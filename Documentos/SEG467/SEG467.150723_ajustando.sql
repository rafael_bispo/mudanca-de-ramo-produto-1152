---PEGANDO  AS PROPOSTAS DE AGENDAMENTO

IF OBJECT_ID('TEMPDB..#agendamento') IS NOT NULL
BEGIN
 DROP TABLE #agendamento
END

SELECT * 
INTO #agendamento
from agendamento_cobranca_atual_tb
WHERE proposta_id IN (255706016,255706021,255706023,255706029,255706031,255706035,255706037,255706039                     --,41620484 (Cancelado) - 719)
                      --,41620482 (Cancelado) - 719)
                      )
AND num_cobranca = 1       

IF OBJECT_ID('TEMPDB..#t') IS NOT NULL
BEGIN
 DROP TABLE #t
END


SELECT   
   c.proposta_id
  ,CONVERT(VARCHAR(MAX),NULL)                                                                                     [Numero_do_Registro]
  ,ISNULL(e.proposta_bb,0)                                                                                        [Numero_da_proposta]   
  ,ISNULL(cer.certificado_id,0)                                                                                   [Numero_do_certificado]
  ,convert(VARCHAR,  ISNULL(CTR.nr_vrs_eds,0))                                                                    [Numero_do_Endosso]  
  ,convert(VARCHAR,  ISNULL(C.nosso_numero_cobranca,0))                                                           [Nosso_Numero]
  ,convert(VARCHAR,  ISNULL(c.nosso_numero_dv,0))                                                                 [Nosso_N�mero_DV]
  ,convert(VARCHAR,  c.num_cobranca)                                                                              [Numero_da_Parcela] 
  ,replace(convert(varchar(10),c.dt_agendamento, 104), '.', '')                                                   [Data_de_Liquidacao]
  ,c.deb_agencia_id                                                                                               [C�digo_da_agencia]
  ,0                                                                                                              [D�gito_verificador_agencia]  
  ,CONVERT(NUMERIC(13,2),c.val_cobranca)                                                                          [Valor_da_parcela]
  ,CONVERT(NUMERIC(13,2),c.val_cobranca)                                                                          [Valor_liquido]
  ,CONVERT(NUMERIC(13,2),0)                                                                                       [Valor_custo_apolice]
  ,CONVERT(NUMERIC(13,2),c.val_iof)                                                                               [Valor_IOF]
  ,CONVERT(NUMERIC(13,2),c.val_comissao_estipulante)                                                              [Valor_do_pro_labore_do_estipulante]
  ,CONVERT(NUMERIC(13,2),0)                                                                                       [Valor_IR_estipulante]
  ,CONVERT(NUMERIC(13,2),c.val_adic_fracionamento)                                                                [Valor_juros]
  ,CONVERT(NUMERIC(13,2),c.val_remuneracao)                                                                       [Valor_remuneracao] 
  ,CONVERT(NUMERIC(13,2),c.val_comissao)                                                                          [Valor_comissao_de_corretor]
  ,CONVERT(NUMERIC(13,2),c.val_ir)                                                                                [Valor_IR_comissao_de_corretor]
  ,CONVERT(NUMERIC(13,2),ISNULL(c.val_cobranca,0) 
    - ISNULL(c.val_iof,0) 
    - ISNULL(c.val_comissao,0) 
    - ISNULL(c.val_comissao_estipulante,0) 
    - ISNULL(c.val_remuneracao,0) 
    + ISNULL(c.val_ir,0))                                                                                         [Valor_repasse]
 ,replace(convert(varchar(10),c.dt_agendamento, 104), '.', '')                                                    [Data_do_vencimento]
 ,0                                                                                                               [Ind_duplicidade]
 ,0                                                                                                               [Ind_repasse_novo]
 ,' '                                                                                                             [Pgto_Flex]
 ,p.subramo_id                                                                                                    [Cod_Ramo]
 ,CONVERT(NUMERIC(13,2),0) AS val_total_repasse
 ,CONVERT(NUMERIC(13,2),0) AS val_total_comissao
  ,linha = CONVERT(VARCHAR(MAX),null )
INTO #T
FROM   #agendamento c
    INNER JOIN proposta_tb p
             ON( C.proposta_id = P.proposta_id ) 
    INNER JOIN produto_tb O 
             ON( O.produto_id = P.produto_id ) 
    LEFT JOIN als_produto_db..item_mdld_prd B
             ON ( O.produto_id = B.cd_prd ) 
    LEFT JOIN als_produto_db..item_mdld_prd_hist_tb A 
             ON  A.cd_item_mdld = B.cd_item_mdld 
             AND a.dt_fim_vigencia is null
    INNER JOIN proposta_adesao_tb E 
             ON P.proposta_id = E.proposta_id
    LEFT JOIN certificado_RE_tb cer
             ON e.proposta_id = cer.proposta_id          
    LEFT JOIN als_operacao_db.dbo.ctr_sgro ctr
             ON ctr.nr_ctr_sgro_bb = e.proposta_bb
               AND ctr.cd_prd = a.cd_prd_BB
               AND ctr.cd_mdld = a.cd_mdld_BB
               AND ctr.cd_item_mdld = a.cd_item_mdld
               AND nr_vrs_eds = (SELECT MAX(nr_vrs_eds) FROM als_operacao_db.dbo.eds_ctr_Sgro eds2
                                      WHERe EDS2.NR_CTR_sGRO = ctr.nr_ctr_Sgro  )   
  
  
UNION ALL

SELECT   
   c.proposta_id
  ,CONVERT(VARCHAR(MAX),NULL)                                                                                     [Numero_do_Registro]
  ,ISNULL(e.proposta_bb,0)                                                                                        [Numero_da_proposta]   
  ,ISNULL(cer.apolice_id,0)                                                                                       [Numero_do_certificado]
  ,convert(VARCHAR,  ISNULL(CTR.nr_vrs_eds,0))                                                                    [Numero_do_Endosso]  
  ,convert(VARCHAR,  ISNULL(C.nosso_numero_cobranca,0))                                                           [Nosso_Numero]
  ,convert(VARCHAR,  ISNULL(c.nosso_numero_dv,0))                                                                 [Nosso_N�mero_DV]
  ,convert(VARCHAR,  c.num_cobranca)                                                                              [Numero_da_Parcela] 
  ,replace(convert(varchar(10),c.dt_agendamento, 104), '.', '')                                                   [Data_de_Liquidacao]
  ,c.deb_agencia_id                                                                                               [C�digo_da_agencia]
  ,0                                                                                                              [D�gito_verificador_agencia]  
  ,CONVERT(NUMERIC(13,2),c.val_cobranca)                                                                          [Valor_da_parcela]
  ,CONVERT(NUMERIC(13,2),c.val_cobranca)                                                                          [Valor_liquido]
  ,CONVERT(NUMERIC(13,2),0)                                                                                       [Valor_custo_apolice]
  ,CONVERT(NUMERIC(13,2),c.val_iof)                                                                               [Valor_IOF]
  ,CONVERT(NUMERIC(13,2),c.val_comissao_estipulante)                                                              [Valor_do_pro_labore_do_estipulante]
  ,CONVERT(NUMERIC(13,2),0)                                                                                       [Valor_IR_estipulante]
  ,CONVERT(NUMERIC(13,2),c.val_adic_fracionamento)                                                                [Valor_juros]
  ,CONVERT(NUMERIC(13,2),c.val_remuneracao)                                                                       [Valor_remuneracao] 
  ,CONVERT(NUMERIC(13,2),c.val_comissao)                                                                          [Valor_comissao_de_corretor]
  ,CONVERT(NUMERIC(13,2),c.val_ir)                                                                                [Valor_IR_comissao_de_corretor]
  ,CONVERT(NUMERIC(13,2),ISNULL(c.val_cobranca,0) 
    - ISNULL(c.val_iof,0) 
    - ISNULL(c.val_comissao,0) 
    - ISNULL(c.val_comissao_estipulante,0) 
    - ISNULL(c.val_remuneracao,0) 
    + ISNULL(c.val_ir,0))                                                                                         [Valor_repasse]
 ,replace(convert(varchar(10),c.dt_agendamento, 104), '.', '')                                                    [Data_do_vencimento]
 ,0                                                                                                               [Ind_duplicidade]
 ,0                                                                                                               [Ind_repasse_novo]
 ,' '                                                                                                             [Pgto_Flex]
 ,p.subramo_id                                                                                                    [Cod_Ramo]
 ,CONVERT(NUMERIC(13,2),0) AS val_total_repasse
 ,CONVERT(NUMERIC(13,2),0) AS val_total_comissao
  ,linha = CONVERT(VARCHAR(MAX),null )
--INTO #T
FROM   #agendamento c
    INNER JOIN proposta_tb p
             ON( C.proposta_id = P.proposta_id ) 
    INNER JOIN produto_tb O 
             ON( O.produto_id = P.produto_id ) 
    LEFT JOIN als_produto_db..item_mdld_prd B
             ON ( O.produto_id = B.cd_prd ) 
    LEFT JOIN als_produto_db..item_mdld_prd_hist_tb A 
             ON  A.cd_item_mdld = B.cd_item_mdld 
             AND a.dt_fim_vigencia is null
    INNER JOIN proposta_fechada_tb E 
             ON P.proposta_id = E.proposta_id
    INNER JOIN apolice_tb cer
             ON e.proposta_id = cer.proposta_id          
    LEFT JOIN als_operacao_db.dbo.ctr_sgro ctr
             ON ctr.nr_ctr_sgro_bb = e.proposta_bb
               AND ctr.cd_prd = a.cd_prd_BB
               AND ctr.cd_mdld = a.cd_mdld_BB
               AND ctr.cd_item_mdld = a.cd_item_mdld
               AND nr_vrs_eds = (SELECT MAX(nr_vrs_eds) FROM als_operacao_db.dbo.eds_ctr_Sgro eds2
                                      WHERE EDS2.NR_CTR_sGRO = ctr.nr_ctr_Sgro  )   
                                   
;
	 
		 
	  WITH A AS 
      (
       SELECT tot_com = SUM([Valor_comissao_de_corretor]) OVER()
             ,tot_rep = SUM([Valor_repasse]) OVER()
             ,val_total_comissao
             ,val_total_repasse
             ,proposta_id 
             ,numero_do_registro, 
             RIGHT(REPLICATE('0', 6 ) + CONVERT(VARCHAR,ROW_NUMBER() OVER(ORDER BY proposta_id)),6) as registro FROM #T
      )
      UPDATE A
      SET numero_do_registro = registro
          ,val_total_comissao = tot_com
          ,val_total_repasse = tot_rep 
      FROM A
              
  
--[Numero_do_Registro] 
--[Numero_da_Proposta]
--[Numero_do_Certificado]
--[Numero_do_Endosso]
--[Nosso_Numero]
--[Nosso_N�mero_DV]
--[Numero_da_Parcela]
--[Data_de_Liquidacao]
--[Codigo_da_ag�ncia]
--[Digito_verificador_agencia]
--[Valor_total]
--[Valor_liquido]
--[Valor_custo_de_apolice]
--[Valor_IOF]
--[Valor_estipulante]
--[Valor_IR_estipulante]
--[Valor_juros]
--[Valor_remuneracao]
--[Valor_corretagem]
--[Valor_IR_Corretagem]
--[Valor_Repasse]
--[Data_vencimento]
--[Ind_duplicidade]
--[Ind_repasse_novo]
--[Pgto_Flex]
--[Cod_Ramo]



---RETIRANDO OS PONTOS 
UPDATE #T 
SET linha =
            [Numero_do_Registro] 
            +RIGHT(REPLICATE('0', 9 ) + CONVERT(VARCHAR,[Numero_da_Proposta]),9)
            +RIGHT(REPLICATE('0', 9 ) + CONVERT(VARCHAR,[Numero_do_Certificado]),9)
            +RIGHT(REPLICATE('0', 9 ) + CONVERT(VARCHAR,[Numero_do_Endosso]),9)
            +RIGHT(REPLICATE('0', 17 ) + CONVERT(VARCHAR,[Nosso_Numero]),17)
            +RIGHT(REPLICATE('0', 1 ) + CONVERT(VARCHAR,[Nosso_N�mero_DV]),1)
            +RIGHT(REPLICATE('0', 3 ) + CONVERT(VARCHAR,[Numero_da_Parcela]),3)
            + CONVERT(VARCHAR,[Data_de_Liquidacao])
            +RIGHT(REPLICATE('0', 4 ) + CONVERT(VARCHAR,[Codigo_da_ag�ncia]),4)
            +RIGHT(REPLICATE('0', 1 ) + CONVERT(VARCHAR,[Digito_verificador_agencia]),1)
            +RIGHT(REPLICATE('0', 15 ) + CONVERT(VARCHAR,REPLACE([Valor_da_parcela],'.','')),15)
            +RIGHT(REPLICATE('0', 15 ) + CONVERT(VARCHAR,REPLACE([Valor_liquido] ,'.','')),15)
            +RIGHT(REPLICATE('0', 15 ) + CONVERT(VARCHAR,REPLACE([Valor_custo_apolice],'.','')),15)  
            +RIGHT(REPLICATE('0', 15 ) + CONVERT(VARCHAR,REPLACE([Valor_IOF],'.','')),15)
            +RIGHT(REPLICATE('0', 15 ) + CONVERT(VARCHAR,REPLACE([Valor_do_pro_labore_do_estipulante],'.','')),15)
            +RIGHT(REPLICATE('0', 15 ) + CONVERT(VARCHAR,REPLACE([Valor_IR_estipulante],'.','')),15)
            +RIGHT(REPLICATE('0', 15 ) + CONVERT(VARCHAR,REPLACE([Valor_juros],'.','')),15)
            +RIGHT(REPLICATE('0', 15 ) + CONVERT(VARCHAR,REPLACE([Valor_remuneracao],'.','')),15) 
            +RIGHT(REPLICATE('0', 15 ) + CONVERT(VARCHAR,REPLACE([Valor_comissao_de_corretor],'.','')),15)
            +RIGHT(REPLICATE('0', 15 ) + CONVERT(VARCHAR,REPLACE([Valor_IR_comissao_de_corretor],'.','')),15)
            +RIGHT(REPLICATE('0', 15 ) + CONVERT(VARCHAR,REPLACE([Valor_repasse],'.','')),15)
            + CONVERT(VARCHAR,[Data_do_vencimento])
            +RIGHT(REPLICATE('0', 1 ) + CONVERT(VARCHAR,[Ind_duplicidade]),1)
            +RIGHT(REPLICATE('0', 1 ) + CONVERT(VARCHAR,[Ind_repasse_novo]),1)
            +RIGHT(REPLICATE('0', 1 ) + CONVERT(VARCHAR,[Pgto_Flex]),1)
            +RIGHT(REPLICATE('0', 9 ) + CONVERT(VARCHAR,[Cod_Ramo]),9)
            +REPLICATE(' ', 48)


	--INSERIR TRAILLER E CABE�ALLHO E OUTRA TABELA AUXILIAR
	 
	IF OBJECT_ID('TEMPDB..#tT') IS NOT NULL
BEGIN
 DROP TABLE #tT
END 
	 
	 
	 
	SELECT LINHA into #TT FROM #T
	
	 INSERT INTO #TT (LINHA)
	 SELECT   ('999999'
	          + '74' 
	          + '8229150'
	          + '6211'
	          + RIGHT(REPLICATE('0', 6 ) + REPLACE(CONVERT(VARCHAR,(SELECT COUNT(*)  from #t )),'.',''), 6 )
	          + RIGHT(REPLICATE('0', 17 ) + REPLACE(CONVERT(VARCHAR,(SELECT TOP 1 val_total_repasse FROM #t)),'.',''), 17 )  
	          + RIGHT(REPLICATE('0', 17 ) + REPLACE(CONVERT(VARCHAR,(SELECT TOP 1 val_total_comissao FROM #t)),'.',''), 17 )                             
	          + '                 ' 
	          + '00000000000000000' 
	          +  REPLICATE(' ', 207)
	          )
	 
	 INSERT INTO #TT (LINHA)
	 VALUES ('000000748229150621130000000002205202030052020' + REPLICATE(' ', 255))
	 
	  
      
   SELECT linha FROM #TT
    ORDER BY substring(linha, 1, 6)




    
    
--select * from proposta_fechada_tb  a 
--inner join proposta_tb b
--on a.proposta_id = b.proposta_id
----and b.produto_id = 109
--where proposta_bb = 259202422





--select * from agendamento_cobranca_atual_Tb where proposta_id in (41604934
--                ,41341338
--                ,41600038
--                ,41155433) and num_cobranca = 1
                
--select sum(CONVERT(NUMERIC(13,2),ISNULL(c.val_cobranca,0) 
--    - ISNULL(c.val_iof,0) 
--    - ISNULL(c.val_comissao,0) 
--    - ISNULL(c.val_comissao_estipulante,0) 
--    - ISNULL(c.val_remuneracao,0) 
--    + ISNULL(c.val_ir,0))), sum(val_comissao)        from agendamento_cobranca_atual_Tb c where 
--proposta_id in (41604934
--                ,41341338
--                ,41600038
--                ,41155433)
--and num_cobranca = 1         


       