VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.2#0"; "MSCOMCTL.OCX"
Begin VB.Form FrmImportacaoAvaliacaoRetornoBB 
   Caption         =   "SEG00000 - Importa��o Avaliacao Retorno BB - "
   ClientHeight    =   6405
   ClientLeft      =   3390
   ClientTop       =   2400
   ClientWidth     =   7965
   ForeColor       =   &H80000016&
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   6405
   ScaleWidth      =   7965
   Begin MSComctlLib.ProgressBar term1 
      Height          =   255
      Left            =   120
      TabIndex        =   17
      Top             =   5730
      Visible         =   0   'False
      Width           =   5715
      _ExtentX        =   10081
      _ExtentY        =   450
      _Version        =   393216
      Appearance      =   0
   End
   Begin VB.CommandButton BtnOk 
      Caption         =   "&Ok"
      Height          =   315
      Left            =   5880
      TabIndex        =   4
      Top             =   5700
      Width           =   975
   End
   Begin VB.CommandButton btnCancelar 
      Caption         =   "&Cancelar"
      Height          =   315
      Left            =   6840
      TabIndex        =   3
      Top             =   5700
      Width           =   975
   End
   Begin TabDlg.SSTab SSTab1 
      Height          =   5535
      Left            =   90
      TabIndex        =   0
      Top             =   120
      Width           =   7755
      _ExtentX        =   13679
      _ExtentY        =   9763
      _Version        =   393216
      Style           =   1
      Tabs            =   2
      TabsPerRow      =   2
      TabHeight       =   520
      TabCaption(0)   =   "Importa��o"
      TabPicture(0)   =   "ImportacaoAvaliacaoRetornoBB.frx":0000
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "Frame2"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "Frame3"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "Frame1"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).ControlCount=   3
      TabCaption(1)   =   "Hist�rico"
      TabPicture(1)   =   "ImportacaoAvaliacaoRetornoBB.frx":001C
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "grid1"
      Tab(1).Control(1)=   "Command4"
      Tab(1).Control(2)=   "BtnAtualizar"
      Tab(1).ControlCount=   3
      Begin VB.Frame Frame1 
         Caption         =   "Inconsistencias da Importa��o"
         Height          =   1335
         Left            =   120
         TabIndex        =   20
         Top             =   4080
         Width           =   7455
         Begin VB.ListBox ListInconsistencias 
            Height          =   840
            Left            =   150
            TabIndex        =   21
            Top             =   270
            Width           =   7125
         End
      End
      Begin VB.CommandButton BtnAtualizar 
         Caption         =   "Buscar"
         Height          =   315
         Left            =   -69540
         TabIndex        =   5
         Top             =   3390
         Width           =   2025
      End
      Begin VB.CommandButton Command4 
         Caption         =   "&Buscar"
         Height          =   315
         Left            =   -69060
         TabIndex        =   6
         Top             =   5460
         Width           =   975
      End
      Begin VB.Frame Frame3 
         Caption         =   "Situa��o do Arquivo: "
         Height          =   1245
         Left            =   120
         TabIndex        =   9
         Top             =   2760
         Width           =   7455
         Begin VB.Label lblstatus 
            AutoSize        =   -1  'True
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Left            =   840
            TabIndex        =   15
            Top             =   900
            Width           =   1875
         End
         Begin VB.Label lblver 
            AutoSize        =   -1  'True
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Left            =   840
            TabIndex        =   14
            Top             =   600
            Width           =   75
         End
         Begin VB.Label lblarq 
            AutoSize        =   -1  'True
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Left            =   840
            TabIndex        =   13
            Top             =   300
            Width           =   75
         End
         Begin VB.Label Label3 
            AutoSize        =   -1  'True
            Caption         =   "Status:"
            Height          =   195
            Left            =   210
            TabIndex        =   12
            Top             =   900
            Width           =   495
         End
         Begin VB.Label Label2 
            AutoSize        =   -1  'True
            Caption         =   "Vers�o:"
            Height          =   195
            Left            =   165
            TabIndex        =   11
            Top             =   600
            Width           =   540
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "Arquivo:"
            Height          =   195
            Left            =   120
            TabIndex        =   10
            Top             =   300
            Width           =   585
         End
      End
      Begin VB.Frame Frame2 
         Caption         =   "Arquivo:"
         Height          =   2325
         Left            =   120
         TabIndex        =   8
         Top             =   360
         Width           =   7470
         Begin VB.TextBox fldpath 
            BackColor       =   &H8000000F&
            Height          =   330
            Left            =   150
            Locked          =   -1  'True
            TabIndex        =   1
            Top             =   450
            Width           =   7050
         End
         Begin VB.FileListBox lstarquivo 
            Height          =   870
            Left            =   120
            Pattern         =   "movvn*"
            TabIndex        =   2
            Top             =   1170
            Width           =   7095
         End
         Begin VB.Label Label4 
            Caption         =   "Arquivos"
            Height          =   195
            Index           =   1
            Left            =   150
            TabIndex        =   19
            Top             =   900
            Width           =   1320
         End
         Begin VB.Label Label4 
            Caption         =   "Path"
            Height          =   195
            Index           =   0
            Left            =   180
            TabIndex        =   18
            Top             =   225
            Width           =   1320
         End
      End
      Begin MSFlexGridLib.MSFlexGrid grid1 
         Height          =   2865
         Left            =   -74820
         TabIndex        =   16
         Top             =   450
         Width           =   7305
         _ExtentX        =   12885
         _ExtentY        =   5054
         _Version        =   393216
         Cols            =   10
         FixedCols       =   0
         FocusRect       =   0
         SelectionMode   =   1
         AllowUserResizing=   1
      End
   End
   Begin MSComctlLib.StatusBar StatusBar1 
      Align           =   2  'Align Bottom
      Height          =   315
      Left            =   0
      TabIndex        =   7
      Top             =   6090
      Width           =   7965
      _ExtentX        =   14049
      _ExtentY        =   556
      Style           =   1
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "FrmImportacaoAvaliacaoRetornoBB"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Dim rdocn1 As New rdoConnection
Dim Reg As String

Dim wk_seg_remessa_arq               As String  '* jmendes - 15/04/2002
Dim wk_seg_remessa_arq_retorno_bb    As String  '* jmendes - 15/04/2002

Dim filenum As Integer
Dim hor As String, dat As String
Dim qtde As String, obs As String
Dim flag_importacao As Boolean
Dim Flag_Erro_Trailler As Boolean
Dim versao_atual As Long, ultima_versao As Long
Dim arqpath As String
Dim copypath As String
Dim logpath As String
Dim arqinput As String
Dim NomeArqRet As String
Dim LogErro As Boolean

Dim tipo_arquivo As String
Dim ano_mes_ref As String

'Stefanini (Maur�cio), em 07/10/2005 para verificar se todos os arquivos
'foram processados corretamente.
Dim flag_todosprocessados As Boolean

'variavel que vai susbstituir no c�digo as valida��es que eram feitas a partir de lblarquivo - ecosta 18/09/08
Public StrArquivo As String
Public StrArquivoVrs As String

'Confitec Sistemas - Diogo Ferreira 17/01/2012 - Demanda 13981228: Inclus�o de mensagem de erro no programa para recusas do conta cart�o
Dim bRecusaContaCartao As Boolean
Dim bRecusaContaCartaoTarefa As Boolean
'Confitec Sistemas - Luiz Gabriel Lopes 10/02/2012 - Demanda 12240504 - TRATAMENTO RECUSA DE COBRAN�A - FATURAMENTO CDC EX�RCITO
Sub Importa_Avaliacoes_Retorno_ALSF159_BB()

Dim Arquivo As String
Dim Caminho As String
Dim SQL As String
Dim Rst As rdoResultset

On Error GoTo TrataErro

lblstatus.Caption = "Processando"

Arquivo = lstarquivo.FileName
Caminho = lstarquivo.PATH & "\" & lstarquivo.FileName

rdocn.BeginTrans
rdocn_Seg2.BeginTrans

SQL = "exec SEGUROS_DB..SEGS9949_SPI '" & Arquivo & "'"
SQL = SQL & ",'" & cUserName & "'"
SQL = SQL & ",'" & Caminho & "'"
Set Rst = rdocn.OpenResultset(SQL)

lblstatus.Caption = "Movendo arquivo"
Move_Arquivo arqpath + "\" + StrArquivoVrs, arqpath + "\..\processados\" + StrArquivoVrs
goProducao.AdicionaLog 2, Rst("Total_Registro_processados"), lstarquivo.ListIndex + 1

'grava log arquivo CTM
If CTM = True Then
    Call GravaLogCTM("OK - ", "", "", Rst("Total_Registro_processados"))
End If


Call Atualiza_Controle_Arquivo(StrArquivoVrs, lblver.Caption, Rst("Total_Registro_arquivo"), Rst("Total_Registro_processados"))
Flag_Erro_Trailler = False

rdocn.CommitTrans
rdocn_Seg2.CommitTrans
Set Rst = Nothing

lblstatus.Caption = "Conclu�do"
BtnOk.Enabled = False
grava_log

Exit Sub

TrataErro:
    Move_Arquivo arqpath + "\" + StrArquivoVrs, arqpath + "\..\pendentes\" + StrArquivoVrs
    Flag_Erro_Trailler = True
    rdocn.RollbackTrans
    rdocn_Seg2.RollbackTrans
    TrataErroGeral "Importa_Avaliacoes_Retorno_ALSF159_BB " & SQL, Me.name
    Call TerminaSEGBR

End Sub

Sub Importa_Avaliacoes_Retorno_SEG987ZAA_BB()

Dim Arquivo As String
Dim Caminho As String
Dim SQL As String
Dim Rst As rdoResultset

On Error GoTo TrataErro

lblstatus.Caption = "Processando"

Arquivo = lstarquivo.FileName
Caminho = lstarquivo.PATH & "\" & lstarquivo.FileName

rdocn.BeginTrans
rdocn_Seg2.BeginTrans

SQL = "exec SEGS8423_SPI '" & Arquivo & "'"
SQL = SQL & ",'" & cUserName & "'"
SQL = SQL & ",'" & Caminho & "'"
Set Rst = rdocn.OpenResultset(SQL)

lblstatus.Caption = "Movendo arquivo"
Move_Arquivo arqpath + "\" + StrArquivoVrs, arqpath + "\..\processados\" + StrArquivoVrs
goProducao.AdicionaLog 2, Rst("Total_Registro_processados"), lstarquivo.ListIndex + 1

'grava log arquivo CTM
If CTM = True Then
    Call GravaLogCTM("OK - ", "", "", Rst("Total_Registro_processados"))
End If

Call Atualiza_Controle_Arquivo(StrArquivoVrs, lblver.Caption, Rst("Total_Registro_arquivo"), Rst("Total_Registro_processados"))
Flag_Erro_Trailler = False

rdocn.CommitTrans
rdocn_Seg2.CommitTrans

Set Rst = Nothing

lblstatus.Caption = "Conclu�do"
BtnOk.Enabled = False
grava_log

Exit Sub

TrataErro:
    Move_Arquivo arqpath + "\" + StrArquivoVrs, arqpath + "\..\pendentes\" + StrArquivoVrs
    Flag_Erro_Trailler = True
    rdocn.RollbackTrans
    rdocn_Seg2.RollbackTrans
    TrataErroGeral "Importa_Avaliacoes_Retorno_SEG987SZ_BB " & SQL, Me.name
    Call TerminaSEGBR

End Sub

'PAULO PELEGRINI - 27/06/2016 - 19306878 - Incus�o do arquivo: SEG987X (INI)
Sub Importa_Avaliacoes_Retorno_SEG987X()

Dim Arquivo As String
Dim Caminho As String
Dim SQL As String
Dim Rst As rdoResultset

On Error GoTo TrataErro

lblstatus.Caption = "Processando"

Arquivo = lstarquivo.FileName
Caminho = lstarquivo.PATH & "\" & lstarquivo.FileName

rdocn.BeginTrans
rdocn_Seg2.BeginTrans

SQL = "exec SEGS12961_SPI '" & Arquivo & "'"
SQL = SQL & ",'" & cUserName & "'"
SQL = SQL & ",'" & Caminho & "'"
Set Rst = rdocn.OpenResultset(SQL)

lblstatus.Caption = "Movendo arquivo"
Move_Arquivo arqpath + "\" + StrArquivoVrs, arqpath + "\..\processados\" + StrArquivoVrs
goProducao.AdicionaLog 2, Rst("Total_Registro_processados"), lstarquivo.ListIndex + 1

'grava log arquivo CTM
If CTM = True Then
    Call GravaLogCTM("OK - ", "", "", Rst("Total_Registro_processados"))
End If

Call Atualiza_Controle_Arquivo(StrArquivoVrs, lblver.Caption, Rst("Total_Registro_arquivo"), Rst("Total_Registro_processados"))
Flag_Erro_Trailler = False

rdocn.CommitTrans
rdocn_Seg2.CommitTrans

Set Rst = Nothing

lblstatus.Caption = "Conclu�do"
BtnOk.Enabled = False
grava_log

Exit Sub

TrataErro:
    Move_Arquivo arqpath + "\" + StrArquivoVrs, arqpath + "\..\pendentes\" + StrArquivoVrs
    Flag_Erro_Trailler = True
    rdocn.RollbackTrans
    rdocn_Seg2.RollbackTrans
    TrataErroGeral "Importa_Avaliacoes_Retorno_SEG987X " & SQL, Me.name
    Call TerminaSEGBR

End Sub
'PAULO PELEGRINI - 27/06/2016 - 19306878 - Incus�o do arquivo: SEG987X (FIM)


Sub Importa_Avaliacoes_Retorno_SEG987S_BB()

Dim Arquivo As String
Dim Caminho As String
Dim SQL As String
Dim Rst As rdoResultset

On Error GoTo TrataErro

lblstatus.Caption = "Processando"

Arquivo = lstarquivo.FileName
Caminho = lstarquivo.PATH & "\" & lstarquivo.FileName

rdocn.BeginTrans
rdocn_Seg2.BeginTrans

SQL = "exec SEGS8423_SPI '" & Arquivo & "'"
SQL = SQL & ",'" & cUserName & "'"
SQL = SQL & ",'" & Caminho & "'"
Set Rst = rdocn_Seg2.OpenResultset(SQL)

lblstatus.Caption = "Movendo arquivo"
Move_Arquivo arqpath + "\" + StrArquivoVrs, arqpath + "\..\processados\" + StrArquivoVrs
goProducao.AdicionaLog 2, Rst("Total_Registro_processados"), lstarquivo.ListIndex + 1

'grava log arquivo CTM
If CTM = True Then
    Call GravaLogCTM("OK - ", "", "", Rst("Total_Registro_processados"))
End If

Call Atualiza_Controle_Arquivo(StrArquivoVrs, lblver.Caption, Rst("Total_Registro_arquivo"), Rst("Total_Registro_processados"))
Flag_Erro_Trailler = False

rdocn.CommitTrans
rdocn_Seg2.CommitTrans
Set Rst = Nothing

lblstatus.Caption = "Conclu�do"
BtnOk.Enabled = False
grava_log

Exit Sub

TrataErro:
    Move_Arquivo arqpath + "\" + StrArquivoVrs, arqpath + "\..\pendentes\" + StrArquivoVrs
    Flag_Erro_Trailler = True
    rdocn.RollbackTrans
    rdocn_Seg2.RollbackTrans
    TrataErroGeral "Importa_Avaliacoes_Retorno_SEG987SZ_BB " & SQL, Me.name
    Call TerminaSEGBR

End Sub


Function Obtem_Endosso(ByVal PPropostaId As String, ByVal PEndossoBB As String) As String
Dim Rst As rdoResultset
Dim SQL As String

On Error GoTo TrataErro

'# Obtendo a proposta a partir da proposta ades�o'''''''''''''''''''''''''''''''''''''''''''
SQL = "SELECT "
SQL = SQL & "   endosso_id "
SQL = SQL & "FROM "
SQL = SQL & "   endosso_tb  WITH (NOLOCK)  "
SQL = SQL & "WHERE"
SQL = SQL & "   proposta_id = " & PPropostaId
SQL = SQL & "   and num_endosso_bb = " & PEndossoBB
    
Set Rst = rdocn.OpenResultset(SQL)
   
If Not Rst.EOF Then
    Obtem_Endosso = Rst!Endosso_id
Else
    Obtem_Endosso = ""
End If
   Exit Function
   
TrataErro:
    TrataErroGeral "Obtem_Endosso", Me.name
    TerminaSEGBR

End Function

Sub suSelecaoArquivo()

On Error GoTo TrataErro

'* Tabela de equivalencia entre os arquivos enviados para o banco e recebidos do banco
'* De acordo com informa��es do Sr. Nilton da Alian�a do Brasil e o Sr. Pablus da JR & P Sistemas - Rio de JAneiro
'* JMendes - 09/05/2002
'  +----------------------+-----------------------------+----------------------------------------------------------------------+
'  | Cod.Produto          | Nome do Produto             | Nome do Arquivo                                                      |
'  +----------------------+-----------------------------+----------------------------------------------------------------------+
'  | SEG987a 549107       | OURO VIDA EMPRESA           | SEG562                                                               |
'  | SEG987b 678093       | OURO VIDA - APOLICE 40      | BBC551                                                               |
'  | SEG987c 678094       | OURO VIDA - APOLICE 9634    | BBC551                                                               |
'  | SEG987d 678095       | OURO VIDA - APOLICE 12114   | BBC551                                                               |
'  | SEG987e 678102       | OURO VIDA GARANTIA          | SEG551                                                               |
'  | SEG987f 678103       | BB SEGURO TRANSP INTERNAC   | SEGF553 - Projeto SEGP0121                                           |
'  | SEG987g 678106       | OURO MAQUINAS               | SEG552  - Projeto SEGP0122 (SEGF496, SEGF495, SEG493)                |
'  | SEG987h 678109       | OURO RESIDENCIAL            | SEG511  - Projeto SEGP0118 (aceitos) / Projeto SEGP0124 (recusados)  |
'  | SEG987i 678110       | OURO EMPRESARIAL            | SEG510  - Projeto SEGP0118 (aceitos) / Projeto SEGP0124 (recusados)  |
'  | SEG987j 678116       | VIDA PROD. RURAL-SECURIT.   | SEG551  - Projeto SEGP0062                                           |
'  | SEG987k 678126       | OURO VIDA PRODUTOR RURAL    | SEG556                                                               |
'  | SEG987l 678135       | VIDA PROD. RURAL-R.RAPIDO   | SEG558                                                               |
'  | SEG987m 678145       | OURO AGRICOLA               | SEG557                                                               |
'  | SEG987n 8229150      | PERSONALIZADO               | SEG552  - Projeto SEGP0122                                           |
'  | SEG987o 678110       | DEVOLUCAO DE PREMIOS        | SEG452                                                               |
'  | SEG987q              | PRONAF                      | SEG585                                                               |
'  | SEG987R              | CONTA CART�O                |                                                                      |
'  | SEG987S              | QUEBRA DE GARANTIA          | SEG521                                                               |
'  | SEG987SS             | QUEBRA DE GARANTIA IM�VEIS  | SEG110                                                                     |
'  |                      | - DEMAIS PARCELAS           |                                                                      |
'  | SEG987SSS            | QUEBRA DE GARANTIA IM�VEIS  | SEG110                                                                     |
'  |                      | - AVERBA��ES E ENDOSSOS     |                                                                      |
'  | SEG987u 678109       | Renova��o Monit. OURO RES.  | SEG512                                                               |
'  | SEG987v 678109       | Renova��o Monit. OURO Emp.  | SEG514                                                               |
'  | SEG987Z              | CONS�RCIO PRESTAMISTA       | SEG522                                                               |
'  | SEG987AA             | CONS�RCIO IMOBILIARIO       | SEG524                                                               |
'  | SEGB987 678109       | Recusa   Residencial        | SEG456Z                                                              |
'  | SEGC987 678110       | Recusa   Empres.            | SEG456D                                                              |
'  | SEGF987 516          | Reativa. Empres./Residencial| SEGF987                                                              |
'  | SEGD987 8622011      | PROTE��O OURO - Ades�es     | SEGD987                                                              |
'  | SEGE987 8622011      | PROTE��O OURO - Financeiro  | SEGE987                                                              |
'  +----------------------+-----------------------------+----------------------------------------------------------------------+
   
   'ALTERACAO MARCO - Val(Mid(Reg, 30, 4) >> Val(Mid(Reg, 34, 4)  - 03/04/2003
      
    wk_seg_remessa_arq_retorno_bb = ""
    'Select Case UCase$(Left(lblarq, 7))
'comando acima substituido pelo comando abaixo - ecosta 18/09/08
    Select Case UCase$(StrArquivo)
        Case Is = "SEG987A"
            wk_seg_remessa_arq_retorno_bb = "SEG562" & "." & Format(Val(Mid(Reg, 34, 4)), "0000")
        Case Is = "SEG987B"
            wk_seg_remessa_arq_retorno_bb = "BBC551" & "." & Format(Val(Mid(Reg, 34, 4)), "0000")
        Case Is = "SEG987C"
            wk_seg_remessa_arq_retorno_bb = "BBC551" & "." & Format(Val(Mid(Reg, 34, 4)), "0000")
        Case Is = "SEG987D"
            wk_seg_remessa_arq_retorno_bb = "BBC551" & "." & Format(Val(Mid(Reg, 34, 4)), "0000")
''''' INICIO -RCA380 - RF00055767 -  J.LIMA - 18-01-2018 - RETIRANDO TRECHO POIS O ARQUIVO SER� PROCESSADO POR UM NOVO JOB - SEGX2118
        ' Case Is = "SEG987E"
    '        wk_seg_remessa_arq_retorno_bb = "SEG551" & "." & Format(Val(Mid(Reg, 34, 4)), "0000")
'''' FIM
        Case Is = "SEG987F" ', "SEG987P" Alterado por Gustavo Machado Costa em 15/07/2003
            wk_seg_remessa_arq_retorno_bb = "SEG613" & "." & Format(Val(Mid(Reg, 34, 4)), "0000")
        Case Is = "SEG987G"
            wk_seg_remessa_arq_retorno_bb = "SEG552" & "." & Format(Val(Mid(Reg, 34, 4)), "0000")
        Case Is = "SEG987H"
            wk_seg_remessa_arq_retorno_bb = "SEG511" & "." & Format(Val(Mid(Reg, 34, 4)), "0000")
        Case Is = "SEG987I"
            wk_seg_remessa_arq_retorno_bb = "SEG510" & "." & Format(Val(Mid(Reg, 34, 4)), "0000")
        Case Is = "SEG987J"
            wk_seg_remessa_arq_retorno_bb = "SEG551" & "." & Format(Val(Mid(Reg, 34, 4)), "0000")
        ''''' INICIO -RCA380 - RF00055767 -  FLAVIO ABREU - 26-02-2018 - RETIRANDO TRECHO POIS O ARQUIVO SER� PROCESSADO POR UM NOVO JOB - SEGX2118
        'Case Is = "SEG987K"
        '    wk_seg_remessa_arq_retorno_bb = "SEG556" & "." & Format(Val(Mid(Reg, 34, 4)), "0000")
        Case Is = "SEG987L"
            wk_seg_remessa_arq_retorno_bb = "SEG558" & "." & Format(Val(Mid(Reg, 34, 4)), "0000")
        Case Is = "SEG987M" '*** Alterado 16/01/03
            wk_seg_remessa_arq_retorno_bb = "SEG557" & "." & Format(Val(Mid(Reg, 34, 4)), "0000")
        Case Is = "SEG987N"
            If tipo_arquivo = "465" Then
                wk_seg_remessa_arq_retorno_bb = "SEG465" & "." & Format(Val(Mid(Reg, 34, 4)), "0000")
            Else
                wk_seg_remessa_arq_retorno_bb = "SEG552" & "." & Format(Val(Mid(Reg, 34, 4)), "0000")
            End If

        Case Is = "SEG987O"
            wk_seg_remessa_arq_retorno_bb = "SEG452" & "." & Format(Val(Mid(Reg, 34, 4)), "0000")
        'Alterado por Gustavo Machado Costa em 15/07/2003
        ''''' INICIO -RCA380 - RF00055767 -  FLAVIO ABREU - 26-02-2018 - RETIRANDO TRECHO POIS O ARQUIVO SER� PROCESSADO POR UM NOVO JOB - SEGX2118
        'Case Is = "SEG987P"
        '    wk_seg_remessa_arq_retorno_bb = "SEGF553" & "." & Format(Val(Mid(Reg, 34, 4)), "0000")
            
        'Adicionado - PRONAF: Rafael Oshiro 16/11/04
        ''''' INICIO -RCA380 - RF00055767 -  FLAVIO ABREU - 26-02-2018 - RETIRANDO TRECHO POIS O ARQUIVO SER� PROCESSADO POR UM NOVO JOB - SEGX2118
        'Case Is = "SEG987Q"
        '    wk_seg_remessa_arq_retorno_bb = "SEG585" & "." & Format(Val(Mid(Reg, 34, 4)), "0000")
        Case Is = "SEG987R"
            wk_seg_remessa_arq_retorno_bb = "SEG465" & "." & Format(Val(Mid(Reg, 34, 4)), "0000")
            
        'lrocha 03/11/2006 - SEG987S
        Case Is = "SEG987S"
            wk_seg_remessa_arq_retorno_bb = "SEG521" & "." & Format(Val(Mid(Reg, 34, 4)), "0000")
        'Confitec Sistemas - Flavio Abreu 07/03/2014 - SEG987SS
        Case Is = "SEG987SS"
            wk_seg_remessa_arq_retorno_bb = "SEG110" & "." & Format(Val(Mid(Reg, 34, 4)), "0000")
        'Confitec Sistemas - Flavio Abreu 07/03/2014 - SEG987SSS
        Case Is = "SEG987SSS"
            wk_seg_remessa_arq_retorno_bb = "SEG110" & "." & Format(Val(Mid(Reg, 34, 4)), "0000")
        ''''' INICIO -RCA380 - RF00055767 -  FLAVIO ABREU - 26-02-2018 - RETIRANDO TRECHO POIS O ARQUIVO SER� PROCESSADO POR UM NOVO JOB - SEGX2118
        'Case Is = "SEG987T"
        '    wk_seg_remessa_arq_retorno_bb = "SEG588" & "." & Format(Val(Mid(Reg, 34, 4)), "0000")
        'carsilva - 21/05/2004
        Case Is = "SEG987U"
            wk_seg_remessa_arq_retorno_bb = "SEG512" & "." & Format(Val(Mid(Reg, 34, 4)), "0000")
        Case Is = "SEG987V"
            wk_seg_remessa_arq_retorno_bb = "SEG514" & "." & Format(Val(Mid(Reg, 34, 4)), "0000")
        'lrocha 03/11/2006 - SEG987Z
        Case Is = "SEG987Z"
            wk_seg_remessa_arq_retorno_bb = "SEG522" & "." & Format(Val(Mid(Reg, 34, 4)), "0000")
        'ECOSTA 18/09/08 CONS. IMOB.
        Case Is = "SEG987AA"
            wk_seg_remessa_arq_retorno_bb = "SEG524" & "." & Format(Val(Mid(Reg, 34, 4)), "0000")
        'PAULO PELEGRINI - 27/06/2016 - 19306878 - Incus�o do arquivo: SEG987X
        Case Is = "SEG987X"
            wk_seg_remessa_arq_retorno_bb = "SEG587" & "." & Format(Val(Mid(Reg, 34, 4)), "0000")

        'Rodrigo Maiellaro(Stefanini) - 28/8/2006
        Case Is = "SEGB987"
            wk_seg_remessa_arq_retorno_bb = "SEG456Z." & Format(Val(Mid(Reg, 34, 4)), "0000")
        Case Is = "SEGC987"
            wk_seg_remessa_arq_retorno_bb = "SEG456D." & Format(Val(Mid(Reg, 34, 4)), "0000")
        ' JCON-DF 09/08/2006
        
        Case Is = "SEGF987"
        '''Tratamento 987 - FLAVIO.ABBREU - 20141028 - INICIO
        '''CORRE��O NO NOME DE ARQUIVO DE ENVIO
        'wk_seg_remessa_arq_retorno_bb = "SEGF987." & Format(Val(Mid(Reg, 34, 4)), "0000")
            wk_seg_remessa_arq_retorno_bb = "SEG516." & Format(Val(Mid(Reg, 34, 4)), "0000")
        '''Tratamento 987 - FLAVIO.ABBREU - 20141028 - FIM
       
       ' Bruno S Moraes 03/07/2007
        Case Is = "SEGD987"
            wk_seg_remessa_arq_retorno_bb = "SEG602" & "." & Format(Val(Mid(Reg, 34, 4)), "0000")
        Case Is = "SEGE987"
            wk_seg_remessa_arq_retorno_bb = "SEG462" & "." & Format(Val(Mid(Reg, 34, 4)), "0000")
       'Rog�rio - Visanet - Demanda 654224 - 17.07.2009
       Case Is = "SEG987AB"
            wk_seg_remessa_arq_retorno_bb = "SEG544" & "." & Format(Val(Mid(Reg, 34, 4)), "0000")
       Case Is = "SEG987AC"
            wk_seg_remessa_arq_retorno_bb = "SEG549" & "." & Format(Val(Mid(Reg, 34, 4)), "0000")

      'Racras BBC452
       Case Is = "BBC987C"
            wk_seg_remessa_arq_retorno_bb = "BBC452" & "." & Format(Val(Mid(Reg, 34, 4)), "0000")
        
       ' gustagom - Demanda 17755093 - 27/11/2012
       Case Is = "SEGG987"
            wk_seg_remessa_arq_retorno_bb = "SEG456B" & "." & Format(Val(Mid(Reg, 34, 4)), "0000")

    End Select
   Exit Sub
   
TrataErro:
    TrataErroGeral "suSelecaoArquivo", Me.name
    TerminaSEGBR
    
End Sub
Function Verifica_Produto_Restituicao_Automatica(ByVal PProduto As String, ByVal Pcod_empresa As String) As Boolean

Dim SQL As String
Dim Rst As rdoResultset

On Error GoTo TrataErro

    SQL = "Select "
    SQL = SQL & "     processa_restituicao_automatica = isnull(processa_restituicao_automatica, 'n') "
    SQL = SQL & " from "
    SQL = SQL & "     produto_tb   WITH (NOLOCK)  "
    SQL = SQL & " where "
    SQL = SQL & "     produto_id = " & PProduto

'Gmarques
If Pcod_empresa = "AB" Then
    Set Rst = rdocn.OpenResultset(SQL)
End If

If Pcod_empresa = "ABS" Then
    Set Rst = rdocn_Seg2.OpenResultset(SQL)
End If

If UCase(Rst!processa_restituicao_automatica) = "S" Then
   Verifica_Produto_Restituicao_Automatica = True
Else
   Verifica_Produto_Restituicao_Automatica = False
End If
Exit Function
   
TrataErro:
    TrataErroGeral "Verifica_Produto_Restituicao_Automatica", Me.name
    TerminaSEGBR

End Function

'Function Verifica_Existencia_Endosso_old(ByVal PPropostaId As String, ByVal PEndossoId As String) As Boolean
'
'Dim Rst As rdoResultset
'Dim SQL As String
'
''# Obtendo a proposta a partir da proposta ades�o'''''''''''''''''''''''''''''''''''''''''''
'
'On Error GoTo TrataErro
'
'SQL = "SELECT "
'SQL = SQL & "   '1' "
'SQL = SQL & "FROM "
'SQL = SQL & "   endosso_tb  (nolock) "
'SQL = SQL & "WHERE"
'SQL = SQL & "   proposta_id = " & PPropostaId
'SQL = SQL & "   and endosso_id = " & PEndossoId
'
''Rafael Oshiro - 27/12/2004
''Procurar pelo endosso na emi_re_proposta_tb e na emi_proposta_tb
'SQL = SQL & " UNION "
'
'SQL = SQL & "SELECT '2' "
'SQL = SQL & "FROM   emi_re_proposta_tb "
'SQL = SQL & "WHERE  proposta_id = " & PPropostaId
'SQL = SQL & "  AND  endosso_id  = " & PEndossoId
'
'SQL = SQL & " UNION "
'
'SQL = SQL & "SELECT '3' "
'SQL = SQL & "FROM   emi_proposta_tb "
'SQL = SQL & "WHERE  proposta_id = " & PPropostaId
'SQL = SQL & "  AND  endosso_id  = " & PEndossoId
'
'Set Rst = rdocn.OpenResultset(SQL)
'
'If Not Rst.EOF Then
'    Verifica_Existencia_Endosso = True
'Else
'    Verifica_Existencia_Endosso = False
'End If
'
'Exit Function
'
'TrataErro:
'    TrataErroGeral "Verifica_Existencia_Endosso", Me.name
'    TerminaSEGBR
'
'End Function

Function Verifica_Existencia_Endosso(ByVal PPropostaId As String, ByVal PEndossoId As String, ByVal Pcod_empresa As String) As Boolean

Dim Rst As rdoResultset
Dim SQL As String

'# Obtendo a proposta a partir da proposta ades�o'''''''''''''''''''''''''''''''''''''''''''

On Error GoTo TrataErro

    SQL = "SELECT "
    SQL = SQL & "   '1' "
    SQL = SQL & "FROM "
    SQL = SQL & "   endosso_tb   WITH (NOLOCK)  "
    SQL = SQL & "WHERE"
    SQL = SQL & "   proposta_id = " & PPropostaId
    SQL = SQL & "   and endosso_id = " & PEndossoId
    SQL = SQL & " UNION "
    SQL = SQL & "SELECT '1' "
    SQL = SQL & "FROM   emi_re_proposta_tb "
    SQL = SQL & "WHERE  proposta_id = " & PPropostaId
    SQL = SQL & "  AND  endosso_id  = " & PEndossoId
    SQL = SQL & " UNION "
    SQL = SQL & "SELECT '1' "
    SQL = SQL & "FROM   emi_proposta_tb "
    SQL = SQL & "WHERE  proposta_id = " & PPropostaId
    SQL = SQL & "  AND  endosso_id  = " & PEndossoId
    '-----------------------------------------------------------------------
    'WBarboza/Fabreu - 2014-07-21
    SQL = SQL & " UNION "
    SQL = SQL & "SELECT '1' "
    SQL = SQL & "FROM   proposta_tb "
    SQL = SQL & "JOIN   endosso_resumo_tb "
    SQL = SQL & "  ON   endosso_resumo_tb.proposta_id = proposta_tb.proposta_id "
    SQL = SQL & "WHERE  proposta_tb.proposta_id = " & PPropostaId
    SQL = SQL & "  AND  endosso_resumo_tb.endosso_resumo_id = " & PEndossoId
    SQL = SQL & "  AND  proposta_tb.produto_id IN (1125, 1141)"

If Pcod_empresa = "AB" Then
    Set Rst = rdocn.OpenResultset(SQL)
End If

If Pcod_empresa = "ABS" Then
    Set Rst = rdocn_Seg2.OpenResultset(SQL)
End If

If Not Rst.EOF Then
    Verifica_Existencia_Endosso = True
Else
    Verifica_Existencia_Endosso = False
End If

Exit Function

   
TrataErro:
    TrataErroGeral "Verifica_Existencia_Endosso", Me.name
    TerminaSEGBR

End Function


'Barney - 22/04/2004
Sub Obtem_Movimentacao_Financeira(ByVal sPropostaId As String, _
                                  ByVal sEndossoId As String, _
                                  ByRef sMovimentacaoId As String, _
                                  ByRef sEndossoResumoId As String, _
                                  ByRef sCod_empresa As String, _
                                  ByRef ProdutoProposta As String)  'INCIDENTE :SD00005455 - ANDERSON.SANTOS
'Altera��o para processar casos do Conta-Cart�o
'Sub Obtem_Movimentacao_Financeira(ByVal sPropostaId As String, _
'                                  ByVal sEndossoId As String, _
'                                  ByRef sMovimentacaoId As String)

Dim sSQL As String
Dim rsMov As rdoResultset

  On Error GoTo TrataErro


If sCod_empresa = "AB" Then
  'Selecionando a movimenta��o financeira de restitui��o
  sSQL = ""
  adSQL sSQL, "SELECT ps_mov_endosso_financeiro_tb.movimentacao_id"
  adSQL sSQL, "  FROM ps_mov_endosso_financeiro_tb  WITH (NOLOCK)  "
  adSQL sSQL, " INNER JOIN endosso_financeiro_tb  WITH (NOLOCK)  "
  adSQL sSQL, "    ON ps_mov_endosso_financeiro_tb.proposta_id = endosso_financeiro_tb.proposta_id"
  adSQL sSQL, "   AND ps_mov_endosso_financeiro_tb.endosso_id = endosso_financeiro_tb.endosso_id"
  adSQL sSQL, " INNER JOIN endosso_tb  WITH (NOLOCK)  "
  adSQL sSQL, "    ON ps_mov_endosso_financeiro_tb.proposta_id = endosso_tb.proposta_id"
  adSQL sSQL, "   AND ps_mov_endosso_financeiro_tb.endosso_id = endosso_tb.endosso_id"
  ' fsa - 21/09/2009 - Incluido para possibilitar buscar campo gera_movimentacao_restituicao
  adSQL sSQL, "  JOIN tp_endosso_tb  WITH (NOLOCK) "
  adSQL sSQL, "    ON tp_endosso_tb.tp_endosso_id = endosso_tb.tp_endosso_id"
  adSQL sSQL, " WHERE ps_mov_endosso_financeiro_tb.proposta_id = " & sPropostaId
  adSQL sSQL, "   AND ISNULL(ps_mov_endosso_financeiro_tb.endosso_id, 0) = " & sEndossoId
  adSQL sSQL, "   AND ISNULL(endosso_financeiro_tb.val_financeiro, 0) < 0"
  'Jorfilho 24-abr-2003: Por enquanto, os tipos de endosso ser�o definidos porque
  'o processo de restitui��o autom�tica s� est� habilitado para estes casos
  'arodrigues - 26/06/2006 - Inclus�o do tp_endosso 101
  
  'fsa - 21/09/2009 - Criado campo gera_movimentacao_restituicao em tp_endosso_tb para identicar endosso de restituicao
  'adSQL sSQL, "   AND endosso_tb.tp_endosso_id IN (63, 70, 80, 101, 104, 64, 269)"
  adSQL sSQL, "   AND tp_endosso_tb.gera_movimentacao_restituicao = 'S'"

  Set rsMov = rdocn.OpenResultset(sSQL)

  If Not rsMov.EOF Then
    
    sMovimentacaoId = rsMov("movimentacao_id")
    sEndossoResumoId = "0"
  
  Else
      'Barney - 22/04/2004
      'Selecionando a movimenta��o financeira de restitui��o do Conta-Cart�o
      rsMov.Close
      
      sSQL = ""
      adSQL sSQL, " SELECT MAX(ps_mov_endosso_financeiro_tb.movimentacao_id) movimentacao_id, endosso_resumo_tb.endosso_resumo_id"
      adSQL sSQL, " FROM endosso_tb  WITH (NOLOCK) "
      adSQL sSQL, " JOIN endosso_resumo_tb  WITH (NOLOCK)  "
      adSQL sSQL, "   on endosso_resumo_tb.proposta_id = endosso_tb.proposta_id "
      adSQL sSQL, "  and endosso_resumo_tb.endosso_id = endosso_tb.endosso_id"
      adSQL sSQL, " JOIN endosso_financeiro_tb  WITH (NOLOCK)  "
      'Barney - 30/07/2004 - Inclus�o da chave proposta_id
      'adSQL sSQL, "   on endosso_resumo_tb.endosso_resumo_id = endosso_financeiro_tb.endosso_resumo_id"
      adSQL sSQL, "   on endosso_financeiro_tb.proposta_id = endosso_tb.proposta_id"
      adSQL sSQL, "  and endosso_financeiro_tb.endosso_resumo_id = endosso_resumo_tb.endosso_resumo_id "
      adSQL sSQL, " JOIN ps_mov_endosso_financeiro_tb  WITH (NOLOCK)  "
      adSQL sSQL, "   on ps_mov_endosso_financeiro_tb.proposta_id = endosso_financeiro_tb.proposta_id"
      adSQL sSQL, "  and ps_mov_endosso_financeiro_tb.endosso_id = endosso_financeiro_tb.endosso_id "
      adSQL sSQL, " WHERE endosso_tb.proposta_id = " & sPropostaId
      adSQL sSQL, "   AND ISNULL(endosso_tb.endosso_id, 0) = " & sEndossoId
      If (Mid(wk_seg_remessa_arq_retorno_bb, 1, 6) = "SEG452" And (ProdutoProposta = "1125" Or ProdutoProposta = "1141")) Then 'INCIDENTE :SD00005455 - ANDERSON.SANTOS
          adSQL sSQL, "   AND ISNULL(endosso_resumo_tb.endosso_resumo_id, 0) = " & sEndossoId
      Else
          adSQL sSQL, "   AND ISNULL(endosso_tb.endosso_id, 0) = " & sEndossoId
      End If
          adSQL sSQL, "   AND ISNULL(endosso_financeiro_tb.val_financeiro, 0) < 0      "
      adSQL sSQL, " GROUP BY endosso_resumo_tb.endosso_resumo_id      "
    
      Set rsMov = rdocn.OpenResultset(sSQL)
      
      If Not rsMov.EOF Then
          sMovimentacaoId = rsMov("movimentacao_id")
          sEndossoResumoId = rsMov("endosso_resumo_id")
      Else
          sMovimentacaoId = "0"
          sEndossoResumoId = "0"
      End If
  End If

  rsMov.Close
  Set rsMov = Nothing
  
  Exit Sub
End If

'Gmarques
If sCod_empresa = "ABS" Then
  'Selecionando a movimenta��o financeira de restitui��o
  sSQL = ""
  adSQL sSQL, "SELECT ps_mov_endosso_financeiro_tb.movimentacao_id"
  adSQL sSQL, "  FROM ps_mov_endosso_financeiro_tb  WITH (NOLOCK)  "
  adSQL sSQL, " INNER JOIN endosso_financeiro_tb  WITH (NOLOCK)  "
  adSQL sSQL, "    ON ps_mov_endosso_financeiro_tb.proposta_id = endosso_financeiro_tb.proposta_id"
  adSQL sSQL, "   AND ps_mov_endosso_financeiro_tb.endosso_id = endosso_financeiro_tb.endosso_id"
  adSQL sSQL, " INNER JOIN endosso_tb  WITH (NOLOCK)  "
  adSQL sSQL, "    ON ps_mov_endosso_financeiro_tb.proposta_id = endosso_tb.proposta_id"
  adSQL sSQL, "   AND ps_mov_endosso_financeiro_tb.endosso_id = endosso_tb.endosso_id"
  ' fsa - 21/09/2009 - Incluido para possibilitar buscar campo gera_movimentacao_restituicao
  adSQL sSQL, "  JOIN tp_endosso_tb  WITH (NOLOCK) "
  adSQL sSQL, "    ON tp_endosso_tb.tp_endosso_id = endosso_tb.tp_endosso_id"
  adSQL sSQL, " WHERE ps_mov_endosso_financeiro_tb.proposta_id = " & sPropostaId
  adSQL sSQL, "   AND ISNULL(ps_mov_endosso_financeiro_tb.endosso_id, 0) = " & sEndossoId
  adSQL sSQL, "   AND ISNULL(endosso_financeiro_tb.val_financeiro, 0) < 0"
  'Jorfilho 24-abr-2003: Por enquanto, os tipos de endosso ser�o definidos porque
  'o processo de restitui��o autom�tica s� est� habilitado para estes casos
  'arodrigues - 26/06/2006 - Inclus�o do tp_endosso 101
  
  'fsa - 21/09/2009 - Criado campo gera_movimentacao_restituicao em tp_endosso_tb para identicar endosso de restituicao
  'adSQL sSQL, "   AND endosso_tb.tp_endosso_id IN (63, 70, 80, 101, 104, 64, 269)"
  adSQL sSQL, "   AND tp_endosso_tb.gera_movimentacao_restituicao = 'S'"

  Set rsMov = rdocn_Seg2.OpenResultset(sSQL)

  If Not rsMov.EOF Then
    
    sMovimentacaoId = rsMov("movimentacao_id")
    sEndossoResumoId = "0"
  
  Else
      'Barney - 22/04/2004
      'Selecionando a movimenta��o financeira de restitui��o do Conta-Cart�o
      rsMov.Close
      
      sSQL = ""
      adSQL sSQL, " SELECT MAX(ps_mov_endosso_financeiro_tb.movimentacao_id) movimentacao_id, endosso_resumo_tb.endosso_resumo_id"
      adSQL sSQL, " FROM endosso_tb  WITH (NOLOCK)  "
      adSQL sSQL, " JOIN endosso_resumo_tb  WITH (NOLOCK)  "
      adSQL sSQL, "   on endosso_resumo_tb.proposta_id = endosso_tb.proposta_id "
      adSQL sSQL, "  and endosso_resumo_tb.endosso_id = endosso_tb.endosso_id"
      adSQL sSQL, " JOIN endosso_financeiro_tb  WITH (NOLOCK)  "
      'Barney - 30/07/2004 - Inclus�o da chave proposta_id
      'adSQL sSQL, "   on endosso_resumo_tb.endosso_resumo_id = endosso_financeiro_tb.endosso_resumo_id"
      adSQL sSQL, "   on endosso_financeiro_tb.proposta_id = endosso_tb.proposta_id"
      adSQL sSQL, "  and endosso_financeiro_tb.endosso_resumo_id = endosso_resumo_tb.endosso_resumo_id "
      adSQL sSQL, " JOIN ps_mov_endosso_financeiro_tb  WITH (NOLOCK)  "
      adSQL sSQL, "   on ps_mov_endosso_financeiro_tb.proposta_id = endosso_financeiro_tb.proposta_id"
      adSQL sSQL, "  and ps_mov_endosso_financeiro_tb.endosso_id = endosso_financeiro_tb.endosso_id "
      adSQL sSQL, " WHERE endosso_tb.proposta_id = " & sPropostaId
      adSQL sSQL, "   AND ISNULL(endosso_tb.endosso_id, 0) = " & sEndossoId
      adSQL sSQL, "   AND ISNULL(endosso_financeiro_tb.val_financeiro, 0) < 0      "
      adSQL sSQL, " GROUP BY endosso_resumo_tb.endosso_resumo_id      "
    
      Set rsMov = rdocn_Seg2.OpenResultset(sSQL)
      
      If Not rsMov.EOF Then
          sMovimentacaoId = rsMov("movimentacao_id")
          sEndossoResumoId = rsMov("endosso_resumo_id")
      Else
          sMovimentacaoId = "0"
          sEndossoResumoId = "0"
      End If
  End If

  rsMov.Close
  Set rsMov = Nothing
  Exit Sub
End If

Exit Sub

TrataErro:
  Call TrataErroGeral("Obtem_Movimentacao_Financeira", Me.name)
  Call TerminaSEGBR

End Sub

Sub Obtem_Produto(ByVal PCodProdutoBB, ByRef PListaProdutos)

Dim SQL As String
Dim Rst As rdoResultset

On Error GoTo TrataErro

Select Case PCodProdutoBB

    Case "0678102"
        PListaProdutos = "(11)"
    Case "0549107"
        PListaProdutos = "(15)"
    Case "0678145"
        PListaProdutos = "(145)"
    Case "0678135"
        PListaProdutos = "(16)"
    Case "0678126", "0678116"
        PListaProdutos = "(14)"
    Case "0678155"
        PListaProdutos = "(155)"
    Case "0678109"
        'PListaProdutos = "(10,109)"
        'PListaProdutos = "(109)"
        'Alterado por Alexandre Debouch - ConfitecSP - 16/06/2010
        'Altera��o solicitada Por Fabio - Alian�a
        PListaProdutos = "(109)"
    Case "0678110"
        'PListaProdutos = "(9,111)"
        PListaProdutos = "(111)"
    Case "0678113"
        PListaProdutos = "(103,400,423)"
    Case "8622011"
        PListaProdutos = "(1125,1141)"
    'Tratamento dos arquivos Visanet - Demanda 654224 - Rog�rio - 17.07.2009
    Case "0000544"
        PListaProdutos = "(109,14)" 'Inclus�o produto 14 - MU-2018-057488 - Cartao de Credito Rural - gustavo.cruz 28.09.208
    Case "0000549"
        PListaProdutos = "(109,14)" 'Inclus�o produto 14 - MU-2018-057488 - Cartao de Credito Rural - gustavo.cruz 04.10.2018
    '----------------------------------------------------------------------------------------
    ' Tratamento do arquivo SEGF987
    '----------------------------------------------------------------------------------------
    Case "0000516"
        'PListaProdutos = "(9, 10)"
        'Rog�rio
        'PListaProdutos = "(109, 111, 811, 113, 809)"   'Fabio incluindo produto 811 produto empresarial personalizado 29/03/2010
                                                  'Fabio incluindo produto 113 produto empresarial personalizado 30/03/2010
    '----------------------------------------------------------------------------------------
        'Niuarque Rosa - 06/02/2012
        'Adicionando os produtos ao filtro
        '100 SEGUR BRASIL
        '103 BB - SEGURO TRANSPORTE INTERNACIONAL
        '810 CONDOM�NIO PERSONALIZADO
        '777 RD EQUIPAMENTOS
        '112 SEGUR BRASIL - CONDOM�NIO
        PListaProdutos = "(100, 103, 109, 111, 112, 113, 777, 810, 811, 809)"
    Case Else

        SQL = "Select distinct"
        SQL = SQL & "     b.Produto_Id, "
        SQL = SQL & "     b.produto_anterior_id "
        SQL = SQL & "From "
        SQL = SQL & "     arquivo_produto_tb a   WITH (NOLOCK)  "
        SQL = SQL & "     Inner Join produto_tb b   WITH (NOLOCK)  "
        SQL = SQL & "           on a.produto_Id = b.produto_Id "
        SQL = SQL & "Where"
        If PCodProdutoBB <> "8229150" Then
           SQL = SQL & "     a.produto_externo_id = " & Right(PCodProdutoBB, 3)
        Else
           SQL = SQL & "     a.arquivo in ('seg493', 'segf495', 'segf496', 'SEG601', 'SEG603') "
        End If
        SQL = SQL & "Order by "
        SQL = SQL & "     b.produto_id, b.produto_anterior_id "
       
        Set Rst = rdocn.OpenResultset(SQL)
        
        If Not Rst.EOF Then
        
            'Colhendo primeiro produto''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            
            PListaProdutos = "("
            PListaProdutos = PListaProdutos & Rst!produto_id
            If IsNull(Rst!Produto_anterior_Id) = False Then
                PListaProdutos = PListaProdutos & "," & Rst!Produto_anterior_Id
            End If
            
            'Colhendo demais produto caso existam'''''''''''''''''''''''''''''''''''''''''''''''''
            
            Rst.MoveNext
            
            Do While Not Rst.EOF
               
               If InStr(Lista_Produtos, Rst!produto_id) = 0 Then
                  PListaProdutos = PListaProdutos & "," & Rst!produto_id
               End If
               
               If IsNull(Rst!Produto_anterior_Id) = False Then
                  If InStr(Lista_Produtos, Rst!Produto_anterior_Id) = 0 Then
                     PListaProdutos = PListaProdutos & "," & Rst!Produto_anterior_Id
                  End If
               End If
               
               Rst.MoveNext
               
            Loop
            
            PListaProdutos = PListaProdutos & ")"
            
        Else
            MensagemBatch "Produto da proposta do endosso n�o encontrado."
            TerminaSEGBR
        End If
    
End Select
Exit Sub
   
TrataErro:
    TrataErroGeral "Obtem_Produto", Me.name
    TerminaSEGBR

End Sub

Function Valida_Existencia_Documento(ByVal PPropostaId As String, _
                                     ByVal PEndossoId As String, _
                                     ByVal Pcod_empresa As String) As Boolean

On Error GoTo TrataErro

Valida_Existencia_Documento = False

If Trim(PPropostaId) = "" Then
   Exit Function
End If

If Val(PEndossoId) > 0 Then
    If Verifica_Existencia_Endosso(PPropostaId, PEndossoId, Pcod_empresa) = False Then
       Exit Function
    End If
End If

Valida_Existencia_Documento = True

Exit Function

TrataErro:
    TrataErroGeral "Obtem_Produto", Me.name
    TerminaSEGBR

End Function



Function Verifica_Cadastro_Atributo_BB(PCodAtributo, Pcod_empresa) As Boolean

Dim Rst As rdoResultset
Dim SQL As String

On Error GoTo TrataErro
'# Obtendo a proposta a partir da proposta ades�o'''''''''''''''''''''''''''''''''''''''''''
    
SQL = "SELECT "
SQL = SQL & "   * "
SQL = SQL & "FROM "
SQL = SQL & "   atributo_bb_tb   WITH (NOLOCK)  "
SQL = SQL & "WHERE"
SQL = SQL & "   atributo_id = " & PCodAtributo
    
    
If Pcod_empresa = "AB" Then
Set Rst = rdocn.OpenResultset(SQL)
End If
   
If Pcod_empresa = "ABS" Then
Set Rst = rdocn_Seg2.OpenResultset(SQL)
End If
   
If Not Rst.EOF Then
    Verifica_Cadastro_Atributo_BB = True
Else
    Verifica_Cadastro_Atributo_BB = False
End If

Rst.Close
Exit Function
   
TrataErro:
    TrataErroGeral "Verifica_Cadastro_Atributo_BB", Me.name
    TerminaSEGBR
End Function

Function Verifica_Cadastro_Tp_Recusa_BB(PTpRecusa, Pcod_empresa) As Boolean

Dim Rst As rdoResultset
Dim SQL As String

'# Obtendo a proposta a partir da proposta ades�o'''''''''''''''''''''''''''''''''''''''''''
On Error GoTo TrataErro

SQL = "SELECT "
SQL = SQL & "   * "
SQL = SQL & "FROM "
SQL = SQL & "   tp_recusa_retorno_bb_tb   WITH (NOLOCK)  "
SQL = SQL & "WHERE"
SQL = SQL & "   tp_recusa_bb_id = " & PTpRecusa
    
If Pcod_empresa = "AB" Then
    Set Rst = rdocn.OpenResultset(SQL)
End If
   
If Pcod_empresa = "ABS" Then
    Set Rst = rdocn_Seg2.OpenResultset(SQL)
End If
   
If Not Rst.EOF Then
    Verifica_Cadastro_Tp_Recusa_BB = True
Else
    Verifica_Cadastro_Tp_Recusa_BB = False
End If

Rst.Close
Exit Function
   
TrataErro:
    TrataErroGeral "Verifica_Cadastro_Tp_Recusa_BB", Me.name
    TerminaSEGBR
End Function

Private Sub Form_Load()
   
   On Error GoTo TrataErro
   'Verifica se foi executado pelo Control-M
    CTM = Verifica_Origem_ControlM(Command)
   
   'glAmbiente_id = 3
   'Data_Sistema = Now
   cUserName = "NOVA"
   
   'Conectando com a base de dados''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
   
   Conexao
   'Necessario quando for executado pelo Control-M
   
   If CTM = True Then
    gsCPF = "12345678909"
    glEmpresa_id = "31"
   End If

   Call Seguranca("SEGP0137", "Importa��o de Avalia��es de Retorno BB")
   
   Call Conexao_Auxiliar
   
   'Obtendo Paths'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
   
   logpath = LerArquivoIni("producao", "log_path")
   arqpath = LerArquivoIni("producao", "producao_path")
   arqpath = arqpath & "..\Arquivos Avaliacao Retorno BB\Recebidos"
   arqpath = "\\10.0.35.104\teste_mls\Arquivos Avaliacao Retorno BB\Recebidos"
   
   'arqpath = arqpath & "\\sisab005\users\Temporario\vapinto"
   'arqpath = "\\sisab007\users\Temporario\valquiria.pinto"
   
   'claudia.silva
   'arqpath = "C:\Temp\Recebidos"
   'logpath = "C:\Temp"
   
   'Inicializando Vari�veis'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
   flag_importacao = False
   obs = Space(50)
   
   'Atualizando interface'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
   'arqpath = App.PATH
   fldpath.Text = arqpath
   lstarquivo.PATH = arqpath
   lstarquivo.Pattern = "*"
   lstarquivo.Refresh
   
   Preenche_Grid
      
   Me.Caption = "SEG00137 - Importa Avaliacao Retorno BB - " & Ambiente
   
   'Inciando processo autom�ticamente caso seja uma chamada do scheduler''''''''''''''''''''''
   If Obtem_agenda_diaria_id(Command) > 0 Or Verifica_Origem_ControlM(Command) = True Then
   
           If lstarquivo.ListCount = 0 Then ' INSERINDO SA�DA DO PROGRAMA CASO N�O HAJA ARQUIVO PARA SER PROCESSADO - JORGE MUNIZ 08/01/2016
                   
                Call GravaLogCTM("OK - ", "N�O EXISTE(M) ARQUIVO(S) PARA SER(EM) PROCESSADO(S).", lstarquivo.List(i), "")
                                
                                '' LGOMES - 14-06-2019 - Garantindo execu��o da apura��o do 987 independente da captura.
                                Call GravaLogCTM("OK - ", Time & " - Atualizando Acompanhamento 987 AB", i, "")
                                        Insere_Acompanhamento_987_AB
                                Call GravaLogCTM("OK - ", Time & " - Atualizando Acompanhamento 987 ABS", i, "")
                                        Insere_Acompanhamento_987_ABS
                                Call GravaLogCTM("OK - ", Time & " - Finalizando Acompanhamento 987 AB e ABS", i, "")
                                
                                TerminaSEGBR
           End If
   
      Me.Show
      Call BtnOk_Click
      ' Francisco (Stefanini), em 11/10/2005 - Padroniza��o do processo de finaliza��o
      ' Call FinalizarAplicacao
      ' Maur�cio(Stefanini), em 07/10/2005 - Processamento com sucesso deve finalizar o programa ap�s o t�rmino.
      Unload Me
      Call TerminaSEGBR
   End If
Exit Sub
   
TrataErro:
    TrataErroGeral "Form_Load", Me.name
    TerminaSEGBR
End Sub

Sub Conexao_Auxiliar()

On Error GoTo Erro

With rdocn1
    .Connect = rdocn.Connect
    .CursorDriver = rdUseServer
    .QueryTimeout = 30000 '' 50 minutos
    .EstablishConnection rdDriverNoPrompt
End With
    
Exit Sub

Erro:
    'mensagem_erro 6, "Conex�o com BRCAPDB indispon�vel."
    Call MensagemBatch("Conex�o com BRCAPDB indispon�vel.")
    'End
    TerminaSEGBR
    
End Sub

Private Sub BtnOk_Click()

On Error GoTo TrataErro

'Incializando parametros de execu��o batch''''''''''''''''''''''''''''''''''''''''''''

'InicializaParametrosExecucaoBatch Me
    
'Caso seja uma chamada do scheduler, ser�o importados todos os arquivos do produto
'agendado'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

MousePointer = 11

'Maur�cio(Stefanini), em 07/10/2005
flag_todosprocessados = True

If goProducao.Automatico = True Or CTM = True Then

    For i = 0 To lstarquivo.ListCount - 1
        goProducao.AdicionaLog 1, lstarquivo.List(i), i + 1
        'grava log arquivo CTM
       ''''' INICIO -RCA380 - RF00055767 -  FLAVIO ABREU/JARBAS LIMA - 26-02-2018 - RETIRANDO TRECHO POIS O ARQUIVO SER� PROCESSADO POR UM NOVO JOB - SEGX2118
           If Not (Trim(Left(lstarquivo.List(i), 7)) = "SEG987E" Or _
                   Trim(Left(lstarquivo.List(i), 7)) = "SEG987P" Or _
                   Trim(Left(lstarquivo.List(i), 7)) = "SEG987K" Or _
                   Trim(Left(lstarquivo.List(i), 7)) = "SEG987Q" Or _
                   Trim(Left(lstarquivo.List(i), 7)) = "SEG987T" Or _
                   Trim(Left(lstarquivo.List(i), 7)) = "ALS987A" Or _
                   Trim(Left(lstarquivo.List(i), 7)) = "ALS987B" Or _
                   Trim(Left(lstarquivo.List(i), 7)) = "BBC987A" Or _
                   Trim(Left(lstarquivo.List(i), 7)) = "BBC987B" Or _
                   Trim(Left(lstarquivo.List(i), 8)) = "SEG987AB" Or _
                   Trim(Left(lstarquivo.List(i), 8)) = "SEG987AC" Or _
                   Trim(Left(lstarquivo.List(i), 7)) = "BBC987C" Or _
                   Trim(Left(lstarquivo.List(i), 7)) = "SEG987F" Or _
                   Trim(Left(lstarquivo.List(i), 7)) = "SEG987I" Or _
                   Trim(Left(lstarquivo.List(i), 7)) = "SEG987O" Or _
                   Trim(Left(lstarquivo.List(i), 7)) = "SEGE987" Or _
                   Trim(Left(lstarquivo.List(i), 7)) = "SEGF987" Or _
                   Trim(Left(lstarquivo.List(i), 7)) = "SEGG987") Then
         
                lstarquivo.ListIndex = i

          If CTM = True Then
             Call GravaLogCTM("OK - ", "", lstarquivo.List(i), i + 1)
          End If
        
          If lblstatus.Caption = "N�o processado" Then
            Inicia
          End If
       End If
    Next

    'Inibido, a fun��o est� sendo alterada de lugar para executar ap�s todos arquivos terem sido processados. FBEZERRA 04/01/2017
    If i >= 0 Then
            '' LGOMES - 04-02-2019 - Substituindo rotina �nica de apura��o do 987 por duas independentes.
            '' Insere_Acompanhamento_987
                Call GravaLogCTM("OK - ", Time & " - Atualizando Acompanhamento 987 AB", i, "")
        Insere_Acompanhamento_987_AB
                Call GravaLogCTM("OK - ", Time & " - Atualizando Acompanhamento 987 ABS", i, "")
                Insere_Acompanhamento_987_ABS
                Call GravaLogCTM("OK - ", Time & " - Finalizando Acompanhamento 987 AB e ABS", i, "")
    End If

Else
    goProducao.AdicionaLog 1, lstarquivo.List(lstarquivo.ListIndex), 1
    
        If CTM = True Then
            Call GravaLogCTM("OK - ", "", "", lstarquivo.List(lstarquivo.ListIndex))
        End If
    
    Inicia
End If

'Finalizando agenda''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''



'Pula de for executado pelo Control-M
If CTM = False Then
    Call goProducao.finaliza
End If

                 
MousePointer = 0

'Maur�cio(Stefanini), em 07/10/2005 - Se algum arquivo n�o processar corretamente,
'programa grava processamento com erro.
If flag_todosprocessados = False Then
    Call Err.Raise(44444, "BtnOK_Click", "Existem arquivos processados com erro na pasta pendentes")
End If

'Confitec Sistemas - Diogo Ferreira 17/01/2012 - Demanda 13981228: Inclus�o de mensagem de erro no programa para recusas do conta cart�o
If bRecusaContaCartaoTarefa = True Then
    Call Err.Raise(44444, "BtnOK_Click", "Existem arquivos recusados do conta cart�o")
End If

Exit Sub

TrataErro:
    TrataErroGeral "BtnOK_Click", Me.name
    Call TerminaSEGBR
    
End Sub

Private Sub btnCancelar_Click()
    TerminaSEGBR
End Sub

Sub Inicia()
   
   Dim FileName As String
   ' Jarbas - inclus�o das vari�veis para valida��o da remssa - RF00055767 - RCA380 - Otimiza��o da Importa��o de Arquivos de Retorno BB - 987 - Fase II 20/02/2018
   Dim Rst As rdoResultset
   Dim SQL As String
   Dim Remessa_Bloqueada As String
   ''''''''''''''''''''''''''''''''''''
   
   On Error GoTo TrataErro
   
   obs = Space(50)
   
   'Verificando se algum arquivo foi selecionado''''''''''''''''''''''''''''''''''''''''''
   
   If lstarquivo.ListIndex = -1 Then
      MensagemBatch "Selecione o arquivo de retorno", , , False
      If goProducao.Automatico Or CTM = True Then
         TerminaSEGBR
      Else
         Exit Sub
      End If
   End If
      
   dat = Date
   hor = Time
   
   'Atualizando interface'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    
   BtnOk.Enabled = False
   lstarquivo.Enabled = False

'Processando informa��es''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

' Inicio - Jarbas - valida��o da remessa - RF00055767 - RCA380 - Otimiza��o da Importa��o de Arquivos de Retorno BB - 987 - Fase II 20/02/2018 '''''''''''''''''''''''''''''''''''''
SQL = ""
SQL = "select remessa_bb from seguros_db.dbo.remessa_retorno_bb_tb where ativo = 's' and remessa_bb in ('" & Trim(Left(lstarquivo.FileName, 7)) & "')"

Set Rst = rdocn.OpenResultset(SQL)

If Not Rst.EOF Then
   Remessa_Bloqueada = Rst!remessa_bb
End If
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    
'  If (Trim(Left(lstarquivo.FileName, 8)) = "SEG987AC" Or _
'           Trim(Left(lstarquivo.FileName, 7)) = "BBC987B" Or _
'           Trim(Left(lstarquivo.FileName, 7)) = "ALS987B") Then
      
'        Call Importa_Avaliacoes_Retorno_Cartao_credito_BB
                         
'  '' LCBG - 27/03/2001
'  ElseIf (Trim(Left(lstarquivo.FileName, 7)) = "ALS987A" Or _
'        Trim(Left(lstarquivo.FileName, 7)) = "BBC987A" Or _
'        Trim(Left(lstarquivo.FileName, 8)) = "SEG987AB") Then
'        Call Importa_Avaliacoes_Retorno_544
'  '' FIM LCBG
  
'  'Confitec Sistemas - Luiz Gabriel Lopes 10/02/2012 - Demanda 12240504 - TRATAMENTO RECUSA DE COBRAN�A - FATURAMENTO CDC EX�RCITO
  'ElseIf (Trim(Left(lstarquivo.FileName, 7)) = "ALSF159") Then
  If (Trim(Left(lstarquivo.FileName, 7)) = "ALSF159") Then

        Call Importa_Avaliacoes_Retorno_ALSF159_BB

  '''''' Incio - Jarbas Lima - RF00055767 - RCA380 - incluindo condi��o para n�o processar a Remessa SEG987E, a mesma ser� executada pela SEGX2118
  'ElseIf (Trim(Left(lstarquivo.FileName, 7)) = "SEG987E") Then
  ElseIf (Trim(Left(lstarquivo.FileName, 7)) = Remessa_Bloqueada) Then
  ''''' Fim
                
                
  'Confitec Sistemas - Flavio Abreu 07/03/2014 - Inclus�o dos arquivos: SEG987SS e SEG987SSS.
  'PAULO PELEGRINI - 27/06/2016 - 19306878 - Incus�o do arquivo: SEG987X
  ElseIf Not (Trim(Left(lstarquivo.FileName, 7)) = "SEG987Z" Or _
              Trim(Left(lstarquivo.FileName, 7)) = "SEG987S" Or _
              Trim(Left(lstarquivo.FileName, 7)) = "SEG987SS" Or _
              Trim(Left(lstarquivo.FileName, 7)) = "SEG987SSS" Or _
              Trim(Left(lstarquivo.FileName, 8)) = "SEG987AA" Or _
              Trim(Left(lstarquivo.FileName, 7)) = "SEG987X") Then
              
        Call Importa_Avaliacoes_Retorno_BB

  End If
     'Mudado para trabalhar com instr rodrigo.moura / sidney.mendes 03/09/2013
    'Alterado inicio rodrigo.moura / sidney.mendes 03/09/2013
    'Select Case Replace(Trim(Left(lstarquivo.FileName, 8)), ".", "")
    Select Case Trim$(Left$(lstarquivo.FileName, ((InStr(1, lstarquivo.FileName, ".")) - 1))) ' Nome do arquivo sem o ponto "."
    'Alterado fim rodrigo.moura / sidney.mendes 03/09/2013
       Case "SEG987Z"
                Importa_Avaliacoes_Retorno_SEG987ZAA_BB
                
        'Alterado inicio rodrigo.moura 03/09/2013
        'Case "SEG987S"
        Case "SEG987S", "SEG987SS", "SEG987SSS"
                Importa_Avaliacoes_Retorno_SEG987S_BB
        'Alterado Fim rodrigo.moura 03/09/2013
        
        Case "SEG987AA"
                Importa_Avaliacoes_Retorno_SEG987ZAA_BB
                        
        'PAULO PELEGRINI - 27/06/2016 - 19306878 - Incus�o do arquivo: SEG987X (INI)
        Case "SEG987X"
                Importa_Avaliacoes_Retorno_SEG987X
        'PAULO PELEGRINI - 27/06/2016 - 19306878 - Incus�o do arquivo: SEG987X (FIM)
        
    End Select
    

  
      
    'PMELO - Carga da tabela auxiliar para gera��o da planilha de acompanhamento do 987 contemplando AB e ABS
    'Insere_Acompanhamento_987 - Inibido, a fun��o est� sendo alterada de lugar para executar ap�s todos arquivos terem sido processados. FBEZERRA 04/01/2017
  
   
   
   'Atualizando interface'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
   
   StatusBar1.SimpleText = "Tarefa de importa��o finalizada!"
   StatusBar1.Refresh
   
   Exit Sub

TrataErro:
    TrataErroGeral "Inicia", Me.name
    Call TerminaSEGBR
    
End Sub


Sub Importa_Avaliacoes_Retorno_BB()
   
   'Declara��es'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
   Const TpPropostaAdesao = 1
   Const TpPropostaFechada = 2
      
   Dim Header As String
   Dim position As Long
   Dim Trailler As String
   Dim SQL As String
   Dim Rst As rdoResultset
   Dim Remessa As String
   Dim Sequencial As String
   
   Dim TotRegsAvaliacao_Detalhe As Long
   Dim TotRegsAvaliacao_Trailler As Long
                 
   Dim DtProcessamento As String
   Dim RemessaArquivoBB As String
   Dim CodProdutoBB As String
   Dim ListaProdutos As String
   
   Dim PropostaId As String
   Dim EndossoId As String
    
   Dim TpRegistro As String
   Dim CodigoCampo As String
   Dim DescricaoCampo As String
   Dim CodigoRecusa As String
   Dim DescricaoRecusa As String
   Dim SequencialArquivoSeguradora As String
   Dim CodigoRetorno As String
   Dim AceiteBB As String
   Dim propostaBB As String
   Dim ApoliceID As String
   Dim endossoBB As String
   Dim TpEndossoBB As String
   Dim InformacaoNaBaseBB As String
   Dim InformacaoRetornada As String
   Dim SeqAvaliacaoRetornoBBId As String
   Dim SituacaoLiberacaoImpressao As String
   Dim RetornoReincidente As String '***
   
   Dim FlagTpProposta As String
   Dim FlagExisteDocumento As Boolean
   Dim FlagMotivoRecusaInfomado As Boolean
   Dim FlagLerProximoRegistro As Boolean
   
   Dim LinhaInconsistencia As String
     
   Dim AutorizacaoPgtoRestituicao As Boolean
   Dim ProdutoProcessaRestituicaoAutomatica As Boolean
   Dim MovimentacaoFinancId As String
   
   'Barney - 22/04/2004 - Conta-Cart�o
   Dim EndossoResumoId As String
   
   Dim rc_aux As rdoResultset
   Dim sSQL As String
   Dim vArquivo As Variant
   Dim bVerificaArq As Boolean
   
   Dim ProdutoProposta As String
   Dim Arquivo_invalido As String
   
   Dim Cod_empresa As String 'Gmarques 16/09/2010
   Dim vlngContador As Long
    
   Dim val_restituicao, motivo_recusa As String
   Dim exec_extrato As Integer
    
On Error GoTo TrataErro
   
   'Abrindo o Arquivo de avalia��es de retorno BB '''''''''''''''''''''''''''''''''''''''''
   arqinput = FreeFile
   Open lstarquivo.PATH & "\" & lstarquivo.FileName For Input As arqinput
   tam_arquivo = LOF(arqinput)
   
   'Configuando interface (Progress Bar)'''''''''''''''''''''''''''''''''''''''''''''''''''
   term1.Min = 0
   term1.Max = tam_arquivo
   term1.Visible = True
   
   'Iniciando transa��o''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
   rdocn.BeginTrans
   rdocn_Seg2.BeginTrans
   
   'Inicializando vari�veis''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
   FlagLerProximoRegistro = True
       
   'Iniciando Leitura dos Registros do Arquivo'''''''''''''''''''''''''''''''''''''''''''''
   vlngContador = 0
   Do
       DoEvents
       vlngContador = vlngContador + 1
       'Inicializando variaveis''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
       SeqAvaliacaoRetornoBBId = ""
       MovimentacaoFinancId = ""
       ProdutoProposta = ""
       
       'Lendo registro'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
       If FlagLerProximoRegistro = True Then
          Line Input #arqinput, Reg
          position = position + 1
          'Atualizando interface''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
          tam = tam + Len(Reg)
          term1.Value = tam
          'Processando registro'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
          Sequencial = Mid(Reg, 1, 6)
          Arquivo_invalido = Mid(Reg, 12, 2)
       End If
       
       If Arquivo_invalido = "08" And Sequencial = "000000" And Trim(Left(lstarquivo.FileName, 7)) = "SEG987T" Then
            Exit Do
       End If
       'Inicializando vari�veis''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
       FlagLerProximoRegistro = True
     
       If Sequencial = "000000" Then                                  'Registro Header

           'Lendo dados do Header
           CodProdutoBB = Mid(Reg, 17, 7)
           
           'ALTERA��O MARCO 31/03/2003
           'RemessaArquivoBB = Mid(Reg, 30, 4)
           RemessaArquivoBB = Mid(Reg, 34, 4)
           DtProcessamento = Mid(Reg, 39, 2) & "/" & Mid(Reg, 42, 2) & "/" & Mid(Reg, 45, 4)
           lblarq.Caption = lstarquivo.FileName
           'tratamento para formatar arquivos que possuem 8 digitos e 7 digitos - ecosta 18/09/08
           StrArquivo = Left(lblarq, 8)
           StrArquivo = Replace(StrArquivo, ".", "")
           
          'wk_seg_remessa_arq = Left(lblarq, 7) & "." & Format(Val(Mid(Reg, 30, 4)), "0000") '* jmendes - 15/04/2002
    'comando acima substituido pelo comando abaixo - ecosta 18/09/08
           wk_seg_remessa_arq = StrArquivo & "." & Format(Val(Mid(Reg, 30, 4)), "0000") '* jmendes - 15/04/2002
           'Tratamento 987 - Arquivo SEG987N (Controle de vers�o Retorno BB Reinicializado) - Flavio Abreu - Pm00000203 - 2018-12-05
           If (StrArquivo = "SEG987N") Then
             wk_seg_remessa_arq = StrArquivo & "." & "1" & Format(Val(Mid(Reg, 30, 4)), "0000")
           End If
                   
           tipo_arquivo = Val(Mid(Reg, 13, 3))
           ano_mes_ref = Mid(Reg, 45, 4) & Mid(Reg, 42, 2)
           
        '* De acordo com a tabela DE/PARA fornecida anteriormente pelo Sr. Nilton,
        '* Esta rotina faz a compara��o dos arquivos para posterior insert na tabela avaliacao_retorno_bb_tb
        
           suSelecaoArquivo
           
           If wk_seg_remessa_arq_retorno_bb = "" Then
                MensagemBatch "Seg. Remessa Arq. Retorno BB inv�lida.", vbCritical
                Call TerminaSEGBR
                'rdocn.BeginTrans
                'Screen.MousePointer = vbArrow
                'Exit Sub
           End If
           
           If CodProdutoBB <> "0000452" Then
              Obtem_Produto CodProdutoBB, ListaProdutos
              AutorizacaoPgtoRestituicao = False
           Else
              ListaProdutos = ""
              AutorizacaoPgtoRestituicao = True
           End If

       ElseIf Sequencial <> "000000" And Sequencial <> "999999" Then  'Registro Detalhe
           'Registro Detalhe

           TpRegistro = Mid(Reg, 7, 1)

           Select Case TpRegistro
                Case "1"
                        'Totalizando registros''''''''''''''''''''''''''''''''''''''''''''''''
                        TotRegsAvaliacao_Detalhe = TotRegsAvaliacao_Detalhe + 1
                        'Lendo dados do registro do detalhe de atributos''''''''''''''''''''''
                        CodigoCampo = Mid(Reg, 8, 4)
                        DescricaoCampo = Mid(Reg, 12, 50)
                        'Incluindo/Atualizando dados'''''''''''''''''''''''''''''''''''''''''''
                        SQL = "Exec atualiza_atributo_bb_spu "
                        SQL = SQL & CodigoCampo
                        SQL = SQL & ",'" & DescricaoCampo & "'"
                        SQL = SQL & ",'" & cUserName & "'"
                        Set Rst = rdocn.OpenResultset(SQL)
                        Rst.Close
                        
                        'Gmarques 16/09/2010
                        'Inclu�ndo inclus�o de atributo enviado pelo bb para o ABS.
                        '------------------------------------------------------------
                        SQL = "Exec atualiza_atributo_bb_spu "
                        SQL = SQL & CodigoCampo
                        SQL = SQL & ",'" & DescricaoCampo & "'"
                        SQL = SQL & ",'" & cUserName & "'"
                        Set Rst = rdocn_Seg2.OpenResultset(SQL)
                        Rst.Close
                        '------------------------------------------------------------
                        
                        'Rafael Oshiro 03/02/2005 - Para processar corretamente caso o arquivo tenha somente Tp Registro 1
                        FlagExisteDocumento = True
                Case "2"
                        'Totalizando registros'''''''''''''''''''''''''''''''''''''''''''''''
                        TotRegsAvaliacao_Detalhe = TotRegsAvaliacao_Detalhe + 1
                        'Lendo dados do registro do detalhe tipos de recusa''''''''''''''''''
                        CodigoRecusa = Mid(Reg, 8, 4)
                        DescricaoRecusa = Mid(Reg, 12, 50)
                        'Incluindo/Atualizando dados'''''''''''''''''''''''''''''''''''''''''
                        SQL = "Exec atualiza_tp_recusa_ret_bb_spu "
                        SQL = SQL & CodigoRecusa
                        SQL = SQL & ",'" & DescricaoRecusa & "'"
                        SQL = SQL & ",'" & cUserName & "'"
                        Set Rst = rdocn.OpenResultset(SQL)
                        Rst.Close
                        
                        'Gmarques 16/09/2010
                        'Inclu�ndo inclus�o de tipo de recusa enviado pelo bb para o ABS.
                        '------------------------------------------------------------
                        SQL = "Exec atualiza_tp_recusa_ret_bb_spu "
                        SQL = SQL & CodigoRecusa
                        SQL = SQL & ",'" & DescricaoRecusa & "'"
                        SQL = SQL & ",'" & cUserName & "'"
                        Set Rst = rdocn_Seg2.OpenResultset(SQL)
                        Rst.Close
                        '------------------------------------------------------------
                        
                        'Rafael Oshiro 03/02/2005 - Para processar corretamente caso o arquivo tenha somente Tp Registro 2
                        FlagExisteDocumento = True
                
                Case "3"
                        'Totalizando registros''''''''''''''''''''''''''''''''''''''''''''''
                        TotRegsAvaliacao_Detalhe = TotRegsAvaliacao_Detalhe + 1
                        'Lendo dados do registro de detalhe de Resutados da An�lise do
                        'retorno BB''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                        If AutorizacaoPgtoRestituicao = True Then
                           ListaProdutos = Mid(Reg, 53, 3)
                           'Maur�cio(Stefanini), em 07/10/2005 - Convers�o dos produtos
                           '1125 e 1141, conforme informado por M�rcio Yoshimura.
                           If ListaProdutos = "125" Then ListaProdutos = "1125"
                           If ListaProdutos = "141" Then ListaProdutos = "1141"
                           'lrocha 15/02/2007
                           If ListaProdutos = "152" Then ListaProdutos = "1152"
                           
                           'Anderson Fiuza FLOW:703448 GPTI 21/01/2009 Adicionado novos produtos ALS
                            'Fabio neto 24/07/2009 adicionado novo produto 1181
                            '---------------------------------------------------
                           'Adicionando a lista de produtos os produtos 1016 e 1201
                           'Alexandre Debouch - 19/04/2011
                           If ListaProdutos = "201" Then ListaProdutos = "1201"
                           If ListaProdutos = "016" Then ListaProdutos = "1016"
                           '---------------------------------------------------
                            
                           If ListaProdutos = "021" Then ListaProdutos = "1021"
                           If ListaProdutos = "038" Then ListaProdutos = "1038"
                           If ListaProdutos = "139" Then ListaProdutos = "1139"
                           If ListaProdutos = "141" Then ListaProdutos = "1141"
                           If ListaProdutos = "144" Then ListaProdutos = "1144"
                           If ListaProdutos = "147" Then ListaProdutos = "1147"
                           If ListaProdutos = "148" Then ListaProdutos = "1148"
                           If ListaProdutos = "149" Then ListaProdutos = "1149"
                           If ListaProdutos = "151" Then ListaProdutos = "1151"
                           If ListaProdutos = "152" Then ListaProdutos = "1152"
                           If ListaProdutos = "162" Then ListaProdutos = "1162"
                           If ListaProdutos = "166" Then ListaProdutos = "1166"
                           If ListaProdutos = "167" Then ListaProdutos = "1167"
                           If ListaProdutos = "168" Then ListaProdutos = "1168"
                           If ListaProdutos = "174" Then ListaProdutos = "1174"
                           If ListaProdutos = "175" Then ListaProdutos = "1175"
                           If ListaProdutos = "176" Then ListaProdutos = "1176" 'Fabio 18/11/2009 inclus�o do produto Residencial Informatica Flowbr 1556776
                           If ListaProdutos = "181" Then ListaProdutos = "1181"
                           If ListaProdutos = "178" Then ListaProdutos = "1178" 'Fabio 28/12/2009 inclus�o do Produto BB Prote��o Residencial - Familiar 2534889
                           If ListaProdutos = "180" Then ListaProdutos = "1180" 'Gmarques 10/03/2010
                           If ListaProdutos = "182" Then ListaProdutos = "1182"
                           If ListaProdutos = "179" Then ListaProdutos = "1179"
                           If ListaProdutos = "177" Then ListaProdutos = "1177"
                           If ListaProdutos = "188" Then ListaProdutos = "1188" 'Fabio 28/04/2010 inclus�o do produto Ouro Residencial Estilo
                           If ListaProdutos = "196" Then ListaProdutos = "1196" 'JFILHO 24/08/2011 inclus�o do produto Ouro Vida Estilo
                           
                           'Alexandre Debouch - Confitec 16/12/2010 inclus�o de produto BB Seguro Cr�dito Protegido (segundo tabela produto_tb
                           If ListaProdutos = "198" Then ListaProdutos = "1198"
                           If ListaProdutos = "017" Then ListaProdutos = "1017"
                           
                           'ysemenov 23/06/2008 Adicionado novos Produtos ALS
                           If ListaProdutos = "038" Then ListaProdutos = "1038"
                           If ListaProdutos = "123" Then ListaProdutos = "1123"
                           '''''''''''''''''''
                                                   'Niuarque Rosa - 06/02/2012
                           'Adicionando o produto 1206 - BB Seguro Vida Empresa Flex
                           If ListaProdutos = "206" Then ListaProdutos = "1206"
                           'FIM
          
                           If ListaProdutos = "204" Then ListaProdutos = "1204"                 'Niuarque Rosa - 21/06/2012
                           If ListaProdutos = "205" Then ListaProdutos = "1205"                 'Niuarque Rosa - 10/08/2012
                           If ListaProdutos = "207" Then ListaProdutos = "1207"                 'Niuarque Rosa - 02/07/2012
                           
                           '---------------------------------------------------------------------------------------------
                           'Adicionando o produto 1211
                           If ListaProdutos = "211" Then ListaProdutos = "1211"                 'TRAMOS  - 04/07/2017
                           '---------------------------------------------------------------------------------------------
                           
                           'Adicionado o produto 1217
                           If ListaProdutos = "217" Then ListaProdutos = "1217"                 'Flavio Abreu  - 29/12/2016
                           '''marcioms - Demanda17833805 - Residencial 2.0 - Inicio
                           '''Incluindo os produtos do residencial 2.0 no De --> Para
                           If ListaProdutos = "220" Then ListaProdutos = "1220"
                           If ListaProdutos = "221" Then ListaProdutos = "1221"
                           If ListaProdutos = "222" Then ListaProdutos = "1222"
                           If ListaProdutos = "223" Then ListaProdutos = "1223"
                           If ListaProdutos = "224" Then ListaProdutos = "1224"
                           '''marcioms - Demanda17833805 - Residencial 2.0 - Fim
                                                   
                           'Adicionando o produto 1225 - BB Seguro Cr�dito Protegido para Empresas
                           If ListaProdutos = "225" Then ListaProdutos = "1225"                 'Flavio Abreu  - 10/10/2014
                           
                           'carlos gomes confitec 08/11/2016
                            If ListaProdutos = "226" Then ListaProdutos = "1226"
'                            If ListaProdutos = "227" Then ListaProdutos = "227,1227"               ' Carlos Gomes  corre��o para o produto 1227
                            If ListaProdutos = "227" Then ListaProdutos = "(227,1227)"              ' Carlos Gomes  corre��o para o produto 1227
                            '---------------------------------------------------
       
                           '---------------------------------------------------------------------------------------------
                           'Adicionando o produto 1228
                           If ListaProdutos = "228" Then ListaProdutos = "1228"                 'FBEZERRA  - 03/03/2017
                           '---------------------------------------------------------------------------------------------
                            
                           'Adicionando o produto 1229
                           If ListaProdutos = "229" Then ListaProdutos = "1229"                 'marciosantos  - 10/09/2015
                           'Adicionando o produto 1231
                           If ListaProdutos = "231" Then ListaProdutos = "1231"                 'marciosantos  - 10/09/2015
                           ' 16/05/2016 - INICIO - Schoralick (Nova Tendencia) -18234489 - Novos produtos para substituir Ouro Vida e outros
                           If ListaProdutos = "235" Then ListaProdutos = "1235"
                           If ListaProdutos = "236" Then ListaProdutos = "1236"
                           If ListaProdutos = "237" Then ListaProdutos = "1237"
                           'In�cio - Isabeli Silva - Confitec SP - 00411015 - Novo Prote��o Ouro - 2018-09-26
                           If ListaProdutos = "239" Then ListaProdutos = "1239"
                           'Fim - Isabeli Silva - Confitec SP - 00411015 - Novo Prote��o Ouro - 2018-09-26
                           ' FIM - 18234489
                                                   
                           ' Carlos Gomes  corre��o para o produto 1227
                           If Not InStr(1, ListaProdutos, ",") > 0 Then
                            ListaProdutos = "(" & Val(ListaProdutos) & ")"
                           End If
                           ' Carlos Gomes  corre��o para o produto 1227
                        
                                                   
                           
                           'racras Ajuste lista produtos BBC
                           If UCase(Left(NomeArqRet, 6)) = "BBC987" Then
                            ListaProdutos = "(12,121,135,136,716)"
                           End If
                           ' FIM RACRAS
                           
                           
                           
                           
                        End If
                        SequencialArquivoSeguradora = Mid(Reg, 8, 6)
                        CodigoRetorno = Mid(Reg, 14, 1)
                        propostaBB = Mid(Reg, 15, 9)
                        ApoliceID = Mid(Reg, 24, 9)
                        endossoBB = Mid(Reg, 33, 9)
                        TpEndossoBB = Mid(Reg, 42, 2)
                        EndossoId = Mid(Reg, 44, 9)
                         'Bruno S Moraes 03/07/2007
                        'If Left(lblarq, 7) = "SEGD987" Or Left(lblarq, 7) = "SEGE987" Then
                        'if acima substituido pelo if abaixo - ecosta 18/09/08
                        If StrArquivo = "SEGD987" Or StrArquivo = "SEGE987" Then
                            propostaBB = 12
                        End If
                        'De Para'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                        If CodigoRetorno = "1" Then
                           AceiteBB = "s"
                        ElseIf CodigoRetorno = "2" Then
                           AceiteBB = "n"
                        End If
                        SituacaoLiberacaoImpressao = AceiteBB
                        
                        If propostaBB <> "" Then
                            If (CDbl(propostaBB) = 0) And StrArquivo = "SEG987O" Then
                                    PropostaId = 0
                                    Call GravaLogErro(StrArquivo, Right(wk_seg_remessa_arq, 4), propostaBB, TpEndossoBB, endossoBB, ListaProdutos, PropostaId, vlngContador, Reg)
                                    GoTo Proximo
                                End If
                        End If
                        'If Left(lblarq, 7) = "SEG987S" Or Left(lblarq, 7) = "SEG987Z" Then
                        'if acima substituido pelo if abaixo - ecosta 18/09/08
                        'Confitec Sistemas - Flavio Abreu 07/03/2014 - Inclus�o dos arquivos:SEG987SS e SEG987SSS.
                        'PAULO PELEGRINI - 27/06/2016 - 19306878 - Incus�o do arquivo: SEG987X
                        If StrArquivo = "SEG987S" Or StrArquivo = "SEG987SS" Or StrArquivo = "SEG987SSS" Or StrArquivo = "SEG987Z" Or StrArquivo = "SEG987AA" Or StrArquivo = "SEG987X" Then 'inclus�o seg987aa - cons. imob. - ecosta 18/09/08
                        
'                            Call Gera_dados_consorcios(Left(lblarq, 7), RemessaArquivoBB, EndossoId, SituacaoLiberacaoImpressao, _
'                                                       AutorizacaoPgtoRestituicao, AceiteBB, DtProcessamento, _
'                                                       PropostaBB, EndossoBB, TpEndossoBB, ListaProdutos, _
'                                                       FlagExisteDocumento, TotRegsAvaliacao_Trailler)
        'comando acima substituido pelo comando abaixo - ecosta 18/09/08
'                            Call Gera_dados_consorcios(StrArquivo, RemessaArquivoBB, EndossoId, SituacaoLiberacaoImpressao, _
'                                                       AutorizacaoPgtoRestituicao, AceiteBB, DtProcessamento, _
'                                                       PropostaBB, EndossoBB, TpEndossoBB, ListaProdutos, _
'                                                       FlagExisteDocumento, TotRegsAvaliacao_Trailler)
                            GoTo TrataErro:


                        Else 'lrocha in�cio
                            'Obtendo proposta_id ''''''''''''''''''''''''''''''''''''''''''''''''
                            'Obtem_Proposta ListaProdutos, PropostaBB, PropostaId, FlagTpProposta, ProdutoProposta, EndossoId
                            Obtem_Proposta ListaProdutos, propostaBB, EndossoId, PropostaId, FlagTpProposta, ProdutoProposta, Cod_empresa
                            'Verificando existencia do documento/gerando log'''''''''''''''''''''
                            FlagExisteDocumento = Valida_Existencia_Documento(PropostaId, EndossoId, Cod_empresa)
                            
                            If FlagExisteDocumento = True Then
                                'Obtendo movimenta��o financeira da restui��o''''''''''''''''''''''''
                                ProdutoProcessaRestituicaoAutomatica = Verifica_Produto_Restituicao_Automatica(ProdutoProposta, Cod_empresa)
                                If (ProdutoProcessaRestituicaoAutomatica = True And Val(EndossoId) > 0 And SituacaoLiberacaoImpressao = "s") Or AutorizacaoPgtoRestituicao = True Then
                                   Obtem_Movimentacao_Financeira PropostaId, EndossoId, MovimentacaoFinancId, EndossoResumoId, Cod_empresa, ProdutoProposta ' INCIDENTE :SD00005455 - ANDERSON.SANTOS - incluindo produto
                                Else
                                   MovimentacaoFinancId = ""
                                End If
                                'Atualizando libera��o de impress�o da proposta'''''''''''''''''''''''''''''
                                If Val(EndossoId) = 0 Then
                                    If FlagTpProposta = TpPropostaAdesao Then
                                        SQL = "Exec atualiza_impressao_adesao_spu "
                                        SQL = SQL & PropostaId
                                        SQL = SQL & ",'" & cUserName & "'"
                                        SQL = SQL & ",'" & SituacaoLiberacaoImpressao & "'"
                                       
                                       If Cod_empresa = "AB" Then
                                            Set Rst = rdocn.OpenResultset(SQL)
                                            Rst.Close
                                       End If
                                       
                                       If Cod_empresa = "ABS" Then
                                            Set Rst = rdocn_Seg2.OpenResultset(SQL)
                                            Rst.Close
                                       End If
                                       
                                    Else
                                    
                                        SQL = "Exec atualiza_impressao_apolice_spu "
                                        SQL = SQL & PropostaId
                                        SQL = SQL & ",'" & cUserName & "'"
                                        SQL = SQL & ",'" & SituacaoLiberacaoImpressao & "'"
                                       
                                       If Cod_empresa = "AB" Then
                                            Set Rst = rdocn.OpenResultset(SQL)
                                            Rst.Close
                                       End If
                                       
                                       If Cod_empresa = "ABS" Then
                                            Set Rst = rdocn_Seg2.OpenResultset(SQL)
                                            Rst.Close
                                       End If
                                       
                                    End If
                                Else
                                    If AutorizacaoPgtoRestituicao = False Then
                                        If Cod_empresa = "AB" Then
                                            'Atualizando libera��o de impress�o do endosso'''''''''''''''
                                            SQL = "Exec atualiza_impressao_endosso_spu "
                                            SQL = SQL & PropostaId
                                            SQL = SQL & "," & EndossoId
                                            SQL = SQL & ",'" & SituacaoLiberacaoImpressao & "'"
                                            SQL = SQL & ",'" & cUserName & "'"
                                            Set Rst = rdocn.OpenResultset(SQL)
                                            Rst.Close
                                            'Atualizando libera��o da movimenta��o financeira'''''''''''''
                                            If Val(MovimentacaoFinancId) > 0 Then
                                                SQL = "Exec aprova_movimentacao_financ_spu "
                                                SQL = SQL & MovimentacaoFinancId
                                                SQL = SQL & ",'s'"
                                                SQL = SQL & ",'" & cUserName & "'"
                                                'Barney - 03/05/2004
                                                SQL = SQL & "," & EndossoResumoId
                                                Set Rst = rdocn.OpenResultset(SQL)
                                                Rst.Close
                                                                                      
                                            End If
                                        End If
                                        
                                        If Cod_empresa = "ABS" Then
                                            'Atualizando libera��o de impress�o do endosso'''''''''''''''
                                            SQL = "Exec atualiza_impressao_endosso_spu "
                                            SQL = SQL & PropostaId
                                            SQL = SQL & "," & EndossoId
                                            SQL = SQL & ",'" & SituacaoLiberacaoImpressao & "'"
                                            SQL = SQL & ",'" & cUserName & "'"
                                            Set Rst = rdocn_Seg2.OpenResultset(SQL)
                                            Rst.Close
                                            'Atualizando libera��o da movimenta��o financeira'''''''''''''
                                            If Val(MovimentacaoFinancId) > 0 Then
                                                SQL = "Exec aprova_movimentacao_financ_spu "
                                                SQL = SQL & MovimentacaoFinancId
                                                SQL = SQL & ",'s'"
                                                SQL = SQL & ",'" & cUserName & "'"
                                                'Barney - 03/05/2004
                                                SQL = SQL & "," & EndossoResumoId
                                                Set Rst = rdocn_Seg2.OpenResultset(SQL)
                                                Rst.Close
                                                                                      
                                            End If
                                        End If
                                        
                                        
                                    End If
                                End If
                                'Incluindo dados da avalia��o do retorno bb'''''''''''''''''''''''''''
                                '* jmendes - 15/04/2002
                                '* Aqui faz o insert na avaliacao_retorno_bb_tb
                                '* Inclus�o das colunas: seg_remessa_arq e seg_remessa_arq_retorno_bb
                                
                                If Verifica_Reincidencia(PropostaId, EndossoId, Cod_empresa) = True Then '***
                                    RetornoReincidente = "S"
                                Else
                                    RetornoReincidente = "N"
                                End If
                                
                               If Mid(wk_seg_remessa_arq_retorno_bb, 1, 6) = "SEG551" And Not bVerificaArq Then
                                    bVerificaArq = True
                                    
                                    sSQL = ""
                                    sSQL = sSQL & " Select arquivo" & vbNewLine
                                    sSQL = sSQL & " From Emi_re_proposta_tb  WITH (NOLOCK)  " & vbNewLine
                                    sSQL = sSQL & " Where" & vbNewLine
                                    sSQL = sSQL & " Proposta_bb = " & propostaBB
                                    sSQL = sSQL & " UNION " & vbNewLine
                                    sSQL = sSQL & " Select arquivo" & vbNewLine
                                    sSQL = sSQL & " From Emi_proposta_tb  WITH (NOLOCK)  " & vbNewLine
                                    sSQL = sSQL & " Where" & vbNewLine
                                    sSQL = sSQL & " Proposta_bb = " & propostaBB
                                        
                                    If Cod_empresa = "AB" Then
                                        Set rc_aux = rdocn.OpenResultset(sSQL)
                                    End If
                                    
                                    If Cod_empresa = "ABS" Then
                                        Set rc_aux = rdocn_Seg2.OpenResultset(sSQL)
                                    End If
                                    
                                    
                                    If Not rc_aux.EOF Then
                                        vArquivo = Split(rc_aux(0), ".")
                                        If UCase(vArquivo(0)) = "SEG486E" Then
                                        'febner
                                         vArquivo(1) = Val(Mid(wk_seg_remessa_arq_retorno_bb, InStr(1, wk_seg_remessa_arq_retorno_bb, ".") + 1))
                                          wk_seg_remessa_arq_retorno_bb = "SEG556." & Format(vArquivo(1), "0000")
                                        End If
                                    End If
                                    rc_aux.Close 'Barney - 14/08/2003
                                End If
                                                                
                                                                'Niuarque Rosa - CONFITEC - 04/09/2013
                                'Carga das informa��es de Movimenta��o e endosso no BBC452, a atual vers�o n�o retorna do BB contendo as mesmas
                                'Solu��o paliativa, O acerto permanente necessita de ajustes no BBC452 por parte do BB, mencionando o endosso de restitui��o do mesmo.
                                If MovimentacaoFinancId = "0" And StrArquivo = "BBC987C" Then
                                    Call cargaRetornoBBC452(PropostaId, wk_seg_remessa_arq_retorno_bb, EndossoId, MovimentacaoFinancId)
                                End If
                            
                            
                                SQL = "Exec avaliacao_retorno_bb_spi "
                                SQL = SQL & PropostaId
                                
                               'WBARBOZA - 17/07/2014 - Tratamento Restitui��o Conta-Cartao
                                If (StrArquivo = "SEG987O" Or StrArquivo = "SEGD987" Or StrArquivo = "SEGE987") And (ProdutoProposta = "1125" Or ProdutoProposta = "1141") And Mid(wk_seg_remessa_arq_retorno_bb, 1, 6) <> "SEG452" Then 'INCIDENTE :SD00005455 - ANDERSON.SANTOS - incluindo validacao 452
                                    SQL = SQL & ", NULL " 'Anula o endosso � registrar, evitando erro de chave
                                Else
                                    SQL = SQL & ", " & IIf(Val(EndossoId) <> 0, EndossoId, "NULL")
                                End If
                                
                                SQL = SQL & ", '" & AceiteBB & "'"
                                SQL = SQL & ", " & RemessaArquivoBB
                                SQL = SQL & ", '" & Format(DtProcessamento, "yyyymmdd") & "'"
                                SQL = SQL & ", '" & cUserName & "'"
                                If AutorizacaoPgtoRestituicao = True Then
                                   SQL = SQL & ", " & MovimentacaoFinancId
                                Else
                                   SQL = SQL & ", NULL"
                                End If
                                SQL = SQL & ", '" & wk_seg_remessa_arq & "' "                '* jmendes
                                SQL = SQL & ", '" & wk_seg_remessa_arq_retorno_bb & "'"    '* jmendes
                                SQL = SQL & ", '" & Format(Data_Sistema, "yyyymmdd") & "'"    '* jmendes
                                SQL = SQL & ", '" & RetornoReincidente & "'" '***
                            
                                If Cod_empresa = "AB" Then

                                   Set Rst = rdocn.OpenResultset(SQL)
                                   'Colhendo c�digo de avalia��o gerado'''''''''''''''''''''''''''''''''
                                   SeqAvaliacaoRetornoBBId = Rst(0)
                                    Rst.Close
                                End If
                                 
                                If Cod_empresa = "ABS" Then
                                   Set Rst = rdocn_Seg2.OpenResultset(SQL)
                                   'Colhendo c�digo de avalia��o gerado'''''''''''''''''''''''''''''''''
                                   SeqAvaliacaoRetornoBBId = Rst(0)
                                   Rst.Close
                                End If
                                
                                'JOAO.MACHADO - 19368999 - cobran�a registrada AB e ABS - INICIO
                                If AceiteBB = "s" Then
                                    
                                                                        If IsNull(EndossoId) Or Trim(EndossoId) = "" Then EndossoId = 0
                                                                        
                                    SQL = ""
                                    SQL = "EXEC SEGUROS_DB.DBO.SEGS13178_SPU "
                                    SQL = SQL & PropostaId
                                    SQL = SQL & ", " & EndossoId
                                    SQL = SQL & ", 's' "
                                    
                                    
                                    If Cod_empresa = "AB" Then
                                        Set Rst = rdocn.OpenResultset(SQL)
                                        Rst.Close
                                    End If
                                     
                                    If Cod_empresa = "ABS" Then
                                       Set Rst = rdocn_Seg2.OpenResultset(SQL)
                                       Rst.Close
                                    End If
                                
                                End If
                                
                                'JOAO.MACHADO - 19368999 - cobran�a registrada AB e ABS - FIM
                                
                                ' 20/04/2004, Atualiza��o do Controle M�s Referencia de Conta Cart�o 'asilva
                                ' Os registros de prolabore do conta cartao possuem endosso_id = 0
    '                            If Val(EndossoId) <> 0 And AceiteBB = "s" Then
    '                              Call AtualizarControleMesReferencia(PropostaId, EndossoId)
    '                            End If
                                
                                If tipo_arquivo = "465" And AceiteBB = "s" Then
                                    'Confitec Sistemas - Diogo Ferreira 20/04/2011 - Flow 10162776: Tratamento AB/ABS
                                    'Call AtualizarControleMesReferencia(ano_mes_ref, PropostaId, Mid(wk_seg_remessa_arq_retorno_bb, Len(wk_seg_remessa_arq_retorno_bb) - 3, 4))
                                    Call AtualizarControleMesReferencia(ano_mes_ref, PropostaId, Mid(wk_seg_remessa_arq_retorno_bb, Len(wk_seg_remessa_arq_retorno_bb) - 3, 4), Cod_empresa)
                                End If
                                ' bcarneiro - 03/06/2004 - No tratamento dos Arquivos 987u e 987v, dever� ser
                                '                          incluido na Renova��o Monitorada o evento 16 (Proposta
                                '                          de Renova��o Aceita pelo BB) para cada proposta tratada.
                                If (Mid(wk_seg_remessa_arq_retorno_bb, 1, 6) = "SEG512" Or Mid(wk_seg_remessa_arq_retorno_bb, 1, 6) = "SEG514") And AceiteBB = "s" Then
                                    If ConsultarPropostaRenovacaoMonitorada(PropostaId, Cod_empresa) Then
                                        Call IncluirEventoRenovacaoMonitorada(PropostaId, Cod_empresa)
                                    End If
                                End If
                                
                                If (Mid(wk_seg_remessa_arq_retorno_bb, 1, 6) = "SEG552" And ProdutoProposta = 721 And AceiteBB = "s" And Val(EndossoId) <> 0) Then
                                    If EndossodeRenovacao(PropostaId, EndossoId, Cod_empresa) Then
                                        If ConsultarPropostaRenovacaoMonitorada(PropostaId, Cod_empresa) Then
                                            Call IncluirEventoRenovacaoMonitorada(PropostaId, Cod_empresa, EndossoId)
                                        End If
                                    End If
                                End If
                            Else
                                'Granvando log de inexistencia de documento''''''''''''''''''''''''''
                                ' A pedido do Radicchi essas propostas ser�o ignoradas
                                If propostaBB <> "321200032" And propostaBB <> "332400257" Then
                                    LinhaInconsistencia = "Proposta BB: " & propostaBB & ", Endosso BB: " & endossoBB & ", Tipo Endosso BB: " & TpEndossoBB & ", Produto: " & ListaProdutos & ", Aceite BB: " & AceiteBB & "; Documento n�o encontrado "
                                    ListInconsistencias.AddItem LinhaInconsistencia
                                    Call GravaLogErro(StrArquivo, Right(wk_seg_remessa_arq, 4), propostaBB, TpEndossoBB, endossoBB, ListaProdutos, PropostaId, vlngContador, Reg)
                                    
                                    'GravaMensagem LinhaInconsistencia, "PROC", "Importa_Avaliacoes_Retorno_BB", Me.name
                                    'Val�ria - 23/04/2003
                                    'Adenilson (N�o termina a opera��o continua com opera��o no pr�ximo arquivo
                                    'Exit Do
                                    'lblstatus.Caption = "Processado com Erro"
                                    'Call grava_log
                                    'TerminaSEGBR
                                ''''''''''''''''''''''''''
                                End If
                            End If
                        
                            If AceiteBB = "n" Then
                                'Lendo motivos de recusa do retorno BB'''''''''''''''''''''''''''
                                MotivoRecusaInfomado = False
                                Do
                                    Line Input #arqinput, Reg
                                    'Atualizando interface
                                    tam = tam + Len(Reg)
                                    term1.Value = tam
                                    Sequencial = Mid(Reg, 1, 6)
                                    If Sequencial = "999999" Then
                                       exec_extrato = 1
                                       GoTo extrato
                                    End If
                                    TpRegistro = Mid(Reg, 7, 1)
                                    If TpRegistro = 4 Then
                                        TotRegsAvaliacao_Detalhe = TotRegsAvaliacao_Detalhe + 1
                                        MotivoRecusaInfomado = True
                                        
                                        'Lendo dados do motivo da recusa'''''''''''''''''''''''''
                                        CodigoRecusa = Mid(Reg, 8, 4)
                                        CodigoCampo = Mid(Reg, 12, 4)
                                        InformacaoNaBaseBB = Mid(Reg, 16, 17)
                                        InformacaoRetornada = Mid(Reg, 33, 17)
                                        If FlagExisteDocumento = True Then
                                            'Caso o atributo da recusa ainda n�o esteja cadastrado,
                                            'o mesmo ser� inclu�do.
                                            If Verifica_Cadastro_Atributo_BB(CodigoCampo, Cod_empresa) = False Then
                                                SQL = "Exec atualiza_atributo_bb_spu "
                                                SQL = SQL & CodigoCampo
                                                SQL = SQL & ",'A DEFINIR'"
                                                SQL = SQL & ",'" & cUserName & "'"
                                                
                                                If Cod_empresa = "AB" Then
                                                    Set Rst = rdocn.OpenResultset(SQL)
                                                    Rst.Close
                                                End If
                                                
                                                If Cod_empresa = "ABS" Then
                                                    Set Rst = rdocn_Seg2.OpenResultset(SQL)
                                                    Rst.Close
                                                End If
                                                
                                            End If
                                            'Caso o tipo de recusa ainda n�o esteja cadastrado,
                                            'o mesmo ser� inclu�do.
                                            If Verifica_Cadastro_Tp_Recusa_BB(CodigoRecusa, Cod_empresa) = False Then
                                                SQL = "Exec atualiza_tp_recusa_ret_bb_spu "
                                                SQL = SQL & CodigoRecusa
                                                SQL = SQL & ",'A DEFINIR'"
                                                SQL = SQL & ",'" & cUserName & "'"
                                                
                                                If Cod_empresa = "AB" Then
                                                    Set Rst = rdocn.OpenResultset(SQL)
                                                    Rst.Close
                                                End If
                                                
                                                If Cod_empresa = "ABS" Then
                                                    Set Rst = rdocn_Seg2.OpenResultset(SQL)
                                                    Rst.Close
                                                End If
                                                
                                            End If
                                            
                                            'Incluindo dados da recusa ''''''''''''''''''''''''''''''''''
                                            SQL = "Exec avaliacao_rec_retorno_bb_spi "
                                            SQL = SQL & SeqAvaliacaoRetornoBBId
                                            SQL = SQL & "," & CodigoCampo
                                            SQL = SQL & "," & CodigoRecusa
                                            SQL = SQL & ",'" & InformacaoRetornada & "'"
                                            SQL = SQL & ",'" & InformacaoNaBaseBB & "'"
                                            SQL = SQL & ",'" & cUserName & "'"
                                                
                                            If Cod_empresa = "AB" Then
                                                Set Rst = rdocn.OpenResultset(SQL)
                                                Rst.Close
                                            End If
                                            
                                            If Cod_empresa = "ABS" Then
                                                Set Rst = rdocn_Seg2.OpenResultset(SQL)
                                                Rst.Close
                                            End If
                                            
                                        End If
                                    Else
                                        FlagLerProximoRegistro = False
                                    End If
                                Loop Until TpRegistro <> "4"
                                If MotivoRecusaInfomado = False Then
                                   MensagemBatch "Motivo da recusa n�o encontrado. "
                                   TerminaSEGBR
                                End If
                            End If
                        
extrato:
                        'daniel.menandro - Nova Consultoria - 14/07/2013
                        '16351367 - Extrato de Restitui��o
                        'Inicio
                         If Not IsNull(PropostaId) And PropostaId <> "" Then
                         
                            SQL = ""
                            SQL = SQL & " select pc.val_movimentacao"
                            SQL = SQL & "   from ps_mov_endosso_financeiro_tb pmef WITH (NOLOCK)"
                            SQL = SQL & "        INNER JOIN ps_mov_cliente_tb pc WITH (NOLOCK)"
                            SQL = SQL & "           ON pmef.movimentacao_id = pc.movimentacao_id"
                            SQL = SQL & "  where pmef.proposta_id = " & PropostaId
                            SQL = SQL & "    and pmef.endosso_id = cast(" & Val(EndossoId) & " as int)"
                             
                            Set Rst = rdocn.OpenResultset(SQL)
                            
                            If Not Rst.EOF Then
                            
                            val_restituicao = Rst(0)
                            
                            Rst.Close
                            
                            'INICIO - CIBELE PEREIRA - 30/10/2014 - INC000004409116
                            
                            If AceiteBB = "s" Then
                                CodigoRecusa = ""
                                motivo_recusa = ""
                             End If
                         
                            'FIM - CIBELE PEREIRA - 30/10/2014 - INC000004409116
                            
                            
                            If Not IsNull(CodigoRecusa) And CodigoRecusa <> "" Then
                            
                                SQL = "select descricao from  seguros_db.dbo.tp_recusa_retorno_bb_tb  WITH (NOLOCK)  where tp_recusa_bb_id = " & CodigoRecusa
                                
                                Set Rst = rdocn.OpenResultset(SQL)
                                
                                If Not Rst.EOF Then
                                
                                motivo_recusa = IIf(IsNull(Rst(0)), "", Rst(0))
                                
                                End If
                                
                                Rst.Close
                                
                            End If
                             
                            If (ProdutoProposta = 11) Or (ProdutoProposta = 12) Or (ProdutoProposta = 121) Or (ProdutoProposta = 135) Or (ProdutoProposta = 136) _
                                    Or (ProdutoProposta = 716) Or (ProdutoProposta = 721) Or (ProdutoProposta = 1174) Or (ProdutoProposta = 1175) _
                                    Or (ProdutoProposta = 1177) Or (ProdutoProposta = 1179) Or (ProdutoProposta = 1180) Or (ProdutoProposta = 1181) _
                                    Or (ProdutoProposta = 1182) Or (ProdutoProposta = 1196) Or (ProdutoProposta = 1198) Or (ProdutoProposta = 1205) _
                                    Or (ProdutoProposta = 1208) Or (ProdutoProposta = 1211) Or (ProdutoProposta = 1217) Or (ProdutoProposta = 1218) Or (ProdutoProposta = 1235) Or (ProdutoProposta = 1236) Or (ProdutoProposta = 1237) _
                                    Or (ProdutoProposta = 1239) Then 'Isabeli Silva - Confitec SP - 00411015 - Novo Prote��o Ouro - 2018-09-26
                                If Mid(wk_seg_remessa_arq, 1, 7) = "SEG987O" Or Mid(wk_seg_remessa_arq, 1, 7) = "BBC987C" Then
                            SQL = ""
                            SQL = SQL & "SET NOCOUNT ON EXEC SEGS11299_SPI"
                            SQL = SQL & " '" & App.Title & "'"
                            SQL = SQL & ", " & PropostaId
                            If Val(EndossoId) <> 0 Then
                                SQL = SQL & "," & Val(EndossoId)
                            Else
                                SQL = SQL & ", NULL"
                            End If
                            SQL = SQL & ", " & IIf(IsNull(val_restituicao) Or val_restituicao = "", "NULL", val_restituicao)
                            SQL = SQL & ",'Sistema Autom�tico'"
                                    If IsNull(motivo_recusa) Or (motivo_recusa = "") Then       'Se foi aceito
                                       SQL = SQL & ",'Aceite do BB'"
                                        SQL = SQL & "," & "'Pagamento da restitui��o confirmado pelo BB'"
                            SQL = SQL & ",'" & IIf(IsNull(motivo_recusa), "", motivo_recusa) & "'"
                                    Else                                'Se foi recusado
                                       SQL = SQL & ",'Recusa do BB'"
                                        SQL = SQL & "," & "'Pagamento da restitui��o recusada pelo BB'"
                                        SQL = SQL & ",'" & IIf(IsNull(motivo_recusa), "", motivo_recusa) & "'"
                                    End If
                                    SQL = SQL & "," & "'" & wk_seg_remessa_arq & "'"
                            Set Rst = rdocn.OpenResultset(SQL)
                                    Rst.Close
                                End If
                            End If
                    Else
                    
                        Rst.Close
                    
                        End If
                        
                 End If
                        

                        
                        If exec_extrato = 1 Then
                            GoTo Executa_Trailler
                        End If
                        
                        'Fim
                        
                        End If 'lrocha fim
                        
                'Rafael Oshiro - STEFANINI - 18/07/05
                Case Else
                    TotRegsAvaliacao_Detalhe = TotRegsAvaliacao_Detalhe + 1
                    
           End Select

       ElseIf Sequencial = "999999" Then ''trailler
                    
Executa_Trailler:
          'Lendo dados do trailler''''''''''''''''''''''''''''''''''''''''''''''''''''''''
              TotRegsAvaliacao_Trailler = TotRegsAvaliacao_Trailler + Val(Mid(Reg, 43, 6))
              
              ' Verifica se o arquivo importado n�o tem detalhes.
              ' Caso n�o tenha, � atribu�do o valor TRUE para a vari�vel FlagExisteDocumento,
              ' para que o arquivo possa ser processado normanlmente.
              ' Carlos Renato 19/05/2003.
              If TotRegsAvaliacao_Trailler = 0 Then
                 FlagExisteDocumento = True
              End If
                 
       End If
       
Proximo:
    
       
       

   Loop Until EOF(arqinput)
   
   vlngContador = 0
   
   
   
   'Fechando arquivo de avalia��es'''''''''''''''''''''''''''''''''''''''''''''''''
   Close #arqinput
   
   
   'carolina.marinho 28/09/2011 - Demanda 11862419 - Restitui��o CDC - Confitec RJ
   'procedure que vai atualizar a flag de aceita��o do banco na ps_mov_cliente_tb, de acordo com a avaliacao_retorno_bb_tb
    If AutorizacaoPgtoRestituicao = True Then
    
        'SQL = "EXEC desenv_db.dbo.CMARINHO_137 " & RemessaArquivoBB & ", '" & cUserName & "'"
        SQL = "EXEC SEGS9783_SPU " & RemessaArquivoBB & ", '" & cUserName & "'"
    
         
         If Cod_empresa = "AB" Then
             rdocn.OpenResultset (SQL)
         End If
        
         If Cod_empresa = "ABS" Then
             rdocn_Seg2.OpenResultset (SQL)
         End If
         
    End If
    'carolina.marinho fim
   
   
   
   'Gravando atualiza�oes e movendo arquivo de avalia��es para o diret�rio de pro-'
   'cessados'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''


    'Confitec Sistemas - Diogo Ferreira 17/01/2012 - Demanda 13981228: Inclus�o de mensagem de erro no programa para recusas do conta cart�o
    If AceiteBB = "n" And (StrArquivo = "SEGD987" Or StrArquivo = "SEGE987") Then
        bRecusaContaCartao = True
    End If
    
    
    If FlagExisteDocumento = True Then
        Grava_Operacao TotRegsAvaliacao_Trailler, TotRegsAvaliacao_Detalhe, ""
    Else
        Grava_Operacao TotRegsAvaliacao_Trailler, TotRegsAvaliacao_Detalhe, "1"
    End If
    
   If Not Flag_Erro_Trailler Then
       'Atualizando dados de controle de importacao de arquivo''''''''''''''''''''''
       Call Atualiza_Controle_Arquivo(StrArquivoVrs, lblver.Caption, position, TotRegsAvaliacao_Detalhe)
       'comentado trocado pelo comando acima ecosta 11/2008
       'Call Atualiza_Controle_Arquivo(lblarq.Caption , lblver.Caption, position, TotRegsAvaliacao_Detalhe)
   Else
       MensagemBatch "Erro no Trailler. Importa��o ser� abortada!"
   End If
    
    
   term1.Visible = False
   grava_log
   Exit Sub
   
   rdocn.RollbackTrans
   rdocn_Seg2.RollbackTrans
   
TrataErro:
    vlngContador = 0
    TrataErroGeral "Importa_Avaliacoes_Retorno_BB - PropostaBB: " & propostaBB & " - Proposta_id: " & PropostaId & " - SQL: " & SQL, Me.name
    Call TerminaSEGBR
    Resume
End Sub

'Sub Gera_dados_consorcios(ByVal Arquivo, _
'                          ByVal RemessaArquivoBB, _
'                          ByVal EndossoId, _
'                          ByVal SituacaoLiberacaoImpressao, _
'                          ByVal AutorizacaoPgtoRestituicao, _
'                          ByVal AceiteBB, _
'                          ByVal DtProcessamento, _
'                          ByVal PropostaBB, _
'                          ByVal EndossoBB, _
'                          ByVal TpEndossoBB, _
'                          ByVal ListaProdutos, _
'                          ByRef FlagExisteDocumento, _
'                          ByRef TotRegsAvaliacao_Trailler)
Public Function GravaLogErro(ByVal StrArquivo As String, ByVal Remessa As String, _
                             ByVal propostaBB As String, ByVal endossoBB As String, ByVal TpEndossoBB As String, _
                             ByVal Produto As String, ByVal PropostaId As String, ByVal vlngContador As Long, ByVal registro_linha As String) As Boolean
    

Dim vstrSQL As String

On Error GoTo TrataErro

    vstrSQL = "exec SEGS11094_SPI '"
    vstrSQL = vstrSQL & StrArquivo & "','"
    vstrSQL = vstrSQL & Remessa & "','"
    vstrSQL = vstrSQL & propostaBB & "','"
    vstrSQL = vstrSQL & endossoBB & "','"
    vstrSQL = vstrSQL & TpEndossoBB & "','"
    vstrSQL = vstrSQL & Produto & "','"
    vstrSQL = vstrSQL & PropostaId & "','"
    vstrSQL = vstrSQL & vlngContador & "','"
    vstrSQL = vstrSQL & registro_linha & "';"
    
    
    If Cod_empresa = "AB" Then
        rdocn.OpenResultset (vstrSQL)
    Else
        rdocn_Seg2.OpenResultset (vstrSQL)
    End If



    Exit Function
TrataErro:
   TrataErroGeral "GravaLogErro", Me.name
   TerminaSEGBR
End Function
                  


                            
Sub cargaRetornoBBC452(ByVal Proposta_Id As String, ByVal arquivo_Envio_BB As String, ByRef Endosso_id As String, ByRef Movimentacao_id As String)

Dim Rst As rdoResultset
Dim SQL As String

On Error GoTo TrataErro

'# Obtendo dados de retorno apartir da PS_MOV '''''''''''''''''''''''''''''''''''''''''''
SQL = " "
SQL = SQL & "SELECT ps_mov_cliente_tb.movimentacao_id, ps_mov_endosso_financeiro_tb.endosso_id" & vbCrLf
SQL = SQL & "  FROM ps_mov_endosso_financeiro_tb  WITH (NOLOCK) " & vbCrLf
SQL = SQL & "  JOIN ps_mov_cliente_tb  WITH (NOLOCK) " & vbCrLf
SQL = SQL & "    ON ps_mov_cliente_tb.movimentacao_id         = ps_mov_endosso_financeiro_tb.movimentacao_id" & vbCrLf
SQL = SQL & " WHERE ps_mov_endosso_financeiro_tb.Proposta_Id  = " & Proposta_Id & vbCrLf
SQL = SQL & "   AND ps_mov_cliente_tb.arquivo_retorno_bb      = '" & arquivo_Envio_BB & "'" & vbCrLf
    
Set Rst = rdocn.OpenResultset(SQL)
   
If Not Rst.EOF Then
    Endosso_id = Rst!Endosso_id
    Movimentacao_id = Rst!Movimentacao_id
End If

   Exit Sub
   
TrataErro:
    TrataErroGeral "cargaRetornoBBC452", Me.name

End Sub
                            
                            

Function Obtem_Proposta(ByVal PListaProdutos As String, _
                        ByVal PPropostaBB As String, _
                        ByVal PEndossoId As String, _
                        ByRef PPropostaId As String, _
                        ByRef PFlagTpProposta As String, _
                        ByRef PProduto, _
                        ByRef Pcod_empresa As String) As String

Dim RstAB As rdoResultset
Dim RstABS As rdoResultset
Dim SQL As String

On Error GoTo TrataErro
'Flavio.Abreu - Limpeza do conte�do das v�riaveis.
'Data: 03/09/2014
PPropostaId = ""
PProduto = ""
PFlagTpProposta = ""
    
SQL = ""
SQL = SQL & "SELECT PROPOSTA_TB.PROPOSTA_ID" & vbNewLine
SQL = SQL & "     , PROPOSTA_TB.PRODUTO_ID" & vbNewLine
'Flow 19436313 - Confitec - Claudia.Silva - 15/09/2016
SQL = SQL & "     , COD_EMPRESA = CASE WHEN PROPOSTA_TB.PRODUTO_ID = 650 AND PROPOSTA_TB.SUBRAMO_ID IN (6507,1428,1434,1828,1834) THEN 'AB' ELSE RTRIM(RAMO_TB.COD_EMPRESA) END " & vbNewLine
'----------------------------------------------------------------------
SQL = SQL & "     , FlagTpProposta = 1" & vbNewLine
SQL = SQL & "  From PROPOSTA_TB" & vbNewLine
SQL = SQL & "  Join RAMO_TB" & vbNewLine
SQL = SQL & "    ON RAMO_TB.RAMO_ID = PROPOSTA_TB.RAMO_ID" & vbNewLine
SQL = SQL & "  Join PROPOSTA_ADESAO_TB" & vbNewLine
SQL = SQL & "    ON PROPOSTA_ADESAO_TB.PROPOSTA_ID = PROPOSTA_TB.PROPOSTA_ID" & vbNewLine
SQL = SQL & " Where 1 = 1" & vbNewLine
SQL = SQL & "   AND PROPOSTA_TB.PRODUTO_ID IN " & PListaProdutos & vbNewLine
'----------------------------------------------------------------------
'Adicionando o X na situa��o (X - Representa propostas com ajustes da Tecnologia
'e que n�o devem ser processadas
'Alexandre - Confitec - 19/04/2011

If StrArquivo = "SEG987O" Then
    SQL = SQL & "   AND PROPOSTA_TB.SITUACAO not in ('T','X','E')" & vbNewLine
Else
    SQL = SQL & "   AND PROPOSTA_TB.SITUACAO not in ('T','X')" & vbNewLine
End If
'-----------------------------------------------------------------------
SQL = SQL & "   AND PROPOSTA_ADESAO_TB.PROPOSTA_BB = " & PPropostaBB & vbNewLine
SQL = SQL & "Union" & vbNewLine
SQL = SQL & "SELECT PROPOSTA_TB.PROPOSTA_ID" & vbNewLine
SQL = SQL & "     , PROPOSTA_TB.PRODUTO_ID" & vbNewLine
'Flow 19436313 - Confitec - Claudia.Silva - 15/09/2016
SQL = SQL & "     , COD_EMPRESA = CASE WHEN PROPOSTA_TB.PRODUTO_ID = 650 AND PROPOSTA_TB.SUBRAMO_ID IN (6507,1428,1434,1828,1834) THEN 'AB' ELSE RTRIM(RAMO_TB.COD_EMPRESA) END " & vbNewLine
'----------------------------------------------------------------------
SQL = SQL & "     , FlagTpProposta = 2" & vbNewLine
SQL = SQL & "  From PROPOSTA_TB" & vbNewLine
SQL = SQL & "  Join RAMO_TB" & vbNewLine
SQL = SQL & "    ON RAMO_TB.RAMO_ID = PROPOSTA_TB.RAMO_ID" & vbNewLine
SQL = SQL & "  Join PROPOSTA_FECHADA_TB" & vbNewLine
SQL = SQL & "    ON PROPOSTA_FECHADA_TB.PROPOSTA_ID = PROPOSTA_TB.PROPOSTA_ID" & vbNewLine
SQL = SQL & " Where 1 = 1" & vbNewLine
SQL = SQL & "   AND PROPOSTA_TB.PRODUTO_ID IN " & PListaProdutos & vbNewLine
SQL = SQL & "   AND PROPOSTA_TB.PRODUTO_ID NOT IN (1125,1141)" & vbNewLine
'----------------------------------------------------------------------
'Adicionando o X na situa��o (X - Representa propostas com ajustes da Tecnologia
'e que n�o devem ser processadas
'Alexandre - Confitec - 19/04/2011

If StrArquivo = "SEG987O" Then
    SQL = SQL & "   AND PROPOSTA_TB.SITUACAO not in ('T','X','E') " & vbNewLine
Else
    SQL = SQL & "   AND PROPOSTA_TB.SITUACAO not in ('T','X') " & vbNewLine
End If
'-----------------------------------------------------------------------
SQL = SQL & "   AND PROPOSTA_FECHADA_TB.PROPOSTA_BB = " & PPropostaBB & vbNewLine


'-----------------------------------------------------------------------
'WBarboza/Fabreu - 2014-07-21
SQL = SQL & "Union" & vbNewLine
SQL = SQL & "SELECT PROPOSTA_TB.PROPOSTA_ID" & vbNewLine
SQL = SQL & "     , PROPOSTA_TB.PRODUTO_ID" & vbNewLine
'Flow 19436313 - Confitec - Claudia.Silva - 15/09/2016
SQL = SQL & "     , COD_EMPRESA = CASE WHEN PROPOSTA_TB.PRODUTO_ID = 650 AND PROPOSTA_TB.SUBRAMO_ID IN (6507,1428,1434,1828,1834) THEN 'AB' ELSE RTRIM(RAMO_TB.COD_EMPRESA) END " & vbNewLine
'----------------------------------------------------------------------
SQL = SQL & "     , FlagTpProposta = 2" & vbNewLine
SQL = SQL & "  From PROPOSTA_TB" & vbNewLine
SQL = SQL & "  Join RAMO_TB" & vbNewLine
SQL = SQL & "    ON RAMO_TB.RAMO_ID = PROPOSTA_TB.RAMO_ID" & vbNewLine
SQL = SQL & "  Join PROPOSTA_FECHADA_TB" & vbNewLine
SQL = SQL & "    ON PROPOSTA_FECHADA_TB.PROPOSTA_ID = PROPOSTA_TB.PROPOSTA_ID" & vbNewLine

If StrArquivo = "SEG987O" Then 's� dever� usar essa tabela quando for importa��o do arquivo SEG987O rtavares 30 07 2014
    SQL = SQL & "  Join ENDOSSO_RESUMO_TB" & vbNewLine
    SQL = SQL & "    ON ENDOSSO_RESUMO_TB.PROPOSTA_ID = PROPOSTA_TB.PROPOSTA_ID" & vbNewLine
End If

SQL = SQL & " Where 1 = 1" & vbNewLine
SQL = SQL & "   AND PROPOSTA_TB.PRODUTO_ID IN (1125,1141)" & vbNewLine
'----------------------------------------------------------------------
'Adicionando o X na situa��o (X - Representa propostas com ajustes da Tecnologia
'e que n�o devem ser processadas
'Alexandre - Confitec - 19/04/2011

If StrArquivo = "SEG987O" Then
    SQL = SQL & "   AND PROPOSTA_TB.SITUACAO not in ('T','X','E') " & vbNewLine
Else
    SQL = SQL & "   AND PROPOSTA_TB.SITUACAO not in ('T','X') " & vbNewLine
End If
'-----------------------------------------------------------------------
If IsNull(PEndossoId) Or PEndossoId = "" Then PEndossoId = 0

SQL = SQL & "   AND PROPOSTA_FECHADA_TB.PROPOSTA_BB = " & PPropostaBB & vbNewLine

If StrArquivo = "SEG987O" Then 's� dever� usar essa tabela quando for importa��o do arquivo SEG987O rtavares 30 07 2014
    SQL = SQL & "   AND ENDOSSO_RESUMO_TB.ENDOSSO_RESUMO_ID = " & PEndossoId & vbNewLine
End If


SQL = SQL & "Union" & vbNewLine
SQL = SQL & "SELECT PROPOSTA_TB.PROPOSTA_ID" & vbNewLine
SQL = SQL & "     , PROPOSTA_TB.PRODUTO_ID" & vbNewLine
'Flow 19436313 - Confitec - Claudia.Silva - 15/09/2016
SQL = SQL & "     , COD_EMPRESA = CASE WHEN PROPOSTA_TB.PRODUTO_ID = 650 AND PROPOSTA_TB.SUBRAMO_ID IN (6507,1428,1434,1828,1834) THEN 'AB' ELSE RTRIM(RAMO_TB.COD_EMPRESA) END " & vbNewLine
'----------------------------------------------------------------------
SQL = SQL & "     , FlagTpProposta = 2" & vbNewLine
SQL = SQL & "  From PROPOSTA_TB" & vbNewLine
SQL = SQL & "  Join RAMO_TB" & vbNewLine
SQL = SQL & "    ON RAMO_TB.RAMO_ID = PROPOSTA_TB.RAMO_ID" & vbNewLine
SQL = SQL & "  Join EMI_RE_PROPOSTA_TB" & vbNewLine
SQL = SQL & "    ON EMI_RE_PROPOSTA_TB.PROPOSTA_ID = PROPOSTA_TB.PROPOSTA_ID" & vbNewLine
SQL = SQL & " Where 1 = 1" & vbNewLine
'----------------------------------------------------------------------
'Adicionando o X na situa��o (X - Representa propostas com ajustes da Tecnologia
'e que n�o devem ser processadas
'Alexandre - Confitec - 19/04/2011
SQL = SQL & "   AND PROPOSTA_TB.PRODUTO_ID IN " & PListaProdutos & vbNewLine

If StrArquivo = "SEG987O" Then
    SQL = SQL & "   AND PROPOSTA_TB.SITUACAO not in ('T','X','E') " & vbNewLine
Else
    SQL = SQL & "   AND PROPOSTA_TB.SITUACAO not in ('T','X') " & vbNewLine
End If
'-----------------------------------------------------------------------
SQL = SQL & "   AND EMI_RE_PROPOSTA_TB.PROPOSTA_BB = " & PPropostaBB & vbNewLine
SQL = SQL & " ORDER By PROPOSTA_TB.PROPOSTA_ID DESC " & vbNewLine

Set RstAB = rdocn.OpenResultset(SQL)
   
If Not RstAB.EOF Then
    PPropostaId = RstAB!Proposta_Id
    PProduto = RstAB!produto_id
    PFlagTpProposta = RstAB!FlagTpProposta
    Pcod_empresa = RstAB!Cod_empresa
    RstAB.Close
    Exit Function
End If

  
SQL = ""
SQL = SQL & "SELECT PROPOSTA_TB.PROPOSTA_ID" & vbNewLine
SQL = SQL & "     , PROPOSTA_TB.PRODUTO_ID" & vbNewLine
'Flow 19436313 - Confitec - Claudia.Silva - 15/09/2016
SQL = SQL & "     , COD_EMPRESA = CASE WHEN PROPOSTA_TB.PRODUTO_ID = 650 AND PROPOSTA_TB.SUBRAMO_ID IN (6507,1428,1434,1828,1834) THEN 'AB' ELSE RTRIM(RAMO_TB.COD_EMPRESA) END " & vbNewLine
'----------------------------------------------------------------------
SQL = SQL & "     , FlagTpProposta = 1" & vbNewLine
SQL = SQL & "  From PROPOSTA_TB" & vbNewLine
SQL = SQL & "  Join RAMO_TB" & vbNewLine
SQL = SQL & "    ON RAMO_TB.RAMO_ID = PROPOSTA_TB.RAMO_ID" & vbNewLine
SQL = SQL & "  Join PROPOSTA_ADESAO_TB" & vbNewLine
SQL = SQL & "    ON PROPOSTA_ADESAO_TB.PROPOSTA_ID = PROPOSTA_TB.PROPOSTA_ID" & vbNewLine
SQL = SQL & " Where 1 = 1" & vbNewLine
SQL = SQL & "   AND PROPOSTA_TB.PRODUTO_ID IN " & PListaProdutos & vbNewLine
'----------------------------------------------------------------------
'Adicionando o X na situa��o (X - Representa propostas com ajustes da Tecnologia
'e que n�o devem ser processadas
'Alexandre - Confitec - 19/04/2011
If StrArquivo = "SEG987O" Then
    SQL = SQL & "   AND PROPOSTA_TB.SITUACAO not in ('T','X','E') " & vbNewLine
Else
    SQL = SQL & "   AND PROPOSTA_TB.SITUACAO not in ('T','X') " & vbNewLine
End If

'-----------------------------------------------------------------------
SQL = SQL & "   AND PROPOSTA_ADESAO_TB.PROPOSTA_BB = " & PPropostaBB & vbNewLine

SQL = SQL & "Union" & vbNewLine
SQL = SQL & "SELECT PROPOSTA_TB.PROPOSTA_ID" & vbNewLine
SQL = SQL & "     , PROPOSTA_TB.PRODUTO_ID" & vbNewLine
'Flow 19436313 - Confitec - Claudia.Silva - 15/09/2016
SQL = SQL & "     , COD_EMPRESA = CASE WHEN PROPOSTA_TB.PRODUTO_ID = 650 AND PROPOSTA_TB.SUBRAMO_ID IN (6507,1428,1434,1828,1834) THEN 'AB' ELSE RTRIM(RAMO_TB.COD_EMPRESA) END " & vbNewLine
'----------------------------------------------------------------------
SQL = SQL & "     , FlagTpProposta = 2" & vbNewLine
SQL = SQL & "  From PROPOSTA_TB" & vbNewLine
SQL = SQL & "  Join RAMO_TB" & vbNewLine
SQL = SQL & "    ON RAMO_TB.RAMO_ID = PROPOSTA_TB.RAMO_ID" & vbNewLine
SQL = SQL & "  Join PROPOSTA_FECHADA_TB" & vbNewLine
SQL = SQL & "    ON PROPOSTA_FECHADA_TB.PROPOSTA_ID = PROPOSTA_TB.PROPOSTA_ID" & vbNewLine
SQL = SQL & " Where 1 = 1" & vbNewLine
SQL = SQL & "   AND PROPOSTA_TB.PRODUTO_ID IN " & PListaProdutos & vbNewLine
SQL = SQL & "   AND PROPOSTA_TB.PRODUTO_ID NOT IN (1125,1141) " & vbNewLine
'----------------------------------------------------------------------
'Adicionando o X na situa��o (X - Representa propostas com ajustes da Tecnologia
'e que n�o devem ser processadas
'Alexandre - Confitec - 19/04/2011
If StrArquivo = "SEG987O" Then
    SQL = SQL & "   AND PROPOSTA_TB.SITUACAO not in ('T','X','E') " & vbNewLine
Else
SQL = SQL & "   AND PROPOSTA_TB.SITUACAO not in ('T','X') " & vbNewLine
End If
'-----------------------------------------------------------------------
SQL = SQL & "   AND PROPOSTA_FECHADA_TB.PROPOSTA_BB = " & PPropostaBB & vbNewLine


'-----------------------------------------------------------------------
'WBarboza/Fabreu - 2014-07-21
SQL = SQL & "Union" & vbNewLine
SQL = SQL & "SELECT PROPOSTA_TB.PROPOSTA_ID" & vbNewLine
SQL = SQL & "     , PROPOSTA_TB.PRODUTO_ID" & vbNewLine
'Flow 19436313 - Confitec - Claudia.Silva - 15/09/2016
SQL = SQL & "     , COD_EMPRESA = CASE WHEN PROPOSTA_TB.PRODUTO_ID = 650 AND PROPOSTA_TB.SUBRAMO_ID IN (6507,1428,1434,1828,1834) THEN 'AB' ELSE RTRIM(RAMO_TB.COD_EMPRESA) END " & vbNewLine
'----------------------------------------------------------------------
SQL = SQL & "     , FlagTpProposta = 2" & vbNewLine
SQL = SQL & "  From PROPOSTA_TB" & vbNewLine
SQL = SQL & "  Join RAMO_TB" & vbNewLine
SQL = SQL & "    ON RAMO_TB.RAMO_ID = PROPOSTA_TB.RAMO_ID" & vbNewLine
SQL = SQL & "  Join PROPOSTA_FECHADA_TB" & vbNewLine
SQL = SQL & "    ON PROPOSTA_FECHADA_TB.PROPOSTA_ID = PROPOSTA_TB.PROPOSTA_ID" & vbNewLine

If StrArquivo = "SEG987O" Then 's� dever� usar essa tabela quando for importa��o do arquivo SEG987O rtavares 01 08 2014
    SQL = SQL & "  Join ENDOSSO_RESUMO_TB" & vbNewLine
    SQL = SQL & "    ON ENDOSSO_RESUMO_TB.PROPOSTA_ID = PROPOSTA_TB.PROPOSTA_ID" & vbNewLine
End If

SQL = SQL & " Where 1 = 1" & vbNewLine
SQL = SQL & "   AND PROPOSTA_TB.PRODUTO_ID IN (1125,1141) " & vbNewLine
'----------------------------------------------------------------------
'Adicionando o X na situa��o (X - Representa propostas com ajustes da Tecnologia
'e que n�o devem ser processadas
'Alexandre - Confitec - 19/04/2011
If StrArquivo = "SEG987O" Then
    SQL = SQL & "   AND PROPOSTA_TB.SITUACAO not in ('T','X','E') " & vbNewLine
Else
SQL = SQL & "   AND PROPOSTA_TB.SITUACAO not in ('T','X') " & vbNewLine
End If
'-----------------------------------------------------------------------
If IsNull(PEndossoId) Or PEndossoId = "" Then PEndossoId = 0

SQL = SQL & "   AND PROPOSTA_FECHADA_TB.PROPOSTA_BB = " & PPropostaBB & vbNewLine

If StrArquivo = "SEG987O" Then 's� dever� usar essa tabela quando for importa��o do arquivo SEG987O rtavares 01 08 2014
    SQL = SQL & "   AND ENDOSSO_RESUMO_TB.ENDOSSO_RESUMO_ID = " & PEndossoId & vbNewLine
End If

SQL = SQL & "Union" & vbNewLine
SQL = SQL & "SELECT PROPOSTA_TB.PROPOSTA_ID" & vbNewLine
SQL = SQL & "     , PROPOSTA_TB.PRODUTO_ID" & vbNewLine
'Flow 19436313 - Confitec - Claudia.Silva - 15/09/2016
SQL = SQL & "     , COD_EMPRESA = CASE WHEN PROPOSTA_TB.PRODUTO_ID = 650 AND PROPOSTA_TB.SUBRAMO_ID IN (6507,1428,1434,1828,1834) THEN 'AB' ELSE RTRIM(RAMO_TB.COD_EMPRESA) END " & vbNewLine
'----------------------------------------------------------------------
SQL = SQL & "     , FlagTpProposta = 2" & vbNewLine
SQL = SQL & "  From PROPOSTA_TB" & vbNewLine
SQL = SQL & "  Join RAMO_TB" & vbNewLine
SQL = SQL & "    ON RAMO_TB.RAMO_ID = PROPOSTA_TB.RAMO_ID" & vbNewLine
SQL = SQL & "  Join EMI_RE_PROPOSTA_TB" & vbNewLine
SQL = SQL & "    ON EMI_RE_PROPOSTA_TB.PROPOSTA_ID = PROPOSTA_TB.PROPOSTA_ID" & vbNewLine
SQL = SQL & " Where 1 = 1" & vbNewLine
SQL = SQL & "   AND PROPOSTA_TB.PRODUTO_ID IN " & PListaProdutos & vbNewLine
'----------------------------------------------------------------------
'Adicionando o X na situa��o (X - Representa propostas com ajustes da Tecnologia
'e que n�o devem ser processadas
'Alexandre - Confitec - 19/04/2011
If StrArquivo = "SEG987O" Then
    SQL = SQL & "   AND PROPOSTA_TB.SITUACAO not in ('T','X','E') " & vbNewLine
Else
    SQL = SQL & "   AND PROPOSTA_TB.SITUACAO not in ('T','X') " & vbNewLine
End If
'-----------------------------------------------------------------------
SQL = SQL & "   AND EMI_RE_PROPOSTA_TB.PROPOSTA_BB = " & PPropostaBB & vbNewLine
SQL = SQL & " ORDER By PROPOSTA_TB.PROPOSTA_ID DESC " & vbNewLine

Set RstABS = rdocn_Seg2.OpenResultset(SQL)
   
If Not RstABS.EOF Then
    PPropostaId = RstABS!Proposta_Id
    PProduto = RstABS!produto_id
    PFlagTpProposta = RstABS!FlagTpProposta
    Pcod_empresa = RstABS!Cod_empresa
    RstABS.Close
    Exit Function
End If
  
    

Exit Function

TrataErro:
   TrataErroGeral "Obtem_Proposta", Me.name
   TerminaSEGBR

End Function


Function Valida_Remessa(PRemessa) As Boolean
On Error GoTo Trata_Erro
        
        Dim posponto As Integer
        
        Valida_Remessa = False
           
        If IsNumeric(PRemessa) = False Then
            obs = ""
            obs = obs & Space(50 - Len(obs))
            Grava_Operacao 0, position - 1, "4"
            grava_log
            term1.Visible = False
            Close #arqinput
            Exit Function
        End If
        
        posponto = InStr(lstarquivo.List(lstarquivo.ListIndex), ".")
        If ParRemessa <> Val(Mid(lstarquivo.List(lstarquivo.ListIndex), posponto + 1, 4)) Then
           obs = ""
           obs = obs & Space(50 - Len(obs))
           Grava_Operacao 0, position - 1, "4"
           grava_log
           term1.Visible = False
           Close #arqinput
           Exit Function
        End If
        
        Valida_Remessa = True

Exit Function

Trata_Erro:
   TrataErroGeral "Valida_Remessa", Me.name
   TerminaSEGBR

End Function

Private Sub BtnAtualizar_Click()
Close #filenum
Preenche_Grid
End Sub

Private Sub Command4_Click()
   Close #filenum
   Preenche_Grid
End Sub

Sub Preenche_Grid()

   Dim reclenght As Long
   Dim log As logfile
   Dim position As Long, lastrecord As Long
   Dim NomeArq As String
   
   On Error GoTo TrataErro
   
   grid1.Clear
   reclength = Len(log)
   filenum = FreeFile
   
   'Rotina para buscar nome do arquivo de log atual
   NomeArq = "LOGIMPAVALIACAORETORNOBB" & Format$(Date, "mmyyyy") & ".TXT"
   logpath = "C:\log_mq"
   Open logpath & "\" & NomeArq For Random As filenum Len = reclength
   position = 1
   grid1.Row = 0
   grid1.Col = 0
   grid1.ColWidth(0) = 1300
   grid1.Text = "Arquivo"
   grid1.Col = 1
   grid1.ColWidth(1) = 600
   grid1.Text = "Vers�o"
   grid1.Col = 2
   grid1.ColWidth(2) = 1600
   grid1.Text = "Status"
   grid1.Col = 3
   grid1.Text = "Usu�rio"
   grid1.Col = 4
   grid1.ColWidth(4) = 950
   grid1.Text = "Data Inicial"
   grid1.Col = 5
   grid1.ColWidth(5) = 900
   grid1.Text = "Hora Inicial"
   grid1.Col = 6
   grid1.ColWidth(6) = 950
   grid1.Text = "Data Final"
   grid1.Col = 7
   grid1.ColWidth(7) = 900
   grid1.Text = "Hora Final"
   grid1.Col = 8
   grid1.ColWidth(8) = 900
   grid1.Text = "Registros"
   grid1.Col = 9
   grid1.ColWidth(9) = 4600
   grid1.Text = "Observa��es"
   grid1.Rows = Int(LOF(filenum) / reclength) + 2
   Get #filenum, position, log
   Do
      
      grid1.Row = position
      grid1.Col = 0
      grid1.Text = log.arq
      grid1.Col = 1
      grid1.Text = log.ver
      grid1.Col = 2
      grid1.Text = log.status
      grid1.Col = 3
      grid1.Text = log.username
      grid1.Col = 4
      grid1.Text = log.data
      grid1.Col = 5
      grid1.Text = log.hora
      grid1.Col = 6
      grid1.Text = log.dataf
      grid1.Col = 7
      grid1.Text = log.horaf
      grid1.Col = 8
      grid1.Text = log.qtde
      grid1.Col = 9
      grid1.Text = log.obs
      position = position + 1
      Get #filenum, position, log
   Loop Until EOF(filenum)
   Close #filenum
  Open logpath & "\" & NomeArq For Append As filenum Len = reclength
Exit Sub
   
TrataErro:
    TrataErroGeral "Preenche_Grid", Me.name
    TerminaSEGBR
End Sub

Public Sub grava_log()
   
   arq = lblarq.Caption & Space(20 - Len(lblarq.Caption))
   ver = lblver.Caption & Space(5 - Len(lblver.Caption))
   sta = lblstatus.Caption & Space(20 - Len(lblstatus.Caption))
   cUsu = cUserName & Space(20 - Len(cUserName))
   dat = Format$(dat, "dd/mm/yyyy")
   hor = Format$(hor, "hh:mm:ss")
   datf = Format$(Date, "dd/mm/yyyy")
   horf = Format$(Time, "hh:mm:ss")
   linha = arq & ver & sta & cUsu & dat & hor & datf & horf & qtde & obs
   Print #1, linha
   
End Sub


Private Sub lstarquivo_Click()
On Error GoTo Trata_Erro
   lblarq.Caption = ""
   lblver.Caption = ""
   lblstatus.Caption = ""
   
   MousePointer = 11
   StatusBar1.SimpleText = "Verificando vers�o e status do arquivo"
   'Busca_Status

   'Verifica em arquivo_versao_tb se existe arquivo e vers�o
   rdocn.BeginTrans
  ' rdocn_Seg2.BeginTrans
   
   Call Verifica_Controle_Arquivo_RetornoBB(lstarquivo.FileName, BtnOk, lblarq, lblver, lblstatus, False)
   NomeArqRet = lstarquivo.List(lstarquivo.ListIndex)
   StatusBar1.SimpleText = ""
   MousePointer = 0
Exit Sub
   
Trata_Erro:
    TrataErroGeral "lstarquivo_Click", Me.name
    TerminaSEGBR

End Sub

'Mover o arquivo para outro diret�rio ap�s sua leitura
Function Move_Arquivo(Source As String, Destination As String) As Boolean

    On Error GoTo Err_Move_Arquivo
    
    'Copiar arquivo para origem
    FileCopy Source, Destination
    
    'Fechar o arquivo original
    'Close arqinput
    
    'Apagar o arquivo na origem
    Kill Source
    
    'Fim
    Move_Arquivo = True
    Exit Function
    
Err_Move_Arquivo:
    
    Move_Arquivo = False
    
End Function


Sub Grava_Operacao(ByVal totreg As String, ByVal position As Long, ByVal operacao As String)
' if operacao <> "", gravar esta operacao
' Utilizar operacao "" �para verificar erros no final do arrquivo, ou seja sem nu=enhuma exce��o (s� para trailler)
    
    On Error GoTo TrataErro
    
    qtde = Format$(position, "00000")
    If operacao = "" Then
          If Val(totreg) = position Then
                'Adenilson
                Move_Arquivo arqpath + "\" + StrArquivoVrs, arqpath + "\..\processados\" + StrArquivoVrs
                'FileCopy arqpath + "\" + lblarq.Caption, arqpath + "\..\processados\" + lblarq.Caption
                'Close arqinput
                'Kill arqpath + "\" + lblarq.Caption
                
                'lblver.Caption = Mid(lblarq.Caption, InStr(lblarq.Caption, ".") + 1)
                BtnOk.Enabled = False
 
                rdocn.CommitTrans
                rdocn_Seg2.CommitTrans
                Flag_Erro_Trailler = False
                
                'Finalizando agenda
                 goProducao.AdicionaLog 2, totreg, lstarquivo.ListIndex + 1
                 
                'grava log arquivo CTM
                If CTM = True Then
                    Call GravaLogCTM("OK - ", "", "", totreg)
                End If

                 
                'SQL = grava_status("1")
                lblstatus.Caption = "Conclu�do"
          Else
                ' o arquivo veio com registros diferentes dos constantes no trailler
                'SQL = grava_status("0")
                lblstatus.Caption = "Erro Trailler"
                'Adenilson
                Move_Arquivo arqpath + "\" + StrArquivoVrs, arqpath + "\..\pendentes\" + StrArquivoVrs
                'lblver.Caption = Mid(lblarq.Caption, InStr(lblarq.Caption, ".") + 1)
                rdocn.RollbackTrans
                rdocn_Seg2.RollbackTrans
                Flag_Erro_Trailler = True
          End If

        'Confitec Sistemas - Diogo Ferreira 17/01/2012 - Demanda 13981228: Inclus�o de mensagem de erro no programa para recusas do conta cart�o
        If bRecusaContaCartao = True And (StrArquivo = "SEGD987" Or StrArquivo = "SEGE987") Then
            bRecusaContaCartao = False
            bRecusaContaCartaoTarefa = True
            MensagemBatch "Houve recusa do arquivo " & StrArquivoVrs
        End If

    Else
    
        'Vefiricar o tipo de opera��o
        Select Case operacao
        
        Case "1"
            lblstatus.Caption = "Processado com Erro"
            'Adenilson
            Move_Arquivo arqpath + "\" + StrArquivoVrs, arqpath + "\..\pendentes\" + StrArquivoVrs
            
        Case "2"
            qtde = "     "
            lblstatus.Caption = "Carac. inv�lidos"
            
        Case "3"
            lblstatus.Caption = "Erro enquadramento"
             
        Case "4"
            lblstatus.Caption = "Vers�o inv�lida"
                
        Case "5"
            lblstatus.Caption = "D�bito inv�lido"
              
        Case "6"
            lblstatus.Caption = "Importacao abortada"
            
        Case "7"
            lblstatus.Caption = "C�digo inv�lido"
            
        Case "8"
            lblstatus.Caption = "Retorno inv�lido"
            
        Case "9"
            lblstatus.Caption = "Campo inv�lido"
            
        Case "10"
            lblstatus.Caption = "Arq fora de Ordem"
            'Martines
            Move_Arquivo arqpath + "\" + StrArquivoVrs, arqpath + "\..\pendentes\" + StrArquivoVrs
        
        Case "11"
            lblstatus.Caption = "Arq j� foi importado"
            'Martines
            Move_Arquivo arqpath + "\" + StrArquivoVrs, arqpath + "\..\pendentes\" + StrArquivoVrs

        End Select
    
        'Maur�cio(Stefanini), em 07/10/2005 - Se algum arquivo n�o processar corretamente,
        'programa grava processamento com erro.
        flag_todosprocessados = False
    
        rdocn.RollbackTrans
        If operacao <> "10" And operacao <> "11" Then
            MensagemBatch lblstatus.Caption
        End If
    End If
    
'    Set rc = rdocn.OpenResultset(SQL)
'    Set rc = Nothing
Exit Sub
   
TrataErro:
    TrataErroGeral "Grava_Operacao", Me.name
    TerminaSEGBR
End Sub

Function grava_status(tipo As String) As String
On Error GoTo Trata_Erro
        If lblstatus.Caption = "n�o processado" Then 'inser��o no vers�o_proposta_tb
             SQL = "exec impbb_atualiza_versao_spi '"
             'SQL = SQL & lblarq.Caption & "', " & Mid(lblarq.Caption, InStr(lblarq.Caption, ".") + 1) & ", '" & tipo & "','" & cUserName & "'"
             SQL = SQL & lblarq.Caption & "', " & lblver.Caption & ", '" & tipo & "','" & cUserName & "'"
        Else
             'SQL = " exec impbb_atualiza_versao_spu '" & tipo & "', '" & lblarq.Caption & "', " & Mid(lblarq.Caption, InStr(lblarq.Caption, ".") + 1) & ",'" & cUserName & "'"
             SQL = " exec impbb_atualiza_versao_spu '" & tipo & "', '" & lblarq.Caption & "', " & lblver.Caption & ",'" & cUserName & "'"
        End If
        grava_status = SQL
Exit Function
   
Trata_Erro:
    TrataErroGeral "grava_status", Me.name
    TerminaSEGBR
        
End Function

Function Arquivo_invalido() As Boolean
   
   Dim binario As Integer, Str As String
   
   On Error GoTo TrataErro
   
   F = FreeFile
   Open lstarquivo.PATH & "\" & lstarquivo.FileName For Binary As F
   tam_arquivo = LOF(F)
   position = 0
   Arquivo_invalido = False
   term1.Min = 0
   term1.Max = tam_arquivo
   term1.Visible = True
   
   Do While position < tam_arquivo
   
        DoEvents
        position = position + 1
        term1.Value = position
        car = Input(1, #F)
        binario = Asc(car)
        'Excetuando-se o <ENTER> e o <LINE FEED> , qualquer outro caracter especial ser� verificado
        If (binario <> 13 And binario <> 10) And (binario >= 0 And binario <= 31) Then
            term1.Value = tam_arquivo
            obs = "ASCII = " & binario & " representado como : " & Chr(binario) & Space(50)
            obs = Mid(obs, 1, 50)
            Grava_Operacao 0, position, "2"
            grava_log
            Arquivo_invalido = True
            Exit Do
        End If
   Loop
   term1.Visible = False
   Close #F
Exit Function
   
TrataErro:
    TrataErroGeral "arquivo_invalido", Me.name
    TerminaSEGBR

End Function

Private Function Verifica_Reincidencia(ByVal PropostaId As String, ByVal EndossoId As String, ByVal Cod_empresa As String) As Boolean
On Erro GoTo TrataErro
Dim Rst As rdoResultset
Dim SQL As String
On Error GoTo TrataErro

SQL = "SELECT PROPOSTA_ID,ENDOSSO_ID"
SQL = SQL & " FROM AVALIACAO_RETORNO_BB_TB  WITH (NOLOCK)  "
SQL = SQL & " WHERE PROPOSTA_ID = " & PropostaId
SQL = SQL & " AND ENDOSSO_ID "
If Val(EndossoId) = 0 Then
    SQL = SQL & "is NULL"
Else
    SQL = SQL & "= " & EndossoId
End If


If Cod_empresa = "AB" Then
    Set Rst = rdocn.OpenResultset(SQL)
End If

'Gmarques
If Cod_empresa = "ABS" Then
    Set Rst = rdocn_Seg2.OpenResultset(SQL)
End If


If Not Rst.EOF Then
    Verifica_Reincidencia = True
Else
    Verifica_Reincidencia = False
End If

Rst.Close

Exit Function

TrataErro:
   TrataErroGeral "Verifica_Reincidencia: " & tipo, Me.name
   TerminaSEGBR
End Function

Sub Verifica_Controle_Arquivo_RetornoBB(ByVal arquivo1 As String, cmdOk As Object, lblarq As Object, lblver As Object, lblstatus As Object, flag As Boolean, Optional UnloadForm As Object = Nothing)
    Dim versao As Integer, max_versao As Integer
    Dim rc As rdoResultset, SQL As String
    Dim Arquivo As String

    On Error GoTo TrataErro

    'Captura nome do arquivo correspondente
    versao = Val(Mid(arquivo1, InStr(1, arquivo1, ".") + 1))
    Arquivo = Mid(arquivo1, 1, InStr(1, arquivo1, ".") - 1)
    'Tratamento 987 - Arquivo SEG987N (Controle de vers�o Retorno BB Reinicializado) - Flavio Abreu - Pm00000203 - 2018-12-04
    If (Arquivo = "SEG987N") Then
       versao = Val("1" + Mid(arquivo1, InStr(1, arquivo1, ".") + 1))
    End If
         
    'ECOSTA 11/2008
    StrArquivo = Arquivo
    StrArquivoVrs = arquivo1
    
    'teste do arquivo
    '------------------------------------
    
'    SQL = "      SELECT"
'    SQL = SQL & "   max(versao) "
'    SQL = SQL & "FROM"
'    SQL = SQL & "   controle_proposta_db..arquivo_versao_recebido_tb  (nolock) "
'    SQL = SQL & "WHERE"
'    SQL = SQL & "   layout_id = (SELECT"
'    SQL = SQL & "                   layout_id "
'    SQL = SQL & "                FROM"
'    SQL = SQL & "                   controle_proposta_db..layout_tb  (nolock) "
'    SQL = SQL & "                WHERE"
'    SQL = SQL & "                   nome = '" & Arquivo & "') AND"
'    SQL = SQL & "   dt_ent_segbr is not null"
    
    SQL = ""
    SQL = SQL & "select max(versao) versao" & vbNewLine
    SQL = SQL & "  from (SELECT max(versao) versao" & vbNewLine
    SQL = SQL & "          From controle_proposta_db.dbo.arquivo_versao_recebido_tb WITH (NOLOCK) " & vbNewLine
    SQL = SQL & "         WHERE layout_id = (SELECT layout_id" & vbNewLine
    SQL = SQL & "                              From controle_proposta_db.dbo.layout_tb WITH (NOLOCK) " & vbNewLine
    SQL = SQL & "                             WHERE nome = '" & Arquivo & "')" & vbNewLine
    SQL = SQL & "           AND dt_ent_segbr is not null" & vbNewLine
    SQL = SQL & "        Union" & vbNewLine
    SQL = SQL & "        SELECT max(versao) versao" & vbNewLine
    SQL = SQL & "          From controle_proposta_db.dbo.arquivo_versao_recebido_tb WITH (NOLOCK) " & vbNewLine
    SQL = SQL & "         WHERE layout_id = (SELECT layout_id" & vbNewLine
    SQL = SQL & "                              From controle_proposta_db.dbo.layout_tb WITH (NOLOCK) " & vbNewLine
    SQL = SQL & "                             WHERE nome = '" & Arquivo & "')" & vbNewLine
    SQL = SQL & "           AND dt_ent_segbr is null) as versoes" & vbNewLine
    Set rc = rdocn.OpenResultset(SQL)
    
    max_versao = IIf(IsNull(rc(0)), 0, rc(0))
    
    Set rc = Nothing
        
    If max_versao = versao Then 'J� foi importado
        
        lblarq.Caption = StrArquivo   'arquivo1
        lblver.Caption = versao
        lblstatus.Caption = "Conclu�do"
        Call MensagemBatch("Arquivo j� importado!")
        
        '------------------------------------------------------------------------------
        ' joconceicao - 29maio2001
        ' encerra a execu��o se for autom�tica
        If Not goProducao Is Nothing Then
            Call Grava_Operacao(0, 0, 11)
            grava_log
        Else
            cmdOk.Enabled = False
        End If
        Exit Sub
    Else
            
        'carsilva - 08/06/2004 - Altera��o para importar os arquivos SEG987T, mesmo fora de ordem
        'ecosta 17/01/07 importar Seg987S e Seg987Z fora de ordem (cons�rcio)
        ' Bruno S Moraes 25/07/2007 - importar arquivos do conta cart�o SEGD987 e SEGE987

'        If (max_versao = versao - 1) Or (Left(arquivo1, 7) = "SEG987N") Or (Left(arquivo1, 7) = "SEG987T") _
'          Or (Left(arquivo1, 7) = "SEG987S") Or (Left(arquivo1, 7) = "SEG987Z") _
'          Or (Left(arquivo1, 7) = "SEGD987") Or (Left(arquivo1, 7) = "SEGE987") Then  'N�o foi importado

'   if acima trocado pelo if abaixo - ecosta 18/09/08 arquivo com 8digitos e inclus�o seg987aa (cons imob)
        'Confitec Sistemas - Flavio Abreu - Inclus�o dos arquivos:SEG987SS e SEG987SSS.
        'PAULO PELEGRINI - 27/06/2016 - 19306878 - Incus�o do arquivo: SEG987X
        If (max_versao = versao - 1) Or (StrArquivo = "SEG987N") Or (StrArquivo = "SEG987T") _
          Or (StrArquivo = "SEG987S") Or (StrArquivo = "SEG987SS") Or (StrArquivo = "SEG987SSS") Or (StrArquivo = "SEG987Z") Or (StrArquivo = "SEG987AA") _
          Or (StrArquivo = "SEGD987") Or (StrArquivo = "SEGE987") Or (StrArquivo = "SEG987X") Then 'N�o foi importado
            cmdOk.Enabled = True
            flag = True
            lblarq.Caption = StrArquivo 'arquivo1
            lblver.Caption = versao
            lblstatus.Caption = "N�o processado"
            
        Else 'Arquivo fora de ordem
                
            Call MensagemBatch("Arquivo fora de ordem!" & Chr(10) & Chr(13) & _
            "Arquivo esperado: " & Arquivo & "." & Format((max_versao + 1), "0000"), vbCritical)
            
            '------------------------------------------------------------------------------
            ' joconceicao - 29maio2001
            ' encerra a execu��o se for autom�tica
            flag = False
            lblarq.Caption = StrArquivo 'arquivo1
            lblver.Caption = versao
            lblstatus.Caption = "Fora de ordem"
                
            If Not goProducao Is Nothing Then
                Call Grava_Operacao(0, 0, 10)
                grava_log
            Else
                cmdOk.Enabled = False
            End If
                
            Exit Sub
                
        End If
                
'        SQL = "      SELECT"
'        SQL = SQL & "     dt_ent_segbr "
'        SQL = SQL & "FROM"
'        SQL = SQL & "     controle_proposta_db..arquivo_versao_recebido_tb  (nolock) "
'        SQL = SQL & "WHERE"
'        SQL = SQL & "     versao = " & versao & " AND"
'        SQL = SQL & "     layout_id = (SELECT"
'        SQL = SQL & "                       layout_id "
'        SQL = SQL & "                  FROM"
'        SQL = SQL & "                       controle_proposta_db..layout_tb  (nolock) "
'        SQL = SQL & "                  WHERE"
'        SQL = SQL & "                       nome = '" & Arquivo & "')"
        
        
        SQL = ""
        SQL = SQL & "select dt_ent_segbr" & vbNewLine
        SQL = SQL & "  from (SELECT dt_ent_segbr" & vbNewLine
        SQL = SQL & "          FROM controle_proposta_db.dbo.arquivo_versao_recebido_tb   WITH (NOLOCK) " & vbNewLine
        SQL = SQL & "         Where versao = " & versao & vbNewLine
        SQL = SQL & "           AND layout_id = (SELECT layout_id" & vbNewLine
        SQL = SQL & "                              FROM controle_proposta_db.dbo.layout_tb   WITH (NOLOCK) " & vbNewLine
        SQL = SQL & "                             WHERE nome = '" & Arquivo & "')" & vbNewLine
        SQL = SQL & "         Union" & vbNewLine
        SQL = SQL & "        SELECT dt_ent_segbr" & vbNewLine
        SQL = SQL & "          FROM controle_proposta_db.dbo.arquivo_versao_recebido_tb   WITH (NOLOCK) " & vbNewLine
        SQL = SQL & "         Where versao = " & versao & vbNewLine
        SQL = SQL & "           AND layout_id = (SELECT layout_id" & vbNewLine
        SQL = SQL & "                              FROM controle_proposta_db.dbo.layout_tb   WITH (NOLOCK) " & vbNewLine
        SQL = SQL & "                             WHERE nome = '" & Arquivo & "')) as dt_ent_segbr" & vbNewLine
        
        Set rc = rdocn.OpenResultset(SQL)

        If rc.EOF Then
                
            'Insere em arquivo_versao_recebido_tb
            SQL = "exec controle_proposta_db.dbo.inclui_arquivos_versao_spi "
            SQL = SQL & "null, null,'" & Arquivo & "'," & versao & ",'"
            SQL = SQL & Format(Data_Sistema, "yyyymmdd") & "','"
            SQL = SQL & Format(Data_Sistema, "yyyymmdd") & "',0,'" & cUserName & "'"
            Call rdocn.OpenResultset(SQL)
            
            'Call rdocn_Seg2.OpenResultset(SQL)
        
        Else
            
            If Not IsNull(rc("dt_ent_segbr")) Then
                Call MensagemBatch("Arquivo j� importado anteriormente!", vbCritical)
                cmdOk.Enabled = False
                lblstatus.Caption = "Fora de ordem - Conclu�do"
            End If
            
        End If
        
    End If

Exit Sub
    
TrataErro:
   TrataErroGeral "Verifica_Controle_Arquivo_RetornoBB ", Me.name
   TerminaSEGBR

End Sub

'Confitec Sistemas - Diogo Ferreira 20/04/2011 - Flow 10162776: Tratamento AB/ABS
Private Sub AtualizarControleMesReferencia(ByVal sAno_mes_ref As String, _
                                           ByVal lPropostaId As Long, _
                                           ByVal sVersaoRetorno As String, _
                                           ByVal sCod_empresa As String)
'Private Sub AtualizarControleMesReferencia(ByVal sAno_mes_ref As String, _
'                                           ByVal lPropostaId As Long, _
'                                           ByVal sVersaoRetorno As String)

' Abosco - 2004 abr 20
' Caso a proposta/endosso fa�a parte de uma fatura resumo do produto
' conta cartao ent�o o flag sinistro_liberado_987 da tabela
' controle_mes_referencia_tb do banco conta_cartao_db ser� atualizado como 's'

On Error GoTo TrataErro

Dim sSQL As String
Dim rs As rdoResultset
Dim lPlanoId As Long

      'Obtendo o plano
       
      sSQL = "SELECT plano_apolice_tb.plano_id" & vbNewLine
      sSQL = sSQL & "FROM conta_cartao_db..plano_apolice_tb plano_apolice_tb" & vbNewLine
      sSQL = sSQL & "JOIN apolice_tb" & vbNewLine
      sSQL = sSQL & "  ON apolice_tb.apolice_id = plano_apolice_tb.apolice_id" & vbNewLine
      sSQL = sSQL & " AND apolice_tb.ramo_id = plano_apolice_tb.ramo_id" & vbNewLine
      sSQL = sSQL & "WHERE apolice_tb.proposta_id = " & lPropostaId & vbNewLine
      sSQL = sSQL & "  AND plano_apolice_tb.dt_inicio_vigencia <= '" & Format(Data_Sistema, "yyyymmdd") & "'" & vbNewLine
      sSQL = sSQL & "  AND (plano_apolice_tb.dt_fim_vigencia >= '" & Format(Data_Sistema, "yyyymmdd") & "'" & vbNewLine
      sSQL = sSQL & "       OR plano_apolice_tb.dt_fim_vigencia IS NULL)"
      

'Confitec Sistemas - Diogo Ferreira 20/04/2011 - Flow 10162776: Tratamento AB/ABS
      'Set rs = rdocn.OpenResultset(sSQL)
      If sCod_empresa = "AB" Then
          Set rs = rdocn.OpenResultset(sSQL)
      ElseIf sCod_empresa = "ABS" Then
          Set rs = rdocn_Seg2.OpenResultset(sSQL)
      End If
      
      lPlanoId = rs(0)
      
      sSQL = "EXEC conta_cartao_db..atualiza_sinistro_liberado_987_spu "
      sSQL = sSQL & "'" & sVersaoRetorno & "',"
      sSQL = sSQL & lPlanoId & ","
      sSQL = sSQL & "'" & cUserName & "'," '@usuario
      sSQL = sSQL & "'" & sAno_mes_ref & "'"
     
      

'Confitec Sistemas - Diogo Ferreira 20/04/2011 - Flow 10162776: Tratamento AB/ABS
      'Call rdocn.Execute(sSQL)
      If sCod_empresa = "AB" Then
          Call rdocn.Execute(sSQL)
      ElseIf sCod_empresa = "ABS" Then
          Call rdocn_Seg2.Execute(sSQL)
      End If
        

Exit Sub

TrataErro:
   TrataErroGeral "Atualizar", Me.name
   TerminaSEGBR


End Sub

Private Function VerificarFaturaResumo(sPropostaId As String, sEndossoId As String) As Boolean

' Abosco - 2004 abr 20
' Verifica se uma proposta/endosso � de uma fatura resumo do produto conta cartao

On Error GoTo TrataErro

Dim sSQL As String
Dim rsFaturaResumo As rdoResultset

    sSQL = "EXEC Verifica_Retorno_Fatura_Resumo_sps "
    sSQL = sSQL & sPropostaId & ","
    sSQL = sSQL & sEndossoId
    
    Set rsFaturaResumo = rdocn.OpenResultset(sSQL)
    
    If rsFaturaResumo.EOF Then
        GoTo TrataErro
    Else
        If Val(rsFaturaResumo(0)) = 1 Then
            'pertence a uma fatura resumo
            VerificarFaturaResumo = True
        Else
            VerificarFaturaResumo = False
        End If
    End If
    
    Set rsFaturaResumo = Nothing
Exit Function

TrataErro:
   TrataErroGeral "VerificarFaturaResumo ", Me.name
   TerminaSEGBR

End Function

Private Function ConsultarPropostaRenovacaoMonitorada(ByVal lPropostaId As Long, ByVal lCod_empresa As String) As Boolean

Dim oRenovacao As Object
Dim rs As Object

On Error GoTo Erro

    ConsultarPropostaRenovacaoMonitorada = False

    Set oRenovacao = CreateObject("SEGL0026.cls00168")


    If lCod_empresa = "AB" Then
    Set rs = oRenovacao.ConsultarProposta(gsSIGLASISTEMA, _
                                          App.Title, _
                                          App.FileDescription, _
                                          glAmbiente_id, _
                                          lPropostaId)
    End If
    
    
    If lCod_empresa = "ABS" Then
        Set rs = oRenovacao.ConsultarProposta(gsSIGLASISTEMA, _
                                              App.Title, _
                                              App.FileDescription, _
                                              glAmbiente_id_seg2, _
                                              lPropostaId)
                                          
    End If
    
    If Not rs.EOF Then
        ConsultarPropostaRenovacaoMonitorada = True
    End If
    
    Set rs = Nothing
    Set oRenovacao = Nothing

    Exit Function

Erro:
    Call TrataErroGeral("ConsultarPropostaRenovacaoMonitorada", Me.name)
    Call TerminaSEGBR
End Function

Private Sub IncluirEventoRenovacaoMonitorada(ByVal lPropostaId As Long, ByVal lCod_empresa As String, Optional ByVal lEndossoId As Integer)

Dim oRenovacao As Object
Const EVENTO_RENOVACAO_ACEITA_BB = 16

On Error GoTo Erro

    Set oRenovacao = CreateObject("SEGL0026.cls00168")

    If lCod_empresa = "AB" Then
        Call oRenovacao.IncluirEventoProposta(gsSIGLASISTEMA, _
                                              App.Title, _
                                              App.FileDescription, _
                                              glAmbiente_id, _
                                              lPropostaId, _
                                              lEndossoId, _
                                              EVENTO_RENOVACAO_ACEITA_BB, _
                                              Data_Sistema, _
                                              cUserName)
    End If
    
    If lCod_empresa = "ABS" Then
        Call oRenovacao.IncluirEventoProposta(gsSIGLASISTEMA, _
                                              App.Title, _
                                              App.FileDescription, _
                                              glAmbiente_id_seg2, _
                                              lPropostaId, _
                                              lEndossoId, _
                                              EVENTO_RENOVACAO_ACEITA_BB, _
                                              Data_Sistema, _
                                              cUserName)
    End If


    Set oRenovacao = Nothing

    Exit Sub

Erro:
    Call TrataErroGeral("IncluirEventoRenovacaoMonitorada", Me.name)
    Call TerminaSEGBR
End Sub


Private Function EndossodeRenovacao(ByVal lPropostaId As Long, ByVal lEndossoId As Integer, ByVal lCod_empresa As String) As Boolean

EndossodeRenovacao = False

On Error GoTo Erro

                
    If lCod_empresa = "AB" Then
        SQL = "SELECT "
        SQL = SQL & "   tp_endosso_id"
        SQL = SQL & " FROM "
        SQL = SQL & "   endosso_tb  WITH (NOLOCK)  "
        SQL = SQL & " WHERE proposta_id = " & lPropostaId
        SQL = SQL & " AND endosso_id = " & lEndossoId
        Set Rst = rdocn.OpenResultset(SQL)
    End If
            
    If lCod_empresa = "ABS" Then
        SQL = "SELECT "
        SQL = SQL & "   tp_endosso_id"
        SQL = SQL & " FROM "
        SQL = SQL & "   endosso_tb  WITH (NOLOCK)  "
        SQL = SQL & " WHERE proposta_id = " & lPropostaId
        SQL = SQL & " AND endosso_id = " & lEndossoId
        Set Rst = rdocn_Seg2.OpenResultset(SQL)
    End If
            
            
    If Not Rst.EOF Then
        If Rst("tp_endosso_id") = 250 Then
            EndossodeRenovacao = True
        End If
    End If
    
    Rst.Close
    
    Exit Function

Erro:
    Call TrataErroGeral("EndossodeRenovacao", Me.name)
    Call TerminaSEGBR
End Function

'' LGOMES - 04-02-2019 - Substituindo rotina �nica de apura��o do 987 por duas independentes.
''Sub Insere_Acompanhamento_987()
''
''Dim SQL As String
''Dim Rst As rdoResultset
''
''On Error GoTo TrataErro
''
''    lblstatus.Caption = "Carregando acompanhamento_987_tb"
''
''    rdocn.BeginTrans
''   rdocn_Seg2.BeginTrans
''
''
''    SQL = "Exec SEGS9157_SPI " & vbNewLine
''    SQL = SQL & "'" & cUserName & "'"
''
''    Set Rst = rdocn.OpenResultset(SQL)
''    Set Rst = rdocn_Seg2.OpenResultset(SQL)
''
''    rdocn.CommitTrans
''    rdocn_Seg2.CommitTrans
''
''    Set Rst = Nothing
''
''    lblstatus.Caption = "Conclu�do"
''    BtnOk.Enabled = False
''
''    Exit Sub
''
''TrataErro:
''    rdocn.RollbackTrans
''    rdocn_Seg2.RollbackTrans
''    TrataErroGeral "Insere_Acompanhamento_987 " & SQL, Me.name
''    Call TerminaSEGBR
''
''End Sub

Sub Insere_Acompanhamento_987_AB()

Dim SQL As String
Dim Rst As rdoResultset

On Error GoTo TrataErro

    lblstatus.Caption = "Carregando acompanhamento_987_tb AB"
    
    rdocn.BeginTrans
    
    SQL = "Exec SEGS9157_SPI " & vbNewLine
    SQL = SQL & "'" & cUserName & "'"

    Set Rst = rdocn.OpenResultset(SQL)

    rdocn.CommitTrans
    
    Set Rst = Nothing
    
    lblstatus.Caption = "AB Conclu�do."
    BtnOk.Enabled = False
    
    Exit Sub

TrataErro:
    rdocn.RollbackTrans
        Call GravaLogCTM("Erro - ", Time & " - " & Err.Number & " - " & Err.Description, i, "")
        Resume Next
    ''TrataErroGeral "Insere_Acompanhamento_987 AB " & SQL, Me.name
    ''Call TerminaSEGBR

End Sub

Sub Insere_Acompanhamento_987_ABS()

Dim SQL As String
Dim Rst As rdoResultset

On Error GoTo TrataErro

    lblstatus.Caption = "Carregando acompanhamento_987_tb ABS"
    
    rdocn_Seg2.BeginTrans
    
    SQL = "Exec SEGS9157_SPI " & vbNewLine
    SQL = SQL & "'" & cUserName & "'"

    Set Rst = rdocn_Seg2.OpenResultset(SQL)

    rdocn_Seg2.CommitTrans
    
    Set Rst = Nothing
    
    lblstatus.Caption = "ABS Conclu�do."
    BtnOk.Enabled = False
    
    Exit Sub

TrataErro:
    rdocn_Seg2.RollbackTrans
        Call GravaLogCTM("Erro - ", Time & " - " & Err.Number & " - " & Err.Description, i, "")
        Resume Next
    ''TrataErroGeral "Insere_Acompanhamento_987 ABS " & SQL, Me.name
    ''Call TerminaSEGBR

End Sub


Private Sub Importa_Avaliacoes_Retorno_Cartao_credito_BB()

' vapinto -  5157755 - trata retorno arquivo 549 - 15022010
   Call AtualizarRepasseCartaoCredito(lstarquivo.FileName, lstarquivo.PATH & "\" & lstarquivo.FileName)

End Sub

Private Sub AtualizarRepasseCartaoCredito(ByVal p_sArquivo As String, ByVal p_sCaminho As String)

' vapinto -  5157755 - trata retorno arquivo 549 - 15022010
' Atualizar o status de aceite da tabela repasse_cartao_credito_tb

On Error GoTo TrataErro

Dim sSQL As String
Dim Rst As rdoResultset
Dim Rst_aux As rdoResultset

    lblstatus.Caption = "Processando"

    rdocn.BeginTrans
    rdocn_Seg2.BeginTrans
    
    'Alice Rodrigues - 03/05/2011 - Cria��o da procedure de verifica��o ambiente AB/ABS
    
    sSQL = "EXEC seguros_db..SEGS9566_SPS "
    sSQL = sSQL & "'" & p_sArquivo & "',"
    sSQL = sSQL & "'" & p_sCaminho & "'"
    
    Set Rst_aux = rdocn.OpenResultset(sSQL)

    If Mid(p_sArquivo, 1, 3) = "ALS" Then
        sSQL = "EXEC seguros_db..SEGS9404_SPI "
    Else
        sSQL = "EXEC seguros_db..SEGS9285_SPI "
    End If
    
    sSQL = sSQL & "'" & p_sArquivo & "',"
    sSQL = sSQL & "'" & cUserName & "'," '@usuario
    sSQL = sSQL & "'" & p_sCaminho & "'"
    
    If Rst_aux("Cod_Empresa") = "AB" Then
        Set Rst = rdocn.OpenResultset(sSQL)
    ElseIf Rst_aux("Cod_Empresa") = "ABS" Then
        Set Rst = rdocn_Seg2.OpenResultset(sSQL)
    Else 'AB
        Set Rst = rdocn.OpenResultset(sSQL)
    End If
    
    lblstatus.Caption = "Movendo arquivo"
    Move_Arquivo arqpath + "\" + StrArquivoVrs, arqpath + "\..\processados\" + StrArquivoVrs
    goProducao.AdicionaLog 2, Rst("Total_Registro_processados"), lstarquivo.ListIndex + 1
    
                'grava log arquivo CTM
                If CTM = True Then
                    Call GravaLogCTM("OK - ", "", "", Rst("Total_Registro_processados"))
                End If
    
    Call Atualiza_Controle_Arquivo(StrArquivoVrs, lblver.Caption, Rst("Total_Registro_arquivo"), Rst("Total_Registro_processados"))
    Flag_Erro_Trailler = False
          
    rdocn.CommitTrans
    rdocn_Seg2.CommitTrans
   
    Set Rst = Nothing
    Set Rst_aux = Nothing
    
    lblstatus.Caption = "Conclu�do"
    BtnOk.Enabled = False
    grava_log
    
Exit Sub
'Resume
TrataErro:
    Move_Arquivo arqpath + "\" + StrArquivoVrs, arqpath + "\..\pendentes\" + StrArquivoVrs
    rdocn.RollbackTrans
    rdocn_Seg2.RollbackTrans
    TrataErroGeral "AtualizarRepasseCartaoCredito", Me.name
    Call TerminaSEGBR

End Sub

Private Sub Importa_Avaliacoes_Retorno_544()

' LCBG - 28/03/2011 - trata retorno arquivo 544
   Call Atualizar_Retorno_544(lstarquivo.FileName, lstarquivo.PATH & "\" & lstarquivo.FileName)

End Sub

Private Sub Atualizar_Retorno_544(ByVal p_sArquivo As String, ByVal p_sCaminho As String)

' LCBG - 28/03/2011 - trata retorno arquivo 544
' Atualizar o status de aceite da tabela arquivo_enviado_544_tb

On Error GoTo TrataErro

Dim sSQL As String
Dim Rst As rdoResultset
Dim Rst_aux As rdoResultset

    lblstatus.Caption = "Processando"

    rdocn.BeginTrans
    rdocn_Seg2.BeginTrans
    
    'Alice Rodrigues - 09/05/2011 - Cria��o da procedure de verifica��o ambiente AB/ABS
    sSQL = "EXEC seguros_db..SEGS9566_SPS "
    sSQL = sSQL & "'" & p_sArquivo & "',"
    sSQL = sSQL & "'" & p_sCaminho & "'"
    
    Set Rst_aux = rdocn.OpenResultset(sSQL)

    If Mid(p_sArquivo, 1, 3) = "ALS" Then
     sSQL = "EXEC seguros_db..SEGS9512_SPU "
    Else 'BBC ou SEG
     sSQL = "EXEC seguros_db..SEGS9513_SPU "
    End If
    sSQL = sSQL & "'" & p_sArquivo & "',"
    sSQL = sSQL & "'" & cUserName & "'," '@usuario
    sSQL = sSQL & "'" & p_sCaminho & "'"
    
    If Rst_aux("Cod_Empresa") = "AB" Then
        Set Rst = rdocn.OpenResultset(sSQL)
    ElseIf Rst_aux("Cod_Empresa") = "ABS" Then
        Set Rst = rdocn_Seg2.OpenResultset(sSQL)
    Else 'AB
        Set Rst = rdocn.OpenResultset(sSQL)
    End If
    
    'Set Rst = rdocn.OpenResultset(sSQL)
    
    lblstatus.Caption = "Movendo arquivo"
    
    goProducao.AdicionaLog 2, Rst("Total_Registro_processados"), lstarquivo.ListIndex + 1
    
                'grava log arquivo CTM
                If CTM = True Then
                    Call GravaLogCTM("OK - ", "", "", Rst("Total_Registro_processados"))
                End If
    
    Call Atualiza_Controle_Arquivo(StrArquivoVrs, lblver.Caption, Rst("Total_Registro_arquivo"), Rst("Total_Registro_processados"))
    Flag_Erro_Trailler = False
          
    rdocn.CommitTrans
    rdocn_Seg2.CommitTrans
   
    Move_Arquivo arqpath + "\" + StrArquivoVrs, arqpath + "\..\processados\" + StrArquivoVrs
   
    Set Rst = Nothing
    
    lblstatus.Caption = "Conclu�do"
    BtnOk.Enabled = False
    grava_log
    
Exit Sub

TrataErro:
    Move_Arquivo arqpath + "\" + StrArquivoVrs, arqpath + "\..\pendentes\" + StrArquivoVrs
    rdocn.RollbackTrans
    rdocn_Seg2.RollbackTrans
    TrataErroGeral "Atualizar_Retorno_544", Me.name
    Call TerminaSEGBR

End Sub

