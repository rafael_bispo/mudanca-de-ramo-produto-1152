VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.2#0"; "MSCOMCTL.OCX"
Begin VB.Form frmGeraLancContabeis 
   Caption         =   "SEG0870 - Gera Lançamentos Contábeis - "
   ClientHeight    =   6450
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   9075
   Icon            =   "frmGeraLancContabeis.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   6450
   ScaleWidth      =   9075
   StartUpPosition =   3  'Windows Default
   Begin VB.Frame Frame2 
      Height          =   705
      Left            =   90
      TabIndex        =   16
      Top             =   5445
      Width           =   8985
      Begin VB.CommandButton CmdSair 
         Caption         =   "&Sair"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   7485
         TabIndex        =   18
         Top             =   210
         Width           =   1395
      End
      Begin VB.CommandButton CmdContabilizar 
         Caption         =   "&Contabilizar"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   6060
         TabIndex        =   17
         Top             =   210
         Width           =   1425
      End
   End
   Begin VB.Frame Frame1 
      Height          =   5430
      Left            =   90
      TabIndex        =   0
      Top             =   30
      Width           =   8970
      Begin VB.TextBox txtFim 
         BackColor       =   &H8000000F&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   3240
         Locked          =   -1  'True
         TabIndex        =   2
         TabStop         =   0   'False
         Top             =   720
         Width           =   2055
      End
      Begin VB.TextBox txtInicio 
         BackColor       =   &H8000000F&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   3240
         Locked          =   -1  'True
         TabIndex        =   1
         TabStop         =   0   'False
         Top             =   240
         Width           =   2055
      End
      Begin VB.Frame Frame3 
         Height          =   4275
         Left            =   5490
         TabIndex        =   15
         Top             =   960
         Width           =   3405
         Begin VB.CheckBox ChkRestituicaoDevolvidas 
            Caption         =   "Restituições Devolvidas"
            Height          =   285
            Left            =   240
            TabIndex        =   27
            Tag             =   "13"
            Top             =   3810
            Width           =   3075
         End
         Begin VB.CheckBox ChkMindep 
            Caption         =   "Ajustamento MINDEP - Resseguro"
            Height          =   285
            Left            =   240
            TabIndex        =   26
            Tag             =   "11"
            Top             =   3510
            Width           =   3075
         End
         Begin VB.CheckBox ChkEstimativa 
            Caption         =   "Estimativa de Sinistros - Resseguro"
            Height          =   285
            Left            =   240
            TabIndex        =   25
            Tag             =   "11"
            Top             =   3195
            Width           =   3075
         End
         Begin VB.CheckBox ChkPIP 
            Caption         =   "PIP (Provisão Insuficiência de Prêmios)"
            Height          =   285
            Left            =   240
            TabIndex        =   24
            Tag             =   "10"
            Top             =   2880
            Width           =   3075
         End
         Begin VB.CheckBox ChkVariacao 
            Caption         =   "Variação Cambial - Resseguro"
            Height          =   285
            Left            =   240
            TabIndex        =   12
            Tag             =   "9"
            Top             =   2565
            Width           =   2445
         End
         Begin VB.CheckBox ChkEmi 
            Caption         =   "Emissão / Endosso"
            Height          =   195
            Left            =   240
            TabIndex        =   4
            Tag             =   "1"
            Top             =   300
            Width           =   2445
         End
         Begin VB.CheckBox ChkCan 
            Caption         =   "Cancelamento"
            Height          =   195
            Left            =   240
            TabIndex        =   7
            Tag             =   "4"
            Top             =   1158
            Width           =   2445
         End
         Begin VB.CheckBox ChkRestituicao 
            Caption         =   "Restituição / Bonificação"
            Height          =   195
            Left            =   240
            TabIndex        =   6
            Tag             =   "3"
            Top             =   872
            Width           =   2445
         End
         Begin VB.CheckBox ChkCob 
            Caption         =   "Cobrança / Fatura"
            Height          =   195
            Left            =   240
            TabIndex        =   5
            Tag             =   "2"
            Top             =   586
            Width           =   2445
         End
         Begin VB.CheckBox chkSinistro 
            Caption         =   "Sinistro"
            Height          =   195
            Left            =   240
            TabIndex        =   8
            Tag             =   "5"
            Top             =   1444
            Width           =   2445
         End
         Begin VB.CheckBox chkPendentes 
            Caption         =   "Acerto de Contas - Congênere"
            Height          =   195
            Left            =   240
            TabIndex        =   9
            Tag             =   "6"
            Top             =   1730
            Width           =   2445
         End
         Begin VB.CheckBox ChkResseguro 
            Caption         =   "Resseguro"
            Height          =   195
            Left            =   240
            TabIndex        =   10
            Tag             =   "7"
            Top             =   2016
            Width           =   2445
         End
         Begin VB.CheckBox ChkRepasse 
            Caption         =   "Repasse"
            Height          =   195
            Left            =   240
            TabIndex        =   11
            Tag             =   "8"
            Top             =   2295
            Width           =   2445
         End
      End
      Begin VB.Frame Frame4 
         Height          =   525
         Left            =   5475
         TabIndex        =   14
         Top             =   420
         Width           =   3420
         Begin VB.CheckBox ChkTodos 
            Caption         =   "TODOS"
            Height          =   195
            Left            =   240
            TabIndex        =   3
            Top             =   210
            Width           =   2145
         End
      End
      Begin MSFlexGridLib.MSFlexGrid GrdResult 
         Height          =   3870
         Left            =   150
         TabIndex        =   13
         Top             =   1440
         Width           =   5175
         _ExtentX        =   9128
         _ExtentY        =   6826
         _Version        =   393216
         Cols            =   3
         FixedCols       =   0
         FormatString    =   "Evento                                                     | Lidos          | Gravados     "
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "Fim Processamento ......................"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   150
         TabIndex        =   23
         Top             =   720
         Width           =   3030
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Início Processamento ..................."
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   150
         TabIndex        =   22
         Top             =   240
         Width           =   3060
      End
      Begin VB.Label Label3 
         AutoSize        =   -1  'True
         Caption         =   "Registros Processados"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   150
         TabIndex        =   21
         Top             =   1230
         Width           =   1950
      End
      Begin VB.Label Label4 
         Caption         =   "Eventos Contábeis"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   5490
         TabIndex        =   20
         Top             =   240
         Width           =   2175
      End
   End
   Begin MSComctlLib.StatusBar StbRodape 
      Align           =   2  'Align Bottom
      Height          =   315
      Left            =   0
      TabIndex        =   19
      Top             =   6135
      Width           =   9075
      _ExtentX        =   16007
      _ExtentY        =   556
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   3
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   10583
            MinWidth        =   10583
            Text            =   "Mensagem"
            TextSave        =   "Mensagem"
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            AutoSize        =   2
            Object.Width           =   2170
            MinWidth        =   2170
            Text            =   "Data do sistema"
            TextSave        =   "Data do sistema"
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            AutoSize        =   2
            Object.Width           =   1931
            MinWidth        =   1940
            Text            =   "Usuário"
            TextSave        =   "Usuário"
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "frmGeraLancContabeis"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Option Explicit
    Dim sSigla_tarefa              As String    ' JLIMA 29/112017 - RCA295- Otimização SEG0594
    Dim sDt_agenda                 As String    ' JLIMA 29/112017 - RCA295- Otimização SEG0594
    Dim sAtividade                 As String    ' JLIMA 29/112017 - RCA295- Otimização SEG0594
    Dim sHorario_inicio_tarefa     As String    ' JLIMA 29/112017 - RCA295- Otimização SEG0594
    Dim sHorario_inicio_atividade  As String    ' JLIMA 29/112017 - RCA295- Otimização SEG0594

Public Sub ContabilizarEndosso()

On Error GoTo Trata_Erro:
    
    Dim lTotLidos As Long
    Dim lTotGravados As Long
    Dim OEndosso As Object
        
    'Instanciando o objeto de Contabilidade''''''''''''''''''''''''''''''''''''''''''''''''
    Set OEndosso = CreateObject("SEGL0169.cls00215")
  
    Call OEndosso.ContabilizarEndosso("SEGBR", _
                                     App.Title, _
                                     App.FileDescription, _
                                     glAmbiente_id, _
                                     cUserName, _
                                     lTotLidos, _
                                     lTotGravados)
    
    'Atualizando interface'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    GrdResult.AddItem "Contabilizar Endosso" & vbTab & lTotLidos & vbTab & lTotGravados
   
    StbRodape.Panels(1).Text = "Selecione o(s) item(s) para contabilização."

    Exit Sub
  
Trata_Erro:
    Call TratarErro("ContabilizarEndosso", Me.name)
    Call TerminaSEGBR
End Sub

Public Sub ContabilizarEmissao_Adesao()

On Error GoTo Trata_Erro:
    
    Dim lTotLidos As Long
    Dim lTotGravados As Long
    Dim oEmissao As Object
        
    'Instanciando o objeto de Contabilidade''''''''''''''''''''''''''''''''''''''''''''''''
    Set oEmissao = CreateObject("SEGL0169.cls00215")
  
    Call oEmissao.ContabilizarEmissao_Adesao("SEGBR", _
                                     App.Title, _
                                     App.FileDescription, _
                                     glAmbiente_id, _
                                     cUserName, _
                                     lTotLidos, _
                                     lTotGravados)
    
    'Atualizando interface'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    GrdResult.AddItem "Contabilizar Emissao Adesão" & vbTab & lTotLidos & vbTab & lTotGravados
   
    StbRodape.Panels(1).Text = "Selecione o(s) item(s) para contabilização."

    Exit Sub
  
Trata_Erro:
    Call TratarErro("ContabilizarEmissao_Adesao", Me.name)
    Call TerminaSEGBR
End Sub

Public Sub ContabilizarEmissao()

On Error GoTo Trata_Erro:
    
    Dim lTotLidos As Long
    Dim lTotGravados As Long
    Dim oEmissao As Object
        
    'Instanciando o objeto de Contabilidade''''''''''''''''''''''''''''''''''''''''''''''''
    Set oEmissao = CreateObject("SEGL0169.cls00215")
  
    Call oEmissao.ContabilizarEmissao("SEGBR", _
                                     App.Title, _
                                     App.FileDescription, _
                                     glAmbiente_id, _
                                     cUserName, _
                                     lTotLidos, _
                                     lTotGravados)
    
    'Atualizando interface'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    GrdResult.AddItem "Contabilizar Emissao" & vbTab & lTotLidos & vbTab & lTotGravados
   
    StbRodape.Panels(1).Text = "Selecione o(s) item(s) para contabilização."

    Exit Sub
  
Trata_Erro:
    Call TratarErro("ContabilizarEmissao", Me.name)
    Call TerminaSEGBR
End Sub
Public Sub ContabilizarEndosso_Adesao()

On Error GoTo Trata_Erro:
    
    Dim lTotLidos As Long
    Dim lTotGravados As Long
    Dim OEndosso As Object
                                             
                                     
    'Instanciando o objeto de Contabilidade''''''''''''''''''''''''''''''''''''''''''''''''
    Set OEndosso = CreateObject("SEGL0169.cls00215")
  
    Call OEndosso.ContabilizarEndosso_Adesao("SEGBR", _
                                           App.Title, _
                                           App.FileDescription, _
                                           glAmbiente_id, _
                                           cUserName, _
                                           lTotLidos, _
                                           lTotGravados)
    
    'Atualizando interface'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    GrdResult.AddItem "Contabilizar Endosso" & vbTab & lTotLidos & vbTab & lTotGravados
   
    StbRodape.Panels(1).Text = "Selecione o(s) item(s) para contabilização."

    Exit Sub
  
Trata_Erro:
    Call TratarErro("ContabilizarEndosso", Me.name)
    Call TerminaSEGBR
End Sub

Public Sub ContabilizarCobrancaBaixaParcela()

On Error GoTo Trata_Erro:
    
    Dim lTotLidos As Long
    Dim lTotGravados As Long
    Dim oCobranca As Object
        
    'Instanciando o objeto de Contabilidade''''''''''''''''''''''''''''''''''''''''''''''''
    Set oCobranca = CreateObject("SEGL0169.cls00215")
  
    Call oCobranca.ContabilizarCobrancaBaixaParcela("SEGBR", _
                                                  App.Title, _
                                                  App.FileDescription, _
                                                  glAmbiente_id, _
                                                  cUserName, _
                                                  lTotLidos, _
                                                  lTotGravados)
    
    'Atualizando interface'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    GrdResult.AddItem "Cobranca - Baixa de Parcelas" & vbTab & lTotLidos & vbTab & lTotGravados
   
    StbRodape.Panels(1).Text = "Selecione o(s) item(s) para contabilização."

    Exit Sub
  
Trata_Erro:
    Call TratarErro("ContabilizarCobrancaBaixaParcela", Me.name)
    Call TerminaSEGBR
End Sub

Public Sub ContabilizarCobrancaFaturas()

On Error GoTo Trata_Erro:
    
    Dim lTotLidos As Long
    Dim lTotGravados As Long
    Dim oCobranca As Object
        
    'Instanciando o objeto de Contabilidade''''''''''''''''''''''''''''''''''''''''''''''''
    Set oCobranca = CreateObject("SEGL0169.cls00215")
  
    Call oCobranca.ContabilizarCobrancaFaturas("SEGBR", _
                                               App.Title, _
                                               App.FileDescription, _
                                               glAmbiente_id, _
                                               cUserName, _
                                               lTotLidos, _
                                               lTotGravados)
    
    'Atualizando interface'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    GrdResult.AddItem "Cobrança - Baixa de Faturas" & vbTab & lTotLidos & vbTab & lTotGravados
   
    StbRodape.Panels(1).Text = "Selecione o(s) item(s) para contabilização."

    Exit Sub
  
Trata_Erro:
    Call TratarErro("ContabilizarCobrancaFaturas", Me.name)
    Call TerminaSEGBR
End Sub

Public Sub ContabilizarVisaNetBaixaParcela()

On Error GoTo Trata_Erro:
    
    Dim lTotLidos As Long
    Dim lTotGravados As Long
    Dim oCobranca As Object
        
    'Instanciando o objeto de Contabilidade''''''''''''''''''''''''''''''''''''''''''''''''
    Set oCobranca = CreateObject("SEGL0169.cls00215")
  
    Call oCobranca.ContabilizarVisaNetBaixaParcela("SEGBR", _
                                                    App.Title, _
                                                    App.FileDescription, _
                                                    glAmbiente_id, _
                                                    cUserName, _
                                                    lTotLidos, _
                                                    lTotGravados)
    
    'Atualizando interface'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    GrdResult.AddItem "Cobrança - Baixa VisaNet Agendamento" & vbTab & lTotLidos & vbTab & lTotGravados
   
    StbRodape.Panels(1).Text = "Selecione o(s) item(s) para contabilização."

    Exit Sub
  
Trata_Erro:
    Call TratarErro("ContabilizarVisaNetBaixaParcela", Me.name)
    Call TerminaSEGBR
End Sub

Public Sub ContabilizarVisaNetFatura()

On Error GoTo Trata_Erro:
    
    Dim lTotLidos As Long
    Dim lTotGravados As Long
    Dim oCobranca As Object
        
    'Instanciando o objeto de Contabilidade''''''''''''''''''''''''''''''''''''''''''''''''
    Set oCobranca = CreateObject("SEGL0169.cls00215")
  
    Call oCobranca.ContabilizarVisaNetFatura("SEGBR", _
                                            App.Title, _
                                            App.FileDescription, _
                                            glAmbiente_id, _
                                            cUserName, _
                                            lTotLidos, _
                                            lTotGravados)
    
    'Atualizando interface'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    GrdResult.AddItem "Cobrança - Baixa VisaNet Fatura " & vbTab & lTotLidos & vbTab & lTotGravados
   
    StbRodape.Panels(1).Text = "Selecione o(s) item(s) para contabilização."

    Exit Sub
  
Trata_Erro:
    Call TratarErro("ContabilizarVisaNetFatura", Me.name)
    Call TerminaSEGBR
End Sub

Public Sub ContabilizarFaturaResumo()

On Error GoTo Trata_Erro:
    
    Dim lTotLidos As Long
    Dim lTotGravados As Long
    Dim oCobranca As Object
        
    'Instanciando o objeto de Contabilidade''''''''''''''''''''''''''''''''''''''''''''''''
    Set oCobranca = CreateObject("SEGL0169.cls00215")
  
    Call oCobranca.ContabilizarFaturaResumo("SEGBR", _
                                          App.Title, _
                                          App.FileDescription, _
                                          glAmbiente_id, _
                                          cUserName, _
                                          lTotLidos, _
                                          lTotGravados)
    
    'Atualizando interface'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    GrdResult.AddItem "Cobrança - Baixa de Fatura Resumo" & vbTab & lTotLidos & vbTab & lTotGravados
   
    StbRodape.Panels(1).Text = "Selecione o(s) item(s) para contabilização."

    Exit Sub
  
Trata_Erro:
    Call TratarErro("ContabilizarFaturaResumo", Me.name)
    Call TerminaSEGBR
End Sub

Public Sub ContabilizarBonificacao()

On Error GoTo Trata_Erro:
    
    Dim lTotLidos As Long
    Dim lTotGravados As Long
    Dim oEmissao As Object
        
    'Instanciando o objeto de Contabilidade''''''''''''''''''''''''''''''''''''''''''''''''
    Set oEmissao = CreateObject("SEGL0169.cls00215")
  
    Call oEmissao.ContabilizarBonificacao("SEGBR", _
                                          App.Title, _
                                          App.FileDescription, _
                                          glAmbiente_id, _
                                          cUserName, _
                                          lTotLidos, _
                                          lTotGravados)
    
    'Atualizando interface'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    GrdResult.AddItem "Bonificação" & vbTab & lTotLidos & vbTab & lTotGravados
   
    StbRodape.Panels(1).Text = "Selecione o(s) item(s) para contabilização."

    Exit Sub
  
Trata_Erro:
    Call TratarErro("ContabilizarBonificacao", Me.name)
    Call TerminaSEGBR
End Sub

Public Sub ContabilizarRestituicao()

On Error GoTo Trata_Erro:
    
    Dim lTotLidos As Long
    Dim lTotGravados As Long
    Dim oEmissao As Object
        
    'Instanciando o objeto de Contabilidade''''''''''''''''''''''''''''''''''''''''''''''''
    Set oEmissao = CreateObject("SEGL0169.cls00215")
  
    Call oEmissao.ContabilizarRestituicao("SEGBR", _
                                          App.Title, _
                                          App.FileDescription, _
                                          glAmbiente_id, _
                                          cUserName, _
                                          lTotLidos, _
                                          lTotGravados)
    
    'Atualizando interface'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    GrdResult.AddItem "Restituição" & vbTab & lTotLidos & vbTab & lTotGravados
   
    StbRodape.Panels(1).Text = "Selecione o(s) item(s) para contabilização."

    Exit Sub
  
Trata_Erro:
    Call TratarErro("ContabilizarRestituicao", Me.name)
    Call TerminaSEGBR
End Sub

Public Sub ContabilizarBonificacaoResseguro()

On Error GoTo Trata_Erro:
    
    Dim lTotLidos As Long
    Dim lTotGravados As Long
    Dim oEmissao As Object
        
    'Instanciando o objeto de Contabilidade''''''''''''''''''''''''''''''''''''''''''''''''
    Set oEmissao = CreateObject("SEGL0169.cls00215")
  
    Call oEmissao.ContabilizarBonificacaoResseguro("SEGBR", _
                                                   App.Title, _
                                                   App.FileDescription, _
                                                   glAmbiente_id, _
                                                   cUserName, _
                                                   lTotLidos, _
                                                   lTotGravados)
    
    'Atualizando interface'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    GrdResult.AddItem "Bonificação Resseguro" & vbTab & lTotLidos & vbTab & lTotGravados
   
    StbRodape.Panels(1).Text = "Selecione o(s) item(s) para contabilização."

    Exit Sub
  
Trata_Erro:
    Call TratarErro("ContabilizarBonificacaoResseguro", Me.name)
    Call TerminaSEGBR
End Sub


Public Sub ContabilizarSinistros()

On Error GoTo Trata_Erro:
    
    Dim lTotLidos As Long
    Dim lTotGravados As Long
    Dim oSinistro As Object
        
    'Instanciando o objeto de Contabilidade''''''''''''''''''''''''''''''''''''''''''''''''
    Set oSinistro = CreateObject("SEGL0169.cls00215") 'cl.Sinistro
  
    Call oSinistro.ContabilizarRecuperacaoSinistro("SEGBR", _
                                                   App.Title, _
                                                   App.FileDescription, _
                                                   glAmbiente_id, _
                                                   cUserName, _
                                                   lTotLidos, _
                                                   lTotGravados)
       
    
    'Atualizando interface'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    GrdResult.AddItem "Recuperação de Sinistro" & vbTab & lTotLidos & vbTab & lTotGravados
   
    StbRodape.Panels(1).Text = "Selecione o(s) item(s) para contabilização."

    Exit Sub
  
Trata_Erro:
    Call TratarErro("ContabilizarSinistros", Me.name)
    Call TerminaSEGBR
End Sub


'-- 19/12/05 -------------------------------------------------
Public Sub ContabilizarPgtoSinistroResseguro()

On Error GoTo Trata_Erro:
    
    Dim lTotLidos As Long
    Dim lTotGravados As Long
    Dim oSinistro As Object
        
    'Instanciando o objeto de Contabilidade''''''''''''''''''''''''''''''''''''''''''''''''
    Set oSinistro = CreateObject("SEGL0169.cls00215")
  
    Call oSinistro.ContabilizarPgtoSinistroResseguro("SEGBR", _
                                                     App.Title, _
                                                     App.FileDescription, _
                                                     glAmbiente_id, _
                                                     cUserName, _
                                                     lTotLidos, _
                                                     lTotGravados)
    
    'Atualizando interface'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    GrdResult.AddItem "Pagamento de Sinistro - Resseguro" & vbTab & lTotLidos & vbTab & lTotGravados
   
    StbRodape.Panels(1).Text = "Selecione o(s) item(s) para contabilização."

    Exit Sub
  
Trata_Erro:
    Call TratarErro("ContabilizarPgtoSinistroResseguro", Me.name)
    Call TerminaSEGBR
End Sub


'-- 29/12/05 -------------------------------------------------
Public Sub ContabilizarEstornoPgtoSinistroResseguro()

On Error GoTo Trata_Erro:
    
    Dim lTotLidos As Long
    Dim lTotGravados As Long
    Dim oSinistro As Object
        
    'Instanciando o objeto de Contabilidade''''''''''''''''''''''''''''''''''''''''''''''''
    Set oSinistro = CreateObject("SEGL0169.cls00215")
  
    Call oSinistro.ContabilizarEstornoPgtoSinistroResseguro("SEGBR", _
                                                            App.Title, _
                                                            App.FileDescription, _
                                                            glAmbiente_id, _
                                                            cUserName, _
                                                            lTotLidos, _
                                                            lTotGravados)
    
    'Atualizando interface'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    GrdResult.AddItem "Estorno de Pagamento de Sinistro - Resseguro" & vbTab & lTotLidos & vbTab & lTotGravados
   
    StbRodape.Panels(1).Text = "Selecione o(s) item(s) para contabilização."

    Exit Sub
  
Trata_Erro:
    Call TratarErro("ContabilizarEstornoPgtoSinistroResseguro", Me.name)
    Call TerminaSEGBR
End Sub


'agin - 18/07/2005
Public Sub ContabilizarResseguro()

On Error GoTo Trata_Erro:
    
    Dim oObjeto As Object
    Dim lTotLidos As Long
    Dim lTotGravados As Long
        
    Set oObjeto = CreateObject("SEGL0169.cls00215")
  
    Call oObjeto.ContabilizarResseguro("SEGBR", _
                                       App.Title, _
                                       App.FileDescription, _
                                       glAmbiente_id, _
                                       cUserName, _
                                       lTotLidos, _
                                       lTotGravados)
       
    GrdResult.AddItem "Emissão de Resseguro" & vbTab & lTotLidos & vbTab & lTotGravados
    StbRodape.Panels(1).Text = "Selecione o(s) item(s) para contabilização."

    Exit Sub
      
Trata_Erro:
    Call TratarErro("ContabilizarResseguro", Me.name)
    Call TerminaSEGBR
End Sub


'RMAURI - 02/10/2006
Public Sub ContabilizarRepasse()

On Error GoTo Trata_Erro:
    
    Dim oObjeto As Object
    Dim lTotLidos As Long
    Dim lTotGravados As Long
        
    Set oObjeto = CreateObject("SEGL0169.cls00215")
  
    Call oObjeto.ContabilizarRepasse("SEGBR", _
                                       App.Title, _
                                       App.FileDescription, _
                                       glAmbiente_id, _
                                       cUserName, _
                                       lTotLidos, _
                                       lTotGravados)
       
    GrdResult.AddItem "Contabilização de Repasse" & vbTab & lTotLidos & vbTab & lTotGravados
    StbRodape.Panels(1).Text = "Selecione o(s) item(s) para contabilização."

    Exit Sub
      
Trata_Erro:
    Call TratarErro("ContabilizarResseguro", Me.name)
    Call TerminaSEGBR
End Sub

Public Sub ContabilizarCancelamentoResseguroProposta()

On Error GoTo Trata_Erro:
    
    Dim oObjeto As Object
    Dim lTotLidos As Long
    Dim lTotGravados As Long
        
    Set oObjeto = CreateObject("SEGL0169.cls00215")
  
    Call oObjeto.ContabilizarCancelamentoResseguroProposta("SEGBR", _
                                                          App.Title, _
                                                          App.FileDescription, _
                                                          glAmbiente_id, _
                                                          cUserName, _
                                                          lTotLidos, _
                                                          lTotGravados)
       
    GrdResult.AddItem "Cancelamento das propostas de Resseguro " & vbTab & lTotLidos & vbTab & lTotGravados
    StbRodape.Panels(1).Text = "Selecione o(s) item(s) para contabilização."

    Exit Sub
      
Trata_Erro:
    Call TratarErro("ContabilizarCancelamentoResseguroProposta", Me.name)
    Call TerminaSEGBR
End Sub

Public Sub ContabilizarCancelamentoResseguroEndosso()

On Error GoTo Trata_Erro:
    
    Dim oObjeto As Object
    Dim lTotLidos As Long
    Dim lTotGravados As Long
        
    Set oObjeto = CreateObject("SEGL0169.cls00215")
  
    Call oObjeto.ContabilizarCancelamentoResseguroEndosso("SEGBR", _
                                                          App.Title, _
                                                          App.FileDescription, _
                                                          glAmbiente_id, _
                                                          cUserName, _
                                                          lTotLidos, _
                                                          lTotGravados)
       
    GrdResult.AddItem "Cancelamento dos endossos de Resseguro " & vbTab & lTotLidos & vbTab & lTotGravados
    StbRodape.Panels(1).Text = "Selecione o(s) item(s) para contabilização."

    Exit Sub
      
Trata_Erro:
    Call TratarErro("ContabilizarCancelamentoResseguroEndosso", Me.name)
    Call TerminaSEGBR
End Sub



Public Sub ContabilizarCancelamentoResseguroFatura()

On Error GoTo Trata_Erro:
    
    Dim oObjeto As Object
    Dim lTotLidos As Long
    Dim lTotGravados As Long
        
    Set oObjeto = CreateObject("SEGL0169.cls00215")
  
    Call oObjeto.ContabilizarCancelamentoResseguroFatura("SEGBR", _
                                                          App.Title, _
                                                          App.FileDescription, _
                                                          glAmbiente_id, _
                                                          cUserName, _
                                                          lTotLidos, _
                                                          lTotGravados)
       
    GrdResult.AddItem "Cancelamento das Faturas de Resseguro " & vbTab & lTotLidos & vbTab & lTotGravados
    StbRodape.Panels(1).Text = "Selecione o(s) item(s) para contabilização."

    Exit Sub
      
Trata_Erro:
    Call TratarErro("ContabilizarCancelamentoResseguroFatura", Me.name)
    Call TerminaSEGBR
End Sub

Public Sub ContabilizarCancelamentoResseguro()

On Error GoTo Trata_Erro:
    
    Dim oObjeto As Object
    Dim lTotLidos As Long
    Dim lTotGravados As Long
        
    Set oObjeto = CreateObject("SEGL0169.cls00215")
  
    Call oObjeto.ContabilizarCancelamentoResseguro("SEGBR", _
                                                   App.Title, _
                                                   App.FileDescription, _
                                                   glAmbiente_id, _
                                                   cUserName, _
                                                   lTotLidos, _
                                                   lTotGravados)
       
    GrdResult.AddItem "Cancelamento de Resseguro" & vbTab & lTotLidos & vbTab & lTotGravados
    StbRodape.Panels(1).Text = "Selecione o(s) item(s) para contabilização."

    Exit Sub
      
Trata_Erro:
    Call TratarErro("ContabilizarCancelamentoResseguro", Me.name)
    Call TerminaSEGBR
End Sub

Public Sub ContabilizarRestituicaoResseguro()

On Error GoTo Trata_Erro:
    
    Dim oObjeto As Object
    Dim lTotLidos As Long
    Dim lTotGravados As Long
        
    Set oObjeto = CreateObject("SEGL0169.cls00215")
  
    Call oObjeto.ContabilizarRestituicaoResseguro("SEGBR", _
                                                  App.Title, _
                                                  App.FileDescription, _
                                                  glAmbiente_id, _
                                                  cUserName, _
                                                  lTotLidos, _
                                                  lTotGravados)
       
    GrdResult.AddItem "Restituição de Resseguro " & vbTab & lTotLidos & vbTab & lTotGravados
    StbRodape.Panels(1).Text = "Selecione o(s) item(s) para contabilização."

    Exit Sub
      
Trata_Erro:
    Call TratarErro("ContabilizarRestituicaoResseguro", Me.name)
    Call TerminaSEGBR
End Sub


Public Sub ContabilizarBaixaPremioIdentificado()

On Error GoTo Trata_Erro:
    
    Dim oObjeto As Object
    Dim lTotLidos As Long
    Dim lTotGravados As Long
        
    Set oObjeto = CreateObject("SEGL0169.cls00215")
  
    Call oObjeto.ContabilizarBaixaPremioIdentificado("SEGBR", _
                                                     App.Title, _
                                                     App.FileDescription, _
                                                     glAmbiente_id, _
                                                     cUserName, _
                                                     lTotLidos, _
                                                     lTotGravados)
       
    GrdResult.AddItem "Baixa de Prêmio Identificado - BPI - Resseguro " & vbTab & lTotLidos & vbTab & lTotGravados
    StbRodape.Panels(1).Text = "Selecione o(s) item(s) para contabilização."

    Exit Sub
      
Trata_Erro:
    Call TratarErro("ContabilizarBaixaPremioIdentificado", Me.name)
    Call TerminaSEGBR
End Sub


Private Sub FormatarTela()
 
    ChkCan.Enabled = False
    ChkCob.Enabled = True
    ChkEmi.Enabled = True
    chkPendentes.Enabled = False
    chkSinistro.Enabled = True
    ChkRestituicao.Enabled = True
    ChkResseguro.Enabled = True
    ChkRepasse.Enabled = True

End Sub

Private Function VerificaCheck() As Boolean

    VerificaCheck = False

    If ChkCan.Value = 1 Or _
       ChkCob.Value = 1 Or _
       ChkEmi.Value = 1 Or _
       chkPendentes.Value = 1 Or _
       chkSinistro.Value = 1 Or _
       ChkRestituicao.Value = 1 Or _
       ChkResseguro.Value = 1 Or _
       ChkRepasse.Value = 1 Or _
       ChkVariacao.Value = 1 Or _
       ChkPIP.Value = 1 Or _
       ChkEstimativa.Value = 1 Or _
       ChkMindep.Value = 1 Or _
       ChkRestituicaoDevolvidas.Value = 1 _
       Then 'Zoro.Gomes - Confitec - 18699317 - Devoluções de Restituição de Prêmio - Fase 2 - Onda 1 - 22/03/2016

        VerificaCheck = True

        If ChkCan.Value = 0 Or _
           ChkCob.Value = 0 Or _
           ChkEmi.Value = 0 Or _
           chkPendentes.Value = 0 Or _
           chkSinistro.Value = 0 Or _
           ChkRestituicao.Value = 0 Or _
           ChkResseguro.Value = 0 Or _
           ChkRepasse.Value = 0 Or _
           ChkVariacao.Value = 0 Or _
           ChkPIP.Value = 0 Or _
           ChkEstimativa.Value = 0 Or _
           ChkMindep.Value = 0 Or _
           ChkRestituicaoDevolvidas.Value = 0 _
           Then 'Zoro.Gomes - Confitec - 18699317 - Devoluções de Restituição de Prêmio - Fase 2 - Onda 1 - 22/03/2016

           ChkTodos.Value = 0
           
        Else
        
           ChkTodos.Value = 1

        End If
    End If

End Function


Private Sub ChkCan_Click()

    CmdContabilizar.Enabled = VerificaCheck

End Sub

Private Sub ChkMindep_Click()

    CmdContabilizar.Enabled = VerificaCheck

End Sub

Private Sub ChkCob_Click()

    CmdContabilizar.Enabled = VerificaCheck

End Sub

Private Sub ChkEmi_Click()

    CmdContabilizar.Enabled = VerificaCheck

End Sub

Private Sub chkPendentes_Click()

    CmdContabilizar.Enabled = VerificaCheck

End Sub

Private Sub ChkPIP_Click()
    CmdContabilizar.Enabled = VerificaCheck
End Sub

'Carlos Henrique 14/06/2011
'FLOW 11319563
'Acerto de Fechamento
Private Sub ChkEstimativa_click()

    CmdContabilizar.Enabled = VerificaCheck

End Sub

Private Sub ChkRepasse_Click()

    CmdContabilizar.Enabled = VerificaCheck

End Sub

Private Sub ChkResseguro_Click()
    
    CmdContabilizar.Enabled = VerificaCheck
    
End Sub

Private Sub ChkRestituicao_Click()

    CmdContabilizar.Enabled = VerificaCheck
    
End Sub

Private Sub ChkRestituicaoDevolvidas_Click()  'Zoro.Gomes - Confitec - 18699317 - Devoluções de Restituição de Prêmio - Fase 2 - Onda 1 - 22/03/2016
    CmdContabilizar.Enabled = VerificaCheck
End Sub

Private Sub chkSinistro_Click()
    CmdContabilizar.Enabled = VerificaCheck
End Sub

Private Sub ChkVariacao_Click()
    CmdContabilizar.Enabled = VerificaCheck
End Sub

Private Sub ChkTodos_Click()
 
    If ChkTodos.Value = 1 Then
        
        ChkCan.Value = 1
        ChkCob.Value = 1
        ChkEmi.Value = 1
        chkPendentes.Value = 1
        chkSinistro.Value = 1
        ChkRestituicao.Value = 1
        ChkResseguro.Value = 1
        ChkRepasse.Value = 1
        ChkVariacao.Value = 1
        'Carlos Henrique 14/06/2011 inicio
        'FLOW 11319563
        'Acerto de Fechamento
        ChkEstimativa.Value = 1
        'Carlos Henrique 14/06/2011 fim
        'FLOW 11319563
        'Acerto de Fechamento
        
        CmdContabilizar.Enabled = True
     
    Else
     
        If ChkCan.Value = 1 And _
           ChkCob.Value = 1 And _
           ChkEmi.Value = 1 And _
           chkPendentes.Value = 1 And _
           chkSinistro.Value = 1 And _
           ChkRestituicao.Value = 1 And _
           ChkResseguro.Value = 1 And _
           ChkRepasse.Value = 1 And _
           ChkVariacao.Value = 1 And _
           ChkEstimativa.Value = 1 Then 'Carlos Henrique 14/06/2011 - FLOW 11319563 - Acerto de Fechamento
           
           Call LimpaCampos
     
        End If
        
    End If

End Sub

Private Sub LimparCampos()

    ChkEmi.Value = 0
    ChkRestituicao.Value = 0
    ChkCan.Value = 0
    ChkCob.Value = 0
    ChkRepasse.Value = 0
    chkSinistro.Value = 0
    chkPendentes.Value = 0
    ChkResseguro.Value = 0
    
    CmdContabilizar.Enabled = False
    
End Sub
Private Sub FormatarGrid()
 
    'Grid de Registros Processados
    
    GrdResult.Rows = 1
     
    GrdResult.Col = 0
    GrdResult.Text = "Evento"
    GrdResult.ColWidth(0) = 3000
    GrdResult.ColAlignment(0) = 1
       
    GrdResult.Col = 1
    GrdResult.Text = "Lidos"
    GrdResult.ColWidth(1) = 1000
    GrdResult.ColAlignment(1) = 3
      
    GrdResult.Col = 2
    GrdResult.Text = "Gravados"
    GrdResult.ColWidth(2) = 1065
    GrdResult.ColAlignment(2) = 3
 
End Sub

Private Sub Processar()

Dim sMensagem As String

    sMensagem = "Gerando lançamentos ..."
    StbRodape.Panels(1).Text = sMensagem
    
        
'' jarbas - 29/11/2017
  ''' Retornando Hora de Execução para registro do LOG
    sHorario_inicio_tarefa = Now
    
  '''- verificando empresa AB/ABS
If glAmbiente_id = 6 Or glAmbiente_id = 7 Then
    sSigla_tarefa = "SEGX1221"
  Else
   sSigla_tarefa = "SEGX0594"
End If
'' fim


    '--- Emissão / Endosso -------------------------------------------------------
    If ChkEmi.Value Or ChkTodos.Value Then

' Ralves - 10/01/2011 - FLOW 3989764 - Circular 395
        Call ContabilizarEmissao_Novo                                'SEGS9257_SPI
        Call ContabilizarEmissaoEndosso_Novo                         'SEGS9278_SPI
'RETIRANDO OPÇÃO DE EMISSAO ANTECIPADA - FADAO - 08/05/2012
        'Call ContabilizarEmissaoAntecipada_NOVO                      'SEGS9358_SPI

'' Ralves - 10/01/2011 - FLOW 3989764 - Circular 395(Processo Antigo)
'        Call ContabilizarEmissao                                     'SEGS4677_SPI
'        Call ContabilizarEmissao_Adesao                              'SEGS4678_SPI
'        Call ContabilizarEndosso                                     'SEGS4679_SPI
'        Call ContabilizarEndosso_Adesao                              'SEGS4680_SPI
'
''Ralves - 14/10/2009 - FLOW 655875 - Emissão Antecipada
'        Call ContabilizarEmissaoAntecipada                           'SEGS7791_SPI
'        Call ContabilizarEmissao_AdesaoAntecipada                    'SEGS7792_SPI

        
    End If
    
    '--- Cobrança / Fatura -------------------------------------------------------
    If ChkCob.Value Or ChkTodos.Value Then
        
        '''Jlima - 29/11/2017 RF00061368 - RCA295 - Otimização: SEGX0594 - Gera Lançamento Contábil - Cobrança/Fatura''''''''''''''''''''''''''''
        sAtividade = "INICIO DO PROCESSAMENTO"
        Call IncluirLogProcessamentoTarefa(sSigla_tarefa, Data_Sistema, sHorario_inicio_tarefa, sAtividade, cUserName, glAmbiente_id)
        
    ''''' Fim ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        
        '''Jlima - 29/11/2017 RF00061368 - RCA295 - Otimização: SEGX0594 - Gera Lançamento Contábil - Cobrança/Fatura''''''''''''''''''''''''''''
                        sAtividade = "INICIO DA ROTINA DE CONTABILIZACAO DE BAIXA DE PARCELA"
                        Call IncluirLogProcessamentoTarefa(sSigla_tarefa, Data_Sistema, sHorario_inicio_tarefa, sAtividade, cUserName, glAmbiente_id)
        '' fim '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        ' Ralves - 19/11/2010 - FLOW 3989764 - Circular 395
       Call ContabilizarCobrancaBaixaParcela_NOVO                    'SEGS9280_SPI
           
        'RETIRANDO OPÇÃO DE EMISSAO ANTECIPADA - FADAO - 08/05/2012
       'Call ContabilizarCobrancaBaixaParcelaAntecipada_NOVO          'SEGS9359_SPI
 
    '''Jlima - 29/11/2017 RF00061368 - RCA295 - Otimização: SEGX0594 - Gera Lançamento Contábil - Cobrança/Fatura''''''''''''''''''''''''''''
        sAtividade = "INICIO DA ROTINA DE CONTABILIZACAO DE BAIXA DE FATURA"
        Call IncluirLogProcessamentoTarefa(sSigla_tarefa, Data_Sistema, sHorario_inicio_tarefa, sAtividade, cUserName, glAmbiente_id)
    '' fim '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                
'       Call ContabilizarCobrancaBaixaParcela                         'SEGS4463_SPI
       Call ContabilizarCobrancaFaturas                              'SEGS4465_SPI
           
        '''Jlima - 29/11/2017 RF00061368 - RCA295 - Otimização: SEGX0594 - Gera Lançamento Contábil - Cobrança/Fatura''''''''''''''''''''''''''''
        sAtividade = "INICIO DA ROTINA DE CONTABILIZACAO DE BAIXA DE FATURA RESUMO"
        Call IncluirLogProcessamentoTarefa(sSigla_tarefa, Data_Sistema, sHorario_inicio_tarefa, sAtividade, cUserName, glAmbiente_id)
    '' fim '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
   
       Call ContabilizarFaturaResumo                                 'SEGS4395_SPI
           
        '''Jlima - 29/11/2017 RF00061368 - RCA295 - Otimização: SEGX0594 - Gera Lançamento Contábil - Cobrança/Fatura''''''''''''''''''''''''''''
        sAtividade = "INICIO DA ROTINA DE CONTABILIZACAO DE BAIXA DE FATURA VISANET"
        Call IncluirLogProcessamentoTarefa(sSigla_tarefa, Data_Sistema, sHorario_inicio_tarefa, sAtividade, cUserName, glAmbiente_id)
     '' fim '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
       
       'fsa - 20/02/2009 - COBRANÇA CARTÃO DE CRÉDITO - VISANET
        Call ContabilizarVisaNetFatura                                'SEGS7436_SPI
'       Call ContabilizarVisaNetBaixaParcela                          'SEGS7435_SPI

'Ralves - 14/10/2009 - FLOW 208624 - Pró-labore Trimestral
       'Call ContabilizarProLabore                                    'SEGS7831_SPI
       
'Ralves - 14/10/2009 - FLOW 655875 - Emissão Antecipada
'       Call ContabilizarCobrancaBaixaParcelaAntecipada               'SEGS7907_SPI
'       Call ContabilizarVisaNetBaixaParcelaAntecipada                'SEGS7913_SPI

    '''Jlima - 29/11/2017 RF00061368 - RCA295 - Otimização: SEGX0594 - Gera Lançamento Contábil - Cobrança/Fatura''''''''''''''''''''''''''''
        sAtividade = "FIM DO PROCESSAMENTO"
        Call IncluirLogProcessamentoTarefa(sSigla_tarefa, Data_Sistema, sHorario_inicio_tarefa, sAtividade, cUserName, glAmbiente_id)
     '' fim '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
           
    End If
     
    '--- Restituição / Bonificação -----------------------------------------------
    If ChkRestituicao.Value Or ChkTodos.Value Then
        ' Carina - 21/06/2018 - Adição de logs para monitorar o tempo de execução das procedures
        Call MensagemBatch("Inicio de processamento - ChkRestituicao " & CStr(Now), vbInformation, Me.Caption, False)
        Call MensagemBatch("Inicio PCD SEGS9305_SPI " & CStr(Now), vbInformation, Me.Caption, False)
        ' Ralves - 11/01/2010 - FLOW 3989764 - Circular 395
           Call ContabilizarRestituicao_NOVO                             'SEGS9305_SPI
        Call MensagemBatch("Inicio PCD SEGS9356_SP " & CStr(Now), vbInformation, Me.Caption, False)
           Call ContabilizarEstornoRestituicao_NOVO                      'SEGS9356_SPI
        'RETIRANDO OPÇÃO DE EMISSAO ANTECIPADA - FADAO - 08/05/2012
           'Call ContabilizarRestituicaoAntecipada_NOVO                   'SEGS9360_SPI
           
        'Ralves - 15/10/2009 - FLOW 1276347 - Separação dos eventos REC e RES
        Call MensagemBatch("Inicio PCD SEGS7860_SPI " & CStr(Now), vbInformation, Me.Caption, False)
           Call ContabilizarRestituicao                                  'SEGS7860_SPI
        Call MensagemBatch("Inicio PCD SEGS8005_SPI " & CStr(Now), vbInformation, Me.Caption, False)
           Call ContabilizarEstornoRestituicao                           'SEGS8005_SPI
    
        'Ralves - 14/10/2009 - FLOW 655875 - Emissão Antecipada
        'RETIRANDO OPÇÃO DE EMISSAO ANTECIPADA - FADAO - 08/05/2012
        'Call ContabilizarRestituicaoAntecipada                        'SEGS7902_SPI
           
        Call MensagemBatch("Inicio PCD SEGS6065_SPI " & CStr(Now), vbInformation, Me.Caption, False)
           Call ContabilizarBonificacao                                  'SEGS6065_SPI
        Call MensagemBatch("Inicio PCD SEGS7759_SPI " & CStr(Now), vbInformation, Me.Caption, False)
           Call ContabilizarBonificacaoRecusadas                         'SEGS7759_SPI
        Call MensagemBatch("Inicio PCD SEGS7760_SPI " & CStr(Now), vbInformation, Me.Caption, False)
           Call ContabilizarBonificacaoAceitas                           'SEGS7760_SPI
        Call MensagemBatch("Inicio PCD SEGS6669_SPI " & CStr(Now), vbInformation, Me.Caption, False)
           Call ContabilizarVoucherBonificacao                           'SEGS6669_SPI
           
        Call MensagemBatch("Fim de processamento - ChkRestituicao " & CStr(Now), vbInformation, Me.Caption, False)
       
       
    End If
    
    '--- Cancelamento ------------------------------------------------------------
    If ChkCan.Value Or ChkTodos.Value Then
' Ralves - 19/11/2010 - FLOW 3989764 - Circular 395
       Call ContabilizarCancelamentoCancelamento                     'SEGS9357_SPI
       Call ContabilizarCancelamentoProposta                         'SEGS9301_SPI
       Call ContabilizarCancelamentoEndosso                          'SEGS9304_SPI
    
    End If
    
    '--- Sinistro ----------------------------------------------------------------
    If chkSinistro.Value Or ChkTodos.Value Then
       Call ContabilizarSinistros                  'contabilizacao_recuperacao_spi
    End If

    '--- Acerto de Contas - congeneres -------------------------------------------
    If chkPendentes.Value Then
    End If

    '--- Resseguro ---------------------------------------------------------------
    If ChkResseguro.Value Or ChkTodos.Value Then
       Call ContabilizarResseguro                                    'SEGS4108_SPI
       Call ContabilizarPgtoSinistroResseguro                        'SEGS4291_SPI
       Call ContabilizarEstornoPgtoSinistroResseguro                 'SEGS4300_SPI
       
       'fsa - 29/11/2007 '''''''''''''''''''''''''''''''''''''''''''''''''''''''''
       Call ContabilizarResseguroCobranca                            'SEGS6844_SPI
       Call ContabilizarResseguroFatura                              'SEGS6845_SPI
       '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
       
       Call ContabilizarCancelamentoResseguroProposta                'SEGS5401_SPI
       Call ContabilizarCancelamentoResseguroEndosso                 'SEGS5402_SPI
       Call ContabilizarCancelamentoResseguroFatura                  'SEGS5403_SPI
       Call ContabilizarRestituicaoResseguro                         'SEGS4191_SPI
       
'' pfiche - 21/08/2007 - Agrícola - Bonificação Resseguro
       Call ContabilizarBonificacaoResseguro                         'SEGS6066_SPI
       
       Call ContabilizarBaixaPremioIdentificado                      'SEGS5296_SPI
    End If
    
    '--- Repasse -----------------------------------------------------------------
    If ChkRepasse.Value Or ChkTodos.Value Then
        Call ContabilizarRepasse                                     'SEGS5409_SPI
        Call ContabilizarAjusteRepasse                               'SEGS6914_SPI 'OuroCard
       'fsa - 20/02/2009 - COBRANÇA CARTÃO DE CRÉDITO - VISANET
        Call ContabilizarVisaNetRepasse                              'SEGS7437_SPI
    End If
    
    '' fsa - 03/12/2007
    '--- Variação Cambial --------------------------------------------------------
    If ChkVariacao.Value Or ChkTodos.Value Then
        Call ContabilizarVCResseguroAgendamento                      'SEGS6056_SPI
        Call ContabilizarVCResseguroPremioIRB                        'SEGS5590_SPI
        Call ContabilizarVCResseguroSinistroIRB                      'SEGS5591_SPI
    End If

    If ChkPIP.Value Or ChkTodos.Value Then
        Call ContabilizarPIP                                         'SEGS6915_SPI
    End If
    
    ' Ralves - 31/08/2009
    '----- Estimativa Resseguro ---------------------------------------------
    If ChkEstimativa.Value Or ChkTodos.Value Then
' Ralves - 10/01/2011 - FLOW 3989764 - Circular 395
        Call ContabilizarEstimativa                                  'SEGS9406_SPI
        Call ContabilizarPagamento                                   'SEGS9372_SPI
        Call ContabilizarEstimativaResseguro                         'SEGS7754_SPI
        'Carlos Henrique 14/06/2011 inicio
        'FLOW 11319563
        'Acerto de Fechamento
        Call Contabilizar_Atualizacao_Monetaria_sinistro             'SEGS7443_SPI
        'Carlos Henrique 14/06/2011 fim
        'FLOW 11319563
        'Acerto de Fechamento
    End If
    
    ' fsa - 08/08/2009
    '----- Ajustamento de Catástrofe ----------------------------------------
    If ChkMindep.Value Or ChkTodos.Value Then
        Call ContabilizarMindepTotal                                 'SEGS7731_SPI
        Call ContabilizarMindep                                      'SEGS7726_SPI
        Call ContabilizarMindepParcelas                              'SEGS7730_SPI
        Call ContabilizarMindepDiferenca                             'SEGS7787_SPI
    End If
    
    'Zoro.Gomes - Confitec - 18699317 - Devoluções de Restituição de Prêmio - Fase 2 - Onda 1 - 22/03/2016
    'baseada na Call ContabilizarRestituicao - SEGS7860_SPI
    If ChkRestituicaoDevolvidas.Value Or ChkTodos.Value Then
       ContabilizarRestituicoes_Devolvidas
    End If
        
End Sub

Private Sub CmdContabilizar_Click()

On Error GoTo Trata_Erro
  
    'Atualizando interface'''''''''''''''''''''''''''''''''''''''''''''''''''''''
    MousePointer = 11
    
    txtFim.Text = ""
    txtFim.Refresh
    txtInicio = Now
    txtInicio.Refresh
    
    CmdSair.Caption = "Cancelar"
     
    CmdContabilizar.Enabled = False
    
    'Incializando AutProd'''''''''''''''''''''''''''''''''''''''''''''''''''''''
    'Call InicializaParametrosExecucaoBatch(Me)
        
    'Processando'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    Call Processar
        
    'Atualizando interface'''''''''''''''''''''''''''''''''''''''''''''''''''''''
    CmdContabilizar.Enabled = True
    
    txtFim = Now
    txtFim.Refresh
    
    MousePointer = 0
    CmdSair.Caption = "Sair"
    
    Call MensagemBatch("Fim de Processamento.", vbInformation, Me.Caption, False)
       
    Call LimparCampos
       
    Call GravarLog
       
    'Pula de for executado pelo Control-M
    If CTM = False Then
        Call goProducao.finaliza
    End If
      
    Exit Sub
Resume
Trata_Erro:
    Call TrataErroGeral("CmdContabilizar_Click", Me.name)
    Call TerminaSEGBR
  
End Sub

Private Sub GravarLog()

  ' Logando o número de registros processados - Scheduler '''''''''''''''''''''''''''
    
Dim i As Integer
Dim iIndice As Integer

    For i = 1 To GrdResult.Rows - 1
      iIndice = iIndice + 1
      Call goProducao.AdicionaLog(1, GrdResult.TextMatrix(i, 0), iIndice)
      Call goProducao.AdicionaLog(2, GrdResult.TextMatrix(i, 1), iIndice)
      Call goProducao.AdicionaLog(3, GrdResult.TextMatrix(i, 2), iIndice)
      
          'grava log arquivo CTM
        If CTM = True Then
            Call GravaLogCTM("OK - ", GrdResult.TextMatrix(i, 0), "", "")
                ' Jarbas Lima - Inicio - 30/07/2018 - RF00061368 - RCA295 - SEGX0594 - Cobrança/Fatura  - Incluindo parametros para identificar os contadores na saida do sysout (descrição e hora).
            'Call GravaLogCTM("OK - ", GrdResult.TextMatrix(i, 1), "", "")
            'Call GravaLogCTM("OK - ", GrdResult.TextMatrix(i, 2), "", "")
                        Call GravaLogCTM("OK - Lidos    - ", GrdResult.TextMatrix(i, 1), "", Now)
                        Call GravaLogCTM("OK - Gravados - ", GrdResult.TextMatrix(i, 2), "", Now)
                ' Jarbas Lima - Fim - 30/07/2018 - RF00061368 - RCA295 - SEGX0594 - Cobrança/Fatura  - Incluindo parametros para identificar os contadores na saida do sysout (descrição e hora).
        End If

      
    Next
    
    Exit Sub
    
Trata_Erro:
    Call TratarErro("GravarLog", Me.name)
    Call FinalizarAplicacao
End Sub


Private Sub CmdSair_Click()
    
    Call FinalizarAplicacao
    
End Sub

Private Sub Form_Load()
 
On Error GoTo TratarErro
'Verifica se foi executado pelo Control-M
CTM = Verifica_Origem_ControlM(Command)

    'Pula se for executado pelo control-m
    If CTM = False Then
       'Verificando permissão de acesso à aplicação'''''''''''''''''''''''''''
       If Not Trata_Parametros(Command) Then
           'Call FinalizarAplicacao
       End If
    End If
      
    'Retirar antes da liberação
   cUserName = "NOVA"
    glAmbiente_id = 3
    
    'Obtendo a data operacional ''''''''''''''''''''''''''''''''''''''''''''
    Call ObterDataSistema("SEGBR")
    
    'Configurando interface'''''''''''''''''''''''''''''''''''''''''''''''''
    Call CentraFrm(Me)
    
    Call FormatarTela
    
    Call FormatarGrid
    
    CmdContabilizar.Enabled = False
      
    Me.Caption = App.Title & " - Gera Lançamentos Contábeis " & ExibirAmbiente(glAmbiente_id)
    
    StbRodape.Panels(1).Text = "Selecione o(s) item(s) para contabilização."
    StbRodape.Panels(2) = Data_Sistema
    StbRodape.Panels(3) = cUserName
    
    If Obtem_agenda_diaria_id(Command) > 0 Or CTM = True Then
        Me.Show
        Call CmdContabilizar_Click
        Call FinalizarAplicacao
    End If
    
    Exit Sub

TratarErro:
    Call TrataErroGeral("Form_Load", Me.name)
    Call FinalizarAplicacao
End Sub

Public Sub ContabilizarVoucherBonificacao()

On Error GoTo Trata_Erro:
    
    Dim lTotLidos As Long
    Dim lTotGravados As Long
    Dim oEmissao As Object
        
    'Instanciando o objeto de Contabilidade''''''''''''''''''''''''''''''''''''''''''''''''
    Set oEmissao = CreateObject("SEGL0169.cls00215")
  
    Call oEmissao.ContabilizarVoucherBonificacao("SEGBR", _
                                          App.Title, _
                                          App.FileDescription, _
                                          glAmbiente_id, _
                                          cUserName, _
                                          lTotLidos, _
                                          lTotGravados)
    
    'Atualizando interface'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    GrdResult.AddItem "Voucher de Bonificação" & vbTab & lTotLidos & vbTab & lTotGravados
   
    StbRodape.Panels(1).Text = "Selecione o(s) item(s) para contabilização."

    Exit Sub
  
Trata_Erro:
    Call TratarErro("ContabilizarVoucherBonificacao", Me.name)
    Call TerminaSEGBR
End Sub

'fsa - 28/11/2007
Public Sub ContabilizarResseguroCobranca()

On Error GoTo Trata_Erro:
    
    Dim lTotLidos As Long
    Dim lTotGravados As Long
    Dim oCobranca As Object
        
    'Instanciando o objeto de Contabilidade''''''''''''''''''''''''''''''''''''''''''''''''
    Set oCobranca = CreateObject("SEGL0169.cls00215")
  
    Call oCobranca.ContabilizarResseguroCobranca("SEGBR", _
                                                  App.Title, _
                                                  App.FileDescription, _
                                                  glAmbiente_id, _
                                                  cUserName, _
                                                  lTotLidos, _
                                                  lTotGravados)
    
    'Atualizando interface'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    GrdResult.AddItem "Resseguro Cobranca - Baixa de Parcelas" & vbTab & lTotLidos & vbTab & lTotGravados
   
    StbRodape.Panels(1).Text = "Selecione o(s) item(s) para contabilização."

    Exit Sub
  
Trata_Erro:
    Call TratarErro("ContabilizarResseguroCobranca", Me.name)
    Call TerminaSEGBR
End Sub

Public Sub ContabilizarResseguroFatura()

On Error GoTo Trata_Erro:
    
    Dim lTotLidos As Long
    Dim lTotGravados As Long
    Dim oCobranca As Object
        
    'Instanciando o objeto de Contabilidade''''''''''''''''''''''''''''''''''''''''''''''''
    Set oCobranca = CreateObject("SEGL0169.cls00215")
  
    Call oCobranca.ContabilizarResseguroFatura("SEGBR", _
                                                App.Title, _
                                                App.FileDescription, _
                                                glAmbiente_id, _
                                                cUserName, _
                                                lTotLidos, _
                                                lTotGravados)
    
    'Atualizando interface'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    GrdResult.AddItem "Resseguro Fatura - Baixa de Parcelas" & vbTab & lTotLidos & vbTab & lTotGravados
   
    StbRodape.Panels(1).Text = "Selecione o(s) item(s) para contabilização."

    Exit Sub
  
Trata_Erro:
    Call TratarErro("ContabilizarResseguroFatura", Me.name)
    Call TerminaSEGBR
End Sub

Public Sub ContabilizarVCResseguroAgendamento()

On Error GoTo Trata_Erro:
    
    Dim lTotLidos As Long
    Dim lTotGravados As Long
    Dim oCobranca As Object
        
    'Instanciando o objeto de Contabilidade''''''''''''''''''''''''''''''''''''''
    Set oCobranca = CreateObject("SEGL0169.cls00215")
  
    Call oCobranca.ContabilizarVCResseguroAgendamento("SEGBR", _
                                                      App.Title, _
                                                      App.FileDescription, _
                                                      glAmbiente_id, _
                                                      cUserName, _
                                                      lTotLidos, _
                                                      lTotGravados)
    
    'Atualizando interface'''''''''''''''''''''''''''''''''''''''''''''''''''''''
    GrdResult.AddItem "Variação Cambial de Resseguro Agendamento" & vbTab & lTotLidos & vbTab & lTotGravados
    
    StbRodape.Panels(1).Text = "Selecione o(s) item(s) para contabilização."

    Exit Sub
  
Trata_Erro:
    Call TratarErro("ContabilizarVCResseguroAgendamento", Me.name)
    Call TerminaSEGBR

End Sub


Public Sub ContabilizarVCResseguroPremioIRB()

On Error GoTo Trata_Erro:
    
    Dim lTotLidos As Long
    Dim lTotGravados As Long
    Dim oCobranca As Object
        
    'Instanciando o objeto de Contabilidade''''''''''''''''''''''''''''''''''''''
    Set oCobranca = CreateObject("SEGL0169.cls00215")
  
    Call oCobranca.ContabilizarVCResseguroPremioIRB("SEGBR", _
                                                      App.Title, _
                                                      App.FileDescription, _
                                                      glAmbiente_id, _
                                                      cUserName, _
                                                      lTotLidos, _
                                                      lTotGravados)
    
    'Atualizando interface'''''''''''''''''''''''''''''''''''''''''''''''''''''''
    GrdResult.AddItem "Variação Cambial de Resseguro Prêmio IRB" & vbTab & lTotLidos & vbTab & lTotGravados
    
    StbRodape.Panels(1).Text = "Selecione o(s) item(s) para contabilização."

    Exit Sub
  
Trata_Erro:
    Call TratarErro("ContabilizarVCResseguroPremioIRB", Me.name)
    Call TerminaSEGBR

End Sub



Public Sub ContabilizarVCResseguroSinistroIRB()

On Error GoTo Trata_Erro:
    
    Dim lTotLidos As Long
    Dim lTotGravados As Long
    Dim oCobranca As Object
        
    'Instanciando o objeto de Contabilidade''''''''''''''''''''''''''''''''''''''
    Set oCobranca = CreateObject("SEGL0169.cls00215")
  
    Call oCobranca.ContabilizarVCResseguroSinistroIRB("SEGBR", _
                                                      App.Title, _
                                                      App.FileDescription, _
                                                      glAmbiente_id, _
                                                      cUserName, _
                                                      lTotLidos, _
                                                      lTotGravados)
    
    'Atualizando interface'''''''''''''''''''''''''''''''''''''''''''''''''''''''
    GrdResult.AddItem "Variação Cambial de Resseguro Sinistro IRB" & vbTab & lTotLidos & vbTab & lTotGravados
    
    StbRodape.Panels(1).Text = "Selecione o(s) item(s) para contabilização."

    Exit Sub
  
Trata_Erro:
    Call TratarErro("ContabilizarVCResseguroSinistroIRB", Me.name)
    Call TerminaSEGBR

End Sub
Public Sub ContabilizarPIP()
On Error GoTo Trata_Erro:
    
    Dim oPip As Object
    Dim lTotLidos As Long
    Dim lTotGravados As Long
        
    Set oPip = CreateObject("SEGL0169.cls00215")
  
    Call oPip.ContabilizarPIP("SEGBR", _
                                App.Title, _
                                App.FileDescription, _
                                glAmbiente_id, _
                                cUserName, _
                                lTotLidos, _
                                lTotGravados)
       
    GrdResult.AddItem "Contabilização da PIP" & vbTab & lTotLidos & vbTab & lTotGravados
    StbRodape.Panels(1).Text = "Selecione o(s) item(s) para contabilização."

    Exit Sub
      
Trata_Erro:
    Call TratarErro("ContabilizarPIP", Me.name)
    Call TerminaSEGBR
End Sub

'RMAURI - 18/02/2008
Public Sub ContabilizarAjusteRepasse()

On Error GoTo Trata_Erro:
    
    Dim oObjeto As Object
    Dim lTotLidos As Long
    Dim lTotGravados As Long
        
    Set oObjeto = CreateObject("SEGL0169.cls00215")
  
    Call oObjeto.ContabilizarAjusteRepasse("SEGBR", _
                                       App.Title, _
                                       App.FileDescription, _
                                       glAmbiente_id, _
                                       cUserName, _
                                       lTotLidos, _
                                       lTotGravados)
       
    GrdResult.AddItem "Contabilização do Ajuste de Repasse - OuroCard" & vbTab & lTotLidos & vbTab & lTotGravados
    StbRodape.Panels(1).Text = "Selecione o(s) item(s) para contabilização."

    Exit Sub
      
Trata_Erro:
    Call TratarErro("ContabilizarAjusteRepasse", Me.name)
    Call TerminaSEGBR
End Sub

Public Sub ContabilizarVisaNetRepasse()

On Error GoTo Trata_Erro:
    
    Dim oObjeto As Object
    Dim lTotLidos As Long
    Dim lTotGravados As Long
        
    Set oObjeto = CreateObject("SEGL0169.cls00215")
  
    Call oObjeto.ContabilizarVisaNetRepasse("SEGBR", _
                                            App.Title, _
                                            App.FileDescription, _
                                            glAmbiente_id, _
                                            cUserName, _
                                            lTotLidos, _
                                            lTotGravados)
       
    GrdResult.AddItem "Contabilização VisaNet de Repasse" & vbTab & lTotLidos & vbTab & lTotGravados
    StbRodape.Panels(1).Text = "Selecione o(s) item(s) para contabilização."

    Exit Sub
      
Trata_Erro:
    Call TratarErro("ContabilizarVisaNetRepasse", Me.name)
    Call TerminaSEGBR
End Sub

'' Ralves - Estimativa Resseguro '''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Public Sub ContabilizarEstimativaResseguro()

On Error GoTo Trata_Erro:
    
    Dim oMindep      As Object
    Dim lTotLidos    As Long
    Dim lTotGravados As Long
        
    Set oMindep = CreateObject("SEGL0169.cls00215")
  
    Call oMindep.ContabilizarEstimativaResseguro("SEGBR", _
                                                 App.Title, _
                                                 App.FileDescription, _
                                                 glAmbiente_id, _
                                                 cUserName, _
                                                 lTotLidos, _
                                                 lTotGravados)
       
    GrdResult.AddItem "Contabilização de Estimativa de Resseguro" & vbTab & lTotLidos & vbTab & lTotGravados
    StbRodape.Panels(1).Text = "Selecione o(s) item(s) para contabilização."

    Exit Sub
      
Trata_Erro:
    Call TratarErro("ContabilizarEstimativaResseguro", Me.name)
    Call TerminaSEGBR
End Sub

'' Ralves - Estimativa Sinistro - Circular 395 ''''''''''''''''''''''''''''''''''''''''''''''''''
Public Sub ContabilizarEstimativa()

On Error GoTo Trata_Erro:
    
    Dim oMindep      As Object
    Dim lTotLidos    As Long
    Dim lTotGravados As Long
        
    Set oMindep = CreateObject("SEGL0169.cls00215")
  
    Call oMindep.ContabilizarEstimativa("SEGBR", _
                                        App.Title, _
                                        App.FileDescription, _
                                        glAmbiente_id, _
                                        cUserName, _
                                        lTotLidos, _
                                        lTotGravados)
       
    GrdResult.AddItem "Contabilização de Estimativa de Sinistro" & vbTab & lTotLidos & vbTab & lTotGravados
    StbRodape.Panels(1).Text = "Selecione o(s) item(s) para contabilização."

    Exit Sub
      
Trata_Erro:
    Call TratarErro("ContabilizarEstimativa", Me.name)
    Call TerminaSEGBR
End Sub

'' Ralves - Estimativa Sinistro - Circular 395 ''''''''''''''''''''''''''''''''''''''''''''''''''
Public Sub ContabilizarPagamento()

On Error GoTo Trata_Erro:
    
    Dim oMindep      As Object
    Dim lTotLidos    As Long
    Dim lTotGravados As Long
        
    Set oMindep = CreateObject("SEGL0169.cls00215")
  
    Call oMindep.ContabilizarPagamento("SEGBR", _
                                       App.Title, _
                                       App.FileDescription, _
                                       glAmbiente_id, _
                                       cUserName, _
                                       lTotLidos, _
                                       lTotGravados)
       
    GrdResult.AddItem "Contabilização de Pagamento de Sinistro" & vbTab & lTotLidos & vbTab & lTotGravados
    StbRodape.Panels(1).Text = "Selecione o(s) item(s) para contabilização."

    Exit Sub
      
Trata_Erro:
    Call TratarErro("ContabilizarPagamento", Me.name)
    Call TerminaSEGBR
End Sub

'' fsa - Ajustamento MINDEP - Resseguro ''''''''''''''''''''''''''''''''''''''''''''''''''''
Public Sub ContabilizarMindep()

On Error GoTo Trata_Erro:
    
    Dim oMindep      As Object
    Dim lTotLidos    As Long
    Dim lTotGravados As Long
        
    Set oMindep = CreateObject("SEGL0169.cls00215")
  
    Call oMindep.ContabilizarMindep("SEGBR", _
                                    App.Title, _
                                    App.FileDescription, _
                                    glAmbiente_id, _
                                    cUserName, _
                                    lTotLidos, _
                                    lTotGravados)
       
    GrdResult.AddItem "Contabilização de Ajustamento Mindep - Resseguro" & vbTab & lTotLidos & vbTab & lTotGravados
    StbRodape.Panels(1).Text = "Selecione o(s) item(s) para contabilização."

    Exit Sub
      
Trata_Erro:
    Call TratarErro("ContabilizarMindep", Me.name)
    Call TerminaSEGBR
End Sub

Public Sub ContabilizarMindepTotal()

On Error GoTo Trata_Erro:
    
    Dim oMindep      As Object
    Dim lTotLidos    As Long
    Dim lTotGravados As Long
        
    Set oMindep = CreateObject("SEGL0169.cls00215")
  
    Call oMindep.ContabilizarMindepTotal("SEGBR", _
                                         App.Title, _
                                         App.FileDescription, _
                                         glAmbiente_id, _
                                         cUserName, _
                                         lTotLidos, _
                                         lTotGravados)
       
    GrdResult.AddItem "Contabilização de Ajustamento Mindep - Resseguro" & vbTab & lTotLidos & vbTab & lTotGravados
    StbRodape.Panels(1).Text = "Selecione o(s) item(s) para contabilização."

    Exit Sub
      
Trata_Erro:
    Call TratarErro("ContabilizarMindepTotal", Me.name)
    Call TerminaSEGBR
End Sub

Public Sub ContabilizarMindepParcelas()

On Error GoTo Trata_Erro:
    
    Dim oMindep      As Object
    Dim lTotLidos    As Long
    Dim lTotGravados As Long
        
    Set oMindep = CreateObject("SEGL0169.cls00215")
  
    Call oMindep.ContabilizarMindepParcelas("SEGBR", _
                                            App.Title, _
                                            App.FileDescription, _
                                            glAmbiente_id, _
                                            cUserName, _
                                            lTotLidos, _
                                            lTotGravados)
       
    GrdResult.AddItem "Contabilização de Ajustamento Mindep - Resseguro" & vbTab & lTotLidos & vbTab & lTotGravados
    StbRodape.Panels(1).Text = "Selecione o(s) item(s) para contabilização."

    Exit Sub
      
Trata_Erro:
    Call TratarErro("ContabilizarMindepParcelas", Me.name)
    Call TerminaSEGBR
End Sub

Public Sub ContabilizarMindepDiferenca()

On Error GoTo Trata_Erro:
    
    Dim oMindep      As Object
    Dim lTotLidos    As Long
    Dim lTotGravados As Long
        
    Set oMindep = CreateObject("SEGL0169.cls00215")
  
    Call oMindep.ContabilizarMindepDiferenca("SEGBR", _
                                             App.Title, _
                                             App.FileDescription, _
                                             glAmbiente_id, _
                                             cUserName, _
                                             lTotLidos, _
                                             lTotGravados)
       
    GrdResult.AddItem "Contabilização de Ajustamento Mindep - Resseguro" & vbTab & lTotLidos & vbTab & lTotGravados
    StbRodape.Panels(1).Text = "Selecione o(s) item(s) para contabilização."

    Exit Sub
      
Trata_Erro:
    Call TratarErro("ContabilizarMindepDiferenca", Me.name)
    Call TerminaSEGBR
End Sub

Public Sub ContabilizarBonificacaoRecusadas()

On Error GoTo Trata_Erro:
    
    Dim lTotLidos As Long
    Dim lTotGravados As Long
    Dim oEmissao As Object
        
    'Instanciando o objeto de Contabilidade''''''''''''''''''''''''''''''''''''''''''''''''
    Set oEmissao = CreateObject("SEGL0169.cls00215")
  
    Call oEmissao.ContabilizarBonificacaoRecusadas("SEGBR", _
                                                   App.Title, _
                                                   App.FileDescription, _
                                                   glAmbiente_id, _
                                                   cUserName, _
                                                   lTotLidos, _
                                                   lTotGravados)
    
    'Atualizando interface'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    GrdResult.AddItem "Bonificação - Recusadas" & vbTab & lTotLidos & vbTab & lTotGravados
   
    StbRodape.Panels(1).Text = "Selecione o(s) item(s) para contabilização."

    Exit Sub
  
Trata_Erro:
    Call TratarErro("ContabilizarBonificacaoRecusadas", Me.name)
    Call TerminaSEGBR
End Sub

Public Sub ContabilizarBonificacaoAceitas()

On Error GoTo Trata_Erro:
    
    Dim lTotLidos As Long
    Dim lTotGravados As Long
    Dim oEmissao As Object
        
    'Instanciando o objeto de Contabilidade''''''''''''''''''''''''''''''''''''''''''''''''
    Set oEmissao = CreateObject("SEGL0169.cls00215")
  
    Call oEmissao.ContabilizarBonificacaoAceitas("SEGBR", _
                                                 App.Title, _
                                                 App.FileDescription, _
                                                 glAmbiente_id, _
                                                 cUserName, _
                                                 lTotLidos, _
                                                 lTotGravados)
    
    'Atualizando interface'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    GrdResult.AddItem "Bonificação - Aceitas" & vbTab & lTotLidos & vbTab & lTotGravados
   
    StbRodape.Panels(1).Text = "Selecione o(s) item(s) para contabilização."

    Exit Sub
  
Trata_Erro:
    Call TratarErro("ContabilizarBonificacaoAceitas", Me.name)
    Call TerminaSEGBR
End Sub

Public Sub ContabilizarProLabore()

On Error GoTo Trata_Erro:
    
    Dim lTotLidos As Long
    Dim lTotGravados As Long
    Dim oEmissao As Object
        
    'Instanciando o objeto de Contabilidade''''''''''''''''''''''''''''''''''''''''''''''''
    Set oEmissao = CreateObject("SEGL0169.cls00215")
  
    Call oEmissao.ContabilizarProLabore("SEGBR", _
                                        App.Title, _
                                        App.FileDescription, _
                                        glAmbiente_id, _
                                        cUserName, _
                                        lTotLidos, _
                                        lTotGravados)
    
    'Atualizando interface'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    GrdResult.AddItem "Cobrança - Pró-Labore" & vbTab & lTotLidos & vbTab & lTotGravados
   
    StbRodape.Panels(1).Text = "Selecione o(s) item(s) para contabilização."

    Exit Sub
  
Trata_Erro:
    Call TratarErro("ContabilizarProLabore", Me.name)
    Call TerminaSEGBR
End Sub

Public Sub ContabilizarEmissaoAntecipada()

On Error GoTo Trata_Erro:
    
    Dim lTotLidos As Long
    Dim lTotGravados As Long
    Dim oEmissao As Object
        
    'Instanciando o objeto de Contabilidade''''''''''''''''''''''''''''''''''''''''''''''''
    Set oEmissao = CreateObject("SEGL0169.cls00215")
  
    Call oEmissao.ContabilizarEmissaoAntecipada("SEGBR", _
                                                App.Title, _
                                                App.FileDescription, _
                                                glAmbiente_id, _
                                                cUserName, _
                                                lTotLidos, _
                                                lTotGravados)
    
    'Atualizando interface'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    GrdResult.AddItem "Contabilizar Emissao Antecipada" & vbTab & lTotLidos & vbTab & lTotGravados
   
    StbRodape.Panels(1).Text = "Selecione o(s) item(s) para contabilização."

    Exit Sub
  
Trata_Erro:
    Call TratarErro("ContabilizarEmissaoAntecipada", Me.name)
    Call TerminaSEGBR
End Sub

Public Sub ContabilizarEmissao_AdesaoAntecipada()

On Error GoTo Trata_Erro:
    
    Dim lTotLidos As Long
    Dim lTotGravados As Long
    Dim oEmissao As Object
        
    'Instanciando o objeto de Contabilidade''''''''''''''''''''''''''''''''''''''''''''''''
    Set oEmissao = CreateObject("SEGL0169.cls00215")
  
    Call oEmissao.ContabilizarEmissao_AdesaoAntecipada("SEGBR", _
                                                       App.Title, _
                                                       App.FileDescription, _
                                                       glAmbiente_id, _
                                                       cUserName, _
                                                       lTotLidos, _
                                                       lTotGravados)
    
    'Atualizando interface'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    GrdResult.AddItem "Contabilizar Emissao Adesão Antecipada" & vbTab & lTotLidos & vbTab & lTotGravados
   
    StbRodape.Panels(1).Text = "Selecione o(s) item(s) para contabilização."

    Exit Sub
  
Trata_Erro:
    Call TratarErro("ContabilizarEmissao_AdesaoAntecipada", Me.name)
    Call TerminaSEGBR
End Sub

Public Sub ContabilizarRestituicaoAntecipada()

On Error GoTo Trata_Erro:
    
    Dim lTotLidos As Long
    Dim lTotGravados As Long
    Dim oEmissao As Object
        
    'Instanciando o objeto de Contabilidade''''''''''''''''''''''''''''''''''''''''''''''''
    Set oEmissao = CreateObject("SEGL0169.cls00215")
  
    Call oEmissao.ContabilizarRestituicaoAntecipada("SEGBR", _
                                                    App.Title, _
                                                    App.FileDescription, _
                                                    glAmbiente_id, _
                                                    cUserName, _
                                                    lTotLidos, _
                                                    lTotGravados)
    
    'Atualizando interface'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    GrdResult.AddItem "Restituição - Antecipada" & vbTab & lTotLidos & vbTab & lTotGravados
   
    StbRodape.Panels(1).Text = "Selecione o(s) item(s) para contabilização."

    Exit Sub
  
Trata_Erro:
    Call TratarErro("ContabilizarRestituicao", Me.name)
    Call TerminaSEGBR
End Sub

Public Sub ContabilizarCobrancaBaixaParcelaAntecipada()

On Error GoTo Trata_Erro:
    
    Dim lTotLidos As Long
    Dim lTotGravados As Long
    Dim oCobranca As Object
        
    'Instanciando o objeto de Contabilidade''''''''''''''''''''''''''''''''''''''''''''''''
    Set oCobranca = CreateObject("SEGL0169.cls00215")
  
    Call oCobranca.ContabilizarCobrancaBaixaParcelaAntecipada("SEGBR", _
                                                              App.Title, _
                                                              App.FileDescription, _
                                                              glAmbiente_id, _
                                                              cUserName, _
                                                              lTotLidos, _
                                                              lTotGravados)
    
    'Atualizando interface'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    GrdResult.AddItem "Cobranca - Baixa de Parcelas - Antecipada" & vbTab & lTotLidos & vbTab & lTotGravados
   
    StbRodape.Panels(1).Text = "Selecione o(s) item(s) para contabilização."

    Exit Sub
  
Trata_Erro:
    Call TratarErro("ContabilizarCobrancaBaixaParcelaAntecipada", Me.name)
    Call TerminaSEGBR
End Sub

Public Sub ContabilizarVisaNetBaixaParcelaAntecipada()

On Error GoTo Trata_Erro:
    
    Dim lTotLidos As Long
    Dim lTotGravados As Long
    Dim oCobranca As Object
        
    'Instanciando o objeto de Contabilidade''''''''''''''''''''''''''''''''''''''''''''''''
    Set oCobranca = CreateObject("SEGL0169.cls00215")
  
    Call oCobranca.ContabilizarVisaNetBaixaParcelaAntecipada("SEGBR", _
                                                             App.Title, _
                                                             App.FileDescription, _
                                                             glAmbiente_id, _
                                                             cUserName, _
                                                             lTotLidos, _
                                                             lTotGravados)
    
    'Atualizando interface'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    GrdResult.AddItem "Cobrança - Baixa VisaNet Agendamento - Antecipada" & vbTab & lTotLidos & vbTab & lTotGravados
   
    StbRodape.Panels(1).Text = "Selecione o(s) item(s) para contabilização."

    Exit Sub
  
Trata_Erro:
    Call TratarErro("ContabilizarVisaNetBaixaParcelaAntecipada", Me.name)
    Call TerminaSEGBR
End Sub

Public Sub ContabilizarEstornoRestituicao()

On Error GoTo Trata_Erro:
    
    Dim lTotLidos As Long
    Dim lTotGravados As Long
    Dim oEmissao As Object
        
    'Instanciando o objeto de Contabilidade''''''''''''''''''''''''''''''''''''''''''''''''
    Set oEmissao = CreateObject("SEGL0169.cls00215")
  
    Call oEmissao.ContabilizarEstornoRestituicao("SEGBR", _
                                                 App.Title, _
                                                 App.FileDescription, _
                                                 glAmbiente_id, _
                                                 cUserName, _
                                                 lTotLidos, _
                                                 lTotGravados)
    
    'Atualizando interface'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    GrdResult.AddItem "Estorno Restituição" & vbTab & lTotLidos & vbTab & lTotGravados
   
    StbRodape.Panels(1).Text = "Selecione o(s) item(s) para contabilização."

    Exit Sub
  
Trata_Erro:
    Call TratarErro("ContabilizarRestituicao", Me.name)
    Call TerminaSEGBR
End Sub

Public Sub ContabilizarEmissao_Novo()

On Error GoTo Trata_Erro:
    
    Dim lTotLidos As Long
    Dim lTotGravados As Long
    Dim oEmissao As Object
        
    'Instanciando o objeto de Contabilidade''''''''''''''''''''''''''''''''''''''''''''''''
    Set oEmissao = CreateObject("SEGL0169.cls00215")
  
    Call oEmissao.ContabilizarEmissao_Novo("SEGBR", _
                                           App.Title, _
                                           App.FileDescription, _
                                           glAmbiente_id, _
                                           cUserName, _
                                           lTotLidos, _
                                           lTotGravados)
    
    'Atualizando interface'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    GrdResult.AddItem "Contabilizar Emissao" & vbTab & lTotLidos & vbTab & lTotGravados
   
    StbRodape.Panels(1).Text = "Selecione o(s) item(s) para contabilização."

    Exit Sub
  
Trata_Erro:
    Call TratarErro("ContabilizarEmissao_Novo", Me.name)
    Call TerminaSEGBR
End Sub

Public Sub ContabilizarEmissaoEndosso_Novo()

On Error GoTo Trata_Erro:
    
    Dim lTotLidos As Long
    Dim lTotGravados As Long
    Dim oEmissao As Object
        
    'Instanciando o objeto de Contabilidade''''''''''''''''''''''''''''''''''''''''''''''''
    Set oEmissao = CreateObject("SEGL0169.cls00215")
  
    Call oEmissao.ContabilizarEmissaoEndosso_Novo("SEGBR", _
                                                  App.Title, _
                                                  App.FileDescription, _
                                                  glAmbiente_id, _
                                                  cUserName, _
                                                  lTotLidos, _
                                                  lTotGravados)
    
    'Atualizando interface'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    GrdResult.AddItem "Contabilizar Emissao Endosso" & vbTab & lTotLidos & vbTab & lTotGravados
   
    StbRodape.Panels(1).Text = "Selecione o(s) item(s) para contabilização."

    Exit Sub
  
Trata_Erro:
    Call TratarErro("ContabilizarEmissaoEndosso_Novo", Me.name)
    Call TerminaSEGBR
End Sub

Public Sub ContabilizarCobrancaBaixaParcela_NOVO()

On Error GoTo Trata_Erro:
    
    Dim lTotLidos As Long
    Dim lTotGravados As Long
    Dim oCobranca As Object
        
    'Instanciando o objeto de Contabilidade''''''''''''''''''''''''''''''''''''''''''''''''
    Set oCobranca = CreateObject("SEGL0169.cls00215")
  
    Call oCobranca.ContabilizarCobrancaBaixaParcela_NOVO("SEGBR", _
                                                         App.Title, _
                                                         App.FileDescription, _
                                                         glAmbiente_id, _
                                                         cUserName, _
                                                         lTotLidos, _
                                                         lTotGravados)
    
    'Atualizando interface'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    GrdResult.AddItem "Cobranca - Baixa de Parcelas - NOVO" & vbTab & lTotLidos & vbTab & lTotGravados
   
    StbRodape.Panels(1).Text = "Selecione o(s) item(s) para contabilização."

    Exit Sub
  
Trata_Erro:
    Call TratarErro("ContabilizarCobrancaBaixaParcela_NOVO", Me.name)
    Call TerminaSEGBR
End Sub

Public Sub ContabilizarRestituicao_NOVO()

On Error GoTo Trata_Erro:
    
    Dim lTotLidos As Long
    Dim lTotGravados As Long
    Dim oEmissao As Object
        
    'Instanciando o objeto de Contabilidade''''''''''''''''''''''''''''''''''''''''''''''''
    Set oEmissao = CreateObject("SEGL0169.cls00215")
  
    Call oEmissao.ContabilizarRestituicao_NOVO("SEGBR", _
                                               App.Title, _
                                               App.FileDescription, _
                                               glAmbiente_id, _
                                               cUserName, _
                                               lTotLidos, _
                                               lTotGravados)
    
    'Atualizando interface'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    GrdResult.AddItem "Restituição - NOVO" & vbTab & lTotLidos & vbTab & lTotGravados
   
    StbRodape.Panels(1).Text = "Selecione o(s) item(s) para contabilização."

    Exit Sub
  
Trata_Erro:
    Call TratarErro("ContabilizarRestituicao_NOVO ", Me.name)
    Call TerminaSEGBR
End Sub

'Zoro.Gomes - Confitec - 18699317 - Devoluções de Restituição de Prêmio - Fase 2 - Onda 1 - 22/03/2016
Public Sub ContabilizarRestituicoes_Devolvidas()

On Error GoTo Trata_Erro:
    
    Dim lTotLidos As Long
    Dim lTotGravados As Long
    Dim oEmissao As Object
        
    'Instanciando o objeto de Contabilidade''''''''''''''''''''''''''''''''''''''''''''''''
    Set oEmissao = CreateObject("SEGL0169.cls00215")
  
    Call oEmissao.ContabilizarRestituicoes_Devolvidas("SEGBR", _
                                               App.Title, _
                                               App.FileDescription, _
                                               glAmbiente_id, _
                                               cUserName, _
                                               lTotLidos, _
                                               lTotGravados)
    
    'Atualizando interface'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    GrdResult.AddItem "Restituição - Devolvidas" & vbTab & lTotLidos & vbTab & lTotGravados
   
    StbRodape.Panels(1).Text = "Selecione o(s) item(s) para contabilização."

    Exit Sub
  
Trata_Erro:
    Call TratarErro("ContabilizarRestituicoes_Devolvidas ", Me.name)
    Call TerminaSEGBR
End Sub

Public Sub ContabilizarCancelamentoCancelamento()

On Error GoTo Trata_Erro:
    
    Dim lTotLidos As Long
    Dim lTotGravados As Long
    Dim oEmissao As Object
        
    'Instanciando o objeto de Contabilidade''''''''''''''''''''''''''''''''''''''''''''''''
    Set oEmissao = CreateObject("SEGL0169.cls00215")
  
    Call oEmissao.ContabilizarCancelamentoCancelamento("SEGBR", _
                                                       App.Title, _
                                                       App.FileDescription, _
                                                       glAmbiente_id, _
                                                       cUserName, _
                                                       lTotLidos, _
                                                       lTotGravados)
    
    'Atualizando interface'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    GrdResult.AddItem "Cancelamento de Cancelamento" & vbTab & lTotLidos & vbTab & lTotGravados
   
    StbRodape.Panels(1).Text = "Selecione o(s) item(s) para contabilização."

    Exit Sub
  
Trata_Erro:
    Call TratarErro("ContabilizarCancelamentoCancelamento ", Me.name)
    Call TerminaSEGBR
End Sub

Public Sub ContabilizarCancelamentoProposta()

On Error GoTo Trata_Erro:
    
    Dim lTotLidos As Long
    Dim lTotGravados As Long
    Dim oEmissao As Object
        
    'Instanciando o objeto de Contabilidade''''''''''''''''''''''''''''''''''''''''''''''''
    Set oEmissao = CreateObject("SEGL0169.cls00215")
  
    Call oEmissao.ContabilizarCancelamentoProposta("SEGBR", _
                                                   App.Title, _
                                                   App.FileDescription, _
                                                   glAmbiente_id, _
                                                   cUserName, _
                                                   lTotLidos, _
                                                   lTotGravados)
    
    'Atualizando interface'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    GrdResult.AddItem "Cancelamento de Proposta" & vbTab & lTotLidos & vbTab & lTotGravados
   
    StbRodape.Panels(1).Text = "Selecione o(s) item(s) para contabilização."

    Exit Sub
  
Trata_Erro:
    Call TratarErro("ContabilizarCancelamentoProposta ", Me.name)
    Call TerminaSEGBR
End Sub

Public Sub ContabilizarCancelamentoEndosso()

On Error GoTo Trata_Erro:
    
    Dim lTotLidos As Long
    Dim lTotGravados As Long
    Dim oEmissao As Object
        
    'Instanciando o objeto de Contabilidade''''''''''''''''''''''''''''''''''''''''''''''''
    Set oEmissao = CreateObject("SEGL0169.cls00215")
  
    Call oEmissao.ContabilizarCancelamentoEndosso("SEGBR", _
                                                  App.Title, _
                                                  App.FileDescription, _
                                                  glAmbiente_id, _
                                                  cUserName, _
                                                  lTotLidos, _
                                                  lTotGravados)
    
    'Atualizando interface'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    GrdResult.AddItem "Cancelamento de Endosso" & vbTab & lTotLidos & vbTab & lTotGravados
   
    StbRodape.Panels(1).Text = "Selecione o(s) item(s) para contabilização."

    Exit Sub
  
Trata_Erro:
    Call TratarErro("ContabilizarCancelamentoEndosso", Me.name)
    Call TerminaSEGBR
End Sub

Public Sub ContabilizarEstornoRestituicao_NOVO()

On Error GoTo Trata_Erro:
    
    Dim lTotLidos As Long
    Dim lTotGravados As Long
    Dim oEmissao As Object
        
    'Instanciando o objeto de Contabilidade''''''''''''''''''''''''''''''''''''''''''''''''
    Set oEmissao = CreateObject("SEGL0169.cls00215")
  
    Call oEmissao.ContabilizarEstornoRestituicao_NOVO("SEGBR", _
                                                      App.Title, _
                                                      App.FileDescription, _
                                                      glAmbiente_id, _
                                                      cUserName, _
                                                      lTotLidos, _
                                                      lTotGravados)
    
    'Atualizando interface'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    GrdResult.AddItem "Estorno Restituicao - NOVO" & vbTab & lTotLidos & vbTab & lTotGravados
   
    StbRodape.Panels(1).Text = "Selecione o(s) item(s) para contabilização."

    Exit Sub
  
Trata_Erro:
    Call TratarErro("ContabilizarEstornoRestituicao_NOVO", Me.name)
    Call TerminaSEGBR
End Sub

Public Sub ContabilizarEmissaoAntecipada_NOVO()

On Error GoTo Trata_Erro:
    
    Dim lTotLidos As Long
    Dim lTotGravados As Long
    Dim oEmissao As Object
        
    'Instanciando o objeto de Contabilidade''''''''''''''''''''''''''''''''''''''''''''''''
    Set oEmissao = CreateObject("SEGL0169.cls00215")
  
    Call oEmissao.ContabilizarEmissaoAntecipada_NOVO("SEGBR", _
                                                      App.Title, _
                                                      App.FileDescription, _
                                                      glAmbiente_id, _
                                                      cUserName, _
                                                      lTotLidos, _
                                                      lTotGravados)
    
    'Atualizando interface'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    GrdResult.AddItem "Contabilizar Emissao Antecipada - NOVO" & vbTab & lTotLidos & vbTab & lTotGravados
   
    StbRodape.Panels(1).Text = "Selecione o(s) item(s) para contabilização."

    Exit Sub
  
Trata_Erro:
    Call TratarErro("ContabilizarEmissaoAntecipada_NOVO", Me.name)
    Call TerminaSEGBR
End Sub

Public Sub ContabilizarCobrancaBaixaParcelaAntecipada_NOVO()

On Error GoTo Trata_Erro:
    
    Dim lTotLidos As Long
    Dim lTotGravados As Long
    Dim oEmissao As Object
        
    'Instanciando o objeto de Contabilidade''''''''''''''''''''''''''''''''''''''''''''''''
    Set oEmissao = CreateObject("SEGL0169.cls00215")
  
    Call oEmissao.ContabilizarCobrancaBaixaParcelaAntecipada_NOVO("SEGBR", _
                                                                  App.Title, _
                                                                  App.FileDescription, _
                                                                  glAmbiente_id, _
                                                                  cUserName, _
                                                                  lTotLidos, _
                                                                  lTotGravados)
    
    'Atualizando interface'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    GrdResult.AddItem "Contabilizar Cobranca Baixa Parcela Antecipada - NOVO" & vbTab & lTotLidos & vbTab & lTotGravados
   
    StbRodape.Panels(1).Text = "Selecione o(s) item(s) para contabilização."

    Exit Sub
  
Trata_Erro:
    Call TratarErro("ContabilizarCobrancaBaixaParcelaAntecipada_NOVO", Me.name)
    Call TerminaSEGBR
End Sub

Public Sub ContabilizarRestituicaoAntecipada_NOVO()

On Error GoTo Trata_Erro:
    
    Dim lTotLidos As Long
    Dim lTotGravados As Long
    Dim oEmissao As Object
        
    'Instanciando o objeto de Contabilidade''''''''''''''''''''''''''''''''''''''''''''''''
    Set oEmissao = CreateObject("SEGL0169.cls00215")
  
    Call oEmissao.ContabilizarRestituicaoAntecipada_NOVO("SEGBR", _
                                                         App.Title, _
                                                         App.FileDescription, _
                                                         glAmbiente_id, _
                                                         cUserName, _
                                                         lTotLidos, _
                                                         lTotGravados)
    
    'Atualizando interface'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    GrdResult.AddItem "Contabilizar Restituicao Antecipada - NOVO" & vbTab & lTotLidos & vbTab & lTotGravados
   
    StbRodape.Panels(1).Text = "Selecione o(s) item(s) para contabilização."

    Exit Sub
  
Trata_Erro:
    Call TratarErro("ContabilizarRestituicaoAntecipada_NOVO", Me.name)
    Call TerminaSEGBR
End Sub

'Carlos Henrique 14/06/2011 inicio
'FLOW 11319563
'Acerto de Fechamento
Public Sub Contabilizar_Atualizacao_Monetaria_sinistro()
On Error GoTo Trata_Erro:
    
    Dim oContabilizar As Object
    Dim lTotLidos As Long
    Dim lTotGravados As Long
        
    Set oContabilizar = CreateObject("SEGL0169.cls00215")
  
    Call oContabilizar.Contabilizar_Atualizacao_Monetaria_sinistro("SEGBR", _
                                App.Title, _
                                App.FileDescription, _
                                glAmbiente_id, _
                                cUserName, _
                                lTotLidos, _
                                lTotGravados)
       
    GrdResult.AddItem "Contabilização da Atualização Monetária de Sinistro" & vbTab & lTotLidos & vbTab & lTotGravados
    StbRodape.Panels(1).Text = "Selecione o(s) item(s) para contabilização."

    Exit Sub
      
Trata_Erro:
    Call TratarErro("Contabilizar_Atualizacao_Monetaria_sinistro", Me.name)
    Call TerminaSEGBR
End Sub
'Carlos Henrique 14/06/2011 fim
'FLOW 11319563
'Acerto de Fechamento


' - jarbas - 29/11/2017
Public Sub IncluirLogProcessamentoTarefa(ByVal sSigla_tarefa As String, _
                                         ByVal Data_Sistema As String, _
                                         ByVal sHorario_inicio_tarefa As String, _
                                         ByVal sAtividade As String, _
                                         ByVal cUserName As String, _
                                         ByVal glAmbiente_id As String)
                                      
    Dim sSQL As String
    
    '' hora inicio da atividade
    
    sHorario_inicio_atividade = Now
    
On Error GoTo Trata_Erro:

    sSQL = ""
    sSQL = "EXEC controle_sistema_db..SEGS8602_SPI " 'controle_sistema_db..log_processamento_tarefa_tb
    sSQL = sSQL & "  '" & sSigla_tarefa & "'"
    sSQL = sSQL & ", '" & Format(Data_Sistema, "yyyymmdd") & "'"
        sSQL = sSQL & ", '" & sAtividade & "'"
    sSQL = sSQL & ", '" & Format(sHorario_inicio_tarefa, "yyyymmdd hh:mm:ss") & "'"
    
    If sHorario_inicio_atividade = "" Then
        sSQL = sSQL & ", NULL"
    Else
        sSQL = sSQL & ", '" & Format(sHorario_inicio_atividade, "yyyymmdd  hh:mm:ss") & "'"
    End If
    sSQL = sSQL & ", '" & cUserName & "'"

     Call ExecutarSQL(gsSIGLASISTEMA, _
                         glAmbiente_id, _
                         App.Title, _
                         App.FileDescription, _
                         sSQL, _
                         False)

    Exit Sub
  
Trata_Erro:
    Call TratarErro("Erro ao na rotina IncluirLogProcessamentoTarefa - " & sAtividade, Me.name)
    Call TerminaSEGBR
End Sub

