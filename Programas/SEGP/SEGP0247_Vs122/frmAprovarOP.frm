VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmAprovarOP 
   Caption         =   "Aprovar OP"
   ClientHeight    =   6525
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   9570
   LinkTopic       =   "Form1"
   ScaleHeight     =   6525
   ScaleWidth      =   9570
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton BtnCancelar 
      Caption         =   "&Cancelar"
      Height          =   375
      Left            =   6840
      TabIndex        =   7
      Top             =   5760
      Width           =   1215
   End
   Begin VB.CommandButton BtnAplicar 
      Caption         =   "&Aplicar"
      Height          =   375
      Left            =   8160
      TabIndex        =   6
      Top             =   5760
      Width           =   1215
   End
   Begin VB.CommandButton BtnOk 
      Caption         =   "&Ok"
      Height          =   375
      Left            =   5520
      TabIndex        =   5
      Top             =   5760
      Width           =   1215
   End
   Begin TabDlg.SSTab SSTab1 
      Height          =   5535
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   9375
      _ExtentX        =   16536
      _ExtentY        =   9763
      _Version        =   327680
      Tabs            =   1
      TabsPerRow      =   1
      TabHeight       =   520
      TabMaxWidth     =   2646
      TabCaption(0)   =   "Aprova��o"
      TabPicture(0)   =   "frmAprovarOP.frx":0000
      Tab(0).ControlCount=   4
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "frameParcelas"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "Frame1"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "frameResultado"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).Control(3)=   "frameAplicar"
      Tab(0).Control(3).Enabled=   0   'False
      Begin VB.Frame frameAplicar 
         Caption         =   "Aplicar a"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   735
         Left            =   3240
         TabIndex        =   12
         Top             =   4680
         Width           =   3015
         Begin VB.OptionButton Option4 
            Caption         =   "Todas"
            Height          =   375
            Left            =   1800
            TabIndex        =   14
            Top             =   240
            Width           =   1095
         End
         Begin VB.OptionButton Option3 
            Caption         =   "Selecionada"
            Height          =   375
            Left            =   240
            TabIndex        =   13
            Top             =   240
            Value           =   -1  'True
            Width           =   1575
         End
      End
      Begin VB.Frame frameResultado 
         Caption         =   "Resultado"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   735
         Left            =   120
         TabIndex        =   9
         Top             =   4680
         Width           =   3015
         Begin VB.OptionButton Option2 
            Caption         =   "Rejeitado"
            Height          =   375
            Left            =   1680
            TabIndex        =   11
            Top             =   240
            Width           =   1215
         End
         Begin VB.OptionButton Option1 
            Caption         =   "Aprovado"
            Height          =   375
            Left            =   240
            TabIndex        =   10
            Top             =   240
            Value           =   -1  'True
            Width           =   1215
         End
      End
      Begin VB.Frame Frame1 
         Caption         =   "Aguardando Aprova��o"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   2055
         Left            =   120
         TabIndex        =   2
         Top             =   360
         Width           =   9135
         Begin MSFlexGridLib.MSFlexGrid flexParaAprovar 
            Height          =   1695
            Left            =   120
            TabIndex        =   3
            Top             =   240
            Width           =   8895
            _ExtentX        =   15690
            _ExtentY        =   2990
            _Version        =   327680
            Cols            =   6
            FixedCols       =   0
            ScrollBars      =   2
            FormatString    =   "N�mero       |Item                                     |Data Emiss�o|Data Acerto|Valor Acerto|Forma de Pagamento      "
         End
      End
      Begin VB.Frame frameParcelas 
         Caption         =   "Composi��o"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   2055
         Left            =   120
         TabIndex        =   1
         Top             =   2520
         Visible         =   0   'False
         Width           =   9135
         Begin MSFlexGridLib.MSFlexGrid flexComposicao 
            Height          =   1695
            Left            =   120
            TabIndex        =   4
            Top             =   240
            Width           =   8895
            _ExtentX        =   15690
            _ExtentY        =   2990
            _Version        =   327680
            Cols            =   5
            FixedCols       =   0
            Enabled         =   0   'False
            FillStyle       =   1
            ScrollBars      =   2
            SelectionMode   =   1
            AllowUserResizing=   1
            FormatString    =   $"frmAprovarOP.frx":001C
         End
      End
   End
   Begin MSComctlLib.StatusBar StatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   8
      Top             =   6240
      Width           =   9570
      _ExtentX        =   16880
      _ExtentY        =   503
      Style           =   1
      SimpleText      =   ""
      _Version        =   327680
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
         NumPanels       =   1
         BeginProperty Panel1 {0713E89F-850A-101B-AFC0-4210102A8DA7} 
            TextSave        =   ""
            Object.Tag             =   ""
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmAprovarOP"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Form_Load()
'Inserido na Migra��o para VB6
    Me.Caption = SEG20247
End Sub
