Attribute VB_Name = "Acertos_Proc"
Public Const conMailLongDate = 0
Public Const conMailListView = 1

Public Const conOptionGeneral = 1       ' Constant for Option Dialog Type - General Options
Public Const conOptionMessage = 2       ' Constant for Option Dialog Type - Message Options

Public Const conUnreadMessage = "*"     ' Constant for string to indicate unread message

Public Const vbRecipTypeTo = 1
Public Const vbRecipTypeCc = 2

Public Const vbMessageFetch = 1
Public Const vbMessageSendDlg = 2
Public Const vbMessageSend = 3
Public Const vbMessageSaveMsg = 4
Public Const vbMessageCopy = 5
Public Const vbMessageCompose = 6
Public Const vbMessageReply = 7
Public Const vbMessageReplyAll = 8
Public Const vbMessageForward = 9
Public Const vbMessageDelete = 10
Public Const vbMessageShowAdBook = 11
Public Const vbMessageShowDetails = 12
Public Const vbMessageResolveName = 13
Public Const vbRecipientDelete = 14
Public Const vbAttachmentDelete = 15

Public Const vbAttachTypeData = 0
Public Const vbAttachTypeEOLE = 1
Public Const vbAttachTypeSOLE = 2

Type ListDisplay
    name As String * 20
    Subject As String * 40
    Date As String * 20
End Type

Public currentRCIndex As Integer
Public UnRead As Integer
Public SendWithMapi As Integer
Public ReturnRequest As Integer
Public OptionType As Integer
Public E_Mail As String

'-----------------------------
'Luciana - 03/10/2003
'-----------------------------
Public bFlexSelecao As Boolean
'-----------------------------

'-----------------------------
'Luciana - 19/01/2004
'-----------------------------
Public chamado_246 As Boolean
'-----------------------------

' Windows API functions
#If Win32 Then
    Declare Function GetProfileString Lib "kernel32" (ByVal lpAppName As String, lpKeyName As Any, ByVal lpDefault As String, ByVal lpReturnedString As String, ByVal nSize As Long) As Long
#Else
    Declare Function GetProfileString% Lib "Kernel" (ByVal lpSection$, ByVal lpEntry$, ByVal lpDefault$, ByVal buffer$, ByVal cbBuffer%)
#End If
' *****************************

Public MensagemAcerto As String
Public Destinatario As String
Public voper As String
Public vopercoseguro As String
Public FlagRetorno As Boolean
Public vTpRamo As String
Public vSinistro As String
Public vSituacao As String
Public vSituacao_Sinistro As String

Global SinistrosSelecionados As New Collection
Global SinistroEmUso As New Collection
Global Pagamentos As New Collection

Public rdocn1 As New rdoConnection

Sub Main()

    '****************************************************************
    '*  Projeto:    495123 - Altera��o de al�adas t�cnicos sinistro *
    '*  Sistena:    SEGBR - Sistema de Seguros                      *
    '*  Data:       03/09/2008                                      *
    '*  Autor:      M�rcio Ossamu Yoshimura                         *
    '****************************************************************
        
    '********************************************************
    '*  Desenvolvido por:   GPTI                            *
    '*  Programado por:     Afonso Dutra Nogueira Filho     *
    '*  Data:               23/09/2008                      *
    '********************************************************

    '********************************
    '*   AFONSO - GPTI              *
    '*   3 - QUALIDADE              *
    '*   2 - EXTRATOR               *
    '*  'cUserName = "14241538835"  *
    '*  'MYOSHIMURA login do Marcio *
    '********************************
    
    glAmbiente_id = 3
    cUserName = "producao3"
  
  Dim i As Byte
    
    'Abre a conex�o com o SEGBR
    Conexao
    Conexao_Auxiliar
    
    'Diego Galizoni Caversan - GPTI - 12/11/2009
    'Demanda 1445418 - N�o permitir que um tecnico aprove sua pr�pria solicita��o
    'Trocar o username pelo CPF
    'cusername = "37107403800"
    cUserName = gsCPF
    
    
    
    '--------------------
    'Luciana - 19/01/2004
    '--------------------
    chamado_246 = False
    '--------------------
    
    'IS 112000019660110001574410000006785
    
    If Len(gsParamAplicacao) > 2 Then
        Call Seguranca("SEGP0247.1", "Aprovar Pagamentos")
        '--------------------
        'Luciana - 19/01/2004
        '--------------------
        chamado_246 = True
        '--------------------
        frmAcertoCon.Show
    End If
    
    Select Case gsParamAplicacao
    Case "I"    'Aprovar Acerto
        Call Seguranca("SEGP0247.1", "Aprovar Pagamentos")
        frmAcertoCon.Show
        
    Case "A"    'Alterar Acerto
        Call Seguranca("SEGP0247.2", "Alterar Situa��o Pagamentos")
        frmAcertoCon.Show
        
    Case "R"    'Reapresentar Acerto
        Call Seguranca("SEGP0247.3", "Reapresentar Pagamentos")
        frmAcertoCon.Show
        
    Case "C"    'Consultar Acerto
        Call Seguranca("SEGP0247.4", "Consultar Pagamentos/Recebimentos")
        frmAcertoCon.Show
        
    Case "IC"    'Aprovar Acerto Co-seguro
        Call Seguranca("SEGP0247.5", "Aprovar Pagamentos Co-seguro")
        frmAcertoCon.Show
        
    Case "AC"    'Alterar Acerto Co-seguro
        Call Seguranca("SEGP0247.6", "Alterar Situa��o Pagamentos Co-seguro")
        frmAcertoCon.Show
        
    Case "RC"    'Reapresentar Acerto Co-seguro
        Call Seguranca("SEGP0247.7", "Reapresentar Pagamentos Co-seguro")
        frmAcertoCon.Show
        
    Case "CC"    'Consultar Acerto Co-seguro
        Call Seguranca("SEGP0247.8", "Consultar Pagamentos/Recebimentos Co-seguro")
        frmAcertoCon.Show
        
    Case "L"    'Aprova��o em Lote
        Load frm_acerto_lote
        frm_acerto_lote.Show
        
    End Select
    
End Sub

Function DateFromMapiDate$(ByVal S$, wFormat%)
' This procedure formats a MAPI date in one of
' two formats for viewing the message.
    y$ = Left$(S$, 4)
    M$ = Mid$(S$, 6, 2)
    d$ = Mid$(S$, 9, 2)
    T$ = Mid$(S$, 12)
    Ds# = DateValue(M$ + "/" + d$ + "/" + y$) + TimeValue(T$)
    Select Case wFormat
        Case conMailLongDate
            F$ = "dddd, mmmm d, yyyy, h:mmAM/PM"
        Case conMailListView
            F$ = "mm/dd/yy hh:mm"
    End Select
    DateFromMapiDate = Format$(Ds#, F$)
End Function

Sub KillRecips(MsgControl As Control)
    ' Delete each recipient.  Loop until no recipients exist.
    While MsgControl.RecipCount
        MsgControl.Action = vbRecipientDelete
    Wend
End Sub

Sub SetRCList(ByVal NameList As String, MSG As Control, RCType As Integer, fResolveNames As Integer)
' Given a list of recipients:
'
'       Person 1;Person 2;Person 3
'
' this procedure places the names into the Msg.Recip
' structures.
    
    If NameList = "" Then
        Exit Sub
    End If

    i = MSG.RecipCount
    Do
        MSG.RecipIndex = i
        MSG.RecipDisplayName = Trim$(Token(NameList, ";"))
        If fResolveNames Then
            MSG.Action = vbMessageResolveName
        End If
        MSG.RecipType = RCType
        i = i + 1
    Loop Until (NameList = "")
End Sub

Function Token$(tmp$, search$)
    x = InStr(1, tmp$, search$)
    If x Then
       Token$ = Mid$(tmp$, 1, x - 1)
       tmp$ = Mid$(tmp$, x + 1)
    Else
       Token$ = tmp$
       tmp$ = ""
    End If
End Function

Public Sub Logon_Click()
    ' Log onto the mail system.
       
    On Error Resume Next
    frmAcerto.MAPISess.username = cUserName  '"BRASILSEG"
    frmAcerto.MAPISess.Action = 1
    If err <> 0 Then
        MsgBox "Logon Failure: " + Error$
    Else
        Screen.MousePointer = 0
        frmAcerto.MAPIMess.SessionID = frmAcerto.MAPISess.SessionID
    End If
End Sub

Public Function SendMsg(vTipo As String) As Boolean

    On Error GoTo Erro
    
    SendMsg = False
    frmAcerto.MAPIMess.Compose
    If vTipo = "r" Then
        frmAcerto.MAPIMess.MsgSubject = "Rejei��o de solicita��o de pagamento"
        frmAcerto.MAPIMess.MsgNoteText = MensagemAcerto
    Else
        frmAcerto.MAPIMess.MsgSubject = "Cancelamento de solicita��o de pagamento"
        frmAcerto.MAPIMess.MsgNoteText = MensagemAcerto
    End If
    frmAcerto.MAPIMess.MsgReceiptRequested = ReturnRequest
    Call KillRecips(frmAcerto.MAPIMess)
    Call SetRCList(Destinatario, frmAcerto.MAPIMess, vbRecipTypeTo, False)
                  
    On Error Resume Next
    frmAcerto.MAPIMess.Action = vbMessageSend
    If err Then
        'MsgBox "Erro mandando mensagem: " & vbNewLine + Str$(Err) + Error
        If MsgBox("N�o foi poss�vel enviar a mensagem, pois " & _
               "seu programa de e-mail n�o est� aberto. Continua ?", vbYesNo, "Aten��o") = vbYes Then
            SendMsg = True
        End If
        Exit Function
    End If
    SendMsg = True

    Exit Function

Erro:
    If err.Number = 32053 Then
        If MsgBox("N�o foi poss�vel enviar a mensagem, pois " & _
               "seu programa de e-mail n�o est� aberto. Continua ?", vbYesNo, "Aten��o") = vbYes Then
            SendMsg = True
        End If
    End If

End Function

Sub Conexao_Auxiliar()
   
    On Error GoTo Erro
    
    With rdocn1
        .Connect = rdocn.Connect
        .CursorDriver = rdUseServer
        .EstablishConnection rdDriverNoPrompt
    End With
   
    Exit Sub

Erro:
    mensagem_erro 6, "Conex�o com BRCAPDB indispon�vel."
    End
    
End Sub
