VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFlxGrd.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Object = "{20C62CAE-15DA-101B-B9A8-444553540000}#1.1#0"; "msmapi32.Ocx"
Begin VB.Form frmAcerto 
   Caption         =   "SEG12018 - Acerto de Contas de Sinistro - "
   ClientHeight    =   8595
   ClientLeft      =   6630
   ClientTop       =   2475
   ClientWidth     =   10770
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   8595
   ScaleWidth      =   10770
   Begin MSMAPI.MAPIMessages MAPIMess 
      Left            =   810
      Top             =   7830
      _ExtentX        =   1005
      _ExtentY        =   1005
      _Version        =   393216
      AddressEditFieldCount=   1
      AddressModifiable=   0   'False
      AddressResolveUI=   0   'False
      FetchSorted     =   0   'False
      FetchUnreadOnly =   0   'False
   End
   Begin MSMAPI.MAPISession MAPISess 
      Left            =   210
      Top             =   7830
      _ExtentX        =   1005
      _ExtentY        =   1005
      _Version        =   393216
      DownloadMail    =   0   'False
      LogonUI         =   -1  'True
      NewSession      =   0   'False
   End
   Begin VB.CommandButton BtnOk 
      Caption         =   "&Ok"
      Height          =   375
      Left            =   6840
      TabIndex        =   4
      Top             =   7935
      Width           =   1215
   End
   Begin VB.CommandButton BtnAplicar 
      Caption         =   "&Aplicar"
      Height          =   375
      Left            =   9480
      TabIndex        =   6
      Top             =   7935
      Width           =   1215
   End
   Begin VB.CommandButton BtnCancelar 
      Caption         =   "&Cancelar"
      Height          =   375
      Left            =   8160
      TabIndex        =   5
      Top             =   7935
      Width           =   1215
   End
   Begin TabDlg.SSTab SSTab1 
      Height          =   7695
      Left            =   90
      TabIndex        =   7
      Top             =   120
      Width           =   10590
      _ExtentX        =   18680
      _ExtentY        =   13573
      _Version        =   393216
      Tabs            =   1
      TabsPerRow      =   1
      TabHeight       =   520
      TabMaxWidth     =   5292
      TabCaption(0)   =   "Pagamentos/Recebimentos"
      TabPicture(0)   =   "frmAcerto.frx":0000
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "frameCancReapresentar"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "FrameVoucher"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "frameEstornar"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).Control(3)=   "frameCancelar"
      Tab(0).Control(3).Enabled=   0   'False
      Tab(0).Control(4)=   "frameAplicar"
      Tab(0).Control(4).Enabled=   0   'False
      Tab(0).Control(5)=   "frameResultado"
      Tab(0).Control(5).Enabled=   0   'False
      Tab(0).Control(6)=   "framePagamentos"
      Tab(0).Control(6).Enabled=   0   'False
      Tab(0).Control(7)=   "frameRejeitado"
      Tab(0).Control(7).Enabled=   0   'False
      Tab(0).Control(8)=   "frameDetalhamento"
      Tab(0).Control(8).Enabled=   0   'False
      Tab(0).Control(9)=   "frameAutorizacao"
      Tab(0).Control(9).Enabled=   0   'False
      Tab(0).Control(10)=   "fraPagamentosAprovados"
      Tab(0).Control(10).Enabled=   0   'False
      Tab(0).Control(11)=   "frame_justifictiva"
      Tab(0).Control(11).Enabled=   0   'False
      Tab(0).Control(12)=   "frameSelecao"
      Tab(0).Control(12).Enabled=   0   'False
      Tab(0).ControlCount=   13
      Begin VB.Frame frameSelecao 
         Caption         =   "Resumo Sinistro"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1305
         Left            =   120
         TabIndex        =   21
         Top             =   360
         Width           =   10350
         Begin VB.TextBox txtNumeroSinistro 
            Height          =   315
            Left            =   2760
            Locked          =   -1  'True
            TabIndex        =   25
            Top             =   480
            Width           =   1092
         End
         Begin VB.TextBox txtNumeroProposta 
            Height          =   315
            Left            =   1440
            Locked          =   -1  'True
            MaxLength       =   9
            TabIndex        =   24
            Top             =   480
            Width           =   1092
         End
         Begin VB.TextBox txtNomeSegurado 
            Height          =   315
            Left            =   4080
            Locked          =   -1  'True
            TabIndex        =   23
            Top             =   480
            Width           =   6135
         End
         Begin VB.TextBox txtNumeroApolice 
            Height          =   315
            Left            =   120
            Locked          =   -1  'True
            MaxLength       =   9
            TabIndex        =   22
            Top             =   480
            Width           =   1092
         End
         Begin VB.Label lblNumeroSinistro 
            Caption         =   "N� Sinistro:"
            Height          =   255
            Left            =   2760
            TabIndex        =   30
            Top             =   240
            Width           =   1335
         End
         Begin VB.Label Label21 
            AutoSize        =   -1  'True
            Caption         =   "N� Proposta:"
            Height          =   195
            Left            =   1440
            TabIndex        =   29
            Top             =   255
            Width           =   900
         End
         Begin VB.Label Label20 
            Caption         =   "Nome Segurado:"
            Height          =   255
            Left            =   4080
            TabIndex        =   28
            Top             =   240
            Width           =   1455
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "N� Ap�lice:"
            Height          =   195
            Index           =   1
            Left            =   120
            TabIndex        =   27
            Top             =   240
            Width           =   795
         End
         Begin VB.Label lblNossaParteCosseguro 
            Caption         =   "Pagamento do sinistro (cosseguro cedido) somente sobre a nossa parte."
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Left            =   120
            TabIndex        =   26
            Top             =   945
            Visible         =   0   'False
            Width           =   10080
         End
      End
      Begin VB.Frame frame_justifictiva 
         Caption         =   "Justificativa Aprova��o de Ideniza��o maior que IS"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   975
         Left            =   9360
         TabIndex        =   50
         Top             =   1800
         Visible         =   0   'False
         Width           =   5805
         Begin VB.TextBox txt_justificativa 
            Height          =   615
            Left            =   135
            MaxLength       =   255
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   51
            Top             =   225
            Width           =   5535
         End
      End
      Begin VB.Frame fraPagamentosAprovados 
         Caption         =   "Pagamentos aprovados"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1815
         Left            =   120
         TabIndex        =   48
         Top             =   5820
         Width           =   10380
         Begin MSFlexGridLib.MSFlexGrid grdPagtosAprovados 
            Height          =   1545
            Left            =   120
            TabIndex        =   49
            Top             =   240
            Width           =   10125
            _ExtentX        =   17859
            _ExtentY        =   2725
            _Version        =   393216
            Cols            =   11
            FixedCols       =   0
            FillStyle       =   1
            SelectionMode   =   1
            FormatString    =   $"frmAcerto.frx":001C
         End
      End
      Begin VB.Frame frameAutorizacao 
         Caption         =   " Dados da Autoriza��o "
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1305
         Left            =   120
         TabIndex        =   37
         Top             =   360
         Width           =   10350
         Begin VB.TextBox txtFormaPgto 
            Height          =   285
            Left            =   8550
            Locked          =   -1  'True
            TabIndex        =   46
            Top             =   645
            Width           =   1665
         End
         Begin VB.TextBox txtMoeda 
            Height          =   285
            Left            =   7290
            Locked          =   -1  'True
            TabIndex        =   44
            Top             =   645
            Width           =   1155
         End
         Begin VB.TextBox txtValorTotal 
            Height          =   285
            Left            =   5550
            Locked          =   -1  'True
            TabIndex        =   42
            Top             =   645
            Width           =   1650
         End
         Begin VB.TextBox txtLider 
            Height          =   285
            Left            =   1785
            Locked          =   -1  'True
            TabIndex        =   39
            Top             =   645
            Width           =   3660
         End
         Begin VB.TextBox txtNumAutorizacao 
            Height          =   285
            Left            =   195
            Locked          =   -1  'True
            TabIndex        =   38
            Top             =   645
            Width           =   1440
         End
         Begin VB.Label Label6 
            Caption         =   "Forma Pgto. :"
            Height          =   225
            Left            =   8550
            TabIndex        =   47
            Top             =   390
            Width           =   1035
         End
         Begin VB.Label Label5 
            Caption         =   "Moeda :"
            Height          =   210
            Left            =   7290
            TabIndex        =   45
            Top             =   390
            Width           =   840
         End
         Begin VB.Label Label4 
            Caption         =   "Valor Total :"
            Height          =   285
            Left            =   5550
            TabIndex        =   43
            Top             =   390
            Width           =   1890
         End
         Begin VB.Label Label3 
            Caption         =   "Seguradora L�der :"
            Height          =   240
            Left            =   1785
            TabIndex        =   41
            Top             =   390
            Width           =   2400
         End
         Begin VB.Label Label2 
            Caption         =   "N� da Autoriza��o :"
            Height          =   195
            Left            =   195
            TabIndex        =   40
            Top             =   390
            Width           =   1980
         End
      End
      Begin VB.Frame frameDetalhamento 
         Caption         =   " Recibo SEGUR "
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1200
         Left            =   120
         TabIndex        =   35
         Top             =   2730
         Width           =   10365
         Begin VB.TextBox txtDetalhe 
            BeginProperty Font 
               Name            =   "Courier"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   885
            Left            =   165
            Locked          =   -1  'True
            MultiLine       =   -1  'True
            TabIndex        =   36
            Top             =   225
            Width           =   10050
         End
      End
      Begin VB.Frame frameRejeitado 
         Caption         =   " Motivo Rejei��o "
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   975
         Left            =   4680
         TabIndex        =   33
         Top             =   1710
         Visible         =   0   'False
         Width           =   5805
         Begin VB.TextBox txtRejeicao 
            Height          =   615
            Left            =   135
            MaxLength       =   255
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   34
            Top             =   225
            Width           =   5535
         End
      End
      Begin VB.Frame framePagamentos 
         Caption         =   "Pagamentos a serem aprovados"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1845
         Left            =   120
         TabIndex        =   31
         Top             =   3960
         Width           =   10380
         Begin MSFlexGridLib.MSFlexGrid flexPgtoSinistro 
            Height          =   1545
            Left            =   120
            TabIndex        =   32
            Top             =   255
            Width           =   10125
            _ExtentX        =   17859
            _ExtentY        =   2725
            _Version        =   393216
            Cols            =   18
            FixedCols       =   0
            FillStyle       =   1
            SelectionMode   =   1
            FormatString    =   $"frmAcerto.frx":0191
         End
      End
      Begin VB.Frame frameResultado 
         Caption         =   " Resultado "
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   975
         Left            =   120
         TabIndex        =   9
         Top             =   1710
         Visible         =   0   'False
         Width           =   2535
         Begin VB.OptionButton optRejeitado 
            Caption         =   "Rejeitado"
            Height          =   315
            Left            =   210
            TabIndex        =   1
            Top             =   570
            Width           =   2115
         End
         Begin VB.OptionButton optAprovado 
            Caption         =   "Aprovado"
            Height          =   315
            Left            =   210
            TabIndex        =   0
            Top             =   240
            Width           =   2175
         End
      End
      Begin VB.Frame frameAplicar 
         Caption         =   "Aplicar a"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   975
         Left            =   2775
         TabIndex        =   8
         Top             =   1710
         Visible         =   0   'False
         Width           =   1815
         Begin VB.OptionButton optSelecionado 
            Caption         =   "Selecionado"
            Height          =   255
            Left            =   195
            TabIndex        =   2
            Top             =   285
            Width           =   1335
         End
         Begin VB.OptionButton optTodos 
            Caption         =   "Todos"
            Height          =   255
            Left            =   195
            TabIndex        =   3
            Top             =   645
            Width           =   1095
         End
      End
      Begin VB.Frame frameCancelar 
         Caption         =   " Alterar Para "
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   975
         Left            =   2775
         TabIndex        =   14
         Top             =   1710
         Visible         =   0   'False
         Width           =   1815
         Begin VB.OptionButton optS�Cancela 
            Caption         =   "Cancelado"
            Height          =   255
            Left            =   270
            TabIndex        =   15
            Top             =   270
            Width           =   1335
         End
      End
      Begin VB.Frame frameEstornar 
         Caption         =   "Alterar Para"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   975
         Left            =   2775
         TabIndex        =   16
         Top             =   1710
         Visible         =   0   'False
         Width           =   1815
         Begin VB.OptionButton optEstornar 
            Caption         =   "Estornado"
            Height          =   255
            Left            =   210
            TabIndex        =   17
            Top             =   285
            Width           =   1155
         End
      End
      Begin VB.Frame FrameVoucher 
         Caption         =   "Voucher"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   975
         Left            =   2775
         TabIndex        =   18
         Top             =   1710
         Visible         =   0   'False
         Width           =   1815
         Begin VB.TextBox TxtVoucher 
            Height          =   315
            Left            =   240
            Locked          =   -1  'True
            TabIndex        =   10
            TabStop         =   0   'False
            Top             =   300
            Width           =   1455
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "N�mero"
            Height          =   195
            Index           =   0
            Left            =   240
            TabIndex        =   19
            Top             =   240
            Width           =   555
         End
      End
      Begin VB.Frame frameCancReapresentar 
         Caption         =   "Alterar Para"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   975
         Left            =   2775
         TabIndex        =   11
         Top             =   1710
         Visible         =   0   'False
         Width           =   1815
         Begin VB.OptionButton optCancelado 
            Caption         =   "Cancelado"
            Height          =   255
            Left            =   120
            TabIndex        =   12
            Top             =   270
            Width           =   1335
         End
         Begin VB.OptionButton optReapresenta 
            Caption         =   "Reapresentar"
            Height          =   255
            Left            =   120
            TabIndex        =   13
            Top             =   630
            Width           =   1455
         End
      End
   End
   Begin MSComctlLib.StatusBar StatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   20
      Top             =   8310
      Width           =   10770
      _ExtentX        =   18997
      _ExtentY        =   503
      Style           =   1
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmAcerto"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim flagOK              As Boolean
Dim Linha_Selecionada   As Integer
Dim vSelecionou         As Boolean
Dim vRejeicao           As String
Dim Carregando_Cosseguro_Aceito As Boolean
Dim Esta_Cancelando     As Boolean
Dim lngontop            As Long

'sergio.pires (Nova Consultoria) 18983905 - Corre��o Monet�ria para Saldo Devedor Autom�tico
Dim Lancou_Pagamento    As Boolean
Dim Exibiu_Msg          As Boolean
Dim vChamadaSegp        As String

'Alan Neves - 05/02/2013 - In�cio - para gerar rs ao carregar o form e evitar execu��o em excesso.
Dim rsMoeda As rdoResultset
Public Tecnico_atual As Integer

Public vval_paridade_moeda As String
Public achou_paridade As Boolean
'Alan Neves - MP - Fim - 05/02/2013

'Constantes de Moedas
Private Const DOLAR_MOEDA_ID = 220
Private Const REAL_MOEDA_ID = 790

' ALLWAYS ON TOP
Private Const HWND_NOTOPMOST = -2
Private Const HWND_TOPMOST = -1
Private Const SWP_NOMOVE = &H2
Private Const SWP_NOSIZE = &H1
Private Declare Function SetWindowPos Lib "user32" (ByVal hwnd As Long, ByVal _
     hWndInsertAfter As Long, ByVal x As Long, ByVal y As Long, ByVal cx As _
     Long, ByVal cy As Long, ByVal wFlags As Long) As Long

'+-----------------------------------------------------------
'| 19406709 - Melhoria nos limites de sinistro e emissao
'| RN05 - petrauskas - 03/2017
Private val_indenizacao_pago As Double
Private val_is As Double
Private produto_regra_especial As Boolean
'| 19406709 - Melhoria nos limites de sinistro e emissao
'+-----------------------------------------------------------


Private Function obter_valor_is(ByVal Proposta_id As Long, ByVal produto_id As Long) As Double
'+-----------------------------------------------------------
'| 19406709 - Melhoria nos limites de sinistro e emissao
'| RN05 - petrauskas - 03/2017
'| Primeiro procura tabelas dos produtos
'| se n�o encontrar, procura demais tabelas
'|

  Dim rs As rdoResultset
  Dim sTabela As String
  Dim sSQL As String
  Dim vIS As Double
  Dim bAux As Boolean
  
  vIS = 0
  bAux = False
  
  Select Case produto_id
    Case 8
      'sSQL = "SELECT SUM(val_is) AS val_is FROM seguros_db.dbo.escolha_tp_cob_maq_tb WITH (NOLOCK) WHERE proposta_id = " & Proposta_id & " AND dt_fim_vigencia_esc IS NULL"
      sTabela = "seguros_db.dbo.escolha_tp_cob_maq_tb"
    Case 155, 156, 227, 228, 229, 230, 300, 650, 680, 701, 1152, 1201, 1204, 1226, 1227, 1240 '06/06/2018 - Schoralick (ntendencia) - 00421597-seguro-faturamento-pecuario  [add produto 1240 (sprint 6)]  'C00149269 - EMISS�O RURAL NA ABS ADI��O DO PRODUTO 230
      'sSQL = "SELECT SUM(val_is) AS val_is FROM seguros_db.dbo.escolha_tp_cob_generico_tb WITH (NOLOCK) WHERE proposta_id = " & Proposta_id & " AND dt_fim_vigencia_esc IS NULL"
      sTabela = "seguros_db.dbo.escolha_tp_cob_generico_tb"
    Case 1210
      'sSQL = "SELECT SUM(val_is) AS val_is FROM seguros_db.dbo.escolha_tp_cob_avulso_tb WITH (NOLOCK) WHERE proposta_id = " & Proposta_id & " AND dt_fim_vigencia_esc IS NULL"
      sTabela = "seguros_db.dbo.escolha_tp_cob_avulso_tb"
    Case Else
      'sSQL = "SELECT SUM(val_is) AS val_is FROM seguros_db.dbo.escolha_tp_cob_generico_tb WITH (NOLOCK) WHERE proposta_id = " & Proposta_id & " AND dt_fim_vigencia_esc IS NULL"
      sTabela = "seguros_db.dbo.escolha_tp_cob_generico_tb"
  End Select
  
  sSQL = "SELECT SUM(val_is) AS val_is FROM " & sTabela & " WITH (NOLOCK) WHERE proposta_id = " & Proposta_id & " AND dt_fim_vigencia_esc IS NULL"
  
  Set rs = rdocn1.OpenResultset(sSQL)
  
  If Not rs.EOF Then
    If Not IsNull(rs("val_IS")) Then
      vIS = Val("0" & rs("val_IS"))
      bAux = True
    End If
  End If
  rs.Close
  
  If Not bAux Then
    sSQL = "SELECT sum(E.val_is) as val_is, E.dt_fim_vigencia_esc, S.dt_aviso_sinistro " & vbCrLf _
         & "FROM " & sTabela & " E WITH (NOLOCK) " & vbCrLf _
         & "INNER JOIN (SELECT proposta_id, MAX(num_endosso) as num_endosso " & vbCrLf _
         & "            FROM " & sTabela & " WITH (NOLOCK) " & vbCrLf _
         & "            WHERE proposta_id = " & Proposta_id & " " & vbCrLf _
         & "            GROUP BY proposta_id) M ON E.proposta_id = M.proposta_id " & vbCrLf _
         & "                                   AND E.num_endosso = M.num_endosso " & vbCrLf _
         & "INNER JOIN seguros_db.dbo.sinistro_tb S ON E.proposta_id = S.proposta_id " & vbCrLf _
         & "                                       AND S.sinistro_id = " & SinistroEmUso.Item(1).sinistro_id & " " & vbCrLf _
         & "group by E.proposta_id, E.dt_fim_vigencia_esc, S.dt_aviso_sinistro"
  
    Set rs = rdocn1.OpenResultset(sSQL)

    If Not rs.EOF Then
      If Not IsNull(rs("val_IS")) And Not IsNull(rs("dt_fim_vigencia_esc")) And Not IsNull(rs("dt_aviso_sinistro")) Then
        If rs("dt_fim_vigencia_esc") > rs("dt_aviso_sinistro") Then
          vIS = CDbl("0" & rs("val_IS"))
        End If
      End If
      bAux = True
    
    End If
    rs.Close
  End If
  
  
  
  If Not bAux And InStr(",155,156,227,230,650,680,1152,1201,1226,1227,", "," & produto_id & ",") > 0 Then
  ' CONFITEC - INCIDENTE (IM00973138)- inicio
    sSQL = "SELECT SUM(val_is) AS val_is FROM seguros_db.dbo.escolha_tp_cob_avulso_tb WITH (NOLOCK) WHERE proposta_id = " & Proposta_id & " AND dt_fim_vigencia_esc IS NULL AND val_is <> 0"
    Set rs = rdocn1.OpenResultset(sSQL)
  ' CONFITEC - INCIDENTE (IM00973138)- fim
    If Not rs.EOF Then
      If Not IsNull(rs("val_IS")) Then
        vIS = rs("val_IS")
      End If
    End If
    rs.Close
    
    If Not bAux Then
      sTabela = "seguros_db.dbo.escolha_tp_cob_avulso_tb"
      
      sSQL = "SELECT sum(E.val_is) as val_is, E.dt_fim_vigencia_esc, S.dt_aviso_sinistro " & vbCrLf _
           & "FROM " & sTabela & " E WITH (NOLOCK) " & vbCrLf _
           & "INNER JOIN (SELECT proposta_id, MAX(num_endosso) as num_endosso " & vbCrLf _
           & "            FROM " & sTabela & " WITH (NOLOCK) " & vbCrLf _
           & "            WHERE proposta_id = " & Proposta_id & " " & vbCrLf _
           & "            GROUP BY proposta_id) M ON E.proposta_id = M.proposta_id " & vbCrLf _
           & "                                   AND E.num_endosso = M.num_endosso " & vbCrLf _
           & "INNER JOIN seguros_db.dbo.sinistro_tb S ON E.proposta_id = S.proposta_id " & vbCrLf _
           & "                                       AND S.sinistro_id = " & SinistroEmUso.Item(1).sinistro_id & " " & vbCrLf _
           & "group by E.proposta_id, E.dt_fim_vigencia_esc, S.dt_aviso_sinistro"
    
      Set rs = rdocn1.OpenResultset(sSQL)
    
      If Not rs.EOF Then
        If Not IsNull(rs("val_IS")) And Not IsNull(rs("dt_fim_vigencia_esc")) And Not IsNull(rs("dt_aviso_sinistro")) Then
          If rs("dt_fim_vigencia_esc") > rs("dt_aviso_sinistro") Then
            vIS = CDbl("0" & rs("val_IS"))
          End If
        End If
        bAux = True
      
      End If
      rs.Close
    End If
  End If
  
  Set rs = Nothing
  
  obter_valor_is = vIS

End Function



Private Function Validar_Indenizacao_Superior_IS(pSinistro_id As String, pTp_Cobertura_id As String, pValor_Indenizacao As Currency) As Boolean
    '-----------------------------------------------------------------
    'Demanda 689336 - talitha - 25/09/2009 - Indeniza��o superior IS.
    '-----------------------------------------------------------------

    'Alan Neves - MP - 12/03/2013 - Inicio
    'Dim Tecnico_atual As Integer
    'Alan Neves - MP - 12/03/2013 - Fim
    
    Dim Cod_Objeto_Segurado As Integer
    
    Dim vSql As String
    Dim rs   As rdoResultset
    
    Validar_Indenizacao_Superior_IS = False
    
    '* Obt�m o C�digo do T�cnico que ir� aprovar o pagamento
    'Alan Neves - MP - 12/03/2013 - Inicio - Codigo do t�cnico j� obtido no carregamento do form
    'Tecnico_atual = Obtem_Codigo_Tecnico
    'Alan Neves - MP - 12/03/2013 - Fim
    
    '* Obtendo o c�digo do objeto segurado pelo sinistro.
    Cod_Objeto_Segurado = 0
    If vTpRamo = 1 Then
        Cod_Objeto_Segurado = 1
    ElseIf vTpRamo = 2 Then
        vSql = "SELECT cod_objeto_segurado "
        vSql = vSql & "FROM sinistro_re_tb WITH(NOLOCK) "
        vSql = vSql & "WHERE sinistro_id = " & pSinistro_id
                
        Set rs = rdocn.OpenResultset(vSql)
        
        If Not rs.EOF Then
            Cod_Objeto_Segurado = rs("cod_objeto_segurado")
        End If
        rs.Close
    End If
    'Diego Galizoni Caversan - Demanda 1391917 - 19/01/2009
    If vTpRamo = 1 Then
        vSql = ""
        vSql = vSql & "select distinct sc.tp_cobertura_id, sc.val_estimativa as valor_indenizacao "
        vSql = vSql & "  from sinistro_cobertura_tb sc WITH(NOLOCK) "
        
        'Alan Neves - MP - 19/03/2013 - Inicio - Leitura na temp
        'vsql = vsql & " inner join sinistro_estimativa_tb se WITH(NOLOCK) "
        vSql = vSql & " inner join #sinistro_estimativa_tb se WITH(NOLOCK) "
        'Alan Neves - MP - 19/03/2013 - Fim
        
        vSql = vSql & " on sc.sinistro_id = se.sinistro_id"
        
        'Alan Neves - MP - 19/03/2013 - Inicio - Alterado para acessar a temporaria
        'vsql = vsql & " INNER JOIN pgto_sinistro_tb pc WITH(NOLOCK) "
        vSql = vSql & " INNER JOIN #pgto_sinistro_tb pc "
        'Alan Neves - MP - 19/03/2013 - Fim
        
        vSql = vSql & "    ON pc.sinistro_id = sc.sinistro_id"
        vSql = vSql & " Where sc.sinistro_id = " & pSinistro_id
        vSql = vSql & " and sc.dt_fim_vigencia is null"
        vSql = vSql & " and se.dt_fim_estimativa is null"
        vSql = vSql & " and se.item_val_estimativa = 1"
        vSql = vSql & " and pc.situacao_op = 'n'"
        vSql = vSql & " and sc.tp_cobertura_id = " & pTp_Cobertura_id
        Set rs = rdocn1.OpenResultset(vSql)
        If Not rs.EOF Then
            pValor_Indenizacao = TrocaPontoPorVirgula(rs("valor_indenizacao"))
        End If
        
        rs.Close
    End If
    
    '* Verifica se o t�cnico tem al�ada para aprovar o sinistro.
    vSql = "EXEC SEGS7442_SPS " & pSinistro_id & ", " & _
                                 pTp_Cobertura_id & ", " & _
                                 Cod_Objeto_Segurado & ", " & _
                                 TrocaVirgulaPorPonto(pValor_Indenizacao) & ", " & _
                                 Tecnico_atual & ", '" & _
                                 cUserName & "', " & _
                                 "1 "
    
    'Alan Neves - 19/03/2013 - Erro executando com rdocn1.
    'Set rs = rdocn1.OpenResultset(vsql)
    Set rs = rdocn.OpenResultset(vSql)

    If rs(0) = 0 Then
        Validar_Indenizacao_Superior_IS = True
    End If
    rs.Close

End Function

'Inicio - Confitec - Cleiton Queiroz 10/07/2014: Trava aprova��o de pagamento ap�s as 18:00h.
Public Function TravaProcessamento(Optional ByVal objAmbiente As Object) As Boolean
  
  On Local Error GoTo TrataExce��o
  Dim RX As Object, Criou As Boolean
  Dim vSql As String
  
  If objAmbiente Is Nothing Then
    Set objAmbiente = CreateObject(gsOBJETOAMBIENTE)
    Criou = True
  Else
    Criou = False
  End If
  
  vSql = ""
  vSql = vSql & "select  case when (convert(varchar(8), getdate(), 108) between '18:00:00' and '23:59:59') then 1 "
  vSql = vSql & "when datepart(dw, getdate()) = 1 then 1 "
  vSql = vSql & "when datepart(dw, getdate()) = 7 then 1 "
  vSql = vSql & "when(select 1 from seguros_db..feriado_tb with(nolock) "
  vSql = vSql & "where year(data) = year(getdate()) and data = dateadd(d,datediff(d,0,getdate()),0)) > 0 then 1  "
  vSql = vSql & "else 0 end "
  
  Set RX = objAmbiente.ExecutaAmbiente(vSql)
  TravaProcessamento = CBool(RX(0))
  RX.Close
  Set RX = Nothing
  If Criou Then
    Set objAmbiente = Nothing
  End If
  Exit Function
TrataExce��o:
  If Criou Then
    Set objAmbiente = Nothing
  End If
End Function
'Fim - Confitec - Cleiton Queiroz 10/07/2014: Trava aprova��o de pagamento ap�s as 18:00h.


Private Sub verifica_pgto_X_IS()
  Dim v_pgto As Double
  Dim Pgto   As Pgto_Sinistro
  Dim ipnt As Integer
  
  If produto_regra_especial And optAprovado.Value And frameResultado.Visible And optAprovado.Visible Then
  
    If optTodos.Value Then
      v_pgto = 0
      For ipnt = 1 To flexPgtoSinistro.Rows - 1
        Set Pgto = Pagamentos.Item(ipnt)
        If Val(Pgto.Item_val_estimativa) = 1 Then
          v_pgto = v_pgto + CDbl(Pgto.val_Acerto) + CDbl(Pgto.val_Correcao)
        End If
      Next ipnt
    ElseIf optSelecionado.Value Then
      If flexPgtoSinistro.Row > 0 Then
        Set Pgto = Pagamentos.Item(flexPgtoSinistro.Row)
        v_pgto = CDbl(Pgto.val_Acerto) + CDbl(Pgto.val_Correcao)
      Else
        v_pgto = 0
      End If
    Else
      v_pgto = 0
    End If
    
    If v_pgto > 0 Then
      frame_justifictiva.Visible = ((v_pgto + val_indenizacao_pago) > val_is)
    Else
      frame_justifictiva.Visible = False
    End If
  Else
    frame_justifictiva.Visible = False
  End If

End Sub

Private Sub BtnAplicar_Click()
    
    Dim Pgto                As Pgto_Sinistro
    Dim Sin As Sinist, i    As Integer
    Dim vResultado          As String
    Dim vConjunto           As String
    Dim SQL As String, rs   As rdoResultset
    Dim AMoeda_Sinistro     As Integer
    Dim OValor_Pago         As Double
    Dim vProduto_ID         As String
    Dim Item_val_estimativa As String
    'Luciana - 29/09/2005
    Dim vSql                As String
    Dim rsAuxi              As rdoResultset
    Dim iQtdItemEstimativa()  As Integer
    Dim countQtdItemEstimativa   As Integer
    Dim Seq_Estimativa As String
    Dim Seq_Pgto As String
    Dim motivoRiscoDup As String 'Cesar Santos CONFITEC - (C00214342)- 09/03/2020
    
    
    On Error GoTo Trata_erro
    
    
    If frameCancReapresentar.Visible = False And _
            frameCancReapresentar.Visible = False And _
            frameCancelar.Visible = False And _
            frameRejeitado.Visible = False And _
            frameResultado.Visible = False And _
            frameAplicar.Visible = False And _
            frameEstornar.Visible = False Then
                mensagem_erro 6, "Nenhum item de pagamento foi selecionado."
                Exit Sub
    ElseIf FrameVoucher.Visible Then
        mensagem_erro 6, "Este pagamento j� possui N�mero de Pagamento."
        Exit Sub
    End If
    
    If optAprovado.Caption = "Cancelado" And optAprovado.Visible Then
        mensagem_erro 6, "Este pagamento j� foi cancelado. N�o pode mais ser alterado."
        Exit Sub
    End If
    
    If frameAutorizacao.Visible = False Then
'        Set Pgto = Pagamentos.Item(flexPgtoSinistro.Row)
'        If (Pgto.Item_val_estimativa = 4) Or (Pgto.Item_val_estimativa = 6) Then
'            mensagem_erro 6, "N�o � poss�vel alterar a situa��o de pagamentos de estimativas de ressarcimento ou salvados."
'            Exit Sub
'        End If
'        Set Pgto = Nothing
        
        Set Sin = SinistroEmUso.Item(1)
    End If
    
     'Inicio - Confitec - Cleiton Queiroz 10/07/2014: Trava aprova��o de pagamento ap�s as 18:00h.
    If voper = "I" And TravaProcessamento = True Then
        mensagem_erro 6, "Processo n�o autorizado ap�s as 18:00h"
        Exit Sub
    End If
    'Fim - Confitec - Cleiton Queiroz 10/07/2014: Trava aprova��o de pagamento ap�s as 18:00h.
    
    Lancou_Pagamento = False
    Exibiu_Msg = False
    vChamadaSegp = ""
    
    If voper = "I" Or voper = "IS" Then
        
        If Not optAprovado.Value And Not optRejeitado.Value Then
            mensagem_erro 6, "Resultado n�o indicado!"
            Exit Sub
        End If
        If Not optSelecionado.Value And Not optTodos.Value Then
            mensagem_erro 6, "Informe se deseja aplicar o resultado ao item selecionado ou a todos!"
            Exit Sub
        End If
        If optRejeitado.Value And Trim(txtRejeicao) = "" Then
            mensagem_erro 6, "Motivo da rejei��o n�o informado!"
            Exit Sub
        ElseIf Trim(txtRejeicao) <> "" Then
            txtRejeicao = MudaAspaSimples(txtRejeicao)
        End If
        If optSelecionado.Value And Not vSelecionou Then
            mensagem_erro 6, "Registro n�o selecionado!"
            Exit Sub
        End If
        
        
'+-----------------------------------------------------------
'| 19406709 - Melhoria nos limites de sinistro e emissao
'| RN05 - petrauskas - 03/2017
        If frame_justifictiva.Visible And Trim(txt_justificativa.Text) = "" Then
            mensagem_erro 6, "Valor da indeniza��o � maior que o valor da import�ncia segurada (IS). Por favor, justifique sua aprova��o!"
            Exit Sub
        ElseIf frame_justifictiva.Visible Then
            txt_justificativa.Text = MudaAspaSimples(txt_justificativa.Text)
            txtRejeicao.Text = txt_justificativa.Text 'rotina sempre grava txtRejeicao.text.
        End If
'| 19406709 - Melhoria nos limites de sinistro e emissao
'+-----------------------------------------------------------
        
        
        If optAprovado Then
            ' N�o pode aprovar se estiver inadimplente
'            SQL = "SELECT COUNT(num_cobranca) FROM agendamento_cobranca_tb " & _
'                   "WHERE proposta_id = " & Sin.Proposta_id & _
'                   "  AND dt_agendamento < '" & Format(Data_Sistema, "yyyymmdd") & "'" & _
'                   "  AND dt_recebimento is NULL "
'            Set Rs = rdocn.OpenResultset(SQL)
'            If Rs(0) > 0 Then
'                MsgBox "Existe(m) parcela(s) em atraso para esta proposta."
'                Rs.Close
'                Exit Sub
'            End If
'            Rs.Close

         ' Cesar Santos CONFITEC - (C00214342)- inicio 09/03/2020
                
		If frameAutorizacao.Visible = False Then
          motivoRiscoDup = "N"
          For i = 1 To flexPgtoSinistro.Rows - 1
              
               
                  SQL = ""
                  SQL = "EXEC SEGS14673_SPS " & Sin.sinistro_id & _
                                                   "," & MudaVirgulaParaPonto(Format(flexPgtoSinistro.TextMatrix(IIf(optSelecionado.Value, flexPgtoSinistro.Row, i), 5), "###########0.00")) & _
                                                   "," & MudaVirgulaParaPonto(Format(flexPgtoSinistro.TextMatrix(IIf(optSelecionado.Value, flexPgtoSinistro.Row, i), 6), "###########0.00")) & _
                                                   "," & "NULL" & _
                                                   "," & "5" & _
                                                   "," & Pagamentos.Item(IIf(optSelecionado.Value, flexPgtoSinistro.Row, i)).Beneficiario_id & _
                                                   "," & "A"
    
                
                                              
               Set rs = rdocn.OpenResultset(SQL)
               
               If Not rs.EOF Then
                    While Not rs.EOF
                        If (rs!RISCO_DUPLICIDADE = "S") Then
                           
                            motivoRiscoDup = rs!MOTIVO_RISCO_DUPLIDADE ' Motivo do risco de Duplicidade de pagamento
                            
                        End If
                    
                    rs.MoveNext
                    Wend
                End If
               
              rs.Close
              
              If optSelecionado.Value Then
                i = flexPgtoSinistro.Rows - 1
              End If
              
              Next i
              
                If motivoRiscoDup <> "N" Then
                    If MsgBox("Aten��o! J� existe um pagamento para o sinistro, gentileza certificar que n�o � Duplicidade." & vbCrLf & "Deseja continuar?", vbYesNo + vbInformation + vbDefaultButton2) = vbNo Then
                        Exit Sub
                    End If
                End If
       
        End If     
       ' Cesar Santos CONFITEC - (C00214342)- Fim 09/03/2020



            vResultado = "a"
        Else
            vResultado = "r"
        End If
    
       '*** Confitec: Cl�udia Silva -> Flow 18707372 - 28/09/2015 ***
       '*** Remover a Valida��o de FAJ para agilizar o processo   ***
       '*** de pagamento de Sinistro                              ***
       'If Valida_FAJ(Sin) = False Then
       '   MsgBox "N�o existe FAJ cadastrada para esta data de solicita��o de pagamento! " & vbCrLf & _
       '          "Entre em contato com a �rea respons�vel.", vbOKOnly + vbExclamation
       '   Exit Sub
       'End If
        
        Screen.MousePointer = 11
        
        rdocn.BeginTrans
        
        If frameAutorizacao.Visible Then ' Autoriza todos da OP
            
            For i = 1 To flexPgtoSinistro.Rows - 1
                
              'Cleber Sardo - INC000005184084 - Data 20/10/2016 - Sinistros Cosseguro
              If Pagamentos.Count = 0 And frameAutorizacao.Visible Then
                Monta_pgto_Cosseguro
              End If
                '
                'P.ROBERTO - FLOWBR12054741 - 17/08/2011
                'INSTANCIANDOA VARIAVEL ABAIXO PARA CARREGAR OS DADOS
                Set Pgto = Pagamentos.Item(i)
                '
                'Se o item for Honor�rios(item 2) ou Despesas(item 3) ent�o critica al�ada
                If (flexPgtoSinistro.TextMatrix(i, 16) = "HONOR�RIOS" Or flexPgtoSinistro.TextMatrix(i, 16) = "DESPESAS") Then
                    If txtMoeda.Tag = DOLAR_MOEDA_ID Then
                       'Obt�m a paridade da moeda nesta data
                       'Alan Neves - Inicio - MP - 06/02/2013 - substituido select pelo achou_paridade

'                       SQL = "SELECT val_paridade_moeda "
'                       SQL = SQL & " FROM paridade_tb WITH(NOLOCK) "
'                       SQL = SQL & " WHERE dt_conversao     = '" & Format(Data_Sistema, "yyyymmdd") & "'"
'                       SQL = SQL & "   AND origem_moeda_id  = " & DOLAR_MOEDA_ID
'                       SQL = SQL & "   AND destino_moeda_id = " & REAL_MOEDA_ID
'                       Set rs = rdocn.OpenResultset(SQL)
                       
                        '  Alan Neves
'                       If Not rs.EOF Then
                       If achou_paridade Then
'                       OValor_Pago = TruncaDecimal(CDbl(flexPgtoSinistro.TextMatrix(i, 19)) * Val(rs("val_paridade_moeda")))
                          OValor_Pago = TruncaDecimal(CDbl(flexPgtoSinistro.TextMatrix(i, 19)) * Val(vval_paridade_moeda))
                       Else
                           ' rs.Close
                           'Alan Neves - MP - 06/02/2013
                           
                           rdocn.RollbackTrans 'C_ANEVES / msalema - 21/06/2013
                           MsgBox "N�o existe Paridade cadastrada para esta data de solicita��o de pagamento! " & vbCrLf & _
                                  "Entre em contato com a �rea respons�vel.", vbOKOnly + vbExclamation
                           'rdocn.RollbackTrans 'C_ANEVES / msalema - 21/06/2013
                           GoTo Trata_erro
                       End If
                       
                       'rs.Close
                       'Alan Neves - MP - 06/02/2013
                    Else
                       OValor_Pago = CDbl(flexPgtoSinistro.TextMatrix(i, 19))
                    End If
                    
                    'OValor_Pago = CDbl(Pgto.val_Acerto)
                    
                    'Obt�m o produto_id atrav�s do sinistro
                    'Alan Neves - MP - 18/03/2013 - Inicio
                    ' vProduto_ID = Obtem_Produto_ID(flexPgtoSinistro.TextMatrix(i, 0), flexPgtoSinistro.TextMatrix(i, 1), flexPgtoSinistro.TextMatrix(i, 2), flexPgtoSinistro.TextMatrix(i, 3), flexPgtoSinistro.TextMatrix(i, 4))
                    'Cleber Sardo - Data 23/01/2017 - INC000005312492 - Tratamento de erro
                    'vProduto_ID = SinistroEmUso.Item(i).produto_id
                    If SinistroEmUso.Count >= i Then
                        vProduto_ID = SinistroEmUso.Item(i).produto_id
                    Else
                        vProduto_ID = SinistroEmUso.Item(1).produto_id
                    End If
                    
                    'Alan Neves - MP - 18/03/2013 - Fim
                                        
                    'Verifica se o t�cnico que solicitou o pagamento tem al�ada para a aprova��o autom�tica
'                    If Not Tecnico_tem_Alcada(IIf(flexPgtoSinistro.TextMatrix(i, 16) = "HONOR�RIOS", "2", "3"), flexPgtoSinistro.TextMatrix(i, 4), vProduto_ID, OValor_Pago) Then
'                       Screen.MousePointer = 0
'                       rdocn.RollbackTrans
'                       Exit Sub
'                    End If
                
                'Alan Neves - MP - 11/03/2013 - In�cio
                'End If
                'Alan Neves - MP - 11/03/2013 - Fim
                
                ' Demanda: 14129660

                'Alan Neves - MP - 11/03/2013 - In�cio
                Else
                  'Alan Neves - MP - 18/03/2013 - Inicio
                  'vProduto_ID = Obtem_Produto_ID(flexPgtoSinistro.TextMatrix(i, 0), flexPgtoSinistro.TextMatrix(i, 1), flexPgtoSinistro.TextMatrix(i, 2), flexPgtoSinistro.TextMatrix(i, 3), flexPgtoSinistro.TextMatrix(i, 4))
                  'Cleber Sardo - Data 23/01/2017 - INC000005312492 - Tratamento de erro
                  'vProduto_ID = SinistroEmUso.Item(i).produto_id
                    If SinistroEmUso.Count >= i Then
                        vProduto_ID = SinistroEmUso.Item(i).produto_id
                    Else
                        vProduto_ID = SinistroEmUso.Item(1).produto_id
                    End If
                  'Alan Neves - MP - 11/03/2013 - Fim
                End If
                'Alan Neves - MP - 11/03/2013 - Fim
                
                OValor_Pago = CDbl(flexPgtoSinistro.TextMatrix(i, 19))
                
                If vProduto_ID = 1183 And vResultado = "a" Then
                    SQL = ""
                    SQL = " SELECT convert(numeric(15,2),d.val_is * 1.03) AS val_is " & vbCrLf & _
                    " FROM sinistro_tb a with(nolock) " & vbCrLf & _
                    " INNER JOIN sinistro_cobertura_tb b with(nolock) " & vbCrLf & _
                    " ON b.sinistro_id = a.sinistro_id " & vbCrLf & _
                    " INNER JOIN tp_cob_comp_tb c with(nolock) " & vbCrLf & _
                    " ON c.tp_cobertura_id = b.tp_cobertura_id " & vbCrLf & _
                    " INNER JOIN escolha_plano_tp_cob_tb d with(nolock) " & vbCrLf & _
                    " ON d.tp_cob_comp_id = c.tp_cob_comp_id  " & vbCrLf & _
                    " AND d.proposta_id = a.proposta_id  " & vbCrLf & _
                    " Where a.sinistro_id = " & Sin.sinistro_id & vbCrLf & _
                    " AND b.dt_fim_vigencia IS NULL " & vbCrLf & _
                    " AND d.dt_inicio_vigencia_cob <= a.dt_ocorrencia_sinistro " & vbCrLf & _
                    " AND (d.dt_fim_vigencia_cob >= a.dt_ocorrencia_sinistro OR d.dt_fim_vigencia_cob IS NULL) "
                    
                    Set rs = rdocn.OpenResultset(SQL)
                    If Not rs.EOF Then
                        If OValor_Pago > Replace(rs!val_is, ".", ",") Then
                            If MsgBox("O valor para pagamento � maior que o capital Segurado Vigente + 3%, deseja prosseguir com a aprova��o?", vbYesNo + vbQuestion, "Saldo") = vbNo Then
                                rs.Close
                                rdocn.RollbackTrans
                                Screen.MousePointer = 1
                                Exit Sub
                            End If
                        End If
                    End If
                    rs.Close
                    
                End If
                Screen.MousePointer = 1
                
                If flexPgtoSinistro.TextMatrix(i, 16) <> "" Then
                    Select Case flexPgtoSinistro.TextMatrix(i, 16)
                        Case "INDENIZA��O"
                            Item_val_estimativa = "1"
                        Case "HONOR�RIOS"
                            Item_val_estimativa = "2"
                        Case "DESPESAS"
                            Item_val_estimativa = "3"
                        Case Else
                            Item_val_estimativa = "0"
                    End Select
                Else
                    Item_val_estimativa = Pgto.Item_val_estimativa
                End If
                
                Seq_Estimativa = Pgto.Seq_Estimativa
                Seq_Pgto = Pgto.Seq_Pgto
                
                'vProduto_ID = Obtem_Produto_ID(flexPgtoSinistro.TextMatrix(i, 0), flexPgtoSinistro.TextMatrix(i, 1), flexPgtoSinistro.TextMatrix(i, 2), flexPgtoSinistro.TextMatrix(i, 3), flexPgtoSinistro.TextMatrix(i, 4))
                
                'Alan Neves - MP - 18/03/2013 - Inicio - Acrescentado nome do produto e do ramo na chamada da function
                'If Not Tecnico_tem_Alcada(Item_val_estimativa, flexPgtoSinistro.TextMatrix(i, 4), vProduto_ID, OValor_Pago, flexPgtoSinistro.TextMatrix(i, 3), flexPgtoSinistro.TextMatrix(i, 2), flexPgtoSinistro.TextMatrix(i, 1), flexPgtoSinistro.TextMatrix(i, 0), Seq_Estimativa, Seq_Pgto) Then
                'If Not Tecnico_tem_Alcada(Item_val_estimativa, flexPgtoSinistro.TextMatrix(i, 4), vProduto_ID, OValor_Pago, flexPgtoSinistro.TextMatrix(i, 3), flexPgtoSinistro.TextMatrix(i, 2), flexPgtoSinistro.TextMatrix(i, 1), flexPgtoSinistro.TextMatrix(i, 0), Seq_Estimativa, Seq_Pgto, SinistroEmUso.Item(i).NomeProduto, SinistroEmUso.Item(i).NomeRamo) Then
                '   Screen.MousePointer = 0
                '    rdocn.RollbackTrans
                '    Exit Sub
                'End If
                'Cleber Sardo - Data 23/01/2017 - INC000005312492 - Tratamento de erro
                If SinistroEmUso.Count >= i Then
                  If Not Tecnico_tem_Alcada(Item_val_estimativa, flexPgtoSinistro.TextMatrix(i, 4), vProduto_ID, OValor_Pago, flexPgtoSinistro.TextMatrix(i, 3), flexPgtoSinistro.TextMatrix(i, 2), flexPgtoSinistro.TextMatrix(i, 1), flexPgtoSinistro.TextMatrix(i, 0), Seq_Estimativa, Seq_Pgto, SinistroEmUso.Item(i).NomeProduto, SinistroEmUso.Item(i).NomeRamo) Then
                      Screen.MousePointer = 0
                      rdocn.RollbackTrans
                      Exit Sub
                  End If
                Else
                  If Not Tecnico_tem_Alcada(Item_val_estimativa, flexPgtoSinistro.TextMatrix(i, 4), vProduto_ID, OValor_Pago, flexPgtoSinistro.TextMatrix(i, 3), flexPgtoSinistro.TextMatrix(i, 2), flexPgtoSinistro.TextMatrix(i, 1), flexPgtoSinistro.TextMatrix(i, 0), Seq_Estimativa, Seq_Pgto, SinistroEmUso.Item(1).NomeProduto, SinistroEmUso.Item(1).NomeRamo) Then
                      Screen.MousePointer = 0
                      rdocn.RollbackTrans
                      Exit Sub
                  End If
                End If
                
                
                'Diego Galizoni Caversan - GPTI - 12/11/2009
                'Demanda 1445418 - N�o permitir que um tecnico aprove sua pr�pria solicita��o
                If Avaliador_solicitante(flexPgtoSinistro.TextMatrix(i, 0), Item_val_estimativa, flexPgtoSinistro.TextMatrix(i, 5), flexPgtoSinistro.TextMatrix(i, 6), flexPgtoSinistro.TextMatrix(i, 7), flexPgtoSinistro.TextMatrix(i, 8)) Then
                    Screen.MousePointer = 0
                    rdocn.RollbackTrans
                    MsgBox "Usu�rio Solicitante n�o pode ser Avaliador."
                    Exit Sub
                End If
                'fim
                
                SQL = "EXEC pgto_sinistro_aprova_spu " & _
                    flexPgtoSinistro.TextMatrix(i, 0) & "," & _
                    flexPgtoSinistro.TextMatrix(i, 1) & "," & _
                    flexPgtoSinistro.TextMatrix(i, 2) & "," & _
                    flexPgtoSinistro.TextMatrix(i, 3) & "," & _
                    flexPgtoSinistro.TextMatrix(i, 4) & ","
        
                Select Case flexPgtoSinistro.TextMatrix(i, 16)
                    Case "INDENIZA��O"
                        SQL = SQL & "1, "
                    Case "HONOR�RIOS"
                        SQL = SQL & "2, "
                    Case "DESPESAS"
                        SQL = SQL & "3, "
                    Case "RESSARCIMENTO"
                        SQL = SQL & "4, "
                    Case "DESP. RESSARCIMENTO"
                        SQL = SQL & "5, "
                    Case "SALVADOS"
                        SQL = SQL & "6, "
                    Case "DESP. SALVADOS"
                        SQL = SQL & "7, "
                End Select
                SQL = SQL & _
                    flexPgtoSinistro.TextMatrix(i, 5) & "," & _
                    flexPgtoSinistro.TextMatrix(i, 6) & "," & _
                    flexPgtoSinistro.TextMatrix(i, 7) & "," & _
                    flexPgtoSinistro.TextMatrix(i, 8) & "," & _
                    "'s', '" & _
                    vResultado & "','" & _
                    txtRejeicao & "','" & _
                    flexPgtoSinistro.TextMatrix(i, 13) & "'," & _
                    "'" & Format(Data_Sistema, "yyyymmdd") & "'," & _
                    "'" & cUserName & "', " & _
                    "NULL, NULL"
        
                Set rs = rdocn.OpenResultset(SQL)
                rs.Close
                
                'Alan Neves - C_ANEVES - 15/05/2013 - Inicio
'                If vResultado = "r" Then
'                    MensagemAcerto = "Autoriza��o N�: " & txtNumAutorizacao.Text & vbNewLine
'                    MensagemAcerto = MensagemAcerto & "Sinistro: " & flexPgtoSinistro.TextMatrix(i, 0) & vbNewLine
'                    MensagemAcerto = MensagemAcerto & "Ap�lice: " & flexPgtoSinistro.TextMatrix(i, 1) & vbNewLine
'                    MensagemAcerto = MensagemAcerto & "Ramo: " & flexPgtoSinistro.TextMatrix(i, 4) & vbNewLine
'                    MensagemAcerto = MensagemAcerto & "Estimativa: " & flexPgtoSinistro.TextMatrix(i, 16) & vbNewLine
'                    MensagemAcerto = MensagemAcerto & "Benefici�rio: " & txtLider.Text & vbNewLine
'                    MensagemAcerto = MensagemAcerto & "Data da rejei��o: " & Format(Data_Sistema, "dd/mm/yyyy") & vbNewLine
'                    MensagemAcerto = MensagemAcerto & "Rejeitado por: " & cUserName & vbNewLine & vbNewLine
'                    MensagemAcerto = MensagemAcerto & "Motivo: " & txtRejeicao
'                    Destinatario = flexPgtoSinistro.TextMatrix(i, 21)
'                    ' Manda e-mail
'                    If MAPISess.SessionID = 0 Then
'                        Logon_Click
'                    End If
'                    If SendMsg("r") = False Then
'                        rdocn.RollbackTrans
'                        Exit Sub
'                    End If
'                End If
                'Alan Neves - C_ANEVES - 15/05/2013 - Fim
            Next i
            
            'Alan Neves - MP - 21/03/2013 - Inicio - Atualiza a temporaria devido a execu��o da pgto_sinistro_aprova_spu
            frmAcertoCon.SelecionaDadosPgtoSinistro
            'Alan Neves - MP - 21/03/2013 - Fim
            
        Else
            vConjunto = "s" ' um pagamento por vez.
            If optTodos Then
                For i = 1 To flexPgtoSinistro.Rows - 1

                    Set Pgto = Pagamentos.Item(i)
                    
                    '-----------------------------------------------------------------
                    'Demanda 689336 - Talitha - 25/09/2009 - Indeniza��o superior IS.
                    '-----------------------------------------------------------------
                    If Val(Pgto.Item_val_estimativa) = 1 Then
                        If Validar_Indenizacao_Superior_IS(Sin.sinistro_id, Pgto.tp_Cobertura_id, CDbl(Pgto.val_Acerto) + CDbl(Pgto.val_Correcao)) Then
                            'HENRIQUE H. HENRIQUES - CONFITEC SP - INI (VISUALIZANDO VALOR A SER APROVADO PARA INDENIZA��O)
                            If MsgBox("Valor da Indeniza��o � superior ao Valor do IS. Deseja continuar?" & vbNewLine & "Valor de indeniza��o: " & (CDbl(Pgto.val_Acerto) + CDbl(Pgto.val_Correcao)), vbYesNo) = vbNo Then
                            'HENRIQUE H. HENRIQUES - CONFITEC SP - FIM (VISUALIZANDO VALOR A SER APROVADO PARA INDENIZA��O)
                                Screen.MousePointer = 0
                                rdocn.RollbackTrans
                                Exit Sub
                            End If
                        End If
                    End If
                    
                    'Se o item for Honor�rios(item 2) ou Despesas(item 3) ent�o critica al�ada
                    'Alan Neves - corre��o na compara��o de item_val_estimativa 3 - Inicio
                    'If (Val(Pgto.Item_val_estimativa) = 2 Or Val(Pgto.Item_val_estimativa = 3)) Then
                    If (Val(Pgto.Item_val_estimativa) = 2 Or Val(Pgto.Item_val_estimativa) = 3) Then
                    'Alan Neves - corre��o na compara��o de item_val_estimativa 3 - Fim
                    
                        'Se a moeda for DOLAR(220) ent�o deve calcular o valor em real para verifica��o do limite de Al�ada
                        AMoeda_Sinistro = Obtem_Moeda_Sinistro(Sin)
                        If AMoeda_Sinistro = DOLAR_MOEDA_ID Then
                           
                           'Alan Neves - MP - Inicio - 06/02/2013
                           'Obt�m a paridade da moeda nesta data
'                           SQL = "SELECT val_paridade_moeda "
'                           SQL = SQL & " FROM paridade_tb WITH(NOLOCK) "
'                           SQL = SQL & " WHERE dt_conversao     = '" & Format(Data_Sistema, "yyyymmdd") & "'"
'                           SQL = SQL & "   AND origem_moeda_id  = " & DOLAR_MOEDA_ID
'                           SQL = SQL & "   AND destino_moeda_id = " & REAL_MOEDA_ID
'                           Set rs = rdocn.OpenResultset(SQL)
                           
'                           If Not rs.EOF Then
                            If achou_paridade Then
'                               OValor_Pago = TruncaDecimal(CDbl(Pgto.val_Acerto) * Val(rs("val_paridade_moeda")))
                               OValor_Pago = TruncaDecimal(CDbl(Pgto.val_Acerto) * Val(vval_paridade_moeda))
                           Else
                               'rs.Close
                               rdocn.RollbackTrans 'C_ANEVES / msalema - 21/06/2013
                               MsgBox "N�o existe Paridade cadastrada para esta data de solicita��o de pagamento! " & vbCrLf & _
                                      "Entre em contato com a �rea respons�vel.", vbOKOnly + vbExclamation
                               'rdocn.RollbackTrans 'C_ANEVES / msalema - 21/06/2013
                               GoTo Trata_erro
                           End If
                           
                           'rs.Close
                           'Alan Neves - MP - Fim - 06/02/2013
                           
                        Else
                           OValor_Pago = CDbl(Pgto.val_Acerto)
                        End If
                        
'                        vProduto_ID = Obtem_Produto_ID(flexPgtoSinistro.TextMatrix(i, 0), flexPgtoSinistro.TextMatrix(i, 1), flexPgtoSinistro.TextMatrix(i, 2), flexPgtoSinistro.TextMatrix(i, 3), flexPgtoSinistro.TextMatrix(i, 4))
'                        OValor_Pago = CDbl(Pgto.val_Acerto)
'                        'Verifica se o t�cnico que solicitou o pagamento tem al�ada para a aprova��o autom�tica
'                        If Not Tecnico_tem_Alcada(Pgto.Item_val_estimativa, Sin.ramo_id, Sin.produto_id, OValor_Pago) Then
'                           Screen.MousePointer = 0
'                           rdocn.RollbackTrans
'                           Exit Sub
'                        End If
                    End If
                    
                    'Alan Neves - MP - 18/03/2013 - Inicio
                    'vProduto_ID = Obtem_Produto_ID(Sin.sinistro_id, Sin.Apolice_id, Sin.Sucursal_id, Sin.Seguradora_id, Sin.ramo_id)
                    vProduto_ID = Sin.produto_id
                    'Alan Neves - MP - 18/03/2013 - Fim
                                        
                    OValor_Pago = CDbl(Pgto.val_Acerto)
                    
                    ' Demanda: 14129660
                                   
                    If vProduto_ID = 1183 And vResultado = "a" Then
                    
                        SQL = ""
                        SQL = " SELECT convert(numeric(15,2),d.val_is * 1.03) AS val_is " & vbCrLf & _
                        " FROM sinistro_tb a with(nolock) " & vbCrLf & _
                        " INNER JOIN sinistro_cobertura_tb b with(nolock) " & vbCrLf & _
                        " ON b.sinistro_id = a.sinistro_id " & vbCrLf & _
                        " INNER JOIN tp_cob_comp_tb c with(nolock) " & vbCrLf & _
                        " ON c.tp_cobertura_id = b.tp_cobertura_id " & vbCrLf & _
                        " INNER JOIN escolha_plano_tp_cob_tb d with(nolock) " & vbCrLf & _
                        " ON d.tp_cob_comp_id = c.tp_cob_comp_id  " & vbCrLf & _
                        " AND d.proposta_id = a.proposta_id  " & vbCrLf & _
                        " Where a.sinistro_id = " & Sin.sinistro_id & vbCrLf & _
                        " AND b.dt_fim_vigencia IS NULL " & vbCrLf & _
                        " AND d.dt_inicio_vigencia_cob <= a.dt_ocorrencia_sinistro " & vbCrLf & _
                        " AND (d.dt_fim_vigencia_cob >= a.dt_ocorrencia_sinistro OR d.dt_fim_vigencia_cob IS NULL) "
                        
                        Set rs = rdocn.OpenResultset(SQL)
                        If Not rs.EOF Then
                            If CDbl(Pgto.val_Acerto) > Replace(rs!val_is, ".", ",") Then
                                If MsgBox("O valor para pagamento � maior que o capital Segurado Vigente + 3%, deseja prosseguir com a aprova��o?", vbYesNo + vbQuestion, "Saldo") = vbNo Then
                                    rs.Close
                                    rdocn.RollbackTrans
                                    Screen.MousePointer = 1
                                    Exit Sub
                                End If
                            End If
                        End If
                        rs.Close

                    End If
                    Screen.MousePointer = 1
                                        
                    Item_val_estimativa = Pgto.Item_val_estimativa
                    Seq_Estimativa = Pgto.Seq_Estimativa
                    Seq_Pgto = Pgto.Seq_Pgto
                    
                    'Verifica se o t�cnico que solicitou o pagamento tem al�ada para a aprova��o autom�tica
                    'antigo
                    
                    'Alan Neves - MP - 18/03/2013 - Acrescentado nome do produto e do ramo na chamada da function
                    'If Not Tecnico_tem_Alcada(Item_val_estimativa, Sin.ramo_id, Sin.produto_id, OValor_Pago, Sin.Seguradora_id, Sin.Sucursal_id, Sin.Apolice_id, Sin.sinistro_id, Seq_Estimativa, Seq_Pgto) Then
                    If Not Tecnico_tem_Alcada(Item_val_estimativa, Sin.ramo_id, Sin.produto_id, OValor_Pago, Sin.Seguradora_id, Sin.Sucursal_id, Sin.Apolice_id, Sin.sinistro_id, Seq_Estimativa, Seq_Pgto, Sin.NomeProduto, Sin.NomeRamo) Then
                       Screen.MousePointer = 0
                       rdocn.RollbackTrans
                       Exit Sub
                    End If
                    'cgama 26/11/2005
                    'verifica se tem chave no sisbb
                    If Not Tecnico_tem_chave(Sin.sinistro_id, Pgto.Item_val_estimativa, Pgto.Seq_Estimativa) Then
                       Screen.MousePointer = 0
                       rdocn.RollbackTrans
                       Exit Sub
                    End If
                    
                    'Diego Galizoni Caversan - GPTI - 12/11/2009
                    'Demanda 1445418 - N�o permitir que um tecnico aprove sua pr�pria solicita��o
                    If Avaliador_solicitante(Sin.sinistro_id, Pgto.Item_val_estimativa, Pgto.Seq_Estimativa, Pgto.Seq_Pgto, Pgto.Num_Parcela, Pgto.Beneficiario_id) Then
                        Screen.MousePointer = 0
                        rdocn.RollbackTrans
                        MsgBox "Usu�rio Solicitante n�o pode ser Avaliador."
                        Exit Sub
                    End If
                    'fim
                    
                    SQL = "EXEC pgto_sinistro_aprova_spu " & _
                    Sin.sinistro_id & "," & _
                    Sin.Apolice_id & "," & _
                    Sin.Sucursal_id & "," & _
                    Sin.Seguradora_id & "," & _
                    Sin.ramo_id & "," & _
                    Pgto.Item_val_estimativa & "," & _
                    Pgto.Seq_Estimativa & "," & _
                    Pgto.Seq_Pgto & "," & _
                    Pgto.Num_Parcela & "," & _
                    Pgto.Beneficiario_id & ",'" & _
                    vConjunto & "','" & _
                    vResultado & "','" & _
                    txtRejeicao & "','" & _
                    Sin.Situacao & "'," & _
                    "'" & Format(Data_Sistema, "yyyymmdd") & "'," & _
                    "'" & cUserName & "', " & _
                    "NULL, NULL"
                    Set rs = rdocn.OpenResultset(SQL)
                    rs.Close
                    '---------------------------------------------INC000004279651---------------------------------------------------INICIO
                    '---ALTERA��O DO TRECHO DO C�DIGO DE COLOCA��O PARA DENTRO DO LA�O DO FOR.
                    '---ALTERA��O DA CONSISTENCIA DE APENAS 2 LINHAS (CABE�ALHO E UM REGISTRO)
                    '---HENRIQUE H. HENRIQUES - CONFITEC(SP) - 29/05/2014
                    If flexPgtoSinistro.Rows > 1 And (vResultado = "r" Or vResultado = "c") And Val(Pgto.Item_val_estimativa) = 1 Then
                        '---------------------------------------------------------------------------------------------------------
                        ' Demanda: 18037006
                        ' Realizar a exclus�o do saldo devedor quando pagamento for rejeitado ou cancelado.
                        ' Eduardo.Maior - Confitec Sistemas - 30/05/2014
                        '---------------------------------------------------------------------------------------------------------
                        SQL = "SELECT sinistro_solicita_saldo " & _
                        "  FROM produto_tb WITH(NOLOCK) " & _
                        " WHERE produto_id = " & Sin.produto_id & _
                        "   AND sinistro_solicita_saldo = 'S' "
                        
                        Set rs = rdocn.OpenResultset(SQL)
                        If Not rs.EOF Then
                            
                            SQL = "SELECT id_beneficiario_bb " & _
                            "  FROM sinistro_benef_tb WITH(NOLOCK) " & _
                            " WHERE sinistro_id = " & Sin.sinistro_id & _
                            "   AND beneficiario_id = " & Pgto.Beneficiario_id
                            
                            Set rs = rdocn.OpenResultset(SQL)
                            
                            If Not rs.EOF Then
                                If LCase(rs!id_beneficiario_bb) = "s" Then
                                    rs.Close
                                    SQL = "EXEC SEGS10119_SPD " & Sin.sinistro_id & ", NULL"
                                    Set rs = rdocn.OpenResultset(SQL)
                                End If
                            End If
                        End If
                        rs.Close
                    End If
                    '---------------------------------------------INC000004279651---------------------------------------------------FIM
                    '---ALTERA��O DO TRECHO DO C�DIGO DE COLOCA��O PARA DENTRO DO LA�O DO FOR.
                    '---ALTERA��O DA CONSISTENCIA DE APENAS 2 LINHAS (CABE�ALHO E UM REGISTRO)
                    '---HENRIQUE H. HENRIQUES - CONFITEC(SP) - 29/05/2014
                Next i
                
                'Alan Neves - MP - 21/03/2013 - Inicio - Atualiza a temporaria devido a execu��o da pgto_sinistro_aprova_spu
                frmAcertoCon.SelecionaDadosPgtoSinistro
                'Alan Neves - MP - 21/03/2013 - Fim
                
                '---------------------------------------------INC000004279651---------------------------------------------------INICIO
                '---ALTERA��O DO TRECHO DO C�DIGO DE COLOCA��O PARA DENTRO DO LA�O DO FOR ACIMA.
                '---HENRIQUE H. HENRIQUES - CONFITEC(SP) - 29/05/2014
                ' Demanda: 12967997 '
                ' Quando o pagto for cancelado, eh para excluir da tabela sinistro_solicita_saldo_tb para nao pedir mais saldo '
                ' COMECO - Anderson Teixeira - Confitec - 29/02/2012 '
                
'                ' Verificar se existe apenas 1 pagto mas o usuario clicou na opcao todos '
'                If flexPgtoSinistro.Rows = 2 And vResultado = "r" And Val(Pgto.Item_val_estimativa) = 1 Then
'
'                    'If vProduto_ID = "1198" Or vProduto_ID = "1183" Then
'                    If vProduto_ID = "1198" Or vProduto_ID = "1183" Or vProduto_ID = "1205" Then
'
'                        SQL = "SELECT id_beneficiario_bb " & _
'                        "  FROM sinistro_benef_tb WITH(NOLOCK) " & _
'                        " Where sinistro_id = " & Sin.sinistro_id & _
'                        "       AND beneficiario_id = " & Pgto.Beneficiario_id
'
'                        Set rs = rdocn.OpenResultset(SQL)
'
'                        If Not rs.EOF Then
'                            If LCase(rs!id_beneficiario_bb) = "s" Then
'                                rs.Close
'                                SQL = "EXEC SEGS10119_SPD " & Sin.sinistro_id & ", NULL"
'                                Set rs = rdocn.OpenResultset(SQL)
'                            End If
'                        End If
'                        rs.Close
'                    End If
'
'                End If
                Screen.MousePointer = 1
                '---------------------------------------------INC000004279651---------------------------------------------------FIM
                '---ALTERA��O DO TRECHO DO C�DIGO DE COLOCA��O PARA DENTRO DO LA�O DO FOR ACIMA.
                '---HENRIQUE H. HENRIQUES - CONFITEC(SP) - 29/05/2014
                ' FIM - Demanda: 12967997 '
                
                'Verifica se realmente todos os pagamentos foram aprovados
                
                'Alan Neves - MP - 12/03/2013 - Inicio
                'SQL = "SELECT * FROM pgto_sinistro_tb WITH(NOLOCK) "
                SQL = "SELECT 1 FROM #pgto_sinistro_tb WITH(NOLOCK) "
                'Alan Neves - MP - 12/03/2013 - Fim
                SQL = SQL & " WHERE sinistro_id             = " & Sin.sinistro_id
                SQL = SQL & " AND   apolice_id              = " & Sin.Apolice_id
                SQL = SQL & " AND   sucursal_seguradora_id  = " & Sin.Sucursal_id
                SQL = SQL & " AND   seguradora_cod_susep    = " & Sin.Seguradora_id
                SQL = SQL & " AND   ramo_id                 = " & Sin.ramo_id
                SQL = SQL & " AND   situacao_op             = 'n'"
                SQL = SQL & " AND   item_val_estimativa     = 1"
                Set rs = rdocn1.OpenResultset(SQL)
                
                If Not rs.EOF Then
                   rs.Close
                   rdocn.RollbackTrans
                   MsgBox "Erro na aprova��o do pagamento. " & vbCrLf & "Nem todos os pagamentos foram aprovados.", vbOKOnly + vbExclamation
                   GoTo Trata_erro
                End If
                rs.Close
            Else
                
                Set Pgto = Pagamentos.Item(flexPgtoSinistro.Row)
                
                '-----------------------------------------------------------------
                'Demanda 689336 - Talitha - 25/09/2009 - Indeniza��o superior IS.
                '-----------------------------------------------------------------
                If Val(Pgto.Item_val_estimativa) = 1 Then
                    If Validar_Indenizacao_Superior_IS(Sin.sinistro_id, Pgto.tp_Cobertura_id, CDbl(Pgto.val_Acerto) + CDbl(Pgto.val_Correcao)) Then
                        'HENRIQUE H. HENRIQUES - CONFITEC SP - INI (VISUALIZANDO VALOR A SER APROVADO PARA INDENIZA��O)
                        If MsgBox("Valor da Indeniza��o � superior ao Valor do IS. Deseja continuar?" & vbNewLine & "Valor de indeniza��o: " & (CDbl(Pgto.val_Acerto) + CDbl(Pgto.val_Correcao)), vbYesNo) = vbNo Then
                        'HENRIQUE H. HENRIQUES - CONFITEC SP - FIM (VISUALIZANDO VALOR A SER APROVADO PARA INDENIZA��O)
                            Screen.MousePointer = 0
                            rdocn.RollbackTrans
                            Exit Sub
                        End If
                    End If
                    
                    'Alan Neves - MP - 12/03/2013 - Inicio
                    'vProduto_ID = Obtem_Produto_ID(Sin.sinistro_id, Sin.Apolice_id, Sin.Sucursal_id, Sin.Seguradora_id, Sin.ramo_id)
                    vProduto_ID = Sin.produto_id
                    'Alan Neves - MP - 12/03/2013 - Fim
                    'vProduto_ID = Obtem_Produto_ID(flexPgtoSinistro.TextMatrix(i, 0), flexPgtoSinistro.TextMatrix(i, 1), flexPgtoSinistro.TextMatrix(i, 2), flexPgtoSinistro.TextMatrix(i, 3), flexPgtoSinistro.TextMatrix(i, 4))
                    
                    If vProduto_ID = 1183 And vResultado = "a" Then
                        SQL = ""
                        SQL = " SELECT convert(numeric(15,2),d.val_is * 1.03) AS val_is " & vbCrLf & _
                        " FROM sinistro_tb a with(nolock) " & vbCrLf & _
                        " INNER JOIN sinistro_cobertura_tb b with(nolock) " & vbCrLf & _
                        " ON b.sinistro_id = a.sinistro_id " & vbCrLf & _
                        " INNER JOIN tp_cob_comp_tb c with(nolock) " & vbCrLf & _
                        " ON c.tp_cobertura_id = b.tp_cobertura_id " & vbCrLf & _
                        " INNER JOIN escolha_plano_tp_cob_tb d with(nolock) " & vbCrLf & _
                        " ON d.tp_cob_comp_id = c.tp_cob_comp_id  " & vbCrLf & _
                        " AND d.proposta_id = a.proposta_id  " & vbCrLf & _
                        " Where a.sinistro_id = " & Sin.sinistro_id & vbCrLf & _
                        " AND b.dt_fim_vigencia IS NULL " & vbCrLf & _
                        " AND d.dt_inicio_vigencia_cob <= a.dt_ocorrencia_sinistro " & vbCrLf & _
                        " AND (d.dt_fim_vigencia_cob >= a.dt_ocorrencia_sinistro OR d.dt_fim_vigencia_cob IS NULL) "

                        
                        Set rs = rdocn.OpenResultset(SQL)
                        If Not rs.EOF Then
                            If CDbl(Pgto.val_Acerto) > Replace(rs!val_is, ".", ",") Then
                                If MsgBox("O valor para pagamento � maior que o capital Segurado Vigente + 3%, deseja prosseguir com a aprova��o?", vbYesNo + vbQuestion, "Saldo") = vbNo Then
                                    rs.Close
                                    rdocn.RollbackTrans
                                    Screen.MousePointer = 1
                                    Exit Sub
                                End If
                            End If
                        End If
                        rs.Close
                    End If
                    
                    Screen.MousePointer = 1
                   
                End If
                'Se o item for Honor�rios(item 2) ou Despesas(item 3) ent�o critica al�ada
                If (Val(Pgto.Item_val_estimativa) = 2 Or Val(Pgto.Item_val_estimativa = 3)) Then
                    'Se a moeda for DOLAR(220) ent�o deve calcular o valor em real para verifica��o do limite de Al�ada
                    AMoeda_Sinistro = Obtem_Moeda_Sinistro(Sin)
                    If AMoeda_Sinistro = DOLAR_MOEDA_ID Then
                       
                       'Alan Neves - MP - Inicio - 06/02/2013
                       'Obt�m a paridade da moeda nesta data
'                       SQL = "SELECT val_paridade_moeda "
'                       SQL = SQL & " FROM paridade_tb WITH(NOLOCK) "
'                       SQL = SQL & " WHERE dt_conversao     = '" & Format(Data_Sistema, "yyyymmdd") & "'"
'                       SQL = SQL & "   AND origem_moeda_id  = " & DOLAR_MOEDA_ID
'                       SQL = SQL & "   AND destino_moeda_id = " & REAL_MOEDA_ID
'                       Set rs = rdocn.OpenResultset(SQL)
'                       If Not rs.EOF Then
                       
                       If achou_paridade Then
                           OValor_Pago = TruncaDecimal(CDbl(Pgto.val_Acerto) * Val(vval_paridade_moeda))
                       
'                           OValor_Pago = TruncaDecimal(CDbl(Pgto.val_Acerto) * Val(rs("val_paridade_moeda")))
                       Else
                           'Alan Neves
                           'rs.Close
                           
                           rdocn.RollbackTrans 'C_ANEVES / msalema - 21/06/2013
                           MsgBox "N�o existe Paridade cadastrada para esta data de solicita��o de pagamento! " & vbCrLf & _
                                  "Entre em contato com a �rea respons�vel.", vbOKOnly + vbExclamation
                           'rdocn.RollbackTrans 'C_ANEVES / msalema - 21/06/2013
                           GoTo Trata_erro
                       End If
                       
                       ' rs.Close
                       'Alan Neves - MP - Fim - 06/02/2013
                    Else
                       OValor_Pago = CDbl(Pgto.val_Acerto)
                    End If
                    
                    
'                    OValor_Pago = CDbl(Pgto.val_Acerto)
'                    'Verifica se o t�cnico que solicitou o pagamento tem al�ada para a aprova��o autom�tica
'                    If Not Tecnico_tem_Alcada(Pgto.Item_val_estimativa, Sin.ramo_id, Sin.Produto_id, OValor_Pago) Then
'                       Screen.MousePointer = 0
'                       rdocn.RollbackTrans
'                       Exit Sub
'                    End If
                End If
                
                'Inclu�do por Gustavo Machado em 30/07/2004
                OValor_Pago = CDbl(Pgto.val_Acerto)
                Seq_Estimativa = Pgto.Seq_Estimativa
                Seq_Pgto = Pgto.Seq_Pgto
                  
                'Alan Neves - MP - 18/03/2013 - Inicio - Acrescentado nome do produto e do ramo na chamada da function
                'If Not Tecnico_tem_Alcada(Pgto.Item_val_estimativa, Sin.ramo_id, Sin.produto_id, OValor_Pago, Sin.Seguradora_id, Sin.Sucursal_id, Sin.Apolice_id, Sin.sinistro_id, Seq_Estimativa, Seq_Pgto) Then
                If Not Tecnico_tem_Alcada(Pgto.Item_val_estimativa, Sin.ramo_id, Sin.produto_id, OValor_Pago, Sin.Seguradora_id, Sin.Sucursal_id, Sin.Apolice_id, Sin.sinistro_id, Seq_Estimativa, Seq_Pgto, Sin.NomeProduto, Sin.NomeRamo) Then
                       Screen.MousePointer = 0
                       rdocn.RollbackTrans
                       Exit Sub
                End If
                
                'Diego Galizoni Caversan - GPTI - 12/11/2009
                'Demanda 1445418 - N�o permitir que um tecnico aprove sua pr�pria solicita��o
                  If Avaliador_solicitante(Sin.sinistro_id, Pgto.Item_val_estimativa, Pgto.Seq_Estimativa, Pgto.Seq_Pgto, Pgto.Num_Parcela, Pgto.Beneficiario_id) Then
                    Screen.MousePointer = 0
                    rdocn.RollbackTrans
                    MsgBox "Usu�rio Solicitante n�o pode ser Avaliador."
                    Exit Sub
                End If
                'fim
                
                SQL = "EXEC pgto_sinistro_aprova_spu " & _
                Sin.sinistro_id & "," & _
                Sin.Apolice_id & "," & _
                Sin.Sucursal_id & "," & _
                Sin.Seguradora_id & "," & _
                Sin.ramo_id & "," & _
                Pgto.Item_val_estimativa & "," & _
                Pgto.Seq_Estimativa & "," & _
                Pgto.Seq_Pgto & "," & _
                Pgto.Num_Parcela & "," & _
                Pgto.Beneficiario_id & ",'" & _
                vConjunto & "','" & _
                vResultado & "','" & _
                txtRejeicao & "','" & _
                Sin.Situacao & "'," & _
                "'" & Format(Data_Sistema, "yyyymmdd") & "'," & _
                "'" & cUserName & "', " & _
                "NULL, NULL"
                Set rs = rdocn.OpenResultset(SQL)
                rs.Close
                
                
                'Alan Neves - MP - 21/03/2013 - Inicio - Atualiza a temporaria devido a execu��o da pgto_sinistro_aprova_spu
                frmAcertoCon.SelecionaDadosPgtoSinistro
                'Alan Neves - MP - 21/03/2013 - Fim
                
                
                
                ' Demanda: 12967997 '
                ' Quando o pagto for cancelado, eh para excluir da tabela sinistro_solicita_saldo_tb para nao pedir mais saldo '
                ' COMECO - Anderson Teixeira - Confitec - 29/02/2012 '
                
                'vProduto_ID = Obtem_Produto_ID(flexPgtoSinistro.TextMatrix(i, 0), flexPgtoSinistro.TextMatrix(i, 1), flexPgtoSinistro.TextMatrix(i, 2), flexPgtoSinistro.TextMatrix(i, 3), flexPgtoSinistro.TextMatrix(i, 4))
                    
                
                If Val(Pgto.Item_val_estimativa) = 1 Then
                
                    'If vResultado = "r" And (vProduto_ID = "1198" Or vProduto_ID = "1183") Then
                       
                    '---------------------------------------------------------------------------------------------------------
                    ' Demanda: 18037006
                    ' Realizar a exclus�o do saldo devedor quando pagamento for rejeitado ou cancelado.
                    ' Eduardo.Maior - Confitec Sistemas - 30/05/2014
                    '---------------------------------------------------------------------------------------------------------
                    If (vResultado = "r" Or vResultado = "c") Then
            
                        SQL = "SELECT sinistro_solicita_saldo " & _
                            "  FROM produto_tb WITH(NOLOCK) " & _
                            " WHERE produto_id = " & Sin.produto_id & _
                            "   AND sinistro_solicita_saldo = 'S' "
                            
                        Set rs = rdocn.OpenResultset(SQL)
                        If Not rs.EOF Then
                            
                            SQL = "SELECT id_beneficiario_bb " & _
                            "  FROM sinistro_benef_tb WITH(NOLOCK) " & _
                            " WHERE sinistro_id = " & Sin.sinistro_id & _
                            "   AND beneficiario_id = " & Pgto.Beneficiario_id
                            
                            Set rs = rdocn.OpenResultset(SQL)
                            
                            If Not rs.EOF Then
                                If LCase(rs!id_beneficiario_bb) = "s" Then
                                    rs.Close
                                    SQL = "EXEC SEGS10119_SPD " & Sin.sinistro_id & ", NULL"
                                    Set rs = rdocn.OpenResultset(SQL)
                                End If
                            End If
                        End If
                        rs.Close
                    End If
                
                End If
                Screen.MousePointer = 1
                
                ' Demanda: 12967997 - FIM '
            
            End If
            
            'Alan Neves - C_ANEVES - 15/05/2013 - Inicio
'            If vResultado = "r" Then
'                MensagemAcerto = "Sinistro: " & Sin.sinistro_id & vbNewLine
'                MensagemAcerto = MensagemAcerto & "Ap�lice: " & Sin.Apolice_id & vbNewLine
'                MensagemAcerto = MensagemAcerto & "Ramo: " & Sin.NomeRamo & vbNewLine
'                MensagemAcerto = MensagemAcerto & "Estimativa: " & Estimativa_texto(Pgto.Item_val_estimativa) & vbNewLine
'                MensagemAcerto = MensagemAcerto & "Benefici�rio: " & Pgto.Beneficiario_Nome & vbNewLine
'                MensagemAcerto = MensagemAcerto & "Data da rejei��o: " & Format(Data_Sistema, "dd/mm/yyyy") & vbNewLine
'                MensagemAcerto = MensagemAcerto & "Rejeitado por: " & cUserName & vbNewLine & vbNewLine
'                MensagemAcerto = MensagemAcerto & "Motivo: " & txtRejeicao
'                Destinatario = Pgto.Usuario
'                ' Manda e-mail
'                If MAPISess.SessionID = 0 Then
'                    Logon_Click
'                End If
'                If SendMsg("r") = False Then
'                    Screen.MousePointer = 0
'                    rdocn.RollbackTrans
'                    Exit Sub
'                End If
'            End If
            'Alan Neves - C_ANEVES - 15/05/2013 - Fim
        End If
        
        rdocn.CommitTrans
        
        'Enviar email
        'Alan Neves - C_ANEVES - 15/05/2013 - Inicio
        If frameAutorizacao.Visible Then ' Autoriza todos da OP
            
            For i = 1 To flexPgtoSinistro.Rows - 1
                If vResultado = "r" Then
                    MensagemAcerto = "Autoriza��o N�: " & txtNumAutorizacao.Text & vbNewLine
                    MensagemAcerto = MensagemAcerto & "Sinistro: " & flexPgtoSinistro.TextMatrix(i, 0) & vbNewLine
                    MensagemAcerto = MensagemAcerto & "Ap�lice: " & flexPgtoSinistro.TextMatrix(i, 1) & vbNewLine
                    MensagemAcerto = MensagemAcerto & "Ramo: " & flexPgtoSinistro.TextMatrix(i, 4) & vbNewLine
                    MensagemAcerto = MensagemAcerto & "Estimativa: " & flexPgtoSinistro.TextMatrix(i, 16) & vbNewLine
                    MensagemAcerto = MensagemAcerto & "Benefici�rio: " & txtLider.Text & vbNewLine
                    MensagemAcerto = MensagemAcerto & "Data da rejei��o: " & Format(Data_Sistema, "dd/mm/yyyy") & vbNewLine
                    MensagemAcerto = MensagemAcerto & "Rejeitado por: " & cUserName & vbNewLine & vbNewLine
                    MensagemAcerto = MensagemAcerto & "Motivo: " & txtRejeicao
                    Destinatario = flexPgtoSinistro.TextMatrix(i, 21)
                    ' Manda e-mail
                    If MAPISess.SessionID = 0 Then
                        Logon_Click
                    End If
                                        
                    If SendMsg("r") = False Then
                        MsgBox "Email de aprova��o da lista de autoriza��o n�o enviado!", vbInformation, Caption
                    End If
                End If
            Next i
        Else
            If vResultado = "r" Then
                MensagemAcerto = "Sinistro: " & Sin.sinistro_id & vbNewLine
                MensagemAcerto = MensagemAcerto & "Ap�lice: " & Sin.Apolice_id & vbNewLine
                MensagemAcerto = MensagemAcerto & "Ramo: " & Sin.NomeRamo & vbNewLine
                MensagemAcerto = MensagemAcerto & "Estimativa: " & Estimativa_texto(Pgto.Item_val_estimativa) & vbNewLine
                MensagemAcerto = MensagemAcerto & "Benefici�rio: " & Pgto.Beneficiario_Nome & vbNewLine
                MensagemAcerto = MensagemAcerto & "Data da rejei��o: " & Format(Data_Sistema, "dd/mm/yyyy") & vbNewLine
                MensagemAcerto = MensagemAcerto & "Rejeitado por: " & cUserName & vbNewLine & vbNewLine
                MensagemAcerto = MensagemAcerto & "Motivo: " & txtRejeicao
                Destinatario = Pgto.Usuario
                ' Manda e-mail
                If MAPISess.SessionID = 0 Then
                    Logon_Click
                End If
                
                If SendMsg("r") = False Then
                    Screen.MousePointer = 0
                    MsgBox "Email de aprova��o de pagamento n�o enviado!", vbInformation, Caption
                End If
            End If
        End If
        
        'Alan Neves
        
        Screen.MousePointer = 0
        MsgBox "Processo Executado Com Sucesso!", vbInformation, Caption
        Esta_Cancelando = False
        
        If optTodos.Value Then
            Unload Me
        Else
            Screen.MousePointer = 11
            Monta_Pgto
            ''pcarvalho - 17/08/2004 - Grid de Pagtos Aprovados
            Call MontarGrdPgtosAprovados
            Screen.MousePointer = 0
            If Pagamentos.Count = 0 Then
                Unload Me
            Else
                optAprovado.Value = False
                optRejeitado.Value = False
                optSelecionado.Value = False
                optTodos.Value = False
                frameRejeitado.Visible = False
                frameEstornar.Visible = False
                BtnCancelar.SetFocus
            End If
        End If
    
    Else 'Par�metro <> I
        
        If frameCancReapresentar.Visible Then
            If Not optCancelado.Value And Not optReapresenta.Value Then
                mensagem_erro 6, "Altera��o n�o indicada!"
                Exit Sub
            End If
            If optCancelado.Value Then
                vResultado = "c"
            Else
                vResultado = "n"
            End If
        ElseIf frameCancelar.Visible Then
            If Not optS�Cancela.Value Then
                mensagem_erro 6, "Altera��o n�o indicada!"
                Exit Sub
            End If
            vResultado = "c"
        ElseIf frameEstornar.Visible Then
            If Not optEstornar.Value Then
                mensagem_erro 6, "Altera��o n�o indicada!"
                Exit Sub
            End If
            vResultado = "e"
        End If
        If frameRejeitado.Visible Then
            txtRejeicao = MudaAspaSimples(txtRejeicao)
        End If
'+-----------------------------------------------------------
'| 19406709 - Melhoria nos limites de sinistro e emissao
'| RN05 - petrauskas - 03/2017
        If frame_justifictiva.Visible And Trim(txt_justificativa.Text) = "" Then
            mensagem_erro 6, "Valor da indeniza��o � maior que o valor da import�ncia segurada (IS). Por favor, justifique sua aprova��o!"
            Exit Sub
        ElseIf frame_justifictiva.Visible Then
            txt_justificativa.Text = MudaAspaSimples(txt_justificativa.Text)
            txtRejeicao.Text = txt_justificativa.Text 'rotina sempre grava txtRejeicao.text.
        End If
'| 19406709 - Melhoria nos limites de sinistro e emissao
'+-----------------------------------------------------------
        
        If frameAutorizacao.Visible Then
            If flexPgtoSinistro.TextMatrix(1, 14) = "ESTORNADO" Then
                mensagem_erro 6, "Esta autoriza��o j� foi estornada!"
                Exit Sub
            ElseIf flexPgtoSinistro.TextMatrix(1, 14) = "CANCELADO" Then
                mensagem_erro 6, "Esta autoriza��o j� foi cancelada!"
                Exit Sub
            End If
        Else
            Set Pgto = Pagamentos.Item(flexPgtoSinistro.Row)
            
            If Pgto.Situacao_OP = "ESTORNADO" Then
                mensagem_erro 6, "Este pagamento/recebimento j� foi estornado!"
                Exit Sub
            ElseIf Pgto.Situacao_OP = "CANCELADO" Then
                mensagem_erro 6, "Este pagamento/recebimento j� foi cancelado!"
                Exit Sub
            End If
        End If
        
        Screen.MousePointer = 11
        
        'Alan Neves - 19/03/2013 - MP - Inicio
        'rdocn.BeginTrans
        'Alan Neves - 19/03/2013 - Fim
        
        'Alterado por Cleber da Stefanini - 21/11/2005 - Flow 112708
        If frameAutorizacao.Visible Then
            For i = 1 To flexPgtoSinistro.Rows - 1
             
                ReDim Preserve iQtdItemEstimativa(i)
            
                'Luciana - 29/09/2005
                
                'Alan Neves - MP - 12/03/2013 - Inicio
                'vsql = "select  count(*)"
                vSql = "select  count(1)"
                'Alan Neves - MP - 12/03/2013 - Fim
                
                'Alan Neves - MP - 19/03/2013 - Inicio - Leitura na temp
                'vsql = vsql & " from sinistro_estimativa_tb with(nolock) "
                vSql = vSql & " from #sinistro_estimativa_tb sinistro_estimativa_tb with(nolock) "
                'Alan Neves - MP - 19/03/2013 - Fim
                
                'vSql = vSql & " where sinistro_id =  " & vSinistro
                'Maur�cio (Stefanini), em 14/11/2005
                vSql = vSql & " where sinistro_id =  " & flexPgtoSinistro.TextMatrix(i, 0)
                vSql = vSql & " and seq_estimativa = (select max(seq_estimativa)"
                
                'Alan Neves - MP - 19/03/2013 - Inicio - Leitura na temp
                'vsql = vsql & " from    sinistro_estimativa_tb se2 with(nolock) "
                vSql = vSql & " from    #sinistro_estimativa_tb se2 with(nolock) "
                'Alan Neves - MP - 19/03/2013 - Fim
                
                vSql = vSql & " where   se2.sinistro_id = sinistro_estimativa_tb.sinistro_id)"
                
                'msalema - 20/06/2013
                'Set rsAuxi = rdocn.OpenResultset(vSql)
                Set rsAuxi = rdocn1.OpenResultset(vSql)
                
                
                iQtdItemEstimativa(i) = IIf(IsNull(rsAuxi(0)), 0, rsAuxi(0))
                rsAuxi.Close
                
            Next
        Else
            'Alan Neves - MP - 12/03/2013 - Inicio
            'vsql = "select  count(*)"
            vSql = "select  count(1)"
            
            'Alan Neves - MP - 19/03/2013 - Inicio - Leitura na temp
            'vsql = vsql & " from sinistro_estimativa_tb with(nolock) "
            vSql = vSql & " from #sinistro_estimativa_tb sinistro_estimativa_tb "
            'Alan Neves - MP - 19/03/2013 - Fim
            
            vSql = vSql & " where sinistro_id =  " & vSinistro
            vSql = vSql & " and seq_estimativa = (select max(seq_estimativa)"
            
            'Alan Neves - MP - 19/03/2013 - Inicio - Leitura na temp
            'vsql = vsql & " from    sinistro_estimativa_tb se2 with(nolock) "
            vSql = vSql & " from    #sinistro_estimativa_tb se2 with(nolock) "
            'Alan Neves - MP - 19/03/2013 - Fim
            
            vSql = vSql & " where   se2.sinistro_id = sinistro_estimativa_tb.sinistro_id)"
            
            Set rsAuxi = rdocn1.OpenResultset(vSql)
            
            countQtdItemEstimativa = IIf(IsNull(rsAuxi(0)), 0, rsAuxi(0))
            rsAuxi.Close
        
        End If
        
        'Alan Neves - 19/03/2013 - MP - Inicio
        rdocn.BeginTrans
        'Alan Neves - 19/03/2013 - Fim
        
        If frameAutorizacao.Visible Then ' Altera todos da OP
            
            For i = 1 To flexPgtoSinistro.Rows - 1
            
                SQL = "EXEC pgto_sinistro_alt_sit_spu " & _
                    flexPgtoSinistro.TextMatrix(i, 0) & "," & _
                    flexPgtoSinistro.TextMatrix(i, 1) & "," & _
                    flexPgtoSinistro.TextMatrix(i, 2) & "," & _
                    flexPgtoSinistro.TextMatrix(i, 3) & "," & _
                    flexPgtoSinistro.TextMatrix(i, 4) & ","
        
                Select Case flexPgtoSinistro.TextMatrix(i, 16)
                    Case "INDENIZA��O"
                        SQL = SQL & "1, "
                    Case "HONOR�RIOS"
                        SQL = SQL & "2, "
                    Case "DESPESAS"
                        SQL = SQL & "3, "
                    Case "RESSARCIMENTO"
                        SQL = SQL & "4, "
                    Case "DESP. RESSARCIMENTO"
                        SQL = SQL & "5, "
                    Case "SALVADOS"
                        SQL = SQL & "6, "
                    Case "DESP. SALVADOS"
                        SQL = SQL & "7, "
                End Select
                SQL = SQL & _
                    flexPgtoSinistro.TextMatrix(i, 5) & "," & _
                    flexPgtoSinistro.TextMatrix(i, 6) & "," & _
                    flexPgtoSinistro.TextMatrix(i, 7) & "," & _
                    flexPgtoSinistro.TextMatrix(i, 8) & ",'" & _
                    vResultado & "','" & _
                    txtRejeicao & "','" & _
                    cUserName & "'"
        
                Set rs = rdocn.OpenResultset(SQL)
                rs.Close
                
                ' Demanda: 12967997 '
                ' Quando o pagto for cancelado, eh para excluir da tabela sinistro_solicita_saldo_tb para nao pedir mais saldo '
                ' COMECO - Anderson Teixeira - Confitec - 29/02/2012 '
            
'                vProduto_ID = Obtem_Produto_ID(flexPgtoSinistro.TextMatrix(i, 0), flexPgtoSinistro.TextMatrix(i, 1), flexPgtoSinistro.TextMatrix(i, 2), flexPgtoSinistro.TextMatrix(i, 3), flexPgtoSinistro.TextMatrix(i, 4))
'
'                If vResultado = "c" And (vProduto_ID = "1198" Or vProduto_ID = "1183") Then
'
'                    SQL = "SELECT id_beneficiario_bb " & _
'                    SQL = "  FROM sinistro_benef_tb WITH(NOLOCK) " & _
'                    SQL = " Where sinistro_id = " & Sin.sinistro_id & _
'                    SQL = "       AND beneficiario_id = " & CInt(flexPgtoSinistro.TextMatrix(i, 8))
'
'                    Set rs = rdocn.OpenResultset(SQL)
'
'                    If Not rs.EOF Then
'                        If LCase(rs!id_beneficiario_bb) = "s" Then
'                            rs.Close
'                            SQL = "EXEC SEGS10119_SPD " & Sin.sinistro_id & ", NULL"
'                            Set rs = rdocn.OpenResultset(SQL)
'                        End If
'                    End If
'                    rs.Close
'                End If
                
                ' Demanda: 12967997 - FIM '
                
                'Alan Neves - MP - 21/03/2013 - Inicio - Atualiza a temporaria devido a execu��o da pgto_sinistro_alt_sit_spu
                SQL = "set nocount on" & vbNewLine
                SQL = SQL & " update #sinistro_tb " & vbNewLine
                SQL = SQL & "    set situacao = sinistro_tb.situacao" & vbNewLine
                SQL = SQL & "   from #sinistro_tb #sinistro_tb  " & vbNewLine
                SQL = SQL & "   join seguros_db..sinistro_tb sinistro_tb with(nolock)                             " & vbNewLine
                SQL = SQL & "     on sinistro_tb.sinistro_id = #sinistro_tb.sinistro_id                           " & vbNewLine
                SQL = SQL & "  where #sinistro_tb.sinistro_id             = " & flexPgtoSinistro.TextMatrix(i, 0) & vbNewLine
                'rdocn.OpenResultset (SQL)  ' Alan Neves - C_ANEVES - 20/06/2013
                rdocn1.OpenResultset (SQL)  ' Alan Neves - C_ANEVES - 20/06/2013
                
                frmAcertoCon.SelecionaDadosPgtoSinistro
                'Alan Neves - MP - 21/03/2013 - Fim
                
                'Luciana - 29/09/2005
                
                'Alan Neves - MP - 12/03/2013 - inicio
                'vsql = "select  count(*)"
                vSql = "select  count(1)"
                'Alan Neves - MP - 12/03/2013 - Fim
                
                'Alan Neves - MP - 19/03/2013 - Inicio - Leitura na temp
                'vsql = vsql & " from sinistro_estimativa_tb with(nolock) "
                vSql = vSql & " from #sinistro_estimativa_tb sinistro_estimativa_tb "
                'Alan Neves - MP - 19/03/2013 - Fim
                
                'vSql = vSql & " where sinistro_id =  " & vSinistro
                'Maur�cio (Stefanini), em 14/11/2005
                vSql = vSql & " where sinistro_id =  " & flexPgtoSinistro.TextMatrix(i, 0)
                vSql = vSql & " and seq_estimativa = (select max(seq_estimativa)"
                
                'Alan Neves - MP - 19/03/2013 - Inicio - Leitura na temp
                'vsql = vsql & " from    sinistro_estimativa_tb se2 with(nolock) "
                vSql = vSql & " from    #sinistro_estimativa_tb se2 "
                'Alan Neves - MP - 19/03/2013 - Fim
                
                vSql = vSql & " where   se2.sinistro_id = sinistro_estimativa_tb.sinistro_id)"
                
                Set rsAuxi = rdocn1.OpenResultset(vSql)
                
                If iQtdItemEstimativa(i) <> rsAuxi(0) Then
                        MsgBox "Sequencial de estimativas incorreto !" & vbCrLf _
                                & "Por favor, abra um FLOWBR relatando este problema com o print da tela!" & vbCrLf _
                                & SQL, vbExclamation
                End If
                
                rsAuxi.Close
        
'               Alan Neves - C_ANEVES - 15/05/2013 - Inicio
'                If vResultado = "c" Then
'                    ' Manda e-mail
'                        MensagemAcerto = "Autoriza��o N�: " & txtNumAutorizacao.Text & vbNewLine
'                        MensagemAcerto = MensagemAcerto & "Sinistro: " & flexPgtoSinistro.TextMatrix(i, 0) & vbNewLine
'                        MensagemAcerto = MensagemAcerto & "Ap�lice: " & flexPgtoSinistro.TextMatrix(i, 1) & vbNewLine
'                        MensagemAcerto = MensagemAcerto & "Ramo: " & flexPgtoSinistro.TextMatrix(i, 4) & vbNewLine
'                        MensagemAcerto = MensagemAcerto & "Estimativa: " & flexPgtoSinistro.TextMatrix(i, 16) & vbNewLine
'                        MensagemAcerto = MensagemAcerto & "Benefici�rio: " & txtLider.Text & vbNewLine
'                        MensagemAcerto = MensagemAcerto & "Data do cancelamento: " & Format(Data_Sistema, "dd/mm/yyyy") & vbNewLine
'                        MensagemAcerto = MensagemAcerto & "Cancelado por: " & cUserName & vbNewLine & vbNewLine
'                        MensagemAcerto = MensagemAcerto & "Motivo: " & txtRejeicao
'                        Destinatario = flexPgtoSinistro.TextMatrix(i, 21)
'                    If MAPISess.SessionID = 0 Then
'                        Logon_Click
'                    End If
'                    If SendMsg("c") = False Then
'                        rdocn.RollbackTrans
'                        Exit Sub
'                    End If
'                End If
'               Alan Neves - C_ANEVES - 15/05/2013 - Inicio
            Next i
        Else
            SQL = "EXEC pgto_sinistro_alt_sit_spu " & _
                            Sin.sinistro_id & "," & _
                            Sin.Apolice_id & "," & _
                            Sin.Sucursal_id & "," & _
                            Sin.Seguradora_id & "," & _
                            Sin.ramo_id & "," & _
                            Pgto.Item_val_estimativa & "," & _
                            Pgto.Seq_Estimativa & "," & _
                            Pgto.Seq_Pgto & "," & _
                            Pgto.Num_Parcela & "," & _
                            Pgto.Beneficiario_id & ",'" & _
                            vResultado & "','" & _
                            txtRejeicao & "','" & _
                            cUserName & "'"
            Set rs = rdocn.OpenResultset(SQL)
            rs.Close
            
            ' Demanda: 12967997 '
            ' Quando o pagto for cancelado, eh para excluir da tabela sinistro_solicita_saldo_tb para nao pedir mais saldo '
            ' COMECO - Anderson Teixeira - Confitec - 29/02/2012 '
            
            'vProduto_ID = Obtem_Produto_ID(flexPgtoSinistro.TextMatrix(i, 0), flexPgtoSinistro.TextMatrix(i, 1), flexPgtoSinistro.TextMatrix(i, 2), flexPgtoSinistro.TextMatrix(i, 3), flexPgtoSinistro.TextMatrix(i, 4))
                
            'If vResultado = "r" And (vProduto_ID = "1198" Or vProduto_ID = "1183") Then
            
            '---------------------------------------------------------------------------------------------------------
            ' Demanda: 18037006
            ' Realizar a exclus�o do saldo devedor quando pagamento for rejeitado ou cancelado.
            ' Eduardo.Maior - Confitec Sistemas - 30/05/2014
            '---------------------------------------------------------------------------------------------------------
            If (vResultado = "r" Or vResultado = "c") Then
            
                SQL = "SELECT sinistro_solicita_saldo " & _
                    "  FROM produto_tb WITH(NOLOCK) " & _
                    " WHERE produto_id = " & Sin.produto_id & _
                    "   AND sinistro_solicita_saldo = 'S' "
                    
                Set rs = rdocn.OpenResultset(SQL)
                If Not rs.EOF Then
                    
                    SQL = "SELECT id_beneficiario_bb " & _
                    "  FROM sinistro_benef_tb WITH(NOLOCK) " & _
                    " WHERE sinistro_id = " & Sin.sinistro_id & _
                    "   AND beneficiario_id = " & Pgto.Beneficiario_id
                    
                    Set rs = rdocn.OpenResultset(SQL)
                    
                    If Not rs.EOF Then
                        If LCase(rs!id_beneficiario_bb) = "s" Then
                           If Not (Lancar_Novo_pagamento(Sin)) Then
                              rs.Close
                              SQL = "EXEC SEGS10119_SPD " & Sin.sinistro_id & ", NULL"
                              Set rs = rdocn.OpenResultset(SQL)
                           End If
                        End If
                    End If
                End If
                rs.Close
            End If
            
            ' Demanda: 12967997 - FIM '
            
            'Alan Neves - MP - 21/03/2013 - Inicio - Atualiza a temporaria devido a execu��o da pgto_sinistro_alt_sit_spu
            SQL = "set nocount on" & vbNewLine
            SQL = SQL & " update #sinistro_tb " & vbNewLine
            SQL = SQL & "    set situacao = sinistro_tb.situacao" & vbNewLine
            SQL = SQL & "   from #sinistro_tb #sinistro_tb " & vbNewLine
            SQL = SQL & "   join seguros_db..sinistro_tb sinistro_tb with(nolock)                             " & vbNewLine
            SQL = SQL & "     on sinistro_tb.sinistro_id = #sinistro_tb.sinistro_id                           " & vbNewLine
            SQL = SQL & "  where #sinistro_tb.sinistro_id             = " & Sin.sinistro_id & vbNewLine
            'rdocn.OpenResultset (SQL)   ' Alan Neves - C_ANEVES - 20/06/2013
            rdocn1.OpenResultset (SQL)  ' Alan Neves - C_ANEVES - 20/06/2013
                
            frmAcertoCon.SelecionaDadosPgtoSinistro
            'Alan Neves - MP - 21/03/2013 - Fim
            
            'Alterado por Cleber da Stefanini - 21/11/2005 - Flow 112708
            If frameAutorizacao.Visible Then
            
                'Alan Neves - MP - 12/03/2013 - Inicio
                'vsql = "select  count(*)"
                vSql = "select  count(1)"
                'Alan Neves - MP - 12/03/2013 - Fim
                
                'Alan Neves - MP - 19/03/2013 - Inicio - Leitura na temp
                'vsql = vsql & " from sinistro_estimativa_tb with(nolock) "
                vSql = vSql & " from #sinistro_estimativa_tb sinistro_estimativa_tb "
                'Alan Neves - MP - 19/03/2013 - Fim
                
                'vSql = vSql & " where sinistro_id =  " & vSinistro
                'Maur�cio (Stefanini), em 14/11/2005
                vSql = vSql & " where sinistro_id =  " & flexPgtoSinistro.TextMatrix(i, 0)
                vSql = vSql & " and seq_estimativa = (select max(seq_estimativa)"
                
                'Alan Neves - MP - 19/03/2013 - Inicio - Leitura na temp
                'vsql = vsql & " from    sinistro_estimativa_tb se2 with(nolock) "
                vSql = vSql & " from    #sinistro_estimativa_tb se2  "
                'Alan Neves - MP - 19/03/2013 - Fim
                
                vSql = vSql & " where   se2.sinistro_id = sinistro_estimativa_tb.sinistro_id)"
                
                Set rsAuxi = rdocn1.OpenResultset(vSql)
                
                If iQtdItemEstimativa(i) <> rsAuxi(0) Then
                        MsgBox "Sequencial de estimativas incorreto !" & vbCrLf _
                                & "Por favor, abra um FLOWBR relatando este problema com o print da tela!" & vbCrLf _
                                & SQL, vbExclamation
                End If
            Else
                'Luciana - 29/09/2005
                'Alan Neves - MP - 12/03/2013 - Inicio
                'vsql = "select  count(*)"
                vSql = "select  count(1)"
                'Alan Neves - MP - 12/03/2013 - Fim
                
                'Alan Neves - MP - 19/03/2013 - Inicio - Leitura na temp
                'vsql = vsql & " from sinistro_estimativa_tb with(nolock) "
                vSql = vSql & " from #sinistro_estimativa_tb sinistro_estimativa_tb"
                'Alan Neves - MP - 19/03/2013 - Fim
                
                vSql = vSql & " where sinistro_id =  " & vSinistro
                vSql = vSql & " and seq_estimativa = (select max(seq_estimativa)"
                
                'Alan Neves - MP - 19/03/2013 - Inicio - Leitura na temp
                'vsql = vsql & " from    sinistro_estimativa_tb se2 with(nolock) "
                vSql = vSql & " from    #sinistro_estimativa_tb se2 "
                'Alan Neves - MP - 19/03/2013 - Fim
                
                vSql = vSql & " where   se2.sinistro_id = sinistro_estimativa_tb.sinistro_id)"
                
                'msalema - 20/06/2013
                'Set rsAuxi = rdocn.OpenResultset(vSql)
                Set rsAuxi = rdocn1.OpenResultset(vSql)
            
                If countQtdItemEstimativa <> rsAuxi(0) Then
                        MsgBox "Sequencial de estimativas incorreto !" & vbCrLf _
                                & "Por favor, abra um FLOWBR relatando este problema com o print da tela!" & vbCrLf _
                                & SQL, vbExclamation
                End If
            End If
            rsAuxi.Close
'           Alan Neves - C_ANEVES - 15/05/2013 - Fim
'            If vResultado = "c" Then
'                ' Manda e-mail
'                MensagemAcerto = "Sinistro: " & Sin.sinistro_id & vbNewLine
'                MensagemAcerto = MensagemAcerto & "Ap�lice: " & Sin.Apolice_id & vbNewLine
'                MensagemAcerto = MensagemAcerto & "Ramo: " & Sin.NomeRamo & vbNewLine
'                MensagemAcerto = MensagemAcerto & "Estimativa: " & Estimativa_texto(Pgto.Item_val_estimativa) & vbNewLine
'                MensagemAcerto = MensagemAcerto & "Benefici�rio: " & Pgto.Beneficiario_Nome & vbNewLine
'                MensagemAcerto = MensagemAcerto & "Data do cancelamento: " & Format(Data_Sistema, "dd/mm/yyyy") & vbNewLine
'                MensagemAcerto = MensagemAcerto & "Cancelado por: " & cUserName & vbNewLine & vbNewLine
'                MensagemAcerto = MensagemAcerto & "Motivo: " & txtRejeicao
'                Destinatario = Pgto.Usuario
'                If MAPISess.SessionID = 0 Then
'                    Logon_Click
'                End If
'                If SendMsg("c") = False Then
'                    rdocn.RollbackTrans
'                    Exit Sub
'                End If
'            End If
'            Alan Neves - C_ANEVES - 15/05/2013 - Fim
        End If
        
        rdocn.CommitTrans
        
        'Enviar email
        'Alan Neves - C_ANEVES - 15/05/2013 - Inicio
        If frameAutorizacao.Visible Then ' Altera todos da OP
            
            For i = 1 To flexPgtoSinistro.Rows - 1
                If vResultado = "c" Then
                    ' Manda e-mail
                        MensagemAcerto = "Autoriza��o N�: " & txtNumAutorizacao.Text & vbNewLine
                        MensagemAcerto = MensagemAcerto & "Sinistro: " & flexPgtoSinistro.TextMatrix(i, 0) & vbNewLine
                        MensagemAcerto = MensagemAcerto & "Ap�lice: " & flexPgtoSinistro.TextMatrix(i, 1) & vbNewLine
                        MensagemAcerto = MensagemAcerto & "Ramo: " & flexPgtoSinistro.TextMatrix(i, 4) & vbNewLine
                        MensagemAcerto = MensagemAcerto & "Estimativa: " & flexPgtoSinistro.TextMatrix(i, 16) & vbNewLine
                        MensagemAcerto = MensagemAcerto & "Benefici�rio: " & txtLider.Text & vbNewLine
                        MensagemAcerto = MensagemAcerto & "Data do cancelamento: " & Format(Data_Sistema, "dd/mm/yyyy") & vbNewLine
                        MensagemAcerto = MensagemAcerto & "Cancelado por: " & cUserName & vbNewLine & vbNewLine
                        MensagemAcerto = MensagemAcerto & "Motivo: " & txtRejeicao
                        Destinatario = flexPgtoSinistro.TextMatrix(i, 21)
                        
                    If MAPISess.SessionID = 0 Then
                        Logon_Click
                    End If
                    
                    If SendMsg("c") = False Then
                        MsgBox "Email da lista de autoriza��o n�o enviado!", vbInformation, Caption
                    End If
                End If
            Next i
        Else
            If vResultado = "c" Then
                ' Manda e-mail
                MensagemAcerto = "Sinistro: " & Sin.sinistro_id & vbNewLine
                MensagemAcerto = MensagemAcerto & "Ap�lice: " & Sin.Apolice_id & vbNewLine
                MensagemAcerto = MensagemAcerto & "Ramo: " & Sin.NomeRamo & vbNewLine
                MensagemAcerto = MensagemAcerto & "Estimativa: " & Estimativa_texto(Pgto.Item_val_estimativa) & vbNewLine
                MensagemAcerto = MensagemAcerto & "Benefici�rio: " & Pgto.Beneficiario_Nome & vbNewLine
                MensagemAcerto = MensagemAcerto & "Data do cancelamento: " & Format(Data_Sistema, "dd/mm/yyyy") & vbNewLine
                MensagemAcerto = MensagemAcerto & "Cancelado por: " & cUserName & vbNewLine & vbNewLine
                MensagemAcerto = MensagemAcerto & "Motivo: " & txtRejeicao
                Destinatario = Pgto.Usuario
                If MAPISess.SessionID = 0 Then
                    Logon_Click
                End If
                
                If SendMsg("c") = False Then
                     MsgBox "Email n�o enviado!", vbInformation, Caption
                End If
            End If
        End If
'        Alan Neves - C_ANEVES - 15/05/2013 - Fim
        
        Screen.MousePointer = 0
        If Not Exibiu_Msg Then
            MsgBox "Altera��o de Situa��o do Pagamento Executada Com Sucesso!", vbInformation, Caption
        End If
        If frameAutorizacao.Visible Then
            Monta_Grid_Autorizacao
            ''pcarvalho - 17/08/2004
            Call MontarGridAutorizacao
            If flexPgtoSinistro.Rows > 1 Then
                flexPgtoSinistro.Row = 1
                ''pcarvalho - 17/08/2004
                grdPagtosAprovados.Rows = 1
                optAprovado.Value = False
                optRejeitado.Value = False
                frameRejeitado.Visible = False
                frameCancelar.Visible = False
                frameCancReapresentar.Visible = False
                frameResultado.Visible = False
                FrameVoucher.Visible = False
                
                Carregando_Cosseguro_Aceito = True
                flexPgtoSinistro_Click
                Carregando_Cosseguro_Aceito = False
            End If
        Else
            Monta_Pgto
            ''pcarvalho - 17/08/2004 - Grid de Pagtos Aprovados
            Call MontarGrdPgtosAprovados
            If Pagamentos.Count = 0 Then
                Unload Me
            Else
                optAprovado.Value = False
                optRejeitado.Value = False
                frameRejeitado.Visible = False
                frameCancelar.Visible = False
                frameCancReapresentar.Visible = False
                frameResultado.Visible = False
                FrameVoucher.Visible = False
                BtnCancelar.SetFocus
            End If
        End If
    End If
    flagOK = True
    If Lancou_Pagamento Then
       Dim dRetorno As Double
       dRetorno = Shell(gsPastaLocalSegbr + "SEGP1293.EXE " + _
       vTpRamo + ".2." + vChamadaSegp + ".1.-.E.S " + _
       Monta_Parametro_Atual("SEGP1293"))
       Unload Me
    End If
    Exit Sub
    Resume
Trata_erro:
    Screen.MousePointer = 0
    TrataErroGeral "Aprova��o de Pagamentos"
    flagOK = False
    Unload Me
    
End Sub

Private Function Estimativa_texto(Item As String) As String

    Dim texto As String
    
    Select Case Item
    Case "1": texto = "INDENIZA��O"
    Case "2": texto = "HONOR�RIOS"
    Case "3": texto = "DESPESAS"
    Case "4": texto = "RESSARCIMENTO"
    Case "5": texto = "DESP. RESSARCIMENTO"
    Case "6": texto = "SALVADOS"
    Case "7": texto = "DESP. SALVADOS"
    End Select

    Estimativa_texto = texto

End Function

Public Sub BtnCancelar_Click()
    
    Esta_Cancelando = True
    Unload Me

End Sub

Private Sub BtnOk_Click()
    
    flagOK = False
    BtnAplicar_Click
    If flagOK Then
        Unload Me
    End If

End Sub

Private Sub flexPgtoSinistro_Click()

    Dim rs As rdoResultset, SQL As String
    Dim Pgto As Pgto_Sinistro
    Dim sinsel As Sinist
    
    If flexPgtoSinistro.Rows > 1 Then
        txtRejeicao = ""
        txtRejeicao.Locked = False
        vSelecionou = True
        
        Set sinsel = SinistroEmUso(1)
        
        If frameAutorizacao.Visible Or Carregando_Cosseguro_Aceito Then
            Monta_pgto_Cosseguro
            Set Pgto = Pagamentos.Item(1)
        Else
            Set Pgto = Pagamentos.Item(flexPgtoSinistro.Row)
        End If
        
        If Pgto.Usuario = "MIGRACAO" Then
            MsgBox "N�o � poss�vel alterar a situa��o de um pagamento migrado."
            frameResultado.Visible = False
            frameCancelar.Visible = False
            frameRejeitado.Visible = False
            FrameVoucher.Visible = False
            frameCancReapresentar.Visible = False
            Exit Sub
        End If
        
        If Not Carregando_Cosseguro_Aceito Then
            txtDetalhe.Text = Pgto.Recibo
        End If
        
        If voper <> "I" And voper <> "IS" Then
            frameResultado.Visible = True
            frameResultado.Enabled = False
            frameCancelar.Visible = False
            frameRejeitado.Visible = False
            FrameVoucher.Visible = False
            frameCancReapresentar.Visible = False
            optAprovado.Visible = True
            optAprovado.Value = True
            optRejeitado.Visible = False
'            If Pgto.Item_val_estimativa = 4 Or Pgto.Item_val_estimativa = 6 Then
'                optAprovado.Visible = True
'                optAprovado.Caption = "Recebido"
'                optAprovado.Value = True
'                optRejeitado.Visible = False
'            End If
            Select Case Pgto.Situacao_OP
                Case "AGUARD. APROVA��O"
                    optAprovado.Caption = "Aguardando Aprova��o"
                    If voper <> "C" Then
                        frameEstornar.Visible = False
                        optEstornar.Value = False
                        frameCancelar.Visible = True
                        optS�Cancela.Value = False
                    End If
                Case "REJEITADO"
                    optAprovado.Visible = False
                    optRejeitado.Visible = True
                    optRejeitado.Value = True
                Case "APROVADO"
                    optAprovado.Caption = "Aprovado"
                    'If Pgto.Item_val_estimativa <> 4 And _
                        Pgto.Item_val_estimativa <> 6 Then
                        If Trim(Pgto.dt_contabilizacao) <> "" Then
                            optAprovado.Caption = "Aprovado e Contabilizado"
                            If Trim(Pgto.Voucher) <> "" Then
                                FrameVoucher.Visible = True
                                TxtVoucher.Text = Pgto.Voucher
                            Else
                                FrameVoucher.Visible = False
                                TxtVoucher.Text = ""
                            End If
                        Else
                            If Trim(Pgto.Voucher) <> "" Then
                                frameCancelar.Visible = False
                                optS�Cancela.Value = False
                                FrameVoucher.Visible = True
                                FrameVoucher.Caption = "Voucher"
                                TxtVoucher.Text = Pgto.Voucher
                            Else
                                'Alan Neves - MP - 19/03/2013 - Inicio - Alterado para acessar a temporaria
                                SQL = "SELECT acerto_id FROM #pgto_sinistro_tb pgto_sinistro_tb WITH(NOLOCK) "
                                'SQL = "SELECT acerto_id FROM pgto_sinistro_tb WITH(NOLOCK) "
                                'Alan Neves - MP - 19/03/2013 - Fim
                                SQL = SQL & " WHERE sinistro_id             = " & sinsel.sinistro_id
                                SQL = SQL & " AND   apolice_id              = " & sinsel.Apolice_id
                                SQL = SQL & " AND   sucursal_seguradora_id  = " & sinsel.Sucursal_id
                                SQL = SQL & " AND   seguradora_cod_susep    = " & sinsel.Seguradora_id
                                SQL = SQL & " AND   ramo_id                 = " & sinsel.ramo_id
                                SQL = SQL & " AND   seq_estimativa          = " & Pgto.Seq_Estimativa
                                SQL = SQL & " AND   item_val_estimativa     = " & Pgto.Item_val_estimativa
                                SQL = SQL & " AND   beneficiario_id         = " & Pgto.Beneficiario_id
                                SQL = SQL & " AND   num_parcela             = " & Pgto.Num_Parcela
                                SQL = SQL & " AND   seq_pgto                = " & Pgto.Seq_Pgto
                                Set rs = rdocn1.OpenResultset(SQL)
                                If Not IsNull(rs!acerto_id) Then
                                   frameCancelar.Visible = False
                                   optS�Cancela.Value = False
                                   FrameVoucher.Visible = True
                                   FrameVoucher.Caption = "Acerto"
                                   TxtVoucher.Text = rs!acerto_id
                                Else
                                   FrameVoucher.Visible = False
                                   TxtVoucher.Text = ""
                                   frameCancelar.Visible = True
                                   optS�Cancela.Value = False
                                End If
                                rs.Close
                            End If
                        
                        End If
                    'End If
                Case "CANCELADO"
                    optAprovado.Caption = "Cancelado"
            End Select
        
        End If
        
        If voper = "A" Or voper = "R" Then
            txtRejeicao.Locked = True
            Select Case Pgto.Situacao_OP
                Case "REJEITADO"
                    frameCancReapresentar.Visible = True
                    optCancelado.Value = False
                    optReapresenta.Value = False
                    frameRejeitado.Visible = True
                    vRejeicao = Pgto.Motivo_Rejeicao
                    txtRejeicao.Text = vRejeicao
                Case "APROVADO"
                    If Trim(Pgto.dt_contabilizacao) <> "" Then
                        If Trim(Pgto.Voucher) = "" Then
                            frameEstornar.Visible = False
                            frameCancelar.Visible = True
                            optEstornar.Value = False
                            FrameVoucher.Visible = False
                            TxtVoucher.Text = ""
                        Else
                            FrameVoucher.Visible = True
                            TxtVoucher.Text = Pgto.Voucher
                        End If
                    End If
            End Select
        
        ElseIf voper = "C" Then
            txtRejeicao.Locked = True
            frameRejeitado.Visible = False
            txtRejeicao.Visible = False
            Select Case Pgto.Situacao_OP
                Case "REJEITADO"
                    frameRejeitado.Visible = True
                    txtRejeicao.Text = Pgto.Motivo_Rejeicao
                    txtRejeicao.Visible = True
                    frameRejeitado.Visible = True
                Case "CANCELADO"
                    optAprovado.Caption = "Cancelado"
                    If Trim(Pgto.Motivo_Rejeicao) <> "" Then
                        frameRejeitado.Visible = True
                        txtRejeicao.Visible = True
                        txtRejeicao = Pgto.Motivo_Rejeicao
                    End If

                Case "ESTORNADO"
                    optAprovado.Caption = "Estornado"

            End Select
            frameCancelar.Visible = False
        End If
    End If

    If frameAutorizacao.Visible Then
        Desmonta_Pagto
    Else
    '+-----------------------------------------------------------
    '| 19406709 - Melhoria nos limites de sinistro e emissao
    '| RN05 - petrauskas - 03/2017
      If produto_regra_especial Then
        verifica_pgto_X_IS
      End If
    '| 19406709 - Melhoria nos limites de sinistro e emissao
    '+-----------------------------------------------------------
    
    End If

End Sub

Private Sub Form_Activate()
    
    vSelecionou = False
    If voper = "C" Then
        BtnAplicar.Visible = False
        BtnOk.Visible = False
        BtnCancelar.Caption = "&Sair"
    End If
    
End Sub

Private Sub Form_Load()

    '****************************************************************
    '*  Projeto:    495123 - Altera��o de al�adas t�cnicos sinistro *
    '*  Sistena:    SEGBR - Sistema de Seguros                      *
    '*  Data:       03/09/2008                                      *
    '*  Autor:      M�rcio Ossamu Yoshimura                         *
    '****************************************************************
        
    '********************************************************
    '*  Desenvolvido por:   GPTI                            *
    '*  Programado por:     Afonso Dutra Nogueira Filho     *
    '*  Data:               23/09/2008                      *
    '********************************************************


    Dim Sin As Sinist, linha As Integer
    Dim i As Integer

    Dim vSql As String

    CentraFrm frmAcerto
    frame_justifictiva.Left = frameRejeitado.Left: frame_justifictiva.Top = frameRejeitado.Top
    
    'Caption do Form
    Select Case gsParamAplicacao
        Case "I":  Caption = "SEG00247 - Aprovar Pagamento - " & Ambiente
        Case "R":  Caption = "SEG00247 - Reapresentar Pagamento - " & Ambiente
        Case "A":  Caption = "SEG00247 - Alterar Situa��o Pagamento/Recebimento - " & Ambiente
        Case "C":  Caption = "SEG00247 - Consultar Pagamentos/Recebimentos - " & Ambiente
        Case "IC": Caption = "SEG00247 - Aprovar Pagamento Co-seguro - " & Ambiente
        Case "RC": Caption = "SEG00247 - Reapresentar Pagamento Co-seguro - " & Ambiente
        Case "AC": Caption = "SEG00247 - Alterar Situa��o Pagamento/Recebimento Co-seguro - " & Ambiente
        Case "CC": Caption = "SEG00247 - Consultar Pagamentos/Recebimentos Co-seguro - " & Ambiente
    End Select
    Me.Caption = Caption
    
    
    'Alan Neves - MP - 12/03/2013 - Inicio - Recupera do form de Consulta
    Tecnico_atual = frmAcertoCon.cod_tecnico
    'Alan Neves - Fim
    
    '----------------
    ' Alan Neves - 05/02/2013 - Melhoria de performance
    'Obt�m a paridade da moeda nesta data
    achou_paridade = False
    vval_paridade_moeda = ""
    'Alan Neves - MP - Fim - 07/02/2013
   
    '----------------------------------------
    'Luciana - 03/10/2003
    '----------------------------------------
    'If frmAcertoCon.flexSelecao.Visible Then
    If bFlexSelecao Then
    '----------------------------------------
        Linha_Selecionada = frmAcertoCon.flexSelecao.Row
        framePagamentos.Caption = " Selecionar um : "
        frameSelecao.Visible = True
        frameAutorizacao.Visible = False
        Set Sin = SinistroEmUso.Item(1)
        'Monta Resumo
        txtNumeroSinistro = Sin.sinistro_id
        txtNumeroApolice = Sin.Apolice_id
        txtNumeroProposta = Sin.Proposta_id
        txtNomeSegurado = Sin.NomeCliente
        If Verifica_Cosseguro = True Then
            lblNossaParteCosseguro.Visible = True
        End If
        
'+-----------------------------------------------------------
'| 19406709 - Melhoria nos limites de sinistro e emissao
'| RN05 - petrauskas - 03/2017
        produto_regra_especial = False
        '06/06/2018 - Schoralick (ntendencia) - 00421597-seguro-faturamento-pecuario  [add produto 1240 (sprint 6)]
        If InStr(",8,155,156,227,230,228,229,300,650,680,701,1152,1201,1204,1210,1226,1227,1240,", "," & Sin.produto_id & ",") > 0 Then
          produto_regra_especial = True
          val_is = obter_valor_is(Sin.Proposta_id, Sin.produto_id)
        End If
'| 19406709 - Melhoria nos limites de sinistro e emissao
'+-----------------------------------------------------------
        
        'Seleciona registros
        If voper = "I" Or voper = "IS" Then
            frameResultado.Visible = True
            frameAplicar.Visible = True
            optAprovado.Value = False
            optRejeitado.Value = False
            optSelecionado.Value = False
            optTodos.Value = False
            
            If vopercoseguro = "C" Then
                If lblNossaParteCosseguro.Visible = True Then
'Ralves - 10/06/2010 - Demanda 3798421 - Dados da conta para pagamento / Inserindo dados bancarios
                                
                                'Fabricio Brandi - Nova Consultoria : 29/09/2011
                                'Adicionado o cpf ao numero de colunas
                    flexPgtoSinistro.FormatString = "Dt. Pagamento|Situa��o Pagamento|Item                                     |Cobertura                                 |Verba          |Valor Pagamento|Valor C. Monet�ria|Forma Pagamento|Benefici�rio                                                                                                        |CPF                 |Dt. Solicita��o|Usu�rio Solicitante    |Voucher  |N�mero NF|S�rie NF|N�mero da Carta|Valor Pgto Cosseguro Nossa Parte| Banco   | Agencia   | Agencia DV | Conta Corrente | Conta Corrente DV| Conta Poupanca | Conta Poupanca DV"
                    flexPgtoSinistro.Cols = 24
                Else
                    flexPgtoSinistro.FormatString = "Dt. Pagamento|Situa��o Pagamento|Item                                     |Cobertura                                 |Verba          |Valor Pagamento|Valor C. Monet�ria|Forma Pagamento|Benefici�rio                                                                                                        |CPF                 |Dt. Solicita��o|Usu�rio Solicitante    |Voucher  |N�mero NF|S�rie NF|N�mero da Carta| Banco   | Agencia   | Agencia DV | Conta Corrente | Conta Corrente DV| Conta Poupanca | Conta Poupanca DV"
                    flexPgtoSinistro.Cols = 23
                End If
            Else
                If lblNossaParteCosseguro.Visible = True Then
                    flexPgtoSinistro.FormatString = "Dt. Pagamento|Situa��o Pagamento|Item                                     |Cobertura                                 |Verba          |Valor Pagamento|Valor C. Monet�ria|Forma Pagamento|Benefici�rio                                                                                                        |CPF                 |Dt. Solicita��o|Usu�rio Solicitante    |Voucher     |N�mero NF|S�rie NF|Valor Pgto Cosseguro Nossa Parte| Banco   | Agencia   | Agencia DV | Conta Corrente | Conta Corrente DV| Conta Poupanca | Conta Poupanca DV"
                    flexPgtoSinistro.Cols = 23
                Else
                    flexPgtoSinistro.FormatString = "Dt. Pagamento|Situa��o Pagamento|Item                                     |Cobertura                                 |Verba          |Valor Pagamento|Valor C. Monet�ria|Forma Pagamento|Benefici�rio                                                                                                        |CPF                 |Dt. Solicita��o|Usu�rio Solicitante    |Voucher     |N�mero NF|S�rie NF| Banco   | Agencia   | Agencia DV | Conta Corrente | Conta Corrente DV| Conta Poupanca | Conta Poupanca DV"
                    flexPgtoSinistro.Cols = 22
                End If
            End If
        Else
            If vopercoseguro = "C" Then
                If lblNossaParteCosseguro.Visible = True Then
                    flexPgtoSinistro.FormatString = "Dt. Pagamento|Situa��o Pagamento|Item                                     |Cobertura                                 |Verba          |Valor Pagamento|Valor C. Monet�ria|Forma Pagamento|Benefici�rio                                                                                                        |CPF                 |Dt. Avalia��o|Usu�rio                |Usu�rio Avaliador|Voucher  |N�mero NF|S�rie NF|N�mero da Carta|Valor Pgto Cosseguro Nossa Parte|N� Autoriza��o| Banco   | Agencia   | Agencia DV | Conta Corrente | Conta Corrente DV| Conta Poupanca | Conta Poupanca DV"
                    flexPgtoSinistro.Cols = 25
                Else
                    flexPgtoSinistro.FormatString = "Dt. Pagamento|Situa��o Pagamento|Item                                     |Cobertura                                 |Verba          |Valor Pagamento|Valor C. Monet�ria|Forma Pagamento|Benefici�rio                                                                                                        |CPF                 |Dt. Avalia��o|Usu�rio                |Usu�rio Avaliador|Voucher  |N�mero NF|S�rie NF|N�mero da Carta|N� Autoriza��o| Banco   | Agencia   | Agencia DV | Conta Corrente | Conta Corrente DV| Conta Poupanca | Conta Poupanca DV"
                    flexPgtoSinistro.Cols = 25
                End If
            Else
                If lblNossaParteCosseguro.Visible = True Then
                    flexPgtoSinistro.FormatString = "Dt. Pagamento|Situa��o Pagamento|Item                                     |Cobertura                                 |Verba          |Valor Pagamento|Valor C. Monet�ria|Forma Pagamento|Benefici�rio                                                                                                        |CPF                 |Dt. Avalia��o|Usu�rio                |Usu�rio Avaliador|Voucher     |N�mero NF|S�rie NF|Valor Pgto Cosseguro Nossa Parte|N� Autoriza��o| Banco   | Agencia   | Agencia DV | Conta Corrente | Conta Corrente DV| Conta Poupanca | Conta Poupanca DV"
                    flexPgtoSinistro.Cols = 24
                Else
                    flexPgtoSinistro.FormatString = "Dt. Pagamento|Situa��o Pagamento|Item                                     |Cobertura                                 |Verba          |Valor Pagamento|Valor C. Monet�ria|Forma Pagamento|Benefici�rio                                                                                                        |CPF                 |Dt. Avalia��o|Usu�rio                |Usu�rio Avaliador|Voucher|N�mero NF|S�rie NF|N� Autoriza��o| Banco   | Agencia   | Agencia DV | Conta Corrente | Conta Corrente DV| Conta Poupanca | Conta Poupanca DV"
                    flexPgtoSinistro.Cols = 24
                End If
            End If
            frameResultado.Caption = "Situa��o do Pagamento"
        End If

'Fabricio Brandi - Nova Consultoria : 29/09/2011
'Adicionado o cpf ao numero de colunas
'------------------------ FIM ---------------------------
        ''pcarvalho - 17/08/2004
        grdPagtosAprovados.FormatString = flexPgtoSinistro.FormatString
        grdPagtosAprovados.Cols = flexPgtoSinistro.Cols
    
        Monta_Pgto
        ''pcarvalho - 17/08/2004 - Grid de Pagtos Aprovados
        Call MontarGrdPgtosAprovados
    
    Else ' flexAutorizacao
        
        framePagamentos.Caption = " Pagamentos : "
        frameSelecao.Visible = False
        frameAutorizacao.Visible = True
        linha = frmAcertoCon.flexAutorizacao.Row
        txtNumAutorizacao.Text = frmAcertoCon.flexAutorizacao.TextMatrix(linha, 0)
        txtLider.Text = frmAcertoCon.flexAutorizacao.TextMatrix(linha, 1)
        txtValorTotal.Text = frmAcertoCon.flexAutorizacao.TextMatrix(linha, 2)
        txtMoeda.Text = frmAcertoCon.flexAutorizacao.TextMatrix(linha, 3)
        txtMoeda.Tag = frmAcertoCon.flexAutorizacao.TextMatrix(linha, 5)
        txtFormaPgto.Text = frmAcertoCon.flexAutorizacao.TextMatrix(linha, 4)
        
        If voper = "I" Or voper = "IS" Then
            frameResultado.Visible = True
            frameAplicar.Visible = True
            optAprovado.Value = False
            optRejeitado.Value = False
            optSelecionado.Value = False
            optSelecionado.Enabled = False
            optTodos.Value = True
        Else
            frameResultado.Caption = "Situa��o do Pagamento"
        End If
        
        flexPgtoSinistro.FormatString = "N� Sinistro       |Ap�lice |Suc|Seg|R|Seq_est|Seq_pgto|num_parcela|benef_id|situacao_op|motivo_rejeicao|dt_contab|recibo|sit_sinistro|Dt. Pagamento|Situa��o Pagamento|Item                           |Cobertura                                 |Verba        |Valor Pagamento   |Dt. Avalia��o|Usu�rio                |Usu�rio Avaliador|Voucher     "
        flexPgtoSinistro.Cols = 24

        ''pcarvalho - 17/08/2004
        grdPagtosAprovados.FormatString = flexPgtoSinistro.FormatString
        grdPagtosAprovados.Cols = flexPgtoSinistro.Cols

        For i = 1 To 13
            flexPgtoSinistro.ColWidth(i) = 0
            ''pcarvalho - 17/08/2004
            grdPagtosAprovados.ColWidth(i) = 0
        Next i
        Monta_Grid_Autorizacao
        Call MontarGridAutorizacao
        
        'Alan Neves - Erro quando n�o possui registro - 21/03/2013 - Inclusao do IF
        If flexPgtoSinistro.Row > 0 Then
        flexPgtoSinistro.Row = 1
        End If
        ''pcarvalho - 17/08/2004
        'grdPagtosAprovados.Row = 1
        
        Carregando_Cosseguro_Aceito = True
        flexPgtoSinistro_Click
        Carregando_Cosseguro_Aceito = False

    End If
    
    If voper = "IS" Then
      lngontop = SetWindowPos(Me.hwnd, HWND_TOPMOST, 0, 0, 0, 0, (SWP_NOMOVE Or SWP_NOSIZE))
    End If
    
    
    'Alan Neves - 11/03/2013 - MP - Criacao temporaria com objetivo de evitar consulta excessiva na tabela fisica
    vSql = ""
    '' GENESCO SILVA - FLOW 18313521 - MIGRA��O SQL SERVER
    vSql = vSql & " set nocount on" & vbNewLine
    'vSql = vSql & " if isnull(OBJECT_ID('tempdb..#limite_aprovacao_pgto_tb'),0) >0" & vbNewLine
    vSql = vSql & " if OBJECT_ID('tempdb..#limite_aprovacao_pgto_tb') IS NOT NULL " & vbNewLine
    vSql = vSql & " begin" & vbNewLine
    vSql = vSql & "     drop table #limite_aprovacao_pgto_tb" & vbNewLine
    vSql = vSql & " end" & vbNewLine
    
    vSql = vSql & " Create Table #limite_aprovacao_pgto_tb                                              " & vbNewLine
    vSql = vSql & " ( tecnico_id  smallint,                                                             " & vbNewLine
    vSql = vSql & " item_val_estimativa   tinyint ,                                                     " & vbNewLine
    vSql = vSql & " ramo_id               int     ,                                                     " & vbNewLine
    vSql = vSql & " produto_id            int     ,                                                     " & vbNewLine
    vSql = vSql & " val_limite_aprovacao  numeric(15,2),                                                " & vbNewLine
    vSql = vSql & " ilimitado             varchar(1),                                                   " & vbNewLine
    vSql = vSql & " alcada_liquida        varchar(1) )                                                   " & vbNewLine
    vSql = vSql & " Create Index #IDX_limite_aprovacao_pgto_tb on #limite_aprovacao_pgto_tb (tecnico_id, item_val_estimativa, ramo_id, produto_id) " & vbNewLine
   
    If bFlexSelecao Then
        vSql = vSql & " insert into #limite_aprovacao_pgto_tb (tecnico_id, item_val_estimativa, ramo_id, produto_id, val_limite_aprovacao, ilimitado, alcada_liquida)" & vbNewLine
        vSql = vSql & " select tecnico_id, item_val_estimativa, ramo_id, produto_id, val_limite_aprovacao, ilimitado, alcada_liquida" & vbNewLine
        vSql = vSql & "   from seguros_db..limite_aprovacao_pgto_tb with(nolock)" & vbNewLine
        vSql = vSql & "  Where ramo_id     = " & Sin.ramo_id & vbNewLine
        vSql = vSql & "    and produto_id  = " & Sin.produto_id & vbNewLine
        vSql = vSql & "    and dt_fim_vigencia IS NULL" & vbNewLine
            'Alan Neves - 12/03/2013 - MP - Criacao temporaria com objetivo de evitar consulta excessiva na tabela fisica
    Else
        'Alan Neves - C_ANEVES - 15/07/2013 - Inicio
        vSql = vSql & " insert into #limite_aprovacao_pgto_tb"
        vSql = vSql & " (tecnico_id, item_val_estimativa, limite_aprovacao_pgto_tb.ramo_id, limite_aprovacao_pgto_tb.produto_id, val_limite_aprovacao, ilimitado, alcada_liquida)" & vbNewLine
        vSql = vSql & " select distinct limite_aprovacao_pgto_tb.tecnico_id, limite_aprovacao_pgto_tb.item_val_estimativa," & vbNewLine
        vSql = vSql & " limite_aprovacao_pgto_tb.ramo_id, limite_aprovacao_pgto_tb.produto_id, limite_aprovacao_pgto_tb.val_limite_aprovacao," & vbNewLine
        vSql = vSql & " limite_aprovacao_pgto_tb.Ilimitado , limite_aprovacao_pgto_tb.Alcada_liquida" & vbNewLine
        vSql = vSql & " from seguros_db..limite_aprovacao_pgto_tb limite_aprovacao_pgto_tb with(nolock)" & vbNewLine
        vSql = vSql & " join #sinistro_tb sinistro_tb" & vbNewLine
        vSql = vSql & " on limite_aprovacao_pgto_tb.ramo_id    =  sinistro_tb.ramo_id" & vbNewLine
        vSql = vSql & " and limite_aprovacao_pgto_tb.produto_id =  sinistro_tb.produto_id" & vbNewLine
        vSql = vSql & " Where limite_aprovacao_pgto_tb.dt_fim_vigencia Is Null" & vbNewLine
        'Alan Neves - C_ANEVES - 15/07/2013 - Fim
    
    End If
   
    'Alan Neves - MP - Inicio - 05/02/2013
    vSql = vSql & " select val_paridade_moeda " & vbNewLine
    vSql = vSql & "   from paridade_tb WITH(NOLOCK) " & vbNewLine
    vSql = vSql & "  where dt_conversao     = '" & Format(Data_Sistema, "yyyymmdd") & "'"
    vSql = vSql & "    and origem_moeda_id  = " & DOLAR_MOEDA_ID
    vSql = vSql & "    and destino_moeda_id = " & REAL_MOEDA_ID

    Set rsMoeda = rdocn1.OpenResultset(vSql)

    If Not rsMoeda.EOF Then
        If rsMoeda(0) > 0 Then
           achou_paridade = True
           vval_paridade_moeda = rsMoeda("val_paridade_moeda")
        End If
    Else
       achou_paridade = False
    End If

    rsMoeda.Close
    'Alan Neves - MP - Fim - 07/02/2013
   
'    If (flexPgtoSinistro.Rows = 1) And _
'       (vopercoseguro = "C") And _
'       (frmAcertoCon.flexSelecao.Visible) Then
'        MsgBox "Todos os pagamentos deste sinistro pertencem a autoriza��es em grupo. Entre com o n�mero da autoriza��o."
'        BtnCancelar_Click
'    Else
        txtRejeicao = ""
'    End If
    
    
    
End Sub

Private Sub Form_Unload(Cancel As Integer)

    Dim rs As rdoResultset, SQL As String
    Dim Sin As Sinist

    If voper <> "C" Then
        If frameAutorizacao.Visible Then
            ' Desbloqueia os sinistros desse pagamento.
            For Each Sin In SinistroEmUso
                SQL = "EXEC sinistro_desmarca_spu " & _
                        Sin.sinistro_id & "," & Sin.Apolice_id & "," & _
                        Sin.Sucursal_id & "," & Sin.Seguradora_id & "," & Sin.ramo_id
                Set rs = rdocn.OpenResultset(SQL)
                rs.Close
            Next
        Else ' frameSelecao
            Set Sin = SinistroEmUso.Item(1)
            SQL = "EXEC sinistro_desmarca_spu " & _
                    Sin.sinistro_id & "," & Sin.Apolice_id & "," & _
                    Sin.Sucursal_id & "," & Sin.Seguradora_id & "," & Sin.ramo_id
            Set rs = rdocn.OpenResultset(SQL)
            rs.Close
        End If
    End If
    
    rdocn.CommitTrans 'Alan Neves - C_ANEVES - 25/06/2013
    
    Desmonta_Colecoes
    If voper <> "IS" Then
      frmAcertoCon.Show
    Else
      End
    End If
    
    If (voper = "I" Or voper = "IS") And Not Esta_Cancelando And frmAcertoCon.flexSelecao.Visible Then
        'frmAcertoCon.Desmonta_Selecao
        If flexPgtoSinistro.Rows = 1 Or optTodos.Value Then
            SinistrosSelecionados.Remove Linha_Selecionada
            If frmAcertoCon.flexSelecao.Rows = 2 Then
                frmAcertoCon.flexSelecao.Rows = 1
            Else
                frmAcertoCon.flexSelecao.RemoveItem Linha_Selecionada
            End If
        End If
    End If

End Sub

Private Sub optAprovado_Click()
'+-----------------------------------------------------------
'| 19406709 - Melhoria nos limites de sinistro e emissao
'| RN05 - petrauskas - 03/2017
  
'  txtRejeicao = ""
'  frameRejeitado.Visible = False
  
  If optAprovado.Value Then
    txtRejeicao = ""
    frameRejeitado.Visible = False
        
    If produto_regra_especial Then
      verifica_pgto_X_IS
    End If
  End If
'| 19406709 - Melhoria nos limites de sinistro e emissao
'+-----------------------------------------------------------

End Sub

Private Sub optCancelado_Click()
    
    If frameCancReapresentar.Enabled Then
        frameRejeitado.Visible = True
        frameRejeitado.Caption = "Motivo Cancelamento"
        txtRejeicao.Locked = False
    End If
        
    If frameRejeitado.Visible Then
        txtRejeicao = vRejeicao
    End If

End Sub

Private Sub optReapresenta_Click()
    
    If frameRejeitado.Visible Then
        vRejeicao = txtRejeicao
        txtRejeicao = ""
        'frameRejeitado.Caption = "Motivo Rejei��o"
        frameRejeitado.Visible = False
        txtRejeicao.Locked = True
    End If
    
End Sub

Private Sub optRejeitado_Click()
'+-----------------------------------------------------------
'| 19406709 - Melhoria nos limites de sinistro e emissao
'| RN05 - petrauskas - 03/2017
      
'  If frameResultado.Enabled Then
'      frameRejeitado.Visible = True
'  End If
'  txtRejeicao = ""

    If frameResultado.Enabled Then
        frameRejeitado.Visible = True
    End If
    txtRejeicao = ""

    frame_justifictiva.Visible = False
'| 19406709 - Melhoria nos limites de sinistro e emissao
'+-----------------------------------------------------------

End Sub

Private Sub Monta_pgto_Cosseguro()

    Dim Pgto As New Pgto_Sinistro
    Dim i As Integer

    For i = 1 To flexPgtoSinistro.Rows - 1
        Set Pgto = New Pgto_Sinistro
        With Pgto
            Select Case flexPgtoSinistro.TextMatrix(i, 16)
                Case "INDENIZA��O"
                    .Item_val_estimativa = 1
                Case "HONOR�RIOS"
                    .Item_val_estimativa = 2
                Case "DESPESAS"
                    .Item_val_estimativa = 3
                Case "RESSARCIMENTO"
                    .Item_val_estimativa = 4
                Case "DESP. RESSARCIMENTO"
                    .Item_val_estimativa = 5
                Case "SALVADOS"
                    .Item_val_estimativa = 6
                Case "DESP. SALVADOS"
                    .Item_val_estimativa = 7
            End Select
            .Recibo = flexPgtoSinistro.TextMatrix(i, 12)
            .Situacao_OP = flexPgtoSinistro.TextMatrix(i, 15)
            .Motivo_Rejeicao = flexPgtoSinistro.TextMatrix(i, 10)
            .dt_contabilizacao = flexPgtoSinistro.TextMatrix(i, 11)
            .Voucher = flexPgtoSinistro.TextMatrix(i, 23)
            .Seq_Estimativa = flexPgtoSinistro.TextMatrix(i, 5)
            .Beneficiario_id = flexPgtoSinistro.TextMatrix(i, 8)
            .Num_Parcela = flexPgtoSinistro.TextMatrix(i, 7)
            .Seq_Pgto = flexPgtoSinistro.TextMatrix(i, 6)
        End With
        Pagamentos.Add Pgto
    Next i
    
End Sub

Private Sub Monta_Pgto()

    Dim Pgto As New Pgto_Sinistro
    Dim rs As rdoResultset, Rs1 As rdoResultset
    Dim SQL As String, linha As String
    Dim Sin As Sinist, Recibo As String, Agrupado As Boolean

    Set Sin = SinistroEmUso.Item(1)
    
'    'Alan Neves - MP - 20/03/2013 - Inicio
'    'Montado select com valida��o Verifica_Aprova_Recusa, para diminuir o excesso de leitura nas tabelas
    SQL = "" & vbNewLine
    '' GENESCO SILVA - FLOW 18313521 - MIGRA��O SQL SERVER
    SQL = SQL & "set nocount on " & vbNewLine
    'Sql = Sql & "if isnull(OBJECT_ID('tempdb..#monta_pgto'),0) > 0" & vbNewLine
    SQL = SQL & "if OBJECT_ID('tempdb..#monta_pgto') IS NOT NULL " & vbNewLine
    SQL = SQL & "begin" & vbNewLine
    SQL = SQL & "  drop table #monta_pgto" & vbNewLine
    SQL = SQL & "end" & vbNewLine

    SQL = SQL & "create table #monta_pgto" & vbNewLine
    SQL = SQL & "(  dt_acerto_contas_sinistro smalldatetime," & vbNewLine
    SQL = SQL & "   val_acerto                numeric(15,2)," & vbNewLine 'Alan Neves - C_ANEVES - 21/06/2013
    SQL = SQL & "   val_correcao              numeric(15,2)," & vbNewLine 'Alan Neves - C_ANEVES - 21/06/2013
    SQL = SQL & "   item_val_estimativa       tinyint, " & vbNewLine
    SQL = SQL & "   situacao_op               char(1), " & vbNewLine
    SQL = SQL & "   motivo_rejeicao           varchar(255)  , " & vbNewLine
    SQL = SQL & "   forma_pgto_id             tinyint   ,    " & vbNewLine
    SQL = SQL & "   nome_forma                varchar(60),   " & vbNewLine
    SQL = SQL & "   beneficiario_id           tinyint ,      " & vbNewLine
    SQL = SQL & "   nome_benef                varchar(60),   " & vbNewLine
    SQL = SQL & "   cpf_benef                 varchar(11),  " & vbNewLine
    SQL = SQL & "   nome_cob                  varchar(60),  " & vbNewLine
    SQL = SQL & "   cod_cob                   int,          " & vbNewLine
    SQL = SQL & "   tp_objeto_id              tinyint,      " & vbNewLine
    SQL = SQL & "   nome_obj                  varchar(60),  " & vbNewLine
    SQL = SQL & "   seq_estimativa            tinyint,      " & vbNewLine
    SQL = SQL & "   seq_pgto                  tinyint,      " & vbNewLine
    SQL = SQL & "   num_parcela               tinyint,      " & vbNewLine
    SQL = SQL & "   dt_contabilizacao         smalldatetime,             " & vbNewLine
    SQL = SQL & "   dt_solicitacao_pgto       smalldatetime,             " & vbNewLine
    SQL = SQL & "   Usuario                   varchar(60) NOT NULL,   " & vbNewLine  'Alan Neves - C_ANEVES - 21/06/2013
    SQL = SQL & "   voucher_id                int,                       " & vbNewLine
    SQL = SQL & "   dt_avaliacao              smalldatetime,             " & vbNewLine
    SQL = SQL & "   usuario_avaliador         varchar(20),               " & vbNewLine
    SQL = SQL & "   num_carta_seguro_aceito   int,                       " & vbNewLine
    'Henrique H. Henriques - Ajuste da preciss�o do campo para poder permitir a aprova��o do pagamento - INI - INC000004886110
    SQL = SQL & "   val_cosseguro             numeric(15,4),             " & vbNewLine
    'Henrique H. Henriques - Ajuste da preciss�o do campo para poder permitir a aprova��o do pagamento - FIM - INC000004886110
    SQL = SQL & "   detalhamento_id           smallint,                  " & vbNewLine
    SQL = SQL & "   chave_autorizacao_sinistro numeric(8),               " & vbNewLine
    SQL = SQL & "   num_nota_fiscal            int,                      " & vbNewLine
    SQL = SQL & "   serie_nota_fiscal          varchar(5),               " & vbNewLine
    SQL = SQL & "   banco                      numeric(3),               " & vbNewLine
    SQL = SQL & "   agencia                    numeric(4),               " & vbNewLine
    SQL = SQL & "   agencia_dv                 char(1) ,                 " & vbNewLine
    SQL = SQL & "   conta_corrente             numeric(11),              " & vbNewLine
    SQL = SQL & "   conta_corrente_dv          char(2),                  " & vbNewLine
    SQL = SQL & "   conta_poupanca             numeric(11),              " & vbNewLine
    SQL = SQL & "   conta_poupanca_dv          varchar(2), " & vbNewLine
    SQL = SQL & "   pgto_beneficiario_id       tinyint ,   " & vbNewLine
    SQL = SQL & "   Verifica_Aprova_Recusa     char(1),    " & vbNewLine
    SQL = SQL & "   Verifica_Sin_BB            char(1),    " & vbNewLine
    SQL = SQL & "   Verifica_Tecnico           char(1),    " & vbNewLine
    SQL = SQL & "   Tipo                       int    )    " & vbNewLine

    SQL = SQL & "insert into #monta_pgto " & vbNewLine
    SQL = SQL & "   (dt_acerto_contas_sinistro,val_acerto,val_correcao,item_val_estimativa,situacao_op, motivo_rejeicao,forma_pgto_id, nome_forma ,beneficiario_id,nome_benef,cpf_benef ,nome_cob,cod_cob ," & vbNewLine
    SQL = SQL & "   tp_objeto_id    ,nome_obj,seq_estimativa,seq_pgto,num_parcela,dt_contabilizacao,dt_solicitacao_pgto,Usuario,voucher_id,dt_avaliacao,usuario_avaliador,num_carta_seguro_aceito," & vbNewLine
    SQL = SQL & "   val_cosseguro,detalhamento_id,chave_autorizacao_sinistro,num_nota_fiscal,serie_nota_fiscal,banco,agencia,agencia_dv ,conta_corrente," & vbNewLine
    SQL = SQL & "   conta_corrente_dv,conta_poupanca,conta_poupanca_dv,pgto_beneficiario_id)" & vbNewLine
    'Alan Neves - MP - 20/03/2013 - Fim
    
    SQL = SQL & " SELECT pgto_sinistro_tb.dt_acerto_contas_sinistro, " & _
            "val_acerto = isnull(pgto_sinistro_tb.val_acerto,0), " & _
            "val_correcao = isnull(pgto_sinistro_tb.val_correcao,0), " & _
            "pgto_sinistro_tb.item_val_estimativa, " & _
            "pgto_sinistro_tb.situacao_op, " & _
            "pgto_sinistro_tb.motivo_rejeicao, " & _
            "forma_pgto_tb.forma_pgto_id, " & _
            "nome_forma = forma_pgto_tb.nome, " & _
            "sinistro_benef_tb.beneficiario_id, " & _
            "nome_benef = sinistro_benef_tb.nome, "
                        'Fabricio Brandi - Nova Consultoria : 29/09/2011
                        'Adicionado o cpf ao numero de colunas
    SQL = SQL & "cpf_benef = sinistro_benef_tb.cpf, " & _
            "nome_cob = tp_cobertura_tb.nome, " & _
            "cod_cob = tp_cobertura_tb.tp_cobertura_id, " & _
            "tp_objeto_tb.tp_objeto_id, " & _
            "nome_obj = tp_objeto_tb.nome, "
    SQL = SQL & "pgto_sinistro_tb.seq_estimativa, " & _
            "pgto_sinistro_tb.seq_pgto, " & _
            "pgto_sinistro_tb.num_parcela, " & _
            "pgto_sinistro_tb.dt_contabilizacao, " & _
            "pgto_sinistro_tb.dt_solicitacao_pgto, " & _
            "pgto_sinistro_tb.Usuario, " & _
            "ps_acerto_pagamento_tb.voucher_id, " & _
            "pgto_sinistro_tb.dt_avaliacao, " & _
            "pgto_sinistro_tb.usuario_avaliador, " & _
            "sinistro_estimativa_tb.num_carta_seguro_aceito, " & _
            "pgto_sinistro_tb.val_cosseguro, " & _
            "pgto_sinistro_tb.detalhamento_id, " & _
            "pgto_sinistro_tb.chave_autorizacao_sinistro,  " & _
            "pgto_sinistro_tb.num_nota_fiscal, " & _
            "pgto_sinistro_tb.serie_nota_fiscal "
'Ralves - 10/06/2010 - Demanda 3798421 - Dados da conta para pagamento / Inserindo dados bancarios
SQL = SQL & ", banco = ISNULL(CASE WHEN forma_pgto_tb.forma_pgto_id IN (13,14) THEN sinistro_benef_tb.banco ELSE 0 END,0)" & _
            ", agencia = ISNULL(CASE WHEN forma_pgto_tb.forma_pgto_id IN (13,14) THEN sinistro_benef_tb.agencia ELSE 0 END,0)" & _
            ", agencia_dv = ISNULL(CASE WHEN forma_pgto_tb.forma_pgto_id IN (13,14) THEN sinistro_benef_tb.agencia_dv ELSE '0' END,'0') " & _
            ", conta_corrente = ISNULL(CASE WHEN forma_pgto_tb.forma_pgto_id IN (13,14) THEN sinistro_benef_tb.conta_corrente ELSE 0 END,0) " & _
            ", conta_corrente_dv = ISNULL(CASE WHEN forma_pgto_tb.forma_pgto_id IN (13,14) THEN sinistro_benef_tb.conta_corrente_dv ELSE '0' END,'0') " & _
            ", conta_poupanca = ISNULL(CASE WHEN forma_pgto_tb.forma_pgto_id IN (14) THEN sinistro_benef_tb.conta_poupanca ELSE 0 END,0)" & _
            ", conta_poupanca_dv = ISNULL(CASE WHEN forma_pgto_tb.forma_pgto_id IN (14) THEN sinistro_benef_tb.conta_poupanca_dv ELSE '0' END,'0')"
    
    'carolina.marinho 08/09/2011 - Demanda 11104810 - M�dulo de Aprova��o - Honor�rios Duplicados
    'conforme solicitado na demanda, caso o item_val_estimativa seja honorarios ou despesas, n�o considerar
    'a cobertura ao mostrar os itens no grid Pagamentos Aprovados
    SQL = SQL & ", pgto_sinistro_tb.beneficiario_id "
    'carolina.marinho fim
    
    'Alan Neves - 19/03/2013 - MP - Inicio
    'SQL = SQL & "FROM pgto_sinistro_tb with(nolock) "
    SQL = SQL & "FROM #pgto_sinistro_tb pgto_sinistro_tb with(nolock) "
    'Alan Neves - 19/03/2013 - Fim
    
    SQL = SQL & _
            "INNER JOIN forma_pgto_tb with(nolock) " & _
             "ON pgto_sinistro_tb.forma_pgto_id = forma_pgto_tb.Forma_pgto_id " & _
            "INNER JOIN sinistro_benef_tb with(nolock) " & _
            "ON pgto_sinistro_tb.sinistro_id = sinistro_benef_tb.sinistro_id " & _
            "AND pgto_sinistro_tb.apolice_id = sinistro_benef_tb.apolice_id " & _
            "AND pgto_sinistro_tb.sucursal_seguradora_id = " & _
                    "sinistro_benef_tb.sucursal_seguradora_id " & _
            "AND pgto_sinistro_tb.seguradora_cod_susep = " & _
                    "sinistro_benef_tb.seguradora_cod_susep " & _
            "AND pgto_sinistro_tb.ramo_id = sinistro_benef_tb.ramo_id " & _
            "AND pgto_sinistro_tb.beneficiario_id = sinistro_benef_tb.beneficiario_id "
    SQL = SQL & "LEFT OUTER JOIN (pgto_sinistro_cobertura_tb with(nolock) " & _
            "       INNER JOIN tp_cobertura_tb with(nolock) ON tp_cobertura_tb.tp_cobertura_id = pgto_sinistro_cobertura_tb.tp_cobertura_id " & _
            "       INNER JOIN tp_objeto_tb with(nolock) ON tp_objeto_tb.tp_objeto_id = pgto_sinistro_cobertura_tb.tp_objeto_id " & _
            "       ) ON " & _
            "     pgto_sinistro_tb.sinistro_id = pgto_sinistro_cobertura_tb.sinistro_id " & _
            " AND pgto_sinistro_tb.apolice_id = pgto_sinistro_cobertura_tb.apolice_id " & _
            " AND pgto_sinistro_tb.sucursal_seguradora_id = pgto_sinistro_cobertura_tb.sucursal_seguradora_id " & _
            " AND pgto_sinistro_tb.seguradora_cod_susep = pgto_sinistro_cobertura_tb.seguradora_cod_susep " & _
            " AND pgto_sinistro_tb.ramo_id = pgto_sinistro_cobertura_tb.ramo_id " & _
            " AND pgto_sinistro_tb.item_val_estimativa = pgto_sinistro_cobertura_tb.item_val_estimativa " & _
            " AND pgto_sinistro_tb.seq_estimativa = pgto_sinistro_cobertura_tb.seq_estimativa " & _
            " AND pgto_sinistro_tb.seq_pgto = pgto_sinistro_cobertura_tb.seq_pgto " & _
            " AND pgto_sinistro_tb.num_parcela = pgto_sinistro_cobertura_tb.num_parcela " & _
            " AND pgto_sinistro_tb.beneficiario_id = pgto_sinistro_cobertura_tb.beneficiario_id "
    'Alan Neves - MP - 19/03/2013 - Inicio - Leitura na temp
    'SQL = SQL & " INNER JOIN sinistro_estimativa_tb with(nolock) " &
     SQL = SQL & " INNER JOIN #sinistro_estimativa_tb sinistro_estimativa_tb " & _
            "ON pgto_sinistro_tb.sinistro_id = sinistro_estimativa_tb.sinistro_id " & _
            "AND pgto_sinistro_tb.apolice_id = sinistro_estimativa_tb.apolice_id " & _
            "AND pgto_sinistro_tb.sucursal_seguradora_id = " & _
                    "sinistro_estimativa_tb.sucursal_seguradora_id " & _
            "AND pgto_sinistro_tb.seguradora_cod_susep = " & _
                    "sinistro_estimativa_tb.seguradora_cod_susep " & _
            "AND pgto_sinistro_tb.ramo_id = sinistro_estimativa_tb.ramo_id " & _
            "AND pgto_sinistro_tb.seq_estimativa = sinistro_estimativa_tb.seq_estimativa " & _
            "AND pgto_sinistro_tb.item_val_estimativa = sinistro_estimativa_tb.item_val_estimativa "
    SQL = SQL & "LEFT OUTER JOIN ps_acerto_pagamento_tb with(nolock) ON pgto_sinistro_tb.acerto_id = ps_acerto_pagamento_tb.acerto_id " & _
            "WHERE pgto_sinistro_tb.sinistro_id = " & Sin.sinistro_id & _
            " AND pgto_sinistro_tb.apolice_id = " & Sin.Apolice_id & _
            " AND pgto_sinistro_tb.sucursal_seguradora_id = " & Sin.Sucursal_id & _
            " AND pgto_sinistro_tb.seguradora_cod_susep = " & Sin.Seguradora_id & _
            " AND pgto_sinistro_tb.ramo_id = " & Sin.ramo_id
    'SQL = SQL & " AND pgto_sinistro_tb.val_resseguro_recuperado IS NULL "
    SQL = SQL & " AND pgto_sinistro_tb.num_parcela = 1 "
    
    'If voper = "C" Then
    If (voper = "C") Or (voper = "A") Then
        SQL = SQL & " AND ((pgto_sinistro_tb.tp_operacao = 'd') OR "
        SQL = SQL & "      (pgto_sinistro_tb.tp_operacao = 'c' and pgto_sinistro_tb.item_val_estimativa in (4,6) )) "
    Else
        SQL = SQL & " AND pgto_sinistro_tb.tp_operacao = 'd' "
    End If
    
    If voper = "I" Or voper = "IS" Then
        SQL = SQL & " AND pgto_sinistro_tb.chave_autorizacao_sinistro is not null "
        SQL = SQL & " AND pgto_sinistro_tb.situacao_op = 'n'"
    ElseIf vSituacao <> "'0'" Then
        SQL = SQL & " AND pgto_sinistro_tb.situacao_op = " & vSituacao
    End If
    If frmAcertoCon.cmbCampo.Text = "Voucher" Then
        SQL = SQL & " AND ps_acerto_pagamento_tb.voucher_id = " & frmAcertoCon.txtConteudo
    End If
    
    'carolina.marinho 08/09/2011 - Demanda 11104810 - M�dulo de Aprova��o - Honor�rios Duplicados
    'conforme solicitado na demanda, caso o item_val_estimativa seja honorarios ou despesas, n�o considerar
    'a cobertura ao mostrar os itens no grid Pagamentos Aprovados
    
    
    SQL = SQL & " AND pgto_sinistro_tb.item_val_estimativa NOT IN (2,3) "
    
    SQL = SQL & " UNION ALL "
    
    SQL = SQL & "SELECT DISTINCT pgto_sinistro_tb.dt_acerto_contas_sinistro, " & _
            "val_acerto = isnull(pgto_sinistro_tb.val_acerto,0), " & _
            "val_correcao = isnull(pgto_sinistro_tb.val_correcao,0), " & _
            "pgto_sinistro_tb.item_val_estimativa, " & _
            "pgto_sinistro_tb.situacao_op, " & _
            "pgto_sinistro_tb.motivo_rejeicao, " & _
            "forma_pgto_tb.forma_pgto_id, " & _
            "nome_forma = forma_pgto_tb.nome, " & _
            "sinistro_benef_tb.beneficiario_id, " & _
            "nome_benef = sinistro_benef_tb.nome, "
    'Fabricio Brandi - Nova Consultoria : 11/10/2011
    'Adicionado o campo CPF
    SQL = SQL & "cpf_benef = sinistro_benef_TB.cpf, " & _
            "nome_cob = '', " & _
            "cod_cob = '', " & _
            "tp_objeto_tb.tp_objeto_id, " & _
            "nome_obj = tp_objeto_tb.nome, "
    SQL = SQL & "pgto_sinistro_tb.seq_estimativa, " & _
            "pgto_sinistro_tb.seq_pgto, " & _
            "pgto_sinistro_tb.num_parcela, " & _
            "pgto_sinistro_tb.dt_contabilizacao, " & _
            "pgto_sinistro_tb.dt_solicitacao_pgto, " & _
            "pgto_sinistro_tb.Usuario, " & _
            "ps_acerto_pagamento_tb.voucher_id, " & _
            "pgto_sinistro_tb.dt_avaliacao, " & _
            "pgto_sinistro_tb.usuario_avaliador, " & _
            "sinistro_estimativa_tb.num_carta_seguro_aceito, " & _
            "pgto_sinistro_tb.val_cosseguro, " & _
            "pgto_sinistro_tb.detalhamento_id, " & _
            "pgto_sinistro_tb.chave_autorizacao_sinistro,  " & _
            "pgto_sinistro_tb.num_nota_fiscal, " & _
            "pgto_sinistro_tb.serie_nota_fiscal "
'Ralves - 10/06/2010 - Demanda 3798421 - Dados da conta para pagamento / Inserindo dados bancarios
SQL = SQL & ", banco = ISNULL(CASE WHEN forma_pgto_tb.forma_pgto_id IN (13,14) THEN sinistro_benef_tb.banco ELSE 0 END,0)" & _
            ", agencia = ISNULL(CASE WHEN forma_pgto_tb.forma_pgto_id IN (13,14) THEN sinistro_benef_tb.agencia ELSE 0 END,0)" & _
            ", agencia_dv = ISNULL(CASE WHEN forma_pgto_tb.forma_pgto_id IN (13,14) THEN sinistro_benef_tb.agencia_dv ELSE '0' END,'0') " & _
            ", conta_corrente = ISNULL(CASE WHEN forma_pgto_tb.forma_pgto_id IN (13,14) THEN sinistro_benef_tb.conta_corrente ELSE 0 END,0) " & _
            ", conta_corrente_dv = ISNULL(CASE WHEN forma_pgto_tb.forma_pgto_id IN (13,14) THEN sinistro_benef_tb.conta_corrente_dv ELSE '0' END,'0') " & _
            ", conta_poupanca = ISNULL(CASE WHEN forma_pgto_tb.forma_pgto_id IN (14) THEN sinistro_benef_tb.conta_poupanca ELSE 0 END,0)" & _
            ", conta_poupanca_dv = ISNULL(CASE WHEN forma_pgto_tb.forma_pgto_id IN (14) THEN sinistro_benef_tb.conta_poupanca_dv ELSE '0' END,'0')"
    
    SQL = SQL & ", pgto_sinistro_tb.beneficiario_id "
    
    'Alan Neves - 19/03/2013 - MP - Inicio
    'SQL = SQL & "FROM pgto_sinistro_tb with(nolock) "
    SQL = SQL & "FROM #pgto_sinistro_tb pgto_sinistro_tb with(nolock) "
    'Alan Neves - 19/03/2013 - MP - Fim
    
    SQL = SQL & _
            "INNER JOIN forma_pgto_tb with(nolock) " & _
             "ON pgto_sinistro_tb.forma_pgto_id = forma_pgto_tb.Forma_pgto_id " & _
            "INNER JOIN sinistro_benef_tb with(nolock) " & _
            "ON pgto_sinistro_tb.sinistro_id = sinistro_benef_tb.sinistro_id " & _
            "AND pgto_sinistro_tb.apolice_id = sinistro_benef_tb.apolice_id " & _
            "AND pgto_sinistro_tb.sucursal_seguradora_id = " & _
                    "sinistro_benef_tb.sucursal_seguradora_id " & _
            "AND pgto_sinistro_tb.seguradora_cod_susep = " & _
                    "sinistro_benef_tb.seguradora_cod_susep " & _
            "AND pgto_sinistro_tb.ramo_id = sinistro_benef_tb.ramo_id " & _
            "AND pgto_sinistro_tb.beneficiario_id = sinistro_benef_tb.beneficiario_id "
    SQL = SQL & "LEFT OUTER JOIN (pgto_sinistro_cobertura_tb with(nolock) " & _
            "       INNER JOIN tp_cobertura_tb with(nolock) ON tp_cobertura_tb.tp_cobertura_id = pgto_sinistro_cobertura_tb.tp_cobertura_id " & _
            "       INNER JOIN tp_objeto_tb with(nolock) ON tp_objeto_tb.tp_objeto_id = pgto_sinistro_cobertura_tb.tp_objeto_id " & _
            "       ) ON " & _
            "     pgto_sinistro_tb.sinistro_id = pgto_sinistro_cobertura_tb.sinistro_id " & _
            " AND pgto_sinistro_tb.apolice_id = pgto_sinistro_cobertura_tb.apolice_id " & _
            " AND pgto_sinistro_tb.sucursal_seguradora_id = pgto_sinistro_cobertura_tb.sucursal_seguradora_id " & _
            " AND pgto_sinistro_tb.seguradora_cod_susep = pgto_sinistro_cobertura_tb.seguradora_cod_susep " & _
            " AND pgto_sinistro_tb.ramo_id = pgto_sinistro_cobertura_tb.ramo_id " & _
            " AND pgto_sinistro_tb.item_val_estimativa = pgto_sinistro_cobertura_tb.item_val_estimativa " & _
            " AND pgto_sinistro_tb.seq_estimativa = pgto_sinistro_cobertura_tb.seq_estimativa " & _
            " AND pgto_sinistro_tb.seq_pgto = pgto_sinistro_cobertura_tb.seq_pgto " & _
            " AND pgto_sinistro_tb.num_parcela = pgto_sinistro_cobertura_tb.num_parcela " & _
            " AND pgto_sinistro_tb.beneficiario_id = pgto_sinistro_cobertura_tb.beneficiario_id "
    'Alan Neves - MP - 19/03/2013 - Inicio - Leitura na temp
    'SQL = SQL & " INNER JOIN sinistro_estimativa_tb with(nolock) " &
     SQL = SQL & " INNER JOIN #sinistro_estimativa_tb sinistro_estimativa_tb with(nolock) " & _
            "ON pgto_sinistro_tb.sinistro_id = sinistro_estimativa_tb.sinistro_id " & _
            "AND pgto_sinistro_tb.apolice_id = sinistro_estimativa_tb.apolice_id " & _
            "AND pgto_sinistro_tb.sucursal_seguradora_id = " & _
                    "sinistro_estimativa_tb.sucursal_seguradora_id " & _
            "AND pgto_sinistro_tb.seguradora_cod_susep = " & _
                    "sinistro_estimativa_tb.seguradora_cod_susep " & _
            "AND pgto_sinistro_tb.ramo_id = sinistro_estimativa_tb.ramo_id " & _
            "AND pgto_sinistro_tb.seq_estimativa = sinistro_estimativa_tb.seq_estimativa " & _
            "AND pgto_sinistro_tb.item_val_estimativa = sinistro_estimativa_tb.item_val_estimativa "
    SQL = SQL & "LEFT OUTER JOIN ps_acerto_pagamento_tb with(nolock) ON pgto_sinistro_tb.acerto_id = ps_acerto_pagamento_tb.acerto_id " & _
            "WHERE pgto_sinistro_tb.sinistro_id = " & Sin.sinistro_id & _
            " AND pgto_sinistro_tb.apolice_id = " & Sin.Apolice_id & _
            " AND pgto_sinistro_tb.sucursal_seguradora_id = " & Sin.Sucursal_id & _
            " AND pgto_sinistro_tb.seguradora_cod_susep = " & Sin.Seguradora_id & _
            " AND pgto_sinistro_tb.ramo_id = " & Sin.ramo_id
    'SQL = SQL & " AND pgto_sinistro_tb.val_resseguro_recuperado IS NULL "
    SQL = SQL & " AND pgto_sinistro_tb.num_parcela = 1 "
    
    'If voper = "C" Then
    If (voper = "C") Or (voper = "A") Then
        SQL = SQL & " AND ((pgto_sinistro_tb.tp_operacao = 'd') OR "
        SQL = SQL & "      (pgto_sinistro_tb.tp_operacao = 'c' and pgto_sinistro_tb.item_val_estimativa in (4,6) )) "
    Else
        SQL = SQL & " AND pgto_sinistro_tb.tp_operacao = 'd' "
    End If
    
    If voper = "I" Or voper = "IS" Then
        SQL = SQL & " AND pgto_sinistro_tb.chave_autorizacao_sinistro is not null "
        SQL = SQL & " AND pgto_sinistro_tb.situacao_op = 'n'"
    ElseIf vSituacao <> "'0'" Then
        SQL = SQL & " AND pgto_sinistro_tb.situacao_op = " & vSituacao
    End If
    If frmAcertoCon.cmbCampo.Text = "Voucher" Then
        SQL = SQL & " AND ps_acerto_pagamento_tb.voucher_id = " & frmAcertoCon.txtConteudo
    End If
    
    SQL = SQL & " AND pgto_sinistro_tb.item_val_estimativa IN (2,3) "
    'carolina.marinho fim
    
    'Alan Neves - MP - 20/03/2013 - Inicio
'    SQL = SQL & " ORDER BY pgto_sinistro_tb.dt_acerto_contas_sinistro, " & _
'                    "pgto_sinistro_tb.beneficiario_id, pgto_sinistro_tb.item_val_estimativa"
    'Alan Neves - MP - 20/03/2013 - Fim
    
    'Alan Neves - MP - 20/03/2013 - Inicio
    'Valida��o em Verifica_Aprova_Recusa, para diminuir o excesso de leitura nas tabelas
    SQL = SQL & "   update #monta_pgto" & vbNewLine
    SQL = SQL & "     set Verifica_Aprova_Recusa = 's'," & vbNewLine
    SQL = SQL & "         Verifica_Sin_BB        = 'n', " & vbNewLine
    SQL = SQL & "         Verifica_Tecnico       = 'n', " & vbNewLine
    SQL = SQL & "         Tipo                   = 1 " & vbNewLine

    SQL = SQL & "   update #monta_pgto " & vbNewLine
    SQL = SQL & "      set Verifica_Sin_BB = 's'  " & vbNewLine
    SQL = SQL & "     from seguros_db..sinistro_bb_tb sinistro_bb with (nolock) " & vbNewLine
    SQL = SQL & "    where sinistro_bb.sinistro_id = " & Sin.sinistro_id & vbNewLine

    SQL = SQL & "   if (select count(1)  " & vbNewLine
    SQL = SQL & "         from #monta_pgto " & vbNewLine
    SQL = SQL & "        where Tipo = 1  " & vbNewLine
    SQL = SQL & "          and Verifica_Sin_BB = 's' ) > 0  " & vbNewLine
    SQL = SQL & "   begin " & vbNewLine
    SQL = SQL & "       update #monta_pgto " & vbNewLine
    SQL = SQL & "          set Verifica_Tecnico = 's'  " & vbNewLine
    SQL = SQL & "         from seguros_db..tecnico_tb tecnico_tb with (nolock) " & vbNewLine
    SQL = SQL & "        where tecnico_tb.tecnico_id = " & Tecnico_atual & vbNewLine

    SQL = SQL & "       if @@rowcount = 0  " & vbNewLine
    SQL = SQL & "           update #monta_pgto " & vbNewLine
    SQL = SQL & "              set Verifica_Aprova_Recusa = 'n', " & vbNewLine
    SQL = SQL & "                  Verifica_Sin_BB        = 'n' " & vbNewLine
    SQL = SQL & "   end " & vbNewLine

    SQL = SQL & "   select  " & vbNewLine
    SQL = SQL & "      dt_acerto_contas_sinistro    " & vbNewLine
    SQL = SQL & "    , val_acerto                   " & vbNewLine
    SQL = SQL & "    , val_correcao                 " & vbNewLine
    SQL = SQL & "    , item_val_estimativa          " & vbNewLine
    SQL = SQL & "    , situacao_op                  " & vbNewLine
    SQL = SQL & "    , motivo_rejeicao              " & vbNewLine
    SQL = SQL & "    , forma_pgto_id                " & vbNewLine
    SQL = SQL & "    , nome_forma                       " & vbNewLine
    SQL = SQL & "    , beneficiario_id                  " & vbNewLine
    SQL = SQL & "    , nome_benef                       " & vbNewLine
    SQL = SQL & "    , cpf_benef                        " & vbNewLine
    SQL = SQL & "    , nome_cob                         " & vbNewLine
    SQL = SQL & "    , cod_cob                          " & vbNewLine
    SQL = SQL & "    , tp_objeto_id                     " & vbNewLine
    SQL = SQL & "    , nome_obj                         " & vbNewLine
    SQL = SQL & "    , seq_estimativa                   " & vbNewLine
    SQL = SQL & "    , seq_pgto                         " & vbNewLine
    SQL = SQL & "    , num_parcela                      " & vbNewLine
    SQL = SQL & "    , dt_contabilizacao                " & vbNewLine
    SQL = SQL & "    , dt_solicitacao_pgto              " & vbNewLine
    SQL = SQL & "    , Usuario                          " & vbNewLine
    SQL = SQL & "    , voucher_id                       " & vbNewLine
    SQL = SQL & "    , dt_avaliacao                     " & vbNewLine
    SQL = SQL & "    , usuario_avaliador                " & vbNewLine
    SQL = SQL & "    , num_carta_seguro_aceito          " & vbNewLine
    SQL = SQL & "    , val_cosseguro                    " & vbNewLine
    SQL = SQL & "    , detalhamento_id                  " & vbNewLine
    SQL = SQL & "    , chave_autorizacao_sinistro       " & vbNewLine
    SQL = SQL & "    , num_nota_fiscal                  " & vbNewLine
    SQL = SQL & "    , serie_nota_fiscal                " & vbNewLine
    SQL = SQL & "    , banco                            " & vbNewLine
    SQL = SQL & "    , agencia                          " & vbNewLine
    SQL = SQL & "    , agencia_dv                       " & vbNewLine
    SQL = SQL & "    , conta_corrente                   " & vbNewLine
    SQL = SQL & "    , conta_corrente_dv                " & vbNewLine
    SQL = SQL & "    , conta_poupanca                   " & vbNewLine
    SQL = SQL & "    , conta_poupanca_dv                " & vbNewLine
    SQL = SQL & "    , pgto_beneficiario_id             " & vbNewLine
    SQL = SQL & "    , Verifica_Aprova_Recusa           " & vbNewLine
    SQL = SQL & "  from #monta_pgto                    " & vbNewLine
    SQL = SQL & "  order by  dt_acerto_contas_sinistro, beneficiario_id, item_val_estimativa"
    'Alan Neves - MP - 20/03/2013 - Inicio
                    
    Set rs = rdocn1.OpenResultset(SQL)
    
    flexPgtoSinistro.Rows = 1
    val_indenizacao_pago = 0 '-- 19406709 - Melhoria nos limites de sinistro e emissao
    
    If Pagamentos.Count > 0 Then
        Desmonta_Pagto
    End If
    
    While Not rs.EOF
        '-------------------------------------------------------------------------------
        'Alterado por Stefanini Consultor: Cleber - 08/11/2004
        'Foi colocado uma consist�ncia no momento em que carrega o grid que
        'lista os pagamentos. O programa somente possibilita pagamento e
        'recusa de indeniza��o por t�cnicos autorizados (com a chave_usuario_SISBB).
        'Para fazer a consist�ncia foi criada 3 fun��es: Verifica_Sin_BB, Verifica_Aprova_Recusa, Verifica_Tecnico
        
        'Alan Neves - MP - 20/03/2013 - Inicio - Valida��o realizada na execu��o do SQL
        'If Verifica_Aprova_Recusa(rs("item_val_estimativa"), Sin.sinistro_id) Then 'rs("sinistro_id")) Then
        If rs("Verifica_Aprova_Recusa") = "s" Then
        'Alan Neves - MP - 20/03/2013 - Fim
        '--------------------------------------------------------------------------------
                   
            Agrupado = False
            If (vopercoseguro = "C") And (Not IsNull(rs("chave_autorizacao_sinistro"))) Then
                'Alan Neves - MP - 11/03/2013 - Inicio
                'SQL = "SELECT count(*) "
                SQL = "SELECT count(1) "
                
                SQL = SQL & " FROM pgto_sinistro_tb  "
                'Alan Neves - MP - 11/03/2013 - Fim
                
                SQL = SQL & " WHERE chave_autorizacao_sinistro = " & rs("chave_autorizacao_sinistro")
                'SQL = SQL & " AND   val_resseguro_recuperado IS NULL"
                SQL = SQL & " AND num_parcela = 1 "
                
                'Alan Neves - MP - 11/03/2013 - Inicio
                'Set Rs1 = rdocn1.OpenResultset(SQL)
                Set Rs1 = rdocn.OpenResultset(SQL)
                'Alan Neves - MP - 11/03/2013 - Fim
                
                If Rs1(0) > 1 Then
                    Agrupado = True
                End If
                Rs1.Close
            End If
            
            'Popula propriedades do componente Pgto
            If (Not Agrupado) Or (voper = "C") Then
                Set Pgto = New Pgto_Sinistro
                With Pgto
                    .dt_Acerto_Contas = Format(rs("dt_acerto_contas_sinistro"), "dd/mm/yyyy")
                    .val_Acerto = Format(Val(rs("val_acerto")), "#,##0.00")
                    .val_Correcao = Format(Val(rs("val_correcao")), "#,##0.00")
                    .Item_val_estimativa = rs("item_val_estimativa")
                    If IsNull(rs("chave_autorizacao_sinistro")) Then
                        .ChaveOP = ""
                    Else
                        .ChaveOP = rs("chave_autorizacao_sinistro")
                    End If
                    
                    Recibo = ""
                    If Not IsNull(rs("detalhamento_id")) Then
                        SQL = "SELECT linha "
                        SQL = SQL & " FROM sinistro_linha_detalhamento_tb with(nolock) "
                        SQL = SQL & " WHERE sinistro_id = " & Sin.sinistro_id
                        SQL = SQL & "   AND apolice_id = " & Sin.Apolice_id
                        SQL = SQL & "   AND seguradora_cod_susep = " & Sin.Seguradora_id
                        SQL = SQL & "   AND sucursal_seguradora_id = " & Sin.Sucursal_id
                        SQL = SQL & "   AND ramo_id = " & Sin.ramo_id
                        SQL = SQL & "   AND detalhamento_id = " & rs("detalhamento_id")
                        
                        'Set Rs1 = rdocn1.OpenResultset(SQL)   'C_ANEVES - 21/06/2013
                        Set Rs1 = rdocn.OpenResultset(SQL)    'C_ANEVES - 21/06/2013
                        While Not Rs1.EOF
                            Recibo = Recibo & "" & Rs1(0) & vbNewLine
                            Rs1.MoveNext
                        Wend
                        Rs1.Close
                    End If
                    .Recibo = Recibo
                    'msalema  - 16/05/2013
                    'Select Case rs("situacao_op")
                    Select Case LCase(rs("situacao_op"))
                    'msalema - 16/05/2013
                        Case "a": .Situacao_OP = "APROVADO"
                        Case "r": .Situacao_OP = "REJEITADO"
                        Case "c": .Situacao_OP = "CANCELADO"
                        Case "e": .Situacao_OP = "ESTORNADO"
                        Case "n": .Situacao_OP = "AGUARD. APROVA��O"
                    End Select
                    If IsNull(rs("motivo_rejeicao")) = True Then
                        .Motivo_Rejeicao = ""
                    Else
                        .Motivo_Rejeicao = rs("motivo_rejeicao")
                    End If
                    .Forma_Pagto_id = rs("forma_pgto_id")
                    .Forma_Pagto = rs("nome_forma")
                    If IsNull(rs("nome_cob")) = True Then
                        .Nome_Cob = ""
                        .tp_Cobertura_id = 0
                    Else
                        .Nome_Cob = rs("nome_cob")
                        .tp_Cobertura_id = rs("cod_cob")
                    End If
                    If IsNull(rs("nome_obj")) = True Then
                        .Nome_Obj = ""
                    Else
                        .Nome_Obj = rs("nome_obj")
                    End If
                    .Beneficiario_id = rs("beneficiario_id")
                    If IsNull(rs("nome_benef")) = True Then
                        .Beneficiario_Nome = ""
                    Else
                        .Beneficiario_Nome = rs("nome_benef")
                    End If
                        'Fabricio Brandi - Nova Consultoria : 29/09/2011
                        'Adicionado o campo CPF
                    If IsNull(rs("cpf_benef")) = True Then
                        .Beneficiario_CPF = ""
                    Else
                        .Beneficiario_CPF = rs("cpf_benef")
                    End If
                        'Fabricio Brandi - Nova Consultoria : 29/09/2011
                        '-------------- FIM ----------------------------
                    .Seq_Estimativa = rs("seq_estimativa")
                    .Seq_Pgto = rs("seq_pgto")
                    .Num_Parcela = rs("num_parcela")
                    If IsNull(rs("dt_contabilizacao")) = True Then
                        .dt_contabilizacao = ""
                    Else
                        .dt_contabilizacao = rs("dt_contabilizacao")
                    End If
                    .dt_emissao = rs("dt_solicitacao_pgto")
                    If IsNull(rs("usuario")) = True Then
                        .Usuario = ""
                    Else
                        .Usuario = rs("usuario")
                    End If
                    If IsNull(rs("voucher_id")) = True Then
                        .Voucher = ""
                    Else
                        .Voucher = rs("voucher_id")
                    End If
                    
                    .Num_Nota_Fiscal = IIf(IsNull(rs("num_nota_fiscal")), "", rs("num_nota_fiscal"))
                    .Serie_Nota_Fiscal = IIf(IsNull(rs("serie_nota_fiscal")), "", rs("serie_nota_fiscal"))
                    
                    If IsNull(rs("dt_avaliacao")) = True Then
                        .dt_Aprovacao = ""
                    Else
                        .dt_Aprovacao = rs("dt_avaliacao")
                    End If
                    If IsNull(rs("usuario_avaliador")) = True Then
                        .usuario_aprovacao = ""
                    Else
                        .usuario_aprovacao = rs("usuario_avaliador")
                    End If
                End With
                Pagamentos.Add Pgto
                
                flexPgtoSinistro.Row = flexPgtoSinistro.Rows - 1
                linha = Format(rs("dt_acerto_contas_sinistro"), "dd/mm/yyyy") & vbTab
                'msalema - 16/05/2013
                'Select Case rs("situacao_op")
                Select Case LCase(rs("situacao_op"))
                'msalema - 16/05/2013
                    Case "a": linha = linha & "APROVADO" & vbTab
                    Case "r": linha = linha & "REJEITADO" & vbTab
                    Case "c": linha = linha & "CANCELADO" & vbTab
                    Case "e": linha = linha & "ESTORNADO" & vbTab
                    Case "n": linha = linha & "AGUARD. APROVA��O" & vbTab
                End Select
                Select Case rs("item_val_estimativa")
                    Case 1
                        linha = linha & "INDENIZA��O" & vbTab
                    Case 2
                        linha = linha & "HONOR�RIOS" & vbTab
                    Case 3
                        linha = linha & "DESPESAS" & vbTab
                    Case 4
                        linha = linha & "RESSARCIMENTO" & vbTab
                    Case 5
                        linha = linha & "DESP. RESSARCIMENTO" & vbTab
                    Case 6
                        linha = linha & "SALVADOS" & vbTab
                    Case 7
                        linha = linha & "DESP. SALVADOS" & vbTab
                End Select
                
                
                linha = linha & rs("nome_cob") & vbTab
                linha = linha & IIf(rs("tp_objeto_id") = 0, "", rs("nome_obj")) & vbTab
                linha = linha & Pgto.val_Acerto & vbTab
                linha = linha & Pgto.val_Correcao & vbTab
                linha = linha & rs("nome_forma") & vbTab
                linha = linha & rs("nome_benef") & vbTab
                'Fabricio Brandi - Nova Consultoria : 29/09/2011
                'Adicionado o campo CPF
                linha = linha & rs("cpf_benef") & vbTab
                '---------------- FIM ----------------------
                If voper = "I" Or voper = "IS" Then
                    linha = linha & Format(rs("dt_solicitacao_pgto"), "dd/mm/yyyy") & vbTab
                    linha = linha & Pgto.Usuario & vbTab
'Ralves - 10/06/2010 - Demanda 3798421 - Dados da conta para pagamento / Inserindo dados bancarios
                    If vopercoseguro = "C" Then
                        If lblNossaParteCosseguro.Visible = True Then
                            linha = linha & Pgto.Voucher & vbTab
                            linha = linha & Pgto.Num_Nota_Fiscal & vbTab
                            linha = linha & Pgto.Serie_Nota_Fiscal & vbTab
                            linha = linha & IIf(IsNull(rs("num_carta_seguro_aceito")), "", rs("num_carta_seguro_aceito")) & vbTab & Format((CCur(Pgto.val_Acerto) - Val(rs("val_cosseguro"))), "#,##0.00") & vbTab
                            linha = linha & rs("banco") & vbTab
                            linha = linha & rs("agencia") & vbTab
                            linha = linha & rs("agencia_dv") & vbTab
                            linha = linha & rs("conta_corrente") & vbTab
                            linha = linha & rs("conta_corrente_dv") & vbTab
                            linha = linha & rs("conta_poupanca") & vbTab
                            linha = linha & rs("conta_poupanca_dv") & vbTab
                        Else
                            linha = linha & Pgto.Voucher & vbTab
                            linha = linha & Pgto.Num_Nota_Fiscal & vbTab
                            linha = linha & Pgto.Serie_Nota_Fiscal & vbTab
                            linha = linha & IIf(IsNull(rs("num_carta_seguro_aceito")), "", rs("num_carta_seguro_aceito")) & vbTab
                            linha = linha & rs("banco") & vbTab
                            linha = linha & rs("agencia") & vbTab
                            linha = linha & rs("agencia_dv") & vbTab
                            linha = linha & rs("conta_corrente") & vbTab
                            linha = linha & rs("conta_corrente_dv") & vbTab
                            linha = linha & rs("conta_poupanca") & vbTab
                            linha = linha & rs("conta_poupanca_dv") & vbTab
                        End If
                    Else
                        If lblNossaParteCosseguro.Visible = True Then
                            linha = linha & Pgto.Voucher & vbTab
                            linha = linha & Pgto.Num_Nota_Fiscal & vbTab
                            linha = linha & Pgto.Serie_Nota_Fiscal & vbTab & Format((CCur(Pgto.val_Acerto) - Val(rs("val_cosseguro"))), "#,##0.00") & vbTab
                            linha = linha & rs("banco") & vbTab
                            linha = linha & rs("agencia") & vbTab
                            linha = linha & rs("agencia_dv") & vbTab
                            linha = linha & rs("conta_corrente") & vbTab
                            linha = linha & rs("conta_corrente_dv") & vbTab
                            linha = linha & rs("conta_poupanca") & vbTab
                            linha = linha & rs("conta_poupanca_dv") & vbTab
                        Else
                            linha = linha & Pgto.Voucher & vbTab
                            linha = linha & Pgto.Num_Nota_Fiscal & vbTab
                            linha = linha & Pgto.Serie_Nota_Fiscal & vbTab
                            linha = linha & rs("banco") & vbTab
                            linha = linha & rs("agencia") & vbTab
                            linha = linha & rs("agencia_dv") & vbTab
                            linha = linha & rs("conta_corrente") & vbTab
                            linha = linha & rs("conta_corrente_dv") & vbTab
                            linha = linha & rs("conta_poupanca") & vbTab
                            linha = linha & rs("conta_poupanca_dv") & vbTab
                        End If
                    End If
                Else
                    linha = linha & IIf(Pgto.dt_Aprovacao = "", "", Format(Pgto.dt_Aprovacao, "dd/mm/yyyy")) & vbTab
                    linha = linha & Pgto.Usuario & vbTab
                    linha = linha & Pgto.usuario_aprovacao & vbTab
                    If vopercoseguro = "C" Then
                        If lblNossaParteCosseguro.Visible = True Then
                            linha = linha & Pgto.Voucher & vbTab
                            linha = linha & Pgto.Num_Nota_Fiscal & vbTab
                            linha = linha & Pgto.Serie_Nota_Fiscal & vbTab
                            linha = linha & IIf(IsNull(rs("num_carta_seguro_aceito")), "", rs("num_carta_seguro_aceito")) & vbTab & Format((CCur(Pgto.val_Acerto) - Val(rs("val_cosseguro"))), "#,##0.00") & vbTab
                            linha = linha & rs("banco") & vbTab
                            linha = linha & rs("agencia") & vbTab
                            linha = linha & rs("agencia_dv") & vbTab
                            linha = linha & rs("conta_corrente") & vbTab
                            linha = linha & rs("conta_corrente_dv") & vbTab
                            linha = linha & rs("conta_poupanca") & vbTab
                            linha = linha & rs("conta_poupanca_dv") & vbTab
                        Else
                            linha = linha & Pgto.Voucher & vbTab
                            linha = linha & Pgto.Num_Nota_Fiscal & vbTab
                            linha = linha & Pgto.Serie_Nota_Fiscal & vbTab
                            linha = linha & IIf(IsNull(rs("num_carta_seguro_aceito")), "", rs("num_carta_seguro_aceito")) & vbTab
                            linha = linha & rs("banco") & vbTab
                            linha = linha & rs("agencia") & vbTab
                            linha = linha & rs("agencia_dv") & vbTab
                            linha = linha & rs("conta_corrente") & vbTab
                            linha = linha & rs("conta_corrente_dv") & vbTab
                            linha = linha & rs("conta_poupanca") & vbTab
                            linha = linha & rs("conta_poupanca_dv") & vbTab
                        End If
                    Else
                        If lblNossaParteCosseguro.Visible = True Then
                            linha = linha & Pgto.Voucher & vbTab
                            linha = linha & Pgto.Num_Nota_Fiscal & vbTab
                            linha = linha & Pgto.Serie_Nota_Fiscal & vbTab & (Pgto.val_Acerto - rs("val_cosseguro")) & vbTab & Format((CCur(Pgto.val_Acerto) - Val(rs("val_cosseguro"))), "#,##0.00") & vbTab
                            linha = linha & rs("banco") & vbTab
                            linha = linha & rs("agencia") & vbTab
                            linha = linha & rs("agencia_dv") & vbTab
                            linha = linha & rs("conta_corrente") & vbTab
                            linha = linha & rs("conta_corrente_dv") & vbTab
                            linha = linha & rs("conta_poupanca") & vbTab
                            linha = linha & rs("conta_poupanca_dv") & vbTab
                        Else
                            linha = linha & Pgto.Voucher & vbTab
                            linha = linha & Pgto.Num_Nota_Fiscal & vbTab
                            linha = linha & Pgto.Serie_Nota_Fiscal & vbTab
                            'HENRIQUE H. HENRIQUES - CONFITEC SP - INI - INC000004527610
                            'Ajuste devido a informa��o n�o estar sendo retornada no select.
                            linha = linha & vbTab
                            'Ajuste devido a informa��o n�o estar sendo retornada no select.
                            'HENRIQUE H. HENRIQUES - CONFITEC SP - FIM - INC000004527610
                            linha = linha & rs("banco") & vbTab
                            linha = linha & rs("agencia") & vbTab
                            linha = linha & rs("agencia_dv") & vbTab
                            linha = linha & rs("conta_corrente") & vbTab
                            linha = linha & rs("conta_corrente_dv") & vbTab
                            linha = linha & rs("conta_poupanca") & vbTab
                            linha = linha & rs("conta_poupanca_dv") & vbTab
                        End If
                    End If
                End If
                
                linha = linha & Pgto.ChaveOP
                
                flexPgtoSinistro.AddItem linha
                
            End If
        End If
        rs.MoveNext
    Wend
    rs.Close
    vSelecionou = False

End Sub

Private Sub MontarGrdPgtosAprovados()

    Dim Pgto As New Pgto_Sinistro
    Dim rs As rdoResultset, Rs1 As rdoResultset
    Dim SQL As String, linha As String
    Dim Sin As Sinist, Recibo As String, Agrupado As Boolean

    Set Sin = SinistroEmUso.Item(1)

    'Ricardo Toledo - Confitec : 13/12/2012 : INC000003852832 : inicio
    'Alterado para "SELECT DISTINCT, pois estava retornando registros duplicados"
    'SQL = "SELECT pgto_sinistro_tb.dt_acerto_contas_sinistro, " & _
    'Ricardo Toledo - Confitec : 13/12/2012 : INC000003852832 : fim
    SQL = "SELECT DISTINCT pgto_sinistro_tb.dt_acerto_contas_sinistro, " & _
            "val_acerto = isnull(pgto_sinistro_tb.val_acerto,0), " & _
            "val_correcao = isnull(pgto_sinistro_tb.val_correcao,0), " & _
            "pgto_sinistro_tb.item_val_estimativa, " & _
            "pgto_sinistro_tb.situacao_op, " & _
            "pgto_sinistro_tb.motivo_rejeicao, " & _
            "forma_pgto_tb.forma_pgto_id, " & _
            "nome_forma = forma_pgto_tb.nome, " & _
            "sinistro_benef_tb.beneficiario_id, " & _
            "nome_benef = sinistro_benef_tb.nome, "
            'Fabricio Brandi - Nova Consultoria : 29/09/2011
            'Adicionado o cpf para ser exibido na grid de benefici�rios
    SQL = SQL & "cpf_benef = sinistro_benef_tb.cpf, " & _
            "nome_cob = tp_cobertura_tb.nome, " & _
            "tp_objeto_tb.tp_objeto_id, " & _
            "nome_obj = tp_objeto_tb.nome, "
            
            
    SQL = SQL & "pgto_sinistro_tb.seq_estimativa, " & _
            "pgto_sinistro_tb.seq_pgto, " & _
            "pgto_sinistro_tb.num_parcela, " & _
            "pgto_sinistro_tb.dt_contabilizacao, " & _
            "pgto_sinistro_tb.dt_solicitacao_pgto, " & _
            "pgto_sinistro_tb.Usuario, " & _
            "ps_acerto_pagamento_tb.voucher_id, " & _
            "pgto_sinistro_tb.dt_avaliacao, " & _
            "pgto_sinistro_tb.usuario_avaliador, " & _
            "sinistro_estimativa_tb.num_carta_seguro_aceito, " & _
            "pgto_sinistro_tb.val_cosseguro, " & _
            "pgto_sinistro_tb.detalhamento_id, " & _
            "pgto_sinistro_tb.chave_autorizacao_sinistro,  " & _
            "pgto_sinistro_tb.num_nota_fiscal, " & _
            "pgto_sinistro_tb.serie_nota_fiscal "
            
'Ralves - 10/06/2010 - Demanda 3798421 - Dados da conta para pagamento / Inserindo dados bancarios
SQL = SQL & ", banco = ISNULL(CASE WHEN forma_pgto_tb.forma_pgto_id IN (13,14) THEN sinistro_benef_tb.banco ELSE 0 END,0)" & _
            ", agencia = ISNULL(CASE WHEN forma_pgto_tb.forma_pgto_id IN (13,14) THEN sinistro_benef_tb.agencia ELSE 0 END,0)" & _
            ", agencia_dv = ISNULL(CASE WHEN forma_pgto_tb.forma_pgto_id IN (13,14) THEN sinistro_benef_tb.agencia_dv ELSE '0' END,'0') " & _
            ", conta_corrente = ISNULL(CASE WHEN forma_pgto_tb.forma_pgto_id IN (13,14) THEN sinistro_benef_tb.conta_corrente ELSE 0 END,0) " & _
            ", conta_corrente_dv = ISNULL(CASE WHEN forma_pgto_tb.forma_pgto_id IN (13,14) THEN sinistro_benef_tb.conta_corrente_dv ELSE '0' END,'0') " & _
            ", conta_poupanca = ISNULL(CASE WHEN forma_pgto_tb.forma_pgto_id IN (14) THEN sinistro_benef_tb.conta_poupanca ELSE 0 END,0)" & _
            ", conta_poupanca_dv = ISNULL(CASE WHEN forma_pgto_tb.forma_pgto_id IN (14) THEN sinistro_benef_tb.conta_poupanca_dv ELSE '0' END,'0')"
    
    'carolina.marinho 31/08/2011 - Demanda 11104810 - M�dulo de aprova��o - Honor�rio / Despesas duplicado
    'conforme solicitado na demanda, caso o item_val_estimativa seja honorarios ou despesas, n�o considerar
    'a cobertura ao mostrar os itens no grid Pagamentos Aprovados
    
    SQL = SQL & ", pgto_sinistro_tb.beneficiario_id "
    
    'carolina.marinho fim
    
    'Alan Neves - MP - 19/03/2013 - Inicio
    'SQL = SQL & "FROM pgto_sinistro_tb with(nolock) "
    SQL = SQL & "FROM #pgto_sinistro_tb pgto_sinistro_tb with(nolock) "
    'Alan Neves - MP - 19/03/2013 - Fim
    
    SQL = SQL & _
            "INNER JOIN forma_pgto_tb with(nolock) " & _
             "ON pgto_sinistro_tb.forma_pgto_id = forma_pgto_tb.Forma_pgto_id " & _
            "INNER JOIN sinistro_benef_tb with(nolock) " & _
            "ON pgto_sinistro_tb.sinistro_id = sinistro_benef_tb.sinistro_id " & _
            "AND pgto_sinistro_tb.apolice_id = sinistro_benef_tb.apolice_id " & _
            "AND pgto_sinistro_tb.sucursal_seguradora_id = " & _
                    "sinistro_benef_tb.sucursal_seguradora_id " & _
            "AND pgto_sinistro_tb.seguradora_cod_susep = " & _
                    "sinistro_benef_tb.seguradora_cod_susep " & _
            "AND pgto_sinistro_tb.ramo_id = sinistro_benef_tb.ramo_id " & _
            "AND pgto_sinistro_tb.beneficiario_id = sinistro_benef_tb.beneficiario_id "
            
                        
    SQL = SQL & "LEFT OUTER JOIN (pgto_sinistro_cobertura_tb with(nolock) " & _
            "       INNER JOIN tp_cobertura_tb with(nolock) ON tp_cobertura_tb.tp_cobertura_id = pgto_sinistro_cobertura_tb.tp_cobertura_id " & _
            "       INNER JOIN tp_objeto_tb with(nolock) ON tp_objeto_tb.tp_objeto_id = pgto_sinistro_cobertura_tb.tp_objeto_id " & _
            "       ) ON " & _
            "     pgto_sinistro_tb.sinistro_id = pgto_sinistro_cobertura_tb.sinistro_id " & _
            " AND pgto_sinistro_tb.apolice_id = pgto_sinistro_cobertura_tb.apolice_id " & _
            " AND pgto_sinistro_tb.sucursal_seguradora_id = pgto_sinistro_cobertura_tb.sucursal_seguradora_id " & _
            " AND pgto_sinistro_tb.seguradora_cod_susep = pgto_sinistro_cobertura_tb.seguradora_cod_susep " & _
            " AND pgto_sinistro_tb.ramo_id = pgto_sinistro_cobertura_tb.ramo_id " & _
            " AND pgto_sinistro_tb.item_val_estimativa = pgto_sinistro_cobertura_tb.item_val_estimativa " & _
            " AND pgto_sinistro_tb.seq_estimativa = pgto_sinistro_cobertura_tb.seq_estimativa " & _
            " AND pgto_sinistro_tb.seq_pgto = pgto_sinistro_cobertura_tb.seq_pgto " & _
            " AND pgto_sinistro_tb.num_parcela = pgto_sinistro_cobertura_tb.num_parcela " & _
            " AND pgto_sinistro_tb.beneficiario_id = pgto_sinistro_cobertura_tb.beneficiario_id "
            
    'Alan Neves - MP - 19/03/2013 - Inicio - Leitura na temp
    'SQL = SQL & " INNER JOIN sinistro_estimativa_tb with(nolock) " &
     SQL = SQL & " INNER JOIN #sinistro_estimativa_tb sinistro_estimativa_tb " & _
            "ON pgto_sinistro_tb.sinistro_id = sinistro_estimativa_tb.sinistro_id " & _
            "AND pgto_sinistro_tb.apolice_id = sinistro_estimativa_tb.apolice_id " & _
            "AND pgto_sinistro_tb.sucursal_seguradora_id = " & _
                    "sinistro_estimativa_tb.sucursal_seguradora_id " & _
            "AND pgto_sinistro_tb.seguradora_cod_susep = " & _
                    "sinistro_estimativa_tb.seguradora_cod_susep " & _
            "AND pgto_sinistro_tb.ramo_id = sinistro_estimativa_tb.ramo_id " & _
            "AND pgto_sinistro_tb.seq_estimativa = sinistro_estimativa_tb.seq_estimativa " & _
            "AND pgto_sinistro_tb.item_val_estimativa = sinistro_estimativa_tb.item_val_estimativa "
            
            
            
    SQL = SQL & "LEFT OUTER JOIN ps_acerto_pagamento_tb with(nolock) ON pgto_sinistro_tb.acerto_id = ps_acerto_pagamento_tb.acerto_id " & _
            "WHERE pgto_sinistro_tb.sinistro_id = " & Sin.sinistro_id & _
            " AND pgto_sinistro_tb.apolice_id = " & Sin.Apolice_id & _
            " AND pgto_sinistro_tb.sucursal_seguradora_id = " & Sin.Sucursal_id & _
            " AND pgto_sinistro_tb.seguradora_cod_susep = " & Sin.Seguradora_id & _
            " AND pgto_sinistro_tb.ramo_id = " & Sin.ramo_id
    'SQL = SQL & " AND pgto_sinistro_tb.val_resseguro_recuperado IS NULL "
    SQL = SQL & " AND pgto_sinistro_tb.num_parcela = 1 "
    
    If (voper = "C") Or (voper = "A") Then
        SQL = SQL & " AND ((pgto_sinistro_tb.tp_operacao = 'd') OR "
        SQL = SQL & "      (pgto_sinistro_tb.tp_operacao = 'c' and pgto_sinistro_tb.item_val_estimativa in (4,6) )) "
    Else
        SQL = SQL & " AND pgto_sinistro_tb.tp_operacao = 'd' "
    End If
    
    SQL = SQL & " AND pgto_sinistro_tb.situacao_op = 'a' "

    If frmAcertoCon.cmbCampo.Text = "Voucher" Then
        SQL = SQL & " AND ps_acerto_pagamento_tb.voucher_id = " & frmAcertoCon.txtConteudo
    End If
    
    'carolina.marinho 31/08/2011 - Demanda 11104810 - M�dulo de aprova��o - Honor�rio / Despesas duplicado
    'conforme solicitado na demanda, caso o item_val_estimativa seja honorarios ou despesas, n�o considerar
    'a cobertura ao mostrar os itens no grid Pagamentos Aprovados
    SQL = SQL & " AND pgto_sinistro_tb.item_val_estimativa NOT IN (2,3) "

    SQL = SQL & "UNION ALL "

    SQL = SQL & "SELECT DISTINCT pgto_sinistro_tb.dt_acerto_contas_sinistro, " & _
            "val_acerto = isnull(pgto_sinistro_tb.val_acerto,0), " & _
            "val_correcao = isnull(pgto_sinistro_tb.val_correcao,0), " & _
            "pgto_sinistro_tb.item_val_estimativa, " & _
            "pgto_sinistro_tb.situacao_op, " & _
            "pgto_sinistro_tb.motivo_rejeicao, " & _
            "forma_pgto_tb.forma_pgto_id, " & _
            "nome_forma = forma_pgto_tb.nome, " & _
            "sinistro_benef_tb.beneficiario_id, " & _
            "nome_benef = sinistro_benef_tb.nome, "
            'Fabricio Brandi - Nova Consultoria : 11/10/2011
            'Adicionado o cpf para ser exibido na grid de benefici�rios'
      SQL = SQL & "cpf_benef = sinistro_benef_tb.CPF, " & _
            "nome_cob = '', " & _
            "tp_objeto_tb.tp_objeto_id, " & _
            "nome_obj = tp_objeto_tb.nome, "

    SQL = SQL & "pgto_sinistro_tb.seq_estimativa, " & _
            "pgto_sinistro_tb.seq_pgto, " & _
            "pgto_sinistro_tb.num_parcela, " & _
            "pgto_sinistro_tb.dt_contabilizacao, " & _
            "pgto_sinistro_tb.dt_solicitacao_pgto, " & _
            "pgto_sinistro_tb.Usuario, " & _
            "ps_acerto_pagamento_tb.voucher_id, " & _
            "pgto_sinistro_tb.dt_avaliacao, " & _
            "pgto_sinistro_tb.usuario_avaliador, " & _
            "sinistro_estimativa_tb.num_carta_seguro_aceito, " & _
            "pgto_sinistro_tb.val_cosseguro, " & _
            "pgto_sinistro_tb.detalhamento_id, " & _
            "pgto_sinistro_tb.chave_autorizacao_sinistro,  " & _
            "pgto_sinistro_tb.num_nota_fiscal, " & _
            "pgto_sinistro_tb.serie_nota_fiscal "

'Ralves - 10/06/2010 - Demanda 3798421 - Dados da conta para pagamento / Inserindo dados bancarios
SQL = SQL & ", banco = ISNULL(CASE WHEN forma_pgto_tb.forma_pgto_id IN (13,14) THEN sinistro_benef_tb.banco ELSE 0 END,0)" & _
            ", agencia = ISNULL(CASE WHEN forma_pgto_tb.forma_pgto_id IN (13,14) THEN sinistro_benef_tb.agencia ELSE 0 END,0)" & _
            ", agencia_dv = ISNULL(CASE WHEN forma_pgto_tb.forma_pgto_id IN (13,14) THEN sinistro_benef_tb.agencia_dv ELSE '0' END,'0') " & _
            ", conta_corrente = ISNULL(CASE WHEN forma_pgto_tb.forma_pgto_id IN (13,14) THEN sinistro_benef_tb.conta_corrente ELSE 0 END,0) " & _
            ", conta_corrente_dv = ISNULL(CASE WHEN forma_pgto_tb.forma_pgto_id IN (13,14) THEN sinistro_benef_tb.conta_corrente_dv ELSE '0' END,'0') " & _
            ", conta_poupanca = ISNULL(CASE WHEN forma_pgto_tb.forma_pgto_id IN (14) THEN sinistro_benef_tb.conta_poupanca ELSE 0 END,0)" & _
            ", conta_poupanca_dv = ISNULL(CASE WHEN forma_pgto_tb.forma_pgto_id IN (14) THEN sinistro_benef_tb.conta_poupanca_dv ELSE '0' END,'0')"

    SQL = SQL & ", pgto_sinistro_tb.beneficiario_id "
    
    'Alan Neves - MP - 19/03/2013 - Inicio
    'SQL = SQL & "FROM pgto_sinistro_tb with(nolock) "
    SQL = SQL & "FROM #pgto_sinistro_tb pgto_sinistro_tb with(nolock) "
    'Alan Neves - MP - 19/03/2013 - Inicio
    
    SQL = SQL & _
            "INNER JOIN forma_pgto_tb with(nolock) " & _
             "ON pgto_sinistro_tb.forma_pgto_id = forma_pgto_tb.Forma_pgto_id " & _
            "INNER JOIN sinistro_benef_tb with(nolock) " & _
            "ON pgto_sinistro_tb.sinistro_id = sinistro_benef_tb.sinistro_id " & _
            "AND pgto_sinistro_tb.apolice_id = sinistro_benef_tb.apolice_id " & _
            "AND pgto_sinistro_tb.sucursal_seguradora_id = " & _
                    "sinistro_benef_tb.sucursal_seguradora_id " & _
            "AND pgto_sinistro_tb.seguradora_cod_susep = " & _
                    "sinistro_benef_tb.seguradora_cod_susep " & _
            "AND pgto_sinistro_tb.ramo_id = sinistro_benef_tb.ramo_id " & _
            "AND pgto_sinistro_tb.beneficiario_id = sinistro_benef_tb.beneficiario_id "
    SQL = SQL & "LEFT OUTER JOIN (pgto_sinistro_cobertura_tb with(nolock) " & _
            "       INNER JOIN tp_cobertura_tb with(nolock) ON tp_cobertura_tb.tp_cobertura_id = pgto_sinistro_cobertura_tb.tp_cobertura_id " & _
            "       INNER JOIN tp_objeto_tb with(nolock) ON tp_objeto_tb.tp_objeto_id = pgto_sinistro_cobertura_tb.tp_objeto_id " & _
            "       ) ON " & _
            "     pgto_sinistro_tb.sinistro_id = pgto_sinistro_cobertura_tb.sinistro_id " & _
            " AND pgto_sinistro_tb.apolice_id = pgto_sinistro_cobertura_tb.apolice_id " & _
            " AND pgto_sinistro_tb.sucursal_seguradora_id = pgto_sinistro_cobertura_tb.sucursal_seguradora_id " & _
            " AND pgto_sinistro_tb.seguradora_cod_susep = pgto_sinistro_cobertura_tb.seguradora_cod_susep " & _
            " AND pgto_sinistro_tb.ramo_id = pgto_sinistro_cobertura_tb.ramo_id " & _
            " AND pgto_sinistro_tb.item_val_estimativa = pgto_sinistro_cobertura_tb.item_val_estimativa " & _
            " AND pgto_sinistro_tb.seq_estimativa = pgto_sinistro_cobertura_tb.seq_estimativa " & _
            " AND pgto_sinistro_tb.seq_pgto = pgto_sinistro_cobertura_tb.seq_pgto " & _
            " AND pgto_sinistro_tb.num_parcela = pgto_sinistro_cobertura_tb.num_parcela " & _
            " AND pgto_sinistro_tb.beneficiario_id = pgto_sinistro_cobertura_tb.beneficiario_id "
     
    'Alan Neves - MP - 19/03/2013 - Inicio - Leitura na temp
    'SQL = SQL & " INNER JOIN sinistro_estimativa_tb with(nolock) " &
     SQL = SQL & " INNER JOIN #sinistro_estimativa_tb sinistro_estimativa_tb " & _
            "ON pgto_sinistro_tb.sinistro_id = sinistro_estimativa_tb.sinistro_id " & _
            "AND pgto_sinistro_tb.apolice_id = sinistro_estimativa_tb.apolice_id " & _
            "AND pgto_sinistro_tb.sucursal_seguradora_id = " & _
                    "sinistro_estimativa_tb.sucursal_seguradora_id " & _
            "AND pgto_sinistro_tb.seguradora_cod_susep = " & _
                    "sinistro_estimativa_tb.seguradora_cod_susep " & _
            "AND pgto_sinistro_tb.ramo_id = sinistro_estimativa_tb.ramo_id " & _
            "AND pgto_sinistro_tb.seq_estimativa = sinistro_estimativa_tb.seq_estimativa " & _
            "AND pgto_sinistro_tb.item_val_estimativa = sinistro_estimativa_tb.item_val_estimativa "
    SQL = SQL & "LEFT OUTER JOIN ps_acerto_pagamento_tb with(nolock) ON pgto_sinistro_tb.acerto_id = ps_acerto_pagamento_tb.acerto_id " & _
            "WHERE pgto_sinistro_tb.sinistro_id = " & Sin.sinistro_id & _
            " AND pgto_sinistro_tb.apolice_id = " & Sin.Apolice_id & _
            " AND pgto_sinistro_tb.sucursal_seguradora_id = " & Sin.Sucursal_id & _
            " AND pgto_sinistro_tb.seguradora_cod_susep = " & Sin.Seguradora_id & _
            " AND pgto_sinistro_tb.ramo_id = " & Sin.ramo_id
    'SQL = SQL & " AND pgto_sinistro_tb.val_resseguro_recuperado IS NULL "
    SQL = SQL & " AND pgto_sinistro_tb.num_parcela = 1 "

    If (voper = "C") Or (voper = "A") Then
        SQL = SQL & " AND ((pgto_sinistro_tb.tp_operacao = 'd') OR "
        SQL = SQL & "      (pgto_sinistro_tb.tp_operacao = 'c' and pgto_sinistro_tb.item_val_estimativa in (4,6) )) "
    Else
        SQL = SQL & " AND pgto_sinistro_tb.tp_operacao = 'd' "
    End If

    SQL = SQL & " AND pgto_sinistro_tb.situacao_op = 'a' "

    If frmAcertoCon.cmbCampo.Text = "Voucher" Then
        SQL = SQL & " AND ps_acerto_pagamento_tb.voucher_id = " & frmAcertoCon.txtConteudo
    End If

    SQL = SQL & " AND pgto_sinistro_tb.item_val_estimativa IN (2,3) "
    'carolina.marinho fim
    
    SQL = SQL & " ORDER BY pgto_sinistro_tb.dt_acerto_contas_sinistro, " & _
                    "pgto_sinistro_tb.beneficiario_id, pgto_sinistro_tb.item_val_estimativa"
    Set rs = rdocn1.OpenResultset(SQL)
    
    grdPagtosAprovados.Rows = 1
    
    While Not rs.EOF
        
        Agrupado = False
        If (vopercoseguro = "C") And (Not IsNull(rs("chave_autorizacao_sinistro"))) Then
            'Alan Neves - MP - 11/03/2013 - Inicio
            'SQL = "SELECT count(*) "
            SQL = "SELECT count(1) "
            'Alan Neves - MP - 11/03/2013 - Fim
            
            'Alan Neves - MP - 19/03/2013 - Inicio
            'SQL = SQL & " FROM pgto_sinistro_tb with(nolock) "
            SQL = SQL & " FROM pgto_sinistro_tb  with(nolock) "
            'Alan Neves - MP - 19/03/2013 - Fim
            SQL = SQL & " WHERE chave_autorizacao_sinistro = " & rs("chave_autorizacao_sinistro")
            'SQL = SQL & " AND   val_resseguro_recuperado IS NULL"
            SQL = SQL & " AND num_parcela = 1 "
            Set Rs1 = rdocn.OpenResultset(SQL)
            If Rs1(0) > 1 Then
                Agrupado = True
            End If
            Rs1.Close
        End If
        
        'Popula propriedades do componente Pgto
        If (Not Agrupado) Or (voper = "C") Then
            Set Pgto = New Pgto_Sinistro
            With Pgto
                .dt_Acerto_Contas = Format(rs("dt_acerto_contas_sinistro"), "dd/mm/yyyy")
                .val_Acerto = Format(Val(rs("val_acerto")), "#,##0.00")
                .val_Correcao = Format(Val(rs("val_correcao")), "#,##0.00")
                .Item_val_estimativa = rs("item_val_estimativa")
                If IsNull(rs("chave_autorizacao_sinistro")) Then
                    .ChaveOP = ""
                Else
                    .ChaveOP = rs("chave_autorizacao_sinistro")
                End If
                
                Recibo = ""
                If Not IsNull(rs("detalhamento_id")) Then
                    SQL = "SELECT linha "
                    SQL = SQL & " FROM sinistro_linha_detalhamento_tb with(nolock) "
                    SQL = SQL & " WHERE sinistro_id = " & Sin.sinistro_id
                    SQL = SQL & "   AND apolice_id = " & Sin.Apolice_id
                    SQL = SQL & "   AND seguradora_cod_susep = " & Sin.Seguradora_id
                    SQL = SQL & "   AND sucursal_seguradora_id = " & Sin.Sucursal_id
                    SQL = SQL & "   AND ramo_id = " & Sin.ramo_id
                    SQL = SQL & "   AND detalhamento_id = " & rs("detalhamento_id")
                    
                    Set Rs1 = rdocn.OpenResultset(SQL)
                    While Not Rs1.EOF
                        Recibo = Recibo & "" & Rs1(0) & vbNewLine
                        Rs1.MoveNext
                    Wend
                    Rs1.Close
                End If
                .Recibo = Recibo
                
'+-----------------------------------------------------------
'| 19406709 - Melhoria nos limites de sinistro e emissao
'| RN05 - petrauskas - 03/2017
                If LCase(rs("situacao_op")) = "a" And rs("item_val_estimativa") = 1 Then
                    val_indenizacao_pago = val_indenizacao_pago + Pgto.val_Acerto
                End If
'| 19406709 - Melhoria nos limites de sinistro e emissao
'+-----------------------------------------------------------
                
                
                'msalema - 16/05/2013
                'Select Case rs("situacao_op")
                Select Case LCase(rs("situacao_op"))
                'msalema - 16/05/2013
                    Case "a": .Situacao_OP = "APROVADO"
                    Case "r": .Situacao_OP = "REJEITADO"
                    Case "c": .Situacao_OP = "CANCELADO"
                    Case "e": .Situacao_OP = "ESTORNADO"
                    Case "n": .Situacao_OP = "AGUARD. APROVA��O"
                End Select
                If IsNull(rs("motivo_rejeicao")) = True Then
                    .Motivo_Rejeicao = ""
                Else
                    .Motivo_Rejeicao = rs("motivo_rejeicao")
                End If
                .Forma_Pagto_id = rs("forma_pgto_id")
                .Forma_Pagto = rs("nome_forma")
                If IsNull(rs("nome_cob")) = True Then
                    .Nome_Cob = ""
                Else
                    .Nome_Cob = rs("nome_cob")
                End If
                If IsNull(rs("nome_obj")) = True Then
                    .Nome_Obj = ""
                Else
                    .Nome_Obj = rs("nome_obj")
                End If
                .Beneficiario_id = rs("beneficiario_id")
                If IsNull(rs("nome_benef")) = True Then
                    .Beneficiario_Nome = ""
                Else
                    .Beneficiario_Nome = rs("nome_benef")
                End If
                .Seq_Estimativa = rs("seq_estimativa")
                .Seq_Pgto = rs("seq_pgto")
                .Num_Parcela = rs("num_parcela")
                If IsNull(rs("dt_contabilizacao")) = True Then
                    .dt_contabilizacao = ""
                Else
                    .dt_contabilizacao = rs("dt_contabilizacao")
                End If
                .dt_emissao = rs("dt_solicitacao_pgto")
                If IsNull(rs("usuario")) = True Then
                    .Usuario = ""
                Else
                    .Usuario = rs("usuario")
                End If
                If IsNull(rs("voucher_id")) = True Then
                    .Voucher = ""
                Else
                    .Voucher = rs("voucher_id")
                End If
                
                .Num_Nota_Fiscal = IIf(IsNull(rs("num_nota_fiscal")), "", rs("num_nota_fiscal"))
                .Serie_Nota_Fiscal = IIf(IsNull(rs("serie_nota_fiscal")), "", rs("serie_nota_fiscal"))
                
                If IsNull(rs("dt_avaliacao")) = True Then
                    .dt_Aprovacao = ""
                Else
                    .dt_Aprovacao = rs("dt_avaliacao")
                End If
                If IsNull(rs("usuario_avaliador")) = True Then
                    .usuario_aprovacao = ""
                Else
                    .usuario_aprovacao = rs("usuario_avaliador")
                End If
            End With
            
            grdPagtosAprovados.Row = grdPagtosAprovados.Rows - 1
            linha = Format(rs("dt_acerto_contas_sinistro"), "dd/mm/yyyy") & vbTab
            'msalema - 16/05/2013
            'Select Case rs("situacao_op")
            Select Case LCase(rs("situacao_op"))
            'msalema - 16/05/2013
                Case "a": linha = linha & "APROVADO" & vbTab
                Case "r": linha = linha & "REJEITADO" & vbTab
                Case "c": linha = linha & "CANCELADO" & vbTab
                Case "e": linha = linha & "ESTORNADO" & vbTab
                Case "n": linha = linha & "AGUARD. APROVA��O" & vbTab
            End Select
            Select Case rs("item_val_estimativa")
                Case 1
                    linha = linha & "INDENIZA��O" & vbTab
                Case 2
                    linha = linha & "HONOR�RIOS" & vbTab
                Case 3
                    linha = linha & "DESPESAS" & vbTab
                Case 4
                    linha = linha & "RESSARCIMENTO" & vbTab
                Case 5
                    linha = linha & "DESP. RESSARCIMENTO" & vbTab
                Case 6
                    linha = linha & "SALVADOS" & vbTab
                Case 7
                    linha = linha & "DESP. SALVADOS" & vbTab
            End Select
            linha = linha & rs("nome_cob") & vbTab
            linha = linha & IIf(rs("tp_objeto_id") = 0, "", rs("nome_obj")) & vbTab
            linha = linha & Pgto.val_Acerto & vbTab
            linha = linha & Pgto.val_Correcao & vbTab
            linha = linha & rs("nome_forma") & vbTab
            linha = linha & rs("nome_benef") & vbTab
                        'Fabricio Brandi - Nova Consultoria : 29/09/2011
                        'Adicionado o cpf para ser exibido na grid de benefici�rios
            linha = linha & rs("cpf_benef") & vbTab
            '----------------------- FIM -------------------------
            If voper = "I" Or voper = "IS" Then
                linha = linha & Format(rs("dt_solicitacao_pgto"), "dd/mm/yyyy") & vbTab
                linha = linha & Pgto.Usuario & vbTab
'Ralves - 10/06/2010 - Demanda 3798421 - Dados da conta para pagamento / Inserindo dados bancarios
                If vopercoseguro = "C" Then
                    If lblNossaParteCosseguro.Visible = True Then
                        linha = linha & Pgto.Voucher & vbTab
                        linha = linha & Pgto.Num_Nota_Fiscal & vbTab
                        linha = linha & Pgto.Serie_Nota_Fiscal & vbTab
                        linha = linha & IIf(IsNull(rs("num_carta_seguro_aceito")), "", rs("num_carta_seguro_aceito")) & vbTab & Format((CCur(Pgto.val_Acerto) - Val(rs("val_cosseguro"))), "#,##0.00") & vbTab
                        linha = linha & rs("banco") & vbTab
                        linha = linha & rs("agencia") & vbTab
                        linha = linha & rs("agencia_dv") & vbTab
                        linha = linha & rs("conta_corrente") & vbTab
                        linha = linha & rs("conta_corrente_dv") & vbTab
                        linha = linha & rs("conta_poupanca") & vbTab
                        linha = linha & rs("conta_poupanca_dv") & vbTab
                    Else
                        linha = linha & Pgto.Voucher & vbTab
                        linha = linha & Pgto.Num_Nota_Fiscal & vbTab
                        linha = linha & Pgto.Serie_Nota_Fiscal & vbTab
                        linha = linha & IIf(IsNull(rs("num_carta_seguro_aceito")), "", rs("num_carta_seguro_aceito")) & vbTab
                        linha = linha & rs("banco") & vbTab
                        linha = linha & rs("agencia") & vbTab
                        linha = linha & rs("agencia_dv") & vbTab
                        linha = linha & rs("conta_corrente") & vbTab
                        linha = linha & rs("conta_corrente_dv") & vbTab
                        linha = linha & rs("conta_poupanca") & vbTab
                        linha = linha & rs("conta_poupanca_dv") & vbTab
                    End If
                Else
                    If lblNossaParteCosseguro.Visible = True Then
                        linha = linha & Pgto.Voucher & vbTab
                        linha = linha & Pgto.Num_Nota_Fiscal & vbTab
                        linha = linha & Pgto.Serie_Nota_Fiscal & vbTab & Format((CCur(Pgto.val_Acerto) - Val(rs("val_cosseguro"))), "#,##0.00") & vbTab
                        linha = linha & rs("banco") & vbTab
                        linha = linha & rs("agencia") & vbTab
                        linha = linha & rs("agencia_dv") & vbTab
                        linha = linha & rs("conta_corrente") & vbTab
                        linha = linha & rs("conta_corrente_dv") & vbTab
                        linha = linha & rs("conta_poupanca") & vbTab
                        linha = linha & rs("conta_poupanca_dv") & vbTab
                    Else
                        linha = linha & Pgto.Voucher & vbTab
                        linha = linha & Pgto.Num_Nota_Fiscal & vbTab
                        linha = linha & Pgto.Serie_Nota_Fiscal & vbTab
                        linha = linha & rs("banco") & vbTab
                        linha = linha & rs("agencia") & vbTab
                        linha = linha & rs("agencia_dv") & vbTab
                        linha = linha & rs("conta_corrente") & vbTab
                        linha = linha & rs("conta_corrente_dv") & vbTab
                        linha = linha & rs("conta_poupanca") & vbTab
                        linha = linha & rs("conta_poupanca_dv") & vbTab
                    End If
                End If
            Else
                linha = linha & IIf(Pgto.dt_Aprovacao = "", "", Format(Pgto.dt_Aprovacao, "dd/mm/yyyy")) & vbTab
                linha = linha & Pgto.Usuario & vbTab
                linha = linha & Pgto.usuario_aprovacao & vbTab
                If vopercoseguro = "C" Then
                    If lblNossaParteCosseguro.Visible = True Then
                        linha = linha & Pgto.Voucher & vbTab
                        linha = linha & Pgto.Num_Nota_Fiscal & vbTab
                        linha = linha & Pgto.Serie_Nota_Fiscal & vbTab
                        linha = linha & IIf(IsNull(rs("num_carta_seguro_aceito")), "", rs("num_carta_seguro_aceito")) & vbTab & Format((CCur(Pgto.val_Acerto) - Val(rs("val_cosseguro"))), "#,##0.00") & vbTab
                        linha = linha & rs("banco") & vbTab
                        linha = linha & rs("agencia") & vbTab
                        linha = linha & rs("agencia_dv") & vbTab
                        linha = linha & rs("conta_corrente") & vbTab
                        linha = linha & rs("conta_corrente_dv") & vbTab
                        linha = linha & rs("conta_poupanca") & vbTab
                        linha = linha & rs("conta_poupanca_dv") & vbTab
                    Else
                        linha = linha & Pgto.Voucher & vbTab
                        linha = linha & Pgto.Num_Nota_Fiscal & vbTab
                        linha = linha & Pgto.Serie_Nota_Fiscal & vbTab
                        linha = linha & IIf(IsNull(rs("num_carta_seguro_aceito")), "", rs("num_carta_seguro_aceito")) & vbTab
                        linha = linha & rs("banco") & vbTab
                        linha = linha & rs("agencia") & vbTab
                        linha = linha & rs("agencia_dv") & vbTab
                        linha = linha & rs("conta_corrente") & vbTab
                        linha = linha & rs("conta_corrente_dv") & vbTab
                        linha = linha & rs("conta_poupanca") & vbTab
                        linha = linha & rs("conta_poupanca_dv") & vbTab
                    End If
                Else
                    If lblNossaParteCosseguro.Visible = True Then
                        linha = linha & Pgto.Voucher & vbTab
                        linha = linha & Pgto.Num_Nota_Fiscal & vbTab
                        linha = linha & Pgto.Serie_Nota_Fiscal & vbTab & (Pgto.val_Acerto - rs("val_cosseguro")) & vbTab & Format((CCur(Pgto.val_Acerto) - Val(rs("val_cosseguro"))), "#,##0.00") & vbTab
                        linha = linha & rs("banco") & vbTab
                        linha = linha & rs("agencia") & vbTab
                        linha = linha & rs("agencia_dv") & vbTab
                        linha = linha & rs("conta_corrente") & vbTab
                        linha = linha & rs("conta_corrente_dv") & vbTab
                        linha = linha & rs("conta_poupanca") & vbTab
                        linha = linha & rs("conta_poupanca_dv") & vbTab
                    Else
                        linha = linha & Pgto.Voucher & vbTab
                        linha = linha & Pgto.Num_Nota_Fiscal & vbTab
                        linha = linha & Pgto.Serie_Nota_Fiscal & vbTab
                        linha = linha & rs("banco") & vbTab
                        linha = linha & rs("agencia") & vbTab
                        linha = linha & rs("agencia_dv") & vbTab
                        linha = linha & rs("conta_corrente") & vbTab
                        linha = linha & rs("conta_corrente_dv") & vbTab
                        linha = linha & rs("conta_poupanca") & vbTab
                        linha = linha & rs("conta_poupanca_dv") & vbTab
                    End If
                End If
            End If
            
            linha = linha & Pgto.ChaveOP
            
            grdPagtosAprovados.AddItem linha
            
        End If
        rs.MoveNext
    Wend
    rs.Close
    vSelecionou = False

End Sub

Private Sub Monta_Grid_Autorizacao()
    Dim Pgto As New Pgto_Sinistro, val_total As Double
    Dim rs As rdoResultset, Rs1 As rdoResultset
    Dim SQL As String, linha As String
    Dim Sin As Sinist, Recibo As String
    
    'Ricardo Toledo (Confitec) : INC000004192958 : 10/10/2013 : Inicio
    'Incluido o distinct, pois, estavam retornando registros duplicados.
    'SQL = "SELECT pgto_sinistro_tb.sinistro_id, pgto_sinistro_tb.apolice_id, "
    SQL = "SELECT DISTINCT pgto_sinistro_tb.sinistro_id, pgto_sinistro_tb.apolice_id, "
    'Ricardo Toledo (Confitec) : INC000004192958 : 10/10/2013 : Fim
    
    SQL = SQL & " pgto_sinistro_tb.sucursal_seguradora_id, pgto_sinistro_tb.seguradora_cod_susep, "
    SQL = SQL & " pgto_sinistro_tb.ramo_id, "
    SQL = SQL & "pgto_sinistro_tb.dt_acerto_contas_sinistro, " & _
            "val_acerto = isnull(pgto_sinistro_tb.val_acerto,0), " & _
            "val_correcao = isnull(pgto_sinistro_tb.val_correcao,0), " & _
            "pgto_sinistro_tb.item_val_estimativa, " & _
            "pgto_sinistro_tb.situacao_op, " & _
            "pgto_sinistro_tb.motivo_rejeicao, " & _
            "forma_pgto_tb.forma_pgto_id, " & _
            "nome_forma = forma_pgto_tb.nome, " & _
            "sinistro_benef_tb.beneficiario_id, " & _
            "nome_benef = sinistro_benef_tb.nome, " & _
            "nome_cob = tp_cobertura_tb.nome, " & _
            "tp_objeto_tb.tp_objeto_id, " & _
            "nome_obj = tp_objeto_tb.nome, " & _
            "pgto_sinistro_tb.seq_estimativa, " & _
            "pgto_sinistro_tb.seq_pgto, " & _
            "pgto_sinistro_tb.num_parcela, " & _
            "pgto_sinistro_tb.dt_contabilizacao, " & _
            "pgto_sinistro_tb.dt_solicitacao_pgto, " & _
            "pgto_sinistro_tb.Usuario, " & _
            "ps_acerto_pagamento_tb.voucher_id, " & _
            "pgto_sinistro_tb.dt_avaliacao, " & _
            "pgto_sinistro_tb.usuario_avaliador, " & _
            "sinistro_estimativa_tb.num_carta_seguro_aceito, " & _
            "pgto_sinistro_tb.val_cosseguro, " & _
            "pgto_sinistro_tb.detalhamento_id, "
    SQL = SQL & "sit_sinistro = sinistro_tb.situacao "
    
    'Alan Neves - MP - 19/03/2013 - Inicio - Alterado para temp
    'SQL = SQL & "FROM pgto_sinistro_tb with(nolock) " &
    'Alan Neves - MP - 19/03/2013 - Inicio
    SQL = SQL & "FROM #pgto_sinistro_tb pgto_sinistro_tb with(nolock) " & _
            "INNER JOIN sinistro_tb with(nolock) " & _
            "ON pgto_sinistro_tb.sinistro_id = sinistro_tb.sinistro_id " & _
            "AND pgto_sinistro_tb.apolice_id = sinistro_tb.apolice_id " & _
            "AND pgto_sinistro_tb.sucursal_seguradora_id = " & _
                    "sinistro_tb.sucursal_seguradora_id " & _
            "AND pgto_sinistro_tb.seguradora_cod_susep = " & _
                    "sinistro_tb.seguradora_cod_susep " & _
            "AND pgto_sinistro_tb.ramo_id = sinistro_tb.ramo_id " & _
            "INNER JOIN forma_pgto_tb with(nolock) " & _
             "ON pgto_sinistro_tb.forma_pgto_id = forma_pgto_tb.Forma_pgto_id " & _
            "INNER JOIN sinistro_benef_tb with(nolock) " & _
            "ON pgto_sinistro_tb.sinistro_id = sinistro_benef_tb.sinistro_id " & _
            "AND pgto_sinistro_tb.apolice_id = sinistro_benef_tb.apolice_id " & _
            "AND pgto_sinistro_tb.sucursal_seguradora_id = " & _
                    "sinistro_benef_tb.sucursal_seguradora_id " & _
            "AND pgto_sinistro_tb.seguradora_cod_susep = " & _
                    "sinistro_benef_tb.seguradora_cod_susep " & _
            "AND pgto_sinistro_tb.ramo_id = sinistro_benef_tb.ramo_id " & _
            "AND pgto_sinistro_tb.beneficiario_id = sinistro_benef_tb.beneficiario_id "
    SQL = SQL & "LEFT OUTER JOIN (pgto_sinistro_cobertura_tb with(nolock) " & _
            "       INNER JOIN tp_cobertura_tb with(nolock) ON tp_cobertura_tb.tp_cobertura_id = pgto_sinistro_cobertura_tb.tp_cobertura_id " & _
            "       INNER JOIN tp_objeto_tb with(nolock) ON tp_objeto_tb.tp_objeto_id = pgto_sinistro_cobertura_tb.tp_objeto_id " & _
            "       ) ON " & _
            "     pgto_sinistro_tb.sinistro_id = pgto_sinistro_cobertura_tb.sinistro_id " & _
            " AND pgto_sinistro_tb.apolice_id = pgto_sinistro_cobertura_tb.apolice_id " & _
            " AND pgto_sinistro_tb.sucursal_seguradora_id = pgto_sinistro_cobertura_tb.sucursal_seguradora_id " & _
            " AND pgto_sinistro_tb.seguradora_cod_susep = pgto_sinistro_cobertura_tb.seguradora_cod_susep " & _
            " AND pgto_sinistro_tb.ramo_id = pgto_sinistro_cobertura_tb.ramo_id " & _
            " AND pgto_sinistro_tb.item_val_estimativa = pgto_sinistro_cobertura_tb.item_val_estimativa " & _
            " AND pgto_sinistro_tb.seq_estimativa = pgto_sinistro_cobertura_tb.seq_estimativa " & _
            " AND pgto_sinistro_tb.seq_pgto = pgto_sinistro_cobertura_tb.seq_pgto " & _
            " AND pgto_sinistro_tb.num_parcela = pgto_sinistro_cobertura_tb.num_parcela " & _
            " AND pgto_sinistro_tb.beneficiario_id = pgto_sinistro_cobertura_tb.beneficiario_id "
     
    'Alan Neves - MP - 19/03/2013 - Inicio - Leitura na temp
    'SQL = SQL & " INNER JOIN sinistro_estimativa_tb with(nolock) " &
     SQL = SQL & " INNER JOIN #sinistro_estimativa_tb sinistro_estimativa_tb " & _
            "ON pgto_sinistro_tb.sinistro_id = sinistro_estimativa_tb.sinistro_id " & _
            "AND pgto_sinistro_tb.apolice_id = sinistro_estimativa_tb.apolice_id " & _
            "AND pgto_sinistro_tb.sucursal_seguradora_id = " & _
                    "sinistro_estimativa_tb.sucursal_seguradora_id " & _
            "AND pgto_sinistro_tb.seguradora_cod_susep = " & _
                    "sinistro_estimativa_tb.seguradora_cod_susep " & _
            "AND pgto_sinistro_tb.ramo_id = sinistro_estimativa_tb.ramo_id " & _
            "AND pgto_sinistro_tb.seq_estimativa = sinistro_estimativa_tb.seq_estimativa " & _
            "AND pgto_sinistro_tb.item_val_estimativa = sinistro_estimativa_tb.item_val_estimativa "
    SQL = SQL & "LEFT OUTER JOIN ps_acerto_pagamento_tb with(nolock) ON pgto_sinistro_tb.acerto_id = ps_acerto_pagamento_tb.acerto_id "
    
    SQL = SQL & "WHERE pgto_sinistro_tb.chave_autorizacao_sinistro = " & Left(txtNumAutorizacao, InStr(txtNumAutorizacao, "-") - 1)
    'SQL = SQL & " AND pgto_sinistro_tb.val_resseguro_recuperado is null "
    SQL = SQL & " AND pgto_sinistro_tb.num_parcela = 1 "
   
    If voper = "C" Then
        SQL = SQL & " AND ((pgto_sinistro_tb.tp_operacao = 'd') OR "
        SQL = SQL & "      (pgto_sinistro_tb.tp_operacao = 'c' and pgto_sinistro_tb.item_val_estimativa in (4,6) )) "
    Else
        SQL = SQL & " AND pgto_sinistro_tb.tp_operacao = 'd' "
    End If
    
    If voper = "I" Or voper = "IS" Then
        SQL = SQL & " AND pgto_sinistro_tb.situacao_op = 'n'"
    ElseIf vSituacao <> "'0'" Then
        SQL = SQL & " AND pgto_sinistro_tb.situacao_op = " & vSituacao
    End If
    
    'Ricardo Toledo (Confitec) : INC000004192958 : 10/10/2013 : Inicio
    'essa parte do codigo foi comentada devido a inclusao do distinct no select
    'SQL = SQL & " ORDER BY pgto_sinistro_tb.sinistro_id, "
    'SQL = SQL & " pgto_sinistro_tb.dt_acerto_contas_sinistro, " & _
    '                "pgto_sinistro_tb.beneficiario_id, pgto_sinistro_tb.item_val_estimativa"
    'Ricardo Toledo (Confitec) : INC000004192958 : 10/10/2013 : Fim
    
    Set rs = rdocn1.OpenResultset(SQL)
    
    flexPgtoSinistro.Rows = 1
    
    While Not rs.EOF
        '-------------------------------------------------------------------------------
        'Alterado por Stefanini Consultor: Cleber - 08/11/2004
        'Foi colocado uma consist�ncia no momento em que carrega o grid que
        'lista os pagamentos. O programa somente possibilita pagamento e
        'recusa de indeniza��o por t�cnicos autorizados (com a chave_usuario_SISBB).
        'Para fazer a consist�ncia foi criada 3 fun��es: Verifica_Sin_BB, Verifica_Aprova_Recusa, Verifica_Tecnico
        If Verifica_Aprova_Recusa(rs("item_val_estimativa"), rs("sinistro_id")) Then ' Sin.Sinistro_id) Then
        '--------------------------------------------------------------------------------

            flexPgtoSinistro.Row = flexPgtoSinistro.Rows - 1
            linha = Str(rs("sinistro_id")) & vbTab
            ' Preenche as colunas invis�veis
            linha = linha & rs("apolice_id") & vbTab
            linha = linha & rs("sucursal_seguradora_id") & vbTab
            linha = linha & rs("seguradora_cod_susep") & vbTab
            linha = linha & rs("ramo_id") & vbTab
            linha = linha & rs("seq_estimativa") & vbTab
            linha = linha & rs("seq_pgto") & vbTab
            linha = linha & rs("num_parcela") & vbTab
            linha = linha & rs("beneficiario_id") & vbTab
            'msalema - 16/05/2013
            'linha = linha & rs("situacao_op") & vbTab
            linha = linha & LCase(rs("situacao_op")) & vbTab
            'msalema - 16/05/2013
            linha = linha & "" & rs("motivo_rejeicao") & vbTab
            If IsNull(rs("dt_contabilizacao")) Then
                linha = linha & vbTab
            Else
                linha = linha & Format(rs("dt_contabilizacao"), "dd/mm/yyyy") & vbTab
            End If
    
            Recibo = ""
            If Not IsNull(rs("detalhamento_id")) Then
                SQL = "SELECT linha "
                SQL = SQL & " FROM sinistro_linha_detalhamento_tb with(nolock) "
                SQL = SQL & " WHERE sinistro_id = " & rs("Sinistro_id")
                SQL = SQL & "   AND apolice_id = " & rs("Apolice_id")
                SQL = SQL & "   AND seguradora_cod_susep = " & rs("Seguradora_cod_susep")
                SQL = SQL & "   AND sucursal_seguradora_id = " & rs("Sucursal_seguradora_id")
                SQL = SQL & "   AND ramo_id = " & rs("ramo_id")
                SQL = SQL & "   AND detalhamento_id = " & rs("detalhamento_id")
    
                'Set Rs1 = rdocn1.OpenResultset(SQL)  'C_ANEVES - 21/06/2013
                Set Rs1 = rdocn.OpenResultset(SQL)  'C_ANEVES - 21/06/2013
                While Not Rs1.EOF
                    Recibo = Recibo & "" & Rs1(0) & vbNewLine
                    Rs1.MoveNext
                Wend
                Rs1.Close
            End If
            linha = linha & Recibo & vbTab
            linha = linha & rs("sit_sinistro") & vbTab
            
            linha = linha & Format(rs("dt_acerto_contas_sinistro"), "dd/mm/yyyy") & vbTab
            'msalema - 16/05/2013
            'Select Case rs("situacao_op")
            Select Case LCase(rs("situacao_op"))
            'msalema - 16/05/2013
                Case "a": linha = linha & "APROVADO" & vbTab
                Case "r": linha = linha & "REJEITADO" & vbTab
                Case "c": linha = linha & "CANCELADO" & vbTab
                Case "e": linha = linha & "ESTORNADO" & vbTab
                Case "n": linha = linha & "AGUARD. APROVA��O" & vbTab
            End Select
            Select Case rs("item_val_estimativa")
                Case 1
                    linha = linha & "INDENIZA��O" & vbTab
                Case 2
                    linha = linha & "HONOR�RIOS" & vbTab
                Case 3
                    linha = linha & "DESPESAS" & vbTab
                Case 4
                    linha = linha & "RESSARCIMENTO" & vbTab
                Case 5
                    linha = linha & "DESP. RESSARCIMENTO" & vbTab
                Case 6
                    linha = linha & "SALVADOS" & vbTab
                Case 7
                    linha = linha & "DESP. SALVADOS" & vbTab
            End Select
            linha = linha & rs("nome_cob") & vbTab
            linha = linha & IIf(rs("tp_objeto_id") = 0, "", rs("nome_obj")) & vbTab
            val_total = Val(rs("val_acerto")) + Val(rs("val_correcao"))
            linha = linha & Format(val_total, "###,###,###,##0.00") & vbTab
            
            If voper = "I" Or voper = "IS" Then
                linha = linha & Format(rs("dt_solicitacao_pgto"), "dd/mm/yyyy") & vbTab
                linha = linha & rs("Usuario")
            Else
                If IsNull(rs("dt_avaliacao")) = True Then
                    linha = linha & "" & vbTab
                Else
                    linha = linha & Format(rs("dt_avaliacao"), "dd/mm/yyyy") & vbTab
                End If
                linha = linha & rs("Usuario") & vbTab
                linha = linha & "" & rs("usuario_avaliador") & vbTab
                linha = linha & "" & rs("Voucher_id")
            End If
            
            flexPgtoSinistro.AddItem linha
        
        End If
        rs.MoveNext
    Wend
    rs.Close
    vSelecionou = False

End Sub

Private Sub MontarGridAutorizacao()
    
    Dim Pgto As New Pgto_Sinistro, val_total As Double
    Dim rs As rdoResultset, Rs1 As rdoResultset
    Dim SQL As String, linha As String
    Dim Sin As Sinist, Recibo As String
    
    SQL = "SELECT pgto_sinistro_tb.sinistro_id, pgto_sinistro_tb.apolice_id, "
    SQL = SQL & " pgto_sinistro_tb.sucursal_seguradora_id, pgto_sinistro_tb.seguradora_cod_susep, "
    SQL = SQL & " pgto_sinistro_tb.ramo_id, "
    SQL = SQL & "pgto_sinistro_tb.dt_acerto_contas_sinistro, " & _
            "val_acerto = isnull(pgto_sinistro_tb.val_acerto,0), " & _
            "val_correcao = isnull(pgto_sinistro_tb.val_correcao,0), " & _
            "pgto_sinistro_tb.item_val_estimativa, " & _
            "pgto_sinistro_tb.situacao_op, " & _
            "pgto_sinistro_tb.motivo_rejeicao, " & _
            "forma_pgto_tb.forma_pgto_id, " & _
            "nome_forma = forma_pgto_tb.nome, " & _
            "sinistro_benef_tb.beneficiario_id, " & _
            "nome_benef = sinistro_benef_tb.nome, " & _
            "nome_cob = tp_cobertura_tb.nome, " & _
            "tp_objeto_tb.tp_objeto_id, " & _
            "nome_obj = tp_objeto_tb.nome, " & _
            "pgto_sinistro_tb.seq_estimativa, " & _
            "pgto_sinistro_tb.seq_pgto, " & _
            "pgto_sinistro_tb.num_parcela, " & _
            "pgto_sinistro_tb.dt_contabilizacao, " & _
            "pgto_sinistro_tb.dt_solicitacao_pgto, " & _
            "pgto_sinistro_tb.Usuario, " & _
            "ps_acerto_pagamento_tb.voucher_id, " & _
            "pgto_sinistro_tb.dt_avaliacao, " & _
            "pgto_sinistro_tb.usuario_avaliador, " & _
            "sinistro_estimativa_tb.num_carta_seguro_aceito, " & _
            "pgto_sinistro_tb.val_cosseguro, " & _
            "pgto_sinistro_tb.detalhamento_id, "
    SQL = SQL & "sit_sinistro = sinistro_tb.situacao "
    'Alan Neves - MP - 19/03/2013 - inicio - Alterado para ler temp
    'SQL = SQL & "FROM pgto_sinistro_tb with(nolock) " &
    SQL = SQL & "from #pgto_sinistro_tb pgto_sinistro_tb " & _
            "INNER JOIN sinistro_tb with(nolock) " & _
            "ON pgto_sinistro_tb.sinistro_id = sinistro_tb.sinistro_id " & _
            "AND pgto_sinistro_tb.apolice_id = sinistro_tb.apolice_id " & _
            "AND pgto_sinistro_tb.sucursal_seguradora_id = " & _
                    "sinistro_tb.sucursal_seguradora_id " & _
            "AND pgto_sinistro_tb.seguradora_cod_susep = " & _
                    "sinistro_tb.seguradora_cod_susep " & _
            "AND pgto_sinistro_tb.ramo_id = sinistro_tb.ramo_id " & _
            "INNER JOIN forma_pgto_tb with(nolock) " & _
             "ON pgto_sinistro_tb.forma_pgto_id = forma_pgto_tb.Forma_pgto_id " & _
            "INNER JOIN sinistro_benef_tb with(nolock) " & _
            "ON pgto_sinistro_tb.sinistro_id = sinistro_benef_tb.sinistro_id " & _
            "AND pgto_sinistro_tb.apolice_id = sinistro_benef_tb.apolice_id " & _
            "AND pgto_sinistro_tb.sucursal_seguradora_id = " & _
                    "sinistro_benef_tb.sucursal_seguradora_id " & _
            "AND pgto_sinistro_tb.seguradora_cod_susep = " & _
                    "sinistro_benef_tb.seguradora_cod_susep " & _
            "AND pgto_sinistro_tb.ramo_id = sinistro_benef_tb.ramo_id " & _
            "AND pgto_sinistro_tb.beneficiario_id = sinistro_benef_tb.beneficiario_id "
    SQL = SQL & "LEFT OUTER JOIN (pgto_sinistro_cobertura_tb with(nolock) " & _
            "       INNER JOIN tp_cobertura_tb with(nolock) ON tp_cobertura_tb.tp_cobertura_id = pgto_sinistro_cobertura_tb.tp_cobertura_id " & _
            "       INNER JOIN tp_objeto_tb with(nolock) ON tp_objeto_tb.tp_objeto_id = pgto_sinistro_cobertura_tb.tp_objeto_id " & _
            "       ) ON " & _
            "     pgto_sinistro_tb.sinistro_id = pgto_sinistro_cobertura_tb.sinistro_id " & _
            " AND pgto_sinistro_tb.apolice_id = pgto_sinistro_cobertura_tb.apolice_id " & _
            " AND pgto_sinistro_tb.sucursal_seguradora_id = pgto_sinistro_cobertura_tb.sucursal_seguradora_id " & _
            " AND pgto_sinistro_tb.seguradora_cod_susep = pgto_sinistro_cobertura_tb.seguradora_cod_susep " & _
            " AND pgto_sinistro_tb.ramo_id = pgto_sinistro_cobertura_tb.ramo_id " & _
            " AND pgto_sinistro_tb.item_val_estimativa = pgto_sinistro_cobertura_tb.item_val_estimativa " & _
            " AND pgto_sinistro_tb.seq_estimativa = pgto_sinistro_cobertura_tb.seq_estimativa " & _
            " AND pgto_sinistro_tb.seq_pgto = pgto_sinistro_cobertura_tb.seq_pgto " & _
            " AND pgto_sinistro_tb.num_parcela = pgto_sinistro_cobertura_tb.num_parcela " & _
            " AND pgto_sinistro_tb.beneficiario_id = pgto_sinistro_cobertura_tb.beneficiario_id "
    'Alan Neves - MP - 19/03/2013 - Inicio - Leitura na temp
    'SQL = SQL & " INNER JOIN sinistro_estimativa_tb with(nolock) " &
     SQL = SQL & " INNER JOIN #sinistro_estimativa_tb sinistro_estimativa_tb " & _
            "ON pgto_sinistro_tb.sinistro_id = sinistro_estimativa_tb.sinistro_id " & _
            "AND pgto_sinistro_tb.apolice_id = sinistro_estimativa_tb.apolice_id " & _
            "AND pgto_sinistro_tb.sucursal_seguradora_id = " & _
                    "sinistro_estimativa_tb.sucursal_seguradora_id " & _
            "AND pgto_sinistro_tb.seguradora_cod_susep = " & _
                    "sinistro_estimativa_tb.seguradora_cod_susep " & _
            "AND pgto_sinistro_tb.ramo_id = sinistro_estimativa_tb.ramo_id " & _
            "AND pgto_sinistro_tb.seq_estimativa = sinistro_estimativa_tb.seq_estimativa " & _
            "AND pgto_sinistro_tb.item_val_estimativa = sinistro_estimativa_tb.item_val_estimativa "
    SQL = SQL & "LEFT OUTER JOIN ps_acerto_pagamento_tb with(nolock) ON pgto_sinistro_tb.acerto_id = ps_acerto_pagamento_tb.acerto_id "
    
    SQL = SQL & "WHERE pgto_sinistro_tb.chave_autorizacao_sinistro = " & Left(txtNumAutorizacao, InStr(txtNumAutorizacao, "-") - 1)
    'SQL = SQL & " AND pgto_sinistro_tb.val_resseguro_recuperado is null "
    SQL = SQL & " AND pgto_sinistro_tb.num_parcela = 1 "
   
    If voper = "C" Then
        SQL = SQL & " AND ((pgto_sinistro_tb.tp_operacao = 'd') OR "
        SQL = SQL & "      (pgto_sinistro_tb.tp_operacao = 'c' and pgto_sinistro_tb.item_val_estimativa in (4,6) )) "
    Else
        SQL = SQL & " AND pgto_sinistro_tb.tp_operacao = 'd' "
    End If
    
    SQL = SQL & " AND pgto_sinistro_tb.situacao_op = 'a'"
    SQL = SQL & " ORDER BY pgto_sinistro_tb.sinistro_id, "
    SQL = SQL & " pgto_sinistro_tb.dt_acerto_contas_sinistro, " & _
                    "pgto_sinistro_tb.beneficiario_id, pgto_sinistro_tb.item_val_estimativa"
    Set rs = rdocn1.OpenResultset(SQL)
    
    grdPagtosAprovados.Rows = 1

    While Not rs.EOF

        grdPagtosAprovados.Row = grdPagtosAprovados.Rows - 1
        linha = Str(rs("sinistro_id")) & vbTab
        ' Preenche as colunas invis�veis
        linha = linha & rs("apolice_id") & vbTab
        linha = linha & rs("sucursal_seguradora_id") & vbTab
        linha = linha & rs("seguradora_cod_susep") & vbTab
        linha = linha & rs("ramo_id") & vbTab
        linha = linha & rs("seq_estimativa") & vbTab
        linha = linha & rs("seq_pgto") & vbTab
        linha = linha & rs("num_parcela") & vbTab
        linha = linha & rs("beneficiario_id") & vbTab
        'msalema - 16/05/2013
        'linha = linha & rs("situacao_op") & vbTab
        linha = linha & LCase(rs("situacao_op")) & vbTab
        'msalema - 16/05/2013
        linha = linha & "" & rs("motivo_rejeicao") & vbTab
        If IsNull(rs("dt_contabilizacao")) Then
            linha = linha & vbTab
        Else
            linha = linha & Format(rs("dt_contabilizacao"), "dd/mm/yyyy") & vbTab
        End If

        Recibo = ""
        If Not IsNull(rs("detalhamento_id")) Then
            SQL = "SELECT linha "
            SQL = SQL & " FROM sinistro_linha_detalhamento_tb with(nolock) "
            SQL = SQL & " WHERE sinistro_id = " & rs("Sinistro_id")
            SQL = SQL & "   AND apolice_id = " & rs("Apolice_id")
            SQL = SQL & "   AND seguradora_cod_susep = " & rs("Seguradora_cod_susep")
            SQL = SQL & "   AND sucursal_seguradora_id = " & rs("Sucursal_seguradora_id")
            SQL = SQL & "   AND ramo_id = " & rs("ramo_id")
            SQL = SQL & "   AND detalhamento_id = " & rs("detalhamento_id")

            'Set Rs1 = rdocn1.OpenResultset(SQL)  'C_ANEVES - 21/06/2013
            Set Rs1 = rdocn.OpenResultset(SQL) 'C_ANEVES - 21/06/2013
            While Not Rs1.EOF
                Recibo = Recibo & "" & Rs1(0) & vbNewLine
                Rs1.MoveNext
            Wend
            Rs1.Close
        End If
        linha = linha & Recibo & vbTab
        linha = linha & rs("sit_sinistro") & vbTab
        
        linha = linha & Format(rs("dt_acerto_contas_sinistro"), "dd/mm/yyyy") & vbTab
        'msalema - 16/05/2013
        'Select Case rs("situacao_op")
        Select Case LCase(rs("situacao_op"))
        'msalema - 16/05/2013
            Case "a": linha = linha & "APROVADO" & vbTab
            Case "r": linha = linha & "REJEITADO" & vbTab
            Case "c": linha = linha & "CANCELADO" & vbTab
            Case "e": linha = linha & "ESTORNADO" & vbTab
            Case "n": linha = linha & "AGUARD. APROVA��O" & vbTab
        End Select
        Select Case rs("item_val_estimativa")
            Case 1
                linha = linha & "INDENIZA��O" & vbTab
            Case 2
                linha = linha & "HONOR�RIOS" & vbTab
            Case 3
                linha = linha & "DESPESAS" & vbTab
            Case 4
                linha = linha & "RESSARCIMENTO" & vbTab
            Case 5
                linha = linha & "DESP. RESSARCIMENTO" & vbTab
            Case 6
                linha = linha & "SALVADOS" & vbTab
            Case 7
                linha = linha & "DESP. SALVADOS" & vbTab
        End Select
        linha = linha & rs("nome_cob") & vbTab
        linha = linha & IIf(rs("tp_objeto_id") = 0, "", rs("nome_obj")) & vbTab
        val_total = Val(rs("val_acerto")) + Val(rs("val_correcao"))
        linha = linha & Format(val_total, "###,###,###,##0.00") & vbTab
        
        If voper = "I" Or voper = "IS" Then
            linha = linha & Format(rs("dt_solicitacao_pgto"), "dd/mm/yyyy") & vbTab
            linha = linha & rs("Usuario")
        Else
            If IsNull(rs("dt_avaliacao")) = True Then
                linha = linha & "" & vbTab
            Else
                linha = linha & Format(rs("dt_avaliacao"), "dd/mm/yyyy") & vbTab
            End If
            linha = linha & rs("Usuario") & vbTab
            linha = linha & "" & rs("usuario_avaliador") & vbTab
            linha = linha & "" & rs("Voucher_id")
        End If
        
        grdPagtosAprovados.AddItem linha
        
        rs.MoveNext
    Wend
    rs.Close
    vSelecionou = False

End Sub

Private Sub Desmonta_Colecoes()

    Dim i As Integer

    'Desmonta a colecao se selecionados
    i = SinistroEmUso.Count
    While SinistroEmUso.Count > 0
        SinistroEmUso.Remove i
        i = i - 1
    Wend
    
    Desmonta_Pagto
    
End Sub

Private Sub Desmonta_Pagto()

    Dim i As Long

    i = Pagamentos.Count
    While Pagamentos.Count > 0
        Pagamentos.Remove i
        i = i - 1
    Wend
End Sub

Private Sub optSelecionado_Click()
'+-----------------------------------------------------------
'| 19406709 - Melhoria nos limites de sinistro e emissao
'| RN05 - petrauskas - 03/2017
  If produto_regra_especial Then
    verifica_pgto_X_IS
  End If
'| 19406709 - Melhoria nos limites de sinistro e emissao
'+-----------------------------------------------------------

End Sub

Private Sub optS�Cancela_Click()

    If frameCancelar.Enabled Then
        frameRejeitado.Visible = True
        frameRejeitado.Caption = "Motivo Cancelamento"
    End If
    txtRejeicao = ""
    txtRejeicao.Locked = False
    
End Sub
Private Function Verifica_Cosseguro() As Boolean
Dim vSql As String
Dim rs As rdoResultset
Dim Sin As Sinist

    Verifica_Cosseguro = False

    For Each Sin In SinistroEmUso
        'Alan Neves - MP - 18/03/2013 - Inicio
'        vsql = "SELECT a.tp_emissao, b.pgto_parcial_co_seguro " & _
'                "FROM Apolice_tb a with(nolock), Sinistro_tb b with(nolock) " & _
'                "  WHERE a.apolice_id               = " & Sin.Apolice_id & _
'                "    AND a.sucursal_seguradora_id   = " & Sin.Sucursal_id & _
'                "    AND a.seguradora_cod_susep     = " & Sin.Seguradora_id & _
'                "    AND a.ramo_id                  = " & Sin.ramo_id & _
'                "    AND a.apolice_id = b.apolice_id" & _
'                "    AND a.sucursal_seguradora_id = b.sucursal_seguradora_id" & _
'                "    AND a.seguradora_cod_susep = b.seguradora_cod_susep" & _
'                "    AND a.ramo_id = b.ramo_id"
                
        vSql = "    set nocount on " & vbNewLine
        vSql = vSql & "select b.tp_emissao, a.pgto_parcial_co_seguro " & vbNewLine
        vSql = vSql & "  from #Sinistro_tb a " & vbNewLine
        vSql = vSql & "  join #apolice_tb b " & vbNewLine
        vSql = vSql & "    on a.apolice_id                = b.apolice_id" & vbNewLine
        vSql = vSql & "   and a.sucursal_seguradora_id    = b.sucursal_seguradora_id" & vbNewLine
        vSql = vSql & "   and a.seguradora_cod_susep      = b.seguradora_cod_susep" & vbNewLine
        vSql = vSql & "   and a.ramo_id                   = b.ramo_id"
        vSql = vSql & " where a.apolice_id                = " & Sin.Apolice_id & vbNewLine
        vSql = vSql & "   and a.sucursal_seguradora_id    = " & Sin.Sucursal_id & vbNewLine
        vSql = vSql & "   AND a.seguradora_cod_susep      = " & Sin.Seguradora_id & vbNewLine
        vSql = vSql & "   AND a.ramo_id                   = " & Sin.ramo_id
        
        'Alan Neves - MP - 18/03/2013 - Fim
                
        Set rs = rdocn1.OpenResultset(vSql)
        If Not rs.EOF Then
            If rs(0) = "C" And rs(1) = "s" Then
                Verifica_Cosseguro = True
            End If
        End If
        rs.Close
    Next
        
End Function

Private Function Valida_FAJ(vSin As Sinist) As Boolean
        
        Dim rdoRS As rdoResultset, SQL As String
        Dim vMoeda_Sinistro As String, vMoeda_Atual As String
        Dim vDt_Indice As String, vPgto As Pgto_Sinistro
        Dim vTp_Emissao As String, Val_indice As Double
        Dim x As Long ' linha do FlexGrid
        
        Valida_FAJ = False
        
        If Not frameAutorizacao.Visible Then
            If optTodos Then
                Set vPgto = Pagamentos.Item(1)
            Else
                Set vPgto = Pagamentos.Item(flexPgtoSinistro.Row)
            End If
        End If

        'Obt�m a Moeda Atual
        vMoeda_Atual = BuscaParametro("MOEDA ATUAL", rdocn)

        'Obtem a Moeda do Sinistro
        'Alan Neves - MP - 18/03/2013 - Inicio
        SQL = "SELECT  s.moeda_id, s.tp_emissao" & vbNewLine
        SQL = SQL & " FROM  #sinistro_tb s " & vbNewLine
'        SQL = SQL & " FROM  sinistro_tb s with(nolock) "
'        SQL = SQL & "   INNER JOIN apolice_tb a with(nolock) "
'        SQL = SQL & "       ON (a.apolice_id                = s.apolice_id"
'        SQL = SQL & "       AND a.sucursal_seguradora_id    = s.sucursal_seguradora_id"
'        SQL = SQL & "       AND a.seguradora_cod_susep      = s.seguradora_cod_susep"
'        SQL = SQL & "       AND a.ramo_id                   = s.ramo_id"
'        SQL = SQL & "       AND a.proposta_id               = s.proposta_id)"
        
        SQL = SQL & "  join #apolice_tb  a " & vbNewLine
        SQL = SQL & "    on a.apolice_id             = s.apolice_id " & vbNewLine
        SQL = SQL & "   and a.sucursal_seguradora_id = s.sucursal_seguradora_id " & vbNewLine
        SQL = SQL & "   and a.seguradora_cod_susep   = s.seguradora_cod_susep " & vbNewLine
        SQL = SQL & "   and a.ramo_id                = s.ramo_id " & vbNewLine
        SQL = SQL & "   and a.proposta_id            = s.proposta_id " & vbNewLine
        'Alan Neves - MP - 18/03/2013 - Fim
        
        If frameAutorizacao.Visible Then
            x = flexPgtoSinistro.Row
            SQL = SQL & " WHERE s.Sinistro_id               = " & flexPgtoSinistro.TextMatrix(x, 0) & vbNewLine
            SQL = SQL & " AND   s.apolice_id                = " & flexPgtoSinistro.TextMatrix(x, 1) & vbNewLine
            SQL = SQL & " AND   s.sucursal_seguradora_id    = " & flexPgtoSinistro.TextMatrix(x, 2) & vbNewLine
            SQL = SQL & " AND   s.seguradora_cod_susep      = " & flexPgtoSinistro.TextMatrix(x, 3) & vbNewLine
            SQL = SQL & " AND   s.ramo_id                   = " & flexPgtoSinistro.TextMatrix(x, 4) & vbNewLine
        Else
            SQL = SQL & " WHERE s.Sinistro_id               = " & vSin.sinistro_id & vbNewLine
            SQL = SQL & " AND   s.apolice_id                = " & vSin.Apolice_id & vbNewLine
            SQL = SQL & " AND   s.sucursal_seguradora_id    = " & vSin.Sucursal_id & vbNewLine
            SQL = SQL & " AND   s.seguradora_cod_susep      = " & vSin.Seguradora_id & vbNewLine
            SQL = SQL & " AND   s.ramo_id                   = " & vSin.ramo_id & vbNewLine
        End If
        Set rdoRS = rdocn1.OpenResultset(SQL)
        If Not rdoRS.EOF Then
           vMoeda_Sinistro = rdoRS("moeda_id")
           vTp_Emissao = rdoRS("tp_emissao")
        End If
        rdoRS.Close
        
        'Verifica se a Moeda do Sinistro � igual � Moeda Atual
        If (vMoeda_Sinistro <> vMoeda_Atual) Or (vTp_Emissao = "A") Then
           Val_indice = 1
           Valida_FAJ = True
        Else
           'Obt�m a Data �ndice para verifica��o do FAJ
           SQL = "SELECT CONVERT(varchar(10), dt_solicitacao_pgto,112)"
           
           'Alan Neves - MP - 18/03/2013 - Inicio
'           SQL = SQL & " FROM  pgto_sinistro_tb with(nolock) "
           SQL = SQL & " FROM  #pgto_sinistro_tb "
           'Alan Neves - MP - 18/03/2013 - Fim
           
           If frameAutorizacao.Visible Then
                x = flexPgtoSinistro.Row
           'Alan Neves - MP - 18/03/2013 - Inicio
'                SQL = SQL & " WHERE s.Sinistro_id               = " & flexPgtoSinistro.TextMatrix(x, 0)
'                SQL = SQL & " AND   s.apolice_id                = " & flexPgtoSinistro.TextMatrix(x, 1)
'                SQL = SQL & " AND   s.sucursal_seguradora_id    = " & flexPgtoSinistro.TextMatrix(x, 2)
'                SQL = SQL & " AND   s.seguradora_cod_susep      = " & flexPgtoSinistro.TextMatrix(x, 3)
'                SQL = SQL & " AND   s.ramo_id                   = " & flexPgtoSinistro.TextMatrix(x, 4)
                
                SQL = SQL & " WHERE Sinistro_id               = " & flexPgtoSinistro.TextMatrix(x, 0)
                SQL = SQL & " AND   apolice_id                = " & flexPgtoSinistro.TextMatrix(x, 1)
                SQL = SQL & " AND   sucursal_seguradora_id    = " & flexPgtoSinistro.TextMatrix(x, 2)
                SQL = SQL & " AND   seguradora_cod_susep      = " & flexPgtoSinistro.TextMatrix(x, 3)
                SQL = SQL & " AND   ramo_id                   = " & flexPgtoSinistro.TextMatrix(x, 4)
           'Alan Neves - MP - 18/03/2013 - Fim
                
                Select Case flexPgtoSinistro.TextMatrix(x, 16)
                    Case "INDENIZA��O"
                        SQL = SQL & " AND   item_val_estimativa     = 1 "
                    Case "HONOR�RIOS"
                        SQL = SQL & " AND   item_val_estimativa     = 2 "
                    Case "DESPESAS"
                        SQL = SQL & " AND   item_val_estimativa     = 3 "
                    Case "RESSARCIMENTO"
                        SQL = SQL & " AND   item_val_estimativa     = 4 "
                    Case "DESP. RESSARCIMENTO"
                        SQL = SQL & " AND   item_val_estimativa     = 5 "
                    Case "SALVADOS"
                        SQL = SQL & " AND   item_val_estimativa     = 6 "
                    Case "DESP. SALVADOS"
                        SQL = SQL & " AND   item_val_estimativa     = 7 "
                End Select
                SQL = SQL & " AND   seq_estimativa          = " & flexPgtoSinistro.TextMatrix(x, 5)
                SQL = SQL & " AND   seq_pgto                = " & flexPgtoSinistro.TextMatrix(x, 6)
                SQL = SQL & " AND   num_parcela             = " & flexPgtoSinistro.TextMatrix(x, 7)
                SQL = SQL & " AND   beneficiario_id         = " & flexPgtoSinistro.TextMatrix(x, 8)
           Else
                SQL = SQL & " WHERE Sinistro_id             = " & vSin.sinistro_id
                SQL = SQL & " AND   apolice_id              = " & vSin.Apolice_id
                SQL = SQL & " AND   sucursal_seguradora_id  = " & vSin.Sucursal_id
                SQL = SQL & " AND   seguradora_cod_susep    = " & vSin.Seguradora_id
                SQL = SQL & " AND   ramo_id                 = " & vSin.ramo_id
                SQL = SQL & " AND   item_val_estimativa     = " & vPgto.Item_val_estimativa
                SQL = SQL & " AND   seq_estimativa          = " & vPgto.Seq_Estimativa
                SQL = SQL & " AND   seq_pgto                = " & vPgto.Seq_Pgto
                SQL = SQL & " AND   num_parcela             = " & vPgto.Num_Parcela
                SQL = SQL & " AND   beneficiario_id         = " & vPgto.Beneficiario_id
           End If
           Set rdoRS = rdocn1.OpenResultset(SQL)
           If Not rdoRS.EOF Then
              vDt_Indice = rdoRS(0)
           End If
           rdoRS.Close

           If vDt_Indice <> "" Then
              SQL = "SELECT val_paridade_moeda"
              SQL = SQL & " FROM   paridade_tb with(nolock) "
              SQL = SQL & " WHERE  origem_moeda_id     = 10"
              SQL = SQL & " AND    destino_moeda_id    = " & vMoeda_Sinistro
              SQL = SQL & " AND    dt_conversao        = '" & vDt_Indice & "'"
              Set rdoRS = rdocn.OpenResultset(SQL)
              If Not rdoRS.EOF Then
                 Valida_FAJ = True
              End If
              rdoRS.Close
           End If
        End If
        
        Set vPgto = Nothing
        
End Function
 
Private Function Obtem_Moeda_Sinistro(ByVal vSin As Sinist) As Integer
    
    Dim SQL         As String
    Dim rs          As rdoResultset
    
    On Error GoTo TrataErro
    
    SQL = "SELECT moeda_id"
    'Alan Neves - MP - 18/03/2013 - Inicio
'    SQL = SQL & " FROM sinistro_tb with(nolock) "
    SQL = SQL & " FROM #sinistro_tb "
    'Alan Neves - MP - 18/03/2013 - Fim
    
    SQL = SQL & " WHERE sinistro_id             = " & vSin.sinistro_id
    SQL = SQL & "   AND apolice_id              = " & vSin.Apolice_id
    SQL = SQL & "   AND sucursal_seguradora_id  = " & vSin.Sucursal_id
    SQL = SQL & "   AND seguradora_cod_susep    = " & vSin.Seguradora_id
    SQL = SQL & "   AND ramo_id                 = " & vSin.ramo_id
    Set rs = rdocn1.OpenResultset(SQL)
    
    Obtem_Moeda_Sinistro = rs("moeda_id")
    rs.Close
    
    Exit Function
    
TrataErro:
    Call TrataErroGeral("Obtem_Moeda_Sinistro", Me.name)
    
End Function

Private Function Obtem_Codigo_Tecnico() As Integer
'Alan Neves - Executada apenas uma unica vez no form FrmAcertoCon
    
    Dim SQL         As String
    Dim rs          As rdoResultset
    Dim OLogin      As String
    
    On Error GoTo TrataErro
    
    Obtem_Codigo_Tecnico = 0
    
    '* Obt�m o login do usu�rio
    'Ricardo Toledo : Confitec : 30/04/2011 : flow 10673500 : inicio
    'OLogin = BuscaLogin(cUserName)
    If glAmbiente_id = 2 Or glAmbiente_id = 3 Then
        OLogin = BuscaLogin(cUserName)
    Else
        OLogin = BuscaLogin(cUserName, 1)
    End If
    'Ricardo Toledo : Confitec : 30/04/2011 : flow 10673500 : inicio
    
    
    'Rodrigo Maiellaro (Stefanini) - 09/08/2006
    'Incluido voper para verificar se o parametro � I, IC ou IS e
    'Query com Distinct e Join com limite_aprovacao_pgto_tb.
    If voper = "I" Or voper = "IC" Or voper = "IS" Then
        SQL = "SELECT distinct(tecnico_tb.tecnico_id)"
        SQL = SQL & " FROM tecnico_tb  with(nolock) "
        SQL = SQL & " JOIN limite_aprovacao_pgto_tb with(nolock) "
        SQL = SQL & "   ON limite_aprovacao_pgto_tb.tecnico_id = tecnico_tb.tecnico_id "
        SQL = SQL & " WHERE eMail = '" & OLogin & "'"
        SQL = SQL & " AND limite_aprovacao_pgto_tb.dt_fim_vigencia IS NULL "
        Set rs = rdocn1.OpenResultset(SQL)
        If Not rs.EOF Then
            Obtem_Codigo_Tecnico = rs("tecnico_id")
        End If
        rs.Close
    Else
        SQL = "SELECT distinct(tecnico_tb.tecnico_id)"
        SQL = SQL & " FROM tecnico_tb  with(nolock) "
        SQL = SQL & " WHERE eMail = '" & OLogin & "'"
        Set rs = rdocn1.OpenResultset(SQL)
        If Not rs.EOF Then
            Obtem_Codigo_Tecnico = rs("tecnico_id")
        End If
        rs.Close
    End If
    
    Exit Function
    
TrataErro:
    Call TrataErroGeral("Obtem_Codigo_Tecnico", Me.name)
    
End Function

'Alan Neves - MP - 18/03/2013 - NomeProduto e NomeRamo passado na funciton
'Private Function Tecnico_tem_Alcada(ByVal OItem_Val_Estimativa As String, ByVal ORamo_id As String, ByVal OProduto_id As String, ByVal OVal_Pago As Double, ByVal OCodigoSeguradora_id As String, ByVal OSucursal_id As String, ByVal OApolice_id As String, ByVal OSinistro_id As String, ByVal OSeq_Estimativa As String, ByVal OSeq_Pgto As String) As Boolean
Private Function Tecnico_tem_Alcada(ByVal OItem_Val_Estimativa As String, ByVal ORamo_id As String, ByVal OProduto_id As String, ByVal OVal_Pago As Double, ByVal OCodigoSeguradora_id As String, ByVal OSucursal_id As String, ByVal OApolice_id As String, ByVal OSinistro_id As String, ByVal OSeq_Estimativa As String, ByVal OSeq_Pgto As String, ByVal ONome_Produto As String, ByVal ONome_Ramo As String) As Boolean
'Alan Neves - MP - 18/03/2013 - Fim
    
    
    '****************************************************************
    '*  Projeto:    495123 - Altera��o de al�adas t�cnicos sinistro *
    '*  Programador:Afonso Dutra Nogueira Filho                     *
    '*  Data:       24/09/2008                                      *
    '*  Autor:      M�rcio Ossamu Yoshimura                         *
    '****************************************************************
    
    Dim SQL             As String
    Dim sSQL            As String
    Dim vSql            As String
    Dim rc              As rdoResultset
    Dim rc_aux          As rdoResultset
    Dim rs              As rdoResultset
    Dim Val_Limite      As Double
    Dim MSG             As String
    Dim NomeProduto     As String
    Dim NomeRamo        As String
    Dim DescrItem       As String
    
    'Alan Neves - MP - MP - 12/03/2013 - Inicio
    'Dim Tecnico_atual   As String
    'Alan Neves - MP - 12/03/2013 - Fim
    
    Dim Ilimitado       As Boolean
    Dim Alcada_liquida  As Boolean
    Dim Total As Currency
    Dim Val_Liquido As Currency
    Dim V_tot As Currency
    Dim v_totaux As Currency
    
    On Error GoTo TrataErro
    
    Tecnico_tem_Alcada = False
    Ilimitado = False
    
    '* Obt�m o C�digo do T�cnico que ir� aprovar o pagamento
    'Alan Neves - MP - 12/03/2013 - Inicio - Codigo do t�cnico j� obtido no carregamento do form
'    Tecnico_atual = Obtem_Codigo_Tecnico
     'Alan Neves - MP - 12/03/2013 - Fim
    
    '* Obt�m o nome do produto e do ramo
    'Alan Neves - MP - 18/03/2013 - Inicio - Comentado a chamada de Obtem_nome_Produto_E_Nome_Ramo
    'Call Obtem_Nome_Produto_E_Nome_Ramo(OProduto_id, ORamo_id, NomeProduto, NomeRamo)
    NomeProduto = ONome_Produto
    NomeRamo = ONome_Ramo
    'Alan Neves - MP - 18/03/2013 - Fim
    
    '* Obt�m a descri��o do Item_val_estimativa
    Select Case OItem_Val_Estimativa
        Case 1
            DescrItem = "INDENIZA��O"
        Case 2
            DescrItem = "HONOR�RIOS"
        Case 3
            DescrItem = "DESPESAS"
        Case Else
            DescrItem = "N�O INFORMADO"
    End Select
    
    'Alan Neves - MP - 12/03/2013 - Inicio - Consulta realizada na temp
'    SQL = "SELECT val_limite_aprovacao, ilimitado, alcada_liquida " & vbNewLine 'Afonso, adicionado ilimitado na busca
'    SQL = SQL & " FROM limite_aprovacao_pgto_tb with(nolock) " & vbNewLine
'    SQL = SQL & " WHERE tecnico_id          = " & Tecnico_atual & vbNewLine
'    SQL = SQL & "   AND item_val_estimativa = " & OItem_Val_Estimativa & vbNewLine
'    SQL = SQL & "   AND ramo_id             = " & ORamo_id & vbNewLine
'    SQL = SQL & "   AND produto_id          = " & OProduto_id & vbNewLine
'    SQL = SQL & "   AND dt_fim_vigencia     IS NULL" & vbNewLine
   
    SQL = "SELECT val_limite_aprovacao, ilimitado, alcada_liquida " & vbNewLine 'Afonso, adicionado ilimitado na busca
    SQL = SQL & "  from #limite_aprovacao_pgto_tb " & vbNewLine
    SQL = SQL & " WHERE tecnico_id          = " & Tecnico_atual & vbNewLine
    SQL = SQL & "   AND item_val_estimativa = " & OItem_Val_Estimativa & vbNewLine
    SQL = SQL & "   AND ramo_id             = " & ORamo_id & vbNewLine
    SQL = SQL & "   AND produto_id          = " & OProduto_id & vbNewLine
    'Alan Neves - MP - 12/03/2013 - Fim
    
    Set rs = rdocn1.OpenResultset(SQL)
    
    If Not rs.EOF Then
        Ilimitado = IIf(rs("ilimitado") = "S", True, False)
        Alcada_liquida = IIf(rs("alcada_liquida") = "S", True, False)
        Val_Limite = Val(rs("val_limite_aprovacao"))
        
        If Alcada_liquida = True Then
            
            Val_Liquido = SelecionaValorLiquido(OItem_Val_Estimativa, _
                                                OSinistro_id, _
                                                OApolice_id, _
                                                OSucursal_id, _
                                                OCodigoSeguradora_id, _
                                                ORamo_id, _
                                                OSeq_Pgto, _
                                                OSeq_Estimativa)
                                                
                              
            If optTodos.Value = True Then
                Total = SomarTodosPagamentosPendentesLiquido(OItem_Val_Estimativa, _
                                                                OSinistro_id, _
                                                                OApolice_id, _
                                                                OSucursal_id, _
                                                                OCodigoSeguradora_id, _
                                                                ORamo_id, _
                                                                "T")
                
                'Somar todos os registros aprovados + registros selecionado para aprova��o
                v_totaux = Total
            Else
                Total = SomarTodosPagamentosPendentesLiquido(OItem_Val_Estimativa, _
                                                                OSinistro_id, _
                                                                OApolice_id, _
                                                                OSucursal_id, _
                                                                OCodigoSeguradora_id, _
                                                                ORamo_id, _
                                                                "S")
                'Francisco H. Berrocal
                'INC000003913034 Duplica��o de pagamento fora do limite da al�ada
                v_totaux = Total
                Total = Total + Val_Liquido
            End If
            'Dim v_totaux As Currency
            'v_totaux = Total
            'Total = Total + Val_Liquido
            
            
            If optTodos.Value = True Then
                V_tot = SomarTodosPagamentosPendentesAprovados(OItem_Val_Estimativa, _
                                                                   OSinistro_id, _
                                                                   OApolice_id, _
                                                                   OSucursal_id, _
                                                                   OCodigoSeguradora_id, _
                                                                   ORamo_id, _
                                                                   "T")
                
                'Somar todos os registros aprovados + registros selecionado para aprova��o
            Else
                V_tot = SomarTodosPagamentosPendentesAprovados(OItem_Val_Estimativa, _
                                                                   OSinistro_id, _
                                                                   OApolice_id, _
                                                                   OSucursal_id, _
                                                                   OCodigoSeguradora_id, _
                                                                   ORamo_id, _
                                                                   "S")
                
            End If
            
           '* Verifica se o limite do t�cnico para este pagamento
           If Total <= Val_Limite Then
                rs.Close
                Tecnico_tem_Alcada = True
                Exit Function
           Else
              Dim auxmsg As Currency
              Dim auxmsgtot As Currency

              MSG = "T�cnico n�o tem al�ada para a aprova��o autom�tica do Pagamento." & vbCrLf
              MSG = MSG & vbCrLf
              MSG = MSG & "Motivo:" & vbCrLf
              MSG = MSG & "----------" & vbCrLf
              MSG = MSG & "    Produto.: " & OProduto_id & " - " & NomeProduto & vbCrLf
              MSG = MSG & "    Ramo....: " & ORamo_id & " - " & NomeRamo & vbCrLf
              MSG = MSG & "    Item.....: " & DescrItem & vbCrLf
              If Not optTodos.Value = True Then
                MSG = MSG & "    Valor a aprovar      R$.: " & Format(OVal_Pago, "###,###,##0.00") & " (L�quido de cosseguro e resseguro: R$.: " & Format(Val_Liquido, "###,###,##0.00") & ")" & vbCrLf
              End If
              If V_tot <> 0 Or v_totaux <> 0 Then
                MSG = MSG & "    Total aprovado      R$.: " & Format(V_tot, "###,###,##0.00") & " (L�quido de cosseguro e resseguro: R$.: " & Format(v_totaux, "###,###,##0.00") & ")" & vbCrLf
                auxmsg = Val_Liquido + v_totaux
                auxmsgtot = V_tot + OVal_Pago
                MSG = MSG & "    Total acumulado    R$.: " & Format(auxmsgtot, "###,###,##0.00") & " (L�quido de cosseguro e resseguro: R$.: " & Format(auxmsg, "###,###,##0.00") & ")" & vbCrLf
              End If
              MSG = MSG & vbCrLf
              MSG = MSG & "O seu limite de aprova��o � de R$: " & Format(Val_Limite, "###,###,##0.00") & " (L�quido de cosseguro e resseguro)" & vbCrLf
          End If
        Else
       '* Verificar se ele � ilimitado
            If Ilimitado = True Then
                '* Verifica se o limite do t�cnico para este pagamento
                If OVal_Pago <= Val_Limite Then
                  rs.Close
                  Tecnico_tem_Alcada = True
                  Exit Function
                Else
                  MSG = "T�cnico n�o tem al�ada para a aprova��o autom�tica do Pagamento." & vbCrLf
                  MSG = MSG & vbCrLf
                  MSG = MSG & "Motivo:" & vbCrLf
                  MSG = MSG & "----------" & vbCrLf
                  MSG = MSG & "        Produto.: " & OProduto_id & " - " & NomeProduto & vbCrLf
                  MSG = MSG & "        Ramo....: " & ORamo_id & " - " & NomeRamo & vbCrLf
                  MSG = MSG & "        Item.....: " & DescrItem & vbCrLf
                  MSG = MSG & "        Valor R$.: " & Format(OVal_Pago, "###,###,##0.00") & vbCrLf
                  MSG = MSG & vbCrLf
                  MSG = MSG & "O Seu Limite de aprova��o � R$: " & Format(Val_Limite, "###,###,##0.00") & vbCrLf
                End If
                
            Else
        '****************************************************************
        '*  Projeto:    495123 - Altera��o de al�adas t�cnicos sinistro *
        '*  Programador:Afonso Dutra Nogueira Filho                     *
        '*  Data:       24/09/2008                                      *
        '*  Autor:      M�rcio Ossamu Yoshimura                         *
        '****************************************************************

            
            'Somar todos os registros pendentes + todos os registros aprovados
            If optTodos.Value = True Then
                Total = SomarTodosPagamentosPendentesAprovados(OItem_Val_Estimativa, _
                                                               OSinistro_id, _
                                                               OApolice_id, _
                                                               OSucursal_id, _
                                                               OCodigoSeguradora_id, _
                                                               ORamo_id, _
                                                               "T")
            
            'Somar todos os registros aprovados + registros selecionado para aprova��o
            Else
                Total = SomarTodosPagamentosPendentesAprovados(OItem_Val_Estimativa, _
                                                               OSinistro_id, _
                                                               OApolice_id, _
                                                               OSucursal_id, _
                                                               OCodigoSeguradora_id, _
                                                               ORamo_id, _
                                                               "S")
            
                Total = Total + OVal_Pago
            End If
            
'            Dim i As Integer
'            Total = 0
'            If flexPgtoSinistro.Rows > 0 Then
'                For i = 1 To flexPgtoSinistro.Rows - 1
'                    Total = Total + flexPgtoSinistro.TextMatrix(i, 5)
'                Next
'            End If
'
'            If grdPagtosAprovados.Rows > 0 Then
'                For i = 1 To grdPagtosAprovados.Rows - 1
'                    Total = Total + grdPagtosAprovados.TextMatrix(i, 5)
'                Next
'            End If
            
            If Total <= Val_Limite Then
              If OVal_Pago <= Val_Limite Then
                rs.Close
                Tecnico_tem_Alcada = True
                Exit Function
              Else
                MSG = "T�cnico n�o tem al�ada para a aprova��o autom�tica do Pagamento." & vbCrLf
                MSG = MSG & vbCrLf
                MSG = MSG & "Motivo:" & vbCrLf
                MSG = MSG & "----------" & vbCrLf
                MSG = MSG & "        Produto.: " & OProduto_id & " - " & NomeProduto & vbCrLf
                MSG = MSG & "        Ramo....: " & ORamo_id & " - " & NomeRamo & vbCrLf
                MSG = MSG & "        Item.....: " & DescrItem & vbCrLf
                MSG = MSG & "        Valor R$.: " & Format(OVal_Pago, "###,###,##0.00") & vbCrLf
                MSG = MSG & "        Total R$.: " & Format(Total, "###,###,##0.00") & vbCrLf
                MSG = MSG & vbCrLf
                MSG = MSG & "O Seu Limite de aprova��o � R$: " & Format(Val_Limite, "###,###,##0.00") & vbCrLf
              End If
              
            Else
              MSG = "T�cnico n�o tem al�ada para a aprova��o autom�tica do Pagamento." & vbCrLf
              MSG = MSG & vbCrLf
              MSG = MSG & "Motivo:" & vbCrLf
              MSG = MSG & "----------" & vbCrLf
              MSG = MSG & "        Produto.: " & OProduto_id & " - " & NomeProduto & vbCrLf
              MSG = MSG & "        Ramo....: " & ORamo_id & " - " & NomeRamo & vbCrLf
              MSG = MSG & "        Item.....: " & DescrItem & vbCrLf
              MSG = MSG & "        Valor R$.: " & Format(OVal_Pago, "###,###,##0.00") & vbCrLf
              MSG = MSG & "        Total R$.: " & Format(Total, "###,###,##0.00") & vbCrLf
              MSG = MSG & vbCrLf
              MSG = MSG & "O Seu Limite de aprova��o � R$: " & Format(Val_Limite, "###,###,##0.00") & vbCrLf
            End If
            
            End If
        End If
        
    Else 'N�o encontrou nenhum registro
    
       rs.Close
       
       '* Se n�o encontrou registro, ent�o verifica qual par�metro que impede o t�cnico de efetuar a aprova��o
       MSG = "T�cnico n�o tem al�ada para a aprova��o autom�tica do Pagamento." & vbCrLf
       MSG = MSG & vbCrLf
       MSG = MSG & "Motivo:" & vbCrLf
       MSG = MSG & "----------" & vbCrLf
       
       'Alan Neves - MP - 19/03/2013 - Inicio
       '* Verifica se existe o produto
'       SQL = "SELECT val_limite_aprovacao"
'       SQL = SQL & " FROM limite_aprovacao_pgto_tb with(nolock) "
'       SQL = SQL & " WHERE tecnico_id          = " & Tecnico_atual
'       SQL = SQL & "   AND produto_id          = " & OProduto_id
'       SQL = SQL & "   AND dt_fim_vigencia     IS NULL"
'
'       Set rs = rdocn.OpenResultset(SQL)
'       If Not rs.EOF Then
'          MSG = MSG & "        Produto.: " & OProduto_id & " - " & NomeProduto & vbCrLf
'       Else
'          MSG = MSG & "        Produto.: INV�LIDO" & vbCrLf
'       End If
'       rs.Close
'
'       '* Verifica se existe o ramo_id
'       SQL = "SELECT val_limite_aprovacao"
'       SQL = SQL & " FROM limite_aprovacao_pgto_tb with(nolock) "
'       SQL = SQL & " WHERE tecnico_id          = " & Tecnico_atual
'       SQL = SQL & "   AND produto_id          = " & OProduto_id
'       SQL = SQL & "   AND ramo_id             = " & ORamo_id
'       SQL = SQL & "   AND dt_fim_vigencia     IS NULL"
'
'       Set rs = rdocn.OpenResultset(SQL)
'
'       If Not rs.EOF Then
'          MSG = MSG & "        Ramo.....: " & ORamo_id & " - " & NomeRamo & vbCrLf
'       Else
'          MSG = MSG & "        Ramo.....: INV�LIDO" & vbCrLf
'       End If
'       rs.Close
'
'       '* Verifica se existe a estimativa
'       SQL = "  SELECT val_limite_aprovacao"
'       SQL = SQL & " FROM limite_aprovacao_pgto_tb with(nolock) "
'       SQL = SQL & " WHERE tecnico_id          = " & Tecnico_atual
'       SQL = SQL & "   AND item_val_estimativa = " & OItem_Val_Estimativa
'       SQL = SQL & "   AND produto_id          = " & OProduto_id
'       SQL = SQL & "   AND ramo_id             = " & ORamo_id
'       SQL = SQL & "   AND dt_fim_vigencia     IS NULL"
'
'       Set rs = rdocn.OpenResultset(SQL)
'
'       If Not rs.EOF Then
'          MSG = MSG & "        Item.......: " & DescrItem & vbCrLf
'       Else
'          MSG = MSG & "        Item.......: INV�LIDO" & vbCrLf
'       End If
     
       'A temp j� criada consideraqndo dt_fim_vigencia is null
       SQL = "select 1" & vbNewLine
       SQL = SQL & "  from #limite_aprovacao_pgto_tb with(nolock) " & vbNewLine
       SQL = SQL & " where tecnico_id          = " & Tecnico_atual & vbNewLine
       SQL = SQL & "   and produto_id          = " & OProduto_id & vbNewLine
       SQL = SQL & "   and ramo_id             = " & ORamo_id & vbNewLine
       
       Set rs = rdocn1.OpenResultset(SQL)
       If Not rs.EOF Then
          MSG = MSG & "        Produto..: " & OProduto_id & " - " & NomeProduto & vbCrLf
          MSG = MSG & "        Ramo......: " & ORamo_id & " - " & NomeRamo & vbCrLf
       Else
          MSG = MSG & "        Produto..: INV�LIDO" & vbCrLf
          MSG = MSG & "        Ramo......: INV�LIDO" & vbCrLf
       End If
       rs.Close
       
       SQL = "      select 1"
       SQL = SQL & "  from #limite_aprovacao_pgto_tb with(nolock) " & vbNewLine
       SQL = SQL & " where tecnico_id          = " & Tecnico_atual & vbNewLine
       SQL = SQL & "   and item_val_estimativa = " & OItem_Val_Estimativa & vbNewLine
       SQL = SQL & "   and produto_id          = " & OProduto_id & vbNewLine
       SQL = SQL & "   and ramo_id             = " & ORamo_id & vbNewLine
       
       Set rs = rdocn1.OpenResultset(SQL)
       
       If Not rs.EOF Then
          MSG = MSG & "        Item........: " & DescrItem & vbCrLf
       Else
          MSG = MSG & "        Item........: INV�LIDO" & vbCrLf
       End If
       'Alan Neves - MP - 19/03/2013 - Fim
       
       '* Final da mensagem
       MSG = MSG & "        Valor R$..: " & Format(OVal_Pago, "###,###,##0.00") & vbCrLf
       MSG = MSG & vbCrLf
       MSG = MSG & "O Seu Limite de aprova��o � R$: 0,00" & vbCrLf
    End If
    rs.Close
    
    MsgBox MSG, vbOKOnly + vbExclamation
    
    Exit Function
    
TrataErro:
    Call TrataErroGeral("Tecnico_tem_Alcada", Me.name)
    
End Function

Private Function Obtem_Produto_ID(ByVal OSinistro_id As String, ByVal AApolice_id As String, ByVal ASucursal_id As String, ByVal ASeguradora_id As String, ByVal ORamo_id As String) As String
'Alan Neves - MP - 18/03/2013 - N�o utilizada mais a function - As informa��es j� encontram-se nas colections criadas.
    
    Dim SQL         As String
    Dim rs          As rdoResultset
    
    On Error GoTo TrataErro
    
    SQL = "SELECT produto_id"
    SQL = SQL & " FROM sinistro_tb s with(nolock) "
    SQL = SQL & "   INNER JOIN proposta_tb p with(nolock) "
    SQL = SQL & "       ON (p.proposta_id = s.proposta_id)"
    SQL = SQL & " WHERE s.sinistro_id            = " & OSinistro_id
    SQL = SQL & "   AND s.apolice_id             = " & AApolice_id
    SQL = SQL & "   AND s.seguradora_cod_susep   = " & ASeguradora_id
    SQL = SQL & "   AND s.sucursal_seguradora_id = " & ASucursal_id
    SQL = SQL & "   AND s.ramo_id                = " & ORamo_id
    Set rs = rdocn.OpenResultset(SQL)
    If Not rs.EOF Then
       Obtem_Produto_ID = rs("produto_id")
    End If
    rs.Close
    
    Exit Function
    
TrataErro:
    Call TrataErroGeral("Obtem_Produto_ID", Me.name)
    
End Function

Private Sub Obtem_Nome_Produto_E_Nome_Ramo(ByVal OProduto_id As String, ByVal ORamo_id As String, ByRef ONome_Produto As String, ByRef ONome_Ramo As String)
    
    Dim SQL         As String
    Dim rs          As rdoResultset
    
    On Error GoTo TrataErro
    
    '* Obt�m o nome do produto
    SQL = "SELECT nome"
    SQL = SQL & " FROM produto_tb with(nolock) "
    SQL = SQL & " WHERE produto_id  = " & OProduto_id
    Set rs = rdocn.OpenResultset(SQL)
    If Not rs.EOF Then
       ONome_Produto = UCase(rs("nome"))
    End If
    rs.Close
    
    '* Obt�m o nome do ramo
    SQL = "SELECT nome"
    SQL = SQL & " FROM ramo_tb with(nolock) "
    SQL = SQL & " WHERE ramo_id  = " & ORamo_id
    Set rs = rdocn.OpenResultset(SQL)
    If Not rs.EOF Then
       ONome_Ramo = UCase(rs("nome"))
    End If
    rs.Close
    
    Exit Sub
    
TrataErro:
    Call TrataErroGeral("Obtem_Nome_Produto_E_Nome_Ramo", Me.name)
    
End Sub

Private Function Verifica_Aprova_Recusa(Tipo As String, NumPagto As String) As Boolean

Verifica_Aprova_Recusa = True
'If Tipo = "INDENIZA��O" And Verifica_Sin_BB Then
If Tipo = 1 And Verifica_Sin_BB Then
    If Not Verifica_Tecnico Then
        'MsgBox "O Pagamento " & NumPagto & "n�o pode ser aprovado e nem recusado." & vbCrLf & _
               "T�cnico sem cadastro no SISBB." & vbCrLf & _
               "Somente � permitido o pagamento de Honor�rio e Despesas ou Indeniza��o que n�o estejam vinculadas ao Banco do Brasil.", vbCritical
        Verifica_Aprova_Recusa = False
    End If
End If

End Function

Private Function Verifica_Sin_BB() As Boolean
Dim Sin             As Sinist
Dim SQL             As String
Dim rsVerifica      As rdoResultset

Verifica_Sin_BB = True

Set Sin = SinistroEmUso.Item(1)

SQL = ""
SQL = SQL & "Select sinistro_bb from sinistro_bb_tb with(nolock) where sinistro_id = " & Sin.sinistro_id
Set rsVerifica = rdocn1.OpenResultset(SQL)

If IsNull(rsVerifica(0)) Then
    Verifica_Sin_BB = False
End If
rsVerifica.Close

End Function

Private Function Verifica_Tecnico() As Boolean
Dim Sin             As Sinist
Dim SQL             As String
Dim rsTecnico       As rdoResultset

Verifica_Tecnico = True

SQL = ""
'Alan Neves - MP - 12/03/2013 - Inicio
'SQL = SQL & "Select tecnico_id from tecnico_tb with(nolock) where tecnico_id = " & Obtem_Codigo_Tecnico
SQL = SQL & "Select tecnico_id from tecnico_tb with(nolock) where tecnico_id = " & Tecnico_atual
'Alan Neves - MP - 12/03/2013 - Fim

Set rsTecnico = rdocn1.OpenResultset(SQL)
 
If rsTecnico.EOF Or IsNull(rsTecnico(0)) Then
    Verifica_Tecnico = False
End If

rsTecnico.Close

End Function

Private Function Tecnico_tem_chave(sinistro_id As String, Item_val_estimativa As Integer, Seq_Estimativa As Integer) As Boolean
Dim SQL             As String
Dim rsTecnico       As rdoResultset
Tecnico_tem_chave = True

If Item_val_estimativa = 1 Then
    'se o aviso estiver vinculado a um bb
    
    'Alan Neves - MP - 19/03/2013 - Inicio
    'SQL = "select sinistro_bb from pgto_sinistro_tb with(nolock)"
    SQL = "select sinistro_bb from #pgto_sinistro_tb pgto_sinistro_tb with(nolock)"
    'Alan Neves - MP - 19/03/2013 - Fim
    
    SQL = SQL & " where sinistro_id = " & sinistro_id
    SQL = SQL & " and Seq_Estimativa = " & Seq_Estimativa
    SQL = SQL & " and Item_val_estimativa = " & Item_val_estimativa
    Set rsTecnico = rdocn1.OpenResultset(SQL)
    If Not IsNull(rsTecnico!sinistro_bb) Then
        'verifica se h� chave sisbb para o usuario
        'demanda
        SQL = "SELECT CHAVE = t.chave_usuario_SISBB"
        'INC000003832930 - 30/11/2012 Thiago Erustes - SEGAB_DB � somente em AB - Inicio
        If glAmbiente_id = 2 Or glAmbiente_id = 3 Then 'AB
            SQL = SQL & " FROM   SEGAB_DB..usuario_tb u  with(nolock)"
        ElseIf glAmbiente_id = 6 Or glAmbiente_id = 7 Then 'ABS
            SQL = SQL & " FROM   abss.SEGAB_DB.dbo.usuario_tb u  with(nolock)"
        End If
        'INC000003832930 - 30/11/2012 Thiago Erustes - SEGAB_DB � somente em AB - Fim
        SQL = SQL & " INNER JOIN tecnico_tb t with(nolock)"
        SQL = SQL & " ON  ISNULL(SUBSTRING(u.eMail, 1, LEN(t.eMail)), u.login_rede) = t.eMail"
        SQL = SQL & " WHERE  u.cpf      = " & "'" & cUserName & "'"
        SQL = SQL & " AND t.chave_usuario_SISBB IS NOT NULL"
        SQL = SQL & " AND t.chave_usuario_SISBB <> '' "
        SQL = SQL & " AND  u.situacao = 'A'"
        SQL = SQL & " Union"
        SQL = SQL & " SELECT CHAVE = t.chave_usuario_SISBB"
        'INC000003832930 - 30/11/2012 Thiago Erustes - SEGAB_DB � somente em AB - Inicio
        If glAmbiente_id = 2 Or glAmbiente_id = 3 Then 'AB
            SQL = SQL & " FROM   SEGAB_DB..usuario_tb u  with(nolock)"
        ElseIf glAmbiente_id = 6 Or glAmbiente_id = 7 Then 'ABS
            SQL = SQL & " FROM   abss.SEGAB_DB.dbo.usuario_tb u  with(nolock)"
        End If
        'INC000003832930 - 30/11/2012 Thiago Erustes - SEGAB_DB � somente em AB - Fim
        SQL = SQL & " INNER JOIN tecnico_tb t  with(nolock)"
        SQL = SQL & " ON  u.login_rede = t.eMail"
        SQL = SQL & " WHERE  u.cpf      = " & "'" & cUserName & "'"
        SQL = SQL & " AND t.chave_usuario_SISBB IS NOT NULL"
        SQL = SQL & " AND t.chave_usuario_SISBB <> '' "
        SQL = SQL & " AND  u.situacao = 'A'"
        SQL = SQL & " Union"
        SQL = SQL & " SELECT CHAVE = t.chave"
        'INC000003832930 - 30/11/2012 Thiago Erustes - SEGAB_DB � somente em AB - Inicio
        If glAmbiente_id = 2 Or glAmbiente_id = 3 Then 'AB
            SQL = SQL & " FROM   SEGAB_DB..usuario_tb u  with(nolock)"
        ElseIf glAmbiente_id = 6 Or glAmbiente_id = 7 Then 'ABS
            SQL = SQL & " FROM   abss.SEGAB_DB.dbo.usuario_tb u  with(nolock)"
        End If
        'INC000003832930 - 30/11/2012 Thiago Erustes - SEGAB_DB � somente em AB - Fim
        SQL = SQL & " INNER JOIN usuario_SISBB_tb t  with(nolock)"
        SQL = SQL & " ON  u.login_rede = t.login"
        SQL = SQL & " WHERE  u.cpf      = " & "'" & cUserName & "'"
        SQL = SQL & " AND t.chave IS NOT NULL"
        SQL = SQL & " AND t.chave <> '' "
        SQL = SQL & " AND  u.situacao = 'A'"
        'Alterado por Cleber da Stefanini - data: 29/11/2005 - Flow 113708
        rsTecnico.Close
        Set rsTecnico = rdocn.OpenResultset(SQL)
        If IsNull(rsTecnico!Chave) Or rsTecnico.EOF Then
            rsTecnico.Close
            Tecnico_tem_chave = False
            rdocn.RollbackTrans
            MsgBox "Erro na aprova��o do pagamento. " & vbCrLf & "N�o foi encontrada a chave do SISBB", vbOKOnly + vbExclamation
        End If
    Else
        'Alterado por Cleber da Stefanini - data: 29/11/2005 - Flow 113708
        rsTecnico.Close
    End If
End If
'Alterado por Cleber da Stefanini - data: 29/11/2005 - Flow 113708
'rsTecnico.Close
End Function

Private Function SelecionaValorLiquido(ByVal v_sItemEstimativa As String, _
                                       ByVal v_sSinistro_id As String, _
                                       ByVal v_sApolice_id As String, _
                                       ByVal v_sSucursal_id As String, _
                                       ByVal v_sSeguradora_id As String, _
                                       ByVal v_sRamo_id As String, _
                                       ByVal v_SeqPgto As String, _
                                       ByVal v_SeqEstimativa As String) As Currency
    
    Dim rsItensSinistros As rdoResultset
    Dim sTextoSql As String
    
    On Error GoTo ErrMetodo
    
    'Alan Neves - MP - 19/03/2013 - Inicio - Alterado para acessar a temporaria
    sTextoSql = "SELECT SUM( isnull(pgto_sinistro_tb.val_acerto,0) - isnull(pgto_sinistro_tb.val_cosseguro,0) - isnull(pgto_sinistro_tb.val_re_seguro,0)) as val_liquido " & _
                "FROM #pgto_sinistro_tb pgto_sinistro_tb with(nolock) "
    
    sTextoSql = sTextoSql & "WHERE pgto_sinistro_tb.sinistro_id = " & v_sSinistro_id & _
            " AND pgto_sinistro_tb.apolice_id = " & v_sApolice_id & _
            " AND pgto_sinistro_tb.sucursal_seguradora_id = " & v_sSucursal_id & _
            " AND pgto_sinistro_tb.seguradora_cod_susep = " & v_sSeguradora_id & _
            " AND pgto_sinistro_tb.ramo_id = " & v_sRamo_id & _
            " AND pgto_sinistro_tb.seq_pgto = " & v_SeqPgto & _
            " AND pgto_sinistro_tb.seq_estimativa = " & v_SeqEstimativa & _
            " AND pgto_sinistro_tb.item_val_estimativa = " & v_sItemEstimativa
    
    Set rsItensSinistros = rdocn1.OpenResultset(sTextoSql)
    
    If Not rsItensSinistros.EOF Then
            SelecionaValorLiquido = Val(rsItensSinistros("val_liquido").Value)
    End If

ExitMetodo:
    Exit Function

ErrMetodo:
    MsgBox err.Number & " " & err.Description
    
End Function

Private Function SomarTodosPagamentosPendentesLiquido(ByVal v_sItemEstimativa As String, _
                                                        ByVal v_sSinistro_id As String, _
                                                        ByVal v_sApolice_id As String, _
                                                        ByVal v_sSucursal_id As String, _
                                                        ByVal v_sSeguradora_id As String, _
                                                        ByVal v_sRamo_id As String, _
                                                        ByVal v_TipoPagto As String) As Currency
    Dim rsItensSinistros As rdoResultset
    Dim sTextoSql As String
    
    On Error GoTo ErrMetodo
    
    'Alan Neves - MP - 19/03/2013 - Inicio - Alterado para acessar a temporaria
    sTextoSql = "SELECT pgto_sinistro_tb.item_val_estimativa , " & _
                "SUM(isnull(pgto_sinistro_tb.val_acerto,0) - isnull(pgto_sinistro_tb.val_cosseguro,0) - isnull(pgto_sinistro_tb.val_re_seguro,0) ) as val_liquido " & _
                "FROM #pgto_sinistro_tb pgto_sinistro_tb with(nolock) "
    
    sTextoSql = sTextoSql & "WHERE pgto_sinistro_tb.sinistro_id = " & v_sSinistro_id & _
            " AND pgto_sinistro_tb.apolice_id = " & v_sApolice_id & _
            " AND pgto_sinistro_tb.sucursal_seguradora_id = " & v_sSucursal_id & _
            " AND pgto_sinistro_tb.seguradora_cod_susep = " & v_sSeguradora_id & _
            " AND pgto_sinistro_tb.ramo_id = " & v_sRamo_id
    
    sTextoSql = sTextoSql & " AND pgto_sinistro_tb.num_parcela = 1 "
    
    If (voper = "C") Or (voper = "A") Then
        sTextoSql = sTextoSql & " AND ((pgto_sinistro_tb.tp_operacao = 'd') OR "
        sTextoSql = sTextoSql & "      (pgto_sinistro_tb.tp_operacao = 'c' and pgto_sinistro_tb.item_val_estimativa in (4,6) )) "
    Else
        sTextoSql = sTextoSql & " AND pgto_sinistro_tb.tp_operacao = 'd' "
    End If
            
    If UCase(Trim$(v_TipoPagto)) = "T" Then
        
        If voper = "I" Or voper = "IS" Then
            sTextoSql = sTextoSql & " AND pgto_sinistro_tb.chave_autorizacao_sinistro is not null "
            sTextoSql = sTextoSql & " AND (pgto_sinistro_tb.situacao_op = 'n' OR pgto_sinistro_tb.situacao_op = 'a') "
        ElseIf vSituacao <> "'0'" Then
            sTextoSql = sTextoSql & " AND (pgto_sinistro_tb.situacao_op = " & vSituacao & " pgto_sinistro_tb.situacao_op = 'a' "
        End If
    
    Else
        
        sTextoSql = sTextoSql & " AND pgto_sinistro_tb.situacao_op = 'a' "
    
    End If
    
    If frmAcertoCon.cmbCampo.Text = "Voucher" Then
        sTextoSql = sTextoSql & " AND ps_acerto_pagamento_tb.voucher_id = " & frmAcertoCon.txtConteudo
    End If
    
    sTextoSql = sTextoSql & " GROUP BY pgto_sinistro_tb.item_val_estimativa "
    sTextoSql = sTextoSql & " ORDER BY pgto_sinistro_tb.item_val_estimativa "
    
    Set rsItensSinistros = rdocn1.OpenResultset(sTextoSql)
    
    Do While Not rsItensSinistros.EOF
        
        If v_sItemEstimativa = rsItensSinistros("item_val_estimativa").Value Then
            If IsNull((rsItensSinistros("val_liquido").Value)) Then
                SomarTodosPagamentosPendentesLiquido = 0
'                Exit Do
            Else
                SomarTodosPagamentosPendentesLiquido = Val(rsItensSinistros("val_liquido").Value)
                Exit Do
            End If
        End If
        rsItensSinistros.MoveNext
    Loop


ExitMetodo:
    Exit Function

ErrMetodo:
    MsgBox err.Number & " " & err.Description
    
End Function

Private Function SomarTodosPagamentosPendentesAprovados(ByVal v_sItemEstimativa As String, _
                                                        ByVal v_sSinistro_id As String, _
                                                        ByVal v_sApolice_id As String, _
                                                        ByVal v_sSucursal_id As String, _
                                                        ByVal v_sSeguradora_id As String, _
                                                        ByVal v_sRamo_id As String, _
                                                        ByVal v_TipoPagto As String) As Currency
    Dim rsItensSinistros As rdoResultset
    Dim sTextoSql As String
    
    On Error GoTo ErrMetodo
    
    'Alan Neves - MP - 19/03/2013 - Inicio - Alterado para acessar a temporaria
    sTextoSql = "SELECT pgto_sinistro_tb.item_val_estimativa , " & _
            "SUM(isnull(pgto_sinistro_tb.val_acerto,0)) AS val_acerto " & _
            "FROM #pgto_sinistro_tb pgto_sinistro_tb with(nolock) "
    
    sTextoSql = sTextoSql & "WHERE pgto_sinistro_tb.sinistro_id = " & v_sSinistro_id & _
            " AND pgto_sinistro_tb.apolice_id = " & v_sApolice_id & _
            " AND pgto_sinistro_tb.sucursal_seguradora_id = " & v_sSucursal_id & _
            " AND pgto_sinistro_tb.seguradora_cod_susep = " & v_sSeguradora_id & _
            " AND pgto_sinistro_tb.ramo_id = " & v_sRamo_id
    
    sTextoSql = sTextoSql & " AND pgto_sinistro_tb.num_parcela = 1 "
    
    If (voper = "C") Or (voper = "A") Then
        sTextoSql = sTextoSql & " AND ((pgto_sinistro_tb.tp_operacao = 'd') OR "
        sTextoSql = sTextoSql & "      (pgto_sinistro_tb.tp_operacao = 'c' and pgto_sinistro_tb.item_val_estimativa in (4,6) )) "
    Else
        sTextoSql = sTextoSql & " AND pgto_sinistro_tb.tp_operacao = 'd' "
    End If
            
    If UCase(Trim$(v_TipoPagto)) = "T" Then
        
        If voper = "I" Or voper = "IS" Then
            sTextoSql = sTextoSql & " AND pgto_sinistro_tb.chave_autorizacao_sinistro is not null "
            sTextoSql = sTextoSql & " AND (pgto_sinistro_tb.situacao_op = 'n' OR pgto_sinistro_tb.situacao_op = 'a') "
        ElseIf vSituacao <> "'0'" Then
            sTextoSql = sTextoSql & " AND (pgto_sinistro_tb.situacao_op = " & vSituacao & " pgto_sinistro_tb.situacao_op = 'a' "
        End If
    
    Else
        
        sTextoSql = sTextoSql & " AND pgto_sinistro_tb.situacao_op = 'a' "
    
    End If
    
    If frmAcertoCon.cmbCampo.Text = "Voucher" Then
        sTextoSql = sTextoSql & " AND ps_acerto_pagamento_tb.voucher_id = " & frmAcertoCon.txtConteudo
    End If
    
    sTextoSql = sTextoSql & " GROUP BY pgto_sinistro_tb.item_val_estimativa "
    sTextoSql = sTextoSql & " ORDER BY pgto_sinistro_tb.item_val_estimativa "
    
    Set rsItensSinistros = rdocn1.OpenResultset(sTextoSql)
    
    Do While Not rsItensSinistros.EOF
        
        If v_sItemEstimativa = rsItensSinistros("item_val_estimativa").Value Then
            If IsNull((rsItensSinistros("val_acerto").Value)) Then
                SomarTodosPagamentosPendentesAprovados = 0
                Exit Do
            Else
                SomarTodosPagamentosPendentesAprovados = Val(rsItensSinistros("val_acerto").Value)
                Exit Do
            End If
        End If
    
        rsItensSinistros.MoveNext
    Loop

ExitMetodo:
    Exit Function

ErrMetodo:
    MsgBox err.Number & " " & err.Description
    
End Function
Private Function Avaliador_solicitante(ByVal sinistro, ByVal val_estimativa, ByVal Seq_Estimativa, ByVal Seq_Pgto, ByVal Num_Parcela, ByVal beneficiario) As Boolean
    
    'Alan Neves - MP - 12/03/2013 - Inicio - Codigo do t�cnico j� obtido no carregamento do form
    'Dim Tecnico_atual As String
    'Alan Neves - 12/03/2013
    
    Dim SQL As String
    Dim rsSolTecnico As rdoResultset
    Dim bResp As Boolean
    
    bResp = False
    'Alan Neves - MP - 12/03/2013 - Inicio - Codigo do t�cnico j� obtido no carregamento do form
    'Tecnico_atual = Obtem_Codigo_Tecnico
    'Alan Neves - MP - 12/03/2013 - Fim
    
    'Alan Neves - MP - 19/03/2013 - Inicio - Verifica��o na temp
'    SQL = "exec SEGS8047_SPS " & sinistro
'    SQL = SQL & ", " & val_estimativa
'    SQL = SQL & ", " & Seq_Estimativa
'    SQL = SQL & ", " & Seq_Pgto
'    SQL = SQL & ", " & Num_Parcela
'    SQL = SQL & ", " & beneficiario
    
    SQL = "select solicitante_tecnico_id from #pgto_sinistro_tb " & vbNewLine
    SQL = SQL & "where sinistro_id = " & sinistro
    SQL = SQL & "  and item_val_estimativa = " & val_estimativa
    SQL = SQL & "  and seq_estimativa = " & Seq_Estimativa
    SQL = SQL & "  and seq_pgto = " & Seq_Pgto
    SQL = SQL & "  and num_parcela = " & Num_Parcela
    SQL = SQL & "  and beneficiario_id = " & beneficiario
    'Alan Neves - MP - 19/03/2013 - Fim
    
    Set rsSolTecnico = rdocn1.OpenResultset(SQL)
    
'+-----------------------------------------------------------
'| FLOW 19213682 - Registro do usu�rio Solicitante do Saldo
'|
'    If rsSolTecnico("solicitante_tecnico_id") = Tecnico_atual Then
'        Avaliador_solicitante = True
'    Else
'        Avaliador_solicitante = False
'    End If
'    rsSolTecnico.Close
    If Not rsSolTecnico.EOF Then
      If rsSolTecnico("solicitante_tecnico_id") = 229 Then
        rsSolTecnico.Close
    
        SQL = "SELECT TOP 1 solicitante_id " & vbCrLf _
            & "FROM seguros_db.dbo.sinistro_solicita_saldo_tb WITH (NOLOCK) " & vbCrLf _
            & "WHERE sinistro_id=" & sinistro & vbCrLf _
            & "ORDER BY dt_inclusao DESC"
        Set rsSolTecnico = rdocn1.OpenResultset(SQL)
        If rsSolTecnico.EOF Then
          bResp = False
        Else
          If UCase(Trim(rsSolTecnico!solicitante_id & "")) = UCase(Trim(gsCPF)) Then
            'mesmo t�cnico.
            bResp = True
          End If
        End If
      Else
        If rsSolTecnico("solicitante_tecnico_id") = Tecnico_atual Then
          bResp = True
        Else
          bResp = False
        End If
      End If
    End If
    rsSolTecnico.Close
    
    Avaliador_solicitante = bResp
'| FIM - FLOW 19213682 - Registro do usu�rio Solicitante do Saldo
'+----------------------------------------------------------------
    
    
End Function


Private Sub optTodos_Click()
'+-----------------------------------------------------------
'| 19406709 - Melhoria nos limites de sinistro e emissao
'| RN05 - petrauskas - 03/2017
  If produto_regra_especial Then
    verifica_pgto_X_IS
  End If
'| 19406709 - Melhoria nos limites de sinistro e emissao
'+-----------------------------------------------------------

End Sub

' sergio.pires (Nova Consultoria) 18983905 - Corre��o Monet�ria para Saldo Devedor Autom�tico - Inicio
Private Function Lancar_Novo_pagamento(ByVal sSinistro As Sinist) As Boolean

   Lancar_Novo_pagamento = False

   If ((sSinistro.produto_id = 1183) Or (sSinistro.produto_id = 1198) Or (sSinistro.produto_id = 1205)) Then
      If Horario_Limite_Pagto Then
            MsgBox "Altera��o de Situa��o do Pagamento Executada Com Sucesso!", vbInformation, Caption
            Exibiu_Msg = True
            If (MsgBox("Deseja lan�ar novo pagamento?", vbYesNo, Caption)) = 6 Then
               vChamadaSegp = sSinistro.sinistro_id + "." + Busca_Ultima_Estimativa(sSinistro.sinistro_id)
               Lancou_Pagamento = True
               Lancar_Novo_pagamento = True
            End If
      End If
   End If

End Function

Private Function Busca_Ultima_Estimativa(ByVal iSinistro_id As String) As String

   Dim sSQL As String
   Dim rs As rdoResultset
   
   sSQL = ""
   sSQL = "      SELECT seq_estimativa "
   sSQL = sSQL + " FROM seguros_db.dbo.sinistro_estimativa_tb WITH (NOLOCK) "
   sSQL = sSQL + "WHERE sinistro_id = " & iSinistro_id
   sSQL = sSQL + "  AND item_val_estimativa = 1 "
   sSQL = sSQL + "  AND dt_fim_estimativa IS NULL "
   
   Set rs = rdocn1.OpenResultset(sSQL)
   
   If Not rs.EOF Then
       Busca_Ultima_Estimativa = rs("seq_estimativa")
   Else
       Busca_Ultima_Estimativa = 0
   End If
   
   rs.Close

End Function

Private Function Horario_Limite_Pagto() As Boolean

    If (Hour(DataHoraAmbiente) < 14) Or ((Hour(DataHoraAmbiente) = 14) And (Minute(DataHoraAmbiente) = 0)) Then
       Horario_Limite_Pagto = True
    Else
       Horario_Limite_Pagto = False
    End If

End Function

Public Function Monta_Parametro_Atual(ByVal OCodPrograma As String) As String

Dim sParametro As String

On Local Error GoTo TrataErro

    'FlowBR 152374 - Ana Paula - Stefanini - 02/08/2006
    gsDHEntrada = DataHoraAmbiente '(, objAmbiente)
    
    ' agrupa os dados para a criptografia (Usu�rio, Computador, MAC, IP, Id_prog, Data e Hora)
    sParametro = OCodPrograma & "|" & glAmbiente_id & "|" & glEmpresa_id & "|" & gsCPF
    
    'Criptografa 2 vezes, um com chave, e coloca os valores em Ascii separados por virgulas
    sParametro = Cifra(sParametro)
    sParametro = Cifra(sParametro, gsCHAVESISTEMA)
    sParametro = ConvStringASCII(sParametro)
    
    Monta_Parametro_Atual = sParametro
    
    Exit Function
    
TrataErro:
  Select Case err
    Case glERRO_MONTA_PARAMETRO
      MontaErro gsERRO_MONTA_PARAMETRO, OCodPrograma
  End Select
  Call TrataErroGeral("Monta_Parametro_Atual", "frmAcerto")
  Call TerminaSEGBR
End Function
' sergio.pires (Nova Consultoria) 18983905 - Corre��o Monet�ria para Saldo Devedor Autom�tico - Fim


