VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFlxGrd.ocx"
Begin VB.Form frm_acerto_lote 
   Caption         =   "SEG12018 - Aprovar Pagamento em Lote"
   ClientHeight    =   8805
   ClientLeft      =   3915
   ClientTop       =   1830
   ClientWidth     =   15645
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   8805
   ScaleWidth      =   15645
   Begin VB.Frame fra_comandos 
      BorderStyle     =   0  'None
      Height          =   1755
      Left            =   12840
      TabIndex        =   22
      Top             =   6960
      Width           =   1875
      Begin VB.CommandButton cmd_marcar 
         Caption         =   "Marcar Todos"
         Height          =   375
         Left            =   60
         TabIndex        =   26
         Top             =   120
         Width           =   1755
      End
      Begin VB.CommandButton cmd_desmarcar 
         Caption         =   "Desmarcar Todos"
         Height          =   375
         Left            =   60
         TabIndex        =   25
         Top             =   540
         Width           =   1755
      End
      Begin VB.CommandButton cmd_aprovar 
         Caption         =   "Aprovar Selecionados"
         Height          =   375
         Left            =   60
         TabIndex        =   24
         Top             =   960
         Width           =   1755
      End
      Begin VB.CommandButton cmd_sair 
         Caption         =   "Sair"
         Height          =   375
         Left            =   60
         TabIndex        =   23
         Top             =   1380
         Width           =   1755
      End
   End
   Begin VB.Frame fra_observacao 
      Height          =   1695
      Left            =   120
      TabIndex        =   20
      Top             =   7020
      Width           =   12675
      Begin VB.TextBox lbl_observacao 
         Appearance      =   0  'Flat
         BackColor       =   &H8000000F&
         BorderStyle     =   0  'None
         Height          =   1155
         Left            =   60
         Locked          =   -1  'True
         TabIndex        =   21
         Top             =   120
         Width           =   12495
      End
      Begin VB.Label lblsel 
         AutoSize        =   -1  'True
         Caption         =   "listados: 0          bloqueados: 0          selecionados: 0"
         Height          =   195
         Left            =   120
         TabIndex        =   27
         Top             =   1380
         Width           =   3720
      End
   End
   Begin VB.Frame fra_pesquisa 
      Height          =   2355
      Left            =   60
      TabIndex        =   2
      Top             =   60
      Width           =   15495
      Begin VB.ComboBox cmb_item_val_estimativa 
         Height          =   315
         Left            =   5340
         TabIndex        =   30
         Text            =   "Combo4"
         Top             =   1140
         Width           =   3015
      End
      Begin VB.ComboBox cmb_tp_pesquisa 
         Height          =   315
         Left            =   120
         TabIndex        =   28
         Text            =   "Combo4"
         Top             =   1860
         Width           =   3015
      End
      Begin VB.CheckBox chk_somente_alcada 
         Caption         =   "Somente casos com al�ada"
         Height          =   195
         Left            =   12300
         TabIndex        =   17
         Top             =   480
         Width           =   2475
      End
      Begin VB.TextBox txt_max_valor 
         Alignment       =   1  'Right Justify
         Height          =   285
         Left            =   14010
         TabIndex        =   16
         Top             =   1230
         Width           =   1395
      End
      Begin VB.CommandButton cmd_Pesquisar 
         Caption         =   "Pesquisar"
         Height          =   435
         Left            =   13440
         TabIndex        =   15
         Top             =   1740
         Width           =   1575
      End
      Begin VB.TextBox txt_min_valor 
         Alignment       =   1  'Right Justify
         Height          =   285
         Left            =   12270
         TabIndex        =   14
         Text            =   "0,01"
         Top             =   1230
         Width           =   1395
      End
      Begin VB.ComboBox cmb_ramo 
         Height          =   315
         Left            =   120
         TabIndex        =   7
         Text            =   "1 Seguro Agr�cola sem cobertura do FESR"
         Top             =   420
         Width           =   4995
      End
      Begin VB.ComboBox cmb_produto 
         Height          =   315
         Left            =   5340
         TabIndex        =   6
         Text            =   "1204 BB SEGURO AGR�COLA FATURAMENTO"
         Top             =   420
         Width           =   6495
      End
      Begin VB.ComboBox cmb_forma_pgto 
         Height          =   315
         Left            =   8580
         TabIndex        =   5
         Text            =   "CR�DITO EM CONTA CORRENTE"
         Top             =   1200
         Width           =   3135
      End
      Begin VB.ComboBox cmb_tecnico 
         Height          =   315
         Left            =   120
         TabIndex        =   4
         Text            =   "Combo4"
         Top             =   1140
         Width           =   4995
      End
      Begin VB.TextBox txt_beneficiario 
         Height          =   285
         Left            =   3180
         TabIndex        =   3
         Top             =   1860
         Width           =   9795
      End
      Begin VB.Label lbl 
         AutoSize        =   -1  'True
         Caption         =   "�tem Pagamento"
         Height          =   195
         Index           =   4
         Left            =   5340
         TabIndex        =   31
         Top             =   900
         Width           =   1395
      End
      Begin VB.Label lbl 
         AutoSize        =   -1  'True
         Caption         =   "Pesquisar por:"
         Height          =   195
         Index           =   8
         Left            =   120
         TabIndex        =   29
         Top             =   1620
         Width           =   1005
      End
      Begin VB.Label lbl 
         AutoSize        =   -1  'True
         Caption         =   "at�"
         Height          =   195
         Index           =   7
         Left            =   13680
         TabIndex        =   19
         Top             =   1320
         Width           =   225
      End
      Begin VB.Label lbl 
         AutoSize        =   -1  'True
         Caption         =   "de"
         Height          =   195
         Index           =   6
         Left            =   12000
         TabIndex        =   18
         Top             =   1320
         Width           =   180
      End
      Begin VB.Label lbl 
         AutoSize        =   -1  'True
         Caption         =   "Valor do acerto"
         Height          =   195
         Index           =   5
         Left            =   12300
         TabIndex        =   13
         Top             =   960
         Width           =   1080
      End
      Begin VB.Label lbl 
         AutoSize        =   -1  'True
         Caption         =   "Ramo"
         Height          =   195
         Index           =   0
         Left            =   120
         TabIndex        =   12
         Top             =   180
         Width           =   420
      End
      Begin VB.Label lbl 
         AutoSize        =   -1  'True
         Caption         =   "Produto"
         Height          =   195
         Index           =   1
         Left            =   5400
         TabIndex        =   11
         Top             =   180
         Width           =   555
      End
      Begin VB.Label lbl 
         AutoSize        =   -1  'True
         Caption         =   "Tipo Pgto."
         Height          =   195
         Index           =   2
         Left            =   8580
         TabIndex        =   10
         Top             =   960
         Width           =   735
      End
      Begin VB.Label lbl 
         AutoSize        =   -1  'True
         Caption         =   "T�cnico Solicitante"
         Height          =   195
         Index           =   3
         Left            =   120
         TabIndex        =   9
         Top             =   900
         Width           =   1365
      End
      Begin VB.Label lbl_pesquisa 
         Caption         =   "Sem Pesquisa"
         Height          =   195
         Left            =   3240
         TabIndex        =   8
         Top             =   1620
         Width           =   4125
      End
   End
   Begin VB.Frame fra_grid 
      Height          =   4515
      Left            =   120
      TabIndex        =   0
      Top             =   2400
      Width           =   14535
      Begin MSFlexGridLib.MSFlexGrid grid 
         Height          =   4515
         Left            =   0
         TabIndex        =   1
         Top             =   60
         Width           =   14595
         _ExtentX        =   25744
         _ExtentY        =   7964
         _Version        =   393216
         Cols            =   25
         FixedCols       =   0
         FormatString    =   $"frm_acerto_lote.frx":0000
      End
   End
End
Attribute VB_Name = "frm_acerto_lote"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Const cnt_selecionado = "R"
Private Const cnt_nao_selecionado = "�"
Private Const cnt_bloqueado = "S"
Private p_cod_tecnico As Long
Private l_listados As Long, l_bloqueados As Long, l_selecionados As Long

Private Sub InicializaInterfaceLocal_Form()

Dim i As Integer
Dim rs As rdoResultset
Dim sSql As String, sAux As String

    'ajustar grid
    grid.Rows = 1
    grid.ColWidth(1) = 1050
    grid.ColWidth(2) = 1700
    grid.ColWidth(3) = 1995
    grid.ColWidth(4) = 2000
    grid.ColWidth(4) = 1125
    grid.ColWidth(6) = 1800
    grid.ColWidth(7) = 2000
    grid.ColWidth(8) = 680
    grid.ColWidth(9) = 1000
    grid.ColWidth(10) = 1000
    grid.ColWidth(11) = 1000
    grid.ColWidth(12) = 600
    For i = 13 To grid.Cols - 1
        grid.ColWidth(i) = 0
    Next i
    
    lblsel.Caption = "listados: 0" & Space$(10) & "bloqueados: 0" & Space$(10) & "selecionados: 0"
    
    'carregar combo ramos
    cmb_ramo.Clear: cmb_ramo.Tag = "-1"
    cmb_ramo.AddItem ("{todos}"): cmb_ramo.ItemData(cmb_ramo.NewIndex) = 0
    sSql = "exec seguros_db.dbo.SEGS13882_SPS"
    Set rs = rdocn.OpenResultset(sSql)
    Do While Not rs.EOF
        sAux = rs!ramo_id & " - " & rs!Nome
        cmb_ramo.AddItem (sAux): cmb_ramo.ItemData(cmb_ramo.NewIndex) = rs!ramo_id
        rs.MoveNext
    Loop
    cmb_ramo.ListIndex = 0
    rs.Close
    
    cmb_forma_pgto.Clear
    cmb_forma_pgto.AddItem "{todos}": cmb_forma_pgto.ItemData(cmb_forma_pgto.NewIndex) = 0
    cmb_forma_pgto.AddItem "CHEQUE": cmb_forma_pgto.ItemData(cmb_forma_pgto.NewIndex) = 4
    cmb_forma_pgto.AddItem "CR�DITO EM CONTA": cmb_forma_pgto.ItemData(cmb_forma_pgto.NewIndex) = 11
    cmb_forma_pgto.AddItem "ORPAG": cmb_forma_pgto.ItemData(cmb_forma_pgto.NewIndex) = 12
    cmb_forma_pgto.AddItem "TRANSFERENCIA BANCARIA": cmb_forma_pgto.ItemData(cmb_forma_pgto.NewIndex) = 13
    cmb_forma_pgto.AddItem "POUPAN�A": cmb_forma_pgto.ItemData(cmb_forma_pgto.NewIndex) = 14
    cmb_forma_pgto.AddItem ("SEGUR"): cmb_forma_pgto.ItemData(cmb_forma_pgto.NewIndex) = 5

    cmb_forma_pgto.ListIndex = 0
'---------------------------------------------------------------------------------------------

    cmb_item_val_estimativa.Clear
    cmb_item_val_estimativa.AddItem "{todos}": cmb_item_val_estimativa.ItemData(cmb_item_val_estimativa.NewIndex) = 0
    cmb_item_val_estimativa.AddItem "Indeniza��o": cmb_item_val_estimativa.ItemData(cmb_item_val_estimativa.NewIndex) = 1
    cmb_item_val_estimativa.AddItem "Honor�rios": cmb_item_val_estimativa.ItemData(cmb_item_val_estimativa.NewIndex) = 2
    cmb_item_val_estimativa.AddItem "Despesas": cmb_item_val_estimativa.ItemData(cmb_item_val_estimativa.NewIndex) = 3
    cmb_item_val_estimativa.AddItem "Ressarcimento": cmb_item_val_estimativa.ItemData(cmb_item_val_estimativa.NewIndex) = 4
    cmb_item_val_estimativa.AddItem "Desp. ressarcimento": cmb_item_val_estimativa.ItemData(cmb_item_val_estimativa.NewIndex) = 5
    cmb_item_val_estimativa.AddItem "Salvados": cmb_item_val_estimativa.ItemData(cmb_item_val_estimativa.NewIndex) = 6
    cmb_item_val_estimativa.AddItem "Desp. salvados": cmb_item_val_estimativa.ItemData(cmb_item_val_estimativa.NewIndex) = 7
    cmb_item_val_estimativa.AddItem "Indeniz. renda": cmb_item_val_estimativa.ItemData(cmb_item_val_estimativa.NewIndex) = 8

    cmb_item_val_estimativa.ListIndex = 0
'---------------------------------------------------------------------------------------------
    cmb_tp_pesquisa.Clear
    cmb_tp_pesquisa.AddItem "{nenhuma}": cmb_tp_pesquisa.ItemData(cmb_tp_pesquisa.NewIndex) = 0
    cmb_tp_pesquisa.AddItem "Nome do Benefici�rio": cmb_tp_pesquisa.ItemData(cmb_tp_pesquisa.NewIndex) = 1
    cmb_tp_pesquisa.AddItem "CPF/CNPJ do Benefici�rio": cmb_tp_pesquisa.ItemData(cmb_tp_pesquisa.NewIndex) = 2
    cmb_tp_pesquisa.AddItem "N� da Nota Fiscal": cmb_tp_pesquisa.ItemData(cmb_tp_pesquisa.NewIndex) = 3
    cmb_tp_pesquisa.AddItem "Sinistro_id": cmb_tp_pesquisa.ItemData(cmb_tp_pesquisa.NewIndex) = 4
    cmb_tp_pesquisa.AddItem "Sinistro BB": cmb_tp_pesquisa.ItemData(cmb_tp_pesquisa.NewIndex) = 5
    
    cmb_tp_pesquisa.ListIndex = 0
'---------------------------------------------------------------------------------------------
    cmb_tecnico.Clear
    cmb_tecnico.AddItem ("{todos}"): cmb_tecnico.ItemData(cmb_tecnico.NewIndex) = 0
    
    sSql = "exec seguros_db.dbo.SEGS13885_SPS"
    Set rs = rdocn.OpenResultset(sSql)
    Do While Not rs.EOF
        sAux = rs!Nome
        cmb_tecnico.AddItem (sAux): cmb_tecnico.ItemData(cmb_tecnico.NewIndex) = rs!Tecnico_id
        rs.MoveNext
    Loop
    cmb_tecnico.ListIndex = 0
    rs.Close
    
    Set rs = Nothing
End Sub
Private Function Obtem_Codigo_Tecnico() As Integer
    
    Dim sSql        As String
    Dim rs          As rdoResultset
    Dim OLogin      As String
    
    On Error GoTo TrataErro
    
    Obtem_Codigo_Tecnico = 0
    
    If glAmbiente_id < 5 Then
        OLogin = BuscaLogin(cUserName)
    Else
        OLogin = BuscaLogin(cUserName, 1)
    End If
    
    sSql = "select distinct tecnico.tecnico_id " & vbCrLf _
         & "from tecnico_tb tecnico with (nolock) " & vbCrLf _
         & "where email = '" & OLogin & "' " & vbCrLf _
         & "  and exists (select 1 " & vbCrLf _
         & "              from limite_aprovacao_pgto_tb with (nolock) " & vbCrLf _
         & "              where tecnico_id = tecnico.tecnico_id " & vbCrLf _
         & "                and dt_fim_vigencia IS NULL) " & vbCrLf

    Set rs = rdocn1.OpenResultset(sSql)
        
    If Not rs.EOF Then
        Obtem_Codigo_Tecnico = rs("tecnico_id")
    End If
    rs.Close: Set rs = Nothing
    
    fra_comandos.Enabled = True
    fra_grid.Enabled = True
    fra_observacao.Enabled = True
    fra_pesquisa.Enabled = True

Exit Function
    
TrataErro:
    Call TrataErroGeral("frm_acerto_lote.Obtem_Codigo_Tecnico", Me.name)
    
End Function
Private Sub aprovar_marcados()

    Dim i As Integer
    Dim sSql As String
    Dim rs As rdoResultset
    Dim l_qt_ok As Long, l_qt_err As Long
    
    
    lbl_observacao.Text = ""
    Screen.MousePointer = vbHourglass
    DoEvents
    
        ' Cesar Santos CONFITEC - (C00214342)- inicio 09/03/2020
        
        For i = 1 To grid.Rows - 1
                If grid.TextMatrix(i, 0) = cnt_selecionado Then
                      sSql = ""
                      sSql = "EXEC SEGS14673_SPS " & grid.TextMatrix(i, 1) & _
                                                       "," & MudaVirgulaParaPonto(Format(grid.TextMatrix(i, 9), "###########0.00")) & _
                                                       "," & MudaVirgulaParaPonto(Format(grid.TextMatrix(i, 10), "###########0.00")) & _
                                                       "," & "NULL" & _
                                                       "," & "5" & _
                                                       "," & grid.TextMatrix(i, 22) & _
                                                       "," & "T"
        
                   Set rs = rdocn.OpenResultset(sSql)
                   
                   If Not rs.EOF Then
                        While Not rs.EOF
                            If ((rs!RISCO_DUPLICIDADE) = "S") Then
                        
                                If MsgBox("Aten��o! J� existe um pagamento para o sinistro: " & (rs!sinistro_id) & ", gentileza certificar que n�o � duplicidade." & vbCrLf & "Deseja aprovar?", vbYesNo + vbInformation + vbDefaultButton2) = vbNo Then
                                    grid.TextMatrix(i, 0) = cnt_nao_selecionado
                                     l_selecionados = l_selecionados - 1
                                     lblsel.Caption = "listados: " & Format(l_listados, "#,##0") & Space$(10) & "bloqueados: " & Format(l_bloqueados, "#,##0") & Space$(10) & "selecionados: " & Format(l_selecionados, "#,##0")
                                End If
                                
                            End If
                        
                        rs.MoveNext
                        Wend
                    End If
                End If
          Next i
        
        Set rs = Nothing
      
    ' Cesar Santos CONFITEC - (C00214342)- fim 09/03/2020
    
    For i = 1 To grid.Rows - 1
        If grid.TextMatrix(i, 0) = cnt_selecionado Then
            lbl_observacao.Text = "aprovando sinistro " & grid.TextMatrix(i, 1)
            DoEvents
            
            On Error GoTo errloop
            rdocn.BeginTrans
            
            sSql = "exec seguros_db.dbo.pgto_sinistro_aprova_spu " _
                 & "@sinistro_id = " & grid.TextMatrix(i, 1) & ", " _
                 & "@apolice_id = " & grid.TextMatrix(i, 14) & ", " _
                 & "@sucursal_seguradora_id = " & grid.TextMatrix(i, 15) & ", " _
                 & "@seguradora_cod_susep = " & grid.TextMatrix(i, 16) & ", " _
                 & "@ramo_id = " & grid.TextMatrix(i, 17) & ", " _
                 & "@item_val_estimativa = " & grid.TextMatrix(i, 18) & ", " _
                 & "@seq_estimativa = " & grid.TextMatrix(i, 19) & ", " _
                 & "@seq_pgto = " & grid.TextMatrix(i, 20) & ", " _
                 & "@num_parcela = " & grid.TextMatrix(i, 21) & ", " _
                 & "@beneficiario_id = " & grid.TextMatrix(i, 22) & ", " _
                 & "@unitario = 's', @resultado = 'a', @motivo = '', " _
                 & "@Situacao = " & grid.TextMatrix(i, 23) & ", " _
                 & "@Dt_aprovacao = '" & Format(Data_Sistema, "yyyymmdd") & "', " _
                 & "@usuario = '" & cUserName & "', " _
                 & "@relprestid = NULL, " _
                 & "@tipobenef = NULL "
                
            Set rs = rdocn.OpenResultset(sSql)
            rs.Close
    
'            Call grava_detalhamento_sinistro(grid.TextMatrix(i, 1) _
'                                           , grid.TextMatrix(i, 11) _
'                                           , grid.TextMatrix(i, 12) _
'                                           , grid.TextMatrix(i, 13) _
'                                           , grid.TextMatrix(i, 14) _
'                                           , grid.TextMatrix(i, 7))
'
'            MsgBox (1 / 0)
            grid.TextMatrix(i, 0) = "�"
            grid.Row = i: grid.Col = 0: grid.CellForeColor = vbBlue
            l_selecionados = l_selecionados - 1
        
            rdocn.CommitTrans
            l_qt_ok = l_qt_ok + 1
continue_loop:
            On Error GoTo 0
        End If
    Next i
    
    lbl_observacao.Text = ""
    lblsel.Caption = "listados: " & Format(l_listados, "#,##0") & Space$(10) & "bloqueados: " & Format(l_bloqueados, "#,##0") & Space$(10) & "selecionados: " & Format(l_selecionados, "#,##0")
    Screen.MousePointer = vbNormal
    DoEvents

    If l_qt_ok > 0 And l_qt_err = 0 Then
      Call MsgBox("Todos os pagamentos aprovados com sucesso", vbInformation)
    ElseIf l_qt_ok > 0 And l_qt_err > 0 Then
      Call MsgBox("Foram aprovados " & l_qt_ok & " pagamentos com sucesso" & vbCr & "Ocorreram erros em " & l_qt_err & " pagamentos", vbInformation)
    Else
      Call MsgBox("N�o foi poss�vel aprovar nenhum pagamentos", vbExclamation)
    End If

Exit Sub


errloop:
    rdocn.RollbackTrans
    If i > 0 Then
        grid.TextMatrix(i, 0) = "�"
        grid.Row = i: grid.Col = 0: grid.CellForeColor = vbRed
    End If
    l_qt_err = l_qt_err + 1
    
    Resume continue_loop

err:
    

End Sub

Private Sub cmb_ramo_Click()
Dim ramo_id As Long
Dim rs As rdoResultset
Dim sSql As String, sAux As String

    If Val(cmb_ramo.Tag) <> cmb_ramo.ListIndex Then
        If cmb_ramo.ListIndex >= 0 Then
            ramo_id = cmb_ramo.ItemData(cmb_ramo.ListIndex)
        Else
            ramo_id = 0
        End If
    
        cmb_produto.Clear
        cmb_produto.AddItem ("{todos}"): cmb_produto.ItemData(cmb_produto.NewIndex) = 0
        
        sSql = "exec seguros_db.dbo.SEGS13883_SPS @ramo_id = " & ramo_id
        
        Set rs = rdocn.OpenResultset(sSql)
        Do While Not rs.EOF
            sAux = rs!produto_id & " - " & rs!Nome
            cmb_produto.AddItem (sAux): cmb_produto.ItemData(cmb_produto.NewIndex) = rs!produto_id
            rs.MoveNext
        Loop
        rs.Close: Set rs = Nothing
        cmb_produto.ListIndex = 0
        cmb_ramo.Tag = cmb_ramo.ListIndex
    End If

End Sub

Private Sub cmb_tp_pesquisa_Click()
    If cmb_tp_pesquisa.ItemData(cmb_tp_pesquisa.ListIndex) = 0 Then
        lbl_pesquisa.Caption = ""
        txt_beneficiario.Enabled = False
    Else
    lbl_pesquisa.Caption = cmb_tp_pesquisa.Text & ":"
        txt_beneficiario.Enabled = True
        txt_beneficiario.SetFocus
        txt_beneficiario.SelStart = 0
        txt_beneficiario.SelLength = Len(txt_beneficiario.Text)
    End If
End Sub

Private Sub cmd_aprovar_Click()
    If MsgBox("Confirma a aprova��o de " & Format(l_selecionados, "#,##0") & " pagamentos selecionados?", vbQuestion + vbYesNo + vbDefaultButton2) = vbYes Then
        Call aprovar_marcados
    End If
End Sub

Private Sub cmd_desmarcar_Click()
    Dim i As Integer
    For i = 1 To grid.Rows - 1
        If grid.TextMatrix(i, 0) = cnt_selecionado Then
            grid.TextMatrix(i, 0) = cnt_nao_selecionado
        End If
    Next i
    l_selecionados = 0
    lblsel.Caption = "listados: " & Format(l_listados, "#,##0") & Space$(10) & "bloqueados: " & Format(l_bloqueados, "#,##0") & Space$(10) & "selecionados: " & Format(l_selecionados, "#,##0")
End Sub

Private Sub cmd_marcar_Click()
    Dim i As Integer
    For i = 1 To grid.Rows - 1
        If grid.TextMatrix(i, 0) = cnt_nao_selecionado Then
            grid.TextMatrix(i, 0) = cnt_selecionado
            l_selecionados = l_selecionados + 1
        End If
    Next i
    lblsel.Caption = "listados: " & Format(l_listados, "#,##0") & Space$(10) & "bloqueados: " & Format(l_bloqueados, "#,##0") & Space$(10) & "selecionados: " & Format(l_selecionados, "#,##0")
End Sub

Private Sub cmd_Pesquisar_Click()
    Dim rs As rdoResultset
    Dim i As Integer
    Dim sSql As String
    Dim sLin As String
    Dim sAux As String
    
    grid.Rows = 1
    lbl_observacao.Text = ""
    Screen.MousePointer = vbHourglass
    DoEvents
    lblsel.Caption = "listados: 0" & Space$(10) & "bloqueados: 0" & Space$(10) & "selecionados: 0"
    l_listados = 0: l_bloqueados = 0: l_selecionados = 0
    
    sSql = "exec SEGS13886_SPS " _
         & "@tecnico_atual_id = " & p_cod_tecnico _
         & ", @so_com_alcada = '" & IIf(chk_somente_alcada = vbChecked, "s", "n") & "'"
    
    If cmb_ramo.ListIndex >= 0 Then
        sSql = sSql & IIf(cmb_ramo.ItemData(cmb_ramo.ListIndex) > 0, ", @ramo_id = " & cmb_ramo.ItemData(cmb_ramo.ListIndex), "")
    End If
    
    If cmb_produto.ListIndex >= 0 Then
        sSql = sSql & IIf(cmb_produto.ItemData(cmb_produto.ListIndex) > 0, ", @produto_id = " & cmb_produto.ItemData(cmb_produto.ListIndex), "")
    End If
    
    If cmb_forma_pgto.ListIndex >= 0 Then
        sSql = sSql & IIf(cmb_forma_pgto.ItemData(cmb_forma_pgto.ListIndex) > 0, ", @forma_pgto_id = " & cmb_forma_pgto.ItemData(cmb_forma_pgto.ListIndex), "")
    End If
    
    If cmb_tecnico.ListIndex >= 0 Then
        sSql = sSql & IIf(cmb_tecnico.ItemData(cmb_tecnico.ListIndex) > 0, ", @tec_solicitante = " & cmb_tecnico.ItemData(cmb_tecnico.ListIndex), "")
    End If
    
    If IsNumeric(txt_min_valor.Text) Then
        sAux = Replace(Replace(txt_min_valor.Text, ".", ""), ",", ".")
        sSql = sSql & ", @min_acerto = " & sAux
    End If

    If IsNumeric(txt_max_valor.Text) Then
        sAux = Replace(Replace(txt_max_valor.Text, ".", ""), ",", ".")
        sSql = sSql & ", @max_acerto = " & sAux
    End If

    If cmb_item_val_estimativa.ListIndex > 0 Then
        sSql = sSql & ", @item_val_estimativa = " & cmb_item_val_estimativa.ItemData(cmb_item_val_estimativa.ListIndex)
    End If
'    If Trim(txt_beneficiario.Text) <> "" Then
'        sAux = Trim(txt_beneficiario.Text)
'        If InStr(sAux, "%") = 0 Then
'            sAux = "%" & sAux & "%"
'        End If
'        sSql = sSql & "   , @beneficiario = '" & sAux & "'"
'    End If
    
    If cmb_tp_pesquisa.ListIndex > 0 Then
        i = cmb_tp_pesquisa.ItemData(cmb_tp_pesquisa.ListIndex)
        sAux = txt_beneficiario.Text
        If i <> 1 Then
        
            sAux = Trim(Replace(Replace(Replace(Replace(sAux, ".", ""), "-", ""), "/", ""), "%", ""))
        End If
        
        If sAux = vbNullString Then
            MsgBox "O preenchimento do valor da pesquisa � obrigat�rio", vbExclamation
            txt_beneficiario.SetFocus
            Exit Sub
        End If
        
        sSql = sSql & ", @tp_pesquisa = " & i _
                    & ", @valor_pesquisa = "
        Select Case i
        Case 1 'pesquisa por nome de benefici�rio
            If InStr(sAux, "%") = 0 Then
                sAux = "%" & sAux & "%"
            End If
            sSql = sSql & "'" & Replace(sAux, "'", " ") & "'"
        Case 2 'pesquisa por cpf/cnpf do benefici�rio
            sSql = sSql & "'" & Replace(sAux, "'", "") & "'"
        Case 3 'pesquisa por nro da nota
            sSql = sSql & "'" & Replace(sAux, "'", "") & "'"
        Case 4 'pesquisa por Sinistro_id
            sSql = sSql & "'" & Replace(sAux, "'", "") & "'"
        Case 5 'pesquisa por Sinistro_BB
            sSql = sSql & "'" & Replace(sAux, "'", "") & "'"
        End Select
    End If
    
    
    Set rs = rdocn.OpenResultset(sSql)
    
    grid.Visible = False
    
    Do While Not rs.EOF
        sLin = IIf(Trim(rs!obs & "") = "", cnt_nao_selecionado, cnt_bloqueado) & vbTab _
             & rs!sinistro_id & vbTab
        
        Select Case rs!Item_val_estimativa
            Case 1: sLin = sLin & "Indeniza��o"
            Case 2: sLin = sLin & "Honor�rios"
            Case 3: sLin = sLin & "Despesas"
            Case 4: sLin = sLin & "Ressarcimento"
            Case 5: sLin = sLin & "Desp. Ressarcimento"
            Case 6: sLin = sLin & "Salvados"
            Case 7: sLin = sLin & "Desp. Salvados"
        End Select
        
        sLin = sLin & vbTab & " " & rs!produto_id & " - " & Trim(rs!produto) & vbTab _
                    & rs!beneficiario & vbTab
        
        If IsNull(rs!dt_solicitacao_pgto) Then
            sLin = sLin & vbTab & vbTab
        Else
            sLin = sLin & Format(rs!dt_solicitacao_pgto, "dd-mm-yyyy") & vbTab
        End If
            
            
        Select Case rs!forma_pgto_id
            Case 4: sLin = sLin & "Cheque"
            Case 11: sLin = sLin & "Cr�dito em Conta"
            Case 12: sLin = sLin & "Orpag"
            Case 13: sLin = sLin & "Transfer�ncia Banc�ria"
            Case 14: sLin = sLin & "Poupan�a"
            Case 5: sLin = sLin & "Segur"
        End Select
        
        sLin = sLin & vbTab & Trim(rs!solicitante & "") & vbTab _
                    & rs!Num_Nota_Fiscal & vbTab _
                    & Format(Val(rs!val_Acerto), "#,##0.00") & vbTab _
                    & Format(Val(rs!val_Correcao), "#,##0.00") & vbTab _
                    & Format(Val(rs!val_estimado), "#,##0.00") & vbTab _
                    & rs!alcada & vbTab
             
        'N�o vis�veis
        sLin = sLin & Trim(rs!obs & "") & vbTab _
                    & rs!Apolice_id & vbTab _
                    & rs!sucursal_seguradora_id & vbTab _
                    & rs!seguradora_cod_susep & vbTab _
                    & rs!ramo_id & vbTab _
                    & rs!Item_val_estimativa & vbTab _
                    & rs!Seq_Estimativa & vbTab _
                    & rs!Seq_Pgto & vbTab _
                    & rs!Num_Parcela & vbTab _
                    & rs!Beneficiario_id & vbTab _
                    & rs!situacao_sinistro & vbTab _
                    & rs!status_saldo_dev_online

        grid.AddItem sLin
        grid.Row = grid.Rows - 1: grid.Col = 0
        grid.CellFontName = "Wingdings 2": grid.CellFontSize = 14: grid.CellAlignment = flexAlignCenterCenter
        l_listados = l_listados + 1
        
        If Trim(rs!obs & "") <> "" Then
            For i = 0 To grid.Cols - 1
                grid.Col = i: grid.CellForeColor = vbRed
            Next i
            l_bloqueados = l_bloqueados + 1
        End If
        
        rs.MoveNext
    Loop
    rs.Close: Set rs = Nothing
    grid.Visible = True
    lblsel.Caption = "listados: " & Format(l_listados, "#,##0") & Space$(10) & "bloqueados: " & Format(l_bloqueados, "#,##0") & Space$(10) & "selecionados: " & Format(l_selecionados, "#,##0")
    Screen.MousePointer = vbNormal
    DoEvents
    
End Sub

Private Sub cmd_sair_Click()
    
    End
End Sub

Private Sub Form_Load()
    Me.Caption = "SEG12018 - Aprovar Pagamento em Lote - " & Ambiente
    grid.Rows = 1
    
    sDecimal = LeArquivoIni("WIN.INI", "intl", "sDecimal")
    If sDecimal = "." Then
        'A parte de pagamento n�o est� preparada para ponto decimal
        mensagem_erro 6, "Este m�dulo n�o pode ser usado com configura��o diferente da brasileira."
        End
    End If
    
    Call InicializaInterfaceLocal_Form
    
    p_cod_tecnico = Obtem_Codigo_Tecnico
    
    Me.Show
    DoEvents
    If Me.Width > Screen.Width Then
        Me.Width = Screen.Width
    End If
    
End Sub

Private Sub Form_Resize()
    If Me.WindowState <> vbMinimized Then
        If Me.Height < 6300 Then Me.Height = 6300
        If Me.Width < 15915 Then Me.Width = 15915
    
        fra_grid.Width = Me.Width - 435
        fra_grid.Height = Me.Height - 4845
        grid.Width = fra_grid.Width
        grid.Height = fra_grid.Height
        fra_comandos.Left = Me.Width - 2160
        fra_comandos.Top = Me.Height - 2420
        fra_observacao.Top = Me.Height - 2360
        fra_observacao.Width = Me.Width - 2325
        lbl_observacao.Width = fra_observacao.Width - 180
        
        If Me.Width > 17000 Then
            grid.ColWidth(4) = 2000 + Me.Width - 17100
        Else
            grid.ColWidth(4) = 2000
        End If
    End If
End Sub


Private Sub grid_Click()
    If grid.Row > 0 Then
        If grid.Col = 0 Then
            If grid.Text = cnt_bloqueado Then
                lbl_observacao.Text = grid.TextMatrix(grid.Row, 10)
            Else
                lbl_observacao.Text = ""
                If grid.Text = cnt_nao_selecionado Then
                    grid.Text = cnt_selecionado
                    l_selecionados = l_selecionados + 1
                Else
                    grid.Text = cnt_nao_selecionado
                    l_selecionados = l_selecionados - 1
                End If
                lblsel.Caption = "listados: " & Format(l_listados, "#,##0") & Space$(10) & "bloqueados: " & Format(l_bloqueados, "#,##0") & Space$(10) & "selecionados: " & Format(l_selecionados, "#,##0")

            End If
        ElseIf grid.TextMatrix(grid.Row, 0) = cnt_bloqueado Then
            lbl_observacao.Text = Replace(grid.TextMatrix(grid.Row, 13), "|", vbCrLf)
        Else
            lbl_observacao.Text = ""
        End If
    Else
        lbl_observacao.Text = ""
    End If
End Sub

Private Sub grid_LostFocus()
    lbl_observacao.Text = ""
End Sub

Private Sub grid_SelChange()
    If grid.Row > 0 Then
        If grid.TextMatrix(grid.Row, 0) = cnt_bloqueado Then
            lbl_observacao.Text = Replace(grid.TextMatrix(grid.Row, 10), "|", vbCrLf)
        Else
            lbl_observacao.Text = ""
        End If
    Else
        lbl_observacao.Text = ""
    End If
End Sub

Private Sub txt_max_valor_LostFocus()
    Call ajustavalor(txt_max_valor)
End Sub

Private Sub txt_min_valor_LostFocus()
    Call ajustavalor(txt_min_valor)
End Sub
Private Sub ajustavalor(ByRef obj As TextBox)
    
    Dim texto As String
    If IsNumeric(obj.Text) Then
        texto = Replace(Replace(obj.Text, ".", ""), ",", ".")
        texto = Format(Val(texto), "#,##0.00")
        obj.Text = texto
    Else
        obj.Text = ""
    End If
End Sub

