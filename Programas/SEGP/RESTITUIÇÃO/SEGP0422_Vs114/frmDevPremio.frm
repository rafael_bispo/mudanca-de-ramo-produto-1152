VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.2#0"; "MSCOMCTL.OCX"
Begin VB.Form frmDevPremio 
   ClientHeight    =   4350
   ClientLeft      =   3555
   ClientTop       =   2295
   ClientWidth     =   7035
   BeginProperty Font 
      Name            =   "Trebuchet MS"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   ScaleHeight     =   4350
   ScaleWidth      =   7035
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame Frame4 
      BeginProperty Font 
         Name            =   "Trebuchet MS"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   765
      Left            =   120
      TabIndex        =   14
      Top             =   3240
      Width           =   6870
      Begin VB.CommandButton cmdOk 
         Caption         =   "&Ok"
         BeginProperty Font 
            Name            =   "Microsoft Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   4140
         TabIndex        =   0
         Top             =   270
         Width           =   1215
      End
      Begin VB.CommandButton cmdCanc 
         Caption         =   "&Sair"
         BeginProperty Font 
            Name            =   "Microsoft Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   350
         Left            =   5490
         TabIndex        =   1
         Top             =   270
         Width           =   1215
      End
   End
   Begin VB.Frame Frame3 
      BeginProperty Font 
         Name            =   "Trebuchet MS"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3060
      Left            =   120
      TabIndex        =   7
      Top             =   120
      Width           =   6870
      Begin VB.Frame Frame1 
         Caption         =   " Processamento "
         BeginProperty Font 
            Name            =   "Microsoft Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1215
         Left            =   165
         TabIndex        =   11
         Top             =   240
         Width           =   6540
         Begin VB.TextBox TxtInicio 
            BeginProperty Font 
               Name            =   "Microsoft Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   4320
            Locked          =   -1  'True
            TabIndex        =   2
            Top             =   315
            Width           =   2070
         End
         Begin VB.TextBox TxtFim 
            BeginProperty Font 
               Name            =   "Microsoft Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   4300
            Locked          =   -1  'True
            TabIndex        =   3
            Top             =   720
            Width           =   2070
         End
         Begin VB.Label Label1 
            Caption         =   "In�cio : .............................................................................."
            BeginProperty Font 
               Name            =   "Microsoft Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Left            =   255
            TabIndex        =   13
            Top             =   405
            Width           =   4200
         End
         Begin VB.Label Label2 
            Caption         =   "Fim : .................................................................................."
            BeginProperty Font 
               Name            =   "Microsoft Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Left            =   255
            TabIndex        =   12
            Top             =   810
            Width           =   4200
         End
      End
      Begin VB.Frame Frame2 
         Caption         =   " Arquivo de Devolu��o de Premios "
         BeginProperty Font 
            Name            =   "Microsoft Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1400
         Left            =   165
         TabIndex        =   8
         Top             =   1560
         Width           =   6540
         Begin VB.TextBox TxtRegsGerados 
            BeginProperty Font 
               Name            =   "Microsoft Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   4300
            Locked          =   -1  'True
            TabIndex        =   5
            Top             =   810
            Width           =   2070
         End
         Begin VB.TextBox TxtArquivo 
            BeginProperty Font 
               Name            =   "Microsoft Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   4300
            Locked          =   -1  'True
            TabIndex        =   4
            Top             =   390
            Width           =   2070
         End
         Begin VB.Label Label4 
            AutoSize        =   -1  'True
            Caption         =   "Registros Gerados Processados : ....................................."
            BeginProperty Font 
               Name            =   "Microsoft Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Left            =   255
            TabIndex        =   10
            Top             =   900
            Width           =   4065
         End
         Begin VB.Label Label5 
            AutoSize        =   -1  'True
            Caption         =   "Nome do arquivo ................................................................."
            BeginProperty Font 
               Name            =   "Microsoft Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Left            =   255
            TabIndex        =   9
            Top             =   480
            Width           =   4095
         End
      End
   End
   Begin MSComctlLib.StatusBar StatusBar1 
      Align           =   2  'Align Bottom
      Height          =   315
      Left            =   0
      TabIndex        =   6
      Top             =   4035
      Width           =   7035
      _ExtentX        =   12409
      _ExtentY        =   556
      Style           =   1
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Microsoft Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmDevPremio"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private Const ENDOSSO_RESUMO = "999999999"
Public Endosso_Contestacao As Boolean

Dim rdocn1 As New rdoConnection
Dim rdocn2 As New rdoConnection
Dim rdocn3 As New rdoConnection
Dim rc1 As rdoResultset
Dim SQL As String
Dim SQL1 As String
Dim Rst As rdoResultset
Dim Proposta As String
Dim Produto As String
Dim CdProduto As String
Dim CdMdld As String
Dim CdItemMdld As String
Dim voucher As String
Dim CodProdutoBB As String
Dim Proposta_bb As String
Dim num_endosso_bb As String
Dim endosso_id As String
Dim restituicao_integral As String
Dim data_devolucao As String
Dim val_financeiro As String
Dim val_iof As String
Dim val_corretagem As String
Dim val_ir As String
Dim val_Estipulante As String
Dim val_ir_estipulante As String
Dim val_movimentacao As String
Dim custo_apolice As String
Dim val_desconto_comercial As String
Dim val_adic_fracionamento As String

Dim indicador_remuneracao As String
Dim codigo_remuneracao As String
Dim tipo_remuneracao As String
Dim pessoa_remuneracao As String
Dim val_remuneracao As String
Dim val_ir_remuneracao As String

Dim tot_val_movimentacao As Currency

Dim Arquivo As String
Dim Nome_Arquivo As String
Dim Path_Nome_Arquivo As String
Dim Versao_Arquivo As String
Dim Linha As String

Dim movimentacao_id As String
'Barney - 03/05/2004
Dim EndossoResumoID As String

Dim Conta_linha As Integer

Dim tam_reg As Integer

Dim NumRemessa As String

'F.BEZERRA - FLOWBR14413025 29/11/2012
Dim bytProcesso As Byte
Private Const bytProcesso_PreRemessa = 1
Private Const bytProcesso_Remessa = 2

'leonardo.santos - Demanda 19408029 - 10/10/2016
Private Type tpVoucher_Arquivo
    RamoId As Integer
    VoucherID As Long
    linhas As Long
    Usado As String
End Type

Function Obtem_Perc_IR_Estip(ByVal pProposta As Long, ByVal pEndosso As Long, ByVal pRamo As Integer, ByVal pDtInicioVig As Variant) As Double
    Dim rc As rdoResultset
    Dim rc2 As rdoResultset
    Dim SQL As String
    Dim perc_ir As Double
    Dim NoEstip As Long

    perc_ir = 0
    NoEstip = 0
    '
    SQL = "SELECT 'PF' tp_pessoa, count(*) tot_estip "
    SQL = SQL & " FROM pro_labore_endosso_fin_tb ef "
    SQL = SQL & " JOIN cliente_tb c "
    SQL = SQL & "   ON c.cliente_id = ef.cliente_id "
    SQL = SQL & " JOIN pessoa_fisica_tb pf "
    SQL = SQL & "   ON pf.pf_cliente_id = c.cliente_id "
    SQL = SQL & " WHERE ef.proposta_id = " & pProposta
    SQL = SQL & " AND   ef.endosso_id = " & pEndosso
    SQL = SQL & " UNION "
    SQL = SQL & "SELECT 'PJ' tp_pessoa, count(*) tot_estip "
    SQL = SQL & " FROM pro_labore_endosso_fin_tb ef "
    SQL = SQL & " JOIN cliente_tb c "
    SQL = SQL & "   ON c.cliente_id = ef.cliente_id "
    SQL = SQL & " JOIN pessoa_juridica_tb pj "
    SQL = SQL & "   ON pj.pj_cliente_id = c.cliente_id "
    SQL = SQL & " WHERE ef.proposta_id = " & pProposta
    SQL = SQL & " AND   ef.endosso_id = " & pEndosso
    '
    Set rc = rdocn2.OpenResultset(SQL)
    '
    If Not rc.EOF Then
        Do While Not rc.EOF
            '
            If rc!tp_pessoa = "PF" Then
                perc_ir = perc_ir + 0
                'NoEstip = rc!tot_estip
            Else
                '' S� calcula IR para PJ
                '' ==> S� acumula no. de estips. PJ para depois multiplicar o %
                NoEstip = NoEstip + 1
                '
                SQL = "SELECT   valor "
                SQL = SQL & "From val_item_financeiro_ramo_tb "
                SQL = SQL & "where   cod_item_financeiro = 'IR' "
                SQL = SQL & "and     ramo_id = " & pRamo
                SQL = SQL & "and     dt_inicio_vigencia <= '" & Format(pDtInicioVig, "yyyymmdd") & "' "
                SQL = SQL & " ORDER BY dt_inicio_vigencia DESC "
                '
                Set rc2 = rdocn3.OpenResultset(SQL)
                If Not rc2.EOF Then
                    perc_ir = perc_ir + Val(rc2!valor)
                    '
                    rc2.Close
                Else
                    perc_ir = perc_ir + 0
                End If
                '
                Set rc2 = Nothing
            End If
            '
            rc.MoveNext
        Loop
    Else
        perc_ir = 0
    End If
    'Se tiver mais de um corretor de PL, soma os percentuais de IR
    perc_ir = perc_ir * NoEstip
    '
    Obtem_Perc_IR_Estip = perc_ir

    Exit Function

Err_Obtem_IR:
    Call TrataErroGeral("Obtem_Perc_IR_Estip", Me.name)
    TerminaSEGBR

End Function



Private Function agrupar_voucher(ByRef rc As rdoResultset, ByVal limite_linhas As Integer, ByRef iRowCount() As Integer) As Collection
'10/10/2016 - leonardo.santos - Demanda 19408029 - Voucher de Devolu��o de Pr�mio.

    Dim i As Integer
    Dim x As Integer
    Dim arrVoucher() As tpVoucher_Arquivo
    Dim colGrupoVouchers As New Collection
    Dim num_vouchers As String
    Dim qtde_linhas As Long       'Integer   RMarins 04/12/2016
    Dim ramo_id As Integer                  'RMarins 09/12/2016

    On Error GoTo Erro

    'Preenche o array com os vouchers para posteriomente efetuar o agrupamento.
    rc.MoveFirst
    Do While Not rc.EOF
        ReDim Preserve arrVoucher(i)
        arrVoucher(i).RamoId = rc!ramo_id               'RMarins 08/12/2016
        arrVoucher(i).VoucherID = rc!voucher_id
        arrVoucher(i).linhas = rc!num_linhas
        arrVoucher(i).Usado = "N"
        i = (i + 1)
        rc.MoveNext
    Loop

    'qtde_linhas = 0
    Set colGrupoVouchers = New Collection

    'Agrupa os vouchers, limitando at� 32.000 linhas "por grupo" e tamb�m
    'Por Ramo_ID para atendimento da instancia 18346598 - incluida em 25/10/2016 por Edilson Silva
    For i = LBound(arrVoucher) To UBound(arrVoucher)
        If arrVoucher(i).Usado = "N" Then
            qtde_linhas = 0
            num_vouchers = ""
            ramo_id = arrVoucher(i).RamoId

            For x = i To UBound(arrVoucher)
                If arrVoucher(x).Usado = "N" And ramo_id = arrVoucher(x).RamoId Then
                    If (qtde_linhas + arrVoucher(x).linhas) <= limite_linhas Then
                        arrVoucher(x).Usado = "S"                                                     '> confirma o uso.
                        qtde_linhas = (qtde_linhas + arrVoucher(x).linhas)                            '> soma.
                        num_vouchers = (num_vouchers & "," & arrVoucher(x).VoucherID)                 '> concatena o acerto_id.
                    End If

                    If qtde_linhas = limite_linhas Then Exit For
                End If
            Next x

            If num_vouchers <> "" Then
                colGrupoVouchers.Add Mid$(num_vouchers, 2)                       '> Grupo de vouchers para gera��o do arquivo
                ReDim Preserve iRowCount(colGrupoVouchers.Count)
                iRowCount(colGrupoVouchers.Count) = qtde_linhas
            End If
        End If
    Next i

    'For i = 1 To colGrupoVouchers.Count    'Teste Remover
    '    Debug.Print colGrupoVouchers.Item(i)
    'Next i

    Set agrupar_voucher = colGrupoVouchers

    Exit Function
Erro:
    TrataErroGeral "agrupar_voucher", Me.name
End Function

Private Sub Form_Load()
    On Error GoTo Erro
    CTM = Verifica_Origem_ControlM(Command)

    'Confitec - Alexandre Debouch - EdsContestacao
    Endosso_Contestacao = Verifica_Endosso_Contestacao(Command)
    'Confitec - Alexandre Debouch - EdsContestacao

    glAmbiente_id = 3            'TESTE
    cUserName = "NOVA"   'TESTE

    Conexao
    Call Seguranca("SEGP0422", "Gera��o de Arquivo de Devolu��o de Premios")
    Conexao_Auxiliar

    Me.Caption = "SEG00422 - Gera��o do Arquivo de Devolu��o de Premios " & Ambiente
    StatusBar1.SimpleText = "Click em OK para iniciar"
    tam_reg = 500
    
    If Obtem_agenda_diaria_id(Command) > 0 Or CTM = True Then
        Me.Show
        Call cmdOK_Click
        TerminaSEGBR
    End If
    Exit Sub
Erro:
    Resume Next

End Sub
Private Sub cmdOK_Click()

    'InicializaParametrosExecucaoBatch (Me)

    MousePointer = vbHourglass

    TxtInicio.Text = Now
    TxtInicio.Refresh
    cmdOk.Enabled = False

    Arquivo = "SEG452"

    Call Processa(1)
    Call Processa(2)

    Call MensagemBatch("Fim do Processamento do SEGS452.", , , False)

    'Call goProducao.adicionalog(1, TxtArquivo)
    'Call goProducao.adicionalog(2, TxtRegsGerados)

    'grava log arquivo CTM
    If CTM = True Then
        Call GravaLogCTM("OK - ", TxtArquivo, "", "")
        Call GravaLogCTM("OK - ", TxtRegsGerados, "", "")
    End If


    '-------------------Arquivo BBC0452--------------------------

    frmDevPremio.Caption = "BBC00452 - Gera��o do Arquivo de Devolu��o de Premios " & Ambiente
    TxtInicio.Text = ""
    TxtFim.Text = ""
    TxtArquivo.Text = ""
    TxtRegsGerados.Text = ""

    StatusBar1.SimpleText = "Processando......"

    tam_reg = 500

    Arquivo = "BBC452"

    Call Processa_BBC452


    '--------------------------fim BBC0452--------------------------



    TxtFim = Now
    TxtFim.Refresh

    MousePointer = vbDefault

    Call MensagemBatch("Fim do Processamento do BBC452", , , False)

    'Call goProducao.adicionalog(1, TxtArquivo)
    'Call goProducao.adicionalog(2, TxtRegsGerados)

    'grava log arquivo CTM
    If CTM = True Then
        Call GravaLogCTM("OK - ", TxtArquivo, "", "")
        Call GravaLogCTM("OK - ", TxtRegsGerados, "", "")
    End If

    'Pula de for executado pelo Control-M
    If CTM = False Then
        'Call goProducao.finaliza
    End If

End Sub

Private Sub cmdCanc_Click()
    Unload Me
End Sub

Private Sub Processa(bytProcesso As Byte)

    Dim sSQL As String
    Dim rpath As String
    Dim perc_ir_estip As Double
    Dim iProdutoContaCartao As Integer
    Dim paridade As Currency
    Dim OrigemEndosso As String
    Dim lAdmClienteId As Long
    Dim sFlgIsencaoIR As String
    Dim lngNumeroPreRemessa As Long
    Dim i As Integer
    Dim iRowCount() As Integer
    Dim colGrupoVouchers As New Collection
    Dim rc1 As rdoResultset

    On Error GoTo Erro
    sSQL = ""
    If bytProcesso = 1 Then
        Me.Caption = App.EXEName & " - Gera��o do lote de Pr�-Remessa " & Ambiente
        Frame2.Caption = "Pr�-Remessa de Devolu��o de Pr�mios"
        TxtArquivo.Visible = False
        Label5.Visible = False
        StatusBar1.SimpleText = "Gerando a PreRemessa de Devolu��es de Premios... "
        StatusBar1.Refresh
        rdocn.BeginTrans

        sSQL = sSQL & "declare @nRetorno int " & vbNewLine
        sSQL = sSQL & "exec seguros_db.dbo.SEGS14351_SPI '" & cUserName & "', @nRetorno OUTPUT" & vbNewLine
        sSQL = sSQL & "select @nRetorno as QtdeRegistrosPreRemessa" & vbNewLine

        Set rc1 = rdocn.OpenResultset(sSQL)
        If Not rc1.EOF Then
            If rc1!QtdeRegistrosPreRemessa > 0 Then
                rdocn.CommitTrans
            Else
                rdocn.RollbackTrans
            End If
        End If
        rc1.Close
    End If

    If bytProcesso = 2 Then
        StatusBar1.SimpleText = "Gerando o Arquivo de Devolu��es de Premios... "
        StatusBar1.Refresh
        Me.Caption = App.EXEName & " - Gerando o Arquivo de Devolu��es de Premios" & Ambiente
        Frame2.Caption = "Arquivo de Devolu��o de Pr�mios"
        TxtArquivo.Visible = True
        Label5.Visible = True
        StatusBar1.SimpleText = "Gerando a PreRemessa de Devolu��es de Premios... "
        rpath = LerArquivoIni("ARQUIVOS DEVOLUCAO PREMIO", "SAIDA_Liberados")
        iProdutoContaCartao = BuscaParametro("Produto ContaCartao")

        sSQL = ""
        sSQL = sSQL & "SELECT DISTINCT ps_mov_cliente_tb.movimentacao_id" & vbNewLine
        sSQL = sSQL & " ,proposta_tb.produto_id" & vbNewLine
        sSQL = sSQL & " ,ps_acerto_pagamento_tb.voucher_id" & vbNewLine
        sSQL = sSQL & " ,item_produto_tb.cod_produto_bb" & vbNewLine
        sSQL = sSQL & " ,isnull(proposta_adesao_tb.proposta_bb, proposta_fechada_tb.proposta_bb) AS proposta_bb" & vbNewLine
        sSQL = sSQL & " ,endosso_tb.num_endosso_bb" & vbNewLine
        sSQL = sSQL & " ,endosso_tb.proposta_id" & vbNewLine
        sSQL = sSQL & " ,endosso_tb.endosso_id" & vbNewLine
        sSQL = sSQL & " ,ISNULL(avaliacao_restituicao_tb.restituicao_integral, 'n') AS restituicao_integral" & vbNewLine
        sSQL = sSQL & " ,ISNULL(endosso_financeiro_tb.val_financeiro, 0) val_financeiro" & vbNewLine
        sSQL = sSQL & " ,ISNULL(endosso_financeiro_tb.val_iof, 0) val_iof" & vbNewLine
        sSQL = sSQL & " ,ISNULL((endosso_financeiro_tb.val_comissao - ISNULL(endosso_financeiro_tb.val_ir, 0)), 0) val_corretagem" & vbNewLine
        sSQL = sSQL & " ,ISNULL(endosso_financeiro_tb.val_ir, 0) AS val_ir" & vbNewLine
        sSQL = sSQL & " ,ISNULL(endosso_financeiro_tb.val_ir_estipulante, 0) AS val_ir_estipulante" & vbNewLine
        sSQL = sSQL & " ,ISNULL(endosso_financeiro_tb.val_comissao_estipulante, 0) AS val_estipulante" & vbNewLine
        sSQL = sSQL & " ,ISNULL(ps_mov_cliente_tb.val_movimentacao, 0) AS val_movimentacao" & vbNewLine
        sSQL = sSQL & " ,ISNULL(endosso_financeiro_tb.custo_apolice, 0) AS custo_apolice" & vbNewLine
        sSQL = sSQL & " ,ISNULL(endosso_financeiro_tb.val_desconto_comercial, 0) AS val_desconto_comercial" & vbNewLine
        sSQL = sSQL & " ,ISNULL(endosso_financeiro_tb.val_adic_fracionamento, 0) AS val_adic_fracionamento" & vbNewLine
        sSQL = sSQL & " ,endosso_tb.dt_pedido_endosso AS dt_inicio_vigencia" & vbNewLine
        sSQL = sSQL & " ,ISNULL(apolice_tb.ramo_id, proposta_adesao_tb.ramo_id) AS ramo_id" & vbNewLine
        sSQL = sSQL & " ,0 endosso_resumo_id" & vbNewLine
        sSQL = sSQL & " ,ISNULL(proposta_processo_susep_tb.cod_produto, 0) AS cd_prd" & vbNewLine
        sSQL = sSQL & " ,ISNULL(proposta_processo_susep_tb.cod_modalidade, 0) AS cd_mdld" & vbNewLine
        sSQL = sSQL & " ,ISNULL(proposta_processo_susep_tb.cod_item_modalidade, 0) AS cd_item_mdld" & vbNewLine
        sSQL = sSQL & " ,indicador_remuneracao = CASE " & vbNewLine
        sSQL = sSQL & "     WHEN proposta_tb.produto_id IN (" & vbNewLine
        sSQL = sSQL & "             1201" & vbNewLine
        sSQL = sSQL & "             ,1226" & vbNewLine
        sSQL = sSQL & "             ,1227" & vbNewLine
        sSQL = sSQL & "             )" & vbNewLine
        sSQL = sSQL & "         THEN item_mdld_prd_hist_tb.in_rmnc_sgro" & vbNewLine
        sSQL = sSQL & "     ELSE CASE " & vbNewLine
        sSQL = sSQL & "             WHEN ISNULL(RMNC_ITEM_MDLD_HIST_TB.CD_RMNC_SGRO, 0) > 0" & vbNewLine
        sSQL = sSQL & "                 THEN 'S'" & vbNewLine
        sSQL = sSQL & "             ELSE 'N'" & vbNewLine
        sSQL = sSQL & "             END" & vbNewLine
        sSQL = sSQL & "     END" & vbNewLine
        sSQL = sSQL & " ,codigo_remuneracao = ISNULL(RMNC_ITEM_MDLD_HIST_TB.CD_RMNC_SGRO, 0)" & vbNewLine
        sSQL = sSQL & " ,tipo_remuneracao = ISNULL(RMNC_ITEM_MDLD_HIST_TB.CD_TIP_RMNC_SGRO, 0)" & vbNewLine
        sSQL = sSQL & " ,pessoa_remuneracao = 0" & vbNewLine
        sSQL = sSQL & " ,val_remuneracao = ISNULL(endosso_financeiro_tb.val_remuneracao, 0)" & vbNewLine
        sSQL = sSQL & " ,val_ir_remuneracao = ISNULL(endosso_financeiro_tb.val_ir_remuneracao, 0)" & vbNewLine
        sSQL = sSQL & "INTO #DadosArquivo" & vbNewLine
        sSQL = sSQL & "FROM ps_mov_cliente_tb ps_mov_cliente_tb WITH (NOLOCK)" & vbNewLine
        sSQL = sSQL & "JOIN ps_acerto_pagamento_tb ps_acerto_pagamento_tb WITH (NOLOCK) ON ps_mov_cliente_tb.acerto_id = ps_acerto_pagamento_tb.acerto_id" & vbNewLine
        sSQL = sSQL & " AND ps_acerto_pagamento_tb.situacao = 'e'" & vbNewLine
        sSQL = sSQL & "JOIN ps_mov_endosso_financeiro_tb ps_mov_endosso_financeiro_tb WITH (NOLOCK) ON ps_mov_cliente_tb.movimentacao_id = ps_mov_endosso_financeiro_tb.movimentacao_id" & vbNewLine
        sSQL = sSQL & "JOIN endosso_financeiro_tb endosso_financeiro_tb WITH (NOLOCK) ON ps_mov_endosso_financeiro_tb.proposta_id = endosso_financeiro_tb.proposta_id" & vbNewLine
        sSQL = sSQL & " AND ps_mov_endosso_financeiro_tb.endosso_id = endosso_financeiro_tb.endosso_id" & vbNewLine
        If Endosso_Contestacao Then
       'Endosso de contesta��o - garantir que o evento foi aceito para n�o gerar queda no 987 por endosso n�o localizado
        sSQL = sSQL & "JOIN eds_contestacao_log_tb eds_contestacao_log_tb WITH (NOLOCK)" & vbNewLine
        sSQL = sSQL & " ON eds_contestacao_log_tb.proposta_id = endosso_financeiro_tb.proposta_id" & vbNewLine
        sSQL = sSQL & " AND eds_contestacao_log_tb.endosso_id = endosso_financeiro_tb.endosso_id" & vbNewLine
        sSQL = sSQL & " JOIN seguros_db.dbo.ps_movimentacao_tb  ps_movimentacao_tb WITH (NOLOCK) " & vbNewLine
        sSQL = sSQL & " ON ps_movimentacao_tb.movimentacao_id = ps_mov_cliente_tb.movimentacao_id " & vbNewLine
        sSQL = sSQL & " AND ps_movimentacao_tb.flag_aprovacao='S' " & vbNewLine
        'Endosso de contesta��o - garantir que o evento foi aceito para n�o gerar queda no 987 por endosso n�o localizado
        End If
        sSQL = sSQL & "JOIN endosso_tb endosso_tb WITH (NOLOCK) ON endosso_financeiro_tb.proposta_id = endosso_tb.proposta_id" & vbNewLine
        sSQL = sSQL & " AND endosso_financeiro_tb.endosso_id = endosso_tb.endosso_id" & vbNewLine
        sSQL = sSQL & "JOIN proposta_tb proposta_tb WITH (NOLOCK) ON endosso_tb.proposta_id = proposta_tb.proposta_id" & vbNewLine
        sSQL = sSQL & "LEFT JOIN proposta_adesao_tb proposta_adesao_tb WITH (NOLOCK) ON proposta_tb.proposta_id = proposta_adesao_tb.proposta_id" & vbNewLine
        sSQL = sSQL & "LEFT JOIN proposta_fechada_tb proposta_fechada_tb WITH (NOLOCK) ON proposta_tb.proposta_id = proposta_fechada_tb.proposta_id" & vbNewLine
        sSQL = sSQL & "LEFT JOIN apolice_tb apolice_tb WITH (NOLOCK) ON proposta_tb.proposta_id = apolice_tb.proposta_id" & vbNewLine
        sSQL = sSQL & "JOIN item_produto_tb item_produto_tb WITH (NOLOCK) ON proposta_tb.produto_id = item_produto_tb.produto_id" & vbNewLine
        sSQL = sSQL & " AND ISNULL(proposta_adesao_tb.ramo_id, apolice_tb.ramo_id) = item_produto_tb.ramo_id" & vbNewLine
        sSQL = sSQL & "LEFT JOIN avaliacao_restituicao_tb avaliacao_restituicao_tb WITH (NOLOCK) ON endosso_tb.proposta_id = avaliacao_restituicao_tb.proposta_id" & vbNewLine
        sSQL = sSQL & " AND endosso_tb.endosso_id = avaliacao_restituicao_tb.endosso_id" & vbNewLine
        sSQL = sSQL & " AND avaliacao_restituicao_tb.situacao = 'a'" & vbNewLine
        sSQL = sSQL & "JOIN produto_tb produto_tb WITH (NOLOCK) ON proposta_tb.produto_id = produto_tb.produto_id" & vbNewLine
        sSQL = sSQL & "LEFT JOIN proposta_processo_susep_tb WITH (NOLOCK) ON proposta_processo_susep_tb.proposta_id = proposta_tb.proposta_id" & vbNewLine
        sSQL = sSQL & "LEFT JOIN proposta_subvencao_tb WITH (NOLOCK) ON proposta_subvencao_tb.proposta_id = proposta_tb.proposta_id" & vbNewLine
        sSQL = sSQL & "LEFT JOIN als_produto_db.dbo.RMNC_ITEM_MDLD_HIST_TB RMNC_ITEM_MDLD_HIST_TB WITH (NOLOCK) ON RMNC_ITEM_MDLD_HIST_TB.CD_PRD_BB = proposta_processo_susep_tb.cod_produto" & vbNewLine
        sSQL = sSQL & " AND RMNC_ITEM_MDLD_HIST_TB.CD_MDLD_BB = proposta_processo_susep_tb.cod_modalidade" & vbNewLine
        sSQL = sSQL & " AND RMNC_ITEM_MDLD_HIST_TB.CD_ITEM_MDLD = proposta_processo_susep_tb.cod_item_modalidade" & vbNewLine
        sSQL = sSQL & " AND RMNC_ITEM_MDLD_HIST_TB.DT_INC_VGC_RMNC <= proposta_tb.dt_contratacao" & vbNewLine
        sSQL = sSQL & " AND (" & vbNewLine
        sSQL = sSQL & "     RMNC_ITEM_MDLD_HIST_TB.DT_FIM_VGC_RMNC >= proposta_tb.dt_contratacao" & vbNewLine
        sSQL = sSQL & "     OR RMNC_ITEM_MDLD_HIST_TB.DT_FIM_VGC_RMNC IS NULL" & vbNewLine
        sSQL = sSQL & "     )" & vbNewLine
        sSQL = sSQL & "LEFT JOIN als_produto_db.dbo.item_mdld_prd_hist_tb item_mdld_prd_hist_tb WITH (NOLOCK) ON item_mdld_prd_hist_tb.CD_PRD_BB = proposta_processo_susep_tb.cod_produto" & vbNewLine
        sSQL = sSQL & " AND item_mdld_prd_hist_tb.CD_MDLD_BB = proposta_processo_susep_tb.cod_modalidade" & vbNewLine
        sSQL = sSQL & " AND item_mdld_prd_hist_tb.CD_ITEM_MDLD = proposta_processo_susep_tb.cod_item_modalidade" & vbNewLine
        sSQL = sSQL & " AND item_mdld_prd_hist_tb.DT_INICIO_VIGENCIA <= proposta_tb.dt_contratacao" & vbNewLine
        sSQL = sSQL & " AND (" & vbNewLine
        sSQL = sSQL & "     item_mdld_prd_hist_tb.DT_FIM_VIGENCIA >= proposta_tb.dt_contratacao" & vbNewLine
        sSQL = sSQL & "     OR item_mdld_prd_hist_tb.DT_FIM_VIGENCIA IS NULL" & vbNewLine
        sSQL = sSQL & "     )" & vbNewLine
        sSQL = sSQL & "JOIN ps_Mov_Cliente_PreRemessa_Tb ps_Mov_Cliente_PreRemessa_Tb WITH (NOLOCK) ON ps_Mov_Cliente_PreRemessa_Tb.movimentacao_id = ps_mov_cliente_tb.movimentacao_id" & vbNewLine
        sSQL = sSQL & "WHERE ps_mov_cliente_tb.arquivo_retorno_bb IS NULL" & vbNewLine
        sSQL = sSQL & " AND upper(proposta_tb.situacao) <> 'T'" & vbNewLine
        sSQL = sSQL & " AND ps_acerto_pagamento_tb.voucher_id IS NOT NULL" & vbNewLine
        sSQL = sSQL & " AND ps_mov_cliente_tb.data_retorno_bb IS NULL" & vbNewLine
        sSQL = sSQL & " AND produto_tb.processa_restituicao_automatica = 's'" & vbNewLine
        sSQL = sSQL & " AND (" & vbNewLine
        sSQL = sSQL & "     proposta_tb.produto_id NOT IN (" & vbNewLine
        sSQL = sSQL & "         12" & vbNewLine
        sSQL = sSQL & "         ,121" & vbNewLine
        sSQL = sSQL & "         ,135" & vbNewLine
        sSQL = sSQL & "         ,136" & vbNewLine
        sSQL = sSQL & "         ,716" & vbNewLine
        sSQL = sSQL & "         )" & vbNewLine
        sSQL = sSQL & "     )" & vbNewLine
        sSQL = sSQL & " AND ps_mov_cliente_tb.canc_endosso_id IS NULL" & vbNewLine
        sSQL = sSQL & " AND (" & vbNewLine
        sSQL = sSQL & "     (" & vbNewLine
        sSQL = sSQL & "         endosso_tb.tp_endosso_id = 63" & vbNewLine
        sSQL = sSQL & "         AND avaliacao_restituicao_tb.proposta_id IS NOT NULL" & vbNewLine
        sSQL = sSQL & "         )" & vbNewLine
        sSQL = sSQL & "     OR endosso_tb.tp_endosso_id = 70" & vbNewLine
        sSQL = sSQL & "     OR endosso_tb.tp_endosso_id = 80" & vbNewLine
        sSQL = sSQL & "     OR endosso_tb.tp_endosso_id = 81" & vbNewLine
        sSQL = sSQL & "     OR endosso_tb.tp_endosso_id = 101" & vbNewLine
        sSQL = sSQL & "     OR endosso_tb.tp_endosso_id = 104" & vbNewLine
        sSQL = sSQL & "     OR (" & vbNewLine
        sSQL = sSQL & "         endosso_tb.tp_endosso_id = 64" & vbNewLine
        sSQL = sSQL & "         AND (" & vbNewLine
        sSQL = sSQL & "             proposta_subvencao_tb.proposta_id IS NOT NULL" & vbNewLine
        sSQL = sSQL & "             OR proposta_tb.produto_id IN (" & vbNewLine
        sSQL = sSQL & "                 1235" & vbNewLine
        sSQL = sSQL & "                 ,1236" & vbNewLine
        sSQL = sSQL & "                 ,1237" & vbNewLine
        sSQL = sSQL & "                 )" & vbNewLine
        sSQL = sSQL & "             )" & vbNewLine
        sSQL = sSQL & "         )" & vbNewLine
        sSQL = sSQL & "     OR endosso_tb.tp_endosso_id = 269" & vbNewLine
        sSQL = sSQL & "     OR endosso_tb.tp_endosso_id = 270" & vbNewLine
        sSQL = sSQL & "     OR endosso_tb.tp_endosso_id = 293" & vbNewLine
        sSQL = sSQL & "     OR endosso_tb.tp_endosso_id = 297" & vbNewLine
        sSQL = sSQL & "     OR endosso_tb.tp_endosso_id = 253" & vbNewLine
        sSQL = sSQL & "     OR endosso_tb.tp_endosso_id = 336" & vbNewLine
        sSQL = sSQL & "     OR endosso_tb.tp_endosso_id = 10" & vbNewLine
        sSQL = sSQL & "     OR endosso_tb.tp_endosso_id IN (" & vbNewLine
        sSQL = sSQL & "         359" & vbNewLine
        sSQL = sSQL & "         ,360" & vbNewLine
        sSQL = sSQL & "         )" & vbNewLine
        sSQL = sSQL & "     OR endosso_tb.tp_endosso_id IN (" & vbNewLine
        sSQL = sSQL & "         361" & vbNewLine
        sSQL = sSQL & "         ,362" & vbNewLine
        sSQL = sSQL & "         )" & vbNewLine
        sSQL = sSQL & "     OR endosso_tb.tp_endosso_id = 338" & vbNewLine
        sSQL = sSQL & "     OR endosso_tb.tp_endosso_id = 340" & vbNewLine
        sSQL = sSQL & "     OR endosso_tb.tp_endosso_id IN (" & vbNewLine
        sSQL = sSQL & "         37" & vbNewLine
        sSQL = sSQL & "         ,350" & vbNewLine
        sSQL = sSQL & "         ,357" & vbNewLine
        sSQL = sSQL & "         ,28" & vbNewLine
        sSQL = sSQL & "         ,30" & vbNewLine
        sSQL = sSQL & "         )" & vbNewLine
        sSQL = sSQL & "     )" & vbNewLine
        sSQL = sSQL & " AND isnull(proposta_adesao_tb.proposta_bb, proposta_fechada_tb.proposta_bb) IS NOT NULL" & vbNewLine
        sSQL = sSQL & " AND ps_mov_cliente_tb.Remessa_Liberada = 'S'" & vbNewLine
        sSQL = sSQL & " AND ps_Mov_Cliente_PreRemessa_Tb.lote_liberado = 'S'" & vbNewLine
        sSQL = sSQL & " AND (" & vbNewLine
        sSQL = sSQL & "     SELECT count(1)" & vbNewLine
        sSQL = sSQL & "     FROM ps_mov_cliente_tb AX WITH (NOLOCK)" & vbNewLine
        sSQL = sSQL & "     JOIN seguros_db.dbo.ps_Mov_Cliente_PreRemessa_Tb ps_Mov_Cliente_PreRemessa_Tb_1 WITH (NOLOCK) ON ps_Mov_Cliente_PreRemessa_Tb_1.movimentacao_id = AX.movimentacao_id" & vbNewLine
        sSQL = sSQL & "         AND AX.acerto_id = ps_mov_cliente_tb.acerto_id" & vbNewLine
        sSQL = sSQL & "         AND AX.remessa_liberada NOT IN ('N')" & vbNewLine
        sSQL = sSQL & "     ) = (" & vbNewLine
        sSQL = sSQL & "     SELECT count(1)" & vbNewLine
        sSQL = sSQL & "     FROM ps_mov_cliente_tb AX WITH (NOLOCK)" & vbNewLine
        sSQL = sSQL & "     JOIN seguros_db.dbo.ps_Mov_Cliente_PreRemessa_Tb ps_Mov_Cliente_PreRemessa_Tb_2 WITH (NOLOCK) ON ps_Mov_Cliente_PreRemessa_Tb_2.movimentacao_id = AX.movimentacao_id" & vbNewLine
        sSQL = sSQL & "         AND ps_Mov_Cliente_PreRemessa_Tb_2.lote_liberado = 'S'" & vbNewLine
        sSQL = sSQL & "         AND AX.acerto_id = ps_mov_cliente_tb.acerto_id" & vbNewLine
        sSQL = sSQL & "     )" & vbNewLine
        If Not Endosso_Contestacao Then
            sSQL = sSQL & " AND NOT EXISTS (" & vbNewLine
            sSQL = sSQL & "     SELECT 1" & vbNewLine
            sSQL = sSQL & "     FROM eds_contestacao_log_tb eds_contestacao_log_tb WITH (NOLOCK)" & vbNewLine
            sSQL = sSQL & "     WHERE eds_contestacao_log_tb.proposta_id = ps_mov_endosso_financeiro_tb.proposta_id" & vbNewLine
            sSQL = sSQL & "         AND eds_contestacao_log_tb.endosso_id = ps_mov_endosso_financeiro_tb.endosso_id" & vbNewLine
            sSQL = sSQL & "     )" & vbNewLine
        End If
        sSQL = sSQL & "UNION" & vbNewLine
        sSQL = sSQL & "SELECT DISTINCT max(ps_mov_cliente_tb.movimentacao_id) movimentacao_id" & vbNewLine
        sSQL = sSQL & " ,proposta_tb.produto_id" & vbNewLine
        sSQL = sSQL & " ,ps_acerto_pagamento_tb.voucher_id" & vbNewLine
        sSQL = sSQL & " ,item_produto_tb.cod_produto_bb" & vbNewLine
        sSQL = sSQL & " ,isnull(proposta_adesao_tb.proposta_bb, proposta_fechada_tb.proposta_bb) AS proposta_bb" & vbNewLine
        sSQL = sSQL & " ,endosso_tb.num_endosso_bb" & vbNewLine
        sSQL = sSQL & " ,endosso_tb.proposta_id" & vbNewLine
        sSQL = sSQL & " ,endosso_resumo_tb.endosso_id" & vbNewLine
        sSQL = sSQL & " ,restituicao_integral = CASE " & vbNewLine
        sSQL = sSQL & "     WHEN ISNULL(produto_tb.prazo_restituicao_integral, 0) = 0" & vbNewLine
        sSQL = sSQL & "         AND ISNULL(prazo_restituicao_integral_renov, 0) = 0" & vbNewLine
        sSQL = sSQL & "         THEN 's'" & vbNewLine
        sSQL = sSQL & "     ELSE 'n'" & vbNewLine
        sSQL = sSQL & "     END" & vbNewLine
        sSQL = sSQL & " ,endosso_resumo_tb.val_financeiro" & vbNewLine
        sSQL = sSQL & " ,endosso_resumo_tb.val_iof" & vbNewLine
        sSQL = sSQL & " ,(endosso_resumo_tb.val_comissao - endosso_resumo_tb.val_ir) val_corretagem" & vbNewLine
        sSQL = sSQL & " ,endosso_resumo_tb.val_ir" & vbNewLine
        sSQL = sSQL & " ,ISNULL(endosso_resumo_tb.val_ir_estipulante, 0) AS val_ir_estipulante" & vbNewLine
        sSQL = sSQL & " ,(endosso_resumo_tb.val_comissao_estipulante - ISNULL(endosso_resumo_tb.val_ir_estipulante, 0)) val_estipulante" & vbNewLine
        sSQL = sSQL & " ,cast((" & vbNewLine
        sSQL = sSQL & "         (" & vbNewLine
        sSQL = sSQL & "             endosso_resumo_tb.val_financeiro - --valor Financeiro" & vbNewLine
        sSQL = sSQL & "             endosso_resumo_tb.val_iof - --Valor IOF" & vbNewLine
        sSQL = sSQL & "             (endosso_resumo_tb.val_comissao - endosso_resumo_tb.val_ir) - --Corretagem Liquida" & vbNewLine
        sSQL = sSQL & "             (endosso_resumo_tb.val_comissao_estipulante - ISNULL(endosso_resumo_tb.val_ir_estipulante, 0))" & vbNewLine
        sSQL = sSQL & "             ) * - 1" & vbNewLine
        sSQL = sSQL & "         ) AS NUMERIC(15, 2)) val_movimentacao" & vbNewLine
        sSQL = sSQL & " ,0 custo_apolice" & vbNewLine
        sSQL = sSQL & " ,endosso_resumo_tb.val_desconto_comercial" & vbNewLine
        sSQL = sSQL & " ,endosso_resumo_tb.val_adic_fracionamento" & vbNewLine
        sSQL = sSQL & " ,endosso_tb.dt_pedido_endosso AS dt_inicio_vigencia" & vbNewLine
        sSQL = sSQL & " ,ISNULL(apolice_tb.ramo_id, proposta_adesao_tb.ramo_id) AS ramo_id" & vbNewLine
        sSQL = sSQL & " ,ISNULL(ISNULL(eds_ctr_sgro.NR_SLCT_EDS_SGRA, endosso_resumo_tb.endosso_resumo_id), 0) AS endosso_resumo_id" & vbNewLine
        sSQL = sSQL & " ,ISNULL(proposta_processo_susep_tb.cod_produto, 0) AS cd_prd" & vbNewLine
        sSQL = sSQL & " ,ISNULL(proposta_processo_susep_tb.cod_modalidade, 0) AS cd_mdld" & vbNewLine
        sSQL = sSQL & " ,ISNULL(proposta_processo_susep_tb.cod_item_modalidade, 0) AS cd_item_mdld" & vbNewLine
        sSQL = sSQL & " ,indicador_remuneracao = CASE " & vbNewLine
        sSQL = sSQL & "     WHEN proposta_tb.produto_id IN (" & vbNewLine
        sSQL = sSQL & "             1201" & vbNewLine
        sSQL = sSQL & "             ,1226" & vbNewLine
        sSQL = sSQL & "             ,1227" & vbNewLine
        sSQL = sSQL & "             )" & vbNewLine
        sSQL = sSQL & "         THEN item_mdld_prd_hist_tb.in_rmnc_sgro" & vbNewLine
        sSQL = sSQL & "     ELSE CASE " & vbNewLine
        sSQL = sSQL & "             WHEN ISNULL(RMNC_ITEM_MDLD_HIST_TB.CD_RMNC_SGRO, 0) > 0" & vbNewLine
        sSQL = sSQL & "                 THEN 'S'" & vbNewLine
        sSQL = sSQL & "             ELSE 'N'" & vbNewLine
        sSQL = sSQL & "             END" & vbNewLine
        sSQL = sSQL & "     END" & vbNewLine
        sSQL = sSQL & " ,codigo_remuneracao = ISNULL(RMNC_ITEM_MDLD_HIST_TB.CD_RMNC_SGRO, 0)" & vbNewLine
        sSQL = sSQL & " ,tipo_remuneracao = ISNULL(RMNC_ITEM_MDLD_HIST_TB.CD_TIP_RMNC_SGRO, 0)" & vbNewLine
        sSQL = sSQL & " ,pessoa_remuneracao = 0" & vbNewLine
        sSQL = sSQL & " ,val_remuneracao = ISNULL(endosso_financeiro_tb.val_remuneracao, 0)" & vbNewLine
        sSQL = sSQL & " ,val_ir_remuneracao = ISNULL(endosso_financeiro_tb.val_ir_remuneracao, 0)" & vbNewLine
        sSQL = sSQL & "FROM ps_mov_cliente_tb ps_mov_cliente_tb WITH (NOLOCK)" & vbNewLine
        sSQL = sSQL & "JOIN ps_acerto_pagamento_tb ps_acerto_pagamento_tb WITH (NOLOCK) ON ps_mov_cliente_tb.acerto_id = ps_acerto_pagamento_tb.acerto_id" & vbNewLine
        sSQL = sSQL & " AND ps_acerto_pagamento_tb.situacao = 'e'" & vbNewLine
        sSQL = sSQL & "JOIN ps_mov_endosso_financeiro_tb ps_mov_endosso_financeiro_tb WITH (NOLOCK) ON ps_mov_cliente_tb.movimentacao_id = ps_mov_endosso_financeiro_tb.movimentacao_id" & vbNewLine
        sSQL = sSQL & "JOIN endosso_financeiro_tb endosso_financeiro_tb WITH (NOLOCK) ON ps_mov_endosso_financeiro_tb.proposta_id = endosso_financeiro_tb.proposta_id" & vbNewLine
        sSQL = sSQL & " AND ps_mov_endosso_financeiro_tb.endosso_id = endosso_financeiro_tb.endosso_id" & vbNewLine
        If Endosso_Contestacao Then
       'Endosso de contesta��o - garantir que o evento foi aceito para n�o gerar queda no 987 por endosso n�o localizado
        sSQL = sSQL & "JOIN eds_contestacao_log_tb eds_contestacao_log_tb WITH (NOLOCK)" & vbNewLine
        sSQL = sSQL & " ON eds_contestacao_log_tb.proposta_id = endosso_financeiro_tb.proposta_id" & vbNewLine
        sSQL = sSQL & " AND eds_contestacao_log_tb.endosso_id = endosso_financeiro_tb.endosso_id" & vbNewLine
        sSQL = sSQL & " JOIN seguros_db.dbo.ps_movimentacao_tb  ps_movimentacao_tb WITH (NOLOCK) " & vbNewLine
        sSQL = sSQL & " ON ps_movimentacao_tb.movimentacao_id = ps_mov_cliente_tb.movimentacao_id " & vbNewLine
        sSQL = sSQL & " AND ps_movimentacao_tb.flag_aprovacao='S' " & vbNewLine
        'Endosso de contesta��o - garantir que o evento foi aceito para n�o gerar queda no 987 por endosso n�o localizado
        End If
        sSQL = sSQL & "JOIN endosso_resumo_tb endosso_resumo_tb WITH (NOLOCK) ON endosso_resumo_tb.endosso_resumo_id = endosso_financeiro_tb.endosso_resumo_id" & vbNewLine
        sSQL = sSQL & "JOIN endosso_tb endosso_tb WITH (NOLOCK) ON endosso_resumo_tb.proposta_id = endosso_tb.proposta_id" & vbNewLine
        sSQL = sSQL & " AND endosso_resumo_tb.endosso_id = endosso_tb.endosso_id" & vbNewLine
        sSQL = sSQL & "JOIN proposta_tb proposta_tb WITH (NOLOCK) ON endosso_tb.proposta_id = proposta_tb.proposta_id" & vbNewLine
        sSQL = sSQL & "LEFT JOIN proposta_adesao_tb proposta_adesao_tb WITH (NOLOCK) ON proposta_tb.proposta_id = proposta_adesao_tb.proposta_id" & vbNewLine
        sSQL = sSQL & "LEFT JOIN proposta_fechada_tb proposta_fechada_tb WITH (NOLOCK) ON proposta_tb.proposta_id = proposta_fechada_tb.proposta_id" & vbNewLine
        sSQL = sSQL & "LEFT JOIN apolice_tb apolice_tb WITH (NOLOCK) ON proposta_tb.proposta_id = apolice_tb.proposta_id" & vbNewLine
        sSQL = sSQL & "JOIN item_produto_tb item_produto_tb WITH (NOLOCK) ON proposta_tb.produto_id = item_produto_tb.produto_id" & vbNewLine
        sSQL = sSQL & " AND ISNULL(proposta_adesao_tb.ramo_id, apolice_tb.ramo_id) = item_produto_tb.ramo_id" & vbNewLine
        sSQL = sSQL & "JOIN produto_tb produto_tb WITH (NOLOCK) ON proposta_tb.produto_id = produto_tb.produto_id" & vbNewLine
        sSQL = sSQL & "LEFT JOIN proposta_processo_susep_tb WITH (NOLOCK) ON proposta_processo_susep_tb.proposta_id = proposta_tb.proposta_id" & vbNewLine
        sSQL = sSQL & "LEFT JOIN als_operacao_db.dbo.eds_ctr_sgro eds_ctr_sgro WITH (NOLOCK) ON eds_ctr_sgro.cd_prd = proposta_processo_susep_tb.cod_produto" & vbNewLine
        sSQL = sSQL & " AND eds_ctr_sgro.cd_mdld = proposta_processo_susep_tb.cod_modalidade" & vbNewLine
        sSQL = sSQL & " AND eds_ctr_sgro.cd_item_mdld = proposta_processo_susep_tb.cod_item_modalidade" & vbNewLine
        sSQL = sSQL & " AND eds_ctr_sgro.nr_ctr_sgro = proposta_processo_susep_tb.num_contrato_seguro" & vbNewLine
        sSQL = sSQL & " AND CONVERT(VARCHAR(6), eds_ctr_sgro.DT_CPTC_EDS, 112) = endosso_resumo_tb.ano_mes_devolucao" & vbNewLine
        sSQL = sSQL & "LEFT JOIN als_produto_db.dbo.RMNC_ITEM_MDLD_HIST_TB RMNC_ITEM_MDLD_HIST_TB WITH (NOLOCK) ON RMNC_ITEM_MDLD_HIST_TB.CD_PRD_BB = proposta_processo_susep_tb.cod_produto" & vbNewLine
        sSQL = sSQL & " AND RMNC_ITEM_MDLD_HIST_TB.CD_MDLD_BB = proposta_processo_susep_tb.cod_modalidade" & vbNewLine
        sSQL = sSQL & " AND RMNC_ITEM_MDLD_HIST_TB.CD_ITEM_MDLD = proposta_processo_susep_tb.cod_item_modalidade" & vbNewLine
        sSQL = sSQL & " AND RMNC_ITEM_MDLD_HIST_TB.DT_INC_VGC_RMNC <= proposta_tb.dt_contratacao" & vbNewLine
        sSQL = sSQL & " AND (" & vbNewLine
        sSQL = sSQL & "     RMNC_ITEM_MDLD_HIST_TB.DT_FIM_VGC_RMNC >= proposta_tb.dt_contratacao" & vbNewLine
        sSQL = sSQL & "     OR RMNC_ITEM_MDLD_HIST_TB.DT_FIM_VGC_RMNC IS NULL" & vbNewLine
        sSQL = sSQL & "     )" & vbNewLine
        sSQL = sSQL & "LEFT JOIN als_produto_db.dbo.item_mdld_prd_hist_tb item_mdld_prd_hist_tb WITH (NOLOCK) ON item_mdld_prd_hist_tb.CD_PRD_BB = proposta_processo_susep_tb.cod_produto" & vbNewLine
        sSQL = sSQL & " AND item_mdld_prd_hist_tb.CD_MDLD_BB = proposta_processo_susep_tb.cod_modalidade" & vbNewLine
        sSQL = sSQL & " AND item_mdld_prd_hist_tb.CD_ITEM_MDLD = proposta_processo_susep_tb.cod_item_modalidade" & vbNewLine
        sSQL = sSQL & " AND item_mdld_prd_hist_tb.DT_INICIO_VIGENCIA <= proposta_tb.dt_contratacao" & vbNewLine
        sSQL = sSQL & " AND (" & vbNewLine
        sSQL = sSQL & "     item_mdld_prd_hist_tb.DT_FIM_VIGENCIA >= proposta_tb.dt_contratacao" & vbNewLine
        sSQL = sSQL & "     OR item_mdld_prd_hist_tb.DT_FIM_VIGENCIA IS NULL" & vbNewLine
        sSQL = sSQL & "     )" & vbNewLine
        sSQL = sSQL & "WHERE ps_mov_cliente_tb.arquivo_retorno_bb IS NULL" & vbNewLine
        sSQL = sSQL & " AND upper(proposta_tb.situacao) <> 'T'" & vbNewLine
        sSQL = sSQL & " AND ps_acerto_pagamento_tb.voucher_id IS NOT NULL" & vbNewLine
        sSQL = sSQL & " AND ps_mov_cliente_tb.data_retorno_bb IS NULL" & vbNewLine
        sSQL = sSQL & " AND produto_tb.processa_restituicao_automatica = 's'" & vbNewLine
        sSQL = sSQL & " AND (" & vbNewLine
        sSQL = sSQL & "     proposta_tb.produto_id NOT IN (" & vbNewLine
        sSQL = sSQL & "         12" & vbNewLine
        sSQL = sSQL & "         ,121" & vbNewLine
        sSQL = sSQL & "         ,135" & vbNewLine
        sSQL = sSQL & "         ,136" & vbNewLine
        sSQL = sSQL & "         ,716" & vbNewLine
        sSQL = sSQL & "         )" & vbNewLine
        sSQL = sSQL & "     )" & vbNewLine
        sSQL = sSQL & " AND ps_mov_cliente_tb.canc_endosso_id IS NULL" & vbNewLine
        sSQL = sSQL & " AND endosso_tb.tp_endosso_id = 200" & vbNewLine
        If Not Endosso_Contestacao Then
            sSQL = sSQL & " AND NOT EXISTS (" & vbNewLine
            sSQL = sSQL & "     SELECT 1" & vbNewLine
            sSQL = sSQL & "     FROM eds_contestacao_log_tb eds_contestacao_log_tb WITH (NOLOCK)" & vbNewLine
            sSQL = sSQL & "     WHERE eds_contestacao_log_tb.proposta_id = ps_mov_endosso_financeiro_tb.proposta_id" & vbNewLine
            sSQL = sSQL & "         AND eds_contestacao_log_tb.endosso_id = ps_mov_endosso_financeiro_tb.endosso_id" & vbNewLine
            sSQL = sSQL & "     )" & vbNewLine
        End If
        sSQL = sSQL & "GROUP BY proposta_tb.produto_id" & vbNewLine
        sSQL = sSQL & " ,ps_acerto_pagamento_tb.voucher_id" & vbNewLine
        sSQL = sSQL & " ,RMNC_ITEM_MDLD_HIST_TB.CD_RMNC_SGRO" & vbNewLine
        sSQL = sSQL & " ,RMNC_ITEM_MDLD_HIST_TB.CD_TIP_RMNC_SGRO" & vbNewLine
        sSQL = sSQL & " ,eds_ctr_sgro.NR_SLCT_EDS_SGRA" & vbNewLine
        sSQL = sSQL & " ,endosso_resumo_tb.val_financeiro" & vbNewLine
        sSQL = sSQL & " ,endosso_resumo_tb.val_iof" & vbNewLine
        sSQL = sSQL & " ,endosso_resumo_tb.val_comissao" & vbNewLine
        sSQL = sSQL & " ,endosso_resumo_tb.val_ir" & vbNewLine
        sSQL = sSQL & " ,endosso_resumo_tb.val_ir_estipulante" & vbNewLine
        sSQL = sSQL & " ,endosso_resumo_tb.val_comissao_estipulante" & vbNewLine
        sSQL = sSQL & " ,endosso_resumo_tb.val_desconto_comercial" & vbNewLine
        sSQL = sSQL & " ,endosso_resumo_tb.val_adic_fracionamento" & vbNewLine
        sSQL = sSQL & " ,endosso_resumo_tb.endosso_resumo_id" & vbNewLine
        sSQL = sSQL & " ,endosso_resumo_tb.endosso_id" & vbNewLine
        sSQL = sSQL & " ,item_produto_tb.cod_produto_bb" & vbNewLine
        sSQL = sSQL & " ,isnull(proposta_adesao_tb.proposta_bb, proposta_fechada_tb.proposta_bb)" & vbNewLine
        sSQL = sSQL & " ,endosso_tb.num_endosso_bb" & vbNewLine
        sSQL = sSQL & " ,endosso_tb.proposta_id" & vbNewLine
        sSQL = sSQL & " ,endosso_tb.dt_pedido_endosso" & vbNewLine
        sSQL = sSQL & " ,ISNULL(apolice_tb.ramo_id, proposta_adesao_tb.ramo_id)" & vbNewLine
        sSQL = sSQL & " ,cod_produto" & vbNewLine
        sSQL = sSQL & " ,cod_modalidade" & vbNewLine
        sSQL = sSQL & " ,cod_item_modalidade" & vbNewLine
        sSQL = sSQL & " ,produto_tb.prazo_restituicao_integral" & vbNewLine
        sSQL = sSQL & " ,produto_tb.prazo_restituicao_integral_renov" & vbNewLine
        sSQL = sSQL & " ,endosso_financeiro_tb.val_remuneracao" & vbNewLine
        sSQL = sSQL & " ,endosso_financeiro_tb.val_ir_remuneracao" & vbNewLine
        sSQL = sSQL & " ,CASE " & vbNewLine
        sSQL = sSQL & "     WHEN proposta_tb.produto_id IN (" & vbNewLine
        sSQL = sSQL & "             1201" & vbNewLine
        sSQL = sSQL & "             ,1226" & vbNewLine
        sSQL = sSQL & "             ,1227" & vbNewLine
        sSQL = sSQL & "             )" & vbNewLine
        sSQL = sSQL & "         THEN item_mdld_prd_hist_tb.in_rmnc_sgro" & vbNewLine
        sSQL = sSQL & "     ELSE CASE " & vbNewLine
        sSQL = sSQL & "             WHEN ISNULL(RMNC_ITEM_MDLD_HIST_TB.CD_RMNC_SGRO, 0) > 0" & vbNewLine
        sSQL = sSQL & "                 THEN 'S'" & vbNewLine
        sSQL = sSQL & "             ELSE 'N'" & vbNewLine
        sSQL = sSQL & "             END" & vbNewLine
        sSQL = sSQL & "     END" & vbNewLine
        sSQL = sSQL & "UNION" & vbNewLine
        sSQL = sSQL & "SELECT DISTINCT max(ps_mov_cliente_tb.movimentacao_id) movimentacao_id" & vbNewLine
        sSQL = sSQL & " ,proposta_tb.produto_id" & vbNewLine
        sSQL = sSQL & " ,ps_acerto_pagamento_tb.voucher_id" & vbNewLine
        sSQL = sSQL & " ,item_produto_tb.cod_produto_bb" & vbNewLine
        sSQL = sSQL & " ,isnull(proposta_adesao_tb.proposta_bb, proposta_fechada_tb.proposta_bb) AS proposta_bb" & vbNewLine
        sSQL = sSQL & " ,endosso_tb.num_endosso_bb" & vbNewLine
        sSQL = sSQL & " ,endosso_tb.proposta_id" & vbNewLine
        sSQL = sSQL & " ,endosso_resumo_tb.endosso_id" & vbNewLine
        sSQL = sSQL & " ,restituicao_integral = CASE " & vbNewLine
        sSQL = sSQL & "     WHEN ISNULL(produto_tb.prazo_restituicao_integral, 0) = 0" & vbNewLine
        sSQL = sSQL & "         AND ISNULL(prazo_restituicao_integral_renov, 0) = 0" & vbNewLine
        sSQL = sSQL & "         THEN 's'" & vbNewLine
        sSQL = sSQL & "     ELSE 'n'" & vbNewLine
        sSQL = sSQL & "     END" & vbNewLine
        sSQL = sSQL & " ,endosso_resumo_tb.val_financeiro" & vbNewLine
        sSQL = sSQL & " ,endosso_resumo_tb.val_iof" & vbNewLine
        sSQL = sSQL & " ,(endosso_resumo_tb.val_comissao - endosso_resumo_tb.val_ir) val_corretagem" & vbNewLine
        sSQL = sSQL & " ,endosso_resumo_tb.val_ir" & vbNewLine
        sSQL = sSQL & " ,ISNULL(endosso_resumo_tb.val_ir_estipulante, 0) AS val_ir_estipulante" & vbNewLine
        sSQL = sSQL & " ,(endosso_resumo_tb.val_comissao_estipulante - ISNULL(endosso_resumo_tb.val_ir_estipulante, 0)) val_estipulante" & vbNewLine
        sSQL = sSQL & " ,cast((" & vbNewLine
        sSQL = sSQL & "         (" & vbNewLine
        sSQL = sSQL & "             endosso_resumo_tb.val_financeiro - --valor Financeiro" & vbNewLine
        sSQL = sSQL & "             endosso_resumo_tb.val_iof - --Valor IOF" & vbNewLine
        sSQL = sSQL & "             (endosso_resumo_tb.val_comissao - endosso_resumo_tb.val_ir) - --Corretagem Liquida" & vbNewLine
        sSQL = sSQL & "             (endosso_resumo_tb.val_comissao_estipulante - ISNULL(endosso_resumo_tb.val_ir_estipulante, 0))" & vbNewLine
        sSQL = sSQL & "             ) * - 1" & vbNewLine
        sSQL = sSQL & "         ) AS NUMERIC(15, 2)) val_movimentacao" & vbNewLine
        sSQL = sSQL & " ,0 custo_apolice" & vbNewLine
        sSQL = sSQL & " ,endosso_resumo_tb.val_desconto_comercial" & vbNewLine
        sSQL = sSQL & " ,endosso_resumo_tb.val_adic_fracionamento" & vbNewLine
        sSQL = sSQL & " ,endosso_tb.dt_pedido_endosso AS dt_inicio_vigencia" & vbNewLine
        sSQL = sSQL & " ,ISNULL(apolice_tb.ramo_id, proposta_adesao_tb.ramo_id) AS ramo_id" & vbNewLine
        sSQL = sSQL & " ,ISNULL(ISNULL(eds_ctr_sgro.NR_SLCT_EDS_SGRA, endosso_resumo_tb.endosso_resumo_id), 0) AS endosso_resumo_id" & vbNewLine
        sSQL = sSQL & " ,ISNULL(proposta_processo_susep_tb.cod_produto, 0) AS cd_prd" & vbNewLine
        sSQL = sSQL & " ,ISNULL(proposta_processo_susep_tb.cod_modalidade, 0) AS cd_mdld" & vbNewLine
        sSQL = sSQL & " ,ISNULL(proposta_processo_susep_tb.cod_item_modalidade, 0) AS cd_item_mdld" & vbNewLine
        sSQL = sSQL & " ,indicador_remuneracao = CASE " & vbNewLine
        sSQL = sSQL & "     WHEN proposta_tb.produto_id IN (" & vbNewLine
        sSQL = sSQL & "             1201" & vbNewLine
        sSQL = sSQL & "             ,1226" & vbNewLine
        sSQL = sSQL & "             ,1227" & vbNewLine
        sSQL = sSQL & "             )" & vbNewLine
        sSQL = sSQL & "         THEN item_mdld_prd_hist_tb.in_rmnc_sgro" & vbNewLine
        sSQL = sSQL & "     ELSE CASE " & vbNewLine
        sSQL = sSQL & "             WHEN ISNULL(RMNC_ITEM_MDLD_HIST_TB.CD_RMNC_SGRO, 0) > 0" & vbNewLine
        sSQL = sSQL & "                 THEN 'S'" & vbNewLine
        sSQL = sSQL & "             ELSE 'N'" & vbNewLine
        sSQL = sSQL & "             END" & vbNewLine
        sSQL = sSQL & "     END" & vbNewLine
        sSQL = sSQL & " ,codigo_remuneracao = ISNULL(RMNC_ITEM_MDLD_HIST_TB.CD_RMNC_SGRO, 0)" & vbNewLine
        sSQL = sSQL & " ,tipo_remuneracao = ISNULL(RMNC_ITEM_MDLD_HIST_TB.CD_TIP_RMNC_SGRO, 0)" & vbNewLine
        sSQL = sSQL & " ,pessoa_remuneracao = 0" & vbNewLine
        sSQL = sSQL & " ,val_remuneracao = ISNULL(endosso_financeiro_tb.val_remuneracao, 0)" & vbNewLine
        sSQL = sSQL & " ,val_ir_remuneracao = ISNULL(endosso_financeiro_tb.val_ir_remuneracao, 0)" & vbNewLine
        sSQL = sSQL & "FROM ps_mov_cliente_tb ps_mov_cliente_tb WITH (NOLOCK)" & vbNewLine
        sSQL = sSQL & "JOIN ps_acerto_pagamento_tb ps_acerto_pagamento_tb WITH (NOLOCK) ON ps_mov_cliente_tb.acerto_id = ps_acerto_pagamento_tb.acerto_id" & vbNewLine
        sSQL = sSQL & " AND ps_acerto_pagamento_tb.situacao = 'e'" & vbNewLine
        sSQL = sSQL & "JOIN ps_mov_endosso_financeiro_tb ps_mov_endosso_financeiro_tb WITH (NOLOCK) ON ps_mov_cliente_tb.movimentacao_id = ps_mov_endosso_financeiro_tb.movimentacao_id" & vbNewLine
        sSQL = sSQL & "JOIN endosso_financeiro_tb endosso_financeiro_tb WITH (NOLOCK) ON ps_mov_endosso_financeiro_tb.proposta_id = endosso_financeiro_tb.proposta_id" & vbNewLine
        sSQL = sSQL & " AND ps_mov_endosso_financeiro_tb.endosso_id = endosso_financeiro_tb.endosso_id" & vbNewLine
        If Endosso_Contestacao Then
       'Endosso de contesta��o - garantir que o evento foi aceito para n�o gerar queda no 987 por endosso n�o localizado
        sSQL = sSQL & "JOIN eds_contestacao_log_tb eds_contestacao_log_tb WITH (NOLOCK)" & vbNewLine
        sSQL = sSQL & " ON eds_contestacao_log_tb.proposta_id = endosso_financeiro_tb.proposta_id" & vbNewLine
        sSQL = sSQL & " AND eds_contestacao_log_tb.endosso_id = endosso_financeiro_tb.endosso_id" & vbNewLine
        sSQL = sSQL & " JOIN seguros_db.dbo.ps_movimentacao_tb  ps_movimentacao_tb WITH (NOLOCK) " & vbNewLine
        sSQL = sSQL & " ON ps_movimentacao_tb.movimentacao_id = ps_mov_cliente_tb.movimentacao_id " & vbNewLine
        sSQL = sSQL & " AND ps_movimentacao_tb.flag_aprovacao='S' " & vbNewLine
        'Endosso de contesta��o - garantir que o evento foi aceito para n�o gerar queda no 987 por endosso n�o localizado
        End If
        sSQL = sSQL & "JOIN endosso_resumo_tb endosso_resumo_tb WITH (NOLOCK) ON endosso_resumo_tb.endosso_resumo_id = endosso_financeiro_tb.endosso_resumo_id" & vbNewLine
        sSQL = sSQL & "JOIN endosso_tb endosso_tb WITH (NOLOCK) ON endosso_resumo_tb.proposta_id = endosso_tb.proposta_id" & vbNewLine
        sSQL = sSQL & " AND endosso_resumo_tb.endosso_id = endosso_tb.endosso_id" & vbNewLine
        sSQL = sSQL & "JOIN proposta_tb proposta_tb WITH (NOLOCK) ON endosso_tb.proposta_id = proposta_tb.proposta_id" & vbNewLine
        sSQL = sSQL & "LEFT JOIN proposta_adesao_tb proposta_adesao_tb WITH (NOLOCK) ON proposta_tb.proposta_id = proposta_adesao_tb.proposta_id" & vbNewLine
        sSQL = sSQL & "LEFT JOIN proposta_fechada_tb proposta_fechada_tb WITH (NOLOCK) ON proposta_tb.proposta_id = proposta_fechada_tb.proposta_id" & vbNewLine
        sSQL = sSQL & "LEFT JOIN apolice_tb apolice_tb WITH (NOLOCK) ON proposta_tb.proposta_id = apolice_tb.proposta_id" & vbNewLine
        sSQL = sSQL & "JOIN item_produto_tb item_produto_tb WITH (NOLOCK) ON proposta_tb.produto_id = item_produto_tb.produto_id" & vbNewLine
        sSQL = sSQL & " AND ISNULL(proposta_adesao_tb.ramo_id, apolice_tb.ramo_id) = item_produto_tb.ramo_id" & vbNewLine
        sSQL = sSQL & "JOIN produto_tb produto_tb WITH (NOLOCK) ON proposta_tb.produto_id = produto_tb.produto_id" & vbNewLine
        sSQL = sSQL & "LEFT JOIN proposta_processo_susep_tb WITH (NOLOCK) ON proposta_processo_susep_tb.proposta_id = proposta_tb.proposta_id" & vbNewLine
        sSQL = sSQL & "LEFT JOIN als_operacao_db.dbo.eds_ctr_sgro eds_ctr_sgro WITH (NOLOCK) ON eds_ctr_sgro.cd_prd = proposta_processo_susep_tb.cod_produto" & vbNewLine
        sSQL = sSQL & " AND eds_ctr_sgro.cd_mdld = proposta_processo_susep_tb.cod_modalidade" & vbNewLine
        sSQL = sSQL & " AND eds_ctr_sgro.cd_item_mdld = proposta_processo_susep_tb.cod_item_modalidade" & vbNewLine
        sSQL = sSQL & " AND eds_ctr_sgro.nr_ctr_sgro = proposta_processo_susep_tb.num_contrato_seguro" & vbNewLine
        sSQL = sSQL & " AND CONVERT(VARCHAR(6), eds_ctr_sgro.DT_CPTC_EDS, 112) = endosso_resumo_tb.ano_mes_devolucao" & vbNewLine
        sSQL = sSQL & "LEFT JOIN als_produto_db.dbo.RMNC_ITEM_MDLD_HIST_TB RMNC_ITEM_MDLD_HIST_TB WITH (NOLOCK) ON RMNC_ITEM_MDLD_HIST_TB.CD_PRD_BB = proposta_processo_susep_tb.cod_produto" & vbNewLine
        sSQL = sSQL & " AND RMNC_ITEM_MDLD_HIST_TB.CD_MDLD_BB = proposta_processo_susep_tb.cod_modalidade" & vbNewLine
        sSQL = sSQL & " AND RMNC_ITEM_MDLD_HIST_TB.CD_ITEM_MDLD = proposta_processo_susep_tb.cod_item_modalidade" & vbNewLine
        sSQL = sSQL & " AND RMNC_ITEM_MDLD_HIST_TB.DT_INC_VGC_RMNC <= proposta_tb.dt_contratacao" & vbNewLine
        sSQL = sSQL & " AND (" & vbNewLine
        sSQL = sSQL & "     RMNC_ITEM_MDLD_HIST_TB.DT_FIM_VGC_RMNC >= proposta_tb.dt_contratacao" & vbNewLine
        sSQL = sSQL & "     OR RMNC_ITEM_MDLD_HIST_TB.DT_FIM_VGC_RMNC IS NULL" & vbNewLine
        sSQL = sSQL & "     )" & vbNewLine
        sSQL = sSQL & "LEFT JOIN als_produto_db.dbo.item_mdld_prd_hist_tb item_mdld_prd_hist_tb WITH (NOLOCK) ON item_mdld_prd_hist_tb.CD_PRD_BB = proposta_processo_susep_tb.cod_produto" & vbNewLine
        sSQL = sSQL & " AND item_mdld_prd_hist_tb.CD_MDLD_BB = proposta_processo_susep_tb.cod_modalidade" & vbNewLine
        sSQL = sSQL & " AND item_mdld_prd_hist_tb.CD_ITEM_MDLD = proposta_processo_susep_tb.cod_item_modalidade" & vbNewLine
        sSQL = sSQL & " AND item_mdld_prd_hist_tb.DT_INICIO_VIGENCIA <= proposta_tb.dt_contratacao" & vbNewLine
        sSQL = sSQL & " AND (" & vbNewLine
        sSQL = sSQL & "     item_mdld_prd_hist_tb.DT_FIM_VIGENCIA >= proposta_tb.dt_contratacao" & vbNewLine
        sSQL = sSQL & "     OR item_mdld_prd_hist_tb.DT_FIM_VIGENCIA IS NULL" & vbNewLine
        sSQL = sSQL & "     )" & vbNewLine
        sSQL = sSQL & "WHERE ps_mov_cliente_tb.arquivo_retorno_bb IS NULL" & vbNewLine
        sSQL = sSQL & " AND upper(proposta_tb.situacao) <> 'T'" & vbNewLine
        sSQL = sSQL & " AND ps_acerto_pagamento_tb.voucher_id IS NOT NULL" & vbNewLine
        sSQL = sSQL & " AND ps_mov_cliente_tb.data_retorno_bb IS NULL" & vbNewLine
        sSQL = sSQL & " AND produto_tb.processa_restituicao_automatica = 's'" & vbNewLine
        sSQL = sSQL & " AND (" & vbNewLine
        sSQL = sSQL & "     proposta_tb.produto_id NOT IN (" & vbNewLine
        sSQL = sSQL & "         12" & vbNewLine
        sSQL = sSQL & "         ,121" & vbNewLine
        sSQL = sSQL & "         ,135" & vbNewLine
        sSQL = sSQL & "         ,136" & vbNewLine
        sSQL = sSQL & "         ,716" & vbNewLine
        sSQL = sSQL & "         )" & vbNewLine
        sSQL = sSQL & "     )" & vbNewLine
        sSQL = sSQL & " AND ps_mov_cliente_tb.canc_endosso_id IS NULL" & vbNewLine
        sSQL = sSQL & " AND endosso_tb.tp_endosso_id = 200" & vbNewLine
        If Not Endosso_Contestacao Then
            sSQL = sSQL & " AND NOT EXISTS (" & vbNewLine
            sSQL = sSQL & "     SELECT 1" & vbNewLine
            sSQL = sSQL & "     FROM eds_contestacao_log_tb eds_contestacao_log_tb WITH (NOLOCK)" & vbNewLine
            sSQL = sSQL & "     WHERE eds_contestacao_log_tb.proposta_id = ps_mov_endosso_financeiro_tb.proposta_id" & vbNewLine
            sSQL = sSQL & "         AND eds_contestacao_log_tb.endosso_id = ps_mov_endosso_financeiro_tb.endosso_id" & vbNewLine
            sSQL = sSQL & "     )" & vbNewLine
        End If
        sSQL = sSQL & "GROUP BY proposta_tb.produto_id" & vbNewLine
        sSQL = sSQL & " ,ps_acerto_pagamento_tb.voucher_id" & vbNewLine
        sSQL = sSQL & " ,RMNC_ITEM_MDLD_HIST_TB.CD_RMNC_SGRO" & vbNewLine
        sSQL = sSQL & " ,RMNC_ITEM_MDLD_HIST_TB.CD_TIP_RMNC_SGRO" & vbNewLine
        sSQL = sSQL & " ,eds_ctr_sgro.NR_SLCT_EDS_SGRA" & vbNewLine
        sSQL = sSQL & " ,endosso_resumo_tb.val_financeiro" & vbNewLine
        sSQL = sSQL & " ,endosso_resumo_tb.val_iof" & vbNewLine
        sSQL = sSQL & " ,endosso_resumo_tb.val_comissao" & vbNewLine
        sSQL = sSQL & " ,endosso_resumo_tb.val_ir" & vbNewLine
        sSQL = sSQL & " ,endosso_resumo_tb.val_ir_estipulante" & vbNewLine
        sSQL = sSQL & " ,endosso_resumo_tb.val_comissao_estipulante" & vbNewLine
        sSQL = sSQL & " ,endosso_resumo_tb.val_desconto_comercial" & vbNewLine
        sSQL = sSQL & " ,endosso_resumo_tb.val_adic_fracionamento" & vbNewLine
        sSQL = sSQL & " ,endosso_resumo_tb.endosso_resumo_id" & vbNewLine
        sSQL = sSQL & " ,endosso_resumo_tb.endosso_id" & vbNewLine
        sSQL = sSQL & " ,item_produto_tb.cod_produto_bb" & vbNewLine
        sSQL = sSQL & " ,isnull(proposta_adesao_tb.proposta_bb, proposta_fechada_tb.proposta_bb)" & vbNewLine
        sSQL = sSQL & " ,endosso_tb.num_endosso_bb" & vbNewLine
        sSQL = sSQL & " ,endosso_tb.proposta_id" & vbNewLine
        sSQL = sSQL & " ,endosso_tb.dt_pedido_endosso" & vbNewLine
        sSQL = sSQL & " ,ISNULL(apolice_tb.ramo_id, proposta_adesao_tb.ramo_id)" & vbNewLine
        sSQL = sSQL & " ,cod_produto" & vbNewLine
        sSQL = sSQL & " ,cod_modalidade" & vbNewLine
        sSQL = sSQL & " ,cod_item_modalidade" & vbNewLine
        sSQL = sSQL & " ,produto_tb.prazo_restituicao_integral" & vbNewLine
        sSQL = sSQL & " ,produto_tb.prazo_restituicao_integral_renov" & vbNewLine
        sSQL = sSQL & " ,endosso_financeiro_tb.val_remuneracao" & vbNewLine
        sSQL = sSQL & " ,endosso_financeiro_tb.val_ir_remuneracao" & vbNewLine
        sSQL = sSQL & " ,CASE " & vbNewLine
        sSQL = sSQL & "     WHEN proposta_tb.produto_id IN (" & vbNewLine
        sSQL = sSQL & "             1201" & vbNewLine
        sSQL = sSQL & "             ,1226" & vbNewLine
        sSQL = sSQL & "             ,1227" & vbNewLine
        sSQL = sSQL & "             )" & vbNewLine
        sSQL = sSQL & "         THEN item_mdld_prd_hist_tb.in_rmnc_sgro" & vbNewLine
        sSQL = sSQL & "     ELSE CASE " & vbNewLine
        sSQL = sSQL & "             WHEN ISNULL(RMNC_ITEM_MDLD_HIST_TB.CD_RMNC_SGRO, 0) > 0" & vbNewLine
        sSQL = sSQL & "                 THEN 'S'" & vbNewLine
        sSQL = sSQL & "             ELSE 'N'" & vbNewLine
        sSQL = sSQL & "             END" & vbNewLine
        sSQL = sSQL & "     END" & vbNewLine

        Screen.MousePointer = vbHourglass
        Set rc1 = rdocn1.OpenResultset(sSQL, rdOpenStatic)

        sSQL = "SELECT ramo_id, voucher_id, num_linhas = COUNT(1) FROM #DadosArquivo GROUP BY ramo_id, voucher_id ORDER BY ramo_id, num_linhas DESC, voucher_id ASC "
        Set rc1 = rdocn1.OpenResultset(sSQL, rdOpenStatic)
        If rc1.EOF Then
            Screen.MousePointer = vbDefault
            Call MensagemBatch("Nenhum registro foi localizado.")
            rc1.Close
            Exit Sub
        End If

        Set colGrupoVouchers = agrupar_voucher(rc1, 32000, iRowCount)  '2000

        'Gera o arquivo po grupo
        For i = 1 To colGrupoVouchers.Count

            sSQL = "SELECT * FROM #DadosArquivo WHERE voucher_id IN (" & colGrupoVouchers.Item(i) & ")"
            Set rc1 = rdocn1.OpenResultset(sSQL, rdOpenStatic)

            Call Obtem_Num_Remessa(Arquivo, NumRemessa)

            Nome_Arquivo = Trim(Arquivo) & "." & NumRemessa

            Path_Nome_Arquivo = rpath & Nome_Arquivo
            Path_Nome_Arquivo = App.PATH & "\" & Nome_Arquivo

            rdocn.BeginTrans
            Conta_linha = 0
            tot_val_movimentacao = 0
            While Not rc1.EOF
                StatusBar1.SimpleText = "Gerando o Arquivo de Devolu��es de Premios... " & Format$((Conta_linha / iRowCount(i)) * 100, "0.00") & "%   (" & Trim(Str(i)) & " de " & Trim(Str(colGrupoVouchers.Count)) & ")"
                DoEvents
                If Conta_linha = 0 Then
                    Call Abre_Arquivo
                    TxtRegsGerados.Text = ""
                End If
                Conta_linha = Conta_linha + 1
                lAdmClienteId = Obter_Estipulante(rc1!proposta_id)
                sFlgIsencaoIR = Obter_Flg_Isencao(lAdmClienteId, rc1!produto_id)
                EndossoResumoID = Val(rc1!endosso_resumo_id)
                movimentacao_id = rc1!movimentacao_id
                Produto = Format(rc1!produto_id, "0000")
                Produto = Mid(Produto, Len(Produto) - 2, 3)
                voucher = Right(Format(IIf(IsNull(rc1!voucher_id), 0, rc1!voucher_id), "000000"), 6)
                CodProdutoBB = Format(IIf(IsNull(rc1!cod_produto_bb), "0", rc1!cod_produto_bb), "0000000")
                Proposta_bb = Format(IIf(IsNull(rc1!Proposta_bb), "0", rc1!Proposta_bb), "000000000")
                CdProduto = Format(rc1!cd_prd, "0000")
                CdMdld = Format(rc1!cd_mdld, "0000")
                CdItemMdld = Format(rc1!cd_item_mdld, "000000000")
                If (CodProdutoBB = 8229151) Or (CodProdutoBB = 8229152) Or (CodProdutoBB = 8223150) Then
                    CodProdutoBB = 8229150
                End If
                If Val(Produto) = "14" And Val(Proposta_bb) > 9900000 Then
                    CodProdutoBB = "0678116"
                End If
                perc_ir_estip = Obtem_Perc_IR_Estip(rc1!proposta_id, rc1!endosso_id, rc1!ramo_id, rc1!dt_inicio_vigencia)
                num_endosso_bb = Format(IIf(IsNull(rc1!num_endosso_bb), "0", rc1!num_endosso_bb), "000000000")
                If Val(rc1!endosso_resumo_id) = 0 Then
                    endosso_id = Format(IIf(IsNull(rc1!endosso_id), "0", rc1!endosso_id), "000000000")
                Else
                    endosso_id = Format(IIf(IsNull(rc1!endosso_resumo_id), "0", rc1!endosso_resumo_id), "000000000")
                End If
                If Trim(CdProduto) <> "0000" Then
                    CodProdutoBB = "0000000"
                End If
                restituicao_integral = IIf(UCase(rc1!restituicao_integral) = "S", "T", "P")
                data_devolucao = Format(Data_Sistema, "ddmmyyyy")
                If Val(rc1!endosso_resumo_id) = 0 Then
                    If Produto = 424 Then
                        paridade = ObterParidade(rc1!proposta_id, rc1!endosso_id)
                        val_financeiro = Format((TruncaDecimal(Val(rc1!val_financeiro) * paridade) * -1) * 100, "000000000000000")
                        val_iof = Format((TruncaDecimal(Val(rc1!val_iof) * paridade) * -1) * 100, "000000000000000")
                        val_corretagem = Format((TruncaDecimal(Val(rc1!val_corretagem) * paridade) * -1) * 100, "000000000000000")
                        val_ir = Format((TruncaDecimal(Val(rc1!val_ir) * paridade) * -1) * 100, "000000000000000")
                        val_ir_estipulante = Format(CCur(TruncaDecimal(Val(rc1!val_Estipulante) * perc_ir_estip * -1) * paridade) * 100, "000000000000000")
                        val_Estipulante = Format(CCur(TruncaDecimal(Val(rc1!val_Estipulante) * (1 - perc_ir_estip) * -1) * paridade) * 100, "000000000000000")
                        val_movimentacao = Format((TruncaDecimal(Val(rc1!val_movimentacao)) * 100), "000000000000000")
                        custo_apolice = Format((TruncaDecimal(Val(rc1!custo_apolice) * paridade) * 100) * -1, "000000000000000")
                        val_desconto_comercial = Format((TruncaDecimal(Val(rc1!val_desconto_comercial) * paridade) * 100) * -1, "000000000000000")
                        val_adic_fracionamento = Format((TruncaDecimal(Val(rc1!val_adic_fracionamento) * paridade) * 100) * -1, "000000000000000")
                    Else
                        val_financeiro = Format((Val(rc1!val_financeiro) * -1) * 100, "000000000000000")
                        val_iof = Format((Val(rc1!val_iof) * -1) * 100, "000000000000000")
                        val_corretagem = Format(Abs(Val(rc1!val_corretagem)) * 100, "000000000000000")
                        val_ir = Format((Val(rc1!val_ir) * -1) * 100, "000000000000000")
                        If sFlgIsencaoIR = "S" Then
                            val_ir_estipulante = Format((Val(rc1!val_ir_estipulante) * -1) * 100, "000000000000000")
                            val_Estipulante = Format(CCur((Val(rc1!val_Estipulante) * (1 - 0) * -1)) * 100, "000000000000000")
                        Else
                            val_ir_estipulante = Format((Val(rc1!val_ir_estipulante) * -1) * 100, "000000000000000")
                            val_Estipulante = Format(CCur((Val(rc1!val_Estipulante) * (1 - perc_ir_estip) * -1)) * 100, "000000000000000")
                        End If
                        val_movimentacao = Format((Val(rc1!val_movimentacao) * 100), "000000000000000")
                        custo_apolice = Format((Val(rc1!custo_apolice) * 100) * -1, "000000000000000")
                        val_desconto_comercial = Format((Val(rc1!val_desconto_comercial) * 100) * -1, "000000000000000")
                        val_adic_fracionamento = Format((Val(rc1!val_adic_fracionamento) * 100) * -1, "000000000000000")
                    End If
                Else
                    val_financeiro = Format((Val(rc1!val_financeiro) * -1) * 100, "000000000000000")
                    val_iof = Format((Val(rc1!val_iof) * -1) * 100, "000000000000000")
                    val_corretagem = Format((Val(rc1!val_corretagem) * -1) * 100, "000000000000000")
                    val_ir = Format((Val(rc1!val_ir) * -1) * 100, "000000000000000")
                    val_ir_estipulante = Format((Val(rc1!val_ir_estipulante) * -1) * 100, "000000000000000")
                    val_Estipulante = Format((Val(rc1!val_Estipulante) * -1) * 100, "000000000000000")
                    val_movimentacao = Format((Val(rc1!val_movimentacao) * 100), "000000000000000")
                    custo_apolice = Format((Val(rc1!custo_apolice) * 100) * -1, "000000000000000")
                    val_desconto_comercial = Format((Val(rc1!val_desconto_comercial) * 100) * -1, "000000000000000")
                    val_adic_fracionamento = Format((Val(rc1!val_adic_fracionamento) * 100) * -1, "000000000000000")
                End If
                indicador_remuneracao = rc1!indicador_remuneracao
                codigo_remuneracao = Format(rc1!codigo_remuneracao, "0000")
                tipo_remuneracao = Format(rc1!tipo_remuneracao, "0000")
                pessoa_remuneracao = Format(rc1!pessoa_remuneracao, "000000000")
                val_remuneracao = Format((Val(rc1!val_remuneracao) * 100) * -1, "000000000000000")
                val_ir_remuneracao = Format((Val(rc1!val_ir_remuneracao) * 100) * -1, "000000000000000")
                tot_val_movimentacao = tot_val_movimentacao + Val(rc1!val_movimentacao)
                Linha = Format(Conta_linha, "000000") & _
                        "01" & _
                        Produto & _
                        voucher & _
                        CodProdutoBB & _
                        Proposta_bb & _
                        num_endosso_bb & _
                        endosso_id & _
                        restituicao_integral & _
                        data_devolucao & _
                        val_financeiro & _
                        val_iof & _
                        val_corretagem & _
                        val_ir & _
                        val_Estipulante & _
                        val_ir_estipulante & _
                        val_movimentacao & _
                        custo_apolice & _
                        val_desconto_comercial & _
                        val_adic_fracionamento & _
                        CdProduto & _
                        CdMdld & _
                        CdItemMdld & _
                        indicador_remuneracao & codigo_remuneracao & tipo_remuneracao & pessoa_remuneracao & val_remuneracao & val_ir_remuneracao & _
                        "00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000"
                Linha = Left(Linha & Space(tam_reg), tam_reg)
                Print #1, Linha
                Call Atualiza_Ps_Mov_Cliente(movimentacao_id, EndossoResumoID)
                OrigemEndosso = Origem_Endosso(rc1!proposta_id, rc1!endosso_id)
                Call evento_retorno(Data_Sistema, Produto, rc1!Proposta_bb, 4, 104, NumRemessa, 422, rc1!proposta_id, rc1!endosso_id, OrigemEndosso, rc1!num_endosso_bb)
                sSQL = ""
                sSQL = sSQL & "SET NOCOUNT ON EXEC SEGS11299_SPI"
                sSQL = sSQL & " 'SEGP0422'"
                sSQL = sSQL & ", " & rc1!proposta_id
                If rc1!endosso_id <> 0 Then
                    sSQL = sSQL & "," & rc1!endosso_id
                Else
                    sSQL = sSQL & ",NULL"
                End If
                sSQL = sSQL & ", " & TrocaVirgulaPorPonto(rc1!val_movimentacao)
                sSQL = sSQL & ",'Sistema Autom�tico'"
                sSQL = sSQL & ",'Processamento do arquivo SEG452 ao Banco.'"
                sSQL = sSQL & ",'Gera Arquivo'"
                sSQL = sSQL & ", NULL"
                sSQL = sSQL & ",'SEG452." & Right("00000" & NumRemessa, 4) & "'"
                rdocn.Execute sSQL
                rc1.MoveNext
            Wend
            rc1.Close
            Call Finaliza_arquivo
            If Endosso_Contestacao Then
               ' Call EmailRemessaSEG452(TxtArquivo.Text, tot_val_movimentacao)
            Else
               ' Call EmailRemessaSEG452(TxtArquivo.Text)
            End If
            TxtRegsGerados.Text = Str(Conta_linha)
            TxtRegsGerados.Refresh
            rdocn.CommitTrans
        Next i
    End If
    StatusBar1.SimpleText = "Fim do Processamento..."
    StatusBar1.Refresh
    Screen.MousePointer = vbDefault
    Exit Sub
Erro:
    Screen.MousePointer = vbDefault
    rdocn.RollbackTrans
    Call TrataErroGeral("Processa", Me.name)
    Call TerminaSEGBR
End Sub


Private Function ObterParidade(ByVal sPropostaId, ByVal sEndossoId As String)
    Dim SQL As String
    Dim rcParidade As rdoResultset

    On Error GoTo erro_ObterParidade

    SQL = "select isnull(val_paridade_moeda,1) as val_paridade"
    SQL = SQL & " from endosso_financeiro_tb"
    SQL = SQL & " where proposta_id = " & sPropostaId
    SQL = SQL & " and endosso_id = " & sEndossoId

    Set rcParidade = rdocn.OpenResultset(SQL)


    If Not rcParidade.EOF Then
        ObterParidade = TrocaPontoPorVirgula(rcParidade!val_paridade)
    Else
        ObterParidade = 1
    End If

    Set rcParidade = Nothing

    Exit Function

erro_ObterParidade:

    Call TrataErroGeral("ObterParidade", Me.name)
    TerminaSEGBR


End Function



Private Sub Finaliza_arquivo()

    On Error GoTo Erro

    If Conta_linha > 0 Then
        Call Insere_Arquivo_Versao_Gerado(Arquivo, Val(Conta_linha), Val(Conta_linha + 2), CInt(NumRemessa))
    End If

    '* Grava trailler do arquivo
    '***************************
    If Conta_linha > 0 Then
        If UCase(Left(Arquivo, 6)) = "BBC452" Then
            Linha = "999999" & "05" & "0678" & Format(Conta_linha, "000000")
        Else
            Linha = "999999" & "05" & "0678" & Format(Conta_linha, "000000") & Format(tot_val_movimentacao * 100, "000000000000000")
        End If
        Linha = Left(Linha & Space(tam_reg), tam_reg)
        Print #1, Linha
        Close #1
    End If
    'F.BEZERRA - FLOWBR14413025 29/11/2012
    '   StatusBar1.SimpleText = "Fim do Processamento..."
    '   StatusBar1.Refresh

    Exit Sub

Erro:
    TrataErroGeral "Finaliza_Arquivo", Me.name
    TerminaSEGBR

End Sub




Sub Obtem_Num_Remessa(nome As String, ByRef NumRemessa As String)

    Dim SQL As String
    Dim rcNum As rdoResultset
    Dim LayOutId As String
    Dim versao As String

    On Error GoTo Erro

    SQL = "       SELECT"
    SQL = SQL & "     l.layout_id"
    SQL = SQL & " FROM"
    SQL = SQL & "     controle_proposta_db..layout_tb l"
    SQL = SQL & " WHERE"
    SQL = SQL & "     l.nome = '" & nome & "'"

    Set rcNum = rdocn.OpenResultset(SQL)

    If rcNum.EOF Then
        Error 1000
    Else
        LayOutId = rcNum(0)

        rcNum.Close

        '    SQL = "       SELECT "
        '    SQL = SQL & "     max(versao) "
        '    SQL = SQL & " FROM "
        '    SQL = SQL & "     controle_proposta_db..arquivo_versao_tb "
        '    SQL = SQL & " WHERE "
        '    SQL = SQL & "     layout_id = " & LayOutId


        SQL = "       SELECT MAX (VERSAO) AS VERSAO "
        SQL = SQL & "     FROM ("
        SQL = SQL & "     SELECT VERSAO = MAX(VERSAO) "
        SQL = SQL & "     FROM CONTROLE_PROPOSTA_DB..ARQUIVO_VERSAO_TB "
        SQL = SQL & "     Where LAYOUT_ID = " & LayOutId
        SQL = SQL & "     Union All "
        SQL = SQL & "     SELECT VERSAO = MAX(VERSAO) "
        SQL = SQL & "     From ABSS.CONTROLE_PROPOSTA_DB.DBO.ARQUIVO_VERSAO_TB "
        SQL = SQL & "     Where LAYOUT_ID = " & LayOutId
        SQL = SQL & "     ) AS TEMP_X "


        Set rcNum = rdocn.OpenResultset(SQL)

        If Not rcNum.EOF Then
            If IsNull(rcNum(0)) Then
                versao = 0
            Else
                versao = rcNum(0)
            End If
        Else
            versao = 0
        End If

        rcNum.Close

        NumRemessa = Format(versao + 1, "0000")

    End If

    Exit Sub

Erro:
    rcNum.Close
    If Err.Number = 1000 Then
        MensagemBatch "O arquivo a ser gerado n�o est� cadastrado! Programa ser� cancelado", vbCritical
    End If
    TrataErroGeral "Obtem_Num_Remessa", Me.name
    TerminaSEGBR
    Resume
End Sub

'pmelo 20110524 - Tratamento 987 para acerto dos valores do Estipulante
Public Function Obter_Estipulante(ByVal lPropostaId As Long) As Long

    Dim sSQL As String
    Dim oRs As rdoResultset
    On Error GoTo Erro


    sSQL = ""
    adSQL sSQL, "SELECT adm_cliente_id "
    adSQL sSQL, "  FROM seguros_db..administracao_apolice_tb WITH (NOLOCK)                                  "
    adSQL sSQL, " WHERE administracao_apolice_tb.dt_fim_administracao IS NULL                          "
    adSQL sSQL, "   AND proposta_id                                    = " & lPropostaId
    adSQL sSQL, "UNION                                                                                 "
    adSQL sSQL, "SELECT administracao_apolice_tb.adm_cliente_id                                        "
    adSQL sSQL, "  FROM seguros_db..proposta_adesao_tb proposta_adesao_tb             WITH (NOLOCK)         "
    adSQL sSQL, "  JOIN seguros_db..apolice_tb apolice_tb                             WITH (NOLOCK)         "
    adSQL sSQL, "    ON apolice_tb.apolice_id                          = proposta_adesao_tb.apolice_id "
    adSQL sSQL, "   AND apolice_tb.ramo_id                             = proposta_adesao_tb.ramo_id    "
    adSQL sSQL, "  JOIN seguros_db..administracao_apolice_tb administracao_apolice_tb WITH (NOLOCK)         "
    adSQL sSQL, "    ON administracao_apolice_tb.proposta_id           = apolice_tb.proposta_id        "
    adSQL sSQL, " WHERE administracao_apolice_tb.dt_fim_administracao IS NULL                          "
    adSQL sSQL, "   AND proposta_adesao_tb.proposta_id                 = " & lPropostaId

    Set oRs = rdocn.OpenResultset(sSQL)

    If Not oRs.EOF Then
        Obter_Estipulante = oRs!adm_cliente_id
    Else
        Obter_Estipulante = 0
    End If

    Set oRs = Nothing

    Exit Function

Erro:
    TrataErroGeral "Obter_Estipulante", Me.name
    TerminaSEGBR

End Function


Public Function Obter_Flg_Isencao(lClienteId As Long, intProdutoId As Integer) As String

    Dim sSQL As String
    Dim oRs As rdoResultset
    On Error GoTo Erro


    sSQL = ""
    adSQL sSQL, "SELECT isento_ir = ISNULL(isento_ir,'N') "
    adSQL sSQL, "  FROM cliente_tb WITH (NOLOCK)               "
    adSQL sSQL, " WHERE cliente_id =                      " & lClienteId

    Set oRs = rdocn.OpenResultset(sSQL)

    If Not oRs.EOF Then
        Obter_Flg_Isencao = oRs!isento_ir
    Else
        Obter_Flg_Isencao = "N"
    End If

    'LGOMES   - 11/04/2017 - Retirar tratamento de isen��o de IR para os produtos 1174,1175 - PM00000203 - Tratamento 987
    ''WBARBOZA - 29/01/2013 - Ajuste no tratamento de isen��o de IR para os produtos 1174,1175 - INC 3801751 - Tratamento 987
    ''If (intProdutoId = 1174 Or intProdutoId = 1175) Then
    ''    Obter_Flg_Isencao = "N"
    ''End If

    Set oRs = Nothing

    Exit Function

Erro:
    TrataErroGeral "Obter_Flg_Isencao", Me.name
    TerminaSEGBR


End Function





Sub Insere_Arquivo_Versao_Gerado(nome As String, qReg As Long, qLinhas As Long, NumRemessa As Integer)

    Dim rcGer As rdoResultset
    Dim SQL As String

    On Error GoTo Erro

    SQL = "exec controle_proposta_db..arquivo_versao_gerado_spi"
    SQL = SQL & "  '" & nome & "'"
    SQL = SQL & ", '" & NumRemessa & "'"
    SQL = SQL & ",  " & qReg
    SQL = SQL & ", '" & Format(Data_Sistema, "yyyymmdd") & "'"
    SQL = SQL & ", '" & qLinhas & "'"
    SQL = SQL & ", '" & cUserName & "'"
    Set rcGer = rdocn.OpenResultset(SQL)

    rcGer.Close

    Exit Sub

Erro:
    TrataErroGeral "Insere_Arquivo_Versao_Gerado", Me.name
    TerminaSEGBR

End Sub


Private Sub Abre_Arquivo()

    Dim rc As rdoResultset
    Dim Data_Remessa As String

    On Error GoTo Erro

    Open Path_Nome_Arquivo For Output As 1
    Linha = "000000" & "05" & "0678" & Format(NumRemessa, "0000")
    Linha = Left(Linha & Space(tam_reg), tam_reg)
    Print #1, Linha
    TxtArquivo.Text = Nome_Arquivo
    TxtArquivo.Refresh

    Exit Sub

Erro:
    TrataErroGeral "Abre_Arquivo", Me.name
    TerminaSEGBR

End Sub


'Barney - 03/05/2004
'Corre��o para atualizar as movimenta��es do Conta-Cart�o
'Private Sub Atualiza_Ps_Mov_Cliente(sMovimentacaoId As String)
Private Sub Atualiza_Ps_Mov_Cliente(sMovimentacaoId As String, sEndossoResumoID As String)
    Dim rc As rdoResultset

    On Error GoTo Erro

    SQL = "exec ps_mov_cliente_spu "
    SQL = SQL & "   " & sMovimentacaoId
    SQL = SQL & ", '" & "s" & "'"
    SQL = SQL & ", '" & Format(Data_Sistema, "yyyymmdd") & "'"
    SQL = SQL & ", '" & Nome_Arquivo & "'"
    SQL = SQL & ", '" & cUserName & "'"
    'Barney - 03/05/2004 - Inclus�o do endosso_resumo
    SQL = SQL & ", " & EndossoResumoID
    Set rc = rdocn.OpenResultset(SQL)

    rc.Close
    Set rc = Nothing

    Exit Sub

Erro:
    TrataErroGeral "Atualiza_Ps_Mov_Cliente", Me.name
    TerminaSEGBR

End Sub


Sub Conexao_Auxiliar()

    On Error GoTo Erro
    With rdocn1
        .Connect = rdocn.Connect
        .CursorDriver = rdUseServer
        .QueryTimeout = 3600
        .EstablishConnection rdDriverNoPrompt
    End With

    With rdocn2
        .Connect = rdocn.Connect
        .CursorDriver = rdUseServer
        .QueryTimeout = 3600
        .EstablishConnection rdDriverNoPrompt
    End With

    With rdocn3
        .Connect = rdocn.Connect
        .CursorDriver = rdUseServer
        .QueryTimeout = 3600
        .EstablishConnection rdDriverNoPrompt
    End With

    Exit Sub

Erro:
    MensagemBatch "Conex�o com BRCAPDB indispon�vel.", vbCritical
    TerminaSEGBR
End Sub


Sub AtualizarPsMovClienteEndossoResumo(lEndossoId As Long)

    On Error GoTo TrataErro

    Dim sSQL As String
    Dim rcMov As rdoResultset

    sSQL = ""
    sSQL = sSQL & "      SELECT movimentacao_id"
    sSQL = sSQL & "        FROM ps_mov_endosso_financeiro_tb"
    sSQL = sSQL & "  INNER JOIN endosso_financeiro_tb"
    sSQL = sSQL & "          ON endosso_financeiro_tb.proposta_id = ps_mov_endosso_financeiro_tb.proposta_id"
    sSQL = sSQL & "         AND endosso_financeiro_tb.endosso_id  = ps_mov_endosso_financeiro_tb.endosso_id"
    sSQL = sSQL & "  INNER JOIN endosso_resumo_tb"
    sSQL = sSQL & "          ON endosso_resumo_tb.endosso_resumo_id = endosso_financeiro_tb.endosso_resumo_id"
    sSQL = sSQL & "       WHERE endosso_resumo_tb.endosso_id = " & lEndossoId

    Set rcMov = rdocn3.OpenResultset(sSQL)

    While Not rcMov.EOF

        Call Atualiza_Ps_Mov_Cliente(rcMov!movimentacao_id, 0)

        rcMov.MoveNext
    Wend

    rcMov.Close
    Set rcMov = Nothing

    Exit Sub

TrataErro:
    MensagemBatch "AtualizarPsMovClienteEndossoResumo.", vbCritical
    TerminaSEGBR

End Sub
Private Sub Processa_BBC452()
'racras metodo criado �para processar produtos bbc
    Dim sSQL As String
    Dim rpath As String
    Dim perc_ir_estip As Double
    Dim iProdutoContaCartao As Integer
    Dim paridade As Currency

    'carsilva - 17/08/2004
    Dim OrigemEndosso As String

    'pmelo 20110622 - Tratamento 987 para acerto dos valores do Estipulante
    Dim lAdmClienteId As Long

    'PMELO 20111207 - Demanda estipulante com isen��o de IR
    Dim sFlgIsencaoIR As String

    On Error GoTo Erro

    StatusBar1.SimpleText = "Gerando o Arquivo de Devolu��es de Premios... "
    StatusBar1.Refresh

    '* Obtem a Versao do Arquivo
    '***************************
    Call Obtem_Num_Remessa(Arquivo, NumRemessa)
    Versao_Arquivo = NumRemessa

    Nome_Arquivo = Trim(Arquivo) & "." & NumRemessa

    rpath = LerArquivoIni("ARQUIVOS DEVOLUCAO PREMIO", "SEG452_PATH")

    iProdutoContaCartao = BuscaParametro("Produto ContaCartao")
    
    rpath = "c:\QLD_ArqDevPremio\Gerados"

    Path_Nome_Arquivo = rpath & "\" & Nome_Arquivo
    'Path_Nome_Arquivo =

    sSQL = ""
    adSQL sSQL, "SELECT a.movimentacao_id,"
    adSQL sSQL, "   f.produto_id,"
    adSQL sSQL, "   b.voucher_id,"
    adSQL sSQL, "   h.cod_produto_bb,"
    adSQL sSQL, "   isnull(g.proposta_bb, g2.proposta_bb) AS proposta_bb,"
    adSQL sSQL, "   e.num_endosso_bb,"
    adSQL sSQL, "   e.proposta_id,"
    adSQL sSQL, "   e.endosso_id,"
    adSQL sSQL, "   ISNULL(i.restituicao_integral, 'n') AS restituicao_integral,"
    adSQL sSQL, "   ISNULL(d.val_financeiro, 0) val_financeiro,"
    adSQL sSQL, "   ISNULL(d.val_iof, 0) val_iof,"
    adSQL sSQL, "   ISNULL((d.val_comissao - ISNULL(d.val_ir, 0)), 0) val_corretagem,"
    adSQL sSQL, "   ISNULL(d.val_ir, 0) AS val_ir,"


    'PMELO 20111206 - Demanda estipulante com isencao de IR
    'Barney - 05/02/2003 - Inclus�o do campo Val_Ir_Estipulante = 0
    'adSQL sSQL, "   0 val_ir_estipulante,"
    adSQL sSQL, "   ISNULL(d.val_ir_estipulante, 0) as val_ir_estipulante,"


    adSQL sSQL, "   ISNULL(d.val_comissao_estipulante, 0) AS val_estipulante,"
    adSQL sSQL, "   ISNULL(a.val_movimentacao, 0) AS val_movimentacao,"
    adSQL sSQL, "   ISNULL(d.custo_apolice, 0) AS custo_apolice,"
    adSQL sSQL, "   ISNULL(d.val_desconto_comercial, 0) AS val_desconto_comercial,"
    adSQL sSQL, "   ISNULL(d.val_adic_fracionamento, 0) AS val_adic_fracionamento,"
    adSQL sSQL, "   e.dt_pedido_endosso AS dt_inicio_vigencia,"
    adSQL sSQL, "   ISNULL(g3.ramo_id, g.ramo_id) AS ramo_id, "
    adSQL sSQL, "   0 endosso_resumo_id, "
    adSQL sSQL, "   ISNULL(proposta_processo_susep_tb.cod_produto, 0) as cd_prd, "
    adSQL sSQL, "   ISNULL(proposta_processo_susep_tb.cod_modalidade, 0) as cd_mdld, "
    adSQL sSQL, "   ISNULL(proposta_processo_susep_tb.cod_item_modalidade, 0) as cd_item_mdld, "

    'bmoraes 21.01.2009
    adSQL sSQL, "   indicador_remuneracao = CASE WHEN ISNULL(rm.CD_RMNC_SGRO, 0) > 0  THEN 'S' ELSE 'N' END,  "
    adSQL sSQL, "   codigo_remuneracao = ISNULL(rm.CD_RMNC_SGRO, 0 ), "
    adSQL sSQL, "   tipo_remuneracao = ISNULL(rm.CD_TIP_RMNC_SGRO, 0 ), "
    adSQL sSQL, "   pessoa_remuneracao = 0, "
    adSQL sSQL, "   val_remuneracao = ISNULL(d.val_remuneracao, 0 ), "
    adSQL sSQL, "   val_ir_remuneracao = ISNULL(d.val_ir_remuneracao, 0 ) "
    '------------------------------------------------------------

    adSQL sSQL, "  FROM ps_mov_cliente_tb a WITH (NOLOCK)"
    adSQL sSQL, "  JOIN ps_acerto_pagamento_tb b WITH (NOLOCK)"
    adSQL sSQL, "    ON a.acerto_id = b.acerto_id"

    'Vbarbosa - O arquivo de retorno poder� ser gerado para os acertos com voucher
    'emaior - 12/11/2007 - Restitui��es que tiverem pagamento aprovado
    'adSQL sSQL, "   AND b.situacao IN('a','e')"
    adSQL sSQL, "   AND b.situacao = 'e'"

    adSQL sSQL, "  JOIN ps_mov_endosso_financeiro_tb c WITH (NOLOCK)"
    adSQL sSQL, "    ON a.movimentacao_id = c.movimentacao_id"
    adSQL sSQL, "  JOIN endosso_financeiro_tb d WITH (NOLOCK)"
    adSQL sSQL, "    ON c.proposta_id = d.proposta_id"
    adSQL sSQL, "   AND c.endosso_id = d.endosso_id"
    adSQL sSQL, "  JOIN endosso_tb e WITH (NOLOCK)"
    adSQL sSQL, "    ON d.proposta_id = e.proposta_id"
    adSQL sSQL, "   AND d.endosso_id = e.endosso_id"
    adSQL sSQL, "  JOIN proposta_tb f WITH (NOLOCK)"
    adSQL sSQL, "    ON e.proposta_id = f.proposta_id"
    adSQL sSQL, "  LEFT JOIN proposta_adesao_tb g WITH (NOLOCK)"
    adSQL sSQL, "    ON f.proposta_id = g.proposta_id"
    adSQL sSQL, "  LEFT JOIN proposta_fechada_tb g2 WITH (NOLOCK)"
    adSQL sSQL, "    ON f.proposta_id = g2.proposta_id"
    adSQL sSQL, "  LEFT JOIN apolice_tb g3 WITH (NOLOCK)"
    adSQL sSQL, "    ON f.proposta_id = g3.proposta_id"
    adSQL sSQL, "  JOIN item_produto_tb h WITH (NOLOCK)"
    adSQL sSQL, "    ON f.produto_id = h.produto_id"
    adSQL sSQL, "   AND ISNULL(g.ramo_id, g3.ramo_id) = h.ramo_id"
    adSQL sSQL, "  LEFT JOIN avaliacao_restituicao_tb i WITH (NOLOCK)"
    adSQL sSQL, "    ON e.proposta_id = i.proposta_id"
    adSQL sSQL, "   AND e.endosso_id = i.endosso_id"
    adSQL sSQL, "   AND i.situacao = 'a'"
    adSQL sSQL, "  JOIN produto_tb j WITH (NOLOCK)"
    adSQL sSQL, "    ON f.produto_id = j.produto_id"
    adSQL sSQL, "  LEFT JOIN proposta_processo_susep_tb WITH (NOLOCK)"
    adSQL sSQL, "    ON proposta_processo_susep_tb.proposta_id = f.proposta_id"

    'bcarneiro - 31/10/2006 - Restitui��o Agr�cola
    adSQL sSQL, "  LEFT JOIN proposta_subvencao_tb WITH (NOLOCK)"
    adSQL sSQL, "    ON proposta_subvencao_tb.proposta_id = f.proposta_id"
    ''''''''''''''''''''''''

    'bmoraes 21.01.2009
    adSQL sSQL, "  LEFT JOIN als_produto_db..RMNC_ITEM_MDLD_HIST_TB rm WITH (NOLOCK)"
    adSQL sSQL, "  ON rm.CD_PRD_BB = proposta_processo_susep_tb.cod_produto"
    adSQL sSQL, "  AND rm.CD_MDLD_BB = proposta_processo_susep_tb.cod_modalidade"
    adSQL sSQL, "  AND rm.CD_ITEM_MDLD = proposta_processo_susep_tb.cod_item_modalidade"
    adSQL sSQL, "  AND rm.DT_INC_VGC_RMNC <= f.dt_contratacao"
    adSQL sSQL, "  AND ( rm.DT_FIM_VGC_RMNC >= f.dt_contratacao OR rm.DT_FIM_VGC_RMNC IS NULL )"
    '-------------------------------------------------------------

    adSQL sSQL, " WHERE a.arquivo_retorno_bb IS NULL"
    'romulo.ribeiro - 01/04/2010
    adSQL sSQL, "   AND upper(f.situacao) <> 'T'"
    'emaior - 12/11/2007
    adSQL sSQL, "   AND b.voucher_id IS NOT NULL"
    'emaior - 25/07/2007 - Restitui��es que n�o tenham retornado para o BB
    adSQL sSQL, "   AND a.data_retorno_bb IS NULL "

    adSQL sSQL, "   AND (f.produto_id in(12,121,135,136,716))"
    adSQL sSQL, "   AND a.canc_endosso_id IS NULL"
    adSQL sSQL, "   AND ((e.tp_endosso_id = 63"
    adSQL sSQL, "   AND   i.proposta_id IS NOT NULL)"
    adSQL sSQL, "    OR  e.tp_endosso_id = 70"
    adSQL sSQL, "    OR  e.tp_endosso_id = 80"
    adSQL sSQL, "    OR  e.tp_endosso_id = 81"    'flavio.abreu - 20121016
    'arodrigues 26/06/2006 - Liberando endosso 101
    adSQL sSQL, "    OR  e.tp_endosso_id = 101"
    adSQL sSQL, "    OR  e.tp_endosso_id = 104"
    'bcarneiro - 14/11/2006 - Restitui��o Agr�cola
    adSQL sSQL, "    OR (e.tp_endosso_id = 64 "    'AND proposta_subvencao_tb.proposta_id IS NOT NULL)
    '20/09/2016 - Schoralick (ntendencia) - 18234489 - Novos produtos - Fix (realizar restitui��o pelo endosso 64 para os novos produtos 1235,1236,1237)
    adSQL sSQL, "        AND ( proposta_subvencao_tb.proposta_id IS NOT NULL OR f.produto_id IN (1235,1236,1237)) ) "
    adSQL sSQL, "    OR  e.tp_endosso_id = 269"
    'bmoraes - 16/09/2008 - Bonifica��o Agr�cola
    adSQL sSQL, "    OR  e.tp_endosso_id = 270"

    'Rafael Oshiro 06/01/2005 - Liberar endosso tipo 10
    adSQL sSQL, "    OR  e.tp_endosso_id = 10"
    'Schoralick - 23/01/2017 - ntendencia - 18234489 - Novos produtos (novos endossos de restitui��o)
    adSQL sSQL, "    OR e.tp_endosso_id IN (37,350,357,28,30) )"
    'Rafael Oshiro 07/01/2005 - N�o enviar proposta_bb nula
    adSQL sSQL, "   AND isnull(g.proposta_bb, g2.proposta_bb) is not null"

    adSQL sSQL, "    UNION"

    adSQL sSQL, "SELECT distinct max(a.movimentacao_id) movimentacao_id, --'999999999' as movimentacao_id "
    adSQL sSQL, "     f.produto_id,"
    adSQL sSQL, "     b.voucher_id,"
    adSQL sSQL, "     h.cod_produto_bb,"
    adSQL sSQL, "     isnull(g.proposta_bb, g2.proposta_bb) AS proposta_bb,"
    adSQL sSQL, "     e.num_endosso_bb,"
    adSQL sSQL, "     e.proposta_id,"
    adSQL sSQL, "     er.endosso_id,"
    adSQL sSQL, "      restituicao_integral = CASE WHEN ISNULL(j.prazo_restituicao_integral,0) = 0 "
    adSQL sSQL, "                                       AND ISNULL(prazo_restituicao_integral_renov,0) = 0 THEN 's'"
    adSQL sSQL, "                                  ELSE 'n'"
    adSQL sSQL, "                             END,"
    adSQL sSQL, "     er.val_financeiro, "
    adSQL sSQL, "     er.val_iof, "
    adSQL sSQL, "     (er.val_comissao - er.val_ir) val_corretagem, "
    adSQL sSQL, "     er.val_ir, "

    '  adSQL sSQL, "     0 val_ir_estipulante,"
    'pfiche - 21/02/2006 - Inclusao do campo val_ir_estipulante de endosso_resumo_tb
    adSQL sSQL, "     ISNULL(er.val_ir_estipulante,0) AS val_ir_estipulante, "

    adSQL sSQL, "     (er.val_comissao_estipulante - ISNULL(er.val_ir_estipulante,0)) val_estipulante, "
    adSQL sSQL, "     cast( ((er.val_financeiro   - --valor Financeiro"
    adSQL sSQL, "            er.val_iof          - --Valor IOF"
    adSQL sSQL, "            (er.val_comissao - er.val_ir) - --Corretagem Liquida"
    adSQL sSQL, "            (er.val_comissao_estipulante - ISNULL(er.val_ir_estipulante,0))) * -1) "
    adSQL sSQL, "         as numeric(15,2))   val_movimentacao,               "
    adSQL sSQL, "      0 custo_apolice, "
    adSQL sSQL, "      er.val_desconto_comercial, "
    adSQL sSQL, "      er.val_adic_fracionamento,"
    adSQL sSQL, "     e.dt_pedido_endosso AS dt_inicio_vigencia,"
    adSQL sSQL, "     ISNULL(g3.ramo_id, g.ramo_id) AS ramo_id, "
    adSQL sSQL, "     ISNULL(ISNULL(eds_ctr_sgro.NR_SLCT_EDS_SGRA,er.endosso_resumo_id),0) AS endosso_resumo_id, "
    adSQL sSQL, "     ISNULL(proposta_processo_susep_tb.cod_produto, 0) as cd_prd, "
    adSQL sSQL, "     ISNULL(proposta_processo_susep_tb.cod_modalidade, 0) as cd_mdld, "
    adSQL sSQL, "     ISNULL(proposta_processo_susep_tb.cod_item_modalidade, 0) as cd_item_mdld, "

    'bmoraes 21.01.2009
    adSQL sSQL, "   indicador_remuneracao = CASE WHEN ISNULL(rm.CD_RMNC_SGRO, 0) > 0  THEN 'S' ELSE 'N' END,  "
    adSQL sSQL, "   codigo_remuneracao = ISNULL(rm.CD_RMNC_SGRO, 0 ), "
    adSQL sSQL, "   tipo_remuneracao = ISNULL(rm.CD_TIP_RMNC_SGRO, 0 ), "
    adSQL sSQL, "   pessoa_remuneracao = 0, "
    adSQL sSQL, "   val_remuneracao = ISNULL(d.val_remuneracao, 0 ), "
    adSQL sSQL, "   val_ir_remuneracao = ISNULL(d.val_ir_remuneracao, 0 ) "
    '------------------------------------------------------------

    adSQL sSQL, "    FROM ps_mov_cliente_tb a WITH (NOLOCK)"
    adSQL sSQL, "    JOIN ps_acerto_pagamento_tb b WITH (NOLOCK)"
    adSQL sSQL, "      ON a.acerto_id = b.acerto_id"
    'emaior - 25/07/2007 - Restitui��es que tiverem pagamento aprovado
    'adSQL sSQL, "     AND b.situacao = 'a'"
    'emaior - 12/11/2007
    adSQL sSQL, "     AND b.situacao = 'e'"

    adSQL sSQL, "    JOIN ps_mov_endosso_financeiro_tb c WITH (NOLOCK)"
    adSQL sSQL, "      ON a.movimentacao_id = c.movimentacao_id"
    adSQL sSQL, "    JOIN endosso_financeiro_tb d WITH (NOLOCK)"
    adSQL sSQL, "      ON c.proposta_id = d.proposta_id"
    adSQL sSQL, "     AND c.endosso_id = d.endosso_id"
    adSQL sSQL, "    JOIN endosso_resumo_tb er WITH (NOLOCK)"
    adSQL sSQL, "      ON er.endosso_resumo_id = d.endosso_resumo_id"
    adSQL sSQL, "    JOIN endosso_tb e WITH (NOLOCK)"
    adSQL sSQL, "      ON er.proposta_id = e.proposta_id"
    adSQL sSQL, "     AND er.endosso_id  = e.endosso_id"
    adSQL sSQL, "    JOIN proposta_tb f WITH (NOLOCK)"
    adSQL sSQL, "      ON e.proposta_id = f.proposta_id"
    adSQL sSQL, "    LEFT JOIN proposta_adesao_tb g WITH (NOLOCK)"
    adSQL sSQL, "      ON f.proposta_id = g.proposta_id"
    adSQL sSQL, "    LEFT JOIN proposta_fechada_tb g2 WITH (NOLOCK)"
    adSQL sSQL, "      ON f.proposta_id = g2.proposta_id"
    adSQL sSQL, "    LEFT JOIN apolice_tb g3 WITH (NOLOCK)"
    adSQL sSQL, "      ON f.proposta_id = g3.proposta_id"
    adSQL sSQL, "    JOIN item_produto_tb h WITH (NOLOCK)"
    adSQL sSQL, "      ON f.produto_id = h.produto_id"
    adSQL sSQL, "     AND ISNULL(g.ramo_id, g3.ramo_id) = h.ramo_id"
    adSQL sSQL, "    JOIN produto_tb j WITH (NOLOCK)"
    adSQL sSQL, "      ON f.produto_id = j.produto_id"
    adSQL sSQL, "    LEFT JOIN proposta_processo_susep_tb WITH (NOLOCK)"
    adSQL sSQL, "      ON proposta_processo_susep_tb.proposta_id = f.proposta_id"
    adSQL sSQL, "    LEFT JOIN als_operacao_db..eds_ctr_sgro eds_ctr_sgro WITH (NOLOCK)"
    adSQL sSQL, "      ON eds_ctr_sgro.cd_prd = proposta_processo_susep_tb.cod_produto"
    adSQL sSQL, "     AND eds_ctr_sgro.cd_mdld = proposta_processo_susep_tb.cod_modalidade"
    adSQL sSQL, "     AND eds_ctr_sgro.cd_item_mdld = proposta_processo_susep_tb.cod_item_modalidade"
    adSQL sSQL, "     AND eds_ctr_sgro.nr_ctr_sgro = proposta_processo_susep_tb.num_contrato_seguro"
    adSQL sSQL, "     AND CONVERT(VARCHAR(6),eds_ctr_sgro.DT_CPTC_EDS,112) = er.ano_mes_devolucao"

    'bmoraes 21.01.2009
    adSQL sSQL, "  LEFT JOIN als_produto_db..RMNC_ITEM_MDLD_HIST_TB rm WITH (NOLOCK)"
    adSQL sSQL, "  ON rm.CD_PRD_BB = proposta_processo_susep_tb.cod_produto"
    adSQL sSQL, "  AND rm.CD_MDLD_BB = proposta_processo_susep_tb.cod_modalidade"
    adSQL sSQL, "  AND rm.CD_ITEM_MDLD = proposta_processo_susep_tb.cod_item_modalidade"
    adSQL sSQL, "  AND rm.DT_INC_VGC_RMNC <= f.dt_contratacao"
    adSQL sSQL, "  AND ( rm.DT_FIM_VGC_RMNC >= f.dt_contratacao OR rm.DT_FIM_VGC_RMNC IS NULL )"
    '-------------------------------------------------------------

    adSQL sSQL, "   WHERE a.arquivo_retorno_bb IS NULL"
    'romulo.ribeiro - 01/04/2010
    adSQL sSQL, "     AND upper(f.situacao) <> 'T'"
    'emaior - 12/11/2007
    adSQL sSQL, "     AND b.voucher_id IS NOT NULL"
    'emaior - 25/07/2007 - Restitui��es que n�o tenham retornado para o BB
    adSQL sSQL, "     AND a.data_retorno_bb IS NULL "

    adSQL sSQL, "     AND (f.produto_id in(12,121,135,136,716))"
    adSQL sSQL, "     AND a.canc_endosso_id IS NULL"
    adSQL sSQL, "     AND e.tp_endosso_id = 200 "
    '  adSQL sSQL, "     AND j.produto_id = " & iProdutoContaCartao
    adSQL sSQL, "GROUP BY f.produto_id,"
    adSQL sSQL, "         b.voucher_id,"
    adSQL sSQL, "         h.cod_produto_bb,"
    adSQL sSQL, "         isnull(g.proposta_bb, g2.proposta_bb),"
    adSQL sSQL, "         e.num_endosso_bb,"
    adSQL sSQL, "         e.proposta_id,"
    adSQL sSQL, "         er.endosso_id,"
    adSQL sSQL, "         e.dt_pedido_endosso,"
    adSQL sSQL, "         ISNULL(g3.ramo_id, g.ramo_id),"
    adSQL sSQL, "         er.val_financeiro,"
    adSQL sSQL, "         er.val_iof, "
    adSQL sSQL, "         er.val_comissao, "
    adSQL sSQL, "         er.val_ir, "
    adSQL sSQL, "         er.val_ir_estipulante, "
    adSQL sSQL, "         er.val_comissao_estipulante, "
    adSQL sSQL, "         er.val_desconto_comercial, "
    adSQL sSQL, "         er.val_adic_fracionamento, "
    adSQL sSQL, "         er.endosso_resumo_id , "
    adSQL sSQL, "         cod_produto, "
    adSQL sSQL, "         cod_modalidade, "
    adSQL sSQL, "         cod_item_modalidade,"
    adSQL sSQL, "         eds_ctr_sgro.NR_SLCT_EDS_SGRA,"
    adSQL sSQL, "         j.prazo_restituicao_integral,"
    adSQL sSQL, "         j.prazo_restituicao_integral_renov,"

    'bmoraes 21.01.2009
    adSQL sSQL, "         rm.CD_RMNC_SGRO,"
    adSQL sSQL, "         rm.CD_TIP_RMNC_SGRO,"
    adSQL sSQL, "         d.val_remuneracao,"
    adSQL sSQL, "         d.val_ir_remuneracao"
    '----------------------------------------------------------


    Set rc1 = rdocn1.OpenResultset(sSQL, rdOpenStatic)

    If rc1.EOF Then
        Call MensagemBatch("Nenhum registro foi localizado.")
        rc1.Close
        Exit Sub
    End If

    rdocn.BeginTrans

    Conta_linha = 0
    tot_val_movimentacao = 0

    While Not rc1.EOF

        DoEvents

        If Conta_linha = 0 Then
            Call Abre_Arquivo
        End If

        Conta_linha = Conta_linha + 1

        'pmelo 20110622 - Tratamento 987 para acerto dos valores do Estipulante
        lAdmClienteId = Obter_Estipulante(rc1!proposta_id)

        'WBARBOZA - 29/01/2013 - Ajuste no tratamento de isen��o de IR para os produtos 1174,1175 - INC 3801751 - Tratamento 987
        sFlgIsencaoIR = Obter_Flg_Isencao(lAdmClienteId, rc1!produto_id)

        'Barney - 03/05/2004
        EndossoResumoID = Val(rc1!endosso_resumo_id)

        movimentacao_id = rc1!movimentacao_id

        Produto = Format(rc1!produto_id, "000")

        'Atualmente os produtos podem ter mais de 3 posi��es.  Para n�o criar problemas
        'no banco, retornaremos com os �ltimos 3 chars do produto at� que o layout seja atualizado
        'Set/2005 - Confitec - Jailson
        Produto = Mid(Produto, Len(Produto) - 2, 3)

        'FLOW 240193 - RNC 1497-01 - MFerreira - Confitec Sistemas - 2007-10-01
        'Voucher sendo gerado com campo vazio, impactando na modifica��o do Layout.
        '--------------------------------------------------------------------------
        'voucher = Format(rc1!voucher_id, "000000")

        'PMELO 20120214 - Tratamento 987
        'voucher = Format(IIf(IsNull(rc1!voucher_id), 0, rc1!voucher_id), "000000")
        voucher = Right(Format(IIf(IsNull(rc1!voucher_id), 0, rc1!voucher_id), "000000"), 6)


        CodProdutoBB = Format(rc1!cod_produto_bb, "0000000")
        Proposta_bb = Format(IIf(IsNull(rc1!Proposta_bb), "0", rc1!Proposta_bb), "000000000")
        'Vnavarro inclus�o de tr�s novos campos no arquivo de devolu��o para ALS
        CdProduto = Format(rc1!cd_prd, "0000")
        CdMdld = Format(rc1!cd_mdld, "0000")
        CdItemMdld = Format(rc1!cd_item_mdld, "000000000")

        'Tratamento para cod_produto_bb = 8229151 e 8229152 - arodrigues - 09/06/2003
        If (CodProdutoBB = 8229151) Or (CodProdutoBB = 8229152) Or (CodProdutoBB = 8223150) Then
            CodProdutoBB = 8229150
        End If

        If Val(Produto) = "14" And Val(Proposta_bb) > 9900000 Then
            CodProdutoBB = "0678116"
        End If

        '* Busca o Tipo Pessoa Estipulante para calcular IR
        perc_ir_estip = Obtem_Perc_IR_Estip(rc1!proposta_id, rc1!endosso_id, rc1!ramo_id, rc1!dt_inicio_vigencia)

        num_endosso_bb = Format(IIf(IsNull(rc1!num_endosso_bb), "0", rc1!num_endosso_bb), "000000000")

        If Val(rc1!endosso_resumo_id) = 0 Then
            endosso_id = Format(IIf(IsNull(rc1!endosso_id), "0", rc1!endosso_id), "000000000")
        Else
            'Se for Conta Cart�o, exibe o endosso_Resumo_id como endosso_id
            endosso_id = Format(IIf(IsNull(rc1!endosso_resumo_id), "0", rc1!endosso_resumo_id), "000000000")

            'bcarneiro - 10/01/2007 - Restitui��o Agr�cola
            'Esse tratamento passa a ser feito para todos os produtos do ALS
            'CodProdutoBB = "0000000"
            '''''''''''''''''''''''''''''''''''''''''''''''
        End If

        'bcarneiro - 10/01/2007 - Restitui��o Agr�cola
        'Se for um produto do ALS, n�o devemos enviar c�digo do produto BB
        If Trim(CdProduto) <> "0000" Then
            CodProdutoBB = "0000000"
        End If
        '''''''''''''''''''''''''''''''''''''''''''''''

        restituicao_integral = IIf(UCase(rc1!restituicao_integral) = "S", "T", "P")
        data_devolucao = Format(Data_Sistema, "ddmmyyyy")

        'Barney - 05/02/2004 - In�cio
        'Implementa��o para o Conta Cart�o
        'val_financeiro = Format((Val(rc1!val_financeiro) * -1) * 100, "000000000000000")
        'val_iof = Format((Val(rc1!val_iof) * -1) * 100, "000000000000000")
        'val_corretagem = Format((Val(rc1!val_corretagem) * -1) * 100, "000000000000000")
        'val_ir = Format((Val(rc1!val_ir) * -1) * 100, "000000000000000")
        'val_ir_estipulante = Format(CCur(Val(rc1!val_Estipulante) * perc_ir_estip * -1) * 100, "000000000000000")
        'val_Estipulante = Format(CCur((Val(rc1!val_Estipulante) * (1 - perc_ir_estip) * -1)) * 100, "000000000000000")
        'val_movimentacao = Format((Val(rc1!val_movimentacao) * 100), "000000000000000")
        'custo_apolice = Format((Val(rc1!custo_apolice) * 100) * -1, "000000000000000")
        'val_desconto_comercial = Format((Val(rc1!val_desconto_comercial) * 100) * -1, "000000000000000")
        'val_adic_fracionamento = Format((Val(rc1!val_adic_fracionamento) * 100) * -1, "000000000000000")

        '    If Produto <> iProdutoContaCartao Then
        If Val(rc1!endosso_resumo_id) = 0 Then
            If Produto = 424 Then
                paridade = ObterParidade(rc1!proposta_id, rc1!endosso_id)
                val_financeiro = Format((TruncaDecimal(Val(rc1!val_financeiro) * paridade) * -1) * 100, "000000000000000")
                val_iof = Format((TruncaDecimal(Val(rc1!val_iof) * paridade) * -1) * 100, "000000000000000")
                val_corretagem = Format((TruncaDecimal(Val(rc1!val_corretagem) * paridade) * -1) * 100, "000000000000000")
                val_ir = Format((TruncaDecimal(Val(rc1!val_ir) * paridade) * -1) * 100, "000000000000000")
                val_ir_estipulante = Format(CCur(TruncaDecimal(Val(rc1!val_Estipulante) * perc_ir_estip * -1) * paridade) * 100, "000000000000000")
                val_Estipulante = Format(CCur(TruncaDecimal(Val(rc1!val_Estipulante) * (1 - perc_ir_estip) * -1) * paridade) * 100, "000000000000000")

                'vbarbosa - 30/11/2006 - Comentado pois o valor da movimenta��o j� est� covertido para a moeda nacional.
                '            val_movimentacao = Format((TruncaDecimal(Val(rc1!val_movimentacao) * paridade) * 100), "000000000000000")
                val_movimentacao = Format((TruncaDecimal(Val(rc1!val_movimentacao)) * 100), "000000000000000")

                custo_apolice = Format((TruncaDecimal(Val(rc1!custo_apolice) * paridade) * 100) * -1, "000000000000000")
                val_desconto_comercial = Format((TruncaDecimal(Val(rc1!val_desconto_comercial) * paridade) * 100) * -1, "000000000000000")
                val_adic_fracionamento = Format((TruncaDecimal(Val(rc1!val_adic_fracionamento) * paridade) * 100) * -1, "000000000000000")
            Else
                val_financeiro = Format((Val(rc1!val_financeiro) * -1) * 100, "000000000000000")
                val_iof = Format((Val(rc1!val_iof) * -1) * 100, "000000000000000")
                'Rafael Oshiro - stefanini - 27/12/2005 capturar o valor absoluto da corretagem
                'val_corretagem = Format((Val(rc1!val_corretagem) * -1) * 100, "000000000000000")
                val_corretagem = Format(Abs(Val(rc1!val_corretagem)) * 100, "000000000000000")
                val_ir = Format((Val(rc1!val_ir) * -1) * 100, "000000000000000")

                'PMELO 20111207 - Demanda estipulante com isencao de IR
                ''pmelo 20110524 - Tratamento 987 para acerto dos valores do Estipulante - FENAB n�o calcula com IR
                'If ((lAdmClienteId = 192191) Or (lAdmClienteId = 2452951)) And ((rc1!produto_id = 8) Or (rc1!produto_id = 12) Or (rc1!produto_id = 15) Or (rc1!produto_id = 109) Or (rc1!produto_id = 111) Or (rc1!produto_id = 121) Or (rc1!produto_id = 135) Or (rc1!produto_id = 136) Or (rc1!produto_id = 716) Or (rc1!produto_id = 721)) Then
                If sFlgIsencaoIR = "S" Then

                    'PMELO 20111207 - Demanda estipulante com isen��o de IR
                    'val_ir_estipulante = "000000000000000" 'Format(CCur(Val(rc1!val_Estipulante) * perc_ir_estip * -1) * 100, "000000000000000")
                    val_ir_estipulante = Format((Val(rc1!val_ir_estipulante) * -1) * 100, "000000000000000")
                    val_Estipulante = Format(CCur((Val(rc1!val_Estipulante) * (1 - 0) * -1)) * 100, "000000000000000")

                Else

                    'PMELO 20111207 - Demanda estipulante com isen��o de IR
                    'val_ir_estipulante = Format(CCur(Val(rc1!val_Estipulante) * perc_ir_estip * -1) * 100, "000000000000000")
                    val_ir_estipulante = Format((Val(rc1!val_ir_estipulante) * -1) * 100, "000000000000000")
                    val_Estipulante = Format(CCur((Val(rc1!val_Estipulante) * (1 - perc_ir_estip) * -1)) * 100, "000000000000000")

                End If

                val_movimentacao = Format((Val(rc1!val_movimentacao) * 100), "000000000000000")
                custo_apolice = Format((Val(rc1!custo_apolice) * 100) * -1, "000000000000000")
                val_desconto_comercial = Format((Val(rc1!val_desconto_comercial) * 100) * -1, "000000000000000")
                val_adic_fracionamento = Format((Val(rc1!val_adic_fracionamento) * 100) * -1, "000000000000000")

            End If
        Else
            val_financeiro = Format((Val(rc1!val_financeiro) * -1) * 100, "000000000000000")
            val_iof = Format((Val(rc1!val_iof) * -1) * 100, "000000000000000")
            val_corretagem = Format((Val(rc1!val_corretagem) * -1) * 100, "000000000000000")
            val_ir = Format((Val(rc1!val_ir) * -1) * 100, "000000000000000")

            'pfiche - 21/02/2006
            val_ir_estipulante = Format((Val(rc1!val_ir_estipulante) * -1) * 100, "000000000000000")
            'val_ir_estipulante = Format(0, "000000000000000")

            val_Estipulante = Format((Val(rc1!val_Estipulante) * -1) * 100, "000000000000000")
            val_movimentacao = Format((Val(rc1!val_movimentacao) * 100), "000000000000000")
            custo_apolice = Format((Val(rc1!custo_apolice) * 100) * -1, "000000000000000")
            val_desconto_comercial = Format((Val(rc1!val_desconto_comercial) * 100) * -1, "000000000000000")
            val_adic_fracionamento = Format((Val(rc1!val_adic_fracionamento) * 100) * -1, "000000000000000")

        End If

        'bmoraes 21.01.2009
        indicador_remuneracao = rc1!indicador_remuneracao
        codigo_remuneracao = Format(rc1!codigo_remuneracao, "0000")
        tipo_remuneracao = Format(rc1!tipo_remuneracao, "0000")
        pessoa_remuneracao = Format(rc1!pessoa_remuneracao, "000000000")
        val_remuneracao = Format((Val(rc1!val_remuneracao) * 100) * -1, "000000000000000")
        val_ir_remuneracao = Format((Val(rc1!val_ir_remuneracao) * 100) * -1, "000000000000000")
        '----------------------------------------------------------------------

        'Barney - 05/02/2004 - Fim

        'FLOW 214117 - Retirada a critica para o produto 424 - Rmarins - 05/05/07
        'If Produto = 424 Then
        '    tot_val_movimentacao = TruncaDecimal(tot_val_movimentacao + (Val(rc1!val_movimentacao) * paridade))
        'Else
        tot_val_movimentacao = tot_val_movimentacao + Val(rc1!val_movimentacao)
        'End If

        Linha = Format(Conta_linha, "000000") & _
                "01" & _
                Produto & _
                voucher & _
                CodProdutoBB & _
                Proposta_bb & _
                num_endosso_bb & _
                endosso_id & _
                restituicao_integral & _
                data_devolucao & _
                val_financeiro & _
                val_iof & _
                val_corretagem & _
                val_ir & _
                val_Estipulante & _
                val_ir_estipulante & _
                val_movimentacao & _
                custo_apolice & _
                val_desconto_comercial & _
                val_adic_fracionamento & _
                CdProduto & _
                CdMdld & _
                CdItemMdld & _
                Replace(indicador_remuneracao, "-", "") & Replace(codigo_remuneracao, "-", "") & Replace(tipo_remuneracao, "-", "") & Replace(pessoa_remuneracao, "-", "") & Replace(val_remuneracao, "-", "") & Replace(val_ir_remuneracao, "-", "") & _
                "00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000"
        'bmoraes 21.01.2009 - adi��o dos valores de remunera��o-----------

        'Vnavarro - Inclus�o de tr�s novos campos do produto ALS para 17/02/2005

        Linha = Left(Linha & Space(tam_reg), tam_reg)

        Print #1, Linha

        'Barney - 03/05/2004
        'Altera��o para atualizar todos os movimentacoes que forem do Conta-Cart�o
        'If movimentacao_id = ENDOSSO_RESUMO Then
        '  Call AtualizarPsMovClienteEndossoResumo(Val(endosso_id))
        'Else
        '  Call Atualiza_Ps_Mov_Cliente(movimentacao_id)
        'End If
        Call Atualiza_Ps_Mov_Cliente(movimentacao_id, EndossoResumoID)

        TxtRegsGerados.Text = Str(Conta_linha)
        TxtRegsGerados.Refresh

        'carsilva - 17/08/2004
        OrigemEndosso = Origem_Endosso(rc1!proposta_id, rc1!endosso_id)
        Call evento_retorno(Data_Sistema, Produto, rc1!Proposta_bb, 4, 1533, NumRemessa, 422, rc1!proposta_id, rc1!endosso_id, OrigemEndosso, rc1!num_endosso_bb)

        sSQL = ""
        sSQL = sSQL & "SET NOCOUNT ON EXEC SEGS11299_SPI"
        sSQL = sSQL & " 'SEGP0422'"
        sSQL = sSQL & ", " & rc1!proposta_id
        If rc1!endosso_id <> 0 Then
            sSQL = sSQL & "," & rc1!endosso_id
        Else
            sSQL = sSQL & ",NULL"
        End If
        sSQL = sSQL & ", " & TrocaVirgulaPorPonto(rc1!val_movimentacao)
        sSQL = sSQL & ",'Sistema Autom�tico'"
        sSQL = sSQL & ",'Processamento do arquivo BBC452 ao Banco.'"
        sSQL = sSQL & ",'Gera Arquivo'"
        sSQL = sSQL & ", NULL"
        sSQL = sSQL & ",'BBC452." & Right("00000" & NumRemessa, 4) & "'"

        rdocn.Execute sSQL


        rc1.MoveNext

    Wend

    rc1.Close

    Call Finaliza_arquivo

    rdocn.CommitTrans

    Exit Sub
    Resume
Erro:
    Call TrataErroGeral("Processa", Me.name)
    Call TerminaSEGBR

End Sub



Private Sub InicializaCargaDados_CriacaoTemporario()
    Dim StrSQL As String
    Dim rsTemp As rdoResultset

    adSQL StrSQL, ""
    '' GENESCO SILVA - FLOW 18313521 - MIGRA��O SQL SERVER
    '    adSQL StrSQL, "if isnull(OBJECT_ID('tempdb..#Temp_PreRemessa'),0) >0"
    adSQL StrSQL, "if OBJECT_ID('tempdb..#Temp_PreRemessa') IS NOT NULL "
    adSQL StrSQL, "BEGIN"
    adSQL StrSQL, "     DROP TABLE #Temp_PreRemessa"
    adSQL StrSQL, "END"
    adSQL StrSQL, "CREATE TABLE #Temp_PreRemessa"
    adSQL StrSQL, "     ( Movimentacao_ID       Int"
    adSQL StrSQL, "     )"

    rdocn.Execute (StrSQL)

End Sub

Private Sub EmailRemessaSEG452(Remessa As String, Optional ByVal dValorRemessa As Double)

    On Error GoTo TrataErro

    Dim vTemp As String
    Dim lOrigem As String
    Dim sSQL As String

    'lOrigem = LerArquivoIni("ARQUIVOS DEVOLUCAO PREMIO", "SEG452_PATH")
    'vTemp = LerArquivoIni("ARQUIVOS DEVOLUCAO PREMIO", "SAIDA_Liberados")

    'lOrigem = "D:\Projetos\AliancaBrasil\SEGBR\Fontes\14413025 - ALTERAR O PROCESSO DE ENVIO DO ARQUIVO SEG452\SEGP0422_Vs78_PRODUCAO_ALTERADO_FINAL\Gerados"
    'vTemp = "D:\Projetos\AliancaBrasil\SEGBR\Fontes\14413025 - ALTERAR O PROCESSO DE ENVIO DO ARQUIVO SEG452\SEGP0422_Vs78_PRODUCAO_ALTERADO_FINAL\Liberados\"

    'FileCopy lOrigem & "\" & Remessa, vTemp & Remessa
    'Kill lOrigem & "\" & Remessa

    Call rdocn.Execute("EXEC SEGS10923_SPU '" & Remessa & "'")

    StatusBar1.SimpleText = "Enviando E-Mail para produ��o..."
    If Endosso_Contestacao Then
        'Email para a opera��o
        sSQL = "exec seguros_db.dbo.envia_email_sp " & "'cxproducao@aliancadobrasil.com.br', " & vbCrLf & _
               "'Libera��o do arquivo: " & Remessa & "', " & vbCrLf & _
               "'Arquivo de Pagamentos Autorizados esta liberado para ser enviado ao Banco do Brasil." & vbCrLf & _
               "Localiza��o do arquivo: \Arquivos Devolucao Premio\Liberados" & vbCrLf & _
               "Mensagem enviada automaticamente pelo sistema.'"
        Call rdocn.Execute(sSQL)
        'Email para o financeiro
        sSQL = "exec seguros_db.dbo.envia_email_sp " & "'GrupoCDC@aliancadobrasil.com.br', " & vbCrLf & _
               "'Libera��o do arquivo: " & Remessa & "', " & vbCrLf & _
               "'Arquivo de Pagamentos Autorizados esta liberado para ser enviado ao Banco do Brasil." & vbCrLf & _
               "O arquivo foi gerado com sucesso e j� est� dispon�vel para envio pela opera��o." & vbCrLf & _
               "Um email foi enviado � opera��o para conhecimento." & vbCrLf & _
               "O valor total do arquivo foi de R$ " & FormataValorParaPadraoBrasil(CStr(dValorRemessa)) & vbCrLf & _
               "Mensagem enviada automaticamente pelo sistema.'"
        Call rdocn.Execute(sSQL)
    Else
        sSQL = "exec seguros_db.dbo.envia_email_sp " & "'cxproducao@aliancadobrasil.com.br', " & vbCrLf & _
               "'Libera��o do arquivo: " & Remessa & "', " & vbCrLf & _
               "'Arquivo de Pagamentos Autorizados esta liberado para ser enviado ao Banco do Brasil." & vbCrLf & _
               "Localiza��o do arquivo: \Arquivos Devolucao Premio\Liberados" & vbCrLf & _
               "Mensagem enviada automaticamente pelo sistema.'"
        Call rdocn.Execute(sSQL)
    End If



    Exit Sub

TrataErro:
    TrataErroGeral "Rotina : Libera��o - " & Err.Description, "SEGP0445"
    TerminaSEGBR
    Resume
End Sub

Public Function ValidaArquivo(RsRemessa As rdoResultset) As Boolean    'FLAVIO.BEZERRA - 14413025 - ALTERAR O PROCESSO DE ENVIO DO ARQUIVO SEG452 - 18/06/2013

    Dim sSQL As String
    Dim RsPreRemessa As rdoResultset
    Dim dblTotalRemessa As Double

    sSQL = ""
    sSQL = sSQL & "SELECT Count(a.movimentacao_id)                     qtd_movimentacao                     " & vbNewLine
    sSQL = sSQL & "     , sum(a.val_movimentacao)                      Total                                " & vbNewLine
    sSQL = sSQL & "  FROM ps_mov_cliente_tb                            a WITH (NOLOCK)                           " & vbNewLine
    sSQL = sSQL & "  JOIN ps_acerto_pagamento_tb                       b WITH (NOLOCK)                           " & vbNewLine
    sSQL = sSQL & "    ON a.acerto_id                                  = b.acerto_id                        " & vbNewLine
    sSQL = sSQL & "   AND b.situacao                                   = 'e'                                " & vbNewLine
    sSQL = sSQL & "  JOIN ps_mov_endosso_financeiro_tb                 c WITH (NOLOCK)                           " & vbNewLine
    sSQL = sSQL & "    ON a.movimentacao_id                            = c.movimentacao_id                  " & vbNewLine
    sSQL = sSQL & "  JOIN endosso_financeiro_tb                        d WITH (NOLOCK)                           " & vbNewLine
    sSQL = sSQL & "    ON c.proposta_id                                = d.proposta_id                      " & vbNewLine
    sSQL = sSQL & "   AND c.endosso_id                                 = d.endosso_id                       " & vbNewLine
    sSQL = sSQL & "  JOIN endosso_tb                                   e WITH (NOLOCK)                           " & vbNewLine
    sSQL = sSQL & "    ON d.proposta_id                                = e.proposta_id                      " & vbNewLine
    sSQL = sSQL & "   AND d.endosso_id                                 = e.endosso_id                       " & vbNewLine
    sSQL = sSQL & "  JOIN proposta_tb                                  f WITH (NOLOCK)                           " & vbNewLine
    sSQL = sSQL & "    ON e.proposta_id                                = f.proposta_id                      " & vbNewLine
    sSQL = sSQL & "  LEFT JOIN proposta_adesao_tb                      g WITH (NOLOCK)                           " & vbNewLine
    sSQL = sSQL & "    ON f.proposta_id                                = g.proposta_id                      " & vbNewLine
    sSQL = sSQL & "  LEFT JOIN proposta_fechada_tb                     g2 WITH (NOLOCK)                          " & vbNewLine
    sSQL = sSQL & "    ON f.proposta_id                                = g2.proposta_id                     " & vbNewLine
    sSQL = sSQL & "  LEFT JOIN apolice_tb                              g3 WITH (NOLOCK)                          " & vbNewLine
    sSQL = sSQL & "    ON f.proposta_id                                = g3.proposta_id                     " & vbNewLine
    sSQL = sSQL & "  JOIN item_produto_tb                              h WITH (NOLOCK)                           " & vbNewLine
    sSQL = sSQL & "    ON f.produto_id                                 = h.produto_id                       " & vbNewLine
    sSQL = sSQL & "   AND ISNULL(g.ramo_id, g3.ramo_id)                = h.ramo_id                          " & vbNewLine
    sSQL = sSQL & "  LEFT JOIN avaliacao_restituicao_tb                i WITH (NOLOCK)                           " & vbNewLine
    sSQL = sSQL & "    ON e.proposta_id                                = i.proposta_id                      " & vbNewLine
    sSQL = sSQL & "   AND e.endosso_id                                 = i.endosso_id                       " & vbNewLine
    sSQL = sSQL & "   AND i.situacao                                   = 'a'                                " & vbNewLine
    sSQL = sSQL & "  JOIN produto_tb                                   j WITH (NOLOCK)                           " & vbNewLine
    sSQL = sSQL & "    ON f.produto_id                                 = j.produto_id                       " & vbNewLine
    sSQL = sSQL & "  LEFT JOIN proposta_processo_susep_tb              proposta_processo_susep_tb WITH (NOLOCK)   " & vbNewLine
    sSQL = sSQL & "    ON proposta_processo_susep_tb.proposta_id       = f.proposta_id                      " & vbNewLine
    sSQL = sSQL & "  LEFT JOIN proposta_subvencao_tb                   proposta_subvencao_tb WITH (NOLOCK)        " & vbNewLine
    sSQL = sSQL & "    ON proposta_subvencao_tb.proposta_id            = f.proposta_id                      " & vbNewLine
    sSQL = sSQL & "  LEFT JOIN als_produto_db..RMNC_ITEM_MDLD_HIST_TB  rm WITH (NOLOCK)                          " & vbNewLine
    sSQL = sSQL & "    ON rm.CD_PRD_BB                                 = proposta_processo_susep_tb.cod_produto" & vbNewLine
    sSQL = sSQL & "   AND rm.CD_MDLD_BB                                = proposta_processo_susep_tb.cod_modalidade" & vbNewLine
    sSQL = sSQL & "   AND rm.CD_ITEM_MDLD                              = proposta_processo_susep_tb.cod_item_modalidade" & vbNewLine
    sSQL = sSQL & "   AND rm.DT_INC_VGC_RMNC                           <= f.dt_contratacao                  " & vbNewLine
    sSQL = sSQL & "   AND ( rm.DT_FIM_VGC_RMNC                         >= f.dt_contratacao OR rm.DT_FIM_VGC_RMNC IS NULL )" & vbNewLine
    sSQL = sSQL & "  JOIN ps_Mov_Cliente_PreRemessa_Tb                 ps_Mov_Cliente_PreRemessa_Tb WITH (NOLOCK)" & vbNewLine
    sSQL = sSQL & "    ON ps_Mov_Cliente_PreRemessa_Tb.movimentacao_id = a.movimentacao_id                  " & vbNewLine
    sSQL = sSQL & " Where a.arquivo_retorno_bb Is Null                                                      " & vbNewLine
    sSQL = sSQL & "   AND upper(f.situacao)                            <> 'T'                               " & vbNewLine
    sSQL = sSQL & "   AND a.data_retorno_bb                            IS NULL                              " & vbNewLine
    sSQL = sSQL & "   AND j.processa_restituicao_automatica            = 's'                                " & vbNewLine
    sSQL = sSQL & "   AND (f.produto_id                                not in(12,121,135,136,716))          " & vbNewLine
    sSQL = sSQL & "   AND a.canc_endosso_id                            IS NULL                              " & vbNewLine
    sSQL = sSQL & "   AND ((e.tp_endosso_id                            = 63                                 " & vbNewLine
    sSQL = sSQL & "   AND i.proposta_id                                IS NOT NULL)                         " & vbNewLine
    sSQL = sSQL & "    OR e.tp_endosso_id                              = 70                                 " & vbNewLine
    sSQL = sSQL & "    OR e.tp_endosso_id                              = 80                                 " & vbNewLine
    sSQL = sSQL & "    OR e.tp_endosso_id                              = 81                                 " & vbNewLine
    sSQL = sSQL & "    OR e.tp_endosso_id                              = 101                                " & vbNewLine
    sSQL = sSQL & "    OR e.tp_endosso_id                              = 104                                " & vbNewLine
    sSQL = sSQL & "    OR (e.tp_endosso_id                             = 64                                 " & vbNewLine    ' AND proposta_subvencao_tb.proposta_id IS NOT NULL)
    '20/09/2016 - Schoralick (ntendencia) - 18234489 - Novos produtos - Fix (realizar restitui��o pelo endosso 64 para os novos produtos 1235,1236,1237)
    sSQL = sSQL & "        AND ( proposta_subvencao_tb.proposta_id IS NOT NULL OR f.produto_id IN (1235,1236,1237)) ) " & vbNewLine
    sSQL = sSQL & "    OR e.tp_endosso_id                              = 269                                " & vbNewLine
    sSQL = sSQL & "    OR e.tp_endosso_id                              = 270                                " & vbNewLine
    sSQL = sSQL & "    OR e.tp_endosso_id                              = 293                                " & vbNewLine
    sSQL = sSQL & "    OR e.tp_endosso_id                              = 297                                " & vbNewLine
    sSQL = sSQL & "    OR e.tp_endosso_id                              = 10                                 " & vbNewLine
    'genjunior - 19/12/2018 - Confitec SP - Melhorias Cooperativa - Novos Endossos de Subven��o
    sSQL = sSQL & "    OR e.tp_endosso_id                              in (361,362)                         " & vbNewLine
    'Schoralick - 23/01/2017 - ntendencia - 18234489 - Novos produtos (novos endossos de restitui��o)
    sSQL = sSQL & "    OR e.tp_endosso_id IN (37,350,357,28,30) )                                           " & vbNewLine
    sSQL = sSQL & "   AND isnull(g.proposta_bb, g2.proposta_bb)        is not null                          " & vbNewLine
    sSQL = sSQL & "   AND a.Remessa_Liberada                           = 'S'                                " & vbNewLine
    sSQL = sSQL & "   AND ps_Mov_Cliente_PreRemessa_Tb.lote_liberado   = 'S'                                " & vbNewLine
    sSQL = sSQL & "   AND a.acerto_id                                  is not null                          " & vbNewLine    'FLAVIO.BEZERRA - 28/06/2013

    Set RsPreRemessa = rdocn.OpenResultset(sSQL)

    If RsPreRemessa!qtd_movimentacao = RsRemessa.RowCount Then
        Do While Not RsRemessa.EOF
            dblTotalRemessa = dblTotalRemessa + Val(RsRemessa!val_movimentacao)
            RsRemessa.MoveNext
        Loop
        If Val(RsPreRemessa!Total) = dblTotalRemessa Then
            ValidaArquivo = True
            rc1.MoveFirst
            Exit Function
        End If
        ValidaArquivo = False
        rc1.MoveFirst
    End If

End Function

