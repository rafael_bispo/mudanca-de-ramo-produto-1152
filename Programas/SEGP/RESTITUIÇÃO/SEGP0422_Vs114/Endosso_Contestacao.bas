Attribute VB_Name = "Endosso_Contestacao"
Function Verifica_Endosso_Contestacao(sParametros As String) As Boolean
    Dim i As Integer
    If sParametros <> "" Then
        arrParm() = Split(SplitArgs(sParametros), Chr$(0))
        For i = 0 To UBound(arrParm)
            If arrParm(i) = "EDSCONTESTACAO" Then
                Verifica_Endosso_Contestacao = True
                Exit Function
            End If
        Next
    End If
End Function

Private Function SplitArgs(sParametros) As String
    Dim sArgs As String
    Dim sChar As String
    Dim nCount As Long
    Dim bQuotes As Boolean
    For nCount = 1 To Len(sParametros)
        sChar = Mid$(sParametros, nCount, 1)
        If sChar = Chr$(34) Then
            bQuotes = Not bQuotes
        End If
        If sChar = Chr$(32) Then
            If bQuotes Then
                sArgs = sArgs & sChar
            Else
                sArgs = sArgs & Chr$(0)
            End If
        Else
            If sChar <> Chr$(34) Then
                sArgs = sArgs & sChar
            End If
        End If
    Next
    SplitArgs = sArgs
End Function


