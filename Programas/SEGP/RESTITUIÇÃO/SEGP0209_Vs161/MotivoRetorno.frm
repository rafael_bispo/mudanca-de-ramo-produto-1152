VERSION 5.00
Begin VB.Form FrmMotivoRetorno 
   Caption         =   "Motivo Retorno Avalia��o T�cnica"
   ClientHeight    =   1830
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   7365
   ControlBox      =   0   'False
   Icon            =   "MotivoRetorno.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   1830
   ScaleWidth      =   7365
   StartUpPosition =   2  'CenterScreen
   Begin VB.ComboBox CmbMotivoRetorno 
      Height          =   315
      ItemData        =   "MotivoRetorno.frx":0442
      Left            =   45
      List            =   "MotivoRetorno.frx":0444
      Style           =   2  'Dropdown List
      TabIndex        =   3
      Top             =   45
      Visible         =   0   'False
      Width           =   7230
   End
   Begin VB.CommandButton BtnCancelar 
      Caption         =   "Cancelar"
      Height          =   405
      Left            =   5820
      TabIndex        =   2
      Top             =   1320
      Width           =   1455
   End
   Begin VB.CommandButton BtnOk 
      Caption         =   "Ok"
      Height          =   405
      Left            =   4290
      TabIndex        =   1
      Top             =   1320
      Width           =   1455
   End
   Begin VB.TextBox TxtMotivoRetorno 
      Height          =   1155
      Left            =   60
      MaxLength       =   255
      MultiLine       =   -1  'True
      TabIndex        =   0
      Top             =   60
      Width           =   7215
   End
End
Attribute VB_Name = "FrmMotivoRetorno"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub BtnCancelar_Click()

TxtMotivoRetorno.Text = ""
Me.Hide

End Sub

Private Sub BtnOk_Click()

Me.Hide
End Sub

Private Sub CmbMotivoRetorno_Click()
    If CmbMotivoRetorno.ListIndex = -1 Then
       btnOK.Enabled = False
    Else
       TxtMotivoRetorno.Text = CmbMotivoRetorno.Text
       btnOK.Enabled = True
    End If
End Sub

Sub Carregar_CmbMotivo_Vida()
    CmbMotivoRetorno.Clear
    
    CmbMotivoRetorno.AddItem "Restitui��o j� realizada"
    CmbMotivoRetorno.AddItem "Restitui��o Indevida"
    CmbMotivoRetorno.AddItem "Restitui��o com Valor Incorreto"
End Sub


Private Sub Form_Load()
btnOK.Enabled = False
    CmbMotivoRetorno.Clear

    If (Produto_id = 11) Or (Produto_id = 12) Or (Produto_id = 121) Or (Produto_id = 135) Or (Produto_id = 136) _
            Or (Produto_id = 716) Or (Produto_id = 721) Or (Produto_id = 1174) Or (Produto_id = 1175) _
            Or (Produto_id = 1177) Or (Produto_id = 1179) Or (Produto_id = 1180) Or (Produto_id = 1181) _
            Or (Produto_id = 1182) Or (Produto_id = 1196) Or (Produto_id = 1198) Or (Produto_id = 1205) _
            Or (Produto_id = 1208) Or (Produto_id = 1211) Or (Produto_id = 1217) Or (Produto_id = 1218) _
            Or (Produto_id = 1235) Or (Produto_id = 1236) Or (Produto_id = 1237) Then
            
        TxtMotivoRetorno.Visible = False
        CmbMotivoRetorno.Visible = True
        Carregar_CmbMotivo_Vida
    Else
        TxtMotivoRetorno.Visible = True
        CmbMotivoRetorno.Visible = False
    End If
End Sub

Private Sub TxtMotivoRetorno_Change()
If Trim(TxtMotivoRetorno.Text) = "" Then
   btnOK.Enabled = False
Else
   btnOK.Enabled = True
End If

End Sub
