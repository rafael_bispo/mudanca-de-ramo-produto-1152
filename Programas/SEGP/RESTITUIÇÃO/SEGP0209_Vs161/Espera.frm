VERSION 5.00
Begin VB.Form FrmEspera 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Aprova��o de Endosso de Cancelamento / Liquida��o"
   ClientHeight    =   1425
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   5580
   ControlBox      =   0   'False
   Icon            =   "Espera.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1425
   ScaleWidth      =   5580
   StartUpPosition =   2  'CenterScreen
   Begin VB.Label Label1 
      Caption         =   "Aguarde, carregando movimentos..."
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      Left            =   540
      TabIndex        =   0
      Top             =   420
      Width           =   4635
   End
End
Attribute VB_Name = "FrmEspera"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Form_Load()
    Call CentraFrm(Me)

           
    If TpOperacao = 1 Then
        Me.Caption = "SEGP0209 - Avalia��o T�cnica de Restitui��o - " & Ambiente
    Else
        Me.Caption = "SEGP0209 - Avalia��o Gerencial de Restitui��o - " & Ambiente
    End If
End Sub
