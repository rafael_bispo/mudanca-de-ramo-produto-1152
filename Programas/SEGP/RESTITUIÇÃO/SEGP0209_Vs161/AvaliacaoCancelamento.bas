Attribute VB_Name = "Local"
'Vari�vel de conex�o
Public RdoCnAux As New rdoConnection

'Vari�veis
Public LinhaGridSelEndossos As Long
Public TpOperacao As String
Public Emi_Id As String
Public Origem As String
Public Produto_id As String
Public bDtCancelamentoValida As Boolean
Public vgsituacao As String
Public existe_perc_restituicao As Boolean


Public restituicao_integral As String
Public gcValTotPremioPago As Currency
Public gcValTotJurosPago As Currency
Public gcValTotAdicFracionamentoPago As Currency
Public gcValTotDescontoPago As Currency
Public gcValTotIOFPago As Currency
Public gcValTotPremioTarifaPago As Currency
Public gcValCustoApoliceCertif As Currency

Public gsPossuiSubvencao As String     'Rmarins - 20/10/06


'rsilva 24/07/2009
'Proposta Modular
Public bModular As Boolean
Public propostaModular As Long
Public LinhaGridSelEndossosModular As Long
Public TpOperacaoModular As String
Public Emi_IdModular As String
Public OrigemModular As String
Public Produto_idModular As String
Public bDtCancelamentoValidaModular As Boolean
Public vgsituacaoModular As String
Public existe_perc_restituicaoModular As Boolean

Global oDadosModular As New modular

Public restituicao_integralModular As String
Public gcValTotPremioPagoModular As Currency
Public gcValTotJurosPagoModular As Currency
Public gcValTotAdicFracionamentoPagoModular As Currency
Public gcValTotDescontoPagoModular As Currency
Public gcValTotIOFPagoModular As Currency
Public gcValTotPremioTarifaPagoModular As Currency
Public gcValCustoApoliceCertifModular As Currency

Public gsPossuiSubvencaoModular As String
Public AvaliacaoGerencial As Boolean


'Constantes
Public Const ProdIdOvProdutorRural = 14
Public Const ProdIdRuralRapido = 16
Public Const ProdIdAgricola = 145
Public Const ProIdPenhorRural = 155

'Cole��es
Public Cancelamentos As New Collection
Public Cobrancas As New Collection
Public Corretagens As New Collection
Public Clientes As New Collection      'Rmarins - 20/10/06
Public Congeneres As New Collection    'Demanda 400422 - emaior 02/12/2008 - Relat�rio de Restitui��o de Comiss�o de Cosseguro

'Renato Vasconcelos
'Flow 11022639
'Inicio
Public comboCarregado As Boolean
Public endossoGerado As Boolean
Public adesaoFechadaGerado As Boolean
Public tpEndossoGerado As Boolean
Public infoBaixaGerado As Boolean
Public pagoRetidoGerado As Boolean
Public clienteGerado As Boolean
Public gerencial As Boolean
'Fim




Sub Atualizar_Bloqueio_Proposta(ByVal PProposta As String, ByVal PStatus As String)

Dim rc As rdoResultset
   
On Error GoTo Erro
   
SQL = "exec " & Ambiente & ".lock_proposta_spu  " & PProposta & ", '" & PStatus & "'"

Set rc = rdocn.OpenResultset(SQL)

rc.Close

Exit Sub

Erro:
   TrataErroGeral "Atualiza Bloqueio Proposta"
   
End Sub

''Sub Cancelar_Agendamentos(ByVal PProposta As String, ByVal PEndossoId As String, _
''                          Optional ByVal PEndossoCancelado As String)
''
''Dim SQL As String
''
''SQL = "exec cancela_cobranca_spi "
''SQL = SQL & PProposta
''SQL = SQL & "," & PEndossoId
''SQL = SQL & ",'" & cUserName & "'"
''
''If Val(Trim(PEndossoCancelado)) <> 0 Then
''    SQL = SQL & "," & PEndossoCancelado
''End If
''
''Call rdocn.OpenResultset(SQL)
''
''End Sub

Function Obter_Motivo_Retorno() As String

FrmMotivoRetorno.Show vbModal

Obter_Motivo_Retorno = FrmMotivoRetorno.TxtMotivoRetorno.Text

Unload FrmMotivoRetorno

End Function

Function Obter_Motivo_Recusa(Optional ByRef tMotivoRecusa As String) As Integer

FrmMotivoRecusa.Show vbModal

If FrmMotivoRecusa.CmbMotivoRecusa.ListIndex <> -1 Then
   Obter_Motivo_Recusa = FrmMotivoRecusa.CmbMotivoRecusa.ItemData(FrmMotivoRecusa.CmbMotivoRecusa.ListIndex)
   tMotivoRecusa = FrmMotivoRecusa.CmbMotivoRecusa.Text
Else
   Obter_Motivo_Recusa = 0
   tMotivoRecusa = ""
End If
Unload FrmMotivoRetorno

End Function

Function Obter_Proposta_Basica(PProduto) As String

Dim SQL As String
Dim Rst As rdoResultset

SQL = "Select val_parametro from ps_parametro_tb where parametro = 'BASICA " & PProduto & "'"

Set Rst = rdocn.OpenResultset(SQL)

If Not Rst.EOF Then
   Obter_Proposta_Basica = Rst!val_parametro
Else
   Obter_Proposta_Basica = ""
End If

End Function

''Function Obter_Num_Endosso(PProposta) As String
''
''Dim rc As rdoResultset
''
''On Error GoTo Erro
''
''SQL = "SELECT isnull(MAX(endosso_id),0) "
''SQL = SQL & " FROM endosso_tb "
''SQL = SQL & " WHERE proposta_id = " & PProposta
''
''Set rc = rdocn.OpenResultset(SQL)
''
''If Not rc.EOF Then
''   Obter_Num_Endosso = rc(0) + 1
''Else
''   Obter_Num_Endosso = 1
''End If
''
''rc.Close
''
''Exit Function
''
''Erro:
''    TrataErroGeral "Obter_Num_Endosso"
''    TerminaSEGBR
''
''End Function

''Function Obter_Cliente_Proposta(PPropostaId) As String
''
'''Obtendo Municipio_Id do endereco de risco'''''''''''''''''''''''''''''''''''''''''''''''''
''
''SQL = "Select "
''SQL = SQL & "      prop_cliente_id "
''SQL = SQL & "From "
''SQL = SQL & "      proposta_tb "
''SQL = SQL & "Where "
''SQL = SQL & "      proposta_id = " & PPropostaId
''
''Set Rst = rdocn.OpenResultset(SQL)
''
''If Not Rst.EOF Then
''    Obter_Cliente_Proposta = Rst!prop_cliente_id
''Else
''    Obter_Cliente_Proposta = ""
''End If
''
''Rst.Close
''
''End Function

''Function Obter_Produto_Proposta(PPropostaId) As String
''
'''Obtendo Municipio_Id do endereco de risco'''''''''''''''''''''''''''''''''''''''''''''''''
''
''SQL = "Select "
''SQL = SQL & "      produto_id "
''SQL = SQL & "From "
''SQL = SQL & "      proposta_tb "
''SQL = SQL & "Where "
''SQL = SQL & "      proposta_id = " & PPropostaId
''
''Set Rst = rdocn.OpenResultset(SQL)
''
''If Not Rst.EOF Then
''    Obter_Produto_Proposta = Rst!Produto_Id
''Else
''    Obter_Produto_Proposta = ""
''End If
''
''Rst.Close
''
''End Function

Function Obter_Beneficiario(PPropostaId) As String

SQL = ""
SQL = SQL & "SELECT proposta_tb.prop_cliente_id, "
'Inicio:Demanda 11292027 Autor:Eduardo.Borges Data:08/06/2011
SQL = SQL & "       produto_tb.processa_restituicao_automatica, "
'Fim:Demanda 11292027 Autor:Eduardo.Borges Data:08/06/2011
SQL = SQL & "       produto_tb.restituicao_tp_beneficiario, "
SQL = SQL & "       produto_tb.restituicao_cliente_id "
SQL = SQL & "FROM proposta_tb "
SQL = SQL & "JOIN produto_tb "
SQL = SQL & "  ON produto_tb.produto_id = proposta_tb.produto_id "
SQL = SQL & "WHERE proposta_id = " & PPropostaId

Set Rst = rdocn.OpenResultset(SQL)

If Not Rst.EOF Then
    If UCase(Rst!restituicao_tp_beneficiario) = "P" Then
       Obter_Beneficiario = Rst!prop_cliente_id
    ElseIf UCase(Rst!restituicao_tp_beneficiario) = "T" Then
       Obter_Beneficiario = Rst!restituicao_cliente_id
   'Inicio:Demanda 11292027 Autor:Eduardo.Borges Data:08/06/2011
    ElseIf IsNull(Rst("restituicao_tp_beneficiario")) And LCase(Rst("processa_restituicao_automatica")) = "n" Then
       Obter_Beneficiario = Rst!prop_cliente_id
   'Fim:Demanda 11292027 Autor:Eduardo.Borges Data:08/06/2011
    Else
       If Rst("tp_ramo_id") = 2 Then
                Obter_Beneficiario = Rst!prop_cliente_id
       Else
       MsgBox "N�o foi poss�vel localizar o benefici�rio da proposta. "
       End If
    End If
Else
    MsgBox "N�o foi poss�vel localizar o benefici�rio da proposta. "
    End
End If

Rst.Close

End Function

Sub Obter_Premio_IS(ByVal PProposta As String, _
                    ByVal PProduto As String, _
                    ByRef PValIS As Currency, _
                    ByRef PValPremio As Currency, _
                    Optional iCobertura As Integer)

Dim SQL As String
Dim Rst As rdoResultset

'Obtendo IS e Premio''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

    SQL = "SELECT "
    SQL = SQL & "   imp_segurada, "
    SQL = SQL & "   val_premio "
    SQL = SQL & "FROM "
    SQL = SQL & "   escolha_plano_tb "
    SQL = SQL & "WHERE "
    SQL = SQL & "   proposta_id = " & PProposta
    SQL = SQL & "   AND dt_fim_vigencia IS NULL "


Set Rst = rdocn.OpenResultset(SQL)

If Rst.EOF Then
    
    Rst.Close
    
    If PProduto = ProdIdAgricola Or PProduto = 1168 Then
    
        Select Case PProduto
            Case ProdIdAgricola
                SQL = "SELECT "
                SQL = SQL & "   sum(val_is), "
                SQL = SQL & "   sum(val_premio) "
                SQL = SQL & "FROM "
                SQL = SQL & "   escolha_tp_cob_generico_tb "
                SQL = SQL & "WHERE "
                SQL = SQL & "   proposta_id = " & PProposta
        '        If PProduto = 1168 And iCobertura > 0 Then
        '            SQL = SQL & "  AND tp_cobertura_id = " & iCobertura & vbNewLine
        '        End If
                SQL = SQL & "   AND dt_fim_vigencia_esc IS NULL "
            Case 1168
                SQL = ""
                adSQL SQL, "SELECT tp_cobertura_id = epsht.tp_cobertura_id, "
                adSQL SQL, "       val_is          = Sum(epsht.val_is),"
                adSQL SQL, "       Val_Premio      = Sum(epsht.VL_PCL_MOEN_SGRO)"
                adSQL SQL, "  FROM emi_parcelas_seguro_habitacional_tb epsht"
                adSQL SQL, " WHERE epsht.proposta_id = " & PProposta
                adSQL SQL, " GROUP BY epsht.tp_cobertura_id"
                
            End Select
                
            Set Rst = rdocn.OpenResultset(SQL)
    
    Else
        
        SQL = "SELECT "
        SQL = SQL & "   val_is, "
        SQL = SQL & "   val_premio_bruto "
        SQL = SQL & "FROM "
        SQL = SQL & "   proposta_adesao_tb "
        SQL = SQL & "WHERE "
        SQL = SQL & "   proposta_id = " & PProposta
        
        Set Rst = rdocn.OpenResultset(SQL)
        
        If Rst.EOF Then
        
            Rst.Close
            
            SQL = "SELECT "
            SQL = SQL & "   val_is, "
            SQL = SQL & "   val_premio_bruto "
            SQL = SQL & "FROM "
            SQL = SQL & "   proposta_fechada_tb "
            SQL = SQL & "WHERE "
            SQL = SQL & "   proposta_id = " & PProposta
            
            Set Rst = rdocn.OpenResultset(SQL)
        
        End If
    End If

End If


If bModular = False Then
    If Not Rst.EOF Then
        PValIS = Val(IIf(IsNull(Rst(0)), 0, Rst(0)))
        PValPremio = Val(IIf(IsNull(Rst(1)), 0, Rst(1)))
    Else
        PValIS = 0
        PValPremio = 0
    End If
Else
    If PProduto <> 1168 Then
        If oDadosModular.proposta_id <> PProposta Then
            PValIS = Val(IIf(IsNull(Rst(0)), 0, Rst(0)))
            PValPremio = Val(IIf(IsNull(Rst(1)), 0, Rst(1)))
        Else
            oDadosModular.Val_IS = Val(IIf(IsNull(Rst(0)), 0, Rst(0)))
            oDadosModular.Val_Premio = Val(IIf(IsNull(Rst(1)), 0, Rst(1)))
        End If
    Else
        While Not Rst.EOF
            If Rst("tp_cobertura_id") <> oDadosModular.iTpCobertura Then
                PValIS = Val(IIf(IsNull(Rst("val_is")), 0, Rst("val_is")))
                PValPremio = Val(IIf(IsNull(Rst("Val_Premio")), 0, Rst("Val_Premio")))
            Else
                oDadosModular.Val_IS = Val(IIf(IsNull(Rst("val_is")), 0, Rst("val_is")))
                oDadosModular.Val_Premio = Val(IIf(IsNull(Rst("Val_Premio")), 0, Rst("Val_Premio")))
            End If
            Rst.MoveNext
        Wend
        Rst.Close
    End If
End If
    
End Sub

''Sub Obter_Tx_Desconto(ByVal PProposta As String, _
''                      ByRef PFatDesconto As Currency)
''
''Dim SQL As String
''Dim Rst As rdoResultset
''
'''Obtendo IS e Premio'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''
''SQL = "Select "
''SQL = SQL & "   ((100 - desconto_renov) / 100) "
''SQL = SQL & " * ((100 - desconto_progres) / 100) "
''SQL = SQL & " * ((100 - desconto_shopping) / 100) "
''SQL = SQL & " * ((100 - desconto_fidelidade) / 100)"
''SQL = SQL & "From "
''SQL = SQL & "   proposta_adesao_tb "
''SQL = SQL & "Where "
''SQL = SQL & "   proposta_id = " & PProposta
''
''Set Rst = rdocn.OpenResultset(SQL)
''
''If Rst.EOF Then
''
''    Rst.Close
''
''    SQL = "Select "
''    SQL = SQL & "   ((100 - desconto_renov) / 100) "
''    SQL = SQL & " * ((100 - desconto_progres) / 100) "
''    SQL = SQL & " * ((100 - desconto_shopping) / 100) "
''    SQL = SQL & " * ((100 - desconto_fidelidade) / 100)"
''    SQL = SQL & "From "
''    SQL = SQL & "   proposta_fechada_tb "
''    SQL = SQL & "Where "
''    SQL = SQL & "   proposta_id = " & PProposta
''
''    Set Rst = rdocn.OpenResultset(SQL)
''
''End If
''
''If Not Rst.EOF Then
''    PFatDesconto = Val(Rst(0))
''Else
''    PFatDesconto = 0
''End If
''
''End Sub

''Function ObterTotalPagoApoliceAberta(ByVal PProposta) As Currency
''
''Dim sSQL As String
''Dim RsTotal As rdoResultset
''
''    sSQL = ""
''    sSQL = sSQL & "SELECT val_pago = ISNULL(SUM(ISNULL(val_pago, 0)), 0) "
''    sSQL = sSQL & "  FROM agendamento_cobranca_tb"
''    sSQL = sSQL & " WHERE proposta_id = " & PProposta
''    sSQL = sSQL & "   AND situacao = 'a' "
''
''    Set RsTotal = rdocn.OpenResultset(sSQL)
''
''    If Not RsTotal.EOF Then
''        ObterTotalPagoApoliceAberta = Val(MudaVirgulaParaPonto(RsTotal("val_pago")))
''    Else
''        ObterTotalPagoApoliceAberta = 0
''    End If
''
''    RsTotal.Close
''    Set RsTotal = Nothing
''
''End Function

Sub Carregar_Colecao_Cobrancas(ByVal PProposta)

Dim SQL As String
Dim Rst As rdoResultset
Dim Linha As String
Dim Cob As Cobranca
Dim existe_adimplente As Integer
Set Cobrancas = Nothing

'Selecionando cobran�as''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
existe_adimplente = 0

SQL = " Select "
SQL = SQL & "   num_cobranca, "
SQL = SQL & "   situacao, "
SQL = SQL & "   num_endosso = isnull(num_endosso, 0), "
SQL = SQL & "   num_parcela_endosso = isnull(num_parcela_endosso, 0), "
SQL = SQL & "   dt_agendamento, "
SQL = SQL & "   val_cobranca, "
SQL = SQL & "   dt_Recebimento = (case when dt_recebimento is null then ' ' else convert(varchar(30),dt_recebimento,103)end),"
SQL = SQL & "   val_pago = isnull(val_pago, 0), "
SQL = SQL & "   val_iof_bb = isnull(val_iof_bb, 0), "
SQL = SQL & "   val_comissao_bb = isnull(val_comissao_bb, 0), "
SQL = SQL & "   val_ir_bb = isnull(val_ir_bb, 0), "
SQL = SQL & "   val_comissao_estipulante_bb = isnull(val_comissao_estipulante_bb, 0), "
SQL = SQL & "   val_adic_fracionamento = isnull(val_adic_fracionamento, 0), "
SQL = SQL & "   val_desconto = isnull(val_desconto, 0), "
SQL = SQL & "   val_juros_mora = isnull(val_juros_mora, 0), "

'Rsilva - 12/01/2010 - Restitui��o Seguro Habitacional
If Produto_id = 1168 Then
    adSQL SQL, " epsht.tp_cobertura_id, "
End If

'bcarneiro - 14/03/2007 - Restitui��o Agr�cola
SQL = SQL & "   parcela_subvencao_sem_remessa = CASE WHEN agendamento_cobranca_tb.num_cobranca = 3 " & vbNewLine
SQL = SQL & "                                         AND proposta_subvencao_tb.proposta_id IS NOT NULL " & vbNewLine
SQL = SQL & "                                         AND proposta_subvencao_tb.remessa_subvencao_emissao_id IS NULL " & vbNewLine
SQL = SQL & "                                        THEN 'S' " & vbNewLine
SQL = SQL & "                                        Else 'N' " & vbNewLine
SQL = SQL & "                                   END " & vbNewLine
''''''''''''''''''''''''''''''''''''''''''''''''

SQL = SQL & " From "
SQL = SQL & "   agendamento_cobranca_tb "
 
'Rsilva - 12/01/2010 - Restitui��o Seguro Habitacional
If Produto_id = 1168 Then
   adSQL SQL, " JOIN seguros_db.dbo.emi_parcelas_seguro_habitacional_tb epsht"
   adSQL SQL, " ON epsht.NR_CBR = agendamento_cobranca_tb.nosso_numero"
End If

'bcarneiro - 14/03/2007 - Restitui��o Agr�cola
SQL = SQL & "LEFT JOIN proposta_subvencao_tb with(NOLOCK) " & vbNewLine
SQL = SQL & "  ON proposta_subvencao_tb.proposta_id = agendamento_cobranca_tb.proposta_id " & vbNewLine

' bmoraes 15/09/2008 - tipo de subven��o
SQL = SQL & "  AND proposta_subvencao_tb.tipo_subvencao_id = 1 " & vbNewLine
'''''''''''''''''''''''''

SQL = SQL & " Where "
SQL = SQL & "   agendamento_cobranca_tb.proposta_id = " & PProposta
'SQL = SQL & "   and isnull(num_endosso, 0) = 0 "
'SQL = SQL & "   and situacao = 'a' "

'bcarneiro - 14/03/2007 - Restitui��o Agr�cola
'SQL = SQL & "  AND agendamento_cobranca_tb.num_cobranca <= (CASE WHEN proposta_subvencao_tb.proposta_id IS NOT NULL THEN " & vbNewLine
'SQL = SQL & "                                                         CASE WHEN proposta_subvencao_tb.remessa_subvencao_emissao_id IS NULL THEN 2 " & vbNewLine
'SQL = SQL & "                                                              ELSE agendamento_cobranca_tb.num_cobranca " & vbNewLine
'SQL = SQL & "                                                         END " & vbNewLine
'SQL = SQL & "                                                     ELSE agendamento_cobranca_tb.num_cobranca " & vbNewLine
'SQL = SQL & "                                               END) " & vbNewLine
'''''''''''''''''''''''''''''''''''''''''''''''''''

SQL = SQL & " Order by "
SQL = SQL & "   num_cobranca "

Set Rst = rdocn.OpenResultset(SQL)

Do While Not Rst.EOF

    Set Cob = New Cobranca
    
    Cob.proposta_id = PProposta
    Cob.Situacao = IIf(Not IsNull(Rst!Situacao), Rst!Situacao, "")
    Cob.endosso_id = IIf(Not IsNull(Rst!num_endosso), Rst!num_endosso, 0)
    Cob.Num_Cobranca = IIf(Not IsNull(Rst!Num_Cobranca), Rst!Num_Cobranca, 0)
    Cob.Num_Parcela_Endosso = IIf(Not IsNull(Rst!Num_Parcela_Endosso), Rst!Num_Parcela_Endosso, 0)
    Cob.Val_Cobranca = IIf(Not IsNull(Rst!Val_Cobranca), Val(Rst!Val_Cobranca), 0)
    Cob.Dt_Agendamento = Format(Rst!Dt_Agendamento, "dd/mm/yyyy")
    Cob.Dt_Recebimento = Format(Rst!Dt_Recebimento, "dd/mm/yyyy")
    
    If Produto_id = 1168 Then
        Cob.Tp_cobertura = Rst!tp_cobertura_id
    End If
        
    If Rst!Situacao = "a" Then
        existe_adimplente = existe_adimplente + 1
    End If
    
    If Rst!Situacao <> "i" Then
        Cob.Val_Pago = Val(Rst!Val_Pago)
        Cob.Val_IOF = Val(Rst!Val_IOF_bb)
        Cob.Val_Comissao = Val(Rst!val_comissao_bb)
        Cob.Val_IR = Val(Rst!Val_ir_bb)
        Cob.Val_Pro_Labore = Val(Rst!val_comissao_estipulante_bb)
        Cob.Val_Adic_Fracionamento = Val(Rst!Val_Adic_Fracionamento)
        Cob.Val_Desconto = Val(Rst!Val_Desconto)
        Cob.Val_Juros = Val(Rst!Val_Juros_mora)
    Else
        Cob.Val_Pago = 0
        Cob.Val_IOF = 0
        Cob.Val_Comissao = 0
        Cob.Val_IR = 0
        Cob.Val_Pro_Labore = 0
        Cob.Val_Adic_Fracionamento = 0
        Cob.Val_Desconto = 0
        Cob.Val_Juros = 0
    End If
    
    'bcarneiro - 14/03/2007 - Restitui��o Agr�cola
    Cob.bSubvencaoSemRemessa = IIf(UCase(Rst!parcela_subvencao_sem_remessa) = "S", True, False)
    ''''''''''''''''''''''''''''''''''''''''''''''
    
    Cobrancas.Add Cob

    Rst.MoveNext
    
Loop

vgsituacao = IIf(existe_adimplente > 0, "a", "p")

End Sub

Sub Main()

'Conectando � base de dados''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

'Teste - Comentar para liberar
cUserName = "NOVA"
glAmbiente_id = 3


Call Conexao

TpOperacao = Left(Command(), 1)

TpOperacao = 2

If TpOperacao = 1 Then
  Call Seguranca("SEGP0209.1", "Avalia��o T�cnica de Restitui��o")
Else
  Call Seguranca("SEGP0209.2", "Avalia��o Gerencial de Restitui��o")
End If

'Renato Vasconcelos
''Flow 11022639
'FrmSelEndosso.Show
'
'If TpOperacao = 1 Then
'   FrmSelEndosso.BtnModo.Visible = False
'Else
'   FrmSelEndosso.BtnModo.Visible = True
'End If


frmFiltroPesquisa.Show

If TpOperacao = 1 Then
    gerencial = False
    frmFiltroPesquisa.BtnModo.Visible = gerencial
Else
    gerencial = True
    frmFiltroPesquisa.BtnModo.Visible = gerencial
End If

Call Unload(FrmEspera)


End Sub

'Function FormataValorParaPadraoBrasil(valor As String) As String
'
'   ConfDecimal = LeArquivoIni("WIN.INI", "intl", "sDecimal")
'
'   'Convertendo valor do d�bito para o padr�o brasileiro
'
'   If ConfDecimal = "." Then
'      valor = TrocaValorAmePorBras(Format(Val(valor), "###,###,##0.00"))
'   Else
'      valor = Format(Val(valor), "###,###,##0.00")
'   End If
'
'   FormataValorParaPadraoBrasil = valor
'
'End Function
                                           
Public Sub Obter_Dados_Adicionais_Proposta(ByVal PProposta, _
                                           ByRef PRamoId As String, _
                                           ByRef PCustoApoliceCertif As Currency, _
                                           Optional ByRef PPeriodo_pgto_id As String, _
                                           Optional ByRef vMoedaPremioId As Variant, _
                                           Optional ByRef sTipoEmissao As String, _
                                           Optional ByRef lSeguradora As Long)

  'Demanda 400422 - emaior 02/12/2008 - Relat�rio de Restitui��o de Comiss�o de Cosseguro
  'Inclus�o dos par�metros "sTipoEmissao" e "lSeguradora"
  
  Dim SQL As String
  Dim Rst As rdoResultset
  
'--------------------------------------------------------------------------------------
'  SQL = " SELECT "
'  SQL = SQL & "       ramo_id, "
'  SQL = SQL & "       custo_apolice_certif = isnull(custo_certificado, 0) , "
'  'pcarvalho - 17/06/2003 Padroniza��o do c�lculo da restitui��o
'  SQL = SQL & "       premio_moeda_id = ISNULL(premio_moeda_id, 0), "
'  SQL = SQL & "       periodo_pgto_id = ISNULL(periodo_pgto_id, 0) "
'  SQL = SQL & " FROM "
'  SQL = SQL & "       proposta_adesao_tb "
'  SQL = SQL & " WHERE "
'  SQL = SQL & "       proposta_id = " & PProposta
'-------------------------------------------------------------------------------------

  'Demanda 400422 - emaior - 02/12/2008 - ALTERADO PARA SELECIONAR O TIPO DE EMISS�O E SEGURADORA DA AP�LICE
  SQL = " SELECT "
  SQL = SQL & "       proposta_adesao_tb.ramo_id, "
  SQL = SQL & "       custo_apolice_certif = isnull(proposta_adesao_tb.custo_certificado, 0) , "
  'pcarvalho - 17/06/2003 Padroniza��o do c�lculo da restitui��o
  SQL = SQL & "       premio_moeda_id = ISNULL(proposta_adesao_tb.premio_moeda_id, 0), "
  SQL = SQL & "       periodo_pgto_id = ISNULL(proposta_adesao_tb.periodo_pgto_id, 0), "
  SQL = SQL & "       apolice_tb.tp_emissao, "
  SQL = SQL & "       apolice_tb.seguradora_cod_susep "
  SQL = SQL & " FROM "
  SQL = SQL & "       proposta_adesao_tb with(NOLOCK) "
  SQL = SQL & " INNER JOIN apolice_tb with(NOLOCK) "
  SQL = SQL & "         ON apolice_tb.apolice_id = proposta_adesao_tb.apolice_id "
  SQL = SQL & "        AND apolice_tb.sucursal_seguradora_id = proposta_adesao_tb.sucursal_seguradora_id "
  SQL = SQL & "        AND apolice_tb.seguradora_cod_susep = proposta_adesao_tb.seguradora_cod_susep "
  SQL = SQL & "        AND apolice_tb.ramo_id = proposta_adesao_tb.ramo_id "
  SQL = SQL & " WHERE "
  SQL = SQL & "       proposta_adesao_tb.proposta_id = " & PProposta
  
  
  Set Rst = rdocn.OpenResultset(SQL)
  
  If Rst.EOF Then
  
     Rst.Close
'-----------------------------------------------------------------------------------------------
'     SQL = " SELECT "
'     SQL = SQL & "       apolice_tb.ramo_id, "
'     SQL = SQL & "       custo_apolice_certif = proposta_fechada_tb.custo_apolice, "
'     'pcarvalho - 17/06/2003 Padroniza��o do c�lculo da restitui��o
'     SQL = SQL & "       premio_moeda_id = ISNULL(premio_moeda_id, 0), "
'     SQL = SQL & "       periodo_pgto_id = ISNULL(periodo_pgto_id, 0) "
'     SQL = SQL & " FROM "
'     SQL = SQL & "       proposta_fechada_tb "
'     SQL = SQL & "       inner join apolice_tb "
'     SQL = SQL & "             on apolice_tb.proposta_id = proposta_fechada_tb.proposta_id "
'     SQL = SQL & " WHERE "
'     SQL = SQL & "       proposta_fechada_tb.proposta_id = " & PProposta
'-----------------------------------------------------------------------------------------------

     'Demanda 400422 - emaior - 02/12/2008 - ALTERADO PARA SELECIONAR O TIPO DE EMISS�O E SEGURADORA DA AP�LICE
     SQL = " SELECT "
     SQL = SQL & "       apolice_tb.ramo_id, "
     SQL = SQL & "       custo_apolice_certif = proposta_fechada_tb.custo_apolice, "
     'pcarvalho - 17/06/2003 Padroniza��o do c�lculo da restitui��o
     SQL = SQL & "       premio_moeda_id = ISNULL(premio_moeda_id, 0), "
     SQL = SQL & "       periodo_pgto_id = ISNULL(periodo_pgto_id, 0), "
     SQL = SQL & "       apolice_tb.tp_emissao, "
     SQL = SQL & "       apolice_tb.seguradora_cod_susep "
     SQL = SQL & " FROM "
     SQL = SQL & "       proposta_fechada_tb "
     SQL = SQL & "       INNER JOIN apolice_tb "
     SQL = SQL & "             ON apolice_tb.proposta_id = proposta_fechada_tb.proposta_id "
     SQL = SQL & " WHERE "
     SQL = SQL & "       proposta_fechada_tb.proposta_id = " & PProposta
  
     Set Rst = rdocn.OpenResultset(SQL)
     
  End If
  
  PRamoId = Rst!ramo_id
  PCustoApoliceCertif = Val(Rst!custo_apolice_certif)
  vMoedaPremioId = Val(Rst("premio_moeda_id"))     'pcarvalho - 17/06/2003 - Padroniza��o do c�lculo da restitui��o
  PPeriodo_pgto_id = Rst!periodo_pgto_id
  sTipoEmissao = Rst!tp_emissao           'Demanda 400422 - emaior - 02/12/2008
  lSeguradora = Rst!seguradora_cod_susep  'Demanda 400422 - emaior - 02/12/2008
  
  'PValPremioTarifa = Val(Rst!val_premio_tarifa)
  'PValDescontoComercial = Val(Rst!val_tot_desconto_comercial)
  
  Rst.Close

End Sub
Public Sub Obter_Dados_Adicionais_Proposta_Modular(ByVal PProposta, _
                                                   ByRef PRamoId As String, _
                                                   ByRef PCustoApoliceCertif As Currency, _
                                                   Optional ByRef PPeriodo_pgto_id As String, _
                                                   Optional ByRef vMoedaPremioId As Variant, _
                                                   Optional ByRef sTipoEmissao As String, _
                                                   Optional ByRef lSeguradora As Long)
  
  Dim SQL As String
  Dim RstM As rdoResultset
  
  SQL = " SELECT " & vbNewLine
  SQL = SQL & "       proposta_adesao_tb.ramo_id, " & vbNewLine
  SQL = SQL & "       custo_apolice_certif = isnull(proposta_adesao_tb.custo_certificado, 0) , " & vbNewLine
  SQL = SQL & "       premio_moeda_id = ISNULL(proposta_adesao_tb.premio_moeda_id, 0), " & vbNewLine
  SQL = SQL & "       periodo_pgto_id = ISNULL(proposta_adesao_tb.periodo_pgto_id, 0), " & vbNewLine
  SQL = SQL & "       apolice_tb.tp_emissao, " & vbNewLine
  SQL = SQL & "       apolice_tb.seguradora_cod_susep " & vbNewLine
  SQL = SQL & " FROM " & vbNewLine
  SQL = SQL & "       proposta_adesao_tb with(NOLOCK) " & vbNewLine
  SQL = SQL & " INNER JOIN apolice_tb with(NOLOCK) " & vbNewLine
  SQL = SQL & "         ON apolice_tb.apolice_id = proposta_adesao_tb.apolice_id " & vbNewLine
  SQL = SQL & "        AND apolice_tb.sucursal_seguradora_id = proposta_adesao_tb.sucursal_seguradora_id " & vbNewLine
  SQL = SQL & "        AND apolice_tb.seguradora_cod_susep = proposta_adesao_tb.seguradora_cod_susep " & vbNewLine
  SQL = SQL & "        AND apolice_tb.ramo_id = proposta_adesao_tb.ramo_id " & vbNewLine
  SQL = SQL & " WHERE " & vbNewLine
  SQL = SQL & "       proposta_adesao_tb.proposta_id = " & PProposta
  
  Set RstM = rdocn.OpenResultset(SQL)
  
  If RstM.EOF Then
  
     RstM.Close
     SQL = " SELECT "
     SQL = SQL & "       apolice_tb.ramo_id, "
     SQL = SQL & "       custo_apolice_certif = proposta_fechada_tb.custo_apolice, "
     SQL = SQL & "       premio_moeda_id = ISNULL(premio_moeda_id, 0), "
     SQL = SQL & "       periodo_pgto_id = ISNULL(periodo_pgto_id, 0), "
     SQL = SQL & "       apolice_tb.tp_emissao, "
     SQL = SQL & "       apolice_tb.seguradora_cod_susep "
     SQL = SQL & " FROM "
     SQL = SQL & "       proposta_fechada_tb "
     SQL = SQL & "       INNER JOIN apolice_tb "
     SQL = SQL & "             ON apolice_tb.proposta_id = proposta_fechada_tb.proposta_id "
     SQL = SQL & " WHERE "
     SQL = SQL & "       proposta_fechada_tb.proposta_id = " & PProposta
  
     Set RstM = rdocn.OpenResultset(SQL)
     
  End If
  
  If Not RstM.EOF Then
    oDadosModular.ramo_id = RstM("ramo_id")
    oDadosModular.Custo_Apolice = Val(RstM("custo_apolice_certif"))
    oDadosModular.MoedaPremioId = RstM("premio_moeda_id")
    oDadosModular.PeriodoPgtoId = RstM("periodo_pgto_id")
    oDadosModular.TipoEmissao = RstM("tp_emissao")
    oDadosModular.Seguradora = RstM("seguradora_cod_susep")
  End If

'  PRamoId = Rst!ramo_id
'  PCustoApoliceCertif = Val(Rst!custo_apolice_certif)
'  vMoedaPremioId = Val(Rst("premio_moeda_id"))
'  PPeriodo_pgto_id = Rst!periodo_pgto_id
'  sTipoEmissao = Rst!tp_emissao
'  lSeguradora = Rst!seguradora_cod_susep

  
  RstM.Close

End Sub

''Function Obter_TpRamo_Produto(ByVal PProduto) As String
''
''Dim SQL As String
''Dim Rst As rdoResultset
''
''SQL = "Select "
''SQL = SQL & "   tp_ramo_id "
''SQL = SQL & "From "
''SQL = SQL & "   item_produto_tb "
''SQL = SQL & "   inner join ramo_tb on ramo_tb.ramo_id = item_produto_tb.ramo_id "
''SQL = SQL & "Where "
''SQL = SQL & "   produto_id = " & PProduto
''
''Set Rst = rdocn.OpenResultset(SQL)
''
''If Not Rst.EOF Then
''   Obter_TpRamo_Produto = Rst!tp_ramo_id
''Else
''   Obter_TpRamo_Produto = ""
''End If
''
''Rst.Close
''
''End Function

Sub Obter_Tx_Adic_Fracionamento(ByVal PProposta As String, _
                                ByRef PFatFracionamento As Currency)

Dim SQL As String
Dim Rst As rdoResultset

SQL = "Select "
SQL = SQL & "     juros_parcelamento_tb.fat_fracionamento "
SQL = SQL & " From "
SQL = SQL & "     proposta_tb "
SQL = SQL & "     inner join proposta_adesao_tb "
SQL = SQL & "          on proposta_adesao_tb.proposta_id = proposta_tb.proposta_id "
SQL = SQL & "     inner join juros_tb"
SQL = SQL & "          on juros_tb.perc_juros = proposta_adesao_tb.taxa_juros"
SQL = SQL & "     inner join juros_parcelamento_tb "
SQL = SQL & "          on juros_parcelamento_tb.juros_id = juros_tb.juros_id "
SQL = SQL & "          and  juros_parcelamento_tb.qtd_parcelas = proposta_adesao_tb.qtd_parcela_premio"
SQL = SQL & " Where"
SQL = SQL & "      proposta_tb.proposta_id = " & PProposta
SQL = SQL & "      and isnull(proposta_adesao_tb.taxa_juros, 0) <> 0 "
SQL = SQL & "      and juros_parcelamento_tb.tp_juros = Case "
SQL = SQL & "                                               When proposta_adesao_tb.val_pgto_ato is not null then 'a' "
SQL = SQL & "                                               Else 'p' "
SQL = SQL & "                                           End "

Set Rst = rdocn.OpenResultset(SQL)

If Rst.EOF Then
     
    Rst.Close
    
    SQL = "Select "
    SQL = SQL & "     juros_parcelamento_tb.fat_fracionamento "
    SQL = SQL & " From "
    SQL = SQL & "     proposta_tb "
    SQL = SQL & "     inner join proposta_fechada_tb "
    SQL = SQL & "          on proposta_fechada_tb.proposta_id = proposta_tb.proposta_id "
    SQL = SQL & "     inner join juros_tb"
    SQL = SQL & "          on juros_tb.perc_juros = proposta_fechada_tb.taxa_juros"
    SQL = SQL & "     inner join juros_parcelamento_tb "
    SQL = SQL & "          on juros_parcelamento_tb.juros_id = juros_tb.juros_id "
    SQL = SQL & "          and  juros_parcelamento_tb.qtd_parcelas = proposta_fechada_tb.num_parcelas"
    SQL = SQL & " Where"
    SQL = SQL & "      proposta_tb.proposta_id = " & PProposta
    SQL = SQL & "      and isnull(proposta_fechada_tb.taxa_juros, 0) <> 0 "
    SQL = SQL & "      and juros_parcelamento_tb.tp_juros = Case "
    SQL = SQL & "                                               When proposta_fechada_tb.val_pgto_ato is not null then 'a' "
    SQL = SQL & "                                               Else 'p' "
    SQL = SQL & "                                           End "
    
    Set Rst = rdocn.OpenResultset(SQL)
        
End If

If Not Rst.EOF Then
   PFatFracionamento = Val(Rst!fat_fracionamento)
Else
   PFatFracionamento = 0
End If

Rst.Close

End Sub

''Function Verificar_Existencia_Avaliacao(ByVal PProposta) As Boolean
''
''Dim Rst As rdoResultset
''Dim SQL As String
''
''SQL = " SELECT "
''SQL = SQL & "       proposta_id "
''SQL = SQL & " FROM "
''SQL = SQL & "       avaliacao_cancelamento_tb "
''SQL = SQL & " WHERE "
''SQL = SQL & "       proposta_id = " & PProposta
''
''Set Rst = rdocn.OpenResultset(SQL)
''
''If Not Rst.EOF Then
''    Verificar_Existencia_Avaliacao = True
''Else
''    Verificar_Existencia_Avaliacao = False
''End If
''
''End Function

Sub Obter_Total_Corretagem(ByRef PTotCorretagem As Currency)

    Dim Corretagem As Corretagem
    Dim TotCorretagem As Currency
    
    For Each Corretagem In Corretagens
        
        TotCorretagem = TotCorretagem + Corretagem.Val_Comissao
                    
    Next

    PTotCorretagem = TotCorretagem
    
End Sub

Function TiraPonto(ValorAux As String) As String

    If InStr(ValorAux, ".") = 0 Then
        TiraPonto = ValorAux
    Else
        '* Alterado por Junior em 13/10/2003 - para tratar mais de UM ponto
        ValorAux = Replace(ValorAux, ".", "")
        'ValorAux = Mid(ValorAux, 1, InStr(ValorAux, ".") - 1) + Mid(ValorAux, InStr(ValorAux, ".") + 1)
        TiraPonto = ValorAux
    End If
    
End Function

               
Sub Obter_Totais_Pagos(ByRef PValPremio As Currency, _
                       ByRef PValJuros As Currency, _
                       ByRef PValAdicFracionamento As Currency, _
                       ByRef PValDesconto As Currency, _
                       ByRef PValIOF As Currency, Optional iFlag As Boolean)

    Dim Cob As Cobranca
    Dim ValCustoApoliceCertif As Currency
    
    Dim MVal_premio As Currency
    Dim MVal_juros As Currency
    Dim MVal_adic_fracionamento As Currency
    Dim MVal_desconto As Currency
    Dim MVal_IOF As Currency
    Dim modular As Integer
    
    
'    If bModular = True Then
'        MVal_premio = PValPremio
'        MVal_juros = PValJuros
'        MVal_adic_fracionamento = PValAdicFracionamento
'        MVal_desconto = PValDesconto
'        MVal_IOF = PValIOF
'    End If
    
    For Each Cob In Cobrancas
    
        'bcarneiro - 14/03/2007 - Restitui��o Agr�cola
        'If UCase(Cob.Situacao) = "A" Then
        If UCase(Cob.Situacao) = "A" And Cob.bSubvencaoSemRemessa = False Then
        ''''''''''''''''''''''''''''''''''''''''''''''''
            If bModular = False Then
                PValPremio = PValPremio + Cob.Val_Cobranca
                PValJuros = PValJuros + Cob.Val_Juros
                PValAdicFracionamento = PValAdicFracionamento + Cob.Val_Adic_Fracionamento
                PValDesconto = PValDesconto + Cob.Val_Desconto
                PValIOF = PValIOF + Cob.Val_IOF
            
            Else
                If Produto_id = 1168 Then
                    If oDadosModular.iTpCobertura <> Cob.Tp_cobertura Then
                        PValPremio = PValPremio + Cob.Val_Cobranca
                        PValJuros = PValJuros + Cob.Val_Juros
                        PValAdicFracionamento = PValAdicFracionamento + Cob.Val_Adic_Fracionamento
                        PValDesconto = PValDesconto + Cob.Val_Desconto
                        PValIOF = PValIOF + Cob.Val_IOF
                    Else
                        modular = 1
                        MVal_premio = MVal_premio + Cob.Val_Cobranca
                        MVal_juros = MVal_juros + Cob.Val_Juros
                        MVal_adic_fracionamento = MVal_adic_fracionamento + Cob.Val_Adic_Fracionamento
                        MVal_desconto = MVal_desconto + Cob.Val_Desconto
                        MVal_IOF = MVal_IOF + Cob.Val_IOF
                    End If
                Else
                    modular = 1
                    MVal_premio = MVal_premio + Cob.Val_Cobranca
                    MVal_juros = MVal_juros + Cob.Val_Juros
                    MVal_adic_fracionamento = MVal_adic_fracionamento + Cob.Val_Adic_Fracionamento
                    MVal_desconto = MVal_desconto + Cob.Val_Desconto
                    MVal_IOF = MVal_IOF + Cob.Val_IOF
                End If
            End If
            
        End If
        
    Next
    
    If bModular = True And modular = 1 Then
        oDadosModular.Val_Premio_Pago = MVal_premio
        oDadosModular.Val_Juros = MVal_juros
        oDadosModular.Val_Adic_Fracionamento = MVal_adic_fracionamento
        oDadosModular.Val_Desconto = MVal_desconto
        oDadosModular.Val_IOF_Restituicao = MVal_IOF
    End If
End Sub

''Sub Obter_Valor_Restituicao(ByRef PPropostaId As String, _
''                            ByVal PRamoId As String, _
''                            ByVal PDtContratacao As String, _
''                            ByVal PDtInicio_Vigencia As String, _
''                            ByVal PDiasVigenciaSeguro As Integer, _
''                            ByVal PDiasUtilizacaoSeguro As Integer, _
''                            ByVal PTpCalcRestituicao As String, _
''                            ByVal PPrazoRestituicaoIntegral As Integer, _
''                            ByVal PTotalPremio As Currency, _
''                            ByVal PTotalPremioPago As Currency, _
''                            ByVal PTotalJurosPago As Currency, _
''                            ByVal PTotalAdicFracionamentoPago As Currency, _
''                            ByVal PTotalDescontoPago As Currency, _
''                            ByVal PTotalIOFPago As Currency, _
''                            ByVal PValorCustoApoliceEmissao As Currency, _
''                            ByRef PValorRestituicao As Currency, _
''                            ByRef PValorIOFRest As Currency, _
''                            ByRef PValorCustoApoliceRest As Currency, _
''                            ByRef PValorAdicFracionamentoRest As Currency, _
''                            ByRef PValorDescontoRest As Currency, _
''                            ByRef PValorPremioTarifaRest As Currency)
''
''
''    Dim FatorDiasVigenciaSeguro As Currency
''    Dim FatorDiasUtilizacaoSeguro As Currency
''    Dim PercRestituicao As String
''    Dim ValorTotalPremioDevido As Currency
''
''    Dim ValorTotalPagamentoExcedente As Currency
''    Dim ValorIOFTotalPagamentoExcedente As Currency
''
''    Dim ValorRestituicao As Currency
''    Dim ValorIOFRest As Currency
''    Dim ValorCustoApoliceRest As Currency
''    Dim ValorAdicFracionamentoRest As Currency
''    Dim ValorDescontoRest As Currency
''    Dim ValorPremioTarifaRest As Currency
''
''    Dim RestituicaoIntegral As Boolean
''
''    Dim FatAdicFracionamentoEmissao As Currency
''    Dim FatDescontoEmissao As Currency
''
''    '---------------------
''    'Luciana - 27/05/2003
''    '---------------------
''    Dim CodMotCanc As Long
''    Dim rc As rdoResultset
''    Dim SQL As String
''    '---------------------
''
''    existe_perc_restituicao = True
''
''    If PDiasUtilizacaoSeguro > PDiasVigenciaSeguro Then
''        PValorRestituicao = 0
''        PValorIOFRest = 0
''        PValorCustoApoliceRest = 0
''        PValorAdicFracionamentoRest = 0
''        PValorDescontoRest = 0
''        PValorPremioTarifaRest = 0
''        Exit Sub
''    End If
''
''    'Verificando se a restitui��o � intergral''''''''''''''''''''''''''''''''''''''''''''''
''
''    If PDiasUtilizacaoSeguro <= PPrazoRestituicaoIntegral Then
''       RestituicaoIntegral = True
''    Else
''       RestituicaoIntegral = False
''    End If
''
''    'Obtendo valor da restitui��o e custo da ap�lice e IOF  '''''''''''''''''''''''''''''''
''
''    If RestituicaoIntegral = True Then
''
''        ValorRestituicao = (PTotalPremioPago - PTotalJurosPago)
''
''        ValorCustoApoliceRest = PValorCustoApoliceEmissao
''
''        ValorIOFRest = PTotalIOFPago
''
''        ValorAdicFracionamentoRest = PTotalAdicFracionamentoPago
''
''        ValorDescontoRest = PTotalDescontoPago
''
''    Else
''
''        '---------------------------------------------------------------------------------------------
''        'Luciana - 27/05/2003 - In�cio - Quando o endosso de cancelamento recebido possuir c�digo de
''        'cancelamento 1 - Frustra��o de Safra ou 4 - Ap�lice pr�-existente, � calculada a devolu��o do
''        'pr�mio pago
''        '---------------------------------------------------------------------------------------------
''        If Produto_Id = ProIdPenhorRural Then
''            SQL = "select cod_mot_canc from seguro_penhor_rural_tb where proposta_id = " & PPropostaId
''            Set rc = rdocn.OpenResultset(SQL)
''            If Not rc.EOF Then
''                CodMotCanc = IIf(IsNull(rc(0)), 0, rc(0))
''            End If
''            rc.Close
''            If CodMotCanc = 1 Or CodMotCanc = 4 Then
''                SQL = "select sum(val_cobranca) from agendamento_cobran�a_tb where proposta_id = " & PPropostaId
''                SQL = SQL & " and situa��o = 'a'"
''                Set rc = rdocn.OpenResultset(SQL)
''                If Not rc.EOF Then
''                    ValorTotalPagamentoExcedente = IIf(IsNull(rc(0)), 0, rc(0))
''                    PTpCalcRestituicao = ""
''                End If
''                rc.Close
''            End If
''        End If
''        '---------------------------------------------------------------------------------------------
''        'Luciana - 27/05/2003 - Fim
''        '---------------------------------------------------------------------------------------------
''
''        Select Case UCase(PTpCalcRestituicao)
''
''            Case "P"
''
''                'Aplicando c�lculo Proporcional
''
''                ValorTotalPremioDevido = Trunca((PTotalPremio / PDiasVigenciaSeguro) * PDiasUtilizacaoSeguro)
''                ValorTotalPagamentoExcedente = (PTotalPremioPago - PTotalJurosPago) - ValorTotalPremioDevido
''
''            Case "C"
''
''                'Aplicando tabela de Curto Prazo
''
''                FatorDiasVigenciaSeguro = PDiasVigenciaSeguro / 365
''                FatorDiasUtilizacaoSeguro = PDiasUtilizacaoSeguro / FatorDiasVigenciaSeguro
''
''                'Adenilson (14/05/2003)
''                'N�mero de dias menor que 3 utiliza-se quantidade de dias igual a 3 na tabela de curto_prazo
''                If FatorDiasUtilizacaoSeguro < 3 Then
''                    FatorDiasUtilizacaoSeguro = 3
''                End If
''
''                'Obtendo percentual de restitui��o
''                SQL = "SELECT perc_restituicao FROM curto_prazo_tb WHERE qtd_dias >= " & MudaVirgulaParaPonto(FatorDiasUtilizacaoSeguro)
''                SQL = SQL & " ORDER BY qtd_dias"
''
''                Set rc = rdocn.OpenResultset(SQL)
''
''                If rc.EOF Then
''                    MensagemBatch "Percentual de restitui��o n�o cadastrado para " & MudaVirgulaParaPonto(FatorDiasUtilizacaoSeguro) & " dia(s) vigente(s).", , , False
''                    Call Atualizar_Bloqueio_Proposta(PPropostaId, "n")
''                    existe_perc_restituicao = False
''                    Exit Sub
''                End If
''
''                PercRestituicao = Val(rc!Perc_Restituicao)
''                rc.Close
''
''                ValorTotalPremioDevido = Trunca(PTotalPremio * (PercRestituicao / 100))
''                ValorTotalPagamentoExcedente = (PTotalPremioPago - PTotalJurosPago) - ValorTotalPremioDevido
''
''        End Select
''
''        'Obtendo Valor do IOF imbutido no pagamento excedente
''
''        'Call Obtem_Valor_IOF(ValorTotalPagamentoExcedente, PDtInicio_Vigencia, PRamoId, ValorIOFTotalPagamentoExcedente)
''        Call Obtem_Valor_IOF(ValorTotalPagamentoExcedente, Data_Sistema, PRamoId, ValorIOFTotalPagamentoExcedente)
''
''        'Obtendo valor da Restituicao
''
''        ValorRestituicao = ValorTotalPagamentoExcedente - PValorCustoApoliceEmissao - ValorIOFTotalPagamentoExcedente
''
''        'Definindo o valor de IOF e Custo de Ap�lice devolvidos
''
''        ValorIOFRest = 0
''        ValorCustoApoliceRest = 0
''
''        'Definindo o valor do adicional de fracionamento e desconto devolvidos
''
''        Call Obter_Tx_Adic_Fracionamento(PPropostaId, FatAdicFracionamentoEmissao)
''        If FatAdicFracionamentoEmissao <> 0 Then
''           ValorAdicFracionamentoRest = Trunca((ValorRestituicao - ValorCustoApoliceRest - ValorIOFRest) - ((ValorRestituicao - ValorCustoApoliceRest - ValorIOFRest) / FatAdicFracionamentoEmissao))
''        Else
''           ValorAdicFracionamentoRest = 0
''        End If
''        If ((ValorTotalPagamentoExcedente - PValorCustoApoliceEmissao) * PTotalDescontoPago) > 0 Then
''            ValorDescontoRest = ((ValorTotalPagamentoExcedente - PValorCustoApoliceEmissao) * PTotalDescontoPago) / (PTotalPremioPago - PValorCustoApoliceEmissao)
''        Else
''            ValorDescontoRest = 0
''        End If
''
''    End If
''
''    'Obtendo Valor do Premio Tarifa''''''''''''''''''''''''''''''''''''''''''''''''''''''
''
''    ValorPremioTarifaRest = ValorRestituicao - ValorCustoApoliceRest - ValorAdicFracionamentoRest - ValorIOFRest + ValorDescontoRest
''
''    'Retornando Valores'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''
''    If ValorRestituicao >= 0 And vgsituacao <> "p" Then
''        PValorRestituicao = ValorRestituicao
''        PValorIOFRest = ValorIOFRest
''        PValorCustoApoliceRest = ValorCustoApoliceRest
''        PValorAdicFracionamentoRest = ValorAdicFracionamentoRest
''        PValorDescontoRest = ValorDescontoRest
''        PValorPremioTarifaRest = ValorPremioTarifaRest
''    Else
''        PValorRestituicao = 0
''        PValorIOFRest = 0
''        PValorCustoApoliceRest = 0
''        PValorAdicFracionamentoRest = 0
''        PValorDescontoRest = 0
''        PValorPremioTarifaRest = 0
''    End If
''
''End Sub

''Sub Obter_Composicao_Valor_Restituicao(ByVal PPropostaId As String, _
''                                       ByVal PValorRestituicao As Currency, _
''                                       ByVal PRamoId As String, _
''                                       ByVal PDtContratacao As String, _
''                                       ByVal PDiasUtilizacaoSeguro As Integer, _
''                                       ByVal PPrazoRestituicaoIntegral As Integer, _
''                                       ByVal PValorCustoApoliceEmissao As Currency, _
''                                       ByRef PValorCustoApoliceRest As Currency, _
''                                       ByRef PValorAdicFracionamentoRest As Currency, _
''                                       ByRef PValorDescontoRest As Currency, _
''                                       ByRef PValorIOFRest As Currency, _
''                                       ByRef PValorPremioTarifaRest As Currency, _
''                                       Optional ByVal PValorIOFRestInformado As Variant, _
''                                       Optional ByVal PValorCustoApoliceRestInformado As Variant)
''
''
''
''    Dim ValorRestituicao As Currency
''    Dim ValorCustoApoliceRest As Currency
''    Dim ValorAdicFracionamentoRest As Currency
''    Dim ValorDescontoRest As Currency
''    Dim ValorIOFRest As Currency
''    Dim ValorPremioTarifaRest As Currency
''
''    Dim FatDescontoEmissao As Currency
''    Dim FatAdicFracionamentoEmissao As Currency
''
''    Dim RestituicaoIntegral As Boolean
''
''    'Verificando se a restitui��o � intergral''''''''''''''''''''''''''''''''''''''''''''''
''
''    If PDiasUtilizacaoSeguro <= PPrazoRestituicaoIntegral Then
''       RestituicaoIntegral = True
''    Else
''       RestituicaoIntegral = False
''    End If
''
''    'Obtendo Valor do IOF e Custo Apolice da Restitui��o''''''''''''''''''''''''''''''''''
''
''    If RestituicaoIntegral = True Then
''
''       If IsMissing(PValorIOFRestInformado) = True Then
''          Call Obtem_Valor_IOF(PValorRestituicao, PDtContratacao, PRamoId, ValorIOFRest)
''       Else
''          ValorIOFRest = PValorIOFRestInformado
''       End If
''
''       If IsMissing(PValorCustoApoliceRestInformado) = True Then
''          ValorCustoApoliceRest = PValorCustoApoliceEmissao
''       Else
''          ValorCustoApoliceRest = PValorCustoApoliceRestInformado
''       End If
''
''    Else
''
''       If IsMissing(PValorIOFRestInformado) = True Then
''           ValorIOFRest = 0
''       Else
''           ValorIOFRest = PValorIOFRestInformado
''       End If
''
''       If IsMissing(PValorCustoApoliceRestInformado) = True Then
''           ValorCustoApoliceRest = 0
''       Else
''           ValorCustoApoliceRest = PValorCustoApoliceRestInformado
''       End If
''
''    End If
''
''    'Obtendo Valor do Adicional de Fracionamento e Desconto''''''''''''''''''''''''''''''
''
''    Call Obter_Tx_Adic_Fracionamento(PPropostaId, FatAdicFracionamentoEmissao)
''    ValorAdicFracionamentoRest = Trunca((PValorRestituicao - ValorCustoApoliceRest - ValorIOFRest) - ((PValorRestituicao - ValorCustoApoliceRest - ValorIOFRest) / FatAdicFracionamentoEmissao))
''
''    'ValorDescontoRest = (PValorRestituicao - ValorCustoApoliceRest - ValorAdicFracionamentoRest - ValorIOFRest) * FatDescontoEmissao
''
''    'Obtendo Valor do Premio Tarifa''''''''''''''''''''''''''''''''''''''''''''''''''''''
''
''    ValorPremioTarifaRest = PValorRestituicao - ValorCustoApoliceRest - ValorAdicFracionamentoRest - ValorIOFRest + ValorDescontoRest
''
''    'Gerando sa�da dos valores calculados''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''
''    PValorCustoApoliceRest = ValorCustoApoliceRest
''    PValorAdicFracionamentoRest = ValorAdicFracionamentoRest
''    PValorDescontoRest = ValorDescontoRest
''    PValorIOFRest = ValorIOFRest
''    PValorPremioTarifaRest = ValorPremioTarifaRest
''
''End Sub
''
''Sub Obter_Composicao_Valor_Restituicao_2(ByVal PPropostaId As String, _
''                                         ByVal PValorPremioTarifaRest As Currency, _
''                                         ByVal PRamoId As String, _
''                                         ByVal PDtContratacao As String, _
''                                         ByVal PDtIniVigencia As String, _
''                                         ByVal PDiasUtilizacaoSeguro As Integer, _
''                                         ByVal PPrazoRestituicaoIntegral As Integer, _
''                                         ByVal PValorCustoApoliceEmissao As Currency, _
''                                         ByVal PValorPremioTarifaCalc As Currency, _
''                                         ByVal PValorDescontoCalc As Currency, _
''                                         ByRef PValorCustoApoliceRest As Currency, _
''                                         ByRef PValorAdicFracionamentoRest As Currency, _
''                                         ByRef PValorDescontoRest As Currency, _
''                                         ByRef PValorIOFRest As Currency, _
''                                         ByRef PValorRestituicao As Currency, _
''                                         ByVal bApoliceAberta As Boolean)
''
''Dim ValorRestituicao As Currency
''Dim ValorCustoApoliceRest As Currency
''Dim ValorAdicFracionamentoRest As Currency
''Dim ValorDescontoRest As Currency
''Dim ValorIOFRest As Currency
''Dim ValorPremioTarifaRest As Currency
''
''Dim RestituicaoIntegral As Boolean
''
''Dim PercIOF As Currency
''Dim FatAdicFracionamentoEmissao As Currency
''
''Dim TotDescPago As Currency
''
''    'Verificando se a restitui��o � integral''''''''''''''''''''''''''''''''''''''''''''''
''
''    If PDiasUtilizacaoSeguro <= PPrazoRestituicaoIntegral Then
''       RestituicaoIntegral = True
''    Else
''       RestituicaoIntegral = False
''    End If
''
''    'Recalculando valor do desconto e do adicional de fracionamento'''''''''''''''''''''''
''
''    If PValorPremioTarifaCalc <> 0 Then
''       ValorDescontoRest = Trunca((PValorDescontoCalc * PValorPremioTarifaRest) / PValorPremioTarifaCalc)
''    Else
''       Call Obter_Totais_Pagos(0, 0, 0, TotDescPago, 0)
''       If TotDescPago = 0 Then
''          ValorDescontoRest = 0
''       Else
''          MsgBox "N�o foi poss�vel recalcular o valor do desconto. A aplica��o ser� cancelada!"
''          End
''       End If
''    End If
''
''    Call Obter_Tx_Adic_Fracionamento(PPropostaId, FatAdicFracionamentoEmissao)
''    If FatAdicFracionamentoEmissao <> 0 Then
''         ValorAdicFracionamentoRest = Trunca(((PValorPremioTarifaRest - ValorDescontoRest) * FatAdicFracionamentoEmissao) - (PValorPremioTarifaRest - ValorDescontoRest))
''    Else
''         ValorAdicFracionamentoRest = 0
''    End If
''
''    'Obtendo Valor do IOF e Custo Apolice da Restitui��o''''''''''''''''''''''''''''''''''
''
''    If RestituicaoIntegral = True Then
''
''         ValorCustoApoliceRest = PValorCustoApoliceEmissao
''
''         Call Obtem_Perc_IOF(Data_Sistema, PRamoId, PercIOF)
''         ValorIOFRest = Trunca((PValorPremioTarifaRest + ValorCustoApoliceRest + ValorAdicFracionamentoRest - ValorDescontoRest) * PercIOF)
''
''    Else
''
''         ValorCustoApoliceRest = 0
''         ValorIOFRest = 0
''
''    End If
''
''    'Recalculando valor da restitui��o'''''''''''''''''''''''''''''''''''''''''''''''''''''
''
''    If vgsituacao <> "p" Or bApoliceAberta Then
''        ValorRestituicao = PValorPremioTarifaRest + ValorCustoApoliceRest + ValorAdicFracionamentoRest - ValorDescontoRest + ValorIOFRest
''    Else
''        ValorRestituicao = 0
''    End If
''
''    'Gerando sa�da dos valores calculados''''''''''''''''''''''''''''''''''''''''''''''''''
''
''    PValorCustoApoliceRest = ValorCustoApoliceRest
''    PValorAdicFracionamentoRest = ValorAdicFracionamentoRest
''    PValorDescontoRest = ValorDescontoRest
''    PValorIOFRest = ValorIOFRest
''    PValorRestituicao = ValorRestituicao
''
''End Sub

''Sub Obtem_Perc_IOF(ByVal PDtReferencia As String, _
''                   ByVal PRamo As String, _
''                   ByRef PPercIOF As Currency)
''
''Dim rc As rdoResultset
''Dim SQL As String
''
''DtReferencia = Format(PDtReferencia, "yyyymmdd")
''
'''Obtendo percentual de IOF
''
''SQL = "SELECT valor "
''SQL = SQL & "From "
''SQL = SQL & "     val_item_financeiro_ramo_tb "
''SQL = SQL & "Where "
''SQL = SQL & "     ramo_id = " & PRamo
''SQL = SQL & "     and ltrim(rtrim(cod_item_financeiro)) = 'IOF'"
''
''SQL = SQL & " and dt_inicio_vigencia <= '" & DtReferencia & "'"
''SQL = SQL & " and (dt_fim_vigencia >= '" & DtReferencia & "' or dt_fim_vigencia is Null)"
''
''Set rc = rdocn.OpenResultset(SQL)
''
''If Not rc.EOF Then
''   PPercIOF = Val(rc!valor)
''Else
''   MensagemBatch "Percentual de IOF n�o encontrado. o Programa ser� cancelado."
''   End
''End If
''
''Exit Sub
''
''TrataErro:
''   TrataErroGeral "Obtem_Perc_IOF", "Local.bas"
''   End
''
''End Sub

Function Obter_Dados_Produto(ByVal PProduto, _
                             ByRef PPrazoRestituicaoIntegral, _
                             ByRef PTpRestituicao, _
                             ByRef PPossuiSubvencao)

Dim SQL As String
Dim Rst As rdoResultset

SQL = "Select "
SQL = SQL & "   prazo_restituicao_integral, "
SQL = SQL & "   tp_calc_restituicao, "
SQL = SQL & "   possui_subvencao "
SQL = SQL & "From "
SQL = SQL & "   produto_tb "
SQL = SQL & "Where "
SQL = SQL & "   produto_id = " & PProduto

Set Rst = rdocn.OpenResultset(SQL)


PPrazoRestituicaoIntegral = Rst!Prazo_Restituicao_Integral
PTpRestituicao = Rst!Tp_Calc_Restituicao
PPossuiSubvencao = Rst!Possui_subvencao   'Rmarins - 20/10/06
Rst.Close

End Function

''Function Obter_Valor_Devolucao_Cliente(ByVal PVlRestituicaoBruto As Currency, _
''                                       ByVal PVlIOF As Currency, _
''                                       ByVal PVLCustoApoliceCertif As Currency, _
''                                       ByVal PRestituicaoIntergral As Boolean) As Currency
''
''Dim VlDevolucao As Currency
''
'''Calculando valores a recuperar''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''
''If PRestituicaoIntergral = True Then
''   VlDevolucao = PVlRestituicaoBruto
''Else
''   VlDevolucao = PVlRestituicaoBruto - PVlIOF - PVLCustoApoliceCertif
''End If
''
''Obter_Valor_Devolucao_Cliente = VlDevolucao
''
''End Function

''Sub Obter_Valores_Avaliacao_Tecnica(ByVal PProposta As String, _
''                                    ByRef PValCustoApoliceCertifRest As Currency, _
''                                    ByRef PValAdicFracionamento_Rest As Currency, _
''                                    ByRef PValDescontoRest As Currency, _
''                                    ByRef PValIOFRest As Currency, _
''                                    ByRef PValPremioTarifaRest As Currency, _
''                                    ByRef PValRestituicao As Currency, _
''                                    ByRef PValRestituicaoCalc As Currency, _
''                                    ByRef PValTotCorretagem As Currency, _
''                                    ByRef PValProLabore As Currency, _
''                                    ByRef PValCustoApoliceCertifCalc As Currency, _
''                                    ByRef PValAdicFracionamentoCalc As Currency, _
''                                    ByRef PValDescontoCalc As Currency, _
''                                    ByRef PValIOFCalc As Currency, _
''                                    ByRef PValPremioTarifaCalc As Currency, _
''                                    ByRef PValTotCorretagemCalc As Currency, _
''                                    ByRef PValProLaboreCalc As Currency)
''
''
''Dim SQL As String
''Dim Rst As rdoResultset
''
''SQL = " SELECT "
''SQL = SQL & "val_Restituicao_Estimado, "
''SQL = SQL & "val_Restituicao, "
''SQL = SQL & "custo_apolice, "
''SQL = SQL & "val_IOF, "
''SQL = SQL & "val_adic_fracionamento,"
''SQL = SQL & "val_desconto_comercial,"
''SQL = SQL & "val_premio_tarifa, "
''SQL = SQL & "Val_Comissao, "
''SQL = SQL & "val_comissao_estipulante, "
''SQL = SQL & "custo_apolice_estimado, "
''SQL = SQL & "val_IOF_estimado, "
''SQL = SQL & "val_Adic_fracionamento_estimado,"
''SQL = SQL & "val_desconto_comercial_estimado,"
''SQL = SQL & "val_premio_tarifa_estimado, "
''SQL = SQL & "Val_Comissao_estimado, "
''SQL = SQL & "val_comissao_estipulante_estimado "
''SQL = SQL & " FROM "
''SQL = SQL & "       avaliacao_cancelamento_tb ac "
''SQL = SQL & " WHERE "
''SQL = SQL & "       ac.proposta_id = " & PProposta
'''SQL = SQL & "       AND ac.situacao = 'p'"
''SQL = SQL & "       AND ac.num_avaliacao = (SELECT max(num_avaliacao)"
''SQL = SQL & "                               FROM avaliacao_cancelamento_tb ac2 "
''SQL = SQL & "                               WHERE ac2.proposta_id = ac.proposta_id) "
''
''Set Rst = rdocn.OpenResultset(SQL)
''
''PValRestituicaoCalc = Val(Rst!Val_Restituicao_Estimado)
''
'''If vgsituacao = "p" Then
'''    PValRestituicao = 0
'''    PValCustoApoliceCertifRest = 0
'''Else
''PValRestituicao = Val(Rst!Val_Restituicao)
''PValCustoApoliceCertifRest = Val(Rst!Custo_Apolice)
'''End If
''
''PValIOFRest = Val(Rst!Val_IOF)
''PValAdicFracionamento_Rest = Val(Rst!Val_Adic_Fracionamento)
''PValDescontoRest = Val(Rst!val_desconto_comercial)
''PValPremioTarifaRest = Val(Rst!val_premio_tarifa)
''PValTotCorretagem = Val(Rst!Val_Comissao)
''PValProLabore = Val(Rst!val_comissao_estipulante)
''
''PValCustoApoliceCertifCalc = Val(Rst!Custo_Apolice_estimado)
''PValIOFCalc = Val(Rst!Val_IOF_estimado)
''PValAdicFracionamentoCalc = Val(Rst!Val_Adic_Fracionamento_estimado)
''PValDescontoCalc = Val(Rst!val_desconto_comercial_estimado)
''PValPremioTarifaCalc = Val(Rst!val_premio_tarifa_estimado)
''PValTotCorretagemCalc = Val(Rst!Val_Comissao_estimado)
''PValProLaboreCalc = Val(Rst!val_comissao_estipulante_estimado)
''
''Rst.Close
''
''End Sub

''Sub Aprovar_Avaliacao_Tecnica(ByVal PPropostaId As String, _
''                              ByVal PNumEndossoBB As String)
''Dim SQL As String
''Dim Rst As rdoResultset
''
'''CREATE PROCEDURE aprovacao_avaliacao_canc_spu
'''@proposta_id            numeric(9,0),
'''@num_endosso_bb         numeric(9,0),
'''@dt_avaliacao_gerencial     smalldatetime,
'''@usuario_avaliacao_gerencial    varchar(20),
'''@usuario            varchar(20)
''
'''SQL = Ambiente & ".aprovacao_avaliacao_canc_spu "
''SQL = "Exec aprovacao_avaliacao_canc_spu "
''SQL = SQL & PPropostaId
''SQL = SQL & "," & PNumEndossoBB
''SQL = SQL & ",'" & Format(Data_Sistema, "yyyymmdd") & "'"
''SQL = SQL & ",'" & cUserName & "'"
''SQL = SQL & ",'" & cUserName & "'"
''
''Set Rst = rdocn.OpenResultset(SQL)
''Rst.Close
''
''End Sub

''Sub Carregar_Rateio_Corretagem(ByVal PProposta, _
''                               ByVal PDtInicio, _
''                               ByVal PValFinanceiro, _
''                               ByVal PValIOF, _
''                               ByVal PValCustoApoliceCertif, _
''                               ByVal PPercIr)
''
''   'Declara��es''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''
''   Dim ValBase As Currency
''   Dim DtInicio As String
''   Dim DtFim As String
''   Dim Corretagem As Corretagem
''
''   Dim ValIR As Currency
''   Dim ValComissao As Currency
''
''   On Error GoTo TrataErro
''
''   'Inicializando cole��o''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''
''   Set Corretagens = Nothing
''
''   'Obtendo Valor Base de Reteio'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''
''   ValBase = PValFinanceiro - PValIOF - PValCustoApoliceCertif
''
''   DtInicio = Format(PDtInicio, "yyyymmdd")
''   DtFim = Format(PDtFim, "yyyymmdd")
''
''   'Selecionando dados da corretagem considerando e o corretor seja pessoa jur�dica
''
''   SQL = "SELECT  a.corretor_id, " & vbNewLine
''   SQL = SQL & "  b.sucursal_corretor_id, " & vbNewLine
''   SQL = SQL & "  isnull(b.perc_corretagem,0) perc_corretagem, " & vbNewLine
''   SQL = SQL & "  c.nome " & vbNewLine
''   SQL = SQL & "  FROM corretagem_tb a, " & vbNewLine
''   SQL = SQL & "  corretagem_pj_tb  b, " & vbNewLine
''   SQL = SQL & "  corretor_tb c " & vbNewLine
''   SQL = SQL & " WHERE" & vbNewLine
''   SQL = SQL & " a.proposta_id = b.proposta_id" & vbNewLine
''   SQL = SQL & " and a.corretor_id = b.corretor_id" & vbNewLine
''   SQL = SQL & " and a.dt_inicio_corretagem = b.dt_inicio_corretagem " & vbNewLine
''   SQL = SQL & " and a.corretor_id = c.corretor_id " & vbNewLine
''   SQL = SQL & " and a.proposta_id = " & PProposta & vbNewLine
''
''   'Alterado at� que seja definido um crit�rio para a data de in�cio de corretagem - arodrigues 07/04/2003
''
''   'SQL = SQL & " and a.dt_inicio_corretagem <= '" & DtInicio & "'"
''   'SQL = SQL & " and (a.dt_fim_corretagem >= '" & DtInicio & "' or a.dt_fim_corretagem is Null)"
''
''   SQL = SQL & " and a.dt_inicio_corretagem = (SELECT MIN(dt_inicio_corretagem) " & vbNewLine
''   SQL = SQL & "                                 FROM corretagem_tb d " & vbNewLine
''   SQL = SQL & "                                WHERE d.Proposta_Id = a.Proposta_Id " & vbNewLine
''   SQL = SQL & "                                  AND d.corretor_id = a.corretor_id) " & vbNewLine
''
''   Set rc = rdocn.OpenResultset(SQL)
''
''   If rc.EOF Then
''
''      rc.Close
''
''      'Selecionando dados da corretagem considerando e o corretor seja pessoa f�sica
''
''      SQL = "SELECT  a.corretor_id, "
''      'SQL = SQL & "  b.sucursal_corretor_id, "
''      SQL = SQL & "  isnull(b.perc_corretagem,0) perc_corretagem, "
''      SQL = SQL & "  c.nome "
''
''      SQL = SQL & "  FROM corretagem_tb a, "
''      SQL = SQL & "  corretagem_pf_tb  b, "
''      SQL = SQL & "  corretor_tb c "
''
''      SQL = SQL & " Where"
''      SQL = SQL & " a.proposta_id = b.proposta_id"
''      SQL = SQL & " and a.corretor_id = b.corretor_id"
''      SQL = SQL & " and a.dt_inicio_corretagem = b.dt_inicio_corretagem "
''      SQL = SQL & " and a.corretor_id = c.corretor_id "
''
''      SQL = SQL & " and a.proposta_id = " & PProposta
''
''      'Alterado at� que seja definido um crit�rio para a data de in�cio de corretagem - arodrigues 07/04/2003
''
''      'SQL = SQL & " and a.dt_inicio_corretagem <= '" & DtInicio & "'"
''      'SQL = SQL & " and (a.dt_fim_corretagem >= '" & DtInicio & "' or a.dt_fim_corretagem is Null)"
''
''      SQL = SQL & " and a.dt_inicio_corretagem = (SELECT MIN(dt_inicio_corretagem) " & vbNewLine
''      SQL = SQL & "                                 FROM corretagem_tb d " & vbNewLine
''      SQL = SQL & "                                WHERE d.Proposta_Id = a.Proposta_Id " & vbNewLine
''      SQL = SQL & "                                  AND d.corretor_id = a.corretor_id) " & vbNewLine
''
''      Set rc = rdocn.OpenResultset(SQL)
''
''   End If
''
''   Do While Not rc.EOF
''
''        PercCorretagem = Val(rc!perc_corretagem)
''        ValComissao = Trunca(ValBase * (PercCorretagem / 100))
''        ValIR = Trunca(ValComissao * PPercIr)
''
''        Set Corretagem = New Corretagem
''
''        Corretagem.Corretor_Id = rc!Corretor_Id
''        Corretagem.PercComissao = Val(rc!perc_corretagem)
''        Corretagem.Nome = rc!Nome
''        Corretagem.Val_Comissao = ValComissao
''        Corretagem.Val_IR = ValIR
''        Corretagens.Add Corretagem, CStr(rc!Corretor_Id)
''
''        rc.MoveNext
''
''   Loop
''
''   rc.Close
''
''   Exit Sub
''
''TrataErro:
''   TrataErroGeral "Carregar_Rateio_Corretagem", "FrmAvaliacao"
''   End
''
''End Sub

''Sub Ratear_ValRestituicao_Corretagem(ByVal PValFinanceiro As Currency, _
''                                     ByVal PValCustoApoliceCertif As Currency, _
''                                     ByVal PValIOF As Currency, _
''                                     ByVal PPercIr As Currency)
''
''   'Declara��es''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''
''   Dim ValBase As Currency
''   Dim PercCorretagem As Currency
''   Dim ValComissao As Currency
''   Dim ValIR As Currency
''   Dim Corretagem As Corretagem
''
''   'Obtendo Valor Base de Reteio'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''
''   ValBase = PValFinanceiro - PValIOF - PValCustoApoliceCertif
''
''   For Each Corretagem In Corretagens
''
''        PercCorretagem = Corretagem.PercComissao
''        ValComissao = Trunca(ValBase * (PercCorretagem / 100))
''
''        ValIR = Trunca(ValComissao * PPercIr)
''
''        Corretagem.Val_Comissao = ValComissao
''        Corretagem.Val_IR = ValIR
''
''   Next
''
''End Sub

''Sub Ratear_Valor_Corretagem(ByVal PValTotCorretagem As Currency, PPercIr)
''
''   'Declara��es''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''
''   Dim Corretagem As Corretagem
''   Dim PercCorretagem As Currency
''   Dim PerCorretagemAux As Currency
''   Dim TotPercCorretagem As Currency
''   Dim ValComissao As Currency
''   Dim TotValCorretagemAux As Currency
''   Dim ValIR As Currency
''   Dim DiferencaRateio As Currency
''
''   Dim ChaveBusca As String
''
''   'Obtendo percentual total de corretagem e IR''''''''''''''''''''''''''''''''''''''''''''
''
''   For Each Corretagem In Corretagens
''        PercCorretagem = Corretagem.PercComissao
''        TotPercCorretagem = TotPercCorretagem + PercCorretagem
''   Next
''
''   'Distribuindo valores'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''
''   For Each Corretagem In Corretagens
''
''        PercCorretagem = Corretagem.PercComissao
''        PerCorretagemAux = Trunca((PercCorretagem * 100) / TotPercCorretagem)
''
''        ValComissao = Trunca(PValTotCorretagem * (PerCorretagemAux / 100))
''        TotValCorretagemAux = TotValCorretagemAux + ValComissao
''
''        ValIR = Trunca(ValComissao * PPercIr)
''
''        Corretagem.Val_Comissao = ValComissao
''        Corretagem.Val_IR = ValIR
''
''        ChaveBusca = Corretagem.Corretor_Id
''   Next
''
''   'Atribuindo diferen�a do rateio sobre o �ltimo corretor
''
''   DiferencaRateio = PValTotCorretagem - TotValCorretagemAux
''
''   If IsNull(ChaveBusca) = False And ChaveBusca <> "" Then
''
''      Set Corretagem = Corretagens(CStr(ChaveBusca))
''
''      ValComissao = Corretagem.Val_Comissao
''      ValComissao = ValComissao + DiferencaRateio
''
''      ValIR = Trunca(ValComissao * PPercIr)
''
''      Corretagem.Val_Comissao = ValComissao
''      Corretagem.Val_IR = ValIR
''
''   End If
''
''End Sub

''Public Sub Inclui_Mov_Item_Financeiro(ByVal PItemFinanceiro, _
''                                      ByVal PDtMovimentacao, _
''                                      ByVal PValMovimentacao, _
''                                      ByVal PTpOperacao, _
''                                      ByVal PTpEspecializacao, _
''                                      ByVal PMovimentacaoId, _
''                                      ByVal PPropostaId, _
''                                      ByVal PEndossoId, _
''                                      ByVal bFlagRetemIOF As Boolean)
''
''   Dim rc   As rdoResultset
''   Dim sRetidoBanco As String
''
''   On Error GoTo TrataErro
''
''   'Retido_Banco = "n"
''
''   'pcarvalho - 17/06/2003 Padroniza��o do c�lculo de restitui��o
''   If bFlagRetemIOF Then
''     sRetidoBanco = "s"
''   Else
''     sRetidoBanco = "n"
''   End If
''
''   SQL = "exec ps_mov_item_financeiro_spi "
''   SQL = SQL & PMovimentacaoId & ","
''   SQL = SQL & "'" & PItemFinanceiro & "',"
''   SQL = SQL & "'" & PTpOperacao & "',"
''   SQL = SQL & "'" & Format(PDtMovimentacao, "yyyymmdd") & "',"
''   SQL = SQL & MudaVirgulaParaPonto(PValMovimentacao) & ","
''   SQL = SQL & "'" & sRetidoBanco & "',"
''   SQL = SQL & PPropostaId & ","
''   SQL = SQL & "0" & ","
''   SQL = SQL & PEndossoId & ","
''   SQL = SQL & "'" & PTpEspecializacao & "',"   ' tipo especializacao 1 - baixa  2 - endosso
''   SQL = SQL & "'" & cUserName & "'"
''
''   Set rc = rdocn.OpenResultset(SQL)
''
''   rc.Close
''
''   Exit Sub
''
''TrataErro:
''    TrataErroGeral "Inclui_Mov_item_IOF", "Local.bas"
''    Call TerminaSEGBR
''
''End Sub

''Sub Liberar_Retorno_BB(ByVal POrigem As String, _
''                       ByVal PSequencialEmi As String, _
''                       ByVal PSituacaoRet As String)
''
''SQL = "Exec habilita_reenvio_ret_bb_spu "
''SQL = SQL & POrigem
''SQL = SQL & "," & PSequencialEmi
''SQL = SQL & ",'" & PSituacaoRet & "'"
''SQL = SQL & ",'" & cUserName & "'"
''
''Set Rst = rdocn.OpenResultset(SQL)
''
''Exit Sub
''
''End Sub

''Function Valida_Dados_Endosso(ByVal PTpOperacao, _
''                              ByVal PNumProposta, _
''                              ByVal PProdutoId, _
''                              ByVal PDtCancelamentoBB, _
''                              ByVal PDtContratacao, _
''                              ByVal PDtIniVigencia, _
''                              ByVal PDtFimVigencia, _
''                              ByVal PRamoId, _
''                              ByVal PNrEndossoBB, _
''                              ByVal PTipoProcesso) As Boolean
''
''Valida_Dados_Endosso = False
''
'''Verificando viabilidade do cancelamento''''''''''''''''''''''''''''''''''''''''''''''''''
''
''If Trim(PNumProposta) = "" Then
''   MsgBox "A proposta do endosso n�o foi encontrada! N�o � possivel realizar este endosso."
''   Exit Function
''End If
''
''If Verificar_Existencia_Avaliacao_Pendente(PNumProposta, PTpOperacao) = True Then
''    MsgBox "Endosso j� avaliado."
''    FrmSelEndosso.Marcar_Linha_Processada
''    Exit Function
''End If
''
''If Verificar_Uso_Proposta(PNumProposta) = True Then
''    MsgBox "A proposta do endosso est� sendo utilizada por outro usu�rio"
''    Exit Function
''End If
''
''If Validar_Tipo_Restituicao(PProdutoId) = False Then
''   MsgBox "Tipo de restitui��o do produto encontra-se inv�lido ou n�o informado."
''   Exit Function
''End If
''
''If Validar_Prazo_Restituicao_Integral(PProdutoId) = False Then
''   MsgBox "Prazo de restituicao integral do produto encontra-se inv�lido ou n�o informado."
''   Exit Function
''End If
''
''If Trim(PTipoProcesso) = "" Then
''   MsgBox "Tipo de Processo inv�lido ou n�o informado."
''   Exit Function
''End If
''
''If IsDate(PDtCancelamentoBB) = False Then
''   If Trim(PTipoProcesso) = "C" Then
''        MsgBox "Data do cancelamento BB inv�lida ou n�o informada."
''   Else
''        MsgBox "Data de liquida��o inv�lida ou n�o informada."
''   End If
''   Exit Function
''End If
''
'''Jorfilho 15-mai-2003: Com a inclus�o de produtos com ap�lice aberta,
'''passar a validar cancelamento sem n�o houver fim de vig�ncia
''If Trim(PDtFimVigencia) <> "" Then
''  If Val(Format(PDtCancelamentoBB, "yyyymmdd")) > Val(Format(PDtFimVigencia, "yyyymmdd")) Then
''    If Trim(PTipoProcesso) = "C" Then
''        MsgBox "Data de cancelamento: " & PDtCancelamentoBB & " superior a data de fim de vigencia:" & PDtFimVigencia
''    Else
''        MsgBox "Data de liquida��o: " & PDtCancelamentoBB & " superior a data de fim de vigencia:" & PDtFimVigencia
''    End If
''    bDtCancelamentoValida = False
''  Else
''    bDtCancelamentoValida = True
''  End If
''Else
''  bDtCancelamentoValida = True
''End If
''
'''If Verificar_Cadastramento_IOF(PDtIniVigencia, PRamoId) = False Then
''If Verificar_Cadastramento_IOF(Data_Sistema, PRamoId) = False Then
''   MsgBox "IOF vigente na data de contrata��o: " & PDtIniVigencia & " n�o cadastrado."
''   Exit Function
''End If
''
''If Verificar_Cadastramento_IR(Data_Sistema, PRamoId) = False Then
''   MsgBox "IR vigente na data de contrata��o: " & PDtIniVigencia & " n�o cadastrado."
''   Exit Function
''End If
''
'''If Verificar_Ocorrencia_Sinistro(PNumProposta) = True Then
'''   MsgBox "A proposta selecionada possui sinistro ocorrido."
'''   Exit Function
'''End If
''
''If Trim(PNrEndossoBB) = "" Then
''   MsgBox "Nr do Endosso BB inv�lido ou n�o informado."
''   Exit Function
''End If
''
''Valida_Dados_Endosso = True
''
''End Function

''Function Verificar_Cadastramento_IOF(ByVal PDtReferencia As String, _
''                                     ByVal PRamo As String) As Boolean
''
''Dim rc As rdoResultset
''Dim SQL As String
''
''DtReferencia = Format(PDtReferencia, "yyyymmdd")
''
'''Obtendo percentual de IOF
''
''SQL = "SELECT * "
''SQL = SQL & "From "
''SQL = SQL & "     val_item_financeiro_ramo_tb "
''SQL = SQL & "Where "
''SQL = SQL & "     ramo_id = " & PRamo
''SQL = SQL & "     and ltrim(rtrim(cod_item_financeiro)) = 'IOF'"
''
''SQL = SQL & " and dt_inicio_vigencia <= '" & DtReferencia & "'"
''SQL = SQL & " and (dt_fim_vigencia >= '" & DtReferencia & "' or dt_fim_vigencia is Null)"
''
''Set rc = rdocn.OpenResultset(SQL)
''
''If Not rc.EOF Then
''   Verificar_Cadastramento_IOF = True
''Else
''   Verificar_Cadastramento_IOF = False
''End If
''
''Exit Function
''
''TrataErro:
''   TrataErroGeral "Verificar_Cadastramento_IOF", "FrmSelEndosso"
''   End
''
''End Function

''Function Verificar_Cadastramento_IR(ByVal PDtReferencia As String, _
''                                    ByVal PRamo As String) As Boolean
''
''Dim rc As rdoResultset
''Dim SQL As String
''
''DtReferencia = Format(PDtReferencia, "yyyymmdd")
''
'''Obtendo percentual de IOF
''
''SQL = "SELECT * "
''SQL = SQL & "From "
''SQL = SQL & "     val_item_financeiro_ramo_tb "
''SQL = SQL & "Where "
''SQL = SQL & "     ramo_id = " & PRamo
''SQL = SQL & "     and ltrim(rtrim(cod_item_financeiro)) = 'IR'"
''
''SQL = SQL & " and dt_inicio_vigencia <= '" & DtReferencia & "'"
''SQL = SQL & " and (dt_fim_vigencia >= '" & DtReferencia & "' or dt_fim_vigencia is Null)"
''
''Set rc = rdocn.OpenResultset(SQL)
''
''If Not rc.EOF Then
''   Verificar_Cadastramento_IR = True
''Else
''   Verificar_Cadastramento_IR = False
''End If
''
''Exit Function
''
''TrataErro:
''   TrataErroGeral "Verificar_Cadastramento_IR", "FrmSelEndosso"
''   End
''
''End Function

''Function Verificar_Ocorrencia_Sinistro(ByVal PProposta As String) As Boolean
''
''Dim rc As rdoResultset
''Dim SQL As String
''
''SQL = "SELECT * "
''SQL = SQL & "From "
''SQL = SQL & "     sinistro_tb "
''SQL = SQL & "Where "
''SQL = SQL & "     proposta_id = " & PProposta
''
''Set rc = rdocn.OpenResultset(SQL)
''
''If Not rc.EOF Then
''   Verificar_Ocorrencia_Sinistro = True
''Else
''   Verificar_Ocorrencia_Sinistro = False
''End If
''
''rc.Close
''
''Exit Function
''
''TrataErro:
''   TrataErroGeral "Verificar_Ocorrencia Sinistro", "FrmSelEndosso"
''   End
''
''End Function

''Sub Cancelar_Proposta(PProposta As String, pCodCanc As String, PEndossoId As String, PDtCancelamentoBB)
''
''Dim SQL As String
''Dim MotivoCanc As String
''Dim rs As rdoResultset
''
''On Error GoTo Erro
''
'''Alterando status da proposta para "c" '''''''''''''''''''''''''''''''''''''''''''''''''''
''
''SQL = "exec " & Ambiente & ".situacao_proposta_spu " & PProposta
''SQL = SQL & ", 'c', '" & cUserName & "'"
''
''Set rs = rdocn.OpenResultset(SQL)
''rs.Close
''
''MotivoCanc = "Cancelamento a Pedido do Segurado"
''
'''Incluindo cancelamento''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''
''SQL = "exec " & Ambiente & ".cancelamento_proposta_spi '" & Format(Data_Sistema, "yyyymmdd") & "'"
''SQL = SQL & ", " & PProposta & ", '" & MotivoCanc & "'"
''SQL = SQL & ", '" & Format(pCodCanc, "0") & "', null, 'n', null"
''SQL = SQL & ", '" & cUserName & "', '" & Format(PDtCancelamentoBB, "yyyymmdd") & "'"
''SQL = SQL & ", " & PEndossoId
''
''Set rs = rdocn.OpenResultset(SQL)
''rs.Close
''
''Exit Sub
''
''Erro:
''   TrataErroGeral "Cancela_Proposta", "Local.bas"
''   TerminaSEGBR
''End Sub


''Sub Incluir_Corretagem_Endosso_Financeiro(ByVal PProposta As String, _
''                                          ByVal PEndossoId As String, _
''                                          ByVal PCorretorId As String, _
''                                          ByVal PSucursalCorretorId As String, _
''                                          ByVal PPercCorretagem As String, _
''                                          ByVal PValComissao As String, _
''                                          ByVal PValIR As String)
''
''
''Dim rs As rdoResultset
''
'''On Error GoTo Trata_Erro:
''
'''# especializa��o de corretagem da fatura para corretor pessoa jur�dica
''
''SQL = "exec corretagem_pj_endosso_fin_spi "
''SQL = SQL & PProposta
''SQL = SQL & "," & PEndossoId
''SQL = SQL & "," & PCorretorId
''SQL = SQL & ",'" & PSucursalCorretorId & "'"
''SQL = SQL & "," & MudaVirgulaParaPonto(PPercCorretagem)
''SQL = SQL & "," & MudaVirgulaParaPonto(PValComissao)
''SQL = SQL & "," & MudaVirgulaParaPonto(PValIR)
''SQL = SQL & ",'" & cUserName & "'"
''
''Set rs = rdocn.OpenResultset(SQL)
''rs.Close
''
''Exit Sub
''
''Trata_Erro:
''    TrataErroGeral "Inclui Corretagem Endosso Financeiro"
''    mensagem_erro 6, "Erro na rotina Inclui Corretagem Endosso Financeiro."
''    End
''End Sub
''
''Sub Incluir_ProLabore_Endosso_Financeiro(ByVal PPropostaBasica As String, _
''                                         ByVal PEndossoId As String, _
''                                         ByVal PEstipulanteId, _
''                                         ByVal PPercProLabore)
''
''Dim i As Integer
''Dim SQL As String
''Dim rs As rdoResultset
''
'''On Error GoTo Trata_Erro
''
''SQL = "exec pro_labore_endosso_fin_spi "
''SQL = SQL & PPropostaBasica
''SQL = SQL & "," & PEndossoId
''SQL = SQL & "," & PEstipulanteId
''SQL = SQL & "," & MudaVirgulaParaPonto(PPercProLabore)
''SQL = SQL & ",'" & cUserName & "'"
''
''Set rs = rdocn.OpenResultset(SQL)
''rs.Close
''
''Exit Sub
''
''Trata_Erro:
''
''    TrataErroGeral "Inclui Pr�-Labore Endosso Financeiro"
''    mensagem_erro 6, "Erro na rotina Inclui Pr�-Labore Endosso Financeiro"
''    End
''
''End Sub
''
''Sub Incluir_Endosso_Financeiro(ByVal PPropostaId, _
''                                       ByVal PTpEndosso, _
''                                       ByVal PNumEndossoBB, _
''                                       ByVal PDtEmissao, _
''                                       ByVal PVlFinanceiro, _
''                                       ByVal PVlComissao, _
''                                       ByVal PVlComissaoEstip, _
''                                       ByVal PVlIOF, _
''                                       ByVal PVlIR, _
''                                       ByVal PVlIS, _
''                                       ByVal PVlDesconto, _
''                                       ByVal PVlAdicFracionamento, _
''                                       ByVal PCustoApoliceCertif, _
''                                       ByVal PVlPremioTarifa, _
''                                       ByVal PDtPedidoEndosso, _
''                                       ByVal PEndossoId)
''
'''On Error GoTo Trata_Erro
''
''Dim rc As rdoResultset
''
''SQL = "exec endosso_financ_spi "
''SQL = SQL & PPropostaId
''SQL = SQL & "," & PEndossoId
''SQL = SQL & ",'" & cUserName & "'"
''SQL = SQL & "," & MudaVirgulaParaPonto(PVlFinanceiro)
''SQL = SQL & ",Null"
''SQL = SQL & "," & MudaVirgulaParaPonto(PVlPremioTarifa)
''SQL = SQL & "," & MudaVirgulaParaPonto(PCustoApoliceCertif)
''SQL = SQL & "," & MudaVirgulaParaPonto(PVlComissao)
''SQL = SQL & "," & MudaVirgulaParaPonto(PVlComissaoEstip)
''SQL = SQL & "," & MudaVirgulaParaPonto(PVlIOF)
''SQL = SQL & "," & MudaVirgulaParaPonto(PVlIR)
''SQL = SQL & "," & MudaVirgulaParaPonto(PVlDesconto)
''SQL = SQL & "," & MudaVirgulaParaPonto(PVlAdicFracionamento)
''SQL = SQL & ",Null"
''SQL = SQL & ",'n'"
''SQL = SQL & ",790"
''SQL = SQL & ",790"
''SQL = SQL & ",1.00"
''SQL = SQL & "," & MudaVirgulaParaPonto(PVlIS)
''
''Set rc = rdocn.OpenResultset(SQL)
''rc.Close
''
''Exit Sub
''
''Trata_Erro:
''   TrataErroGeral "Inclui Endosso Financeiro"
''   MsgBox "Rotina : Inclui Endosso Financeiro "
''   End
''
''End Sub
''
''Sub Incluir_Mov_Cliente(ByVal PDtMovimentacao, _
''                        ByVal PValorRestituicao, _
''                        ByVal PTpOperacao, _
''                        ByVal PTpEspecializacao, _
''                        ByRef PMovimentacaoId, _
''                        ByVal PClienteID, _
''                        ByVal PPropostaId, _
''                        ByVal PEndossoId)
''
''   Dim rc   As rdoResultset
''
''   'On Error GoTo TrataErro
''
''   Retido_Banco = "n"
''
''   SQL = "exec ps_mov_cliente_spi "
''   SQL = SQL & IIf(Val(Trim(PMovimentacaoId)) = 0, "NULL", PMovimentacaoId) & ","
''   SQL = SQL & PClienteID & ","
''   SQL = SQL & "'" & PTpOperacao & "',"
''   SQL = SQL & "'" & Format(PDtMovimentacao, "yyyymmdd") & "',"
''   SQL = SQL & MudaVirgulaParaPonto(PValorRestituicao) & ","
''   SQL = SQL & "'" & Retido_Banco & "',"
''   SQL = SQL & PPropostaId & ","
''   SQL = SQL & "0" & ","
''   SQL = SQL & PEndossoId & ","
''   SQL = SQL & "'" & PTpEspecializacao & "',"
''   SQL = SQL & "'" & cUserName & "',"
''   SQL = SQL & "'n'"
''
''   Set rc = rdocn.OpenResultset(SQL)
''
''   If Val(Trim(PMovimentacaoId)) = 0 Then
''        PMovimentacaoId = rc(0)
''   End If
''
''   rc.Close
''
''   Exit Sub
''
''Trata_Erro:
''   TrataErroGeral "Inclui Mov Cliente"
''   Linha = "Rotina = Inclui Mov Cliente. Programa ser� Cancelado"
''   MsgBox Linha
''   End
''
''End Sub
''
''Sub Incluir_Mov_Proposta(ByVal PMovimentacaoId, _
''                         ByVal PPropostaId, _
''                         ByVal PTpOperacao, _
''                         ByVal PValorRestituicao, _
''                         ByVal PEndossoId, _
''                         ByVal PTpEspecializacao, _
''                         ByVal PPropostaBasica, _
''                         ByVal PValorIOF, _
''                         ByVal PValorCorretagem, _
''                         ByVal PValorProLabore, _
''                         ByVal PValorCustoApoliceCertif)
''
''   Dim rc As rdoResultset
''
''   On Error GoTo TrataErro
''
''   Retido_Banco = "n"
''
''   SQL = "exec ps_mov_proposta_spi "
''   SQL = SQL & PMovimentacaoId & ","
''   SQL = SQL & PPropostaId & ","
''   SQL = SQL & "'" & PTpOperacao & "',"
''   SQL = SQL & "'" & Format(Data_Sistema, "yyyymmdd") & "',"
''   SQL = SQL & MudaVirgulaParaPonto(PValorRestituicao) & ","
''   SQL = SQL & "'" & Retido_Banco & "',"
''   SQL = SQL & PPropostaBasica & ","
''   SQL = SQL & "0" & ","
''   SQL = SQL & PEndossoId & ","
''   SQL = SQL & "'" & PTpEspecializacao & "'" & ","
''   SQL = SQL & "'" & cUserName & "',"
''   SQL = SQL & MudaVirgulaParaPonto(PValorIOF) & ","
''   SQL = SQL & MudaVirgulaParaPonto(PValorCorretagem) & ","
''   SQL = SQL & MudaVirgulaParaPonto(PValorProLabore) & ","
''   SQL = SQL & MudaVirgulaParaPonto(PValorCustoApoliceCertif)
''
''   Set rc = rdocn.OpenResultset(SQL)
''   rc.Close
''
''   Exit Sub
''
''TrataErro:
''   TrataErroGeral "Inclui Mov Proposta"
''   Linha = "Rotina = Inclui Mov Proposta. Programa ser� Cancelado"
''   MsgBox Linha
''   End
''End Sub
''
''Sub Incluir_Mov_Corretor(ByVal PDtMovimentacao, _
''                         ByVal PValComissaoBruta, _
''                         ByVal PCorretorId, _
''                         ByVal PSucursalCorretorId, _
''                         ByVal PFlagTipoCorretor, _
''                         ByVal PValIR, _
''                         ByVal PPercComissao, _
''                         ByVal PTpOperacao, _
''                         ByVal PTpEspecializacao, _
''                         ByVal PPropostaId, _
''                         ByVal PEndossoId, _
''                         ByRef PMovimentacaoId, _
''                         ByVal bFlagRetemComissao As Boolean)
''
''    On Error GoTo Trata_Erro
''
''    Dim rc   As rdoResultset
''    'Const Retido_Banco = "s"
''    Dim sRetidoBanco As String
''
''    'pcarvalho - 17/06/2003 Padroniza��o do c�lculo de restitui��o
''    If bFlagRetemComissao Then
''      sRetidoBanco = "s"
''    Else
''      sRetidoBanco = "n"
''    End If
''
''    SQL = "exec ps_mov_corretor_spi "
''    SQL = SQL & PCorretorId & ","
''    SQL = SQL & "'" & PSucursalCorretorId & "',"
''    SQL = SQL & "'" & PTpOperacao & "',"
''    SQL = SQL & "'" & Format(PDtMovimentacao, "yyyymmdd") & "',"
''    SQL = SQL & MudaVirgulaParaPonto(PValComissaoBruta) & ","
''    SQL = SQL & "'" & sRetidoBanco & "',"
''    SQL = SQL & "'" & PFlagTipoCorretor & "',"
''    SQL = SQL & PPropostaId & ","
''    SQL = SQL & "0" & ","
''    SQL = SQL & PEndossoId & ","
''    SQL = SQL & "'" & PTpEspecializacao & "',"   ' tipo especializacao 1 - baixa  2 - endosso
''    SQL = SQL & "'" & cUserName & "',"
''    If Trim(PMovimentacaoId) = "" Then
''       SQL = SQL & "null,"
''    Else
''       SQL = SQL & PMovimentacaoId & ","
''    End If
''    SQL = SQL & MudaVirgulaParaPonto(PValIR) & ","
''    SQL = SQL & MudaVirgulaParaPonto(PPercComissao) & ","
''    SQL = SQL & "n"
''
''    Set rc = rdocn.OpenResultset(SQL)
''
''    PMovimentacaoId = rc(0)
''
''    rc.Close
''
''    Exit Sub
''
''Trata_Erro:
''   TrataErroGeral "Inclui Mov Corretor"
''   Linha = "Rotina = Inclui Mov Corretor. Programa ser� Cancelado"
''   MsgBox Linha
''   Call TerminaSEGBR
''End Sub
''
''Public Sub Incluir_Mov_Estipulante(ByVal PDtMovimentacao, _
''                                   ByVal PValComissaoEstipulante, _
''                                   ByVal PPercProLabore, _
''                                   ByVal PTpOperacao, _
''                                   ByVal PTpEspecializacao, _
''                                   ByVal PMovimentacaoId, _
''                                   ByVal PEstipulanteId, _
''                                   ByVal PPropostaId, _
''                                   ByVal PEndossoId, _
''                                   ByVal bFlagRetemComissao As Boolean)
''
''   Dim rc   As rdoResultset
''   Dim sRetidoBanco As String
''
''   On Error GoTo Trata_Erro
''
''   'pcarvalho - 17/06/2003 Padroniza��o do c�lculo da restitui��o
''   'Retido_Banco = "s"
''   If bFlagRetemComissao Then
''     sRetidoBanco = "s"
''   Else
''     sRetidoBanco = "n"
''   End If
''
''   SQL = "exec ps_mov_estipulante_spi "
''   SQL = SQL & PMovimentacaoId & ","
''   SQL = SQL & PEstipulanteId & ","
''   SQL = SQL & "'" & PTpOperacao & "',"
''   SQL = SQL & "'" & Format(PDtMovimentacao, "yyyymmdd") & "',"
''   SQL = SQL & MudaVirgulaParaPonto(PValComissaoEstipulante) & ","
''   SQL = SQL & "'" & sRetidoBanco & "',"
''   SQL = SQL & PPropostaId & ","
''   SQL = SQL & "0" & ","
''   SQL = SQL & PEndossoId & ","
''   SQL = SQL & "'" & PTpEspecializacao & "',"   ' tipo especializacao 1 - baixa  2 - endosso
''   SQL = SQL & "'" & cUserName & "',"
''   SQL = SQL & MudaVirgulaParaPonto(PPercProLabore)
''
''   Set rc = rdocn.OpenResultset(SQL)
''
''   rc.Close
''
''   Exit Sub
''
''Trata_Erro:
''   TrataErroGeral "Inclui Mov Estipulante"
''   Linha = "Rotina = Inclui Mov Estipulante. Programa ser� Cancelado"
''   MsgBox Linha
''   End
''End Sub

Function Verificar_Uso_Proposta(ByVal PProposta As String) As Boolean

    Dim rc As rdoResultset
    Dim Lock_Aceite As String
    
    On Error GoTo Erro
   
    SQL = "select lock_aceite from proposta_tb where proposta_id =  " & PProposta
    
    Set rc = rdocn.OpenResultset(SQL)
    
    If Not rc.EOF Then
        
        Lock_Aceite = UCase(rc(0))
        If Lock_Aceite = "S" Then
           Verificar_Uso_Proposta = True
        Else
           Verificar_Uso_Proposta = False
        End If
        
    End If
    
    rc.Close
    
    Exit Function

Erro:
    TrataErroGeral "Verifica Proposta Uso"
    mensagem_erro 6, "Erro na rotina Verifica Proposta Uso : " & Err.Description & ". Programa ser� cancelado."
    End
End Function

''Sub Atualizar_Registro_Emi(ByVal PEmiId As String, _
''                           ByVal POrigem As String, _
''                           ByVal Psituacao As String, _
''                           ByVal PPropostaId As String, _
''                           ByVal PEndossoId As String)
''
''Dim SQL As String
''Dim Rst As rdoResultset
''
''If POrigem <> 3 Then
''
''  Select Case POrigem
''    Case 1
''      SQL = "Exec endosso_emi_proposta_spu "
''    Case 2
''      SQL = "Exec endosso_emi_re_proposta_spu "
''  End Select
''
''  SQL = SQL & "'" & Psituacao & "', "      '@situacao    varchar(1),
''  SQL = SQL & PEmiId & ", "                '@emi_proposta_id int,
''  SQL = SQL & "'" & cUserName & "',"       '@usuario     varchar(20)
''  If POrigem = 1 Then
''    SQL = SQL & "NULL,"
''  End If
''  SQL = SQL & PPropostaId & ", "
''  SQL = SQL & PEndossoId
''
''Else
''
''  'Jorfilho 14-mai-2003: atualizar dados de Transporte Internacional
''  SQL = "EXEC situacao_re_propostaBB_TI_spu "
''  SQL = SQL & PEmiId                                         '@emi_transp_proposta_id
''  SQL = SQL & ", '" & Psituacao & "'"                        '@situacao
''  SQL = SQL & ", '" & Format(Data_Sistema, "yyyymmdd") & "'" '@datasistema
''  SQL = SQL & ", '" & cUserName & "'"                        '@usuario
''  SQL = SQL & ", " & PPropostaId                             '@proposta_id
''  SQL = SQL & ", " & PEndossoId                              '@endosso_id
''
''End If
''
''Set Rst = rdocn.OpenResultset(SQL)
''
''End Sub

''Sub Insere_Endosso(ByVal PPropostaId As String, _
''                    ByVal PNumEndosso As String, _
''                    ByVal PNumEndossoBB As String, _
''                    ByVal PDtPedidoEndosso As String, _
''                    ByVal PTpEndosso As String)
''Dim SQL As String
''Dim Rst As rdoResultset
''
'''CREATE PROCEDURE desenv.endosso_tb_spi
'''@proposta_id         numeric(9,0),
'''@num_endosso_aux     integer,
'''@num_endosso_bb      numeric(9,0)  = null,
'''@tp_endosso_bb       int,
'''@dt_pedido_endosso   smalldatetime,
'''@dt_emissao          smalldatetime,
'''@usuario             varchar(20),
'''@descricao_endosso   text = null,
'''@dt_impressao       smalldatetime = null
''
''SQL = Ambiente & ".endosso_tb_spi "
''SQL = SQL & PPropostaId
''SQL = SQL & "," & PNumEndosso
''SQL = SQL & "," & IIf(Trim(PNumEndossoBB) <> "", PNumEndossoBB, "NULL")
''SQL = SQL & "," & PTpEndosso
''SQL = SQL & ",'" & Format(PDtPedidoEndosso, "yyyymmdd") & "'"
''SQL = SQL & ",'" & Format(Data_Sistema, "yyyymmdd") & "'"
''SQL = SQL & ",'" & cUserName & "'"
'''joconceicao - 2003 apr 17
'''@descricao_endosso   text = null,
'''@dt_impressao        smalldatetime = null,
'''@origem_endosso      char(1) = null
''
''SQL = SQL & ",null " ' descricao_endosso   'text = null,
''SQL = SQL & ",null " ' dt_impressao        'smalldatetime = null,
''SQL = SQL & ",'m'  " ' origem_endosso
''
''Set Rst = rdocn.OpenResultset(SQL)
''Rst.Close
''
''End Sub

''Function Verificar_Existencia_Avaliacao_Pendente(ByVal PProposta, ByVal PTpOperacao) As Boolean
''
''    Dim rc As rdoResultset
''    Dim Situacao As String
''
''    If PTpOperacao = 1 Then
''        SQL = " SELECT "
''        SQL = SQL & "       proposta_id "
''        SQL = SQL & " FROM "
''        SQL = SQL & "       avaliacao_cancelamento_tb "
''        SQL = SQL & " WHERE "
''        SQL = SQL & "       proposta_id = " & PProposta
''        SQL = SQL & "       AND situacao in ('p','r') "
''    Else
''        SQL = " SELECT "
''        SQL = SQL & "       situacao "
''        SQL = SQL & " FROM "
''        SQL = SQL & "       avaliacao_cancelamento_tb ac "
''        SQL = SQL & " WHERE "
''        SQL = SQL & "       ac.proposta_id = " & PProposta
''        SQL = SQL & "       AND ac.situacao in ('a','i') "
''        SQL = SQL & "       AND ac.num_avaliacao = (SELECT max(num_avaliacao)"
''        SQL = SQL & "                               FROM avaliacao_cancelamento_tb ac2 "
''        SQL = SQL & "                               WHERE ac2.proposta_id = ac.proposta_id) "
''    End If
''
''    Set rc = rdocn.OpenResultset(SQL)
''
''    If Not rc.EOF Then
''       Verificar_Existencia_Avaliacao_Pendente = True
''    Else
''       Verificar_Existencia_Avaliacao_Pendente = False
''    End If
''
''    rc.Close
''
''    Exit Function
''
''End Function

Function VerificarExistenciaAvaliacaoRestituicaoPendente(ByVal PProposta, ByVal PAvaliacao, ByVal PTpOperacao) As Boolean

    Dim rc As rdoResultset
    Dim Situacao As String

    If PTpOperacao = 1 Then
        SQL = " SELECT "
        SQL = SQL & "       proposta_id "
        SQL = SQL & " FROM "
        SQL = SQL & "       avaliacao_restituicao_tb "
        SQL = SQL & " WHERE "
        SQL = SQL & "       proposta_id = " & PProposta
        SQL = SQL & "   AND num_avaliacao = " & PAvaliacao
        SQL = SQL & "   AND situacao in ('t','r') "
    Else
        SQL = " SELECT "
        SQL = SQL & "       proposta_id "
        SQL = SQL & " FROM "
        SQL = SQL & "       avaliacao_restituicao_tb"
        SQL = SQL & " WHERE "
        SQL = SQL & "       proposta_id = " & PProposta
        SQL = SQL & "   AND num_avaliacao = " & PAvaliacao
        SQL = SQL & "   AND situacao in ('a','i') "
    End If

    Set rc = rdocn.OpenResultset(SQL)
    
    If Not rc.EOF Then
       VerificarExistenciaAvaliacaoRestituicaoPendente = True
    Else
       VerificarExistenciaAvaliacaoRestituicaoPendente = False
    End If
    
    rc.Close
    
    Exit Function

End Function

''Function Validar_Tipo_Restituicao(ByVal PProdutoId) As Boolean
''
''    Dim rc As rdoResultset
''    Dim Situacao As String
''
''    SQL = "Select tp_calc_restituicao from produto_tb where produto_id = " & PProdutoId
''
''    Set rc = rdocn.OpenResultset(SQL)
''
''    If Not rc.EOF Then
''        If IsNull(rc(0)) = True Then
''           Validar_Tipo_Restituicao = False
''        Else
''           If UCase(rc(0)) = "C" Or UCase(rc(0)) = "P" Then
''              Validar_Tipo_Restituicao = True
''           Else
''              Validar_Tipo_Restituicao = False
''           End If
''        End If
''    End If
''
''    rc.Close
''
''
''End Function

''Function Validar_Prazo_Restituicao_Integral(ByVal PProdutoId) As Boolean
''
''    Dim rc As rdoResultset
''    Dim Situacao As String
''
''    SQL = "Select prazo_restituicao_integral from produto_tb where produto_id = " & PProdutoId
''
''    Set rc = rdocn.OpenResultset(SQL)
''
''    If Not rc.EOF Then
''        If IsNull(rc(0)) = True Then
''           Validar_Prazo_Restituicao_Integral = False
''        Else
''           Validar_Prazo_Restituicao_Integral = True
''        End If
''    End If
''
''    rc.Close
''
''End Function

''Function Verificar_Pendencia_Endosso_Anterior(PPropostaBB, PNumEndossoBB) As Boolean
''
''Dim Rst As rdoResultset
''Dim SQL As String
''Dim Linha As String
''
'''Verificando endossos pendentes de aceite'''''''''''''''''''''''''''''''''''''''''''''''''
''
''SQL = "Select "
''SQL = SQL & "* "
''SQL = SQL & "From "
''SQL = SQL & "     emi_re_proposta_tb "
''SQL = SQL & "Where "
''SQL = SQL & "     tp_registro = '03'"
''SQL = SQL & "     and tipo_endosso = 51"
''SQL = SQL & "     and situacao = 'n' "
''SQL = SQL & "     and arquivo like 'seg490%' "
''SQL = SQL & "     and proposta_bb = " & PPropostaBB
''SQL = SQL & "     and convert(numeric(9),num_endosso) < " & PNumEndossoBB
''
''Set Rst = rdocn.OpenResultset(SQL)
''
''If Not Rst.EOF Then
''    Verificar_Pendencia_Endosso_Anterior = True
''Else
''    Verificar_Pendencia_Endosso_Anterior = False
''End If
''
''End Function

Sub Obter_Dados_ProLabore(ByVal PProposta, _
                          ByVal PDtReferencia, _
                          ByVal PValFinanceiro, _
                          ByVal PValIOF, _
                          ByVal PValCustoApoliceCertif, _
                          ByRef PValProLabore, _
                          ByRef PEstipulanteId, _
                          ByRef PNomeEstipulante, _
                          ByRef PPercProLabore)


Dim SQL As String
Dim Rst As rdoResultset

Dim EstipulanteId As String
Dim NomeEstipulante As String
Dim PercProLabore As Currency
Dim ValProLabore As Currency

'Obtendo EstipulanteId e Percentual de ProLabore'''''''''''''''''''''''''''''''''''''''''''
                                          

'Alterado at� que seja definido um crit�rio para a data de in�cio de corretagem - arodrigues 07/04/2003
  
'Obtem_Dados_Comissao_Estipulante PProposta, _
                                 PDtReferencia, _
                                 EstipulanteId, _
                                 PercProLabore
                                 
          

'Obtem_Dados_Comissao_Estipulante(ByVal PProposta As String, ByVal PDtReferencia As String, ByRef PEstipulanteId As String, ByRef PPercComissaoEstipulante As Currency)
   
SQL = "SELECT a.est_cliente_id, "
SQL = SQL & " isnull(a.perc_pro_labore,0) perc_pro_labore "
SQL = SQL & " FROM administracao_apolice_tb a"
SQL = SQL & " WHERE a.proposta_id = " & PProposta
SQL = SQL & " and a.dt_inicio_administracao = (SELECT MIN(dt_inicio_administracao) " & vbNewLine
SQL = SQL & "                                    FROM administracao_apolice_tb b " & vbNewLine
SQL = SQL & "                                   WHERE a.Proposta_Id = b.Proposta_Id) " & vbNewLine

Set Rst = rdocn.OpenResultset(SQL)
   
If Not Rst.EOF Then
   'carsilva - 18/05/2004 - Fixar o percentual de pro-labore para o Ouro Empresarial em 2%
   If Produto_id = 111 Then
      EstipulanteId = Rst!est_cliente_id
      PercProLabore = 2
   Else
      EstipulanteId = Rst!est_cliente_id
      PercProLabore = Val(Rst!perc_pro_labore)
   End If
Else
   EstipulanteId = 0
   PercProLabore = 0
End If
      
Rst.Close
       

If Val(EstipulanteId) <> 0 Then

    'Obtendo Nome do Estipulante'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    
    SQL = "Select nome from cliente_tb where cliente_id = " & EstipulanteId
    
    Set Rst = rdocn.OpenResultset(SQL)
    
    If Not Rst.EOF Then
        NomeEstipulante = Rst!Nome
    Else
        NomeEstipulante = ""
    End If

    'Calculando Valor do ProLabore''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

    ValProLabore = Trunca((PValFinanceiro - PValIOF - PValCustoApoliceCertif) * (PercProLabore / 100))
    
Else
    
    ValProLabore = 0
    EstipulanteId = 0
    NomeEstipulante = ""
    PercProLabore = 0
    
End If

'Retornando valores'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

PValProLabore = ValProLabore
PEstipulanteId = EstipulanteId
PNomeEstipulante = NomeEstipulante
PPercProLabore = PercProLabore

End Sub


''Public Function verifica_endosso_liquidacao(dtLiquidacao As String, dtFimVigencia As String, TipoEndosso As String)
''
''   Dim Rst As rdoResultset
''   Dim SQL As String
''
''   If TipoEndosso <> "LIQUIDA��O" And TipoEndosso <> "L" Then
''      verifica_endosso_liquidacao = True
''      Exit Function
''   End If
''
''   SQL = "SELECT DATEDIFF(day, '" & Year(dtLiquidacao) & _
''      Format(Month(dtLiquidacao), "00") & _
''      Format(Day(dtLiquidacao), "00") & "', '" & Year(dtFimVigencia) & _
''      Format(Month(dtFimVigencia), "00") & _
''      Format(Day(dtFimVigencia), "00") & "') as diferenca"
''
''   Set Rst = rdocn.OpenResultset(SQL)
''
''   If Rst.EOF = False Then
''
''      If Val(Rst(0)) < 30 Then
''         verifica_endosso_liquidacao = False
''      Else
''         verifica_endosso_liquidacao = True
''      End If
''
''   Else
''
''      verifica_endosso_liquidacao = True
''
''   End If
''
''End Function

Public Sub VerificarRetencao(ByVal sConvenio As String, _
                             ByRef bFlagRetemComissao As Boolean, _
                             ByRef bFlagRetemIOF As Boolean, _
                             ByRef bFlagRetemProLabore As Boolean)
   
  'pcarvalho - 17/06/2003
  
  Dim oRs As rdoResultset
  Dim sSQL As String
  
  On Error GoTo TrataErro
    
  sSQL = ""
  adSQL sSQL, "SELECT retencao_comissao, retencao_pro_labore, "
  adSQL sSQL, "       retencao_iof"
  adSQL sSQL, "  FROM convenio_tb"
  adSQL sSQL, " WHERE num_convenio = '" & sConvenio & "'"
   
  Set oRs = rdocn.OpenResultset(sSQL)
  
  If Not oRs.EOF Then
     
    If UCase(oRs("retencao_comissao")) = "S" Then
       bFlagRetemComissao = True
    Else
       bFlagRetemComissao = False
    End If
    
    If UCase(oRs("retencao_pro_labore")) = "S" Then
       bFlagRetemProLabore = True
    Else
       bFlagRetemProLabore = False
    End If
    
    If UCase(oRs("retencao_iof")) = "S" Then
       bFlagRetemIOF = True
    Else
       bFlagRetemIOF = False
    End If
    
    oRs.Close
     
  Else
    bFlagRetemComissao = True
    bFlagRetemIOF = True
  End If
  
  Set oRs = Nothing
     
  Exit Sub
    
TrataErro:
  Call TrataErroGeral("VerificarRetencao", "local")
  Call TerminaSEGBR

End Sub

'''Luciana - 12/08/2003 - Cancelamento de endosso
''Sub Insere_Cancelamento_Endosso(ByVal PropostaId As String, _
''                    ByVal CancEndossoId As String, _
''                    ByVal EndossoId As String)
''
''    Dim Rst As rdoResultset
''
''    SQS = "exec " & Ambiente & ".cancelamento_endosso_spi "
''    adSQS FormatoSQLF(",", PropostaId, CancEndossoId, , , , , cUserName)
''    adSQS FormatoSQL(EndossoId, Format(Data_Sistema, "yyyymmdd"))
''
''    Set Rst = rdocn.OpenResultset(SQS)
''    Rst.Close
''
''End Sub

Public Sub AprovarRestituicao(ByVal lPropostaId As Long, _
                              ByVal lNumAvaliacao As Long, _
                              ByVal lProdutoId As Long, _
                              ByVal sDtContratacao As String, _
                              ByVal sDtInicioVig As String, _
                              ByVal sDtFimVig As String, _
                              ByVal sDtPedidoAvaliacao As String)

'Projeto Restitui��o por Iniciativa da Seguradora - bcarneiro - 03/12/2003

Const SITUACAO_PENDENTE_AVALIACAO_GERENCIAL = "T"

Const COD_MOV_FINANC = 3  'Devolu��o

Dim OEndosso As Object
Dim oFinanceiro As Object

Dim lEndossoId As Long
Dim lRestEndossoId As Long

Dim cValCustoApoliceCertifEst As Currency
Dim cValAdicFracionamentoEst As Currency
Dim cValDescontoEst As Currency
Dim cValIOFEst As Currency
Dim cValPremioTarifaEst As Currency
Dim cValRestituicaoEst As Currency
Dim cValTotCorretagemEst As Currency
Dim cValProLaboreEst As Currency


'mathayde 27/03/2009
Dim cValRemuneracao As Currency
Dim cValRemuneracaoEst As Currency



Dim cValCustoApoliceCertifRest As Currency
Dim cValAdicFracionamentoRest As Currency
Dim cValDescontoRest As Currency
Dim cValIOFRest As Currency
Dim cValPremioTarifaRest As Currency
Dim cValRestituicao As Currency
Dim cValTotCorretagemRest As Currency
Dim cValProLaboreRest As Currency

Dim iTpEndossoId As Integer
Dim lTpEndossoId_ALS As Integer
Dim iOrigemProposta As Integer
Dim lEventoId As Long
Dim lClienteIDBeneficiario As Long

Dim sTextoEndosso As String

'Rmarins - 20/10/06
Dim cValSubvencaoFederal As Currency
Dim cValSubvencaoEstadual As Currency
Dim cValSubvencaoFederalEst As Currency
Dim cValSubvencaoEstadualEst As Currency
Dim cValCambio As Currency
Dim cValIsRestituicao As Currency
'
Dim colCliente As New Collection
Dim oCliente As Cliente

'HOLIVEIRA: Variavel que ira guardar a conexao DT: 05/02/2007
Dim lConexao As Long

'RSilva - Tipo de Cobertura
Dim lTipoCobertura As Long
Dim sSQL As String
Dim rs As rdoResultset

On Error GoTo TrataErro

    MousePointer = vbHourglass

    'Iniciando transa��o''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    rdocn.BeginTrans
    
    
   If bModular = True Or lProdutoId = 1168 Then
        sSQL = ""
        adSQL sSQL, "SELECT tp_cobertura_id"
        adSQL sSQL, "  From seguros_db.dbo.avaliacao_restituicao_habitacional_tb"
        adSQL sSQL, " WHERE proposta_id   = " & lPropostaId
        adSQL sSQL, "   AND num_avaliacao = " & lNumAvaliacao
        
        
        Set rs = rdocn.OpenResultset(sSQL)

        If Not rs.EOF Then
            lTipoCobertura = rs("tp_cobertura_id")
        Else
            lTipoCobertura = 0
        End If
        'If oDadosModular.iTpCobertura = 526 Then
        '    lTipoCobertura = 527
        'Else
        '    lTipoCobertura = 526
        'End If
    Else
        lTipoCobertura = 0
    End If

    
    
    'Obtendo dados da avalia��o t�cnica aprovada''''''''''''''''''''''''''''''''''''''''''''''''
    Call FrmSelEndosso.ObterValoresAvaliacaoRestituicao(lPropostaId, _
                                                        lNumAvaliacao, _
                                                        SITUACAO_PENDENTE_AVALIACAO_GERENCIAL, _
                                                        cValRestituicao, _
                                                        cValIOFRest, _
                                                        cValCustoApoliceCertifRest, _
                                                        cValAdicFracionamentoRest, _
                                                        cValDescontoRest, _
                                                        cValPremioTarifaRest, _
                                                        cValTotCorretagemRest, _
                                                        cValProLaboreRest, _
                                                        cValCustoApoliceCertifEst, _
                                                        cValAdicFracionamentoEst, _
                                                        cValDescontoEst, _
                                                        cValIOFEst, _
                                                        cValPremioTarifaEst, _
                                                        cValRestituicaoEst, _
                                                        cValTotCorretagemEst, _
                                                        cValProLaboreEst, _
                                                        lRestEndossoId, _
                                                        cValCambio, _
                                                        sTextoEndosso, _
                                                        cValSubvencaoFederal, cValSubvencaoEstadual, _
                                                        cValSubvencaoFederalEst, cValSubvencaoEstadualEst, _
                                                        cValRemuneracao, cValRemuneracaoEst)
                                                        'Mathayde 30/03/2009 - Obtem os valores de cValRemuneracao e cValRemuneracaoEst
    
'    If lProdutoId = 1168 Then
'        cValRestituicao = cValRestituicao + oDadosModular.ValRestituicao
'        cValIOFRest = cValIOFRest + oDadosModular.Val_IOF_Restituicao
'        cValCustoApoliceCertifRest = ""
'        cValAdicFracionamentoRest = ""
'        cValDescontoRest = ""
'        cValPremioTarifaRest = ""
'        cValTotCorretagemRest = ""
'        cValProLaboreRest = ""
'        cValCustoApoliceCertifEst = ""
'        cValAdicFracionamentoEst = ""
'        cValDescontoEst = ""
'        cValIOFEst = ""
'        cValPremioTarifaEst = ""
'        cValRestituicaoEst = ""
'        cValTotCorretagemEst = ""
'        cValProLaboreEst = ""
'        lRestEndossoId = ""
'    End If
                                
    'Obtendo dados da Proposta '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    Call Obter_Premio_IS(lPropostaId, lProdutoId, cValIsRestituicao, 0)
    
    'Calculando os valores com subven��o por Cliente - Rmarins 20/10/2006 ''''''''''''''''''''''
    If gsPossuiSubvencao = "S" Then
                        
        If cValSubvencaoFederal <> 0 Then
            Call CalcularValoresRestituicaoCliente("FEDERAL", _
                                                   lProdutoId, _
                                                   cValRestituicao, _
                                                   cValIOFRest, _
                                                   cValCustoApoliceCertifRest, _
                                                   cValAdicFracionamentoRest, _
                                                   cValDescontoRest, _
                                                   cValPremioTarifaRest, _
                                                   cValTotCorretagemRest, _
                                                   cValProLaboreRest, _
                                                   cValCustoApoliceCertifEst, _
                                                   cValAdicFracionamentoEst, _
                                                   cValDescontoEst, _
                                                   cValIOFEst, _
                                                   cValPremioTarifaEst, _
                                                   cValRestituicaoEst, _
                                                   cValTotCorretagemEst, _
                                                   cValProLaboreEst, _
                                                   cValIsRestituicao, _
                                                   cValSubvencaoFederal, _
                                                   cValSubvencaoEstadual, _
                                                   colCliente)

                                                                                                                               
        End If
        
        'bcarneiro - 06/12/2006 - Retirado temporariamente enquanto n�o � definido o endosso
        '                         para restitui��o estadual
        'bmoraes 08.03.2009 - restitui��o estadual
        If cValSubvencaoEstadual <> 0 Then
            Call CalcularValoresRestituicaoCliente("ESTADUAL", _
                                                   lProdutoId, _
                                                   cValRestituicao, _
                                                   cValIOFRest, _
                                                   cValCustoApoliceCertifRest, _
                                                   cValAdicFracionamentoRest, _
                                                   cValDescontoRest, _
                                                   cValPremioTarifaRest, _
                                                   cValTotCorretagemRest, _
                                                   cValProLaboreRest, _
                                                   cValCustoApoliceCertifEst, _
                                                   cValAdicFracionamentoEst, _
                                                   cValDescontoEst, _
                                                   cValIOFEst, _
                                                   cValPremioTarifaEst, _
                                                   cValRestituicaoEst, _
                                                   cValTotCorretagemEst, _
                                                   cValProLaboreEst, _
                                                   cValIsRestituicao, _
                                                   cValSubvencaoFederal, _
                                                   cValSubvencaoEstadual, _
                                                   colCliente)

        End If
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        
    End If
    
    'Obtendo dados da proposta''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    lClienteIDBeneficiario = Obter_Beneficiario(lPropostaId)
    
    Call CalcularValoresRestituicaoCliente("CLIENTE", _
                                           lProdutoId, _
                                           cValRestituicao, _
                                           cValIOFRest, _
                                           cValCustoApoliceCertifRest, _
                                           cValAdicFracionamentoRest, _
                                           cValDescontoRest, _
                                           cValPremioTarifaRest, _
                                           cValTotCorretagemRest, _
                                           cValProLaboreRest, _
                                           cValCustoApoliceCertifEst, _
                                           cValAdicFracionamentoEst, _
                                           cValDescontoEst, _
                                           cValIOFEst, _
                                           cValPremioTarifaEst, _
                                           cValRestituicaoEst, _
                                           cValTotCorretagemEst, _
                                           cValProLaboreEst, _
                                           cValIsRestituicao, _
                                           cValSubvencaoFederal, _
                                           cValSubvencaoEstadual, _
                                           colCliente, _
                                           lClienteIDBeneficiario)

    
    'HOLIVEIRA: Cria o objeto neste momento DT: 05/02/2007

    ' C_ANEVES - 26/08/2013 - Demanda: 17860335 - Inicio
    Set OEndosso = CreateObject("SEGL0026.cls00124")
    'Set OEndosso = CreateObject("SEGL0355.cls00626")
    ' C_ANEVES - 26/08/2013 - Demanda: 17860335 - Fim
                
    'HOLIVEIRA: Abre a conexao do objeto oEndosso DT: 05/02/2007
    lConexao = OEndosso.AbrirConexao("SEGBR", glAmbiente_id, App.Title, App.FileDescription)

    'HOLIVEIRA: Abre a transa��o do objeto oEndosso DT: 27/12/2006
    If Not OEndosso.AbrirTransacao(lConexao) Then GoTo TrataErro

    For Each oCliente In colCliente
    
        'Emitindo o Endosso por Cliente - Rmarins - 20/10/06
        Call ProcessarEmissaoEndosso(lPropostaId, _
                                     lProdutoId, _
                                     lNumAvaliacao, _
                                     oCliente.Cliente_Id, _
                                     lRestEndossoId, _
                                     oCliente.Tp_Cliente, _
                                     oCliente.Val_Restituicao, _
                                     oCliente.Val_IOF_Rest, _
                                     oCliente.Val_Custo_Apolice_Rest, _
                                     oCliente.Val_Tot_Corretagem_Rest, _
                                     oCliente.Val_Pro_Labore_Rest, _
                                     oCliente.Val_Premio_Tarifa_Rest, _
                                     oCliente.Val_Desconto_Rest, _
                                     oCliente.Val_Adic_Frac_Rest, _
                                     oCliente.Val_IS_Restituicao, _
                                     cValCambio, _
                                     sDtPedidoAvaliacao, _
                                     sTextoEndosso, _
                                     iOrigemProposta, _
                                     oCliente.iTpEndossoId, _
                                     lEndossoId, _
                                     lConexao, _
                                     OEndosso, _
                                     cValRemuneracao)
                                     'Mathayde 30/03/2009 - Envia cValRemuneracao para calcular o cValIrRemuneracao

    
        If oCliente.Tp_Cliente = "CLIENTE" Or oCliente.Tp_Cliente = "FEDERAL" Or oCliente.Tp_Cliente = "ESTADUAL" Then
        
            If iOrigemProposta = 2 Then  'ALS
                    
                lTpEndossoId_ALS = ObterTp_Endosso(lPropostaId, lEndossoId, lEventoId) 'crosendo - Altera��o realizada em 11/08/2006

                'HOLIVEIRA: Cria o objeto antes DT: 05/02/2007
                'Set oEndosso = CreateObject("SEGL0026.cls00124")
                      
                Select Case lEventoId 'Rmarins 20/10/06 - Altera��o p/ gera��o de endosso por Evento
                                
                    Case 3801
                                
                        Call OEndosso.GerarEndossoAls(cUserName, _
                                                      gsSIGLASISTEMA, _
                                                      App.Title, _
                                                      App.FileDescription, _
                                                      glAmbiente_id, _
                                                      Data_Sistema, _
                                                      lPropostaId, _
                                                      Format(Data_Sistema, "yyyymm"), _
                                                      sDtPedidoAvaliacao, _
                                                      lEndossoId, _
                                                      oCliente.Val_Restituicao, _
                                                      lTpEndossoId_ALS, _
                                                      COD_MOV_FINANC, _
                                                      0, _
                                                      lEventoId, _
                                                      , _
                                                      lConexao) 'HOLIVEIRA: Passa lConexao como parametro DT: 05/02/2007
                    
                    Case 3804
                                
                        Call OEndosso.GerarEndossoAls(cUserName, _
                                                      gsSIGLASISTEMA, _
                                                      App.Title, _
                                                      App.FileDescription, _
                                                      glAmbiente_id, _
                                                      Data_Sistema, _
                                                      lPropostaId, _
                                                      Format(Data_Sistema, "yyyymm"), _
                                                      sDtPedidoAvaliacao, _
                                                      lEndossoId, _
                                                      oCliente.Val_Restituicao, _
                                                      lTpEndossoId_ALS, _
                                                      COD_MOV_FINANC, _
                                                      0, _
                                                      lEventoId, _
                                                      , _
                                                      lConexao) 'HOLIVEIRA: Passa lConexao como parametro DT: 05/02/2007
                                
                    Case 3800
                    
                        Call OEndosso.AtualizarEndossoAls(cUserName, _
                                                          gsSIGLASISTEMA, _
                                                          App.Title, _
                                                          App.FileDescription, _
                                                          glAmbiente_id, _
                                                          lPropostaId, _
                                                          Abs(oCliente.Val_Restituicao), _
                                                          lEventoId, _
                                                          "", _
                                                          lConexao) 'HOLIVEIRA: Passa lConexao como parametro DT: 05/02/2007
                                                           
                    'RodriSilva - 21/12/2009
                    Case 3812
                        Call OEndosso.GerarEndossoAls(cUserName, _
                                                      gsSIGLASISTEMA, _
                                                      App.Title, _
                                                      App.FileDescription, _
                                                      glAmbiente_id, _
                                                      Data_Sistema, _
                                                      lPropostaId, _
                                                      Format(Data_Sistema, "yyyymm"), _
                                                      sDtPedidoAvaliacao, _
                                                      lEndossoId, _
                                                      oCliente.Val_Restituicao, _
                                                      lTpEndossoId_ALS, _
                                                      COD_MOV_FINANC, _
                                                      0, _
                                                      lEventoId, _
                                                      , _
                                                      lConexao, lTipoCobertura)

                End Select
                    
            End If
            
            If oCliente.Tp_Cliente = "CLIENTE" Then
                
                'Aprovando avalia��o t�cnica''''''''''''''''''''''''''''''''''''''''''''''''''''''
                If bModular = False Then
                    Call AprovarAvaliacaoRestituicao(lPropostaId, lNumAvaliacao, lEndossoId, lConexao, OEndosso, cValRemuneracao) 'HOLIVEIRA: Passa lConexao e o objeto oEndosso como parametro DT: 05/02/2007
                                                                                                            'Mathayde 30/03/2009 - Envia cValRemuneracao e passa para AtualizarAvaliacaoRestituicao para gravar em avaliacao_restituicao_tb
                Else
                    If lProdutoId = 1168 Then
                        If lNumAvaliacao = oDadosModular.Num_avaliacao Then
                            'HOLIVEIRA: Passa lConexao e o objeto oEndosso como parametro DT: 05/02/2007
                            'Mathayde 30/03/2009 - Envia cValRemuneracao e passa para AtualizarAvaliacaoRestituicao para gravar em avaliacao_restituicao_tb
                            Call AprovarAvaliacaoRestituicao(lPropostaId, oDadosModular.Num_avaliacao, lEndossoId, lConexao, OEndosso, cValRemuneracao)
                        Else
                            Call AprovarAvaliacaoRestituicao(lPropostaId, lNumAvaliacao, lEndossoId, lConexao, OEndosso, cValRemuneracao) 'HOLIVEIRA: Passa lConexao e o objeto oEndosso como parametro DT: 05/02/2007
                        End If
                    Else
                        If lPropostaId = oDadosModular.proposta_id Then
                            Call AprovarAvaliacaoRestituicao(lPropostaId, oDadosModular.Num_avaliacao, lEndossoId, lConexao, OEndosso, cValRemuneracao)
                        Else
                            Call AprovarAvaliacaoRestituicao(lPropostaId, lNumAvaliacao, lEndossoId, lConexao, OEndosso, cValRemuneracao) 'HOLIVEIRA: Passa lConexao e o objeto oEndosso como parametro DT: 05/02/2007
                        End If
                    End If
                End If

                'Inserindo em evento_emissao_tb '''''''''''''''''''''''''''''''''''''''''''''''''''
'                If bModular = False Then
                    Call InserirEvento(lPropostaId, CInt(lProdutoId), Val(sRamoId), lEndossoId, lTpEndossoId_ALS)
 '               Else
  '                  Call InserirEvento(lPropostaId, CInt(lProdutoId), oDadosModular.ramo_id, lEndossoId, lTpEndossoId_ALS)
   '             End If
                
            End If
        
        End If
        
        
      '(in�cio) -- rsouza -- 03/03/2011 ---------------------------------------'
       Call OEndosso.Insere_Tabelas_Circular(cUserName, _
                                             gsSIGLASISTEMA, _
                                             App.Title, _
                                             App.FileDescription, _
                                             glAmbiente_id, _
                                             lPropostaId, lEndossoId, lConexao)
      '(fim)    -- rsouza -- 03/03/2011 ---------------------------------------'
                                                                                                                               
    Next

    'Desbloqueando proposta'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    Call Atualizar_Bloqueio_Proposta(lPropostaId, "n")
    
    
    
    'Finalizando transa��o''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    rdocn.CommitTrans
    OEndosso.ConfirmarTransacao (lConexao) 'HOLIVEIRA: Confirma a transa��o DT: 05/02/2007
    OEndosso.FecharConexao (lConexao) 'HOLIVEIRA: Fecha a conexao DT: 05/02/2007
    
'''''    rdocn.RollbackTrans
'''''    oEndosso.RetornarTransacao (lConexao) 'HOLIVEIRA: Desfaz a transa��o DT: 05/02/2007
'''''    oEndosso.FecharConexao (lConexao) 'HOLIVEIRA: Fecha a conexao DT: 05/02/2007

    'Destruindo objetos
    Set OEndosso = Nothing
    MousePointer = vbDefault
    
Exit Sub
TrataErro:

    rdocn.RollbackTrans
    OEndosso.RetornarTransacao (lConexao) 'HOLIVEIRA: Desfaz a transa��o DT: 05/02/2007
    OEndosso.FecharConexao (lConexao) 'HOLIVEIRA: Fecha a conexao DT: 05/02/2007
    
    'Destruindo objetos
    Set OEndosso = Nothing
    
    Call TrataErroGeral("AprovarRestituicao")
    Call TerminaSEGBR
    
End Sub

Public Sub AprovarRestituicaoModular(ByVal lPropostaId As Long, _
                                     ByVal lNumAvaliacao As Long, _
                                     ByVal lProdutoId As Long, _
                                     ByVal sDtContratacao As String, _
                                     ByVal sDtInicioVig As String, _
                                     ByVal sDtFimVig As String, _
                                     ByVal sDtPedidoAvaliacao As String)

'Projeto Restitui��o por Iniciativa da Seguradora - bcarneiro - 03/12/2003

Const SITUACAO_PENDENTE_AVALIACAO_GERENCIAL = "T"

Const COD_MOV_FINANC = 3  'Devolu��o

Dim OEndosso As Object
Dim oFinanceiro As Object

Dim lEndossoId As Long
Dim lRestEndossoId As Long

Dim cValCustoApoliceCertifEst As Currency
Dim cValAdicFracionamentoEst As Currency
Dim cValDescontoEst As Currency
Dim cValIOFEst As Currency
Dim cValPremioTarifaEst As Currency
Dim cValRestituicaoEst As Currency
Dim cValTotCorretagemEst As Currency
Dim cValProLaboreEst As Currency


'mathayde 27/03/2009
Dim cValRemuneracao As Currency
Dim cValRemuneracaoEst As Currency



Dim cValCustoApoliceCertifRest As Currency
Dim cValAdicFracionamentoRest As Currency
Dim cValDescontoRest As Currency
Dim cValIOFRest As Currency
Dim cValPremioTarifaRest As Currency
Dim cValRestituicao As Currency
Dim cValTotCorretagemRest As Currency
Dim cValProLaboreRest As Currency

Dim iTpEndossoId As Integer
Dim lTpEndossoId_ALS As Integer
Dim iOrigemProposta As Integer
Dim lEventoId As Long
Dim lClienteIDBeneficiario As Long

Dim sTextoEndosso As String

'Rmarins - 20/10/06
Dim cValSubvencaoFederal As Currency
Dim cValSubvencaoEstadual As Currency
Dim cValSubvencaoFederalEst As Currency
Dim cValSubvencaoEstadualEst As Currency
Dim cValCambio As Currency
Dim cValIsRestituicao As Currency
'
Dim colCliente As New Collection
Dim oCliente As Cliente

'HOLIVEIRA: Variavel que ira guardar a conexao DT: 05/02/2007
Dim lConexao As Long

Dim lTipoCobertura As Long
 
On Error GoTo TrataErro

    MousePointer = vbHourglass

    'Iniciando transa��o''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    rdocn.BeginTrans
    
    If Produto_id = 1168 Then
        lTipoCobertura = oDadosModular.iTpCobertura
    Else
        lTipoCobertura = 0
    End If
    '
    'lEndossoId = oDadosModular.EndossoId
    'Obtendo dados da avalia��o t�cnica aprovada''''''''''''''''''''''''''''''''''''''''''''''''
    Call FrmSelEndosso.ObterValoresAvaliacaoRestituicao(lPropostaId, _
                                                        lNumAvaliacao, _
                                                        SITUACAO_PENDENTE_AVALIACAO_GERENCIAL, _
                                                        cValRestituicao, _
                                                        cValIOFRest, _
                                                        cValCustoApoliceCertifRest, _
                                                        cValAdicFracionamentoRest, _
                                                        cValDescontoRest, _
                                                        cValPremioTarifaRest, _
                                                        cValTotCorretagemRest, _
                                                        cValProLaboreRest, _
                                                        cValCustoApoliceCertifEst, _
                                                        cValAdicFracionamentoEst, _
                                                        cValDescontoEst, _
                                                        cValIOFEst, _
                                                        cValPremioTarifaEst, _
                                                        cValRestituicaoEst, _
                                                        cValTotCorretagemEst, _
                                                        cValProLaboreEst, _
                                                        lRestEndossoId, _
                                                        cValCambio, _
                                                        sTextoEndosso, _
                                                        cValSubvencaoFederal, cValSubvencaoEstadual, _
                                                        cValSubvencaoFederalEst, cValSubvencaoEstadualEst, _
                                                        cValRemuneracao, cValRemuneracaoEst)
                                                        'Mathayde 30/03/2009 - Obtem os valores de cValRemuneracao e cValRemuneracaoEst

                                
    'Obtendo dados da Proposta '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    Call Obter_Premio_IS(lPropostaId, lProdutoId, cValIsRestituicao, 0)
    
    'Obtendo dados da proposta''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'    lClienteIDBeneficiario = Obter_Beneficiario(lPropostaId)
    lClienteIDBeneficiario = Obter_Beneficiario(oDadosModular.proposta_id)
    
    Call CalcularValoresRestituicaoCliente("CLIENTE", _
                                           lProdutoId, _
                                           oDadosModular.ValRestituicao, _
                                           oDadosModular.ValIOFRest, _
                                           oDadosModular.ValCustoApoliceCertifRest, _
                                           oDadosModular.ValAdicFracionamentoRest, _
                                           oDadosModular.ValDescontoRest, _
                                           oDadosModular.ValPremioTarifaRest, _
                                           oDadosModular.ValTotCorretagemRest, _
                                           oDadosModular.ValProLaboreRest, _
                                           oDadosModular.ValCustoApoliceCertifEst, _
                                           oDadosModular.ValAdicFracionamentoEst, _
                                           oDadosModular.ValDescontoEst, _
                                           oDadosModular.ValIOFEst, _
                                           oDadosModular.ValPremioTarifaEst, _
                                           oDadosModular.ValRestituicaoEst, _
                                           oDadosModular.Val_Tot_Corretagem_Est, _
                                           oDadosModular.Val_Pro_Lab_Est, _
                                           oDadosModular.Val_IS, _
                                           0, _
                                           0, _
                                           colCliente, _
                                           lClienteIDBeneficiario)


    'HOLIVEIRA: Cria o objeto neste momento DT: 05/02/2007
    ' C_ANEVES - 26/08/2013 - Demanda: 17860335 - Inicio
    Set OEndosso = CreateObject("SEGL0026.cls00124")
    'Set OEndosso = CreateObject("SEGL0355.cls00626")
    ' C_ANEVES - 26/08/2013 - Demanda: 17860335 - Fim

    'HOLIVEIRA: Abre a conexao do objeto oEndosso DT: 05/02/2007
    lConexao = OEndosso.AbrirConexao("SEGBR", glAmbiente_id, App.Title, App.FileDescription)

    'HOLIVEIRA: Abre a transa��o do objeto oEndosso DT: 27/12/2006
    If Not OEndosso.AbrirTransacao(lConexao) Then GoTo TrataErro

    For Each oCliente In colCliente
    
        'Emitindo o Endosso por Cliente - Rmarins - 20/10/06
        Call ProcessarEmissaoEndosso(lPropostaId, _
                                     lProdutoId, _
                                     lNumAvaliacao, _
                                     oCliente.Cliente_Id, _
                                     lRestEndossoId, _
                                     oCliente.Tp_Cliente, _
                                     oCliente.Val_Restituicao, _
                                     oCliente.Val_IOF_Rest, _
                                     oCliente.Val_Custo_Apolice_Rest, _
                                     oCliente.Val_Tot_Corretagem_Rest, _
                                     oCliente.Val_Pro_Labore_Rest, _
                                     oCliente.Val_Premio_Tarifa_Rest, _
                                     oCliente.Val_Desconto_Rest, _
                                     oCliente.Val_Adic_Frac_Rest, _
                                     oCliente.Val_IS_Restituicao, _
                                     cValCambio, _
                                     sDtPedidoAvaliacao, _
                                     sTextoEndosso, _
                                     iOrigemProposta, _
                                     oCliente.iTpEndossoId, _
                                     oDadosModular.EndossoId, _
                                     lConexao, _
                                     OEndosso, _
                                     cValRemuneracao)
                                     'Mathayde 30/03/2009 - Envia cValRemuneracao para calcular o cValIrRemuneracao

    
        If oCliente.Tp_Cliente = "CLIENTE" Or oCliente.Tp_Cliente = "FEDERAL" Or oCliente.Tp_Cliente = "ESTADUAL" Then
        
            If iOrigemProposta = 2 Then  'ALS
                    
                lTpEndossoId_ALS = ObterTp_Endosso(lPropostaId, lNumAvaliacao, lEventoId) 'crosendo - Altera��o realizada em 11/08/2006

                'HOLIVEIRA: Cria o objeto antes DT: 05/02/2007
                'Set oEndosso = CreateObject("SEGL0026.cls00124")
                      
                Select Case lEventoId 'Rmarins 20/10/06 - Altera��o p/ gera��o de endosso por Evento
                                
                    Case 3801
                                
                        Call OEndosso.GerarEndossoAls(cUserName, _
                                                      gsSIGLASISTEMA, _
                                                      App.Title, _
                                                      App.FileDescription, _
                                                      glAmbiente_id, _
                                                      Data_Sistema, _
                                                      lPropostaId, _
                                                      Format(Data_Sistema, "yyyymm"), _
                                                      sDtPedidoAvaliacao, _
                                                      lEndossoId, _
                                                      oCliente.Val_Restituicao, _
                                                      lTpEndossoId_ALS, _
                                                      COD_MOV_FINANC, _
                                                      0, _
                                                      lEventoId, _
                                                      , _
                                                      lConexao) 'HOLIVEIRA: Passa lConexao como parametro DT: 05/02/2007
                    
                    Case 3804
                                
                        Call OEndosso.GerarEndossoAls(cUserName, _
                                                      gsSIGLASISTEMA, _
                                                      App.Title, _
                                                      App.FileDescription, _
                                                      glAmbiente_id, _
                                                      Data_Sistema, _
                                                      lPropostaId, _
                                                      Format(Data_Sistema, "yyyymm"), _
                                                      sDtPedidoAvaliacao, _
                                                      lEndossoId, _
                                                      oCliente.Val_Restituicao, _
                                                      lTpEndossoId_ALS, _
                                                      COD_MOV_FINANC, _
                                                      0, _
                                                      lEventoId, _
                                                      , _
                                                      lConexao) 'HOLIVEIRA: Passa lConexao como parametro DT: 05/02/2007
                                
                    Case 3800
                    
                        Call OEndosso.AtualizarEndossoAls(cUserName, _
                                                          gsSIGLASISTEMA, _
                                                          App.Title, _
                                                          App.FileDescription, _
                                                          glAmbiente_id, _
                                                          lPropostaId, _
                                                          Abs(oCliente.Val_Restituicao), _
                                                          lEventoId, _
                                                          "", _
                                                          lConexao) 'HOLIVEIRA: Passa lConexao como parametro DT: 05/02/2007
                                                          
                    Case 3812
                        
                        
                        Call OEndosso.GerarEndossoAls(cUserName, _
                                                      gsSIGLASISTEMA, _
                                                      App.Title, _
                                                      App.FileDescription, _
                                                      glAmbiente_id, _
                                                      Data_Sistema, _
                                                      lPropostaId, _
                                                      Format(Data_Sistema, "yyyymm"), _
                                                      sDtPedidoAvaliacao, _
                                                      lEndossoId, _
                                                      oCliente.Val_Restituicao, _
                                                      lTpEndossoId_ALS, _
                                                      COD_MOV_FINANC, _
                                                      0, _
                                                      lEventoId, _
                                                      , _
                                                      lConexao, lTipoCobertura)
                                                          
                End Select
                    
            End If
            
            If oCliente.Tp_Cliente = "CLIENTE" Then
                
                'Aprovando avalia��o t�cnica''''''''''''''''''''''''''''''''''''''''''''''''''''''
                Call AprovarAvaliacaoRestituicao(lPropostaId, lNumAvaliacao, oDadosModular.Num_avaliacao, lConexao, OEndosso, cValRemuneracao) 'HOLIVEIRA: Passa lConexao e o objeto oEndosso como parametro DT: 05/02/2007
                                                                                                            'Mathayde 30/03/2009 - Envia cValRemuneracao e passa para AtualizarAvaliacaoRestituicao para gravar em avaliacao_restituicao_tb

                'Inserindo em evento_emissao_tb '''''''''''''''''''''''''''''''''''''''''''''''''''
                Call InserirEvento(lPropostaId, CInt(lProdutoId), Val(oDadosModular.ramo_id), oDadosModular.EndossoId, lTpEndossoId_ALS)
            End If
        End If
    Next

    'Desbloqueando proposta'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    Call Atualizar_Bloqueio_Proposta(lPropostaId, "n")

    'Finalizando transa��o''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    rdocn.CommitTrans
    OEndosso.ConfirmarTransacao (lConexao) 'HOLIVEIRA: Confirma a transa��o DT: 05/02/2007
    OEndosso.FecharConexao (lConexao) '     HOLIVEIRA: Fecha a conexao DT: 05/02/2007
    
    '' rdocn.RollbackTrans
    '' oEndosso.RetornarTransacao (lConexao) 'HOLIVEIRA: Desfaz a transa��o DT: 05/02/2007
    '' oEndosso.FecharConexao (lConexao) 'HOLIVEIRA: Fecha a conexao DT: 05/02/2007

    'Destruindo objetos
    Set OEndosso = Nothing
    MousePointer = vbDefault
    
Exit Sub
TrataErro:

    rdocn.RollbackTrans
    OEndosso.RetornarTransacao (lConexao) 'HOLIVEIRA: Desfaz a transa��o DT: 05/02/2007
    OEndosso.FecharConexao (lConexao) 'HOLIVEIRA: Fecha a conexao DT: 05/02/2007
    
    'Destruindo objetos
    Set OEndosso = Nothing
    
    Call TrataErroGeral("AprovarRestituicao")
    Call TerminaSEGBR
    
End Sub


Public Function ObterClienteSubvencao(ByVal lProdutoId As Long) As Object

Dim sSQL As String
Dim oRs As rdoResultset
On Error GoTo TrataErro
    
    sSQL = ""
    adSQL sSQL, "SELECT ISNULL(cliente_subvencao_federal_id, 0) as cliente_subvencao_federal_id, "
    adSQL sSQL, "       ISNULL(cliente_subvencao_estadual_id, 0) as cliente_subvencao_estadual_id "
    adSQL sSQL, "  FROM produto_tb with(NOLOCK)"
    adSQL sSQL, " WHERE produto_Id = " & lProdutoId
            
    Set oRs = rdocn.OpenResultset(sSQL)

    If Not oRs.EOF Then
       Set ObterClienteSubvencao = oRs
    End If
      
    Set oRs = Nothing
                                                       
Exit Function
    
TrataErro:
    Call Err.Raise(Err.Number, , "ObterClienteSubvencao - " & Err.Description)
End Function



Public Sub CalcularValoresRestituicaoCliente(ByVal sFlagSubvencao As String, _
                                             ByVal lProdutoId As Long, _
                                             ByVal cValRestituicao As Currency, _
                                             ByVal cValIOFRest As Currency, _
                                             ByVal cValCustoApoliceCertifRest As Currency, _
                                             ByVal cValAdicFracionamentoRest As Currency, _
                                             ByVal cValDescontoRest As Currency, _
                                             ByVal cValPremioTarifaRest As Currency, _
                                             ByVal cValTotCorretagemRest As Currency, _
                                             ByVal cValProLaboreRest As Currency, _
                                             ByVal cValCustoApoliceCertifEst As Currency, _
                                             ByVal cValAdicFracionamentoEst As Currency, _
                                             ByVal cValDescontoEst As Currency, _
                                             ByVal cValIOFEst As Currency, _
                                             ByVal cValPremioTarifaEst As Currency, _
                                             ByVal cValRestituicaoEst As Currency, _
                                             ByVal cValTotCorretagemEst As Currency, _
                                             ByVal cValProLaboreEst As Currency, _
                                             ByVal cValIsRestituicao As Currency, _
                                             ByVal cValSubvencaoFederal As Currency, _
                                             ByVal cValSubvencaoEstadual As Currency, _
                                             ByRef colCliente As Collection, _
                                             Optional lClienteId As Long)


'## Calcula os valores de restitui��o para cada cliente - Rmarins 20/10/06

Dim oRs As rdoResultset
Dim oCliente As New Cliente

Dim cValRestituicaoAux As Currency
Dim dPercCli As Double

On Error GoTo TrataErro
        
    'Obtendo a informa��o do Cliente '''''''''''''''''''''''''''''''''''''''''''''''''''
    If sFlagSubvencao = "CLIENTE" Then
        oCliente.Cliente_Id = lClienteId
        oCliente.iTpEndossoId = 0
    Else
                
        Set oRs = ObterClienteSubvencao(lProdutoId)
            
        If Not oRs.EOF Then
        
            If sFlagSubvencao = "FEDERAL" Then
                oCliente.Cliente_Id = oRs("cliente_subvencao_federal_id")
                oCliente.iTpEndossoId = 269
                
            'bmoraes 08.03.2009 - estadual
            Else
                oCliente.Cliente_Id = oRs("cliente_subvencao_estadual_id")
                oCliente.iTpEndossoId = 269
            ''''''''''''''''''''''''''''''''''''''
            End If
            
            Set oRs = Nothing
            
        End If
        
    End If
    
    oCliente.Tp_Cliente = sFlagSubvencao
    
    'Encontrando o Fator para c�lculo dos valores ''''''''''''''''''''''''''''''''''''''''
    If sFlagSubvencao = "CLIENTE" Then
    
        cValRestituicaoAux = cValRestituicao - cValSubvencaoFederal - cValSubvencaoEstadual
        dPercCli = FrmSelEndosso.CalcularPercValor(cValPremioTarifaRest, cValPremioTarifaRest - cValSubvencaoFederal - cValSubvencaoEstadual)
        
    ElseIf sFlagSubvencao = "FEDERAL" Then
    
        dPercCli = FrmSelEndosso.CalcularPercValor(cValPremioTarifaRest, cValSubvencaoFederal)
        
    Else
        
        dPercCli = FrmSelEndosso.CalcularPercValor(cValPremioTarifaRest, cValSubvencaoEstadual)
    
    End If
    
    'Calculando os valores '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    If sFlagSubvencao = "CLIENTE" Then
        oCliente.Val_Restituicao = cValRestituicaoAux
        oCliente.Val_IOF_Rest = cValIOFRest
        oCliente.Val_Custo_Apolice_Rest = cValCustoApoliceCertifRest
        oCliente.Val_Adic_Frac_Rest = cValAdicFracionamentoRest
        oCliente.Val_Desconto_Rest = cValDescontoRest
        oCliente.Val_Premio_Tarifa_Rest = Trunca((cValPremioTarifaRest / 100) * dPercCli)
        oCliente.Val_Tot_Corretagem_Rest = Trunca((cValTotCorretagemRest / 100) * dPercCli)
        oCliente.Val_Pro_Labore_Rest = Trunca((cValProLaboreRest / 100) * dPercCli)
        
        oCliente.Val_Restituicao_Est = cValRestituicaoAux
        oCliente.Val_IOF_Est = cValIOFEst
        oCliente.Val_Custo_Apolice_Est = cValCustoApoliceCertifEst
        oCliente.Val_Adic_Frac_Est = cValAdicFracionamentoEst
        oCliente.Val_Desconto_Est = cValDescontoEst
        oCliente.Val_Premio_Tarifa_Est = Trunca((cValPremioTarifaEst / 100) * dPercCli)
        oCliente.Val_Tot_Corretagem_Est = Trunca((cValTotCorretagemEst / 100) * dPercCli)
        oCliente.Val_Pro_Labore_Est = Trunca((cValProLaboreEst / 100) * dPercCli)
        oCliente.Val_IS_Restituicao = cValIsRestituicao
    Else
        
        If sFlagSubvencao = "FEDERAL" Then
            oCliente.Val_Restituicao = cValSubvencaoFederal
            oCliente.Val_Premio_Tarifa_Rest = cValSubvencaoFederal
            oCliente.Val_Restituicao_Est = cValSubvencaoFederal
            oCliente.Val_Premio_Tarifa_Est = cValSubvencaoFederal
        Else
            oCliente.Val_Restituicao = cValSubvencaoEstadual
            oCliente.Val_Premio_Tarifa_Rest = cValSubvencaoEstadual
            oCliente.Val_Restituicao_Est = cValSubvencaoEstadual
            oCliente.Val_Premio_Tarifa_Est = cValSubvencaoEstadual
        End If
        
        oCliente.Val_IOF_Rest = 0
        oCliente.Val_Custo_Apolice_Rest = 0
        oCliente.Val_Adic_Frac_Rest = 0
        oCliente.Val_Desconto_Rest = 0
        oCliente.Val_Tot_Corretagem_Rest = Trunca((cValTotCorretagemRest / 100) * dPercCli)
        oCliente.Val_Pro_Labore_Rest = Trunca((cValProLaboreRest / 100) * dPercCli)
        
        oCliente.Val_IOF_Est = 0
        oCliente.Val_Custo_Apolice_Est = 0
        oCliente.Val_Adic_Frac_Est = 0
        oCliente.Val_Desconto_Est = 0
        oCliente.Val_Tot_Corretagem_Est = Trunca((cValTotCorretagemEst / 100) * dPercCli)
        oCliente.Val_Pro_Labore_Est = Trunca((cValProLaboreEst / 100) * dPercCli)
        
        oCliente.Val_IS_Restituicao = 0
    End If
    
    colCliente.Add oCliente
                   
    'Destruindo objeto
    Set oCliente = Nothing

    Exit Sub

TrataErro:
   Call Err.Raise(Err.Number, , "CalcularValoresRestituicaoCliente - " & Err.Description)

End Sub
Public Sub CalcularValoresRestituicaoClienteModular(ByVal sFlagSubvencao As String, _
                                                    ByVal lProdutoId As Long, _
                                                    ByVal cValRestituicao As Currency, _
                                                    ByVal cValIOFRest As Currency, _
                                                    ByVal cValCustoApoliceCertifRest As Currency, _
                                                    ByVal cValAdicFracionamentoRest As Currency, _
                                                    ByVal cValDescontoRest As Currency, _
                                                    ByVal cValPremioTarifaRest As Currency, _
                                                    ByVal cValTotCorretagemRest As Currency, _
                                                    ByVal cValProLaboreRest As Currency, _
                                                    ByVal cValCustoApoliceCertifEst As Currency, _
                                                    ByVal cValAdicFracionamentoEst As Currency, _
                                                    ByVal cValDescontoEst As Currency, _
                                                    ByVal cValIOFEst As Currency, _
                                                    ByVal cValPremioTarifaEst As Currency, _
                                                    ByVal cValRestituicaoEst As Currency, _
                                                    ByVal cValTotCorretagemEst As Currency, _
                                                    ByVal cValProLaboreEst As Currency, _
                                                    ByVal cValIsRestituicao As Currency, _
                                                    ByVal cValSubvencaoFederal As Currency, _
                                                    ByVal cValSubvencaoEstadual As Currency, _
                                                    ByRef colCliente As Collection, _
                                                    Optional lClienteId As Long)


'## Calcula os valores de restitui��o para cada cliente - Rmarins 20/10/06

Dim oRs As rdoResultset
Dim oCliente As New Cliente

Dim cValRestituicaoAux As Currency
Dim dPercCli As Double

On Error GoTo TrataErro
        
    'Obtendo a informa��o do Cliente '''''''''''''''''''''''''''''''''''''''''''''''''''
    If sFlagSubvencao = "CLIENTE" Then
        oCliente.Cliente_Id = lClienteId
        oCliente.iTpEndossoId = 0
    Else
                
        Set oRs = ObterClienteSubvencao(lProdutoId)
            
        If Not oRs.EOF Then
        
            If sFlagSubvencao = "FEDERAL" Then
                oCliente.Cliente_Id = oRs("cliente_subvencao_federal_id")
                oCliente.iTpEndossoId = 269
                
            'bmoraes 08.03.2009 - estadual
            Else
                oCliente.Cliente_Id = oRs("cliente_subvencao_estadual_id")
                oCliente.iTpEndossoId = 269
            ''''''''''''''''''''''''''''''''''''''
            End If
            
            Set oRs = Nothing
            
        End If
        
    End If
    
    oCliente.Tp_Cliente = sFlagSubvencao
    
    'Encontrando o Fator para c�lculo dos valores ''''''''''''''''''''''''''''''''''''''''
    If sFlagSubvencao = "CLIENTE" Then
    
        cValRestituicaoAux = cValRestituicao - cValSubvencaoFederal - cValSubvencaoEstadual
        dPercCli = FrmSelEndosso.CalcularPercValor(cValPremioTarifaRest, cValPremioTarifaRest - cValSubvencaoFederal - cValSubvencaoEstadual)
        
    ElseIf sFlagSubvencao = "FEDERAL" Then
    
        dPercCli = FrmSelEndosso.CalcularPercValor(cValPremioTarifaRest, cValSubvencaoFederal)
        
    Else
        
        dPercCli = FrmSelEndosso.CalcularPercValor(cValPremioTarifaRest, cValSubvencaoEstadual)
    
    End If
    
    'Calculando os valores '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    If sFlagSubvencao = "CLIENTE" Then
        oCliente.Val_Restituicao = cValRestituicaoAux
        oCliente.Val_IOF_Rest = cValIOFRest
        oCliente.Val_Custo_Apolice_Rest = cValCustoApoliceCertifRest
        oCliente.Val_Adic_Frac_Rest = cValAdicFracionamentoRest
        oCliente.Val_Desconto_Rest = cValDescontoRest
        oCliente.Val_Premio_Tarifa_Rest = Trunca((cValPremioTarifaRest / 100) * dPercCli)
        oCliente.Val_Tot_Corretagem_Rest = Trunca((cValTotCorretagemRest / 100) * dPercCli)
        oCliente.Val_Pro_Labore_Rest = Trunca((cValProLaboreRest / 100) * dPercCli)
        
        oCliente.Val_Restituicao_Est = cValRestituicaoAux
        oCliente.Val_IOF_Est = cValIOFEst
        oCliente.Val_Custo_Apolice_Est = cValCustoApoliceCertifEst
        oCliente.Val_Adic_Frac_Est = cValAdicFracionamentoEst
        oCliente.Val_Desconto_Est = cValDescontoEst
        oCliente.Val_Premio_Tarifa_Est = Trunca((cValPremioTarifaEst / 100) * dPercCli)
        oCliente.Val_Tot_Corretagem_Est = Trunca((cValTotCorretagemEst / 100) * dPercCli)
        oCliente.Val_Pro_Labore_Est = Trunca((cValProLaboreEst / 100) * dPercCli)
        oCliente.Val_IS_Restituicao = cValIsRestituicao
    Else
        
        If sFlagSubvencao = "FEDERAL" Then
            oCliente.Val_Restituicao = cValSubvencaoFederal
            oCliente.Val_Premio_Tarifa_Rest = cValSubvencaoFederal
            oCliente.Val_Restituicao_Est = cValSubvencaoFederal
            oCliente.Val_Premio_Tarifa_Est = cValSubvencaoFederal
        Else
            oCliente.Val_Restituicao = cValSubvencaoEstadual
            oCliente.Val_Premio_Tarifa_Rest = cValSubvencaoEstadual
            oCliente.Val_Restituicao_Est = cValSubvencaoEstadual
            oCliente.Val_Premio_Tarifa_Est = cValSubvencaoEstadual
        End If
        
        oCliente.Val_IOF_Rest = 0
        oCliente.Val_Custo_Apolice_Rest = 0
        oCliente.Val_Adic_Frac_Rest = 0
        oCliente.Val_Desconto_Rest = 0
        oCliente.Val_Tot_Corretagem_Rest = Trunca((cValTotCorretagemRest / 100) * dPercCli)
        oCliente.Val_Pro_Labore_Rest = Trunca((cValProLaboreRest / 100) * dPercCli)
        
        oCliente.Val_IOF_Est = 0
        oCliente.Val_Custo_Apolice_Est = 0
        oCliente.Val_Adic_Frac_Est = 0
        oCliente.Val_Desconto_Est = 0
        oCliente.Val_Tot_Corretagem_Est = Trunca((cValTotCorretagemEst / 100) * dPercCli)
        oCliente.Val_Pro_Labore_Est = Trunca((cValProLaboreEst / 100) * dPercCli)
        
        oCliente.Val_IS_Restituicao = 0
    End If
    
    colCliente.Add oCliente
                   
    'Destruindo objeto
    Set oCliente = Nothing

    Exit Sub

TrataErro:
   Call Err.Raise(Err.Number, , "CalcularValoresRestituicaoCliente - " & Err.Description)

End Sub
'Inclus�o realizada em 11/08/2006 - crosendo
Public Function ObterTp_Endosso(ByVal sPropostaId As String, _
                                ByVal sEndossoId As Integer, _
                                ByRef lCodEventoALS As Long) As Integer
Dim sSQL As String
Dim Rst As rdoResultset

On Error GoTo TrataErro

    sSQL = ""
    sSQL = sSQL & " SELECT endosso_tb.tp_endosso_id, " & vbNewLine
    sSQL = sSQL & "        tp_endosso_tb.cod_evento_als " & vbNewLine 'Rmarins - Inclu�do em 20/10/06
    sSQL = sSQL & " FROM endosso_tb with(NOLOCK) " & vbNewLine 'HOLIVEIRA: with(NOLOCK) para tabela em transa��o DT: 05/02/2007
    sSQL = sSQL & " JOIN tp_endosso_tb with(NOLOCK) " & vbNewLine 'HOLIVEIRA: with(NOLOCK) para tabela em transa��o DT: 05/02/2007
    sSQL = sSQL & "   ON endosso_tb.tp_endosso_id = tp_endosso_tb.tp_endosso_id "
    sSQL = sSQL & " WHERE endosso_tb.proposta_id = " & sPropostaId & vbNewLine
    sSQL = sSQL & "   AND endosso_tb.endosso_id = " & sEndossoId & vbNewLine
    
    Set Rst = rdocn.OpenResultset(sSQL)

    If Not Rst.EOF Then
       ObterTp_Endosso = Val(Rst(0))
       lCodEventoALS = Rst(1)
    Else
       ObterTp_Endosso = 0
       lCodEventoALS = 0
    End If

    Set Rst = Nothing
    
  Exit Function

TrataErro:
    Call Err.Raise(Err.Number, , "ConsultarTp_Endosso - " & Err.Description)
    
End Function

Private Sub AprovarAvaliacaoRestituicao(ByVal lPropostaId As Long, _
                                        ByVal lNumAvaliacao As Long, _
                                        ByVal lEndossoId As Long, _
                                        ByVal lConexao As Long, _
                                        ByRef OEndosso As Object, _
                                        ByRef cValRemuneracao As Currency) 'HOLIVEIRA: Recebe lConexao e o objeto oEndosso como parametro DT: 05/02/2007
                                        'Mathayde 30/03/2009 - Recebendo cValRemuneracao e passa para AtualizarAvaliacaoRestituicao

' bcarneiro - 03/12/2003 - Projeto Restitui��o por Iniciativa da Seguradora

Const SITUACAO_APROVACAO_TECNICA = "A"

Dim oFinanceiro As Object

On Error GoTo TrataErro

    'Recusando avalia��o t�cnica''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    
    'HOLIVEIRA: Cria o objeto oFinanceiro a partir do objeto oEndosso, para manter a
    '           mesma transa��o. DT: 05/02/2007
    'Set oFinanceiro = CreateObject("SEGL0026.cls00125")
    Set oFinanceiro = OEndosso.CriaObjFinanceiro
    
    Call oFinanceiro.AtualizarAvaliacaoRestituicao(gsSIGLASISTEMA, _
                                                   App.Title, _
                                                   App.FileDescription, _
                                                   glAmbiente_id, _
                                                   cUserName, _
                                                   Data_Sistema, _
                                                   lPropostaId, _
                                                   lNumAvaliacao, _
                                                   SITUACAO_APROVACAO_TECNICA, _
                                                   , _
                                                   , _
                                                   , _
                                                   , _
                                                   , _
                                                   , _
                                                   , _
                                                   , _
                                                   , _
                                                   , _
                                                   lEndossoId, _
                                                   , _
                                                   , _
                                                   , _
                                                   , _
                                                   lConexao, cValRemuneracao) 'HOLIVEIRA: Passa lConexao como parametro DT: 05/02/2007
                                                    'Mathayde 30/03/2009 - Passa cValRemuneracao para gravar em avaliacao_restituicao_tb
    
    'HOLIVEIRA: Destroi o objeto oFinanceiro DT: 05/02/2007
    OEndosso.DestroiObjFinanceiro
    Set oFinanceiro = Nothing
    
    'Atualizando interface'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    MousePointer = vbDefault
    
    Exit Sub

TrataErro:
   Call TrataErroGeral("AprovarAvaliacaoRestituicao")
   Call TerminaSEGBR
    
End Sub

Public Sub CarregarRateioCorretagemRestituicao(ByVal lPropostaId As Long, _
                                               ByVal lNumAvaliacao As Long, _
                                               ByVal cValFinanceiro As Currency, _
                                               ByVal cValIOF As Currency, _
                                               ByVal cValCustoApoliceCertif As Currency, _
                                               ByVal cPercIR As Currency)


' bcarneiro - 08/12/2003 - Projeto Restitui��o por Iniciativa da Seguradora

Dim oFinanceiro As Object

On Error GoTo TrataErro

    'Rateando valor de corretagem'''''''''''''''''''''''''''''''''''''''''''''''''''''
    
    Set oFinanceiro = CreateObject("SEGL0026.cls00125")
    
    Set Corretagens = oFinanceiro.ObterAvaliacaoRestituicaoCorretagem(gsSIGLASISTEMA, _
                                                                      App.Title, _
                                                                      App.FileDescription, _
                                                                      glAmbiente_id, _
                                                                      lPropostaId, _
                                                                      lNumAvaliacao, _
                                                                      cValFinanceiro, _
                                                                      cValIOF, _
                                                                      cValCustoApoliceCertif, _
                                                                      cPercIR)

    If bModular = True Then
        If Produto_id = 1168 Then
            If lNumAvaliacao = oDadosModular.Num_avaliacao Then
                oDadosModular.ValIOFRest = cValIOF
            End If
        Else
            If oDadosModular.proposta_id = lPropostaId Then
                oDadosModular.ValIOFRest = cValIOF
            End If
        End If
    End If
    
    Set oFinanceiro = Nothing
    
    Exit Sub
    
TrataErro:
    Call TrataErroGeral("CarregarRateioCorretagemRestituicao")
    Call TerminaSEGBR
    
End Sub

Public Sub CarregarCongeneres(ByVal lPropostaId As Long)

'Demanda 400422 - emaior 02/12/2008 - Preencher cole��o com Restitui��o de Pendentes Cedidos

Dim oRestituicao As Object

On Error GoTo TrataErro
    
    Set oRestituicao = CreateObject("SEGL0026.cls00125")
    
    Set Congeneres = oRestituicao.ObterCongeneres(gsSIGLASISTEMA, _
                                                  App.Title, _
                                                  App.FileDescription, _
                                                  glAmbiente_id, _
                                                  lPropostaId)
    
    Set oRestituicao = Nothing
    
    Exit Sub
    
TrataErro:
    Call TrataErroGeral("CarregarCongeneres")
    Call TerminaSEGBR
    
End Sub

Public Function ObterPercentualTotalCorretagem(ByVal lPropostaId As Long) As Double

Dim oCorretagem As Object
Dim dTotCorretagem As Double

' bcarneiro - 08/12/2003 - Projeto Restitui��o por Iniciativa da Seguradora
On Error GoTo TrataErro


    dTotCorretagem = 0
    For Each oCorretagem In Corretagens
        dTotCorretagem = dTotCorretagem + oCorretagem.PercComissao
    Next
    ObterPercentualTotalCorretagem = dTotCorretagem

    Set oCorretagem = Nothing

    
    Exit Function
    
TrataErro:
    Call TrataErroGeral("ObterPercentualTotalCorretagem")
    Call TerminaSEGBR
    
End Function

Public Sub InserirEvento(lPropostaId As Long, _
                         iProdutoId As Integer, _
                         iRamoId As Integer, _
                         lEndossoId As Long, _
                         iTpEndossoId As Integer)

On Error GoTo TrataErro

  Call IncluirEventoEmissao(lPropostaId, _
                            lEndossoId, _
                            iProdutoId, _
                            iRamoId, _
                            iTpEndossoId, _
                            App.EXEName, _
                            Data_Sistema, _
                            cUserName, _
                            rdocn)
                            
  Exit Sub

TrataErro:
   Call TrataErroGeral("InserirEvento")
   Call TerminaSEGBR
End Sub


Sub ProcessarEmissaoEndosso(ByVal lPropostaId As Long, _
                            ByVal lProdutoId As Long, _
                            ByVal lNumAvaliacao As Long, _
                            ByVal lClienteId As Long, _
                            ByVal lRestEndossoId As Long, _
                            ByVal sFlagTpCliente As String, _
                            ByVal cValRestituicao As Currency, _
                            ByVal cValIOFRest As Currency, _
                            ByVal cValCustoApoliceCertifRest As Currency, _
                            ByVal cValTotCorretagemRest As Currency, _
                            ByVal cValProLaboreRest As Currency, _
                            ByVal cValPremioTarifaRest As Currency, _
                            ByVal cValDescontoRest As Currency, _
                            ByVal cValAdicFracionamentoRest As Currency, _
                            ByVal cValIsRestituicao As Currency, _
                            ByVal cValCambio As Currency, _
                            ByVal sDtPedidoAvaliacao As String, _
                            ByVal sTextoEndosso As String, _
                            ByRef iOrigemProposta As Integer, _
                            ByVal iTpEndossoId As Integer, _
                            ByRef lEndossoId As Long, _
                            ByVal lConexao As Long, _
                            ByRef OEndosso As Object, _
                            ByRef cValRemuneracao As Currency) 'HOLIVEIRA: Recebe lConexao e o objeto oEndosso como parametro DT: 05/02/2007
                            'Mathayde 30/03/2009 - Recebe cValRemuneracao para calcular o cValIrRemuneracao

Const TP_OPERACAO_RESTITUICAO = "03"

Dim sRamoId As String
Dim sConvenio As String
Dim sFlagAprovacao As String

Dim iMoedaPremioId As Integer
Dim cPercIR As Currency
Dim cValTotIRCorretagem As Currency
Dim cValRepasseBB As Currency
Dim bFlagRetemComissao As Boolean
Dim bFlagRetemProLabore As Boolean
Dim bFlagRetemIOF As Boolean

Dim oProposta As Object


'mathayde 27/03/2009
Dim cValIrRemuneracao As Currency
Dim cPercIRRemun As Currency

'pmelo 20110524 - Tratamento 987 para acerto dos valores de movimentacao
Dim lAdmClienteId As Long


'HOLIVEIRA: Recebe o objeto oEndosso como parametro DT: 05/02/2007
'Dim oEndosso As Object

Dim oObjetoVazioAgendamento As Object

'Demanda 400422 - emaior 02/12/2008 - Relat�rio de Restitui��o de Comiss�o de Cosseguro
Dim sTipoEmissao As String
Dim lSeguradora As Long

On Error GoTo TrataErro

    'Obtendo dados da Proposta ''''''''''''''''''''''''''''''''''''''''''''''''''''
    'Demanda 400422 - emaior 02/12/2008 - Obtendo tamb�m tipo de emiss�o e seguradora da proposta
    Call Obter_Dados_Adicionais_Proposta(lPropostaId, sRamoId, 0, , iMoedaPremioId, sTipoEmissao, lSeguradora)
    
    Call Obtem_Perc_IR(sRamoId, Data_Sistema, cPercIR)
        
    'Obtendo rateio de corretagem ''''''''''''''''''''''''''''''''''''''''''''''''''
    Call CarregarRateioCorretagemRestituicao(lPropostaId, _
                                             lNumAvaliacao, _
                                             cValRestituicao, _
                                             cValIOFRest, _
                                             cValCustoApoliceCertifRest, _
                                             cPercIR)
                                             
    'Demanda 400422 - emaior 02/12/2008 - Relat�rio de Restitui��o de Comiss�o de Cosseguro
    Call CarregarCongeneres(lPropostaId)
    
    cValTotIRCorretagem = Trunca(cValTotCorretagemRest * cPercIR)
        
    'mathayde 27/03/2009 - Calcula o valor do IR de remunera��o
    cPercIRRemun = ObterPercIRRemuneracao(sRamoId, Data_Sistema)
    cValIrRemuneracao = Trunca(cValRemuneracao * cPercIRRemun)
    
    
    'Obtendo valor de repasse para o BB''''''''''''''''''''''''''''''''''''''''''''''
    sConvenio = Obtem_Numero_do_Convenio(Val(lProdutoId), Val(sRamoId), 1, iMoedaPremioId)
    
    'Call VerificarRetencao(sConvenio, bFlagRetemComissao, bFlagRetemIOF)
    Call VerificarRetencao(sConvenio, bFlagRetemComissao, bFlagRetemIOF, bFlagRetemProLabore)  'RMAURI - Incluido variaveis bFlagRetemProLabore
    
    'pmelo 20110524 - Tratamento 987 para acerto dos valores de movimentacao
    lAdmClienteId = Obter_Estipulante(lPropostaId)
    
    'N�o restituir IR para estipulante FENAB FED NAC A BCO BRASIL
    'Altera��o realizada para deixar a variavel compativel com o valor recebido em 27/05/2011 - Inicio - Confitec
    'If lAdmClienteId = 192191 Then

    If ((lAdmClienteId = 192191) Or (lAdmClienteId = 2452951)) And ((lProdutoId = 12) Or (lProdutoId = 15) Or (lProdutoId = 121) Or (lProdutoId = 135) Or (lProdutoId = 136) Or (lProdutoId = 716) Or (lProdutoId = 721)) Then
    'If lAdmClienteId = 192191 Then
    'Altera��o realizada para deixar a variavel compativel com o valor recebido em 27/05/2011 - Fim - Confitec
        cValRepasseBB = CalcularRepasseSemIrEstipulante(cValTotCorretagemRest, _
                                                        cValProLaboreRest, _
                                                        cValRestituicao, _
                                                        cPercIR, _
                                                        cValIOFRest, _
                                                        bFlagRetemComissao, _
                                                        bFlagRetemIOF)
    
                                        
    Else
    
        'pmelo 20110524 - Tratamento 987 para acerto dos valores de movimentacao
        cValRepasseBB = CalcularRepasse(cValTotCorretagemRest, _
                                        cValProLaboreRest, _
                                        cValRestituicao, _
                                        cPercIR, _
                                        cValIOFRest, _
                                        bFlagRetemComissao, _
                                        bFlagRetemIOF)
    
    End If
    
    
    'mathayde 27/03/2009 - Calcula o cValIrRemuneracao e cValRemuneracao em cValRepasseBB
    cValRepasseBB = cValRepasseBB + cValIrRemuneracao - cValRemuneracao
    
    
    Set oProposta = CreateObject("SEGL0022.cls00117")  'crosendo - Altera��o realizada em 11/08/2006
    
    Set oRsProposta = oProposta.ConsultarProposta(gsSIGLASISTEMA, _
                                                  App.Title, _
                                                  App.FileDescription, _
                                                  glAmbiente_id, _
                                                  lPropostaId)
    '
    If Not oRsProposta.EOF Then

        iOrigemProposta = IIf(IsNull(oRsProposta("origem_proposta_id")), 0, oRsProposta("origem_proposta_id"))
                
        If iOrigemProposta = 2 Then  'ALS
           sFlagAprovacao = "S"
        Else
           sFlagAprovacao = "N"
        End If
        
        'RMauri 22-06-2007 - Flow 212774
        'Gravar como aprovado para produtos que n�o possuem proposta_Bb, para gerar pagamentos de restitui��o sem aprova��o do BB
        Select Case CDbl("0" + oRsProposta("produto_id"))
        ' racras inclus�o produtos BBC
'            Case 123, 400, 12, 121, 135, 136, 716 'Produtos on-line - Produtos BBC     'AKIO OKUNO - FLOWBR11332043 - 07/07/2011
            Case 123, 400, 12, 121, 135, 136, 716, 223 'Produtos on-line - Produtos BBC
                sFlagAprovacao = "S"
            Case Else
                sFlagAprovacao = sFlagAprovacao
        End Select
        
    End If
    
    '***********************************************************************************************************************
    '***********************************************************************************************************************
    'Rely ------------------------------------------------------------------------------------------------------------------
    'Verificando se o tp_endosso = 304 para gerar um endosso 304 apenas para controle
    ' SELECT para nao midificar a avriavel iTpEndossoId recebida como parametro
'    Set oFaturamento = CreateObject("SEGL0022.cls00117")
'
'    Set oFaturamento = oFaturamento.ConsultarFaturamento(gsSIGLASISTEMA, _
'                                                  App.Title, _
'                                                  App.FileDescription, _
'                                                  glAmbiente_id, _
'                                                  lPropostaId, _
'                                                  lNumAvaliacao)
'
'     If Not oFaturamento.EOF Then
'
'        If IsNull(oFaturamento("tp_endosso_id")) = False Then
'
'            If oFaturamento("tp_endosso_id") = 304 Then
'
'                    Dim Dt_Pedido_Avaliacao As String
'                    Dt_Pedido_Avaliacao = oFaturamento("dt_pedido_avaliacao")
'
'                            'GERAR ENDOSSO ALS (RELY)
'
'                             Set oFaturamento = CreateObject("SEGL0026.cls00124")
'
'                             Call oFaturamento.GerarEndossoAls(cUserName, _
'                                                                            gsSIGLASISTEMA, _
'                                                                            App.Title, _
'                                                                            App.FileDescription, _
'                                                                            glAmbiente_id, _
'                                                                            Data_Sistema, _
'                                                                            lPropostaId, _
'                                                                            Format(Data_Sistema, "yyyymm"), _
'                                                                            Dt_Pedido_Avaliacao, _
'                                                                            0, _
'                                                                            0, _
'                                                                            304, _
'                                                                            1, _
'                                                                            0, _
'                                                                            3818, _
'                                                                            , _
'                                                                            lConexao)
'
'
'
'
'
'                          ' Atualiza question�rio ALS (RELY)
'                          Set oFaturamento = CreateObject("SEGL0026.cls00124")
'
'                          Call oFaturamento.AtualizaQuestionarioALS(gsSIGLASISTEMA, _
'                                                  App.Title, _
'                                                  App.FileDescription, _
'                                                  glAmbiente_id, _
'                                                  lPropostaId, _
'                                                  lConexao)
'
'
''                    End If
'
'            End If
'
'        End If
'
'     End If
'
'     Set oFaturamento = Nothing
'    '------------------------------------------------------------------------------------------------------------------------
    '************************************************************************************************************************
    '************************************************************************************************************************
                                                                                                                       
    If iTpEndossoId = 0 Then
        iTpEndossoId = 80 'Endosso de Restitui��o a Pedido da Seguradora
    End If
    
    If sFlagTpCliente <> "CLIENTE" Then 'Caso seja um endosso
        lRestEndossoId = 0
    Else
    
    End If
    
    Set oObjetoVazioAgendamento = New Collection
    
    'HOLIVEIRA: Recebe o objeto oEndosso como parametro DT: 05/02/2007
    'Set oEndosso = CreateObject("SEGL0026.cls00124")
    
    'Gerando endosso '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    'HOLIVEIRA: Passa lConexao como parametro DT: 05/02/2007
    lEndossoId = OEndosso.EmitirEndosso(cUserName, gsSIGLASISTEMA, App.Title, App.FileDescription, glAmbiente_id, _
                                        0, 0, lSeguradora, lPropostaId, Val(sRamoId), cValRestituicao, _
                                        iTpEndossoId, "n", 0, 0, 1, cValPremioTarifaRest, _
                                        TP_OPERACAO_RESTITUICAO, "", "", "", _
                                        cValCustoApoliceCertifRest, cValTotCorretagemRest, _
                                        0, _
                                        cValProLaboreRest, cValIOFRest, _
                                        cValTotIRCorretagem, cValDescontoRest, _
                                        cValAdicFracionamentoRest, "", _
                                        iMoedaPremioId, iMoedaPremioId, _
                                        cValCambio, cValIsRestituicao, _
                                        sDtPedidoAvaliacao, Data_Sistema, _
                                        lClienteId, cPercIR, cValRepasseBB, _
                                        0, 0, _
                                        0, Corretagens, _
                                        oObjetoVazioAgendamento, _
                                        0, _
                                        lConexao, _
                                        , sFlagAprovacao, sTextoEndosso, lRestEndossoId, , , , , bFlagRetemComissao, bFlagRetemProLabore, , Congeneres, sTipoEmissao)
                                        'RMauri 13/02/2007 - Incluido variaveis bFlagRetemComissao, bFlagRetemProLabore
                                        'Demanda 400422 - emaior 02/12/2008 - objeto Congeneres e sTipoEmissao como par�metros
    
    
    If (bModular = True And lPropostaId = oDadosModular.proposta_id And lProdutoId <> 1168) Or (lNumAvaliacao = oDadosModular.Num_avaliacao) Then
        oDadosModular.EndossoId = lEndossoId
    End If
    
    
    
    'Mathayde 27/03/2009 - A rotina EmitirEndosso n�o tem espa�o para passagem de par�metros
    'Ent�o � inserido em endosso_financeiro_tb e ap�s atualizado remunera��o e ir remunera��o
     
     Call OEndosso.AtualizarValoresDeRemuneracao(gsSIGLASISTEMA, _
                                                glAmbiente_id, _
                                                App.Title, _
                                                App.FileDescription, _
                                                lPropostaId, _
                                                lEndossoId, _
                                                cValRemuneracao, _
                                                cValIrRemuneracao, _
                                                lConexao)
                                                'Mathayde  30/03/2009 - Envia cValRemuneracao e cValIrRemuneracao para atualizar em endosso_financeiro_tb
                                                


    'Destruindo objetos
    oRsProposta.Close
    Set oRsProposta = Nothing
    Set oProposta = Nothing
    
    'HOLIVEIRA: Recebe o objeto oEndosso como parametro DT: 05/02/2007
    'Set oEndosso = Nothing

    Set colCorretagem = Nothing  'Maur�cio (Stefanini), 24/01/2006 - FLOWBR 120082
    
    Set colCongeneres = Nothing  'Demanda 400422 - emaior 02/12/2008
    
    
    

Exit Sub

TrataErro:
    Call Err.Raise(Err.Number, , "ProcessarEmissaoEndosso - " & Err.Description)
    
End Sub


'mathayde - solicita��o e-mail bruno
          
 Function ObterPercIRRemuneracao(ByVal iRamo, ByVal PDtReferencia As String) As Currency
                                    
     
Dim SQL As String
Dim Rst As rdoResultset

    SQL = " SELECT valor "
    SQL = SQL & " From val_item_financeiro_ramo_TB "
    SQL = SQL & " WHERE cod_item_financeiro = 'IR_REMUN' "
    SQL = SQL & " AND ramo_id = " & iRamo
    SQL = SQL & " and dt_inicio_vigencia <= '" & DtReferencia & "'"
    SQL = SQL & " and (dt_fim_vigencia >= '" & DtReferencia & "' or dt_fim_vigencia is Null)"
    
    
    Set Rst = rdocn.OpenResultset(SQL)
    If Rst.EOF Then
        ObterPercIRRemuneracao = 0
    Else
        ObterPercIRRemuneracao = (Rst!valor)
    End If
    Rst.Close

End Function



    
'Function FormataValorParaPadraoBrasil(valor As String) As String



   
'   Dim ConfDecimal As String
   
'   valor = MudaVirgulaParaPonto(valor)
   
'   ConfDecimal = LeArquivoIni("WIN.INI", "intl", "sDecimal")
   
   'Convertendo valor do d�bito para o padr�o brasileiro
   
'   If ConfDecimal = "." Then
'      valor = TrocaValorAmePorBras(Format(Val(valor), "###,###,##0.00"))
'   Else
'      valor = Format(Val(valor), "###,###,##0.00")
'   End If

'   FormataValorParaPadraoBrasil = valor
   
'End Function

'pmelo 20110524 - Tratamento 987
'Obter_Estipulante (lPropostaId)
Public Function Obter_Estipulante(ByVal lPropostaId As Long) As Long

Dim sSQL As String
Dim oRs As rdoResultset
On Error GoTo TrataErro
    
    
    sSQL = ""
    adSQL sSQL, "SELECT adm_cliente_id "
    adSQL sSQL, "  FROM seguros_db..administracao_apolice_tb with(NOLOCK)                                  "
    adSQL sSQL, " WHERE administracao_apolice_tb.dt_fim_administracao IS NULL                          "
    adSQL sSQL, "   AND proposta_id                                    = " & lPropostaId
    adSQL sSQL, "UNION                                                                                 "
    adSQL sSQL, "SELECT administracao_apolice_tb.adm_cliente_id                                        "
    adSQL sSQL, "  FROM seguros_db..proposta_adesao_tb proposta_adesao_tb             with(NOLOCK)         "
    adSQL sSQL, "  JOIN seguros_db..apolice_tb apolice_tb                             with(NOLOCK)         "
    adSQL sSQL, "    ON apolice_tb.apolice_id                          = proposta_adesao_tb.apolice_id "
    adSQL sSQL, "   AND apolice_tb.ramo_id                             = proposta_adesao_tb.ramo_id    "
    adSQL sSQL, "  JOIN seguros_db..administracao_apolice_tb administracao_apolice_tb with(NOLOCK)         "
    adSQL sSQL, "    ON administracao_apolice_tb.proposta_id           = apolice_tb.proposta_id        "
    adSQL sSQL, " WHERE administracao_apolice_tb.dt_fim_administracao IS NULL                          "
    adSQL sSQL, "   AND proposta_adesao_tb.proposta_id                 = " & lPropostaId
    
    Set oRs = rdocn.OpenResultset(sSQL)

    If Not oRs.EOF Then
       Obter_Estipulante = oRs!adm_cliente_id
    Else
       Obter_Estipulante = 0
    End If
      
    Set oRs = Nothing
                                                       
Exit Function
    
TrataErro:
    Call Err.Raise(Err.Number, , "Obter_Estipulante - " & Err.Description)
End Function


'pmelo 20110524 - Tratamento 987
Public Function CalcularRepasseSemIrEstipulante(ByVal cValComissao As Currency, _
                                     ByVal cValComissaoEstipulante As Currency, _
                                     ByVal cValorRestituicao As Currency, _
                                     ByVal dPercentualIR As Double, _
                                     ByVal cValorIOF As Currency, _
                                     ByVal bFlagRetemComissao As Boolean, _
                                     ByVal bFlagRetemIOF As Boolean) As Currency

    'pcarvalho - 04/06/2003
    'Objetivo: Padroniza��o do c�lculo de restitui��o

    Dim cValorIRCorretor As Currency
    Dim cValorIREstipulante As Currency
    Dim cValRepasse As Currency

    On Error GoTo TrataErro


    ''## Alterado por Alexandre Ricardo em 11/07/2003 - Confomre discutido entre J�nior e Ana Cl�udia
    ''## O Valor de Restitui��o n�o � somado ao Valor de IR.
    ''## cValRestituicaoAux = cValorRestituicao + cValorIRCorretor + cValorIREstipulante
    cValRepasse = cValorRestituicao

    'Se houver reten��o de comiss�o, retirar comiss�o de corretagem e estipulante
    'e aplicar o valor do IR de corretagem e o IR de estipulante
    If bFlagRetemComissao Then

        'Obtendo o valor de IR de corretagem e do estipulante
        cValorIRCorretor = TruncaDecimal(cValComissao * dPercentualIR)
        cValorIREstipulante = 0 'TruncaDecimal(cValComissaoEstipulante * dPercentualIR)
        
        cValRepasse = cValRepasse - cValComissao - cValComissaoEstipulante _
                                  + cValorIRCorretor + cValorIREstipulante

    End If

    'Se houver reten��o de IOF, retirar o valor do IOF
    If bFlagRetemIOF Then
        cValRepasse = cValRepasse - cValorIOF
    End If

    'Retornando o valor do repasse
    CalcularRepasseSemIrEstipulante = cValRepasse

    Exit Function

TrataErro:
    Call Err.Raise(Err.Number, , "CalcularRepasseSemIrEstipulante - " & Err.Description)


End Function


'Renato Vasconcelos

'Flow:11022639
'INICIO
'Alan Neves - C_ANEVES - Demanda: 17860335 - 29/05/2013 - Inicio
'Sub AtualizaDadosRestituicao(lProposta As Long, iProduto As Integer)
'    On Error GoTo TrataErro
'    Dim sSQL As String
'    Dim rc As rdoResultset
'
'    sSQL = ""
'    sSQL = sSQL & "select * from #avaliacao_restituicao_tb where atualizado = 'N' " & vbNewLine
'
'    If lProposta > 0 Then
'        sSQL = sSQL & "AND proposta_id = " & CStr(lProposta) & vbNewLine
'    ElseIf iProduto > 0 Then
'        sSQL = sSQL & "AND produto_id = " & CStr(iProduto) & vbNewLine
'    End If
'
'    Set rc = rdocn.OpenResultset(sSQL)
'
'    If rc.EOF Then
'        Exit Sub
'    End If
'    rc.Close
'    Set rc = Nothing
'
'    'Endosso
'    sSQL = ""
'    sSQL = sSQL & " Set Nocount On " & vbNewLine
'    sSQL = sSQL & "  Update #avaliacao_restituicao_tb Set DT_Emissao  = Convert(Varchar(10),Endosso_tb.Dt_emissao,103)," & vbNewLine
'    sSQL = sSQL & "                                       Dt_inclusao = Convert(VarChar(10), ENDOSSO_TB.Dt_inclusao, 103)" & vbNewLine
'    sSQL = sSQL & "         From ENDOSSO_TB(Nolock)" & vbNewLine
'    sSQL = sSQL & "         where #avaliacao_restituicao_tb.ENDOSSO_ID  = ENDOSSO_TB.ENDOSSO_ID" & vbNewLine
'    sSQL = sSQL & "         and #avaliacao_restituicao_tb.proposta_id = ENDOSSO_TB.proposta_id" & vbNewLine
'    sSQL = sSQL & "         and #avaliacao_restituicao_tb.atualizado = 'N'" & vbNewLine
'    If lProposta > 0 Then
'        sSQL = sSQL & "AND #avaliacao_restituicao_tb.proposta_id = " & CStr(lProposta) & vbNewLine
'    ElseIf iProduto > 0 Then
'        sSQL = sSQL & "AND #avaliacao_restituicao_tb.produto_id = " & CStr(iProduto) & vbNewLine
'    End If
'
'    Set rc = rdocn.OpenResultset(sSQL)
'    rc.Close
'    Set rc = Nothing
'
'
'    'Proposta_fechada/adesao
'
'    sSQL = sSQL & " Update #avaliacao_restituicao_tb Set     dt_inicio_vigencia  = Convert(Varchar(10), Coalesce(Apolice_tb.dt_inicio_vigencia , proposta_adesao_tb.dt_inicio_vigencia , DateAdd(Month, 12, Proposta_tb.Dt_Contratacao)),103)," & vbNewLine
'    sSQL = sSQL & "                                          dt_fim_vigencia = Convert(Varchar(10), Coalesce(Apolice_tb.dt_fim_vigencia, proposta_adesao_tb.dt_fim_vigencia ),103)," & vbNewLine
'    sSQL = sSQL & "                                          Origem = Case When Isnull(Apolice_tb.dt_fim_vigencia,'') = '' Then 1 Else 2 End," & vbNewLine
'    sSQL = sSQL & "                                          ValIs  = Coalesce(proposta_fechada_tb.val_is,proposta_adesao_tb.val_is)," & vbNewLine
'    sSQL = sSQL & "                                          FatorCambio = Coalesce(proposta_fechada_tb.seguro_moeda_id,proposta_adesao_tb.seguro_moeda_id)," & vbNewLine
'    sSQL = sSQL & "                                          proposta_bb = Coalesce(Proposta_Fechada_tb.proposta_bb, Proposta_Adesao_tb.proposta_bb)" & vbNewLine
'    sSQL = sSQL & "                                          From Apolice_tb(Nolock)" & vbNewLine
'    sSQL = sSQL & "                                            RIGHT JOIN #avaliacao_restituicao_tb (Nolock)" & vbNewLine
'    sSQL = sSQL & "                                           ON #avaliacao_restituicao_tb.Proposta_id  = Apolice_tb.Proposta_id" & vbNewLine
'    sSQL = sSQL & "                                          Left Join Proposta_Adesao_tb (Nolock)" & vbNewLine
'    sSQL = sSQL & "                                           On  Proposta_Adesao_tb.proposta_id = #avaliacao_restituicao_tb.Proposta_id" & vbNewLine
'    sSQL = sSQL & "                                          Left Join Proposta_Fechada_tb (Nolock)" & vbNewLine
'    sSQL = sSQL & "                                           On  Proposta_Fechada_tb.proposta_id = #avaliacao_restituicao_tb.Proposta_id" & vbNewLine
'    sSQL = sSQL & "                                          Left Join Proposta_TB (Nolock)" & vbNewLine
'    sSQL = sSQL & "                                           On  Proposta_tb.proposta_id = #avaliacao_restituicao_tb.Proposta_id" & vbNewLine
'    sSQL = sSQL & "         WHERE #avaliacao_restituicao_tb.atualizado = 'N'" & vbNewLine
'    If lProposta > 0 Then
'        sSQL = sSQL & "AND #avaliacao_restituicao_tb.proposta_id = " & CStr(lProposta) & vbNewLine
'    ElseIf iProduto > 0 Then
'        sSQL = sSQL & "AND #avaliacao_restituicao_tb.produto_id = " & CStr(iProduto) & vbNewLine
'    End If
'
'    Set rc = rdocn.OpenResultset(sSQL)
'    rc.Close
'    Set rc = Nothing
'
'    'tpEndosso_tb
'    sSQL = ""
'
'    sSQL = sSQL & " Update #avaliacao_restituicao_tb Set TipoAvaliacao  = Case When Isnull(Tp_endosso_tb.Descricao,'') = '' Then 'Restitui��o por iniciativa da seguradora' Else Tp_endosso_tb.Descricao End" & vbNewLine
'    sSQL = sSQL & "                                      From tp_endosso_tb(Nolock)" & vbNewLine
'    sSQL = sSQL & "                                      Right Join  #avaliacao_restituicao_tb on #avaliacao_restituicao_tb.tp_endosso_id  = tp_endosso_tb.tp_endosso_id" & vbNewLine
'    sSQL = sSQL & " WHERE   #avaliacao_restituicao_tb.atualizado = 'N'" & vbNewLine
'    If lProposta > 0 Then
'        sSQL = sSQL & "AND #avaliacao_restituicao_tb.proposta_id = " & CStr(lProposta) & vbNewLine
'    ElseIf iProduto > 0 Then
'        sSQL = sSQL & "AND #avaliacao_restituicao_tb.produto_id = " & CStr(iProduto) & vbNewLine
'    End If
'
'
'    Set rc = rdocn.OpenResultset(sSQL)
'    rc.Close
'    Set rc = Nothing
'
''--info baixa
'    sSQL = ""
'
'    sSQL = sSQL & " Set Nocount On " & vbNewLine
'
'    sSQL = sSQL & " Update #avaliacao_restituicao_tb Set Existe_info_baixa = Case When agendamento_cobranca_tb.Situacao = 'A' Then 'S' Else 'N' End" & vbNewLine
'    sSQL = sSQL & "                                     From agendamento_cobranca_tb(Nolock)" & vbNewLine
'    sSQL = sSQL & "                                     where #avaliacao_restituicao_tb.Proposta_id  = agendamento_cobranca_tb.Proposta_id" & vbNewLine
'    sSQL = sSQL & "                                     and agendamento_cobranca_tb.Situacao = 'A'" & vbNewLine
'    sSQL = sSQL & "                                     and #avaliacao_restituicao_tb.atualizado = 'N'" & vbNewLine
'    If lProposta > 0 Then
'        sSQL = sSQL & "AND #avaliacao_restituicao_tb.proposta_id = " & CStr(lProposta) & vbNewLine
'    ElseIf iProduto > 0 Then
'        sSQL = sSQL & "AND #avaliacao_restituicao_tb.produto_id = " & CStr(iProduto) & vbNewLine
'    End If
'
'    Set rc = rdocn.OpenResultset(sSQL)
'    rc.Close
'    Set rc = Nothing
'
''Premio pago/retido
'    sSQL = ""
'
'     sSQL = sSQL & " Set Nocount On " & vbNewLine
'
'    sSQL = sSQL & " Select isnull(Sum(A.Val_cobranca),0) as TotCobranca ,B.Proposta_id as Proposta_id" & vbNewLine
'    sSQL = sSQL & " Into #PremioPago" & vbNewLine
'    sSQL = sSQL & " From #avaliacao_restituicao_tb B (Nolock)" & vbNewLine
'    sSQL = sSQL & " Left Join agendamento_cobranca_tb A (Nolock) On A.Proposta_id = B.Proposta_id" & vbNewLine
'    sSQL = sSQL & "WHERE B.atualizado = 'N' " & vbNewLine
'     If lProposta > 0 Then
'        sSQL = sSQL & "AND B.proposta_id = " & CStr(lProposta) & vbNewLine
'    ElseIf iProduto > 0 Then
'        sSQL = sSQL & "AND B.produto_id = " & CStr(iProduto) & vbNewLine
'    End If
'
'    sSQL = sSQL & " Group by B.Proposta_id" & vbNewLine
'
'    sSQL = sSQL & " Update #avaliacao_restituicao_tb Set PremioPago   = A.TotCobranca * Case When #avaliacao_restituicao_tb.FatorCambio = 790 Then 1 else #avaliacao_restituicao_tb.val_cambio End" & vbNewLine
'    sSQL = sSQL & "                                      From #PremioPago A (Nolock)" & vbNewLine
'    sSQL = sSQL & "                                      where A.Proposta_id = #avaliacao_restituicao_tb.Proposta_id" & vbNewLine
'    sSQL = sSQL & "         and #avaliacao_restituicao_tb.atualizado = 'N'" & vbNewLine
'    If lProposta > 0 Then
'        sSQL = sSQL & "AND #avaliacao_restituicao_tb.proposta_id = " & CStr(lProposta) & vbNewLine
'    ElseIf iProduto > 0 Then
'        sSQL = sSQL & "AND #avaliacao_restituicao_tb.produto_id = " & CStr(iProduto) & vbNewLine
'    End If
'
'
'    If TpOperacao = 1 Then
'        'Avalia��o t�cnica
'        sSQL = sSQL & " Update #avaliacao_restituicao_tb Set PremioRetido = ISNULL(PremioPago, 0) - (ISNULL(Val_restituicao_estimado, 0) * Case When #avaliacao_restituicao_tb.FatorCambio = 790 Then 1 else #avaliacao_restituicao_tb.val_cambio End)" & vbNewLine
'    Else
'        'Avalia��o Gerencial
'        sSQL = sSQL & " Update #avaliacao_restituicao_tb Set PremioRetido = ISNULL(PremioPago, 0) - (ISNULL(Val_restituicao, 0) * Case When #avaliacao_restituicao_tb.FatorCambio = 790 Then 1 else #avaliacao_restituicao_tb.val_cambio End)" & vbNewLine
'    End If
'
'    sSQL = sSQL & "         WHERE #avaliacao_restituicao_tb.atualizado = 'N'" & vbNewLine
'    If lProposta > 0 Then
'        sSQL = sSQL & "AND #avaliacao_restituicao_tb.proposta_id = " & CStr(lProposta) & vbNewLine
'    ElseIf iProduto > 0 Then
'        sSQL = sSQL & "AND #avaliacao_restituicao_tb.produto_id = " & CStr(iProduto) & vbNewLine
'    End If
'
'    sSQL = sSQL & "drop table #PremioPago"
'
'    Set rc = rdocn.OpenResultset(sSQL)
'    rc.Close
'    Set rc = Nothing
'
''cliente
'     sSQL = ""
'
'    sSQL = sSQL & " Set Nocount On " & vbNewLine
'
'    sSQL = sSQL & "  Update #avaliacao_restituicao_tb Set Cliente = cliente_tb.nome" & vbNewLine
'    sSQL = sSQL & "                              From proposta_tb(Nolock)" & vbNewLine
'    sSQL = sSQL & "                              join Cliente_tb On cliente_tb.cliente_id = proposta_tb.prop_cliente_id" & vbNewLine
'    sSQL = sSQL & "                              where #avaliacao_restituicao_tb.Proposta_id  = proposta_tb.Proposta_id" & vbNewLine
'
'    sSQL = sSQL & "         and #avaliacao_restituicao_tb.atualizado = 'N'" & vbNewLine
'    If lProposta > 0 Then
'        sSQL = sSQL & "AND #avaliacao_restituicao_tb.proposta_id = " & CStr(lProposta) & vbNewLine
'    ElseIf iProduto > 0 Then
'        sSQL = sSQL & "AND #avaliacao_restituicao_tb.produto_id = " & CStr(iProduto) & vbNewLine
'    End If
'
'
'    Set rc = rdocn.OpenResultset(sSQL)
'    rc.Close
'    Set rc = Nothing
'
''update #avaliacao_restituicao_tb
'
'    sSQL = ""
'
'    sSQL = sSQL & "Update #avaliacao_restituicao_tb set Atualizado = 'S' where atualizado = 'N'"
'
'    If lProposta > 0 Then
'        sSQL = sSQL & "AND #avaliacao_restituicao_tb.proposta_id = " & CStr(lProposta) & vbNewLine
'    ElseIf iProduto > 0 Then
'        sSQL = sSQL & "AND #avaliacao_restituicao_tb.produto_id = " & CStr(iProduto) & vbNewLine
'    End If
'
'    Set rc = rdocn.OpenResultset(sSQL)
'    rc.Close
'    Set rc = Nothing
'
'
'    Exit Sub
'
'TrataErro:
'    Call Err.Raise(Err.Number, , "AtualizaDadosRestituicao - " & Err.Description)
'
'End Sub
Function AtualizaDadosRestituicao(lProposta As Long, iProduto As Integer, Optional vsql As String = "") As rdoResultset

    On Error GoTo TrataErro
    Dim sSQL As String
    sSQL = "exec seguros_db.dbo.SEGS11164_SPU " & lProposta & " , " & iProduto & " , " & TpOperacao & IIf(vsql <> "", ", '" & vsql & " ' ", Null) & vbNewLine
    
    Set AtualizaDadosRestituicao = rdocn.OpenResultset(sSQL)
    
    Exit Function
    
TrataErro:
    Call Err.Raise(Err.Number, , "AtualizaDadosRestituicao - " & Err.Description)
Resume
End Function
'Alan Neves - C_ANEVES - Demanda: 17860335 - 29/05/2013 - Fim

Sub aprovacao_tecnica(ByVal strPropostas As String, ByVal strNumAvaliacao, ByVal produtoId As Integer, ByVal strWhere As String)
'Alan Neves - C_ANEVES - Demanda: 17860335 - 03/06/2013 - Inicio
'Sub aprovacao_tecnica(ByVal strPropostas As String, ByVal strNumAvaliacao, ByVal produtoId As Integer, ByVal strWhere As String)
'
'    On Error GoTo TrataErro
'    Dim oFinanceiro As Object
'
'
'    FrmEspera.Label1 = "Aguarde, executando a aprova��o t�cnica..."
'    FrmEspera.Show
'
'    Set oFinanceiro = CreateObject("SEGL0026.cls00125")
'
'    If Len(strPropostas) = 0 And Len(strNumAvaliacao) = 0 And produtoId <> 0 And Len(strWhere) > 0 Then
'        Dim sSQL As String
'        Dim rc As rdoResultset
'
'        sSQL = "select * from #avaliacao_restituicao_tb where produto_id = " & CStr(produtoId) & strWhere
'        Set rc = rdocn.OpenResultset(sSQL)
'
'        Do While Not rc.EOF
'            strPropostas = strPropostas & rc("proposta_id") & ","
'            strNumAvaliacao = strNumAvaliacao & rc("num_avaliacao") & ","
'            If rc("modular") = "S" Then
'                strPropostas = strPropostas & rc("proposta_modular") & ","
'                strNumAvaliacao = strNumAvaliacao & rc("num_avaliacao_modular") & ","
'            End If
'            rc.MoveNext
'        Loop
'        rc.Close
'        Set rc = Nothing
'        strPropostas = Left(strPropostas, Len(strPropostas) - 1)
'        strNumAvaliacao = Left(strNumAvaliacao, Len(strNumAvaliacao) - 1)
'
'    End If
'
'    Call oFinanceiro.AprovacaoTecnica(gsSIGLASISTEMA, App.Title, App.FileDescription, glAmbiente_id, strPropostas, strNumAvaliacao, Data_Sistema, cUserName)
'
'    '--Matando tabelas tempor�rias
'   '--Matando tabelas tempor�rias
'    sSQL = "SET NOCOUNT ON" & vbNewLine
'    sSQL = sSQL & "CREATE TABLE #propostas" & vbNewLine
'    sSQL = sSQL & "(" & vbNewLine
'    sSQL = sSQL & "proposta_id numeric(9,0)" & vbNewLine
'    sSQL = sSQL & ")" & vbNewLine
'    Set rc = rdocn.OpenResultset(sSQL)
'    rc.Close
'    Set rc = Nothing
'
'    Dim Vetor As Variant
'    Vetor = Split(strPropostas, ",")
'
'    Dim i As Long
'    For i = LBound(Vetor) To UBound(Vetor)
'        sSQL = "insert into #propostas(proposta_id)" & vbNewLine
'        sSQL = sSQL & "Values(" & Vetor(i) & ");" & vbNewLine
'        Set rc = rdocn.OpenResultset(sSQL)
'        rc.Close
'        Set rc = Nothing
'    Next
'
'    sSQL = "SET NOCOUNT ON" & vbNewLine
'    'sSQL = sSQL & "delete from #avaliacao_restituicao_tb where proposta_id IN (" & strPropostas & ")"
'    'Renato Vasconcelos
'    'Flow 17801752
'    sSQL = sSQL & "delete A from #avaliacao_restituicao_tb A" & vbNewLine & _
'            "inner join avaliacao_restituicao_tb B (nolock)" & vbNewLine & _
'            " ON A.PROPOSTA_ID = B.PROPOSTA_ID" & vbNewLine & _
'            " AND A.NUM_AVALIACAO = B.NUM_AVALIACAO" & vbNewLine & _
'            " AND A.SITUACAO <> B.SITUACAO " & vbNewLine & _
'            "where a.proposta_id IN (select proposta_id from #propostas);"
'    sSQL = sSQL & "DROP TABLE #propostas"
'    Set rc = rdocn.OpenResultset(sSQL)
'    rc.Close
'    Set rc = Nothing
'
'    Call Unload(FrmEspera)
'
'    MsgBox "Opera��o conclu�da!", vbInformation
'
'
'    Exit Sub
'TrataErro:
'    Call Err.Raise(Err.Number, , "geraTempCliente - " & Err.Description)
'End Sub
    On Error GoTo TrataErro
    Dim oFinanceiro As Object
    
    
    FrmEspera.Label1 = "Aguarde, executando a aprova��o t�cnica..."
    FrmEspera.Show
    
    Set oFinanceiro = CreateObject("SEGL0026.cls00125")
    
    If Len(strPropostas) = 0 And Len(strNumAvaliacao) = 0 And produtoId <> 0 And Len(strWhere) > 0 Then
        Dim sSQL As String
        Dim rc As rdoResultset
        
        sSQL = "select * from #avaliacao_restituicao_tb where produto_id = " & CStr(produtoId) & strWhere
        Set rc = rdocn.OpenResultset(sSQL)
        
        Do While Not rc.EOF
            If rc("Cor") <> "A" Then
                strPropostas = strPropostas & rc("proposta_id") & ","
                strNumAvaliacao = strNumAvaliacao & rc("num_avaliacao") & ","
                If rc("modular") = "S" Then
                    strPropostas = strPropostas & rc("proposta_modular") & ","
                    strNumAvaliacao = strNumAvaliacao & rc("num_avaliacao_modular") & ","
                End If
            End If
            rc.MoveNext
        Loop
        rc.Close
        Set rc = Nothing
        strPropostas = Left(strPropostas, Len(strPropostas) - 1)
        strNumAvaliacao = Left(strNumAvaliacao, Len(strNumAvaliacao) - 1)
                    
    End If
    
    Call oFinanceiro.AprovacaoTecnica(gsSIGLASISTEMA, App.Title, App.FileDescription, glAmbiente_id, strPropostas, strNumAvaliacao, Data_Sistema, cUserName)
    
   '--Matando tabelas tempor�rias
    sSQL = "SET NOCOUNT ON" & vbNewLine
    sSQL = sSQL & "CREATE TABLE #propostas" & vbNewLine
    sSQL = sSQL & "(" & vbNewLine
    sSQL = sSQL & "proposta_id numeric(9,0)" & vbNewLine
    sSQL = sSQL & ")" & vbNewLine
    Set rc = rdocn.OpenResultset(sSQL)
    rc.Close
    Set rc = Nothing
    
    Dim Vetor As Variant
    Vetor = Split(strPropostas, ",")
    
    Dim i As Long
    For i = LBound(Vetor) To UBound(Vetor)
        sSQL = "insert into #propostas(proposta_id)" & vbNewLine
        sSQL = sSQL & "Values(" & Vetor(i) & ");" & vbNewLine
        Set rc = rdocn.OpenResultset(sSQL)
        rc.Close
        Set rc = Nothing
    Next
    
    sSQL = "SET NOCOUNT ON" & vbNewLine
    'sSQL = sSQL & "delete from #avaliacao_restituicao_tb where proposta_id IN (" & strPropostas & ")"
    'Renato Vasconcelos
    'Flow 17801752
    sSQL = sSQL & "delete A from #avaliacao_restituicao_tb A" & vbNewLine & _
            "inner join avaliacao_restituicao_tb B with(nolock)" & vbNewLine & _
            " ON A.PROPOSTA_ID = B.PROPOSTA_ID" & vbNewLine & _
            " AND A.NUM_AVALIACAO = B.NUM_AVALIACAO" & vbNewLine & _
            " AND A.SITUACAO <> B.SITUACAO " & vbNewLine & _
            "where a.proposta_id IN (select proposta_id from #propostas);"
    sSQL = sSQL & "DROP TABLE #propostas"
    Set rc = rdocn.OpenResultset(sSQL)
    rc.Close
    Set rc = Nothing
    
    Call Unload(FrmEspera)
    
    MsgBox "Opera��o conclu�da!", vbInformation

    
    Exit Sub
TrataErro:
    Call Err.Raise(Err.Number, , "geraTempCliente - " & Err.Description)
End Sub
'Alan Neves - C_ANEVES - Demanda: 17860335 - 03/06/2013 - Fim


Function aprovacao_gerencial(ByVal strPropostas As String, ByVal strNumAvaliacao, ByVal produtoId As Integer, ByVal strWhere As String) As Boolean
    'On Error GoTo TrataErro
    Dim oFinanceiro As Object
    Dim lConexao As Long
    Dim i As Long
    Dim rs As Object
'Alan Neves - C_ANEVES - Demanda: 17860335 - 03/06/2013 - Inicio
'Function aprovacao_gerencial(ByVal strPropostas As String, ByVal strNumAvaliacao, ByVal produtoId As Integer, ByVal strWhere As String) As Boolean
'    'On Error GoTo TrataErro
'    Dim oFinanceiro As Object
'    Dim lConexao As Long
'    Dim i As Long
'    Dim rs As Object
'
'    FrmEspera.Label1 = "Aguarde, executando a aprova��o gerencial..."
'    FrmEspera.Show
'
'
'    Set oFinanceiro = CreateObject("SEGL0026.cls00125")
'
'    If Len(strPropostas) = 0 And Len(strNumAvaliacao) = 0 And produtoId <> 0 And Len(strWhere) > 0 Then
'        Dim sSQL As String
'        Dim rc As rdoResultset
'
'        sSQL = "select * from #avaliacao_restituicao_tb where produto_id = " & CStr(produtoId) & strWhere
'        Set rc = rdocn.OpenResultset(sSQL)
'
'        Do While Not rc.EOF
'            strPropostas = strPropostas & rc("proposta_id") & ","
'            strNumAvaliacao = strNumAvaliacao & rc("num_avaliacao") & ","
'            If rc("modular") = "S" Then
'                strPropostas = strPropostas & rc("proposta_modular") & ","
'                strNumAvaliacao = strNumAvaliacao & rc("num_avaliacao_modular") & ","
'            End If
'            rc.MoveNext
'        Loop
'        rc.Close
'        Set rc = Nothing
'        strPropostas = Left(strPropostas, Len(strPropostas) - 1)
'        strNumAvaliacao = Left(strNumAvaliacao, Len(strNumAvaliacao) - 1)
'    End If
'
'
'    Set rs = oFinanceiro.AprovacaoGerencial(gsSIGLASISTEMA, App.Title, App.FileDescription, glAmbiente_id, strPropostas, strNumAvaliacao, produtoId, Data_Sistema, cUserName)
'
'     Call Unload(FrmEspera)
'    If rs.EOF Then
'
'        MsgBox "Ocorreu algum problema ao aprovar as restitui��es!", vbInformation
'        aprovacao_gerencial = False
'        rs.Close
'        Set rs = Nothing
'        Exit Function
'    End If
'
'     'Renato Vasconcelos
'    'procura de propostas com erro
'    '06/10/2011
'    Dim strPropostasErro As String
'
'    strPropostasErro = ""
'
'    Dim blnAprovacao As Boolean
'    blnAprovacao = False
'    Do While Not rs.EOF
'        If rs(0) = "0" Then
'            blnAprovacao = True
'            Exit Do
'        Else
'            strPropostasErro = strPropostasErro & rs(1) & ", "
'        End If
'        rs.MoveNext
'    Loop
'
'
'    If blnAprovacao Then
'    '--Matando tabelas tempor�rias
'      '--Matando tabelas tempor�rias
'   '--Matando tabelas tempor�rias
'        sSQL = "SET NOCOUNT ON" & vbNewLine
'        sSQL = sSQL & "CREATE TABLE #propostas" & vbNewLine
'        sSQL = sSQL & "(" & vbNewLine
'        sSQL = sSQL & "proposta_id numeric(9,0)" & vbNewLine
'        sSQL = sSQL & ")" & vbNewLine
'        Set rc = rdocn.OpenResultset(sSQL)
'        rc.Close
'        Set rc = Nothing
'
'        Dim Vetor As Variant
'        Vetor = Split(strPropostas, ",")
'
'
'        For i = LBound(Vetor) To UBound(Vetor)
'            sSQL = "insert into #propostas(proposta_id)" & vbNewLine
'            sSQL = sSQL & "Values(" & Vetor(i) & ");" & vbNewLine
'            Set rc = rdocn.OpenResultset(sSQL)
'            rc.Close
'            Set rc = Nothing
'        Next
'
'        sSQL = "SET NOCOUNT ON" & vbNewLine
'        'sSQL = sSQL & "delete from #avaliacao_restituicao_tb where proposta_id IN (" & strPropostas & ")"
'        'sSQL = sSQL & "delete from #avaliacao_restituicao_tb where proposta_id IN (select proposta_id from #propostas);"
'        'Renato Vasconcelos
'        'Flow 17801752
'        sSQL = sSQL & "delete A from #avaliacao_restituicao_tb A" & vbNewLine & _
'                "inner join avaliacao_restituicao_tb B (nolock)" & vbNewLine & _
'                " ON A.PROPOSTA_ID = B.PROPOSTA_ID" & vbNewLine & _
'                " AND A.NUM_AVALIACAO = B.NUM_AVALIACAO" & vbNewLine & _
'                " AND A.SITUACAO <> B.SITUACAO " & vbNewLine & _
'                "where A.proposta_id IN (select proposta_id from #propostas);"
'            sSQL = sSQL & "DROP TABLE #propostas"
'            Set rc = rdocn.OpenResultset(sSQL)
'        rc.Close
'        Set rc = Nothing
'
'
'        aprovacao_gerencial = True
'    Else
'        strPropostasErro = Left(strPropostasErro, Len(strPropostasErro) - 2)
'        'Dim Vetor As Variant
'
'        Vetor = Split(strPropostasErro, ",")
'
'         sSQL = "SET NOCOUNT ON" & vbNewLine
'        sSQL = sSQL & "CREATE TABLE #propostas" & vbNewLine
'        sSQL = sSQL & "(" & vbNewLine
'        sSQL = sSQL & "proposta_id numeric(9,0)" & vbNewLine
'        sSQL = sSQL & ")" & vbNewLine
'        Set rc = rdocn.OpenResultset(sSQL)
'        rc.Close
'        Set rc = Nothing
'
'        'Dim Vetor As Variant
'        Vetor = Split(strPropostas, ",")
'
'
'        For i = LBound(Vetor) To UBound(Vetor)
'            sSQL = "insert into #propostas(proposta_id)" & vbNewLine
'            sSQL = sSQL & "Values(" & Vetor(i) & ");" & vbNewLine
'            Set rc = rdocn.OpenResultset(sSQL)
'            rc.Close
'            Set rc = Nothing
'        Next
'
'        sSQL = "SET NOCOUNT ON" & vbNewLine
'        'sSQL = sSQL & "delete from #avaliacao_restituicao_tb where proposta_id IN (" & strPropostas & ")"
'         'Renato Vasconcelos
'        'Flow 17801752
'
'       ' sSQL = sSQL & "delete from #avaliacao_restituicao_tb where proposta_id IN (select proposta_id from #propostas)" & _
'              " and not proposta_id IN(" & strPropostasErro & ");"
'
'         sSQL = sSQL & "delete A from #avaliacao_restituicao_tb A" & vbNewLine & _
'                " inner join avaliacao_restituicao_tb B (nolock)" & vbNewLine & _
'                " ON A.PROPOSTA_ID = B.PROPOSTA_ID" & vbNewLine & _
'                " AND A.NUM_AVALIACAO = B.NUM_AVALIACAO" & vbNewLine & _
'                " AND A.SITUACAO <> B.SITUACAO " & vbNewLine & _
'            " where A.proposta_id IN (select proposta_id from #propostas)" & _
'            " and not A.proposta_id IN(" & strPropostasErro & ");"
'            '29/05/2013 Corre��o efetuada de (and not proposta_id IN(& strPropostasErro &)) para (and not A.proposta_id IN(& strPropostasErro &)) - Tales de S�'
'
'
'
'
'
'        sSQL = sSQL & "DROP TABLE #propostas"
'        Set rc = rdocn.OpenResultset(sSQL)
'        rc.Close
'        Set rc = Nothing
''
''        sSQL = "SET NOCOUNT ON" & vbNewLine
''        sSQL = sSQL & "delete from #avaliacao_restituicao_tb where proposta_id IN (" & strPropostas & ") " & _
''            " and not proposta_id IN(" & strPropostasErro & ")"
''
''        Set rc = rdocn.OpenResultset(sSQL)
''        rc.Close
'        Dim msg As String
'       ' Dim i As Integer
'        strPropostasErro = ""
'
'        For i = LBound(Vetor) To UBound(Vetor)
'            strPropostasErro = strPropostasErro & Vetor(i) & ", "
'            If i + 1 Mod 15 = 0 Then
'                strPropostasErro = strPropostasErro & vbNewLine
'            End If
'        Next
'        strPropostasErro = Left(strPropostasErro, Len(strPropostasErro) - 2)
'        msg = "A(s) Proposta(s)" & vbNewLine & _
'            strPropostasErro & vbNewLine & _
'            "N�o foi(foram) aprovada(s), devido a algum erro durante a aprova��o!" & vbNewLine & _
'            "A(s) outra(s) foi(foram) aprovada(s) "
'        If Len(msg) < 1024 Then
'            MsgBox msg, vbInformation
'
'        Else
'            frmPropostasErro.lblMensagem = msg
'            frmPropostasErro.Show vbModal
'        End If
'        aprovacao_gerencial = True
''
'    End If
'    rs.Close
'    Set rs = Nothing
'
'
'    Exit Function
'
'TrataErro:
'    Call Err.Raise(Err.Number, , "geraTempCliente - " & Err.Description)
'
'
'
'End Function
    
    
    FrmEspera.Label1 = "Aguarde, executando a aprova��o gerencial..."
    FrmEspera.Show
    
    
    Set oFinanceiro = CreateObject("SEGL0026.cls00125")
    
    If Len(strPropostas) = 0 And Len(strNumAvaliacao) = 0 And produtoId <> 0 And Len(strWhere) > 0 Then
        Dim sSQL As String
        Dim rc As rdoResultset
        
        sSQL = "select * from #avaliacao_restituicao_tb where produto_id = " & CStr(produtoId) & strWhere
        Set rc = rdocn.OpenResultset(sSQL)
        
        Do While Not rc.EOF
            strPropostas = strPropostas & rc("proposta_id") & ","
            strNumAvaliacao = strNumAvaliacao & rc("num_avaliacao") & ","
            If rc("modular") = "S" Then
                strPropostas = strPropostas & rc("proposta_modular") & ","
                strNumAvaliacao = strNumAvaliacao & rc("num_avaliacao_modular") & ","
            End If
            rc.MoveNext
        Loop
        rc.Close
        Set rc = Nothing
        strPropostas = Left(strPropostas, Len(strPropostas) - 1)
        strNumAvaliacao = Left(strNumAvaliacao, Len(strNumAvaliacao) - 1)
    End If
    

    Set rs = oFinanceiro.AprovacaoGerencial(gsSIGLASISTEMA, App.Title, App.FileDescription, glAmbiente_id, strPropostas, strNumAvaliacao, produtoId, Data_Sistema, cUserName)
    
     Call Unload(FrmEspera)
    If rs.EOF Then
        
        MsgBox "Ocorreu algum problema ao aprovar as restitui��es!", vbInformation
        aprovacao_gerencial = False
        rs.Close
        Set rs = Nothing
        Exit Function
    End If
    
     'Renato Vasconcelos
    'procura de propostas com erro
    '06/10/2011
    Dim strPropostasErro As String
    
    strPropostasErro = ""
    
    Dim blnAprovacao As Boolean
    blnAprovacao = False
    Do While Not rs.EOF
        If rs(0) = "0" Then
            blnAprovacao = True
            Exit Do
        Else
            strPropostasErro = strPropostasErro & rs(1) & ", "
        End If
        rs.MoveNext
    Loop
    
    
    If blnAprovacao Then
    '--Matando tabelas tempor�rias
        sSQL = "SET NOCOUNT ON" & vbNewLine
        sSQL = sSQL & "CREATE TABLE #propostas" & vbNewLine
        sSQL = sSQL & "(" & vbNewLine
        sSQL = sSQL & "proposta_id numeric(9,0)" & vbNewLine
        sSQL = sSQL & ")" & vbNewLine
        Set rc = rdocn.OpenResultset(sSQL)
        rc.Close
        Set rc = Nothing
        
        Dim Vetor As Variant
        Vetor = Split(strPropostas, ",")
        
           
        For i = LBound(Vetor) To UBound(Vetor)
            sSQL = "insert into #propostas(proposta_id)" & vbNewLine
            sSQL = sSQL & "Values(" & Vetor(i) & ");" & vbNewLine
            Set rc = rdocn.OpenResultset(sSQL)
            rc.Close
            Set rc = Nothing
        Next
        
        sSQL = "SET NOCOUNT ON" & vbNewLine
        'sSQL = sSQL & "delete from #avaliacao_restituicao_tb where proposta_id IN (" & strPropostas & ")"
        'sSQL = sSQL & "delete from #avaliacao_restituicao_tb where proposta_id IN (select proposta_id from #propostas);"
        'Renato Vasconcelos
        'Flow 17801752
        sSQL = sSQL & "delete A from #avaliacao_restituicao_tb A" & vbNewLine & _
                "inner join avaliacao_restituicao_tb B with(nolock)" & vbNewLine & _
                " ON A.PROPOSTA_ID = B.PROPOSTA_ID" & vbNewLine & _
                " AND A.NUM_AVALIACAO = B.NUM_AVALIACAO" & vbNewLine & _
                " AND A.SITUACAO <> B.SITUACAO " & vbNewLine & _
                "where A.proposta_id IN (select proposta_id from #propostas);"
            sSQL = sSQL & "DROP TABLE #propostas"
            Set rc = rdocn.OpenResultset(sSQL)
        rc.Close
        Set rc = Nothing
        
        
        aprovacao_gerencial = True
    Else
        strPropostasErro = Left(strPropostasErro, Len(strPropostasErro) - 2)
        'Dim Vetor As Variant
        
        Vetor = Split(strPropostasErro, ",")
        
         sSQL = "SET NOCOUNT ON" & vbNewLine
        sSQL = sSQL & "CREATE TABLE #propostas" & vbNewLine
        sSQL = sSQL & "(" & vbNewLine
        sSQL = sSQL & "proposta_id numeric(9,0)" & vbNewLine
        sSQL = sSQL & ")" & vbNewLine
        Set rc = rdocn.OpenResultset(sSQL)
        rc.Close
        Set rc = Nothing
        
        'Dim Vetor As Variant
        Vetor = Split(strPropostas, ",")
        
           
        For i = LBound(Vetor) To UBound(Vetor)
            sSQL = "insert into #propostas(proposta_id)" & vbNewLine
            sSQL = sSQL & "Values(" & Vetor(i) & ");" & vbNewLine
            Set rc = rdocn.OpenResultset(sSQL)
            rc.Close
            Set rc = Nothing
        Next
        
        sSQL = "SET NOCOUNT ON" & vbNewLine
        'sSQL = sSQL & "delete from #avaliacao_restituicao_tb where proposta_id IN (" & strPropostas & ")"
         'Renato Vasconcelos
        'Flow 17801752
        
       ' sSQL = sSQL & "delete from #avaliacao_restituicao_tb where proposta_id IN (select proposta_id from #propostas)" & _
              " and not proposta_id IN(" & strPropostasErro & ");"
        
         sSQL = sSQL & "delete A from #avaliacao_restituicao_tb A" & vbNewLine & _
                " inner join avaliacao_restituicao_tb B with(nolock)" & vbNewLine & _
                " ON A.PROPOSTA_ID = B.PROPOSTA_ID" & vbNewLine & _
                " AND A.NUM_AVALIACAO = B.NUM_AVALIACAO" & vbNewLine & _
                " AND A.SITUACAO <> B.SITUACAO " & vbNewLine & _
            " where A.proposta_id IN (select proposta_id from #propostas)" & _
            " and not A.proposta_id IN(" & strPropostasErro & ");"
            '29/05/2013 Corre��o efetuada de (and not proposta_id IN(& strPropostasErro &)) para (and not A.proposta_id IN(& strPropostasErro &)) - Tales de S�'
            
            
            
        
        
        sSQL = sSQL & "DROP TABLE #propostas"
        Set rc = rdocn.OpenResultset(sSQL)
        rc.Close
        Set rc = Nothing
'
'        sSQL = "SET NOCOUNT ON" & vbNewLine
'        sSQL = sSQL & "delete from #avaliacao_restituicao_tb where proposta_id IN (" & strPropostas & ") " & _
'            " and not proposta_id IN(" & strPropostasErro & ")"
'
'        Set rc = rdocn.OpenResultset(sSQL)
'        rc.Close
        Dim msg As String
       ' Dim i As Integer
        strPropostasErro = ""
        
        For i = LBound(Vetor) To UBound(Vetor)
            strPropostasErro = strPropostasErro & Vetor(i) & ", "
            If i + 1 Mod 15 = 0 Then
                strPropostasErro = strPropostasErro & vbNewLine
            End If
        Next
        strPropostasErro = Left(strPropostasErro, Len(strPropostasErro) - 2)
        msg = "A(s) Proposta(s)" & vbNewLine & _
            strPropostasErro & vbNewLine & _
            "N�o foi(foram) aprovada(s), devido a algum erro durante a aprova��o!" & vbNewLine & _
            "A(s) outra(s) foi(foram) aprovada(s) "
        If Len(msg) < 1024 Then
            MsgBox msg, vbInformation

        Else
            frmPropostasErro.lblMensagem = msg
            frmPropostasErro.Show vbModal
        End If
        aprovacao_gerencial = True

    End If
    
    rs.Close
    Set rs = Nothing


    Exit Function

TrataErro:
    Call Err.Raise(Err.Number, , "geraTempCliente - " & Err.Description)
    

End Function

'Alan Neves - C_ANEVES - Demanda: 17860335 - 03/06/2013 - Fim



