VERSION 5.00
Begin VB.Form FrmMotivoRecusa 
   Caption         =   "Motivo Recusa"
   ClientHeight    =   1455
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   8745
   ControlBox      =   0   'False
   Icon            =   "MotivoRecusa.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   1455
   ScaleWidth      =   8745
   StartUpPosition =   2  'CenterScreen
   Begin VB.ComboBox CmbMotivoRecusa 
      Height          =   315
      ItemData        =   "MotivoRecusa.frx":0442
      Left            =   150
      List            =   "MotivoRecusa.frx":0444
      Style           =   2  'Dropdown List
      TabIndex        =   2
      Top             =   90
      Width           =   8445
   End
   Begin VB.CommandButton BtnCancelar 
      Caption         =   "Cancelar"
      Height          =   405
      Left            =   7140
      TabIndex        =   1
      Top             =   960
      Width           =   1455
   End
   Begin VB.CommandButton BtnOk 
      Caption         =   "Ok"
      Height          =   405
      Left            =   5610
      TabIndex        =   0
      Top             =   960
      Width           =   1455
   End
End
Attribute VB_Name = "FrmMotivoRecusa"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Sub Carregar_CmbMotivoRecusa(PProduto)

Dim SQL As String
Dim Rst As rdoResultset

SQL = "Select "
SQL = SQL & "   tp_avaliacao_tb.tp_avaliacao_id, "
SQL = SQL & "   tp_avaliacao_tb.nome "
SQL = SQL & " From "
SQL = SQL & "   tp_avaliacao_tb "
SQL = SQL & "   inner join produto_tp_avaliacao_tb "
SQL = SQL & "         on produto_tp_avaliacao_tb.tp_avaliacao_id = tp_avaliacao_tb.tp_avaliacao_id "
SQL = SQL & " Where "
SQL = SQL & "     tp_avaliacao_tb.tratamento = 'm' "
SQL = SQL & "     and produto_tp_avaliacao_tb.produto_id = " & PProduto

Set Rst = rdocn.OpenResultset(SQL)

CmbMotivoRecusa.Clear

Do While Not Rst.EOF

    CmbMotivoRecusa.AddItem Rst!Nome
    CmbMotivoRecusa.ItemData(CmbMotivoRecusa.NewIndex) = Rst!tp_avaliacao_id
    
    Rst.MoveNext
Loop

End Sub

Private Sub BtnCancelar_Click()

CmbMotivoRecusa.ListIndex = -1
Me.Hide

End Sub

Private Sub BtnOk_Click()

Me.Hide

End Sub


Private Sub CmbMotivoRecusa_Click()

If CmbMotivoRecusa.ListIndex = -1 Then
   btnOK.Enabled = False
Else
   btnOK.Enabled = True
End If
End Sub

Sub Carregar_CmbMotivo_Vida()
    Dim SQL As String
    Dim Rst As rdoResultset
    
    SQL = "Select "
    SQL = SQL & "   tp_avaliacao_tb.tp_avaliacao_id, "
    SQL = SQL & "   tp_avaliacao_tb.nome "
    SQL = SQL & " From "
    SQL = SQL & "   tp_avaliacao_tb "
    SQL = SQL & " Where "
    SQL = SQL & "     tp_avaliacao_tb.tp_avaliacao_id in (375, 376, 1817) "

    Set Rst = rdocn.OpenResultset(SQL)
    
    CmbMotivoRecusa.Clear
    
    Do While Not Rst.EOF
        CmbMotivoRecusa.AddItem Rst!Nome
        CmbMotivoRecusa.ItemData(CmbMotivoRecusa.NewIndex) = Rst!tp_avaliacao_id
        
        Rst.MoveNext
    Loop
End Sub

Private Sub Form_Load()

btnOK.Enabled = False

    If (produto_id = 11) Or (produto_id = 12) Or (produto_id = 121) Or (produto_id = 135) Or (produto_id = 136) _
            Or (produto_id = 716) Or (produto_id = 721) Or (produto_id = 1174) Or (produto_id = 1175) _
            Or (produto_id = 1177) Or (produto_id = 1179) Or (produto_id = 1180) Or (produto_id = 1181) _
            Or (produto_id = 1182) Or (produto_id = 1196) Or (produto_id = 1198) Or (produto_id = 1205) _
            Or (produto_id = 1208) Or (produto_id = 1211) Or (produto_id = 1217) Or (produto_id = 1218) _
            Or (produto_id = 1235) Or (produto_id = 1236) Or (produto_id = 1237) Or (produto_id = 1225) _
            Or (produto_id = 1231) Then
            
        Carregar_CmbMotivo_Vida
    Else
Carregar_CmbMotivoRecusa produto_id
    End If

End Sub


