VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "mscomctl.OCX"
Begin VB.Form FrmSelEndosso 
   ClientHeight    =   7665
   ClientLeft      =   960
   ClientTop       =   1170
   ClientWidth     =   11010
   BeginProperty Font 
      Name            =   "MS Sans Serif"
      Size            =   8.25
      Charset         =   0
      Weight          =   700
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "SelEndosso.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   7665
   ScaleWidth      =   11010
   Begin VB.PictureBox Picture1 
      Height          =   1335
      Left            =   60
      ScaleHeight     =   1275
      ScaleWidth      =   10845
      TabIndex        =   9
      Top             =   6030
      Width           =   10905
      Begin VB.CommandButton cmdExportExcel 
         Caption         =   "Exportar para o Excel"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   8760
         TabIndex        =   16
         Top             =   360
         Width           =   2055
      End
      Begin VB.CommandButton btnAceiteTecnico 
         Caption         =   "Aprovar"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   5760
         TabIndex        =   15
         Top             =   840
         Width           =   1665
      End
      Begin MSComctlLib.ImageList ImageList1 
         Left            =   3990
         Top             =   240
         _ExtentX        =   1005
         _ExtentY        =   1005
         BackColor       =   -2147483643
         ImageWidth      =   16
         ImageHeight     =   16
         MaskColor       =   12632256
         _Version        =   393216
         BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
            NumListImages   =   2
            BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "SelEndosso.frx":0442
               Key             =   ""
            EndProperty
            BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "SelEndosso.frx":075C
               Key             =   ""
            EndProperty
         EndProperty
      End
      Begin VB.CommandButton BtnModo 
         Caption         =   "Modo"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   795
         Left            =   3030
         Picture         =   "SelEndosso.frx":0A76
         Style           =   1  'Graphical
         TabIndex        =   2
         ToolTipText     =   "Alterar Modo de Trabalho"
         Top             =   420
         Width           =   885
      End
      Begin VB.CommandButton BtnSelecionarTodos 
         Caption         =   "Selecionar_Todos"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   3990
         TabIndex        =   3
         Top             =   840
         Width           =   1665
      End
      Begin VB.CommandButton BtnSair 
         Caption         =   "Sair"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   9120
         TabIndex        =   6
         Top             =   840
         Width           =   1665
      End
      Begin VB.CommandButton BtnAtualizarGrid 
         Caption         =   "Atualizar Lista"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   7410
         TabIndex        =   5
         Top             =   840
         Width           =   1665
      End
      Begin VB.Frame FrameLegenda 
         Caption         =   "Legenda"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1155
         Left            =   60
         TabIndex        =   10
         Top             =   60
         Width           =   2895
         Begin VB.Label Label3 
            Caption         =   "Sem Informa��o de Baixa"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Left            =   570
            TabIndex        =   12
            Top             =   810
            Width           =   1965
         End
         Begin VB.Image Image2 
            Height          =   375
            Left            =   150
            Picture         =   "SelEndosso.frx":0EB8
            Stretch         =   -1  'True
            Top             =   690
            Width           =   405
         End
         Begin VB.Label Label1 
            Caption         =   "Com Informa��o de Baixa"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Left            =   570
            TabIndex        =   11
            Top             =   360
            Width           =   1965
         End
         Begin VB.Image Image1 
            Height          =   495
            Left            =   120
            Picture         =   "SelEndosso.frx":11C2
            Stretch         =   -1  'True
            Top             =   210
            Width           =   465
         End
      End
      Begin VB.CommandButton BtnAprovar 
         Caption         =   "Aprovar"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   5730
         TabIndex        =   4
         Top             =   840
         Width           =   1665
      End
   End
   Begin VB.Frame Frame2 
      Caption         =   "Produto:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1005
      Left            =   60
      TabIndex        =   8
      Top             =   60
      Width           =   10875
      Begin VB.TextBox txtQtdeLista 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   9240
         TabIndex        =   13
         Top             =   600
         Width           =   1335
      End
      Begin VB.ComboBox CmbProduto 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   210
         Style           =   2  'Dropdown List
         TabIndex        =   0
         Top             =   240
         Width           =   10455
      End
      Begin VB.Label lblQtdeLista 
         Caption         =   "Qtde registros na lista(Opcional):"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   6720
         TabIndex        =   14
         Top             =   660
         Width           =   2415
      End
   End
   Begin MSComctlLib.StatusBar StatusBar1 
      Align           =   2  'Align Bottom
      Height          =   315
      Left            =   0
      TabIndex        =   7
      Top             =   7350
      Width           =   11010
      _ExtentX        =   19420
      _ExtentY        =   556
      Style           =   1
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSComctlLib.ListView ListSelEndosso 
      Height          =   4815
      Left            =   60
      TabIndex        =   1
      Top             =   1140
      Width           =   10905
      _ExtentX        =   19235
      _ExtentY        =   8493
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      Checkboxes      =   -1  'True
      _Version        =   393217
      SmallIcons      =   "ImageList1"
      ForeColor       =   -2147483646
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      NumItems        =   0
   End
End
Attribute VB_Name = "FrmSelEndosso"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
' bcarneiro - 01/12/2003 - Projeto Restitui��o por Iniciativa da Seguradora
Const SITUACAO_PENDENTE_AVALIACAO_TECNICA = "P"
Const SITUACAO_PENDENTE_AVALIACAO_GERENCIAL = "T"
Public sDtInicioEsc As String
Public sDtFimEsc As String
Public lAvaliacaoModular As Long

Private Declare Function SendMessage Lib "user32" Alias "SendMessageA" (ByVal hwnd As Long, ByVal wMsg As Long, ByVal wParam As Long, lparam As Any) As Long
Private Declare Function LockWindowUpdate Lib "user32" (ByVal hwndLock As Long) As Long
Dim OrdAsc As Boolean 'serve para poder classificar como ascendente/descendente
'Public bModular As Boolean



''Sub Aprovar_Cancelamentos()
''
''Dim Item As ListItem
''
''Dim Proposta_Basica_Adesao As String
''
''Dim Nr_Endosso_BB As String
''Dim Proposta_Id As String
''Dim Endosso_Id As String
''Dim Dt_Contratacao As String
''Dim Dt_Cancelamento_BB As String
''Dim Dt_Inicio_vig As String
''Dim Dt_Fim_vig As String
''
''Dim Val_Custo_Apolice_Certif_Rest As Currency
''Dim Val_Adic_Fracionamento_Rest As Currency
''Dim Val_Desconto_Rest As Currency
''Dim Val_IOF_Rest As Currency
''Dim Val_Premio_Tarifa_Rest As Currency
''Dim Val_Restituicao As Currency
''Dim Val_Tot_Corretagem_Rest As Currency
''Dim Val_ProLabore_Rest As Currency
''
''Dim Corretagem As Corretagem
''Dim Estipulante_Id As String
''Dim Perc_Pro_Labore As Currency
''
''Dim ramo_id As String
''Dim Perc_IR As Currency
''Dim Val_Tot_IR_Corretagem As Currency
''Dim Val_IR_Estipulante As Currency
''
''Dim Val_Is_Restituicao As Currency
''Dim Val_Repasse_BB As Currency
''Dim Movimentacao_Id As String
''Dim Processado As Boolean
''Dim Proposta_BB As String
''Dim iTipoEndosso As Integer
''
'''pcarvalho - 17/06/2003 Padroniza��o do c�lculo de restitui��o
''Dim sConvenio As String
''Dim bFlagRetemIOF As Boolean
''Dim bFlagRetemComissao As Boolean
''Dim iMoedaPremioId As Integer
''
'''---------------------
'''Luciana - 12/08/2003
'''---------------------
''Dim rc As rdoResultset
'''---------------------
''
'''Atualizando interface'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''
''MousePointer = 11
''
'''Iniciando transa��o'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''
''rdocn.BeginTrans
''
''For i = 1 To ListSelEndosso.ListItems.Count
''
''    Set Item = ListSelEndosso.ListItems(i)
''
''    Proposta_Id = Item.SubItems(4)
''    Processado = Verificar_Existencia_Avaliacao_Pendente(Proposta_Id, TpOperacao)
''
''    If Item.Checked = True And Processado = False Then
''
''        'Obtendo dados da proposta'''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''
''        Proposta_BB = Item.SubItems(3)
''        Nr_Endosso_BB = Item.SubItems(1)
''        Produto_Id = Item.SubItems(5)
''        Dt_Contratacao = Item.SubItems(7)
''        Dt_Inicio_vig = Item.SubItems(8)
''        Dt_Fim_vig = Item.SubItems(9)
''
''        Dt_Cancelamento_BB = Item.SubItems(10)
''        Emi_Id = Item.SubItems(11)
''        Origem = Item.SubItems(13)
''
''        If Origem = "1" Then
''           Proposta_Basica_Adesao = BuscaParametro("BASICA " & Produto_Id)
''        End If
''
''        Call Obter_Dados_Adicionais_Proposta(Proposta_Id, ramo_id, 0, , iMoedaPremioId)
''        Call Obter_Premio_IS(Proposta_Id, Produto_Id, Val_Is_Restituicao, 0)
''
''        Cliente_ID_Beneficiario = Obter_Beneficiario(Proposta_Id)
''
''        'Obtendo dados da avalia��o t�cnica aprovada''''''''''''''''''''''''''''''''''''''
''
''        Call Obter_Valores_Avaliacao_Tecnica(Proposta_Id, _
''                               Val_Custo_Apolice_Certif_Rest, _
''                               Val_Adic_Fracionamento_Rest, _
''                               Val_Desconto_Rest, _
''                               Val_IOF_Rest, _
''                               Val_Premio_Tarifa_Rest, _
''                               Val_Restituicao, _
''                               0, _
''                               Val_Tot_Corretagem_Rest, _
''                               Val_ProLabore_Rest, _
''                               0, _
''                               0, _
''                               0, _
''                               0, _
''                               0, _
''                               0, _
''                               0)
''
''        'Obtendo valor de IR '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''
''        'Call Obtem_Perc_IR(ramo_id, Dt_Inicio_vig, Perc_IR)
''        Call Obtem_Perc_IR(ramo_id, Data_Sistema, Perc_IR)
''
''        Val_Tot_IR_Corretagem = Trunca(Val_Tot_Corretagem_Rest * Perc_IR)
''        Val_IR_Estipulante = Trunca(Val_ProLabore_Rest * Perc_IR)  '*** Adriana - Atualiza Base para correta devolu��o de pr�mios
''
''        'Obtendo valor de repasse para o BB'''''''''''''''''''''''''''''''''''''''''''''''
''
''        'pcarvalho - 17/06/2003 Padroniza��o do c�lculo de restitui��o
''        sConvenio = Obtem_Numero_do_Convenio(Val(Produto_Id), Val(ramo_id), 1, iMoedaPremioId)
''        Call VerificarRetencao(sConvenio, bFlagRetemComissao, bFlagRetemIOF)
''
''        'Val_Repasse_BB = Val_Restituicao - Val_Tot_Corretagem_Rest - Val_ProLabore_Rest + Val_Tot_IR_Corretagem + Val_IR_Estipulante - Val_IOF_Rest
''        Val_Repasse_BB = CalcularRepasse(Val_Tot_Corretagem_Rest, Val_ProLabore_Rest, _
''                                         Val_Restituicao, Perc_IR, Val_IOF_Rest, _
''                                         bFlagRetemComissao, bFlagRetemIOF)
''
''        'Rateando valor de corretagem'''''''''''''''''''''''''''''''''''''''''''''''''''''
''
''        'Call Carregar_Rateio_Corretagem(Proposta_Id, Dt_Inicio_vig, 0, 0, 0, 0)
''        'bcarneiro - 13/03/03
''        'Call Carregar_Rateio_Corretagem(Proposta_Id, Data_Sistema, 0, 0, 0, 0)
''        'Call Carregar_Rateio_Corretagem(Proposta_Id, Dt_Contratacao, 0, 0, 0, 0)
''        Call Carregar_Rateio_Corretagem(Proposta_Id, Dt_Inicio_vig, 0, 0, 0, 0) 'Alterado por fpimenta / Imar�s - 18/06/2003 (Regra definida pelo analista Marcio)
''
''        Call Ratear_Valor_Corretagem(Val_Tot_Corretagem_Rest, Perc_IR)
''
''        'Obtendo C�digo do estipulante''''''''''''''''''''''''''''''''''''''''''''''''''''
''
''        'bcarneiro - 13/03/03
''        'Call Obter_Dados_ProLabore(IIf(Origem = 1, Proposta_Basica_Adesao, Proposta_Id), Dt_Inicio_vig, 0, 0, 0, 0, Estipulante_Id, "", Perc_Pro_Labore)
''        Call Obter_Dados_ProLabore(IIf(Origem = 1, Proposta_Basica_Adesao, Proposta_Id), Dt_Contratacao, 0, 0, 0, 0, Estipulante_Id, "", Perc_Pro_Labore)
''
''        'Aprovando avalia��o t�cnica''''''''''''''''''''''''''''''''''''''''''''''''''''''
''
''        Call Aprovar_Avaliacao_Tecnica(Proposta_Id, Nr_Endosso_BB)
''
''        'Cancelando proposta''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''
''        Endosso_Id = Obter_Num_Endosso(Proposta_Id)
''
''        If Trim(Item.SubItems(15)) = "LIQUIDA��O" Then
''           iTipoEndosso = 104
''           Call Insere_Endosso(Proposta_Id, Endosso_Id, Nr_Endosso_BB, Dt_Cancelamento_BB, iTipoEndosso)
''           Call Atualizar_Registro_Emi(Emi_Id, Origem, "e", Proposta_Id, Endosso_Id)
''        Else
''
''           iTipoEndosso = 63
''           Call Insere_Endosso(Proposta_Id, Endosso_Id, Nr_Endosso_BB, Dt_Cancelamento_BB, iTipoEndosso)
''           Call Cancelar_Proposta(Proposta_Id, "02", Endosso_Id, Dt_Cancelamento_BB)
''           Call Cancelar_Agendamentos(Proposta_Id, Endosso_Id)
''
''           '----------------------------------------------------------------------------
''           'Luciana - 12/08/2003 - Rotina inclu�da para cancelar os endossos da proposta
''           '----------------------------------------------------------------------------
''           For j = 1 To Endosso_Id - 1
''               SQS = "SELECT * FROM agendamento_cobranca_tb "
''               SQS = SQS & " WHERE proposta_id  = " & Proposta_Id
''               SQS = SQS & "   AND num_endosso  = " & j
''               SQS = SQS & "   AND situacao    <> 'a'"
''               SQS = SQS & "   AND canc_endosso_id IS NULL"
''               Set rc = rdocn.OpenResultset(SQS)
''               If Not rc.EOF Then
''                   Call Insere_Cancelamento_Endosso(Proposta_Id, j, Endosso_Id)
''                   Call Cancelar_Agendamentos(Proposta_Id, Endosso_Id, j)
''               End If
''               rc.Close
''           Next
''           '----------------------------------------------------------------------------
''
''           Call Atualizar_Registro_Emi(Emi_Id, Origem, "c", Proposta_Id, Endosso_Id)
''
''        End If
''
''        'Gerando movimenta��o financeira''''''''''''''''''''''''''''''''''''''''''''''''''
''
''        If Val_Restituicao > 0 And verifica_endosso_liquidacao(Item.SubItems(10), Item.SubItems(9), Trim(Item.SubItems(15))) Then
''
''            'Incluido endosso''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''
''            Incluir_Endosso_Financeiro Proposta_Id, _
''                                       iTipoEndosso, _
''                                       Nr_Endosso_BB, _
''                                       Data_Sistema, _
''                                       Val_Restituicao * -1, _
''                                       Val_Tot_Corretagem_Rest * -1, _
''                                       Val_ProLabore_Rest * -1, _
''                                       Val_IOF_Rest * -1, _
''                                       Val_Tot_IR_Corretagem * -1, _
''                                       Val_Is_Restituicao * -1, _
''                                       Val_Desconto_Rest * -1, _
''                                       Val_Adic_Fracionamento_Rest * -1, _
''                                       Val_Custo_Apolice_Certif_Rest * -1, _
''                                       Val_Premio_Tarifa_Rest * -1, _
''                                       Dt_Cancelamento_BB, _
''                                       Endosso_Id
''
''            'Incluido Corretagem do endosso''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''
''            For Each Corretagem In Corretagens
''
''                Incluir_Corretagem_Endosso_Financeiro _
''                                           Proposta_Id, _
''                                           Endosso_Id, _
''                                           Corretagem.Corretor_Id, _
''                                           "0000", _
''                                           Corretagem.PercComissao, _
''                                           Corretagem.Val_Comissao * -1, _
''                                           Corretagem.Val_IR * -1
''            Next
''
''            'Incluindo Pr�-Labore do endosso''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''
''            If Val(Estipulante_Id) <> 0 Then
''                Incluir_ProLabore_Endosso_Financeiro _
''                                           Proposta_Id, _
''                                           Endosso_Id, _
''                                           Estipulante_Id, _
''                                           Perc_Pro_Labore
''            End If
''
''
''            'Incluindo Dados para cobran�a do corretor''''''''''''''''''''''''''''''''''''''''''''''''
''
''            Movimentacao_Id = ""
''            For Each Corretagem In Corretagens
''                Incluir_Mov_Corretor _
''                        Data_Sistema, _
''                        Corretagem.Val_Comissao, _
''                        Corretagem.Corretor_Id, _
''                        "0000", _
''                        "J", _
''                        Corretagem.Val_IR, _
''                        Corretagem.PercComissao, _
''                        "d", _
''                        "2", _
''                        Proposta_Id, _
''                        Endosso_Id, _
''                        Movimentacao_Id, _
''                        bFlagRetemComissao
''
''            Next
''
''            'Incluindo dados para cobran�a do estipulante''''''''''''''''''''''''''''''''''''''''''''
''
''            If Val(Estipulante_Id) <> 0 Then
''                Incluir_Mov_Estipulante _
''                        Data_Sistema, _
''                        Val_ProLabore_Rest, _
''                        Perc_Pro_Labore, _
''                        "d", _
''                        "2", _
''                        Movimentacao_Id, _
''                        Estipulante_Id, _
''                        Proposta_Id, _
''                        Endosso_Id, _
''                        bFlagRetemComissao
''            End If
''
''            'Incluindo dados para pagamento do cliente''''''''''''''''''''''''''''''''''''''''''''''
''
''            Incluir_Mov_Cliente _
''                    Data_Sistema, _
''                    Val_Repasse_BB, _
''                    "c", _
''                    "2", _
''                    Movimentacao_Id, _
''                    Cliente_ID_Beneficiario, _
''                    Proposta_Id, _
''                    Endosso_Id
''
''            'Incluido dados para recupera��o de imposto''''''''''''''''''''''''''''''''''''''
''
''            If Val_IOF_Rest > 0 Then
''                Inclui_Mov_Item_Financeiro _
''                                        "IOF", _
''                                        Data_Sistema, _
''                                        Val_IOF_Rest, _
''                                        "d", _
''                                        "2", _
''                                        Movimentacao_Id, _
''                                        Proposta_Id, _
''                                        Endosso_Id, _
''                                        bFlagRetemIOF
''            End If
''
''        End If
''
''    End If
''
''   'Inserindo dados em evento_tb - arodrigues 06/02/2003
''   'Call InserirEvento(Proposta_Id, Endosso_Id, iTipoEndosso, ramo_id, Produto_Id, Proposta_BB, Nr_Endosso_BB)
''
''Next
''
'''Finalizando transa��o''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''
''rdocn.CommitTrans
''
'''Atualizando interface''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''
''MousePointer = 0
''
''MsgBox "Opera��o conclu�da"
''
''Preparar_Informacoes CmbProduto.ItemData(CmbProduto.ListIndex)
''
''End Sub

''Sub Exibir_Detalhes(ByVal Item As ListItem)
''
'''Declara��es''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''
''Dim Arquivo As String
''Dim Proposta_BB As String
''Dim Nr_Endosso_BB As String
''Dim Dt_Recebimento_Endosso As String
''Dim Dt_Contratacao As String
''Dim Dt_Inicio_Vigencia As String
''Dim dt_fim_vigencia As String
''Dim Dt_Cancelamento_BB As String
''Dim Num_Proposta As String
''Dim ramo_id As String
''Dim Proposta_Basica_Adesao As String
''Dim periodo_pgto_id As String
''
''Dim val_is As Currency
''Dim Val_Premio As Currency
''Dim Val_Tot_IOF_Pago As Currency
''Dim Val_Tot_Premio_Pago As Currency
''Dim Val_Tot_Juros_Pago As Currency
''Dim Val_Tot_Adic_Fracionamento_Pago As Currency
''Dim Val_Tot_Desconto_Pago As Currency
''Dim Val_Custo_Apolice_Certif As Currency
''
''Dim Prazo_Restituicao_Integral As String
''Dim Tp_Calc_Restituicao As String
''
''Dim Val_Custo_Apolice_Certif_Rest As Currency
''Dim Val_Adic_Fracionamento_Rest As Currency
''Dim Val_Desconto_Rest As Currency
''Dim Val_IOF_Rest As Currency
''Dim Val_Premio_Tarifa_Rest As Currency
''Dim Val_Restituicao As Currency
''
''Dim Val_Tot_Corretagem_Rest As Currency
''Dim Val_ProLabore_Rest As Currency
''
''Dim Perc_IR As Currency
''
''Dim Val_Custo_Apolice_Certif_Calc As Currency
''Dim Val_Adic_Fracionamento_Calc As Currency
''Dim Val_Desconto_Calc As Currency
''Dim Val_IOF_Calc As Currency
''Dim Val_Premio_Tarifa_Calc As Currency
''Dim Val_Restituicao_Calc As Currency
''Dim Val_Tot_Corretagem_Calc As Currency
''Dim Val_ProLabore_Calc As Currency
''
''    'Atualizando interface'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''
''    MousePointer = vbHourglass
''
''    'Obtendo Vari�veis P�blicas''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''
''    Produto_Id = Item.SubItems(5)
''    Emi_Id = Item.SubItems(11)
''    Origem = Item.SubItems(13)
''
''    'Lendo Vari�veis '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''
''    Nr_Endosso_BB = Item.SubItems(1)
''    Dt_Recebimento_Endosso = Item.SubItems(2)
''    Proposta_BB = Item.SubItems(3)
''    Num_Proposta = Item.SubItems(4)
''    Dt_Contratacao = Item.SubItems(7)
''    Dt_Inicio_Vigencia = Item.SubItems(8)
''    dt_fim_vigencia = Item.SubItems(9)
''    Dt_Cancelamento_BB = Item.SubItems(10)
''    Arquivo = Left(Item.SubItems(12), InStr(Item.SubItems(12), ".") - 1)
''
''    'bcarneiro - 16/01/2003 - Inclus�o de Avalia��o de Liquida��o
''    If Trim(Item.SubItems(15)) = "LIQUIDA��O" Then
''        FrmAvaliacao.lblTipoAvaliacao = "Avalia��o de Pedido de Liquida��o"
''        FrmAvaliacao.gsTipoProcesso = "L"
''        FrmAvaliacao.lblDtLiq.Visible = True
''    Else
''        FrmAvaliacao.lblTipoAvaliacao = "Avalia��o de Pedido de Cancelamento"
''        FrmAvaliacao.gsTipoProcesso = "C"
''        FrmAvaliacao.lblDtLiq.Visible = False
''    End If
''
''    Call Obter_Dados_Adicionais_Proposta(Num_Proposta, ramo_id, Val_Custo_Apolice_Certif, periodo_pgto_id)
''
''    If Origem = "1" Then
''        Proposta_Basica_Adesao = BuscaParametro("BASICA " & Produto_Id)
''    End If
''
''    'Validando dados'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''
''    If Valida_Dados_Endosso(TpOperacao, Num_Proposta, Produto_Id, Dt_Cancelamento_BB, Dt_Contratacao, Dt_Inicio_Vigencia, dt_fim_vigencia, ramo_id, Nr_Endosso_BB, UCase(Left(Trim(Item.SubItems(15)), 1))) = False Then
''        MousePointer = vbDefault
''        Exit Sub
''    End If
''
''    'Bloqueando proposta'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''
''    Call Atualizar_Bloqueio_Proposta(Num_Proposta, "s")
''
''    'Lendo dados complementares''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''
''    Call Obter_Premio_IS(Num_Proposta, Produto_Id, val_is, Val_Premio)
''    Call Carregar_Colecao_Cobrancas(Num_Proposta)
''    Call Obter_Totais_Pagos(Val_Tot_Premio_Pago, Val_Tot_Juros_Pago, Val_Tot_Adic_Fracionamento_Pago, Val_Tot_Desconto_Pago, Val_Tot_IOF_Pago)
''    Call Obter_Dados_Produto(Produto_Id, Prazo_Restituicao_Integral, Tp_Calc_Restituicao)
''
''    'Obtendo dados percentual de pro-labore''''''''''''''''''''''''''''''''''''''''''''''''''''
''
''    'Call Obtem_Perc_IR(ramo_id, Dt_Inicio_Vigencia, Perc_IR)
''    Call Obtem_Perc_IR(ramo_id, Data_Sistema, Perc_IR) 'Alterado por Fl�vio D. Pimenta / Imar�s - 27/01/2003
''
''    'Verificando se a proposta j� foi avaliada'''''''''''''''''''''''''''''''''''''''''''''''''
''
''    'Existe_Avaliacao = Verificar_Existencia_Avaliacao(Num_Proposta)
''
''    'Obtendo valores da opera��o'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''
''    If TpOperacao = 1 Then
''
''        'Jorfilho 15-mai-2003: Com a inclus�o de produtos com ap�lice aberta,
''        'n�o calcular restitui��o se n�o houver fim de vig�ncia
''        If Trim(dt_fim_vigencia) <> "" Then
''
''          If periodo_pgto_id <> "4" Then
''
''            Call Obter_Valor_Restituicao(Num_Proposta, _
''                                         ramo_id, _
''                                         Dt_Contratacao, _
''                                         Dt_Inicio_Vigencia, _
''                                         DateDiff("d", Dt_Inicio_Vigencia, dt_fim_vigencia), _
''                                         DateDiff("d", Dt_Inicio_Vigencia, Dt_Cancelamento_BB), _
''                                         Tp_Calc_Restituicao, _
''                                         Prazo_Restituicao_Integral, _
''                                         Val_Premio, _
''                                         Val_Tot_Premio_Pago, _
''                                         Val_Tot_Juros_Pago, _
''                                         Val_Tot_Adic_Fracionamento_Pago, _
''                                         Val_Tot_Desconto_Pago, _
''                                         Val_Tot_IOF_Pago, _
''                                         Val_Custo_Apolice_Certif, _
''                                         Val_Restituicao, _
''                                         Val_IOF_Rest, _
''                                         Val_Custo_Apolice_Certif_Rest, _
''                                         Val_Adic_Fracionamento_Rest, _
''                                         Val_Desconto_Rest, _
''                                         Val_Premio_Tarifa_Rest)
''
''            If Not existe_perc_restituicao Then
''                MousePointer = vbDefault
''                Exit Sub
''            End If
''
''            'bcarneiro - 13/03/03
''            'Call Carregar_Rateio_Corretagem(Num_Proposta, Dt_Inicio_Vigencia, Val_Restituicao, Val_IOF_Rest, Val_Custo_Apolice_Certif_Rest, Perc_IR)
''            Call Carregar_Rateio_Corretagem(Num_Proposta, Dt_Contratacao, Val_Restituicao, Val_IOF_Rest, Val_Custo_Apolice_Certif_Rest, Perc_IR)
''
''            Call Obter_Total_Corretagem(Val_Tot_Corretagem_Rest)
''
''            'bcarneiro - 13/03/03
''            'Call Obter_Dados_ProLabore(IIf(Origem = 1, Proposta_Basica_Adesao, Num_Proposta), Dt_Inicio_Vigencia, Val_Restituicao, Val_IOF_Rest, Val_Custo_Apolice_Certif_Rest, Val_ProLabore_Rest, "", "", 0)
''            Call Obter_Dados_ProLabore(IIf(Origem = 1, Proposta_Basica_Adesao, Num_Proposta), Dt_Contratacao, Val_Restituicao, Val_IOF_Rest, Val_Custo_Apolice_Certif_Rest, Val_ProLabore_Rest, "", "", 0)
''
''            Val_Custo_Apolice_Certif_Calc = Val_Custo_Apolice_Certif_Rest
''            Val_Adic_Fracionamento_Calc = Val_Adic_Fracionamento_Rest
''            Val_Desconto_Calc = Val_Desconto_Rest
''            Val_IOF_Calc = Val_IOF_Rest
''            Val_Premio_Tarifa_Calc = Val_Premio_Tarifa_Rest
''            Val_Restituicao_Calc = Val_Restituicao
''            Val_Tot_Corretagem_Calc = Val_Tot_Corretagem_Rest
''            Val_ProLabore_Calc = Val_ProLabore_Rest
''
''            If vgsituacao = "p" Then
''                Val_Custo_Apolice_Certif = 0
''            End If
''
''          End If
''
''        End If
''
''    Else
''
''        Call Obter_Valores_Avaliacao_Tecnica(Num_Proposta, _
''                                       Val_Custo_Apolice_Certif_Rest, _
''                                       Val_Adic_Fracionamento_Rest, _
''                                       Val_Desconto_Rest, _
''                                       Val_IOF_Rest, _
''                                       Val_Premio_Tarifa_Rest, _
''                                       Val_Restituicao, _
''                                       Val_Restituicao_Calc, _
''                                       Val_Tot_Corretagem_Rest, _
''                                       Val_ProLabore_Rest, _
''                                       Val_Custo_Apolice_Certif_Calc, _
''                                       Val_Adic_Fracionamento_Calc, _
''                                       Val_Desconto_Calc, _
''                                       Val_IOF_Calc, _
''                                       Val_Premio_Tarifa_Calc, _
''                                       Val_Tot_Corretagem_Calc, _
''                                       Val_ProLabore_Calc)
''
''        'bcarneiro - 13/03/03
''        'Call Carregar_Rateio_Corretagem(Num_Proposta, Dt_Inicio_Vigencia , 0, 0, 0, 0)
''        'Call Carregar_Rateio_Corretagem(Num_Proposta, Dt_Contratacao, 0, 0, 0, 0)
''        Call Carregar_Rateio_Corretagem(Num_Proposta, Dt_Inicio_Vigencia, 0, 0, 0, 0) 'Alterado por fpimenta / Imar�s - 18/06/2003 (Regra definida pelo analista Marcio)
''
''        Call Ratear_Valor_Corretagem(Val_Tot_Corretagem_Rest, Perc_IR)
''
''    End If
''
''    'Descarregando form de sele��o''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''
''    Me.Hide
''
''    'Configurando interface'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''
''    FrmAvaliacao.Mask2ValPremioTarifa.Mask = "12V2"
''    FrmAvaliacao.Mask2ValPremioTarifa.Text = "0,00"
''
''    'Exibindo form de cancelamento''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''
''    FrmAvaliacao.Show
''
''    If TpOperacao = 1 Then
''        'bcarneiro - 16/01/2003 - Inclus�o de Avalia��o de Liquida��o
''        If Trim(Item.SubItems(15)) = "LIQUIDA��O" Then
''            FrmAvaliacao.Caption = "SEGP0209 - Avalia��o T�cnica de Liquida��o - " & Ambiente
''        Else
''            FrmAvaliacao.Caption = "SEGP0209 - Avalia��o T�cnica de Cancelamento - " & Ambiente
''        End If
''    Else
''        'bcarneiro - 16/01/2003 - Inclus�o de Avalia��o de Liquida��o
''        If Trim(Item.SubItems(15)) = "LIQUIDA��O" Then
''            FrmAvaliacao.Caption = "SEGP0209 - Avalia��o Gerencial de Liquida��o - " & Ambiente
''        Else
''            FrmAvaliacao.Caption = "SEGP0209 - Avalia��o Gerencial de Cancelamento - " & Ambiente
''        End If
''    End If
''
''    'Exibindo dados necess�rios para o cancelamento'''''''''''''''''''''''''''''''''''''''''''
''    'A pedido da Mary o campo Pr�mio Tarifa a ser restitu�do sempre estar� habilitado.
''    'FrmAvaliacao.Mask2ValPremioTarifa.Enabled = IIf(vgsituacao = "a", False, True)
''    FrmAvaliacao.Mask2ValPremioTarifa.Enabled = True
''
''    FrmAvaliacao.Ler_Dados_Proposta Num_Proposta
''    FrmAvaliacao.Preencher_GridCobrancas
''    FrmAvaliacao.Ler_Endossos Num_Proposta
''
''    FrmAvaliacao.Ler_Avaliacoes Num_Proposta, Nr_Endosso_BB
''
''    'Jorfilho 15-mai-2003: Com a inclus�o de produtos com ap�lice aberta,
''    'passar a validar cancelamento se n�o houver fim de vig�ncia
''    If Trim(dt_fim_vigencia) <> "" Then
''      FrmAvaliacao.LblPrazoSeguro.Caption = DateDiff("d", Dt_Inicio_Vigencia, dt_fim_vigencia)
''    Else
''      FrmAvaliacao.LblPrazoSeguro.Caption = ""
''    End If
''
''    FrmAvaliacao.LblDiasUtilizacaoSeguro.Caption = DateDiff("d", Dt_Inicio_Vigencia, Dt_Cancelamento_BB)
''
''    FrmAvaliacao.TxtDtRecebimentoEndosso.Text = Dt_Recebimento_Endosso
''    FrmAvaliacao.TxtDtCancelamentoBB.Text = Dt_Cancelamento_BB
''    FrmAvaliacao.TxtPropostaBB.Text = Proposta_BB
''    FrmAvaliacao.TxtEndossoBB.Text = Nr_Endosso_BB
''    FrmAvaliacao.txtProposta.Text = Num_Proposta
''    FrmAvaliacao.txtIs.Text = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(val_is))
''    FrmAvaliacao.txtPremio.Text = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(Val_Premio))
''
''    'Exibindo dados necess�rios para restitui��o''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''
''    FrmAvaliacao.LblTotValPremioPago.Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(Val_Tot_Premio_Pago))
''    FrmAvaliacao.LblTotValJuros.Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(Val_Tot_Juros_Pago))
''    FrmAvaliacao.LblCustoCertifApolice.Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(Val_Custo_Apolice_Certif))
''    FrmAvaliacao.LblValPremioEmitido.Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(Val_Premio))
''
''    'Exibindo valor estimado de restitui��o'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''
''    FrmAvaliacao.LblValRestituicaoEstimado = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(Val_Restituicao_Estimado))
''
''    'Exibindo valores calculados da restitui��o'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''
''    FrmAvaliacao.LblValRestituicaoEstimado.Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(Val_Restituicao_Calc))
''
''    FrmAvaliacao.LblValPremioTarifaCalc.Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(Val_Premio_Tarifa_Calc))
''    FrmAvaliacao.LblCustoCertifApoliceCalc.Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(Val_Custo_Apolice_Certif_Calc))
''    FrmAvaliacao.LblValIOFCalc.Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(Val_IOF_Calc))
''    FrmAvaliacao.LblValAdicFracionamentoCalc.Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(Val_Adic_Fracionamento_Calc))
''    FrmAvaliacao.LblValDescontoCalc.Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(Val_Desconto_Calc))
''    FrmAvaliacao.LblValRestituicaoCalc.Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(Val_Restituicao_Calc))
''
''    'Propondo valores da restitui��o informados''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''
''    FrmAvaliacao.Mask2ValPremioTarifa.Text = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(Val_Premio_Tarifa_Rest))
''    FrmAvaliacao.LblCustoCertifApoliceRest.Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(Val_Custo_Apolice_Certif_Rest))
''    FrmAvaliacao.LblValIOF.Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(Val_IOF_Rest))
''    FrmAvaliacao.LblValAdicFracionamento.Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(Val_Adic_Fracionamento_Rest))
''    FrmAvaliacao.LblValDesconto.Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(Val_Desconto_Rest))
''    FrmAvaliacao.LblValRestituicao.Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(Val_Restituicao))
''
''    'Exibindo valores calculados de comiss�o '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''
''    FrmAvaliacao.LblTotValCorretagemCalc.Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(Val_Tot_Corretagem_Calc))
''    FrmAvaliacao.LblValProLaboreCalc.Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(Val_ProLabore_Calc))
''
''    'Exibindo valores comiss�o Informados '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''
''    FrmAvaliacao.LblTotValCorretagem.Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(Val_Tot_Corretagem_Rest))
''    FrmAvaliacao.LblValProLabore.Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(Val_ProLabore_Rest))
''
''    'Configurando interface'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''
''    FrmAvaliacao.Definir_Exibicao_Objetos
''
''    'Verificando possibilidade de autoriza��o t�cnica'''''''''''''''''''''''''''''''''''''''''''
''
''    If TpOperacao = 1 Then
''
''        If Verificar_Ocorrencia_Sinistro(Num_Proposta) = True Then
''
''            MousePointer = vbDefault
''
''            'Alterado por Junior em 03/07/2002 conforme solicitado pelo Ricardo (SAS02-0074)
''            'Permitir a autoriza��o de cancelamento mesmo que exista sinistro, apenas alertando o usu�rio.
''            Call MsgBox("J� existe sinistro para esta proposta. O valor de restitui��o n�o ser� sugerido.", vbOKOnly + vbExclamation, "Aten��o")
''            Call Zera_Valores_Restituicao
''
''            'MsgBox "A proposta selecionada possui sinistro ocorrido. N�o � poss�vel a realiza��o da autoriza��o t�cnica.", vbOKOnly + vbExclamation, "Aten��o"
''            'FrmAvaliacao.BtnAceiteTecnico.Enabled = False
''            'FrmAvaliacao.Mask2ValPremioTarifa.Enabled = False
''
''        End If
''
''    End If
''
''    FrmAvaliacao.fraSemRestituicao.Visible = False
''
''    'bcarneiro - 26/02/2003 - Mensagem s� deve aparecer para Liquida��o de CPR
''    If Not verifica_endosso_liquidacao(Item.SubItems(10), Item.SubItems(9), Trim(Item.SubItems(15))) Then
''
''        FrmAvaliacao.LblPrazoRestituicaoIntegral.Caption = "0"
''        FrmAvaliacao.LblPrazoSeguro.Caption = "0"
''        FrmAvaliacao.LblTotValPremioPago.Caption = "0,00"
''        FrmAvaliacao.LblTotValJuros.Caption = "0,00"
''        FrmAvaliacao.LblValRestituicaoEstimado.Caption = "0,00"
''        FrmAvaliacao.LblValPremioTarifaCalc.Caption = "0,00"
''        FrmAvaliacao.LblCustoCertifApoliceCalc.Caption = "0,00"
''        FrmAvaliacao.LblValAdicFracionamentoCalc.Caption = "0,00"
''        FrmAvaliacao.LblValDescontoCalc.Caption = "0,00"
''        FrmAvaliacao.LblValIOFCalc.Caption = "0,00"
''        FrmAvaliacao.LblValRestituicaoCalc.Caption = "0,00"
''        FrmAvaliacao.LblTotValCorretagemCalc.Caption = "0,00"
''        FrmAvaliacao.LblValProLaboreCalc.Caption = "0,00"
''
''        FrmAvaliacao.LblTpCalcRestituicao.Caption = "0"
''        FrmAvaliacao.LblDiasUtilizacaoSeguro.Caption = "0"
''        FrmAvaliacao.LblValPremioEmitido.Caption = "0,00"
''        FrmAvaliacao.LblCustoCertifApolice.Caption = "0,00"
''        FrmAvaliacao.LblCustoCertifApoliceRest.Caption = "0,00"
''        FrmAvaliacao.LblValAdicFracionamento.Caption = "0,00"
''        FrmAvaliacao.LblValDesconto.Caption = "0,00"
''        FrmAvaliacao.LblValIOF.Caption = "0,00"
''        FrmAvaliacao.LblValRestituicao.Caption = "0,00"
''        FrmAvaliacao.LblTotValCorretagem.Caption = "0,00"
''        FrmAvaliacao.LblValProLabore.Caption = "0,00"
''
''        FrmAvaliacao.fraSemRestituicao.Visible = True
''
''    End If
''
''    'Configurando interface'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''    FrmAvaliacao.SSTabDados.Tab = 4
''
''    'Implementado por fpimenta / Imar�s - 08/07/2003 (Solicitado pelo usu�rio M�rio / Acompanhado pelo Analista M�rcio)
''    sSQL = "select   proposta_id" & vbCrLf
''    sSQL = sSQL & "from     endosso_tb a" & vbCrLf
''    sSQL = sSQL & "Where    a.Proposta_Id = " & Num_Proposta & vbCrLf
''    sSQL = sSQL & "and      a.tp_endosso_id = 200" & vbCrLf
''    sSQL = sSQL & "Union" & vbCrLf
''    sSQL = sSQL & "select   proposta_id" & vbCrLf
''    sSQL = sSQL & "From     endosso_financeiro_tb" & vbCrLf
''    sSQL = sSQL & "Where    Proposta_Id = " & Num_Proposta
''
''    Set rs = rdocn.OpenResultset(sSQL)
''
''    If Not rs.EOF Then
''        Call MsgBox("Proposta com endosso de movimenta��o financeira", vbInformation, "Aten��o")
''        If TpOperacao = 1 Then
''            FrmAvaliacao.Mask2ValPremioTarifa.Text = "0,00"
''        End If
''    End If
''
''    rs.Close
''    'Fim
''
''    MousePointer = vbDefault
''
''End Sub

Sub Marcar_Linha_Processada()

Dim Item As ListItem

    Set Item = ListSelEndosso.SelectedItem
    
    ' bcarneiro - 03/12/2003 - Projeto Restitui��o por Iniciativa da Seguradora
    ''If optRestituicao.Value Then
        Item.SubItems(12) = "S"
    ''Else
    ''    Item.SubItems(14) = "S"
    ''End If
    
    Item.Checked = False

    Call SetForecolorforListItem(Item, &H808080)

End Sub

Sub Selecionar_Todos()

Dim i As Integer
Dim Item As ListItem

    For i = 1 To ListSelEndosso.ListItems.Count

        Set Item = ListSelEndosso.ListItems(i)
        Item.Checked = True

    Next

End Sub

Private Sub btnAceiteTecnico_Click()
    If ListSelEndosso.ListItems.Count = 0 Then Exit Sub
    
    Call AutorizarRestituicoes
End Sub

Private Sub BtnAprovar_Click()

    If ListSelEndosso.ListItems.Count = 0 Then Exit Sub

    ' bcarneiro - 04/12/2003 - Projeto Restitui��o por Iniciativa da Seguradora
    ''If optRestituicao.Value Then
        
        Call AprovarRestituicoes
    ''Else
    ''    Call Aprovar_Cancelamentos
    ''End If
    
    

End Sub

Private Sub BtnAtualizarGrid_Click()

    If CmbProduto.ListIndex <> -1 Then
        ' bcarneiro - 04/12/2003 - Projeto Restitui��o por Iniciativa da Seguradora
        ''If optCancelamento.Value Then
        ''    Call Preparar_Informacoes(CmbProduto.ItemData(CmbProduto.ListIndex))
        ''Else
            Call PrepararInformacoesRestituicao(CmbProduto.ItemData(CmbProduto.ListIndex))
        ''End If
    End If

End Sub

Private Sub BtnModo_Click()

    If TpOperacao = 1 Then
        If MsgBox("Confirma a altera��o do modo para Avalia��o Gerencial?", vbYesNo + vbQuestion) = vbYes Then
            TpOperacao = 2
        Else
            Exit Sub
        End If
    Else
        If MsgBox("Confirma a altera��o do modo para Avalia��o T�cnica?", vbYesNo + vbQuestion) = vbYes Then
            TpOperacao = 1
        Else
            Exit Sub
        End If
    End If

    Call Unload(Me)

    FrmSelEndosso.Show

End Sub

Private Sub BtnSair_Click()

    End

End Sub

Private Sub BtnSelecionarTodos_Click()

    Call Selecionar_Todos

End Sub

Private Sub CmbProduto_Click()
    
    ' bcarneiro - 04/12/2003 - Projeto Restitui��o por Iniciativa da Seguradora
    ''If optCancelamento.Value Then
    ''    Call Preparar_Informacoes(CmbProduto.ItemData(CmbProduto.ListIndex))
    ''Else
        Call PrepararInformacoesRestituicao(CmbProduto.ItemData(CmbProduto.ListIndex))
    ''End If

End Sub

Private Sub cmdExportExcel_Click()
'Renato Vasconcelos
'Diego de Oliviera Campos
'In�cio

Dim xlApp As Object
   Dim xlWb As Object
   Dim xlWs As Object
   
   Set xlApp = CreateObject("Excel.Application")
   Set xlWb = xlApp.Workbooks.Add
   Set xlWs = xlWb.Worksheets(1)
   
   xlApp.Visible = True
   xlApp.UserControl = True
           
   Dim i As Integer
   i = 1
   
   xlWs.Cells(i, 1).Value = "Proposta BB"
   xlWs.Cells(i, 1).ColumnWidth = 15
   xlWs.Cells(i, 2).Value = "Proposta AB"
   xlWs.Cells(i, 2).ColumnWidth = 15
   xlWs.Cells(i, 3).Value = "Valor de Restitui��o"
   xlWs.Cells(i, 3).ColumnWidth = 20
   xlWs.Cells(i, 4).Value = "Nr. Avalia��o"
   xlWs.Cells(i, 4).ColumnWidth = 15
   xlWs.Cells(i, 5).Value = "Dt. Pedido da Avalia��o"
   xlWs.Cells(i, 5).ColumnWidth = 23
   xlWs.Cells(i, 6).Value = "Produto"
   xlWs.Cells(i, 6).ColumnWidth = 15
   xlWs.Cells(i, 7).Value = "Cliente"
   xlWs.Cells(i, 7).ColumnWidth = 35
   xlWs.Cells(i, 8).Value = "Dt. Contrata��o"
   xlWs.Cells(i, 8).ColumnWidth = 15
   xlWs.Cells(i, 9).Value = "Tipo de Avalia��o"
   xlWs.Cells(i, 9).ColumnWidth = 40
   xlWs.Cells(i, 10).Value = "Valor Subven��o Federal"
   xlWs.Cells(i, 10).ColumnWidth = 25
   xlWs.Cells(i, 11).Value = "Valor Subven��o Estadual"
   xlWs.Cells(i, 11).ColumnWidth = 25
   xlWs.Cells(i, 12).Value = "In�cio de Vig�ncia"
   xlWs.Cells(i, 12).ColumnWidth = 18
   xlWs.Cells(i, 13).Value = "Fim de Vig�ncia"
   xlWs.Cells(i, 13).ColumnWidth = 15
   xlWs.Cells(i, 14).Value = "IS (Import�ncia Segurada)"
   xlWs.Cells(i, 14).ColumnWidth = 27
   xlWs.Cells(i, 15).Value = "Valor Pr�mio Pago"
   xlWs.Cells(i, 15).ColumnWidth = 20
   xlWs.Cells(i, 16).Value = "Valor Pr�mio Retido"
   xlWs.Cells(i, 16).ColumnWidth = 20
   xlWs.Cells(i, 17).Value = "Data de Emiss�o do Endosso"
   xlWs.Cells(i, 17).ColumnWidth = 30
   'Renato Vasconcelos
   
   xlWs.Cells(i, 18).Value = "Valor Comiss�o"
   xlWs.Cells(i, 18).ColumnWidth = 15
   xlWs.Cells(i, 19).Value = "Valor Remunera��o"
   xlWs.Cells(i, 19).ColumnWidth = 18
   'Fim
   Dim j As Integer
   
   For j = 1 To 19
       xlWs.Cells(i, j).Font.Bold = True
   Next
   
   
   For i = 2 To ListSelEndosso.ListItems.Count + 1
     xlWs.Cells(i, 1).Value = ListSelEndosso.ListItems(i - 1).SubItems(1)
    xlWs.Cells(i, 2).Value = ListSelEndosso.ListItems(i - 1).SubItems(2)
    If Len(ListSelEndosso.ListItems(i - 1).SubItems(3)) = 0 Then
        xlWs.Cells(i, 3).Value = ""
    Else
        xlWs.Cells(i, 3).Value = CDbl(ListSelEndosso.ListItems(i - 1).SubItems(3))
        xlWs.Cells(i, 3).NumberFormat = "#,##0.00"
    End If
    
    xlWs.Cells(i, 4).Value = ListSelEndosso.ListItems(i - 1).SubItems(4)
    
    If Len(ListSelEndosso.ListItems(i - 1).SubItems(5)) = 0 Then
        xlWs.Cells(i, 5).Value = ""
    Else
        xlWs.Cells(i, 5).Value = CDate(ListSelEndosso.ListItems(i - 1).SubItems(5))
    End If
    
    xlWs.Cells(i, 6).Value = ListSelEndosso.ListItems(i - 1).SubItems(6)
    xlWs.Cells(i, 7).Value = ListSelEndosso.ListItems(i - 1).SubItems(7)
    If Len(ListSelEndosso.ListItems(i - 1).SubItems(8)) = 0 Then
        xlWs.Cells(i, 8).Value = ""
    Else
        xlWs.Cells(i, 8).Value = CDate(ListSelEndosso.ListItems(i - 1).SubItems(8))
        'xlWs.Cells(i, 8).DateFormat = "dd/mm/yyyy"
    End If
    
    
    xlWs.Cells(i, 9).Value = ListSelEndosso.ListItems(i - 1).SubItems(13)
    If Len(ListSelEndosso.ListItems(i - 1).SubItems(14)) = 0 Then
        xlWs.Cells(i, 10).Value = ""
    Else
        xlWs.Cells(i, 10).Value = CDbl(ListSelEndosso.ListItems(i - 1).SubItems(14))
        xlWs.Cells(i, 10).NumberFormat = "#,##0.00"
    End If
    
    
    
    xlWs.Cells(i, 11).Value = CDbl(ListSelEndosso.ListItems(i - 1).SubItems(15))
    xlWs.Cells(i, 11).NumberFormat = "#,##0.00"
    If IsDate(ListSelEndosso.ListItems(i - 1).SubItems(9)) Then
        xlWs.Cells(i, 12).Value = CDate(ListSelEndosso.ListItems(i - 1).SubItems(9))
    Else
        xlWs.Cells(i, 12).Value = ""
    End If
    If IsDate(ListSelEndosso.ListItems(i - 1).SubItems(10)) Then
        xlWs.Cells(i, 13).Value = CDate(ListSelEndosso.ListItems(i - 1).SubItems(10))
    Else
        xlWs.Cells(i, 13).Value = ""
    End If
    
    
    
    
    If Len(Trim(ListSelEndosso.ListItems(i - 1).SubItems(21))) > 0 Then
        xlWs.Cells(i, 14).Value = CDbl(ListSelEndosso.ListItems(i - 1).SubItems(21))
        xlWs.Cells(i, 14).NumberFormat = "#,##0.00"
    End If
    
    If Len(Trim(ListSelEndosso.ListItems(i - 1).SubItems(16))) > 0 Then
        xlWs.Cells(i, 15).Value = CDbl(ListSelEndosso.ListItems(i - 1).SubItems(16))
        xlWs.Cells(i, 15).NumberFormat = "#,##0.00"
        
    End If
    'Renato Vasconcelos
    If Len(Trim(ListSelEndosso.ListItems(i - 1).SubItems(17))) > 0 Then
        xlWs.Cells(i, 16).Value = CDbl(ListSelEndosso.ListItems(i - 1).SubItems(17))
        xlWs.Cells(i, 16).NumberFormat = "#,##0.00"
    
    End If
    
    If (IsDate(ListSelEndosso.ListItems(i - 1).SubItems(18))) Then
        xlWs.Cells(i, 17).Value = CDate(ListSelEndosso.ListItems(i - 1).SubItems(18))
    Else
        xlWs.Cells(i, 17).Value = ""
    End If
    
    
    
    
    If Len(Trim(ListSelEndosso.ListItems(i - 1).SubItems(19))) > 0 Then
        xlWs.Cells(i, 18).Value = CDbl(ListSelEndosso.ListItems(i - 1).SubItems(19))
        xlWs.Cells(i, 18).NumberFormat = "#,##0.00"
    End If
    If Len(Trim(ListSelEndosso.ListItems(i - 1).SubItems(20))) > 0 Then
        xlWs.Cells(i, 19).Value = CDbl(ListSelEndosso.ListItems(i - 1).SubItems(20))
        xlWs.Cells(i, 19).NumberFormat = "#,##0.00"
        
    End If
    'Fim
Next
'Fim
End Sub

Private Sub Form_Load()

    Call CentraFrm(Me)

    Call Carregar_ComboProduto

    If TpOperacao = 1 Then
    
        Me.Caption = "SEGP0209 - Avalia��o T�cnica de Restitui��o - " & Ambiente
        'Diego de Oliveira
        'BtnAprovar.Visible = False
        'BtnSelecionarTodos.Visible = False
        btnAprovar.Visible = False
        'Fim
        'Renato Vasconcelos
        btnAceiteTecnico.Visible = True
        cmdExportExcel.Visible = True
        'Fim
        BtnModo.ToolTipText = "Alterar para modo gerencial"
        
    Else
    
        Me.Caption = "SEGP0209 - Avalia��o Gerencial de Restitui��o - " & Ambiente
        BtnSelecionarTodos.Visible = True
        
        btnAprovar.Visible = True
        'Renato Vasconcelos
        btnAceiteTecnico.Visible = False
        'Diego de Oliveira
        cmdExportExcel.Visible = False
        'Fim
        BtnModo.ToolTipText = "Alterar para modo t�cnico"
        
    End If

End Sub

Private Sub Carregar_ComboProduto()

Dim rc As rdoResultset
Dim rc2 As Object
Dim sSQL As String
Dim iQuant As Double 'EDUARDO.BORGES - CONFITEC, 15/06/2011 - FLOWBR 11417511
Dim oFinanceiro As Object
Dim iProdutoId As Integer
Dim sSituacao As String

    MousePointer = vbHourglass
    
    If TpOperacao = 1 Then
        ListSelEndosso.Checkboxes = False
        sSituacao = SITUACAO_PENDENTE_AVALIACAO_TECNICA
    Else
        ListSelEndosso.Checkboxes = True
        sSituacao = SITUACAO_PENDENTE_AVALIACAO_GERENCIAL
    End If

    sSQL = ""
    sSQL = "SELECT "
    sSQL = sSQL & "   produto_id, "
    sSQL = sSQL & "   nome "
    sSQL = sSQL & "FROM "
    sSQL = sSQL & "   produto_tb  "
    sSQL = sSQL & "WHERE "
    ' racras inclus�o produtos BBC
    sSQL = sSQL & "  processa_restituicao_automatica = 's' or produto_id in(12,121,135,136,716,400) " 'Adicionado produto 400 para atender flow 4140933
    sSQL = sSQL & "ORDER BY "
    sSQL = sSQL & "   nome "

    Set rc = rdocn.OpenResultset(sSQL)

    Do While Not rc.EOF

        iProdutoId = rc("produto_id")
        
        Set oFinanceiro = CreateObject("SEGL0026.cls00125")
        
        'Demanda 846659 - Inclus�o de campos
        'Alterado em 11/05/2009 - Joana Agapito - GPTI
        ' Altera��o com o intuito de melhorar a performance, visto que a aplica��o SEGP0209 verifica
        'a exist�ncia de avalia��o de restitui��o para cada produto antes de exibir a combo.
        
        'Set rc2 = oFinanceiro.ConsultarAvaliacaoRestituicao(gsSIGLASISTEMA, _
                                                            App.Title, _
                                                            App.FileDescription, _
                                                            glAmbiente_id, _
                                                            sSituacao, _
                                                            iProdutoId)
                     
        
        Set rc2 = oFinanceiro.VerificaExistenciaAvaliacaoRestituicao(gsSIGLASISTEMA, _
                                                            App.Title, _
                                                            App.FileDescription, _
                                                            glAmbiente_id, _
                                                            sSituacao, _
                                                            iProdutoId)
                                                            
        iQuant = 0
        
        While Not rc2.EOF
            iQuant = iQuant + 1
            rc2.MoveNext
        Wend
        
        Set oFinanceiro = Nothing
            
        Set rc2 = Nothing

        If iQuant > 0 Then

            CmbProduto.AddItem UCase(rc("nome")) & " - " & iQuant & " endosso(s)"
            CmbProduto.ItemData(CmbProduto.NewIndex) = iProdutoId
            
        End If
        
        rc.MoveNext

    Loop

    rc.Close
    Set rc = Nothing
    
    MousePointer = vbNormal

End Sub

''Sub Preparar_Informacoes_Old(ByVal PProduto)
''
''Dim Rst As rdoResultset
''Dim SQL As String
'''  Dim Linha As String
''Dim Item As ListItem
''
'''Atualizando interface''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''
''ListSelEndosso.Visible = False
''
'''Configurando ListView''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''
''ListSelEndosso.View = lvwReport
''ListSelEndosso.FullRowSelect = True
''ListSelEndosso.HideSelection = False
''ListSelEndosso.LabelEdit = lvwManual
''ListSelEndosso.HotTracking = True
''
'''Configurando header do ListView''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''
''ListSelEndosso.ColumnHeaders.Clear
''
''If TpOperacao = 1 Then
''    ListSelEndosso.ColumnHeaders.Add , , "Selecionar", 0
''Else
''    ListSelEndosso.ColumnHeaders.Add , , "Selecionar", 600
''End If
''ListSelEndosso.ColumnHeaders.Add , , "Nr. Endosso", 1200
''ListSelEndosso.ColumnHeaders.Add , , "Recebimento", 1500
''ListSelEndosso.ColumnHeaders.Add , , "Proposta BB", 1200
''ListSelEndosso.ColumnHeaders.Add , , "Proposta Id", 1200
''ListSelEndosso.ColumnHeaders.Add , , "Produto", 800
''ListSelEndosso.ColumnHeaders.Add , , "Cliente", 8500
''ListSelEndosso.ColumnHeaders.Add , , "Contratacao", 1500
''ListSelEndosso.ColumnHeaders.Add , , "Inicio Vigencia", 1500
''ListSelEndosso.ColumnHeaders.Add , , "Fim Vigencia", 1500
''ListSelEndosso.ColumnHeaders.Add , , "Dt. Cancelamento", 1500
''ListSelEndosso.ColumnHeaders.Add , , "Emi Id", 0
''ListSelEndosso.ColumnHeaders.Add , , "Arquivo", 2000
''ListSelEndosso.ColumnHeaders.Add , , "Origem", 0
''ListSelEndosso.ColumnHeaders.Add , , "Processado", 0
''
'''Carregando form de espera'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''
''FrmEspera.Show
''
''DoEvents
''
'''Selecionando endossos de cancelamento com gera��o de restitui��o pendentes de aceite
'''na emi_proposta_tb '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''
''SQL = "Select "
''SQL = SQL & "a.num_endosso, "
''SQL = SQL & "dt_recebimento = a.dt_inclusao, "
''SQL = SQL & "a.proposta_bb, "
''SQL = SQL & "c.proposta_id, "
''SQL = SQL & "c.produto_id, "
''SQL = SQL & "d.nome, "
''SQL = SQL & "c.dt_contratacao, "
''SQL = SQL & "b.dt_inicio_vigencia, "
''SQL = SQL & "b.dt_fim_vigencia, "
''SQL = SQL & "dt_cancelamento = isnull(a.dt_cancelamento, a.dt_endosso), "
''SQL = SQL & "emi_id = a.emi_proposta_id, "
''SQL = SQL & "a.arquivo, "
''SQL = SQL & "origem = 1 "
''SQL = SQL & "From "
''SQL = SQL & "     emi_proposta_tb a "
''SQL = SQL & "     Inner Join proposta_adesao_tb b on b.proposta_bb = a.proposta_bb "
''SQL = SQL & "     Inner Join proposta_tb c on c.proposta_id = b.proposta_id "
''SQL = SQL & "     Inner Join cliente_tb d on d.cliente_id = c.prop_cliente_id "
''SQL = SQL & "     Inner Join produto_tb p on p.produto_id = c.produto_id "
''SQL = SQL & "Where "
''SQL = SQL & "     a.tp_registro = '65'"
''SQL = SQL & "     and a.situacao = 'p' "
'''SQL = SQL & "     and c.situacao in ('a','i') "
''SQL = SQL & "     and p.processa_restituicao_automatica = 's' "
''SQL = SQL & "     and c.produto_id in "
''SQL = SQL & "                       (SELECT "
''SQL = SQL & "                               f.Produto_Id "
''SQL = SQL & "                        FROM "
''SQL = SQL & "                               arquivo_produto_tb e "
''SQL = SQL & "                               Inner Join produto_tb f on f.produto_id = e.produto_id "
''SQL = SQL & "                        WHERE "
''SQL = SQL & "                               e.arquivo = substring(a.arquivo,1,charindex('.',a.arquivo) - 1)) "
''
''SQL = SQL & "     and EXISTS "
''SQL = SQL & "                       (SELECT proposta_id "
''SQL = SQL & "                        FROM agendamento_cobranca_tb "
''SQL = SQL & "                        WHERE proposta_id = c.proposta_id )"
'''MARTINES
'''SQL = SQL & "                              and situacao = 'a') "
''
''SQL = SQL & "     and c.produto_id = " & PProduto
''
''If TpOperacao = "1" Then
''
''    'An�lise t�cnica
''
''    SQL = SQL & "     and (NOT EXISTS "
''    SQL = SQL & "                       (SELECT proposta_id "
''    SQL = SQL & "                        FROM avaliacao_cancelamento_tb "
''    SQL = SQL & "                        WHERE proposta_id = c.proposta_id) "
''    SQL = SQL & "          OR 'r' = (SELECT situacao "
''    SQL = SQL & "                    FROM avaliacao_cancelamento_tb ac "
''    SQL = SQL & "                    WHERE "
''    SQL = SQL & "                           ac.proposta_id = c.proposta_id "
''    SQL = SQL & "                           AND ac.num_avaliacao = (SELECT max(num_avaliacao)"
''    SQL = SQL & "                                                   FROM avaliacao_cancelamento_tb ac2 "
''    SQL = SQL & "                                                   WHERE ac2.proposta_id = ac.proposta_id))) "
''
''Else
''
''    'An�lise gerencial
''
''    SQL = SQL & "     and  'p' = (SELECT situacao "
''    SQL = SQL & "                 FROM avaliacao_cancelamento_tb ac "
''    SQL = SQL & "                 WHERE "
''    SQL = SQL & "                       ac.proposta_id = c.proposta_id "
''    SQL = SQL & "                       AND ac.num_avaliacao = (SELECT max(num_avaliacao)"
''    SQL = SQL & "                                               FROM avaliacao_cancelamento_tb ac2 "
''    SQL = SQL & "                                               WHERE ac2.proposta_id = ac.proposta_id)) "
''
''End If
''
''SQL = SQL & "Order by "
''SQL = SQL & "     a.num_endosso "
''
''Set Rst = rdocn.OpenResultset(SQL)
''
''If Rst.EOF Then
''
''    Rst.Close
''
''    'Selecionando endossos de cancelamento com gera��o de restitui��o pendentes de aceite
''    'na emi_re_proposta_tb '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''
''    SQL = "Select "
''    SQL = SQL & "a.num_endosso, "
''    SQL = SQL & "dt_recebimento = a.dt_inclusao, "
''    SQL = SQL & "a.proposta_bb, "
''    SQL = SQL & "c.proposta_id, "
''    SQL = SQL & "c.produto_id, "
''    SQL = SQL & "d.nome, "
''    SQL = SQL & "c.dt_contratacao, "
''    SQL = SQL & "b.dt_inicio_vigencia, "
''    SQL = SQL & "b.dt_fim_vigencia, "
''    SQL = SQL & "dt_cancelamento = isnull(a.dt_cancelamento, a.dt_endosso), "
''    SQL = SQL & "emi_id = a.emi_re_proposta_id, "
''    SQL = SQL & "a.arquivo, "
''    SQL = SQL & "origem = 2 "
''    SQL = SQL & "From "
''    SQL = SQL & "     emi_re_proposta_tb a "
''    SQL = SQL & "     Inner Join proposta_adesao_tb b on b.proposta_bb = a.proposta_bb "
''    SQL = SQL & "     Inner Join proposta_tb c on c.proposta_id = b.proposta_id "
''    SQL = SQL & "     Inner Join cliente_tb d on d.cliente_id = c.prop_cliente_id "
''    SQL = SQL & "     Inner Join produto_tb p on p.produto_id = c.produto_id "
''    SQL = SQL & "Where "
''    SQL = SQL & "     a.tp_registro = '03'"
''    SQL = SQL & "     and a.tipo_endosso = 65 "
''    SQL = SQL & "     and a.situacao = 'p' "
''    'SQL = SQL & "     and c.situacao in ('a','i') "
''    SQL = SQL & "     and p.processa_restituicao_automatica = 's' "
''    SQL = SQL & "     and c.produto_id in "
''    SQL = SQL & "                       (Select "
''    SQL = SQL & "                               f.Produto_Id "
''    SQL = SQL & "                       From "
''    SQL = SQL & "                               arquivo_produto_tb e "
''    SQL = SQL & "                               Inner Join produto_tb f on f.produto_id = e.produto_id "
''    SQL = SQL & "                       Where "
''    SQL = SQL & "                               e.arquivo = substring(a.arquivo,1,charindex('.',a.arquivo) - 1)) "
''    SQL = SQL & "     and EXISTS "
''    SQL = SQL & "                       (SELECT proposta_id "
''    SQL = SQL & "                        FROM agendamento_cobranca_tb "
''    SQL = SQL & "                        WHERE proposta_id = c.proposta_id AND num_endosso > 0) "
''    'MARTINES
''    'SQL = SQL & "                              and situacao = 'a') "
''
''    SQL = SQL & "     and NOT EXISTS "
''    SQL = SQL & "                       (SELECT proposta_id "
''    SQL = SQL & "                        FROM endosso_tb "
''    SQL = SQL & "                        WHERE proposta_id = c.proposta_id"
''    SQL = SQL & "                              AND tp_endosso_id = 200) "
''
''    SQL = SQL & "     and c.produto_id = " & PProduto
''
''    If TpOperacao = 1 Then
''
''        'An�lise t�cnica
''
''        SQL = SQL & "     and (NOT EXISTS "
''        SQL = SQL & "                       (SELECT proposta_id "
''        SQL = SQL & "                        FROM avaliacao_cancelamento_tb "
''        SQL = SQL & "                        WHERE proposta_id = c.proposta_id) "
''        SQL = SQL & "          OR 'r' = (SELECT situacao "
''        SQL = SQL & "                    FROM avaliacao_cancelamento_tb ac "
''        SQL = SQL & "                    WHERE "
''        SQL = SQL & "                           ac.proposta_id = c.proposta_id "
''        SQL = SQL & "                           AND ac.num_avaliacao = (SELECT max(num_avaliacao)"
''        SQL = SQL & "                                                   FROM avaliacao_cancelamento_tb ac2 "
''        SQL = SQL & "                                                   WHERE ac2.proposta_id = ac.proposta_id))) "
''
''    Else
''
''        'An�lise gerencial
''
''        SQL = SQL & "      and 'p' = (SELECT situacao "
''        SQL = SQL & "                 FROM avaliacao_cancelamento_tb ac "
''        SQL = SQL & "                 WHERE "
''        SQL = SQL & "                           ac.proposta_id = c.proposta_id "
''        SQL = SQL & "                           AND ac.num_avaliacao = (SELECT max(num_avaliacao)"
''        SQL = SQL & "                                                   FROM avaliacao_cancelamento_tb ac2 "
''        SQL = SQL & "                                                   WHERE ac2.proposta_id = ac.proposta_id)) "
''
''    End If
''
''    SQL = SQL & "Order by "
''    SQL = SQL & "     a.num_endosso "
''
''    Set Rst = rdocn.OpenResultset(SQL)
''
''End If
''
'''Limpando grid''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''
''ListSelEndosso.ListItems.Clear
''
''Do While Not Rst.EOF
''
''    'Preenchendo Grid'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''
''    Set Item = ListSelEndosso.ListItems.Add(, , "")
''
''    Item.SubItems(1) = Rst!Num_endosso
''    Item.SubItems(2) = Format(Rst!Dt_Recebimento, "dd/mm/yyyy")
''    Item.SubItems(3) = Rst!Proposta_BB
''    Item.SubItems(4) = Rst!Proposta_Id
''    Item.SubItems(5) = Rst!Produto_Id
''    Item.SubItems(6) = Rst!Nome
''    Item.SubItems(7) = Format(Rst!Dt_Contratacao, "dd/mm/yyyy")
''    Item.SubItems(8) = Format(Rst!Dt_Inicio_Vigencia, "dd/mm/yyyy")
''    Item.SubItems(9) = Format(Rst!dt_fim_vigencia, "dd/mm/yyyy")
''    Item.SubItems(10) = Format(Rst!dt_cancelamento, "dd/mm/yyyy")
''    Item.SubItems(11) = Rst!Emi_Id
''    Item.SubItems(12) = Rst!Arquivo
''    Item.SubItems(13) = Rst!Origem
''    Item.SubItems(14) = "N"
''
''    'Posicionando-se no pr�ximo registro'''''''''''''''''''''''''''''''''''''''''''''''''''
''
''    Rst.MoveNext
''
''Loop
''
''Rst.Close
''
'''Atualizando interface''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''
''ListSelEndosso.Visible = True
''
''StatusBar1.SimpleText = "Total de registros encontrados: " & ListSelEndosso.ListItems.Count
''
'''Descarregando form de espera'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''
''Unload FrmEspera
''
''End Sub
''
''Sub Preparar_Informacoes_Teste_Left_Join_RE(ByVal PProduto)
''
''Dim Rst As rdoResultset
''Dim SQL As String
'''    Dim Linha As String
''Dim Item As ListItem
''
''Dim TpRamoProduto As String
''
''Const TpRamoVida = 1
''Const TpRamoRE = 2
''
'''Atualizando interface''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''
''ListSelEndosso.Visible = False
''
'''Configurando ListView''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''
''ListSelEndosso.View = lvwReport
''ListSelEndosso.FullRowSelect = True
''ListSelEndosso.HideSelection = False
''ListSelEndosso.LabelEdit = lvwManual
''ListSelEndosso.HotTracking = True
''
'''Configurando header do ListView''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''
''ListSelEndosso.ColumnHeaders.Clear
''
''If TpOperacao = 1 Then
''    ListSelEndosso.ColumnHeaders.Add , , "Selecionar", 0
''Else
''    ListSelEndosso.ColumnHeaders.Add , , "Selecionar", 600
''End If
''ListSelEndosso.ColumnHeaders.Add , , "Nr. Endosso", 1200
''ListSelEndosso.ColumnHeaders.Add , , "Recebimento", 1500
''ListSelEndosso.ColumnHeaders.Add , , "Proposta BB", 1200
''ListSelEndosso.ColumnHeaders.Add , , "Proposta Id", 1200
''ListSelEndosso.ColumnHeaders.Add , , "Produto", 800
''ListSelEndosso.ColumnHeaders.Add , , "Cliente", 8500
''ListSelEndosso.ColumnHeaders.Add , , "Contratacao", 1500
''ListSelEndosso.ColumnHeaders.Add , , "Inicio Vigencia", 1500
''ListSelEndosso.ColumnHeaders.Add , , "Fim Vigencia", 1500
''ListSelEndosso.ColumnHeaders.Add , , "Dt. Cancelamento", 1500
''ListSelEndosso.ColumnHeaders.Add , , "Emi Id", 0
''ListSelEndosso.ColumnHeaders.Add , , "Arquivo", 2000
''ListSelEndosso.ColumnHeaders.Add , , "Origem", 0
''ListSelEndosso.ColumnHeaders.Add , , "Processado", 0
''
'''Carregando form de espera'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''
''FrmEspera.Show
''
''DoEvents
''
'''Obtendo ramo do produto''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''
''TpRamoProduto = Obter_TpRamo_Produto(PProduto)
''
'''Selecionando endossos de cancelamento com gera��o de restitui��o pendentes de aceite
'''na emi_proposta_tb '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''
''Select Case TpRamoProduto
''
''    Case TpRamoVida
''
''        SQL = "Select "
''        SQL = SQL & "a.num_endosso, "
''        SQL = SQL & "dt_recebimento = a.dt_inclusao, "
''        SQL = SQL & "a.proposta_bb, "
''        SQL = SQL & "c.proposta_id, "
''        SQL = SQL & "c.produto_id, "
''        SQL = SQL & "d.nome, "
''        SQL = SQL & "c.dt_contratacao, "
''        SQL = SQL & "b.dt_inicio_vigencia, "
''        SQL = SQL & "b.dt_fim_vigencia, "
''        SQL = SQL & "dt_cancelamento = isnull(a.dt_cancelamento, a.dt_endosso), "
''        SQL = SQL & "emi_id = a.emi_proposta_id, "
''        SQL = SQL & "a.arquivo, "
''        SQL = SQL & "origem = 1 "
''        SQL = SQL & "From "
''        SQL = SQL & "     emi_proposta_tb a "
''        SQL = SQL & "     Inner Join proposta_adesao_tb b on b.proposta_bb = a.proposta_bb "
''        SQL = SQL & "     Inner Join proposta_tb c on c.proposta_id = b.proposta_id "
''        SQL = SQL & "     Inner Join cliente_tb d on d.cliente_id = c.prop_cliente_id "
''        SQL = SQL & "     Inner Join produto_tb p on p.produto_id = c.produto_id "
''        SQL = SQL & "Where "
''        SQL = SQL & "     a.tp_registro = '65'"
''        SQL = SQL & "     and a.situacao = 'p' "
''        'SQL = SQL & "     and c.situacao in ('a','i') "
''        SQL = SQL & "     and p.processa_restituicao_automatica = 's' "
''        SQL = SQL & "     and c.produto_id in "
''        SQL = SQL & "                       (SELECT "
''        SQL = SQL & "                               f.Produto_Id "
''        SQL = SQL & "                        FROM "
''        SQL = SQL & "                               arquivo_produto_tb e "
''        SQL = SQL & "                               Inner Join produto_tb f on f.produto_id = e.produto_id "
''        SQL = SQL & "                        WHERE "
''        SQL = SQL & "                               e.arquivo = substring(a.arquivo,1,charindex('.',a.arquivo) - 1)) "
''
''        SQL = SQL & "     and EXISTS "
''        SQL = SQL & "                       (SELECT proposta_id "
''        SQL = SQL & "                        FROM agendamento_cobranca_tb "
''        SQL = SQL & "                        WHERE proposta_id = c.proposta_id AND num_endosso > 0) "
'''       MARTINES
'''       SQL = SQL & "                              and situacao = 'a') "
''
''        SQL = SQL & "     and c.produto_id = " & PProduto
''
''        If TpOperacao = "1" Then
''
''            'An�lise t�cnica
''
''            SQL = SQL & "     and (NOT EXISTS "
''            SQL = SQL & "                       (SELECT proposta_id "
''            SQL = SQL & "                        FROM avaliacao_cancelamento_tb "
''            SQL = SQL & "                        WHERE proposta_id = c.proposta_id) "
''            SQL = SQL & "          OR 'r' = (SELECT situacao "
''            SQL = SQL & "                    FROM avaliacao_cancelamento_tb ac "
''            SQL = SQL & "                    WHERE "
''            SQL = SQL & "                           ac.proposta_id = c.proposta_id "
''            SQL = SQL & "                           AND ac.num_avaliacao = (SELECT max(num_avaliacao)"
''            SQL = SQL & "                                                   FROM avaliacao_cancelamento_tb ac2 "
''            SQL = SQL & "                                                   WHERE ac2.proposta_id = ac.proposta_id))) "
''
''        Else
''
''            'An�lise gerencial
''
''            SQL = SQL & "     and  'p' = (SELECT situacao "
''            SQL = SQL & "                 FROM avaliacao_cancelamento_tb ac "
''            SQL = SQL & "                 WHERE "
''            SQL = SQL & "                       ac.proposta_id = c.proposta_id "
''            SQL = SQL & "                       AND ac.num_avaliacao = (SELECT max(num_avaliacao)"
''            SQL = SQL & "                                               FROM avaliacao_cancelamento_tb ac2 "
''            SQL = SQL & "                                               WHERE ac2.proposta_id = ac.proposta_id)) "
''
''        End If
''
''        SQL = SQL & "Order by "
''        SQL = SQL & "     a.num_endosso "
''
''        Set Rst = rdocn.OpenResultset(SQL)
''
''    Case TpRamoRE
''
''        'Selecionando endossos de cancelamento com gera��o de restitui��o pendentes de aceite
''        'na emi_re_proposta_tb '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''
''        SQL = "Select "
''        SQL = SQL & "a.num_endosso, "
''        SQL = SQL & "dt_recebimento = a.dt_inclusao, "
''        SQL = SQL & "a.proposta_bb, "
''        SQL = SQL & "c.proposta_id, "
''        SQL = SQL & "c.produto_id, "
''        SQL = SQL & "d.nome, "
''        SQL = SQL & "c.dt_contratacao, "
''        SQL = SQL & "dt_inicio_vigencia = isnull(b.dt_inicio_vigencia, b3.dt_inicio_vigencia), "
''        SQL = SQL & "dt_fim_vigencia = isnull(b.dt_fim_vigencia, b3.dt_fim_vigencia), "
''        SQL = SQL & "dt_cancelamento = isnull(a.dt_cancelamento, a.dt_endosso), "
''        SQL = SQL & "emi_id = a.emi_re_proposta_id, "
''        SQL = SQL & "a.arquivo, "
''        SQL = SQL & "origem = 2 "
''        SQL = SQL & "From "
''        SQL = SQL & "     emi_re_proposta_tb a "
''        SQL = SQL & "     Left Join proposta_adesao_tb b on b.proposta_bb = a.proposta_bb "
''        SQL = SQL & "     Left Join proposta_fechada_tb b2 on b2.proposta_bb = a.proposta_bb "
''        SQL = SQL & "     Left Join apolice_tb b3 on b3.proposta_id = isnull(b.proposta_id, b2.proposta_id) "
''        SQL = SQL & "     Inner Join proposta_tb c on c.proposta_id = isnull(b.proposta_id, b2.proposta_id) "
''        SQL = SQL & "     Inner Join cliente_tb d on d.cliente_id = c.prop_cliente_id "
''        SQL = SQL & "     Inner Join produto_tb p on p.produto_id = c.produto_id "
''        SQL = SQL & "Where "
''        SQL = SQL & "     a.tp_registro = '03'"
''        SQL = SQL & "     and a.tipo_endosso = 65 "
''        SQL = SQL & "     and a.situacao = 'p' "
''        'SQL = SQL & "     and c.situacao in ('a','i') "
''        SQL = SQL & "     and p.processa_restituicao_automatica = 's' "
''        SQL = SQL & "     and c.produto_id in "
''        SQL = SQL & "                       (Select "
''        SQL = SQL & "                               f.Produto_Id "
''        SQL = SQL & "                       From "
''        SQL = SQL & "                               arquivo_produto_tb e "
''        SQL = SQL & "                               Inner Join produto_tb f on f.produto_id = e.produto_id "
''        SQL = SQL & "                       Where "
''        SQL = SQL & "                               e.arquivo = substring(a.arquivo,1,charindex('.',a.arquivo) - 1)) "
''        SQL = SQL & "     and EXISTS "
''        SQL = SQL & "                       (SELECT proposta_id "
''        SQL = SQL & "                        FROM agendamento_cobranca_tb "
''        SQL = SQL & "                        WHERE proposta_id = c.proposta_id AND num_endosso > 0) "
'''       MARTINES
'''       SQL = SQL & "                              and situacao = 'a') "
''
''        SQL = SQL & "     and NOT EXISTS "
''        SQL = SQL & "                       (SELECT proposta_id "
''        SQL = SQL & "                        FROM endosso_tb "
''        SQL = SQL & "                        WHERE proposta_id = c.proposta_id"
''        SQL = SQL & "                              AND tp_endosso_id = 200) "
''
''        SQL = SQL & "     and c.produto_id = " & PProduto
''
''        If TpOperacao = 1 Then
''
''            'An�lise t�cnica
''
''            SQL = SQL & "     and (NOT EXISTS "
''            SQL = SQL & "                       (SELECT proposta_id "
''            SQL = SQL & "                        FROM avaliacao_cancelamento_tb "
''            SQL = SQL & "                        WHERE proposta_id = c.proposta_id) "
''            SQL = SQL & "          OR 'r' = (SELECT situacao "
''            SQL = SQL & "                    FROM avaliacao_cancelamento_tb ac "
''            SQL = SQL & "                    WHERE "
''            SQL = SQL & "                           ac.proposta_id = c.proposta_id "
''            SQL = SQL & "                           AND ac.num_avaliacao = (SELECT max(num_avaliacao)"
''            SQL = SQL & "                                                   FROM avaliacao_cancelamento_tb ac2 "
''            SQL = SQL & "                                                   WHERE ac2.proposta_id = ac.proposta_id))) "
''
''        Else
''
''            'An�lise gerencial
''
''            SQL = SQL & "      and 'p' = (SELECT situacao "
''            SQL = SQL & "                 FROM avaliacao_cancelamento_tb ac "
''            SQL = SQL & "                 WHERE "
''            SQL = SQL & "                           ac.proposta_id = c.proposta_id "
''            SQL = SQL & "                           AND ac.num_avaliacao = (SELECT max(num_avaliacao)"
''            SQL = SQL & "                                                   FROM avaliacao_cancelamento_tb ac2 "
''            SQL = SQL & "                                                   WHERE ac2.proposta_id = ac.proposta_id)) "
''
''        End If
''
''        SQL = SQL & "Order by "
''        SQL = SQL & "     a.num_endosso "
''
''        Set Rst = rdocn.OpenResultset(SQL)
''
''End Select
''
'''Limpando grid''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''
''ListSelEndosso.ListItems.Clear
''
''Do While Not Rst.EOF
''
''    'Preenchendo Grid'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''
''    Set Item = ListSelEndosso.ListItems.Add(, , "")
''
''    Item.SubItems(1) = Rst!Num_endosso
''    Item.SubItems(2) = Format(Rst!Dt_Recebimento, "dd/mm/yyyy")
''    Item.SubItems(3) = Rst!Proposta_BB
''    Item.SubItems(4) = Rst!Proposta_Id
''    Item.SubItems(5) = Rst!Produto_Id
''    Item.SubItems(6) = Rst!Nome
''    Item.SubItems(7) = Format(Rst!Dt_Contratacao, "dd/mm/yyyy")
''    Item.SubItems(8) = Format(Rst!Dt_Inicio_Vigencia, "dd/mm/yyyy")
''    Item.SubItems(9) = Format(Rst!dt_fim_vigencia, "dd/mm/yyyy")
''    Item.SubItems(10) = Format(Rst!dt_cancelamento, "dd/mm/yyyy")
''    Item.SubItems(11) = Rst!Emi_Id
''    Item.SubItems(12) = Rst!Arquivo
''    Item.SubItems(13) = Rst!Origem
''    Item.SubItems(14) = "N"
''
''    'Posicionando-se no pr�ximo registro'''''''''''''''''''''''''''''''''''''''''''''''''''
''
''    Rst.MoveNext
''
''Loop
''
''Rst.Close
''
'''Atualizando interface''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''
''ListSelEndosso.Visible = True
''
''StatusBar1.SimpleText = "Total de registros encontrados: " & ListSelEndosso.ListItems.Count
''
'''Descarregando form de espera'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''
''Unload FrmEspera
''
''End Sub

''Sub Preparar_Informacoes(ByVal PProduto)
''
''Dim Rst As rdoResultset
''Dim SQL As String
''Dim Item As ListItem
''Dim TpRamoProduto As String
''
''Const TpRamoVida = 1
''Const TpRamoRE = 2
''
''    'Atualizando interface''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''
''    ListSelEndosso.Visible = False
''
''    'Configurando ListView''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''
''    If TpOperacao = 1 Then
''        ListSelEndosso.Checkboxes = False
''    Else
''        ListSelEndosso.Checkboxes = True
''    End If
''
''    ListSelEndosso.View = lvwReport
''    ListSelEndosso.FullRowSelect = True
''    ListSelEndosso.LabelEdit = lvwManual
''    ListSelEndosso.HotTracking = True
''
''    'Configurando header do ListView''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''
''    ListSelEndosso.ColumnHeaders.Clear
''
''    If TpOperacao = 1 Then
''        ListSelEndosso.ColumnHeaders.Add , , "", 300
''    Else
''        ListSelEndosso.ColumnHeaders.Add , , "Selecionar", 600
''    End If
''    ListSelEndosso.ColumnHeaders.Add , , "Nr. Endosso", 1200
''    ListSelEndosso.ColumnHeaders.Add , , "Recebimento", 1500
''    ListSelEndosso.ColumnHeaders.Add , , "Proposta BB", 1200
''    ListSelEndosso.ColumnHeaders.Add , , "Proposta Id", 1200
''    ListSelEndosso.ColumnHeaders.Add , , "Produto", 800
''    ListSelEndosso.ColumnHeaders.Add , , "Cliente", 8500
''    ListSelEndosso.ColumnHeaders.Add , , "Contratacao", 1500
''    ListSelEndosso.ColumnHeaders.Add , , "Inicio Vigencia", 1500
''    ListSelEndosso.ColumnHeaders.Add , , "Fim Vigencia", 1500
''    ListSelEndosso.ColumnHeaders.Add , , "Dt. Cancelamento", 1500
''    ListSelEndosso.ColumnHeaders.Add , , "Emi Id", 2000
''    ListSelEndosso.ColumnHeaders.Add , , "Arquivo", 2000
''    ListSelEndosso.ColumnHeaders.Add , , "Origem", 0
''    ListSelEndosso.ColumnHeaders.Add , , "Processado", 0
''    ListSelEndosso.ColumnHeaders.Add , , "Tipo de Avalia��o", 2000
''
''    'Carregando form de espera'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''
''    FrmEspera.Show
''
''    DoEvents
''
''    'Obtendo ramo do produto'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''
''    TpRamoProduto = Obter_TpRamo_Produto(PProduto)
''
''    'Selecionando endossos de cancelamento com gera��o de restitui��o pendentes de aceite
''    'na emi_proposta_tb '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''
''    'Os produtos 115 - SEGUR BRASIL ABERTA - VIDA
''    '            150 - SEGUR BRASIL - VIDA
''    '            850 - PERSONALIZADO/COTA��O - VIDA
''    '            160 - PROGER
''    'Apesar de serem do ramo Vida s�o tratados com RE
''
''    If TpRamoProduto = TpRamoVida And PProduto <> 115 And PProduto <> 150 And PProduto <> 850 And PProduto <> 160 Then 'Por Daniel Romualdo - 13/10/2003
''
''        SQL = "SELECT " & vbNewLine
''        SQL = SQL & "   a.num_endosso, " & vbNewLine
''        SQL = SQL & "   dt_recebimento = a.dt_inclusao, " & vbNewLine
''        SQL = SQL & "   a.proposta_bb, " & vbNewLine
''        SQL = SQL & "   c.proposta_id, " & vbNewLine
''        SQL = SQL & "   c.produto_id, " & vbNewLine
''        SQL = SQL & "   d.nome, " & vbNewLine
''        SQL = SQL & "   c.dt_contratacao, " & vbNewLine
''        SQL = SQL & "   dt_inicio_vigencia = CASE " & vbNewLine
''        SQL = SQL & "                           WHEN a.arquivo LIKE 'seg492%' THEN ISNULL(b.dt_inicio_vigencia, c.dt_contratacao) " & vbNewLine
''        SQL = SQL & "                           ELSE b.dt_inicio_vigencia " & vbNewLine
''        SQL = SQL & "                        END, " & vbNewLine
''        SQL = SQL & "   dt_fim_vigencia = CASE " & vbNewLine
''        SQL = SQL & "                        WHEN a.arquivo LIKE 'seg492%' THEN ISNULL(b.dt_fim_vigencia, DATEADD(Month, 12, c.dt_contratacao)) " & vbNewLine
''        SQL = SQL & "                        ELSE b.dt_fim_vigencia " & vbNewLine
''        SQL = SQL & "                     END, " & vbNewLine
''        SQL = SQL & "   dt_cancelamento = ISNULL(a.dt_cancelamento, a.dt_endosso), " & vbNewLine
''        SQL = SQL & "   emi_id = a.emi_proposta_id, " & vbNewLine
''        SQL = SQL & "   a.arquivo, " & vbNewLine
''        SQL = SQL & "   origem = 1, " & vbNewLine
''        SQL = SQL & "   existe_info_baixa = CASE "
''        SQL = SQL & "                           WHEN EXISTS (SELECT proposta_id " & vbNewLine
''        SQL = SQL & "                                        FROM agendamento_cobranca_tb " & vbNewLine
''        SQL = SQL & "                                        WHERE proposta_id = c.proposta_id" & vbNewLine
''        SQL = SQL & "                                              AND situacao = 'a') then 's' " & vbNewLine
''        SQL = SQL & "                           ELSE 'n' "
''        SQL = SQL & "                       END, "
''        SQL = SQL & "   a.tipo_endosso "
''        SQL = SQL & "FROM " & vbNewLine
''        SQL = SQL & "     emi_proposta_tb a " & vbNewLine
''        SQL = SQL & "     Inner Join proposta_adesao_tb b on b.proposta_bb = a.proposta_bb " & vbNewLine
''        SQL = SQL & "     Inner Join proposta_tb c on c.proposta_id = b.proposta_id " & vbNewLine
''        SQL = SQL & "     Inner Join cliente_tb d on d.cliente_id = c.prop_cliente_id " & vbNewLine
''        SQL = SQL & "     Inner Join produto_tb p on p.produto_id = c.produto_id " & vbNewLine
''        SQL = SQL & "WHERE " & vbNewLine
''        SQL = SQL & "     (a.tp_registro = '65' OR (arquivo LIKE 'seg492d%' and tp_registro = '02')) " & vbNewLine
''        SQL = SQL & "     AND a.situacao in ('p', 'n') " & vbNewLine
''        SQL = SQL & "     AND c.situacao IN ('a', 'i') " & vbNewLine
''        SQL = SQL & "     AND right(a.arquivo, 4) <> '.000' " & vbNewLine
''        SQL = SQL & "     AND c.produto_id IN " & vbNewLine
''        SQL = SQL & "                       (SELECT " & vbNewLine
''        SQL = SQL & "                               f.Produto_Id " & vbNewLine
''        SQL = SQL & "                        FROM " & vbNewLine
''        SQL = SQL & "                               arquivo_produto_tb e " & vbNewLine
''        SQL = SQL & "                               INNER JOIN produto_tb f on f.produto_id = e.produto_id " & vbNewLine
''        SQL = SQL & "                        WHERE " & vbNewLine
''        SQL = SQL & "                               e.arquivo = substring(a.arquivo, 1, charindex('.',a.arquivo) - 1)) " & vbNewLine
''        SQL = SQL & "     AND c.produto_id = " & PProduto & vbNewLine
''
''        If TpOperacao = "1" Then
''            'An�lise t�cnica
''            SQL = SQL & "     AND (NOT EXISTS " & vbNewLine
''            SQL = SQL & "                       (SELECT proposta_id " & vbNewLine
''            SQL = SQL & "                        FROM avaliacao_cancelamento_tb " & vbNewLine
''            SQL = SQL & "                        WHERE proposta_id = c.proposta_id) " & vbNewLine
''            SQL = SQL & "          OR 'i' = (SELECT situacao " & vbNewLine
''            SQL = SQL & "                    FROM avaliacao_cancelamento_tb ac " & vbNewLine
''            SQL = SQL & "                    WHERE " & vbNewLine
''            SQL = SQL & "                           ac.proposta_id = c.proposta_id " & vbNewLine
''            SQL = SQL & "                           AND ac.num_avaliacao = (SELECT max(num_avaliacao)" & vbNewLine
''            SQL = SQL & "                                                   FROM avaliacao_cancelamento_tb ac2 " & vbNewLine
''            SQL = SQL & "                                                   WHERE ac2.proposta_id = ac.proposta_id))) " & vbNewLine
''        Else
''            'An�lise gerencial
''            SQL = SQL & "     AND  'p' = (SELECT situacao " & vbNewLine
''            SQL = SQL & "                 FROM avaliacao_cancelamento_tb ac " & vbNewLine
''            SQL = SQL & "                 WHERE " & vbNewLine
''            SQL = SQL & "                       ac.proposta_id = c.proposta_id " & vbNewLine
''            SQL = SQL & "                       AND ac.num_avaliacao = (SELECT max(num_avaliacao)" & vbNewLine
''            SQL = SQL & "                                               FROM avaliacao_cancelamento_tb ac2 " & vbNewLine
''            SQL = SQL & "                                               WHERE ac2.proposta_id = ac.proposta_id)) " & vbNewLine
''
''        End If
''
''        SQL = SQL & "ORDER BY " & vbNewLine
''        SQL = SQL & "     a.num_endosso " & vbNewLine
''
''        Set Rst = rdocn.OpenResultset(SQL)
''
''    End If
''
''    If (TpRamoProduto = TpRamoRE And PProduto <> 424 And PProduto <> 103) Or _
''       (TpRamoProduto = TpRamoVida And (PProduto = 115 Or PProduto = 150 Or PProduto = 850 Or PProduto = 160)) Then 'Por Daniel Romualdo - 13/10/2003
''
''        'Selecionando endossos de cancelamento com gera��o de restitui��o pendentes de aceite
''        'na emi_re_proposta_tb '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''
''        SQL = "SELECT "
''        SQL = SQL & "   a.num_endosso, " & vbNewLine
''        SQL = SQL & "   dt_recebimento = a.dt_inclusao, " & vbNewLine
''        SQL = SQL & "   a.proposta_bb, " & vbNewLine
''        SQL = SQL & "   c.proposta_id, " & vbNewLine
''        SQL = SQL & "   c.produto_id, " & vbNewLine
''        SQL = SQL & "   d.nome, " & vbNewLine
''        SQL = SQL & "   c.dt_contratacao, " & vbNewLine
''        SQL = SQL & "   b.dt_inicio_vigencia, " & vbNewLine
''        SQL = SQL & "   b.dt_fim_vigencia, " & vbNewLine
''        SQL = SQL & "   dt_cancelamento = isnull(a.dt_cancelamento, a.dt_endosso), " & vbNewLine
''        SQL = SQL & "   emi_id = a.emi_re_proposta_id, " & vbNewLine
''        SQL = SQL & "   a.arquivo, " & vbNewLine
''        SQL = SQL & "   origem = 2, " & vbNewLine
''        SQL = SQL & "   existe_info_baixa = CASE "
''        SQL = SQL & "                           WHEN EXISTS (SELECT proposta_id " & vbNewLine
''        SQL = SQL & "                                        FROM agendamento_cobranca_tb " & vbNewLine
''        SQL = SQL & "                                        WHERE proposta_id = c.proposta_id" & vbNewLine
''        SQL = SQL & "                                              AND situacao = 'a') then 's' " & vbNewLine
''        SQL = SQL & "                           ELSE 'n' "
''        SQL = SQL & "                       END, "
''        SQL = SQL & "   a.tipo_endosso "
''        SQL = SQL & "   INTO #tmp "
''        SQL = SQL & "FROM " & vbNewLine
''        SQL = SQL & "     emi_re_proposta_tb a (nolock) " & vbNewLine
''        SQL = SQL & "     INNER JOIN proposta_adesao_tb b (nolock) on b.proposta_bb = a.proposta_bb " & vbNewLine
''        SQL = SQL & "     INNER JOIN proposta_tb c (nolock) on c.proposta_id = b.proposta_id " & vbNewLine
''        SQL = SQL & "     INNER JOIN cliente_tb d (nolock) on  d.cliente_id = c.prop_cliente_id " & vbNewLine
''        SQL = SQL & "     INNER JOIN produto_tb p (nolock) on p.produto_id = c.produto_id " & vbNewLine
''        SQL = SQL & "WHERE " & vbNewLine
''        SQL = SQL & "     a.tp_registro = '03'" & vbNewLine
''
''        'bcarneiro - 16/01/2003 - Inclus�o do tipo_endosso 67 para avalia��o de liquida��o
''        SQL = SQL & "     AND a.tipo_endosso in (63, 65, 67) " & vbNewLine
''
''        'Alterado por Daniel Romualdo
''        'Chamado/Tarefa 146662539
''        SQL = SQL & "     AND a.situacao in ('p', 'n') " & vbNewLine
''
''        SQL = SQL & "     AND right(a.arquivo, 4) <> '.000' " & vbNewLine
''        SQL = SQL & "     AND c.situacao in ('a', 'i') " & vbNewLine
''        SQL = SQL & "     AND c.produto_id = " & PProduto & vbNewLine
''
''        'Jorfilho 11/12/2002 - N�o selecionar o antigo Penhor Rural
''        If PProduto = "155" Then
''
''            'Alterado por Daniel Romualdo - 10/07/2003
''            SQL = SQL & "     AND a.arquivo LIKE 'SEG488%' "
''            'FIM
''            SQL = SQL & "     AND EXISTS (SELECT *"
''            SQL = SQL & "                   FROM emi_re_proposta_tb em (nolock) "
''            SQL = SQL & "                  WHERE c.proposta_id = em.proposta_id"
''            SQL = SQL & "                    AND em.arquivo LIKE 'SEG488%'"
''            SQL = SQL & "                    AND em.tp_registro = '01') "
''
''        End If
''
''        If TpOperacao = 1 Then
''
''            'An�lise t�cnica
''
''            SQL = SQL & "     AND (NOT EXISTS " & vbNewLine
''            SQL = SQL & "                       (SELECT proposta_id " & vbNewLine
''            SQL = SQL & "                        FROM avaliacao_cancelamento_tb (nolock) " & vbNewLine
''            SQL = SQL & "                        WHERE proposta_id = c.proposta_id) " & vbNewLine
''            SQL = SQL & "          OR 'i' = (SELECT situacao " & vbNewLine
''            SQL = SQL & "                    FROM avaliacao_cancelamento_tb ac (nolock) " & vbNewLine
''            SQL = SQL & "                    WHERE " & vbNewLine
''            SQL = SQL & "                           ac.proposta_id = c.proposta_id " & vbNewLine
''            SQL = SQL & "                           AND ac.num_avaliacao = (SELECT max(num_avaliacao)" & vbNewLine
''            SQL = SQL & "                                                   FROM avaliacao_cancelamento_tb ac2 " & vbNewLine
''            SQL = SQL & "                                                   WHERE ac2.proposta_id = ac.proposta_id))) " & vbNewLine
''
''        Else
''
''            'An�lise gerencial
''
''            SQL = SQL & "      AND 'p' = (SELECT situacao " & vbNewLine
''            SQL = SQL & "                 FROM avaliacao_cancelamento_tb ac (nolock) " & vbNewLine
''            SQL = SQL & "                 WHERE " & vbNewLine
''            SQL = SQL & "                           ac.proposta_id = c.proposta_id " & vbNewLine
''            SQL = SQL & "                           AND ac.num_avaliacao = (SELECT max(num_avaliacao)" & vbNewLine
''            SQL = SQL & "                                                   FROM avaliacao_cancelamento_tb ac2 (nolock) " & vbNewLine
''            SQL = SQL & "                                                   WHERE ac2.proposta_id = ac.proposta_id)) " & vbNewLine
''
''        End If
''
''        'SQL = SQL & "ORDER BY " & vbNewLine
''        'SQL = SQL & "     a.num_endosso " & vbNewLine
''
''        Set Rst = rdocn.OpenResultset(SQL)
''
''        'Implementado por Daniel Romualdo
''        'Indicado a solu��o pelo DBA Anderson Lorusso
''        'O select acima estava travando os usuario
''        '28/05/2003
''        Rst.Close
''
''        SQL = " CREATE CLUSTERED INDEX #AK001_tmp ON #tmp (num_endosso,arquivo) "
''        Set Rst = rdocn.OpenResultset(SQL)
''
''        SQL = "     SELECT * FROM #tmp x " & vbNewLine
''        SQL = SQL & " where x.produto_id in " & vbNewLine
''        SQL = SQL & "                       (SELECT " & vbNewLine
''        SQL = SQL & "                               f.Produto_Id " & vbNewLine
''        SQL = SQL & "                       FROM " & vbNewLine
''        SQL = SQL & "                               arquivo_produto_tb e " & vbNewLine
''        SQL = SQL & "                               Inner Join produto_tb f on f.produto_id = e.produto_id " & vbNewLine
''        SQL = SQL & "                       WHERE " & vbNewLine
''        SQL = SQL & "                               e.arquivo = substring(x.arquivo,1,charindex('.',x.arquivo) - 1)) " & vbNewLine
''        SQL = SQL & "ORDER BY " & vbNewLine
''        SQL = SQL & "     x.num_endosso " & vbNewLine
''        Set Rst = rdocn.OpenResultset(SQL)
''
''        temp_exist = True
''
''        If Rst.EOF Then
''
''            Rst.Close
''
''            SQL = " drop table #tmp "
''            Set Rst = rdocn.OpenResultset(SQL)
''
''            temp_exist = False
''
''            SQL = "SELECT "
''            SQL = SQL & "   a.num_endosso, " & vbNewLine
''            SQL = SQL & "   dt_recebimento = a.dt_inclusao, " & vbNewLine
''            SQL = SQL & "   a.proposta_bb, " & vbNewLine
''            SQL = SQL & "   c.proposta_id, " & vbNewLine
''            SQL = SQL & "   c.produto_id, " & vbNewLine
''            SQL = SQL & "   d.nome, " & vbNewLine
''            SQL = SQL & "   c.dt_contratacao, " & vbNewLine
''            SQL = SQL & "   b2.dt_inicio_vigencia, " & vbNewLine
''            SQL = SQL & "   b2.dt_fim_vigencia, " & vbNewLine
''            SQL = SQL & "   dt_cancelamento = isnull(a.dt_cancelamento, a.dt_endosso), " & vbNewLine
''            SQL = SQL & "   emi_id = a.emi_re_proposta_id, " & vbNewLine
''            SQL = SQL & "   a.arquivo, " & vbNewLine
''            SQL = SQL & "   origem = 2, " & vbNewLine
''            SQL = SQL & "   existe_info_baixa = CASE "
''            SQL = SQL & "                           WHEN EXISTS (SELECT proposta_id " & vbNewLine
''            SQL = SQL & "                                        FROM agendamento_cobranca_tb " & vbNewLine
''            SQL = SQL & "                                        WHERE proposta_id = c.proposta_id" & vbNewLine
''            SQL = SQL & "                                              AND situacao = 'a') then 's' " & vbNewLine
''            SQL = SQL & "                           ELSE 'n' "
''            SQL = SQL & "                       END, "
''            SQL = SQL & "   a.tipo_endosso "
''            SQL = SQL & "FROM " & vbNewLine
''            SQL = SQL & "     emi_re_proposta_tb a (nolock) " & vbNewLine
''            SQL = SQL & "     INNER JOIN proposta_fechada_tb b (nolock) on b.proposta_bb = a.proposta_bb " & vbNewLine
''            SQL = SQL & "     INNER JOIN apolice_tb b2 (nolock) on b2.proposta_id = b.proposta_id " & vbNewLine
''            SQL = SQL & "     INNER JOIN proposta_tb c (nolock) on c.proposta_id = b.proposta_id " & vbNewLine
''            SQL = SQL & "     INNER JOIN cliente_tb d (nolock) on d.cliente_id = c.prop_cliente_id " & vbNewLine
''            SQL = SQL & "     INNER JOIN produto_tb p (nolock) on p.produto_id = c.produto_id " & vbNewLine
''            SQL = SQL & "WHERE " & vbNewLine
''            SQL = SQL & "     a.tp_registro = '03'" & vbNewLine
''            SQL = SQL & "     AND a.tipo_endosso in (63) " & vbNewLine
''            'Luciana - 16/10/2003 - Alterado de: a.situacao = 'p' - Para: a.situacao in ('p', 'n')
''            SQL = SQL & "     AND a.situacao in ('p', 'n') " & vbNewLine
''            SQL = SQL & "     AND right(a.arquivo, 4) <> '.000'" & vbNewLine
''            SQL = SQL & "     AND c.situacao in ('a', 'i') " & vbNewLine
''            SQL = SQL & "     AND c.produto_id in " & vbNewLine
''            SQL = SQL & "                       (SELECT " & vbNewLine
''            SQL = SQL & "                               f.Produto_Id " & vbNewLine
''            SQL = SQL & "                       FROM " & vbNewLine
''            SQL = SQL & "                               arquivo_produto_tb e (nolock) " & vbNewLine
''            SQL = SQL & "                               INNER JOIN produto_tb f (nolock) on f.produto_id = e.produto_id " & vbNewLine
''            SQL = SQL & "                       WHERE " & vbNewLine
''            SQL = SQL & "                               e.arquivo = substring(a.arquivo,1,charindex('.',a.arquivo) - 1)) " & vbNewLine
''            SQL = SQL & "     AND c.produto_id = " & PProduto & vbNewLine
''
''            'Jorfilho 11/12/2002 - N�o selecionar o antigo Penhor Rural
''            If PProduto = "155" Then
''              SQL = SQL & "     AND EXISTS (SELECT *"
''              SQL = SQL & "                   FROM emi_re_proposta_tb em (nolock) "
''              SQL = SQL & "                  WHERE c.proposta_id = em.proposta_id"
''              SQL = SQL & "                    AND em.arquivo LIKE 'SEG488%'"
''              SQL = SQL & "                    AND em.tp_registro = '01') "
''            End If
''
''            If TpOperacao = 1 Then
''                'An�lise t�cnica
''                SQL = SQL & "     AND (NOT EXISTS " & vbNewLine
''                SQL = SQL & "                       (SELECT proposta_id " & vbNewLine
''                SQL = SQL & "                        FROM avaliacao_cancelamento_tb (nolock) " & vbNewLine
''                SQL = SQL & "                        WHERE proposta_id = c.proposta_id) " & vbNewLine
''                SQL = SQL & "          OR 'i' = (SELECT situacao " & vbNewLine
''                SQL = SQL & "                    FROM avaliacao_cancelamento_tb ac (nolock) " & vbNewLine
''                SQL = SQL & "                    WHERE " & vbNewLine
''                SQL = SQL & "                           ac.proposta_id = c.proposta_id " & vbNewLine
''                SQL = SQL & "                           AND ac.num_avaliacao = (SELECT max(num_avaliacao)" & vbNewLine
''                SQL = SQL & "                                                   FROM avaliacao_cancelamento_tb ac2 (nolock) " & vbNewLine
''                SQL = SQL & "                                                   WHERE ac2.proposta_id = ac.proposta_id))) " & vbNewLine
''            Else
''                'An�lise gerencial
''                SQL = SQL & "      AND 'p' = (SELECT situacao " & vbNewLine
''                SQL = SQL & "                 FROM avaliacao_cancelamento_tb ac (nolock) " & vbNewLine
''                SQL = SQL & "                 WHERE " & vbNewLine
''                SQL = SQL & "                           ac.proposta_id = c.proposta_id " & vbNewLine
''                SQL = SQL & "                           AND ac.num_avaliacao = (SELECT max(num_avaliacao)" & vbNewLine
''                SQL = SQL & "                                                   FROM avaliacao_cancelamento_tb ac2 (nolock) " & vbNewLine
''                SQL = SQL & "                                                   WHERE ac2.proposta_id = ac.proposta_id)) " & vbNewLine
''            End If
''            SQL = SQL & "ORDER BY " & vbNewLine
''            SQL = SQL & "     a.num_endosso " & vbNewLine
''
''            Set Rst = rdocn.OpenResultset(SQL)
''
''        End If
''
''    End If
''
''    'Jorfilho 09-mai-2003: Tratamento para os produtos
''    '424 - TRANSPORTE INTERNACIONAL AP�LICE AVULSA
''    '103 - TRANSPORTE INTERNACIONAL AP�LICE ABERTA
''    If TpRamoProduto = TpRamoRE And (PProduto = 424 Or PProduto = 103) Then
''
''        If temp_exist Then
''
''            Rst.Close
''
''            SQL = " drop table #tmp "
''            Set Rst = rdocn.OpenResultset(SQL)
''
''        End If
''
''        SQL = ""
''        adSQL SQL, "SELECT a.num_endosso_bb AS num_endosso,"
''        adSQL SQL, "       a.dt_inclusao AS dt_recebimento,"
''        adSQL SQL, "       a.proposta_bb,"
''        adSQL SQL, "       c.proposta_id,"
''        adSQL SQL, "       c.produto_id,"
''        adSQL SQL, "       d.nome,"
''        adSQL SQL, "       c.dt_contratacao,"
''        adSQL SQL, "       b2.dt_inicio_vigencia,"
''        adSQL SQL, "       b2.dt_fim_vigencia,"
''        adSQL SQL, "       ISNULL(a.dt_cancelamento, a.dt_endosso) AS dt_cancelamento,"
''        adSQL SQL, "       a.emi_transp_proposta_id AS emi_id,"
''        adSQL SQL, "       a.arquivo,"
''        adSQL SQL, "       3 AS origem,"
''        adSQL SQL, "       CASE"
''        adSQL SQL, "          WHEN EXISTS (SELECT proposta_id"
''        adSQL SQL, "                         FROM agendamento_cobranca_tb"
''        adSQL SQL, "                        WHERE proposta_id = c.proposta_id"
''        adSQL SQL, "                          AND situacao = 'a') THEN 's'"
''        adSQL SQL, "          ELSE 'n'"
''        adSQL SQL, "       END AS existe_info_baixa,"
''        adSQL SQL, "       a.tp_endosso_seg AS tipo_endosso"
''        adSQL SQL, "  FROM emi_re_transp_proposta_tb a"
''        adSQL SQL, " INNER JOIN proposta_fechada_tb b"
''        adSQL SQL, "    ON b.proposta_bb = a.proposta_bb"
''        adSQL SQL, " INNER JOIN apolice_tb b2"
''        adSQL SQL, "    ON b2.proposta_id = b.proposta_id"
''        adSQL SQL, " INNER JOIN proposta_tb c"
''        adSQL SQL, "    ON c.proposta_id = b.proposta_id"
''        adSQL SQL, " INNER JOIN cliente_tb d"
''        adSQL SQL, "    ON d.cliente_id = c.prop_cliente_id"
''        adSQL SQL, " INNER JOIN produto_tb p"
''        adSQL SQL, "    ON p.produto_id = c.produto_id"
''        adSQL SQL, " WHERE a.tp_registro IN ('02','03')"
''        adSQL SQL, "   AND a.tp_endosso_seg = 63"
''        adSQL SQL, "   AND a.situacao = 'p'"
''        adSQL SQL, "   AND c.situacao IN ('a','i')"
''        adSQL SQL, "   AND c.produto_id IN (SELECT f.produto_id"
''        adSQL SQL, "                          FROM arquivo_produto_tb e"
''        adSQL SQL, "                         INNER JOIN produto_tb f"
''        adSQL SQL, "                            ON f.produto_id = e.produto_id"
''        adSQL SQL, "                         WHERE e.arquivo = SUBSTRING(a.arquivo, 1, CHARINDEX('.', a.arquivo) - 1))"
''        adSQL SQL, "   AND c.produto_id = " & PProduto
''
''        If TpOperacao = 1 Then
''
''            'An�lise t�cnica
''            adSQL SQL, "   AND (NOT EXISTS (SELECT proposta_id"
''            adSQL SQL, "                      FROM avaliacao_cancelamento_tb"
''            adSQL SQL, "                     WHERE proposta_id = c.proposta_id)"
''            adSQL SQL, "    OR  'i' = (SELECT situacao"
''            adSQL SQL, "                 FROM avaliacao_cancelamento_tb ac"
''            adSQL SQL, "                WHERE ac.proposta_id = c.proposta_id"
''            adSQL SQL, "                  AND ac.num_avaliacao = (SELECT MAX(num_avaliacao)"
''            adSQL SQL, "                                            FROM avaliacao_cancelamento_tb ac2"
''            adSQL SQL, "                                           WHERE ac2.proposta_id = ac.proposta_id)))"
''
''        Else
''
''            'An�lise gerencial
''            adSQL SQL, "   AND 'p' = (SELECT situacao"
''            adSQL SQL, "                FROM avaliacao_cancelamento_tb ac"
''            adSQL SQL, "               WHERE ac.proposta_id = c.proposta_id"
''            adSQL SQL, "                 AND ac.num_avaliacao = (SELECT MAX(num_avaliacao)"
''            adSQL SQL, "                                           FROM avaliacao_cancelamento_tb ac2"
''            adSQL SQL, "                                          WHERE ac2.proposta_id = ac.proposta_id))"
''
''        End If
''
''        adSQL SQL, "ORDER BY a.num_endosso_bb"
''
''        Set Rst = rdocn.OpenResultset(SQL)
''
''    End If
''
''    'Limpando grid''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''
''    ListSelEndosso.ListItems.Clear
''
''    Do While Not Rst.EOF
''
''        'Preenchendo Grid'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''
''        If UCase(Rst!existe_info_baixa) = "S" Then
''            Set Item = ListSelEndosso.ListItems.Add(, , "", , 1)
''        Else
''            Set Item = ListSelEndosso.ListItems.Add(, , "", , 2)
''        End If
''
''        Item.SubItems(1) = IIf(IsNull(Rst!Num_endosso), "", Rst!Num_endosso)
''        Item.SubItems(2) = Format(Rst!Dt_Recebimento, "dd/mm/yyyy")
''        Item.SubItems(3) = Rst!Proposta_BB
''        Item.SubItems(4) = Rst!Proposta_Id
''        Item.SubItems(5) = Rst!Produto_Id
''        Item.SubItems(6) = Rst!Nome
''        Item.SubItems(7) = Format(Rst!Dt_Contratacao, "dd/mm/yyyy")
''        Item.SubItems(8) = Format(Rst!Dt_Inicio_Vigencia, "dd/mm/yyyy")
''        Item.SubItems(9) = Format(Rst!dt_fim_vigencia, "dd/mm/yyyy")
''        Item.SubItems(10) = Format(Rst!dt_cancelamento, "dd/mm/yyyy")
''        Item.SubItems(11) = Rst!Emi_Id
''        Item.SubItems(12) = Rst!Arquivo
''        Item.SubItems(13) = Rst!Origem
''        Item.SubItems(14) = "N"
''
''        'bcarneiro - 16/01/2003 - Inclus�o de Avalia��o de Liquida��o
''        If Rst!Tipo_Endosso = 67 Then
''            Item.SubItems(15) = "LIQUIDA��O"
''        Else
''            Item.SubItems(15) = "CANCELAMENTO"
''        End If
''
''        'Definindo cor da linha
''        If UCase(Rst!existe_info_baixa) = "N" Then
''            Call SetForecolorforListItem(Item, vbRed)
''        End If
''
''        'Posicionando-se no pr�ximo registro'''''''''''''''''''''''''''''''''''''''''''''''''''
''        Rst.MoveNext
''
''    Loop
''
''    Rst.Close
''
''    If temp_exist Then
''        SQL = " drop table #tmp "
''        Set Rst = rdocn.OpenResultset(SQL)
''    End If
''
''    'Atualizando interface''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''
''    ListSelEndosso.Visible = True
''
''    StatusBar1.SimpleText = "Total de registros encontrados: " & ListSelEndosso.ListItems.Count
''
''    'Descarregando form de espera'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''
''    Call Unload(FrmEspera)
''
''End Sub
''
''Sub Preparar_Informacoes_Teste_Com_SP(ByVal PProduto)
''
''Dim Rst As rdoResultset
''Dim SQL As String
''' Dim Linha As String
''Dim Item As ListItem
''
''Dim TpRamoProduto As String
''
'''Atualizando interface''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''
''ListSelEndosso.Visible = False
''
'''Configurando ListView''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''
''ListSelEndosso.View = lvwReport
''ListSelEndosso.FullRowSelect = True
''ListSelEndosso.HideSelection = False
''ListSelEndosso.LabelEdit = lvwManual
''ListSelEndosso.HotTracking = True
''
'''Configurando header do ListView''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''
''ListSelEndosso.ColumnHeaders.Clear
''
''If TpOperacao = 1 Then
''    ListSelEndosso.ColumnHeaders.Add , , "Selecionar", 0
''Else
''    ListSelEndosso.ColumnHeaders.Add , , "Selecionar", 600
''End If
''ListSelEndosso.ColumnHeaders.Add , , "Nr. Endosso", 1200
''ListSelEndosso.ColumnHeaders.Add , , "Recebimento", 1500
''ListSelEndosso.ColumnHeaders.Add , , "Proposta BB", 1200
''ListSelEndosso.ColumnHeaders.Add , , "Proposta Id", 1200
''ListSelEndosso.ColumnHeaders.Add , , "Produto", 800
''ListSelEndosso.ColumnHeaders.Add , , "Cliente", 8500
''ListSelEndosso.ColumnHeaders.Add , , "Contratacao", 1500
''ListSelEndosso.ColumnHeaders.Add , , "Inicio Vigencia", 1500
''ListSelEndosso.ColumnHeaders.Add , , "Fim Vigencia", 1500
''ListSelEndosso.ColumnHeaders.Add , , "Dt. Cancelamento", 1500
''ListSelEndosso.ColumnHeaders.Add , , "Emi Id", 0
''ListSelEndosso.ColumnHeaders.Add , , "Arquivo", 2000
''ListSelEndosso.ColumnHeaders.Add , , "Origem", 0
''ListSelEndosso.ColumnHeaders.Add , , "Processado", 0
''
'''Carregando form de espera'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''
''FrmEspera.Show
''
''DoEvents
''
'''Obtendo e criticando tipo de ramo do produto''''''''''''''''''''''''''''''''''''''''''''''
''
''TpRamoProduto = Obter_TpRamo_Produto(PProduto)
''
''If Trim(TpRamoProduto) = "" Then
''    MsgBox "O Produto selecionado n�o est� associado a nenhum ramo. A pesquisa n�o pode ser realizada."
''    Exit Sub
''End If
''
'''Selecionando endossos de cancelamento com gera��o de restitui��o pendentes de aceite
'''na emi_proposta_tb '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''
''If TpOperacao = 1 Then
''
''    SQL = "Exec avaliacao_tecnica_cancelamento_sps "
''    SQL = SQL & PProduto
''    SQL = SQL & "," & TpRamoProduto
''
''Else
''
''    SQL = "Exec avaliacao_gerencial_cancelamento_sps "
''    SQL = SQL & PProduto
''    SQL = SQL & "," & TpRamoProduto
''
''End If
''
''Set Rst = rdocn.OpenResultset(SQL)
''
'''Limpando grid''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''
''ListSelEndosso.ListItems.Clear
''
''Do While Not Rst.EOF
''
''    'Preenchendo Grid'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''
''    Set Item = ListSelEndosso.ListItems.Add(, , "")
''
''    Item.SubItems(1) = Rst!Num_endosso
''    Item.SubItems(2) = Format(Rst!Dt_Recebimento, "dd/mm/yyyy")
''    Item.SubItems(3) = Rst!Proposta_BB
''    Item.SubItems(4) = Rst!Proposta_Id
''    Item.SubItems(5) = Rst!Produto_Id
''    Item.SubItems(6) = Rst!Nome
''    Item.SubItems(7) = Format(Rst!Dt_Contratacao, "dd/mm/yyyy")
''    Item.SubItems(8) = Format(Rst!Dt_Inicio_Vigencia, "dd/mm/yyyy")
''    Item.SubItems(9) = Format(Rst!dt_fim_vigencia, "dd/mm/yyyy")
''    Item.SubItems(10) = Format(Rst!dt_cancelamento, "dd/mm/yyyy")
''    Item.SubItems(11) = Rst!Emi_Id
''    Item.SubItems(12) = Rst!Arquivo
''    Item.SubItems(13) = Rst!Origem
''    Item.SubItems(14) = "N"
''
''    'Posicionando-se no pr�ximo registro'''''''''''''''''''''''''''''''''''''''''''''''''''
''
''    Rst.MoveNext
''
''Loop
''
''Rst.Close
''
'''Atualizando interface''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''
''ListSelEndosso.Visible = True
''
''StatusBar1.SimpleText = "Total de registros encontrados: " & ListSelEndosso.ListItems.Count
''
'''Descarregando form de espera'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''
''Unload FrmEspera
''
''End Sub



Private Sub ListSelEndosso_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)

'Criado por:    Cinthia Monteiro
'Data:          26/04/2011
'Demanda:       9064130
'Parametros:    N = N�meros, D = Datas, T = Textos

If ColumnHeader.Tag = "N" Then '
    SortListView ListSelEndosso, ColumnHeader.Index, "N", OrdAsc
ElseIf ColumnHeader.Tag = "T" Then
    SortListView ListSelEndosso, ColumnHeader.Index, "T", OrdAsc
Else
    SortListView ListSelEndosso, ColumnHeader.Index, "D", OrdAsc
End If

OrdAsc = Not OrdAsc

End Sub

Private Sub ListSelEndosso_DblClick()


    vgsituacao = ""
    
    ' bcarneiro - 02/12/2003 - Projeto Restitui��o por Iniciativa da Seguradora
    ''If optCancelamento.Value Then
    ''    Call Exibir_Detalhes(ListSelEndosso.SelectedItem)
    ''Else
        Call ExibirDetalhesRestituicao(ListSelEndosso.SelectedItem, False)
    ''End If

'Dim OPropostaModular As Object
'Dim rs As Recordset
'
'Dim lPropostaModular As Long
'
'vgsituacao = ""
'Set OPropostaModular = CreateObject("SEGL0026.cls00124")
'lPropostaModular = OPropostaModular.ObterPropostaModular(gsSIGLASISTEMA, _
'                                                         App.Title, _
'                                                         App.FileDescription, _
'                                                         glAmbiente_id, _
'                                                         lPropostaId)
'
'Set OPropostaModular = Nothing
''rsilva
'If lPropostaModular = 0 Then
'    bModular = False
'    Call ExibirDetalhesRestituicao(ListSelEndosso.SelectedItem)
'Else
'    bModular = True
'    Call ExibirDetalhesRestituicaoModular(ListSelEndosso.SelectedItem)
'End If



End Sub

Private Sub ListSelEndosso_ItemCheck(ByVal Item As MSComctlLib.ListItem)

Dim proposta_id As String
Dim lAvaliacao As Long

    proposta_id = Item.SubItems(2)
    lAvaliacao = Item.SubItems(4)

    If VerificarExistenciaAvaliacaoRestituicaoPendente(proposta_id, lAvaliacao, TpOperacao) Then
        Call MsgBox("Endosso j� avaliado")
        Item.Checked = False
    End If

End Sub

''Private Sub Zera_Valores_Restituicao()
''
''    With FrmAvaliacao
''        .Mask2ValPremioTarifa.Text = "0,00"
''        .LblCustoCertifApoliceRest = "0,00"
''        .LblValAdicFracionamento = "0,00"
''        .LblValDesconto = "0,00"
''        .LblValIOF = "0,00"
''        .LblValRestituicao = "0,00"
''        .LblTotValCorretagem = "0,00"
''        .LblValProLabore = "0,00"
''    End With
''
''End Sub

''Private Sub optCancelamento_Click()
''
''    If optCancelamento.Value And CmbProduto.ListIndex > -1 Then
''        Call CmbProduto_Click
''    End If
''
''End Sub
''
''Private Sub optRestituicao_Click()
''
''    If optRestituicao.Value And CmbProduto.ListIndex > -1 Then
''        Call CmbProduto_Click
''    End If
''
''End Sub

Sub PrepararInformacoesRestituicao(ByVal iProdutoId As Integer)

' bcarneiro - 01/12/2003 - Projeto Restitui��o por Iniciativa da Seguradora

Dim rs As Object
Dim Item As ListItem
Dim sSituacao As String
Dim oFinanceiro As Object

    'Atualizando interface''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

    ListSelEndosso.Visible = False

    'Configurando ListView''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

    If TpOperacao = 1 Then
        'Diego De Oliveira
        ListSelEndosso.Checkboxes = True
        'Fim
        sSituacao = SITUACAO_PENDENTE_AVALIACAO_TECNICA
    Else
        ListSelEndosso.Checkboxes = True
        sSituacao = SITUACAO_PENDENTE_AVALIACAO_GERENCIAL
    End If
    
    ListSelEndosso.View = lvwReport
    ListSelEndosso.FullRowSelect = True
    ListSelEndosso.LabelEdit = lvwManual
    ListSelEndosso.HotTracking = True

    'Configurando header do ListView''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

    ListSelEndosso.ColumnHeaders.Clear
    'Diego de Oliveira
    ListSelEndosso.ColumnHeaders.Add , , "Selecionar", 600
    'fim
    ListSelEndosso.ColumnHeaders.Add , , "Proposta BB", 1200
    ListSelEndosso.ColumnHeaders.Add , , "Proposta Id", 1200
    '----------------------------------------------------------
    ' jconceicao - fev 2006 - inclusao atributo val_restituicao
    ListSelEndosso.ColumnHeaders.Add , , "Val.Restitui��o", 1300
    '----------------------------------------------------------
    ListSelEndosso.ColumnHeaders.Add , , "Nr. Avalia��o", 1500
    ListSelEndosso.ColumnHeaders.Add , , "Pedido da Avalia��o", 1500
    ListSelEndosso.ColumnHeaders.Add , , "Produto", 800
    ListSelEndosso.ColumnHeaders.Add , , "Cliente", 8500
    ListSelEndosso.ColumnHeaders.Add , , "Contratacao", 1500
    ListSelEndosso.ColumnHeaders.Add , , "Inicio Vigencia", 1500
    ListSelEndosso.ColumnHeaders.Add , , "Fim Vigencia", 1500
    ListSelEndosso.ColumnHeaders.Add , , "Origem", 0
    ListSelEndosso.ColumnHeaders.Add , , "Processado", 0
    ListSelEndosso.ColumnHeaders.Add , , "Tipo de Avalia��o", 2000
    'ListSelEndosso.ColumnHeaders.Add , , "Perc Corretagem", 0
    'Rmarins - 20/10/06
    ListSelEndosso.ColumnHeaders.Add , , "Val.Subven��o Federal", 1300
    ListSelEndosso.ColumnHeaders.Add , , "Val.Subven��o Estadual", 1300

    'Demanda 846659 - Inclus�o de campos
    'Alterado em 11/05/2009 - Joana Agapito - GPTI
    'Inser��o dos campos: valor do pr�mio pago e valor do pr�mio retido - Apenas para avalia��o gerencial
   'Diego De Oliveira
    'If TpOperacao <> 1 Then
    
        ListSelEndosso.ColumnHeaders.Add , , "Val.Pr�mio Pago", 1300
        ListSelEndosso.ColumnHeaders.Add , , "Val.Pr�mio Retido", 1300
        
        
   
    'End If
     'Fim
     
     'Renato Vasconcelos
        ListSelEndosso.ColumnHeaders.Add , , "Endosso", 0
        ListSelEndosso.ColumnHeaders.Add , , "Val.Comiss�o", 0
        ListSelEndosso.ColumnHeaders.Add , , "Val.Remunera��o", 0
        ListSelEndosso.ColumnHeaders.Add , , "IS", 0
     'Fim
        
    'Definindo as tags para a ordena��o da listview
    ListSelEndosso.ColumnHeaders(2).Tag = "N"
    ListSelEndosso.ColumnHeaders(3).Tag = "N"
    ListSelEndosso.ColumnHeaders(4).Tag = "N"
    ListSelEndosso.ColumnHeaders(5).Tag = "N"
    ListSelEndosso.ColumnHeaders(6).Tag = "D"
    ListSelEndosso.ColumnHeaders(7).Tag = "N"
    ListSelEndosso.ColumnHeaders(8).Tag = "T"
    ListSelEndosso.ColumnHeaders(9).Tag = "D"
    ListSelEndosso.ColumnHeaders(10).Tag = "D"
    ListSelEndosso.ColumnHeaders(11).Tag = "D"
    ListSelEndosso.ColumnHeaders(14).Tag = "T"
    ListSelEndosso.ColumnHeaders(15).Tag = "N"
    ListSelEndosso.ColumnHeaders(16).Tag = "N"
    'Renato Vasconcelos
    'Inicio
    'If TpOperacao <> 1 Then
        ListSelEndosso.ColumnHeaders(17).Tag = "N"
        ListSelEndosso.ColumnHeaders(18).Tag = "N"
    'End If
    'Fim
    'Carregando form de espera'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    FrmEspera.Caption = "Aprova��o de Restitui��o"
    FrmEspera.Show

    DoEvents
    
    'Limpando grid''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

    ListSelEndosso.ListItems.Clear

    Set oFinanceiro = CreateObject("SEGL0026.cls00125")

    Set rs = oFinanceiro.ConsultarAvaliacaoRestituicao(gsSIGLASISTEMA, _
                                                       App.Title, _
                                                       App.FileDescription, _
                                                       glAmbiente_id, _
                                                       sSituacao, _
                                                       iProdutoId)
        'Diego de Oliveira

    Dim cont As Integer
    Dim txt As Integer
    If txtQtdeLista.Text = "" Then
    txt = 0
    Else
    txt = txtQtdeLista.Text
    End If
    cont = 0
    
    Do While Not rs.EOF
        If txt = cont And txt >= 1 Then
        Exit Do
        End If
        'Fim
        'Preenchendo Grid'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    
        If UCase(rs!existe_info_baixa) = "S" Then
            Set Item = ListSelEndosso.ListItems.Add(, , "", , 1)
        Else
            Set Item = ListSelEndosso.ListItems.Add(, , "", , 2)
        End If
        
        Item.SubItems(1) = IIf(IsNull(rs!proposta_bb), "", rs!proposta_bb)
        Item.SubItems(2) = rs!proposta_id
        '----------------------------------------------------------
        ' jconceicao - fev 2006 - inclusao atributo val_restituicao
        
        'Renato Vasconcelos
        Item.SubItems(3) = Format(IIf(IsNull(rs!Val_Restituicao), rs!val_restituicao_estimado, rs!Val_Restituicao), "#,##0.00")
       
        'Fim
        '
        Item.SubItems(4) = rs!Num_avaliacao
        Item.SubItems(5) = Format(rs!Dt_Pedido_Avaliacao, "dd/mm/yyyy")
        Item.SubItems(6) = rs!Produto_id
        Item.SubItems(7) = rs!Nome
        Item.SubItems(8) = Format(rs!Dt_Contratacao, "dd/mm/yyyy")
        Item.SubItems(9) = Format(rs!Dt_Inicio_Vigencia, "dd/mm/yyyy")
        Item.SubItems(10) = Format(rs!Dt_Fim_Vigencia, "dd/mm/yyyy")
        Item.SubItems(11) = rs!Origem
        Item.SubItems(12) = "N"
        
        If Trim(rs!Nome_Tp_Endosso) = "" Then
            Item.SubItems(13) = "Restitui��o por iniciativa da seguradora"
        Else
            Item.SubItems(13) = Trim(rs!Nome_Tp_Endosso)
        End If
        
        Item.SubItems(14) = Format(rs!val_subvencao_federal_estimado, "#,##0.00")
        Item.SubItems(15) = Format(rs!val_subvencao_estadual_estimado, "#,##0.00")

        '-------------------------------------------------------------------
        'Demanda 846659 - Inclus�o de campos
        'Alterado em 11/05/2009 - Joana Agapito - GPTI
        'Inser��o dos campos: valor do pr�mio pago e valor do pr�mio retido- Apenas para avalia��o gerencial
        Dim vlPremioPago As Double, vlPremioRetido As Double
        'If TpOperacao <> 1 Then
            
            'Recupera��o do pr�mio pago
            vlPremioPago = oFinanceiro.recuperar_PremioPago_PremioRetido(gsSIGLASISTEMA, App.Title, _
                                                           App.FileDescription, glAmbiente_id, _
                                                           CDbl("0" & rs!val_cambio), _
                                                           CLng("0" & rs!proposta_id), _
                                                           0, _
                                                           0, _
                                                           "P")
            
                                    
            Item.SubItems(16) = Format(vlPremioPago, "#,##0.00")
            
            'Recupera��o do pr�mio retido
            vlPremioRetido = oFinanceiro.recuperar_PremioPago_PremioRetido(gsSIGLASISTEMA, App.Title, _
                                                           App.FileDescription, glAmbiente_id, _
                                                           CDbl("0" & rs!val_cambio), _
                                                           CLng("0" & rs!proposta_id), _
                                                           vlPremioPago, _
                                                           IIf(IsNull(rs!Val_Restituicao), rs!val_restituicao_estimado, rs!Val_Restituicao), _
                                                           "R")
                                                           
                                                           
            
            Item.SubItems(17) = Format(vlPremioRetido, "#,##0.00")
            
        
        'Diego de Oliveira
        'Item.SubItems(18) = Format(rs!dt_emissao, "dd/mm/yyyy")
        'Fim
        'Renato Vasconcelos
'            cValRestituicao = cValPremioTarifa + cValCustoApoliceCertif + cValAdicFracionamento - cValDesconto + cValIOF
'            cValPercRemuneracao = FrmAvaliacao.ObterPercRemuneracao(rs!Produto_id, rs!ramo_id, Data_Sistema)
'            cValRemuneracao = cValRestituicao * (cValPercRemuneracao / 100)
'            If TpOperacao = 1 Then
'                cValPremioTarifa = IIf(IsNull(rs!val_restituicao_estimado), "0", rs!val_restituicao_estimado)
'                cValCustoApoliceCertf = IIf(IsNull(rs!custo_apolice_estimado), "0", custo_apolice_estimado)
'            End If
'
            Item.SubItems(18) = Format(rs!dt_emissao, "dd/mm/yyyy")
            Item.SubItems(19) = Format(IIf(IsNull(rs!Val_Comissao), rs!Val_Comissao_estimado, rs!Val_Comissao), "#,##0.00")
            Item.SubItems(20) = Format(IIf(rs!Val_Remuneracao = 0, rs!Val_Remuneracao_estimado, rs!Val_Remuneracao), "#,##0.00")
            Item.SubItems(21) = Format(IIf(IsNull(rs!Val_IS), "0", rs!Val_IS), "#,##0.00")
            
            
        'End If
        'Fim - Demanda 846659 - Inclus�o de campos
        '-------------------------------------------------------------------
        
        
        'Definindo cor da linha
        If UCase(rs!existe_info_baixa) = "N" Then
            Call SetForecolorforListItem(Item, vbRed)
        End If

        'Posicionando-se no pr�ximo registro'''''''''''''''''''''''''''''''''''''''''''''''''''
        rs.MoveNext
    cont = cont + 1
    Loop

    rs.Close
    Set oFinanceiro = Nothing
    
    'Atualizando interface''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

    ListSelEndosso.Visible = True

    StatusBar1.SimpleText = "Total de registros encontrados: " & ListSelEndosso.ListItems.Count

    'Descarregando form de espera'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

    Call Unload(FrmEspera)


End Sub

Sub ExibirDetalhesRestituicao(ByVal Item As ListItem, alteraEmLote As Boolean)

' bcarneiro - 02/12/2003 - Projeto Restitui��o por Iniciativa da Seguradora

'Declara��es
Dim lPropostaBB As Long
Dim lAvaliacao As Long
Dim sSituacao As String
Dim sDtPedidoAvaliacao As String
Dim sDtContratacao As String
Dim sDtInicioVigencia As String
Dim sDtFimVigencia As String
Dim lPropostaId As Long
Dim sRamoId As String

Dim cValIS As Currency
Dim cValPremio As Currency
Dim cValTotIOFPago As Currency
Dim cValTotPremioPago As Currency
Dim cValTotJurosPago As Currency
Dim cValTotAdicFracionamentoPago As Currency
Dim cValTotDescontoPago As Currency
Dim cValCustoApoliceCertif As Currency

Dim iPrazoRestituicaoIntegral As Integer
Dim sTpCalcRestituicao As String

Dim sPossuiSubvencao As String  'Rmarins - inclu�dos em 20/10/2006
Dim cValSubvencaoFederal As Currency
Dim cValSubvencaoEstadual As Currency
Dim cValSubvencaoFederalEst As Currency
Dim cValSubvencaoEstadualEst As Currency

Dim cValCustoApoliceCertifEst As Currency
Dim cValAdicFracionamentoEst As Currency
Dim cValDescontoEst As Currency
Dim cValIOFEst As Currency
Dim cValPremioTarifaEst As Currency
Dim cValRestituicaoEst As Currency
Dim cValTotCorretagemEst As Currency
Dim cValProLaboreEst As Currency

Dim cValCustoApoliceCertifRest As Currency
Dim cValAdicFracionamentoRest As Currency
Dim cValDescontoRest As Currency
Dim cValIOFRest As Currency
Dim cValPremioTarifaRest As Currency
Dim cValRestituicao As Currency
Dim cValTotCorretagemRest As Currency
Dim cValProLaboreRest As Currency
Dim cPercIR As Currency

'mathayde 27/03/2009
Dim cValRemuneracao As Currency
Dim cValRemuneracaoEst As Currency

'rsilva 23/07/2009
Dim OPropostaModular As Object
Dim rs As Recordset
Dim lPropostaModular As Long


Dim lEndossoId As Long

On Error GoTo TrataErro



    'Atualizando interface'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    MousePointer = vbHourglass

    'Obtendo Vari�veis P�blicas''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    Produto_id = Item.SubItems(6)
    iOrigem = Item.SubItems(11)
    
    'Lendo Vari�veis '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    lPropostaBB = IIf(Item.SubItems(1) = "", 0, Item.SubItems(1))
    lPropostaId = Item.SubItems(2)
    lAvaliacao = Item.SubItems(4)
    'Verificando se h� propostaModular
    If Produto_id = 1168 Then
        lAvaliacaoModular = lAvaliacao
    End If

    ' C_ANEVES - 26/08/2013 - Demanda: 17860335 - Inicio
    Set OPropostaModular = CreateObject("SEGL0026.cls00124")
    'Set OPropostaModular = CreateObject("SEGL0355.cls00626")
    ' C_ANEVES - 26/08/2013 - Demanda: 17860335 - Fim
    
    lPropostaModular = OPropostaModular.ObterPropostaModular(gsSIGLASISTEMA, _
                                                             App.Title, _
                                                             App.FileDescription, _
                                                             glAmbiente_id, _
                                                             lPropostaId, _
                                                             IIf(TpOperacao = 1, SITUACAO_PENDENTE_AVALIACAO_TECNICA, SITUACAO_PENDENTE_AVALIACAO_GERENCIAL))
                                                             

    
    If lPropostaModular = 0 And Produto_id <> 1168 Then
        bModular = False
        propostaModular = 0
    Else
        Call DadosModular(lPropostaId, lPropostaModular)
'        bModular = True
'
'        If Produto_id = 1168 Then
'            lPropostaModular = lPropostaId
'        End If
'
'        oDadosModular.proposta_id = lPropostaModular
'        ''executar todas as fun��es para proposta modular
'
'        Set oFinanceiro = CreateObject("SEGL0026.cls00125")
'
'        Set rs = oFinanceiro.ConsultarAvaliacaoRestituicao(gsSIGLASISTEMA, _
'                                                           App.Title, _
'                                                           App.FileDescription, _
'                                                           glAmbiente_id, _
'                                                           IIf(TpOperacao = 1, SITUACAO_PENDENTE_AVALIACAO_TECNICA, SITUACAO_PENDENTE_AVALIACAO_GERENCIAL), _
'                                                           IIf(Produto_id = 1168, 1168, 0), lPropostaModular, IIf(Produto_id = 1168, lAvaliacao, 0))
'
'
'        If Not rs.EOF Then
'            oDadosModular.proposta_bb = rs("proposta_bb")
'            oDadosModular.Num_avaliacao = rs("num_avaliacao")
'            oDadosModular.Dt_Pedido_Avaliacao = Format(rs("dt_pedido_avaliacao"), "dd/mm/yyyy")
'            oDadosModular.Produto_id = rs("produto_id")
'            oDadosModular.Dt_Contratacao = rs("dt_contratacao")
'            oDadosModular.Dt_Inicio_Vigencia = rs("dt_inicio_vigencia")
'            oDadosModular.Dt_Fim_Vigencia = rs("dt_fim_vigencia")
'            oDadosModular.Origem = rs("origem")
'            oDadosModular.Nome_Tp_Endosso = rs("nome_tp_endosso")
'
'            If Produto_id = 1168 Then
'                If lAvaliacao > rs("num_avaliacao") Then
'                    oDadosModular.iTpCobertura = 526
'                Else
'                    oDadosModular.iTpCobertura = 527
'                End If
'            End If
'
'            Call Obter_Dados_Adicionais_Proposta_Modular(lPropostaModular, _
'                                                         sRamoId, _
'                                                         cValCustoApoliceCertif)
'            Call Obter_Premio_IS(lPropostaModular, rs("produto_id"), _
'                                                   oDadosModular.Val_IS, _
'                                                   oDadosModular.Val_Premio)
'
'            Call Carregar_Colecao_Cobrancas(lPropostaModular)
'
'            'rsilva - 02/09/2009
'            'Nova regra para o calculo do premio a ser restituido
'            Call ConsultarSubRamoModular(lPropostaModular)
'
'            Call ConsultarPercIOFModular(oDadosModular.Dt_Inicio_Vigencia, _
'                                         oDadosModular.SubRamoId)
'
'            Call Obter_Totais_Pagos(oDadosModular.Val_Premio_Pago, _
'                                    oDadosModular.Val_Juros, _
'                                    oDadosModular.Val_Adic_Fracionamento, _
'                                    oDadosModular.Val_Desconto, _
'                                    oDadosModular.Val_IOF_Restituicao, True)
'
'            Call ObterValoresAvaliacaoRestituicaoModular(lPropostaModular, _
'                                                         oDadosModular.Num_avaliacao)
'
'            Call Obtem_Perc_IRModular(oDadosModular.ramo_id, _
'                                        oDadosModular.Dt_Inicio_Vigencia, _
'                                        0)
'
'            Call CarregarRateioCorretagemRestituicao(lPropostaModular, _
'                                                     lAvaliacao, _
'                                                     oDadosModular.ValRestituicao, _
'                                                     oDadosModular.ValIOFRest, _
'                                                     oDadosModular.ValCustoApoliceCertifRest, _
'                                                     oDadosModular.PercIR)
'
'
'            oDadosModular.PercCorretagemRest = ObterPercentualTotalCorretagem(lPropostaModular)
'
'       End If
    End If
    
    

    sDtPedidoAvaliacao = Item.SubItems(5)
    sDtContratacao = Item.SubItems(8)
    sDtInicioVigencia = Item.SubItems(9)
    sDtFimVigencia = Item.SubItems(10)
    Origem = Item.SubItems(11)
    
    FrmAvaliacao.lblTipoAvaliacao = "Avalia��o de Restitui��o"
    ''FrmAvaliacao.gsTipoProcesso = "R"
    FrmAvaliacao.lblDtLiq.Visible = False

    Call Obter_Dados_Adicionais_Proposta(lPropostaId, sRamoId, cValCustoApoliceCertif)

    bDtCancelamentoValida = True

    'Verificando se a proposta est� sendo utilizada por outro usu�rio'''''''''''''''''''''''''''
    If Verificar_Uso_Proposta(lPropostaId) Then
        MsgBox "A proposta do endosso est� sendo utilizada por outro usu�rio"
        MousePointer = vbDefault
        Exit Sub
    End If

    'Verificando se a proposta j� foi processada'''''''''''''''''''''''''''''''''''''''''''''''
    If VerificarExistenciaAvaliacaoRestituicaoPendente(lPropostaId, lAvaliacao, TpOperacao) Then
        MsgBox "Endosso j� avaliado."
        FrmSelEndosso.Marcar_Linha_Processada
        MousePointer = vbDefault
        Exit Sub
    End If

    'Bloqueando proposta'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    'rsilva retirar antes de liberar
    'Renato Vasconcelos
    If Not alteraEmLote Then
        Call Atualizar_Bloqueio_Proposta(lPropostaId, "s")
    End If
    'Lendo dados complementares''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    Call Obter_Premio_IS(lPropostaId, Produto_id, cValIS, cValPremio)
    Call Carregar_Colecao_Cobrancas(lPropostaId)
    Call Obter_Totais_Pagos(cValTotPremioPago, cValTotJurosPago, cValTotAdicFracionamentoPago, cValTotDescontoPago, cValTotIOFPago)
    Call Obter_Dados_Produto(Produto_id, iPrazoRestituicaoIntegral, sTpCalcRestituicao, sPossuiSubvencao)  'Rmarins - 20/10/06
    
    
    'rsilva
    'Call ConsultarSubRamo(lPropostaId)
    
    
    gcValTotPremioPago = cValTotPremioPago
    gcValTotJurosPago = cValTotJurosPago
    gcValTotAdicFracionamentoPago = cValTotAdicFracionamentoPago
    gcValTotDescontoPago = cValTotDescontoPago
    gcValTotIOFPago = cValTotIOFPago
    gcValCustoApoliceCertif = cValCustoApoliceCertif
    gcValTotPremioTarifaPago = cValTotPremioPago - cValTotAdicFracionamentoPago - cValTotIOFPago - cValCustoApoliceCertif + cValTotDescontoPago
    gsPossuiSubvencao = UCase(sPossuiSubvencao) 'Rmarins - 20/10/06
    
    'Obtendo dados percentual de pro-labore''''''''''''''''''''''''''''''''''''''''''''''''''''
    Call Obtem_Perc_IR(sRamoId, Data_Sistema, cPercIR)

    'Obtendo valores da opera��o'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    If TpOperacao = 1 Then
        sSituacao = SITUACAO_PENDENTE_AVALIACAO_TECNICA
    Else
        sSituacao = SITUACAO_PENDENTE_AVALIACAO_GERENCIAL
    End If
    'mathayde 27/03/2009 - Carrega os valores de remunera��o
    Call ObterValoresAvaliacaoRestituicao(lPropostaId, _
                                          lAvaliacao, _
                                          sSituacao, _
                                          cValRestituicao, _
                                          cValIOFRest, _
                                          cValCustoApoliceCertifRest, _
                                          cValAdicFracionamentoRest, _
                                          cValDescontoRest, _
                                          cValPremioTarifaRest, _
                                          cValTotCorretagemRest, _
                                          cValProLaboreRest, _
                                          cValCustoApoliceCertifEst, _
                                          cValAdicFracionamentoEst, _
                                          cValDescontoEst, _
                                          cValIOFEst, _
                                          cValPremioTarifaEst, _
                                          cValRestituicaoEst, _
                                          cValTotCorretagemEst, _
                                          cValProLaboreEst, _
                                          lEndossoId, _
                                          0, "", _
                                          cValSubvencaoFederal, cValSubvencaoEstadual, _
                                          cValSubvencaoFederalEst, cValSubvencaoEstadualEst, _
                                          cValRemuneracao, cValRemuneracaoEst)
                                          'Mathayde 30/03/2009 - obtem valores de cValRemuneracao e cValRemuneracaoEst

    'Obtendo rateio de corretagem ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    Call CarregarRateioCorretagemRestituicao(lPropostaId, _
                                             lAvaliacao, _
                                             cValRestituicao, _
                                             cValIOFRest, _
                                             cValCustoApoliceCertifRest, _
                                             cPercIR)
                                             
    'Demanda 400422 - emaior 02/12/2008 - Relat�rio de Restitui��o de Comiss�o de Cosseguro
    Call CarregarCongeneres(lPropostaId)
    
    
    'Obtendo percentual total de corretagem ''''''''''''''''''''''''''''''''''''''''''''''''''
    FrmAvaliacao.dPercCorretagemRest = ObterPercentualTotalCorretagem(lPropostaId)

    'Obtendo o valores da subven��o - Rmarins - 20/10/06'''''''''''''''''''''''''''''''
    If gsPossuiSubvencao = "S" And bModular = False Then
    
        'Exibindo campos para subven��o
        FrmAvaliacao.lblEstadualCalc.Visible = True
        FrmAvaliacao.lblValSubvencaoEstadualCalc.Visible = True
        FrmAvaliacao.lblEstadual.Visible = True
        FrmAvaliacao.maskValSubvencaoEstadual.Visible = True
        FrmAvaliacao.lblFederalCalc.Visible = True
        FrmAvaliacao.lblValSubvencaoFederalCalc.Visible = True
        FrmAvaliacao.lblFederal.Visible = True
        FrmAvaliacao.lblValSubvencaoFederal.Visible = True

        'bcarneiro - 05/03/2007 - Restitui��o Agr�cola
        'Call ObterValoresSubvencao(lPropostaId, cValPremio)
        Call ObterValoresSubvencao(lPropostaId, gcValTotPremioTarifaPago)
        '''''''''''''''''''''''''''''''''''''''''''''''
        
    End If

    'Descarregando form de sele��o''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    If Not alteraEmLote Then
        Me.Hide
    End If

    'Configurando interface'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    FrmAvaliacao.Mask2ValPremioTarifa.Mask = "12V2"
    FrmAvaliacao.Mask2ValPremioTarifa.Text = "0,00"
    
    'Exibindo form '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    If Not alteraEmLote Then
'        Exit Sub
'        FrmAvaliacao.Hide
'    Else
        FrmAvaliacao.Show
    End If

    If TpOperacao = 1 Then
        FrmAvaliacao.Caption = "SEGP0209 - Avalia��o T�cnica de Restitui��o - " & Ambiente
    Else
        FrmAvaliacao.Caption = "SEGP0209 - Avalia��o Gerencial de Restitui��o - " & Ambiente
    End If

    'Exibindo dados necess�rios para a restitui��o '''''''''''''''''''''''''''''''''''''''''''
    FrmAvaliacao.Mask2ValPremioTarifa.Enabled = True
    
    FrmAvaliacao.txtTipoEndosso.Text = Trim(Item.SubItems(13))
    FrmAvaliacao.txtTpEmdossoRest.Text = Trim(Item.SubItems(13))
    If bModular = True Then
        FrmAvaliacao.txtTpEmdossoRestModular.Text = Trim(Item.SubItems(13))
    End If
    
    FrmAvaliacao.Ler_Dados_Proposta lPropostaId
    
    Call ConsultarPercIOF(sDtInicioVigencia, _
                          FrmAvaliacao.txtSubramoId)
    
    
    FrmAvaliacao.Preencher_GridCobrancas
    FrmAvaliacao.Ler_Endossos lPropostaId

    'FrmAvaliacao.Ler_Avaliacoes lPropostaId, Nr_Endosso_BB
    FrmAvaliacao.LerAvaliacoesRestituicao lPropostaId

    If Trim(sDtFimVigencia) <> "" Then
        FrmAvaliacao.LblPrazoSeguro.Caption = DateDiff("d", sDtInicioVigencia, sDtFimVigencia)
    Else
        FrmAvaliacao.LblPrazoSeguro.Caption = ""
    End If
    
    If bModular = True Then
        FrmAvaliacao.lblValParametroRestituicao(1).Caption = FrmAvaliacao.LblPrazoSeguro.Caption
    
        'modular
        If Trim(oDadosModular.Dt_Fim_Vigencia) <> "" Then
            FrmAvaliacao.lblValParametroRestituicao(9).Caption = DateDiff("d", oDadosModular.Dt_Inicio_Vigencia, oDadosModular.Dt_Fim_Vigencia)
        Else
            FrmAvaliacao.lblValParametroRestituicao(9).Caption = ""
        End If
    End If
    'Alessandra Grig�rio - 09/03/2004 - Deve exibir utiliza��o do seguro considerando a data de pedido do cancelamento
    'FrmAvaliacao.LblDiasUtilizacaoSeguro.Caption = DateDiff("d", sDtInicioVigencia, Data_Sistema)
    FrmAvaliacao.LblDiasUtilizacaoSeguro.Caption = DateDiff("d", sDtInicioVigencia, sDtPedidoAvaliacao)
    
    If bModular = True Then
        FrmAvaliacao.lblValParametroRestituicao(5).Caption = FrmAvaliacao.LblDiasUtilizacaoSeguro.Caption
        FrmAvaliacao.lblValParametroRestituicao(13).Caption = DateDiff("d", oDadosModular.Dt_Inicio_Vigencia, oDadosModular.Dt_Pedido_Avaliacao)
    End If
    
    FrmAvaliacao.fraDados.Caption = "Dados da Restitui��o"
    FrmAvaliacao.TxtPropostaBB.Text = lPropostaBB
    FrmAvaliacao.lblTexto(13).Caption = "Nr.Avalia��o:"
    FrmAvaliacao.TxtEndossoBB.Text = lAvaliacao
    FrmAvaliacao.lblDtCanc.Caption = "Dt.Pedido:"
    FrmAvaliacao.lblDtLiq.Visible = False
    FrmAvaliacao.TxtDtCancelamentoBB.Text = sDtPedidoAvaliacao
    FrmAvaliacao.lblDtReceb.Visible = False
    FrmAvaliacao.TxtDtRecebimentoEndosso.Visible = False
        
    FrmAvaliacao.txtProposta.Text = lPropostaId
    
    If bModular = True Then
        FrmAvaliacao.txtpropostaModular(0).Text = lPropostaId
        FrmAvaliacao.txtpropostaModular(1).Text = oDadosModular.proposta_id
    End If
    
    FrmAvaliacao.txtIs.Text = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValIS))
    FrmAvaliacao.txtPremio.Text = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValPremio))

    If TpOperacao = 1 Then  'Avaliacao Tecnica
        FrmAvaliacao.Mask2ValPremioTarifa.Locked = False
        FrmAvaliacao.Mask2ValPremioTarifaModular(0).Locked = False
        FrmAvaliacao.Mask2ValPremioTarifaModular(1).Locked = False
        'FrmAvaliacao.maskValSubvencaoEstadual.Locked = False
    Else
        FrmAvaliacao.Mask2ValPremioTarifa.Locked = True
        FrmAvaliacao.Mask2ValPremioTarifaModular(0).Locked = True
        FrmAvaliacao.Mask2ValPremioTarifaModular(1).Locked = True
        FrmAvaliacao.maskValSubvencaoEstadual.Locked = True
    End If
    
    'Exibindo dados necess�rios para restitui��o''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    FrmAvaliacao.LblTotValPremioPago.Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValTotPremioPago))
    FrmAvaliacao.LblTotValJuros.Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValTotJurosPago))
    FrmAvaliacao.LblCustoCertifApolice.Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValCustoApoliceCertif))
    FrmAvaliacao.LblValPremioEmitido.Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValPremio))

    If bModular = True Then
        FrmAvaliacao.lblValParametroRestituicao(2).Caption = FrmAvaliacao.LblTotValPremioPago.Caption 'Total Premio Pago
        FrmAvaliacao.lblValParametroRestituicao(3).Caption = FrmAvaliacao.LblTotValJuros.Caption 'Total Juros Pago
        FrmAvaliacao.lblValParametroRestituicao(6).Caption = FrmAvaliacao.LblValPremioEmitido.Caption 'Premio Emitido
        FrmAvaliacao.lblValParametroRestituicao(7).Caption = FrmAvaliacao.LblCustoCertifApolice.Caption 'Custo Certificacao
        
        
        FrmAvaliacao.lblValParametroRestituicao(10).Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(oDadosModular.Val_Premio_Pago)) 'Total Premio Pago
        FrmAvaliacao.lblValParametroRestituicao(11).Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(oDadosModular.Val_Juros)) 'Total Juros Pago
        FrmAvaliacao.lblValParametroRestituicao(14).Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(oDadosModular.Val_Premio)) 'Premio Emitido
        FrmAvaliacao.lblValParametroRestituicao(15).Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(oDadosModular.Custo_Apolice)) 'Custo Certificacao
    End If

    'Exibindo valor estimado de restitui��o'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    FrmAvaliacao.LblValRestituicaoEstimado = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValRestituicaoEst))
    If bModular = True Then
        FrmAvaliacao.lblestimativaModular(0).Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValRestituicaoEst))
        FrmAvaliacao.lblestimativaModular(1).Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(oDadosModular.ValRestituicaoEst))
        If Produto_id = 1168 Then
            FrmAvaliacao.LblValRestituicaoEstimado = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValRestituicaoEst + oDadosModular.ValRestituicaoEst))
        End If
    End If
    
    'Exibindo valores calculados da restitui��o'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    FrmAvaliacao.LblValPremioTarifaCalc.Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValPremioTarifaRest))
    FrmAvaliacao.LblCustoCertifApoliceCalc.Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValCustoApoliceCertifRest))
    FrmAvaliacao.LblValIOFCalc.Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValIOFRest))
    FrmAvaliacao.LblValAdicFracionamentoCalc.Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValAdicFracionamentoRest))
    FrmAvaliacao.LblValDescontoCalc.Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValDesconto))
    FrmAvaliacao.LblValRestituicaoCalc.Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValRestituicao))
    If bModular = True Then
        'Premio Tarifa
        FrmAvaliacao.lblvalCalculadoModular(0).Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValPremioTarifaEst))
        FrmAvaliacao.lblvalCalculadoModular(17).Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(oDadosModular.ValPremioTarifaEst))
        'Custo Certificado
        FrmAvaliacao.lblvalCalculadoModular(1).Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValCustoApoliceCertifEst))
        FrmAvaliacao.lblvalCalculadoModular(18).Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(oDadosModular.ValCustoApoliceCertifEst))
        'Adic. Fracionamento
        FrmAvaliacao.lblvalCalculadoModular(2).Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValAdicFracionamentoEst))
        FrmAvaliacao.lblvalCalculadoModular(19).Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(oDadosModular.ValAdicFracionamentoEst))
        'Desconto
        FrmAvaliacao.lblvalCalculadoModular(3).Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValDescontoEst))
        FrmAvaliacao.lblvalCalculadoModular(20).Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(oDadosModular.ValDescontoEst))
        'IOF
        FrmAvaliacao.lblvalCalculadoModular(4).Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValIOFEst))
        FrmAvaliacao.lblvalCalculadoModular(21).Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(oDadosModular.ValIOFEst))
        'Valor Devolu��o
        FrmAvaliacao.lblvalCalculadoModular(5).Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValRestituicaoEst))
        FrmAvaliacao.lblvalCalculadoModular(22).Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(oDadosModular.ValRestituicaoEst))
        
        If Produto_id = 1168 Then
            FrmAvaliacao.LblValPremioTarifaCalc.Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValPremioTarifaEst + oDadosModular.ValPremioTarifaEst))
            FrmAvaliacao.LblCustoCertifApoliceCalc.Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValCustoApoliceCertifEst + oDadosModular.ValCustoApoliceCertifEst))
            FrmAvaliacao.LblValIOFCalc.Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValIOFEst + oDadosModular.ValIOFEst))
            FrmAvaliacao.LblValAdicFracionamentoCalc.Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValAdicFracionamentoEst + oDadosModular.ValAdicFracionamentoEst))
            FrmAvaliacao.LblValDescontoCalc.Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValDescontoEst + oDadosModular.ValDescontoEst))
            FrmAvaliacao.LblValRestituicaoCalc.Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValRestituicaoEst + oDadosModular.ValRestituicaoEst))
        End If
        
    End If
    'Propondo valores da restitui��o informados''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    FrmAvaliacao.Mask2ValPremioTarifa.Text = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValPremioTarifaRest))
    FrmAvaliacao.LblCustoCertifApoliceRest.Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValCustoApoliceCertifRest))
    FrmAvaliacao.LblValIOF.Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValIOFRest))
    FrmAvaliacao.LblValAdicFracionamento.Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValAdicFracionamentoRest))
    FrmAvaliacao.LblValDesconto.Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValDescontoRest))
    FrmAvaliacao.LblValRestituicao.Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValRestituicao))
    
    If bModular = True Then
        'Premio Tarifa
        FrmAvaliacao.Mask2ValPremioTarifaModular(0).Text = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValPremioTarifaRest))
        FrmAvaliacao.Mask2ValPremioTarifaModular(1).Text = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(oDadosModular.ValPremioTarifaRest))
        'Custo Certificado
        FrmAvaliacao.lblvalCalculadoModular(9).Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValCustoApoliceCertifRest))
        FrmAvaliacao.lblvalCalculadoModular(26).Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(oDadosModular.ValCustoApoliceCertifRest))
        'Adic. Fracionamento
        FrmAvaliacao.lblvalCalculadoModular(10).Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValAdicFracionamentoRest))
        FrmAvaliacao.lblvalCalculadoModular(27).Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(oDadosModular.ValAdicFracionamentoRest))
        'Desconto
        FrmAvaliacao.lblvalCalculadoModular(11).Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValDescontoRest))
        FrmAvaliacao.lblvalCalculadoModular(28).Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(oDadosModular.ValDescontoRest))
        'IOF
        FrmAvaliacao.lblvalCalculadoModular(12).Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValIOFRest))
        FrmAvaliacao.lblvalCalculadoModular(29).Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(oDadosModular.ValIOFRest))
        'Valor Devolu��o
        FrmAvaliacao.lblvalCalculadoModular(13).Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValRestituicao))
        FrmAvaliacao.lblvalCalculadoModular(30).Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(oDadosModular.ValRestituicao))
        
        If Produto_id = 1168 Then
            FrmAvaliacao.Mask2ValPremioTarifa.Text = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValPremioTarifaRest + oDadosModular.ValPremioTarifaRest))
            FrmAvaliacao.LblCustoCertifApoliceRest.Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValCustoApoliceCertifRest + oDadosModular.ValCustoApoliceCertifRest))
            FrmAvaliacao.LblValIOF.Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValIOFRest + oDadosModular.ValIOFRest))
            FrmAvaliacao.LblValAdicFracionamento.Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValAdicFracionamentoRest + oDadosModular.ValAdicFracionamentoRest))
            FrmAvaliacao.LblValDesconto.Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValDescontoRest + oDadosModular.ValDescontoRest))
            FrmAvaliacao.LblValRestituicao.Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValRestituicao + oDadosModular.ValRestituicao))
        End If
        
    End If

    'Exibindo valores calculados de comiss�o '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    FrmAvaliacao.LblTotValCorretagemCalc.Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValTotCorretagemEst))
    FrmAvaliacao.LblValProLaboreCalc.Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValProLaboreEst))
    'mathayde 27/03/2009 - Exibe a remunera��o
    FrmAvaliacao.LblValRemuneracaoCalc.Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValRemuneracaoEst))
    'Exibindo valores comiss�o Informados '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    FrmAvaliacao.LblTotValCorretagem.Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValTotCorretagemRest))
    FrmAvaliacao.LblValProLabore.Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValProLaboreRest))
    'mathayde 27/03/2009 - Exibe a remunera��o
    FrmAvaliacao.LblValRemuneracao.Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValRemuneracao))
    
    If bModular = True Then
        FrmAvaliacao.lblvalCalculadoModular(6).Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValTotCorretagemEst))
        FrmAvaliacao.lblvalCalculadoModular(23).Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(oDadosModular.Val_Tot_Corretagem_Est))
        FrmAvaliacao.lblvalCalculadoModular(7).Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValProLaboreEst))
        FrmAvaliacao.lblvalCalculadoModular(24).Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(oDadosModular.Val_Pro_Lab_Est))
        FrmAvaliacao.lblvalCalculadoModular(8).Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValRemuneracaoEst))
        FrmAvaliacao.lblvalCalculadoModular(25).Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(oDadosModular.Val_Remuneracao_Est))
        
        
        FrmAvaliacao.lblvalCalculadoModular(14).Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValTotCorretagemRest))
        FrmAvaliacao.lblvalCalculadoModular(31).Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(oDadosModular.ValTotCorretagemRest))
        FrmAvaliacao.lblvalCalculadoModular(15).Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValProLaboreRest))
        FrmAvaliacao.lblvalCalculadoModular(32).Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(oDadosModular.ValProLaboreRest))
        FrmAvaliacao.lblvalCalculadoModular(16).Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValRemuneracao))
        FrmAvaliacao.lblvalCalculadoModular(33).Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(oDadosModular.Val_Remuneracao))
        
        If Produto_id = 1168 Then
            FrmAvaliacao.LblTotValCorretagemCalc.Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValTotCorretagemEst + oDadosModular.Val_Tot_Corretagem_Est))
            FrmAvaliacao.LblValProLaboreCalc.Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValProLaboreEst + oDadosModular.Val_Pro_Lab_Est))
            FrmAvaliacao.LblValRemuneracaoCalc.Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValRemuneracaoEst + oDadosModular.Val_Remuneracao_Est))
            FrmAvaliacao.LblTotValCorretagem.Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValTotCorretagemRest + oDadosModular.ValTotCorretagemRest))
            FrmAvaliacao.LblValProLabore.Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValProLaboreRest + oDadosModular.ValProLaboreRest))
            FrmAvaliacao.LblValRemuneracao.Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValRemuneracao + oDadosModular.Val_Remuneracao))
        End If
        
    End If
    
    
    If bModular = False Then
        'Exibindo valores calculados de subven��o ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        FrmAvaliacao.lblValSubvencaoEstadualCalc.Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValSubvencaoEstadualEst))
        FrmAvaliacao.lblValSubvencaoFederalCalc.Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValSubvencaoFederalEst))
        
        'Exibindo valores de subven��o informados ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        FrmAvaliacao.maskValSubvencaoEstadual.Mask = "12V2"
        FrmAvaliacao.maskValSubvencaoEstadual.Text = "0,00"
        FrmAvaliacao.maskValSubvencaoEstadual.Text = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValSubvencaoEstadual))
        FrmAvaliacao.lblValSubvencaoFederal.Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValSubvencaoFederal))
    End If
    
    
    
    'Exibindo valores calculados de subven��o ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    FrmAvaliacao.lblValSubvencaoEstadualCalc.Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValSubvencaoEstadualEst))
    FrmAvaliacao.lblValSubvencaoFederalCalc.Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValSubvencaoFederalEst))
    
    'Exibindo valores de subven��o informados ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    FrmAvaliacao.maskValSubvencaoEstadual.Mask = "12V2"
    FrmAvaliacao.maskValSubvencaoEstadual.Text = "0,00"
    FrmAvaliacao.maskValSubvencaoEstadual.Text = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValSubvencaoEstadual))
    FrmAvaliacao.lblValSubvencaoFederal.Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValSubvencaoFederal))

    'Configurando interface'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    FrmAvaliacao.Definir_Exibicao_Objetos

    FrmAvaliacao.fraSemRestituicao.Visible = False

    If bModular = False Then
        FrmAvaliacao.SSTabDados.TabVisible(5) = False
        FrmAvaliacao.SSTabDados.TabVisible(4) = True
        FrmAvaliacao.SSTabDados.Tab = 4


    Else
        FrmAvaliacao.SSTabDados.TabVisible(4) = False
        FrmAvaliacao.SSTabDados.TabVisible(5) = True
        FrmAvaliacao.SSTabDados.Tab = 5
    End If
    
    'rsilva 25/08/2009
    'Identificar se a proposta para a qual esta sendo atribuindo o valor � a de RE ou a de AP.
    If bModular = True Then
        Dim OPropModular As Object
        Dim Tipo As String

        Set OPropModular = CreateObject("SEGL0026.cls00125")
    
        If sDtInicioEsc = "" Or sDtInicioSec = "" Then
            Call OPropModular.ObterParametroRestituicao(gsSIGLASISTEMA, _
                                                        App.Title, _
                                                        App.FileDescription, _
                                                        glAmbiente_id, _
                                                        lPropostaId, oDadosModular.proposta_id, _
                                                        sDtInicioEsc, _
                                                        sDtFimEsc)
                                                        
            
            oDadosModular.sDtInicioVigenciaEsc = sDtInicioEsc
            oDadosModular.sDtFimVigenciaEsc = sDtFimEsc
        
        End If
        
        
        If Produto_id <> 1168 Then

            ' C_ANEVES - 26/08/2013 - Demanda: 17860335 - Inicio
            Set OPropModular = CreateObject("SEGL0026.cls00124")
            'Set OPropModular = CreateObject("SEGL0355.cls00626")
            ' C_ANEVES - 26/08/2013 - Demanda: 17860335 - Fim
        
            FrmAvaliacao.lbltipoProposta(0).AutoSize = True
            FrmAvaliacao.lbltipoProposta(1).AutoSize = True
        
            FrmAvaliacao.lbltipoProposta(0).Caption = ""
            FrmAvaliacao.lbltipoProposta(1).Caption = ""
        
            
            Tipo = OPropModular.IdentificaValorRestituicao(gsSIGLASISTEMA, _
                                                           App.Title, _
                                                           App.FileDescription, _
                                                           glAmbiente_id, _
                                                           lPropostaId)
            
            FrmAvaliacao.lbltipoProposta(0).Caption = "Tipo: " & Tipo
            
            
            Tipo = OPropModular.IdentificaValorRestituicao(gsSIGLASISTEMA, _
                                                                 App.Title, _
                                                                 App.FileDescription, _
                                                                 glAmbiente_id, _
                                                                 oDadosModular.proposta_id)
            
            FrmAvaliacao.lbltipoProposta(1).Caption = "Tipo: " & Tipo
        Else
            'If lAvaliacao > oDadosModular.Num_avaliacao Then
            If oDadosModular.iTpCobertura = 526 Then
                FrmAvaliacao.lbltipoProposta(0).Caption = "Tipo: MIP"
                FrmAvaliacao.lbltipoProposta(1).Caption = "Tipo: DFI"
            Else
                FrmAvaliacao.lbltipoProposta(0).Caption = "Tipo: DFI"
                FrmAvaliacao.lbltipoProposta(1).Caption = "Tipo: MIP"
            End If
        
        End If
    End If
    
    

    MousePointer = vbDefault

    Exit Sub

TrataErro:
    Call TrataErroGeral("ExibirDetalhesRestituicao", Me.name)
    Call TerminaSEGBR

End Sub

Sub Obtem_Perc_IRModular(ByVal PRamo As String, ByVal PDtReferencia As String, ByRef PPercIr As Currency)

Dim rc As rdoResultset
Dim SQL As String
Dim DtReferencia As String

DtReferencia = Format(PDtReferencia, "yyyymmdd")

'Obtendo percentual de IR

SQL = "SELECT valor "
SQL = SQL & "From "
SQL = SQL & "     val_item_financeiro_ramo_tb "
SQL = SQL & "Where "
SQL = SQL & "     ramo_id = " & PRamo
SQL = SQL & "     and ltrim(rtrim(cod_item_financeiro)) = 'IR'"

SQL = SQL & " and dt_inicio_vigencia <= '" & DtReferencia & "'"
SQL = SQL & " and (dt_fim_vigencia >= '" & DtReferencia & "' or dt_fim_vigencia is Null)"

Set rc = rdocn.OpenResultset(SQL)

If Not rc.EOF Then
   oDadosModular.PercIR = Val(rc!valor)
Else
   MensagemBatch "Percentual de IR n�o encontrado. o Programa ser� cancelado."
   End
End If

Exit Sub

TrataErro:
   TrataErroGeral "Obtem_Perc_IR", "Rateio.bas"
   End

End Sub

Public Sub ObterValoresSubvencao(ByVal lPropostaId As Long, _
                                 ByVal cValPremio As Currency)

Dim rs As Object
Dim oSubvencao As Object

Dim cValSubvencaoInformado As Currency
Dim dPercFederal As Double

'Consulta os valores de subvencao por proposta - Rmarins 20/10/06

On Error GoTo TrataErro
    
    cValSubvencaoInformado = 0
    bPossuiSubvencao = False
    
    Set oSubvencao = CreateObject("SEGL0026.cls00125")
    
    Set rs = oSubvencao.ObterPropostaSubvencao(gsSIGLASISTEMA, _
                                               App.Title, _
                                               App.FileDescription, _
                                               glAmbiente_id, _
                                               lPropostaId)
    
    Set oSubvencao = Nothing
    
    If Not rs.EOF Then
        cValSubvencaoInformado = CCur(rs("val_subvencao_informado"))
        bPossuiSubvencao = True
    End If
     
    Set rs = Nothing
        
    dPercFederal = CalcularPercValor(cValPremio, _
                                     cValSubvencaoInformado)
    
    FrmAvaliacao.lblPercFederal.Caption = Format(dPercFederal, "##0.00000")
        
    Exit Sub
    
TrataErro:
   Call TratarErro("ObterValoresSubvencao", Me.name)
   FinalizarAplicacao
   
End Sub


Public Function CalcularPercValor(ByVal cValPrincipal As Currency, _
                                  ByVal cValInformado As Currency) As Double

Dim dPercentual As Double

On Error GoTo TrataErro

    If cValPrincipal = 0 Then
       cValInformado = 0
       CalcularPercValor = cValInformado
       Exit Function
    End If
                
    dPercentual = ((cValInformado / cValPrincipal) * 100)
    
    If dPercentual < 0 Then
       dPercentual = 0
    End If
    
    CalcularPercValor = dPercentual
    
    Exit Function

TrataErro:
    Call TratarErro("CalcularPercValor", Me.name)
    Call FinalizarAplicacao

End Function


Public Sub ObterValoresAvaliacaoRestituicaoModular(ByVal lPropostaId As Long, _
                                                    ByVal lAvaliacao As Long)
    
Dim oFinanceiro As Object
Dim rs As Object

On Error GoTo TrataErro

    Set oFinanceiro = CreateObject("SEGL0026.cls00125")
    
    Set rs = oFinanceiro.ConsultarAvaliacaoRestituicao(gsSIGLASISTEMA, _
                                                       App.Title, _
                                                       App.FileDescription, _
                                                       glAmbiente_id, _
                                                       sSituacao, _
                                                       , _
                                                       lPropostaId, _
                                                       lAvaliacao)

    Set oFinanceiro = Nothing

    If Not rs.EOF Then
        With oDadosModular
            If TpOperacao = 1 Then
    
                .ValRestituicao = rs("val_restituicao_estimado")
                .ValIOFRest = rs("val_iof_estimado")
                .ValCustoApoliceCertifRest = rs("custo_apolice_estimado")
                .ValAdicFracionamentoRest = rs("val_adic_fracionamento_estimado")
                .ValDescontoRest = rs("val_desconto_comercial_estimado")
                .ValPremioTarifaRest = rs("val_premio_tarifa_estimado")
                .ValTotCorretagemRest = rs("val_comissao_estimado")
                .ValProLaboreRest = rs("val_comissao_estipulante_estimado")
    
                .ValRestituicaoEst = rs("val_restituicao_estimado")
                .ValIOFEst = rs("val_iof_estimado")
                .ValCustoApoliceCertifEst = rs("custo_apolice_estimado")
                .ValAdicFracionamentoEst = rs("val_adic_fracionamento_estimado")
                .ValDescontoEst = rs("val_desconto_comercial_estimado")
                .ValPremioTarifaEst = rs("val_premio_tarifa_estimado")
                .Val_Tot_Corretagem_Est = rs("val_comissao_estimado")
                .Val_Pro_Lab_Est = rs("val_comissao_estipulante_estimado")
                
                'mathayde 27/03/2009 - Obtem o valor da remunera��o
                .Val_Remuneracao = rs("val_remuneracao")
                .Val_Remuneracao_Est = rs("val_remuneracao_estimado")
                
                
    
                .ValCambio = rs("val_cambio")
                'sTextoEndosso = Trim(rs("descricao"))
                .EndossoId = rs("endosso_id")
                
            Else
                .ValRestituicao = rs("val_restituicao")
                .ValIOFRest = rs("val_iof")
                .ValCustoApoliceCertifRest = rs("custo_apolice")
                .ValAdicFracionamentoRest = rs("val_adic_fracionamento")
                .ValDescontoRest = rs("val_desconto_comercial")
                .ValPremioTarifaRest = rs("val_premio_tarifa")
                .ValTotCorretagemRest = rs("val_comissao")
                .ValProLaboreRest = rs("val_comissao_estipulante")
                
                .ValRestituicaoEst = rs("val_restituicao_estimado")
                .ValIOFEst = rs("val_iof_estimado")
                .ValCustoApoliceCertifEst = rs("custo_apolice_estimado")
                .ValAdicFracionamentoEst = rs("val_adic_fracionamento_estimado")
                .ValDescontoEst = rs("val_desconto_comercial_estimado")
                .ValPremioTarifaEst = rs("val_premio_tarifa_estimado")
                .Val_Tot_Corretagem_Est = rs("val_comissao_estimado")
                .Val_Pro_Lab_Est = rs("val_comissao_estipulante_estimado")
                
                'mathayde 27/03/2009 - Obtem o valor da remunera��o
                .Val_Remuneracao = rs("val_remuneracao")
                .Val_Remuneracao_Est = rs("val_remuneracao_estimado")
                
                .ValCambio = rs("val_cambio")
                'sTextoEndosso = Trim(rs("descricao"))
                .EndossoId = rs("endosso_id")
                
'                cValRestituicao = rs("val_restituicao")
    '            cValIOFRest = rs("val_iof")
    '            cValCustoApoliceCertifRest = rs("custo_apolice")
    '            cValAdicFracionamentoRest = rs("val_adic_fracionamento")
    '            cValDescontoRest = rs("val_desconto_comercial")
    '            cValPremioTarifaRest = rs("val_premio_tarifa")
    '            cValTotCorretagemRest = rs("val_comissao")
    '            cValProLaboreRest = rs("val_comissao_estipulante")
    '
    '            cValRestituicaoEst = rs("val_restituicao_estimado")
    '            cValIOFEst = rs("val_iof_estimado")
    '            cValCustoApoliceCertifEst = rs("custo_apolice_estimado")
    '            cValAdicFracionamentoEst = rs("val_adic_fracionamento_estimado")
    '            cValDescontoEst = rs("val_desconto_comercial_estimado")
    '            cValPremioTarifaEst = rs("val_premio_tarifa_estimado")
    '            cValTotCorretagemEst = rs("val_comissao_estimado")
    '            cValProLaboreEst = rs("val_comissao_estipulante_estimado")
    '
    '            'mathayde 27/03/2009 - Obtem o valor da remunera��o
    '            cValRemuneracao = rs("val_remuneracao")
    '            cValRemuneracaoEst = rs("val_remuneracao_estimado")
    '
    '
    '            cValCambio = rs("val_cambio")
    '            sTextoEndosso = Trim(rs("descricao"))
    '            lEndossoId = rs("endosso_id")
    '
    '            'Rmarins - 20/10/06
    '            cValSubvencaoFederal = rs("val_subvencao_federal")
    '            cValSubvencaoEstadual = rs("val_subvencao_estadual")
    '            cValSubvencaoFederalEst = rs("val_subvencao_federal_estimado")
    '            cValSubvencaoEstadualEst = rs("val_subvencao_estadual_estimado")
    '            '
        End If
           
            
        'End If
        End With
    End If

    rs.Close
    
    Exit Sub

TrataErro:
    Call TrataErroGeral("ObterValoresAvaliacaoRestituicao", Me.name)
    Call TerminaSEGBR

End Sub

Public Sub ObterValoresAvaliacaoRestituicao(ByVal lPropostaId As Long, _
                                            ByVal lAvaliacao As Long, _
                                            ByVal sSituacao As String, _
                                            ByRef cValRestituicao As Currency, _
                                            ByRef cValIOFRest As Currency, _
                                            ByRef cValCustoApoliceCertifRest As Currency, _
                                            ByRef cValAdicFracionamentoRest As Currency, _
                                            ByRef cValDescontoRest As Currency, _
                                            ByRef cValPremioTarifaRest As Currency, _
                                            ByRef cValTotCorretagemRest As Currency, _
                                            ByRef cValProLaboreRest As Currency, _
                                            ByRef cValCustoApoliceCertifEst As Currency, _
                                            ByRef cValAdicFracionamentoEst As Currency, _
                                            ByRef cValDescontoEst As Currency, _
                                            ByRef cValIOFEst As Currency, _
                                            ByRef cValPremioTarifaEst As Currency, _
                                            ByRef cValRestituicaoEst As Currency, _
                                            ByRef cValTotCorretagemEst As Currency, _
                                            ByRef cValProLaboreEst As Currency, _
                                            ByRef lEndossoId As Long, _
                                            Optional ByRef cValCambio As Currency, _
                                            Optional ByRef sTextoEndosso As String, _
                                            Optional ByRef cValSubvencaoFederal As Currency, Optional ByRef cValSubvencaoEstadual As Currency, _
                                            Optional ByRef cValSubvencaoFederalEst As Currency, Optional ByRef cValSubvencaoEstadualEst As Currency, _
                                            Optional ByRef cValRemuneracao As Currency, Optional ByRef cValRemuneracaoEst As Currency)
                                            'Mathayde 30/03/2009 - Recebe cValRemuneracao e cValRemuneracaoEst do Banco

' bcarneiro - 02/12/2003 - Projeto Restitui��o por Iniciativa da Seguradora

Dim oFinanceiro As Object
Dim rs As Object

On Error GoTo TrataErro

    Set oFinanceiro = CreateObject("SEGL0026.cls00125")
    
    Set rs = oFinanceiro.ConsultarAvaliacaoRestituicao(gsSIGLASISTEMA, _
                                                       App.Title, _
                                                       App.FileDescription, _
                                                       glAmbiente_id, _
                                                       sSituacao, _
                                                       , _
                                                       lPropostaId, _
                                                       lAvaliacao)

    Set oFinanceiro = Nothing

    If Not rs.EOF Then

        If TpOperacao = 1 Then

            cValRestituicao = rs("val_restituicao_estimado")
            cValIOFRest = rs("val_iof_estimado")
            cValCustoApoliceCertifRest = rs("custo_apolice_estimado")
            cValAdicFracionamentoRest = rs("val_adic_fracionamento_estimado")
            cValDescontoRest = rs("val_desconto_comercial_estimado")
            cValPremioTarifaRest = rs("val_premio_tarifa_estimado")
            cValTotCorretagemRest = rs("val_comissao_estimado")
            cValProLaboreRest = rs("val_comissao_estipulante_estimado")

            cValRestituicaoEst = rs("val_restituicao_estimado")
            cValIOFEst = rs("val_iof_estimado")
            cValCustoApoliceCertifEst = rs("custo_apolice_estimado")
            cValAdicFracionamentoEst = rs("val_adic_fracionamento_estimado")
            cValDescontoEst = rs("val_desconto_comercial_estimado")
            cValPremioTarifaEst = rs("val_premio_tarifa_estimado")
            cValTotCorretagemEst = rs("val_comissao_estimado")
            cValProLaboreEst = rs("val_comissao_estipulante_estimado")
            
            'mathayde 27/03/2009 - Obtem o valor da remunera��o
            cValRemuneracaoEst = rs("val_remuneracao_estimado")
            cValRemuneracao = rs("val_remuneracao_estimado")
            

            cValCambio = rs("val_cambio")
            sTextoEndosso = Trim(rs("descricao"))
            lEndossoId = rs("endosso_id")
            
            'Rmarins - 20/10/06
            cValSubvencaoFederal = rs("val_subvencao_federal_estimado")
            cValSubvencaoEstadual = rs("val_subvencao_estadual_estimado")
            cValSubvencaoFederalEst = rs("val_subvencao_federal_estimado")
            cValSubvencaoEstadualEst = rs("val_subvencao_estadual_estimado")
            '
        Else
            '
            '06/12/2010 - P. Roberto
            'Flow - 7497885 - Verifica se os valores retornados s�o nulos
            cValRestituicao = VerNullNumerico(rs("val_restituicao"))
            cValPremioTarifaRest = VerNullNumerico(rs("val_premio_tarifa"))
            cValTotCorretagemRest = VerNullNumerico(rs("val_comissao"))
            cValProLaboreRest = VerNullNumerico(rs("val_comissao_estipulante"))
            '
            cValIOFRest = rs("val_iof")
            cValCustoApoliceCertifRest = rs("custo_apolice")
            cValAdicFracionamentoRest = rs("val_adic_fracionamento")
            cValDescontoRest = rs("val_desconto_comercial")
            
            cValRestituicaoEst = rs("val_restituicao_estimado")
            cValIOFEst = rs("val_iof_estimado")
            cValCustoApoliceCertifEst = rs("custo_apolice_estimado")
            cValAdicFracionamentoEst = rs("val_adic_fracionamento_estimado")
            cValDescontoEst = rs("val_desconto_comercial_estimado")
            cValPremioTarifaEst = rs("val_premio_tarifa_estimado")
            cValTotCorretagemEst = rs("val_comissao_estimado")
            cValProLaboreEst = rs("val_comissao_estipulante_estimado")
             
            'mathayde 27/03/2009 - Obtem o valor da remunera��o
            cValRemuneracao = rs("val_remuneracao")
            cValRemuneracaoEst = rs("val_remuneracao_estimado")
                
            If AvaliacaoGerencial = True And bModular = True Then
                cValRestituicao = rs("val_restituicao") + oDadosModular.ValRestituicao
                cValIOFRest = rs("val_iof") + oDadosModular.ValIOFRest
                cValCustoApoliceCertifRest = rs("custo_apolice") + oDadosModular.ValCustoApoliceCertifRest
                cValAdicFracionamentoRest = rs("val_adic_fracionamento") + oDadosModular.ValAdicFracionamentoRest
                cValDescontoRest = rs("val_desconto_comercial") + oDadosModular.ValDescontoRest
                cValPremioTarifaRest = rs("val_premio_tarifa") + oDadosModular.ValPremioTarifaRest
                cValTotCorretagemRest = rs("val_comissao") + oDadosModular.ValTotCorretagemRest
                cValProLaboreRest = rs("val_comissao_estipulante") + oDadosModular.ValProLaboreRest

                cValRestituicaoEst = rs("val_restituicao_estimado") + oDadosModular.ValRestituicaoEst
                cValIOFEst = rs("val_iof_estimado") + oDadosModular.ValIOFEst
                cValCustoApoliceCertifEst = rs("custo_apolice_estimado") + oDadosModular.ValCustoApoliceCertifEst
                cValAdicFracionamentoEst = rs("val_adic_fracionamento_estimado") + oDadosModular.ValCustoApoliceCertifEst
                cValDescontoEst = rs("val_desconto_comercial_estimado") + oDadosModular.ValDescontoEst
                cValPremioTarifaEst = rs("val_premio_tarifa_estimado") + oDadosModular.ValPremioTarifaEst
                cValTotCorretagemEst = rs("val_comissao_estimado") + oDadosModular.Val_Tot_Corretagem_Est
                cValProLaboreEst = rs("val_comissao_estipulante_estimado") + oDadosModular.Val_Pro_Lab_Est

                'mathayde 27/03/2009 - Obtem o valor da remunera��o
                cValRemuneracao = rs("val_remuneracao") + oDadosModular.Val_Remuneracao
                cValRemuneracaoEst = rs("val_remuneracao_estimado") + oDadosModular.Val_Remuneracao_Est
            End If
            
            
                cValCambio = rs("val_cambio")
                sTextoEndosso = Trim(rs("descricao"))
                lEndossoId = rs("endosso_id")
            
                'Rmarins - 20/10/06
                cValSubvencaoFederal = rs("val_subvencao_federal")
                cValSubvencaoEstadual = rs("val_subvencao_estadual")
                cValSubvencaoFederalEst = rs("val_subvencao_federal_estimado")
                cValSubvencaoEstadualEst = rs("val_subvencao_estadual_estimado")
                '
                
           
        End If

    End If

    rs.Close
    
    
    Exit Sub

TrataErro:
    Call TrataErroGeral("ObterValoresAvaliacaoRestituicao", Me.name)
    Call TerminaSEGBR

End Sub

Function VerNullNumerico(ByVal dado As Variant) As Variant
    If IsNull(dado) Then
        VerNullNumerico = 0
    ElseIf IsEmpty(dado) Or Trim(dado) = "" Then
        VerNullNumerico = 0
    Else
        VerNullNumerico = dado
    End If
End Function
'-----------------------------------------------
'Novo autorizar Restitui��o
'Renato Vasconcelos
Sub RegistrarAvaliacaoRestituicao(ByVal sSituacao As String)

' bcarneiro - 03/12/2003 - Projeto Restitui��o por Iniciativa da Seguradora

Dim oFinanceiro As Object

Dim cValPremioTarifa As Currency
Dim cValCustoApoliceCertif As Currency
Dim cValAdicFracionamento As Currency
Dim cValDesconto As Currency
Dim cValIOF As Currency
Dim cValRestituicao As Currency
Dim cValTotCorretagem As Currency
Dim cValProLabore As Currency
Dim cValSubvencaoFederal As Currency 'Rmarins - 20/10/06
Dim cValSubvencaoEstadual As Currency '

'MATHAYDE 27/03/2009 - C�lculo da remunera��o de do IR da remunera��o
Dim cValRemuneracao As Currency
Dim cValIrRemuneracao As Currency
Dim cPercIR As Currency
'''''''''''''''''''''''''''''''''



On Error GoTo TrataErro

    'Atualizando interface''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    
    MousePointer = vbHourglass
    
    'Obtendo valores opera��o''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    If bModular = False Then
        cValPremioTarifa = Val(TrocaVirgulaPorPonto(TiraPonto(FrmAvaliacao.Mask2ValPremioTarifa.Text)))
        cValCustoApoliceCertif = Val(TrocaVirgulaPorPonto(TiraPonto(FrmAvaliacao.LblCustoCertifApoliceRest.Caption)))
        cValAdicFracionamento = Val(TrocaVirgulaPorPonto(TiraPonto(FrmAvaliacao.LblValAdicFracionamento.Caption)))
        cValDesconto = Val(TrocaVirgulaPorPonto(TiraPonto(FrmAvaliacao.LblValDesconto.Caption)))
        cValIOF = Val(TrocaVirgulaPorPonto(TiraPonto(FrmAvaliacao.LblValIOF.Caption)))
        cValRestituicao = Val(TrocaVirgulaPorPonto(TiraPonto(FrmAvaliacao.LblValRestituicao.Caption)))
        cValTotCorretagem = Val(TrocaVirgulaPorPonto(TiraPonto(FrmAvaliacao.LblTotValCorretagem.Caption)))
        cValProLabore = Val(TrocaVirgulaPorPonto(TiraPonto(FrmAvaliacao.LblValProLabore.Caption)))
        cValSubvencaoFederal = Val(TrocaVirgulaPorPonto(TiraPonto(FrmAvaliacao.lblValSubvencaoFederal.Caption))) 'Rmarins - 20/10/06
        cValSubvencaoEstadual = Val(TrocaVirgulaPorPonto(TiraPonto(FrmAvaliacao.maskValSubvencaoEstadual.Text)))
        'MATHAYDE 27/03/2009
        cValRemuneracao = Val(TrocaVirgulaPorPonto(TiraPonto(FrmAvaliacao.LblValRemuneracao.Caption)))
        
        Set oFinanceiro = CreateObject("SEGL0026.cls00125")
    
        Call oFinanceiro.AtualizarAvaliacaoRestituicao(gsSIGLASISTEMA, _
                                                       App.Title, _
                                                       App.FileDescription, _
                                                       glAmbiente_id, _
                                                       cUserName, _
                                                       Data_Sistema, _
                                                       FrmAvaliacao.txtProposta.Text, _
                                                       FrmAvaliacao.TxtEndossoBB.Text, _
                                                       sSituacao, _
                                                       cValPremioTarifa, _
                                                       cValCustoApoliceCertif, _
                                                       cValAdicFracionamento, _
                                                       cValDesconto, _
                                                       cValIOF, _
                                                       cValRestituicao, _
                                                       cValTotCorretagem, _
                                                       cValProLabore, , , , _
                                                       restituicao_integral, _
                                                       gsPossuiSubvencao, _
                                                       cValSubvencaoFederal, _
                                                      cValSubvencaoEstadual, _
                                                       , cValRemuneracao)
    'Mathayde 30/03/2009 - Envia cValRemuneracai para gravar em avaliacao_restituicao_tb
             
    Set oFinanceiro = Nothing
    
    Else
    
        'Verificar se � ser tem q ser pego o calc mesmo'''''''''''''''''''
        cValPremioTarifa = Val(TrocaVirgulaPorPonto(TiraPonto(FrmAvaliacao.Mask2ValPremioTarifaModular(0).Text)))
        cValCustoApoliceCertif = Val(TrocaVirgulaPorPonto(TiraPonto(FrmAvaliacao.lblvalCalculadoModular(9).Caption)))
        cValAdicFracionamento = Val(TrocaVirgulaPorPonto(TiraPonto(FrmAvaliacao.lblvalCalculadoModular(10).Caption)))
        cValDesconto = Val(TrocaVirgulaPorPonto(TiraPonto(FrmAvaliacao.lblvalCalculadoModular(11).Caption)))
        cValIOF = Val(TrocaVirgulaPorPonto(TiraPonto(FrmAvaliacao.lblvalCalculadoModular(12).Caption)))
        cValRestituicao = Val(TrocaVirgulaPorPonto(TiraPonto(FrmAvaliacao.lblvalCalculadoModular(13).Caption)))
        cValTotCorretagem = Val(TrocaVirgulaPorPonto(TiraPonto(FrmAvaliacao.lblvalCalculadoModular(14).Caption)))
        cValProLabore = Val(TrocaVirgulaPorPonto(TiraPonto(lFrmAvaliacao.blvalCalculadoModular(15).Caption)))
        cValSubvencaoFederal = 0 'Val(TrocaVirgulaPorPonto(TiraPonto(lblValSubvencaoFederal.Caption))) 'Rmarins - 20/10/06
        cValSubvencaoEstadual = 0 'Val(TrocaVirgulaPorPonto(TiraPonto(maskValSubvencaoEstadual.Text)))
        'MATHAYDE 27/03/2009
        cValRemuneracao = Val(TrocaVirgulaPorPonto(TiraPonto(FrmAvaliacao.lblvalCalculadoModular(16).Caption)))
        'Registrando Avalia��o ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    
        Set oFinanceiro = CreateObject("SEGL0026.cls00125")
        
        Call oFinanceiro.AtualizarAvaliacaoRestituicao(gsSIGLASISTEMA, _
                                                       App.Title, _
                                                       App.FileDescription, _
                                                       glAmbiente_id, _
                                                       cUserName, _
                                                       Data_Sistema, _
                                                       FrmAvaliacao.txtProposta.Text, _
                                                       FrmAvaliacao.TxtEndossoBB.Text, _
                                                       sSituacao, _
                                                       cValPremioTarifa, _
                                                       cValCustoApoliceCertif, _
                                                       cValAdicFracionamento, _
                                                       cValDesconto, _
                                                       cValIOF, _
                                                       cValRestituicao, _
                                                       cValTotCorretagem, _
                                                       cValProLabore, , , , _
                                                       restituicao_integral, _
                                                       gsPossuiSubvencao, _
                                                       cValSubvencaoFederal, _
                                                       cValSubvencaoEstadual, _
                                                       , cValRemuneracao)
        'Mathayde 30/03/2009 - Envia cValRemuneracai para gravar em avaliacao_restituicao_tb
             
        Set oFinanceiro = Nothing
        
        cValPremioTarifa = Val(TrocaVirgulaPorPonto(TiraPonto(FrmAvaliacao.Mask2ValPremioTarifaModular(1).Text)))
        cValCustoApoliceCertif = Val(TrocaVirgulaPorPonto(TiraPonto(FrmAvaliacao.lblvalCalculadoModular(26).Caption)))
        cValAdicFracionamento = Val(TrocaVirgulaPorPonto(TiraPonto(FrmAvaliacao.lblvalCalculadoModular(27).Caption)))
        cValDesconto = Val(TrocaVirgulaPorPonto(TiraPonto(FrmAvaliacao.lblvalCalculadoModular(28).Caption)))
        cValIOF = Val(TrocaVirgulaPorPonto(TiraPonto(FrmAvaliacao.lblvalCalculadoModular(29).Caption)))
        cValRestituicao = Val(TrocaVirgulaPorPonto(TiraPonto(FrmAvaliacao.lblvalCalculadoModular(30).Caption)))
        cValTotCorretagem = Val(TrocaVirgulaPorPonto(TiraPonto(FrmAvaliacao.lblvalCalculadoModular(31).Caption)))
        cValProLabore = Val(TrocaVirgulaPorPonto(TiraPonto(FrmAvaliacao.lblvalCalculadoModular(32).Caption)))
        cValSubvencaoFederal = 0 'Val(TrocaVirgulaPorPonto(TiraPonto(lblValSubvencaoFederal.Caption))) 'Rmarins - 20/10/06
        cValSubvencaoEstadual = 0 'Val(TrocaVirgulaPorPonto(TiraPonto(maskValSubvencaoEstadual.Text)))
        'MATHAYDE 27/03/2009
        cValRemuneracao = Val(TrocaVirgulaPorPonto(TiraPonto(FrmAvaliacao.lblvalCalculadoModular(33).Caption)))
        'Registrando Avalia��o ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    
        Set oFinanceiro = CreateObject("SEGL0026.cls00125")
        
        Call oFinanceiro.AtualizarAvaliacaoRestituicao(gsSIGLASISTEMA, _
                                                       App.Title, _
                                                       App.FileDescription, _
                                                       glAmbiente_id, _
                                                       cUserName, _
                                                       Data_Sistema, _
                                                       oDadosModular.proposta_id, _
                                                       oDadosModular.Num_avaliacao, _
                                                       sSituacao, _
                                                       cValPremioTarifa, _
                                                       cValCustoApoliceCertif, _
                                                       cValAdicFracionamento, _
                                                       cValDesconto, _
                                                       cValIOF, _
                                                       cValRestituicao, _
                                                       cValTotCorretagem, _
                                                       cValProLabore, , , , _
                                                       restituicao_integral, _
                                                       gsPossuiSubvencao, _
                                                       cValSubvencaoFederal, _
                                                       cValSubvencaoEstadual, _
                                                       , cValRemuneracao)
        'Mathayde 30/03/2009 - Envia cValRemuneracai para gravar em avaliacao_restituicao_tb
             
        Set oFinanceiro = Nothing
    End If
    'Atualizando interface'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    
    MousePointer = vbDefault
    
    Exit Sub

TrataErro:
   Call TrataErroGeral("RegistrarAvaliacaoRestituicao", Me.name)
   Call TerminaSEGBR

End Sub
'Fim
'-----------------------------------------------


Sub AutorizarRestituicoes()
'Renato Vasconcelos

Dim Item As ListItem
Dim contador As Integer
Dim registrosAutorizados As Integer
Dim registrosEmUso As Integer
Dim numerosPropostas As String

'Const SITUACAO_AVALIACAO_TECNICA = "T"

On Error GoTo TrataErro

'For i = 1 To ListView1.ListItems.Count
'MsgBox ListView1.ListItems(i).Text
'Next i

    
    contador = 0
    registrosAutorizados = 0
    registrosEmUso = 0
    numerosPropostas = ""
    For i = 1 To ListSelEndosso.ListItems.Count

       'Set item = ListSelEndosso.ListItems(i)

        If ListSelEndosso.ListItems(i).Checked = True Then
            contador = contador + 1
            If Not Verificar_Uso_Proposta(ListSelEndosso.ListItems(i).SubItems(2)) Then
                registrosAutorizados = registrosAutorizados + 1
                Call ExibirDetalhesRestituicao(ListSelEndosso.ListItems(i), True)
                'Call ExibirDetalhesRestituicao(ListSelEndosso.SelectedItem, True)
                'Call FrmAvaliacao.AutorizarRestituicao(True)
                Call RegistrarAvaliacaoRestituicao("T")
                    'Obtendo dados da proposta'''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Else
                registrosEmUso = registrosEmUso + 1
                numerosPropostas = numerosPropostas & ListSelEndosso.ListItems(i).SubItems(2) & vbCrLf
            End If
        




       End If
    Next i
    'Loop

    'Atualizando interface'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

    MousePointer = vbNormal

    'Autorizar cancelamento''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    MsgBox "Opera��o conclu�da"
    Dim Mensagem As String
    
    Mensagem = contador & " Registro(s)" & vbCrLf & registrosAutorizados & " -  Autorizado(s)"
    If registrosEmUso > 0 Then
        Mensagem = Mensagem & vbCrLf & registrosEmUso & " Proposta(s) em uso por outro(s) usu�rio(s)!" & vbCrLf & "Propostas:" & vbCrLf & numerosPropostas
    End If
    
    MsgBox Mensagem, vbInformation, "AVISO"
    
    

    Call PrepararInformacoesRestituicao(CmbProduto.ItemData(CmbProduto.ListIndex))



    Exit Sub
'
TrataErro:
   Call TrataErroGeral("AutorizarRestituicao", Me.name)
   Call TerminaSEGBR

End Sub


Sub AprovarRestituicoes()

Dim Item As ListItem

Dim lPropostaId As Long
Dim lProdutoId As Long
Dim lNumAvaliacao As Long
Dim sDtContratacao As String
Dim sDtInicioVig As String
Dim sDtFimVig As String
Dim sDtPedidoAvaliacao As String

'RSilva
Dim sSQL As String
Dim rs As rdoResultset

bModular = False

On Error GoTo TrataErro

    For i = 1 To ListSelEndosso.ListItems.Count
        
        Set Item = ListSelEndosso.ListItems(i)
       
        If Item.Checked = True Then
        
        
        
            'Obtendo dados da proposta'''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            
            lPropostaId = Item.SubItems(2)
            lNumAvaliacao = Item.SubItems(4)
            sDtPedidoAvaliacao = Item.SubItems(5)
            lProdutoId = Item.SubItems(6)
            sDtContratacao = Item.SubItems(8)
            sDtInicioVig = Item.SubItems(9)
            sDtFimVig = Item.SubItems(10)
                        
            Call Obter_Dados_Produto(lProdutoId, Null, Null, gsPossuiSubvencao)   'Rmarins - 20/10/06

            If lProdutoId = 1168 Then
                sSQL = ""
                adSQL sSQL, "SELECT num_avaliacao "
                adSQL sSQL, "  FROM avaliacao_restituicao_tb with(NOLOCK)"
                adSQL sSQL, " WHERE proposta_id = " & lPropostaId
                adSQL sSQL, "   AND num_avaliacao <> " & lNumAvaliacao
                If TpOperacao = 2 Then
                    adSQL sSQL, "   AND dt_avaliacao_gerencial IS NULL"
                End If
                
                Set rs = rdocn.OpenResultset(sSQL)
                
                If Not rs.EOF Then
                    oDadosModular.proposta_id = lPropostaId
                    oDadosModular.Num_avaliacao = rs(0)
                End If
                
            End If

            'Aprovando restitui��o ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                        
            Call AprovarRestituicao(lPropostaId, _
                                    lNumAvaliacao, _
                                    lProdutoId, _
                                    sDtContratacao, _
                                    sDtInicioVig, _
                                    sDtFimVig, _
                                    sDtPedidoAvaliacao)
    
        End If
    
    Next
    
    MsgBox "Opera��o conclu�da"

    Call PrepararInformacoesRestituicao(CmbProduto.ItemData(CmbProduto.ListIndex))

    Exit Sub

TrataErro:
    Call TrataErroGeral("AprovarRestituicoes", Me.name)
    Call TerminaSEGBR
    
End Sub
Private Sub ConsultarPercIOFModular(ByVal dtIOF As String, ByVal Subramo_id As Integer)
Dim oFinanceiro As Object
'RSILVA
On Error GoTo TrataErro

    If IsDate(dtIOF) Then
        dtIOF = Format(dtIOF, "yyyymmdd")
    End If

    Set oFinanceiro = CreateObject("SEGL0026.cls00125")

    oDadosModular.PercIOF = CDbl(oFinanceiro.ObterPercentualItemFinanceiro(cUserName, _
                                                                           gsSIGLASISTEMA, _
                                                                           App.Title, _
                                                                           App.FileDescription, _
                                                                           glAmbiente_id, _
                                                                           "IOF", _
                                                                           oDadosModular.ramo_id, _
                                                                           Data_Sistema, _
                                                                           dtIOF, _
                                                                           Subramo_id))

    Set oFinanceiro = Nothing

    Exit Sub

TrataErro:
    Call TratarErro("ConsultarPercIOF", Me.name)
    Call FinalizarAplicacao

End Sub
Private Sub ConsultarPercIOF(ByVal dtIOF As String, ByVal Subramo_id As Integer)
Dim oFinanceiro As Object
'RSILVA
On Error GoTo TrataErro

    If IsDate(dtIOF) Then
        dtIOF = Format(dtIOF, "yyyymmdd")
    End If

    Set oFinanceiro = CreateObject("SEGL0026.cls00125")

    FrmAvaliacao.dPercIOF = CDbl(oFinanceiro.ObterPercentualItemFinanceiro(cUserName, _
                                                                           gsSIGLASISTEMA, _
                                                                           App.Title, _
                                                                           App.FileDescription, _
                                                                           glAmbiente_id, _
                                                                           "IOF", _
                                                                           oDadosModular.ramo_id, _
                                                                           Data_Sistema, _
                                                                           dtIOF, _
                                                                           Subramo_id))

    Set oFinanceiro = Nothing

    Exit Sub

TrataErro:
    Call TratarErro("ConsultarPercIOF", Me.name)
    Call FinalizarAplicacao

End Sub
Private Sub ConsultarSubRamoModular(ByVal lPropostaId As Long)

Dim oApolice As Object
Dim rs As Recordset

Dim sTpEmissao As String

On Error GoTo TrataErro

    Set oApolice = CreateObject("SEGL0026.cls00126")

    Set rs = oApolice.ConsultarDados(gsSIGLASISTEMA, _
                                     App.Title, _
                                     App.FileDescription, _
                                     glAmbiente_id, _
                                     lPropostaId)
    
    Set oApolice = Nothing
    
    If Not rs.EOF Then
        oDadosModular.SubRamoId = rs("subramo_id")
    Else
        oDadosModular.SubRamoId = 0
    End If
    
    Set rs = Nothing
    
    Exit Sub

TrataErro:
    Call TratarErro("ConsultarSubRamoModular", Me.name)
    Call FinalizarAplicacao

End Sub
Private Sub ConsultarSubRamo(ByVal lPropostaId As Long)

Dim oApolice As Object
Dim rs As Recordset

Dim sTpEmissao As String

On Error GoTo TrataErro

    Set oApolice = CreateObject("SEGL0026.cls00126")

    Set rs = oApolice.ConsultarDados(gsSIGLASISTEMA, _
                                     App.Title, _
                                     App.FileDescription, _
                                     glAmbiente_id, _
                                     lPropostaId)
    
    Set oApolice = Nothing
    
    If Not rs.EOF Then
        FrmAvaliacao.txtSubramoId = rs("subramo_id")
    Else
        FrmAvaliacao.txtSubramoId = 0
    End If
    
    Set rs = Nothing
    
    Exit Sub

TrataErro:
    Call TratarErro("ConsultarSubRamoModular", Me.name)
    Call FinalizarAplicacao

End Sub
Sub DadosModular(ByVal lPropostaId As Long, ByVal lPropostaModular As Long)
    Dim oFinanceiro As Object
    
    bModular = True

    If Produto_id = 1168 Then
        lPropostaModular = lPropostaId
        lAvaliacao = lAvaliacaoModular
    End If

    oDadosModular.proposta_id = lPropostaModular
    ''executar todas as fun��es para proposta modular

    Set oFinanceiro = CreateObject("SEGL0026.cls00125")
    Set rs = oFinanceiro.ConsultarAvaliacaoRestituicao(gsSIGLASISTEMA, _
                                                       App.Title, _
                                                       App.FileDescription, _
                                                       glAmbiente_id, _
                                                       IIf(TpOperacao = 1, SITUACAO_PENDENTE_AVALIACAO_TECNICA, SITUACAO_PENDENTE_AVALIACAO_GERENCIAL), _
                                                       IIf(Produto_id = 1168, 1168, 0), lPropostaModular, IIf(Produto_id = 1168, lAvaliacao, 0))


    If Not rs.EOF Then
        oDadosModular.proposta_bb = rs("proposta_bb")
        oDadosModular.Num_avaliacao = rs("num_avaliacao")
        oDadosModular.Dt_Pedido_Avaliacao = Format(rs("dt_pedido_avaliacao"), "dd/mm/yyyy")
        oDadosModular.Produto_id = rs("produto_id")
        oDadosModular.Dt_Contratacao = rs("dt_contratacao")
        oDadosModular.Dt_Inicio_Vigencia = rs("dt_inicio_vigencia")
        oDadosModular.Dt_Fim_Vigencia = rs("dt_fim_vigencia")
        oDadosModular.Origem = rs("origem")
        oDadosModular.Nome_Tp_Endosso = rs("nome_tp_endosso")

        If Produto_id = 1168 Then
            If lAvaliacao > rs("num_avaliacao") Then
                oDadosModular.iTpCobertura = 526
            Else
                oDadosModular.iTpCobertura = 527
            End If
        End If
        
        Call Obter_Dados_Adicionais_Proposta_Modular(lPropostaModular, _
                                                     oDadosModular.ramo_id, _
                                                     oDadosModular.Custo_Apolice)
        
        Call Obter_Premio_IS(lPropostaModular, rs("produto_id"), _
                                                   oDadosModular.Val_IS, _
                                                   oDadosModular.Val_Premio)

        Call Carregar_Colecao_Cobrancas(lPropostaModular)

        'rsilva - 02/09/2009
        'Nova regra para o calculo do premio a ser restituido
        Call ConsultarSubRamoModular(lPropostaModular)

        Call ConsultarPercIOFModular(oDadosModular.Dt_Inicio_Vigencia, _
                                     oDadosModular.SubRamoId)
        
        Call Obter_Totais_Pagos(oDadosModular.Val_Premio_Pago, _
                                oDadosModular.Val_Juros, _
                                oDadosModular.Val_Adic_Fracionamento, _
                                oDadosModular.Val_Desconto, _
                                oDadosModular.Val_IOF_Restituicao, True)
        
        Call ObterValoresAvaliacaoRestituicaoModular(lPropostaModular, _
                                                     oDadosModular.Num_avaliacao)

        Call Obtem_Perc_IRModular(oDadosModular.ramo_id, _
                                  oDadosModular.Dt_Inicio_Vigencia, _
                                  0)

        Call CarregarRateioCorretagemRestituicao(lPropostaModular, _
                                                 oDadosModular.Num_avaliacao, _
                                                 oDadosModular.ValRestituicao, _
                                                 oDadosModular.ValIOFRest, _
                                                 oDadosModular.ValCustoApoliceCertifRest, _
                                                 oDadosModular.PercIR)


        oDadosModular.PercCorretagemRest = ObterPercentualTotalCorretagem(lPropostaModular)

        Else
            bModular = False

       End If
End Sub

Private Function InvNumber(ByVal Number As String) As String
'Criado por:    Cinthia Monteiro
'Data:          26/04/2011
'Demanda:       9064130
'Objetivo:      Ordenar os numeros que est�o formatados como string

Static i As Integer

For i = 1 To Len(Number)
    Select Case Mid$(Number, i, 1)
        Case "-": Mid$(Number, i, 1) = " "
        Case "0": Mid$(Number, i, 1) = "9"
        Case "1": Mid$(Number, i, 1) = "8"
        Case "2": Mid$(Number, i, 1) = "7"
        Case "3": Mid$(Number, i, 1) = "6"
        Case "4": Mid$(Number, i, 1) = "5"
        Case "5": Mid$(Number, i, 1) = "4"
        Case "6": Mid$(Number, i, 1) = "3"
        Case "7": Mid$(Number, i, 1) = "2"
        Case "8": Mid$(Number, i, 1) = "1"
        Case "9": Mid$(Number, i, 1) = "0"
    End Select
    
Next

InvNumber = Number

End Function

Private Sub SortListView(ListView As ListView, ByVal Index As Integer, ByVal DataType As String, ByVal Ascending As Boolean)
'Criado por:    Cinthia Monteiro
'Data:          26/04/2011
'Demanda:       9064130
'Objetivo:      Ordenar uma listview pelo tipo de Dados: N = Numeros, T = Texto, D = Data

    On Error Resume Next
    Dim i As Integer
    Dim l As Long
    Dim strFormat As String
    Dim lngCursor As Long
    lngCursor = ListView.MousePointer
    ListView.MousePointer = vbHourglass
    LockWindowUpdate ListView.hwnd
    
    Dim blnRestoreFromTag As Boolean
    
    Select Case DataType
    Case "T"
        blnRestoreFromTag = False
        
    Case "N"
        strFormat = String$(20, "0") & "." & String$(10, "0")
        With ListView.ListItems
            If (Index = 1) Then
                For l = 1 To .Count
                    With .Item(l)
                        .Tag = .Text & Chr$(0) & .Tag
                        If IsNumeric(.Text) Then
                            If CDbl(.Text) >= 0 Then
                                .Text = Format(CDbl(.Text), strFormat)
                            Else
                                .Text = "&" & InvNumber(Format(0 - CDbl(.Text), strFormat))
                            End If
                        Else
                            .Text = ""
                        End If
                    End With
                Next l
            Else
                For l = 1 To .Count
                    With .Item(l).ListSubItems(Index - 1)
                        .Tag = .Text & Chr$(0) & .Tag
                        If IsNumeric(.Text) Then
                            If CDbl(.Text) >= 0 Then
                                .Text = Format(CDbl(.Text), strFormat)
                            Else
                                .Text = "&" & InvNumber(Format(0 - CDbl(.Text), strFormat))
                            End If
                        Else
                            .Text = ""
                        End If
                    End With
                Next l
            End If
        End With
        
        blnRestoreFromTag = True
    
    Case "D"
        strFormat = "YYYYMMDDHhNnSs"
        
        Dim dte As Date
       
        With ListView.ListItems
            If (Index = 1) Then
                For l = 1 To .Count
                    With .Item(l)
                        .Tag = .Text & Chr$(0) & .Tag
                        dte = CDate(.Text)
                        .Text = Format$(dte, strFormat)
                    End With
                Next l
            Else
                For l = 1 To .Count
                    With .Item(l).ListSubItems(Index - 1)
                        .Tag = .Text & Chr$(0) & .Tag
                        dte = CDate(.Text)
                        .Text = Format$(dte, strFormat)
                    End With
                Next l
            End If
        End With
        
        blnRestoreFromTag = True
        
    End Select
       
    ListView.SortOrder = IIf(Ascending, lvwAscending, lvwDescending)
    ListView.SortKey = Index - 1
    ListView.Sorted = True
        
    If blnRestoreFromTag Then
                
        With ListView.ListItems
            If (Index = 1) Then
                For l = 1 To .Count
                    With .Item(l)
                        i = InStr(.Tag, Chr$(0))
                        .Text = Left$(.Tag, i - 1)
                        .Tag = Mid$(.Tag, i + 1)
                    End With
                Next l
            Else
                For l = 1 To .Count
                    With .Item(l).ListSubItems(Index - 1)
                        i = InStr(.Tag, Chr$(0))
                        .Text = Left$(.Tag, i - 1)
                        .Tag = Mid$(.Tag, i + 1)
                    End With
                Next l
            End If
        End With
    End If
        
    LockWindowUpdate 0&
        
    ListView.MousePointer = lngCursor

End Sub

Private Sub txtQtdeLista_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        If CmbProduto.ListIndex < 0 Then Exit Sub
        ListSelEndosso.SetFocus
        Exit Sub
    End If
    If KeyAscii = 46 Then KeyAscii = 44
    If KeyAscii = 44 And InStr(txtQtdeLista.Text, ",") <> 0 Then
        KeyAscii = 0
        Exit Sub
    End If
    If (KeyAscii < 48 Or KeyAscii > 57) And KeyAscii <> 8 And KeyAscii <> 44 Then
        KeyAscii = 0
    End If
End Sub

Private Sub txtQtdeLista_LostFocus()
    If CmbProduto.ListIndex < 0 Then Exit Sub
    Call PrepararInformacoesRestituicao(CmbProduto.ItemData(CmbProduto.ListIndex))
End Sub
