VERSION 5.00
Object = "{C932BA88-4374-101B-A56C-00AA003668DC}#1.1#0"; "MSMASK32.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "mscomctl.OCX"
Begin VB.Form frmAnalitico 
   Caption         =   "Form1"
   ClientHeight    =   7875
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   11250
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   ScaleHeight     =   7875
   ScaleWidth      =   11250
   Begin VB.Frame Frame1 
      Caption         =   "Dados do Produto"
      Height          =   1275
      Left            =   120
      TabIndex        =   4
      Top             =   0
      Width           =   11055
      Begin MSMask.MaskEdBox txtPesquisa 
         Height          =   315
         Left            =   4560
         TabIndex        =   18
         Top             =   840
         Width           =   4455
         _ExtentX        =   7858
         _ExtentY        =   556
         _Version        =   393216
         PromptChar      =   "_"
      End
      Begin VB.ComboBox cboPesquisa 
         Enabled         =   0   'False
         Height          =   315
         ItemData        =   "frmAnalitico.frx":0000
         Left            =   2280
         List            =   "frmAnalitico.frx":0023
         TabIndex        =   17
         Top             =   840
         Width           =   2175
      End
      Begin VB.CommandButton cmdPesquisar 
         Caption         =   "&Pesquisar"
         Enabled         =   0   'False
         Height          =   315
         Left            =   9120
         TabIndex        =   16
         Top             =   840
         Width           =   1815
      End
      Begin VB.CheckBox chkFiltro 
         Caption         =   "Filtro Personalizado"
         Height          =   255
         Left            =   240
         TabIndex        =   14
         Top             =   900
         Width           =   1815
      End
      Begin VB.CheckBox chkSelecionar 
         Caption         =   "Selecionar todos"
         Height          =   255
         Left            =   240
         TabIndex        =   5
         Top             =   610
         Width           =   1815
      End
      Begin VB.Label lblConteudo 
         Caption         =   "Conte�do"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   4560
         TabIndex        =   15
         Top             =   610
         Width           =   2175
      End
      Begin VB.Label lblFiltro 
         Caption         =   "Tipo de Sele��o"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   2280
         TabIndex        =   13
         Top             =   610
         Width           =   1575
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Produto:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   240
         TabIndex        =   7
         Top             =   300
         Width           =   735
      End
      Begin VB.Label lblProduto 
         AutoSize        =   -1  'True
         Caption         =   "Label2"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   1080
         TabIndex        =   6
         Top             =   300
         Width           =   585
      End
   End
   Begin VB.PictureBox Picture1 
      Height          =   1335
      Left            =   120
      ScaleHeight     =   1275
      ScaleWidth      =   10965
      TabIndex        =   0
      Top             =   6600
      Width           =   11025
      Begin VB.CommandButton btnRecusa 
         Caption         =   "Recusa"
         Height          =   375
         Left            =   3120
         TabIndex        =   19
         Top             =   720
         Width           =   2055
      End
      Begin VB.Frame FrameLegenda 
         Caption         =   "Legenda"
         Height          =   1155
         Left            =   0
         TabIndex        =   10
         Top             =   0
         Width           =   2895
         Begin VB.Image Image1 
            Height          =   495
            Left            =   0
            Picture         =   "frmAnalitico.frx":00CA
            Stretch         =   -1  'True
            Top             =   240
            Width           =   465
         End
         Begin VB.Image Image2 
            Height          =   375
            Left            =   30
            Picture         =   "frmAnalitico.frx":03D4
            Stretch         =   -1  'True
            Top             =   720
            Width           =   405
         End
         Begin VB.Label Label2 
            Caption         =   "Com Informa��o de Baixa"
            Height          =   255
            Left            =   570
            TabIndex        =   12
            Top             =   360
            Width           =   1965
         End
         Begin VB.Label Label3 
            Caption         =   "Sem Informa��o de Baixa"
            Height          =   255
            Left            =   570
            TabIndex        =   11
            Top             =   810
            Width           =   1965
         End
      End
      Begin VB.CommandButton btnExportar 
         Caption         =   "Exportar para o Excel"
         Height          =   375
         Left            =   5280
         TabIndex        =   3
         Top             =   720
         Width           =   2055
      End
      Begin VB.CommandButton btnSair 
         Caption         =   "Sair"
         Height          =   375
         Left            =   9240
         TabIndex        =   2
         Top             =   720
         Width           =   1665
      End
      Begin VB.CommandButton btnAprovar 
         Caption         =   "Aprovar"
         Height          =   375
         Left            =   7440
         TabIndex        =   1
         Top             =   720
         Width           =   1665
      End
      Begin MSComctlLib.ImageList ImageList1 
         Left            =   0
         Top             =   0
         _ExtentX        =   1005
         _ExtentY        =   1005
         BackColor       =   -2147483643
         ImageWidth      =   16
         ImageHeight     =   16
         MaskColor       =   12632256
         _Version        =   393216
         BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
            NumListImages   =   2
            BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmAnalitico.frx":06DE
               Key             =   ""
            EndProperty
            BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmAnalitico.frx":09F8
               Key             =   ""
            EndProperty
         EndProperty
      End
   End
   Begin MSComctlLib.ListView ListSelEndosso 
      Height          =   5295
      Left            =   120
      TabIndex        =   8
      Top             =   1320
      Width           =   11025
      _ExtentX        =   19447
      _ExtentY        =   9340
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      Checkboxes      =   -1  'True
      _Version        =   393217
      SmallIcons      =   "ImageList1"
      ForeColor       =   -2147483646
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      NumItems        =   0
   End
   Begin MSComctlLib.StatusBar StatusBar1 
      Align           =   2  'Align Bottom
      Height          =   315
      Left            =   0
      TabIndex        =   9
      Top             =   7560
      Width           =   11250
      _ExtentX        =   19844
      _ExtentY        =   556
      Style           =   1
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmAnalitico"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim OrdAsc As Boolean
Public strWhere As String
Public idProduto As Integer
Public inicial As Double
Public intervalo As Double
Public maiorQue As Double
Public QTD As Long
Public lAvaliacaoModular As Long
Public sDtInicioEsc As String
Public sDtFimEsc As String
Public sProduto  As String  'Alan Neves - C_ANEVES - MP: 17860335
Public Proposta As String
Public NumAvaliacao As String
Public ValRemuneracao As String
Public Endosso As String
Private Declare Function LockWindowUpdate Lib "user32" (ByVal hwndLock As Long) As Long


Private Sub BtnAprovar_Click()
    Dim sSQL As String
    Dim selecionado As Boolean
    selecionado = False
    Dim strPropostas As String
    Dim strNumAvaliacao As String
    Dim aprovado As Boolean
    
    Dim X As Long
    
    
    For X = 1 To ListSelEndosso.ListItems.Count
        Set Item = ListSelEndosso.ListItems(X)
        If Item.Checked = True Then
             selecionado = True
             strPropostas = strPropostas & Item.SubItems(2) & ","
             strNumAvaliacao = strNumAvaliacao & Item.SubItems(4) & ","
                
             If Item.SubItems(22) = "S" Then
                strPropostas = strPropostas & Item.SubItems(23) & ","
                strNumAvaliacao = strNumAvaliacao & Item.SubItems(24) & ","
             End If
            
        End If
    Next
    If Not selecionado Then
        MsgBox "Selecione pelo menos uma proposta!", vbInformation
        Call Unload(FrmEspera)
        Exit Sub
    End If
    strPropostas = Left(strPropostas, Len(strPropostas) - 1)
    strNumAvaliacao = Left(strNumAvaliacao, Len(strNumAvaliacao) - 1)
        
    If TpOperacao = 1 Then
        Call aprovacao_tecnica(strPropostas, strNumAvaliacao, 0, "")
        aprovado = True
    Else
        aprovado = aprovacao_gerencial(strPropostas, strNumAvaliacao, idProduto, "")
        
    End If
    
    If Not aprovado Then
        Exit Sub
    End If
    
    
    Call Unload(Me)
    
    frmAnalitico.idProduto = idProduto
    frmAnalitico.inicial = inicial
    frmAnalitico.intervalo = intervalo
    frmAnalitico.maiorQue = maiorQue
    frmAnalitico.strWhere = strWhere
    frmAnalitico.QTD = QTD
    frmAnalitico.sProduto = sProduto
    Call frmAnalitico.carregarForm
    frmAnalitico.Show
    
    
    If ListSelEndosso.ListItems.Count = 0 Then
        Unload Me
    End If
    
    
        
End Sub

'Alan Neves - C_ANEVES - Demanda: 17860335 - 03/06/2013 - Inicio
'Private Sub btnExportar_Click()
'
'    Dim sSQL As String
'    Dim selecionado As Boolean
'    selecionado = False
'    Dim strPropostas
'
'    strPropostas = " and proposta_id IN("
'    Dim X As Long
'    For X = 1 To ListSelEndosso.ListItems.Count
'        Set Item = ListSelEndosso.ListItems(X)
'        If Item.Checked = True Then
'             selecionado = True
'             strPropostas = strPropostas & Item.SubItems(2) & ", "
'        End If
'    Next
'    If Not selecionado Then
'        MsgBox "Selecione pelo menos uma restitui��o!", vbInformation
'        Exit Sub
'    End If
'    strPropostas = Left(strPropostas, Len(strPropostas) - 2)
'    strPropostas = strPropostas & ")"
'
'    FrmEspera.Label1 = "Aguarde, Exportando para o Excel..."
'    FrmEspera.Show
'
'
'   'atualizando dados proposta
'    Call AtualizaDadosRestituicao(0, idProduto)
'
'    sSQL = "select * from #avaliacao_restituicao_tb where produto_id = " & CStr(idProduto) & strPropostas
'    Dim rc As rdoResultset
'    Set rc = rdocn.OpenResultset(sSQL)
'
'
'    Dim xlApp As Object
'    Dim xlWb As Object
'    Dim xlWs As Object
'
'    Set xlApp = CreateObject("Excel.Application")
'    Set xlWb = xlApp.Workbooks.Add
'    Set xlWs = xlWb.Worksheets(1)
'
'    xlApp.Visible = False
'    xlApp.UserControl = True
'
'    Dim i As Long
'    i = 1
'
'    xlWs.Cells(i, 1).Value = "Proposta BB"
'    xlWs.Cells(i, 1).ColumnWidth = 15
'    xlWs.Cells(i, 2).Value = "Proposta AB"
'    xlWs.Cells(i, 2).ColumnWidth = 15
'    xlWs.Cells(i, 3).Value = "Valor de Restitui��o"
'    xlWs.Cells(i, 3).ColumnWidth = 20
'    xlWs.Cells(i, 4).Value = "Nr. Avalia��o"
'    xlWs.Cells(i, 4).ColumnWidth = 15
'    xlWs.Cells(i, 5).Value = "Dt. Pedido da Avalia��o"
'    xlWs.Cells(i, 5).ColumnWidth = 23
'    xlWs.Cells(i, 6).Value = "Produto"
'    xlWs.Cells(i, 6).ColumnWidth = 15
'    xlWs.Cells(i, 7).Value = "Cliente"
'    xlWs.Cells(i, 7).ColumnWidth = 35
'    xlWs.Cells(i, 8).Value = "Dt. Contrata��o"
'    xlWs.Cells(i, 8).ColumnWidth = 15
'    xlWs.Cells(i, 9).Value = "Tipo de Avalia��o"
'    xlWs.Cells(i, 9).ColumnWidth = 40
'    xlWs.Cells(i, 10).Value = "Valor Subven��o Federal"
'    xlWs.Cells(i, 10).ColumnWidth = 25
'    xlWs.Cells(i, 11).Value = "Valor Subven��o Estadual"
'    xlWs.Cells(i, 11).ColumnWidth = 25
'    xlWs.Cells(i, 12).Value = "In�cio de Vig�ncia"
'    xlWs.Cells(i, 12).ColumnWidth = 18
'    xlWs.Cells(i, 13).Value = "Fim de Vig�ncia"
'    xlWs.Cells(i, 13).ColumnWidth = 15
'    xlWs.Cells(i, 14).Value = "IS (Import�ncia Segurada)"
'    xlWs.Cells(i, 14).ColumnWidth = 27
'    xlWs.Cells(i, 15).Value = "Valor Pr�mio Pago"
'    xlWs.Cells(i, 15).ColumnWidth = 20
'    xlWs.Cells(i, 16).Value = "Valor Pr�mio Retido"
'    xlWs.Cells(i, 16).ColumnWidth = 20
'    xlWs.Cells(i, 17).Value = "Data de Emiss�o do Endosso"
'    xlWs.Cells(i, 17).ColumnWidth = 30
'
'    xlWs.Cells(i, 18).Value = "Valor Comiss�o"
'    xlWs.Cells(i, 18).ColumnWidth = 15
'    xlWs.Cells(i, 19).Value = "Valor Remunera��o"
'    xlWs.Cells(i, 19).ColumnWidth = 18
'
'    Dim j As Long
'
'    For j = 1 To 19
'        xlWs.Cells(i, j).Font.Bold = True
'    Next
'
'
'   'Dim Item As ListItem
'    Dim Y As Long
'
'
'    Do While Not rc.EOF
'        i = i + 1
'        xlWs.Cells(i, 1).Value = rc("proposta_bb")
'        xlWs.Cells(i, 2).Value = rc("proposta_id")
'        xlWs.Cells(i, 3).Value = IIf(Not IsNull(rc("val_restituicao_estimado")), CDbl(Replace(rc("val_restituicao_estimado"), ".", ",")), 0)
'        xlWs.Cells(i, 3).NumberFormat = "#,##0.00"
'
'        xlWs.Cells(i, 4).Value = rc("num_avaliacao")
'
'        If IsDate(rc("dt_pedido_avaliacao")) Then
'            xlWs.Cells(i, 5).Value = CDate(rc("dt_pedido_avaliacao"))
'        Else
'            xlWs.Cells(i, 5).Value = ""
'        End If
'        xlWs.Cells(i, 6).Value = rc("produto_id")
'        xlWs.Cells(i, 7).Value = rc("Cliente")
'
'        If IsDate(rc("DT_Contratacao")) Then
'            xlWs.Cells(i, 8).Value = CDate(rc("DT_Contratacao"))
'        Else
'            xlWs.Cells(i, 8).Value = ""
'        End If
'
'        xlWs.Cells(i, 9).Value = rc("TipoAvaliacao")
'
'        If Not IsNull(rc("val_subvencao_federal_estimado")) Then
'            xlWs.Cells(i, 10).Value = CDbl(Replace(rc("val_subvencao_federal_estimado"), ".", ","))
'        Else
'            xlWs.Cells(i, 10).Value = 0
'        End If
'        xlWs.Cells(i, 10).NumberFormat = "#,##0.00"
'
'        If Not IsNull(rc("val_subvencao_estadual_estimado")) Then
'            xlWs.Cells(i, 11).Value = CDbl(Replace(rc("val_subvencao_estadual_estimado"), ".", ","))
'        Else
'            xlWs.Cells(i, 11).Value = 0
'        End If
'        xlWs.Cells(i, 11).NumberFormat = "#,##0.00"
'
'        If IsDate(rc("dt_inicio_vigencia")) Then
'            xlWs.Cells(i, 12).Value = CDate(rc("dt_inicio_vigencia"))
'        Else
'            xlWs.Cells(i, 12).Value = ""
'        End If
'
'        If IsDate(rc("dt_fim_vigencia")) Then
'            xlWs.Cells(i, 13).Value = CDate(rc("dt_fim_vigencia"))
'        Else
'            xlWs.Cells(i, 13).Value = ""
'        End If
'
'        If Not IsNull(rc("valis")) Then
'            xlWs.Cells(i, 14).Value = CDbl(Replace(rc("ValIS"), ".", ","))
'        Else
'            xlWs.Cells(i, 14).Value = 0
'        End If
'        xlWs.Cells(i, 14).NumberFormat = "#,##0.00"
'
'        If Not IsNull(rc("PremioPago")) Then
'            xlWs.Cells(i, 15).Value = CDbl(Replace(rc("PremioPago"), ".", ","))
'        Else
'            xlWs.Cells(i, 15).Value = 0
'        End If
'        xlWs.Cells(i, 15).NumberFormat = "#,##0.00"
'
'        If Not IsNull(rc("PremioRetido")) Then
'            xlWs.Cells(i, 16).Value = CDbl(Replace(rc("PremioRetido"), ".", ","))
'        Else
'            xlWs.Cells(i, 16).Value = 0
'        End If
'        xlWs.Cells(i, 16).NumberFormat = "#,##0.00"
'
'
'        If IsDate(rc("dt_emissao")) Then
'            xlWs.Cells(i, 17).Value = CDate(rc("dt_emissao"))
'        Else
'            xlWs.Cells(i, 17).Value = ""
'        End If
'
'
'        If Not IsNull(rc("Val_comissao_estimado")) Then
'            xlWs.Cells(i, 18).Value = CDbl(Replace(rc("Val_comissao_estimado"), ".", ","))
'        Else
'            xlWs.Cells(i, 18).Value = 0
'        End If
'
'        xlWs.Cells(i, 18).NumberFormat = "#,##0.00"
'        If Not IsNull(rc("val_restituicao_estimado")) Then
'            xlWs.Cells(i, 19).Value = CDbl(Replace(rc("val_restituicao_estimado"), ".", ","))
'        Else
'            xlWs.Cells(i, 19).Value = 0
'        End If
'        xlWs.Cells(i, 19).NumberFormat = "#,##0.00"
'
'
'
'        rc.MoveNext
'    Loop
'
'    Call Unload(FrmEspera)
'    xlApp.Visible = True
'
'
'End Sub


Private Sub btnExportar_Click()
    
    Dim sSQL As String
    Dim selecionado As Boolean
    selecionado = False
    Dim strPropostas
    
    strPropostas = " and proposta_id IN("
    Dim X As Long
    For X = 1 To ListSelEndosso.ListItems.Count
        Set Item = ListSelEndosso.ListItems(X)
        If Item.Checked = True Then
             selecionado = True
             strPropostas = strPropostas & Item.SubItems(2) & ", "
        End If
    Next
    If Not selecionado Then
        MsgBox "Selecione pelo menos uma restitui��o!", vbInformation
        Exit Sub
    End If
    strPropostas = Left(strPropostas, Len(strPropostas) - 2)
    strPropostas = strPropostas & ")"
    
    FrmEspera.Label1 = "Aguarde, Exportando para o Excel..."
    FrmEspera.Show
    
   
   'atualizando dados proposta
    Call AtualizaDadosRestituicao(0, idProduto)
    
    sSQL = "select * from #avaliacao_restituicao_tb where produto_id = " & CStr(idProduto) & strPropostas
    Dim rc As rdoResultset
    Set rc = rdocn.OpenResultset(sSQL)
    
    
    Dim xlApp As Object
    Dim xlWb As Object
    Dim xlWs As Object
    
    Set xlApp = CreateObject("Excel.Application")
    Set xlWb = xlApp.Workbooks.Add
    Set xlWs = xlWb.Worksheets(1)
    
    xlApp.Visible = False
    xlApp.UserControl = True
    
    Dim i As Long
    i = 1
    
    xlWs.Cells(i, 1).Value = "Proposta BB"
    xlWs.Cells(i, 1).ColumnWidth = 15
    xlWs.Cells(i, 2).Value = "Proposta AB"
    xlWs.Cells(i, 2).ColumnWidth = 15
    xlWs.Cells(i, 3).Value = "Valor de Restitui��o"
    xlWs.Cells(i, 3).ColumnWidth = 20
    xlWs.Cells(i, 4).Value = "Nr. Avalia��o"
    xlWs.Cells(i, 4).ColumnWidth = 15
    xlWs.Cells(i, 5).Value = "Dt. Pedido da Avalia��o"
    xlWs.Cells(i, 5).ColumnWidth = 23
    xlWs.Cells(i, 6).Value = "Produto"
    xlWs.Cells(i, 6).ColumnWidth = 15
    ' MU-2017-054666 - Rog�rio Melo - Confitec SP
    '13/03/2018 nova coluna do excel
    xlWs.Cells(i, 7).Value = "Usu�rio"
    xlWs.Cells(i, 7).ColumnWidth = 35
    ' MU-2017-054666 - Rog�rio Melo - Confitec SP
    '13/03/2018 nova coluna do excel
    xlWs.Cells(i, 8).Value = "Cliente"
    xlWs.Cells(i, 8).ColumnWidth = 35
    xlWs.Cells(i, 9).Value = "Dt. Contrata��o"
    xlWs.Cells(i, 9).ColumnWidth = 15
    xlWs.Cells(i, 10).Value = "Tipo de Avalia��o"
    xlWs.Cells(i, 10).ColumnWidth = 40
    xlWs.Cells(i, 11).Value = "Valor Subven��o Federal"
    xlWs.Cells(i, 11).ColumnWidth = 25
    xlWs.Cells(i, 12).Value = "Valor Subven��o Estadual"
    xlWs.Cells(i, 12).ColumnWidth = 25
    xlWs.Cells(i, 13).Value = "In�cio de Vig�ncia"
    xlWs.Cells(i, 13).ColumnWidth = 18
    xlWs.Cells(i, 14).Value = "Fim de Vig�ncia"
    xlWs.Cells(i, 14).ColumnWidth = 15
    xlWs.Cells(i, 15).Value = "IS (Import�ncia Segurada)"
    xlWs.Cells(i, 15).ColumnWidth = 27
    xlWs.Cells(i, 16).Value = "Valor Pr�mio Pago"
    xlWs.Cells(i, 16).ColumnWidth = 20
    xlWs.Cells(i, 17).Value = "Valor Pr�mio Retido"
    xlWs.Cells(i, 17).ColumnWidth = 20
    xlWs.Cells(i, 18).Value = "Data de Emiss�o do Endosso"
    xlWs.Cells(i, 18).ColumnWidth = 30
 
    xlWs.Cells(i, 19).Value = "Valor Comiss�o"
    xlWs.Cells(i, 19).ColumnWidth = 15
    xlWs.Cells(i, 20).Value = "Valor Remunera��o"
    xlWs.Cells(i, 20).ColumnWidth = 18
    ' MU-2017-054666 - Rog�rio Melo - Confitec SP
    '13/03/2018 nova coluna do excel
    xlWs.Cells(i, 21).Value = "Dt. Pedido Endosso Res."
    xlWs.Cells(i, 21).ColumnWidth = 15
    xlWs.Cells(i, 22).Value = "Parcelas Pagas"
    xlWs.Cells(i, 22).ColumnWidth = 15
    xlWs.Cells(i, 23).Value = "Periodicidade de Pagto"
    xlWs.Cells(i, 23).ColumnWidth = 35
    xlWs.Cells(i, 24).Value = "Tipo Endosso"
    xlWs.Cells(i, 24).ColumnWidth = 35
    xlWs.Cells(i, 25).Value = "Endosso"
    xlWs.Cells(i, 25).ColumnWidth = 35
    xlWs.Cells(i, 26).Value = "Descri��o do Endosso"
    xlWs.Cells(i, 26).ColumnWidth = 35
    ' MU-2017-054666 - Rog�rio Melo - Confitec SP
    '13/03/2018 nova coluna do excel
    Dim j As Long
    
    For j = 1 To 25
        xlWs.Cells(i, j).Font.Bold = True
    Next
    
    
   'Dim Item As ListItem
    Dim Y As Long
    
    
    Do While Not rc.EOF
        i = i + 1
        xlWs.Cells(i, 1).Value = rc("proposta_bb")
        xlWs.Cells(i, 2).Value = rc("proposta_id")
        
        'INC000005070679 - Leandro Amaral 08/08/2016 '
        If Not IsNull(rc("val_restituicao_estimado")) Then
            xlWs.Cells(i, 3).Value = IIf(Not IsNull(rc("val_restituicao_estimado")), CDbl(Replace(rc("val_restituicao_estimado"), ".", ",")), 0)
        Else
            xlWs.Cells(i, 3).Value = 0
        End If
        xlWs.Cells(i, 3).NumberFormat = "#,##0.00"
        'INC000005070679 - Leandro Amaral 08/08/2016 '
        
        xlWs.Cells(i, 4).Value = rc("num_avaliacao")
        
        If IsDate(rc("dt_pedido_avaliacao")) Then
            xlWs.Cells(i, 5).Value = CDate(rc("dt_pedido_avaliacao"))
        Else
            xlWs.Cells(i, 5).Value = ""
        End If
        xlWs.Cells(i, 6).Value = rc("produto_id")
        xlWs.Cells(i, 7).Value = rc("usuario_restituicao")
        xlWs.Cells(i, 8).Value = rc("Cliente")
        
        If IsDate(rc("DT_Contratacao")) Then
            xlWs.Cells(i, 9).Value = CDate(rc("DT_Contratacao"))
        Else
            xlWs.Cells(i, 9).Value = ""
        End If
        
        xlWs.Cells(i, 10).Value = rc("TipoAvaliacao")
        
        If Not IsNull(rc("val_subvencao_federal_estimado")) Then
            xlWs.Cells(i, 11).Value = CDbl(Replace(rc("val_subvencao_federal_estimado"), ".", ","))
        Else
            xlWs.Cells(i, 11).Value = 0
        End If
        xlWs.Cells(i, 11).NumberFormat = "#,##0.00"
        
        If Not IsNull(rc("val_subvencao_estadual_estimado")) Then
            xlWs.Cells(i, 12).Value = CDbl(Replace(rc("val_subvencao_estadual_estimado"), ".", ","))
        Else
            xlWs.Cells(i, 12).Value = 0
        End If
        xlWs.Cells(i, 12).NumberFormat = "#,##0.00"
        
        If IsDate(rc("dt_inicio_vigencia")) Then
            xlWs.Cells(i, 13).Value = CDate(rc("dt_inicio_vigencia"))
        Else
            xlWs.Cells(i, 13).Value = ""
        End If
        
        If IsDate(rc("dt_fim_vigencia")) Then
            xlWs.Cells(i, 14).Value = CDate(rc("dt_fim_vigencia"))
        Else
            xlWs.Cells(i, 14).Value = ""
        End If
        
        If Not IsNull(rc("valis")) Then
            xlWs.Cells(i, 15).Value = CDbl(Replace(rc("ValIS"), ".", ","))
        Else
            xlWs.Cells(i, 15).Value = 0
        End If
        xlWs.Cells(i, 15).NumberFormat = "#,##0.00"
        
        If Not IsNull(rc("PremioPago")) Then
            xlWs.Cells(i, 16).Value = CDbl(Replace(rc("PremioPago"), ".", ","))
        Else
            xlWs.Cells(i, 16).Value = 0
        End If
        xlWs.Cells(i, 16).NumberFormat = "#,##0.00"
        
        If Not IsNull(rc("PremioRetido")) Then
            xlWs.Cells(i, 17).Value = CDbl(Replace(rc("PremioRetido"), ".", ","))
        Else
            xlWs.Cells(i, 17).Value = 0
        End If
        xlWs.Cells(i, 17).NumberFormat = "#,##0.00"
       
        
        If IsDate(rc("dt_emissao")) Then
            xlWs.Cells(i, 18).Value = CDate(rc("dt_emissao"))
        Else
            xlWs.Cells(i, 18).Value = ""
        End If
        
                
        If Not IsNull(rc("Val_comissao_estimado")) Then
            xlWs.Cells(i, 19).Value = CDbl(Replace(rc("Val_comissao_estimado"), ".", ","))
        Else
            xlWs.Cells(i, 19).Value = 0
        End If
        
        xlWs.Cells(i, 19).NumberFormat = "#,##0.00"
        If Not IsNull(rc("val_restituicao_estimado")) Then
            xlWs.Cells(i, 20).Value = CDbl(Replace(rc("val_restituicao_estimado"), ".", ","))
        Else
            xlWs.Cells(i, 20).Value = 0
        End If
        xlWs.Cells(i, 20).NumberFormat = "#,##0.00"
        
        If IsDate(rc("dt_pedido_endosso")) Then
            xlWs.Cells(i, 21).Value = CDate(rc("dt_pedido_endosso"))
        Else
            xlWs.Cells(i, 21).Value = ""
        End If
        
        xlWs.Cells(i, 22).Value = rc("qtd_parcela_pagas")
        xlWs.Cells(i, 23).Value = rc("periodicidade_pagamento")
        xlWs.Cells(i, 24).Value = rc("tp_endosso_id")
        xlWs.Cells(i, 25).Value = rc("endosso_id")
        xlWs.Cells(i, 26).Value = rc("descricao_endosso")
        rc.MoveNext
    Loop
    
    Call Unload(FrmEspera)
    xlApp.Visible = True
    
    
End Sub
'Alan Neves - C_ANEVES - Demanda: 17860335 - 03/06/2013 - Fim

Public Sub btnRecusa_Click()
    Dim sSQL As String
    Dim selecionado As Boolean
    selecionado = False
    Dim strPropostas As String
    Dim aprovado As Boolean
    Dim X As Long
    Dim iMotivoRecusa As Integer
    Dim tMotivoRecusa As String
    Dim qtd_recusa As Long, qtd_erro As Long
    
'criar txt
Dim strArquivo As String
Dim hArq As Integer

    On Error Resume Next

    'Nome do arquivo
    hArq = FreeFile

    strArquivo = Environ("TEMP") & "\~EndossoRecusado.txt"

     'Abre Arquivo

    Open strArquivo For Output As #hArq

     'Grava no arquivo

    Print #hArq, "Propostas; Endossos; Motivo"
    strPropostas = ""
    qtd_recusa = 0
    qtd_erro = 0

     'Fecha Arquivo

On Error GoTo TrataErro
    
    Produto_id = ListSelEndosso.SelectedItem.SubItems(6)
    
    iMotivoRecusa = Obter_Motivo_Recusa(tMotivoRecusa)
    
    If (iMotivoRecusa = 0) Then
        Close #hArq
        Kill (strArquivo)
        Exit Sub
    End If


    For X = 1 To ListSelEndosso.ListItems.Count
        Set Item = ListSelEndosso.ListItems(X)
        If Item.Checked = True Then
             selecionado = True
             Proposta = Item.SubItems(2)
             NumAvaliacao = Item.SubItems(4)
                
        If (Mid(LTrim(Item.SubItems(20)), 1, 1) = ".") Then
            ValRemuneracao = Val(Item.SubItems(20))
        Else
            ValRemuneracao = Format(Val(Replace(Replace(Item.SubItems(20), ".", ""), ",", ".")), "#,##0.00")
        End If
        
            
             Endosso = Item.SubItems(27)
       
            If TpOperacao = 1 Then
                       
                        sSQL = "select proposta_id  from #avaliacao_restituicao_tb where proposta_id = " & Proposta & " and situacao in ('p','i') and num_avaliacao = " & NumAvaliacao
            Else
                  
                        sSQL = "select proposta_id from #avaliacao_restituicao_tb where proposta_id = " & Proposta & " and situacao in ('t') and num_avaliacao = " & NumAvaliacao
            End If
            
            Set rc = rdocn.OpenResultset(sSQL)
            
            If rc.EOF Then
               strPropostas = strPropostas & Proposta & ";       " & Endosso & " ; " & "Inv�lida!" & "," & vbCrLf
               qtd_erro = qtd_erro + 1
    
            Else
                If Verificar_Uso_Proposta(Proposta) Then
                
                    strPropostas = strPropostas & Proposta & ";       " & Endosso & " ; " & "Endosso est� sendo utilizada por outro usu�rio." & "," & vbCrLf
                    qtd_erro = qtd_erro + 1
                 
                Else
                    Call RecusarRestituicao(iMotivoRecusa, tMotivoRecusa)
                    qtd_recusa = qtd_recusa + 1
                End If
                
            End If
    
            rc.Close
            Set rc = Nothing
        
        End If
        If Len(strPropostas) > 3000 Then
            If Right(strPropostas, 2) = vbCrLf Then
            strPropostas = Mid(strPropostas, 1, Len(strPropostas) - 2)
            End If
            Print #hArq, Replace(strPropostas, ",", "")
            strPropostas = ""
        End If
        
    Next
    If Len(strPropostas) > 0 Then
        If Right(strPropostas, 2) = vbCrLf Then
        strPropostas = Mid(strPropostas, 1, Len(strPropostas) - 2)
        End If
        Print #hArq, Replace(strPropostas, ",", "")
        strPropostas = ""
    End If

Close #hArq

    If Not selecionado Then
        MsgBox "Selecione pelo menos uma proposta!", vbInformation
        Call Unload(FrmEspera)
        Exit Sub
    End If

    If qtd_erro = 0 Then
     
       MsgBox qtd_recusa & " restitui��es recusadas com sucesso", vbInformation
     
    Else
        Dim smsg$
        smsg = IIf(qtd_recusa > 0, qtd_recusa & " restitui��es recusadas com sucesso" & vbCrLf, "") _
                 & qtd_erro & " endosso(s) n�o tiveram sua recusa processada" & vbNewLine _
                 & "Deseja visualizar os endossos n�o processados?"
            
            If MsgBox(smsg, vbYesNo + vbInformation + vbDefaultButton1, "Question") = vbYes Then
          
                Call Shell("Notepad " & strArquivo, vbNormalFocus)
            
            End If
     End If

    frmAnalitico.strWhere = strWhere
    frmAnalitico.inicial = inicial
    frmAnalitico.intervalo = intervalo
    frmAnalitico.maiorQue = maiorQue
    frmAnalitico.idProduto = idProduto
    frmAnalitico.QTD = QTD
    frmAnalitico.sProduto = Produto_id
    frmAnalitico.carregarForm
    frmAnalitico.Show
    chkSelecionar.Value = 0
    
On Error Resume Next
   Kill (strArquivo)
Exit Sub
TrataErro:
    Call Err.Raise(Err.Number, , "Recusa em lote - " & Err.Description)
End Sub
Private Function Obter_Endosso(Situacao As String) As Long
  Dim sSQL As String
  Dim rsEndosso As rdoResultset

sSQL = ""
sSQL = sSQL & "select top 1 endosso_id, tp_endosso_id "
sSQL = sSQL & "FROM avaliacao_restituicao_tb "
sSQL = sSQL & "Where proposta_id = " & Val(Proposta)
Select Case Situacao
    Case "T"
      sSQL = sSQL & " and situacao = 'T' "
    Case "I"
      sSQL = sSQL & " and situacao = 'I' "
    Case "R"
      sSQL = sSQL & " and situacao = 'R' "
  End Select

sSQL = sSQL & "order by num_avaliacao desc "

 Set rsEndosso = rdocn.OpenResultset(sSQL)

 Obter_Endosso = CLng(IIf(IsNull(rsEndosso!endosso_id) = True, 0, rsEndosso!endosso_id))
 rsEndosso.Close
   
  Set rsEndosso = Nothing

End Function
Sub Carregar_CmbMotivo_Vida()
    Dim SQL As String
    Dim Rst As rdoResultset
    
    SQL = "Select "
    SQL = SQL & "   tp_avaliacao_tb.tp_avaliacao_id, "
    SQL = SQL & "   tp_avaliacao_tb.nome "
    SQL = SQL & " From "
    SQL = SQL & "   tp_avaliacao_tb "
    SQL = SQL & " Where "
    SQL = SQL & "     tp_avaliacao_tb.tp_avaliacao_id in (375, 376, 1817) "

    Set Rst = rdocn.OpenResultset(SQL)
    
    CmbMotivoRecusa.Clear
    
    Do While Not Rst.EOF
        CmbMotivoRecusa.AddItem Rst!Nome
        CmbMotivoRecusa.ItemData(CmbMotivoRecusa.NewIndex) = Rst!tp_avaliacao_id
        
        Rst.MoveNext
    Loop
End Sub
Sub Carregar_CmbMotivoRecusa(PProduto)

Dim SQL As String
Dim Rst As rdoResultset

SQL = "Select "
SQL = SQL & "   tp_avaliacao_tb.tp_avaliacao_id, "
SQL = SQL & "   tp_avaliacao_tb.nome "
SQL = SQL & " From "
SQL = SQL & "   tp_avaliacao_tb "
SQL = SQL & "   inner join produto_tp_avaliacao_tb "
SQL = SQL & "         on produto_tp_avaliacao_tb.tp_avaliacao_id = tp_avaliacao_tb.tp_avaliacao_id "
SQL = SQL & " Where "
SQL = SQL & "     tp_avaliacao_tb.tratamento = 'm' "
SQL = SQL & "     and produto_tp_avaliacao_tb.produto_id = " & PProduto

Set Rst = rdocn.OpenResultset(SQL)

CmbMotivoRecusa.Clear

Do While Not Rst.EOF

    CmbMotivoRecusa.AddItem Rst!Nome
    CmbMotivoRecusa.ItemData(CmbMotivoRecusa.NewIndex) = Rst!tp_avaliacao_id
    
    Rst.MoveNext
Loop

End Sub

Private Sub RecusarRestituicao(iMotivoRecusa, tMotivoRecusa)

'Dim iMotivoRecusa As Integer
'Dim tMotivoRecusa As String

On Error GoTo TrataErro


'    iMotivoRecusa = Obter_Motivo_Recusa(tMotivoRecusa)
    
    If iMotivoRecusa <> 0 Then
        
        'Atualizando interface'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        
        MousePointer = vbHourglass
        
        'Recusando pedido de restitui��o ''''''''''''''''''''''''''''''''''''''''''''''''''''''
        
        Call RecusarAvaliacaoRestituicao(iMotivoRecusa, tMotivoRecusa)
        
        'Desbloqueando proposta'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    
        Call Atualizar_Bloqueio_Proposta(Proposta, "n")
    
        'Atualizando interface'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    
        MousePointer = vbDefault
        
'        MsgBox "Recusa efetuada", , ""
        
       
        
        sSQL = "delete from #avaliacao_restituicao_tb where proposta_id = " & Proposta & vbNewLine
        Set rc = rdocn.OpenResultset(sSQL)
        
        rc.Close
        Set rc = Nothing
'        Unload Me
'        Set rc = rdocn.OpenResultset(sSQL)
'        If formAnterior = "frmFiltroPesquisa" Then
'            Call Unload(frmFiltroPesquisa)
'            'frmFiltroPesquisa.Show
'        Else
'            frmAnalitico.strWhere = strWhere
'            frmAnalitico.inicial = inicial
'            frmAnalitico.intervalo = intervalo
'            frmAnalitico.maiorQue = maiorQue
'            frmAnalitico.idProduto = idProduto
'            frmAnalitico.QTD = QTD
'
'            frmAnalitico.sProduto = sProduto ' Alan Neves - C_ANEVES - - Demanda: 17860335 - 28/05/2013
'
'            frmAnalitico.carregarForm
'            frmAnalitico.Show
'
'        End If
                
    End If

    Exit Sub

TrataErro:
   Call TrataErroGeral("RecusarRestituicao", Me.name)
   Call TerminaSEGBR

End Sub
Private Sub RecusarAvaliacaoRestituicao(ByVal iMotivoRecusa As Integer, Optional ByVal sDescricaoMotivo As String)

' bcarneiro - 03/12/2003 - Projeto Restitui��o por Iniciativa da Seguradora

Const SITUACAO_RECUSA_TECNICA = "R"

Dim oFinanceiro As Object
Dim endosso_id As Long

On Error GoTo TrataErro

    'Atualizando interface''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    
    MousePointer = vbHourglass
    
    'Recusando avalia��o t�cnica''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    
    Set oFinanceiro = CreateObject("SEGL0026.cls00125")
    
    Call oFinanceiro.AtualizarAvaliacaoRestituicao(gsSIGLASISTEMA, _
                                                   App.Title, _
                                                   App.FileDescription, _
                                                   glAmbiente_id, _
                                                   cUserName, _
                                                   Data_Sistema, _
                                                   Proposta, _
                                                   NumAvaliacao, _
                                                   SITUACAO_RECUSA_TECNICA, _
                                                   , _
                                                   , _
                                                   , _
                                                   , _
                                                   , _
                                                   , _
                                                   , _
                                                   , _
                                                   iMotivoRecusa, _
                                                   , , , , , , -1, ValRemuneracao)
                                                   'FLOW 1186681 - Confitec - JFILHO - Corrigindo erro de l�gica, que estava passando os par�metros de forma errada.
                                                   ', , , , , , LblValRemuneracao.Caption)
'daniel.menandro - Nova Consultoria - 14/07/2013
'16351367 - Extrato de Restitui��o
'Inicio
             
endosso_id = Obter_Endosso("R")

Call oFinanceiro.InserirExtratoRestituicao(gsSIGLASISTEMA, _
                                                    App.Title, _
                                                    App.FileDescription, _
                                                    glAmbiente_id, _
                                                    cUserName, _
                                                    Val(Proposta), _
                                                    ValRemuneracao, _
                                                    endosso_id, _
                                                    0, _
                                                    "Recusa T�cnica", _
                                                    "Endosso de restitui��o recusada pelo t�cnico", _
                                                    sDescricaoMotivo)

             
    Set oFinanceiro = Nothing
    
    'Atualizando interface'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    
    MousePointer = vbDefault
    
    Exit Sub

TrataErro:
   Call TrataErroGeral("RecusarAvaliacaoRestituicao", Me.name)
   Call TerminaSEGBR
    
End Sub

Private Sub BtnSair_Click()
    If Len(strWhere) > 0 Then
    
        frmAnaliseSintetica.inicial = inicial
        frmAnaliseSintetica.intervalo = intervalo
        frmAnaliseSintetica.maiorQue = maiorQue
        frmAnaliseSintetica.Show
        Call frmAnaliseSintetica.Carrega_Form
        Call Unload(Me)
    Else
        frmFiltroPesquisa.Show
        Call Unload(Me)
    End If
    
End Sub

Private Sub cboPesquisa_Click()
If cboPesquisa.ListIndex >= 0 Then
    If cboPesquisa.ItemData(cboPesquisa.ListIndex) = 4 Then
        txtPesquisa.Mask = " "
        txtPesquisa.Mask = ""
        txtPesquisa.Mask = "##/####"
        txtPesquisa.SetFocus
    Else
        txtPesquisa.Mask = " "
        txtPesquisa.Mask = ""
        txtPesquisa.SetFocus
    End If

End If
End Sub

Private Sub chkFiltro_Click()

        cboPesquisa.Enabled = (chkFiltro.Value = vbChecked)
        txtPesquisa.Enabled = (chkFiltro.Value = vbChecked)
        cmdPesquisar.Enabled = (chkFiltro.Value = vbChecked)
    If chkFiltro.Value = vbUnchecked Then
        Call carregarTecnico(QTD, True)
        cboPesquisa = " "
        txtPesquisa.Mask = " "
        txtPesquisa.Text = " "
       
        
    End If
End Sub

Private Sub chkSelecionar_Click()
    Dim i As Long
    Dim Item As ListItem
    
    For i = 1 To ListSelEndosso.ListItems.Count
        If chkSelecionar.Value = 1 Then
            Set Item = ListSelEndosso.ListItems(i)
            If Item.SubItems(25) = "A" Then 'FLAVIO.BEZERRA - 06/06/2013
                Item.Checked = False
            Else
                Item.Checked = True
            End If
        Else
            Set Item = ListSelEndosso.ListItems(i)
            Item.Checked = False
        End If
    Next
End Sub


Private Sub cmdPesquisar_Click()
    
    Call carregarTecnico(QTD, True)
    
End Sub

Private Sub Form_Load()
    Call CentraFrm(Me)
End Sub

Public Sub carregarForm()
    Dim sSQL As String
    
    chkFiltro.Visible = False
    lblFiltro.Visible = False
    lblConteudo.Visible = False
    cboPesquisa.Visible = False
    txtPesquisa.Visible = False
    cmdPesquisar.Visible = False
    btnRecusa.Visible = False
    
    If InStr(",11,12,121,135,136,716,721,1174,1175,1177,1179,1180,1181,1182,1183,1196,1198,1205,1206,1208,1211,1217,1218,1225,1231,1235,1236,1237,", "," & idProduto & ",") > 0 Then
        chkFiltro.Visible = True
        lblFiltro.Visible = True
        lblConteudo.Visible = True
        cboPesquisa.Visible = True
        txtPesquisa.Visible = True
        cmdPesquisar.Visible = True
        btnRecusa.Visible = True
    End If
    
   
    
    If TpOperacao = 1 Then
        Me.Caption = "SEGP0209 - Avalia��o T�cnica de Restitui��o - " & Ambiente
    Else
        Me.Caption = "SEGP0209 - Avalia��o Gerencial de Restitui��o - " & Ambiente
        btnExportar.Visible = False
    End If
    
    If sProduto = "" Then     'Alan Neves - C_ANEVES - Demanda: 17860335 - 28/05/2013 - Recupera caso n�o exista o nome do produto
    'Recuperando produto
    sSQL = "select top 1 produto_id, produto from #avaliacao_restituicao_tb where produto_id = " & CStr(idProduto)
    Dim rcProduto As rdoResultset
    Set rcProduto = rdocn.OpenResultset(sSQL)
    
    If Not rcProduto.EOF Then
        lblProduto.Caption = rcProduto("produto_id") & " - " & rcProduto("produto")
    End If
    
    rcProduto.Close
    Set rcProduto = Nothing

    Else
        lblProduto.Caption = CStr(idProduto) & " - " & sProduto
    End If
    
    FrmEspera.Show
    
    Call AtualizaDadosRestituicao(0, idProduto)
    
    If TpOperacao = 1 Then
        'T�cnico
        Call carregarTecnico(QTD)
    
    Else
        'Gerencial
        Call carregarGerencial(QTD)
        
    End If
    
    Call Unload(FrmEspera)
    
    StatusBar1.SimpleText = "Total de registros encontrados: " & ListSelEndosso.ListItems.Count
End Sub

Private Sub carregarTecnico(ByVal QTD As Double, Optional bfiltro As Boolean = False)

    Dim Item As ListItem

    ListSelEndosso.ListItems.Clear
    ListSelEndosso.View = lvwReport
    ListSelEndosso.FullRowSelect = True
    ListSelEndosso.LabelEdit = lvwManual
    ListSelEndosso.HotTracking = True


    ListSelEndosso.ColumnHeaders.Clear
    'Carregando cabe�alho do grid
    ListSelEndosso.ColumnHeaders.Add , , "Selecionar", 600
    ListSelEndosso.ColumnHeaders.Add , , "Proposta BB", 1200, lvwColumnCenter
    ListSelEndosso.ColumnHeaders.Add , , "Proposta Id", 1200, lvwColumnCenter
    ListSelEndosso.ColumnHeaders.Add , , "Val.Restitui��o", 1300, lvwColumnRight
    ListSelEndosso.ColumnHeaders.Add , , "Nr. Avalia��o", 1500, lvwColumnRight
    ListSelEndosso.ColumnHeaders.Add , , "Pedido da Avalia��o", 1500
    ListSelEndosso.ColumnHeaders.Add , , "Produto", 800, lvwColumnRight
    ListSelEndosso.ColumnHeaders.Add , , "Usu�rio", 800, lvwColumnRight
    ListSelEndosso.ColumnHeaders.Add , , "Cliente", 8500
    ListSelEndosso.ColumnHeaders.Add , , "Contratacao", 1500
    ListSelEndosso.ColumnHeaders.Add , , "Inicio Vigencia", 1500
    ListSelEndosso.ColumnHeaders.Add , , "Fim Vigencia", 1500
    ListSelEndosso.ColumnHeaders.Add , , "Origem", 0
    ListSelEndosso.ColumnHeaders.Add , , "Processado", 0
    ListSelEndosso.ColumnHeaders.Add , , "Tipo de Avalia��o", 2000
    ListSelEndosso.ColumnHeaders.Add , , "Val.Subven��o Federal", 1300, lvwColumnRight
    ListSelEndosso.ColumnHeaders.Add , , "Val.Subven��o Estadual", 1300, lvwColumnRight
    ListSelEndosso.ColumnHeaders.Add , , "Val.Pr�mio Pago", 1300, lvwColumnRight
    ListSelEndosso.ColumnHeaders.Add , , "Val.Pr�mio Retido", 1300, lvwColumnRight
    ListSelEndosso.ColumnHeaders.Add , , "Endosso", 0
    ListSelEndosso.ColumnHeaders.Add , , "Val.Comiss�o", 0
    ListSelEndosso.ColumnHeaders.Add , , "Val.Remunera��o", 0
    ListSelEndosso.ColumnHeaders.Add , , "IS", 0
    ListSelEndosso.ColumnHeaders.Add , , "Modular", 0
    ListSelEndosso.ColumnHeaders.Add , , "proposta_modular", 0
    ListSelEndosso.ColumnHeaders.Add , , "Nr.Avaliacao_modular", 0

    'Renato Vasconcelos
    'Flow 17801752
    ListSelEndosso.ColumnHeaders.Add , , "Cor", 0
    ListSelEndosso.ColumnHeaders.Add , , "endosso_id", 0


    'tipo de  campo para ordenar corretamente
    ListSelEndosso.ColumnHeaders(2).Tag = "N"
    ListSelEndosso.ColumnHeaders(3).Tag = "N"
    ListSelEndosso.ColumnHeaders(4).Tag = "N"
    ListSelEndosso.ColumnHeaders(5).Tag = "N"
    ListSelEndosso.ColumnHeaders(6).Tag = "D"
    ListSelEndosso.ColumnHeaders(7).Tag = "N"
    ListSelEndosso.ColumnHeaders(8).Tag = "T"
    ListSelEndosso.ColumnHeaders(9).Tag = "T"
    ListSelEndosso.ColumnHeaders(10).Tag = "D"
    ListSelEndosso.ColumnHeaders(11).Tag = "D"
    ListSelEndosso.ColumnHeaders(14).Tag = "D"
    ListSelEndosso.ColumnHeaders(15).Tag = "T"
    ListSelEndosso.ColumnHeaders(16).Tag = "N"
    ListSelEndosso.ColumnHeaders(17).Tag = "N"
    ListSelEndosso.ColumnHeaders(18).Tag = "N"
    ListSelEndosso.ColumnHeaders(19).Tag = "N"
'    ListSelEndosso.ColumnHeaders(27).Width = 0

    Dim sSQL As String
    Dim top As String
    Dim rc As rdoResultset


    top = ""
    If QTD > 0 Then
        top = "top " & CStr(QTD)
    End If

    'Alan Neves - C_ANEVES - Demanda: 17860335 - 28/05/2013 - Inicio
    'sSQL = "SELECT " & top & " * FROM #avaliacao_restituicao_tb where produto_id = " & CStr(idProduto) & strWhere
    sSQL = "SELECT " & top & " existe_info_baixa, proposta_bb, proposta_id, val_restituicao_estimado,  num_avaliacao, dt_pedido_avaliacao, produto_id,usuario_restituicao, cliente, " & vbNewLine
    sSQL = sSQL & " dt_contratacao, dt_inicio_vigencia, dt_fim_vigencia, origem, TipoAvaliacao, val_subvencao_federal_estimado, val_subvencao_estadual_estimado, " & vbNewLine
    sSQL = sSQL & " PremioPago, PremioRetido, dt_emissao, Val_comissao_estimado, val_restituicao_estimado, ValIS, modular, proposta_modular, num_avaliacao_modular, cor,endosso_id " & vbNewLine
    sSQL = sSQL & " FROM #avaliacao_restituicao_tb where produto_id = " & CStr(idProduto) & strWhere & " "


    'Alan Neves - C_ANEVES - Demanda: 17860335 - 28/05/2013 - Fim
    ' MU-2017-054666 - Rog�rio Melo - Confitec SP
    '13/03/2018 filtro de sele��o de restitui��o
    If chkFiltro.Value = vbChecked Then

        If IsNull(txtPesquisa) Or txtPesquisa = "" Then
            MsgBox "Favor informar o conte�do de pesquisa.", vbExclamation
            txtPesquisa.SetFocus
            Exit Sub
        End If

        If cboPesquisa.Text = "Proposta Id" Then
            sSQL = sSQL & " and proposta_id = " & txtPesquisa.Text & strWhere
        ElseIf cboPesquisa.Text = "Produto" Then
            sSQL = sSQL & "and produto_id =" & txtPesquisa.Text & strWhere
        ElseIf cboPesquisa.Text = "Proposta BB" Then
            sSQL = sSQL & "and proposta_bb =" & txtPesquisa.Text & strWhere
        ElseIf cboPesquisa.Text = "Nr. Avalia��o" Then
            sSQL = sSQL & "and num_avaliacao =" & txtPesquisa.Text & strWhere
        ElseIf cboPesquisa.Text = "Pedido Avalia��o" Then
            Dim data As String
            data = txtPesquisa.Text
            data = "01/" & data
            sSQL = sSQL & " and dt_pedido_avaliacao between convert(smalldatetime,CONVERT (VARCHAR,'" & data & "', 102),103) and  eomonth(convert(smalldatetime,CONVERT (VARCHAR,'" & data & "', 102),103))" & strWhere
        ElseIf cboPesquisa.Text = "Cliente" Then
            sSQL = sSQL & "and cliente like '%" & txtPesquisa.Text & "%' " & strWhere
        ElseIf cboPesquisa.Text = "Tipo de Avalia��o" Then
            sSQL = sSQL & "and TipoAvaliacao like '%" & LTrim(RTrim(txtPesquisa.Text)) & "%' " & strWhere
        ElseIf cboPesquisa.Text = "Val. Subven��o Federal" Then
            sSQL = sSQL & "and val_subvencao_federal_estimado =" & Replace(Replace(txtPesquisa.Text, ".", ""), ",", ".") & strWhere
        ElseIf cboPesquisa.Text = "Val. Subven��o Estadual" Then
            sSQL = sSQL & "and val_subvencao_estadual_estimado =" & Replace(Replace(txtPesquisa.Text, ".", ""), ",", ".") & strWhere
        ElseIf cboPesquisa.Text = "Val. Pr�mio Pago" Then
            sSQL = sSQL & "and PremioPago =" & Replace(Replace(txtPesquisa.Text, ".", ""), ",", ".") & strWhere
        ElseIf cboPesquisa.Text = "Val. pr�meio Restituido" Then
            sSQL = sSQL & "and val_restituicao_estimado =" & Replace(Replace(txtPesquisa.Text, ".", ""), ",", ".") & strWhere
        End If

    End If
   ' MU-2017-054666 - Rog�rio Melo - Confitec SP
    '13/03/2018 filtro de sele��o de restitui��o
    
    sSQL = sSQL & " ORDER by dt_inclusao asc, proposta_bb," & vbNewLine
    sSQL = sSQL & "          proposta_id," & vbNewLine
    sSQL = sSQL & "          num_avaliacao" & vbNewLine

    
    Set rc = rdocn.OpenResultset(sSQL)

    If rc.EOF And Not bfiltro Then
        rc.Close
        Set rc = Nothing
        Unload Me
        frmFiltroPesquisa.Show
        Exit Sub
    End If

    Do While Not rc.EOF
        'verificar depois

        If UCase(rc("existe_info_baixa")) = "S" Then
            Set Item = ListSelEndosso.ListItems.Add(, , "", , 1)
        Else
            Set Item = ListSelEndosso.ListItems.Add(, , "", , 2)
        End If

        Item.SubItems(1) = IIf(IsNull(rc("proposta_bb")), "", rc("proposta_bb"))

        Item.SubItems(2) = rc("proposta_id")

        If Not IsNull(rc("val_restituicao_estimado")) Then
            Item.SubItems(3) = Format(Replace(rc("val_restituicao_estimado"), ".", ","), "#,##0.00")
        Else
            Item.SubItems(3) = "0,00"
        End If

        Item.SubItems(4) = rc("num_avaliacao")
        Item.SubItems(5) = Format(rc("dt_pedido_avaliacao"), "dd/mm/yyyy")
        Item.SubItems(6) = rc("produto_id")
        Item.SubItems(7) = rc("usuario_restituicao")
        Item.SubItems(8) = rc("cliente")
        Item.SubItems(9) = Format(rc("dt_contratacao"), "dd/mm/yyyy")
        Item.SubItems(10) = Format(rc("dt_inicio_vigencia"), "dd/mm/yyyy")
        Item.SubItems(11) = Format(rc("dt_fim_vigencia"), "dd/mm/yyyy")
        Item.SubItems(12) = rc("origem") 'origem
        Item.SubItems(13) = "N"
        Item.SubItems(14) = rc("TipoAvaliacao")


        If Not IsNull(rc("val_subvencao_federal_estimado")) Then
            Item.SubItems(15) = Format(Replace(rc("val_subvencao_federal_estimado"), ".", ","), "#,##0.00")
        Else
           Item.SubItems(15) = "0,00"
        End If


        If Not IsNull(rc("val_subvencao_estadual_estimado")) Then
            Item.SubItems(16) = Format(Replace(rc("val_subvencao_estadual_estimado"), ".", ","), "#,##0.00")
        Else
           Item.SubItems(16) = "0,00"
        End If

        Item.SubItems(17) = Format(Replace(rc("PremioPago"), ".", ","), "#,##0.00")
        Item.SubItems(18) = Format(Replace(rc("PremioRetido"), ".", ","), "#,##0.00")
        Item.SubItems(19) = Format(rc("dt_emissao"), "dd/mm/yyyy")
        Item.SubItems(20) = Format(rc("Val_comissao_estimado"), "#,##0.00")
        Item.SubItems(21) = Format(rc("val_restituicao_estimado"), "#,##0.00")
'        Item.SubItems(20) = Format(Val(rc("Val_comissao_estimado")), "#,##0.00")
        
'        If (rc("val_restituicao_estimado") < 1) Then
'            Item.SubItems(21) = Format(Replace(rc("val_restituicao_estimado"), ".", ","), "#,##0.00")
'        Else
'            Item.SubItems(21) = Format(rc("val_restituicao_estimado"), "#,##0.00")
'        End If
        
'        Item.SubItems(21) = Format(Val(rc("val_restituicao_estimado")), "#,##0.00")
        'val(
        
        Item.SubItems(22) = Format(IIf(IsNull(rc("ValIS")), "0", rc("ValIS")), "#,##0.00")
        Item.SubItems(23) = rc("modular")
        Item.SubItems(24) = rc("proposta_modular")
        Item.SubItems(25) = rc("num_avaliacao_modular")
        'Renato Vasconcelos
        'Flow 17801752
        Item.SubItems(26) = rc("cor")
        
        If Not IsNull(rc("endosso_id")) Then
           Item.SubItems(27) = rc("endosso_id")
        Else
        
            Item.SubItems(27) = 0
        End If
        
        rc.MoveNext
    Loop

    ' wander    inicio  17801752    07/03/2013
    Dim item_atual(3)
    Dim contadorA As Long
    For contadorA = 1 To ListSelEndosso.ListItems.Count
        '        item_atual(0) = ListSelEndosso.ListItems(contadorA).SubItems(2)         'Proposta_id
        '        item_atual(1) = ListSelEndosso.ListItems(contadorA).SubItems(6)         'Produto_id
        '        item_atual(2) = ListSelEndosso.ListItems(contadorA).SubItems(7)         'Cliente
        '
        '        For contadorB = contadorA + 1 To ListSelEndosso.ListItems.Count
        '            If item_atual(0) = ListSelEndosso.ListItems(contadorB).SubItems(2) _
        '                And item_atual(1) = ListSelEndosso.ListItems(contadorB).SubItems(6) _
        '                And item_atual(2) = ListSelEndosso.ListItems(contadorB).SubItems(7) Then
        '                    For Coluna = 1 To ListSelEndosso.ListItems(contadorB).ListSubItems.Count
        '                        ListSelEndosso.ListItems(contadorB).ListSubItems(Coluna).ForeColor = vbRed
        '                    Next
        '                End If
        '        Next
        If ListSelEndosso.ListItems(contadorA).ListSubItems(26).Text = "V" Then 'IM00352193
            For Coluna = 1 To ListSelEndosso.ListItems(contadorA).ListSubItems.Count
                ListSelEndosso.ListItems(contadorA).ListSubItems(Coluna).ForeColor = vbRed
            Next
        ElseIf ListSelEndosso.ListItems(contadorA).ListSubItems(26).Text = "A" Then          'IM00352193          FLAVIO.BEZERRA - 06/06/2013
            For Coluna = 1 To ListSelEndosso.ListItems(contadorA).ListSubItems.Count
                ListSelEndosso.ListItems(contadorA).ListSubItems(Coluna).ForeColor = 33023
            Next
        End If
        'Next
    Next
    ' wander    Fim  17801752    07/03/2013

End Sub


Private Sub carregarGerencial(ByVal QTD As Double)
   Dim Item As ListItem

    ListSelEndosso.ListItems.Clear
    ListSelEndosso.View = lvwReport
    ListSelEndosso.FullRowSelect = True
    ListSelEndosso.LabelEdit = lvwManual
    ListSelEndosso.HotTracking = True

    ListSelEndosso.ColumnHeaders.Clear
    'Carregando cabe�alho do grid
    ListSelEndosso.ColumnHeaders.Add , , "Selecionar", 600
    ListSelEndosso.ColumnHeaders.Add , , "Proposta BB", 1200, lvwColumnCenter
    ListSelEndosso.ColumnHeaders.Add , , "Proposta Id", 1200, lvwColumnCenter
    ListSelEndosso.ColumnHeaders.Add , , "Val.Restitui��o", 1300, lvwColumnRight
    ListSelEndosso.ColumnHeaders.Add , , "Nr. Avalia��o", 1500, lvwColumnRight
    ListSelEndosso.ColumnHeaders.Add , , "Pedido da Avalia��o", 1500
    ListSelEndosso.ColumnHeaders.Add , , "Produto", 800, lvwColumnRight
    ListSelEndosso.ColumnHeaders.Add , , "Usu�rio", 800, lvwColumnRight
    ListSelEndosso.ColumnHeaders.Add , , "Cliente", 8500
    ListSelEndosso.ColumnHeaders.Add , , "Contratacao", 1500
    ListSelEndosso.ColumnHeaders.Add , , "Inicio Vigencia", 1500
    ListSelEndosso.ColumnHeaders.Add , , "Fim Vigencia", 1500
    ListSelEndosso.ColumnHeaders.Add , , "Origem", 0
    ListSelEndosso.ColumnHeaders.Add , , "Processado", 0
    ListSelEndosso.ColumnHeaders.Add , , "Tipo de Avalia��o", 2000
    ListSelEndosso.ColumnHeaders.Add , , "Val.Subven��o Federal", 1300, lvwColumnRight
    ListSelEndosso.ColumnHeaders.Add , , "Val.Subven��o Estadual", 1300, lvwColumnRight
    ListSelEndosso.ColumnHeaders.Add , , "Val.Pr�mio Pago", 1300, lvwColumnRight
    ListSelEndosso.ColumnHeaders.Add , , "Val.Pr�mio Retido", 1300, lvwColumnRight
    ListSelEndosso.ColumnHeaders.Add , , "Endosso", 0
    ListSelEndosso.ColumnHeaders.Add , , "Val.Comiss�o", 0
    ListSelEndosso.ColumnHeaders.Add , , "Val.Remunera��o", 0
    ListSelEndosso.ColumnHeaders.Add , , "IS", 0
    ListSelEndosso.ColumnHeaders.Add , , "Modular", 0
    ListSelEndosso.ColumnHeaders.Add , , "proposta_modular", 0
    ListSelEndosso.ColumnHeaders.Add , , "Nr.Avaliacao_modular", 0
     'Renato Vasconcelos
    'Flow 17801752
    ListSelEndosso.ColumnHeaders.Add , , "Cor", 0
    ListSelEndosso.ColumnHeaders.Add , , "endosso_id", 0
    
    'tipo de  campo para ordenar corretamente
    ListSelEndosso.ColumnHeaders(2).Tag = "N"
    ListSelEndosso.ColumnHeaders(3).Tag = "N"
    ListSelEndosso.ColumnHeaders(4).Tag = "N"
    ListSelEndosso.ColumnHeaders(5).Tag = "N"
    ListSelEndosso.ColumnHeaders(6).Tag = "D"
    ListSelEndosso.ColumnHeaders(7).Tag = "N"
    ListSelEndosso.ColumnHeaders(8).Tag = "T"
    ListSelEndosso.ColumnHeaders(9).Tag = "D"
    ListSelEndosso.ColumnHeaders(10).Tag = "D"
    ListSelEndosso.ColumnHeaders(11).Tag = "D"
    ListSelEndosso.ColumnHeaders(14).Tag = "T"
    ListSelEndosso.ColumnHeaders(15).Tag = "N"
    ListSelEndosso.ColumnHeaders(16).Tag = "N"
    ListSelEndosso.ColumnHeaders(17).Tag = "N"
    ListSelEndosso.ColumnHeaders(18).Tag = "N"
    
    
    
    Dim sSQL As String
    Dim top As String
    Dim rc As rdoResultset
    top = ""
    If QTD > 0 Then
        top = "top " & CStr(QTD)
    End If
    
    'Alan Neves - C_ANEVES - Demanda: 17860335 - 29/05/2013 - Inicio
'    sSQL = "SELECT " & top & " * FROM #avaliacao_restituicao_tb where produto_id = " & CStr(idProduto) & strWhere
'    sSQL = sSQL & " ORDER by dt_inclusao asc, proposta_bb," & vbNewLine
'    sSQL = sSQL & "          proposta_id," & vbNewLine
'    sSQL = sSQL & "          num_avaliacao" & vbNewLine

    sSQL = "SELECT " & top & " existe_info_baixa, proposta_bb, proposta_id, val_restituicao,  num_avaliacao, dt_pedido_avaliacao, produto_id,usuario_restituicao, cliente, " & vbNewLine
    sSQL = sSQL & " dt_contratacao, dt_inicio_vigencia, dt_fim_vigencia, origem, TipoAvaliacao, val_subvencao_federal_estimado, val_subvencao_estadual_estimado, " & vbNewLine
    sSQL = sSQL & " PremioPago, PremioRetido, dt_emissao, Val_comissao, Val_restituicao, ValIS, modular, proposta_modular, num_avaliacao_modular, cor, dt_inclusao,endosso_id " & vbNewLine
    sSQL = sSQL & " from #avaliacao_restituicao_tb " & vbNewLine
    sSQL = sSQL & " where produto_id = " & CStr(idProduto) & strWhere
    
    
     ' MU-2017-054666 - Rog�rio Melo - Confitec SP
    '13/03/2018 filtro de sele��o de restitui��o
    If chkFiltro.Value = vbChecked Then

        If IsNull(txtPesquisa) Or txtPesquisa = "" Then
            MsgBox "Favor informar o conte�do de pesquisa.", vbExclamation
            txtPesquisa.SetFocus
            Exit Sub
        End If

        If cboPesquisa.Text = "Proposta Id" Then
            sSQL = sSQL & " and proposta_id = " & txtPesquisa.Text & strWhere
        ElseIf cboPesquisa.Text = "Produto" Then
            sSQL = sSQL & "and produto_id =" & txtPesquisa.Text & strWhere
        ElseIf cboPesquisa.Text = "Proposta BB" Then
            sSQL = sSQL & "and proposta_bb =" & txtPesquisa.Text & strWhere
        ElseIf cboPesquisa.Text = "Nr. Avalia��o" Then
            sSQL = sSQL & "and num_avaliacao =" & txtPesquisa.Text & strWhere
        ElseIf cboPesquisa.Text = "Pedido Avalia��o" Then
            Dim data As String
            data = txtPesquisa.Text
            data = "01/" & data
            sSQL = sSQL & " and dt_pedido_avaliacao between convert(smalldatetime,CONVERT (VARCHAR,'" & data & "', 102),103) and  eomonth(convert(smalldatetime,CONVERT (VARCHAR,'" & data & "', 102),103))" & strWhere
        ElseIf cboPesquisa.Text = "Cliente" Then
            sSQL = sSQL & "and cliente like '%" & txtPesquisa.Text & "%' " & strWhere
        ElseIf cboPesquisa.Text = "Tipo de Avalia��o" Then
            sSQL = sSQL & "and TipoAvaliacao like '%" & LTrim(RTrim(txtPesquisa.Text)) & "%' " & strWhere
        ElseIf cboPesquisa.Text = "Val. Subven��o Federal" Then
            sSQL = sSQL & "and val_subvencao_federal_estimado =" & Replace(Replace(txtPesquisa.Text, ".", ""), ",", ".") & strWhere
        ElseIf cboPesquisa.Text = "Val. Subven��o Estadual" Then
            sSQL = sSQL & "and val_subvencao_estadual_estimado =" & Replace(Replace(txtPesquisa.Text, ".", ""), ",", ".") & strWhere
        ElseIf cboPesquisa.Text = "Val. Pr�mio Pago" Then
            sSQL = sSQL & "and PremioPago =" & Replace(Replace(txtPesquisa.Text, ".", ""), ",", ".") & strWhere
        ElseIf cboPesquisa.Text = "Val. pr�meio Restituido" Then
            sSQL = sSQL & "and val_restituicao_estimado =" & Replace(Replace(txtPesquisa.Text, ".", ""), ",", ".") & strWhere
        End If

    End If
   ' MU-2017-054666 - Rog�rio Melo - Confitec SP
    '13/03/2018 filtro de sele��o de restitui��o
    
    sSQL = sSQL & " ORDER by dt_inclusao asc, proposta_bb," & vbNewLine
    sSQL = sSQL & "          proposta_id," & vbNewLine
    sSQL = sSQL & "          num_avaliacao" & vbNewLine
    'Alan Neves - C_ANEVES - Demanda: 17860335 - 29/05/2013 - Fim

    
    Set rc = rdocn.OpenResultset(sSQL)
    
    If rc.EOF Then
        rc.Close
        Set rc = Nothing
        Unload Me
        frmFiltroPesquisa.Show
        Exit Sub
    End If
    Do While Not rc.EOF
        'verificar depois
        
        If UCase(rc("existe_info_baixa")) = "S" Then
            Set Item = ListSelEndosso.ListItems.Add(, , "", , 1)
        Else
            Set Item = ListSelEndosso.ListItems.Add(, , "", , 2)
        End If
        
        Item.SubItems(1) = IIf(IsNull(rc("proposta_bb")), "", rc("proposta_bb"))
        Item.SubItems(2) = rc("proposta_id")
        
        If Not IsNull(rc("val_restituicao")) Then
            Item.SubItems(3) = Format(Replace(rc("val_restituicao"), ".", ","), "#,##0.00")
        Else
            Item.SubItems(3) = "0,00"
        End If
        
        Item.SubItems(4) = rc("num_avaliacao")
        Item.SubItems(5) = Format(rc("dt_pedido_avaliacao"), "dd/mm/yyyy")
        Item.SubItems(6) = rc("produto_id")
        Item.SubItems(7) = rc("usuario_restituicao")
        Item.SubItems(8) = rc("cliente")
        Item.SubItems(9) = Format(rc("dt_contratacao"), "dd/mm/yyyy")
        Item.SubItems(10) = Format(rc("dt_inicio_vigencia"), "dd/mm/yyyy")
        Item.SubItems(11) = Format(rc("dt_fim_vigencia"), "dd/mm/yyyy")
        Item.SubItems(12) = rc("origem") 'origem
        Item.SubItems(13) = "N"
        Item.SubItems(14) = rc("TipoAvaliacao")
        
        If Not IsNull(rc("val_subvencao_federal_estimado")) Then
            Item.SubItems(15) = Format(Replace(rc("val_subvencao_federal_estimado"), ".", ","), "#,##0.00")
        Else
           Item.SubItems(15) = "0,00"
        End If
        
        
        If Not IsNull(rc("val_subvencao_estadual_estimado")) Then
            Item.SubItems(16) = Format(Replace(rc("val_subvencao_estadual_estimado"), ".", ","), "#,##0.00")
        Else
           Item.SubItems(16) = "0,00"
        End If
      
        Item.SubItems(17) = Format(Replace(rc("PremioPago"), ".", ","), "#,##0.00")
        Item.SubItems(18) = Format(Replace(rc("PremioRetido"), ".", ","), "#,##0.00")
            
        Item.SubItems(19) = Format(rc("dt_emissao"), "dd/mm/yyyy")
        Item.SubItems(20) = Format(rc("Val_comissao"), "#,##0.00")
        Item.SubItems(21) = Format(rc("val_restituicao"), "#,##0.00")
        Item.SubItems(22) = Format(IIf(IsNull(rc("ValIS")), "0", rc("ValIS")), "#,##0.00")
        Item.SubItems(23) = rc("modular")
        Item.SubItems(24) = rc("proposta_modular")
        Item.SubItems(25) = rc("num_avaliacao_modular")
        
         'Renato Vasconcelos
        'Flow 17801752
        Item.SubItems(26) = rc("cor")
        
        If Not IsNull(rc("endosso_id")) Then
           Item.SubItems(27) = rc("endosso_id")
        Else
        
            Item.SubItems(27) = 0
        End If
        rc.MoveNext
        

    Loop
        
  ' wander    inicio  17801752    07/03/2013
    Dim item_atual(3)
    Dim contadorA As Long
    Dim Coluna As Integer
    For contadorA = 1 To ListSelEndosso.ListItems.Count
        '        item_atual(0) = ListSelEndosso.ListItems(contadorA).SubItems(2)         'Proposta_id
        '        item_atual(1) = ListSelEndosso.ListItems(contadorA).SubItems(6)         'Produto_id
        '        item_atual(2) = ListSelEndosso.ListItems(contadorA).SubItems(7)         'Cliente
        '
        '        For contadorB = contadorA + 1 To ListSelEndosso.ListItems.Count
        '            If item_atual(0) = ListSelEndosso.ListItems(contadorB).SubItems(2) _
        '                And item_atual(1) = ListSelEndosso.ListItems(contadorB).SubItems(6) _
        '                And item_atual(2) = ListSelEndosso.ListItems(contadorB).SubItems(7) Then
        '                    For Coluna = 1 To ListSelEndosso.ListItems(contadorB).ListSubItems.Count
        '                        ListSelEndosso.ListItems(contadorB).ListSubItems(Coluna).ForeColor = vbRed
        '                    Next
        '                End If
        '        Next
        If ListSelEndosso.ListItems(contadorA).ListSubItems(25).Text = "V" Then
              For Coluna = 1 To ListSelEndosso.ListItems(contadorA).ListSubItems.Count
                ListSelEndosso.ListItems(contadorA).ListSubItems(Coluna).ForeColor = vbRed
              Next
              
        End If
        'Next
    Next
    ' wander    Fim  17801752    07/03/2013
        
    rc.Close
    Set rc = Nothing
End Sub


Private Sub ListSelEndosso_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
    If ColumnHeader.Tag = "N" Then
        SortListView ListSelEndosso, ColumnHeader.Index, "N", OrdAsc
    ElseIf ColumnHeader.Tag = "T" Then
        SortListView ListSelEndosso, ColumnHeader.Index, "T", OrdAsc
    Else
        SortListView ListSelEndosso, ColumnHeader.Index, "D", OrdAsc
    End If
    
    OrdAsc = Not OrdAsc
End Sub

Private Function InvNumber(ByVal Number As String) As String
'Criado por:    Cinthia Monteiro
'Data:          26/04/2011
'Demanda:       9064130
'Objetivo:      Ordenar os numeros que est�o formatados como string

Static i As Integer

For i = 1 To Len(Number)
    Select Case Mid$(Number, i, 1)
        Case "-": Mid$(Number, i, 1) = " "
        Case "0": Mid$(Number, i, 1) = "9"
        Case "1": Mid$(Number, i, 1) = "8"
        Case "2": Mid$(Number, i, 1) = "7"
        Case "3": Mid$(Number, i, 1) = "6"
        Case "4": Mid$(Number, i, 1) = "5"
        Case "5": Mid$(Number, i, 1) = "4"
        Case "6": Mid$(Number, i, 1) = "3"
        Case "7": Mid$(Number, i, 1) = "2"
        Case "8": Mid$(Number, i, 1) = "1"
        Case "9": Mid$(Number, i, 1) = "0"
    End Select
    
Next

InvNumber = Number

End Function
Private Sub SortListView(ListView As ListView, ByVal Index As Integer, ByVal DataType As String, ByVal Ascending As Boolean)
    'Demanda:       9064130
    'Objetivo:      Ordenar uma listview pelo tipo de Dados: N = Numeros, T = Texto, D = Data

    On Error Resume Next
    Dim i As Integer
    Dim l As Long
    Dim strFormat As String
    Dim lngCursor As Long
    lngCursor = ListView.MousePointer
    ListView.MousePointer = vbHourglass
    LockWindowUpdate ListView.hwnd
    
    Dim blnRestoreFromTag As Boolean
    
    Select Case DataType
    Case "T"
        blnRestoreFromTag = False
        
    Case "N"
        strFormat = String$(20, "0") & "." & String$(10, "0")
        With ListView.ListItems
            If (Index = 1) Then
                For l = 1 To .Count
                    With .Item(l)
                        .Tag = .Text & Chr$(0) & .Tag
                        If IsNumeric(.Text) Then
                            If CDbl(.Text) >= 0 Then
                                .Text = Format(CDbl(.Text), strFormat)
                            Else
                                .Text = "&" & InvNumber(Format(0 - CDbl(.Text), strFormat))
                            End If
                        Else
                            .Text = ""
                        End If
                    End With
                Next l
            Else
                For l = 1 To .Count
                    With .Item(l).ListSubItems(Index - 1)
                        .Tag = .Text & Chr$(0) & .Tag
                        If IsNumeric(.Text) Then
                            If CDbl(.Text) >= 0 Then
                                .Text = Format(CDbl(.Text), strFormat)
                            Else
                                .Text = "&" & InvNumber(Format(0 - CDbl(.Text), strFormat))
                            End If
                        Else
                            .Text = ""
                        End If
                    End With
                Next l
            End If
        End With
        
        blnRestoreFromTag = True
    
    Case "D"
        strFormat = "YYYYMMDDHhNnSs"
        
        Dim dte As Date
       
        With ListView.ListItems
            If (Index = 1) Then
                For l = 1 To .Count
                    With .Item(l)
                        .Tag = .Text & Chr$(0) & .Tag
                        dte = CDate(.Text)
                        .Text = Format$(dte, strFormat)
                    End With
                Next l
            Else
                For l = 1 To .Count
                    With .Item(l).ListSubItems(Index - 1)
                        .Tag = .Text & Chr$(0) & .Tag
                        dte = CDate(.Text)
                        .Text = Format$(dte, strFormat)
                    End With
                Next l
            End If
        End With
        
        blnRestoreFromTag = True
        
    End Select
       
    ListView.SortOrder = IIf(Ascending, lvwAscending, lvwDescending)
    ListView.SortKey = Index - 1
    ListView.Sorted = True
        
    If blnRestoreFromTag Then
                
        With ListView.ListItems
            If (Index = 1) Then
                For l = 1 To .Count
                    With .Item(l)
                        i = InStr(.Tag, Chr$(0))
                        .Text = Left$(.Tag, i - 1)
                        .Tag = Mid$(.Tag, i + 1)
                    End With
                Next l
            Else
                For l = 1 To .Count
                    With .Item(l).ListSubItems(Index - 1)
                        i = InStr(.Tag, Chr$(0))
                        .Text = Left$(.Tag, i - 1)
                        .Tag = Mid$(.Tag, i + 1)
                    End With
                Next l
            End If
        End With
    End If
        
    LockWindowUpdate 0&
        
    ListView.MousePointer = lngCursor

End Sub

Private Sub ListSelEndosso_DblClick()
        'Jessica.Adao - Confitec Sistemas - 08/01/2013 - INC000003894658 : Inconsist�ncia
    'Call detalhe_proposta(CLng(ListSelEndosso.SelectedItem.SubItems(2)))
        If ListSelEndosso.SelectedItem.SubItems(25) <> "A" Then 'FLAVIO.BEZERRA - 06/06/2013
                Call detalhe_proposta(CLng(ListSelEndosso.SelectedItem.SubItems(2)), CLng(ListSelEndosso.SelectedItem.SubItems(4)))
        End If
End Sub

'Jessica.Adao - Confitec Sistemas - 08/01/2013 - INC000003894658 : Inconsist�ncia
'Private Sub detalhe_proposta(ByVal proposta_id As Long)
Private Sub detalhe_proposta(ByVal proposta_id As Long, ByVal NumAvaliacao As Long)
      
    Dim sSQL As String
    Dim rc As rdoResultset
    'verifica se a proposta existe
    If TpOperacao = 1 Then
                'Jessica.Adao - Confitec Sistemas - 08/01/2013 - INC000003894658 : Inconsist�ncia
        'sSQL = "select proposta_id from #avaliacao_restituicao_tb where proposta_id = " & CStr(proposta_id) & " and situacao in ('p','i')"
                sSQL = "select proposta_id from #avaliacao_restituicao_tb where proposta_id = " & CStr(proposta_id) & " and situacao in ('p','i') and num_avaliacao = " & CStr(NumAvaliacao)
    Else
                'Jessica.Adao - Confitec Sistemas - 08/01/2013 - INC000003894658 : Inconsist�ncia
        'sSQL = "select proposta_id from #avaliacao_restituicao_tb where proposta_id = " & CStr(proposta_id) & " and situacao in ('t')"
                sSQL = "select proposta_id from #avaliacao_restituicao_tb where proposta_id = " & CStr(proposta_id) & " and situacao in ('t') and num_avaliacao = " & CStr(NumAvaliacao)
    End If
    
    Set rc = rdocn.OpenResultset(sSQL)
    
    If rc.EOF Then
        MsgBox "Proposta inv�lida!", vbInformation 'mudar mensagem
        rc.Close
        Set rc = Nothing
        Exit Sub
    End If
    
    rc.Close
    Set rc = Nothing
    
    
    
    FrmEspera.Label1 = "Aguarde, carregando proposta.."
    FrmEspera.Show
       
    'preenchendo os campos de proposta_adesao e proposta_fechada
    
    'retorna os dados da proposta
    If TpOperacao = 1 Then
                'Jessica.Adao - Confitec Sistemas - 08/01/2013 - INC000003894658 : Inconsist�ncia
        'sSQL = "select * from #avaliacao_restituicao_tb where proposta_id = " & CStr(proposta_id) & " and situacao in ('p','i')"
'                sSQL = "select * from #avaliacao_restituicao_tb where proposta_id = " & CStr(proposta_id) & " and situacao in ('p','i') and num_avaliacao = " & CStr(numAvaliacao)
       
                sSQL = "select * from #avaliacao_restituicao_tb where proposta_id = " & CStr(proposta_id) & " and situacao in ('p','i') and num_avaliacao = " & CStr(NumAvaliacao)
    
                
    Else
                'Jessica.Adao - Confitec Sistemas - 08/01/2013 - INC000003894658 : Inconsist�ncia
        'sSQL = "select * from #avaliacao_restituicao_tb where proposta_id = " & CStr(proposta_id) & " and situacao in ('t')"
                sSQL = "select * from #avaliacao_restituicao_tb where proposta_id = " & CStr(proposta_id) & " and situacao in ('t') and num_avaliacao = " & CStr(NumAvaliacao)
    End If
    
    Set rc = rdocn.OpenResultset(sSQL)
       
     
    'Declara��es
    Dim lPropostaBB As Long
    Dim lAvaliacao As Long
    Dim sSituacao As String
    Dim sDtPedidoAvaliacao As String
    Dim sDtContratacao As String
    Dim sDtInicioVigencia As String
    Dim sDtFimVigencia As String
    Dim lPropostaId As Long
    Dim sRamoId As String
    
    Dim cValIS As Currency
    Dim cValPremio As Currency
    Dim cValTotIOFPago As Currency
    Dim cValTotPremioPago As Currency
    Dim cValTotJurosPago As Currency
    Dim cValTotAdicFracionamentoPago As Currency
    Dim cValTotDescontoPago As Currency
    Dim cValCustoApoliceCertif As Currency
    
    Dim iPrazoRestituicaoIntegral As Integer
    Dim sTpCalcRestituicao As String
    
    Dim sPossuiSubvencao As String  'Rmarins - inclu�dos em 20/10/2006
    Dim cValSubvencaoFederal As Currency
    Dim cValSubvencaoEstadual As Currency
    Dim cValSubvencaoFederalEst As Currency
    Dim cValSubvencaoEstadualEst As Currency
    
    Dim cValCustoApoliceCertifEst As Currency
    Dim cValAdicFracionamentoEst As Currency
    Dim cValDescontoEst As Currency
    Dim cValIOFEst As Currency
    Dim cValPremioTarifaEst As Currency
    Dim cValRestituicaoEst As Currency
    Dim cValTotCorretagemEst As Currency
    Dim cValProLaboreEst As Currency
    
    Dim cValCustoApoliceCertifRest As Currency
    Dim cValAdicFracionamentoRest As Currency
    Dim cValDescontoRest As Currency
    Dim cValIOFRest As Currency
    Dim cValPremioTarifaRest As Currency
    Dim cValRestituicao As Currency
    Dim cValTotCorretagemRest As Currency
    Dim cValProLaboreRest As Currency
    Dim cPercIR As Currency
    
    'mathayde 27/03/2009
    Dim cValRemuneracao As Currency
    Dim cValRemuneracaoEst As Currency


    Dim OPropostaModular As Object
    Dim rs As Recordset
    Dim lPropostaModular As Long
    
    
    Dim lEndossoId As Long
    
    On Error GoTo TrataErro
    
    MousePointer = vbHourglass

    
    Produto_id = rc("produto_id")
    iOrigem = rc("origem")
    
    
    lPropostaBB = VerNullNumerico(rc("proposta_bb"))
    lPropostaId = rc("proposta_id")
    lAvaliacao = rc("num_avaliacao")
    
    If Produto_id = 1168 Then
        lAvaliacaoModular = lAvaliacao
    End If

    ' C_ANEVES - 26/08/2013 - Demanda: 17860335 - Inicio
    Set OPropostaModular = CreateObject("SEGL0026.cls00124")
    'Set OPropostaModular = CreateObject("SEGL0355.cls00626")
    ' C_ANEVES - 26/08/2013 - Demanda: 17860335 - Fim
    
    lPropostaModular = OPropostaModular.ObterPropostaModular(gsSIGLASISTEMA, _
                                                             App.Title, _
                                                             App.FileDescription, _
                                                             glAmbiente_id, _
                                                             lPropostaId, _
                                                             IIf(TpOperacao = 1, SITUACAO_PENDENTE_AVALIACAO_TECNICA, SITUACAO_PENDENTE_AVALIACAO_GERENCIAL))
                                                             

    
    If lPropostaModular = 0 And Produto_id <> 1168 Then
        bModular = False
        propostaModular = 0
    Else
        Call DadosModular(lPropostaId, lPropostaModular)
    End If
       

    sDtPedidoAvaliacao = rc("dt_pedido_avaliacao")
    sDtContratacao = rc("dt_contratacao")
    sDtInicioVigencia = rc("dt_inicio_vigencia")
    
    If IsDate(rc("dt_fim_vigencia")) Then
        sDtFimVigencia = rc("dt_fim_vigencia")
    End If
    
    Origem = rc("origem")
    
    FrmAvaliacao.lblTipoAvaliacao = "Avalia��o de Restitui��o"
    
    FrmAvaliacao.lblDtLiq.Visible = False
    
    FrmAvaliacao.txtTipoEndosso.Text = Trim(rc("tipoAvaliacao"))
    FrmAvaliacao.txtTpEmdossoRest.Text = Trim(rc("tipoAvaliacao"))
    
    If bModular Then
        FrmAvaliacao.txtTpEmdossoRestModular.Text = Trim(rc("tipoAvaliacao"))
    End If
    rc.Close
    Set rc = Nothing

    Call Obter_Dados_Adicionais_Proposta(lPropostaId, sRamoId, cValCustoApoliceCertif)

    bDtCancelamentoValida = True

    If Verificar_Uso_Proposta(lPropostaId) Then
        MsgBox "A proposta do endosso est� sendo utilizada por outro usu�rio"
        MousePointer = vbDefault
        Call Unload(FrmEspera)
        Exit Sub
    End If

    If VerificarExistenciaAvaliacaoRestituicaoPendente(lPropostaId, lAvaliacao, TpOperacao) Then
        MsgBox "Endosso j� avaliado."
        FrmSelEndosso.Marcar_Linha_Processada
        MousePointer = vbDefault
        Exit Sub
    End If

    Call Atualizar_Bloqueio_Proposta(lPropostaId, "s")
      
    Call Obter_Premio_IS(lPropostaId, Produto_id, cValIS, cValPremio)
    If lPropostaBB <> 0 Then
       Call Carregar_Colecao_Cobrancas(lPropostaId)
    End If
    Call Obter_Totais_Pagos(cValTotPremioPago, cValTotJurosPago, cValTotAdicFracionamentoPago, cValTotDescontoPago, cValTotIOFPago)
    Call Obter_Dados_Produto(Produto_id, iPrazoRestituicaoIntegral, sTpCalcRestituicao, sPossuiSubvencao)  'Rmarins - 20/10/06
    
    
    
    
    gcValTotPremioPago = cValTotPremioPago
    gcValTotJurosPago = cValTotJurosPago
    gcValTotAdicFracionamentoPago = cValTotAdicFracionamentoPago
    gcValTotDescontoPago = cValTotDescontoPago
    gcValTotIOFPago = cValTotIOFPago
    gcValCustoApoliceCertif = cValCustoApoliceCertif
    gcValTotPremioTarifaPago = cValTotPremioPago - cValTotAdicFracionamentoPago - cValTotIOFPago - cValCustoApoliceCertif + cValTotDescontoPago
    gsPossuiSubvencao = UCase(sPossuiSubvencao)
    
    
    Call Obtem_Perc_IR(sRamoId, Data_Sistema, cPercIR)

    
    If TpOperacao = 1 Then
        sSituacao = SITUACAO_PENDENTE_AVALIACAO_TECNICA
    Else
        sSituacao = SITUACAO_PENDENTE_AVALIACAO_GERENCIAL
    End If
    
    Call ObterValoresAvaliacaoRestituicao(lPropostaId, _
                                          lAvaliacao, _
                                          sSituacao, _
                                          cValRestituicao, _
                                          cValIOFRest, _
                                          cValCustoApoliceCertifRest, _
                                          cValAdicFracionamentoRest, _
                                          cValDescontoRest, _
                                          cValPremioTarifaRest, _
                                          cValTotCorretagemRest, _
                                          cValProLaboreRest, _
                                          cValCustoApoliceCertifEst, _
                                          cValAdicFracionamentoEst, _
                                          cValDescontoEst, _
                                          cValIOFEst, _
                                          cValPremioTarifaEst, _
                                          cValRestituicaoEst, _
                                          cValTotCorretagemEst, _
                                          cValProLaboreEst, _
                                          lEndossoId, _
                                          0, "", _
                                          cValSubvencaoFederal, cValSubvencaoEstadual, _
                                          cValSubvencaoFederalEst, cValSubvencaoEstadualEst, _
                                          cValRemuneracao, cValRemuneracaoEst)
                                          'Mathayde 30/03/2009 - obtem valores de cValRemuneracao e cValRemuneracaoEst

    
    Call CarregarRateioCorretagemRestituicao(lPropostaId, _
                                             lAvaliacao, _
                                             cValRestituicao, _
                                             cValIOFRest, _
                                             cValCustoApoliceCertifRest, _
                                             cPercIR)
                                             
    
    Call CarregarCongeneres(lPropostaId)
    
    
    
    FrmAvaliacao.dPercCorretagemRest = ObterPercentualTotalCorretagem(lPropostaId)

    
    If gsPossuiSubvencao = "S" And bModular = False Then
       
        FrmAvaliacao.lblEstadualCalc.Visible = True
        FrmAvaliacao.lblValSubvencaoEstadualCalc.Visible = True
        FrmAvaliacao.lblEstadual.Visible = True
        FrmAvaliacao.maskValSubvencaoEstadual.Visible = True
        FrmAvaliacao.lblFederalCalc.Visible = True
        FrmAvaliacao.lblValSubvencaoFederalCalc.Visible = True
        FrmAvaliacao.lblFederal.Visible = True
        FrmAvaliacao.lblValSubvencaoFederal.Visible = True

        Call ObterValoresSubvencao(lPropostaId, gcValTotPremioTarifaPago)
    
        
    End If

    
    If Not alteraEmLote Then
        Me.Hide
    End If

    Call Unload(Me)
        
    FrmAvaliacao.Mask2ValPremioTarifa.Mask = "12V2"
    FrmAvaliacao.Mask2ValPremioTarifa.Text = "0,00"
    FrmAvaliacao.formAnterior = "frmAnalitico"
    FrmAvaliacao.inicial = inicial
    FrmAvaliacao.intervalo = intervalo
    FrmAvaliacao.maiorQue = maiorQue
    FrmAvaliacao.strWhere = strWhere
    FrmAvaliacao.idProduto = idProduto
    FrmAvaliacao.QTD = QTD
    FrmAvaliacao.Show
    If TpOperacao = 1 Then
        FrmAvaliacao.Caption = "SEGP0209 - Avalia��o T�cnica de Restitui��o - " & Ambiente
    Else
        FrmAvaliacao.Caption = "SEGP0209 - Avalia��o Gerencial de Restitui��o - " & Ambiente
    End If

    FrmAvaliacao.Mask2ValPremioTarifa.Enabled = True
    
 
    
    FrmAvaliacao.Ler_Dados_Proposta lPropostaId
    
    Call ConsultarPercIOF(sDtInicioVigencia, _
                          FrmAvaliacao.txtSubramoId)
    
    
    FrmAvaliacao.Preencher_GridCobrancas
    FrmAvaliacao.Ler_Endossos lPropostaId

    
    FrmAvaliacao.LerAvaliacoesRestituicao lPropostaId

    If Trim(sDtFimVigencia) <> "" Then
        FrmAvaliacao.LblPrazoSeguro.Caption = DateDiff("d", sDtInicioVigencia, sDtFimVigencia)
    Else
        FrmAvaliacao.LblPrazoSeguro.Caption = ""
    End If
    
    If bModular = True Then
        FrmAvaliacao.lblValParametroRestituicao(1).Caption = FrmAvaliacao.LblPrazoSeguro.Caption
    
        'modular
        If Trim(oDadosModular.Dt_Fim_Vigencia) <> "" Then
            FrmAvaliacao.lblValParametroRestituicao(9).Caption = DateDiff("d", oDadosModular.Dt_Inicio_Vigencia, oDadosModular.Dt_Fim_Vigencia)
        Else
            FrmAvaliacao.lblValParametroRestituicao(9).Caption = ""
        End If
    End If
    
    FrmAvaliacao.LblDiasUtilizacaoSeguro.Caption = DateDiff("d", sDtInicioVigencia, sDtPedidoAvaliacao)
    
    If bModular = True Then
        FrmAvaliacao.lblValParametroRestituicao(5).Caption = FrmAvaliacao.LblDiasUtilizacaoSeguro.Caption
        FrmAvaliacao.lblValParametroRestituicao(13).Caption = DateDiff("d", oDadosModular.Dt_Inicio_Vigencia, oDadosModular.Dt_Pedido_Avaliacao)
    End If
    
    FrmAvaliacao.fraDados.Caption = "Dados da Restitui��o"
    FrmAvaliacao.TxtPropostaBB.Text = lPropostaBB
    FrmAvaliacao.lblTexto(13).Caption = "Nr.Avalia��o:"
    FrmAvaliacao.TxtEndossoBB.Text = lAvaliacao
    FrmAvaliacao.lblDtCanc.Caption = "Dt.Pedido:"
    FrmAvaliacao.lblDtLiq.Visible = False
    FrmAvaliacao.TxtDtCancelamentoBB.Text = sDtPedidoAvaliacao
    FrmAvaliacao.lblDtReceb.Visible = False
    FrmAvaliacao.TxtDtRecebimentoEndosso.Visible = False
        
    FrmAvaliacao.txtProposta.Text = lPropostaId
    
    If bModular = True Then
        FrmAvaliacao.txtpropostaModular(0).Text = lPropostaId
        FrmAvaliacao.txtpropostaModular(1).Text = oDadosModular.proposta_id
    End If
    
    FrmAvaliacao.txtIs.Text = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValIS))
    FrmAvaliacao.txtPremio.Text = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValPremio))

    If TpOperacao = 1 Then  'Avaliacao Tecnica
        FrmAvaliacao.Mask2ValPremioTarifa.Locked = False
        FrmAvaliacao.Mask2ValPremioTarifaModular(0).Locked = False
        FrmAvaliacao.Mask2ValPremioTarifaModular(1).Locked = False
    Else
        FrmAvaliacao.Mask2ValPremioTarifa.Locked = True
        FrmAvaliacao.Mask2ValPremioTarifaModular(0).Locked = True
        FrmAvaliacao.Mask2ValPremioTarifaModular(1).Locked = True
        FrmAvaliacao.maskValSubvencaoEstadual.Locked = True
    End If
    
    'Exibindo dados necess�rios para restitui��o''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    FrmAvaliacao.LblTotValPremioPago.Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValTotPremioPago))
    FrmAvaliacao.LblTotValJuros.Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValTotJurosPago))
    FrmAvaliacao.LblCustoCertifApolice.Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValCustoApoliceCertif))
    FrmAvaliacao.LblValPremioEmitido.Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValPremio))

    If bModular = True Then
        FrmAvaliacao.lblValParametroRestituicao(2).Caption = FrmAvaliacao.LblTotValPremioPago.Caption 'Total Premio Pago
        FrmAvaliacao.lblValParametroRestituicao(3).Caption = FrmAvaliacao.LblTotValJuros.Caption 'Total Juros Pago
        FrmAvaliacao.lblValParametroRestituicao(6).Caption = FrmAvaliacao.LblValPremioEmitido.Caption 'Premio Emitido
        FrmAvaliacao.lblValParametroRestituicao(7).Caption = FrmAvaliacao.LblCustoCertifApolice.Caption 'Custo Certificacao
        
        
        FrmAvaliacao.lblValParametroRestituicao(10).Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(oDadosModular.Val_Premio_Pago)) 'Total Premio Pago
        FrmAvaliacao.lblValParametroRestituicao(11).Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(oDadosModular.Val_Juros)) 'Total Juros Pago
        FrmAvaliacao.lblValParametroRestituicao(14).Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(oDadosModular.Val_Premio)) 'Premio Emitido
        FrmAvaliacao.lblValParametroRestituicao(15).Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(oDadosModular.Custo_Apolice)) 'Custo Certificacao
    End If

    'Exibindo valor estimado de restitui��o'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    FrmAvaliacao.LblValRestituicaoEstimado = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValRestituicaoEst))
    If bModular = True Then
        FrmAvaliacao.lblestimativaModular(0).Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValRestituicaoEst))
        FrmAvaliacao.lblestimativaModular(1).Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(oDadosModular.ValRestituicaoEst))
        If Produto_id = 1168 Then
            FrmAvaliacao.LblValRestituicaoEstimado = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValRestituicaoEst + oDadosModular.ValRestituicaoEst))
        End If
    End If
    
    'Exibindo valores calculados da restitui��o'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    FrmAvaliacao.LblValPremioTarifaCalc.Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValPremioTarifaRest))
    FrmAvaliacao.LblCustoCertifApoliceCalc.Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValCustoApoliceCertifRest))
    FrmAvaliacao.LblValIOFCalc.Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValIOFRest))
    FrmAvaliacao.LblValAdicFracionamentoCalc.Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValAdicFracionamentoRest))
    FrmAvaliacao.LblValDescontoCalc.Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValDesconto))
    FrmAvaliacao.LblValRestituicaoCalc.Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValRestituicao))
    If bModular = True Then
        'Premio Tarifa
        FrmAvaliacao.lblvalCalculadoModular(0).Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValPremioTarifaEst))
        FrmAvaliacao.lblvalCalculadoModular(17).Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(oDadosModular.ValPremioTarifaEst))
        'Custo Certificado
        FrmAvaliacao.lblvalCalculadoModular(1).Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValCustoApoliceCertifEst))
        FrmAvaliacao.lblvalCalculadoModular(18).Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(oDadosModular.ValCustoApoliceCertifEst))
        'Adic. Fracionamento
        FrmAvaliacao.lblvalCalculadoModular(2).Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValAdicFracionamentoEst))
        FrmAvaliacao.lblvalCalculadoModular(19).Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(oDadosModular.ValAdicFracionamentoEst))
        'Desconto
        FrmAvaliacao.lblvalCalculadoModular(3).Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValDescontoEst))
        FrmAvaliacao.lblvalCalculadoModular(20).Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(oDadosModular.ValDescontoEst))
        'IOF
        FrmAvaliacao.lblvalCalculadoModular(4).Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValIOFEst))
        FrmAvaliacao.lblvalCalculadoModular(21).Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(oDadosModular.ValIOFEst))
        'Valor Devolu��o
        FrmAvaliacao.lblvalCalculadoModular(5).Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValRestituicaoEst))
        FrmAvaliacao.lblvalCalculadoModular(22).Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(oDadosModular.ValRestituicaoEst))
        
        If Produto_id = 1168 Then
            FrmAvaliacao.LblValPremioTarifaCalc.Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValPremioTarifaEst + oDadosModular.ValPremioTarifaEst))
            FrmAvaliacao.LblCustoCertifApoliceCalc.Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValCustoApoliceCertifEst + oDadosModular.ValCustoApoliceCertifEst))
            FrmAvaliacao.LblValIOFCalc.Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValIOFEst + oDadosModular.ValIOFEst))
            FrmAvaliacao.LblValAdicFracionamentoCalc.Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValAdicFracionamentoEst + oDadosModular.ValAdicFracionamentoEst))
            FrmAvaliacao.LblValDescontoCalc.Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValDescontoEst + oDadosModular.ValDescontoEst))
            FrmAvaliacao.LblValRestituicaoCalc.Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValRestituicaoEst + oDadosModular.ValRestituicaoEst))
        End If
        
    End If
    'Propondo valores da restitui��o informados''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    FrmAvaliacao.Mask2ValPremioTarifa.Text = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValPremioTarifaRest))
    FrmAvaliacao.LblCustoCertifApoliceRest.Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValCustoApoliceCertifRest))
    FrmAvaliacao.LblValIOF.Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValIOFRest))
    FrmAvaliacao.LblValAdicFracionamento.Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValAdicFracionamentoRest))
    FrmAvaliacao.LblValDesconto.Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValDescontoRest))
    FrmAvaliacao.LblValRestituicao.Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValRestituicao))
    
    If bModular = True Then
        'Premio Tarifa
        FrmAvaliacao.Mask2ValPremioTarifaModular(0).Text = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValPremioTarifaRest))
        FrmAvaliacao.Mask2ValPremioTarifaModular(1).Text = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(oDadosModular.ValPremioTarifaRest))
        'Custo Certificado
        FrmAvaliacao.lblvalCalculadoModular(9).Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValCustoApoliceCertifRest))
        FrmAvaliacao.lblvalCalculadoModular(26).Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(oDadosModular.ValCustoApoliceCertifRest))
        'Adic. Fracionamento
        FrmAvaliacao.lblvalCalculadoModular(10).Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValAdicFracionamentoRest))
        FrmAvaliacao.lblvalCalculadoModular(27).Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(oDadosModular.ValAdicFracionamentoRest))
        'Desconto
        FrmAvaliacao.lblvalCalculadoModular(11).Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValDescontoRest))
        FrmAvaliacao.lblvalCalculadoModular(28).Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(oDadosModular.ValDescontoRest))
        'IOF
        FrmAvaliacao.lblvalCalculadoModular(12).Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValIOFRest))
        FrmAvaliacao.lblvalCalculadoModular(29).Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(oDadosModular.ValIOFRest))
        'Valor Devolu��o
        FrmAvaliacao.lblvalCalculadoModular(13).Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValRestituicao))
        FrmAvaliacao.lblvalCalculadoModular(30).Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(oDadosModular.ValRestituicao))
        
        If Produto_id = 1168 Then
            FrmAvaliacao.Mask2ValPremioTarifa.Text = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValPremioTarifaRest + oDadosModular.ValPremioTarifaRest))
            FrmAvaliacao.LblCustoCertifApoliceRest.Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValCustoApoliceCertifRest + oDadosModular.ValCustoApoliceCertifRest))
            FrmAvaliacao.LblValIOF.Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValIOFRest + oDadosModular.ValIOFRest))
            FrmAvaliacao.LblValAdicFracionamento.Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValAdicFracionamentoRest + oDadosModular.ValAdicFracionamentoRest))
            FrmAvaliacao.LblValDesconto.Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValDescontoRest + oDadosModular.ValDescontoRest))
            FrmAvaliacao.LblValRestituicao.Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValRestituicao + oDadosModular.ValRestituicao))
        End If
        
    End If

    'Exibindo valores calculados de comiss�o '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    FrmAvaliacao.LblTotValCorretagemCalc.Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValTotCorretagemEst))
    FrmAvaliacao.LblValProLaboreCalc.Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValProLaboreEst))
    'mathayde 27/03/2009 - Exibe a remunera��o
    FrmAvaliacao.LblValRemuneracaoCalc.Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValRemuneracaoEst))
    'Exibindo valores comiss�o Informados '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    FrmAvaliacao.LblTotValCorretagem.Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValTotCorretagemRest))
    FrmAvaliacao.LblValProLabore.Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValProLaboreRest))
    'mathayde 27/03/2009 - Exibe a remunera��o
    FrmAvaliacao.LblValRemuneracao.Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValRemuneracao))
    
    If bModular = True Then
        FrmAvaliacao.lblvalCalculadoModular(6).Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValTotCorretagemEst))
        FrmAvaliacao.lblvalCalculadoModular(23).Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(oDadosModular.Val_Tot_Corretagem_Est))
        FrmAvaliacao.lblvalCalculadoModular(7).Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValProLaboreEst))
        FrmAvaliacao.lblvalCalculadoModular(24).Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(oDadosModular.Val_Pro_Lab_Est))
        FrmAvaliacao.lblvalCalculadoModular(8).Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValRemuneracaoEst))
        FrmAvaliacao.lblvalCalculadoModular(25).Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(oDadosModular.Val_Remuneracao_Est))
        
        
        FrmAvaliacao.lblvalCalculadoModular(14).Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValTotCorretagemRest))
        FrmAvaliacao.lblvalCalculadoModular(31).Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(oDadosModular.ValTotCorretagemRest))
        FrmAvaliacao.lblvalCalculadoModular(15).Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValProLaboreRest))
        FrmAvaliacao.lblvalCalculadoModular(32).Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(oDadosModular.ValProLaboreRest))
        FrmAvaliacao.lblvalCalculadoModular(16).Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValRemuneracao))
        FrmAvaliacao.lblvalCalculadoModular(33).Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(oDadosModular.Val_Remuneracao))
        
        If Produto_id = 1168 Then
            FrmAvaliacao.LblTotValCorretagemCalc.Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValTotCorretagemEst + oDadosModular.Val_Tot_Corretagem_Est))
            FrmAvaliacao.LblValProLaboreCalc.Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValProLaboreEst + oDadosModular.Val_Pro_Lab_Est))
            FrmAvaliacao.LblValRemuneracaoCalc.Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValRemuneracaoEst + oDadosModular.Val_Remuneracao_Est))
            FrmAvaliacao.LblTotValCorretagem.Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValTotCorretagemRest + oDadosModular.ValTotCorretagemRest))
            FrmAvaliacao.LblValProLabore.Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValProLaboreRest + oDadosModular.ValProLaboreRest))
            FrmAvaliacao.LblValRemuneracao.Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValRemuneracao + oDadosModular.Val_Remuneracao))
        End If
        
    End If
    
    
    If bModular = False Then
        'Exibindo valores calculados de subven��o ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        FrmAvaliacao.lblValSubvencaoEstadualCalc.Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValSubvencaoEstadualEst))
        FrmAvaliacao.lblValSubvencaoFederalCalc.Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValSubvencaoFederalEst))
        
        'Exibindo valores de subven��o informados ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        FrmAvaliacao.maskValSubvencaoEstadual.Mask = "12V2"
        FrmAvaliacao.maskValSubvencaoEstadual.Text = "0,00"
        FrmAvaliacao.maskValSubvencaoEstadual.Text = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValSubvencaoEstadual))
        FrmAvaliacao.lblValSubvencaoFederal.Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValSubvencaoFederal))
    End If
    
    
    
    'Exibindo valores calculados de subven��o ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    FrmAvaliacao.lblValSubvencaoEstadualCalc.Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValSubvencaoEstadualEst))
    FrmAvaliacao.lblValSubvencaoFederalCalc.Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValSubvencaoFederalEst))
    
    'Exibindo valores de subven��o informados ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    FrmAvaliacao.maskValSubvencaoEstadual.Mask = "12V2"
    FrmAvaliacao.maskValSubvencaoEstadual.Text = "0,00"
    FrmAvaliacao.maskValSubvencaoEstadual.Text = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValSubvencaoEstadual))
    FrmAvaliacao.lblValSubvencaoFederal.Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValSubvencaoFederal))

    'Configurando interface'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    FrmAvaliacao.Definir_Exibicao_Objetos

    FrmAvaliacao.fraSemRestituicao.Visible = False

    If bModular = False Then
        FrmAvaliacao.SSTabDados.TabVisible(5) = False
        FrmAvaliacao.SSTabDados.TabVisible(4) = True
        FrmAvaliacao.SSTabDados.Tab = 4


    Else
        FrmAvaliacao.SSTabDados.TabVisible(4) = False
        FrmAvaliacao.SSTabDados.TabVisible(5) = True
        FrmAvaliacao.SSTabDados.Tab = 5
    End If
        
    'Identificar se a proposta para a qual esta sendo atribuindo o valor � a de RE ou a de AP.
    If bModular = True Then
        Dim OPropModular As Object
        Dim Tipo As String

        Set OPropModular = CreateObject("SEGL0026.cls00125")
    
        If sDtInicioEsc = "" Or sDtInicioSec = "" Then
            Call OPropModular.ObterParametroRestituicao(gsSIGLASISTEMA, _
                                                        App.Title, _
                                                        App.FileDescription, _
                                                        glAmbiente_id, _
                                                        lPropostaId, oDadosModular.proposta_id, _
                                                        sDtInicioEsc, _
                                                        sDtFimEsc)
                                                        
            
            oDadosModular.sDtInicioVigenciaEsc = sDtInicioEsc
            oDadosModular.sDtFimVigenciaEsc = sDtFimEsc
        
        End If
        
        
        If Produto_id <> 1168 Then

            ' C_ANEVES - 26/08/2013 - Demanda: 17860335 - Inicio
            Set OPropModular = CreateObject("SEGL0026.cls00124")
            'Set OPropModular = CreateObject("SEGL0355.cls00626")
            ' C_ANEVES - 26/08/2013 - Demanda: 17860335 - Fim
        
            FrmAvaliacao.lbltipoProposta(0).AutoSize = True
            FrmAvaliacao.lbltipoProposta(1).AutoSize = True
        
            FrmAvaliacao.lbltipoProposta(0).Caption = ""
            FrmAvaliacao.lbltipoProposta(1).Caption = ""
        
            
            Tipo = OPropModular.IdentificaValorRestituicao(gsSIGLASISTEMA, _
                                                           App.Title, _
                                                           App.FileDescription, _
                                                           glAmbiente_id, _
                                                           lPropostaId)
            
            FrmAvaliacao.lbltipoProposta(0).Caption = "Tipo: " & Tipo
            
            
            Tipo = OPropModular.IdentificaValorRestituicao(gsSIGLASISTEMA, _
                                                                 App.Title, _
                                                                 App.FileDescription, _
                                                                 glAmbiente_id, _
                                                                 oDadosModular.proposta_id)
            
            FrmAvaliacao.lbltipoProposta(1).Caption = "Tipo: " & Tipo
        Else
            'If lAvaliacao > oDadosModular.Num_avaliacao Then
            If oDadosModular.iTpCobertura = 526 Then
                FrmAvaliacao.lbltipoProposta(0).Caption = "Tipo: MIP"
                FrmAvaliacao.lbltipoProposta(1).Caption = "Tipo: DFI"
            Else
                FrmAvaliacao.lbltipoProposta(0).Caption = "Tipo: DFI"
                FrmAvaliacao.lbltipoProposta(1).Caption = "Tipo: MIP"
            End If
        
        End If
    End If
    
    

    MousePointer = vbDefault
    Call Unload(FrmEspera)
    Exit Sub

TrataErro:
    Call TrataErroGeral("ExibirDetalhesRestituicao", Me.name)
    Call TerminaSEGBR

End Sub


Public Sub ObterValoresAvaliacaoRestituicao(ByVal lPropostaId As Long, _
                                            ByVal lAvaliacao As Long, _
                                            ByVal sSituacao As String, _
                                            ByRef cValRestituicao As Currency, _
                                            ByRef cValIOFRest As Currency, _
                                            ByRef cValCustoApoliceCertifRest As Currency, _
                                            ByRef cValAdicFracionamentoRest As Currency, _
                                            ByRef cValDescontoRest As Currency, _
                                            ByRef cValPremioTarifaRest As Currency, _
                                            ByRef cValTotCorretagemRest As Currency, _
                                            ByRef cValProLaboreRest As Currency, _
                                            ByRef cValCustoApoliceCertifEst As Currency, _
                                            ByRef cValAdicFracionamentoEst As Currency, _
                                            ByRef cValDescontoEst As Currency, _
                                            ByRef cValIOFEst As Currency, _
                                            ByRef cValPremioTarifaEst As Currency, _
                                            ByRef cValRestituicaoEst As Currency, _
                                            ByRef cValTotCorretagemEst As Currency, _
                                            ByRef cValProLaboreEst As Currency, _
                                            ByRef lEndossoId As Long, _
                                            Optional ByRef cValCambio As Currency, _
                                            Optional ByRef sTextoEndosso As String, _
                                            Optional ByRef cValSubvencaoFederal As Currency, Optional ByRef cValSubvencaoEstadual As Currency, _
                                            Optional ByRef cValSubvencaoFederalEst As Currency, Optional ByRef cValSubvencaoEstadualEst As Currency, _
                                            Optional ByRef cValRemuneracao As Currency, Optional ByRef cValRemuneracaoEst As Currency)
                                            'Mathayde 30/03/2009 - Recebe cValRemuneracao e cValRemuneracaoEst do Banco

' bcarneiro - 02/12/2003 - Projeto Restitui��o por Iniciativa da Seguradora

Dim oFinanceiro As Object
Dim rs As Object

On Error GoTo TrataErro

    Set oFinanceiro = CreateObject("SEGL0026.cls00125")
    
    Set rs = oFinanceiro.ConsultarAvaliacaoRestituicao(gsSIGLASISTEMA, _
                                                       App.Title, _
                                                       App.FileDescription, _
                                                       glAmbiente_id, _
                                                       sSituacao, _
                                                       , _
                                                       lPropostaId, _
                                                       lAvaliacao)

    Set oFinanceiro = Nothing

    If Not rs.EOF Then

        If TpOperacao = 1 Then

            cValRestituicao = IIf(IsNull(rs("val_restituicao_estimado")), 0, rs("val_restituicao_estimado"))
            cValIOFRest = IIf(IsNull(rs("val_iof_estimado")), 0, rs("val_iof_estimado"))
            cValCustoApoliceCertifRest = IIf(IsNull(rs("custo_apolice_estimado")), 0, rs("custo_apolice_estimado"))
            cValAdicFracionamentoRest = IIf(IsNull(rs("val_adic_fracionamento_estimado")), 0, rs("val_adic_fracionamento_estimado"))
            cValDescontoRest = IIf(IsNull(rs("val_desconto_comercial_estimado")), 0, rs("val_desconto_comercial_estimado"))
            cValPremioTarifaRest = IIf(IsNull(rs("val_premio_tarifa_estimado")), 0, rs("val_premio_tarifa_estimado"))
            cValTotCorretagemRest = IIf(IsNull(rs("val_comissao_estimado")), 0, rs("val_comissao_estimado"))
            cValProLaboreRest = IIf(IsNull(rs("val_comissao_estipulante_estimado")), 0, rs("val_comissao_estipulante_estimado"))

            cValRestituicaoEst = IIf(IsNull(rs("val_restituicao_estimado")), 0, rs("val_restituicao_estimado"))
            cValIOFEst = IIf(IsNull(rs("val_iof_estimado")), 0, rs("val_iof_estimado"))
            cValCustoApoliceCertifEst = IIf(IsNull(rs("custo_apolice_estimado")), 0, rs("custo_apolice_estimado"))
            cValAdicFracionamentoEst = IIf(IsNull(rs("val_adic_fracionamento_estimado")), 0, rs("val_adic_fracionamento_estimado"))
            cValDescontoEst = IIf(IsNull(rs("val_desconto_comercial_estimado")), 0, rs("val_desconto_comercial_estimado"))
            cValPremioTarifaEst = IIf(IsNull(rs("val_premio_tarifa_estimado")), 0, rs("val_premio_tarifa_estimado"))
            cValTotCorretagemEst = IIf(IsNull(rs("val_comissao_estimado")), 0, rs("val_comissao_estimado"))
            cValProLaboreEst = IIf(IsNull(rs("val_comissao_estipulante_estimado")), 0, rs("val_comissao_estipulante_estimado"))
            
            'mathayde 27/03/2009 - Obtem o valor da remunera��o
            cValRemuneracaoEst = IIf(IsNull(rs("val_remuneracao_estimado")), 0, rs("val_remuneracao_estimado"))
            cValRemuneracao = IIf(IsNull(rs("val_remuneracao_estimado")), 0, rs("val_remuneracao_estimado"))
            

            cValCambio = IIf(IsNull(rs("val_cambio")), 0, rs("val_cambio"))
            If Not IsNull(rs("descricao")) Then
                sTextoEndosso = Trim(rs("descricao"))
            Else
                sTextoEndosso = ""
            End If
            lEndossoId = IIf(IsNull(rs("endosso_id")), 0, rs("endosso_id"))
            
            'Rmarins - 20/10/06
            cValSubvencaoFederal = IIf(IsNull(rs("val_subvencao_federal_estimado")), 0, rs("val_subvencao_federal_estimado"))
            cValSubvencaoEstadual = IIf(IsNull(rs("val_subvencao_estadual_estimado")), 0, rs("val_subvencao_estadual_estimado"))
            cValSubvencaoFederalEst = IIf(IsNull(rs("val_subvencao_federal_estimado")), 0, rs("val_subvencao_federal_estimado"))
            cValSubvencaoEstadualEst = IIf(IsNull(rs("val_subvencao_estadual_estimado")), 0, rs("val_subvencao_estadual_estimado"))
            '
        Else
            
            cValRestituicao = VerNullNumerico(rs("val_restituicao"))
            cValPremioTarifaRest = VerNullNumerico(rs("val_premio_tarifa"))
            cValTotCorretagemRest = VerNullNumerico(rs("val_comissao"))
            cValProLaboreRest = VerNullNumerico(rs("val_comissao_estipulante"))
            
            cValIOFRest = VerNullNumerico(rs("val_iof"))
            cValCustoApoliceCertifRest = VerNullNumerico(rs("custo_apolice"))
            cValAdicFracionamentoRest = VerNullNumerico(rs("val_adic_fracionamento"))
            cValDescontoRest = VerNullNumerico(rs("val_desconto_comercial"))
            
            cValRestituicaoEst = VerNullNumerico(rs("val_restituicao_estimado"))
            cValIOFEst = VerNullNumerico(rs("val_iof_estimado"))
            cValCustoApoliceCertifEst = VerNullNumerico(rs("custo_apolice_estimado"))
            cValAdicFracionamentoEst = VerNullNumerico(rs("val_adic_fracionamento_estimado"))
            cValDescontoEst = VerNullNumerico(rs("val_desconto_comercial_estimado"))
            cValPremioTarifaEst = VerNullNumerico(rs("val_premio_tarifa_estimado"))
            cValTotCorretagemEst = VerNullNumerico(rs("val_comissao_estimado"))
            cValProLaboreEst = VerNullNumerico(rs("val_comissao_estipulante_estimado"))
             
            
            cValRemuneracao = VerNullNumerico(rs("val_remuneracao"))
            cValRemuneracaoEst = VerNullNumerico(rs("val_remuneracao_estimado"))
                
            If AvaliacaoGerencial = True And bModular = True Then
                cValRestituicao = VerNullNumerico(rs("val_restituicao")) + oDadosModular.ValRestituicao
                cValIOFRest = VerNullNumerico(rs("val_iof")) + oDadosModular.ValIOFRest
                cValCustoApoliceCertifRest = VerNullNumerico(rs("custo_apolice")) + oDadosModular.ValCustoApoliceCertifRest
                cValAdicFracionamentoRest = VerNullNumerico(rs("val_adic_fracionamento")) + oDadosModular.ValAdicFracionamentoRest
                cValDescontoRest = VerNullNumerico(rs("val_desconto_comercial")) + oDadosModular.ValDescontoRest
                cValPremioTarifaRest = VerNullNumerico(rs("val_premio_tarifa")) + oDadosModular.ValPremioTarifaRest
                cValTotCorretagemRest = VerNullNumerico(rs("val_comissao")) + oDadosModular.ValTotCorretagemRest
                cValProLaboreRest = VerNullNumerico(rs("val_comissao_estipulante")) + oDadosModular.ValProLaboreRest

                cValRestituicaoEst = VerNullNumerico(rs("val_restituicao_estimado")) + oDadosModular.ValRestituicaoEst
                cValIOFEst = VerNullNumerico(rs("val_iof_estimado")) + oDadosModular.ValIOFEst
                cValCustoApoliceCertifEst = VerNullNumerico(rs("custo_apolice_estimado")) + oDadosModular.ValCustoApoliceCertifEst
                cValAdicFracionamentoEst = VerNullNumerico(rs("val_adic_fracionamento_estimado")) + oDadosModular.ValCustoApoliceCertifEst
                cValDescontoEst = VerNullNumerico(rs("val_desconto_comercial_estimado")) + oDadosModular.ValDescontoEst
                cValPremioTarifaEst = VerNullNumerico(rs("val_premio_tarifa_estimado")) + oDadosModular.ValPremioTarifaEst
                cValTotCorretagemEst = VerNullNumerico(rs("val_comissao_estimado")) + oDadosModular.Val_Tot_Corretagem_Est
                cValProLaboreEst = VerNullNumerico(rs("val_comissao_estipulante_estimado")) + oDadosModular.Val_Pro_Lab_Est

                
                cValRemuneracao = VerNullNumerico(rs("val_remuneracao")) + oDadosModular.Val_Remuneracao
                cValRemuneracaoEst = VerNullNumerico(rs("val_remuneracao_estimado")) + oDadosModular.Val_Remuneracao_Est
            End If
            
            
                cValCambio = VerNullNumerico(rs("val_cambio"))
                If Not IsNull(rs("descricao")) Then
                    sTextoEndosso = Trim(rs("descricao"))
                Else
                    sTextoEndosso = ""
                End If
                
                lEndossoId = VerNullNumerico(rs("endosso_id"))
            
                
                cValSubvencaoFederal = VerNullNumerico(rs("val_subvencao_federal"))
                cValSubvencaoEstadual = VerNullNumerico(rs("val_subvencao_estadual"))
                cValSubvencaoFederalEst = VerNullNumerico(rs("val_subvencao_federal_estimado"))
                cValSubvencaoEstadualEst = VerNullNumerico(rs("val_subvencao_estadual_estimado"))
                
                
           
        End If

    End If

    rs.Close
    
    
    Exit Sub

TrataErro:
    Call TrataErroGeral("ObterValoresAvaliacaoRestituicao", Me.name)
    Call TerminaSEGBR
End Sub



Function VerNullNumerico(ByVal dado As Variant) As Variant
    If IsNull(dado) Then
        VerNullNumerico = 0
    ElseIf IsEmpty(dado) Or Trim(dado) = "" Then
        VerNullNumerico = 0
    Else
        VerNullNumerico = dado
    End If
End Function


Public Sub ObterValoresSubvencao(ByVal lPropostaId As Long, _
                                 ByVal cValPremio As Currency)

Dim rs As Object
Dim oSubvencao As Object

Dim cValSubvencaoInformado As Currency
Dim dPercFederal As Double

'Consulta os valores de subvencao por proposta - Rmarins 20/10/06

On Error GoTo TrataErro
    
    cValSubvencaoInformado = 0
    bPossuiSubvencao = False
    
    Set oSubvencao = CreateObject("SEGL0026.cls00125")
    
    Set rs = oSubvencao.ObterPropostaSubvencao(gsSIGLASISTEMA, _
                                               App.Title, _
                                               App.FileDescription, _
                                               glAmbiente_id, _
                                               lPropostaId)
    
    Set oSubvencao = Nothing
    
    If Not rs.EOF Then
        cValSubvencaoInformado = CCur(rs("val_subvencao_informado"))
        bPossuiSubvencao = True
    End If
     
    Set rs = Nothing
        
    dPercFederal = CalcularPercValor(cValPremio, _
                                     cValSubvencaoInformado)
    
    FrmAvaliacao.lblPercFederal.Caption = Format(dPercFederal, "##0.00000")
        
    Exit Sub
    
TrataErro:
   Call TratarErro("ObterValoresSubvencao", Me.name)
   FinalizarAplicacao
   
End Sub


Sub DadosModular(ByVal lPropostaId As Long, ByVal lPropostaModular As Long)
    Dim oFinanceiro As Object
    
    bModular = True

    If Produto_id = 1168 Then
        lPropostaModular = lPropostaId
        lAvaliacao = lAvaliacaoModular
    End If

    oDadosModular.proposta_id = lPropostaModular
    ''executar todas as fun��es para proposta modular

    Set oFinanceiro = CreateObject("SEGL0026.cls00125")
    
    Set rs = oFinanceiro.ConsultarAvaliacaoRestituicao(gsSIGLASISTEMA, _
                                                       App.Title, _
                                                       App.FileDescription, _
                                                       glAmbiente_id, _
                                                       IIf(TpOperacao = 1, SITUACAO_PENDENTE_AVALIACAO_TECNICA, SITUACAO_PENDENTE_AVALIACAO_GERENCIAL), _
                                                       IIf(Produto_id = 1168, 1168, 0), lPropostaModular, IIf(Produto_id = 1168, lAvaliacao, 0))


    If Not rs.EOF Then
        oDadosModular.proposta_bb = rs("proposta_bb")
        oDadosModular.Num_avaliacao = rs("num_avaliacao")
        oDadosModular.Dt_Pedido_Avaliacao = Format(rs("dt_pedido_avaliacao"), "dd/mm/yyyy")
        oDadosModular.Produto_id = rs("produto_id")
        oDadosModular.Dt_Contratacao = rs("dt_contratacao")
        oDadosModular.Dt_Inicio_Vigencia = rs("dt_inicio_vigencia")
        oDadosModular.Dt_Fim_Vigencia = rs("dt_fim_vigencia")
        oDadosModular.Origem = rs("origem")
        oDadosModular.Nome_Tp_Endosso = rs("nome_tp_endosso")

        If Produto_id = 1168 Then
            If lAvaliacao > rs("num_avaliacao") Then
                oDadosModular.iTpCobertura = 526
            Else
                oDadosModular.iTpCobertura = 527
            End If
        End If
        
        Call Obter_Dados_Adicionais_Proposta_Modular(lPropostaModular, _
                                                     oDadosModular.ramo_id, _
                                                     oDadosModular.Custo_Apolice)
        
        Call Obter_Premio_IS(lPropostaModular, rs("produto_id"), _
                                                   oDadosModular.Val_IS, _
                                                   oDadosModular.Val_Premio)

        Call Carregar_Colecao_Cobrancas(lPropostaModular)

        'rsilva - 02/09/2009
        'Nova regra para o calculo do premio a ser restituido
        Call ConsultarSubRamoModular(lPropostaModular)

        Call ConsultarPercIOFModular(oDadosModular.Dt_Inicio_Vigencia, _
                                     oDadosModular.SubRamoId)
        
        Call Obter_Totais_Pagos(oDadosModular.Val_Premio_Pago, _
                                oDadosModular.Val_Juros, _
                                oDadosModular.Val_Adic_Fracionamento, _
                                oDadosModular.Val_Desconto, _
                                oDadosModular.Val_IOF_Restituicao, True)
        
        Call ObterValoresAvaliacaoRestituicaoModular(lPropostaModular, _
                                                     oDadosModular.Num_avaliacao)

        Call Obtem_Perc_IRModular(oDadosModular.ramo_id, _
                                  oDadosModular.Dt_Inicio_Vigencia, _
                                  0)

        Call CarregarRateioCorretagemRestituicao(lPropostaModular, _
                                                 oDadosModular.Num_avaliacao, _
                                                 oDadosModular.ValRestituicao, _
                                                 oDadosModular.ValIOFRest, _
                                                 oDadosModular.ValCustoApoliceCertifRest, _
                                                 oDadosModular.PercIR)


        oDadosModular.PercCorretagemRest = ObterPercentualTotalCorretagem(lPropostaModular)

        Else
            bModular = False

       End If
End Sub

Private Sub ConsultarSubRamoModular(ByVal lPropostaId As Long)

Dim oApolice As Object
Dim rs As Recordset

Dim sTpEmissao As String

On Error GoTo TrataErro

    Set oApolice = CreateObject("SEGL0026.cls00126")

    Set rs = oApolice.ConsultarDados(gsSIGLASISTEMA, _
                                     App.Title, _
                                     App.FileDescription, _
                                     glAmbiente_id, _
                                     lPropostaId)
    
    Set oApolice = Nothing
    
    If Not rs.EOF Then
        oDadosModular.SubRamoId = rs("subramo_id")
    Else
        oDadosModular.SubRamoId = 0
    End If
    
    Set rs = Nothing
    
    Exit Sub

TrataErro:
    Call TratarErro("ConsultarSubRamoModular", Me.name)
    Call FinalizarAplicacao

End Sub

Private Sub ConsultarPercIOFModular(ByVal dtIOF As String, ByVal Subramo_id As Integer)
Dim oFinanceiro As Object
'RSILVA
On Error GoTo TrataErro

    If IsDate(dtIOF) Then
        dtIOF = Format(dtIOF, "yyyymmdd")
    End If

    Set oFinanceiro = CreateObject("SEGL0026.cls00125")

    oDadosModular.PercIOF = CDbl(oFinanceiro.ObterPercentualItemFinanceiro(cUserName, _
                                                                           gsSIGLASISTEMA, _
                                                                           App.Title, _
                                                                           App.FileDescription, _
                                                                           glAmbiente_id, _
                                                                           "IOF", _
                                                                           oDadosModular.ramo_id, _
                                                                           Data_Sistema, _
                                                                           dtIOF, _
                                                                           Subramo_id))

    Set oFinanceiro = Nothing

    Exit Sub

TrataErro:
    Call TratarErro("ConsultarPercIOF", Me.name)
    Call FinalizarAplicacao

End Sub

Public Sub ObterValoresAvaliacaoRestituicaoModular(ByVal lPropostaId As Long, _
                                                    ByVal lAvaliacao As Long)
    
Dim oFinanceiro As Object
Dim rs As Object

On Error GoTo TrataErro

    Set oFinanceiro = CreateObject("SEGL0026.cls00125")
    
    Set rs = oFinanceiro.ConsultarAvaliacaoRestituicao(gsSIGLASISTEMA, _
                                                       App.Title, _
                                                       App.FileDescription, _
                                                       glAmbiente_id, _
                                                       sSituacao, _
                                                       , _
                                                       lPropostaId, _
                                                       lAvaliacao)

    Set oFinanceiro = Nothing

    If Not rs.EOF Then
        With oDadosModular
            If TpOperacao = 1 Then
    
                .ValRestituicao = rs("val_restituicao_estimado")
                .ValIOFRest = rs("val_iof_estimado")
                .ValCustoApoliceCertifRest = rs("custo_apolice_estimado")
                .ValAdicFracionamentoRest = rs("val_adic_fracionamento_estimado")
                .ValDescontoRest = rs("val_desconto_comercial_estimado")
                .ValPremioTarifaRest = rs("val_premio_tarifa_estimado")
                .ValTotCorretagemRest = rs("val_comissao_estimado")
                .ValProLaboreRest = rs("val_comissao_estipulante_estimado")
    
                .ValRestituicaoEst = rs("val_restituicao_estimado")
                .ValIOFEst = rs("val_iof_estimado")
                .ValCustoApoliceCertifEst = rs("custo_apolice_estimado")
                .ValAdicFracionamentoEst = rs("val_adic_fracionamento_estimado")
                .ValDescontoEst = rs("val_desconto_comercial_estimado")
                .ValPremioTarifaEst = rs("val_premio_tarifa_estimado")
                .Val_Tot_Corretagem_Est = rs("val_comissao_estimado")
                .Val_Pro_Lab_Est = rs("val_comissao_estipulante_estimado")
                               
                .Val_Remuneracao = rs("val_remuneracao")
                .Val_Remuneracao_Est = rs("val_remuneracao_estimado")
                .ValCambio = rs("val_cambio")
                .EndossoId = rs("endosso_id")
                
            Else
                .ValRestituicao = rs("val_restituicao")
                .ValIOFRest = rs("val_iof")
                .ValCustoApoliceCertifRest = rs("custo_apolice")
                .ValAdicFracionamentoRest = rs("val_adic_fracionamento")
                .ValDescontoRest = rs("val_desconto_comercial")
                .ValPremioTarifaRest = rs("val_premio_tarifa")
                .ValTotCorretagemRest = rs("val_comissao")
                .ValProLaboreRest = rs("val_comissao_estipulante")
                
                .ValRestituicaoEst = rs("val_restituicao_estimado")
                .ValIOFEst = rs("val_iof_estimado")
                .ValCustoApoliceCertifEst = rs("custo_apolice_estimado")
                .ValAdicFracionamentoEst = rs("val_adic_fracionamento_estimado")
                .ValDescontoEst = rs("val_desconto_comercial_estimado")
                .ValPremioTarifaEst = rs("val_premio_tarifa_estimado")
                .Val_Tot_Corretagem_Est = rs("val_comissao_estimado")
                .Val_Pro_Lab_Est = rs("val_comissao_estipulante_estimado")
                
                
                .Val_Remuneracao = rs("val_remuneracao")
                .Val_Remuneracao_Est = rs("val_remuneracao_estimado")
                
                .ValCambio = rs("val_cambio")
                
                .EndossoId = rs("endosso_id")
                

        End If
           
            
        'End If
        End With
    End If

    rs.Close
    
    Exit Sub

TrataErro:
    Call TrataErroGeral("ObterValoresAvaliacaoRestituicao", Me.name)
    Call TerminaSEGBR

End Sub


Sub Obtem_Perc_IRModular(ByVal PRamo As String, ByVal PDtReferencia As String, ByRef PPercIr As Currency)

Dim rc As rdoResultset
Dim SQL As String
Dim DtReferencia As String

DtReferencia = Format(PDtReferencia, "yyyymmdd")

'Obtendo percentual de IR

SQL = "SELECT valor "
SQL = SQL & "From "
SQL = SQL & "     val_item_financeiro_ramo_tb "
SQL = SQL & "Where "
SQL = SQL & "     ramo_id = " & PRamo
SQL = SQL & "     and ltrim(rtrim(cod_item_financeiro)) = 'IR'"

SQL = SQL & " and dt_inicio_vigencia <= '" & DtReferencia & "'"
SQL = SQL & " and (dt_fim_vigencia >= '" & DtReferencia & "' or dt_fim_vigencia is Null)"

Set rc = rdocn.OpenResultset(SQL)

If Not rc.EOF Then
   oDadosModular.PercIR = Val(rc!valor)
Else
   MensagemBatch "Percentual de IR n�o encontrado. o Programa ser� cancelado."
   End
End If

Exit Sub

TrataErro:
   TrataErroGeral "Obtem_Perc_IR", "Rateio.bas"
   End

End Sub



Private Sub ConsultarPercIOF(ByVal dtIOF As String, ByVal Subramo_id As Integer)
Dim oFinanceiro As Object
'RSILVA
On Error GoTo TrataErro

    If IsDate(dtIOF) Then
        dtIOF = Format(dtIOF, "yyyymmdd")
    End If

    Set oFinanceiro = CreateObject("SEGL0026.cls00125")

    FrmAvaliacao.dPercIOF = CDbl(oFinanceiro.ObterPercentualItemFinanceiro(cUserName, _
                                                                           gsSIGLASISTEMA, _
                                                                           App.Title, _
                                                                           App.FileDescription, _
                                                                           glAmbiente_id, _
                                                                           "IOF", _
                                                                           oDadosModular.ramo_id, _
                                                                           Data_Sistema, _
                                                                           dtIOF, _
                                                                           Subramo_id))

    Set oFinanceiro = Nothing

    Exit Sub

TrataErro:
    Call TratarErro("ConsultarPercIOF", Me.name)
    Call FinalizarAplicacao

End Sub






Public Function CalcularPercValor(ByVal cValPrincipal As Currency, _
                                  ByVal cValInformado As Currency) As Double

Dim dPercentual As Double

On Error GoTo TrataErro

    If cValPrincipal = 0 Then
       cValInformado = 0
       CalcularPercValor = cValInformado
       Exit Function
    End If
                
    dPercentual = ((cValInformado / cValPrincipal) * 100)
    
    If dPercentual < 0 Then
       dPercentual = 0
    End If
    
    CalcularPercValor = dPercentual
    
    Exit Function

TrataErro:
    Call TratarErro("CalcularPercValor", Me.name)
    Call FinalizarAplicacao

End Function

Private Sub ListSelEndosso_ItemCheck(ByVal Item As MSComctlLib.ListItem) 'FLAVIO.BEZERRA - 06/06/2013
    If Item.SubItems(25) = "A" Then
        Item.Checked = False
    End If
End Sub
