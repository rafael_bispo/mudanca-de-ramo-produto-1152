VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Object = "{1CB70356-FEA2-11D4-87FA-00805F396245}#1.0#0"; "mask2s.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "mscomctl.OCX"
Begin VB.Form FrmAvaliacao 
   ClientHeight    =   9240
   ClientLeft      =   780
   ClientTop       =   600
   ClientWidth     =   14925
   ControlBox      =   0   'False
   Icon            =   "AvaliacaoCancelamento.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   9240
   ScaleWidth      =   14925
   Begin VB.CommandButton BtnRetornoParaAvaliacaoTecnica 
      Caption         =   "Retornar"
      Height          =   375
      Left            =   11430
      TabIndex        =   53
      Top             =   8460
      Width           =   1695
   End
   Begin VB.CommandButton BtnRecusar 
      Caption         =   "Aceite s/ Devolu��o"
      Height          =   375
      Left            =   11430
      TabIndex        =   135
      Top             =   8460
      Width           =   1695
   End
   Begin VB.CommandButton BtnAprovar 
      Caption         =   "Aprovar"
      Height          =   375
      Left            =   9720
      TabIndex        =   52
      Top             =   8460
      Width           =   1695
   End
   Begin VB.CommandButton BtnAceiteTecnico 
      Caption         =   "Autorizar"
      Height          =   375
      Left            =   9720
      TabIndex        =   51
      Top             =   8460
      Width           =   1695
   End
   Begin VB.CommandButton BtnSair 
      Caption         =   "Sair"
      Height          =   375
      Left            =   13140
      TabIndex        =   54
      Top             =   8460
      Width           =   1695
   End
   Begin VB.Frame fraDados 
      Caption         =   "Dados do Endosso"
      Height          =   735
      Left            =   30
      TabIndex        =   55
      Top             =   30
      Width           =   14835
      Begin VB.TextBox TxtDtCancelamentoBB 
         Height          =   285
         Left            =   5880
         Locked          =   -1  'True
         TabIndex        =   2
         Top             =   270
         Width           =   1275
      End
      Begin VB.TextBox TxtEndossoBB 
         BackColor       =   &H00FFFFFF&
         Height          =   300
         Left            =   3570
         Locked          =   -1  'True
         TabIndex        =   1
         TabStop         =   0   'False
         Tag             =   "Proposta BB"
         Top             =   270
         Width           =   1260
      End
      Begin VB.TextBox TxtPropostaBB 
         Height          =   300
         Left            =   1170
         Locked          =   -1  'True
         TabIndex        =   0
         TabStop         =   0   'False
         Tag             =   "Proposta BB"
         Top             =   270
         Width           =   1275
      End
      Begin VB.TextBox TxtDtRecebimentoEndosso 
         Height          =   285
         Left            =   8520
         Locked          =   -1  'True
         TabIndex        =   3
         Top             =   270
         Width           =   1275
      End
      Begin VB.Label lblDtLiq 
         Alignment       =   1  'Right Justify
         Caption         =   "Dt. Liquid:"
         Height          =   225
         Left            =   4920
         TabIndex        =   137
         Top             =   300
         Visible         =   0   'False
         Width           =   915
      End
      Begin VB.Label lblDtCanc 
         Alignment       =   1  'Right Justify
         Caption         =   "Dt. Canc BB:"
         Height          =   225
         Left            =   4890
         TabIndex        =   88
         Top             =   330
         Width           =   945
      End
      Begin VB.Label lblTexto 
         Alignment       =   1  'Right Justify
         AutoSize        =   -1  'True
         Caption         =   "Endosso BB:"
         Height          =   195
         Index           =   13
         Left            =   2520
         TabIndex        =   59
         Top             =   300
         Width           =   1005
      End
      Begin VB.Label lblTexto 
         AutoSize        =   -1  'True
         Caption         =   "Proposta BB:"
         Height          =   195
         Index           =   20
         Left            =   150
         TabIndex        =   58
         Top             =   300
         Width           =   930
      End
      Begin VB.Label lblDtReceb 
         Caption         =   "Dt.Recebimento:"
         Height          =   225
         Left            =   7290
         TabIndex        =   56
         Top             =   300
         Width           =   1185
      End
   End
   Begin MSComctlLib.StatusBar StatusBar1 
      Align           =   2  'Align Bottom
      Height          =   315
      Left            =   0
      TabIndex        =   57
      Top             =   8925
      Width           =   14925
      _ExtentX        =   26326
      _ExtentY        =   556
      Style           =   1
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin TabDlg.SSTab SSTabDados 
      Height          =   7575
      Left            =   0
      TabIndex        =   60
      Top             =   840
      Width           =   14805
      _ExtentX        =   26114
      _ExtentY        =   13361
      _Version        =   393216
      Tabs            =   6
      Tab             =   4
      TabsPerRow      =   6
      TabHeight       =   520
      TabCaption(0)   =   "Proposta"
      TabPicture(0)   =   "AvaliacaoCancelamento.frx":0442
      Tab(0).ControlEnabled=   0   'False
      Tab(0).Control(0)=   "Frame3"
      Tab(0).Control(1)=   "frameProposta"
      Tab(0).Control(2)=   "Frame2(0)"
      Tab(0).Control(3)=   "FrameDadosPessoaFisica"
      Tab(0).Control(4)=   "FrameDadosPessoaJuridica"
      Tab(0).ControlCount=   5
      TabCaption(1)   =   "Cobran�as"
      TabPicture(1)   =   "AvaliacaoCancelamento.frx":045E
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "GridCobrancas"
      Tab(1).ControlCount=   1
      TabCaption(2)   =   "Endossos"
      TabPicture(2)   =   "AvaliacaoCancelamento.frx":047A
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "GridHistoricoEndosso"
      Tab(2).ControlCount=   1
      TabCaption(3)   =   "Avalia��es"
      TabPicture(3)   =   "AvaliacaoCancelamento.frx":0496
      Tab(3).ControlEnabled=   0   'False
      Tab(3).Control(0)=   "Frame6"
      Tab(3).Control(1)=   "GridHistoricoCancelamento"
      Tab(3).ControlCount=   2
      TabCaption(4)   =   "Restitui��o"
      TabPicture(4)   =   "AvaliacaoCancelamento.frx":04B2
      Tab(4).ControlEnabled=   -1  'True
      Tab(4).Control(0)=   "fraSemRestituicao"
      Tab(4).Control(0).Enabled=   0   'False
      Tab(4).Control(1)=   "Frame4"
      Tab(4).Control(1).Enabled=   0   'False
      Tab(4).Control(2)=   "Frame5"
      Tab(4).Control(2).Enabled=   0   'False
      Tab(4).Control(3)=   "Frame9"
      Tab(4).Control(3).Enabled=   0   'False
      Tab(4).Control(4)=   "Frame12"
      Tab(4).Control(4).Enabled=   0   'False
      Tab(4).ControlCount=   5
      TabCaption(5)   =   "Restitui��o"
      TabPicture(5)   =   "AvaliacaoCancelamento.frx":04CE
      Tab(5).ControlEnabled=   0   'False
      Tab(5).Control(0)=   "fraRestituicaoModular(2)"
      Tab(5).Control(1)=   "fraRestituicaoModular(8)"
      Tab(5).Control(2)=   "fraRestituicaoModular(7)"
      Tab(5).Control(3)=   "fraRestituicaoModular(4)"
      Tab(5).Control(4)=   "fraRestituicaoModular(3)"
      Tab(5).Control(5)=   "fraRestituicaoModular(1)"
      Tab(5).Control(6)=   "fraRestituicaoModular(0)"
      Tab(5).Control(7)=   "Frame7"
      Tab(5).ControlCount=   8
      Begin VB.Frame fraRestituicaoModular 
         Caption         =   "Parametros de Restitui��o"
         Height          =   1935
         Index           =   2
         Left            =   -67800
         TabIndex        =   299
         Top             =   1200
         Width           =   6795
         Begin VB.TextBox txtpropostaModular 
            BackColor       =   &H8000000F&
            Height          =   285
            Index           =   1
            Left            =   2400
            Locked          =   -1  'True
            TabIndex        =   325
            Top             =   240
            Width           =   2055
         End
         Begin VB.Label lbltipoProposta 
            Height          =   255
            Index           =   1
            Left            =   4560
            TabIndex        =   327
            Top             =   255
            Width           =   2175
         End
         Begin VB.Label Label53 
            AutoSize        =   -1  'True
            Caption         =   "Proposta:"
            Height          =   195
            Left            =   1560
            TabIndex        =   323
            Top             =   255
            Width           =   675
         End
         Begin VB.Label lblValParametroRestituicao 
            Alignment       =   1  'Right Justify
            BorderStyle     =   1  'Fixed Single
            Height          =   255
            Index           =   14
            Left            =   5190
            TabIndex        =   318
            Top             =   1320
            Width           =   1185
         End
         Begin VB.Label lblParametroRestituicao 
            AutoSize        =   -1  'True
            Caption         =   "Pr�mio Emitido:"
            Height          =   195
            Index           =   18
            Left            =   4095
            TabIndex        =   317
            Top             =   1320
            Width           =   1080
         End
         Begin VB.Label lblParametroRestituicao 
            Caption         =   "(Dias)"
            Height          =   225
            Index           =   17
            Left            =   5670
            TabIndex        =   316
            Top             =   1020
            Width           =   465
         End
         Begin VB.Label lblParametroRestituicao 
            Caption         =   "(Dias)"
            Height          =   225
            Index           =   16
            Left            =   3000
            TabIndex        =   315
            Top             =   960
            Width           =   435
         End
         Begin VB.Label Label50 
            Caption         =   "(Dias)"
            Height          =   225
            Left            =   2970
            TabIndex        =   314
            Top             =   630
            Width           =   435
         End
         Begin VB.Label lblValParametroRestituicao 
            Alignment       =   1  'Right Justify
            BorderStyle     =   1  'Fixed Single
            Height          =   255
            Index           =   11
            Left            =   2430
            TabIndex        =   313
            Top             =   1560
            Width           =   1185
         End
         Begin VB.Label lblValParametroRestituicao 
            Alignment       =   1  'Right Justify
            BorderStyle     =   1  'Fixed Single
            Height          =   255
            Index           =   10
            Left            =   2430
            TabIndex        =   312
            Top             =   1230
            Width           =   1185
         End
         Begin VB.Label lblParametroRestituicao 
            AutoSize        =   -1  'True
            Caption         =   "Total Juros Pago:"
            Height          =   195
            Index           =   15
            Left            =   1020
            TabIndex        =   311
            Top             =   1590
            Width           =   1245
         End
         Begin VB.Label lblParametroRestituicao 
            AutoSize        =   -1  'True
            Caption         =   "Total Premio Pago:"
            Height          =   195
            Index           =   14
            Left            =   930
            TabIndex        =   310
            Top             =   1290
            Width           =   1350
         End
         Begin VB.Label lblParametroRestituicao 
            AutoSize        =   -1  'True
            Caption         =   "Tipo de C�lculo:"
            Height          =   195
            Index           =   13
            Left            =   4020
            TabIndex        =   309
            Top             =   690
            Width           =   1155
         End
         Begin VB.Label lblValParametroRestituicao 
            BorderStyle     =   1  'Fixed Single
            Height          =   285
            Index           =   12
            Left            =   5190
            TabIndex        =   308
            Top             =   645
            Width           =   1185
         End
         Begin VB.Label lblParametroRestituicao 
            AutoSize        =   -1  'True
            Caption         =   "Prazo de Restitui��o Integral:"
            Height          =   195
            Index           =   12
            Left            =   195
            TabIndex        =   307
            Top             =   630
            Width           =   2085
         End
         Begin VB.Label lblValParametroRestituicao 
            BorderStyle     =   1  'Fixed Single
            Height          =   255
            Index           =   8
            Left            =   2430
            TabIndex        =   306
            Top             =   630
            Width           =   465
         End
         Begin VB.Label lblValParametroRestituicao 
            BorderStyle     =   1  'Fixed Single
            Height          =   255
            Index           =   13
            Left            =   5190
            TabIndex        =   305
            Top             =   975
            Width           =   435
         End
         Begin VB.Label lblParametroRestituicao 
            AutoSize        =   -1  'True
            Caption         =   "Per�odo de Utiliza��o:"
            Height          =   195
            Index           =   11
            Left            =   3600
            TabIndex        =   304
            Top             =   1020
            Width           =   1575
         End
         Begin VB.Label lblParametroRestituicao 
            AutoSize        =   -1  'True
            Caption         =   "Prazo do Seguro:"
            Height          =   195
            Index           =   10
            Left            =   1050
            TabIndex        =   303
            Top             =   960
            Width           =   1230
         End
         Begin VB.Label lblValParametroRestituicao 
            BorderStyle     =   1  'Fixed Single
            Height          =   255
            Index           =   9
            Left            =   2430
            TabIndex        =   302
            Top             =   960
            Width           =   465
         End
         Begin VB.Label lblParametroRestituicao 
            AutoSize        =   -1  'True
            Caption         =   "Custo Certificado:"
            Height          =   195
            Index           =   9
            Left            =   3930
            TabIndex        =   301
            Top             =   1620
            Width           =   1245
         End
         Begin VB.Label lblValParametroRestituicao 
            Alignment       =   1  'Right Justify
            BorderStyle     =   1  'Fixed Single
            Height          =   255
            Index           =   15
            Left            =   5190
            TabIndex        =   300
            Top             =   1590
            Width           =   1185
         End
      End
      Begin VB.Frame fraRestituicaoModular 
         Caption         =   "Demonstrativo da Restitui��o"
         Height          =   3615
         Index           =   8
         Left            =   -67800
         TabIndex        =   248
         Top             =   3720
         Width           =   6795
         Begin VB.Frame fraRestituicaoModular 
            Caption         =   "Valor Informado"
            Height          =   3315
            Index           =   10
            Left            =   3300
            TabIndex        =   274
            Top             =   240
            Width           =   3345
            Begin Mask2S.ConMask2S Mask2ValPremioTarifaModular 
               Height          =   255
               Index           =   1
               Left            =   1800
               TabIndex        =   275
               Top             =   180
               Width           =   1185
               _ExtentX        =   2090
               _ExtentY        =   450
               mask            =   ""
               text            =   "0,00"
               locked          =   0   'False
               enabled         =   -1  'True
            End
            Begin VB.Label lblvalCalculadoModular 
               Alignment       =   1  'Right Justify
               BorderStyle     =   1  'Fixed Single
               Height          =   255
               Index           =   33
               Left            =   1800
               TabIndex        =   298
               Top             =   2835
               Width           =   1185
            End
            Begin VB.Label lblValCalculado 
               Alignment       =   1  'Right Justify
               AutoSize        =   -1  'True
               Caption         =   "Valor Remunera��o:"
               Height          =   195
               Index           =   29
               Left            =   195
               TabIndex        =   297
               Top             =   2880
               Width           =   1455
            End
            Begin VB.Label lblvalCalculadoModular 
               Alignment       =   1  'Right Justify
               BorderStyle     =   1  'Fixed Single
               Height          =   255
               Index           =   31
               Left            =   1800
               TabIndex        =   296
               Top             =   2205
               Width           =   1185
            End
            Begin VB.Label lblValCalculado 
               AutoSize        =   -1  'True
               Caption         =   "Corretagem:"
               Height          =   195
               Index           =   28
               Left            =   780
               TabIndex        =   295
               Top             =   2250
               Width           =   855
            End
            Begin VB.Label lblvalCalculadoModular 
               Alignment       =   1  'Right Justify
               BorderStyle     =   1  'Fixed Single
               Height          =   255
               Index           =   32
               Left            =   1800
               TabIndex        =   294
               Top             =   2520
               Width           =   1185
            End
            Begin VB.Label lblValCalculado 
               AutoSize        =   -1  'True
               Caption         =   "Pr�-Labore:"
               Height          =   195
               Index           =   27
               Left            =   810
               TabIndex        =   293
               Top             =   2565
               Width           =   825
            End
            Begin VB.Label lblvalCalculadoModular 
               Alignment       =   1  'Right Justify
               BorderStyle     =   1  'Fixed Single
               Height          =   255
               Index           =   30
               Left            =   1800
               TabIndex        =   292
               Top             =   1920
               Width           =   1185
            End
            Begin VB.Label Label49 
               Caption         =   "="
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   12
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Left            =   3120
               TabIndex        =   291
               Top             =   1890
               Width           =   195
            End
            Begin VB.Label Label48 
               Caption         =   "-"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   12
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Left            =   3120
               TabIndex        =   290
               Top             =   1110
               Width           =   195
            End
            Begin VB.Label Label47 
               Caption         =   "+"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   12
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Left            =   3090
               TabIndex        =   289
               Top             =   1440
               Width           =   195
            End
            Begin VB.Label Label45 
               Caption         =   "+"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   12
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Left            =   3090
               TabIndex        =   288
               Top             =   840
               Width           =   195
            End
            Begin VB.Label Label44 
               Caption         =   "+"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   12
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Left            =   3090
               TabIndex        =   287
               Top             =   510
               Width           =   195
            End
            Begin VB.Label Label38 
               Caption         =   "+"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   12
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Left            =   3090
               TabIndex        =   286
               Top             =   180
               Width           =   195
            End
            Begin VB.Label lblValCalculado 
               AutoSize        =   -1  'True
               Caption         =   "Valor Devolu��o :"
               Height          =   195
               Index           =   26
               Left            =   345
               TabIndex        =   285
               Top             =   1920
               Width           =   1275
            End
            Begin VB.Label lblvalCalculadoModular 
               Alignment       =   1  'Right Justify
               BorderStyle     =   1  'Fixed Single
               Height          =   255
               Index           =   29
               Left            =   1800
               TabIndex        =   284
               Top             =   1440
               Width           =   1185
            End
            Begin VB.Label lblValCalculado 
               AutoSize        =   -1  'True
               Caption         =   "IOF:"
               Height          =   195
               Index           =   25
               Left            =   1320
               TabIndex        =   283
               Top             =   1500
               Width           =   300
            End
            Begin VB.Label lblValCalculado 
               AutoSize        =   -1  'True
               Caption         =   "Premio Tarifa:"
               Height          =   195
               Index           =   24
               Left            =   645
               TabIndex        =   282
               Top             =   240
               Width           =   975
            End
            Begin VB.Label lblvalCalculadoModular 
               Alignment       =   1  'Right Justify
               BorderStyle     =   1  'Fixed Single
               Height          =   255
               Index           =   26
               Left            =   1770
               TabIndex        =   281
               Top             =   540
               Width           =   1185
            End
            Begin VB.Label lblValCalculado 
               AutoSize        =   -1  'True
               Caption         =   "Custo Certificado:"
               Height          =   195
               Index           =   23
               Left            =   375
               TabIndex        =   280
               Top             =   570
               Width           =   1245
            End
            Begin VB.Label lblvalCalculadoModular 
               Alignment       =   1  'Right Justify
               BorderStyle     =   1  'Fixed Single
               Height          =   255
               Index           =   27
               Left            =   1770
               TabIndex        =   279
               Top             =   840
               Width           =   1185
            End
            Begin VB.Label lblValCalculado 
               AutoSize        =   -1  'True
               Caption         =   "Adic. Fracionamento:"
               Height          =   195
               Index           =   22
               Left            =   120
               TabIndex        =   278
               Top             =   870
               Width           =   1500
            End
            Begin VB.Label lblvalCalculadoModular 
               Alignment       =   1  'Right Justify
               BorderStyle     =   1  'Fixed Single
               Height          =   255
               Index           =   28
               Left            =   1800
               TabIndex        =   277
               Top             =   1140
               Width           =   1185
            End
            Begin VB.Label lblValCalculado 
               AutoSize        =   -1  'True
               Caption         =   "Desconto:"
               Height          =   195
               Index           =   21
               Left            =   885
               TabIndex        =   276
               Top             =   1170
               Width           =   735
            End
            Begin VB.Line Line3 
               Index           =   1
               X1              =   120
               X2              =   3120
               Y1              =   1800
               Y2              =   1800
            End
         End
         Begin VB.Frame fraRestituicaoModular 
            Caption         =   "Valor Calculado"
            Height          =   3315
            Index           =   9
            Left            =   150
            TabIndex        =   249
            Top             =   240
            Width           =   3105
            Begin VB.Label lblvalCalculadoModular 
               Alignment       =   1  'Right Justify
               BorderStyle     =   1  'Fixed Single
               Height          =   255
               Index           =   25
               Left            =   1560
               TabIndex        =   273
               Top             =   2835
               Width           =   1185
            End
            Begin VB.Label lblvalCalculadoModular 
               Alignment       =   1  'Right Justify
               BorderStyle     =   1  'Fixed Single
               Height          =   255
               Index           =   24
               Left            =   1560
               TabIndex        =   272
               Top             =   2535
               Width           =   1185
            End
            Begin VB.Label lblvalCalculadoModular 
               Alignment       =   1  'Right Justify
               BorderStyle     =   1  'Fixed Single
               Height          =   255
               Index           =   23
               Left            =   1560
               TabIndex        =   271
               Top             =   2220
               Width           =   1185
            End
            Begin VB.Label lblvalCalculadoModular 
               Alignment       =   1  'Right Justify
               BorderStyle     =   1  'Fixed Single
               Height          =   255
               Index           =   22
               Left            =   1560
               TabIndex        =   270
               Top             =   1920
               Width           =   1185
            End
            Begin VB.Line Line4 
               Index           =   1
               X1              =   120
               X2              =   2880
               Y1              =   1800
               Y2              =   1800
            End
            Begin VB.Label lblvalCalculadoModular 
               Alignment       =   1  'Right Justify
               BorderStyle     =   1  'Fixed Single
               Height          =   255
               Index           =   20
               Left            =   1560
               TabIndex        =   269
               Top             =   1155
               Width           =   1185
            End
            Begin VB.Label lblvalCalculadoModular 
               Alignment       =   1  'Right Justify
               BorderStyle     =   1  'Fixed Single
               Height          =   255
               Index           =   19
               Left            =   1560
               TabIndex        =   268
               Top             =   855
               Width           =   1185
            End
            Begin VB.Label lblvalCalculadoModular 
               Alignment       =   1  'Right Justify
               BorderStyle     =   1  'Fixed Single
               Height          =   255
               Index           =   18
               Left            =   1560
               TabIndex        =   267
               Top             =   540
               Width           =   1185
            End
            Begin VB.Label lblvalCalculadoModular 
               Alignment       =   1  'Right Justify
               BorderStyle     =   1  'Fixed Single
               Height          =   255
               Index           =   21
               Left            =   1560
               TabIndex        =   266
               Top             =   1470
               Width           =   1185
            End
            Begin VB.Label lblsinais 
               Caption         =   "+"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   12
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Index           =   11
               Left            =   2850
               TabIndex        =   265
               Top             =   180
               Width           =   195
            End
            Begin VB.Label lblsinais 
               Caption         =   "+"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   12
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Index           =   10
               Left            =   2850
               TabIndex        =   264
               Top             =   510
               Width           =   195
            End
            Begin VB.Label lblsinais 
               Caption         =   "+"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   12
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Index           =   9
               Left            =   2850
               TabIndex        =   263
               Top             =   840
               Width           =   195
            End
            Begin VB.Label lblsinais 
               Caption         =   "+"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   12
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Index           =   8
               Left            =   2850
               TabIndex        =   262
               Top             =   1470
               Width           =   195
            End
            Begin VB.Label lblsinais 
               Caption         =   "-"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   12
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Index           =   7
               Left            =   2880
               TabIndex        =   261
               Top             =   1140
               Width           =   105
            End
            Begin VB.Label lblsinais 
               Caption         =   "="
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   12
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Index           =   6
               Left            =   2850
               TabIndex        =   260
               Top             =   1920
               Width           =   195
            End
            Begin VB.Label lblValCalculado 
               Alignment       =   1  'Right Justify
               AutoSize        =   -1  'True
               Caption         =   "Valor Remunera��o:"
               Height          =   195
               Index           =   20
               Left            =   90
               TabIndex        =   259
               Top             =   2880
               Width           =   1455
            End
            Begin VB.Label lblValCalculado 
               AutoSize        =   -1  'True
               Caption         =   "Pr�-Labore:"
               Height          =   195
               Index           =   19
               Left            =   690
               TabIndex        =   258
               Top             =   2550
               Width           =   855
            End
            Begin VB.Label lblValCalculado 
               AutoSize        =   -1  'True
               Caption         =   "Corretagem:"
               Height          =   195
               Index           =   18
               Left            =   690
               TabIndex        =   257
               Top             =   2235
               Width           =   855
            End
            Begin VB.Label lblValCalculado 
               AutoSize        =   -1  'True
               Caption         =   "Desconto:"
               Height          =   195
               Index           =   17
               Left            =   810
               TabIndex        =   256
               Top             =   1185
               Width           =   735
            End
            Begin VB.Label lblValCalculado 
               AutoSize        =   -1  'True
               Caption         =   "Adic. Fracionamento:"
               Height          =   195
               Index           =   16
               Left            =   45
               TabIndex        =   255
               Top             =   885
               Width           =   1500
            End
            Begin VB.Label lblValCalculado 
               AutoSize        =   -1  'True
               Caption         =   "IOF:"
               Height          =   195
               Index           =   15
               Left            =   1245
               TabIndex        =   254
               Top             =   1515
               Width           =   300
            End
            Begin VB.Label lblValCalculado 
               AutoSize        =   -1  'True
               Caption         =   "Valor Devolu��o :"
               Height          =   195
               Index           =   14
               Left            =   270
               TabIndex        =   253
               Top             =   1920
               Width           =   1275
            End
            Begin VB.Label lblValCalculado 
               AutoSize        =   -1  'True
               Caption         =   "Custo Certificado:"
               Height          =   195
               Index           =   13
               Left            =   300
               TabIndex        =   252
               Top             =   570
               Width           =   1245
            End
            Begin VB.Label lblvalCalculadoModular 
               Alignment       =   1  'Right Justify
               BorderStyle     =   1  'Fixed Single
               Height          =   255
               Index           =   17
               Left            =   1560
               TabIndex        =   251
               Top             =   210
               Width           =   1185
            End
            Begin VB.Label lblValCalculado 
               AutoSize        =   -1  'True
               Caption         =   "Premio Tarifa:"
               Height          =   195
               Index           =   12
               Left            =   570
               TabIndex        =   250
               Top             =   255
               Width           =   975
            End
         End
      End
      Begin VB.Frame fraRestituicaoModular 
         Caption         =   "Estimativa"
         Height          =   615
         Index           =   7
         Left            =   -67800
         TabIndex        =   245
         Top             =   3120
         Width           =   6795
         Begin VB.Label lblTexto 
            AutoSize        =   -1  'True
            Caption         =   "Valor de Devolu��o:"
            Height          =   195
            Index           =   32
            Left            =   780
            TabIndex        =   247
            Top             =   210
            Width           =   1455
         End
         Begin VB.Label lblestimativaModular 
            Alignment       =   1  'Right Justify
            BorderStyle     =   1  'Fixed Single
            Height          =   255
            Index           =   1
            Left            =   2430
            TabIndex        =   246
            Top             =   210
            Width           =   1785
         End
      End
      Begin VB.Frame fraRestituicaoModular 
         Caption         =   "Demonstrativo da Restitui��o"
         Height          =   3615
         Index           =   4
         Left            =   -74880
         TabIndex        =   194
         Top             =   3720
         Width           =   6795
         Begin VB.Frame fraRestituicaoModular 
            Caption         =   "Valor Calculado"
            Height          =   3315
            Index           =   5
            Left            =   150
            TabIndex        =   220
            Top             =   240
            Width           =   3105
            Begin VB.Label lblValCalculado 
               AutoSize        =   -1  'True
               Caption         =   "Premio Tarifa:"
               Height          =   195
               Index           =   0
               Left            =   570
               TabIndex        =   244
               Top             =   255
               Width           =   975
            End
            Begin VB.Label lblvalCalculadoModular 
               Alignment       =   1  'Right Justify
               BorderStyle     =   1  'Fixed Single
               Height          =   255
               Index           =   0
               Left            =   1560
               TabIndex        =   243
               Top             =   210
               Width           =   1185
            End
            Begin VB.Label lblValCalculado 
               AutoSize        =   -1  'True
               Caption         =   "Custo Certificado:"
               Height          =   195
               Index           =   37
               Left            =   300
               TabIndex        =   242
               Top             =   570
               Width           =   1245
            End
            Begin VB.Label lblValCalculado 
               AutoSize        =   -1  'True
               Caption         =   "Valor Devolu��o :"
               Height          =   195
               Index           =   2
               Left            =   270
               TabIndex        =   241
               Top             =   1920
               Width           =   1275
            End
            Begin VB.Label lblValCalculado 
               AutoSize        =   -1  'True
               Caption         =   "IOF:"
               Height          =   195
               Index           =   1
               Left            =   1245
               TabIndex        =   240
               Top             =   1515
               Width           =   300
            End
            Begin VB.Label lblValCalculado 
               AutoSize        =   -1  'True
               Caption         =   "Adic. Fracionamento:"
               Height          =   195
               Index           =   36
               Left            =   45
               TabIndex        =   239
               Top             =   885
               Width           =   1500
            End
            Begin VB.Label lblValCalculado 
               AutoSize        =   -1  'True
               Caption         =   "Desconto:"
               Height          =   195
               Index           =   35
               Left            =   810
               TabIndex        =   238
               Top             =   1185
               Width           =   735
            End
            Begin VB.Label lblValCalculado 
               AutoSize        =   -1  'True
               Caption         =   "Corretagem:"
               Height          =   195
               Index           =   3
               Left            =   690
               TabIndex        =   237
               Top             =   2235
               Width           =   855
            End
            Begin VB.Label lblValCalculado 
               AutoSize        =   -1  'True
               Caption         =   "Pr�-Labore:"
               Height          =   195
               Index           =   4
               Left            =   690
               TabIndex        =   236
               Top             =   2550
               Width           =   855
            End
            Begin VB.Label lblValCalculado 
               Alignment       =   1  'Right Justify
               AutoSize        =   -1  'True
               Caption         =   "Valor Remunera��o:"
               Height          =   195
               Index           =   5
               Left            =   90
               TabIndex        =   235
               Top             =   2880
               Width           =   1455
            End
            Begin VB.Label lblsinais 
               Caption         =   "="
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   12
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Index           =   5
               Left            =   2850
               TabIndex        =   234
               Top             =   1920
               Width           =   195
            End
            Begin VB.Label lblsinais 
               Caption         =   "-"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   12
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Index           =   3
               Left            =   2880
               TabIndex        =   233
               Top             =   1140
               Width           =   105
            End
            Begin VB.Label lblsinais 
               Caption         =   "+"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   12
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Index           =   4
               Left            =   2850
               TabIndex        =   232
               Top             =   1470
               Width           =   195
            End
            Begin VB.Label lblsinais 
               Caption         =   "+"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   12
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Index           =   2
               Left            =   2850
               TabIndex        =   231
               Top             =   840
               Width           =   195
            End
            Begin VB.Label lblsinais 
               Caption         =   "+"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   12
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Index           =   1
               Left            =   2850
               TabIndex        =   230
               Top             =   510
               Width           =   195
            End
            Begin VB.Label lblsinais 
               Caption         =   "+"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   12
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Index           =   0
               Left            =   2850
               TabIndex        =   229
               Top             =   180
               Width           =   195
            End
            Begin VB.Label lblvalCalculadoModular 
               Alignment       =   1  'Right Justify
               BorderStyle     =   1  'Fixed Single
               Height          =   255
               Index           =   4
               Left            =   1560
               TabIndex        =   228
               Top             =   1470
               Width           =   1185
            End
            Begin VB.Label lblvalCalculadoModular 
               Alignment       =   1  'Right Justify
               BorderStyle     =   1  'Fixed Single
               Height          =   255
               Index           =   1
               Left            =   1560
               TabIndex        =   227
               Top             =   540
               Width           =   1185
            End
            Begin VB.Label lblvalCalculadoModular 
               Alignment       =   1  'Right Justify
               BorderStyle     =   1  'Fixed Single
               Height          =   255
               Index           =   2
               Left            =   1560
               TabIndex        =   226
               Top             =   855
               Width           =   1185
            End
            Begin VB.Label lblvalCalculadoModular 
               Alignment       =   1  'Right Justify
               BorderStyle     =   1  'Fixed Single
               Height          =   255
               Index           =   3
               Left            =   1560
               TabIndex        =   225
               Top             =   1155
               Width           =   1185
            End
            Begin VB.Line Line4 
               Index           =   0
               X1              =   120
               X2              =   2880
               Y1              =   1800
               Y2              =   1800
            End
            Begin VB.Label lblvalCalculadoModular 
               Alignment       =   1  'Right Justify
               BorderStyle     =   1  'Fixed Single
               Height          =   255
               Index           =   5
               Left            =   1560
               TabIndex        =   224
               Top             =   1920
               Width           =   1185
            End
            Begin VB.Label lblvalCalculadoModular 
               Alignment       =   1  'Right Justify
               BorderStyle     =   1  'Fixed Single
               Height          =   255
               Index           =   6
               Left            =   1560
               TabIndex        =   223
               Top             =   2220
               Width           =   1185
            End
            Begin VB.Label lblvalCalculadoModular 
               Alignment       =   1  'Right Justify
               BorderStyle     =   1  'Fixed Single
               Height          =   255
               Index           =   7
               Left            =   1560
               TabIndex        =   222
               Top             =   2535
               Width           =   1185
            End
            Begin VB.Label lblvalCalculadoModular 
               Alignment       =   1  'Right Justify
               BorderStyle     =   1  'Fixed Single
               Height          =   255
               Index           =   8
               Left            =   1560
               TabIndex        =   221
               Top             =   2835
               Width           =   1185
            End
         End
         Begin VB.Frame fraRestituicaoModular 
            Caption         =   "Valor Informado"
            Height          =   3315
            Index           =   6
            Left            =   3300
            TabIndex        =   195
            Top             =   240
            Width           =   3345
            Begin Mask2S.ConMask2S Mask2ValPremioTarifaModular 
               Height          =   255
               Index           =   0
               Left            =   1800
               TabIndex        =   196
               Top             =   180
               Width           =   1185
               _ExtentX        =   2090
               _ExtentY        =   450
               mask            =   ""
               text            =   "0,00"
               locked          =   0   'False
               enabled         =   -1  'True
            End
            Begin VB.Line Line3 
               Index           =   0
               X1              =   120
               X2              =   3120
               Y1              =   1800
               Y2              =   1800
            End
            Begin VB.Label lblValCalculado 
               AutoSize        =   -1  'True
               Caption         =   "Desconto:"
               Height          =   195
               Index           =   34
               Left            =   885
               TabIndex        =   219
               Top             =   1170
               Width           =   735
            End
            Begin VB.Label lblvalCalculadoModular 
               Alignment       =   1  'Right Justify
               BorderStyle     =   1  'Fixed Single
               Height          =   255
               Index           =   11
               Left            =   1770
               TabIndex        =   218
               Top             =   1140
               Width           =   1185
            End
            Begin VB.Label lblValCalculado 
               AutoSize        =   -1  'True
               Caption         =   "Adic. Fracionamento:"
               Height          =   195
               Index           =   33
               Left            =   120
               TabIndex        =   217
               Top             =   870
               Width           =   1500
            End
            Begin VB.Label lblvalCalculadoModular 
               Alignment       =   1  'Right Justify
               BorderStyle     =   1  'Fixed Single
               Height          =   255
               Index           =   10
               Left            =   1770
               TabIndex        =   216
               Top             =   840
               Width           =   1185
            End
            Begin VB.Label lblValCalculado 
               AutoSize        =   -1  'True
               Caption         =   "Custo Certificado:"
               Height          =   195
               Index           =   32
               Left            =   375
               TabIndex        =   215
               Top             =   570
               Width           =   1245
            End
            Begin VB.Label lblvalCalculadoModular 
               Alignment       =   1  'Right Justify
               BorderStyle     =   1  'Fixed Single
               Height          =   255
               Index           =   9
               Left            =   1770
               TabIndex        =   214
               Top             =   540
               Width           =   1185
            End
            Begin VB.Label lblValCalculado 
               AutoSize        =   -1  'True
               Caption         =   "Premio Tarifa:"
               Height          =   195
               Index           =   6
               Left            =   645
               TabIndex        =   213
               Top             =   240
               Width           =   975
            End
            Begin VB.Label lblValCalculado 
               AutoSize        =   -1  'True
               Caption         =   "IOF:"
               Height          =   195
               Index           =   7
               Left            =   1320
               TabIndex        =   212
               Top             =   1500
               Width           =   300
            End
            Begin VB.Label lblvalCalculadoModular 
               Alignment       =   1  'Right Justify
               BorderStyle     =   1  'Fixed Single
               Height          =   285
               Index           =   12
               Left            =   1800
               TabIndex        =   211
               Top             =   1440
               Width           =   1185
            End
            Begin VB.Label lblValCalculado 
               AutoSize        =   -1  'True
               Caption         =   "Valor Devolu��o :"
               Height          =   195
               Index           =   8
               Left            =   345
               TabIndex        =   210
               Top             =   1920
               Width           =   1275
            End
            Begin VB.Label Label64 
               Caption         =   "+"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   12
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Left            =   3090
               TabIndex        =   209
               Top             =   180
               Width           =   195
            End
            Begin VB.Label Label63 
               Caption         =   "+"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   12
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Left            =   3090
               TabIndex        =   208
               Top             =   510
               Width           =   195
            End
            Begin VB.Label Label62 
               Caption         =   "+"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   12
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Left            =   3090
               TabIndex        =   207
               Top             =   840
               Width           =   195
            End
            Begin VB.Label Label61 
               Caption         =   "+"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   12
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Left            =   3090
               TabIndex        =   206
               Top             =   1440
               Width           =   195
            End
            Begin VB.Label Label60 
               Caption         =   "-"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   12
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Left            =   3120
               TabIndex        =   205
               Top             =   1110
               Width           =   195
            End
            Begin VB.Label Label59 
               Caption         =   "="
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   12
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Left            =   3120
               TabIndex        =   204
               Top             =   1890
               Width           =   195
            End
            Begin VB.Label lblvalCalculadoModular 
               Alignment       =   1  'Right Justify
               BorderStyle     =   1  'Fixed Single
               Height          =   255
               Index           =   13
               Left            =   1800
               TabIndex        =   203
               Top             =   1890
               Width           =   1185
            End
            Begin VB.Label lblValCalculado 
               AutoSize        =   -1  'True
               Caption         =   "Pr�-Labore:"
               Height          =   195
               Index           =   10
               Left            =   810
               TabIndex        =   202
               Top             =   2565
               Width           =   825
            End
            Begin VB.Label lblvalCalculadoModular 
               Alignment       =   1  'Right Justify
               BorderStyle     =   1  'Fixed Single
               Height          =   255
               Index           =   15
               Left            =   1800
               TabIndex        =   201
               Top             =   2520
               Width           =   1185
            End
            Begin VB.Label lblValCalculado 
               AutoSize        =   -1  'True
               Caption         =   "Corretagem:"
               Height          =   195
               Index           =   9
               Left            =   780
               TabIndex        =   200
               Top             =   2250
               Width           =   855
            End
            Begin VB.Label lblvalCalculadoModular 
               Alignment       =   1  'Right Justify
               BorderStyle     =   1  'Fixed Single
               Height          =   255
               Index           =   14
               Left            =   1800
               TabIndex        =   199
               Top             =   2205
               Width           =   1185
            End
            Begin VB.Label lblValCalculado 
               Alignment       =   1  'Right Justify
               AutoSize        =   -1  'True
               Caption         =   "Valor Remunera��o:"
               Height          =   195
               Index           =   11
               Left            =   195
               TabIndex        =   198
               Top             =   2880
               Width           =   1455
            End
            Begin VB.Label lblvalCalculadoModular 
               Alignment       =   1  'Right Justify
               BorderStyle     =   1  'Fixed Single
               Height          =   255
               Index           =   16
               Left            =   1800
               TabIndex        =   197
               Top             =   2835
               Width           =   1185
            End
         End
      End
      Begin VB.Frame fraRestituicaoModular 
         Caption         =   "Estimativa"
         Height          =   615
         Index           =   3
         Left            =   -74880
         TabIndex        =   191
         Top             =   3120
         Width           =   6795
         Begin VB.Label lblestimativaModular 
            Alignment       =   1  'Right Justify
            BorderStyle     =   1  'Fixed Single
            Height          =   255
            Index           =   0
            Left            =   2430
            TabIndex        =   193
            Top             =   240
            Width           =   1785
         End
         Begin VB.Label lblTexto 
            AutoSize        =   -1  'True
            Caption         =   "Valor de Devolu��o:"
            Height          =   195
            Index           =   31
            Left            =   780
            TabIndex        =   192
            Top             =   210
            Width           =   1455
         End
      End
      Begin VB.Frame fraRestituicaoModular 
         Caption         =   "Parametros de Restitui��o"
         Height          =   1935
         Index           =   1
         Left            =   -74880
         TabIndex        =   172
         Top             =   1200
         Width           =   6795
         Begin VB.TextBox txtpropostaModular 
            BackColor       =   &H8000000F&
            Height          =   285
            Index           =   0
            Left            =   2400
            Locked          =   -1  'True
            TabIndex        =   324
            Top             =   240
            Width           =   2055
         End
         Begin VB.Label lbltipoProposta 
            Height          =   255
            Index           =   0
            Left            =   4560
            TabIndex        =   326
            Top             =   255
            Width           =   2175
         End
         Begin VB.Label lblValParametroRestituicao 
            BorderStyle     =   1  'Fixed Single
            Height          =   255
            Index           =   0
            Left            =   2430
            TabIndex        =   322
            Top             =   630
            Width           =   465
         End
         Begin VB.Label Label52 
            AutoSize        =   -1  'True
            Caption         =   "Proposta:"
            Height          =   195
            Left            =   1560
            TabIndex        =   321
            Top             =   300
            Width           =   675
         End
         Begin VB.Label lblValParametroRestituicao 
            Alignment       =   1  'Right Justify
            BorderStyle     =   1  'Fixed Single
            Height          =   255
            Index           =   7
            Left            =   5190
            TabIndex        =   190
            Top             =   1590
            Width           =   1185
         End
         Begin VB.Label lblParametroRestituicao 
            AutoSize        =   -1  'True
            Caption         =   "Custo Certificado:"
            Height          =   195
            Index           =   31
            Left            =   3930
            TabIndex        =   189
            Top             =   1620
            Width           =   1245
         End
         Begin VB.Label lblValParametroRestituicao 
            BorderStyle     =   1  'Fixed Single
            Height          =   255
            Index           =   1
            Left            =   2430
            TabIndex        =   188
            Top             =   960
            Width           =   465
         End
         Begin VB.Label lblParametroRestituicao 
            AutoSize        =   -1  'True
            Caption         =   "Prazo do Seguro:"
            Height          =   195
            Index           =   1
            Left            =   1050
            TabIndex        =   187
            Top             =   960
            Width           =   1230
         End
         Begin VB.Label lblParametroRestituicao 
            AutoSize        =   -1  'True
            Caption         =   "Per�odo de Utiliza��o:"
            Height          =   195
            Index           =   5
            Left            =   3600
            TabIndex        =   186
            Top             =   1020
            Width           =   1575
         End
         Begin VB.Label lblValParametroRestituicao 
            BorderStyle     =   1  'Fixed Single
            Height          =   255
            Index           =   5
            Left            =   5190
            TabIndex        =   185
            Top             =   960
            Width           =   435
         End
         Begin VB.Label lblParametroRestituicao 
            AutoSize        =   -1  'True
            Caption         =   "Prazo de Restitui��o Integral:"
            Height          =   195
            Index           =   0
            Left            =   195
            TabIndex        =   184
            Top             =   630
            Width           =   2085
         End
         Begin VB.Label lblValParametroRestituicao 
            BorderStyle     =   1  'Fixed Single
            Height          =   285
            Index           =   4
            Left            =   5190
            TabIndex        =   183
            Top             =   645
            Width           =   1185
         End
         Begin VB.Label lblParametroRestituicao 
            AutoSize        =   -1  'True
            Caption         =   "Tipo de C�lculo:"
            Height          =   195
            Index           =   4
            Left            =   4020
            TabIndex        =   182
            Top             =   690
            Width           =   1155
         End
         Begin VB.Label lblParametroRestituicao 
            AutoSize        =   -1  'True
            Caption         =   "Total Premio Pago:"
            Height          =   195
            Index           =   2
            Left            =   930
            TabIndex        =   181
            Top             =   1290
            Width           =   1350
         End
         Begin VB.Label lblParametroRestituicao 
            AutoSize        =   -1  'True
            Caption         =   "Total Juros Pago:"
            Height          =   195
            Index           =   3
            Left            =   1020
            TabIndex        =   180
            Top             =   1590
            Width           =   1245
         End
         Begin VB.Label lblValParametroRestituicao 
            Alignment       =   1  'Right Justify
            BorderStyle     =   1  'Fixed Single
            Height          =   255
            Index           =   2
            Left            =   2430
            TabIndex        =   179
            Top             =   1230
            Width           =   1185
         End
         Begin VB.Label lblValParametroRestituicao 
            Alignment       =   1  'Right Justify
            BorderStyle     =   1  'Fixed Single
            Height          =   255
            Index           =   3
            Left            =   2430
            TabIndex        =   178
            Top             =   1560
            Width           =   1185
         End
         Begin VB.Label Label48lblParametroRestituicao 
            Caption         =   "(Dias)"
            Height          =   225
            Left            =   2970
            TabIndex        =   177
            Top             =   630
            Width           =   435
         End
         Begin VB.Label lblParametroRestituicao 
            Caption         =   "(Dias)"
            Height          =   225
            Index           =   7
            Left            =   3000
            TabIndex        =   176
            Top             =   960
            Width           =   435
         End
         Begin VB.Label lblParametroRestituicao 
            Caption         =   "(Dias)"
            Height          =   225
            Index           =   8
            Left            =   5670
            TabIndex        =   175
            Top             =   660
            Width           =   465
         End
         Begin VB.Label lblParametroRestituicao 
            AutoSize        =   -1  'True
            Caption         =   "Pr�mio Emitido:"
            Height          =   195
            Index           =   6
            Left            =   4095
            TabIndex        =   174
            Top             =   1320
            Width           =   1080
         End
         Begin VB.Label lblValParametroRestituicao 
            Alignment       =   1  'Right Justify
            BorderStyle     =   1  'Fixed Single
            Height          =   255
            Index           =   6
            Left            =   5190
            TabIndex        =   173
            Top             =   1320
            Width           =   1185
         End
      End
      Begin VB.Frame fraRestituicaoModular 
         Height          =   615
         Index           =   0
         Left            =   -74880
         TabIndex        =   169
         Top             =   360
         Width           =   14595
         Begin VB.TextBox txtTpEmdossoRestModular 
            ForeColor       =   &H000000FF&
            Height          =   315
            Left            =   2460
            Locked          =   -1  'True
            TabIndex        =   170
            TabStop         =   0   'False
            Tag             =   "Data de Nascimento"
            Top             =   180
            Width           =   12000
         End
         Begin VB.Label lblTexto 
            AutoSize        =   -1  'True
            Caption         =   "Tipo de endosso a ser gerado :"
            Height          =   195
            Index           =   30
            Left            =   150
            TabIndex        =   171
            Top             =   270
            Width           =   2205
         End
      End
      Begin VB.Frame Frame12 
         Height          =   615
         Left            =   2880
         TabIndex        =   145
         Top             =   360
         Width           =   9315
         Begin VB.TextBox txtTpEmdossoRest 
            ForeColor       =   &H000000FF&
            Height          =   315
            Left            =   2460
            Locked          =   -1  'True
            TabIndex        =   146
            TabStop         =   0   'False
            Tag             =   "Data de Nascimento"
            Top             =   180
            Width           =   6600
         End
         Begin VB.Label lblTexto 
            AutoSize        =   -1  'True
            Caption         =   "Tipo de endosso a ser gerado :"
            Height          =   195
            Index           =   29
            Left            =   150
            TabIndex        =   147
            Top             =   270
            Width           =   2205
         End
      End
      Begin VB.Frame Frame3 
         Height          =   615
         Left            =   -72090
         TabIndex        =   142
         Top             =   5250
         Width           =   9315
         Begin VB.TextBox txtTipoEndosso 
            Height          =   315
            Left            =   2460
            Locked          =   -1  'True
            TabIndex        =   143
            TabStop         =   0   'False
            Tag             =   "Data de Nascimento"
            Top             =   180
            Width           =   6600
         End
         Begin VB.Label lblTexto 
            AutoSize        =   -1  'True
            Caption         =   "Tipo de endosso a ser gerado :"
            Height          =   195
            Index           =   28
            Left            =   150
            TabIndex        =   144
            Top             =   270
            Width           =   2205
         End
      End
      Begin MSFlexGridLib.MSFlexGrid GridHistoricoEndosso 
         Height          =   3375
         Left            =   -74790
         TabIndex        =   28
         Top             =   540
         Width           =   14415
         _ExtentX        =   25426
         _ExtentY        =   5953
         _Version        =   393216
         Cols            =   6
         FixedCols       =   0
         AllowUserResizing=   1
         Appearance      =   0
         FormatString    =   $"AvaliacaoCancelamento.frx":04EA
      End
      Begin VB.Frame Frame6 
         Caption         =   "Legenda"
         Height          =   885
         Left            =   -74850
         TabIndex        =   128
         Top             =   4350
         Width           =   3435
         Begin VB.Shape Shape1 
            BackColor       =   &H80000002&
            BackStyle       =   1  'Opaque
            Height          =   165
            Left            =   180
            Top             =   300
            Width           =   225
         End
         Begin VB.Label Label29 
            Caption         =   "Pendentes de Avalia��o Gerencial"
            Height          =   165
            Left            =   480
            TabIndex        =   130
            Top             =   300
            Width           =   2805
         End
         Begin VB.Shape Shape2 
            BackColor       =   &H000000C0&
            BackStyle       =   1  'Opaque
            Height          =   165
            Left            =   180
            Top             =   570
            Width           =   225
         End
         Begin VB.Label Label24 
            Caption         =   "Indeferidos pela Avalia��o Gerencial"
            Height          =   225
            Left            =   480
            TabIndex        =   129
            Top             =   540
            Width           =   2775
         End
      End
      Begin VB.Frame Frame9 
         Caption         =   "Demonstrativo da Restitui��o"
         Height          =   4335
         Left            =   2880
         TabIndex        =   101
         Top             =   3120
         Width           =   9315
         Begin VB.Frame Frame1 
            Caption         =   "Valor Informado"
            Height          =   4035
            Left            =   4740
            TabIndex        =   115
            Top             =   210
            Width           =   4425
            Begin Mask2S.ConMask2S Mask2ValPremioTarifa 
               Height          =   345
               Left            =   2280
               TabIndex        =   40
               Top             =   180
               Width           =   1785
               _ExtentX        =   3149
               _ExtentY        =   609
               mask            =   ""
               text            =   "0,00"
               locked          =   0   'False
               enabled         =   -1  'True
            End
            Begin Mask2S.ConMask2S maskValSubvencaoEstadual 
               Height          =   345
               Left            =   2250
               TabIndex        =   156
               Top             =   2640
               Visible         =   0   'False
               Width           =   1785
               _ExtentX        =   3149
               _ExtentY        =   609
               mask            =   ""
               text            =   "0,00"
               locked          =   -1  'True
               enabled         =   -1  'True
            End
            Begin VB.Label LblValRemuneracao 
               Alignment       =   1  'Right Justify
               BorderStyle     =   1  'Fixed Single
               Height          =   285
               Left            =   2250
               TabIndex        =   168
               Top             =   3670
               Width           =   1815
            End
            Begin VB.Label Label40 
               Alignment       =   1  'Right Justify
               AutoSize        =   -1  'True
               Caption         =   "Valor Remunera��o:"
               Height          =   195
               Left            =   675
               TabIndex        =   167
               Top             =   3690
               Width           =   1455
            End
            Begin VB.Label lblPercFederal 
               Alignment       =   1  'Right Justify
               BorderStyle     =   1  'Fixed Single
               Caption         =   "0,00"
               Height          =   255
               Left            =   60
               TabIndex        =   164
               Top             =   2340
               Visible         =   0   'False
               Width           =   555
            End
            Begin VB.Label lblEstadual 
               AutoSize        =   -1  'True
               Caption         =   "Subven��o Estadual:"
               Height          =   195
               Left            =   570
               TabIndex        =   163
               Top             =   2700
               Visible         =   0   'False
               Width           =   1530
            End
            Begin VB.Label lblValSubvencaoFederal 
               Alignment       =   1  'Right Justify
               BorderStyle     =   1  'Fixed Single
               Height          =   285
               Left            =   2250
               TabIndex        =   162
               Top             =   2340
               Visible         =   0   'False
               Width           =   1785
            End
            Begin VB.Label lblFederal 
               AutoSize        =   -1  'True
               Caption         =   "Subven��o Federal:"
               Height          =   195
               Left            =   660
               TabIndex        =   161
               Top             =   2370
               Visible         =   0   'False
               Width           =   1500
            End
            Begin VB.Label LblTotValCorretagem 
               Alignment       =   1  'Right Justify
               BorderStyle     =   1  'Fixed Single
               Height          =   285
               Left            =   2250
               TabIndex        =   160
               Top             =   3000
               Width           =   1785
            End
            Begin VB.Label Label27 
               AutoSize        =   -1  'True
               Caption         =   "Corretagem:"
               Height          =   195
               Left            =   1260
               TabIndex        =   159
               Top             =   3030
               Width           =   855
            End
            Begin VB.Label LblValProLabore 
               Alignment       =   1  'Right Justify
               BorderStyle     =   1  'Fixed Single
               Height          =   285
               Left            =   2250
               TabIndex        =   158
               Top             =   3330
               Width           =   1785
            End
            Begin VB.Label Label28 
               AutoSize        =   -1  'True
               Caption         =   "Pr�-Labore:"
               Height          =   195
               Left            =   1290
               TabIndex        =   157
               Top             =   3390
               Width           =   825
            End
            Begin VB.Label LblValRestituicao 
               Alignment       =   1  'Right Justify
               BorderStyle     =   1  'Fixed Single
               Height          =   255
               Left            =   2250
               TabIndex        =   50
               Top             =   1920
               Width           =   1785
            End
            Begin VB.Label Label20 
               Caption         =   "="
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   12
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Left            =   4170
               TabIndex        =   127
               Top             =   1890
               Width           =   195
            End
            Begin VB.Label Label19 
               Caption         =   "-"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   12
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Left            =   4200
               TabIndex        =   126
               Top             =   1110
               Width           =   195
            End
            Begin VB.Label Label18 
               Caption         =   "+"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   12
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Left            =   4170
               TabIndex        =   125
               Top             =   1440
               Width           =   195
            End
            Begin VB.Label Label14 
               Caption         =   "+"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   12
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Left            =   4170
               TabIndex        =   124
               Top             =   840
               Width           =   195
            End
            Begin VB.Label Label13 
               Caption         =   "+"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   12
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Left            =   4170
               TabIndex        =   123
               Top             =   510
               Width           =   195
            End
            Begin VB.Label Label12 
               Caption         =   "+"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   12
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Left            =   4170
               TabIndex        =   122
               Top             =   180
               Width           =   195
            End
            Begin VB.Label Label8 
               AutoSize        =   -1  'True
               Caption         =   "Valor Devolu��o Informado:"
               Height          =   195
               Left            =   120
               TabIndex        =   121
               Top             =   1920
               Width           =   1980
            End
            Begin VB.Label LblValIOF 
               Alignment       =   1  'Right Justify
               BorderStyle     =   1  'Fixed Single
               Height          =   285
               Left            =   2250
               TabIndex        =   48
               Top             =   1440
               Width           =   1785
            End
            Begin VB.Label Label17 
               AutoSize        =   -1  'True
               Caption         =   "IOF:"
               Height          =   195
               Left            =   1800
               TabIndex        =   120
               Top             =   1500
               Width           =   300
            End
            Begin VB.Label Label23 
               AutoSize        =   -1  'True
               Caption         =   "Premio Tarifa:"
               Height          =   195
               Left            =   1125
               TabIndex        =   119
               Top             =   240
               Width           =   975
            End
            Begin VB.Label LblCustoCertifApoliceRest 
               Alignment       =   1  'Right Justify
               BorderStyle     =   1  'Fixed Single
               Height          =   255
               Left            =   2250
               TabIndex        =   42
               Top             =   540
               Width           =   1785
            End
            Begin VB.Label lblTexto 
               AutoSize        =   -1  'True
               Caption         =   "Custo Certificado:"
               Height          =   195
               Index           =   12
               Left            =   855
               TabIndex        =   118
               Top             =   570
               Width           =   1245
            End
            Begin VB.Label LblValAdicFracionamento 
               Alignment       =   1  'Right Justify
               BorderStyle     =   1  'Fixed Single
               Height          =   255
               Left            =   2250
               TabIndex        =   44
               Top             =   840
               Width           =   1785
            End
            Begin VB.Label lblTexto 
               AutoSize        =   -1  'True
               Caption         =   "Adic. Fracionamento:"
               Height          =   195
               Index           =   21
               Left            =   600
               TabIndex        =   117
               Top             =   870
               Width           =   1500
            End
            Begin VB.Label LblValDesconto 
               Alignment       =   1  'Right Justify
               BorderStyle     =   1  'Fixed Single
               Height          =   255
               Left            =   2250
               TabIndex        =   46
               Top             =   1140
               Width           =   1785
            End
            Begin VB.Label lblTexto 
               AutoSize        =   -1  'True
               Caption         =   "Desconto:"
               Height          =   195
               Index           =   22
               Left            =   1365
               TabIndex        =   116
               Top             =   1170
               Width           =   735
            End
            Begin VB.Line Line1 
               X1              =   120
               X2              =   4050
               Y1              =   1800
               Y2              =   1800
            End
         End
         Begin VB.Frame Frame8 
            Caption         =   "Valor Calculado"
            Height          =   4035
            Left            =   150
            TabIndex        =   102
            Top             =   210
            Width           =   4425
            Begin VB.Label LblValRemuneracaoCalc 
               Alignment       =   1  'Right Justify
               BorderStyle     =   1  'Fixed Single
               Height          =   285
               Left            =   2280
               TabIndex        =   166
               Top             =   3670
               Width           =   1815
            End
            Begin VB.Label Label1 
               Alignment       =   1  'Right Justify
               AutoSize        =   -1  'True
               Caption         =   "Valor Remunera��o:"
               Height          =   195
               Left            =   690
               TabIndex        =   165
               Top             =   3690
               Width           =   1455
            End
            Begin VB.Label lblValSubvencaoEstadualCalc 
               Alignment       =   1  'Right Justify
               BorderStyle     =   1  'Fixed Single
               Height          =   285
               Left            =   2280
               TabIndex        =   155
               Top             =   2670
               Visible         =   0   'False
               Width           =   1815
            End
            Begin VB.Label lblEstadualCalc 
               AutoSize        =   -1  'True
               Caption         =   "Subven��o Estadual:"
               Height          =   195
               Left            =   600
               TabIndex        =   154
               Top             =   2700
               Visible         =   0   'False
               Width           =   1530
            End
            Begin VB.Label lblValSubvencaoFederalCalc 
               Alignment       =   1  'Right Justify
               BorderStyle     =   1  'Fixed Single
               Height          =   285
               Left            =   2280
               TabIndex        =   153
               Top             =   2340
               Visible         =   0   'False
               Width           =   1815
            End
            Begin VB.Label lblFederalCalc 
               AutoSize        =   -1  'True
               Caption         =   "Subven��o Federal:"
               Height          =   195
               Left            =   690
               TabIndex        =   152
               Top             =   2370
               Visible         =   0   'False
               Width           =   1440
            End
            Begin VB.Label Label39 
               AutoSize        =   -1  'True
               Caption         =   "Pr�-Labore:"
               Height          =   195
               Left            =   1290
               TabIndex        =   151
               Top             =   3360
               Width           =   855
            End
            Begin VB.Label LblValProLaboreCalc 
               Alignment       =   1  'Right Justify
               BorderStyle     =   1  'Fixed Single
               Height          =   285
               Left            =   2280
               TabIndex        =   150
               Top             =   3330
               Width           =   1815
            End
            Begin VB.Label Label37 
               AutoSize        =   -1  'True
               Caption         =   "Corretagem:"
               Height          =   195
               Left            =   1290
               TabIndex        =   149
               Top             =   3000
               Width           =   855
            End
            Begin VB.Label LblTotValCorretagemCalc 
               Alignment       =   1  'Right Justify
               BorderStyle     =   1  'Fixed Single
               Height          =   285
               Left            =   2280
               TabIndex        =   148
               Top             =   3000
               Width           =   1815
            End
            Begin VB.Label LblValRestituicaoCalc 
               Alignment       =   1  'Right Justify
               BorderStyle     =   1  'Fixed Single
               Height          =   255
               Left            =   2280
               TabIndex        =   49
               Top             =   1890
               Width           =   1785
            End
            Begin VB.Line Line2 
               X1              =   120
               X2              =   4050
               Y1              =   1800
               Y2              =   1800
            End
            Begin VB.Label lblTexto 
               AutoSize        =   -1  'True
               Caption         =   "Desconto:"
               Height          =   195
               Index           =   25
               Left            =   1380
               TabIndex        =   114
               Top             =   1170
               Width           =   735
            End
            Begin VB.Label LblValDescontoCalc 
               Alignment       =   1  'Right Justify
               BorderStyle     =   1  'Fixed Single
               Height          =   255
               Left            =   2280
               TabIndex        =   45
               Top             =   1140
               Width           =   1785
            End
            Begin VB.Label lblTexto 
               AutoSize        =   -1  'True
               Caption         =   "Adic. Fracionamento:"
               Height          =   195
               Index           =   18
               Left            =   615
               TabIndex        =   113
               Top             =   870
               Width           =   1500
            End
            Begin VB.Label LblValAdicFracionamentoCalc 
               Alignment       =   1  'Right Justify
               BorderStyle     =   1  'Fixed Single
               Height          =   255
               Left            =   2280
               TabIndex        =   43
               Top             =   840
               Width           =   1785
            End
            Begin VB.Label lblTexto 
               AutoSize        =   -1  'True
               Caption         =   "Custo Certificado:"
               Height          =   195
               Index           =   17
               Left            =   870
               TabIndex        =   112
               Top             =   570
               Width           =   1245
            End
            Begin VB.Label LblCustoCertifApoliceCalc 
               Alignment       =   1  'Right Justify
               BorderStyle     =   1  'Fixed Single
               Height          =   255
               Left            =   2280
               TabIndex        =   41
               Top             =   540
               Width           =   1785
            End
            Begin VB.Label LblValPremioTarifaCalc 
               Alignment       =   1  'Right Justify
               BorderStyle     =   1  'Fixed Single
               Height          =   285
               Left            =   2280
               TabIndex        =   39
               Top             =   210
               Width           =   1785
            End
            Begin VB.Label Label36 
               AutoSize        =   -1  'True
               Caption         =   "Premio Tarifa:"
               Height          =   195
               Left            =   1140
               TabIndex        =   111
               Top             =   240
               Width           =   975
            End
            Begin VB.Label Label35 
               AutoSize        =   -1  'True
               Caption         =   "IOF:"
               Height          =   195
               Left            =   1815
               TabIndex        =   110
               Top             =   1530
               Width           =   300
            End
            Begin VB.Label LblValIOFCalc 
               Alignment       =   1  'Right Justify
               BorderStyle     =   1  'Fixed Single
               Height          =   285
               Left            =   2280
               TabIndex        =   47
               Top             =   1470
               Width           =   1785
            End
            Begin VB.Label Label33 
               AutoSize        =   -1  'True
               Caption         =   "Valor Devolu��o Calculado:"
               Height          =   195
               Left            =   120
               TabIndex        =   109
               Top             =   1890
               Width           =   1980
            End
            Begin VB.Label Label32 
               Caption         =   "+"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   12
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Left            =   4170
               TabIndex        =   108
               Top             =   180
               Width           =   195
            End
            Begin VB.Label Label31 
               Caption         =   "+"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   12
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Left            =   4170
               TabIndex        =   107
               Top             =   510
               Width           =   195
            End
            Begin VB.Label Label30 
               Caption         =   "+"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   12
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Left            =   4170
               TabIndex        =   106
               Top             =   840
               Width           =   195
            End
            Begin VB.Label Label26 
               Caption         =   "+"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   12
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Left            =   4170
               TabIndex        =   105
               Top             =   1470
               Width           =   195
            End
            Begin VB.Label Label22 
               Caption         =   "-"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   12
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Left            =   4200
               TabIndex        =   104
               Top             =   1140
               Width           =   195
            End
            Begin VB.Label Label21 
               Caption         =   "="
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   12
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Left            =   4170
               TabIndex        =   103
               Top             =   1860
               Width           =   195
            End
         End
      End
      Begin VB.Frame Frame5 
         Caption         =   "Parametros de Restitui��o"
         Height          =   1575
         Left            =   2880
         TabIndex        =   93
         Top             =   960
         Width           =   9315
         Begin VB.Label LblValPremioEmitido 
            Alignment       =   1  'Right Justify
            BorderStyle     =   1  'Fixed Single
            Height          =   255
            Left            =   6990
            TabIndex        =   35
            Top             =   930
            Width           =   1785
         End
         Begin VB.Label Label34 
            AutoSize        =   -1  'True
            Caption         =   "Pr�mio Emitido:"
            Height          =   195
            Left            =   5730
            TabIndex        =   134
            Top             =   960
            Width           =   1080
         End
         Begin VB.Label Label43 
            Caption         =   "(Dias)"
            Height          =   225
            Left            =   7470
            TabIndex        =   133
            Top             =   660
            Width           =   465
         End
         Begin VB.Label Label42 
            Caption         =   "(Dias)"
            Height          =   225
            Left            =   2970
            TabIndex        =   132
            Top             =   600
            Width           =   435
         End
         Begin VB.Label Label41 
            Caption         =   "(Dias)"
            Height          =   225
            Left            =   2970
            TabIndex        =   131
            Top             =   270
            Width           =   435
         End
         Begin VB.Label LblTotValJuros 
            Alignment       =   1  'Right Justify
            BorderStyle     =   1  'Fixed Single
            Height          =   255
            Left            =   2430
            TabIndex        =   36
            Top             =   1200
            Width           =   1785
         End
         Begin VB.Label LblTotValPremioPago 
            Alignment       =   1  'Right Justify
            BorderStyle     =   1  'Fixed Single
            Height          =   255
            Left            =   2430
            TabIndex        =   34
            Top             =   870
            Width           =   1785
         End
         Begin VB.Label Label16 
            AutoSize        =   -1  'True
            Caption         =   "Total Juros Pago:"
            Height          =   195
            Left            =   1020
            TabIndex        =   100
            Top             =   1230
            Width           =   1245
         End
         Begin VB.Label Label15 
            AutoSize        =   -1  'True
            Caption         =   "Total Premio Pago:"
            Height          =   195
            Left            =   930
            TabIndex        =   99
            Top             =   930
            Width           =   1350
         End
         Begin VB.Label Label2 
            AutoSize        =   -1  'True
            Caption         =   "Tipo de C�lculo de Restitui��o:"
            Height          =   195
            Left            =   4635
            TabIndex        =   98
            Top             =   330
            Width           =   2220
         End
         Begin VB.Label LblTpCalcRestituicao 
            BorderStyle     =   1  'Fixed Single
            Height          =   285
            Left            =   6990
            TabIndex        =   31
            Top             =   300
            Width           =   1785
         End
         Begin VB.Label Label9 
            AutoSize        =   -1  'True
            Caption         =   "Prazo de Restitui��o Integral:"
            Height          =   195
            Left            =   195
            TabIndex        =   97
            Top             =   270
            Width           =   2085
         End
         Begin VB.Label LblPrazoRestituicaoIntegral 
            BorderStyle     =   1  'Fixed Single
            Height          =   255
            Left            =   2430
            TabIndex        =   30
            Top             =   270
            Width           =   465
         End
         Begin VB.Label LblDiasUtilizacaoSeguro 
            BorderStyle     =   1  'Fixed Single
            Height          =   255
            Left            =   6990
            TabIndex        =   33
            Top             =   630
            Width           =   435
         End
         Begin VB.Label Label11 
            AutoSize        =   -1  'True
            Caption         =   "Per�odo de Utiliza��o de Seguro:"
            Height          =   195
            Left            =   4500
            TabIndex        =   96
            Top             =   660
            Width           =   2355
         End
         Begin VB.Label Label10 
            AutoSize        =   -1  'True
            Caption         =   "Prazo do Seguro:"
            Height          =   195
            Left            =   1050
            TabIndex        =   95
            Top             =   600
            Width           =   1230
         End
         Begin VB.Label LblPrazoSeguro 
            BorderStyle     =   1  'Fixed Single
            Height          =   255
            Left            =   2430
            TabIndex        =   32
            Top             =   570
            Width           =   465
         End
         Begin VB.Label lblTexto 
            AutoSize        =   -1  'True
            Caption         =   "Custo Certificado:"
            Height          =   195
            Index           =   16
            Left            =   5610
            TabIndex        =   94
            Top             =   1260
            Width           =   1245
         End
         Begin VB.Label LblCustoCertifApolice 
            Alignment       =   1  'Right Justify
            BorderStyle     =   1  'Fixed Single
            Height          =   255
            Left            =   6990
            TabIndex        =   37
            Top             =   1230
            Width           =   1785
         End
      End
      Begin VB.Frame Frame4 
         Caption         =   "Estimativa"
         Height          =   615
         Left            =   2880
         TabIndex        =   91
         Top             =   2520
         Width           =   9315
         Begin VB.Label lbltipoProposta 
            Height          =   255
            Index           =   2
            Left            =   4560
            TabIndex        =   332
            Top             =   210
            Width           =   1695
         End
         Begin VB.Label Label25 
            AutoSize        =   -1  'True
            Caption         =   "Valor de Devolu��o:"
            Height          =   195
            Left            =   780
            TabIndex        =   92
            Top             =   210
            Width           =   1455
         End
         Begin VB.Label LblValRestituicaoEstimado 
            Alignment       =   1  'Right Justify
            BorderStyle     =   1  'Fixed Single
            Height          =   255
            Left            =   2430
            TabIndex        =   38
            Top             =   210
            Width           =   1785
         End
      End
      Begin VB.Frame frameProposta 
         Caption         =   "Dados B�sicos da Proposta"
         Height          =   2385
         Left            =   -72090
         TabIndex        =   78
         Top             =   480
         Width           =   9315
         Begin VB.TextBox txtSubramoId 
            Height          =   300
            Left            =   7800
            Locked          =   -1  'True
            TabIndex        =   331
            TabStop         =   0   'False
            Tag             =   "Situa��o da Proposta"
            Top             =   570
            Visible         =   0   'False
            Width           =   1230
         End
         Begin VB.TextBox TxtDtFimVigencia 
            Height          =   300
            Left            =   7800
            Locked          =   -1  'True
            TabIndex        =   329
            TabStop         =   0   'False
            Tag             =   "Data de Fim de Vig�ncia:"
            Top             =   900
            Width           =   1230
         End
         Begin VB.TextBox TxtRamo 
            Height          =   300
            Left            =   7800
            Locked          =   -1  'True
            TabIndex        =   328
            TabStop         =   0   'False
            Tag             =   "Situa��o da Proposta"
            Top             =   240
            Width           =   1230
         End
         Begin VB.TextBox txtTipoApolice 
            Height          =   300
            Left            =   1650
            Locked          =   -1  'True
            TabIndex        =   140
            TabStop         =   0   'False
            Tag             =   "Situa��o da Proposta"
            Top             =   1880
            Width           =   4305
         End
         Begin VB.TextBox TxtApolice 
            Height          =   300
            Left            =   4710
            Locked          =   -1  'True
            TabIndex        =   5
            TabStop         =   0   'False
            Tag             =   "Situa��o da Proposta"
            Top             =   240
            Width           =   1230
         End
         Begin VB.TextBox txtAgencia 
            Height          =   285
            Left            =   7800
            Locked          =   -1  'True
            TabIndex        =   12
            TabStop         =   0   'False
            Tag             =   "Ag�ncia Contratante"
            Top             =   1590
            Width           =   1245
         End
         Begin VB.TextBox txtDtContratacao 
            Height          =   300
            Left            =   1650
            Locked          =   -1  'True
            TabIndex        =   8
            TabStop         =   0   'False
            Tag             =   "Data de Contrata��o"
            Top             =   900
            Width           =   1230
         End
         Begin VB.TextBox TxtNomeProduto 
            Height          =   300
            Left            =   1650
            Locked          =   -1  'True
            TabIndex        =   11
            TabStop         =   0   'False
            Tag             =   "Situa��o da Proposta"
            Top             =   1530
            Width           =   4305
         End
         Begin VB.TextBox txtStatus 
            Height          =   300
            Left            =   1650
            Locked          =   -1  'True
            TabIndex        =   10
            TabStop         =   0   'False
            Tag             =   "Situa��o da Proposta"
            Top             =   1230
            Width           =   7395
         End
         Begin VB.TextBox txtProposta 
            Height          =   300
            Left            =   1650
            Locked          =   -1  'True
            TabIndex        =   4
            TabStop         =   0   'False
            Tag             =   "Nr. da Proposta"
            Top             =   270
            Width           =   1230
         End
         Begin VB.TextBox txtIs 
            Height          =   285
            Left            =   1650
            Locked          =   -1  'True
            TabIndex        =   6
            TabStop         =   0   'False
            Tag             =   "Capital Segurado"
            Top             =   600
            Width           =   1965
         End
         Begin VB.TextBox txtPremio 
            Height          =   285
            Left            =   4710
            Locked          =   -1  'True
            TabIndex        =   7
            TabStop         =   0   'False
            Tag             =   "Pr�mio"
            Top             =   570
            Width           =   1230
         End
         Begin VB.TextBox TxtDtInicioVigencia 
            Height          =   315
            Left            =   4710
            TabIndex        =   9
            TabStop         =   0   'False
            Tag             =   "Data de Inicio de Vig�ncia:"
            Top             =   870
            Width           =   1230
         End
         Begin VB.Label lblTexto 
            AutoSize        =   -1  'True
            Caption         =   "Subramo: "
            Height          =   195
            Index           =   33
            Left            =   7080
            TabIndex        =   330
            Top             =   623
            Visible         =   0   'False
            Width           =   720
         End
         Begin VB.Label lblTexto 
            AutoSize        =   -1  'True
            Caption         =   "Tipo de Ap�lice:"
            Height          =   195
            Index           =   26
            Left            =   375
            TabIndex        =   141
            Top             =   1933
            Width           =   1155
         End
         Begin VB.Label lblTexto 
            AutoSize        =   -1  'True
            Caption         =   "Ramo:"
            Height          =   195
            Index           =   15
            Left            =   7260
            TabIndex        =   90
            Top             =   300
            Width           =   465
         End
         Begin VB.Label lblTexto 
            AutoSize        =   -1  'True
            Caption         =   "Apolice:"
            Height          =   195
            Index           =   14
            Left            =   4080
            TabIndex        =   89
            Top             =   300
            Width           =   570
         End
         Begin VB.Label Label46 
            AutoSize        =   -1  'True
            Caption         =   "Ag�ncia Contratante:"
            Height          =   195
            Left            =   6240
            TabIndex        =   87
            Top             =   1650
            Width           =   1500
         End
         Begin VB.Label lblTexto 
            AutoSize        =   -1  'True
            Caption         =   "Dt.Contrata��o:"
            Height          =   195
            Index           =   23
            Left            =   405
            TabIndex        =   86
            Top             =   930
            Width           =   1125
         End
         Begin VB.Label lblTexto 
            AutoSize        =   -1  'True
            Caption         =   "Produto:"
            Height          =   195
            Index           =   7
            Left            =   930
            TabIndex        =   85
            Top             =   1560
            Width           =   600
         End
         Begin VB.Label lblTexto 
            AutoSize        =   -1  'True
            Caption         =   "Situa��o:"
            Height          =   195
            Index           =   24
            Left            =   855
            TabIndex        =   84
            Top             =   1230
            Width           =   675
         End
         Begin VB.Label lblTexto 
            AutoSize        =   -1  'True
            Caption         =   "Proposta:"
            Height          =   195
            Index           =   19
            Left            =   855
            TabIndex        =   83
            Top             =   300
            Width           =   675
         End
         Begin VB.Label Label5 
            AutoSize        =   -1  'True
            Caption         =   "Capital Segurado:"
            Height          =   195
            Left            =   270
            TabIndex        =   82
            Top             =   600
            Width           =   1260
         End
         Begin VB.Label Label4 
            AutoSize        =   -1  'True
            Caption         =   "Pr�mio:"
            Height          =   195
            Left            =   4140
            TabIndex        =   81
            Top             =   630
            Width           =   525
         End
         Begin VB.Label Label6 
            AutoSize        =   -1  'True
            Caption         =   "Inicio de Vig�ncia:"
            Height          =   195
            Left            =   3360
            TabIndex        =   80
            Top             =   930
            Width           =   1305
         End
         Begin VB.Label Label7 
            AutoSize        =   -1  'True
            Caption         =   "Fim de Vig�ncia:"
            Height          =   195
            Left            =   6540
            TabIndex        =   79
            Top             =   960
            Width           =   1170
         End
      End
      Begin VB.Frame Frame2 
         Caption         =   "Segurado"
         Height          =   1575
         Index           =   0
         Left            =   -72090
         TabIndex        =   68
         Top             =   2910
         Width           =   9315
         Begin VB.TextBox txtCCDeb 
            Height          =   285
            Left            =   7740
            Locked          =   -1  'True
            TabIndex        =   21
            TabStop         =   0   'False
            Tag             =   "Conta Corrente"
            Top             =   1170
            Width           =   1350
         End
         Begin VB.TextBox txtAgenciaDeb 
            Height          =   285
            Left            =   5910
            Locked          =   -1  'True
            TabIndex        =   20
            TabStop         =   0   'False
            Tag             =   "Ag�ncia do Cliente"
            Top             =   1200
            Width           =   735
         End
         Begin VB.TextBox txtCliente 
            Height          =   300
            Left            =   1650
            Locked          =   -1  'True
            TabIndex        =   13
            TabStop         =   0   'False
            Tag             =   "Nome do Cliente"
            Top             =   180
            Width           =   7425
         End
         Begin VB.TextBox txtEndereco 
            Height          =   300
            Left            =   1650
            Locked          =   -1  'True
            TabIndex        =   14
            TabStop         =   0   'False
            Tag             =   "Endereco"
            Top             =   510
            Width           =   7425
         End
         Begin VB.TextBox txtMunicipio 
            Height          =   300
            Left            =   1650
            Locked          =   -1  'True
            TabIndex        =   15
            TabStop         =   0   'False
            Tag             =   "Cidade"
            Top             =   840
            Width           =   4065
         End
         Begin VB.TextBox txtUF 
            Height          =   330
            Left            =   6270
            Locked          =   -1  'True
            TabIndex        =   16
            TabStop         =   0   'False
            Tag             =   "UF"
            Top             =   840
            Width           =   375
         End
         Begin VB.TextBox txtCEP 
            Height          =   300
            Left            =   7740
            Locked          =   -1  'True
            TabIndex        =   17
            TabStop         =   0   'False
            Tag             =   "CEP"
            Top             =   840
            Width           =   1350
         End
         Begin VB.TextBox txtDDD 
            Height          =   300
            Left            =   1650
            Locked          =   -1  'True
            TabIndex        =   18
            TabStop         =   0   'False
            Tag             =   "DDD"
            Top             =   1170
            Width           =   615
         End
         Begin VB.TextBox txtTelefone 
            Height          =   300
            Left            =   3300
            Locked          =   -1  'True
            TabIndex        =   19
            TabStop         =   0   'False
            Tag             =   "Telefone"
            Top             =   1170
            Width           =   1395
         End
         Begin VB.Label lblCCDeb 
            AutoSize        =   -1  'True
            Caption         =   "Conta C.::"
            Height          =   195
            Left            =   6960
            TabIndex        =   77
            Top             =   1230
            Width           =   705
         End
         Begin VB.Label lblAgenciaDeb 
            AutoSize        =   -1  'True
            Caption         =   "Ag�ncia:"
            Height          =   195
            Left            =   5160
            TabIndex        =   76
            Top             =   1260
            Width           =   630
         End
         Begin VB.Label lblTexto 
            AutoSize        =   -1  'True
            Caption         =   "Endere�o:"
            Height          =   195
            Index           =   0
            Left            =   840
            TabIndex        =   75
            Top             =   510
            Width           =   735
         End
         Begin VB.Label lblTexto 
            AutoSize        =   -1  'True
            Caption         =   "DDD:"
            Height          =   195
            Index           =   3
            Left            =   1140
            TabIndex        =   74
            Top             =   1230
            Width           =   405
         End
         Begin VB.Label lblTexto 
            AutoSize        =   -1  'True
            Caption         =   "Cidade:"
            Height          =   195
            Index           =   5
            Left            =   1020
            TabIndex        =   73
            Top             =   900
            Width           =   540
         End
         Begin VB.Label lblTexto 
            AutoSize        =   -1  'True
            Caption         =   "Nome:"
            Height          =   195
            Index           =   27
            Left            =   1110
            TabIndex        =   72
            Top             =   210
            Width           =   465
         End
         Begin VB.Label lblTexto 
            AutoSize        =   -1  'True
            Caption         =   "Telefone:"
            Height          =   195
            Index           =   2
            Left            =   2490
            TabIndex        =   71
            Top             =   1230
            Width           =   675
         End
         Begin VB.Label lblTexto 
            AutoSize        =   -1  'True
            Caption         =   "CEP:"
            Height          =   195
            Index           =   4
            Left            =   7290
            TabIndex        =   70
            Top             =   900
            Width           =   360
         End
         Begin VB.Label lblTexto 
            AutoSize        =   -1  'True
            Caption         =   "UF:"
            Height          =   195
            Index           =   6
            Left            =   5940
            TabIndex        =   69
            Top             =   900
            Width           =   255
         End
      End
      Begin VB.Frame FrameDadosPessoaFisica 
         Caption         =   "Dados de Pessoa F�sica"
         Height          =   615
         Left            =   -72090
         TabIndex        =   61
         Top             =   4560
         Width           =   9315
         Begin VB.TextBox txtCPFTit 
            Height          =   285
            Left            =   5010
            Locked          =   -1  'True
            TabIndex        =   25
            TabStop         =   0   'False
            Tag             =   "CPF"
            Top             =   210
            Width           =   1635
         End
         Begin VB.TextBox txtDtNascTit 
            Height          =   315
            Left            =   7740
            Locked          =   -1  'True
            TabIndex        =   26
            TabStop         =   0   'False
            Tag             =   "Data de Nascimento"
            Top             =   180
            Width           =   1320
         End
         Begin VB.TextBox txtSexo 
            Height          =   285
            Left            =   1680
            Locked          =   -1  'True
            TabIndex        =   23
            TabStop         =   0   'False
            Tag             =   "Sexo"
            Top             =   210
            Width           =   375
         End
         Begin VB.TextBox txtEstCivil 
            Height          =   285
            Left            =   2910
            Locked          =   -1  'True
            TabIndex        =   24
            TabStop         =   0   'False
            Tag             =   "Estado Civil"
            Top             =   210
            Width           =   1575
         End
         Begin VB.Label lblTexto 
            Caption         =   "Dt.Nasc.:"
            Height          =   255
            Index           =   11
            Left            =   6990
            TabIndex        =   65
            Top             =   210
            Width           =   735
         End
         Begin VB.Label lblTexto 
            Caption         =   "CPF:"
            Height          =   195
            Index           =   10
            Left            =   4590
            TabIndex        =   64
            Top             =   240
            Width           =   345
         End
         Begin VB.Label lblTexto 
            AutoSize        =   -1  'True
            Caption         =   "Est.Civil:"
            Height          =   195
            Index           =   9
            Left            =   2250
            TabIndex        =   63
            Top             =   240
            Width           =   600
         End
         Begin VB.Label lblTexto 
            Caption         =   "Sexo:"
            Height          =   255
            Index           =   8
            Left            =   1170
            TabIndex        =   62
            Top             =   240
            Width           =   495
         End
      End
      Begin MSFlexGridLib.MSFlexGrid GridCobrancas 
         Height          =   3705
         Left            =   -74790
         TabIndex        =   27
         Top             =   540
         Width           =   14415
         _ExtentX        =   25426
         _ExtentY        =   6535
         _Version        =   393216
         Cols            =   13
         FixedCols       =   0
         ForeColor       =   8388608
         Appearance      =   0
         FormatString    =   $"AvaliacaoCancelamento.frx":0579
      End
      Begin MSFlexGridLib.MSFlexGrid GridHistoricoCancelamento 
         Height          =   3705
         Left            =   -74880
         TabIndex        =   29
         Top             =   540
         Width           =   14535
         _ExtentX        =   25638
         _ExtentY        =   6535
         _Version        =   393216
         Cols            =   9
         FixedCols       =   0
         ForeColor       =   -2147483646
         BackColorSel    =   -2147483646
         AllowUserResizing=   1
         Appearance      =   0
         FormatString    =   $"AvaliacaoCancelamento.frx":0660
      End
      Begin VB.Frame FrameDadosPessoaJuridica 
         Caption         =   "Dados de Pessoa Jur�dica"
         Height          =   630
         Left            =   -72090
         TabIndex        =   66
         Top             =   4560
         Width           =   9315
         Begin VB.TextBox TxtCGC 
            Height          =   300
            Left            =   1650
            Locked          =   -1  'True
            TabIndex        =   22
            TabStop         =   0   'False
            Tag             =   "CGC"
            Top             =   210
            Width           =   1815
         End
         Begin VB.Label lblTexto 
            AutoSize        =   -1  'True
            Caption         =   "CGC:"
            Height          =   195
            Index           =   1
            Left            =   1140
            TabIndex        =   67
            Top             =   240
            Width           =   375
         End
      End
      Begin VB.Frame Frame7 
         Height          =   2025
         Left            =   -71160
         TabIndex        =   319
         Top             =   2880
         Visible         =   0   'False
         Width           =   6615
         Begin VB.Label Label51 
            Alignment       =   2  'Center
            Caption         =   "N�o h� restitui��o porque a data de liquida��o � inferior a 30 dias do fim de vig�ncia da proposta"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   12
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000080&
            Height          =   615
            Left            =   120
            TabIndex        =   320
            Top             =   690
            Width           =   6375
         End
      End
      Begin VB.Frame fraSemRestituicao 
         Height          =   2025
         Left            =   4290
         TabIndex        =   138
         Top             =   1920
         Visible         =   0   'False
         Width           =   6615
         Begin VB.Label Label3 
            Alignment       =   2  'Center
            Caption         =   "N�o h� restitui��o porque a data de liquida��o � inferior a 30 dias do fim de vig�ncia da proposta"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   12
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000080&
            Height          =   615
            Left            =   120
            TabIndex        =   139
            Top             =   690
            Width           =   6375
         End
      End
   End
   Begin VB.Label lblTipoAvaliacao 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   150
      TabIndex        =   136
      Top             =   7680
      Width           =   4095
   End
End
Attribute VB_Name = "FrmAvaliacao"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'Barney - 18/07/2003
Dim Sem_Devolucao As Boolean

'Global para identifica��o de Cancelamento ou Liquida��o
''Public gsTipoProcesso As String

' bcarneiro - 03/12/2003 - Projeto Restitui��o por Iniciativa da Seguradora
Public dPercCorretagemRest As Double
Public dPercIOF As Double



'Renato Vasconcelos
'Flow 11022639
Public strWhere As String
Public idProduto As Integer
Public inicial As Double
Public intervalo As Double
Public maiorQue As Double
Public QTD As Long
Public formAnterior As String

''Sub Aprovar_Cancelamento()
''
''Dim Proposta_Basica_Adesao As String
''Dim Nr_Endosso_BB As String
''Dim Proposta_Id As String
''Dim Endosso_Id As String
''Dim Dt_Contratacao As String
''Dim Dt_Cancelamento_BB As String
''Dim Dt_Inicio_vig As String
''Dim Dt_Fim_vig As String
''
''Dim Val_Custo_Apolice_Certif_Rest As Currency
''Dim Val_Adic_Fracionamento_Rest As Currency
''Dim Val_Desconto_Rest As Currency
''Dim Val_IOF_Rest As Currency
''Dim Val_Premio_Tarifa_Rest As Currency
''Dim Val_Restituicao As Currency
''Dim Val_Tot_Corretagem_Rest As Currency
''Dim Val_ProLabore_Rest As Currency
''
''Dim Corretagem As Corretagem
''Dim Estipulante_Id As String
''Dim Perc_Pro_Labore As Currency
''
''Dim ramo_id As String
''Dim Perc_IR As Currency
''Dim Val_Tot_IR_Corretagem As Currency
''Dim Val_IR_Estipulante As Currency
''
''Dim Val_Is_Restituicao As Currency
''Dim Val_Repasse_BB As Currency
''Dim Movimentacao_Id As String
''Dim iTp_Endosso_id As Integer
''
'''pcarvalho - 17/06/2003 Padroniza��o do c�lculo de restitui��o
''Dim sConvenio As String
''Dim bFlagRetemIOF As Boolean
''Dim bFlagRetemComissao As Boolean
''Dim iMoedaPremioId As Integer
''
'''Atualizando interface'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''
''MousePointer = 11
''
'''Iniciando transa��o'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''
''rdocn.BeginTrans
''
'''Obtendo dados da proposta'''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''
''Nr_Endosso_BB = TxtEndossoBB.Text
''Proposta_Id = txtProposta.Text
''Dt_Contratacao = txtDtContratacao.Text
''Dt_Inicio_vig = TxtDtInicioVigencia.Text
''Dt_Fim_vig = TxtDtFimVigencia.Text
''
''Dt_Cancelamento_BB = TxtDtCancelamentoBB.Text
''
''Call Obter_Dados_Adicionais_Proposta(Proposta_Id, ramo_id, 0, , iMoedaPremioId)
''Call Obter_Premio_IS(Proposta_Id, Produto_Id, Val_Is_Restituicao, 0)
''
''Cliente_ID_Beneficiario = Obter_Beneficiario(Proposta_Id)
''
''If Origem = "1" Then
''   Proposta_Basica_Adesao = BuscaParametro("BASICA " & Produto_Id)
''End If
''
'''Obtendo dados da avalia��o t�cnica aprovada''''''''''''''''''''''''''''''''''''''
''
''Call Obter_Valores_Avaliacao_Tecnica(Proposta_Id, _
''                       Val_Custo_Apolice_Certif_Rest, _
''                       Val_Adic_Fracionamento_Rest, _
''                       Val_Desconto_Rest, _
''                       Val_IOF_Rest, _
''                       Val_Premio_Tarifa_Rest, _
''                       Val_Restituicao, _
''                       0, _
''                       Val_Tot_Corretagem_Rest, _
''                       Val_ProLabore_Rest, _
''                       0, _
''                       0, _
''                       0, _
''                       0, _
''                       0, _
''                       0, _
''                       0)
''
'''Obtendo valor de IR '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''
'''Call Obtem_Perc_IR(ramo_id, Dt_Inicio_vig, Perc_IR)
''Call Obtem_Perc_IR(ramo_id, Data_Sistema, Perc_IR)
''
''Val_Tot_IR_Corretagem = Trunca(Val_Tot_Corretagem_Rest * Perc_IR)
''Val_IR_Estipulante = Trunca(Val_ProLabore_Rest * Perc_IR)  '*** Adriana - Atualiza Base para correta devolu��o de pr�mios
'''Val_IR_Estipulante = 0
''
'''Obtendo valor de repasse para o BB'''''''''''''''''''''''''''''''''''''''''''''''
''
'''pcarvalho - 17/06/2003 Padroniza��o do c�lculo de restitui��o
''sConvenio = Obtem_Numero_do_Convenio(Val(Produto_Id), Val(ramo_id), 1, iMoedaPremioId)
''Call VerificarRetencao(sConvenio, bFlagRetemComissao, bFlagRetemIOF)
''
'''Val_Repasse_BB = Val_Restituicao - Val_Tot_Corretagem_Rest - Val_ProLabore_Rest + Val_Tot_IR_Corretagem + Val_IR_Estipulante - Val_IOF_Rest
''Val_Repasse_BB = CalcularRepasse(Val_Tot_Corretagem_Rest, Val_ProLabore_Rest, _
''                                 Val_Restituicao, Perc_IR, Val_IOF_Rest, _
''                                 bFlagRetemComissao, bFlagRetemIOF)
''
'''Rateando valor de corretagem'''''''''''''''''''''''''''''''''''''''''''''''''''''
''
'''Call Carregar_Rateio_Corretagem(Proposta_Id, Dt_Inicio_vig, 0, 0, 0, 0)
'''bcarneiro 13/03/03
'''Call Carregar_Rateio_Corretagem(Proposta_Id, Data_Sistema, 0, 0, 0, 0)
''Call Carregar_Rateio_Corretagem(Proposta_Id, Dt_Inicio_vig, 0, 0, 0, 0)
'''Call Carregar_Rateio_Corretagem(Proposta_Id, Dt_Contratacao, 0, 0, 0, 0)
''Call Ratear_Valor_Corretagem(Val_Tot_Corretagem_Rest, Perc_IR)
'''MARTINES
'''Obtendo C�digo do estipulante''''''''''''''''''''''''''''''''''''''''''''''''''''
''
'''bcarneiro - 13/03/03
'''Call Obter_Dados_ProLabore(IIf(Origem = 1, Proposta_Basica_Adesao, Proposta_Id), Dt_Inicio_vig, 0, 0, 0, 0, Estipulante_Id, "", Perc_Pro_Labore)
''Call Obter_Dados_ProLabore(IIf(Origem = 1, Proposta_Basica_Adesao, Proposta_Id), Dt_Contratacao, 0, 0, 0, 0, Estipulante_Id, "", Perc_Pro_Labore)
''
'''Aprovando avalia��o t�cnica''''''''''''''''''''''''''''''''''''''''''''''''''''''
''
''Call Aprovar_Avaliacao_Tecnica(Proposta_Id, Nr_Endosso_BB)
''
'''Se for sem devolucao zera todos os valroes - Barney 18/07/2003
''If Sem_Devolucao Then
''    Zera_Valores Val_Custo_Apolice_Certif_Rest, _
''                     Val_Adic_Fracionamento_Rest, _
''                     Val_Desconto_Rest, _
''                     Val_IOF_Rest, _
''                     Val_Premio_Tarifa_Rest, _
''                     Val_Restituicao, _
''                     0, _
''                     Val_Tot_Corretagem_Rest, _
''                     Val_ProLabore_Rest, _
''                     0, _
''                     0, _
''                     0, _
''                     0, _
''                     0, _
''                     0, _
''                     0
'' End If
''
'''Cancelando proposta''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''Endosso_Id = Obter_Num_Endosso(Proposta_Id)
''
'''bcarneiro - 17/01/2003 - Liquida��o n�o cancela a proposta e gera endosso de liquida��o
''If Trim(gsTipoProcesso) = "C" Then
''
''    iTp_Endosso_id = 63    ' Endosso de Cancelamento
''    Call Insere_Endosso(Proposta_Id, Endosso_Id, Nr_Endosso_BB, Dt_Cancelamento_BB, iTp_Endosso_id)
''    Call Cancelar_Proposta(Proposta_Id, "02", Endosso_Id, Dt_Cancelamento_BB)
''    Call Cancela_Agendamento(Proposta_Id, Endosso_Id)
''
''    '----------------------------------------------------------------------------
''    'Luciana - 12/08/2003 - Rotina inclu�da para cancelar os endossos da proposta
''    '----------------------------------------------------------------------------
''    For j = 1 To Endosso_Id - 1
''        SQS = "SELECT * FROM agendamento_cobranca_tb "
''        SQS = SQS & " WHERE proposta_id  = " & Proposta_Id
''        SQS = SQS & "   AND num_endosso  = " & j
''        SQS = SQS & "   AND situacao    <> 'a'"
''        SQS = SQS & "   AND canc_endosso_id IS NULL"
''        Set rc = rdocn.OpenResultset(SQS)
''        If Not rc.EOF Then
''            Call Insere_Cancelamento_Endosso(Proposta_Id, j, Endosso_Id)
''            Call Cancelar_Agendamentos(Proposta_Id, Endosso_Id, j)
''        End If
''        rc.Close
''    Next
''    '----------------------------------------------------------------------------
''
''    Call Atualizar_Registro_Emi(Emi_Id, Origem, "c", Proposta_Id, Endosso_Id)
''
''Else
''    iTp_Endosso_id = 104   ' Endosso de Liquida��o
''    Call Insere_Endosso(Proposta_Id, Endosso_Id, Nr_Endosso_BB, Dt_Cancelamento_BB, iTp_Endosso_id)
''    Call Atualizar_Registro_Emi(Emi_Id, Origem, "e", Proposta_Id, Endosso_Id)
''End If
''
'''Gerando movimenta��o financeira''''''''''''''''''''''''''''''''''''''''''''''''''
''
''If Val_Restituicao > 0 And verifica_endosso_liquidacao(Dt_Cancelamento_BB, Dt_Fim_vig, Trim(gsTipoProcesso)) Then
''
''    'Incluido endosso''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''
''    Incluir_Endosso_Financeiro Proposta_Id, _
''                               iTp_Endosso_id, _
''                               Nr_Endosso_BB, _
''                               Data_Sistema, _
''                               Val_Restituicao * -1, _
''                               Val_Tot_Corretagem_Rest * -1, _
''                               Val_ProLabore_Rest * -1, _
''                               Val_IOF_Rest * -1, _
''                               Val_Tot_IR_Corretagem * -1, _
''                               Val_Is_Restituicao * -1, _
''                               Val_Desconto_Rest * -1, _
''                               Val_Adic_Fracionamento_Rest * -1, _
''                               Val_Custo_Apolice_Certif_Rest * -1, _
''                               Val_Premio_Tarifa_Rest * -1, _
''                               Dt_Cancelamento_BB, _
''                               Endosso_Id
''
''    'Incluido Corretagem do endosso''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''
''    For Each Corretagem In Corretagens
''
''        Incluir_Corretagem_Endosso_Financeiro _
''                                   Proposta_Id, _
''                                   Endosso_Id, _
''                                   Corretagem.Corretor_Id, _
''                                   "0000", _
''                                   Corretagem.PercComissao, _
''                                   Corretagem.Val_Comissao * -1, _
''                                   Corretagem.Val_IR * -1
''    Next
''
''    'Incluindo Pr�-Labore do endosso''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''
''    If Val(Estipulante_Id) <> 0 Then
''        Incluir_ProLabore_Endosso_Financeiro _
''                                   Proposta_Id, _
''                                   Endosso_Id, _
''                                   Estipulante_Id, _
''                                   Perc_Pro_Labore
''    End If
''
''
''    'Incluindo Dados para cobran�a do corretor''''''''''''''''''''''''''''''''''''''''''''''''
''
''    Movimentacao_Id = ""
''    For Each Corretagem In Corretagens
''        Incluir_Mov_Corretor _
''                Data_Sistema, _
''                Corretagem.Val_Comissao, _
''                Corretagem.Corretor_Id, _
''                "0000", _
''                "J", _
''                Corretagem.Val_IR, _
''                Corretagem.PercComissao, _
''                "d", _
''                "2", _
''                Proposta_Id, _
''                Endosso_Id, _
''                Movimentacao_Id, _
''                bFlagRetemComissao
''    Next
''
''    'Incluindo dados para cobran�a do estipulante''''''''''''''''''''''''''''''''''''''''''''
''
''    If Val(Estipulante_Id) <> 0 Then
''        Incluir_Mov_Estipulante _
''                Data_Sistema, _
''                Val_ProLabore_Rest, _
''                Perc_Pro_Labore, _
''                "d", _
''                "2", _
''                Movimentacao_Id, _
''                Estipulante_Id, _
''                Proposta_Id, _
''                Endosso_Id, _
''                bFlagRetemComissao
''    End If
''
''    'Incluindo dados para pagamento do cliente''''''''''''''''''''''''''''''''''''''''''''''
''
''    Incluir_Mov_Cliente _
''            Data_Sistema, _
''            Val_Repasse_BB, _
''            "c", _
''            "2", _
''            Movimentacao_Id, _
''            Cliente_ID_Beneficiario, _
''            Proposta_Id, _
''            Endosso_Id
''
''    'Incluido dados para recupera��o de imposto''''''''''''''''''''''''''''''''''''''
''
''    If Val_IOF_Rest > 0 Then
''        Inclui_Mov_Item_Financeiro _
''                                "IOF", _
''                                Data_Sistema, _
''                                Val_IOF_Rest, _
''                                "d", _
''                                "2", _
''                                Movimentacao_Id, _
''                                Proposta_Id, _
''                                Endosso_Id, _
''                                bFlagRetemIOF
''    End If
''
''End If
''
'''Inserindo dados em evento_tb - arodrigues 06/02/2003
'''Call InserirEvento(Proposta_Id, Endosso_Id, iTp_Endosso_id, ramo_id, Produto_Id, TxtPropostaBB, Nr_Endosso_BB)
''
'''Finalizando transa��o''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''
''rdocn.CommitTrans
''
'''Desbloqueando proposta'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''
''Call Atualizar_Bloqueio_Proposta(Proposta_Id, "n")
''
'''Atualizando interface''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''
''MousePointer = 0
''
''MsgBox "Opera��o conclu�da", , ""
''
''Unload Me
''FrmSelEndosso.Show
''FrmSelEndosso.Marcar_Linha_Processada
'''Preparar_Informacoes CmbProduto.ItemData(CmbProduto.ListIndex)
''
''End Sub

''Sub Autorizar_Cancelamento()
''
'''Atualizando interface'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''
''MousePointer = vbHourglass
''
'''Processo sem Devolu��o - Barney 18/07/2003
''Sem_Devolucao = False
''
'''Barney - 02/09/2003
'''Verifica se tem apolice / Certificado
''If Not Obtem_apolice_certificado(TxtPropostaBB.Text, Produto_Id) Then
''   MousePointer = 0
''   Exit Sub
''End If
''
'''Autorizar cancelamento''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''
'''Luciana - 01/12/2003
''rdocn.BeginTrans
''
''Registrar_Avaliacao "p"
''
'''Desbloqueando proposta'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''Call Atualizar_Bloqueio_Proposta(txtProposta.Text, "n")
''
'''Luciana - 01/12/2003
''rdocn.CommitTrans
''
'''Atualizando interface'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''
''MousePointer = 0
''
''MsgBox "Autoriza��o efetuada", vbOKOnly + vbInformation, ""
''
''Unload Me
''FrmSelEndosso.Show
''FrmSelEndosso.Marcar_Linha_Processada
''
''End Sub
''
''Sub Ler_Avaliacoes(PProposta, Optional PNumEndossoBB = 0)
''
''Dim SQL As String
''Dim Rst As rdoResultset
''Dim Linha As String
''
'''Selecionando avalia��es'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''
''SQL = "Select"
''SQL = SQL & "   num_avaliacao, "
''SQL = SQL & "   val_restituicao_estimado, "
''SQL = SQL & "   val_restituicao,"
''SQL = SQL & "   dt_avaliacao_tecnica,"
''SQL = SQL & "   usuario_avaliacao_tecnica,"
''SQL = SQL & "   situacao,"
''SQL = SQL & "   dt_avaliacao_gerencial,"
''SQL = SQL & "   usuario_avaliacao_gerencial,"
''SQL = SQL & "   motivo_recusa_gerencial"
''SQL = SQL & " From "
''SQL = SQL & "   avaliacao_cancelamento_tb"
''SQL = SQL & " Where"
''SQL = SQL & "   proposta_id = " & PProposta
''
''' bcarneiro - 02/12/2003 - Projeto Restitui��o por Iniciativa da Seguradora
''If PNumEndossoBB > 0 Then
''    SQL = SQL & "   and num_endosso_bb = " & PNumEndossoBB
''End If
''
''SQL = SQL & " Order by"
''SQL = SQL & "   num_avaliacao"
''
''Set Rst = rdocn.OpenResultset(SQL)
''
''GridHistoricoCancelamento.Rows = 1
''
''Do While Not Rst.EOF
''
''    'Preenchendo grid'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''
''    Linha = Rst!num_avaliacao
''    Linha = Linha & vbTab & FormataValorParaPadraoBrasil(Rst!Val_Restituicao_Estimado)
''    Linha = Linha & vbTab & FormataValorParaPadraoBrasil(Rst!Val_Restituicao)
''    Select Case UCase(Rst!Situacao)
''        Case "A"
''            Linha = Linha & vbTab & "Aprovado"
''        Case "P"
''            Linha = Linha & vbTab & "Pendente"
''        Case "I"
''            Linha = Linha & vbTab & "Indeferido"
''    End Select
''    Linha = Linha & vbTab & Rst!motivo_recusa_gerencial
''    Linha = Linha & vbTab & Format(Rst!dt_avaliacao_tecnica, "dd/mm/yyyy")
''    Linha = Linha & vbTab & Rst!usuario_avaliacao_tecnica
''    Linha = Linha & vbTab & Format(Rst!dt_avaliacao_gerencial, "dd/mm/yyy")
''    Linha = Linha & vbTab & Rst!usuario_avaliacao_gerencial
''
''    GridHistoricoCancelamento.AddItem Linha
''
''    'Definindo cor da linha'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''
''    Dim i As Integer
''
''    GridHistoricoCancelamento.Row = GridHistoricoCancelamento.Rows - 1
''
''    For i = 0 To GridHistoricoCancelamento.Cols - 1
''        GridHistoricoCancelamento.Col = i
''        If UCase(Rst!Situacao) = "I" Then
''           GridHistoricoCancelamento.CellForeColor = &HC0&
''        Else
''           GridHistoricoCancelamento.CellForeColor = &H80000002
''        End If
''    Next
''
''    'Posicionando-se no pr�ximo registro''''''''''''''''''''''''''''''''''''''''''''''''''''
''
''    Rst.MoveNext
''
''Loop
''
''End Sub

Sub LerAvaliacoesRestituicao(ByVal lPropostaId As Long)

Dim sSQL As String
Dim rsAvaliacao As rdoResultset
Dim Linha As String

'Selecionando avalia��es'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

sSQL = ""
sSQL = sSQL & "SELECT num_avaliacao,"
sSQL = sSQL & "       ISNULL(val_restituicao_estimado, 0) AS val_restituicao_estimado,"
sSQL = sSQL & "       ISNULL(val_restituicao, 0) AS val_restituicao,"
sSQL = sSQL & "       dt_avaliacao_tecnica,"
sSQL = sSQL & "       usuario_avaliacao_tecnica,"
sSQL = sSQL & "       situacao,"
sSQL = sSQL & "       dt_avaliacao_gerencial,"
sSQL = sSQL & "       usuario_avaliacao_gerencial,"
sSQL = sSQL & "       motivo_recusa_gerencial,"
sSQL = sSQL & "       val_subvencao_federal, "
sSQL = sSQL & "       val_subvencao_estadual "
sSQL = sSQL & "  FROM avaliacao_restituicao_tb"
sSQL = sSQL & " WHERE proposta_id = " & lPropostaId
sSQL = sSQL & " ORDER BY num_avaliacao"

Set rsAvaliacao = rdocn.OpenResultset(sSQL)

GridHistoricoCancelamento.Rows = 1

Do While Not rsAvaliacao.EOF

    'Preenchendo grid'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

    Linha = rsAvaliacao!Num_avaliacao
    Linha = Linha & vbTab & FormataValorParaPadraoBrasil(rsAvaliacao!val_restituicao_estimado)
    Linha = Linha & vbTab & FormataValorParaPadraoBrasil(rsAvaliacao!Val_Restituicao)

    Select Case UCase(rsAvaliacao!Situacao)
        Case "A"
            Linha = Linha & vbTab & "Aprovado"
        Case "P"
            Linha = Linha & vbTab & "Pendente"
        Case "T"
            Linha = Linha & vbTab & "T�cnico"
        Case "I"
            Linha = Linha & vbTab & "Indeferido"
    End Select

    Linha = Linha & vbTab & rsAvaliacao!motivo_recusa_gerencial
    Linha = Linha & vbTab & Format(rsAvaliacao!dt_avaliacao_tecnica, "dd/mm/yyyy")
    Linha = Linha & vbTab & rsAvaliacao!usuario_avaliacao_tecnica
    Linha = Linha & vbTab & Format(rsAvaliacao!dt_avaliacao_gerencial, "dd/mm/yyy")
    Linha = Linha & vbTab & rsAvaliacao!usuario_avaliacao_gerencial

    GridHistoricoCancelamento.AddItem Linha

    'Definindo cor da linha'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

    Dim i As Integer

    GridHistoricoCancelamento.Row = GridHistoricoCancelamento.Rows - 1

    For i = 0 To GridHistoricoCancelamento.Cols - 1
        GridHistoricoCancelamento.Col = i
        If UCase(rsAvaliacao!Situacao) = "I" Then
           GridHistoricoCancelamento.CellForeColor = &HC0&
        Else
           GridHistoricoCancelamento.CellForeColor = &H80000002
        End If
    Next

    'Posicionando-se no pr�ximo registro''''''''''''''''''''''''''''''''''''''''''''''''''''

    rsAvaliacao.MoveNext

Loop

End Sub

Sub Ler_Endossos(ByVal PProposta)


Dim SQL As String
Dim Rst As rdoResultset
Dim Linha As String

'Selecionando avalia��es'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

SQL = "Select"
SQL = SQL & "   endosso_tb.endosso_id, "
SQL = SQL & "   endosso_tb.dt_emissao, "
SQL = SQL & "   tp_endosso_tb.descricao,"
SQL = SQL & "   endosso_financeiro_tb.val_financeiro, "
SQL = SQL & "   ISNULL(endosso_financeiro_tb.val_is, 0) AS val_is, "
SQL = SQL & "   canc_endosso_id = cancelamento_endosso_tb.endosso_id "
SQL = SQL & " From "
SQL = SQL & "   endosso_tb "
SQL = SQL & "   Inner Join tp_endosso_tb "
SQL = SQL & "         On tp_endosso_tb.tp_endosso_id = endosso_tb.tp_endosso_id "
SQL = SQL & "   Inner Join endosso_financeiro_tb "
SQL = SQL & "         On endosso_financeiro_tb.proposta_id = endosso_tb.proposta_id "
SQL = SQL & "         And endosso_financeiro_tb.endosso_id = endosso_tb.endosso_id "
SQL = SQL & "   Left Join cancelamento_endosso_tb "
SQL = SQL & "         On cancelamento_endosso_tb.proposta_id = endosso_tb.proposta_id "
SQL = SQL & "         And cancelamento_endosso_tb.canc_endosso_id = endosso_tb.endosso_id "
SQL = SQL & " Where"
SQL = SQL & "   endosso_tb.proposta_id = " & PProposta
SQL = SQL & " Order by"
SQL = SQL & "   endosso_tb.endosso_id "

Set Rst = rdocn.OpenResultset(SQL)

GridHistoricoEndosso.Rows = 1

Do While Not Rst.EOF
    
    'Preenchendo grid'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    
    Linha = Rst!endosso_id
    Linha = Linha & vbTab & Format(Rst!dt_emissao, "dd/mm/yyyy")
    Linha = Linha & vbTab & Rst!Descricao
    Linha = Linha & vbTab & FormataValorParaPadraoBrasil(Rst!Val_Financeiro)
    Linha = Linha & vbTab & FormataValorParaPadraoBrasil(Rst!Val_IS)
    Linha = Linha & vbTab & Rst!canc_endosso_id
    
    GridHistoricoEndosso.AddItem Linha
    
    'Posicionando-se no pr�ximo registro''''''''''''''''''''''''''''''''''''''''''''''''''''
    
    Rst.MoveNext
    
Loop

Rst.Close

End Sub


Sub Preencher_GridCobrancas()

Dim Cob As Cobranca

'Preenchendo grid de cobran�as''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

GridCobrancas.Rows = 1

For Each Cob In Cobrancas

    Linha = Cob.Num_Cobranca
    Linha = Linha & vbTab & Cob.endosso_id
    Linha = Linha & vbTab & Cob.Num_Parcela_Endosso
    Linha = Linha & vbTab & Cob.Dt_Agendamento
    Linha = Linha & vbTab & FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(Cob.Val_Cobranca))
    Linha = Linha & vbTab & Format(Cob.Dt_Recebimento, "dd/mm/yyyy")
    Linha = Linha & vbTab & FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(Cob.Val_Pago))
    Linha = Linha & vbTab & FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(Cob.Val_Juros))
    Linha = Linha & vbTab & FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(Cob.Val_Comissao))
    Linha = Linha & vbTab & FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(Cob.Val_Pro_Labore))
    Linha = Linha & vbTab & FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(Cob.Val_IR))
    Linha = Linha & vbTab & FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(Cob.Val_Adic_Fracionamento))
    Linha = Linha & vbTab & FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(Cob.Val_Desconto))
    
    GridCobrancas.AddItem Linha

Next

End Sub

''Sub Recusar_Cancelamento()
''
'''Dim SQL As String
''Dim MotivoRecusa As Integer
''
''MotivoRecusa = Obter_Motivo_Recusa
''
''If MotivoRecusa <> 0 Then
''
''    'Atualizando interface'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''
''    MousePointer = 11
''
''    'Recusando pedido de cancelamento''''''''''''''''''''''''''''''''''''''''''''''''''''''
''
''    rdocn.BeginTrans
''
''    If Verificar_Existencia_Avaliacao(txtProposta.Text) = False Then
''       Registrar_Avaliacao "r", MotivoRecusa
''    Else
''       Recusar_Avaliacao MotivoRecusa
''    End If
''
''    Call Atualizar_Registro_Emi(Emi_Id, Origem, "r", "NULL", "NULL")
''
''    rdocn.CommitTrans
''
''    'Desbloqueando proposta'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''
''    Call Atualizar_Bloqueio_Proposta(txtProposta.Text, "n")
''
''    'Atualizando interface'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''
''    MousePointer = 0
''
''    MsgBox "Recusa efetuada", , ""
''
''    Unload Me
''    FrmSelEndosso.Show
''    FrmSelEndosso.Marcar_Linha_Processada
''
''End If
''
''End Sub

''Sub Retornar_Para_Avaliacao_Tecnica()
''
''Dim MotivoRetornoInformado As String
''
''MotivoRetornoInformado = Obter_Motivo_Retorno
''
''If Trim(MotivoRetornoInformado) <> "" Then
''
''    'Recusando avalia��o t�cnica''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''
''    'CREATE PROCEDURE dbo.recusa_avaliacao_canc_spu
''    '@proposta_id            numeric(9,0),
''    '@num_endosso_bb         numeric(9,0),
''    '@dt_avaliacao_gerencial     smalldatetime,
''    '@usuario_avaliacao_gerencial    varchar(20),
''    '@motivo_recusa_gerencial        varchar(255),
''    '@usuario            varchar(20)
''
''    SQL = "exec retorno_avaliacao_canc_spu "
''    SQL = SQL & txtProposta.Text
''    SQL = SQL & "," & TxtEndossoBB.Text
''    SQL = SQL & ",'" & Format(Data_Sistema, "yyyymmdd") & "'"
''    SQL = SQL & ",'" & cUserName & "'"
''    SQL = SQL & ",'" & MotivoRetornoInformado & "'"
''    SQL = SQL & ",'" & cUserName & "'"
''
''    Set Rst = rdocn.OpenResultset(SQL)
''
''    'Desbloqueando proposta''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''
''    Call Atualizar_Bloqueio_Proposta(txtProposta.Text, "n")
''
''    'Atualizando interface'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''
''    MsgBox "Retorno efetuado", , ""
''
''    Unload Me
''    FrmSelEndosso.Show
''    FrmSelEndosso.Marcar_Linha_Processada
''    'FrmSelEndosso.Preparar_Informacoes FrmSelEndosso.CmbProduto.ItemData(FrmSelEndosso.CmbProduto.ListIndex)
''End If
''
''End Sub

''Sub Recusar_Avaliacao(ByVal PMotivoRecusa As Integer)
''
''    'Recusando avalia��o t�cnica''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''
''    SQL = "exec recusa_avaliacao_canc_spu "
''    SQL = SQL & txtProposta.Text
''    SQL = SQL & "," & TxtEndossoBB.Text
''    SQL = SQL & ",'" & Format(Data_Sistema, "yyyymmdd") & "'"
''    SQL = SQL & ",'" & cUserName & "'"
''    SQL = SQL & ",'" & PMotivoRecusa & "'"
''    SQL = SQL & ",'" & cUserName & "'"
''
''    Set Rst = rdocn.OpenResultset(SQL)
''    Rst.Close
''
''End Sub

''Sub Registrar_Avaliacao(ByVal Psituacao As String, _
''                        Optional ByVal PTpRecusaId As Variant)
''
''Dim SQL As String
''Dim Rst As rdoResultset
''
''Dim Restituicao_Integral As String
''
''Dim Val_Premio_Tarifa_Rest As Currency
''Dim Val_Custo_Apolice_Certif_Rest As Currency
''Dim Val_Adic_Fracionamento_Rest As Currency
''Dim Val_Desconto_Rest As Currency
''Dim Val_IOF_Rest As Currency
''Dim Val_Restituicao As Currency
''Dim Val_Tot_Corretagem_Rest As Currency
''Dim Val_ProLabore_Rest As Currency
''
''Dim Val_Premio_Tarifa_Calc As Currency
''Dim Val_Custo_Apolice_Certif_Calc As Currency
''Dim Val_Adic_Fracionamento_Calc As Currency
''Dim Val_Desconto_Calc As Currency
''Dim Val_IOF_Calc As Currency
''Dim Val_Restituicao_Calc As Currency
''Dim Val_Tot_Corretagem_Calc As Currency
''Dim Val_ProLabore_Calc As Currency
''
'''Atualizando interface'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''
''MousePointer = 11
''
'''Obtendo valores opera��o''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''
''If Val(LblDiasUtilizacaoSeguro.Caption) <= Val(LblPrazoRestituicaoIntegral.Caption) Then
''    Restituicao_Integral = "s"
''Else
''    Restituicao_Integral = "n"
''End If
''
''Val_Premio_Tarifa_Rest = Val(TrocaVirgulaPorPonto(TiraPonto(Mask2ValPremioTarifa.Text)))
''Val_Custo_Apolice_Certif_Rest = Val(TrocaVirgulaPorPonto(TiraPonto(LblCustoCertifApoliceRest.Caption)))
''Val_Adic_Fracionamento_Rest = Val(TrocaVirgulaPorPonto(TiraPonto(LblValAdicFracionamento.Caption)))
''Val_Desconto_Rest = Val(TrocaVirgulaPorPonto(TiraPonto(LblValDesconto.Caption)))
''Val_IOF_Rest = Val(TrocaVirgulaPorPonto(TiraPonto(LblValIOF.Caption)))
''Val_Restituicao = Val(TrocaVirgulaPorPonto(TiraPonto(LblValRestituicao.Caption)))
''Val_Tot_Corretagem_Rest = Val(TrocaVirgulaPorPonto(TiraPonto(LblTotValCorretagem.Caption)))
''Val_ProLabore_Rest = Val(TrocaVirgulaPorPonto(TiraPonto(LblValProLabore.Caption)))
''
''Val_Premio_Tarifa_Calc = Val(TrocaVirgulaPorPonto(TiraPonto(LblValPremioTarifaCalc.Caption)))
''Val_Custo_Apolice_Certif_Calc = Val(TrocaVirgulaPorPonto(TiraPonto(LblCustoCertifApoliceCalc.Caption)))
''Val_Adic_Fracionamento_Calc = Val(TrocaVirgulaPorPonto(TiraPonto(LblValAdicFracionamentoCalc.Caption)))
''Val_Desconto_Calc = Val(TrocaVirgulaPorPonto(TiraPonto(LblValDescontoCalc.Caption)))
''Val_IOF_Calc = Val(TrocaVirgulaPorPonto(TiraPonto(LblValIOFCalc.Caption)))
''Val_Restituicao_Calc = Val(TrocaVirgulaPorPonto(TiraPonto(LblValRestituicaoCalc.Caption)))
''Val_Tot_Corretagem_Calc = Val(TrocaVirgulaPorPonto(TiraPonto(LblTotValCorretagemCalc.Caption)))
''Val_ProLabore_Calc = Val(TrocaVirgulaPorPonto(TiraPonto(LblValProLaboreCalc.Caption)))
''
'''Registrando avaliacao t�cnica'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''
''SQL = "exec avaliacao_cancelamento_spi "
''SQL = SQL & txtProposta.Text
''SQL = SQL & "," & TxtEndossoBB.Text
''SQL = SQL & "," & MudaVirgulaParaPonto(Val_Restituicao_Calc)
''SQL = SQL & "," & MudaVirgulaParaPonto(Val_Restituicao)
''SQL = SQL & ",'" & Restituicao_Integral & " '"
''SQL = SQL & ",'" & Format(Data_Sistema, "yyyymmdd") & "'"
''SQL = SQL & ",'" & cUserName & "'"
''SQL = SQL & ",'" & Psituacao & "'"
''SQL = SQL & ",Null"
''SQL = SQL & ",Null"
''SQL = SQL & ",Null"
''SQL = SQL & ",'" & cUserName & "'"
''SQL = SQL & "," & MudaVirgulaParaPonto(Val_Tot_Corretagem_Rest)
''SQL = SQL & "," & MudaVirgulaParaPonto(Val_IOF_Rest)
''SQL = SQL & "," & MudaVirgulaParaPonto(Val_Adic_Fracionamento_Rest)
''SQL = SQL & "," & MudaVirgulaParaPonto(Val_Desconto_Rest)
''SQL = SQL & "," & MudaVirgulaParaPonto(Val_Premio_Tarifa_Rest)
''SQL = SQL & "," & MudaVirgulaParaPonto(Val_Custo_Apolice_Certif_Rest)
''SQL = SQL & "," & MudaVirgulaParaPonto(Val_ProLabore_Rest)
''SQL = SQL & "," & MudaVirgulaParaPonto(Val_Tot_Corretagem_Calc)
''SQL = SQL & "," & MudaVirgulaParaPonto(Val_IOF_Calc)
''SQL = SQL & "," & MudaVirgulaParaPonto(Val_Adic_Fracionamento_Calc)
''SQL = SQL & "," & MudaVirgulaParaPonto(Val_Desconto_Calc)
''SQL = SQL & "," & MudaVirgulaParaPonto(Val_Premio_Tarifa_Calc)
''SQL = SQL & "," & MudaVirgulaParaPonto(Val_Custo_Apolice_Certif_Calc)
''SQL = SQL & "," & MudaVirgulaParaPonto(Val_ProLabore_Calc)
''If IsMissing(PTpRecusaId) = False Then
''   SQL = SQL & "," & PTpRecusaId
''Else
''   SQL = SQL & ",Null"
''End If
''
'''bcarneiro - 17/01/2003 - Tp_processo
''SQL = SQL & ",'" & gsTipoProcesso & "'"
''
''
''Set Rst = rdocn.OpenResultset(SQL)
''Rst.Close
''
''End Sub

Sub RecalcularValores()

'Declara��es'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

Dim Proposta_Basica_Adesao As String

Dim Val_Custo_Apolice_Certif As Currency
Dim Val_Custo_Apolice_Certif_Rest As Currency
Dim Val_Adic_Fracionamento_Rest As Currency
Dim Val_Desconto_Rest As Currency
Dim Val_IOF_Rest As Currency
Dim Val_Premio_Tarifa_Rest As Currency
Dim Val_Restituicao As Currency
Dim Val_Tot_Corretagem_Rest As Currency
Dim Val_ProLabore_Rest As Currency
Dim Perc_IR As Currency
 
Dim Val_Premio_Tarifa_Calc As Currency
Dim Val_Desconto_Calc As Currency
Dim Val_Adic_Fracionamento_Calc As Currency
Dim Val_IOF_Calc As Currency
Dim Val_Restituicao_Calc As Currency

Dim Val_Tot_Pago As Currency

Dim bApoliceAberta As Boolean

    'Obtendo proposta b�sica da proposta de adesao '''''''''''''''''''''''''''''''''''''''''''''

    If Origem = "1" Then
        'Proposta_Basica_Adesao = BuscaParametro("BASICA " & Produto_Id)
        Proposta_Basica_Adesao = IIf(IsNull(BuscaParametro("BASICA " & produto_id)), txtProposta, BuscaParametro("BASICA " & produto_id))
    End If

    ' bcarneiro - 02/12/2003 - Projeto Restitui��o por Iniciativa da Seguradora
    ''If gsTipoProcesso = "R" Then

    Call RecalcularRestituicao(Proposta_Basica_Adesao)

    Exit Sub

    ''End If

''    'Obtendo valores informados ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''
''    Val_Tot_Pago = Val(TrocaVirgulaPorPonto(TiraPonto(LblTotValPremioPago.Caption)))
''
''    Val_Premio_Tarifa_Calc = Val(TrocaVirgulaPorPonto(TiraPonto(LblValPremioTarifaCalc.Caption)))
''    Val_Desconto_Calc = Val(TrocaVirgulaPorPonto(TiraPonto(LblValDescontoCalc.Caption)))
''    Val_Adic_Fracionamento_Calc = Val(TrocaVirgulaPorPonto(TiraPonto(LblValAdicFracionamentoCalc.Caption)))
''    Val_IOF_Calc = Val(TrocaVirgulaPorPonto(TiraPonto(LblValIOFCalc.Caption)))
''
''    Val_Restituicao_Calc = Val(TrocaVirgulaPorPonto(TiraPonto(LblValRestituicaoCalc.Caption)))
''
''    Val_Premio_Tarifa_Rest = Val(TrocaVirgulaPorPonto(TiraPonto(Mask2ValPremioTarifa.Text)))
''
''    'Obtendo valor do custo da ap�lice '''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''
''    Val_Custo_Apolice_Certif = Val(TrocaVirgulaPorPonto(TiraPonto(LblCustoCertifApolice.Caption)))
''
''    'Obtendo valores da restitui��o ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''
'''    Val_Desconto_Rest = Trunca((Val_Desconto_Calc * Val_Premio_Tarifa_Rest) / Val_Premio_Tarifa_Calc)
'''    Val_Adic_Fracionamento_Rest = Trunca((Val_Adic_Fracionamento_Calc * Val_Premio_Tarifa_Rest) / Val_Premio_Tarifa_Calc)
'''    Val_IOF_Rest = Trunca((Val_IOF_Calc * Val_Premio_Tarifa_Rest) / Val_Premio_Tarifa_Calc)
'''    Val_Restituicao = Trunca((Val_Restituicao_Calc * Val_Premio_Tarifa_Rest) / Val_Premio_Tarifa_Calc)
''
''    'Jorfilho 11-set-2003: Verifica��o de ap�lice aberta
''    bApoliceAberta = Trim(TxtDtFimVigencia.Text = "")
''
''    Call Obter_Composicao_Valor_Restituicao_2(txtProposta.Text, _
''                                              Val_Premio_Tarifa_Rest, _
''                                              TxtRamo.Text, _
''                                              txtDtContratacao.Text, _
''                                              TxtDtInicioVigencia.Text, _
''                                              LblDiasUtilizacaoSeguro.Caption, _
''                                              LblPrazoRestituicaoIntegral, _
''                                              LblCustoCertifApolice.Caption, _
''                                              Val_Premio_Tarifa_Calc, _
''                                              Val_Desconto_Calc, _
''                                              Val_Custo_Apolice_Certif_Rest, _
''                                              Val_Adic_Fracionamento_Rest, _
''                                              Val_Desconto_Rest, _
''                                              Val_IOF_Rest, _
''                                              Val_Restituicao, _
''                                              bApoliceAberta)
''
''    'Acertando diferen�as de arredondamento'''''''''''''''''''''''''''''''''''''''''''''''''
''
''    'Op��o 1
''
'''    If Val_Premio_Tarifa_Calc = Val_Premio_Tarifa_Rest Then
'''        If Abs(Val_IOF_Rest - Val_IOF_Calc) <= Val("0.02") Then
'''            Val_IOF_Rest = Val_IOF_Calc
'''        End If
'''        If Abs(Val_Adic_Fracionamento_Rest - Val_Adic_Fracionamento_Calc) <= Val("0.02") Then
'''            Val_Adic_Fracionamento_Rest = Val_Adic_Fracionamento_Calc
'''        End If
'''        Val_Restituicao = Val_Premio_Tarifa_Rest + Val_Custo_Apolice_Certif_Rest + Val_Adic_Fracionamento_Rest - Val_Desconto_Rest + Val_IOF_Rest
'''    End If
''
''    'Op��o 2
''
''    If Val_Premio_Tarifa_Calc = Val_Premio_Tarifa_Rest Then
''        If Val_IOF_Rest > Val_IOF_Calc Then
''            Val_IOF_Rest = Val_IOF_Calc
''        End If
''        If Val_Adic_Fracionamento_Rest > Val_Adic_Fracionamento_Calc Then
''            Val_Adic_Fracionamento_Rest = Val_Adic_Fracionamento_Calc
''        End If
''        If Mask2ValPremioTarifa.Enabled = False Then
''            If vgsituacao <> "p" Or bApoliceAberta Then
''                Val_Restituicao = Val_Premio_Tarifa_Rest + Val_Custo_Apolice_Certif_Rest + Val_Adic_Fracionamento_Rest - Val_Desconto_Rest + Val_IOF_Rest
''            Else
''                Val_Restituicao = 0
''            End If
''        End If
''    End If
''
''    'Recalculando demais valores'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''
''    'Call Obtem_Perc_IR(TxtRamo.Text, TxtDtInicioVigencia.Text, Perc_IR)
''    Call Obtem_Perc_IR(TxtRamo.Text, Data_Sistema, Perc_IR)
''
''    'Jorfilho 15/01/2004
''    'Call Ratear_ValRestituicao_Corretagem(Val_Restituicao, Val_Custo_Apolice_Certif_Rest, Val_IOF_Rest, Perc_IR)
''    Call Carregar_Rateio_Corretagem(txtProposta.Text, txtDtContratacao.Text, Val_Restituicao, Val_IOF_Rest, Val_Custo_Apolice_Certif_Rest, Perc_IR)
''    Call Obter_Total_Corretagem(Val_Tot_Corretagem_Rest)
''
''    'bcarneiro 13/03/03
''    'Call Obter_Dados_ProLabore(IIf(Origem = 1, Proposta_Basica_Adesao, txtProposta.Text), TxtDtInicioVigencia.Text, Val_Restituicao, Val_IOF_Rest, Val_Custo_Apolice_Certif_Rest, Val_ProLabore_Rest, "", 0, 0)
''    Call Obter_Dados_ProLabore(IIf(Origem = 1, Proposta_Basica_Adesao, txtProposta.Text), txtDtContratacao.Text, Val_Restituicao, Val_IOF_Rest, Val_Custo_Apolice_Certif_Rest, Val_ProLabore_Rest, "", 0, 0)
''
''    'Exibindo dados necess�rios para restitu��o''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''
''    LblCustoCertifApolice.Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(Val_Custo_Apolice_Certif_Rest))
''    LblValAdicFracionamento.Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(Val_Adic_Fracionamento_Rest))
''    LblValDesconto.Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(Val_Desconto_Rest))
''    LblValIOF.Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(Val_IOF_Rest))
''    LblValRestituicao.Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(Val_Restituicao))
''    LblTotValCorretagem.Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(Val_Tot_Corretagem_Rest))
''    LblValProLabore.Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(Val_ProLabore_Rest))
''
''    'Criticando valor de devolu��o informado'''''''''''''''''''''''''''''''''''''''''''''''''''''
''
''    If bApoliceAberta Then
''        Val_Tot_Pago = ObterTotalPagoApoliceAberta(txtProposta.Text)
''    End If
''
''    If Val_Restituicao > Val_Tot_Pago Then
''       MsgBox "Valor de restitui��o informado est� maior que o total pago!"
''       Mask2ValPremioTarifa.SetFocus
''       Exit Sub
''    End If

''End Sub

''Public Sub Cancela_Agendamento(PProposta As String, PEndossoId As String)
''
''Dim SQL As String
''
''On Error GoTo Erro
''
''SQL = "EXEC cancela_cobranca_spi " & PProposta
''SQL = SQL & ", " & PEndossoId
''SQL = SQL & ", '" & cUserName & "'"
''
''rdocn.Execute SQL
''
''Exit Sub
''
''Erro:
''   TrataErroGeral "Cancela_Agendamento", Me.name
''   TerminaSEGBR
''End Sub

'Barney - 18/07/2003
''Sub Aceite_Sem_Devolucao(ByVal Psituacao As String, _
''                        Optional ByVal PTpRecusaId As Variant)
''
''Dim Rst As rdoResultset
''
''Dim Restituicao_Integral As String
''
''Dim Val_Premio_Tarifa_Rest As Currency
''Dim Val_Custo_Apolice_Certif_Rest As Currency
''Dim Val_Adic_Fracionamento_Rest As Currency
''Dim Val_Desconto_Rest As Currency
''Dim Val_IOF_Rest As Currency
''Dim Val_Restituicao As Currency
''Dim Val_Tot_Corretagem_Rest As Currency
''Dim Val_ProLabore_Rest As Currency
''
''Dim Val_Premio_Tarifa_Calc As Currency
''Dim Val_Custo_Apolice_Certif_Calc As Currency
''Dim Val_Adic_Fracionamento_Calc As Currency
''Dim Val_Desconto_Calc As Currency
''Dim Val_IOF_Calc As Currency
''Dim Val_Restituicao_Calc As Currency
''Dim Val_Tot_Corretagem_Calc As Currency
''Dim Val_ProLabore_Calc As Currency
''
'''Atualizando interface'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''MousePointer = 11
''
'''Flag_sem_devolucao
''Sem_Devolucao = True
''
'''Barney - 02/09/2003
'''Verifica se tem apolice / Certificado
''If Not Obtem_apolice_certificado(TxtPropostaBB.Text, Produto_Id) Then
''   MousePointer = 0
''   Exit Sub
''End If
''
'''Inclui ZERO em todos os valores
''Zera_Valores Val_Custo_Apolice_Certif_Rest, _
''                 Val_Adic_Fracionamento_Rest, _
''                 Val_Desconto_Rest, _
''                 Val_IOF_Rest, _
''                 Val_Premio_Tarifa_Rest, _
''                 Val_Restituicao, _
''                 Val_Restituicao_Calc, _
''                 Val_Tot_Corretagem_Rest, _
''                 Val_ProLabore_Rest, _
''                 Val_Premio_Tarifa_Calc, _
''                 Val_Custo_Apolice_Certif_Calc, _
''                 Val_Desconto_Calc, _
''                 Val_IOF_Calc, _
''                 Val_Restituicao_Calc, _
''                 Val_Tot_Corretagem_Calc, _
''                 Val_ProLabore_Calc
''
''    'Incluo Proposta na Avaliacao Cancelamento com valores ZERADOS
''    If Val(LblDiasUtilizacaoSeguro.Caption) <= Val(LblPrazoRestituicaoIntegral.Caption) Then
''        Restituicao_Integral = "s"
''    Else
''        Restituicao_Integral = "n"
''    End If
''    'Registrando avaliacao t�cnica'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''    SQL = "exec avaliacao_cancelamento_spi "
''    SQL = SQL & txtProposta.Text
''    SQL = SQL & "," & TxtEndossoBB.Text
''    SQL = SQL & "," & MudaVirgulaParaPonto(Val_Restituicao_Calc)
''    SQL = SQL & "," & MudaVirgulaParaPonto(Val_Restituicao)
''    SQL = SQL & ",'" & Restituicao_Integral & " '"
''    SQL = SQL & ",'" & Format(Data_Sistema, "yyyymmdd") & "'"
''    SQL = SQL & ",'" & cUserName & "'"
''    SQL = SQL & ",'" & Psituacao & "'"
''    SQL = SQL & ",Null"
''    SQL = SQL & ",Null"
''    SQL = SQL & ",Null"
''    SQL = SQL & ",'" & cUserName & "'"
''    SQL = SQL & "," & MudaVirgulaParaPonto(Val_Tot_Corretagem_Rest)
''    SQL = SQL & "," & MudaVirgulaParaPonto(Val_IOF_Rest)
''    SQL = SQL & "," & MudaVirgulaParaPonto(Val_Adic_Fracionamento_Rest)
''    SQL = SQL & "," & MudaVirgulaParaPonto(Val_Desconto_Rest)
''    SQL = SQL & "," & MudaVirgulaParaPonto(Val_Premio_Tarifa_Rest)
''    SQL = SQL & "," & MudaVirgulaParaPonto(Val_Custo_Apolice_Certif_Rest)
''    SQL = SQL & "," & MudaVirgulaParaPonto(Val_ProLabore_Rest)
''    SQL = SQL & "," & MudaVirgulaParaPonto(Val_Tot_Corretagem_Calc)
''    SQL = SQL & "," & MudaVirgulaParaPonto(Val_IOF_Calc)
''    SQL = SQL & "," & MudaVirgulaParaPonto(Val_Adic_Fracionamento_Calc)
''    SQL = SQL & "," & MudaVirgulaParaPonto(Val_Desconto_Calc)
''    SQL = SQL & "," & MudaVirgulaParaPonto(Val_Premio_Tarifa_Calc)
''    SQL = SQL & "," & MudaVirgulaParaPonto(Val_Custo_Apolice_Certif_Calc)
''    SQL = SQL & "," & MudaVirgulaParaPonto(Val_ProLabore_Calc)
''    If IsMissing(PTpRecusaId) = False Then
''       SQL = SQL & "," & PTpRecusaId
''    Else
''       SQL = SQL & ",Null"
''    End If
''
''    SQL = SQL & ",'" & gsTipoProcesso & "'"
''
''    Set Rst = rdocn.OpenResultset(SQL)
''    Rst.Close
''
''
'''Desbloqueando proposta'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''Call Atualizar_Bloqueio_Proposta(txtProposta.Text, "n")
''
'''Atualizando interface'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''MousePointer = 0
''
''MsgBox "Autoriza��o efetuada", vbOKOnly + vbInformation, ""
''
''Unload Me
''FrmSelEndosso.Show
''FrmSelEndosso.Marcar_Linha_Processada
''
''End Sub

'Barney - 18/07/2003
''Sub Zera_Valores(ByRef PValCustoApoliceCertifRest As Currency, _
''                 ByRef PValAdicFracionamento_Rest As Currency, _
''                 ByRef PValDescontoRest As Currency, _
''                 ByRef PValIOFRest As Currency, _
''                 ByRef PValPremioTarifaRest As Currency, _
''                 ByRef PValRestituicao As Currency, _
''                 ByRef PValRestituicaoCalc As Currency, _
''                 ByRef PValTotCorretagem As Currency, _
''                 ByRef PValProLabore As Currency, _
''                 ByRef PValCustoApoliceCertifCalc As Currency, _
''                 ByRef PValAdicFracionamentoCalc As Currency, _
''                 ByRef PValDescontoCalc As Currency, _
''                 ByRef PValIOFCalc As Currency, _
''                 ByRef PValPremioTarifaCalc As Currency, _
''                 ByRef PValTotCorretagemCalc As Currency, _
''                 ByRef PValProLaboreCalc As Currency)
''
''PValCustoApoliceCertifRest = 0
''PValAdicFracionamento_Rest = 0
''PValDescontoRest = 0
''PValIOFRest = 0
''PValPremioTarifaRest = 0
''PValRestituicao = 0
''PValRestituicaoCalc = 0
''PValTotCorretagem = 0
''PValProLabore = 0
''PValCustoApoliceCertifCalc = 0
''PValAdicFracionamentoCalc = 0
''PValDescontoCalc = 0
''PValIOFCalc = 0
''PValPremioTarifaCalc = 0
''PValTotCorretagemCalc = 0
''PValProLaboreCalc = 0
''
''End Sub


''Public Function Obtem_apolice_certificado(ByVal Proposta_BB As String, ByVal Produto_Id As Integer) As Boolean
'''Barney - 02/09/2003
'''FUN��O CRIADA EM 2 DE SETEMBRO DE 2003 PARA VERIFICAR SE A PROPOSTA POSSUI NUMERO DE CERTIFICADO
'''OU APOLICE ID E SE POSSUI TAMB�M UM AGENDAMENTO , CASO N�O TENHA N�O PODE SER CANCELADA
''
''    Dim rs    As rdoResultset
''    Dim lPropostaId As Long
''    Dim certificado_id As Long
''    Dim apolice_id As Long
''    Dim PFPI As Long
''    Dim PAPI As Long
''
''    Obtem_apolice_certificado = False
''
''    On Error GoTo Erro
''
''    'Iniciliza vari�veis da proposta
''    PFPI = 0
''    PAPI = 0
''
''    'VERIFICA CERTIFICADO E APOLICE ID XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
''    SQL = " Select pf.proposta_id as PFPI "
''    SQL = SQL & " from proposta_tb pt (nolock)"
''    SQL = SQL & " left join proposta_fechada_tb pf  (nolock) on pt.proposta_id = pf.proposta_id"
''    SQL = SQL & " left join produto_tb pb (nolock) on pb.produto_id = pt.produto_id"
''    SQL = SQL & " Where pt.produto_id = " & Produto_Id & " And "
''    SQL = SQL & " pf.proposta_bb = " & Proposta_BB
''    Set rs = rdocn.OpenResultset(SQL)
''
''    If rs.EOF Then
''        SQL = " Select pa.proposta_id as PAPI"
''        SQL = SQL & " from proposta_tb pt (nolock)"
''        SQL = SQL & " left join proposta_adesao_tb pa (nolock) on pt.proposta_id = pa.proposta_id"
''        SQL = SQL & " left join produto_tb pb (nolock) on pb.produto_id = pt.produto_id"
''        SQL = SQL & " Where pt.produto_id = " & Produto_Id & " And "
''        SQL = SQL & "       pa.proposta_bb = " & Proposta_BB
''        rs.Close
''        Set rs = rdocn.OpenResultset(SQL)
''
''        If rs.EOF Then
''            MensagemBatch "N�o existe certificado ou n�mero de apolice para este registro, ele n�o pode ser cancelado!!! ,Verifique. ", _
''                          vbInformation, "Avalia��o T�cnica", False
''            Obtem_apolice_certificado = False
''            Exit Function
''        Else
''            PAPI = rs!PAPI
''            rs.Close
''        End If
''    Else
''       PFPI = rs!PFPI
''       rs.Close
''    End If
''
'''    If PFPI <> 0 Then
'''
'''        'proposta_fechada
'''        lpropostaID = IIf(IsNull(rs!PFPI), 0, rs!PFPI)
'''        rs.Close
'''        SQL = " select apolice_id from apolice_tb where proposta_id = " & lpropostaID
'''        Set rs = rdocn.OpenResultset(SQL)
'''
'''        If Not rs.EOF Then
'''
'''            apolice_id = rs!apolice_id
'''
'''        Else
'''            Obtem_apolice_certificado = False
'''            MensagemBatch "N�o existe apolice_id para este registro, ele n�o pode ser cancelado!!! , Verifique. ", _
'''                           vbInformation, "Avalia��o T�cnica", False
'''            Obtem_apolice_certificado = False
'''            Exit Function
'''        End If
'''
'''    Else
'''        If PAPI <> 0 Then
'''            lpropostaID = IIf(IsNull(rs!PAPI), 0, rs!PAPI)
'''           'proposta_adesao
'''            rs.Close
'''            SQL = " select certificado_id from certificado_tb where proposta_id = " & lpropostaID & vbNewLine
'''            SQL = SQL + " union all " & vbNewLine
'''            SQL = SQL + " select certificado_id from certificado_re_tb where proposta_id = " & lpropostaID
'''            Set rs = rdocn.OpenResultset(SQL)
'''
'''            If Not rs.EOF Then
'''               certificado_id = rs!certificado_id
'''            Else
'''                Obtem_apolice_certificado = False
'''                MensagemBatch "N�o existe certificado_id para este registro, ele n�o pode ser cancelado!!! , Verifique. ", _
'''                               vbInformation, "Avalia��o T�cnica", False
'''                Exit Function
'''            End If
'''        Else
'''
'''            Obtem_apolice_certificado = False
'''            MensagemBatch "N�o existe certificado_id para este registro, ele n�o pode ser cancelado!!! , Verifique. ", _
'''                               vbInformation, "Avalia��o T�cnica", False
'''            Exit Function
'''
'''        End If
'''    End If
''  'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
''  'VERIFICA AGENDAMENTO XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
''  'Por Daniel Romualdo - 24/09/2003
''  If PFPI <> 0 Then 'Proposta Fechada
''    SQL = " select * from agendamento_cobranca_tb where proposta_id = " & PFPI
''    Set rs = rdocn.OpenResultset(SQL)
''
''    'Luciana - 01/12/2003
''    If Produto_Id <> 115 Then
''        If rs.EOF Then
''            MensagemBatch "A proposta n�o possui agendamento de cobran�as , n�o pode ser cancelada !!!", _
''                          vbCritical, "Avalia��o T�cnica", False
''            Obtem_apolice_certificado = False
''            Exit Function
''        End If
''    End If
''
''  Else 'Proposta Ades�o
''
''    SQL = " select * from  agendamento_cobranca_tb where proposta_id = " & PAPI
''    Set rs = rdocn.OpenResultset(SQL)
''
''    If rs.EOF Then 'N�o tem na agendamento e devemos verificar se existe na basica
''        rs.Close
''        SQL = "select * from agendamento_cobranca_tb where proposta_id = "
''        SQL = SQL & "(select top 1 proposta_id from apolice_tb where apolice_id = "
''        SQL = SQL & "(select top 1 apolice_id from proposta_adesao_Tb where proposta_id = " & PAPI
''        SQL = SQL & ") and ramo_id =    (select top 1 ramo_id from proposta_adesao_Tb where proposta_id = " & PAPI & "))"
''        Set rs = rdocn.OpenResultset(SQL)
''
''        'Luciana - 01/12/2003
''        If Produto_Id <> 115 Then
''            If rs.EOF Then
''                MensagemBatch "A proposta n�o possui agendamento de cobran�as , n�o pode ser cancelada !!!", _
''                              vbCritical, "Avalia��o T�cnica", False
''                Obtem_apolice_certificado = False
''                Exit Function
''            End If
''        End If
''    End If
''  End If
''    rs.Close
''  'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
''
''  Obtem_apolice_certificado = True
''  Exit Function
''
''Erro:
''   TrataErroGeral "Obtem_apolice", Me.name
''   Unload Me
''   TerminaSEGBR
''
''End Function



End Sub

Private Sub btnAceiteTecnico_Click()

'Renato Vasconcelos
'    'Flow 11022639
'    Dim strNumAvaliacao As String
'    Dim strPropostas As String
'    Call Atualizar_Bloqueio_Proposta(txtProposta.Text, "n")
'
'    strPropostas = txtProposta.Text
'    strNumAvaliacao = TxtEndossoBB.Text
'
'    Call aprovacao_tecnica(strPropostas, strNumAvaliacao, 0, "")

'    If bModular Then
'        strPropostas = strPropostas & "," & CStr(oDadosModular.proposta_id)
'        strNumAvaliacao = strNumAvaliacao & "," & CStr(oDadosModular.Num_avaliacao)
'
'    End If
'
    Call AutorizarRestituicao
    Dim strProposta As String
    strProposta = txtProposta.Text
    sSQL = "SET NOCOUNT ON" & vbNewLine
    sSQL = sSQL & "delete from #avaliacao_restituicao_tb where proposta_id IN (" & strProposta & ")"
    Set rc = rdocn.OpenResultset(sSQL)
    rc.Close
    Set rc = Nothing
'
      
    
    Unload Me
    
    If formAnterior = "frmFiltroPesquisa" Then
        Call Unload(frmFiltroPesquisa)
        frmFiltroPesquisa.Show
    Else
        frmAnalitico.strWhere = strWhere
        frmAnalitico.inicial = inicial
        frmAnalitico.intervalo = intervalo
        frmAnalitico.maiorQue = maiorQue
        frmAnalitico.idProduto = idProduto
        frmAnalitico.QTD = QTD
        frmAnalitico.carregarForm
        frmAnalitico.Show
        
    End If
    ' bcarneiro - 03/12/2003 - Projeto Restitui��o por Iniciativa da Seguradora
    ''If gsTipoProcesso = "R" Then

    'If ValidarTotalPremioInformado Then
        
    'End If

    ''Else
    ''    Call Autorizar_Cancelamento
    ''End If

End Sub

Private Sub BtnAprovar_Click()
    
    
    'Renato Vasconcelos
    'Flow 11022639
    Dim strNumAvaliacao As String
    Dim strPropostas As String
    Dim aprovado As Boolean
     
    strPropostas = txtProposta.Text
    strNumAvaliacao = TxtEndossoBB.Text
    
      
    If bModular Then
        strPropostas = strPropostas & "," & CStr(oDadosModular.proposta_id)
        strNumAvaliacao = strNumAvaliacao & "," & CStr(oDadosModular.Num_avaliacao)
       
    End If
    
    
    Call aprovacao_gerencial(strPropostas, strNumAvaliacao, idProduto, "")
    
   ' Exit Sub
    
    
    Call Atualizar_Bloqueio_Proposta(txtProposta.Text, "n")
     
    Unload Me
    
    If formAnterior = "frmFiltroPesquisa" Then
        Call Unload(frmFiltroPesquisa)
        frmFiltroPesquisa.Show
    Else
        frmAnalitico.strWhere = strWhere
        frmAnalitico.inicial = inicial
        frmAnalitico.intervalo = intervalo
        frmAnalitico.maiorQue = maiorQue
        frmAnalitico.idProduto = idProduto

        frmAnalitico.sProduto = sProduto  'Alan Neves - C_ANEVES - Demanda: 17860335 - 28/05/2013
        
        frmAnalitico.QTD = QTD
        frmAnalitico.carregarForm
        If frmAnalitico.ListSelEndosso.ListItems.Count = 0 Then
             Call Unload(frmFiltroPesquisa)
            frmFiltroPesquisa.Show
        Else
            frmAnalitico.Show
        End If
    End If
    

    ' bcarneiro - 03/12/2003 - Projeto Restitui��o por Iniciativa da Seguradora
    ''If gsTipoProcesso = "R" Then
'    AvaliacaoGerencial = True
'
'        If bModular = False And Produto_id <> 1168 Then
'            Call AprovarRestituicao(txtProposta.Text, _
'                                    Val(TxtEndossoBB.Text), _
'                                    Produto_id, _
'                                    txtDtContratacao.Text, _
'                                    TxtDtInicioVigencia.Text, _
'                                    TxtDtFimVigencia.Text, _
'                                    TxtDtCancelamentoBB.Text)
'
'        Else
'            If Produto_id = 1168 Then
'                Call AprovarRestituicao(txtProposta.Text, _
'                                        Val(TxtEndossoBB.Text), _
'                                        Produto_id, _
'                                        txtDtContratacao.Text, _
'                                        TxtDtInicioVigencia.Text, _
'                                        TxtDtFimVigencia.Text, _
'                                        TxtDtCancelamentoBB.Text)
'                'If Val(TxtEndossoBB.Text) <> oDadosModular.Num_avaliacao Then
'                If bModular = True Then
'                    Call AprovarRestituicaoModular(oDadosModular.proposta_id, _
'                                                   oDadosModular.Num_avaliacao, _
'                                                   oDadosModular.Produto_id, _
'                                                   oDadosModular.Dt_Contratacao, _
'                                                   oDadosModular.Dt_Inicio_Vigencia, _
'                                                   oDadosModular.Dt_Fim_Vigencia, _
'                                                   oDadosModular.Dt_Pedido_Avaliacao)
'                End If
'
'            Else 'rsilva
'
'                Call AprovarRestituicao(txtpropostaModular(0).Text, _
'                                        Val(TxtEndossoBB.Text), _
'                                        Produto_id, _
'                                        txtDtContratacao.Text, _
'                                        TxtDtInicioVigencia.Text, _
'                                        TxtDtFimVigencia.Text, _
'                                        TxtDtCancelamentoBB.Text)
'
'                Call AprovarRestituicaoModular(oDadosModular.proposta_id, _
'                                               oDadosModular.Num_avaliacao, _
'                                               oDadosModular.Produto_id, _
'                                               oDadosModular.Dt_Contratacao, _
'                                               oDadosModular.Dt_Inicio_Vigencia, _
'                                               oDadosModular.Dt_Fim_Vigencia, _
'                                               oDadosModular.Dt_Pedido_Avaliacao)
'            End If
'        End If
'        AvaliacaoGerencial = False
'
'        MsgBox "Opera��o conclu�da", , ""
'
'        Unload Me
'        FrmSelEndosso.Show
'        FrmSelEndosso.Marcar_Linha_Processada
'
    ''Else
    ''    Call Aprovar_Cancelamento
    ''End If
    
End Sub

Private Sub BtnRecusar_Click()
    
    ' bcarneiro - 03/12/2003 - Projeto Restitui��o por Iniciativa da Seguradora
    ''If gsTipoProcesso = "R" Then
        Call RecusarRestituicao
    ''Else
        'Recusar_Cancelamento - Barney 18/07/2003
    ''    Call Aceite_Sem_Devolucao("P")
    ''End If
    
End Sub

Private Sub BtnRetornoParaAvaliacaoTecnica_Click()

    ' bcarneiro - 03/12/2003 - Projeto Restitui��o por Iniciativa da Seguradora
    ''If gsTipoProcesso = "R" Then
        Call RetornarAvaliacaoTecnicaRestituicao
    ''Else
    ''    Retornar_Para_Avaliacao_Tecnica
    ''End If

End Sub


Private Sub BtnSair_Click()

'Desbloqueando proposta''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''


Call Atualizar_Bloqueio_Proposta(txtProposta.Text, "n")

Unload Me

If formAnterior = "frmFiltroPesquisa" Then
    frmFiltroPesquisa.Show
Else
    frmAnalitico.strWhere = strWhere
    frmAnalitico.inicial = inicial
    frmAnalitico.intervalo = intervalo
    frmAnalitico.maiorQue = maiorQue
    frmAnalitico.idProduto = idProduto
    frmAnalitico.QTD = QTD
    frmAnalitico.carregarForm
    frmAnalitico.Show
    
End If


'FrmSelEndosso.Show
'verificar
End Sub

Sub Ler_Dados_Proposta(PProposta)

Dim rc As rdoResultset

    'Selecionando dados atuais da proposta'''''''''''''''''''''''''''''''''''''''''''''''''

    SQL = ""
    SQL = SQL & "SELECT a.dt_contratacao, "
    SQL = SQL & "       sb.subramo_id,"
    SQL = SQL & "       a.situacao, "
    SQL = SQL & "       b.cliente_id,"
    SQL = SQL & "       nome_cliente = b.nome,"
    SQL = SQL & "       b.ddd_1, "
    SQL = SQL & "       b.telefone_1, "
    SQL = SQL & "       c.cpf, "
    SQL = SQL & "       c.dt_nascimento, "
    SQL = SQL & "       c.sexo, "
    SQL = SQL & "       c.estado_civil, "
    SQL = SQL & "       d.cgc, "
    SQL = SQL & "       d.contato, "
    SQL = SQL & "       e.endereco,"
    SQL = SQL & "       e.cep,"
    SQL = SQL & "       e.municipio,"
    SQL = SQL & "       e.estado,"
    SQL = SQL & "       cont_agencia_id = isnull(f.cont_agencia_id, f2.cont_agencia_id), "
    SQL = SQL & "       deb_agencia_id = isnull(f.deb_agencia_id, f2.agencia_id), "
    SQL = SQL & "       deb_conta_corrente_id = isnull(f.deb_conta_corrente_id, f.deb_conta_corrente_id), "
    SQL = SQL & "       CASE "
    SQL = SQL & "          WHEN f.dt_inicio_vigencia IS NOT NULL THEN f.dt_inicio_vigencia "
    SQL = SQL & "          WHEN f3.dt_inicio_vigencia IS NOT NULL THEN f3.dt_inicio_vigencia "
    SQL = SQL & "          ELSE a.dt_Contratacao "
    SQL = SQL & "       END AS dt_inicio_vigencia,"
    SQL = SQL & "       CASE "
    SQL = SQL & "          WHEN f.dt_fim_vigencia IS NOT NULL THEN f.dt_fim_vigencia "
    SQL = SQL & "          WHEN f3.dt_fim_vigencia IS NOT NULL THEN f3.dt_fim_vigencia "
    SQL = SQL & "          ELSE CASE ISNULL(r.tp_ramo_id, r3.tp_ramo_id) "
    SQL = SQL & "                  WHEN 1 THEN CASE "
    SQL = SQL & "                                 WHEN a.produto_id NOT IN (115, 150, 850) "
    SQL = SQL & "                                 THEN DATEADD(Month, 12, a.dt_contratacao) "
    SQL = SQL & "                                 ELSE f3.dt_fim_vigencia "
    SQL = SQL & "                              END"
    SQL = SQL & "                  ELSE f3.dt_fim_vigencia "
    SQL = SQL & "               END"
    SQL = SQL & "       END AS dt_fim_vigencia, "
    SQL = SQL & "       custo_certificado = isnull(f.custo_certificado, f2.custo_apolice),"
    SQL = SQL & "       apolice_id = isnull(f.apolice_id, f3.apolice_id), "
    SQL = SQL & "       ramo_id = isnull(f.ramo_id, f3.ramo_id), "
    SQL = SQL & "       nome_produto = g.nome, "
    SQL = SQL & "       g.tp_calc_restituicao, "
    SQL = SQL & "       ISNULL(g.prazo_restituicao_integral, 0) AS prazo_restituicao_integral, "
    SQL = SQL & "       ISNULL(g.prazo_restituicao_integral_renov, 0) AS prazo_restituicao_integral_renov, "
    SQL = SQL & "       CASE "
    SQL = SQL & "          WHEN f.proposta_id IS NOT NULL THEN ISNULL(f.proposta_bb_anterior, 0) "
    SQL = SQL & "          ELSE ISNULL(f2.proposta_bb_anterior, 0) "
    SQL = SQL & "       END AS proposta_bb_anterior "
    SQL = SQL & "  FROM proposta_tb a "
    SQL = SQL & " INNER JOIN cliente_tb b on b.cliente_id = a.prop_cliente_id "
    SQL = SQL & "  LEFT JOIN pessoa_fisica_tb c on c.pf_cliente_id = a.prop_cliente_id "
    SQL = SQL & "  LEFT JOIN pessoa_juridica_tb d on d.pj_cliente_id = a.prop_cliente_id "
    SQL = SQL & " INNER JOIN endereco_corresp_tb e on e.proposta_id = a.proposta_id "
    SQL = SQL & "  LEFT JOIN proposta_adesao_tb f on f.proposta_id = a.proposta_id "
    SQL = SQL & "  LEFT JOIN proposta_fechada_tb f2 on f2.proposta_id = a.proposta_id "
    SQL = SQL & "  LEFT JOIN apolice_tb f3 on f3.proposta_id = a.proposta_id "
    SQL = SQL & "  LEFT JOIN ramo_tb r ON f.ramo_id = r.ramo_id"
    SQL = SQL & "  LEFT JOIN ramo_tb r3 ON f3.ramo_id = r3.ramo_id"
    SQL = SQL & " INNER JOIN produto_tb g on g.produto_id = a.produto_id "
    SQL = SQL & " INNER JOIN subramo_tb sb ON sb.subramo_id = a.subramo_id"
    SQL = SQL & " WHERE a.proposta_id = " & PProposta

    Set rc = rdocn.OpenResultset(SQL)

    If Not rc.EOF Then

        'Exibindo dados selecionados'''''''''''''''''''''''''''''''''''''''''''''''''''''''

        txtDtContratacao = Format(rc!Dt_Contratacao, "dd/mm/yyyy")
        TxtDtInicioVigencia = Format(rc!Dt_Inicio_Vigencia, "dd/mm/yyyy")

        'Jorfilho 15-mai-2003: Tratar o fim de vig�ncia da ap�lice
        If IsNull(rc!Dt_Fim_Vigencia) Then
            TxtDtFimVigencia = ""
        Else
            TxtDtFimVigencia = Format(rc!Dt_Fim_Vigencia, "dd/mm/yyyy")
        End If

        txtAgencia = IIf(IsNull(rc!cont_agencia_id), "0000", rc!cont_agencia_id)

        If IsNull(rc!deb_agencia_id) Then
            txtAgenciaDeb = ""
        Else
            txtAgenciaDeb = rc!deb_agencia_id
        End If

        If IsNull(rc!deb_conta_corrente_id) Then
            txtCCDeb = ""
        Else
            txtCCDeb = rc!deb_conta_corrente_id
        End If

        If IsNull(rc!nome_cliente) Then
            txtCliente = ""
            txtCliente.Tag = ""
        Else
            txtCliente = rc!nome_cliente
            txtCliente.Tag = rc!Cliente_Id
        End If

        If IsNull(rc!Endereco) Then
            txtEndereco = ""
        Else
            txtEndereco = rc!Endereco
        End If

        If IsNull(rc!municipio) Then
            txtMunicipio = ""
        Else
            txtMunicipio = rc!municipio
        End If

        If IsNull(rc!estado) Then
            txtUF = ""
        Else
            txtUF = rc!estado
        End If

        If IsNull(rc!Cep) Then
            txtCEP = ""
        Else
            txtCEP = Mid(rc!Cep, 1, 5) & "-" & Mid(rc!Cep, 6, 3)
        End If

        If IsNull(rc!ddd_1) Then
            txtDDD = ""
        Else
            txtDDD = rc!ddd_1
        End If

        If IsNull(rc!telefone_1) Then
            txtTelefone = ""
        Else
            txtTelefone = rc!telefone_1
        End If

        If IsNull(rc!SEXO) Or UCase(rc!SEXO) = "N" Then
            txtSexo = ""
        Else
            txtSexo = UCase(rc!SEXO)
        End If

        If IsNull(rc!estado_civil) Then
            txtEstCivil = ""
        Else
            Select Case UCase(rc!estado_civil)
                Case "S"
                    txtEstCivil = "SOLTEIRO"
                Case "C"
                    txtEstCivil = "CASADO"
                Case "V"
                    txtEstCivil = "VIUVO"
                Case "E"
                    txtEstCivil = "SEPARADO"
                Case "D"
                    txtEstCivil = "DIVORCIADO"
                Case "N"
                    txtEstCivil = "N�O INFORMADO"
            End Select
        End If

        If IsNull(rc!CPF) Then
            txtCPFTit = ""
        Else
            txtCPFTit = Mid(rc!CPF, 1, 3) & "." & Mid(rc!CPF, 4, 3) & "." & Mid(rc!CPF, 7, 3) & "-" & Mid(rc!CPF, 10, 2)
        End If
        
        If IsNull(rc!dt_nascimento) Then
            txtDtNascTit = ""
        Else
            txtDtNascTit = Format(rc!dt_nascimento, "dd/mm/yyyy")
        End If
       
        Select Case UCase(rc!Situacao)
            Case "A"
                txtStatus = "ATIVA"
            Case "I"
                txtStatus = "ATIVA COM CERTIFICADO EMITIDO"
            Case "C"
                txtStatus = "CANCELADA"
            Case "E"
                txtStatus = "EM ESTUDO"
            Case "P"
                txtStatus = "ERRO NO CADASTRAMENTO"
            Case "V"
                txtStatus = "EM AVALIA��O M�DICA"
            Case "L"
                txtStatus = "PEDIDO DE LAUDO"
            Case "M"
                txtStatus = "PEDIDO DE LAUDO EMITIDO"
            Case "R"
                txtStatus = "RECUSADA"
            Case "S"
                txtStatus = "CARTA RECUSA EMITIDA"
            Case "O"
                txtStatus = "AGUARDANDO RENOVA��O"
        End Select

        If IsNull(rc!CGC) Then
            Tp_Pessoa = "F"
            TxtCGC.Text = ""
        Else
            Tp_Pessoa = "J"
            TxtCGC.Text = rc!CGC
        End If

        If IsNull(rc!nome_produto) Then
            TxtNomeProduto = ""
        Else
            TxtNomeProduto = rc!nome_produto
        End If
        
        'rsilva
        If IsNull(rc("subramo_id")) Then
            txtSubramoId.Text = 0
        Else
            txtSubramoId = rc("subramo_id")
        End If
        

        If IsNull(rc!Tp_Calc_Restituicao) Then
            LblTpCalcRestituicao.Tag = "n"
            LblTpCalcRestituicao.Caption = "N�O INFORMADO"
        Else
            LblTpCalcRestituicao.Tag = rc!Tp_Calc_Restituicao
            Select Case UCase(rc!Tp_Calc_Restituicao)
                Case "C"
                    LblTpCalcRestituicao.Caption = "PRAZO CURTO"
                Case "P"
                    LblTpCalcRestituicao.Caption = "PROPORCIONAL"
            End Select
        End If
        If bModular = True Then
            lblValParametroRestituicao(4).Caption = LCase(LblTpCalcRestituicao.Caption)
            lblValParametroRestituicao(12).Caption = LCase(LblTpCalcRestituicao.Caption)
        End If

        'jfilho - 09/03/2006 - Verifica��o de renova��o para prazo de restitui��o
        If rc!proposta_bb_anterior > 0 Then
            If rc!Prazo_Restituicao_Integral_Renov > 0 Then
                LblPrazoRestituicaoIntegral.Caption = rc!Prazo_Restituicao_Integral_Renov
            Else
                LblPrazoRestituicaoIntegral.Caption = rc!Prazo_Restituicao_Integral
            End If
        Else
            LblPrazoRestituicaoIntegral.Caption = rc!Prazo_Restituicao_Integral
        End If
        
        If bModular = True Then
            lblValParametroRestituicao(0).Caption = LblPrazoRestituicaoIntegral.Caption
            lblValParametroRestituicao(8).Caption = LblPrazoRestituicaoIntegral.Caption
        End If

        If IsNull(rc!custo_certificado) Then
            LblCustoCertifApolice.Caption = "0,00"
        Else
            LblCustoCertifApolice.Caption = FormataValorParaPadraoBrasil(rc!custo_certificado)
        End If
        
        If bModular = True Then
            lblValParametroRestituicao(7).Caption = LblCustoCertifApolice.Caption
            lblValParametroRestituicao(15).Caption = LblCustoCertifApolice.Caption
        End If

        TxtApolice.Text = rc!apolice_id
        TxtRamo.Text = rc!ramo_id
        
        
        'Jorfilho 15-mai-2003: Tratar o fim de vig�ncia da ap�lice
        If IsNull(rc!Dt_Fim_Vigencia) Then
            txtTipoApolice.ForeColor = vbRed
            txtTipoApolice.Text = "ABERTA"
        Else
            txtTipoApolice.ForeColor = vbBlack
            txtTipoApolice.Text = "AVULSA"
        End If

    Else

        Call MsgBox("Os dados da propostas selecionada n�o foram encontrados. O programa ser� cancelado ")
        End

    End If

    rc.Close
    Set rc = Nothing

End Sub

''Sub Obter_Total_Juros()
''
''    Dim Cob As Cobranca
''    Dim TotValJuros As Currency
''
''    For Each Cob In Cobrancas
''
''        If UCase(Cob.Val_Juros) = "A" Then
''           TotValJuros = TotValJuros + Cob.Val_Juros
''        End If
''
''    Next
''
''    LblTotValJuros.Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(TotValJuros))
''
''End Sub

Sub Definir_Exibicao_Objetos()

If Trim(txtCPFTit.Text) <> "" Then
   FrameDadosPessoaFisica.Visible = True
   FrameDadosPessoaJuridica.Visible = False
Else
   FrameDadosPessoaFisica.Visible = False
   FrameDadosPessoaJuridica.Visible = True
End If

If TpOperacao = 1 Then
    btnAceiteTecnico.Visible = True
    btnAprovar.Visible = False
    BtnRetornoParaAvaliacaoTecnica.Visible = False
    'Mask2ValPremioTarifa.Enabled = IIf(vgsituacao = "a", False, True)
    BtnRecusar.Visible = True
    ' ** Solicitado Alian�a **
    If bDtCancelamentoValida Then
      btnAceiteTecnico.Enabled = True
    Else
      btnAceiteTecnico.Enabled = False
    End If
Else
    btnAceiteTecnico.Visible = False
    btnAprovar.Visible = True
    BtnRetornoParaAvaliacaoTecnica.Visible = True
    'Mask2ValPremioTarifa.Enabled = False
    BtnRecusar.Visible = False
End If

' bcarneiro - 03/12/2003 - Projeto Restitui��o por Iniciativa da Seguradora
''If gsTipoProcesso = "R" Then
    BtnRecusar.Caption = "Recusar"
''End If

End Sub

Private Sub Form_Load()

    Call CentraFrm(Me)
    
    'lrocha 29/05/2009 - reiniciando a vari�vel
    restituicao_integral = ""
    
End Sub


Private Sub Mask2ValPremioTarifa_LostFocus()

    Call RecalcularValores

End Sub

Private Sub RecalcularRestituicao(ByVal sPropostaBasicaAdesao As String)

' bcarneiro - 02/12/2003 - Projeto Restitui��o por Iniciativa da Seguradora

Dim cValRestituicao As Currency
Dim cValIOF As Currency
Dim cValCustoApoliceCertif As Currency
Dim cValAdicFracionamento As Currency
Dim cValDesconto As Currency
Dim cValPremioTarifa As Currency
Dim cValTotCorretagem As Currency
Dim cValProLabore As Currency
Dim dPercCorretagem As Double
Dim dPercProLabore As Double
Dim dFatorPremio As Double
Dim dFatorPremioModular As Double
'Rmarins - 20/10/06
Dim cValSubvencaoFederal As Currency
Dim cValSubvencaoEstadual As Currency
Dim dPercFederal As Double
'

'mathayde 27/03/2009
Dim cValRemuneracao As Currency
Dim cValPercRemuneracao As Currency



On Error GoTo TrataErro
    
    ' Obtendo valores ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    
    If Val(TrocaVirgulaPorPonto(TiraPonto(Mask2ValPremioTarifa.Text))) = 0 Then

      LblCustoCertifApoliceRest.Caption = FormataValorParaPadraoBrasil("0.00")
      LblValAdicFracionamento.Caption = FormataValorParaPadraoBrasil("0.00")
      LblValDesconto.Caption = FormataValorParaPadraoBrasil("0.00")
      LblValIOF.Caption = FormataValorParaPadraoBrasil("0.00")
      LblValRestituicao.Caption = FormataValorParaPadraoBrasil("0.00")
      LblTotValCorretagem.Caption = FormataValorParaPadraoBrasil("0.00")
      LblValProLabore.Caption = FormataValorParaPadraoBrasil("0.00")
      'mathayde 27/03/2009
      LblValRemuneracao.Caption = FormataValorParaPadraoBrasil("0.00")
      
      
      If gsPossuiSubvencao = "S" Then 'Rmarins - 20/10/06
         lblValSubvencaoFederal.Caption = FormataValorParaPadraoBrasil("0.00")
      End If

      Exit Sub

    End If
    

        cValRestituicao = Val(TrocaVirgulaPorPonto(TiraPonto(LblValRestituicao.Caption)))
        cValIOF = Val(TrocaVirgulaPorPonto(TiraPonto(LblValIOF.Caption)))
        cValCustoApoliceCertif = Val(TrocaVirgulaPorPonto(TiraPonto(LblCustoCertifApoliceRest.Caption)))
        cValAdicFracionamento = Val(TrocaVirgulaPorPonto(TiraPonto(LblValAdicFracionamento.Caption)))
        cValDesconto = Val(TrocaVirgulaPorPonto(TiraPonto(LblValDesconto.Caption)))
        cValPremioTarifa = Val(TrocaVirgulaPorPonto(TiraPonto(Mask2ValPremioTarifa.Text)))
        cValTotCorretagem = Val(TrocaVirgulaPorPonto(TiraPonto(LblTotValCorretagem.Caption)))
        cValProLabore = Val(TrocaVirgulaPorPonto(TiraPonto(LblValProLabore.Caption)))
        'mathayde 27/03/2009 -  exibe na tela
        cValRemuneracao = Val(TrocaVirgulaPorPonto(TiraPonto(LblValRemuneracao.Caption)))
        
        If gsPossuiSubvencao = "S" Then  'Rmarins - 20/10/06
            dPercFederal = lblPercFederal.Caption
            cValSubvencaoFederal = Val(TrocaVirgulaPorPonto(TiraPonto(lblValSubvencaoFederal.Caption)))
            cValSubvencaoEstadual = Val(TrocaVirgulaPorPonto(TiraPonto(maskValSubvencaoEstadual.Text)))
        End If
        
        dFatorPremio = Val(TrocaVirgulaPorPonto(TiraPonto(LblValPremioTarifaCalc.Caption))) / cValPremioTarifa
        restituicao_integral = ObterFlagRestituicaoIntegral(txtProposta.Text)

        If restituicao_integral = "s" Then
        ' restituicao integral ''''''''''''''''''''''''

        ' total do premio
        'If Not ValidarTotalPremioInformado Then Exit Sub

        If cValPremioTarifa = gcValTotPremioTarifaPago Then

            ' busca os totais pagos
            cValIOF = gcValTotIOFPago
            cValCustoApoliceCertif = gcValCustoApoliceCertif
            cValAdicFracionamento = gcValTotAdicFracionamentoPago
            cValDesconto = gcValTotDescontoPago
    
            ' altera a restituicao parcial para integral
             restituicao_integral = "s"
    
        Else
    
            ' recalcula valores
            cValIOF = 0
            cValCustoApoliceCertif = 0
            cValAdicFracionamento = (Val(TrocaVirgulaPorPonto(TiraPonto(LblValAdicFracionamentoCalc.Caption))) / CDbl(dFatorPremio))
            cValDesconto = (Val(TrocaVirgulaPorPonto(TiraPonto(LblValDescontoCalc.Caption))) / CDbl(dFatorPremio))
    
            ' altera a restituicao integral para parcial
            restituicao_integral = "n"
    
        End If

    Else
    ' restituicao parcial '''''''''''''''''''''''''

      ' total do premio
      'If Not ValidarTotalPremioInformado Then Exit Sub

      If cValPremioTarifa = gcValTotPremioTarifaPago And txtTpEmdossoRest <> "RESTITUI��O POR INICIATIVA DA SEGURADORA" Then

        ' busca os totais pagos
        cValIOF = gcValTotIOFPago
        cValCustoApoliceCertif = gcValCustoApoliceCertif
        cValAdicFracionamento = gcValTotAdicFracionamentoPago
        cValDesconto = gcValTotDescontoPago

        ' altera a restituicao parcial para integral
         restituicao_integral = "s"

      Else

          ' recalcula os valores
          cValIOF = 0
          cValCustoApoliceCertif = 0
          cValAdicFracionamento = (Val(TrocaVirgulaPorPonto(TiraPonto(LblValAdicFracionamentoCalc.Caption))) / CDbl(dFatorPremio))
          cValDesconto = (Val(TrocaVirgulaPorPonto(TiraPonto(LblValDescontoCalc.Caption))) / CDbl(dFatorPremio))

          ' a restituicao permanece parcial
          restituicao_integral = "n"

      End If

    End If

    ' Recalculando valores '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

    cValRestituicao = cValPremioTarifa + cValCustoApoliceCertif + cValAdicFracionamento - cValDesconto + cValIOF
    
    'Rmarins - 20/10/06
    'Recalculando o Valor do Premio em cima dos valores de Subven��o Federal e Estadual ''''''''''''''''''''''''
    If gsPossuiSubvencao = "S" Then
        cValSubvencaoFederal = (cValPremioTarifa / 100)
        cValSubvencaoFederal = cValSubvencaoFederal * dPercFederal
    End If

    dPercCorretagem = dPercCorretagemRest

    'carsilva - 29/04/2004
    'Segundo orienta��o do Paulo, o c�lculo da corretagem foi alterado para ser
    'feito utlizando o valor da restitui��o, ao inv�s do valor do pr�mio tarifa.
    'cValTotCorretagem = CCur(Trunca((cValPremioTarifa * dPercCorretagem) / 100))
    'cValTotCorretagem = CCur(Trunca((cValRestituicao * dPercCorretagem) / 100))
    
    'carsilva - 23/06/2004
    'Altera��o para corre��o do calculo da corretagem. A altera��o anterior s� era v�lida para
    'restitui��es parciais.
    cValTotCorretagem = CCur(Trunca(((cValPremioTarifa + cValAdicFracionamento - cValDesconto) * dPercCorretagem) / 100))
    
    Call Obter_Dados_ProLabore(IIf(Origem = 1, sPropostaBasicaAdesao, txtProposta.Text), _
                               txtDtContratacao.Text, _
                               cValRestituicao, _
                               cValIOF, _
                               cValCustoApoliceCertif, _
                               cValProLabore, _
                               0, _
                               "", _
                               dPercProLabore)
                               
                               
    'Mathayde 27/03/2009 - atualiza valor de remunera��o
    cValPercRemuneracao = ObterPercRemuneracao(produto_id, TxtRamo.Text, Data_Sistema)
    cValRemuneracao = cValRestituicao * (cValPercRemuneracao / 100)
    
    
                                   
    ' Atualizando valores ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    LblCustoCertifApoliceRest.Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValCustoApoliceCertif))
    LblValAdicFracionamento.Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValAdicFracionamento))
    LblValDesconto.Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValDesconto))
    LblValIOF.Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValIOF))
    LblValRestituicao.Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValRestituicao))
    LblTotValCorretagem.Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValTotCorretagem))
    LblValProLabore.Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValProLabore))
    
    'Mathayde 27/03/2009
    LblValRemuneracao.Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValRemuneracao))
    
    
    If gsPossuiSubvencao = "S" Then 'Rmarins - 20/10/06
        lblValSubvencaoFederal.Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValSubvencaoFederal))
    End If

    '## abosco @ 2004-mar-31 - recalculo devido troca de reintegracao parcial para total
    
    Exit Sub

TrataErro:

   Call TrataErroGeral("RecalcularRestituicao", Me.name)
   Call TerminaSEGBR

End Sub

Private Sub RecalcularRestituicaoModular(ByVal sPropostaBasicaAdesao As String, _
                                         ByVal Tipo As Integer)

' bcarneiro - 02/12/2003 - Projeto Restitui��o por Iniciativa da Seguradora

Dim cValRestituicao As Currency
Dim cValIOF As Currency
Dim cValCustoApoliceCertif As Currency
Dim cValAdicFracionamento As Currency
Dim cValDesconto As Currency
Dim cValPremioTarifa As Currency
Dim cValTotCorretagem As Currency
Dim cValProLabore As Currency
Dim dPercCorretagem As Double
Dim dPercProLabore As Double
Dim dFatorPremio As Double

'Rmarins - 20/10/06
Dim cValSubvencaoFederal As Currency
Dim cValSubvencaoEstadual As Currency
Dim dPercFederal As Double
'

'mathayde 27/03/2009
Dim cValRemuneracao As Currency
Dim cValPercRemuneracao As Currency



'rsilva 03/09/2009
Dim sDataInicio As String
Dim sDataFim As String
Dim sDataCanc As String
Dim dFatorDias As Double
Dim dDias As Double



On Error GoTo TrataErro
    
    ' Obtendo valores ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Select Case Tipo

    Case 0
        If Val(TrocaVirgulaPorPonto(TiraPonto(Mask2ValPremioTarifaModular(0).Text))) = 0 Then
            
            Mask2ValPremioTarifaModular(0).Text = FormataValorParaPadraoBrasil("0.00")
            lblvalCalculadoModular(9).Caption = FormataValorParaPadraoBrasil("0.00")
            lblvalCalculadoModular(10).Caption = FormataValorParaPadraoBrasil("0.00")
            lblvalCalculadoModular(11).Caption = FormataValorParaPadraoBrasil("0.00")
            lblvalCalculadoModular(12).Caption = FormataValorParaPadraoBrasil("0.00")
            lblvalCalculadoModular(13).Caption = FormataValorParaPadraoBrasil("0.00")
            lblvalCalculadoModular(14).Caption = FormataValorParaPadraoBrasil("0.00")
            lblvalCalculadoModular(15).Caption = FormataValorParaPadraoBrasil("0.00")
            lblvalCalculadoModular(16).Caption = FormataValorParaPadraoBrasil("0.00")
          
          Exit Sub
    
        End If
    
        cValRestituicao = Val(TrocaVirgulaPorPonto(TiraPonto(lblvalCalculadoModular(13).Caption)))
        cValIOF = Val(TrocaVirgulaPorPonto(TiraPonto(lblvalCalculadoModular(12).Caption)))
        cValCustoApoliceCertif = Val(TrocaVirgulaPorPonto(TiraPonto(lblvalCalculadoModular(9).Caption)))
        cValAdicFracionamento = Val(TrocaVirgulaPorPonto(TiraPonto(lblvalCalculadoModular(10).Caption)))
        cValDesconto = Val(TrocaVirgulaPorPonto(TiraPonto(lblvalCalculadoModular(11).Caption)))
        cValPremioTarifa = Val(TrocaVirgulaPorPonto(TiraPonto(Mask2ValPremioTarifaModular(0).Text)))
        cValTotCorretagem = Val(TrocaVirgulaPorPonto(TiraPonto(lblvalCalculadoModular(14).Caption)))
        cValProLabore = Val(TrocaVirgulaPorPonto(TiraPonto(lblvalCalculadoModular(15).Caption)))
        cValRemuneracao = Val(TrocaVirgulaPorPonto(TiraPonto(lblvalCalculadoModular(16).Caption)))

        dFatorPremio = Val(TrocaVirgulaPorPonto(TiraPonto(lblvalCalculadoModular(5).Caption))) / cValPremioTarifa
        restituicao_integral = ObterFlagRestituicaoIntegral(txtpropostaModular(0).Text)

        If restituicao_integral = "s" Then
        ' restituicao integral ''''''''''''''''''''''''
            If cValPremioTarifa = gcValTotPremioTarifaPago Then
                ' busca os totais pagos
                cValIOF = gcValTotIOFPago
                cValCustoApoliceCertif = gcValCustoApoliceCertif
                cValAdicFracionamento = gcValTotAdicFracionamentoPago
                cValDesconto = gcValTotDescontoPago
                ' altera a restituicao parcial para integral
                 restituicao_integral = "s"
             Else
                ' recalcula valores
                cValIOF = 0
                cValCustoApoliceCertif = 0
                cValAdicFracionamento = (Val(TrocaVirgulaPorPonto(TiraPonto(LblValAdicFracionamentoCalc.Caption))) / CDbl(dFatorPremio))
                cValDesconto = (Val(TrocaVirgulaPorPonto(TiraPonto(LblValDescontoCalc.Caption))) / CDbl(dFatorPremio))
                ' altera a restituicao integral para parcial
                restituicao_integral = "n"
             End If
            Else
                ' restituicao parcial '''''''''''''''''''''''''
        
                ' total do premio
                'If Not ValidarTotalPremioInformado Then Exit Sub

                If cValPremioTarifa = CDbl(lblValParametroRestituicao(2).Caption) And txtTpEmdossoRest <> "RESTITUI��O POR INICIATIVA DA SEGURADORA" Then

                ' busca os totais pagos
                cValIOF = gcValTotIOFPago
                cValCustoApoliceCertif = gcValCustoApoliceCertif
                cValAdicFracionamento = gcValTotAdicFracionamentoPago
                cValDesconto = gcValTotDescontoPago

                ' altera a restituicao parcial para integral
                restituicao_integral = "s"

            Else

                ' recalcula os valores
                cValIOF = 0
                cValCustoApoliceCertif = 0
                If dFatorPremio > 0 Then
                    cValAdicFracionamento = (Val(TrocaVirgulaPorPonto(TiraPonto(lblvalCalculadoModular(2).Caption))) / CDbl(dFatorPremio))
                    cValDesconto = (Val(TrocaVirgulaPorPonto(TiraPonto(lblvalCalculadoModular(3).Caption))) / CDbl(dFatorPremio))
                Else
                    cValAdicFracionamento = 0
                    cValDesconto = 0
                End If
                

                  ' a restituicao permanece parcial
                restituicao_integral = "n"

              End If

            End If
    
    ' Recalculando valores '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    'rsilva - 02/09/2009
    
    'cValRestituicao = cValPremioTarifa + oDadosModular.PercIOF
'    sDataInicio = oDadosModular.sDtInicioVigenciaEsc
'    sDataFim = oDadosModular.sDtFimVigenciaEsc
'
'    sDataCanc = Trim(FrmAvaliacao.TxtDtCancelamentoBB.Text)
'
'    'dFatorDias = (DateDiff("d", sDataInicio, sDataFim) / 365)
'    dDias = DateDiff("d", sDataCanc, sDataFim)
'
'    'cValRestituicao = (cValPremioTarifa / (1 + dPercIOF))
'    cValRestituicao = cValPremioTarifa * (dDias / 30)

    If produto_id = 1168 Then
        sDataInicio = oDadosModular.sDtInicioVigenciaEsc
        sDataFim = oDadosModular.sDtFimVigenciaEsc
        sDataCanc = Trim(FrmAvaliacao.TxtDtCancelamentoBB.Text)
        
        dDias = DateDiff("d", sDataCanc, sDataFim)
        cValRestituicao = cValPremioTarifa * (dDias / 30)
    End If
    
    'cValRestituicao = cValPremioTarifa + cValCustoApoliceCertif + cValAdicFracionamento - cValDesconto + cValIOF

    dPercCorretagem = dPercCorretagemRest

    cValTotCorretagem = CCur(Trunca(((cValPremioTarifa + cValAdicFracionamento - cValDesconto) * dPercCorretagem) / 100))
    
    Call Obter_Dados_ProLabore(IIf(Origem = 1, sPropostaBasicaAdesao, txtProposta.Text), _
                               txtDtContratacao.Text, _
                               cValRestituicao, _
                               cValIOF, _
                               cValCustoApoliceCertif, _
                               cValProLabore, _
                               0, _
                               "", _
                               dPercProLabore)
                               
                               
    'Mathayde 27/03/2009 - atualiza valor de remunera��o
    cValPercRemuneracao = ObterPercRemuneracao(produto_id, TxtRamo.Text, Data_Sistema)
    cValRemuneracao = cValRestituicao * (cValPercRemuneracao / 100)
    
    
                                   
    ' Atualizando valores ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    lblvalCalculadoModular(9).Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValCustoApoliceCertif))
    lblvalCalculadoModular(10).Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValAdicFracionamento))
    lblvalCalculadoModular(11).Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValDesconto))
    lblvalCalculadoModular(12).Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValIOF))
    lblvalCalculadoModular(13).Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValRestituicao))
    lblvalCalculadoModular(14).Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValTotCorretagem))
    lblvalCalculadoModular(15).Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValProLabore))
    
    'Mathayde 27/03/2009
    lblvalCalculadoModular(16).Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValRemuneracao))
'########################################################
    
    Case 1
        
        If Val(TrocaVirgulaPorPonto(TiraPonto(Mask2ValPremioTarifaModular(1).Text))) = 0 Then
            
            Mask2ValPremioTarifaModular(1).Text = FormataValorParaPadraoBrasil("0.00")
            lblvalCalculadoModular(26).Caption = FormataValorParaPadraoBrasil("0.00")
            lblvalCalculadoModular(27).Caption = FormataValorParaPadraoBrasil("0.00")
            lblvalCalculadoModular(28).Caption = FormataValorParaPadraoBrasil("0.00")
            lblvalCalculadoModular(29).Caption = FormataValorParaPadraoBrasil("0.00")
            lblvalCalculadoModular(30).Caption = FormataValorParaPadraoBrasil("0.00")
            lblvalCalculadoModular(31).Caption = FormataValorParaPadraoBrasil("0.00")
            lblvalCalculadoModular(32).Caption = FormataValorParaPadraoBrasil("0.00")
            lblvalCalculadoModular(33).Caption = FormataValorParaPadraoBrasil("0.00")
          
          Exit Sub
    
        End If
    
            
            cValCustoApoliceCertif = Val(TrocaVirgulaPorPonto(TiraPonto(lblvalCalculadoModular(26).Caption)))
            cValAdicFracionamento = Val(TrocaVirgulaPorPonto(TiraPonto(lblvalCalculadoModular(27).Caption)))
            cValDesconto = Val(TrocaVirgulaPorPonto(TiraPonto(lblvalCalculadoModular(28).Caption)))
            cValIOF = Val(TrocaVirgulaPorPonto(TiraPonto(lblvalCalculadoModular(29).Caption)))
            cValRestituicao = Val(TrocaVirgulaPorPonto(TiraPonto(lblvalCalculadoModular(30).Caption)))
            cValTotCorretagem = Val(TrocaVirgulaPorPonto(TiraPonto(lblvalCalculadoModular(31).Caption)))
            cValProLabore = Val(TrocaVirgulaPorPonto(TiraPonto(lblvalCalculadoModular(32).Caption)))
            cValRemuneracao = Val(TrocaVirgulaPorPonto(TiraPonto(lblvalCalculadoModular(33).Caption)))
            cValPremioTarifa = Val(TrocaVirgulaPorPonto(TiraPonto(Mask2ValPremioTarifaModular(1).Text)))
            
            dFatorPremio = Val(TrocaVirgulaPorPonto(TiraPonto(lblvalCalculadoModular(22).Caption))) / cValPremioTarifa
            restituicao_integral = ObterFlagRestituicaoIntegral(txtpropostaModular(1).Text)

        If restituicao_integral = "s" Then
        ' restituicao integral ''''''''''''''''''''''''
            If cValPremioTarifa = oDadosModular.Val_Premio_Pago Then
                ' busca os totais pagos
                cValIOF = oDadosModular.Val_IOF_Restituicao
                cValCustoApoliceCertif = oDadosModular.Custo_Apolice
                cValAdicFracionamento = oDadosModular.Val_Adic_Fracionamento
                cValDesconto = oDadosModular.Val_Desconto
                ' altera a restituicao parcial para integral
                 restituicao_integral = "s"
            Else
                ' recalcula valores
                cValIOF = 0
                cValCustoApoliceCertif = 0
                cValAdicFracionamento = (Val(TrocaVirgulaPorPonto(TiraPonto(lblvalCalculadoModular(19).Caption))) / CDbl(dFatorPremio))
                cValDesconto = (Val(TrocaVirgulaPorPonto(TiraPonto(lblvalCalculadoModular(20).Caption))) / CDbl(dFatorPremio))
                ' altera a restituicao integral para parcial
                restituicao_integral = "n"
            End If
    Else
    ' restituicao parcial '''''''''''''''''''''''''

      ' total do premio
      'If Not ValidarTotalPremioInformado Then Exit Sub

        If cValPremioTarifa = CDbl(lblValParametroRestituicao(10).Caption) And txtTpEmdossoRest <> "RESTITUI��O POR INICIATIVA DA SEGURADORA" Then
    
            ' busca os totais pagos
            cValIOF = oDadosModular.Val_IOF_Restituicao
            cValCustoApoliceCertif = oDadosModular.Custo_Apolice
            cValAdicFracionamento = oDadosModular.Val_Adic_Fracionamento
            cValDesconto = oDadosModular.Val_Desconto

            ' altera a restituicao parcial para integral
            restituicao_integral = "s"

        Else
            'recalcula os valores
            cValIOF = 0
            cValCustoApoliceCertif = 0
'            cValAdicFracionamento = (Val(TrocaVirgulaPorPonto(TiraPonto(lblvalCalculadoModular(19).Caption))) / CDbl(dFatorPremio))
'            cValDesconto = (Val(TrocaVirgulaPorPonto(TiraPonto(lblvalCalculadoModular(20).Caption))) / CDbl(dFatorPremio))
            If dFatorPremio > 0 Then
                cValAdicFracionamento = (Val(TrocaVirgulaPorPonto(TiraPonto(lblvalCalculadoModular(2).Caption))) / CDbl(dFatorPremio))
                cValDesconto = (Val(TrocaVirgulaPorPonto(TiraPonto(lblvalCalculadoModular(3).Caption))) / CDbl(dFatorPremio))
            Else
                cValAdicFracionamento = 0
                cValDesconto = 0
            End If

          ' a restituicao permanece parcial
          restituicao_integral = "n"

      End If

    End If

    ' Recalculando valores '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    
    sDataInicio = oDadosModular.sDtInicioVigenciaEsc
    sDataFim = oDadosModular.sDtFimVigenciaEsc
    sDataCanc = Trim(FrmAvaliacao.TxtDtCancelamentoBB.Text)
    
    dDias = DateDiff("d", sDataCanc, sDataFim)
    
    'cValRestituicao = (cValPremioTarifa / (1 + dPercIOF))
    cValRestituicao = cValPremioTarifa * (dDias / 30)
    
    dPercCorretagem = oDadosModular.PercCorretagemRest  'dPercCorretagemRest

    cValTotCorretagem = CCur(Trunca(((cValPremioTarifa + cValAdicFracionamento - cValDesconto) * dPercCorretagem)) / 100)
    
    Call Obter_Dados_ProLabore(IIf(Origem = 1, sPropostaBasicaAdesao, txtpropostaModular(1).Text), _
                               oDadosModular.Dt_Contratacao, _
                               cValRestituicao, _
                               cValIOF, _
                               cValCustoApoliceCertif, _
                               cValProLabore, _
                               0, _
                               "", _
                               dPercProLabore)
                               
                               
    'Mathayde 27/03/2009 - atualiza valor de remunera��o
    cValPercRemuneracao = ObterPercRemuneracao(oDadosModular.produto_id, oDadosModular.ramo_id, Data_Sistema)
    cValRemuneracao = cValRestituicao * (cValPercRemuneracao / 100)
    
    
                                   
    ' Atualizando valores ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    lblvalCalculadoModular(26).Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValCustoApoliceCertif))
    lblvalCalculadoModular(27).Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValAdicFracionamento))
    lblvalCalculadoModular(28).Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValDesconto))
    lblvalCalculadoModular(29).Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValIOF))
    lblvalCalculadoModular(30).Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValRestituicao))
    lblvalCalculadoModular(31).Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValTotCorretagem))
    lblvalCalculadoModular(32).Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValProLabore))
    
    'Mathayde 27/03/2009
    lblvalCalculadoModular(33).Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValRemuneracao))
    
    End Select
    
    Exit Sub

TrataErro:

   Call TrataErroGeral("RecalcularRestituicao", Me.name)
   Call TerminaSEGBR

End Sub

Sub AutorizarRestituicao()

' bcarneiro - 03/12/2003 - Projeto Restitui��o por Iniciativa da Seguradora

Const SITUACAO_AVALIACAO_TECNICA = "T"

On Error GoTo TrataErro

    'Atualizando interface'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    
    MousePointer = vbHourglass
    
    'Autorizar cancelamento''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        
    Call RegistrarAvaliacaoRestituicao(SITUACAO_AVALIACAO_TECNICA)
    
    'Desbloqueando proposta'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    
    Call Atualizar_Bloqueio_Proposta(txtProposta.Text, "n")
    
    'Atualizando interface'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    
    MousePointer = vbDefault
    
    
    MsgBox "Autoriza��o efetuada", vbOKOnly + vbInformation, ""
    
    
'    Unload Me
'    FrmSelEndosso.Show
'    FrmSelEndosso.Marcar_Linha_Processada

    Exit Sub
    
TrataErro:
   Call TrataErroGeral("AutorizarRestituicao", Me.name)
   Call TerminaSEGBR

End Sub

Sub RegistrarAvaliacaoRestituicao(ByVal sSituacao As String)

' bcarneiro - 03/12/2003 - Projeto Restitui��o por Iniciativa da Seguradora

Dim oFinanceiro As Object

Dim cValPremioTarifa As Currency
Dim cValCustoApoliceCertif As Currency
Dim cValAdicFracionamento As Currency
Dim cValDesconto As Currency
Dim cValIOF As Currency
Dim cValRestituicao As Currency
Dim cValTotCorretagem As Currency
Dim cValProLabore As Currency
Dim cValSubvencaoFederal As Currency 'Rmarins - 20/10/06
Dim cValSubvencaoEstadual As Currency '

'MATHAYDE 27/03/2009 - C�lculo da remunera��o de do IR da remunera��o
Dim cValRemuneracao As Currency
Dim cValIrRemuneracao As Currency
Dim cPercIR As Currency
'''''''''''''''''''''''''''''''''
Dim endosso_id As Long



On Error GoTo TrataErro

    'Atualizando interface''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    
    MousePointer = vbHourglass
    
    'Obtendo valores opera��o''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    If bModular = False Then
        cValPremioTarifa = Val(TrocaVirgulaPorPonto(TiraPonto(Mask2ValPremioTarifa.Text)))
        cValCustoApoliceCertif = Val(TrocaVirgulaPorPonto(TiraPonto(LblCustoCertifApoliceRest.Caption)))
        cValAdicFracionamento = Val(TrocaVirgulaPorPonto(TiraPonto(LblValAdicFracionamento.Caption)))
        cValDesconto = Val(TrocaVirgulaPorPonto(TiraPonto(LblValDesconto.Caption)))
        cValIOF = Val(TrocaVirgulaPorPonto(TiraPonto(LblValIOF.Caption)))
        cValRestituicao = Val(TrocaVirgulaPorPonto(TiraPonto(LblValRestituicao.Caption)))
        cValTotCorretagem = Val(TrocaVirgulaPorPonto(TiraPonto(LblTotValCorretagem.Caption)))
        cValProLabore = Val(TrocaVirgulaPorPonto(TiraPonto(LblValProLabore.Caption)))
        cValSubvencaoFederal = Val(TrocaVirgulaPorPonto(TiraPonto(lblValSubvencaoFederal.Caption))) 'Rmarins - 20/10/06
        cValSubvencaoEstadual = Val(TrocaVirgulaPorPonto(TiraPonto(maskValSubvencaoEstadual.Text)))
        'MATHAYDE 27/03/2009
        cValRemuneracao = Val(TrocaVirgulaPorPonto(TiraPonto(LblValRemuneracao.Caption)))
        
        Set oFinanceiro = CreateObject("SEGL0026.cls00125")
    
        Call oFinanceiro.AtualizarAvaliacaoRestituicao(gsSIGLASISTEMA, _
                                                       App.Title, _
                                                       App.FileDescription, _
                                                       glAmbiente_id, _
                                                       cUserName, _
                                                       Data_Sistema, _
                                                       txtProposta.Text, _
                                                       TxtEndossoBB.Text, _
                                                       sSituacao, _
                                                       cValPremioTarifa, _
                                                       cValCustoApoliceCertif, _
                                                       cValAdicFracionamento, _
                                                       cValDesconto, _
                                                       cValIOF, _
                                                       cValRestituicao, _
                                                       cValTotCorretagem, _
                                                       cValProLabore, , , , _
                                                       restituicao_integral, _
                                                       gsPossuiSubvencao, _
                                                       cValSubvencaoFederal, _
                                                      cValSubvencaoEstadual, _
                                                       , cValRemuneracao)
    'Mathayde 30/03/2009 - Envia cValRemuneracai para gravar em avaliacao_restituicao_tb
             
    Else
    
        'Verificar se � ser tem q ser pego o calc mesmo'''''''''''''''''''
        cValPremioTarifa = Val(TrocaVirgulaPorPonto(TiraPonto(Mask2ValPremioTarifaModular(0).Text)))
        cValCustoApoliceCertif = Val(TrocaVirgulaPorPonto(TiraPonto(lblvalCalculadoModular(9).Caption)))
        cValAdicFracionamento = Val(TrocaVirgulaPorPonto(TiraPonto(lblvalCalculadoModular(10).Caption)))
        cValDesconto = Val(TrocaVirgulaPorPonto(TiraPonto(lblvalCalculadoModular(11).Caption)))
        cValIOF = Val(TrocaVirgulaPorPonto(TiraPonto(lblvalCalculadoModular(12).Caption)))
        cValRestituicao = Val(TrocaVirgulaPorPonto(TiraPonto(lblvalCalculadoModular(13).Caption)))
        cValTotCorretagem = Val(TrocaVirgulaPorPonto(TiraPonto(lblvalCalculadoModular(14).Caption)))
        cValProLabore = Val(TrocaVirgulaPorPonto(TiraPonto(lblvalCalculadoModular(15).Caption)))
        cValSubvencaoFederal = 0 'Val(TrocaVirgulaPorPonto(TiraPonto(lblValSubvencaoFederal.Caption))) 'Rmarins - 20/10/06
        cValSubvencaoEstadual = 0 'Val(TrocaVirgulaPorPonto(TiraPonto(maskValSubvencaoEstadual.Text)))
        'MATHAYDE 27/03/2009
        cValRemuneracao = Val(TrocaVirgulaPorPonto(TiraPonto(lblvalCalculadoModular(16).Caption)))
        'Registrando Avalia��o ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    
        Set oFinanceiro = CreateObject("SEGL0026.cls00125")
        
        Call oFinanceiro.AtualizarAvaliacaoRestituicao(gsSIGLASISTEMA, _
                                                       App.Title, _
                                                       App.FileDescription, _
                                                       glAmbiente_id, _
                                                       cUserName, _
                                                       Data_Sistema, _
                                                       txtProposta.Text, _
                                                       TxtEndossoBB.Text, _
                                                       sSituacao, _
                                                       cValPremioTarifa, _
                                                       cValCustoApoliceCertif, _
                                                       cValAdicFracionamento, _
                                                       cValDesconto, _
                                                       cValIOF, _
                                                       cValRestituicao, _
                                                       cValTotCorretagem, _
                                                       cValProLabore, , , , _
                                                       restituicao_integral, _
                                                       gsPossuiSubvencao, _
                                                       cValSubvencaoFederal, _
                                                       cValSubvencaoEstadual, _
                                                       , cValRemuneracao)
        'Mathayde 30/03/2009 - Envia cValRemuneracai para gravar em avaliacao_restituicao_tb
             
        Set oFinanceiro = Nothing
        
        cValPremioTarifa = Val(TrocaVirgulaPorPonto(TiraPonto(Mask2ValPremioTarifaModular(1).Text)))
        cValCustoApoliceCertif = Val(TrocaVirgulaPorPonto(TiraPonto(lblvalCalculadoModular(26).Caption)))
        cValAdicFracionamento = Val(TrocaVirgulaPorPonto(TiraPonto(lblvalCalculadoModular(27).Caption)))
        cValDesconto = Val(TrocaVirgulaPorPonto(TiraPonto(lblvalCalculadoModular(28).Caption)))
        cValIOF = Val(TrocaVirgulaPorPonto(TiraPonto(lblvalCalculadoModular(29).Caption)))
        cValRestituicao = Val(TrocaVirgulaPorPonto(TiraPonto(lblvalCalculadoModular(30).Caption)))
        cValTotCorretagem = Val(TrocaVirgulaPorPonto(TiraPonto(lblvalCalculadoModular(31).Caption)))
        cValProLabore = Val(TrocaVirgulaPorPonto(TiraPonto(lblvalCalculadoModular(32).Caption)))
        cValSubvencaoFederal = 0 'Val(TrocaVirgulaPorPonto(TiraPonto(lblValSubvencaoFederal.Caption))) 'Rmarins - 20/10/06
        cValSubvencaoEstadual = 0 'Val(TrocaVirgulaPorPonto(TiraPonto(maskValSubvencaoEstadual.Text)))
        'MATHAYDE 27/03/2009
        cValRemuneracao = Val(TrocaVirgulaPorPonto(TiraPonto(lblvalCalculadoModular(33).Caption)))
        'Registrando Avalia��o ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    
        Set oFinanceiro = CreateObject("SEGL0026.cls00125")
        
        Call oFinanceiro.AtualizarAvaliacaoRestituicao(gsSIGLASISTEMA, _
                                                       App.Title, _
                                                       App.FileDescription, _
                                                       glAmbiente_id, _
                                                       cUserName, _
                                                       Data_Sistema, _
                                                       oDadosModular.proposta_id, _
                                                       oDadosModular.Num_avaliacao, _
                                                       sSituacao, _
                                                       cValPremioTarifa, _
                                                       cValCustoApoliceCertif, _
                                                       cValAdicFracionamento, _
                                                       cValDesconto, _
                                                       cValIOF, _
                                                       cValRestituicao, _
                                                       cValTotCorretagem, _
                                                       cValProLabore, , , , _
                                                       restituicao_integral, _
                                                       gsPossuiSubvencao, _
                                                       cValSubvencaoFederal, _
                                                       cValSubvencaoEstadual, _
                                                       , cValRemuneracao)
        'Mathayde 30/03/2009 - Envia cValRemuneracai para gravar em avaliacao_restituicao_tb
             

    End If
    
'daniel.menandro - Nova Consultoria - 14/07/2013
'16351367 - Extrato de Restitui��o
'Inicio

endosso_id = Obter_Endosso("T")

Call oFinanceiro.InserirExtratoRestituicao(gsSIGLASISTEMA, _
                                                    App.Title, _
                                                    App.FileDescription, _
                                                    glAmbiente_id, _
                                                    cUserName, _
                                                    Val(txtProposta.Text), _
                                                    cValRestituicao, _
                                                    endosso_id, _
                                                    0, _
                                                    "Aprova��o t�cnica", _
                                                    "Aguardando avalia��o gerencial de restitui��o")


        Set oFinanceiro = Nothing

'Fim
        
    
    
    'Atualizando interface'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    
    MousePointer = vbDefault
    
    Exit Sub

TrataErro:
   Call TrataErroGeral("RegistrarAvaliacaoRestituicao", Me.name)
   Call TerminaSEGBR

End Sub

'daniel.menandro - Nova Consultoria - 14/07/2013
'16351367 - Extrato de Restitui��o
'Inicio
Private Function Obter_Endosso(Situacao As String) As Long
  Dim sSQL As String
  Dim rsEndosso As rdoResultset

sSQL = ""
sSQL = sSQL & "select top 1 endosso_id, tp_endosso_id "
sSQL = sSQL & "FROM avaliacao_restituicao_tb "
sSQL = sSQL & "Where proposta_id = " & Val(txtProposta.Text)
Select Case Situacao
    Case "T"
      sSQL = sSQL & " and situacao = 'T' "
    Case "I"
      sSQL = sSQL & " and situacao = 'I' "
    Case "R"
      sSQL = sSQL & " and situacao = 'R' "
  End Select

sSQL = sSQL & "order by num_avaliacao desc "

 Set rsEndosso = rdocn.OpenResultset(sSQL)

 Obter_Endosso = CLng(IIf(IsNull(rsEndosso!endosso_id) = True, 0, rsEndosso!endosso_id))
 rsEndosso.Close
   
  Set rsEndosso = Nothing

End Function
' Fim

Private Sub RecusarRestituicao()

Dim iMotivoRecusa As Integer
Dim tMotivoRecusa As String

On Error GoTo TrataErro


    iMotivoRecusa = Obter_Motivo_Recusa(tMotivoRecusa)
    
    If iMotivoRecusa <> 0 Then
        
        'Atualizando interface'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        
        MousePointer = vbHourglass
        
        'Recusando pedido de restitui��o ''''''''''''''''''''''''''''''''''''''''''''''''''''''
        
        Call RecusarAvaliacaoRestituicao(iMotivoRecusa, tMotivoRecusa)
        
        'Desbloqueando proposta'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    
        Call Atualizar_Bloqueio_Proposta(txtProposta.Text, "n")
    
        'Atualizando interface'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    
        MousePointer = vbDefault
        
        MsgBox "Recusa efetuada", , ""
        
       
        
        sSQL = "delete from #avaliacao_restituicao_tb where proposta_id = " & txtProposta.Text & vbNewLine
     
        Unload Me
        Set rc = rdocn.OpenResultset(sSQL)
        If formAnterior = "frmFiltroPesquisa" Then
            Call Unload(frmFiltroPesquisa)
            frmFiltroPesquisa.Show
        Else
            frmAnalitico.strWhere = strWhere
            frmAnalitico.inicial = inicial
            frmAnalitico.intervalo = intervalo
            frmAnalitico.maiorQue = maiorQue
            frmAnalitico.idProduto = idProduto
            frmAnalitico.QTD = QTD

            frmAnalitico.sProduto = sProduto ' Alan Neves - C_ANEVES - - Demanda: 17860335 - 28/05/2013

            frmAnalitico.carregarForm
            frmAnalitico.Show
            
        End If
                
    End If

    Exit Sub

TrataErro:
   Call TrataErroGeral("RecusarRestituicao", Me.name)
   Call TerminaSEGBR

End Sub


Private Sub RecusarAvaliacaoRestituicao(ByVal iMotivoRecusa As Integer, Optional ByVal sDescricaoMotivo As String)

' bcarneiro - 03/12/2003 - Projeto Restitui��o por Iniciativa da Seguradora

Const SITUACAO_RECUSA_TECNICA = "R"

Dim oFinanceiro As Object
Dim endosso_id As Long

On Error GoTo TrataErro

    'Atualizando interface''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    
    MousePointer = vbHourglass
    
    'Recusando avalia��o t�cnica''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    
    Set oFinanceiro = CreateObject("SEGL0026.cls00125")
    
    Call oFinanceiro.AtualizarAvaliacaoRestituicao(gsSIGLASISTEMA, _
                                                   App.Title, _
                                                   App.FileDescription, _
                                                   glAmbiente_id, _
                                                   cUserName, _
                                                   Data_Sistema, _
                                                   txtProposta.Text, _
                                                   TxtEndossoBB.Text, _
                                                   SITUACAO_RECUSA_TECNICA, _
                                                   , _
                                                   , _
                                                   , _
                                                   , _
                                                   , _
                                                   , _
                                                   , _
                                                   , _
                                                   iMotivoRecusa, _
                                                   , , , , , , -1, LblValRemuneracao.Caption)
                                                   'FLOW 1186681 - Confitec - JFILHO - Corrigindo erro de l�gica, que estava passando os par�metros de forma errada.
                                                   ', , , , , , LblValRemuneracao.Caption)
'daniel.menandro - Nova Consultoria - 14/07/2013
'16351367 - Extrato de Restitui��o
'Inicio
             
endosso_id = Obter_Endosso("R")

Call oFinanceiro.InserirExtratoRestituicao(gsSIGLASISTEMA, _
                                                    App.Title, _
                                                    App.FileDescription, _
                                                    glAmbiente_id, _
                                                    cUserName, _
                                                    Val(txtProposta.Text), _
                                                    LblValRestituicaoEstimado, _
                                                    endosso_id, _
                                                    0, _
                                                    "Recusa T�cnica", _
                                                    "Endosso de restitui��o recusada pelo t�cnico", _
                                                    sDescricaoMotivo)

             
    Set oFinanceiro = Nothing
    
    'Atualizando interface'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    
    MousePointer = vbDefault
    
    Exit Sub

TrataErro:
   Call TrataErroGeral("RecusarAvaliacaoRestituicao", Me.name)
   Call TerminaSEGBR
    
End Sub

Sub RetornarAvaliacaoTecnicaRestituicao()

' bcarneiro - 03/12/2003 - Projeto Restitui��o por Iniciativa da Seguradora

Dim oFinanceiro As Object
Dim sMotivoRetornoInformado As String

On Error GoTo TrataErro

    sMotivoRetornoInformado = Obter_Motivo_Retorno
    
    If Trim(sMotivoRetornoInformado) <> "" Then
        
        'Recusando avalia��o t�cnica''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        
        Call RetornarAvaliacaoRestituicao(sMotivoRetornoInformado, sMotivoRetornoInformado)
        
        'Desbloqueando proposta''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        
        Call Atualizar_Bloqueio_Proposta(txtProposta.Text, "n")
        
        'Atualizando interface'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        
        MsgBox "Retorno efetuado", , ""
        
        sSQL = "delete from #avaliacao_restituicao_tb where proposta_id = " & txtProposta.Text & vbNewLine

          Unload Me
      
        Set rc = rdocn.OpenResultset(sSQL)

        
        Unload Me
        
        If formAnterior = "frmFiltroPesquisa" Then
            Call Unload(frmFiltroPesquisa)
            frmFiltroPesquisa.Show
        Else
            frmAnalitico.strWhere = strWhere
            frmAnalitico.inicial = inicial
            frmAnalitico.intervalo = intervalo
            frmAnalitico.maiorQue = maiorQue
            frmAnalitico.idProduto = idProduto

            frmAnalitico.sProduto = sProduto  'Alan Neves - C_ANEVES - Demanda: 17860335 - 29/05/2013 - Evite select para recuperar o nome do produto utilizado
            
            frmAnalitico.QTD = QTD
            frmAnalitico.carregarForm
            frmAnalitico.Show
            
        End If
    End If
    
    Exit Sub

TrataErro:
   Call TrataErroGeral("RetornarAvaliacaoTecnicaRestituicao", Me.name)
   Call TerminaSEGBR

End Sub

Private Sub RetornarAvaliacaoRestituicao(ByVal sMotivoRetorno As String, _
                                         Optional ByVal sDescricaoMotivo As String)

' bcarneiro - 03/12/2003 - Projeto Restitui��o por Iniciativa da Seguradora

Const SITUACAO_RETORNO_TECNICA = "I"

Dim oFinanceiro As Object
Dim endosso_id As Long

On Error GoTo TrataErro

    'Atualizando interface''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    
    MousePointer = vbHourglass
    
    'Recusando avalia��o t�cnica''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    
    Set oFinanceiro = CreateObject("SEGL0026.cls00125")
    
    Call oFinanceiro.AtualizarAvaliacaoRestituicao(gsSIGLASISTEMA, _
                                                   App.Title, _
                                                   App.FileDescription, _
                                                   glAmbiente_id, _
                                                   cUserName, _
                                                   Data_Sistema, _
                                                   txtProposta.Text, _
                                                   TxtEndossoBB.Text, _
                                                   SITUACAO_RETORNO_TECNICA, _
                                                   , _
                                                   , _
                                                   , _
                                                   , _
                                                   , _
                                                   , _
                                                   , _
                                                   , _
                                                   , _
                                                   sMotivoRetorno, _
                                                   , _
                                                   , _
                                                   , _
                                                   , _
                                                   , , LblValRemuneracao.Caption)
             
'daniel.menandro - Nova Consultoria - 14/07/2013
'16351367 - Extrato de Restitui��o
'Inicio
    
endosso_id = Obter_Endosso("I")

Call oFinanceiro.InserirExtratoRestituicao(gsSIGLASISTEMA, _
                                                    App.Title, _
                                                    App.FileDescription, _
                                                    glAmbiente_id, _
                                                    cUserName, _
                                                    Val(txtProposta.Text), _
                                                    LblValRestituicaoEstimado, _
                                                    endosso_id, _
                                                    0, _
                                                    "Recusa Gerencial", _
                                                    "Aguardando avalia��o t�cnica de restitui��o", _
                                                    sDescricaoMotivo)

    
    Set oFinanceiro = Nothing
    
    If bModular = True Then
    Set oFinanceiro = CreateObject("SEGL0026.cls00125")
    
    Dim propModular As Long
    Dim endossoModular As Long
    propModular = oDadosModular.proposta_id
    endossoModular = oDadosModular.EndossoId
    Call oFinanceiro.AtualizarAvaliacaoRestituicao(gsSIGLASISTEMA, _
                                                   App.Title, _
                                                   App.FileDescription, _
                                                   glAmbiente_id, _
                                                   cUserName, _
                                                   Data_Sistema, _
                                                   propModular, _
                                                   endossoModular, _
                                                   SITUACAO_RETORNO_TECNICA, _
                                                   , _
                                                   , _
                                                   , _
                                                   , _
                                                   , _
                                                   , _
                                                   , _
                                                   , _
                                                   , _
                                                   sMotivoRetorno, _
                                                   , _
                                                   , _
                                                   , _
                                                   , _
                                                   , , LblValRemuneracao.Caption)
    End If
             
    Set oFinanceiro = Nothing
    
    'Atualizando interface'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    
    MousePointer = vbDefault
    
    Exit Sub

TrataErro:
   Call TrataErroGeral("RetornarAvaliacaoRestituicao", Me.name)
   Call TerminaSEGBR
    
End Sub

Private Function ValidarTotalPremioInformado() As Boolean

  If Val(TrocaVirgulaPorPonto(TiraPonto(Mask2ValPremioTarifa.Text))) > gcValTotPremioTarifaPago Then
  
  
    Call MsgBox("O valor informado � maior que o pr�mio tarifa pago", vbCritical)
    Mask2ValPremioTarifa.SetFocus
    ValidarTotalPremioInformado = False
    
  Else
    ValidarTotalPremioInformado = True
    
  End If
  
End Function

Private Function ObterFlagRestituicaoIntegral(ByVal sProposta As String) As String

On Error GoTo TrataErro
  Dim sSQL As String
  Dim rsRestituicao As rdoResultset
   
  ObterFlagRestituicaoIntegral = "n"
   
  sSQL = ""
  sSQL = sSQL & "  SELECT restituicao_integral " & vbNewLine
  sSQL = sSQL & "    FROM seguros_db..avaliacao_restituicao_tb " & vbNewLine
  sSQL = sSQL & "   WHERE proposta_id = " & sProposta & vbNewLine
  If sProposta <> oDadosModular.proposta_id Then
    sSQL = sSQL & "     AND num_avaliacao = " & TxtEndossoBB.Text & vbNewLine
  Else
    sSQL = sSQL & "     AND num_avaliacao = " & oDadosModular.Num_Endosso_BB & vbNewLine
  End If
  
  Set rsRestituicao = rdocn.OpenResultset(sSQL)
  
  If Not rsRestituicao.EOF Then
  
    ObterFlagRestituicaoIntegral = LCase(Trim(rsRestituicao(0)))
    rsRestituicao.Close
  
  End If
  
  Set rsRestituicao = Nothing

Exit Function
TrataErro:
   Call TrataErroGeral("ObterFlagRestituicaoIntegral", Me.name)
   Call TerminaSEGBR
End Function

'mathayde 27/03/2009 - Obtem o percenteual de remunera��o para c�lculo de IR
    Function ObterPercRemuneracao(ByVal iProduto As Integer, _
                                  ByVal iRamo As Integer, _
                                  ByVal sDtReferencia As String) As Currency

        Dim eSQL As String
        Dim Rst As rdoResultset
        
        sDtReferencia = Format(sDtReferencia, "yyyymmdd")

        SQL = "SELECT perc_remuneracao_servico = isnull(perc_remuneracao_servico, 0)"
        SQL = SQL & " FROM seguros_db..custo_tb"
        SQL = SQL & " WHERE produto_id = " & iProduto
        SQL = SQL & " AND ramo_id = " & iRamo
'        SQL = SQL & " AND dt_inicio_vigencia <= '" & Format(sDtInicioVigencia, "yyyymmdd") & "'"
'        SQL = SQL & " AND (dt_fim_vigencia >= '" & Format(sDtFimVigencia, "yyyymmdd") & "' or dt_fim_vigencia is Null)"
        SQL = SQL & " and dt_inicio_vigencia <= '" & sDtReferencia & "'"
        SQL = SQL & " and (dt_fim_vigencia >= '" & sDtReferencia & "' or dt_fim_vigencia is Null)"
    
    
    
        Set Rst = rdocn.OpenResultset(SQL)
        
            '''VER -->>
            If Rst.EOF Then
                ObterPercRemuneracao = 0
            Else
                ObterPercRemuneracao = MudaPontoParaVirgula(Rst!perc_remuneracao_servico)
            End If
            
        Rst.Close
    
    End Function

Function MudaPontoParaVirgula(ByVal valor As String) As String

    If InStr(valor, ".") = 0 Then
       MudaPontoParaVirgula = valor
    Else
      valor = Mid$(valor, 1, InStr(valor, ".") - 1) + "," + Mid$(valor, InStr(valor, ".") + 1)
      MudaPontoParaVirgula = valor
    End If

End Function

Private Sub Mask2ValPremioTarifaModular_LostFocus(Index As Integer)
    If TpOperacao = 1 Then
        Call RecalcularRestituicaoModular(txtpropostaModular(Index).Text, Index)
    End If

End Sub

