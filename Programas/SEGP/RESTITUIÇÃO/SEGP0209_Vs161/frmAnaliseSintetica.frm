VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "mscomctl.OCX"
Begin VB.Form frmAnaliseSintetica 
   Caption         =   "Form1"
   ClientHeight    =   6495
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   9210
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   ScaleHeight     =   6495
   ScaleWidth      =   9210
   Begin VB.PictureBox Picture1 
      Height          =   735
      Left            =   120
      ScaleHeight     =   675
      ScaleWidth      =   8925
      TabIndex        =   5
      Top             =   5640
      Width           =   8985
      Begin VB.CommandButton btnDetalhe 
         Caption         =   "Detalhe"
         Height          =   375
         Left            =   1560
         TabIndex        =   9
         Top             =   120
         Width           =   1665
      End
      Begin VB.CommandButton btnAprovar 
         Caption         =   "Aprovar"
         Height          =   375
         Left            =   5280
         TabIndex        =   8
         Top             =   120
         Width           =   1665
      End
      Begin VB.CommandButton btnSair 
         Caption         =   "Sair"
         Height          =   375
         Left            =   7080
         TabIndex        =   7
         Top             =   120
         Width           =   1665
      End
      Begin VB.CommandButton btnExportar 
         Caption         =   "Exportar para o Excel"
         Height          =   375
         Left            =   3360
         TabIndex        =   6
         Top             =   120
         Width           =   1815
      End
      Begin MSComctlLib.ImageList ImageList1 
         Left            =   0
         Top             =   0
         _ExtentX        =   1005
         _ExtentY        =   1005
         BackColor       =   -2147483643
         ImageWidth      =   16
         ImageHeight     =   16
         MaskColor       =   12632256
         _Version        =   393216
         BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
            NumListImages   =   2
            BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmAnaliseSintetica.frx":0000
               Key             =   ""
            EndProperty
            BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmAnaliseSintetica.frx":031A
               Key             =   ""
            EndProperty
         EndProperty
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Dados do Produto"
      Height          =   975
      Left            =   120
      TabIndex        =   1
      Top             =   120
      Width           =   9015
      Begin VB.CheckBox chkSelecionar 
         Caption         =   "Selecionar todos"
         Height          =   255
         Left            =   120
         TabIndex        =   4
         Top             =   600
         Width           =   2175
      End
      Begin VB.Label lblProduto 
         AutoSize        =   -1  'True
         Caption         =   "Label2"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   960
         TabIndex        =   3
         Top             =   360
         Width           =   585
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Produto:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   120
         TabIndex        =   2
         Top             =   360
         Width           =   735
      End
   End
   Begin MSComctlLib.ListView ListSelEndosso 
      Height          =   4455
      Left            =   120
      TabIndex        =   0
      Top             =   1200
      Width           =   8985
      _ExtentX        =   15849
      _ExtentY        =   7858
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      Checkboxes      =   -1  'True
      _Version        =   393217
      SmallIcons      =   "ImageList1"
      ForeColor       =   -2147483646
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      NumItems        =   0
   End
End
Attribute VB_Name = "frmAnaliseSintetica"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public idProduto As Integer

Public sProduto  As String  'Alan Neves - C_ANEVES - MP: 17860335

Public inicial As Double
Public intervalo As Double
Public maiorQue As Double
Const strChecked = "�"
Const strUnChecked = "q"


Private Sub BtnAprovar_Click()
    
    Dim strWhere As String
    Dim selecionado As Boolean
    Dim strCampo As String
    Dim aprovas As Boolean
    selecionado = False
    strWhere = " AND ("
    Dim X As Long
    
    If TpOperacao = 1 Then
        strCampo = "val_restituicao_estimado"
    Else
        strCampo = "val_restituicao"
    End If
    
    For X = 1 To ListSelEndosso.ListItems.Count
        Set Item = ListSelEndosso.ListItems(X)
        If Item.Checked = True Then
             selecionado = True
             strWhere = strWhere & strCampo & " " & Item.SubItems(4) & " OR "
        End If
    Next
    If Not selecionado Then
        MsgBox "Selecione pelo menos uma distribui��o!", vbInformation
        Exit Sub
    End If
    strWhere = Left(strWhere, Len(strWhere) - 4)
    strWhere = strWhere & ")"
    If TpOperacao = 1 Then
         Call aprovacao_tecnica("", "", idProduto, strWhere)
         aprovado = True
    Else
         aprovado = aprovacao_gerencial("", "", idProduto, strWhere)
    End If
    If Not aprovado Then
        Exit Sub
    End If
    
    Call Unload(Me)
        
    frmAnaliseSintetica.idProduto = idProduto
    frmAnaliseSintetica.inicial = inicial
    frmAnaliseSintetica.intervalo = intervalo
    frmAnaliseSintetica.maiorQue = maiorQue

    frmAnaliseSintetica.sProduto = sProduto 'Alan Neves - C_ANEVES - Demanda: 17860335
    
    'Carregando o combo do outro form
    Call frmAnaliseSintetica.Carrega_Form

    frmAnaliseSintetica.Show
    

    If ListSelEndosso.ListItems.Count = 0 Then
        Unload Me
    End If
        
    
End Sub

Private Sub btnDetalhe_Click()
    
    Dim strWhere As String
    Dim strCampo As String
    Dim selecionado As Boolean
    
    If TpOperacao = 1 Then
        strCampo = "val_restituicao_estimado"
    Else
        strCampo = "val_restituicao"
    End If
    strWhere = " AND ("
    Dim X As Long
    For X = 1 To ListSelEndosso.ListItems.Count
        Set Item = ListSelEndosso.ListItems(X)
        If Item.Checked = True Then
             selecionado = True
             strWhere = strWhere & strCampo & " " & Item.SubItems(4) & " OR "
        End If
    Next
    If Not selecionado Then
        MsgBox "Selecione pelo menos uma distribui��o!", vbInformation
        Exit Sub
    End If
    strWhere = Left(strWhere, Len(strWhere) - 4)
    strWhere = strWhere & ")"
    frmAnalitico.idProduto = idProduto
    frmAnalitico.inicial = inicial
    frmAnalitico.intervalo = intervalo
    frmAnalitico.maiorQue = maiorQue
    frmAnalitico.strWhere = strWhere
    frmAnalitico.QTD = 0

    frmAnalitico.sProduto = sProduto 'Alan Neves - C_ANEVES - Demanda: 17860335 - MP - 28/05/2013
    
    Call frmAnalitico.carregarForm
    frmAnalitico.Show
    Call Unload(Me)
    
    
    
       
End Sub

'Alan Neves - C_ANEVES - Demanda: 17860335 - 03/06/2013 - Inicio
'Private Sub btnExportar_Click()
'
'
'     Dim sSQL As String
'    Dim strWhere As String
'
'
'    Dim selecionado As Boolean
'    selecionado = False
'    strWhere = " AND ("
'    Dim X As Long
'    For X = 1 To ListSelEndosso.ListItems.Count
'        Set Item = ListSelEndosso.ListItems(X)
'        If Item.Checked = True Then
'             selecionado = True
'             strWhere = strWhere & "val_restituicao_estimado " & Item.SubItems(4) & " OR "
'        End If
'    Next
'    If Not selecionado Then
'        MsgBox "Selecione pelo menos uma distribui��o!", vbInformation
'        Exit Sub
'    End If
'    strWhere = Left(strWhere, Len(strWhere) - 4)
'    strWhere = strWhere & ")"
'
'    FrmEspera.Label1 = "Aguarde, Exportando para o Excel..."
'    FrmEspera.Show
'
'
'    Call AtualizaDadosRestituicao(0, idProduto)
'
'
'
'    sSQL = "select * from #avaliacao_restituicao_tb where produto_id = " & CStr(idProduto) & strWhere
'    Dim rc As rdoResultset
'    Set rc = rdocn.OpenResultset(sSQL)
'
'
'    Dim xlApp As Object
'    Dim xlWb As Object
'    Dim xlWs As Object
'
'    Set xlApp = CreateObject("Excel.Application")
'    Set xlWb = xlApp.Workbooks.Add
'    Set xlWs = xlWb.Worksheets(1)
'
'    xlApp.Visible = False
'    xlApp.UserControl = True
'
'    Dim i As Long
'    i = 1
'
'    xlWs.Cells(i, 1).Value = "Proposta BB"
'    xlWs.Cells(i, 1).ColumnWidth = 15
'    xlWs.Cells(i, 2).Value = "Proposta AB"
'    xlWs.Cells(i, 2).ColumnWidth = 15
'    xlWs.Cells(i, 3).Value = "Valor de Restitui��o"
'    xlWs.Cells(i, 3).ColumnWidth = 20
'    xlWs.Cells(i, 4).Value = "Nr. Avalia��o"
'    xlWs.Cells(i, 4).ColumnWidth = 15
'    xlWs.Cells(i, 5).Value = "Dt. Pedido da Avalia��o"
'    xlWs.Cells(i, 5).ColumnWidth = 23
'    xlWs.Cells(i, 6).Value = "Produto"
'    xlWs.Cells(i, 6).ColumnWidth = 15
'    xlWs.Cells(i, 7).Value = "Cliente"
'    xlWs.Cells(i, 7).ColumnWidth = 35
'    xlWs.Cells(i, 8).Value = "Dt. Contrata��o"
'    xlWs.Cells(i, 8).ColumnWidth = 15
'    xlWs.Cells(i, 9).Value = "Tipo de Avalia��o"
'    xlWs.Cells(i, 9).ColumnWidth = 40
'    xlWs.Cells(i, 10).Value = "Valor Subven��o Federal"
'    xlWs.Cells(i, 10).ColumnWidth = 25
'    xlWs.Cells(i, 11).Value = "Valor Subven��o Estadual"
'    xlWs.Cells(i, 11).ColumnWidth = 25
'    xlWs.Cells(i, 12).Value = "In�cio de Vig�ncia"
'    xlWs.Cells(i, 12).ColumnWidth = 18
'    xlWs.Cells(i, 13).Value = "Fim de Vig�ncia"
'    xlWs.Cells(i, 13).ColumnWidth = 15
'    xlWs.Cells(i, 14).Value = "IS (Import�ncia Segurada)"
'    xlWs.Cells(i, 14).ColumnWidth = 27
'    xlWs.Cells(i, 15).Value = "Valor Pr�mio Pago"
'    xlWs.Cells(i, 15).ColumnWidth = 20
'    xlWs.Cells(i, 16).Value = "Valor Pr�mio Retido"
'    xlWs.Cells(i, 16).ColumnWidth = 20
'    xlWs.Cells(i, 17).Value = "Data de Emiss�o do Endosso"
'    xlWs.Cells(i, 17).ColumnWidth = 30
'
'    xlWs.Cells(i, 18).Value = "Valor Comiss�o"
'    xlWs.Cells(i, 18).ColumnWidth = 15
'    xlWs.Cells(i, 19).Value = "Valor Remunera��o"
'    xlWs.Cells(i, 19).ColumnWidth = 18
'
'    Dim j As Long
'
'    For j = 1 To 19
'        xlWs.Cells(i, j).Font.Bold = True
'    Next
'
'
'   'Dim Item As ListItem
'    Dim Y As Long
'
'
'    Do While Not rc.EOF
'        i = i + 1
'        xlWs.Cells(i, 1).Value = rc("proposta_bb")
'        xlWs.Cells(i, 2).Value = rc("proposta_id")
'        xlWs.Cells(i, 3).Value = IIf(Not IsNull(rc("val_restituicao_estimado")), CDbl(Replace(rc("val_restituicao_estimado"), ".", ",")), 0)
'        xlWs.Cells(i, 3).NumberFormat = "#,##0.00"
'
'        xlWs.Cells(i, 4).Value = rc("num_avaliacao")
'
'        If IsDate(rc("dt_pedido_avaliacao")) Then
'            xlWs.Cells(i, 5).Value = CDate(rc("dt_pedido_avaliacao"))
'        Else
'            xlWs.Cells(i, 5).Value = ""
'        End If
'        xlWs.Cells(i, 6).Value = rc("produto_id")
'        xlWs.Cells(i, 7).Value = rc("Cliente")
'
'        If IsDate(rc("DT_Contratacao")) Then
'            xlWs.Cells(i, 8).Value = CDate(rc("DT_Contratacao"))
'        Else
'            xlWs.Cells(i, 8).Value = ""
'        End If
'
'        xlWs.Cells(i, 9).Value = rc("TipoAvaliacao")
'
'        If Not IsNull(rc("val_subvencao_federal_estimado")) Then
'            xlWs.Cells(i, 10).Value = CDbl(Replace(rc("val_subvencao_federal_estimado"), ".", ","))
'        Else
'            xlWs.Cells(i, 10).Value = 0
'        End If
'        xlWs.Cells(i, 10).NumberFormat = "#,##0.00"
'
'        If Not IsNull(rc("val_subvencao_estadual_estimado")) Then
'            xlWs.Cells(i, 11).Value = CDbl(Replace(rc("val_subvencao_estadual_estimado"), ".", ","))
'        Else
'            xlWs.Cells(i, 11).Value = 0
'        End If
'        xlWs.Cells(i, 11).NumberFormat = "#,##0.00"
'
'        If IsDate(rc("dt_inicio_vigencia")) Then
'            xlWs.Cells(i, 12).Value = CDate(rc("dt_inicio_vigencia"))
'        Else
'            xlWs.Cells(i, 12).Value = ""
'        End If
'
'        If IsDate(rc("dt_fim_vigencia")) Then
'            xlWs.Cells(i, 13).Value = CDate(rc("dt_fim_vigencia"))
'        Else
'            xlWs.Cells(i, 13).Value = ""
'        End If
'
'        If Not IsNull(rc("valis")) Then
'            xlWs.Cells(i, 14).Value = CDbl(Replace(rc("ValIS"), ".", ","))
'        Else
'            xlWs.Cells(i, 14).Value = 0
'        End If
'        xlWs.Cells(i, 14).NumberFormat = "#,##0.00"
'
'        If Not IsNull(rc("PremioPago")) Then
'            xlWs.Cells(i, 15).Value = CDbl(Replace(rc("PremioPago"), ".", ","))
'        Else
'            xlWs.Cells(i, 15).Value = 0
'        End If
'        xlWs.Cells(i, 15).NumberFormat = "#,##0.00"
'
'        If Not IsNull(rc("PremioRetido")) Then
'            xlWs.Cells(i, 16).Value = CDbl(Replace(rc("PremioRetido"), ".", ","))
'        Else
'            xlWs.Cells(i, 16).Value = 0
'        End If
'        xlWs.Cells(i, 16).NumberFormat = "#,##0.00"
'
'
'        If IsDate(rc("dt_emissao")) Then
'            xlWs.Cells(i, 17).Value = CDate(rc("dt_emissao"))
'        Else
'            xlWs.Cells(i, 17).Value = ""
'        End If
'
'
'        If Not IsNull(rc("Val_comissao_estimado")) Then
'            xlWs.Cells(i, 18).Value = CDbl(Replace(rc("Val_comissao_estimado"), ".", ","))
'        Else
'            xlWs.Cells(i, 18).Value = 0
'        End If
'
'        xlWs.Cells(i, 18).NumberFormat = "#,##0.00"
'        If Not IsNull(rc("val_restituicao_estimado")) Then
'            xlWs.Cells(i, 19).Value = CDbl(Replace(rc("val_restituicao_estimado"), ".", ","))
'        Else
'            xlWs.Cells(i, 19).Value = 0
'        End If
'        xlWs.Cells(i, 19).NumberFormat = "#,##0.00"
'
'
'
'        rc.MoveNext
'    Loop
'
'    Call Unload(FrmEspera)
'    xlApp.Visible = True
'
'
'End Sub



Private Sub btnExportar_Click()
    
    
     Dim sSQL As String
    Dim strWhere As String
    
    
    Dim selecionado As Boolean
    selecionado = False
    strWhere = " AND ("
    Dim X As Long
    For X = 1 To ListSelEndosso.ListItems.Count
        Set Item = ListSelEndosso.ListItems(X)
        If Item.Checked = True Then
             selecionado = True
             strWhere = strWhere & "val_restituicao_estimado " & Item.SubItems(4) & " OR "
        End If
    Next
    If Not selecionado Then
        MsgBox "Selecione pelo menos uma distribui��o!", vbInformation
        Exit Sub
    End If
    strWhere = Left(strWhere, Len(strWhere) - 4)
    strWhere = strWhere & ")"
    
    FrmEspera.Label1 = "Aguarde, Exportando para o Excel..."
    FrmEspera.Show

    sSQL = "select proposta_bb, proposta_id, val_restituicao_estimado, num_avaliacao, dt_pedido_avaliacao, produto_id, Cliente, DT_Contratacao, " & vbNewLine
    sSQL = sSQL & "TipoAvaliacao, val_subvencao_federal_estimado, val_subvencao_estadual_estimado, dt_inicio_vigencia, dt_fim_vigencia, valis, PremioPago, PremioRetido, " & vbNewLine
    sSQL = sSQL & "dt_emissao, Val_comissao_estimado, val_restituicao_estimado from #avaliacao_restituicao_tb where produto_id = " & CStr(idProduto) & strWhere
    
    Dim rc As rdoResultset

    Set rc = AtualizaDadosRestituicao(0, idProduto, sSQL)
    
    Dim xlApp As Object
    Dim xlWb As Object
    Dim xlWs As Object
    
    Set xlApp = CreateObject("Excel.Application")
    Set xlWb = xlApp.Workbooks.Add
    Set xlWs = xlWb.Worksheets(1)
    
    xlApp.Visible = False
    xlApp.UserControl = True
    
    Dim i As Long
    i = 1
    
    xlWs.Cells(i, 1).Value = "Proposta BB"
    xlWs.Cells(i, 1).ColumnWidth = 15
    xlWs.Cells(i, 2).Value = "Proposta AB"
    xlWs.Cells(i, 2).ColumnWidth = 15
    xlWs.Cells(i, 3).Value = "Valor de Restitui��o"
    xlWs.Cells(i, 3).ColumnWidth = 20
    xlWs.Cells(i, 4).Value = "Nr. Avalia��o"
    xlWs.Cells(i, 4).ColumnWidth = 15
    xlWs.Cells(i, 5).Value = "Dt. Pedido da Avalia��o"
    xlWs.Cells(i, 5).ColumnWidth = 23
    xlWs.Cells(i, 6).Value = "Produto"
    xlWs.Cells(i, 6).ColumnWidth = 15
    xlWs.Cells(i, 7).Value = "Cliente"
    xlWs.Cells(i, 7).ColumnWidth = 35
    xlWs.Cells(i, 8).Value = "Dt. Contrata��o"
    xlWs.Cells(i, 8).ColumnWidth = 15
    xlWs.Cells(i, 9).Value = "Tipo de Avalia��o"
    xlWs.Cells(i, 9).ColumnWidth = 40
    xlWs.Cells(i, 10).Value = "Valor Subven��o Federal"
    xlWs.Cells(i, 10).ColumnWidth = 25
    xlWs.Cells(i, 11).Value = "Valor Subven��o Estadual"
    xlWs.Cells(i, 11).ColumnWidth = 25
    xlWs.Cells(i, 12).Value = "In�cio de Vig�ncia"
    xlWs.Cells(i, 12).ColumnWidth = 18
    xlWs.Cells(i, 13).Value = "Fim de Vig�ncia"
    xlWs.Cells(i, 13).ColumnWidth = 15
    xlWs.Cells(i, 14).Value = "IS (Import�ncia Segurada)"
    xlWs.Cells(i, 14).ColumnWidth = 27
    xlWs.Cells(i, 15).Value = "Valor Pr�mio Pago"
    xlWs.Cells(i, 15).ColumnWidth = 20
    xlWs.Cells(i, 16).Value = "Valor Pr�mio Retido"
    xlWs.Cells(i, 16).ColumnWidth = 20
    xlWs.Cells(i, 17).Value = "Data de Emiss�o do Endosso"
    xlWs.Cells(i, 17).ColumnWidth = 30
 
    xlWs.Cells(i, 18).Value = "Valor Comiss�o"
    xlWs.Cells(i, 18).ColumnWidth = 15
    xlWs.Cells(i, 19).Value = "Valor Remunera��o"
    xlWs.Cells(i, 19).ColumnWidth = 18
    
    Dim j As Long
    
    For j = 1 To 19
        xlWs.Cells(i, j).Font.Bold = True
    Next
    
    
   'Dim Item As ListItem
    Dim Y As Long
    
    
    Do While Not rc.EOF
        i = i + 1
        xlWs.Cells(i, 1).Value = rc("proposta_bb")
        xlWs.Cells(i, 2).Value = rc("proposta_id")
        
        'INC000005070679 - Leandro Amaral 08/08/2016 '
        If Not IsNull(rc("val_restituicao_estimado")) Then
            xlWs.Cells(i, 3).Value = IIf(Not IsNull(rc("val_restituicao_estimado")), CDbl(Replace(rc("val_restituicao_estimado"), ".", ",")), 0)
        Else
            xlWs.Cells(i, 3).Value = 0
        End If
        xlWs.Cells(i, 3).NumberFormat = "#,##0.00"
        'INC000005070679 - Leandro Amaral 08/08/2016 '
        
        xlWs.Cells(i, 4).Value = rc("num_avaliacao")
        
        If IsDate(rc("dt_pedido_avaliacao")) Then
            xlWs.Cells(i, 5).Value = CDate(rc("dt_pedido_avaliacao"))
        Else
            xlWs.Cells(i, 5).Value = ""
        End If
        xlWs.Cells(i, 6).Value = rc("produto_id")
        xlWs.Cells(i, 7).Value = rc("Cliente")
        
        If IsDate(rc("DT_Contratacao")) Then
            xlWs.Cells(i, 8).Value = CDate(rc("DT_Contratacao"))
        Else
            xlWs.Cells(i, 8).Value = ""
        End If
        
        xlWs.Cells(i, 9).Value = rc("TipoAvaliacao")
        
        If Not IsNull(rc("val_subvencao_federal_estimado")) Then
            xlWs.Cells(i, 10).Value = CDbl(Replace(rc("val_subvencao_federal_estimado"), ".", ","))
        Else
            xlWs.Cells(i, 10).Value = 0
        End If
        xlWs.Cells(i, 10).NumberFormat = "#,##0.00"
        
        If Not IsNull(rc("val_subvencao_estadual_estimado")) Then
            xlWs.Cells(i, 11).Value = CDbl(Replace(rc("val_subvencao_estadual_estimado"), ".", ","))
        Else
            xlWs.Cells(i, 11).Value = 0
        End If
        xlWs.Cells(i, 11).NumberFormat = "#,##0.00"
        
        If IsDate(rc("dt_inicio_vigencia")) Then
            xlWs.Cells(i, 12).Value = CDate(rc("dt_inicio_vigencia"))
        Else
            xlWs.Cells(i, 12).Value = ""
        End If
        
        If IsDate(rc("dt_fim_vigencia")) Then
            xlWs.Cells(i, 13).Value = CDate(rc("dt_fim_vigencia"))
        Else
            xlWs.Cells(i, 13).Value = ""
        End If
        
        If Not IsNull(rc("valis")) Then
            xlWs.Cells(i, 14).Value = CDbl(Replace(rc("ValIS"), ".", ","))
        Else
            xlWs.Cells(i, 14).Value = 0
        End If
        xlWs.Cells(i, 14).NumberFormat = "#,##0.00"
        
        If Not IsNull(rc("PremioPago")) Then
            xlWs.Cells(i, 15).Value = CDbl(Replace(rc("PremioPago"), ".", ","))
        Else
            xlWs.Cells(i, 15).Value = 0
        End If
        xlWs.Cells(i, 15).NumberFormat = "#,##0.00"
        
        If Not IsNull(rc("PremioRetido")) Then
            xlWs.Cells(i, 16).Value = CDbl(Replace(rc("PremioRetido"), ".", ","))
        Else
            xlWs.Cells(i, 16).Value = 0
        End If
        xlWs.Cells(i, 16).NumberFormat = "#,##0.00"
       
        
        If IsDate(rc("dt_emissao")) Then
            xlWs.Cells(i, 17).Value = CDate(rc("dt_emissao"))
        Else
            xlWs.Cells(i, 17).Value = ""
        End If
        
                
        If Not IsNull(rc("Val_comissao_estimado")) Then
            xlWs.Cells(i, 18).Value = CDbl(Replace(rc("Val_comissao_estimado"), ".", ","))
        Else
            xlWs.Cells(i, 18).Value = 0
        End If
        
        xlWs.Cells(i, 18).NumberFormat = "#,##0.00"
        If Not IsNull(rc("val_restituicao_estimado")) Then
            xlWs.Cells(i, 19).Value = CDbl(Replace(rc("val_restituicao_estimado"), ".", ","))
        Else
            xlWs.Cells(i, 19).Value = 0
        End If
        xlWs.Cells(i, 19).NumberFormat = "#,##0.00"
        
        rc.MoveNext
    Loop
    
    
    Call Unload(FrmEspera)
    xlApp.Visible = True
    
End Sub
'Alan Neves - C_ANEVES - Demanda: 17860335 - 03/06/2013 - Fim
   

Private Sub BtnSair_Click()
    frmFiltroPesquisa.Show
    Call Unload(Me)
End Sub

Private Sub chkSelecionar_Click()
   
    Dim i As Long
    Dim Item As ListItem
    
    For i = 1 To ListSelEndosso.ListItems.Count
        If chkSelecionar.Value = 1 Then
            Set Item = ListSelEndosso.ListItems(i)
            Item.Checked = True
        Else
            Set Item = ListSelEndosso.ListItems(i)
            Item.Checked = False
        End If
    Next
    
    
    
    
    
End Sub


'Renato Vasconcelos
'Flow 11022639
Private Sub Form_Load()
   
    Call CentraFrm(Me)
    
    
   
        
End Sub

Public Sub Carrega_Form()
    Dim sSQL As String
    If TpOperacao = 1 Then
        Me.Caption = "SEGP0209 - Avalia��o T�cnica de Restitui��o - " & Ambiente
        btnDetalhe.Left = 1560
    Else
        Me.Caption = "SEGP0209 - Avalia��o Gerencial de Restitui��o - " & Ambiente
        btnExportar.Visible = False
        btnDetalhe.Left = 3480
    End If
    
    'Recuperando produto
    If sProduto = "" Then     'Alan Neves - C_ANEVES - Demanda: 17860335 - 28/05/2013 - Recupera caso n�o exista o nome do produto
        sSQL = "select top 1 produto_id, produto from #avaliacao_restituicao_tb where produto_id = " & CStr(idProduto)
    Dim rcProduto As rdoResultset
    Set rcProduto = rdocn.OpenResultset(sSQL)
    
    If Not rcProduto.EOF Then
        lblProduto.Caption = rcProduto("produto_id") & " - " & rcProduto("produto")
    End If
    
    rcProduto.Close
    Set rcProduto = Nothing

    Else 'Alan Neves - C_ANEVES - Demanda: 17860335 - 28/05/2013 - Recupera caso n�o exista o nome do produto
        lblProduto.Caption = CStr(idProduto) & " - " & sProduto 'Alan Neves - C_ANEVES - Demanda: 17860335 - 28/05/2013 - Recupera caso n�o exista o nome do produto
    End If 'Alan Neves - C_ANEVES - Demanda: 17860335 - 28/05/2013 - Recupera caso n�o exista o nome do produto
     
    Dim Item As ListItem
   
    Dim rc As rdoResultset
    
    FrmEspera.Label1.Caption = "Carregando Analise Sint�tica"
    FrmEspera.Show

    ListSelEndosso.Checkboxes = True

    ListSelEndosso.Visible = False
    ListSelEndosso.View = lvwReport
    ListSelEndosso.FullRowSelect = True
    ListSelEndosso.LabelEdit = lvwManual
    ListSelEndosso.HotTracking = True


    ListSelEndosso.ColumnHeaders.Clear

    ListSelEndosso.ColumnHeaders.Add , , "Selecionar", 600
    ListSelEndosso.ColumnHeaders.Add , , "Distribui��o", 4000
    ListSelEndosso.ColumnHeaders.Add , , "Qtd", 1200, lvwColumnRight
    ListSelEndosso.ColumnHeaders.Add , , "Val Total", 2000, lvwColumnRight
    ListSelEndosso.ColumnHeaders.Add , , "Regra", 0

    sSQL = "set nocount on " & vbNewLine
    '--Declarando vari�veis
        sSQL = sSQL & "Declare @valorInicial numeric(15,2)" & vbNewLine
        sSQL = sSQL & "Declare @intervalo    numeric(15,2)" & vbNewLine
        sSQL = sSQL & "Declare @final        decimal(15,2)" & vbNewLine
        sSQL = sSQL & "Declare @produto_id   int" & vbNewLine
        '--Setando valores
        sSQL = sSQL & "SET @valorInicial = " & Replace(CStr(TruncaDecimal(inicial, 2)), ",", ".") & vbNewLine
        sSQL = sSQL & "SET @intervalo    = " & Replace(CStr(TruncaDecimal(intervalo, 2)), ",", ".") & vbNewLine
        sSQL = sSQL & "SET @final        = " & Replace(CStr(TruncaDecimal(maiorQue, 2)), ",", ".") & vbNewLine
        sSQL = sSQL & "Set @produto_id   = " & CStr(idProduto) & vbNewLine
               
        '--Criando tabelas tempor�rias
        sSQL = sSQL & "create table #sintetica" & vbNewLine
        sSQL = sSQL & "("
        sSQL = sSQL & "    descricao varchar(30)," & vbNewLine
        sSQL = sSQL & "    quantidade int," & vbNewLine
        sSQL = sSQL & "    total numeric(15,2)," & vbNewLine
        sSQL = sSQL & "    regra VarChar(30)" & vbNewLine
        sSQL = sSQL & ")" & vbNewLine
        
        '--Criando vari�vel auxiliar
        sSQL = sSQL & "declare @contAux numeric(15,2)" & vbNewLine
        sSQL = sSQL & "SET @contAux = 0" & vbNewLine
        sSQL = sSQL & "IF @valorInicial >= @intervalo" & vbNewLine
        sSQL = sSQL & "    SET @contAux = @valorInicial" & vbNewLine
        
        
    If TpOperacao = 1 Then ' t�cnico
             
        '--inserindo valores menores que o inicial
        sSQL = sSQL & "insert into #sintetica(descricao, quantidade, total, regra)" & vbNewLine
        sSQL = sSQL & "select" & vbNewLine
        sSQL = sSQL & "    'Valor Restitui��o < ' + convert(varchar(20),@valorInicial), count(1), sum(val_restituicao_estimado), '< ' + convert(varchar(20),@valorInicial)" & vbNewLine
        sSQL = sSQL & "    from #avaliacao_restituicao_tb" & vbNewLine
        sSQL = sSQL & "Where" & vbNewLine
        sSQL = sSQL & "    produto_id = @produto_id" & vbNewLine
        sSQL = sSQL & "    and val_restituicao_estimado < @valorInicial" & vbNewLine
      
        'FLAVIO.BEZERRA - 06/06/2013
        sSQL = sSQL & "    and cor <> 'A'"
      
        '--inserindo valores do intervalo
        sSQL = sSQL & "While @contAux < @final" & vbNewLine
        sSQL = sSQL & "BEGIN" & vbNewLine
        sSQL = sSQL & "    SET @contAux = @contAux + @intervalo" & vbNewLine
        sSQL = sSQL & "    if @contAux > @final" & vbNewLine
        sSQL = sSQL & "        SET @contAux = @final" & vbNewLine
        sSQL = sSQL & "    insert into #sintetica(descricao, quantidade, total, regra)" & vbNewLine
        sSQL = sSQL & "    select" & vbNewLine
        sSQL = sSQL & "        convert(varchar(20), @valorInicial) + ' a ' + convert(varchar(20),@contAux), count(1), sum(val_restituicao_estimado), 'between ' + convert(varchar(20), @valorInicial) + ' and ' + convert(varchar(20),@contAux)" & vbNewLine
        sSQL = sSQL & "        from #avaliacao_restituicao_tb" & vbNewLine
        sSQL = sSQL & "    Where" & vbNewLine
        sSQL = sSQL & "        produto_id = @produto_id" & vbNewLine
        sSQL = sSQL & "        and val_restituicao_estimado between @valorInicial and @contAux" & vbNewLine
        
        'FLAVIO.BEZERRA - 06/06/2013
        sSQL = sSQL & "        and cor <> 'A'"
        
        sSQL = sSQL & "    SET @valorInicial = @contAux + 0.01" & vbNewLine
        sSQL = sSQL & "    --SET @contAux = @contAux" & vbNewLine
        sSQL = sSQL & "End" & vbNewLine
        '--inserindo valores maiores que o final
        sSQL = sSQL & "insert into #sintetica(descricao, quantidade, total, regra)" & vbNewLine
        sSQL = sSQL & "select" & vbNewLine
        sSQL = sSQL & "    'Valor Restitui��o > ' + convert(varchar(20), @final), count(1), sum(val_restituicao_estimado), ' > ' + convert(varchar(20), @final)" & vbNewLine
        sSQL = sSQL & "From" & vbNewLine
        sSQL = sSQL & "    #avaliacao_restituicao_tb" & vbNewLine
        sSQL = sSQL & "Where" & vbNewLine
        sSQL = sSQL & "    produto_id = @produto_id" & vbNewLine
        sSQL = sSQL & "    and val_restituicao_estimado >= @final" & vbNewLine
        
        'FLAVIO.BEZERRA - 06/06/2013
        sSQL = sSQL & "    and cor <> 'A'"
        
    Else 'Gerencial
        '--inserindo valores menores que o inicial
        sSQL = sSQL & "insert into #sintetica(descricao, quantidade, total, regra)" & vbNewLine
        sSQL = sSQL & "select" & vbNewLine
        sSQL = sSQL & "    'Valor Restitui��o < ' + convert(varchar(20),@valorInicial), count(1), sum(val_restituicao), '< ' + convert(varchar(20),@valorInicial)" & vbNewLine
        sSQL = sSQL & "    from #avaliacao_restituicao_tb" & vbNewLine
        sSQL = sSQL & "Where" & vbNewLine
        sSQL = sSQL & "    produto_id = @produto_id" & vbNewLine
        sSQL = sSQL & "    and val_restituicao < @valorInicial" & vbNewLine
      
        'FLAVIO.BEZERRA - 06/06/2013
        sSQL = sSQL & "    and cor <> 'A'"
      
        '--inserindo valores do intervalo
        sSQL = sSQL & "While @contAux < @final" & vbNewLine
        sSQL = sSQL & "BEGIN" & vbNewLine
        sSQL = sSQL & "    SET @contAux = @contAux + @intervalo" & vbNewLine
        sSQL = sSQL & "    if @contAux > @final" & vbNewLine
        sSQL = sSQL & "        SET @contAux = @final" & vbNewLine
        sSQL = sSQL & "    insert into #sintetica(descricao, quantidade, total, regra)" & vbNewLine
        sSQL = sSQL & "    select" & vbNewLine
        sSQL = sSQL & "        convert(varchar(20), @valorInicial) + ' a ' + convert(varchar(20),@contAux), count(1), sum(val_restituicao), 'between ' + convert(varchar(20), @valorInicial) + ' and ' + convert(varchar(20),@contAux)" & vbNewLine
        sSQL = sSQL & "        from #avaliacao_restituicao_tb" & vbNewLine
        sSQL = sSQL & "    Where" & vbNewLine
        sSQL = sSQL & "        produto_id = @produto_id" & vbNewLine
        sSQL = sSQL & "        and val_restituicao between @valorInicial and @contAux" & vbNewLine
        
        'FLAVIO.BEZERRA - 06/06/2013
        sSQL = sSQL & "    and cor <> 'A'"
        
        sSQL = sSQL & "    SET @valorInicial = @contAux + 0.01" & vbNewLine
        sSQL = sSQL & "    --SET @contAux = @contAux" & vbNewLine
        sSQL = sSQL & "End" & vbNewLine
        '--inserindo valores maiores que o final
        sSQL = sSQL & "insert into #sintetica(descricao, quantidade, total, regra)" & vbNewLine
        sSQL = sSQL & "select" & vbNewLine
        sSQL = sSQL & "    'Valor Restitui��o > ' + convert(varchar(20), @final), count(1), sum(val_restituicao), ' > ' + convert(varchar(20), @final)" & vbNewLine
        sSQL = sSQL & "From" & vbNewLine
        sSQL = sSQL & "    #avaliacao_restituicao_tb" & vbNewLine
        sSQL = sSQL & "Where" & vbNewLine
        sSQL = sSQL & "    produto_id = @produto_id" & vbNewLine
        sSQL = sSQL & "    and val_restituicao >= @final" & vbNewLine
        
        'FLAVIO.BEZERRA - 06/06/2013
        sSQL = sSQL & "    and cor <> 'A'"
        
    End If
    
      '--retornando valores
        sSQL = sSQL & "select descricao, quantidade, total, regra from #sintetica where total is not null" & vbNewLine
        '--matando tabela tempor�ria
        sSQL = sSQL & "drop table #sintetica" & vbNewLine
    Set rc = rdocn.OpenResultset(sSQL)
    
    Dim totalQtd As Double
    Dim totalValor As Double
    totalQtd = 0
    totalValor = 0
    
            
    If rc.EOF Then
        rc.Close
        Set rc = Nothing
        Unload Me
        frmFiltroPesquisa.Show
        Exit Sub
    End If
    
    
    Do While Not rc.EOF
        Set Item = ListSelEndosso.ListItems.Add(, , "", , 1)
        Item.SubItems(1) = rc("descricao")
        Item.SubItems(2) = rc("quantidade")
        Item.SubItems(3) = Format(Replace(rc("total"), ".", ","), "#,##0.00")
        Item.SubItems(4) = rc("regra")
        totalQtd = totalQtd + CDbl(rc("quantidade"))
        totalValor = totalValor + CDbl(Format(Replace(rc("total"), ".", ","), "#,##0.00"))
        rc.MoveNext
        
    Loop
    
    rc.Close
    Set rc = Nothing
    
    Call Unload(FrmEspera)
    ListSelEndosso.Visible = True
        
End Sub

Private Sub TriggerCheckbox(iRow As Long, iCol As Integer)
        With msfSintetico
            If .TextMatrix(iRow, iCol) = strUnChecked Then
                .TextMatrix(iRow, iCol) = strChecked
            Else
                .TextMatrix(iRow, iCol) = strUnChecked
            End If
        End With
End Sub


Private Sub msfSintetico_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Or KeyAscii = 32 Then 'Enter/Space
        With msfSintetico
            Call TriggerCheckbox(.Row, .Col)
        End With
    End If
End Sub

Private Sub msfSintetico_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
    If Button = 1 Then
        With msfSintetico
            If .MouseRow <> 0 And .MouseCol <> 0 Then
                Call TriggerCheckbox(.MouseRow, .MouseCol)
            End If
        End With
    End If
End Sub
