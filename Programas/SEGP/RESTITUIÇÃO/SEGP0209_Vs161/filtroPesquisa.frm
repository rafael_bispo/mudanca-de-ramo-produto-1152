VERSION 5.00
Begin VB.Form frmFiltroPesquisa 
   Caption         =   "SEGP0209 - Filtro"
   ClientHeight    =   2940
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   11025
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   ScaleHeight     =   2940
   ScaleWidth      =   11025
   Begin VB.Frame Frame1 
      Caption         =   "Tipo de Pesquisa"
      Height          =   2055
      Left            =   120
      TabIndex        =   12
      Top             =   840
      Width           =   10875
      Begin VB.CommandButton BtnModo 
         Caption         =   "Modo"
         Height          =   795
         Left            =   5640
         Picture         =   "filtroPesquisa.frx":0000
         Style           =   1  'Graphical
         TabIndex        =   20
         ToolTipText     =   "Alterar Modo de Trabalho"
         Top             =   960
         Width           =   885
      End
      Begin VB.Frame Frame3 
         Caption         =   "Intervalo de Valores"
         Height          =   735
         Left            =   1440
         TabIndex        =   15
         Top             =   120
         Width           =   5055
         Begin VB.TextBox txtFinal 
            Height          =   285
            Left            =   4320
            TabIndex        =   4
            Top             =   240
            Width           =   615
         End
         Begin VB.TextBox txtInicial 
            Height          =   285
            Left            =   1200
            TabIndex        =   2
            Top             =   240
            Width           =   615
         End
         Begin VB.TextBox txtIntervalo 
            Height          =   285
            Left            =   2760
            TabIndex        =   3
            Top             =   240
            Width           =   615
         End
         Begin VB.Label Label5 
            Caption         =   "Maior que:"
            Height          =   255
            Left            =   3480
            TabIndex        =   18
            Top             =   240
            Width           =   855
         End
         Begin VB.Label Label2 
            AutoSize        =   -1  'True
            Caption         =   "Valor Inicial:"
            Height          =   195
            Left            =   240
            TabIndex        =   17
            Top             =   240
            Width           =   855
         End
         Begin VB.Label Label4 
            AutoSize        =   -1  'True
            Caption         =   "Intervalo:"
            Height          =   195
            Left            =   1920
            TabIndex        =   16
            Top             =   240
            Width           =   660
         End
      End
      Begin VB.CommandButton btnSair 
         Caption         =   "&Sair"
         Height          =   375
         Left            =   8640
         TabIndex        =   9
         Top             =   1440
         Width           =   735
      End
      Begin VB.CommandButton btnOK 
         Caption         =   "&OK"
         Height          =   375
         Left            =   9480
         TabIndex        =   10
         Top             =   1440
         Width           =   735
      End
      Begin VB.OptionButton optSintetico 
         Caption         =   "Sint�tico"
         Height          =   255
         Left            =   240
         TabIndex        =   1
         Top             =   360
         Width           =   1575
      End
      Begin VB.OptionButton optAnalitico 
         Caption         =   "Anal�tico"
         Height          =   255
         Left            =   240
         TabIndex        =   5
         Top             =   960
         Width           =   1095
      End
      Begin VB.OptionButton optProposta 
         Caption         =   "Proposta AB espec�fica:"
         Height          =   255
         Left            =   240
         TabIndex        =   7
         Top             =   1335
         Width           =   2055
      End
      Begin VB.TextBox txtProposta 
         Height          =   285
         Left            =   2280
         MaxLength       =   9
         TabIndex        =   8
         Top             =   1320
         Width           =   1335
      End
      Begin VB.TextBox txtQtdeLista 
         Height          =   285
         Left            =   2280
         TabIndex        =   6
         Top             =   945
         Width           =   1095
      End
      Begin VB.Label Label6 
         Caption         =   "restitui��es"
         Height          =   255
         Left            =   3480
         TabIndex        =   19
         Top             =   960
         Width           =   975
      End
      Begin VB.Label Label1 
         Caption         =   "Primeiras"
         Height          =   255
         Left            =   1440
         TabIndex        =   13
         Top             =   960
         Width           =   735
      End
   End
   Begin VB.Frame Frame2 
      Caption         =   "Produto:"
      Height          =   765
      Left            =   120
      TabIndex        =   11
      Top             =   0
      Width           =   10875
      Begin VB.ComboBox CmbProduto 
         Height          =   315
         Left            =   210
         Style           =   2  'Dropdown List
         TabIndex        =   0
         Top             =   240
         Width           =   10455
      End
   End
   Begin VB.Label Label3 
      Caption         =   "De:"
      Height          =   375
      Left            =   4080
      TabIndex        =   14
      Top             =   1200
      Width           =   615
   End
End
Attribute VB_Name = "frmFiltroPesquisa"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'Renato Vasconcelos
'Flow 11022639
Public sDtInicioEsc As String
Public sDtFimEsc As String
Public lAvaliacaoModular As Long

Private Sub BtnModo_Click()

   
        
    If TpOperacao = 1 Then
        If MsgBox("Confirma a altera��o do modo para Avalia��o Gerencial?", vbYesNo + vbQuestion) = vbYes Then
            TpOperacao = 2
        Else
            Exit Sub
        End If
    Else
        If MsgBox("Confirma a altera��o do modo para Avalia��o T�cnica?", vbYesNo + vbQuestion) = vbYes Then
            TpOperacao = 1
        Else
            Exit Sub
        End If
    End If
    
    '--Matando tabelas tempor�rias
    sSQL = "SET NOCOUNT ON drop table #avaliacao_restituicao_tb;" & vbNewLine

   comboCarregado = False
   
    Set rc = rdocn.OpenResultset(sSQL)
    
    FrmEspera.Label1 = "Aguarde, carregando o SEGP0209"
    FrmEspera.Show

    Call Unload(Me)

    frmFiltroPesquisa.Show
    frmFiltroPesquisa.SetFocus
    Call Unload(FrmEspera)
End Sub

Private Sub BtnOk_Click()
    Dim sSQL            As String  'FLAVIO.BEZERRA - 06/06/2013
    
    If optSintetico.Value Then
        If txtInicial.Text = "," Then txtInicial.Text = ""
        If txtIntervalo.Text = "," Then txtIntervalo.Text = ""
        If txtFinal.Text = "," Then txtFinal.Text = ""
        
        If CmbProduto.ListIndex < 0 Then
            MsgBox "Selecione o produto!", vbInformation, "SEGP0209"
            Exit Sub
        End If
        If Len(txtInicial.Text) = 0 Then
            MsgBox "Preencha o campo Valor Inicial!", vbInformation, "SEGP0209"
            Exit Sub
        End If
        If Len(txtIntervalo.Text) = 0 Then
            MsgBox "Preencha o campo Intervalo!", vbInformation, "SEGP0209"
            Exit Sub
        End If
        
        If Len(txtFinal.Text) = 0 Then
            MsgBox "Preencha o campo Maior Que!", vbInformation, "SEGP0209"
            Exit Sub
        End If
        
        If (CDbl(txtFinal.Text) < CDbl(txtInicial.Text)) Or (CDbl(txtFinal.Text) < CDbl(txtIntervalo.Text)) Then
            MsgBox "O Campo Maior que n�o pode ser menor que o Inicial e nem menor que o Intervalo!", vbInformation, "SEGP0209"
            Exit Sub
        End If
        
        frmAnaliseSintetica.idProduto = CInt(CmbProduto.ItemData(CmbProduto.ListIndex))

        frmAnaliseSintetica.sProduto = Mid(CmbProduto.Text, 1, 60)  'Alan Neves - C_ANEVES - Demanda: 17860335 - MP - 28/05/2013
        
        frmAnaliseSintetica.inicial = CDbl(txtInicial.Text)
        frmAnaliseSintetica.intervalo = CDbl(txtIntervalo.Text)
        frmAnaliseSintetica.maiorQue = CDbl(txtFinal.Text)
        
        'Carregando o combo do outro form
        
        'FLAVIO.BEZERRA - 06/06/2013
        sSQL = ""
        sSQL = " exec seguros_db.dbo.SEGS11179_SPS " & CInt(CmbProduto.ItemData(CmbProduto.ListIndex))
        rdocn.Execute (sSQL)
        'FLAVIO.BEZERRA - 06/06/2013
        
        Call frmAnaliseSintetica.Carrega_Form
        Call Unload(Me)
        frmAnaliseSintetica.Show
        
        
    ElseIf optAnalitico.Value Then
        If CmbProduto.ListIndex < 0 Then
            MsgBox "Selecione o produto!", vbInformation, "SEGP0209"
            Exit Sub
        End If
        Dim QTD As Double
        If IsNumeric(txtQtdeLista.Text) Then
            QTD = CDbl(txtQtdeLista.Text)
        End If
        frmAnalitico.idProduto = CInt(CmbProduto.ItemData(CmbProduto.ListIndex))
        frmAnalitico.strWhere = ""
        frmAnalitico.QTD = QTD
        
        'FLAVIO.BEZERRA - 06/06/2013
        sSQL = ""
        'sSQL = " exec seguros_db.dbo.PreRemessaRecusadas_SPS " & CInt(CmbProduto.ItemData(CmbProduto.ListIndex))
        sSQL = " exec seguros_db.dbo.SEGS11179_SPS " & CInt(CmbProduto.ItemData(CmbProduto.ListIndex))
        rdocn.Execute (sSQL)
        'FLAVIO.BEZERRA - 06/06/2013
        
        Call frmAnalitico.carregarForm
        Call Unload(Me)
        frmAnalitico.Show
        
    Else
        If Len(txtProposta.Text) = 0 Then
            MsgBox "Digite o n�mero da proposta!", vbInformation, "SEGP0209"
            Exit Sub
        End If
        
        detalhe_proposta (CLng(txtProposta.Text))
       
    End If
    
End Sub

Private Sub BtnSair_Click()
    End
End Sub



Private Sub Form_Load()
     
    Call CentraFrm(Me)

    Call Carregar_ComboProduto
        
    If TpOperacao = 1 Then
        Me.Caption = "SEGP0209 - Avalia��o T�cnica de Restitui��o - " & Ambiente
        BtnModo.ToolTipText = "Alterar para modo gerencial"
    Else
        Me.Caption = "SEGP0209 - Avalia��o Gerencial de Restitui��o - " & Ambiente
        BtnModo.ToolTipText = "Alterar para modo t�cnico"
    End If
    BtnModo.Visible = gerencial
    
    
   
    optSintetico.Value = True
End Sub

'Alan Neves - C_ANEVES - Demanda: 17860335 - 03/06/2013 - Inicio
'Private Sub Carregar_ComboProduto()
'       Dim rc As rdoResultset
'    Dim oProduto As Object
'    Dim sSQL     As String
'
'
'    If Not comboCarregado Then
'       sSQL = "SET NOCOUNT ON" & vbNewLine
'       sSQL = sSQL & "SELECT" & vbNewLine
'       sSQL = sSQL & " produto_tb.produto_id," & vbNewLine
'       sSQL = sSQL & " produto_tb.nome as Produto," & vbNewLine
'       sSQL = sSQL & " avaliacao_restituicao_tb.proposta_id," & vbNewLine
'       sSQL = sSQL & " avaliacao_restituicao_tb.num_avaliacao," & vbNewLine
'       sSQL = sSQL & " avaliacao_restituicao_tb.Endosso_id as Endosso_id," & vbNewLine
'       sSQL = sSQL & " avaliacao_restituicao_tb.tp_endosso_id," & vbNewLine
'       sSQL = sSQL & " avaliacao_restituicao_tb.dt_avaliacao_tecnica," & vbNewLine
'       sSQL = sSQL & " avaliacao_restituicao_tb.usuario_avaliacao_tecnica," & vbNewLine
'       sSQL = sSQL & " avaliacao_restituicao_tb.Situacao," & vbNewLine
'       sSQL = sSQL & " avaliacao_restituicao_tb.val_restituicao_estimado," & vbNewLine
'       sSQL = sSQL & " avaliacao_restituicao_tb.Val_comissao_estimado," & vbNewLine
'       sSQL = sSQL & " avaliacao_restituicao_tb.Val_iof_estimado," & vbNewLine
'       sSQL = sSQL & " avaliacao_restituicao_tb.val_adic_fracionamento_estimado," & vbNewLine
'       sSQL = sSQL & " avaliacao_restituicao_tb.val_desconto_comercial_estimado," & vbNewLine
'       sSQL = sSQL & " avaliacao_restituicao_tb.val_premio_tarifa_estimado," & vbNewLine
'       sSQL = sSQL & " avaliacao_restituicao_tb.custo_apolice_estimado," & vbNewLine
'       sSQL = sSQL & " avaliacao_restituicao_tb.val_comissao_estipulante_estimado," & vbNewLine
'       sSQL = sSQL & " avaliacao_restituicao_tb.val_remuneracao_estimado," & vbNewLine
'       sSQL = sSQL & " avaliacao_restituicao_tb.val_cambio," & vbNewLine
'       sSQL = sSQL & " avaliacao_restituicao_tb.dt_pedido_avaliacao," & vbNewLine
'       sSQL = sSQL & " avaliacao_restituicao_tb.val_subvencao_federal," & vbNewLine
'       sSQL = sSQL & " avaliacao_restituicao_tb.val_subvencao_estadual," & vbNewLine
'       sSQL = sSQL & " avaliacao_restituicao_tb.val_subvencao_federal_estimado," & vbNewLine
'       sSQL = sSQL & " avaliacao_restituicao_tb.val_subvencao_estadual_estimado," & vbNewLine
'       sSQL = sSQL & " avaliacao_restituicao_tb.val_restituicao," & vbNewLine
'       sSQL = sSQL & " avaliacao_restituicao_tb.val_comissao," & vbNewLine
'       sSQL = sSQL & " avaliacao_restituicao_tb.val_iof," & vbNewLine
'       sSQL = sSQL & " avaliacao_restituicao_tb.val_adic_fracionamento," & vbNewLine
'       sSQL = sSQL & " avaliacao_restituicao_tb.val_desconto_comercial," & vbNewLine
'       sSQL = sSQL & " avaliacao_restituicao_tb.val_premio_tarifa," & vbNewLine
'       sSQL = sSQL & " avaliacao_restituicao_tb.custo_apolice," & vbNewLine
'       sSQL = sSQL & " avaliacao_restituicao_tb.val_comissao_estipulante," & vbNewLine
'       sSQL = sSQL & " avaliacao_restituicao_tb.motivo_recusa_gerencial," & vbNewLine
'       sSQL = sSQL & " avaliacao_restituicao_tb.dt_avaliacao_gerencial," & vbNewLine
'       sSQL = sSQL & " avaliacao_restituicao_tb.usuario_avaliacao_gerencial," & vbNewLine
'       sSQL = sSQL & " ISNULL(avaliacao_restituicao_tb.val_remuneracao, 0) AS val_remuneracao," & vbNewLine
'       sSQL = sSQL & " ISNULL(avaliacao_restituicao_tb.val_restituicao_bb, 0) AS val_restituicao_bb," & vbNewLine
'       sSQL = sSQL & " ISNULL(avaliacao_restituicao_tb.val_iof_bb, 0) AS val_iof_bb," & vbNewLine
'       sSQL = sSQL & " ISNULL(avaliacao_restituicao_tb.val_comissao_bb, 0) AS val_comissao_bb," & vbNewLine
'       sSQL = sSQL & " ISNULL(avaliacao_restituicao_tb.val_comissao_estipulante_bb, 0) AS val_comissao_estipulante_bb," & vbNewLine
'       sSQL = sSQL & " ISNULL(avaliacao_restituicao_tb.val_repasse_bb, 0) AS val_repasse_bb," & vbNewLine
'       sSQL = sSQL & " ISNULL(avaliacao_restituicao_tb.custo_apolice_bb, 0) AS custo_apolice_bb," & vbNewLine
'       sSQL = sSQL & " ISNULL(avaliacao_restituicao_tb.val_desconto_comercial_bb, 0) AS val_desconto_comercial_bb," & vbNewLine
'       sSQL = sSQL & " ISNULL(avaliacao_restituicao_tb.val_adic_fracionamento_bb, 0) AS val_adic_fracionamento_bb," & vbNewLine
'       sSQL = sSQL & " Convert(Varchar(10),'') as dt_inicio_vigencia," & vbNewLine
'       sSQL = sSQL & " Convert(Varchar(10),'') as dt_fim_vigencia," & vbNewLine
'       sSQL = sSQL & " Convert(Varchar(10),'') as DT_Emissao," & vbNewLine
'       sSQL = sSQL & " Convert(Varchar(10),'') as DT_Inclusao," & vbNewLine
'       sSQL = sSQL & " Convert(numeric(15,2),0) as PremioPago," & vbNewLine
'       sSQL = sSQL & " Convert(numeric(15,2),0) as PremioRetido," & vbNewLine
'       sSQL = sSQL & " Convert(Varchar(10),Proposta_tb.Dt_Contratacao,103) as DT_Contratacao," & vbNewLine
'       sSQL = sSQL & " Convert(Varchar(60),'') as TipoAvaliacao," & vbNewLine
'       sSQL = sSQL & " Convert(Varchar(1),'') as Existe_info_baixa," & vbNewLine
'       sSQL = sSQL & " Convert(Int,0) as Origem," & vbNewLine
'       sSQL = sSQL & " Convert(Varchar(60),'') as Cliente," & vbNewLine
'       sSQL = sSQL & " Convert(numeric(15,2),0) as ValIS," & vbNewLine
'       sSQL = sSQL & " Convert(Numeric(3,0),0) as FatorCambio," & vbNewLine
'       sSQL = sSQL & " Convert(Numeric(9), 0) As proposta_bb," & vbNewLine
'       sSQL = sSQL & " Convert(char(1), 'N') As modular," & vbNewLine
'       sSQL = sSQL & " Convert(Numeric(9), 0 ) As proposta_Modular," & vbNewLine
'       sSQL = sSQL & " Convert(tinyint, 0) As num_avaliacao_modular," & vbNewLine
'       sSQL = sSQL & " Convert(char(1), 'N') As atualizado" & vbNewLine
'
'       'Renato Vasconcelos
'       'Flow 17801752
'       sSQL = sSQL & ", Convert(char(1), 'P') AS cor" & vbNewLine
'
'       sSQL = sSQL & "into #avaliacao_restituicao_tb" & vbNewLine
'       sSQL = sSQL & " From avaliacao_restituicao_tb(nolock)" & vbNewLine
'       sSQL = sSQL & " lEFT JOIN proposta_tb (nolock)" & vbNewLine
'       sSQL = sSQL & " ON proposta_tb.proposta_id = avaliacao_restituicao_tb.proposta_id" & vbNewLine
'       sSQL = sSQL & " lEFT JOIN produto_tb (nolock)" & vbNewLine
'       sSQL = sSQL & " ON produto_tb.produto_id = proposta_tb.produto_id" & vbNewLine
'       sSQL = sSQL & " WHERE proposta_tb.situacao <> 't'" & vbNewLine
'
'        If TpOperacao = 1 Then
'            'Avaliacao t�cnica
'            sSQL = sSQL & " AND avaliacao_restituicao_tb.situacao in ('p','i')" & vbNewLine
'       Else
'            'Avaliacao Gerencial
'            sSQL = sSQL & " AND avaliacao_restituicao_tb.situacao in ('t')" & vbNewLine
'       End If
'       'sSQL = sSQL & " AND proposta_tb.produto_id=1198 " & vbNewLine 'comentar
'
'
'       sSQL = sSQL & " AND(produto_tb.processa_restituicao_automatica = 's' or produto_tb.produto_id in(12,121,135,136,716,400)" & vbNewLine
'       'Vanessa Barbosa - Confitec - 06/06/2012
'       'Incluindo produtos  BESC no filtro - Demanda: 12006056
'       sSQL = sSQL & "     OR produto_tb.produto_id in(200,201,202,203,204,205,208,209,210,211,212,213,216,217,218,219,220,221,224,225))" & vbNewLine
'
'
'        '--pegando todos que s�o propostas modulares
'        sSQL = sSQL & " SELECT" & vbNewLine
'        sSQL = sSQL & "    proposta_id," & vbNewLine
'        sSQL = sSQL & "    numero_agrupador," & vbNewLine
'        sSQL = sSQL & "    Convert(Numeric(9),0) As proposta_Modular," & vbNewLine
'        sSQL = sSQL & "    Convert(tinyint, 0) num_avaliacao" & vbNewLine
'        sSQL = sSQL & "Into" & vbNewLine
'        sSQL = sSQL & "    #propostas_modulares" & vbNewLine
'        sSQL = sSQL & "From" & vbNewLine
'        sSQL = sSQL & "    proposta_modular_tb" & vbNewLine
'        sSQL = sSQL & "Where" & vbNewLine
'        sSQL = sSQL & "    proposta_id in" & vbNewLine
'        sSQL = sSQL & "        (select proposta_id from #avaliacao_restituicao_tb where produto_id <> 1168)" & vbNewLine
'        sSQL = sSQL & "    and numero_agrupador is not null" & vbNewLine
'
'        '--trazendo propostas modulares
'        sSQL = sSQL & "UPDATE #propostas_modulares" & vbNewLine
'        sSQL = sSQL & "    Set proposta_Modular = PM.proposta_id" & vbNewLine
'        sSQL = sSQL & "From" & vbNewLine
'        sSQL = sSQL & "    proposta_modular_tb PM" & vbNewLine
'        sSQL = sSQL & "Where" & vbNewLine
'        sSQL = sSQL & "    PM.numero_agrupador = #propostas_modulares.numero_agrupador" & vbNewLine
'        sSQL = sSQL & "    AND PM.proposta_id <> #propostas_modulares.proposta_id" & vbNewLine
'
'        '--apagando quem n�o veio
'        sSQL = sSQL & "delete from #propostas_modulares where proposta_modular = 0" & vbNewLine
'
'        sSQL = sSQL & "--atualizando numero da avaliacao" & vbNewLine
'        sSQL = sSQL & "UPDATE #propostas_modulares" & vbNewLine
'        sSQL = sSQL & "Set Num_avaliacao = a.Num_avaliacao" & vbNewLine
'        sSQL = sSQL & "From" & vbNewLine
'        sSQL = sSQL & "    avaliacao_restituicao_tb a" & vbNewLine
'        sSQL = sSQL & "Where" & vbNewLine
'        sSQL = sSQL & "    #propostas_modulares.proposta_modular = a.proposta_id" & vbNewLine
'        If TpOperacao = 1 Then
'            'Avaliacao t�cnica
'            sSQL = sSQL & " AND a.situacao in ('p', 'i')" & vbNewLine
'        Else
'            'Avaliacao Gerencial
'            sSQL = sSQL & " AND a.situacao in ('t')" & vbNewLine
'        End If
'
'        '--deletando num_avaliacao = 0
'        sSQL = sSQL & "delete from #propostas_modulares where num_avaliacao = 0" & vbNewLine
'
'        '--atualizando modulares da tabela principal quando produto for <> 1168
'        sSQL = sSQL & "update #avaliacao_restituicao_tb" & vbNewLine
'        sSQL = sSQL & "    set" & vbNewLine
'        sSQL = sSQL & "        modular = 'S'," & vbNewLine
'        sSQL = sSQL & "        proposta_Modular = #propostas_modulares.proposta_Modular," & vbNewLine
'        sSQL = sSQL & "        num_avaliacao_modular = #propostas_modulares.num_avaliacao" & vbNewLine
'        sSQL = sSQL & "From" & vbNewLine
'        sSQL = sSQL & "    #propostas_modulares" & vbNewLine
'        sSQL = sSQL & "Where" & vbNewLine
'        sSQL = sSQL & "    #avaliacao_restituicao_tb.proposta_id = #propostas_modulares.proposta_id" & vbNewLine
'
'
'        '--atualizando modulares da tabela principal quando produto for = 1168
'
'        sSQL = sSQL & "update #avaliacao_restituicao_tb" & vbNewLine
'        sSQL = sSQL & "    set" & vbNewLine
'        sSQL = sSQL & "        modular = 'S'," & vbNewLine
'        sSQL = sSQL & "        proposta_Modular = a.proposta_id," & vbNewLine
'        sSQL = sSQL & "        num_avaliacao_modular = a.Num_avaliacao" & vbNewLine
'        sSQL = sSQL & "From" & vbNewLine
'        sSQL = sSQL & "    avaliacao_restituicao_tb a" & vbNewLine
'        sSQL = sSQL & "Where" & vbNewLine
'        sSQL = sSQL & "    #avaliacao_restituicao_tb.proposta_id = a.proposta_id" & vbNewLine
'        sSQL = sSQL & "    AND #avaliacao_restituicao_tb.num_avaliacao <> a.num_avaliacao" & vbNewLine
'        sSQL = sSQL & "    AND #avaliacao_restituicao_tb.produto_id = 1168" & vbNewLine
'      If TpOperacao = 1 Then
'            'Avaliacao t�cnica
'            sSQL = sSQL & " AND a.situacao in ('p', 'i')" & vbNewLine
'        Else
'            'Avaliacao Gerencial
'            sSQL = sSQL & " AND a.situacao in ('t')" & vbNewLine
'        End If
'
'        'matando tabela temporaria
'        sSQL = sSQL & "drop table #propostas_modulares" & vbNewLine
'
'        'Renato Vasconcelos
'        'Flow 17801752
'        'Inicio
'         sSQL = sSQL & "UPDATE A " & vbNewLine
'         sSQL = sSQL & "    SET A.cor = 'V'" & vbNewLine
'         sSQL = sSQL & "FROM" & vbNewLine
'         sSQL = sSQL & "    #avaliacao_restituicao_tb A" & vbNewLine
'         sSQL = sSQL & "    INNER JOIN avaliacao_restituicao_tb B (NOLOCK)" & vbNewLine
'         sSQL = sSQL & "        ON A.proposta_id = B.proposta_id" & vbNewLine
''         sSQL = sSQL & "        AND A.val_restituicao_estimado = B.val_restituicao_estimado" & vbNewLine
''         sSQL = sSQL & "        AND ISNULL(A.val_restituicao,0) = ISNULL(B.val_restituicao,0)" & vbNewLine
'         sSQL = sSQL & "        AND A.num_avaliacao <> B.num_avaliacao" & vbNewLine
'         sSQL = sSQL & "    Where" & vbNewLine
'         sSQL = sSQL & "        A.num_avaliacao > 1" & vbNewLine
'         sSQL = sSQL & "        AND B.SITUACAO <> 'R'" & vbNewLine
'        'Fim
'
'
'      Set rc = rdocn.OpenResultset(sSQL)
'
'      rc.Close
'      Set rc = Nothing
'
'      comboCarregado = True
'    End If
'
'    If TpOperacao = 1 Then
'          'Avalia��o T�cnica
'          sSQL = "Select count(1) as ProdTotal, Produto_id,Produto  from #avaliacao_restituicao_tb  where situacao in ('p','i') group by Produto,Produto_id"
'    Else
'          'Avalia��o Gerencial
'          sSQL = "Select count(1) as ProdTotal, Produto_id,Produto from #avaliacao_restituicao_tb  where situacao in ('t') group by Produto,Produto_id"
'    End If
'
'
'    Set rc = rdocn.OpenResultset(sSQL)
'    Do While Not rc.EOF
'        CmbProduto.AddItem UCase(rc("Produto")) & " - " & rc("ProdTotal") & " endosso(s)"
'        CmbProduto.ItemData(CmbProduto.NewIndex) = rc("produto_id")
'        rc.MoveNext
'    Loop
'    rc.Close
'    Set rc = Nothing
'
'End Sub



Private Sub Carregar_ComboProduto()
       Dim rc As rdoResultset
    Dim oProduto As Object
    Dim sSQL     As String
    
    
    If Not comboCarregado Then
       sSQL = "SET NOCOUNT ON " & vbNewLine
       sSQL = sSQL & "SELECT" & vbNewLine
       sSQL = sSQL & " produto_tb.produto_id," & vbNewLine
       sSQL = sSQL & " produto_tb.nome as Produto," & vbNewLine
       sSQL = sSQL & " avaliacao_restituicao_tb.proposta_id," & vbNewLine
       sSQL = sSQL & " avaliacao_restituicao_tb.num_avaliacao," & vbNewLine
       sSQL = sSQL & " avaliacao_restituicao_tb.Endosso_id as Endosso_id," & vbNewLine
       sSQL = sSQL & " avaliacao_restituicao_tb.tp_endosso_id," & vbNewLine
       sSQL = sSQL & " tp_endosso_tb.descricao," & vbNewLine
       sSQL = sSQL & " avaliacao_restituicao_tb.dt_avaliacao_tecnica," & vbNewLine
       sSQL = sSQL & " avaliacao_restituicao_tb.usuario_avaliacao_tecnica," & vbNewLine
       sSQL = sSQL & " avaliacao_restituicao_tb.Situacao," & vbNewLine
       sSQL = sSQL & " avaliacao_restituicao_tb.val_restituicao_estimado," & vbNewLine
       sSQL = sSQL & " avaliacao_restituicao_tb.Val_comissao_estimado," & vbNewLine
       sSQL = sSQL & " avaliacao_restituicao_tb.Val_iof_estimado," & vbNewLine
       sSQL = sSQL & " avaliacao_restituicao_tb.val_adic_fracionamento_estimado," & vbNewLine
       sSQL = sSQL & " avaliacao_restituicao_tb.val_desconto_comercial_estimado," & vbNewLine
       sSQL = sSQL & " avaliacao_restituicao_tb.val_premio_tarifa_estimado," & vbNewLine
       sSQL = sSQL & " avaliacao_restituicao_tb.custo_apolice_estimado," & vbNewLine
       sSQL = sSQL & " avaliacao_restituicao_tb.val_comissao_estipulante_estimado," & vbNewLine
       sSQL = sSQL & " avaliacao_restituicao_tb.val_remuneracao_estimado," & vbNewLine
           'In�cio Altera��o - Tales de S� - INC000004234556
       'sSQL = sSQL & " avaliacao_restituicao_tb.val_cambio," & vbNewLine
           sSQL = sSQL & " ISNULL(avaliacao_restituicao_tb.val_cambio, 0 ) as val_cambio," & vbNewLine
           'Fim Altera��o - INC000004234556
       sSQL = sSQL & " avaliacao_restituicao_tb.dt_pedido_avaliacao," & vbNewLine
       sSQL = sSQL & " avaliacao_restituicao_tb.val_subvencao_federal," & vbNewLine
       sSQL = sSQL & " avaliacao_restituicao_tb.val_subvencao_estadual," & vbNewLine
       sSQL = sSQL & " avaliacao_restituicao_tb.val_subvencao_federal_estimado," & vbNewLine
       sSQL = sSQL & " avaliacao_restituicao_tb.val_subvencao_estadual_estimado," & vbNewLine
       sSQL = sSQL & " avaliacao_restituicao_tb.val_restituicao," & vbNewLine
       sSQL = sSQL & " avaliacao_restituicao_tb.val_comissao," & vbNewLine
       sSQL = sSQL & " avaliacao_restituicao_tb.val_iof," & vbNewLine
       sSQL = sSQL & " avaliacao_restituicao_tb.val_adic_fracionamento," & vbNewLine
       sSQL = sSQL & " avaliacao_restituicao_tb.val_desconto_comercial," & vbNewLine
       sSQL = sSQL & " avaliacao_restituicao_tb.val_premio_tarifa," & vbNewLine
       sSQL = sSQL & " avaliacao_restituicao_tb.custo_apolice," & vbNewLine
       sSQL = sSQL & " avaliacao_restituicao_tb.val_comissao_estipulante," & vbNewLine
       sSQL = sSQL & " avaliacao_restituicao_tb.motivo_recusa_gerencial," & vbNewLine
       sSQL = sSQL & " avaliacao_restituicao_tb.dt_avaliacao_gerencial," & vbNewLine
       sSQL = sSQL & " avaliacao_restituicao_tb.usuario_avaliacao_gerencial," & vbNewLine
       sSQL = sSQL & " ISNULL(avaliacao_restituicao_tb.val_remuneracao, 0) AS val_remuneracao," & vbNewLine
       sSQL = sSQL & " ISNULL(avaliacao_restituicao_tb.val_restituicao_bb, 0) AS val_restituicao_bb," & vbNewLine
       sSQL = sSQL & " ISNULL(avaliacao_restituicao_tb.val_iof_bb, 0) AS val_iof_bb," & vbNewLine
       sSQL = sSQL & " ISNULL(avaliacao_restituicao_tb.val_comissao_bb, 0) AS val_comissao_bb," & vbNewLine
       sSQL = sSQL & " ISNULL(avaliacao_restituicao_tb.val_comissao_estipulante_bb, 0) AS val_comissao_estipulante_bb," & vbNewLine
       sSQL = sSQL & " ISNULL(avaliacao_restituicao_tb.val_repasse_bb, 0) AS val_repasse_bb," & vbNewLine
       sSQL = sSQL & " ISNULL(avaliacao_restituicao_tb.custo_apolice_bb, 0) AS custo_apolice_bb," & vbNewLine
       sSQL = sSQL & " ISNULL(avaliacao_restituicao_tb.val_desconto_comercial_bb, 0) AS val_desconto_comercial_bb," & vbNewLine
       sSQL = sSQL & " ISNULL(avaliacao_restituicao_tb.val_adic_fracionamento_bb, 0) AS val_adic_fracionamento_bb," & vbNewLine
       sSQL = sSQL & " Convert(Varchar(10),'') as dt_inicio_vigencia," & vbNewLine
       sSQL = sSQL & " Convert(Varchar(10),'') as dt_fim_vigencia," & vbNewLine
       sSQL = sSQL & " Convert(Varchar(10),'') as DT_Emissao," & vbNewLine
       sSQL = sSQL & " Convert(Varchar(10),'') as DT_Inclusao," & vbNewLine
       sSQL = sSQL & " Convert(numeric(15,2),0) as PremioPago," & vbNewLine
       sSQL = sSQL & " Convert(numeric(15,2),0) as PremioRetido," & vbNewLine
       sSQL = sSQL & " Convert(Varchar(10),Proposta_tb.Dt_Contratacao,103) as DT_Contratacao," & vbNewLine
       sSQL = sSQL & " Convert(Varchar(60),'') as TipoAvaliacao," & vbNewLine
       sSQL = sSQL & " Convert(Varchar(1),'') as Existe_info_baixa," & vbNewLine
       sSQL = sSQL & " Convert(Int,0) as Origem," & vbNewLine
       sSQL = sSQL & " Convert(Varchar(60),'') as Cliente," & vbNewLine
       sSQL = sSQL & " Convert(numeric(15,2),0) as ValIS," & vbNewLine
       sSQL = sSQL & " Convert(Numeric(3,0),0) as FatorCambio," & vbNewLine
       sSQL = sSQL & " Convert(Numeric(9), 0) As proposta_bb," & vbNewLine
       sSQL = sSQL & " Convert(char(1), 'N') As modular," & vbNewLine
       sSQL = sSQL & " Convert(Numeric(9), 0 ) As proposta_Modular," & vbNewLine
       sSQL = sSQL & " Convert(tinyint, 0) As num_avaliacao_modular," & vbNewLine
       sSQL = sSQL & " Convert(char(1), 'N') As atualizado" & vbNewLine
       
       'Renato Vasconcelos
       'Flow 17801752
       sSQL = sSQL & ", Convert(char(1), 'P') AS cor" & vbNewLine
       ' MU-2017-054666 - Rog�rio Melo - Confitec SP
       '13/03/2018 inclus�o de novos campos
       sSQL = sSQL & ", isnull(endosso_tb.dt_pedido_endosso, avaliacao_restituicao_tb.dt_pedido_avaliacao) as dt_pedido_endosso" & vbNewLine
       sSQL = sSQL & ", Convert(Int,0) as qtd_parcela_pagas" & vbNewLine
       sSQL = sSQL & ", Convert(Varchar(60),'') as periodicidade_pagamento" & vbNewLine
       sSQL = sSQL & ", case when isnull(endosso_tb.usuario,avaliacao_restituicao_tb.usuario) like '%_sp%' then 'SVCCONTROLM'" & vbNewLine
       sSQL = sSQL & "       else isnull(endosso_tb.usuario,avaliacao_restituicao_tb.usuario) end as usuario_restituicao" & vbNewLine
       sSQL = sSQL & ",  Convert(Varchar(max),endosso_tb.descricao_endosso)  as descricao_endosso" & vbNewLine
       ' MU-2017-054666 - Rog�rio Melo - Confitec SP
       '13/03/2018 inclus�o de novos campos
        
       sSQL = sSQL & "into #avaliacao_restituicao_tb" & vbNewLine
       sSQL = sSQL & " From avaliacao_restituicao_tb with(nolock)" & vbNewLine
       sSQL = sSQL & " lEFT JOIN proposta_tb with(nolock)" & vbNewLine
       sSQL = sSQL & " ON proposta_tb.proposta_id = avaliacao_restituicao_tb.proposta_id" & vbNewLine
       sSQL = sSQL & " lEFT JOIN produto_tb with(nolock)" & vbNewLine
       sSQL = sSQL & " ON produto_tb.produto_id = proposta_tb.produto_id" & vbNewLine
       ' MU-2017-054666 - Rog�rio Melo - Confitec SP
       '13/03/2018 join para trazer campo dt_pedido_endosso
       sSQL = sSQL & " LEFT JOIN seguros_db.dbo.endosso_tb AS endosso_tb with(nolock)" & vbNewLine
       sSQL = sSQL & "    ON endosso_tb.proposta_id = avaliacao_restituicao_tb.proposta_id" & vbNewLine
       sSQL = sSQL & "   AND endosso_tb.endosso_id =  avaliacao_restituicao_tb.Endosso_id" & vbNewLine
       sSQL = sSQL & " LEFT JOIN seguros_db.dbo.tp_endosso_tb AS tp_endosso_tb" & vbNewLine
       sSQL = sSQL & "    ON tp_endosso_tb.tp_endosso_id = endosso_tb.tp_endosso_id" & vbNewLine
       ' MU-2017-054666 - Rog�rio Melo - Confitec SP
       '13/03/2018 join para trazer campo dt_pedido_endosso
       sSQL = sSQL & " WHERE proposta_tb.situacao <> 't'" & vbNewLine
       
    '-- Nova Tendencia -- 18794751 - Reestrutura��o do Endosso 253 -- Paulo Prata Tavares -- INICIO
       'sSQL = sSQL & " AND avaliacao_restituicao_tb.ind_libera_avaliacao <> 'N' " & vbNewLine
       sSQL = sSQL & " AND (IND_LIBERA_AVALIACAO = 'S' or  IND_LIBERA_AVALIACAO IS NULL) " & vbNewLine
    '-- Nova Tendencia -- 18794751 - Reestrutura��o do Endosso 253 -- Paulo Prata Tavares -- INICIO
       
        If TpOperacao = 1 Then
            'Avaliacao t�cnica
            sSQL = sSQL & " AND avaliacao_restituicao_tb.situacao in ('p','i')" & vbNewLine
       Else
            'Avaliacao Gerencial
            sSQL = sSQL & " AND avaliacao_restituicao_tb.situacao in ('t')" & vbNewLine
       End If
       'sSQL = sSQL & " AND proposta_tb.produto_id=1198 " & vbNewLine 'comentar
       
       'FLAVIO.BEZERRA - IN�CIO - 14413025 - 10/06/2013
       sSQL = sSQL & " Union" & vbNewLine
        sSQL = sSQL & " SELECT" & vbNewLine
        sSQL = sSQL & " produto_tb.produto_id," & vbNewLine
        sSQL = sSQL & " produto_tb.nome as Produto," & vbNewLine
        sSQL = sSQL & " avaliacao_restituicao_tb.proposta_id," & vbNewLine
        sSQL = sSQL & " avaliacao_restituicao_tb.num_avaliacao," & vbNewLine
        sSQL = sSQL & " avaliacao_restituicao_tb.Endosso_id as Endosso_id," & vbNewLine
        sSQL = sSQL & " avaliacao_restituicao_tb.tp_endosso_id," & vbNewLine
        sSQL = sSQL & " tp_endosso_tb.descricao," & vbNewLine
        sSQL = sSQL & " avaliacao_restituicao_tb.dt_avaliacao_tecnica," & vbNewLine
        sSQL = sSQL & " avaliacao_restituicao_tb.usuario_avaliacao_tecnica," & vbNewLine
        sSQL = sSQL & " avaliacao_restituicao_tb.Situacao," & vbNewLine
        sSQL = sSQL & " avaliacao_restituicao_tb.val_restituicao_estimado," & vbNewLine
        sSQL = sSQL & " avaliacao_restituicao_tb.Val_comissao_estimado," & vbNewLine
        sSQL = sSQL & " avaliacao_restituicao_tb.Val_iof_estimado," & vbNewLine
        sSQL = sSQL & " avaliacao_restituicao_tb.val_adic_fracionamento_estimado," & vbNewLine
        sSQL = sSQL & " avaliacao_restituicao_tb.val_desconto_comercial_estimado," & vbNewLine
        sSQL = sSQL & " avaliacao_restituicao_tb.val_premio_tarifa_estimado," & vbNewLine
        sSQL = sSQL & " avaliacao_restituicao_tb.custo_apolice_estimado," & vbNewLine
        sSQL = sSQL & " avaliacao_restituicao_tb.val_comissao_estipulante_estimado," & vbNewLine
        sSQL = sSQL & " avaliacao_restituicao_tb.val_remuneracao_estimado," & vbNewLine
        'In�cio Altera��o - Tales de S� - INC000004234556
        'sSQL = sSQL & " avaliacao_restituicao_tb.val_cambio," & vbNewLine
            sSQL = sSQL & " ISNULL(avaliacao_restituicao_tb.val_cambio, 0 ) as val_cambio," & vbNewLine
            'Fim Altera��o - INC000004234556
        sSQL = sSQL & " avaliacao_restituicao_tb.dt_pedido_avaliacao," & vbNewLine
        sSQL = sSQL & " avaliacao_restituicao_tb.val_subvencao_federal," & vbNewLine
        sSQL = sSQL & " avaliacao_restituicao_tb.val_subvencao_estadual," & vbNewLine
        sSQL = sSQL & " avaliacao_restituicao_tb.val_subvencao_federal_estimado," & vbNewLine
        sSQL = sSQL & " avaliacao_restituicao_tb.val_subvencao_estadual_estimado," & vbNewLine
        sSQL = sSQL & " avaliacao_restituicao_tb.val_restituicao," & vbNewLine
        sSQL = sSQL & " avaliacao_restituicao_tb.val_comissao," & vbNewLine
        sSQL = sSQL & " avaliacao_restituicao_tb.val_iof," & vbNewLine
        sSQL = sSQL & " avaliacao_restituicao_tb.val_adic_fracionamento," & vbNewLine
        sSQL = sSQL & " avaliacao_restituicao_tb.val_desconto_comercial," & vbNewLine
        sSQL = sSQL & " avaliacao_restituicao_tb.val_premio_tarifa," & vbNewLine
        sSQL = sSQL & " avaliacao_restituicao_tb.custo_apolice," & vbNewLine
        sSQL = sSQL & " avaliacao_restituicao_tb.val_comissao_estipulante," & vbNewLine
        sSQL = sSQL & " avaliacao_restituicao_tb.motivo_recusa_gerencial," & vbNewLine
        sSQL = sSQL & " avaliacao_restituicao_tb.dt_avaliacao_gerencial," & vbNewLine
        sSQL = sSQL & " avaliacao_restituicao_tb.usuario_avaliacao_gerencial," & vbNewLine
        sSQL = sSQL & " ISNULL(avaliacao_restituicao_tb.val_remuneracao, 0) AS val_remuneracao," & vbNewLine
        sSQL = sSQL & " ISNULL(avaliacao_restituicao_tb.val_restituicao_bb, 0) AS val_restituicao_bb," & vbNewLine
        sSQL = sSQL & " ISNULL(avaliacao_restituicao_tb.val_iof_bb, 0) AS val_iof_bb," & vbNewLine
        sSQL = sSQL & " ISNULL(avaliacao_restituicao_tb.val_comissao_bb, 0) AS val_comissao_bb," & vbNewLine
        sSQL = sSQL & " ISNULL(avaliacao_restituicao_tb.val_comissao_estipulante_bb, 0) AS val_comissao_estipulante_bb," & vbNewLine
        sSQL = sSQL & " ISNULL(avaliacao_restituicao_tb.val_repasse_bb, 0) AS val_repasse_bb," & vbNewLine
        sSQL = sSQL & " ISNULL(avaliacao_restituicao_tb.custo_apolice_bb, 0) AS custo_apolice_bb," & vbNewLine
        sSQL = sSQL & " ISNULL(avaliacao_restituicao_tb.val_desconto_comercial_bb, 0) AS val_desconto_comercial_bb," & vbNewLine
        sSQL = sSQL & " ISNULL(avaliacao_restituicao_tb.val_adic_fracionamento_bb, 0) AS val_adic_fracionamento_bb," & vbNewLine
        sSQL = sSQL & " Convert(Varchar(10),'') as dt_inicio_vigencia," & vbNewLine
        sSQL = sSQL & " Convert(Varchar(10),'') as dt_fim_vigencia," & vbNewLine
        sSQL = sSQL & " Convert(Varchar(10),'') as DT_Emissao," & vbNewLine
        sSQL = sSQL & " Convert(Varchar(10),'') as DT_Inclusao," & vbNewLine
        sSQL = sSQL & " Convert(numeric(15,2),0) as PremioPago," & vbNewLine
        sSQL = sSQL & " Convert(numeric(15,2),0) as PremioRetido," & vbNewLine
        sSQL = sSQL & " Convert(Varchar(10),Proposta_tb.Dt_Contratacao,103) as DT_Contratacao," & vbNewLine
        sSQL = sSQL & " Convert(Varchar(60),'') as TipoAvaliacao," & vbNewLine
        sSQL = sSQL & " Convert(Varchar(1),'') as Existe_info_baixa," & vbNewLine
        sSQL = sSQL & " Convert(Int,0) as Origem," & vbNewLine
        sSQL = sSQL & " Convert(Varchar(60),'') as Cliente," & vbNewLine
        sSQL = sSQL & " Convert(numeric(15,2),0) as ValIS," & vbNewLine
        sSQL = sSQL & " Convert(Numeric(3,0),0) as FatorCambio," & vbNewLine
        sSQL = sSQL & " Convert(Numeric(9), 0) As proposta_bb," & vbNewLine
        sSQL = sSQL & " Convert(char(1), 'N') As modular," & vbNewLine
        sSQL = sSQL & " Convert(Numeric(9), 0 ) As proposta_Modular," & vbNewLine
        sSQL = sSQL & " Convert(tinyint, 0) As num_avaliacao_modular," & vbNewLine
        sSQL = sSQL & " Convert(char(1), 'N') As atualizado, " & vbNewLine
        sSQL = sSQL & " 'A' AS cor" & vbNewLine
        ' MU-2017-054666 - Rog�rio Melo - Confitec SP
        '13/03/2018 inclus�o de novos campos
        sSQL = sSQL & ", isnull(endosso_tb.dt_pedido_endosso, avaliacao_restituicao_tb.dt_pedido_avaliacao) as dt_pedido_endosso" & vbNewLine
        sSQL = sSQL & ", Convert(Int,0) as qtd_parcela_pagas" & vbNewLine
        sSQL = sSQL & ", Convert(Varchar(60),'') as periodicidade_pagamento" & vbNewLine
        sSQL = sSQL & ", case when isnull(endosso_tb.usuario,avaliacao_restituicao_tb.usuario) like '%_sp%' then 'SVCCONTROLM'" & vbNewLine
        sSQL = sSQL & "       else isnull(endosso_tb.usuario,avaliacao_restituicao_tb.usuario) end as usuario_restituicao" & vbNewLine
        sSQL = sSQL & ",  Convert(Varchar(max),endosso_tb.descricao_endosso)  as descricao_endosso" & vbNewLine
        ' MU-2017-054666 - Rog�rio Melo - Confitec SP
        '13/03/2018 inclus�o de novos campos
        sSQL = sSQL & " From avaliacao_restituicao_tb with(nolock)" & vbNewLine
        sSQL = sSQL & " LEFT JOIN proposta_tb with(nolock)" & vbNewLine
        sSQL = sSQL & " ON proposta_tb.proposta_id = avaliacao_restituicao_tb.proposta_id" & vbNewLine
        sSQL = sSQL & " LEFT JOIN produto_tb with(nolock)" & vbNewLine
        sSQL = sSQL & " ON produto_tb.produto_id = proposta_tb.produto_id" & vbNewLine
        sSQL = sSQL & " Join ps_mov_endosso_financeiro_tb with(nolock)" & vbNewLine
        sSQL = sSQL & "   ON ps_mov_endosso_financeiro_tb.proposta_id = proposta_tb.proposta_id" & vbNewLine
         'In�cio Altera��o - Cibele Pereira - 12/02/2014 INC000004262687
        sSQL = sSQL & "   AND ps_mov_endosso_financeiro_tb.endosso_id = avaliacao_restituicao_tb.endosso_id" & vbNewLine
         'Fim Altera��o - Cibele Pereira - 12/02/2014 INC000004262687
        sSQL = sSQL & " Join ps_mov_cliente_tb with(nolock)" & vbNewLine
        sSQL = sSQL & "   ON ps_mov_cliente_tb.movimentacao_id = ps_mov_endosso_financeiro_tb.movimentacao_id" & vbNewLine
        
        ' MU-2017-054666 - Rog�rio Melo - Confitec SP
        '13/03/2018 join para trazer campo dt_pedido_endosso
        sSQL = sSQL & " LEFT JOIN seguros_db.dbo.endosso_tb AS endosso_tb with(nolock)" & vbNewLine
        sSQL = sSQL & "    ON endosso_tb.proposta_id = avaliacao_restituicao_tb.proposta_id" & vbNewLine
        sSQL = sSQL & "   AND endosso_tb.endosso_id =  avaliacao_restituicao_tb.Endosso_id" & vbNewLine
        sSQL = sSQL & " LEFT JOIN seguros_db.dbo.tp_endosso_tb AS tp_endosso_tb" & vbNewLine
        sSQL = sSQL & "    ON tp_endosso_tb.tp_endosso_id = endosso_tb.tp_endosso_id" & vbNewLine
        ' MU-2017-054666 - Rog�rio Melo - Confitec SP
        '13/03/2018 join para trazer campo dt_pedido_endosso
        
        sSQL = sSQL & " WHERE proposta_tb.situacao <> 't'" & vbNewLine
        sSQL = sSQL & " AND avaliacao_restituicao_tb.situacao in ('a')" & vbNewLine
        sSQL = sSQL & " AND ps_mov_cliente_tb.remessa_liberada = 'N'" & vbNewLine
       sSQL = sSQL & " AND(produto_tb.processa_restituicao_automatica = 's' or produto_tb.produto_id in(12,121,135,136,716,400)" & vbNewLine
       'Vanessa Barbosa - Confitec - 06/06/2012
       'Incluindo produtos  BESC no filtro - Demanda: 12006056
       sSQL = sSQL & "     OR produto_tb.produto_id in(200,201,202,203,204,205,208,209,210,211,212,213,216,217,218,219,220,221,224,225))" & vbNewLine
       'FLAVIO.BEZERRA - FIM - 14413025 - 10/06/2013
       
        '-- Nova Tendencia -- 18794751 - Reestrutura��o do Endosso 253 -- Paulo Prata Tavares -- INICIO
            'sSQL = sSQL & " AND avaliacao_restituicao_tb.ind_libera_avaliacao <> 'N' " & vbNewLine
            sSQL = sSQL & " AND (IND_LIBERA_AVALIACAO = 'S' or  IND_LIBERA_AVALIACAO IS NULL) " & vbNewLine
        '-- Nova Tendencia -- 18794751 - Reestrutura��o do Endosso 253 -- Paulo Prata Tavares -- INICIO
      
       
        '--pegando todos que s�o propostas modulares
        sSQL = sSQL & " SELECT" & vbNewLine
        sSQL = sSQL & "    proposta_id," & vbNewLine
        sSQL = sSQL & "    numero_agrupador," & vbNewLine
        sSQL = sSQL & "    Convert(Numeric(9),0) As proposta_Modular," & vbNewLine
        sSQL = sSQL & "    Convert(tinyint, 0) num_avaliacao" & vbNewLine
        sSQL = sSQL & "Into" & vbNewLine
        sSQL = sSQL & "    #propostas_modulares" & vbNewLine
        sSQL = sSQL & "From" & vbNewLine
        sSQL = sSQL & "    proposta_modular_tb" & vbNewLine
        sSQL = sSQL & "Where" & vbNewLine
        sSQL = sSQL & "    proposta_id in" & vbNewLine
  
      sSQL = sSQL & "        (select proposta_id from #avaliacao_restituicao_tb where produto_id <> 1168)" & vbNewLine
        sSQL = sSQL & "    and numero_agrupador is not null" & vbNewLine
        
        '--trazendo propostas modulares
        sSQL = sSQL & "UPDATE #propostas_modulares" & vbNewLine
        sSQL = sSQL & "    Set proposta_Modular = PM.proposta_id" & vbNewLine
        sSQL = sSQL & "From" & vbNewLine
        sSQL = sSQL & "    proposta_modular_tb PM" & vbNewLine
        sSQL = sSQL & "Where" & vbNewLine
        sSQL = sSQL & "    PM.numero_agrupador = #propostas_modulares.numero_agrupador" & vbNewLine
        sSQL = sSQL & "    AND PM.proposta_id <> #propostas_modulares.proposta_id" & vbNewLine
        
        '--apagando quem n�o veio
        sSQL = sSQL & "delete from #propostas_modulares where proposta_modular = 0" & vbNewLine
        
        sSQL = sSQL & "--atualizando numero da avaliacao" & vbNewLine
        sSQL = sSQL & "UPDATE #propostas_modulares" & vbNewLine
        sSQL = sSQL & "Set Num_avaliacao = a.Num_avaliacao" & vbNewLine
        sSQL = sSQL & "From" & vbNewLine
        sSQL = sSQL & "    avaliacao_restituicao_tb a" & vbNewLine
        sSQL = sSQL & "Where" & vbNewLine
        sSQL = sSQL & "    #propostas_modulares.proposta_modular = a.proposta_id" & vbNewLine
        If TpOperacao = 1 Then
            'Avaliacao t�cnica
            sSQL = sSQL & " AND a.situacao in ('p', 'i')" & vbNewLine
        Else
            'Avaliacao Gerencial
            sSQL = sSQL & " AND a.situacao in ('t')" & vbNewLine
        End If
        
        '--deletando num_avaliacao = 0
        sSQL = sSQL & "delete from #propostas_modulares where num_avaliacao = 0" & vbNewLine
        
        'FLAVIO.BEZERRA - IN�CIO - 14413025 - 10/06/2013
        sSQL = sSQL & "Delete #avaliacao_restituicao_tb                                                                                     " & vbNewLine
        sSQL = sSQL & "  From #avaliacao_restituicao_tb                                                A with(nolock)                       " & vbNewLine
        sSQL = sSQL & "  Join seguros_db.dbo.cancelamento_endosso_tb                                   cancelamento_endosso_tb with(nolock) " & vbNewLine
        sSQL = sSQL & "    on cancelamento_endosso_tb.canc_endosso_id                                  = A.endosso_id                       " & vbNewLine
        sSQL = sSQL & "   And cancelamento_endosso_tb.proposta_id                                      = A.proposta_id                      " & vbNewLine
        'FLAVIO.BEZERRA - FIM - 14413025 - 10/06/2013
         'Renato Vasconcelos
        'MU-2018-021454
        'Inicio
        sSQL = sSQL & "WHERE produto_id not in(11, 1198, 1183, 1205, 1208, 1211, 1225, 1231, 1177, 1179, 1180, 1181, 1182, 12, 121, 135, 136, 716, 721, 1174, 1175, 1196, 1206, 1217, 1235, 1236, 1237, 1218) " & vbNewLine
        'Fim
        
        '--atualizando modulares da tabela principal quando produto for <> 1168
        sSQL = sSQL & "update #avaliacao_restituicao_tb" & vbNewLine
        sSQL = sSQL & "    set" & vbNewLine
        sSQL = sSQL & "        modular = 'S'," & vbNewLine
        sSQL = sSQL & "        proposta_Modular = #propostas_modulares.proposta_Modular," & vbNewLine
        sSQL = sSQL & "        num_avaliacao_modular = #propostas_modulares.num_avaliacao" & vbNewLine
        sSQL = sSQL & "From" & vbNewLine
        sSQL = sSQL & "    #propostas_modulares" & vbNewLine
        sSQL = sSQL & "Where" & vbNewLine
        sSQL = sSQL & "    #avaliacao_restituicao_tb.proposta_id = #propostas_modulares.proposta_id" & vbNewLine
        
        
        '--atualizando modulares da tabela principal quando produto for = 1168
        
        sSQL = sSQL & "update #avaliacao_restituicao_tb" & vbNewLine
        sSQL = sSQL & "    set" & vbNewLine
        sSQL = sSQL & "        modular = 'S'," & vbNewLine
        sSQL = sSQL & "        proposta_Modular = a.proposta_id," & vbNewLine
        sSQL = sSQL & "        num_avaliacao_modular = a.Num_avaliacao" & vbNewLine
        sSQL = sSQL & "From" & vbNewLine
        sSQL = sSQL & "    avaliacao_restituicao_tb a" & vbNewLine
        sSQL = sSQL & "Where" & vbNewLine
        sSQL = sSQL & "    #avaliacao_restituicao_tb.proposta_id = a.proposta_id" & vbNewLine
        sSQL = sSQL & "    AND #avaliacao_restituicao_tb.num_avaliacao <> a.num_avaliacao" & vbNewLine
        sSQL = sSQL & "    AND #avaliacao_restituicao_tb.produto_id = 1168" & vbNewLine
        

      If TpOperacao = 1 Then
            'Avaliacao t�cnica
            sSQL = sSQL & " AND a.situacao in ('p', 'i')" & vbNewLine
        Else
            'Avaliacao Gerencial
            sSQL = sSQL & " AND a.situacao in ('t')" & vbNewLine
        End If
        
        'matando tabela temporaria
        sSQL = sSQL & "drop table #propostas_modulares" & vbNewLine
        
        'Renato Vasconcelos
        'Flow 17801752
        'Inicio
         sSQL = sSQL & "UPDATE A " & vbNewLine
         sSQL = sSQL & "    SET A.cor = 'V'" & vbNewLine
         sSQL = sSQL & "FROM" & vbNewLine
         sSQL = sSQL & "    #avaliacao_restituicao_tb A" & vbNewLine
         sSQL = sSQL & "    INNER JOIN avaliacao_restituicao_tb B with(NOLOCK)" & vbNewLine
         sSQL = sSQL & "        ON A.proposta_id = B.proposta_id" & vbNewLine
'         sSQL = sSQL & "        AND A.val_restituicao_estimado = B.val_restituicao_estimado" & vbNewLine
'         sSQL = sSQL & "        AND ISNULL(A.val_restituicao,0) = ISNULL(B.val_restituicao,0)" & vbNewLine
         sSQL = sSQL & "        AND A.num_avaliacao <> B.num_avaliacao" & vbNewLine
         sSQL = sSQL & "    Where" & vbNewLine
         sSQL = sSQL & "        A.num_avaliacao > 1" & vbNewLine
         sSQL = sSQL & "        AND B.SITUACAO <> 'R'" & vbNewLine
        'Fim
            
        ' MU-2017-054666 - Rog�rio Melo - Confitec SP
        '13/03/2018 atualizando a quantidade de parcelas pagas ate a data de pedido do endosso
        sSQL = sSQL & "  UPDATE #avaliacao_restituicao_tb" & vbNewLine
        sSQL = sSQL & "     Set qtd_parcela_pagas = agendamento.Num_Cobranca" & vbNewLine
        sSQL = sSQL & "    FROM #avaliacao_restituicao_tb as #avaliacao_restituicao_tb" & vbNewLine
        sSQL = sSQL & "  inner join (select count(num_cobranca)as num_cobranca" & vbNewLine
        sSQL = sSQL & "                   , agendamento.proposta_id as proposta_id" & vbNewLine
        sSQL = sSQL & "                FROM seguros_db.dbo.agendamento_cobranca_tb AS agendamento WITH(NOLOCK)" & vbNewLine
        sSQL = sSQL & "                JOIN #avaliacao_restituicao_tb AS #avaliacao_restituicao_tb" & vbNewLine
        sSQL = sSQL & "                  ON agendamento.proposta_id = #avaliacao_restituicao_tb.proposta_id" & vbNewLine
        sSQL = sSQL & " AND agendamento.dt_baixa <= #avaliacao_restituicao_tb.dt_pedido_endosso" & vbNewLine
        sSQL = sSQL & "                 AND agendamento.situacao = 'a'" & vbNewLine
        sSQL = sSQL & "            group by agendamento.proposta_id) as agendamento" & vbNewLine
        sSQL = sSQL & "     on agendamento.proposta_id = #avaliacao_restituicao_tb.proposta_id" & vbNewLine
        
        sSQL = sSQL & "update #avaliacao_restituicao_tb" & vbNewLine
        sSQL = sSQL & "   set periodicidade_pagamento =  periodo_pgto_tb.nome" & vbNewLine
        sSQL = sSQL & "  from #avaliacao_restituicao_tb as #avaliacao_restituicao_tb" & vbNewLine
        sSQL = sSQL & "  left join seguros_db.dbo.proposta_fechada_tb as proposta_fechada_tb with(nolock)" & vbNewLine
        sSQL = sSQL & "    on proposta_fechada_tb.proposta_id = #avaliacao_restituicao_tb.proposta_id " & vbNewLine
        sSQL = sSQL & "  left join seguros_db.dbo.proposta_adesao_tb as proposta_adesao_tb with(nolock)" & vbNewLine
        sSQL = sSQL & "    on proposta_adesao_tb.proposta_id = #avaliacao_restituicao_tb.proposta_id  " & vbNewLine
        sSQL = sSQL & "  left join seguros_db.dbo.periodo_pgto_tb as periodo_pgto_tb with(nolock)" & vbNewLine
        sSQL = sSQL & "    on periodo_pgto_tb.periodo_pgto_id = proposta_fechada_tb.periodo_pgto_id" & vbNewLine
        sSQL = sSQL & " or periodo_pgto_tb.periodo_pgto_id = proposta_adesao_tb.periodo_pgto_id" & vbNewLine
        ' MU-2017-054666 - Rog�rio Melo - Confitec SP
        '13/03/2018 atualizando a quantidade de parcelas pagas ate a data de pedido do endosso
      Set rc = rdocn.OpenResultset(sSQL)
       
      
      
      rc.Close
      Set rc = Nothing
      
      comboCarregado = True
    
    Else
        sSQL = ""
    End If

    If TpOperacao = 1 Then
          'Avalia��o T�cnica
          'sSQL = "Select count(1) as ProdTotal, Produto_id,Produto  from #avaliacao_restituicao_tb  where situacao in ('p','i') group by Produto,Produto_id" 'FLAVIO.BEZERRA - 10/06/2013
          sSQL = "Select count(1) as ProdTotal, Produto_id,Produto  from #avaliacao_restituicao_tb  where situacao in ('p','i','a') group by Produto,Produto_id" 'FLAVIO.BEZERRA - 10/06/2013
    Else
          'Avalia��o Gerencial
          sSQL = "Select count(1) as ProdTotal, Produto_id,Produto from #avaliacao_restituicao_tb  where situacao in ('t') group by Produto,Produto_id"
    End If
      
     
    Set rc = rdocn.OpenResultset(sSQL)
    Do While Not rc.EOF
        CmbProduto.AddItem UCase(rc("Produto")) & " - " & rc("ProdTotal") & " endosso(s)"
        CmbProduto.ItemData(CmbProduto.NewIndex) = rc("produto_id")
        rc.MoveNext
    Loop
    rc.Close
    Set rc = Nothing
        
End Sub
'Alan Neves - C_ANEVES - Demanda: 17860335 - 03/06/2013 - Fim


Private Sub optProposta_Click()
    CmbProduto.ListIndex = -1
End Sub

Private Sub optSintetico_Click()
    'txtInicial.SetFocus
End Sub

Private Sub txtFinal_GotFocus()
    optSintetico.Value = True
End Sub

Private Sub txtFinal_KeyPress(KeyAscii As Integer)
    If KeyAscii = 46 Then KeyAscii = 44
    If KeyAscii = 44 And InStr(txtFinal.Text, ",") <> 0 Then
        KeyAscii = 0
        Exit Sub
    End If
    If (KeyAscii < 48 Or KeyAscii > 57) And KeyAscii <> 8 And KeyAscii <> 44 Then
        KeyAscii = 0
    End If
End Sub

Private Sub txtInicial_GotFocus()
    optSintetico.Value = True
End Sub

Private Sub txtInicial_KeyPress(KeyAscii As Integer)
 If KeyAscii = 46 Then KeyAscii = 44
    If KeyAscii = 44 And InStr(txtInicial.Text, ",") <> 0 Then
        KeyAscii = 0
        Exit Sub
    End If
    If (KeyAscii < 48 Or KeyAscii > 57) And KeyAscii <> 8 And KeyAscii <> 44 Then
        KeyAscii = 0
    End If
End Sub


Private Sub txtIntervalo_GotFocus()
    optSintetico.Value = True
End Sub

Private Sub txtIntervalo_KeyPress(KeyAscii As Integer)
 If KeyAscii = 46 Then KeyAscii = 44
    If KeyAscii = 44 And InStr(txtIntervalo.Text, ",") <> 0 Then
        KeyAscii = 0
        Exit Sub
    End If
    If (KeyAscii < 48 Or KeyAscii > 57) And KeyAscii <> 8 And KeyAscii <> 44 Then
        KeyAscii = 0
    End If
End Sub

Private Sub txtProposta_GotFocus()
    optProposta.Value = True
End Sub

Private Sub txtProposta_KeyPress(KeyAscii As Integer)
      If KeyAscii = 46 Then KeyAscii = 44
    If KeyAscii = 44 Then
        KeyAscii = 0
        Exit Sub
    End If
    If (KeyAscii < 48 Or KeyAscii > 57) And KeyAscii <> 8 And KeyAscii <> 44 Then
        KeyAscii = 0
    End If
End Sub

Private Sub txtQtdeLista_GotFocus()
    optAnalitico.Value = True
End Sub

Private Sub txtQtdeLista_KeyPress(KeyAscii As Integer)
  If KeyAscii = 46 Then KeyAscii = 44
    If KeyAscii = 44 Then
        KeyAscii = 0
        Exit Sub
    End If
    If (KeyAscii < 48 Or KeyAscii > 57) And KeyAscii <> 8 And KeyAscii <> 44 Then
        KeyAscii = 0
    End If
End Sub

Private Sub detalhe_proposta(ByVal proposta_id As Long)
        
    Dim sSQL As String
    Dim rc As rdoResultset
    Dim strMensagem As String
    Dim iProduto_id     As String        'FLAVIO.BEZERRA - 14413025 - ALTERAR O PROCESSO DE ENVIO DO ARQUIVO SEG452 - 24/06/2013

    
    'verifica se a proposta existe
    If TpOperacao = 1 Then
        strMensagem = "t�cnica"
        'sSQL = "select proposta_id from #avaliacao_restituicao_tb where proposta_id = " & CStr(proposta_id) & " and situacao in ('p','i')"         'FLAVIO.BEZERRA - 14413025 - ALTERAR O PROCESSO DE ENVIO DO ARQUIVO SEG452 - 24/06/2013
        sSQL = "select proposta_id,produto_id from #avaliacao_restituicao_tb where proposta_id = " & CStr(proposta_id) & " and situacao in ('p','i','a')"      'FLAVIO.BEZERRA - 14413025 - ALTERAR O PROCESSO DE ENVIO DO ARQUIVO SEG452 - 24/06/2013
    Else
        strMensagem = "gerencial"
        sSQL = "select proposta_id, produto_id from #avaliacao_restituicao_tb where proposta_id = " & CStr(proposta_id) & " and situacao in ('t') "
    End If
    
    Set rc = rdocn.OpenResultset(sSQL)
    
    If rc.EOF Then
        MsgBox "N�o h� pend�ncia de avalia��o " & strMensagem & " de restitui��o para essa proposta!", vbInformation 'mudar mensagem
        rc.Close
        Set rc = Nothing
        Exit Sub
    End If
   
    iProduto_id = rc("Produto_id")
    rc.Close
    Set rc = Nothing
    
    
    'Call AtualizaDadosRestituicao(proposta_id, 0)     'Alan Neves - C_ANEVES - Demanda: 17860335 - 03/06/2013

    FrmEspera.Label1 = "Aguarde, carregando proposta.."
    FrmEspera.Show
       
    'atualizando dados dessa proposta
    
    
'    If Not adesaoFechadaGerado Then
'        Call geraAdesaoFechada
'        adesaoFechadaGerado = True
'    End If
'
'    'preenchendo os campos de tp_endosso_id
'    If Not tpEndossoGerado Then
'        Call geraTpEndosso
'        tpEndossoGerado = True
'    End If
       
    'retorna os dados da proposta - Retorno realizado no inicio desta fun��o
   
    'Alan Neves - C_ANEVES - Demanda: 17860335 - 03/06/2013 - Inicio
    If TpOperacao = 1 Then
        'sSQL = "select * from #avaliacao_restituicao_tb where proposta_id = " & CStr(proposta_id) & " and situacao in ('p','i')"       'FLAVIO.BEZERRA - 14413025 - ALTERAR O PROCESSO DE ENVIO DO ARQUIVO SEG452 - 24/06/2013
        sSQL = " exec seguros_db.dbo.SEGS11179_SPS " & iProduto_id
        'sSQL = sSQL & "select * from #avaliacao_restituicao_tb where proposta_id = " & CStr(proposta_id) & " and situacao in ('p','i','a')"    'FLAVIO.BEZERRA - 14413025 - ALTERAR O PROCESSO DE ENVIO DO ARQUIVO SEG452 - 24/06/2013
        ''
        'Diogo Paes - 17893450  - consulta proposta especifica corre��o de sintaxe - 03/07/2015
        sSQL = sSQL & "select * from #avaliacao_restituicao_tb where proposta_id = " & CStr(proposta_id) & " and situacao in (''p'',''i'',''a'')"
    Else
        'sSQL = "select proposta_bb,proposta_id,num_avaliacao,dt_pedido_avaliacao,dt_contratacao,dt_inicio_vigencia,dt_fim_vigencia,origem,tipoAvaliacao, produto_id from #avaliacao_restituicao_tb where proposta_id = " & CStr(proposta_id) & " and situacao in ('t')"
        'Diogo Paes - 17893450  - consulta proposta especifica inclus�o de campo COR no select - 03/07/2015 object collection
        sSQL = "select cor, proposta_bb,proposta_id,num_avaliacao,dt_pedido_avaliacao,dt_contratacao,dt_inicio_vigencia,dt_fim_vigencia,origem,tipoAvaliacao, produto_id from #avaliacao_restituicao_tb where proposta_id = " & CStr(proposta_id) & " and situacao in (''t'')"
    End If
        'Alan Neves - C_ANEVES - Demanda: 17860335 - 03/06/2013 - Fim
    'Set rc = rdocn.OpenResultset(sSQL) 'Alan Neves - C_ANEVES - Demanda: 17860335 - 03/06/2013
    Set rc = AtualizaDadosRestituicao(proposta_id, 0, sSQL)  ' Alan Neves - C_ANEVES - Demanda: 17860335 - 03/06/2013
     
    'Declara��es
    Dim lPropostaBB As Long
    Dim lAvaliacao As Long
    Dim sSituacao As String
    Dim sDtPedidoAvaliacao As String
    Dim sDtContratacao As String
    Dim sDtInicioVigencia As String
    Dim sDtFimVigencia As String
    Dim lPropostaId As Long
    Dim sRamoId As String
    
    Dim cValIS As Currency
    Dim cValPremio As Currency
    Dim cValTotIOFPago As Currency
    Dim cValTotPremioPago As Currency
    Dim cValTotJurosPago As Currency
    Dim cValTotAdicFracionamentoPago As Currency
    Dim cValTotDescontoPago As Currency
    Dim cValCustoApoliceCertif As Currency
    
    Dim iPrazoRestituicaoIntegral As Integer
    Dim sTpCalcRestituicao As String
    
    Dim sPossuiSubvencao As String  'Rmarins - inclu�dos em 20/10/2006
    Dim cValSubvencaoFederal As Currency
    Dim cValSubvencaoEstadual As Currency
    Dim cValSubvencaoFederalEst As Currency
    Dim cValSubvencaoEstadualEst As Currency
    
    Dim cValCustoApoliceCertifEst As Currency
    Dim cValAdicFracionamentoEst As Currency
    Dim cValDescontoEst As Currency
    Dim cValIOFEst As Currency
    Dim cValPremioTarifaEst As Currency
    Dim cValRestituicaoEst As Currency
    Dim cValTotCorretagemEst As Currency
    Dim cValProLaboreEst As Currency
    
    Dim cValCustoApoliceCertifRest As Currency
    Dim cValAdicFracionamentoRest As Currency
    Dim cValDescontoRest As Currency
    Dim cValIOFRest As Currency
    Dim cValPremioTarifaRest As Currency
    Dim cValRestituicao As Currency
    Dim cValTotCorretagemRest As Currency
    Dim cValProLaboreRest As Currency
    Dim cPercIR As Currency
    
    'mathayde 27/03/2009
    Dim cValRemuneracao As Currency
    Dim cValRemuneracaoEst As Currency


    Dim OPropostaModular As Object
    Dim rs As Recordset
    Dim lPropostaModular As Long
    
    
    Dim lEndossoId As Long
        
    Dim strCor      As String 'FLAVIO.BEZERRA - 14413025 - ALTERAR O PROCESSO DE ENVIO DO ARQUIVO SEG452 - 24/06/2013

        
    On Error GoTo TrataErro
    
    MousePointer = vbHourglass
    
    strCor = rc("Cor") 'FLAVIO.BEZERRA - 14413025 - ALTERAR O PROCESSO DE ENVIO DO ARQUIVO SEG452 - 24/06/2013

    
    Produto_id = rc("produto_id")
    iOrigem = rc("origem")
        
    lPropostaBB = VerNullNumerico(rc("proposta_bb"))
    lPropostaId = rc("proposta_id")
    lAvaliacao = rc("num_avaliacao")
    
    
    If Produto_id = 1168 Then
        lAvaliacaoModular = lAvaliacao
    End If

    ' C_ANEVES - 26/08/2013 - Demanda: 17860335 - Inicio
    Set OPropostaModular = CreateObject("SEGL0026.cls00124")
    'Set OPropostaModular = CreateObject("SEGL0355.cls00626")
    ' C_ANEVES - 26/08/2013 - Demanda: 17860335 - Fim
    lPropostaModular = OPropostaModular.ObterPropostaModular(gsSIGLASISTEMA, _
                                                             App.Title, _
                                                             App.FileDescription, _
                                                             glAmbiente_id, _
                                                             lPropostaId, _
                                                             IIf(TpOperacao = 1, SITUACAO_PENDENTE_AVALIACAO_TECNICA, SITUACAO_PENDENTE_AVALIACAO_GERENCIAL))
                                                             

    
    If lPropostaModular = 0 And Produto_id <> 1168 Then
        bModular = False
        propostaModular = 0
    Else
        Call DadosModular(lPropostaId, lPropostaModular)
    End If
       
 
    sDtPedidoAvaliacao = rc("dt_pedido_avaliacao")
    sDtContratacao = rc("dt_contratacao")
    sDtInicioVigencia = rc("dt_inicio_vigencia")
    If Not IsNull(rc("dt_fim_vigencia")) Then
        sDtFimVigencia = rc("dt_fim_vigencia")
    End If
    
    Origem = rc("origem")
    
    FrmAvaliacao.lblTipoAvaliacao = "Avalia��o de Restitui��o"
    
    FrmAvaliacao.lblDtLiq.Visible = False
    
       
    FrmAvaliacao.txtTipoEndosso.Text = Trim(rc("tipoAvaliacao"))
    FrmAvaliacao.txtTpEmdossoRest.Text = Trim(rc("tipoAvaliacao"))
    
    If bModular Then
        FrmAvaliacao.txtTpEmdossoRestModular.Text = Trim(rc("tipoAvaliacao"))
    End If
    
    rc.Close
    Set rc = Nothing

    Call Obter_Dados_Adicionais_Proposta(lPropostaId, sRamoId, cValCustoApoliceCertif)

    bDtCancelamentoValida = True

    If Verificar_Uso_Proposta(lPropostaId) Then
        MsgBox "A proposta do endosso est� sendo utilizada por outro usu�rio"
        MousePointer = vbDefault
        Call Unload(FrmEspera)
        Exit Sub
    End If

    If VerificarExistenciaAvaliacaoRestituicaoPendente(lPropostaId, lAvaliacao, TpOperacao) Then
        MsgBox "Endosso j� avaliado."
        'FrmSelEndosso.Marcar_Linha_Processada
        MousePointer = vbDefault
        Exit Sub
    End If

    Call Atualizar_Bloqueio_Proposta(lPropostaId, "s")
      
    Call Obter_Premio_IS(lPropostaId, Produto_id, cValIS, cValPremio)
    If lPropostaBB <> 0 Then
        Call Carregar_Colecao_Cobrancas(lPropostaId)
    End If
    Call Obter_Totais_Pagos(cValTotPremioPago, cValTotJurosPago, cValTotAdicFracionamentoPago, cValTotDescontoPago, cValTotIOFPago)
    Call Obter_Dados_Produto(Produto_id, iPrazoRestituicaoIntegral, sTpCalcRestituicao, sPossuiSubvencao)  'Rmarins - 20/10/06
    
    
    
    
    gcValTotPremioPago = cValTotPremioPago
    gcValTotJurosPago = cValTotJurosPago
    gcValTotAdicFracionamentoPago = cValTotAdicFracionamentoPago
    gcValTotDescontoPago = cValTotDescontoPago
    gcValTotIOFPago = cValTotIOFPago
    gcValCustoApoliceCertif = cValCustoApoliceCertif
    gcValTotPremioTarifaPago = cValTotPremioPago - cValTotAdicFracionamentoPago - cValTotIOFPago - cValCustoApoliceCertif + cValTotDescontoPago
    gsPossuiSubvencao = UCase(sPossuiSubvencao)
    
    
    Call Obtem_Perc_IR(sRamoId, Data_Sistema, cPercIR)

    
    If TpOperacao = 1 Then
        sSituacao = SITUACAO_PENDENTE_AVALIACAO_TECNICA
    Else
        sSituacao = SITUACAO_PENDENTE_AVALIACAO_GERENCIAL
    End If
    
    Call ObterValoresAvaliacaoRestituicao(lPropostaId, _
                                          lAvaliacao, _
                                          sSituacao, _
                                          cValRestituicao, _
                                          cValIOFRest, _
                                          cValCustoApoliceCertifRest, _
                                          cValAdicFracionamentoRest, _
                                          cValDescontoRest, _
                                          cValPremioTarifaRest, _
                                          cValTotCorretagemRest, _
                                          cValProLaboreRest, _
                                          cValCustoApoliceCertifEst, _
                                          cValAdicFracionamentoEst, _
                                          cValDescontoEst, _
                                          cValIOFEst, _
                                          cValPremioTarifaEst, _
                                          cValRestituicaoEst, _
                                          cValTotCorretagemEst, _
                                          cValProLaboreEst, _
                                          lEndossoId, _
                                          0, "", _
                                          cValSubvencaoFederal, cValSubvencaoEstadual, _
                                          cValSubvencaoFederalEst, cValSubvencaoEstadualEst, _
                                          cValRemuneracao, cValRemuneracaoEst)
                                          'Mathayde 30/03/2009 - obtem valores de cValRemuneracao e cValRemuneracaoEst

    
    Call CarregarRateioCorretagemRestituicao(lPropostaId, _
                                             lAvaliacao, _
                                             cValRestituicao, _
                                             cValIOFRest, _
                                             cValCustoApoliceCertifRest, _
                                             cPercIR)
                                             
    
    Call CarregarCongeneres(lPropostaId)
    
    
    
    FrmAvaliacao.dPercCorretagemRest = ObterPercentualTotalCorretagem(lPropostaId)

    
    If gsPossuiSubvencao = "S" And bModular = False Then
       
        FrmAvaliacao.lblEstadualCalc.Visible = True
        FrmAvaliacao.lblValSubvencaoEstadualCalc.Visible = True
        FrmAvaliacao.lblEstadual.Visible = True
        FrmAvaliacao.maskValSubvencaoEstadual.Visible = True
        FrmAvaliacao.lblFederalCalc.Visible = True
        FrmAvaliacao.lblValSubvencaoFederalCalc.Visible = True
        FrmAvaliacao.lblFederal.Visible = True
        FrmAvaliacao.lblValSubvencaoFederal.Visible = True

        Call ObterValoresSubvencao(lPropostaId, gcValTotPremioTarifaPago)
    
        
    End If

    
'    If Not alteraEmLote Then
'        Me.Hide
'    End If

    Call Unload(Me)
        
    FrmAvaliacao.Mask2ValPremioTarifa.Mask = "12V2"
    FrmAvaliacao.Mask2ValPremioTarifa.Text = "0,00"
    FrmAvaliacao.formAnterior = "frmFiltroPesquisa"
    FrmAvaliacao.idProduto = Produto_id
    FrmAvaliacao.Show
    If TpOperacao = 1 Then
        FrmAvaliacao.Caption = "SEGP0209 - Avalia��o T�cnica de Restitui��o - " & Ambiente
    Else
        FrmAvaliacao.Caption = "SEGP0209 - Avalia��o Gerencial de Restitui��o - " & Ambiente
    End If

    FrmAvaliacao.Mask2ValPremioTarifa.Enabled = True
 
    
    FrmAvaliacao.Ler_Dados_Proposta lPropostaId
    
    Call ConsultarPercIOF(sDtInicioVigencia, _
                          FrmAvaliacao.txtSubramoId)
    
    
    FrmAvaliacao.Preencher_GridCobrancas
    FrmAvaliacao.Ler_Endossos lPropostaId

    
    FrmAvaliacao.LerAvaliacoesRestituicao lPropostaId

    If Trim(sDtFimVigencia) <> "" Then
        FrmAvaliacao.LblPrazoSeguro.Caption = DateDiff("d", sDtInicioVigencia, sDtFimVigencia)
    Else
        FrmAvaliacao.LblPrazoSeguro.Caption = ""
    End If
    
    If bModular = True Then
        FrmAvaliacao.lblValParametroRestituicao(1).Caption = FrmAvaliacao.LblPrazoSeguro.Caption
    
        'modular
        If Trim(oDadosModular.Dt_Fim_Vigencia) <> "" Then
            FrmAvaliacao.lblValParametroRestituicao(9).Caption = DateDiff("d", oDadosModular.Dt_Inicio_Vigencia, oDadosModular.Dt_Fim_Vigencia)
        Else
            FrmAvaliacao.lblValParametroRestituicao(9).Caption = ""
        End If
    End If
    
    FrmAvaliacao.LblDiasUtilizacaoSeguro.Caption = DateDiff("d", sDtInicioVigencia, sDtPedidoAvaliacao)
    
    If bModular = True Then
        FrmAvaliacao.lblValParametroRestituicao(5).Caption = FrmAvaliacao.LblDiasUtilizacaoSeguro.Caption
        FrmAvaliacao.lblValParametroRestituicao(13).Caption = DateDiff("d", oDadosModular.Dt_Inicio_Vigencia, oDadosModular.Dt_Pedido_Avaliacao)
    End If
    
    FrmAvaliacao.fraDados.Caption = "Dados da Restitui��o"
    FrmAvaliacao.TxtPropostaBB.Text = lPropostaBB
    FrmAvaliacao.lblTexto(13).Caption = "Nr.Avalia��o:"
    FrmAvaliacao.TxtEndossoBB.Text = lAvaliacao
    FrmAvaliacao.lblDtCanc.Caption = "Dt.Pedido:"
    FrmAvaliacao.lblDtLiq.Visible = False
    FrmAvaliacao.TxtDtCancelamentoBB.Text = sDtPedidoAvaliacao
    FrmAvaliacao.lblDtReceb.Visible = False
    FrmAvaliacao.TxtDtRecebimentoEndosso.Visible = False
        
    FrmAvaliacao.txtProposta.Text = lPropostaId
    
    If bModular = True Then
        FrmAvaliacao.txtpropostaModular(0).Text = lPropostaId
        FrmAvaliacao.txtpropostaModular(1).Text = oDadosModular.proposta_id
    End If
    
    FrmAvaliacao.txtIs.Text = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValIS))
    FrmAvaliacao.txtPremio.Text = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValPremio))

    If TpOperacao = 1 Then  'Avaliacao Tecnica
        FrmAvaliacao.Mask2ValPremioTarifa.Locked = False
        FrmAvaliacao.Mask2ValPremioTarifaModular(0).Locked = False
        FrmAvaliacao.Mask2ValPremioTarifaModular(1).Locked = False
    Else
        FrmAvaliacao.Mask2ValPremioTarifa.Locked = True
        FrmAvaliacao.Mask2ValPremioTarifaModular(0).Locked = True
        FrmAvaliacao.Mask2ValPremioTarifaModular(1).Locked = True
        FrmAvaliacao.maskValSubvencaoEstadual.Locked = True
    End If
    
    'Exibindo dados necess�rios para restitui��o''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    FrmAvaliacao.LblTotValPremioPago.Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValTotPremioPago))
    FrmAvaliacao.LblTotValJuros.Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValTotJurosPago))
    FrmAvaliacao.LblCustoCertifApolice.Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValCustoApoliceCertif))
    FrmAvaliacao.LblValPremioEmitido.Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValPremio))

    If bModular = True Then
        FrmAvaliacao.lblValParametroRestituicao(2).Caption = FrmAvaliacao.LblTotValPremioPago.Caption 'Total Premio Pago
        FrmAvaliacao.lblValParametroRestituicao(3).Caption = FrmAvaliacao.LblTotValJuros.Caption 'Total Juros Pago
        FrmAvaliacao.lblValParametroRestituicao(6).Caption = FrmAvaliacao.LblValPremioEmitido.Caption 'Premio Emitido
        FrmAvaliacao.lblValParametroRestituicao(7).Caption = FrmAvaliacao.LblCustoCertifApolice.Caption 'Custo Certificacao
        
        
        FrmAvaliacao.lblValParametroRestituicao(10).Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(oDadosModular.Val_Premio_Pago)) 'Total Premio Pago
        FrmAvaliacao.lblValParametroRestituicao(11).Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(oDadosModular.Val_Juros)) 'Total Juros Pago
        FrmAvaliacao.lblValParametroRestituicao(14).Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(oDadosModular.Val_Premio)) 'Premio Emitido
        FrmAvaliacao.lblValParametroRestituicao(15).Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(oDadosModular.Custo_Apolice)) 'Custo Certificacao
    End If

    'Exibindo valor estimado de restitui��o'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    FrmAvaliacao.LblValRestituicaoEstimado = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValRestituicaoEst))
    If bModular = True Then
        FrmAvaliacao.lblestimativaModular(0).Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValRestituicaoEst))
        FrmAvaliacao.lblestimativaModular(1).Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(oDadosModular.ValRestituicaoEst))
        If Produto_id = 1168 Then
            FrmAvaliacao.LblValRestituicaoEstimado = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValRestituicaoEst + oDadosModular.ValRestituicaoEst))
        End If
    End If
    
    'Exibindo valores calculados da restitui��o'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    FrmAvaliacao.LblValPremioTarifaCalc.Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValPremioTarifaRest))
    FrmAvaliacao.LblCustoCertifApoliceCalc.Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValCustoApoliceCertifRest))
    FrmAvaliacao.LblValIOFCalc.Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValIOFRest))
    FrmAvaliacao.LblValAdicFracionamentoCalc.Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValAdicFracionamentoRest))
    FrmAvaliacao.LblValDescontoCalc.Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValDesconto))
    FrmAvaliacao.LblValRestituicaoCalc.Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValRestituicao))
    If bModular = True Then
        'Premio Tarifa
        FrmAvaliacao.lblvalCalculadoModular(0).Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValPremioTarifaEst))
        FrmAvaliacao.lblvalCalculadoModular(17).Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(oDadosModular.ValPremioTarifaEst))
        'Custo Certificado
        FrmAvaliacao.lblvalCalculadoModular(1).Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValCustoApoliceCertifEst))
        FrmAvaliacao.lblvalCalculadoModular(18).Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(oDadosModular.ValCustoApoliceCertifEst))
        'Adic. Fracionamento
        FrmAvaliacao.lblvalCalculadoModular(2).Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValAdicFracionamentoEst))
        FrmAvaliacao.lblvalCalculadoModular(19).Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(oDadosModular.ValAdicFracionamentoEst))
        'Desconto
        FrmAvaliacao.lblvalCalculadoModular(3).Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValDescontoEst))
        FrmAvaliacao.lblvalCalculadoModular(20).Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(oDadosModular.ValDescontoEst))
        'IOF
        FrmAvaliacao.lblvalCalculadoModular(4).Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValIOFEst))
        FrmAvaliacao.lblvalCalculadoModular(21).Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(oDadosModular.ValIOFEst))
        'Valor Devolu��o
        FrmAvaliacao.lblvalCalculadoModular(5).Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValRestituicaoEst))
        FrmAvaliacao.lblvalCalculadoModular(22).Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(oDadosModular.ValRestituicaoEst))
        
        If Produto_id = 1168 Then
            FrmAvaliacao.LblValPremioTarifaCalc.Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValPremioTarifaEst + oDadosModular.ValPremioTarifaEst))
            FrmAvaliacao.LblCustoCertifApoliceCalc.Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValCustoApoliceCertifEst + oDadosModular.ValCustoApoliceCertifEst))
            FrmAvaliacao.LblValIOFCalc.Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValIOFEst + oDadosModular.ValIOFEst))
            FrmAvaliacao.LblValAdicFracionamentoCalc.Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValAdicFracionamentoEst + oDadosModular.ValAdicFracionamentoEst))
            FrmAvaliacao.LblValDescontoCalc.Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValDescontoEst + oDadosModular.ValDescontoEst))
            FrmAvaliacao.LblValRestituicaoCalc.Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValRestituicaoEst + oDadosModular.ValRestituicaoEst))
        End If
        
    End If
    'Propondo valores da restitui��o informados''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    FrmAvaliacao.Mask2ValPremioTarifa.Text = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValPremioTarifaRest))
    FrmAvaliacao.LblCustoCertifApoliceRest.Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValCustoApoliceCertifRest))
    FrmAvaliacao.LblValIOF.Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValIOFRest))
    FrmAvaliacao.LblValAdicFracionamento.Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValAdicFracionamentoRest))
    FrmAvaliacao.LblValDesconto.Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValDescontoRest))
    FrmAvaliacao.LblValRestituicao.Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValRestituicao))
    
    If bModular = True Then
        'Premio Tarifa
        FrmAvaliacao.Mask2ValPremioTarifaModular(0).Text = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValPremioTarifaRest))
        FrmAvaliacao.Mask2ValPremioTarifaModular(1).Text = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(oDadosModular.ValPremioTarifaRest))
        'Custo Certificado
        FrmAvaliacao.lblvalCalculadoModular(9).Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValCustoApoliceCertifRest))
        FrmAvaliacao.lblvalCalculadoModular(26).Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(oDadosModular.ValCustoApoliceCertifRest))
        'Adic. Fracionamento
        FrmAvaliacao.lblvalCalculadoModular(10).Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValAdicFracionamentoRest))
        FrmAvaliacao.lblvalCalculadoModular(27).Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(oDadosModular.ValAdicFracionamentoRest))
        'Desconto
        FrmAvaliacao.lblvalCalculadoModular(11).Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValDescontoRest))
        FrmAvaliacao.lblvalCalculadoModular(28).Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(oDadosModular.ValDescontoRest))
        'IOF
        FrmAvaliacao.lblvalCalculadoModular(12).Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValIOFRest))
        FrmAvaliacao.lblvalCalculadoModular(29).Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(oDadosModular.ValIOFRest))
        'Valor Devolu��o
        FrmAvaliacao.lblvalCalculadoModular(13).Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValRestituicao))
        FrmAvaliacao.lblvalCalculadoModular(30).Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(oDadosModular.ValRestituicao))
        
        If Produto_id = 1168 Then
            FrmAvaliacao.Mask2ValPremioTarifa.Text = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValPremioTarifaRest + oDadosModular.ValPremioTarifaRest))
            FrmAvaliacao.LblCustoCertifApoliceRest.Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValCustoApoliceCertifRest + oDadosModular.ValCustoApoliceCertifRest))
            FrmAvaliacao.LblValIOF.Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValIOFRest + oDadosModular.ValIOFRest))
            FrmAvaliacao.LblValAdicFracionamento.Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValAdicFracionamentoRest + oDadosModular.ValAdicFracionamentoRest))
            FrmAvaliacao.LblValDesconto.Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValDescontoRest + oDadosModular.ValDescontoRest))
            FrmAvaliacao.LblValRestituicao.Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValRestituicao + oDadosModular.ValRestituicao))
        End If
        
    End If

    'Exibindo valores calculados de comiss�o '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    FrmAvaliacao.LblTotValCorretagemCalc.Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValTotCorretagemEst))
    FrmAvaliacao.LblValProLaboreCalc.Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValProLaboreEst))
    'mathayde 27/03/2009 - Exibe a remunera��o
    FrmAvaliacao.LblValRemuneracaoCalc.Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValRemuneracaoEst))
    'Exibindo valores comiss�o Informados '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    FrmAvaliacao.LblTotValCorretagem.Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValTotCorretagemRest))
    FrmAvaliacao.LblValProLabore.Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValProLaboreRest))
    'mathayde 27/03/2009 - Exibe a remunera��o
    FrmAvaliacao.LblValRemuneracao.Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValRemuneracao))
    
    If bModular = True Then
        FrmAvaliacao.lblvalCalculadoModular(6).Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValTotCorretagemEst))
        FrmAvaliacao.lblvalCalculadoModular(23).Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(oDadosModular.Val_Tot_Corretagem_Est))
        FrmAvaliacao.lblvalCalculadoModular(7).Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValProLaboreEst))
        FrmAvaliacao.lblvalCalculadoModular(24).Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(oDadosModular.Val_Pro_Lab_Est))
        FrmAvaliacao.lblvalCalculadoModular(8).Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValRemuneracaoEst))
        FrmAvaliacao.lblvalCalculadoModular(25).Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(oDadosModular.Val_Remuneracao_Est))
        
        
        FrmAvaliacao.lblvalCalculadoModular(14).Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValTotCorretagemRest))
        FrmAvaliacao.lblvalCalculadoModular(31).Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(oDadosModular.ValTotCorretagemRest))
        FrmAvaliacao.lblvalCalculadoModular(15).Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValProLaboreRest))
        FrmAvaliacao.lblvalCalculadoModular(32).Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(oDadosModular.ValProLaboreRest))
        FrmAvaliacao.lblvalCalculadoModular(16).Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValRemuneracao))
        FrmAvaliacao.lblvalCalculadoModular(33).Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(oDadosModular.Val_Remuneracao))
        
        If Produto_id = 1168 Then
            FrmAvaliacao.LblTotValCorretagemCalc.Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValTotCorretagemEst + oDadosModular.Val_Tot_Corretagem_Est))
            FrmAvaliacao.LblValProLaboreCalc.Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValProLaboreEst + oDadosModular.Val_Pro_Lab_Est))
            FrmAvaliacao.LblValRemuneracaoCalc.Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValRemuneracaoEst + oDadosModular.Val_Remuneracao_Est))
            FrmAvaliacao.LblTotValCorretagem.Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValTotCorretagemRest + oDadosModular.ValTotCorretagemRest))
            FrmAvaliacao.LblValProLabore.Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValProLaboreRest + oDadosModular.ValProLaboreRest))
            FrmAvaliacao.LblValRemuneracao.Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValRemuneracao + oDadosModular.Val_Remuneracao))
        End If
        
    End If
    
    
    If bModular = False Then
        'Exibindo valores calculados de subven��o ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        FrmAvaliacao.lblValSubvencaoEstadualCalc.Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValSubvencaoEstadualEst))
        FrmAvaliacao.lblValSubvencaoFederalCalc.Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValSubvencaoFederalEst))
        
        'Exibindo valores de subven��o informados ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        FrmAvaliacao.maskValSubvencaoEstadual.Mask = "12V2"
        FrmAvaliacao.maskValSubvencaoEstadual.Text = "0,00"
        FrmAvaliacao.maskValSubvencaoEstadual.Text = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValSubvencaoEstadual))
        FrmAvaliacao.lblValSubvencaoFederal.Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValSubvencaoFederal))
    End If
    
    
    
    'Exibindo valores calculados de subven��o ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    FrmAvaliacao.lblValSubvencaoEstadualCalc.Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValSubvencaoEstadualEst))
    FrmAvaliacao.lblValSubvencaoFederalCalc.Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValSubvencaoFederalEst))
    
    'Exibindo valores de subven��o informados ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    FrmAvaliacao.maskValSubvencaoEstadual.Mask = "12V2"
    FrmAvaliacao.maskValSubvencaoEstadual.Text = "0,00"
    FrmAvaliacao.maskValSubvencaoEstadual.Text = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValSubvencaoEstadual))
    FrmAvaliacao.lblValSubvencaoFederal.Caption = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(cValSubvencaoFederal))

    'Configurando interface'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    FrmAvaliacao.Definir_Exibicao_Objetos

    FrmAvaliacao.fraSemRestituicao.Visible = False

    If bModular = False Then
        FrmAvaliacao.SSTabDados.TabVisible(5) = False
        FrmAvaliacao.SSTabDados.TabVisible(4) = True
        FrmAvaliacao.SSTabDados.Tab = 4


    Else
        FrmAvaliacao.SSTabDados.TabVisible(4) = False
        FrmAvaliacao.SSTabDados.TabVisible(5) = True
        FrmAvaliacao.SSTabDados.Tab = 5
    End If
        
    'Identificar se a proposta para a qual esta sendo atribuindo o valor � a de RE ou a de AP.
    If bModular = True Then
        Dim OPropModular As Object
        Dim Tipo As String

        Set OPropModular = CreateObject("SEGL0026.cls00125")
    
        If sDtInicioEsc = "" Or sDtInicioEsc = "" Then
            Call OPropModular.ObterParametroRestituicao(gsSIGLASISTEMA, _
                                                        App.Title, _
                                                        App.FileDescription, _
                                                        glAmbiente_id, _
                                                        lPropostaId, oDadosModular.proposta_id, _
                                                        sDtInicioEsc, _
                                                        sDtFimEsc)
                                                        
            
            oDadosModular.sDtInicioVigenciaEsc = sDtInicioEsc
            oDadosModular.sDtFimVigenciaEsc = sDtFimEsc
        
        End If
        
        
        If Produto_id <> 1168 Then

            ' C_ANEVES - 26/08/2013 - Demanda: 17860335 - Inicio
            Set OPropModular = CreateObject("SEGL0026.cls00124")
            'Set OPropModular = CreateObject("SEGL0355.cls00626")
            ' C_ANEVES - 26/08/2013 - Demanda: 17860335 - Fim
        
            FrmAvaliacao.lbltipoProposta(0).AutoSize = True
            FrmAvaliacao.lbltipoProposta(1).AutoSize = True
        
            FrmAvaliacao.lbltipoProposta(0).Caption = ""
            FrmAvaliacao.lbltipoProposta(1).Caption = ""
        
            
            Tipo = OPropModular.IdentificaValorRestituicao(gsSIGLASISTEMA, _
                                                           App.Title, _
                                                           App.FileDescription, _
                                                           glAmbiente_id, _
                                                           lPropostaId)
            
            FrmAvaliacao.lbltipoProposta(0).Caption = "Tipo: " & Tipo
            
            
            Tipo = OPropModular.IdentificaValorRestituicao(gsSIGLASISTEMA, _
                                                                 App.Title, _
                                                                 App.FileDescription, _
                                                                 glAmbiente_id, _
                                                                 oDadosModular.proposta_id)
            
            FrmAvaliacao.lbltipoProposta(1).Caption = "Tipo: " & Tipo
        Else
            'If lAvaliacao > oDadosModular.Num_avaliacao Then
            If oDadosModular.iTpCobertura = 526 Then
                FrmAvaliacao.lbltipoProposta(0).Caption = "Tipo: MIP"
                FrmAvaliacao.lbltipoProposta(1).Caption = "Tipo: DFI"
            Else
                FrmAvaliacao.lbltipoProposta(0).Caption = "Tipo: DFI"
                FrmAvaliacao.lbltipoProposta(1).Caption = "Tipo: MIP"
            End If
        
        End If
    End If
    
    'FLAVIO.BEZERRA - 14413025 - ALTERAR O PROCESSO DE ENVIO DO ARQUIVO SEG452 - 24/06/2013
    If strCor = "A" Then
        FrmAvaliacao.btnAceiteTecnico.Enabled = False
        FrmAvaliacao.btnAprovar.Enabled = False
        FrmAvaliacao.BtnRetornoParaAvaliacaoTecnica.Enabled = False
        FrmAvaliacao.BtnRecusar.Enabled = False
    End If
    'FLAVIO.BEZERRA - 14413025 - ALTERAR O PROCESSO DE ENVIO DO ARQUIVO SEG452 - 24/06/2013

    MousePointer = vbDefault
    Call Unload(FrmEspera)
    Exit Sub

TrataErro:
    Call TrataErroGeral("ExibirDetalhesRestituicao", Me.name)
    Call TerminaSEGBR
    

End Sub


Public Sub ObterValoresAvaliacaoRestituicao(ByVal lPropostaId As Long, _
                                            ByVal lAvaliacao As Long, _
                                            ByVal sSituacao As String, _
                                            ByRef cValRestituicao As Currency, _
                                            ByRef cValIOFRest As Currency, _
                                            ByRef cValCustoApoliceCertifRest As Currency, _
                                            ByRef cValAdicFracionamentoRest As Currency, _
                                            ByRef cValDescontoRest As Currency, _
                                            ByRef cValPremioTarifaRest As Currency, _
                                            ByRef cValTotCorretagemRest As Currency, _
                                            ByRef cValProLaboreRest As Currency, _
                                            ByRef cValCustoApoliceCertifEst As Currency, _
                                            ByRef cValAdicFracionamentoEst As Currency, _
                                            ByRef cValDescontoEst As Currency, _
                                            ByRef cValIOFEst As Currency, _
                                            ByRef cValPremioTarifaEst As Currency, _
                                            ByRef cValRestituicaoEst As Currency, _
                                            ByRef cValTotCorretagemEst As Currency, _
                                            ByRef cValProLaboreEst As Currency, _
                                            ByRef lEndossoId As Long, _
                                            Optional ByRef cValCambio As Currency, _
                                            Optional ByRef sTextoEndosso As String, _
                                            Optional ByRef cValSubvencaoFederal As Currency, Optional ByRef cValSubvencaoEstadual As Currency, _
                                            Optional ByRef cValSubvencaoFederalEst As Currency, Optional ByRef cValSubvencaoEstadualEst As Currency, _
                                            Optional ByRef cValRemuneracao As Currency, Optional ByRef cValRemuneracaoEst As Currency)
                                            'Mathayde 30/03/2009 - Recebe cValRemuneracao e cValRemuneracaoEst do Banco

' bcarneiro - 02/12/2003 - Projeto Restitui��o por Iniciativa da Seguradora

Dim oFinanceiro As Object
Dim rs As Object

On Error GoTo TrataErro

    Set oFinanceiro = CreateObject("SEGL0026.cls00125")
    
    Set rs = oFinanceiro.ConsultarAvaliacaoRestituicao(gsSIGLASISTEMA, _
                                                       App.Title, _
                                                       App.FileDescription, _
                                                       glAmbiente_id, _
                                                       sSituacao, _
                                                       , _
                                                       lPropostaId, _
                                                       lAvaliacao)

    Set oFinanceiro = Nothing

    If Not rs.EOF Then
  If TpOperacao = 1 Then

            cValRestituicao = IIf(IsNull(rs("val_restituicao_estimado")), 0, rs("val_restituicao_estimado"))
            cValIOFRest = IIf(IsNull(rs("val_iof_estimado")), 0, rs("val_iof_estimado"))
            cValCustoApoliceCertifRest = IIf(IsNull(rs("custo_apolice_estimado")), 0, rs("custo_apolice_estimado"))
            cValAdicFracionamentoRest = IIf(IsNull(rs("val_adic_fracionamento_estimado")), 0, rs("val_adic_fracionamento_estimado"))
            cValDescontoRest = IIf(IsNull(rs("val_desconto_comercial_estimado")), 0, rs("val_desconto_comercial_estimado"))
            cValPremioTarifaRest = IIf(IsNull(rs("val_premio_tarifa_estimado")), 0, rs("val_premio_tarifa_estimado"))
            cValTotCorretagemRest = IIf(IsNull(rs("val_comissao_estimado")), 0, rs("val_comissao_estimado"))
            cValProLaboreRest = IIf(IsNull(rs("val_comissao_estipulante_estimado")), 0, rs("val_comissao_estipulante_estimado"))

            cValRestituicaoEst = IIf(IsNull(rs("val_restituicao_estimado")), 0, rs("val_restituicao_estimado"))
            cValIOFEst = IIf(IsNull(rs("val_iof_estimado")), 0, rs("val_iof_estimado"))
            cValCustoApoliceCertifEst = IIf(IsNull(rs("custo_apolice_estimado")), 0, rs("custo_apolice_estimado"))
            cValAdicFracionamentoEst = IIf(IsNull(rs("val_adic_fracionamento_estimado")), 0, rs("val_adic_fracionamento_estimado"))
            cValDescontoEst = IIf(IsNull(rs("val_desconto_comercial_estimado")), 0, rs("val_desconto_comercial_estimado"))
            cValPremioTarifaEst = IIf(IsNull(rs("val_premio_tarifa_estimado")), 0, rs("val_premio_tarifa_estimado"))
            cValTotCorretagemEst = IIf(IsNull(rs("val_comissao_estimado")), 0, rs("val_comissao_estimado"))
            cValProLaboreEst = IIf(IsNull(rs("val_comissao_estipulante_estimado")), 0, rs("val_comissao_estipulante_estimado"))
            
            'mathayde 27/03/2009 - Obtem o valor da remunera��o
            cValRemuneracaoEst = IIf(IsNull(rs("val_remuneracao_estimado")), 0, rs("val_remuneracao_estimado"))
            cValRemuneracao = IIf(IsNull(rs("val_remuneracao_estimado")), 0, rs("val_remuneracao_estimado"))
            

            cValCambio = IIf(IsNull(rs("val_cambio")), 0, rs("val_cambio"))
            If Not IsNull(rs("descricao")) Then
                sTextoEndosso = Trim(rs("descricao"))
            Else
                sTextoEndosso = ""
            End If
            lEndossoId = IIf(IsNull(rs("endosso_id")), 0, rs("endosso_id"))
            
            'Rmarins - 20/10/06
            cValSubvencaoFederal = IIf(IsNull(rs("val_subvencao_federal_estimado")), 0, rs("val_subvencao_federal_estimado"))
            cValSubvencaoEstadual = IIf(IsNull(rs("val_subvencao_estadual_estimado")), 0, rs("val_subvencao_estadual_estimado"))
            cValSubvencaoFederalEst = IIf(IsNull(rs("val_subvencao_federal_estimado")), 0, rs("val_subvencao_federal_estimado"))
            cValSubvencaoEstadualEst = IIf(IsNull(rs("val_subvencao_estadual_estimado")), 0, rs("val_subvencao_estadual_estimado"))
            '
        Else
            
            cValRestituicao = VerNullNumerico(rs("val_restituicao"))
            cValPremioTarifaRest = VerNullNumerico(rs("val_premio_tarifa"))
            cValTotCorretagemRest = VerNullNumerico(rs("val_comissao"))
            cValProLaboreRest = VerNullNumerico(rs("val_comissao_estipulante"))
            
            cValIOFRest = VerNullNumerico(rs("val_iof"))
            cValCustoApoliceCertifRest = VerNullNumerico(rs("custo_apolice"))
            cValAdicFracionamentoRest = VerNullNumerico(rs("val_adic_fracionamento"))
            cValDescontoRest = VerNullNumerico(rs("val_desconto_comercial"))
            
            cValRestituicaoEst = VerNullNumerico(rs("val_restituicao_estimado"))
            cValIOFEst = VerNullNumerico(rs("val_iof_estimado"))
            cValCustoApoliceCertifEst = VerNullNumerico(rs("custo_apolice_estimado"))
            cValAdicFracionamentoEst = VerNullNumerico(rs("val_adic_fracionamento_estimado"))
            cValDescontoEst = VerNullNumerico(rs("val_desconto_comercial_estimado"))
            cValPremioTarifaEst = VerNullNumerico(rs("val_premio_tarifa_estimado"))
            cValTotCorretagemEst = VerNullNumerico(rs("val_comissao_estimado"))
            cValProLaboreEst = VerNullNumerico(rs("val_comissao_estipulante_estimado"))
             
            
            cValRemuneracao = VerNullNumerico(rs("val_remuneracao"))
            cValRemuneracaoEst = VerNullNumerico(rs("val_remuneracao_estimado"))
                
            If AvaliacaoGerencial = True And bModular = True Then
                cValRestituicao = VerNullNumerico(rs("val_restituicao")) + oDadosModular.ValRestituicao
                cValIOFRest = VerNullNumerico(rs("val_iof")) + oDadosModular.ValIOFRest
                cValCustoApoliceCertifRest = VerNullNumerico(rs("custo_apolice")) + oDadosModular.ValCustoApoliceCertifRest
                cValAdicFracionamentoRest = VerNullNumerico(rs("val_adic_fracionamento")) + oDadosModular.ValAdicFracionamentoRest
                cValDescontoRest = VerNullNumerico(rs("val_desconto_comercial")) + oDadosModular.ValDescontoRest
                cValPremioTarifaRest = VerNullNumerico(rs("val_premio_tarifa")) + oDadosModular.ValPremioTarifaRest
                cValTotCorretagemRest = VerNullNumerico(rs("val_comissao")) + oDadosModular.ValTotCorretagemRest
                cValProLaboreRest = VerNullNumerico(rs("val_comissao_estipulante")) + oDadosModular.ValProLaboreRest

                cValRestituicaoEst = VerNullNumerico(rs("val_restituicao_estimado")) + oDadosModular.ValRestituicaoEst
                cValIOFEst = VerNullNumerico(rs("val_iof_estimado")) + oDadosModular.ValIOFEst
                cValCustoApoliceCertifEst = VerNullNumerico(rs("custo_apolice_estimado")) + oDadosModular.ValCustoApoliceCertifEst
                cValAdicFracionamentoEst = VerNullNumerico(rs("val_adic_fracionamento_estimado")) + oDadosModular.ValCustoApoliceCertifEst
                cValDescontoEst = VerNullNumerico(rs("val_desconto_comercial_estimado")) + oDadosModular.ValDescontoEst
                cValPremioTarifaEst = VerNullNumerico(rs("val_premio_tarifa_estimado")) + oDadosModular.ValPremioTarifaEst
                cValTotCorretagemEst = VerNullNumerico(rs("val_comissao_estimado")) + oDadosModular.Val_Tot_Corretagem_Est
                cValProLaboreEst = VerNullNumerico(rs("val_comissao_estipulante_estimado")) + oDadosModular.Val_Pro_Lab_Est

                
                cValRemuneracao = VerNullNumerico(rs("val_remuneracao")) + oDadosModular.Val_Remuneracao
                cValRemuneracaoEst = VerNullNumerico(rs("val_remuneracao_estimado")) + oDadosModular.Val_Remuneracao_Est
            End If
            
            
                cValCambio = VerNullNumerico(rs("val_cambio"))
                If Not IsNull(rs("descricao")) Then
                    sTextoEndosso = Trim(rs("descricao"))
                Else
                    sTextoEndosso = ""
                End If
                
                lEndossoId = VerNullNumerico(rs("endosso_id"))
            
                
                cValSubvencaoFederal = VerNullNumerico(rs("val_subvencao_federal"))
                cValSubvencaoEstadual = VerNullNumerico(rs("val_subvencao_estadual"))
                cValSubvencaoFederalEst = VerNullNumerico(rs("val_subvencao_federal_estimado"))
                cValSubvencaoEstadualEst = VerNullNumerico(rs("val_subvencao_estadual_estimado"))
                
                
           
        End If

    End If

    rs.Close
    
    
    Exit Sub

TrataErro:
    Call TrataErroGeral("ObterValoresAvaliacaoRestituicao", Me.name)
    Call TerminaSEGBR
End Sub



Function VerNullNumerico(ByVal dado As Variant) As Variant
    If IsNull(dado) Then
        VerNullNumerico = 0
    ElseIf IsEmpty(dado) Or Trim(dado) = "" Then
        VerNullNumerico = 0
    Else
        VerNullNumerico = dado
    End If
End Function


Public Sub ObterValoresSubvencao(ByVal lPropostaId As Long, _
                                 ByVal cValPremio As Currency)

Dim rs As Object
Dim oSubvencao As Object

Dim cValSubvencaoInformado As Currency
Dim dPercFederal As Double

'Consulta os valores de subvencao por proposta - Rmarins 20/10/06

On Error GoTo TrataErro
    
    cValSubvencaoInformado = 0
    bPossuiSubvencao = False
    
    Set oSubvencao = CreateObject("SEGL0026.cls00125")
    
    Set rs = oSubvencao.ObterPropostaSubvencao(gsSIGLASISTEMA, _
                                               App.Title, _
                                               App.FileDescription, _
                                               glAmbiente_id, _
                                               lPropostaId)
    
    Set oSubvencao = Nothing
    
    If Not rs.EOF Then
        cValSubvencaoInformado = CCur(rs("val_subvencao_informado"))
        bPossuiSubvencao = True
    End If
     
    Set rs = Nothing
        
    dPercFederal = CalcularPercValor(cValPremio, _
                                     cValSubvencaoInformado)
    
    FrmAvaliacao.lblPercFederal.Caption = Format(dPercFederal, "##0.00000")
        
    Exit Sub
    
TrataErro:
   Call TratarErro("ObterValoresSubvencao", Me.name)
   FinalizarAplicacao
   
End Sub


Sub DadosModular(ByVal lPropostaId As Long, ByVal lPropostaModular As Long)
    Dim oFinanceiro As Object
    
    bModular = True

    If Produto_id = 1168 Then
        lPropostaModular = lPropostaId
        lAvaliacao = lAvaliacaoModular
    End If

    oDadosModular.proposta_id = lPropostaModular
    ''executar todas as fun��es para proposta modular

    Set oFinanceiro = CreateObject("SEGL0026.cls00125")
    
    Set rs = oFinanceiro.ConsultarAvaliacaoRestituicao(gsSIGLASISTEMA, _
                                                       App.Title, _
                                                       App.FileDescription, _
                                                       glAmbiente_id, _
                                                       IIf(TpOperacao = 1, SITUACAO_PENDENTE_AVALIACAO_TECNICA, SITUACAO_PENDENTE_AVALIACAO_GERENCIAL), _
                                                       IIf(Produto_id = 1168, 1168, 0), lPropostaModular, IIf(Produto_id = 1168, lAvaliacao, 0))


    If Not rs.EOF Then
        oDadosModular.proposta_bb = rs("proposta_bb")
        oDadosModular.Num_avaliacao = rs("num_avaliacao")
        oDadosModular.Dt_Pedido_Avaliacao = Format(rs("dt_pedido_avaliacao"), "dd/mm/yyyy")
        oDadosModular.Produto_id = rs("produto_id")
        oDadosModular.Dt_Contratacao = rs("dt_contratacao")
        oDadosModular.Dt_Inicio_Vigencia = rs("dt_inicio_vigencia")
        oDadosModular.Dt_Fim_Vigencia = rs("dt_fim_vigencia")
        oDadosModular.Origem = rs("origem")
        oDadosModular.Nome_Tp_Endosso = rs("nome_tp_endosso")

        If Produto_id = 1168 Then
            If lAvaliacao > rs("num_avaliacao") Then
                oDadosModular.iTpCobertura = 526
            Else
                oDadosModular.iTpCobertura = 527
            End If
        End If
        
        Call Obter_Dados_Adicionais_Proposta_Modular(lPropostaModular, _
                                                     oDadosModular.ramo_id, _
                                                     oDadosModular.Custo_Apolice)
        
        Call Obter_Premio_IS(lPropostaModular, rs("produto_id"), _
                                                   oDadosModular.Val_IS, _
                                                   oDadosModular.Val_Premio)

        Call Carregar_Colecao_Cobrancas(lPropostaModular)

        'rsilva - 02/09/2009
        'Nova regra para o calculo do premio a ser restituido
        Call ConsultarSubRamoModular(lPropostaModular)

        Call ConsultarPercIOFModular(oDadosModular.Dt_Inicio_Vigencia, _
                                     oDadosModular.SubRamoId)
        
        Call Obter_Totais_Pagos(oDadosModular.Val_Premio_Pago, _
                                oDadosModular.Val_Juros, _
                                oDadosModular.Val_Adic_Fracionamento, _
                                oDadosModular.Val_Desconto, _
                                oDadosModular.Val_IOF_Restituicao, True)
        
        Call ObterValoresAvaliacaoRestituicaoModular(lPropostaModular, _
                                                     oDadosModular.Num_avaliacao)

        Call Obtem_Perc_IRModular(oDadosModular.ramo_id, _
                                  oDadosModular.Dt_Inicio_Vigencia, _
                                  0)

        Call CarregarRateioCorretagemRestituicao(lPropostaModular, _
                                                 oDadosModular.Num_avaliacao, _
                                                 oDadosModular.ValRestituicao, _
                                                 oDadosModular.ValIOFRest, _
                                                 oDadosModular.ValCustoApoliceCertifRest, _
                                                 oDadosModular.PercIR)


        oDadosModular.PercCorretagemRest = ObterPercentualTotalCorretagem(lPropostaModular)

        Else
            bModular = False

       End If
End Sub

Private Sub ConsultarSubRamoModular(ByVal lPropostaId As Long)

Dim oApolice As Object
Dim rs As Recordset

Dim sTpEmissao As String

On Error GoTo TrataErro

    Set oApolice = CreateObject("SEGL0026.cls00126")

    Set rs = oApolice.ConsultarDados(gsSIGLASISTEMA, _
                                     App.Title, _
                                     App.FileDescription, _
                                     glAmbiente_id, _
                                     lPropostaId)
    
    Set oApolice = Nothing
    
    If Not rs.EOF Then
        oDadosModular.SubRamoId = rs("subramo_id")
    Else
        oDadosModular.SubRamoId = 0
    End If
    
    Set rs = Nothing
    
    Exit Sub

TrataErro:
    Call TratarErro("ConsultarSubRamoModular", Me.name)
    Call FinalizarAplicacao

End Sub

Private Sub ConsultarPercIOFModular(ByVal dtIOF As String, ByVal Subramo_id As Integer)
Dim oFinanceiro As Object
'RSILVA
On Error GoTo TrataErro

    If IsDate(dtIOF) Then
        dtIOF = Format(dtIOF, "yyyymmdd")
    End If

    Set oFinanceiro = CreateObject("SEGL0026.cls00125")

    oDadosModular.PercIOF = CDbl(oFinanceiro.ObterPercentualItemFinanceiro(cUserName, _
                                                                           gsSIGLASISTEMA, _
                                                                           App.Title, _
                                                                           App.FileDescription, _
                                                                           glAmbiente_id, _
                                                                           "IOF", _
                                                                           oDadosModular.ramo_id, _
                                                                           Data_Sistema, _
                                                                           dtIOF, _
                                                                           Subramo_id))

    Set oFinanceiro = Nothing

    Exit Sub

TrataErro:
    Call TratarErro("ConsultarPercIOF", Me.name)
    Call FinalizarAplicacao

End Sub

Public Sub ObterValoresAvaliacaoRestituicaoModular(ByVal lPropostaId As Long, _
                                                    ByVal lAvaliacao As Long)
    
Dim oFinanceiro As Object
Dim rs As Object

On Error GoTo TrataErro

    Set oFinanceiro = CreateObject("SEGL0026.cls00125")
    
    Set rs = oFinanceiro.ConsultarAvaliacaoRestituicao(gsSIGLASISTEMA, _
                                                       App.Title, _
                                                       App.FileDescription, _
                                                       glAmbiente_id, _
                                                       sSituacao, _
                                                       , _
                                                       lPropostaId, _
                                                       lAvaliacao)

    Set oFinanceiro = Nothing

    If Not rs.EOF Then
        With oDadosModular
            If TpOperacao = 1 Then
    
                .ValRestituicao = rs("val_restituicao_estimado")
                .ValIOFRest = rs("val_iof_estimado")
                .ValCustoApoliceCertifRest = rs("custo_apolice_estimado")
                .ValAdicFracionamentoRest = rs("val_adic_fracionamento_estimado")
                .ValDescontoRest = rs("val_desconto_comercial_estimado")
                .ValPremioTarifaRest = rs("val_premio_tarifa_estimado")
                .ValTotCorretagemRest = rs("val_comissao_estimado")
                .ValProLaboreRest = rs("val_comissao_estipulante_estimado")
    
                .ValRestituicaoEst = rs("val_restituicao_estimado")
                .ValIOFEst = rs("val_iof_estimado")
                .ValCustoApoliceCertifEst = rs("custo_apolice_estimado")
                .ValAdicFracionamentoEst = rs("val_adic_fracionamento_estimado")
                .ValDescontoEst = rs("val_desconto_comercial_estimado")
                .ValPremioTarifaEst = rs("val_premio_tarifa_estimado")
                .Val_Tot_Corretagem_Est = rs("val_comissao_estimado")
                .Val_Pro_Lab_Est = rs("val_comissao_estipulante_estimado")
                               
                .Val_Remuneracao = rs("val_remuneracao")
                .Val_Remuneracao_Est = rs("val_remuneracao_estimado")
                .ValCambio = rs("val_cambio")
                .EndossoId = rs("endosso_id")
                
            Else
                .ValRestituicao = rs("val_restituicao")
                .ValIOFRest = rs("val_iof")
                .ValCustoApoliceCertifRest = rs("custo_apolice")
                .ValAdicFracionamentoRest = rs("val_adic_fracionamento")
                .ValDescontoRest = rs("val_desconto_comercial")
                .ValPremioTarifaRest = rs("val_premio_tarifa")
                .ValTotCorretagemRest = rs("val_comissao")
                .ValProLaboreRest = rs("val_comissao_estipulante")
                
                .ValRestituicaoEst = rs("val_restituicao_estimado")
                .ValIOFEst = rs("val_iof_estimado")
                .ValCustoApoliceCertifEst = rs("custo_apolice_estimado")
                .ValAdicFracionamentoEst = rs("val_adic_fracionamento_estimado")
                .ValDescontoEst = rs("val_desconto_comercial_estimado")
                .ValPremioTarifaEst = rs("val_premio_tarifa_estimado")
                .Val_Tot_Corretagem_Est = rs("val_comissao_estimado")
                .Val_Pro_Lab_Est = rs("val_comissao_estipulante_estimado")
                
                
                .Val_Remuneracao = rs("val_remuneracao")
                .Val_Remuneracao_Est = rs("val_remuneracao_estimado")
                
                .ValCambio = rs("val_cambio")
                
                .EndossoId = rs("endosso_id")
                

        End If
           
            
        'End If
        End With
    End If

    rs.Close
    
    Exit Sub

TrataErro:
    Call TrataErroGeral("ObterValoresAvaliacaoRestituicao", Me.name)
    Call TerminaSEGBR

End Sub


Sub Obtem_Perc_IRModular(ByVal PRamo As String, ByVal PDtReferencia As String, ByRef PPercIr As Currency)

Dim rc As rdoResultset
Dim SQL As String
Dim DtReferencia As String

DtReferencia = Format(PDtReferencia, "yyyymmdd")

'Obtendo percentual de IR

SQL = "SELECT valor "
SQL = SQL & "From "
SQL = SQL & "     val_item_financeiro_ramo_tb "
SQL = SQL & "Where "
SQL = SQL & "     ramo_id = " & PRamo
SQL = SQL & "     and ltrim(rtrim(cod_item_financeiro)) = 'IR'"

SQL = SQL & " and dt_inicio_vigencia <= '" & DtReferencia & "'"
SQL = SQL & " and (dt_fim_vigencia >= '" & DtReferencia & "' or dt_fim_vigencia is Null)"

Set rc = rdocn.OpenResultset(SQL)

If Not rc.EOF Then
   oDadosModular.PercIR = Val(rc!valor)
Else
   MensagemBatch "Percentual de IR n�o encontrado. o Programa ser� cancelado."
   End
End If

Exit Sub

TrataErro:
   TrataErroGeral "Obtem_Perc_IR", "Rateio.bas"
   End

End Sub



Private Sub ConsultarPercIOF(ByVal dtIOF As String, ByVal Subramo_id As Integer)
Dim oFinanceiro As Object
'RSILVA
On Error GoTo TrataErro

    If IsDate(dtIOF) Then
        dtIOF = Format(dtIOF, "yyyymmdd")
    End If

    Set oFinanceiro = CreateObject("SEGL0026.cls00125")

    FrmAvaliacao.dPercIOF = CDbl(oFinanceiro.ObterPercentualItemFinanceiro(cUserName, _
                                                                           gsSIGLASISTEMA, _
                                                                           App.Title, _
                                                                           App.FileDescription, _
                                                                           glAmbiente_id, _
                                                                           "IOF", _
                                                                           oDadosModular.ramo_id, _
                                                                           Data_Sistema, _
                                                                           dtIOF, _
                                                                           Subramo_id))

    Set oFinanceiro = Nothing

    Exit Sub

TrataErro:
    Call TratarErro("ConsultarPercIOF", Me.name)
    Call FinalizarAplicacao

End Sub



Public Function CalcularPercValor(ByVal cValPrincipal As Currency, _
                                  ByVal cValInformado As Currency) As Double

Dim dPercentual As Double

On Error GoTo TrataErro

    If cValPrincipal = 0 Then
       cValInformado = 0
       CalcularPercValor = cValInformado
       Exit Function
    End If
                
    dPercentual = ((cValInformado / cValPrincipal) * 100)
    
    If dPercentual < 0 Then
       dPercentual = 0
    End If
    
    CalcularPercValor = dPercentual
    
    Exit Function

TrataErro:
    Call TratarErro("CalcularPercValor", Me.name)
    Call FinalizarAplicacao

End Function




