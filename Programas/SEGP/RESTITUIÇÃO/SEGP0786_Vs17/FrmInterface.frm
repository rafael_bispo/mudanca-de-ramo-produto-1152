VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.2#0"; "MSCOMCTL.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form FrmInterface 
   Caption         =   "SEGP00786 - Gera��o de Informa��es Financeiras"
   ClientHeight    =   5010
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   9540
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   5010
   ScaleWidth      =   9540
   StartUpPosition =   3  'Windows Default
   Begin VB.Frame frame_escolha 
      Height          =   4635
      Left            =   90
      TabIndex        =   0
      Top             =   0
      Width           =   9315
      Begin VB.OptionButton optVerificaProd719 
         Caption         =   "Verifica Produto 719"
         Height          =   255
         Left            =   120
         TabIndex        =   14
         Tag             =   "4"
         Top             =   4080
         Width           =   2055
      End
      Begin VB.OptionButton optNaoVerificaProd719 
         Caption         =   "N�o Verifica Produto 719"
         Height          =   255
         Left            =   120
         TabIndex        =   13
         Top             =   4320
         Width           =   2535
      End
      Begin VB.ListBox lstOrigem 
         Height          =   2010
         ItemData        =   "FrmInterface.frx":0000
         Left            =   3690
         List            =   "FrmInterface.frx":0025
         TabIndex        =   10
         Tag             =   "3"
         Top             =   210
         Width           =   5445
      End
      Begin VB.Frame frmProcessamento 
         Height          =   2115
         Left            =   120
         TabIndex        =   5
         Top             =   120
         Width           =   3495
         Begin VB.OptionButton optReemissao 
            Caption         =   "Reemiss�o"
            Enabled         =   0   'False
            Height          =   255
            Left            =   1800
            TabIndex        =   12
            Tag             =   "2"
            Top             =   270
            Width           =   1215
         End
         Begin VB.OptionButton optEmissao 
            Caption         =   "Emiss�o"
            Height          =   255
            Left            =   180
            TabIndex        =   11
            Tag             =   "1"
            Top             =   240
            Value           =   -1  'True
            Width           =   1215
         End
         Begin VB.TextBox txtFim 
            BeginProperty Font 
               Name            =   "Times New Roman"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   840
            Locked          =   -1  'True
            TabIndex        =   7
            Top             =   1350
            Width           =   2415
         End
         Begin VB.TextBox txtInicio 
            BeginProperty Font 
               Name            =   "Times New Roman"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   840
            Locked          =   -1  'True
            TabIndex        =   6
            Top             =   840
            Width           =   2415
         End
         Begin VB.Label Label2 
            AutoSize        =   -1  'True
            Caption         =   "Fim......"
            Height          =   195
            Left            =   120
            TabIndex        =   9
            Top             =   1350
            Width           =   510
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "In�cio.."
            Height          =   195
            Left            =   120
            TabIndex        =   8
            Top             =   840
            Width           =   495
         End
      End
      Begin VB.CommandButton cmdOk 
         Caption         =   "&Ok"
         Enabled         =   0   'False
         Height          =   375
         Left            =   6960
         TabIndex        =   2
         Top             =   4170
         Width           =   1095
      End
      Begin VB.CommandButton cmdSair 
         Caption         =   "&Sair"
         Height          =   375
         Left            =   8070
         TabIndex        =   1
         Top             =   4170
         Width           =   1095
      End
      Begin MSFlexGridLib.MSFlexGrid GridResumo 
         Height          =   1695
         Left            =   120
         TabIndex        =   3
         Top             =   2400
         Width           =   9015
         _ExtentX        =   15901
         _ExtentY        =   2990
         _Version        =   393216
         Cols            =   3
         FixedCols       =   0
         FormatString    =   $"FrmInterface.frx":012E
      End
   End
   Begin MSComctlLib.StatusBar StbRodape 
      Align           =   2  'Align Bottom
      Height          =   315
      Left            =   0
      TabIndex        =   4
      Top             =   4695
      Width           =   9540
      _ExtentX        =   16828
      _ExtentY        =   556
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   4
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   8997
            MinWidth        =   8997
            Text            =   "Mensagem"
            TextSave        =   "Mensagem"
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   6
            Alignment       =   1
            AutoSize        =   2
            Object.Width           =   1535
            MinWidth        =   88
            Text            =   "Data do sistema"
            TextSave        =   "28/5/2020"
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            AutoSize        =   2
            Object.Width           =   1111
            MinWidth        =   88
            Text            =   "Usu�rio"
            TextSave        =   "Usu�rio"
         EndProperty
         BeginProperty Panel4 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   6068
            MinWidth        =   6068
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "FrmInterface"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmdSair_Click()

    On Error GoTo Trata_Erro

    Call FinalizarAplicacao

    Exit Sub

Trata_Erro:

    Call TratarErro("cmdSair_Click", Me.name)
    Call TerminaSEGBR

End Sub

Private Sub Form_Load()

    On Error GoTo Trata_Erro
    'Verifica se foi executado pelo Control-M
    CTM = Verifica_Origem_ControlM(Command)

    'Pula se for executado pelo control-m
    If CTM = False Then
        ' Verificando permiss�o de acesso � aplica��o '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        If Not Trata_Parametros(Command) Then
            'Call FinalizarAplicacao
        End If
    End If
'' Comentar ap�s testes!! ''''''''''
    cUserName = "DICAMPOS"
    glAmbiente_id = 3
''''''''''''''''''''''''''''''''''''

    ' Obtendo a data operacional do sistema '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

    Call ObterDataSistema(gsSIGLASISTEMA)

    Call CentraFrm(Me)

    Me.Caption = "SEGP0786 - Gera��o de informa��es financeiras - " & Ambiente

    GridResumo.Rows = 1
    GridResumo.Row = 0

    ' LBorges - 29/12/2003 - agenda diaria
    If Obtem_agenda_diaria_id(Command) > 0 Or CTM = True Then
        Me.Show
        Call cmdOk_Click
        Call TerminaSEGBR
    End If

    Exit Sub

Trata_Erro:

    Call TratarErro("Form_Load", Me.name)
    Call TerminaSEGBR

End Sub

Private Sub cmdOk_Click()

Dim bGTR_Implantado    As Boolean
Dim sDiretorio         As String
Dim objInterface       As Object
Dim lRemessa           As Long
Dim lTotalVoucher      As Long
'Diego de Oliveira Campos - JM COnfitec - 06/05/2011
'Flow 10145332
Dim padrao             As String
'Fim

    On Error GoTo Trata_Erro
  
    ' Inicializando os parametros para o AutProd ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

   ' Call InicializaParametrosExecucaoBatch(Me)

    ' Colocando a Data de In�cio do Processo, desabilitando o OK, e formatando o Mouse Pointer ''''''''''''''''''''''''


    'Diego de Oliveira Campos - JM COnfitec - 06/05/2011
    'Flow 10145332
    If optVerificaProd719.Value = True Then
        padrao = "S"
    Else
        padrao = "N"
    End If
    'Fim - Diego de Oliveira

    cmdOk.Enabled = False
    lstOrigem.Enabled = False
    txtInicio.Text = Now
    StbRodape.SimpleText = "Processando..."
    Screen.MousePointer = vbHourglass

    sDiretorio = LerArquivoIni("PAGNET", "AP_PATH")

    ' Verificando se o GTR est� ativo '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

    bGTR_Implantado = Obtem_GTR_Implantado

    Set objInterface = CreateObject("SEGL0064.CLS00132")

    If lstOrigem.Selected(0) = True Then
  
        ' Gerando o Arquivo de Interface de indeniza��o de sinistro '''''''''''''''''''''''''''''''''''''''''''''''''''

        lRemessa = objInterface.BuscarIndenizacaoSinistro(cUserName, _
                                                          gsSIGLASISTEMA, _
                                                          App.Title, _
                                                          App.FileDescription, _
                                                          glAmbiente_id, _
                                                          Data_Sistema, _
                                                          bGTR_Implantado, _
                                                          sDiretorio, _
                                                          lTotalVoucher, _
                                                          padrao) 'Diego De Oliveira Campos - 27/04/2011 - JM Confitec - Flow 10145332
    
                                                      
        ' Adicionando um item na lista ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Call GridResumo.AddItem(lRemessa & vbTab & lstOrigem.List(0) & vbTab & Str(lTotalVoucher))

        Call goProducao.AdicionaLog(1, IIf(Len(Trim(lRemessa)) = 0, 0, lRemessa), 1)
        Call goProducao.AdicionaLog(2, IIf(Len(Trim(lTotalVoucher)) = 0, 0, lTotalVoucher), 1)
        
        'grava log arquivo CTM
        If CTM = True Then
            Call GravaLogCTM("OK - ", IIf(Len(Trim(lRemessa)) = 0, 0, lRemessa), "", "")
            Call GravaLogCTM("OK - ", IIf(Len(Trim(lTotalVoucher)) = 0, 0, lTotalVoucher), "", "")
        End If


    End If

    If lstOrigem.Selected(1) = True Then

        ' Gerando o Arquivo de Interface de honor�rios e despesas de sinistro '''''''''''''''''''''''''''''''''''''''''

        lRemessa = objInterface.BuscarHonorariosDespesas(cUserName, _
                                                         gsSIGLASISTEMA, _
                                                         App.Title, _
                                                         App.FileDescription, _
                                                         glAmbiente_id, _
                                                         Data_Sistema, _
                                                         bGTR_Implantado, _
                                                         sDiretorio, _
                                                         lTotalVoucher)
                                                         
        ' Adicionando um item na lista ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Call GridResumo.AddItem(lRemessa & vbTab & lstOrigem.List(1) & vbTab & Str(lTotalVoucher))

        Call goProducao.AdicionaLog(1, IIf(Len(Trim(lRemessa)) = 0, 0, lRemessa), 2)
        Call goProducao.AdicionaLog(2, IIf(Len(Trim(lTotalVoucher)) = 0, 0, lTotalVoucher), 2)
        
        'grava log arquivo CTM
        If CTM = True Then
            Call GravaLogCTM("OK - ", IIf(Len(Trim(lRemessa)) = 0, 0, lRemessa), "", "")
            Call GravaLogCTM("OK - ", IIf(Len(Trim(lTotalVoucher)) = 0, 0, lTotalVoucher), "", "")
        End If


    End If

    If lstOrigem.Selected(2) = True Then

        ' Gerando o Arquivo de Interface de Restitui��o de Pr�mio '''''''''''''''''''''''''''''''''''''''''''''''''''''

        lRemessa = objInterface.BuscarRestituicaoPremio(cUserName, _
                                                        gsSIGLASISTEMA, _
                                                        App.Title, _
                                                        App.FileDescription, _
                                                        glAmbiente_id, _
                                                        Data_Sistema, _
                                                        sDiretorio, _
                                                        lTotalVoucher)

        ' Adicionando um item na lista ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Call GridResumo.AddItem(lRemessa & vbTab & lstOrigem.List(2) & vbTab & Str(lTotalVoucher))

        Call goProducao.AdicionaLog(1, IIf(Len(Trim(lRemessa)) = 0, 0, lRemessa), 3)
        Call goProducao.AdicionaLog(2, IIf(Len(Trim(lTotalVoucher)) = 0, 0, lTotalVoucher), 3)
        
        'grava log arquivo CTM
        If CTM = True Then
            Call GravaLogCTM("OK - ", IIf(Len(Trim(lRemessa)) = 0, 0, lRemessa), "", "")
            Call GravaLogCTM("OK - ", IIf(Len(Trim(lTotalVoucher)) = 0, 0, lTotalVoucher), "", "")
        End If


    End If
  
    If lstOrigem.Selected(3) = True Then

        ' Gerando o Arquivo de Interface de Restitui��o de Pr�mio BB ''''''''''''''''''''''''''''''''''''''''''''''''''

        lRemessa = objInterface.BuscarRestituicaoPremioBB(cUserName, _
                                                          gsSIGLASISTEMA, _
                                                          App.Title, _
                                                          App.FileDescription, _
                                                          glAmbiente_id, _
                                                          Data_Sistema, _
                                                          sDiretorio, _
                                                          lTotalVoucher)
    

        ' Adicionando um item na lista ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Call GridResumo.AddItem(lRemessa & vbTab & lstOrigem.List(3) & vbTab & Str(lTotalVoucher))

        Call goProducao.AdicionaLog(1, IIf(Len(Trim(lRemessa)) = 0, 0, lRemessa), 4)
        Call goProducao.AdicionaLog(2, IIf(Len(Trim(lTotalVoucher)) = 0, 0, lTotalVoucher), 4)

        'grava log arquivo CTM
        If CTM = True Then
            Call GravaLogCTM("OK - ", IIf(Len(Trim(lRemessa)) = 0, 0, lRemessa), "", "")
            Call GravaLogCTM("OK - ", IIf(Len(Trim(lTotalVoucher)) = 0, 0, lTotalVoucher), "", "")
        End If

    End If

    If lstOrigem.Selected(4) = True Then

        ' Gerando o Arquivo de Interface de Comiss�o de Corretagem ''''''''''''''''''''''''''''''''''''''''''''''''''''

        lRemessa = objInterface.BuscarComissaoCorretagem(cUserName, _
                                                         gsSIGLASISTEMA, _
                                                         App.Title, _
                                                         App.FileDescription, _
                                                         glAmbiente_id, _
                                                         Data_Sistema, _
                                                         sDiretorio, _
                                                         lTotalVoucher)
    

        ' Adicionando um item na lista ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Call GridResumo.AddItem(lRemessa & vbTab & lstOrigem.List(4) & vbTab & Str(lTotalVoucher))

        Call goProducao.AdicionaLog(1, IIf(Len(Trim(lRemessa)) = 0, 0, lRemessa), 5)
        Call goProducao.AdicionaLog(2, IIf(Len(Trim(lTotalVoucher)) = 0, 0, lTotalVoucher), 5)

        'grava log arquivo CTM
        If CTM = True Then
            Call GravaLogCTM("OK - ", IIf(Len(Trim(lRemessa)) = 0, 0, lRemessa), "", "")
            Call GravaLogCTM("OK - ", IIf(Len(Trim(lTotalVoucher)) = 0, 0, lTotalVoucher), "", "")
        End If

    End If

    If lstOrigem.Selected(5) = True Then

        ' Gerando o Arquivo de Interface de Cosseguro Cedido ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        lRemessa = objInterface.BuscarCoSeguroCedido(cUserName, _
                                                     gsSIGLASISTEMA, _
                                                     App.Title, _
                                                     App.FileDescription, _
                                                     glAmbiente_id, _
                                                     Data_Sistema, _
                                                     sDiretorio, _
                                                     lTotalVoucher)
    

        ' Adicionando um item na lista ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Call GridResumo.AddItem(lRemessa & vbTab & lstOrigem.List(5) & vbTab & Str(lTotalVoucher))

        Call goProducao.AdicionaLog(1, IIf(Len(Trim(lRemessa)) = 0, 0, lRemessa), 6)
        Call goProducao.AdicionaLog(2, IIf(Len(Trim(lTotalVoucher)) = 0, 0, lTotalVoucher), 6)

        'grava log arquivo CTM
        If CTM = True Then
            Call GravaLogCTM("OK - ", IIf(Len(Trim(lRemessa)) = 0, 0, lRemessa), "", "")
            Call GravaLogCTM("OK - ", IIf(Len(Trim(lTotalVoucher)) = 0, 0, lTotalVoucher), "", "")
        End If

    End If

    If lstOrigem.Selected(6) = True Then

        ' Gerando o Arquivo de Interface de Comiss�o Retida '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        lRemessa = objInterface.BuscarComissaoRetida(cUserName, _
                                                     gsSIGLASISTEMA, _
                                                     App.Title, _
                                                     App.FileDescription, _
                                                     glAmbiente_id, _
                                                     Data_Sistema, _
                                                     sDiretorio, _
                                                     lTotalVoucher)
   

        ' Adicionando um item na lista ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Call GridResumo.AddItem(lRemessa & vbTab & lstOrigem.List(6) & vbTab & Str(lTotalVoucher))

        Call goProducao.AdicionaLog(1, IIf(Len(Trim(lRemessa)) = 0, 0, lRemessa), 7)
        Call goProducao.AdicionaLog(2, IIf(Len(Trim(lTotalVoucher)) = 0, 0, lTotalVoucher), 7)
        
                'grava log arquivo CTM
        If CTM = True Then
            Call GravaLogCTM("OK - ", IIf(Len(Trim(lRemessa)) = 0, 0, lRemessa), "", "")
            Call GravaLogCTM("OK - ", IIf(Len(Trim(lTotalVoucher)) = 0, 0, lTotalVoucher), "", "")
        End If

    End If
    
    'lrocha 23/10/2007 - Restitui��o de Pr�mio MAPA
    If lstOrigem.Selected(7) = True Then

        ' Gerando o Arquivo de Interface de Restitui��o de Pr�mio MAPA '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        
        lRemessa = objInterface.BuscarRestituicaoPremioMAPA(cUserName, _
                                                            gsSIGLASISTEMA, _
                                                            App.Title, _
                                                            App.FileDescription, _
                                                            glAmbiente_id, _
                                                            Data_Sistema, _
                                                            lTotalVoucher)

        ' Adicionando um item na lista ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Call GridResumo.AddItem(lRemessa & vbTab & lstOrigem.List(6) & vbTab & Str(lTotalVoucher))

        Call goProducao.AdicionaLog(1, IIf(Len(Trim(lRemessa)) = 0, 0, lRemessa), 7)
        Call goProducao.AdicionaLog(2, IIf(Len(Trim(lTotalVoucher)) = 0, 0, lTotalVoucher), 7)
        
                'grava log arquivo CTM
        If CTM = True Then
            Call GravaLogCTM("OK - ", IIf(Len(Trim(lRemessa)) = 0, 0, lRemessa), "", "")
            Call GravaLogCTM("OK - ", IIf(Len(Trim(lTotalVoucher)) = 0, 0, lTotalVoucher), "", "")
        End If

    End If
    
    'fsa - 17/09/2009 - Restitui��o de Pr�mio Bonifica��o ''''''''''''''''''''''''''''''''''
    If lstOrigem.Selected(8) = True Then

        ' Gerando o Arquivo de Interface de Restitui��o de Pr�mio Bonifica��o ''''''''''''''
        
        lRemessa = objInterface.BuscarRestituicaoPremioBonificacao(cUserName, _
                                                                   gsSIGLASISTEMA, _
                                                                   App.Title, _
                                                                   App.FileDescription, _
                                                                   glAmbiente_id, _
                                                                   Data_Sistema, _
                                                                   sDiretorio, _
                                                                   lTotalVoucher)

        ' Adicionando um item na lista '''''''''''''''''''''''''''''''''''''''''''''''''''''

        Call GridResumo.AddItem(lRemessa & vbTab & lstOrigem.List(8) & vbTab & Str(lTotalVoucher))

        Call goProducao.AdicionaLog(1, IIf(Len(Trim(lRemessa)) = 0, 0, lRemessa), 9)
        Call goProducao.AdicionaLog(2, IIf(Len(Trim(lTotalVoucher)) = 0, 0, lTotalVoucher), 9)
        
                'grava log arquivo CTM
        If CTM = True Then
            Call GravaLogCTM("OK - ", IIf(Len(Trim(lRemessa)) = 0, 0, lRemessa), "", "")
            Call GravaLogCTM("OK - ", IIf(Len(Trim(lTotalVoucher)) = 0, 0, lTotalVoucher), "", "")
        End If

    End If
    
    'pmelo
    If lstOrigem.Selected(9) = True Then

        ' Gerando o Arquivo de Interface de Resseguro '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        lRemessa = objInterface.BuscarResseguro(cUserName, _
                                                gsSIGLASISTEMA, _
                                                App.Title, _
                                                App.FileDescription, _
                                                glAmbiente_id, _
                                                Data_Sistema, _
                                                sDiretorio, _
                                                lTotalVoucher)
    

        ' Adicionando um item na lista ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Call GridResumo.AddItem(lRemessa & vbTab & lstOrigem.List(9) & vbTab & Str(lTotalVoucher))

        Call goProducao.AdicionaLog(1, IIf(Len(Trim(lRemessa)) = 0, 0, lRemessa), 6)
        Call goProducao.AdicionaLog(2, IIf(Len(Trim(lTotalVoucher)) = 0, 0, lTotalVoucher), 6)
        
                'grava log arquivo CTM
        If CTM = True Then
            Call GravaLogCTM("OK - ", IIf(Len(Trim(lRemessa)) = 0, 0, lRemessa), "", "")
            Call GravaLogCTM("OK - ", IIf(Len(Trim(lTotalVoucher)) = 0, 0, lTotalVoucher), "", "")
        End If
   End If

    'JOAO.MACHADO - IPREM - 25-09-2013
    If lstOrigem.Selected(10) = True Then

         ' Gerando o Arquivo de Interface de custo de assistencias '''''''''''''''''''''''''''''''''''''''''''''''''''''
        lRemessa = objInterface.BuscarCustoAssistencia(cUserName, _
                                                        gsSIGLASISTEMA, _
                                                        App.Title, _
                                                        App.FileDescription, _
                                                        glAmbiente_id, _
                                                        Data_Sistema, _
                                                        sDiretorio, _
                                                        lTotalVoucher)

        ' Adicionando um item na lista ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Call GridResumo.AddItem(lRemessa & vbTab & lstOrigem.List(2) & vbTab & Str(lTotalVoucher))

        Call goProducao.AdicionaLog(1, IIf(Len(Trim(lRemessa)) = 0, 0, lRemessa), 3)
        Call goProducao.AdicionaLog(2, IIf(Len(Trim(lTotalVoucher)) = 0, 0, lTotalVoucher), 3)
    
        'grava log arquivo CTM
        If CTM = True Then
            Call GravaLogCTM("OK - ", IIf(Len(Trim(lRemessa)) = 0, 0, lRemessa), "", "")
            Call GravaLogCTM("OK - ", IIf(Len(Trim(lTotalVoucher)) = 0, 0, lTotalVoucher), "", "")
        End If
    End If

    ' Colocando a Data Fim do Processo ''''''''''''''''''''''''''''''''''''''''''''''''''''''

    txtFim.Text = Now

    Set objInterface = Nothing

    'LBorges - 29/12/2003 - Finaliza Scheduler
    'Pula de for executado pelo Control-M
    If CTM = False Then
        Call goProducao.finaliza
    End If


    StbRodape.SimpleText = "Fim do Processamento."
    Screen.MousePointer = vbDefault
    lstOrigem.ListIndex = -1

    ' Tratamento para caso nao haja registros
    ' Abosco @ 2004-01-07
    If lRemessa = 0 Then
        Call MensagemBatch("Nenhum registro processado")
    Else
        Call MensagemBatch("Processamento finalizado com sucesso!")
    End If

    Exit Sub

Trata_Erro:

    Call TratarErro("cmdOk_Click", Me.name)
    Call TerminaSEGBR

End Sub

Private Function Obtem_GTR_Implantado() As Boolean

Dim oSABL0010 As Object
Dim objAmbiente As Object
Dim strGTR_Implantado As String

    On Error GoTo Trata_Erro

    Obtem_GTR_Implantado = False

    Set oSABL0010 = CreateObject("SABL0010.cls00009")

    strGTR_Implantado = oSABL0010.RetornaValorAmbiente("GTR", "SISTEMA", "GTR_IMPLANTADO", gsCHAVESISTEMA)

    If (strGTR_Implantado = "s") Then
        Obtem_GTR_Implantado = True
    End If

    Set oSABL0010 = Nothing

    Exit Function

Trata_Erro:

    Call TratarErro("Obtem_GTR_Implantado", Me.name)
    Call TerminaSEGBR

End Function

Private Sub lstOrigem_Click()

Dim bSelecionou As Boolean
Dim iIndice As Integer

    bSelecionou = False

    For iIndice = 0 To lstOrigem.ListCount - 1
        If lstOrigem.Selected(iIndice) = True Then
            bSelecionou = True
            Exit For
        End If
    Next

    cmdOk.Enabled = bSelecionou

End Sub
