VERSION 5.00
Object = "{00025600-0000-0000-C000-000000000046}#5.2#0"; "Crystl32.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.2#0"; "MSCOMCTL.OCX"
Begin VB.Form FrmDevAutorizadas 
   Caption         =   "Devolu��es Autorizadas"
   ClientHeight    =   3345
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   7575
   LinkTopic       =   "Form1"
   ScaleHeight     =   3345
   ScaleWidth      =   7575
   StartUpPosition =   1  'CenterOwner
   Begin VB.CommandButton cmdAutorizar 
      Caption         =   "Prosseguir"
      Height          =   390
      Left            =   4500
      TabIndex        =   9
      Top             =   2520
      Width           =   1380
   End
   Begin VB.CommandButton cmdDetalhe 
      Caption         =   "Detalhe"
      Height          =   390
      Left            =   1600
      TabIndex        =   8
      Top             =   2520
      Width           =   1380
   End
   Begin VB.CommandButton cmdMarcar 
      Caption         =   "Selecionar &Todas"
      Height          =   390
      Left            =   120
      TabIndex        =   7
      Top             =   2520
      Width           =   1485
   End
   Begin MSComctlLib.StatusBar StatusBar1 
      Align           =   2  'Align Bottom
      Height          =   375
      Left            =   0
      TabIndex        =   6
      Top             =   2970
      Width           =   7575
      _ExtentX        =   13361
      _ExtentY        =   661
      Style           =   1
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   9596
            MinWidth        =   9596
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.FileListBox File1 
      Height          =   480
      Left            =   15
      TabIndex        =   5
      Top             =   2400
      Visible         =   0   'False
      Width           =   855
   End
   Begin MSComctlLib.ListView lsvRemessas 
      Height          =   2295
      Left            =   60
      TabIndex        =   4
      ToolTipText     =   "Duplo click imprimi "
      Top             =   120
      Visible         =   0   'False
      Width           =   7335
      _ExtentX        =   12938
      _ExtentY        =   4048
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      Checkboxes      =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483646
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   0
   End
   Begin Crystal.CrystalReport CR 
      Left            =   1080
      Top             =   2490
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   348160
      PrintFileLinesPerPage=   60
   End
   Begin VB.CommandButton cmdOk 
      Caption         =   "&Imprimir"
      Height          =   390
      Index           =   1
      Left            =   3000
      TabIndex        =   0
      Top             =   2520
      Width           =   1485
   End
   Begin VB.CommandButton cmdSair 
      Caption         =   "&Sair"
      Height          =   390
      Index           =   1
      Left            =   5880
      TabIndex        =   1
      Top             =   2520
      Width           =   1485
   End
   Begin VB.CommandButton cmdOk 
      Caption         =   "&OK"
      Height          =   570
      Index           =   0
      Left            =   -1170
      TabIndex        =   3
      Top             =   5730
      Width           =   1635
   End
   Begin VB.CommandButton cmdSair 
      Caption         =   "&Sair"
      Height          =   570
      Index           =   0
      Left            =   1080
      TabIndex        =   2
      Top             =   5730
      Width           =   1635
   End
End
Attribute VB_Name = "FrmDevAutorizadas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Function Busca_Path() As String

'-- Busca caminho de Saida do Arquivo SEG452

    On Error GoTo Trata_Erro

    Dim rs As rdoResultset
    Dim SQL As String

    SQL = "Select * from controle_sistema_db..PARAMETRO_tb with (nolock) " & vbNewLine
    SQL = SQL & "Where SECAO='ARQUIVOS DEVOLUCAO PREMIO' " & vbNewLine
    SQL = SQL & "AND Sigla_sistema='SEGBR' " & vbNewLine
    SQL = SQL & "AND campo='SEG452_PATH' " & vbNewLine
    SQL = SQL & "AND ambiente_id = " & glAmbiente_id & vbNewLine

    Set rs = rdocn.OpenResultset(SQL)

    If Not rs.EOF Then
        Busca_Path = IIf(IsNull(rs!valor), "", rs!valor)
    Else
        Busca_Path = "*"
    End If
    
    Busca_Path = "C:\QLD_ArqDevPremio\Gerados"

    Exit Function

    Resume

Trata_Erro:
    Screen.MousePointer = vbDefault
    Call TrataErroGeral("Busca_Path", Me.name)

End Function
Public Sub SetForecolorforListItem(xListItem As Object, ColorIn As Long)
' joconceicao - 13/jul/01
' set a cor de uma linha num listview , basta passar
'                                       xListitem as Listitem
'                                       ColorIn --> cor a ser usada
    Dim xSubItem As Object
    On Error Resume Next
    With xListItem
        .ForeColor = ColorIn
        .Bold = True
        For Each xSubItem In .ListSubItems
            xSubItem.ForeColor = ColorIn
        Next
    End With

End Sub

Sub Marcar_Linha_Processada()

    Dim i As Integer

    Dim Item As ListItem

    Set Item = lsvRemessas.SelectedItem
    'Item.SubItems(14) = "S"
    Item.Checked = False
    Call SetForecolorforListItem(Item, &H808080)

End Sub

Sub Selecionar_Todos()

    Dim i As Integer
    Dim Item As ListItem

    For i = 1 To lsvRemessas.ListItems.Count

        Set Item = lsvRemessas.ListItems(i)
        Item.Checked = True

    Next

End Sub

Public Sub Imprimir_1(data As String, Remessa As String)
    On Error GoTo Trata_Erro
    DoEvents
    Screen.MousePointer = vbHourglass
    CR.Reset
    '  Data = Item.SubItems(3) '-- Data de Aprova��o
    CR.Connect = "DSN=RPT_SEGBR;UID=" & SIS_usuario & ";PWD=" & UCase(SIS_senha)    'conex�o ODBC

    'CR.ReportFileName = App.PATH & "\rpt_devolucoes_autorizadas.rpt"
    CR.ReportFileName = App.PATH & "\SEGR0423-01.rpt"

    CR.StoredProcParam(0) = ""    'Format(Data, "yyyymmdd")
    CR.StoredProcParam(1) = ""    'Format(Data, "yyyymmdd")
    CR.Formulas(0) = "Formula = 'Per�odo: De " & data & " At� " & data & "'"
    CR.StoredProcParam(2) = 0    '-- Voucher
    CR.StoredProcParam(3) = Remessa    '-- Voucher
    CR.Action = 1

    Screen.MousePointer = vbDefault
    'StatusBar1.SimpleText = "Total de remessas a ser liberadas : " & (File1.ListCount - 1) 'lsvRemessas.ListItems.Count
    StatusBar1.SimpleText = "Total de remessas a ser liberadas : " & lsvRemessas.ListItems.Count
    Exit Sub
    Resume
Trata_Erro:
    Screen.MousePointer = vbDefault
    Call TrataErroGeral("Imprimir", Me.name)

End Sub

Public Sub Selecao_Remessas()
    Dim DataSel As String
    Dim data As String
    Dim Remessa As String
    Dim First As Boolean
    Dim i As Integer

    Dim Item As ListItem
    First = True
    If lsvRemessas.ListItems.Count = 0 Then
        MsgBox "N�o existe item selecionado para imprimir.", vbInformation, "Aviso"
        Exit Sub
    End If
    Screen.MousePointer = vbHourglass
    StatusBar1.SimpleText = "Aguarde, imprimindo remessas..."
    For i = 1 To lsvRemessas.ListItems.Count

        Set Item = lsvRemessas.ListItems(i)

        If lsvRemessas.ListItems(i).Checked = True Then
            data = ""
            Remessa = Item.SubItems(1)

            If lsvRemessas.ListItems(i).Tag <> "" Then
                Remessa = Item.Tag
                Imprimir_PreRemessa data, Remessa
            Else
                Imprimir_1 data, Remessa
            End If

        End If
    Next i

    Screen.MousePointer = vbDefault
    StatusBar1.SimpleText = "Total de Pre-Remessas a ser liberadas: " & lsvRemessas.ListItems.Count

End Sub
Public Sub Imprimir(ByVal Item As ListItem)
    On Error GoTo Trata_Erro
    Dim data As String
    Dim Remessa As String
    DoEvents
    Screen.MousePointer = vbHourglass
    data = Item.SubItems(3)    '-- Data de Aprova��o
    Remessa = Item.SubItems(1)

    CR.Connect = "DSN=RPT_SEGBR;UID=" & SIS_usuario & ";PWD=" & UCase(SIS_senha)    'conex�o ODBC

    'CR.ReportFileName = App.PATH & "\rpt_devolucoes_autorizadas.rpt"
    If Item.Tag <> "" Then
        CR.ReportFileName = App.PATH & "\SEGR0423-02.rpt"
    Else
        CR.ReportFileName = App.PATH & "\SEGR0423-01.rpt"
    End If

    CR.StoredProcParam(0) = ""    'Format(Data, "yyyymmdd")
    CR.StoredProcParam(1) = ""    'Format(Data, "yyyymmdd")
    CR.Formulas(0) = "Formula = 'Per�odo: De " & data & " At� " & data & "'"
    CR.StoredProcParam(2) = 0    '-- Voucher
    CR.StoredProcParam(3) = Remessa    '-- Voucher
    CR.Action = 1

    Screen.MousePointer = vbDefault
    StatusBar1.SimpleText = "Total de remessas a ser liberadas : " & lsvRemessas.ListItems.Count
    Exit Sub
    Resume
Trata_Erro:
    Screen.MousePointer = vbDefault
    Call TrataErroGeral("Imprimir", Me.name)

End Sub
Public Sub Carrega_Remessas()
    On Error GoTo Trata_Erro
    Dim lOrigem As String
    File1.Pattern = "*.*"
    'lOrigem = LeArquivoIni(App.PATH & "\ConsDevPremio.ini", "OPTIONS", "SAIDA_GERADOS")

    lOrigem = Busca_Path

    'teste
    'lOrigem = App.PATH

    If lOrigem = "*" Then
        Err.Number = 76
        lOrigem = ""
        File1.PATH = lOrigem
        File1.Refresh
        GoTo Trata_Erro
    End If
    
    lOrigem = "C:\QLD_ArqDevPremio\Gerados"
    File1.PATH = lOrigem
    File1.Refresh

    Call Preparar_Informacoes

    Exit Sub
    Resume

Trata_Erro:
    Screen.MousePointer = vbDefault
    If Err.Number = 76 Then    '-- Path not found
        Call TrataErroGeral("A Pasta " & lOrigem & " n�o esta disponivel.", "Carrega_Remessas")
    Else
        Call TrataErroGeral("Carrega_Remessas", Me.name)
    End If

End Sub

Public Sub Preparar_Informacoes()

    Dim Rst As rdoResultset
    Dim SQL As String
    Dim linha As String
    Dim Item As ListItem
    Dim i As Integer

    'Atualizando interface

    lsvRemessas.Visible = False

    'Configurando ListView

    lsvRemessas.View = lvwReport
    lsvRemessas.FullRowSelect = True
    lsvRemessas.HideSelection = False
    lsvRemessas.LabelEdit = lvwManual
    lsvRemessas.HotTracking = True

    'Configurando header do ListView

    lsvRemessas.ColumnHeaders.Clear

    lsvRemessas.ColumnHeaders.Add , , "Selecionar", 600
    lsvRemessas.ColumnHeaders.Add , , "Remessa", 6400
    'lsvRemessas.ColumnHeaders.Add , , "Voucher", 0 '900
    'lsvRemessas.ColumnHeaders.Add , , "Dt Aprova��o", 0 ' 1200
    'lsvRemessas.ColumnHeaders.Add , , "Pagamento", 0 '1100
    'lsvRemessas.ColumnHeaders.Add , , "Valor", 0 '1700

    'Limpando grid

    lsvRemessas.ListItems.Clear

    DoEvents

    If InStr(1, File1.PATH, "c:\SEGBR") Then
        Exit Sub
    End If

    For i = 0 To File1.ListCount - 1

        SQL = ""
        SQL = "Select Distinct a.arquivo_retorno_bb Remessa" & vbNewLine
        'SQL = SQL & "     a.arquivo_retorno_bb Remessa,b.voucher_id voucher, " & vbNewLine
        'SQL = SQL & "     c.dt_aprovacao,a.acerto_id Pagamento,c.val_total_acerto Valor " & vbNewLine
        SQL = SQL & "From" & vbNewLine
        SQL = SQL & "     ps_mov_cliente_tb a with (nolock) " & vbNewLine
        SQL = SQL & "     Inner Join ps_acerto_pagamento_tb b with (nolock) " & vbNewLine
        SQL = SQL & "           On b.acerto_id = a.acerto_id and b.cod_origem='RP'" & vbNewLine
        SQL = SQL & "     Inner Join ps_acerto_tb c with (nolock) " & vbNewLine
        SQL = SQL & "           On c.acerto_id = a.acerto_id" & vbNewLine
        SQL = SQL & "Where " & vbNewLine
        SQL = SQL & "     arquivo_retorno_bb= '" & File1.List(i) & "'"

        Set Rst = rdocn.OpenResultset(SQL)

        Do While Not Rst.EOF

            'Preenchendo Grid

            Set Item = lsvRemessas.ListItems.Add(, , "")

            Item.SubItems(1) = Rst!Remessa
            'Item.SubItems(2) = Rst!Voucher
            'Item.SubItems(3) = Format(Rst!Dt_Aprovacao, "dd/mm/yyyy")
            'Item.SubItems(4) = Rst!Pagamento
            'Item.SubItems(5) = Format(Replace(Val(Rst!Valor), ".", ","), "##,##0.#0")

            'Posicionando-se no pr�ximo registro

            Rst.MoveNext

        Loop

        Rst.Close
    Next i

    'Atualizando interface

    lsvRemessas.Visible = True

    'StatusBar1.SimpleText = "Total de remessas a ser liberadas : " & File1.ListCount  'lsvRemessas.ListItems.Count
    StatusBar1.SimpleText = "Total de remessas a ser liberadas : " & lsvRemessas.ListItems.Count
End Sub

Private Sub cmdAutorizar_Click()
    Dim Msg As String
    Dim StrSQL As String
    Dim intLinha As Integer
    Dim RsMovimentacao As rdoResultset
    Dim PreRemessa As Integer
    Dim bSomentePreRemessa As Boolean

    Dim sSQLRemessa As String

    On Error GoTo Trata_Erros

    Msg = MsgBox("Confirma autorizar devolu��es dos(s) arquivo(s) selecionado(s)?", vbYesNo + vbExclamation, "Caixa de Mensagem")    ' Mostrar� uma caixa de mensagem com bot�o Sim n�o, com titulo e com a foto exclama��o, para checar se clicou em yes fa�a isso.
    If Msg = vbYes Then


        bSomentePreRemessa = True
        StrSQL = ""

        'OS ITENS CHECADOS RECEBERAM "S" NO CAMPO REMESSA_LIBERADA
        With lsvRemessas
            sSQLRemessa = InicializaCargaDados_CriacaoTemporario
            Do While intLinha < .ListItems.Count
                intLinha = intLinha + 1
                If .ListItems.Item(intLinha).Checked Then
                    If .ListItems.Item(intLinha).Tag <> "" Then
                        PreRemessa = .ListItems.Item(intLinha).Tag
                        StrSQL = StrSQL & "                                                                                                                         " & vbNewLine
                        StrSQL = StrSQL & "Insert into #RemessaLiberada                                                                                             " & vbNewLine
                        StrSQL = StrSQL & "Select ps_mov_Cliente_PreRemessa_tb.Movimentacao_ID                                                                      " & vbNewLine
                        StrSQL = StrSQL & "     , ps_mov_Cliente_PreRemessa_tb.Controle_Interno_PreRemessa_ID                                                       " & vbNewLine
                        StrSQL = StrSQL & "     , 'S' Situacao                                                                                                      " & vbNewLine
                        StrSQL = StrSQL & "  From ps_mov_Cliente_PreRemessa_tb                                      ps_mov_Cliente_PreRemessa_tb WITH(NOLOCK)       " & vbNewLine
                        StrSQL = StrSQL & "  Join ps_mov_Cliente_tb                                                 ps_mov_Cliente_tb with(nolock)                  " & vbNewLine
                        StrSQL = StrSQL & "    On ps_mov_Cliente_tb.Movimentacao_ID                                 = ps_mov_Cliente_PreRemessa_tb.Movimentacao_ID  " & vbNewLine
                        'Inicio Autor: Rodrigo Moura N�o processar endosso de contestacao
                        StrSQL = StrSQL & "  join seguros_db.dbo.ps_mov_endosso_financeiro_tb  ps_mov_endosso_financeiro_tb  with (nolock)" & vbNewLine
                        StrSQL = StrSQL & "    On ps_mov_endosso_financeiro_tb.Movimentacao_ID                                 = ps_mov_Cliente_tb.Movimentacao_ID  " & vbNewLine
                        StrSQL = StrSQL & "  left join seguros_db.dbo.eds_contestacao_log_tb  eds_contestacao_log_tb  with (nolock)" & vbNewLine
                        StrSQL = StrSQL & "    On eds_contestacao_log_tb.proposta_id                                 = ps_mov_endosso_financeiro_tb.proposta_id " & vbNewLine
                        StrSQL = StrSQL & "    and eds_contestacao_log_tb.endosso_id                                 = ps_mov_endosso_financeiro_tb.endosso_id " & vbNewLine
                        'Fim Autor: Rodrigo Moura N�o processar endosso de contestacao

                        StrSQL = StrSQL & " Where ps_mov_Cliente_PreRemessa_tb.Controle_Interno_PreRemessa_ID       = " & PreRemessa & vbNewLine
                        StrSQL = StrSQL & "   And ps_mov_Cliente_tb.Remessa_Liberada                                is null " & vbNewLine
                        'Inicio Autor: Rodrigo Moura N�o processar endosso de contestacao
                        StrSQL = StrSQL & "   and eds_contestacao_log_tb.proposta_id is null " & vbNewLine
                        'Fim Autor: Rodrigo Moura N�o processar endosso de contestacao

                        StrSQL = StrSQL & " Exec seguros_db.dbo.SEGS11302_SPU " & PreRemessa & vbNewLine
                        'strSql = strSql & " Exec desenv_db.dbo.SEGS11302_SPU " & PreRemessa & vbNewLine
                        'rdocn.Execute (strSql)
                        bSomentePreRemessa = True
                    Else
                        bSomentePreRemessa = False
                    End If

                End If
            Loop
        End With

        If bSomentePreRemessa Then                  'FLAVIO.BEZERRA - 14413025 - ALTERAR O PROCESSO DE ENVIO DO ARQUIVO SEG452 - 24/06/2013
            StrSQL = sSQLRemessa & StrSQL
            'rdocn.Execute (strSql)
        End If

        If bSomentePreRemessa = False Then
            If (MsgBox("Essa rotina libera apenas as Pr� Remessas, deseja continuar?", vbYesNo)) = vbNo Then
                Exit Sub
            End If
        End If

        If StrSQL <> "" Then
            rdocn.BeginTrans
            'strSql = ""
            StrSQL = StrSQL & " Exec seguros_db.dbo.SEGS11158_SPU '" & cUserName & "', 'SEGP0423', 'S'"       'FLAVIO.BEZERRA 31/05/2013
            rdocn.Execute (StrSQL)
            rdocn.CommitTrans

            Call Carrega_Remessas
            Call Carrega_PreRemessas
        End If

        Exit Sub

    Else

        Exit Sub

    End If



Trata_Erros:
    rdocn.RollbackTrans
    Call TrataErroGeral("cmdAutorizar", Me.name)
End Sub

Private Sub cmdDetalhe_Click()
    Dim Cont As Integer
    Dim intChk As Integer
    Dim bSomentePreRemessa As Boolean

    bSomentePreRemessa = True

    With lsvRemessas
        Do While Cont < .ListItems.Count
            Cont = Cont + 1
            If .ListItems(Cont).Checked Then
                If .ListItems(Cont).Tag = "" Then
                    bSomentePreRemessa = False
                End If
                intChk = intChk + 1
            End If
        Loop
    End With

    If intChk > 1 Then
        MsgBox "N�o se pode selecionar mais de um lote para detalhamento."
    Else
        If bSomentePreRemessa And intChk > 0 Then
            frmRecusar.Show vbModal
        Else
            MsgBox "A rotina de detalhamento somente � permitida para os lotes de Pr� Remessa."
        End If

    End If

End Sub

Private Sub cmdMarcar_Click()
    Selecionar_Todos
End Sub

Private Sub cmdOk_Click(Index As Integer)
    Call Selecao_Remessas
End Sub

Private Sub cmdSair_Click(Index As Integer)
    Unload Me
End Sub

Private Sub Form_Load()

    On Error GoTo Trata_Erro

    glAmbiente_id = 3

    Call Conexao
    Call Seguranca("SEGP0423", "Relat�rio de Devolu��o Autorizadas")
    Me.Caption = "SEGP0423 - Devolu��es Autorizadas - " & Ambiente

    Call Carrega_Remessas
    'mathayde

    Call Carrega_PreRemessas


    Exit Sub
    Resume
Trata_Erro:
    Call TrataErroGeral("Form_Load", Me.name)
End Sub

Private Sub Form_Unload(Cancel As Integer)

    Call TerminaSEGBR

End Sub


Private Sub lsvRemessas_Click()
'    If lsvRemessas.SelectedItem.Checked = True Then
'        If lsvRemessas.SelectedItem.Tag <> "" Then
'            cmdDetalhe.Enabled = True
'        Else
'            cmdDetalhe.Enabled = False
'        End If
'    End If
End Sub

Private Sub lsvRemessas_DblClick()
'-- Imprimi item selecionado
    Dim data As String
    Dim Remessa As String
    Dim i As Integer

    Dim Item As ListItem

    If lsvRemessas.SelectedItem.Tag = "" Then   'FLAVIO.BEZERRA - 14413025 - ALTERAR O PROCESSO DE ENVIO DO ARQUIVO SEG452 - 26/06/2013
        Screen.MousePointer = vbHourglass

        DoEvents

        StatusBar1.SimpleText = "Aguarde, imprimindo remessa..."

        Marcar_Linha_Processada

        Imprimir lsvRemessas.SelectedItem
    End If

End Sub




'F.BEZERRA - Carrega_PreRemessas - FLOWBR14413025 03/12/2012
Public Sub Carrega_PreRemessas()
    On Error GoTo Trata_Erro

    Call InicializaCargaDados_CriacaoTemporario

    Call Preparar_Informacoes_Pre_Remessas

    Exit Sub

    Resume

Trata_Erro:
    Call TrataErroGeral("Carrega_PreRemessas", Me.name)

End Sub


'F.BEZERRA - FLOWBR14413025 03/12/2012
Public Function InicializaCargaDados_CriacaoTemporario() As String
    Dim StrSQL As String
    Dim rsTemp As rdoResultset

    On Error GoTo Trata_Erro

    StrSQL = ""
    StrSQL = StrSQL & "If isnull(OBJECT_ID('tempdb..#RemessaLiberada'),0) >0                   " & vbNewLine
    StrSQL = StrSQL & "BEGIN                                                                   " & vbNewLine
    StrSQL = StrSQL & "     DROP TABLE #RemessaLiberada                                        " & vbNewLine
    StrSQL = StrSQL & "END                                                                     " & vbNewLine
    StrSQL = StrSQL & "CREATE TABLE #RemessaLiberada                                           " & vbNewLine
    StrSQL = StrSQL & "           ( Movimentacao_ID                      Numeric(10)           " & vbNewLine
    StrSQL = StrSQL & "           , Controle_Interno_PreRemessa_ID       Numeric(10)           " & vbNewLine
    StrSQL = StrSQL & "           , Situacao                             VarChar(1)            " & vbNewLine
    StrSQL = StrSQL & "           )                                                            " & vbNewLine

    InicializaCargaDados_CriacaoTemporario = StrSQL

    Exit Function

Trata_Erro:
    Call TrataErroGeral("InicializaCargaDados_CriacaoTemporario", Me.name)

End Function


'F.BEZERRA - INICIO - FLOWBR14413025 03/12/2012'
Public Sub Preparar_Informacoes_Pre_Remessas()
    Dim Rst As rdoResultset
    Dim SQL As String
    Dim linha As String
    Dim Item As ListItem
    Dim i As Integer
    Dim Coluna As Integer

    On Error GoTo Trata_Erro

    With lsvRemessas
        .Visible = False

        'Configurando ListView
        .View = lvwReport
        .FullRowSelect = True
        .HideSelection = False
        .LabelEdit = lvwManual
        .HotTracking = True

        'Configurando header do ListView
        '.ColumnHeaders.Clear
        '.ColumnHeaders.Add , , "Selecionar", 600
        '.ColumnHeaders.Add , , "Lote (Controle Interno de Restitui��o)", 4400
        .ColumnHeaders.Add , , "Ramo", 1000    'Demanda 18346598 - edilson silva - 25/10/2016
        .ColumnHeaders.Add , , "Data de Emiss�o", 2400

        'Limpando grid
        '        .ListItems.Clear

        DoEvents

        SQL = ""
        SQL = SQL & "Select ps_mov_Cliente_PreRemessa_TB.Controle_Interno_PreRemessa_ID PreRemessa                      " & vbNewLine
        SQL = SQL & "       , ps_mov_Cliente_PreRemessa_TB.Dt_Inclusao                                                  " & vbNewLine
        SQL = SQL & "       , ps_mov_Cliente_PreRemessa_TB.Lote_Liberado                                                " & vbNewLine
        SQL = SQL & "       , p.ramo_id                                                                                 " & vbNewLine
        SQL = SQL & "  From Seguros_Db.Dbo.ps_mov_Cliente_PreRemessa_TB   ps_mov_Cliente_PreRemessa_TB   with(nolock)   " & vbNewLine
        SQL = SQL & "  Join Seguros_Db.Dbo.ps_mov_Cliente_TB              ps_mov_Cliente_TB      with(nolock)           " & vbNewLine
        SQL = SQL & "    On ps_mov_Cliente_PreRemessa_TB.Movimentacao_ID  = ps_mov_Cliente_TB.Movimentacao_ID           " & vbNewLine
        SQL = SQL & "   And ps_mov_Cliente_TB.Remessa_Liberada            is null                                       " & vbNewLine
        SQL = SQL & "   And ps_mov_Cliente_PreRemessa_TB.Controle_Interno_PreRemessa_ID is not null                     " & vbNewLine



        'Demanda 18346598 - edilson silva - 25/10/2016 - Inicio
        SQL = SQL & "  left join seguros_db.dbo.ps_mov_endosso_financeiro_tb pmef WITH (NOLOCK)                         " & vbNewLine
        SQL = SQL & "      ON ps_mov_Cliente_TB.movimentacao_id = pmef.movimentacao_id                                  " & vbNewLine
        SQL = SQL & "  left join seguros_db.dbo.proposta_tb p with (nolock)                                             " & vbNewLine
        SQL = SQL & "      on pmef.proposta_id = p.proposta_id                                                          " & vbNewLine
        'Demanda 18346598 - edilson silva - 25/10/2016 - fim
        'Inicio- Autor: Rodrigo Moura Adicionado para o endosso de contesta��o----------------------------------
        SQL = SQL & "  left join seguros_db.dbo.eds_contestacao_log_tb eds_contestacao_log_tb with (nolock)             " & vbNewLine
        SQL = SQL & "      on eds_contestacao_log_tb.proposta_id =  pmef.proposta_id                   " & vbNewLine
        SQL = SQL & "      and eds_contestacao_log_tb.endosso_id =  pmef.endosso_id                    " & vbNewLine
        SQL = SQL & " Where eds_contestacao_log_tb.proposta_id Is Null                                                  " & vbNewLine
        'Fim- Autor: Rodrigo Moura Adicionado para o endosso de contesta��o----------------------------------

        SQL = SQL & " group by Controle_Interno_PreRemessa_ID, ps_mov_Cliente_PreRemessa_TB.Dt_Inclusao                 " & vbNewLine
        SQL = SQL & "     , ps_mov_Cliente_PreRemessa_TB.Lote_Liberado                                                  " & vbNewLine

        'Demanda 18346598 - edilson silva - 25/10/2016 - Inicio
        SQL = SQL & "     ,p.ramo_id                                                                                    " & vbNewLine
        'Demanda 18346598 - edilson silva - 25/10/2016 - fim

        Set Rst = rdocn.OpenResultset(SQL)

        Do While Not Rst.EOF

            'Preenchendo Grid
            Set Item = .ListItems.Add(, , "")
            Item.Tag = Right("00000" & Rst!PreRemessa, 4)
            Item.SubItems(1) = "SEG452." & Right("00000" & Rst!PreRemessa, 4) & " (Controle Interno de Restitui��o)"
            'Demanda 18346598 - edilson silva - 25/10/2016 - Inicio
            'Item.SubItems(2) = Format(Rst!Dt_Inclusao, "dd/mm/yyyy")
            Item.SubItems(2) = Rst!ramo_id
            Item.SubItems(3) = Format(Rst!Dt_Inclusao, "dd/mm/yyyy")
            'Demanda 18346598 - edilson silva - 25/10/2016 - Fim


            If Rst!Lote_Liberado = "N" Then
                For Coluna = 1 To Item.ListSubItems.Count
                    Item.ListSubItems(Coluna).ForeColor = 33023
                Next
            End If

            'Posicionando-se no pr�ximo registro
            Rst.MoveNext

        Loop
        'Atualizando interface
        .Visible = True
        StatusBar1.SimpleText = "Total de Lote a ser liberados : " & .ListItems.Count
    End With


    Exit Sub

Trata_Erro:
    Call TrataErroGeral("Preparar_Informacoes_Pre_Remessas", Me.name)
End Sub


Public Sub Imprimir_PreRemessa(data As String, PreRemessa As String)

    On Error GoTo Trata_Erro

    DoEvents
    Screen.MousePointer = vbHourglass
    CR.Reset
    CR.Connect = "DSN=RPT_SEGBR;UID=" & SIS_usuario & ";PWD=" & UCase(SIS_senha)    'conex�o ODBC
    'CR.ReportFileName = App.PATH & "\PreRemessa.rpt"
    CR.ReportFileName = App.PATH & "\SEGR0423-02.rpt"    'FLAVIO.BEZERRA 31/05/2013

    CR.StoredProcParam(0) = PreRemessa    'PreRemessa
    CR.Formulas(0) = "Formula = 'Remessa: SEG452." & PreRemessa & " (Controle Interno de Restitui��o)'"
    CR.Action = 1

    Screen.MousePointer = vbDefault

    StatusBar1.SimpleText = "Total de Pre-Remessas a ser liberadas: " & lsvRemessas.ListItems.Count
    Exit Sub
    Resume

Trata_Erro:
    Screen.MousePointer = vbDefault
    Call TrataErroGeral("Imprimir", Me.name)

End Sub
