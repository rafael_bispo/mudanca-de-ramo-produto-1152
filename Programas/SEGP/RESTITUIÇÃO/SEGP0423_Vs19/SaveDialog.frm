VERSION 5.00
Begin VB.Form SaveFileDialog 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Save As"
   ClientHeight    =   4455
   ClientLeft      =   2760
   ClientTop       =   3750
   ClientWidth     =   6885
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4455
   ScaleWidth      =   6885
   ShowInTaskbar   =   0   'False
   Begin VB.Frame Frame1 
      Height          =   4455
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   6855
      Begin VB.CommandButton btnCancelar 
         Cancel          =   -1  'True
         Caption         =   "Cancelar"
         Height          =   375
         Left            =   5280
         TabIndex        =   5
         Top             =   3840
         Width           =   1335
      End
      Begin VB.CommandButton btnSalvar 
         Caption         =   "Salvar"
         Height          =   375
         Left            =   3720
         TabIndex        =   4
         Top             =   3840
         Width           =   1335
      End
      Begin VB.TextBox tboxFileName 
         Height          =   375
         Left            =   240
         TabIndex        =   3
         Text            =   "agendamentos.xls"
         Top             =   3840
         Width           =   3255
      End
      Begin VB.DirListBox saveDirList 
         Height          =   2790
         Left            =   240
         TabIndex        =   2
         Top             =   840
         Width           =   6495
      End
      Begin VB.DriveListBox saveDialogDriveList 
         Height          =   315
         Left            =   240
         TabIndex        =   1
         Top             =   360
         Width           =   6495
      End
   End
End
Attribute VB_Name = "SaveFileDialog"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'vitor.cabral - Nova Consultoria - 10/10/2013 - Demanda 17886578 � Registro das avalia��es na recep��o de parcelas
Option Explicit

Private driveName As String
Private filePath As String
Private FileName As String
Private confirmSaving As Boolean


Public Property Get SharedDriveName() As String
    SharedDriveName = driveName
End Property

Public Property Let SharedDriveName(ByVal drive As String)
    driveName = drive
End Property

Public Property Get SharedFilePath() As String
    SharedFilePath = filePath
End Property

Public Property Let SharedFilePath(ByVal PATH As String)
    filePath = PATH
End Property

Public Property Get SharedFileName() As String
    SharedFileName = FileName
End Property

Public Property Let SharedFileName(ByVal file As String)
    FileName = file
End Property

Public Property Get Confirmacao() As Boolean
    Confirmacao = confirmSaving
End Property

Public Property Let Confirmacao(ByVal confirm As Boolean)
    confirmSaving = confirm
End Property

Private Sub btnCancelar_Click()

    SharedDriveName = ""
    SharedFilePath = ""
    SharedFileName = ""

    Confirmacao = False

    Me.Hide

End Sub

Private Sub btnSalvar_Click()

    Dim searchChar As String

    SharedDriveName = saveDialogDriveList.drive
    SharedFilePath = saveDirList.PATH

    If InStr(1, tboxFileName.Text, searchChar) Then
        SharedFileName = Split(tboxFileName.Text, ".", 2)(0)
    Else
        SharedFileName = tboxFileName.Text
    End If

    Confirmacao = True

    Me.Hide

End Sub

Private Sub Form_Load()
    Dim data As String
    saveDialogDriveList.Visible = True
    saveDirList.Visible = True
    Confirmacao = False

    '-------- Brauner Rangel - Nova Conaultoria ---------
    data = Year(Data_Sistema)
    data = data & "-" & Month(Data_Sistema)
    data = data & "-" & Day(Data_Sistema)

    tboxFileName.Text = data & "- Devolu��es Autorizadas "
    '-------- Nova Consultoria ----------

End Sub

Private Sub saveDialogDriveList_Change()

    saveDirList.PATH = Split(saveDialogDriveList.drive, ":", 2)(0) & ":\"

End Sub
