VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.2#0"; "MSCOMCTL.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.ocx"
Begin VB.Form frmRecusar 
   Caption         =   "SEGP0423 - Devolu��es Autorizadas"
   ClientHeight    =   7155
   ClientLeft      =   1995
   ClientTop       =   2115
   ClientWidth     =   12825
   BeginProperty Font 
      Name            =   "MS Sans Serif"
      Size            =   9.75
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   ScaleHeight     =   7155
   ScaleWidth      =   12825
   Begin VB.CommandButton cmdExcel 
      Caption         =   "Exportar"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   390
      Left            =   7785
      TabIndex        =   8
      Top             =   6360
      Width           =   1485
   End
   Begin VB.CommandButton cmdDesfazerRecusa 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   390
      Left            =   6075
      TabIndex        =   7
      Top             =   6360
      Visible         =   0   'False
      Width           =   1485
   End
   Begin VB.CommandButton cmdRecusar 
      Caption         =   "Recusar"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   390
      Left            =   9480
      TabIndex        =   4
      Top             =   6360
      Width           =   1485
   End
   Begin VB.CheckBox chkSelecionarTodos 
      Caption         =   "Selecionar todos"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   240
      TabIndex        =   3
      Top             =   480
      Width           =   1695
   End
   Begin VB.CommandButton cmdSair 
      Caption         =   "&Sair"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   390
      Left            =   11160
      TabIndex        =   1
      Top             =   6360
      Width           =   1485
   End
   Begin MSComctlLib.StatusBar stbPrincipal 
      Align           =   2  'Align Bottom
      Height          =   375
      Left            =   0
      TabIndex        =   0
      Top             =   6780
      Width           =   12825
      _ExtentX        =   22622
      _ExtentY        =   661
      Style           =   1
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   9596
            MinWidth        =   9596
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSFlexGridLib.MSFlexGrid grdPrincipal 
      Height          =   5295
      Left            =   135
      TabIndex        =   2
      Top             =   840
      Width           =   12615
      _ExtentX        =   22251
      _ExtentY        =   9340
      _Version        =   393216
      Cols            =   13
      FixedCols       =   0
      FormatString    =   $"FrmRecusar.frx":0000
   End
   Begin VB.Label lblLote 
      Height          =   255
      Left            =   1200
      TabIndex        =   6
      Top             =   120
      Width           =   5415
   End
   Begin VB.Label lblLoteEscolhido 
      Caption         =   "Arquivo:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   240
      TabIndex        =   5
      Top             =   120
      Width           =   975
   End
End
Attribute VB_Name = "frmRecusar"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private strLote As String
Private bolChkSeleciona As Boolean

Private Const strChecked = "�"      'Na fonte Wingdings � um Checkbox Checado
Private Const strUnChecked = "q"    'Na fonte Wingdings � um Checkbox n�o Checado


'F.BEZERRA - FLOWBR14413025 03/12/2012
Private Sub CarregaInformacoes()
    Dim StrSQL As String
    Dim intLinha As Integer
    Dim RsGridAutoriza As rdoResultset

    On Error GoTo TrataErros

    Screen.MousePointer = vbHourglass
    'rdocn.BeginTrans
    'strSql = strSql & "Exec desenv_db.dbo.DetalhaPreRemessa_SPS " & strLote & vbNewLine
    StrSQL = StrSQL & "Exec seguros_db.dbo.SEGS11157_SPS " & strLote & vbNewLine    'FLAVIO.BEZERRA 31/05/2013

    Set RsGridAutoriza = rdocn.OpenResultset(StrSQL)

    LimpaGrid_GrdPrincipal

    intLinha = 1

    With grdPrincipal
        Do While Not RsGridAutoriza.EOF

            .Rows = .Rows + 1
            .TextMatrix(intLinha, 0) = strUnChecked
            ColocarCheck grdPrincipal, .Rows - 1, 0, strUnChecked
            .TextMatrix(intLinha, 1) = IIf(IsNull(RsGridAutoriza!Proposta_BB), "", RsGridAutoriza!Proposta_BB)
            .TextMatrix(intLinha, 2) = IIf(IsNull(RsGridAutoriza!proposta_id), "", RsGridAutoriza!proposta_id)
            .TextMatrix(intLinha, 3) = IIf(IsNull(RsGridAutoriza!Val_Restituicao_Estimado), "0,00", RsGridAutoriza!Val_Restituicao_Estimado)
            .TextMatrix(intLinha, 4) = IIf(IsNull(RsGridAutoriza!Produto_ID), "", RsGridAutoriza!Produto_ID)
            .TextMatrix(intLinha, 5) = IIf(IsNull(RsGridAutoriza!Cliente_ID), "", RsGridAutoriza!Cliente_ID)
            .TextMatrix(intLinha, 6) = IIf(IsNull(RsGridAutoriza!Nome), "", RsGridAutoriza!Nome)

            .TextMatrix(intLinha, 7) = IIf(IsNull(RsGridAutoriza!Dt_Emissao), "", RsGridAutoriza!Dt_Emissao)
            .TextMatrix(intLinha, 8) = IIf(IsNull(RsGridAutoriza!InicioVig), "", RsGridAutoriza!InicioVig)
            .TextMatrix(intLinha, 9) = IIf(IsNull(RsGridAutoriza!FimVig), "", RsGridAutoriza!FimVig)
            .TextMatrix(intLinha, 10) = IIf(IsNull(RsGridAutoriza!Descricao), "", RsGridAutoriza!Descricao)
            .TextMatrix(intLinha, 11) = IIf(IsNull(RsGridAutoriza!Movimentacao_ID), "", RsGridAutoriza!Movimentacao_ID)
            .TextMatrix(intLinha, 12) = IIf(IsNull(RsGridAutoriza!Remessa_Liberada), "", RsGridAutoriza!Remessa_Liberada)

            If RsGridAutoriza!Remessa_Liberada = "N" Then
                MudaCorLinha (intLinha)
            End If
            RsGridAutoriza.MoveNext
            intLinha = intLinha + 1

        Loop
    End With
    stbPrincipal.SimpleText = "Total de registros encontrados: " & (intLinha - 1)
    Screen.MousePointer = vbNormal


    Exit Sub

TrataErros:

    Call TrataErroGeral("CarregaInformacoes", Me.name)
    Resume
End Sub

Private Sub chkSelecionarTodos_Click()
    Dim iContador As Integer

    With grdPrincipal
        If chkSelecionarTodos.Value Then
            Do While iContador < .Rows - 1
                iContador = iContador + 1
                .TextMatrix(iContador, 0) = strChecked
            Loop
            bolChkSeleciona = False
        Else
            Do While iContador < .Rows - 1 And Not bolChkSeleciona
                iContador = iContador + 1
                .TextMatrix(iContador, 0) = strUnChecked
            Loop
            bolChkSeleciona = False
        End If
    End With
End Sub


Private Sub cmdDesfazerRecusa_Click()
    Dim StrSQL As String
    Dim intLinha As Integer

    On Error GoTo TrataErros

    If MsgBox("Confirma a a��o de desfazer as recusas selecionadas?", vbYesNo) = vbYes Then

        Screen.MousePointer = vbHourglass

        StrSQL = ""
        StrSQL = StrSQL & "If isnull(OBJECT_ID('tempdb..#RemessaLiberada'),0) >0                   " & vbNewLine
        StrSQL = StrSQL & "BEGIN                                                                   " & vbNewLine
        StrSQL = StrSQL & "     DROP TABLE #RemessaLiberada                                        " & vbNewLine
        StrSQL = StrSQL & "END                                                                     " & vbNewLine
        StrSQL = StrSQL & "CREATE TABLE #RemessaLiberada                                           " & vbNewLine
        StrSQL = StrSQL & "           ( Movimentacao_ID                      Numeric(10)           " & vbNewLine
        StrSQL = StrSQL & "           , Controle_Interno_PreRemessa_ID       Numeric(10)           " & vbNewLine
        StrSQL = StrSQL & "           , Situacao                             VarChar(1)            " & vbNewLine
        StrSQL = StrSQL & "           )                                                            " & vbNewLine

        rdocn.Execute (StrSQL)

        'OS ITENS CHECADOS TEM A SITUA��O "N" NO CAMPO REMESSA_LIBERADA SER�O ALTERADOS PARA SITUA��O "NULL"
        With grdPrincipal
            Do While intLinha < .Rows - 1
                intLinha = intLinha + 1
                If .TextMatrix(intLinha, 0) = strChecked And .TextMatrix(intLinha, 12) = "N" Then
                    StrSQL = ""
                    StrSQL = StrSQL & "   Insert Into #RemessaLiberada                            " & vbNewLine
                    StrSQL = StrSQL & "               ( Movimentacao_ID                           " & vbNewLine
                    StrSQL = StrSQL & "               , Controle_Interno_PreRemessa_ID            " & vbNewLine
                    StrSQL = StrSQL & "               , Situacao                                  " & vbNewLine
                    StrSQL = StrSQL & "               )                                           " & vbNewLine
                    StrSQL = StrSQL & "       Values                                              " & vbNewLine
                    StrSQL = StrSQL & "               (" & .TextMatrix(intLinha, 11) & vbNewLine
                    StrSQL = StrSQL & "               , " & strLote & vbNewLine
                    StrSQL = StrSQL & "               , Null                                      " & vbNewLine
                    StrSQL = StrSQL & "               )                                           " & vbNewLine

                    rdocn.Execute StrSQL
                End If
            Loop
        End With

        sSQL = ""
        'sSQL = sSQL & " Exec desenv_db.dbo.Atualiza_Remessa_SPU"
        sSQL = sSQL & " Exec seguros_db.dbo.SEGS11158_SPU"  'FLAVIO.BEZERRA 31/05/2013
        rdocn.Execute sSQL
        rdocn.CommitTrans

        Screen.MousePointer = vbNormal
        CarregaInformacoes
    End If
    Exit Sub

TrataErros:
    rdocn.RollbackTrans
    Screen.MousePointer = vbNormal
    Call TrataErroGeral("cmdRecusar", Me.name)

End Sub

Private Sub cmdExcel_Click()

'------Brauner Rangel - Nova Consultoria 10/12/2013-----

    On Local Error GoTo Trata_Erro
    Dim i, j, linha As Integer
    Dim dialogo As New SaveFileDialog
    Dim caminho As String

    dialogo.Show vbModal

    If dialogo.Confirmacao Then

        If dialogo.SharedFilePath <> "" Then
            If Right$(dialogo.SharedFilePath, 1) <> "\" Then
                caminho = dialogo.SharedFilePath & "\" & dialogo.SharedFileName
            Else
                caminho = dialogo.SharedFilePath & dialogo.SharedFileName
            End If
        End If
    Else: Exit Sub
    End If

    linha = 0

    Set EApp = CreateObject("excel.application")
    Set EwkB = EApp.Workbooks.Add
    Set EwkS = EwkB.Sheets(1)

    EApp.Application.Visible = False

    If tabAtual = 0 Then



        Dim Coluna As Integer
        Coluna = grdPrincipal.FixedCols + 1

        'primeira passada, seta o nome das colunas
        grdPrincipal.Row = 1
        For i = grdPrincipal.FixedCols + 1 To grdPrincipal.Cols - 1    'remove o campo de marcacao
            If (i <> 5) And (i <> 11) And (i <> 12) Then
                grdPrincipal.Col = i
                EwkS.Range(Chr(64 + Coluna) & "1").FormulaR1C1 = grdPrincipal.TextMatrix(0, grdPrincipal.Col)
                EwkS.Range(Chr(64 + Coluna) & "1").Font.Bold = True
                If i = 10 Then

                    EwkS.Range(Chr(64 + Coluna) & "1").ColumnWidth = 50
                Else
                    EwkS.Range(Chr(64 + Coluna) & "1").ColumnWidth = 25
                End If
                Coluna = Coluna + 1
            End If

        Next i

        'segunda passada, faz a insercao da grid

        For j = 1 To grdPrincipal.Rows - 1


            Coluna = grdPrincipal.FixedCols + 1
            grdPrincipal.Row = j
            grdPrincipal.Col = 0
            ' If grdPrincipal.CellPicture <> 0 Then
            linha = linha + 1
            For i = grdPrincipal.FixedCols + 1 To grdPrincipal.Cols - 1    'remove o campo de marcacao
                grdPrincipal.Col = i
                If (grdPrincipal.Col <> 5) And (grdPrincipal.Col <> 11) And (grdPrincipal.Col <> 12) Then
                    '  If (i = 4) Or (i = 6) Then
                    '     EwkS.Range(Chr(64 + coluna) & linha + 1).FormulaR1C1 = Format(grdPrincipal.TextMatrix(j, grdPrincipal.Col), "dd/mm/yyyy")
                    '       EwkS.Range(Chr(64 + coluna) & linha + 1).HorizontalAlignment = xlRight
                    '  Else
                    EwkS.Range(Chr(64 + Coluna) & linha + 1).FormulaR1C1 = grdPrincipal.TextMatrix(j, grdPrincipal.Col)

                    Coluna = Coluna + 1

                End If
            Next i
            ' End If
        Next j



    End If

    EwkB.SaveAs caminho, xlNormal

    EwkB.Close
    EApp.Quit

    Set EApp = Nothing
    Set EwkB = Nothing
    Set EwkS = Nothing

    MsgBox ("Arquivo gerado com sucesso!")

    Exit Sub
Trata_Erro:
    Call TratarErro("cmbExcel_Click", Me.name)

    '------------- Nova consultoria----------

End Sub

'F.BEZERRA - FLOWBR14413025 03/12/2012
Private Sub cmdRecusar_Click()
    Dim StrSQL As String
    Dim intLinha As Integer

    On Error GoTo TrataErros

    If MsgBox("Confirma a recusa das restitui��es selecionadas?", vbYesNo) = vbYes Then

        Screen.MousePointer = vbHourglass

        StrSQL = ""
        StrSQL = StrSQL & "If isnull(OBJECT_ID('tempdb..#RemessaLiberada'),0) >0                   " & vbNewLine
        StrSQL = StrSQL & "BEGIN                                                                   " & vbNewLine
        StrSQL = StrSQL & "     DROP TABLE #RemessaLiberada                                        " & vbNewLine
        StrSQL = StrSQL & "END                                                                     " & vbNewLine
        StrSQL = StrSQL & "CREATE TABLE #RemessaLiberada                                           " & vbNewLine
        StrSQL = StrSQL & "           ( Movimentacao_ID                      Numeric(10)           " & vbNewLine
        StrSQL = StrSQL & "           , Controle_Interno_PreRemessa_ID       Numeric(10)           " & vbNewLine
        StrSQL = StrSQL & "           , Situacao                             VarChar(1)            " & vbNewLine
        StrSQL = StrSQL & "           )                                                            " & vbNewLine

        'rdocn.Execute (strSql)

        'OS ITENS CHECADOS RECEBERAM "N" NO CAMPO REMESSA_LIBERADA
        With grdPrincipal
            Do While intLinha < .Rows - 1
                intLinha = intLinha + 1
                If .TextMatrix(intLinha, 0) = strChecked Then
                    'strSql = ""
                    StrSQL = StrSQL & "   Insert Into #RemessaLiberada                            " & vbNewLine
                    StrSQL = StrSQL & "               ( Movimentacao_ID                           " & vbNewLine
                    StrSQL = StrSQL & "               , Controle_Interno_PreRemessa_ID            " & vbNewLine
                    StrSQL = StrSQL & "               , Situacao                                  " & vbNewLine
                    StrSQL = StrSQL & "               )                                           " & vbNewLine
                    StrSQL = StrSQL & "       Values                                              " & vbNewLine
                    StrSQL = StrSQL & "               (" & .TextMatrix(intLinha, 11) & vbNewLine
                    StrSQL = StrSQL & "               , " & strLote & vbNewLine
                    StrSQL = StrSQL & "               , 'N'                                       " & vbNewLine
                    StrSQL = StrSQL & "               )                                           " & vbNewLine

                    'rdocn.Execute strSql

                    '.RemoveItem (intLinha)

                End If
            Loop
        End With

        rdocn.BeginTrans
        'sSQL = ""
        sSQL = sSQL & " Exec seguros_db.dbo.SEGS11158_SPU '" & cUserName & "', 'SEGP0423', 'N'"               'FLAVIO.BEZERRA 31/05/2013
        rdocn.Execute StrSQL + sSQL
        rdocn.CommitTrans

        Screen.MousePointer = vbNormal

        CarregaInformacoes
    End If
    Exit Sub

TrataErros:
    rdocn.RollbackTrans
    Screen.MousePointer = vbNormal
    Call TrataErroGeral("cmdRecusar", Me.name)

End Sub

Private Sub cmdSair_Click()

    Unload Me
End Sub

Private Sub Form_Load()

    On Error GoTo TrataErros

    InicializaInterfaceLocal

    CarregaInformacoes

    Exit Sub

TrataErros:

    Call TrataErroGeral("Form_Load", Me.name)

End Sub

'F.BEZERRA - FLOWBR14413025 03/12/2012
Private Sub InicializaInterfaceLocal()

    On Error GoTo TrataErros

    Screen.MousePointer = vbHourglass

    Call CentraFrm(Me)

    strLote = BuscaLoteMarcado
    lblLote = "SEG452." & Right("00000" & strLote, 4) & " (Controle Interno de Restitui��o)"

    Screen.MousePointer = vbNormal

    Exit Sub
TrataErros:

    Call TrataErroGeral("InicializaInterfaceLocal", Me.name)

End Sub


Private Function BuscaLoteMarcado() As String
    Dim Cont As Integer

    Cont = 1
    Do While Cont <= FrmDevAutorizadas.lsvRemessas.ListItems.Count
        If FrmDevAutorizadas.lsvRemessas.ListItems.Item(Cont).Checked Then
            BuscaLoteMarcado = FrmDevAutorizadas.lsvRemessas.ListItems.Item(Cont).Tag
            Exit Function
        End If
        Cont = Cont + 1
    Loop

End Function


Private Sub LimpaGrid_GrdPrincipal()    'F.BEZERRA - FLOWBR14413025 03/12/2012

    On Error GoTo TrataErros

    With grdPrincipal
        '   .FormatString = "^X   | Proposta BB | Proposta Id | Val.Restitui��o     | Produto | Cliente_ID | Cliente                                                 | Emiss�o                | In�cio Vig�ncia                | Fim Vig�ncia                | Tipo de Devolu��o                                      | Movimentacao_ID|Situacao "
        .ColWidth(5) = 0
        .ColWidth(11) = 0
        .ColWidth(12) = 0


        .Rows = 1
    End With

    Exit Sub
TrataErros:

    Call TrataErroGeral("LimpaGrid_GrdPrincipal", Me.name)

End Sub

Public Sub ColocarCheck(ByRef grdGrid As Object _
     , ByVal bytLinha As Integer _
     , ByVal bytColuna As Integer _
     , Optional strFixo As String)

    With grdGrid
        .Row = bytLinha
        .Col = bytColuna
        .CellFontName = "Wingdings"
        .CellFontSize = 10
        .CellAlignment = flexAlignCenterCenter
        .CellAlignment = vbCenter
        If LenB(Trim(strFixo)) <> 0 Then
            .Text = strFixo
        Else
            If .Text = strUnChecked Then
                .Text = strChecked
            Else
                .Text = strUnChecked
            End If
        End If
    End With

    Exit Sub

Trata_Erro:
    Exit Sub
End Sub


Private Sub Form_Unload(Cancel As Integer)
    FrmDevAutorizadas.Carrega_Remessas
    FrmDevAutorizadas.Carrega_PreRemessas
End Sub

Private Sub grdPrincipal_Click()

'-------------- Brauner Rangel - Nova Consultoria ------------

    Dim bytSelecionados As Byte
    Dim lCol As Long
    lCol = grdPrincipal.ColSel
    With grdPrincipal

        Dim l As Long
        Dim d As Long
        d = .RowSel
        l = .Row



        If .Rows > 1 Then
            If .ColSel = 0 Then
                If .TextMatrix(.RowSel, 0) = strChecked Then
                    .TextMatrix(.RowSel, 0) = strUnChecked

                Else
                    .TextMatrix(.RowSel, 0) = strChecked

                End If
            Else
                If l = 1 And d = .Rows - 1 Then
                    .Col = lCol

                    If (.ColSel <> 8) And (.ColSel <> 7) And (.ColSel <> 9) Then
                        .Sort = flexSortGenericAscending



                    Else
                        If (.ColSel = 8) Then

                            ordenaData (8)

                        ElseIf (.ColSel = 7) Then

                            ordenaData (7)

                        Else

                            ordenaData (9)

                        End If

                    End If



                End If
            End If
            For i = 1 To .Rows - 1
                If .TextMatrix(i, 0) = strChecked Then
                    bytSelecionados = bytSelecionados + 1
                    bolChkSeleciona = True
                End If
            Next
            chkSelecionarTodos.Value = IIf((bytSelecionados = .Rows - 1), 1, 0)
        End If
        If .TextMatrix(.RowSel, 12) = "N" Then
            .BackColorSel = RGB(64, 0, 0)
        Else
            .BackColorSel = -2147483635
        End If
    End With







    '----------Nova Consultoria --------------

End Sub

Private Sub MudaCorLinha(intLinha As Integer)
    With grdPrincipal
        .Row = intLinha
        Do While intCol < .Cols - 1
            intCol = intCol + 1
            .Col = intCol
            '.CellBackColor = vbRed
            .CellBackColor = vbYellow
        Loop
    End With
End Sub


Private Sub ordenaData(Coluna As Integer)
    Dim Ro As Integer
    Dim SortCol As Integer
    Dim SortDate As Double

    'inclui a coluna que atuara como a chave de classifica��o
    grdPrincipal.Cols = grdPrincipal.Cols + 1
    SortCol = grdPrincipal.Cols - 1
    grdPrincipal.ColWidth(SortCol) = 0   'a coluna invis�vel
    'calcula os novos valores e preenche a coluna
    For Ro = 1 To grdPrincipal.Rows - 1
        If grdPrincipal.TextMatrix(Ro, Coluna) <> "" Then
            SortDate = DateValue(grdPrincipal.TextMatrix(Ro, Coluna))   ' define a coluna que ser� classificada
        End If
        grdPrincipal.TextMatrix(Ro, SortCol) = SortDate
    Next Ro
    'efetua a classificacao
    grdPrincipal.Col = SortCol    'define o criterio
    grdPrincipal.Sort = flexSortNumericDescending


    grdPrincipal.Cols = grdPrincipal.Cols - 1
End Sub
