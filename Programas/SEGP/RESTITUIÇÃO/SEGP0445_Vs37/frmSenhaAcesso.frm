VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.2#0"; "MSCOMCTL.OCX"
Begin VB.Form frmSenhaAcesso 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "SEGP0420 - Aprova��o de Pagamentos de Sinistros para o SISBB - DBO"
   ClientHeight    =   3765
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6735
   Icon            =   "frmSenhaAcesso.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3765
   ScaleWidth      =   6735
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton cmdAcesso 
      Caption         =   "&Sair"
      Height          =   375
      Index           =   1
      Left            =   4680
      TabIndex        =   8
      Top             =   3015
      Width           =   1905
   End
   Begin VB.CommandButton cmdAcesso 
      Caption         =   "&Acessar Confirma��o"
      Height          =   375
      Index           =   0
      Left            =   2655
      TabIndex        =   7
      Top             =   3015
      Width           =   1905
   End
   Begin VB.Frame fraSenhaAcesso 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2760
      Left            =   135
      TabIndex        =   0
      Top             =   135
      Width           =   6450
      Begin VB.ComboBox cboEmpresa 
         Height          =   315
         Left            =   1530
         Style           =   2  'Dropdown List
         TabIndex        =   6
         Top             =   2295
         Width           =   2625
      End
      Begin VB.ComboBox cboAmbiente 
         Height          =   315
         Left            =   1530
         Style           =   2  'Dropdown List
         TabIndex        =   4
         Top             =   1845
         Width           =   2625
      End
      Begin VB.TextBox txtSenha 
         BackColor       =   &H8000000F&
         Enabled         =   0   'False
         Height          =   285
         IMEMode         =   3  'DISABLE
         Index           =   0
         Left            =   1530
         MaxLength       =   14
         PasswordChar    =   "*"
         TabIndex        =   2
         Text            =   "12345678901234"
         Top             =   1440
         Width           =   1500
      End
      Begin VB.TextBox txtChave 
         Appearance      =   0  'Flat
         BackColor       =   &H8000000F&
         BorderStyle     =   0  'None
         Height          =   285
         Left            =   1530
         Locked          =   -1  'True
         MaxLength       =   8
         TabIndex        =   14
         Text            =   "E1079530"
         Top             =   1080
         Width           =   1455
      End
      Begin VB.TextBox txtCPF 
         Appearance      =   0  'Flat
         BackColor       =   &H8000000F&
         BorderStyle     =   0  'None
         Height          =   285
         Left            =   1530
         Locked          =   -1  'True
         MaxLength       =   14
         TabIndex        =   12
         Text            =   "277.560.858-25"
         Top             =   675
         Width           =   1455
      End
      Begin VB.TextBox txtUsuario 
         Appearance      =   0  'Flat
         BackColor       =   &H8000000F&
         BorderStyle     =   0  'None
         Height          =   285
         Left            =   1530
         Locked          =   -1  'True
         MaxLength       =   50
         TabIndex        =   10
         Text            =   "JOS� RICARDO PEREIRA JUNIOR"
         Top             =   270
         Width           =   4740
      End
      Begin VB.Label lblAcesso 
         Caption         =   "Empresa.........:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   5
         Left            =   180
         TabIndex        =   5
         Top             =   2340
         Width           =   1320
      End
      Begin VB.Label lblAcesso 
         Caption         =   "Ambiente........:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   4
         Left            =   180
         TabIndex        =   3
         Top             =   1890
         Width           =   1320
      End
      Begin VB.Label lblAcesso 
         Caption         =   "Senha SEGBR:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   3
         Left            =   180
         TabIndex        =   1
         Top             =   1485
         Width           =   1320
      End
      Begin VB.Label lblAcesso 
         Caption         =   "Chave SEGUR:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   2
         Left            =   180
         TabIndex        =   13
         Top             =   1080
         Width           =   1320
      End
      Begin VB.Label lblAcesso 
         Caption         =   "CPF...............:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   1
         Left            =   180
         TabIndex        =   11
         Top             =   675
         Width           =   1320
      End
      Begin VB.Label lblAcesso 
         Caption         =   "Usu�rio..........:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   0
         Left            =   180
         TabIndex        =   9
         Top             =   270
         Width           =   1320
      End
   End
   Begin MSComctlLib.StatusBar stsbarAcesso 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   15
      Top             =   3480
      Width           =   6735
      _ExtentX        =   11880
      _ExtentY        =   503
      Style           =   1
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmSenhaAcesso"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim uSQL    As String
Dim rdoRsTB As ADODB.Recordset

Private Sub cmdAcesso_Click(Index As Integer)
    
    Select Case Index
           
        Case 0 'OK
            '           If Trim(txtSenha(0).Text) = "" Then
            '               MsgBox "Digite a Senha !", vbOKOnly + vbExclamation, "!!! Aten��o !!!"
            '               txtSenha(0).SetFocus
            '               Exit Sub
            '           End If
            '           If Not Valida_Senha() Then
            '               txtSenha(0).SetFocus
            '               Exit Sub
            '           End If
            '           AChave_SISBB = txtChave.Text
            Unload Me
            frmMain.Show
        Case 1 'Sair
            Call TerminaSEGBR
    End Select
        
End Sub

Private Sub Form_Load()

    If Not Trata_Parametros(Command) Then
       ' Call FinalizarAplicacao
    End If

    Me.Caption = "SEGP0445 - Consulta Arquivos de Devolu��o de Restitui��o Pr�mio - SEG452 - BBC452" & Ambiente
    
    glAmbiente_id = 3
    cUserName = "producao3"
   
    Call Conexao
    
    Call Trata_Parametros(Command)
    
    Call Limpa_Campos
    
    Call Carrega_Dados_Usuario
    'cUserName = "producao3"
    
    Call CentraFrm(Me)
    
   ' Call cmdAcesso_Click(0)
    
End Sub

Private Sub Limpa_Campos()
    
    txtUsuario.Text = ""
    txtCPF.Text = ""
    txtChave.Text = ""
    txtSenha(0).Text = ""
    stsbarAcesso.SimpleText = "Digite a Senha de acesso ao SEGBR"
    
End Sub

Private Sub txtAcesso_Change()
    If Len(txtSenha(0).Text) = 0 Then stsbarAcesso.SimpleText = "Digite a Senha de acesso ao SEGBR"
End Sub

Private Function Valida_Senha() As Boolean
    
    Dim objValSenha As Object
    Dim Valida      As Integer
    
    On Error GoTo TrataErro
    
    Valida_Senha = False
    
    Set objValSenha = CreateObject("SABL0015.clsSeguranca")
    
    Valida = objValSenha.ValidarSeguranca(txtCPF.Tag, cboEmpresa.ItemData(cboEmpresa.ListIndex), cboAmbiente.ItemData(cboAmbiente.ListIndex), txtSenha(0).Text)
    Select Case Valida
        Case 0: Valida_Senha = True
        Case 1: MsgBox "Senha Bloqueada !", vbOKOnly + vbExclamation
        Case 2: MsgBox "Senha Expirada !", vbOKOnly + vbExclamation
        Case 3: MsgBox "Senha Inexistente !", vbOKOnly + vbExclamation
        Case 4: MsgBox "Senha Inv�lida !", vbOKOnly + vbExclamation
    End Select
    
    Set objValSenha = Nothing
    
    Exit Function

TrataErro:
    mensagem_erro 6, "Erro ao validar Usu�rio. Descri��o: " & Err.Description
    Call TerminaSEGBR
End Function

Private Function Valida_Senha_OLD(vuUsuario As String) As Boolean
        
    Valida_Senha_OLD = True
        
    On Error GoTo TrataErro
        
    uSQL = "SELECT * FROM usuario_SISBB_tb with (nolock) "
    uSQL = uSQL & " WHERE chave = '" & vuUsuario & "'"
    'Set rdoRsTB = rdocn.OpenResultset(uSQL)
    Set rdoRsTB = New ADODB.Recordset
    Set rdoRsTB = ExecutarSQL("SEGBR", _
       glAmbiente_id, _
       App.Title, _
       App.FileDescription, _
       uSQL, _
       True, False)
        
    If rdoRsTB.EOF Then
        Valida_Senha_OLD = False
        MsgBox "Chave inv�lida para usu�rio " & cUserName & " !", vbOKOnly + vbExclamation, "!!! Aten��o !!!"
        txtSenha(0).SetFocus
    End If
    rdoRsTB.Close
        
    Exit Function
        
TrataErro:
    mensagem_erro 6, "Erro ao validar Usu�rio."
    Call TerminaSEGBR
        
End Function

Private Sub txtSenha_KeyPress(Index As Integer, KeyAscii As Integer)
    
    If KeyAscii <> 13 Then Exit Sub
    If Trim(txtSenha(0).Text) = "" Then Exit Sub
    Call cmdAcesso_Click(0)
    
End Sub

Private Sub Carrega_Dados_Usuario()
    Dim OLogin As String
    Dim sCpf   As String

    On Error GoTo TrataErro
    Dim rdoRsTB As ADODB.Recordset
       
    'Comentar
    '*
    cUserName = "ALENCAR"
    '*
    If IsNumeric(cUserName) Then
        sCpf = cUserName
    Else
        OLogin = BuscaCpf(cUserName)
        sCpf = OLogin
    End If
    sCpf = "31192730828"
    
    uSQL = "SELECT u2.nome, u2.cpf, u1.chave"
    uSQL = uSQL & " FROM usuario_SISBB_tb u1 with (nolock) "
    'INICIO - INC000003993540 - EDNETO - 20130327
    'ALTERADO PARA BUSCAR NA TABELA USUARIO_TB DE AB QUANDO EM ABS.
    If glAmbiente_id = 6 Or glAmbiente_id = 7 Then
        uSQL = uSQL & "     JOIN ABSS.SEGAB_DB.dbo.usuario_tb u2 with (nolock) "
    Else
        uSQL = uSQL & "     JOIN SEGAB_DB..usuario_tb u2 with (nolock) "
    End If
    'FIM - INC000003993540 - EDNETO - 20130327
    uSQL = uSQL & "         ON u2.login_rede = u1.login"
    uSQL = uSQL & " WHERE u2.cpf        = '" & sCpf & "'"
    uSQL = uSQL & "   AND u2.situacao   = 'A'"

    'Set rdoRsTB = ExecutarSQL("SEGBR", _
     '  glAmbiente_id, _
      ' App.Title, _
       'App.FileDescription, _
       'uSQL, _
       'True, False)
                           
    'If Not rdoRsTB.EOF Then
        txtUsuario.Text = UCase("mfrasca")
        txtCPF.Text = "311.927.308-28"
        txtCPF.Tag = "31192730828"
        txtChave.Text = "E1078413"
        Call Preenche_Combo_Ambiente("31192730828")
        Call Preenche_Combo_Empresa("31192730828")
    'Else
        'MsgBox "Usu�rio sem acesso a fun��o !!!", vbOKOnly + vbExclamation
        'fraSenhaAcesso.Enabled = False
        'cmdAcesso(0).Enabled = False
        'cmdAcesso(1).SetFocus
    '    End
   ' End If
    'rdoRsTB.Close
    
    Exit Sub
    
TrataErro:
    Call MsgBox("Erro na rotina: Carrega_Dados_Usuario. Descri��o: " & Err.Description)
End Sub

Private Sub Preenche_Combo_Ambiente(ByVal OCPF As String)
    
    Dim objAmbInstancia As Object
    Dim objAmbiente     As Object
    
    On Error GoTo Trata_Erro
    
    cboAmbiente.Clear
    
    Set objAmbInstancia = CreateObject("SABL0015.clsSeguranca")
    
    For Each objAmbiente In objAmbInstancia.Retornar_Ambiente_Acesso(OCPF)
    
        Call cboAmbiente.AddItem(objAmbiente.nome)
        cboAmbiente.ItemData(cboAmbiente.NewIndex) = objAmbiente.ambiente_id
    
    Next
        
    If cboAmbiente.ListCount > 0 Then cboAmbiente.ListIndex = 0
       
    Exit Sub
    
Trata_Erro:
    Call MsgBox("Erro na sub Preenche_Combo_Ambiente")
    
End Sub

Private Sub Preenche_Combo_Empresa(sCpf As String)

    Dim objEmpInstancia As Object
    Dim objEmpresa      As Object
    
    On Error GoTo Trata_Erro
    
    cboEmpresa.Clear
      
    Set objEmpInstancia = CreateObject("SABL0015.clsSeguranca")
    
    For Each objEmpresa In objEmpInstancia.Retornar_Empresa_Acesso(sCpf)
      
        Call cboEmpresa.AddItem(objEmpresa.nome)
        cboEmpresa.ItemData(cboEmpresa.NewIndex) = objEmpresa.empresa_id
      
    Next
           
    If cboEmpresa.ListCount > 0 Then cboEmpresa.ListIndex = 0
       
    Exit Sub
  
Trata_Erro:
    Call MsgBox("Erro em Preenche_Combo_Empresa")
  
End Sub

Private Function Formata_CPF(ByVal OCPF As String) As String
    
    OCPF = Format(OCPF, "00000000000")
    
    Formata_CPF = Mid(OCPF, 1, 3) & "."
    Formata_CPF = Formata_CPF & Mid(OCPF, 4, 3) & "."
    Formata_CPF = Formata_CPF & Mid(OCPF, 7, 3) & "-"
    Formata_CPF = Formata_CPF & Mid(OCPF, 10, 2)
    
End Function
Public Function BuscaCpf(ByVal sLogin As String)

    Dim rs   As Recordset
    Dim sSQL As String
    
    'INICIO - INC000003993540 - EDNETO - 20130327
    'EXECUTA A PROCEDURE EM AB QUANDO EM ABS.
    If glAmbiente_id = 6 Or glAmbiente_id = 7 Then
        sSQL = "Set NoCount on Exec abss.segab_db.dbo.retorna_cpf_sps '" & sLogin & "'"
    Else
        sSQL = "Set NoCount on Exec segab_db.dbo.retorna_cpf_sps '" & sLogin & "'"
    End If
    'FIM - INC000003993540 - EDNETO - 20130327
    'Dim objDB As Object 'New cls000100
    Set rs = rdocn.Execute(sSQL)

    '    Set rs = ExecutarSQL("SEGBR", _
    '                         2, _
    '                         App.Title, _
    '                         App.FileDescription, _
    '                         sSQL, True, False, 30000, 1, 2, 0)
    '
    If rs.State = 0 Or Err <> 0 Then GoTo TrataErro

    If rs.EOF Then
        BuscaCpf = ""
    Else
        BuscaCpf = Trim(rs.Fields("CPF"))
    End If

TrataErro:
End Function

