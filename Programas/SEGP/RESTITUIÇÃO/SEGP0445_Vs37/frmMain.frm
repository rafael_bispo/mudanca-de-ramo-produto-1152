VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.ocx"
Object = "{20C62CAE-15DA-101B-B9A8-444553540000}#1.1#0"; "msmapi32.Ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.2#0"; "MSCOMCTL.OCX"
Begin VB.Form frmMain 
   Caption         =   "SEGP0445 - Consulta Arquivos de Devolu��o de Restitui��o Pr�mio - SEG452 "
   ClientHeight    =   8415
   ClientLeft      =   1050
   ClientTop       =   525
   ClientWidth     =   13005
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmMain.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   8415
   ScaleWidth      =   13005
   Visible         =   0   'False
   Begin MSComctlLib.StatusBar StatusBar1 
      Align           =   2  'Align Bottom
      Height          =   330
      Left            =   0
      TabIndex        =   21
      Top             =   8085
      Width           =   13005
      _ExtentX        =   22939
      _ExtentY        =   582
      Style           =   1
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   10583
            MinWidth        =   10583
         EndProperty
      EndProperty
   End
   Begin MSMAPI.MAPIMessages MAPIMess 
      Left            =   2370
      Top             =   7545
      _ExtentX        =   1005
      _ExtentY        =   1005
      _Version        =   393216
      AddressEditFieldCount=   1
      AddressModifiable=   0   'False
      AddressResolveUI=   0   'False
      FetchSorted     =   0   'False
      FetchUnreadOnly =   0   'False
   End
   Begin MSMAPI.MAPISession MAPISess 
      Left            =   1710
      Top             =   7575
      _ExtentX        =   1005
      _ExtentY        =   1005
      _Version        =   393216
      DownloadMail    =   -1  'True
      LogonUI         =   -1  'True
      NewSession      =   0   'False
   End
   Begin VB.Frame Frame1 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   7815
      Left            =   90
      TabIndex        =   0
      Top             =   90
      Width           =   12795
      Begin VB.CommandButton cmdRecusa 
         Caption         =   "Recusar"
         Height          =   405
         Left            =   8010
         TabIndex        =   22
         Top             =   7275
         Width           =   1125
      End
      Begin MSComctlLib.ProgressBar pnlProg 
         Height          =   285
         Left            =   2565
         TabIndex        =   20
         Top             =   7305
         Visible         =   0   'False
         Width           =   3045
         _ExtentX        =   5371
         _ExtentY        =   503
         _Version        =   393216
         Appearance      =   1
      End
      Begin VB.CommandButton cmdImprimir 
         Caption         =   "&Liberar"
         Height          =   405
         Left            =   10295
         TabIndex        =   17
         Top             =   7275
         Width           =   1095
      End
      Begin VB.OptionButton optCaminho 
         Caption         =   "Liberados"
         Height          =   195
         Index           =   2
         Left            =   1215
         TabIndex        =   14
         Top             =   7245
         Width           =   1035
      End
      Begin VB.OptionButton optCaminho 
         Caption         =   "Enviados"
         Height          =   195
         Index           =   1
         Left            =   150
         TabIndex        =   13
         Top             =   7515
         Width           =   1020
      End
      Begin VB.OptionButton optCaminho 
         Caption         =   "Gerados"
         Height          =   195
         Index           =   0
         Left            =   150
         TabIndex        =   12
         Top             =   7230
         Width           =   1185
      End
      Begin VB.CommandButton cmdParcelas 
         Caption         =   "Parcelas"
         Height          =   405
         Left            =   9155
         TabIndex        =   5
         Top             =   7275
         Width           =   1125
      End
      Begin VB.PictureBox pnlProdutos 
         Height          =   6795
         Left            =   135
         ScaleHeight     =   6735
         ScaleWidth      =   3750
         TabIndex        =   4
         Top             =   285
         Width           =   3810
         Begin MSComctlLib.ListView lstPreRemessa 
            Height          =   6135
            Left            =   75
            TabIndex        =   23
            Top             =   420
            Width           =   3615
            _ExtentX        =   6376
            _ExtentY        =   10821
            LabelWrap       =   -1  'True
            HideSelection   =   -1  'True
            HideColumnHeaders=   -1  'True
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   1
            NumItems        =   1
            BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Text            =   "PreRemessa (Controle Interno)"
               Object.Width           =   3510
            EndProperty
         End
         Begin VB.FileListBox File1 
            Height          =   6135
            Left            =   75
            TabIndex        =   15
            Top             =   420
            Width           =   2085
         End
         Begin VB.Label lblSprArq 
            Caption         =   "Arquivos"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   105
            TabIndex        =   16
            Top             =   75
            Width           =   2745
         End
      End
      Begin VB.CommandButton cmdSair 
         Caption         =   "Sai&r"
         Height          =   405
         Left            =   11400
         TabIndex        =   1
         Top             =   7275
         Width           =   1125
      End
      Begin VB.PictureBox pnlResumo 
         Height          =   6810
         Left            =   4080
         ScaleHeight     =   6750
         ScaleWidth      =   8535
         TabIndex        =   2
         Top             =   285
         Width           =   8595
         Begin VB.TextBox txtData 
            Alignment       =   1  'Right Justify
            Height          =   285
            Left            =   7440
            Locked          =   -1  'True
            TabIndex        =   18
            Top             =   60
            Width           =   990
         End
         Begin MSFlexGridLib.MSFlexGrid sprDetalhes 
            Height          =   6195
            Left            =   120
            TabIndex        =   6
            Top             =   480
            Width           =   8355
            _ExtentX        =   14737
            _ExtentY        =   10927
            _Version        =   393216
            FixedCols       =   0
            SelectionMode   =   1
            AllowUserResizing=   3
         End
         Begin VB.Label Label2 
            Caption         =   "Dt Arquivo"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Left            =   6480
            TabIndex        =   19
            Top             =   60
            Width           =   930
         End
         Begin VB.Label lblMinData 
            Alignment       =   2  'Center
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            Height          =   270
            Left            =   960
            TabIndex        =   7
            Top             =   60
            Width           =   780
         End
         Begin VB.Label lblRemessa 
            Alignment       =   1  'Right Justify
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            Height          =   270
            Left            =   4320
            TabIndex        =   10
            Top             =   60
            Width           =   2115
         End
         Begin VB.Label Label3 
            Caption         =   "Total"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   270
            Left            =   3840
            TabIndex        =   11
            Top             =   60
            Width           =   600
         End
         Begin VB.Label Label1 
            Alignment       =   2  'Center
            Caption         =   "Quantidade "
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   270
            Left            =   1800
            TabIndex        =   9
            Top             =   60
            Width           =   1095
         End
         Begin VB.Label lblMaxData 
            Alignment       =   2  'Center
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            Height          =   270
            Left            =   2880
            TabIndex        =   8
            Top             =   60
            Width           =   795
         End
         Begin VB.Label Label5 
            Caption         =   "Remessa"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   270
            Left            =   120
            TabIndex        =   3
            Top             =   60
            Width           =   810
         End
      End
   End
End
Attribute VB_Name = "frmMain"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public vArqNome As String
Public lorigem  As String
Dim Arq, NomeMostrado As String

'F.BEZERRA - FLOWBR14413025 11/06/2013
Public bytProcesso                 As Byte
Private Const bytProcesso_PreRemessa = 1
Private Const bytProcesso_Remessa = 2
Public Sub ConsultaPreRemessa_SEG452(iOptSelecionado As Integer)  'FLAVIO.BEZERRA - 06/06/2013
    Dim sSQL            As String
    Dim rsPreRemessa    As Recordset
    Dim Item            As ListItem
    
    lblMinData.Caption = ""
    lblRemessa.Caption = ""
    lblMaxData.Caption = ""
    txtData.Text = ""
    File1.Visible = False
    lstPreRemessa.View = lvwReport
    lstPreRemessa.FullRowSelect = True
    lstPreRemessa.LabelEdit = lvwManual
    lstPreRemessa.HotTracking = True
    lstPreRemessa.ColumnHeaders.Clear
    lstPreRemessa.ListItems.Clear
    lstPreRemessa.Refresh
    
    'Carregando cabe�alho do grid
    lstPreRemessa.ColumnHeaders.Add , , "PreRemessa (Controle Interno)", 3700
    lstPreRemessa.ColumnHeaders.Add , , "Quantidade", 0
    lstPreRemessa.ColumnHeaders.Add , , "Total", 0
    lstPreRemessa.ColumnHeaders.Add , , "Emissao", 0
    
    If iOptSelecionado = 0 Then
        sSQL = ""
        sSQL = sSQL & "SELECT ps_mov_cliente_preremessa_tb.Controle_Interno_PreRemessa_ID               PreRemessa_ID                                    " & vbNewLine
        sSQL = sSQL & "     , count(ps_mov_cliente_tb.Movimentacao_ID)                                  Quantidade                                       " & vbNewLine
        sSQL = sSQL & "     , sum(ps_mov_cliente_tb.val_movimentacao)                                   Total                                            " & vbNewLine
        sSQL = sSQL & "     , ps_mov_cliente_preremessa_tb.dt_inclusao                                  Emissao                                          " & vbNewLine
        sSQL = sSQL & "  FROM ps_mov_cliente_preremessa_tb                                              ps_mov_cliente_preremessa_tb with (nolock)       " & vbNewLine
        sSQL = sSQL & "  JOIN ps_mov_cliente_tb                                                         ps_mov_cliente_tb with (nolock)                  " & vbNewLine
        sSQL = sSQL & "    ON ps_mov_cliente_tb.Movimentacao_ID                                         = ps_mov_cliente_preremessa_tb.Movimentacao_ID   " & vbNewLine
        sSQL = sSQL & "   AND ps_mov_cliente_preremessa_tb.Lote_Liberado                                IS NULL                                          " & vbNewLine
        
        'Inicio- Autor: Rodrigo Moura Adicionado para o endosso de contesta��o----------------------------------
        sSQL = sSQL & "    join seguros_db.dbo.ps_mov_endosso_financeiro_tb pmef with (nolock)                                                              " & vbNewLine
        sSQL = sSQL & "      on pmef.movimentacao_id = ps_mov_cliente_tb.movimentacao_id                                                                 " & vbNewLine
        sSQL = sSQL & "left join seguros_db.dbo.eds_contestacao_log_tb eds_contestacao_log_tb with (nolock)                                              " & vbNewLine
        sSQL = sSQL & "      on eds_contestacao_log_tb.proposta_id =  pmef.proposta_id                                                                   " & vbNewLine
        sSQL = sSQL & "     and eds_contestacao_log_tb.endosso_id =  pmef.endosso_id                                                                     " & vbNewLine
        'Fim- Autor: Rodrigo Moura Adicionado para o endosso de contesta��o----------------------------------
        
        
        sSQL = sSQL & " WHERE ps_mov_cliente_tb.Remessa_Liberada                                        = 'S'                                            " & vbNewLine
        'Inicio- Autor: Rodrigo Moura Adicionado para o endosso de contesta��o----------------------------------
        sSQL = sSQL & "   AND eds_contestacao_log_tb.proposta_id Is Null                                                                                 " & vbNewLine
        'Fim- Autor: Rodrigo Moura Adicionado para o endosso de contesta��o----------------------------------
        sSQL = sSQL & " GROUP BY ps_mov_cliente_preremessa_tb.Controle_Interno_PreRemessa_ID                                                             " & vbNewLine
        sSQL = sSQL & "     , ps_mov_cliente_preremessa_tb.dt_inclusao                                                                                   " & vbNewLine
        
        Set rsPreRemessa = rdocn.Execute(sSQL)
    
        Do While Not rsPreRemessa.EOF
            
            Set Item = lstPreRemessa.ListItems.Add()
            
            Item.Text = "SEG452." & Right("00000" & rsPreRemessa.Fields!PreRemessa_id, 4) & " (Controle Interno de Restitui��o)"
            Item.ToolTipText = Item.Text
            Item.Tag = Right("00000" & rsPreRemessa.Fields!PreRemessa_id, 4)
            Item.SubItems(1) = (rsPreRemessa.Fields!Quantidade)
            Item.SubItems(2) = (rsPreRemessa.Fields!Total)
            Item.SubItems(3) = (rsPreRemessa.Fields!Emissao)
            rsPreRemessa.MoveNext
        
        Loop
    End If
    
    i = 0
    Do While i < File1.ListCount
        
        Set Item = lstPreRemessa.ListItems.Add()
        
        Item.Text = File1.List(i)
        Item.ToolTipText = Item.Text
        Item.Tag = "Remessa"
        Item.SubItems(1) = 0
        Item.SubItems(2) = 0
        Item.SubItems(3) = 0
        i = i + 1
        
    Loop
    
End Sub

Sub EnviarEmail(pMsg As String)
    
    '-- Pega perfil do usuario logado
    With Me.MAPISess
        .username = cUserName
        .Action = 1
        MAPIMess.SessionID = .SessionID
    End With
    '-- Envia mensagem
    Call SendMsg(pMsg)
    MAPISess.SignOff
End Sub

Public Sub EnviaEmail()
    'Set OUT = New Outlook.Application
    'Set NS = OUT.GetNamespace("MAPI")
    'Set Folder = NS.GetDefaultFolder(olFolderInbox)
    'Set Mens = Folder.Items.Add
    'Dim txtBody As String
    'txtBody = "Produ��o," & vbCrLf & vbCrLf & "O arquivo acima j� esta liberado para ser enviado."
    ''Arq = App.PATH & "\check.bmp" '-- Arq= nome arquivo a ser atachado
    ''NomeMostrado = "check.bmp" '-- Nome do arquivo atachado
    'With Mens
    ''.To = "producao@aliancadobrasil.com.br"
    '.To = "nrocha.aimares@aliancadobrasil.com.br"
    '.Subject = "Libera��o do arquivo " & File1.FileName
    '.Body = txtBody
    ''.CC = txtCopia.Text
    ''.Attachments.Add Arq, 1, Len(txtBody) + 2, NomeMostrado
    '.Attachments.Add "", 1, Len(txtBody) + 2
    '.Send
    'End With

End Sub

Public Sub Detalhe_SEG452()
    Dim rc2 As Recordset
    On Error GoTo TrataErro
    Screen.MousePointer = vbHourglass
    vSql = ""
    
    'jose.souza em 18/12/2013
    bUsarConexaoABS = False
        
    If vTabTmp <> "##SEG452_" & cUserName Then
        If vTabTmp <> "" Then
            vSql = vSql + "DROP TABLE "
            vSql = vSql + "     " + vTabTmp + " "
            'vSql = vSql + " GO "
        End If
        vSql = vSql + "CREATE TABLE " & vbNewLine
        vSql = vSql + "   ##SEG452_" & cUserName & vbNewLine
        vSql = vSql + "   ( " & vbNewLine
        vSql = vSql + "     Sequencial           char(6), " & vbNewLine
        vSql = vSql + "     Tp_Registro          char(2) , " & vbNewLine
        vSql = vSql + "     Cod_Prod_Seguradora  char(9), " & vbNewLine
        vSql = vSql + "     Cod_Produto          Char(7), " & vbNewLine
        vSql = vSql + "     Proposta_BB          char(9), " & vbNewLine
        vSql = vSql + "     Endosso_BB           char(9), " & vbNewLine
        vSql = vSql + "     Endosso_id           char(9)," & vbNewLine
        vSql = vSql + "     Tp_Devolucao         char(1), " & vbNewLine
        vSql = vSql + "     dt_Devolucao         smalldatetime, " & vbNewLine
        vSql = vSql + "     Valor_Devolucao      numeric(15,2), " & vbNewLine
        vSql = vSql + "     Valor_IOF            numeric(15,2), " & vbNewLine
        vSql = vSql + "     Valor_Corretagem     numeric(15,2), " & vbNewLine
        vSql = vSql + "     Valor_IR_Corretagem  numeric(15,2), " & vbNewLine
        vSql = vSql + "     Valor_Estipulante    numeric(15,2), " & vbNewLine
        vSql = vSql + "     Valor_IR_Estipulante numeric(15,2), " & vbNewLine
        vSql = vSql + "     Valor_Seguradora     numeric(15,2), " & vbNewLine
        vSql = vSql + "     Valor_Custo_Apolice  numeric(15,2), " & vbNewLine
        vSql = vSql + "     Valor_Desconto       numeric(15,2), " & vbNewLine
        vSql = vSql + "     Valor_adicional_Frac numeric(15,2), " & vbNewLine
        vSql = vSql + "     Cliente varchar(60)               , " & vbNewLine
        vSql = vSql + "   ) "
    Else
        vSql = vSql + "TRUNCATE TABLE "
        vSql = vSql + "   ##SEG452_" & cUserName
    End If
        
    rdocn.Execute vSql, 64
        
    vSql = ""
    vSql = vSql + "INSERT INTO " & vbNewLine
    vSql = vSql + "     ##SEG452_" & cUserName & vbNewLine
    vSql = vSql + "( " & vbNewLine
    vSql = vSql + "     Sequencial," & vbNewLine
    vSql = vSql + "     Tp_Registro, " & vbNewLine
    vSql = vSql + "     Cod_Prod_Seguradora,  " & vbNewLine
    vSql = vSql + "     Cod_Produto,          " & vbNewLine
    vSql = vSql + "     Proposta_BB,          " & vbNewLine
    vSql = vSql + "     Endosso_BB,           " & vbNewLine
    vSql = vSql + "     Endosso_id,           " & vbNewLine
    vSql = vSql + "     Tp_Devolucao,         " & vbNewLine
    vSql = vSql + "     dt_Devolucao,         " & vbNewLine
    vSql = vSql + "     Valor_Devolucao,      " & vbNewLine
    vSql = vSql + "     Valor_IOF,            " & vbNewLine
    vSql = vSql + "     Valor_Corretagem,     " & vbNewLine
    vSql = vSql + "     Valor_IR_Corretagem,  " & vbNewLine
    vSql = vSql + "     Valor_Estipulante,    " & vbNewLine
    vSql = vSql + "     Valor_IR_Estipulante, " & vbNewLine
    vSql = vSql + "     Valor_Seguradora,    " & vbNewLine
    vSql = vSql + "     Valor_Custo_Apolice,  " & vbNewLine
    vSql = vSql + "     Valor_Desconto,       " & vbNewLine
    vSql = vSql + "     Valor_adicional_Frac " & vbNewLine
    vSql = vSql + ") " & vbNewLine
    vSql = vSql + "SELECT " & vbNewLine
    vSql = vSql + "     SUBSTRING(Linha, 1, 6), " & vbNewLine
    vSql = vSql + "     SUBSTRING(Linha, 7, 2), " & vbNewLine
    vSql = vSql + "     SUBSTRING(Linha, 9, 9), " & vbNewLine
    vSql = vSql + "     SUBSTRING(Linha, 18, 7), " & vbNewLine
    vSql = vSql + "     SUBSTRING(Linha, 25, 9), " & vbNewLine
    vSql = vSql + "     SUBSTRING(Linha, 34, 9), " & vbNewLine
    vSql = vSql + "     SUBSTRING(Linha, 43, 9), " & vbNewLine
    vSql = vSql + "     SUBSTRING(Linha, 52, 1), " & vbNewLine
    vSql = vSql + "     CONVERT(smalldatetime, SUBSTRING(Linha, 57, 4)+SUBSTRING(Linha, 55, 2)+SUBSTRING(Linha, 53, 2)), " & vbNewLine
    vSql = vSql + "     CONVERT(numeric(15,2), SUBSTRING(Linha, 61, 13)+'.'+SUBSTRING(Linha, 74, 2)), " & vbNewLine
    vSql = vSql + "     CONVERT(numeric(15,2), SUBSTRING(Linha, 76, 13)+'.'+SUBSTRING(Linha, 89, 2)), " & vbNewLine
    vSql = vSql + "     CONVERT(numeric(15,2), SUBSTRING(Linha, 91, 13)+'.'+SUBSTRING(Linha, 104, 2)), " & vbNewLine
    vSql = vSql + "     CONVERT(numeric(15,2), SUBSTRING(Linha, 106, 13)+'.'+SUBSTRING(Linha, 119, 2)), " & vbNewLine
    vSql = vSql + "     CONVERT(numeric(15,2), SUBSTRING(Linha, 121, 13)+'.'+SUBSTRING(Linha, 134, 2)), " & vbNewLine
    vSql = vSql + "     CONVERT(numeric(15,2), SUBSTRING(Linha, 136, 13)+'.'+SUBSTRING(Linha, 149, 2)), " & vbNewLine
    vSql = vSql + "     CONVERT(numeric(15,2), SUBSTRING(Linha, 151, 13)+'.'+SUBSTRING(Linha, 164, 2)), " & vbNewLine
    vSql = vSql + "     CONVERT(numeric(15,2), SUBSTRING(Linha, 166, 13)+'.'+SUBSTRING(Linha, 179, 2)), " & vbNewLine
    vSql = vSql + "     CONVERT(numeric(15,2), SUBSTRING(Linha, 181, 13)+'.'+SUBSTRING(Linha, 194, 2)), " & vbNewLine
    vSql = vSql + "     CONVERT(numeric(15,2), SUBSTRING(Linha, 196, 13)+'.'+SUBSTRING(Linha, 209, 2)) " & vbNewLine
    vSql = vSql + " FROM " & vbNewLine
    vSql = vSql + "     ##tmp_arquivo_" & cUserName & vbNewLine
    vSql = vSql + "WHERE " & vbNewLine
    vSql = vSql + "     SUBSTRING(Linha, 1, 6) <>'000000' AND SUBSTRING(Linha, 1, 6) <>'999999'"

    rdocn.Execute vSql, 64
    sprDetalhes.Clear
   
    vSql = "Select * From ##SEG452_" & cUserName & " Order by Proposta_BB"
    Set rc = rdocn.Execute(vSql)
        
    sprDetalhes.Visible = False
        
    Call Cabecalho
        
    pnlProg.Min = 0
    pnlProg.Max = Val(lblMaxData)
    pnlProg.Visible = True
    pnlProg.Value = 0

    Do Until rc.EOF
        If pnlProg.Value < pnlProg.Max Then
            pnlProg.Value = pnlProg.Value + 1
        End If

        sprDetalhes.Rows = sprDetalhes.Rows + 1
        sprDetalhes.Row = sprDetalhes.Rows - 1
            
        sprDetalhes.Col = 0
        sprDetalhes.Text = rc!Proposta_BB
        sprDetalhes.Col = 1
        sprDetalhes.Text = rc!Endosso_BB
        sprDetalhes.Col = 2
        sprDetalhes.Text = rc!Endosso_id
        sprDetalhes.Col = 3
        sprDetalhes.Text = IIf(rc!tp_Devolucao = "T", "Total", "Parcial")
        sprDetalhes.Col = 4
        sprDetalhes.Text = rc!dt_devolucao
        sprDetalhes.Col = 5
        sprDetalhes.Text = IIf(IsNull(rc("Valor_Devolucao")), 0, Format(CDbl(rc("Valor_Devolucao")), "##,##0.#0"))
        sprDetalhes.Col = 6
        sprDetalhes.Text = IIf(IsNull(rc("Valor_IOF")), 0, Format(CDbl(rc("Valor_IOF")), "##,##0.#0"))
        sprDetalhes.Col = 7
        sprDetalhes.Text = IIf(IsNull(rc("Valor_Corretagem")), 0, Format(CDbl(rc("Valor_Corretagem")), "##,##0.#0"))
        sprDetalhes.Col = 8
        sprDetalhes.Text = IIf(IsNull(rc("Valor_IR_Corretagem")), 0, Format(CDbl(rc("Valor_IR_Corretagem")), "##,##0.#0"))
        sprDetalhes.Col = 9
        sprDetalhes.Text = IIf(IsNull(rc("Valor_Estipulante")), 0, Format(CDbl(rc("Valor_Estipulante")), "##,##0.#0"))
        sprDetalhes.Col = 10
        sprDetalhes.Text = IIf(IsNull(rc("Valor_IR_Estipulante")), 0, Format(CDbl(rc("Valor_IR_Estipulante")), "##,##0.#0"))
        sprDetalhes.Col = 11
        sprDetalhes.Text = IIf(IsNull(rc("Valor_Seguradora")), 0, Format(CDbl(rc("Valor_Seguradora")), "##,##0.#0"))
        sprDetalhes.Col = 12
        sprDetalhes.Text = IIf(IsNull(rc("Valor_Custo_Apolice")), 0, Format(CDbl(rc("Valor_Custo_Apolice")), "##,##0.#0"))
        sprDetalhes.Col = 13
        sprDetalhes.Text = IIf(IsNull(rc("Valor_Desconto")), 0, Format(CDbl(rc("Valor_Desconto")), "##,##0.#0"))
        sprDetalhes.Col = 14
        sprDetalhes.Text = IIf(IsNull(rc("Valor_adicional_Frac")), 0, Format(CDbl(rc("Valor_adicional_Frac")), "##,##0.#0"))
        sprDetalhes.Col = 15
            
        '-- Identifica nome do cliente para Proposta BB
        'vsql = "Select c.nome From Proposta_adesao_tb a,Proposta_tb b,Cliente_tb c where Proposta_BB=" & rc!Proposta_BB & " And b.proposta_id = a.proposta_id And c.Cliente_id=prop_cliente_id"
        vSql = ""
        vSql = vSql & " SELECT c.nome " & vbNewLine
        vSql = vSql & "   FROM              Proposta_adesao_tb a with (nolock) " & vbNewLine
        vSql = vSql & "         INNER JOIN  Proposta_tb b with (nolock) ON b.proposta_id = a.proposta_id " & vbNewLine
        vSql = vSql & "         INNER JOIN  Cliente_tb  c with (nolock) ON c.Cliente_id = b.prop_cliente_id " & vbNewLine
        vSql = vSql & "  WHERE  a.Proposta_BB = " & rc!Proposta_BB & vbNewLine
        vSql = vSql & "    AND  b.produto_id = " & Mid(rc!Cod_Prod_Seguradora, 1, 3) & vbNewLine
        vSql = vSql & "    AND  b.situacao <> 'T'" & vbNewLine
        If bUsarConexaoABS Then
            vSql = vSql & " UNION ALL " & vbNewLine
            vSql = vSql & " SELECT c.nome " & vbNewLine
            vSql = vSql & "   FROM              ABSS.seguros_db.dbo.Proposta_adesao_tb a with (nolock) " & vbNewLine
            vSql = vSql & "         INNER JOIN  ABSS.seguros_db.dbo.Proposta_tb b with (nolock) ON b.proposta_id = a.proposta_id " & vbNewLine
            vSql = vSql & "         INNER JOIN  ABSS.seguros_db.dbo.Cliente_tb  c with (nolock) ON c.Cliente_id = b.prop_cliente_id " & vbNewLine
            vSql = vSql & "  WHERE  a.Proposta_BB = " & rc!Proposta_BB & vbNewLine
            vSql = vSql & "    AND  b.produto_id = " & Mid(rc!Cod_Prod_Seguradora, 1, 3) & vbNewLine
            vSql = vSql & "    AND  b.situacao <> 'T'" & vbNewLine
        End If
        Set rc2 = rdocn.Execute(vSql)
        DoEvents
        If rc2.EOF Then
            vSql = ""
            vSql = vSql & "SELECT c.nome " & vbNewLine
            vSql = vSql & "  FROM Proposta_Fechada_tb a with (nolock),Proposta_tb b with (nolock),Cliente_tb c with (nolock) " & vbNewLine
            vSql = vSql & " WHERE Proposta_BB   = " & rc!Proposta_BB & "            " & vbNewLine
            vSql = vSql & "   AND b.proposta_id = a.proposta_id And c.Cliente_id = prop_cliente_id " & vbNewLine
            vSql = vSql & "   AND b.produto_id  = " & Mid(rc!Cod_Prod_Seguradora, 1, 3) & "        " & vbNewLine
            vSql = vSql & "   AND b.situacao <> 'T'" & vbNewLine
            If bUsarConexaoABS Then
                vSql = vSql & "UNION ALL " & vbNewLine
                vSql = vSql & "SELECT c.nome " & vbNewLine
                vSql = vSql & "  FROM Proposta_Fechada_tb a with (nolock),Proposta_tb b with (nolock),Cliente_tb c with (nolock) " & vbNewLine
                vSql = vSql & " WHERE Proposta_BB   = " & rc!Proposta_BB & "            " & vbNewLine
                vSql = vSql & "   AND b.proposta_id = a.proposta_id And c.Cliente_id = prop_cliente_id " & vbNewLine
                vSql = vSql & "   AND b.produto_id  = " & Mid(rc!Cod_Prod_Seguradora, 1, 3) & "        " & vbNewLine
                vSql = vSql & "   AND b.situacao <> 'T'" & vbNewLine
            End If
            rc2.Close
            Set rc2 = Nothing
            Set rc2 = rdocn.Execute(vSql)
            If Not rc2.EOF Then
                sprDetalhes.Text = IIf(IsNull(rc2("nome")), "", rc2("nome"))
            Else
                sprDetalhes.Text = "Nome n�o localizado"
            End If
        Else
            sprDetalhes.Text = IIf(IsNull(rc2("nome")), "", rc2("nome"))
        End If
        rc2.Close
        Set rc2 = Nothing
        sprDetalhes.Col = 16
        sprDetalhes.Text = IIf(IsNull(rc("Cod_Prod_Seguradora")), 0, rc!Cod_Prod_Seguradora)

        rc.MoveNext
    Loop
    rc.Close
    Set rc = Nothing
    Screen.MousePointer = vbDefault
        
    vTabTmp = "##SEG452_" & cUserName
        
    vSql = vSql + "     DROP TABLE #tmp_arquivo_" & cUserName
    rdocn.Execute vSql, 64
        
    StatusBar1.SimpleText = ""
    sprDetalhes.Visible = True
    pnlResumo.Visible = True
    pnlProg.Visible = False
    'cmdParcelas.Visible = False
    cmdParcelas.Enabled = False
    cmdImprimir.Enabled = True
    Exit Sub
    Resume
TrataErro:
    TrataErroGeral "Rotina : Detalhe_SEG452 - BBC452" & Err.Description, "SEGP0445"
    TerminaSEGBR
    Resume
End Sub
Public Sub Cabecalho()
    sprDetalhes.Rows = 1
    sprDetalhes.Cols = 17

    sprDetalhes.Row = 0
    sprDetalhes.Col = 0
    sprDetalhes.ColWidth(0) = 1000
    sprDetalhes.Text = "Proposta BB"
    sprDetalhes.Col = 1
    sprDetalhes.ColWidth(1) = 1100
    sprDetalhes.Text = "Endosso BB"
    sprDetalhes.Col = 2
    sprDetalhes.ColWidth(2) = 1000
    sprDetalhes.Text = "Endosso"
    sprDetalhes.Col = 3
    sprDetalhes.ColWidth(3) = 1190
    sprDetalhes.Text = "Tipo Devolu��o"
    sprDetalhes.Col = 4
    sprDetalhes.ColWidth(4) = 1100
    sprDetalhes.Text = "Dt Devolu��o"
    sprDetalhes.Col = 5
    sprDetalhes.ColWidth(5) = 1500
    sprDetalhes.Text = "Vl Devolu��o"
    sprDetalhes.Col = 6
    sprDetalhes.ColWidth(6) = 1100
    sprDetalhes.Text = "Vl IOF"
    sprDetalhes.Col = 7
    sprDetalhes.ColWidth(7) = 1200
    sprDetalhes.Text = "Vl Corretagem"
    sprDetalhes.Col = 8
    sprDetalhes.ColWidth(8) = 1300
    sprDetalhes.Text = "Vl IR Corretagem"
    sprDetalhes.Col = 9
    sprDetalhes.ColWidth(9) = 1500
    sprDetalhes.Text = "Vl Estipulante"
    sprDetalhes.Col = 10
    sprDetalhes.ColWidth(10) = 1300
    sprDetalhes.Text = "Vl IR Estipulante"
    sprDetalhes.Col = 11
    sprDetalhes.ColWidth(11) = 1500
    sprDetalhes.Text = "Vl Seguradora"
    sprDetalhes.Col = 12
    sprDetalhes.ColWidth(12) = 1100
    sprDetalhes.Text = "Custo Ap�lice"
    sprDetalhes.Col = 13
    sprDetalhes.ColWidth(13) = 1500
    sprDetalhes.Text = "Vl Desconto"
    sprDetalhes.Col = 14
    sprDetalhes.ColWidth(14) = 1500
    sprDetalhes.Text = "Adic. Fracionamento"
    sprDetalhes.Col = 15
    sprDetalhes.ColWidth(15) = 4500
    sprDetalhes.Text = "Segurado"
    sprDetalhes.Col = 16
    sprDetalhes.ColWidth(16) = 1100
    sprDetalhes.Text = "C�d. Produto"

End Sub
Private Sub ImportaArquivo()

    Dim vTemp As String
    Dim rc2   As Recordset
    'Carla Corr�a (09/12/2013: INC000004222334) -> Declarando a vari�vel vTotalbb para sanar problema no c�lculo com casas decimais
    Dim vTotalbb As Currency
    On Error GoTo TrataErro
    
    Screen.MousePointer = vbHourglass
   
    lblMaxData.Caption = ""
    lblMinData.Caption = ""
    txtData.Text = ""
    'cmdParcelas.Visible = False
    cmdParcelas.Enabled = False
    
    StatusBar1.SimpleText = "Lendo arquivo .." + File1.PATH & "\" & File1.FileName
     
    '    sprArq.Col = 0
    Open File1.PATH & "\" & File1.FileName For Input Access Read As #1
    
    vArqNome = File1.FileName
   
    vSql = ""
    vSql = "IF EXISTS ( SELECT 1 FROM TempDB.dbo.sysobjects WHERE NAME = '##tmp_arquivo_" & cUserName & "') " & vbNewLine
    vSql = vSql + "     DROP TABLE ##tmp_arquivo_" & cUserName & vbNewLine
    vSql = vSql + "     CREATE TABLE ##tmp_arquivo_" & cUserName
    vSql = vSql + "          ( Linha varchar(8000)) "
    rdocn.Execute vSql, 64
        
    'Alessandra Grig�rio - Inclus�o da fun��o INT pois se o resultado fosse decimal na hora de somar 1 gerava erro.
    vNumLin = Int(LOF(1) / 500)
                
    pnlProg.Min = 0
    pnlProg.Max = vNumLin
    pnlProg.Visible = True
    pnlProg.Value = 0
    
    vArqNome = Trim(File1.FileName)
    vRemessa = Mid(vArqNome, InStr(1, vArqNome, ".") + 1, 4)
    lblMinData.Caption = vRemessa
    txtData = Format(FileDateTime(File1.PATH & "\" & File1.FileName), "dd/mm/yyyy")
       
  Do Until EOF(1)
        DoEvents
        If pnlProg.Value < pnlProg.Max Then
            pnlProg.Value = pnlProg.Value + 1
        End If
            
        Line Input #1, vTemp
        If Mid(vTemp, 1, 6) = "999999" Then
            lblMaxData.Caption = CInt(Mid(vTemp, 13, 6))
            'racras  ajuste para bbc452
            If Left(File1.FileName, 2) = "BB" Then
             lblRemessa.Caption = vTotalbb
            Else
            lblRemessa.Caption = Format(CDbl(Mid(vTemp, 19, 13) & "," & Mid(vTemp, 32, 2)), "##,##0.#0")
            End If
            
         Else
             If Left(File1.FileName, 2) = "BB" Then
             vl = vl + 1
             If Left(File1.FileName, 2) = "BB" And vl > 1 Then
             'Carla Corr�a (09/12/2013: INC000004222334) -> Declarando a vari�vel vTotalbb para sanar problema no c�lculo com casas decimais
              vTotalbb = CCur(vTotalbb) + CCur(Mid(vTemp, 151, 15) / 100)
             'vTotalbb = CDbl(vTotalbb) + (CDbl(Mid(vTemp, 151, 15) / 100))
             ' lblRemessa.Caption = vTotalbb
            
             End If
             lblRemessa.Caption = vTotalbb
            End If
          
        End If
        
        
        
        
        
        vSql = ""
        vSql = vSql + "INSERT INTO "
        vSql = vSql + "     ##tmp_arquivo_" & cUserName
        vSql = vSql + "( Linha ) Values ("
        vSql = vSql + " '" + vTemp + "' )"
        rdocn.Execute vSql, 64
  
    Loop
    Close #1
    'cmdParcelas.Visible = True
    cmdParcelas.Enabled = True
    pnlProg.Visible = False
    Screen.MousePointer = vbDefault
    StatusBar1.SimpleText = ""
    Exit Sub
    Resume
TrataErro:
    TrataErroGeral "Rotina : ImportaArquivo - " & Err.Description, "SEGP0445"
    TerminaSEGBR

End Sub

Private Sub cmdImprimir_old_Click()
    Dim vTemp As String
    Dim vRow  As Long
    
    Screen.MousePointer = 11
    
    If pnlResumoSinistros.Visible = True Or _
       pnlResumoCancelamento.Visible = True Or _
       pnlResumoEmissao.Visible = True Or _
       pnlResumo.Visible = True Then
        'If MsgBox("Imprimir detalhes do arquivo " + vArqNome + _
         " ou listar todos os arquivos e datas do produto " + lstArq.Text + "?", vbQuestion) = "D" Then
        Printer.Font = "Arial"
        Printer.FontSize = 16
        Printer.FontBold = True
        Printer.Print "Arquivo de cobran�a - " + vArqNome
        Printer.FontSize = 12
        Printer.Print "Produto " + lstArq.Text
            
        Printer.Font = "Courier New"
        Printer.FontSize = 12
        Printer.FontBold = False
        Printer.Print
        Printer.Print
            
        Select Case vLayout
            Case "SEG457"
                Printer.Print "Remessa" + Space(40 - Len("Remessa") - Len(lblRemessa.Caption)) + lblRemessa.Caption
                Printer.Print "Data da remessa" + Space(40 - Len("Data da remessa") - Len(lblDtaArq.Caption)) + lblDtaArq.Caption
                Printer.Print "Parcelas" + Space(40 - Len("Parcelas") - Len(lblParcelas.Caption)) + lblParcelas.Caption
                Printer.Print "Pr�mio Bruto" + Space(40 - Len("Pr�mio Bruto") - Len(lblPremioBruto.Caption)) + lblPremioBruto.Caption
                Printer.Print "IOF" + Space(40 - Len("IOF") - Len(lblIOF.Caption)) + lblIOF.Caption
                Printer.Print "Bruto-IOF" + Space(40 - Len("Bruto-IOF") - Len(lblBrutoSemIOF.Caption)) + lblBrutoSemIOF.Caption
                Printer.Print "Corretagem" + Space(40 - Len("Corretagem") - Len(lblCorretagem.Caption)) + lblCorretagem.Caption
                Printer.Print "Repasse" + Space(40 - Len("Repasse") - Len(lblRepasse.Caption)) + lblRepasse.Caption
                Printer.Print "Estipulante" + Space(40 - Len("Estipulante") - Len(lblEstipulante.Caption)) + lblEstipulante.Caption
                Printer.Print "Juros" + Space(40 - Len("Juros") - Len(lblJuros.Caption)) + lblJuros.Caption
            Case "CBR"
                Printer.Print "Remessa" + Space(40 - Len("Remessa") - Len(lblRetorno.Caption)) + lblRetorno.Caption
                Printer.Print "Banco" + Space(40 - Len("Banco") - Len(lblBanco.Caption)) + lblBanco.Caption
                Printer.Print "Data da remessa" + Space(40 - Len("Data da remessa") - Len(lblDtaArqCBR643.Caption)) + lblDtaArqCBR643.Caption
                Printer.Print "Parcelas" + Space(40 - Len("Parcelas") - Len(lblParcelasCBR643.Caption)) + lblParcelasCBR643.Caption
                Printer.Print "T�tulos" + Space(40 - Len("T�tulos") - Len(lblVlrTitulos.Caption)) + lblVlrTitulos.Caption
                Printer.Print "Desconto" + Space(40 - Len("Desconto") - Len(lblVlrDesconto.Caption)) + lblVlrDesconto.Caption
                Printer.Print "IOF" + Space(40 - Len("IOF") - Len(lblVlrIOF.Caption)) + lblVlrIOF.Caption
                Printer.Print "Tarifa" + Space(40 - Len("Tarifa") - Len(lblVlrTarifa.Caption)) + lblVlrTarifa.Caption
                Printer.Print "Outras despesas" + Space(40 - Len("Outras despesas") - Len(lblVlrOutras.Caption)) + lblVlrOutras.Caption
                Printer.Print "Juros de desconto" + Space(40 - Len("Juros de desconto") - Len(lblVlrJurosDesconto.Caption)) + lblVlrJurosDesconto.Caption
                Printer.Print "IOF de desconto" + Space(40 - Len("IOF de desconto") - Len(lblVlrIOFDesconto.Caption)) + lblVlrIOFDesconto.Caption
                Printer.Print "Abatimento" + Space(40 - Len("Abatimento") - Len(lblVlrAbatimento.Caption)) + lblVlrAbatimento.Caption
                Printer.Print "Desconto concedido" + Space(40 - Len("Desconto concedido") - Len(lblVlrAbatimento.Caption)) + lblVlrAbatimento.Caption
                Printer.Print "Recebido" + Space(40 - Len("Recebido") - Len(lblVlrRecebido.Caption)) + lblVlrRecebido.Caption
                Printer.Print "Juros de Mora" + Space(40 - Len("Juros de Mora") - Len(lblVlrJurosMora.Caption)) + lblVlrJurosMora.Caption
                Printer.Print "Outros recebimentos" + Space(40 - Len("Outros recebimentos") - Len(lblVlrOutrosRecebimentos.Caption)) + lblVlrOutrosRecebimentos.Caption
                Printer.Print "Abat. N�o Aproveit." + Space(40 - Len("Abat. N�o Aproveit.") - Len(lblVlrAbatimNaoAproveitado.Caption)) + lblVlrAbatimNaoAproveitado.Caption
                Printer.Print "Lan�amento" + Space(40 - Len("Lan�amento") - Len(lblVlrLancamento.Caption)) + lblVlrLancamento.Caption
                Printer.Print "Valor de ajuste" + Space(40 - Len("Valor de ajuste") - Len(lblVlrAjuste.Caption)) + lblVlrAjuste.Caption
                Printer.Print "Lan�amento dep�sito" + Space(40 - Len("Lan�amento dep�sito") - Len(lblVlrLancDeposito.Caption)) + lblVlrLancDeposito.Caption
            Case "CDR753"
                Printer.Print "Remessa" + Space(40 - Len("Remessa") - Len(lblRemessaCDR753.Caption)) + lblRemessaCDR753.Caption
                Printer.Print "Data da remessa" + Space(40 - Len("Data da remessa") - Len(lblDtaArqCDR753.Caption)) + lblDtaArqCDR753.Caption
                Printer.Print "Parcelas" + Space(40 - Len("Parcelas") - Len(lblParcelasCDR753.Caption)) + lblParcelasCDR753.Caption
                Printer.Print "Pr�mio Bruto" + Space(40 - Len("Pr�mio Bruto") - Len(lblPremioBrutoCDR753.Caption)) + lblPremioBrutoCDR753.Caption
            Case "SEG498D"
                Printer.Print "Remessa" + Space(40 - Len("Remessa") - Len(lblRemessaSEG498D.Caption)) + lblRemessaSEG498D.Caption
                Printer.Print "Parcelas" + Space(40 - Len("Remessa") - Len(lblParcelasSEG498D.Caption)) + lblParcelasSEG498D.Caption
                Printer.Print "Cobran�a" + Space(40 - Len("Remessa") - Len(lblCobrancaSEG498D.Caption)) + lblCobrancaSEG498D.Caption
                Printer.Print "IOF" + Space(40 - Len("Remessa") - Len(lblIOFSEG498D.Caption)) + lblIOFSEG498D.Caption
                Printer.Print "Corretagem" + Space(40 - Len("Remessa") - Len(lblCorretagemSEG498D.Caption)) + lblCorretagemSEG498D.Caption
                Printer.Print "Repasse" + Space(40 - Len("Remessa") - Len(lblRepasseSEG498D.Caption)) + lblRepasseSEG498D.Caption
        End Select
    Else
        Printer.Font = "Arial"
        Printer.FontSize = 16
        Printer.FontBold = True
        Printer.Print "Arquivo de cobran�a - " + vProdNome
        'Printer.FontSize = 12
        'Printer.Print "Produto " + vProdNome
            
        Printer.Font = "Courier New"
        Printer.FontSize = 10
        Printer.Print "Arquivo     " + vbTab + "Data"
        Printer.Print "------------" + vbTab + "--------"
        Printer.FontBold = False
            
        For vRow = 1 To sprArq.Rows - 1
            vTemp = ""
            sprArq.Row = vRow
            sprArq.Col = 0
            vTemp = sprArq.Text
            sprArq.Col = 1
            vTemp = vTemp + vbTab + sprArq.Text
            Printer.Print vTemp
        Next
    End If
    
    Printer.EndDoc
    MsgBox "Impress�o enviada"
    Screen.MousePointer = 0
End Sub

Private Sub Imprimir()

    On Error GoTo TrataErro

    Dim vTemp As String
    Dim C, R As Integer
    
    If optCaminho(0).Value = True Then
        If File1.FileName = "" Then
            MsgBox "N�o existe arquivo para libera��o.", vbInformation, "Aviso"
            Exit Sub
        End If
        If MsgBox("Confirma autoriza��o para libera��o do arquivo " & File1.FileName & " ?", vbQuestion + vbYesNo, "Aviso") = vbNo Then
            Exit Sub
        End If
        
        'vTemp = get_ini(App.PATH & "\ConsDevPremio.ini", "OPTIONS", "SAIDA_Liberados")
        vTemp = LerArquivoIni("ARQUIVOS DEVOLUCAO PREMIO", "SAIDA_Liberados")
        '*
        'Comentar
        '*
        'vTemp = App.PATH & "\Liberados\"
    
        FileCopy File1.PATH & "\" & File1.FileName, vTemp & File1.FileName

        optCaminho(0).Value = True
        cmdImprimir.Enabled = False
       
        Kill File1.PATH & "\" & File1.FileName
        
        'Gustavo.Macedo - 19/02/2013 - Inicio - RCA094
        Call rdocn.Execute("EXEC SEGS10923_SPU '" & File1.FileName & "'")
        'Gustavo.Macedo - 19/02/2013 - Fim - RCA094
        
        StatusBar1.SimpleText = "Enviando E-Mail para produ��o..."
        Call EnviarEmail("Produ��o," & vbCrLf & vbCrLf & _
           "Arquivo de Pagamentos Autorizados esta liberado para ser enviado ao Banco do Brasil." & vbCrLf & vbCrLf & _
           "Localiza��o do arquivo: \Arquivos Devolucao Premio\Liberados" & vbCrLf & vbCrLf & _
           "Mensagem enviada automaticamente pelo sistema.")
        File1.Refresh
    End If
    
    MsgBox "Libera��o efetuada da Remessa SEG452 / BBC452." & lblMinData, vbInformation, "Aviso"
    lblMinData.Caption = ""
    lblRemessa.Caption = ""
    lblMaxData.Caption = ""
    sprDetalhes.Rows = 1
    sprDetalhes.Visible = False
    StatusBar1.SimpleText = ""
    
    Exit Sub
    Resume
TrataErro:
    TrataErroGeral "Rotina : Libera��o - " & Err.Description, "SEGP0445"
    TerminaSEGBR
    
End Sub

Public Function ValidarUsuario(ByVal sCpf As String) As Boolean

    Dim rs   As Recordset
    Dim sSQL As String

    ValidarUsuario = False

    '
    '
    'sSQL = ""
    'sSQL = sSQL & " SELECT usuario_tb.nome, " & vbNewLine
    'sSQL = sSQL & "        usuario_tb.CPF, " & vbNewLine
    'sSQL = sSQL & "        usuario_SISBB_tb.chave " & vbNewLine
    'sSQL = sSQL & "   FROM SEGAB_DB..usuario_tb usuario_tb " & vbNewLine
    'sSQL = sSQL & "   JOIN usuario_SISBB_tb usuario_SISBB_tb " & vbNewLine
    'sSQL = sSQL & "     ON usuario_tb.login_rede = usuario_SISBB_tb.login " & vbNewLine
    'sSQL = sSQL & "  WHERE usuario_tb.cpf = " & sCpf & vbNewLine
    'sSQL = sSQL & "    AND usuario_tb.situacao = 'A'"
    '
    'Set rs = rdocn.Execute(sSQL)
    '
    'If rs.EOF Then
    '    Set rs = Nothing
    '    Exit Function
    'Else
    '    frmSenhaAcesso.lblUsuario.Caption = rs("nome")
    '    frmSenhaAcesso.lblCPF.Caption = rs("cpf")
    '    frmSenhaAcesso.lblChaveSEGUR.Caption = rs("chave")
    '    frmSenhaAcesso.txtEmpresa.Text = glAmbiente_id
    ''    frmSenhaAcesso.Show (modal)
    '    Set rs = Nothing
    '    ValidarUsuario = True
    'End If
    ValidarUsuario = True
    Exit Function

End Function

Private Sub cmdImprimir_Click()
    Dim sCpf        As String
    Dim strSql      As String
    'sCpf = BuscaCpf(cUserName)
    '
    'If sCpf = "" Then
    '    MsgBox "Usu�rio sem acesso a fun��o !!!", vbOKOnly + vbExclamation
    '    Exit Sub
    'End If
    '
    'frmSenhaAcesso.Show 1
   If lblMaxData <> "" Or lblMinData <> "" Then
   
    'FLAVIO.BEZERRA - 11/09/2013 - 14413025 - ALTERAR O PROCESSO DE ENVIO DO ARQUIVO SEG452
    If bytProcesso = 1 Then

        Screen.MousePointer = vbHourglass
        rdocn.BeginTrans
        
        strSql = ""
        'strSql = strSql & "exec desenv_db.dbo.Libera_Lote_PreRemessa_SPU " & CStr(lblMinData) & vbNewLine
        strSql = strSql & "exec seguros_db.dbo.SEGS11203_SPU " & CStr(lblMinData) & ", '" & cUserName & "'" & vbNewLine
        strSql = strSql & "exec seguros_db.dbo.SEGS11661_SPI " & CStr(lblMinData) & ", '" & cUserName & "'" & vbNewLine

        rdocn.Execute (strSql)
        rdocn.CommitTrans
        
        Screen.MousePointer = vbNormal
    Else
        Call Imprimir
    End If
    'FIM DA ALTERA��O-----------------------------------------------------------------------
    
    optCaminho_Click (0)
   End If
End Sub
Public Function BuscaCpf(ByVal sLogin As String)

    Dim rs   As Recordset
    Dim sSQL As String
    
    'INICIO - INC000003993540 - EDNETO - 20130327
    'EXECUTA A PROCEDURE EM AB QUANDO EM ABS.
    If glAmbiente_id = 6 Or glAmbiente_id = 7 Then
        sSQL = "Set NoCount on Exec abss.segab_db.dbo.retorna_cpf_sps '" & sLogin & "'"
    Else
        sSQL = "Set NoCount on Exec segab_db..retorna_cpf_sps '" & sLogin & "'"
    End If
    'FIM - INC000003993540 - EDNETO - 20130327
    'Dim objDB As Object 'New cls000100
    Set rs = rdocn.Execute(sSQL)

    '    Set rs = ExecutarSQL("SEGBR", _
    '                         2, _
    '                         App.Title, _
    '                         App.FileDescription, _
    '                         sSQL, True, False, 30000, 1, 2, 0)
    '
    If rs.State = 0 Or Err <> 0 Then GoTo TrataErro

    If rs.EOF Then
        BuscaCpf = ""
    Else
        BuscaCpf = Trim(rs.Fields("CPF"))
    End If

TrataErro:
End Function

Private Sub cmdParcelas_Click()

    Call Detalhe_SEG452

End Sub

Private Sub cmdRecusa_Click() 'FLAVIO.BEZERRA 08/06/2013
Dim strSql              As String

If lblMaxData <> "" Or lblMinData <> "" Then

        Screen.MousePointer = vbHourglass
        rdocn.BeginTrans
        
        strSql = ""
        '' GENESCO SILVA - FLOW 18313521 - MIGRA��O SQL SERVER
        'StrSQL = StrSQL & "If isnull(OBJECT_ID('tempdb..#RemessaLiberada'),0) >0                                                                    " & vbNewLine
        strSql = strSql & "If OBJECT_ID('tempdb..#RemessaLiberada') IS NOT NULL                                                                     " & vbNewLine
        strSql = strSql & "BEGIN                                                                                                                    " & vbNewLine
        strSql = strSql & "     DROP TABLE #RemessaLiberada                                                                                         " & vbNewLine
        strSql = strSql & "END                                                                                                                      " & vbNewLine
        strSql = strSql & "SELECT ps_mov_cliente_tb.Movimentacao_ID                                Movimentacao_ID                                                 " & vbNewLine
        strSql = strSql & ", ps_mov_cliente_preremessa_tb.Controle_Interno_PreRemessa_ID           Controle_Interno_PreRemessa_ID                                                 " & vbNewLine
        strSql = strSql & ", ps_mov_cliente_tb.Remessa_Liberada                                    Situacao                                                                                                                " & vbNewLine
        strSql = strSql & "INTO #RemessaLiberada                                                                                                    " & vbNewLine
        strSql = strSql & "FROM ps_mov_cliente_preremessa_tb                                       ps_mov_cliente_preremessa_tb with (nolock)        " & vbNewLine
        strSql = strSql & "JOIN ps_mov_cliente_tb                                                  ps_mov_cliente_tb with (nolock)                   " & vbNewLine
        strSql = strSql & "ON ps_mov_cliente_tb.Movimentacao_ID                                    = ps_mov_cliente_preremessa_tb.Movimentacao_ID   " & vbNewLine
        strSql = strSql & "AND ps_mov_cliente_preremessa_tb.Lote_Liberado                          IS NULL                                          " & vbNewLine
        strSql = strSql & "WHERE ps_mov_cliente_tb.Remessa_Liberada                                = 'S'                                            " & vbNewLine
        strSql = strSql & "AND ps_mov_cliente_preremessa_tb.Controle_Interno_PreRemessa_ID         = " & lblMinData
        
        strSql = strSql & "   Update #RemessaLiberada                                            " & vbNewLine
        strSql = strSql & "      Set Situacao                                       = null       " & vbNewLine
        strSql = strSql & "    Where #RemessaLiberada.Situacao = 'S'                             " & vbNewLine
   
        
        'strSql = strSql & " Exec desenv_db.dbo.SEGS11158_SPU"
        strSql = strSql & " Exec seguros_db.dbo.SEGS11158_SPU '" & cUserName & "', 'SEGP0445', 'N'" & vbNewLine
        
        strSql = strSql & " Exec seguros_db.dbo.SEGS11301_SPU " & CStr(lblMinData) & vbNewLine
        
        rdocn.Execute (strSql)
        rdocn.CommitTrans
        
        Screen.MousePointer = vbNormal
        ConsultaPreRemessa_SEG452 (0)

End If
End Sub

Private Sub cmdSair_Click()
    rdocn.Close
    '   rdoConexao.Close
    End
End Sub

Private Sub File1_Click()
 
    Call ImportaArquivo

End Sub

Private Sub Form_Load()

    Dim vTemp()
    Dim vCounter   As Integer
    Dim vIndex     As Integer
    Dim vProduto() As String
    Dim vItem      As Integer
    
'    bytProcesso = Left(Command, 1)
    
    Call gFunCentraTela(Me)
    
    'Call Funcoes_ADO.Conexao
    
    'Call Seguranca("SEGP0445", "Consulta Arquivos de Devolu��o de Restitui��o Pr�mio")
    Me.Show
    'Call conexao_auxiliar
    Me.Height = 8985
    Me.Width = 13245
    Me.Caption = "SEGP0445 - Consulta Arquivos de Devolu��o de Restitui��o Pr�mio - SEG452- BBC452 " & Ambiente
    optCaminho(0).Value = True
    'cmdImprimir.Enabled = False
    
End Sub

Private Sub lstArq_Click()
    ' ******************************************************
    ' Altera��o  : Retirar limita��o de carga de arquivos
    '              menores que 1K
    ' Respons�vel: Edson F. Yoshino
    ' Data       : 21/08/2001
    ' ******************************************************
    Dim vTemp As String
    Dim vArq()
    Dim vIndex As Integer
    
    'If Mid(lstArq, 1, 5) <> "�����" Then
    'lstFile.PATH = vPath + lstArq + vCaminho
    Screen.MousePointer = 11
        
    StatusBar1.SimpleText = "Verificando arquivos existentes..."
        
    sprArq.Clear
    sprArq.Rows = 2
        
    For Each Control In frmMain.Controls
        If Mid(Control.Tag, 1, 3) = "lbl" Then
            Control.Caption = ""
        End If
    Next Control
        
    'cmdParcelas.Visible = False
    cmdParcelas.Enabled = False
        
    pnlResumo.Visible = False
        
    '        If Trim(lstArq.Text) = "Baixa" Then
    '            vLayout = "ECBAI504"
    '        ElseIf Trim(lstArq.Text) = "Cancelamento" Then
    '            vLayout = "ECCAN504"
    '        ElseIf Trim(lstArq.Text) = "Emissao" Then
    '            vLayout = "EC504"
    '        ElseIf Trim(lstArq.Text) = "Sinistros" Then
    '            vLayout = "SIN"
    '        End If
        
    If Dir(vPath + lstArq + vCaminho + "*.*") <> "" Then
        ReDim vArq(2, 1)
        If vLayout = "SIN" Then
            vTemp = Dir(vPath + lstArq + vCaminho + "*.*")
                
            If Val(Mid(vTemp, InStr(1, vTemp, ".") + 1, 5)) > 0 Then
                vArq(1, 1) = Dir(vPath + lstArq + vCaminho + "*.*")
                vArq(2, 1) = Format(FileDateTime(vPath + lstArq + vCaminho + vArq(1, 1)), "yyyymmdd")
            End If
        Else
            vTemp = Dir(vPath + lstArq + vCaminho + vLayout + "*.*")
            If Val(Mid(vTemp, InStr(1, vTemp, ".") + 1, 4)) > 0 Then
                vArq(1, 1) = Dir(vPath + lstArq + vCaminho + vLayout + "*.*")
                vArq(2, 1) = Format(FileDateTime(vPath + lstArq + vCaminho + vArq(1, 1)), "yyyymmdd")
            End If
        End If
            
        vIndex = 1
        Do Until vTemp = ""
            vTemp = Dir
            If vTemp <> "" And Val(Mid(vTemp, InStr(1, vTemp, ".") + 1, 4)) > 0 Then
                vIndex = vIndex + 1
                ReDim Preserve vArq(2, vIndex)
                '' N�o carrega arquivos menores que 1K
                'If FileLen(vPath + lstArq + vCaminho + vTemp) > 1024 Then
                If vTemp <> "" Then
                    vArq(1, vIndex) = vTemp
                    vArq(2, vIndex) = Format(FileDateTime(vPath + lstArq + vCaminho + vArq(1, vIndex)), "yyyymmdd")
                Else
                    ReDim Preserve vArq(2, vIndex - 1)
                End If
                'Else
                '    vIndex = vIndex - 1
                '    ReDim Preserve vArq(2, vIndex)
                'End If
            End If
        Loop
            
        Call gFunSelectionSort(vArq)
            
        Call mSubCarregaData(vArq())
    Else
        MsgBox "N�o existem arquivos Gerados para este produto"
    End If
        
    Screen.MousePointer = 0
    StatusBar1.SimpleText = ""
    'End If
End Sub

Function gFunSelectionSort(TempArray As Variant)
    Dim MaxVal   As Variant
    Dim MaxIndex As Integer
    Dim i, j As Integer
    
    ' Varre a array iniciando com o �ltimo elemento
    For i = UBound(TempArray, 2) To 1 Step -1

        ' Seta MaxVal para o elemento na array e
        ' salva o index deste como MaxIndex
        MaxVal = TempArray(1, i)
        MaxVal2 = TempArray(2, i)
        MaxIndex = i

        ' Loop atrav�s dos elementos restantes verificando se algum � menor que MaxVal.
        ' Caso positivo, seta este elemento como o novo MaxVal.
        For j = 1 To i
            'If TempArray(2, j) > MaxVal Then
            If TempArray(2, j) < MaxVal2 Then
                MaxVal2 = TempArray(2, j)
                MaxVal = TempArray(1, j)
                MaxIndex = j
            End If
        Next j

        ' Se o index do maior elemento n�o � i,
        ' ent�o, troca este elemento com o elemento i.
        If MaxIndex < i Then
            TempArray(1, MaxIndex) = TempArray(1, i)
            TempArray(2, MaxIndex) = TempArray(2, i)
            TempArray(1, i) = MaxVal
            TempArray(2, i) = MaxVal2
        End If
    Next i
    'vTemp = TempArray
End Function

Public Sub mSubCarregaData(vArq())
    
    sprArq.Clear
    sprArq.Visible = False
    sprArq.Rows = 2
    sprArq.Cols = 2
    sprArq.FixedCols = 0
    sprArq.FixedRows = 1
    sprArq.Row = 0
    sprArq.Col = 0
    sprArq.ColWidth(0) = 1500
    sprArq.Text = "Arquivo"
    sprArq.Col = 1
    sprArq.ColWidth(1) = 1100
    sprArq.Text = "Data"
    
    'Call gFunRedimGrid(pnlArq, sprArq)
    
    For N = 1 To UBound(vArq, 2)
        sprArq.Rows = N + 1
        sprArq.Row = N
        sprArq.Col = 0
        sprArq.Text = vArq(1, N)
        sprArq.Col = 1
        sprArq.Text = Mid(vArq(2, N), 7, 2) + "/" + Mid(vArq(2, N), 5, 2) + "/" + Mid(vArq(2, N), 1, 4)
    Next
    sprArq.Visible = True
    lblSprArq.Visible = True
End Sub

Private Sub sprArqOLD_DblClick()
    Dim vTemp        As String
    Dim vVlrPago     As Double
    Dim vVlrIOF      As Double
    Dim vVlrJuros    As Double
    Dim vVlrRemunera As Double
    
    Screen.MousePointer = 11
    sprDetalhe.Visible = False
    
    sprDetalhe.Clear
    
    sprArq.Col = 0
    Open vPath + lstArq + "\Gerados\" + _
       Trim(sprArq.Text) For Input Access Read As #1
    
    'Header
    Line Input #1, vTemp
    If Mid(vTemp, 1, 6) <> "000000" Or _
       Mid(vTemp, 7, 2) <> "51" Then
        MsgBox "Arquivo com cabe�alho inv�lido"
    Else
        sprDetalhe.Rows = 2
        sprDetalhe.Cols = 9
        sprDetalhe.FixedRows = 1
        sprDetalhe.FixedCols = 0
        
        sprDetalhe.Row = 0
        sprDetalhe.Col = 0
        sprDetalhe.ColWidth(0) = 450
        sprDetalhe.Text = "Rem."
        
        sprDetalhe.Col = 1
        sprDetalhe.ColWidth(1) = 1000
        sprDetalhe.Text = "Data Arq."
        
        sprDetalhe.Col = 2
        sprDetalhe.ColWidth(2) = 1250
        sprDetalhe.Text = "Pr�mio Bruto"
        
        sprDetalhe.Col = 3
        sprDetalhe.ColWidth(3) = 1250
        sprDetalhe.Text = "IOF"
        
        sprDetalhe.Col = 4
        sprDetalhe.ColWidth(4) = 1250
        sprDetalhe.Text = "Bruto-IOF"
        
        sprDetalhe.Col = 5
        sprDetalhe.ColWidth(5) = 1250
        sprDetalhe.Text = "Corretagem"
        
        sprDetalhe.Col = 6
        sprDetalhe.ColWidth(6) = 0
        sprDetalhe.Text = "Estipulante"
        
        sprDetalhe.Col = 7
        sprDetalhe.ColWidth(7) = 1250
        sprDetalhe.Text = "Repasse"
        
        sprDetalhe.Col = 8
        sprDetalhe.ColWidth(8) = 1000
        sprDetalhe.Text = "Registros"
        
        sprDetalhe.Row = 1
        sprDetalhe.Col = 0
        sprDetalhe.Text = Val(Mid(vTemp, 20, 4))
        sprDetalhe.Col = 1
        sprDetalhe.Text = Mid(vTemp, 30, 2) + "/" + Mid(vTemp, 32, 2) + "/" + Mid(vTemp, 34, 4)
        
        vVlrPago = 0
        vIOF = 0
        vVlrJuros = 0
        vVlrRemunera = 0
        Do Until EOF(1)
            Line Input #1, vTemp
            If Mid(vTemp, 1, 6) = "999999" And Mid(vTemp, 7, 2) = "51" Then
                sprDetalhe.Col = 5
                sprDetalhe.Text = Format(Val(Mid(vTemp, 43, 15) + "." + Mid(vTemp, 58, 2)), "#,#0.00")
                sprDetalhe.Col = 7
                sprDetalhe.Text = Format(Val(Mid(vTemp, 26, 15) + "." + Mid(vTemp, 41, 2)), "#,#0.00")
                sprDetalhe.Col = 8
                sprDetalhe.Text = Format(Val(Mid(vTemp, 20, 6)), "#,#00")
            Else
                vVlrPago = vVlrPago + Val(Mid(vTemp, 57, 13) + "." + Mid(vTemp, 70, 2))
                vVlrIOF = vVlrIOF + Val(Mid(vTemp, 77, 13) + "." + Mid(vTemp, 90, 2))
                vVlrJuros = vVlrJuros + Val(Mid(vTemp, 132, 7) + "." + Mid(vTemp, 139, 2))
                vVlrRemunera = vVlrRemunera + Val(Mid(vTemp, 141, 7) + "." + Mid(vTemp, 147, 2))
            End If
        Loop
        sprDetalhe.Col = 2
        sprDetalhe.Text = Format(vVlrPago, "#,#0.00")
        sprDetalhe.Col = 3
        sprDetalhe.Text = Format(vVlrIOF, "#,#0.00")
        sprDetalhe.Col = 4
        sprDetalhe.Text = Format(vVlrPago - vVlrIOF, "#,#0.00")
        'sprDetalhe.Col = 6
        'sprDetalhe.Text = Format(vVlrJuros, "#,#0.00")
        'sprDetalhe.Text = Format(vVlrRemunera, "#,#0.00")
    End If
    
    Close #1
    
    sprDetalhe.Visible = True
    Screen.MousePointer = 0
End Sub

Private Sub lstPreRemessa_Click()
  
  sprDetalhes.Clear
  sprDetalhes.Visible = False
  
  If lstPreRemessa.SelectedItem.Tag = "Remessa" Then
    bytProcesso = 2
    cmdRecusa.Enabled = False
    File1.FileName = lstPreRemessa.SelectedItem.Text
    File1_Click
  Else
    bytProcesso = 1
    cmdRecusa.Enabled = True
    cmdParcelas.Enabled = False
    lblMinData = lstPreRemessa.SelectedItem.Tag
    lblMaxData = lstPreRemessa.SelectedItem.SubItems(1)
    lblRemessa = FormataValorParaPadraoBrasil(lstPreRemessa.SelectedItem.SubItems(2))
    txtData = lstPreRemessa.SelectedItem.SubItems(3)
  End If
End Sub

Private Sub optCaminho_Click(Index As Integer)

    lblMinData.Caption = ""
    lblRemessa.Caption = ""
    lblMaxData.Caption = ""
    txtData.Text = ""
     ' Racras - inclus�o dos arquivos "BBC452"
    File1.Pattern = "BBC452*.* ;SEG452*.* "
    sprDetalhes.Clear
    cmdImprimir.Enabled = False
    sprDetalhes.Visible = False
    'cmdParcelas.Visible = False
    cmdParcelas.Enabled = False
    File1.Refresh
    vSql = "IF EXISTS (SELECT 1 FROM TempDB..sysobjects WHERE NAME = '##tmp_arquivo_" & cUserName & "') " & vbNewLine
    vSql = vSql + "     DROP TABLE ##tmp_arquivo_" & cUserName
    
    rdocn.Execute vSql, 64
    
    Select Case Index
        Case 0
            cmdImprimir.Enabled = True
            cmdRecusa.Enabled = True    'FLAVIO.BEZERRA - 14413025 - ALTERAR O PROCESSO DE ENVIO DO ARQUIVO SEG452 - 18/06/2013
            ' lorigem = get_ini(App.PATH & "\ConsDevPremio.ini", "OPTIONS", "SAIDA_GERADOS")
            lorigem = LerArquivoIni("ARQUIVOS DEVOLUCAO PREMIO", "SEG452_PATH") & "\"
            '*
            'Comentar
            'lorigem = App.PATH & "\Gerados\"
            '*
        Case 1
            cmdRecusa.Enabled = False   'FLAVIO.BEZERRA - 14413025 - ALTERAR O PROCESSO DE ENVIO DO ARQUIVO SEG452 - 18/06/2013
            'lorigem = get_ini(App.PATH & "\ConsDevPremio.ini", "OPTIONS", "SAIDA_Enviados")
            lorigem = LerArquivoIni("ARQUIVOS DEVOLUCAO PREMIO", "SAIDA_Enviados")
            '*
            'Comentar
            'lorigem = App.PATH & "\Enviados\"
            '*
        Case 2
            cmdRecusa.Enabled = False   'FLAVIO.BEZERRA - 14413025 - ALTERAR O PROCESSO DE ENVIO DO ARQUIVO SEG452 - 18/06/2013
            'lorigem = get_ini(App.PATH & "\ConsDevPremio.ini", "OPTIONS", "SAIDA_Liberados")
            lorigem = LerArquivoIni("ARQUIVOS DEVOLUCAO PREMIO", "SAIDA_Liberados")
            '*
            'Comentar
            'lorigem = App.PATH & "\Liberados\"
            '*
    End Select
    'File1.PATH = lorigem
    File1.Refresh
    
   'FLAVIO.BEZERRA - IN�CIO - 14/06/2013
    ConsultaPreRemessa_SEG452 (Index)
   'FLAVIO.BEZERRA - FIM - 14/06/2013
    
End Sub

Private Sub sprArq_DblClick()
    Dim vTemp                    As String
    Dim vVlrPago                 As Double
    Dim vVlrTitulo               As Double
    Dim vVlrIOF                  As Double
    Dim vVlrJuros                As Double
    Dim vVlrRemunera             As Double
    Dim vVlrTarifa               As Double
    Dim vNumLin                  As Long
    Dim vVlrDesconto             As Double
    Dim vVlrOutras               As Double
    Dim vVlrJurosDesc            As Double
    Dim vVlrIOFDesc              As Double
    Dim vVlrAbatimento           As Double
    Dim vVlrDescConcedido        As Double
    Dim vVlrRecebido             As Double
    Dim vVlrJurosMora            As Double
    Dim vVlrOutrosRecebimentos   As Double
    Dim vVlrAbatimNaoAproveitado As Double
    Dim vVlrLancamento           As Double
    Dim vVlrAjuste               As Double
    Dim vVlrLancamDeposito       As Double
    Dim vVlrCorretagem           As Double
    Dim vVlrRepasse              As Double
    
    Dim vRemessa                 As String
    Dim rc                       As ADODB.Recordset
    Dim vRamo                    As Integer
    Dim vQtdRamo                 As Long
    Dim vValorPago               As Double
    Dim vComissao                As Double
    Dim vAdicFrac                As Double
    Dim vGastosLider             As Double
    Dim vPremioLiq               As Double
    Dim vTpPagamento             As Integer
    Dim vMoeda                   As Integer
    Dim vTpDocumento             As Integer
    Dim vQtdParcela              As Long
    
    On Error GoTo TrataErro
    
    Screen.MousePointer = 11
    
    lblMaxData.Caption = ""
    lblMinData.Caption = ""
    
    StatusBar1.SimpleText = "Lendo arquivo ..\" + lstArq.Text + "\" + Trim(sprArq.Text)
    
    sprArq.Col = 0
    Open vPath + lstArq.Text + vCaminho + _
       Trim(sprArq.Text) For Input Access Read As #1
        
    'Header
    'Line Input #1, vTemp
    
    vSql = ""
    vSql = vSql + "IF EXISTS ( SELECT 1 FROM sysobjects WHERE NAME = '##tmp_arquivo' AND TYPE = 'u' ) "
    vSql = vSql + "     DROP TABLE ##tmp_arquivo "
    vSql = vSql + " CREATE TABLE ##tmp_arquivo "
    vSql = vSql + "     ( Linha varchar(8000)) "
    rdocn.Execute vSql, 64
    
    Select Case vLayout
            '
        Case "ECBAI504" 'Baixa
            vNumLin = Int(LOF(1) / 151) + 10
        
            pnlProg.Min = 0
            pnlProg.Max = vNumLin
            pnlProg.Visible = True
            pnlProg.Value = 0
    
            vArqNome = Trim(sprArq.Text)
            vRemessa = Mid(vArqNome, InStr(1, vArqNome, ".") + 1, 4)
        
            vVlrPago = 0
            vIOF = 0
            vVlrJuros = 0
            vVlrRemunera = 0
        
            Do Until EOF(1)
                DoEvents
                If pnlProg.Value < pnlProg.Max Then
                    pnlProg.Value = pnlProg.Value + 1
                End If
            
                Line Input #1, vTemp
                vSql = ""
                vSql = vSql + "INSERT INTO "
                vSql = vSql + "     ##tmp_arquivo "
                vSql = vSql + "( Linha ) Values ("
                vSql = vSql + " '" + vTemp + "' )"
                rdocn.Execute vSql, 64
            Loop
        
            vSql = ""
            If vTabTmp <> "##tmp_ecbai504" Then
                If vTabTmp <> "" Then
                    vSql = vSql + "DROP TABLE "
                    vSql = vSql + "     " + vTabTmp + " "
                    'vSql = vSql + " GO "
                End If
                vSql = vSql + "CREATE TABLE                                 "
                vSql = vSql + "   ##tmp_ecbai504                            "
                vSql = vSql + "   (                                         "
                vSql = vSql + "     cod_lider       numeric,                "
                vSql = vSql + "     nome_segurado   varchar(40),            "
                vSql = vSql + "     ramo_id            int,                 "
                vSql = vSql + "     apolice_id      numeric,                "
                vSql = vSql + "     endosso_id      int,                    "
                vSql = vSql + "     num_ordem       numeric(13,0),          "
                vSql = vSql + "     parcela         int,                    "
                vSql = vSql + "     qtd_parcelas    int,                    "
                vSql = vSql + "     dt_pagamento    smalldatetime,          "
                vSql = vSql + "     premio_tarifa   numeric(11,2),          "
                vSql = vSql + "     comissao        numeric(11,2),          "
                vSql = vSql + "     gastos_lider_cosseguro  numeric(11,2),  "
                vSql = vSql + "     adic_fracionamento      numeric(11,2),  "
                vSql = vSql + "     premio_liquido numeric(11,2)            "
                vSql = vSql + "   )                                         "
            Else
                vSql = vSql + "TRUNCATE TABLE                               "
                vSql = vSql + "   ##tmp_ecbai504                            "
            End If
            rdocn.Execute vSql, 64
        
            vSql = ""
            vSql = vSql + "INSERT INTO "
            vSql = vSql + "     ##tmp_ecbai504 "
            vSql = vSql + "( "
            vSql = vSql + "     cod_lider, "
            vSql = vSql + "     nome_segurado, "
            vSql = vSql + "     ramo_id, "
            vSql = vSql + "     apolice_id, "
            vSql = vSql + "     endosso_id, "
            vSql = vSql + "     num_ordem, "
            vSql = vSql + "     parcela, "
            vSql = vSql + "     qtd_parcelas, "
            vSql = vSql + "     dt_pagamento, "
            vSql = vSql + "     premio_tarifa, "
            vSql = vSql + "     comissao, "
            vSql = vSql + "     gastos_lider_cosseguro, "
            vSql = vSql + "     adic_fracionamento, "
            vSql = vSql + "     premio_liquido "
            vSql = vSql + ") "
            vSql = vSql + "SELECT "
            vSql = vSql + "     CONVERT(int, SUBSTRING(Linha, 1, 4)), "   ' cod_lider
            vSql = vSql + "     SUBSTRING(Linha, 5, 40), "  ' nome_segurado
            vSql = vSql + "     CONVERT(int, SUBSTRING(Linha, 45, 2)), "    ' ramo
            vSql = vSql + "     CONVERT(numeric, SUBSTRING(Linha, 47, 7)), "   ' apolice_id
            vSql = vSql + "     CONVERT(int, SUBSTRING(Linha, 54, 6)), "   ' endosso_id
            vSql = vSql + "     CONVERT(int, SUBSTRING(Linha, 60, 13)), "   ' num_ordem
            vSql = vSql + "     CONVERT(int, SUBSTRING(Linha, 73, 2)), "   ' parcela
            vSql = vSql + "     CONVERT(int, SUBSTRING(Linha, 75, 2)), "   ' qtd_parcelas
            vSql = vSql + "     CONVERT(smalldatetime, SUBSTRING(Linha, 81, 4)+SUBSTRING(Linha, 79, 2)+SUBSTRING(Linha, 77, 2)), "   ' dt_pagamento
            vSql = vSql + "     CONVERT(numeric(11,2), SUBSTRING(Linha, 85, 11)+'.'+SUBSTRING(Linha, 96, 2)), "   ' premio_tarifa
            vSql = vSql + "     CONVERT(numeric(11,2), SUBSTRING(Linha, 98, 11)+'.'+SUBSTRING(Linha, 109, 2)), "   ' comissao
            vSql = vSql + "     CONVERT(numeric(11,2), SUBSTRING(Linha, 111, 11)+'.'+SUBSTRING(Linha, 122, 2)), "   ' gastos_lider_cosseguro
            vSql = vSql + "     CONVERT(numeric(11,2), SUBSTRING(Linha, 124, 11)+'.'+SUBSTRING(Linha, 135, 2)), "   ' adic_fracionamento
            vSql = vSql + "     CONVERT(numeric(11,2), SUBSTRING(Linha, 137, 11)+'.'+SUBSTRING(Linha, 148, 2)) "   ' premio_liquido
            vSql = vSql + "FROM "
            vSql = vSql + "     ##tmp_arquivo "
            rdocn.Execute vSql, 64
        
            vSql = ""
            vSql = vSql + "SELECT "
            vSql = vSql + "     MaxDtPagamento = Max(dt_pagamento), "
            vSql = vSql + "     MinDtPagamento = Min(dt_pagamento) "
            vSql = vSql + "FROM "
            vSql = vSql + "     ##tmp_ecbai504 "
            Set rc = rdocn.Execute(vSql)
        
            If rc.EOF = False Then
                lblMaxData.Caption = Format(rc("MaxDtPagamento"), "dd/mm/yyyy")
                lblMinData.Caption = Format(rc("MinDtPagamento"), "dd/mm/yyyy")
            End If
            rc.Close
        
            vSql = ""
            vSql = vSql + "SELECT "
            vSql = vSql + "     ramo_id, "
            vSql = vSql + "     qtd_ramo = count(1), "
            vSql = vSql + "     valor_pago = sum(premio_tarifa), "
            vSql = vSql + "     comissao = sum(comissao), "
            vSql = vSql + "     gastos_lider = sum(gastos_lider_cosseguro), "
            vSql = vSql + "     adic_fracionamento = sum(adic_fracionamento), "
            vSql = vSql + "     premio_liquido = sum(premio_liquido) "
            'vSql = vSql + "     Max_dt_pagamento = max(dt_pagamento), "
            'vSql = vSql + "     Min_dt_pagamento = min(dt_pagamento) "
            vSql = vSql + "FROM "
            vSql = vSql + "     ##tmp_ecbai504 "
            vSql = vSql + "GROUP BY "
            vSql = vSql + "     ramo_id "
            vSql = vSql + "ORDER BY "
            vSql = vSql + "     ramo_id "
            Set rc = rdocn.Execute(vSql)
        
            vRamo = -1
            vTpPagamento = -1
            vMoeda = -1
        
            sprDetalhes.Visible = False
        
            sprDetalhes.Rows = 1
            sprDetalhes.Cols = 7
        
            sprDetalhes.Row = 0
            sprDetalhes.Col = 0
            sprDetalhes.ColWidth(0) = 600
            sprDetalhes.Text = "Ramo"
            sprDetalhes.Col = 1
            sprDetalhes.ColWidth(1) = 800
            sprDetalhes.Text = "Qtd.Reg."
            sprDetalhes.Col = 2
            sprDetalhes.ColWidth(2) = 1500
            sprDetalhes.Text = "Valor Pago"
            sprDetalhes.Col = 3
            sprDetalhes.ColWidth(3) = 1500
            sprDetalhes.Text = "Comiss�o"
            sprDetalhes.Col = 4
            sprDetalhes.ColWidth(4) = 1500
            sprDetalhes.Text = "Gastos L�der"
            sprDetalhes.Col = 5
            sprDetalhes.ColWidth(5) = 1500
            sprDetalhes.Text = "Adic.Fracionamento"
            sprDetalhes.Col = 6
            sprDetalhes.ColWidth(6) = 1500
            sprDetalhes.Text = "Premio L�quido"
        
            Do Until rc.EOF
                sprDetalhes.Rows = sprDetalhes.Rows + 1
                sprDetalhes.Row = sprDetalhes.Rows - 1
            
                sprDetalhes.Col = 0
                sprDetalhes.Text = rc("ramo_id")
                sprDetalhes.Col = 1
                sprDetalhes.Text = Format(rc("qtd_ramo"), "##,##0")
                vQtdRamo = vQtdRamo + CLng(rc("qtd_ramo"))
                sprDetalhes.Col = 2
                sprDetalhes.Text = Format(CDbl(rc("valor_pago")), "##,##0.#0")
                vValorPago = vValorPago + CDbl(rc("valor_pago"))
                sprDetalhes.Col = 3
                sprDetalhes.Text = Format(CDbl(rc("comissao")), "##,##0.#0")
                vComissao = vComissao + CDbl(rc("comissao"))
                sprDetalhes.Col = 4
                sprDetalhes.Text = Format(CDbl(rc("gastos_lider")), "##,##0.#0")
                vGastosLider = vGastosLider + CDbl(rc("gastos_lider"))
                sprDetalhes.Col = 5
                sprDetalhes.Text = Format(CDbl(rc("adic_fracionamento")), "##,##0.#0")
                vAdicFrac = vAdicFrac + CDbl(rc("adic_fracionamento"))
                sprDetalhes.Col = 6
                sprDetalhes.Text = Format(CDbl(rc("premio_liquido")), "##,##0.#0")
                vPremioLiq = vPremioLiq + CDbl(rc("premio_liquido"))
                rc.MoveNext
            Loop
            rc.Close
        
            sprDetalhes.Rows = sprDetalhes.Rows + 1
            sprDetalhes.Row = sprDetalhes.Rows - 1
        
            sprDetalhes.Col = 0
            sprDetalhes.CellForeColor = vbRed
            sprDetalhes.Text = "TOTAL"
            sprDetalhes.Col = 1
            sprDetalhes.CellForeColor = vbRed
            sprDetalhes.Text = Format(vQtdRamo, "##,##0")
            sprDetalhes.Col = 2
            sprDetalhes.CellForeColor = vbRed
            sprDetalhes.Text = Format(vValorPago, "##,##0.#0")
            sprDetalhes.Col = 3
            sprDetalhes.CellForeColor = vbRed
            sprDetalhes.Text = Format(vComissao, "##,##0.#0")
            sprDetalhes.Col = 4
            sprDetalhes.CellForeColor = vbRed
            sprDetalhes.Text = Format(vGastosLider, "##,##0.#0")
            sprDetalhes.Col = 5
            sprDetalhes.CellForeColor = vbRed
            sprDetalhes.Text = Format(vAdicFrac, "##,##0.#0")
            sprDetalhes.Col = 6
            sprDetalhes.CellForeColor = vbRed
            sprDetalhes.Text = Format(vPremioLiq, "##,##0.#0")
        
            vTabTmp = "##tmp_ecbai504"
        
            sprDetalhes.Visible = True
            pnlResumo.Visible = True
            '
        Case "SIN" 'Sinistros Pagos
            If UCase(Mid(vArqNome, 1, 3)) = "SIN" Then
                vNumLin = Int((LOF(1) / 128) + 10)
            Else
                vNumLin = Int((LOF(1) / 327) + 10)
            End If
        
            pnlProg.Min = 0
            pnlProg.Max = vNumLin
            pnlProg.Visible = True
            pnlProg.Value = 0

            vArqNome = Trim(sprArq.Text)
            vRemessa = Mid(vArqNome, InStr(1, vArqNome, ".") + 1, 4)

            Do Until EOF(1)
                DoEvents
                If pnlProg.Value < pnlProg.Max Then
                    pnlProg.Value = pnlProg.Value + 1
                End If

                Line Input #1, vTemp
                vSql = ""
                vSql = vSql + "INSERT INTO "
                vSql = vSql + "     ##tmp_arquivo "
                vSql = vSql + "( Linha ) Values ( "
                vSql = vSql + " '" + vTemp + "' ) "
                rdocn.Execute vSql, 64
            Loop

            If UCase(Mid(vArqNome, 1, 3)) = "SIN" Then
        
                vSql = ""
                If vTabTmp <> "##tmp_sinpagos" Then
                    If vTabTmp <> "" Then
                        vSql = vSql + "DROP TABLE "
                        vSql = vSql + "     " + vTabTmp + " "
                        'vSql = vSql + " GO "
                    End If
                    vSql = vSql + "CREATE TABLE "
                    vSql = vSql + "   ##tmp_sinpagos "
                    vSql = vSql + "   ( "
                    vSql = vSql + "     ramo_id         int, "
                    vSql = vSql + "     apolice_id      numeric, "
                    vSql = vSql + "     endosso_id      int, "
                    vSql = vSql + "     exercicio       int, "
                    vSql = vSql + "     sinistro        numeric, "
                    vSql = vSql + "     cod_lider       numeric, "
                    vSql = vSql + "     beneficiario    varchar(50), "
                    vSql = vSql + "     tp_pagamento    int, "
                    vSql = vSql + "     valor_pagamento numeric(13,2), "
                    vSql = vSql + "     moeda           int, "
                    vSql = vSql + "     dt_pagamento    smalldatetime, "
                    vSql = vSql + "     valor_pago_seg  numeric(13,2), "
                    vSql = vSql + "     cod_cobertura   int, "
                    'vsql = vsql + "     fator_cambio    numeric(13,2) "
                    vSql = vSql + "     fator_cambio    numeric(9,6) "
                    vSql = vSql + "   ) "
                Else
                    vSql = vSql + "TRUNCATE TABLE "
                    vSql = vSql + "   ##tmp_sinpagos "
                End If
                rdocn.Execute vSql, 64
    
                vSql = ""
                vSql = vSql + "INSERT INTO "
                vSql = vSql + "     ##tmp_sinpagos "
                vSql = vSql + "( "
                vSql = vSql + "     ramo_id, "
                vSql = vSql + "     apolice_id, "
                vSql = vSql + "     endosso_id, "
                vSql = vSql + "     exercicio, "
                vSql = vSql + "     sinistro, "
                vSql = vSql + "     cod_lider, "
                vSql = vSql + "     beneficiario, "
                vSql = vSql + "     tp_pagamento, "
                vSql = vSql + "     valor_pagamento, "
                vSql = vSql + "     moeda, "
                vSql = vSql + "     dt_pagamento, "
                vSql = vSql + "     valor_pago_seg, "
                vSql = vSql + "     cod_cobertura, "
                vSql = vSql + "     fator_cambio "
                vSql = vSql + ") "
                vSql = vSql + "SELECT "
                vSql = vSql + "     CONVERT(int, SUBSTRING(Linha, 1, 2)), "   ' ramo_id
                vSql = vSql + "     CONVERT(numeric, SUBSTRING(Linha, 3, 7)), "   ' apolice_id
                vSql = vSql + "     CONVERT(int, SUBSTRING(Linha, 10, 6)), "    ' endosso_id
                vSql = vSql + "     CONVERT(int, SUBSTRING(Linha, 16, 2)), "   ' exercicio
                vSql = vSql + "     CONVERT(numeric, SUBSTRING(Linha, 18, 5)), "   ' sinistro
                vSql = vSql + "     CONVERT(int, SUBSTRING(Linha, 23, 4)), "   ' cod_lider
                vSql = vSql + "     SUBSTRING(Linha, 27, 50), "  ' beneficiario
                vSql = vSql + "     CONVERT(int, SUBSTRING(Linha, 77, 1)), "   ' tp_pagamento
                vSql = vSql + "     CASE WHEN SUBSTRING(Linha, 77, 1) = '6' OR SUBSTRING(Linha, 77, 1) = '7' THEN "
                vSql = vSql + "         CONVERT(numeric(13,2), '-' + SUBSTRING(Linha, 78, 13)+'.'+SUBSTRING(Linha, 91, 2)) "   ' valor_pagamento
                vSql = vSql + "     ELSE "
                vSql = vSql + "         CONVERT(numeric(13,2), SUBSTRING(Linha, 78, 13)+'.'+SUBSTRING(Linha, 91, 2)) "   ' valor_pagamento
                vSql = vSql + "     END, "
                vSql = vSql + "     CONVERT(int, SUBSTRING(Linha, 93, 2)), "   ' moeda
                vSql = vSql + "     CONVERT(smalldatetime, SUBSTRING(Linha, 95, 6)), " ' dt_pagamento
                vSql = vSql + "     CASE WHEN SUBSTRING(Linha, 77, 1) = '6' OR SUBSTRING(Linha, 77, 1) = '7' THEN "
                vSql = vSql + "         CONVERT(numeric(13,2), '-' + SUBSTRING(Linha, 101, 13)+'.'+SUBSTRING(Linha, 114, 2)) "   ' valor_pago_seg
                vSql = vSql + "     ELSE "
                vSql = vSql + "         CONVERT(numeric(13,2), SUBSTRING(Linha, 101, 13)+'.'+SUBSTRING(Linha, 114, 2)) "   ' valor_pago_seg
                vSql = vSql + "     END, "
                vSql = vSql + "     CONVERT(int, SUBSTRING(Linha, 116, 3)), "   ' cod_cobertura
                'vsql = vsql + "     CONVERT(numeric(13,2), SUBSTRING(Linha, 119, 13)+'.'+SUBSTRING(Linha, 132, 2)) "   ' fator_cambio
                vSql = vSql + "     CONVERT(numeric(9,6), SUBSTRING(Linha, 119, 3)+'.'+SUBSTRING(Linha, 122, 6)) "   ' fator_cambio
                vSql = vSql + "FROM "
                vSql = vSql + "     ##tmp_arquivo "
                rdocn.Execute vSql, 64
    
                vSql = ""
                vSql = vSql + "SELECT "
                vSql = vSql + "     MaxDtPagamento = Max(dt_pagamento), "
                vSql = vSql + "     MinDtPagamento = Min(dt_pagamento) "
                vSql = vSql + "FROM "
                vSql = vSql + "     ##tmp_sinpagos "
                Set rc = rdocn.Execute(vSql)
    
                If rc.EOF = False Then
                    lblMaxData.Caption = Format(rc("MaxDtPagamento"), "dd/mm/yyyy")
                    lblMinData.Caption = Format(rc("MinDtPagamento"), "dd/mm/yyyy")
                End If
                rc.Close
    
                vSql = ""
                vSql = vSql + "SELECT "
                vSql = vSql + "     ramo_id, "
                vSql = vSql + "     tp_pagamento, "
                vSql = vSql + "     moeda, "
                vSql = vSql + "     fator_cambio, "
                vSql = vSql + "     val_pagamento = sum(valor_pagamento * fator_cambio), "
                'vsql = vsql + "     CASE moeda "
                'vsql = vsql + "         WHEN 0 THEN sum(valor_pagamento) "
                'vsql = vsql + "         WHEN 1 THEN sum(valor_pagamento * fator_cambio) "
                'vsql = vsql + "     END as val_pagamento, "
                vSql = vSql + "     val_pago_seg = sum(valor_pago_seg * fator_cambio), "
                'vsql = vsql + "     CASE moeda "
                'vsql = vsql + "         WHEN 0 THEN sum(valor_pago_seg) "
                'vsql = vsql + "         WHEN 1 THEN sum(valor_pago_seg * fator_cambio) "
                'vsql = vsql + "     END as val_pago_seg, "
                vSql = vSql + "     qtd = count(1) "
                vSql = vSql + "FROM "
                vSql = vSql + "     ##tmp_sinpagos "
                vSql = vSql + "GROUP BY "
                vSql = vSql + "     ramo_id, "
                vSql = vSql + "     tp_pagamento, "
                vSql = vSql + "     moeda, "
                vSql = vSql + "     fator_cambio "
                vSql = vSql + "ORDER BY "
                vSql = vSql + "     ramo_id, "
                vSql = vSql + "     tp_pagamento, "
                vSql = vSql + "     moeda "
                Set rc = rdocn.Execute(vSql)
    
                vRamo = -1
                vTpPagamento = -1
                vMoeda = -1
            
                sprDetalhes.Visible = False
    
                sprDetalhes.Rows = 1
                sprDetalhes.Cols = 6
    
                sprDetalhes.Row = 0
                sprDetalhes.Col = 0
                sprDetalhes.ColWidth(0) = 600
                sprDetalhes.Text = "Ramo"
                sprDetalhes.Col = 1
                sprDetalhes.ColWidth(1) = 1000
                sprDetalhes.Text = "Tipo Pag."
                sprDetalhes.Col = 2
                sprDetalhes.ColWidth(2) = 1000
                sprDetalhes.Text = "Moeda"
                sprDetalhes.Col = 3
                sprDetalhes.ColWidth(3) = 1500
                sprDetalhes.Text = "Qtd.Reg."
                sprDetalhes.Col = 4
                sprDetalhes.ColWidth(4) = 1500
                sprDetalhes.Text = "Valor Pagamento"
                sprDetalhes.Col = 5
                sprDetalhes.ColWidth(5) = 1500
                sprDetalhes.Text = "Valor Pago Seguradora"
            
                vQtdRamo = 0
                vValorPago = 0
                vValPagoSeg = 0
            
                Do Until rc.EOF
                    sprDetalhes.Rows = sprDetalhes.Rows + 1
                    sprDetalhes.Row = sprDetalhes.Rows - 1
            
                    If vRamo <> rc("ramo_id") Then
                        sprDetalhes.Col = 0
                        sprDetalhes.Text = rc("ramo_id")
                        vRamo = rc("ramo_id")
                    End If
            
                    'If vTpPagamento <> rc("tp_pagamento") Then
                    sprDetalhes.Col = 1
                    sprDetalhes.Text = rc("tp_pagamento")
                    vTpPagamento = rc("tp_pagamento")
                    'End If
            
                    'If vMoeda <> rc("moeda") Then
                    sprDetalhes.Col = 2
                    sprDetalhes.Text = rc("moeda")
                    vMoeda = rc("moeda")
                    'End If
            
                    sprDetalhes.Col = 3
                    sprDetalhes.Text = Format(rc("qtd"), "##,##0")
                    vQtdRamo = vQtdRamo + rc("qtd")
                    sprDetalhes.Col = 4
                    sprDetalhes.Text = Format(CDbl(rc("val_pagamento")), "#,###.#0")

                    vValorPago = vValorPago + CDbl(rc("val_pagamento"))
                    sprDetalhes.Col = 5
                    sprDetalhes.Text = Format(CDbl(rc("val_pago_seg")), "#,###.#0")
                    vValPagoSeg = vValPagoSeg + CDbl(rc("val_pago_seg"))
                    rc.MoveNext
                Loop
                rc.Close
    
                sprDetalhes.Rows = sprDetalhes.Rows + 1
                sprDetalhes.Row = sprDetalhes.Rows - 1
    
                sprDetalhes.Col = 0
                sprDetalhes.CellForeColor = vbRed
                sprDetalhes.Text = "TOTAL"
                sprDetalhes.Col = 3
                sprDetalhes.CellForeColor = vbRed
                sprDetalhes.Text = Format(vQtdRamo, "##,##0")
                sprDetalhes.Col = 4
                sprDetalhes.CellForeColor = vbRed
                sprDetalhes.Text = Format(vValorPago, "##,##0.#0")
                sprDetalhes.Col = 5
                sprDetalhes.CellForeColor = vbRed
                sprDetalhes.Text = Format(vValPagoSeg, "##,##0.#0")
    
                vTabTmp = "##tmp_sinpagos"
    
                sprDetalhes.Visible = True
                pnlResumo.Visible = True
        
            Else ' Sinistros Avisados
            
                vSql = ""
                If vTabTmp <> "##tmp_sinaviso" Then
                    If vTabTmp <> "" Then
                        vSql = vSql + "DROP TABLE "
                        vSql = vSql + "     " + vTabTmp + " "
                    End If
                    vSql = vSql + "CREATE TABLE "
                    vSql = vSql + "   ##tmp_sinaviso "
                    vSql = vSql + "   ( "
                    vSql = vSql + "     ramo_id            int, "
                    vSql = vSql + "     apolice_id      numeric, "
                    vSql = vSql + "     endosso_id      int, "
                    vSql = vSql + "     exercicio       int, "
                    vSql = vSql + "     sinistro        numeric, "
                    vSql = vSql + "     cod_lider       numeric, "
                    vSql = vSql + "     segurado        varchar(50), "
                    vSql = vSql + "     estipulante     varchar(50), "
                    vSql = vSql + "     perc_partic     numeric(4,2), "
                    vSql = vSql + "     val_is          numeric(15,2), "
                    vSql = vSql + "     val_estima      numeric(15,2), "
                    vSql = vSql + "     dt_sinistro     smalldatetime, "
                    vSql = vSql + "     local_sinistro  varchar(50), "
                    vSql = vSql + "     bens_sinistro   varchar(50), "
                    vSql = vSql + "     nat_evento      varchar(50), "
                    vSql = vSql + "     moeda           int, "
                    vSql = vSql + "     tp_operacao     int, "
                    vSql = vSql + "     item_sinistro   int, "
                    vSql = vSql + "     cod_cobertura   int, "
                    vSql = vSql + "     item_estima     char(1) "
                    vSql = vSql + "   ) "
                Else
                    vSql = vSql + "TRUNCATE TABLE "
                    vSql = vSql + "   ##tmp_sinaviso "
                End If
                rdocn.Execute vSql, 64

                vSql = ""
                vSql = vSql + "INSERT INTO "
                vSql = vSql + "     ##tmp_sinaviso "
                vSql = vSql + "( "
                vSql = vSql + "     ramo_id, "
                vSql = vSql + "     apolice_id, "
                vSql = vSql + "     endosso_id, "
                vSql = vSql + "     exercicio, "
                vSql = vSql + "     sinistro, "
                vSql = vSql + "     cod_lider, "
                vSql = vSql + "     segurado, "
                vSql = vSql + "     estipulante, "
                vSql = vSql + "     perc_partic, "
                vSql = vSql + "     val_is, "
                vSql = vSql + "     val_estima, "
                vSql = vSql + "     dt_sinistro, "
                vSql = vSql + "     local_sinistro, "
                vSql = vSql + "     bens_sinistro, "
                vSql = vSql + "     nat_evento, "
                vSql = vSql + "     moeda, "
                vSql = vSql + "     tp_operacao, "
                vSql = vSql + "     item_sinistro, "
                vSql = vSql + "     cod_cobertura, "
                vSql = vSql + "     item_estima"
                vSql = vSql + ") "
                vSql = vSql + "SELECT "
                vSql = vSql + "     CONVERT(int, SUBSTRING(Linha, 1, 2)), "   ' ramo_id
                vSql = vSql + "     CONVERT(numeric, SUBSTRING(Linha, 3, 7)), "   ' apolice_id
                vSql = vSql + "     CONVERT(int, SUBSTRING(Linha, 10, 6)), "    ' endosso_id
                vSql = vSql + "     CONVERT(int, SUBSTRING(Linha, 16, 2)), "   ' exercicio
                vSql = vSql + "     CONVERT(numeric, SUBSTRING(Linha, 18, 5)), "   ' sinistro
                vSql = vSql + "     CONVERT(int, SUBSTRING(Linha, 23, 4)), "   ' cod_lider
                vSql = vSql + "     SUBSTRING(Linha, 27, 50), "  ' segurado
                vSql = vSql + "     SUBSTRING(Linha, 77, 50), "   ' estipulante
                vSql = vSql + "     CONVERT(numeric(4,2), SUBSTRING(Linha, 127, 2)+'.'+SUBSTRING(Linha, 129, 2)), "   ' perc_partic
                vSql = vSql + "     CONVERT(numeric(13,2), SUBSTRING(Linha, 131, 13)+'.'+SUBSTRING(Linha, 144, 2)), "   ' val_is
                vSql = vSql + "     CONVERT(numeric(13,2), SUBSTRING(Linha, 146, 13)+'.'+SUBSTRING(Linha, 159, 2)), "   ' val_estima
                vSql = vSql + "     SUBSTRING(Linha, 161, 6), " ' dt_sinistro
                vSql = vSql + "     SUBSTRING(Linha, 167, 50), "   ' local_sinistro
                vSql = vSql + "     SUBSTRING(Linha, 217, 50), "   ' bens_sinistro
                vSql = vSql + "     SUBSTRING(Linha, 267, 50), "   ' nat_evento
                vSql = vSql + "     SUBSTRING(Linha, 317, 2), "   ' moeda
                vSql = vSql + "     SUBSTRING(Linha, 319, 1), "   ' tp_operacao
                vSql = vSql + "     SUBSTRING(Linha, 320, 4), "   ' item_sinistro
                vSql = vSql + "     SUBSTRING(Linha, 324, 3), "   ' cod_cobertura
                vSql = vSql + "     SUBSTRING(Linha, 3327, 1) "   ' item_estima
                vSql = vSql + "FROM "
                vSql = vSql + "     ##tmp_arquivo "
                rdocn.Execute vSql, 64

                vSql = ""
                vSql = vSql + "SELECT "
                vSql = vSql + "     MaxDtSinistro = Max(dt_sinistro), "
                vSql = vSql + "     MinDtSinistro = Min(dt_sinistro) "
                vSql = vSql + "FROM "
                vSql = vSql + "     ##tmp_sinaviso "
                Set rc = rdocn.Execute(vSql)

                If rc.EOF = False Then
                    lblMaxData.Caption = Format(rc("MaxDtSinistro"), "dd/mm/yyyy")
                    lblMinData.Caption = Format(rc("MinDtSinistro"), "dd/mm/yyyy")
                End If
                rc.Close

                vSql = ""
                vSql = vSql + "SELECT "
                vSql = vSql + "     ramo_id, "
                vSql = vSql + "     tp_operacao, "
                vSql = vSql + "     moeda, "
                vSql = vSql + "     qtd = count(1), "
                vSql = vSql + "     val_is = sum(val_is), "
                vSql = vSql + "     val_estima = sum(val_estima) "
                vSql = vSql + "FROM "
                vSql = vSql + "     ##tmp_sinaviso "
                vSql = vSql + "GROUP BY "
                vSql = vSql + "     ramo_id, "
                vSql = vSql + "     tp_operacao, "
                vSql = vSql + "     moeda "
                vSql = vSql + "ORDER BY "
                vSql = vSql + "     ramo_id, "
                vSql = vSql + "     tp_operacao, "
                vSql = vSql + "     moeda "
                Set rc = rdocn.Execute(vSql)

                vRamo = -1
                vTpPagamento = -1
                vMoeda = -1

                sprDetalhes.Visible = False

                sprDetalhes.Rows = 1
                sprDetalhes.Cols = 6

                sprDetalhes.Row = 0
                sprDetalhes.Col = 0
                sprDetalhes.ColWidth(0) = 600
                sprDetalhes.Text = "Ramo"
                sprDetalhes.Col = 1
                sprDetalhes.ColWidth(1) = 1000
                sprDetalhes.Text = "Tipo Oper."
                sprDetalhes.Col = 2
                sprDetalhes.ColWidth(2) = 1000
                sprDetalhes.Text = "Moeda"
                sprDetalhes.Col = 3
                sprDetalhes.ColWidth(3) = 1500
                sprDetalhes.Text = "Qtd.Reg."
                sprDetalhes.Col = 4
                sprDetalhes.ColWidth(4) = 1500
                sprDetalhes.Text = "Valor IS"
                sprDetalhes.Col = 5
                sprDetalhes.ColWidth(5) = 1500
                sprDetalhes.Text = "Fator Estimativa"

                Do Until rc.EOF
                    sprDetalhes.Rows = sprDetalhes.Rows + 1
                    sprDetalhes.Row = sprDetalhes.Rows - 1

                    If vRamo <> rc("ramo_id") Then
                        sprDetalhes.Col = 0
                        sprDetalhes.Text = rc("ramo_id")
                        vRamo = rc("ramo_id")
                    End If

                    'If vTpPagamento <> rc("tp_operacao") Then
                    sprDetalhes.Col = 1
                    sprDetalhes.Text = rc("tp_operacao")
                    vTpPagamento = rc("tp_operacao")
                    'End If

                    'If vMoeda <> rc("moeda") Then
                    sprDetalhes.Col = 2
                    sprDetalhes.Text = rc("moeda")
                    vMoeda = rc("moeda")
                    'End If

                    sprDetalhes.Col = 3
                    sprDetalhes.Text = Format(CLng(rc("qtd")), "##,##0")
                    vQtdRamo = vQtdRamo + CLng(rc("qtd"))
                    sprDetalhes.Col = 4
                    sprDetalhes.Text = Format(CDbl(rc("val_is")), "##,##0.#0")
                    vValorPago = vValorPago + CDbl(rc("val_is"))
                    sprDetalhes.Col = 5
                    sprDetalhes.Text = Format(CDbl(rc("val_estima")), "##,##0.#0")
                    vValPagoSeg = vValPagoSeg + CDbl(rc("val_estima"))
                    rc.MoveNext
                Loop
                rc.Close

                sprDetalhes.Rows = sprDetalhes.Rows + 1
                sprDetalhes.Row = sprDetalhes.Rows - 1

                sprDetalhes.Col = 0
                sprDetalhes.CellForeColor = vbRed
                sprDetalhes.Text = "TOTAL"
                sprDetalhes.Col = 3
                sprDetalhes.CellForeColor = vbRed
                sprDetalhes.Text = Format(vQtdRamo, "##,##0")
                sprDetalhes.Col = 4
                sprDetalhes.CellForeColor = vbRed
                sprDetalhes.Text = Format(vValorPago, "##,##0.#0")
                sprDetalhes.Col = 5
                sprDetalhes.CellForeColor = vbRed
                sprDetalhes.Text = Format(vValPagoSeg, "##,##0.#0")

                vTabTmp = "##tmp_sinaviso"

                sprDetalhes.Visible = True
                pnlResumo.Visible = True
                '
            End If
            '
        Case "ECCAN504" ' Cancelamentos
            vNumLin = Int(LOF(1) / 47) + 10

            pnlProg.Min = 0
            pnlProg.Max = vNumLin
            pnlProg.Visible = True
            pnlProg.Value = 0

            vArqNome = Trim(sprArq.Text)
            vRemessa = Mid(vArqNome, InStr(1, vArqNome, ".") + 1, 4)

            vVlrPago = 0
            vIOF = 0
            vVlrJuros = 0
            vVlrRemunera = 0

            Do Until EOF(1)
                DoEvents
                If pnlProg.Value < pnlProg.Max Then
                    pnlProg.Value = pnlProg.Value + 1
                End If

                Line Input #1, vTemp
            
                vSql = ""
                vSql = vSql + "INSERT INTO "
                vSql = vSql + "     ##tmp_arquivo "
                vSql = vSql + "( Linha ) Values ( "
                vSql = vSql + " '" + vTemp + "' ) "
                rdocn.Execute vSql, 64
            Loop

            vSql = ""
            If vTabTmp <> "##tmp_eccan504" Then
                If vTabTmp <> "" Then
                    vSql = vSql + "DROP TABLE "
                    vSql = vSql + "     " + vTabTmp + " "
                    'vSql = vSql + " GO "
                End If
                vSql = vSql + "CREATE TABLE "
                vSql = vSql + "   ##tmp_eccan504 "
                vSql = vSql + "   ( "
                vSql = vSql + "     cod_lider       numeric, "
                vSql = vSql + "     ramo_id            int, "
                vSql = vSql + "     apolice_id      numeric, "
                vSql = vSql + "     endosso_id      int, "
                vSql = vSql + "     num_ordem       numeric, "
                vSql = vSql + "     parcela         int, "
                vSql = vSql + "     num_ordem_parc  numeric(13,0) "
                vSql = vSql + "   ) "
            Else
                vSql = vSql + "TRUNCATE TABLE "
                vSql = vSql + "   ##tmp_eccan504 "
            End If
            rdocn.Execute vSql, 64

            vSql = ""
            vSql = vSql + "INSERT INTO "
            vSql = vSql + "     ##tmp_eccan504 "
            vSql = vSql + "( "
            vSql = vSql + "     cod_lider, "
            vSql = vSql + "     ramo_id, "
            vSql = vSql + "     apolice_id, "
            vSql = vSql + "     endosso_id, "
            vSql = vSql + "     num_ordem, "
            vSql = vSql + "     parcela, "
            vSql = vSql + "     num_ordem_parc "
            vSql = vSql + ") "
            vSql = vSql + "SELECT "
            vSql = vSql + "     CONVERT(int, SUBSTRING(Linha, 1, 4)), "   ' cod_lider
            vSql = vSql + "     CONVERT(int, SUBSTRING(Linha, 5, 2)), "   ' ramo_id
            vSql = vSql + "     CONVERT(numeric, SUBSTRING(Linha, 7, 7)), "    ' apolice_id
            vSql = vSql + "     CONVERT(int, SUBSTRING(Linha, 14, 6)), "   ' endosso_id
            vSql = vSql + "     CONVERT(numeric(13,0), SUBSTRING(Linha, 20, 13)), "   ' num_ordem
            vSql = vSql + "     CONVERT(int, SUBSTRING(Linha, 33, 2)), "   ' parcela
            vSql = vSql + "     CONVERT(numeric(13,0), SUBSTRING(Linha, 35, 13)) "   ' num_ordem_parc
            vSql = vSql + "FROM "
            vSql = vSql + "     ##tmp_arquivo "
            rdocn.Execute vSql, 64

            vSql = ""
            vSql = vSql + "SELECT "
            vSql = vSql + "     ramo_id, "
            vSql = vSql + "     qtd_apolice = count(apolice_id), "
            vSql = vSql + "     qtd_parcela = sum(parcela) "
            vSql = vSql + "FROM "
            vSql = vSql + "     ##tmp_eccan504 "
            vSql = vSql + "GROUP BY "
            vSql = vSql + "     ramo_id "
            vSql = vSql + "ORDER BY "
            vSql = vSql + "     ramo_id "
            Set rc = rdocn.Execute(vSql)

            vRamo = -1

            sprDetalhes.Visible = False

            sprDetalhes.Rows = 1
            sprDetalhes.Cols = 3

            sprDetalhes.Row = 0
            sprDetalhes.Col = 0
            sprDetalhes.ColWidth(0) = 600
            sprDetalhes.Text = "Ramo"
            sprDetalhes.Col = 1
            sprDetalhes.ColWidth(1) = 1000
            sprDetalhes.Text = "Qtd.Apolices"
            sprDetalhes.Col = 2
            sprDetalhes.ColWidth(2) = 1000
            sprDetalhes.Text = "Qtd.Parcelas"

            Do Until rc.EOF
                sprDetalhes.Rows = sprDetalhes.Rows + 1
                sprDetalhes.Row = sprDetalhes.Rows - 1

                If vRamo <> rc("ramo_id") Then
                    sprDetalhes.Col = 0
                    sprDetalhes.Text = rc("ramo_id")
                    vRamo = rc("ramo_id")
                End If

                sprDetalhes.Col = 1
                sprDetalhes.Text = Format(CLng(rc("qtd_apolice")), "##,##0")
                vQtdRamo = vQtdRamo + CLng(rc("qtd_apolice"))
                sprDetalhes.Col = 2
                sprDetalhes.Text = Format(CLng(rc("qtd_parcela")), "##,##0")
                vQtdParcela = vQtdParcela + CLng(rc("qtd_parcela"))
                rc.MoveNext
            Loop
            rc.Close

            sprDetalhes.Rows = sprDetalhes.Rows + 1
            sprDetalhes.Row = sprDetalhes.Rows - 1

            sprDetalhes.Col = 0
            sprDetalhes.CellForeColor = vbRed
            sprDetalhes.Text = "TOTAL"
            sprDetalhes.Col = 1
            sprDetalhes.CellForeColor = vbRed
            sprDetalhes.Text = Format(vQtdRamo, "##,##0")
            sprDetalhes.Col = 2
            sprDetalhes.CellForeColor = vbRed
            sprDetalhes.Text = Format(vQtdParcela, "##,##0")

            vTabTmp = "##tmp_eccan504"

            sprDetalhes.Visible = True
            pnlResumo.Visible = True
            '
        Case "EC504" ' Emiss�o
            vNumLin = Int(LOF(1) / 2426) + 10

            pnlProg.Min = 0
            pnlProg.Max = vNumLin
            pnlProg.Visible = True
            pnlProg.Value = 0

            vArqNome = Trim(sprArq.Text)
            vRemessa = Mid(vArqNome, InStr(1, vArqNome, ".") + 1, 4)

            vVlrPago = 0
            vIOF = 0
            vVlrJuros = 0
            vVlrRemunera = 0

            Do Until EOF(1)
                DoEvents
                If pnlProg.Value < pnlProg.Max Then
                    pnlProg.Value = pnlProg.Value + 1
                End If

                Line Input #1, vTemp
                vSql = ""
                vSql = vSql + "INSERT INTO "
                vSql = vSql + "     ##tmp_arquivo "
                vSql = vSql + "( Linha ) Values ( "
                vSql = vSql + " '" + vTemp + "' ) "
                rdocn.Execute vSql, 64
            Loop

            vSql = ""
            If vTabTmp <> "##tmp_ec504" Then
            
                If vTabTmp <> "" Then
                    vSql = vSql + "DROP TABLE "
                    vSql = vSql + "     " + vTabTmp + " "
                    'vSql = vSql + " GO "
                End If
            
                vSql = vSql + "CREATE TABLE "
                vSql = vSql + "   ##tmp_ec504 "
                vSql = vSql + "   ( "
                vSql = vSql + "     subramo         int, "
                vSql = vSql + "     cod_lider       numeric, "
                vSql = vSql + "     nome_segurado   varchar(40), "
                vSql = vSql + "     cpf_cgc         varchar(14), "
                vSql = vSql + "     num_ordem       numeric, "
                vSql = vSql + "     cod_corretor_susep numeric, "
                vSql = vSql + "     perc_particip   numeric(4,2), "
                vSql = vSql + "     perc_comissao   numeric(4,2), "
                vSql = vSql + "     perc_desc_cong  numeric(4,2), "
                vSql = vSql + "     val_is_total    numeric(13,2), "
                vSql = vSql + "     tp_documento    int, "
                vSql = vSql + "     apolice         numeric, "
                vSql = vSql + "     endosso         int, "
                'vSql = vSql + "     dt_emissao      smalldatetime,"
                'vSql = vSql + "     dt_inicio_vigencia  smalldatetime,"
                'vSql = vSql + "     dt_fim_vigencia smalldatetime,"
                vSql = vSql + "     dt_emissao      smalldatetime, "
                vSql = vSql + "     dt_inicio_vigencia  smalldatetime, "
                vSql = vSql + "     dt_fim_vigencia varchar(10), "
                vSql = vSql + "     qtd_parcela     int, "
                vSql = vSql + "     moeda           int, "
                vSql = vSql + "     dt_contratacao  varchar(10), "
                vSql = vSql + "     cod_produto     int, "
                vSql = vSql + "     perc_desp_lider numeric(4,2) "
                vSql = vSql + "   ) "
            Else
                vSql = vSql + "TRUNCATE TABLE "
                vSql = vSql + "   ##tmp_ec504 "
            End If
            rdocn.Execute vSql, 64

            vSql = ""
            vSql = vSql + "INSERT INTO "
            vSql = vSql + "     ##tmp_ec504 "
            vSql = vSql + "( "
            vSql = vSql + "     subramo, "
            vSql = vSql + "     cod_lider, "
            vSql = vSql + "     nome_segurado, "
            vSql = vSql + "     cpf_cgc, "
            vSql = vSql + "     num_ordem, "
            vSql = vSql + "     cod_corretor_susep, "
            vSql = vSql + "     perc_particip, "
            vSql = vSql + "     perc_comissao, "
            vSql = vSql + "     perc_desc_cong, "
            vSql = vSql + "     val_is_total, "
            vSql = vSql + "     tp_documento, "
            vSql = vSql + "     apolice, "
            vSql = vSql + "     endosso, "
            vSql = vSql + "     dt_emissao, "
            vSql = vSql + "     dt_inicio_vigencia, "
            vSql = vSql + "     dt_fim_vigencia, "
            vSql = vSql + "     qtd_parcela, "
            vSql = vSql + "     moeda, "
            vSql = vSql + "     dt_contratacao "
            'vSql = vSql + "     cod_produto, "
            'vSql = vSql + "     perc_desp_lider "
            vSql = vSql + ") "
            vSql = vSql + "SELECT "
            vSql = vSql + "     CONVERT(int, SUBSTRING(Linha, 1, 4)), "   ' subramo
            vSql = vSql + "     CONVERT(int, SUBSTRING(Linha, 5, 3)), "   ' cod_lider
            vSql = vSql + "     SUBSTRING(Linha, 8, 40), "    ' nome_segurado
            vSql = vSql + "     SUBSTRING(Linha, 48, 14), "   ' cpf_cgc
            vSql = vSql + "     CONVERT(numeric(13,0), SUBSTRING(Linha, 62, 13)), "   ' num_ordem
            vSql = vSql + "     CONVERT(numeric, SUBSTRING(Linha, 75, 9)), "   ' cod_corretor_susep
            vSql = vSql + "     CONVERT(numeric(4,2), SUBSTRING(Linha, 84, 2)+'.'+SUBSTRING(Linha, 86, 2)), "   ' perc_particip
            vSql = vSql + "     CONVERT(numeric(4,2), SUBSTRING(Linha, 88, 2)+'.'+SUBSTRING(Linha, 90, 2)), "   ' perc_comissao
            vSql = vSql + "     CONVERT(numeric(4,2), SUBSTRING(Linha, 92, 2)+'.'+SUBSTRING(Linha, 94, 2)), "   ' perc_desc_cong
            vSql = vSql + "     CONVERT(numeric(13,2), SUBSTRING(Linha, 96, 13)+'.'+SUBSTRING(Linha, 109, 2)), "   ' val_is_total
            vSql = vSql + "     SUBSTRING(Linha, 111,2), " ' tp_documento
            vSql = vSql + "     SUBSTRING(Linha, 113, 7), "   ' apolice
            vSql = vSql + "     SUBSTRING(Linha, 120, 6), "   ' endosso
            vSql = vSql + "     SUBSTRING(Linha, 130, 4)+SUBSTRING(Linha, 128, 2)+SUBSTRING(Linha, 126, 2), "   ' dt_emissao
            vSql = vSql + "     SUBSTRING(Linha, 138, 4)+SUBSTRING(Linha, 136, 2)+SUBSTRING(Linha, 134, 2), "   ' dt_inicio_vigencia
            vSql = vSql + "     SUBSTRING(Linha, 146, 4)+SUBSTRING(Linha, 144, 2)+SUBSTRING(Linha, 142, 2), "   ' dt_fim_vigencia
            vSql = vSql + "     SUBSTRING(Linha, 150, 6), "   ' qtd_parcela
            vSql = vSql + "     CONVERT(int, SUBSTRING(Linha, 152, 2)), "   ' moeda
            vSql = vSql + "     SUBSTRING(Linha, 146, 4)+SUBSTRING(Linha, 144, 2)+SUBSTRING(Linha, 142, 2) "   ' dt_contratacao
            'vSql = vSql + "     CONVERT(int, SUBSTRING(Linha, 2426, 3)), "   ' cod_produto
            'vSql = vSql + "     CONVERT(numeric(4,2), SUBSTRING(Linha, 2429, 4)+'.'+SUBSTRING(Linha, 2433, 2)) "   '
            vSql = vSql + "FROM "
            vSql = vSql + "     ##tmp_arquivo "
            rdocn.Execute vSql, 64

            vSql = ""
            vSql = vSql + "SELECT "
            vSql = vSql + "     MaxDtEmissao = Max(dt_emissao), "
            vSql = vSql + "     MinDtEmissao = Min(dt_emissao) "
            vSql = vSql + "FROM "
            vSql = vSql + "     ##tmp_ec504 "
            Set rc = rdocn.Execute(vSql)

            If rc.EOF = False Then
                lblMaxData.Caption = Format(rc("MaxDtEmissao"), "dd/mm/yyyy")
                lblMinData.Caption = Format(rc("MinDtEmissao"), "dd/mm/yyyy")
            End If
            rc.Close
    
            vSql = ""
            vSql = vSql + "SELECT "
            vSql = vSql + "     subramo, "
            vSql = vSql + "     tp_documento, "
            vSql = vSql + "     qtd = count(1), "
            vSql = vSql + "     qtd_parc = sum(qtd_parcela), "
            vSql = vSql + "     val_is = sum(val_is_total) "
            vSql = vSql + "FROM "
            vSql = vSql + "     ##tmp_ec504 "
            vSql = vSql + "GROUP BY "
            vSql = vSql + "     subramo, "
            vSql = vSql + "     tp_documento "
            vSql = vSql + "ORDER BY "
            vSql = vSql + "     subramo, "
            vSql = vSql + "     tp_documento "
            Set rc = rdocn.Execute(vSql)

            vRamo = -1

            sprDetalhes.Visible = False

            sprDetalhes.Rows = 1
            sprDetalhes.Cols = 5

            sprDetalhes.Row = 0
            sprDetalhes.Col = 0
            sprDetalhes.ColWidth(0) = 800
            sprDetalhes.Text = "SubRamo"
            sprDetalhes.Col = 1
            sprDetalhes.ColWidth(1) = 800
            sprDetalhes.Text = "Tipo Doc"
            sprDetalhes.Col = 2
            sprDetalhes.ColWidth(2) = 1000
            sprDetalhes.Text = "Qtd.Doc."
            sprDetalhes.Col = 3
            sprDetalhes.ColWidth(3) = 1500
            sprDetalhes.Text = "Qtd.Parc."
            sprDetalhes.Col = 4
            sprDetalhes.ColWidth(4) = 2000
            sprDetalhes.Text = "IS Total"

            Do Until rc.EOF
                sprDetalhes.Rows = sprDetalhes.Rows + 1
                sprDetalhes.Row = sprDetalhes.Rows - 1

                If vRamo <> rc("subramo") Then
                    sprDetalhes.Col = 0
                    sprDetalhes.Text = rc("subramo")
                    vRamo = rc("subramo")
                End If

                If vTpDocumento <> rc("tp_documento") Then
                    sprDetalhes.Col = 1
                    sprDetalhes.Text = rc("tp_documento")
                    vTpDocumento = rc("tp_documento")
                End If
            
                sprDetalhes.Col = 2
                sprDetalhes.Text = Format(CLng(rc("qtd")), "##,##0")
                vQtdRamo = vQtdRamo + CLng(rc("qtd"))
            
                sprDetalhes.Col = 3
                sprDetalhes.Text = Format(CLng(rc("qtd_parc")), "##,##0")
                vQtdParcela = vQtdParcela + CLng(rc("qtd_parc"))
            
                sprDetalhes.Col = 4
                sprDetalhes.Text = Format(CDbl(rc("val_is")), "##,##0.#0")
                vValorPago = vValorPago + CDbl(rc("val_is"))
            
                rc.MoveNext
            Loop
            rc.Close

            sprDetalhes.Rows = sprDetalhes.Rows + 1
            sprDetalhes.Row = sprDetalhes.Rows - 1

            sprDetalhes.Col = 0
            sprDetalhes.CellForeColor = vbRed
            sprDetalhes.Text = "TOTAL"
            sprDetalhes.Col = 2
            sprDetalhes.CellForeColor = vbRed
            sprDetalhes.Text = Format(vQtdRamo, "##,##0")
            sprDetalhes.Col = 3
            sprDetalhes.CellForeColor = vbRed
            sprDetalhes.Text = Format(vQtdParcela, "##,##0")
            sprDetalhes.Col = 4
            sprDetalhes.CellForeColor = vbRed
            sprDetalhes.Text = Format(vValorPago, "##,##0.#0")

            vTabTmp = "##tmp_ec504"

            sprDetalhes.Visible = True
            pnlResumo.Visible = True
            '

    End Select

    'lblRemessa.Caption = vRemessa
    lblRemessa.Caption = vArqNome
    
    Close #1
    
    vSql = ""
    vSql = vSql + "DROP TABLE "
    vSql = vSql + "     ##tmp_arquivo "
    rdocn.Execute vSql, 64
    
    StatusBar1.SimpleText = ""
    pnlProg.Visible = False
    Screen.MousePointer = 0
    Exit Sub
    
    Resume
    
TrataErro:
    MsgBox "Erro: " + Str(Err) + ": " + Err.Description
    
    'Call TrataErroGeral("sprArq_click")
    Close #1
    
    'vSql = ""
    'vSql = vSql + "IF EXISTS ( SELECT * FROM sysobjects "
    'vSql = vSql + "            WHERE name = '##tmp_arquivo') "
    'vSql = vSql + "  DROP TABLE "
    'vSql = vSql + "      ##tmp_arquivo "
    'rdocn.Execute vSql, 64
    rdocn.Close
    
    vTabTmp = ""
    
    Call Conexao
    
    StatusBar1.SimpleText = ""
    pnlProg.Visible = False
    Screen.MousePointer = 0

End Sub

Function gFunRedimGrid(vPanel As Object, vGrid As Object)
    Dim vColW As Double
    Dim vCol  As Integer
    Dim vPerc As Double
    
    vColW = (vPanel.Width - 450) / vGrid.Cols
    
    For vCol = 0 To vGrid.Cols - 1
        vGrid.Col = vCol
        vGrid.ColWidth(vCol) = vColW
    Next
    
    'vColW = 0
    'For vCol = 0 To vGrid.Cols - 1
    '    vGrid.Col = vCol
    '    vColW = vColW + vGrid.ColWidth(vCol)
    'Next
    '
    'vPerc = (vPanel.Width - 250) / vColW
    'vGrid.Width = vPanel.Width - 100
    '
    'For vCol = 0 To vGrid.Cols - 1
    '    vGrid.Col = vCol
    '    vGrid.ColWidth(vCol) = vPerc * vGrid.ColWidth(vCol)
    'Next
    
End Function

