VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.2#0"; "MSCOMCTL.OCX"
Object = "{20C62CAE-15DA-101B-B9A8-444553540000}#1.1#0"; "MSMAPI32.OCX"
Begin VB.Form FrmImportacaoAvaliacaoRetornoBB 
   Caption         =   "SEG1449 - Importa��o Avaliacao Retorno BB - "
   ClientHeight    =   6060
   ClientLeft      =   3390
   ClientTop       =   2400
   ClientWidth     =   7830
   ForeColor       =   &H80000016&
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   6060
   ScaleWidth      =   7830
   Begin MSMAPI.MAPIMessages MAPIMess 
      Left            =   840
      Top             =   6120
      _ExtentX        =   1005
      _ExtentY        =   1005
      _Version        =   393216
      AddressEditFieldCount=   1
      AddressModifiable=   0   'False
      AddressResolveUI=   0   'False
      FetchSorted     =   0   'False
      FetchUnreadOnly =   0   'False
   End
   Begin MSMAPI.MAPISession MAPISess 
      Left            =   210
      Top             =   6150
      _ExtentX        =   1005
      _ExtentY        =   1005
      _Version        =   393216
      DownloadMail    =   -1  'True
      LogonUI         =   -1  'True
      NewSession      =   0   'False
   End
   Begin MSComctlLib.ProgressBar term1 
      Height          =   255
      Left            =   120
      TabIndex        =   17
      Top             =   5730
      Visible         =   0   'False
      Width           =   5715
      _ExtentX        =   10081
      _ExtentY        =   450
      _Version        =   393216
      Appearance      =   0
   End
   Begin VB.CommandButton BtnOk 
      Caption         =   "&Ok"
      Height          =   315
      Left            =   5880
      TabIndex        =   4
      Top             =   5700
      Width           =   975
   End
   Begin VB.CommandButton btnCancelar 
      Caption         =   "&Cancelar"
      Height          =   315
      Left            =   6840
      TabIndex        =   3
      Top             =   5700
      Width           =   975
   End
   Begin TabDlg.SSTab SSTab1 
      Height          =   5535
      Left            =   90
      TabIndex        =   0
      Top             =   120
      Width           =   7755
      _ExtentX        =   13679
      _ExtentY        =   9763
      _Version        =   393216
      Style           =   1
      Tabs            =   2
      TabsPerRow      =   2
      TabHeight       =   520
      TabCaption(0)   =   "Importa��o"
      TabPicture(0)   =   "ImportacaoAvaliacaoRetornoBB.frx":0000
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "Frame2"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "Frame3"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "Frame1"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).ControlCount=   3
      TabCaption(1)   =   "Hist�rico"
      TabPicture(1)   =   "ImportacaoAvaliacaoRetornoBB.frx":001C
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "grid1"
      Tab(1).Control(1)=   "Command4"
      Tab(1).Control(2)=   "BtnAtualizar"
      Tab(1).ControlCount=   3
      Begin VB.Frame Frame1 
         Caption         =   "Inconsistencias da Importa��o"
         Height          =   1335
         Left            =   120
         TabIndex        =   20
         Top             =   4080
         Width           =   7455
         Begin VB.ListBox ListInconsistencias 
            Height          =   840
            Left            =   150
            TabIndex        =   21
            Top             =   270
            Width           =   7125
         End
      End
      Begin VB.CommandButton BtnAtualizar 
         Caption         =   "Buscar"
         Height          =   315
         Left            =   -69540
         TabIndex        =   5
         Top             =   3390
         Width           =   2025
      End
      Begin VB.CommandButton Command4 
         Caption         =   "&Buscar"
         Height          =   315
         Left            =   -69060
         TabIndex        =   6
         Top             =   5460
         Width           =   975
      End
      Begin VB.Frame Frame3 
         Caption         =   "Situa��o do Arquivo: "
         Height          =   1245
         Left            =   120
         TabIndex        =   9
         Top             =   2760
         Width           =   7455
         Begin VB.Label lblstatus 
            AutoSize        =   -1  'True
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Left            =   840
            TabIndex        =   15
            Top             =   900
            Width           =   1875
         End
         Begin VB.Label lblver 
            AutoSize        =   -1  'True
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Left            =   840
            TabIndex        =   14
            Top             =   600
            Width           =   75
         End
         Begin VB.Label lblarq 
            AutoSize        =   -1  'True
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Left            =   840
            TabIndex        =   13
            Top             =   300
            Width           =   75
         End
         Begin VB.Label Label3 
            AutoSize        =   -1  'True
            Caption         =   "Status:"
            Height          =   195
            Left            =   210
            TabIndex        =   12
            Top             =   900
            Width           =   495
         End
         Begin VB.Label Label2 
            AutoSize        =   -1  'True
            Caption         =   "Vers�o:"
            Height          =   195
            Left            =   165
            TabIndex        =   11
            Top             =   600
            Width           =   540
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "Arquivo:"
            Height          =   195
            Left            =   120
            TabIndex        =   10
            Top             =   300
            Width           =   585
         End
      End
      Begin VB.Frame Frame2 
         Caption         =   "Arquivo:"
         Height          =   2325
         Left            =   120
         TabIndex        =   8
         Top             =   360
         Width           =   7470
         Begin VB.TextBox fldpath 
            BackColor       =   &H8000000F&
            Height          =   330
            Left            =   150
            Locked          =   -1  'True
            TabIndex        =   1
            Top             =   450
            Width           =   7050
         End
         Begin VB.FileListBox lstarquivo 
            Height          =   870
            Left            =   120
            Pattern         =   "movvn*"
            TabIndex        =   2
            Top             =   1170
            Width           =   7095
         End
         Begin VB.Label Label4 
            Caption         =   "Arquivos"
            Height          =   195
            Index           =   1
            Left            =   150
            TabIndex        =   19
            Top             =   900
            Width           =   1320
         End
         Begin VB.Label Label4 
            Caption         =   "Path"
            Height          =   195
            Index           =   0
            Left            =   180
            TabIndex        =   18
            Top             =   225
            Width           =   1320
         End
      End
      Begin MSFlexGridLib.MSFlexGrid grid1 
         Height          =   2865
         Left            =   -74820
         TabIndex        =   16
         Top             =   450
         Width           =   7305
         _ExtentX        =   12885
         _ExtentY        =   5054
         _Version        =   393216
         Cols            =   10
         FixedCols       =   0
         FocusRect       =   0
         SelectionMode   =   1
         AllowUserResizing=   1
      End
   End
   Begin MSComctlLib.StatusBar StatusBar1 
      Align           =   2  'Align Bottom
      Height          =   315
      Left            =   0
      TabIndex        =   7
      Top             =   5745
      Width           =   7830
      _ExtentX        =   13811
      _ExtentY        =   556
      Style           =   1
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "FrmImportacaoAvaliacaoRetornoBB"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Dim rdocn1 As New rdoConnection
Dim hor As String, dat As String
Dim qtde As String, obs As String
Dim flag_importacao As Boolean
Dim Flag_Erro_Trailler As Boolean
Dim versao_atual As Long, ultima_versao As Long
Dim arqpath As String
Dim copypath As String
Dim logpath As String
Dim arqinput As String
Dim NomeArqRet As String
Dim LogErro As Boolean
'-----
Public LogArqIncon As String
Public LogArqDesat As String
Public LogArqProcess As String
Public vErro As String

Public StrArquivo As String
Public StrArquivoVrs As String
Public StrProcessado As String
Public StrPasta As String


Public Function TiraCaracterErro(ByVal Cadeia As String) As String
  Const CadeiaComAcento = "'"
  Const CadeiaSemAcento = " "
  Dim IndiceCadeia As Integer
  Dim IndiceAcento As Integer
  For IndiceCadeia = 1 To Len(Cadeia)
    IndiceAcento = InStr(CadeiaComAcento, Mid(Cadeia, IndiceCadeia, 1))
    If Logico(IndiceAcento) Then
      Mid(Cadeia, IndiceCadeia) = Mid(CadeiaSemAcento, IndiceAcento, 1)
    End If
  Next IndiceCadeia
  TiraCaracterErro = Cadeia
End Function


Public Function Obtem_Pastas(ByRef vPasta As Integer) As String

  Dim rs As Object
  Dim sSQL As String
  ' - Pastas de Destino:
  ' - --------------------------------------------------------------------
  ' - 1 - Pasta Pendentes   (Inconsist�ncias ocorrida no processamento); -
  ' - 2 - Pasta Recebidos   (Retorno BB recebido);                       -
  ' - 3 - Pasta Processados (Retorno BB processado com sucesso).         -
  ' - --------------------------------------------------------------------
 
  Select Case vPasta
         Case 1
              sSQL = "SELECT TOP 1 remessa_retorno_bb_tb.caminho_remessa_inconsistente FROM seguros_db.dbo.remessa_retorno_bb_tb remessa_retorno_bb_tb WITH (NOLOCK)  WHERE ativo in('s') "
         Case 2
              sSQL = "SELECT TOP 1 remessa_retorno_bb_tb.caminho_remessa_recebida      FROM seguros_db.dbo.remessa_retorno_bb_tb remessa_retorno_bb_tb WITH (NOLOCK)  WHERE ativo in('s') "
         Case 3
              sSQL = "SELECT TOP 1 remessa_retorno_bb_tb.caminho_remessa_processada    FROM seguros_db.dbo.remessa_retorno_bb_tb remessa_retorno_bb_tb WITH (NOLOCK)  WHERE ativo in('s') "
  End Select

  Set rs = rdocn1.OpenResultset(sSQL)

  If Not rs.EOF Then
     Obtem_Pastas = rs(0)
  End If

  rs.Close

End Function
Sub Importa_Avaliacoes_Retorno_BB(ByVal vArquivo As String)

On Error GoTo TrataErro


'Dim Arquivo As String
Dim InfImportacao As Integer
Dim SQL As String
Dim Rst As rdoResultset
Dim Rst_Aux As rdoResultset

    lblstatus.Caption = "Processando"
    
    cUserName = "Nova"
 Dim caminho As String
 caminho = "\\10.0.35.104\teste_mls\Arquivos Avaliacao Retorno BB\Recebidos"

   'Procedure de consulta e valida��o da remessa
   SQL = "EXEC desenv_db.dbo.SEGS13515_SPS '" & vArquivo & "'"
   SQL = SQL & ",'" & cUserName & "'"

   Set Rst = rdocn.OpenResultset(SQL)

   If Not Rst.EOF Then
      'Se a remessa for v�lida para importa��o
      '(
      '  1 - Remessa BB v�lida para processamento;
      '  2 - Remessa BB com inconsist�ncia
      ').
      'flag de valida��o para processamento
      InfImportacao = Rst(0)
   
      If InfImportacao = 1 Then
         'Rst(1) - Nome da procedure
         'Rst(2) - Caminho para processamento
         'Rst(3) - Remessa BB
         

      
         'Verifica execu��o AB ou ABS
         SQL = ""
         SQL = "EXEC desenv_db.dbo.SEGS13690_SPS '" & vArquivo & "','" & caminho & "' "
         
         Set Rst_Aux = rdocn.OpenResultset(SQL)
      
         SQL = ""
         SQL = "Exec " & Rst(1) & " '" & caminho & "' , '" & Rst(3) & "' ,'" & cUserName & "'"
      
         If Rst_Aux(0) = "AB" Then
             Set Rst = rdocn.OpenResultset(SQL)
         Else
             Set Rst = rdocn_Seg2.OpenResultset(SQL)
         End If
                   
         lblstatus.Caption = "Movendo arquivo."
        
         If Rst(0) = 1 Then
            lblstatus.Caption = "Movendo arquivo para processados."
            StrProcessado = Rst(0)
            LogArqProcess = LogArqProcess & vbNewLine & NomeArqRet & " - Ok."
         Else
            lblstatus.Caption = "Movendo arquivo para pendentes."
            StrProcessado = Rst(0)
            
            If Not (Rst(0) = 2 And Rst(6) = 9) Then
               LogArqProcess = LogArqProcess & vbNewLine & NomeArqRet & " - com inconsist�ncia em processamento."
            Else
               LogArqProcess = LogArqProcess & vbNewLine & NomeArqRet & " - Remessa integralmente recusada. Favor validar com urg�ncia."
            End If
            
         End If
             
      Else
            lblstatus.Caption = "Movendo arquivo para pendentes."

            If Rst(0) = 2 Then
                StrProcessado = Rst(0)
                
                LogArqProcess = LogArqProcess & vbNewLine & NomeArqRet & " - com inconsist�ncia em processamento."
            
             Else
                StrProcessado = Rst(0)
                LogArqProcess = LogArqProcess & vbNewLine & NomeArqRet & " - Remessa desativada."
           End If
      End If

      'Move_Arquivo Rst(3) + "\" + vArquivo, Rst(4) + "\" + vArquivo
   
   Else
      lblstatus.Caption = "Movendo arquivo para pendentes."
      
      
      If Mid(NomeArqRet, 1, 7) <> "ALSF159" Then
         LogArqProcess = LogArqProcess & vbNewLine & UCase(NomeArqRet) & " - n�o identificado para processamento."
      Else
         LogArqProcess = LogArqProcess & vbNewLine & UCase(NomeArqRet) & " - Retorno do Agendamento do CDC Ex�rcito. Comunicar o financeiro."
      End If
      
      Move_Arquivo Obtem_Pastas(2) + "\" + vArquivo, Obtem_Pastas(1) + "\" = vArquivo
   End If

   Flag_Erro_Trailler = False

   Set Rst = Nothing

   lblstatus.Caption = "Conclu�do"
   
   BtnOk.Enabled = False

   Exit Sub

TrataErro:
    StrProcessado = 3
    vErro = Err.Description
    Envia_Email_Responsavel

    Flag_Erro_Trailler = True
    TrataErroGeral "Importa_Avaliacoes_Retorno_SEG987E_BB " & SQL, Me.name
    Call TerminaSEGBR
End Sub


Sub Insere_Acompanhamento_987_AB()

Dim SQL As String
Dim Rst As rdoResultset

On Error GoTo TrataErro

    lblstatus.Caption = "Carregando acompanhamento_987_tb AB"
    
    rdocn.BeginTrans
    
    SQL = "Exec SEGS9157_SPI " & vbNewLine
    SQL = SQL & "'" & cUserName & "'"

    Set Rst = rdocn.OpenResultset(SQL)

    rdocn.CommitTrans
    
    Set Rst = Nothing
    
    lblstatus.Caption = "AB Conclu�do."
    BtnOk.Enabled = False
    
    Exit Sub

TrataErro:
    rdocn.RollbackTrans
        Call GravaLogCTM("Erro - ", Time & " - " & Err.Number & " - " & Err.Description, i, "")
        Resume Next
    ''TrataErroGeral "Insere_Acompanhamento_987 AB " & SQL, Me.name
    ''Call TerminaSEGBR

End Sub

Sub Insere_Acompanhamento_987_ABS()

Dim SQL As String
Dim Rst As rdoResultset

On Error GoTo TrataErro

    lblstatus.Caption = "Carregando acompanhamento_987_tb ABS"
    
    rdocn_Seg2.BeginTrans
    
    SQL = "Exec SEGS9157_SPI " & vbNewLine
    SQL = SQL & "'" & cUserName & "'"

    Set Rst = rdocn_Seg2.OpenResultset(SQL)

    rdocn_Seg2.CommitTrans
    
    Set Rst = Nothing
    
    lblstatus.Caption = "ABS Conclu�do."
    BtnOk.Enabled = False
    
    Exit Sub

TrataErro:
    rdocn_Seg2.RollbackTrans
        Call GravaLogCTM("Erro - ", Time & " - " & Err.Number & " - " & Err.Description, i, "")
        Resume Next
    ''TrataErroGeral "Insere_Acompanhamento_987 ABS " & SQL, Me.name
    ''Call TerminaSEGBR

End Sub


Private Sub Form_Load()
   
   On Error GoTo TrataErro
   'Verifica se foi executado pelo Control-M
    CTM = Verifica_Origem_ControlM(Command)
   
   
   'Conectando com a base de dados''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
   Conexao
   
   'Necessario quando for executado pelo Control-M
   If CTM = True Then
    gsCPF = "12345678909"
    glEmpresa_id = "31"
   End If

   Call Seguranca("SEGP1449", "Importa��o de Avalia��es de Retorno BB")
   
   Call Conexao_Auxiliar
   
   'Obtendo Paths'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
   'logpath = LerArquivoIni("producao", "log_path")
   arqpath = LerArquivoIni("producao", "producao_path")
   arqpath = arqpath & "..\Arquivos Avaliacao Retorno BB\Recebidos"
   arqpath = "\\10.0.35.104\teste_mls\Arquivos Avaliacao Retorno BB\Recebidos"
   'Inicializando Vari�veis'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
   flag_importacao = False
   obs = Space(50)
   
   'Atualizando interface'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
   fldpath.Text = arqpath
   lstarquivo.PATH = arqpath
   lblstatus.Caption = "N�o processado"
   
   'Limitador de leitura de arquivos''''''''''''''''''''''''''''''''''''''''''''''''''''''''
   lstarquivo.Pattern = "*.*" '"ALS987A.*;ALS987B.*;BBC987A.*;BBC987B.*;BBC987C.*;SEG987AB.*;SEG987AC.*;SEG987E.*;SEG987F.*;SEG987I.*;SEG987K.*;SEG987O.*;SEG987P.*;SEG987Q.*;SEG987T.*;SEGE987.*;SEGF987.*;SEGG987.*;SEG987N.*;SEG987H.*;SEGC987.*;SEGB987.*;SEGD987.*;SEG987A.*;SEG987R.*;SEG987S.*" ';;SEG987SS.*;SEG987SSS.*;SEG987X.*;SEG987Z.*"
   
   lstarquivo.Refresh
      
   Me.Caption = "SEGP1449 - Importa Avaliacao Retorno BB - " & Ambiente
   
   'Inciando processo autom�ticamente caso seja uma chamada do scheduler''''''''''''''''''''''
   If Obtem_agenda_diaria_id(Command) > 0 Or Verifica_Origem_ControlM(Command) = True Then
   
      If lstarquivo.ListCount = 0 Then
         Call GravaLogCTM("OK - ", "N�O EXISTE(M) ARQUIVO(S) PARA SER PROCESSADO(S)", lstarquivo.List(i), "")
                
         '' LGOMES - 14-06-2019 - Garantindo execu��o da apura��o do 987 independente da captura.
         Call GravaLogCTM("OK - ", Time & " - Atualizando Acompanhamento 987 AB", i, "")
         Insere_Acompanhamento_987_AB
         Call GravaLogCTM("OK - ", Time & " - Atualizando Acompanhamento 987 ABS", i, "")
         Insere_Acompanhamento_987_ABS
         Call GravaLogCTM("OK - ", Time & " - Finalizando Acompanhamento 987 AB e ABS", i, "")
                
         StrProcessado = 1
         Envia_Email_Responsavel
               
         Unload Me
         TerminaSEGBR
                
      End If
   
      Me.Show
      Call BtnOk_Click
      
      Unload Me
      Call TerminaSEGBR
   End If
   
   Exit Sub
   
TrataErro:

    StrProcessado = 3
    vErro = Err.Description
    Envia_Email_Responsavel
    TrataErroGeral "Form_Load", Me.name
    TerminaSEGBR
End Sub

Sub Conexao_Auxiliar()

On Error GoTo Erro

With rdocn1
    .Connect = rdocn.Connect
    .CursorDriver = rdUseServer
    .QueryTimeout = 30000 '' 50 minutos
    .EstablishConnection rdDriverNoPrompt
End With
    
Exit Sub

Erro:
    Call MensagemBatch("Conex�o com BRCAPDB indispon�vel.")
    TerminaSEGBR
    
End Sub

Private Sub BtnOk_Click()

On Error GoTo TrataErro

'Incializando parametros de execu��o batch''''''''''''''''''''''''''''''''''''''''''''

'InicializaParametrosExecucaoBatch Me
    
'Caso seja uma chamada do scheduler, ser�o importados todos os arquivos do produto
'agendado'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

MousePointer = 11
StrProcessado = 1


'CTM = True 'Comentar essa linha - flavio.abreu - 20171114
If goProducao.Automatico = True Or CTM = True Then

    For i = 0 To lstarquivo.ListCount - 1
        goProducao.AdicionaLog 1, lstarquivo.List(i), i + 1
        lstarquivo.ListIndex = i
        Inicia
    Next
    
    If i >= 0 Then
            '' LGOMES - 04-02-2019 - Substituindo rotina �nica de apura��o do 987 por duas independentes.
            '' Insere_Acompanhamento_987
        Call GravaLogCTM("OK - ", Time & " - Atualizando Acompanhamento 987 AB", i, "")
        Insere_Acompanhamento_987_AB
        Call GravaLogCTM("OK - ", Time & " - Atualizando Acompanhamento 987 ABS", i, "")
        Insere_Acompanhamento_987_ABS
        Call GravaLogCTM("OK - ", Time & " - Finalizando Acompanhamento 987 AB e ABS", i, "")
    End If

Else
    goProducao.AdicionaLog 1, lstarquivo.List(lstarquivo.ListIndex), 1
    
        If CTM = True Then
            Call GravaLogCTM("OK - ", "", "", lstarquivo.List(lstarquivo.ListIndex))
        End If
    
    Inicia
End If

'------ enviando email ao respons�veis ''''''''''''''
'Envia_Email_Responsavel


'Finalizando agenda''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

'Pula de for executado pelo Control-M
If CTM = False Then
   ' Call goProducao.finaliza
End If
     
   ' Unload Me
    'Call TerminaSEGBR

MousePointer = 0

Exit Sub

TrataErro:
    StrProcessado = 3
    vErro = Err.Description
    Envia_Email_Responsavel
    TrataErroGeral "BtnOK_Click", Me.name
    Call TerminaSEGBR
    
End Sub

Private Sub btnCancelar_Click()
    TerminaSEGBR
End Sub

Sub Inicia()
   
   Dim FileName As String
   
   On Error GoTo TrataErro
   
   obs = Space(50)
   
   'Verificando se algum arquivo foi selecionado''''''''''''''''''''''''''''''''''''''''''
   
   If lstarquivo.ListIndex = -1 Then
      MensagemBatch "Selecione o arquivo de retorno", , , False
      If goProducao.Automatico Or CTM = True Then
         TerminaSEGBR
      Else
         Exit Sub
      End If
   End If
      
   dat = Date
   hor = Time
   
   'Atualizando interface'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    
   BtnOk.Enabled = False
   lstarquivo.Enabled = False

   'Processando informa��es''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'  +----------------------+-----------------------------+----------------------------------------------------------------------+
'  | Cod.Produto          | Nome do Produto             | Nome do Arquivo                                                      |
'  +----------------------+-----------------------------+----------------------------------------------------------------------+
'  | SEG987E 678102       | OURO VIDA GARANTIA          | SEG551                                                               |
'  +----------------------+-----------------------------+----------------------------------------------------------------------+
    'Select Case Trim$(Left$(lstarquivo.FileName, ((InStr(1, lstarquivo.FileName, ".")) - 1))) ' Nome do arquivo sem o ponto "."
    '   Case "SEG987E"
                'Importa_Avaliacoes_Retorno_SEG987E_BB
    'End Select
    Arquivo = lstarquivo.FileName
    Importa_Avaliacoes_Retorno_BB (Arquivo)
    
   
   'Atualizando interface'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
   
   StatusBar1.SimpleText = "Tarefa de importa��o finalizada!"
   StatusBar1.Refresh
   
   Exit Sub

TrataErro:
    TrataErroGeral "Inicia", Me.name
    Call TerminaSEGBR
    
End Sub


Private Sub lstarquivo_Click()

On Error GoTo Trata_Erro
   Dim iTamArq As Integer
   lblarq.Caption = ""
   lblver.Caption = ""
   lblstatus.Caption = ""
   
   MousePointer = 11
   StatusBar1.SimpleText = "Verificando vers�o e status do arquivo"
   
   Select Case Len(lstarquivo.List(lstarquivo.ListIndex))
          Case 12
               iTamArq = 1
          Case 13
               iTamArq = 2
          Case 14
               iTamArq = 3
          Case 15
               iTamArq = 4
          Case Else
               iTamArq = 1
   End Select
          
   NomeArqRet = Mid(lstarquivo.List(lstarquivo.ListIndex) & String(10, " "), 1, (16 - iTamArq))
   StatusBar1.SimpleText = ""
   MousePointer = 0
   
   Exit Sub
   
Trata_Erro:
    TrataErroGeral "lstarquivo_Click", Me.name
    TerminaSEGBR

End Sub


'Mover o arquivo para outro diret�rio ap�s sua leitura
Function Move_Arquivo(Source As String, Destination As String) As Boolean

    On Error GoTo Err_Move_Arquivo
    
    'Copiar arquivo para origem
    FileCopy Source, Destination
    
    'Fechar o arquivo original
    'Close arqinput
    
    'Apagar o arquivo na origem
    Kill Source
    
    'Fim
    Move_Arquivo = True
    Exit Function
    
Err_Move_Arquivo:

    Move_Arquivo = False

End Function

Sub Envia_Email_Responsavel()

On Error GoTo TrataErro

Dim vAssunto As String

StatusBar1.SimpleText = "Enviando e-mail... "

'' --- Destinat�rio
vDestinatario = "operacao@bbmapfre.com.br;operacao@confitec.com.br;ger_producao@bbmapfre.com.br;producao@brasilseg.com.br;BeSouza@brasilseg.com.br;sustentacao_equipe987@confitec.com.br;"

''--- Assunto ------
vAssunto = "Retorno BB (987) - Log de Processamento - " & Data_Sistema
   
Select Case StrProcessado
    
    
    Case 0 ''  Para remessa desativada
        
        vMensagem = "A Aplica��o foi executada com sucesso. " _
        & vbNewLine _
        & vbNewLine _
        & "Remessa(s) Desativada(s):" _
        & vbNewLine _
        & LogArqDesat _
        & vbNewLine _
        & vbNewLine _
        & "Mensagem autom�tica da aplica��o SEGBR - SEGP1449 (SEGX2118), n�o responder." & vbNewLine
        
        If CTM = True Then
            Call GravaLogCTM("OK - ", vMensagem, "Remessa(s) Desativada(s):", LogArqIncon)
        End If
        

      Case 1 ''' remessa com sucesso
        If LogArqProcess <> "" Then
           vMensagem = "A aplica��o foi executada com sucesso. " _
         & vbNewLine _
         & vbNewLine _
         & "Status de processamento do(s) Retorno(s):" _
         & vbNewLine _
         & LogArqProcess _
         & vbNewLine _
         & vbNewLine _
         & "Mensagem autom�tica da aplica��o SEGBR - SEGP1449 (SEGX2118), n�o responder." & vbNewLine
       Else
           vMensagem = "A aplica��o foi executada com sucesso. " _
         & vbNewLine _
         & vbNewLine _
         & "N�o h� arquivo(s) para processamento." _
         & vbNewLine _
         & LogArqProcess _
         & vbNewLine _
         & vbNewLine _
         & "Mensagem autom�tica da aplica��o SEGBR - SEGP1449 (SEGX2118), n�o responder." & vbNewLine
       End If
        If CTM = True Then
            Call GravaLogCTM("OK - ", vMensagem, "", "")
        End If
        
      Case 2 ''' Remessas com inconsistencia
        
        vMensagem = "A aplica��o foi executada com sucesso, por�m ocorreu inconsist�ncia na carga de uma ou mais remessas." _
        & vbNewLine _
        & vbNewLine _
        & "Acionar a Equipe de Sustenta��o. " _
        & vbNewLine _
        & "Status de processamento do(s) Retorno(s):" _
        & vbNewLine _
        & LogArqProcess _
        & vbNewLine _
        & vbNewLine _
        & "Mensagem autom�tica da aplica��o SEGBR - SEGP1449 (SEGX2118), n�o responder." & vbNewLine
    
        If CTM = True Then
            Call GravaLogCTM("OK - ", vMensagem, "", "")
        End If
        
    Case 3 '''' erro na aplica��o
                
        vMensagem = "A Aplica��o n�o foi processada. " & vbNewLine _
        & " Acionar a Equipe de Sustenta��o. " & vbNewLine _
        & vbNewLine _
        & "ERRO - " & TiraCaracterErro(vErro) & " . " _
        & vbNewLine _
        & vbNewLine _
        & "Mensagem autom�tica da aplica��o SEGBR - SEGP1449 (SEGX2118), n�o responder." & vbNewLine
    
        If CTM = True Then
            Call GravaLogCTM("NOT OK - ", vMensagem, "", "Acionar a Equipe de Sustenta��o.")
        End If
        
    End Select

    Dim SQI    As String

        SQI = " EXEC SEGUROS_DB.DBO.envia_email_sp @email = '" & vDestinatario & "', @assunto = '" & vAssunto & "',@mensagem = '" & vMensagem & "'"
        rdocn1.Execute (SQI)

Exit Sub

TrataErro:
    TrataErroGeral "Erro no Envio de Email", Me.name
    Call GravaLogCTM("NOT OK - ", vMensagem, "", "N�o houve envio de email devido ao ERRO - ." & Err.Description)
    TerminaSEGBR

End Sub
