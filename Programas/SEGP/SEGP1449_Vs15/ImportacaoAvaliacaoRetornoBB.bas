Attribute VB_Name = "ModImportacaoAvaliacaoRetornoBB"
   Type logfile
      arq As String * 20
      ver As String * 5
      status As String * 20
      username As String * 20
      data As String * 10
      hora As String * 8
      dataf As String * 10
      horaf As String * 8
      qtde As String * 5
      obs As String * 50
      filler As String * 2 'CR e LF
   End Type

Sub Main()
   
   Conexao
   
   Call Seguranca("FRMIMPORTACAOAVALIACAORETORNOBB", "Importação de Avaliações de Retorno BB")
  
   FrmImportacaoAvaliacaoRetornoBB.Show

End Sub
