VERSION 5.00
Object = "{1CB70356-FEA2-11D4-87FA-00805F396245}#1.0#0"; "mask2s.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.2#0"; "MSCOMCTL.OCX"
Object = "{C932BA88-4374-101B-A56C-00AA003668DC}#1.1#0"; "MSMASK32.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form frmEndosso 
   BackColor       =   &H00000000&
   BorderStyle     =   5  'Sizable ToolWindow
   ClientHeight    =   9345
   ClientLeft      =   1215
   ClientTop       =   450
   ClientWidth     =   12540
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   9345
   ScaleWidth      =   12540
   Begin MSComctlLib.StatusBar StbRodape 
      Align           =   2  'Align Bottom
      Height          =   315
      Left            =   0
      TabIndex        =   0
      Top             =   9030
      Width           =   12540
      _ExtentX        =   22119
      _ExtentY        =   556
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   3
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   8997
            MinWidth        =   8997
            Text            =   "Mensagem"
            TextSave        =   "Mensagem"
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   6
            Alignment       =   1
            AutoSize        =   2
            Object.Width           =   1535
            MinWidth        =   88
            Text            =   "Data do sistema"
            TextSave        =   "24/6/2020"
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            AutoSize        =   2
            Object.Width           =   1111
            MinWidth        =   88
            Text            =   "Usu�rio"
            TextSave        =   "Usu�rio"
         EndProperty
      EndProperty
   End
   Begin TabDlg.SSTab stbEndosso 
      Height          =   9045
      Left            =   0
      TabIndex        =   1
      Top             =   0
      Width           =   12480
      _ExtentX        =   22013
      _ExtentY        =   15954
      _Version        =   393216
      Tabs            =   19
      TabsPerRow      =   6
      TabHeight       =   520
      TabCaption(0)   =   "Proposta"
      TabPicture(0)   =   "frmEndosso.frx":0000
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "Frame5(6)"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "Frame5(7)"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).ControlCount=   2
      TabCaption(1)   =   "Cobran�a/Restitui��o"
      TabPicture(1)   =   "frmEndosso.frx":001C
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "fraRestituicao"
      Tab(1).Control(1)=   "fraCorretagem"
      Tab(1).Control(2)=   "fraEstipulante"
      Tab(1).Control(3)=   "cmdSairRestituicao"
      Tab(1).Control(4)=   "cmdAplicarRestituicao"
      Tab(1).Control(5)=   "fraCambio"
      Tab(1).Control(6)=   "Frame5(9)"
      Tab(1).Control(7)=   "FrameRestituicaoModular"
      Tab(1).Control(8)=   "fraCobranca"
      Tab(1).ControlCount=   9
      TabCaption(2)   =   "Cancelamento Ap�lice"
      TabPicture(2)   =   "frmEndosso.frx":0038
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "cmdSairCancApolice"
      Tab(2).Control(1)=   "cmdAplicarCancApolice"
      Tab(2).Control(2)=   "Frame5(13)"
      Tab(2).ControlCount=   3
      TabCaption(3)   =   "Cancelamento Endosso"
      TabPicture(3)   =   "frmEndosso.frx":0054
      Tab(3).ControlEnabled=   0   'False
      Tab(3).Control(0)=   "Frame5(14)"
      Tab(3).Control(1)=   "cmdSairCancEndosso"
      Tab(3).Control(2)=   "cmdAplicarCancEndosso"
      Tab(3).ControlCount=   3
      TabCaption(4)   =   "Dados Cadastrais"
      TabPicture(4)   =   "frmEndosso.frx":0070
      Tab(4).ControlEnabled=   0   'False
      Tab(4).Control(0)=   "Frame5(15)"
      Tab(4).Control(1)=   "Frame5(16)"
      Tab(4).Control(2)=   "cmdAplicarCadastral"
      Tab(4).Control(3)=   "cmdSairCadastral"
      Tab(4).ControlCount=   4
      TabCaption(5)   =   "Estipulante"
      TabPicture(5)   =   "frmEndosso.frx":008C
      Tab(5).ControlEnabled=   0   'False
      Tab(5).Control(0)=   "Frame5(17)"
      Tab(5).Control(1)=   "cmdSairEst"
      Tab(5).Control(2)=   "cmdAplicarEst"
      Tab(5).ControlCount=   3
      TabCaption(6)   =   "Corretagem"
      TabPicture(6)   =   "frmEndosso.frx":00A8
      Tab(6).ControlEnabled=   0   'False
      Tab(6).Control(0)=   "cmdSairCorretagem"
      Tab(6).Control(1)=   "CmdOkCor"
      Tab(6).Control(2)=   "Frame5(18)"
      Tab(6).ControlCount=   3
      TabCaption(7)   =   "Benefici�rio"
      TabPicture(7)   =   "frmEndosso.frx":00C4
      Tab(7).ControlEnabled=   0   'False
      Tab(7).Control(0)=   "cmdSairBenef"
      Tab(7).Control(1)=   "cmdAplicarBenef"
      Tab(7).Control(2)=   "Frame5(19)"
      Tab(7).ControlCount=   3
      TabCaption(8)   =   "Endere�o"
      TabPicture(8)   =   "frmEndosso.frx":00E0
      Tab(8).ControlEnabled=   0   'False
      Tab(8).Control(0)=   "Frame5(21)"
      Tab(8).Control(1)=   "Frame5(20)"
      Tab(8).Control(2)=   "cmdSairEndereco"
      Tab(8).Control(3)=   "cmdAplicarEndereco"
      Tab(8).ControlCount=   4
      TabCaption(9)   =   "Limite de Emiss�o"
      TabPicture(9)   =   "frmEndosso.frx":00FC
      Tab(9).ControlEnabled=   0   'False
      Tab(9).Control(0)=   "cmdAplicarLimite"
      Tab(9).Control(1)=   "cmdSairLimite"
      Tab(9).Control(2)=   "Frame5(0)"
      Tab(9).ControlCount=   3
      TabCaption(10)  =   "Import�ncia Segurada"
      TabPicture(10)  =   "frmEndosso.frx":0118
      Tab(10).ControlEnabled=   0   'False
      Tab(10).Control(0)=   "cmdSairIS"
      Tab(10).Control(1)=   "cmdAplicarIS"
      Tab(10).Control(2)=   "Frame5(1)"
      Tab(10).Control(3)=   "Frame5(2)"
      Tab(10).ControlCount=   4
      TabCaption(11)  =   "Reativa��o de Fatura"
      TabPicture(11)  =   "frmEndosso.frx":0134
      Tab(11).ControlEnabled=   0   'False
      Tab(11).Control(0)=   "cmdAplicarReativacaoFatura"
      Tab(11).Control(1)=   "cmdSairReativacaoFatura"
      Tab(11).Control(2)=   "Frame5(5)"
      Tab(11).Control(3)=   "Frame5(4)"
      Tab(11).ControlCount=   4
      TabCaption(12)  =   "Reativa��o Proposta"
      TabPicture(12)  =   "frmEndosso.frx":0150
      Tab(12).ControlEnabled=   0   'False
      Tab(12).Control(0)=   "Frame5(10)"
      Tab(12).ControlCount=   1
      TabCaption(13)  =   "Redu��o de IS"
      TabPicture(13)  =   "frmEndosso.frx":016C
      Tab(13).ControlEnabled=   0   'False
      Tab(13).Control(0)=   "Frame1(6)"
      Tab(13).Control(1)=   "Frame1(7)"
      Tab(13).Control(2)=   "Frame1(2)"
      Tab(13).Control(3)=   "Frame1(1)"
      Tab(13).Control(4)=   "Frame1(0)"
      Tab(13).Control(5)=   "grdSinistros"
      Tab(13).ControlCount=   6
      TabCaption(14)  =   "Reintegra��o IS"
      TabPicture(14)  =   "frmEndosso.frx":0188
      Tab(14).ControlEnabled=   0   'False
      Tab(14).Control(0)=   "cmdAplicar(2)"
      Tab(14).Control(1)=   "Frame1(13)"
      Tab(14).Control(2)=   "Frame1(12)"
      Tab(14).Control(3)=   "Frame1(11)"
      Tab(14).Control(4)=   "Frame1(10)"
      Tab(14).Control(5)=   "grdReintegracao(1)"
      Tab(14).ControlCount=   6
      TabCaption(15)  =   "Adequa��o �rea"
      TabPicture(15)  =   "frmEndosso.frx":01A4
      Tab(15).ControlEnabled=   0   'False
      Tab(15).Control(0)=   "TxtArea(36)"
      Tab(15).Control(1)=   "Frame1(20)"
      Tab(15).Control(2)=   "Frame1(16)"
      Tab(15).Control(3)=   "cmdAplicar(3)"
      Tab(15).Control(4)=   "Frame1(8)"
      Tab(15).Control(5)=   "Frame1(5)"
      Tab(15).Control(6)=   "Frame1(4)"
      Tab(15).Control(7)=   "Frame1(3)"
      Tab(15).Control(8)=   "grdReintegracao(0)"
      Tab(15).Control(9)=   "Label2(30)"
      Tab(15).ControlCount=   10
      TabCaption(16)  =   "Adeq. Produtividade"
      TabPicture(16)  =   "frmEndosso.frx":01C0
      Tab(16).ControlEnabled=   0   'False
      Tab(16).Control(0)=   "Frame1(15)"
      Tab(16).Control(1)=   "Frame1(14)"
      Tab(16).Control(2)=   "Frame1(18)"
      Tab(16).Control(3)=   "Frame1(9)"
      Tab(16).Control(4)=   "grdReintegracao(2)"
      Tab(16).ControlCount=   5
      TabCaption(17)  =   "Subsidio de Pr�mio"
      TabPicture(17)  =   "frmEndosso.frx":01DC
      Tab(17).ControlEnabled=   0   'False
      Tab(17).Control(0)=   "cmdAplicar(5)"
      Tab(17).Control(1)=   "Frame1(27)"
      Tab(17).Control(2)=   "grdReintegracao(3)"
      Tab(17).ControlCount=   3
      TabCaption(18)  =   "Cancelamento Sinistro"
      TabPicture(18)  =   "frmEndosso.frx":01F8
      Tab(18).ControlEnabled=   0   'False
      Tab(18).Control(0)=   "cmdSairCancSinistro"
      Tab(18).Control(1)=   "cmdAplicarCancSinistro"
      Tab(18).Control(2)=   "Frame5(24)"
      Tab(18).ControlCount=   3
      Begin VB.Frame fraRestituicao 
         Caption         =   "Restitui��o"
         Height          =   2865
         Left            =   -74880
         TabIndex        =   288
         Top             =   5460
         Width           =   11625
         Begin VB.Frame Frame5 
            Caption         =   "Valor Corretagem e Pr�-Labore"
            Height          =   1080
            Index           =   12
            Left            =   8160
            TabIndex        =   441
            Top             =   660
            Width           =   3060
            Begin VB.Label lblValProLabore 
               Alignment       =   1  'Right Justify
               BorderStyle     =   1  'Fixed Single
               Caption         =   "0,00"
               Height          =   255
               Left            =   1110
               TabIndex        =   445
               Top             =   675
               Width           =   1785
            End
            Begin VB.Label lblValCorretagem 
               Alignment       =   1  'Right Justify
               BorderStyle     =   1  'Fixed Single
               Caption         =   "0,00"
               Height          =   255
               Left            =   1110
               TabIndex        =   444
               Top             =   285
               Width           =   1785
            End
            Begin VB.Label Lbl 
               AutoSize        =   -1  'True
               Caption         =   "Pr�-Labore:"
               Height          =   195
               Index           =   15
               Left            =   210
               TabIndex        =   443
               Top             =   690
               Width           =   825
            End
            Begin VB.Label Lbl 
               AutoSize        =   -1  'True
               Caption         =   "Corretagem:"
               Height          =   195
               Index           =   16
               Left            =   180
               TabIndex        =   442
               Top             =   315
               Width           =   855
            End
         End
         Begin VB.Frame fraSubvencao 
            Caption         =   "Subven��o"
            Height          =   1935
            Left            =   5160
            TabIndex        =   435
            Top             =   660
            Visible         =   0   'False
            Width           =   2805
            Begin Mask2S.ConMask2S mskValEstadual 
               Height          =   315
               Left            =   870
               TabIndex        =   436
               Top             =   930
               Width           =   1785
               _ExtentX        =   3149
               _ExtentY        =   556
               mask            =   ""
               text            =   "0,00"
               locked          =   -1  'True
               enabled         =   -1  'True
            End
            Begin VB.Label Lbl 
               AutoSize        =   -1  'True
               Caption         =   "Federal:"
               Height          =   195
               Index           =   30
               Left            =   180
               TabIndex        =   440
               Top             =   570
               Width           =   570
            End
            Begin VB.Label Lbl 
               AutoSize        =   -1  'True
               Caption         =   "Estadual:"
               Height          =   195
               Index           =   31
               Left            =   120
               TabIndex        =   439
               Top             =   945
               Width           =   660
            End
            Begin VB.Label lblValFederal 
               Alignment       =   1  'Right Justify
               BorderStyle     =   1  'Fixed Single
               Caption         =   "0,00"
               Height          =   255
               Left            =   870
               TabIndex        =   438
               Top             =   540
               Width           =   1785
            End
            Begin VB.Label lblPercFederal 
               Alignment       =   1  'Right Justify
               BorderStyle     =   1  'Fixed Single
               Caption         =   "0,00"
               Height          =   255
               Left            =   1350
               TabIndex        =   437
               Top             =   210
               Visible         =   0   'False
               Width           =   1305
            End
         End
         Begin VB.Frame Frame5 
            Caption         =   "Valor Remunera��o Servi�o"
            Height          =   690
            Index           =   23
            Left            =   8160
            TabIndex        =   433
            Top             =   1905
            Width           =   3060
            Begin VB.Label lblValRemunercaoServico 
               Alignment       =   1  'Right Justify
               BorderStyle     =   1  'Fixed Single
               Caption         =   "0,00"
               Height          =   255
               Left            =   1110
               TabIndex        =   434
               Top             =   270
               Width           =   1785
            End
         End
         Begin Mask2S.ConMask2S mskValPremioTarifaRes 
            Height          =   315
            Left            =   2640
            TabIndex        =   289
            Top             =   510
            Width           =   1785
            _ExtentX        =   3149
            _ExtentY        =   556
            mask            =   ""
            text            =   "0,00"
            locked          =   0   'False
            enabled         =   -1  'True
         End
         Begin Mask2S.ConMask2S mskValIOFRes 
            Height          =   315
            Left            =   2640
            TabIndex        =   290
            Top             =   1710
            Width           =   1785
            _ExtentX        =   3149
            _ExtentY        =   556
            mask            =   ""
            text            =   "0,00"
            locked          =   -1  'True
            enabled         =   -1  'True
         End
         Begin Mask2S.ConMask2S mskDescontoRes 
            Height          =   315
            Left            =   2640
            TabIndex        =   291
            Top             =   1410
            Width           =   1785
            _ExtentX        =   3149
            _ExtentY        =   556
            mask            =   ""
            text            =   "0,00"
            locked          =   -1  'True
            enabled         =   -1  'True
         End
         Begin Mask2S.ConMask2S mskAdicFracionamentoRes 
            Height          =   315
            Left            =   2640
            TabIndex        =   292
            Top             =   1110
            Width           =   1785
            _ExtentX        =   3149
            _ExtentY        =   556
            mask            =   ""
            text            =   "0,00"
            locked          =   -1  'True
            enabled         =   -1  'True
         End
         Begin Mask2S.ConMask2S mskCustoCertificadoRes 
            Height          =   315
            Left            =   2640
            TabIndex        =   293
            Top             =   810
            Width           =   1785
            _ExtentX        =   3149
            _ExtentY        =   556
            mask            =   ""
            text            =   "0,00"
            locked          =   -1  'True
            enabled         =   -1  'True
         End
         Begin VB.Label lblTipoProposta 
            Height          =   255
            Index           =   2
            Left            =   1680
            TabIndex        =   430
            Top             =   240
            Width           =   975
         End
         Begin VB.Label lblValRestituicaoRes 
            Alignment       =   1  'Right Justify
            BorderStyle     =   1  'Fixed Single
            Caption         =   "0,00"
            Height          =   255
            Left            =   2640
            TabIndex        =   306
            Top             =   2160
            Width           =   1785
         End
         Begin VB.Label lbl_sinal 
            Caption         =   "="
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   5
            Left            =   4560
            TabIndex        =   305
            Top             =   2130
            Width           =   195
         End
         Begin VB.Label lbl_sinal 
            Caption         =   "-"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   3
            Left            =   4590
            TabIndex        =   304
            Top             =   1470
            Width           =   195
         End
         Begin VB.Label lbl_sinal 
            Caption         =   "+"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   4
            Left            =   4560
            TabIndex        =   303
            Top             =   1770
            Width           =   195
         End
         Begin VB.Label lbl_sinal 
            Caption         =   "+"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   2
            Left            =   4560
            TabIndex        =   302
            Top             =   1170
            Width           =   195
         End
         Begin VB.Label lbl_sinal 
            Caption         =   "+"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   1
            Left            =   4560
            TabIndex        =   301
            Top             =   840
            Width           =   195
         End
         Begin VB.Label lbl_sinal 
            Caption         =   "+"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   0
            Left            =   4560
            TabIndex        =   300
            Top             =   540
            Width           =   195
         End
         Begin VB.Label lblLegendaEndosso 
            AutoSize        =   -1  'True
            Caption         =   "Valor da Restitui��o:"
            Height          =   195
            Index           =   5
            Left            =   1020
            TabIndex        =   299
            Top             =   2190
            Width           =   1470
         End
         Begin VB.Label lblLegendaEndosso 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "IOF:"
            Height          =   195
            Index           =   4
            Left            =   2190
            TabIndex        =   298
            Top             =   1770
            Width           =   300
         End
         Begin VB.Label lblLegendaEndosso 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Premio Tarifa:"
            Height          =   195
            Index           =   2
            Left            =   1515
            TabIndex        =   297
            Top             =   570
            Width           =   975
         End
         Begin VB.Label lblLegendaEndosso 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Custo Certificado:"
            Height          =   195
            Index           =   0
            Left            =   1245
            TabIndex        =   296
            Top             =   870
            Width           =   1245
         End
         Begin VB.Label lblLegendaEndosso 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Adic. Fracionamento:"
            Height          =   195
            Index           =   21
            Left            =   990
            TabIndex        =   295
            Top             =   1170
            Width           =   1500
         End
         Begin VB.Label lblLegendaEndosso 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Desconto:"
            Height          =   195
            Index           =   22
            Left            =   1755
            TabIndex        =   294
            Top             =   1470
            Width           =   735
         End
         Begin VB.Line Line1 
            Index           =   0
            X1              =   900
            X2              =   4830
            Y1              =   2070
            Y2              =   2070
         End
      End
      Begin VB.CommandButton cmdSairCancSinistro 
         Caption         =   "&Sair"
         Height          =   435
         Left            =   -66000
         TabIndex        =   579
         Top             =   8280
         Width           =   1365
      End
      Begin VB.CommandButton cmdAplicarCancSinistro 
         Caption         =   "&Aplicar"
         Height          =   435
         Left            =   -64530
         TabIndex        =   578
         Top             =   8280
         Width           =   1365
      End
      Begin VB.Frame Frame5 
         Caption         =   "Cancelamento por Sinistro"
         Height          =   6555
         Index           =   24
         Left            =   -74760
         TabIndex        =   577
         Top             =   1440
         Width           =   11625
         Begin VB.TextBox txtMotivoCancelamentoSin 
            Height          =   325
            Left            =   240
            MaxLength       =   50
            TabIndex        =   581
            Top             =   600
            Width           =   8415
         End
         Begin VB.Label Lbl 
            AutoSize        =   -1  'True
            Caption         =   "Motivo do Cancelamento:"
            Height          =   195
            Index           =   40
            Left            =   240
            TabIndex        =   580
            Top             =   360
            Width           =   1815
         End
      End
      Begin VB.TextBox TxtArea 
         Enabled         =   0   'False
         Height          =   285
         Index           =   36
         Left            =   -68520
         TabIndex        =   567
         Top             =   6750
         Width           =   2415
      End
      Begin VB.CommandButton cmdAplicar 
         Caption         =   "&Aplicar"
         Height          =   375
         Index           =   5
         Left            =   -64680
         TabIndex        =   562
         Top             =   4430
         Width           =   975
      End
      Begin VB.Frame Frame1 
         Caption         =   "Valores para c�lculo"
         Height          =   1095
         Index           =   27
         Left            =   -74760
         TabIndex        =   556
         Top             =   4020
         Width           =   11655
         Begin VB.Frame Frame1 
            Height          =   855
            Index           =   25
            Left            =   9600
            TabIndex        =   563
            Top             =   120
            Width           =   1940
         End
         Begin VB.TextBox TxtArea 
            Height          =   285
            Index           =   52
            Left            =   2640
            TabIndex        =   558
            Top             =   480
            Width           =   2415
         End
         Begin VB.TextBox TxtArea 
            Enabled         =   0   'False
            Height          =   285
            Index           =   44
            Left            =   120
            TabIndex        =   557
            Top             =   480
            Width           =   2415
         End
         Begin VB.Label Label2 
            Caption         =   "Valor a Restituir"
            Height          =   255
            Index           =   35
            Left            =   2640
            TabIndex        =   560
            Top             =   240
            Width           =   1215
         End
         Begin VB.Label Label2 
            Caption         =   "Valor Pago"
            Height          =   255
            Index           =   34
            Left            =   120
            TabIndex        =   559
            Top             =   240
            Width           =   975
         End
      End
      Begin VB.Frame Frame1 
         Caption         =   "Nova IS Cobertura Adicional"
         Height          =   975
         Index           =   20
         Left            =   -68160
         TabIndex        =   545
         Top             =   7260
         Width           =   3375
         Begin VB.TextBox TxtArea 
            Enabled         =   0   'False
            Height          =   285
            Index           =   29
            Left            =   120
            TabIndex        =   546
            Top             =   480
            Width           =   3135
         End
         Begin VB.Label Label2 
            Caption         =   "IS = Nova �rea  x Custeio x Fator Replant."
            Height          =   255
            Index           =   27
            Left            =   120
            TabIndex        =   547
            Top             =   240
            Width           =   3135
         End
      End
      Begin VB.Frame Frame1 
         Height          =   1095
         Index           =   16
         Left            =   -74760
         TabIndex        =   538
         Top             =   5460
         Width           =   11650
         Begin VB.Frame Frame1 
            Height          =   855
            Index           =   19
            Left            =   6000
            TabIndex        =   542
            Top             =   120
            Width           =   5550
            Begin VB.TextBox TxtArea 
               Enabled         =   0   'False
               Height          =   285
               Index           =   33
               Left            =   4680
               TabIndex        =   554
               Top             =   480
               Visible         =   0   'False
               Width           =   255
            End
            Begin VB.TextBox TxtArea 
               Enabled         =   0   'False
               Height          =   285
               Index           =   28
               Left            =   240
               TabIndex        =   543
               Top             =   400
               Width           =   3375
            End
            Begin VB.Label Label4 
               Caption         =   "Reducao"
               Height          =   255
               Index           =   0
               Left            =   4560
               TabIndex        =   555
               Top             =   240
               Visible         =   0   'False
               Width           =   855
            End
            Begin VB.Label Label2 
               Caption         =   "Import�ncia Segurada  Cobertura Adicional (IS)"
               Height          =   255
               Index           =   24
               Left            =   240
               TabIndex        =   544
               Top             =   165
               Width           =   3375
            End
         End
         Begin VB.Frame Frame1 
            Height          =   855
            Index           =   17
            Left            =   120
            TabIndex        =   539
            Top             =   120
            Width           =   5820
            Begin VB.TextBox TxtArea 
               Enabled         =   0   'False
               Height          =   285
               Index           =   22
               Left            =   240
               TabIndex        =   540
               Top             =   400
               Width           =   3255
            End
            Begin VB.Label Label2 
               Caption         =   "Import�ncia Segurada Cobertura B�sica (IS)"
               Height          =   255
               Index           =   23
               Left            =   240
               TabIndex        =   541
               Top             =   160
               Width           =   3135
            End
         End
      End
      Begin VB.Frame Frame1 
         Caption         =   "Valores calculados"
         Height          =   1575
         Index           =   15
         Left            =   -72000
         TabIndex        =   535
         Top             =   5940
         Width           =   5775
         Begin VB.TextBox TxtArea 
            Enabled         =   0   'False
            Height          =   285
            Index           =   34
            Left            =   120
            TabIndex        =   565
            Top             =   480
            Width           =   3375
         End
         Begin VB.TextBox TxtArea 
            Enabled         =   0   'False
            Height          =   285
            Index           =   21
            Left            =   120
            TabIndex        =   536
            Top             =   1080
            Width           =   3375
         End
         Begin VB.Label Label2 
            Caption         =   "Faturamento Esperado = (Produtividade/60) x �rea Segurada x Pre�o Base"
            Height          =   255
            Index           =   28
            Left            =   120
            TabIndex        =   564
            Top             =   240
            Width           =   5535
         End
         Begin VB.Label Label2 
            Caption         =   "IS = Faturamento Esperado x N�vel Cobert."
            Height          =   255
            Index           =   14
            Left            =   120
            TabIndex        =   537
            Top             =   840
            Width           =   3135
         End
      End
      Begin VB.TextBox TxtArea 
         Enabled         =   0   'False
         Height          =   285
         Index           =   14
         Left            =   -65760
         TabIndex        =   531
         Top             =   4080
         Width           =   2175
      End
      Begin VB.Frame Frame1 
         Caption         =   "Valors Informados"
         Height          =   1575
         Index           =   14
         Left            =   -74400
         TabIndex        =   525
         Top             =   5940
         Width           =   2295
         Begin VB.TextBox TxtArea 
            Height          =   285
            Index           =   46
            Left            =   120
            TabIndex        =   526
            Top             =   480
            Width           =   1935
         End
         Begin VB.Label Label2 
            Caption         =   "Nova Produtividade"
            Height          =   255
            Index           =   39
            Left            =   120
            TabIndex        =   527
            Top             =   240
            Width           =   1815
         End
      End
      Begin VB.Frame Frame1 
         Caption         =   "Valores para c�lculo"
         Height          =   1815
         Index           =   18
         Left            =   -74400
         TabIndex        =   514
         Top             =   4020
         Width           =   10935
         Begin VB.TextBox TxtArea 
            Enabled         =   0   'False
            Height          =   285
            Index           =   47
            Left            =   6840
            TabIndex        =   551
            Top             =   1200
            Width           =   2055
         End
         Begin VB.TextBox TxtArea 
            Enabled         =   0   'False
            Height          =   285
            Index           =   30
            Left            =   8160
            TabIndex        =   549
            Top             =   480
            Width           =   2535
         End
         Begin VB.TextBox TxtArea 
            Enabled         =   0   'False
            Height          =   285
            Index           =   20
            Left            =   6360
            TabIndex        =   533
            Top             =   480
            Width           =   1695
         End
         Begin VB.TextBox TxtArea 
            Enabled         =   0   'False
            Height          =   285
            Index           =   13
            Left            =   2280
            TabIndex        =   529
            Top             =   1200
            Width           =   2295
         End
         Begin VB.TextBox TxtArea 
            Enabled         =   0   'False
            Height          =   285
            Index           =   51
            Left            =   4320
            TabIndex        =   519
            Top             =   480
            Width           =   1935
         End
         Begin VB.TextBox TxtArea 
            Enabled         =   0   'False
            Height          =   285
            Index           =   50
            Left            =   2280
            TabIndex        =   518
            Top             =   480
            Width           =   1935
         End
         Begin VB.TextBox TxtArea 
            Enabled         =   0   'False
            Height          =   285
            Index           =   49
            Left            =   240
            TabIndex        =   517
            Top             =   480
            Width           =   1935
         End
         Begin VB.TextBox TxtArea 
            Enabled         =   0   'False
            Height          =   285
            Index           =   48
            Left            =   240
            TabIndex        =   516
            Top             =   1200
            Width           =   1935
         End
         Begin VB.TextBox TxtArea 
            Enabled         =   0   'False
            Height          =   285
            Index           =   45
            Left            =   4680
            TabIndex        =   515
            Top             =   1200
            Width           =   2055
         End
         Begin VB.Label Label2 
            Caption         =   "Produtividade Municipio"
            Height          =   255
            Index           =   46
            Left            =   6840
            TabIndex        =   550
            Top             =   960
            Width           =   1815
         End
         Begin VB.Label Label2 
            Caption         =   "I.S.  Cobertura B�sica (IS)"
            Height          =   255
            Index           =   45
            Left            =   8280
            TabIndex        =   548
            Top             =   240
            Width           =   2175
         End
         Begin VB.Label Label2 
            Caption         =   "Produtividade Esperada"
            Height          =   255
            Index           =   13
            Left            =   6360
            TabIndex        =   534
            Top             =   240
            Width           =   1695
         End
         Begin VB.Label Label2 
            Caption         =   "N�vel Cobertura"
            Height          =   255
            Index           =   11
            Left            =   2280
            TabIndex        =   528
            Top             =   960
            Width           =   1815
         End
         Begin VB.Label Label2 
            Caption         =   "Fator Cobert. Replantio"
            Height          =   255
            Index           =   44
            Left            =   240
            TabIndex        =   524
            Top             =   960
            Width           =   1695
         End
         Begin VB.Label Label2 
            Caption         =   "Valor Custeio"
            Height          =   255
            Index           =   43
            Left            =   4320
            TabIndex        =   523
            Top             =   240
            Width           =   1215
         End
         Begin VB.Label Label2 
            Caption         =   "�rea Vigente"
            Height          =   255
            Index           =   42
            Left            =   2280
            TabIndex        =   522
            Top             =   240
            Width           =   1335
         End
         Begin VB.Label Label2 
            Caption         =   "�rea Original"
            Height          =   255
            Index           =   41
            Left            =   240
            TabIndex        =   521
            Top             =   240
            Width           =   975
         End
         Begin VB.Label Label3 
            Caption         =   "Pre�o Base"
            Height          =   255
            Index           =   4
            Left            =   4680
            TabIndex        =   520
            Top             =   960
            Width           =   1455
         End
      End
      Begin VB.Frame Frame1 
         Height          =   1575
         Index           =   9
         Left            =   -66120
         TabIndex        =   502
         Top             =   5940
         Width           =   2700
         Begin VB.CommandButton cmdAplicar 
            Caption         =   "&Aplicar"
            Height          =   375
            Index           =   4
            Left            =   720
            TabIndex        =   504
            Top             =   600
            Width           =   1095
         End
      End
      Begin VB.CommandButton cmdAplicar 
         Caption         =   "&Aplicar"
         Height          =   375
         Index           =   3
         Left            =   -64365
         TabIndex        =   501
         Top             =   7620
         Width           =   975
      End
      Begin VB.Frame Frame1 
         Height          =   975
         Index           =   8
         Left            =   -64680
         TabIndex        =   500
         Top             =   7260
         Width           =   1575
      End
      Begin VB.Frame Frame1 
         Caption         =   "Nova IS Cobertura B�sica"
         Height          =   975
         Index           =   5
         Left            =   -72000
         TabIndex        =   497
         Top             =   7260
         Width           =   3735
         Begin VB.TextBox TxtArea 
            Enabled         =   0   'False
            Height          =   285
            Index           =   11
            Left            =   120
            TabIndex        =   498
            Top             =   480
            Width           =   3495
         End
         Begin VB.Label Label2 
            Caption         =   "IS = �rea segurada X Valor custeio"
            Height          =   255
            Index           =   10
            Left            =   120
            TabIndex        =   499
            Top             =   240
            Width           =   3495
         End
      End
      Begin VB.Frame Frame1 
         Caption         =   "Valores para c�lculo"
         Height          =   1095
         Index           =   4
         Left            =   -74760
         TabIndex        =   489
         Top             =   4260
         Width           =   11655
         Begin VB.TextBox TxtArea 
            Enabled         =   0   'False
            Height          =   285
            Index           =   27
            Left            =   8520
            TabIndex        =   509
            Top             =   480
            Width           =   1215
         End
         Begin VB.TextBox TxtArea 
            Enabled         =   0   'False
            Height          =   285
            Index           =   26
            Left            =   7200
            TabIndex        =   508
            Top             =   480
            Width           =   1215
         End
         Begin VB.TextBox TxtArea 
            Enabled         =   0   'False
            Height          =   285
            Index           =   25
            Left            =   5760
            TabIndex        =   507
            Top             =   480
            Width           =   1335
         End
         Begin VB.TextBox TxtArea 
            Enabled         =   0   'False
            Height          =   285
            Index           =   24
            Left            =   4320
            TabIndex        =   506
            Top             =   480
            Width           =   1335
         End
         Begin VB.TextBox TxtArea 
            Enabled         =   0   'False
            Height          =   285
            Index           =   23
            Left            =   9840
            TabIndex        =   505
            Top             =   480
            Width           =   1695
         End
         Begin VB.TextBox TxtArea 
            Enabled         =   0   'False
            Height          =   285
            Index           =   10
            Left            =   120
            TabIndex        =   492
            Top             =   480
            Width           =   1335
         End
         Begin VB.TextBox TxtArea 
            Enabled         =   0   'False
            Height          =   285
            Index           =   9
            Left            =   1560
            TabIndex        =   491
            Top             =   480
            Width           =   1335
         End
         Begin VB.TextBox TxtArea 
            Enabled         =   0   'False
            Height          =   285
            Index           =   8
            Left            =   3000
            TabIndex        =   490
            Top             =   480
            Width           =   1215
         End
         Begin VB.Label Label3 
            Caption         =   "N�vel Cobertura"
            Height          =   255
            Index           =   2
            Left            =   8520
            TabIndex        =   530
            Top             =   240
            Width           =   1215
         End
         Begin VB.Label Label2 
            Caption         =   "Produt. Esperada"
            Height          =   255
            Index           =   26
            Left            =   5760
            TabIndex        =   513
            Top             =   240
            Width           =   1335
         End
         Begin VB.Label Label3 
            Caption         =   "Pre�o Base"
            Height          =   255
            Index           =   0
            Left            =   7200
            TabIndex        =   511
            Top             =   240
            Width           =   1815
         End
         Begin VB.Label Label2 
            Caption         =   "Produt. Municipio"
            Height          =   255
            Index           =   25
            Left            =   4320
            TabIndex        =   510
            Top             =   240
            Width           =   1335
         End
         Begin VB.Label Label2 
            Caption         =   "�rea Original"
            Height          =   255
            Index           =   9
            Left            =   120
            TabIndex        =   496
            Top             =   240
            Width           =   975
         End
         Begin VB.Label Label2 
            Caption         =   "�rea Vigente"
            Height          =   255
            Index           =   8
            Left            =   1560
            TabIndex        =   495
            Top             =   240
            Width           =   1215
         End
         Begin VB.Label Label2 
            Caption         =   "Valor Custeio"
            Height          =   255
            Index           =   7
            Left            =   3000
            TabIndex        =   494
            Top             =   240
            Width           =   1215
         End
         Begin VB.Label Label2 
            Caption         =   "Fator Cobert. Replant."
            Height          =   255
            Index           =   6
            Left            =   9840
            TabIndex        =   493
            Top             =   240
            Width           =   1695
         End
      End
      Begin VB.Frame Frame1 
         Caption         =   "Valors Informados"
         Height          =   975
         Index           =   3
         Left            =   -74760
         TabIndex        =   486
         Top             =   7260
         Width           =   2655
         Begin VB.TextBox TxtArea 
            Height          =   285
            Index           =   7
            Left            =   120
            TabIndex        =   487
            Top             =   480
            Width           =   2415
         End
         Begin VB.Label Label2 
            Caption         =   "Nova �rea"
            Height          =   375
            Index           =   5
            Left            =   120
            TabIndex        =   488
            Top             =   240
            Width           =   1575
         End
      End
      Begin VB.TextBox TxtArea 
         Enabled         =   0   'False
         Height          =   285
         Index           =   12
         Left            =   -67920
         TabIndex        =   483
         Top             =   4080
         Width           =   2055
      End
      Begin VB.CommandButton cmdAplicar 
         Caption         =   "&Aplicar"
         Height          =   375
         Index           =   2
         Left            =   -66240
         TabIndex        =   482
         Top             =   5220
         Width           =   975
      End
      Begin VB.Frame Frame1 
         Caption         =   "Valores calculados"
         Height          =   975
         Index           =   13
         Left            =   -71640
         TabIndex        =   479
         Top             =   4860
         Width           =   3375
         Begin VB.TextBox TxtArea 
            Enabled         =   0   'False
            Height          =   285
            Index           =   19
            Left            =   120
            TabIndex        =   480
            Top             =   480
            Width           =   3135
         End
         Begin VB.Label Label2 
            Caption         =   "IS = Nova �rea  x Custeio x Fator Replant."
            Height          =   255
            Index           =   20
            Left            =   120
            TabIndex        =   481
            Top             =   240
            Width           =   3135
         End
      End
      Begin VB.Frame Frame1 
         Caption         =   "Valors Informados"
         Height          =   975
         Index           =   12
         Left            =   -74400
         TabIndex        =   476
         Top             =   4860
         Width           =   2655
         Begin VB.TextBox TxtArea 
            Height          =   285
            Index           =   18
            Left            =   120
            TabIndex        =   477
            Top             =   480
            Width           =   2295
         End
         Begin VB.Label Label2 
            Caption         =   "�rea a reintegrar"
            Height          =   375
            Index           =   19
            Left            =   120
            TabIndex        =   478
            Top             =   240
            Width           =   1575
         End
      End
      Begin VB.Frame Frame1 
         Caption         =   "Valores para c�lculo"
         Height          =   975
         Index           =   11
         Left            =   -74400
         TabIndex        =   469
         Top             =   3900
         Width           =   10935
         Begin VB.TextBox TxtArea 
            Enabled         =   0   'False
            Height          =   285
            Index           =   32
            Left            =   6480
            TabIndex        =   553
            Top             =   480
            Width           =   2055
         End
         Begin VB.TextBox TxtArea 
            Enabled         =   0   'False
            Height          =   285
            Index           =   31
            Left            =   8640
            TabIndex        =   552
            Top             =   480
            Width           =   1815
         End
         Begin VB.TextBox TxtArea 
            Enabled         =   0   'False
            Height          =   285
            Index           =   15
            Left            =   4320
            TabIndex        =   474
            Top             =   480
            Width           =   2055
         End
         Begin VB.TextBox TxtArea 
            Enabled         =   0   'False
            Height          =   285
            Index           =   17
            Left            =   2160
            TabIndex        =   471
            Top             =   480
            Width           =   2055
         End
         Begin VB.TextBox TxtArea 
            Enabled         =   0   'False
            Height          =   285
            Index           =   16
            Left            =   120
            TabIndex        =   470
            Top             =   480
            Width           =   1935
         End
         Begin VB.Label Label2 
            Caption         =   "Import�ncia Segurada (IS)"
            Height          =   255
            Index           =   12
            Left            =   6480
            TabIndex        =   532
            Top             =   240
            Width           =   2175
         End
         Begin VB.Label Label2 
            Caption         =   "Fator Cobert. Replantio"
            Height          =   255
            Index           =   21
            Left            =   8640
            TabIndex        =   484
            Top             =   240
            Width           =   2055
         End
         Begin VB.Label Label2 
            Caption         =   "Valor Custeio"
            Height          =   255
            Index           =   16
            Left            =   4320
            TabIndex        =   475
            Top             =   240
            Width           =   1575
         End
         Begin VB.Label Label2 
            Caption         =   "�rea Vigente"
            Height          =   255
            Index           =   18
            Left            =   2160
            TabIndex        =   473
            Top             =   240
            Width           =   975
         End
         Begin VB.Label Label2 
            Caption         =   "�rea Original"
            Height          =   255
            Index           =   17
            Left            =   120
            TabIndex        =   472
            Top             =   240
            Width           =   975
         End
      End
      Begin VB.Frame Frame1 
         Height          =   975
         Index           =   10
         Left            =   -68160
         TabIndex        =   468
         Top             =   4860
         Width           =   4665
      End
      Begin VB.Frame Frame1 
         Caption         =   "Valors Informados"
         Height          =   975
         Index           =   6
         Left            =   -74400
         TabIndex        =   463
         Top             =   5340
         Width           =   2655
         Begin VB.TextBox TxtArea 
            Height          =   285
            Index           =   1
            Left            =   170
            TabIndex        =   464
            Top             =   480
            Width           =   2295
         End
         Begin VB.Label Label2 
            Caption         =   "�rea afetada sinistro"
            Height          =   375
            Index           =   1
            Left            =   240
            TabIndex        =   465
            Top             =   240
            Width           =   1575
         End
      End
      Begin VB.Frame Frame1 
         Caption         =   "Valores calculados"
         Height          =   975
         Index           =   7
         Left            =   -71685
         TabIndex        =   458
         Top             =   5340
         Width           =   5895
         Begin VB.TextBox TxtArea 
            Enabled         =   0   'False
            Height          =   285
            Index           =   5
            Left            =   2880
            TabIndex        =   460
            Top             =   480
            Width           =   2895
         End
         Begin VB.TextBox TxtArea 
            Enabled         =   0   'False
            Height          =   285
            Index           =   2
            Left            =   120
            TabIndex        =   459
            Top             =   480
            Width           =   2655
         End
         Begin VB.Label Label2 
            Caption         =   "IS = �rea p�s  x Custeio x Fator Replant."
            Height          =   255
            Index           =   4
            Left            =   2880
            TabIndex        =   462
            Top             =   240
            Width           =   2895
         End
         Begin VB.Label Label2 
            Caption         =   "�rea p�s redu��o (Vigente - afetada)"
            Height          =   255
            Index           =   15
            Left            =   120
            TabIndex        =   461
            Top             =   240
            Width           =   2775
         End
      End
      Begin VB.Frame Frame1 
         Height          =   975
         Index           =   2
         Left            =   -65730
         TabIndex        =   454
         Top             =   5340
         Width           =   2220
         Begin VB.CommandButton cmdAplicar 
            Caption         =   "&Aplicar"
            Height          =   375
            Index           =   0
            Left            =   600
            TabIndex        =   457
            Top             =   360
            Width           =   975
         End
      End
      Begin VB.Frame Frame1 
         Height          =   975
         Index           =   1
         Left            =   -66165
         TabIndex        =   453
         Top             =   4260
         Width           =   2640
         Begin VB.TextBox TxtArea 
            Enabled         =   0   'False
            Height          =   285
            Index           =   6
            Left            =   120
            TabIndex        =   467
            Top             =   480
            Width           =   2295
         End
         Begin VB.Label Label2 
            Caption         =   "Import�ncia Segurada (IS)"
            Height          =   255
            Index           =   22
            Left            =   120
            TabIndex        =   466
            Top             =   240
            Width           =   2175
         End
      End
      Begin VB.Frame Frame1 
         Caption         =   "Valores para c�lculo"
         Height          =   975
         Index           =   0
         Left            =   -74400
         TabIndex        =   448
         Top             =   4260
         Width           =   8175
         Begin VB.TextBox TxtArea 
            Enabled         =   0   'False
            Height          =   285
            Index           =   4
            Left            =   5280
            TabIndex        =   455
            Top             =   480
            Width           =   2655
         End
         Begin VB.TextBox TxtArea 
            Enabled         =   0   'False
            Height          =   285
            Index           =   0
            Left            =   240
            TabIndex        =   450
            Top             =   480
            Width           =   2415
         End
         Begin VB.TextBox TxtArea 
            Enabled         =   0   'False
            Height          =   285
            Index           =   3
            Left            =   2760
            TabIndex        =   449
            Top             =   480
            Width           =   2415
         End
         Begin VB.Label Label2 
            Caption         =   "Fator Cobert. Replantio"
            Height          =   255
            Index           =   3
            Left            =   5280
            TabIndex        =   456
            Top             =   240
            Width           =   2655
         End
         Begin VB.Label Label2 
            Caption         =   "�rea Vigente"
            Height          =   255
            Index           =   0
            Left            =   240
            TabIndex        =   452
            Top             =   240
            Width           =   975
         End
         Begin VB.Label Label2 
            Caption         =   "Valor Custeio"
            Height          =   255
            Index           =   2
            Left            =   2760
            TabIndex        =   451
            Top             =   240
            Width           =   1575
         End
      End
      Begin VB.Frame Frame5 
         Height          =   7455
         Index           =   10
         Left            =   -74850
         TabIndex        =   364
         Top             =   1380
         Width           =   11655
         Begin VB.CheckBox chkImpedirAgendamento 
            Caption         =   "Impedir agendamento / cobran�a futura"
            Height          =   195
            Left            =   1890
            TabIndex        =   582
            Top             =   1170
            Width           =   3315
         End
         Begin VB.Frame Frame5 
            Caption         =   "Parcelas:"
            Height          =   3735
            Index           =   22
            Left            =   510
            TabIndex        =   366
            Top             =   1560
            Width           =   5895
            Begin VB.CommandButton cmdBtnReativacaoProposta 
               Caption         =   "Reagendamento Autom�tico"
               Height          =   495
               Index           =   2
               Left            =   4500
               TabIndex        =   584
               Top             =   2340
               Width           =   1335
            End
            Begin VB.CommandButton cmdBtnReativacaoProposta 
               Caption         =   "Inclus�o Autom�tica"
               Height          =   495
               Index           =   3
               Left            =   4500
               TabIndex        =   583
               Top             =   2970
               Width           =   1335
            End
            Begin VB.CommandButton cmdExcluirParcelaReativacao 
               Caption         =   "Excluir >>"
               Height          =   495
               Left            =   4485
               TabIndex        =   376
               Top             =   1680
               Width           =   1335
            End
            Begin VB.CommandButton cmdAlterarParcelaReativacao 
               Caption         =   "<< Alterar >>"
               Height          =   495
               Left            =   4485
               TabIndex        =   374
               Top             =   1020
               Width           =   1335
            End
            Begin VB.CommandButton CmdIncluirParcelaReativacao 
               Caption         =   "<< Incluir "
               Height          =   495
               Left            =   4485
               TabIndex        =   373
               Top             =   360
               Width           =   1335
            End
            Begin MSMask.MaskEdBox mskDtParcelaReativacao 
               Height          =   300
               Left            =   1380
               TabIndex        =   370
               Top             =   3180
               Width           =   1215
               _ExtentX        =   2143
               _ExtentY        =   529
               _Version        =   393216
               MaxLength       =   10
               Mask            =   "##/##/####"
               PromptChar      =   "_"
            End
            Begin MSFlexGridLib.MSFlexGrid grdParcelasReativacao 
               Height          =   2445
               Left            =   240
               TabIndex        =   375
               Top             =   360
               Width           =   4095
               _ExtentX        =   7223
               _ExtentY        =   4313
               _Version        =   393216
               Rows            =   1
               Cols            =   5
               FixedCols       =   0
               FocusRect       =   0
               HighLight       =   2
               SelectionMode   =   1
               FormatString    =   "< N� Parcela | Valor Parcela       | Data Parcela       | situacao | anomesref"
            End
            Begin Mask2S.ConMask2S mskValParcelaReativacao 
               Height          =   300
               Left            =   240
               TabIndex        =   568
               Top             =   3180
               Width           =   975
               _ExtentX        =   1720
               _ExtentY        =   529
               mask            =   ""
               text            =   "0,00"
               locked          =   0   'False
               enabled         =   -1  'True
            End
            Begin VB.Label Lbl 
               AutoSize        =   -1  'True
               Caption         =   "Val. Parcela:"
               Height          =   195
               Index           =   35
               Left            =   240
               TabIndex        =   368
               Top             =   2940
               Width           =   900
            End
            Begin VB.Label Lbl 
               AutoSize        =   -1  'True
               Caption         =   "Data Parcela:"
               Height          =   195
               Index           =   34
               Left            =   1380
               TabIndex        =   367
               Top             =   2940
               Width           =   975
            End
         End
         Begin VB.CommandButton cmdBtnReativacaoProposta 
            Caption         =   "&Sair"
            Height          =   435
            Index           =   0
            Left            =   8640
            TabIndex        =   371
            Top             =   6840
            Width           =   1365
         End
         Begin VB.CommandButton cmdBtnReativacaoProposta 
            Caption         =   "&Aplicar"
            Height          =   435
            Index           =   1
            Left            =   10080
            TabIndex        =   372
            Top             =   6840
            Width           =   1365
         End
         Begin MSMask.MaskEdBox mskDtReativacao 
            Height          =   300
            Left            =   510
            TabIndex        =   369
            Top             =   1110
            Width           =   1215
            _ExtentX        =   2143
            _ExtentY        =   529
            _Version        =   393216
            AutoTab         =   -1  'True
            MaxLength       =   10
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Mask            =   "##/##/####"
            PromptChar      =   "_"
         End
         Begin VB.Label Lbl 
            AutoSize        =   -1  'True
            Caption         =   "Data Reativa��o:"
            Height          =   195
            Index           =   29
            Left            =   510
            TabIndex        =   365
            Top             =   840
            Width           =   1260
         End
      End
      Begin VB.CommandButton cmdAplicarReativacaoFatura 
         Caption         =   "&Aplicar"
         Height          =   435
         Left            =   -64800
         TabIndex        =   363
         Top             =   8400
         Width           =   1335
      End
      Begin VB.CommandButton cmdSairReativacaoFatura 
         Caption         =   "&Sair"
         Height          =   435
         Left            =   -66240
         TabIndex        =   362
         Top             =   8400
         Width           =   1335
      End
      Begin VB.Frame Frame5 
         Caption         =   "Faturas Canceladas"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   2745
         Index           =   5
         Left            =   -74700
         TabIndex        =   359
         Top             =   4350
         Width           =   11235
         Begin MSFlexGridLib.MSFlexGrid grdFaturaCancelada 
            Height          =   2205
            Left            =   510
            TabIndex        =   360
            Top             =   330
            Width           =   10275
            _ExtentX        =   18124
            _ExtentY        =   3889
            _Version        =   393216
            Cols            =   7
            FixedCols       =   0
            AllowBigSelection=   0   'False
            FocusRect       =   0
            SelectionMode   =   1
            FormatString    =   "Fatura           |Imp. Segurada      |Valor Bruto            |Situa��o      |In�cio de Vig�ncia |Fim de Vig�ncia    |"
         End
      End
      Begin VB.Frame Frame5 
         Caption         =   "Subgrupos"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   2805
         Index           =   4
         Left            =   -74700
         TabIndex        =   357
         Top             =   1410
         Width           =   11235
         Begin MSFlexGridLib.MSFlexGrid grdSubGrupos 
            Height          =   2295
            Left            =   540
            TabIndex        =   358
            Top             =   330
            Width           =   10245
            _ExtentX        =   18071
            _ExtentY        =   4048
            _Version        =   393216
            FixedCols       =   0
            AllowBigSelection=   0   'False
            FocusRect       =   0
            SelectionMode   =   1
            FormatString    =   $"frmEndosso.frx":0214
         End
      End
      Begin VB.CommandButton cmdSairIS 
         Caption         =   "&Sair"
         Height          =   435
         Left            =   -66285
         TabIndex        =   339
         Top             =   8340
         Width           =   1365
      End
      Begin VB.CommandButton cmdAplicarIS 
         Caption         =   "&Aplicar"
         Height          =   435
         Left            =   -64815
         TabIndex        =   340
         Top             =   8340
         Width           =   1365
      End
      Begin VB.CommandButton cmdAplicarLimite 
         Caption         =   "&Aplicar"
         Height          =   435
         Left            =   -64680
         TabIndex        =   311
         Top             =   8400
         Width           =   1335
      End
      Begin VB.CommandButton cmdSairLimite 
         Caption         =   "&Sair"
         Height          =   435
         Left            =   -66120
         TabIndex        =   310
         Top             =   8400
         Width           =   1335
      End
      Begin VB.Frame Frame5 
         Height          =   1695
         Index           =   0
         Left            =   -74640
         TabIndex        =   309
         Top             =   1440
         Width           =   11295
         Begin Mask2S.ConMask2S mskLimiteAtual 
            Height          =   285
            Index           =   1
            Left            =   8040
            TabIndex        =   315
            Top             =   1080
            Width           =   1935
            _ExtentX        =   3413
            _ExtentY        =   503
            mask            =   ""
            text            =   "0,00"
            locked          =   0   'False
            enabled         =   -1  'True
         End
         Begin Mask2S.ConMask2S mskLimiteAtual 
            Height          =   285
            Index           =   0
            Left            =   8040
            TabIndex        =   312
            Top             =   480
            Width           =   1935
            _ExtentX        =   3413
            _ExtentY        =   503
            mask            =   ""
            text            =   "0,00"
            locked          =   0   'False
            enabled         =   0   'False
         End
         Begin MSMask.MaskEdBox mskDtAtualLimite 
            Height          =   285
            Left            =   2760
            TabIndex        =   313
            Top             =   480
            Width           =   1335
            _ExtentX        =   2355
            _ExtentY        =   503
            _Version        =   393216
            Enabled         =   0   'False
            MaxLength       =   10
            Mask            =   "##/##/####"
            PromptChar      =   "_"
         End
         Begin MSMask.MaskEdBox mskDtNovoLimite 
            Height          =   285
            Left            =   2760
            TabIndex        =   314
            Top             =   1080
            Width           =   1335
            _ExtentX        =   2355
            _ExtentY        =   503
            _Version        =   393216
            MaxLength       =   10
            Mask            =   "##/##/####"
            PromptChar      =   "_"
         End
         Begin VB.Label Lbl 
            AutoSize        =   -1  'True
            Caption         =   "Limite Atual :"
            Height          =   195
            Index           =   22
            Left            =   6960
            TabIndex        =   347
            Top             =   480
            Width           =   900
         End
         Begin VB.Label Lbl 
            AutoSize        =   -1  'True
            Caption         =   "Limite Novo :"
            Height          =   195
            Index           =   21
            Left            =   6960
            TabIndex        =   346
            Top             =   1080
            Width           =   930
         End
         Begin VB.Label Lbl 
            AutoSize        =   -1  'True
            Caption         =   "In�cio de Vig�ncia Nova :"
            Height          =   195
            Index           =   20
            Left            =   720
            TabIndex        =   345
            Top             =   1080
            Width           =   1815
         End
         Begin VB.Label Lbl 
            AutoSize        =   -1  'True
            Caption         =   "In�cio de Vig�ncia Atual :"
            Height          =   195
            Index           =   19
            Left            =   720
            TabIndex        =   344
            Top             =   480
            Width           =   1785
         End
      End
      Begin VB.CommandButton cmdAplicarEndereco 
         Caption         =   "&Aplicar"
         Height          =   435
         Left            =   -64635
         TabIndex        =   308
         Top             =   8460
         Width           =   1365
      End
      Begin VB.CommandButton cmdSairEndereco 
         Caption         =   "&Sair"
         Height          =   435
         Left            =   -66105
         TabIndex        =   307
         Top             =   8460
         Width           =   1365
      End
      Begin VB.Frame Frame5 
         Caption         =   "   Sele��o de Endosso   "
         Height          =   4845
         Index           =   7
         Left            =   120
         TabIndex        =   224
         Top             =   4005
         Width           =   11625
         Begin VB.Frame Frame5 
            Caption         =   "Dados gerais do endosso:"
            Height          =   3195
            Index           =   8
            Left            =   180
            TabIndex        =   229
            Top             =   1080
            Width           =   11310
            Begin VB.TextBox txtPermiteRestituicao 
               Enabled         =   0   'False
               Height          =   315
               Left            =   10530
               MaxLength       =   11
               TabIndex        =   377
               Top             =   300
               Visible         =   0   'False
               Width           =   615
            End
            Begin VB.TextBox txtDescEndosso 
               BeginProperty Font 
                  Name            =   "Courier"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   1005
               Left            =   180
               MultiLine       =   -1  'True
               ScrollBars      =   2  'Vertical
               TabIndex        =   236
               Top             =   2040
               Width           =   11010
            End
            Begin VB.TextBox txtNumOrd 
               Height          =   315
               Left            =   1980
               MaxLength       =   11
               TabIndex        =   235
               Top             =   1410
               Visible         =   0   'False
               Width           =   1215
            End
            Begin VB.TextBox txtEndossoLider 
               Height          =   315
               Left            =   3840
               MaxLength       =   11
               TabIndex        =   234
               Top             =   1410
               Visible         =   0   'False
               Width           =   1095
            End
            Begin VB.Frame fraIS 
               Caption         =   "Import�ncia Segurada (manter a v�rgula)"
               Height          =   885
               Left            =   3840
               TabIndex        =   230
               Top             =   240
               Visible         =   0   'False
               Width           =   3375
               Begin VB.OptionButton optNovVal 
                  Caption         =   "Novo valor"
                  Height          =   255
                  Left            =   120
                  TabIndex        =   233
                  Top             =   225
                  Width           =   1095
               End
               Begin VB.OptionButton optRestVal 
                  Caption         =   "Valor a reduzir "
                  Height          =   255
                  Left            =   120
                  TabIndex        =   232
                  Top             =   540
                  Width           =   1425
               End
               Begin VB.TextBox mskIS 
                  Height          =   315
                  Left            =   1680
                  MaxLength       =   15
                  TabIndex        =   231
                  Top             =   360
                  Visible         =   0   'False
                  Width           =   1575
               End
            End
            Begin MSMask.MaskEdBox mskDtEndosso 
               Height          =   315
               Left            =   180
               TabIndex        =   237
               Top             =   720
               Width           =   1215
               _ExtentX        =   2143
               _ExtentY        =   556
               _Version        =   393216
               AutoTab         =   -1  'True
               MaxLength       =   10
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Mask            =   "##/##/####"
               PromptChar      =   "_"
            End
            Begin MSMask.MaskEdBox mskDtEmissao 
               Height          =   315
               Left            =   180
               TabIndex        =   238
               Top             =   1410
               Width           =   1215
               _ExtentX        =   2143
               _ExtentY        =   556
               _Version        =   393216
               AutoTab         =   -1  'True
               Enabled         =   0   'False
               MaxLength       =   10
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Mask            =   "##/##/####"
               PromptChar      =   "_"
            End
            Begin MSMask.MaskEdBox mskFimVig 
               Height          =   315
               Left            =   1980
               TabIndex        =   239
               Top             =   720
               Visible         =   0   'False
               Width           =   1215
               _ExtentX        =   2143
               _ExtentY        =   556
               _Version        =   393216
               AutoTab         =   -1  'True
               HideSelection   =   0   'False
               MaxLength       =   10
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Mask            =   "##/##/####"
               PromptChar      =   "_"
            End
            Begin MSMask.MaskEdBox mskDtRecPro 
               Height          =   315
               Left            =   5190
               TabIndex        =   240
               Top             =   1410
               Visible         =   0   'False
               Width           =   1215
               _ExtentX        =   2143
               _ExtentY        =   556
               _Version        =   393216
               AutoTab         =   -1  'True
               HideSelection   =   0   'False
               MaxLength       =   10
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Mask            =   "##/##/####"
               PromptChar      =   "_"
            End
            Begin VB.Label Lbl 
               AutoSize        =   -1  'True
               Caption         =   "Fim de Vig�ncia:"
               Height          =   195
               Index           =   28
               Left            =   1980
               TabIndex        =   361
               Top             =   480
               Visible         =   0   'False
               Width           =   1170
            End
            Begin VB.Label Lbl 
               AutoSize        =   -1  'True
               Caption         =   "N� de Ordem:"
               Height          =   195
               Index           =   14
               Left            =   1995
               TabIndex        =   343
               Top             =   1200
               Visible         =   0   'False
               Width           =   960
            End
            Begin VB.Label Lbl 
               AutoSize        =   -1  'True
               Caption         =   "Endosso L�der:"
               Height          =   195
               Index           =   13
               Left            =   3840
               TabIndex        =   342
               Top             =   1200
               Visible         =   0   'False
               Width           =   1080
            End
            Begin VB.Label Lbl 
               AutoSize        =   -1  'True
               Caption         =   "Recebimento do processo :"
               Height          =   195
               Index           =   12
               Left            =   5190
               TabIndex        =   341
               Top             =   1200
               Visible         =   0   'False
               Width           =   1950
            End
            Begin VB.Label Lbl 
               Caption         =   "In�cio de Vig�ncia / Pedido:"
               Height          =   375
               Index           =   75
               Left            =   180
               TabIndex        =   243
               Top             =   300
               Width           =   1365
            End
            Begin VB.Label Lbl 
               AutoSize        =   -1  'True
               Caption         =   "Texto do Endosso:"
               Height          =   195
               Index           =   80
               Left            =   180
               TabIndex        =   242
               Top             =   1800
               Width           =   1380
            End
            Begin VB.Label Lbl 
               AutoSize        =   -1  'True
               Caption         =   "Emiss�o:"
               Height          =   195
               Index           =   86
               Left            =   180
               TabIndex        =   241
               Top             =   1200
               Width           =   675
            End
         End
         Begin VB.CommandButton cmdAplicarProposta 
            Caption         =   "&Aplicar"
            Height          =   390
            Left            =   10080
            TabIndex        =   228
            Top             =   4320
            Width           =   1365
         End
         Begin VB.Frame Frame5 
            Caption         =   "Tipo de Endosso"
            Height          =   795
            Index           =   11
            Left            =   180
            TabIndex        =   226
            Top             =   255
            Width           =   11295
            Begin VB.ComboBox cboTipoEndosso 
               Height          =   315
               ItemData        =   "frmEndosso.frx":02DD
               Left            =   210
               List            =   "frmEndosso.frx":02DF
               Style           =   2  'Dropdown List
               TabIndex        =   227
               Top             =   300
               Width           =   10950
            End
         End
         Begin VB.CommandButton cmdSairProposta 
            Caption         =   "&Sair"
            Height          =   390
            Left            =   8670
            TabIndex        =   225
            Top             =   4320
            Width           =   1365
         End
      End
      Begin VB.Frame Frame5 
         Height          =   2685
         Index           =   6
         Left            =   105
         TabIndex        =   195
         Top             =   1260
         Width           =   11640
         Begin VB.TextBox txtNomeEstipulante 
            BackColor       =   &H80000004&
            Height          =   330
            Index           =   1
            Left            =   225
            Locked          =   -1  'True
            TabIndex        =   432
            Top             =   2250
            Width           =   10860
         End
         Begin VB.TextBox txtNumEndosso 
            BackColor       =   &H80000004&
            Height          =   345
            Left            =   7935
            Locked          =   -1  'True
            TabIndex        =   208
            TabStop         =   0   'False
            Top             =   405
            Width           =   1455
         End
         Begin VB.TextBox txtApolice 
            BackColor       =   &H80000004&
            Height          =   315
            Left            =   240
            Locked          =   -1  'True
            TabIndex        =   207
            TabStop         =   0   'False
            Top             =   1620
            Width           =   1335
         End
         Begin VB.TextBox txtFimVigencia 
            BackColor       =   &H80000004&
            Height          =   315
            Left            =   9720
            Locked          =   -1  'True
            TabIndex        =   206
            TabStop         =   0   'False
            Top             =   1590
            Width           =   1335
         End
         Begin VB.TextBox txtIniVigencia 
            BackColor       =   &H80000004&
            Height          =   315
            Left            =   9705
            Locked          =   -1  'True
            TabIndex        =   205
            TabStop         =   0   'False
            Top             =   1005
            Width           =   1335
         End
         Begin VB.TextBox txtPropostaid 
            BackColor       =   &H80000004&
            Height          =   285
            Index           =   0
            Left            =   240
            Locked          =   -1  'True
            TabIndex        =   204
            TabStop         =   0   'False
            Top             =   405
            Width           =   1335
         End
         Begin VB.TextBox txtDtContratacao 
            BackColor       =   &H80000004&
            Height          =   345
            Left            =   9720
            Locked          =   -1  'True
            TabIndex        =   203
            TabStop         =   0   'False
            Top             =   405
            Width           =   1335
         End
         Begin VB.TextBox txtProduto 
            Alignment       =   1  'Right Justify
            BackColor       =   &H80000004&
            Height          =   315
            Left            =   1875
            Locked          =   -1  'True
            TabIndex        =   202
            TabStop         =   0   'False
            Top             =   405
            Width           =   645
         End
         Begin VB.TextBox txtPropostaid 
            BackColor       =   &H80000004&
            Height          =   285
            Index           =   1
            Left            =   240
            Locked          =   -1  'True
            TabIndex        =   201
            TabStop         =   0   'False
            Top             =   1005
            Width           =   1335
         End
         Begin VB.TextBox txtRamo 
            Alignment       =   1  'Right Justify
            BackColor       =   &H80000004&
            Height          =   315
            Left            =   1890
            Locked          =   -1  'True
            TabIndex        =   200
            TabStop         =   0   'False
            Top             =   1005
            Width           =   615
         End
         Begin VB.TextBox txtSubramo 
            Alignment       =   1  'Right Justify
            BackColor       =   &H80000004&
            Height          =   315
            Left            =   1890
            Locked          =   -1  'True
            TabIndex        =   199
            TabStop         =   0   'False
            Top             =   1620
            Width           =   615
         End
         Begin VB.TextBox txtNomeRamo 
            BackColor       =   &H80000004&
            Height          =   315
            Left            =   2700
            Locked          =   -1  'True
            TabIndex        =   198
            TabStop         =   0   'False
            Top             =   1005
            Width           =   6705
         End
         Begin VB.TextBox txtNomeSubramo 
            BackColor       =   &H80000004&
            Height          =   315
            Left            =   2700
            Locked          =   -1  'True
            TabIndex        =   197
            TabStop         =   0   'False
            Top             =   1620
            Width           =   4905
         End
         Begin VB.TextBox txtNomeProduto 
            BackColor       =   &H80000004&
            Height          =   345
            Left            =   2700
            Locked          =   -1  'True
            TabIndex        =   196
            TabStop         =   0   'False
            Top             =   405
            Width           =   4905
         End
         Begin VB.Label Lbl 
            Caption         =   "Estipulante"
            Height          =   240
            Index           =   57
            Left            =   225
            TabIndex        =   431
            Top             =   2025
            Width           =   1050
         End
         Begin VB.Label Lbl 
            AutoSize        =   -1  'True
            Caption         =   "Endosso N�:"
            Height          =   195
            Index           =   5
            Left            =   7935
            TabIndex        =   223
            Top             =   180
            Width           =   930
         End
         Begin VB.Label Lbl 
            AutoSize        =   -1  'True
            Caption         =   "Ap�lice:"
            Height          =   195
            Index           =   4
            Left            =   240
            TabIndex        =   222
            Top             =   1380
            Width           =   615
         End
         Begin VB.Label Lbl 
            AutoSize        =   -1  'True
            Caption         =   "Fim de Vig�ncia:"
            Height          =   195
            Index           =   7
            Left            =   9720
            TabIndex        =   221
            Top             =   1380
            Width           =   1215
         End
         Begin VB.Label Lbl 
            AutoSize        =   -1  'True
            Caption         =   "In�cio de Vig�ncia:"
            Height          =   195
            Index           =   6
            Left            =   9705
            TabIndex        =   220
            Top             =   795
            Width           =   1380
         End
         Begin VB.Label Lbl 
            AutoSize        =   -1  'True
            Caption         =   "Proposta:"
            Height          =   195
            Index           =   52
            Left            =   240
            TabIndex        =   219
            Top             =   180
            Width           =   720
         End
         Begin VB.Label Lbl 
            AutoSize        =   -1  'True
            Caption         =   "Ramo:"
            Height          =   195
            Index           =   3
            Left            =   1875
            TabIndex        =   218
            Top             =   780
            Width           =   510
         End
         Begin VB.Label Lbl 
            AutoSize        =   -1  'True
            Caption         =   "Produto:"
            Height          =   195
            Index           =   51
            Left            =   1875
            TabIndex        =   217
            Top             =   180
            Width           =   645
         End
         Begin VB.Label Lbl 
            AutoSize        =   -1  'True
            Caption         =   "Proposta BB:"
            Height          =   195
            Index           =   58
            Left            =   240
            TabIndex        =   216
            Top             =   780
            Width           =   975
         End
         Begin VB.Label Lbl 
            AutoSize        =   -1  'True
            Caption         =   "Data da Contrata��o:"
            Height          =   195
            Index           =   50
            Left            =   9720
            TabIndex        =   215
            Top             =   180
            Width           =   1530
         End
         Begin VB.Label lblTpEmissao 
            Alignment       =   2  'Center
            BorderStyle     =   1  'Fixed Single
            Caption         =   "Tipo Emiss�o"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   7995
            TabIndex        =   214
            Top             =   1620
            Width           =   1425
         End
         Begin VB.Label Lbl 
            AutoSize        =   -1  'True
            Caption         =   "Tipo de Emiss�o:"
            Height          =   195
            Index           =   67
            Left            =   7965
            TabIndex        =   213
            Top             =   1380
            Width           =   1260
         End
         Begin VB.Label Lbl 
            AutoSize        =   -1  'True
            Caption         =   "Subramo:"
            Height          =   195
            Index           =   0
            Left            =   1890
            TabIndex        =   212
            Top             =   1380
            Width           =   675
         End
         Begin VB.Label Lbl 
            AutoSize        =   -1  'True
            Caption         =   "-"
            Height          =   195
            Index           =   8
            Left            =   2580
            TabIndex        =   211
            Top             =   1065
            Width           =   45
         End
         Begin VB.Label Lbl 
            AutoSize        =   -1  'True
            Caption         =   "-"
            Height          =   195
            Index           =   9
            Left            =   2580
            TabIndex        =   210
            Top             =   1680
            Width           =   45
         End
         Begin VB.Label Lbl 
            AutoSize        =   -1  'True
            Caption         =   "-"
            Height          =   195
            Index           =   10
            Left            =   2580
            TabIndex        =   209
            Top             =   495
            Width           =   45
         End
      End
      Begin VB.Frame fraCorretagem 
         Caption         =   "Corretagem"
         Height          =   1665
         Left            =   -74880
         TabIndex        =   184
         Top             =   2160
         Width           =   11625
         Begin VB.CommandButton cmdCorretorCobRemover 
            Caption         =   "Remover  >>"
            Enabled         =   0   'False
            Height          =   345
            Left            =   10080
            TabIndex        =   190
            Top             =   1155
            Width           =   1215
         End
         Begin VB.CommandButton cmdCorretorCobAdicionar 
            Caption         =   "<<  Adicionar"
            Enabled         =   0   'False
            Height          =   345
            Left            =   10080
            TabIndex        =   189
            Top             =   240
            Width           =   1215
         End
         Begin VB.CommandButton cmdCorretorCobAlterar 
            Caption         =   "<< Alterar >>"
            Enabled         =   0   'False
            Height          =   345
            Left            =   10080
            TabIndex        =   188
            Top             =   690
            Width           =   1215
         End
         Begin VB.ComboBox cboTpCorretor 
            Height          =   315
            ItemData        =   "frmEndosso.frx":02E1
            Left            =   5010
            List            =   "frmEndosso.frx":02E3
            Style           =   2  'Dropdown List
            TabIndex        =   187
            Top             =   210
            Visible         =   0   'False
            Width           =   1350
         End
         Begin VB.ComboBox cboSucursalCorretorCob 
            Height          =   315
            Left            =   6540
            Style           =   2  'Dropdown List
            TabIndex        =   186
            Top             =   210
            Visible         =   0   'False
            Width           =   945
         End
         Begin VB.ComboBox cboCorretorCob 
            Height          =   315
            ItemData        =   "frmEndosso.frx":02E5
            Left            =   870
            List            =   "frmEndosso.frx":02E7
            Style           =   2  'Dropdown List
            TabIndex        =   185
            Top             =   210
            Width           =   6615
         End
         Begin MSFlexGridLib.MSFlexGrid grdCorretagem 
            Height          =   1005
            Left            =   180
            TabIndex        =   191
            Top             =   570
            Width           =   9585
            _ExtentX        =   16907
            _ExtentY        =   1773
            _Version        =   393216
            Cols            =   5
            FixedCols       =   0
            FormatString    =   $"frmEndosso.frx":02E9
         End
         Begin Mask2S.ConMask2S mskPercCorretagemCob 
            Height          =   375
            Left            =   8760
            TabIndex        =   192
            Top             =   195
            Width           =   975
            _ExtentX        =   1720
            _ExtentY        =   661
            mask            =   ""
            text            =   "0,00"
            locked          =   0   'False
            enabled         =   -1  'True
         End
         Begin VB.Label Lbl 
            AutoSize        =   -1  'True
            Caption         =   "% Comiss�o:"
            Height          =   195
            Index           =   46
            Left            =   7830
            TabIndex        =   194
            Top             =   300
            Width           =   930
         End
         Begin VB.Label Lbl 
            AutoSize        =   -1  'True
            Caption         =   "Corretor:"
            Height          =   195
            Index           =   64
            Left            =   210
            TabIndex        =   193
            Top             =   270
            Width           =   645
         End
      End
      Begin VB.Frame fraEstipulante 
         Caption         =   "Estipulante"
         Height          =   1665
         Left            =   -74880
         TabIndex        =   175
         Top             =   3840
         Width           =   11625
         Begin VB.CommandButton cmdEstipulanteRemover 
            Caption         =   "Remover  >>"
            Enabled         =   0   'False
            Height          =   345
            Left            =   10080
            TabIndex        =   179
            Top             =   1125
            Width           =   1215
         End
         Begin VB.CommandButton cmdEstipulanteAdicionar 
            Caption         =   "<<  Adicionar"
            Enabled         =   0   'False
            Height          =   345
            Left            =   10080
            TabIndex        =   178
            Top             =   210
            Width           =   1215
         End
         Begin VB.CommandButton cmdEstipulanteAlterar 
            Caption         =   "<< Alterar >>"
            Enabled         =   0   'False
            Height          =   345
            Left            =   10080
            TabIndex        =   177
            Top             =   660
            Width           =   1215
         End
         Begin VB.ComboBox CboEstipulante 
            Height          =   315
            Left            =   720
            Style           =   2  'Dropdown List
            TabIndex        =   176
            Top             =   240
            Width           =   6765
         End
         Begin MSFlexGridLib.MSFlexGrid grdEstipulante 
            Height          =   945
            Left            =   210
            TabIndex        =   180
            Top             =   600
            Width           =   9555
            _ExtentX        =   16854
            _ExtentY        =   1667
            _Version        =   393216
            Cols            =   3
            FixedCols       =   0
            HighLight       =   2
            SelectionMode   =   1
            FormatString    =   $"frmEndosso.frx":039C
         End
         Begin Mask2S.ConMask2S mskPercProLabore 
            Height          =   405
            Left            =   8820
            TabIndex        =   181
            Top             =   150
            Width           =   915
            _ExtentX        =   1614
            _ExtentY        =   714
            mask            =   ""
            text            =   "0,00"
            locked          =   0   'False
            enabled         =   -1  'True
         End
         Begin VB.Label Lbl 
            AutoSize        =   -1  'True
            Caption         =   "Nome:"
            Height          =   195
            Index           =   1
            Left            =   210
            TabIndex        =   183
            Top             =   270
            Width           =   465
         End
         Begin VB.Label Lbl 
            AutoSize        =   -1  'True
            Caption         =   "% Estipulante:"
            Height          =   195
            Index           =   2
            Left            =   7800
            TabIndex        =   182
            Top             =   270
            Width           =   990
         End
      End
      Begin VB.CommandButton cmdSairRestituicao 
         Caption         =   "&Sair"
         Height          =   435
         Left            =   -66060
         TabIndex        =   174
         Top             =   8460
         Width           =   1365
      End
      Begin VB.CommandButton cmdAplicarRestituicao 
         Caption         =   "&Aplicar"
         Height          =   435
         Left            =   -64620
         TabIndex        =   173
         Top             =   8460
         Width           =   1365
      End
      Begin VB.Frame fraCambio 
         Height          =   885
         Left            =   -66870
         TabIndex        =   168
         Top             =   1260
         Width           =   3615
         Begin VB.ComboBox cboMoeda 
            Height          =   315
            Left            =   960
            Style           =   2  'Dropdown List
            TabIndex        =   169
            Top             =   180
            Width           =   2265
         End
         Begin Mask2S.ConMask2S mskCambio 
            Height          =   315
            Left            =   960
            TabIndex        =   170
            Top             =   510
            Width           =   1095
            _ExtentX        =   1931
            _ExtentY        =   556
            mask            =   ""
            text            =   "0,00"
            locked          =   0   'False
            enabled         =   -1  'True
         End
         Begin VB.Label Lbl 
            AutoSize        =   -1  'True
            Caption         =   "Moeda:"
            Height          =   195
            Index           =   85
            Left            =   240
            TabIndex        =   172
            Top             =   210
            Width           =   585
         End
         Begin VB.Label Lbl 
            AutoSize        =   -1  'True
            Caption         =   "C�mbio:"
            Height          =   225
            Index           =   39
            Left            =   240
            TabIndex        =   171
            Top             =   540
            Width           =   615
         End
      End
      Begin VB.Frame Frame5 
         Caption         =   "Cancelamento de Ap�lice"
         Height          =   7005
         Index           =   13
         Left            =   -74880
         TabIndex        =   163
         Top             =   1320
         Width           =   11625
         Begin VB.TextBox txtMotCancel 
            Height          =   285
            Left            =   2040
            MaxLength       =   60
            TabIndex        =   164
            Top             =   720
            Width           =   8895
         End
         Begin MSMask.MaskEdBox mskDtVigCan 
            Height          =   300
            Left            =   360
            TabIndex        =   165
            Top             =   690
            Width           =   1215
            _ExtentX        =   2143
            _ExtentY        =   529
            _Version        =   393216
            AutoTab         =   -1  'True
            MaxLength       =   10
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Mask            =   "##/##/####"
            PromptChar      =   "_"
         End
         Begin VB.Label Lbl 
            AutoSize        =   -1  'True
            Caption         =   "Motivo do Cancelamento:"
            Height          =   195
            Index           =   18
            Left            =   2040
            TabIndex        =   167
            Top             =   360
            Width           =   1815
         End
         Begin VB.Label Lbl 
            AutoSize        =   -1  'True
            Caption         =   "Final de Vig�ncia:"
            Height          =   195
            Index           =   17
            Left            =   360
            TabIndex        =   166
            Top             =   360
            Width           =   1305
         End
      End
      Begin VB.CommandButton cmdAplicarCancApolice 
         Caption         =   "&Aplicar"
         Height          =   435
         Left            =   -64560
         TabIndex        =   162
         Top             =   8430
         Width           =   1365
      End
      Begin VB.CommandButton cmdSairCancApolice 
         Caption         =   "&Sair"
         Height          =   435
         Left            =   -66060
         TabIndex        =   161
         Top             =   8430
         Width           =   1365
      End
      Begin VB.Frame Frame5 
         Height          =   885
         Index           =   9
         Left            =   -74880
         TabIndex        =   157
         Top             =   1260
         Width           =   7845
         Begin VB.OptionButton optCobSem 
            Caption         =   "Sem Cobran�a / Restitui��o"
            Height          =   255
            Left            =   2520
            TabIndex        =   160
            Top             =   390
            Width           =   2415
         End
         Begin VB.OptionButton optCobRes 
            Caption         =   "Restitui��o"
            Height          =   375
            Left            =   5640
            TabIndex        =   159
            Top             =   330
            Width           =   1125
         End
         Begin VB.OptionButton optCobAdi 
            Caption         =   "Cobran�a"
            Height          =   375
            Left            =   540
            TabIndex        =   158
            Top             =   360
            Width           =   1005
         End
      End
      Begin VB.Frame Frame5 
         Caption         =   "Cancelamento de Endosso"
         Height          =   7050
         Index           =   14
         Left            =   -74880
         TabIndex        =   150
         Top             =   1320
         Width           =   11655
         Begin VB.TextBox TxtNumEndossoCanc 
            Alignment       =   2  'Center
            Height          =   315
            Left            =   8160
            MaxLength       =   11
            MultiLine       =   -1  'True
            TabIndex        =   152
            Top             =   1680
            Width           =   615
         End
         Begin VB.CheckBox chkEndossoNaLider 
            Caption         =   "N�mero do endosso na l�der"
            Height          =   255
            Left            =   3000
            TabIndex        =   151
            Top             =   1020
            Width           =   2415
         End
         Begin MSMask.MaskEdBox mskDtVigCanEndosso 
            Height          =   300
            Left            =   3000
            TabIndex        =   153
            Top             =   2670
            Width           =   1215
            _ExtentX        =   2143
            _ExtentY        =   529
            _Version        =   393216
            AutoTab         =   -1  'True
            MaxLength       =   10
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Mask            =   "##/##/####"
            PromptChar      =   "_"
         End
         Begin VB.Label Lbl 
            AutoSize        =   -1  'True
            Caption         =   "Confirma o cancelamento do endosso N�"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   12
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   32
            Left            =   3000
            TabIndex        =   156
            Top             =   1680
            Width           =   4965
         End
         Begin VB.Label Lbl 
            AutoSize        =   -1  'True
            Caption         =   "Data Cancelamento:"
            Height          =   195
            Index           =   71
            Left            =   3000
            TabIndex        =   155
            Top             =   2400
            Width           =   1500
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "?"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   12
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Left            =   9000
            TabIndex        =   154
            Top             =   1680
            Width           =   165
         End
      End
      Begin VB.CommandButton cmdSairCancEndosso 
         Caption         =   "&Sair"
         Height          =   435
         Left            =   -66060
         TabIndex        =   149
         Top             =   8430
         Width           =   1365
      End
      Begin VB.CommandButton cmdAplicarCancEndosso 
         Caption         =   "&Aplicar"
         Height          =   435
         Left            =   -64590
         TabIndex        =   148
         Top             =   8430
         Width           =   1365
      End
      Begin VB.Frame Frame5 
         Caption         =   "Atual"
         Height          =   2955
         Index           =   15
         Left            =   -74550
         TabIndex        =   116
         Top             =   1410
         Width           =   11160
         Begin VB.TextBox txtforma_pgto_Ant 
            BackColor       =   &H80000004&
            Enabled         =   0   'False
            Height          =   285
            Index           =   1
            Left            =   4320
            TabIndex        =   573
            Top             =   1890
            Width           =   3345
         End
         Begin VB.TextBox txtforma_pgto_Ant 
            BackColor       =   &H80000004&
            Enabled         =   0   'False
            Height          =   285
            Index           =   0
            Left            =   3105
            TabIndex        =   572
            Top             =   1890
            Width           =   1050
         End
         Begin VB.CheckBox chkPPE 
            Caption         =   "PPE"
            Height          =   240
            Left            =   10200
            TabIndex        =   378
            Top             =   300
            Width           =   690
         End
         Begin VB.TextBox txtEstCivilDescAnt 
            BackColor       =   &H80000004&
            Enabled         =   0   'False
            Height          =   285
            Left            =   7110
            Locked          =   -1  'True
            TabIndex        =   133
            Top             =   840
            Width           =   1545
         End
         Begin VB.TextBox txtTpPessoaAnt 
            BackColor       =   &H80000004&
            Height          =   285
            Left            =   270
            Locked          =   -1  'True
            TabIndex        =   132
            Top             =   840
            Width           =   1335
         End
         Begin VB.TextBox txtCPFCNPJAnt 
            BackColor       =   &H80000004&
            Height          =   285
            Left            =   1830
            Locked          =   -1  'True
            TabIndex        =   131
            Top             =   840
            Width           =   2115
         End
         Begin VB.TextBox txtCodCliAnt 
            BackColor       =   &H80000003&
            Enabled         =   0   'False
            Height          =   285
            Left            =   1380
            Locked          =   -1  'True
            TabIndex        =   130
            Top             =   270
            Width           =   1335
         End
         Begin VB.TextBox TxtTelefoneAnt 
            BackColor       =   &H80000004&
            Height          =   285
            Left            =   9270
            Locked          =   -1  'True
            MaxLength       =   9
            TabIndex        =   129
            Top             =   1350
            Width           =   1215
         End
         Begin VB.TextBox TxtDDDAnt 
            BackColor       =   &H80000004&
            Height          =   285
            Left            =   7110
            Locked          =   -1  'True
            MaxLength       =   4
            TabIndex        =   128
            Top             =   1350
            Width           =   615
         End
         Begin VB.TextBox txtNomeAnt 
            BackColor       =   &H80000004&
            Height          =   285
            Left            =   3780
            Locked          =   -1  'True
            MaxLength       =   60
            TabIndex        =   127
            Top             =   270
            Width           =   6195
         End
         Begin VB.TextBox TxtCodAgAnt 
            BackColor       =   &H80000004&
            Height          =   285
            Left            =   5220
            Locked          =   -1  'True
            MaxLength       =   5
            TabIndex        =   126
            Top             =   2400
            Width           =   615
         End
         Begin VB.TextBox txtEndoCad 
            BackColor       =   &H80000004&
            Height          =   285
            Index           =   8
            Left            =   960
            Locked          =   -1  'True
            MaxLength       =   3
            TabIndex        =   125
            Top             =   2400
            Width           =   615
         End
         Begin VB.TextBox TxtCCAnt 
            BackColor       =   &H80000004&
            Height          =   285
            Left            =   9270
            Locked          =   -1  'True
            MaxLength       =   11
            TabIndex        =   124
            Top             =   2400
            Width           =   1605
         End
         Begin VB.TextBox TxtNomeAgAnt 
            BackColor       =   &H80000004&
            Enabled         =   0   'False
            Height          =   285
            Left            =   5910
            Locked          =   -1  'True
            TabIndex        =   123
            Top             =   2400
            Width           =   2415
         End
         Begin VB.TextBox txtEndoCad 
            BackColor       =   &H80000004&
            Enabled         =   0   'False
            Height          =   285
            Index           =   9
            Left            =   1620
            Locked          =   -1  'True
            TabIndex        =   122
            Top             =   2400
            Width           =   2535
         End
         Begin VB.TextBox txtIdTpPessoaAnt 
            Height          =   285
            Left            =   270
            Locked          =   -1  'True
            MaxLength       =   3
            TabIndex        =   121
            Top             =   840
            Visible         =   0   'False
            Width           =   315
         End
         Begin VB.TextBox txtSexoDescAnt 
            BackColor       =   &H8000000F&
            Height          =   285
            Left            =   4170
            Locked          =   -1  'True
            MaxLength       =   10
            TabIndex        =   120
            Top             =   840
            Width           =   1635
         End
         Begin VB.TextBox txtEstCivilAnt 
            Height          =   285
            Left            =   7110
            Locked          =   -1  'True
            MaxLength       =   2
            TabIndex        =   119
            Top             =   840
            Visible         =   0   'False
            Width           =   375
         End
         Begin VB.TextBox txtSexoAnt 
            Height          =   285
            Left            =   4170
            Locked          =   -1  'True
            MaxLength       =   1
            TabIndex        =   118
            Top             =   840
            Visible         =   0   'False
            Width           =   375
         End
         Begin VB.TextBox TxtEmailAnt 
            BackColor       =   &H80000004&
            Height          =   285
            Left            =   270
            Locked          =   -1  'True
            MaxLength       =   60
            TabIndex        =   117
            Top             =   1350
            Width           =   5535
         End
         Begin MSMask.MaskEdBox txtNascAnt 
            Height          =   285
            Left            =   9270
            TabIndex        =   134
            Top             =   840
            Width           =   1215
            _ExtentX        =   2143
            _ExtentY        =   503
            _Version        =   393216
            BackColor       =   -2147483644
            AutoTab         =   -1  'True
            Enabled         =   0   'False
            MaxLength       =   10
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Mask            =   "##/##/####"
            PromptChar      =   "_"
         End
         Begin VB.Label lblCad 
            AutoSize        =   -1  'True
            Caption         =   "Forma de Pagamento:"
            Height          =   195
            Index           =   36
            Left            =   1350
            TabIndex        =   570
            Top             =   1935
            Width           =   1590
         End
         Begin VB.Label lblCad 
            AutoSize        =   -1  'True
            Caption         =   "C�d. Cliente :"
            Height          =   195
            Index           =   0
            Left            =   270
            TabIndex        =   147
            Top             =   300
            Width           =   945
         End
         Begin VB.Label lblCad 
            AutoSize        =   -1  'True
            Caption         =   "Tipo Pessoa :"
            Height          =   195
            Index           =   2
            Left            =   270
            TabIndex        =   146
            Top             =   600
            Width           =   975
         End
         Begin VB.Label lblCad 
            AutoSize        =   -1  'True
            Caption         =   "Telefone :"
            Height          =   195
            Index           =   6
            Left            =   9270
            TabIndex        =   145
            Top             =   1140
            Width           =   720
         End
         Begin VB.Label lblCad 
            AutoSize        =   -1  'True
            Caption         =   "DDD :"
            Height          =   195
            Index           =   5
            Left            =   7110
            TabIndex        =   144
            Top             =   1140
            Width           =   450
         End
         Begin VB.Label lblCad 
            AutoSize        =   -1  'True
            Caption         =   "Data do Nascimento :"
            Height          =   195
            Index           =   30
            Left            =   9270
            TabIndex        =   143
            Top             =   600
            Width           =   1545
         End
         Begin VB.Label lblCad 
            AutoSize        =   -1  'True
            Caption         =   "CPF / CNPJ :"
            Height          =   195
            Index           =   27
            Left            =   1830
            TabIndex        =   142
            Top             =   600
            Width           =   960
         End
         Begin VB.Label lblCad 
            AutoSize        =   -1  'True
            Caption         =   "Nome :"
            Height          =   195
            Index           =   1
            Left            =   3180
            TabIndex        =   141
            Top             =   300
            Width           =   510
         End
         Begin VB.Label lblCad 
            AutoSize        =   -1  'True
            Caption         =   "Banco :"
            Height          =   195
            Index           =   10
            Left            =   270
            TabIndex        =   140
            Top             =   2430
            Width           =   555
         End
         Begin VB.Label lblCad 
            AutoSize        =   -1  'True
            Caption         =   "Conta :"
            Height          =   195
            Index           =   12
            Left            =   8670
            TabIndex        =   139
            Top             =   2430
            Width           =   510
         End
         Begin VB.Label lblCad 
            AutoSize        =   -1  'True
            Caption         =   "Ag. D�bito :"
            Height          =   195
            Index           =   11
            Left            =   4320
            TabIndex        =   138
            Top             =   2430
            Width           =   840
         End
         Begin VB.Label lblCad 
            AutoSize        =   -1  'True
            Caption         =   "Sexo :"
            Height          =   195
            Index           =   28
            Left            =   4170
            TabIndex        =   137
            Top             =   600
            Width           =   450
         End
         Begin VB.Label lblCad 
            AutoSize        =   -1  'True
            Caption         =   "Estado Civil :"
            Height          =   195
            Index           =   29
            Left            =   7110
            TabIndex        =   136
            Top             =   600
            Width           =   915
         End
         Begin VB.Label lblCad 
            AutoSize        =   -1  'True
            Caption         =   "E-mail :"
            Height          =   195
            Index           =   26
            Left            =   270
            TabIndex        =   135
            Top             =   1140
            Width           =   510
         End
      End
      Begin VB.Frame Frame5 
         Caption         =   "Endosso "
         Height          =   2955
         Index           =   16
         Left            =   -74580
         TabIndex        =   88
         Top             =   4800
         Width           =   11160
         Begin VB.ComboBox cboFormaPagamento 
            Height          =   315
            ItemData        =   "frmEndosso.frx":0440
            Left            =   3015
            List            =   "frmEndosso.frx":0442
            Style           =   2  'Dropdown List
            TabIndex        =   576
            Top             =   1980
            Visible         =   0   'False
            Width           =   4200
         End
         Begin VB.TextBox txtforma_pgto 
            BackColor       =   &H80000004&
            Height          =   285
            Index           =   1
            Left            =   4275
            Locked          =   -1  'True
            TabIndex        =   575
            Top             =   1980
            Width           =   3345
         End
         Begin VB.TextBox txtforma_pgto 
            BackColor       =   &H80000004&
            Height          =   285
            Index           =   0
            Left            =   3060
            Locked          =   -1  'True
            TabIndex        =   574
            Top             =   1980
            Width           =   1050
         End
         Begin VB.ComboBox cboTpPessoa 
            Height          =   315
            ItemData        =   "frmEndosso.frx":0444
            Left            =   300
            List            =   "frmEndosso.frx":044E
            Style           =   2  'Dropdown List
            TabIndex        =   101
            Top             =   930
            Width           =   1335
         End
         Begin VB.TextBox txtEndoCad 
            Height          =   285
            Index           =   0
            Left            =   870
            MaxLength       =   60
            TabIndex        =   100
            Top             =   300
            Width           =   7815
         End
         Begin VB.TextBox txtEndoCad 
            Height          =   285
            Index           =   2
            Left            =   7110
            MaxLength       =   4
            TabIndex        =   99
            Top             =   1470
            Width           =   585
         End
         Begin VB.TextBox txtEndoCad 
            Height          =   285
            Index           =   3
            Left            =   9270
            MaxLength       =   9
            TabIndex        =   98
            Top             =   1470
            Width           =   1215
         End
         Begin VB.TextBox txtCodAgDebito 
            BackColor       =   &H80000004&
            Height          =   285
            Index           =   0
            Left            =   5205
            MaxLength       =   5
            TabIndex        =   97
            Top             =   2355
            Width           =   615
         End
         Begin VB.TextBox txtBanco 
            BackColor       =   &H80000004&
            Height          =   285
            Left            =   915
            Locked          =   -1  'True
            MaxLength       =   3
            TabIndex        =   96
            Top             =   2385
            Width           =   615
         End
         Begin VB.TextBox txtEndoCad 
            BackColor       =   &H80000004&
            Height          =   285
            Index           =   7
            Left            =   9225
            MaxLength       =   11
            TabIndex        =   95
            Top             =   2385
            Width           =   1605
         End
         Begin VB.TextBox txtCodAgDebito 
            BackColor       =   &H80000004&
            Enabled         =   0   'False
            Height          =   285
            Index           =   1
            Left            =   5895
            Locked          =   -1  'True
            TabIndex        =   94
            Top             =   2385
            Width           =   2415
         End
         Begin VB.TextBox txtEndoCad 
            BackColor       =   &H80000004&
            Enabled         =   0   'False
            Height          =   285
            Index           =   6
            Left            =   1575
            Locked          =   -1  'True
            TabIndex        =   93
            Top             =   2385
            Width           =   2535
         End
         Begin VB.CommandButton cmdRestaurar 
            Caption         =   "Restaurar"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   405
            Left            =   9660
            TabIndex        =   92
            Top             =   240
            Width           =   1215
         End
         Begin VB.ComboBox cboSexo 
            Height          =   315
            ItemData        =   "frmEndosso.frx":0464
            Left            =   4200
            List            =   "frmEndosso.frx":0471
            Style           =   2  'Dropdown List
            TabIndex        =   91
            Top             =   900
            Width           =   1635
         End
         Begin VB.ComboBox cboEstCivil 
            Height          =   315
            ItemData        =   "frmEndosso.frx":0499
            Left            =   7110
            List            =   "frmEndosso.frx":04AF
            Style           =   2  'Dropdown List
            TabIndex        =   90
            Top             =   900
            Width           =   1875
         End
         Begin VB.TextBox txtEndoCad 
            Height          =   285
            Index           =   1
            Left            =   300
            MaxLength       =   60
            TabIndex        =   89
            Top             =   1470
            Width           =   5535
         End
         Begin MSMask.MaskEdBox txtNascimento 
            Height          =   285
            Left            =   9270
            TabIndex        =   102
            Top             =   930
            Width           =   1215
            _ExtentX        =   2143
            _ExtentY        =   503
            _Version        =   393216
            AutoTab         =   -1  'True
            MaxLength       =   10
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Mask            =   "##/##/####"
            PromptChar      =   "_"
         End
         Begin MSMask.MaskEdBox txtCPFCNPJ 
            Height          =   285
            Left            =   1830
            TabIndex        =   103
            Top             =   930
            Width           =   2115
            _ExtentX        =   3731
            _ExtentY        =   503
            _Version        =   393216
            AutoTab         =   -1  'True
            MaxLength       =   18
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Format          =   "##.###.###/####-##"
            Mask            =   "##.###.###/####-##"
            PromptChar      =   "_"
         End
         Begin VB.Label lblCad 
            AutoSize        =   -1  'True
            Caption         =   "Forma de Pagamento:"
            Height          =   195
            Index           =   37
            Left            =   1350
            TabIndex        =   571
            Top             =   2025
            Width           =   1590
         End
         Begin VB.Label lblCad 
            AutoSize        =   -1  'True
            Caption         =   "Nome :"
            Height          =   195
            Index           =   14
            Left            =   300
            TabIndex        =   115
            Top             =   330
            Width           =   510
         End
         Begin VB.Label lblCad 
            AutoSize        =   -1  'True
            Caption         =   "CPF / CNPJ :"
            Height          =   195
            Index           =   31
            Left            =   1830
            TabIndex        =   114
            Top             =   720
            Width           =   960
         End
         Begin VB.Label lblCad 
            AutoSize        =   -1  'True
            Caption         =   "Data do Nascimento :"
            Height          =   195
            Index           =   34
            Left            =   9285
            TabIndex        =   113
            Top             =   690
            Width           =   1545
         End
         Begin VB.Label lblCad 
            AutoSize        =   -1  'True
            Caption         =   "DDD :"
            Height          =   195
            Index           =   18
            Left            =   7110
            TabIndex        =   112
            Top             =   1245
            Width           =   450
         End
         Begin VB.Label lblCad 
            AutoSize        =   -1  'True
            Caption         =   "Telefone :"
            Height          =   195
            Index           =   19
            Left            =   9300
            TabIndex        =   111
            Top             =   1245
            Width           =   720
         End
         Begin VB.Label lblCad 
            AutoSize        =   -1  'True
            Caption         =   "Tipo Pessoa :"
            Height          =   195
            Index           =   22
            Left            =   285
            TabIndex        =   110
            Top             =   720
            Width           =   975
         End
         Begin VB.Label lblCad 
            AutoSize        =   -1  'True
            Caption         =   "Banco :"
            Height          =   195
            Index           =   23
            Left            =   285
            TabIndex        =   109
            Top             =   2415
            Width           =   555
         End
         Begin VB.Label lblCad 
            AutoSize        =   -1  'True
            Caption         =   "Conta :"
            Height          =   195
            Index           =   25
            Left            =   8655
            TabIndex        =   108
            Top             =   2445
            Width           =   510
         End
         Begin VB.Label lblCad 
            AutoSize        =   -1  'True
            Caption         =   "Ag. D�bito :"
            Height          =   195
            Index           =   24
            Left            =   4275
            TabIndex        =   107
            Top             =   2415
            Width           =   840
         End
         Begin VB.Label lblCad 
            AutoSize        =   -1  'True
            Caption         =   "Sexo :"
            Height          =   195
            Index           =   32
            Left            =   4230
            TabIndex        =   106
            Top             =   690
            Width           =   450
         End
         Begin VB.Label lblCad 
            AutoSize        =   -1  'True
            Caption         =   "Estado Civil :"
            Height          =   195
            Index           =   33
            Left            =   7110
            TabIndex        =   105
            Top             =   690
            Width           =   915
         End
         Begin VB.Label lblCad 
            AutoSize        =   -1  'True
            Caption         =   "E-mail :"
            Height          =   195
            Index           =   13
            Left            =   300
            TabIndex        =   104
            Top             =   1260
            Width           =   510
         End
      End
      Begin VB.CommandButton cmdAplicarCadastral 
         Caption         =   "&Aplicar"
         Height          =   435
         Left            =   -64680
         TabIndex        =   87
         Top             =   8430
         Width           =   1365
      End
      Begin VB.CommandButton cmdSairCadastral 
         Caption         =   "&Sair"
         Height          =   435
         Left            =   -66150
         TabIndex        =   86
         Top             =   8430
         Width           =   1365
      End
      Begin VB.Frame Frame5 
         Height          =   7035
         Index           =   17
         Left            =   -74820
         TabIndex        =   72
         Top             =   1320
         Width           =   11505
         Begin VB.TextBox txtEstipulante 
            Height          =   300
            Left            =   780
            Locked          =   -1  'True
            MaxLength       =   7
            TabIndex        =   82
            Top             =   1500
            Width           =   945
         End
         Begin VB.CommandButton cmdRestaurarEst 
            Caption         =   "Restaurar"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   435
            Left            =   9600
            TabIndex        =   81
            Top             =   1380
            Width           =   1215
         End
         Begin VB.CommandButton cmdEstRemover 
            Caption         =   "Remover  >>"
            Enabled         =   0   'False
            Height          =   345
            Left            =   9600
            TabIndex        =   78
            Top             =   2730
            Width           =   1215
         End
         Begin VB.CommandButton cmdEstAdicionar 
            Caption         =   "<<  Adicionar"
            Enabled         =   0   'False
            Height          =   345
            Left            =   9600
            TabIndex        =   77
            Top             =   1950
            Width           =   1215
         End
         Begin VB.CommandButton cmdEstAlterar 
            Caption         =   "<< Alterar >>"
            Enabled         =   0   'False
            Height          =   345
            Left            =   9600
            TabIndex        =   76
            Top             =   2340
            Width           =   1215
         End
         Begin VB.TextBox txtNomeEstipulante 
            BackColor       =   &H80000004&
            Enabled         =   0   'False
            Height          =   300
            Index           =   0
            Left            =   2820
            Locked          =   -1  'True
            TabIndex        =   75
            Top             =   1500
            Width           =   5355
         End
         Begin VB.CommandButton cmdConCliente 
            Caption         =   "Cliente"
            Height          =   300
            Left            =   1830
            TabIndex        =   74
            Top             =   1500
            Width           =   915
         End
         Begin VB.ComboBox cboSubGrupoEst 
            Height          =   315
            Left            =   1830
            Style           =   2  'Dropdown List
            TabIndex        =   73
            Top             =   660
            Width           =   7515
         End
         Begin Mask2S.ConMask2S mskPercEstipulante 
            Height          =   405
            Left            =   8280
            TabIndex        =   79
            Top             =   1440
            Width           =   1035
            _ExtentX        =   1826
            _ExtentY        =   714
            mask            =   ""
            text            =   "0,00000"
            locked          =   0   'False
            enabled         =   -1  'True
         End
         Begin MSFlexGridLib.MSFlexGrid grdEstipulanteEst 
            Height          =   1515
            Left            =   750
            TabIndex        =   80
            Top             =   1920
            Width           =   8625
            _ExtentX        =   15214
            _ExtentY        =   2672
            _Version        =   393216
            Cols            =   4
            FixedCols       =   0
            HighLight       =   2
            SelectionMode   =   1
            FormatString    =   $"frmEndosso.frx":04F3
         End
         Begin VB.Label lblEstipulante 
            AutoSize        =   -1  'True
            Caption         =   "Percentual :"
            Height          =   255
            Index           =   2
            Left            =   8310
            TabIndex        =   85
            Top             =   1260
            Width           =   855
         End
         Begin VB.Label lblEstipulante 
            AutoSize        =   -1  'True
            Caption         =   "Estipulante :"
            Height          =   195
            Index           =   1
            Left            =   780
            TabIndex        =   84
            Top             =   1260
            Width           =   870
         End
         Begin VB.Label lblEstipulante 
            AutoSize        =   -1  'True
            Caption         =   "SubGrupo :"
            Height          =   195
            Index           =   0
            Left            =   780
            TabIndex        =   83
            Top             =   720
            Width           =   810
         End
      End
      Begin VB.CommandButton cmdSairEst 
         Caption         =   "&Sair"
         Height          =   435
         Left            =   -66120
         TabIndex        =   71
         Top             =   8460
         Width           =   1365
      End
      Begin VB.CommandButton cmdAplicarEst 
         Caption         =   "&Aplicar"
         Height          =   435
         Left            =   -64680
         TabIndex        =   70
         Top             =   8460
         Width           =   1365
      End
      Begin VB.CommandButton cmdSairCorretagem 
         Caption         =   "Sair"
         Height          =   435
         Left            =   -66120
         TabIndex        =   69
         Top             =   8400
         Width           =   1335
      End
      Begin VB.CommandButton CmdOkCor 
         Caption         =   "&Aplicar"
         Height          =   435
         Left            =   -64680
         TabIndex        =   68
         Top             =   8400
         Width           =   1335
      End
      Begin VB.Frame Frame5 
         Height          =   6975
         Index           =   18
         Left            =   -74760
         TabIndex        =   52
         Top             =   1320
         Width           =   11385
         Begin VB.ComboBox CboSubGrupo 
            Height          =   315
            Left            =   1350
            Style           =   2  'Dropdown List
            TabIndex        =   62
            Top             =   270
            Width           =   3315
         End
         Begin VB.TextBox TxtTpCorretor 
            Height          =   285
            Left            =   4230
            MaxLength       =   1
            TabIndex        =   61
            TabStop         =   0   'False
            Text            =   "T"
            Top             =   780
            Visible         =   0   'False
            Width           =   1065
         End
         Begin VB.TextBox TxtSucCorretor 
            Height          =   285
            Left            =   2940
            MaxLength       =   4
            TabIndex        =   60
            TabStop         =   0   'False
            Text            =   "Suc"
            Top             =   780
            Visible         =   0   'False
            Width           =   1065
         End
         Begin VB.CommandButton CmdConCorretor 
            Caption         =   "Corretor"
            Height          =   300
            Left            =   1350
            TabIndex        =   59
            Top             =   765
            Width           =   1215
         End
         Begin VB.TextBox TxtCodCorretor 
            Height          =   300
            Index           =   0
            Left            =   210
            Locked          =   -1  'True
            MaxLength       =   9
            TabIndex        =   58
            Top             =   1155
            Width           =   1065
         End
         Begin VB.TextBox TxtCodCorretor 
            BackColor       =   &H80000004&
            Enabled         =   0   'False
            Height          =   300
            Index           =   1
            Left            =   1350
            Locked          =   -1  'True
            TabIndex        =   57
            Top             =   1155
            Width           =   5175
         End
         Begin VB.CommandButton CmdRestaura 
            Caption         =   "Restaurar"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Left            =   10080
            TabIndex        =   56
            Top             =   330
            Width           =   1215
         End
         Begin VB.CommandButton cmdCorretorAlterar 
            Caption         =   "<< Alterar >>"
            Enabled         =   0   'False
            Height          =   375
            Left            =   10050
            TabIndex        =   55
            Top             =   2310
            Width           =   1215
         End
         Begin VB.CommandButton cmdCorretorAdicionar 
            Caption         =   "<<  Adicionar"
            Enabled         =   0   'False
            Height          =   375
            Left            =   10050
            TabIndex        =   54
            Top             =   1680
            Width           =   1215
         End
         Begin VB.CommandButton cmdCorretorRemover 
            Caption         =   "Remover  >>"
            Enabled         =   0   'False
            Height          =   375
            Left            =   10050
            TabIndex        =   53
            Top             =   2910
            Width           =   1215
         End
         Begin Mask2S.ConMask2S mskPercCorretagem 
            Height          =   375
            Left            =   6630
            TabIndex        =   63
            Top             =   1110
            Width           =   1035
            _ExtentX        =   1826
            _ExtentY        =   661
            mask            =   ""
            text            =   "0,00000"
            locked          =   0   'False
            enabled         =   -1  'True
         End
         Begin MSFlexGridLib.MSFlexGrid GridCorretagem 
            Height          =   1965
            Left            =   120
            TabIndex        =   64
            Top             =   1560
            Width           =   9825
            _ExtentX        =   17330
            _ExtentY        =   3466
            _Version        =   393216
            Cols            =   6
            FixedCols       =   0
            FocusRect       =   0
            HighLight       =   2
            SelectionMode   =   1
            AllowUserResizing=   1
            FormatString    =   $"frmEndosso.frx":0591
         End
         Begin VB.Label Lbl 
            AutoSize        =   -1  'True
            Caption         =   "SubGrupo :"
            Height          =   195
            Index           =   100
            Left            =   300
            TabIndex        =   67
            Top             =   300
            Width           =   810
         End
         Begin VB.Label Lbl 
            AutoSize        =   -1  'True
            Caption         =   "Corretor :"
            Height          =   195
            Index           =   101
            Left            =   330
            TabIndex        =   66
            Top             =   810
            Width           =   645
         End
         Begin VB.Label Lbl 
            AutoSize        =   -1  'True
            Caption         =   "Percentual :"
            Height          =   255
            Index           =   102
            Left            =   6780
            TabIndex        =   65
            Top             =   900
            Width           =   855
         End
      End
      Begin VB.Frame Frame5 
         Height          =   6975
         Index           =   19
         Left            =   -74760
         TabIndex        =   32
         Top             =   1320
         Width           =   11415
         Begin VB.CommandButton cmdRemoverBenef 
            Caption         =   "Remover  >>"
            Enabled         =   0   'False
            Height          =   375
            Left            =   10080
            TabIndex        =   42
            Top             =   2460
            Width           =   1215
         End
         Begin VB.CommandButton cmdAdicionarBenef 
            Caption         =   "<<  Adicionar"
            Height          =   375
            Left            =   10080
            TabIndex        =   41
            Top             =   1230
            Width           =   1215
         End
         Begin VB.CommandButton cmdAlterarBenef 
            Caption         =   "<< Alterar >>"
            Enabled         =   0   'False
            Height          =   375
            Left            =   10080
            TabIndex        =   40
            Top             =   1860
            Width           =   1215
         End
         Begin VB.CommandButton cmdRestaurarBenef 
            Caption         =   "Restaurar"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Left            =   10080
            TabIndex        =   39
            Top             =   600
            Width           =   1215
         End
         Begin VB.TextBox txtNomeBenef 
            Height          =   315
            Index           =   0
            Left            =   240
            TabIndex        =   38
            Top             =   4200
            Width           =   8655
         End
         Begin VB.TextBox txtNomeBenef 
            Height          =   315
            Index           =   1
            Left            =   240
            TabIndex        =   37
            Top             =   4920
            Width           =   8655
         End
         Begin VB.TextBox txtBairroBenef 
            Height          =   315
            Index           =   0
            Left            =   240
            TabIndex        =   36
            Top             =   5640
            Width           =   3375
         End
         Begin VB.TextBox txtBairroBenef 
            Height          =   315
            Index           =   1
            Left            =   240
            TabIndex        =   35
            Top             =   6360
            Width           =   3375
         End
         Begin VB.ComboBox cboCidadeBenef 
            Height          =   315
            Left            =   3840
            Style           =   2  'Dropdown List
            TabIndex        =   34
            Top             =   5640
            Width           =   4215
         End
         Begin VB.TextBox txtBairroBenef 
            Height          =   315
            Index           =   2
            Left            =   8280
            Locked          =   -1  'True
            TabIndex        =   33
            Top             =   5640
            Width           =   615
         End
         Begin MSFlexGridLib.MSFlexGrid grdBeneficiario 
            Height          =   2595
            Left            =   240
            TabIndex        =   43
            Top             =   1200
            Width           =   9705
            _ExtentX        =   17119
            _ExtentY        =   4577
            _Version        =   393216
            Cols            =   8
            FixedCols       =   0
            FocusRect       =   0
            HighLight       =   2
            SelectionMode   =   1
            AllowUserResizing=   1
            FormatString    =   $"frmEndosso.frx":0642
         End
         Begin MSMask.MaskEdBox txtCepBenef 
            Height          =   315
            Left            =   3840
            TabIndex        =   44
            Top             =   6360
            Width           =   1605
            _ExtentX        =   2831
            _ExtentY        =   556
            _Version        =   393216
            AutoTab         =   -1  'True
            MaxLength       =   10
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Format          =   "##.###-###"
            Mask            =   "##.###-###"
            PromptChar      =   "_"
         End
         Begin VB.Label lblBenef 
            AutoSize        =   -1  'True
            Caption         =   "Nome:"
            Height          =   195
            Index           =   0
            Left            =   240
            TabIndex        =   51
            Top             =   3960
            Width           =   465
         End
         Begin VB.Label lblBenef 
            AutoSize        =   -1  'True
            Caption         =   "CEP:"
            Height          =   195
            Index           =   2
            Left            =   3840
            TabIndex        =   50
            Top             =   6120
            Width           =   360
         End
         Begin VB.Label lblBenef 
            AutoSize        =   -1  'True
            Caption         =   "Pais:"
            Height          =   195
            Index           =   3
            Left            =   240
            TabIndex        =   49
            Top             =   6120
            Width           =   345
         End
         Begin VB.Label lblBenef 
            AutoSize        =   -1  'True
            Caption         =   "Estado:"
            Height          =   195
            Index           =   4
            Left            =   8280
            TabIndex        =   48
            Top             =   5400
            Width           =   540
         End
         Begin VB.Label lblBenef 
            AutoSize        =   -1  'True
            Caption         =   "Cidade:"
            Height          =   195
            Index           =   5
            Left            =   3840
            TabIndex        =   47
            Top             =   5400
            Width           =   540
         End
         Begin VB.Label lblBenef 
            AutoSize        =   -1  'True
            Caption         =   "Bairro:"
            Height          =   195
            Index           =   6
            Left            =   240
            TabIndex        =   46
            Top             =   5400
            Width           =   450
         End
         Begin VB.Label lblBenef 
            AutoSize        =   -1  'True
            Caption         =   "Endere�o:"
            Height          =   195
            Index           =   7
            Left            =   240
            TabIndex        =   45
            Top             =   4680
            Width           =   735
         End
      End
      Begin VB.CommandButton cmdAplicarBenef 
         Caption         =   "&Aplicar"
         Height          =   435
         Left            =   -64680
         TabIndex        =   31
         Top             =   8400
         Width           =   1335
      End
      Begin VB.CommandButton cmdSairBenef 
         Caption         =   "Sair"
         Height          =   435
         Left            =   -66120
         TabIndex        =   30
         Top             =   8400
         Width           =   1335
      End
      Begin VB.Frame Frame5 
         Caption         =   "Atual"
         Height          =   2145
         Index           =   20
         Left            =   -74640
         TabIndex        =   15
         Top             =   1440
         Width           =   11160
         Begin VB.TextBox TxtBairroAnt 
            Height          =   285
            Left            =   7950
            Locked          =   -1  'True
            MaxLength       =   50
            TabIndex        =   22
            Top             =   1050
            Width           =   2925
         End
         Begin VB.TextBox txtNomeEndereco 
            Height          =   285
            Left            =   3780
            Locked          =   -1  'True
            MaxLength       =   60
            TabIndex        =   21
            Top             =   270
            Width           =   7095
         End
         Begin VB.TextBox TxtEnderecoAnt 
            Height          =   285
            Left            =   270
            Locked          =   -1  'True
            MaxLength       =   50
            TabIndex        =   20
            Top             =   1050
            Width           =   7455
         End
         Begin VB.TextBox TxtCidadeAnt 
            Height          =   285
            Left            =   270
            Locked          =   -1  'True
            MaxLength       =   30
            TabIndex        =   19
            Top             =   1560
            Width           =   7470
         End
         Begin VB.TextBox TxtUFAnt 
            Height          =   285
            Index           =   0
            Left            =   7950
            Locked          =   -1  'True
            MaxLength       =   2
            TabIndex        =   18
            Top             =   1560
            Width           =   375
         End
         Begin VB.TextBox TxtCEPAnt 
            Height          =   285
            Left            =   9270
            Locked          =   -1  'True
            MaxLength       =   10
            TabIndex        =   17
            Top             =   1560
            Width           =   1125
         End
         Begin VB.TextBox txtCodCliEndereco 
            BackColor       =   &H80000003&
            Enabled         =   0   'False
            Height          =   285
            Left            =   1380
            Locked          =   -1  'True
            TabIndex        =   16
            Top             =   240
            Width           =   1335
         End
         Begin VB.Label lblCad 
            AutoSize        =   -1  'True
            Caption         =   "Bairro :"
            Height          =   195
            Index           =   3
            Left            =   7950
            TabIndex        =   29
            Top             =   840
            Width           =   495
         End
         Begin VB.Label lblCad 
            AutoSize        =   -1  'True
            Caption         =   "Nome :"
            Height          =   195
            Index           =   42
            Left            =   3180
            TabIndex        =   28
            Top             =   300
            Width           =   510
         End
         Begin VB.Label lblCad 
            AutoSize        =   -1  'True
            Caption         =   "Endere�o de Correspond�ncia :"
            Height          =   195
            Index           =   35
            Left            =   270
            TabIndex        =   27
            Top             =   840
            Width           =   2250
         End
         Begin VB.Label lblCad 
            AutoSize        =   -1  'True
            Caption         =   "Cidade :"
            Height          =   195
            Index           =   4
            Left            =   270
            TabIndex        =   26
            Top             =   1350
            Width           =   585
         End
         Begin VB.Label lblCad 
            AutoSize        =   -1  'True
            Caption         =   "UF :"
            Height          =   195
            Index           =   7
            Left            =   7980
            TabIndex        =   25
            Top             =   1350
            Width           =   300
         End
         Begin VB.Label lblCad 
            AutoSize        =   -1  'True
            Caption         =   "CEP :"
            Height          =   195
            Index           =   8
            Left            =   9270
            TabIndex        =   24
            Top             =   1350
            Width           =   405
         End
         Begin VB.Label lblCad 
            AutoSize        =   -1  'True
            Caption         =   "C�d. Cliente :"
            Height          =   195
            Index           =   52
            Left            =   270
            TabIndex        =   23
            Top             =   300
            Width           =   945
         End
      End
      Begin VB.Frame Frame5 
         Caption         =   "Endosso "
         Height          =   1995
         Index           =   21
         Left            =   -74640
         TabIndex        =   2
         Top             =   3720
         Width           =   11160
         Begin VB.CommandButton cmdPesquisarCEP 
            Caption         =   "..."
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   13.5
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   380
            Left            =   10455
            TabIndex        =   8
            Top             =   1350
            Width           =   440
         End
         Begin VB.TextBox txtEndoCad 
            Height          =   285
            Index           =   5
            Left            =   7920
            MaxLength       =   50
            TabIndex        =   7
            Top             =   900
            Width           =   2925
         End
         Begin VB.CommandButton cmdRestaurarEndereco 
            Caption         =   "Restaurar"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   405
            Left            =   9660
            TabIndex        =   6
            Top             =   240
            Width           =   1215
         End
         Begin VB.ComboBox cboCidade 
            Height          =   315
            ItemData        =   "frmEndosso.frx":076E
            Left            =   300
            List            =   "frmEndosso.frx":0778
            TabIndex        =   5
            Text            =   "cboCidade"
            Top             =   1410
            Width           =   7455
         End
         Begin VB.TextBox TxtUFAnt 
            Height          =   285
            Index           =   1
            Left            =   7950
            Locked          =   -1  'True
            MaxLength       =   2
            TabIndex        =   4
            Top             =   1440
            Width           =   375
         End
         Begin VB.TextBox txtEndoCad 
            Height          =   285
            Index           =   4
            Left            =   285
            MaxLength       =   50
            TabIndex        =   3
            Top             =   900
            Width           =   7455
         End
         Begin MSMask.MaskEdBox txtCep 
            Height          =   285
            Left            =   9270
            TabIndex        =   9
            Top             =   1440
            Width           =   1125
            _ExtentX        =   1984
            _ExtentY        =   503
            _Version        =   393216
            AutoTab         =   -1  'True
            MaxLength       =   10
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Format          =   "##.###-###"
            Mask            =   "##.###-###"
            PromptChar      =   "_"
         End
         Begin VB.Label lblCad 
            AutoSize        =   -1  'True
            Caption         =   "Bairro :"
            Height          =   195
            Index           =   16
            Left            =   7950
            TabIndex        =   14
            Top             =   690
            Width           =   495
         End
         Begin VB.Label lblCad 
            AutoSize        =   -1  'True
            Caption         =   "CEP :"
            Height          =   195
            Index           =   21
            Left            =   9300
            TabIndex        =   13
            Top             =   1230
            Width           =   405
         End
         Begin VB.Label lblCad 
            AutoSize        =   -1  'True
            Caption         =   "UF :"
            Height          =   195
            Index           =   20
            Left            =   7950
            TabIndex        =   12
            Top             =   1230
            Width           =   300
         End
         Begin VB.Label lblCad 
            AutoSize        =   -1  'True
            Caption         =   "Cidade :"
            Height          =   195
            Index           =   17
            Left            =   300
            TabIndex        =   11
            Top             =   1200
            Width           =   585
         End
         Begin VB.Label lblCad 
            AutoSize        =   -1  'True
            Caption         =   "Endere�o de Correspond�ncia :"
            Height          =   195
            Index           =   15
            Left            =   300
            TabIndex        =   10
            Top             =   690
            Width           =   2250
         End
      End
      Begin VB.Frame Frame5 
         Caption         =   "Situa��o Atual"
         Height          =   2535
         Index           =   1
         Left            =   -74760
         TabIndex        =   316
         Top             =   1320
         Width           =   11295
         Begin VB.ComboBox cboObjetoSegurado 
            Height          =   315
            Left            =   120
            Style           =   2  'Dropdown List
            TabIndex        =   317
            Top             =   480
            Width           =   1755
         End
         Begin MSFlexGridLib.MSFlexGrid grdCoberturasAtual 
            Height          =   1575
            Left            =   120
            TabIndex        =   318
            Top             =   840
            Width           =   10845
            _ExtentX        =   19129
            _ExtentY        =   2778
            _Version        =   393216
            Cols            =   11
            AllowBigSelection=   0   'False
            HighLight       =   0
            FormatString    =   $"frmEndosso.frx":07A4
         End
         Begin VB.Label Lbl 
            AutoSize        =   -1  'True
            Caption         =   "Objeto Segurado:"
            Height          =   195
            Index           =   11
            Left            =   120
            TabIndex        =   319
            Top             =   240
            Width           =   1290
         End
      End
      Begin VB.Frame Frame5 
         Caption         =   "Endosso"
         Height          =   3735
         Index           =   2
         Left            =   -74760
         TabIndex        =   320
         Top             =   3960
         Width           =   11295
         Begin VB.ComboBox cboCoberturaAfetada 
            Height          =   315
            Left            =   5970
            Style           =   2  'Dropdown List
            TabIndex        =   330
            Top             =   1800
            Width           =   5055
         End
         Begin VB.TextBox txtTextoFranquia 
            Height          =   285
            Left            =   5145
            MaxLength       =   35
            TabIndex        =   338
            Top             =   3255
            Width           =   5835
         End
         Begin VB.Frame Frame5 
            Caption         =   "Tipo de movimenta��o:"
            Height          =   735
            Index           =   3
            Left            =   135
            TabIndex        =   326
            Top             =   1530
            Width           =   5655
            Begin VB.OptionButton optRedIS 
               Caption         =   "Redu��o de IS"
               Height          =   255
               Left            =   1800
               TabIndex        =   328
               Top             =   360
               Width           =   1575
            End
            Begin VB.OptionButton optAumIS 
               Caption         =   "Aumento de IS"
               Height          =   255
               Left            =   120
               TabIndex        =   327
               Top             =   360
               Width           =   1575
            End
            Begin VB.OptionButton optNovCob 
               Caption         =   "Nova Cobertura"
               Height          =   255
               Left            =   3600
               TabIndex        =   329
               Top             =   360
               Width           =   1575
            End
         End
         Begin VB.CommandButton cmdBensRemover 
            Caption         =   "Excluir >>"
            Enabled         =   0   'False
            Height          =   375
            Left            =   9780
            TabIndex        =   324
            Top             =   1230
            Width           =   1215
         End
         Begin VB.CommandButton cmdBensAlterar 
            Caption         =   "<< Alterar >>"
            Enabled         =   0   'False
            Height          =   375
            Left            =   9780
            TabIndex        =   323
            Top             =   735
            Width           =   1215
         End
         Begin VB.CommandButton cmdBensAdicionar 
            Caption         =   "<<  Adicionar"
            Enabled         =   0   'False
            Height          =   375
            Left            =   9780
            TabIndex        =   322
            Top             =   240
            Width           =   1215
         End
         Begin MSFlexGridLib.MSFlexGrid grdCoberturas 
            Height          =   1215
            Left            =   120
            TabIndex        =   321
            Top             =   240
            Width           =   9375
            _ExtentX        =   16536
            _ExtentY        =   2143
            _Version        =   393216
            Cols            =   12
            AllowBigSelection=   0   'False
            HighLight       =   0
            FormatString    =   $"frmEndosso.frx":08B0
         End
         Begin Mask2S.ConMask2S mskPerDesc 
            Height          =   375
            Left            =   8760
            TabIndex        =   325
            Top             =   720
            Visible         =   0   'False
            Width           =   855
            _ExtentX        =   1508
            _ExtentY        =   661
            mask            =   ""
            text            =   "0,00"
            locked          =   0   'False
            enabled         =   -1  'True
         End
         Begin Mask2S.ConMask2S mskPremioTarifa 
            Height          =   375
            Left            =   9630
            TabIndex        =   335
            Top             =   2490
            Width           =   1335
            _ExtentX        =   2355
            _ExtentY        =   661
            mask            =   ""
            text            =   "0,00"
            locked          =   0   'False
            enabled         =   -1  'True
         End
         Begin Mask2S.ConMask2S mskImpSeg 
            Height          =   375
            Left            =   240
            TabIndex        =   331
            Top             =   2490
            Width           =   1785
            _ExtentX        =   3149
            _ExtentY        =   661
            mask            =   ""
            text            =   "0,00"
            locked          =   0   'False
            enabled         =   -1  'True
         End
         Begin Mask2S.ConMask2S mskTaxaTarifa 
            Height          =   375
            Left            =   5145
            TabIndex        =   333
            Top             =   2490
            Width           =   1455
            _ExtentX        =   2566
            _ExtentY        =   661
            mask            =   ""
            text            =   "0,00"
            locked          =   0   'False
            enabled         =   -1  'True
         End
         Begin Mask2S.ConMask2S mskLimFra 
            Height          =   375
            Left            =   255
            TabIndex        =   336
            Top             =   3210
            Width           =   1800
            _ExtentX        =   3175
            _ExtentY        =   661
            mask            =   ""
            text            =   "0,00"
            locked          =   0   'False
            enabled         =   -1  'True
         End
         Begin Mask2S.ConMask2S mskFatFranquia 
            Height          =   360
            Left            =   2820
            TabIndex        =   337
            Top             =   3210
            Width           =   1425
            _ExtentX        =   2514
            _ExtentY        =   635
            mask            =   ""
            text            =   "0,00"
            locked          =   0   'False
            enabled         =   -1  'True
         End
         Begin Mask2S.ConMask2S mskTaxaNet 
            Height          =   375
            Left            =   2850
            TabIndex        =   332
            Top             =   2490
            Width           =   1455
            _ExtentX        =   2566
            _ExtentY        =   661
            mask            =   ""
            text            =   "0,00"
            locked          =   0   'False
            enabled         =   -1  'True
         End
         Begin Mask2S.ConMask2S mskPremioNet 
            Height          =   375
            Left            =   7425
            TabIndex        =   334
            Top             =   2490
            Width           =   1335
            _ExtentX        =   2355
            _ExtentY        =   661
            mask            =   ""
            text            =   "0,00"
            locked          =   0   'False
            enabled         =   -1  'True
         End
         Begin VB.Label Lbl 
            AutoSize        =   -1  'True
            Caption         =   "Pr�mio Tarifa:"
            Height          =   195
            Index           =   27
            Left            =   9645
            TabIndex        =   356
            Top             =   2310
            Width           =   975
         End
         Begin VB.Label Lbl 
            AutoSize        =   -1  'True
            Caption         =   "Pr�mio Net:"
            Height          =   195
            Index           =   26
            Left            =   7440
            TabIndex        =   355
            Top             =   2325
            Width           =   825
         End
         Begin VB.Label Lbl 
            AutoSize        =   -1  'True
            Caption         =   "Lim. Min. Franquia:"
            Height          =   195
            Index           =   25
            Left            =   255
            TabIndex        =   354
            Top             =   3030
            Width           =   1335
         End
         Begin VB.Label Lbl 
            AutoSize        =   -1  'True
            Caption         =   "Taxa Tarifa:"
            Height          =   195
            Index           =   24
            Left            =   5145
            TabIndex        =   353
            Top             =   2310
            Width           =   855
         End
         Begin VB.Label Lbl 
            AutoSize        =   -1  'True
            Caption         =   "Cobertura:"
            Height          =   195
            Index           =   78
            Left            =   5970
            TabIndex        =   352
            Top             =   1560
            Width           =   735
         End
         Begin VB.Label Lbl 
            AutoSize        =   -1  'True
            Caption         =   "Imp. Segurada:"
            Height          =   195
            Index           =   23
            Left            =   240
            TabIndex        =   351
            Top             =   2310
            Width           =   1080
         End
         Begin VB.Label Lbl 
            AutoSize        =   -1  'True
            Caption         =   "Texto Franquia:"
            Height          =   195
            Index           =   77
            Left            =   5130
            TabIndex        =   350
            Top             =   3030
            Width           =   1155
         End
         Begin VB.Label Lbl 
            AutoSize        =   -1  'True
            Caption         =   "Fat. Franquia:"
            Height          =   165
            Index           =   76
            Left            =   2820
            TabIndex        =   349
            Top             =   3030
            Width           =   1215
            WordWrap        =   -1  'True
         End
         Begin VB.Label Lbl 
            AutoSize        =   -1  'True
            Caption         =   "Taxa Net:"
            Height          =   195
            Index           =   68
            Left            =   2805
            TabIndex        =   348
            Top             =   2310
            Width           =   705
         End
      End
      Begin VB.Frame FrameRestituicaoModular 
         Caption         =   "Restitui��o"
         Height          =   3015
         Left            =   -73440
         TabIndex        =   379
         Top             =   6360
         Width           =   11655
         Begin VB.TextBox txtProposta 
            Alignment       =   2  'Center
            BackColor       =   &H8000000F&
            Height          =   285
            Index           =   1
            Left            =   7695
            Locked          =   -1  'True
            TabIndex        =   427
            Top             =   160
            Width           =   1065
         End
         Begin VB.TextBox txtProposta 
            Alignment       =   2  'Center
            BackColor       =   &H8000000F&
            Height          =   285
            Index           =   0
            Left            =   1920
            Locked          =   -1  'True
            TabIndex        =   425
            Top             =   160
            Width           =   1185
         End
         Begin Mask2S.ConMask2S mskValPremioTarifaResCancelamento 
            Height          =   315
            Index           =   1
            Left            =   1920
            TabIndex        =   391
            Top             =   750
            Width           =   1785
            _ExtentX        =   3149
            _ExtentY        =   556
            mask            =   ""
            text            =   "0,00"
            locked          =   -1  'True
            enabled         =   -1  'True
         End
         Begin Mask2S.ConMask2S mskValPremioTarifaResCancelamento 
            Height          =   315
            Index           =   0
            Left            =   1920
            TabIndex        =   393
            Top             =   480
            Width           =   1785
            _ExtentX        =   3149
            _ExtentY        =   556
            mask            =   ""
            text            =   "0,00"
            locked          =   0   'False
            enabled         =   -1  'True
         End
         Begin Mask2S.ConMask2S mskValPremioTarifaResCancelamento 
            Height          =   315
            Index           =   2
            Left            =   1920
            TabIndex        =   394
            Top             =   1050
            Width           =   1785
            _ExtentX        =   3149
            _ExtentY        =   556
            mask            =   ""
            text            =   "0,00"
            locked          =   -1  'True
            enabled         =   -1  'True
         End
         Begin Mask2S.ConMask2S mskValPremioTarifaResCancelamento 
            Height          =   315
            Index           =   3
            Left            =   1920
            TabIndex        =   395
            Top             =   1350
            Width           =   1785
            _ExtentX        =   3149
            _ExtentY        =   556
            mask            =   ""
            text            =   "0,00"
            locked          =   -1  'True
            enabled         =   -1  'True
         End
         Begin Mask2S.ConMask2S mskValPremioTarifaResCancelamento 
            Height          =   315
            Index           =   4
            Left            =   1920
            TabIndex        =   396
            Top             =   1680
            Width           =   1785
            _ExtentX        =   3149
            _ExtentY        =   556
            mask            =   ""
            text            =   "0,00"
            locked          =   -1  'True
            enabled         =   -1  'True
         End
         Begin Mask2S.ConMask2S mskValPremioTarifaResCancelamento 
            Height          =   315
            Index           =   5
            Left            =   7680
            TabIndex        =   408
            Top             =   450
            Width           =   1785
            _ExtentX        =   3149
            _ExtentY        =   556
            mask            =   ""
            text            =   "0,00"
            locked          =   0   'False
            enabled         =   -1  'True
         End
         Begin Mask2S.ConMask2S mskValPremioTarifaResCancelamento 
            Height          =   315
            Index           =   6
            Left            =   7680
            TabIndex        =   409
            Top             =   750
            Width           =   1785
            _ExtentX        =   3149
            _ExtentY        =   556
            mask            =   ""
            text            =   "0,00"
            locked          =   -1  'True
            enabled         =   -1  'True
         End
         Begin Mask2S.ConMask2S mskValPremioTarifaResCancelamento 
            Height          =   315
            Index           =   8
            Left            =   7680
            TabIndex        =   410
            Top             =   1350
            Width           =   1785
            _ExtentX        =   3149
            _ExtentY        =   556
            mask            =   ""
            text            =   "0,00"
            locked          =   -1  'True
            enabled         =   -1  'True
         End
         Begin Mask2S.ConMask2S mskValPremioTarifaResCancelamento 
            Height          =   315
            Index           =   9
            Left            =   7680
            TabIndex        =   411
            Top             =   1680
            Width           =   1785
            _ExtentX        =   3149
            _ExtentY        =   556
            mask            =   ""
            text            =   "0,00"
            locked          =   -1  'True
            enabled         =   -1  'True
         End
         Begin Mask2S.ConMask2S mskValPremioTarifaResCancelamento 
            Height          =   315
            Index           =   7
            Left            =   7680
            TabIndex        =   418
            Top             =   1080
            Width           =   1785
            _ExtentX        =   3149
            _ExtentY        =   556
            mask            =   ""
            text            =   "0,00"
            locked          =   -1  'True
            enabled         =   -1  'True
         End
         Begin VB.Label lblTipoProposta 
            Height          =   255
            Index           =   1
            Left            =   9000
            TabIndex        =   429
            Top             =   175
            Width           =   975
         End
         Begin VB.Label lblTipoProposta 
            Height          =   255
            Index           =   0
            Left            =   3240
            TabIndex        =   428
            Top             =   175
            Width           =   975
         End
         Begin VB.Label lblLegendaEndosso 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Proposta"
            Height          =   195
            Index           =   17
            Left            =   6960
            TabIndex        =   426
            Top             =   960
            Width           =   630
         End
         Begin VB.Label lblLegendaEndosso 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Proposta"
            Height          =   195
            Index           =   16
            Left            =   1080
            TabIndex        =   424
            Top             =   165
            Width           =   630
         End
         Begin VB.Label lblvalcorretagemprolabore 
            Alignment       =   1  'Right Justify
            BorderStyle     =   1  'Fixed Single
            Caption         =   "0,00"
            Height          =   255
            Index           =   3
            Left            =   8940
            TabIndex        =   423
            Top             =   2550
            Width           =   945
         End
         Begin VB.Label lblvalcorretagemprolabore 
            Alignment       =   1  'Right Justify
            BorderStyle     =   1  'Fixed Single
            Caption         =   "0,00"
            Height          =   255
            Index           =   2
            Left            =   6960
            TabIndex        =   422
            Top             =   2520
            Width           =   945
         End
         Begin VB.Label Lbl 
            AutoSize        =   -1  'True
            Caption         =   "Pr�-Labore:"
            Height          =   195
            Index           =   38
            Left            =   8070
            TabIndex        =   421
            Top             =   2565
            Width           =   825
         End
         Begin VB.Label Lbl 
            AutoSize        =   -1  'True
            Caption         =   "Corretagem:"
            Height          =   195
            Index           =   37
            Left            =   6000
            TabIndex        =   420
            Top             =   2550
            Width           =   855
         End
         Begin VB.Label lbl_sinal 
            Caption         =   "+"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   15
            Left            =   9480
            TabIndex        =   419
            Top             =   1065
            Width           =   195
         End
         Begin VB.Label lbl_sinal 
            Caption         =   "+"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   17
            Left            =   9480
            TabIndex        =   417
            Top             =   465
            Width           =   195
         End
         Begin VB.Label lbl_sinal 
            Caption         =   "+"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   16
            Left            =   9480
            TabIndex        =   416
            Top             =   765
            Width           =   195
         End
         Begin VB.Label lbl_sinal 
            Caption         =   "+"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   14
            Left            =   9480
            TabIndex        =   415
            Top             =   1665
            Width           =   195
         End
         Begin VB.Label lbl_sinal 
            Caption         =   "-"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   13
            Left            =   9540
            TabIndex        =   414
            Top             =   1365
            Width           =   135
         End
         Begin VB.Label lbl_sinal 
            Caption         =   "="
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   12
            Left            =   9480
            TabIndex        =   413
            Top             =   2085
            Width           =   195
         End
         Begin VB.Label lblValRestituicaoResCancelamento 
            Alignment       =   1  'Right Justify
            BorderStyle     =   1  'Fixed Single
            Caption         =   "0,00"
            Height          =   255
            Index           =   1
            Left            =   7680
            TabIndex        =   412
            Top             =   2100
            Width           =   1785
         End
         Begin VB.Label lblLegendaEndosso 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Premio Tarifa:"
            Height          =   195
            Index           =   15
            Left            =   6495
            TabIndex        =   407
            Top             =   510
            Width           =   975
         End
         Begin VB.Line Line1 
            Index           =   3
            X1              =   5880
            X2              =   9810
            Y1              =   2010
            Y2              =   2010
         End
         Begin VB.Label lblLegendaEndosso 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Desconto:"
            Height          =   195
            Index           =   14
            Left            =   6735
            TabIndex        =   406
            Top             =   1410
            Width           =   735
         End
         Begin VB.Label lblLegendaEndosso 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Adic. Fracionamento:"
            Height          =   195
            Index           =   13
            Left            =   5970
            TabIndex        =   405
            Top             =   1110
            Width           =   1500
         End
         Begin VB.Label lblLegendaEndosso 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Custo Certificado:"
            Height          =   195
            Index           =   12
            Left            =   6225
            TabIndex        =   404
            Top             =   810
            Width           =   1245
         End
         Begin VB.Label lblLegendaEndosso 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "IOF:"
            Height          =   195
            Index           =   11
            Left            =   7170
            TabIndex        =   403
            Top             =   1710
            Width           =   300
         End
         Begin VB.Label lblLegendaEndosso 
            AutoSize        =   -1  'True
            Caption         =   "Valor da Restitui��o:"
            Height          =   195
            Index           =   10
            Left            =   6000
            TabIndex        =   402
            Top             =   2130
            Width           =   1470
         End
         Begin VB.Label lblvalcorretagemprolabore 
            Alignment       =   1  'Right Justify
            BorderStyle     =   1  'Fixed Single
            Caption         =   "0,00"
            Height          =   255
            Index           =   1
            Left            =   3060
            TabIndex        =   401
            Top             =   2550
            Width           =   945
         End
         Begin VB.Label lblvalcorretagemprolabore 
            Alignment       =   1  'Right Justify
            BorderStyle     =   1  'Fixed Single
            Caption         =   "0,00"
            Height          =   255
            Index           =   0
            Left            =   1050
            TabIndex        =   400
            Top             =   2520
            Width           =   945
         End
         Begin VB.Label Lbl 
            AutoSize        =   -1  'True
            Caption         =   "Pr�-Labore:"
            Height          =   195
            Index           =   36
            Left            =   2190
            TabIndex        =   399
            Top             =   2565
            Width           =   825
         End
         Begin VB.Label Lbl 
            AutoSize        =   -1  'True
            Caption         =   "Corretagem:"
            Height          =   195
            Index           =   33
            Left            =   120
            TabIndex        =   398
            Top             =   2550
            Width           =   855
         End
         Begin VB.Line Line1 
            Index           =   2
            X1              =   5520
            X2              =   5520
            Y1              =   240
            Y2              =   2760
         End
         Begin VB.Label lblValRestituicaoResCancelamento 
            Alignment       =   1  'Right Justify
            BorderStyle     =   1  'Fixed Single
            Caption         =   "0,00"
            Height          =   255
            Index           =   0
            Left            =   1920
            TabIndex        =   397
            Top             =   2100
            Width           =   1785
         End
         Begin VB.Label lblLegendaEndosso 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Premio Tarifa:"
            Height          =   195
            Index           =   6
            Left            =   735
            TabIndex        =   392
            Top             =   510
            Width           =   975
         End
         Begin VB.Line Line1 
            Index           =   1
            X1              =   120
            X2              =   4050
            Y1              =   2010
            Y2              =   2010
         End
         Begin VB.Label lblLegendaEndosso 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Desconto:"
            Height          =   195
            Index           =   9
            Left            =   975
            TabIndex        =   390
            Top             =   1410
            Width           =   735
         End
         Begin VB.Label lblLegendaEndosso 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Adic. Fracionamento:"
            Height          =   195
            Index           =   8
            Left            =   210
            TabIndex        =   389
            Top             =   1110
            Width           =   1500
         End
         Begin VB.Label lblLegendaEndosso 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Custo Certificado:"
            Height          =   195
            Index           =   7
            Left            =   465
            TabIndex        =   388
            Top             =   810
            Width           =   1245
         End
         Begin VB.Label lblLegendaEndosso 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "IOF:"
            Height          =   195
            Index           =   3
            Left            =   1410
            TabIndex        =   387
            Top             =   1710
            Width           =   300
         End
         Begin VB.Label lblLegendaEndosso 
            AutoSize        =   -1  'True
            Caption         =   "Valor da Restitui��o:"
            Height          =   195
            Index           =   1
            Left            =   240
            TabIndex        =   386
            Top             =   2130
            Width           =   1470
         End
         Begin VB.Label lbl_sinal 
            Caption         =   "+"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   11
            Left            =   3780
            TabIndex        =   385
            Top             =   465
            Width           =   195
         End
         Begin VB.Label lbl_sinal 
            Caption         =   "+"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   10
            Left            =   3780
            TabIndex        =   384
            Top             =   765
            Width           =   195
         End
         Begin VB.Label lbl_sinal 
            Caption         =   "+"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   9
            Left            =   3780
            TabIndex        =   383
            Top             =   1065
            Width           =   195
         End
         Begin VB.Label lbl_sinal 
            Caption         =   "+"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   8
            Left            =   3780
            TabIndex        =   382
            Top             =   1695
            Width           =   195
         End
         Begin VB.Label lbl_sinal 
            Caption         =   "-"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   7
            Left            =   3840
            TabIndex        =   381
            Top             =   1365
            Width           =   135
         End
         Begin VB.Label lbl_sinal 
            Caption         =   "="
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   6
            Left            =   3780
            TabIndex        =   380
            Top             =   2070
            Width           =   195
         End
      End
      Begin VB.Frame fraCobranca 
         Caption         =   "Cobran�a"
         Height          =   2865
         Left            =   -74160
         TabIndex        =   244
         Top             =   6360
         Width           =   11625
         Begin VB.CheckBox chkIsento_IOF 
            Caption         =   "Isento IOF"
            Height          =   375
            Left            =   8190
            TabIndex        =   252
            Top             =   360
            Width           =   735
         End
         Begin VB.Frame fra 
            Caption         =   "Periodicidade"
            Height          =   585
            Index           =   21
            Left            =   9075
            TabIndex        =   249
            Top             =   240
            Width           =   2205
            Begin VB.OptionButton optPerDia 
               Caption         =   "30 Dias"
               Height          =   375
               Left            =   1080
               TabIndex        =   251
               Top             =   180
               Width           =   855
            End
            Begin VB.OptionButton optPerMes 
               Caption         =   "1 M�s"
               Height          =   375
               Left            =   120
               TabIndex        =   250
               Top             =   180
               Width           =   735
            End
         End
         Begin VB.CommandButton cmdCalcPgto 
            Caption         =   "Calcula Pgto"
            Height          =   345
            Left            =   8490
            TabIndex        =   248
            Top             =   960
            Width           =   1365
         End
         Begin VB.ComboBox cmbTaxaJuros 
            Height          =   315
            Left            =   1275
            Style           =   2  'Dropdown List
            TabIndex        =   247
            Top             =   990
            Width           =   1455
         End
         Begin VB.CommandButton cmdCanPgto 
            Caption         =   "Recalcula Pgto"
            Height          =   345
            Left            =   9990
            TabIndex        =   246
            Top             =   1830
            Visible         =   0   'False
            Width           =   1245
         End
         Begin VB.CommandButton cmdPagAlterar 
            Caption         =   "<<  Alterar >>"
            Enabled         =   0   'False
            Height          =   345
            Left            =   9990
            TabIndex        =   245
            Top             =   1440
            Width           =   1245
         End
         Begin Mask2S.ConMask2S mskCustoEndossoCob 
            Height          =   315
            Left            =   1290
            TabIndex        =   253
            Top             =   450
            Width           =   1455
            _ExtentX        =   2566
            _ExtentY        =   556
            mask            =   ""
            text            =   "0,00"
            locked          =   -1  'True
            enabled         =   -1  'True
         End
         Begin MSMask.MaskEdBox mskNumParcelas 
            Height          =   315
            Left            =   375
            TabIndex        =   254
            Top             =   990
            Width           =   435
            _ExtentX        =   767
            _ExtentY        =   556
            _Version        =   393216
            PromptInclude   =   0   'False
            MaxLength       =   2
            Mask            =   "##"
            PromptChar      =   " "
         End
         Begin MSMask.MaskEdBox mskDtPagamento 
            Height          =   315
            Left            =   6780
            TabIndex        =   255
            Top             =   990
            Width           =   1215
            _ExtentX        =   2143
            _ExtentY        =   556
            _Version        =   393216
            AutoTab         =   -1  'True
            HideSelection   =   0   'False
            MaxLength       =   10
            Mask            =   "##/##/####"
            PromptChar      =   "_"
         End
         Begin Mask2S.ConMask2S mskValPremioTarifa 
            Height          =   375
            Left            =   3225
            TabIndex        =   256
            Top             =   405
            Width           =   1455
            _ExtentX        =   2566
            _ExtentY        =   661
            mask            =   ""
            text            =   "0,00"
            locked          =   0   'False
            enabled         =   -1  'True
         End
         Begin Mask2S.ConMask2S mskValJuros 
            Height          =   330
            Left            =   3195
            TabIndex        =   257
            Top             =   990
            Width           =   1455
            _ExtentX        =   2566
            _ExtentY        =   582
            mask            =   ""
            text            =   "0,00"
            locked          =   0   'False
            enabled         =   -1  'True
         End
         Begin Mask2S.ConMask2S mskValIOF 
            Height          =   330
            Left            =   6780
            TabIndex        =   258
            Top             =   450
            Width           =   1215
            _ExtentX        =   2143
            _ExtentY        =   582
            mask            =   ""
            text            =   "0,00"
            locked          =   -1  'True
            enabled         =   -1  'True
         End
         Begin Mask2S.ConMask2S mskValCom 
            Height          =   315
            Left            =   5115
            TabIndex        =   259
            Top             =   990
            Width           =   1245
            _ExtentX        =   2196
            _ExtentY        =   556
            mask            =   ""
            text            =   "0,00"
            locked          =   0   'False
            enabled         =   -1  'True
         End
         Begin Mask2S.ConMask2S mskValDesc 
            Height          =   315
            Left            =   5100
            TabIndex        =   260
            Top             =   450
            Width           =   1245
            _ExtentX        =   2196
            _ExtentY        =   556
            mask            =   ""
            text            =   "0,00"
            locked          =   0   'False
            enabled         =   -1  'True
         End
         Begin Mask2S.ConMask2S mskValLiquido 
            Height          =   330
            Left            =   9960
            TabIndex        =   261
            Top             =   960
            Visible         =   0   'False
            Width           =   1245
            _ExtentX        =   2196
            _ExtentY        =   582
            mask            =   ""
            text            =   "0,00"
            locked          =   0   'False
            enabled         =   0   'False
         End
         Begin MSFlexGridLib.MSFlexGrid grdPagamento 
            Height          =   870
            Left            =   240
            TabIndex        =   262
            Top             =   1380
            Width           =   9645
            _ExtentX        =   17013
            _ExtentY        =   1535
            _Version        =   393216
            Cols            =   9
            FixedCols       =   0
            Enabled         =   -1  'True
            FormatString    =   "Parc.|Dt Pagamento|Valor Tarif�rio|Custo Endosso|Valor Desconto|Adic. Frac    | Valor IOF     |Valor Comiss�o|Valor Parcela"
         End
         Begin Mask2S.ConMask2S mskValComEst 
            Height          =   315
            Left            =   8490
            TabIndex        =   263
            Top             =   2475
            Width           =   1245
            _ExtentX        =   2196
            _ExtentY        =   556
            mask            =   ""
            text            =   "0,00"
            locked          =   0   'False
            enabled         =   0   'False
         End
         Begin Mask2S.ConMask2S mskTotTarifa 
            Height          =   330
            Left            =   270
            TabIndex        =   264
            TabStop         =   0   'False
            Top             =   2475
            Width           =   1215
            _ExtentX        =   2143
            _ExtentY        =   582
            mask            =   ""
            text            =   "0,00"
            locked          =   -1  'True
            enabled         =   0   'False
         End
         Begin Mask2S.ConMask2S mskTotJuros 
            Height          =   330
            Left            =   3000
            TabIndex        =   265
            TabStop         =   0   'False
            Top             =   2475
            Width           =   1215
            _ExtentX        =   2143
            _ExtentY        =   582
            mask            =   ""
            text            =   "0,00"
            locked          =   -1  'True
            enabled         =   0   'False
         End
         Begin Mask2S.ConMask2S mskTotLiquido 
            Height          =   330
            Left            =   7110
            TabIndex        =   266
            TabStop         =   0   'False
            Top             =   2475
            Width           =   1215
            _ExtentX        =   2143
            _ExtentY        =   582
            mask            =   ""
            text            =   "0,00"
            locked          =   -1  'True
            enabled         =   0   'False
         End
         Begin Mask2S.ConMask2S mskTotIof 
            Height          =   330
            Left            =   4380
            TabIndex        =   267
            TabStop         =   0   'False
            Top             =   2475
            Width           =   1215
            _ExtentX        =   2143
            _ExtentY        =   582
            mask            =   ""
            text            =   "0,00"
            locked          =   -1  'True
            enabled         =   0   'False
         End
         Begin Mask2S.ConMask2S mskTotCom 
            Height          =   330
            Left            =   5745
            TabIndex        =   268
            TabStop         =   0   'False
            Top             =   2475
            Width           =   1215
            _ExtentX        =   2143
            _ExtentY        =   582
            mask            =   ""
            text            =   "0,00"
            locked          =   -1  'True
            enabled         =   0   'False
         End
         Begin Mask2S.ConMask2S mskTotDesc 
            Height          =   330
            Left            =   1635
            TabIndex        =   269
            TabStop         =   0   'False
            Top             =   2475
            Width           =   1215
            _ExtentX        =   2143
            _ExtentY        =   582
            mask            =   ""
            text            =   "0,00"
            locked          =   -1  'True
            enabled         =   0   'False
         End
         Begin VB.Label lblNumParcAlt 
            AutoSize        =   -1  'True
            Caption         =   "7"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   12
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   300
            Left            =   495
            TabIndex        =   287
            Top             =   480
            Visible         =   0   'False
            Width           =   165
         End
         Begin VB.Label lblParcAlt 
            AutoSize        =   -1  'True
            Caption         =   "Parcela:"
            Height          =   195
            Left            =   300
            TabIndex        =   286
            Top             =   240
            Visible         =   0   'False
            Width           =   630
         End
         Begin VB.Label Lbl 
            AutoSize        =   -1  'True
            Caption         =   "Valor Desconto:"
            Height          =   195
            Index           =   88
            Left            =   5115
            TabIndex        =   285
            Top             =   240
            Width           =   1185
         End
         Begin VB.Label Lbl 
            AutoSize        =   -1  'True
            Caption         =   "Valor Comiss�o:"
            Height          =   195
            Index           =   55
            Left            =   5115
            TabIndex        =   284
            Top             =   780
            Width           =   1170
         End
         Begin VB.Label Lbl 
            AutoSize        =   -1  'True
            Caption         =   "Custo Endosso:"
            Height          =   195
            Index           =   61
            Left            =   1275
            TabIndex        =   283
            Top             =   240
            Width           =   1110
         End
         Begin VB.Label Lbl 
            AutoSize        =   -1  'True
            Caption         =   "Valor IOF:"
            Height          =   195
            Index           =   63
            Left            =   6795
            TabIndex        =   282
            Top             =   240
            Width           =   750
         End
         Begin VB.Label lblDtVcto 
            AutoSize        =   -1  'True
            Caption         =   "Data  1�  Vcto:"
            Height          =   195
            Left            =   6795
            TabIndex        =   281
            Top             =   780
            Width           =   1050
         End
         Begin VB.Label Lbl 
            AutoSize        =   -1  'True
            Caption         =   "Valor Tarifa:"
            Height          =   195
            Index           =   62
            Left            =   3195
            TabIndex        =   280
            Top             =   240
            Width           =   900
         End
         Begin VB.Label lblPerJuros 
            AutoSize        =   -1  'True
            Caption         =   "% Juros:"
            Height          =   195
            Left            =   1275
            TabIndex        =   279
            Top             =   780
            Width           =   630
         End
         Begin VB.Label lblNumParc 
            AutoSize        =   -1  'True
            Caption         =   "Parcelas:"
            Height          =   195
            Left            =   300
            TabIndex        =   278
            Top             =   780
            Width           =   720
            WordWrap        =   -1  'True
         End
         Begin VB.Label Lbl 
            AutoSize        =   -1  'True
            Caption         =   "Adic. Fracionamento:"
            Height          =   195
            Index           =   65
            Left            =   3195
            TabIndex        =   277
            Top             =   780
            Width           =   1545
         End
         Begin VB.Label Lbl 
            AutoSize        =   -1  'True
            Caption         =   "Valor Desconto:"
            Height          =   195
            Index           =   90
            Left            =   1635
            TabIndex        =   276
            Top             =   2265
            Width           =   1140
         End
         Begin VB.Label Lbl 
            AutoSize        =   -1  'True
            Caption         =   "Valor Comiss�o:"
            Height          =   195
            Index           =   89
            Left            =   5745
            TabIndex        =   275
            Top             =   2265
            Width           =   1125
         End
         Begin VB.Label Lbl 
            Caption         =   "Pr�mio Tarif�rio:"
            Height          =   165
            Index           =   53
            Left            =   270
            TabIndex        =   274
            Top             =   2265
            Width           =   1275
         End
         Begin VB.Label Lbl 
            AutoSize        =   -1  'True
            Caption         =   "Adic. Fracion.:"
            Height          =   195
            Index           =   73
            Left            =   3060
            TabIndex        =   273
            Top             =   2265
            Width           =   1065
         End
         Begin VB.Label Lbl 
            Caption         =   "Pr�mio Total:"
            Height          =   195
            Index           =   54
            Left            =   7110
            TabIndex        =   272
            Top             =   2265
            Width           =   1095
         End
         Begin VB.Label Lbl 
            AutoSize        =   -1  'True
            Caption         =   "Valor IOF:"
            Height          =   195
            Index           =   72
            Left            =   4425
            TabIndex        =   271
            Top             =   2265
            Width           =   705
         End
         Begin VB.Label Lbl 
            AutoSize        =   -1  'True
            Caption         =   "Comiss�o Estip:"
            Height          =   195
            Index           =   56
            Left            =   8490
            TabIndex        =   270
            Top             =   2280
            Width           =   1110
         End
      End
      Begin MSFlexGridLib.MSFlexGrid grdSinistros 
         Height          =   2445
         Left            =   -74400
         TabIndex        =   446
         Top             =   1620
         Width           =   10935
         _ExtentX        =   19288
         _ExtentY        =   4313
         _Version        =   393216
         Rows            =   1
         Cols            =   16
         FixedCols       =   0
         FocusRect       =   0
         HighLight       =   2
         SelectionMode   =   1
         FormatString    =   $"frmEndosso.frx":09BD
      End
      Begin MSFlexGridLib.MSFlexGrid grdReintegracao 
         Height          =   2205
         Index           =   1
         Left            =   -74400
         TabIndex        =   447
         Top             =   1620
         Width           =   10935
         _ExtentX        =   19288
         _ExtentY        =   3889
         _Version        =   393216
         Rows            =   1
         Cols            =   22
         FixedCols       =   0
         FocusRect       =   0
         HighLight       =   2
         SelectionMode   =   1
         FormatString    =   $"frmEndosso.frx":0AB1
      End
      Begin MSFlexGridLib.MSFlexGrid grdReintegracao 
         Height          =   2205
         Index           =   0
         Left            =   -74760
         TabIndex        =   485
         Top             =   1740
         Width           =   11655
         _ExtentX        =   20558
         _ExtentY        =   3889
         _Version        =   393216
         Rows            =   1
         Cols            =   22
         FixedCols       =   0
         FocusRect       =   0
         HighLight       =   2
         FormatString    =   $"frmEndosso.frx":0BE1
      End
      Begin MSFlexGridLib.MSFlexGrid grdReintegracao 
         Height          =   2205
         Index           =   2
         Left            =   -74400
         TabIndex        =   503
         Top             =   1620
         Width           =   10935
         _ExtentX        =   19288
         _ExtentY        =   3889
         _Version        =   393216
         Rows            =   1
         Cols            =   22
         FixedCols       =   0
         FocusRect       =   0
         HighLight       =   2
         SelectionMode   =   1
         FormatString    =   "Proposta|Endosso|Apolice|Sucursal Seguradora|Seguradora Susep|Dt Fim Viegncia|Ramo|Agencia|Conta|Seguro Moeda|Cobertuta"
      End
      Begin MSFlexGridLib.MSFlexGrid grdReintegracao 
         Height          =   2205
         Index           =   3
         Left            =   -74760
         TabIndex        =   561
         Top             =   1620
         Width           =   11655
         _ExtentX        =   20558
         _ExtentY        =   3889
         _Version        =   393216
         Rows            =   1
         Cols            =   8
         FixedCols       =   0
         FocusRect       =   0
         HighLight       =   2
         SelectionMode   =   1
         FormatString    =   "Proposta | Endosso | Parcela | Respons�vel | Valor Parcela | Valor Pago | Dt Pagamento | Ramo"
      End
      Begin VB.Label Label2 
         Caption         =   "Faturamento Esperado: (Produt. Esperada / 60) X Pre�o Base X Nova �rea "
         Height          =   375
         Index           =   30
         Left            =   -74640
         TabIndex        =   566
         Top             =   6780
         Width           =   5775
      End
   End
   Begin VB.Label lblCad 
      AutoSize        =   -1  'True
      Caption         =   "Banco :"
      Height          =   195
      Index           =   9
      Left            =   0
      TabIndex        =   569
      Top             =   0
      Width           =   555
   End
   Begin VB.Label Label3 
      Caption         =   "Pre�o Base"
      Height          =   375
      Index           =   1
      Left            =   0
      TabIndex        =   512
      Top             =   4600
      Width           =   2055
   End
End
Attribute VB_Name = "frmEndosso"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'---------------------------------------------------------------------------------------
' M�dulo    : frmEndosso
' Autor     : dsilva
' Flow      : 380882
' Data      : 16/05/2008
' Objetivo  : Utilizar a Data do Sistema para a busca do IOF
'---------------------------------------------------------------------------------------


Option Explicit

Const TP_ENDOSSO_RESTITUICAO = 80
Const TP_ENDOSSO_CANCELAMENTO_APOLICE = 63
Const TP_ENDOSSO_CANCELAMENTO_APOLICE_SEGURADORA = 64
Const TP_ENDOSSO_CANCELAMENTO_ENDOSSO = 101
Const TP_ENDOSSO_CADASTRAL = 51
Const TP_ENDOSSO_COBRANCA = 200
Const TP_ENDOSSO_ESTIPULANTE = 105
Const TP_ENDOSSO_CORRETAGEM = 106
Const TP_ENDOSSO_BENEFICIARIO = 2
Const TP_ENDOSSO_ALTERACAO_ENDERECO = 108
Const TP_ENDOSSO_LIMITE_EMISSAO = 210
Const TP_ENDOSSO_IMPORTANCIA_SEGURADA = 10
Const TP_ENDOSSO_REATIVACAO_FATURA = 204
Const TP_EVENTO_EMISSAO_ENDOSSO = 5002    'alucas 06/10/2005
Const TP_ENDOSSO_REATIVACAO_PROPOSTA = 266
Const TP_ENDOSSO_REDUCAO_IS = 94
Const TP_REINTEGRACAO_IS = 61
Const TP_ENDOSSO_ADEQUACAO_AREA = 304
Const TP_ENDOSSO_ADEQUACAO_PRODUTIVIDADE = 305
Const TP_RESTITUICAO_SUBSIDIO = 269
Const TP_ALTERACAO_FORMA_PAGAMENTO = 252    'R.FOUREAUX 25/06/2012 - DEMANDA 12006056
Const TP_ENDOSSO_ALTERACAO_PRECO_BASE = 335   'JOAO.MACHADO
Const TP_ENDOSSO_CANCELAMENTO_SINISTRO = 68

'G&P - Flow - 219229
Const TP_ENDOSSO_CANCELAMENTO_INADIMP_1_PARCELA = 90
Const TP_ENDOSSO_CANCELAMENTO_INADIMP_DEMAIS_PARCELAS = 92



'''''BNEVES 29/02/2008 FLOW 280844
Const TP_ENDOSSO_MOVIMENTACAO_PREMIO = 200


'lrocha 11/12/2007
Const TP_ENDOSSO_BONIFICACAO = 270
''''''''''''''''''

Const OURO_AGRICOLA = 1152  'Rmarins 13/12/06
Const OURO_RESIDENCIAL_ESTILO = 1188    'pablo.dias 08/03/2013.


'Alexandre Debouch - Confitec - Demanda EdsContestacao
Const TP_ENDOSSO_CONTESTACAO = 366
'Alexandre Debouch - Confitec - Demanda EdsContestacao

'Sergio Pires Nova Tend�ncia CCM Registro de estapas no SEGBR
Const TP_ENDOSSO_ALTERACAO_TIPO_ENVIO_DOCUMENTO = 370

Public oDadosEndosso As New DadosEndosso
Public oDadosModular As New DadosModular   'rsilva 22/07/2009

Public bPossuiSubvencao As Boolean   'Rmarins 18/10/06

'bcarneiro - 02/03/2007
Public bRestIntegral As Boolean

Public iModular As Integer  'rsilva - 22/07/2009
Public bModular As Boolean  'rsilva - 22/07/2009
Public sDtFimEsc As String
Public sDtInicioEsc As String
'''''''''''''''''''''''
Public vlRetorno As Integer    ' 04/12/2015 - Schoralick (nova consultoria) - 18690344 - Melhorias Processo Reagendamento e Reativa��o BESC - inicio
Public vlProdutoId As Integer    ' 11/12/2015 - Schoralick (nova consultoria) - 18690344 - Melhorias Processo Reagendamento e Reativa��o BESC - inicio
' 04/12/2015 - Schoralick (nova consultoria) - 18690344 - Melhorias Processo Reagendamento e Reativa��o BESC - inicio
Public vlDataReativacao As String
Public vlRetornoReativacao As Integer

Public vOutrosCanais As String    'fernanda.souza Confitec


Private colEstipulante As New Collection
Private colCorretor As New Collection
Private colSubGrupo As New Collection
Private colBeneficiario As New Collection

Private VetorEstadoCivil(0 To 5) As String
Private iAuxLinha As Integer

Dim sEndereco As String
Dim sBairro As String
Dim sCidade As String
Dim sUF As String
Dim sCep As String

Dim iTpPessoa As Integer
Dim sNascimento As String
Dim sCPFCNPJ As String
Dim sSexo As String
Dim sEstCivil As String
Dim sDDD As String
Dim sTelefone As String
Dim sEmail As String
Dim sBanco As String
Dim sNomeBanco As String
Dim sCodAg As String
Dim sNomeAg As String
Dim sCC As String

Dim sTabelaSeguro As String
Dim sTabelaEscolha As String
Dim lSeqCancEndossoSeg As Long
Dim bConfiguracaoBrasil As Boolean
Dim cValPremioAnterior As Currency
Dim cValPremioNovo As Currency
Dim colCoberturas As New Collection
Dim colNovaCobertura As New Collection
Dim colCoberturasEndossadas As New Collection
Dim lCodCoberturaBasica As Long
Dim dRiscoCoberturaBasica As Double
Dim dPercLimMaxCobertura As Double
Dim dPercLimMinCobertura As Double
Dim dLimMaxCobertura As Double
Dim dLimMinCobertura As Double
Dim sAcumulaIS As String
Dim sAcumulaResseguro As String
Dim cPercCorretagem As Currency
Dim cPremioTotal As Currency
Dim cValorPago As Currency
Dim cValorJuros As Currency
Dim cValorDesconto As Currency
Dim cAuxPremio As Currency
Dim cValFinanceiro As Currency
Dim sDtIniVigenciaSeg() As String
Dim iMaiorObjeto As Integer
Dim sFlagTipoOperacao As String

'Luciana - 31/01/2005
Private lSeguro_moeda_id As Long
Private lPremio_moeda_id As Long

Dim lEndossoCancelamentoId As Long
'scasagrande - 27/04/2006
Dim lUltimaParcelaReativacao As Long
'Ralves - 11/05/2012 / Demanda 7979370 - BB Seguro Agr�cola Faturamento
Dim ObterAgendamentosReativacao As Recordset
Dim ObterTpEndossoCancId As Integer

'Reintegra��o
Dim Coberturas() As tp_Cob

Dim Proposta_id_ant As String
Dim Proposta_BB_ant As String
Dim Arquivo_ant As String
Dim Cod_ramo_BB_ant As String
Dim Proponente_ant As String
Dim Sinistro_id_ant As String
Dim Apolice_id_ant As String
Dim Sucursal_ant As String
Dim Seguradora_ant As String
Dim ramo_id_ant As String
Dim seguro_moeda_id_ant As String
Dim Tp_Pessoa_ant As String
Dim CPF_ant As String
Dim CGC_ant As String
Dim Dt_Nascimento_ant As String
Dim Tp_Pgto_ant As String
Dim Dt_Inicio_Vigencia_Aux As String
Dim Dt_endosso_ant As String
Dim Dt_pedido_endosso_ant As String
Dim Produto_id_ant As String

Dim Num_Cartao_ant As String
Dim Dt_Validade_Cartao_ant As String


Dim Banco_ant As String
Dim Agencia_ant As String
Dim Conta_Corrente_ant As String
Dim Dt_Inicio_Vigencia_ant As String
'Brauner Rangel Nova consultoria
Dim uTipoEndosso As Integer
Dim uProduto As Integer
'---------------------------

'Sergio Pires Nova Tend�ncia CCM Registro de estapas no SEGBR
Dim bApolice_Aberta As Boolean

Public dFatTaxa As Double
Public Subramo_id As String

Public PrecoBaseAnterior As String
Public PrecoBaseNovo As String

Public bObteveSaldoRestituir As Boolean    'cristovao.rodrigues 25/04/2013


'Dim rdocn1 As New rdoConnection
'Dim rdocn1 As New Connection
Public Function CalcularRestituicaoCurtoPrazo(ByVal dDias As Double, _
                                              ByVal cValPago As Currency, _
                                              ByVal cPremioTotal As Currency) As Currency

    Dim oFinanceiro As Object
    Dim rs As Recordset
    Dim dPercRestituicao As Double
    Dim cValRestituicao As Currency

    On Error GoTo TrataErro

    Set oFinanceiro = CreateObject("SEGL0022.cls00117")

    Set rs = oFinanceiro.ConsultarCurtoPrazo(gsSIGLASISTEMA, _
                                             App.Title, _
                                             App.FileDescription, _
                                             glAmbiente_id, _
                                             dDias)

    Set oFinanceiro = Nothing

    If Not rs.EOF Then
        dPercRestituicao = Val(rs("perc_restituicao"))
    End If

    'rsilva - 02/09/2009
    'Novo calculo do pr�mio a ser restituido para proposta modulares
    If bModular = False Then
        cValRestituicao = cValPago - ((cPremioTotal * (dPercRestituicao / 100)))
    Else
        cValRestituicao = (cValPago / (1 + oDadosEndosso.PercIOF)) * (dDias / 30)
        cValRestituicao = (cValPago / (1 + oDadosModular.PercIOF)) * (dDias / 30)
    End If


    cValRestituicao = TruncaDecimal(cValRestituicao)

    CalcularRestituicaoCurtoPrazo = cValRestituicao

    Set rs = Nothing
    'oFinanceiro = Nothing

    Exit Function

TrataErro:
    Call TratarErro("CalcularRestituicaoCurtoPrazo", Me.name)
    Call FinalizarAplicacao

End Function

Public Function ProcessarRestituicaoALS(ByVal dDias As Double, _
                                        ByVal cValPago As Currency, _
                                        ByVal cPremioTotal As Currency, _
                                        Optional ByRef bRestIntegralALS As Boolean = False) As Currency    'HOLIVEIRA: Retorna bRestIntegralALS = TRUE quando for Restitui��o Integral ALS. DT: 12/02/2007


    Dim oFinanceiro As Object
    Dim rs As Recordset
    Dim iPrazoRestIntegral As Integer
    Dim cValRestituicao As Currency

    On Error GoTo TrataErro

    Set oFinanceiro = CreateObject("SEGL0026.cls00125")

    Set rs = oFinanceiro.ConsultarPrazoRestituicaoIntegral(gsSIGLASISTEMA, _
                                                           App.Title, _
                                                           App.FileDescription, _
                                                           glAmbiente_id, _
                                                           oDadosEndosso.produtoId)

    If Not rs.EOF Then
        iPrazoRestIntegral = rs("prazo_restituicao_integral")
    End If

    'Bruno Faria - 19/01/2011
    'Obs.: coloquei a verifica��o da vari�vel "bModular" abaixo pois s� deve fazer a restitui��o integral para o BB Prote��o,
    'para os demais produtos n�o deve fazer devolu��o de IOF e Custo de Ap�lice;

    'Inicio Customizado por Marcio Santos (CWI)
    'Altera��o: Tamb�m deve fazer a restitui��o integral para os Produtos BB Seguro Agr�cola (1152)
    'If dDias <= iPrazoRestIntegral And bModular = True Then
    If dDias <= iPrazoRestIntegral And (bModular = True Or txtProduto.Text = 1152) Then
        'Fim Customizado por Marcio Santos (CWI)
        cValRestituicao = cPremioTotal      'Restitui��o Integral
        bRestIntegralALS = True    'HOLIVEIRA: Retorna bRestIntegralALS = TRUE quando for Restitui��o Integral ALS. DT: 12/02/2007
    Else
        cValRestituicao = CalcularRestituicaoCurtoPrazo(dDias, cValPago, cPremioTotal)
    End If

    ProcessarRestituicaoALS = cValRestituicao

    Set rs = Nothing
    Set oFinanceiro = Nothing

    Exit Function

TrataErro:
    Call TratarErro("ProcessarRestituicaoALS", Me.name)
    Call FinalizarAplicacao

End Function



Private Sub cboCidade_Click()

    If cboCidade.ListIndex = -1 Then Exit Sub

    TxtUFAnt(1).Text = Right(cboCidade.List(cboCidade.ListIndex), 2)

End Sub

Private Sub cboCidadeBenef_Click()

    If cboCidadeBenef.ListIndex = -1 Then Exit Sub

    txtBairroBenef(2).Text = Right(cboCidadeBenef.List(cboCidadeBenef.ListIndex), 2)

End Sub

Private Sub cboCoberturaAfetada_Click()

    Dim oCobertura As Cobertura

    On Error GoTo Erro

    If cboCoberturaAfetada.ListIndex <> -1 Then

        mskImpSeg.Text = "0,00"
        mskTaxaTarifa.Text = "0,0000000"
        mskTaxaNet.Text = "0,0000000"
        mskPerDesc.Text = "0,00000"
        mskPremioTarifa.Text = "0,00"
        mskPremioNet.Text = "0,00"
        mskLimFra.Text = "0,00"

        For Each oCobertura In colNovaCobertura
            If oCobertura.TpCoberturaId = cboCoberturaAfetada.ItemData(cboCoberturaAfetada.ListIndex) Then
                dPercLimMaxCobertura = oCobertura.PercMax
                dPercLimMinCobertura = oCobertura.PercMin
                dLimMaxCobertura = oCobertura.LimMax
                dLimMinCobertura = oCobertura.LimMin
                sAcumulaIS = oCobertura.Acumula_IS
                sAcumulaResseguro = oCobertura.Acumula_Resseguro
                Exit For
            End If
        Next

        'Para aumento de IS, sugerir taxa tarifa da cobertura original
        If optAumIS.Value Then
            For Each oCobertura In colCoberturas
                If oCobertura.TpCoberturaId = cboCoberturaAfetada.ItemData(cboCoberturaAfetada.ListIndex) _
                   And oCobertura.CodObjSegurado = cboObjetoSegurado.Text Then
                    mskTaxaTarifa.Text = Format(oCobertura.ValTaxa, "#.0000000")
                    Call mskTaxaTarifa_LostFocus
                    Exit For
                End If
            Next
        End If

        cmdBensAdicionar.Enabled = True
        cmdBensAlterar.Enabled = False
        cmdBensRemover.Enabled = False

        mskImpSeg.SetFocus

    End If

    Set oCobertura = Nothing

    Exit Sub

Erro:
    Call TratarErro("cboCoberturaAfetada_Click", Me.name)
    Call FinalizarAplicacao
End Sub

Private Sub cboCorretorCob_Click()

    Dim lCorretorId As Long
    Dim iCont As Integer

    If cboCorretorCob.ListIndex <> -1 Then

        lCorretorId = cboCorretorCob.ItemData(cboCorretorCob.ListIndex)

        For iCont = 0 To cboTpCorretor.ListCount - 1
            cboTpCorretor.ListIndex = iCont
            If cboTpCorretor.ItemData(cboTpCorretor.ListIndex) = lCorretorId Then
                Exit For
            End If
        Next

        For iCont = 0 To cboSucursalCorretorCob.ListCount - 1
            cboSucursalCorretorCob.ListIndex = iCont
            If cboSucursalCorretorCob.ItemData(cboSucursalCorretorCob.ListIndex) = lCorretorId Then
                Exit For
            End If
        Next

        cmdCorretorCobAdicionar.Enabled = True
        cmdCorretorCobAlterar.Enabled = False
        cmdCorretorCobRemover.Enabled = False

    End If

End Sub



Private Sub CboEstipulante_Click()

    Dim lClienteId As Long
    Dim iCont As Integer

    If CboEstipulante.ListIndex <> -1 Then

        lClienteId = CboEstipulante.ItemData(CboEstipulante.ListIndex)

        For iCont = 0 To CboEstipulante.ListCount - 1
            CboEstipulante.ListIndex = iCont
            If CboEstipulante.ItemData(CboEstipulante.ListIndex) = lClienteId Then
                Exit For
            End If
        Next

        cmdEstipulanteAdicionar.Enabled = True
        cmdEstipulanteAlterar.Enabled = False
        cmdEstipulanteRemover.Enabled = False

    End If

End Sub

'R.FOUREAUX 25/06/2012 - DEMANDA 12006056
Private Sub cboFormaPagamento_Click()
    With cboFormaPagamento
        If .ListCount <> 0 Then
            'CLIQUE EM DEBITO EM CONTA
            If .ItemData(.ListIndex) = 1 Then
                'FORMA PGTO SOME PARA CBO
                txtforma_pgto(0).Visible = False
                txtforma_pgto(1).Visible = False

                'AGENCIA APARECE
                txtCodAgDebito(0).Visible = True
                txtCodAgDebito(0).Enabled = True
                txtCodAgDebito(0).BackColor = &H80000005
                'CC APARECE
                txtEndoCad(7).Visible = True
                txtEndoCad(7).Enabled = True
                txtEndoCad(7).BackColor = &H80000005

                'NOME AGENCIA APARECE MAIS NAO MUDA
                txtCodAgDebito(1).Visible = True
                txtCodAgDebito(1).Enabled = False
                txtCodAgDebito(1).BackColor = &H80000004
                'CODIGO BANCO APARECE MAIS NAO MUDA
                txtBanco.Visible = True
                txtBanco.Enabled = False
                txtBanco.BackColor = &H80000004
                'NOME BANCO APARECE MAIS NAO MUDA
                txtEndoCad(6).Visible = True
                txtEndoCad(6).Enabled = False
                txtEndoCad(6).BackColor = &H80000004

                'LBL APARECE
                lblCad(23).Visible = True
                lblCad(24).Visible = True
                lblCad(25).Visible = True

            End If
            'CLIQUE EM COMPENSACAO BANCARIA
            If .ItemData(.ListIndex) = 3 Then
                txtforma_pgto(0).Visible = False
                txtforma_pgto(1).Visible = False
                txtCodAgDebito(0).Visible = False
                txtCodAgDebito(1).Visible = False
                txtBanco.Visible = False
                txtEndoCad(6).Visible = False
                txtEndoCad(7).Visible = False
                lblCad(23).Visible = False
                lblCad(24).Visible = False
                lblCad(25).Visible = False
            End If
        End If
    End With
End Sub

Private Sub cboMoeda_Click()

    Dim oDadosBasicos As Object
    Dim rs As Recordset

    Const MOEDA_ID_REAL = 790

    On Error GoTo TrataErro

    If cboMoeda.ItemData(cboMoeda.ListIndex) <> MOEDA_ID_REAL Then

        Lbl(39).Visible = True
        mskCambio.Visible = True

        Set oDadosBasicos = CreateObject("SEGL0022.cls00117")

        Set rs = oDadosBasicos.ConsultarParidade(gsSIGLASISTEMA, _
                                                 App.Title, _
                                                 App.FileDescription, _
                                                 glAmbiente_id, _
                                                 cboMoeda.ItemData(cboMoeda.ListIndex), _
                                                 MOEDA_ID_REAL, _
                                                 Data_Sistema)

        Set oDadosBasicos = Nothing

        If Not rs.EOF Then
            If Not IsNull(rs("val_paridade_moeda")) Then
                mskCambio.Text = Val(rs("val_paridade_moeda"))
                oDadosEndosso.FatorCambio = Val(rs("val_paridade_moeda"))
            End If
        Else
            oDadosEndosso.FatorCambio = 1
        End If

        Call mskCambio_LostFocus

    Else

        Lbl(39).Visible = False
        mskCambio.Visible = False
        oDadosEndosso.FatorCambio = 1
        mskCambio.Text = "1,000000"

    End If

    Set rs = Nothing

    Exit Sub

TrataErro:
    Call TratarErro("cboMoeda_Click", Me.name)
    Call FinalizarAplicacao

End Sub

Private Sub cboObjetoSegurado_Click()

    If cboObjetoSegurado.ListIndex <> -1 Then
        Call MontarGridCoberturasAtual(cboObjetoSegurado.Text)
        Call MontarGridCoberturasEndossadas(cboObjetoSegurado.Text)
    End If

End Sub

'Private Sub CboSubGrupo_Change()
'
'    Dim sIndice As String
'
'    sIndice = CStr(CboSubGrupo.ItemData(CboSubGrupo.ListIndex))
'
'    Set colCorretor = Nothing
'    Set colCorretorAnt = Nothing
'
'    TxtCodCorretor(0).Text = ""
'    TxtCodCorretor(1).Text = ""
'    TxtTpCorretor.Text = ""
'    CboSucCorretor.Clear
'    TxtSucCorretor.Text = ""
'    mskPercCorretagem.Text = Format("0,00", "0.00000")
'
'    Set colCorretor = colSubGrupo(sIndice).corretores
'    If sIndice <= cont_sub_orig Then
'        Set colCorretorAnt = colSubGrupoAnt(sIndice).corretores
'    End If
'
'    Call MontarGridCorretagem
'
'End Sub

Private Sub CboSubGrupo_Click()

    Dim oSubGrupo As Object

    On Error GoTo TrataErro

    If CboSubGrupo.ListIndex <> -1 Then
        Set oSubGrupo = colSubGrupo.Item(CboSubGrupo.ItemData(CboSubGrupo.ListIndex))
        Call MontarGridCorretagem(oSubGrupo.Corretores)
        CmdRestaura.Enabled = True
        CmdConCorretor.Enabled = True
        mskPercCorretagem.Locked = False
        TxtCodCorretor(0).Text = ""
        TxtCodCorretor(1).Text = ""
        TxtTpCorretor.Text = ""
        mskPercCorretagem.Text = Format("0,00", "0.00000")
    End If

    Set oSubGrupo = Nothing

    Exit Sub

TrataErro:
    Call TratarErro("cboSubGrupo_Click", Me.name)
    Call FinalizarAplicacao

End Sub

Private Sub cboSubGrupoEst_Click()

    Dim oSubGrupo As Object

    On Error GoTo TrataErro

    If cboSubGrupoEst.ListIndex <> -1 Then
        Set oSubGrupo = colSubGrupo.Item(cboSubGrupoEst.ItemData(cboSubGrupoEst.ListIndex))
        Call PreencherGridEstipulante(oSubGrupo.estipulantes)
        cmdRestaurarEst.Enabled = True
        cmdConCliente.Enabled = True
        mskPercEstipulante.Locked = False
    End If

    Set oSubGrupo = Nothing

    Exit Sub

TrataErro:
    Call TratarErro("cboSubGrupoEst_Click", Me.name)
    Call FinalizarAplicacao

End Sub

Private Sub cboTipoEndosso_Click()

'JOAO.MACHADO
    If cboTipoEndosso.ItemData(cboTipoEndosso.ListIndex) = TP_ENDOSSO_ALTERACAO_PRECO_BASE Then
        Call RetornarPrecoBase(CLng(txtPropostaid(0).Text))
    Else
        txtDescEndosso.Text = ""
    End If

    If cboTipoEndosso.ListIndex <> -1 Then

        Call ConsultaPermiteRestituicao(frmSelecao.grdSelecao.TextMatrix(frmSelecao.grdSelecao.Row, 3))

        'S�rgio Pires Nova Tend�ncia CCM Registro de Etapas no SEGBR
        If cboTipoEndosso.ItemData(cboTipoEndosso.ListIndex) = TP_ENDOSSO_ALTERACAO_TIPO_ENVIO_DOCUMENTO Then
            mskDtEndosso.Text = Format(Data_Sistema, "dd/mm/yyyy")
            mskDtEndosso.Enabled = False
            txtDescEndosso.Text = ""
            txtDescEndosso.Enabled = False
            fraIS.Visible = False
            mskIS.Visible = False
            Lbl(28).Visible = False
            mskFimVig.Visible = False
            optNovVal.Value = False
        Else
            txtDescEndosso.Enabled = True
            If bApolice_Aberta Then
               fraIS.Visible = True
               fraIS.Refresh
               mskIS.Visible = True
               mskIS.Text = "0,00"
               Lbl(28).Visible = True
               mskFimVig.Visible = True
               optNovVal.Value = True
            End If
        End If

        'Jessica.Adao - Confitec Sistemas - 20170602 - Projeto MU-2017-014324 : Endosso de Restituicao
        If cboTipoEndosso.ItemData(cboTipoEndosso.ListIndex) = TP_ENDOSSO_RESTITUICAO And bloqueiaVigenciaRestituicao(oDadosEndosso.produtoId) Then
            mskDtEndosso.Text = txtIniVigencia.Text
            mskDtEndosso.Enabled = False
        Else
            mskDtEndosso.Text = Format(Data_Sistema, "dd/mm/yyyy")
            mskDtEndosso.Enabled = True
        End If
        'Jessica.Adao - Confitec Sistemas - 20170602 - Projeto MU-2017-014324 : Endosso de Restituicao

    End If
    cmdAplicarProposta.Enabled = True

    'Alexandre Debouch - Confitec - Demanda EdsContestacao
    If cboTipoEndosso.ItemData(cboTipoEndosso.ListIndex) = TP_ENDOSSO_CONTESTACAO Then
        Dim oEdsContestacao As Object
        Set oEdsContestacao = CreateObject("SEGL0026.cls00500")

        If oEdsContestacao.CriticaTipoEdsContestacao(gsSIGLASISTEMA, App.Title, App.FileDescription, glAmbiente_id, CLng(txtPropostaid(0).Text)) Then
            txtDescEndosso.Text = "Endosso de Contesta��o - "
            txtDescEndosso.SetFocus
        Else
            Call oEdsContestacao.InserirExtratoRestituicaoContestacao(gsSIGLASISTEMA, _
                                                    App.Title, _
                                                    App.FileDescription, _
                                                    glAmbiente_id, _
                                                    -1, _
                                                    cUserName, _
                                                    txtPropostaid(0).Text, _
                                                    "0", _
                                                    "0", _
                                                    TP_ENDOSSO_CONTESTACAO, _
                                                    "As regras para prioriza��o da devolu��o do pr�mio n�o foram atendidas.", _
                                                    "")

            cmdAplicarProposta.Enabled = False
        End If
        Set oEdsContestacao = Nothing
    End If
    'Alexandre Debouch - Confitec - Demanda EdsContestacao
End Sub





Private Sub chkImpedirAgendamento_Click()
    Dim bEnabled As Boolean

    bEnabled = True

    If chkImpedirAgendamento.Value = 1 Then
        If MsgBox("As altera��es realizadas nas parcelas ser�o perdidas. Deseja realmente continuar?", vbYesNo) = vbNo Then
            chkImpedirAgendamento.Value = 0
            Exit Sub
        End If

        bEnabled = False
    End If

    mskDtReativacao.Enabled = bEnabled
    grdParcelasReativacao.Enabled = bEnabled
    mskValParcelaReativacao.Enabled = bEnabled
    mskDtParcelaReativacao.Enabled = bEnabled
    CmdIncluirParcelaReativacao.Enabled = bEnabled
    cmdAlterarParcelaReativacao.Enabled = bEnabled
    cmdExcluirParcelaReativacao.Enabled = bEnabled
    cmdBtnReativacaoProposta(2).Enabled = bEnabled
    cmdBtnReativacaoProposta(3).Enabled = bEnabled
End Sub

Private Sub chkIsento_IOF_Click()

    Call CalcularValoresCobranca

End Sub

Private Sub cmbTaxaJuros_Click()

    If cmbTaxaJuros.ListIndex > 0 Then
        If Val(mskNumParcelas.Text) > 0 Then
            If IsDate(mskDtPagamento.Text) And IsDate(mskDtEndosso.Text) Then
                If DateDiff("d", mskDtPagamento.Text, mskDtEndosso.Text) = 0 Then
                    Call CalcularValorJuros("A")
                Else
                    Call CalcularValorJuros("P")
                End If
            Else
                Call CalcularValorJuros("P")
            End If
        Else
            mensagem_erro 3, "N�mero de Parcelas"
            cmbTaxaJuros.ListIndex = 0
            mskNumParcelas.SetFocus
            Exit Sub
        End If
    Else
        oDadosEndosso.valTaxaJuros = 1
        mskValJuros.Text = "0,00"
        Call CalcularValoresCobranca
    End If

End Sub

Public Sub CalcularValorJuros(ByVal sTipoJuros As String)

    Dim oDadosBasicos As Object
    Dim rs As Recordset

    On Error GoTo TrataErro

    Set oDadosBasicos = CreateObject("SEGL0022.cls00117")

    Set rs = oDadosBasicos.ConsultarJurosParcelamento(gsSIGLASISTEMA, _
                                                      App.Title, _
                                                      App.FileDescription, _
                                                      glAmbiente_id, _
                                                      cmbTaxaJuros.ItemData(cmbTaxaJuros.ListIndex), _
                                                      mskNumParcelas.Text, _
                                                      sTipoJuros)

    Set oDadosBasicos = Nothing

    If Not rs.EOF Then
        oDadosEndosso.valTaxaJuros = CCur(rs("fat_fracionamento"))
        mskValJuros.Text = Format$(((CCur(mskValPremioTarifa.Text) - CCur(mskValDesc.Text)) * oDadosEndosso.valTaxaJuros) - (CCur(mskValPremioTarifa.Text) - CCur(mskValDesc.Text)), "#,##0.00")
    End If

    Call CalcularValoresCobranca

    Set rs = Nothing

    Exit Sub

TrataErro:
    Call TratarErro("CalcularValorJuros", Me.name)
    Call FinalizarAplicacao

End Sub

Private Sub cmdAdicionarBenef_Click()

    MousePointer = vbHourglass

    Call AdicionarBeneficiario

    MousePointer = vbDefault

End Sub

Private Sub cmdAlterar_Click()

End Sub

Private Sub cmdAlterarBenef_Click()

    MousePointer = vbHourglass

    Call AlterarBeneficiario

    MousePointer = vbDefault

End Sub

Sub AlterarBeneficiario()

    Dim oBeneficiario As Object

    On Error GoTo Erro

    'Checa preenchimento dos campos

    If Trim(txtNomeBenef(0).Text) = "" Then
        mensagem_erro 3, "Nome Benefici�rio"
        txtNomeBenef(0).SetFocus
        Exit Sub
    End If

    Set oBeneficiario = colBeneficiario.Item(grdBeneficiario.TextMatrix(grdBeneficiario.Row, 0))

    With oBeneficiario

        .sNome = Trim(txtNomeBenef(0).Text)
        .sEndereco = Trim(txtNomeBenef(1).Text)
        .sBairro = Trim(txtBairroBenef(0).Text)

        If cboCidadeBenef.ListIndex > -1 Then
            .sCidade = Left(Trim(cboCidadeBenef.List(cboCidadeBenef.ListIndex)), Len(cboCidadeBenef.List(cboCidadeBenef.ListIndex)) - 5)
        Else
            .sCidade = ""
        End If

        .sEstado = Trim(txtBairroBenef(2).Text)
        .sPais = Trim(txtBairroBenef(1).Text)

        If Trim(txtCepBenef.ClipText) = "" Then
            .sCep = ""
        Else
            .sCep = Trim(txtCepBenef.Text)
        End If

        .sOperacao = "A"

    End With

    Call PreencherGridBeneficiario(colBeneficiario)

    Set oBeneficiario = Nothing

    Call LimparCamposBenficiario

    cmdAlterarBenef.Enabled = False
    cmdRemoverBenef.Enabled = False
    'cmdAdicionarBenef.Enabled = False

    cmdAplicarBenef.Enabled = True

    Exit Sub

Erro:
    Call TratarErro("AlterarBeneficiario", Me.name)
    Call FinalizarAplicacao
End Sub

Private Sub cmdAlterarParcelaReativacao_Click()
    Dim iCont As Integer
    '15/12/2015 - Schoralick (nova consultoria) - 18690344 - Melhorias Processo Reagendamento e Reativa��o BESC - inicio
    Dim sValAntCol1 As String
    Dim sValAntCol2 As String
    Dim sValAntCol3 As String
    Dim sLinhas As String
    Dim i As Integer
    Dim numseq As Integer
    Dim lMsg As String
    Dim sData1 As String
    Dim sData2 As String
    'fim

    If grdParcelasReativacao.Rows > 1 Then
        If Not IsNumeric(mskValParcelaReativacao.Text) Then
            MsgBox "Valor da parcela inv�lido", vbCritical, App.Title
            mskValParcelaReativacao.SetFocus
            Exit Sub
        End If

        If Not Val(mskValParcelaReativacao.Text) > 0 And oDadosEndosso.produtoId <> 1152 And oDadosEndosso.produtoId <> 1204 And oDadosEndosso.produtoId <> 1240 Then    '16/07/2018 - Schoralick (ntendencia) - 00421597-seguro-faturamento-pecuario [add produto 1240]
            MsgBox "Valor da parcela inv�lido", vbCritical, App.Title
            mskValParcelaReativacao.SetFocus
            Exit Sub
        End If

        If Not VerificaData2(mskDtParcelaReativacao.Text) Then
            MsgBox "Data da parcela inv�lida", vbCritical, App.Title
            mskDtParcelaReativacao.SetFocus
            Exit Sub
        End If

        'pamela.maia 28/04/2014 FLOW_17872929
        If CDate(mskDtParcelaReativacao.Text) < CDate(txtIniVigencia.Text) Then
            MsgBox "Data da parcela n�o pode estar fora da vig�ncia da proposta.", vbExclamation, App.Title
            mskDtParcelaReativacao.SetFocus
            Exit Sub
        Else
            If (txtFimVigencia.Text <> "") Then
                If CDate(mskDtParcelaReativacao.Text) > CDate(txtFimVigencia.Text) Then
                    MsgBox "Data da parcela n�o pode estar fora da vig�ncia da proposta.", vbExclamation, App.Title
                    mskDtParcelaReativacao.SetFocus
                    Exit Sub
                End If
            End If
        End If

        If CDate(mskDtParcelaReativacao) < CDate(Data_Sistema) Then
            MsgBox ("O agendamento est� inferior � data do sistema")
            Exit Sub
        End If

        sData1 = Mid$(Format(CDate(mskDtParcelaReativacao.Text), "yyyymmdd"), 1, 6)
        Select Case vlProdutoId
        Case 200, 201, 202, 203, 204, 205, 208, 209, 210, 211, 213, 216, 217, 218, 219, 221, 222, 223, 224, 225, 226
            If grdParcelasReativacao.TextMatrix(iCont, 3) <> "c" Then
                For iCont = 0 To grdParcelasReativacao.Rows - 1
                    sData2 = grdParcelasReativacao.TextMatrix(iCont, 4)
                    If sData1 = sData2 Then
                        MsgBox ("A data escolhida j� existe, relativo ao ano e mes de referecia da parcela.")
                        Exit Sub
                    End If
                Next
            End If

            sLinhas = montaLinha(grdParcelasReativacao.TextMatrix(grdParcelasReativacao.Row, 0), mskDtParcelaReativacao.Text, mskValParcelaReativacao.Text)

            lMsg = ValidaParcela(sLinhas)
            If lMsg <> "" Then
                MsgBox (lMsg)
                Exit Sub
            End If

            If grdParcelasReativacao.TextMatrix(grdParcelasReativacao.Row, 3) <> "c" Then
                grdParcelasReativacao.TextMatrix(grdParcelasReativacao.Row, 1) = sData1
            End If
        Case Else
            'lrocha 20/08/2009 - Tratamento 987 - Recusa: "A DEFINIR"
            For iCont = 0 To grdParcelasReativacao.Rows - 1
                If (mskDtParcelaReativacao.Text = grdParcelasReativacao.TextMatrix(iCont, 2)) And _
                   (iCont <> grdParcelasReativacao.Row) And oDadosEndosso.produtoId <> 1152 And oDadosEndosso.produtoId <> 1204 And oDadosEndosso.produtoId <> 1240 Then    '16/07/2018 - Schoralick (ntendencia) - 00421597-seguro-faturamento-pecuario [add produto 1240]
                    MsgBox ("A data escolhida j� existe")
                    Exit Sub
                End If
            Next
        End Select

        grdParcelasReativacao.TextMatrix(grdParcelasReativacao.Row, 1) = mskValParcelaReativacao.Text
        grdParcelasReativacao.TextMatrix(grdParcelasReativacao.Row, 2) = mskDtParcelaReativacao.Text
    End If
    '20-12-2015 - Schoralick (nova consultoria) - 18690344 - Melhorias Processo Reagendamento e Reativa��o BESC - fim
End Sub

Private Sub cmdAplicar_Click(Index As Integer)

    Dim sProc As String
    Dim SQL As String
    Dim sSQL As String
    Dim rc As Recordset
    Dim rc2 As rdoResultset
    Dim rsParcelas As Recordset
    Dim Parcelas As Object


    'Redu��o de IS
    If Index = 0 Then

        If TxtArea(1).Text = "" Then
            MsgBox "� necess�rio informar a �rea (ha) afetada pelo sinistro. ", vbInformation, "SEGP0777"
            TxtArea(1).SetFocus
            Exit Sub
        End If

        If CLng(txtProduto.Text) = 1152 Then
            If Val(TxtArea(6).Text) < Val(grdSinistros.TextMatrix(grdSinistros.Row, 6)) Then
                MsgBox "A Import�ncia segurada n�o pode ser menor que o valor da indeniza��o.", vbInformation, "SEGP0777-Redu��o de IS"
                Exit Sub
            End If
        End If

        'rdocn.Open
        'rdocn.BeginTrans

        ''''' VEIRIFICA SE NOVA IS CALCULADA � MAIOR QUE ZERO
        If TxtArea(5).Text > 0 Then

            ' Reduzir IS
            ' Ao reduzir a IS , encerrar a vigencia da cobertura e abrir
            ' outra vigencia.
            sProc = " escolha_tp_cob_reducao_IS_spu (Proposta_id = " & grdSinistros.TextMatrix(grdSinistros.Row, 0) & ")"
            SQL = "set nocount on " & vbCrLf
            SQL = SQL & "exec escolha_tp_cob_reducao_IS_spu "
            SQL = SQL & grdSinistros.TextMatrix(grdSinistros.Row, 7)
            SQL = SQL & ", " & grdSinistros.TextMatrix(grdSinistros.Row, 2)
            SQL = SQL & ", " & grdSinistros.TextMatrix(grdSinistros.Row, 13)
            SQL = SQL & ", " & grdSinistros.TextMatrix(grdSinistros.Row, 0)
            SQL = SQL & ", " & grdSinistros.TextMatrix(grdSinistros.Row, 11)
            SQL = SQL & ", " & TrocaVirgulaPorPonto(Format(TxtArea(6).Text - TxtArea(5).Text, "##0.00"))
            SQL = SQL & ", '" & Format(Data_Sistema, "yyyymmdd") & "'"
            SQL = SQL & ", Null "
            ' Caso o segurado tenha manifestado a inten��o de reintegrar
            ' a IS da cobertura (campo "processa_reintegracao_is"), ent�o
            ' o endosso de redu��o ser� gravado com status de "P",
            ' no campo situacao_reintegra��o, que significa que est� pendente
            ' de an�lise para verificar se haver� reintegra��o. Caso contr�rio,
            ' fica com "N" (n�o haver� reintegra��o).
            SQL = SQL & ", 'P' "
            SQL = SQL & ", '" & cUserName & "'" & vbCrLf
            SQL = SQL & "set nocount off "

            If rdocn Is Nothing Then
                Call Conexao
            Else
                If rdocn.State = 0 Then Call Conexao
            End If

            Set rc = rdocn.Execute(SQL)

            Dim endosso_id As Long
            endosso_id = rc!endosso_id

        End If

        SQL = "EXEC pgto_sinistro_reducao_is_spu "
        SQL = SQL & " " & grdSinistros.TextMatrix(grdSinistros.Row, 1)
        SQL = SQL & ", " & grdSinistros.TextMatrix(grdSinistros.Row, 10)
        SQL = SQL & ", " & grdSinistros.TextMatrix(grdSinistros.Row, 8)
        SQL = SQL & ", " & grdSinistros.TextMatrix(grdSinistros.Row, 9)
        SQL = SQL & ", " & grdSinistros.TextMatrix(grdSinistros.Row, 7)
        SQL = SQL & ", '" & Format(Data_Sistema, "yyyymmdd") & "'"    '@dt_reducao_is
        SQL = SQL & ", " & endosso_id
        SQL = SQL & ", '" & cUserName & "'"    '@usuario

        If rdocn Is Nothing Then
            Call Conexao
        Else
            If rdocn.State = 0 Then Call Conexao
        End If

        Set rc = rdocn.Execute(SQL)
        '''''
        sSQL = ""
        sSQL = sSQL & " SELECT endosso_tb.tp_endosso_id, " & vbNewLine
        sSQL = sSQL & "        tp_endosso_tb.cod_evento_als, " & vbNewLine
        sSQL = sSQL & "        proposta_tb.produto_id " & vbNewLine
        sSQL = sSQL & " FROM endosso_tb  WITH (NOLOCK)  " & vbNewLine
        sSQL = sSQL & " JOIN tp_endosso_tb  WITH (NOLOCK)  " & vbNewLine
        sSQL = sSQL & "   ON endosso_tb.tp_endosso_id = tp_endosso_tb.tp_endosso_id " & vbNewLine
        sSQL = sSQL & " JOIN proposta_tb  WITH (NOLOCK)  " & vbNewLine
        sSQL = sSQL & "   ON proposta_tb.proposta_id = endosso_tb.proposta_id "
        sSQL = sSQL & " WHERE endosso_tb.proposta_id = " & grdSinistros.TextMatrix(grdSinistros.Row, 0) & vbNewLine
        sSQL = sSQL & "   AND endosso_tb.endosso_id = " & endosso_id & vbNewLine

        If rdocn Is Nothing Then
            Call Conexao
        Else
            If rdocn.State = 0 Then Call Conexao
        End If

        Set rc = rdocn.Execute(SQL)

        Dim OEndossoAux As Object
        Set OEndossoAux = CreateObject("SEGL0026.cls00124")

        'Call OEndossoAux.GerarEndossoals(cUserName, gsSIGLASISTEMA, App.Title, App.FileDescription, glAmbiente_id, Data_Sistema, CLng(grdSinistros.TextMatrix(grdSinistros.Row, 0)), Format(Data_Sistema, "yyyymm"), Data_Sistema, CLng(endosso_id), 0, rc(0), 1, 0, rc(1))
        'Redu��o passa o tp_endosso_id 94 e evento 3817
        Call OEndossoAux.GerarEndossoAls(cUserName, gsSIGLASISTEMA, App.Title, App.FileDescription, glAmbiente_id, Data_Sistema, CLng(grdSinistros.TextMatrix(grdSinistros.Row, 0)), Format(Data_Sistema, "yyyymm"), Data_Sistema, CLng(endosso_id), 0, 94, 1, 0, 3817)

        SQL = "EXEC pgto_sinistro_reducao_is_spu "
        SQL = SQL & " " & grdSinistros.TextMatrix(grdSinistros.Row, 1)
        SQL = SQL & ", " & grdSinistros.TextMatrix(grdSinistros.Row, 10)
        SQL = SQL & ", " & grdSinistros.TextMatrix(grdSinistros.Row, 8)
        SQL = SQL & ", " & grdSinistros.TextMatrix(grdSinistros.Row, 9)
        SQL = SQL & ", " & grdSinistros.TextMatrix(grdSinistros.Row, 7)
        SQL = SQL & ", '" & Format(Data_Sistema, "yyyymmdd") & "'"    '@dt_reducao_is
        SQL = SQL & ", " & endosso_id
        SQL = SQL & ", '" & cUserName & "'"    '@usuario
        '
        If rdocn Is Nothing Then
            Call Conexao
        Else
            If rdocn.State = 0 Then Call Conexao
        End If

        Set rc = rdocn.Execute(SQL)

        'Atualiza campo nova �rea, para futuramente verificar at� quanto uma �rea pode ser reintegrada
        'sem ultrapassar o limite da �rea original do objeto segurado

        SQL = "EXEC SEGS9789_SPU "
        SQL = SQL & " '" & cUserName & "'"
        SQL = SQL & ", " & grdSinistros.TextMatrix(grdSinistros.Row, 0)
        SQL = SQL & ", " & grdSinistros.TextMatrix(grdSinistros.Row, 11)
        SQL = SQL & ", '" & TxtArea(2).Text & "'"

        If rdocn Is Nothing Then
            Call Conexao
        Else
            If rdocn.State = 0 Then Call Conexao
        End If

        Set rc = rdocn.Execute(SQL)

        MsgBox "O endosso de redu�ao de IS foi gerado com sucesso.", vbInformation, "SEGP0777 - Redu��o de IS"

        Call grdSinistros_Click
        'grdReintegracao(1).Clear
        'Call Carregar_Dados_Reintegracao_IS(CLng(txtPropostaid(0).Text))


        'Reintegra��o de IS
    ElseIf Index = 2 Then

        If grdReintegracao(1).Row < 1 Then
            MsgBox "Reintegra��o n�o dispon�vel ou j� realizada.", vbInformation, "SEGP0777 - Reintegra��o de IS"
            Exit Sub
        End If

        'If txtProduto.Text = "1204" Then
        If TxtArea(18).Text = "" Then
            MsgBox "�rea a reintegrar deve ser preenchida.", vbInformation, "SEGP0777 - Reintegra��o de IS"
            Exit Sub
        End If


        Dim Endosso_id_novo As String
        'Dim Seq_Cob As Integer


        Proposta_id_ant = grdReintegracao(1).TextMatrix(grdReintegracao(1).Row, 0)
        Sinistro_id_ant = grdReintegracao(1).TextMatrix(grdReintegracao(1).Row, 2)
        Apolice_id_ant = grdReintegracao(1).TextMatrix(grdReintegracao(1).Row, 3)
        Sucursal_ant = grdReintegracao(1).TextMatrix(grdReintegracao(1).Row, 4)
        Seguradora_ant = grdReintegracao(1).TextMatrix(grdReintegracao(1).Row, 5)
        ramo_id_ant = grdReintegracao(1).TextMatrix(grdReintegracao(1).Row, 6)
        Endosso_id_novo = "NULL"
        Seq_Cob = 0
        Dt_endosso_ant = Format(grdReintegracao(1).TextMatrix(grdReintegracao(1).Row, 7), "DD/MM/YYYY")
        Dt_pedido_endosso_ant = Format(grdReintegracao(1).TextMatrix(grdReintegracao(1).Row, 8), "DD/MM/YYYY")
        Produto_id_ant = grdReintegracao(1).TextMatrix(grdReintegracao(1).Row, 9)
        Num_Cartao_ant = Format(grdReintegracao(1).TextMatrix(grdReintegracao(1).Row, 17), "DD/MM/YYYY")
        Dt_Validade_Cartao_ant = Format(grdReintegracao(1).TextMatrix(grdReintegracao(1).Row, 18), "DD/MM/YYYY")
        Banco_ant = grdReintegracao(1).TextMatrix(grdReintegracao(1).Row, 14)
        Agencia_ant = grdReintegracao(1).TextMatrix(grdReintegracao(1).Row, 15)
        Conta_Corrente_ant = grdReintegracao(1).TextMatrix(grdReintegracao(1).Row, 16)
        Dt_Inicio_Vigencia_ant = Format(grdReintegracao(1).TextMatrix(grdReintegracao(1).Row, 19), "DD/MM/YYYY")
        seguro_moeda_id_ant = grdReintegracao(1).TextMatrix(grdReintegracao(1).Row, 20)

        'Obtem os dados que ser�o gravados na emi_re_proposta_tb
        '        Call Obtem_Dados_emi_RE _
                 '            (Proposta_id_ant, Proposta_BB_ant, Arquivo_ant, _
                 '            Cod_ramo_BB_ant, Tp_Pessoa_ant, CPF_ant, _
                 '            CGC_ant, Dt_Nascimento_ant, Tp_Pgto_ant, _
                 '            Dt_Inicio_Vigencia_Aux, Proponente_ant)

        SQL = "exec escolha_tp_cob_reintegracao_IS_spu "
        SQL = SQL & grdReintegracao(1).TextMatrix(grdReintegracao(1).Row, 0)
        SQL = SQL & ", " & CInt(txtProduto.Text)
        SQL = SQL & ", " & grdReintegracao(1).TextMatrix(grdReintegracao(1).Row, 1)
        SQL = SQL & ",  Null  "
        SQL = SQL & ", " & grdReintegracao(1).TextMatrix(grdReintegracao(1).Row, 6)
        SQL = SQL & ", " & grdReintegracao(1).TextMatrix(grdReintegracao(1).Row, 10)
        SQL = SQL & ", " & grdReintegracao(1).TextMatrix(grdReintegracao(1).Row, 12)
        SQL = SQL & ", " & TrocaVirgulaPorPonto(Format(TxtArea(19).Text - TxtArea(32).Text, "##0.00"))
        SQL = SQL & ", '" & cUserName & "'"

        If rdocn Is Nothing Then
            Call Conexao
        Else
            If rdocn.State = 0 Then Call Conexao
        End If

        Set rc = rdocn.Execute(SQL)

        'Obt�m o endosso_id gerado pela stored procedure
        Endosso_id_novo = rc!endosso_id


        Seq_Cob = Seq_Cob + 1

        ReDim Preserve Coberturas(Seq_Cob) As tp_Cob
        Coberturas(Seq_Cob).ID = grdReintegracao(1).TextMatrix(grdReintegracao(1).Row, 10)
        Coberturas(Seq_Cob).Obj_id = grdReintegracao(1).TextMatrix(grdReintegracao(1).Row, 12)
        'Coberturas(Seq_Cob).Valor_a_Reintegrar = TxtArea(19).Text
        Coberturas(Seq_Cob).Valor_a_Reintegrar = TxtArea(19).Text - TxtArea(32).Text
        Coberturas(Seq_Cob).Novo_Valor_IS = TxtArea(19).Text
        Coberturas(Seq_Cob).Nome = grdReintegracao(1).TextMatrix(grdReintegracao(1).Row, 12)


        'Marca a data para n�o processar mais e grava o endosso_id gerado
        SQL = "EXEC pgto_sinistro_reintegracao_is_spu "
        SQL = SQL & " " & grdReintegracao(1).TextMatrix(grdReintegracao(1).Row, 2)
        SQL = SQL & ", " & grdReintegracao(1).TextMatrix(grdReintegracao(1).Row, 12)
        SQL = SQL & ", " & grdReintegracao(1).TextMatrix(grdReintegracao(1).Row, 3)
        SQL = SQL & ", " & grdReintegracao(1).TextMatrix(grdReintegracao(1).Row, 5)
        SQL = SQL & ", " & grdReintegracao(1).TextMatrix(grdReintegracao(1).Row, 6)
        SQL = SQL & ", '" & Format(Data_Sistema, "yyyymmdd") & "'"    '@dt_reintegracao_is
        SQL = SQL & ", " & grdReintegracao(1).TextMatrix(grdReintegracao(1).Row, 1)
        SQL = SQL & ", " & Endosso_id_novo              '@reintegracao_endosso_id
        SQL = SQL & ", '" & cUserName & "'"             '@usuario

        If rdocn Is Nothing Then
            Call Conexao
        Else
            If rdocn.State = 0 Then Call Conexao
        End If

        Set rc = rdocn.Execute(SQL)

        With grpProposta
            .ID = grdReintegracao(1).TextMatrix(grdReintegracao(1).Row, 0)
            .Produto_id = CInt(txtProduto.Text)
            .Apolice_id = grdReintegracao(1).TextMatrix(grdReintegracao(1).Row, 3)
            .Sucursal_id = grdReintegracao(1).TextMatrix(grdReintegracao(1).Row, 4)
            .Seguradora_id = grdReintegracao(1).TextMatrix(grdReintegracao(1).Row, 5)
            .ramo_id = grdReintegracao(1).TextMatrix(grdReintegracao(1).Row, 6)
            .endosso_id = Endosso_id_novo
            .Dt_Endosso = grdReintegracao(1).TextMatrix(grdReintegracao(1).Row, 7)
            .Dt_Pedido_Endosso = grdReintegracao(1).TextMatrix(grdReintegracao(1).Row, 8)
            .Num_Cartao = grdReintegracao(1).TextMatrix(grdReintegracao(1).Row, 17)
            .Dt_Validade_Cartao = grdReintegracao(1).TextMatrix(grdReintegracao(1).Row, 18)
            .Banco = grdReintegracao(1).TextMatrix(grdReintegracao(1).Row, 14)
            .Agencia = grdReintegracao(1).TextMatrix(grdReintegracao(1).Row, 15)
            .Conta_Corrente = grdReintegracao(1).TextMatrix(grdReintegracao(1).Row, 16)
            .Dt_Inicio_Vigencia = grdReintegracao(1).TextMatrix(grdReintegracao(1).Row, 19)
            .Seguro_Moeda_id = grdReintegracao(1).TextMatrix(grdReintegracao(1).Row, 20)
            .sTipoEndosso = "Reintegra��o Agricola"

        End With

        Dim iSubRamo As Integer

        Call BuscaSubRamo(grdReintegracao(1).TextMatrix(grdReintegracao(1).Row, 0), iSubRamo)

        Call Gera_Cobranca_da_Reintegracao(iSubRamo, grpProposta.Dt_Pedido_Endosso, grpProposta, Coberturas)


        sSQL = ""
        sSQL = sSQL & " SELECT val_financeiro " & vbNewLine
        sSQL = sSQL & " FROM endosso_financeiro_tb  WITH (NOLOCK)  " & vbNewLine
        sSQL = sSQL & " JOIN proposta_tb  WITH (NOLOCK)  " & vbNewLine
        sSQL = sSQL & "   ON proposta_tb.proposta_id = endosso_financeiro_tb.proposta_id "
        sSQL = sSQL & " WHERE endosso_financeiro_tb.proposta_id = " & grdReintegracao(1).TextMatrix(grdReintegracao(1).Row, 0) & vbNewLine
        sSQL = sSQL & "   AND endosso_financeiro_tb.endosso_id = " & Endosso_id_novo & vbNewLine

        If rdocn Is Nothing Then
            Call Conexao
        Else
            If rdocn.State = 0 Then Call Conexao
        End If

        Set rc = rdocn.Execute(sSQL)

        Dim OEndossoRei As Object
        Dim dValFinanceiro As Double
        Dim cValFinanceiro As Currency

        Set OEndossoRei = CreateObject("SEGL0026.cls00124")

        dValFinanceiro = Format(rc!val_financeiro, "##0.00")

        'Call OEndossoAux.GerarEndossoals(cUserName, gsSIGLASISTEMA, App.Title, App.FileDescription, glAmbiente_id, Data_Sistema, CLng(grdSinistros.TextMatrix(grdSinistros.Row, 0)), Format(Data_Sistema, "yyyymm"), Data_Sistema, CLng(endosso_id), 0, rc , 1, 0, rc(1))
        'Reintegra��o passa o tp_endosso_id 61 e evento 3800
        Call OEndossoRei.GerarEndossoAls(cUserName, gsSIGLASISTEMA, App.Title, App.FileDescription, glAmbiente_id, Data_Sistema, CLng(grdReintegracao(1).TextMatrix(grdReintegracao(1).Row, 0)), Format(Data_Sistema, "yyyymm"), Data_Sistema, CLng(Endosso_id_novo), dValFinanceiro, 61, 2, 0, 3815, Data_Sistema)

        'Atualiza campo nova �rea, para futuramente verificar at� quanto uma �rea pode ser reintegrada
        'sem ultrapassar o limite da �rea original do objeto segurado

        SQL = "EXEC SEGS9789_SPU "
        SQL = SQL & " '" & cUserName & "'"
        SQL = SQL & ", " & grdReintegracao(1).TextMatrix(grdReintegracao(1).Row, 0)
        SQL = SQL & ", 1 "
        SQL = SQL & ", '" & TxtArea(18).Text & "'"

        If rdocn Is Nothing Then
            Call Conexao
        Else
            If rdocn.State = 0 Then Call Conexao
        End If

        Set rc = rdocn.Execute(SQL)

        MsgBox "O endosso de reintegra��o de IS foi gerado com sucesso.", vbInformation, "SEGP0777 - Redu��o de IS"

        grdReintegracao(1).Rows = 1
        Call Carregar_Dados_Reintegracao_IS(CLng(txtPropostaid(0).Text))


    ElseIf Index = 3 Then
        'Adequa��o de �rea

        Dim iRamo As Integer

        '-- VALIDA��O DE PARCELAS PAGAS ----------------------------------------------------------------------------------------------
        ' EM CASO DE REDU��O (RELY)
        If txtProduto = "1204" Then
            If Val(TxtArea(7).Text) < Val(TxtArea(9).Text) Then

                Set Parcelas = CreateObject("SEGL0026.cls00126")
                Set rsParcelas = Parcelas.Valida_Parcelas(gsSIGLASISTEMA, _
                                                          App.Title, _
                                                          App.FileDescription, _
                                                          glAmbiente_id, _
                                                          grdReintegracao(0).TextMatrix(grdReintegracao(0).Row, 0), _
                                                          grdReintegracao(0).TextMatrix(grdReintegracao(0).Row, 1))

                If Not rsParcelas.EOF Then
                    If rsParcelas("situacao") = "p" Then
                        MsgBox "N�o � poss�vel diminuir a �rea vigente porque a proposta possui parcelas n�o pagas.", vbInformation, "SEGP0777-Adequa��o de �rea"
                        Exit Sub
                    End If
                End If

            End If
        End If

        '------------------------------------------------------------------------------------------------------------------------------

        'Call BuscaRamo(grdReintegracao(0).TextMatrix(grdReintegracao(0).Row, 0), iRamo)

        SQL = "EXEC SEGS9824_SPI "
        SQL = SQL & " '" & cUserName & "'"
        SQL = SQL & "," & grdReintegracao(0).TextMatrix(grdReintegracao(0).Row, 0)
        SQL = SQL & "," & txtProduto.Text
        SQL = SQL & "," & TrocaVirgulaPorPonto(TxtArea(24).Text)
        SQL = SQL & "," & TrocaVirgulaPorPonto(TxtArea(25).Text)
        SQL = SQL & "," & TrocaVirgulaPorPonto(TxtArea(26).Text)
        SQL = SQL & "," & TrocaVirgulaPorPonto(TxtArea(9).Text)
        SQL = SQL & "," & TrocaVirgulaPorPonto(TxtArea(7).Text)
        SQL = SQL & "," & TrocaVirgulaPorPonto(TxtArea(27).Text)
        If txtProduto = "1204" Then
            SQL = SQL & "," & TrocaVirgulaPorPonto(TxtArea(23).Text)
        Else
            SQL = SQL & ",0 "
        End If
        SQL = SQL & "," & TrocaVirgulaPorPonto(TxtArea(8).Text)
        SQL = SQL & "," & grdReintegracao(0).TextMatrix(grdReintegracao(0).Row, 6)
        If txtProduto = "1204" Then
            SQL = SQL & "," & 835
        ElseIf txtProduto = "1152" Then
            SQL = SQL & "," & 339
        End If
        SQL = SQL & "," & grdReintegracao(0).TextMatrix(grdReintegracao(0).Row, 1)
        SQL = SQL & "," & grdReintegracao(0).TextMatrix(grdReintegracao(0).Row, 13)
        '� 1204 e n�o possui endosso de redu��o (afeta duas coberturas 834 e 835)
        If txtProduto = "1204" And TxtArea(33).Text = "N" Then
            SQL = SQL & "," & " 'N' "

            '� 1204 possui endosso de redu��o (afeta somente a cobertura 835)
        ElseIf txtProduto = "1204" And TxtArea(33).Text = "S" Then
            SQL = SQL & "," & " 'S' "
            'Independente de endosso s� afeta a b�sica (339)
        ElseIf txtProduto = "1152" Then
            SQL = SQL & "," & " 'S' "
        End If

        If rdocn Is Nothing Then
            Call Conexao
        Else
            If rdocn.State = 0 Then Call Conexao
        End If

        Set rc = rdocn.Execute(SQL)

        'Atualiza campo nova �rea, para futuramente verificar at� quanto uma �rea pode ser reintegrada
        'sem ultrapassar o limite da �rea original do objeto segurado

        SQL = "EXEC SEGS9789_SPU "
        SQL = SQL & " '" & cUserName & "'"
        SQL = SQL & ", " & grdReintegracao(0).TextMatrix(grdReintegracao(0).Row, 0)
        SQL = SQL & ", 1 "
        SQL = SQL & ", '" & TxtArea(7).Text & "'"

        If rdocn Is Nothing Then
            Call Conexao
        Else
            If rdocn.State = 0 Then Call Conexao
        End If

        Set rc = rdocn.Execute(SQL)

        MsgBox "Endosso de adequa��o de �rea inserido com sucesso", vbInformation, "SEGP0777 - Adequa��o de �rea"


    ElseIf Index = 4 Then
        'Adequa��o de produtividade

        If TxtArea(46).Text = "" Then
            MsgBox "� necess�rio informar a nova produtividade para a adequa��o. ", vbInformation, "SEGP0777"
            TxtArea(46).SetFocus
            Exit Sub
        End If


        '-- VALIDA��O DE PARCELAS PAGAS --------------------------------------------------------------------------------------------------------
        ' EM CASO DE REDU��O (RELY)
        If txtProduto = "1204" Then
            If Val(TxtArea(46).Text) < Val(TxtArea(20).Text) Then

                Set Parcelas = CreateObject("SEGL0026.cls00126")
                Set rsParcelas = Parcelas.Valida_Parcelas(gsSIGLASISTEMA, _
                                                          App.Title, _
                                                          App.FileDescription, _
                                                          glAmbiente_id, _
                                                          grdReintegracao(2).TextMatrix(grdReintegracao(0).Row, 0), _
                                                          grdReintegracao(2).TextMatrix(grdReintegracao(0).Row, 1))

                If Not rsParcelas.EOF Then
                    If rsParcelas("situacao") = "p" Then
                        MsgBox "N�o � poss�vel diminuir a produtividade vigente porque a proposta possui parcelas n�o pagas.", vbInformation, "SEGP0777-Adequa��o de produtividade"
                        Exit Sub
                    End If
                End If

            End If
        End If
        '---------------------------------------------------------------------------------------------------------------------------------------

        If rdocn Is Nothing Then
            Call Conexao
        Else
            If rdocn.State = 0 Then Call Conexao
        End If

        SQL = "EXEC SEGS9825_SPI "
        SQL = SQL & " '" & cUserName & "'"
        SQL = SQL & "," & grdReintegracao(2).TextMatrix(grdReintegracao(2).Row, 0)
        SQL = SQL & "," & txtProduto.Text
        SQL = SQL & "," & TrocaVirgulaPorPonto(Format(TxtArea(20).Text, "##0.00"))
        If txtProduto = "1204" Then
            SQL = SQL & "," & TrocaVirgulaPorPonto(Format(TxtArea(45).Text, "##0.00"))
        ElseIf txtProduto = "1152" Then
            SQL = SQL & ",0 "
        End If
        SQL = SQL & "," & TrocaVirgulaPorPonto(Format(TxtArea(50).Text, "##0.00"))
        SQL = SQL & "," & TrocaVirgulaPorPonto(Format(TxtArea(50).Text, "##0.00"))
        SQL = SQL & "," & TrocaVirgulaPorPonto(TxtArea(13).Text)
        If txtProduto = "1204" Then
            SQL = SQL & "," & TrocaVirgulaPorPonto(Format(TxtArea(48).Text, "##0.00"))
        ElseIf txtProduto = "1152" Then
            SQL = SQL & ",0 "
        End If
        SQL = SQL & "," & TrocaVirgulaPorPonto(Format(TxtArea(51).Text, "##0.00"))
        SQL = SQL & "," & grdReintegracao(2).TextMatrix(grdReintegracao(2).Row, 6)
        SQL = SQL & "," & grdReintegracao(2).TextMatrix(grdReintegracao(2).Row, 10)
        SQL = SQL & "," & grdReintegracao(2).TextMatrix(grdReintegracao(2).Row, 1)
        SQL = SQL & "," & grdReintegracao(2).TextMatrix(grdReintegracao(2).Row, 9)
        SQL = SQL & "," & TrocaVirgulaPorPonto(Format(TxtArea(46).Text, "##0.00"))

        'EXEC SEGS9825_SPI  '',555001840,1152,2500,,398.00,398.00,0.00,,1200.00,2,339,0,790,2200

        '        'TESTE
        '        SQL = "EXEC SEGS9825_SPI 'rely' "
        '        SQL = SQL & "," & 15923370
        '        SQL = SQL & "," & 1204
        '        SQL = SQL & "," & 50
        '        SQL = SQL & "," & 14
        '        SQL = SQL & "," & 50
        '        SQL = SQL & "," & 14
        '        SQL = SQL & "," & 10
        '        SQL = SQL & "," & 9
        '        SQL = SQL & "," & 999

        Set rc = rdocn.Execute(SQL)

        MsgBox "Endosso de produtividade de �rea inserido com sucesso", vbInformation, "SEGP0777 - Produtividade de �rea"

        'RESITUI��O DE SUBS�DIO
    ElseIf Index = 5 Then

        Dim Reintegracao_Integral As String
        Dim Mapa As String

        '-- VERIFICA SE J� EXISTE RESTIUI��O DE SUBVEN��O PARA A PARCELA (RELY)
        '-----------------------------------------------------------------------------------------------------------------
        '            Dim rsSubvencao As Recordset
        '            Dim Subvencao As Object
        '            Set Subvencao = CreateObject("SEGL0026.cls00126")
        '            Set rsSubvencao = Subvencao.Valida_Parcela_Subvencao(gsSIGLASISTEMA, _
                     '                                                            App.Title, _
                     '                                                            App.FileDescription, _
                     '                                                            glAmbiente_id, _
                     '                                                            grdReintegracao(3).TextMatrix(grdReintegracao(3).Row, 0), _
                     '                                                            grdReintegracao(3).TextMatrix(grdReintegracao(3).Row, 1))
        '
        '
        '         If Not rsSubvencao.EOF Then
        '
        '            If rsSubvencao("existe_parcela") = "S" Then
        '                MsgBox "J� existe restitui��o de subven��o para a parcela " & grdReintegracao(3).TextMatrix(grdReintegracao(3).Row, 2) & " da proposta " & grdReintegracao(3).TextMatrix(grdReintegracao(3).Row, 0), vbInformation, "SEGP0777 - Restiui��o de subs�dio"
        '                    TxtArea(7).SetFocus
        '                    Exit Sub
        '            End If
        '
        '         End If


        If TxtArea(44).Text = "" Then
            MsgBox "A parcela selecionada n�o foi paga e n�o pode ser restituida", vbInformation, "SEGP0777 - Restiui��o de subs�dio"
            Exit Sub
        End If

        If IsNumeric(TxtArea(52).Text) = False Then
            MsgBox "O valor a ser resitu�do n�o � v�lido.", vbInformation, "SEGP0777 - Restiui��o de subs�dio"
            TxtArea(52).SetFocus
            Exit Sub
        End If

        If TxtArea(52).Text = "" Or Val(TxtArea(52).Text) > Val(grdReintegracao(3).TextMatrix(grdReintegracao(3).Row, 5)) Then
            MsgBox "Informe o valor a restituir e certifique-se que � menor ou igual ao valor pago da parcela. ", vbInformation, "SEGP0777 - Restiui��o de subs�dio"
            Exit Sub
        End If

        If TxtArea(52).Text < grdReintegracao(3).TextMatrix(grdReintegracao(3).Row, 5) Then
            Reintegracao_Integral = "N"
        Else
            Reintegracao_Integral = "S"
        End If

        If grdReintegracao(3).TextMatrix(grdReintegracao(3).Row, 3) = "MAPA" Then
            Mapa = "F"
        Else
            Mapa = "E"
        End If

        SQL = "EXEC SEGS9945_SPI "
        SQL = SQL & "'" & cUserName & "'"
        SQL = SQL & "," & grdReintegracao(3).TextMatrix(grdReintegracao(3).Row, 0)
        SQL = SQL & "," & TrocaVirgulaPorPonto(Format(TxtArea(52).Text, "##0.00"))
        SQL = SQL & ",'" & Mapa & "'"

        If rdocn Is Nothing Then
            Call Conexao
        Else
            If rdocn.State = 0 Then Call Conexao
        End If

        Set rc = rdocn.Execute(SQL)

        MsgBox "Restitui��o de subs�dio de pr�mio inclu�do com sucesso.", vbInformation, "SEGP0777 - Restitui��o de subs�dio"


    End If

    Call sair

End Sub

Private Sub cmdAplicarBenef_Click()

    MousePointer = vbHourglass

    Call ProcessarEndossoBeneficiario

    MousePointer = vbDefault

End Sub

Sub ProcessarEndossoBeneficiario()

    Dim OEndosso As Object

    On Error GoTo Erro

    Set OEndosso = CreateObject("SEGL0140.cls00172")

    If Not OEndosso.GerarEndossoBeneficiario(gsSIGLASISTEMA, App.Title, App.FileDescription, glAmbiente_id, _
                                             oDadosEndosso.TipoEndosso, oDadosEndosso.ApoliceId, _
                                             oDadosEndosso.SucursalSeguradoraId, oDadosEndosso.SeguradoraCodSusep, _
                                             oDadosEndosso.PropostaId, oDadosEndosso.RamoId, _
                                             mskDtEndosso.Text, mskDtEmissao.Text, _
                                             txtDescEndosso.Text, colBeneficiario, Trim(cUserName)) Then
        Exit Sub
    End If

    Call MsgBox("Endosso realizado com sucesso." & vbNewLine & _
                "Proposta: " & oDadosEndosso.PropostaId & vbNewLine & _
                "Endosso : " & txtNumEndosso.Text, vbInformation)

    Call sair

    Set OEndosso = Nothing

    Exit Sub

Erro:
    Call TratarErro("ProcessarEndossoBeneficiario", Me.name)
    Call FinalizarAplicacao
End Sub

Private Sub cmdAplicarCadastral_Click()

    Call AplicarCadastral

End Sub

Private Sub cmdAplicarCancApolice_Click()

    MousePointer = vbHourglass

    Call AplicarCancelamentoApolice

    MousePointer = vbDefault

End Sub

Private Sub cmdAplicarCancEndosso_Click()

    Call AplicarCancelamentoEndosso

End Sub

Private Sub cmdAplicarCancSinistro_Click()

    Call AplicarCancelamentoPorSinistro

End Sub
Private Sub cmdAplicarEst_Click()

    Call AplicarEndossoEstipulante

End Sub

Private Sub cmdAplicarIS_Click()

    MousePointer = vbHourglass

    Call AplicarEndossoIS

    MousePointer = vbDefault

End Sub

Private Sub AplicarEndossoIS()

    On Error GoTo Erro

    If cboObjetoSegurado.ListIndex = -1 Then
        Call mensagem_erro(3, "Objeto Segurado")
        cboObjetoSegurado.SetFocus
        Exit Sub
    End If

    If grdCoberturas.Rows < 2 And cmdBensAdicionar.Enabled Then
        If MsgBox("Existem dados de Import�ncia Segurada n�o Adicionados. Deseja realmente Retornar?", vbYesNo) = vbNo Then
            Exit Sub
        End If
    End If

    'Se pr�mio for a menor calcula usando curto prazo
    If cValPremioNovo < cValPremioAnterior Then
        sFlagTipoOperacao = "R"
        cValFinanceiro = cValPremioNovo - cValPremioAnterior
        Call CarregarDadosRestituicao
        Call CalcularValoresRestituicao
    ElseIf cValPremioNovo > cValPremioAnterior Then
        sFlagTipoOperacao = "C"
        cValFinanceiro = cValPremioNovo - cValPremioAnterior
        Call CarregarDadosCobranca
        Call CalcularValoresCobranca
    Else
        sFlagTipoOperacao = "S"
        cValFinanceiro = 0
        Call CarregarDadosSemCobranca
    End If

    stbEndosso.TabEnabled(10) = False

    Exit Sub

Erro:
    Call TratarErro("AplicarEndossoIS", Me.name)
    Call FinalizarAplicacao
End Sub

'@gbarbosa - 17/06/2004 - Endosso para Registro de Limite de Emiss�o
Private Sub cmdAplicarLimite_Click()

    Call AplicarLimiteEmissao

End Sub

Private Sub AplicarLimiteEmissao()

    Dim oEndossoLimite As Object

    ' Verificando se a data � v�lida ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

    If Trim(mskDtNovoLimite.Text) = "" Then
        Call MsgBox("Nova data de in�cio de vig�ncia n�o preenchida", vbCritical)
        mskDtNovoLimite.SetFocus
        Exit Sub
    End If

    If Not VerificaData2(mskDtNovoLimite.Text) Then
        Call MsgBox("Nova data de in�cio de vig�ncia inv�lida", vbCritical)
        mskDtNovoLimite.SetFocus
        Exit Sub
    End If

    If DateDiff("d", mskDtAtualLimite.Text, mskDtNovoLimite.Text) <= 0 Then
        Call MsgBox("Nova data de in�cio de vig�ncia n�o pode ser menor ou igual � anterior", vbCritical)
        mskDtNovoLimite.SetFocus
        Exit Sub
    End If

    If DateDiff("d", mskDtAtualLimite.Tag, mskDtNovoLimite.Text) > 0 Then
        Call MsgBox("Nova data de in�cio de vig�ncia n�o pode ser maior que o fim de vig�ncia anterior: " & mskDtAtualLimite.Tag, vbCritical)
        mskDtNovoLimite.SetFocus
        Exit Sub
    End If

    ' Verificando se o valor de limite de emiss�o � v�lido ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

    If Trim(mskLimiteAtual(1).Text) = "" Then
        Call MsgBox("Novo limite de emiss�o n�o preenchido", vbCritical)
        txtEndoCad(0).SetFocus
        Exit Sub
    End If

    If CCur(mskLimiteAtual(1).Text) = 0 Then
        Call MsgBox("Novo limite de emiss�o n�o preenchido", vbCritical)
        txtEndoCad(0).SetFocus
        Exit Sub
    End If

    MousePointer = vbHourglass

    Set oEndossoLimite = CreateObject("SEGL0140.cls00230")

    Call oEndossoLimite.GerarEndossoLimiteEmissao(gsSIGLASISTEMA, _
                                                  App.Title, _
                                                  App.FileDescription, _
                                                  glAmbiente_id, _
                                                  oDadosEndosso.PropostaId, _
                                                  oDadosEndosso.produtoId, _
                                                  oDadosEndosso.TipoEndosso, _
                                                  mskDtEndosso.Text, _
                                                  mskDtEmissao.Text, _
                                                  txtDescEndosso.Text, _
                                                  mskDtAtualLimite.Text, _
                                                  mskLimiteAtual(1).Text, _
                                                  mskDtNovoLimite.Text, _
                                                  Data_Sistema, _
                                                  cUserName)

    Set oEndossoLimite = Nothing

    MousePointer = vbDefault

    Call MsgBox("Endosso realizado com sucesso." & vbNewLine & _
                "Proposta: " & oDadosEndosso.PropostaId & vbNewLine & _
                "Endosso : " & txtNumEndosso.Text, vbInformation)

    Call sair

    Exit Sub

End Sub

Private Sub cmdAplicarProposta_Click()

    'Sergio Pires - Nova Tend�ncia - CCM Registro de Etapas no SGBR
    If cboTipoEndosso.ItemData(cboTipoEndosso.ListIndex) = TP_ENDOSSO_ALTERACAO_TIPO_ENVIO_DOCUMENTO Then
        If frmSelecao.grdSelecao.TextMatrix(frmSelecao.grdSelecao.Row, 7) = "Cancelada" Then
           Call MsgBox("Proposta Cancelada, tipo de endosso n�o permitido.", vbInformation)
            Exit Sub
        End If
    End If

    MousePointer = vbHourglass
    bObteveSaldoRestituir = True    'cristovao.rodrigues 25/04/2013

    'C�lculo do IOF com base na data de pedido do endosso - Flow 276233
    'Diego Diniz - Flow 380882 - 16/05/2008 - Utilizar a Data do Sistema para a busca do IOF
    'Call ConsultarPercIOF(mskDtEndosso, CInt(txtSubramo.Text))
    Call ConsultarPercIOF(Data_Sistema, CInt(txtSubramo.Text))

    'RSilva - 04/11/2009
    If bModular = True Or txtProduto.Text = 1168 Then
        Call ObterValoresRestituicaoModular
        If Not bObteveSaldoRestituir Then Exit Sub    'cristovao.rodrigues 25/04/2013
    End If

    Call AplicarProposta

    MousePointer = vbDefault


End Sub

Private Sub cmdAplicarReativacaoFatura_Click()

    If grdFaturaCancelada.Row > 0 Then
        If ValidarDadosReativacaoFatura Then
            Call ReativarFatura
        End If
    End If

End Sub

Private Sub cmdAplicarReativacaoProposta_Click()

    MousePointer = vbHourglass

    Call AplicarReativacaoProposta

    MousePointer = vbDefault

End Sub

Private Sub cmdAplicarRestituicao_Click()

    Dim oPremioPago As Object
    Dim PremioPago As Currency

    If optCobAdi.Value Then

        Call AplicarCobranca

    ElseIf optCobRes.Value Or optCobSem.Value Then

        If cboTipoEndosso.ItemData(cboTipoEndosso.ListIndex) = TP_ENDOSSO_RESTITUICAO Or _
           cboTipoEndosso.ItemData(cboTipoEndosso.ListIndex) = TP_ENDOSSO_CANCELAMENTO_APOLICE_SEGURADORA Then
            Set oPremioPago = CreateObject("SEGL0140.cls00171")

            PremioPago = oPremioPago.ObterPremioBruto(cUserName, _
                                                      gsSIGLASISTEMA, _
                                                      App.Title, _
                                                      App.FileDescription, _
                                                      glAmbiente_id, _
                                                      CLng(txtPropostaid(0).Text))


            If lblValRestituicaoRes > PremioPago And (bModular = False And txtProduto.Text <> 1168) Then  'rsilva  22/07/09
                Call MsgBox("O Valor de restitui��o � maior que o premio pago!!!", vbOKOnly, "Critica")
                Exit Sub
            Else
                If lblValRestituicaoResCancelamento(0).Caption > PremioPago And (bModular = True Or txtProduto.Text = 1168) Then
                    Call MsgBox("O Valor de restitui��o � maior que o premio pago!!!", vbOKOnly, "Critica")
                    Exit Sub
                End If
            End If

            'rsilva
            If bModular = True Or txtProduto.Text = 1168 Then
                PremioPago = oPremioPago.ObterPremioBruto(cUserName, _
                                                          gsSIGLASISTEMA, _
                                                          App.Title, _
                                                          App.FileDescription, _
                                                          glAmbiente_id, _
                                                          oDadosModular.PropostaId)

                If lblValRestituicaoResCancelamento(1).Caption > PremioPago Then
                    Call MsgBox("O Valor de restitui��o � maior que o premio pago!!!", vbOKOnly, "Critica")
                    Exit Sub
                End If
            End If
        End If

        Call AplicarRestituicao

    End If

End Sub

Private Sub cmdAplicarEndereco_Click()

    Call AplicarEndereco

End Sub
Private Sub CalcularPagamentoCobranca()

    Dim cValorCobranca As Currency

    On Error GoTo TrataErro

    If CCur(mskValJuros.Text) < 0 Or CCur(mskValIOF.Text) < 0 Or _
       CCur(mskValDesc.Text) < 0 Or CCur(mskValPremioTarifa.Text) < 0 Or _
       CCur(mskCustoEndossoCob.Text) < 0 Or CCur(mskValCom.Text) < 0 Then
        MsgBox "Nenhum campo usado para o c�lculo pode conter valores negativos , c�lculo cancelado...!!", vbCritical
        Exit Sub
    End If

    If mskCambio.Visible And mskCambio.Text = "0,000" Then
        mensagem_erro 3, "cambio"
        mskCambio.SetFocus
        Exit Sub
    End If

    chkIsento_IOF.Enabled = False
    If Not ValidarGridPagamento Then
        Exit Sub
    End If

    If Val(mskNumParcelas.Text) > 0 Then

        Call MontarGridPagamento
        Call LimparVariaveisPgto
        cmdCalcPgto.Visible = False
        cmdCanPgto.Visible = True
        lblNumParc.Visible = False
        mskNumParcelas.Visible = False
        lblPerJuros.Visible = False
        cmbTaxaJuros.Visible = False
        cmdAplicarRestituicao.Enabled = True
    Else
        mensagem_erro 3, "N�mero de Parcelas"
        mskNumParcelas.SetFocus
        Exit Sub
    End If

    Exit Sub

TrataErro:
    Call TratarErro("CalcularPagamentoCobranca", Me.name)
    Call FinalizarAplicacao

End Sub

Public Function ValidarGridPagamento() As Boolean

    On Error GoTo TrataErro

    ValidarGridPagamento = False

    If CDbl(mskValPremioTarifa.Text) <= 0 Then
        mensagem_erro 3, "Valor Total"
        mskValPremioTarifa.SetFocus
        Exit Function
    End If


    If Not IsDate(mskDtPagamento.Text) Then
        mensagem_erro 3, "Data de Pagamento"
        mskDtPagamento.SetFocus
        Exit Function
    End If

    ValidarGridPagamento = True

    Exit Function

TrataErro:
    Call TratarErro("ValidarGridPagamento", Me.name)
    Call FinalizarAplicacao

End Function

Private Function ValidarLimiteCobertura() As Boolean

    Dim dAuxIs As Double
    Dim oSeguro As Object
    Dim oCobertura As Cobertura

    On Error GoTo Erro

    ValidarLimiteCobertura = False

    'se for cobertura b�sica, permite altera��o sem cr�ticas
    If oDadosEndosso.RamoId = 22 Or oDadosEndosso.RamoId = 21 Then

        Call IniciarConexao(gsSIGLASISTEMA, _
                            App.Title, _
                            App.FileDescription, _
                            glAmbiente_id, _
                            cUserName)

        Set oSeguro = CreateObject("SEGL0148.cls00181")

        Set oSeguro.oConexao = oConexao

        If oSeguro.VerificarCoberturaBasica(oDadosEndosso.PropostaId, cboCoberturaAfetada.ItemData(cboCoberturaAfetada.ListIndex)) Then
            ValidarLimiteCobertura = True
            Set oSeguro = Nothing
            Exit Function
        End If

        Call FecharConexao

        Set oSeguro = Nothing

    ElseIf lCodCoberturaBasica = cboCoberturaAfetada.ItemData(cboCoberturaAfetada.ListIndex) Then
        ValidarLimiteCobertura = True
        Exit Function
    End If

    dAuxIs = CDbl(mskImpSeg.Text)

    'Busca IS da cobertura original se for aumento ou redu��es
    For Each oCobertura In colCoberturas
        If oCobertura.TpCoberturaId = cboCoberturaAfetada.ItemData(cboCoberturaAfetada.ListIndex) _
           And oCobertura.CodObjSegurado = cboObjetoSegurado.Text Then

            'Aumento de IS
            If optAumIS.Value Then
                dAuxIs = Abs(dAuxIs + CDbl(oCobertura.ValIS))

                'Exclus�o de cobertura
            ElseIf optRedIS.Value And (CDbl(mskImpSeg.Text) = CDbl(oCobertura.ValIS)) Then
                ValidarLimiteCobertura = True
                Exit Function

                'Redu��o de IS
            Else
                dAuxIs = Abs(dAuxIs - CDbl(oCobertura.ValIS))
            End If

            Exit For

        End If
    Next

    Set oCobertura = Nothing

    'Acima do Limite
    If ((dAuxIs > TruncaDecimal((dPercLimMaxCobertura / 100) * dRiscoCoberturaBasica)) And (dPercLimMaxCobertura = 0)) Or ((dAuxIs > dLimMaxCobertura) And (dLimMaxCobertura <> 0)) Then
        Call MsgBox("IS acima do limite para esta cobertura.")
        Exit Function
    End If

    'Abaixo do Limite
    If (((dAuxIs < TruncaDecimal((dPercLimMinCobertura / 100) * dRiscoCoberturaBasica)) And (dPercLimMinCobertura <> 0)) Or ((dAuxIs < dLimMinCobertura) And (dLimMinCobertura <> 0))) Then
        Call MsgBox("IS abaixo do limite para esta cobertura.")
        Exit Function
    End If

    ValidarLimiteCobertura = True

    Exit Function

Erro:
    Call TratarErro("ValidarLimiteCobertura", Me.name)
    Call FinalizarAplicacao
End Function

Function TruncaDecimal(ByVal vValor As Variant) As String

    Dim iPos As Integer

    If bConfiguracaoBrasil Then
        iPos = InStr(vValor, ",")
    Else
        iPos = InStr(vValor, ".")
    End If

    If iPos <> 0 Then
        vValor = Mid(vValor, 1, iPos + 2)
    End If

    TruncaDecimal = vValor

End Function

Sub AdicionarGridCoberturas()

    Dim iIndice As Integer
    Dim sLinha As String
    Dim oCobertura As Cobertura
    Dim bAchouCob As Boolean

    On Error GoTo Erro

    bAchouCob = False

    'Checa preenchimento dos campos
    If Not optRedIS.Value And Not optAumIS.Value And Not optNovCob.Value Then
        Call mensagem_erro(3, "Tipo de Movimenta��o")
        Exit Sub
    End If

    If cboCoberturaAfetada.ListIndex = -1 Then
        Call mensagem_erro(3, "Cobertura Afetada")
        cboCoberturaAfetada.SetFocus
        Exit Sub
    End If

    If cboObjetoSegurado.ListIndex = -1 Then
        Call mensagem_erro(3, "Objeto Segurado")
        cboObjetoSegurado.SetFocus
        Exit Sub
    End If

    If Not (Val(txtProduto.Text) = 105 And oDadosEndosso.RamoId = 21) Then
        If CDbl(mskImpSeg.Text) <= 0 Then
            Call mensagem_erro(3, "Import�ncia Segurada")
            mskImpSeg.SetFocus
            Exit Sub
        End If
    End If

    If Not ValidarLimiteCobertura Then
        mskImpSeg.SetFocus
        Exit Sub
    End If

    If optAumIS.Value Or optNovCob.Value Then
        If Not (Val(txtProduto.Text) = 105 And oDadosEndosso.RamoId = 21) Then
            If CDbl(mskPremioTarifa.Text) <= 0 Then
                Call mensagem_erro(3, "Pr�mio")
                mskPremioTarifa.SetFocus
                Exit Sub
            End If
        End If
    End If

    For Each oCobertura In colCoberturas
        If oCobertura.TpCoberturaId = cboCoberturaAfetada.ItemData(cboCoberturaAfetada.ListIndex) _
           And oCobertura.CodObjSegurado = cboObjetoSegurado.Text Then
            bAchouCob = True

            If optRedIS.Value And (CDbl(oCobertura.ValIS) < CDbl(mskImpSeg.Text)) Then
                MsgBox "Valor para redu��o acima da IS original."
                Exit Sub
            End If

        End If
    Next

    If Not bAchouCob And optRedIS.Value Then
        Call MsgBox("Imposs�vel reduzir, cobertura n�o informada nesta proposta.")
        Exit Sub
    End If

    'Checa se a cobertura j� est� no grid
    If grdCoberturas.Rows > 1 Then

        For iIndice = 1 To grdCoberturas.Rows - 1
            If cboCoberturaAfetada.ItemData(cboCoberturaAfetada.ListIndex) = grdCoberturas.TextMatrix(iIndice, 0) Then
                Call mensagem_erro(6, "Cobertura j� selecionada!")
                cboCoberturaAfetada.SetFocus
                Exit Sub
            End If
        Next

    End If

    'Guarda nova cobertura no grid
    sLinha = cboCoberturaAfetada.ItemData(cboCoberturaAfetada.ListIndex) & vbTab
    sLinha = sLinha & cboCoberturaAfetada.Text & vbTab
    sLinha = sLinha & mskImpSeg.Text & vbTab
    sLinha = sLinha & mskTaxaTarifa.Text & vbTab
    sLinha = sLinha & mskPerDesc.Text & vbTab
    sLinha = sLinha & mskPremioTarifa.Text & vbTab & " "
    sLinha = sLinha & UCase(txtTextoFranquia.Text) & vbTab
    sLinha = sLinha & mskLimFra.Text & vbTab
    sLinha = sLinha & mskFatFranquia.Text & vbTab
    sLinha = sLinha & mskDtEndosso & vbTab
    sLinha = sLinha & "" & vbTab

    If optRedIS.Value Then
        sLinha = sLinha & "R"
    ElseIf optAumIS.Value Then
        sLinha = sLinha & "A"
    Else
        sLinha = sLinha & "N"
    End If

    grdCoberturas.AddItem sLinha

    grdCoberturas.RowData(grdCoberturas.Rows - 1) = cboCoberturaAfetada.ItemData(cboCoberturaAfetada.ListIndex)

    'Atualizando novo pr�mio
    cValPremioNovo = cValPremioNovo + CCur(mskPremioTarifa.Text)

    For Each oCobertura In colCoberturasEndossadas
        If oCobertura.CodObjSegurado = cboObjetoSegurado.Text _
           And oCobertura.TpCoberturaId = cboCoberturaAfetada.ItemData(cboCoberturaAfetada.ListIndex) Then
            Exit For
        End If
    Next

    If oCobertura Is Nothing Then
        Set oCobertura = New Cobertura
    End If

    oCobertura.CodObjSegurado = cboObjetoSegurado.Text
    oCobertura.DESCRICAO = cboCoberturaAfetada.Text
    oCobertura.DescFranquia = UCase(txtTextoFranquia.Text)
    oCobertura.TpCoberturaId = cboCoberturaAfetada.ItemData(cboCoberturaAfetada.ListIndex)
    oCobertura.ValIS = mskImpSeg.Text
    oCobertura.ValPremio = mskPremioTarifa.Text
    oCobertura.ValTaxa = mskTaxaTarifa.Text
    oCobertura.PercDesconto = mskPerDesc.Text
    oCobertura.ValFranquia = mskLimFra.Text
    oCobertura.FatFranquia = mskFatFranquia.Text
    oCobertura.Acumula_IS = sAcumulaIS
    oCobertura.Acumula_Resseguro = sAcumulaResseguro

    If optRedIS.Value Then
        oCobertura.TpCobranca = "R"
    ElseIf optAumIS.Value Then
        oCobertura.TpCobranca = "A"
    Else
        oCobertura.TpCobranca = "N"
    End If

    oCobertura.Operacao = "I"
    oCobertura.Persistente = False

    colCoberturasEndossadas.Add oCobertura

    Call LimparCamposEndossoIS
    Call DesabilitarCamposEndossoIS

    cmdAplicarIS.Enabled = True
    cmdBensAdicionar.Enabled = False

    Set oCobertura = Nothing

    Exit Sub

Erro:
    Call TratarErro("AdicionarGridCoberturas", Me.name)
    Call FinalizarAplicacao
End Sub

Private Sub cmdBensAdicionar_Click()

    Call AdicionarGridCoberturas

End Sub

Sub LimparCoberturas()

    Set colCoberturas = Nothing
    Set colCoberturasEndossadas = Nothing
    Set colNovaCobertura = Nothing

End Sub

Sub LimparCamposEndossoIS()

    cboCoberturaAfetada.ListIndex = -1
    mskImpSeg.mask = "9,2"
    mskImpSeg.Text = "0,00"
    mskTaxaTarifa.mask = "3,7"
    mskTaxaTarifa.Text = "0,0000000"
    mskTaxaNet.mask = "3,7"
    mskTaxaNet.Text = "0,0000000"
    mskPerDesc.mask = "3,5"
    mskPerDesc.Text = "0,00000"
    mskPremioTarifa.mask = "9,2"
    mskPremioTarifa.Text = "0,00"
    mskPremioNet.mask = "9,2"
    mskPremioNet.Text = "0,00"
    mskLimFra.mask = "9,2"
    mskLimFra.Text = "0,00"
    mskFatFranquia.mask = "1,5"
    mskFatFranquia.Text = "0,00000"
    txtTextoFranquia.Text = ""
    optAumIS.Value = False
    optRedIS.Value = False
    optNovCob.Value = False

End Sub

Private Sub cmdBensAlterar_Click()

    Call AlterarGridCoberturas

End Sub

Sub AlterarGridCoberturas()

    Dim oCoberturaEndossada As Cobertura
    Dim oCobertura As Cobertura
    Dim bAchouCob As Boolean

    On Error GoTo Erro

    bAchouCob = False

    If cboObjetoSegurado.ListIndex = -1 Then
        Call mensagem_erro(3, "Objeto Segurado")
        cboObjetoSegurado.SetFocus
        Exit Sub
    End If

    If Not optRedIS.Value And Not optAumIS.Value And Not optNovCob.Value Then
        Call mensagem_erro(3, "Tipo de Movimenta��o")
        Exit Sub
    End If

    If Not ValidarLimiteCobertura Then
        mskImpSeg.SetFocus
        Exit Sub
    End If

    For Each oCoberturaEndossada In colCoberturasEndossadas
        If oCoberturaEndossada.CodObjSegurado = cboObjetoSegurado.Text Then
            If oCoberturaEndossada.TpCoberturaId = grdCoberturas.TextMatrix(grdCoberturas.RowSel, 0) Then
                Exit For
            End If
        End If
    Next

    For Each oCobertura In colCoberturas
        If oCobertura.TpCoberturaId = cboCoberturaAfetada.ItemData(cboCoberturaAfetada.ListIndex) _
           And oCobertura.CodObjSegurado = cboObjetoSegurado.Text Then

            bAchouCob = True

            If optRedIS.Value And (CDbl(oCobertura.ValIS) < CDbl(mskImpSeg.Text)) Then
                MsgBox "Valor para redu��o acima da IS original."
                Exit Sub
            End If

        End If
    Next

    If Not bAchouCob And optRedIS.Value Then
        MsgBox "Imposs�vel reduzir, cobertura n�o informada nesta proposta."
        Exit Sub
    End If

    'Trata pr�mio tarifa alterado
    cValPremioNovo = cValPremioNovo - CCur(grdCoberturas.TextMatrix(grdCoberturas.RowSel, 5))
    cValPremioNovo = cValPremioNovo + CCur(mskPremioTarifa.Text)

    grdCoberturas.TextMatrix(grdCoberturas.Row, 2) = mskImpSeg.Text
    grdCoberturas.TextMatrix(grdCoberturas.Row, 3) = mskTaxaTarifa.Text
    grdCoberturas.TextMatrix(grdCoberturas.Row, 4) = mskPerDesc.Text
    grdCoberturas.TextMatrix(grdCoberturas.Row, 5) = mskPremioTarifa.Text
    grdCoberturas.TextMatrix(grdCoberturas.Row, 6) = txtTextoFranquia.Text
    grdCoberturas.TextMatrix(grdCoberturas.Row, 7) = mskLimFra.Text
    grdCoberturas.TextMatrix(grdCoberturas.Row, 8) = mskFatFranquia.Text

    If Not VerificaData2(mskDtEndosso.Text) Then
        grdCoberturas.TextMatrix(grdCoberturas.Row, 9) = ""
    Else
        grdCoberturas.TextMatrix(grdCoberturas.Row, 9) = mskDtEndosso.Text
    End If

    If optRedIS.Value Then
        grdCoberturas.TextMatrix(grdCoberturas.Row, 11) = "R"
    ElseIf optAumIS.Value Then
        grdCoberturas.TextMatrix(grdCoberturas.Row, 11) = "A"
    Else
        grdCoberturas.TextMatrix(grdCoberturas.Row, 11) = "N"
    End If

    'Altera na cole��o
    oCoberturaEndossada.CodObjSegurado = cboObjetoSegurado.Text
    oCoberturaEndossada.DESCRICAO = cboCoberturaAfetada.Text
    oCoberturaEndossada.DescFranquia = UCase(txtTextoFranquia.Text)
    oCoberturaEndossada.TpCoberturaId = cboCoberturaAfetada.ItemData(cboCoberturaAfetada.ListIndex)
    oCoberturaEndossada.ValIS = mskImpSeg.Text
    oCoberturaEndossada.ValPremio = mskPremioTarifa.Text
    oCoberturaEndossada.ValTaxa = mskTaxaTarifa.Text
    oCoberturaEndossada.PercDesconto = mskPerDesc.Text
    oCoberturaEndossada.ValFranquia = mskLimFra.Text
    oCoberturaEndossada.FatFranquia = mskFatFranquia.Text

    If optRedIS.Value Then
        oCoberturaEndossada.TpCobranca = "R"
    ElseIf optAumIS.Value Then
        oCoberturaEndossada.TpCobranca = "A"
    Else
        oCoberturaEndossada.TpCobranca = "N"
    End If

    If oCoberturaEndossada.Persistente Then
        oCoberturaEndossada.Operacao = "A"
    End If

    Call LimparCamposEndossoIS
    Call DesabilitarCamposEndossoIS

    cmdAplicarIS.Enabled = True
    cmdBensAlterar.Enabled = False
    cmdBensRemover.Enabled = False

    Set oCobertura = Nothing
    Set oCoberturaEndossada = Nothing

    Exit Sub

Erro:
    Call TratarErro("AlterarGridCoberturas", Me.name)
    Call FinalizarAplicacao
End Sub

Private Sub cmdBensRemover_Click()

    Call ExcluirGridCoberturas

End Sub

Sub ExcluirGridCoberturas()

    Dim iIndice As Integer
    Dim oCoberturaEndossada As Cobertura

    On Error GoTo Erro

    If cboObjetoSegurado.ListIndex = -1 Then
        Call mensagem_erro(3, "Objeto Segurado")
        cboObjetoSegurado.SetFocus
        Exit Sub
    End If

    If Not optRedIS.Value And Not optAumIS.Value And Not optNovCob.Value Then
        Call mensagem_erro(3, "Tipo de Movimenta��o")
        Exit Sub
    End If

    iIndice = 0

    For Each oCoberturaEndossada In colCoberturasEndossadas
        iIndice = iIndice + 1
        If oCoberturaEndossada.CodObjSegurado = cboObjetoSegurado.Text _
           And oCoberturaEndossada.TpCoberturaId = grdCoberturas.TextMatrix(grdCoberturas.RowSel, 0) Then
            Exit For
        End If
    Next

    'Trata pr�mio tarifa alterado
    cValPremioNovo = cValPremioNovo - CCur(grdCoberturas.TextMatrix(grdCoberturas.RowSel, 5))

    'Exclui das endossadas
    Call colCoberturasEndossadas.Remove(iIndice)

    If grdCoberturas.Rows = 2 Then
        grdCoberturas.Rows = 1
    Else
        Call grdCoberturas.RemoveItem(grdCoberturas.RowSel)
    End If

    Call LimparCamposEndossoIS
    Call DesabilitarCamposEndossoIS

    cmdAplicarIS.Enabled = True
    cmdBensRemover.Enabled = False
    cmdBensAlterar.Enabled = False

    Set oCoberturaEndossada = Nothing

    Exit Sub

Erro:
    Call TratarErro("ExcluirGridCoberturas", Me.name)
    Call FinalizarAplicacao
End Sub

'20-12-2015 - Schoralick (nova consultoria) - 18690344 - Melhorias Processo Reagendamento e Reativa��o BESC - inicio
Private Sub cmdBtnReativacaoProposta_Click(Index As Integer)
    Dim lIndex As Integer
    lIndex = Index

    Select Case lIndex
    Case 0    'sair
        stbEndosso.TabEnabled(0) = True
        stbEndosso.TabVisible(12) = False
        stbEndosso.Tab = 0

    Case 1    'aplicar
        MousePointer = vbHourglass
        If chkImpedirAgendamento = 0 Then
            Call AplicarReativacaoProposta
        Else
            If atualizarPropostaImpedimento Then
                MsgBox "Opera��o realizada com suscesso."

                Call sair
            End If
        End If
        MousePointer = vbDefault

    Case 2    'reagendamento automatico
        vlRetornoReativacao = 0
        frmReagendamentoEmMassa.Show vbModal
        If vlRetornoReativacao > 0 Then
            reagendamentoAutomatico vlRetornoReativacao, vlDataReativacao
        End If

    Case 3    'inclusao automatica
        vlRetornoReativacao = 0
        frmInclusaoAutomatica.Show vbModal
        If vlRetornoReativacao > 0 Then
            inclusaoAutomatica vlRetornoReativacao, vlDataReativacao
        End If


    End Select
End Sub
'20-12-2015 - Schoralick (nova consultoria) - 18690344 - Melhorias Processo Reagendamento e Reativa��o BESC - fim

Private Sub cmdCalcPgto_Click()

    Call CalcularPagamentoCobranca

End Sub

Sub InicializarFrameCobranca()

    mskCustoEndossoCob.Locked = False

    mskTotTarifa.Text = "0,00"
    mskTotJuros.Text = "0,00"
    mskTotIof.Text = "0,00"
    mskTotLiquido.Text = "0,00"
    mskTotCom.Text = "0,00"
    mskTotDesc.Text = "0,00"
    mskValComEst.Text = "0,00"
    chkIsento_IOF.Enabled = True

    lblDtVcto.Caption = "Data 1� Vcto:"

    lblParcAlt.Visible = False
    lblNumParcAlt.Visible = False

    cmdCalcPgto.Visible = True
    cmdCanPgto.Visible = False
    cmdPagAlterar.Enabled = False
    cmdAplicarRestituicao.Enabled = False

    lblPerJuros.Visible = True
    cmbTaxaJuros.Visible = True

    lblNumParc.Visible = True
    mskNumParcelas.Visible = True
    mskNumParcelas.Text = ""

End Sub

Private Sub cmdCanPgto_Click()

    Dim cValCustoEndosso As Currency

    On Error GoTo TrataErro

    mskCustoEndossoCob.Locked = False

    mskTotTarifa.Text = "0,00"
    mskTotJuros.Text = "0,00"
    mskTotIof.Text = "0,00"
    mskTotLiquido.Text = "0,00"
    mskTotCom.Text = "0,00"
    mskTotDesc.Text = "0,00"
    mskValComEst.Text = "0,00"
    chkIsento_IOF.Enabled = True

    lblDtVcto.Caption = "Data 1� Vcto:"

    lblParcAlt.Visible = False
    lblNumParcAlt.Visible = False

    ' Recalculando o custo pois usu�rio pode ter zerado '''''''''''''''''''''''''''''''''''
    If oDadosEndosso.CriticaCustoRamo Then
        cValCustoEndosso = CalcularCustoEndosso(oDadosEndosso.valPremioTarifa)
    End If

    mskCustoEndossoCob.Text = Format(cValCustoEndosso, "###0.00")
    mskValPremioTarifa.Text = Format(oDadosEndosso.valPremioTarifa, "###0.00")
    mskValDesc.Text = Format(oDadosEndosso.valDesconto, "###0.00")

    mskDtPagamento.Text = Format(DateAdd("m", 1, Data_Sistema), "dd/mm/yyyy")

    mskValPremioTarifa.SetFocus

    mskNumParcelas.Text = grdPagamento.Rows - 1

    grdPagamento.Rows = 1

    cmdCalcPgto.Visible = True
    cmdCanPgto.Visible = False
    cmdPagAlterar.Enabled = False
    cmdAplicarRestituicao.Enabled = False

    lblPerJuros.Visible = True
    cmbTaxaJuros.Visible = True

    lblNumParc.Visible = True
    mskNumParcelas.Visible = True
    mskNumParcelas.SetFocus

    Exit Sub

TrataErro:
    Call TratarErro("cmdCanPgto_Click", Me.name)
    Call FinalizarAplicacao

End Sub


Private Sub cmdConCliente_Click()

    FrmConClientes.Show vbModal

End Sub

Private Sub CmdConCorretor_Click()

    FrmConCorretores.Show vbModal

End Sub

Private Sub cmdCorretorAdicionar_Click()

    MousePointer = vbHourglass

    Call AdicionarCorretagem

    MousePointer = vbDefault

End Sub

Private Sub AdicionarCorretagem()

    Dim i As Integer
    Dim oCorretor As Object
    Dim oSubGrupo As Object

    'Checa preenchimento dos campos
    If Trim(TxtCodCorretor(0).Text) = "" Then
        mensagem_erro 3, "C�d. Corretor"
        TxtCodCorretor(0).SetFocus
        Exit Sub
    End If

    If Trim(TxtCodCorretor(1).Text) = "" Then
        mensagem_erro 3, "Nome Corretor"
        TxtCodCorretor(1).SetFocus
        Exit Sub
    End If

    If TxtTpCorretor.Text = "1" Then
        If Trim(TxtSucCorretor.Text) = "" Then
            mensagem_erro 3, "Sucursal Corretor"
            Exit Sub
        End If
    End If

    If CDbl(mskPercCorretagem.Text) <= 0 Then
        mensagem_erro 3, "Percentual de Corretagem"
        mskPercCorretagem.SetFocus
        Exit Sub
    ElseIf CDbl(mskPercCorretagem.Text) >= 100 Then
        mensagem_erro 3, "Percentual de Corretagem"
        mskPercCorretagem.SetFocus
        Exit Sub
    End If

    Set oCorretor = CreateObject("SEGL0140.cls00149")

    If CboSubGrupo.Enabled Then

        Set oSubGrupo = colSubGrupo.Item(CboSubGrupo.ItemData(CboSubGrupo.ListIndex))

        For Each oCorretor In oSubGrupo.Corretores
            If Trim(TxtCodCorretor(0).Text) = oCorretor.Corretor_ID Then
                mensagem_erro 6, "Corretor j� selecionado."
                Exit Sub
            End If
        Next

        Set oCorretor = Nothing

        Set oCorretor = CreateObject("SEGL0140.cls00149")

        With oCorretor
            .Corretor_ID = TxtCodCorretor(0).Text
            .Nome = TxtCodCorretor(1).Text
            .PercComissao = mskPercCorretagem.Text
            .Tipo = TxtTpCorretor.Text
            .Sucursal = TxtSucCorretor.Text
            .Persistente = False
            .Operacao = "I"
        End With

        oSubGrupo.Corretores.Add oCorretor, CStr(oCorretor.Corretor_ID)

        Call MontarGridCorretagem(oSubGrupo.Corretores)

        Set oCorretor = Nothing
        Set oSubGrupo = Nothing

    Else

        For Each oCorretor In colCorretor
            If Trim(TxtCodCorretor(0).Text) = Trim(oCorretor.Corretor_ID) Then
                mensagem_erro 6, "Corretor j� selecionado."
                Exit Sub
            End If
        Next

        Set oCorretor = Nothing

        Set oCorretor = CreateObject("SEGL0140.cls00149")

        With oCorretor
            .Corretor_ID = TxtCodCorretor(0).Text
            .Nome = TxtCodCorretor(1).Text
            .PercComissao = mskPercCorretagem.Text
            .Tipo = TxtTpCorretor.Text
            .Sucursal = TxtSucCorretor.Text
            .Persistente = False
            .Operacao = "I"
        End With

        colCorretor.Add oCorretor, CStr(oCorretor.Corretor_ID)

        Call MontarGridCorretagem(colCorretor)

        Set oCorretor = Nothing

    End If

    TxtCodCorretor(0).Text = ""
    TxtCodCorretor(1).Text = ""
    TxtTpCorretor.Text = ""

    mskPercCorretagem.Text = Format("0,00", "0.00000")
    cmdCorretorAdicionar.Enabled = False

    CmdOkCor.Enabled = True

End Sub

Private Sub AdicionarBeneficiario()

    Dim oBeneficiario As Object
    Dim iMaiorBeneficiario As Integer

    On Error GoTo Erro

    If Trim(txtNomeBenef(0).Text) = "" Then
        mensagem_erro 3, "Nome Benefici�rio"
        txtNomeBenef(0).SetFocus
        Exit Sub
    End If

    Set oBeneficiario = CreateObject("SEGL0140.cls00999")

    iMaiorBeneficiario = 0
    For Each oBeneficiario In colBeneficiario
        If oBeneficiario.lBeneficiarioId > iMaiorBeneficiario Then
            iMaiorBeneficiario = oBeneficiario.lBeneficiarioId
        End If
    Next

    Set oBeneficiario = Nothing

    Set oBeneficiario = CreateObject("SEGL0140.cls00999")

    With oBeneficiario

        .lBeneficiarioId = iMaiorBeneficiario + 1
        .sNome = Trim(txtNomeBenef(0).Text)
        .sEndereco = Trim(txtNomeBenef(1).Text)
        .sBairro = Trim(txtBairroBenef(0).Text)

        If cboCidadeBenef.ListIndex = -1 Then
            .sCidade = ""
        Else
            .sCidade = Left(cboCidadeBenef.List(cboCidadeBenef.ListIndex), Len(cboCidadeBenef.List(cboCidadeBenef.ListIndex)) - 5)
        End If

        .sEstado = Trim(txtBairroBenef(2).Text)
        .sPais = Trim(txtBairroBenef(1).Text)

        If Trim(txtCepBenef.ClipText) = "" Then
            .sCep = ""
        Else
            .sCep = Trim(txtCepBenef.Text)
        End If

        .sOperacao = "I"

    End With

    colBeneficiario.Add oBeneficiario, CStr(oBeneficiario.lBeneficiarioId)

    Call PreencherGridBeneficiario(colBeneficiario)

    Set oBeneficiario = Nothing

    Call LimparCamposBenficiario

    'cmdAdicionarBenef.Enabled = False

    cmdAplicarBenef.Enabled = True

    Exit Sub

Erro:
    Call TratarErro("AdicionarBeneficiario", Me.name)
    Call FinalizarAplicacao
End Sub

Sub LimparCamposBenficiario()

    txtNomeBenef(0).Text = ""
    txtNomeBenef(1).Text = ""
    txtBairroBenef(0).Text = ""
    txtBairroBenef(2).Text = ""
    cboCidadeBenef.ListIndex = -1
    txtCepBenef.Text = "__.___-___"
    txtBairroBenef(1).Text = ""

End Sub

Private Sub cmdCorretorAlterar_Click()

    MousePointer = vbHourglass

    Call AlterarCorretagem

    MousePointer = vbDefault

End Sub

Sub AlterarCorretagem()

    Dim oCorretor As Object
    Dim oSubGrupo As Object

    'Checa preenchimento dos campos
    If Trim(TxtCodCorretor(0).Text) = "" Then
        mensagem_erro 3, "C�d. Corretor"
        TxtCodCorretor(0).SetFocus
        Exit Sub
    End If

    If Trim(TxtCodCorretor(1).Text) = "" Then
        mensagem_erro 3, "C�d. Corretor"
        TxtCodCorretor(1).SetFocus
        Exit Sub
    End If

    If TxtTpCorretor.Text = "1" Then
        If Trim(TxtSucCorretor.Text) = "" Then
            mensagem_erro 3, "Sucursal Corretor"
            Exit Sub
        End If
    End If

    If CDbl(mskPercCorretagem.Text) < 0 Then
        mensagem_erro 3, "Percentual de Corretagem"
        mskPercCorretagem.SetFocus
        Exit Sub
    ElseIf CDbl(mskPercCorretagem.Text) >= 100 Then
        mensagem_erro 3, "Percentual de Corretagem"
        mskPercCorretagem.SetFocus
        Exit Sub
    End If

    If CboSubGrupo.Enabled Then

        Set oSubGrupo = colSubGrupo.Item(CboSubGrupo.ItemData(CboSubGrupo.ListIndex))
        Set oCorretor = oSubGrupo.Corretores.Item(Replace(GridCorretagem.TextMatrix(iAuxLinha, 0), "-", ""))
        oCorretor.Operacao = "A"
        oCorretor.PercComissao = CDbl(mskPercCorretagem.Text)
        Call MontarGridCorretagem(oSubGrupo.Corretores)
        Set oCorretor = Nothing
        Set oSubGrupo = Nothing

    Else

        Set oCorretor = colCorretor.Item(Replace(GridCorretagem.TextMatrix(iAuxLinha, 0), "-", ""))
        oCorretor.Operacao = "A"
        oCorretor.PercComissao = CDbl(mskPercCorretagem.Text)
        Call MontarGridCorretagem(colCorretor)
        Set oCorretor = Nothing

    End If

    TxtCodCorretor(0).Text = ""
    TxtCodCorretor(1).Text = ""
    TxtTpCorretor.Text = ""
    mskPercCorretagem.Text = Format("0,00", "0.00000")

    cmdCorretorAlterar.Enabled = False
    cmdCorretorRemover.Enabled = False
    cmdCorretorAdicionar.Enabled = False

    CmdOkCor.Enabled = True

End Sub

Private Sub cmdCorretorCobAdicionar_Click()

    Dim iCont As Integer
    Dim sLinha As String

    'Checa preenchimento dos campos
    If cboCorretorCob.ListIndex = -1 Then
        mensagem_erro 3, "Corretor"
        cboCorretorCob.SetFocus
        Exit Sub
    End If

    If cboSucursalCorretorCob.ListCount > 0 Then
        If cboSucursalCorretorCob.ListIndex = -1 Then
            mensagem_erro 3, "Sucursal do Corretor"
            cboSucursalCorretorCob.SetFocus
            Exit Sub
        End If
    End If

    If CDbl(mskPercCorretagemCob.Text) <= 0 Then
        mensagem_erro 3, "Percentual de Corretagem"
        mskPercCorretagemCob.SetFocus
        Exit Sub
    ElseIf CDbl(mskPercCorretagemCob.Text) > 100 Then
        mensagem_erro 3, "Percentual de Corretagem"
        mskPercCorretagemCob.SetFocus
        Exit Sub
    End If

    'Checa se o corretor j� est� no grid
    If grdCorretagem.Rows > 1 Then
        For iCont = 1 To grdCorretagem.Rows - 1
            If Format(cboCorretorCob.ItemData(cboCorretorCob.ListIndex), "000000-000") = grdCorretagem.TextMatrix(iCont, 0) Then
                mensagem_erro 6, "Corretor j� selecionado.!"
                cboCorretorCob.SetFocus
                Exit Sub
            End If
        Next
    End If

    sLinha = Mid(cboCorretorCob.Text, 1, 10) & vbTab
    sLinha = sLinha & UCase(Mid(cboCorretorCob.Text, 14)) & vbTab
    sLinha = sLinha & mskPercCorretagemCob.Text & vbTab
    sLinha = sLinha & cboSucursalCorretorCob.Text
    sLinha = sLinha & cboTpCorretor.List(cboTpCorretor.ListIndex) & vbTab

    grdCorretagem.AddItem (sLinha)

    cboCorretorCob.ListIndex = -1
    cboSucursalCorretorCob.ListIndex = -1
    mskPercCorretagemCob.Text = "0,00"

    cmdCorretorCobAdicionar.Enabled = False
    cmdCorretorCobAlterar.Enabled = False
    cmdCorretorCobRemover.Enabled = False

    If CCur(mskValPremioTarifaRes.Text) > 0 Then
        Call CalcularValoresRestituicao
    ElseIf CCur(mskValPremioTarifa.Text) > 0 Then
        Call CalcularValoresCobranca
    End If

End Sub

Private Sub cmdCorretorCobAlterar_Click()

'Checa preenchimento dos campos
    If cboCorretorCob.ListIndex = -1 Then
        mensagem_erro 3, "Corretor"
        cboCorretorCob.SetFocus
        Exit Sub
    End If

    If cboSucursalCorretorCob.ListCount > 0 Then
        If cboSucursalCorretorCob.ListIndex = -1 Then
            mensagem_erro 3, "Sucursal do Corretor"
            cboSucursalCorretorCob.SetFocus
            Exit Sub
        End If
    End If

    If CDbl(mskPercCorretagemCob.Text) <= 0 Then
        mensagem_erro 3, "Percentual de Participa��o"
        mskPercCorretagemCob.SetFocus
        Exit Sub
    ElseIf CDbl(mskPercCorretagemCob.Text) > 100 Then
        mensagem_erro 3, "Percentual de Participa��o"
        mskPercCorretagemCob.SetFocus
        Exit Sub
    End If

    grdCorretagem.Row = iAuxLinha
    grdCorretagem.TextMatrix(grdCorretagem.Row, 2) = mskPercCorretagemCob.Text

    mskPercCorretagemCob.Text = "0,00"

    cmdCorretorCobAdicionar.Enabled = False
    cmdCorretorCobAlterar.Enabled = False
    cmdCorretorCobRemover.Enabled = False

    If CCur(mskValPremioTarifaRes.Text) > 0 Then
        Call CalcularValoresRestituicao
    ElseIf CCur(mskValPremioTarifa.Text) > 0 Then
        Call CalcularValoresCobranca
    End If

End Sub

Private Sub cmdCorretorCobRemover_Click()

    If grdCorretagem.Rows = 2 Then
        grdCorretagem.Rows = 1
    Else
        grdCorretagem.RemoveItem (grdCorretagem.Row)
    End If

    'Limpa campos
    mskPercCorretagemCob.Text = "0,00"

    cmdCorretorCobAdicionar.Enabled = False
    cmdCorretorCobAlterar.Enabled = False
    cmdCorretorCobRemover.Enabled = False

    If CCur(mskValPremioTarifaRes.Text) > 0 Then
        Call CalcularValoresRestituicao
    ElseIf CCur(mskValPremioTarifa.Text) > 0 Then
        Call CalcularValoresCobranca
    End If

End Sub

Private Sub cmdCorretorRemover_Click()

    MousePointer = vbHourglass

    Call RemoverCorretagem

    MousePointer = vbDefault

End Sub

Sub RemoverBeneficiario()

    Dim oBeneficiario As Object

    On Error GoTo Erro

    Set oBeneficiario = colBeneficiario.Item(grdBeneficiario.TextMatrix(grdBeneficiario.Row, 0))

    oBeneficiario.sOperacao = "E"

    Call PreencherGridBeneficiario(colBeneficiario)

    Set oBeneficiario = Nothing

    'Limpa campos
    Call LimparCamposBenficiario

    cmdAplicarBenef.Enabled = True

    cmdRemoverBenef.Enabled = False
    cmdAlterarBenef.Enabled = False

    Exit Sub

Erro:
    Call TratarErro("RemoverBeneficiario", Me.name)
    Call FinalizarAplicacao
End Sub

Sub RemoverCorretagem()

    Dim oCorretor As Object
    Dim oSubGrupo As Object

    If CboSubGrupo.Enabled Then

        Set oSubGrupo = colSubGrupo.Item(CboSubGrupo.ItemData(CboSubGrupo.ListIndex))
        Set oCorretor = oSubGrupo.Corretores.Item(Replace(GridCorretagem.TextMatrix(GridCorretagem.Row, 0), "-", ""))
        oCorretor.Operacao = "E"
        Call MontarGridCorretagem(oSubGrupo.Corretores)
        Set oCorretor = Nothing
        Set oSubGrupo = Nothing

    Else

        Set oCorretor = colCorretor.Item(Replace(GridCorretagem.TextMatrix(GridCorretagem.Row, 0), "-", ""))
        oCorretor.Operacao = "E"
        Call MontarGridCorretagem(colCorretor)
        Set oCorretor = Nothing

    End If

    'Limpa campos
    TxtCodCorretor(0).Text = ""
    TxtCodCorretor(1).Text = ""
    TxtTpCorretor.Text = ""
    mskPercCorretagem.Text = Format("0,00", "0.00000")
    CmdOkCor.Enabled = True

    cmdCorretorRemover.Enabled = False
    cmdCorretorAlterar.Enabled = False

End Sub

Private Sub cmdEstAdicionar_Click()
    Call IncluirEndEstipulante
End Sub

Private Sub IncluirEndEstipulante()

    Dim oSubGrupo As Object
    Dim oEstipulante As Object

    On Error GoTo TrataErro

    If Trim(txtEstipulante.Text) = "" Then
        mensagem_erro 3, "C�d. Estipulante"
        txtEstipulante.SetFocus
        Exit Sub
    End If

    If Trim(txtNomeEstipulante(0).Text) = "" Then
        mensagem_erro 3, "Nome do Estipulante"
        txtNomeEstipulante(0).SetFocus
        Exit Sub
    End If

    If CDbl(mskPercEstipulante.Text) <= 0 Then
        mensagem_erro 3, "Percentual de Participa��o"
        mskPercEstipulante.SetFocus
        Exit Sub
    ElseIf CDbl(mskPercEstipulante.Text) > 100 Then
        mensagem_erro 3, "Percentual de Participa��o"
        mskPercEstipulante.SetFocus
        Exit Sub
    End If

    If cboSubGrupoEst.Enabled Then

        Set oSubGrupo = colSubGrupo.Item(cboSubGrupoEst.ItemData(cboSubGrupoEst.ListIndex))

        For Each oEstipulante In oSubGrupo.estipulantes
            If Trim(txtEstipulante.Text) = oEstipulante.ID Then
                mensagem_erro 6, "Estipulante j� selecionado."
                Exit Sub
            End If
        Next

        Set oEstipulante = Nothing

        Set oEstipulante = CreateObject("SEGL0140.cls00174")

        oEstipulante.ID = Trim(txtEstipulante.Text)
        oEstipulante.Nome = Trim(UCase(txtNomeEstipulante(0).Text))
        oEstipulante.DtIniAdm = Trim(mskDtEmissao.Text)
        oEstipulante.Operacao = "I"
        oEstipulante.PercProLabore = CDbl(mskPercEstipulante.Text)

        oSubGrupo.estipulantes.Add oEstipulante, CStr(oEstipulante.ID)

        Call PreencherGridEstipulante(oSubGrupo.estipulantes)
        Set oEstipulante = Nothing
        Set oSubGrupo = Nothing

    Else

        For Each oEstipulante In colEstipulante
            If Trim(txtEstipulante.Text) = oEstipulante.ID Then
                mensagem_erro 6, "Estipulante j� selecionado."
                Exit Sub
            End If
        Next

        Set oEstipulante = Nothing

        Set oEstipulante = CreateObject("SEGL0140.cls00174")

        oEstipulante.ID = Trim(txtEstipulante.Text)
        oEstipulante.Nome = Trim(UCase(txtNomeEstipulante(0).Text))
        oEstipulante.DtIniAdm = Trim(mskDtEmissao.Text)
        oEstipulante.Operacao = "I"
        oEstipulante.PercProLabore = CDbl(mskPercEstipulante.Text)

        colEstipulante.Add oEstipulante, CStr(oEstipulante.ID)

        Call PreencherGridEstipulante(colEstipulante)
        Set oEstipulante = Nothing

    End If


    cmdEstRemover.Enabled = False
    cmdEstAdicionar.Enabled = False
    cmdEstAlterar.Enabled = False

    txtEstipulante.Text = ""
    txtNomeEstipulante(0).Text = ""
    mskPercEstipulante.mask = "3,4"
    mskPercEstipulante.Text = "0,0000"

    Exit Sub

TrataErro:
    Call TratarErro("AlterarEndEstipulante", Me.name)
    Call FinalizarAplicacao

End Sub

Private Sub cmdEstAlterar_Click()
    Call AlterarEndEstipulante
End Sub

Private Sub AlterarEndEstipulante()

    Dim oSubGrupo As Object
    Dim oEstipulante As Object

    On Error GoTo TrataErro

    If Trim(txtEstipulante.Text) = "" Then
        mensagem_erro 3, "C�d. Estipulante"
        txtEstipulante.SetFocus
        Exit Sub
    End If

    If Trim(txtNomeEstipulante(0).Text) = "" Then
        mensagem_erro 3, "Nome do Estipulante"
        txtNomeEstipulante(0).SetFocus
        Exit Sub
    End If

    If CDbl(mskPercEstipulante.Text) <= 0 Then
        mensagem_erro 3, "Percentual de Participa��o"
        mskPercEstipulante.SetFocus
        Exit Sub
    ElseIf CDbl(mskPercEstipulante.Text) > 100 Then
        mensagem_erro 3, "Percentual de Participa��o"
        mskPercEstipulante.SetFocus
        Exit Sub
    End If

    If cboSubGrupoEst.Enabled Then

        Set oSubGrupo = colSubGrupo.Item(cboSubGrupoEst.ItemData(cboSubGrupoEst.ListIndex))
        Set oEstipulante = oSubGrupo.estipulantes.Item(grdEstipulanteEst.TextMatrix(iAuxLinha, 0))
        oEstipulante.Operacao = "A"
        oEstipulante.PercProLabore = CDbl(mskPercEstipulante.Text)
        Call PreencherGridEstipulante(oSubGrupo.estipulantes)
        Set oEstipulante = Nothing
        Set oSubGrupo = Nothing

    Else

        Set oEstipulante = colEstipulante.Item(grdEstipulanteEst.TextMatrix(iAuxLinha, 0))
        oEstipulante.Operacao = "A"
        oEstipulante.PercProLabore = CDbl(mskPercEstipulante.Text)
        Call PreencherGridEstipulante(colEstipulante)
        Set oEstipulante = Nothing

    End If

    cmdEstRemover.Enabled = False
    cmdEstAdicionar.Enabled = False
    cmdEstAlterar.Enabled = False

    txtEstipulante.Text = ""
    txtNomeEstipulante(0).Text = ""
    mskPercEstipulante.mask = "3,4"
    mskPercEstipulante.Text = "0,0000"

    Exit Sub

TrataErro:
    Call TratarErro("AlterarEndEstipulante", Me.name)
    Call FinalizarAplicacao

End Sub

Private Sub cmdEstipulanteAdicionar_Click()

    Dim iCont As Integer
    Dim sLinha As String

    If CboEstipulante.ListIndex = -1 Then
        mensagem_erro 3, "Estipulante"
        CboEstipulante.SetFocus
        Exit Sub
    End If

    If CDbl(mskPercProLabore.Text) <= 0 Then
        mensagem_erro 3, "Percentual de ProLabore"
        mskPercProLabore.SetFocus
        Exit Sub
    ElseIf CDbl(mskPercProLabore.Text) > 100 Then
        mensagem_erro 3, "Percentual de ProLabore"
        mskPercProLabore.SetFocus
        Exit Sub
    End If

    If grdEstipulante.Rows > 1 Then
        For iCont = 1 To grdEstipulante.Rows - 1
            If CboEstipulante.ItemData(CboEstipulante.ListIndex) = grdEstipulante.TextMatrix(iCont, 0) Then
                mensagem_erro 6, "Estipulante j� selecionado."
                CboEstipulante.SetFocus
                Exit Sub
            End If
        Next
    End If

    sLinha = CboEstipulante.ItemData(CboEstipulante.ListIndex) & vbTab
    sLinha = sLinha & CboEstipulante.Text & vbTab
    sLinha = sLinha & mskPercProLabore.Text

    grdEstipulante.AddItem (sLinha)

    CboEstipulante.ListIndex = -1
    mskPercProLabore.Text = "0,00"

    cmdEstipulanteAdicionar.Enabled = False
    cmdEstipulanteAlterar.Enabled = False
    cmdEstipulanteRemover.Enabled = False

    If CCur(mskValPremioTarifaRes.Text) > 0 Then
        Call CalcularValoresRestituicao
    ElseIf CCur(mskValPremioTarifa.Text) > 0 Then
        Call CalcularValoresCobranca
    End If

End Sub

Private Sub cmdEstipulanteAlterar_Click()

    If CboEstipulante.ListIndex = -1 Then
        mensagem_erro 3, "Corretor"
        CboEstipulante.SetFocus
        Exit Sub
    End If

    If CDbl(mskPercProLabore.Text) <= 0 Then
        mensagem_erro 3, "Percentual de Participa��o"
        mskPercProLabore.SetFocus
        Exit Sub
    ElseIf CDbl(mskPercProLabore.Text) > 100 Then
        mensagem_erro 3, "Percentual de Participa��o"
        mskPercProLabore.SetFocus
        Exit Sub
    End If

    grdEstipulante.Row = iAuxLinha
    grdEstipulante.TextMatrix(grdEstipulante.Row, 2) = mskPercProLabore.Text

    mskPercProLabore.Text = "0,00"

    cmdEstipulanteAdicionar.Enabled = False
    cmdEstipulanteAlterar.Enabled = False
    cmdEstipulanteRemover.Enabled = False

    If CCur(mskValPremioTarifaRes.Text) > 0 Then
        Call CalcularValoresRestituicao
    ElseIf CCur(mskValPremioTarifa.Text) > 0 Then
        Call CalcularValoresCobranca
    End If

End Sub

Private Sub cmdEstipulanteRemover_Click()

    If grdEstipulante.Rows = 2 Then
        grdEstipulante.Rows = 1
    Else
        grdEstipulante.RemoveItem (grdEstipulante.Row)
    End If

    mskPercProLabore.Text = "0,00"

    cmdEstipulanteAdicionar.Enabled = False
    cmdEstipulanteAlterar.Enabled = False
    cmdEstipulanteRemover.Enabled = False

    If CCur(mskValPremioTarifaRes.Text) > 0 Then
        Call CalcularValoresRestituicao
    ElseIf CCur(mskValPremioTarifa.Text) > 0 Then
        Call CalcularValoresCobranca
    End If

End Sub

Private Sub cmdEstRemover_Click()
    Call RemoverEndEstipulante
End Sub

Private Sub RemoverEndEstipulante()

    Dim oSubGrupo As Object
    Dim oEstipulante As Object

    On Error GoTo TrataErro

    If cboSubGrupoEst.Enabled Then

        Set oSubGrupo = colSubGrupo.Item(cboSubGrupoEst.ItemData(cboSubGrupoEst.ListIndex))
        Set oEstipulante = oSubGrupo.estipulantes.Item(grdEstipulanteEst.TextMatrix(grdEstipulanteEst.Row, 0))
        oEstipulante.Operacao = "E"
        Call PreencherGridEstipulante(oSubGrupo.estipulantes)
        Set oEstipulante = Nothing
        Set oSubGrupo = Nothing

    Else

        Set oEstipulante = colEstipulante.Item(grdEstipulanteEst.TextMatrix(grdEstipulanteEst.Row, 0))
        oEstipulante.Operacao = "E"
        Call PreencherGridEstipulante(colEstipulante)
        Set oEstipulante = Nothing

    End If

    'Limpa campos
    mskPercCorretagemCob.Text = "0,00"

    cmdEstRemover.Enabled = False
    cmdEstAdicionar.Enabled = False
    cmdEstAlterar.Enabled = False

    txtEstipulante.Text = ""
    txtNomeEstipulante(0).Text = ""
    mskPercEstipulante.mask = "3,4"
    mskPercEstipulante.Text = "0,0000"

    Exit Sub

TrataErro:

    Call TratarErro("RemoverEndEstipulante", Me.name)
    Call FinalizarAplicacao

End Sub

Private Sub cmdExcluirParcelaReativacao_Click()

    Dim oEndossoReativacao As Object
    Dim iIndice As Integer

    On Error GoTo TrataErro
    '20-12-2015 - Schoralick (nova consultoria) - 18690344 - Melhorias Processo Reagendamento e Reativa��o BESC - inicio
    Select Case vlProdutoId
    Case 200, 201, 202, 203, 204, 205, 208, 209, 210, 211, 213, 216, 217, 218, 219, 221, 222, 223, 224, 225, 226
        If grdParcelasReativacao.TextMatrix(grdParcelasReativacao.Row, 3) = "c" Then
            MsgBox "N�o � permitido excluir uma parcela pendente de um produtos BESC no processo de reativa��o de proposta.", vbCritical, App.Title
            Exit Sub
        End If
    End Select
    '20-12-2015 - Schoralick (nova consultoria) - 18690344 - Melhorias Processo Reagendamento e Reativa��o BESC - fim
    If grdParcelasReativacao.Rows = 1 Then
        Exit Sub
    End If

    If grdParcelasReativacao.Rows = 2 Then

        grdParcelasReativacao.Rows = 1

        Set oEndossoReativacao = CreateObject("SEGL0140.cls00172")

        lUltimaParcelaReativacao = oEndossoReativacao.ObterUltimaParcela(gsSIGLASISTEMA, _
                                                                         glAmbiente_id, _
                                                                         App.Title, _
                                                                         App.FileDescription, _
                                                                         oDadosEndosso.PropostaId)

        Set oEndossoReativacao = Nothing

    Else

        For iIndice = grdParcelasReativacao.Row To grdParcelasReativacao.Rows - 1
            grdParcelasReativacao.TextMatrix(iIndice, 0) = Val(grdParcelasReativacao.TextMatrix(iIndice, 0)) - 1
        Next

        Call grdParcelasReativacao.RemoveItem(grdParcelasReativacao.Row)

        lUltimaParcelaReativacao = lUltimaParcelaReativacao - 1

    End If

    Exit Sub

TrataErro:
    Call TratarErro("cmdExcluirParcelaReativacao_Click", Me.name)
    Call FinalizarAplicacao
End Sub

Private Sub CmdIncluirParcelaReativacao_Click()

    Dim sLinha As String
    Dim iCont As Integer
    Dim dt_valida As Date
    '20-12-2015 - Schoralick (nova consultoria) - 18690344 - Melhorias Processo Reagendamento e Reativa��o BESC - inicio
    Dim lsMsg As String
    Dim sData1 As String
    Dim sData2 As String
    '20-12-2015 - Schoralick (nova consultoria) - 18690344 - Melhorias Processo Reagendamento e Reativa��o BESC - fim

    'Checa preenchimento dos campos

    If Not IsNumeric(mskValParcelaReativacao.Text) Then
        MsgBox "Valor da parcela inv�lido", vbCritical, App.Title
        mskValParcelaReativacao.SetFocus
        Exit Sub
    End If

    If Not Val(mskValParcelaReativacao.Text) > 0 And oDadosEndosso.produtoId <> 1152 And oDadosEndosso.produtoId <> 1204 Then
        MsgBox "Valor da parcela inv�lido", vbCritical, App.Title
        mskValParcelaReativacao.SetFocus
        Exit Sub
    End If

    If Not VerificaData2(mskDtParcelaReativacao.Text) Then
        MsgBox "Data da parcela inv�lida", vbCritical, App.Title
        mskDtParcelaReativacao.SetFocus
        Exit Sub
    End If

    'pamela.maia 28/04/2014 FLOW_17872929
    'Corre��o desta demanda incluindo os produtos massificados. - Alexandre Debouch - Confitec - 14/10/2016
    'Esta verifica��o deixar� de ser efetuada para outros produtos.

    Select Case vlProdutoId
    Case 104, 106, 108, 109, 116, 119, 400, 809, 1167, 1176, 1178, 1188, 1220, 1221, 1222, 1223, 1224, 112, _
         810, 100, 111, 113, 120, 800, 811, 1123, 1228, 1229, 719, 777, 1016, 1017, 1021, 1125, 1141, 1146, _
         1147, 1148, 1149, 1150, 1151, 1207
        If CDate(mskDtParcelaReativacao.Text) < CDate(txtIniVigencia.Text) Or CDate(mskDtParcelaReativacao.Text) > CDate(txtFimVigencia.Text) Then
            MsgBox "Data da parcela n�o pode estar fora da vig�ncia da proposta.", vbExclamation, App.Title
            mskDtParcelaReativacao.SetFocus
            Exit Sub
        End If
    End Select

    If CDate(mskDtParcelaReativacao) < CDate(Data_Sistema) Then
        MsgBox ("O agendamento est� inferior � data do sistema")
        Exit Sub
    End If

    ' 20/12/2015 - schoralick (nova consultoria) - - inicio
    'dt_valida = DiasUteis(Data_Sistema, 7)
    'If DateDiff("d", dt_valida, mskDtParcelaReativacao.Text) < 0 Then
    '    If MsgBox("O ideal � que a data do primeiro agendamento deva ser para no m�nimo 7 dias �teis da data atual! Deseja confirmar?", vbInformation + vbYesNo) = vbYes Then
    '        mskDtParcelaReativacao.Text = Format(dt_valida, "dd/mm/yyyy")
    '    End If
    'End If
    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''

    If Not (CDate(mskDtParcelaReativacao.Text) > (CDate(Data_Sistema) + 15)) Then
        MsgBox ("N�o � permitido data de parcela menor que a data atual + 15 dias. Ou seja, a data informada deve ser superior �: " & Format((CDate(Data_Sistema) + 15), "dd/mm/yyyy"))
        mskDtParcelaReativacao.SetFocus
    End If

    sData1 = Mid$(Format(CDate(mskDtParcelaReativacao.Text), "yyyymmdd"), 1, 6)
    Select Case vlProdutoId
    Case 200, 201, 202, 203, 204, 205, 208, 209, 210, 211, 213, 216, 217, 218, 219, 221, 222, 223, 224, 225, 226
        For iCont = 0 To grdParcelasReativacao.Rows - 1
            sData2 = grdParcelasReativacao.TextMatrix(iCont, 4)
            If sData1 = sData2 Then
                MsgBox ("A data escolhida j� existe, relativo ao ano e mes de referecia da parcela.")
                Exit Sub
            End If
        Next

        sLinha = montaLinha(lUltimaParcelaReativacao, mskDtParcelaReativacao.Text, mskValParcelaReativacao.Text)
        lsMsg = ValidaParcelaInc(sLinha)
        If lsMsg <> "" Then
            MsgBox (lsMsg)
            Exit Sub
        End If
    Case Else
        'lrocha 20/08/2009 - Tratamento 987 - Recusa: "A DEFINIR"
        For iCont = 0 To grdParcelasReativacao.Rows - 1
            If mskDtParcelaReativacao.Text = grdParcelasReativacao.TextMatrix(iCont, 2) And oDadosEndosso.produtoId <> 1152 And oDadosEndosso.produtoId <> 1204 Then
                MsgBox ("A data escolhida j� existe")
                Exit Sub
            End If
        Next

    End Select
    ' fim - 20/12/2015 - schoralick (nova consultoria) - - inicio

    sLinha = ""
    sLinha = sLinha & lUltimaParcelaReativacao & vbTab
    sLinha = sLinha & mskValParcelaReativacao.Text & vbTab
    '20-12-2015 - Schoralick (nova consultoria) - 18690344 - Melhorias Processo Reagendamento e Reativa��o BESC - inicio
    sLinha = sLinha & mskDtParcelaReativacao.Text & vbTab

    Select Case vlProdutoId
    Case 200, 201, 202, 203, 204, 205, 208, 209, 210, 211, 213, 216, 217, 218, 219, 221, 222, 223, 224, 225, 226
        sLinha = sLinha & "i" & vbTab
        sLinha = sLinha & Mid$(Format(CDate(mskDtParcelaReativacao.Text), "yyyymmdd"), 1, 6)
    Case Else
        sLinha = sLinha & "i"
    End Select
    '20-12-2015 - Schoralick (nova consultoria) - 18690344 - Melhorias Processo Reagendamento e Reativa��o BESC - fim
    Call grdParcelasReativacao.AddItem(sLinha)

    lUltimaParcelaReativacao = lUltimaParcelaReativacao + 1
End Sub


Private Sub CmdOkCor_Click()

    MousePointer = vbHourglass

    If ValidarEndossoCorretagem Then
        Call ProcessarEndossoCorretagem
    End If

    MousePointer = vbDefault

End Sub

Function ValidarEndossoCorretagem() As Boolean

    Dim i As Integer
    Dim oCorretor As Object
    Dim oSubGrupo As Object

    ValidarEndossoCorretagem = False

    i = 0

    If colSubGrupo.Count = 0 Then

        Set oCorretor = CreateObject("SEGL0140.cls00149")

        For Each oCorretor In colCorretor
            If oCorretor.Operacao <> "E" Then i = i + 1
        Next

        Set oCorretor = Nothing

        If i = 0 Then
            MsgBox "Ap�lice tem que ter pelo menos um corretor!", vbCritical
            Exit Function
        End If

        If Not ValidarPercComissao(colCorretor) Then
            Exit Function
        End If

    Else

        Set oSubGrupo = CreateObject("SEGL0140.cls00175")

        For Each oSubGrupo In colSubGrupo

            Set colCorretor = Nothing
            i = 0

            Set colCorretor = oSubGrupo.Corretores

            For Each oCorretor In colCorretor
                If oCorretor.Operacao <> "E" Then i = i + 1
            Next

            If i = 0 Then
                MsgBox "SubGrupo " & Format(oSubGrupo.ID, "000") & " - " & oSubGrupo.Nome & " n�o tem corretor associado!", vbCritical
                Exit Function
            End If

            If Not ValidarPercComissao(oSubGrupo.Corretores) Then
                Exit Function
            End If
        Next
    End If

    ValidarEndossoCorretagem = True

End Function

Public Function ValidarPercComissao(ByRef colCorret As Collection) As Boolean

    Dim dAuxComissao As Double
    Dim iCont As Integer

    ValidarPercComissao = True
    dAuxComissao = 0

    If colCorret.Count > 0 Then
        For iCont = 1 To colCorret.Count
            If colCorret(iCont).Operacao <> "E" Then
                dAuxComissao = dAuxComissao + CDbl(colCorret(iCont).PercComissao)
            End If
        Next

        If dAuxComissao < 0 Then
            MsgBox "Percentual total de comiss�o abaixo de 0%!", vbInformation
            ValidarPercComissao = False
        ElseIf dAuxComissao >= 100 Then
            MsgBox "Percentual total de comiss�o acima de 100%!", vbInformation
            ValidarPercComissao = False
            '        ElseIf dAuxComissao > Val(mskCorretagemAnt.Text) And (txtFimVigencia.Text <> "") Then
            '            MsgBox "Percentual total de comiss�o acima do limite da Ap�lice!", vbInformation
            '            ValidarPercComissao = False
        End If
    End If

End Function

Sub ProcessarEndossoCorretagem()

    Dim OEndosso As Object

    On Error GoTo Erro

    Set OEndosso = CreateObject("SEGL0140.cls00172")

    If Not OEndosso.GerarEndossoCorretagem(gsSIGLASISTEMA, App.Title, App.FileDescription, glAmbiente_id, _
                                           oDadosEndosso.ApoliceId, oDadosEndosso.SucursalSeguradoraId, _
                                           oDadosEndosso.SeguradoraCodSusep, oDadosEndosso.PropostaId, _
                                           oDadosEndosso.RamoId, oDadosEndosso.TipoEndosso, _
                                           mskDtEndosso.Text, mskDtEmissao.Text, colCorretor, colSubGrupo, _
                                           txtDescEndosso.Text, cUserName) Then
        Exit Sub
    End If

    Call MsgBox("Endosso realizado com sucesso." & vbNewLine & _
                "Proposta: " & oDadosEndosso.PropostaId & vbNewLine & _
                "Endosso : " & txtNumEndosso.Text, vbInformation)

    Call sair

    Set OEndosso = Nothing

    Exit Sub

Erro:
    Call TratarErro("ProcessarEndossoCorretagem", Me.name)
    Call FinalizarAplicacao
End Sub

Private Sub cmdPagAlterar_Click()

    Call AlterarPagamento

End Sub

Private Sub AlterarPagamento()

    Dim iCont As Integer
    Dim cTotValLiquidoGrid As Currency
    Dim cTotValFracGrid As Currency
    Dim cTotValIOFGrid As Currency
    Dim cTotValDescGrid As Currency
    Dim cTotValTarifGrid As Currency
    Dim cTotValComGrid As Currency

    On Error GoTo TrataErro

    If Not ValidarGridPagamento Then
        Exit Sub
    End If

    Call RemontarGridPagamento

    lblDtVcto.Caption = "Data  1�  Vcto:"

    grdPagamento.Row = iAuxLinha

    lblParcAlt.Visible = False
    lblNumParcAlt.Visible = False

    mskTotTarifa.Text = Format(CCur(mskTotTarifa.Text) + CCur(mskValPremioTarifa.Text) - CCur(grdPagamento.TextMatrix(grdPagamento.Row, 2)), "###########0.00")
    mskTotDesc.Text = Format(CCur(mskTotDesc.Text) + CCur(mskValDesc.Text) - CCur(grdPagamento.TextMatrix(grdPagamento.Row, 4)), "###########0.00")
    mskTotJuros.Text = Format(CCur(mskTotJuros.Text) + CCur(mskValJuros.Text) - CCur(grdPagamento.TextMatrix(grdPagamento.Row, 5)), "###########0.00")
    mskTotIof.Text = Format(CCur(mskTotIof.Text) + CCur(mskValIOF.Text) - CCur(grdPagamento.TextMatrix(grdPagamento.Row, 6)), "###########0.00")
    mskTotCom.Text = Format(CCur(mskTotCom.Text) + CCur(mskValCom.Text) - CCur(grdPagamento.TextMatrix(grdPagamento.Row, 7)), "###########0.00")
    mskTotLiquido.Text = CCur(mskTotTarifa.Text) - CCur(mskTotDesc.Text) + CCur(mskTotJuros.Text) + CCur(mskTotIof.Text) + CCur(mskCustoEndossoCob.Text)

    ' Inserindo no grid ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    grdPagamento.TextMatrix(grdPagamento.Row, 1) = mskDtPagamento.Text
    grdPagamento.TextMatrix(grdPagamento.Row, 2) = mskValPremioTarifa.Text
    grdPagamento.TextMatrix(grdPagamento.Row, 3) = mskCustoEndossoCob.Text
    grdPagamento.TextMatrix(grdPagamento.Row, 4) = mskValDesc.Text
    grdPagamento.TextMatrix(grdPagamento.Row, 5) = mskValJuros.Text
    grdPagamento.TextMatrix(grdPagamento.Row, 6) = mskValIOF.Text
    grdPagamento.TextMatrix(grdPagamento.Row, 7) = mskValCom.Text
    grdPagamento.TextMatrix(grdPagamento.Row, 8) = mskValLiquido.Text

    cTotValLiquidoGrid = 0
    cTotValFracGrid = 0
    cTotValIOFGrid = 0
    cTotValDescGrid = 0
    cTotValTarifGrid = 0
    cTotValComGrid = 0

    For iCont = 1 To grdPagamento.Rows - 2
        cTotValLiquidoGrid = CCur(cTotValLiquidoGrid) + CCur(grdPagamento.TextMatrix(iCont, 8))
        cTotValFracGrid = CCur(cTotValFracGrid) + grdPagamento.TextMatrix(iCont, 5)
        cTotValIOFGrid = CCur(cTotValIOFGrid) + grdPagamento.TextMatrix(iCont, 6)
        cTotValDescGrid = CCur(cTotValDescGrid) + grdPagamento.TextMatrix(iCont, 4)
        cTotValTarifGrid = CCur(cTotValTarifGrid) + grdPagamento.TextMatrix(iCont, 2)
        cTotValComGrid = CCur(cTotValComGrid) + grdPagamento.TextMatrix(iCont, 7)
    Next

    grdPagamento.TextMatrix(grdPagamento.Rows - 1, 8) = Format(CCur(mskTotLiquido.Text) - CCur(cTotValLiquidoGrid), "##0.00")
    grdPagamento.TextMatrix(grdPagamento.Rows - 1, 4) = Format(CCur(mskTotDesc.Text) - CCur(cTotValDescGrid), "##0.00")
    grdPagamento.TextMatrix(grdPagamento.Rows - 1, 5) = Format(CCur(mskTotJuros.Text) - CCur(cTotValFracGrid), "##0.00")
    grdPagamento.TextMatrix(grdPagamento.Rows - 1, 6) = Format(CCur(mskTotIof.Text) - CCur(cTotValIOFGrid), "##0.00")
    grdPagamento.TextMatrix(grdPagamento.Rows - 1, 2) = Format(CCur(mskTotTarifa.Text) - CCur(cTotValTarifGrid), "##0.00")
    grdPagamento.TextMatrix(grdPagamento.Rows - 1, 7) = Format(CCur(mskTotCom.Text) - CCur(cTotValComGrid), "##0.00")

    Call LimparVariaveisPgto
    cmdPagAlterar.Enabled = False

    Exit Sub

TrataErro:
    Call TratarErro("AlterarPagamento", Me.name)
    Call FinalizarAplicacao

End Sub


Public Sub RemontarGridPagamento()

    Dim iCont As Integer

    On Error GoTo TrataErro

    For iCont = 1 To grdPagamento.Rows - 1
        grdPagamento.Row = iCont
        grdPagamento.TextMatrix(grdPagamento.Row, 0) = Val(grdPagamento.Row)
    Next

    Exit Sub

TrataErro:
    Call TratarErro("RemontarGridPagamento", Me.name)
    Call FinalizarAplicacao

End Sub

Private Sub cmdPesquisarCEP_Click()

    Dim rs As ADODB.Recordset, rsAux As ADODB.Recordset
    Dim oDadosBasicos As Object
    Dim lMunicipioId As Long
    Dim sMunicipio As String
    Dim sUF As String
    Dim sChvlocalDne As String

    'Dim sMunicipioDescricao As String

    On Error GoTo TrataErro

    If txtCep.ClipText <> "" Then

        Me.MousePointer = vbHourglass

        'Obtem o endere�o, de acordo com o CEP

        Set oDadosBasicos = CreateObject("SEGL0022.cls00117")

        Set rs = oDadosBasicos.consultarEnderecoPeloCEP(gsSIGLASISTEMA, _
                                                        App.Title, _
                                                        App.FileDescription, _
                                                        glAmbiente_id, _
                                                        txtCep.ClipText)



        If Not rs.EOF Then

            txtEndoCad(4).Text = Trim(rs!tp_logra) & " " & Trim(rs!logra)

            If IsNull(rs!Municipio) = False Then
                If rs!Municipio <> "" Then
                    cboCidade.Text = Trim(rs!Municipio) & " - " & Trim(rs!UF)
                Else

                    lMunicipioId = oDadosBasicos.IncluirMunicipio(gsSIGLASISTEMA, _
                                                                  App.Title, _
                                                                  App.FileDescription, _
                                                                  glAmbiente_id, _
                                                                  rs!Municipio, _
                                                                  rs!UF, _
                                                                  rs!chave_correio)


                    Call MontarComboCidade(cboCidade)
                    cboCidade.Text = Trim(rs!Municipio) & " - " & Trim(rs!UF)

                End If

            Else
                lMunicipioId = oDadosBasicos.IncluirMunicipio(gsSIGLASISTEMA, _
                                                              App.Title, _
                                                              App.FileDescription, _
                                                              glAmbiente_id, _
                                                              rs!Municipio, _
                                                              rs!UF, _
                                                              rs!chave_correio)


                Call MontarComboCidade(cboCidade)
                cboCidade.Text = Trim(rs!Municipio) & " - " & Trim(rs!UF)
            End If

            txtEndoCad(5).Text = IIf(IsNull(rs!Bairro), "", Trim(rs!Bairro))
            TxtUFAnt(1).Text = rs!UF
            sMunicipio = rs!Municipio
            sUF = rs!UF
            sChvlocalDne = rs!chave_correio

            txtEndoCad(0).SetFocus

            'Verifica se existe na municpio_tb

            Set rsAux = oDadosBasicos.ConsultarMunicipioNome(gsSIGLASISTEMA, _
                                                             App.Title, _
                                                             App.FileDescription, _
                                                             glAmbiente_id, _
                                                             sMunicipio, _
                                                             sUF, _
                                                             sChvlocalDne)


            If Not rsAux.EOF Then

                'Caso n�o tenha o ID da base dos Correios
                If rsAux!chvlocal_dne = "" Then

                    Call oDadosBasicos.alterarMunicipio(gsSIGLASISTEMA, _
                                                        App.Title, _
                                                        App.FileDescription, _
                                                        glAmbiente_id, _
                                                        rsAux!municipio_id, _
                                                        rsAux!tp_pessoa_id, _
                                                        rsAux!Nome, _
                                                        rsAux!estado, _
                                                        rsAux!municipio_ibge_id, _
                                                        rsAux!municipio_bacen_id, _
                                                        rsAux!municipio_bacen_dv, _
                                                        sChvlocalDne, _
                                                        cUserName)

                End If

            Else
                'Caso n�o exista em municipio_tb
                lMunicipioId = oDadosBasicos.IncluirMunicipio(gsSIGLASISTEMA, _
                                                              App.Title, _
                                                              App.FileDescription, _
                                                              glAmbiente_id, _
                                                              sMunicipio, _
                                                              sUF, _
                                                              sChvlocalDne, _
                                                              cUserName)


                Call MontarComboCidade(cboCidade)
                cboCidade.Text = Trim(rs!Municipio) & " - " & Trim(rs!UF)
            End If

        End If

        Set rs = Nothing

        Set oDadosBasicos = Nothing

        Me.MousePointer = vbDefault

    End If

    Exit Sub

TrataErro:
    Call TratarErro("cmdPesquisarCEP_Click", Me.name)
    Call FinalizarAplicacao

End Sub

Private Sub cmdRemoverBenef_Click()

    MousePointer = vbHourglass

    Call RemoverBeneficiario

    MousePointer = vbDefault

End Sub

Private Sub CmdRestaura_Click()

    TxtCodCorretor(0).Text = ""
    TxtCodCorretor(1).Text = ""
    TxtTpCorretor.Text = ""
    TxtSucCorretor.Text = ""
    mskPercCorretagem.Text = Format$(0, "#0.00000")
    GridCorretagem.Rows = 1

    cmdCorretorAdicionar.Enabled = False
    cmdCorretorAlterar.Enabled = False
    cmdCorretorRemover.Enabled = False

    Call MontarComboSubGrupo

    CmdOkCor.Enabled = False

End Sub

Private Sub cmdRestaurar_Click()

    Me.MousePointer = vbHourglass

    Call CarregarDadosCadastrais

    Me.MousePointer = vbDefault

End Sub

Private Sub cmdRestaurarBenef_Click()

    MousePointer = vbHourglass

    Call LimparCamposBenficiario

    'cmdAdicionarBenef.Enabled = False
    cmdAlterarBenef.Enabled = False
    cmdRemoverBenef.Enabled = False

    Call CarregarDadosBeneficiario

    cmdAplicarBenef.Enabled = False

    MousePointer = vbDefault

End Sub

Private Sub cmdRestaurarEndereco_Click()

    Me.MousePointer = vbHourglass

    Call CarregarDadosEndereco

    Me.MousePointer = vbDefault

End Sub

Private Sub cmdRestaurarEst_Click()

    cmdEstRemover.Enabled = False
    cmdEstAdicionar.Enabled = False
    cmdEstAlterar.Enabled = False
    grdEstipulanteEst.Rows = 1

    txtEstipulante.Text = ""
    txtNomeEstipulante(0).Text = ""
    mskPercEstipulante.mask = "3,4"
    mskPercEstipulante.Text = "0,0000"

    Call MontarComboSubGrupo

End Sub

Private Sub cmdSairBenef_Click()

    stbEndosso.TabEnabled(0) = True
    stbEndosso.TabVisible(7) = False
    stbEndosso.Tab = 0

End Sub

Private Sub cmdSairCadastral_Click()
'R.FOUREAUX 25/06/2012 - DEMANDA 12006056
'RESETA PADROES NOS FORMS SEM FECHAR APLICACAO
    txtEndoCad(0).Enabled = True
    txtEndoCad(0).BackColor = &H80000005
    cboTpPessoa.Enabled = True
    cboTpPessoa.BackColor = &H80000005
    txtCPFCNPJ.Enabled = True
    txtCPFCNPJ.BackColor = &H80000005
    cboSexo.Enabled = True
    cboSexo.BackColor = &H80000005
    cboEstCivil.Enabled = True
    cboEstCivil.BackColor = &H80000005
    txtNascimento.Enabled = True
    txtNascimento.BackColor = &H80000005
    txtEndoCad(1).Enabled = True
    txtEndoCad(1).BackColor = &H80000005
    txtEndoCad(2).Enabled = True
    txtEndoCad(2).BackColor = &H80000005
    txtEndoCad(3).Enabled = True
    txtEndoCad(3).BackColor = &H80000005
    cboFormaPagamento.Enabled = True
    cboFormaPagamento.BackColor = &H80000005
    txtforma_pgto(0).Enabled = False
    txtforma_pgto(0).BackColor = &H80000004
    txtforma_pgto(1).Enabled = False
    txtforma_pgto(1).BackColor = &H80000004
    txtBanco.Enabled = False
    txtBanco.BackColor = &H80000004
    txtEndoCad(6).Enabled = False
    txtEndoCad(6).BackColor = &H80000004
    txtCodAgDebito(0).Enabled = False
    txtCodAgDebito(0).BackColor = &H80000004
    txtCodAgDebito(1).Enabled = False
    txtCodAgDebito(1).BackColor = &H80000004

    '
    stbEndosso.TabEnabled(0) = True
    stbEndosso.TabVisible(4) = False
    stbEndosso.Tab = 0
    cmdAplicarCadastral.Enabled = True

End Sub

Private Sub cmdSairCancApolice_Click()

    stbEndosso.TabEnabled(0) = True
    stbEndosso.TabVisible(2) = False
    stbEndosso.Tab = 0

End Sub

Private Sub cmdSairCancEndosso_Click()

    stbEndosso.TabEnabled(0) = True
    stbEndosso.TabVisible(3) = False
    stbEndosso.Tab = 0

End Sub

Private Sub cmdSairCancSinistro_Click()

    stbEndosso.TabEnabled(0) = True
    stbEndosso.TabVisible(18) = False
    stbEndosso.Tab = 0

End Sub

Private Sub cmdSairCorretagem_Click()

    stbEndosso.TabEnabled(0) = True
    stbEndosso.TabVisible(6) = False
    stbEndosso.Tab = 0

End Sub


Private Sub cmdSairEst_Click()

    stbEndosso.TabEnabled(0) = True
    stbEndosso.TabVisible(5) = False
    stbEndosso.Tab = 0

End Sub

Private Sub cmdSairEndereco_Click()

    stbEndosso.TabEnabled(0) = True
    stbEndosso.TabVisible(8) = False
    stbEndosso.Tab = 0

End Sub

Private Sub cmdSairIS_Click()

    optAumIS.Enabled = True
    optCobAdi.Enabled = True
    optCobSem.Enabled = True

    optAumIS.Value = False
    optCobAdi.Value = False
    optCobSem.Value = False

    cValFinanceiro = 0
    cValPremioNovo = 0

    stbEndosso.TabEnabled(0) = True
    stbEndosso.TabVisible(10) = False
    stbEndosso.Tab = 0

    Call LimparCoberturas

End Sub

Private Sub cmdSairReativacaoFatura_Click()

    stbEndosso.TabEnabled(0) = True
    stbEndosso.TabVisible(11) = False
    stbEndosso.Tab = 0

End Sub

Private Sub cmdSairLimite_Click()

    stbEndosso.TabEnabled(0) = True
    stbEndosso.TabVisible(9) = False
    stbEndosso.Tab = 0

End Sub

Public Sub cmdSairProposta_Click()

    Call sair

    'Modifica��o realizada em 11/02/2010 -- Confitec - Rodrigo Souza
    'Descri��o: Fecha conex�o quando a tela de endosso � fechada.
    Call fechaConn
    '---------------------------------------------------------------'

End Sub

'Modifica��o realizada em 11/02/2010 -- Confitec - Rodrigo Souza
'Descri��o: Fecha conex�o quando a tela de endosso � fechada.
Private Sub fechaConn()
    If rdocn.State <> 0 Then
        rdocn.Close
    End If
    ''Inclu�do a pedido da area de desenvolvimento da BB Mapfre (n�o faz parte da demanda em quest�o)
    If Not rdocn_Seg2 Is Nothing Then
        If rdocn_Seg2.State = 1 Then rdocn_Seg2.Close
        Set rdocn_Seg2 = Nothing
    End If
    ''
End Sub
'---------------------------------------------------------------'
Private Sub cmdSairRestituicao_Click()

    Dim iCont As Integer

    Select Case oDadosEndosso.TipoEndosso
    Case TP_ENDOSSO_RESTITUICAO
        stbEndosso.TabEnabled(0) = True
        stbEndosso.TabVisible(1) = False
        stbEndosso.Tab = 0

    Case TP_ENDOSSO_CANCELAMENTO_APOLICE, TP_ENDOSSO_CANCELAMENTO_APOLICE_SEGURADORA
        stbEndosso.TabEnabled(2) = True
        stbEndosso.TabVisible(1) = False
        stbEndosso.Tab = 2

    Case TP_ENDOSSO_IMPORTANCIA_SEGURADA
        stbEndosso.TabEnabled(10) = True
        stbEndosso.TabVisible(1) = False
        stbEndosso.Tab = 10
        Call InicializarFrameCobranca
    Case Else
        stbEndosso.TabEnabled(0) = True
        For iCont = 1 To stbEndosso.Tabs - 1
            stbEndosso.TabEnabled(iCont) = True
            stbEndosso.TabVisible(iCont) = False
        Next
        stbEndosso.Tab = 0

    End Select

End Sub


Private Sub ConMask2S1_GotFocus()

End Sub

Private Sub Form_Load()
    Call CentraFrm(Me)

    bObteveSaldoRestituir = True    'cristovao.rodrigues 25/04/2013

    Me.chkPPE.Enabled = False
    Call ConfigurarInterface
    Call Conexao

End Sub

Sub ObterPercCorretagem(ByVal lPropostaId As Long)

    Dim oEmissao As Object

    On Error GoTo Erro

    Set oEmissao = CreateObject("SEGL0026.cls00126")

    cPercCorretagem = oEmissao.ObterPercCorretagem(gsSIGLASISTEMA, _
                                                   App.Title, _
                                                   App.FileDescription, _
                                                   glAmbiente_id, _
                                                   lPropostaId)

    Set oEmissao = Nothing

    Exit Sub

Erro:
    Call TratarErro("ObterPercCorretagem", Me.name)
    Call FinalizarAplicacao
End Sub

Private Sub ConfigurarInterface()

    Dim lPropostaId As Long
    Dim lApoliceId As Long
    Dim LramoId As Long
    Dim lSubRamoId As Long
    Dim iCont As Integer

    On Error GoTo TrataErro

    Me.Caption = "SEGP0777 - Endosso On-Line"

    Call DefinirConfiguracaoBrasil

    'bcarneiro - 02/03/2007
    bRestIntegral = False
    ''''''''''''''''''''''''

    For iCont = 1 To stbEndosso.Tabs - 1
        stbEndosso.TabVisible(iCont) = False
    Next

    With frmSelecao

        lApoliceId = .grdSelecao.TextMatrix(.grdSelecao.Row, 0)
        txtApolice.Text = Format(lApoliceId, "000000000")

        txtPropostaid(1).Text = .grdSelecao.TextMatrix(.grdSelecao.Row, 1)

        lPropostaId = .grdSelecao.TextMatrix(.grdSelecao.Row, 3)
        txtPropostaid(0).Text = lPropostaId

        txtDtContratacao.Text = .grdSelecao.TextMatrix(.grdSelecao.Row, 6)

        LramoId = .grdSelecao.TextMatrix(.grdSelecao.Row, 9)
        txtRamo.Text = CStr(LramoId)
        txtNomeRamo.Text = .grdSelecao.TextMatrix(.grdSelecao.Row, 10)

        lSubRamoId = .grdSelecao.TextMatrix(.grdSelecao.Row, 12)
        txtSubramo.Text = CStr(lSubRamoId)
        txtNomeSubramo.Text = .grdSelecao.TextMatrix(.grdSelecao.Row, 13)

        txtProduto.Text = .grdSelecao.TextMatrix(.grdSelecao.Row, 4)
        txtNomeProduto.Text = .grdSelecao.TextMatrix(.grdSelecao.Row, 11)

        oDadosEndosso.ClienteId = .grdSelecao.TextMatrix(.grdSelecao.Row, 8)

        'Modifica��o Realizada em 03/02/2010 - Confitec (Rodrigo Souza)
        'Descri��o: Recebe par�metro do servidor, conforme a linha selecionada no Grid.
        glAmbiente_id = .grdSelecao.TextMatrix(.grdSelecao.Row, 16)
        '        MsgBox glAmbiente_id
        '        glAmbiente_id = 4 'comentar - Rodrigo
        '---------------------------------------------------------------------------------'

        'Luciana - 31/01/2005
        lSeguro_moeda_id = .grdSelecao.TextMatrix(.grdSelecao.Row, 14)
        lPremio_moeda_id = .grdSelecao.TextMatrix(.grdSelecao.Row, 15)

    End With

    Call InicializarVetor
    Call ConsultarProposta(lPropostaId)
    Call ConsultarEndosso(lPropostaId)
    Call ConsultarPercIR
    'Como o c�lculo do IOF necessita de uma data base (e a data nesse caso � a do pedido do
    'endosso), o c�lculo do mesmo foi transferido para o evento click do bot�o "aplicar"
    'dessa aba (nesse momento a data de pedido do endosso est� consolidada) - Flow 276233
    'Call ConsultarPercIOF
    Call ConsultarBeneficiario(lPropostaId)
    'lrocha 07/12/2006
    Call ConsultarSubvencaoProduto(txtProduto.Text)

    mskDtEndosso.Text = Format(Data_Sistema, "dd/mm/yyyy")
    mskDtEmissao.Text = Format(Data_Sistema, "dd/mm/yyyy")

    Call PreencherListaEndossos(lPropostaId)

    If txtProduto.Text <> 1168 Then
        Call ConsultaPropostaModular(lPropostaId)
    Else

        oDadosModular.PropostaId = lPropostaId
        txtProposta(0).Text = lPropostaId
        txtProposta(1).Text = lPropostaId

        Call ObterTipoCobertura(lPropostaId, Val(txtProduto), Val(txtRamo.Text))

    End If

    cmdAplicarProposta.Enabled = True


    'Else
    '    bModular = True
    'End If

    Exit Sub

TrataErro:
    Call TratarErro("ConfigurarInterface", Me.name)
    Call FinalizarAplicacao

End Sub
Public Sub ObterTipoCobertura(ByVal lPropostaId As Long, _
                              ByVal iProdutoId As Integer, _
                              ByVal iRamoId As Integer)

    On Error GoTo Trata_Erro

    Dim oConsulta As Object
    Dim oResultado As Object

    Set oConsulta = CreateObject("SEGL0026.cls00126")
    Set oResultado = oConsulta.ObterCoberturaHabitacional(gsSIGLASISTEMA, _
                                                          App.Title, _
                                                          App.FileDescription, _
                                                          glAmbiente_id, _
                                                          iProdutoId, _
                                                          iRamoId, _
                                                          lPropostaId)



    Exit Sub
Trata_Erro:
    Call TratarErro("ConfigurarInterface", Me.name)
    Call FinalizarAplicacao

End Sub


Public Sub ConsultarSubvencaoProduto(ByVal produtoId As Integer)

    Dim oConsulta As Object
    Dim oResultado As Recordset

    On Error GoTo TrataErro

    bPossuiSubvencao = False

    Set oConsulta = CreateObject("SEGL0026.cls00126")

    Set oResultado = oConsulta.ObterSubvencaoProduto(gsSIGLASISTEMA, _
                                                     App.Title, _
                                                     App.FileDescription, _
                                                     glAmbiente_id, _
                                                     produtoId)

    If Not oResultado.EOF Then
        If UCase(oResultado("possui_subvencao")) = "S" Then
            bPossuiSubvencao = True
        End If
    End If

    Set oConsulta = Nothing
    Set oResultado = Nothing

    Exit Sub

TrataErro:
    Call TratarErro("ConfigurarInterface", Me.name)
    Call FinalizarAplicacao

End Sub
Sub DefinirConfiguracaoBrasil()

    Dim sDecimal As String

    sDecimal = LeArquivoIni("WIN.INI", "intl", "sDecimal")
    If sDecimal = "." Then
        bConfiguracaoBrasil = False
    Else
        bConfiguracaoBrasil = True
    End If

End Sub

Private Sub ConsultarObjetoSegurado(ByVal lPropostaId As Long)

    Dim rsObjeto As Recordset
    Dim oSeguro As Object

    On Error GoTo Erro

    Set oSeguro = CreateObject("SEGL0148.cls00181")

    Set oSeguro.oConexao = oConexao

    Set rsObjeto = oSeguro.ConsultarObjetoSegurado(lPropostaId)

    If Not rsObjeto.EOF Then

        lSeqCancEndossoSeg = rsObjeto("seq_canc_endosso_seg")

        sTabelaSeguro = rsObjeto("tabela")

        cboObjetoSegurado.Clear

        While Not rsObjeto.EOF

            cboObjetoSegurado.AddItem rsObjeto("cod_objeto_segurado")
            cboObjetoSegurado.ItemData(cboObjetoSegurado.NewIndex) = rsObjeto("cod_objeto_segurado")

            Call AssociarDataVigenciaObjeto(rsObjeto("cod_objeto_segurado"), rsObjeto("dt_inicio_vigencia_seg"))

            rsObjeto.MoveNext

        Wend

        cboObjetoSegurado.ListIndex = 0
        rsObjeto.Close

    End If

    Set rsObjeto = Nothing
    Set oSeguro = Nothing

    Exit Sub

Erro:
    Call TratarErro("ConsultarObjetoSegurado", Me.name)
    Call FinalizarAplicacao
End Sub

Private Sub ConsultarCoberturas(ByVal lPropostaId As Long, ByVal iProdutoId As Integer)

    Dim oSeguro As Object
    Dim rsCobertura As Recordset

    On Error GoTo Erro

    Set oSeguro = CreateObject("SEGL0148.cls00181")

    Set oSeguro.oConexao = oConexao

    Set rsCobertura = oSeguro.ConsultarValoresCoberturas(lPropostaId, iProdutoId, txtIniVigencia.Text)

    If Not rsCobertura.EOF Then

        Call MontarColecaoCobertura(rsCobertura)
        Call MontarGridCoberturasAtual(1)

        rsCobertura.Close

    Else

        'Buscando a tabela onde ser�o
        'gravadas as coberturas do endosso, j� que esta ainda n�o possui
        'nenhuma cobertura

        sTabelaEscolha = oSeguro.ConsultarTabelaCobertura(lPropostaId)

        If sTabelaEscolha = "" Then
            Call MsgBox("Proposta de Produto que n�o permite endosso On-Line." & vbCrLf & "Produto : " & iProdutoId, vbCritical)
            Call FinalizarAplicacao
        End If

    End If

    Set oSeguro = Nothing
    Set rsCobertura = Nothing

    Exit Sub

Erro:
    Call TratarErro("ConsultarCoberturas", Me.name)
    Call FinalizarAplicacao
End Sub

Sub MontarGridCoberturasEndossadas(ByVal iCodObjetoSegurado As Integer)

    Dim sLinha As String
    Dim oCobertura As Cobertura

    On Error GoTo Erro

    grdCoberturas.Rows = 1

    For Each oCobertura In colCoberturasEndossadas
        If oCobertura.CodObjSegurado = iCodObjetoSegurado Then

            sLinha = oCobertura.TpCoberturaId & vbTab
            sLinha = sLinha & oCobertura.DESCRICAO & vbTab
            sLinha = sLinha & oCobertura.ValIS & vbTab
            sLinha = sLinha & oCobertura.ValTaxa & vbTab
            sLinha = sLinha & oCobertura.PercDesconto & vbTab
            sLinha = sLinha & oCobertura.ValPremio & vbTab
            sLinha = sLinha & oCobertura.DescFranquia & vbTab
            sLinha = sLinha & oCobertura.ValFranquia & vbTab
            sLinha = sLinha & oCobertura.FatFranquia & vbTab
            sLinha = sLinha & oCobertura.DtInicioVigencia & vbTab
            sLinha = sLinha & oCobertura.DtFimVigencia

            grdCoberturas.AddItem sLinha

        End If
    Next

    Exit Sub

Erro:
    Call TratarErro("MontarGridCoberturasAtual", Me.name)
    Call FinalizarAplicacao
End Sub

Sub MontarGridCoberturasAtual(ByVal iCodObjetoSegurado As Integer)

    Dim oCobertura As Cobertura
    Dim sLinha As String

    On Error GoTo Erro

    grdCoberturasAtual.Rows = 1

    For Each oCobertura In colCoberturas

        If oCobertura.CodObjSegurado = iCodObjetoSegurado Then

            sLinha = oCobertura.TpCoberturaId & vbTab
            sLinha = sLinha & oCobertura.DESCRICAO & vbTab
            sLinha = sLinha & oCobertura.ValIS & vbTab
            sLinha = sLinha & oCobertura.ValTaxa & vbTab
            sLinha = sLinha & oCobertura.PercDesconto & vbTab
            sLinha = sLinha & oCobertura.ValPremio & vbTab
            sLinha = sLinha & oCobertura.DescFranquia & vbTab
            sLinha = sLinha & oCobertura.ValFranquia & vbTab
            sLinha = sLinha & oCobertura.FatFranquia & vbTab
            sLinha = sLinha & oCobertura.DtInicioVigencia & vbTab
            sLinha = sLinha & oCobertura.DtFimVigencia

            If oCobertura.ClasCobertura = "B" Then
                dRiscoCoberturaBasica = oCobertura.ValIS
                lCodCoberturaBasica = oCobertura.TpCoberturaId
            End If

            grdCoberturasAtual.AddItem sLinha

        End If
    Next

    Exit Sub

Erro:
    Call TratarErro("MontarGridCoberturasAtual", Me.name)
    Call FinalizarAplicacao
End Sub

Sub MontarColecaoCobertura(ByRef rs As Recordset)

    Dim dDesconto As Double
    Dim oCobertura As New Cobertura
    Dim sValPremio As String
    Dim sValIS As String
    Dim sFatTaxa As String
    Dim sValFranquia As String
    Dim sFatFranquia As String

    On Error GoTo Erro

    cValPremioAnterior = 0

    Select Case rs("tabela")
    Case "1"
        sTabelaEscolha = "escolha_tp_cob_emp_tb"
    Case "2"
        sTabelaEscolha = "escolha_tp_cob_res_tb"
    Case "3"
        sTabelaEscolha = "escolha_tp_cob_cond_tb"
    Case "4"
        sTabelaEscolha = "escolha_tp_cob_maq_tb"
    Case "5"
        sTabelaEscolha = "escolha_tp_cob_aceito_tb"
    Case "6"
        sTabelaEscolha = "escolha_tp_cob_avulso_tb"
    Case "7"
        sTabelaEscolha = "escolha_tp_cob_generico_tb"
    End Select

    While Not rs.EOF

        If bConfiguracaoBrasil Then
            sValIS = Format(CCur(rs("val_is")), "###,###,###,##0.00")
        Else
            sValIS = TrocaValorAmePorBras(Format(CCur(rs("val_is")), "###,###,###,##0.00"))
        End If

        If bConfiguracaoBrasil Then
            sFatTaxa = Format(ValN(rs("fat_taxa")), "0.0000000")
        Else
            sFatTaxa = TrocaValorAmePorBras(Format(ValN(rs("fat_taxa")), "0.0000000"))
        End If

        If Not IsNull(rs("fat_desconto_tecnico")) Then
            dDesconto = Val(rs("fat_desconto_tecnico"))
            If Not dDesconto = 0 Then
                dDesconto = (1 - dDesconto) * 100
            End If
        Else
            dDesconto = 0
        End If

        If bConfiguracaoBrasil Then
            dDesconto = Format(dDesconto, "##0.00")
        Else
            dDesconto = TrocaValorAmePorBras(Format(dDesconto, "##0.00"))
        End If

        If bConfiguracaoBrasil Then
            sValPremio = Format(CCur(rs("val_Premio")), "###,###,###,##0.00")
        Else
            sValPremio = TrocaValorAmePorBras(Format(CCur(rs("val_Premio")), "###,###,###,##0.00"))
        End If

        If bConfiguracaoBrasil Then
            sFatFranquia = Format(ValN(rs("fat_franquia")), "0.00000")
        Else
            sFatFranquia = TrocaValorAmePorBras(Format(Val(rs("fat_franquia")), "0.00000"))
        End If

        If bConfiguracaoBrasil Then
            sValFranquia = Format(ValN(rs("val_min_franquia")), "###,###,###,##0.00")
        Else
            sValFranquia = TrocaValorAmePorBras(Format(Val(rs("val_min_franquia")), "###,###,###,##0.00"))
        End If

        Set oCobertura = New Cobertura

        With oCobertura
            .CodObjSegurado = rs("cod_objeto_segurado")
            .TpCoberturaId = rs("Tp_Cobertura_id")
            .DESCRICAO = rs("Nome")
            .ValIS = sValIS
            .ValTaxa = sFatTaxa
            .PercDesconto = dDesconto
            .ValPremio = sValPremio
            .ClasCobertura = IIf(IsNull(rs("class_tp_cobertura")), "", rs("class_tp_cobertura"))
            .SeqCancSeg = rs("seq_canc_endosso_seg")

            If Not IsNull(rs("texto_franquia")) Then
                .DescFranquia = rs("texto_franquia")
            Else
                .DescFranquia = " "
            End If

            .ValFranquia = sValFranquia
            .FatFranquia = sFatFranquia
            .DtInicioVigencia = Format$(rs("dt_inicio_vigencia_esc"), "dd/mm/yyyy")
            .Persistente = True
            .Operacao = "O"
            .Acumula_IS = IIf(IsNull(rs("Acumula_IS")), "", rs("Acumula_IS"))
            .Acumula_Resseguro = IIf(IsNull(rs("Acumula_Resseguro")), "", rs("Acumula_Resseguro"))

            If Not IsNull(rs("dt_fim_vigencia_esc")) Then
                .DtFimVigencia = Format(rs("dt_fim_vigencia_esc"), "dd/mm/yyyy")
            Else
                .DtFimVigencia = ""
            End If
        End With

        colCoberturas.Add oCobertura

        'Armazena Pr�mio anterior para validar se � restitui��o ou cobran�a adicional
        cValPremioAnterior = Format(cValPremioAnterior + sValPremio, "0.00")

        rs.MoveNext

    Wend

    'Inicializo pr�mio novo com o pr�mio anterior se n�o houver altera��o
    cValPremioNovo = cValPremioAnterior

    Exit Sub

Erro:
    Call TratarErro("MontarColecaoCobertura", Me.name)
    Call FinalizarAplicacao
End Sub

Public Sub AssociarDataVigenciaObjeto(ByVal iCodObjetoSegurado As Integer, _
                                      ByVal sDtInicioVigencia As String)

    If iCodObjetoSegurado > iMaiorObjeto Then
        ReDim Preserve sDtIniVigenciaSeg(iCodObjetoSegurado + 1)
        iMaiorObjeto = iCodObjetoSegurado
    End If

    If VerificaData2(Format(sDtInicioVigencia, "dd/mm/yyyy")) Then
        sDtIniVigenciaSeg(iCodObjetoSegurado) = Format(sDtInicioVigencia, "dd/mm/yyyy")
    Else
        sDtIniVigenciaSeg(iCodObjetoSegurado) = "01/01/1901"
    End If

End Sub

Public Sub IniciarConexao(ByVal sSiglaSistema As String, _
                          ByVal sSiglaRecurso As String, _
                          ByVal sDescricaoRecurso As String, _
                          ByVal iAmbienteId As Integer, _
                          ByVal sUsuario As String)

    Set oConexao = CreateObject("SEGL0171.cls00229")

    Call oConexao.CriarConexao(sSiglaSistema, _
                               sSiglaRecurso, _
                               sDescricaoRecurso, _
                               iAmbienteId, _
                               sUsuario)

End Sub

Private Sub Form_Resize()

    On Error GoTo TrataErro

    Call InicializarRodape

    Exit Sub

TrataErro:
    Call TratarErro("Form_Resize", Me.name)
    Call FinalizarAplicacao

End Sub

Private Sub InicializarVetor()

    VetorEstadoCivil(0) = "C"
    VetorEstadoCivil(1) = "D"
    VetorEstadoCivil(2) = "E"
    VetorEstadoCivil(3) = "S"
    VetorEstadoCivil(4) = "V"
    VetorEstadoCivil(5) = "N"

End Sub

Private Sub PreencherListaEndossos(ByVal lPropostaId As Long)

    Dim oDadosBasicos As Object
    Dim rs As Recordset
    Dim sOutrosCanais As String

    On Error GoTo TrataErro

    sOutrosCanais = VerificaOutrosCanais(txtProduto.Text)
    'Pelegrini - 13/11/2014 - 18353226
    'VALIDA��O DE ACEITE DE ENDOSSO 80 PARA OS PRODUTOS ABAIXO:
    'CASO EXISTIR UM PROCESSO DO ENDOSSO 80 EM ANDAMENTO OU ACEITO PELO BB, O ENDOSSO 101 N�O SER� HABILITADO PARA ESTES PRODUTOS.
    Dim bLiberar As Boolean
    bLiberar = True
    If txtProduto.Text = 227 Or txtProduto.Text = 228 Or txtProduto.Text = 229 Or _
       txtProduto.Text = 1152 Or txtProduto.Text = 1204 Or txtProduto.Text = 300 Or txtProduto.Text = 701 Or _
       txtProduto.Text = 155 Or txtProduto.Text = 156 Or txtProduto.Text = 8 Or _
       txtProduto.Text = 230 Or txtProduto.Text = 302 Or _
       txtProduto.Text = 14 Or txtProduto.Text = 722 Then
       'Rafael Martins - demanda C00149269 - EMISS�O RURAL NA ABS - inclus�o dos produto 230 e 302

        If rdocn Is Nothing Then
            Call Conexao
        Else
            If rdocn.State = 0 Then Call Conexao
        End If

        '09/09/2016 - Schoralick (ntendencia) - INICIO - 18927546 - Altera��o no Processo de Endosso de Movimenta��o
        'SQL = "EXEC SEGS12268_SPS " & lPropostaId & ", " & txtProduto.Text
        SQL = "EXEC SEGS12268_SPS " & lPropostaId & ", " & txtProduto.Text & ", 1"    'Adicionando parametro "1" para query verificar se proposta esta aprovada gerencialmente e recusada na subscri�ao da restitui��o
        '09/09/2016 - Schoralick (ntendencia) - FIM - 18927546

        Set rs = rdocn.Execute(SQL)

        If Not rs.EOF Then
            'If rs(0) = "S" Then 'O rs(0) -> retorna "s" min�sculo
            If UCase(rs(0)) = "S" Then    'Add Renato Fran�a - 17/03/2016 - INC000004877160
                rs.Close
                bLiberar = False
            Else
                rs.Close
            End If
        Else
            rs.Close
        End If


        Set rdocn = Nothing
        Set rdocn_Seg = Nothing
        Set rdocn_Seg2 = Nothing


    End If

    'FIM Pelegrini - 13/11/2014 - 18353226


    Set oDadosBasicos = CreateObject("SEGL0022.cls00117")

    Set rs = oDadosBasicos.ConsultarSubramoTpEndosso(gsSIGLASISTEMA, _
                                                     App.Title, _
                                                     App.FileDescription, _
                                                     glAmbiente_id, _
                                                     lPropostaId)

    Set oDadosBasicos = Nothing

    If Not rs.EOF Then

        While Not rs.EOF

            'Demanda 19346897 - IN�CIO
            If rs("tp_endosso_id") = 63 Then

                If txtProduto.Text <> 156 And _
                   txtProduto.Text <> 227 And _
                   txtProduto.Text <> 228 And _
                   txtProduto.Text <> 229 And _
                   txtProduto.Text <> 300 And _
                   txtProduto.Text <> 610 And _
                   txtProduto.Text <> 650 And _
                   txtProduto.Text <> 680 And _
                    txtProduto.Text <> 230 And _
                    txtProduto.Text <> 302 And _
                   txtProduto.Text <> 701 Then
                    'Rafael Martins - demanda C00149269 - EMISS�O RURAL NA ABS - inclus�o dos produto 230 e 302

                    cboTipoEndosso.AddItem UCase(rs("descricao"))
                    cboTipoEndosso.ItemData(cboTipoEndosso.NewIndex) = rs("tp_endosso_id")
                End If
            Else
                'Demanda 19346897 - FIM

                If (txtProduto.Text = 200 Or _
                    txtProduto.Text = 201 Or _
                    txtProduto.Text = 202 Or _
                    txtProduto.Text = 203 Or _
                    txtProduto.Text = 204 Or _
                    txtProduto.Text = 205 Or _
                    txtProduto.Text = 208 Or _
                    txtProduto.Text = 209 Or _
                    txtProduto.Text = 210 Or _
                    txtProduto.Text = 211 Or _
                    txtProduto.Text = 212 Or _
                    txtProduto.Text = 213 Or _
                    txtProduto.Text = 216 Or _
                    txtProduto.Text = 217 Or _
                    txtProduto.Text = 218 Or _
                    txtProduto.Text = 219 Or _
                    txtProduto.Text = 220 Or _
                    txtProduto.Text = 221 Or _
                    txtProduto.Text = 224 Or _
                    txtProduto.Text = 225 Or _
                    txtProduto.Text = 227 Or txtProduto.Text = 228 Or txtProduto.Text = 229 Or _
                        txtProduto.Text = 230 Or txtProduto.Text = 302 Or _
                    txtProduto.Text = 1152 Or txtProduto.Text = 1204 Or txtProduto.Text = 1240 Or txtProduto.Text = 300 Or txtProduto.Text = 701 Or _
                    txtProduto.Text = 155 Or txtProduto.Text = 156 Or txtProduto.Text = 8 Or _
                    txtProduto.Text = 14 Or txtProduto.Text = 722 Or (sOutrosCanais = "S")) And bLiberar Then
                    ' Produtos 227, 228, 229, 1152, 1204, 300, 701, 155, 156, 8 - Adicionado por Pelegrini - 13/11/2014 - 18353226 - LIBERA ENDOSSO 101 PARA ESTES PRODUTOS.
                    ' Produtos 14, 722 - Schoralick (ntendencia) - 09/09/2016 - 18927546 - LIBERA ENDOSSO 101 PARA ESTES PRODUTOS.
                    'Incluido Produto 1240 - 00421597-seguro-faturamento-pecuario - 02/05/2018
                        'Rafael Martins - demanda C00149269 - EMISS�O RURAL NA ABS - inclus�o dos produto 230 e 302

                    cboTipoEndosso.AddItem UCase(rs("descricao"))    'R.FOUREAUX 25/06/2012 - DEMANDA 12006056 -  UCASE
                    cboTipoEndosso.ItemData(cboTipoEndosso.NewIndex) = rs("tp_endosso_id")
                Else

                    If Trim(rs("descricao")) <> "CANCELAMENTO DE ENDOSSO" Then
                        cboTipoEndosso.AddItem UCase(rs("descricao"))    'R.FOUREAUX 25/06/2012 - DEMANDA 12006056 -  UCASE
                        cboTipoEndosso.ItemData(cboTipoEndosso.NewIndex) = rs("tp_endosso_id")
                    End If

                End If

                'Demanda 19346897 - IN�CIO
            End If
            'Demanda 19346897 - FIM

            rs.MoveNext
        Wend

    End If
    'Braune Rangel nova consultoria

    rs.Close

    Exit Sub

TrataErro:
    Call TratarErro("Form_Resize", Me.name)
    Call FinalizarAplicacao

End Sub

Private Sub ConsultaPermiteRestituicao(ByVal lPropostaId As Long)

    Dim oDadosBasicos As Object
    Dim rs As Recordset
    Dim rc As Recordset    '19/02/2016 - schoralick (nova consultoria) - 18234489 - Novos produtos para substituir Ouro Vida e outros
    Dim SQL As String    '19/02/2016 - schoralick (nova consultoria) - 18234489 - Novos produtos para substituir Ouro Vida e outros

    On Error GoTo TrataErro

    Call LimpaCampo(txtPermiteRestituicao)

    Set oDadosBasicos = CreateObject("SEGL0022.cls00117")

    Set rs = oDadosBasicos.ConsultarSubramoTpEndosso(gsSIGLASISTEMA, _
                                                     App.Title, _
                                                     App.FileDescription, _
                                                     glAmbiente_id, _
                                                     lPropostaId, _
                                                     "", cboTipoEndosso.ItemData(cboTipoEndosso.ListIndex))

    Set oDadosBasicos = Nothing

    If Not rs.EOF Then
        txtPermiteRestituicao = rs("permite_restituicao")
        oDadosModular.iTpEndossoId = rs("tp_endosso_id")
        oDadosModular.TipoEndosso = cboTipoEndosso.ItemData(cboTipoEndosso.ListIndex)
    End If

    rs.Close

    '19/02/2016 - schoralick (nova consultoria) - 18234489 - Novos produtos para substituir Ouro Vida e outros - INICIO
    If cboTipoEndosso.ItemData(cboTipoEndosso.ListIndex) = TP_ENDOSSO_CANCELAMENTO_APOLICE_SEGURADORA Then
        Select Case oDadosEndosso.produtoId
        Case 1235, 1236, 1237
            SQL = ""
            SQL = SQL & " SELECT permite_restituicao= 'S' " & vbNewLine
            SQL = SQL & "   FROM seguros_db.dbo.subramo_tp_endosso_tb s WITH(NOLOCK) " & vbNewLine
            SQL = SQL & "        INNER JOIN seguros_db.dbo.proposta_tb p WITH(NOLOCK) " & vbNewLine
            SQL = SQL & "                ON p.ramo_id = s.ramo_id " & vbNewLine
            SQL = SQL & "               AND p.subramo_id = s.subramo_id " & vbNewLine
            SQL = SQL & "        INNER JOIN seguros_db.dbo.proposta_fechada_tb pa " & vbNewLine
            SQL = SQL & "                ON pa.proposta_id = p.proposta_id " & vbNewLine
            SQL = SQL & "  WHERE p.proposta_id = " & lPropostaId & vbNewLine
            SQL = SQL & "    AND s.tp_endosso_id = 64" & vbNewLine
            SQL = SQL & "    AND p.produto_id IN (1235,1236,1237) " & vbNewLine

            If rdocn Is Nothing Then
                Call Conexao
            Else
                If rdocn.State = 0 Then Call Conexao
            End If

            Set rc = rdocn.Execute(SQL)

            If Not rc.EOF Then
                txtPermiteRestituicao = rc!permite_restituicao
            Else
                txtPermiteRestituicao = "N"
            End If

            rc.Close
        End Select
    End If
    'FIM - 18234489

    Exit Sub

TrataErro:
    Call TratarErro("ConsultaPermiteRestituicao", Me.name)
    Call FinalizarAplicacao

End Sub

Private Sub ConsultarEndosso(ByVal lPropostaId As Long)

    Dim OEndosso As Object
    Dim rs As Recordset

    Dim lEndossoId As Long

    On Error GoTo TrataErro

    Set OEndosso = CreateObject("SEGL0026.cls00124")

    lEndossoId = OEndosso.ObterProximoEndosso(gsSIGLASISTEMA, _
                                              App.Title, _
                                              App.FileDescription, _
                                              glAmbiente_id, _
                                              lPropostaId)

    Set OEndosso = Nothing

    txtNumEndosso.Text = lEndossoId

    Exit Sub

TrataErro:
    Call TratarErro("ConsultarEndosso", Me.name)
    Call FinalizarAplicacao

End Sub
'rsilva 22/07/2009
Private Sub ConsultarEndossoModular(ByVal lPropostaId As Long)

    Dim OEndosso As Object
    Dim rs As Recordset

    Dim lEndossoId As Long

    On Error GoTo TrataErro

    Set OEndosso = CreateObject("SEGL0026.cls00124")

    lEndossoId = OEndosso.ObterProximoEndosso(gsSIGLASISTEMA, _
                                              App.Title, _
                                              App.FileDescription, _
                                              glAmbiente_id, _
                                              lPropostaId)

    Set OEndosso = Nothing

    'txtNumEndosso.Text = lEndossoId
    oDadosModular.EndossoId = lEndossoId

    Exit Sub

TrataErro:
    Call TratarErro("ConsultarEndosso", Me.name)
    Call FinalizarAplicacao

End Sub
Private Sub sair()

    Set oDadosEndosso = Nothing
    Set rdocn = Nothing    '20-12-2015 - Schoralick (nova consultoria) - 18690344 - Melhorias Processo Reagendamento e Reativa��o BESC

    frmSelecao.Show
    Me.Hide
    Call Unload(Me)

End Sub
Private Sub ConsultaPropostaModular(ByVal lPropostaId As Long)

    Dim OPropostaModular As Object

    Dim rs As Recordset

    Dim lPropostaModular As Long

    On Error GoTo TrataErro

    Set OPropostaModular = CreateObject("SEGL0026.cls00124")

    lPropostaModular = OPropostaModular.ObterPropostaModular(gsSIGLASISTEMA, _
                                                             App.Title, _
                                                             App.FileDescription, _
                                                             glAmbiente_id, _
                                                             lPropostaId)

    Set OPropostaModular = Nothing

    If lPropostaModular = 0 Then
        txtProposta(0).Text = ""
        txtProposta(1).Text = ""
        bModular = False
    Else
        txtProposta(0).Text = lPropostaId
        txtProposta(1).Text = lPropostaModular

        bModular = True

        Call ConsultarPropostaModular(lPropostaModular)
        Call ConsultarPercIOFModular(Data_Sistema, oDadosModular.SubRamoId)
        Call ConsultarEndossoModular(lPropostaModular)

    End If


    Exit Sub

TrataErro:
    Call TratarErro("ConsultarEndosso", Me.name)
    Call FinalizarAplicacao

End Sub

Private Sub CarregarDadosRestituicao()

    Dim lPropostaId As Long

    On Error GoTo TrataErro

    lPropostaId = oDadosEndosso.PropostaId

    Call ConsultarCorretagem(lPropostaId)
    Call ConsultarEstipulante(lPropostaId)

    optCobAdi.Enabled = False

    If oDadosEndosso.TipoEndosso = TP_ENDOSSO_RESTITUICAO Then
        optCobRes.Value = True
        optCobRes.Enabled = True
        optCobSem.Enabled = False
    End If

    If oDadosEndosso.TipoEndosso = TP_ENDOSSO_CANCELAMENTO_APOLICE_SEGURADORA Or oDadosEndosso.TipoEndosso = TP_ENDOSSO_CANCELAMENTO_APOLICE Then

        If UCase(txtPermiteRestituicao) = "S" Then  'Rmarins - 17/10/2006
            optCobSem.Value = True
            optCobRes.Enabled = True
            optCobSem.Enabled = True
        Else
            optCobSem.Value = True
            optCobRes.Enabled = False
            optCobSem.Enabled = True
        End If

        'N�o gerar restitui��o para produtos outros canais 11/11/2019

        If (vOutrosCanais = "S") Then
            optCobSem.Value = True
            optCobRes.Enabled = False
            optCobSem.Enabled = True
        End If


    End If

    Dim OPropostaModular As Object
    Set OPropostaModular = CreateObject("SEGL0026.cls00124")

    lblTipoProposta(0).AutoSize = True
    lblTipoProposta(1).AutoSize = True
    lblTipoProposta(2).AutoSize = True

    lblTipoProposta(0).Caption = ""
    lblTipoProposta(1).Caption = ""
    lblTipoProposta(2).Caption = ""


    Dim Tipo As String

    If txtProduto.Text <> 1168 Then

        Tipo = OPropostaModular.IdentificaValorRestituicao(gsSIGLASISTEMA, _
                                                           App.Title, _
                                                           App.FileDescription, _
                                                           glAmbiente_id, _
                                                           oDadosEndosso.PropostaId)
        lblTipoProposta(0).Caption = "Tipo: " & Tipo

        If bModular = True Then
            Tipo = OPropostaModular.IdentificaValorRestituicao(gsSIGLASISTEMA, _
                                                               App.Title, _
                                                               App.FileDescription, _
                                                               glAmbiente_id, _
                                                               oDadosModular.PropostaId)
            lblTipoProposta(1).Caption = "Tipo: " & Tipo
        End If

    Else

        lblTipoProposta(0).Caption = "Tipo: DFI"
        lblTipoProposta(1).Caption = "Tipo: MIP"

    End If


    stbEndosso.TabVisible(1) = True
    stbEndosso.Tab = 1

    If txtProduto.Text = 1217 Or (txtProduto.Text = 1225 And Not oDadosEndosso.TipoEndosso = 80) Then    'Zoro.Gomes - confitec - 27/02/2014 - N�o gera restirui��o para o produto da Amparo
        Frame5(9).Enabled = False
        optCobSem.Value = True
    End If

    mskValPremioTarifaRes.mask = "9,2"
    mskAdicFracionamentoRes.mask = "9,2"
    mskCustoCertificadoRes.mask = "9,2"
    mskValIOFRes.mask = "9,2"
    mskDescontoRes.mask = "9,2"
    mskPercCorretagemCob.mask = "9,2"
    mskPercProLabore.mask = "9,2"
    mskCambio.mask = "3,6"

    'INICIO - CIBELE - INC000004360630 - 15/07/2014
    'COMENTADO PARA N�O LIMPAR OS CAMPOS ABAIXO.

    'mskValPremioTarifaRes.Text = "0,00"
    'mskAdicFracionamentoRes.Text = "0,00"
    'mskCustoCertificadoRes.Text = "0,00"
    'mskValIOFRes.Text = "0,00"
    'mskDescontoRes.Text = "0,00"
    'mskPercCorretagemCob.Text = "0,00"
    'mskPercProLabore.Text = "0,00"
    'mskCambio.Text = "0,000000"

    'FIM - CIBELE - INC000004360630 - 15/07/2014
    'N�O LIMPAR OS CAMPOS POIS OS CALCULOS J� FORAM PROCESSADOS.


    'RSilva - 04/11/2009
    'RSilva - 06/01/2010 - Inclusao do produto 1168 - Seguro Habitacional
    If bModular = True Or txtProduto.Text = 1168 Then
        mskValPremioTarifaResCancelamento(5).mask = "9,2"  'Premio Tarifa
        mskValPremioTarifaResCancelamento(6).mask = "9,2"  'Adicional Fracionamento
        mskValPremioTarifaResCancelamento(7).mask = "9,2"  'Custo Certificado
        mskValPremioTarifaResCancelamento(8).mask = "9,2"  'Valor IOF
        mskValPremioTarifaResCancelamento(9).mask = "9,2"  'Desconto

        mskValPremioTarifaResCancelamento(5).Text = "0,00"
        mskValPremioTarifaResCancelamento(6).Text = "0,00"  'Adicional Fracionamento
        mskValPremioTarifaResCancelamento(7).Text = "0,00"  'Custo Certificado
        mskValPremioTarifaResCancelamento(8).Text = "0,00"  'Valor IOF
        mskValPremioTarifaResCancelamento(9).Text = "0,00"  'Desconto
        lblvalcorretagemprolabore(2).Caption = "0,00"  'PercCorretagem
        lblvalcorretagemprolabore(3).Caption = "0,00"  'PercProLabore

    End If

    'INICIO - CIBELE - INC000004360630 - 15/07/2014
    'COMENTADO PARA N�O LIMPAR OS CAMPOS ABAIXO.

    'lblValRestituicaoRes.Caption = "0,00"
    'lblValCorretagem.Caption = "0,00"
    'lblValProLabore.Caption = "0,00"
    'lblValRemunercaoServico.Caption = "0,00"

    'FIM - CIBELE - INC000004360630 - 15/07/2014
    'COMENTADO PARA N�O LIMPAR OS CAMPOS ABAIXO.

    ''' BNEVES EM 29/02/2008   INSERIDO CRITICAR TP_ENDOSSO_MOVIMENTACAO_PREMIO
    If oDadosEndosso.TipoEndosso = TP_ENDOSSO_IMPORTANCIA_SEGURADA Or oDadosEndosso.TipoEndosso = TP_ENDOSSO_MOVIMENTACAO_PREMIO Then
        optCobRes.Value = True
        optCobRes.Enabled = True
        optCobAdi.Enabled = True
        mskValPremioTarifaRes.Text = Format(cValFinanceiro * -1, "#0.00")
    End If

    If bPossuiSubvencao = True Then
        mskValEstadual.mask = "9,2"
        'mskValEstadual.Text = "0,00"
        lblValFederal.Caption = "0,00"
    End If

    Call MontarComboMoeda

    Exit Sub

TrataErro:
    Call TratarErro("CarregarDadosRestituicao", Me.name)
    Call FinalizarAplicacao

End Sub






Sub CarregarDadosGenericos()

    stbEndosso.TabVisible(1) = True
    stbEndosso.Tab = 1

    mskValPremioTarifaRes.mask = "9,2"
    mskAdicFracionamentoRes.mask = "9,2"
    mskCustoCertificadoRes.mask = "9,2"
    mskValIOFRes.mask = "9,2"
    mskDescontoRes.mask = "9,2"
    mskPercCorretagemCob.mask = "9,2"
    mskPercProLabore.mask = "9,2"
    mskCambio.mask = "3,6"

    mskValPremioTarifaRes.Text = "0,00"
    mskAdicFracionamentoRes.Text = "0,00"
    mskCustoCertificadoRes.Text = "0,00"
    mskValIOFRes.Text = "0,00"
    mskDescontoRes.Text = "0,00"
    mskPercCorretagemCob.Text = "0,00"
    mskPercProLabore.Text = "0,00"
    mskCambio.Text = "0,000000"

    lblValRestituicaoRes.Caption = "0,00"
    lblValCorretagem.Caption = "0,00"
    lblValProLabore.Caption = "0,00"
    lblValRemunercaoServico.Caption = "0,00"

    Call MontarComboMoeda

End Sub


Private Sub AplicarProposta()

    Dim oEndossoReativacao As Object    '20-12-2015 - Schoralick (nova consultoria) - 18690344 - Melhorias Processo Reagendamento e Reativa��o BESC
    Dim iProduto As Integer
    Dim iTipoEndosso As Integer
    Dim bEnabled As Boolean    '20-12-2015 - Schoralick (nova consultoria) - 18690344 - Melhorias Processo Reagendamento e Reativa��o BESC
    Dim bAltera_Tipo_Envio_Documento 'Sergio Pires - Nova Tend�ncia - CCM Registro de Etapas no SGBR

    On Error GoTo TrataErro

    ' Verificando se o tipo de endosso foi selecionado '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

    If cboTipoEndosso.ListIndex = -1 Then
        Call MsgBox("Tipo de endosso n�o selecionado.", vbCritical)
        cboTipoEndosso.SetFocus
        Exit Sub
    End If

    If cboTipoEndosso.ItemData(cboTipoEndosso.ListIndex) = TP_ENDOSSO_RESTITUICAO Then
        If Trim(txtFimVigencia) <> "" And (Format(txtFimVigencia, "yyyymmdd") < Format(Data_Sistema, "yyyymmdd")) Then
            If MsgBox("A proposta j� est� finalizada, deseja continuar??", vbOKCancel, "Critica") = vbCancel Then
                Exit Sub
            End If
        End If
    End If

    ' Verificando se o tipo de endosso foi implementado '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    'Alexandre Debouch - Confitec - Demanda EdsContestacao
    Select Case cboTipoEndosso.ItemData(cboTipoEndosso.ListIndex)
    Case TP_ENDOSSO_RESTITUICAO
    Case TP_ENDOSSO_CANCELAMENTO_APOLICE
    Case TP_ENDOSSO_CANCELAMENTO_APOLICE_SEGURADORA
    Case TP_ENDOSSO_CANCELAMENTO_ENDOSSO
    Case TP_ENDOSSO_CADASTRAL
    Case TP_ENDOSSO_COBRANCA
    Case TP_ENDOSSO_ESTIPULANTE
    Case TP_ENDOSSO_CORRETAGEM
    Case TP_ENDOSSO_BENEFICIARIO
    Case TP_ENDOSSO_ALTERACAO_ENDERECO
    Case TP_ENDOSSO_LIMITE_EMISSAO
    Case TP_ENDOSSO_REATIVACAO_FATURA
    Case TP_ENDOSSO_IMPORTANCIA_SEGURADA
    Case TP_ENDOSSO_CANCELAMENTO_INADIMP_1_PARCELA
    Case TP_ENDOSSO_CANCELAMENTO_INADIMP_DEMAIS_PARCELAS
    Case TP_ENDOSSO_MOVIMENTACAO_PREMIO
    Case TP_ENDOSSO_REDUCAO_IS
    Case TP_REINTEGRACAO_IS
    Case TP_ENDOSSO_ADEQUACAO_AREA
    Case TP_ENDOSSO_ADEQUACAO_PRODUTIVIDADE
    Case TP_RESTITUICAO_SUBSIDIO
    Case TP_ENDOSSO_REATIVACAO_PROPOSTA
    Case TP_ALTERACAO_FORMA_PAGAMENTO
    Case TP_ENDOSSO_ALTERACAO_PRECO_BASE
    Case TP_ENDOSSO_CANCELAMENTO_SINISTRO
    Case TP_ENDOSSO_CONTESTACAO
    Case TP_ENDOSSO_ALTERACAO_TIPO_ENVIO_DOCUMENTO 'Sergio Pires - Nova Tend�ncia - CCM Registro de Etapas no SGBR
    Case Else
        Call MsgBox("Tipo de Endosso n�o implementado.", vbInformation)
        cboTipoEndosso.SetFocus
        Exit Sub
    End Select

    '    If cboTipoEndosso.ItemData(cboTipoEndosso.ListIndex) <> TP_ENDOSSO_RESTITUICAO And _
         '       cboTipoEndosso.ItemData(cboTipoEndosso.ListIndex) <> TP_ENDOSSO_CANCELAMENTO_APOLICE And _
         '       cboTipoEndosso.ItemData(cboTipoEndosso.ListIndex) <> TP_ENDOSSO_CANCELAMENTO_APOLICE_SEGURADORA And _
         '       cboTipoEndosso.ItemData(cboTipoEndosso.ListIndex) <> TP_ENDOSSO_CANCELAMENTO_ENDOSSO And _
         '       cboTipoEndosso.ItemData(cboTipoEndosso.ListIndex) <> TP_ENDOSSO_CADASTRAL And _
         '       cboTipoEndosso.ItemData(cboTipoEndosso.ListIndex) <> TP_ENDOSSO_COBRANCA And _
         '       cboTipoEndosso.ItemData(cboTipoEndosso.ListIndex) <> TP_ENDOSSO_ESTIPULANTE And _
         '       cboTipoEndosso.ItemData(cboTipoEndosso.ListIndex) <> TP_ENDOSSO_CORRETAGEM And _
         '       cboTipoEndosso.ItemData(cboTipoEndosso.ListIndex) <> TP_ENDOSSO_BENEFICIARIO And _
         '       cboTipoEndosso.ItemData(cboTipoEndosso.ListIndex) <> TP_ENDOSSO_ALTERACAO_ENDERECO And _
         '       cboTipoEndosso.ItemData(cboTipoEndosso.ListIndex) <> TP_ENDOSSO_LIMITE_EMISSAO And _
         '       cboTipoEndosso.ItemData(cboTipoEndosso.ListIndex) <> TP_ENDOSSO_REATIVACAO_FATURA And _
         '       cboTipoEndosso.ItemData(cboTipoEndosso.ListIndex) <> TP_ENDOSSO_IMPORTANCIA_SEGURADA And _
         '       cboTipoEndosso.ItemData(cboTipoEndosso.ListIndex) <> TP_ENDOSSO_CANCELAMENTO_INADIMP_1_PARCELA And _
         '       cboTipoEndosso.ItemData(cboTipoEndosso.ListIndex) <> TP_ENDOSSO_CANCELAMENTO_INADIMP_DEMAIS_PARCELAS And _
         '       cboTipoEndosso.ItemData(cboTipoEndosso.ListIndex) <> TP_ENDOSSO_MOVIMENTACAO_PREMIO And _
         '       cboTipoEndosso.ItemData(cboTipoEndosso.ListIndex) <> TP_ENDOSSO_REDUCAO_IS And _
         '       cboTipoEndosso.ItemData(cboTipoEndosso.ListIndex) <> TP_REINTEGRACAO_IS And _
         '       cboTipoEndosso.ItemData(cboTipoEndosso.ListIndex) <> TP_ENDOSSO_ADEQUACAO_AREA And _
         '       cboTipoEndosso.ItemData(cboTipoEndosso.ListIndex) <> TP_ENDOSSO_ADEQUACAO_PRODUTIVIDADE And _
         '       cboTipoEndosso.ItemData(cboTipoEndosso.ListIndex) <> TP_RESTITUICAO_SUBSIDIO And _
         '       cboTipoEndosso.ItemData(cboTipoEndosso.ListIndex) <> TP_ENDOSSO_REATIVACAO_PROPOSTA And _
         '       cboTipoEndosso.ItemData(cboTipoEndosso.ListIndex) <> TP_ALTERACAO_FORMA_PAGAMENTO And _
         '       cboTipoEndosso.ItemData(cboTipoEndosso.ListIndex) <> TP_ENDOSSO_ALTERACAO_PRECO_BASE And cboTipoEndosso.ItemData(cboTipoEndosso.ListIndex) <> TP_ENDOSSO_CANCELAMENTO_SINISTRO _
'       Then
    '        'R.FOUREAUX 25/06/2012 - DEMANDA 12006056 - COLOCOU <> TP_ALTERACAO_FORMA_PAGAMENTO _
             '         '''''' BNEVES EM 29/02/2008 COLOCOU <> TP_ENDOSSO_MOVIMENTACAO_PREMIO ACIMA
    '        '@gbarbosa - 17/06/2004 - Endosso para Registro de Limite de Emiss�o
    '
    '        Call MsgBox("Tipo de Endosso n�o implementado.", vbInformation)
    '        cboTipoEndosso.SetFocus
    '        Exit Sub
    '    End If
    'Alexandre Debouch - Confitec - Demanda EdsContestacao

    ' Verificando a data do pedido do endosso '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

    If Not VerificaData2(mskDtEndosso.Text) Then
        Call MsgBox("Data do pedido do endosso inv�lida.", vbCritical)
        mskDtEndosso.SetFocus
        Exit Sub
    Else
        If DateDiff("d", CDate(mskDtEndosso.Text), CDate(txtIniVigencia.Text)) > 0 Then
            Call MsgBox("A data do pedido do endosso n�o pode ser menor que a data de in�cio de vig�ncia.", vbCritical)
            mskDtEndosso.SetFocus
            Exit Sub
        End If
    End If

    ' Verificando se o texto do endosso foi preenchido ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

    If Trim(txtDescEndosso.Text) = "" And _
       cboTipoEndosso.ItemData(cboTipoEndosso.ListIndex) <> TP_ENDOSSO_ALTERACAO_TIPO_ENVIO_DOCUMENTO Then 'Sergio Pires - Nova Tend�ncia - CCM Registro de Etapas no SGBR
        Call MsgBox("Texto do endosso n�o preenchido.", vbCritical)
        txtDescEndosso.Enabled = True
        txtDescEndosso.SetFocus
        Exit Sub
    End If

    'lrocha 11/12/2007 - Verificando exist�ncia do endosso 270 para produto 1152 e tipo endosso cancelamento apolice seguradora
    iTipoEndosso = cboTipoEndosso.ItemData(cboTipoEndosso.ListIndex)
    iProduto = frmSelecao.grdSelecao.TextMatrix(frmSelecao.grdSelecao.Row, 4)
    '20-12-2015 - Schoralick (nova consultoria) - 18690344 - Melhorias Processo Reagendamento e Reativa��o BESC - ini
    vlProdutoId = iProduto

    grdParcelasReativacao.ColWidth(3) = 0    ' 20/12/2015 - schoralick (nova consultoria) -
    grdParcelasReativacao.ColWidth(4) = 0    ' 20/12/2015 - schoralick (nova consultoria) -



    'fernanda.souza - Confitec - Novo Produto Outros Canais
    vOutrosCanais = VerificaOutrosCanais(vlProdutoId)

    Dim bProdutoBESC As Boolean
    bProdutoBESC = ValidaBESC(vlProdutoId)

    If bProdutoBESC Or (vOutrosCanais = "S") Then

        cmdBtnReativacaoProposta(2).Enabled = True
        cmdBtnReativacaoProposta(3).Enabled = True

        chkImpedirAgendamento.Visible = True
        chkImpedirAgendamento.Value = 0

        Set oEndossoReativacao = CreateObject("SEGL0140.cls00172")
        Dim lImpedirAgendamentoBesc As Boolean

        lImpedirAgendamentoBesc = oEndossoReativacao.VerificaImpedirAgendamentoBesc(gsSIGLASISTEMA, _
                                                                                    App.Title, _
                                                                                    App.FileDescription, _
                                                                                    glAmbiente_id, _
                                                                                    oDadosEndosso.PropostaId)

        bEnabled = Not (lImpedirAgendamentoBesc)

        mskDtReativacao.Enabled = bEnabled
        grdParcelasReativacao.Enabled = bEnabled
        mskValParcelaReativacao.Enabled = bEnabled
        mskDtParcelaReativacao.Enabled = bEnabled
        CmdIncluirParcelaReativacao.Enabled = bEnabled
        cmdAlterarParcelaReativacao.Enabled = bEnabled
        cmdExcluirParcelaReativacao.Enabled = bEnabled
        cmdBtnReativacaoProposta(2).Enabled = bEnabled
        cmdBtnReativacaoProposta(3).Enabled = bEnabled

    Else

        cmdBtnReativacaoProposta(2).Enabled = False
        cmdBtnReativacaoProposta(3).Enabled = False
        chkImpedirAgendamento.Visible = False

    End If
    'fernanda.souza - Confitec - Novo Produto Outros Canais


    '    Select Case vlProdutoId
    '        Case 200, 201, 202, 203, 204, 205, 208, 209, 210, 211, 213, 216, 217, 218, 219, 221, 222, 223, 224, 225, 226, 129
    '            cmdBtnReativacaoProposta(2).Enabled = True
    '            cmdBtnReativacaoProposta(3).Enabled = True
    '
    '            chkImpedirAgendamento.Visible = True
    '            chkImpedirAgendamento.Value = 0
    '
    '            Set oEndossoReativacao = CreateObject("SEGL0140.cls00172")
    '            Dim lImpedirAgendamentoBesc As Boolean
    '
    '            lImpedirAgendamentoBesc = oEndossoReativacao.VerificaImpedirAgendamentoBesc(gsSIGLASISTEMA, _
                 '                                                                                        App.Title, _
                 '                                                                                        App.FileDescription, _
                 '                                                                                        glAmbiente_id, _
                 '                                                                                        oDadosEndosso.PropostaId)
    '
    '
    '
    '            bEnabled = Not (lImpedirAgendamentoBesc)
    '
    '            mskDtReativacao.Enabled = bEnabled
    '            grdParcelasReativacao.Enabled = bEnabled
    '            mskValParcelaReativacao.Enabled = bEnabled
    '            mskDtParcelaReativacao.Enabled = bEnabled
    '            CmdIncluirParcelaReativacao.Enabled = bEnabled
    '            cmdAlterarParcelaReativacao.Enabled = bEnabled
    '            cmdExcluirParcelaReativacao.Enabled = bEnabled
    '            cmdBtnReativacaoProposta(2).Enabled = bEnabled
    '            cmdBtnReativacaoProposta(3).Enabled = bEnabled
    '
    '        Case Else
    '            cmdBtnReativacaoProposta(2).Enabled = False
    '            cmdBtnReativacaoProposta(3).Enabled = False
    '            chkImpedirAgendamento.Visible = False
    '    End Select

    '20-12-2015 - Schoralick (nova consultoria) - 18690344 - Melhorias Processo Reagendamento e Reativa��o BESC - fim

    'Brauner Nova consultoria

    uTipoEndosso = iTipoEndosso
    uProduto = iProduto

    If iTipoEndosso = TP_ENDOSSO_CANCELAMENTO_APOLICE_SEGURADORA And iProduto = 1152 Then
        If ValidarEndossoProduto = False Then
            Call MsgBox("N�o ser� poss�vel efetuar a devolu��o do pr�mio devido � exist�ncia de endosso de bonifica��o para a proposta.", vbCritical)
            Exit Sub
        End If
    End If
    ''''''''''''''''''''''''''''''''

    'Brauner Rangel nova consultoia


    If iTipoEndosso = 101 Or iTipoEndosso = 200 Then
        If iProduto = 200 Or _
           iProduto = 201 Or _
           iProduto = 202 Or _
           iProduto = 203 Or _
           iProduto = 204 Or _
           iProduto = 205 Or _
           iProduto = 208 Or _
           iProduto = 209 Or _
           iProduto = 210 Or _
           iProduto = 211 Or _
           iProduto = 212 Or _
           iProduto = 213 Or _
           iProduto = 216 Or _
           iProduto = 217 Or _
           iProduto = 218 Or _
           iProduto = 219 Or _
           iProduto = 220 Or _
           iProduto = 221 Or _
           iProduto = 224 Or _
           iProduto = 225 _
           Then
            frmReativacaoReagendamento.EndossoId = iTipoEndosso
            frmReativacaoReagendamento.PropostaId = oDadosEndosso.PropostaId
            frmReativacaoReagendamento.produtoId = txtProduto.Text    '10/12/2015 - Schoralick (nova consultoria) - 18690344 - Melhorias Processo Reagendamento e Reativa��o BESC

            Dim lEndossoCancelamentoId As Long
            Dim endossocanc As Integer
            Dim ultimaParcela As Integer

            Set oEndossoReativacao = CreateObject("SEGL0140.cls00172")

            lEndossoCancelamentoId = oEndossoReativacao.ObterEndossoCancelamento(gsSIGLASISTEMA, _
                                                                                 App.Title, _
                                                                                 App.FileDescription, _
                                                                                 glAmbiente_id, _
                                                                                 oDadosEndosso.PropostaId)
            endossocanc = oEndossoReativacao.ObterUltimoEndosso(gsSIGLASISTEMA, _
                                                                App.Title, _
                                                                App.FileDescription, _
                                                                glAmbiente_id, _
                                                                oDadosEndosso.PropostaId)

            ultimaParcela = oEndossoReativacao.ObterUltimaParcela(gsSIGLASISTEMA, _
                                                                  glAmbiente_id, _
                                                                  App.Title, _
                                                                  App.FileDescription, _
                                                                  oDadosEndosso.PropostaId)



            If iTipoEndosso = 101 Then
                If lEndossoCancelamentoId = 0 Then
                    Set oEndossoReativacao = Nothing
                    MsgBox "Proposta n�o esta cancelada!", vbCritical

                    Exit Sub
                End If
                If endossocanc <> 90 Then

                    MsgBox " Endosso nao permitido para esta proposta!", vbCritical
                    Exit Sub
                End If

            End If
            If iTipoEndosso = 200 Then
                If lEndossoCancelamentoId <> 0 Then
                    Set oEndossoReativacao = Nothing
                    MsgBox "A proposta est� cancelada!", vbCritical
                    Exit Sub
                End If
                If ultimaParcela = 0 Then
                    MsgBox "A proposta n�o possui parcelas!", vbCritical
                    Exit Sub
                End If
            End If
        End If

    End If
    'fim ____________________

    ' Desabilitando a tela de endosso '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

    'JFILHO - CONFITEC - FLOW 1440998 - Corrigindo erro na implementa��o da regra de neg�cio, que n�o permitia que endossos de resitui��o fossem gerados em propostas canceladas
    'bmoraes 24.03.2010
    If iTipoEndosso <> TP_ENDOSSO_RESTITUICAO And _
       iTipoEndosso <> TP_ENDOSSO_REATIVACAO_PROPOSTA And _
       iTipoEndosso <> TP_RESTITUICAO_SUBSIDIO And _
       iTipoEndosso <> TP_ENDOSSO_ADEQUACAO_AREA And _
       iTipoEndosso <> 101 And _
       iTipoEndosso <> TP_ENDOSSO_ADEQUACAO_PRODUTIVIDADE And _
       iTipoEndosso <> 200 And _
       iTipoEndosso <> TP_ENDOSSO_CONTESTACAO And _
       iTipoEndosso <> TP_ENDOSSO_ALTERACAO_TIPO_ENVIO_DOCUMENTO Then 'Sergio Pires - Nova Tend�ncia - CCM Registro de Etapas no SGBR

        'bmoraes - FIM


        'Raimundo Belas da Silva Junior
        'GPTI - 30/07/2009 - Flow 1086754
        'Verifica se o endosso possui restitui��o ou est� com cancelamento recusado
        'antes de efetuar um novo cancelamento de proposta
        Dim asSQL As String
        Dim RX As Recordset

        If rdocn Is Nothing Then
            Call Conexao
        Else
            If rdocn.State = 0 Then Call Conexao
        End If

        asSQL = "EXEC SEGS7721_SPS " & frmSelecao.grdSelecao.TextMatrix(frmSelecao.grdSelecao.Row, 3)

        Set RX = rdocn.Execute(asSQL)

        If RX(0) = "FALSE" Then
            Call MsgBox("Cancelamento n�o permitido. J� existe o endosso de cancelamento para esta proposta.", vbCritical)

            Set RX = Nothing
            Exit Sub
        End If

        rdocn.Close
        Set rdocn = Nothing


        'Modifica��o realizada em 17/02/2010 - Confitec (Rodrigo Souza)
        'Descri��o: Fecha conex�o secund�ria antes que ela seja aberta novamente.
        If bUsarConexaoABS And rdocn_Seg2.State <> 0 Then
            'rdocn_Seg2.Close
            Set rdocn_Seg2 = Nothing
        End If
        '-------------------------------------------------------------------------'

        'Raimundo - Fim
    End If


    stbEndosso.TabEnabled(0) = False

    ' Selecionando o tipo de endosso ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

    oDadosEndosso.TipoEndosso = cboTipoEndosso.ItemData(cboTipoEndosso.ListIndex)

    bAltera_Tipo_Envio_Documento = False 'Sergio Pires - Nova Tend�ncia - CCM Registro de Etapas no SGBR

    Select Case oDadosEndosso.TipoEndosso

    Case TP_ENDOSSO_RESTITUICAO
        Call CarregarDadosRestituicao
        'bmoraes 11.02.2010 - Selecionar Valores Restitui��o
        Call ObterValoresRestituicao

        'cristovao.rodrigues 25/04/2013
        If Not bObteveSaldoRestituir Then
            stbEndosso.Tab = 0
            stbEndosso.TabVisible(1) = False
            Exit Sub
        End If

        Call mskValPremioTarifaRes_LostFocus
        'bmoraes - fim

        'G&P - Flow - 219229
    Case TP_ENDOSSO_CANCELAMENTO_APOLICE, TP_ENDOSSO_CANCELAMENTO_APOLICE_SEGURADORA, _
         TP_ENDOSSO_CANCELAMENTO_INADIMP_1_PARCELA, TP_ENDOSSO_CANCELAMENTO_INADIMP_DEMAIS_PARCELAS
        Call CarregarDadosCancelamentoApolice
        '20-12-2015 - Schoralick (nova consultoria) - 18690344 - Melhorias Processo Reagendamento e Reativa��o BESC - inicio
    Case TP_ENDOSSO_CANCELAMENTO_ENDOSSO
        Select Case txtProduto.Text
        Case 200, 201, 202, 203, 204, 205, 208, 209, 210, 211, 213, 216, 217, 218, 219, 221, 222, 223, 224, 225, 226
            MsgBox ("Endosso n�o permitido para esta proposta.")
            Exit Sub
        Case Else
            Call CarregarDadosCancelamentoEndosso
        End Select
        '20-12-2015 - Schoralick (nova consultoria) - 18690344 - Melhorias Processo Reagendamento e Reativa��o BESC - fim
    Case TP_ENDOSSO_CADASTRAL
        Call CarregarDadosCadastrais
        ' Brauner Rangel nova consultoria
        'alterar para descri��o do endosso

    Case 200
        frmReativacaoReagendamento.Show

        'fim --------

    Case TP_ENDOSSO_COBRANCA
        Call CarregarDadosCobranca

    Case TP_ENDOSSO_ESTIPULANTE
        Call CarregarDadosEndossoEstipulante

    Case TP_ENDOSSO_CORRETAGEM
        Call CarregarDadosEndossoCorretagem

    Case TP_ENDOSSO_BENEFICIARIO
        Call CarregarDadosBeneficiario

    Case TP_ENDOSSO_ALTERACAO_ENDERECO
        Call CarregarDadosEndereco

    Case TP_ENDOSSO_LIMITE_EMISSAO
        Call CarregarDadosLimiteEmissao

    Case TP_ENDOSSO_REATIVACAO_FATURA
        Call CarregarDadosReativacaoFatura

    Case TP_ENDOSSO_IMPORTANCIA_SEGURADA
        Call CarregarDadosImportanciaSegurada(oDadosEndosso.PropostaId, _
                                              oDadosEndosso.produtoId, _
                                              oDadosEndosso.RamoId)

    Case TP_ENDOSSO_REATIVACAO_PROPOSTA
        Call CarregarDadosReativacaoProposta

    Case TP_ENDOSSO_REDUCAO_IS
        Call Carregar_Dados_Reducao_IS(CLng(txtPropostaid(0).Text))

    Case TP_REINTEGRACAO_IS
        Call Carregar_Dados_Reintegracao_IS(CLng(txtPropostaid(0).Text))

    Case TP_ENDOSSO_ADEQUACAO_AREA
        Call Carregar_Dados_Adequacao_Area(CLng(txtPropostaid(0).Text), 15, 0)

    Case TP_ENDOSSO_ADEQUACAO_PRODUTIVIDADE
        Call Carregar_Dados_Adequacao_Produtividade(CLng(txtPropostaid(0).Text), 16, 2)

    Case TP_RESTITUICAO_SUBSIDIO
        Call Carregar_Dados_Restituicao_Subsidio(CLng(txtPropostaid(0).Text))

        'ALTERACAO PARA FORMA DE PAGAMENTO SOMENTE PARA PRODUTOS BESK - 'R.FOUREAUX 25/06/2012 - DEMANDA 12006056
    Case TP_ALTERACAO_FORMA_PAGAMENTO
        If oDadosEndosso.produtoId >= 200 And oDadosEndosso.produtoId <= 225 Then
            Call CarregarDadosCadastrais
        Else
            Call MsgBox("Tipo de Endosso n�o implementado.", vbCritical)
        End If
    Case TP_ENDOSSO_ALTERACAO_PRECO_BASE
        frmPrecoBase.Show
        frmEndosso.Hide
    Case TP_ENDOSSO_CANCELAMENTO_SINISTRO
        'Apenas produtos ALS e BESC individuais
        If oDadosEndosso.produtoId > 1000 _
           Or (oDadosEndosso.produtoId >= 200 And oDadosEndosso.produtoId <= 205) _
           Or (oDadosEndosso.produtoId >= 208 And oDadosEndosso.produtoId <= 213) _
           Or (oDadosEndosso.produtoId >= 216 And oDadosEndosso.produtoId <= 221) _
           Or (oDadosEndosso.produtoId >= 224 And oDadosEndosso.produtoId <= 225) Then
            Call CarregarDadosCancelamentoPorSinistro
        Else
            Call MsgBox("Tipo de Endosso n�o implementado para este produto.", vbCritical)
        End If

        'Alexandre Debouch - Confitec - EdsContestacao
    Case TP_ENDOSSO_CONTESTACAO
        Call CarregarDadosRestituicaoEdsContestacao
        
        'Adicionei esse metodo
        Call CalcularValorCorretagemContestacao
        'Adicionei esse metodo
        
        mskValPremioTarifaRes.Enabled = False
        mskCustoCertificadoRes.Enabled = False
        mskAdicFracionamentoRes.Enabled = False
        mskDescontoRes.Enabled = False
        mskValIOFRes.Enabled = False
        cmdAplicarRestituicao.SetFocus
        'Alexandre Debouch - Confitec - EdsContestacao
    Case TP_ENDOSSO_ALTERACAO_TIPO_ENVIO_DOCUMENTO 'Sergio Pires - Nova Tend�ncia - CCM Registro de Etapas no SGBR
         bAltera_Tipo_Envio_Documento = True
    Case Else
        Call MsgBox("Tipo de Endosso n�o implementado.", vbCritical)
    End Select


    'R.FOUREAUX 28/08/2012 FECHA CONEXAO SECUNDARIA ANTES DE ABRIR NOVAMENTE
    If bUsarConexaoABS And rdocn_Seg2.State <> 0 Then
        Set rdocn_Seg2 = Nothing
    End If


    'fernanda.souza - Confitec - Novo Produto Outros Canais
    If ValidaBESC(iProduto) Or (vOutrosCanais = "S") Then

        frmReativacaoReagendamento.TP_EVENTO_ENDOSSO = TP_EVENTO_EMISSAO_ENDOSSO
        frmReativacaoReagendamento.ApoliceId = oDadosEndosso.ApoliceId
        frmReativacaoReagendamento.RamoId = oDadosEndosso.RamoId
        frmReativacaoReagendamento.produtoId = oDadosEndosso.produtoId
        frmReativacaoReagendamento.PropostaId = oDadosEndosso.PropostaId
        frmReativacaoReagendamento.ENDOSSO_EVENTO = Val(txtNumEndosso.Text)
        frmReativacaoReagendamento.txtDescEndosso = txtDescEndosso.Text

        Exit Sub

    End If
    'fernanda.souza - Confitec - Novo Produto Outros Canais


    '    'Brauner Rangel nova consultoria
    '    If iProduto = 200 Or _
         '       iProduto = 201 Or _
         '       iProduto = 202 Or _
         '       iProduto = 203 Or _
         '       iProduto = 204 Or _
         '       iProduto = 205 Or _
         '       iProduto = 208 Or _
         '       iProduto = 209 Or _
         '       iProduto = 210 Or _
         '       iProduto = 211 Or _
         '       iProduto = 212 Or _
         '       iProduto = 213 Or _
         '       iProduto = 216 Or _
         '       iProduto = 217 Or _
         '       iProduto = 218 Or _
         '       iProduto = 219 Or _
         '       iProduto = 220 Or _
         '       iProduto = 221 Or _
         '       iProduto = 224 Or _
         '       iProduto = 225 Or _
         '       iProduto = 129 _
         '       Then
    '
    '
    '        frmReativacaoReagendamento.TP_EVENTO_ENDOSSO = TP_EVENTO_EMISSAO_ENDOSSO
    '        frmReativacaoReagendamento.ApoliceId = oDadosEndosso.ApoliceId
    '        frmReativacaoReagendamento.RamoId = oDadosEndosso.RamoId
    '        frmReativacaoReagendamento.ProdutoId = oDadosEndosso.ProdutoId
    '        frmReativacaoReagendamento.PropostaId = oDadosEndosso.PropostaId
    '        frmReativacaoReagendamento.ENDOSSO_EVENTO = Val(txtNumEndosso.Text)
    '        frmReativacaoReagendamento.txtDescEndosso = txtDescEndosso.Text
    '
    '        Exit Sub
    '    End If

 'Sergio Pires - Nova Tend�ncia - CCM Registro de Etapas no SEGBR
    If bAltera_Tipo_Envio_Documento = True Then
        If rdocn Is Nothing Then
            Call Conexao
        Else
            If rdocn.State = 0 Then Call Conexao
        End If
        Dim tsSQL As String
        Dim rs As Recordset
        tsSQL = "EXEC seguros_db.dbo.SEGS14459_SPI " & oDadosEndosso.PropostaId & ", " & _
                                                "'" & Format(mskDtEndosso, "yyyymmdd") & "', " & _
                                                "'" & Format(mskDtEmissao, "yyyymmdd") & "', " & _
                                                "'" & cUserName & "' "

        Set rs = rdocn.Execute(tsSQL)
        Set rs = Nothing
        rdocn.Close
        Set rdocn = Nothing
        Call MsgBox("Endosso de altera��o do tipo de envio documento. Aplicado com sucesso!", vbInformation)
        Call cmdSairProposta_Click
    End If


    '    'Inserindo o evento em evento_tb feito por alucas 06/10/2005
    '    If rdocn Is Nothing Then
    '        Call Conexao
    '    Else
    '        If rdocn.State = 0 Then Call Conexao
    '    End If
    '
    '    rdocn.BeginTrans
    '
    '    Call IncluirEventoEmissao(oDadosEndosso.PropostaId, _
         '                              Val(txtNumEndosso.Text), _
         '                              oDadosEndosso.ProdutoId, _
         '                              oDadosEndosso.RamoId, _
         '                              oDadosEndosso.TipoEndosso, _
         '                              App.EXEName, _
         '                              Data_Sistema, _
         '                              cUserName, _
         '                              rdocn)
    '
    '    If bModular = True And txtProduto.Text <> 1168 Then
    '        Call IncluirEventoEmissao(oDadosModular.PropostaId, _
             '                                  oDadosModular.EndossoId, _
             '                                  oDadosModular.ProdutoId, _
             '                                  oDadosModular.RamoId, _
             '                                  oDadosModular.TipoEndosso, _
             '                                  App.EXEName, _
             '                                  Data_Sistema, _
             '                                  cUserName, _
             '                                  rdocn)
    '    End If
    '
    '    'Incluir informa�es em Evento_tb feito por alucas 06/10/2005
    '    Call IncluirEvento(TP_EVENTO_EMISSAO_ENDOSSO, _
         '                       oDadosEndosso.ApoliceId, _
         '                       oDadosEndosso.RamoId, _
         '                       oDadosEndosso.ProdutoId, _
         '                       oDadosEndosso.PropostaId, _
         '                       Val(txtNumEndosso.Text), _
         '                       Format(Data_Sistema, "yyyymmdd"), _
         '                       rdocn)
    '
    '    If bModular = True And txtProduto.Text <> 1168 Then
    '        Call IncluirEvento(TP_EVENTO_EMISSAO_ENDOSSO, _
             '                           oDadosModular.ApoliceId, _
             '                           oDadosModular.RamoId, _
             '                           oDadosModular.ProdutoId, _
             '                           oDadosModular.PropostaId, _
             '                           oDadosModular.EndossoId, _
             '                           Format(Data_Sistema, "yyyymmdd"), _
             '                           rdocn)
    '    End If
    '
    '    rdocn.CommitTrans
    '    rdocn.Close
    '    Set rdocn = Nothing
    '
    '    If bUsarConexaoABS And rdocn_Seg2.State <> 0 Then
    '        Set rdocn_Seg2 = Nothing
    '    End If

    Exit Sub

TrataErro:

    Call TratarErro("AplicarProposta", Me.name)
    Call FinalizarAplicacao

End Sub


'Sub MontarColecaoCorretagem(ByRef rs As Recordset, ByVal bRestaura As Boolean)
'
'    Dim sPercCorretagem As String
'    Dim sNomeCorretor As String
'    Dim oNovoCorretor As Object
'    Dim oNovoCorretorAnt As Object
'    Dim sDecimal As String
'    Dim bConfiguracaoBrasil As Boolean
'
'    On Error GoTo Erro
'
'    Set colCorretor = Nothing
'
'    Do While Not rs.EOF
'
'        sPercCorretagem = IIf(IsNull(rs("perc_corretagem")), "0,00000", Format(Val(rs("perc_corretagem")), "0.00000"))
'        sNomeCorretor = Trim(rs("nome"))
'
'        sDecimal = LeArquivoIni("WIN.INI", "intl", "sDecimal")
'        If sDecimal = "." Then
'            bConfiguracaoBrasil = False
'        Else
'            bConfiguracaoBrasil = True
'        End If
'
'        If Trim(sPercCorretagem) <> "" And (Not bConfiguracaoBrasil) Then
'            sPercCorretagem = TrocaValorAmePorBras(sPercCorretagem)
'        End If
'
'        If Not bRestaura Then
'
'            Set oNovoCorretorAnt = CreateObject("SEGL0140.cls00149")
'
'            With oNovoCorretorAnt
'                .corretor_id = rs!corretor_id
'                .Tipo = rs!tp_pessoa
'                .Nome = sNomeCorretor
'                .percComissao = sPercCorretagem
'                .DtInicioCorretagem = CDate(rs!dt_inicio_corretagem)
'                .Sucursal = IIf(IsNull(rs!suc), Empty, rs!suc)
'                .Operacao = "N"
'                .Persistente = True
'            End With
'
'            colCorretorAnt.Add oNovoCorretorAnt, CStr(oNovoCorretorAnt.id)
'
'            Set oNovoCorretorAnt = Nothing
'
'        End If
'
'        Set oNovoCorretor = CreateObject("SEGL0140.cls00149")
'
'        With oNovoCorretor
'            .corretor_id = rs!corretor_id
'            .Tipo = rs!tp_pessoa
'            .Nome = sNomeCorretor
'            .percComissao = sPercCorretagem
'            .DtInicioCorretagem = CDate(rs!dt_inicio_corretagem)
'            .Sucursal = IIf(IsNull(rs!suc), Empty, rs!suc)
'            .Operacao = "N"
'            .Persistente = True
'        End With
'
'        colCorretor.Add oNovoCorretor
'
'        Set oNovoCorretor = Nothing
'
'        rs.MoveNext
'
'    Loop
'
'    Exit Sub
'
'Erro:
'    Call TratarErro("MontarColecaoCorretagem", Me.name)
'    Call FinalizarAplicacao
'End Sub

Sub MontarGridCorretagem(ByRef colCorret As Collection)

    Dim sLinha As String
    Dim oCorretor As Object
    Dim i As Integer

    Set oCorretor = CreateObject("SEGL0140.cls00149")

    GridCorretagem.Rows = 1

    For Each oCorretor In colCorret

        sLinha = Format(oCorretor.Corretor_ID, "000000-000") & vbTab
        sLinha = sLinha & IIf(oCorretor.Tipo = "1", "J", "F") & vbTab
        sLinha = sLinha & Format(CDbl(oCorretor.PercComissao), "0.0000") & vbTab
        sLinha = sLinha & oCorretor.Nome & vbTab
        sLinha = sLinha & oCorretor.Sucursal & vbTab

        If oCorretor.Persistente Then
            sLinha = sLinha & Format(oCorretor.DtInicioCorretagem, "dd/mm/yyyy")
        Else
            sLinha = sLinha & Format(mskDtEndosso.Text, "dd/mm/yyyy")
        End If

        GridCorretagem.AddItem sLinha
        GridCorretagem.RowData(GridCorretagem.Rows - 1) = oCorretor.Corretor_ID

        GridCorretagem.Row = GridCorretagem.Rows - 1

        Select Case oCorretor.Operacao
        Case "E"
            For i = 0 To GridCorretagem.Cols - 1
                GridCorretagem.Col = i
                GridCorretagem.CellBackColor = vbWhite    'Branco
                GridCorretagem.CellForeColor = vbRed   'Vermelho
            Next
        Case "A"
            For i = 0 To GridCorretagem.Cols - 1
                GridCorretagem.Col = i
                GridCorretagem.CellBackColor = vbWhite    'Branco
                GridCorretagem.CellForeColor = &HC000&    'Verde
            Next
        Case "I"
            For i = 0 To GridCorretagem.Cols - 1
                GridCorretagem.Col = i
                GridCorretagem.CellBackColor = vbWhite    'Branco
                GridCorretagem.CellForeColor = vbBlue  'Azul
            Next
        Case Else
            For i = 0 To GridCorretagem.Cols - 1
                GridCorretagem.Col = i
                GridCorretagem.CellBackColor = vbWhite    'Branco
                GridCorretagem.CellForeColor = vbBlack    'Preto
            Next
        End Select

    Next

    Set oCorretor = Nothing

End Sub

'Sub MontarColecaoCorretagemSubGrupo(ByRef rs As Recordset, ByVal bRestaura As Boolean)
'
'    Dim sPercCorretagem As String
'    Dim sNomeCorretor As String
'    Dim oNovoCorretor As Object
'    Dim oNovoCorretorAnt As Object
'    Dim sDecimal As String
'    Dim bConfiguracaoBrasil As Boolean
'
'    On Error GoTo Erro
'
'    While Not rs.EOF
'
'        sPercCorretagem = Format(IIf(IsNull(rs!perc_corretagem), "0,00000", Val(rs!perc_corretagem)), "0.00000")
'        sNomeCorretor = Trim(rs!Nome)
'
'        sDecimal = LeArquivoIni("WIN.INI", "intl", "sDecimal")
'        If sDecimal = "." Then
'            bConfiguracaoBrasil = False
'        Else
'            bConfiguracaoBrasil = True
'        End If
'
'        If (Trim(sPercCorretagem) <> "") And (Not bConfiguracaoBrasil) Then
'            sPercCorretagem = TrocaValorAmePorBras(sPercCorretagem)
'        End If
'
'        If Not bRestaura Then
'
'            Set oNovoCorretorAnt = CreateObject("SEGL0140.cls00149")
'
'            oNovoCorretorAnt.corretor_id = rs!corretor_id
'            oNovoCorretorAnt.Sucursal = rs!sucursal_corretor_id
'            oNovoCorretorAnt.Tipo = rs!tp_pessoa
'            oNovoCorretorAnt.Nome = sNomeCorretor
'            oNovoCorretorAnt.percComissao = sPercCorretagem
'            oNovoCorretorAnt.DtInicioCorretagem = rs!dt_inicio_corretagem
'            oNovoCorretorAnt.Operacao = "N"
'            oNovoCorretorAnt.Persistente = True
'
'            colSubGrupoAnt(CStr(rs!sub_grupo_id)).colCorretor.Add oNovoCorretorAnt, CStr(rs!corretor_id)
'
'            Set oNovoCorretorAnt = Nothing
'
'        End If
'
'        Set oNovoCorretor = CreateObject("SEGL0140.cls00149")
'
'        oNovoCorretor.corretor_id = rs!corretor_id
'        oNovoCorretor.Sucursal = rs!sucursal_corretor_id
'        oNovoCorretor.Tipo = rs!tp_pessoa
'        oNovoCorretor.Nome = sNomeCorretor
'        oNovoCorretor.percComissao = sPercCorretagem
'        oNovoCorretor.DtInicioCorretagem = rs!dt_inicio_corretagem
'        oNovoCorretor.Operacao = "N"
'        oNovoCorretor.Persistente = True
'
'        colSubGrupo(CStr(rs!sub_grupo_id)).colCorretor.Add oNovoCorretor, CStr(rs!corretor_id)
'
'        Set oNovoCorretor = Nothing
'
'        rs.MoveNext
'
'    Wend
'
'    Exit Sub
'
'Erro:
'    Call TratarErro("MontarColecaoCorretagemSubGrupo", Me.name)
'    Call FinalizarAplicacao
'End Sub



'Sub CarregarDadosCorretagem(Optional ByVal bRestaura As Boolean = False)
'
'    Dim oCorretagem As Object
'    Dim rs As Recordset
'
'    stbEndosso.TabVisible(6) = True
'    stbEndosso.Tab = 6
'
'    Set oCorretagem = CreateObject("SEGL0026.cls00126")
'
'    If colSubGrupo.Count = 0 Then
'
'
'    Set rs = oCorretagem.ObterDadosCorretor(glAmbiente_id, App.Title, _
     '                                            App.FileDescription, gsSIGLASISTEMA, _
     '                                            oDadosEndosso.PropostaId, colSubGrupo.Count, _
     '                                            oDadosEndosso.RamoId, oDadosEndosso.SeguradoraCodSusep, _
     '                                            oDadosEndosso.SucursalSeguradoraId, oDadosEndosso.ApoliceId)
'
'    Set oCorretagem = Nothing
'
'    If Not rs.EOF Then
'        '' Para ap�lices sem subgrupo
'        If colSubGrupo.Count = 0 Then
'            Call MontarColecaoCorretagem(rs, bRestaura)
'            Call MontarGridCorretagem
'        Else
'            Call MontarColecaoCorretagemSubGrupo(rs, bRestaura)
'        End If
'        rs.Close
'    End If
'
'    Set rs = Nothing
'
'End Sub

Sub ConsultarEstipulante(ByVal lPropostaId As Long)

    Dim oApolice As Object

    Dim cPercProLabore As Currency
    Dim lEstClienteId As Long
    Dim sNomeEstipulante As String

    On Error GoTo TrataErro

    grdEstipulante.Rows = 1

    Set oApolice = CreateObject("SEGL0026.cls00126")

    Call oApolice.ObterPercentualProLabore(gsSIGLASISTEMA, _
                                           App.Title, _
                                           App.FileDescription, _
                                           glAmbiente_id, _
                                           lPropostaId, _
                                           Data_Sistema, _
                                           cPercProLabore, _
                                           lEstClienteId, _
                                           sNomeEstipulante)

    Set oApolice = Nothing

    If lEstClienteId > 0 Then
        grdEstipulante.AddItem lEstClienteId & vbTab & _
                               sNomeEstipulante & vbTab & _
                               Format(cPercProLabore, "0.00")

        CboEstipulante.AddItem sNomeEstipulante
        CboEstipulante.ItemData(CboEstipulante.NewIndex) = lEstClienteId

    End If

    Exit Sub

TrataErro:
    Call TratarErro("ConsultarEstipulante", Me.name)
    Call FinalizarAplicacao

End Sub

Private Sub CalcularValorSubvencao()

    Dim cValPremioTarifa As Currency
    Dim cValSubvencaoFederal As Currency
    Dim cValSubvencaoEstadual As Currency
    Dim dPercFederal As Double

    On Error GoTo TrataErro

    'Obtendo valores da Interface''''''''''''''''''''''''''''''''''''''''''''

    cValPremioTarifa = CCur(mskValPremioTarifaRes.Text)
    cValSubvencaoEstadual = CCur(mskValEstadual.Text)
    dPercFederal = lblPercFederal.Caption

    'Calculando o Valor da Subven��o Federal ''''''''''''''''''''''''''''''''
    cValSubvencaoFederal = (cValPremioTarifa / 100)
    cValSubvencaoFederal = cValSubvencaoFederal * dPercFederal

    'Atualizando interface'''''''''''''''''''''''''''''''''''''''''''''''''''

    lblValFederal.Caption = Format(cValSubvencaoFederal, "#,##0.00")
    'mskValFederal.Text = lblValFederal.Caption 'Retirar Talitha 25.02.2010

    Exit Sub

TrataErro:
    Call TratarErro("CalcularValorSubvencao", Me.name)
    Call FinalizarAplicacao

End Sub

Private Sub CalcularValorRemuneracao()
'Procedimento criado para calcular o valor da remunera��o
'bnery - 14/07/2011 - Nova Consultoria
'Demanda 11239622 - Melhorias Ferramentas de Emiss�o

    Dim cValPremioTarifa As Currency
    Dim cValRemuneracao As Currency
    Dim cValPercRemuneracao As Double
    Dim oFinanceiro As Object
    Dim rs As ADODB.Recordset

    On Error GoTo TrataErro

    Set oFinanceiro = CreateObject("SEGL0026.cls00125")

    Set rs = oFinanceiro.ConsultarCusto(gsSIGLASISTEMA, _
                                        App.Title, _
                                        App.FileDescription, _
                                        glAmbiente_id, _
                                        oDadosEndosso.RamoId, _
                                        oDadosEndosso.produtoId, _
                                        Data_Sistema)

    If Not rs.EOF Then
        cValPercRemuneracao = rs("perc_remuneracao_servico")
        rs.Close
    Else
        cValPercRemuneracao = 0
    End If

    Set rs = Nothing
    Set oFinanceiro = Nothing

    cValPremioTarifa = CCur(mskValPremioTarifaRes.Text)

    cValRemuneracao = cValPremioTarifa * (cValPercRemuneracao / 100)

    lblValRemunercaoServico.Caption = Format(cValRemuneracao, "#,##0.00")

    Exit Sub

TrataErro:

    Call TratarErro("CalcularValorRemuneracao", Me.name)
    Call FinalizarAplicacao

End Sub

Sub CalcularValorRestituicao()

    Dim cValAdicFracionamento As Currency
    Dim cValCustoCertificado As Currency
    Dim cValPremioTarifa As Currency
    Dim cValRestituicao As Currency
    Dim cValDesconto As Currency
    Dim cValIOF As Currency

    On Error GoTo TrataErro

    'Obtendo valores da Interface''''''''''''''''''''''''''''''''''''''''''''

    cValPremioTarifa = CCur(mskValPremioTarifaRes.Text)
    cValCustoCertificado = CCur(mskCustoCertificadoRes.Text)
    cValDesconto = CCur(mskDescontoRes.Text)
    cValAdicFracionamento = CCur(mskAdicFracionamentoRes.Text)
    cValIOF = CCur(mskValIOFRes.Text)

    cValRestituicao = cValPremioTarifa _
                    + cValCustoCertificado _
                    + cValAdicFracionamento _
                    - cValDesconto _
                    + cValIOF

    'Atualizando interface'''''''''''''''''''''''''''''''''''''''''''''''''

    lblValRestituicaoRes.Caption = Format(cValRestituicao, "#,##0.00")

    Exit Sub

TrataErro:
    Call TratarErro("CalcularValorRestituicao", Me.name)
    Call FinalizarAplicacao

End Sub

Private Sub CalcularValorCorretagem()

    Dim cPercTotalCorretagem As Currency
    Dim iCont As Integer

    On Error GoTo TrataErro

    cPercTotalCorretagem = 0

    For iCont = 1 To grdCorretagem.Rows - 1
        cPercTotalCorretagem = cPercTotalCorretagem + CCur(grdCorretagem.TextMatrix(iCont, 2))
    Next

    lblValCorretagem.Caption = Format((CCur(mskValPremioTarifaRes.Text)) * (cPercTotalCorretagem / 100), "#,##0.00")
    mskValCom.Text = Format((CCur(mskValPremioTarifa.Text) - CCur(mskValDesc.Text) + CCur(mskValJuros.Text)) * (cPercTotalCorretagem / 100), "#,##0.00")

    oDadosEndosso.PercCorretagem = cPercTotalCorretagem

    Exit Sub

TrataErro:
    Call TratarErro("CalcularValorCorretagem", Me.name)
    Call FinalizarAplicacao
End Sub

Private Sub CalcularValorCorretagemContestacao()

    Dim cPercTotalCorretagem As Currency
    Dim iCont As Integer
    Dim lblValCorretagemAnterior As Double

    On Error GoTo TrataErro

    cPercTotalCorretagem = 0

    For iCont = 1 To grdCorretagem.Rows - 1
        cPercTotalCorretagem = cPercTotalCorretagem + CCur(grdCorretagem.TextMatrix(iCont, 2))
    Next

    lblValCorretagemAnterior = lblValCorretagem.Caption

    'lblValCorretagem.Caption =  Format((CCur(mskValPremioTarifaRes.Text)) * (cPercTotalCorretagem / 100), "#,##0.00")
    
    lblValCorretagem.Caption = Format(TruncaDecimal((CCur(mskValPremioTarifaRes.Text)) * (cPercTotalCorretagem / 100)), "#,##0.00")
    
    
    
    If CDbl(lblValCorretagem.Caption) > lblValCorretagemAnterior Then
        lblValCorretagem.Caption = lblValCorretagemAnterior
    End If
    
    
    mskValCom.Text = Format((CCur(mskValPremioTarifa.Text) - CCur(mskValDesc.Text) + CCur(mskValJuros.Text)) * (cPercTotalCorretagem / 100), "#,##0.00")

    oDadosEndosso.PercCorretagem = cPercTotalCorretagem

    Exit Sub

TrataErro:
    Call TratarErro("CalcularValorCorretagemContestacao", Me.name)
    Call FinalizarAplicacao


End Sub
Private Sub CalcularValorModular()

    Dim cValAdicFracionamento As Currency
    Dim cValCustoCertificado As Currency
    Dim cValPremioTarifa As Currency
    Dim cValRestituicao As Currency
    Dim cValDesconto As Currency
    Dim cValIOF As Currency
    Dim cPercTotalCorretagem As Currency
    Dim iCont As Integer
    Dim cPercTotalProLabore As Currency


    Dim sDataInicio As String
    Dim sDataCanc As String
    Dim sDataFim As String

    Dim dFatorDias As Double
    Dim dDias As Double


    cPercTotalCorretagem = 0
    cPercTotalProLabore = 0

    On Error GoTo TrataErro


    'Obtendo valores da Interface''''''''''''''''''''''''''''''''''''''''''''

    Select Case iModular
    Case 0
        cValPremioTarifa = CCur(mskValPremioTarifaResCancelamento(0).Text)
        cValCustoCertificado = CCur(mskValPremioTarifaResCancelamento(1).Text)
        cValDesconto = CCur(mskValPremioTarifaResCancelamento(3).Text)
        cValAdicFracionamento = CCur(mskValPremioTarifaResCancelamento(2).Text)
        cValIOF = CCur(mskValPremioTarifaResCancelamento(4).Text)

        cValRestituicao = cValPremioTarifa _
                        + cValCustoCertificado _
                        + cValAdicFracionamento _
                        - cValDesconto _
                        + cValIOF

        'Atualiza valor corretagem
        lblValRestituicaoResCancelamento(0).Caption = Format(cValRestituicao, "#,##0.00")

        For iCont = 1 To grdCorretagem.Rows - 1
            cPercTotalCorretagem = cPercTotalCorretagem + CCur(grdCorretagem.TextMatrix(iCont, 2))
        Next

        lblvalcorretagemprolabore(0).Caption = Format((CCur(mskValPremioTarifaResCancelamento(0).Text)) * (cPercTotalCorretagem / 100), "#,##0.00")
        'mskValCom.Text = Format((CCur(mskValPremioTarifaResCancelamento(0).Text) - CCur(mskValPremioTarifaResCancelamento(3).Text) + CCur(mskValPremioTarifaResCancelamento(4).Text)) * (cPercTotalCorretagem / 100), "#,##0.00")



        'oDadosEndosso.percCorretagem = cPercTotalCorretagem
        oDadosEndosso.PercCorretagem = cPercTotalCorretagem

        'Valor ProLabore

        For iCont = 1 To grdEstipulante.Rows - 1
            cPercTotalProLabore = cPercTotalProLabore + CCur(grdEstipulante.TextMatrix(iCont, 2))
        Next

        If txtProduto.Text = "111" Then
            cPercTotalProLabore = 2
        End If

        lblvalcorretagemprolabore(1).Caption = Format(CCur(mskValPremioTarifaResCancelamento(0).Text) * (cPercTotalProLabore / 100), "#,##0.00")
        'mskValComEst.Text = Format((CCur(mskValPremioTarifa.Text) - CCur(mskValDesc.Text) + CCur(mskValJuros.Text)) * (cPercTotalProLabore / 100), "#,##0.00")

        If grdEstipulante.Rows > 1 Then
            oDadosEndosso.EstipulanteIdModular = Val(grdEstipulante.TextMatrix(1, 0))
            oDadosEndosso.percProLaboreModular = cPercTotalProLabore
        Else
            oDadosEndosso.EstipulanteIdModular = 0
            oDadosEndosso.percProLaboreModular = 0
        End If

    Case 5
        cValPremioTarifa = CCur(mskValPremioTarifaResCancelamento(5).Text)
        cValCustoCertificado = CCur(mskValPremioTarifaResCancelamento(6).Text)
        cValDesconto = CCur(mskValPremioTarifaResCancelamento(7).Text)
        cValAdicFracionamento = CCur(mskValPremioTarifaResCancelamento(8).Text)
        cValIOF = CCur(mskValPremioTarifaResCancelamento(9).Text)

        '            cValRestituicao = cValPremioTarifa _
                     '                            + cValCustoCertificado _
                     '                            + cValAdicFracionamento _
                     '                            - cValDesconto _
                     '                            + cValIOF

        'rsilva - novo calculo para valor de restitui��o
        '            If oDadosModular.iTpEndossoId <> 80 Then
        '                sDataInicio = oDadosModular.sDtInicioVigenciaEsc
        '                sDataFim = oDadosModular.sDtFimVigenciaEsc
        '                sDataCanc = Data_Sistema 'Trim(mskDtVigCan.Text)
        '
        '                'dFatorDias = (DateDiff("d", sDataInicio, sDataFim) / 365)
        '                dDias = Val(DateDiff("d", sDataCanc, sDataFim))
        '                'cValRestituicao = (cValPremioTarifa / (1 + oDadosEndosso.PercIOF))
        '                cValRestituicao = cValPremioTarifa * (dDias / 30)
        '
        '            Else
        cValRestituicao = cValPremioTarifa _
                        + cValCustoCertificado _
                        + cValAdicFracionamento _
                        - cValDesconto _
                        + cValIOF
        '            End If

        'Atualiza valor corretagem
        lblValRestituicaoResCancelamento(1).Caption = Format(cValRestituicao, "#,##0.00")

        For iCont = 1 To grdCorretagem.Rows - 1
            cPercTotalCorretagem = cPercTotalCorretagem + CCur(grdCorretagem.TextMatrix(iCont, 2))
        Next

        lblvalcorretagemprolabore(2).Caption = Format((CCur(mskValPremioTarifaResCancelamento(5).Text)) * (cPercTotalCorretagem / 100), "#,##0.00")
        'mskValCom.Text = Format((CCur(mskValPremioTarifaResCancelamento(0).Text) - CCur(mskValPremioTarifaResCancelamento(3).Text) + CCur(mskValPremioTarifaResCancelamento(4).Text)) * (cPercTotalCorretagem / 100), "#,##0.00")

        'oDadosEndosso.percCorretagem = cPercTotalCorretagem
        oDadosModular.PercCorretagem = cPercTotalCorretagem

        'Valor ProLabore
        For iCont = 1 To grdEstipulante.Rows - 1
            cPercTotalProLabore = cPercTotalProLabore + CCur(grdEstipulante.TextMatrix(iCont, 2))
        Next

        If txtProduto.Text = "111" Then
            cPercTotalProLabore = 2
        End If

        lblvalcorretagemprolabore(3).Caption = Format(CCur(mskValPremioTarifaResCancelamento(5).Text) * (cPercTotalProLabore / 100), "#,##0.00")
        'mskValComEst.Text = Format((CCur(mskValPremioTarifa.Text) - CCur(mskValDesc.Text) + CCur(mskValJuros.Text)) * (cPercTotalProLabore / 100), "#,##0.00")

        If grdEstipulante.Rows > 1 Then
            oDadosEndosso.EstipulanteIdModular = Val(grdEstipulante.TextMatrix(1, 0))
            oDadosEndosso.percProLaboreModular = cPercTotalProLabore
        Else
            oDadosEndosso.EstipulanteIdModular = 0
            oDadosEndosso.percProLaboreModular = 0
        End If

    End Select

    Call PreencherDadosEndossoRestituicaoModular






    Exit Sub

TrataErro:
    Call TratarErro("CalcularValorModular", Me.name)
    Call FinalizarAplicacao


End Sub

Public Sub PreencherDadosEndossoRestituicao()

    On Error GoTo TrataErro

    oDadosEndosso.valPremioTarifa = CCur(mskValPremioTarifaRes.Text)
    oDadosEndosso.valCustoEmissao = CCur(mskCustoCertificadoRes.Text)
    oDadosEndosso.valAdicFracionamento = CCur(mskAdicFracionamentoRes.Text)
    oDadosEndosso.valDesconto = CCur(mskDescontoRes.Text)
    oDadosEndosso.valIOF = CCur(mskValIOFRes.Text)
    oDadosEndosso.ValRestituicao = CCur(lblValRestituicaoRes.Caption)
    oDadosEndosso.valCorretagem = CCur(lblValCorretagem.Caption)
    oDadosEndosso.ValProLabore = CCur(lblValProLabore.Caption)

    If bPossuiSubvencao = True Then
        oDadosEndosso.valSubvencaoEstadual = CCur(mskValEstadual.Text)
        oDadosEndosso.valSubvencaoFederal = CCur(lblValFederal.Caption)
        oDadosEndosso.PossuiSubvencao = bPossuiSubvencao
    End If

    'bnery - 14/07/2011 - Nova Consultoria
    'Demanda 11239622 - Melhorias Ferramentas de Emiss�o
    oDadosEndosso.valRemuneracaoServico = CCur(lblValRemunercaoServico.Caption)

    oDadosEndosso.valRepasse = CalcularRepasse()

    Exit Sub

TrataErro:
    Call TratarErro("PreencherDadosEndossoRestituicao", Me.name)
    Call FinalizarAplicacao

End Sub

Public Sub PreencherDadosEndossoRestituicaoModular()

    On Error GoTo TrataErro

    If iModular = 0 Then

        oDadosEndosso.valPremioTarifa = CCur(mskValPremioTarifaResCancelamento(0).Text)
        oDadosEndosso.valCustoEmissao = CCur(mskValPremioTarifaResCancelamento(1).Text)
        oDadosEndosso.valAdicFracionamento = CCur(mskValPremioTarifaResCancelamento(2).Text)
        oDadosEndosso.valDesconto = CCur(mskValPremioTarifaResCancelamento(3).Text)
        oDadosEndosso.valIOF = CCur(mskValPremioTarifaResCancelamento(4).Text)
        oDadosEndosso.ValRestituicao = CCur(lblValRestituicaoResCancelamento(0).Caption)
        oDadosEndosso.valCorretagem = CCur(lblvalcorretagemprolabore(0).Caption)
        oDadosEndosso.ValProLabore = CCur(lblvalcorretagemprolabore(1).Caption)

        oDadosEndosso.valRepasse = CalcularRepasseModular()

    End If

    If iModular = 5 Then
        oDadosModular.valPremioTarifa = CCur(mskValPremioTarifaResCancelamento(5).Text)
        oDadosModular.valCustoEmissao = CCur(mskValPremioTarifaResCancelamento(6).Text)
        oDadosModular.valAdicFracionamento = CCur(mskValPremioTarifaResCancelamento(7).Text)
        oDadosModular.valDesconto = CCur(mskValPremioTarifaResCancelamento(8).Text)
        oDadosModular.valIOF = CCur(mskValPremioTarifaResCancelamento(9).Text)
        oDadosModular.ValRestituicao = CCur(lblValRestituicaoResCancelamento(1).Caption)
        oDadosModular.valCorretagem = CCur(lblvalcorretagemprolabore(2).Caption)
        oDadosModular.ValProLabore = CCur(lblvalcorretagemprolabore(3).Caption)

        oDadosModular.valRepasse = CalcularRepasseModular()
    End If

    Exit Sub

TrataErro:
    Call TratarErro("PreencherDadosEndossoRestituicao", Me.name)
    Call FinalizarAplicacao

End Sub

Sub IncluirAvaliacaoRestituicao(ByVal lPropostaId As Long, _
                                ByVal cValRestituicao As Currency, _
                                ByVal cValComissao As Currency, _
                                ByVal cValIOF As Currency, _
                                ByVal cValAdicFracionamento As Currency, _
                                ByVal cValDescontoComercial As Currency, _
                                ByVal cValPremioTarifa As Currency, _
                                ByVal cValCustoApolice As Currency, _
                                ByVal cValComissaoEstipulante As Currency, _
                                ByVal dValCambio As Double, _
                                ByVal sDtPedidoAvaliacao As String, _
                                ByVal sDescricao As String, _
                                Optional ByVal lEndossoId As Long = 0, _
                                Optional ByVal lTpEndossoId As Long = 0, _
                                Optional ByVal bPossuiSubvencao As Boolean = False, _
                                Optional ByVal cValSubvencaoFederal As Currency = 0, _
                                Optional ByVal cValSubvencaoEstadual As Currency = 0, _
                                Optional ByVal cValRemuneracaoServico As Currency = 0)

' bcarneiro - 24/11/2003 - Projeto Restitui��o por Iniciativa da Seguradora

    Dim oFinanceiro As Object
    Dim colCorretagem As Collection
    Dim glAmbiente_id_ABS As Integer

    On Error GoTo TrataErro

    Set colCorretagem = ObterCorretagemAvaliacaoRestituicao(cValPremioTarifa)

    Set oFinanceiro = CreateObject("SEGL0026.cls00125")

    If bModular = False And txtProduto.Text <> 1168 Then
        '24/05/2016 - Schoralick (nova consultoria) - 18234489 - Novos produtos para substituir Ouro Vida e outros - inicio
        If (txtProduto.Text = 1235 Or txtProduto.Text = 1236 Or txtProduto.Text = 1237) And (oDadosEndosso.TipoEndosso = TP_ENDOSSO_CANCELAMENTO_APOLICE_SEGURADORA) Then
            lTpEndossoId = 64
        Else
            lTpEndossoId = IIf(txtProduto.Text = "1231", 80, lTpEndossoId)
            If oDadosEndosso.TipoEndosso = TP_ENDOSSO_CONTESTACAO Then
                lTpEndossoId = 80
            End If
        End If
        'fim - 18234489
        Call oFinanceiro.IncluirAvaliacaoRestituicao(gsSIGLASISTEMA, App.Title, App.FileDescription, glAmbiente_id, _
                                                     cUserName, _
                                                     lPropostaId, _
                                                     cValRestituicao, cValComissao, _
                                                     cValIOF, cValAdicFracionamento, _
                                                     cValDescontoComercial, cValPremioTarifa, _
                                                     cValCustoApolice, cValComissaoEstipulante, dValCambio, _
                                                     sDtPedidoAvaliacao, sDescricao, _
                                                     colCorretagem, _
                                                     lEndossoId, lTpEndossoId, _
                                                     0, 0, 0, 0, 0, 0, 0, 0, _
                                                     IIf(bRestIntegral, "S", "N"), 0, _
                                                     bPossuiSubvencao, cValSubvencaoFederal, cValSubvencaoEstadual, cValRemuneracaoServico, _
                                                     "SEGP0777", _
                                                     IIf(oDadosEndosso.TipoEndosso = TP_ENDOSSO_CONTESTACAO, "S", ""))

    Else
        If txtProduto.Text = 1168 Then
            'Retirado a pedido da Alessandra - RSilva - 08/09/2010
            'If oDadosEndosso.ValRestituicao > 0 Then
            Call oFinanceiro.IncluirAvaliacaoRestituicao(gsSIGLASISTEMA, _
                                                         App.Title, _
                                                         App.FileDescription, _
                                                         glAmbiente_id, _
                                                         cUserName, _
                                                         oDadosEndosso.PropostaId, _
                                                         oDadosEndosso.ValRestituicao, _
                                                         oDadosEndosso.valCorretagem, _
                                                         oDadosEndosso.valIOF, _
                                                         oDadosEndosso.valAdicFracionamento, _
                                                         oDadosEndosso.valDesconto, _
                                                         oDadosEndosso.valPremioTarifa, _
                                                         oDadosEndosso.valCustoEmissao, _
                                                         oDadosEndosso.ValProLabore, _
                                                         dValCambio, _
                                                         sDtPedidoAvaliacao, _
                                                         sDescricao, _
                                                         colCorretagem, _
                                                         lEndossoId, _
                                                         lTpEndossoId, _
                                                         0, 0, 0, 0, 0, 0, 0, 0, IIf(bRestIntegral, "S", "N"), _
                                                         0, _
                                                         bPossuiSubvencao, _
                                                         cValSubvencaoFederal, _
                                                         cValSubvencaoEstadual, oDadosEndosso.valRemuneracaoServico)
            'Inicio Demanda:10113924 Data:31/03/2011 Autor:Rodrigo Moura - Confitec
            'Este parametro foi comentado por n�o est� sendo recebido
            'pelo metodo IncluirAvaliacaoRestituicao
            '--, 526)
            'Fim Demanda:10113924 Data:31/03/2011 Autor:Rodrigo Moura - Confitec
            'End If
            'Retirado a pedido da Alessandra - RSilva - 08/09/2010
            'If oDadosModular.ValRestituicao > 0 Then
            Call oFinanceiro.IncluirAvaliacaoRestituicao(gsSIGLASISTEMA, _
                                                         App.Title, _
                                                         App.FileDescription, _
                                                         glAmbiente_id, _
                                                         cUserName, _
                                                         oDadosModular.PropostaId, _
                                                         oDadosModular.ValRestituicao, _
                                                         oDadosModular.valCorretagem, _
                                                         oDadosModular.valIOF, _
                                                         oDadosModular.valAdicFracionamento, _
                                                         oDadosModular.valDesconto, _
                                                         oDadosModular.valPremioTarifa, _
                                                         oDadosModular.valCustoEmissao, _
                                                         oDadosModular.ValProLabore, _
                                                         dValCambio, _
                                                         sDtPedidoAvaliacao, _
                                                         sDescricao, _
                                                         colCorretagem, _
                                                         lEndossoId, _
                                                         lTpEndossoId, _
                                                         0, 0, 0, 0, 0, 0, 0, 0, IIf(bRestIntegral, "S", "N"), _
                                                         0, _
                                                         bPossuiSubvencao, _
                                                         cValSubvencaoFederal, _
                                                         cValSubvencaoEstadual)
            'Inicio Demanda:10113924 Data:31/03/2011 Autor:Rodrigo Moura - Confitec
            'Este parametro foi comentado por n�o est� sendo recebido
            'pelo metodo IncluirAvaliacaoRestituicao
            '--, 527)
            'Fim Demanda:10113924 Data:31/03/2011 Autor:Rodrigo Moura - Confitec

            'End If

        Else
            Call oFinanceiro.IncluirAvaliacaoRestituicao(gsSIGLASISTEMA, _
                                                         App.Title, _
                                                         App.FileDescription, _
                                                         glAmbiente_id, _
                                                         cUserName, _
                                                         oDadosEndosso.PropostaId, _
                                                         oDadosEndosso.ValRestituicao, _
                                                         oDadosEndosso.valCorretagem, _
                                                         oDadosEndosso.valIOF, _
                                                         oDadosEndosso.valAdicFracionamento, _
                                                         oDadosEndosso.valDesconto, _
                                                         oDadosEndosso.valPremioTarifa, _
                                                         oDadosEndosso.valCustoEmissao, _
                                                         oDadosEndosso.ValProLabore, _
                                                         dValCambio, _
                                                         sDtPedidoAvaliacao, _
                                                         sDescricao, _
                                                         colCorretagem, _
                                                         lEndossoId, _
                                                         lTpEndossoId, _
                                                         0, 0, 0, 0, 0, 0, 0, 0, IIf(bRestIntegral, "S", "N"), _
                                                         0, _
                                                         bPossuiSubvencao, _
                                                         cValSubvencaoFederal, _
                                                         cValSubvencaoEstadual, oDadosEndosso.valRemuneracaoServico)


            'Inicio Demanda:9420699 Data:31/03/2011 Autor:Rodrigo Moura - Confitec
            Select Case glAmbiente_id
            Case 2
                glAmbiente_id_ABS = 6
            Case 3
                glAmbiente_id_ABS = 7
            Case Else
                glAmbiente_id_ABS = 6
            End Select
            'Fim Demanda:9420699 Data:31/03/2011 Autor:Rodrigo Moura - Confitec
            Call oFinanceiro.IncluirAvaliacaoRestituicao(gsSIGLASISTEMA, _
                                                         App.Title, _
                                                         App.FileDescription, _
                                                         glAmbiente_id_ABS, _
                                                         cUserName, _
                                                         oDadosModular.PropostaId, _
                                                         oDadosModular.ValRestituicao, _
                                                         oDadosModular.valCorretagem, _
                                                         oDadosModular.valIOF, _
                                                         oDadosModular.valAdicFracionamento, _
                                                         oDadosModular.valDesconto, _
                                                         oDadosModular.valPremioTarifa, _
                                                         oDadosModular.valCustoEmissao, _
                                                         oDadosModular.ValProLabore, _
                                                         dValCambio, _
                                                         sDtPedidoAvaliacao, _
                                                         sDescricao, _
                                                         colCorretagem, _
                                                         lEndossoId, _
                                                         lTpEndossoId, _
                                                         0, 0, 0, 0, 0, 0, 0, 0, IIf(bRestIntegral, "S", "N"), _
                                                         0, _
                                                         bPossuiSubvencao, _
                                                         cValSubvencaoFederal, _
                                                         cValSubvencaoEstadual)




        End If
    End If

    Set oFinanceiro = Nothing

    Exit Sub

TrataErro:

    Call TratarErro("IncluirAvaliacaoRestituicao", Me.name)
    Call FinalizarAplicacao

End Sub

Private Sub AplicarRestituicao()

    Dim lPropostaId As Long
    Dim cValComissao As Currency
    Dim cValProLabore As Currency
    Dim cValCambio As Currency
    Dim sDescricao As String
    Dim cValIR As Currency
    Dim oExtrato As Object

    Dim lTpEndosso As Long

    Const FLAG_APROVACAO = "N"

    On Error GoTo TrataErro

    ' Verificando o valor da restitui��o ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

    If oDadosEndosso.TipoEndosso = TP_ENDOSSO_RESTITUICAO Or _
       (oDadosEndosso.TipoEndosso = TP_ENDOSSO_IMPORTANCIA_SEGURADA And optCobRes.Value) Then
        If CCur(lblValRestituicaoRes.Caption) = 0 And (bModular = False And txtProduto.Text <> 1168) Then
            Call MsgBox("Valor de restitui��o n�o pode ser zero.")
            mskValPremioTarifaRes.SetFocus
            Exit Sub
        Else
            If CCur(lblValRestituicaoResCancelamento(0).Caption) = 0 And (bModular = True And txtProduto.Text = 1168) Then
                Call MsgBox("Valor de restitui��o n�o pode ser zero.")
                mskValPremioTarifaResCancelamento(0).SetFocus
                Exit Sub
            End If
            If CCur(lblValRestituicaoResCancelamento(1).Caption) = 0 And (bModular = True And txtProduto.Text = 1168) Then
                Call MsgBox("Valor de restitui��o n�o pode ser zero.")
                mskValPremioTarifaResCancelamento(1).SetFocus
                Exit Sub
            End If
        End If
    End If

    lPropostaId = txtPropostaid(0).Text
    sDescricao = Trim(txtDescEndosso.Text)
    cValIR = oDadosEndosso.PercIR * oDadosEndosso.valCorretagem

    ' Gerando o endosso selecionado '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

    Me.MousePointer = vbHourglass

    'daniel.menandro - Nova Consultoria - 14/07/2013
    '16351367 - Extrato de Restitui��o
    'Inicio
    Select Case oDadosEndosso.TipoEndosso

    Case TP_ENDOSSO_RESTITUICAO
        Set oExtrato = CreateObject("SEGL0026.cls00125")
        Call IncluirAvaliacaoRestituicao(oDadosEndosso.PropostaId, _
                                         TruncaDecimal(oDadosEndosso.ValRestituicao), _
                                         TruncaDecimal(oDadosEndosso.valCorretagem), _
                                         TruncaDecimal(oDadosEndosso.valIOF), _
                                         TruncaDecimal(oDadosEndosso.valAdicFracionamento), _
                                         TruncaDecimal(oDadosEndosso.valDesconto), _
                                         TruncaDecimal(oDadosEndosso.valPremioTarifa), _
                                         TruncaDecimal(oDadosEndosso.valCustoEmissao), _
                                         TruncaDecimal(oDadosEndosso.ValProLabore), _
                                         CCur(mskCambio.Text), _
                                         Data_Sistema, _
                                         sDescricao, , , , , , _
                                         TruncaDecimal(oDadosEndosso.valRemuneracaoServico))

        Call oExtrato.InserirExtratoRestituicao(gsSIGLASISTEMA, _
                                                App.Title, _
                                                App.FileDescription, _
                                                glAmbiente_id, _
                                                cUserName, _
                                                oDadosEndosso.PropostaId, _
                                                TruncaDecimal(oDadosEndosso.ValRestituicao), _
                                                0, _
                                                oDadosEndosso.TipoEndosso, _
                                                "Cria��o do Endosso: ", _
                                                "Aguardando avalia��o t�cnica de restitui��o")

        ' G&P - Flow - 219229
        Set oExtrato = Nothing
    Case TP_ENDOSSO_CANCELAMENTO_APOLICE, TP_ENDOSSO_CANCELAMENTO_APOLICE_SEGURADORA, TP_ENDOSSO_CANCELAMENTO_INADIMP_1_PARCELA, _
         TP_ENDOSSO_CANCELAMENTO_INADIMP_DEMAIS_PARCELAS
        ''inicio: enio.oliveira - Nova Consultoria 27/12/2016 - 19286581 - Corre��o Carta Recusa
        Dim lTpCancelamento As Integer
        lTpCancelamento = 2
        'Valida��o tipo de endosso igual 64 (Cancelamento a pedido da seguradora)
        If oDadosEndosso.TipoEndosso = TP_ENDOSSO_CANCELAMENTO_APOLICE_SEGURADORA Then
            Dim oObterTipoRamo As Object
            Dim ltpRamo As Integer
            Set oObterTipoRamo = CreateObject("SEGL0026.cls00126")

            ltpRamo = oObterTipoRamo.ObterTipoRamo(gsSIGLASISTEMA, _
                                                   App.Title, _
                                                   App.FileDescription, _
                                                   glAmbiente_id, _
                                                   txtRamo.Text)

            Set oObterTipoRamo = Nothing

            If (ltpRamo = 1 And Not (txtProduto.Text = 1226 Or txtProduto.Text = 1227)) Or txtProduto.Text = 1218 Then
                lTpCancelamento = 4
            End If
        End If
        ''fim: enio.oliveira - Nova Consultoria 27/12/2016 - 19286581 - Corre��o Carta Recusa

        Call IncluirEndossoCancelamentoApolice(oDadosEndosso.ApoliceId, _
                                               oDadosEndosso.SucursalSeguradoraId, _
                                               oDadosEndosso.SeguradoraCodSusep, _
                                               oDadosEndosso.PropostaId, _
                                               oDadosEndosso.RamoId, _
                                               0, _
                                               oDadosEndosso.TipoEndosso, _
                                               0, _
                                               0, _
                                               0, _
                                               0, _
                                               0, _
                                               0, _
                                               0, 0, _
                                               0, 0, _
                                               0, 0, _
                                               mskDtEndosso.Text, mskDtEmissao.Text, _
                                               oDadosEndosso.BeneficiarioId, oDadosEndosso.PercIR, _
                                               0, _
                                               "", _
                                               sDescricao, _
                                               mskDtVigCan.Text, _
                                               mskDtVigCan.Text, _
                                               lTpCancelamento, _
                                               Trim(txtMotCancel.Text))


        If optCobRes And (mskValPremioTarifaRes.Text > 0 Or mskValPremioTarifaResCancelamento(0).Text > 0) Then
            Set oExtrato = CreateObject("SEGL0026.cls00125")

            Call IncluirAvaliacaoRestituicao(oDadosEndosso.PropostaId, _
                                             TruncaDecimal(oDadosEndosso.ValRestituicao), _
                                             TruncaDecimal(oDadosEndosso.valCorretagem), _
                                             TruncaDecimal(oDadosEndosso.valIOF), _
                                             TruncaDecimal(oDadosEndosso.valAdicFracionamento), _
                                             TruncaDecimal(oDadosEndosso.valDesconto), _
                                             TruncaDecimal(oDadosEndosso.valPremioTarifa), _
                                             TruncaDecimal(oDadosEndosso.valCustoEmissao), _
                                             TruncaDecimal(oDadosEndosso.ValProLabore), _
                                             CCur(mskCambio.Text), _
                                             Data_Sistema, _
                                             sDescricao, _
                                             IIf(txtProduto.Text = "1231", 0, Val(txtNumEndosso.Text)), 0, _
                                             oDadosEndosso.PossuiSubvencao, _
                                             TruncaDecimal(oDadosEndosso.valSubvencaoFederal), _
                                             TruncaDecimal(oDadosEndosso.valSubvencaoEstadual), _
                                             TruncaDecimal(oDadosEndosso.valRemuneracaoServico))     'Rmarins - 18/10/06
            '                If bModular = True Then
            '                    Call IncluirAvaliacaoRestituicao(oDadosModular.PropostaId, _
                                 '                                                 TruncaDecimal(oDadosModular.ValRestituicao), _
                                 '                                                 TruncaDecimal(oDadosModular.valCorretagem), _
                                 '                                                 TruncaDecimal(oDadosModular.valIOF), _
                                 '                                                 TruncaDecimal(oDadosModular.valAdicFracionamento), _
                                 '                                                 TruncaDecimal(oDadosModular.valDesconto), _
                                 '                                                 TruncaDecimal(oDadosModular.valPremioTarifa), _
                                 '                                                 TruncaDecimal(oDadosModular.valCustoEmissao), _
                                 '                                                 TruncaDecimal(oDadosModular.valProLabore), _
                                 '                                                 CCur(mskCambio.Text), _
                                 '                                                 Data_Sistema, _
                                 '                                                 sDescricao, _
                                 '                                                 oDadosModular.EndossoId, 0, _
                                 '                                                 oDadosEndosso.PossuiSubvencao, _
                                 '                                                 TruncaDecimal(oDadosEndosso.valSubvencaoFederal), _
                                 '                                                 TruncaDecimal(oDadosEndosso.valSubvencaoEstadual))    'Rmarins - 18/10/06
            '
            '
            '                End If
            Call oExtrato.InserirExtratoRestituicao(gsSIGLASISTEMA, _
                                                    App.Title, _
                                                    App.FileDescription, _
                                                    glAmbiente_id, _
                                                    cUserName, _
                                                    oDadosEndosso.PropostaId, _
                                                    TruncaDecimal(oDadosEndosso.ValRestituicao), _
                                                    IIf(txtProduto.Text = "1231", 0, Val(txtNumEndosso.Text)), _
                                                    IIf(txtProduto.Text = "1231", 80, oDadosEndosso.TipoEndosso), _
                                                    "Cria��o do Endosso: ", _
                                                    "Aguardando avalia��o t�cnica de restitui��o")
            Set oExtrato = Nothing
        End If

    Case TP_ENDOSSO_IMPORTANCIA_SEGURADA

        Call IncluirEndossoISRestituicao

        If optCobRes.Value Then
            Set oExtrato = CreateObject("SEGL0026.cls00125")

            Call IncluirAvaliacaoRestituicao(oDadosEndosso.PropostaId, _
                                             TruncaDecimal(oDadosEndosso.ValRestituicao), _
                                             TruncaDecimal(oDadosEndosso.valCorretagem), _
                                             TruncaDecimal(oDadosEndosso.valIOF), _
                                             TruncaDecimal(oDadosEndosso.valAdicFracionamento), _
                                             TruncaDecimal(oDadosEndosso.valDesconto), _
                                             TruncaDecimal(oDadosEndosso.valPremioTarifa), _
                                             TruncaDecimal(oDadosEndosso.valCustoEmissao), _
                                             TruncaDecimal(oDadosEndosso.ValProLabore), _
                                             CCur(mskCambio.Text), _
                                             Data_Sistema, _
                                             txtDescEndosso.Text, _
                                             Val(txtNumEndosso.Text))

            Call oExtrato.InserirExtratoRestituicao(gsSIGLASISTEMA, _
                                                    App.Title, _
                                                    App.FileDescription, _
                                                    glAmbiente_id, _
                                                    cUserName, _
                                                    oDadosEndosso.PropostaId, _
                                                    TruncaDecimal(oDadosEndosso.ValRestituicao), _
                                                    Val(txtNumEndosso.Text), _
                                                    oDadosEndosso.TipoEndosso, _
                                                    "Cria��o do Endosso: ", _
                                                    "Aguardando avalia��o t�cnica de restitui��o")


            Set oExtrato = Nothing
        End If

        Call GravarCoberturas

        'Alexandre Debouch - Confitec - EdsContestacao
    Case TP_ENDOSSO_CONTESTACAO
        Dim oEdsContestacao As Object
        Dim lTipoRetornoEndossoContestacao As Long
        Me.MousePointer = vbHourglass
        Call PreencherDadosEndossoRestituicao
        frmEndosso.mskDtVigCan.Text = frmEndosso.mskDtEndosso.Text
        
        Set oEdsContestacao = CreateObject("SEGL0026.cls00500")
        
        lTipoRetornoEndossoContestacao = 0
        lTipoRetornoEndossoContestacao = oEdsContestacao.ProcessarContestacao(gsSIGLASISTEMA, App.Title, App.FileDescription, _
                                                glAmbiente_id, oDadosEndosso.PropostaId, frmEndosso.mskDtEndosso.Text, _
                                                frmEndosso.mskDtEmissao.Text, Trim(frmEndosso.txtDescEndosso.Text), cUserName, _
                                                oDadosEndosso.produtoId, txtRamo.Text, frmEndosso.oDadosEndosso.ApoliceId, _
                                                frmEndosso.oDadosEndosso.SucursalSeguradoraId, frmEndosso.oDadosEndosso.SeguradoraCodSusep, _
                                                frmEndosso.oDadosEndosso.BeneficiarioId, frmEndosso.oDadosEndosso.PercIR, Date, frmEndosso.mskDtVigCan.Text, _
                                                CCur(frmEndosso.oDadosEndosso.ValRestituicao), CCur(frmEndosso.oDadosEndosso.valCorretagem), _
                                                CCur(frmEndosso.oDadosEndosso.valIOF), CCur(frmEndosso.oDadosEndosso.valAdicFracionamento), _
                                                CCur(frmEndosso.oDadosEndosso.valDesconto), CCur(frmEndosso.oDadosEndosso.valPremioTarifa), _
                                                CCur(frmEndosso.oDadosEndosso.valCustoEmissao), CCur(frmEndosso.oDadosEndosso.ValProLabore), _
                                                frmEndosso.mskCambio.Text, "Avalia��o para endosso de restitui��o a pedido da seguradora - Eds. Contesta��o", _
                                                frmEndosso.oDadosEndosso.PossuiSubvencao, CCur(frmEndosso.oDadosEndosso.valSubvencaoFederal), _
                                                CCur(frmEndosso.oDadosEndosso.valSubvencaoEstadual), CCur(frmEndosso.oDadosEndosso.valRemuneracaoServico), _
                                                frmEndosso.oDadosEndosso.PremioMoedaId, frmEndosso.oDadosEndosso.FatorCambio, frmEndosso.oDadosEndosso.TpEmissao, CCur(frmEndosso.oDadosEndosso.PercProLabore), _
                                                False)
        Me.MousePointer = vbDefault
        Select Case lTipoRetornoEndossoContestacao
        Case 0
            Me.MousePointer = vbDefault
            Call MsgBox("Ocorreu um erro no processo. " & vbNewLine & vbNewLine & _
                        "Verifique o extrato de restitui��o para nova tentativa.", vbCritical)
        Case 1
            Call MsgBox("Somente o Endosso de Contesta��o foi gerado." & vbNewLine & vbNewLine & _
                        "Ocorreu algum erro no processo, por favor verifique.", vbCritical)
        Case 2
            Call MsgBox("Somente os Endossos de Contesta��o e Cancelamento foram gerados." & vbNewLine & vbNewLine & _
                        "Verifique no extrato de restitui��o se existem valores a serem devolvidos para a(o) segurada(o)." & vbNewLine & vbNewLine & _
                        "Ou ocorreu algum erro no processo.", vbCritical)
        Case 2
            Call MsgBox("Os Endossos de Contesta��o e Cancelamento, conjuntamente com a avalia��o restitui��o foram gerados." & vbNewLine & vbNewLine & _
                        "Ocorreu algum erro no processo, por favor verifique.", vbCritical)
        Case 4
            Call MsgBox("Endosso de contesta��o, cancelamento e restitui��o realizados com sucesso.", vbInformation)
        End Select
        Set oEdsContestacao = Nothing
        Me.MousePointer = vbDefault
        Call sair
        Exit Sub
    End Select
    'Alexandre Debouch - Confitec - EdsContestacao

    Set oExtrato = Nothing
    Me.MousePointer = vbDefault

    'Atualiza Retorno BB
    If vOutrosCanais = "S" Then

        If oDadosEndosso.TipoEndosso = TP_ENDOSSO_CANCELAMENTO_APOLICE Or oDadosEndosso.TipoEndosso = TP_ENDOSSO_CANCELAMENTO_APOLICE_SEGURADORA Then

            Call RetornoBBOutrosCanais(oDadosEndosso.PropostaId, Val(txtNumEndosso.Text))

            If bModular = True Then

                Call RetornoBBOutrosCanais(oDadosModular.PropostaId, oDadosModular.EndossoId)

            End If

        End If

    End If

    ' Exibindo mensagem de sucesso ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

    If oDadosEndosso.TipoEndosso = TP_ENDOSSO_RESTITUICAO Then

        Call MsgBox("Restitui��o por Iniciativa da Seguradora processada com sucesso.", vbInformation)

    Else
        If bModular = False Then
            If oDadosEndosso.TipoEndosso = TP_ENDOSSO_CONTESTACAO Then
                Call MsgBox("Endosso de contesta��o, cancelamento e restitui��o realizados com sucesso.", vbInformation)
            Else
                Call MsgBox("Endosso realizado com sucesso." & vbNewLine & _
                            "Proposta: " & oDadosEndosso.PropostaId & vbNewLine & _
                            "Endosso : " & txtNumEndosso.Text, vbInformation)
            End If
        Else
            Call MsgBox("Endosso realizado com sucesso." & vbNewLine & _
                        "Proposta: " & oDadosEndosso.PropostaId & vbNewLine & _
                        "Endosso : " & txtNumEndosso.Text, vbInformation)
            Call MsgBox("Endosso realizado com sucesso." & vbNewLine & _
                        "Proposta: " & oDadosModular.PropostaId & vbNewLine & _
                        "Endosso : " & oDadosModular.EndossoId, vbInformation, Me.Caption & " - Modular")
        End If
    End If

    ' Retornando ao formul�rio de sele��o de propostas ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

    Call sair

    Exit Sub

TrataErro:

    Call TratarErro("AplicarRestituicao", Me.name)
    FinalizarAplicacao

End Sub

Private Function GravarCoberturas() As Boolean

    Dim oCobertura As Cobertura
    Dim oCoberturaEndossada As Cobertura
    Dim bEncontrou As Boolean
    Dim sAuxVigSeg As String
    Dim oSeguro As Object

    On Error GoTo Erro

    GravarCoberturas = False

    Call IniciarConexao(gsSIGLASISTEMA, _
                        App.Title, _
                        App.FileDescription, _
                        glAmbiente_id, _
                        cUserName)

    Call oConexao.IniciarTransacao

    Set oSeguro = CreateObject("SEGL0148.cls00181")

    Set oSeguro.oConexao = oConexao

    For Each oCoberturaEndossada In colCoberturasEndossadas

        bEncontrou = False

        For Each oCobertura In colCoberturas

            If (oCoberturaEndossada.CodObjSegurado = oCobertura.CodObjSegurado) And _
               oCoberturaEndossada.TpCoberturaId = oCobertura.TpCoberturaId Then

                'Exclus�o de Cobertura
                If oCoberturaEndossada.TpCobranca = "R" And ((CDbl(oCoberturaEndossada.ValIS) - CDbl(oCobertura.ValIS)) = 0) Then

                    Call oSeguro.GravarCobertura(oDadosEndosso.PropostaId, "E", _
                                                 Val(txtNumEndosso.Text), oCoberturaEndossada.TpCoberturaId, _
                                                 oDadosEndosso.ProdutoExternoId, oDadosEndosso.produtoId, _
                                                 oDadosEndosso.RamoId, oCoberturaEndossada.CodObjSegurado, _
                                                 sTabelaEscolha, Format(mskDtEndosso, "yyyymmdd"), _
                                                 Format(sDtIniVigenciaSeg(oCoberturaEndossada.CodObjSegurado), "yyyymmdd"), _
                                                 MudaVirgulaParaPonto(Format(oCoberturaEndossada.ValIS, "000000000.00")), _
                                                 MudaVirgulaParaPonto(Format(oCoberturaEndossada.ValTaxa, "000000000.0000000")), _
                                                 MudaVirgulaParaPonto(Format(oCoberturaEndossada.ValPremio, "000000000.00")), _
                                                 MudaVirgulaParaPonto(Format(oCoberturaEndossada.PercDesconto, "000000000.0000000")), _
                                                 MudaVirgulaParaPonto(Format(oCoberturaEndossada.ValFranquia, "000000000.00")), _
                                                 MudaVirgulaParaPonto(Format(oCoberturaEndossada.FatFranquia, "000000000.0000000")), _
                                                 Trim(oCoberturaEndossada.DescFranquia), _
                                                 Format(mskDtEndosso.Text, "yyyymmdd"), _
                                                 cUserName, oDadosEndosso.TpEmissao, oCoberturaEndossada.Acumula_IS, _
                                                 oCoberturaEndossada.Acumula_Resseguro, lSeqCancEndossoSeg)

                    'Redu��o de IS e Pr�mio
                ElseIf oCoberturaEndossada.TpCobranca = "R" And ((CDbl(oCoberturaEndossada.ValIS) - CDbl(oCobertura.ValIS)) <> 0) Then

                    Call oSeguro.GravarCobertura(oDadosEndosso.PropostaId, "A", _
                                                 Val(txtNumEndosso.Text), oCoberturaEndossada.TpCoberturaId, _
                                                 oDadosEndosso.ProdutoExternoId, oDadosEndosso.produtoId, _
                                                 oDadosEndosso.RamoId, oCoberturaEndossada.CodObjSegurado, _
                                                 sTabelaEscolha, Format(mskDtEndosso, "yyyymmdd"), _
                                                 Format(sDtIniVigenciaSeg(oCoberturaEndossada.CodObjSegurado), "yyyymmdd"), _
                                                 MudaVirgulaParaPonto(Format(CDbl(oCobertura.ValIS) - CDbl(oCoberturaEndossada.ValIS), "000000000.00")), _
                                                 MudaVirgulaParaPonto(Format(0 & oCobertura.ValTaxa, "000000000.0000000")), _
                                                 MudaVirgulaParaPonto(Format((CDbl(oCobertura.ValIS) - CDbl(oCoberturaEndossada.ValIS)) * CDbl(oCoberturaEndossada.ValTaxa), "000000000.00")), _
                                                 MudaVirgulaParaPonto(Format(oCoberturaEndossada.PercDesconto, "000000000.0000000")), _
                                                 MudaVirgulaParaPonto(Format(oCoberturaEndossada.ValFranquia, "000000000.00")), _
                                                 MudaVirgulaParaPonto(Format(oCoberturaEndossada.FatFranquia, "000000000.0000000")), _
                                                 Trim(oCoberturaEndossada.DescFranquia), _
                                                 Format(mskDtEndosso.Text, "yyyymmdd"), _
                                                 cUserName, oDadosEndosso.TpEmissao, oCoberturaEndossada.Acumula_IS, _
                                                 oCoberturaEndossada.Acumula_Resseguro)

                    'Aumento de IS e Premio
                ElseIf oCoberturaEndossada.TpCobranca = "A" Then

                    Call oSeguro.GravarCobertura(oDadosEndosso.PropostaId, "A", _
                                                 Val(txtNumEndosso.Text), oCoberturaEndossada.TpCoberturaId, _
                                                 oDadosEndosso.ProdutoExternoId, oDadosEndosso.produtoId, _
                                                 oDadosEndosso.RamoId, oCoberturaEndossada.CodObjSegurado, _
                                                 sTabelaEscolha, Format(mskDtEndosso, "yyyymmdd"), _
                                                 Format(sDtIniVigenciaSeg(oCoberturaEndossada.CodObjSegurado), "yyyymmdd"), _
                                                 MudaVirgulaParaPonto(Format(CDbl(oCobertura.ValIS) + CDbl(oCoberturaEndossada.ValIS), "000000000.00")), _
                                                 MudaVirgulaParaPonto(Format(oCoberturaEndossada.ValTaxa, "000000000.0000000")), _
                                                 MudaVirgulaParaPonto(Format((CDbl(oCobertura.ValIS) + CDbl(oCoberturaEndossada.ValIS)) * CDbl(oCoberturaEndossada.ValTaxa), "000000000.00")), _
                                                 MudaVirgulaParaPonto(Format(oCoberturaEndossada.PercDesconto, "000000000.0000000")), _
                                                 MudaVirgulaParaPonto(Format(oCoberturaEndossada.ValFranquia, "000000000.00")), _
                                                 MudaVirgulaParaPonto(Format(oCoberturaEndossada.FatFranquia, "000000000.0000000")), _
                                                 Trim(oCoberturaEndossada.DescFranquia), _
                                                 Format(mskDtEndosso.Text, "yyyymmdd"), _
                                                 cUserName, oDadosEndosso.TpEmissao, oCoberturaEndossada.Acumula_IS, _
                                                 oCoberturaEndossada.Acumula_Resseguro)

                End If

                bEncontrou = True

            End If

        Next

        'Se n�o encontrou na cole��o de coberturas existentes, � uma nova cobertura
        If Not bEncontrou Then

            Call oSeguro.GravarCobertura(oDadosEndosso.PropostaId, "I", _
                                         Val(txtNumEndosso.Text), oCoberturaEndossada.TpCoberturaId, _
                                         oDadosEndosso.ProdutoExternoId, oDadosEndosso.produtoId, _
                                         oDadosEndosso.RamoId, oCoberturaEndossada.CodObjSegurado, _
                                         sTabelaEscolha, Format(mskDtEndosso, "yyyymmdd"), _
                                         Format(sDtIniVigenciaSeg(oCoberturaEndossada.CodObjSegurado), "yyyymmdd"), _
                                         MudaVirgulaParaPonto(Format(oCoberturaEndossada.ValIS, "000000000.00")), _
                                         MudaVirgulaParaPonto(Format(oCoberturaEndossada.ValTaxa, "000000000.0000000")), _
                                         MudaVirgulaParaPonto(Format(oCoberturaEndossada.ValPremio, "000000000.00")), _
                                         MudaVirgulaParaPonto(Format(oCoberturaEndossada.PercDesconto, "000000000.0000000")), _
                                         MudaVirgulaParaPonto(Format(oCoberturaEndossada.ValFranquia, "000000000.00")), _
                                         MudaVirgulaParaPonto(Format(oCoberturaEndossada.FatFranquia, "000000000.0000000")), _
                                         Trim(oCoberturaEndossada.DescFranquia), _
                                         Format(mskDtEndosso.Text, "yyyymmdd"), _
                                         cUserName, oDadosEndosso.TpEmissao, oCoberturaEndossada.Acumula_IS, _
                                         oCoberturaEndossada.Acumula_Resseguro)

        End If

    Next

    Call oConexao.CommitTransacao

    Call FecharConexao

    GravarCoberturas = True

    Set oCobertura = Nothing
    Set oCoberturaEndossada = Nothing
    Set oSeguro = Nothing

    Exit Function

Erro:
    Call TratarErro("GravarCoberturas", Me.name)
    Call FinalizarAplicacao
End Function

Private Sub IncluirEndossoISRestituicao()

    Dim oEmissao As Object
    Dim colCorretagem As New Collection
    Dim iEndossoId As Integer
    Const TP_OPERACAO_FINANCEIRA_RESTITUICAO = "03"

    On Error GoTo Erro

    Set colCorretagem = ObterCorretagem(TruncaDecimal(CCur(mskTotTarifa.Text) * oDadosEndosso.FatorCambio))

    Set oEmissao = CreateObject("SEGL0140.cls00172")

    With oDadosEndosso

        iEndossoId = oEmissao.EmitirEndosso(cUserName, gsSIGLASISTEMA, App.Title, _
                                            App.FileDescription, glAmbiente_id, _
                                            .ApoliceId, .SucursalSeguradoraId, _
                                            .SeguradoraCodSusep, .PropostaId, _
                                            .RamoId, 0, _
                                            TP_ENDOSSO_IMPORTANCIA_SEGURADA, "n", _
                                            .EstipulanteId, 0, 1, _
                                            0, _
                                            TP_OPERACAO_FINANCEIRA_RESTITUICAO, "", "", "", 0, _
                                            0, 0, 0, _
                                            0, _
                                            0, _
                                            0, _
                                            0, _
                                            "", 0, 0, _
                                            0, 0, mskDtEndosso.Text, mskDtEmissao.Text, _
                                            .BeneficiarioId, 0, 0, 0, 0, _
                                            0, colCorretagem, Nothing, 0, -1, "", "", txtDescEndosso.Text, "m")

        '*******************************************************
        'ALterado por Cleber da Stefanini data: 25/11/2004
        'Foi adicionado o par�metro "m", pois estava gerando agendamento e n�o estava sendo enviado para o BB.
        'A linha alterado foi a que esta acima e antes estava da forma abaixo:
        '  0, colCorretagem, Nothing, 0, -1, "", "", txtDescEndosso.Text)
        '*******************************************************


    End With

    Set oEmissao = Nothing
    Set colCorretagem = Nothing

    Exit Sub

Erro:
    Call TratarErro("IncluirEndossoISRestituicao", Me.name)
    Call FinalizarAplicacao
End Sub

Private Sub AplicarCobranca()

    Dim sDescricao As String

    On Error GoTo TrataErro

    If mskCambio.Visible And CDbl(mskCambio.Text) = 0 Then
        MsgBox "Valor do c�mbio n�o pode ser nulo."
        mskCambio.SetFocus
        Exit Sub
    End If

    sDescricao = Trim(txtDescEndosso.Text)

    ' Gerando o endosso selecionado '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

    Me.MousePointer = vbHourglass

    Select Case oDadosEndosso.TipoEndosso

    Case TP_ENDOSSO_COBRANCA

        Call IncluirEndossoCobranca(oDadosEndosso.ApoliceId, _
                                    oDadosEndosso.SucursalSeguradoraId, _
                                    oDadosEndosso.SeguradoraCodSusep, _
                                    oDadosEndosso.PropostaId, _
                                    oDadosEndosso.RamoId, _
                                    TruncaDecimal(CCur(mskTotLiquido.Text) * oDadosEndosso.FatorCambio), _
                                    oDadosEndosso.TipoEndosso, _
                                    TruncaDecimal(CCur(mskTotTarifa.Text) * oDadosEndosso.FatorCambio), _
                                    TruncaDecimal(CCur(grdPagamento.TextMatrix(1, 3)) * oDadosEndosso.FatorCambio), _
                                    TruncaDecimal(CCur(mskTotCom.Text) * oDadosEndosso.FatorCambio), _
                                    TruncaDecimal(CCur(mskValComEst.Text) * oDadosEndosso.FatorCambio), _
                                    TruncaDecimal(CCur(mskTotIof.Text) * oDadosEndosso.FatorCambio), _
                                    TruncaDecimal((CCur(mskTotCom.Text) * oDadosEndosso.PercIR) * oDadosEndosso.FatorCambio), _
                                    TruncaDecimal(CCur(mskTotDesc.Text) * oDadosEndosso.FatorCambio), _
                                    TruncaDecimal(CCur(mskTotJuros.Text) * oDadosEndosso.FatorCambio), _
                                    mskNumParcelas.Text, cboMoeda.ItemData(cboMoeda.ListIndex), oDadosEndosso.PremioMoedaId, _
                                    CCur(mskCambio.Text), 0, _
                                    mskDtEndosso.Text, mskDtEmissao.Text, _
                                    oDadosEndosso.PercIR, sDescricao, _
                                    oDadosEndosso.EstipulanteId, oDadosEndosso.PercProLabore, _
                                    oDadosEndosso.PercCorretagem)

    Case TP_ENDOSSO_IMPORTANCIA_SEGURADA

        Call IncluirEndossoISCobranca

        Call GravarCoberturas

    End Select

    Me.MousePointer = vbDefault

    ' Exibindo mensagem de sucesso ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

    Call MsgBox("Endosso realizado com sucesso." & vbNewLine & _
                "Proposta: " & oDadosEndosso.PropostaId & vbNewLine & _
                "Endosso : " & txtNumEndosso.Text, vbInformation)

    ' Retornando ao formul�rio de sele��o de propostas ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

    Call sair

    Exit Sub

TrataErro:

    Call TratarErro("AplicarCobranca", Me.name)
    Call FinalizarAplicacao

End Sub

Private Sub IncluirEndossoISCobranca()

    Dim oEmissao As Object
    Dim colCorretagem As New Collection
    Dim colAgendamento As New Collection
    Const TP_OPERACAO_FINANCEIRA_COBRANCA = "01"
    Dim cCustoApolice As Currency
    Dim iIndice As Integer

    On Error GoTo Erro

    cCustoApolice = 0
    For iIndice = 1 To grdPagamento.Rows - 1
        cCustoApolice = cCustoApolice + CCur(grdPagamento.TextMatrix(iIndice, 3))
    Next

    Set colCorretagem = ObterCorretagem(TruncaDecimal(CCur(mskTotTarifa.Text) * oDadosEndosso.FatorCambio))

    Set colAgendamento = ObterColecaoAgendamento

    Set oEmissao = CreateObject("SEGL0140.cls00172")

    With oDadosEndosso

        Call oEmissao.EmitirEndosso(cUserName, gsSIGLASISTEMA, App.Title, _
                                    App.FileDescription, glAmbiente_id, _
                                    .ApoliceId, .SucursalSeguradoraId, _
                                    .SeguradoraCodSusep, .PropostaId, _
                                    .RamoId, CCur(mskTotLiquido.Text), _
                                    TP_ENDOSSO_IMPORTANCIA_SEGURADA, "n", _
                                    .EstipulanteId, .PercProLabore, _
                                    mskNumParcelas.Text, _
                                    TruncaDecimal(CCur(mskTotTarifa.Text) * oDadosEndosso.FatorCambio), _
                                    TP_OPERACAO_FINANCEIRA_COBRANCA, "", "", "", cCustoApolice, _
                                    TruncaDecimal(CCur(mskTotCom.Text) * oDadosEndosso.FatorCambio), 0, _
                                    TruncaDecimal(CCur(mskValComEst.Text) * oDadosEndosso.FatorCambio), _
                                    TruncaDecimal(CCur(mskTotIof.Text) * oDadosEndosso.FatorCambio), _
                                    TruncaDecimal((CCur(mskTotCom.Text) * oDadosEndosso.PercIR) * oDadosEndosso.FatorCambio), _
                                    TruncaDecimal(CCur(mskTotDesc.Text) * oDadosEndosso.FatorCambio), _
                                    TruncaDecimal(CCur(mskTotJuros.Text) * oDadosEndosso.FatorCambio), _
                                    "", cboMoeda.ItemData(cboMoeda.ListIndex), cboMoeda.ItemData(cboMoeda.ListIndex), _
                                    CCur(mskCambio.Text), 0, mskDtEndosso.Text, mskDtEmissao.Text, _
                                    0, .PercIR, 0, .PercCorretagem, 0, _
                                    0, colCorretagem, colAgendamento, 0, -1, "", "", txtDescEndosso.Text)

    End With

    Set oEmissao = Nothing
    Set colCorretagem = Nothing
    Set colAgendamento = Nothing

    Exit Sub

Erro:
    Call TratarErro("IncluirEndossoISCobranca", Me.name)
    Call FinalizarAplicacao
End Sub

Private Function ObterCorretagem(ByVal cValPremioTarifa As Currency) As Collection

    Dim colCorretagem As New Collection
    Dim oCorretagem As Object
    Dim iCont As Integer

    On Error GoTo TrataErro

    For iCont = 1 To grdCorretagem.Rows - 1

        Set oCorretagem = CreateObject("SEGL0140.cls00149")

        oCorretagem.Corretor_ID = Val(Replace(grdCorretagem.TextMatrix(iCont, 0), "-", ""))
        oCorretagem.PercComissao = CCur(grdCorretagem.TextMatrix(iCont, 2))
        oCorretagem.Sucursal = Trim(grdCorretagem.TextMatrix(iCont, 3))
        oCorretagem.Tipo = Trim(grdCorretagem.TextMatrix(iCont, 4))
        oCorretagem.Val_Comissao = TruncaDecimal(cValPremioTarifa * (oCorretagem.PercComissao / 100))
        oCorretagem.Val_IR = TruncaDecimal(oCorretagem.Val_Comissao * oDadosEndosso.PercIR)
        colCorretagem.Add oCorretagem

        Set oCorretagem = Nothing

    Next

    Set ObterCorretagem = colCorretagem

    Set colCorretagem = Nothing

    Exit Function

TrataErro:
    Call TratarErro("ObterCorretagem", Me.name)
    Call FinalizarAplicacao
End Function

Private Sub Form_Unload(Cancel As Integer)

    Call LimparCoberturas

End Sub


Private Sub grdBeneficiario_Click()

    Dim oBeneficiario As Object
    Dim lCont As Long

    If grdBeneficiario.Rows > 1 Then

        txtNomeBenef(0).Text = grdBeneficiario.TextMatrix(grdBeneficiario.RowSel, 1)

        Set oBeneficiario = colBeneficiario.Item(grdBeneficiario.TextMatrix(grdBeneficiario.RowSel, 0))

        txtNomeBenef(1).Text = Trim(oBeneficiario.sEndereco)
        txtBairroBenef(0).Text = Trim(oBeneficiario.sBairro)
        txtBairroBenef(2).Text = Trim(oBeneficiario.sEstado)

        If Trim(oBeneficiario.sCep) <> "" Then
            txtCepBenef.Text = oBeneficiario.sCep
        Else
            txtCepBenef.Text = "__.___-___"
        End If

        txtBairroBenef(1).Text = Trim(oBeneficiario.sPais)

        cboCidadeBenef.ListIndex = -1

        For lCont = 0 To cboCidadeBenef.ListCount - 1
            If Left(UCase(cboCidadeBenef.List(lCont)), Len(cboCidadeBenef.List(lCont)) - 5) = UCase(Trim(oBeneficiario.sCidade)) Then
                cboCidadeBenef.ListIndex = lCont
                Exit For
            End If
        Next

        If oBeneficiario.sOperacao <> "I" Then
            'cmdAdicionarBenef.Enabled = False
            cmdAlterarBenef.Enabled = True
            cmdRemoverBenef.Enabled = True
        Else
            'cmdAdicionarBenef.Enabled = False
            cmdAlterarBenef.Enabled = False
            cmdRemoverBenef.Enabled = False
        End If

        Set oBeneficiario = Nothing

        iAuxLinha = grdBeneficiario.RowSel

    End If

End Sub

Private Sub grdCoberturas_Click()

    Dim iIndice As Integer

    On Error GoTo Erro

    'Trata para grid vazio
    If grdCoberturas.Rows <= 1 Then Exit Sub

    'Habilito o tipo de movimenta��o para carregar a combo de coberturas
    grdCoberturas.Col = 11

    If grdCoberturas.Text = "R" Then
        optRedIS.Value = True
    ElseIf grdCoberturas.Text = "A" Then
        optAumIS.Value = True
    ElseIf grdCoberturas.Text = "N" Then
        optNovCob.Value = True
    End If

    For iIndice = 0 To cboCoberturaAfetada.ListCount - 1
        If cboCoberturaAfetada.ItemData(iIndice) = CInt(grdCoberturas.TextMatrix(grdCoberturas.Row, 0)) Then

            cboCoberturaAfetada.ListIndex = iIndice
            mskImpSeg.Text = Format(CDbl(grdCoberturas.TextMatrix(grdCoberturas.Row, 2)), "###########0.00")
            mskTaxaTarifa.Text = Format(CDbl(grdCoberturas.TextMatrix(grdCoberturas.Row, 3)), "###########0.0000000")
            mskPerDesc.Text = grdCoberturas.TextMatrix(grdCoberturas.Row, 4)
            mskPremioTarifa.Text = Format(CDbl(grdCoberturas.TextMatrix(grdCoberturas.Row, 5)), "###########0.00")
            txtTextoFranquia.Text = grdCoberturas.TextMatrix(grdCoberturas.Row, 6)
            mskLimFra.Text = Format(CDbl(grdCoberturas.TextMatrix(grdCoberturas.Row, 7)), "###########0.00")
            mskFatFranquia.Text = Format(CDbl(grdCoberturas.TextMatrix(grdCoberturas.Row, 8)), "###########0.00000")

            Exit For

        End If
    Next

    cmdBensAdicionar.Enabled = False
    cmdBensAlterar.Enabled = True

    If grdCoberturas.Rows > 1 Then
        cmdBensRemover.Enabled = True
    Else
        cmdBensRemover.Enabled = False
    End If

    Call mskTaxaTarifa_LostFocus

    Exit Sub

Erro:
    Call TratarErro("grdCoberturas_Click", Me.name)
    Call FinalizarAplicacao
End Sub

Private Sub grdCorretagem_Click()

    Dim iCont As Integer

    If grdCorretagem.Rows = 2 And grdCorretagem.RowSel = 1 Then
        If grdCorretagem.TextMatrix(grdCorretagem.RowSel, 0) = "" Then
            Exit Sub
        End If
    ElseIf grdCorretagem.Rows = 1 Then
        Exit Sub
    End If

    mskPercCorretagemCob.Text = grdCorretagem.TextMatrix(grdCorretagem.RowSel, 2)

    For iCont = 0 To cboCorretorCob.ListCount - 1
        If Format(cboCorretorCob.ItemData(iCont), "000000-000") = grdCorretagem.TextMatrix(grdCorretagem.RowSel, 0) Then
            cboCorretorCob.ListIndex = iCont
            Exit For
        End If
    Next

    For iCont = 0 To cboSucursalCorretorCob.ListCount - 1
        If cboSucursalCorretorCob.List(iCont) = grdCorretagem.TextMatrix(grdCorretagem.RowSel, 3) Then
            cboSucursalCorretorCob.ListIndex = iCont
            Exit For
        End If
    Next

    For iCont = 0 To cboTpCorretor.ListCount - 1
        If cboTpCorretor.List(iCont) = grdCorretagem.TextMatrix(grdCorretagem.RowSel, 3) Then
            cboTpCorretor.ListIndex = iCont
            Exit For
        End If
    Next

    cmdCorretorCobAdicionar.Enabled = False

    If grdCorretagem.Rows > 1 Then
        cmdCorretorCobRemover.Enabled = True
        cmdCorretorCobAlterar.Enabled = True
    Else
        cmdCorretorCobRemover.Enabled = False
        cmdCorretorCobAlterar.Enabled = False
    End If

    mskPercCorretagemCob.SetFocus
    iAuxLinha = grdCorretagem.RowSel

End Sub

Private Function ObterColecaoAgendamento() As Collection

    Dim colAgendamento As New Collection
    Dim oAgendamento As Object
    Dim oFinanceiro As Object

    Dim iCont As Integer
    Dim iNumCobranca As Integer
    Dim sNossoNumero As String
    Dim sNossoNumeroDV As String

    On Error GoTo TrataErro

    Set oFinanceiro = CreateObject("SEGL0026.cls00125")

    'Obtendo o n�mero da pr�xima cobran�a ''''''''''''''''''''''''''''''''''''''''''

    iNumCobranca = oFinanceiro.ObterProximaCobranca(App.Title, _
                                                    App.FileDescription, _
                                                    gsSIGLASISTEMA, _
                                                    glAmbiente_id, _
                                                    oDadosEndosso.PropostaId)
    '*******************************************************************************
    '*************Alterado por Cleber da Stefanini - data: 26/11/2004***************
    'Pois estava gerando faturas com o mesmo nosso_numero, portanto foi colocado a
    'chamada abaixo dentro do FOR logo abaixo.
    '*******************************************************************************
    '''Obtendo NossoNumero e NossoNumeroDV '''''''''''''''''''''''''''''''''''''''''''

    'Call oFinanceiro.ObterNossoNumero(cUserName, _
     gsSIGLASISTEMA, _
     App.Title, _
     App.FileDescription, _
     glAmbiente_id, _
     oDadosEndosso.ProdutoId, _
     oDadosEndosso.RamoId, _
     sNossoNumero, _
     sNossoNumeroDV)

    '''Set oFinanceiro = Nothing
    '*******************************************************************************

    For iCont = 1 To grdPagamento.Rows - 1

        '*******************************************************************************
        'Obtendo NossoNumero e NossoNumeroDV '''''''''''''''''''''''''''''''''''''''''''
        Call oFinanceiro.ObterNossoNumero(cUserName, _
                                          gsSIGLASISTEMA, _
                                          App.Title, _
                                          App.FileDescription, _
                                          glAmbiente_id, _
                                          oDadosEndosso.produtoId, _
                                          oDadosEndosso.RamoId, _
                                          sNossoNumero, _
                                          sNossoNumeroDV)
        '*******************************************************************************

        Set oAgendamento = CreateObject("SEGL0140.cls00148")

        oAgendamento.NumCobranca = iNumCobranca
        oAgendamento.ValParcela = TruncaDecimal(CCur(grdPagamento.TextMatrix(iCont, 8)) * oDadosEndosso.FatorCambio)
        oAgendamento.QtdParcelas = Val(mskNumParcelas.Text)
        oAgendamento.valIOF = TruncaDecimal(CCur(grdPagamento.TextMatrix(iCont, 6)) * oDadosEndosso.FatorCambio)
        oAgendamento.ValJuros = TruncaDecimal(CCur(grdPagamento.TextMatrix(iCont, 5)) * oDadosEndosso.FatorCambio)
        oAgendamento.valDesconto = TruncaDecimal(CCur(grdPagamento.TextMatrix(iCont, 4)) * oDadosEndosso.FatorCambio)
        oAgendamento.NumParcelaEndosso = iCont
        oAgendamento.ValComissao = TruncaDecimal(CCur(grdPagamento.TextMatrix(iCont, 7)) * oDadosEndosso.FatorCambio)
        oAgendamento.ValIRComissaoCorretor = TruncaDecimal((CCur(grdPagamento.TextMatrix(iCont, 7))) * oDadosEndosso.PercIR * oDadosEndosso.FatorCambio)
        oAgendamento.ValComissaoEstipulante = TruncaDecimal((CCur(mskValComEst.Text) / Val(mskNumParcelas.Text)) * oDadosEndosso.FatorCambio)
        oAgendamento.NossoNumero = sNossoNumero
        oAgendamento.NossoNumeroDV = sNossoNumeroDV
        oAgendamento.Situacao = "p"
        oAgendamento.DtAgendamento = grdPagamento.TextMatrix(iCont, 1)
        oAgendamento.AnoMes = Mid(grdPagamento.TextMatrix(iCont, 1), 7, 4) & Mid(grdPagamento.TextMatrix(iCont, 1), 4, 2)

        colAgendamento.Add oAgendamento

        Set oAgendamento = Nothing

        iNumCobranca = iNumCobranca + 1

    Next
    '***********************************************************************
    'Continua��o da Altera��o feita por Cleber da Stefanini data: 29/11/2004
    'Antes nao tinha essa linha aqui, ela era mais para cima.
    Set oFinanceiro = Nothing
    '***********************************************************************

    Set ObterColecaoAgendamento = colAgendamento

    Set colAgendamento = Nothing

    Exit Function

TrataErro:

    Call TratarErro("ObterColecaoAgendamento", Me.name)
    Call FinalizarAplicacao

End Function

Private Sub grdEstipulante_Click()

    Dim iCont As Integer

    If grdEstipulante.Rows = 2 And grdEstipulante.RowSel = 1 Then
        If grdEstipulante.TextMatrix(grdEstipulante.RowSel, 0) = "" Then Exit Sub
    ElseIf grdEstipulante.Rows = 1 Then Exit Sub
    End If

    mskPercProLabore.Text = grdEstipulante.TextMatrix(grdEstipulante.RowSel, 2)

    For iCont = 0 To CboEstipulante.ListCount - 1
        If CboEstipulante.ItemData(iCont) = grdEstipulante.TextMatrix(grdEstipulante.RowSel, 0) Then
            CboEstipulante.ListIndex = iCont
            Exit For
        End If
    Next

    cmdEstipulanteAdicionar.Enabled = False

    If grdEstipulante.Rows > 1 Then
        cmdEstipulanteRemover.Enabled = True
        cmdEstipulanteAlterar.Enabled = True
    Else
        cmdEstipulanteRemover.Enabled = False
        cmdEstipulanteAlterar.Enabled = False
    End If

    mskPercProLabore.SetFocus
    iAuxLinha = grdEstipulante.RowSel

End Sub

Private Sub grdEstipulanteEst_Click()

    If grdEstipulanteEst.Rows > 1 Then

        txtEstipulante.Text = grdEstipulanteEst.TextMatrix(grdEstipulanteEst.RowSel, 0)
        txtNomeEstipulante(0).Text = grdEstipulanteEst.TextMatrix(grdEstipulanteEst.RowSel, 1)
        mskPercEstipulante.Text = grdEstipulanteEst.TextMatrix(grdEstipulanteEst.RowSel, 2)

        cmdEstAdicionar.Enabled = False
        cmdEstAlterar.Enabled = True
        cmdEstRemover.Enabled = True

        iAuxLinha = grdEstipulante.RowSel

    End If

End Sub

Private Sub grdPagamento_Click()

    If grdPagamento.Rows > 1 Then

        lblDtVcto.Caption = "Vencimento:"

        iAuxLinha = grdPagamento.RowSel

        If grdPagamento.Row = 1 Then
            mskCustoEndossoCob.Locked = False
            mskCustoEndossoCob.Text = Format(CDbl(grdPagamento.TextMatrix(grdPagamento.Row, 3)), "###.00")
        Else
            mskCustoEndossoCob.Locked = True
            mskCustoEndossoCob.Text = "0,00"
        End If

        lblParcAlt.Visible = True
        lblNumParcAlt.Visible = True
        lblNumParcAlt.Caption = grdPagamento.Row

        mskDtPagamento.Text = Format(grdPagamento.TextMatrix(grdPagamento.Row, 1), "dd/mm/yyyy")
        mskValPremioTarifa.Text = Format(CCur(grdPagamento.TextMatrix(grdPagamento.Row, 2)), "###########0.00")
        mskValDesc.Text = Format(CCur(grdPagamento.TextMatrix(grdPagamento.Row, 4)), "##0.00")
        mskValJuros.Text = Format(CCur(grdPagamento.TextMatrix(grdPagamento.Row, 5)), "##0.00")
        mskValIOF.Text = Format(CCur(grdPagamento.TextMatrix(grdPagamento.Row, 6)), "###########0.00")
        mskValCom.Text = Format(CCur(grdPagamento.TextMatrix(grdPagamento.Row, 7)), "##0.00")
        mskValLiquido.Text = Format(CCur(grdPagamento.TextMatrix(grdPagamento.Row, 8)), "###########0.00")

        mskTotTarifa.Text = Format(CCur(mskTotTarifa.Text) + CCur(mskValPremioTarifa.Text) - CCur(grdPagamento.TextMatrix(grdPagamento.Row, 2)), "###########0.00")
        mskTotDesc.Text = Format(CCur(mskTotDesc.Text) + CCur(mskValDesc.Text) - CCur(grdPagamento.TextMatrix(grdPagamento.Row, 4)), "###########0.00")
        mskTotJuros.Text = Format(CCur(mskTotJuros.Text) + CCur(mskValJuros.Text) - CCur(grdPagamento.TextMatrix(grdPagamento.Row, 5)), "###########0.00")
        mskTotIof.Text = Format(CCur(mskTotIof.Text) + CCur(mskValIOF.Text) - CCur(grdPagamento.TextMatrix(grdPagamento.Row, 6)), "###########0.00")
        mskTotCom.Text = Format(CCur(mskTotCom.Text) + CCur(mskValCom.Text) - CCur(grdPagamento.TextMatrix(grdPagamento.Row, 7)), "###########0.00")
        mskTotLiquido.Text = Format(CCur(mskTotLiquido.Text) + CCur(mskValLiquido.Text) - CCur(grdPagamento.TextMatrix(grdPagamento.Row, 8)), "###########0.00")

        cmdPagAlterar.Enabled = True

    End If

End Sub

Private Sub grdParcelasReativacao_Click()

    If grdParcelasReativacao.Rows > 1 Then

        mskValParcelaReativacao.Text = grdParcelasReativacao.TextMatrix(grdParcelasReativacao.Row, 1)
        mskDtParcelaReativacao.Text = grdParcelasReativacao.TextMatrix(grdParcelasReativacao.Row, 2)

    End If

End Sub

Private Sub grdReintegracao_Click(Index As Integer)

'On Error GoTo TrataErro

    Dim OValores As Object
    Dim rSValores As Recordset
    Dim StrSQL As String

    Dim Area As String
    Dim Custeio As String
    Dim Cobertura As Integer
    Dim val_is As String


    'Reintegra��o de IS
    If Index = 1 Then

        If grdReintegracao(1).Rows < 2 Then
            Exit Sub
        End If

        Set OValores = CreateObject("SEGL0026.cls00126")

        Set rSValores = OValores.Consulta_Area_Segurada(gsSIGLASISTEMA, _
                                                        App.Title, _
                                                        App.FileDescription, _
                                                        glAmbiente_id, _
                                                        CLng(grdReintegracao(1).TextMatrix(grdReintegracao(1).Row, 0)), _
                                                        CInt(grdReintegracao(1).TextMatrix(grdReintegracao(1).Row, 21)), _
                                                        txtProduto.Text)


        While Not rSValores.EOF

            If rSValores!Tipo = "Area" Then
                TxtArea(16).Text = Trim(rSValores!texto_resposta)
            ElseIf rSValores!Tipo = "fator_cobertura" Then
                TxtArea(31).Text = Format(Trim(rSValores!texto_resposta), "##0.00")
                TxtArea(31).Text = Format(TxtArea(31).Text, "##0.00")
            ElseIf rSValores!Tipo = "Area_Atual" Then
                If Not IsNull(rSValores!texto_resposta) Then
                    TxtArea(17).Text = Trim(rSValores!texto_resposta)
                Else
                    TxtArea(17).Text = TxtArea(16).Text
                End If
            ElseIf rSValores!Tipo = "Custeio" Then
                TxtArea(15).Text = Trim(rSValores!texto_resposta)
            End If

            rSValores.MoveNext
        Wend


        rSValores.Close

        If CLng(txtProduto.Text) = 1152 Then
            Cobertura = 339
        ElseIf CLng(txtProduto.Text) = 1204 Then
            Cobertura = 834
        End If


        Set OValores = CreateObject("SEGL0026.cls00126")

        'If Cobertura = "834" Then
        Set rSValores = OValores.Consulta_IS(gsSIGLASISTEMA, _
                                             App.Title, _
                                             App.FileDescription, _
                                             glAmbiente_id, _
                                             CLng(grdReintegracao(1).TextMatrix(grdSinistros.Row, 0)), _
                                             Cobertura, 1)
        TxtArea(32).Text = Format(Trim(rSValores!val_is), "##0.00")
        TxtArea(32).Text = Replace(TxtArea(32).Text, ".", "")

        '1152
        If CLng(txtProduto.Text) = 1152 Then
            Set rSValores = OValores.Consulta_IS_Anterior_Reducao(gsSIGLASISTEMA, _
                                                                  App.Title, _
                                                                  App.FileDescription, _
                                                                  glAmbiente_id, _
                                                                  CLng(grdReintegracao(1).TextMatrix(grdSinistros.Row, 0)), _
                                                                  Cobertura, 1)

            TxtArea(19).Text = Format(Trim(rSValores!val_is), "##0.00")
            TxtArea(19).Text = Replace(TxtArea(19).Text, ".", "")
            '           TxtArea(32).Text = Format(Trim(rSValores!val_is), "##0.00")
            '           TxtArea(32).Text = Replace(TxtArea(32).Text, ".", "")

        End If


        rSValores.Close

        'TxtArea(32).SetFocus

        'Adequa��o �rea
    ElseIf Index = 0 Then


        Set OValores = CreateObject("SEGL0026.cls00126")

        Set rSValores = OValores.Consultar_Valores_Adequacao(gsSIGLASISTEMA, _
                                                             App.Title, _
                                                             App.FileDescription, _
                                                             glAmbiente_id, _
                                                             CLng(grdReintegracao(0).TextMatrix(grdReintegracao(0).Row, 0)), _
                                                             1, txtProduto.Text)


        While Not rSValores.EOF

            If Not IsNull(rSValores!texto_resposta) Then

                If rSValores!Tipo = "Area" Then
                    TxtArea(10).Text = Format(Trim(rSValores!texto_resposta), "##0.00")
                ElseIf rSValores!Tipo = "Area_Atual" Then
                    If Not IsNull(rSValores!texto_resposta) And Not rSValores!texto_resposta = Empty Then
                        TxtArea(9).Text = Format(Trim(rSValores!texto_resposta), "##0.00")
                    Else
                        TxtArea(9).Text = TxtArea(10).Text
                    End If
                    ''fadao
                ElseIf rSValores!Tipo = "valor_custeio" Then
                    TxtArea(8).Text = Trim(rSValores!texto_resposta)
                ElseIf rSValores!Tipo = "fator_cobertura" Then
                    TxtArea(23).Text = Format(Trim(rSValores!texto_resposta), "##0.00")
                    TxtArea(23).Text = Format(TxtArea(23).Text, "##0.00")
                ElseIf rSValores!Tipo = "produtividade_municipio" Then
                    TxtArea(24).Text = TrocaPontoPorVirgula(rSValores!texto_resposta)
                ElseIf rSValores!Tipo = "produtividade_esperada" Then
                    TxtArea(25).Text = TrocaPontoPorVirgula(rSValores!texto_resposta)
                ElseIf rSValores!Tipo = "preco_base" Then
                    TxtArea(26).Text = Trim(rSValores!texto_resposta)
                ElseIf rSValores!Tipo = "nivel_cobertura" Then
                    TxtArea(27).Text = TrocaPontoPorVirgula(rSValores!texto_resposta)
                    'txtArea(27).Text = Format(Trim(rSValores!texto_resposta), "##0.00")
                End If


            End If

            rSValores.MoveNext
        Wend

        rSValores.Close

        'Busca as importancias seguradas das coberturas b�sicas
        If CLng(txtProduto.Text) = 1152 Then
            Cobertura = 339
        ElseIf CLng(txtProduto.Text) = 1204 Then
            Cobertura = 835
        End If

        Set OValores = CreateObject("SEGL0026.cls00126")

        Set rSValores = OValores.Consulta_IS(gsSIGLASISTEMA, _
                                             App.Title, _
                                             App.FileDescription, _
                                             glAmbiente_id, _
                                             CLng(grdReintegracao(0).TextMatrix(grdReintegracao(0).Row, 0)), _
                                             Cobertura, 1)

        'Cobertura B�sica
        TxtArea(22).Text = Format(rSValores!val_is, "##0.00")

        rSValores.Close

        'Cobertura adicional
        If CLng(txtProduto.Text) = 1204 Then

            Set OValores = CreateObject("SEGL0026.cls00126")

            Set rSValores = OValores.Consulta_IS(gsSIGLASISTEMA, _
                                                 App.Title, _
                                                 App.FileDescription, _
                                                 glAmbiente_id, _
                                                 CLng(grdReintegracao(0).TextMatrix(grdReintegracao(0).Row, 0)), _
                                                 834, 1)

            'Cobertura B�sica
            TxtArea(28).Text = Format(rSValores!val_is, "##0.00")

            rSValores.Close

        End If


        If TxtArea(8).Text = "" Then TxtArea(8).Text = 0
        If TxtArea(24).Text = "" Then TxtArea(24).Text = 0
        If TxtArea(25).Text = "" Then TxtArea(25).Text = 0
        If TxtArea(26).Text = "" Then TxtArea(26).Text = 0
        If TxtArea(27).Text = "" Then TxtArea(27).Text = 0
        If TxtArea(23).Text = "" Then TxtArea(23).Text = 0

        'Adequa��o de Produtividade
    ElseIf Index = 2 Then

        Set OValores = CreateObject("SEGL0026.cls00126")

        Set rSValores = OValores.Consultar_Valores_Adequacao(gsSIGLASISTEMA, _
                                                             App.Title, _
                                                             App.FileDescription, _
                                                             glAmbiente_id, _
                                                             CLng(grdReintegracao(Index).TextMatrix(grdReintegracao(Index).Row, 0)), _
                                                             1, txtProduto.Text)

        While Not rSValores.EOF

            If Not IsNull(rSValores!texto_resposta) Then

                If rSValores!Tipo = "Area" Then
                    TxtArea(49).Text = Trim(rSValores!texto_resposta)
                ElseIf rSValores!Tipo = "Area_Atual" Then
                    If Not IsNull(rSValores!texto_resposta) And Not rSValores!texto_resposta = Empty Then
                        TxtArea(50).Text = Trim(rSValores!texto_resposta)
                    Else
                        TxtArea(50).Text = TxtArea(49).Text
                    End If
                ElseIf rSValores!Tipo = "valor_custeio" Then
                    TxtArea(51).Text = Trim(rSValores!texto_resposta)
                ElseIf rSValores!Tipo = "fator_cobertura" Then
                    TxtArea(48).Text = Format(Trim(rSValores!texto_resposta), "##0.00")
                    TxtArea(48).Text = Format(TxtArea(48).Text, "##0.00")
                ElseIf rSValores!Tipo = "produtividade_municipio" Then
                    TxtArea(47).Text = Trim(rSValores!texto_resposta)
                ElseIf rSValores!Tipo = "produtividade_esperada" Then
                    TxtArea(20).Text = Trim(rSValores!texto_resposta)
                ElseIf rSValores!Tipo = "preco_base" Then
                    TxtArea(45).Text = Trim(rSValores!texto_resposta)
                ElseIf rSValores!Tipo = "nivel_cobertura" Then
                    TxtArea(13).Text = Trim(rSValores!texto_resposta)
                End If

            End If

            rSValores.MoveNext
        Wend


        rSValores.Close

        'Busca as importancias seguradas das coberturas b�sicas - fadao
        If CLng(txtProduto.Text) = 1152 Then
            Cobertura = 339
        ElseIf CLng(txtProduto.Text) = 1204 Then
            Cobertura = 835
        End If

        Set OValores = CreateObject("SEGL0026.cls00126")

        Set rSValores = OValores.Consulta_IS(gsSIGLASISTEMA, _
                                             App.Title, _
                                             App.FileDescription, _
                                             glAmbiente_id, _
                                             CLng(grdReintegracao(Index).TextMatrix(grdReintegracao(0).Row, 0)), _
                                             Cobertura, 1)

        'Cobertura B�sica
        'IM01298830 - Confitec - Leandro Amaral - Ajuste no tratamento da consulta, para evitar erro ao n�o trazer registro.
        'TxtArea(30).Text = Format(rSValores!val_is, "##0.00")

        Me.TxtArea(30).Text = "0,00"
        
        If Not rSValores.EOF Then
            Me.TxtArea(30).Text = Format(rSValores!val_is, "##0.00")
        End If
        
        rSValores.Close
        'IM01298830 - Confitec - Leandro Amaral - Ajuste no tratamento da consulta, para evitar erro ao n�o trazer registro.
        
        'Adequa��o de produtividade
    ElseIf Index = 2 Then


        Set OValores = CreateObject("SEGL0026.cls00126")

        Set rSValores = OValores.Consultar_Valores_Adequacao(gsSIGLASISTEMA, _
                                                             App.Title, _
                                                             App.FileDescription, _
                                                             glAmbiente_id, _
                                                             CLng(grdReintegracao(2).TextMatrix(grdReintegracao(2).Row, 0)), _
                                                             1, txtProduto.Text)


        While Not rSValores.EOF

            If Not IsNull(rSValores!texto_resposta) Then

                If rSValores!Tipo = "Area" Then
                    TxtArea(49).Text = Format(Trim(rSValores!texto_resposta), "##0.00")
                ElseIf rSValores!Tipo = "Area_Atual" Then
                    If Not IsNull(rSValores!texto_resposta) Then
                        TxtArea(50).Text = Format(Trim(rSValores!texto_resposta), "##0.00")
                    Else
                        TxtArea(50).Text = TxtArea(49).Text
                    End If
                    ''fadao
                ElseIf rSValores!Tipo = "valor_custeio" Then
                    TxtArea(51).Text = Trim(rSValores!texto_resposta)
                ElseIf rSValores!Tipo = "fator_cobertura" Then
                    TxtArea(48).Text = Format(Trim(rSValores!texto_resposta), "##0.00")
                    TxtArea(48).Text = Format(TxtArea(48).Text, "##0.00")
                ElseIf rSValores!Tipo = "produtividade_municipio" Then
                    TxtArea(24).Text = Trim(rSValores!texto_resposta)
                ElseIf rSValores!Tipo = "produtividade_esperada" Then
                    TxtArea(20).Text = TrocaPontoPorVirgula(rSValores!texto_resposta)
                ElseIf rSValores!Tipo = "preco_base" Then
                    TxtArea(45).Text = Trim(rSValores!texto_resposta)
                ElseIf rSValores!Tipo = "nivel_cobertura" Then
                    TxtArea(13).Text = TrocaPontoPorVirgula(rSValores!texto_resposta)
                    'txtArea(27).Text = Format(Trim(rSValores!texto_resposta), "##0.00")
                End If


            End If

            rSValores.MoveNext
        Wend


        rSValores.Close

        'RESTITUI��O DE SUBSIDIO
    ElseIf Index = 3 Then

        If grdReintegracao(3).TextMatrix(grdReintegracao(3).Row, 3) = "MAPA" _
           Or grdReintegracao(3).TextMatrix(grdReintegracao(3).Row, 3) = "Estadual" Then

            If grdReintegracao(3).TextMatrix(grdReintegracao(3).Row, 5) <> "" Or _
               grdReintegracao(3).TextMatrix(grdReintegracao(3).Row, 5) <> Null Then
                TxtArea(44).Text = grdReintegracao(3).TextMatrix(grdReintegracao(3).Row, 5)
                TxtArea(44).Text = Format(Trim(TxtArea(44).Text), "##0.00")
            Else
                TxtArea(44).Text = ""
            End If
        Else
            TxtArea(44).Text = ""
        End If


    End If

    Exit Sub

End Sub

Private Sub grdSubGrupos_Click()

    If grdSubGrupos.Row > 0 Then
        oDadosEndosso.SubGrupoId = Val(grdSubGrupos.TextMatrix(grdSubGrupos.Row, 0))
        Call CarregarDadosFaturasCanceladas
    End If

End Sub

Private Sub GridCorretagem_Click()

    Dim oCorretor As Object

    If GridCorretagem.Rows > 1 Then

        TxtCodCorretor(0).Text = GridCorretagem.RowData(GridCorretagem.RowSel)
        TxtCodCorretor(1).Text = GridCorretagem.TextMatrix(GridCorretagem.RowSel, 3)
        TxtSucCorretor.Text = GridCorretagem.TextMatrix(GridCorretagem.RowSel, 4)
        TxtTpCorretor.Text = IIf(GridCorretagem.TextMatrix(GridCorretagem.RowSel, 1) = "J", "1", "2")
        mskPercCorretagem.Text = Format(CDbl(GridCorretagem.TextMatrix(GridCorretagem.RowSel, 2)), "0.0000")

        If Not CboSubGrupo.Enabled Then
            Set oCorretor = colCorretor.Item(Replace(GridCorretagem.RowData(GridCorretagem.Row), "-", ""))
        Else
            Set oCorretor = colSubGrupo(CboSubGrupo.ItemData(CboSubGrupo.ListIndex)).Corretores.Item(Replace(GridCorretagem.RowData(GridCorretagem.Row), "-", ""))
        End If

        If oCorretor.Operacao <> "I" Then
            cmdCorretorAdicionar.Enabled = False
            cmdCorretorAlterar.Enabled = True
            cmdCorretorRemover.Enabled = True
        Else
            cmdCorretorAdicionar.Enabled = False
            cmdCorretorAlterar.Enabled = False
            cmdCorretorRemover.Enabled = False
        End If

        Set oCorretor = Nothing

        iAuxLinha = GridCorretagem.RowSel

    End If

End Sub




Private Sub mskCambio_LostFocus()

    oDadosEndosso.FatorCambio = mskCambio.Text

    If optCobRes.Value Then
        Call mskValPremioTarifa_LostFocus
    End If

End Sub



Private Sub mskCustoEndossoCob_LostFocus()

    If mskValPremioTarifa.Text <> 0 Then
        Call CalcularValoresCobranca
    End If

End Sub

Private Sub CalcularValoresCobranca()

    Dim oDadosBasicos As Object

    Dim cValCustoEndosso As Currency
    Dim cValCustoEndossoInformado As Currency
    Dim cValCobranca As Currency

    Dim rs As Recordset

    On Error GoTo TrataErro

    cValCustoEndossoInformado = mskCustoEndossoCob.Text

    cValCobranca = mskValPremioTarifa.Text

    If oDadosEndosso.CriticaCustoRamo Then
        If oDadosEndosso.valMaxCustoEndosso < Val(TrocaVirgulaPorPonto(Format(mskCustoEndossoCob.Text, "0.00"))) Then
            MsgBox "Valor do custo do endosso maior que o permitido" & vbCrLf & _
                   "Valor informado : " & Format(mskCustoEndossoCob.Text, "#,##0.00") & vbCrLf & _
                   "Valor maximo    : " & Format(oDadosEndosso.valMaxCustoEndosso, "#,##0.00"), vbCritical
            mskCustoEndossoCob.Text = Format(oDadosEndosso.valMaxCustoEndosso, "#,##0.00")
            mskCustoEndossoCob.SetFocus
        End If
    End If

    If grdPagamento.Rows = 1 Then
        If (oDadosEndosso.CriticaCustoRamo And mskCustoEndossoCob.Text > 0) Or (Not oDadosEndosso.PermiteAlteracaoCustoEndosso) Then
            cValCustoEndosso = CalcularCustoEndosso(CCur(mskValPremioTarifa.Text))
            mskCustoEndossoCob.Text = Format(cValCustoEndosso, "#0.00")
        Else
            cValCustoEndosso = cValCustoEndossoInformado
        End If
    End If

    If Not oDadosEndosso.CriticaCustoRamo Then
        If grdPagamento.Rows > 1 Then
            If Val(grdPagamento.TextMatrix(1, 3)) > 0 Then
                cValCustoEndosso = Format(CCur(grdPagamento.TextMatrix(1, 3)), "##0.00")
            End If
        End If
    End If

    If oDadosEndosso.valTaxaJuros > 0 Then
        mskValJuros.Text = Format((CCur(mskValPremioTarifa.Text) - CCur(mskValDesc.Text)) * oDadosEndosso.valTaxaJuros - (CCur(mskValPremioTarifa.Text) - CCur(mskValDesc.Text)), "0.00")
    End If

    If chkIsento_IOF.Value = vbUnchecked Then
        mskValIOF.Text = Format(TruncaDecimal((CCur(mskCustoEndossoCob.Text) + CCur(mskValPremioTarifa.Text) + CCur(mskValJuros.Text) - CCur(mskValDesc.Text)) * oDadosEndosso.PercIOF), "0.00")
    Else
        mskValIOF.Text = "0,00"
    End If

    mskValLiquido.Text = Format(CCur(mskCustoEndossoCob.Text) + CCur(mskValPremioTarifa.Text) + CCur(mskValJuros.Text) + CCur(mskValIOF.Text) - CCur(mskValDesc.Text), "0.00")

    Call CalcularValorCorretagem
    Call CalcularValorProLabore

    If grdPagamento.Rows = 1 Then
        oDadosEndosso.valPremioTarifa = CCur(mskValPremioTarifa.Text)
        oDadosEndosso.valDesconto = CCur(mskValDesc.Text)
    End If

    Exit Sub

TrataErro:
    Call TratarErro("CalcularValoresCobranca", Me.name)
    Call FinalizarAplicacao

End Sub




Private Sub mskDtPagamento_LostFocus()

    If IsDate(mskDtPagamento.Text) And IsDate(mskDtEndosso.Text) And mskNumParcelas.Text <> "" Then
        If DateDiff("d", mskDtPagamento.Text, mskDtEndosso.Text) = 0 Then
            If cmbTaxaJuros.ListIndex <> -1 And cmbTaxaJuros.ListIndex <> 0 Then
                Call CalcularValorJuros("A")
            End If
        Else
            Call CalcularValorJuros("P")
        End If
    End If

End Sub

Private Sub VerificarCriticaCustoRamo()

    Dim oDadosBasicos As Object
    Dim rs As Recordset

    On Error GoTo TrataErro

    Set oDadosBasicos = CreateObject("SEGL0022.cls00117")

    Set rs = oDadosBasicos.ConsultarRamo(gsSIGLASISTEMA, _
                                         App.Title, _
                                         App.FileDescription, _
                                         glAmbiente_id, _
                                         Trim(txtRamo.Text))

    Set oDadosBasicos = Nothing

    If Not rs.EOF Then
        If rs("critica_custo") = "S" Then
            oDadosEndosso.CriticaCustoRamo = True
        Else
            oDadosEndosso.CriticaCustoRamo = False
        End If
    Else
        oDadosEndosso.CriticaCustoRamo = False
    End If

    Exit Sub

TrataErro:
    Call TratarErro("VerificarCriticaCustoRamo", Me.name)
    Call FinalizarAplicacao

End Sub

Private Sub mskDtReativacao_LostFocus()

'    MousePointer = vbHourglass
'
'    If VerificaData2(mskDtReativacao.Text) Then
'        Call PreencherGridReativacao(False)
'    Else
'        grdParcelasReativacao.Rows = 1
'    End If
'
'    MousePointer = vbHourglass

End Sub

Private Sub mskImpSeg_LostFocus()

    Dim oCobertura As Cobertura
    Dim oEmissao As Object
    Dim oFinanceiro As Object
    Dim cPremioTarifa As Currency
    Dim rs As Recordset

    On Error GoTo Erro

    If optRedIS.Value And cboCoberturaAfetada.ListIndex <> -1 Then

        For Each oCobertura In colCoberturas
            If oCobertura.TpCoberturaId = cboCoberturaAfetada.ItemData(cboCoberturaAfetada.ListIndex) And _
               oCobertura.CodObjSegurado = cboObjetoSegurado.Text Then
                Exit For
            End If
        Next

        cAuxPremio = CDbl(mskImpSeg.Text * IIf(oCobertura.ValTaxa = "", 0, oCobertura.ValTaxa))

        Set oCobertura = Nothing

        Set oFinanceiro = CreateObject("SEGL0026.cls00125")

        Set rs = oFinanceiro.ObterPremioPago(gsSIGLASISTEMA, _
                                             App.Title, _
                                             App.FileDescription, _
                                             glAmbiente_id, _
                                             oDadosEndosso.PropostaId)

        Set oFinanceiro = Nothing

        If Not rs.EOF Then
            While Not rs.EOF
                cPremioTotal = cPremioTotal + CCur(rs("valor_pago"))
                cValorJuros = cValorJuros + CCur(rs("val_adic_fracionamento"))
                cValorDesconto = cValorDesconto + CCur(rs("val_desconto"))

                If UCase(rs("situacao")) = "A" Then
                    cValorPago = cValorPago + CCur(rs("valor_pago"))
                End If

                rs.MoveNext
            Wend
            rs.Close
        End If

        Set rs = Nothing

        If cValorPago > 0 Then
            cValorPago = cValorPago - oDadosEndosso.valCustoApoliceCert
            cPremioTotal = cPremioTotal - oDadosEndosso.valCustoApoliceCert
        End If

        If cValorPago > 0 And Trim(txtFimVigencia.Text) <> "" Then
            If DateValue(txtFimVigencia.Text) >= DateValue(Data_Sistema) Then

                Set oEmissao = CreateObject("SEGL0026.cls00126")

                cPremioTarifa = oEmissao.CalcularRestituicaoReducaoIS(gsSIGLASISTEMA, App.Title, App.FileDescription, glAmbiente_id, cPremioTotal, _
                                                                      txtIniVigencia.Text, _
                                                                      txtFimVigencia.Text, _
                                                                      mskDtEndosso.Text, _
                                                                      cAuxPremio, _
                                                                      oDadosEndosso.PercIOF, _
                                                                      0)

                Set oEmissao = Nothing

                mskPremioTarifa.Text = -1 * (TruncaDecimal(cPremioTarifa))

            End If
        End If

        Call mskPremioTarifa_LostFocus

    Else

        mskPremioTarifa.Text = Format((mskImpSeg.Text * mskTaxaTarifa.Text), "##0.00")
        mskPremioNet.Text = Format((mskImpSeg.Text * mskTaxaNet.Text), "##0.00")

    End If

    Exit Sub

Erro:
    Call TratarErro("mskImpSeg_LostFocus", Me.name)
    Call FinalizarAplicacao
End Sub

Private Sub mskNumParcelas_Change()

    If cmbTaxaJuros.ListCount > 0 Then
        cmbTaxaJuros.ListIndex = 0
    End If

End Sub

Private Sub mskPremioNet_LostFocus()

    Dim oEmissao As Object
    Dim dTaxaTarifa As Double
    Dim dTaxaNet As Double
    Dim cPremioTarifa As Currency
    Dim cPremioNet As Currency

    On Error GoTo Erro

    If Val(mskPremioNet.Text) > 0 And Val(mskImpSeg.Text) > 0 Then

        dTaxaTarifa = CDbl(mskTaxaTarifa.Text)
        dTaxaNet = CDbl(mskTaxaNet.Text)
        cPremioNet = 0
        cPremioTarifa = CCur(mskPremioTarifa.Text)

        Set oEmissao = CreateObject("SEGL0026.cls00126")

        ''Chama m�todo de c�lculo
        Call oEmissao.CalcularPremiosTaxas(CCur(mskImpSeg.Text), _
                                           cPercCorretagem, _
                                           dTaxaTarifa, _
                                           dTaxaNet, _
                                           cPremioTarifa, _
                                           cPremioNet, _
                                           cPremioTotal)

        mskTaxaNet.Text = Format(dTaxaNet, "#.0000000")
        mskTaxaTarifa.Text = Format(dTaxaTarifa, "#.0000000")
        mskPremioTarifa.Text = Format(cPremioTarifa, "##0.00")

    End If

    Set oEmissao = Nothing

    Exit Sub

Erro:
    Call TratarErro("mskPremioNet_LostFocus", Me.name)
    Call FinalizarAplicacao
End Sub

Private Sub mskPremioTarifa_LostFocus()

    Dim oEmissao As Object
    Dim dTaxaTarifa As Double
    Dim dTaxaNet As Double
    Dim cPremioTarifa As Currency
    Dim cPremioNet As Currency

    On Error GoTo Erro

    If Val(mskPremioTarifa.Text) > 0 And Val(mskImpSeg.Text) > 0 Then

        dTaxaTarifa = CDbl(mskTaxaTarifa.Text)
        dTaxaNet = CDbl(mskTaxaNet.Text)
        cPremioNet = CCur(mskPremioNet.Text)
        cPremioTarifa = 0

        Set oEmissao = CreateObject("SEGL0026.cls00126")

        ''Chama m�todo de c�lculo
        Call oEmissao.CalcularPremiosTaxas(CCur(mskImpSeg.Text), _
                                           cPercCorretagem, _
                                           dTaxaTarifa, _
                                           dTaxaNet, _
                                           cPremioTarifa, _
                                           cPremioNet, _
                                           cPremioTotal)

        mskTaxaNet.Text = Format(dTaxaNet, "#.0000000")
        mskTaxaTarifa.Text = Format(dTaxaTarifa, "#.0000000")
        mskPremioNet.Text = Format(cPremioNet, "##0.00")

    End If

    Set oEmissao = Nothing

    Exit Sub

Erro:
    Call TratarErro("mskPremioTarifa_LostFocus", Me.name)
    Call FinalizarAplicacao
End Sub

Private Sub mskTaxaNet_LostFocus()

    Dim oEmissao As Object
    Dim dTaxaTarifa As Double
    Dim dTaxaNet As Double
    Dim cPremioTarifa As Currency
    Dim cPremioNet As Currency

    On Error GoTo Erro

    If optAumIS.Value Or optNovCob.Value Then

        dTaxaTarifa = CDbl(mskTaxaTarifa.Text)
        dTaxaNet = 0
        cPremioNet = CCur(mskPremioNet.Text)
        cPremioTarifa = CCur(mskPremioTarifa.Text)

        Set oEmissao = CreateObject("SEGL0026.cls00126")

        ''Chama m�todo de c�lculo
        Call oEmissao.CalcularPremiosTaxas(CCur(mskImpSeg.Text), _
                                           cPercCorretagem, _
                                           dTaxaTarifa, _
                                           dTaxaNet, _
                                           cPremioTarifa, _
                                           cPremioNet, _
                                           0)

        mskPremioNet.Text = Format(cPremioNet, "##0.00")
        mskTaxaTarifa.Text = Format(dTaxaTarifa, "#.0000000")
        mskPremioTarifa.Text = Format(cPremioTarifa, "##0.00")

    End If

    Set oEmissao = Nothing

    Exit Sub

Erro:
    Call TratarErro("mskTaxaNet_LostFocus", Me.name)
    Call FinalizarAplicacao
End Sub

Private Sub mskTaxaTarifa_LostFocus()

    Dim oEmissao As Object
    Dim dTaxaTarifa As Double
    Dim dTaxaNet As Double
    Dim cPremioTarifa As Currency
    Dim cPremioNet As Currency

    On Error GoTo Erro

    If optAumIS.Value Or optNovCob.Value Then

        dTaxaTarifa = 0
        dTaxaNet = CDbl(mskTaxaNet.Text)
        cPremioNet = CCur(mskPremioNet.Text)
        cPremioTarifa = CCur(mskPremioTarifa.Text)

        Set oEmissao = CreateObject("SEGL0026.cls00126")

        ''Chama m�todo de c�lculo
        Call oEmissao.CalcularPremiosTaxas(CCur(mskImpSeg.Text), _
                                           cPercCorretagem, _
                                           dTaxaTarifa, _
                                           dTaxaNet, _
                                           cPremioTarifa, _
                                           cPremioNet, _
                                           0)

        mskPremioNet.Text = Format(cPremioNet, "##0.00")
        mskTaxaNet.Text = Format(dTaxaNet, "#.0000000")
        mskPremioTarifa.Text = Format(cPremioTarifa, "##0.00")

    End If

    Set oEmissao = Nothing

    Exit Sub

Erro:
    Call TratarErro("mskTaxaTarifa_LostFocus", Me.name)
    Call FinalizarAplicacao
End Sub

Private Sub mskValCom_LostFocus()

    If CCur(mskValCom.Text) > 0 And grdCorretagem.Rows = 1 Then
        mskValCom.Text = "0,00"
        Call MsgBox("N�o � permitido informar comiss�o sem corretores para este endosso!")
    End If

End Sub

Private Sub mskValDesc_LostFocus()

    Call CalcularValoresCobranca

End Sub

Private Sub mskValEstadual_LostFocus()
'RETIRAR INICIO- INCLUIDO ENQUANTO N�O ACERTA O PROGRAMA ATRAV�S DEMANDA 1270473 - TALITHA 25.02.2010
    If frmSelecao.grdSelecao.TextMatrix(frmSelecao.grdSelecao.Row, 4) = 1152 _
       And oDadosEndosso.TipoEndosso = TP_ENDOSSO_CANCELAMENTO_APOLICE_SEGURADORA Then
        Call PreencherDadosEndossoRestituicao
    Else
        'RETIRAR FIM - INCLUIDO ENQUANTO N�O ACERTA O PROGRAMA ATRAV�S DEMANDA 1270473 - TALITHA 25.02.2010
        Call CalcularValoresRestituicao
    End If
End Sub

Private Sub mskValFederal_LostFocus()
'lblValFederal.Caption = mskValFederal.Text


    Call PreencherDadosEndossoRestituicao

End Sub

Private Sub mskValIOF_LostFocus()

    Call CalcularValoresCobranca

End Sub

Private Sub mskValJuros_LostFocus()

    If (CCur(mskValPremioTarifa.Text) - CCur(mskValDesc.Text)) <= 0 Then
        oDadosEndosso.valTaxaJuros = 1
    Else
        oDadosEndosso.valTaxaJuros = Format$(1 + (CCur(mskValJuros.Text) / (CCur(mskValPremioTarifa.Text) - CCur(mskValDesc.Text))), "0.0000")
    End If

    Call CalcularValoresCobranca

End Sub

Private Sub mskValPremioTarifa_LostFocus()

    Call CalcularValoresCobranca

End Sub

Private Sub mskValPremioTarifaRes_LostFocus()

    Call CalcularValoresRestituicao

End Sub

Private Sub mskValPremioTarifaResCancelamento_LostFocus(Index As Integer)

    If Index = 0 Or Index = 5 Then
        iModular = Index
        If txtProduto.Text <> 1168 Then
            Call CalcularValorModular
        Else
            Call CalcularValorModular
        End If
    End If

End Sub

Private Sub optAumIS_Click()

    Call CarregarCboCoberturaAfetadaAumentoIS

End Sub

Private Sub optCobAdi_Click()

    fraCobranca.Visible = True
    fraRestituicao.Visible = False
    fraCorretagem.Visible = True
    fraEstipulante.Visible = True
    fraCambio.Visible = True
    FrameRestituicaoModular.Visible = False
    'fraSubvencao.Visible = True

    If sFlagTipoOperacao <> "C" And oDadosEndosso.TipoEndosso = TP_ENDOSSO_IMPORTANCIA_SEGURADA Then

        cmbTaxaJuros.Refresh
        mskCustoEndossoCob.SetFocus

        mskDtPagamento.Text = Format(DateAdd("m", 1, Data_Sistema), "dd/mm/yyyy")

        mskValCom.Text = "0,00"

        grdPagamento.Rows = 1

        optPerMes.Value = vbChecked

        Call LimparVariaveisPgto
        Call MontarComboTaxaJuros

    End If

    '''''' BNEVES EM 29/02/2008   INSERIDO CRITICAR TP_ENDOSSO_MOVIMENTACAO_PREMIO
    If oDadosEndosso.TipoEndosso = TP_ENDOSSO_IMPORTANCIA_SEGURADA Or oDadosEndosso.TipoEndosso = TP_ENDOSSO_MOVIMENTACAO_PREMIO Then
        If grdPagamento.Rows > 1 Then
            cmdAplicarRestituicao.Enabled = True
        Else
            cmdAplicarRestituicao.Enabled = False
        End If
    End If

End Sub

Private Sub optCobRes_Click()

    fraCobranca.Visible = False
    fraCorretagem.Visible = True
    fraEstipulante.Visible = True
    fraCambio.Visible = True

    If (txtProposta(0).Text = "" And txtProposta(1).Text = "") And txtProduto.Text <> 1168 Then
        fraRestituicao.Visible = True
        FrameRestituicaoModular.Visible = False
    Else
        fraRestituicao.Visible = False
        FrameRestituicaoModular.Visible = True
    End If

    Frame5(12).Left = 6900
    Frame5(23).Left = 6900

    If sFlagTipoOperacao <> "R" And oDadosEndosso.TipoEndosso = TP_ENDOSSO_IMPORTANCIA_SEGURADA Then
        mskValPremioTarifaRes.mask = "9,2"
        mskAdicFracionamentoRes.mask = "9,2"
        mskCustoCertificadoRes.mask = "9,2"
        mskValIOFRes.mask = "9,2"
        mskDescontoRes.mask = "9,2"
        mskPercCorretagemCob.mask = "9,2"
        mskPercProLabore.mask = "9,2"
        mskCambio.mask = "3,6"

        mskValPremioTarifaRes.Text = "0,00"
        mskAdicFracionamentoRes.Text = "0,00"
        mskCustoCertificadoRes.Text = "0,00"
        mskValIOFRes.Text = "0,00"
        mskDescontoRes.Text = "0,00"
        mskPercCorretagemCob.Text = "0,00"
        mskPercProLabore.Text = "0,00"
        mskCambio.Text = "0,000000"

        lblValRestituicaoRes.Caption = "0,00"
        lblValCorretagem.Caption = "0,00"
        lblValProLabore.Caption = "0,00"
        lblValRemunercaoServico.Caption = "0,00"

        'Rmarins - 18/10/06
        If bPossuiSubvencao = True Then
            mskValEstadual.mask = "9,2"
            'mskValEstadual.Text = "0,00"
            lblValFederal.Caption = mskValEstadual.Text
        End If

    End If

    'Rmarins - 18/10/06
    If bPossuiSubvencao = True Then
        fraSubvencao.Visible = True
        Frame5(12).Left = 7920
        Frame5(23).Left = 7920
    End If

    ''' BNEVES EM 29/02/2008   Habilita o Bot�o Aplicar para Endosso = 200
    'Alexandre Debouch - Confitec - Demanda EdsContestacao
    'Incluindo o endosso de contesta��o para habilitar o bot�o de aplicar a restituicao
    If oDadosEndosso.TipoEndosso = TP_ENDOSSO_IMPORTANCIA_SEGURADA Or _
       oDadosEndosso.TipoEndosso = TP_ENDOSSO_MOVIMENTACAO_PREMIO Or _
       oDadosEndosso.TipoEndosso = TP_ENDOSSO_CONTESTACAO Then
        cmdAplicarRestituicao.Enabled = True
    End If
    'Incluindo o endosso de contesta��o para habilitar o bot�o de aplicar a restituicao
    'Alexandre Debouch - Confitec - Demanda EdsContestacao

    'lrocha 06/12/2007 - desabilitando campo Pr�mio tarifa para produto 1152
    If frmSelecao.grdSelecao.TextMatrix(frmSelecao.grdSelecao.Row, 4) = 1152 _
       And oDadosEndosso.TipoEndosso = TP_ENDOSSO_CANCELAMENTO_APOLICE_SEGURADORA Then
        mskValPremioTarifaRes.Locked = False    'CORRETO = TRUE - ALTERADO ENQUANTO N�O ACERTA O PROGRAMA ATRAV�S DEMANDA 1270473 - TALITHA 25.02.2010
        'RETIRAR INICIO - INSERIDO ENQUANTO N�O ACERTA O PROGRAMA ATRAV�S DEMANDA 1270473 - TALITHA 25.02.2010
        mskValEstadual.Locked = False

        'Inicio Customizado por Marcio Santos (CWI)
        'Altera��o: Este campo deve permenecer visivel - A Pedido de Andre.
        'lblValFederal.Visible = False
        'Fim Customizado por Marcio Santos (CWI)

        'mskValFederal.Visible = True
        mskValPremioTarifaRes.Locked = True
        'mskValFederal.mask = "9,2"
        'RETIRAR FIM - INSERIDO ENQUANTO N�O ACERTA O PROGRAMA ATRAV�S DEMANDA 1270473 - TALITHA 25.02.2010
    Else
        mskValPremioTarifaRes.Locked = False
        'RETIRAR INICIO - INSERIDO ENQUANTO N�O ACERTA O PROGRAMA ATRAV�S DEMANDA 1270473 - TALITHA 25.02.2010
        mskValEstadual.Locked = True
        lblValFederal.Visible = True
        'RETIRAR FIM - INSERIDO ENQUANTO N�O ACERTA O PROGRAMA ATRAV�S DEMANDA 1270473 - TALITHA 25.02.2010
    End If

    'Jessica.Adao - Confitec Sistemas - 20150715 - Projeto 18319298 - Fase III
    If txtProduto.Text = "1231" Then calculaRestituicaoPJ

End Sub

Private Sub optCobSem_Click()

    fraCobranca.Visible = False
    fraRestituicao.Visible = False
    fraCorretagem.Visible = False
    fraEstipulante.Visible = False
    fraCambio.Visible = False
    FrameRestituicaoModular.Visible = False
    'fraSubvencao.Visible = False

    If oDadosEndosso.TipoEndosso = TP_ENDOSSO_IMPORTANCIA_SEGURADA Then
        cmdAplicarRestituicao.Enabled = True
    End If


    '    If txtProposta(0).Text <> "" And txtProposta(1).Text <> ""  Then
    '        FrameRestituicaoModular.Visible = True
    '    Else
    '        FrameRestituicaoModular.Visible = False
    '    End If



End Sub

Private Sub CalcularValoresRestituicao()

'Jessica.Adao (Inicio) - Confitec Sistemas - 20150715 - Projeto 18319298 - Fase III
'If Not txtProduto.Text = "1231" Then Call CalcularValorRestituicao
    Call CalcularValorRestituicao
    'Jessica.Adao (Fim) - Confitec Sistemas - 20150715 - Projeto 18319298 - Fase III

    Call CalcularValorCorretagem
    Call CalcularValorProLabore

    'Rmarins - 18/10/06
    If bPossuiSubvencao = True Then
        Call CalcularValorSubvencao
    End If

    'bnery - 14/07/2011 - Nova Consultoria
    'Demanda 11239622 - Melhorias Ferramentas Emiss�o
    Call CalcularValorRemuneracao

    Call PreencherDadosEndossoRestituicao

    cmdAplicarRestituicao.Enabled = True

End Sub

Sub ConsultarCorretagem(ByVal lPropostaId As Long)

    Dim oApolice As Object
    Dim colCorretagem As New Collection
    Dim oCls00149 As Object

    On Error GoTo TrataErro

    grdCorretagem.Rows = 1

    Set oApolice = CreateObject("SEGL0026.cls00126")

    Set colCorretagem = oApolice.ObterCorretagem(gsSIGLASISTEMA, _
                                                 App.Title, _
                                                 App.FileDescription, _
                                                 glAmbiente_id, _
                                                 lPropostaId, _
                                                 txtIniVigencia.Text, _
                                                 0, _
                                                 0, _
                                                 0, _
                                                 0)

    Set oApolice = Nothing

    For Each oCls00149 In colCorretagem

        grdCorretagem.AddItem Format(oCls00149.Corretor_ID, "000000-000") & vbTab & _
                              oCls00149.Nome & vbTab & _
                              Format(oCls00149.PercComissao, "0.00") & vbTab & _
                              oCls00149.Sucursal & vbTab & _
                              Trim(oCls00149.Tipo)

        cboCorretorCob.AddItem Format(oCls00149.Corretor_ID, "000000-000") & " - " & oCls00149.Nome
        cboCorretorCob.ItemData(cboCorretorCob.NewIndex) = oCls00149.Corretor_ID

        cboSucursalCorretorCob.AddItem oCls00149.Sucursal
        cboSucursalCorretorCob.ItemData(cboSucursalCorretorCob.NewIndex) = oCls00149.Corretor_ID

        cboTpCorretor.AddItem oCls00149.Tipo
        cboTpCorretor.ItemData(cboTpCorretor.NewIndex) = oCls00149.Corretor_ID

    Next

    If grdCorretagem.Rows = 1 Then

        Set oApolice = CreateObject("SEGL0026.cls00126")

        Set oCls00149 = Nothing
        Set colCorretagem = Nothing
        Set colCorretagem = oApolice.ObterCorretagem(gsSIGLASISTEMA, _
                                                     App.Title, _
                                                     App.FileDescription, _
                                                     glAmbiente_id, _
                                                     lPropostaId, _
                                                     txtDtContratacao.Text, _
                                                     0, _
                                                     0, _
                                                     0, _
                                                     0)

        Set oApolice = Nothing

        For Each oCls00149 In colCorretagem

            grdCorretagem.AddItem Format(oCls00149.Corretor_ID, "000000-000") & vbTab & _
                                  oCls00149.Nome & vbTab & _
                                  Format(oCls00149.PercComissao, "0.00") & vbTab & _
                                  oCls00149.Sucursal & vbTab & _
                                  Trim(oCls00149.Tipo)

            cboCorretorCob.AddItem Format(oCls00149.Corretor_ID, "000000-000") & " - " & oCls00149.Nome
            cboCorretorCob.ItemData(cboCorretorCob.NewIndex) = oCls00149.Corretor_ID

            cboSucursalCorretorCob.AddItem oCls00149.Sucursal
            cboSucursalCorretorCob.ItemData(cboSucursalCorretorCob.NewIndex) = oCls00149.Corretor_ID

            cboTpCorretor.AddItem oCls00149.Tipo
            cboTpCorretor.ItemData(cboTpCorretor.NewIndex) = oCls00149.Corretor_ID

        Next

    End If


    Set oCls00149 = Nothing
    Set colCorretagem = Nothing

    grdCorretagem.ColWidth(3) = 0

    Exit Sub

TrataErro:
    Call TratarErro("ConsultarCorretagem")
    Call FinalizarAplicacao

End Sub



Private Sub CarregarDadosCobranca()

    Dim lPropostaId As Long

    On Error GoTo TrataErro

    lPropostaId = oDadosEndosso.PropostaId

    Call ConsultarCorretagem(lPropostaId)
    Call ConsultarEstipulante(lPropostaId)

    optPerMes.Value = vbChecked
    stbEndosso.TabVisible(1) = True
    stbEndosso.Tab = 1
    optCobRes.Enabled = False
    optCobAdi.Value = True
    optCobAdi.Enabled = True
    optCobSem.Enabled = False

    '''' BNEVES em 29/02/2008 para habilitar as options de resitui��o
    If oDadosEndosso.TipoEndosso = TP_ENDOSSO_IMPORTANCIA_SEGURADA Or oDadosEndosso.TipoEndosso = TP_ENDOSSO_MOVIMENTACAO_PREMIO Then
        optCobRes.Enabled = True
        optCobAdi.Value = True
        optCobAdi.Enabled = True
        optCobSem.Enabled = True
    End If

    Call VerificarCriticaCustoRamo
    Call MontarComboMoeda
    Call LimparVariaveisPgto
    Call MontarComboTaxaJuros
    If oDadosEndosso.CriticaCustoRamo Then
        mskCustoEndossoCob.Text = Format(CalcularCustoEndosso(oDadosEndosso.valPremioTarifa), "##0.00")
    End If

    cmbTaxaJuros.Refresh
    mskCustoEndossoCob.SetFocus

    mskDtPagamento.Text = Format(DateAdd("m", 1, Data_Sistema), "dd/mm/yyyy")

    If grdPagamento.Rows > 1 Then
        If Val(grdPagamento.TextMatrix(1, 3)) > 0 Then
            mskCustoEndossoCob.Text = Format(CDbl(grdPagamento.TextMatrix(grdPagamento.Row, 3)), "###.00")
        End If
    End If

    If oDadosEndosso.TipoEndosso = TP_ENDOSSO_IMPORTANCIA_SEGURADA Then
        mskValPremioTarifa.Text = Format$(cValFinanceiro, "###0.00")
    Else
        mskValPremioTarifa.Text = Format$(oDadosEndosso.valPremioTarifa, "###0.00")
    End If

    mskValCom.Text = "0,00"

    grdPagamento.Rows = 1

    Exit Sub

TrataErro:
    Call TratarErro("CarregarDadosCobranca", Me.name)
    Call FinalizarAplicacao

End Sub

Private Sub CarregarDadosSemCobranca()

    On Error GoTo Erro

    stbEndosso.TabVisible(1) = True
    stbEndosso.Tab = 1
    optCobRes.Enabled = False
    optCobAdi.Enabled = False
    optCobSem.Value = True
    optCobSem.Enabled = True


    ''' BNEVES EM 29/02/2008   INSERIDO CRITICAR TP_ENDOSSO_MOVIMENTACAO_PREMIO
    If oDadosEndosso.TipoEndosso = TP_ENDOSSO_IMPORTANCIA_SEGURADA Or oDadosEndosso.TipoEndosso = TP_ENDOSSO_MOVIMENTACAO_PREMIO Then
        optCobRes.Enabled = True
        optCobAdi.Enabled = True
        optCobSem.Value = True
        optCobSem.Enabled = True
    End If

    Exit Sub

Erro:
    Call TratarErro("CarregarDadosSemCobranca", Me.name)
    Call FinalizarAplicacao
End Sub

Public Sub LimparVariaveisPgto()

    On Error GoTo TrataErro

    mskCustoEndossoCob.mask = "9,2"
    mskValPremioTarifa.mask = "9,2"
    mskValJuros.mask = "9,2"
    mskValIOF.mask = "9,2"
    mskValCom.mask = "9,2"
    mskValDesc.mask = "9,2"
    mskValLiquido.mask = "9,2"
    mskDtPagamento.mask = "##/##/####"
    mskCambio.mask = "3,6"

    mskCustoEndossoCob.Text = "0,00"
    mskValPremioTarifa.Text = "0,00"
    mskValJuros.Text = "0,00"
    mskValIOF.Text = "0,00"
    mskValCom.Text = "0,00"
    mskValDesc.Text = "0,00"
    mskValLiquido.Text = "0,00"
    mskDtPagamento.Text = "__/__/____"
    mskCambio.Text = "0,000000"

    Exit Sub

TrataErro:
    Call TratarErro("LimparVariaveisPgto", Me.name)
    Call FinalizarAplicacao

End Sub


Public Function CalcularCustoEndosso(ByVal cValCobertura As Currency) As Currency

    Dim oFinanceiro As Object
    Dim oDadosBasicos As Object

    Dim dPercCustoEndosso As Double
    Dim cMaxCustoEndosso As Currency
    Dim iMoedaAtual As Integer

    Dim rs As Recordset

    On Error GoTo TrataErro

    Set oFinanceiro = CreateObject("SEGL0026.cls00125")

    Set rs = oFinanceiro.ConsultarCusto(gsSIGLASISTEMA, _
                                        App.Title, _
                                        App.FileDescription, _
                                        glAmbiente_id, _
                                        Trim(txtRamo.Text), _
                                        Trim(txtProduto.Text), _
                                        Trim(mskDtEndosso.Text))

    Set oFinanceiro = Nothing

    Set oDadosBasicos = CreateObject("SEGL0022.cls00117")

    ' Obtendo moeda atual do sistema ''''''''''''''''''''''''''''''''''''''''''

    iMoedaAtual = Val(oDadosBasicos.ConsultarParametro(gsSIGLASISTEMA, _
                                                       App.Title, _
                                                       App.FileDescription, _
                                                       glAmbiente_id, _
                                                       "MOEDA ATUAL"))

    Set oDadosBasicos = Nothing

    If Not rs.EOF Then
        If UCase(rs("custo_me")) = "N" And cboMoeda.ItemData(cboMoeda.ListIndex) <> iMoedaAtual Then
            dPercCustoEndosso = 0
            cMaxCustoEndosso = 0
        Else
            dPercCustoEndosso = Val(rs("perc_custo_endosso")) / 100
            cMaxCustoEndosso = CCur(rs("val_custo_endosso_max"))
        End If

        oDadosEndosso.valMaxCustoEndosso = cMaxCustoEndosso

        If UCase(rs("permite_alteracao")) = "S" Then
            mskCustoEndossoCob.Locked = False
            oDadosEndosso.PermiteAlteracaoCustoEndosso = True
        Else
            mskCustoEndossoCob.Locked = True
            oDadosEndosso.PermiteAlteracaoCustoEndosso = False
        End If
    End If

    Set rs = Nothing

    CalcularCustoEndosso = CCur(cValCobertura * dPercCustoEndosso)

    If CalcularCustoEndosso > cMaxCustoEndosso Then
        CalcularCustoEndosso = cMaxCustoEndosso
    End If

    Exit Function

TrataErro:
    Call TratarErro("CalcularCustoEndosso", Me.name)
    Call FinalizarAplicacao

End Function

Private Sub CarregarDadosCancelamentoApolice()

    On Error GoTo TrataErro

    stbEndosso.TabVisible(2) = True
    stbEndosso.Tab = 2

    mskDtVigCan.Text = mskDtEndosso.Text
    txtMotCancel.Text = ""

    Exit Sub

TrataErro:
    Call TratarErro("CarregarDadosCancelamentoApolice", Me.name)
    Call FinalizarAplicacao

End Sub

Private Sub CarregarDadosCancelamentoPorSinistro()

    On Error GoTo TrataErro

    stbEndosso.TabVisible(18) = True
    stbEndosso.Tab = 18

    Exit Sub

TrataErro:
    Call TratarErro("CarregarDadosCancelamentoPorSinistro", Me.name)
    Call FinalizarAplicacao

End Sub

Private Sub CarregarDadosLimiteEmissao()

    Dim OEndosso As Object
    Dim rsLimiteEmissao As Object

    On Error GoTo TrataErro

    stbEndosso.TabVisible(9) = True
    stbEndosso.Tab = 9

    Set OEndosso = CreateObject("SEGL0140.cls00230")

    Set rsLimiteEmissao = OEndosso.BuscarLimiteEmissaoAtual(cUserName, _
                                                            gsSIGLASISTEMA, _
                                                            App.Title, _
                                                            App.FileDescription, _
                                                            glAmbiente_id, _
                                                            oDadosEndosso.PropostaId)



    mskLimiteAtual(0).mask = "13V2"
    mskLimiteAtual(0).Text = Format(rsLimiteEmissao("val_lim_is_apolice"), "############0.00")

    mskLimiteAtual(1).mask = "13V2"
    mskLimiteAtual(1).Text = "0.00"

    mskDtAtualLimite.Text = Format(rsLimiteEmissao("dt_inicio_contratacao"), "dd/mm/yyyy")
    mskDtAtualLimite.Tag = IIf(IsNull(rsLimiteEmissao("dt_fim_contratacao")), "", Format(rsLimiteEmissao("dt_fim_contratacao"), "dd/mm/yyyy"))

    mskDtNovoLimite.Text = Format(Data_Sistema, "dd/mm/yyyy")

    Set rsLimiteEmissao = Nothing
    Set OEndosso = Nothing


    Exit Sub

TrataErro:
    Call TratarErro("CarregarDadosLimiteEmissao", Me.name)
    Call FinalizarAplicacao

End Sub

Private Sub CarregarDadosCancelamentoEndosso()

    On Error GoTo TrataErro

    stbEndosso.TabVisible(3) = True
    stbEndosso.Tab = 3

    mskDtVigCanEndosso.Text = mskDtEndosso.Text
    TxtNumEndossoCanc.Text = Val(txtNumEndosso) - 1

    If uTipoEndosso = 101 Or uTipoEndosso = 200 Then
        If uProduto = 200 Or _
           uProduto = 201 Or _
           uProduto = 202 Or _
           uProduto = 203 Or _
           uProduto = 204 Or _
           uProduto = 205 Or _
           uProduto = 208 Or _
           uProduto = 209 Or _
           uProduto = 210 Or _
           uProduto = 211 Or _
           uProduto = 212 Or _
           uProduto = 213 Or _
           uProduto = 216 Or _
           uProduto = 217 Or _
           uProduto = 218 Or _
           uProduto = 219 Or _
           uProduto = 220 Or _
           uProduto = 221 Or _
           uProduto = 224 Or _
           uProduto = 225 _
           Then
            TxtNumEndossoCanc.Enabled = False
        End If
    End If
    If oDadosEndosso.TpEmissao = "A" Then
        chkEndossoNaLider.Visible = True
    Else
        chkEndossoNaLider.Visible = False
    End If


    Exit Sub

TrataErro:
    Call TratarErro("CarregarDadosCancelamentoEndosso", Me.name)
    Call FinalizarAplicacao

End Sub

Private Sub CarregarDadosReativacaoProposta()

    Dim oEndossoReativacao As Object
    Dim dt_parc_aux As Date
    Dim sLinha2 As String
    Dim iConta As Integer    '20-12-2015 - Schoralick (nova consultoria) - 18690344 - Melhorias Processo Reagendamento e Reativa��o BESC

    On Error GoTo TrataErro

    'Jessica.Adao (Inicio) - Confitec Sistemas - 20140401 - PROJETO 17919477 : BB SEGURO CREDITO PROTEGIDO PJ
    If oDadosEndosso.produtoId = 1225 And Not reativaProposta(oDadosEndosso.PropostaId) Then
        MsgBox "Proposta cancelada a mais de 90 dias! Reativa��o n�o ser� realizada!", vbCritical
        stbEndosso.TabEnabled(0) = True
        stbEndosso.TabVisible(12) = False
        stbEndosso.Tab = 0
        Exit Sub
    End If
    'Jessica.Adao (Fim) - Confitec Sistemas - 20140401 - PROJETO 17919477 : BB SEGURO CREDITO PROTEGIDO PJ


    'Jessica.Adao - Confitec Sistemas - 20131023 - Demanda 16429073 : Inclusao do produto 1217
    'JOAO.MACHADO -  INCLUINDO OS RESIDENCIAIS 2.0
    'JOAO.MACHADO - INCLUINDO O PRODUTO 1231, 1229
    '12/02/2016 - schoralick (nova consultoria) - 18234489 - Novos produtos para substituir Ouro Vida e outros (inicio)
    Select Case oDadosEndosso.produtoId
        'In�cio - Isabeli Silva - Confitec SP - Flow 00411015  - Novo Prote��o Ouro - 20180-07-10
    Case 1205, 1217, 1218, 1225, 1220, 1221, 1222, 1223, 1224, 1231, 1196, 1229, 1235, 1236, 1237, 1239
        'Fim - Isabeli Silva - Confitec SP - Flow 00411015  - Novo Prote��o Ouro - 20180-07-10
        'fim - 18234489
        frmReativacaoProposta.Show
        frmEndosso.Hide
    Case Else
        stbEndosso.TabVisible(12) = True
        stbEndosso.Tab = 12
        mskValParcelaReativacao.mask = "5,2"
        mskValParcelaReativacao.Text = "0,00"
        mskDtReativacao.Text = mskDtEndosso.Text

        ' Autor: pablo.dias (Nova Consultoria) - Data da Altera��o: 08/03/2013
        ' Demanda: 17789414 - Melhorias SEGBR para aumento da capacidade de vendas BB
        Frame5(22).Visible = (oDadosEndosso.produtoId <> OURO_RESIDENCIAL_ESTILO)
    End Select

    dt_parc_aux = Data_Sistema

    Set oEndossoReativacao = CreateObject("SEGL0140.cls00172")

    lEndossoCancelamentoId = oEndossoReativacao.ObterEndossoCancelamento(gsSIGLASISTEMA, _
                                                                         App.Title, _
                                                                         App.FileDescription, _
                                                                         glAmbiente_id, _
                                                                         oDadosEndosso.PropostaId)

    If lEndossoCancelamentoId = 0 Then
        Set oEndossoReativacao = Nothing
        MsgBox "A proposta n�o est� cancelada", vbCritical
        Call Unload(frmReativacaoProposta)
        Call cmdBtnReativacaoProposta_Click(0)    '03-12-2015 - Schoralick (nova consultoria) - 18690344 - Melhorias Processo Reagendamento e Reativa��o BESC
        'Jessica.Adao - Confitec Sistemas - 20131023 - Demanda 16429073 : Inclusao do produto 1217
        'JOAO.MACHADO -  INCLUINDO OS RESIDENCIAIS 2.0
        'JOAO.MACHADO - INCLUINDO O PRODUTO 1231, 1229
        '20-12-2015 - Schoralick (nova consultoria) - 18690344 - Melhorias Processo Reagendamento e Reativa��o BESC: inclusao de 200, 201, 202, 203, 204, 205, 208, 209, 210, 211, 213, 216, 217, 218, 219, 221, 222, 223, 224, 225, 226
        Select Case oDadosEndosso.produtoId
            '12/02/2016 - schoralick (nova consultoria) - 18234489 - Novos produtos para substituir Ouro Vida e outros - add produtos (1235, 1236, 1237)
        Case 200, 201, 202, 203, 204, 205, 208, 209, 210, 211, 213, 216, 217, 218, 219, 221, 222, 223, 224, 225, 226, 1196, 1205, 1217, 1218, 1220, 1221, 1222, 1223, 1224, 1225, 1229, 1231, 1235, 1236, 1237
            Exit Sub
        End Select
    End If

    lUltimaParcelaReativacao = oEndossoReativacao.ObterUltimaParcela(gsSIGLASISTEMA, _
                                                                     glAmbiente_id, _
                                                                     App.Title, _
                                                                     App.FileDescription, _
                                                                     oDadosEndosso.PropostaId)

    ' 14/12/2015 - Schoralick (nova consultoria) - 18717252 - Cobran�a parcela atual + parcela anterior - BESC
    Select Case oDadosEndosso.produtoId
    Case 200, 201, 202, 203, 204, 205, 208, 209, 210, 211, 213, 216, 217, 218, 219, 221, 222, 223, 224, 225, 226
        iConta = 0
        grdParcelasReativacao.Rows = 1
        Set ObterAgendamentosReativacao = oEndossoReativacao.ObterAgendamentosReativacaoBesc(gsSIGLASISTEMA, _
                                                                                             glAmbiente_id, _
                                                                                             App.Title, _
                                                                                             App.FileDescription, _
                                                                                             oDadosEndosso.PropostaId, _
                                                                                             lEndossoCancelamentoId)

        While Not ObterAgendamentosReativacao.EOF
            sLinha2 = ""
            sLinha2 = sLinha2 & ObterAgendamentosReativacao!Num_Cobranca & vbTab
            sLinha2 = sLinha2 & Format(ObterAgendamentosReativacao!Val_Cobranca, "0.00") & vbTab
            sLinha2 = sLinha2 & Format(ObterAgendamentosReativacao!Dt_Agendamento, "dd/mm/yyyy") & vbTab
            sLinha2 = sLinha2 & "c" & vbTab
            sLinha2 = sLinha2 & ObterAgendamentosReativacao!ano_mes_ref

            Call grdParcelasReativacao.AddItem(sLinha2)
            iConta = iConta + 1

            dt_parc_aux = DateAdd("m", 1, dt_parc_aux)
            ObterAgendamentosReativacao.MoveNext

        Wend

        cmdBtnReativacaoProposta(2).Enabled = (iConta > 1)
        '27/12/2015 - schoralick (nova consultoria) - 18690344 - Melhorias Processo Reagendamento e Reativa��o BESC - fim
    Case Else
        If oDadosEndosso.produtoId = 1152 Or oDadosEndosso.produtoId = 1204 Or oDadosEndosso.produtoId = 1240 Then    '16/07/2018 - Schoralick (ntendencia) - 00421597-seguro-faturamento-pecuario [add produto 1240]

            ObterTpEndossoCancId = oEndossoReativacao.ObterTpEndossoCancId(gsSIGLASISTEMA, _
                                                                           glAmbiente_id, _
                                                                           App.Title, _
                                                                           App.FileDescription, _
                                                                           oDadosEndosso.PropostaId, _
                                                                           lEndossoCancelamentoId)

            If ObterTpEndossoCancId = "92" Then

                Set ObterAgendamentosReativacao = oEndossoReativacao.ObterAgendamentosReativacao(gsSIGLASISTEMA, _
                                                                                                 glAmbiente_id, _
                                                                                                 App.Title, _
                                                                                                 App.FileDescription, _
                                                                                                 oDadosEndosso.PropostaId, _
                                                                                                 lEndossoCancelamentoId)

                While Not ObterAgendamentosReativacao.EOF

                    sLinha2 = ""
                    sLinha2 = sLinha2 & lUltimaParcelaReativacao & vbTab
                    sLinha2 = sLinha2 & Format(ObterAgendamentosReativacao!Val_Cobranca, "0.00") & vbTab
                    sLinha2 = sLinha2 & Format(dt_parc_aux, "dd/mm/yyyy")

                    Call grdParcelasReativacao.AddItem(sLinha2)

                    dt_parc_aux = DateAdd("m", 1, dt_parc_aux)
                    lUltimaParcelaReativacao = lUltimaParcelaReativacao + 1
                    ObterAgendamentosReativacao.MoveNext

                Wend

            Else

                MsgBox "O produto " & oDadosEndosso.produtoId & " permite apenas reativa��o para cancelamento de inadimplencia da primeira parcela!", vbCritical, App.Title
                stbEndosso.TabEnabled(0) = True
                stbEndosso.TabVisible(12) = False
                stbEndosso.Tab = 0

            End If

        End If
    End Select

    Set oEndossoReativacao = Nothing


    Exit Sub

TrataErro:
    Call TratarErro("CarregarDadosReativacaoProposta", Me.name)
    Call FinalizarAplicacao

End Sub


Sub PreencherGridReativacao(Optional ByVal bAlteraValorParcela As Boolean = True)

    Dim oEndossoReativacao As Object
    Dim iParcela As Integer
    Dim sDtParcela As String
    Dim iDiaCobranca As Integer
    Dim sDtReativacao As String
    Dim iAntecedenciaCobranca As Integer
    Dim sNossoNumero As String
    Dim iProximaCobranca As Integer
    Dim rs As Recordset

    On Error GoTo Erro

    Set oEndossoReativacao = CreateObject("SEGL0140.cls00172")

    Set rs = oEndossoReativacao.ObterParcelasReativacao(gsSIGLASISTEMA, _
                                                        glAmbiente_id, _
                                                        App.Title, _
                                                        App.FileDescription, _
                                                        oDadosEndosso.PropostaId, _
                                                        Data_Sistema, _
                                                        lEndossoCancelamentoId, _
                                                        mskDtReativacao.Text, _
                                                        iProximaCobranca, _
                                                        iAntecedenciaCobranca, _
                                                        iDiaCobranca)

    Set oEndossoReativacao = Nothing

    iParcela = 0
    sDtParcela = ""

    If bAlteraValorParcela Then
        grdParcelasReativacao.Rows = 1
    End If

    While Not rs.EOF

        iParcela = iParcela + 1

        If iParcela = 1 Then
            sDtParcela = Format(iDiaCobranca, "00") & "/" & Mid(Data_Sistema, 4, 7)
            While DateDiff("D", DateValue(mskDtReativacao.Text), DateValue(sDtParcela)) < iAntecedenciaCobranca
                sDtParcela = DateAdd("M", 1, sDtParcela)
            Wend
        Else
            sDtParcela = DateAdd("M", 1, sDtParcela)
        End If

        If IsNull(rs("nosso_numero")) Or IsNull(rs("nosso_numero_dv")) Then
            sNossoNumero = ""
        Else
            sNossoNumero = Trim(rs("nosso_numero")) & "-" & Trim(rs("nosso_numero_dv"))
        End If

        If bAlteraValorParcela Then

            grdParcelasReativacao.AddItem iProximaCobranca & vbTab & _
                                          Format(rs("val_cobranca"), "###,###,##0.00") & vbTab & _
                                          Format(sDtParcela, "dd/mm/yyyy") & vbTab & _
                                          sNossoNumero

        Else

            grdParcelasReativacao.TextMatrix(iParcela, 2) = Format(sDtParcela, "dd/mm/yyyy")

        End If

        iProximaCobranca = iProximaCobranca + 1

        rs.MoveNext

    Wend

    rs.Close
    Set rs = Nothing

    Exit Sub

Erro:
    Call TratarErro("preencherGridReativacao", Me.name)
    Call FinalizarAplicacao
End Sub

Private Sub CarregarDadosCadastrais()

    Dim oApolice As Object
    Dim rsCadastral As Recordset
    Dim rsConsultaBancarios As Recordset    'R.FOUREAUX 25/06/2012 - DEMANDA 12006056
    Dim iIndice As Integer
    Dim iFormaPagamento As Integer    'R.FOUREAUX 25/06/2012 - DEMANDA 12006056



    On Error GoTo TrataErro

    stbEndosso.TabVisible(4) = True
    stbEndosso.Tab = 4

    Call MontarComboCidade(cboCidade)

    Set oApolice = CreateObject("SEGL0026.cls00126")

    Set rsCadastral = oApolice.ConsultarDadosCadastrais(gsSIGLASISTEMA, _
                                                        App.Title, _
                                                        App.FileDescription, _
                                                        glAmbiente_id, _
                                                        oDadosEndosso.PropostaId)


    If Not rsCadastral.EOF Then
        'R.FOUREAUX 25/06/2012 - DEMANDA 12006056 - IDENTIFICANDO BESK E ENDOSSO 252 PARA CRITICA EDICAO
        If oDadosEndosso.produtoId >= 200 And _
           oDadosEndosso.produtoId <= 225 And _
           cboTipoEndosso.ItemData(cboTipoEndosso.ListIndex) = TP_ALTERACAO_FORMA_PAGAMENTO _
           Then

            'DADOS GERAIS
            txtEndoCad(0).Enabled = False
            cboTpPessoa.Enabled = False
            txtCPFCNPJ.Enabled = False
            cboSexo.Enabled = False
            cboEstCivil.Enabled = False
            txtNascimento.Enabled = False
            txtEndoCad(1).Enabled = False
            txtEndoCad(2).Enabled = False
            txtEndoCad(3).Enabled = False
            txtEndoCad(0).BackColor = &H80000004
            cboTpPessoa.BackColor = &H80000004
            txtCPFCNPJ.BackColor = &H80000004
            cboSexo.BackColor = &H80000004
            cboEstCivil.BackColor = &H80000004
            txtNascimento.BackColor = &H80000004
            txtEndoCad(1).BackColor = &H80000004
            txtEndoCad(2).BackColor = &H80000004
            txtEndoCad(3).BackColor = &H80000004
            'CONTAS
            txtforma_pgto(0).Enabled = False
            txtforma_pgto(1).Enabled = False
            txtBanco.Enabled = False
            txtEndoCad(6).Enabled = False
            txtCodAgDebito(0).Enabled = False
            txtCodAgDebito(1).Enabled = False
            txtEndoCad(7).Enabled = False
            txtforma_pgto(0).BackColor = &H80000004
            txtforma_pgto(1).BackColor = &H80000004
            txtBanco.BackColor = &H80000004
            txtEndoCad(6).BackColor = &H80000004
            txtCodAgDebito(0).BackColor = &H80000004
            txtCodAgDebito(1).BackColor = &H80000004
            txtEndoCad(7).BackColor = &H80000004

            'RS CONSULTA OS DADOS BANCARIOS
            Set rsConsultaBancarios = oApolice.ConsultarDadosBancarios(gsSIGLASISTEMA, _
                                                                       App.Title, _
                                                                       App.FileDescription, _
                                                                       glAmbiente_id, _
                                                                       oDadosEndosso.PropostaId)
            iFormaPagamento = rsConsultaBancarios("forma_pgto_id")
            'FORMA PAGAMENTO IGUAL A D�BITO
            'If rsConsultaBancarios("forma_pgto_id") = 1 Then
            'cboFormaPagamento.Visible = False
            'cmdAplicarCadastral.Enabled = False
            'Philip.Rocha (In�cio) - Confitec Sistemas - 20130718 - INC000004122797 : Campos desabilitados
            If True Then
                cboFormaPagamento.Visible = True
                cboFormaPagamento.Enabled = False
                cmdAplicarCadastral.Enabled = True

                Call CarregarCboFormaPagamento(1)
                Call cboFormaPagamento_Click
                'Philip.Rocha (Fim) - Confitec Sistemas - 20130718 - INC000004122797 : Campos desabilitados
            Else
                'FORMA PAGAMENTO IGUAL A FICHA COMPENSACAO BANCARIA, HABILITAR TROCA SOMENTE PARA DEBITO 1 , POPULANDO AGENCIA PARA BB E VALIDANDO AG E CC PREENCHIMENTO OBRIGATORIO
                If rsConsultaBancarios("forma_pgto_id") = 3 Then
                    'CARREGA CBO
                    Call CarregarCboFormaPagamento(iFormaPagamento)
                    'MOSTRA CBO DE TROCA SOMENTE PARA DEBITO
                    cboFormaPagamento.Visible = True
                    'OCULTA CAMPOS DE TP PGTO 1
                    txtforma_pgto(0).Visible = False
                    txtforma_pgto(1).Visible = False
                    txtCodAgDebito(0).Visible = False
                    txtCodAgDebito(1).Visible = False
                    txtBanco.Visible = False
                    txtEndoCad(6).Visible = False
                    txtEndoCad(7).Visible = False
                    lblCad(23).Visible = False
                    lblCad(24).Visible = False
                    lblCad(25).Visible = False

                End If

            End If
            rsConsultaBancarios.Close
            Set rsConsultaBancarios = Nothing
        End If


        txtCodCliAnt.Text = oDadosEndosso.ClienteId

        ' Nome ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        If Not IsNull(rsCadastral("nome")) Then
            txtEndoCad(0).Text = Trim(rsCadastral("nome"))
            txtNomeAnt.Text = rsCadastral("nome")
            txtNomeAnt.Tag = Trim(rsCadastral("nome"))
        Else
            txtNomeAnt.Tag = ""
        End If

        ' Tipo de Pessoa ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        cboTpPessoa.ListIndex = rsCadastral("tp_pessoa") - 1
        txtIdTpPessoaAnt.Text = rsCadastral("tp_pessoa")
        txtTpPessoaAnt.Text = IIf(rsCadastral("tp_pessoa") = 1, "F�SICA", "JUR�DICA")

        If rsCadastral("tp_pessoa") = 1 Then

            ' Data de Nascimento ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            If Not IsNull(rsCadastral("dt_nasc")) Then
                txtNascimento.Text = Format(rsCadastral("dt_nasc"), "dd/mm/yyyy")
                txtNascAnt.Text = Format(rsCadastral("dt_nasc"), "dd/mm/yyyy")
                txtNascAnt.Tag = Format(rsCadastral("dt_nasc"), "dd/mm/yyyy")
            Else
                txtNascAnt.Tag = "__/__/____"
            End If

            ' CPF '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            If Not IsNull(rsCadastral("cpf_cgc")) Then
                lblCad(31).Caption = "CPF :"
                lblCad(27).Caption = "CPF :"
                txtCPFCNPJ.mask = "###.###.###-##"
                txtCPFCNPJ.Text = Format(rsCadastral("cpf_cgc"), "&&&.&&&.&&&-&&")
                txtCPFCNPJAnt.Text = Format(rsCadastral("cpf_cgc"), "&&&.&&&.&&&-&&")
                txtCPFCNPJAnt.Tag = Format(rsCadastral("cpf_cgc"), "&&&.&&&.&&&-&&")

            Else
                txtCPFCNPJAnt.Tag = ""
            End If

            ' Sexo ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            txtSexoAnt.Text = UCase(rsCadastral("sexo"))

            Select Case txtSexoAnt.Text
            Case "M"
                txtSexoDescAnt.Text = "MASCULINO"
            Case "F"
                txtSexoDescAnt.Text = "FEMININO"
            Case Else
                txtSexoDescAnt.Text = "N�O INFORMADO"
            End Select

            For iIndice = 0 To cboSexo.ListCount - 1
                If Left(cboSexo.List(iIndice), 1) = txtSexoAnt.Text Then
                    cboSexo.ListIndex = iIndice
                    txtSexoDescAnt.Tag = iIndice
                    Exit For
                End If
            Next

            ' Estado Civil ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            txtEstCivilAnt.Text = UCase(rsCadastral("estado_civil"))

            Select Case txtEstCivilAnt.Text
            Case "C"
                txtEstCivilDescAnt.Text = "CASADO"
            Case "D"
                txtEstCivilDescAnt.Text = "DIVORCIADO"
            Case "E"
                txtEstCivilDescAnt.Text = "DESQUITADO"
            Case "S"
                txtEstCivilDescAnt.Text = "SOLTEIRO"
            Case "V"
                txtEstCivilDescAnt.Text = "VI�VO"
            Case Else
                txtEstCivilDescAnt.Text = "N�O INFORMADO"
            End Select

            For iIndice = 0 To cboEstCivil.ListCount - 1
                If VetorEstadoCivil(cboEstCivil.ItemData(iIndice)) = txtEstCivilAnt.Text Then
                    cboEstCivil.ListIndex = iIndice
                    txtEstCivilDescAnt.Tag = iIndice
                    Exit For
                End If
            Next

        Else

            lblCad(28).Visible = False
            txtSexoAnt.Visible = False
            txtSexoDescAnt.Visible = False
            lblCad(29).Visible = False
            txtEstCivilAnt.Visible = False
            txtEstCivilDescAnt.Visible = False
            lblCad(30).Visible = False
            txtNascAnt.Visible = False
            lblCad(32).Visible = False
            cboSexo.Visible = False
            lblCad(33).Visible = False
            cboEstCivil.Visible = False
            lblCad(34).Visible = False
            txtNascimento.Visible = False

            ' CNPJ ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            If Not IsNull(rsCadastral("cpf_cgc")) Then
                lblCad(31).Caption = "CNPJ :"
                lblCad(27).Caption = "CNPJ :"
                txtCPFCNPJ.mask = "##.###.###/####-##"
                txtCPFCNPJ.Text = Format(rsCadastral("cpf_cgc"), "&&.&&&.&&&/&&&&-&&")
                txtCPFCNPJAnt.Text = Format(rsCadastral("cpf_cgc"), "&&.&&&.&&&/&&&&-&&")
                txtCPFCNPJAnt.Tag = Format(rsCadastral("cpf_cgc"), "&&.&&&.&&&/&&&&-&&")
            Else
                txtCPFCNPJAnt.Tag = ""
            End If

        End If

        'Ezequiel - Dados comentados relativos a endere�o (est�o em outra tab)
        ' Endere�o ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        If Not IsNull(rsCadastral("endereco")) Then
            sEndereco = Trim(rsCadastral("endereco"))
            '    txtEndoCad(4).Text = Trim(rsCadastral("endereco"))
            '    TxtEnderecoAnt.Text = Trim(rsCadastral("endereco"))
            '    TxtEnderecoAnt.Tag = Trim(rsCadastral("endereco"))
        Else
            sEndereco = ""
            '    TxtEnderecoAnt.Tag = ""
        End If

        ' Bairro ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        If Not IsNull(rsCadastral("bairro")) Then
            sBairro = Trim(rsCadastral("bairro"))
            '    txtEndoCad(5).Text = Trim(rsCadastral("bairro"))
            '    TxtBairroAnt.Text = Trim(rsCadastral("bairro"))
            '    TxtBairroAnt.Tag = Trim(rsCadastral("bairro"))
        Else
            sBairro = ""
            '    TxtBairroAnt.Tag = ""
        End If

        ' Munic�pio '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        If Not IsNull(rsCadastral("municipio")) Then

            sCidade = Trim(rsCadastral("municipio")) & " - " & Trim(rsCadastral("estado"))
            'TxtCidadeAnt.Text = Trim(rsCadastral("municipio")) & " - " & Trim(rsCadastral("estado"))

            'For iIndice = 0 To cboCidade.ListCount - 1
            '    If Left(cboCidade.List(iIndice), Len(cboCidade.List(iIndice)) - 5) = Trim(rsCadastral!Municipio) Then
            '        cboCidade.ListIndex = iIndice
            '        TxtCidadeAnt.Tag = iIndice
            '        Exit For
            '    End If
            'Next

            'If cboCidade.ListIndex = -1 Then
            '    TxtCidadeAnt.Tag = -1
            'End If

        Else
            sCidade = ""
            '    TxtCidadeAnt.Tag = -1
        End If

        ' Estado ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        If Not IsNull(rsCadastral("estado")) Then
            sUF = Trim(rsCadastral("estado"))
            '    TxtUFAnt(1).Text = Trim(rsCadastral("estado"))
            '    TxtUFAnt(1)Ant(0).Text = Trim(rsCadastral("estado"))
            '    TxtUFAnt(1)Ant(0).Tag = Trim(rsCadastral("estado"))
        Else
            sUF = ""
            '    TxtUFAnt(1)Ant(0).Tag = ""
        End If

        ' CEP '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        If Not IsNull(rsCadastral("cep")) Then
            sCep = Format(rsCadastral("cep"), "&&.&&&-&&&")
            '    If Trim(rsCadastral("cep")) <> "" Then
            '        TxtCep.Text = Format(rsCadastral("cep"), "&&.&&&-&&&")
            '        TxtCEPAnt.Text = Format(rsCadastral("cep"), "&&.&&&-&&&")
            '        TxtCEPAnt.Tag = Format(rsCadastral("cep"), "&&.&&&-&&&")
            '    Else
            '        TxtCEPAnt.Tag = ""
            '    End If
        Else
            sCep = ""
            '    TxtCEPAnt.Tag = ""
        End If

        ' DDD '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        If Not IsNull(rsCadastral("ddd_1")) Then
            txtEndoCad(2).Text = rsCadastral("ddd_1")
            TxtDDDAnt.Text = rsCadastral("ddd_1")
            TxtDDDAnt.Tag = rsCadastral("ddd_1")
        Else
            TxtDDDAnt.Tag = ""
        End If

        ' Telefone ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        If Not IsNull(rsCadastral("telefone_1")) Then
            txtEndoCad(3).Text = rsCadastral("telefone_1")
            TxtTelefoneAnt.Text = rsCadastral("telefone_1")
            TxtTelefoneAnt.Tag = rsCadastral("telefone_1")
        Else
            TxtTelefoneAnt.Tag = ""
        End If

        ' E-mail ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        If Not IsNull(rsCadastral("e_mail")) Then
            TxtEmailAnt.Text = Trim(rsCadastral("e_mail"))
            txtEndoCad(1).Text = Trim(rsCadastral("e_mail"))
            TxtEmailAnt.Tag = Trim(rsCadastral("e_mail"))
        Else
            TxtEmailAnt.Tag = ""
        End If

        rsCadastral.Close

    End If

    Set rsCadastral = oApolice.ConsultarDadosBancarios(gsSIGLASISTEMA, _
                                                       App.Title, _
                                                       App.FileDescription, _
                                                       glAmbiente_id, _
                                                       oDadosEndosso.PropostaId)

    If Not rsCadastral.EOF Then

        If Not IsNull(rsCadastral("banco_id")) Then
            txtEndoCad(8).Text = rsCadastral("banco_id")
            txtBanco.Text = rsCadastral("banco_id")
            txtEndoCad(9).Text = rsCadastral("nome_banco")
            txtEndoCad(6).Text = rsCadastral("nome_banco")

        Else
            txtEndoCad(8).Tag = ""
        End If

        If Not IsNull(rsCadastral("agencia_id")) Then
            TxtCodAgAnt.Text = rsCadastral("agencia_id")
            txtCodAgDebito(0).Text = rsCadastral("agencia_id")
            TxtNomeAgAnt.Text = rsCadastral("nome_agencia")
            txtCodAgDebito(1).Text = rsCadastral("nome_agencia")
        Else
            TxtCodAgAnt.Tag = ""
        End If

        If Not IsNull(rsCadastral("conta_corrente_id")) Then
            TxtCCAnt.Text = rsCadastral("conta_corrente_id")
            txtEndoCad(7).Text = rsCadastral("conta_corrente_id")
        Else
            TxtCCAnt.Tag = ""
        End If

        'R.FOUREAUX 25/06/2012 - DEMANDA 12006056 - MONTA FORMA DE PAGAMENTO
        If Not IsNull(rsCadastral("forma_pgto_id")) Then
            txtforma_pgto_Ant(0).Text = rsCadastral("forma_pgto_id")
            txtforma_pgto_Ant(1).Text = rsCadastral("forma_pgto_nome")
            txtforma_pgto(0).Text = rsCadastral("forma_pgto_id")
            txtforma_pgto(1).Text = rsCadastral("forma_pgto_nome")
        Else
            txtforma_pgto_Ant(0).Tag = ""
            txtforma_pgto_Ant(1).Tag = ""
        End If



        rsCadastral.Close

    End If

    Set rsCadastral = Nothing
    Set oApolice = Nothing

    '*** Inclus�o do Sinalizado de PPE - Pessoas politicamente expostas - demanda 269994
    '*** 27/08/2008 - Fabio (Stefanini)
    If IdentificaPPE(txtCPFCNPJ.ClipText) Then Me.chkPPE.Value = 1

    Exit Sub

TrataErro:

    Call TratarErro("CarregarDadosCadastral", Me.name)
    Call FinalizarAplicacao

End Sub

Private Sub CarregarDadosImportanciaSegurada(ByVal lPropostaId As Long, ByVal iProdutoId As Integer, _
                                             ByVal iRamoId As Integer)

    stbEndosso.TabVisible(10) = True
    stbEndosso.Tab = 10

    Call LimparCoberturas
    Call LimparCamposEndossoIS
    Call DesabilitarCamposEndossoIS

    Call IniciarConexao(gsSIGLASISTEMA, _
                        App.Title, _
                        App.FileDescription, _
                        glAmbiente_id, _
                        cUserName)

    Call ConsultarObjetoSegurado(lPropostaId)
    Call ConsultarCoberturas(lPropostaId, iProdutoId)
    Call CarregarColecaoCoberturasProduto(iProdutoId, iRamoId)

    Call FecharConexao

    Call ObterPercCorretagem(lPropostaId)

End Sub

Sub FecharConexao()

    Call oConexao.DestruirConexao

End Sub

Private Sub AplicarCancelamentoApolice()

    On Error GoTo TrataErro

    If Not IsDate(mskDtVigCan.Text) Then
        MsgBox "Data Inicio Vig�ncia Incorreta.", vbCritical
        mskDtVigCan.SetFocus
        Exit Sub
    End If

    If CDate(mskDtVigCan.Text) < CDate(txtIniVigencia) Then
        MsgBox "Data de Cancelamento n�o pode ser menor que data de in�cio de vig�ncia da Ap�lice.", vbCritical
        mskDtVigCan.Text = mskDtEndosso.Text
        mskDtVigCan.SetFocus
        Exit Sub
    End If

    If txtMotCancel.Text = "" Then
        MsgBox "Informe o Motivo do Cancelamento."
        txtMotCancel.SetFocus
        Exit Sub
    End If

    stbEndosso.TabEnabled(2) = False

    'Rmarins - 18/10/06
    If bPossuiSubvencao = True And oDadosEndosso.TipoEndosso = TP_ENDOSSO_CANCELAMENTO_APOLICE_SEGURADORA And UCase(txtPermiteRestituicao) = "S" Then
        Call ObterValoresSubvencao
    End If

    Call CarregarDadosRestituicao

    'Comentado para que os valores sejam calculado em "ObterValoresSubvencap" - Cibele Pereira - INC000004421713 - 09/10/2014
    'Call ObterValoresRestituicao 'Cibele Pereira - INC000004403706 23/09/2014

    If Not bObteveSaldoRestituir Then Exit Sub    'cristovao.rodrigues 25/04/2013

    'RSilva - 04/11/2009
    'If bModular = True Then
    '    Call ObterValoresRestituicaoModular
    'End If


    If bModular = False Then
        Call mskValPremioTarifaRes_LostFocus
    Else
        Call mskValPremioTarifaResCancelamento_LostFocus(0)
        Call mskValPremioTarifaResCancelamento_LostFocus(5)
    End If

    Exit Sub

TrataErro:
    Call TratarErro("AplicarCancelamentoApolice", Me.name)
    Call FinalizarAplicacao

End Sub

Public Function VerificaRestituicaoProduto1205(proposta_id As Long) As Boolean


    Dim sSQL As String
    Dim rs As New Recordset
    Dim periodo As Integer
    
        If rdocn Is Nothing Then
            Call Conexao
        Else
            If rdocn.State = 0 Then Call Conexao
        End If
    
    VerificaRestituicaoProduto1205 = False

    sSQL = "SELECT periodo_pgto_id FROM seguros_db.dbo.proposta_adesao_tb WITH(NOLOCK) WHERE proposta_id = " & proposta_id

    Set rs = rdocn.Execute(sSQL)
                                                    
   If Not rs.EOF Then
    While Not rs.EOF
        periodo = rs("periodo_pgto_id")
        rs.MoveNext
    Wend
   End If
   
   rs.Close
   Set rs = Nothing
   
   If periodo = 12 Then
    VerificaRestituicaoProduto1205 = True
End If
End Function


 

Public Sub ObterValoresRestituicao()

    Dim oFinanceiro As Object
    Dim rs As Recordset

    Dim cValPago As Currency
    Dim cValJuros As Currency
    Dim cValDesconto As Currency
    Dim cValIOF As Currency
    Dim cPremioTotal As Currency
    Dim cValRestituicao As Currency

    'HOLIVEIRA: Se bRestIntegralALS = TRUE ent�o � Restitui��o Integral ALS. DT: 12/02/2007
    Dim bRestIntegralALS As Boolean

    On Error GoTo TrataErro

    cValPago = 0
    cValJuros = 0
    cValDesconto = 0
    cPremioTotal = 0

    'bcarneiro - 02/03/2007
    bRestIntegral = False
    ''''''''''''''''''''''''

    Set oFinanceiro = CreateObject("SEGL0026.cls00125")
    
    If uProduto = 1205 Then
        If VerificaRestituicaoProduto1205(Trim(txtPropostaid(0).Text)) Then
            
            cValRestituicao = oFinanceiro.CalcularRestituicao1205(gsSIGLASISTEMA, _
                                             App.Title, _
                                             App.FileDescription, _
                                             glAmbiente_id, _
                                             Trim(txtPropostaid(0).Text), _
                                             Trim(mskDtEndosso.Text))
            mskValPremioTarifaRes.Text = Format(cValRestituicao, "##0.00")
            mskValPremioTarifaResCancelamento(0).Text = mskValPremioTarifaRes.Text
            Exit Sub
        
        End If
    End If
    
    If bModular = False Then
        Set rs = oFinanceiro.ObterPremioPago(gsSIGLASISTEMA, _
                                             App.Title, _
                                             App.FileDescription, _
                                             glAmbiente_id, _
                                             Trim(txtPropostaid(0).Text))
    Else

        Set rs = oFinanceiro.ObterPremioPagoModular(gsSIGLASISTEMA, _
                                                    App.Title, _
                                                    App.FileDescription, _
                                                    glAmbiente_id, _
                                                    Trim(txtPropostaid(0).Text))

    End If
    Set oFinanceiro = Nothing

    If Not rs.EOF Then
        While Not rs.EOF
            cPremioTotal = cPremioTotal + CCur(rs("valor_pago"))
            cValJuros = cValJuros + CCur(rs("val_adic_fracionamento"))
            cValDesconto = cValDesconto + CCur(rs("val_desconto"))
            cValIOF = cValIOF + CCur(rs("val_iof"))
            If UCase(rs("situacao")) = "A" Then
                cValPago = cValPago + CCur(rs("valor_pago"))
            End If

            rs.MoveNext
        Wend
    End If

    Set rs = Nothing

    If cValPago > 0 And bModular = False Then
        cValPago = cValPago - oDadosEndosso.valCustoApoliceCert
        cPremioTotal = cPremioTotal - oDadosEndosso.valCustoApoliceCert
    End If



    If bModular = True Then
        Dim iDias As Integer
        Dim dDias As Double

        dDias = Val(DateDiff("d", Data_Sistema, oDadosModular.sDtFimVigenciaEsc))
        cValRestituicao = cValPago * (dDias / 30)
    End If


    If bModular = False Then
        cValRestituicao = CalcularRestituicao(cPremioTotal, _
                                              cValJuros, _
                                              cValDesconto, _
                                              cValIOF, _
                                              cValPago, _
                                              bRestIntegralALS)    'HOLIVEIRA: Retorna TRUE em bRestIntegralALS quando for Restitui��o Integral ALS. DT: 12/02/2007
        If Not bObteveSaldoRestituir Then Exit Sub    'cristovao.rodrigues 25/04/2013

    End If
    mskValPremioTarifaRes.Text = Format(cValRestituicao, "##0.00")
    mskValPremioTarifaResCancelamento(0).Text = mskValPremioTarifaRes.Text

    'HOLIVEIRA: Exibe os valores quando for Restitui��o Integral ALS. DT: 12/02/2007
    If bRestIntegralALS = True Then

        mskCustoCertificadoRes.Text = Format(oDadosEndosso.valCustoApoliceCert, "##0.00")
        mskAdicFracionamentoRes.Text = Format(cValJuros, "##0.00")
        mskDescontoRes.Text = Format(cValDesconto, "##0.00")
        mskValIOFRes.Text = Format(cValIOF, "##0.00")
        'bcarneiro - 02/03/2007
        bRestIntegral = True
        ''''''''''''''''''''''''
        If bModular = True Then
            mskValPremioTarifaResCancelamento(1).Text = mskCustoCertificadoRes.Text
            mskValPremioTarifaResCancelamento(2).Text = mskAdicFracionamentoRes.Text
            mskValPremioTarifaResCancelamento(3).Text = mskDescontoRes.Text
            mskValPremioTarifaResCancelamento(4).Text = mskValIOFRes.Text
        End If
    End If


    Exit Sub

TrataErro:
    Call TratarErro("ObterValoresRestituicao", Me.name)
    FinalizarAplicacao

End Sub



Public Sub ObterValoresRestituicaoModular()

    Dim oFinanceiro As Object
    Dim rs As Recordset

    Dim cValPago As Currency
    Dim cValJuros As Currency
    Dim cValDesconto As Currency
    Dim cValIOF As Currency
    Dim cPremioTotal As Currency
    Dim cValRestituicao As Currency

    'HOLIVEIRA: Se bRestIntegralALS = TRUE ent�o � Restitui��o Integral ALS. DT: 12/02/2007
    Dim bRestIntegralALS As Boolean

    'RSILVA
    Dim sDataInicio As String
    Dim sDataCanc As String
    Dim sDataFim As String
    Dim dDias As Double
    Dim cValPremioTarifa As Currency
    Dim glAmbiente_id_ABS As Integer

    On Error GoTo TrataErro

    '    cValPago = 0
    '    cValJuros = 0
    '    cValDesconto = 0
    '    cPremioTotal = 0
    '
    'bcarneiro - 02/03/2007
    bRestIntegral = False
    ''''''''''''''''''''''''
    Set oFinanceiro = CreateObject("SEGL0026.cls00125")

    If sDtInicioEsc = "" Or sDtFimEsc = "" Then
        Call oFinanceiro.ObterParametroRestituicao(gsSIGLASISTEMA, _
                                                   App.Title, _
                                                   App.FileDescription, _
                                                   glAmbiente_id, _
                                                   Trim(txtPropostaid(0).Text), oDadosModular.PropostaId, _
                                                   sDtInicioEsc, sDtFimEsc)
        'Inicio Demanda:9420699 Data:31/03/2011 Autor:Rodrigo Moura - Confitec
        If sDtInicioEsc = "" Or sDtFimEsc = "" Then
            Select Case glAmbiente_id

            Case 2
                glAmbiente_id_ABS = 6
            Case 3
                glAmbiente_id_ABS = 7
            Case Else
                glAmbiente_id_ABS = 6
            End Select

            Call oFinanceiro.ObterParametroRestituicao(gsSIGLASISTEMA, _
                                                       App.Title, _
                                                       App.FileDescription, _
                                                       glAmbiente_id_ABS, _
                                                       Trim(txtPropostaid(0).Text), oDadosModular.PropostaId, _
                                                       sDtInicioEsc, sDtFimEsc)


        End If
        'Fim Demanda:9420699 Data:31/03/2011 Autor:Rodrigo Moura - Confitec

        oDadosModular.sDtInicioVigenciaEsc = sDtInicioEsc
        oDadosModular.sDtFimVigenciaEsc = sDtFimEsc

    End If

    'Set oFinanceiro = CreateObject("SEGL0026.cls00125")
    Set rs = oFinanceiro.ObterPremioPagoModular(gsSIGLASISTEMA, _
                                                App.Title, _
                                                App.FileDescription, _
                                                glAmbiente_id, _
                                                oDadosModular.PropostaId)

    Set oFinanceiro = Nothing

    If Not rs.EOF Then
        While Not rs.EOF
            oDadosModular.ValPremioTotalRest = oDadosModular.ValPremioTotalRest + CCur(rs("valor_pago"))
            oDadosModular.ValJurosRest = oDadosModular.ValJurosRest + CCur(rs("val_adic_fracionamento"))
            oDadosModular.ValDescontoRest = oDadosModular.ValDescontoRest + CCur(rs("val_desconto"))
            oDadosModular.ValIOFRest = oDadosModular.ValIOFRest + CCur(rs("val_iof"))
            If UCase(rs("situacao")) = "A" Then
                oDadosModular.ValPagoRest = oDadosModular.ValPagoRest + CCur(rs("valor_pago"))
            End If

            rs.MoveNext
        Wend
    End If

    Set rs = Nothing

    '    If oDadosModular.ValPagoRest > 0 Then
    '        oDadosModular.ValPagoRest = oDadosModular.ValPagoRest - oDadosEndosso.valCustoApoliceCert
    '        oDadosModular.ValPremioTotalRest = oDadosModular.ValPremioTotalRest - oDadosEndosso.valCustoApoliceCert
    '    End If

    sDataInicio = oDadosModular.sDtInicioVigenciaEsc
    sDataFim = oDadosModular.sDtFimVigenciaEsc
    sDataCanc = Data_Sistema  'Trim(mskDtVigCan.Text)
    cValPremioTarifa = oDadosModular.ValPremioTotalRest



    dDias = Val(DateDiff("d", sDataCanc, sDataFim))

    cValRestituicao = cValPremioTarifa * (dDias / 30)
    If bModular = False Then
        cValRestituicao = CalcularRestituicao(oDadosModular.ValPremioTotalRest, _
                                              oDadosModular.ValJurosRest, _
                                              oDadosModular.ValDescontoRest, _
                                              oDadosModular.ValIOFRest, _
                                              oDadosModular.ValPagoRest, _
                                              bRestIntegralALS)    'HOLIVEIRA: Retorna TRUE em bRestIntegralALS quando for Restitui��o Integral ALS. DT: 12/02/2007
        If Not bObteveSaldoRestituir Then Exit Sub    'cristovao.rodrigues 25/04/2013

    End If
    oDadosModular.ValRestituicao = cValRestituicao

    mskValPremioTarifaRes.Text = Format(cValRestituicao, "##0.00")
    mskValPremioTarifaResCancelamento(5).Text = mskValPremioTarifaRes.Text

    'HOLIVEIRA: Exibe os valores quando for Restitui��o Integral ALS. DT: 12/02/2007
    If bRestIntegralALS = True Then
        mskCustoCertificadoRes.Text = Format(oDadosEndosso.valCustoApoliceCert, "##0.00")
        mskAdicFracionamentoRes.Text = Format(oDadosModular.ValJurosRest, "##0.00")
        mskDescontoRes.Text = Format(oDadosModular.ValDescontoRest, "##0.00")
        mskValIOFRes.Text = Format(oDadosModular.ValIOFRest, "##0.00")
        'bcarneiro - 02/03/2007
        bRestIntegral = True
        ''''''''''''''''''''''''
        If bModular = True Then

            mskValPremioTarifaResCancelamento(6).Text = mskCustoCertificadoRes.Text
            mskValPremioTarifaResCancelamento(7).Text = mskAdicFracionamentoRes.Text
            mskValPremioTarifaResCancelamento(8).Text = mskDescontoRes.Text
            mskValPremioTarifaResCancelamento(9).Text = mskValIOFRes.Text
        End If
    End If

    Exit Sub

TrataErro:
    Call TratarErro("ObterValoresRestituicao", Me.name)
    FinalizarAplicacao

End Sub


Public Sub ObterValoresSubvencao()

    Dim rs As Recordset
    Dim oSubvencao As Object

    Dim cValPremioTarifa As Currency
    Dim cValSubvencaoInformado As Currency
    Dim dPercFederal As Double

    'INICIO - CIBELE - INC000004360630 - 15/07/2014

    Dim cValRestituicao As Currency

    'FIM - CIBELE - INC000004360630 - 15/07/2014

    Dim cValSubvencaoEstadualInformado As Currency
    Dim dPercEstadual As Double


    'Consulta os valores de subvencao por proposta - Rmarins 18/10/06

    On Error GoTo TrataErro

    cValPremioTarifa = 0
    cValSubvencaoInformado = 0
    cValSubvencaoEstadualInformado = 0

    Set oSubvencao = CreateObject("SEGL0026.cls00125")

    Set rs = oSubvencao.ObterPropostaSubvencao(gsSIGLASISTEMA, _
                                               App.Title, _
                                               App.FileDescription, _
                                               glAmbiente_id, _
                                               Trim(txtPropostaid(0).Text))

    Set oSubvencao = Nothing

    If Not rs.EOF Then
        cValSubvencaoInformado = CCur(rs("val_subvencao_informado"))
        cValPremioTarifa = CCur(rs("val_premio_tarifa"))
        'bmoraes 06.03.2009
        cValSubvencaoEstadualInformado = CCur(rs("val_subvencao_estadual_informado"))

    End If

    Set rs = Nothing

    mskValEstadual.Text = cValSubvencaoEstadualInformado

    dPercFederal = CalcularPercSubvencaoFederal(cValSubvencaoInformado, _
                                                cValPremioTarifa)

    lblPercFederal.Caption = Format(dPercFederal, "##0.00000")

    'INICIO - CIBELE - INC000004360630 - 15/07/2014

    mskValPremioTarifaRes.Text = Format(cValPremioTarifa, "##0.00")
    mskValPremioTarifaResCancelamento(0).Text = mskValPremioTarifaRes.Text


    'FIM - CIBELE - INC000004360630 - 15/07/2014

    Exit Sub

TrataErro:
    Call TratarErro("ObterValoresSubvencao", Me.name)
    FinalizarAplicacao

End Sub


Public Function CalcularRestituicao(ByVal cPremioTotal As Currency, _
                                    ByVal cValorJuros As Currency, _
                                    ByVal cValDesconto As Currency, _
                                    ByVal cValIOF As Currency, _
                                    ByVal cValPago As Currency, _
                                    Optional ByRef bRestIntegralALS As Boolean = False) As Currency    'HOLIVEIRA: Retorna bRestIntegralALS = TRUE quando for Restitui��o Integral ALS. DT: 12/02/2007

    Dim sDataInicio As String
    Dim sDataCanc As String
    Dim sDataFim As String

    Dim dFatorDias As Double
    Dim dDias As Double

    Dim cValRestituicao As Currency

    Dim oProposta As Object  'Rmarins 13/12/2006
    Dim oRsProposta As Object
    Dim iOrigemProposta As Integer

    '(INI) Bruno Faria - 19/01/2011
    Dim oCancelamentoProposta As Object
    Dim oRsCancelamentoProposta As Object
    '(FIM) Bruno Faria - 19/01/2011

    On Error GoTo TrataErro

    'Validando dados
    If Trim(Trim(txtFimVigencia.Text)) = "" Then
        cValRestituicao = 0
        CalcularRestituicao = cValRestituicao
        Exit Function
    End If

    If cValPago = 0 Then
        cValRestituicao = 0
        CalcularRestituicao = cValRestituicao
        Exit Function
    End If

    sDataInicio = Trim(txtIniVigencia.Text)
    sDataFim = Trim(txtFimVigencia.Text)
    sDataCanc = Trim(mskDtVigCan.Text)

    dFatorDias = (DateDiff("d", sDataInicio, sDataFim) / 365)

    '(INI) Bruno Faria - 19/01/2011
    'Objetivo: Selecionar a data de cancelamento da proposta, caso n�o esteja preenchido na tela e n�o seja proposta do BB Prote��o;
    If bModular = False And (Not IsDate(sDataCanc)) Then

        Set oCancelamentoProposta = CreateObject("SEGL0022.cls00117")

        Set oRsCancelamentoProposta = oCancelamentoProposta.ConsultarCancelamentoProposta(gsSIGLASISTEMA, _
                                                                                          App.Title, _
                                                                                          App.FileDescription, _
                                                                                          glAmbiente_id, _
                                                                                          Trim(txtPropostaid(0).Text))

        If Not oRsCancelamentoProposta.EOF Then
            sDataCanc = IIf(IsNull(oRsCancelamentoProposta("dt_inicio_cancelamento")), "", oRsCancelamentoProposta("dt_inicio_cancelamento"))
        End If

        Set oRsCancelamentoProposta = Nothing
        Set oCancelamentoProposta = Nothing

    End If
    '(FIM) Bruno Faria - 19/01/2011

    'bmoraes 02.11.2010 - Casos de endosso de restitui��o - devolu��o integral
    If IsDate(sDataCanc) = False Then
        dDias = 0
    Else
        dDias = Val(DateDiff("d", sDataInicio, sDataCanc) / dFatorDias)
    End If
    'bmoraes - fim

    'Rmarins - 13/12/06 - Consultando a origem da proposta '''''''''''''''''''''''''''''
    Set oProposta = CreateObject("SEGL0022.cls00117")

    Set oRsProposta = oProposta.ConsultarProposta(gsSIGLASISTEMA, _
                                                  App.Title, _
                                                  App.FileDescription, _
                                                  glAmbiente_id, _
                                                  Trim(txtPropostaid(0).Text))

    If Not oRsProposta.EOF Then
        iOrigemProposta = IIf(IsNull(oRsProposta("origem_proposta_id")), 0, oRsProposta("origem_proposta_id"))
    End If

    'Encontrando o valor da restitui��o '''''''''''''''''''''''''''''''''''''''''''''''''
    If iOrigemProposta = 2 Then  'ALS
        'HOLIVEIRA: Retorna bRestIntegralALS = TRUE quando for Restitui��o Integral ALS. DT: 12/02/2007
        cValRestituicao = ProcessarRestituicaoALS(dDias, cValPago, cPremioTotal, bRestIntegralALS)
    Else

        'cristovao.rodrigues 06/12/2012 aqui fazer verificao do novo calculo para produto 11, incluido if ...
        'produto_atual
        If txtProduto.Text = 11 Then    ' And Val(mskNumParcelas.Text) > 1 Then

            If Retorna_Quantidade_Parcelas(Trim(txtPropostaid(0).Text)) > 1 Then
                cValRestituicao = Retorno_Saldo_Restituir(Trim(txtPropostaid(0).Text))

                'cristovao.rodrigues 25/04/2013
                'cristovao.rodrigues 05/07/2013 comentado EXIT FUNCTION,
                'fazer o calculo do valor conforme anteriormente, caso nao encontre valor para restituicao
                If Not bObteveSaldoRestituir Then    ' -- Exit Function
                    bObteveSaldoRestituir = True
                    cValRestituicao = CalcularRestituicaoCurtoPrazo(dDias, cValPago, cPremioTotal)    'cristovao.rodrigues 05/07/2013
                End If
            Else
                cValRestituicao = CalcularRestituicaoCurtoPrazo(dDias, cValPago, cPremioTotal)
            End If

        Else
            cValRestituicao = CalcularRestituicaoCurtoPrazo(dDias, cValPago, cPremioTotal)
        End If
    End If

    If cValRestituicao < 0 Then
        cValRestituicao = 0
    End If

    CalcularRestituicao = cValRestituicao

    Set oRsProposta = Nothing
    Set oProposta = Nothing

    Exit Function

TrataErro:
    Call TratarErro("CalcularRestituicao", Me.name)
    Call FinalizarAplicacao
    Resume
End Function

'cristovao.rodrigues 06/12/2012
Private Function Retorna_Quantidade_Parcelas(Num_Proposta As String) As Integer

    Dim sSQL As String
    Dim rs As Recordset

    On Error GoTo TrataErro

    If rdocn Is Nothing Then
        Call Conexao
    Else
        If rdocn.State = 0 Then Call Conexao
    End If

    sSQL = ""
    sSQL = "select count(1) from agendamento_cobranca_tb  WITH (NOLOCK)  where proposta_id = " & Num_Proposta

    Set rs = rdocn.Execute(sSQL)

    Retorna_Quantidade_Parcelas = rs(0)

    'rs.Close
    Set rs = Nothing

    Exit Function

TrataErro:
    Call TratarErro("Retorna_Quantidade_Parcelas", Me.name)

End Function


'cristovao.rodrigues 05/12/2012
Private Function Retorno_Saldo_Restituir(Num_Proposta As String) As Currency
    Dim sSQL As String
    Dim rSr As Recordset
    'Dim Num_Proposta As String

    On Error GoTo TrataErro

    'Num_Proposta = "29752247"

    '-- 01 dias vigencia
    'sSQL = sSQL & " drop table #dias_vigencia"
    'sSQL = ""
    'sSQL = sSQL & " select proposta_id, count(1) qtd_parcelas_pagas, dias_vigencia = count(1) * 45.625 " & vbCrLf
    'sSQL = sSQL & " into #dias_vigencia  " & vbCrLf
    'sSQL = sSQL & " from agendamento_cobranca_tb  WITH (NOLOCK) " & vbCrLf
    'sSQL = sSQL & " where proposta_id = " & Num_Proposta & vbCrLf
    'sSQL = sSQL & "     and situacao = 'a'" & vbCrLf
    'sSQL = sSQL & "     and dt_recebimento is not null " & vbCrLf
    'sSQL = sSQL & " group by proposta_id " & vbCrLf
    '-- select * from #dias_vigencia


    sSQL = ""
    sSQL = sSQL & " select count(1) qtd_parcelas_pagas  " & vbCrLf
    sSQL = sSQL & " from agendamento_cobranca_tb  WITH (NOLOCK)  " & vbCrLf
    sSQL = sSQL & " where proposta_id = " & Num_Proposta & vbCrLf
    sSQL = sSQL & "     and situacao = 'a' " & vbCrLf
    sSQL = sSQL & "     and dt_recebimento is not null " & vbCrLf
    sSQL = sSQL & " group by proposta_id " & vbCrLf

    Set rSr = rdocn.Execute(sSQL)

    If rSr.EOF Then
        Retorno_Saldo_Restituir = 0
        rSr.Close
        Exit Function
    End If

    rSr.Close

    sSQL = ""

    '' GENESCO SILVA - FLOW 18313521 - MIGRA��O SQL SERVER

    'cristovao.rodrigues 25/04/2013
    'sSQL = sSQL & " if isnull(OBJECT_ID('tempdb..#parcelas_pagas'),0) >0" & vbCrLf
    sSQL = sSQL & " if OBJECT_ID('tempdb..#parcelas_pagas') IS NOT NULL " & vbCrLf
    sSQL = sSQL & " BEGIN " & vbCrLf
    sSQL = sSQL & "     DROP TABLE #parcelas_pagas " & vbCrLf
    sSQL = sSQL & " END " & vbCrLf

    'sSQL = sSQL & " if isnull(OBJECT_ID('tempdb..#parcelas_contratadas'),0) >0" & vbCrLf
    sSQL = sSQL & " if OBJECT_ID('tempdb..#parcelas_contratadas') IS NOT NULL " & vbCrLf
    sSQL = sSQL & " BEGIN " & vbCrLf
    sSQL = sSQL & "     DROP TABLE #parcelas_contratadas " & vbCrLf
    sSQL = sSQL & " END " & vbCrLf

    'sSQL = sSQL & " if isnull(OBJECT_ID('tempdb..#dias_vigencia'),0) >0" & vbCrLf
    sSQL = sSQL & " if OBJECT_ID('tempdb..#dias_vigencia') IS NOT NULL " & vbCrLf
    sSQL = sSQL & " BEGIN " & vbCrLf
    sSQL = sSQL & "     DROP TABLE #dias_vigencia " & vbCrLf
    sSQL = sSQL & " END " & vbCrLf

    'sSQL = sSQL & " if isnull(OBJECT_ID('tempdb..#parcela_liq_iof'),0) >0" & vbCrLf
    sSQL = sSQL & " if OBJECT_ID('tempdb..#parcela_liq_iof') IS NOT NULL " & vbCrLf
    sSQL = sSQL & " BEGIN " & vbCrLf
    sSQL = sSQL & "     DROP TABLE #parcela_liq_iof " & vbCrLf
    sSQL = sSQL & " END " & vbCrLf

    'sSQL = sSQL & " if isnull(OBJECT_ID('tempdb..#premio_dia'),0) >0" & vbCrLf
    sSQL = sSQL & " if OBJECT_ID('tempdb..#premio_dia') IS NOT NULL " & vbCrLf
    sSQL = sSQL & " BEGIN " & vbCrLf
    sSQL = sSQL & "     DROP TABLE #premio_dia " & vbCrLf
    sSQL = sSQL & " END " & vbCrLf

    'sSQL = sSQL & " if isnull(OBJECT_ID('tempdb..#Premio_periodo_Vigencia'),0) >0" & vbCrLf
    sSQL = sSQL & " if OBJECT_ID('tempdb..#Premio_periodo_Vigencia') IS NOT NULL " & vbCrLf
    sSQL = sSQL & " BEGIN " & vbCrLf
    sSQL = sSQL & "     DROP TABLE #Premio_periodo_Vigencia " & vbCrLf
    sSQL = sSQL & " END " & vbCrLf
    'cristovao.rodrigues 25/04/2013


    sSQL = sSQL & " -- drop table #parcelas_pagas " & vbCrLf
    sSQL = sSQL & " select proposta_id, count(1) qtd_parcelas_pagas  " & vbCrLf
    sSQL = sSQL & " into #parcelas_pagas " & vbCrLf
    sSQL = sSQL & " from agendamento_cobranca_tb  WITH (NOLOCK)  " & vbCrLf
    sSQL = sSQL & " where proposta_id = " & Num_Proposta & vbCrLf
    sSQL = sSQL & "     and situacao = 'a' " & vbCrLf
    sSQL = sSQL & "     and dt_recebimento is not null " & vbCrLf
    sSQL = sSQL & " group by proposta_id " & vbCrLf
    sSQL = sSQL & " select * from #parcelas_pagas  " & vbCrLf

    sSQL = sSQL & " --drop table  #parcelas_contratadas " & vbCrLf
    sSQL = sSQL & " select agendamento_cobranca_tb.proposta_id, count(1) parcelas " & vbCrLf
    sSQL = sSQL & " into  #parcelas_contratadas " & vbCrLf
    sSQL = sSQL & " from agendamento_cobranca_tb  WITH (NOLOCK)  " & vbCrLf
    sSQL = sSQL & " inner join #parcelas_pagas " & vbCrLf
    sSQL = sSQL & "     on #parcelas_pagas.proposta_id = agendamento_cobranca_tb.proposta_id " & vbCrLf
    sSQL = sSQL & " group by agendamento_cobranca_tb.proposta_id  " & vbCrLf
    sSQL = sSQL & " -- select * from  #parcelas_contratadas " & vbCrLf

    sSQL = sSQL & " --drop table #dias_vigencia " & vbCrLf
    sSQL = sSQL & " select #parcelas_contratadas.proposta_id, dias_vigencia =  cast(convert(decimal,365) / #parcelas_contratadas.parcelas as numeric(10,3)) * #parcelas_pagas.qtd_parcelas_pagas  " & vbCrLf
    sSQL = sSQL & " into #dias_vigencia " & vbCrLf
    sSQL = sSQL & " from #parcelas_contratadas " & vbCrLf
    sSQL = sSQL & " inner join #parcelas_pagas " & vbCrLf
    sSQL = sSQL & "     on #parcelas_pagas.proposta_id = #parcelas_contratadas.proposta_id " & vbCrLf
    sSQL = sSQL & " -- select * from #dias_vigencia " & vbCrLf

    '-- 02 parcela liq iof
    'sSQL = sSQL & " drop table #parcela_liq_iof"
    'sSQL = ""
    sSQL = sSQL & " select  agendamento_cobranca_tb.proposta_id, num_cobranca, val_pago, val_iof, val_pago / (1 + val_iof) parcela_liq_iof " & vbCrLf
    sSQL = sSQL & " into #parcela_liq_iof " & vbCrLf
    sSQL = sSQL & " from agendamento_cobranca_tb  WITH (NOLOCK) " & vbCrLf
    sSQL = sSQL & " inner join #dias_vigencia " & vbCrLf
    sSQL = sSQL & "     on #dias_vigencia.proposta_id = agendamento_cobranca_tb.proposta_id " & vbCrLf
    sSQL = sSQL & " where 1 = 1 " & vbCrLf
    sSQL = sSQL & "     and situacao = 'a' " & vbCrLf
    sSQL = sSQL & "     and dt_recebimento is not null " & vbCrLf
    '-- select * from #parcela_liq_iof

    '-- 03 premio ao dia
    'sSQL = sSQL & " drop table #premio_dia"
    'sSQL = ""
    sSQL = sSQL & " select #parcela_liq_iof.proposta_id , premio_dia = sum(parcela_liq_iof) / dias_vigencia " & vbCrLf
    sSQL = sSQL & " into #premio_dia " & vbCrLf
    sSQL = sSQL & " from #parcela_liq_iof " & vbCrLf
    sSQL = sSQL & " inner join #dias_vigencia " & vbCrLf
    sSQL = sSQL & "     on #dias_vigencia.proposta_id = #parcela_liq_iof.proposta_id " & vbCrLf
    sSQL = sSQL & " group by #parcela_liq_iof.proposta_id, #dias_vigencia.dias_vigencia " & vbCrLf
    '-- select * from #premio_dia

    '-- 04 premio periodo vigorou
    'sSQL = sSQL & " drop table #Premio_periodo_Vigencia"
    'sSQL = ""
    sSQL = sSQL & " select proposta_adesao_tb.proposta_id, " & vbCrLf
    sSQL = sSQL & " DateDiff(d, ISNULL(proposta_adesao_tb.dt_inicio_vigencia, proposta_tb.dt_contratacao), ISNULL(emi_proposta_tb.dt_cancelamento, emi_proposta_tb.dt_endosso)) vigencia " & vbCrLf
    sSQL = sSQL & " ,premio_periodo_vig = DateDiff(d, ISNULL(proposta_adesao_tb.dt_inicio_vigencia, proposta_tb.dt_contratacao), ISNULL(emi_proposta_tb.dt_cancelamento, emi_proposta_tb.dt_endosso)) * premio_dia " & vbCrLf
    sSQL = sSQL & " into #Premio_periodo_Vigencia " & vbCrLf
    sSQL = sSQL & " from emi_proposta_tb  WITH (NOLOCK)  " & vbCrLf
    sSQL = sSQL & " INNER JOIN proposta_adesao_tb   WITH (NOLOCK)  " & vbCrLf
    sSQL = sSQL & "     ON proposta_adesao_tb.proposta_bb = emi_proposta_tb.proposta_bb " & vbCrLf
    sSQL = sSQL & " Inner Join proposta_tb   WITH (NOLOCK)  " & vbCrLf
    sSQL = sSQL & "     on proposta_tb.proposta_id = proposta_adesao_tb.proposta_id " & vbCrLf
    sSQL = sSQL & " inner join #premio_dia " & vbCrLf
    sSQL = sSQL & "     on #premio_dia.proposta_id = proposta_tb.proposta_id " & vbCrLf
    sSQL = sSQL & "  where 1 = 1 " & vbCrLf
    sSQL = sSQL & "     and tipo_endosso in (63,64,80)"
    'sSQL = sSQL & "     and emi_proposta_tb.situacao <> 'c' " & vbCrLf
    'sSQL = sSQL & "     and emi_proposta_tb.dt_inicio_vigencia is not null " & vbCrLf
    'sSql = sSql & "    and emi_proposta_tb.tp_registro = '02' " & vbCrLf
    'sSql = sSql & " -- select * from #Premio_periodo_Vigencia " & vbCrLf

    Call rdocn.Execute(sSQL)

    '-- 05 saldo restituir
    sSQL = ""
    sSQL = sSQL & " select #Premio_periodo_Vigencia.proposta_id, " & vbCrLf
    sSQL = sSQL & " sum(parcela_liq_iof), premio_periodo_vig, " & vbCrLf
    sSQL = sSQL & " saldo_restituir = sum(parcela_liq_iof) - premio_periodo_vig " & vbCrLf
    '-- sSql = sSql & " into #Saldo_Restituir " & vbCrLf
    sSQL = sSQL & " from #Premio_periodo_Vigencia " & vbCrLf
    sSQL = sSQL & " inner join #parcela_liq_iof " & vbCrLf
    sSQL = sSQL & " on #parcela_liq_iof.proposta_id = #Premio_periodo_Vigencia.proposta_id " & vbCrLf
    sSQL = sSQL & " group by #Premio_periodo_Vigencia.proposta_id, premio_periodo_vig " & vbCrLf
    sSQL = sSQL & " order by #Premio_periodo_Vigencia.proposta_id " & vbCrLf

    Set rSr = rdocn.Execute(sSQL)

    'cristovao.rodrigues 25/04/2013 incluido verificacao .eof
    If rSr.EOF Then
        Retorno_Saldo_Restituir = 0
        'cristovao.rodrigues 05/07/2013 trecho comentado, incluido Goto Sair
        'MensagemBatch "N�o foi localizado valor a restituir para o endosso.", vbOKOnly, "saldo restituir", True
        'bObteveSaldoRestituir = False 'cristovao.rodrigues 05/07/2013 retirar , manter = TRUE
        'Exit Function
        GoTo sair
    Else
        Retorno_Saldo_Restituir = rSr("saldo_restituir")

    End If

    'cristovao.rodrigues 05/07/2013
sair:

    rSr.Close
    Set rSr = Nothing

    sSQL = ""
    sSQL = sSQL & " drop table #parcelas_pagas " & vbCrLf
    sSQL = sSQL & " drop table #parcelas_contratadas " & vbCrLf
    sSQL = sSQL & " drop table #dias_vigencia " & vbCrLf
    sSQL = sSQL & " drop table #parcela_liq_iof " & vbCrLf
    sSQL = sSQL & " drop table #premio_dia " & vbCrLf
    sSQL = sSQL & " drop table #Premio_periodo_Vigencia " & vbCrLf

    Call rdocn.Execute(sSQL)

    'rSr.Close
    'Set rSr = Nothing

    Exit Function

TrataErro:
    TrataErroGeral "Retorna_Saldo_Restituir", "frmEndosso"
    bObteveSaldoRestituir = False    'cristovao.rodrigues 25/04/2013
    'Resume
    End

End Function


Public Function CalcularPercSubvencaoFederal(ByVal cValSubvencaoInformado As Currency, _
                                             ByVal cValPremioBruto As Currency) As Double


    Dim dPercSubvencaoFederal As Double

    On Error GoTo TrataErro

    If cValPremioBruto = 0 Then
        cValSubvencaoInformado = 0
        CalcularPercSubvencaoFederal = cValSubvencaoInformado
        Exit Function
    End If

    dPercSubvencaoFederal = ((cValSubvencaoInformado / cValPremioBruto) * 100)

    If dPercSubvencaoFederal < 0 Then
        dPercSubvencaoFederal = 0
    End If

    CalcularPercSubvencaoFederal = dPercSubvencaoFederal

    Exit Function

TrataErro:
    Call TratarErro("CalcularPercSubvencaoFederal", Me.name)
    Call FinalizarAplicacao

End Function

Sub IncluirEndossoCancelamentoApolice(ByVal lApoliceId As Long, ByVal lSucursal As Long, _
                                      ByVal lSeguradora As Long, ByVal lPropostaId As Long, _
                                      ByVal LramoId As Long, _
                                      ByVal cValFinanceiro As Currency, _
                                      ByVal lTpEndossoId As Long, _
                                      ByVal cValPremioTarifa As Currency, _
                                      ByVal cValCustoEmissao As Currency, _
                                      ByVal cValCorretagem As Currency, _
                                      ByVal cValProLabore As Currency, ByVal cValIOF As Currency, _
                                      ByVal cValIR As Currency, ByVal cValDesconto As Currency, _
                                      ByVal cValAdicFracionamento As Currency, _
                                      ByVal lSeguroMoedaId As Long, ByVal lPremioMoedaId As Long, _
                                      ByVal cValParidadeMoeda As Currency, ByVal cValIs As Currency, _
                                      ByVal sDtPedidoEndosso As String, ByVal sDtEmissao As String, _
                                      ByVal lBeneficiarioRestituicaoClienteId As Long, ByVal cPercIR As Currency, _
                                      ByVal cValCliente As Currency, ByVal sFlagAprovacao As String, _
                                      ByVal sTextoEndosso As String, ByVal sDtBaixa As String, _
                                      ByVal sDtInicioCancelamento, ByVal sTpCancelamento As String, _
                                      ByVal sMotivoCancelamento As String)

    Dim oCancelamento As Object
    Dim colCorretagem As Collection
    Dim colAgendamento As New Collection

    On Error GoTo TrataErro

    Set colCorretagem = ObterCorretagem(cValPremioTarifa)
    Set oCancelamento = CreateObject("SEGL0140.cls00171")

    'Autor: romulo.barbosa (Nova Consultoria) - Data da Altera��o: 23/04/2012
    Dim fazerRestituicao As Boolean
    fazerRestituicao = False

    If (optCobRes And oDadosEndosso.ValRestituicao <> 0) Then
        fazerRestituicao = True
    End If


    Call oCancelamento.CancelarApolice(gsSIGLASISTEMA, App.Title, _
                                       App.FileDescription, glAmbiente_id, _
                                       IIf(TP_ENDOSSO_CONTESTACAO, "SEGP0777", cUserName), lApoliceId, lSucursal, _
                                       lSeguradora, lPropostaId, _
                                       LramoId, cValFinanceiro, _
                                       lTpEndossoId, cValPremioTarifa, _
                                       cValCustoEmissao, colAgendamento, _
                                       colCorretagem, cValCorretagem, _
                                       cValProLabore, cValIOF, _
                                       cValIR, cValDesconto, _
                                       cValAdicFracionamento, _
                                       lSeguroMoedaId, lPremioMoedaId, _
                                       cValParidadeMoeda, cValIs, _
                                       sDtPedidoEndosso, sDtEmissao, _
                                       lBeneficiarioRestituicaoClienteId, cPercIR, cValCliente, _
                                       sFlagAprovacao, _
                                       sTextoEndosso, _
                                       sDtBaixa, sDtInicioCancelamento, _
                                       sTpCancelamento, sMotivoCancelamento, , , _
                                       fazerRestituicao, txtProduto.Text)    'JOAO.MACHADO - INCLUINDO O PRODUTO_ID
    'Autor: romulo.barbosa (Nova Consultoria) - Data da Altera��o: 23/04/2012 (fazerRestituicao)

    '
    'Set oCancelamento = Nothing

    'Set oCancelamento = CreateObject("SEGL0140.cls00171")

    If bModular = True Then
        Call oCancelamento.CancelarApolice(gsSIGLASISTEMA, App.Title, _
                                           App.FileDescription, glAmbiente_id, _
                                           IIf(TP_ENDOSSO_CONTESTACAO, "SEGP0777", cUserName), oDadosModular.ApoliceId, oDadosModular.SucursalSeguradoraId, _
                                           oDadosModular.SucursalSeguradoraId, oDadosModular.PropostaId, _
                                           oDadosModular.RamoId, cValFinanceiro, _
                                           lTpEndossoId, oDadosModular.valPremioTarifa, _
                                           oDadosModular.valCustoEmissao, colAgendamento, _
                                           colCorretagem, oDadosModular.valCorretagem, _
                                           oDadosModular.ValProLabore, oDadosModular.valIOF, _
                                           cValIR, oDadosModular.valDesconto, _
                                           oDadosModular.valAdicFracionamento, _
                                           lSeguroMoedaId, lPremioMoedaId, _
                                           cValParidadeMoeda, cValIs, _
                                           sDtPedidoEndosso, sDtEmissao, _
                                           lBeneficiarioRestituicaoClienteId, cPercIR, cValCliente, _
                                           sFlagAprovacao, _
                                           sTextoEndosso, _
                                           sDtBaixa, sDtInicioCancelamento, _
                                           sTpCancelamento, sMotivoCancelamento, , , _
                                           IIf(oDadosEndosso.ValRestituicao <> 0, True, False))

        '
        Set oCancelamento = Nothing

    End If

    Exit Sub

TrataErro:

    Call TratarErro("IncluirEndossoCancelamentoApolice", Me.name)
    Call FinalizarAplicacao

End Sub

Private Sub IncluirEndossoEstipulante(ByVal lApoliceId As Long, ByVal lSucursal As Long, _
                                      ByVal lSeguradora As Long, ByVal LramoId As Long, _
                                      ByVal lPropostaId As Long, ByVal sDtPedidoEndosso As String, _
                                      ByVal lTpEndossoId As Long, ByVal sDtEmissao As String, _
                                      ByVal sTextoEndosso As String, ByRef colEstipulantes As Collection, _
                                      ByRef colSubGrupo As Collection)

    Dim OEndosso As Object

    Dim colCorretagem As Collection
    Dim colAgendamento As Collection

    On Error GoTo TrataErro

    Set OEndosso = CreateObject("SEGL0140.cls00172")

    Call OEndosso.GerarEndossoEstipulante(gsSIGLASISTEMA, App.Title, _
                                          App.FileDescription, glAmbiente_id, _
                                          lApoliceId, lSucursal, _
                                          lSeguradora, LramoId, _
                                          lPropostaId, sDtPedidoEndosso, _
                                          Data_Sistema, cUserName, _
                                          lTpEndossoId, sDtEmissao, _
                                          sTextoEndosso, colEstipulantes, _
                                          colSubGrupo)

    Set OEndosso = Nothing

    Exit Sub

TrataErro:
    Call TratarErro("IncluirEndossoEstipulante", Me.name)
    Call FinalizarAplicacao

End Sub


Private Sub ConsultarProposta(ByVal lPropostaId As Long)

    Dim oApolice As Object
    Dim rs As Recordset

    Dim sTpEmissao As String

    On Error GoTo TrataErro

    Set oApolice = CreateObject("SEGL0026.cls00126")

    Set rs = oApolice.ConsultarDados(gsSIGLASISTEMA, _
                                     App.Title, _
                                     App.FileDescription, _
                                     glAmbiente_id, _
                                     lPropostaId)

    Set oApolice = Nothing

    bApolice_Aberta = False

    If Not rs.EOF Then

        oDadosEndosso.PremioMoedaId = rs("premio_moeda_id")
        If Not IsNull(rs("dt_inicio_vigencia")) Then
            txtIniVigencia.Text = Format(rs("dt_inicio_vigencia"), "dd/mm/yyyy")
        End If
        If Not IsNull(rs("dt_fim_vigencia")) Then
            txtFimVigencia.Text = Format(rs("dt_fim_vigencia"), "dd/mm/yyyy")
        Else
            ' Ap�lice aberta ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            bApolice_Aberta = True
            fraIS.Visible = True
            fraIS.Refresh
            mskIS.Visible = True
            mskIS.Text = "0,00"
            Lbl(28).Visible = True
            mskFimVig.Visible = True
            optNovVal.Value = True
        End If

        sTpEmissao = Trim(UCase(rs("tp_emissao")))
        If sTpEmissao = "A" Then
            lblTpEmissao.Caption = "ACEITO"
        ElseIf sTpEmissao = "D" Then
            lblTpEmissao.Caption = "DIRETO"
        ElseIf sTpEmissao = "C" Then
            lblTpEmissao.Caption = "CEDIDO"
        End If

        txtNomeEstipulante(1).Text = IIf(IsNull(rs("estipulante_id")), "", rs("estipulante_id") & " - " & rs("estipulante_nome"))

        oDadosEndosso.PropostaId = rs("proposta_id")
        oDadosEndosso.ApoliceId = rs("apolice_id")
        oDadosEndosso.RamoId = rs("ramo_id")
        oDadosEndosso.SeguradoraCodSusep = rs("seguradora_cod_susep")
        oDadosEndosso.SucursalSeguradoraId = rs("sucursal_seguradora_id")
        oDadosEndosso.valCustoApoliceCert = rs("custo")
        oDadosEndosso.produtoId = rs("produto_id")
        oDadosEndosso.TpEmissao = Trim(UCase(rs("tp_emissao")))
        oDadosEndosso.ProdutoExternoId = IIf(IsNull(rs("produto_externo_id")), 0, rs("produto_externo_id"))


        If Trim(rs("tipo")) = "F" Then
            'lblCustoCertificadoRes.Caption = "Custo Ap�lice:"
            lblLegendaEndosso(0).Caption = "Custo Ap�lice:"
        Else
            'lblCustoCertificadoRes.Caption = "Custo Certificado:"
            lblLegendaEndosso(0).Caption = "Custo Certificado:"
        End If

    End If

    Set rs = Nothing

    Exit Sub
    Resume
TrataErro:
    Call TratarErro("ConsultarProposta", Me.name)
    Call FinalizarAplicacao

End Sub
Private Sub ConsultarPropostaModular(ByVal lPropostaId As Long)

    Dim oApolice As Object
    Dim rs As Recordset

    Dim sTpEmissao As String

    On Error GoTo TrataErro

    Set oApolice = CreateObject("SEGL0026.cls00126")


    '-- (in�cio) -- rsouza -- 30/09/2010 -------Demanda (5384394)-----------------------'
    '-- Demanda: (5384394)
    '-----------------------------------------------------------------------------------'
    'Antes da modifica��o:
    'Set rs = oApolice.ConsultarDados(gsSIGLASISTEMA, _
     '                                 App.Title, _
     '                                 App.FileDescription, _
     '                                 glAmbiente_id, _
     '                                 lPropostaId)
    '------------------------------------------------------------------------------------'
    'Depois da modifica��o:
    Set rs = oApolice.ConsultarDados(gsSIGLASISTEMA, _
                                     App.Title, _
                                     App.FileDescription, _
                                     glAmbiente_id, _
                                     lPropostaId, , , , , , , True)
    '-- (fim)    -- rsouza -- 30/09/2010 ------------------------------------------------'



    Set oApolice = Nothing

    If Not rs.EOF Then

        oDadosModular.PremioMoedaId = rs("premio_moeda_id")
        If Not IsNull(rs("dt_inicio_vigencia")) Then
            'txtIniVigencia.Text = Format(rs("dt_inicio_vigencia"), "dd/mm/yyyy")
            oDadosModular.DtInicioVigencia = Format(rs("dt_inicio_vigencia"), "dd/mm/yyyy")
        End If
        If Not IsNull(rs("dt_fim_vigencia")) Then
            'txtFimVigencia.Text = Format(rs("dt_fim_vigencia"), "dd/mm/yyyy")
            oDadosModular.DtFimVigencia = Format(rs("dt_fim_vigencia"), "dd/mm/yyyy")
        End If


        oDadosModular.SubRamoId = rs("subramo_id")
        'oDadosModular.TipoEndosso = cboTipoEndosso.ItemData(cboTipoEndosso.ListIndex)
        oDadosModular.PropostaId = rs("proposta_id")
        oDadosModular.ApoliceId = rs("apolice_id")
        oDadosModular.RamoId = rs("ramo_id")
        oDadosModular.SeguradoraCodSusep = rs("seguradora_cod_susep")
        oDadosModular.SucursalSeguradoraId = rs("sucursal_seguradora_id")
        oDadosModular.valCustoApoliceCert = rs("custo")
        oDadosModular.produtoId = rs("produto_id")
        oDadosModular.TpEmissao = Trim(UCase(rs("tp_emissao")))
        oDadosModular.ProdutoExternoId = IIf(IsNull(rs("produto_externo_id")), 0, rs("produto_externo_id"))

        'Call ConsultarEndossoModular(lPropostaId)

    End If

    Set rs = Nothing

    Exit Sub

TrataErro:
    Call TratarErro("ConsultarProposta", Me.name)
    Call FinalizarAplicacao

End Sub

Private Sub ConsultarPercIR()

    Dim oFinanceiro As Object

    On Error GoTo TrataErro

    Set oFinanceiro = CreateObject("SEGL0026.cls00125")

    oDadosEndosso.PercIR = CDbl(oFinanceiro.ObterPercentualItemFinanceiro(cUserName, _
                                                                          gsSIGLASISTEMA, _
                                                                          App.Title, _
                                                                          App.FileDescription, _
                                                                          glAmbiente_id, _
                                                                          "IR", _
                                                                          oDadosEndosso.RamoId, _
                                                                          Data_Sistema, _
                                                                          "19000101", _
                                                                          0))

    Set oFinanceiro = Nothing

    Exit Sub

TrataErro:
    Call TratarErro("ConsultarPercIR", Me.name)
    Call FinalizarAplicacao

End Sub

Private Sub ConsultarPercIOF(ByVal dtIOF As String, ByVal Subramo_id As Integer)

    Dim oFinanceiro As Object

    On Error GoTo TrataErro

    If IsDate(dtIOF) Then
        dtIOF = Format(dtIOF, "yyyymmdd")
    End If

    Set oFinanceiro = CreateObject("SEGL0026.cls00125")

    oDadosEndosso.PercIOF = CDbl(oFinanceiro.ObterPercentualItemFinanceiro(cUserName, _
                                                                           gsSIGLASISTEMA, _
                                                                           App.Title, _
                                                                           App.FileDescription, _
                                                                           glAmbiente_id, _
                                                                           "IOF", _
                                                                           oDadosEndosso.RamoId, _
                                                                           Data_Sistema, _
                                                                           dtIOF, _
                                                                           Subramo_id))

    Set oFinanceiro = Nothing

    Exit Sub

TrataErro:
    Call TratarErro("ConsultarPercIOF", Me.name)
    Call FinalizarAplicacao

End Sub


Private Sub ConsultarPercIOFModular(ByVal dtIOF As String, ByVal Subramo_id As Integer)
    Dim oFinanceiro As Object
    'RSILVA
    On Error GoTo TrataErro

    If IsDate(dtIOF) Then
        dtIOF = Format(dtIOF, "yyyymmdd")
    End If

    Set oFinanceiro = CreateObject("SEGL0026.cls00125")

    oDadosModular.PercIOF = CDbl(oFinanceiro.ObterPercentualItemFinanceiro(cUserName, _
                                                                           gsSIGLASISTEMA, _
                                                                           App.Title, _
                                                                           App.FileDescription, _
                                                                           glAmbiente_id, _
                                                                           "IOF", _
                                                                           oDadosEndosso.RamoId, _
                                                                           Data_Sistema, _
                                                                           dtIOF, _
                                                                           Subramo_id))

    Set oFinanceiro = Nothing

    Exit Sub

TrataErro:
    Call TratarErro("ConsultarPercIOF", Me.name)
    Call FinalizarAplicacao

End Sub

Public Function CalcularRepasse() As Currency

    Dim oFinanceiro As Object

    On Error GoTo TrataErro

    Set oFinanceiro = CreateObject("SEGL0026.cls00125")

    CalcularRepasse = oFinanceiro.CalcularParteSeguradoraRestituicao(gsSIGLASISTEMA, _
                                                                     App.Title, _
                                                                     App.FileDescription, _
                                                                     glAmbiente_id, _
                                                                     oDadosEndosso.ValRestituicao, _
                                                                     oDadosEndosso.valIOF, _
                                                                     oDadosEndosso.valCorretagem, _
                                                                     oDadosEndosso.ValProLabore, _
                                                                     TruncaDecimal(oDadosEndosso.valCorretagem * oDadosEndosso.PercIR), _
                                                                     TruncaDecimal(oDadosEndosso.ValProLabore * oDadosEndosso.PercIR), _
                                                                     oDadosEndosso.produtoId, _
                                                                     oDadosEndosso.RamoId, _
                                                                     1, _
                                                                     oDadosEndosso.PremioMoedaId)

    Set oFinanceiro = Nothing

    Exit Function

TrataErro:
    Call TratarErro("CalcularRepasse", Me.name)
    Call FinalizarAplicacao

End Function

Public Function CalcularRepasseModular() As Currency
'rsilva
    Dim oFinanceiro As Object

    On Error GoTo TrataErro

    Set oFinanceiro = CreateObject("SEGL0026.cls00125")

    If iModular = 0 Then
        CalcularRepasseModular = oFinanceiro.CalcularParteSeguradoraRestituicao(gsSIGLASISTEMA, _
                                                                                App.Title, _
                                                                                App.FileDescription, _
                                                                                glAmbiente_id, _
                                                                                oDadosEndosso.ValRestituicao, _
                                                                                oDadosEndosso.valIOF, _
                                                                                oDadosEndosso.valCorretagem, _
                                                                                oDadosEndosso.ValProLabore, _
                                                                                TruncaDecimal(oDadosEndosso.valCorretagem * oDadosEndosso.PercIR), _
                                                                                TruncaDecimal(oDadosEndosso.ValProLabore * oDadosEndosso.PercIR), _
                                                                                oDadosEndosso.produtoId, _
                                                                                oDadosEndosso.RamoId, _
                                                                                1, _
                                                                                oDadosEndosso.PremioMoedaId)
    Else
        If txtProduto.Text <> 1168 Then
            CalcularRepasseModular = oFinanceiro.CalcularParteSeguradoraRestituicao(gsSIGLASISTEMA, _
                                                                                    App.Title, _
                                                                                    App.FileDescription, _
                                                                                    glAmbiente_id, _
                                                                                    oDadosModular.ValRestituicao, _
                                                                                    oDadosModular.valIOF, _
                                                                                    oDadosModular.valCorretagem, _
                                                                                    oDadosModular.ValProLabore, _
                                                                                    TruncaDecimal(oDadosModular.valCorretagem * oDadosModular.PercIR), _
                                                                                    TruncaDecimal(oDadosModular.ValProLabore * oDadosModular.PercIR), _
                                                                                    oDadosModular.produtoId, _
                                                                                    oDadosModular.RamoId, _
                                                                                    1, _
                                                                                    oDadosModular.PremioMoedaId)
        Else
            CalcularRepasseModular = oFinanceiro.CalcularParteSeguradoraRestituicao(gsSIGLASISTEMA, _
                                                                                    App.Title, _
                                                                                    App.FileDescription, _
                                                                                    glAmbiente_id, _
                                                                                    oDadosEndosso.ValRestituicao, _
                                                                                    oDadosEndosso.valIOF, _
                                                                                    oDadosEndosso.valCorretagem, _
                                                                                    oDadosEndosso.ValProLabore, _
                                                                                    TruncaDecimal(oDadosEndosso.valCorretagem * oDadosEndosso.PercIR), _
                                                                                    TruncaDecimal(oDadosEndosso.ValProLabore * oDadosEndosso.PercIR), _
                                                                                    oDadosEndosso.produtoId, _
                                                                                    oDadosEndosso.RamoId, _
                                                                                    1, _
                                                                                    oDadosEndosso.PremioMoedaId)

        End If
    End If


    Set oFinanceiro = Nothing

    Exit Function

TrataErro:
    Call TratarErro("CalcularRepasse", Me.name)
    Call FinalizarAplicacao

End Function

Private Sub ConsultarBeneficiario(ByVal lPropostaId As Long)

    Dim oApolice As Object

    On Error GoTo TrataErro

    Set oApolice = CreateObject("SEGL0026.cls00126")

    oDadosEndosso.BeneficiarioId = oApolice.ObterBeneficiario(gsSIGLASISTEMA, _
                                                              App.Title, _
                                                              App.FileDescription, _
                                                              glAmbiente_id, _
                                                              lPropostaId)

    Set oApolice = Nothing

    Exit Sub

TrataErro:
    Call TratarErro("ConsultarBeneficiario", Me.name)
    Call FinalizarAplicacao

End Sub

'27/12/2015 - schoralick (nova consultoria) - 18690344 - Melhorias Processo Reagendamento e Reativa��o BESC
Private Function atualizarPropostaImpedimento() As Boolean

    Dim oReativacao As Object
    Dim Resultado As Boolean
    Resultado = False

    Set oReativacao = CreateObject("SEGL0140.cls00172")

    On Error GoTo TrataErro

    Call oReativacao.atualizarPropostaImpdimento(gsSIGLASISTEMA, _
                                                 App.Title, _
                                                 App.FileDescription, _
                                                 glAmbiente_id, _
                                                 txtPropostaid(0).Text, _
                                                 chkImpedirAgendamento.Value, _
                                                 cUserName, _
                                                 0, _
                                                 oDadosEndosso.TipoEndosso, _
                                                 mskDtEndosso.Text, _
                                                 mskDtEmissao.Text, _
                                                 Trim(txtDescEndosso.Text), _
                                                 mskDtReativacao.Text, _
                                                 lEndossoCancelamentoId, _
                                                 Data_Sistema)


    Set oReativacao = Nothing

    Resultado = True
    atualizarPropostaImpedimento = Resultado

    Exit Function

TrataErro:
    Call TratarErro("atualizarPropostaImpedimento", Me.name)
    Call FinalizarAplicacao
End Function

Private Sub AplicarReativacaoProposta()

    Dim oReativacao As Object
    Dim colCobranca As Collection
    Dim oCobranca As Object
    Dim iIndice As Integer
    '27/12/2015 - schoralick (nova consultoria) - 18690344 - Melhorias Processo Reagendamento e Reativa��o BESC - inicio
    Dim lTempUltimaParcelaReativacao As Integer

    Dim sLinha As String
    Dim msg As String
    Dim i As Integer
    Dim sData As String
    '27/12/2015 - schoralick (nova consultoria) - 18690344 - Melhorias Processo Reagendamento e Reativa��o BESC - fim
    On Error GoTo TrataErro

    If Not VerificaData2(mskDtReativacao.Text) Then
        MsgBox "Data de reativa��o incorreta.", vbCritical
        mskDtReativacao.SetFocus
        Exit Sub
    End If

    If DateValue(mskDtReativacao.Text) < DateValue(Data_Sistema) Then
        MsgBox "Data de reativa��o n�o pode ser menor que a data do sistema.", vbCritical
        mskDtReativacao.Text = mskDtEndosso.Text
        mskDtReativacao.SetFocus
        Exit Sub
    End If

    'bmoraes 06.05.2010 - reativa��o de propostas sem cobran�a
    '    If grdParcelasReativacao.Rows = 1 Then
    '        MsgBox "N�o foram agendadas parcelas para a proposta.", vbCritical
    '        grdParcelasReativacao.SetFocus
    '        Exit Sub
    '    End If
    ' bmoraes - FIM

    If lEndossoCancelamentoId = 0 Then
        MsgBox "A proposta n�o est� cancelada", vbCritical
        Exit Sub
    End If

    '27/12/2015 - schoralick (nova consultoria) - 18690344 - Melhorias Processo Reagendamento e Reativa��o BESC - inicio
    Select Case vlProdutoId
    Case 200, 201, 202, 203, 204, 205, 208, 209, 210, 211, 213, 216, 217, 218, 219, 221, 222, 223, 224, 225, 226
        lTempUltimaParcelaReativacao = lUltimaParcelaReativacao
        'marcando todas as linhas para valida��o
        sLinha = ""
        For i = 1 To grdParcelasReativacao.Rows - 1
            sLinha = sLinha & "(" & txtPropostaid(0).Text & ","
            If grdParcelasReativacao.TextMatrix(i, 3) = "c" Then
                sLinha = sLinha & lTempUltimaParcelaReativacao & ","    'num_parcela
                lTempUltimaParcelaReativacao = lTempUltimaParcelaReativacao + 1
            Else
                sLinha = sLinha & grdParcelasReativacao.TextMatrix(i, 0) & ","    'num_parcela
            End If
            sLinha = sLinha & "'a'" & ","    ' situacao
            sLinha = sLinha & grdParcelasReativacao.TextMatrix(i, 4) & ","  'mes_ano_ref
            sData = Mid$(grdParcelasReativacao.TextMatrix(i, 2), 7, 4) & "-" & Mid$(grdParcelasReativacao.TextMatrix(i, 2), 4, 2) & "-" & Mid$(grdParcelasReativacao.TextMatrix(i, 2), 1, 2)
            sLinha = sLinha & "'" & sData & "',"    'data_parcela
            sLinha = sLinha & "'" & Replace(Replace(grdParcelasReativacao.TextMatrix(i, 1), ".", ""), ",", ".") & "',"    'valor
            sLinha = sLinha & i & ")|"
        Next i

        msg = ValidaParcela(sLinha)
        If msg <> "" Then
            MsgBox (msg)
            Exit Sub
        End If

        Set colCobranca = New Collection

        For iIndice = 1 To grdParcelasReativacao.Rows - 1

            Set oCobranca = New Cobranca

            If grdParcelasReativacao.TextMatrix(iIndice, 3) = "c" Then
                oCobranca.NumCobranca = lUltimaParcelaReativacao
                lUltimaParcelaReativacao = lUltimaParcelaReativacao + 1
            Else
                oCobranca.NumCobranca = grdParcelasReativacao.TextMatrix(iIndice, 0)
            End If

            oCobranca.ValCobranca = grdParcelasReativacao.TextMatrix(iIndice, 1)
            oCobranca.DtCobranca = grdParcelasReativacao.TextMatrix(iIndice, 2)
            oCobranca.AnoMesRef = grdParcelasReativacao.TextMatrix(iIndice, 4)
            colCobranca.Add oCobranca, CStr(iIndice)
        Next

        Set oCobranca = Nothing

        Set oReativacao = CreateObject("SEGL0140.cls00172")

        Call oReativacao.GerarEndossoReativacaoBesc(gsSIGLASISTEMA, _
                                                    App.Title, _
                                                    App.FileDescription, _
                                                    glAmbiente_id, _
                                                    cUserName, _
                                                    oDadosEndosso.PropostaId, _
                                                    oDadosEndosso.TipoEndosso, _
                                                    mskDtEndosso.Text, _
                                                    mskDtEmissao.Text, _
                                                    Trim(txtDescEndosso.Text), _
                                                    0, _
                                                    oDadosEndosso.RamoId, _
                                                    oDadosEndosso.ApoliceId, _
                                                    oDadosEndosso.SeguradoraCodSusep, _
                                                    oDadosEndosso.SucursalSeguradoraId, _
                                                    lEndossoCancelamentoId, _
                                                    mskDtReativacao.Text, _
                                                    oDadosEndosso.produtoId, _
                                                    Data_Sistema, _
                                                    colCobranca)

        Set oReativacao = Nothing
        Set colCobranca = Nothing
    Case Else
        '27/12/2015 - schoralick (nova consultoria) - 18690344 - Melhorias Processo Reagendamento e Reativa��o BESC - fim
        Set colCobranca = New Collection

        ' Autor: pablo.dias (Nova Consultoria) - Data da Altera��o: 08/03/2013
        ' Demanda: 17789414 - Melhorias SEGBR para aumento da capacidade de vendas BB
        If oDadosEndosso.produtoId = OURO_RESIDENCIAL_ESTILO Then
            grdParcelasReativacao.Rows = 1
        End If

        For iIndice = 1 To grdParcelasReativacao.Rows - 1

            Set oCobranca = New Cobranca

            oCobranca.NumCobranca = grdParcelasReativacao.TextMatrix(iIndice, 0)
            oCobranca.ValCobranca = grdParcelasReativacao.TextMatrix(iIndice, 1)
            oCobranca.DtCobranca = grdParcelasReativacao.TextMatrix(iIndice, 2)

            colCobranca.Add oCobranca, CStr(iIndice)

        Next

        Set oCobranca = Nothing

        Set oReativacao = CreateObject("SEGL0140.cls00172")

        Call oReativacao.GerarEndossoReativacao(gsSIGLASISTEMA, _
                                                App.Title, _
                                                App.FileDescription, _
                                                glAmbiente_id, _
                                                cUserName, _
                                                oDadosEndosso.PropostaId, _
                                                oDadosEndosso.TipoEndosso, _
                                                mskDtEndosso.Text, _
                                                mskDtEmissao.Text, _
                                                Trim(txtDescEndosso.Text), _
                                                0, _
                                                oDadosEndosso.RamoId, _
                                                oDadosEndosso.ApoliceId, _
                                                oDadosEndosso.SeguradoraCodSusep, _
                                                oDadosEndosso.SucursalSeguradoraId, _
                                                lEndossoCancelamentoId, _
                                                mskDtReativacao.Text, _
                                                oDadosEndosso.produtoId, _
                                                Data_Sistema, _
                                                colCobranca)

        Set oReativacao = Nothing
        Set colCobranca = Nothing
    End Select


    Call MsgBox("Reativa��o realizada com sucesso." & vbNewLine & _
                "Proposta: " & oDadosEndosso.PropostaId & vbNewLine, vbInformation)

    Call sair

    Exit Sub

TrataErro:
    Call TratarErro("AplicarReativacaoProposta", Me.name)
    Call FinalizarAplicacao
End Sub

Private Sub AplicarCancelamentoEndosso()

    Dim iEndossoSegBr As Integer

    Const TP_CANCELAMENTO = "2"

    On Error GoTo TrataErro

    If Not IsDate(mskDtVigCanEndosso.Text) Then
        MsgBox "Data Inicio Vig�ncia Incorreta.", vbCritical
        mskDtVigCan.SetFocus
        Exit Sub
    End If

    If CDate(mskDtVigCanEndosso.Text) < Data_Sistema Then
        MsgBox "Data de Cancelamento n�o pode ser menor que data do sistema.", vbCritical
        mskDtVigCan.Text = mskDtEndosso.Text
        mskDtVigCan.SetFocus
        Exit Sub
    End If

    '    If chkEndossoNaLider.Value Then
    '        If Existe_Endosso_Aceito(txtNumero.Text, TxtNumEndossoCanc.Text, iEndossoSegBr) Then
    '            TxtNumEndossoCanc.Text = iEndossoSegBr
    '        Else
    '            MsgBox "Endosso na l�der n�o cadastrado ou inexistente.", vbCritical
    '            TxtNumEndossoCanc.SetFocus
    '            Exit Sub
    '        End If
    '    Else
    If Val(Trim(TxtNumEndossoCanc.Text)) = 0 Or Val(Trim(TxtNumEndossoCanc.Text)) >= Val(txtNumEndosso) Then
        MsgBox "N�mero do endosso incorreto.", vbCritical
        TxtNumEndossoCanc.SetFocus
        Exit Sub
    End If
    '    End If


    'Brauner Rangel nova consultoria

    If uTipoEndosso = 101 Then
        If uProduto = 200 Or _
           uProduto = 201 Or _
           uProduto = 202 Or _
           uProduto = 203 Or _
           uProduto = 204 Or _
           uProduto = 205 Or _
           uProduto = 208 Or _
           uProduto = 209 Or _
           uProduto = 210 Or _
           uProduto = 211 Or _
           uProduto = 212 Or _
           uProduto = 213 Or _
           uProduto = 216 Or _
           uProduto = 217 Or _
           uProduto = 218 Or _
           uProduto = 219 Or _
           uProduto = 220 Or _
           uProduto = 221 Or _
           uProduto = 224 Or _
           uProduto = 225 _
           Then
            frmReativacaoReagendamento.PropostaId = oDadosEndosso.PropostaId
            frmReativacaoReagendamento.EndossoId = oDadosEndosso.TipoEndosso

            frmReativacaoReagendamento.Show
            frmEndosso.Enabled = False
            Exit Sub
        End If
    End If


    Me.MousePointer = vbHourglass

    Call IncluirEndossoCancelamentoEndosso(oDadosEndosso.ApoliceId, _
                                           oDadosEndosso.SucursalSeguradoraId, _
                                           oDadosEndosso.SeguradoraCodSusep, _
                                           oDadosEndosso.PropostaId, _
                                           oDadosEndosso.RamoId, _
                                           0, _
                                           oDadosEndosso.TipoEndosso, _
                                           Val(TxtNumEndossoCanc.Text), _
                                           mskDtEndosso.Text, _
                                           mskDtEmissao.Text, _
                                           Trim(txtDescEndosso.Text), _
                                           mskDtVigCanEndosso.Text)

    Me.MousePointer = vbDefault

    Call MsgBox("Endosso realizado com sucesso." & vbNewLine & _
                "Proposta: " & oDadosEndosso.PropostaId & vbNewLine & _
                "Endosso : " & txtNumEndosso.Text, vbInformation)

    Call sair

    Exit Sub

TrataErro:
    Call TratarErro("AplicarCancelamentoEndosso", Me.name)
    Call FinalizarAplicacao

End Sub

Private Sub AplicarCancelamentoPorSinistro()

    Dim sSQL As String
    Dim oCancelamento As Object
    Dim sMensagem As String

    On Error GoTo TrataErro

    Me.MousePointer = vbHourglass

    Set oCancelamento = CreateObject("SEGL0140.cls00171")

    If oCancelamento.ValidarEndossoCancSinistro(oDadosEndosso.PropostaId, _
                                                oDadosEndosso.produtoId, _
                                                gsSIGLASISTEMA, _
                                                App.Title, _
                                                App.FileDescription, _
                                                glAmbiente_id, _
                                                sMensagem) Then

        Call oCancelamento.IncluirEndossoCancSinistro(oDadosEndosso.PropostaId, _
                                                      RetiraCaracterEspecial(Trim(txtMotivoCancelamentoSin.Text)), _
                                                      RetiraCaracterEspecial(Trim(txtDescEndosso.Text)), _
                                                      gsSIGLASISTEMA, _
                                                      App.Title, _
                                                      App.FileDescription, _
                                                      glAmbiente_id, cUserName)




        Me.MousePointer = vbDefault

        Call MsgBox("Endosso realizado com sucesso." & vbNewLine & _
                    "Proposta: " & oDadosEndosso.PropostaId & vbNewLine & _
                    "Endosso : " & txtNumEndosso.Text, vbInformation)

        Call sair

    Else

        Me.MousePointer = vbDefault

        Call MsgBox(sMensagem, vbCritical)

    End If

    Set oCancelamento = Nothing

    Exit Sub

TrataErro:
    Call TratarErro("AplicarCancelamentoPorSinistro", Me.name)
    Call FinalizarAplicacao
End Sub

'Private Function VerificarExistenciaEndossoAceito(ByVal lPropostaId As Long, _
 '                                                  ByVal iEndossoCosseguro As Integer, _
 '                                                  Optional ByRef iEndossoAceito As Integer) As Boolean
'
'Dim oEndosso As Object
'Dim bExiste As Boolean
'Dim rs As Recordset
'
'On Error GoTo TrataErro
'
'    sSQL = "" & vbNewLine
'    sSQL = sSQL & "SELECT endosso_id " & vbNewLine
'    sSQL = sSQL & "  FROM endosso_co_seguro_aceito_tb " & vbNewLine
'    sSQL = sSQL & " WHERE proposta_id = " & lPropostaId & vbNewLine
'    sSQL = sSQL & "   AND num_endosso_cosseguro = " & iEndossoCosseguro & vbNewLine
'
'    Set rs = rdocn.OpenResultset(sSQL)
'
'    If Not rs.EOF Then
'        bExiste = True
'        iEndossoAceito = rs("endosso_id")
'    Else
'        bExiste = False
'    End If
'
'    Set rs = Nothing
'
'    VerificarExistenciaEndossoAceito = bExiste
'
'    Exit Function
'
'TrataErro:
'    Call TratarErro("AplicarCancelamentoEndosso", me.name)
'    call FinalizarAplicacao
'
'End Function

Private Sub IncluirEndossoCancelamentoEndosso(ByVal lApoliceId As Long, ByVal lSucursal As Long, _
                                              ByVal lSeguradora As Long, ByVal lPropostaId As Long, _
                                              ByVal LramoId As Long, ByVal lNumEndossoBB As Long, _
                                              ByVal lTpEndossoId As Long, ByVal iEndossoCancId As Integer, _
                                              ByVal sDtPedidoEndosso As String, ByVal sDtEmissao As String, _
                                              ByVal sTextoEndosso As String, ByVal sDtBaixa As String)

    Dim oCancelamento As Object

    On Error GoTo TrataErro

    Set oCancelamento = CreateObject("SEGL0140.cls00171")

    Call oCancelamento.GerarCancelamentoEndosso(cUserName, _
                                                gsSIGLASISTEMA, _
                                                App.Title, _
                                                App.FileDescription, _
                                                glAmbiente_id, _
                                                txtApolice.Text, _
                                                LramoId, _
                                                lSucursal, _
                                                lSeguradora, _
                                                txtPropostaid(0).Text, _
                                                iEndossoCancId, _
                                                lTpEndossoId, _
                                                lNumEndossoBB, _
                                                sDtPedidoEndosso, _
                                                sDtEmissao, _
                                                mskDtVigCanEndosso.Text, _
                                                sDtBaixa, _
                                                sTextoEndosso, _
                                                lSeguro_moeda_id, _
                                                lPremio_moeda_id)
    'Luciana - 31/01/2005
    'Acrescentei os par�metros lSeguro_moeda_id e lPremio_moeda_id

    Set oCancelamento = Nothing

    Exit Sub

TrataErro:
    Call TratarErro("IncluirEndossoCancelamentoEndosso", Me.name)
    Call FinalizarAplicacao

End Sub

Private Sub AplicarCadastral()

    Dim iEndossoSegBr As Integer
    Dim sEstadoCivil As String

    Const TP_CANCELAMENTO = "2"

    On Error GoTo TrataErro

    ' Verificando se o nome foi preenchido ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

    If Trim(txtEndoCad(0).Text) = "" Then
        Call MsgBox("Nome n�o preenchido", vbCritical)
        txtEndoCad(0).SetFocus
        Exit Sub
    End If

    If cboTpPessoa.ItemData(cboTpPessoa.ListIndex) = 1 Then

        ' Verificando se o CPF foi preenchido '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        If Trim(txtCPFCNPJ.ClipText) = "" Then
            Call MsgBox("CPF n�o preenchido", vbCritical)
            txtEndoCad(0).SetFocus
            Exit Sub
        End If

        ' Verificando se a data de nascimento foi preenchida ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        If Trim(txtNascimento.ClipText) = "" Then
            Call MsgBox("Data de Nascimento n�o preenchida", vbCritical)
            txtEndoCad(0).SetFocus
            Exit Sub
        End If

        ' Verificando se o CPF foi preenchido '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        If cboSexo.ListIndex = -1 Then
            Call MsgBox("Sexo n�o preenchido", vbCritical)
            txtEndoCad(0).SetFocus
            Exit Sub
        End If

        sEstadoCivil = Trim(VetorEstadoCivil(cboEstCivil.ListIndex))

    Else

        ' Verificando se o CNPJ foi preenchido ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        If Trim(txtCPFCNPJ.ClipText) = "" Then
            Call MsgBox("CNPJ n�o preenchido", vbCritical)
            txtEndoCad(0).SetFocus
            Exit Sub
        End If

        sEstadoCivil = ""

    End If

    'R.FOUREAUX 25/06/2012 - DEMANDA 12006056
    ' VERIFICANDO PREENCHIMENTO DA CONTA E AG QUANDO FORMA DE PAGAMENTO FOR IGUAL A D�BITO E O TIPO ENDOSSO FOR FORMA PAGAMENTO
    If oDadosEndosso.TipoEndosso = TP_ALTERACAO_FORMA_PAGAMENTO Then
        If cboFormaPagamento.ItemData(cboFormaPagamento.ListIndex) = 1 Then
            If txtCodAgDebito(0).Text = "" Then
                Call MsgBox("Agencia em branco!", vbCritical)
                txtCodAgDebito(0).SetFocus
                Exit Sub
            End If
            If txtEndoCad(7).Text = "" Then
                Call MsgBox("Conta Corrente em branco!", vbCritical)
                txtEndoCad(7).SetFocus
                Exit Sub
            End If
        End If
    End If
    Me.MousePointer = vbHourglass




    If oDadosEndosso.TipoEndosso <> TP_ALTERACAO_FORMA_PAGAMENTO Then
        Call IncluirEndossoDadosCadastrais(oDadosEndosso.PropostaId, oDadosEndosso.TipoEndosso, _
                                           mskDtEndosso.Text, mskDtEmissao.Text, _
                                           Trim(txtDescEndosso.Text), Data_Sistema, cUserName, _
                                           oDadosEndosso.ClienteId, Trim(txtNomeAnt.Text), _
                                           Trim(txtSexoAnt.Text), Trim(txtEstCivilAnt.Text), _
                                           sEndereco, Trim(TxtTelefoneAnt.Text), _
                                           Trim(TxtDDDAnt.Text), sCidade, _
                                           sUF, sCep, _
                                           Trim(TxtEmailAnt.Text), Trim(TxtCodAgAnt.Text), _
                                           Trim(TxtCCAnt.Text), Trim(txtNascAnt.Text), _
                                           Trim(txtCPFCNPJAnt.Text), Trim(txtEndoCad(8).Text), _
                                           sBairro, Trim(txtIdTpPessoaAnt.Text), _
                                           Trim(txtEndoCad(0).Text), Trim(Left(cboSexo.Text, 1)), _
                                           sEstadoCivil, _
                                           sEndereco, Trim(txtEndoCad(3).Text), _
                                           Trim(txtEndoCad(2).Text), sCidade, _
                                           sUF, sCep, _
                                           Trim(txtEndoCad(1).Text), Trim(txtCodAgDebito(0).Text), _
                                           Trim(txtEndoCad(7).Text), Trim(txtNascimento.Text), _
                                           Trim(txtCPFCNPJ.Text), Trim(txtBanco.Text), _
                                           sBairro, _
                                           Trim(cboTpPessoa.ItemData(cboTpPessoa.ListIndex)))
    Else

        Call IncluirEndossoDadosCadastrais(oDadosEndosso.PropostaId, oDadosEndosso.TipoEndosso, _
                                           mskDtEndosso.Text, mskDtEmissao.Text, _
                                           Trim(txtDescEndosso.Text), Data_Sistema, cUserName, _
                                           oDadosEndosso.ClienteId, Trim(txtNomeAnt.Text), _
                                           Trim(txtSexoAnt.Text), Trim(txtEstCivilAnt.Text), _
                                           sEndereco, Trim(TxtTelefoneAnt.Text), _
                                           Trim(TxtDDDAnt.Text), sCidade, _
                                           sUF, sCep, _
                                           Trim(TxtEmailAnt.Text), Trim(TxtCodAgAnt.Text), _
                                           Trim(TxtCCAnt.Text), Trim(txtNascAnt.Text), _
                                           Trim(txtCPFCNPJAnt.Text), Trim(txtEndoCad(8).Text), _
                                           sBairro, Trim(txtIdTpPessoaAnt.Text), _
                                           Trim(txtEndoCad(0).Text), Trim(Left(cboSexo.Text, 1)), _
                                           sEstadoCivil, _
                                           sEndereco, Trim(txtEndoCad(3).Text), _
                                           Trim(txtEndoCad(2).Text), sCidade, _
                                           sUF, sCep, _
                                           Trim(txtEndoCad(1).Text), Trim(txtCodAgDebito(0).Text), _
                                           Trim(txtEndoCad(7).Text), Trim(txtNascimento.Text), _
                                           Trim(txtCPFCNPJ.Text), Trim(txtBanco.Text), _
                                           sBairro, _
                                           Trim(cboTpPessoa.ItemData(cboTpPessoa.ListIndex)), _
                                           cboFormaPagamento.ItemData(cboFormaPagamento.ListIndex))  'R.FOUREAUX 25/06/2012 - DEMANDA 12006056

    End If




    Me.MousePointer = vbDefault

    Call MsgBox("Endosso realizado com sucesso." & vbNewLine & _
                "Proposta: " & oDadosEndosso.PropostaId & vbNewLine & _
                "Endosso : " & txtNumEndosso.Text, vbInformation)

    Call sair

    Exit Sub

TrataErro:
    Call TratarErro("AplicarCancelamentoEndosso", Me.name)
    Call FinalizarAplicacao

End Sub

Private Sub IncluirEndossoDadosCadastrais(ByVal lPropostaId As Long, ByVal lTpEndossoId As Long, _
                                          ByVal sDtPedidoEndosso As String, ByVal sDtEmissao As String, _
                                          ByVal sTextoEndosso As String, ByVal sDataSistema As String, _
                                          ByVal sUsuario As String, ByVal lClienteId As Long, _
                                          ByVal sUltNome As String, ByVal sUltSexo As String, _
                                          ByVal sUltEstadoCivil As String, ByVal sUltEndereco As String, _
                                          ByVal sUltTelefone As String, ByVal sUltDdd As String, _
                                          ByVal sUltMunicipio As String, ByVal sUltEstado As String, _
                                          ByVal sUltCep As String, ByVal sUltEmail As String, _
                                          ByVal sUltDebAgenciaId, ByVal sUltDebContaId As String, _
                                          ByVal sUltDtNascimento As String, ByVal sUltCgcCpf As String, _
                                          ByVal sUltBanco As String, ByVal sUltBairro As String, _
                                          ByVal sUltTpPessoa As String, ByVal sNome As String, _
                                          ByVal sSexo As String, ByVal sEstadoCivil As String, _
                                          ByVal sEndereco As String, ByVal sTelefone As String, _
                                          ByVal sDDD As String, ByVal sMunicipio As String, _
                                          ByVal sEstado As String, ByVal sCep As String, _
                                          ByVal sEmail As String, ByVal sDebAgenciaId As String, _
                                          ByVal sDebContaId As String, ByVal sDtNascimento As String, _
                                          ByVal sCgcCpf As String, ByVal sBanco As String, _
                                          ByVal sBairro As String, ByVal sTpPessoa As String, Optional ByVal iFormaPgto As Integer)    'R.FOUREAUX 25/06/2012 - DEMANDA 12006056

    Dim OEndosso As Object

    On Error GoTo TrataErro

    'Gabriel C�mara - Confitec - INC000004251295 - Tratamento CPF
    sUltCgcCpf = DatCPF(sUltCgcCpf)
    sCgcCpf = DatCPF(sCgcCpf)

    Set OEndosso = CreateObject("SEGL0140.cls00172")

    Call OEndosso.GerarEndossoDadosCadastrais(gsSIGLASISTEMA, App.Title, _
                                              App.FileDescription, glAmbiente_id, _
                                              lPropostaId, _
                                              lClienteId, sDtPedidoEndosso, _
                                              sUltNome, sUltSexo, _
                                              sUltEstadoCivil, sUltEndereco, _
                                              sUltTelefone, sUltDdd, _
                                              Mid(sUltMunicipio, 1, Len(sUltMunicipio) - 5), sUltEstado, _
                                              RetiraQualquerCaracter(sUltCep), sUltEmail, _
                                              sUltDebAgenciaId, sUltDebContaId, _
                                              sUltDtNascimento, sUltCgcCpf, _
                                              sUltBanco, sUltBairro, _
                                              sUltTpPessoa, sNome, sSexo, _
                                              sEstadoCivil, sEndereco, _
                                              sTelefone, sDDD, _
                                              Mid(sMunicipio, 1, Len(sMunicipio) - 5), sEstado, _
                                              RetiraQualquerCaracter(sCep), sEmail, _
                                              sDebAgenciaId, sDebContaId, _
                                              sDtNascimento, sCgcCpf, _
                                              sBanco, sBairro, _
                                              sTpPessoa, sDataSistema, _
                                              sUsuario, lTpEndossoId, _
                                              sDtEmissao, sTextoEndosso, iFormaPgto)

    Set OEndosso = Nothing

    Exit Sub

TrataErro:
    Call TratarErro("IncluirEndossoCancelamentoEndosso", Me.name)
    Call FinalizarAplicacao

End Sub

Private Sub MontarComboCidade(ByRef cboCidade As ComboBox)

    Dim oDadosBasicos As Object
    Dim rsCidade As Recordset

    cboCidade.Clear

    Set oDadosBasicos = CreateObject("SEGL0022.cls00117")

    Set rsCidade = oDadosBasicos.ConsultarMunicipio(gsSIGLASISTEMA, _
                                                    App.Title, _
                                                    App.FileDescription, _
                                                    glAmbiente_id)

    While Not rsCidade.EOF
        cboCidade.AddItem Trim(rsCidade("nome")) & " - " & Trim(rsCidade("estado"))
        cboCidade.ItemData(cboCidade.NewIndex) = rsCidade("municipio_id")
        rsCidade.MoveNext
    Wend

    Set rsCidade = Nothing
    Set oDadosBasicos = Nothing

    Exit Sub

End Sub

Public Sub MontarComboTaxaJuros()

    Dim oFinanceiro As Object
    Dim sSQL As String
    Dim rs As Recordset

    On Error GoTo TrataErro

    Set oFinanceiro = CreateObject("SEGL0026.cls00125")

    Set rs = oFinanceiro.ConsultarJuros(gsSIGLASISTEMA, _
                                        App.Title, _
                                        App.FileDescription, _
                                        glAmbiente_id)

    Set oFinanceiro = Nothing

    cmbTaxaJuros.Clear
    cmbTaxaJuros.AddItem "< Nenhum >"

    While Not rs.EOF
        cmbTaxaJuros.AddItem Format(rs("perc_juros"), "###0.0000")
        cmbTaxaJuros.ItemData(cmbTaxaJuros.NewIndex) = rs("juros_id")
        rs.MoveNext
    Wend

    cmbTaxaJuros.ListIndex = 0

    Set rs = Nothing

    Exit Sub

TrataErro:
    Call TratarErro("MontarComboTaxaJuros", Me.name)
    Call FinalizarAplicacao

End Sub

Private Function ObterCorretagemAvaliacaoRestituicao(ByVal cValPremioTarifa As Currency) As Collection

    Dim colCorretagem As New Collection
    Dim oCorretagem As Object
    Dim iCont As Integer

    On Error GoTo TrataErro

    For iCont = 1 To grdCorretagem.Rows - 1

        Set oCorretagem = CreateObject("SEGL0026.cls00149")

        oCorretagem.Corretor_ID = Val(Replace(grdCorretagem.TextMatrix(iCont, 0), "-", ""))
        oCorretagem.PercComissao = CCur(grdCorretagem.TextMatrix(iCont, 2))
        oCorretagem.Sucursal = Trim(grdCorretagem.TextMatrix(iCont, 3))
        oCorretagem.Tipo = IIf(Trim(grdCorretagem.TextMatrix(iCont, 4)) = "PJ", "1", "2")
        oCorretagem.Val_Comissao = TruncaDecimal(cValPremioTarifa * (oCorretagem.PercComissao / 100))
        oCorretagem.Val_IR = TruncaDecimal(oCorretagem.Val_Comissao * oDadosEndosso.PercIR)

        colCorretagem.Add oCorretagem

        Set oCorretagem = Nothing

    Next

    Set ObterCorretagemAvaliacaoRestituicao = colCorretagem

    Set colCorretagem = Nothing

    Exit Function

TrataErro:

    Call TratarErro("ObterCorretagemAvaliacaoRestituicao", Me.name)
    Call FinalizarAplicacao

End Function

Public Sub MontarGridPagamento()

    Dim iCont As Integer
    Dim sLinha As String

    Dim sAuxVcto1 As String
    Dim sAuxVcto2 As String
    Dim cAuxPremioBruto As Currency
    Dim cAuxAdicFracionamento As Currency
    Dim cAuxIOF As Currency
    Dim cAuxComissao As Currency
    Dim cAuxDesconto As Currency
    Dim cAuxPremioLiq As Currency

    Dim cValPremioBruto As Currency
    Dim cValAdicFracionamento As Currency
    Dim cValIOF As Currency
    Dim cValComissao As Currency
    Dim cValDesconto As Currency
    Dim cValPremioLiq As Currency

    Dim cTotValLiquidoGrid As Currency
    Dim cTotValFracGrid As Currency
    Dim cTotValIOFGrid As Currency
    Dim cTotValDescGrid As Currency
    Dim cTotValTarifaGrid As Currency
    Dim cTotValComGrid As Currency

    On Error GoTo TrataErro

    mskTotTarifa.Text = "0,00"
    mskTotJuros.Text = "0,00"
    mskTotIof.Text = "0,00"
    mskTotDesc.Text = "0,00"
    mskTotCom.Text = "0,00"
    mskTotLiquido.Text = "0,00"

    cAuxPremioBruto = 0
    cAuxAdicFracionamento = 0
    cAuxIOF = 0
    cAuxComissao = 0
    cAuxDesconto = 0
    cAuxPremioLiq = 0

    If oDadosEndosso.valTaxaJuros = 0 Then oDadosEndosso.valTaxaJuros = 1

    grdPagamento.Rows = 1

    sAuxVcto1 = mskDtPagamento.Text

    ' Dividindo os valores por parcelas ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

    cValComissao = Format((CCur(mskValCom.Text) / mskNumParcelas.Text), "##0.00")
    cValPremioBruto = (CCur(mskValPremioTarifa.Text) / mskNumParcelas.Text)
    cValDesconto = (CCur(mskValDesc.Text) / mskNumParcelas.Text)
    cValAdicFracionamento = Format(CCur(((cValPremioBruto - cValDesconto) * oDadosEndosso.valTaxaJuros) - (cValPremioBruto - cValDesconto)), "###0.00")

    For iCont = 1 To mskNumParcelas.Text

        sAuxVcto2 = sAuxVcto1

        'Inserindo no grid '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        If iCont = 1 Then   'Custo do endosso vai na 1�, c�lculos s�o proporcionais

            sLinha = iCont & vbTab
            sLinha = sLinha & Format$(sAuxVcto2, "dd/mm/yyyy") & vbTab

            'Pr�mio Bruto
            sLinha = sLinha & Format(cValPremioBruto, "##0.00") & vbTab
            cAuxPremioBruto = CCur(cAuxPremioBruto) + CCur(cValPremioBruto)


            'Custo Endosso
            sLinha = sLinha & Format((mskCustoEndossoCob.Text), "##0.00") & vbTab


            'Desconto
            sLinha = sLinha & Format(cValDesconto, "##0.00") & vbTab
            cAuxDesconto = CCur(cAuxDesconto) + CCur(cValDesconto)


            'Adic Frac
            sLinha = sLinha & Format(cValAdicFracionamento, "##0.00") & vbTab
            cAuxAdicFracionamento = CCur(cAuxAdicFracionamento) + CCur(cValAdicFracionamento)


            'IOF
            If chkIsento_IOF.Value = vbUnchecked Then
                cValIOF = (CCur(mskCustoEndossoCob.Text) + cValPremioBruto - cValDesconto + cValAdicFracionamento) * oDadosEndosso.PercIOF
            Else
                cValIOF = 0
            End If
            sLinha = sLinha & Format(cValIOF, "##0.00") & vbTab
            cAuxIOF = CCur(cAuxIOF) + CCur(cValIOF)

            'Comiss�o
            sLinha = sLinha & Format(cValComissao, "##0.00") & vbTab
            cAuxComissao = CCur(cAuxComissao) + CCur(cValComissao)


            'Pr�mio L�quido
            cValPremioLiq = CCur(mskCustoEndossoCob.Text) + CCur(cValPremioBruto) - cValDesconto + CCur(cValIOF) + CCur(cValAdicFracionamento)
            sLinha = sLinha & Format(cValPremioLiq, "##0.00") & vbTab
            cAuxPremioLiq = CCur(cAuxPremioLiq) + CCur(cValPremioLiq)

        Else

            sLinha = iCont & vbTab
            sLinha = sLinha & sAuxVcto2 & vbTab

            If iCont <> mskNumParcelas.Text Then

                'Pr�mio Bruto
                sLinha = sLinha & Format(cValPremioBruto, "##0.00") & vbTab
                cAuxPremioBruto = CCur(cAuxPremioBruto) + CCur(cValPremioBruto)


                'Custo Endosso
                sLinha = sLinha & "0,00" & vbTab


                'Desconto
                sLinha = sLinha & Format(cValDesconto, "##0.00") & vbTab
                cAuxDesconto = CCur(cAuxDesconto) + CCur(cValDesconto)


                'Adic Frac
                sLinha = sLinha & Format(cValAdicFracionamento, "##0.00") & vbTab
                cAuxAdicFracionamento = CCur(cAuxAdicFracionamento) + CCur(cValAdicFracionamento)


                'Iof
                If chkIsento_IOF.Value = vbUnchecked Then
                    cValIOF = (CCur(cValPremioBruto - cValDesconto + cValAdicFracionamento) * oDadosEndosso.PercIOF)
                Else
                    cValIOF = 0
                End If
                sLinha = sLinha & Format(cValIOF, "##0.00") & vbTab
                cAuxIOF = CCur(cAuxIOF) + CCur(cValIOF)


                'Comiss�o
                sLinha = sLinha & Format(cValComissao, "##0.00") & vbTab
                cAuxComissao = CCur(cAuxComissao) + CCur(cValComissao)


                'Pr�mio L�quido
                cValPremioLiq = CCur(cValPremioBruto) - CCur(cValDesconto) + CCur(cValIOF) + CCur(cValAdicFracionamento)
                sLinha = sLinha & Format(cValPremioLiq, "##0.00") & vbTab
                cAuxPremioLiq = CCur(cAuxPremioLiq) + CCur(cValPremioLiq)


            Else

                'Pr�mio Bruto
                cAuxPremioBruto = CCur(cAuxPremioBruto) + CCur(cValPremioBruto)
                If CCur(cAuxPremioBruto) < CCur(mskValPremioTarifa.Text) Then
                    sLinha = sLinha & Format((CCur(mskValPremioTarifa.Text) / mskNumParcelas.Text) + (CCur(mskValPremioTarifa.Text) - cAuxPremioBruto), "##0.00") & vbTab
                    cAuxPremioBruto = CCur(cAuxPremioBruto) + (CCur(mskValPremioTarifa.Text) - CCur(cAuxPremioBruto))
                ElseIf CCur(cAuxPremioBruto) > CCur(mskValPremioTarifa.Text) Then
                    sLinha = sLinha & Format((CCur(mskValPremioTarifa.Text) / CCur(mskNumParcelas.Text) - (CCur(cAuxPremioBruto) - CCur(mskValPremioTarifa.Text))), "##0.00") & vbTab
                    cAuxPremioBruto = CCur(cAuxPremioBruto) - (CCur(cAuxPremioBruto) - CCur(mskValPremioTarifa.Text))
                Else
                    sLinha = sLinha & Format((CCur(mskValPremioTarifa.Text) / mskNumParcelas.Text), "##0.00") & vbTab
                End If


                'Custo Endosso
                sLinha = sLinha & "0,00" & vbTab


                'Desconto
                cAuxDesconto = cAuxDesconto + cValDesconto
                If cAuxDesconto < CCur(mskValDesc.Text) Then
                    sLinha = sLinha & Format(CCur(cValDesconto) + (CCur(mskValDesc.Text) - CCur(cAuxDesconto)), "##0.00") & vbTab
                    cAuxDesconto = CCur(cAuxDesconto) + (CCur(mskValDesc.Text) - cAuxDesconto)
                ElseIf CCur(cAuxDesconto) > CCur(mskValDesc.Text) Then
                    sLinha = sLinha & Format((CCur(cValDesconto) - (CCur(cAuxDesconto) - CCur(mskValDesc.Text))), "##0.00") & vbTab
                    cAuxDesconto = CCur(cAuxDesconto) - (CCur(cAuxDesconto) - CCur(mskValDesc.Text))
                Else
                    sLinha = sLinha & Format(cValDesconto, "##0.00") & vbTab
                End If


                'Adic Frac
                cAuxAdicFracionamento = CCur(cAuxAdicFracionamento) + CCur(cValAdicFracionamento)
                If CCur(cAuxAdicFracionamento) < CCur(mskValJuros.Text) Then
                    sLinha = sLinha & Format(CCur(cValAdicFracionamento) + (CCur(mskValJuros.Text) - CCur(cAuxAdicFracionamento)), "##0.00") & vbTab
                    cAuxAdicFracionamento = CCur(cAuxAdicFracionamento) + (CCur(mskValJuros.Text) - CCur(cAuxAdicFracionamento))
                ElseIf CCur(cAuxAdicFracionamento) > CCur(mskValJuros.Text) Then
                    sLinha = sLinha & Format((CCur(cValAdicFracionamento) - (CCur(cAuxAdicFracionamento) - CCur(mskValJuros.Text))), "##0.00") & vbTab
                    cAuxAdicFracionamento = CCur(cAuxAdicFracionamento) - (CCur(cAuxAdicFracionamento) - CCur(mskValJuros.Text))
                Else
                    sLinha = sLinha & Format(cValAdicFracionamento, "##0.00") & vbTab
                End If


                'IOF
                cAuxIOF = CCur(cAuxIOF) + CCur(cValIOF)
                If cAuxIOF < CCur(mskValJuros.Text) Then
                    sLinha = sLinha & Format(CCur(cValIOF) + (CCur(mskValIOF.Text) - CCur(cAuxIOF)), "##0.00") & vbTab
                    cAuxIOF = CCur(cAuxIOF) + (CCur(mskValIOF.Text) - CCur(cAuxIOF))
                ElseIf CCur(cAuxIOF) > CCur(mskValIOF.Text) Then
                    sLinha = sLinha & Format((CCur(cValIOF) - (CCur(cAuxIOF) - CCur(mskValIOF.Text))), "##0.00") & vbTab
                    cAuxIOF = CCur(cAuxIOF) - (CCur(cAuxIOF) - CCur(mskValIOF.Text))
                Else
                    sLinha = sLinha & Format(cValIOF, "##0.00") & vbTab
                End If


                'Comiss�o
                sLinha = sLinha & Format(cValComissao, "##0.00") & vbTab
                cAuxComissao = CCur(cAuxComissao) + CCur(cValComissao)


                'Pr�mio L�quido
                cAuxPremioLiq = CCur(cAuxPremioLiq) + CCur(cValPremioLiq)
                If CCur(cAuxPremioLiq) < CCur(mskValLiquido.Text) Then
                    sLinha = sLinha & Format(CCur(cValPremioLiq) + (CCur(mskValLiquido.Text) - CCur(cAuxPremioLiq)), "##0.00") & vbTab
                    cAuxPremioLiq = cAuxPremioLiq + (CCur(mskValLiquido.Text) - cAuxPremioLiq)
                ElseIf CCur(cAuxPremioLiq) > CCur(mskValLiquido.Text) Then
                    sLinha = sLinha & Format((CCur(cValPremioLiq) - (CCur(cAuxPremioLiq) - CCur(mskValLiquido.Text))), "##0.00") & vbTab
                    cAuxPremioLiq = CCur(cAuxPremioLiq) - (CCur(cAuxPremioLiq) - CCur(mskValLiquido.Text))
                Else
                    sLinha = sLinha & Format(cValPremioLiq, "##0.00")
                End If

            End If

        End If

        grdPagamento.AddItem sLinha

        'Desconto
        grdPagamento.Row = iCont
        grdPagamento.Col = 4
        grdPagamento.CellBackColor = vbWhite    'Branco
        grdPagamento.CellForeColor = vbRed   'Vermelho

        'Comiss�o
        grdPagamento.Col = 7
        grdPagamento.CellBackColor = vbWhite    'Branco
        grdPagamento.CellForeColor = vbBlue  'Azul

        If optPerMes Then
            sAuxVcto1 = Format$(DateAdd("m", 1, sAuxVcto1), "dd/mm/yyyy")
        Else
            sAuxVcto1 = Format$(DateAdd("d", 30, sAuxVcto1), "dd/mm/yyyy")
        End If
    Next

    ' Totalizando ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    mskTotTarifa.Text = Format(cAuxPremioBruto, "##0.00")
    mskTotJuros.Text = Format(cAuxAdicFracionamento, "##0.00")
    mskTotIof.Text = Format(cAuxIOF, "##0.00")
    mskTotCom.Text = Format(cAuxComissao, "##0.00")
    mskTotDesc.Text = Format(cAuxDesconto, "##0.00")
    mskTotLiquido.Text = Format(cAuxPremioLiq, "##0.00")

    cTotValLiquidoGrid = 0
    cTotValFracGrid = 0
    cTotValIOFGrid = 0
    cTotValDescGrid = 0
    cTotValTarifaGrid = 0
    cTotValComGrid = 0

    For iCont = 1 To grdPagamento.Rows - 2
        cTotValLiquidoGrid = CCur(cTotValLiquidoGrid) + CCur(grdPagamento.TextMatrix(iCont, 8))
        cTotValFracGrid = CCur(cTotValFracGrid) + grdPagamento.TextMatrix(iCont, 5)
        cTotValIOFGrid = CCur(cTotValIOFGrid) + grdPagamento.TextMatrix(iCont, 6)
        cTotValDescGrid = CCur(cTotValDescGrid) + grdPagamento.TextMatrix(iCont, 4)
        cTotValTarifaGrid = CCur(cTotValTarifaGrid) + grdPagamento.TextMatrix(iCont, 2)
        cTotValComGrid = CCur(cTotValComGrid) + grdPagamento.TextMatrix(iCont, 7)
    Next

    'Calculo para garantir (endosso_financeiro_tb.valores = agendamento_cobranca_tb.valores)
    grdPagamento.TextMatrix(grdPagamento.Rows - 1, 8) = Format(CCur(mskTotLiquido.Text) - CCur(cTotValLiquidoGrid), "##0.00")
    grdPagamento.TextMatrix(grdPagamento.Rows - 1, 4) = Format(CCur(mskTotDesc.Text) - CCur(cTotValDescGrid), "##0.00")
    grdPagamento.TextMatrix(grdPagamento.Rows - 1, 5) = Format(CCur(mskTotJuros.Text) - CCur(cTotValFracGrid), "##0.00")
    grdPagamento.TextMatrix(grdPagamento.Rows - 1, 6) = Format(CCur(mskTotIof.Text) - CCur(cTotValIOFGrid), "##0.00")
    grdPagamento.TextMatrix(grdPagamento.Rows - 1, 2) = Format(CCur(mskTotTarifa.Text) - CCur(cTotValTarifaGrid), "##0.00")
    grdPagamento.TextMatrix(grdPagamento.Rows - 1, 7) = Format(CCur(mskTotCom.Text) - CCur(cTotValComGrid), "##0.00")

    Exit Sub

TrataErro:
    Call TratarErro("MontarGridPagamento", Me.name)
    Call FinalizarAplicacao

End Sub

Private Sub CarregarDadosEndossoEstipulante()

    On Error GoTo TrataErro

    stbEndosso.TabVisible(5) = True
    stbEndosso.Tab = 5

    mskPercEstipulante.mask = "3,4"
    mskPercEstipulante.Text = "0,0000"

    Call MontarComboSubGrupo

    Exit Sub

TrataErro:
    Call TratarErro("CarregarDadosEndossoEstipulante", Me.name)
    Call FinalizarAplicacao

End Sub

Private Sub PreencherGridBeneficiario(ByRef colBenef As Collection)

    Dim oBeneficiario As Object
    Dim iCont As Integer

    On Error GoTo TrataErro

    grdBeneficiario.Rows = 1

    Set oBeneficiario = CreateObject("SEGL0140.cls00999")

    For Each oBeneficiario In colBenef

        Call grdBeneficiario.AddItem(oBeneficiario.lBeneficiarioId & vbTab & _
                                     oBeneficiario.sNome & vbTab & _
                                     oBeneficiario.sEndereco & vbTab & _
                                     oBeneficiario.sBairro & vbTab & _
                                     oBeneficiario.sCidade & vbTab & _
                                     oBeneficiario.sEstado & vbTab & _
                                     oBeneficiario.sPais & vbTab & _
                                     oBeneficiario.sCep)

        grdBeneficiario.Row = grdBeneficiario.Rows - 1

        Select Case oBeneficiario.sOperacao
        Case "E"
            For iCont = 0 To grdBeneficiario.Cols - 1
                grdBeneficiario.Col = iCont
                grdBeneficiario.CellBackColor = vbWhite    'Branco
                grdBeneficiario.CellForeColor = vbRed   'Vermelho
            Next
        Case "A"
            For iCont = 0 To grdBeneficiario.Cols - 1
                grdBeneficiario.Col = iCont
                grdBeneficiario.CellBackColor = vbWhite    'Branco
                grdBeneficiario.CellForeColor = &HC000&    'Verde
            Next
        Case "I"
            For iCont = 0 To grdBeneficiario.Cols - 1
                grdBeneficiario.Col = iCont
                grdBeneficiario.CellBackColor = vbWhite    'Branco
                grdBeneficiario.CellForeColor = vbBlue  'Azul
            Next
        Case Else
            For iCont = 0 To grdBeneficiario.Cols - 1
                grdBeneficiario.Col = iCont
                grdBeneficiario.CellBackColor = vbWhite    'Branco
                grdBeneficiario.CellForeColor = vbBlack    'Preto
            Next
        End Select

    Next

    Set oBeneficiario = Nothing

    Exit Sub

TrataErro:
    Call TratarErro("PreencherGridBeneficiario", Me.name)
    Call FinalizarAplicacao
End Sub

Private Sub CarregarDadosBeneficiario()

    stbEndosso.TabVisible(7) = True
    stbEndosso.Tab = 7

    Set colBeneficiario = Nothing

    Call MontarComboCidade(cboCidadeBenef)
    Call CarregarColecaoBeneficiario(colBeneficiario)
    Call PreencherGridBeneficiario(colBeneficiario)

End Sub

Private Sub CarregarColecaoBeneficiario(ByRef colBenef As Collection)

    Dim rs As Recordset
    Dim oApolice As Object
    Dim oBeneficiario As Object

    On Error GoTo Erro

    Set oApolice = CreateObject("SEGL0026.cls00126")

    Set rs = oApolice.ConsultarBeneficiario(glAmbiente_id, _
                                            App.Title, _
                                            App.FileDescription, _
                                            gsSIGLASISTEMA, _
                                            oDadosEndosso.PropostaId)

    Set oApolice = Nothing

    While Not rs.EOF

        Set oBeneficiario = CreateObject("SEGL0140.cls00999")

        oBeneficiario.lBeneficiarioId = rs("beneficiario_id")
        oBeneficiario.sNome = Trim(rs("nome"))
        oBeneficiario.sEndereco = Trim(rs("endereco"))
        oBeneficiario.sBairro = Trim(rs("bairro"))
        oBeneficiario.sCidade = Trim(rs("cidade"))
        oBeneficiario.sEstado = Trim(rs("estado"))
        oBeneficiario.sPais = Trim(rs("pais"))
        oBeneficiario.sCep = Format(rs("cep"), "&&.&&&-&&&")
        oBeneficiario.sOperacao = "N"

        colBenef.Add oBeneficiario, CStr(oBeneficiario.lBeneficiarioId)

        Set oBeneficiario = Nothing

        rs.MoveNext
    Wend

    Set rs = Nothing

    Exit Sub

Erro:
    Call TratarErro("CarregarColecaoBeneficiario", Me.name)
    Call FinalizarAplicacao
End Sub

Private Sub CarregarDadosEndossoCorretagem()

    On Error GoTo TrataErro

    stbEndosso.TabVisible(6) = True
    stbEndosso.Tab = 6

    mskPercCorretagem.mask = "3,4"
    mskPercCorretagem.Text = "0,0000"

    Call MontarComboSubGrupo

    Exit Sub

TrataErro:
    Call TratarErro("CarregarDadosEndossoCorretagem", Me.name)
    Call FinalizarAplicacao
End Sub

Private Sub MontarComboSubGrupo()

    Dim oSubGrupo As Object
    Dim oApolice As Object
    Dim rs As Recordset

    On Error GoTo Trata_Erro

    Set colSubGrupo = Nothing
    Set colEstipulante = Nothing
    Set colCorretor = Nothing

    cboSubGrupoEst.Clear
    CboSubGrupo.Clear

    Set oApolice = CreateObject("SEGL0026.cls00126")

    Set rs = oApolice.ConsultarSubGrupo(gsSIGLASISTEMA, _
                                        App.Title, _
                                        App.FileDescription, _
                                        glAmbiente_id, _
                                        oDadosEndosso.produtoId, _
                                        oDadosEndosso.ApoliceId, _
                                        oDadosEndosso.RamoId, _
                                        oDadosEndosso.SeguradoraCodSusep, _
                                        oDadosEndosso.SucursalSeguradoraId)

    Set oApolice = Nothing

    If Not rs.EOF Then
        While Not rs.EOF

            cboSubGrupoEst.AddItem rs("nome")
            cboSubGrupoEst.ItemData(cboSubGrupoEst.NewIndex) = rs("sub_grupo_id")

            CboSubGrupo.AddItem rs("nome")
            CboSubGrupo.ItemData(CboSubGrupo.NewIndex) = rs("sub_grupo_id")

            Set oSubGrupo = CreateObject("SEGL0140.cls00175")

            oSubGrupo.ID = rs("sub_grupo_id")
            oSubGrupo.Nome = Trim(rs("nome"))

            Set oSubGrupo.Corretores = New Collection

            Call CarregarDadosCorretagem(oSubGrupo.Corretores, oSubGrupo.ID)

            Set oSubGrupo.estipulantes = New Collection

            Call CarregarDadosEstipulante(oSubGrupo.estipulantes, oSubGrupo.ID)

            colSubGrupo.Add oSubGrupo, CStr(oSubGrupo.ID)

            Set oSubGrupo = Nothing

            rs.MoveNext
        Wend
        CmdRestaura.Enabled = False
        CmdConCorretor.Enabled = False
        mskPercCorretagem.Locked = True

        cmdRestaurarEst.Enabled = False
        cmdConCliente.Enabled = False
        mskPercEstipulante.Locked = True
    Else
        Call CarregarDadosEstipulante(colEstipulante)
        Call PreencherGridEstipulante(colEstipulante)

        Call CarregarDadosCorretagem(colCorretor)
        Call MontarGridCorretagem(colCorretor)

        cboSubGrupoEst.Enabled = False
        CboSubGrupo.Enabled = False
    End If

    Set rs = Nothing

    Exit Sub

Trata_Erro:

    Call TratarErro("MontarComboSubGrupo", Me.name)
    Call FinalizarAplicacao

End Sub


Sub CalcularValorProLabore()

    Dim cPercTotalProLabore As Currency
    Dim iCont As Integer

    On Error GoTo TrataErro

    cPercTotalProLabore = 0

    For iCont = 1 To grdEstipulante.Rows - 1
        cPercTotalProLabore = cPercTotalProLabore + CCur(grdEstipulante.TextMatrix(iCont, 2))
    Next

    If txtProduto.Text = "111" Then
        cPercTotalProLabore = 2
    End If

    lblValProLabore.Caption = Format(CCur(mskValPremioTarifaRes.Text) * (cPercTotalProLabore / 100), "#,##0.00")
    mskValComEst.Text = Format((CCur(mskValPremioTarifa.Text) - CCur(mskValDesc.Text) + CCur(mskValJuros.Text)) * (cPercTotalProLabore / 100), "#,##0.00")

    If grdEstipulante.Rows > 1 Then
        oDadosEndosso.EstipulanteId = Val(grdEstipulante.TextMatrix(1, 0))
        oDadosEndosso.PercProLabore = cPercTotalProLabore
    Else
        oDadosEndosso.EstipulanteId = 0
        oDadosEndosso.PercProLabore = 0
    End If

    Exit Sub

TrataErro:
    Call TratarErro("CalcularValorProLabore")
    Call FinalizarAplicacao

End Sub


Sub MontarComboMoeda()

    Dim oDadosBasicos As Object
    Dim rs As Recordset
    Dim iCont As Integer
    Dim iPremioMoedaId As Integer

    Dim iMoedaAtualId As Integer

    Const MOEDA_ID_DOLAR_AMERICANO = 220

    On Error GoTo TrataErro

    cboMoeda.Clear
    mskCambio.Text = "0,000000"

    iPremioMoedaId = oDadosEndosso.PremioMoedaId

    Set oDadosBasicos = CreateObject("SEGL0022.cls00117")

    ' Incluindo moeda atual do sistema ''''''''''''''''''''''''''''''''''''''''''

    iMoedaAtualId = Val(oDadosBasicos.ConsultarParametro(gsSIGLASISTEMA, _
                                                         App.Title, _
                                                         App.FileDescription, _
                                                         glAmbiente_id, _
                                                         "MOEDA ATUAL"))

    Set rs = oDadosBasicos.ConsultarMoeda(gsSIGLASISTEMA, _
                                          App.Title, _
                                          App.FileDescription, _
                                          glAmbiente_id, _
                                          iMoedaAtualId)

    If Not rs.EOF Then
        cboMoeda.AddItem rs("nome")
        cboMoeda.ItemData(cboMoeda.NewIndex) = rs("moeda_id")
        rs.MoveNext
    End If

    Set rs = Nothing

    If iMoedaAtualId <> iPremioMoedaId Then

        ' Incluindo moeda da proposta '''''''''''''''''''''''''''''''''''''''''''''''

        Set rs = oDadosBasicos.ConsultarMoeda(gsSIGLASISTEMA, _
                                              App.Title, _
                                              App.FileDescription, _
                                              glAmbiente_id, _
                                              iPremioMoedaId)

        If Not rs.EOF Then
            cboMoeda.AddItem rs("nome")
            cboMoeda.ItemData(cboMoeda.NewIndex) = rs("moeda_id")
            rs.MoveNext
        End If

        Set rs = Nothing

    End If

    If (iMoedaAtualId <> MOEDA_ID_DOLAR_AMERICANO) And (iPremioMoedaId <> MOEDA_ID_DOLAR_AMERICANO) Then

        ' Incluindo moeda Dolar Americano '''''''''''''''''''''''''''''''''''''''''''''''

        Set rs = oDadosBasicos.ConsultarMoeda(gsSIGLASISTEMA, _
                                              App.Title, _
                                              App.FileDescription, _
                                              glAmbiente_id, _
                                              MOEDA_ID_DOLAR_AMERICANO)

        If Not rs.EOF Then
            cboMoeda.AddItem rs("nome")
            cboMoeda.ItemData(cboMoeda.NewIndex) = rs("moeda_id")
            rs.MoveNext
        End If

        Set rs = Nothing

    End If

    Set oDadosBasicos = Nothing

    For iCont = 0 To cboMoeda.ListCount - 1
        If cboMoeda.ItemData(iCont) = iPremioMoedaId Then
            cboMoeda.ListIndex = iCont
            Exit For
        End If
    Next

    Exit Sub

TrataErro:
    Call TratarErro("MontarComboMoeda")
    Call FinalizarAplicacao

End Sub




Private Sub CarregarDadosEstipulante(ByRef colEstip As Collection, _
                                     Optional ByVal lSubGrupoId As Long = 0)

    Dim oEstipulante As Object
    Dim oApolice As Object
    Dim rs As Recordset

    Dim bTemSubGrupo As Boolean

    On Error GoTo Trata_Erro

    If lSubGrupoId > 0 Then
        bTemSubGrupo = True
    Else
        bTemSubGrupo = False
    End If

    Set oApolice = CreateObject("SEGL0026.cls00126")

    Set rs = oApolice.ConsultarEstipulante(gsSIGLASISTEMA, _
                                           App.Title, _
                                           App.FileDescription, _
                                           glAmbiente_id, _
                                           oDadosEndosso.PropostaId, _
                                           bTemSubGrupo, _
                                           oDadosEndosso.ApoliceId, _
                                           oDadosEndosso.RamoId, _
                                           oDadosEndosso.SeguradoraCodSusep, _
                                           oDadosEndosso.SucursalSeguradoraId, _
                                           lSubGrupoId)

    Set oApolice = Nothing

    While Not rs.EOF

        Set oEstipulante = CreateObject("SEGL0140.cls00174")

        oEstipulante.ID = rs("adm_cliente_id")
        oEstipulante.IdEstipulante = rs("est_cliente_id")
        oEstipulante.Nome = Trim(UCase(rs("nome")))
        oEstipulante.DtIniAdm = Format(rs("dt_inicio_administracao"), "dd/mm/yyyy")
        oEstipulante.DtIniRep = Format(rs("dt_inicio_representacao"), "dd/mm/yyyy")
        oEstipulante.Operacao = ""
        oEstipulante.PercProLabore = CDbl(rs("perc_pro_labore"))

        colEstip.Add oEstipulante, CStr(oEstipulante.ID)

        Set oEstipulante = Nothing

        rs.MoveNext
    Wend

    Set rs = Nothing

    Exit Sub

Trata_Erro:

    Call TratarErro("CarregarDadosEstipulante", Me.name)
    Call FinalizarAplicacao

End Sub

Private Sub CarregarDadosCorretagem(ByRef colCorret As Collection, _
                                    Optional ByVal lSubGrupoId As Long = 0)

    Dim oCorretagem As Object
    Dim oApolice As Object
    Dim rs As Recordset
    Dim sPercCorretagem As String
    Dim sNomeCorretor As String

    Dim bTemSubGrupo As Boolean

    On Error GoTo Trata_Erro

    If lSubGrupoId > 0 Then
        bTemSubGrupo = True
    Else
        bTemSubGrupo = False
    End If

    Set oApolice = CreateObject("SEGL0026.cls00126")

    Set rs = oApolice.ObterDadosCorretor(glAmbiente_id, App.Title, _
                                         App.FileDescription, gsSIGLASISTEMA, _
                                         oDadosEndosso.PropostaId, bTemSubGrupo, _
                                         oDadosEndosso.RamoId, oDadosEndosso.SeguradoraCodSusep, _
                                         oDadosEndosso.SucursalSeguradoraId, oDadosEndosso.ApoliceId)


    Set oApolice = Nothing

    While Not rs.EOF

        sPercCorretagem = IIf(IsNull(rs("perc_corretagem")), "0,00000", Format(CDbl(rs("perc_corretagem")), "0.00000"))
        sNomeCorretor = Trim(rs("nome"))

        Set oCorretagem = CreateObject("SEGL0140.cls00149")

        With oCorretagem

            .Corretor_ID = rs!Corretor_ID
            .Tipo = rs!tp_pessoa
            .Nome = sNomeCorretor
            .PercComissao = sPercCorretagem
            .DtInicioCorretagem = CDate(rs!dt_inicio_corretagem)
            .Sucursal = IIf(IsNull(rs!suc), Empty, rs!suc)
            .Operacao = "N"
            .Persistente = True

        End With

        colCorret.Add oCorretagem, CStr(oCorretagem.Corretor_ID)

        Set oCorretagem = Nothing

        rs.MoveNext
    Wend

    Set rs = Nothing

    Exit Sub

Trata_Erro:

    Call TratarErro("CarregarDadosCorretagem", Me.name)
    Call FinalizarAplicacao

End Sub


Private Sub PreencherGridEstipulante(ByRef colEstip As Collection)

    Dim oEstipulante As Object

    On Error GoTo TrataErro

    grdEstipulanteEst.Rows = 1

    Set oEstipulante = CreateObject("SEGL0140.cls00174")

    For Each oEstipulante In colEstip
        If oEstipulante.Operacao <> "E" Then
            Call grdEstipulanteEst.AddItem(oEstipulante.ID & vbTab & _
                                           oEstipulante.Nome & vbTab & _
                                           Format(oEstipulante.PercProLabore, "#0.0000") & vbTab & _
                                           oEstipulante.DtIniAdm)
        End If
    Next

    Set oEstipulante = Nothing

    Exit Sub

TrataErro:

    Call TratarErro("PreencherGridEstipulante", Me.name)
    Call FinalizarAplicacao

End Sub

Private Sub AplicarEndossoEstipulante()

    Dim iEndossoSegBr As Integer

    On Error GoTo TrataErro

    ' Gerando o endosso selecionado '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

    Me.MousePointer = vbHourglass

    Call IncluirEndossoEstipulante(oDadosEndosso.ApoliceId, _
                                   oDadosEndosso.SucursalSeguradoraId, _
                                   oDadosEndosso.SeguradoraCodSusep, _
                                   oDadosEndosso.RamoId, _
                                   oDadosEndosso.PropostaId, _
                                   mskDtEndosso.Text, _
                                   oDadosEndosso.TipoEndosso, _
                                   mskDtEmissao.Text, _
                                   Trim(txtDescEndosso.Text), _
                                   colEstipulante, _
                                   colSubGrupo)

    Me.MousePointer = vbDefault

    Call MsgBox("Endosso realizado com sucesso." & vbNewLine & _
                "Proposta: " & oDadosEndosso.PropostaId & vbNewLine & _
                "Endosso : " & txtNumEndosso.Text, vbInformation)

    Call sair

    Exit Sub

TrataErro:
    Call TratarErro("AplicarEndossoEstipulante", Me.name)
    Call FinalizarAplicacao

End Sub

Private Sub IncluirEndossoCobranca(ByVal lApoliceId As Long, ByVal lSucursal As Long, _
                                   ByVal lSeguradora As Long, ByVal lPropostaId As Long, _
                                   ByVal LramoId As Long, ByVal cValFinanceiro As Currency, _
                                   ByVal lTpEndossoId As Long, ByVal cValPremioTarifa As Currency, _
                                   ByVal cValCustoEmissao As Currency, ByVal cValCorretagem As Currency, _
                                   ByVal cValProLabore As Currency, ByVal cValIOF As Currency, _
                                   ByVal cValIR As Currency, ByVal cValDesconto As Currency, _
                                   ByVal cValAdicFracionamento As Currency, ByVal lNumParcelas As Long, _
                                   ByVal lSeguroMoedaId As Long, ByVal lPremioMoedaId As Long, _
                                   ByVal cValParidadeMoeda As Currency, ByVal cValIs As Currency, _
                                   ByVal sDtPedidoEndosso As String, ByVal sDtEmissao As String, _
                                   ByVal cPercIR As Currency, ByVal sTextoEndosso As String, _
                                   ByVal lEstipulanteId As Long, ByVal cPercProLabore As Currency, _
                                   ByVal cPercCorretagem As Currency)

    Dim OEndosso As Object

    Dim colCorretagem As Collection
    Dim colAgendamento As Collection

    On Error GoTo TrataErro

    Set colCorretagem = ObterCorretagem(cValPremioTarifa)

    Set colAgendamento = ObterColecaoAgendamento

    Set OEndosso = CreateObject("SEGL0140.cls00172")

    Call OEndosso.GerarEndossoCobranca(gsSIGLASISTEMA, App.Title, _
                                       App.FileDescription, glAmbiente_id, _
                                       cUserName, txtApolice.Text, lSucursal, _
                                       lSeguradora, txtPropostaid(0).Text, _
                                       txtRamo.Text, cValFinanceiro, _
                                       lTpEndossoId, cValPremioTarifa, _
                                       cValCustoEmissao, colAgendamento, _
                                       colCorretagem, cValCorretagem, _
                                       cValProLabore, cValIOF, _
                                       cValIR, cValDesconto, _
                                       cValAdicFracionamento, lNumParcelas, _
                                       lSeguroMoedaId, lPremioMoedaId, _
                                       cValParidadeMoeda, cValIs, _
                                       sDtPedidoEndosso, sDtEmissao, _
                                       cPercIR, sTextoEndosso, _
                                       lEstipulanteId, cPercProLabore, _
                                       cPercCorretagem)

    Set OEndosso = Nothing

    Exit Sub

TrataErro:
    Call TratarErro("IncluirEndossoCobranca", Me.name)
    Call FinalizarAplicacao

End Sub


Private Sub optNovCob_Click()

    Call CarregarCboCoberturaAfetadaNovaCobertura

End Sub

Private Sub optRedIS_Click()

    Call CarregarCboCoberturaAfetadaReducaoIS

End Sub

Sub CarregarCboCoberturaAfetadaNovaCobertura()

    Dim oCobertura As Cobertura
    Dim oNovaCobertura As Cobertura
    Dim bAchou As Boolean

    On Error GoTo Erro

    'Nova - Carrega combo com coberturas <> da original

    cboCoberturaAfetada.Clear

    For Each oNovaCobertura In colNovaCobertura
        bAchou = False
        For Each oCobertura In colCoberturas
            If oCobertura.TpCoberturaId = oNovaCobertura.TpCoberturaId _
               And oCobertura.CodObjSegurado = cboObjetoSegurado.Text Then
                bAchou = True
            End If
        Next

        If Not bAchou Then
            cboCoberturaAfetada.AddItem oNovaCobertura.DESCRICAO
            cboCoberturaAfetada.ItemData(cboCoberturaAfetada.NewIndex) = oNovaCobertura.TpCoberturaId
        End If
    Next

    Set oCobertura = Nothing
    Set oNovaCobertura = Nothing

    Lbl(27).Caption = "Premio Tarifa:"
    Lbl(26).Caption = "Premio Net:"

    Call HabilitarCamposEndossoIS

    Exit Sub

Erro:
    Call TratarErro("CarregarCboCoberturaAfetadaNovaCobertura", Me.name)
    Call FinalizarAplicacao
End Sub

Sub CarregarCboCoberturaAfetadaReducaoIS()

    Dim oCobertura As Cobertura
    Dim oNovaCobertura As Cobertura

    On Error GoTo Erro

    'Redu��o e altera��o - Carrega combo com mesmas coberturas original
    cboCoberturaAfetada.Clear

    For Each oNovaCobertura In colNovaCobertura
        For Each oCobertura In colCoberturas
            If oCobertura.TpCoberturaId = oNovaCobertura.TpCoberturaId _
               And oCobertura.CodObjSegurado = cboObjetoSegurado.Text Then
                cboCoberturaAfetada.AddItem oNovaCobertura.DESCRICAO
                cboCoberturaAfetada.ItemData(cboCoberturaAfetada.NewIndex) = oNovaCobertura.TpCoberturaId
            End If
        Next
    Next

    Set oCobertura = Nothing
    Set oNovaCobertura = Nothing

    Lbl(27).Caption = "Pr�mio Tarifa Restituir:"
    Lbl(26).Caption = "Pr�mio Net Restituir:"

    Call DesabilitarCamposEndossoIS

    mskPremioTarifa.Enabled = True
    cboCoberturaAfetada.Enabled = True
    mskImpSeg.Enabled = True

    Exit Sub

Erro:
    Call TratarErro("CarregarCboCoberturaAfetadaReducaoIS", Me.name)
    Call FinalizarAplicacao
End Sub




'R.FOUREAUX 25/06/2012 - DEMANDA 12006056 - CARREGAR CBO FORMA PGT BESK
Private Sub CarregarCboFormaPagamento(ByVal iFormaPagamento As Integer)

    Dim rsFormaPagamento As Recordset
    Dim rsSelPgto As Recordset
    Dim sSQL As String
    Dim iIndex As Integer

    On Error GoTo Erro

    cboFormaPagamento.Clear

    sSQL = ""
    sSQL = sSQL & " SELECT forma_pgto_id " & vbNewLine
    sSQL = sSQL & "      , nome" & vbNewLine
    sSQL = sSQL & "   FROM forma_pgto_tb" & vbNewLine
    sSQL = sSQL & "  WHERE forma_pgto_id IN(1,3)" & vbNewLine    'TRAZ SOMENTE:  1-D�BITO EM CONTA 3-FICHA DE COMPENSA��O BANC�RIA

    If rdocn Is Nothing Then
        Call Conexao
    Else
        If rdocn.State = 0 Then Call Conexao
    End If


    Set rsFormaPagamento = rdocn.Execute(sSQL)

    If Not rsFormaPagamento.EOF Then
        iIndex = 0
        While Not rsFormaPagamento.EOF
            cboFormaPagamento.AddItem rsFormaPagamento("forma_pgto_id") & " - " & UCase(rsFormaPagamento("nome"))
            cboFormaPagamento.ItemData(cboFormaPagamento.NewIndex) = rsFormaPagamento("forma_pgto_id")
            If rsFormaPagamento("forma_pgto_id") = iFormaPagamento Then
                iIndex = cboFormaPagamento.NewIndex
            End If
            rsFormaPagamento.MoveNext

        Wend

        cboFormaPagamento.ListIndex = iIndex
        rsFormaPagamento.Close

    End If

    Set rsFormaPagamento = Nothing

    Exit Sub

Erro:

    Call TratarErro("CarregarCboFormaPagamento", Me.name)
    Call FinalizarAplicacao
End Sub

Sub CarregarCboCoberturaAfetadaAumentoIS()

    Dim oCobertura As Cobertura
    Dim oNovaCobertura As Cobertura

    On Error GoTo Erro

    'Redu��o e altera��o - Carrega combo com mesmas coberturas original
    cboCoberturaAfetada.Clear

    For Each oNovaCobertura In colNovaCobertura
        For Each oCobertura In colCoberturas
            If oCobertura.TpCoberturaId = oNovaCobertura.TpCoberturaId _
               And oCobertura.CodObjSegurado = cboObjetoSegurado.Text Then
                cboCoberturaAfetada.AddItem oNovaCobertura.DESCRICAO
                cboCoberturaAfetada.ItemData(cboCoberturaAfetada.NewIndex) = oNovaCobertura.TpCoberturaId
            End If
        Next
    Next

    Set oCobertura = Nothing
    Set oNovaCobertura = Nothing

    Lbl(27).Caption = "Pr�mio Tarifa:"
    Lbl(26).Caption = "Pr�mio Net:"

    Call HabilitarCamposEndossoIS

    Exit Sub

Erro:
    Call TratarErro("CarregarCboCoberturaAfetadaAumentoIS", Me.name)
    Call FinalizarAplicacao
End Sub

Public Sub HabilitarCamposEndossoIS()

    cboCoberturaAfetada.Enabled = True
    mskImpSeg.Enabled = True
    mskTaxaNet.Enabled = True
    mskTaxaTarifa.Enabled = True
    mskPremioNet.Enabled = True
    mskPremioTarifa.Enabled = True
    mskLimFra.Enabled = True
    mskFatFranquia.Enabled = True
    txtTextoFranquia.Enabled = True

    mskImpSeg.Text = "0,00"
    mskTaxaNet.Text = "0,0000000"
    mskTaxaTarifa.Text = "0,0000000"
    mskPerDesc.Text = "0,00000"
    mskPremioNet.Text = "0,00"
    mskPremioTarifa.Text = "0,00"
    mskLimFra.Text = "0,00"

End Sub

Public Sub DesabilitarCamposEndossoIS()

    cboCoberturaAfetada.Enabled = False
    mskImpSeg.Enabled = False
    mskTaxaTarifa.Enabled = False
    mskTaxaNet.Enabled = False
    mskPremioTarifa.Enabled = False
    mskPremioNet.Enabled = False
    mskLimFra.Enabled = False
    mskFatFranquia.Enabled = False
    txtTextoFranquia.Enabled = False

    mskImpSeg.Text = "0,00"
    mskTaxaTarifa.Text = "0,0000000"
    mskTaxaNet.Text = "0,0000000"
    mskPerDesc.Text = "0,00000"
    mskPremioTarifa.Text = "0,00"
    mskPremioNet.Text = "0,00"
    mskLimFra.Text = "0,00"
    mskFatFranquia.Text = "0,00000"
    txtTextoFranquia.Text = ""

End Sub

Public Sub CarregarColecaoCoberturasProduto(ByVal iProdutoId As Integer, ByVal iRamoId As Integer)

    Dim oSeguro As Object
    Dim oCobertura As Cobertura
    Dim rs As Recordset

    On Error GoTo Erro

    'Carrega Combo de coberturas

    Set oSeguro = CreateObject("SEGL0148.cls00181")

    Set oSeguro.oConexao = oConexao

    Set rs = oSeguro.ConsultarCoberturaProduto(iProdutoId, iRamoId)

    Set oSeguro = Nothing

    If Not rs.EOF Then
        While Not rs.EOF

            Set oCobertura = New Cobertura

            oCobertura.TpCoberturaId = rs("Tp_Cobertura_id")
            oCobertura.DESCRICAO = rs("Nome")
            oCobertura.LimMax = rs("lim_max_Cobertura")
            oCobertura.LimMin = rs("lim_min_Cobertura")
            oCobertura.PercMax = rs("perc_lim_Max_Cobertura")
            oCobertura.PercMin = rs("perc_lim_Min_Cobertura")
            oCobertura.Acumula_IS = IIf(IsNull(rs("Acumula_IS")), "", rs("Acumula_IS"))
            oCobertura.Acumula_Resseguro = IIf(IsNull(rs("Acumula_Resseguro")), "", rs("Acumula_Resseguro"))

            colNovaCobertura.Add oCobertura

            rs.MoveNext

        Wend
        rs.Close
    End If

    Set rs = Nothing
    Set oCobertura = Nothing

    Exit Sub

Erro:
    Call TratarErro("CarregarColecaoCoberturasProduto", Me.name)
    Call FinalizarAplicacao
End Sub



Private Sub txtArea_LostFocus(Index As Integer)

    Dim i As Integer

    If Index = 1 Then

        'N�o deixa digitar ponto para n�o gerar um valor inv�lido uma vez que o programa formata o numero com ',00'
        For i = 1 To Len(TxtArea(1).Text)
            If Mid(TxtArea(1).Text, i, 1) = "." Then
                MsgBox "O valor digitado s� pode conter n�meros ou v�rgula.", vbInformation, "SEGP0777 - Redu��o IS"
                TxtArea(1).SetFocus
                Exit Sub
            End If
        Next


        If IsNumeric(TxtArea(1).Text) = True Then

            'Maior que 100%
            If Val(TxtArea(1).Text) >= Val(TxtArea(0).Text) Then
                MsgBox "�rea afetada pelo sinistro n�o pode ser maior que a �rea vigente.", vbInformation, "SEGP0777"
                TxtArea(1).SetFocus
                Exit Sub
            End If

            'Menor que 20%
            If Val(TxtArea(1).Text) < Val(TxtArea(0).Text) / 100 * 20 Then
                MsgBox "�rea afetada pelo sinistro deve ser superior a 20% da �rea vigente.", vbInformation, "SEGP0777"
                TxtArea(1).SetFocus
                Exit Sub
            End If


            TxtArea(2).Text = Format((TxtArea(0).Text - TxtArea(1).Text), "#,##0.00")

            'C�lculo da IS
            If txtProduto.Text = "1204" Then
                'Se n�o houver fator cobertura multiplica por 1, para manter o valor
                TxtArea(5).Text = Format((TxtArea(2).Text * TxtArea(3).Text) * IIf(TxtArea(4).Text <> "", TxtArea(4).Text, 1), "#,##0.00")
            End If

        End If

        'REINTEGRA��O DE IS
    ElseIf Index = 18 Then

        'N�o deixa digitar ponto para n�o gerar um valor inv�lido uma vez que o programa formata o numero com ',00'
        For i = 1 To Len(TxtArea(18).Text)
            If Mid(TxtArea(18).Text, i, 1) = "." Then
                MsgBox "O valor digitado s� pode conter n�meros ou v�rgula.", vbInformation, "SEGP0777 - Redu��o IS"
                TxtArea(18).SetFocus
                Exit Sub
            End If
        Next


        If IsNumeric(TxtArea(18).Text) = True Then

            If Val(TxtArea(18).Text) > Val(TxtArea(16).Text) Or Val(TxtArea(18).Text) < Val(TxtArea(17).Text) Then
                MsgBox "Nova �rea tem que estar entre �rea vigente e �rea original", vbInformation, "SEGP0777"
                TxtArea(18).SetFocus
                Exit Sub
            End If

            'txtArea(19).Text = Format((txtArea(0).Text - txtArea(1).Text), "#,##0.00")

            If txtProduto.Text = "1204" Then
                'Se n�o houver fator cobertura multiplica por 1, para manter o valor
                TxtArea(19).Text = Format((TxtArea(15).Text * TxtArea(18).Text) * IIf(TxtArea(31).Text <> "", TxtArea(31).Text, 1), "#,##0.00")
            End If

            TxtArea(19).Text = Format((TxtArea(19).Text), "#,##0.00")

            TxtArea(19).Text = Replace(TxtArea(19).Text, ".", "")

        End If

    ElseIf Index = 7 Then
        Dim Produtividade As Variant
        Dim Faturamento_Esperado As Double

        'Relativo a redu��o
        '-----------------------------------------------------------------------------------------------------------------
        Dim rsReducao As Recordset
        Dim Reducao As Object

        If IsNumeric(TxtArea(7).Text) = False Then
            MsgBox "A �rea s� pode conter valores num�ricos.", vbInformation, "SEGP0777 - Adequa��o de �rea"
            Exit Sub
        End If

        If TxtArea(7).Text <= 0 Then
            MsgBox "O valor da �rea deve ser maior que zero.", vbInformation, "SEGP0777 - Adequa��o de �rea"
            Exit Sub
        End If

        Set Reducao = CreateObject("SEGL0026.cls00126")
        Set rsReducao = Reducao.Verifica_Reducao(gsSIGLASISTEMA, _
                                                 App.Title, _
                                                 App.FileDescription, _
                                                 glAmbiente_id, _
                                                 grdReintegracao(0).TextMatrix(grdReintegracao(0).Row, 0))

        If Not rsReducao.EOF Then

            If rsReducao("Reducao") = "S" Then

                If CDbl(TxtArea(7).Text) >= CDbl(TxtArea(9).Text) Then
                    MsgBox "Existe um endosso de redu��o de IS para a proposta. A �rea s� pode ser reduzida.", vbInformation, "SEGP0777 - Adequa��o de �rea"
                    TxtArea(7).SetFocus
                    Exit Sub
                End If

                TxtArea(33).Text = "S"

            End If

        End If
        '-------------------------------------------------------------------------------------------------------------------



        'Relativo ao 1204 somente - fadao
        If txtProduto.Text = "1204" Then
            ' Produtividade = TrocaVirgulaPorPonto(TxtArea(7).Text) * TrocaVirgulaPorPonto(TxtArea(24).Text) 'area X produt municipio
            'TxtArea(35).Text = TxtArea(7).Text * TxtArea(24).Text
            'Faturamento_Esperado = (TxtArea(7).Text / 60) * TxtArea(26).Text * TxtArea(7).Text
            TxtArea(36).Text = Format((TxtArea(25).Text / 60) * TxtArea(26).Text * TxtArea(7).Text, "#,##0.00")
            TxtArea(11).Text = Format((TxtArea(36).Text * TxtArea(27).Text), "#,##0.00")
            TxtArea(29).Text = Format(TxtArea(7).Text * TxtArea(8).Text * TxtArea(23).Text, "#,##0.00")
        End If

        If txtProduto.Text = "1152" Then
            TxtArea(7).Text = Format((TxtArea(7).Text), "#,##0.00")
            If IsNumeric(TxtArea(7).Text) = True Then
                TxtArea(11) = Format((TxtArea(7).Text * TxtArea(8).Text), "#,##0.00")
            End If
        End If



        'fadao - inclus�o do calculo para adequa��o produtividade - fadao
    ElseIf Index = 46 Then
        Dim dProdutividadeAd As Double
        Dim Faturamento_EsperadoAd As Double
        Dim dPrecoBase As Double
        Dim dAreaAdequacao As Double
        Dim dNivelCobertura As Double
        Dim dImportanciaSegurada As Double



        'Relativo ao 1204 somente - fadao
        If txtProduto.Text = "1204" Then
            dProdutividadeAd = TxtArea(46).Text / 60    'area X produt municipio
            dPrecoBase = TxtArea(45).Text
            dAreaAdequacao = TxtArea(50).Text
            dNivelCobertura = TrocaPontoPorVirgula(TxtArea(13).Text)

            Faturamento_EsperadoAd = Format(dProdutividadeAd * dPrecoBase * dAreaAdequacao, "#,##0.00")
            dImportanciaSegurada = Faturamento_EsperadoAd * dNivelCobertura

            TxtArea(34).Text = Faturamento_EsperadoAd
            'Troca

            TxtArea(21).Text = Format(dImportanciaSegurada, "#,##0.00")

            'TxtArea(21).Text = Replace(TxtArea(21).Text, ",", ".")
        End If

        '         If txtProduto.Text = "1152" Then
        '            TxtArea(7).Text = Format((TxtArea(7).Text), "#,##0.00")
        '            If IsNumeric(TxtArea(7).Text) = True Then
        '              TxtArea(11) = Format((TxtArea(7).Text * TxtArea(8).Text), "#,##0.00")
        '            End If
        '         End If

    ElseIf Index = 52 Then

        For i = 1 To Len(TxtArea(52).Text)
            If Mid(TxtArea(52).Text, i, 1) = "." Then
                MsgBox "O valor digitado s� pode conter n�meros ou v�rgula.", vbInformation, "SEGP0777 - Redu��o IS"
                TxtArea(52).SetFocus
                Exit Sub
            End If
        Next

    End If


End Sub

Private Sub txtBanco_Validate(Cancel As Boolean)

    MousePointer = vbHourglass

    Call ConsultarBanco

    MousePointer = vbDefault

End Sub

Sub ConsultarBanco()

    Dim oDadoBasico As Object

    On Error GoTo Erro

    If Trim(txtBanco.Text) = "" Or Not IsNumeric(txtBanco.Text) Then
        Set oDadoBasico = Nothing
        Exit Sub
    End If

    Set oDadoBasico = CreateObject("SEGL0022.cls00117")

    txtEndoCad(6).Text = oDadoBasico.ConsultarBanco(glAmbiente_id, App.Title, _
                                                    App.FileDescription, gsSIGLASISTEMA, _
                                                    Val(txtBanco.Text))

    Set oDadoBasico = Nothing

    Exit Sub

Erro:
    Call TratarErro("ConsultarBanco", Me.name)
    Call FinalizarAplicacao
End Sub

Private Sub txtCodAgDebito_LostFocus(Index As Integer)

    MousePointer = vbHourglass

    Call ConsultarAgencia

    MousePointer = vbDefault

End Sub

'Private Sub txtCodAgDebito_Validate(Cancel As Boolean)
'
'    MousePointer = vbHourglass
'
'    Call ConsultarAgencia
'
'    MousePointer = vbDefault
'
'End Sub

Sub ConsultarAgencia()

    Dim oDadoBasico As Object

    On Error GoTo Erro

    If Trim(txtBanco.Text) = "" Or Not IsNumeric(txtBanco.Text) _
       Or Trim(txtCodAgDebito(0).Text) = "" Or Not IsNumeric(txtCodAgDebito(0).Text) Then
        Set oDadoBasico = Nothing
        Exit Sub
    End If

    Set oDadoBasico = CreateObject("SEGL0022.cls00117")

    txtCodAgDebito(1).Text = oDadoBasico.ConsultarAgencia(glAmbiente_id, App.Title, _
                                                          App.FileDescription, gsSIGLASISTEMA, _
                                                          Val(txtBanco.Text), Val(txtCodAgDebito(0).Text))

    Set oDadoBasico = Nothing

    Exit Sub

Erro:
    Call TratarErro("ConsultarAgencia", Me.name)
    Call FinalizarAplicacao
End Sub

Private Sub CarregarDadosEndereco()

    Dim oApolice As Object
    Dim rsCadastral As Recordset
    Dim iIndice As Integer

    On Error GoTo TrataErro

    stbEndosso.TabVisible(8) = True
    stbEndosso.Tab = 8

    Call MontarComboCidade(cboCidade)

    Set oApolice = CreateObject("SEGL0026.cls00126")

    Set rsCadastral = oApolice.ConsultarDadosCadastrais(gsSIGLASISTEMA, _
                                                        App.Title, _
                                                        App.FileDescription, _
                                                        glAmbiente_id, _
                                                        oDadosEndosso.PropostaId)

    If Not rsCadastral.EOF Then

        txtCodCliEndereco.Text = oDadosEndosso.ClienteId

        ' Nome ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        If Not IsNull(rsCadastral("nome")) Then
            txtEndoCad(0).Text = Trim(rsCadastral("nome"))
            txtNomeEndereco.Text = rsCadastral("nome")
            txtNomeEndereco.Tag = Trim(rsCadastral("nome"))
        Else
            txtNomeEndereco.Tag = ""
        End If

        ' Tipo de Pessoa ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        iTpPessoa = rsCadastral("tp_pessoa")

        If rsCadastral("tp_pessoa") = 1 Then

            ' Data de Nascimento ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            If Not IsNull(rsCadastral("dt_nasc")) Then
                sNascimento = Format(rsCadastral("dt_nasc"), "dd/mm/yyyy")
            Else
                sNascimento = ""
            End If

            ' CPF '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            If Not IsNull(rsCadastral("cpf_cgc")) Then
                sCPFCNPJ = Format(rsCadastral("cpf_cgc"), "&&&.&&&.&&&-&&")
            Else
                sCPFCNPJ = ""
            End If

            ' Sexo ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            sSexo = UCase(rsCadastral("sexo"))

            ' Estado Civil ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            sEstCivil = UCase(rsCadastral("estado_civil"))

        Else

            ' CNPJ ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            If Not IsNull(rsCadastral("cpf_cgc")) Then
                sCPFCNPJ = Format(rsCadastral("cpf_cgc"), "&&.&&&.&&&/&&&&-&&")
            Else
                sCPFCNPJ = ""
            End If

        End If

        ' Endere�o ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        If Not IsNull(rsCadastral("endereco")) Then
            txtEndoCad(4).Text = Trim(rsCadastral("endereco"))
            TxtEnderecoAnt.Text = Trim(rsCadastral("endereco"))
            TxtEnderecoAnt.Tag = Trim(rsCadastral("endereco"))
        Else
            TxtEnderecoAnt.Tag = ""
        End If

        ' Bairro ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        If Not IsNull(rsCadastral("bairro")) Then
            txtEndoCad(5).Text = Trim(rsCadastral("bairro"))
            TxtBairroAnt.Text = Trim(rsCadastral("bairro"))
            TxtBairroAnt.Tag = Trim(rsCadastral("bairro"))
        Else
            TxtBairroAnt.Tag = ""
        End If

        ' Munic�pio '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        If Not IsNull(rsCadastral("municipio")) Then

            TxtCidadeAnt.Text = Trim(rsCadastral("municipio")) & " - " & Trim(rsCadastral("estado"))

            For iIndice = 0 To cboCidade.ListCount - 1
                If Left(cboCidade.List(iIndice), Len(cboCidade.List(iIndice)) - 5) = Trim(rsCadastral!Municipio) Then
                    cboCidade.ListIndex = iIndice
                    TxtCidadeAnt.Tag = iIndice
                    Exit For
                End If
            Next

            If cboCidade.ListIndex = -1 Then
                TxtCidadeAnt.Tag = -1
            End If

        Else
            TxtCidadeAnt.Tag = -1
        End If

        ' Estado ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        If Not IsNull(rsCadastral("estado")) Then
            TxtUFAnt(1).Text = Trim(rsCadastral("estado"))
            TxtUFAnt(1).Text = Trim(rsCadastral("estado"))
            TxtUFAnt(1).Tag = Trim(rsCadastral("estado"))
        Else
            TxtUFAnt(1).Tag = ""
        End If

        ' CEP '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        If Not IsNull(rsCadastral("cep")) Then
            If Trim(rsCadastral("cep")) <> "" Then
                txtCep.Text = Format(rsCadastral("cep"), "&&.&&&-&&&")
                TxtCEPAnt.Text = Format(rsCadastral("cep"), "&&.&&&-&&&")
                TxtCEPAnt.Tag = Format(rsCadastral("cep"), "&&.&&&-&&&")
            Else
                TxtCEPAnt.Tag = ""
            End If
        Else
            TxtCEPAnt.Tag = ""
        End If

        ' DDD '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        If Not IsNull(rsCadastral("ddd_1")) Then
            sDDD = rsCadastral("ddd_1")
        Else
            sDDD = ""
        End If

        ' Telefone ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        If Not IsNull(rsCadastral("telefone_1")) Then
            sTelefone = rsCadastral("telefone_1")
        Else
            sTelefone = ""
        End If

        ' E-mail ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        If Not IsNull(rsCadastral("e_mail")) Then
            sEmail = Trim(rsCadastral("e_mail"))
        Else
            sEmail = ""
        End If

        rsCadastral.Close

    End If

    Set rsCadastral = oApolice.ConsultarDadosBancarios(gsSIGLASISTEMA, _
                                                       App.Title, _
                                                       App.FileDescription, _
                                                       glAmbiente_id, _
                                                       oDadosEndosso.PropostaId)

    If Not rsCadastral.EOF Then

        If Not IsNull(rsCadastral("banco_id")) Then
            sBanco = rsCadastral("banco_id")
        Else
            sBanco = ""
        End If

        If Not IsNull(rsCadastral("agencia_id")) Then
            sCodAg = rsCadastral("agencia_id")
        Else
            sCodAg = ""
        End If

        If Not IsNull(rsCadastral("conta_corrente_id")) Then
            sCC = rsCadastral("conta_corrente_id")
        Else
            sCC = ""
        End If

        rsCadastral.Close

    End If

    Set rsCadastral = Nothing
    Set oApolice = Nothing

    Exit Sub

TrataErro:

    Call TratarErro("CarregarDadosCadastral", Me.name)
    Call FinalizarAplicacao

End Sub

Private Sub AplicarEndereco()

    Dim iEndossoSegBr As Integer
    Dim sEstadoCivil As String

    Const TP_CANCELAMENTO = "2"

    On Error GoTo TrataErro

    Me.MousePointer = vbHourglass

    'Ezequiel - 2004-05-20 - Inclus�o somente com altera��o de endere�o
    Call IncluirEndossoDadosCadastrais(oDadosEndosso.PropostaId, oDadosEndosso.TipoEndosso, _
                                       mskDtEndosso.Text, mskDtEmissao.Text, _
                                       Trim(txtDescEndosso.Text), Data_Sistema, cUserName, _
                                       oDadosEndosso.ClienteId, Trim(txtNomeEndereco.Text), _
                                       sSexo, sEstCivil, _
                                       Trim(TxtEnderecoAnt.Text), sTelefone, _
                                       sDDD, Trim(TxtCidadeAnt.Text), _
                                       Trim(TxtUFAnt(1).Text), Trim(TxtCEPAnt.Text), _
                                       sEmail, sCodAg, _
                                       sCC, sNascimento, _
                                       sCPFCNPJ, sBanco, _
                                       Trim(TxtBairroAnt.Text), Trim(Str(iTpPessoa)), _
                                       Trim(txtNomeEndereco.Text), sSexo, _
                                       sEstCivil, _
                                       Trim(txtEndoCad(4).Text), sTelefone, _
                                       sDDD, Trim(cboCidade.Text), _
                                       Trim(TxtUFAnt(1).Text), Trim(txtCep.Text), _
                                       sEmail, sCodAg, _
                                       sCC, sNascimento, _
                                       sCPFCNPJ, sBanco, _
                                       Trim(txtEndoCad(5).Text), _
                                       iTpPessoa)

    '    Call IncluirEndossoDadosCadastrais(oDadosEndosso.PropostaId, oDadosEndosso.TipoEndosso, _
         mskDtEndosso.Text, mskDtEmissao.Text, _
         Trim(txtDescEndosso.Text), Data_Sistema, cUserName, _
         oDadosEndosso.ClienteId, Trim(txtNomeAnt.Text), _
         Trim(txtSexoAnt.Text), Trim(txtEstCivilAnt.Text), _
         Trim(TxtEnderecoAnt.Text), Trim(TxtTelefoneAnt.Text), _
         Trim(TxtDDDAnt.Text), Trim(TxtCidadeAnt.Text), _
         Trim(TxtUFAnt(1)Ant(0).Text), Trim(TxtCEPAnt.Text), _
         Trim(TxtEmailAnt.Text), Trim(TxtCodAgAnt.Text), _
         Trim(TxtCCAnt.Text), Trim(txtNascAnt.Text), _
         Trim(txtCPFCNPJAnt.Text), Trim(txtendocad(8).Text), _
         Trim(TxtBairroAnt.Text), Trim(txtIdTpPessoaAnt.Text), _
         Trim(txtEndoCad(0).Text), Trim(Left(cboSexo.Text, 1)), _
         sEstadoCivil, _
         Trim(txtEndoCad(4).Text), Trim(txtEndoCad(3).Text), _
         Trim(txtEndoCad(2).Text), Trim(cboCidade.Text), _
         Trim(TxtUFAnt(1).Text), Trim(txtCep.Text), _
         Trim(txtEndoCad(1).Text), Trim(txtCodAgDebito(0).Text), _
         Trim(txtendocad(7).Text), Trim(txtNascimento.Text), _
         Trim(txtCPFCNPJ.Text), Trim(txtBanco.Text), _
         Trim(txtEndoCad(5).Text), _
         Trim(cboTpPessoa.ItemData(cboTpPessoa.ListIndex)))


    Me.MousePointer = vbDefault

    Call MsgBox("Endosso realizado com sucesso." & vbNewLine & _
                "Proposta: " & oDadosEndosso.PropostaId & vbNewLine & _
                "Endosso : " & txtNumEndosso.Text, vbInformation)

    Call sair

    Exit Sub

TrataErro:
    Call TratarErro("AplicarCancelamentoEndosso", Me.name)
    Call FinalizarAplicacao

End Sub

'bcarneiro - 18/07/2004 - Endosso de reativa��o de fatura
Private Sub CarregarDadosReativacaoFatura()

    Dim oSubGrupo As Object
    Dim rsSubgrupo As Recordset
    Dim rs As Recordset

    On Error GoTo TrataErro

    stbEndosso.TabVisible(11) = True
    stbEndosso.Tab = 11
    StbRodape.Panels(1).Text = "Selecione um subgrupo..."

    Frame5(5).Visible = False

    Set oSubGrupo = CreateObject("SEGL0020.cls00116")

    Set rsSubgrupo = oSubGrupo.Consultar(gsSIGLASISTEMA, _
                                         App.Title, _
                                         App.FileDescription, _
                                         glAmbiente_id, _
                                         oDadosEndosso.ApoliceId, _
                                         oDadosEndosso.RamoId, _
                                         oDadosEndosso.SeguradoraCodSusep, _
                                         oDadosEndosso.SucursalSeguradoraId)

    grdSubGrupos.Rows = 1

    While Not rsSubgrupo.EOF

        Set rs = oSubGrupo.ConsultarApoliceSubGrupo(gsSIGLASISTEMA, _
                                                    App.Title, _
                                                    App.FileDescription, _
                                                    glAmbiente_id, _
                                                    0, _
                                                    oDadosEndosso.SeguradoraCodSusep, _
                                                    oDadosEndosso.SucursalSeguradoraId, _
                                                    oDadosEndosso.RamoId, _
                                                    oDadosEndosso.ApoliceId)

        If Not rs.EOF Then

            If rs("situacao") <> "c" Then
                grdSubGrupos.AddItem rsSubgrupo("sub_grupo_id") & vbTab & _
                                     rsSubgrupo("nome")
            Else
                Call MsgBox("Esta ap�lice est� cancelada, n�o ser� poss�vel cancelar ou reativar faturas.", vbInformation)
                Call cmdSairReativacaoFatura_Click
            End If

        End If

        Set rs = Nothing
        rsSubgrupo.MoveNext

    Wend

    Set rsSubgrupo = Nothing
    Set oSubGrupo = Nothing

    Exit Sub

TrataErro:
    Call TratarErro("CarregarDadosReativacaoFatura", Me.name)
    Call FinalizarAplicacao

End Sub

'bcarneiro - 18/07/2004 - Endosso de reativa��o de fatura
Private Function VerificarMovimentacaoAberta()

    Const SITUACAO_MOV_ABERTA = "a"

    Dim oMovimentacaoApolice As Object
    Dim rsMovimentacao As Object

    On Error GoTo TrataErro

    Set oMovimentacaoApolice = CreateObject("SEGL0020.cls00115")

    Set rsMovimentacao = oMovimentacaoApolice.Consultar(gsSIGLASISTEMA, _
                                                        App.Title, _
                                                        App.FileDescription, _
                                                        glAmbiente_id, _
                                                        oDadosEndosso.SeguradoraCodSusep, _
                                                        oDadosEndosso.SucursalSeguradoraId, _
                                                        oDadosEndosso.RamoId, _
                                                        oDadosEndosso.SubGrupoId, _
                                                        oDadosEndosso.ApoliceId, _
                                                        SITUACAO_MOV_ABERTA)

    If Not rsMovimentacao.EOF Then
        VerificarMovimentacaoAberta = True
        Call MsgBox("Este subgrupo est� com movimenta��o aberta e suas faturas n�o podem ser reativadas.", vbInformation)
    Else
        VerificarMovimentacaoAberta = False
    End If

    Set rsMovimentacao = Nothing
    Set oMovimentacaoApolice = Nothing

    Exit Function

TrataErro:
    Call TrataErroGeral("VerificarMovimentacaoAberta", Me.Caption)
    Call FinalizarAplicacao

End Function

'bcarneiro - 18/07/2004 - Endosso de reativa��o de fatura
Public Sub CarregarDadosFaturasCanceladas()

    Dim oFinanceiro As Object
    Dim rsFatura As Object
    Dim sSituacao As String

    Const CANCELADOS_NAO_REATIVADOS = True

    On Error GoTo TrataErro

    MousePointer = vbHourglass

    If Not VerificarMovimentacaoAberta Then

        StbRodape.Panels(1).Text = "Selecione uma fatura cancelada para reativa��o..."

        sSituacao = "c"

        grdFaturaCancelada.Rows = 1

        Set oFinanceiro = CreateObject("SEGL0026.cls00125")

        Set rsFatura = oFinanceiro.ConsultarFatura(gsSIGLASISTEMA, _
                                                   App.Title, _
                                                   App.FileDescription, _
                                                   glAmbiente_id, _
                                                   oDadosEndosso.SeguradoraCodSusep, _
                                                   oDadosEndosso.SucursalSeguradoraId, _
                                                   oDadosEndosso.ApoliceId, _
                                                   oDadosEndosso.RamoId, _
                                                   oDadosEndosso.SubGrupoId, _
                                                   sSituacao, _
                                                   CANCELADOS_NAO_REATIVADOS)

        While Not rsFatura.EOF

            grdFaturaCancelada.AddItem rsFatura("fatura_id") & vbTab & _
                                       Format(rsFatura("tot_imp_segurada"), "#,##0.00") & vbTab & _
                                       Format(rsFatura("val_bruto"), "#,##0.00") & vbTab & _
                                       "Cancelada" & vbTab & _
                                       Format(rsFatura("dt_inicio_vigencia"), "dd/mm/yyyy") & vbTab & _
                                       Format(rsFatura("dt_fim_vigencia"), "dd/mm/yyyy") & vbTab & _
                                       rsFatura("endosso_id")

            rsFatura.MoveNext
        Wend

        grdFaturaCancelada.ColWidth(6) = 0

        Set rsFatura = Nothing
        Set oFinanceiro = Nothing

        Frame5(5).Visible = True

    End If

    MousePointer = vbNormal

    Exit Sub

TrataErro:

    Call TratarErro("CarregarDadosFaturasCanceladas", Me.name)
    Call FinalizarAplicacao

End Sub

'bcarneiro - 18/07/2004 - Endosso de reativa��o de fatura
Private Sub ReativarFatura()
    Dim linhas As String    '10/12/2015 - schoralik - 18690344 - Melhorias Processo Reagendamento e Reativa��o BESC
    Dim oFatura As Object
    Dim lEndossoId As Long
    Dim lFaturaId As Long

    On Error GoTo TrataErro
    lFaturaId = Val(grdFaturaCancelada.TextMatrix(grdFaturaCancelada.Row, 0))

    If MsgBox("Fatura Cancelada : " & lFaturaId & vbNewLine & "Tem certeza que deseja reativar essa fatura ?", vbYesNo) = vbYes Then

        MousePointer = vbHourglass

        Set oFatura = CreateObject("SEGL0020.cls00161")

        lEndossoId = oFatura.Reativar(gsSIGLASISTEMA, _
                                      App.Title, _
                                      App.FileDescription, _
                                      glAmbiente_id, _
                                      oDadosEndosso.SeguradoraCodSusep, _
                                      oDadosEndosso.SucursalSeguradoraId, _
                                      oDadosEndosso.ApoliceId, _
                                      oDadosEndosso.RamoId, _
                                      lFaturaId, _
                                      Data_Sistema, _
                                      mskDtEndosso.Text, _
                                      Trim(txtDescEndosso.Text), _
                                      cUserName)

        Set oFatura = Nothing

        MousePointer = vbNormal

        Call MsgBox("Endosso realizado com sucesso." & vbNewLine & _
                    "Proposta: " & oDadosEndosso.PropostaId & vbNewLine & _
                    "Endosso : " & lEndossoId, vbInformation)

        Call sair

    End If

    Exit Sub

TrataErro:
    Call TrataErroGeral("ReativarFatura", Me.Caption)
    Call FinalizarAplicacao

End Sub

'bcarneiro - 18/07/2004 - Endosso de reativa��o de fatura
Private Function ValidarDadosReativacaoFatura() As Boolean

    On Error GoTo TrataErro

    ValidarDadosReativacaoFatura = True

    If grdSubGrupos.Row < 1 Or Trim(grdSubGrupos.TextMatrix(grdSubGrupos.Row, 0)) = "" Then
        Call MsgBox("Selecione um subgrupo.", vbCritical)
        grdSubGrupos.SetFocus
        ValidarDadosReativacaoFatura = False
        Exit Function
    End If

    If grdFaturaCancelada.Row < 1 Or Trim(grdFaturaCancelada.TextMatrix(grdFaturaCancelada.Row, 0)) = "" Then
        Call MsgBox("Selecione uma fatura para reativa��o.", vbCritical)
        grdSubGrupos.SetFocus
        ValidarDadosReativacaoFatura = False
        Exit Function
    End If

    Exit Function

TrataErro:
    Call TrataErroGeral("ValidarDadosReativacaoFatura", Me.Caption)

End Function
'lrocha 11/12/2007
Private Function ValidarEndossoProduto() As Boolean
    On Error GoTo TrataErro

    Dim lPropostaId As Long
    Dim OEndosso As Object
    Dim oRs As Object

    lPropostaId = frmSelecao.grdSelecao.TextMatrix(frmSelecao.grdSelecao.Row, 3)

    'Obtendo endosso
    Set OEndosso = CreateObject("SEGL0026.cls00124")

    Set oRs = OEndosso.ObterEndosso(gsSIGLASISTEMA, _
                                    App.Title, _
                                    App.FileDescription, _
                                    glAmbiente_id, _
                                    lPropostaId, _
                                    TP_ENDOSSO_BONIFICACAO)

    ValidarEndossoProduto = True

    If Not oRs.EOF Then
        ValidarEndossoProduto = False
    End If

    oRs.Close
    Set OEndosso = Nothing


    Exit Function

TrataErro:

    Call TratarErro("ValidarEndossoProduto", Me.name)
    Call FinalizarAplicacao

End Function

Private Function IdentificaPPE(strCPF As String) As Boolean

'Sessao de variaveis
    Dim StrSQL As String
    Dim rc As Recordset

    'Manipulador de erros
    On Error GoTo ErrIdentificaPPE

    'Valor Padrao
    IdentificaPPE = False

    'Retira Formacao caso exista
    strCPF = Replace(strCPF, ".", "")
    strCPF = Replace(strCPF, "-", "")

    'Busca Identificar se faz parte do PPE
    StrSQL = "SELECT COUNT(*) AS TOTAL "
    StrSQL = StrSQL & " FROM seguros_db..PPE_ORGAO_CARGO_TB as TPPE "
    StrSQL = StrSQL & " WHERE CAST(TPPE.CPF AS BIGINT) = CAST('" & strCPF & "' AS BIGINT)"
    StrSQL = StrSQL & " AND (TPPE.DT_EXCLUSAO_PPE IS NULL OR  TPPE.DT_EXCLUSAO_PPE >= GETDATE())"

    If rdocn Is Nothing Then
        Call Conexao
    Else
        If rdocn.State = 0 Then Call Conexao
    End If

    'Executa o Comando
    Set rc = rdocn.Execute(StrSQL)

    'Verifica se voltou registro
    If rc.EOF Then GoTo Finaliza

    'Verifica Valor Valido
    If IsNull(rc!TOTAL) Then GoTo Finaliza

    'vERIFICA nao ENCONTROU
    If CInt(rc!TOTAL) <= 0 Then GoTo Finaliza

    'Valor Padrao
    IdentificaPPE = True

    'Finaliza
Finaliza:
    rc.Close
    rdocn.Close
    Set rdocn = Nothing
    Set rc = Nothing

    'Sai
    Exit Function

    'Manipulador de erros
ErrIdentificaPPE:

    'Fecha Recordsrt
    Err.Clear
    Set rc = Nothing

End Function


'lrocha 20/08/2009 - Selecionar o 7� dia �ltil a partir da data do sistema
Function DiasUteis(ByVal DataBase, ByVal NumDias) As Variant
    Dim OSQL As String
    Dim FlagDia As Integer
    Dim RsFeriados As Recordset

    Call Conexao


    FlagDia = 1
    While FlagDia <= NumDias

        DataBase = Format(CVDate(DataBase) + 1, "dd/mm/yyyy")

        ' Verifica se data encontrada � s�bado ou domingo

        If Weekday(DataBase) = 1 Then     ' Domingo
            DataBase = CVDate(DataBase) + 1
        ElseIf Weekday(DataBase) = 7 Then    ' S�bado
            DataBase = CVDate(DataBase) + 2
        End If

        DataBase = Format(DataBase, "dd/mm/yyyy")

        ' Verifica se data encontrada � feriado

        OSQL = "Select nome "
        OSQL = OSQL & "from FERIADO_TB "
        OSQL = OSQL & "where DATA = " & FormataData(DataBase, "S", "S") & " "
        Set RsFeriados = rdocn.Execute(OSQL)


        If Not RsFeriados.EOF Then
            Do While Not RsFeriados.EOF Or (Weekday(CVDate(DataBase)) = 7 Or Weekday(CVDate(DataBase)) = 1)
                DataBase = Format(CVDate(DataBase) + 1, "dd/mm/yyyy")
                OSQL = "Select nome "
                OSQL = OSQL & "from FERIADO_TB "
                OSQL = OSQL & "where DATA = " & FormataData(DataBase, "S", "S") & " "
                Set RsFeriados = rdocn.Execute(OSQL)
            Loop
        End If
        RsFeriados.Close

        FlagDia = FlagDia + 1

    Wend

    DiasUteis = DataBase

    Set rdocn = Nothing

End Function
Private Function Carregar_Dados_Reducao_IS(Proposta As Long)


    On Error GoTo TrataErro

    Dim OSinistro As Object
    Dim oRs As Recordset

    Dim i As Integer
    Dim Linha As String
    Dim Situacao As String

    stbEndosso.TabVisible(13) = True

    If txtProduto.Text = "1204" Then
        TxtArea(4).Visible = True
        Label2(3).Visible = True
        Label2(4).Caption = "IS = �rea p�s  x Custeio x Fator Replant."
        '1152
    Else
        TxtArea(4).Visible = False
        Label2(3).Visible = False
        Label2(4).Caption = "IS = IS - Valor Indenizado"
    End If

    'Obtendo endosso
    Set OSinistro = CreateObject("SEGL0026.cls00126")

    Set oRs = OSinistro.Consulta_Sinistros(gsSIGLASISTEMA, _
                                           App.Title, _
                                           App.FileDescription, _
                                           glAmbiente_id, _
                                           Proposta)



    While Not oRs.EOF

        Linha = oRs!proposta_id & vbTab & oRs!Sinistro_id & vbTab & oRs!tp_cobertura_id & vbTab & oRs!nome_cob & vbTab & Format(oRs!dt_aviso_sinistro, "dd/mm/yyyy") & vbTab & "Encerrado" & vbTab & oRs!Val_Pago & vbTab & _
                oRs!ramo_id & vbTab & oRs!sucursal_seguradora_id & vbTab & oRs!seguradora_cod_susep & vbTab & oRs!Apolice_id & vbTab & _
                oRs!cod_objeto_segurado & vbTab & oRs!processa_reintegracao_is & vbTab & oRs!Tp_Objeto_id & vbTab & oRs!Val_Pago & vbTab & oRs!dt_evento

        grdSinistros.AddItem Linha

        oRs.MoveNext
    Wend


    grdSinistros.ColWidth(0) = 1000
    grdSinistros.ColWidth(1) = 1050
    grdSinistros.ColWidth(2) = 800
    grdSinistros.ColWidth(3) = 2550
    grdSinistros.ColWidth(4) = 1400
    grdSinistros.ColWidth(5) = 1250
    grdSinistros.ColWidth(6) = 1300

    grdSinistros.ColWidth(7) = 0
    grdSinistros.ColWidth(8) = 0
    grdSinistros.ColWidth(9) = 0
    grdSinistros.ColWidth(10) = 0
    grdSinistros.ColWidth(11) = 0
    grdSinistros.ColWidth(12) = 0
    grdSinistros.ColWidth(13) = 0
    grdSinistros.ColWidth(14) = 0
    grdSinistros.ColWidth(15) = 0
    'grdSinistros.ColWidth(16) = 0
    'grdSinistros.ColWidth(17) = 0



    oRs.Close
    Set OSinistro = Nothing

    Exit Function

TrataErro:

    Call TratarErro("ValidarEndossoProduto", Me.name)

End Function
Public Sub grdSinistros_Click()

    On Error GoTo TrataErro
    '
    Dim OValores As Object
    Dim rSValores As Recordset
    Dim StrSQL As String
    '
    Dim Area As String
    Dim Custeio As String
    Dim Cobertura As Integer
    Dim val_is As String

    Set OValores = CreateObject("SEGL0026.cls00126")

    Set rSValores = OValores.Consulta_Objeto_Segurado(gsSIGLASISTEMA, _
                                                      App.Title, _
                                                      App.FileDescription, _
                                                      glAmbiente_id, _
                                                      CLng(grdSinistros.TextMatrix(grdSinistros.Row, 0)), _
                                                      CLng(txtProduto.Text), _
                                                      CInt(grdSinistros.TextMatrix(grdSinistros.Row, 11)), _
                                                      CDbl(grdSinistros.TextMatrix(grdSinistros.Row, 1)))

    While Not rSValores.EOF

        If rSValores!Tipo = "Area" Then
            TxtArea(0).Text = Trim(rSValores!texto_resposta)
        ElseIf rSValores!Tipo = "Custeio" Then
            TxtArea(3).Text = Trim(rSValores!texto_resposta)
        ElseIf rSValores!Tipo = "area_sinistrada" Then
            TxtArea(1).Text = Format(Trim(rSValores!texto_resposta), "##0.00")
        Else
            TxtArea(4).Text = Format(Trim(rSValores!texto_resposta), "##0.00") / 100
            TxtArea(4).Text = Format(TxtArea(4).Text, "##0.00")
        End If

        rSValores.MoveNext
    Wend

    Set rSValores = Nothing

    If CLng(txtProduto.Text) = 1152 Then
        Cobertura = 339
    ElseIf CLng(txtProduto.Text) = 1204 Then
        Cobertura = 834
    End If


    Set OValores = CreateObject("SEGL0026.cls00126")

    Set rSValores = OValores.Consulta_IS(gsSIGLASISTEMA, _
                                         App.Title, _
                                         App.FileDescription, _
                                         glAmbiente_id, _
                                         CLng(grdSinistros.TextMatrix(grdSinistros.Row, 0)), _
                                         Cobertura, CInt(grdSinistros.TextMatrix(grdSinistros.Row, 11)))


    TxtArea(6).Text = Format(rSValores!val_is, "##0.00")

    rSValores.Close


    'C�lculo da IS
    If txtProduto.Text = "1152" Then
        TxtArea(5).Text = Format((TxtArea(6).Text - grdSinistros.TextMatrix(grdSinistros.Row, 6)), "#,##0.00")
        TxtArea(5).Text = Replace(TxtArea(5).Text, ".", "")
    End If

    TxtArea(1).SetFocus

    Exit Sub

TrataErro:

    Call TratarErro("grdSinistros_Click", Me.name)

End Sub
Private Function Carregar_Dados_Reintegracao_IS(Proposta As Long)


    On Error GoTo TrataErro

    Dim OSinistroReintegra As Object
    Dim oRs As Recordset

    Dim i As Integer
    Dim Linha As String
    Dim Situacao As String

    stbEndosso.TabVisible(14) = True

    If txtProduto.Text = "1204" Then
        TxtArea(31).Visible = True
        Label2(21).Visible = True
        'Label2(12).Visible = True
        'TxtArea(32).Visible = True
        Label2(20).Caption = "IS = Nova �rea  x Custeio x Fator Replant."
    Else
        TxtArea(31).Visible = False
        Label2(21).Visible = False
        'Label2(12).Visible = False
        'TxtArea(32).Visible = False
        Label2(20).Caption = "IS"
    End If

    'Obtendo endosso
    Set OSinistroReintegra = CreateObject("SEGL0026.cls00126")

    Set oRs = OSinistroReintegra.Consulta_Sinistros_Reintegracao(gsSIGLASISTEMA, _
                                                                 App.Title, _
                                                                 App.FileDescription, _
                                                                 glAmbiente_id, _
                                                                 Proposta)



    While Not oRs.EOF

        Linha = oRs!proposta_id & vbTab & oRs!endosso_id & vbTab & oRs!Sinistro_id & vbTab & oRs!Apolice_id & vbTab & oRs!sucursal_seguradora_id & vbTab & _
                oRs!seguradora_cod_susep & vbTab & oRs!ramo_id & vbTab & oRs!Dt_Endosso & vbTab & oRs!Dt_Pedido_Endosso & vbTab & oRs!Produto_id & vbTab & oRs!tp_cobertura_id _
              & vbTab & oRs!nome_cob & vbTab & oRs!Tp_Objeto_id & vbTab & oRs!val_Reintegracao & vbTab & oRs!banco_id & vbTab & oRs!agencia_id & vbTab & oRs!conta_corrente_id & vbTab & oRs!Num_Cartao & vbTab & oRs!Dt_Validade_Cartao & vbTab & oRs!Dt_Inicio_Vigencia & vbTab & oRs!Seguro_Moeda_id & vbTab & oRs!cod_objeto_segurado
        grdReintegracao(1).AddItem Linha

        oRs.MoveNext
    Wend


    grdReintegracao(1).ColWidth(0) = 1000
    grdReintegracao(1).ColWidth(1) = 1050
    grdReintegracao(1).ColWidth(2) = 1200
    grdReintegracao(1).ColWidth(3) = 1000
    grdReintegracao(1).ColWidth(4) = 0
    grdReintegracao(1).ColWidth(5) = 0
    grdReintegracao(1).ColWidth(6) = 1000
    grdReintegracao(1).ColWidth(7) = 1300
    grdReintegracao(1).ColWidth(8) = 1300
    grdReintegracao(1).ColWidth(9) = 1300
    grdReintegracao(1).ColWidth(10) = 1000
    grdReintegracao(1).ColWidth(11) = 4000
    grdReintegracao(1).ColWidth(12) = 0
    grdReintegracao(1).ColWidth(13) = 0
    grdReintegracao(1).ColWidth(14) = 0
    grdReintegracao(1).ColWidth(15) = 0
    grdReintegracao(1).ColWidth(16) = 0
    grdReintegracao(1).ColWidth(17) = 0
    grdReintegracao(1).ColWidth(18) = 0
    grdReintegracao(1).ColWidth(19) = 0
    grdReintegracao(1).ColWidth(20) = 0
    grdReintegracao(1).ColWidth(21) = 0
    'grdReintegracao(1).ColWidth(22) = 0

    oRs.Close
    Set OSinistroReintegra = Nothing

    Exit Function

TrataErro:

    Call TratarErro("ValidarEndossoProduto", Me.name)

End Function
Sub BuscaSubRamo(ByVal sPropostaId As String, ByRef iSubRamo As Integer)

    Dim rc As Recordset
    Dim sSQL As String

    On Error GoTo TrataErro

    'Obtendo o pr�ximo endosso ''''''''''''''''''''''

    sSQL = ""
    sSQL = sSQL & "Select subramo_id" & vbNewLine
    sSQL = sSQL & " FROM proposta_tb  WITH (NOLOCK)  " & vbNewLine
    sSQL = sSQL & "WHERE proposta_id = " & sPropostaId

    If rdocn Is Nothing Then
        Call Conexao
    Else
        If rdocn.State = 0 Then Call Conexao
    End If

    Set rc = rdocn.Execute(sSQL)

    iSubRamo = rc!Subramo_id


    Exit Sub


TrataErro:
    Call TrataErroGeral("BuscaSubRamo", Me.name)
    Call TerminaSEGBR

End Sub
Private Sub Processa_endosso_da_proposta_anterior(ByVal ONovo_Endosso_id As String)

'Gera registro de retorno de reintegra��o para o BB
    If Proposta_BB_ant <> "0" Then
        'Gera registro de retorno ao BB
        Call Gera_Registro_emi_RE(Proposta_BB_ant, Arquivo_ant, Cod_ramo_BB_ant, Proponente_ant, ONovo_Endosso_id)
    End If

End Sub
Private Sub Gera_Registro_emi_RE(ByVal Proposta_BB As String, ByVal ARQUIVO As String, ByVal Cod_ramo_BB As String, ByVal Proponente As String, ByVal ONovo_Endosso_id As String)

    Dim SQL As String, rc As Recordset, Emi_RE_proposta_id As String

    On Error GoTo Erro

    SQL = "EXEC emi_re_proposta_spi "
    SQL = SQL & Proposta_BB    '@proposta_bb
    SQL = SQL & ", '" & ARQUIVO & "'"    '@arquivo
    SQL = SQL & ", '03' "    '@tp_registro (03=endosso)
    SQL = SQL & ", 0"    '@sequencial
    SQL = SQL & ", NULL"    '@cod_seguradora
    SQL = SQL & ", '" & Cod_ramo_BB & "'"    '@cod_ramo_bb
    SQL = SQL & ", NULL"    '@agencia_contratante
    SQL = SQL & ", NULL"    '@agencia_contratante_dv
    SQL = SQL & ", NULL"    '@agencia_contratante_nome
    SQL = SQL & ", " & IIf(Nosso_Numero = "", "NULL", "'" & Nosso_Numero & "'")    '@nosso_numero
    SQL = SQL & ", " & IIf(Nosso_Numero_dv = "", "NULL", "'" & Nosso_Numero_dv & "'")    '@nosso_numero_dv
    SQL = SQL & ", NULL"    '@funcionario_bb
    SQL = SQL & ", '3' "    '@tp_operacao
    SQL = SQL & ", NULL"    '@seguro_anterior
    SQL = SQL & ", NULL"    '@agencia_cliente
    SQL = SQL & ", NULL"    '@agencia_cliente_dv
    SQL = SQL & ", NULL"    '@conta_corrente
    SQL = SQL & ", NULL"    '@conta_corrente_dv
    SQL = SQL & ", NULL"    '@cod_super
    SQL = SQL & ", NULL"    '@ourocard
    SQL = SQL & ", NULL"    '@cod_bdc
    SQL = SQL & ", " & Tp_Pessoa_ant    '@tp_pessoa
    SQL = SQL & ", '" & Proponente & "'"    '@proponente
    SQL = SQL & ", " & IIf(CPF_ant = "", "NULL", "'" & CPF_ant & "'")    '@cpf
    SQL = SQL & ", " & IIf(CGC_ant = "", "NULL", "'" & CGC_ant & "'")    '@cgc
    SQL = SQL & ", " & IIf(Dt_Nascimento_ant = "", "NULL", "'" & Dt_Nascimento_ant & "'")    '@dt_nascimento
    SQL = SQL & ", NULL"    '@sexo
    SQL = SQL & ", NULL"    '@estado_civil
    SQL = SQL & ", NULL"    '@endereco
    SQL = SQL & ", NULL"    '@Bairro
    SQL = SQL & ", NULL"    '@Municipio
    SQL = SQL & ", NULL"    '@estado
    SQL = SQL & ", NULL"    '@CEP
    SQL = SQL & ", NULL"    '@ddd
    SQL = SQL & ", NULL"    '@telefone
    SQL = SQL & ", NULL"    '@e_mail
    SQL = SQL & ", NULL"    '@cod_prof_pf
    SQL = SQL & ", NULL"    '@desc_prof_pf
    SQL = SQL & ", NULL"    '@cod_atividade
    SQL = SQL & ", NULL"    '@nome_atividade
    SQL = SQL & ", '" & Dt_Inicio_Vigencia_Aux & "'"    '@dt_inicio_vigencia
    SQL = SQL & ", NULL"    '@clausula_benef
    SQL = SQL & ", NULL"    '@danos_elet_ativ
    SQL = SQL & ", NULL"    '@roubo_regiao
    SQL = SQL & ", NULL"    '@roubo_ativ
    SQL = SQL & ", NULL"    '@tp_const_inc
    SQL = SQL & ", NULL"    '@local_risco_inc
    SQL = SQL & ", NULL"    '@reg_vendaval
    SQL = SQL & ", NULL"    '@proposta_bb_anterior
    SQL = SQL & ", NULL"    '@ind_houve_sinistro
    SQL = SQL & ", NULL"    '@capital_segurado
    SQL = SQL & ", NULL"    '@val_is_total
    SQL = SQL & ", NULL"    '@val_premio_liquido
    SQL = SQL & ", NULL"    '@desconto_renov
    SQL = SQL & ", NULL"    '@desconto_progres
    SQL = SQL & ", NULL"    '@desconto_local
    SQL = SQL & ", NULL"    '@val_iof
    SQL = SQL & ", NULL"    '@custo_apolice
    SQL = SQL & ", " & Str(Valor_premio)    '@val_premio_bruto
    SQL = SQL & ", NULL"    '@num_parcelas
    SQL = SQL & ", NULL"    '@taxa_juros
    SQL = SQL & ", NULL"    '@pgto_ato
    SQL = SQL & ", NULL"    '@val_pgto_ato
    SQL = SQL & ", NULL"    '@val_parcela
    SQL = SQL & ", NULL"    '@val_total_demais
    SQL = SQL & ", NULL"    '@perc_corretagem
    SQL = SQL & ", '" & Tp_Pgto_ant & "'"    '@tp_pgto
    SQL = SQL & ", NULL"    '@dia_debito
    SQL = SQL & ", NULL"    '@prazo_seguro
    SQL = SQL & ", NULL"    '@dt_emissao
    SQL = SQL & ", 94"   '@tipo_endosso (94=Redu��o de IS)
    SQL = SQL & ", " & IIf(ONovo_Endosso_id = "", "NULL", ONovo_Endosso_id)    '@num_endosso
    SQL = SQL & ", '" & Format(Data_Sistema, "yyyymmdd") & "'"    '@dt_contratacao
    SQL = SQL & ", NULL"    '@cod_moeda
    SQL = SQL & ", 'e' "    '@Situacao
    SQL = SQL & ", '" & Dt_Inicio_Vigencia_Aux & "'"  '@dt_alteracao_situacao
    SQL = SQL & ", NULL"    '@situacao_estoque
    SQL = SQL & ", NULL"    '@dt_cancelamento
    SQL = SQL & ", NULL"    '@dt_aceite
    SQL = SQL & ", '" & Format(Data_Sistema, "yyyymmdd") & "'"    '@dt_endosso
    SQL = SQL & ", 'n' "    '@retorno_bb
    SQL = SQL & ", NULL"    '@periodo_pgto
    SQL = SQL & ", NULL"    '@dt_venc_visa
    SQL = SQL & ", NULL"    '@cod_corretor
    SQL = SQL & ", '" & Cod_ramo_BB & "'"    '@produto_externo
    SQL = SQL & ", NULL"    '@ind_sinistro
    SQL = SQL & ", NULL"    '@ind_terreno
    SQL = SQL & ", NULL"    '@ind_favela
    SQL = SQL & ", NULL"    '@ind_combustivel
    SQL = SQL & ", NULL"    '@coef_ajuste
    SQL = SQL & ", NULL"    '@dt_fim_vigencia
    SQL = SQL & ", NULL"    '@apolice_certif
    SQL = SQL & ", NULL"    '@outra_seguradora
    SQL = SQL & ", NULL"    '@tp_const_res
    SQL = SQL & ", NULL"    '@tp_predio
    SQL = SQL & ", NULL"    '@tp_moradia
    SQL = SQL & ", '" & cUserName & "'"    '@usuario
    SQL = SQL & ", NULL"    '@val_desconto
    SQL = SQL & ", NULL"    '@coef_fracionamento
    SQL = SQL & ", NULL"    '@cod_outra_seguradora
    SQL = SQL & ", NULL"    '@distribuicao_verba
    SQL = SQL & ", NULL"    '@cod_cliente_bb
    SQL = SQL & ", NULL"    '@seq_cliente_bb
    SQL = SQL & ", NULL"    '@ind_tp_endosso
    SQL = SQL & ", NULL"    '@cod_rubrica
    SQL = SQL & ", NULL"    '@cod_sub_rubrica
    SQL = SQL & ", NULL"    '@local_condicao
    SQL = SQL & ", NULL"    '@ind_cliente_ouro
    SQL = SQL & ", NULL"    '@desconto_shopping
    SQL = SQL & ", NULL"    '@desconto_fidelidade
    SQL = SQL & ", " & Proposta_id_ant  '@Proposta_id
    SQL = SQL & ", " & IIf(ONovo_Endosso_id = "", "NULL", ONovo_Endosso_id)    '@Endosso_id
    SQL = SQL & ", NULL"    '@CanalVenda

    If rdocn Is Nothing Then
        Call Conexao
    Else
        If rdocn.State = 0 Then Call Conexao
    End If

    Set rc = rdocn.Execute(SQL)


    'Obt�m o emi_RE_proposta_id para gravar na emi_RE_cobertura_tb
    Emi_RE_proposta_id = rc(0)
    rc.Close

    Call Gera_Registro_emi_RE_cobertura(Emi_RE_proposta_id)

    Exit Sub

Erro:
    TrataErroGeral "Gera_Registro_emi_RE"
    TerminaSEGBR

End Sub
Private Sub Gera_Registro_emi_RE_cobertura(ByVal Emi_RE_proposta_id As String)

    Dim i As Integer, SQL As String, rc As Recordset

    On Error GoTo Erro

    'For i = 1 To Seq_Cob
    'Grava registro da cobertura na emi_re_cobertura
    SQL = "EXEC emi_re_cobertura_spi "
    SQL = SQL & Emi_RE_proposta_id      '@emi_re_proposta_id
    SQL = SQL & ", " & i                '@item
    SQL = SQL & ", '" & Coberturas(i).ID & "'"     '@cod_cobertura
    SQL = SQL & ", '" & Coberturas(i).Nome & "'"   '@desc_cobertura
    SQL = SQL & ", " & Str(Coberturas(i).Novo_Valor_IS)    '@val_risco_cobertura
    SQL = SQL & ", NULL"                '@taxa_net
    SQL = SQL & ", NULL"                '@taxa_cobertura
    SQL = SQL & ", NULL"                '@desconto
    SQL = SQL & ", NULL"                '@val_premio
    SQL = SQL & ", '" & cUserName & "'"    '@usuario
    SQL = SQL & ", NULL"                '@coef_agravacao
    SQL = SQL & ", NULL"                '@val_franquia
    SQL = SQL & ", NULL"                '@franquia
    SQL = SQL & ", '" & Coberturas(i).ID & "'"    '@cod_cobertura_seg
    SQL = SQL & ", '" & Coberturas(i).ID & "'"    '@cod_cobertura_sise
    SQL = SQL & ", 7"                   '@comando de redu��o e Reintegra��o de IS por sinistro
    SQL = SQL & ", NULL"                '@relacionamento

    If rdocn Is Nothing Then
        Call Conexao
    Else
        If rdocn.State = 0 Then Call Conexao
    End If

    Set rc = rdocn.Execute(SQL)

    'Next i

    Exit Sub

Erro:
    TrataErroGeral "Gera_Registro_emi_RE_cobertura"
    TerminaSEGBR

End Sub
Private Sub Obtem_Dados_emi_RE( _
        ByVal AProposta_id As String, ByRef AProposta_BB As String, ByRef OArquivo As String, _
        ByRef OCod_ramo_BB As String, ByRef OTp_Pessoa As String, ByRef OCPF As String, _
        ByRef OCGC As String, ByRef ADt_Nascimento As String, ByRef OTp_Pgto As String, _
        ByRef ADt_Inicio_Vigencia As String, ByRef OProponente As String)

    Dim SQL As String
    Dim rc As Recordset

    On Error GoTo Erro

    AProposta_BB = "0"
    'Obt�m a proposta BB
    SQL = "SELECT f.proposta_BB, f.forma_pgto_id, a.dt_inicio_vigencia"
    SQL = SQL & " FROM proposta_fechada_tb f"
    SQL = SQL & "   INNER JOIN apolice_tb a"
    SQL = SQL & "      ON a.proposta_id = f.proposta_id"
    SQL = SQL & " WHERE f.proposta_id = " & AProposta_id
    SQL = SQL & " UNION "
    SQL = SQL & " SELECT proposta_BB, forma_pgto_id, dt_inicio_vigencia"
    SQL = SQL & " FROM proposta_adesao_tb "
    SQL = SQL & " WHERE proposta_id = " & AProposta_id

    If rdocn Is Nothing Then
        Call Conexao
    Else
        If rdocn.State = 0 Then Call Conexao
    End If

    Set rc = rdocn.Execute(SQL)

    If Not rc.EOF Then
        AProposta_BB = IIf(IsNull(rc("proposta_BB")), "0", rc("proposta_BB"))
        OTp_Pgto = IIf(IsNull(rc("forma_pgto_id")), "0", rc("forma_pgto_id"))
        ADt_Inicio_Vigencia = IIf(IsNull(rc("dt_inicio_vigencia")), "19000101", Format(rc("dt_inicio_vigencia"), "YYYYMMDD"))
    End If
    rc.Close

    If AProposta_BB = "0" Then Exit Sub

    'Obt�m o nome do arquivo e o ramo BB
    SQL = "SELECT a.arquivo, a.produto_externo_id, c.nome, tp_pessoa = CASE WHEN pj.cgc IS NULL THEN 1 ELSE 2 END, "
    SQL = SQL & " CPF = pf.cpf, CGC = pj.cgc, data_nascimento = convert(varchar(8), pf.dt_nascimento, 112)"
    SQL = SQL & " FROM arquivo_produto_tb a"
    SQL = SQL & "     INNER JOIN proposta_tb b"
    SQL = SQL & "         ON (b.produto_id     = a.produto_id"
    SQL = SQL & "         AND b.produto_externo_id = a.produto_externo_id)"
    SQL = SQL & "     INNER JOIN cliente_tb c"
    SQL = SQL & "         ON c.cliente_id = b.prop_cliente_id"
    SQL = SQL & "     LEFT JOIN pessoa_fisica_tb pf"
    SQL = SQL & "         ON pf.pf_cliente_id = c.cliente_id"
    SQL = SQL & "     LEFT JOIN pessoa_juridica_tb pj"
    SQL = SQL & "         ON pj.pj_cliente_id = c.cliente_id"
    SQL = SQL & " WHERE b.Proposta_id = " & AProposta_id

    If rdocn Is Nothing Then
        Call Conexao
    Else
        If rdocn.State = 0 Then Call Conexao
    End If

    Set rc = rdocn.Execute(SQL)

    If Not rc.EOF Then
        OArquivo = rc("arquivo") & ".0000"
        OCod_ramo_BB = rc("produto_externo_id")
        OProponente = rc("nome")
        OTp_Pessoa = rc("tp_pessoa")
        OCPF = IIf(IsNull(rc("cpf")), "", rc("cpf"))
        OCGC = IIf(IsNull(rc("cgc")), "", rc("cgc"))
        ADt_Nascimento = IIf(IsNull(rc("data_nascimento")), "", rc("data_nascimento"))
    Else
        MensagemBatch "Dados do arquivo ou cliente n�o encontrado!", vbOKOnly, "Redu��o IS", True, Me
        TerminaSEGBR
    End If

    Exit Sub

Erro:
    MensagemBatch "Rotina: Obtem_Dados_emi_RE. Programa ser� cancelado.", vbOKOnly, "Erro obtendo dados emi RE", True
    Call TerminaSEGBR

End Sub

Private Function Carregar_Dados_Adequacao_Area(Proposta As Long, num_tab As Integer, num_grid As Integer)


    On Error GoTo TrataErro

    Dim oAdequacao_Area As Object
    Dim oRs As Recordset

    Dim i As Integer
    Dim Linha As String
    Dim Situacao As String

    stbEndosso.TabVisible(num_tab) = True

    TxtArea(33).Text = "N"
    TxtArea(33).Visible = False

    If txtProduto.Text = "1204" Then
        TxtArea(23).Visible = True
        Label2(6).Visible = True
        TxtArea(28).Visible = True
        Label2(24).Visible = True
        Frame1(2).Visible = True
        Label2(10).Caption = "Faturamento Esperado X N�vel de Cobertura"
        'fadao
        Frame1(20).Visible = True
        'TxtArea(35).Visible = True
        TxtArea(36).Visible = True
        'Label2(29).Visible = True
        Label2(30).Visible = True
    Else
        TxtArea(23).Visible = False
        Label2(6).Visible = False
        TxtArea(28).Visible = False
        Label2(24).Visible = False
        Frame1(2).Visible = False
        'fadao
        Frame1(20).Visible = False
        Label2(10).Caption = "�rea segurada X valor do custeio"
        'TxtArea(35).Visible = False
        TxtArea(36).Visible = False
        'Label2(29).Visible = False
        Label2(30).Visible = False
        TxtArea(26).Visible = False
        TxtArea(27).Visible = False
        Label3(0).Visible = False
        Label3(2).Visible = False
    End If



    'Obtendo endosso
    Set oAdequacao_Area = CreateObject("SEGL0026.cls00126")

    Set oRs = oAdequacao_Area.Consulta_Dados_Adequacao_Area(gsSIGLASISTEMA, _
                                                            App.Title, _
                                                            App.FileDescription, _
                                                            glAmbiente_id, _
                                                            Proposta)





    While Not oRs.EOF

        Linha = oRs!proposta_id & vbTab & oRs!num_endosso & vbTab & oRs!Apolice_id & vbTab & oRs!sucursal_seguradora_id & vbTab & _
                oRs!seguradora_cod_susep & vbTab & oRs!Dt_Fim_Vigencia & vbTab & oRs!ramo_id & vbTab & _
                oRs!banco_id & vbTab & oRs!agencia_id & vbTab & oRs!conta_corrente_id & _
                vbTab & oRs!Num_Cartao & vbTab & oRs!Dt_Validade_Cartao & vbTab & oRs!Dt_Inicio_Vigencia & vbTab & _
                oRs!Seguro_Moeda_id & vbTab & oRs!tp_cobertura
        grdReintegracao(num_grid).AddItem Linha

        oRs.MoveNext
    Wend


    grdReintegracao(num_grid).ColWidth(0) = 1000
    grdReintegracao(num_grid).ColWidth(1) = 1050
    grdReintegracao(num_grid).ColWidth(2) = 800
    grdReintegracao(num_grid).ColWidth(3) = 2550
    grdReintegracao(num_grid).ColWidth(4) = 1400
    grdReintegracao(num_grid).ColWidth(5) = 1250
    grdReintegracao(num_grid).ColWidth(6) = 1300

    oRs.Close
    Set oAdequacao_Area = Nothing

    Exit Function

TrataErro:

    Call TratarErro("ValidarEndossoProduto", Me.name)

End Function


Private Function Carregar_Dados_Adequacao_Produtividade(Proposta As Long, num_tab As Integer, num_grid As Integer)


    On Error GoTo TrataErro

    Dim oAdequacao_Area As Object
    Dim oRs As Recordset

    Dim i As Integer
    Dim Linha As String
    Dim Situacao As String

    stbEndosso.TabVisible(num_tab) = True

    If txtProduto.Text = "1204" Then
        TxtArea(48).Visible = True
        Label2(44).Visible = True
        Label2(28).Visible = True
        Label2(14).Visible = True
        TxtArea(34).Visible = True
        TxtArea(21).Visible = True
        Label2(11).Visible = True
        TxtArea(13).Visible = True
        Label3(4).Visible = True
        TxtArea(45).Visible = True
        Label2(46).Visible = True
        TxtArea(47).Visible = True
    Else
        TxtArea(48).Visible = False
        Label2(44).Visible = False
        Label2(28).Visible = False
        Label2(14).Visible = False
        TxtArea(34).Visible = False
        TxtArea(21).Visible = False
        Label2(11).Visible = False
        TxtArea(13).Visible = False
        Label3(4).Visible = False
        TxtArea(45).Visible = False
        Label2(46).Visible = False
        TxtArea(47).Visible = False
    End If

    'Obtendo endosso
    Set oAdequacao_Area = CreateObject("SEGL0026.cls00126")

    Set oRs = oAdequacao_Area.Consulta_Dados_Adequacao_Area_Produtividade(gsSIGLASISTEMA, _
                                                                          App.Title, _
                                                                          App.FileDescription, _
                                                                          glAmbiente_id, _
                                                                          Proposta)


    While Not oRs.EOF

        Linha = oRs!proposta_id & vbTab & oRs!num_endosso & vbTab & oRs!Apolice_id & vbTab & oRs!sucursal_seguradora_id & vbTab & _
                oRs!seguradora_cod_susep & vbTab & oRs!Dt_Fim_Vigencia & vbTab & oRs!ramo_id & vbTab & _
                oRs!agencia_id & vbTab & oRs!conta_corrente_id & vbTab & _
                oRs!Seguro_Moeda_id & vbTab & oRs!tp_cobertura
        grdReintegracao(num_grid).AddItem Linha

        oRs.MoveNext
    Wend

    grdReintegracao(num_grid).ColWidth(0) = 1300
    grdReintegracao(num_grid).ColWidth(1) = 900
    grdReintegracao(num_grid).ColWidth(2) = 900
    grdReintegracao(num_grid).ColWidth(3) = 1600
    grdReintegracao(num_grid).ColWidth(4) = 1550
    grdReintegracao(num_grid).ColWidth(5) = 1500
    grdReintegracao(num_grid).ColWidth(6) = 900
    grdReintegracao(num_grid).ColWidth(7) = 900
    grdReintegracao(num_grid).ColWidth(8) = 900
    grdReintegracao(num_grid).ColWidth(9) = 1200
    grdReintegracao(num_grid).ColWidth(10) = 1200

    grdReintegracao(num_grid).ColAlignment(0) = 1
    grdReintegracao(num_grid).ColAlignment(1) = 1
    grdReintegracao(num_grid).ColAlignment(2) = 1
    grdReintegracao(num_grid).ColAlignment(3) = 1
    grdReintegracao(num_grid).ColAlignment(4) = 1
    grdReintegracao(num_grid).ColAlignment(5) = 1
    grdReintegracao(num_grid).ColAlignment(6) = 1
    grdReintegracao(num_grid).ColAlignment(7) = 1
    grdReintegracao(num_grid).ColAlignment(8) = 1
    grdReintegracao(num_grid).ColAlignment(9) = 1
    grdReintegracao(num_grid).ColAlignment(10) = 1

    oRs.Close
    Set oAdequacao_Area = Nothing

    Exit Function

TrataErro:

    Call TratarErro("ValidarEndossoProduto", Me.name)

End Function
Private Function Carregar_Dados_Restituicao_Subsidio(Proposta As Long)


    On Error GoTo TrataErro

    Dim ORestituicao_Subsidio As Object
    Dim oRs As Recordset

    Dim i As Integer
    Dim Linha As String
    Dim Situacao As String

    '* Usadas para saber quais s�o as parcelas do subsidio MAPA e FEDERAL
    Dim cont_Linhas As Integer
    Dim cont_Total As Integer
    Dim Responsavel As String
    Dim alterna_endosso As Integer

    '********************************************************************

    stbEndosso.TabVisible(17) = True

    '    If txtProduto.Text = "1204" Then
    '        TxtArea(4).Visible = True
    '        Label2(3).Visible = True
    '        Label2(4).Caption = "IS = �rea p�s  x Custeio x Fator Replant."
    '    '1152
    '    Else
    '        TxtArea(4).Visible = False
    '        Label2(3).Visible = False
    '        Label2(4).Caption = "IS = IS - Valor Indenizado"
    '    End If

    'Obtendo endosso
    Set ORestituicao_Subsidio = CreateObject("SEGL0026.cls00126")

    Set oRs = ORestituicao_Subsidio.Consulta_Dados_Restituicao_Subsidio(gsSIGLASISTEMA, _
                                                                        App.Title, _
                                                                        App.FileDescription, _
                                                                        glAmbiente_id, _
                                                                        Proposta)



    cont_Linhas = 0
    cont_Total = 0
    'Come�ando com um endosso absurdo para n�o atrapalhar na compara��o, porque existe endosso 0, que � a proposta (RELY)
    ' alterna_endosso = 0

    While Not oRs.EOF

        cont_Linhas = cont_Linhas + 1
        cont_Total = cont_Total + 1

        If cont_Total = 1 Then
            alterna_endosso = oRs!endosso_id
        End If

        If cont_Linhas = 1 Or cont_Linhas = 2 Or cont_Linhas = 5 Then
            Responsavel = "Segurado"
        ElseIf cont_Linhas = 3 Then
            Responsavel = "MAPA"
        ElseIf cont_Linhas = 4 Then
            Responsavel = "Estadual"
        End If

        Linha = oRs!Proposta & vbTab & oRs!endosso_id & vbTab & oRs!Parcela & vbTab & Responsavel & vbTab & _
                oRs!valor_parcela & vbTab & oRs!valor_pago & vbTab & Format(oRs!data_pagamento, "dd/mm/yyyy") & vbTab & oRs!ramo_id
        grdReintegracao(3).AddItem Linha

        'Flag para identificar mudan�a de endosso
        If alterna_endosso <> oRs!endosso_id Then
            alterna_endosso = oRs!endosso_id
            'cont_Linhas = 0
        End If

        grdReintegracao(3).Row = cont_Total

        For i = 0 To grdReintegracao(3).Cols - 1
            If cont_Linhas = 3 Or cont_Linhas = 4 Then
                grdReintegracao(3).Col = i
                grdReintegracao(3).CellBackColor = &HC0FFFF
                grdReintegracao(3).CellAlignment = 1
                '                 Else
                '                     grdReintegracao(3).Col = i
                '                      grdReintegracao(3).CellAlignment = 1
            End If
        Next

        If cont_Linhas = 5 Then
            cont_Linhas = 0
        End If

        oRs.MoveNext
    Wend

    grdReintegracao(3).ColWidth(0) = 1300
    grdReintegracao(3).ColWidth(1) = 1300
    grdReintegracao(3).ColWidth(2) = 1050
    grdReintegracao(3).ColWidth(3) = 1200
    grdReintegracao(3).ColWidth(4) = 1700
    grdReintegracao(3).ColWidth(5) = 1700
    grdReintegracao(3).ColWidth(6) = 2200
    grdReintegracao(3).ColWidth(7) = 0

    grdReintegracao(3).ColAlignment(0) = 1
    grdReintegracao(3).ColAlignment(1) = 1
    grdReintegracao(3).ColAlignment(2) = 1
    grdReintegracao(3).ColAlignment(3) = 1
    grdReintegracao(3).ColAlignment(4) = 1
    grdReintegracao(3).ColAlignment(5) = 1
    grdReintegracao(3).ColAlignment(6) = 1

    oRs.Close
    Set ORestituicao_Subsidio = Nothing

    Exit Function

TrataErro:

    Call TratarErro("ValidarEndossoProduto", Me.name)

End Function
'JOAO.MACHADO
Private Sub RetornarPrecoBase(ByVal lPropostaId As Long)

    Dim rc As Recordset
    Dim sSQL As String
    On Error GoTo TrataErro

    sSQL = ""
    sSQL = sSQL & " SELECT convert(numeric(10,4),LTRIM(RTRIM(REPLACE(ISNULL(TEXTO_RESPOSTA,0),',','.')))) as preco_base_anterior " & vbNewLine
    sSQL = sSQL & "   FROM questionario_objeto_tb  WITH (NOLOCK)  " & vbNewLine
    sSQL = sSQL & "  WHERE Proposta_Id = " & lPropostaId & vbNewLine
    sSQL = sSQL & "    AND cod_objeto_segurado =  1 AND pergunta_id = 7740 " & vbNewLine
    sSQL = sSQL & "    AND endosso_id = (select max(endosso_id) endosso_id from questionario_objeto_tb  WITH (NOLOCK)   where proposta_id = " & lPropostaId & " ) "


    If rdocn Is Nothing Then
        Call Conexao
    Else
        If rdocn.State = 0 Then Call Conexao
    End If

    Set rc = rdocn.Execute(sSQL)

    If Not rc.EOF Then

        PrecoBaseAnterior = rc("preco_base_anterior")

    End If

    rc.Close

    'Rodrigo Robalo - FlowBr n.� 18300347 - Ajuste Processo Endosso Faturamento - 06.05.2015
    '16/07/2018 - Schoralick (ntendencia) - 00421597-seguro-faturamento-pecuario [add pre�o base para produto 1240] - inicio
    SQL = ""
    SQL = SQL & " SELECT preco_base.preco_base_valor" & vbNewLine
    SQL = SQL & " FROM seguros_db.dbo.preco_base_tb AS preco_base WITH (NOLOCK)" & vbNewLine
    SQL = SQL & " INNER JOIN seguros_db.dbo.seguro_esp_agricola_tb AS agricola WITH (NOLOCK)" & vbNewLine
    SQL = SQL & "     ON preco_base.tp_cultura_id = agricola.tp_cultura_id" & vbNewLine
    SQL = SQL & " INNER JOIN seguros_db.dbo.proposta_tb AS proposta_tb WITH (NOLOCK)" & vbNewLine
    SQL = SQL & "     ON proposta_tb.proposta_id = " & lPropostaId & " " & vbNewLine
    SQL = SQL & " WHERE agricola.proposta_id = " & lPropostaId & " " & vbNewLine
    SQL = SQL & "     AND preco_base.dt_inicio_vigencia <= GETDATE()" & vbNewLine
    SQL = SQL & "     AND (" & vbNewLine
    SQL = SQL & "         preco_base.dt_fim_vigencia IS NULL" & vbNewLine
    SQL = SQL & "         OR preco_base.dt_fim_vigencia >= GETDATE()" & vbNewLine
    SQL = SQL & "         )" & vbNewLine
    SQL = SQL & "     AND proposta_tb.produto_id <> 1240" & vbNewLine
    SQL = SQL & "" & vbNewLine
    SQL = SQL & " UNION" & vbNewLine
    SQL = SQL & "" & vbNewLine
    SQL = SQL & " SELECT preco_base.preco_base_valor" & vbNewLine
    SQL = SQL & " FROM seguros_db.dbo.preco_base_tb AS preco_base WITH (NOLOCK)" & vbNewLine
    SQL = SQL & " INNER JOIN seguros_db.dbo.seguro_esp_pecuario_tb AS pecuario WITH (NOLOCK)" & vbNewLine
    SQL = SQL & "     ON preco_base.tp_cultura_id = pecuario.tp_cultura_id" & vbNewLine
    SQL = SQL & " INNER JOIN seguros_db.dbo.proposta_tb AS proposta_tb WITH (NOLOCK)" & vbNewLine
    SQL = SQL & "     ON proposta_tb.proposta_id = " & lPropostaId & " " & vbNewLine
    SQL = SQL & " WHERE pecuario.proposta_id = " & lPropostaId & " " & vbNewLine
    SQL = SQL & "     AND preco_base.dt_inicio_vigencia <= GETDATE()" & vbNewLine
    SQL = SQL & "     AND (" & vbNewLine
    SQL = SQL & "         preco_base.dt_fim_vigencia IS NULL" & vbNewLine
    SQL = SQL & "         OR preco_base.dt_fim_vigencia >= GETDATE()" & vbNewLine
    SQL = SQL & "         )" & vbNewLine
    SQL = SQL & "     AND proposta_tb.produto_id = 1240"
    '16/07/2018 - Schoralick (ntendencia) - 00421597-seguro-faturamento-pecuario [add pre�o base para produto 1240] - fim

    Set rc = rdocn.Execute(SQL)

    If Not rc.EOF Then
        If bConfiguracaoBrasil Then
            PrecoBaseNovo = Format(CCur(rc("preco_base_valor")), "###,###,###,##0.0000")
        Else
            PrecoBaseNovo = TrocaValorAmePorBras(Format(CCur(rc("preco_base_valor")), "###,###,###,##0.0000"))
        End If
    End If

    rc.Close

    txtDescEndosso.Text = "Pre�o Base Anterior: " & PrecoBaseAnterior & " / Novo Pre�o Base: " & PrecoBaseNovo


    Exit Sub

TrataErro:

    Call TratarErro("RetornarPrecoBase", Me.name)

End Sub

'Jessica.Adao (Inicio) - Confitec Sistemas - 20140401 - PROJETO 17919477 : BB SEGURO CREDITO PROTEGIDO PJ
Private Function reativaProposta(ByVal lPropostaId As Long) As Boolean

'On Error GoTo TrataErro

    Dim sRs As Recordset
    Dim sQry As String

    'If Not lProduto = 1225 Then
    '    reativaProposta = True
    '    Exit Function
    'Else
    '    reativaProposta = False
    'End If

    reativaProposta = True

    sQry = ""
    sQry = sQry & "SELECT qtd_dias = ISNULL(DATEDIFF(DAY, dt_inicio_cancelamento, GETDATE()), 0)" & vbNewLine
    sQry = sQry & "  FROM seguros_db.dbo.cancelamento_proposta_tb" & vbNewLine
    sQry = sQry & " WHERE proposta_id = " & lPropostaId & vbNewLine
    sQry = sQry & "   AND dt_fim_cancelamento IS NULL" & vbNewLine

    If rdocn Is Nothing Then
        Call Conexao
    Else
        If rdocn.State = 0 Then Call Conexao
    End If

    Set sRs = rdocn.Execute(sQry)

    If Not sRs.EOF Then
        If Not sRs("qtd_dias") <= 90 Then reativaProposta = False
    End If

    sRs.Close
    Set sRs = Nothing

    'TratarErro:
    'Call TratarErro("reativaProposta", Me.name)
End Function
'Jessica.Adao (Fim) - Confitec Sistemas - 20140401 - PROJETO 17919477 : BB SEGURO CREDITO PROTEGIDO PJ

'Jessica.Adao (Inicio) - Confitec Sistemas - 20150715 - Projeto 18319298 - Fase III
Private Sub calculaRestituicaoPJ()

    Dim pLiqDia As Currency
    Dim diaPerman As Integer
    Dim diaNaoDec As Integer

    Dim oRestituicao As Object
    Dim oRsRestituicao As Currency
    Dim cPremioTarifa As Currency
    Dim cIOF As Currency

    On Error GoTo TrataErro

    Set oRestituicao = CreateObject("SEGL0026.cls00126")

    oRsRestituicao = oRestituicao.calculaRestituicaoPJ(gsSIGLASISTEMA, _
                                                       App.Title, _
                                                       App.FileDescription, _
                                                       glAmbiente_id, _
                                                       Trim(txtPropostaid(0).Text), cPremioTarifa, cIOF)

    Set oRestituicao = Nothing

    mskValPremioTarifaResCancelamento(0).Text = cPremioTarifa
    mskValPremioTarifaResCancelamento(4).Text = cIOF
    lblValRestituicaoResCancelamento(0).Caption = oRsRestituicao

    mskValPremioTarifaRes.Text = cPremioTarifa
    mskValIOFRes.Text = cIOF
    lblValRestituicaoRes.Caption = oRsRestituicao

    Call CalcularValoresRestituicao

    Exit Sub

TrataErro:
    Call TratarErro("calculaRestituicaoPJ", Me.name)
    FinalizarAplicacao
End Sub
'Jessica.Adao (Fim) - Confitec Sistemas - 20150715 - Projeto 18319298 - Fase III

'10/12/2015 - Schoralick (nova consultoria) - 18690344 - Melhorias Processo Reagendamento e Reativa��o BESC
Private Sub reagendamentoAutomatico(ByVal opcao As Integer, data As String)
    Dim i As Integer
    Dim Soma As Integer
    Dim sLinha As String
    Dim lMsg As String
    Dim sData As String

    Soma = 0

    If opcao > 0 Then
        sLinha = ""
        For i = 1 To grdParcelasReativacao.Rows - 1
            If grdParcelasReativacao.TextMatrix(i, 3) = "c" Then
                sLinha = sLinha & "(" & txtPropostaid(0).Text & ","
                sLinha = sLinha & lUltimaParcelaReativacao & ","    'num_parcela
                sLinha = sLinha & "'a'" & ","    ' situacao
                sLinha = sLinha & "'000000'" & ","  'mes_ano_ref
                If opcao = 1 Then
                    sData = Format(CDate(data), "dd/mm/yyyy")
                End If
                If opcao = 2 Then
                    sData = Format((CDate(data) + Soma), "dd/mm/yyyy")
                    Soma = Soma + 20
                End If
                sData = Mid$(sData, 7, 4) & "-" & Mid$(sData, 4, 2) & "-" & Mid$(sData, 1, 2)
                sLinha = sLinha & "'" & sData & "',"    'data_parcela
                sLinha = sLinha & "'" & Replace(Replace(grdParcelasReativacao.TextMatrix(i, 1), ".", ""), ",", ".") & "',"    'valor
                sLinha = sLinha & i & ")|"
            End If
        Next i

        lMsg = ValidaParcela(sLinha)
        If lMsg <> "" Then
            MsgBox (lMsg)
            Exit Sub
        End If

        Soma = 0
        For i = 1 To grdParcelasReativacao.Rows - 1
            If grdParcelasReativacao.TextMatrix(i, 3) = "c" Then
                If opcao = 1 Then
                    grdParcelasReativacao.TextMatrix(i, 2) = Format(CDate(data), "dd/mm/yyyy")
                End If

                If opcao = 2 Then
                    sData = Format((CDate(data) + Soma), "dd/mm/yyyy")
                    grdParcelasReativacao.TextMatrix(i, 2) = sData
                    Soma = Soma + 20
                End If
            End If
        Next i
    End If
End Sub

Private Sub inclusaoAutomatica(ByVal opcao As Integer, data As String)
    Dim i As Integer
    Dim j As Integer
    Dim Soma As Integer
    Dim sLinha As String
    Dim lMsg As String
    Dim sData As String
    Dim iUltimaParcelaCancelada As Integer
    Dim iUltimaParcelaTemp As Integer
    Dim oEndossoReativacao As Object
    Dim sUltimoAnoMesRef As String
    Dim sUltimoValor As String
    Dim iAno_i As Integer
    Dim iAno_f As Integer
    Dim iMes_i As Integer
    Dim iMes_f As Integer
    Dim lAnoMes_i As Long
    Dim lAnoMes_f As Long
    Dim sData1 As String
    Dim sData2 As String
    Dim sAnoMesRef As String
    Dim DataTemp As String
    Dim bJaCadastradado As Boolean

    Set oEndossoReativacao = CreateObject("SEGL0140.cls00172")

    sUltimoAnoMesRef = oEndossoReativacao.ObterUltimaAnoMesRefParcelaCancelada(gsSIGLASISTEMA, _
                                                                               glAmbiente_id, _
                                                                               App.Title, _
                                                                               App.FileDescription, _
                                                                               oDadosEndosso.PropostaId)

    sUltimoValor = oEndossoReativacao.ObterUltimoValCobrancaParcelaCancelada(gsSIGLASISTEMA, _
                                                                             glAmbiente_id, _
                                                                             App.Title, _
                                                                             App.FileDescription, _
                                                                             oDadosEndosso.PropostaId)

    'verificando impedimentos para incluir as propostas
    sLinha = ""
    iUltimaParcelaTemp = iUltimaParcelaCancelada
    Soma = 0
    DataTemp = data
    iAno_i = Mid$(sUltimoAnoMesRef, 1, 4)
    iMes_i = Mid$(sUltimoAnoMesRef, 5, 2) + 1
    iAno_f = Mid$(Format(Data_Sistema, "yyyymmdd"), 1, 4)
    iMes_f = Mid$(Format(Data_Sistema, "yyyymmdd"), 5, 2)
    j = 1
    lAnoMes_i = (Format(iAno_i, "0000") & Format(iMes_i, "00"))
    lAnoMes_f = (Format(iAno_f, "0000") & Format(iMes_f, "00"))
    'While ((iAno_i <= iAno_f) And (iMes_i <= iMes_f))
    While (lAnoMes_i <= lAnoMes_f)
        sData1 = Mid$(Format(CDate(DataTemp), "yyyymmdd"), 1, 6)
        bJaCadastradado = False
        If ((grdParcelasReativacao.Rows - 1) > 0) Then
            For i = 1 To grdParcelasReativacao.Rows - 1
                sData2 = grdParcelasReativacao.TextMatrix(i, 4)
                If sData1 = sData2 Then
                    bJaCadastradado = True
                End If
            Next i
        End If

        If Not bJaCadastradado Then
            sLinha = sLinha & "(" & txtPropostaid(0).Text & ","
            sLinha = sLinha & iUltimaParcelaTemp & ","    'num_parcela
            sLinha = sLinha & "'a'" & ","    ' situacao
            sAnoMesRef = Format(iAno_i, "0000") & Format(iMes_i, "00")
            sLinha = sLinha & sAnoMesRef & ","  'mes_ano_ref
            If opcao = 1 Then
                DataTemp = Format(CDate(data), "dd/mm/yyyy") & vbTab
            End If
            If opcao = 2 Then
                DataTemp = Format((CDate(data) + Soma), "dd/mm/yyyy")
                Soma = Soma + 20
            End If
            sData = Mid$(DataTemp, 7, 4) & "-" & Mid$(DataTemp, 4, 2) & "-" & Mid$(DataTemp, 1, 2)
            sLinha = sLinha & "'" & sData & "',"    'data_parcela
            sLinha = sLinha & Replace(Replace(sUltimoValor, ".", ""), ",", ".") & ","    'valor
            sLinha = sLinha & j & ")|"

            iUltimaParcelaTemp = iUltimaParcelaTemp + 1
            j = j + 1
        End If

        iMes_i = iMes_i + 1
        If iMes_i > 12 Then
            iMes_i = 1
            iAno_i = iAno_i + 1
        End If

        lAnoMes_i = (Format(iAno_i, "0000") & Format(iMes_i, "00"))
        lAnoMes_f = (Format(iAno_f, "0000") & Format(iMes_f, "00"))
    Wend

    lMsg = ValidaParcelaInc(sLinha)
    If lMsg <> "" Then
        MsgBox (lMsg)
        Exit Sub
    End If

    'realizando a inclus�o das propostas
    Soma = 0
    iAno_i = Mid$(sUltimoAnoMesRef, 1, 4)
    iMes_i = Mid$(sUltimoAnoMesRef, 5, 2) + 1
    iAno_f = Mid$(Format(Data_Sistema, "yyyymmdd"), 1, 4)
    iMes_f = Mid$(Format(Data_Sistema, "yyyymmdd"), 5, 2)
    lAnoMes_i = (Format(iAno_i, "0000") & Format(iMes_i, "00"))
    lAnoMes_f = (Format(iAno_f, "0000") & Format(iMes_f, "00"))
    'While ((iAno_i <= iAno_f) And (iMes_i <= iMes_f))
    While (lAnoMes_i <= lAnoMes_f)
        sData1 = Mid$(Format(CDate(data), "yyyymmdd"), 1, 6)
        bJaCadastradado = False
        If ((grdParcelasReativacao.Rows - 1) > 0) Then
            For i = 1 To grdParcelasReativacao.Rows - 1
                sData2 = grdParcelasReativacao.TextMatrix(grdParcelasReativacao.Row, 4)
                If sData1 = sData2 Then
                    bJaCadastradado = True
                End If
            Next i
        End If

        If Not bJaCadastradado Then
            sLinha = ""
            sLinha = sLinha & lUltimaParcelaReativacao & vbTab    ' ultima parcela
            sLinha = sLinha & Format(sUltimoValor, "0.00") & vbTab    ' valor

            If opcao = 1 Then
                sLinha = sLinha & Format(CDate(data), "dd/mm/yyyy") & vbTab    ' data parcela
            End If

            If opcao = 2 Then
                sData = Format((CDate(data) + Soma), "dd/mm/yyyy")
                sLinha = sLinha & sData & vbTab    ' data parcela
                Soma = Soma + 20
            End If

            sLinha = sLinha & "a" & vbTab    ' situacao
            sAnoMesRef = Format(iAno_i, "0000") & Format(iMes_i, "00")
            sLinha = sLinha & sAnoMesRef    'mes_ano_ref

            Call grdParcelasReativacao.AddItem(sLinha)

            lUltimaParcelaReativacao = lUltimaParcelaReativacao + 1
        End If

        iMes_i = iMes_i + 1
        If iMes_i > 12 Then
            iMes_i = 1
            iAno_i = iAno_i + 1
        End If
        lAnoMes_i = (Format(iAno_i, "0000") & Format(iMes_i, "00"))
        lAnoMes_f = (Format(iAno_f, "0000") & Format(iMes_f, "00"))
    Wend
End Sub

Private Function ValidaParcela(ByVal linhas As String) As String
    Dim lMsg As String
    lMsg = ""

    If ParcelasValida(EnumValidInadimplenciaSAP, linhas) = False Then
        lMsg = "N�o � poss�vel a reativa��o da propostas." + vbNewLine + "Motivo: uma ou mais parcelas n�o possuem retorno ou possuem retorno positivo da mesma."
    ElseIf ParcelasValida(EnumValidDataReagendamento, linhas) = False Then
        lMsg = "N�o � poss�vel a reativa��o da propostas." + vbNewLine + "Motivo: uma ou mais parcelas est�o com a data de pagamento menor que a data atual + 15 dias."
    ElseIf ParcelasValida(EnumValidVigencia, linhas) = False Then
        lMsg = "N�o � poss�vel a reativa��o da propostas." + vbNewLine + "Motivo: uma ou mais parcelas n�o est�o definidas dentro da vigencia da proposta."
    ElseIf ParcelasValida(EnumValidVlParcela, linhas) = False Then
        lMsg = "N�o � poss�vel a reativa��o da propostas." + vbNewLine + "Motivo: uma ou mais parcelas n�o possuem seu valor da parcela maior ou igual a 1."
    End If

    ValidaParcela = lMsg
End Function

Private Function ValidaParcelaInc(ByVal linhas As String) As String
    Dim lMsg As String
    lMsg = ""

    If ParcelasValida(EnumValidDataReagendamento, linhas) = False Then
        lMsg = "N�o � poss�vel a reativa��o da propostas." + vbNewLine + "Motivo: uma ou mais parcelas est�o com a data de pagamento menor que a data atual + 15 dias."
    ElseIf ParcelasValida(EnumValidVigencia, linhas) = False Then
        lMsg = "N�o � poss�vel a reativa��o da propostas." + vbNewLine + "Motivo: uma ou mais parcelas n�o est�o definidas dentro da vigencia da proposta."
    ElseIf ParcelasValida(EnumValidVlParcela, linhas) = False Then
        lMsg = "N�o � poss�vel a reativa��o da propostas." + vbNewLine + "Motivo: uma ou mais parcelas n�o possuem seu valor da parcela maior ou igual a 1."
    End If

    ValidaParcelaInc = lMsg
End Function



Private Function ParcelasValida(ByVal lEnum As EnumValidaParcela, ByVal linhas As String) As Boolean
    Dim lRetorno As Boolean
    Dim oEndossoReativacao As Object
    Dim reativacaoValidaParcela As Recordset

    Set oEndossoReativacao = CreateObject("SEGL0140.cls00172")

    Set reativacaoValidaParcela = oEndossoReativacao.reativacaoValidaParcela(gsSIGLASISTEMA, _
                                                                             glAmbiente_id, _
                                                                             App.Title, _
                                                                             App.FileDescription, _
                                                                             linhas, _
                                                                             lEnum)



    If reativacaoValidaParcela!Resultado = 1 Then
        lRetorno = False
    Else
        lRetorno = True
    End If

    Set oEndossoReativacao = Nothing
    ParcelasValida = lRetorno
End Function

Private Function converteGrdParcelasEmLinha(ByVal i As Integer, ByVal lEnum As EnumOpLinhaRetorno) As String
    Dim l As Integer
    Dim linhas As String
    linhas = ""

    If lEnum = EnumOpUmaLinha Then
        linhas = RetornaUmaLinhaGrdParcela(i)
    End If

    If lEnum = EnumOpTodasLinhas Then
        For l = i To grdParcelasReativacao.Rows - 1
            linhas = linhas & RetornaUmaLinhaGrdParcela(l)
        Next l
    End If

    converteGrdParcelasEmLinha = linhas
End Function

Private Function RetornaUmaLinhaGrdParcela(ByVal i As Integer) As String
    Dim Linha As String
    Dim sData As String
    Dim mes_ref As String

    Linha = ""
    Linha = Linha & "(" & txtPropostaid(0).Text & ","
    Linha = Linha & grdParcelasReativacao.TextMatrix(i, 0) & ","    'num_parcela
    Linha = Linha & "'" & grdParcelasReativacao.TextMatrix(i, 3) & "'" & ","    ' situacao
    Linha = Linha & "'000000'" & ","  'mes_ano_ref
    sData = Mid$(grdParcelasReativacao.TextMatrix(i, 2), 7, 4) & "-" & Mid$(grdParcelasReativacao.TextMatrix(i, 2), 4, 2) & "-" & Mid$(grdParcelasReativacao.TextMatrix(i, 2), 1, 2)
    Linha = Linha & "'" & sData & "',"    'data_parcela
    Linha = Linha & "'" & Replace(Replace(grdParcelasReativacao.TextMatrix(i, 1), ".", ""), ",", ".") & "',"    'valor
    Linha = Linha & i & ")|"

    RetornaUmaLinhaGrdParcela = Linha
End Function

Private Function montaLinha(ByVal lsParcela, ByVal lsData As String, ByVal lsValor As String) As String
    Dim sLinha As String

    sLinha = ""
    sLinha = sLinha & "(" & txtPropostaid(0).Text & ","
    sLinha = sLinha & lsParcela & ","    'num_parcela
    sLinha = sLinha & "'a'" & ","    ' situacao
    sLinha = sLinha & "'000000'" & ","  'mes_ano_ref
    lsData = Mid$(lsData, 7, 4) & "-" & Mid$(lsData, 4, 2) & "-" & Mid$(lsData, 1, 2)
    sLinha = sLinha & "'" & lsData & "',"    'data_parcela
    sLinha = sLinha & "'" & Replace(Replace(lsValor, ".", ""), ",", ".") & "',"    'valor
    sLinha = sLinha & 0 & ")|"
    montaLinha = sLinha
End Function
'fim - 18690344 - Melhorias Processo Reagendamento e Reativa��o BESC

'Jessica.Adao - Confitec Sistemas - 20170602 - Projeto MU-2017-014324 : Endosso de Restituicao
Private Function bloqueiaVigenciaRestituicao(ByVal iProduto As Integer) As Boolean

    bloqueiaVigenciaRestituicao = False

    Select Case iProduto
    Case 100, 104, 106, 109, 111, 112, 113, 116, 119, 777, 809, 810, 811, 1021, 1123, 1176, 1178, 1188, 1207, 1220, 1221, 1222, 1223, 1224, 1228, 1229
        bloqueiaVigenciaRestituicao = True
    End Select

End Function


'fernanda.souza - Confitec - Novo Produto Outros Canais
Public Function VerificaOutrosCanais(ByVal sProdutoId As String) As String

    Dim rc As Recordset
    Dim sSQL As String

    On Error GoTo TrataErro

    'Obtendo campo outros canais ''''''''''''''''''''''

    sSQL = "" _
         & "SELECT " & vbNewLine _
         & "    UPPER(ISNULL(outros_canais, 'N')) as outros_canais " & vbNewLine _
         & "FROM seguros_db.dbo.produto_tb WITH(NOLOCK) " & vbNewLine _
         & "WHERE " & vbNewLine _
         & "    produto_id = '" & sProdutoId & "' "

    If rdocn Is Nothing Then
        Call Conexao
    Else
        If rdocn.State = 0 Then Call Conexao
    End If

    Set rc = rdocn.Execute(sSQL)

    VerificaOutrosCanais = rc!outros_canais

    rdocn.Close
    Set rdocn = Nothing
    Set rdocn_Seg = Nothing
    Set rdocn_Seg2 = Nothing


    Exit Function


TrataErro:
    Call TrataErroGeral("VerificaOutrosCanais", Me.name)
    Call FinalizarAplicacao

End Function

Public Function ValidaBESC(ByVal sProdutoId As String) As Boolean

    On Error GoTo TrataErro

    Dim produtoBESC
    produtoBESC = Array("200", "201", "202", "203", "204", "205", "208", "209", "210", "211", "213", "216", "217", "218", "219", "221", "222", "223", "224", "225", "226")

    ValidaBESC = (UBound(Filter(produtoBESC, sProdutoId)) > -1)

    Exit Function

TrataErro:
    Call TrataErroGeral("ValidaBESC", Me.name)
    Call FinalizarAplicacao

End Function

Public Sub RetornoBBOutrosCanais(ByVal sPropostaId As String, ByVal iEndossoId As Integer)

    Dim rc As Recordset
    Dim sSQL As String

    On Error GoTo TrataErro

    'Obtendo campo outros canais ''''''''''''''''''''''

    sSQL = "" _
         & "EXEC retorno_bb_endosso_spu " & vbNewLine _
         & "    @proposta_id = '" & sPropostaId & "' " & vbNewLine _
         & ",   @endosso_id = '" & iEndossoId & "' " & vbNewLine _
         & ",   @dt_retorno = '" & Format(Data_Sistema, "yyyyMMdd") & "' " & vbNewLine _
         & ",   @situacao = 'S'" & vbNewLine _
         & ",   @usuario = '" & cUserName & "'"

    If rdocn Is Nothing Then
        Call Conexao
    Else
        If rdocn.State = 0 Then Call Conexao
    End If

    Set rc = rdocn.Execute(sSQL)

    'VerificaOutrosCanais = rc!outros_canais

    rdocn.Close
    Set rdocn = Nothing

    Exit Sub


TrataErro:
    Call TrataErroGeral("RetornoBBOutrosCanais", Me.name)
    Call FinalizarAplicacao

End Sub
