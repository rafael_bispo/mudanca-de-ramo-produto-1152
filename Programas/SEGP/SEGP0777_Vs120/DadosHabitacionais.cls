VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "DadosHabitacionais"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Private mvarTpCoberturaId As String

Public Property Let TpCoberturaId(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.TpCoberturaId = 5
    mvarTpCoberturaId = vData
End Property

Public Property Get TpCoberturaId() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.TpCoberturaId
    TpCoberturaId = mvarTpCoberturaId
End Property

