VERSION 5.00
Object = "{C932BA88-4374-101B-A56C-00AA003668DC}#1.1#0"; "MSMASK32.ocx"
Begin VB.Form frmReagendamentoEmMassa 
   Caption         =   "Reagendamento Autom�tico"
   ClientHeight    =   5055
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   7515
   ScaleHeight     =   5055
   ScaleWidth      =   7515
   StartUpPosition =   3  'Windows Default
   Begin VB.Frame fraPrincipal 
      Height          =   4515
      Left            =   315
      TabIndex        =   0
      Top             =   225
      Width           =   6900
      Begin VB.CommandButton cmdVoltar 
         Caption         =   "Voltar"
         Height          =   555
         Left            =   3690
         TabIndex        =   6
         Top             =   3600
         Width           =   2175
      End
      Begin VB.CommandButton cmdEfetivar 
         Caption         =   "Efetivar"
         Height          =   555
         Left            =   1170
         TabIndex        =   5
         Top             =   3600
         Width           =   2175
      End
      Begin VB.OptionButton optFracionado 
         Caption         =   "Autom�tico Fracionado Vencimento 20 dias"
         Height          =   285
         Left            =   1035
         TabIndex        =   3
         Top             =   1800
         Width           =   3795
      End
      Begin VB.OptionButton optVista 
         Caption         =   "Autom�tico � Vista"
         Height          =   330
         Left            =   1035
         TabIndex        =   2
         Top             =   1395
         Width           =   2535
      End
      Begin MSMask.MaskEdBox mskDtParcela 
         Height          =   300
         Left            =   3510
         TabIndex        =   7
         Top             =   2835
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   529
         _Version        =   393216
         MaxLength       =   10
         Mask            =   "##/##/####"
         PromptChar      =   "_"
      End
      Begin VB.Label lblDtParcela 
         Caption         =   "Data Parcela (� vista ou fracionado):"
         Height          =   240
         Left            =   765
         TabIndex        =   4
         Top             =   2880
         Width           =   2670
      End
      Begin VB.Label lblQuestao 
         Alignment       =   2  'Center
         Caption         =   "Como deseja realizar o reagendamento das parcelas pendentes?"
         Height          =   690
         Left            =   1305
         TabIndex        =   1
         Top             =   405
         Width           =   4515
      End
   End
End
Attribute VB_Name = "frmReagendamentoEmMassa"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub cmdEfetivar_Click()
    If ((optVista Or optFracionado) And Trim(mskDtParcela.Text) <> Empty) Then
        If VerificaData2(mskDtParcela.Text) Then
            If (CDate(mskDtParcela.Text) > (CDate(Data_Sistema) + 15)) Then
                frmReativacaoReagendamento.vlDataReagendamento = Format(CDate(mskDtParcela.Text), "dd/mm/yyyy")
                frmEndosso.vlDataReativacao = Format(CDate(mskDtParcela.Text), "dd/mm/yyyy")

                If optVista.Value Then
                    frmReativacaoReagendamento.vlRetornoReagendamento = 1
                    frmEndosso.vlRetornoReativacao = 1
                End If
                If optFracionado Then
                    frmReativacaoReagendamento.vlRetornoReagendamento = 2
                    frmEndosso.vlRetornoReativacao = 2
                End If

                Unload Me
            Else
                MsgBox ("N�o � permitido data de parcela menor que a data atual + 15 dias. Ou seja, a data informada deve ser superior �: " & Format((CDate(Data_Sistema) + 15), "dd/mm/yyyy"))
                mskDtParcela.SetFocus
            End If
        Else
            MsgBox ("Data Ivalida!")
            mskDtParcela.SetFocus
        End If
    Else
        MsgBox ("� necess�rio que uma das op��es esteja selecionada, e que a data da parcela esteja preenchida.")
        optVista.SetFocus
    End If
End Sub

Private Sub cmdVoltar_Click()
    frmReativacaoReagendamento.vlRetornoReagendamento = 0
    frmEndosso.vlRetornoReativacao = 0
    Unload Me
End Sub

Private Sub Form_Load()
    optVista.Value = False
    optFracionado.Value = False
End Sub
