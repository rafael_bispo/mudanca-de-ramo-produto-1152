VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Cobertura"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
'local variable(s) to hold property value(s)
Private mvarCodObjSegurado As String
Private mvarTpCoberturaId As String
Private mvarTaxa As String
Private mvarPercDesconto As String
Private mvarPremio As String
Private mvarValIS As String
Private mvarDescricao As String
Private mvarFranquia As String
Private mvarFatFranquia As String
Private mvarDescFranquia As String
Private mvarDtInicioVigencia As String
Private mvarDtFimVigencia As String
Private mvarOperacao As String
Private mvarPersistente As Boolean
Private mvarTpCobranca As String
Private mvarLimMin As String
Private mvarLimMax As String
Private mvarPercMin As String
Private mvarPercMax As String
Private mvarClasCobertura As String
Private mvarAcumula_IS As String
Private mvarAcumula_Resseguro As String
'local variable(s) to hold property value(s)
Private mvarSeqCanc As Long    'local copy
'local variable(s) to hold property value(s)
Private mvarDtInicioVigenciaSeg As String    'local copy
Public Function ChaveCob() As String
    ChaveCob = CodObjSegurado & " " & TpCoberturaId & " " & DtInicioVigencia _
             & " " & DtInicioVigenciaSeg & " " & SeqCancSeg
End Function

Public Property Let DtInicioVigenciaSeg(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.DtInicioVigenciaSeg = 5
    mvarDtInicioVigenciaSeg = vData
End Property


Public Property Get DtInicioVigenciaSeg() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.DtInicioVigenciaSeg
    DtInicioVigenciaSeg = mvarDtInicioVigenciaSeg
End Property

Public Property Let SeqCancSeg(ByVal vData As Long)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.SeqCanc = 5
    mvarSeqCanc = vData
End Property


Public Property Get SeqCancSeg() As Long
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.SeqCanc
    SeqCancSeg = mvarSeqCanc
End Property



Public Property Let DtFimVigencia(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.DtFimVigencia = 5
    mvarDtFimVigencia = vData
End Property


Public Property Get DtFimVigencia() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.DtFimVigencia
    DtFimVigencia = mvarDtFimVigencia
End Property


Public Property Let DtInicioVigencia(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.DtInicioVigencia = 5
    mvarDtInicioVigencia = vData
End Property

Public Property Get DtInicioVigencia() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.DtInicioVigencia
    DtInicioVigencia = mvarDtInicioVigencia
End Property


Public Property Let DescFranquia(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.DescFranquia = 5
    mvarDescFranquia = vData
End Property

Public Property Get DescFranquia() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.DescFranquia
    DescFranquia = mvarDescFranquia
End Property


Public Property Let Descricao(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Descricao = 5
    mvarDescricao = vData
End Property

Public Property Get Descricao() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Descricao
    Descricao = mvarDescricao
End Property


Public Property Get ClasCobertura() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Descricao
    ClasCobertura = mvarClasCobertura
End Property


Public Property Let ValIS(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.ValIS = 5
    mvarValIS = vData
End Property

Public Property Get ValIS() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.ValIS
    ValIS = mvarValIS
End Property


Public Property Let ValFranquia(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Franquia = 5
    mvarFranquia = vData
End Property

Public Property Let LimMin(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Franquia = 5
    mvarLimMin = vData
End Property

Public Property Let PercMax(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Franquia = 5
    mvarPercMax = vData
End Property


Public Property Let PercMin(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Franquia = 5
    mvarPercMin = vData
End Property


Public Property Let LimMax(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Franquia = 5
    mvarLimMax = vData
End Property


Public Property Get ValFranquia() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Franquia
    ValFranquia = mvarFranquia
End Property


Public Property Let FatFranquia(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Franquia = 5
    mvarFatFranquia = vData
End Property

Public Property Get FatFranquia() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Franquia
    FatFranquia = mvarFatFranquia
End Property


Public Property Get LimMin() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Franquia
    LimMin = mvarLimMin
End Property

Public Property Get LimMax() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Franquia
    LimMax = mvarLimMax
End Property

Public Property Get PercMax() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Franquia
    PercMax = mvarPercMax
End Property


Public Property Get PercMin() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Franquia
    PercMin = mvarPercMin
End Property


Public Property Let ValPremio(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Premio = 5
    mvarPremio = vData
End Property


Public Property Get ValPremio() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Premio
    ValPremio = mvarPremio
End Property


Public Property Let PercDesconto(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.PercDesconto = 5
    mvarPercDesconto = vData
End Property

Public Property Get PercDesconto() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.PercDesconto
    PercDesconto = mvarPercDesconto
End Property


Public Property Let ValTaxa(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Taxa = 5
    mvarTaxa = vData
End Property

Public Property Get ValTaxa() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Taxa
    ValTaxa = mvarTaxa
End Property


Public Property Let TpCoberturaId(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.TpCoberturaId = 5
    mvarTpCoberturaId = vData
End Property

Public Property Get TpCoberturaId() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.TpCoberturaId
    TpCoberturaId = mvarTpCoberturaId
End Property


Public Property Let CodObjSegurado(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.CodObjSegurado = 5
    mvarCodObjSegurado = vData
End Property

Public Property Let ClasCobertura(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.CodObjSegurado = 5
    mvarClasCobertura = vData
End Property


Public Property Get CodObjSegurado() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.CodObjSegurado
    CodObjSegurado = mvarCodObjSegurado
End Property


Public Property Let Operacao(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.CodObjSegurado = 5
    mvarOperacao = vData
End Property

Public Property Let Acumula_IS(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.CodObjSegurado = 5
    mvarAcumula_IS = vData
End Property

Public Property Let Acumula_Resseguro(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.CodObjSegurado = 5
    mvarAcumula_Resseguro = vData
End Property


Public Property Get Operacao() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.CodObjSegurado
    Operacao = mvarOperacao
End Property


Public Property Get Acumula_IS() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.CodObjSegurado
    Acumula_IS = mvarAcumula_IS
End Property


Public Property Get Acumula_Resseguro() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.CodObjSegurado
    Acumula_Resseguro = mvarAcumula_Resseguro
End Property



Public Property Let Persistente(ByVal vData As Boolean)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.CodObjSegurado = 5
    mvarPersistente = vData
End Property

Public Property Let TpCobranca(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.CodObjSegurado = 5
    mvarTpCobranca = vData
End Property


Public Property Get Persistente() As Boolean
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.CodObjSegurado
    Persistente = mvarPersistente
End Property


Public Property Get TpCobranca() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.CodObjSegurado
    TpCobranca = mvarTpCobranca
End Property


