VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "DadosModular"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'local variable(s) to hold property value(s)
Private mvarvalPremioTarifa As Currency    'local copy
Private mvarvalCustoEmissao As Currency    'local copy
Private mvarvalAdicFracionamento As Currency    'local copy
Private mvarValDesconto As Currency    'local copy
Private mvarValRestituicao As Currency    'local copy
Private mvarValIOF As Currency    'local copy
Private mvarvalTaxaJuros As Double    'local copy
Private mvarPermiteAlteracaoCustoEndosso As Boolean    'local copy
Private mvarTipoEndosso As Integer    'local copy
Private mvarSucursalSeguradoraId As Long    'local copy
Private mvarSeguradoraCodSusep As Long    'local copy
Private mvarPropostaId As Long    'local copy
Private mvarApoliceId As Long    'local copy
Private mvarRamoId As Long    'local copy

Private mvarvalCorretagem As Currency    'local copy
Private mvarvalProLabore As Currency    'local copy
Private mvarClienteId As Long    'local copy
Private mvarFatorCambio As Double    'local copy
Private mvarPremioMoedaId As Integer    'local copy
Private mvarvalCustoApoliceCert As Currency    'local copy
Private mvarPercIR As Double    'local copy
Private mvarValRepasse As Currency    'local copy
Private mvarProdutoId As Integer    'local copy
Private mvarBeneficiarioId As Long    'local copy
Private mvarTpEmissao As String    'local copy
Private mvarvalMaxCustoEndosso As Currency    'local copy
Private mvarCriticaCustoRamo As Boolean    'local copy
Private mvarPercIOF As Double    'local copy
Private mvarEstipulanteId As Long    'local copy
Private mvarpercProLabore As Double    'local copy
Private mvarpercCorretagem As Double    'local copy
Private mvarProdutoExternoId As Long    'local copy
Private mvarSubGrupoId As Integer    'local copy
Private mvarvalSubvencaoEstadual As Currency    'local copy
Private mvarvalSubvencaoFederal As Currency    'local copy
Private mvarPossuiSubvencao As Boolean    'local copy
Private mvarEndossoId As Long    'local copy

Private mvarSubRamoId As Long    'rsilva
Private mvarDtInicioVigencia As String
Private mvarDtFimVigencia As String
Private mvarValPremioTotalRest As Currency
Private mvarValJurosRest As Currency
Private mvarValDescontoRest As Currency
Private mvarValIOFRest As Currency
Private mvarsDtFimVigenciaEsc As String
Private mvarsDtInicioVigenciaEsc As String
Private mvariTpEndossoId As Integer

'Private mvarValRestituicao As Currency

'Public Property Let ValRestituicao(ByVal vData As Currency)
'    mvarValRestituicao = vData
'End Property
'Public Property Get ValRestituicao() As Currency
'    ValRestituicao = mvarValRestituicao
'End Property
'
Public Property Let iTpEndossoId(ByVal vData As Integer)
    mvariTpEndossoId = vData
End Property
Public Property Get iTpEndossoId() As Integer
    iTpEndossoId = mvariTpEndossoId
End Property

Public Property Let sDtFimVigenciaEsc(ByVal vData As String)
    mvarsDtFimVigenciaEsc = vData
End Property
Public Property Get sDtFimVigenciaEsc() As String
    sDtFimVigenciaEsc = mvarsDtFimVigenciaEsc
End Property

Public Property Let sDtInicioVigenciaEsc(ByVal vData As String)
    mvarsDtInicioVigenciaEsc = vData
End Property
Public Property Get sDtInicioVigenciaEsc() As String
    sDtInicioVigenciaEsc = mvarsDtInicioVigenciaEsc
End Property



Public Property Let ValPagoRest(ByVal vData As Currency)
    mvarValPagoRest = vData
End Property
Public Property Get ValPagoRest() As Currency
    ValPagoRest = mvarValPagoRest
End Property


Public Property Let ValIOFRest(ByVal vData As Currency)
    mvarValIOFRest = vData
End Property
Public Property Get ValIOFRest() As Currency
    ValIOFRest = mvarValIOFRest
End Property


Public Property Let ValDescontoRest(ByVal vData As Currency)
    mvarValDescontoRest = vData
End Property
Public Property Get ValDescontoRest() As Currency
    ValDescontoRest = mvarValDescontoRest
End Property


Public Property Let ValJurosRest(ByVal vData As Currency)
    mvarValJurosRest = vData
End Property
Public Property Get ValJurosRest() As Currency
    ValJurosRest = mvarValJurosRest
End Property


Public Property Let ValPremioTotalRest(ByVal vData As Currency)
    mvarValPremioTotalRest = vData
End Property
Public Property Get ValPremioTotalRest() As Currency
    ValPremioTotalRest = mvarValPremioTotalRest
End Property

Public Property Let DtInicioVigencia(ByVal vData As String)
    mvarDtInicioVigencia = vData
End Property
Public Property Get DtInicioVigencia() As String
    DtInicioVigencia = mvarDtInicioVigencia
End Property
Public Property Let DtFimVigencia(ByVal vData As String)
    mvarDtFimVigencia = vData
End Property
Public Property Get DtFimVigencia() As String
    DtInicioVigencia = mvarDtFimVigencia
End Property

Public Property Let SubGrupoId(ByVal vData As Integer)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.SubGrupoId = 5
    mvarSubGrupoId = vData
End Property

Public Property Get SubGrupoId() As Integer
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.SubGrupoId
    SubGrupoId = mvarSubGrupoId
End Property

Public Property Let ProdutoExternoId(ByVal vData As Long)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.ProdutoExternoId = 5
    mvarProdutoExternoId = vData
End Property

Public Property Get ProdutoExternoId() As Long
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.ProdutoExternoId
    ProdutoExternoId = mvarProdutoExternoId
End Property
'rsilva
Public Property Let EndossoId(ByVal vData As Long)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.ProdutoExternoId = 5
    mvarEndossoId = vData
End Property

Public Property Get EndossoId() As Long
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.ProdutoExternoId
    EndossoId = mvarEndossoId
End Property

Public Property Let PercCorretagem(ByVal vData As Double)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.percCorretagem = 5
    mvarpercCorretagem = vData
End Property

Public Property Get PercCorretagem() As Double
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.percCorretagem
    PercCorretagem = mvarpercCorretagem
End Property

Public Property Let PercProLabore(ByVal vData As Double)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.percProLabore = 5
    mvarpercProLabore = vData
End Property

Public Property Get PercProLabore() As Double
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.percProLabore
    PercProLabore = mvarpercProLabore
End Property

Public Property Let EstipulanteId(ByVal vData As Long)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.EstipulanteId = 5
    mvarEstipulanteId = vData
End Property

Public Property Get EstipulanteId() As Long
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.EstipulanteId
    EstipulanteId = mvarEstipulanteId
End Property

Public Property Let PercIOF(ByVal vData As Double)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.PercIOF = 5
    mvarPercIOF = vData
End Property

Public Property Get PercIOF() As Double
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.PercIOF
    PercIOF = mvarPercIOF
End Property

Public Property Let CriticaCustoRamo(ByVal vData As Boolean)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.CriticaCustoRamo = 5
    mvarCriticaCustoRamo = vData
End Property

Public Property Get CriticaCustoRamo() As Boolean
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.CriticaCustoRamo
    CriticaCustoRamo = mvarCriticaCustoRamo
End Property

Public Property Let valMaxCustoEndosso(ByVal vData As Currency)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.valMaxCustoEndosso = 5
    mvarvalMaxCustoEndosso = vData
End Property

Public Property Get valMaxCustoEndosso() As Currency
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.valMaxCustoEndosso
    valMaxCustoEndosso = mvarvalMaxCustoEndosso
End Property

Public Property Let TpEmissao(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.TpEmissao = 5
    mvarTpEmissao = vData
End Property

Public Property Get TpEmissao() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.TpEmissao
    TpEmissao = mvarTpEmissao
End Property

Public Property Let BeneficiarioId(ByVal vData As Long)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.BeneficiarioId = 5
    mvarBeneficiarioId = vData
End Property

Public Property Get BeneficiarioId() As Long
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.BeneficiarioId
    BeneficiarioId = mvarBeneficiarioId
End Property

Public Property Let produtoId(ByVal vData As Integer)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.ProdutoId = 5
    mvarProdutoId = vData
End Property

Public Property Get produtoId() As Integer
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.ProdutoId
    produtoId = mvarProdutoId
End Property

Public Property Let valRepasse(ByVal vData As Currency)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.valRepasse = 5
    mvarValRepasse = vData
End Property

Public Property Get valRepasse() As Currency
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.valRepasse
    valRepasse = mvarValRepasse
End Property

Public Property Let PercIR(ByVal vData As Double)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.PercIR = 5
    mvarPercIR = vData
End Property

Public Property Get PercIR() As Double
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.PercIR
    PercIR = mvarPercIR
End Property

Public Property Let valCustoApoliceCert(ByVal vData As Currency)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.valCustoApoliceCert = 5
    mvarvalCustoApoliceCert = vData
End Property

Public Property Get valCustoApoliceCert() As Currency
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.valCustoApoliceCert
    valCustoApoliceCert = mvarvalCustoApoliceCert
End Property

Public Property Let PremioMoedaId(ByVal vData As Integer)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.PremioMoedaId = 5
    mvarPremioMoedaId = vData
End Property

Public Property Get PremioMoedaId() As Integer
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.PremioMoedaId
    PremioMoedaId = mvarPremioMoedaId
End Property

Public Property Let FatorCambio(ByVal vData As Double)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.FatorCambio = 5
    mvarFatorCambio = vData
End Property

Public Property Get FatorCambio() As Double
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.FatorCambio
    FatorCambio = mvarFatorCambio
End Property

Public Property Let ClienteId(ByVal vData As Long)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.ClienteId = 5
    mvarClienteId = vData
End Property

Public Property Get ClienteId() As Long
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.ClienteId
    ClienteId = mvarClienteId
End Property

Public Property Let ValProLabore(ByVal vData As Currency)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.valProLabore = 5
    mvarvalProLabore = vData
End Property

Public Property Get ValProLabore() As Currency
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.valProLabore
    ValProLabore = mvarvalProLabore
End Property

Public Property Let valCorretagem(ByVal vData As Currency)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.valCorretagem = 5
    mvarvalCorretagem = vData
End Property

Public Property Get valCorretagem() As Currency
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.valCorretagem
    valCorretagem = mvarvalCorretagem
End Property

Public Property Let RamoId(ByVal vData As Long)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.RamoId = 5
    mvarRamoId = vData
End Property

Public Property Get SubRamoId() As Long
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.RamoId
    SubRamoId = mvarSubRamoId
End Property
Public Property Let SubRamoId(ByVal vData As Long)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.RamoId = 5
    mvarSubRamoId = vData
End Property

Public Property Get RamoId() As Long
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.RamoId
    RamoId = mvarRamoId
End Property

Public Property Let ApoliceId(ByVal vData As Long)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.ApoliceId = 5
    mvarApoliceId = vData
End Property

Public Property Get ApoliceId() As Long
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.ApoliceId
    ApoliceId = mvarApoliceId
End Property

Public Property Let PropostaId(ByVal vData As Long)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.PropostaId = 5
    mvarPropostaId = vData
End Property

Public Property Get PropostaId() As Long
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.PropostaId
    PropostaId = mvarPropostaId
End Property

Public Property Let SeguradoraCodSusep(ByVal vData As Long)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.SeguradoraCodSusep = 5
    mvarSeguradoraCodSusep = vData
End Property

Public Property Get SeguradoraCodSusep() As Long
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.SeguradoraCodSusep
    SeguradoraCodSusep = mvarSeguradoraCodSusep
End Property

Public Property Let SucursalSeguradoraId(ByVal vData As Long)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.SucursalSeguradoraId = 5
    mvarSucursalSeguradoraId = vData
End Property

Public Property Get SucursalSeguradoraId() As Long
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.SucursalSeguradoraId
    SucursalSeguradoraId = mvarSucursalSeguradoraId
End Property

Public Property Let TipoEndosso(ByVal vData As Integer)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.TipoEndosso = 5
    mvarTipoEndosso = vData
End Property

Public Property Get TipoEndosso() As Integer
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.TipoEndosso
    TipoEndosso = mvarTipoEndosso
End Property

Public Property Let PermiteAlteracaoCustoEndosso(ByVal vData As Boolean)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.PermiteAlteracaoCustoEndosso = 5
    mvarPermiteAlteracaoCustoEndosso = vData
End Property

Public Property Get PermiteAlteracaoCustoEndosso() As Boolean
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.PermiteAlteracaoCustoEndosso
    PermiteAlteracaoCustoEndosso = mvarPermiteAlteracaoCustoEndosso
End Property

Public Property Let valTaxaJuros(ByVal vData As Double)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.valTaxaJuros = 5
    mvarvalTaxaJuros = vData
End Property

Public Property Get valTaxaJuros() As Double
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.valTaxaJuros
    valTaxaJuros = mvarvalTaxaJuros
End Property

Public Property Let valIOF(ByVal vData As Currency)
    mvarValIOF = vData
End Property

Public Property Get valIOF() As Currency
    valIOF = mvarValIOF
End Property

Public Property Let ValRestituicao(ByVal vData As Currency)
    mvarValRestituicao = vData
End Property

Public Property Get ValRestituicao() As Currency
    ValRestituicao = mvarValRestituicao
End Property

Public Property Let valDesconto(ByVal vData As Currency)
    mvarValDesconto = vData
End Property

Public Property Get valDesconto() As Currency
    valDesconto = mvarValDesconto
End Property

Public Property Let valAdicFracionamento(ByVal vData As Currency)
    mvarvalAdicFracionamento = vData
End Property

Public Property Get valAdicFracionamento() As Currency
    valAdicFracionamento = mvarvalAdicFracionamento
End Property

Public Property Let valCustoEmissao(ByVal vData As Currency)
    mvarvalCustoEmissao = vData
End Property

Public Property Get valCustoEmissao() As Currency
    valCustoEmissao = mvarvalCustoEmissao
End Property

Public Property Let valPremioTarifa(ByVal vData As Currency)
    mvarvalPremioTarifa = vData
End Property

Public Property Get valPremioTarifa() As Currency
    valPremioTarifa = mvarvalPremioTarifa
End Property

Public Property Let valSubvencaoEstadual(ByVal vData As Currency)
    mvarvalSubvencaoEstadual = vData
End Property

Public Property Get valSubvencaoEstadual() As Currency
    valSubvencaoEstadual = mvarvalSubvencaoEstadual
End Property

Public Property Let valSubvencaoFederal(ByVal vData As Currency)
    mvarvalSubvencaoFederal = vData
End Property

Public Property Get valSubvencaoFederal() As Currency
    valSubvencaoFederal = mvarvalSubvencaoFederal
End Property

Public Property Let PossuiSubvencao(ByVal vData As Boolean)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.PossuiSubvencao = 5
    mvarPossuiSubvencao = vData
End Property

Public Property Get PossuiSubvencao() As Boolean
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.PossuiSubvencao
    PossuiSubvencao = mvarPossuiSubvencao
End Property

