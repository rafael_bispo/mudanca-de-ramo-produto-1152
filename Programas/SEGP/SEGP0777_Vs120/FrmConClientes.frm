VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.2#0"; "MSCOMCTL.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.ocx"
Begin VB.Form FrmConClientes 
   Caption         =   "SEG06057 - Consulta de Clientes - "
   ClientHeight    =   6045
   ClientLeft      =   1785
   ClientTop       =   1560
   ClientWidth     =   8430
   LinkTopic       =   "Form1"
   ScaleHeight     =   6045
   ScaleWidth      =   8430
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdOk 
      Caption         =   "Ok"
      Enabled         =   0   'False
      Height          =   375
      Left            =   5505
      TabIndex        =   6
      Top             =   5280
      Width           =   1395
   End
   Begin MSComctlLib.StatusBar StatusBar1 
      Align           =   2  'Align Bottom
      Height          =   300
      Left            =   0
      TabIndex        =   8
      Top             =   5745
      Width           =   8430
      _ExtentX        =   14870
      _ExtentY        =   529
      Style           =   1
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "Cancelar"
      Height          =   375
      Left            =   6945
      TabIndex        =   7
      Top             =   5280
      Width           =   1395
   End
   Begin VB.Frame Framecriterios 
      Caption         =   "Crit�rios"
      Height          =   1485
      Left            =   60
      TabIndex        =   0
      Top             =   30
      Width           =   8295
      Begin VB.CommandButton Btnpesquisa 
         Caption         =   "Pesquisar"
         Height          =   345
         Left            =   6855
         TabIndex        =   5
         Top             =   990
         Width           =   1305
      End
      Begin VB.ComboBox Cmbcampo 
         Height          =   315
         Left            =   180
         Style           =   2  'Dropdown List
         TabIndex        =   2
         Top             =   570
         Width           =   2625
      End
      Begin VB.TextBox txtConteudo 
         Height          =   315
         Left            =   3000
         TabIndex        =   4
         Top             =   570
         Width           =   5145
      End
      Begin VB.Label Label1 
         Caption         =   "Pesquisar por:"
         Height          =   225
         Left            =   210
         TabIndex        =   1
         Top             =   300
         Width           =   1185
      End
      Begin VB.Label Label2 
         Caption         =   "Conte�do:"
         Height          =   225
         Left            =   3030
         TabIndex        =   3
         Top             =   300
         Width           =   975
      End
   End
   Begin MSFlexGridLib.MSFlexGrid grdCliente 
      Height          =   3615
      Left            =   30
      TabIndex        =   9
      Top             =   1620
      Width           =   8325
      _ExtentX        =   14684
      _ExtentY        =   6376
      _Version        =   393216
      FixedCols       =   0
      HighLight       =   2
      SelectionMode   =   1
   End
End
Attribute VB_Name = "FrmConClientes"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmdOk_Click()

    frmEndosso.cmdEstAdicionar.Enabled = True
    frmEndosso.cmdEstAlterar.Enabled = False
    frmEndosso.cmdEstRemover.Enabled = False

    frmEndosso.txtEstipulante.Text = grdCliente.TextMatrix(grdCliente.Row, 0)
    frmEndosso.txtNomeEstipulante(0).Text = grdCliente.TextMatrix(grdCliente.Row, 1)
    frmEndosso.mskPercEstipulante.mask = "3,4"
    frmEndosso.mskPercEstipulante.Text = "0,0000"

    Unload Me

End Sub

Private Sub cmdCancelar_Click()
    Unload Me
End Sub

Private Sub Btnpesquisa_Click()
    Call PesquisarCliente
End Sub

Private Sub PesquisarCliente()

    Dim oCliente As Object
    Dim rs As Recordset

    On Error GoTo TrataErro

    If Cmbcampo.Text = "" Then
        mensagem_erro 6, "Informe o campo a ser pesquisado."
        Cmbcampo.SetFocus
        Exit Sub
    End If

    If txtConteudo.Text = "" Then
        mensagem_erro 6, "Informe o conte�do a ser pesquisado."
        txtConteudo.SetFocus
        Exit Sub
    End If

    If Cmbcampo.Text = "CGC" Then
        If Not IsNumeric(txtConteudo.Text) Then
            mensagem_erro 6, "O conte�do informado n�o � um n�mero."
            txtConteudo.SetFocus
            Exit Sub
        End If
    End If

    MousePointer = vbHourglass

    Set oCliente = CreateObject("CLIL0003.cls00110")

    Select Case Cmbcampo.ListIndex
    Case 0    'CGC
        Set rs = oCliente.ConsultarClientePFPJ(gsSIGLASISTEMA, _
                                               App.Title, _
                                               App.FileDescription, _
                                               glAmbiente_id, _
               , _
                                               Trim(txtConteudo.Text))

    Case 1    'Nome
        Set rs = oCliente.ConsultarClientePFPJ(gsSIGLASISTEMA, _
                                               App.Title, _
                                               App.FileDescription, _
                                               glAmbiente_id, _
                                               Trim(txtConteudo.Text))

    End Select

    Set oCliente = Nothing

    grdCliente.Rows = 1

    If Not rs.EOF Then
        While Not rs.EOF
            grdCliente.AddItem rs("cliente_id") & vbTab & rs("nome")
            rs.MoveNext
        Wend
    End If

    Set rs = Nothing

    MousePointer = vbDefault

    If grdCliente.Rows > 1 Then
        StatusBar1.SimpleText = "Clique no cliente desejado para estipulante."
    Else
        mensagem_erro 6, "N�o foram encontrados registros para condi��o especificada."
        StatusBar1.SimpleText = "Escolha o campo de pesquisa e informe o  conte�do a ser pesquisado."
        cmdOk.Enabled = False
    End If

    Exit Sub

TrataErro:
    Call TratarErro("PesquisarCliente", Me.name)
    Call FinalizarAplicacao

End Sub

Private Sub CmbCampo_Click()
    txtConteudo.SetFocus
End Sub

Private Sub Form_Activate()
    Cmbcampo.SetFocus
    StatusBar1.SimpleText = "Escolha o campo de pesquisa e informe o conte�do a ser pesquisado."
End Sub

Private Sub Form_Load()

    Me.Caption = "SEG30777 - Consulta de Clientes"

End Sub

Private Sub grdCliente_Click()

    cmdOk.Enabled = True

End Sub
