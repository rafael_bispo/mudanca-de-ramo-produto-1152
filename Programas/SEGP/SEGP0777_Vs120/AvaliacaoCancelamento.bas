Attribute VB_Name = "Local"
'Vari�vel de conex�o
Public RdoCnAux As New rdoConnection

'Vari�veis
Public LinhaGridSelEndossos As Long
Public TpOperacao As String
Public Emi_Id As String
Public Origem As String
Public Produto_id As String
Public bDtCancelamentoValida As Boolean
Public vgsituacao As String
Public existe_perc_restituicao As Boolean


Public restituicao_integral As String
Public gcValTotPremioPago As Currency
Public gcValTotJurosPago As Currency
Public gcValTotAdicFracionamentoPago As Currency
Public gcValTotDescontoPago As Currency
Public gcValTotIOFPago As Currency
Public gcValTotPremioTarifaPago As Currency
Public gcValCustoApoliceCertif As Currency

Public gsPossuiSubvencao As String     'Rmarins - 20/10/06


'rsilva 24/07/2009
'Proposta Modular
Public bModular As Boolean
Public propostaModular As Long
Public LinhaGridSelEndossosModular As Long
Public TpOperacaoModular As String
Public Emi_IdModular As String
Public OrigemModular As String
Public Produto_idModular As String
Public bDtCancelamentoValidaModular As Boolean
Public vgsituacaoModular As String
Public existe_perc_restituicaoModular As Boolean

Public restituicao_integralModular As String
Public gcValTotPremioPagoModular As Currency
Public gcValTotJurosPagoModular As Currency
Public gcValTotAdicFracionamentoPagoModular As Currency
Public gcValTotDescontoPagoModular As Currency
Public gcValTotIOFPagoModular As Currency
Public gcValTotPremioTarifaPagoModular As Currency
Public gcValCustoApoliceCertifModular As Currency

Public gsPossuiSubvencaoModular As String
Public AvaliacaoGerencial As Boolean


'Constantes
Public Const ProdIdOvProdutorRural = 14
Public Const ProdIdRuralRapido = 16
Public Const ProdIdAgricola = 145
Public Const ProIdPenhorRural = 155

'Cole��es
Public Cancelamentos As New Collection
Public Cobrancas As New Collection
Public Corretagens As New Collection
Public Clientes As New Collection      'Rmarins - 20/10/06
Public Congeneres As New Collection    'Demanda 400422 - emaior 02/12/2008 - Relat�rio de Restitui��o de Comiss�o de Cosseguro

'Renato Vasconcelos
'Flow 11022639
'Inicio
Public comboCarregado As Boolean
Public endossoGerado As Boolean
Public adesaoFechadaGerado As Boolean
Public tpEndossoGerado As Boolean
Public infoBaixaGerado As Boolean
Public pagoRetidoGerado As Boolean
Public clienteGerado As Boolean
Public gerencial As Boolean
'Fim




Sub Atualizar_Bloqueio_Proposta(ByVal PProposta As String, ByVal PStatus As String)

    Dim rc As rdoResultset

    On Error GoTo Erro

    SQL = "exec " & Ambiente & ".lock_proposta_spu  " & PProposta & ", '" & PStatus & "'"

    Set rc = rdocn.OpenResultset(SQL)

    rc.Close

    Exit Sub

Erro:
    TrataErroGeral "Atualiza Bloqueio Proposta"

End Sub


Function Obter_Motivo_Retorno() As String

    FrmMotivoRetorno.Show vbModal

    Obter_Motivo_Retorno = FrmMotivoRetorno.TxtMotivoRetorno.Text

    Unload FrmMotivoRetorno

End Function

Function Obter_Motivo_Recusa(Optional ByRef tMotivoRecusa As String) As Integer

    FrmMotivoRecusa.Show vbModal

    If FrmMotivoRecusa.CmbMotivoRecusa.ListIndex <> -1 Then
        Obter_Motivo_Recusa = FrmMotivoRecusa.CmbMotivoRecusa.ItemData(FrmMotivoRecusa.CmbMotivoRecusa.ListIndex)
        tMotivoRecusa = FrmMotivoRecusa.CmbMotivoRecusa.Text
    Else
        Obter_Motivo_Recusa = 0
        tMotivoRecusa = ""
    End If
    Unload FrmMotivoRetorno

End Function

Function Obter_Proposta_Basica(PProduto) As String

    Dim SQL As String
    Dim Rst As rdoResultset

    SQL = "Select val_parametro from ps_parametro_tb where parametro = 'BASICA " & PProduto & "'"

    Set Rst = rdocn.OpenResultset(SQL)

    If Not Rst.EOF Then
        Obter_Proposta_Basica = Rst!val_parametro
    Else
        Obter_Proposta_Basica = ""
    End If

End Function

Function Obter_Beneficiario(PPropostaId) As String

    SQL = ""
    SQL = SQL & "SELECT proposta_tb.prop_cliente_id, "
    'Inicio:Demanda 11292027 Autor:Eduardo.Borges Data:08/06/2011
    SQL = SQL & "       produto_tb.processa_restituicao_automatica, "
    'Fim:Demanda 11292027 Autor:Eduardo.Borges Data:08/06/2011
    SQL = SQL & "       produto_tb.restituicao_tp_beneficiario, "
    SQL = SQL & "       produto_tb.restituicao_cliente_id "
    SQL = SQL & "FROM proposta_tb "
    SQL = SQL & "JOIN produto_tb "
    SQL = SQL & "  ON produto_tb.produto_id = proposta_tb.produto_id "
    SQL = SQL & "WHERE proposta_id = " & PPropostaId

    Set Rst = rdocn.OpenResultset(SQL)

    If Not Rst.EOF Then
        If UCase(Rst!restituicao_tp_beneficiario) = "P" Then
            Obter_Beneficiario = Rst!prop_cliente_id
        ElseIf UCase(Rst!restituicao_tp_beneficiario) = "T" Then
            Obter_Beneficiario = Rst!restituicao_cliente_id
            'Inicio:Demanda 11292027 Autor:Eduardo.Borges Data:08/06/2011
        ElseIf IsNull(Rst("restituicao_tp_beneficiario")) And LCase(Rst("processa_restituicao_automatica")) = "n" Then
            Obter_Beneficiario = Rst!prop_cliente_id
            'Fim:Demanda 11292027 Autor:Eduardo.Borges Data:08/06/2011
        Else
            If Rst("tp_ramo_id") = 2 Then
                Obter_Beneficiario = Rst!prop_cliente_id
            Else
                MsgBox "N�o foi poss�vel localizar o benefici�rio da proposta. "
            End If
        End If
    Else
        MsgBox "N�o foi poss�vel localizar o benefici�rio da proposta. "
        End
    End If

    Rst.Close

End Function

Sub Obter_Premio_IS(ByVal PProposta As String, _
                    ByVal PProduto As String, _
                    ByRef PValIS As Currency, _
                    ByRef PValPremio As Currency, _
                    Optional iCobertura As Integer)

    Dim SQL As String
    Dim Rst As rdoResultset

    'Obtendo IS e Premio''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

    SQL = "SELECT "
    SQL = SQL & "   imp_segurada, "
    SQL = SQL & "   val_premio "
    SQL = SQL & "FROM "
    SQL = SQL & "   escolha_plano_tb "
    SQL = SQL & "WHERE "
    SQL = SQL & "   proposta_id = " & PProposta
    SQL = SQL & "   AND dt_fim_vigencia IS NULL "


    Set Rst = rdocn.OpenResultset(SQL)

    If Rst.EOF Then

        Rst.Close

        If PProduto = ProdIdAgricola Or PProduto = 1168 Then

            Select Case PProduto
            Case ProdIdAgricola
                SQL = "SELECT "
                SQL = SQL & "   sum(val_is), "
                SQL = SQL & "   sum(val_premio) "
                SQL = SQL & "FROM "
                SQL = SQL & "   escolha_tp_cob_generico_tb "
                SQL = SQL & "WHERE "
                SQL = SQL & "   proposta_id = " & PProposta
                '        If PProduto = 1168 And iCobertura > 0 Then
                '            SQL = SQL & "  AND tp_cobertura_id = " & iCobertura & vbNewLine
                '        End If
                SQL = SQL & "   AND dt_fim_vigencia_esc IS NULL "
            Case 1168
                SQL = ""
                adSQL SQL, "SELECT tp_cobertura_id = epsht.tp_cobertura_id, "
                adSQL SQL, "       val_is          = Sum(epsht.val_is),"
                adSQL SQL, "       Val_Premio      = Sum(epsht.VL_PCL_MOEN_SGRO)"
                adSQL SQL, "  FROM emi_parcelas_seguro_habitacional_tb epsht"
                adSQL SQL, " WHERE epsht.proposta_id = " & PProposta
                adSQL SQL, " GROUP BY epsht.tp_cobertura_id"

            End Select

            Set Rst = rdocn.OpenResultset(SQL)

        Else

            SQL = "SELECT "
            SQL = SQL & "   val_is, "
            SQL = SQL & "   val_premio_bruto "
            SQL = SQL & "FROM "
            SQL = SQL & "   proposta_adesao_tb "
            SQL = SQL & "WHERE "
            SQL = SQL & "   proposta_id = " & PProposta

            Set Rst = rdocn.OpenResultset(SQL)

            If Rst.EOF Then

                Rst.Close

                SQL = "SELECT "
                SQL = SQL & "   val_is, "
                SQL = SQL & "   val_premio_bruto "
                SQL = SQL & "FROM "
                SQL = SQL & "   proposta_fechada_tb "
                SQL = SQL & "WHERE "
                SQL = SQL & "   proposta_id = " & PProposta

                Set Rst = rdocn.OpenResultset(SQL)

            End If
        End If

    End If


    If bModular = False Then
        If Not Rst.EOF Then
            PValIS = Val(IIf(IsNull(Rst(0)), 0, Rst(0)))
            PValPremio = Val(IIf(IsNull(Rst(1)), 0, Rst(1)))
        Else
            PValIS = 0
            PValPremio = 0
        End If
    Else
        If PProduto <> 1168 Then
            If oDadosModular.proposta_id <> PProposta Then
                PValIS = Val(IIf(IsNull(Rst(0)), 0, Rst(0)))
                PValPremio = Val(IIf(IsNull(Rst(1)), 0, Rst(1)))
            Else
                oDadosModular.val_is = Val(IIf(IsNull(Rst(0)), 0, Rst(0)))
                oDadosModular.Val_Premio = Val(IIf(IsNull(Rst(1)), 0, Rst(1)))
            End If
        Else
            While Not Rst.EOF
                If Rst("tp_cobertura_id") <> oDadosModular.iTpCobertura Then
                    PValIS = Val(IIf(IsNull(Rst("val_is")), 0, Rst("val_is")))
                    PValPremio = Val(IIf(IsNull(Rst("Val_Premio")), 0, Rst("Val_Premio")))
                Else
                    oDadosModular.val_is = Val(IIf(IsNull(Rst("val_is")), 0, Rst("val_is")))
                    oDadosModular.Val_Premio = Val(IIf(IsNull(Rst("Val_Premio")), 0, Rst("Val_Premio")))
                End If
                Rst.MoveNext
            Wend
            Rst.Close
        End If
    End If

End Sub

Public Sub Obter_Dados_Adicionais_Proposta(ByVal PProposta, _
                                           ByRef PRamoId As String, _
                                           ByRef PCustoApoliceCertif As Currency, _
                                           Optional ByRef PPeriodo_pgto_id As String, _
                                           Optional ByRef vMoedaPremioId As Variant, _
                                           Optional ByRef sTipoEmissao As String, _
                                           Optional ByRef lSeguradora As Long)

    On Error GoTo TrataErro
    Dim SQL As String
    Dim Rst As rdoResultset

    'Demanda 400422 - emaior - 02/12/2008 - ALTERADO PARA SELECIONAR O TIPO DE EMISS�O E SEGURADORA DA AP�LICE
    SQL = " SELECT " & vbNewLine
    SQL = SQL & "       proposta_adesao_tb.ramo_id, " & vbNewLine
    SQL = SQL & "       custo_apolice_certif = isnull(proposta_adesao_tb.custo_certificado, 0) , " & vbNewLine
    'pcarvalho - 17/06/2003 Padroniza��o do c�lculo da restitui��o
    SQL = SQL & "       premio_moeda_id = ISNULL(proposta_adesao_tb.premio_moeda_id, 0), " & vbNewLine
    SQL = SQL & "       periodo_pgto_id = ISNULL(proposta_adesao_tb.periodo_pgto_id, 0), " & vbNewLine
    SQL = SQL & "       apolice_tb.tp_emissao, " & vbNewLine
    SQL = SQL & "       apolice_tb.seguradora_cod_susep " & vbNewLine
    SQL = SQL & " FROM " & vbNewLine
    SQL = SQL & "       proposta_adesao_tb with(NOLOCK) " & vbNewLine
    SQL = SQL & " INNER JOIN apolice_tb with(NOLOCK) " & vbNewLine
    SQL = SQL & "         ON apolice_tb.apolice_id = proposta_adesao_tb.apolice_id " & vbNewLine
    SQL = SQL & "        AND apolice_tb.sucursal_seguradora_id = proposta_adesao_tb.sucursal_seguradora_id " & vbNewLine
    SQL = SQL & "        AND apolice_tb.seguradora_cod_susep = proposta_adesao_tb.seguradora_cod_susep " & vbNewLine
    SQL = SQL & "        AND apolice_tb.ramo_id = proposta_adesao_tb.ramo_id " & vbNewLine
    SQL = SQL & " WHERE " & vbNewLine
    SQL = SQL & "       proposta_adesao_tb.proposta_id = " & PProposta


    Set Rst = rdocn.OpenResultset(SQL)

    If Rst.EOF Then

        Rst.Close
        'Demanda 400422 - emaior - 02/12/2008 - ALTERADO PARA SELECIONAR O TIPO DE EMISS�O E SEGURADORA DA AP�LICE
        SQL = " SELECT "
        SQL = SQL & "       apolice_tb.ramo_id, "
        SQL = SQL & "       custo_apolice_certif = proposta_fechada_tb.custo_apolice, "
        'pcarvalho - 17/06/2003 Padroniza��o do c�lculo da restitui��o
        SQL = SQL & "       premio_moeda_id = ISNULL(premio_moeda_id, 0), "
        SQL = SQL & "       periodo_pgto_id = ISNULL(periodo_pgto_id, 0), "
        SQL = SQL & "       apolice_tb.tp_emissao, "
        SQL = SQL & "       apolice_tb.seguradora_cod_susep "
        SQL = SQL & " FROM "
        SQL = SQL & "       proposta_fechada_tb "
        SQL = SQL & "       INNER JOIN apolice_tb "
        SQL = SQL & "             ON apolice_tb.proposta_id = proposta_fechada_tb.proposta_id "
        SQL = SQL & " WHERE "
        SQL = SQL & "       proposta_fechada_tb.proposta_id = " & PProposta

        Set Rst = rdocn.OpenResultset(SQL)

    End If

    PRamoId = Rst!ramo_id
    PCustoApoliceCertif = Val(Rst!custo_apolice_certif)
    vMoedaPremioId = Val(Rst("premio_moeda_id"))     'pcarvalho - 17/06/2003 - Padroniza��o do c�lculo da restitui��o
    PPeriodo_pgto_id = Rst!periodo_pgto_id
    sTipoEmissao = Rst!tp_emissao           'Demanda 400422 - emaior - 02/12/2008
    lSeguradora = Rst!seguradora_cod_susep  'Demanda 400422 - emaior - 02/12/2008

    Rst.Close
    Exit Sub
TrataErro:
    Call Err.Raise(Err.Number, , "Obter_Dados_Adicionais_Proposta - " & Err.Description)

End Sub
Public Sub Obter_Dados_Adicionais_Proposta_Modular(ByVal PProposta, _
                                                   ByRef PRamoId As String, _
                                                   ByRef PCustoApoliceCertif As Currency, _
                                                   Optional ByRef PPeriodo_pgto_id As String, _
                                                   Optional ByRef vMoedaPremioId As Variant, _
                                                   Optional ByRef sTipoEmissao As String, _
                                                   Optional ByRef lSeguradora As Long)

    Dim SQL As String
    Dim RstM As rdoResultset

    SQL = " SELECT " & vbNewLine
    SQL = SQL & "       proposta_adesao_tb.ramo_id, " & vbNewLine
    SQL = SQL & "       custo_apolice_certif = isnull(proposta_adesao_tb.custo_certificado, 0) , " & vbNewLine
    SQL = SQL & "       premio_moeda_id = ISNULL(proposta_adesao_tb.premio_moeda_id, 0), " & vbNewLine
    SQL = SQL & "       periodo_pgto_id = ISNULL(proposta_adesao_tb.periodo_pgto_id, 0), " & vbNewLine
    SQL = SQL & "       apolice_tb.tp_emissao, " & vbNewLine
    SQL = SQL & "       apolice_tb.seguradora_cod_susep " & vbNewLine
    SQL = SQL & " FROM " & vbNewLine
    SQL = SQL & "       proposta_adesao_tb with(NOLOCK) " & vbNewLine
    SQL = SQL & " INNER JOIN apolice_tb with(NOLOCK) " & vbNewLine
    SQL = SQL & "         ON apolice_tb.apolice_id = proposta_adesao_tb.apolice_id " & vbNewLine
    SQL = SQL & "        AND apolice_tb.sucursal_seguradora_id = proposta_adesao_tb.sucursal_seguradora_id " & vbNewLine
    SQL = SQL & "        AND apolice_tb.seguradora_cod_susep = proposta_adesao_tb.seguradora_cod_susep " & vbNewLine
    SQL = SQL & "        AND apolice_tb.ramo_id = proposta_adesao_tb.ramo_id " & vbNewLine
    SQL = SQL & " WHERE " & vbNewLine
    SQL = SQL & "       proposta_adesao_tb.proposta_id = " & PProposta

    Set RstM = rdocn.OpenResultset(SQL)

    If RstM.EOF Then

        RstM.Close
        SQL = " SELECT "
        SQL = SQL & "       apolice_tb.ramo_id, "
        SQL = SQL & "       custo_apolice_certif = proposta_fechada_tb.custo_apolice, "
        SQL = SQL & "       premio_moeda_id = ISNULL(premio_moeda_id, 0), "
        SQL = SQL & "       periodo_pgto_id = ISNULL(periodo_pgto_id, 0), "
        SQL = SQL & "       apolice_tb.tp_emissao, "
        SQL = SQL & "       apolice_tb.seguradora_cod_susep "
        SQL = SQL & " FROM "
        SQL = SQL & "       proposta_fechada_tb "
        SQL = SQL & "       INNER JOIN apolice_tb "
        SQL = SQL & "             ON apolice_tb.proposta_id = proposta_fechada_tb.proposta_id "
        SQL = SQL & " WHERE "
        SQL = SQL & "       proposta_fechada_tb.proposta_id = " & PProposta

        Set RstM = rdocn.OpenResultset(SQL)

    End If

    If Not RstM.EOF Then
        oDadosModular.ramo_id = RstM("ramo_id")
        oDadosModular.custo_apolice = Val(RstM("custo_apolice_certif"))
        oDadosModular.MoedaPremioId = RstM("premio_moeda_id")
        oDadosModular.PeriodoPgtoId = RstM("periodo_pgto_id")
        oDadosModular.TipoEmissao = RstM("tp_emissao")
        oDadosModular.Seguradora = RstM("seguradora_cod_susep")
    End If

    RstM.Close

End Sub


Sub Obter_Tx_Adic_Fracionamento(ByVal PProposta As String, _
                                ByRef PFatFracionamento As Currency)

    Dim SQL As String
    Dim Rst As rdoResultset

    SQL = "Select "
    SQL = SQL & "     juros_parcelamento_tb.fat_fracionamento "
    SQL = SQL & " From "
    SQL = SQL & "     proposta_tb "
    SQL = SQL & "     inner join proposta_adesao_tb "
    SQL = SQL & "          on proposta_adesao_tb.proposta_id = proposta_tb.proposta_id "
    SQL = SQL & "     inner join juros_tb"
    SQL = SQL & "          on juros_tb.perc_juros = proposta_adesao_tb.taxa_juros"
    SQL = SQL & "     inner join juros_parcelamento_tb "
    SQL = SQL & "          on juros_parcelamento_tb.juros_id = juros_tb.juros_id "
    SQL = SQL & "          and  juros_parcelamento_tb.qtd_parcelas = proposta_adesao_tb.qtd_parcela_premio"
    SQL = SQL & " Where"
    SQL = SQL & "      proposta_tb.proposta_id = " & PProposta
    SQL = SQL & "      and isnull(proposta_adesao_tb.taxa_juros, 0) <> 0 "
    SQL = SQL & "      and juros_parcelamento_tb.tp_juros = Case "
    SQL = SQL & "                                               When proposta_adesao_tb.val_pgto_ato is not null then 'a' "
    SQL = SQL & "                                               Else 'p' "
    SQL = SQL & "                                           End "

    Set Rst = rdocn.OpenResultset(SQL)

    If Rst.EOF Then

        Rst.Close

        SQL = "Select "
        SQL = SQL & "     juros_parcelamento_tb.fat_fracionamento "
        SQL = SQL & " From "
        SQL = SQL & "     proposta_tb "
        SQL = SQL & "     inner join proposta_fechada_tb "
        SQL = SQL & "          on proposta_fechada_tb.proposta_id = proposta_tb.proposta_id "
        SQL = SQL & "     inner join juros_tb"
        SQL = SQL & "          on juros_tb.perc_juros = proposta_fechada_tb.taxa_juros"
        SQL = SQL & "     inner join juros_parcelamento_tb "
        SQL = SQL & "          on juros_parcelamento_tb.juros_id = juros_tb.juros_id "
        SQL = SQL & "          and  juros_parcelamento_tb.qtd_parcelas = proposta_fechada_tb.num_parcelas"
        SQL = SQL & " Where"
        SQL = SQL & "      proposta_tb.proposta_id = " & PProposta
        SQL = SQL & "      and isnull(proposta_fechada_tb.taxa_juros, 0) <> 0 "
        SQL = SQL & "      and juros_parcelamento_tb.tp_juros = Case "
        SQL = SQL & "                                               When proposta_fechada_tb.val_pgto_ato is not null then 'a' "
        SQL = SQL & "                                               Else 'p' "
        SQL = SQL & "                                           End "

        Set Rst = rdocn.OpenResultset(SQL)

    End If

    If Not Rst.EOF Then
        PFatFracionamento = Val(Rst!fat_fracionamento)
    Else
        PFatFracionamento = 0
    End If

    Rst.Close

End Sub

Function TiraPonto(ValorAux As String) As String

    If InStr(ValorAux, ".") = 0 Then
        TiraPonto = ValorAux
    Else
        '* Alterado por Junior em 13/10/2003 - para tratar mais de UM ponto
        ValorAux = Replace(ValorAux, ".", "")
        'ValorAux = Mid(ValorAux, 1, InStr(ValorAux, ".") - 1) + Mid(ValorAux, InStr(ValorAux, ".") + 1)
        TiraPonto = ValorAux
    End If

End Function

Function Obter_Dados_Produto(ByVal PProduto, _
                             ByRef PPrazoRestituicaoIntegral, _
                             ByRef PTpRestituicao, _
                             ByRef PPossuiSubvencao)

    Dim SQL As String
    Dim Rst As rdoResultset

    SQL = "Select "
    SQL = SQL & "   prazo_restituicao_integral, "
    SQL = SQL & "   tp_calc_restituicao, "
    SQL = SQL & "   possui_subvencao "
    SQL = SQL & "From "
    SQL = SQL & "   produto_tb "
    SQL = SQL & "Where "
    SQL = SQL & "   produto_id = " & PProduto

    Set Rst = rdocn.OpenResultset(SQL)


    PPrazoRestituicaoIntegral = Rst!Prazo_Restituicao_Integral
    PTpRestituicao = Rst!Tp_Calc_Restituicao
    PPossuiSubvencao = Rst!Possui_subvencao   'Rmarins - 20/10/06
    Rst.Close

End Function


Function Verificar_Uso_Proposta(ByVal PProposta As String) As Boolean

    Dim rc As rdoResultset
    Dim Lock_Aceite As String

    On Error GoTo Erro

    SQL = "select lock_aceite from proposta_tb where proposta_id =  " & PProposta

    Set rc = rdocn.OpenResultset(SQL)

    If Not rc.EOF Then

        Lock_Aceite = UCase(rc(0))
        If Lock_Aceite = "S" Then
            Verificar_Uso_Proposta = True
        Else
            Verificar_Uso_Proposta = False
        End If

    End If

    rc.Close

    Exit Function

Erro:
    TrataErroGeral "Verifica Proposta Uso"
    mensagem_erro 6, "Erro na rotina Verifica Proposta Uso : " & Err.Description & ". Programa ser� cancelado."
    End
End Function


Function VerificarExistenciaAvaliacaoRestituicaoPendente(ByVal PProposta, ByVal PAvaliacao, ByVal PTpOperacao) As Boolean

    Dim rc As rdoResultset
    Dim Situacao As String

    If PTpOperacao = 1 Then
        SQL = " SELECT "
        SQL = SQL & "       proposta_id "
        SQL = SQL & " FROM "
        SQL = SQL & "       avaliacao_restituicao_tb "
        SQL = SQL & " WHERE "
        SQL = SQL & "       proposta_id = " & PProposta
        SQL = SQL & "   AND num_avaliacao = " & PAvaliacao
        SQL = SQL & "   AND situacao in ('t','r') "
    Else
        SQL = " SELECT "
        SQL = SQL & "       proposta_id "
        SQL = SQL & " FROM "
        SQL = SQL & "       avaliacao_restituicao_tb"
        SQL = SQL & " WHERE "
        SQL = SQL & "       proposta_id = " & PProposta
        SQL = SQL & "   AND num_avaliacao = " & PAvaliacao
        SQL = SQL & "   AND situacao in ('a','i') "
    End If

    Set rc = rdocn.OpenResultset(SQL)

    If Not rc.EOF Then
        VerificarExistenciaAvaliacaoRestituicaoPendente = True
    Else
        VerificarExistenciaAvaliacaoRestituicaoPendente = False
    End If

    rc.Close

    Exit Function

End Function


Sub Obter_Dados_ProLabore(ByVal PProposta, _
                          ByVal PDtReferencia, _
                          ByVal PValFinanceiro, _
                          ByVal PValIOF, _
                          ByVal PValCustoApoliceCertif, _
                          ByRef PValProLabore, _
                          ByRef PEstipulanteId, _
                          ByRef PNomeEstipulante, _
                          ByRef PPercProLabore)


    Dim SQL As String
    Dim Rst As rdoResultset

    Dim EstipulanteId As String
    Dim NomeEstipulante As String
    Dim PercProLabore As Currency
    Dim ValProLabore As Currency

    SQL = "SELECT a.est_cliente_id, "
    SQL = SQL & " isnull(a.perc_pro_labore,0) perc_pro_labore "
    SQL = SQL & " FROM administracao_apolice_tb a"
    SQL = SQL & " WHERE a.proposta_id = " & PProposta
    SQL = SQL & " and a.dt_inicio_administracao = (SELECT MIN(dt_inicio_administracao) " & vbNewLine
    SQL = SQL & "                                    FROM administracao_apolice_tb b " & vbNewLine
    SQL = SQL & "                                   WHERE a.Proposta_Id = b.Proposta_Id) " & vbNewLine

    Set Rst = rdocn.OpenResultset(SQL)

    If Not Rst.EOF Then
        'carsilva - 18/05/2004 - Fixar o percentual de pro-labore para o Ouro Empresarial em 2%
        If Produto_id = 111 Then
            EstipulanteId = Rst!Est_Cliente_ID
            PercProLabore = 2
        Else
            EstipulanteId = Rst!Est_Cliente_ID
            PercProLabore = Val(Rst!perc_pro_labore)
        End If
    Else
        EstipulanteId = 0
        PercProLabore = 0
    End If

    Rst.Close


    If Val(EstipulanteId) <> 0 Then

        'Obtendo Nome do Estipulante'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        SQL = "Select nome from cliente_tb where cliente_id = " & EstipulanteId

        Set Rst = rdocn.OpenResultset(SQL)

        If Not Rst.EOF Then
            NomeEstipulante = Rst!Nome
        Else
            NomeEstipulante = ""
        End If

        'Calculando Valor do ProLabore''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        ValProLabore = Truncadecimal((PValFinanceiro - PValIOF - PValCustoApoliceCertif) * (PercProLabore / 100))

    Else

        ValProLabore = 0
        EstipulanteId = 0
        NomeEstipulante = ""
        PercProLabore = 0

    End If

    'Retornando valores'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

    PValProLabore = ValProLabore
    PEstipulanteId = EstipulanteId
    PNomeEstipulante = NomeEstipulante
    PPercProLabore = PercProLabore

End Sub

Public Sub AprovarRestituicao(ByVal lPropostaId As Long, _
                              ByVal lNumAvaliacao As Long, _
                              ByVal lProdutoId As Long, _
                              ByVal sDtContratacao As String, _
                              ByVal sDtInicioVig As String, _
                              ByVal sDtFimVig As String, _
                              ByVal sDtPedidoAvaliacao As String)

'Projeto Restitui��o por Iniciativa da Seguradora - bcarneiro - 03/12/2003

    Const SITUACAO_PENDENTE_AVALIACAO_GERENCIAL = "T"

    Const COD_MOV_FINANC = 3  'Devolu��o

    Dim OEndosso As Object
    Dim oFinanceiro As Object

    Dim lEndossoId As Long
    Dim lRestEndossoId As Long

    Dim cValCustoApoliceCertifEst As Currency
    Dim cValAdicFracionamentoEst As Currency
    Dim cValDescontoEst As Currency
    Dim cValIOFEst As Currency
    Dim cValPremioTarifaEst As Currency
    Dim cValRestituicaoEst As Currency
    Dim cValTotCorretagemEst As Currency
    Dim cValProLaboreEst As Currency


    'mathayde 27/03/2009
    Dim cValRemuneracao As Currency
    Dim cValRemuneracaoEst As Currency



    Dim cValCustoApoliceCertifRest As Currency
    Dim cValAdicFracionamentoRest As Currency
    Dim cValDescontoRest As Currency
    Dim cValIOFRest As Currency
    Dim cValPremioTarifaRest As Currency
    Dim cValRestituicao As Currency
    Dim cValTotCorretagemRest As Currency
    Dim cValProLaboreRest As Currency

    Dim iTpEndossoId As Integer
    Dim lTpEndossoId_ALS As Integer
    Dim iOrigemProposta As Integer
    Dim lEventoId As Long
    Dim lClienteIDBeneficiario As Long

    Dim sTextoEndosso As String

    'Rmarins - 20/10/06
    Dim cValSubvencaoFederal As Currency
    Dim cValSubvencaoEstadual As Currency
    Dim cValSubvencaoFederalEst As Currency
    Dim cValSubvencaoEstadualEst As Currency
    Dim cValCambio As Currency
    Dim cValIsRestituicao As Currency
    '
    Dim colCliente As New Collection
    Dim oCliente As Cliente

    'HOLIVEIRA: Variavel que ira guardar a conexao DT: 05/02/2007
    Dim lConexao As Long

    'RSilva - Tipo de Cobertura
    Dim lTipoCobertura As Long
    Dim sSQL As String
    Dim rs As rdoResultset

    On Error GoTo TrataErro

    MousePointer = vbHourglass

    'Iniciando transa��o''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    rdocn.BeginTrans


    If bModular = True Or lProdutoId = 1168 Then
        sSQL = ""
        adSQL sSQL, "SELECT tp_cobertura_id"
        adSQL sSQL, "  From seguros_db.dbo.avaliacao_restituicao_habitacional_tb"
        adSQL sSQL, " WHERE proposta_id   = " & lPropostaId
        adSQL sSQL, "   AND num_avaliacao = " & lNumAvaliacao


        Set rs = rdocn.OpenResultset(sSQL)

        If Not rs.EOF Then
            lTipoCobertura = rs("tp_cobertura_id")
        Else
            lTipoCobertura = 0
        End If
        'If oDadosModular.iTpCobertura = 526 Then
        '    lTipoCobertura = 527
        'Else
        '    lTipoCobertura = 526
        'End If
    Else
        lTipoCobertura = 0
    End If



    'Obtendo dados da avalia��o t�cnica aprovada''''''''''''''''''''''''''''''''''''''''''''''''
    Call FrmSelEndosso.ObterValoresAvaliacaoRestituicao(lPropostaId, _
                                                        lNumAvaliacao, _
                                                        SITUACAO_PENDENTE_AVALIACAO_GERENCIAL, _
                                                        cValRestituicao, _
                                                        cValIOFRest, _
                                                        cValCustoApoliceCertifRest, _
                                                        cValAdicFracionamentoRest, _
                                                        cValDescontoRest, _
                                                        cValPremioTarifaRest, _
                                                        cValTotCorretagemRest, _
                                                        cValProLaboreRest, _
                                                        cValCustoApoliceCertifEst, _
                                                        cValAdicFracionamentoEst, _
                                                        cValDescontoEst, _
                                                        cValIOFEst, _
                                                        cValPremioTarifaEst, _
                                                        cValRestituicaoEst, _
                                                        cValTotCorretagemEst, _
                                                        cValProLaboreEst, _
                                                        lRestEndossoId, _
                                                        cValCambio, _
                                                        sTextoEndosso, _
                                                        cValSubvencaoFederal, cValSubvencaoEstadual, _
                                                        cValSubvencaoFederalEst, cValSubvencaoEstadualEst, _
                                                        cValRemuneracao, cValRemuneracaoEst)
    'Mathayde 30/03/2009 - Obtem os valores de cValRemuneracao e cValRemuneracaoEst

    '    If lProdutoId = 1168 Then
    '        cValRestituicao = cValRestituicao + oDadosModular.ValRestituicao
    '        cValIOFRest = cValIOFRest + oDadosModular.Val_IOF_Restituicao
    '        cValCustoApoliceCertifRest = ""
    '        cValAdicFracionamentoRest = ""
    '        cValDescontoRest = ""
    '        cValPremioTarifaRest = ""
    '        cValTotCorretagemRest = ""
    '        cValProLaboreRest = ""
    '        cValCustoApoliceCertifEst = ""
    '        cValAdicFracionamentoEst = ""
    '        cValDescontoEst = ""
    '        cValIOFEst = ""
    '        cValPremioTarifaEst = ""
    '        cValRestituicaoEst = ""
    '        cValTotCorretagemEst = ""
    '        cValProLaboreEst = ""
    '        lRestEndossoId = ""
    '    End If

    'Obtendo dados da Proposta '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    Call Obter_Premio_IS(lPropostaId, lProdutoId, cValIsRestituicao, 0)

    'Calculando os valores com subven��o por Cliente - Rmarins 20/10/2006 ''''''''''''''''''''''
    If gsPossuiSubvencao = "S" Then

        If cValSubvencaoFederal <> 0 Then
            Call CalcularValoresRestituicaoCliente("FEDERAL", _
                                                   lProdutoId, _
                                                   cValRestituicao, _
                                                   cValIOFRest, _
                                                   cValCustoApoliceCertifRest, _
                                                   cValAdicFracionamentoRest, _
                                                   cValDescontoRest, _
                                                   cValPremioTarifaRest, _
                                                   cValTotCorretagemRest, _
                                                   cValProLaboreRest, _
                                                   cValCustoApoliceCertifEst, _
                                                   cValAdicFracionamentoEst, _
                                                   cValDescontoEst, _
                                                   cValIOFEst, _
                                                   cValPremioTarifaEst, _
                                                   cValRestituicaoEst, _
                                                   cValTotCorretagemEst, _
                                                   cValProLaboreEst, _
                                                   cValIsRestituicao, _
                                                   cValSubvencaoFederal, _
                                                   cValSubvencaoEstadual, _
                                                   colCliente)


        End If

        'bcarneiro - 06/12/2006 - Retirado temporariamente enquanto n�o � definido o endosso
        '                         para restitui��o estadual
        'bmoraes 08.03.2009 - restitui��o estadual
        If cValSubvencaoEstadual <> 0 Then
            Call CalcularValoresRestituicaoCliente("ESTADUAL", _
                                                   lProdutoId, _
                                                   cValRestituicao, _
                                                   cValIOFRest, _
                                                   cValCustoApoliceCertifRest, _
                                                   cValAdicFracionamentoRest, _
                                                   cValDescontoRest, _
                                                   cValPremioTarifaRest, _
                                                   cValTotCorretagemRest, _
                                                   cValProLaboreRest, _
                                                   cValCustoApoliceCertifEst, _
                                                   cValAdicFracionamentoEst, _
                                                   cValDescontoEst, _
                                                   cValIOFEst, _
                                                   cValPremioTarifaEst, _
                                                   cValRestituicaoEst, _
                                                   cValTotCorretagemEst, _
                                                   cValProLaboreEst, _
                                                   cValIsRestituicao, _
                                                   cValSubvencaoFederal, _
                                                   cValSubvencaoEstadual, _
                                                   colCliente)

        End If
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

    End If

    'Obtendo dados da proposta''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    lClienteIDBeneficiario = Obter_Beneficiario(lPropostaId)

    Call CalcularValoresRestituicaoCliente("CLIENTE", _
                                           lProdutoId, _
                                           cValRestituicao, _
                                           cValIOFRest, _
                                           cValCustoApoliceCertifRest, _
                                           cValAdicFracionamentoRest, _
                                           cValDescontoRest, _
                                           cValPremioTarifaRest, _
                                           cValTotCorretagemRest, _
                                           cValProLaboreRest, _
                                           cValCustoApoliceCertifEst, _
                                           cValAdicFracionamentoEst, _
                                           cValDescontoEst, _
                                           cValIOFEst, _
                                           cValPremioTarifaEst, _
                                           cValRestituicaoEst, _
                                           cValTotCorretagemEst, _
                                           cValProLaboreEst, _
                                           cValIsRestituicao, _
                                           cValSubvencaoFederal, _
                                           cValSubvencaoEstadual, _
                                           colCliente, _
                                           lClienteIDBeneficiario)


    'HOLIVEIRA: Cria o objeto neste momento DT: 05/02/2007

    ' C_ANEVES - 26/08/2013 - Demanda: 17860335 - Inicio
    Set OEndosso = CreateObject("SEGL0026.cls00124")
    'Set OEndosso = CreateObject("SEGL0355.cls00626")
    ' C_ANEVES - 26/08/2013 - Demanda: 17860335 - Fim

    'HOLIVEIRA: Abre a conexao do objeto oEndosso DT: 05/02/2007
    lConexao = OEndosso.AbrirConexao("SEGBR", glAmbiente_id, App.Title, App.FileDescription)

    'HOLIVEIRA: Abre a transa��o do objeto oEndosso DT: 27/12/2006
    If Not OEndosso.AbrirTransacao(lConexao) Then GoTo TrataErro

    For Each oCliente In colCliente

        'Emitindo o Endosso por Cliente - Rmarins - 20/10/06
        Call ProcessarEmissaoEndosso(lPropostaId, _
                                     lProdutoId, _
                                     lNumAvaliacao, _
                                     oCliente.Cliente_Id, _
                                     lRestEndossoId, _
                                     oCliente.Tp_Cliente, _
                                     oCliente.Val_Restituicao, _
                                     oCliente.Val_IOF_Rest, _
                                     oCliente.Val_Custo_Apolice_Rest, _
                                     oCliente.Val_Tot_Corretagem_Rest, _
                                     oCliente.Val_Pro_Labore_Rest, _
                                     oCliente.Val_Premio_Tarifa_Rest, _
                                     oCliente.Val_Desconto_Rest, _
                                     oCliente.Val_Adic_Frac_Rest, _
                                     oCliente.Val_IS_Restituicao, _
                                     cValCambio, _
                                     sDtPedidoAvaliacao, _
                                     sTextoEndosso, _
                                     iOrigemProposta, _
                                     oCliente.iTpEndossoId, _
                                     lEndossoId, _
                                     lConexao, _
                                     OEndosso, _
                                     cValRemuneracao)
        'Mathayde 30/03/2009 - Envia cValRemuneracao para calcular o cValIrRemuneracao


        If oCliente.Tp_Cliente = "CLIENTE" Or oCliente.Tp_Cliente = "FEDERAL" Or oCliente.Tp_Cliente = "ESTADUAL" Then

            If iOrigemProposta = 2 Then  'ALS

                lTpEndossoId_ALS = ObterTp_Endosso(lPropostaId, lEndossoId, lEventoId)    'crosendo - Altera��o realizada em 11/08/2006

                'HOLIVEIRA: Cria o objeto antes DT: 05/02/2007
                'Set oEndosso = CreateObject("SEGL0026.cls00124")

                Select Case lEventoId    'Rmarins 20/10/06 - Altera��o p/ gera��o de endosso por Evento

                Case 3801

                    Call OEndosso.GerarEndossoAls(cUserName, _
                                                  gsSIGLASISTEMA, _
                                                  App.Title, _
                                                  App.FileDescription, _
                                                  glAmbiente_id, _
                                                  Data_Sistema, _
                                                  lPropostaId, _
                                                  Format(Data_Sistema, "yyyymm"), _
                                                  sDtPedidoAvaliacao, _
                                                  lEndossoId, _
                                                  oCliente.Val_Restituicao, _
                                                  lTpEndossoId_ALS, _
                                                  COD_MOV_FINANC, _
                                                  0, _
                                                  lEventoId, _
                       , _
                                                  lConexao)    'HOLIVEIRA: Passa lConexao como parametro DT: 05/02/2007

                Case 3804

                    Call OEndosso.GerarEndossoAls(cUserName, _
                                                  gsSIGLASISTEMA, _
                                                  App.Title, _
                                                  App.FileDescription, _
                                                  glAmbiente_id, _
                                                  Data_Sistema, _
                                                  lPropostaId, _
                                                  Format(Data_Sistema, "yyyymm"), _
                                                  sDtPedidoAvaliacao, _
                                                  lEndossoId, _
                                                  oCliente.Val_Restituicao, _
                                                  lTpEndossoId_ALS, _
                                                  COD_MOV_FINANC, _
                                                  0, _
                                                  lEventoId, _
                       , _
                                                  lConexao)    'HOLIVEIRA: Passa lConexao como parametro DT: 05/02/2007

                Case 3800

                    Call OEndosso.AtualizarEndossoAls(cUserName, _
                                                      gsSIGLASISTEMA, _
                                                      App.Title, _
                                                      App.FileDescription, _
                                                      glAmbiente_id, _
                                                      lPropostaId, _
                                                      Abs(oCliente.Val_Restituicao), _
                                                      lEventoId, _
                                                      "", _
                                                      lConexao)    'HOLIVEIRA: Passa lConexao como parametro DT: 05/02/2007

                    'RodriSilva - 21/12/2009
                Case 3812
                    Call OEndosso.GerarEndossoAls(cUserName, _
                                                  gsSIGLASISTEMA, _
                                                  App.Title, _
                                                  App.FileDescription, _
                                                  glAmbiente_id, _
                                                  Data_Sistema, _
                                                  lPropostaId, _
                                                  Format(Data_Sistema, "yyyymm"), _
                                                  sDtPedidoAvaliacao, _
                                                  lEndossoId, _
                                                  oCliente.Val_Restituicao, _
                                                  lTpEndossoId_ALS, _
                                                  COD_MOV_FINANC, _
                                                  0, _
                                                  lEventoId, _
                       , _
                                                  lConexao, lTipoCobertura)

                End Select

            End If

            If oCliente.Tp_Cliente = "CLIENTE" Then

                'Aprovando avalia��o t�cnica''''''''''''''''''''''''''''''''''''''''''''''''''''''
                If bModular = False Then
                    Call AprovarAvaliacaoRestituicao(lPropostaId, lNumAvaliacao, lEndossoId, lConexao, OEndosso, cValRemuneracao)    'HOLIVEIRA: Passa lConexao e o objeto oEndosso como parametro DT: 05/02/2007
                    'Mathayde 30/03/2009 - Envia cValRemuneracao e passa para AtualizarAvaliacaoRestituicao para gravar em avaliacao_restituicao_tb
                Else
                    If lProdutoId = 1168 Then
                        If lNumAvaliacao = oDadosModular.Num_avaliacao Then
                            'HOLIVEIRA: Passa lConexao e o objeto oEndosso como parametro DT: 05/02/2007
                            'Mathayde 30/03/2009 - Envia cValRemuneracao e passa para AtualizarAvaliacaoRestituicao para gravar em avaliacao_restituicao_tb
                            Call AprovarAvaliacaoRestituicao(lPropostaId, oDadosModular.Num_avaliacao, lEndossoId, lConexao, OEndosso, cValRemuneracao)
                        Else
                            Call AprovarAvaliacaoRestituicao(lPropostaId, lNumAvaliacao, lEndossoId, lConexao, OEndosso, cValRemuneracao)    'HOLIVEIRA: Passa lConexao e o objeto oEndosso como parametro DT: 05/02/2007
                        End If
                    Else
                        If lPropostaId = oDadosModular.proposta_id Then
                            Call AprovarAvaliacaoRestituicao(lPropostaId, oDadosModular.Num_avaliacao, lEndossoId, lConexao, OEndosso, cValRemuneracao)
                        Else
                            Call AprovarAvaliacaoRestituicao(lPropostaId, lNumAvaliacao, lEndossoId, lConexao, OEndosso, cValRemuneracao)    'HOLIVEIRA: Passa lConexao e o objeto oEndosso como parametro DT: 05/02/2007
                        End If
                    End If
                End If

                'Inserindo em evento_emissao_tb '''''''''''''''''''''''''''''''''''''''''''''''''''
                '                If bModular = False Then
                Call InserirEvento(lPropostaId, CInt(lProdutoId), Val(sRamoId), lEndossoId, lTpEndossoId_ALS)
                '               Else
                '                  Call InserirEvento(lPropostaId, CInt(lProdutoId), oDadosModular.ramo_id, lEndossoId, lTpEndossoId_ALS)
                '             End If

            End If

        End If


        '(in�cio) -- rsouza -- 03/03/2011 ---------------------------------------'
        Call OEndosso.Insere_Tabelas_Circular(cUserName, _
                                              gsSIGLASISTEMA, _
                                              App.Title, _
                                              App.FileDescription, _
                                              glAmbiente_id, _
                                              lPropostaId, lEndossoId, lConexao)
        '(fim)    -- rsouza -- 03/03/2011 ---------------------------------------'

    Next

    'Desbloqueando proposta'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    Call Atualizar_Bloqueio_Proposta(lPropostaId, "n")



    'Finalizando transa��o''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    rdocn.CommitTrans
    OEndosso.ConfirmarTransacao (lConexao)    'HOLIVEIRA: Confirma a transa��o DT: 05/02/2007
    OEndosso.FecharConexao (lConexao)    'HOLIVEIRA: Fecha a conexao DT: 05/02/2007

    '''''    rdocn.RollbackTrans
    '''''    oEndosso.RetornarTransacao (lConexao) 'HOLIVEIRA: Desfaz a transa��o DT: 05/02/2007
    '''''    oEndosso.FecharConexao (lConexao) 'HOLIVEIRA: Fecha a conexao DT: 05/02/2007

    'Destruindo objetos
    Set OEndosso = Nothing
    MousePointer = vbDefault

    Exit Sub
TrataErro:

    rdocn.RollbackTrans
    OEndosso.RetornarTransacao (lConexao)    'HOLIVEIRA: Desfaz a transa��o DT: 05/02/2007
    OEndosso.FecharConexao (lConexao)    'HOLIVEIRA: Fecha a conexao DT: 05/02/2007

    'Destruindo objetos
    Set OEndosso = Nothing

    Call TrataErroGeral("AprovarRestituicao")
    Call TerminaSEGBR

End Sub

Public Sub AprovarRestituicaoModular(ByVal lPropostaId As Long, _
                                     ByVal lNumAvaliacao As Long, _
                                     ByVal lProdutoId As Long, _
                                     ByVal sDtContratacao As String, _
                                     ByVal sDtInicioVig As String, _
                                     ByVal sDtFimVig As String, _
                                     ByVal sDtPedidoAvaliacao As String)

'Projeto Restitui��o por Iniciativa da Seguradora - bcarneiro - 03/12/2003

    Const SITUACAO_PENDENTE_AVALIACAO_GERENCIAL = "T"

    Const COD_MOV_FINANC = 3  'Devolu��o

    Dim OEndosso As Object
    Dim oFinanceiro As Object

    Dim lEndossoId As Long
    Dim lRestEndossoId As Long

    Dim cValCustoApoliceCertifEst As Currency
    Dim cValAdicFracionamentoEst As Currency
    Dim cValDescontoEst As Currency
    Dim cValIOFEst As Currency
    Dim cValPremioTarifaEst As Currency
    Dim cValRestituicaoEst As Currency
    Dim cValTotCorretagemEst As Currency
    Dim cValProLaboreEst As Currency


    'mathayde 27/03/2009
    Dim cValRemuneracao As Currency
    Dim cValRemuneracaoEst As Currency



    Dim cValCustoApoliceCertifRest As Currency
    Dim cValAdicFracionamentoRest As Currency
    Dim cValDescontoRest As Currency
    Dim cValIOFRest As Currency
    Dim cValPremioTarifaRest As Currency
    Dim cValRestituicao As Currency
    Dim cValTotCorretagemRest As Currency
    Dim cValProLaboreRest As Currency

    Dim iTpEndossoId As Integer
    Dim lTpEndossoId_ALS As Integer
    Dim iOrigemProposta As Integer
    Dim lEventoId As Long
    Dim lClienteIDBeneficiario As Long

    Dim sTextoEndosso As String

    'Rmarins - 20/10/06
    Dim cValSubvencaoFederal As Currency
    Dim cValSubvencaoEstadual As Currency
    Dim cValSubvencaoFederalEst As Currency
    Dim cValSubvencaoEstadualEst As Currency
    Dim cValCambio As Currency
    Dim cValIsRestituicao As Currency
    '
    Dim colCliente As New Collection
    Dim oCliente As Cliente

    'HOLIVEIRA: Variavel que ira guardar a conexao DT: 05/02/2007
    Dim lConexao As Long

    Dim lTipoCobertura As Long

    On Error GoTo TrataErro

    MousePointer = vbHourglass

    'Iniciando transa��o''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    rdocn.BeginTrans

    If Produto_id = 1168 Then
        lTipoCobertura = oDadosModular.iTpCobertura
    Else
        lTipoCobertura = 0
    End If
    '
    'lEndossoId = oDadosModular.EndossoId
    'Obtendo dados da avalia��o t�cnica aprovada''''''''''''''''''''''''''''''''''''''''''''''''
    Call FrmSelEndosso.ObterValoresAvaliacaoRestituicao(lPropostaId, _
                                                        lNumAvaliacao, _
                                                        SITUACAO_PENDENTE_AVALIACAO_GERENCIAL, _
                                                        cValRestituicao, _
                                                        cValIOFRest, _
                                                        cValCustoApoliceCertifRest, _
                                                        cValAdicFracionamentoRest, _
                                                        cValDescontoRest, _
                                                        cValPremioTarifaRest, _
                                                        cValTotCorretagemRest, _
                                                        cValProLaboreRest, _
                                                        cValCustoApoliceCertifEst, _
                                                        cValAdicFracionamentoEst, _
                                                        cValDescontoEst, _
                                                        cValIOFEst, _
                                                        cValPremioTarifaEst, _
                                                        cValRestituicaoEst, _
                                                        cValTotCorretagemEst, _
                                                        cValProLaboreEst, _
                                                        lRestEndossoId, _
                                                        cValCambio, _
                                                        sTextoEndosso, _
                                                        cValSubvencaoFederal, cValSubvencaoEstadual, _
                                                        cValSubvencaoFederalEst, cValSubvencaoEstadualEst, _
                                                        cValRemuneracao, cValRemuneracaoEst)
    'Mathayde 30/03/2009 - Obtem os valores de cValRemuneracao e cValRemuneracaoEst


    'Obtendo dados da Proposta '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    Call Obter_Premio_IS(lPropostaId, lProdutoId, cValIsRestituicao, 0)

    'Obtendo dados da proposta''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    '    lClienteIDBeneficiario = Obter_Beneficiario(lPropostaId)
    lClienteIDBeneficiario = Obter_Beneficiario(oDadosModular.proposta_id)

    Call CalcularValoresRestituicaoCliente("CLIENTE", _
                                           lProdutoId, _
                                           oDadosModular.ValRestituicao, _
                                           oDadosModular.ValIOFRest, _
                                           oDadosModular.ValCustoApoliceCertifRest, _
                                           oDadosModular.ValAdicFracionamentoRest, _
                                           oDadosModular.ValDescontoRest, _
                                           oDadosModular.ValPremioTarifaRest, _
                                           oDadosModular.ValTotCorretagemRest, _
                                           oDadosModular.ValProLaboreRest, _
                                           oDadosModular.ValCustoApoliceCertifEst, _
                                           oDadosModular.ValAdicFracionamentoEst, _
                                           oDadosModular.ValDescontoEst, _
                                           oDadosModular.ValIOFEst, _
                                           oDadosModular.ValPremioTarifaEst, _
                                           oDadosModular.ValRestituicaoEst, _
                                           oDadosModular.Val_Tot_Corretagem_Est, _
                                           oDadosModular.Val_Pro_Lab_Est, _
                                           oDadosModular.val_is, _
                                           0, _
                                           0, _
                                           colCliente, _
                                           lClienteIDBeneficiario)


    'HOLIVEIRA: Cria o objeto neste momento DT: 05/02/2007
    ' C_ANEVES - 26/08/2013 - Demanda: 17860335 - Inicio
    Set OEndosso = CreateObject("SEGL0026.cls00124")
    'Set OEndosso = CreateObject("SEGL0355.cls00626")
    ' C_ANEVES - 26/08/2013 - Demanda: 17860335 - Fim

    'HOLIVEIRA: Abre a conexao do objeto oEndosso DT: 05/02/2007
    lConexao = OEndosso.AbrirConexao("SEGBR", glAmbiente_id, App.Title, App.FileDescription)

    'HOLIVEIRA: Abre a transa��o do objeto oEndosso DT: 27/12/2006
    If Not OEndosso.AbrirTransacao(lConexao) Then GoTo TrataErro

    For Each oCliente In colCliente

        'Emitindo o Endosso por Cliente - Rmarins - 20/10/06
        Call ProcessarEmissaoEndosso(lPropostaId, _
                                     lProdutoId, _
                                     lNumAvaliacao, _
                                     oCliente.Cliente_Id, _
                                     lRestEndossoId, _
                                     oCliente.Tp_Cliente, _
                                     oCliente.Val_Restituicao, _
                                     oCliente.Val_IOF_Rest, _
                                     oCliente.Val_Custo_Apolice_Rest, _
                                     oCliente.Val_Tot_Corretagem_Rest, _
                                     oCliente.Val_Pro_Labore_Rest, _
                                     oCliente.Val_Premio_Tarifa_Rest, _
                                     oCliente.Val_Desconto_Rest, _
                                     oCliente.Val_Adic_Frac_Rest, _
                                     oCliente.Val_IS_Restituicao, _
                                     cValCambio, _
                                     sDtPedidoAvaliacao, _
                                     sTextoEndosso, _
                                     iOrigemProposta, _
                                     oCliente.iTpEndossoId, _
                                     oDadosModular.EndossoId, _
                                     lConexao, _
                                     OEndosso, _
                                     cValRemuneracao)
        'Mathayde 30/03/2009 - Envia cValRemuneracao para calcular o cValIrRemuneracao


        If oCliente.Tp_Cliente = "CLIENTE" Or oCliente.Tp_Cliente = "FEDERAL" Or oCliente.Tp_Cliente = "ESTADUAL" Then

            If iOrigemProposta = 2 Then  'ALS

                lTpEndossoId_ALS = ObterTp_Endosso(lPropostaId, lNumAvaliacao, lEventoId)    'crosendo - Altera��o realizada em 11/08/2006

                'HOLIVEIRA: Cria o objeto antes DT: 05/02/2007
                'Set oEndosso = CreateObject("SEGL0026.cls00124")

                Select Case lEventoId    'Rmarins 20/10/06 - Altera��o p/ gera��o de endosso por Evento

                Case 3801

                    Call OEndosso.GerarEndossoAls(cUserName, _
                                                  gsSIGLASISTEMA, _
                                                  App.Title, _
                                                  App.FileDescription, _
                                                  glAmbiente_id, _
                                                  Data_Sistema, _
                                                  lPropostaId, _
                                                  Format(Data_Sistema, "yyyymm"), _
                                                  sDtPedidoAvaliacao, _
                                                  lEndossoId, _
                                                  oCliente.Val_Restituicao, _
                                                  lTpEndossoId_ALS, _
                                                  COD_MOV_FINANC, _
                                                  0, _
                                                  lEventoId, _
                       , _
                                                  lConexao)    'HOLIVEIRA: Passa lConexao como parametro DT: 05/02/2007

                Case 3804

                    Call OEndosso.GerarEndossoAls(cUserName, _
                                                  gsSIGLASISTEMA, _
                                                  App.Title, _
                                                  App.FileDescription, _
                                                  glAmbiente_id, _
                                                  Data_Sistema, _
                                                  lPropostaId, _
                                                  Format(Data_Sistema, "yyyymm"), _
                                                  sDtPedidoAvaliacao, _
                                                  lEndossoId, _
                                                  oCliente.Val_Restituicao, _
                                                  lTpEndossoId_ALS, _
                                                  COD_MOV_FINANC, _
                                                  0, _
                                                  lEventoId, _
                       , _
                                                  lConexao)    'HOLIVEIRA: Passa lConexao como parametro DT: 05/02/2007

                Case 3800

                    Call OEndosso.AtualizarEndossoAls(cUserName, _
                                                      gsSIGLASISTEMA, _
                                                      App.Title, _
                                                      App.FileDescription, _
                                                      glAmbiente_id, _
                                                      lPropostaId, _
                                                      Abs(oCliente.Val_Restituicao), _
                                                      lEventoId, _
                                                      "", _
                                                      lConexao)    'HOLIVEIRA: Passa lConexao como parametro DT: 05/02/2007

                Case 3812


                    Call OEndosso.GerarEndossoAls(cUserName, _
                                                  gsSIGLASISTEMA, _
                                                  App.Title, _
                                                  App.FileDescription, _
                                                  glAmbiente_id, _
                                                  Data_Sistema, _
                                                  lPropostaId, _
                                                  Format(Data_Sistema, "yyyymm"), _
                                                  sDtPedidoAvaliacao, _
                                                  lEndossoId, _
                                                  oCliente.Val_Restituicao, _
                                                  lTpEndossoId_ALS, _
                                                  COD_MOV_FINANC, _
                                                  0, _
                                                  lEventoId, _
                       , _
                                                  lConexao, lTipoCobertura)

                End Select

            End If

            If oCliente.Tp_Cliente = "CLIENTE" Then

                'Aprovando avalia��o t�cnica''''''''''''''''''''''''''''''''''''''''''''''''''''''
                Call AprovarAvaliacaoRestituicao(lPropostaId, lNumAvaliacao, oDadosModular.Num_avaliacao, lConexao, OEndosso, cValRemuneracao)    'HOLIVEIRA: Passa lConexao e o objeto oEndosso como parametro DT: 05/02/2007
                'Mathayde 30/03/2009 - Envia cValRemuneracao e passa para AtualizarAvaliacaoRestituicao para gravar em avaliacao_restituicao_tb

                'Inserindo em evento_emissao_tb '''''''''''''''''''''''''''''''''''''''''''''''''''
                Call InserirEvento(lPropostaId, CInt(lProdutoId), Val(oDadosModular.ramo_id), oDadosModular.EndossoId, lTpEndossoId_ALS)
            End If
        End If
    Next

    'Desbloqueando proposta'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    Call Atualizar_Bloqueio_Proposta(lPropostaId, "n")

    'Finalizando transa��o''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    rdocn.CommitTrans
    OEndosso.ConfirmarTransacao (lConexao)    'HOLIVEIRA: Confirma a transa��o DT: 05/02/2007
    OEndosso.FecharConexao (lConexao)    '     HOLIVEIRA: Fecha a conexao DT: 05/02/2007

    '' rdocn.RollbackTrans
    '' oEndosso.RetornarTransacao (lConexao) 'HOLIVEIRA: Desfaz a transa��o DT: 05/02/2007
    '' oEndosso.FecharConexao (lConexao) 'HOLIVEIRA: Fecha a conexao DT: 05/02/2007

    'Destruindo objetos
    Set OEndosso = Nothing
    MousePointer = vbDefault

    Exit Sub
TrataErro:

    rdocn.RollbackTrans
    OEndosso.RetornarTransacao (lConexao)    'HOLIVEIRA: Desfaz a transa��o DT: 05/02/2007
    OEndosso.FecharConexao (lConexao)    'HOLIVEIRA: Fecha a conexao DT: 05/02/2007

    'Destruindo objetos
    Set OEndosso = Nothing

    Call TrataErroGeral("AprovarRestituicao")
    Call TerminaSEGBR

End Sub


Public Function ObterClienteSubvencao(ByVal lProdutoId As Long) As Object

    Dim sSQL As String
    Dim oRs As rdoResultset
    On Error GoTo TrataErro

    sSQL = ""
    adSQL sSQL, "SELECT ISNULL(cliente_subvencao_federal_id, 0) as cliente_subvencao_federal_id, "
    adSQL sSQL, "       ISNULL(cliente_subvencao_estadual_id, 0) as cliente_subvencao_estadual_id "
    adSQL sSQL, "  FROM produto_tb with(NOLOCK)"
    adSQL sSQL, " WHERE produto_Id = " & lProdutoId

    Set oRs = rdocn.OpenResultset(sSQL)

    If Not oRs.EOF Then
        Set ObterClienteSubvencao = oRs
    End If

    Set oRs = Nothing

    Exit Function

TrataErro:
    Call Err.Raise(Err.Number, , "ObterClienteSubvencao - " & Err.Description)
End Function



Public Sub CalcularValoresRestituicaoCliente(ByVal sFlagSubvencao As String, _
                                             ByVal lProdutoId As Long, _
                                             ByVal cValRestituicao As Currency, _
                                             ByVal cValIOFRest As Currency, _
                                             ByVal cValCustoApoliceCertifRest As Currency, _
                                             ByVal cValAdicFracionamentoRest As Currency, _
                                             ByVal cValDescontoRest As Currency, _
                                             ByVal cValPremioTarifaRest As Currency, _
                                             ByVal cValTotCorretagemRest As Currency, _
                                             ByVal cValProLaboreRest As Currency, _
                                             ByVal cValCustoApoliceCertifEst As Currency, _
                                             ByVal cValAdicFracionamentoEst As Currency, _
                                             ByVal cValDescontoEst As Currency, _
                                             ByVal cValIOFEst As Currency, _
                                             ByVal cValPremioTarifaEst As Currency, _
                                             ByVal cValRestituicaoEst As Currency, _
                                             ByVal cValTotCorretagemEst As Currency, _
                                             ByVal cValProLaboreEst As Currency, _
                                             ByVal cValIsRestituicao As Currency, _
                                             ByVal cValSubvencaoFederal As Currency, _
                                             ByVal cValSubvencaoEstadual As Currency, _
                                             ByRef colCliente As Collection, _
                                             Optional lClienteId As Long)


'## Calcula os valores de restitui��o para cada cliente - Rmarins 20/10/06

    Dim oRs As rdoResultset
    Dim oCliente As New Cliente

    Dim cValRestituicaoAux As Currency
    Dim dPercCli As Double

    On Error GoTo TrataErro

    'Obtendo a informa��o do Cliente '''''''''''''''''''''''''''''''''''''''''''''''''''
    If sFlagSubvencao = "CLIENTE" Then
        oCliente.Cliente_Id = lClienteId
        oCliente.iTpEndossoId = 0
    Else

        Set oRs = ObterClienteSubvencao(lProdutoId)

        If Not oRs.EOF Then

            If sFlagSubvencao = "FEDERAL" Then
                oCliente.Cliente_Id = oRs("cliente_subvencao_federal_id")
                oCliente.iTpEndossoId = 269

                'bmoraes 08.03.2009 - estadual
            Else
                oCliente.Cliente_Id = oRs("cliente_subvencao_estadual_id")
                oCliente.iTpEndossoId = 269
                ''''''''''''''''''''''''''''''''''''''
            End If

            Set oRs = Nothing

        End If

    End If

    oCliente.Tp_Cliente = sFlagSubvencao

    'Encontrando o Fator para c�lculo dos valores ''''''''''''''''''''''''''''''''''''''''
    If sFlagSubvencao = "CLIENTE" Then

        cValRestituicaoAux = cValRestituicao - cValSubvencaoFederal - cValSubvencaoEstadual
        dPercCli = FrmSelEndosso.CalcularPercValor(cValPremioTarifaRest, cValPremioTarifaRest - cValSubvencaoFederal - cValSubvencaoEstadual)

    ElseIf sFlagSubvencao = "FEDERAL" Then

        dPercCli = FrmSelEndosso.CalcularPercValor(cValPremioTarifaRest, cValSubvencaoFederal)

    Else

        dPercCli = FrmSelEndosso.CalcularPercValor(cValPremioTarifaRest, cValSubvencaoEstadual)

    End If

    'Calculando os valores '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    If sFlagSubvencao = "CLIENTE" Then
        oCliente.Val_Restituicao = cValRestituicaoAux
        oCliente.Val_IOF_Rest = cValIOFRest
        oCliente.Val_Custo_Apolice_Rest = cValCustoApoliceCertifRest
        oCliente.Val_Adic_Frac_Rest = cValAdicFracionamentoRest
        oCliente.Val_Desconto_Rest = cValDescontoRest
        oCliente.Val_Premio_Tarifa_Rest = Truncadecimal((cValPremioTarifaRest / 100) * dPercCli)
        oCliente.Val_Tot_Corretagem_Rest = Truncadecimal((cValTotCorretagemRest / 100) * dPercCli)
        oCliente.Val_Pro_Labore_Rest = Truncadecimal((cValProLaboreRest / 100) * dPercCli)

        oCliente.Val_Restituicao_Est = cValRestituicaoAux
        oCliente.Val_IOF_Est = cValIOFEst
        oCliente.Val_Custo_Apolice_Est = cValCustoApoliceCertifEst
        oCliente.Val_Adic_Frac_Est = cValAdicFracionamentoEst
        oCliente.Val_Desconto_Est = cValDescontoEst
        oCliente.Val_Premio_Tarifa_Est = Truncadecimal((cValPremioTarifaEst / 100) * dPercCli)
        oCliente.Val_Tot_Corretagem_Est = Truncadecimal((cValTotCorretagemEst / 100) * dPercCli)
        oCliente.Val_Pro_Labore_Est = Truncadecimal((cValProLaboreEst / 100) * dPercCli)
        oCliente.Val_IS_Restituicao = cValIsRestituicao
    Else

        If sFlagSubvencao = "FEDERAL" Then
            oCliente.Val_Restituicao = cValSubvencaoFederal
            oCliente.Val_Premio_Tarifa_Rest = cValSubvencaoFederal
            oCliente.Val_Restituicao_Est = cValSubvencaoFederal
            oCliente.Val_Premio_Tarifa_Est = cValSubvencaoFederal
        Else
            oCliente.Val_Restituicao = cValSubvencaoEstadual
            oCliente.Val_Premio_Tarifa_Rest = cValSubvencaoEstadual
            oCliente.Val_Restituicao_Est = cValSubvencaoEstadual
            oCliente.Val_Premio_Tarifa_Est = cValSubvencaoEstadual
        End If

        oCliente.Val_IOF_Rest = 0
        oCliente.Val_Custo_Apolice_Rest = 0
        oCliente.Val_Adic_Frac_Rest = 0
        oCliente.Val_Desconto_Rest = 0
        oCliente.Val_Tot_Corretagem_Rest = Truncadecimal((cValTotCorretagemRest / 100) * dPercCli)
        oCliente.Val_Pro_Labore_Rest = Truncadecimal((cValProLaboreRest / 100) * dPercCli)

        oCliente.Val_IOF_Est = 0
        oCliente.Val_Custo_Apolice_Est = 0
        oCliente.Val_Adic_Frac_Est = 0
        oCliente.Val_Desconto_Est = 0
        oCliente.Val_Tot_Corretagem_Est = Truncadecimal((cValTotCorretagemEst / 100) * dPercCli)
        oCliente.Val_Pro_Labore_Est = Truncadecimal((cValProLaboreEst / 100) * dPercCli)

        oCliente.Val_IS_Restituicao = 0
    End If

    colCliente.Add oCliente

    'Destruindo objeto
    Set oCliente = Nothing

    Exit Sub

TrataErro:
    Call Err.Raise(Err.Number, , "CalcularValoresRestituicaoCliente - " & Err.Description)

End Sub
Public Sub CalcularValoresRestituicaoClienteModular(ByVal sFlagSubvencao As String, _
                                                    ByVal lProdutoId As Long, _
                                                    ByVal cValRestituicao As Currency, _
                                                    ByVal cValIOFRest As Currency, _
                                                    ByVal cValCustoApoliceCertifRest As Currency, _
                                                    ByVal cValAdicFracionamentoRest As Currency, _
                                                    ByVal cValDescontoRest As Currency, _
                                                    ByVal cValPremioTarifaRest As Currency, _
                                                    ByVal cValTotCorretagemRest As Currency, _
                                                    ByVal cValProLaboreRest As Currency, _
                                                    ByVal cValCustoApoliceCertifEst As Currency, _
                                                    ByVal cValAdicFracionamentoEst As Currency, _
                                                    ByVal cValDescontoEst As Currency, _
                                                    ByVal cValIOFEst As Currency, _
                                                    ByVal cValPremioTarifaEst As Currency, _
                                                    ByVal cValRestituicaoEst As Currency, _
                                                    ByVal cValTotCorretagemEst As Currency, _
                                                    ByVal cValProLaboreEst As Currency, _
                                                    ByVal cValIsRestituicao As Currency, _
                                                    ByVal cValSubvencaoFederal As Currency, _
                                                    ByVal cValSubvencaoEstadual As Currency, _
                                                    ByRef colCliente As Collection, _
                                                    Optional lClienteId As Long)


'## Calcula os valores de restitui��o para cada cliente - Rmarins 20/10/06

    Dim oRs As rdoResultset
    Dim oCliente As New Cliente

    Dim cValRestituicaoAux As Currency
    Dim dPercCli As Double

    On Error GoTo TrataErro

    'Obtendo a informa��o do Cliente '''''''''''''''''''''''''''''''''''''''''''''''''''
    If sFlagSubvencao = "CLIENTE" Then
        oCliente.Cliente_Id = lClienteId
        oCliente.iTpEndossoId = 0
    Else

        Set oRs = ObterClienteSubvencao(lProdutoId)

        If Not oRs.EOF Then

            If sFlagSubvencao = "FEDERAL" Then
                oCliente.Cliente_Id = oRs("cliente_subvencao_federal_id")
                oCliente.iTpEndossoId = 269

                'bmoraes 08.03.2009 - estadual
            Else
                oCliente.Cliente_Id = oRs("cliente_subvencao_estadual_id")
                oCliente.iTpEndossoId = 269
                ''''''''''''''''''''''''''''''''''''''
            End If

            Set oRs = Nothing

        End If

    End If

    oCliente.Tp_Cliente = sFlagSubvencao

    'Encontrando o Fator para c�lculo dos valores ''''''''''''''''''''''''''''''''''''''''
    If sFlagSubvencao = "CLIENTE" Then

        cValRestituicaoAux = cValRestituicao - cValSubvencaoFederal - cValSubvencaoEstadual
        dPercCli = FrmSelEndosso.CalcularPercValor(cValPremioTarifaRest, cValPremioTarifaRest - cValSubvencaoFederal - cValSubvencaoEstadual)

    ElseIf sFlagSubvencao = "FEDERAL" Then

        dPercCli = FrmSelEndosso.CalcularPercValor(cValPremioTarifaRest, cValSubvencaoFederal)

    Else

        dPercCli = FrmSelEndosso.CalcularPercValor(cValPremioTarifaRest, cValSubvencaoEstadual)

    End If

    'Calculando os valores '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    If sFlagSubvencao = "CLIENTE" Then
        oCliente.Val_Restituicao = cValRestituicaoAux
        oCliente.Val_IOF_Rest = cValIOFRest
        oCliente.Val_Custo_Apolice_Rest = cValCustoApoliceCertifRest
        oCliente.Val_Adic_Frac_Rest = cValAdicFracionamentoRest
        oCliente.Val_Desconto_Rest = cValDescontoRest
        oCliente.Val_Premio_Tarifa_Rest = Truncadecimal((cValPremioTarifaRest / 100) * dPercCli)
        oCliente.Val_Tot_Corretagem_Rest = Truncadecimal((cValTotCorretagemRest / 100) * dPercCli)
        oCliente.Val_Pro_Labore_Rest = Truncadecimal((cValProLaboreRest / 100) * dPercCli)

        oCliente.Val_Restituicao_Est = cValRestituicaoAux
        oCliente.Val_IOF_Est = cValIOFEst
        oCliente.Val_Custo_Apolice_Est = cValCustoApoliceCertifEst
        oCliente.Val_Adic_Frac_Est = cValAdicFracionamentoEst
        oCliente.Val_Desconto_Est = cValDescontoEst
        oCliente.Val_Premio_Tarifa_Est = Truncadecimal((cValPremioTarifaEst / 100) * dPercCli)
        oCliente.Val_Tot_Corretagem_Est = Truncadecimal((cValTotCorretagemEst / 100) * dPercCli)
        oCliente.Val_Pro_Labore_Est = Truncadecimal((cValProLaboreEst / 100) * dPercCli)
        oCliente.Val_IS_Restituicao = cValIsRestituicao
    Else

        If sFlagSubvencao = "FEDERAL" Then
            oCliente.Val_Restituicao = cValSubvencaoFederal
            oCliente.Val_Premio_Tarifa_Rest = cValSubvencaoFederal
            oCliente.Val_Restituicao_Est = cValSubvencaoFederal
            oCliente.Val_Premio_Tarifa_Est = cValSubvencaoFederal
        Else
            oCliente.Val_Restituicao = cValSubvencaoEstadual
            oCliente.Val_Premio_Tarifa_Rest = cValSubvencaoEstadual
            oCliente.Val_Restituicao_Est = cValSubvencaoEstadual
            oCliente.Val_Premio_Tarifa_Est = cValSubvencaoEstadual
        End If

        oCliente.Val_IOF_Rest = 0
        oCliente.Val_Custo_Apolice_Rest = 0
        oCliente.Val_Adic_Frac_Rest = 0
        oCliente.Val_Desconto_Rest = 0
        oCliente.Val_Tot_Corretagem_Rest = Truncadecimal((cValTotCorretagemRest / 100) * dPercCli)
        oCliente.Val_Pro_Labore_Rest = Truncadecimal((cValProLaboreRest / 100) * dPercCli)

        oCliente.Val_IOF_Est = 0
        oCliente.Val_Custo_Apolice_Est = 0
        oCliente.Val_Adic_Frac_Est = 0
        oCliente.Val_Desconto_Est = 0
        oCliente.Val_Tot_Corretagem_Est = Truncadecimal((cValTotCorretagemEst / 100) * dPercCli)
        oCliente.Val_Pro_Labore_Est = Truncadecimal((cValProLaboreEst / 100) * dPercCli)

        oCliente.Val_IS_Restituicao = 0
    End If

    colCliente.Add oCliente

    'Destruindo objeto
    Set oCliente = Nothing

    Exit Sub

TrataErro:
    Call Err.Raise(Err.Number, , "CalcularValoresRestituicaoCliente - " & Err.Description)

End Sub
'Inclus�o realizada em 11/08/2006 - crosendo
Public Function ObterTp_Endosso(ByVal sPropostaId As String, _
                                ByVal sEndossoId As Integer, _
                                ByRef lCodEventoALS As Long) As Integer
    Dim sSQL As String
    Dim Rst As rdoResultset

    On Error GoTo TrataErro

    sSQL = ""
    sSQL = sSQL & " SELECT endosso_tb.tp_endosso_id, " & vbNewLine
    sSQL = sSQL & "        tp_endosso_tb.cod_evento_als " & vbNewLine    'Rmarins - Inclu�do em 20/10/06
    sSQL = sSQL & " FROM endosso_tb with(NOLOCK) " & vbNewLine    'HOLIVEIRA: with(NOLOCK) para tabela em transa��o DT: 05/02/2007
    sSQL = sSQL & " JOIN tp_endosso_tb with(NOLOCK) " & vbNewLine    'HOLIVEIRA: with(NOLOCK) para tabela em transa��o DT: 05/02/2007
    sSQL = sSQL & "   ON endosso_tb.tp_endosso_id = tp_endosso_tb.tp_endosso_id "
    sSQL = sSQL & " WHERE endosso_tb.proposta_id = " & sPropostaId & vbNewLine
    sSQL = sSQL & "   AND endosso_tb.endosso_id = " & sEndossoId & vbNewLine

    Set Rst = rdocn.OpenResultset(sSQL)

    If Not Rst.EOF Then
        ObterTp_Endosso = Val(Rst(0))
        lCodEventoALS = Rst(1)
    Else
        ObterTp_Endosso = 0
        lCodEventoALS = 0
    End If

    Set Rst = Nothing

    Exit Function

TrataErro:
    Call Err.Raise(Err.Number, , "ConsultarTp_Endosso - " & Err.Description)

End Function

Private Sub AprovarAvaliacaoRestituicao(ByVal lPropostaId As Long, _
                                        ByVal lNumAvaliacao As Long, _
                                        ByVal lEndossoId As Long, _
                                        ByVal lConexao As Long, _
                                        ByRef OEndosso As Object, _
                                        ByRef cValRemuneracao As Currency)    'HOLIVEIRA: Recebe lConexao e o objeto oEndosso como parametro DT: 05/02/2007
'Mathayde 30/03/2009 - Recebendo cValRemuneracao e passa para AtualizarAvaliacaoRestituicao

' bcarneiro - 03/12/2003 - Projeto Restitui��o por Iniciativa da Seguradora

    Const SITUACAO_APROVACAO_TECNICA = "A"

    Dim oFinanceiro As Object

    On Error GoTo TrataErro

    'Recusando avalia��o t�cnica''''''''''''''''''''''''''''''''''''''''''''''''''''''''

    'HOLIVEIRA: Cria o objeto oFinanceiro a partir do objeto oEndosso, para manter a
    '           mesma transa��o. DT: 05/02/2007
    'Set oFinanceiro = CreateObject("SEGL0026.cls00125")
    Set oFinanceiro = OEndosso.CriaObjFinanceiro

    Call oFinanceiro.AtualizarAvaliacaoRestituicao(gsSIGLASISTEMA, _
                                                   App.Title, _
                                                   App.FileDescription, _
                                                   glAmbiente_id, _
                                                   cUserName, _
                                                   Data_Sistema, _
                                                   lPropostaId, _
                                                   lNumAvaliacao, _
                                                   SITUACAO_APROVACAO_TECNICA, _
       , _
     , _
   , _
 , _
, _
, _
, _
, _
, _
, _
                                                   lEndossoId, _
       , _
     , _
   , _
 , _
                                                   lConexao, cValRemuneracao)    'HOLIVEIRA: Passa lConexao como parametro DT: 05/02/2007
    'Mathayde 30/03/2009 - Passa cValRemuneracao para gravar em avaliacao_restituicao_tb

    'HOLIVEIRA: Destroi o objeto oFinanceiro DT: 05/02/2007
    OEndosso.DestroiObjFinanceiro
    Set oFinanceiro = Nothing

    'Atualizando interface'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    MousePointer = vbDefault

    Exit Sub

TrataErro:
    Call TrataErroGeral("AprovarAvaliacaoRestituicao")
    Call TerminaSEGBR

End Sub

Public Sub CarregarRateioCorretagemRestituicao(ByVal lPropostaId As Long, _
                                               ByVal lNumAvaliacao As Long, _
                                               ByVal cValFinanceiro As Currency, _
                                               ByVal cValIOF As Currency, _
                                               ByVal cValCustoApoliceCertif As Currency, _
                                               ByVal cPercIR As Currency)


' bcarneiro - 08/12/2003 - Projeto Restitui��o por Iniciativa da Seguradora

    Dim oFinanceiro As Object

    On Error GoTo TrataErro

    'Rateando valor de corretagem'''''''''''''''''''''''''''''''''''''''''''''''''''''

    Set oFinanceiro = CreateObject("SEGL0026.cls00125")

    Set Corretagens = oFinanceiro.ObterAvaliacaoRestituicaoCorretagem(gsSIGLASISTEMA, _
                                                                      App.Title, _
                                                                      App.FileDescription, _
                                                                      glAmbiente_id, _
                                                                      lPropostaId, _
                                                                      lNumAvaliacao, _
                                                                      cValFinanceiro, _
                                                                      cValIOF, _
                                                                      cValCustoApoliceCertif, _
                                                                      cPercIR)

    If bModular = True Then
        If Produto_id = 1168 Then
            If lNumAvaliacao = oDadosModular.Num_avaliacao Then
                oDadosModular.ValIOFRest = cValIOF
            End If
        Else
            If oDadosModular.proposta_id = lPropostaId Then
                oDadosModular.ValIOFRest = cValIOF
            End If
        End If
    End If

    Set oFinanceiro = Nothing

    Exit Sub

TrataErro:
    Call TrataErroGeral("CarregarRateioCorretagemRestituicao")
    Call TerminaSEGBR

End Sub

Public Sub CarregarCongeneres(ByVal lPropostaId As Long)

'Demanda 400422 - emaior 02/12/2008 - Preencher cole��o com Restitui��o de Pendentes Cedidos

    Dim oRestituicao As Object

    On Error GoTo TrataErro

    Set oRestituicao = CreateObject("SEGL0026.cls00125")

    Set Congeneres = oRestituicao.ObterCongeneres(gsSIGLASISTEMA, _
                                                  App.Title, _
                                                  App.FileDescription, _
                                                  glAmbiente_id, _
                                                  lPropostaId)

    Set oRestituicao = Nothing

    Exit Sub

TrataErro:
    Call TrataErroGeral("CarregarCongeneres")
    Call TerminaSEGBR

End Sub

Public Function ObterPercentualTotalCorretagem(ByVal lPropostaId As Long) As Double

    Dim oCorretagem As Object
    Dim dTotCorretagem As Double

    ' bcarneiro - 08/12/2003 - Projeto Restitui��o por Iniciativa da Seguradora
    On Error GoTo TrataErro


    dTotCorretagem = 0
    For Each oCorretagem In Corretagens
        dTotCorretagem = dTotCorretagem + oCorretagem.PercComissao
    Next
    ObterPercentualTotalCorretagem = dTotCorretagem

    Set oCorretagem = Nothing


    Exit Function

TrataErro:
    Call TrataErroGeral("ObterPercentualTotalCorretagem")
    Call TerminaSEGBR

End Function

Public Sub InserirEvento(lPropostaId As Long, _
                         iProdutoId As Integer, _
                         iRamoId As Integer, _
                         lEndossoId As Long, _
                         iTpEndossoId As Integer)

    On Error GoTo TrataErro

    Call IncluirEventoEmissao(lPropostaId, _
                              lEndossoId, _
                              iProdutoId, _
                              iRamoId, _
                              iTpEndossoId, _
                              App.EXEName, _
                              Data_Sistema, _
                              cUserName, _
                              rdocn)

    Exit Sub

TrataErro:
    Call TrataErroGeral("InserirEvento")
    Call TerminaSEGBR
End Sub

'
'Sub ProcessarEmissaoEndosso(ByVal lPropostaId As Long, _
 '                            ByVal lProdutoId As Long, _
 '                            ByVal lNumAvaliacao As Long, _
 '                            ByVal lClienteId As Long, _
 '                            ByVal lRestEndossoId As Long, _
 '                            ByVal sFlagTpCliente As String, _
 '                            ByVal cValRestituicao As Currency, _
 '                            ByVal cValIOFRest As Currency, _
 '                            ByVal cValCustoApoliceCertifRest As Currency, _
 '                            ByVal cValTotCorretagemRest As Currency, _
 '                            ByVal cValProLaboreRest As Currency, _
 '                            ByVal cValPremioTarifaRest As Currency, _
 '                            ByVal cValDescontoRest As Currency, _
 '                            ByVal cValAdicFracionamentoRest As Currency, _
 '                            ByVal cValIsRestituicao As Currency, _
 '                            ByVal cValCambio As Currency, _
 '                            ByVal sDtPedidoAvaliacao As String, _
 '                            ByVal sTextoEndosso As String, _
 '                            ByRef iOrigemProposta As Integer, _
 '                            ByVal iTpEndossoId As Integer, _
 '                            ByRef lEndossoId As Long, _
 '                            ByVal lConexao As Long, _
 '                            ByRef OEndosso As Object, _
 '                            ByRef cValRemuneracao As Currency)    'HOLIVEIRA: Recebe lConexao e o objeto oEndosso como parametro DT: 05/02/2007
''Mathayde 30/03/2009 - Recebe cValRemuneracao para calcular o cValIrRemuneracao
'
'    Const TP_OPERACAO_RESTITUICAO = "03"
'
'    Dim sRamoId As String
'    Dim sConvenio As String
'    Dim sFlagAprovacao As String
'
'    Dim iMoedaPremioId As Integer
'    Dim cPercIR As Currency
'    Dim cValTotIRCorretagem As Currency
'    Dim cValRepasseBB As Currency
'    Dim bFlagRetemComissao As Boolean
'    Dim bFlagRetemProLabore As Boolean
'    Dim bFlagRetemIOF As Boolean
'
'    Dim oProposta As Object
'
'
'    'mathayde 27/03/2009
'    Dim cValIrRemuneracao As Currency
'    Dim cPercIRRemun As Currency
'
'    'pmelo 20110524 - Tratamento 987 para acerto dos valores de movimentacao
'    Dim lAdmClienteId As Long
'
'
'    'HOLIVEIRA: Recebe o objeto oEndosso como parametro DT: 05/02/2007
'    'Dim oEndosso As Object
'
'    Dim oObjetoVazioAgendamento As Object
'
'    'Demanda 400422 - emaior 02/12/2008 - Relat�rio de Restitui��o de Comiss�o de Cosseguro
'    Dim sTipoEmissao As String
'    Dim lSeguradora As Long
'
'    On Error GoTo TrataErro
'
'    'Obtendo dados da Proposta ''''''''''''''''''''''''''''''''''''''''''''''''''''
'    'Demanda 400422 - emaior 02/12/2008 - Obtendo tamb�m tipo de emiss�o e seguradora da proposta
'    Call Obter_Dados_Adicionais_Proposta(lPropostaId, sRamoId, 0, , iMoedaPremioId, sTipoEmissao, lSeguradora)
'
'    Call Obtem_Perc_IR(sRamoId, Data_Sistema, cPercIR)
'
'    'Obtendo rateio de corretagem ''''''''''''''''''''''''''''''''''''''''''''''''''
'    Call CarregarRateioCorretagemRestituicao(lPropostaId, _
     '                                             lNumAvaliacao, _
     '                                             cValRestituicao, _
     '                                             cValIOFRest, _
     '                                             cValCustoApoliceCertifRest, _
     '                                             cPercIR)
'
'    'Demanda 400422 - emaior 02/12/2008 - Relat�rio de Restitui��o de Comiss�o de Cosseguro
'    Call CarregarCongeneres(lPropostaId)
'
'    cValTotIRCorretagem = Truncadecimal(cValTotCorretagemRest * cPercIR)
'
'    'mathayde 27/03/2009 - Calcula o valor do IR de remunera��o
'    cPercIRRemun = ObterPercIRRemuneracao(sRamoId, Data_Sistema)
'    cValIrRemuneracao = Truncadecimal(cValRemuneracao * cPercIRRemun)
'
'
'    'Obtendo valor de repasse para o BB''''''''''''''''''''''''''''''''''''''''''''''
'    sConvenio = Obtem_Numero_do_Convenio(Val(lProdutoId), Val(sRamoId), 1, iMoedaPremioId)
'
'    'Call VerificarRetencao(sConvenio, bFlagRetemComissao, bFlagRetemIOF)
'    Call VerificarRetencao(sConvenio, bFlagRetemComissao, bFlagRetemIOF, bFlagRetemProLabore)  'RMAURI - Incluido variaveis bFlagRetemProLabore
'
'    'pmelo 20110524 - Tratamento 987 para acerto dos valores de movimentacao
'    lAdmClienteId = Obter_Estipulante(lPropostaId)
'
'    'N�o restituir IR para estipulante FENAB FED NAC A BCO BRASIL
'    'Altera��o realizada para deixar a variavel compativel com o valor recebido em 27/05/2011 - Inicio - Confitec
'    'If lAdmClienteId = 192191 Then
'
'    If ((lAdmClienteId = 192191) Or (lAdmClienteId = 2452951)) And ((lProdutoId = 12) Or (lProdutoId = 15) Or (lProdutoId = 121) Or (lProdutoId = 135) Or (lProdutoId = 136) Or (lProdutoId = 716) Or (lProdutoId = 721)) Then
'        'If lAdmClienteId = 192191 Then
'        'Altera��o realizada para deixar a variavel compativel com o valor recebido em 27/05/2011 - Fim - Confitec
'        cValRepasseBB = CalcularRepasseSemIrEstipulante(cValTotCorretagemRest, _
         '                                                        cValProLaboreRest, _
         '                                                        cValRestituicao, _
         '                                                        cPercIR, _
         '                                                        cValIOFRest, _
         '                                                        bFlagRetemComissao, _
         '                                                        bFlagRetemIOF)
'
'
'    Else
'
'        'pmelo 20110524 - Tratamento 987 para acerto dos valores de movimentacao
'        cValRepasseBB = CalcularRepasse(cValTotCorretagemRest, _
         '                                        cValProLaboreRest, _
         '                                        cValRestituicao, _
         '                                        cPercIR, _
         '                                        cValIOFRest, _
         '                                        bFlagRetemComissao, _
         '                                        bFlagRetemIOF)
'
'    End If
'
'
'    'mathayde 27/03/2009 - Calcula o cValIrRemuneracao e cValRemuneracao em cValRepasseBB
'    cValRepasseBB = cValRepasseBB + cValIrRemuneracao - cValRemuneracao
'
'
'    Set oProposta = CreateObject("SEGL0022.cls00117")  'crosendo - Altera��o realizada em 11/08/2006
'
'    Set oRsProposta = oProposta.ConsultarProposta(gsSIGLASISTEMA, _
     '                                                  App.Title, _
     '                                                  App.FileDescription, _
     '                                                  glAmbiente_id, _
     '                                                  lPropostaId)
'    '
'    If Not oRsProposta.EOF Then
'
'        iOrigemProposta = IIf(IsNull(oRsProposta("origem_proposta_id")), 0, oRsProposta("origem_proposta_id"))
'
'        If iOrigemProposta = 2 Then  'ALS
'            sFlagAprovacao = "S"
'        Else
'            sFlagAprovacao = "N"
'        End If
'
'        'RMauri 22-06-2007 - Flow 212774
'        'Gravar como aprovado para produtos que n�o possuem proposta_Bb, para gerar pagamentos de restitui��o sem aprova��o do BB
'        Select Case CDbl("0" + oRsProposta("produto_id"))
'            ' racras inclus�o produtos BBC
'            '            Case 123, 400, 12, 121, 135, 136, 716 'Produtos on-line - Produtos BBC     'AKIO OKUNO - FLOWBR11332043 - 07/07/2011
'        Case 123, 400, 12, 121, 135, 136, 716, 223    'Produtos on-line - Produtos BBC
'            sFlagAprovacao = "S"
'        Case Else
'            sFlagAprovacao = sFlagAprovacao
'        End Select
'
'    End If
'
'    '***********************************************************************************************************************
'    '***********************************************************************************************************************
'    'Rely ------------------------------------------------------------------------------------------------------------------
'    'Verificando se o tp_endosso = 304 para gerar um endosso 304 apenas para controle
'    ' SELECT para nao midificar a avriavel iTpEndossoId recebida como parametro
'    '    Set oFaturamento = CreateObject("SEGL0022.cls00117")
'    '
'    '    Set oFaturamento = oFaturamento.ConsultarFaturamento(gsSIGLASISTEMA, _
     '         '                                                  App.Title, _
     '         '                                                  App.FileDescription, _
     '         '                                                  glAmbiente_id, _
     '         '                                                  lPropostaId, _
     '         '                                                  lNumAvaliacao)
'    '
'    '     If Not oFaturamento.EOF Then
'    '
'    '        If IsNull(oFaturamento("tp_endosso_id")) = False Then
'    '
'    '            If oFaturamento("tp_endosso_id") = 304 Then
'    '
'    '                    Dim Dt_Pedido_Avaliacao As String
'    '                    Dt_Pedido_Avaliacao = oFaturamento("dt_pedido_avaliacao")
'    '
'    '                            'GERAR ENDOSSO ALS (RELY)
'    '
'    '                             Set oFaturamento = CreateObject("SEGL0026.cls00124")
'    '
'    '                             Call oFaturamento.GerarEndossoAls(cUserName, _
     '                                  '                                                                            gsSIGLASISTEMA, _
     '                                  '                                                                            App.Title, _
     '                                  '                                                                            App.FileDescription, _
     '                                  '                                                                            glAmbiente_id, _
     '                                  '                                                                            Data_Sistema, _
     '                                  '                                                                            lPropostaId, _
     '                                  '                                                                            Format(Data_Sistema, "yyyymm"), _
     '                                  '                                                                            Dt_Pedido_Avaliacao, _
     '                                  '                                                                            0, _
     '                                  '                                                                            0, _
     '                                  '                                                                            304, _
     '                                  '                                                                            1, _
     '                                  '                                                                            0, _
     '                                  '                                                                            3818, _
     '                                  '                                                                            , _
     '                                  '                                                                            lConexao)
'    '
'    '
'    '
'    '
'    '
'    '                          ' Atualiza question�rio ALS (RELY)
'    '                          Set oFaturamento = CreateObject("SEGL0026.cls00124")
'    '
'    '                          Call oFaturamento.AtualizaQuestionarioALS(gsSIGLASISTEMA, _
     '                               '                                                  App.Title, _
     '                               '                                                  App.FileDescription, _
     '                               '                                                  glAmbiente_id, _
     '                               '                                                  lPropostaId, _
     '                               '                                                  lConexao)
'    '
'    '
'    ''                    End If
'    '
'    '            End If
'    '
'    '        End If
'    '
'    '     End If
'    '
'    '     Set oFaturamento = Nothing
'    '    '------------------------------------------------------------------------------------------------------------------------
'    '************************************************************************************************************************
'    '************************************************************************************************************************
'
'    If iTpEndossoId = 0 Then
'        iTpEndossoId = 80    'Endosso de Restitui��o a Pedido da Seguradora
'    End If
'
'    If sFlagTpCliente <> "CLIENTE" Then    'Caso seja um endosso
'        lRestEndossoId = 0
'    Else
'
'    End If
'
'    Set oObjetoVazioAgendamento = New Collection
'
'    'HOLIVEIRA: Recebe o objeto oEndosso como parametro DT: 05/02/2007
'    'Set oEndosso = CreateObject("SEGL0026.cls00124")
'
'    'Gerando endosso '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'    'HOLIVEIRA: Passa lConexao como parametro DT: 05/02/2007
'    lEndossoId = OEndosso.EmitirEndosso(cUserName, gsSIGLASISTEMA, App.Title, App.FileDescription, glAmbiente_id, _
     '                                        0, 0, lSeguradora, lPropostaId, Val(sRamoId), cValRestituicao, _
     '                                        iTpEndossoId, "n", 0, 0, 1, cValPremioTarifaRest, _
     '                                        TP_OPERACAO_RESTITUICAO, "", "", "", _
     '                                        cValCustoApoliceCertifRest, cValTotCorretagemRest, _
     '                                        0, _
     '                                        cValProLaboreRest, cValIOFRest, _
     '                                        cValTotIRCorretagem, cValDescontoRest, _
     '                                        cValAdicFracionamentoRest, "", _
     '                                        iMoedaPremioId, iMoedaPremioId, _
     '                                        cValCambio, cValIsRestituicao, _
     '                                        sDtPedidoAvaliacao, Data_Sistema, _
     '                                        lClienteId, cPercIR, cValRepasseBB, _
     '                                        0, 0, _
     '                                        0, Corretagens, _
     '                                        oObjetoVazioAgendamento, _
     '                                        0, _
     '                                        lConexao, _
     '               , sFlagAprovacao, sTextoEndosso, lRestEndossoId, , , , , bFlagRetemComissao, bFlagRetemProLabore, , Congeneres, sTipoEmissao)
'    'RMauri 13/02/2007 - Incluido variaveis bFlagRetemComissao, bFlagRetemProLabore
'    'Demanda 400422 - emaior 02/12/2008 - objeto Congeneres e sTipoEmissao como par�metros
'
'
'    If (bModular = True And lPropostaId = oDadosModular.proposta_id And lProdutoId <> 1168) Or (lNumAvaliacao = oDadosModular.Num_avaliacao) Then
'        oDadosModular.EndossoId = lEndossoId
'    End If
'
'
'
'    'Mathayde 27/03/2009 - A rotina EmitirEndosso n�o tem espa�o para passagem de par�metros
'    'Ent�o � inserido em endosso_financeiro_tb e ap�s atualizado remunera��o e ir remunera��o
'
'    Call OEndosso.AtualizarValoresDeRemuneracao(gsSIGLASISTEMA, _
     '                                                glAmbiente_id, _
     '                                                App.Title, _
     '                                                App.FileDescription, _
     '                                                lPropostaId, _
     '                                                lEndossoId, _
     '                                                cValRemuneracao, _
     '                                                cValIrRemuneracao, _
     '                                                lConexao)
'    'Mathayde  30/03/2009 - Envia cValRemuneracao e cValIrRemuneracao para atualizar em endosso_financeiro_tb
'
'
'
'    'Destruindo objetos
'    oRsProposta.Close
'    Set oRsProposta = Nothing
'    Set oProposta = Nothing
'
'    'HOLIVEIRA: Recebe o objeto oEndosso como parametro DT: 05/02/2007
'    'Set oEndosso = Nothing
'
'    Set colCorretagem = Nothing  'Maur�cio (Stefanini), 24/01/2006 - FLOWBR 120082
'
'    Set colCongeneres = Nothing  'Demanda 400422 - emaior 02/12/2008
'
'
'
'
'    Exit Sub
'
'TrataErro:
'    Call Err.Raise(Err.Number, , "ProcessarEmissaoEndosso - " & Err.Description)
'
'End Sub


'mathayde - solicita��o e-mail bruno

Function ObterPercIRRemuneracao(ByVal iRamo, ByVal PDtReferencia As String) As Currency


    Dim SQL As String
    Dim Rst As rdoResultset

    SQL = " SELECT valor "
    SQL = SQL & " From val_item_financeiro_ramo_TB "
    SQL = SQL & " WHERE cod_item_financeiro = 'IR_REMUN' "
    SQL = SQL & " AND ramo_id = " & iRamo
    SQL = SQL & " and dt_inicio_vigencia <= '" & DtReferencia & "'"
    SQL = SQL & " and (dt_fim_vigencia >= '" & DtReferencia & "' or dt_fim_vigencia is Null)"


    Set Rst = rdocn.OpenResultset(SQL)
    If Rst.EOF Then
        ObterPercIRRemuneracao = 0
    Else
        ObterPercIRRemuneracao = (Rst!valor)
    End If
    Rst.Close

End Function

Public Function Obter_Estipulante(ByVal lPropostaId As Long) As Long

    Dim sSQL As String
    Dim oRs As Recordset
    On Error GoTo TrataErro


    sSQL = ""
    sSQL = sSQL & "SELECT adm_cliente_id " & vbNewLine
    sSQL = sSQL & "  FROM seguros_db..administracao_apolice_tb with(NOLOCK) " & vbNewLine
    sSQL = sSQL & " WHERE administracao_apolice_tb.dt_fim_administracao IS NULL " & vbNewLine
    sSQL = sSQL & "   AND proposta_id = " & lPropostaId
    sSQL = sSQL & "UNION " & vbNewLine
    sSQL = sSQL & "SELECT administracao_apolice_tb.adm_cliente_id " & vbNewLine
    sSQL = sSQL & "  FROM seguros_db..proposta_adesao_tb proposta_adesao_tb with(NOLOCK) " & vbNewLine
    sSQL = sSQL & "  JOIN seguros_db..apolice_tb apolice_tb with(NOLOCK) ON apolice_tb.apolice_id = proposta_adesao_tb.apolice_id " & vbNewLine
    sSQL = sSQL & "   AND apolice_tb.ramo_id = proposta_adesao_tb.ramo_id " & vbNewLine
    sSQL = sSQL & "  JOIN seguros_db..administracao_apolice_tb administracao_apolice_tb with(NOLOCK) ON administracao_apolice_tb.proposta_id = apolice_tb.proposta_id " & vbNewLine
    sSQL = sSQL & " WHERE administracao_apolice_tb.dt_fim_administracao IS NULL " & vbNewLine
    sSQL = sSQL & "   AND proposta_adesao_tb.proposta_id                 = " & lPropostaId

    If rdocn Is Nothing Then
        Call Conexao
    Else
        If rdocn.State = 0 Then Call Conexao
    End If

    Set oRs = rdocn.Execute(sSQL)

    If Not oRs.EOF Then
        Obter_Estipulante = oRs!adm_cliente_id
    Else
        Obter_Estipulante = 0
    End If

    Set oRs = Nothing
    rdocn.Close
    Set rdocn = Nothing
    Set rdocn_Seg = Nothing
    Set rdocn_Seg2 = Nothing

    Exit Function

TrataErro:
    Call Err.Raise(Err.Number, , "Obter_Estipulante - " & Err.Description)
End Function


'pmelo 20110524 - Tratamento 987
Public Function CalcularRepasseSemIrEstipulante(ByVal cValComissao As Currency, _
                                                ByVal cValComissaoEstipulante As Currency, _
                                                ByVal cValorRestituicao As Currency, _
                                                ByVal dPercentualIR As Double, _
                                                ByVal cValorIOF As Currency, _
                                                ByVal bFlagRetemComissao As Boolean, _
                                                ByVal bFlagRetemIOF As Boolean) As Currency

'pcarvalho - 04/06/2003
'Objetivo: Padroniza��o do c�lculo de restitui��o

    Dim cValorIRCorretor As Currency
    Dim cValorIREstipulante As Currency
    Dim cValRepasse As Currency

    On Error GoTo TrataErro


    ''## Alterado por Alexandre Ricardo em 11/07/2003 - Confomre discutido entre J�nior e Ana Cl�udia
    ''## O Valor de Restitui��o n�o � somado ao Valor de IR.
    ''## cValRestituicaoAux = cValorRestituicao + cValorIRCorretor + cValorIREstipulante
    cValRepasse = cValorRestituicao

    'Se houver reten��o de comiss�o, retirar comiss�o de corretagem e estipulante
    'e aplicar o valor do IR de corretagem e o IR de estipulante
    If bFlagRetemComissao Then

        'Obtendo o valor de IR de corretagem e do estipulante
        cValorIRCorretor = Truncadecimal(cValComissao * dPercentualIR)
        cValorIREstipulante = 0    'TruncaDecimal(cValComissaoEstipulante * dPercentualIR)

        cValRepasse = cValRepasse - cValComissao - cValComissaoEstipulante _
                    + cValorIRCorretor + cValorIREstipulante

    End If

    'Se houver reten��o de IOF, retirar o valor do IOF
    If bFlagRetemIOF Then
        cValRepasse = cValRepasse - cValorIOF
    End If

    'Retornando o valor do repasse
    CalcularRepasseSemIrEstipulante = cValRepasse

    Exit Function

TrataErro:
    Call Err.Raise(Err.Number, , "CalcularRepasseSemIrEstipulante - " & Err.Description)


End Function

Function AtualizaDadosRestituicao(lProposta As Long, iProduto As Integer, Optional vSql As String = "") As rdoResultset

    On Error GoTo TrataErro
    Dim sSQL As String
    sSQL = "exec seguros_db.dbo.SEGS11164_SPU " & lProposta & " , " & iProduto & " , " & TpOperacao & IIf(vSql <> "", ", '" & vSql & " ' ", Null) & vbNewLine

    Set AtualizaDadosRestituicao = rdocn.OpenResultset(sSQL)

    Exit Function

TrataErro:
    Call Err.Raise(Err.Number, , "AtualizaDadosRestituicao - " & Err.Description)
    Resume
End Function
'Alan Neves - C_ANEVES - Demanda: 17860335 - 29/05/2013 - Fim

Sub aprovacao_tecnica(ByVal strPropostas As String, ByVal strNumAvaliacao, ByVal produtoId As Integer, ByVal strWhere As String)

    On Error GoTo TrataErro
    Dim oFinanceiro As Object


    FrmEspera.Label1 = "Aguarde, executando a aprova��o t�cnica..."
    FrmEspera.Show

    Set oFinanceiro = CreateObject("SEGL0026.cls00125")

    If Len(strPropostas) = 0 And Len(strNumAvaliacao) = 0 And produtoId <> 0 And Len(strWhere) > 0 Then
        Dim sSQL As String
        Dim rc As rdoResultset

        sSQL = "select * from #avaliacao_restituicao_tb where produto_id = " & CStr(produtoId) & strWhere
        Set rc = rdocn.OpenResultset(sSQL)

        Do While Not rc.EOF
            If rc("Cor") <> "A" Then
                strPropostas = strPropostas & rc("proposta_id") & ","
                strNumAvaliacao = strNumAvaliacao & rc("num_avaliacao") & ","
                If rc("modular") = "S" Then
                    strPropostas = strPropostas & rc("proposta_modular") & ","
                    strNumAvaliacao = strNumAvaliacao & rc("num_avaliacao_modular") & ","
                End If
            End If
            rc.MoveNext
        Loop
        rc.Close
        Set rc = Nothing
        strPropostas = Left(strPropostas, Len(strPropostas) - 1)
        strNumAvaliacao = Left(strNumAvaliacao, Len(strNumAvaliacao) - 1)

    End If

    Call oFinanceiro.AprovacaoTecnica(gsSIGLASISTEMA, App.Title, App.FileDescription, glAmbiente_id, strPropostas, strNumAvaliacao, Data_Sistema, cUserName)

    '--Matando tabelas tempor�rias
    sSQL = "SET NOCOUNT ON" & vbNewLine
    sSQL = sSQL & "CREATE TABLE #propostas" & vbNewLine
    sSQL = sSQL & "(" & vbNewLine
    sSQL = sSQL & "proposta_id numeric(9,0)" & vbNewLine
    sSQL = sSQL & ")" & vbNewLine
    Set rc = rdocn.OpenResultset(sSQL)
    rc.Close
    Set rc = Nothing

    Dim Vetor As Variant
    Vetor = Split(strPropostas, ",")

    Dim i As Long
    For i = LBound(Vetor) To UBound(Vetor)
        sSQL = "insert into #propostas(proposta_id)" & vbNewLine
        sSQL = sSQL & "Values(" & Vetor(i) & ");" & vbNewLine
        Set rc = rdocn.OpenResultset(sSQL)
        rc.Close
        Set rc = Nothing
    Next

    sSQL = "SET NOCOUNT ON" & vbNewLine
    'sSQL = sSQL & "delete from #avaliacao_restituicao_tb where proposta_id IN (" & strPropostas & ")"
    'Renato Vasconcelos
    'Flow 17801752
    sSQL = sSQL & "delete A from #avaliacao_restituicao_tb A" & vbNewLine & _
           "inner join avaliacao_restituicao_tb B with(nolock)" & vbNewLine & _
         " ON A.PROPOSTA_ID = B.PROPOSTA_ID" & vbNewLine & _
         " AND A.NUM_AVALIACAO = B.NUM_AVALIACAO" & vbNewLine & _
         " AND A.SITUACAO <> B.SITUACAO " & vbNewLine & _
           "where a.proposta_id IN (select proposta_id from #propostas);"
    sSQL = sSQL & "DROP TABLE #propostas"
    Set rc = rdocn.OpenResultset(sSQL)
    rc.Close
    Set rc = Nothing

    Call Unload(FrmEspera)

    MsgBox "Opera��o conclu�da!", vbInformation


    Exit Sub
TrataErro:
    Call Err.Raise(Err.Number, , "geraTempCliente - " & Err.Description)
End Sub
'Alan Neves - C_ANEVES - Demanda: 17860335 - 03/06/2013 - Fim


Function aprovacao_gerencial(ByVal strPropostas As String, ByVal strNumAvaliacao, ByVal produtoId As Integer, ByVal strWhere As String) As Boolean
'On Error GoTo TrataErro
    Dim oFinanceiro As Object
    Dim lConexao As Long
    Dim i As Long
    Dim rs As Object
    FrmEspera.Label1 = "Aguarde, executando a aprova��o gerencial..."
    FrmEspera.Show


    Set oFinanceiro = CreateObject("SEGL0026.cls00125")

    If Len(strPropostas) = 0 And Len(strNumAvaliacao) = 0 And produtoId <> 0 And Len(strWhere) > 0 Then
        Dim sSQL As String
        Dim rc As rdoResultset

        sSQL = "select * from #avaliacao_restituicao_tb where produto_id = " & CStr(produtoId) & strWhere
        Set rc = rdocn.OpenResultset(sSQL)

        Do While Not rc.EOF
            strPropostas = strPropostas & rc("proposta_id") & ","
            strNumAvaliacao = strNumAvaliacao & rc("num_avaliacao") & ","
            If rc("modular") = "S" Then
                strPropostas = strPropostas & rc("proposta_modular") & ","
                strNumAvaliacao = strNumAvaliacao & rc("num_avaliacao_modular") & ","
            End If
            rc.MoveNext
        Loop
        rc.Close
        Set rc = Nothing
        strPropostas = Left(strPropostas, Len(strPropostas) - 1)
        strNumAvaliacao = Left(strNumAvaliacao, Len(strNumAvaliacao) - 1)
    End If


    Set rs = oFinanceiro.AprovacaoGerencial(gsSIGLASISTEMA, App.Title, App.FileDescription, glAmbiente_id, strPropostas, strNumAvaliacao, produtoId, Data_Sistema, cUserName)

    Call Unload(FrmEspera)
    If rs.EOF Then

        MsgBox "Ocorreu algum problema ao aprovar as restitui��es!", vbInformation
        aprovacao_gerencial = False
        rs.Close
        Set rs = Nothing
        Exit Function
    End If

    Dim strPropostasErro As String

    strPropostasErro = ""

    Dim blnAprovacao As Boolean
    blnAprovacao = False
    Do While Not rs.EOF
        If rs(0) = "0" Then
            blnAprovacao = True
            Exit Do
        Else
            strPropostasErro = strPropostasErro & rs(1) & ", "
        End If
        rs.MoveNext
    Loop


    If blnAprovacao Then
        '--Matando tabelas tempor�rias
        sSQL = "SET NOCOUNT ON" & vbNewLine
        sSQL = sSQL & "CREATE TABLE #propostas" & vbNewLine
        sSQL = sSQL & "(" & vbNewLine
        sSQL = sSQL & "proposta_id numeric(9,0)" & vbNewLine
        sSQL = sSQL & ")" & vbNewLine
        Set rc = rdocn.OpenResultset(sSQL)
        rc.Close
        Set rc = Nothing

        Dim Vetor As Variant
        Vetor = Split(strPropostas, ",")


        For i = LBound(Vetor) To UBound(Vetor)
            sSQL = "insert into #propostas(proposta_id)" & vbNewLine
            sSQL = sSQL & "Values(" & Vetor(i) & ");" & vbNewLine
            Set rc = rdocn.OpenResultset(sSQL)
            rc.Close
            Set rc = Nothing
        Next

        sSQL = "SET NOCOUNT ON" & vbNewLine
        'sSQL = sSQL & "delete from #avaliacao_restituicao_tb where proposta_id IN (" & strPropostas & ")"
        'sSQL = sSQL & "delete from #avaliacao_restituicao_tb where proposta_id IN (select proposta_id from #propostas);"
        'Renato Vasconcelos
        'Flow 17801752
        sSQL = sSQL & "delete A from #avaliacao_restituicao_tb A" & vbNewLine & _
               "inner join avaliacao_restituicao_tb B with(nolock)" & vbNewLine & _
             " ON A.PROPOSTA_ID = B.PROPOSTA_ID" & vbNewLine & _
             " AND A.NUM_AVALIACAO = B.NUM_AVALIACAO" & vbNewLine & _
             " AND A.SITUACAO <> B.SITUACAO " & vbNewLine & _
               "where A.proposta_id IN (select proposta_id from #propostas);"
        sSQL = sSQL & "DROP TABLE #propostas"
        Set rc = rdocn.OpenResultset(sSQL)
        rc.Close
        Set rc = Nothing


        aprovacao_gerencial = True
    Else
        strPropostasErro = Left(strPropostasErro, Len(strPropostasErro) - 2)
        'Dim Vetor As Variant

        Vetor = Split(strPropostasErro, ",")

        sSQL = "SET NOCOUNT ON" & vbNewLine
        sSQL = sSQL & "CREATE TABLE #propostas" & vbNewLine
        sSQL = sSQL & "(" & vbNewLine
        sSQL = sSQL & "proposta_id numeric(9,0)" & vbNewLine
        sSQL = sSQL & ")" & vbNewLine
        Set rc = rdocn.OpenResultset(sSQL)
        rc.Close
        Set rc = Nothing

        'Dim Vetor As Variant
        Vetor = Split(strPropostas, ",")


        For i = LBound(Vetor) To UBound(Vetor)
            sSQL = "insert into #propostas(proposta_id)" & vbNewLine
            sSQL = sSQL & "Values(" & Vetor(i) & ");" & vbNewLine
            Set rc = rdocn.OpenResultset(sSQL)
            rc.Close
            Set rc = Nothing
        Next

        sSQL = "SET NOCOUNT ON" & vbNewLine
        'sSQL = sSQL & "delete from #avaliacao_restituicao_tb where proposta_id IN (" & strPropostas & ")"
        'Renato Vasconcelos
        'Flow 17801752

        ' sSQL = sSQL & "delete from #avaliacao_restituicao_tb where proposta_id IN (select proposta_id from #propostas)" & _
          " and not proposta_id IN(" & strPropostasErro & ");"

        sSQL = sSQL & "delete A from #avaliacao_restituicao_tb A" & vbNewLine & _
             " inner join avaliacao_restituicao_tb B with(nolock)" & vbNewLine & _
             " ON A.PROPOSTA_ID = B.PROPOSTA_ID" & vbNewLine & _
             " AND A.NUM_AVALIACAO = B.NUM_AVALIACAO" & vbNewLine & _
             " AND A.SITUACAO <> B.SITUACAO " & vbNewLine & _
             " where A.proposta_id IN (select proposta_id from #propostas)" & _
             " and not A.proposta_id IN(" & strPropostasErro & ");"
        '29/05/2013 Corre��o efetuada de (and not proposta_id IN(& strPropostasErro &)) para (and not A.proposta_id IN(& strPropostasErro &)) - Tales de S�'





        sSQL = sSQL & "DROP TABLE #propostas"
        Set rc = rdocn.OpenResultset(sSQL)
        rc.Close
        Set rc = Nothing
        '
        '        sSQL = "SET NOCOUNT ON" & vbNewLine
        '        sSQL = sSQL & "delete from #avaliacao_restituicao_tb where proposta_id IN (" & strPropostas & ") " & _
                 '            " and not proposta_id IN(" & strPropostasErro & ")"
        '
        '        Set rc = rdocn.OpenResultset(sSQL)
        '        rc.Close
        Dim msg As String
        ' Dim i As Integer
        strPropostasErro = ""

        For i = LBound(Vetor) To UBound(Vetor)
            strPropostasErro = strPropostasErro & Vetor(i) & ", "
            If i + 1 Mod 15 = 0 Then
                strPropostasErro = strPropostasErro & vbNewLine
            End If
        Next
        strPropostasErro = Left(strPropostasErro, Len(strPropostasErro) - 2)
        msg = "A(s) Proposta(s)" & vbNewLine & _
              strPropostasErro & vbNewLine & _
              "N�o foi(foram) aprovada(s), devido a algum erro durante a aprova��o!" & vbNewLine & _
              "A(s) outra(s) foi(foram) aprovada(s) "
        If Len(msg) < 1024 Then
            MsgBox msg, vbInformation

        Else
            frmPropostasErro.lblMensagem = msg
            frmPropostasErro.Show vbModal
        End If
        aprovacao_gerencial = True

    End If

    rs.Close
    Set rs = Nothing


    Exit Function

TrataErro:
    Call Err.Raise(Err.Number, , "geraTempCliente - " & Err.Description)


End Function

Public Function CalcularRepasse(ByVal cValComissao As Currency, _
                                ByVal cValComissaoEstipulante As Currency, _
                                ByVal cValorRestituicao As Currency, _
                                ByVal dPercentualIR As Double, _
                                ByVal cValorIOF As Currency, _
                                ByVal bFlagRetemComissao As Boolean, _
                                ByVal bFlagRetemIOF As Boolean) As Currency

    Dim cValorIRCorretor As Currency
    Dim cValorIREstipulante As Currency
    Dim cValRepasse As Currency

    On Error GoTo Trata_Erro

    ''## Alterado por Alexandre Ricardo em 11/07/2003 - Confomre discutido entre J�nior e Ana Cl�udia
    ''## O Valor de Restitui��o n�o � somado ao Valor de IR.
    ''## cValRestituicaoAux = cValorRestituicao + cValorIRCorretor + cValorIREstipulante
    cValRepasse = cValorRestituicao

    'Se houver reten��o de comiss�o, retirar comiss�o de corretagem e estipulante
    'e aplicar o valor do IR de corretagem e o IR de estipulante
    If bFlagRetemComissao Then

        'Obtendo o valor de IR de corretagem e do estipulante
        cValorIRCorretor = Truncadecimal(cValComissao * dPercentualIR)
        cValorIREstipulante = Truncadecimal(cValComissaoEstipulante * dPercentualIR)

        cValRepasse = cValRepasse - cValComissao - cValComissaoEstipulante _
                    + cValorIRCorretor + cValorIREstipulante

    End If

    'Se houver reten��o de IOF, retirar o valor do IOF
    If bFlagRetemIOF Then
        cValRepasse = cValRepasse - cValorIOF
    End If

    'Retornando o valor do repasse
    CalcularRepasse = cValRepasse

    Exit Function

Trata_Erro:

    Call TrataErroGeral("CalcularRepasse", "Funcoes_RDO")
    Call TerminaSEGBR

End Function




