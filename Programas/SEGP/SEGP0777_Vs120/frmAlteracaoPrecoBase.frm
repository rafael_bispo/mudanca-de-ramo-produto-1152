VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.ocx"
Begin VB.Form frmPrecoBase 
   Caption         =   "Altera��o de Pre�o Base"
   ClientHeight    =   8925
   ClientLeft      =   120
   ClientTop       =   450
   ClientWidth     =   12690
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   ScaleHeight     =   8925
   ScaleWidth      =   12690
   StartUpPosition =   3  'Windows Default
   Begin VB.Frame Frame1 
      Height          =   1095
      Index           =   16
      Left            =   480
      TabIndex        =   45
      Top             =   4800
      Width           =   11655
      Begin VB.TextBox txtPrecoBase 
         Alignment       =   1  'Right Justify
         Enabled         =   0   'False
         Height          =   285
         Index           =   22
         Left            =   7620
         TabIndex        =   49
         Top             =   450
         Width           =   3255
      End
      Begin VB.TextBox txtPrecoBase 
         Alignment       =   1  'Right Justify
         Enabled         =   0   'False
         Height          =   285
         Index           =   13
         Left            =   1750
         TabIndex        =   46
         Top             =   450
         Width           =   3255
      End
      Begin VB.Label Label2 
         Caption         =   "Novo Pre�o Base com Des�gio"
         Height          =   255
         Index           =   4
         Left            =   5280
         TabIndex        =   48
         Top             =   480
         Width           =   2295
      End
      Begin VB.Label Label2 
         Caption         =   "Novo Pre�o Base"
         Height          =   255
         Index           =   23
         Left            =   345
         TabIndex        =   47
         Top             =   480
         Width           =   1455
      End
   End
   Begin VB.CommandButton btnSair 
      Caption         =   "&Sair"
      Height          =   375
      Index           =   0
      Left            =   8880
      TabIndex        =   44
      Top             =   8280
      Width           =   1500
   End
   Begin VB.Frame Frame1 
      Caption         =   "Valores Calculados"
      Height          =   1935
      Index           =   0
      Left            =   480
      TabIndex        =   27
      Top             =   6100
      Width           =   11655
      Begin VB.TextBox txtPrecoBase 
         Alignment       =   1  'Right Justify
         Enabled         =   0   'False
         Height          =   285
         Index           =   17
         Left            =   8300
         TabIndex        =   35
         Top             =   480
         Width           =   2500
      End
      Begin VB.TextBox txtPrecoBase 
         Alignment       =   1  'Right Justify
         Enabled         =   0   'False
         Height          =   285
         Index           =   15
         Left            =   3000
         TabIndex        =   34
         Top             =   480
         Width           =   2500
      End
      Begin VB.TextBox txtPrecoBase 
         Alignment       =   1  'Right Justify
         Enabled         =   0   'False
         Height          =   285
         Index           =   14
         Left            =   350
         TabIndex        =   33
         Top             =   480
         Width           =   2500
      End
      Begin VB.TextBox txtPrecoBase 
         Alignment       =   1  'Right Justify
         Enabled         =   0   'False
         Height          =   285
         Index           =   16
         Left            =   5650
         TabIndex        =   32
         Top             =   480
         Width           =   2500
      End
      Begin VB.TextBox txtPrecoBase 
         Alignment       =   1  'Right Justify
         Enabled         =   0   'False
         Height          =   285
         Index           =   21
         Left            =   8300
         TabIndex        =   31
         Top             =   1200
         Width           =   2500
      End
      Begin VB.TextBox txtPrecoBase 
         Alignment       =   1  'Right Justify
         Enabled         =   0   'False
         Height          =   285
         Index           =   20
         Left            =   5650
         TabIndex        =   30
         Top             =   1200
         Width           =   2500
      End
      Begin VB.TextBox txtPrecoBase 
         Alignment       =   1  'Right Justify
         Enabled         =   0   'False
         Height          =   285
         Index           =   19
         Left            =   3000
         TabIndex        =   29
         Top             =   1200
         Width           =   2500
      End
      Begin VB.TextBox txtPrecoBase 
         Alignment       =   1  'Right Justify
         Enabled         =   0   'False
         Height          =   285
         Index           =   18
         Left            =   350
         TabIndex        =   28
         Top             =   1200
         Width           =   2500
      End
      Begin VB.Label Label3 
         Caption         =   "Fat. Esperado"
         Height          =   255
         Index           =   3
         Left            =   8300
         TabIndex        =   43
         Top             =   240
         Width           =   1215
      End
      Begin VB.Label Label2 
         Caption         =   "% de Redu��o / Aumento"
         Height          =   255
         Index           =   17
         Left            =   345
         TabIndex        =   42
         Top             =   240
         Width           =   2175
      End
      Begin VB.Label Label2 
         Caption         =   "Valor Total a Restituir"
         Height          =   255
         Index           =   16
         Left            =   3000
         TabIndex        =   41
         Top             =   240
         Width           =   2055
      End
      Begin VB.Label Label2 
         Caption         =   "Valor Total a Cobrar"
         Height          =   255
         Index           =   15
         Left            =   5650
         TabIndex        =   40
         Top             =   240
         Width           =   2055
      End
      Begin VB.Label Label2 
         Caption         =   "IS Cob. B�sica"
         Height          =   255
         Index           =   14
         Left            =   350
         TabIndex        =   39
         Top             =   960
         Width           =   1200
      End
      Begin VB.Label Label2 
         Caption         =   "IS Cob. Adicional"
         Height          =   255
         Index           =   13
         Left            =   3000
         TabIndex        =   38
         Top             =   960
         Width           =   1335
      End
      Begin VB.Label Label2 
         Caption         =   "IS Total"
         Height          =   255
         Index           =   12
         Left            =   5650
         TabIndex        =   37
         Top             =   960
         Width           =   615
      End
      Begin VB.Label Label2 
         Caption         =   "Pr�mio Novo"
         Height          =   255
         Index           =   11
         Left            =   8300
         TabIndex        =   36
         Top             =   960
         Width           =   975
      End
   End
   Begin VB.CommandButton cmdAplicar 
      Caption         =   "&Aplicar"
      Height          =   375
      Index           =   3
      Left            =   10650
      TabIndex        =   18
      Top             =   8280
      Width           =   1500
   End
   Begin VB.Frame Frame1 
      Caption         =   "Valores da Proposta"
      Height          =   1935
      Index           =   4
      Left            =   480
      TabIndex        =   1
      Top             =   2640
      Width           =   11655
      Begin VB.TextBox txtPrecoBase 
         Alignment       =   1  'Right Justify
         Enabled         =   0   'False
         Height          =   285
         Index           =   7
         Left            =   350
         TabIndex        =   26
         Top             =   1160
         Width           =   1500
      End
      Begin VB.TextBox txtPrecoBase 
         Alignment       =   1  'Right Justify
         Enabled         =   0   'False
         Height          =   285
         Index           =   8
         Left            =   2300
         TabIndex        =   25
         Top             =   1160
         Width           =   1500
      End
      Begin VB.TextBox txtPrecoBase 
         Alignment       =   1  'Right Justify
         Enabled         =   0   'False
         Height          =   285
         Index           =   9
         Left            =   4000
         TabIndex        =   24
         Top             =   1160
         Width           =   1500
      End
      Begin VB.TextBox txtPrecoBase 
         Alignment       =   1  'Right Justify
         Enabled         =   0   'False
         Height          =   285
         Index           =   10
         Left            =   5700
         TabIndex        =   23
         Top             =   1160
         Width           =   1500
      End
      Begin VB.TextBox txtPrecoBase 
         Alignment       =   1  'Right Justify
         Enabled         =   0   'False
         Height          =   285
         Index           =   3
         Left            =   4000
         TabIndex        =   9
         Top             =   500
         Width           =   1500
      End
      Begin VB.TextBox txtPrecoBase 
         Alignment       =   1  'Right Justify
         Enabled         =   0   'False
         Height          =   285
         Index           =   1
         Left            =   350
         TabIndex        =   8
         Top             =   500
         Width           =   1500
      End
      Begin VB.TextBox txtPrecoBase 
         Alignment       =   1  'Right Justify
         Enabled         =   0   'False
         Height          =   285
         Index           =   2
         Left            =   2300
         TabIndex        =   7
         Top             =   500
         Width           =   1500
      End
      Begin VB.TextBox txtPrecoBase 
         Alignment       =   1  'Right Justify
         Enabled         =   0   'False
         Height          =   285
         Index           =   11
         Left            =   7400
         TabIndex        =   6
         Top             =   1160
         Width           =   1500
      End
      Begin VB.TextBox txtPrecoBase 
         Alignment       =   1  'Right Justify
         Enabled         =   0   'False
         Height          =   285
         Index           =   4
         Left            =   5700
         TabIndex        =   5
         Top             =   500
         Width           =   1500
      End
      Begin VB.TextBox txtPrecoBase 
         Alignment       =   1  'Right Justify
         Enabled         =   0   'False
         Height          =   285
         Index           =   5
         Left            =   7400
         TabIndex        =   4
         Top             =   500
         Width           =   1500
      End
      Begin VB.TextBox txtPrecoBase 
         Alignment       =   1  'Right Justify
         Enabled         =   0   'False
         Height          =   285
         Index           =   6
         Left            =   9105
         TabIndex        =   3
         Top             =   500
         Width           =   1500
      End
      Begin VB.TextBox txtPrecoBase 
         Alignment       =   1  'Right Justify
         Enabled         =   0   'False
         Height          =   285
         Index           =   12
         Left            =   9105
         TabIndex        =   2
         Top             =   1160
         Width           =   1500
      End
      Begin VB.Label Label2 
         Caption         =   "Pr�mio Total"
         Height          =   255
         Index           =   3
         Left            =   9105
         TabIndex        =   22
         Top             =   900
         Width           =   1095
      End
      Begin VB.Label Label2 
         Caption         =   "Pr�mio Pago"
         Height          =   255
         Index           =   2
         Left            =   7400
         TabIndex        =   21
         Top             =   900
         Width           =   975
      End
      Begin VB.Label Label2 
         Caption         =   "IS Total"
         Height          =   255
         Index           =   1
         Left            =   5700
         TabIndex        =   20
         Top             =   900
         Width           =   615
      End
      Begin VB.Label Label2 
         Caption         =   "IS Cob. Adicional"
         Height          =   255
         Index           =   0
         Left            =   4000
         TabIndex        =   19
         Top             =   900
         Width           =   1335
      End
      Begin VB.Label Label2 
         Caption         =   "IS Cob. B�sica"
         Height          =   255
         Index           =   6
         Left            =   2300
         TabIndex        =   17
         Top             =   900
         Width           =   1200
      End
      Begin VB.Label Label2 
         Caption         =   "�rea Segurada"
         Height          =   255
         Index           =   7
         Left            =   4000
         TabIndex        =   16
         Top             =   240
         Width           =   1215
      End
      Begin VB.Label Label2 
         Caption         =   "Prod. Esperada"
         Height          =   255
         Index           =   8
         Left            =   2300
         TabIndex        =   15
         Top             =   240
         Width           =   1215
      End
      Begin VB.Label Label2 
         Caption         =   "Pre�o Base com Des�gio"
         Height          =   255
         Index           =   9
         Left            =   350
         TabIndex        =   14
         Top             =   240
         Width           =   2055
      End
      Begin VB.Label Label2 
         Caption         =   "N�vel de Cobertura"
         Height          =   255
         Index           =   25
         Left            =   5700
         TabIndex        =   13
         Top             =   240
         Width           =   1575
      End
      Begin VB.Label Label3 
         Caption         =   "Taxa da Cob. B�sica"
         Height          =   255
         Index           =   0
         Left            =   9105
         TabIndex        =   12
         Top             =   240
         Width           =   1635
      End
      Begin VB.Label Label2 
         Caption         =   "% Des�gio"
         Height          =   255
         Index           =   26
         Left            =   7400
         TabIndex        =   11
         Top             =   240
         Width           =   1335
      End
      Begin VB.Label Label3 
         Caption         =   "Fat. Esperado"
         Height          =   255
         Index           =   2
         Left            =   350
         TabIndex        =   10
         Top             =   900
         Width           =   1215
      End
   End
   Begin MSFlexGridLib.MSFlexGrid grdPrecoBase 
      Height          =   2205
      Index           =   0
      Left            =   480
      TabIndex        =   0
      Top             =   360
      Width           =   11655
      _ExtentX        =   20558
      _ExtentY        =   3889
      _Version        =   393216
      Rows            =   1
      Cols            =   22
      FixedCols       =   0
      FocusRect       =   0
      HighLight       =   2
      FormatString    =   $"frmAlteracaoPrecoBase.frx":0000
   End
End
Attribute VB_Name = "frmPrecoBase"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub btnSair_Click(Index As Integer)
    On Error GoTo TrataErro
    frmSelecao.Show
    Me.Hide
    Call Unload(Me)
    Call Unload(frmEndosso)
    Exit Sub

TrataErro:
    MsgBox Err.Description
    Call TratarErro("btnSair_Click", Me.name)
End Sub

Private Sub cmdAplicar_Click(Index As Integer)
    On Error GoTo TrataErro

    Call GerarEndossoAlteracaoPrecoBase(CLng(frmEndosso.txtPropostaid(0).Text))

    frmSelecao.Show
    Me.Hide
    Call Unload(Me)
    Call Unload(frmEndosso)

    Exit Sub

TrataErro:
    MsgBox Err.Description
    Call TratarErro("cmdAplicar_Click", Me.name)

End Sub

Private Sub Form_Load()
    On Error GoTo TrataErro
    Call Conexao
    Call CarregarGridPrecoBase
    Call CarregarDadosProposta
    Exit Sub

TrataErro:
    MsgBox Err.Description
    Call TratarErro("Form_Load", Me.name)
End Sub
Private Sub CarregarDadosProposta()
    Dim oPrecoBase As Object
    Dim oRs As Recordset
    On Error GoTo TrataErro

    Set oPrecoBase = CreateObject("SEGL0026.cls00126")

    Set oRs = oPrecoBase.ConsultarDadosProposta(gsSIGLASISTEMA, _
                                                App.Title, _
                                                App.FileDescription, _
                                                glAmbiente_id, _
                                                CLng(frmEndosso.txtPropostaid(0).Text))

    If Not oRs.EOF Then

        txtPrecoBase(2).Text = oRs("produtividade_esperada")
        txtPrecoBase(3).Text = oRs("area_segurada")
        If bConfiguracaoBrasil Then
            txtPrecoBase(1).Text = Format(CCur(oRs("PRECO_BASE_ANTERIOR")), "###########0.0000")
            txtPrecoBase(4).Text = TrocaValorAmePorBras(Format(CCur(oRs("nivel_cobertura")), "###########0.00"))
            txtPrecoBase(5).Text = TrocaValorAmePorBras(Format(CCur(oRs("perc_desagio")), "###########0.00"))
            txtPrecoBase(6).Text = TrocaValorAmePorBras(Format(oRs("taxa_cob_basica"), "###########0.0000000"))
            txtPrecoBase(7).Text = Replace$(Replace$(Replace$(TrocaValorAmePorBras(Format(CCur(oRs("fator_esperado")), "#,##0.00")), ".", ";"), ",", "."), ";", ",")    '###########0.00
            txtPrecoBase(8).Text = Replace$(Replace$(Replace$(TrocaValorAmePorBras(Format(CCur(oRs("val_IS_cob_basica")), "#,##0.00")), ".", ";"), ",", "."), ";", ",")    '###########0.00
            txtPrecoBase(9).Text = Replace$(Replace$(Replace$(TrocaValorAmePorBras(Format(CCur(oRs("val_IS_cob_adicional")), "#,##0.00")), ".", ";"), ",", "."), ";", ",")    '###########0.00
            txtPrecoBase(10).Text = Replace$(Replace$(Replace$(TrocaValorAmePorBras(Format(CCur(oRs("val_IS_total")), "#,##0.00")), ".", ";"), ",", "."), ";", ",")    '###########0.00
            txtPrecoBase(11).Text = Replace$(Replace$(Replace$(TrocaValorAmePorBras(Format(CCur(oRs("val_pago")), "#,##0.00")), ".", ";"), ",", "."), ";", ",")    '###########0.00

            txtPrecoBase(12).Text = Replace$(Replace$(Replace$(TrocaValorAmePorBras(Format(CCur(oRs("val_premio_total")), "#,##0.00")), ".", ";"), ",", "."), ";", ",")    '###########0.00

            txtPrecoBase(13).Text = Replace$(Replace$(Replace$(TrocaValorAmePorBras(Format(CCur(IIf(frmEndosso.PrecoBaseNovo = "", "0.0000", frmEndosso.PrecoBaseNovo)), "#,##0.0000")), ".", ";"), ",", "."), ";", ",")    '###########0.00
            txtPrecoBase(14).Text = Replace$(Replace$(Replace$(TrocaValorAmePorBras(Format(CCur(oRs("PERC_REDUCAO_AUMENTO")), "#,##0.0000")), ".", ";"), ",", "."), ";", ",")    '###########0.00)
            txtPrecoBase(15).Text = Replace$(Replace$(Replace$(TrocaValorAmePorBras(Format(CCur(oRs("VAL_RESTITUICAO")), "#,##0.00")), ".", ";"), ",", "."), ";", ",")    '###########0.00
            txtPrecoBase(16).Text = Replace$(Replace$(Replace$(TrocaValorAmePorBras(Format(CCur(oRs("VAL_TOTAL_COBRANCA")), "#,##0.00")), ".", ";"), ",", "."), ";", ",")    '###########0.00
            txtPrecoBase(17).Text = Replace$(Replace$(Replace$(TrocaValorAmePorBras(Format(CCur(oRs("FATOR_ESPERADO_NOVO")), "#,##0.00")), ".", ";"), ",", "."), ";", ",")    '###########0.00
            txtPrecoBase(18).Text = Replace$(Replace$(Replace$(TrocaValorAmePorBras(Format(CCur(oRs("VAL_IS_COB_BASICA_NOVA")), "#,##0.00")), ".", ";"), ",", "."), ";", ",")    '###########0.00
            txtPrecoBase(19).Text = Replace$(Replace$(Replace$(TrocaValorAmePorBras(Format(CCur(oRs("val_IS_cob_adicional")), "#,##0.00")), ".", ";"), ",", "."), ";", ",")    '###########0.00
            txtPrecoBase(20).Text = Replace$(Replace$(Replace$(TrocaValorAmePorBras(Format(CCur(oRs("VAL_IS_TOTAL_NOVO")), "#,##0.00")), ".", ";"), ",", "."), ";", ",")    '###########0.00
            txtPrecoBase(21).Text = Replace$(Replace$(Replace$(TrocaValorAmePorBras(Format(CCur(oRs("VAL_PREMIO_NOVO_AGENDADO")), "#,##0.00")), ".", ";"), ",", "."), ";", ",")    '###########0.00
            txtPrecoBase(22).Text = Replace$(Replace$(Replace$(TrocaValorAmePorBras(Format(CCur(oRs("PRECO_BASE_NOVO_DESAGIO")), "#,##0.0000")), ".", ";"), ",", "."), ";", ",")    '###########0.00


        Else

            txtPrecoBase(1).Text = Format(CCur(oRs("PRECO_BASE_ANTERIOR")), "###########0.0000")
            txtPrecoBase(4).Text = TrocaValorAmePorBras(Format(CCur(oRs("nivel_cobertura")), "###########0.00"))
            txtPrecoBase(5).Text = TrocaValorAmePorBras(Format(CCur(oRs("perc_desagio")), "###########0.00"))
            txtPrecoBase(6).Text = TrocaValorAmePorBras(Format(oRs("taxa_cob_basica"), "###########0.0000000"))
            txtPrecoBase(7).Text = Replace$(Replace$(Replace$(TrocaValorAmePorBras(Format(CCur(oRs("fator_esperado")), "#,##0.00")), ".", ";"), ",", "."), ";", ",")    '###########0.00
            txtPrecoBase(8).Text = Replace$(Replace$(Replace$(TrocaValorAmePorBras(Format(CCur(oRs("val_IS_cob_basica")), "#,##0.00")), ".", ";"), ",", "."), ";", ",")    '###########0.00
            txtPrecoBase(9).Text = Replace$(Replace$(Replace$(TrocaValorAmePorBras(Format(CCur(oRs("val_IS_cob_adicional")), "#,##0.00")), ".", ";"), ",", "."), ";", ",")    '###########0.00
            txtPrecoBase(10).Text = Replace$(Replace$(Replace$(TrocaValorAmePorBras(Format(CCur(oRs("val_IS_total")), "#,##0.00")), ".", ";"), ",", "."), ";", ",")    '###########0.00
            txtPrecoBase(11).Text = Replace$(Replace$(Replace$(TrocaValorAmePorBras(Format(CCur(oRs("val_pago")), "#,##0.00")), ".", ";"), ",", "."), ";", ",")    '###########0.00

            txtPrecoBase(12).Text = Replace$(Replace$(Replace$(TrocaValorAmePorBras(Format(CCur(oRs("val_premio_total")), "#,##0.00")), ".", ";"), ",", "."), ";", ",")    '###########0.00

            txtPrecoBase(13).Text = Replace$(Replace$(Replace$(TrocaValorAmePorBras(Format(CCur(IIf(frmEndosso.PrecoBaseNovo = "", "0.0000", frmEndosso.PrecoBaseNovo)), "#,##0.0000")), ".", ";"), ",", "."), ";", ",")    '###########0.00
            txtPrecoBase(14).Text = Replace$(Replace$(Replace$(TrocaValorAmePorBras(Format(CCur(oRs("PERC_REDUCAO_AUMENTO")), "#,##0.0000")), ".", ";"), ",", "."), ";", ",")    '###########0.00)
            txtPrecoBase(15).Text = Replace$(Replace$(Replace$(TrocaValorAmePorBras(Format(CCur(oRs("VAL_RESTITUICAO")), "#,##0.00")), ".", ";"), ",", "."), ";", ",")    '###########0.00
            txtPrecoBase(16).Text = Replace$(Replace$(Replace$(TrocaValorAmePorBras(Format(CCur(oRs("VAL_TOTAL_COBRANCA")), "#,##0.00")), ".", ";"), ",", "."), ";", ",")    '###########0.00
            txtPrecoBase(17).Text = Replace$(Replace$(Replace$(TrocaValorAmePorBras(Format(CCur(oRs("FATOR_ESPERADO_NOVO")), "#,##0.00")), ".", ";"), ",", "."), ";", ",")    '###########0.00
            txtPrecoBase(18).Text = Replace$(Replace$(Replace$(TrocaValorAmePorBras(Format(CCur(oRs("VAL_IS_COB_BASICA_NOVA")), "#,##0.00")), ".", ";"), ",", "."), ";", ",")    '###########0.00
            txtPrecoBase(19).Text = Replace$(Replace$(Replace$(TrocaValorAmePorBras(Format(CCur(oRs("val_IS_cob_adicional")), "#,##0.00")), ".", ";"), ",", "."), ";", ",")    '###########0.00
            txtPrecoBase(20).Text = Replace$(Replace$(Replace$(TrocaValorAmePorBras(Format(CCur(oRs("VAL_IS_TOTAL_NOVO")), "#,##0.00")), ".", ";"), ",", "."), ";", ",")    '###########0.00
            txtPrecoBase(21).Text = Replace$(Replace$(Replace$(TrocaValorAmePorBras(Format(CCur(oRs("VAL_PREMIO_NOVO_AGENDADO")), "#,##0.00")), ".", ";"), ",", "."), ";", ",")    '###########0.00
            txtPrecoBase(22).Text = Replace$(Replace$(Replace$(TrocaValorAmePorBras(Format(CCur(oRs("PRECO_BASE_NOVO_DESAGIO")), "#,##0.0000")), ".", ";"), ",", "."), ";", ",")    '###########0.00
        End If


        If Val(txtPrecoBase(1).Text) > Val(txtPrecoBase(13).Text) Then
            'Redu��o
            Label2(17).Caption = "% de Redu��o"
        Else
            'Aumento
            Label2(17).Caption = "% de Aumento"
        End If
    End If

    oRs.Close

    Exit Sub

TrataErro:
    MsgBox Err.Description
    Call TratarErro("CarregarDadosProposta", Me.name)
End Sub
Private Sub CarregarGridPrecoBase()
    Dim oPrecoBase As Object
    Dim oRs As Recordset
    On Error GoTo TrataErro

    Set oPrecoBase = CreateObject("SEGL0026.cls00126")

    Set oRs = oPrecoBase.ConsultarDadosPrecoBase(gsSIGLASISTEMA, _
                                                 App.Title, _
                                                 App.FileDescription, _
                                                 glAmbiente_id, _
                                                 CLng(frmEndosso.txtPropostaid(0).Text))

    While Not oRs.EOF

        Linha = oRs!proposta_id & vbTab & oRs!num_endosso & vbTab & oRs!Apolice_id & vbTab & oRs!sucursal_seguradora_id & vbTab & _
                oRs!seguradora_cod_susep & vbTab & oRs!Dt_Fim_Vigencia & vbTab & oRs!ramo_id & vbTab & _
                oRs!banco_id & vbTab & oRs!agencia_id & vbTab & oRs!conta_corrente_id & _
                vbTab & oRs!Num_Cartao & vbTab & oRs!Dt_Validade_Cartao & vbTab & oRs!Dt_Inicio_Vigencia & vbTab & _
                oRs!Seguro_Moeda_id & vbTab & oRs!tp_cobertura
        grdPrecoBase(num_grid).AddItem Linha

        oRs.MoveNext
    Wend


    grdPrecoBase(num_grid).ColWidth(0) = 1000
    grdPrecoBase(num_grid).ColWidth(1) = 1050
    grdPrecoBase(num_grid).ColWidth(2) = 800
    grdPrecoBase(num_grid).ColWidth(3) = 2550
    grdPrecoBase(num_grid).ColWidth(4) = 1400
    grdPrecoBase(num_grid).ColWidth(5) = 1250
    grdPrecoBase(num_grid).ColWidth(6) = 1300

    oRs.Close


    Exit Sub

TrataErro:
    MsgBox Err.Description
    Call TratarErro("CarregarGridPrecoBase", Me.name)

End Sub
'JOAO.MACHADO
'13-04-2013
'NOVO ENDOSSO DE ALTERACAO DO PRECO BASE
Private Sub GerarEndossoAlteracaoPrecoBase(ByVal lProposta As Long)
    Dim rc As Recordset
    Dim PrecoBase As Double
    On Error GoTo TrataErro

    SQL = ""
    SQL = SQL & "SELECT PROPOSTA_TB.PROPOSTA_ID, "
    SQL = SQL & "       PROPOSTA_TB.SITUACAO, "
    SQL = SQL & "       CASE WHEN PROPOSTA_FECHADA_TB.DT_FIM_VIG IS NULL THEN 'TRUE' "
    SQL = SQL & "            WHEN PROPOSTA_FECHADA_TB.DT_FIM_VIG > GETDATE() THEN 'TRUE' "
    SQL = SQL & "            Else 'FALSE' END AS PROPOSTA_VIGENTE"
    SQL = SQL & "  FROM PROPOSTA_TB  WITH (NOLOCK) "
    SQL = SQL & "  JOIN PROPOSTA_FECHADA_TB  WITH (NOLOCK) "
    SQL = SQL & "    ON PROPOSTA_TB.PROPOSTA_ID = PROPOSTA_FECHADA_TB.PROPOSTA_ID "
    SQL = SQL & " WHERE PROPOSTA_TB.PROPOSTA_ID = " & lProposta

    If rdocn Is Nothing Then
        Call Conexao
    Else
        If rdocn.State = 0 Then Call Conexao
    End If

    Set rc = rdocn.Execute(SQL)

    If rc!Situacao <> "i" Then
        MensagemBatch "Proposta n�o est� emitida!", vbOKOnly, "Altera��o de pre�o base", True, Me
        TerminaSEGBR
    End If

    If rc!PROPOSTA_VIGENTE = False Then
        MensagemBatch "Proposta fora da vig�ncia!", vbOKOnly, "Altera��o de pre�o base", True, Me
        TerminaSEGBR
    End If


    rc.Close

    SQL = ""
    SQL = SQL & " SELECT COUNT(1) AS QTD_PARCELAS_ATRASO"
    SQL = SQL & "   From AGENDAMENTO_COBRANCA_ATUAL_TB"
    SQL = SQL & "  Where PROPOSTA_ID = " & lProposta
    SQL = SQL & "    AND ISNULL(CANC_ENDOSSO_ID,0) = 0"
    SQL = SQL & "    AND (DT_BAIXA IS NULL AND  DT_AGENDAMENTO < GETDATE())"

    Set rc = rdocn.Execute(SQL)

    If rc!QTD_PARCELAS_ATRASO > 0 Then
        If MsgBox("Esta proposta possui parcelas em atraso! Em caso de RESTITUI��O, o valor calculado ser� sobre as parcelas PAGAS, deseja continuar?", vbYesNo) = vbNo Then
            TerminaSEGBR
        End If
    End If

    rc.Close

    If CDbl(txtPrecoBase(22).Text) <= 0 Then
        MsgBox ("Pre�o base n�o cadastrado!")
        Exit Sub
    End If

    ' VALIDANDO SE TEVE ALTERACAO DE PRECO BASE DA CONTRATACAO
    SQL = ""
    SQL = SQL & " SELECT PRECO_BASE_ANTERIOR  = convert(numeric(10,4),LTRIM(RTRIM(REPLACE(ISNULL(TEXTO_RESPOSTA,0),',','.')))) "
    SQL = SQL & " FROM QUESTIONARIO_OBJETO_TB  WITH (NOLOCK)  "
    SQL = SQL & " WHERE PROPOSTA_ID = " & frmEndosso.txtPropostaid(0).Text
    SQL = SQL & " AND COD_OBJETO_SEGURADO =  1 "
    SQL = SQL & " AND PERGUNTA_ID = 7740 "
    SQL = SQL & " AND ENDOSSO_ID = (SELECT MAX(ENDOSSO_ID) ENDOSSO_ID FROM QUESTIONARIO_OBJETO_TB  WITH (NOLOCK)   WHERE PROPOSTA_ID = " & frmEndosso.txtPropostaid(0).Text & " ) "

    Set rc = rdocn.Execute(SQL)

    If CDbl(txtPrecoBase(22).Text) = CDbl(rc!PRECO_BASE_ANTERIOR) Then
        MsgBox ("Pre�o base vigente � o mesmo da contrata��o ou endosso anterior!")
        Exit Sub
    End If

    rc.Close

    SQL = ""
    SQL = " EXEC seguros_db..SEGS11170_SPI  "
    SQL = SQL & CLng(frmEndosso.txtPropostaid(0).Text)
    SQL = SQL & " ,'" & frmEndosso.txtDescEndosso.Text & "' "
    SQL = SQL & " ,'" & cUserName & "' "
    SQL = SQL & " , " & TrocaVirgulaPorPonto(Replace$(txtPrecoBase(22).Text, ".", ""))
    SQL = SQL & " , " & TrocaVirgulaPorPonto(Replace$(txtPrecoBase(1).Text, ".", ""))
    SQL = SQL & " , " & TrocaVirgulaPorPonto(Replace$(txtPrecoBase(15).Text, ".", ""))
    SQL = SQL & " , " & TrocaVirgulaPorPonto(Replace$(txtPrecoBase(21).Text, ".", ""))
    SQL = SQL & " , " & TrocaVirgulaPorPonto(Replace$(txtPrecoBase(18).Text, ".", ""))
    SQL = SQL & " , " & TrocaVirgulaPorPonto(Replace$(txtPrecoBase(16).Text, ".", ""))
    SQL = SQL & " , " & TrocaVirgulaPorPonto(Replace$(txtPrecoBase(14).Text, ".", ""))
    '26/09/2017 - Schoralick (ntendencia) - MU2017052506 - melhorias no endosso de faturamento (inicio)
    If frmEndosso.oDadosEndosso.TipoEndosso = 335 And frmEndosso.oDadosEndosso.produtoId = 1204 Then
        SQL = SQL & " , " & TrocaVirgulaPorPonto(Replace$(txtPrecoBase(17).Text, ".", ""))
        SQL = SQL & " , " & TrocaVirgulaPorPonto(Replace$(txtPrecoBase(18).Text, ".", ""))
    End If
    '26/09/2017 - Schoralick (ntendencia) - MU2017052506 - melhorias no endosso de faturamento (fim)
    Set rc = rdocn.Execute(SQL)

    MsgBox "Endosso de altera��o do pre�o base efetuado com sucesso!", vbInformation, "SEGP0777 - Altera��o de Pre�o Base"



    Exit Sub

TrataErro:
    MsgBox Err.Description
    Call TratarErro("GerarEndossoAlteracaoPrecoBase", Me.name)
End Sub


