Attribute VB_Name = "EndossoContestacao"
Function CriticaTipoEdsContestacao(lPropostaId As Long) As Boolean
    Dim oCriticaEds As Object
    Dim sMsg As String
    Dim sRetornoEds As String

    On Error GoTo TrataErro

    sMsg = ""
    CriticaTipoEdsContestacao = False

    Set oCriticaEds = CreateObject("SEGL0026.cls00168")

    If oCriticaEds.CriticarEdsContestacaoEmitida(gsSIGLASISTEMA, App.Title, App.FileDescription, glAmbiente_id, lPropostaId) Then
        sMsg = "N�o � possivel liberar endosso de contesta��o para esta proposta. Proposta n�o est� emitida."
        GoTo ExibeMsgCritica
    End If

    If Not oCriticaEds.CriticarEdsContestacaoCanalVenda(gsSIGLASISTEMA, App.Title, App.FileDescription, glAmbiente_id, lPropostaId) Then
        sMsg = "N�o � possivel liberar endosso de contesta��o para esta proposta. Canal de venda incompat�vel."
        GoTo ExibeMsgCritica
    End If

    If oCriticaEds.CriticarEdsContestacaoDuploSim(gsSIGLASISTEMA, App.Title, App.FileDescription, glAmbiente_id, lPropostaId) Then
        sMsg = "N�o � possivel liberar endosso de contesta��o para esta proposta. Houve confirma��o da contrata��o."
        GoTo ExibeMsgCritica
    End If

    If oCriticaEds.CriticarEdsContestacaoPin(gsSIGLASISTEMA, App.Title, App.FileDescription, glAmbiente_id, lPropostaId) Then
        sMsg = "N�o � possivel liberar endosso de contesta��o para esta proposta. Cliente utilizou plataforma para confirmar proposta."
        GoTo ExibeMsgCritica
    End If

    If oCriticaEds.CriticarEdsContestacaoSinistro(gsSIGLASISTEMA, App.Title, App.FileDescription, glAmbiente_id, lPropostaId) Then
        sMsg = "N�o � possivel liberar endosso de contesta��o para esta proposta. Proposta possui sinistro."
        GoTo ExibeMsgCritica
    End If


    sRetornoEds = oCriticaEds.CriticarEdsContestacaoEndosso(gsSIGLASISTEMA, App.Title, App.FileDescription, glAmbiente_id, lPropostaId)
    If sRetornoEds <> "" Then
        sMsg = "N�o � possivel liberar endosso de contesta��o para esta proposta. " & vbNewLine
        sMsg = sMsg & "Proposta possui o(s) endosso(s): " & vbNewLine & vbNewLine

        Dim aEds() As String
        Dim i As Integer
        aEds = Split(sRetornoEds, "/")
        For i = 0 To LBound(aEds)
            sMsg = sMsg & aEds(i) & vbNewLine
        Next
        GoTo ExibeMsgCritica
    End If

    If oCriticaEds.CriticarEdsContestacaoRestituicaoAndamento(gsSIGLASISTEMA, App.Title, App.FileDescription, glAmbiente_id, lPropostaId) Then
        sMsg = "N�o � possivel liberar endosso de contesta��o para esta proposta. Proposta possui restitui��o(�es) em andamento."
        GoTo ExibeMsgCritica
    End If

    If oCriticaEds.CriticarEdsContestacaoPeriodo(gsSIGLASISTEMA, App.Title, App.FileDescription, glAmbiente_id, lPropostaId) Then
        sMsg = "N�o � possivel liberar endosso de contesta��o para esta proposta. Proposta tem mais de 11 meses de vig�ncia."
        GoTo ExibeMsgCritica
    End If


    CriticaTipoEdsContestacao = True
    Set oCriticaEds = Nothing

    Exit Function

ExibeMsgCritica:
    Set oCriticaEds = Nothing

    If sMsg <> "" Then
        MsgBox sMsg, vbCritical
    End If
    Exit Function

TrataErro:
    Call TratarErro("CriticaTipoEdsContestacao")
    Call FinalizarAplicacao

End Function

Sub CarregarDadosRestituicaoEdsContestacao()
'Alexandre Debouch - Confitec - EdsContestacao

    Dim lPropostaId As Long

    On Error GoTo TrataErro

    lPropostaId = frmEndosso.oDadosEndosso.PropostaId

    Call frmEndosso.ConsultarCorretagem(lPropostaId)
    Call frmEndosso.ConsultarEstipulante(lPropostaId)
    Call frmEndosso.CalcularValorProLabore
    Call ObterValoresRestituicaoEdsContestacao(lPropostaId)
    Call frmEndosso.CalcularValorRestituicao

    frmEndosso.optCobAdi.Enabled = False
    frmEndosso.optCobRes.Enabled = False
    frmEndosso.optCobSem.Enabled = False
    frmEndosso.optCobRes.Value = True

    frmEndosso.stbEndosso.TabVisible(1) = True
    frmEndosso.stbEndosso.Tab = 1

    frmEndosso.mskValPremioTarifaRes.mask = "9,2"
    frmEndosso.mskAdicFracionamentoRes.mask = "9,2"
    frmEndosso.mskCustoCertificadoRes.mask = "9,2"
    frmEndosso.mskValIOFRes.mask = "9,2"
    frmEndosso.mskDescontoRes.mask = "9,2"
    frmEndosso.mskPercCorretagemCob.mask = "9,2"
    frmEndosso.mskPercProLabore.mask = "9,2"
    frmEndosso.mskCambio.mask = "3,6"

    Call frmEndosso.MontarComboMoeda

    Exit Sub

TrataErro:
    Call TratarErro("CarregarDadosRestituicao")
    Call FinalizarAplicacao
    'Alexandre Debouch - Confitec - EdsContestacao
End Sub

Sub ObterValoresRestituicaoEdsContestacao(lPropostaId As Long)

    Dim oFinanceiro As Object
    Dim rs As Recordset
    Dim cValPago As Currency
    Dim cValJuros As Currency
    Dim cValDesconto As Currency
    Dim cValIOF As Currency
    Dim cPremioTotal As Currency
    Dim cValCorretagem As Currency
    Dim cValRemuneracao As Currency
    Dim cValRestituicao As Currency
    Dim bRestIntegralALS As Boolean
    Dim cValRemuneracao_novo As Currency

    On Error GoTo TrataErro

    cValPago = 0
    cValJuros = 0
    cValDesconto = 0
    cPremioTotal = 0


    bRestIntegral = True

    Set oFinanceiro = CreateObject("SEGL0026.cls00125")
    Set rs = oFinanceiro.ObterPremioPagoEdsContestacao(gsSIGLASISTEMA, _
                                                       App.Title, _
                                                       App.FileDescription, _
                                                       glAmbiente_id, _
                                                       lPropostaId)

    Set oFinanceiro = Nothing
    If Not rs.EOF Then
        While Not rs.EOF
            cPremioTotal = cPremioTotal + CCur(rs("valor_pago"))
            cValJuros = cValJuros + CCur(rs("val_adic_fracionamento"))
            cValDesconto = cValDesconto + CCur(rs("val_desconto"))
            cValIOF = cValIOF + CCur(rs("val_iof"))
            If UCase(rs("situacao")) = "A" Then
                cValPago = cValPago + CCur(rs("valor_pago"))
            End If
            cValCorretagem = cValCorretagem + CCur(rs("val_comissao"))
            cValRemuneracao = cValRemuneracao + CCur(rs("val_remuneracao"))
            rs.MoveNext
        Wend
    End If
    Set rs = Nothing
    cValPago = cValPago - frmEndosso.oDadosEndosso.valCustoApoliceCert
    cPremioTotal = cPremioTotal - frmEndosso.oDadosEndosso.valCustoApoliceCert
    cValRestituicao = cValPago

    frmEndosso.mskValPremioTarifaRes.Text = Format(cValRestituicao, "##0.00")
    frmEndosso.mskValPremioTarifaResCancelamento(0).Text = frmEndosso.mskValPremioTarifaRes.Text
    frmEndosso.lblValCorretagem.Caption = Format(cValCorretagem, "#,##0.00")
    
    cValRemuneracao_novo = CalcularValorRemuneracao_contestacao(Format(cValRestituicao, "##0.00"))
    
    If cValRemuneracao_novo > cValRemuneracao Then
        cValRemuneracao_novo = cValRemuneracao
    End If
    
    frmEndosso.lblValRemunercaoServico.Caption = Format(cValRemuneracao_novo, "#,##0.00")
    Exit Sub
TrataErro:
    Call TratarErro("ObterValoresRestituicao")
    FinalizarAplicacao

End Sub


Private Function CalcularValorRemuneracao_contestacao(ByVal cValRestituicao As Currency) As Currency

'Procedimento criado para calcular o valor da remunera��o
'bnery - 14/07/2011 - Nova Consultoria
'Demanda 11239622 - Melhorias Ferramentas de Emiss�o

    Dim cValPremioTarifa As Currency
    Dim cValRemuneracao As Currency
    Dim cValPercRemuneracao As Double
    Dim oFinanceiro As Object
    Dim rs As ADODB.Recordset

    On Error GoTo TrataErro

    Set oFinanceiro = CreateObject("SEGL0026.cls00125")

    Set rs = oFinanceiro.ConsultarCusto(gsSIGLASISTEMA, _
                                        App.Title, _
                                        App.FileDescription, _
                                        glAmbiente_id, _
                                        frmEndosso.oDadosEndosso.RamoId, _
                                        frmEndosso.oDadosEndosso.produtoId, _
                                        Data_Sistema)

    If Not rs.EOF Then
        cValPercRemuneracao = rs("perc_remuneracao_servico")
        rs.Close
    Else
        cValPercRemuneracao = 0
    End If

    Set rs = Nothing
    Set oFinanceiro = Nothing

    
    CalcularValorRemuneracao_contestacao = Truncadecimal(cValRestituicao * (cValPercRemuneracao / 100))

    

    Exit Function

TrataErro:

    Call TratarErro("CalcularValorRemuneracao_contestacao")
    Call FinalizarAplicacao

End Function


Sub IncluirEndossoContestacaoEndosso(TP_ENDOSSO_CONTESTACAO As Integer)
'Alexandre Debouch - Confitec - EdsContestacao

    On Error GoTo TrataErro
    Dim OEndosso As Object
    Dim oExtrato As Object
    Dim iEndossoId As Integer

    Set OEndosso = CreateObject("SEGL0026.cls00124")
    iEndossoId = OEndosso.ObterProximoEndosso(gsSIGLASISTEMA, _
                                              App.Title, _
                                              App.FileDescription, _
                                              glAmbiente_id, _
                                              frmEndosso.txtPropostaid(0).Text)

    Call OEndosso.IncluirEndosso(gsSIGLASISTEMA, _
                                 App.Title, _
                                 App.FileDescription, _
                                 glAmbiente_id, _
                                 frmEndosso.txtPropostaid(0).Text, _
                                 iEndossoId, _
                                 "0", _
                                 TP_ENDOSSO_CONTESTACAO, _
                                 frmEndosso.mskDtEndosso.Text, _
                                 frmEndosso.mskDtEmissao.Text, _
                                 Trim(frmEndosso.txtDescEndosso.Text), _
                                 "SEGP0777")
    'cUserName)

    Call OEndosso.IncluirLogContestacao(gsSIGLASISTEMA, _
                                        App.Title, _
                                        App.FileDescription, _
                                        glAmbiente_id, _
                                        frmEndosso.oDadosEndosso.produtoId, _
                                        frmEndosso.oDadosEndosso.PropostaId, _
                                        iEndossoId, _
                                        TP_ENDOSSO_CONTESTACAO, _
                                        Data_Sistema, _
                                        frmEndosso.mskDtEmissao.Text, _
                                        cUserName)

    Set OEndosso = Nothing
    Set oExtrato = CreateObject("SEGL0026.cls00125")

    Call oExtrato.InserirExtratoRestituicao(gsSIGLASISTEMA, _
                                            App.Title, _
                                            App.FileDescription, _
                                            glAmbiente_id, _
                                            "SEGP0777", _
                                            frmEndosso.txtPropostaid(0).Text, _
                                            "0.00", _
                                            iEndossoId, _
                                            0, _
                                            "Inclus�o do Endosso de Contesta��o", _
                                            "Finalizado", _
                                            "Processo Autom�tico do Endosso de Contesta��o")


    Set oExtrato = Nothing
    Exit Sub

TrataErro:

    Call TratarErro("IncluirEndossoContesta��o")
    FinalizarAplicacao
    'Alexandre Debouch - Confitec - EdsContestacao
End Sub

Sub IncluirEndossoContestacaoCancelamento()
'Alexandre Debouch - Confitec - EdsContestacao
    On Error GoTo TrataErro

    Dim lTpCancelamento As Integer
    Dim oObterTipoRamo As Object
    Dim ltpRamo As Integer

    lTpCancelamento = 2
    Set oObterTipoRamo = CreateObject("SEGL0026.cls00126")
    ltpRamo = oObterTipoRamo.ObterTipoRamo(gsSIGLASISTEMA, _
                                           App.Title, _
                                           App.FileDescription, _
                                           glAmbiente_id, _
                                           frmEndosso.txtRamo.Text)

    Set oObterTipoRamo = Nothing

    If (ltpRamo = 1 And Not (frmEndosso.txtProduto.Text = 1226 Or frmEndosso.txtProduto.Text = 1227)) Or frmEndosso.txtProduto.Text = 1218 Then
        lTpCancelamento = 4
    End If

    Call frmEndosso.IncluirEndossoCancelamentoApolice(frmEndosso.oDadosEndosso.ApoliceId, _
                                                      frmEndosso.oDadosEndosso.SucursalSeguradoraId, _
                                                      frmEndosso.oDadosEndosso.SeguradoraCodSusep, _
                                                      frmEndosso.oDadosEndosso.PropostaId, _
                                                      frmEndosso.oDadosEndosso.RamoId, _
                                                      0#, _
                                                      64, _
                                                      0, _
                                                      0, _
                                                      0, _
                                                      0, _
                                                      0, _
                                                      0, _
                                                      0, 0, _
                                                      0, 0, _
                                                      0, 0, _
                                                      frmEndosso.mskDtEndosso.Text, frmEndosso.mskDtEmissao.Text, _
                                                      frmEndosso.oDadosEndosso.BeneficiarioId, frmEndosso.oDadosEndosso.PercIR, _
                                                      0, _
                                                      "", _
                                                      frmEndosso.txtMotCancel.Text, _
                                                      frmEndosso.mskDtVigCan.Text, _
                                                      frmEndosso.mskDtVigCan.Text, _
                                                      lTpCancelamento, _
                                                      Trim(frmEndosso.txtMotCancel.Text))


    Dim iEndossoId As Integer
    Dim rsEds As Recordset
    Dim OEndosso As Object

    Set OEndosso = CreateObject("SEGL0026.cls00124")
    Set rsEds = OEndosso.ObterEndosso(gsSIGLASISTEMA, _
                                      App.Title, _
                                      App.FileDescription, _
                                      glAmbiente_id, _
                                      frmEndosso.txtPropostaid(0).Text, _
                                      64)

    If Not rsEds.EOF() Then
        iEndossoId = rsEds!endosso_id

        Call OEndosso.IncluirLogContestacao(gsSIGLASISTEMA, _
                                            App.Title, _
                                            App.FileDescription, _
                                            glAmbiente_id, _
                                            frmEndosso.oDadosEndosso.produtoId, _
                                            frmEndosso.oDadosEndosso.PropostaId, _
                                            iEndossoId, _
                                            64, _
                                            Data_Sistema, _
                                            frmEndosso.mskDtEmissao.Text, _
                                            cUserName)

    End If
    Set rsEds = Nothing
    Set OEndosso = Nothing

    Dim oExtrato As Object
    Set oExtrato = CreateObject("SEGL0026.cls00125")
    Call oExtrato.InserirExtratoRestituicao(gsSIGLASISTEMA, _
                                            App.Title, _
                                            App.FileDescription, _
                                            glAmbiente_id, _
                                            "SEGP0777", _
                                            frmEndosso.oDadosEndosso.PropostaId, _
                                            "0.00", _
                                            iEndossoId, _
                                            0, _
                                            "Cancelamento a pedido da seguradora", _
                                            "Finalizado", _
                                            "Processo Autom�tico do Endosso de Contesta��o")


    Set oExtrato = Nothing
    Exit Sub
    'Alexandre Debouch - Confitec - EdsContestacao
TrataErro:

    Call TratarErro("IncluirEndossoContesta��o")
    FinalizarAplicacao
End Sub

Sub IncluirEndossoContestacaoAvaliacao()
    On Error GoTo TrataErro
    'Alexandre Debouch - Confitec - EdsContestacao
    Call frmEndosso.PreencherDadosEndossoRestituicao
    If frmEndosso.oDadosEndosso.ValRestituicao > 0 Then
        Call frmEndosso.IncluirAvaliacaoRestituicao(frmEndosso.oDadosEndosso.PropostaId, _
                                                    Truncadecimal(frmEndosso.oDadosEndosso.ValRestituicao), _
                                                    Truncadecimal(frmEndosso.oDadosEndosso.valCorretagem), _
                                                    Truncadecimal(frmEndosso.oDadosEndosso.valIOF), _
                                                    Truncadecimal(frmEndosso.oDadosEndosso.valAdicFracionamento), _
                                                    Truncadecimal(frmEndosso.oDadosEndosso.valDesconto), _
                                                    Truncadecimal(frmEndosso.oDadosEndosso.valPremioTarifa), _
                                                    Truncadecimal(frmEndosso.oDadosEndosso.valCustoEmissao), _
                                                    Truncadecimal(frmEndosso.oDadosEndosso.ValProLabore), _
                                                    CCur(frmEndosso.mskCambio.Text), _
                                                    Data_Sistema, _
                                                    "Avalia��o para endosso de restitui��o a pedido da seguradora - Eds. Contesta��o", _
                                                    0, 0, _
                                                    frmEndosso.oDadosEndosso.PossuiSubvencao, _
                                                    Truncadecimal(frmEndosso.oDadosEndosso.valSubvencaoFederal), _
                                                    Truncadecimal(frmEndosso.oDadosEndosso.valSubvencaoEstadual), _
                                                    Truncadecimal(frmEndosso.oDadosEndosso.valRemuneracaoServico))

        Dim oExtrato As Object
        Set oExtrato = CreateObject("SEGL0026.cls00125")
        Call oExtrato.InserirExtratoRestituicao(gsSIGLASISTEMA, _
                                                App.Title, _
                                                App.FileDescription, _
                                                glAmbiente_id, _
                                                "SEGP0777", _
                                                frmEndosso.oDadosEndosso.PropostaId, _
                                                Truncadecimal(frmEndosso.oDadosEndosso.ValRestituicao), _
                                                0, _
                                                80, _
                                                "Avalia��o para restitui��o a pedido da seguradora ", _
                                                "Finalizado", _
                                                "Processo Autom�tico do Endosso de Contesta��o")

        Set oExtrato = Nothing
    End If
    Exit Sub
    'Alexandre Debouch - Confitec - EdsContestacao
TrataErro:

    Call TratarErro("IncluirEndossoContesta��o")
    FinalizarAplicacao
End Sub

Sub IncluirEndossoContestacaoGeracaoRestituicao()
    On Erro GoTo TrataErro
    MousePointer = vbHourglass

    Dim iNumAvaliacao As Integer
    Dim rsAvaliacao As Recordset
    Dim oAvaliacao As Object
    Dim OEndosso As Object
    Dim oBeneficiario As Object
    Dim oFinanceiro As Object
    Dim oRetencao As Object
    Dim lEndossoId As Long
    Dim lEventoId As Long
    Dim lConexao As Long
    Dim lTipoCobertura As Long
    Dim lClienteId As Long
    Dim lTpEndossoId_ALS As Long
    Dim sRamoId As String
    Dim sConvenio As String
    Dim sFlagAprovacao As String
    Dim iMoedaPremioId As Integer
    Dim cPercIR As Currency
    Dim cValTotIRCorretagem As Currency
    Dim cValRepasseBB As Currency
    Dim bFlagRetemComissao As Boolean
    Dim bFlagRetemProLabore As Boolean
    Dim bFlagRetemIOF As Boolean
    Dim oProposta As Object
    Dim rsProposta As Recordset
    Dim cValIrRemuneracao As Currency
    Dim cPercIRRemun As Currency
    Dim lAdmClienteId As Long
    Dim oObjetoVazioAgendamento As Object
    Dim sTipoEmissao As String
    Dim lSeguradora As Long
    Dim colCliente As Collection

    Const TP_OPERACAO_RESTITUICAO = "03"
    Const COD_MOV_FINANC = 3

    Set oAvaliacao = CreateObject("SEGL0026.cls00125")
    Set rsAvaliacao = oAvaliacao.ConsultarAvaliacaoRestituicao(gsSIGLASISTEMA, _
                                                               App.Title, _
                                                               App.FileDescription, _
                                                               glAmbiente_id, _
                                                               "A", _
                                                               frmEndosso.oDadosEndosso.produtoId, _
                                                               frmEndosso.oDadosEndosso.PropostaId)

    Set oAvaliacao = Nothing

    If rsAvaliacao.EOF Then
        Set rsAvaliacao = Nothing
        Exit Sub
    End If

    lPropostaId = rsAvaliacao!proposta_id
    lNumAvaliacao = rsAvaliacao!Num_avaliacao
    lProdutoId = rsAvaliacao!Produto_id
    lTipoCobertura = IIf(IsNull(rsAvaliacao!tp_cobertura), 0, rsAvaliacao!tp_cobertura)
    sDtContratacao = rsAvaliacao!dt_contratacao
    sDtInicioVig = rsAvaliacao!Dt_Inicio_Vigencia
    sDtFimVig = rsAvaliacao!Dt_Fim_Vigencia
    sDtPedidoAvaliacao = rsAvaliacao!dt_pedido_avaliacao
    cValCustoApoliceCertifRest = rsAvaliacao!custo_apolice
    cValRemuneracao = rsAvaliacao!val_remuneracao
    cValTotCorretagemRest = rsAvaliacao!Val_Comissao
    cValRestituicao = rsAvaliacao!Val_Restituicao
    cValPremioTarifaRest = rsAvaliacao!val_premio_tarifa
    cValIsRestituicao = rsAvaliacao!val_is
    iOrigemProposta = rsAvaliacao!Origem
    cValCambio = rsAvaliacao!val_cambio

    sFlagTpCliente = "CLIENTE"
    sTextoEndosso = "Endosso de restitui��o a pedido da seguradora - Eds. Contesta��o"
    iTpEndossoId = 80
    sRamoId = frmEndosso.oDadosEndosso.RamoId
    iMoedaPremioId = frmEndosso.oDadosEndosso.PremioMoedaId
    sTipoEmissao = frmEndosso.oDadosEndosso.TpEmissao
    lSeguradora = frmEndosso.oDadosEndosso.SeguradoraCodSusep
    cPercIR = frmEndosso.oDadosEndosso.PercIR
    cPercIRRemun = cValIrRemuneracao
    cValIrRemuneracao = Truncadecimal(cValRemuneracao * cPercIRRemun)
    cValTotIRCorretagem = Truncadecimal(cValTotCorretagemRest * cPercIR)

    Set oBeneficiario = CreateObject("SEGL0026.cls00126")
    lClienteId = oBeneficiario.ObterBeneficiario(gsSIGLASISTEMA, _
                                                 App.Title, _
                                                 App.FileDescription, _
                                                 glAmbiente_id, _
                                                 rsAvaliacao!proposta_id)
    Set oBeneficiario = Nothing

    If rdocn Is Nothing Then
        Call Conexao
    Else
        If rdocn.State = 0 Then Call Conexao
    End If

    sConvenio = Obtem_Numero_do_Convenio(Val(lProdutoId), Val(sRamoId), 1, iMoedaPremioId)

    rdocn.Close
    Set rdocn = Nothing
    Set rdocn_Seg = Nothing
    Set rdocn_Seg2 = Nothing


    Call CarregarCongeneres(lPropostaId)

    Set oRetencao = CreateObject("SEGL0026.cls00125")
    Call oRetencao.VerificarRetencao(gsSIGLASISTEMA, _
                                     App.Title, _
                                     App.FileDescription, _
                                     glAmbiente_id, _
                                     sConvenio, _
                                     bFlagRetemComissao, _
                                     bFlagRetemIOF, _
                                     bFlagRetemProLabore)
    Set oRetencao = Nothing
    lAdmClienteId = Obter_Estipulante(lPropostaId)

    cValRepasseBB = CalcularRepasse(cValTotCorretagemRest, _
                                    cValProLaboreRest, _
                                    cValRestituicao, _
                                    cPercIR, _
                                    rsAvaliacao!Val_IOF, _
                                    bFlagRetemComissao, _
                                    bFlagRetemIOF)

    cValRepasseBB = cValRepasseBB + cValIrRemuneracao - cValRemuneracao
    If cValRepasseBB = 0 Then
        cValRepasseBB = cValRestituicao
    End If
    sFlagAprovacao = ""

    Set oProposta = CreateObject("SEGL0022.cls00117")
    Set rsProposta = oProposta.ConsultarProposta(gsSIGLASISTEMA, _
                                                 App.Title, _
                                                 App.FileDescription, _
                                                 glAmbiente_id, _
                                                 lPropostaId)
    If Not rsProposta.EOF Then
        iOrigemProposta = IIf(IsNull(rsProposta("origem_proposta_id")), 0, rsProposta("origem_proposta_id"))
    End If
    rsProposta.Close
    Set rsProposta = Nothing
    Set oProposta = Nothing

    If sFlagTpCliente <> "CLIENTE" Then    'Caso seja um endosso
        lRestEndossoId = 0
    End If

    Call CarregarRateioCorretagemRestituicao(lPropostaId, _
                                             lNumAvaliacao, _
                                             ByVal cValRestituicao, _
                                             ByVal cValIOF, _
                                             ByVal cValCustoApoliceCertif, _
                                             ByVal cPercIR)


    Set oObjetoVazioAgendamento = New Collection

    Set OEndosso = CreateObject("SEGL0026.cls00124")
    lConexao = OEndosso.AbrirConexao("SEGBR", glAmbiente_id, App.Title, App.FileDescription)
    If Not OEndosso.AbrirTransacao(lConexao) Then GoTo TrataErro


    lEndossoId = OEndosso.EmitirEndosso( _
                 "SEGP0777", gsSIGLASISTEMA, App.Title, _
                 App.FileDescription, glAmbiente_id, CLng(frmEndosso.oDadosEndosso.ApoliceId), _
                 CLng(frmEndosso.oDadosEndosso.SucursalSeguradoraId), CLng(frmEndosso.oDadosEndosso.SeguradoraCodSusep), lPropostaId, _
                 CLng(sRamoId), cValRestituicao, CLng(iTpEndossoId), _
                 "n", 0, CCur(frmEndosso.oDadosEndosso.PercProLabore), _
                 1, cValPremioTarifaRest, TP_OPERACAO_RESTITUICAO, _
                 "", "", "", _
                 cValCustoApoliceCertifRest, cValTotCorretagemRest, 0, _
                 cValProLaboreRest, cValIOFRest, cValTotIRCorretagem, _
                 cValDescontoRest, cValAdicFracionamentoRest, "", _
                 iMoedaPremioId, iMoedaPremioId, cValCambio, _
                 cValIsRestituicao, sDtPedidoAvaliacao, Data_Sistema, _
                 lClienteId, cPercIR, cValRepasseBB, _
                 cPercIR, 0, 0, _
                 Corretagens, oObjetoVazioAgendamento, 0, _
                 lConexao, Format(Data_Sistema, "yyyy/mm"), sFlagAprovacao, _
                 sTextoEndosso, CLng(lRestEndossoId), 0, _
                 0, 0, cPercIR, _
                 bFlagRetemComissao, bFlagRetemProLabore, False, _
                 Congeneres, sTipoEmissao)

    Call OEndosso.IncluirLogContestacao(gsSIGLASISTEMA, _
                                        App.Title, _
                                        App.FileDescription, _
                                        glAmbiente_id, _
                                        frmEndosso.oDadosEndosso.produtoId, _
                                        frmEndosso.oDadosEndosso.PropostaId, _
                                        lEndossoId, _
                                        80, _
                                        Data_Sistema, _
                                        frmEndosso.mskDtEmissao.Text, _
                                        cUserName)

    Call OEndosso.AtualizarValoresDeRemuneracao(gsSIGLASISTEMA, _
                                                glAmbiente_id, _
                                                App.Title, _
                                                App.FileDescription, _
                                                lPropostaId, _
                                                lEndossoId, _
                                                cValRemuneracao, _
                                                cValIrRemuneracao, _
                                                lConexao)

    Call OEndosso.GerarEndossoAls("SEGP0777", _
                                  gsSIGLASISTEMA, _
                                  App.Title, _
                                  App.FileDescription, _
                                  glAmbiente_id, _
                                  Data_Sistema, _
                                  rsAvaliacao!proposta_id, _
                                  Format(Data_Sistema, "yyyymm"), _
                                  Data_Sistema, _
                                  lEndossoId, _
                                  rsAvaliacao!Val_Restituicao, _
                                  80, _
                                  COD_MOV_FINANC, _
                                  0, _
                                  lEventoId, _
                                  Data_Sistema, _
                                  lConexao, _
                                  lTipoCobertura)

    Call OEndosso.Insere_Tabelas_Circular("SEGP0777", _
                                          gsSIGLASISTEMA, _
                                          App.Title, _
                                          App.FileDescription, _
                                          glAmbiente_id, _
                                          lPropostaId, _
                                          lEndossoId, _
                                          lConexao)



    Dim oExtrato As Object
    Set oExtrato = CreateObject("SEGL0026.cls00125")

    Call oExtrato.AtualizarAvaliacaoRestituicao(gsSIGLASISTEMA, App.Title, App.FileDescription, glAmbiente_id, _
                                                "SEGP0777", Data_Sistema, lPropostaId, lNumAvaliacao, "A", _
                                                cValPremioTarifa, cValCustoApoliceCertif, cValAdicFracionamento, _
                                                cValDesconto, cValIOF, cValRestituicao, cValTotCorretagem, _
                                                cValProLabore, iTpAvaliacaoId, sMotivoRecusaGerencial, _
                                                lEndossoId, sRestituicaoIntegral, sPossuiSubvencao, cValSubvencaoFederal, _
                                                cValSubvencaoEstadual, lConexao, cValRemuneracao)


    Call oExtrato.InserirExtratoRestituicao(gsSIGLASISTEMA, _
                                            App.Title, _
                                            App.FileDescription, _
                                            glAmbiente_id, _
                                            "SEGP0777", _
                                            frmEndosso.oDadosEndosso.PropostaId, _
                                            "0.00", _
                                            lEndossoId, _
                                            0, _
                                            "Endosso de restitui��o a pedido da seguradora", _
                                            "Finalizado", _
                                            "Processo Autom�tico do Endosso de Contesta��o")


    If oExtrato.obterLogEdsContestacao(gsSIGLASISTEMA, App.Title, App.FileDescription, glAmbiente_id, lConexao, lProdutoId, lPropostaId) Then
        lMovimentacaoId = oExtrato.obterMovimentacao(gsSIGLASISTEMA, App.Title, App.FileDescription, glAmbiente_id, lConexao, lPropostaId, lEndossoId)
        If lMovimentacaoId > 0 Then
            Call oExtrato.GerarPreRemessa(gsSIGLASISTEMA, App.Title, App.FileDescription, glAmbiente_id, lConexao, "SEGP0777", lMovimentacaoId)
            Call oExtrato.LiberarRemessa(gsSIGLASISTEMA, App.Title, App.FileDescription, glAmbiente_id, lConexao, lMovimentacaoId)
        End If
    End If

    '    Call oExtrato.InserirExtratoRestituicao(gsSIGLASISTEMA, _
         '                                            App.Title, _
         '                                            App.FileDescription, _
         '                                            glAmbiente_id, _
         '                                            "SEGP0777", _
         '                                            frmEndosso.oDadosEndosso.PropostaId, _
         '                                            "0.00", _
         '                                            lEndossoId, _
         '                                            0, _
         '                                            "Pr�-Remessa e remessa de restitui��o a pedido da seguradora", _
         '                                            "Aprovados", _
         '                                            "Processo Autom�tico do Endosso de Contesta��o")

    Call oExtrato.InserirExtratoRestituicao(gsSIGLASISTEMA, _
                                            App.Title, _
                                            App.FileDescription, _
                                            glAmbiente_id, _
                                            "SEGP0777", _
                                            frmEndosso.oDadosEndosso.PropostaId, _
                                            "0.00", _
                                            lEndossoId, _
                                            0, _
                                            "Aguardar gera��o do arquivo 452 para envio ao BB", _
                                            "Em espera", _
                                            "Processo Autom�tico do Endosso de Contesta��o")

    OEndosso.ConfirmarTransacao (lConexao)
    OEndosso.FecharConexao (lConexao)

    Set OEndosso = Nothing
    Set oExtrato = Nothing

    MousePointer = vbDefault

    Exit Sub
TrataErro:

    OEndosso.RetornarTransacao (lConexao)
    OEndosso.FecharConexao (lConexao)

    Set OEndosso = Nothing

    Call TrataErroGeral("EndossoContestacaoAprovarRestituicao")
    Call TerminaSEGBR

End Sub

Sub ProcessarEmissaoEndosso(ByVal lPropostaId As Long, _
                            ByVal lProdutoId As Long, _
                            ByVal lNumAvaliacao As Long, _
                            ByVal lClienteId As Long, _
                            ByVal lRestEndossoId As Long, _
                            ByVal sFlagTpCliente As String, _
                            ByVal cValRestituicao As Currency, _
                            ByVal cValIOFRest As Currency, _
                            ByVal cValCustoApoliceCertifRest As Currency, _
                            ByVal cValTotCorretagemRest As Currency, _
                            ByVal cValProLaboreRest As Currency, _
                            ByVal cValPremioTarifaRest As Currency, _
                            ByVal cValDescontoRest As Currency, _
                            ByVal cValAdicFracionamentoRest As Currency, _
                            ByVal cValIsRestituicao As Currency, _
                            ByVal cValCambio As Currency, _
                            ByVal sDtPedidoAvaliacao As String, _
                            ByVal sTextoEndosso As String, _
                            ByRef iOrigemProposta As Integer, _
                            ByVal iTpEndossoId As Integer, _
                            ByRef lEndossoId As Long, _
                            ByVal lConexao As Long, _
                            ByRef OEndosso As Object, _
                            ByRef cValRemuneracao As Currency)

    Const TP_OPERACAO_RESTITUICAO = "03"

    Dim sRamoId As String
    Dim sConvenio As String
    Dim sFlagAprovacao As String
    Dim iMoedaPremioId As Integer
    Dim cPercIR As Currency
    Dim cValTotIRCorretagem As Currency
    Dim cValRepasseBB As Currency
    Dim bFlagRetemComissao As Boolean
    Dim bFlagRetemProLabore As Boolean
    Dim bFlagRetemIOF As Boolean
    Dim oProposta As Object
    Dim rsProposta As Recordset
    Dim cValIrRemuneracao As Currency
    Dim cPercIRRemun As Currency
    Dim lAdmClienteId As Long
    Dim oObjetoVazioAgendamento As Object
    Dim sTipoEmissao As String
    Dim lSeguradora As Long
    Dim colCliente As Collection

    On Error GoTo TrataErro

    sRamoId = frmEndosso.oDadosEndosso.RamoId
    iMoedaPremioId = frmEndosso.oDadosEndosso.PremioMoedaId
    sTipoEmissao = frmEndosso.oDadosEndosso.TpEmissao
    lSeguradora = frmEndosso.oDadosEndosso.SeguradoraCodSusep
    cPercIR = frmEndosso.oDadosEndosso.PercIR
    cValCustoApoliceCertifRest = frmEndosso.oDadosEndosso.valCustoApoliceCert
    cPercIRRemun = cValIrRemuneracao
    cValIrRemuneracao = Truncadecimal(cValRemuneracao * cPercIRRemun)
    cValTotIRCorretagem = Truncadecimal(cValTotCorretagemRest * cPercIR)
    Call CarregarCongeneres(lPropostaId)
    sConvenio = Obtem_Numero_do_Convenio(Val(lProdutoId), Val(sRamoId), 1, iMoedaPremioId)

    cValRepasseBB = CalcularRepasseSemIrEstipulante(cValTotCorretagemRest, _
                                                    cValProLaboreRest, _
                                                    cValRestituicao, _
                                                    cPercIR, _
                                                    cValIOFRest, _
                                                    bFlagRetemComissao, _
                                                    bFlagRetemIOF)

    cValRepasseBB = cValRepasseBB + cValIrRemuneracao - cValRemuneracao

    Set oProposta = CreateObject("SEGL0022.cls00117")
    Set rsProposta = oProposta.ConsultarProposta(gsSIGLASISTEMA, _
                                                 App.Title, _
                                                 App.FileDescription, _
                                                 glAmbiente_id, _
                                                 lPropostaId)

    sFlagAprovacao = "N"
    If Not rsProposta.EOF Then
        iOrigemProposta = IIf(IsNull(rsProposta("origem_proposta_id")), 0, rsProposta("origem_proposta_id"))
    End If

    iTpEndossoId = 80

    If sFlagTpCliente <> "CLIENTE" Then    'Caso seja um endosso
        lRestEndossoId = 0
    End If

    Set oObjetoVazioAgendamento = New Collection
    '
    '    lEndossoId = OEndosso.EmitirEndosso(cUserName, gsSIGLASISTEMA, App.Title, App.FileDescription, glAmbiente_id, _
         '                                        0, 0, lSeguradora, lPropostaId, Val(sRamoId), cValRestituicao, _
         '                                        iTpEndossoId, "n", 0, 0, 1, cValPremioTarifaRest, _
         '                                        TP_OPERACAO_RESTITUICAO, "", "", "", _
         '                                        cValCustoApoliceCertifRest, cValTotCorretagemRest, _
         '                                        0, cValProLaboreRest, cValIOFRest, _
         '                                        cValTotIRCorretagem, cValDescontoRest, _
         '                                        cValAdicFracionamentoRest, "", _
         '                                        iMoedaPremioId, iMoedaPremioId, _
         '                                        cValCambio, cValIsRestituicao, _
         '                                        sDtPedidoAvaliacao, Data_Sistema, _
         '                                        lClienteId, cPercIR, cValRepasseBB, _
         '                                        0, 0, 0, Corretagens, _
         '                                        oObjetoVazioAgendamento, 0, lConexao, _
         '                                        , sFlagAprovacao, sTextoEndosso, lRestEndossoId, , , , , _
         '                                        bFlagRetemComissao, bFlagRetemProLabore, , Congeneres, sTipoEmissao)

    Call OEndosso.AtualizarValoresDeRemuneracao(gsSIGLASISTEMA, _
                                                glAmbiente_id, _
                                                App.Title, _
                                                App.FileDescription, _
                                                lPropostaId, _
                                                lEndossoId, _
                                                cValRemuneracao, _
                                                cValIrRemuneracao, _
                                                lConexao)


    rsProposta.Close
    Set rsProposta = Nothing
    Set oProposta = Nothing
    'Set colCorretagem = Nothing
    'Set colCongeneres = Nothing
    Exit Sub

TrataErro:
    Call Err.Raise(Err.Number, , "ProcessarEmissaoEndosso - " & Err.Description)

End Sub





