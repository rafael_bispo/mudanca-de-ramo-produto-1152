VERSION 5.00
Object = "{C932BA88-4374-101B-A56C-00AA003668DC}#1.1#0"; "MSMASK32.ocx"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.ocx"
Begin VB.Form frmReativacaoReagendamento 
   Caption         =   "Reativa��o Proposta / Reagendamento"
   ClientHeight    =   8310
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   12510
   LinkTopic       =   "Form1"
   ScaleHeight     =   8310
   ScaleWidth      =   12510
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton cmdExcluir 
      Caption         =   "Excluir >>"
      Height          =   465
      Left            =   7515
      TabIndex        =   3
      Top             =   2700
      Width           =   1365
   End
   Begin VB.CommandButton cmdAlterar 
      Caption         =   "<< Alterar >>"
      Height          =   465
      Left            =   7515
      TabIndex        =   2
      Top             =   2025
      Width           =   1365
   End
   Begin VB.CommandButton cmdIncluir 
      Caption         =   "<< Incluir"
      Height          =   420
      Left            =   7515
      TabIndex        =   1
      Top             =   1440
      Width           =   1365
   End
   Begin VB.CommandButton cmdSair 
      Caption         =   "&Sair"
      Height          =   465
      Left            =   8640
      TabIndex        =   5
      Top             =   7515
      Width           =   1560
   End
   Begin VB.Frame FrmReagendamento 
      Height          =   8160
      Left            =   90
      TabIndex        =   4
      Top             =   45
      Width           =   11940
      Begin VB.TextBox txtDataReativacao 
         Enabled         =   0   'False
         Height          =   285
         Left            =   585
         TabIndex        =   9
         Top             =   540
         Width           =   1410
      End
      Begin VB.Frame frmParcelas 
         Caption         =   "Parcelas:"
         Height          =   6090
         Left            =   360
         TabIndex        =   7
         Top             =   1080
         Width           =   8610
         Begin VB.CommandButton cmdReagenda 
            Caption         =   "Reagendamento Autom�tico"
            Height          =   465
            Left            =   7065
            TabIndex        =   16
            Top             =   2250
            Width           =   1365
         End
         Begin MSMask.MaskEdBox mskValParcela 
            Height          =   285
            Left            =   360
            TabIndex        =   15
            Top             =   5535
            Width           =   1050
            _ExtentX        =   1852
            _ExtentY        =   503
            _Version        =   393216
            Enabled         =   0   'False
            Format          =   "#,##0.00;(#,##0.00)"
            PromptChar      =   "_"
         End
         Begin MSMask.MaskEdBox mskDtParcela 
            Height          =   285
            Left            =   1980
            TabIndex        =   0
            Top             =   5535
            Width           =   1365
            _ExtentX        =   2408
            _ExtentY        =   503
            _Version        =   393216
            MaxLength       =   10
            Mask            =   "##/##/####"
            PromptChar      =   "_"
         End
         Begin MSFlexGridLib.MSFlexGrid grdParcelas 
            Height          =   4785
            Left            =   360
            TabIndex        =   14
            Top             =   315
            Width           =   6495
            _ExtentX        =   11456
            _ExtentY        =   8440
            _Version        =   393216
            Rows            =   1
            Cols            =   6
            FixedCols       =   0
            Enabled         =   -1  'True
            FormatString    =   "N� Parcelas |Valor Parcela     |Data Parcela     |Ano Mes Referencia |Data Baixa   |Situacao "
         End
         Begin VB.TextBox txtMesReferencia 
            Enabled         =   0   'False
            Height          =   285
            Left            =   4005
            TabIndex        =   8
            Top             =   5535
            Width           =   1320
         End
         Begin VB.Label lblMesReferencia 
            Caption         =   "M�s Refer�ncia:"
            Height          =   510
            Left            =   4005
            TabIndex        =   13
            Top             =   5220
            Width           =   1635
         End
         Begin VB.Label lblDataParcelas 
            Caption         =   "Data Parcela:"
            Height          =   465
            Left            =   1980
            TabIndex        =   12
            Top             =   5220
            Width           =   1545
         End
         Begin VB.Label lblValorParcela 
            Caption         =   "Val. Parcela:"
            Height          =   330
            Left            =   360
            TabIndex        =   11
            Top             =   5220
            Width           =   1365
         End
      End
      Begin VB.CommandButton cmdAplicar 
         Caption         =   "&Aplicar"
         Height          =   465
         Left            =   10260
         TabIndex        =   6
         Top             =   7470
         Width           =   1545
      End
      Begin VB.Label lblDataReativacao 
         Caption         =   "Data Reativa��o:"
         Height          =   330
         Left            =   675
         TabIndex        =   10
         Top             =   225
         Width           =   2220
      End
   End
End
Attribute VB_Name = "frmReativacaoReagendamento"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Public PropostaId As Long
Public EndossoId As Long
Dim dataAlterada As Date
Dim dataHoje As Date
Dim mesRef As String
Dim ultimaParcela As Integer
Dim maiorMesRef As String
Dim ultimoValor As String
Public TP_EVENTO_ENDOSSO As Long
Public ApoliceId As Long
Public RamoId As Long
Public produtoId As Long
Public ENDOSSO_EVENTO As Long
Public txtDescEndosso As String
' 04/12/2015 - Schoralick (nova consultoria) - 18690344 - Melhorias Processo Reagendamento e Reativa��o BESC - inicio
Public vlDataReagendamento As String
Public vlRetornoReagendamento As Integer

Public Enum EnumValidaParcela
    EnumValidInadimplenciaSAP = 1
    EnumValidEndossoAtrelado = 2
    EnumValidPendente = 3
    EnumValidDataReagendamento = 4
    EnumValidVigencia = 5
    EnumValidVlParcela = 6
End Enum

Public Enum EnumOpLinhaRetorno
    EnumOpUmaLinha = 1
    EnumOpTodasLinhas = 2
End Enum
' fim - 18690344 - Melhorias Processo Reagendamento e Reativa��o BESC

Private Sub cmdAlterar_Click()

    Dim numseq As Integer
    '08/12/2015 - Schoralick (nova consultoria) - 18690344 - Melhorias Processo Reagendamento e Reativa��o BESC - inicio
    Dim sValAntCol1 As String
    Dim sValAntCol2 As String
    Dim sValAntCol5 As String
    Dim sValAntMesRef As String
    Dim sLinhas As String
    Dim bImpedido As Boolean
    Dim i As Integer
    Dim lMsg As String
    ' fim - 18690344 - Melhorias Processo Reagendamento e Reativa��o BESC

    numseq = grdParcelas.TextMatrix(grdParcelas.Row, 0)
    mesRef = Mid$(mskDtParcela.Text, 4, 7)

    If grdParcelas.TextMatrix(grdParcelas.Row, 4) = "" Then

        If VerificaData2(mskDtParcela.Text) Then
            Select Case produtoId
            Case 200, 201, 202, 203, 204, 205, 208, 209, 210, 211, 213, 216, 217, 218, 219, 221, 222, 223, 224, 225, 226
                If (CDate(mskDtParcela.Text) > (CDate(Data_Sistema) + 15)) Then    '08/12/2015 - Schoralick (nova consultoria) - 18690344 - Melhorias Processo Reagendamento e Reativa��o BESC
                    dataAlterada = CDate(mskDtParcela.Text)
                    dataHoje = CDate(Data_Sistema)

                    For i = 1 To grdParcelas.Rows - 1
                        If grdParcelas.TextMatrix(i, 0) = numseq Then
                            '08/12/2015 - Schoralick (nova consultoria) - 18690344 - Melhorias Processo Reagendamento e Reativa��o BESC - inicio
                            sValAntCol1 = grdParcelas.TextMatrix(i, 1)
                            sValAntCol2 = grdParcelas.TextMatrix(i, 2)
                            sValAntCol5 = grdParcelas.TextMatrix(i, 5)
                            sValAntMesRef = txtMesReferencia
                            ' fim - 18690344 - Melhorias Processo Reagendamento e Reativa��o BESC

                            grdParcelas.TextMatrix(i, 1) = mskValParcela
                            grdParcelas.TextMatrix(i, 2) = mskDtParcela
                            If grdParcelas.TextMatrix(i, 5) <> "i" Then
                                grdParcelas.TextMatrix(i, 5) = "a"
                            End If
                            txtMesReferencia = mesRef

                            '08/12/2015 - Schoralick (nova consultoria) - 18690344 - Melhorias Processo Reagendamento e Reativa��o BESC - inicio
                            bImpedido = False

                            sLinhas = converteGrdParcelasEmLinha(i, EnumOpUmaLinha)

                            lMsg = ValidaParcela(sLinhas)
                            If lMsg <> "" Then
                                grdParcelas.TextMatrix(i, 1) = sValAntCol1
                                grdParcelas.TextMatrix(i, 2) = sValAntCol2
                                grdParcelas.TextMatrix(i, 5) = sValAntCol5

                                MsgBox (lMsg)

                                Exit Sub
                            End If
                            ' fim - 18690344 - Melhorias Processo Reagendamento e Reativa��o BESC
                        End If
                    Next i
                Else    '08/12/2015 - Schoralick (nova consultoria) - 18690344 - Melhorias Processo Reagendamento e Reativa��o BESC
                    MsgBox ("N�o � permitido data de parcela menor que a data atual + 15 dias. Ou seja, a data informada deve ser superior �: " & Format((CDate(Data_Sistema) + 15), "dd/mm/yyyy"))
                End If
            Case Else
                For i = 1 To grdParcelas.Rows - 1

                    If grdParcelas.TextMatrix(i, 0) = numseq Then

                        grdParcelas.TextMatrix(i, 1) = mskValParcela
                        grdParcelas.TextMatrix(i, 2) = mskDtParcela
                        If grdParcelas.TextMatrix(i, 5) <> "i" Then
                            grdParcelas.TextMatrix(i, 5) = "a"
                        End If
                        txtMesReferencia = mesRef
                    End If
                Next i

            End Select
        Else
            MsgBox ("Data Ivalida!")
        End If


    Else
        MsgBox ("N�o � possivel alterar parcela baixada!")

    End If

    mskValParcela = ultimoValor
    txtMesReferencia = mesReferencia(grdParcelas.TextMatrix(1, 3))
End Sub

Private Sub cmdAplicar_Click()

    Set oEndossoReativacao = CreateObject("SEGL0140.cls00172")
    Dim linhas As String
    Dim l As Integer
    Dim data As String
    Dim mes_ref As String
    Dim msg As String


    msg = MsgBox("Confirma aplicar endosso?", vbYesNo + vbExclamation, "Caixa de Mensagem")    ' Mostrar� uma caixa de mensagem com bot�o Sim n�o, com titulo e com a foto exclama��o, para checar se clicou em yes fa�a isso.

    If msg = vbYes Then


        If comparaData() Then

            linhas = ""

            For l = 1 To grdParcelas.Rows - 1

                linhas = linhas & "(" & PropostaId & ","
                linhas = linhas & grdParcelas.TextMatrix(l, 0) & ","    'num_parcela
                'linhas = linhas & IIf(grdParcelas.TextMatrix(l, 5) = "", "'i'", "'" & grdParcelas.TextMatrix(l, 5) & "'") & "," ' situacao
                linhas = linhas & "'" & grdParcelas.TextMatrix(l, 5) & "'" & ","    ' situacao



                'linhas = linhas & grdParcelas.TextMatrix(l, 1) & "," val_parcela
                mes_ref = Mid$(grdParcelas.TextMatrix(l, 3), 4, 4) & Mid$(grdParcelas.TextMatrix(l, 3), 1, 2)
                linhas = linhas & mes_ref & ","  'mes_ano_ref
                data = Mid$(grdParcelas.TextMatrix(l, 2), 7, 4) & "-" & Mid$(grdParcelas.TextMatrix(l, 2), 4, 2) & "-" & Mid$(grdParcelas.TextMatrix(l, 2), 1, 2)
                linhas = linhas & "'" & data & "',"    'data_parcela
                linhas = linhas & l & ")|"
                '   linhas = linhas & grdParcelas.TextMatrix(l, 1) & ")|"
            Next l
            '(proposta_id, num_cobranca, situacao, ano_mes_ref,data_parcela, seq)

            If EndossoId = 101 Then

                Call oEndossoReativacao.atualizarParcelas(gsSIGLASISTEMA, _
                                                          App.Title, _
                                                          App.FileDescription, _
                                                          glAmbiente_id, _
                                                          linhas, _
                                                          101, _
                                                          PropostaId, _
                                                          Trim(cUserName), _
                                                          txtDescEndosso)

            End If

            Select Case produtoId
            Case 200, 201, 202, 203, 204, 205, 208, 209, 210, 211, 213, 216, 217, 218, 219, 221, 222, 223, 224, 225, 226
                If EndossoId = 200 Then
                    '08/12/2015 - Schoralick (nova consultoria) - 18690344 - Melhorias Processo Reagendamento e Reativa��o BESC - inicio
                    linhas = converteGrdParcelasEmLinha(1, EnumOpTodasLinhas)    '08/12/2015 - Schoralick (nova consultoria) - 18690344 - Melhorias Processo Reagendamento e Reativa��o BESC

                    msg = ValidaParcela(linhas)
                    If lMsg <> "" Then
                        MsgBox (msg)
                        Set oEndossoReativacao = Nothing
                        Exit Sub
                    End If

                    Call oEndossoReativacao.atualizarParcelasBesc(gsSIGLASISTEMA, _
                                                                  App.Title, _
                                                                  App.FileDescription, _
                                                                  glAmbiente_id, _
                                                                  linhas, _
                                                                  200, _
                                                                  PropostaId, _
                                                                  Trim(cUserName), _
                                                                  txtDescEndosso)
                    ' fim - 18690344 - Melhorias Processo Reagendamento e Reativa��o BESC - inicio
                End If
            Case Else
                If EndossoId = 200 Then
                    Call oEndossoReativacao.atualizarParcelas(gsSIGLASISTEMA, _
                                                              App.Title, _
                                                              App.FileDescription, _
                                                              glAmbiente_id, _
                                                              linhas, _
                                                              200, _
                                                              PropostaId, _
                                                              Trim(cUserName), _
                                                              txtDescEndosso)

                End If

                Call IncluirEvento(TP_EVENTO_ENDOSSO, _
                                   ApoliceId, _
                                   RamoId, _
                                   produtoId, _
                                   PropostaId, _
                                   ENDOSSO_EVENTO, _
                                   Format(Data_Sistema, "yyyymmdd"), _
                                   rdocn)
            End Select

            MsgBox ("Endosso Efetivado!")
            Set oEndossoReativacao = Nothing

            Call Unload(frmEndosso)
            frmSelecao.Show
            Call Unload(Me)

        Else
            MsgBox ("N�o pode incluir parcelas com data de pagamento menor que a data atual + 5 dias!")

        End If
    End If
    Set oEndossoReativacao = Nothing

End Sub

Private Sub cmdExcluir_Click()

    If EndossoId = 200 Then
        MsgBox ("N�o � possivel excluir parcelas a serem reagendandas!")

    Else
        If grdParcelas.TextMatrix(grdParcelas.RowSel, 5) = "i" Then
            grdParcelas.RemoveItem (grdParcelas.RowSel)
        Else

            If grdParcelas.TextMatrix(grdParcelas.RowSel, 5) = "a" Then
                MsgBox ("N�o � possivel excluir parcelas que nao tenham sido inseridas nesta tela!")
            End If

            If grdParcelas.TextMatrix(grdParcelas.RowSel, 5) = "" And grdParcelas.TextMatrix(grdParcelas.RowSel, 4) = "" Then
                MsgBox ("N�o � possivel excluir parcelas que nao tenham sido inseridas nesta tela!")
            End If

            If grdParcelas.TextMatrix(grdParcelas.RowSel, 4) <> "i" And grdParcelas.TextMatrix(grdParcelas.RowSel, 4) <> "a" And grdParcelas.TextMatrix(grdParcelas.RowSel, 4) <> "" Then
                MsgBox ("N�o � possivel excluir parcelas ja baixadas!")
            End If

        End If

        maiorMesRef = grdParcelas.TextMatrix(1, 3)

        txtMesReferencia = mesReferencia(maiorMesRef)
        stemp = mskDtParcela.mask
        mskDtParcela.mask = ""
        mskDtParcela.Text = ""
        mskDtParcela.mask = stemp


    End If

End Sub

Private Sub cmdIncluir_Click()

    Dim sLinha As String

    If EndossoId = 200 Then
        MsgBox ("N�o � possivel incluir parcelas para reagendamento atraves do endosso selecionado! ")
    Else

        If (mskValParcela.ClipText = "") Or (mskDtParcela.ClipText = "") Then
            MsgBox ("Campo em branco favor verificar!")
        Else
            If VerificaData2(mskDtParcela.Text) Then

                dataAlterada = CDate(mskDtParcela.Text)
                dataHoje = CDate(Data_Sistema)
                mesRef = mesReferencia(maiorMesRef)


                sLinha = ""
                sLinha = sLinha & grdParcelas.TextMatrix(1, 0) + 1 & vbTab
                sLinha = sLinha & mskValParcela & vbTab
                sLinha = sLinha & mskDtParcela.Text & vbTab
                sLinha = sLinha & mesRef & vbTab
                sLinha = sLinha & "" & vbTab
                sLinha = sLinha & "i"

                grdParcelas.AddItem (sLinha)

                maiorMesRef = mesReferencia(grdParcelas.TextMatrix(1, 3))
                grdParcelas.Col = 0
                grdParcelas.Sort = flexSortNumericDescending

                mskValParcela = ultimoValor

                txtMesReferencia = mesReferencia(maiorMesRef)
                ultimaParcela = ultimaParcela + 1


            Else
                MsgBox ("Data invalida!")
            End If
        End If



    End If

End Sub

Private Sub cmdSair_Click()
    frmEndosso.Show
    frmEndosso.Enabled = True
    Me.Hide
    Call Unload(Me)
End Sub

Private Function mesReferencia(ByVal data As String) As String

    Dim dataaux As String
    Dim mesAux As String
    Dim anoAux As String

    dataaux = data
    anoAux = CInt(Right(dataaux, 4))

    If Len(dataaux) > 7 Then
        mesAux = CInt(Mid$(dataaux, 4, 2))
    Else
        mesAux = Left$(dataaux, Len(dataaux) - 5)
    End If

    If mesAux < 12 Then
        mesAux = mesAux + 1

        If mesAux < 10 Then
            mesAux = Trim$(Str(mesAux))
            mesAux = "0" & mesAux
            dataaux = mesAux & "/" & Trim(Str(anoAux))
        Else
            dataaux = Trim(Trim(mesAux)) & "/" & Trim(Str(anoAux))
        End If

        mesReferencia = dataaux


    Else
        anoAux = anoAux + 1
        dataaux = "01/" & Trim(Str(anoAux))
        mesReferencia = dataaux
    End If


End Function


Private Sub cmdReagenda_Click()
    vlRetornoReagendamento = 0
    frmReagendamentoEmMassa.Show vbModal
    If vlRetornoReagendamento > 0 Then
        reagendamentoAutomatico vlRetornoReagendamento, vlDataReagendamento
    End If
End Sub

Private Sub Form_Load()


    Dim oEndossoReativacao As Object
    Dim dt_parc_aux As Date
    Dim sLinha2 As String
    Dim lEndossoCancelamentoId As Integer
    Dim Str As String
    '07/12/2015 - Schoralick (nova consultoria) - 18690344 - Melhorias Processo Reagendamento e Reativa��o BESC - inicio
    Dim linhas As String
    Dim i As Integer
    Dim iCont As Integer
    iCont = 0
    'fim - 18690344 - Melhorias Processo Reagendamento e Reativa��o BESC - inicio

    txtDataReativacao.Text = Data_Sistema

    Set oEndossoReativacao = CreateObject("SEGL0140.cls00172")

    dt_parc_aux = Data_Sistema
    ultimaParcela = 0


    Set oEndossoReativacao = CreateObject("SEGL0140.cls00172")


    Set ObterAgendamentosReagendamento = oEndossoReativacao.ObterAgendamentosReagendamento(gsSIGLASISTEMA, _
                                                                                           glAmbiente_id, _
                                                                                           App.Title, _
                                                                                           App.FileDescription, _
                                                                                           PropostaId)


    While Not ObterAgendamentosReagendamento.EOF
        sLinha2 = ""
        sLinha2 = sLinha2 & (ObterAgendamentosReagendamento!Num_Cobranca) & vbTab
        sLinha2 = sLinha2 & Format(ObterAgendamentosReagendamento!Val_Cobranca, "0.00") & vbTab
        sLinha2 = sLinha2 & Format(ObterAgendamentosReagendamento!Dt_Agendamento, "dd/mm/yyyy") & vbTab

        Str = ""
        If Not (ObterAgendamentosReagendamento!ano_mes_ref) Then
            Str = Right$(ObterAgendamentosReagendamento!ano_mes_ref, 2) & "/" & Left$(ObterAgendamentosReagendamento!ano_mes_ref, Len(ObterAgendamentosReagendamento!ano_mes_ref) - 2)
            sLinha2 = sLinha2 & Str & vbTab
        Else
            'sLinha2 = sLinha2 & ObterAgendamentosReagendamento!ano_mes_referencia & vbTab - Cibele Pereira 04/02/2015 - INC000004508234
            sLinha2 = sLinha2 & ObterAgendamentosReagendamento!ano_mes_ref & vbTab    ' Cibele Pereira 04/02/2015 - INC000004508234
        End If

        sLinha2 = sLinha2 & Format(ObterAgendamentosReagendamento!Dt_Recebimento, "dd/mm/yyyy")

        grdParcelas.AddItem (sLinha2)

        dt_parc_aux = DateAdd("m", 1, dt_parc_aux)
        ultimaParcela = ultiultimaParcela + 1
        ObterAgendamentosReagendamento.MoveNext
    Wend

    'ordena o grid
    grdParcelas.Col = 0
    grdParcelas.Sort = flexSortNumericDescending

    Set oEndossoReativacao = Nothing

    maiorMesRef = grdParcelas.TextMatrix(1, 3)
    txtMesReferencia = mesReferencia(maiorMesRef)
    ultimoValor = grdParcelas.TextMatrix(1, 1)
    mskValParcela = ultimoValor
    grdParcelas.ColWidth(5) = 0

    cmdAlterar.Enabled = False

    '07/12/2015 - Schoralick (nova tendencia) - 18690344 - Melhorias Processo Reagendamento e Reativa��o BESC - inicio
    Select Case produtoId
    Case 200, 201, 202, 203, 204, 205, 208, 209, 210, 211, 213, 216, 217, 218, 219, 221, 222, 223, 224, 225, 226
        For i = 1 To grdParcelas.Rows - 1
            linhas = converteGrdParcelasEmLinha(i, EnumOpUmaLinha)
            If ParcelasValida(EnumValidPendente, linhas) Then
                iCont = iCont + 1
            End If
        Next i
        cmdReagenda.Enabled = (iCont > 1)
    Case Else
        cmdReagenda.Enabled = False
    End Select
    'fim - 18690344 - Melhorias Processo Reagendamento e Reativa��o BESC

    Exit Sub


TrataErro:
    Call TratarErro("CarregarDadosReativacaoProposta", Me.name)
    Call FinalizarAplicacao


End Sub


Private Sub grdParcelas_Click()

    If grdParcelas.TextMatrix(grdParcelas.Row, 4) = "" Then
        mskDtParcela.Enabled = True
        cmdAlterar.Enabled = True
        mskValParcela.Text = grdParcelas.TextMatrix(grdParcelas.Row, 1)
        mskDtParcela.Text = grdParcelas.TextMatrix(grdParcelas.Row, 2)
        txtMesReferencia = grdParcelas.TextMatrix(grdParcelas.Row, 3)
    Else
        cmdAlterar.Enabled = False
        MsgBox ("N�o � possivel alterar parcela baixada!")
    End If

End Sub

Private Function comparaData() As Boolean
    Dim datahj As Date
    Dim i As Integer
    Dim datateste As Date
    Dim x As Integer

    datahj = CDate(Data_Sistema)
    comparaData = True

    For i = 1 To grdParcelas.Rows - 1
        If grdParcelas.TextMatrix(i, 5) = "a" Or grdParcelas.TextMatrix(i, 5) = "i" Then
            datateste = CDate(grdParcelas.TextMatrix(i, 2))
            x = DateDiff("d", datahj, datateste)
            If x < 4 And grdParcelas.TextMatrix(i, 4) = "" Then
                comparaData = False
                Exit For
            End If
        End If
    Next i


End Function


Private Function comparaMesReferencia(ByVal data As String, numParcela As Integer, Acao As String) As Boolean
    Dim a As Integer
    comparaMesReferencia = True

    For a = 0 To grdParcelas.Rows - 1

        grdParcelas.Row = a
        If data = grdParcelas.TextMatrix(grdParcelas.Row, 3) Then
            If Acao = "a" Then
                If grdParcelas.TextMatrix(grdParcelas.Row, 0) <> numParcela Then
                    comparaMesReferencia = False
                End If
            Else
                comparaMesReferencia = False
            End If
        End If

    Next a

End Function

Private Function retornaMaiorData() As String
    Dim i As Integer
    Dim data As Date
    Dim data2 As Date
    Dim x As Integer
    data = CDate(grdParcelas.TextMatrix(1, 2))
    retornaMaiorData = (grdParcelas.TextMatrix(1, 2))

    For i = 2 To grdParcelas.Rows - 1

        data2 = CDate(grdParcelas.TextMatrix(i, 2))
        x = DateDiff("d", data, data2)

        If x > 0 Then
            retornaMaiorData = grdParcelas.TextMatrix(i, 2)
            data = CDate(grdParcelas.TextMatrix(i, 2))
        End If
    Next i

End Function

'03/12/2015 - Schoralick (nova consultoria) - 18690344 - Melhorias Processo Reagendamento e Reativa��o BESC
Public Sub reagendamentoAutomatico(ByVal opcao As Integer, data As String)
    Dim i As Integer
    Dim Soma As Integer
    Dim linhas As String
    Dim cSituacaoAnterior As String
    Dim cDataAnterior As String
    Dim SomaTemp As Integer
    Dim dtTemp As String
    Dim lMsg As String
    Dim sResposta As String

    If opcao > 0 Then
        Soma = 0

        'For i = 1 To grdParcelas.Rows - 1
        For i = grdParcelas.Rows - 1 To 1 Step -1
            If (grdParcelas.TextMatrix(i, 4) = "") And (grdParcelas.TextMatrix(i, 5) <> "i") Then
                linhas = ""
                linhas = linhas & "(" & PropostaId & ","
                linhas = linhas & grdParcelas.TextMatrix(i, 0) & ","    'num_parcela
                linhas = linhas & "'a'" & ","    ' situacao
                mes_ref = Mid$(grdParcelas.TextMatrix(i, 3), 4, 4) & Mid$(grdParcelas.TextMatrix(i, 3), 1, 2)
                linhas = linhas & mes_ref & ","  'mes_ano_ref

                SomaTemp = Soma
                If opcao = 1 Then
                    dtTemp = Format(CDate(data), "dd/mm/yyyy")
                End If
                If opcao = 2 Then
                    dtTemp = Format((CDate(data) + SomaTemp), "dd/mm/yyyy")
                End If

                data = Mid$(dtTemp, 7, 4) & "-" & Mid$(dtTemp, 4, 2) & "-" & Mid$(dtTemp, 1, 2)
                linhas = linhas & "'" & data & "',"    'data_parcela
                linhas = linhas & "'0.00',"    'valor

                linhas = linhas & i & ")|"
            End If
        Next i

        lMsg = ValidaParcela(linhas)
        If lMsg <> "" Then
            sResposta = MsgBox(lMsg + vbNewLine + vbNewLine + "Deseja continuar somente para as parcelas sem impedimentos?", vbYesNo + vbExclamation, "Caixa de Mensagem")
            If sResposta = vbNo Then
                Exit Sub
            End If
        End If

        Soma = 0
        'For i = 1 To grdParcelas.Rows - 1
        For i = grdParcelas.Rows - 1 To 1 Step -1
            If (grdParcelas.TextMatrix(i, 4) = "") And (grdParcelas.TextMatrix(i, 5) <> "i") Then
                SomaTemp = Soma
                cSituacaoAnterior = grdParcelas.TextMatrix(i, 5)
                cDataAnterior = grdParcelas.TextMatrix(i, 2)


                grdParcelas.TextMatrix(i, 5) = "a"
                If opcao = 1 Then
                    grdParcelas.TextMatrix(i, 2) = Format(CDate(data), "dd/mm/yyyy")
                End If
                If opcao = 2 Then
                    dtTemp = Format((CDate(data) + SomaTemp), "dd/mm/yyyy")
                    grdParcelas.TextMatrix(i, 2) = dtTemp
                End If

                linhas = converteGrdParcelasEmLinha(i, EnumOpUmaLinha)

                If ValidaParcela(linhas) = "" Then
                    If opcao = 2 Then
                        Soma = Soma + 20
                    End If

                    grdParcelas.TextMatrix(i, 5) = "a"
                Else
                    grdParcelas.TextMatrix(i, 5) = cSituacaoAnterior
                    grdParcelas.TextMatrix(i, 2) = cDataAnterior
                End If
            End If
        Next i
    End If
End Sub

Private Function ValidaParcela(ByVal linhas As String) As String
    Dim lMsg As String
    lMsg = ""

    If ParcelasValida(EnumValidInadimplenciaSAP, linhas) = False Then
        lMsg = "N�o � poss�vel o reagendamento de uma ou mais parcelas." + vbNewLine + "Motivo: n�o h� retorno ou exite um retorno positivo da parcela."
    ElseIf ParcelasValida(EnumValidEndossoAtrelado, linhas) = False Then
        lMsg = "N�o � poss�vel o reagendamento de uma ou mais parcelas." + vbNewLine + "Motivo: existem parcelas que est�o associadas a um dos endossos que criaram outras parcelas a serem reagendadas."
    ElseIf ParcelasValida(EnumValidDataReagendamento, linhas) = False Then
        lMsg = "N�o � poss�vel o reagendamento de uma ou mais parcelas." + vbNewLine + "Motivo: a data de pagamento � menor que a data atual + 15 dias."
    ElseIf ParcelasValida(EnumValidPendente, linhas) = False Then
        lMsg = "N�o � possivel o reagendamento de uma ou mais parcelas." + vbNewLine + "Motivo: existem parcelas que n�o est�o pendentes no sistema."
    End If

    ValidaParcela = lMsg
End Function

Private Function ParcelasValida(ByVal lEnum As EnumValidaParcela, ByVal linhas As String) As Boolean
    Set oEndossoReativacao = CreateObject("SEGL0140.cls00172")

    Dim lRetorno As Boolean


    Set reagendamentoValidaParcela = oEndossoReativacao.reativacaoValidaParcela(gsSIGLASISTEMA, _
                                                                                glAmbiente_id, _
                                                                                App.Title, _
                                                                                App.FileDescription, _
                                                                                linhas, _
                                                                                lEnum)

    If reagendamentoValidaParcela!Resultado = 1 Then
        lRetorno = False
    Else
        lRetorno = True
    End If

    Set oEndossoReativacao = Nothing
    ParcelasValida = lRetorno
End Function

Private Function converteGrdParcelasEmLinha(ByVal i As Integer, ByVal lEnum As EnumOpLinhaRetorno) As String
    Dim l As Integer
    Dim linhas As String
    linhas = ""

    If lEnum = EnumOpUmaLinha Then
        linhas = RetornaUmaLinhaGrdParcela(i)
    End If

    If lEnum = EnumOpTodasLinhas Then
        'For l = i To grdParcelas.Rows - 1
        For l = grdParcelas.Rows - 1 To i Step -1
            linhas = linhas & RetornaUmaLinhaGrdParcela(l)
        Next l
    End If

    converteGrdParcelasEmLinha = linhas
End Function

Private Function RetornaUmaLinhaGrdParcela(ByVal i As Integer) As String
    Dim Linha As String
    Dim data As String
    Dim mes_ref As String

    Linha = ""
    Linha = Linha & "(" & PropostaId & ","
    Linha = Linha & grdParcelas.TextMatrix(i, 0) & ","    'num_parcela
    Linha = Linha & "'" & grdParcelas.TextMatrix(i, 5) & "'" & ","    ' situacao

    mes_ref = Mid$(grdParcelas.TextMatrix(i, 3), 4, 4) & Mid$(grdParcelas.TextMatrix(i, 3), 1, 2)
    Linha = Linha & mes_ref & ","  'mes_ano_ref

    data = Mid$(grdParcelas.TextMatrix(i, 2), 7, 4) & "-" & Mid$(grdParcelas.TextMatrix(i, 2), 4, 2) & "-" & Mid$(grdParcelas.TextMatrix(i, 2), 1, 2)
    Linha = Linha & "'" & data & "',"    'data_parcela
    Linha = Linha & "'0.00',"    'valor

    Linha = Linha & i & ")|"

    RetornaUmaLinhaGrdParcela = Linha
End Function
'fim - 18690344 - Melhorias Processo Reagendamento e Reativa��o BESC
