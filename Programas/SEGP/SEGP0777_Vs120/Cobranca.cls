VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Cobranca"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Private mvarNumCobranca As Integer    'local copy
Private mvarValCobranca As Currency    'local copy
Private mvarDtCobranca As String    'local copy
'27/12/2015 - 18690344 - Melhorias Processo Reagendamento e Reativação BESC
Private mvarAnoMesRef As String

Public Property Let AnoMesRef(ByVal vAnoMesRef As String)
    mvarAnoMesRef = vAnoMesRef
End Property

Public Property Get AnoMesRef() As String
    AnoMesRef = mvarAnoMesRef
End Property
'fim - '27/12/2015 - 18690344 - Melhorias Processo Reagendamento e Reativação BESC

Public Property Let DtCobranca(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.DtCobranca = 5
    mvarDtCobranca = vData
End Property


Public Property Get DtCobranca() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.DtCobranca
    DtCobranca = mvarDtCobranca
End Property



Public Property Let ValCobranca(ByVal vData As Currency)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.ValCobranca = 5
    mvarValCobranca = vData
End Property


Public Property Get ValCobranca() As Currency
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.ValCobranca
    ValCobranca = mvarValCobranca
End Property



Public Property Let NumCobranca(ByVal vData As Integer)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.NumCobranca = 5
    mvarNumCobranca = vData
End Property


Public Property Get NumCobranca() As Integer
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.NumCobranca
    NumCobranca = mvarNumCobranca
End Property



