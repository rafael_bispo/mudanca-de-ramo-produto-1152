Attribute VB_Name = "modCobranca"
'Declara��o de Vari�veis de utiliza��o do programa de Reintegra��o OnLine
Option Explicit

Public rdocn1 As New rdoConnection
Public PropostaBB As String
Public flag_ok As Boolean
Public Coberturas As New Collection
Public ConfiguracaoBrasil As Boolean

'Contador para as coberturas
Public Seq_Cob As Integer

'Declara��o de Vari�veis Tipo Definido para Cobertura
Public Type tp_Cob
    ID As String
    Obj_id As String
    Nome As String
    Valor_a_Reintegrar As Double
    Novo_Valor_IS As Double
End Type

'Declara��o de Vari�veis Tipo Definido para Proposta
Private Type tp_Proposta
    ID As String
    Produto_id As String
    Apolice_id As String
    Sucursal_id As String
    Seguradora_id As String
    ramo_id As String
    endosso_id As String
    Dt_Endosso As String
    Dt_Pedido_Endosso As String
    Num_Cartao As String
    Dt_Validade_Cartao As String
    Banco As String
    Agencia As String
    Conta_Corrente As String
    Dt_Inicio_Vigencia As String
    Seguro_Moeda_id As String
    sTipoEndosso As String
End Type
Public grpProposta As tp_Proposta

'Declara��o de Vari�veis Vari�veis de utiliza��o Geral
Public Proposta_Atual As String
Public Produto_Atual As String
Public Apolice_Atual As String
Public Sucursal_Atual As String
Public Seguradora_Atual As String
Public Ramo_Atual As String
Public Dt_comparacao As String

'fadao
Public sTipoEndosso As String


Public Custo_Endosso As Currency
Public Custo_Endosso_Max As Currency
Public perc_custo_endosso As Currency
Public Val_IOF As Currency
Public Est_Cliente_ID As String
Public Corretor_ID As String
Public Tp_Corretagem As String
Public Sucursal_Corretor_id As String
Public Perc_Corretagem As Currency
Public Perc_Comissao_Est As Currency
Public perc_iof As Currency
Public Val_IR As Currency
Public Val_Comissao As Currency
Public Val_Comissao_bruta As Currency
Public Val_Comissao_Est As Currency
Public Val_Ad_Fracionamento As Currency
Public Valor_premio As Currency
Public Valor_premio_liquido As Currency

'fadao
Public Valor_premio_Reintegracao As Currency
Public sCoberturaId As String
Public dFatTaxa As Double
Public dValor_a_Reintegrar As Double

Public Forma_Pgto As Variant
Public Dt_Ini_Vigencia As Variant
Public Dt_Fim_Vigencia As Variant
Public maior_reintegracao As Variant
Public Banco As Variant
Public Agencia As Variant
Public Conta_Corrente As Variant
Public Num_Cartao As Variant
Public Dt_Validade_Cartao As Variant
Public Dia_Debito As Variant
Public Endosso_Reducao_id As String
Public Endosso_Reintegracao_id As String
Public Data_Endosso As String
Public Data_Pedido_Endosso As String
Public NomeObjeto As String
Public PropostaCancelada As Boolean
Public Nome_Segurado As String
Public Tipo As String
Public vEmi_RE_proposta_id As String

Public tp_predio_aux As String
Public mat_construcao_aux As String
Public tp_moradia_aux As String
Public classe_local_aux As String
Public classe_atividade_aux As String
Public tp_atividade_aux As String
Public tp_cidade_aux As String
Public tp_regiao_aux As String
Public tp_construcao_aux As String
Public cat_cliente_aux As String
Public classe_ocupacao_aux As String
Public tp_condominio_aux As String
Public tp_equipamento_aux As String
Public cod_classe_equipamento_aux As String
Public tp_implemento_aux As String
Public end_risco_aux As String
Public CoberturaAux As String
Public ObjetoAux As String
Public Cod_Objeto_Aux As String
Public String_Condicao As String
Public Subramo_id As String
Public Tp_taxa_id As String

Public Num_parcela As String
Public Convenio As String
Public Seguro_Moeda_id As Integer
Public Nosso_Numero As String
Public Nosso_Numero_dv As String

Public Taxa As String
Public Taxa_Juros As String
Public Valor_cob As Currency

Public flag_tp_produto As String

Public SQL As String
Public Val_minimo_endosso As Currency

Public Sub Obtem_Dados_Seguro()

    Dim rc As Recordset
    Dim SQL As String

    On Error GoTo Erro

    'TABELA: seguro_generico_tb
    SQL = "SELECT isnull(condicao,'0001') condicao, subramo_id, cod_objeto_segurado "
    SQL = SQL & " FROM seguro_generico_tb "
    SQL = SQL & " WHERE proposta_id = " & Proposta_Atual

    If rdocn Is Nothing Then
        Call Conexao
    Else
        If rdocn.State = 0 Then Call Conexao
    End If

    Set rc = rdocn.Execute(SQL)

    If Not rc.EOF Then
        String_Condicao = rc!condicao
        Subramo_id = rc!Subramo_id
        Cod_Objeto_Aux = rc!cod_objeto_segurado
        flag_tp_produto = "8"
        rc.Close

        'Obt�m o tp_taxa_id
        SQL = "SELECT tp_taxa_id "
        SQL = SQL & " FROM tp_taxa_tb "
        SQL = SQL & " WHERE subramo_id    = " & Subramo_id
        SQL = SQL & " AND produto_id      = " & Produto_Atual
        SQL = SQL & " AND dt_fim_vigencia IS NULL"

        If rdocn Is Nothing Then
            Call Conexao
        Else
            If rdocn.State = 0 Then Call Conexao
        End If

        Set rc = rdocn.Execute(SQL)

        If Not rc.EOF Then
            Tp_taxa_id = rc!Tp_taxa_id
        Else
            Tp_taxa_id = 0
        End If

        rc.Close

        Exit Sub

    End If

    rc.Close

    MensagemBatch "Dados do seguro n�o encontrado. Programa ser� cancelado.", vbOKOnly, "Erro obtendo dados do seguro", True
    Call TerminaSEGBR

    Exit Sub

Erro:
    MensagemBatch "Rotina: Obtem Dados Seguro. Programa ser� cancelado", vbOKOnly, "Erro obtendo dados do seguro", True
    Call TerminaSEGBR

End Sub

Public Function Calcula_Premio(ByVal Cobertura As String, ByVal OBJETO As String, ByVal DtIniVigencia As String, ByVal dtCalculo As String, ByVal val_is As Double, perc_agravamento As String) As Currency

    Dim data1 As Date
    Dim data2 As Date
    Dim dias As Integer

    On Error GoTo Erro

    CoberturaAux = Cobertura

    Select Case flag_tp_produto
    Case "1": Call Obtem_Taxa_Residencial
    Case "2": Call Obtem_Taxa_Empresarial
    Case "3": Call Obtem_Taxa_Condominio
    Case "4": Call Obtem_Taxa_Maquinas
    Case "5": Call Obtem_Taxa_Avulso
    Case "6": Call Obtem_Taxa_CPR
    Case "7": Call Obtem_Taxa_Transporte
    Case "8": Call Obtem_Taxa_Generico
    End Select

    If IsNumeric(perc_agravamento) Then
        perc_agravamento = 1 + (perc_agravamento / 100)
    Else
        perc_agravamento = 1
    End If

    Valor_cob = Truncadecimal(val_is * Taxa * perc_agravamento)

    data1 = DtIniVigencia
    data2 = dtCalculo

    'FLOW 405878 - RNC 1664-01 - Marcelo Ferreira - Confitec Sistemas - 2008-06-05
    'Tratamento para c�lculo de datas em anos bissextos (v�lido at� 2099)
    If Year(data2) Mod 4 = 0 Then
        dias = 366 - dias
    Else
        dias = 365 - dias
    End If

    If dias < 0 Then
        GoTo Erro_Calculo
    End If

    Valor_cob = Truncadecimal(Valor_cob * dias / 365)

    'Retorna pra fun��o o valor calculado
    Calcula_Premio = Valor_cob

    Exit Function

Erro_Calculo:
    MensagemBatch "Rotina: Calcula_Premio. Erro ao calcular o pr�mio da proposta_id: " & Proposta_Atual, vbOKOnly, "Erro ao calcular valor do premio", True
    Call TerminaSEGBR

Erro:
    MensagemBatch "Rotina: Calcula_Premio. Programa ser� cancelado", vbOKOnly, "Erro ao calcular valor do premio", True
    Call TerminaSEGBR

End Function
Private Sub Obtem_Taxa_Residencial()

    Dim rc As rdoResultset

    On Error GoTo Erro

    SQL = "SELECT isnull(fat_taxa,0) FROM premio_tp_predio_tb "
    SQL = SQL & " WHERE produto_id = " & Produto_Atual
    SQL = SQL & "   AND Ramo_id = " & Ramo_Atual
    SQL = SQL & "   AND tp_cobertura_id = " & CoberturaAux
    SQL = SQL & "   AND cat_cliente = " & cat_cliente_aux
    SQL = SQL & "   AND tp_predio_id = " & tp_predio_aux
    SQL = SQL & "   AND dt_fim_vigencia is null "
    Set rc = rdocn.OpenResultset(SQL)

    If Not rc.EOF Then
        Taxa = Val(rc(0))
        rc.Close
        Exit Sub
    End If
    rc.Close

    SQL = "SELECT isnull(fat_taxa,0) FROM premio_tp_predio_mat_tb "
    SQL = SQL & " WHERE produto_id = " & Produto_Atual
    SQL = SQL & "   AND Ramo_id = " & Ramo_Atual
    SQL = SQL & "   AND tp_cobertura_id = " & CoberturaAux
    SQL = SQL & "   AND cat_cliente = " & cat_cliente_aux
    SQL = SQL & "   AND tp_predio_id = " & tp_predio_aux
    SQL = SQL & "   AND material_construcao_id = " & mat_construcao_aux
    SQL = SQL & "   AND dt_fim_vigencia is null "
    Set rc = rdocn.OpenResultset(SQL)

    If Not rc.EOF Then
        Taxa = Val(rc(0))
        rc.Close
        Exit Sub
    End If
    rc.Close

    SQL = "SELECT isnull(fat_taxa,0) FROM premio_mat_construcao_tb "
    SQL = SQL & " WHERE produto_id = " & Produto_Atual
    SQL = SQL & "   AND Ramo_id = " & Ramo_Atual
    SQL = SQL & "   AND tp_cobertura_id = " & CoberturaAux
    SQL = SQL & "   AND cat_cliente = " & cat_cliente_aux
    SQL = SQL & "   AND material_construcao_id = " & mat_construcao_aux
    SQL = SQL & "   AND dt_fim_vigencia is null "
    Set rc = rdocn.OpenResultset(SQL)

    If Not rc.EOF Then
        Taxa = Val(rc(0))
        rc.Close
        Exit Sub
    End If
    rc.Close

    SQL = "SELECT isnull(fat_taxa,0) FROM premio_tp_cob_tb "
    SQL = SQL & " WHERE produto_id = " & Produto_Atual
    SQL = SQL & "   AND Ramo_id = " & Ramo_Atual
    SQL = SQL & "   AND tp_cobertura_id = " & CoberturaAux
    SQL = SQL & "   AND cat_cliente = " & cat_cliente_aux
    SQL = SQL & "   AND dt_fim_vigencia is null "
    Set rc = rdocn.OpenResultset(SQL)

    If Not rc.EOF Then
        Taxa = Val(rc(0))
        rc.Close
        Exit Sub
    End If
    rc.Close

    MensagemBatch "Taxa para a cobertura " & CoberturaAux & " n�o encontrada. ", vbOKOnly, "Erro obtendo taxa residencial", True

    Exit Sub

Erro:
    MensagemBatch "Rotina: Obtem Taxa Residencial. Programa ser� cancelado", vbOKOnly, "Erro obtendo taxa residencial", True
    Call TerminaSEGBR

End Sub
Private Sub Obtem_Taxa_Empresarial()

    Dim rc As rdoResultset

    On Error GoTo Erro

    SQL = "SELECT isnull(fat_taxa,0) FROM premio_ativ_tp_cidade_tb "
    SQL = SQL & " WHERE produto_id = " & Produto_Atual
    SQL = SQL & "   AND ramo_id = " & Ramo_Atual
    SQL = SQL & "   AND tp_cobertura_id = " & CoberturaAux
    SQL = SQL & "   AND cat_cliente = " & cat_cliente_aux
    SQL = SQL & "   AND classe_atividade_id = " & classe_atividade_aux
    SQL = SQL & "   AND tp_cidade_id = " & tp_cidade_aux
    SQL = SQL & "   AND dt_fim_vigencia is null "
    Set rc = rdocn.OpenResultset(SQL)

    If Not rc.EOF Then
        Taxa = Val(rc(0))
        rc.Close
        Exit Sub
    End If
    rc.Close

    SQL = "SELECT isnull(fat_taxa,0) FROM premio_tp_atividade_tb "
    SQL = SQL & " WHERE produto_id = " & Produto_Atual
    SQL = SQL & "   AND Ramo_id = " & Ramo_Atual
    SQL = SQL & "   AND tp_cobertura_id = " & CoberturaAux
    SQL = SQL & "   AND cat_cliente = " & cat_cliente_aux
    SQL = SQL & "   AND tp_atividade_id = " & tp_atividade_aux
    SQL = SQL & "   AND dt_fim_vigencia is null "
    Set rc = rdocn.OpenResultset(SQL)

    If Not rc.EOF Then
        Taxa = Val(rc(0))
        rc.Close
        Exit Sub
    End If
    rc.Close

    SQL = "SELECT isnull(fat_taxa,0) FROM premio_loc_tb "
    SQL = SQL & " WHERE produto_id = " & Produto_Atual
    SQL = SQL & "   AND Ramo_id = " & Ramo_Atual
    SQL = SQL & "   AND tp_cobertura_id = " & CoberturaAux
    SQL = SQL & "   AND cat_cliente = " & cat_cliente_aux
    SQL = SQL & "   AND tp_construcao_id = " & tp_construcao_aux
    SQL = SQL & "   AND classe_local_id = " & classe_local_aux
    SQL = SQL & "   AND classe_ocupacao_id = " & classe_ocupacao_aux
    SQL = SQL & "   AND dt_fim_vigencia is null "
    Set rc = rdocn.OpenResultset(SQL)

    If Not rc.EOF Then
        Taxa = Val(rc(0))
        rc.Close
        Exit Sub
    End If
    rc.Close

    SQL = "SELECT isnull(fat_taxa,0) FROM premio_tp_cob_tb "
    SQL = SQL & " WHERE produto_id = " & Produto_Atual
    SQL = SQL & "   AND Ramo_id = " & Ramo_Atual
    SQL = SQL & "   AND tp_cobertura_id = " & CoberturaAux
    SQL = SQL & "   AND cat_cliente = " & cat_cliente_aux
    SQL = SQL & "   AND dt_fim_vigencia is null "
    Set rc = rdocn.OpenResultset(SQL)

    If Not rc.EOF Then
        Taxa = Val(rc(0))
        rc.Close
        Exit Sub
    End If
    rc.Close

    SQL = "SELECT isnull(fat_taxa,0) FROM premio_tp_regiao_tb "
    SQL = SQL & " WHERE produto_id = " & Produto_Atual
    SQL = SQL & "   AND Ramo_id = " & Ramo_Atual
    SQL = SQL & "   AND tp_cobertura_id = " & CoberturaAux
    SQL = SQL & "   AND cat_cliente = " & cat_cliente_aux
    SQL = SQL & "   AND tp_regiao_id = " & tp_regiao_aux
    SQL = SQL & "   AND dt_fim_vigencia is null "
    Set rc = rdocn.OpenResultset(SQL)

    If Not rc.EOF Then
        Taxa = Val(rc(0))
        rc.Close
        Exit Sub
    End If
    rc.Close

    MensagemBatch "Taxa para a cobertura " & CoberturaAux & " n�o encontrada. ", vbOKOnly, "Erro obtendo taxa empresarial", True

    Exit Sub

Erro:
    MensagemBatch "Rotina: Obtem Taxa Empresarial. Programa ser� cancelado", vbOKOnly, "Erro obtendo taxa empresarial", True
    Call TerminaSEGBR

End Sub
Private Sub Obtem_Taxa_Condominio()

    Dim rc As rdoResultset

    On Error GoTo Erro

    SQL = "SELECT isnull(fat_taxa,0) FROM premio_tp_condominio_tb "
    SQL = SQL & " WHERE produto_id = " & Produto_Atual
    SQL = SQL & "   AND Ramo_id = " & Ramo_Atual
    SQL = SQL & "   AND tp_cobertura_id = " & CoberturaAux
    SQL = SQL & "   AND cat_cliente = " & cat_cliente_aux
    SQL = SQL & "   AND tp_condominio_id = " & tp_condominio_aux
    SQL = SQL & "   AND dt_fim_vigencia is null "
    Set rc = rdocn.OpenResultset(SQL)

    If Not rc.EOF Then
        Taxa = Val(rc(0))
        rc.Close
        Exit Sub
    End If
    rc.Close

    MensagemBatch "Taxa para a cobertura " & CoberturaAux & " n�o encontrada. ", vbOKOnly, "Erro obtendo taxa condom�nio", True

    Exit Sub

Erro:
    MensagemBatch "Rotina: Obtem Taxa Condom�nio. Programa ser� cancelado", vbOKOnly, "Erro obtendo taxa condom�nio", True
    Call TerminaSEGBR

End Sub
Private Sub Obtem_Taxa_Maquinas()

    Dim rc As rdoResultset

    On Error GoTo Erro

    SQL = "SELECT isnull(fat_taxa,0) FROM premio_tp_equipamento_tb "
    SQL = SQL & " WHERE produto_id = " & Produto_Atual
    SQL = SQL & "   AND Ramo_id = " & Ramo_Atual
    SQL = SQL & "   AND tp_cobertura_id = " & CoberturaAux
    SQL = SQL & "   AND cat_cliente = " & cat_cliente_aux
    SQL = SQL & "   AND tp_equipamento_id = " & tp_equipamento_aux
    SQL = SQL & "   AND cod_classe_equipamento = " & cod_classe_equipamento_aux
    SQL = SQL & "   AND dt_fim_vigencia is null "
    Set rc = rdocn.OpenResultset(SQL)

    If Not rc.EOF Then
        Taxa = Val(rc(0))
        rc.Close
        Exit Sub
    End If
    rc.Close

    MensagemBatch "Taxa para a cobertura " & CoberturaAux & " n�o encontrada. ", vbOKOnly, "Erro obtendo taxa m�quinas", True

    Exit Sub

Erro:
    MensagemBatch "Rotina: Obtem Taxa Maquinas. Programa ser� cancelado", vbOKOnly, "Erro obtendo taxa m�quinas", True
    Call TerminaSEGBR

End Sub
Private Sub Obtem_Taxa_Avulso()

    Dim rc As rdoResultset

    On Error GoTo Erro

    SQL = "SELECT isnull(fat_taxa,0) "
    SQL = SQL & " FROM escolha_tp_cob_avulso_tb "
    SQL = SQL & " WHERE proposta_id         = " & Proposta_Atual
    SQL = SQL & "   AND tp_cobertura_id     = " & CoberturaAux
    SQL = SQL & "   AND cod_objeto_segurado = " & Cod_Objeto_Aux
    SQL = SQL & "   AND num_endosso         IS NULL"
    Set rc = rdocn.OpenResultset(SQL)

    If Not rc.EOF Then
        Taxa = Val(rc(0))
        rc.Close
        Exit Sub
    End If
    rc.Close

    MensagemBatch "Taxa para a cobertura " & CoberturaAux & " n�o encontrada. ", vbOKOnly, "Erro obtendo taxa avulso", True

    Exit Sub

Erro:
    MensagemBatch "Rotina: Obtem Taxa Avulso. Programa ser� cancelado", vbOKOnly, "Erro obtendo taxa avulso", True
    Call TerminaSEGBR

End Sub
Private Sub Obtem_Taxa_CPR()

    Dim rc As rdoResultset

    On Error GoTo Erro

    SQL = "SELECT isnull(fat_taxa,0) "
    SQL = SQL & " FROM premio_tp_cob_tb "
    SQL = SQL & " WHERE produto_id      = " & Produto_Atual
    SQL = SQL & "   AND ramo_id         = " & Ramo_Atual
    SQL = SQL & "   AND tp_cobertura_id = " & CoberturaAux
    SQL = SQL & "   AND cat_cliente     = " & cat_cliente_aux
    SQL = SQL & "   AND dt_fim_vigencia IS NULL"
    Set rc = rdocn.OpenResultset(SQL)

    If Not rc.EOF Then
        Taxa = Val(rc(0))
        rc.Close
        Exit Sub
    End If
    rc.Close

    MensagemBatch "Taxa para a cobertura " & CoberturaAux & " n�o encontrada. ", vbOKOnly, "Erro obtendo taxa cpr", True

    Exit Sub

Erro:
    MensagemBatch "Rotina: Obtem Taxa CPR. Programa ser� cancelado", vbOKOnly, "Erro obtendo taxa cpr", True
    Call TerminaSEGBR

End Sub
Private Sub Obtem_Taxa_Transporte()

    Dim rc As rdoResultset

    On Error GoTo Erro

    SQL = "SELECT isnull(fat_taxa,0) "
    SQL = SQL & " FROM escolha_tp_cob_generico_tb "
    SQL = SQL & " WHERE proposta_id         = " & Proposta_Atual
    SQL = SQL & "   AND tp_cobertura_id     = " & CoberturaAux
    SQL = SQL & "   AND cod_objeto_segurado = " & Cod_Objeto_Aux
    SQL = SQL & "   AND num_endosso         IS NULL"
    Set rc = rdocn.OpenResultset(SQL)

    If Not rc.EOF Then
        Taxa = Val(rc(0))
        rc.Close
        Exit Sub
    End If
    rc.Close

    MensagemBatch "Taxa para a cobertura " & CoberturaAux & " n�o encontrada. ", vbOKOnly, "Erro obtendo taxa transporte", True

    Exit Sub

Erro:
    MensagemBatch "Rotina: Obtem Taxa Transporte. Programa ser� cancelado", vbOKOnly, "Erro obtendo taxa transporte", True
    Call TerminaSEGBR

End Sub
Private Sub Obtem_Taxa_Generico()

    Dim rc As rdoResultset

    'carsilva - 07/06/2004
    Dim rs As rdoResultset

    On Error GoTo Erro

    'carsilva - 07/06/2004
    'SQL = "SELECT isnull(fat_taxa,0) FROM taxa_tb "
    SQL = "SELECT isnull(fat_taxa,0), relacionamento FROM taxa_tb "
    SQL = SQL & " WHERE tp_taxa_id      = " & Tp_taxa_id
    SQL = SQL & "   AND condicao        = '" & String_Condicao & "'"
    SQL = SQL & "   AND tp_cobertura_id = " & CoberturaAux
    SQL = SQL & "   AND dt_fim_vigencia IS NULL "
    Set rc = rdocn.OpenResultset(SQL)

    If Not rc.EOF Then
        Taxa = Val(rc(0))
        'carsilva - 07/06/2004
        If Produto_Atual = 111 Then
            If Verificar_promocao_empresarial(Proposta_Atual) Then
                SQL = "       Select fat_indice "
                SQL = SQL & " from taxa_promocional_tb "
                SQL = SQL & " where relacionamento = " & rc!relacionamento

                Set rs = rdocn.OpenResultset(SQL)

                If Not rs.EOF Then
                    Taxa = Taxa * Val(rs(0))
                End If
                rs.Close
            End If
        End If
        rc.Close
        Exit Sub
    End If
    rc.Close

    SQL = "SELECT isnull(fat_taxa,0) "
    SQL = SQL & " FROM escolha_tp_cob_generico_tb "
    SQL = SQL & " WHERE proposta_id         = " & Proposta_Atual
    SQL = SQL & "   AND tp_cobertura_id     = " & CoberturaAux
    SQL = SQL & "   AND cod_objeto_segurado = " & Cod_Objeto_Aux
    'FLOW - 10260182 - 07-04-2011  - Anderson Barreto - Referencias no FLOW 10304960
    'SQL = SQL & "   AND num_endosso         IS NULL"
    SQL = SQL & "   AND isnull(num_endosso, 0) = 0"
    'fim
    Set rc = rdocn.OpenResultset(SQL)

    If Not rc.EOF Then
        Taxa = Val(rc(0))
        rc.Close
        Exit Sub
    End If
    rc.Close

    MensagemBatch "Taxa para a cobertura " & CoberturaAux & " n�o encontrada. ", vbOKOnly, "Erro obtendo taxa gen�rico", True

    Exit Sub

Erro:
    MensagemBatch "Rotina: Obtem Taxa Generico. Programa ser� cancelado", vbOKOnly, "Erro obtendo taxa gen�rico", True
    Call TerminaSEGBR

End Sub

Public Sub Gera_Cobranca_da_Reintegracao(Subramo_id As Integer, Dt_calculo_iof As String, grupoProposta As tp_Proposta, Cob() As tp_Cob, Optional Chamada_Batch As Boolean = True, Optional So_Calcula As Boolean = False)

    Dim Dt_Inicio_Vigencia As String
    Dim PremioAux As Currency
    Dim i As Integer

    On Error GoTo Erro

    'Obt�m os par�metros passados que ser�o utilizados em toda a rotina de cobran�a
    With grupoProposta
        Proposta_Atual = .ID
        Produto_Atual = .Produto_id
        Apolice_Atual = .Apolice_id
        Sucursal_Atual = .Sucursal_id
        Seguradora_Atual = .Seguradora_id
        Ramo_Atual = .ramo_id
        Endosso_Reintegracao_id = .endosso_id
        Data_Endosso = .Dt_Endosso
        Data_Pedido_Endosso = .Dt_Pedido_Endosso
        Num_Cartao = .Num_Cartao
        Dt_Validade_Cartao = .Dt_Validade_Cartao
        Banco = .Banco
        Agencia = .Agencia
        Conta_Corrente = .Conta_Corrente
        Dt_Inicio_Vigencia = .Dt_Inicio_Vigencia
        If Len(.Seguro_Moeda_id) > 0 Then
            Seguro_Moeda_id = .Seguro_Moeda_id
        End If
        sTipoEndosso = .sTipoEndosso
    End With

    'Obt�m os dados do seguro
    Call Obtem_Dados_Seguro

    'Efetua o calculo do pr�mio l�quido
    Valor_premio_liquido = 0

    For i = 1 To Seq_Cob
        'PremioAux = Calcula_Premio(CStr(Cob(i).ID), Cob(i).Obj_id, Dt_Inicio_Vigencia, Data_Sistema, Cob(i).Valor_a_Reintegrar, "")
        'fadao
        If Trim(sTipoEndosso) <> "Reintegra��o Agricola" Then
            PremioAux = Calcula_Premio(CStr(Cob(i).ID), Cob(i).Obj_id, Dt_Inicio_Vigencia, Data_Pedido_Endosso, Cob(i).Valor_a_Reintegrar, "")
            Valor_premio_liquido = Valor_premio_liquido + PremioAux
        Else
            sCoberturaId = CStr(Cob(i).ID)
            Call Obtem_Taxa_Cobertura_Reintegracao
            dValor_a_Reintegrar = Cob(i).Valor_a_Reintegrar
            Valor_premio_liquido = dFatTaxa * dValor_a_Reintegrar
        End If
    Next i

    'Efetua c�lculos de valores de pr�mio, custo do endosso, IOF
    Call Calcula_Valores(Subramo_id, Dt_calculo_iof)

    'Grava registro na tabela endosso_financeiro_tb e agendamento_cobranca_tb
    Call Grava_Cobranca_Reintegracao

    Exit Sub

Erro:
    MensagemBatch "Rotina: Calcula Valor a ser Cobrado. Programa ser� cancelado.", vbOKOnly, "Erro c�lculo do valor a ser cobrado", True
    Call TerminaSEGBR

End Sub

Public Sub Calcula_Valores(Subramo_id As Integer, dt_referencia As String)

    On Error GoTo Erro

    'Obt�m o percentual do custo do endosso e o valor m�x. do custo do endosso
    Call Le_Custo_Endosso

    'Obt�m o percentual do IOF a ser utilizado
    Call Le_IOF(Subramo_id, dt_referencia)

    'Obt�m a taxa de juros a ser utilizada para o Produto
    Call Obtem_Taxa_Juros

    Custo_Endosso = Truncadecimal(Valor_premio_liquido * (perc_custo_endosso / 100))
    If Custo_Endosso > Custo_Endosso_Max Then
        Custo_Endosso = Custo_Endosso_Max
    End If

    'Calcula valores
    Valor_premio = Valor_premio_liquido + Custo_Endosso
    Val_Ad_Fracionamento = Valor_premio * (Taxa_Juros - 1)
    Valor_premio = Valor_premio + Val_Ad_Fracionamento
    Val_IOF = Truncadecimal(Valor_premio * perc_iof)
    Valor_premio = Valor_premio + Val_IOF


    'Gmarques 14/03/2008
    Call valor_Minimo_endosso(grpProposta.Produto_id, grpProposta.ramo_id)

    'If Val_minimo_endosso > Valor_premio Then
    '    Valor_premio = Val_minimo_endosso
    '    Val_IOF = Valor_premio - (Valor_premio / (1 + perc_iof))
    '    Val_Ad_Fracionamento = ((Valor_premio - Val_IOF) / (1 / (Taxa_Juros - 1) + 1))
    'End If

    'alterado por vpontes - Confitec em 10/04/2008 para atualizar tamb�m o custo_endosso e o valor_premio_liquido
    Dim curPremio_Aux As Currency
    curPremio_Aux = 0
    If Val_minimo_endosso > Valor_premio Then

        Valor_premio = Val_minimo_endosso
        curPremio_Aux = CCur(Format(Valor_premio, "0.00"))

        Val_IOF = Truncadecimal(curPremio_Aux - (curPremio_Aux / (1 + perc_iof)))
        curPremio_Aux = curPremio_Aux - Val_IOF

        Val_Ad_Fracionamento = Truncadecimal(curPremio_Aux - (curPremio_Aux / Taxa_Juros))
        curPremio_Aux = curPremio_Aux - Val_Ad_Fracionamento

        Custo_Endosso = Truncadecimal(curPremio_Aux - (curPremio_Aux / (1 + (perc_custo_endosso / 100))))
        curPremio_Aux = curPremio_Aux - Custo_Endosso

        Valor_premio_liquido = curPremio_Aux

    End If


    Exit Sub

Erro:
    MensagemBatch "Rotina: Calcula_Valores. Programa ser� cancelado.", vbOKOnly, "Erro c�lculo do valor a ser cobrado", True
    Call TerminaSEGBR

End Sub

Public Sub valor_Minimo_endosso(produtoId, RamoId As String)
    Dim rc As Recordset
    On Error GoTo Erro

    SQL = "SELECT a.ramo_id ,b.nome, a.dt_inicio_vigencia, a.dt_fim_vigencia, a.objetivo, a.participacao, a.processa_cancelamento, a.processa_cancelamento_endosso, a.max_dias_atraso, a.max_dias_atraso_endosso, a.permite_aviso_proposta_basica, a.tp_calculo_iof, "
    SQL = SQL & " a.val_minimo_premio, a.val_premio_minimo_endosso, a.processa_reducao_is, a.processa_reintegracao_is, a.gera_retorno_bb_alteracao_is, qtd_min_cobertura, cod_produto_bb, val_lim_automatico_is  "
    'pcarvalho - 10/06/2003 Incluir Pr�-an�lise
    SQL = SQL & ", a.processa_pre_analise "
    SQL = SQL & " FROM item_produto_tb a, ramo_tb b "
    SQL = SQL & " WHERE a.ramo_id = " & RamoId & " and produto_id = " & produtoId
    SQL = SQL & "   and dt_fim_vigencia is NULL "
    SQL = SQL & " ORDER BY a.ramo_id"

    If rdocn Is Nothing Then
        Call Conexao
    Else
        If rdocn.State = 0 Then Call Conexao
    End If

    Set rc = rdocn.Execute(SQL)

    If Not rc.EOF Then
        Val_minimo_endosso = Val(rc!val_premio_minimo_endosso)
    End If

    rc.Close

    Exit Sub

Erro:
    MensagemBatch "Rotina: valor_Minimo_endosso. Programa ser� cancelado.", vbOKOnly, "Erro c�lculo do custo", True
    Call TerminaSEGBR

End Sub


Public Sub Grava_Cobranca_Reintegracao()

    Dim perc_corretagem_aux As Currency

    On Error GoTo Erro

    'Obt�m Corretagens
    Call Obtem_Corretagem
    Call Obtem_Corretagem_estipulante

    'Obt�m a �ltima parcela do agendamento
    Call Obtem_Ultima_Parcela

    'Calcula valores de comiss�o
    perc_corretagem_aux = Perc_Corretagem    '- Perc_Comissao_Est  ' 2% se refere a comissao de estipulante
    Val_Comissao_bruta = Truncadecimal((Valor_premio - Val_IOF - Custo_Endosso) * (perc_corretagem_aux / 100))
    Val_IR = Truncadecimal(Val_Comissao_bruta * 0.015)
    Val_Comissao_Est = Truncadecimal((Valor_premio - Val_IOF - Custo_Endosso) * (Perc_Comissao_Est / 100))
    Val_Comissao = Val_Comissao_bruta - Val_IR

    'Grava registro do endosso financeiro e agendamento cobran�a
    If Trim(sTipoEndosso) <> "Reintegra��o Agricola" Then
        Call Inclui_Endosso_Financeiro
    Else
        Call Inclui_Endosso_Financeiro_Reintegracao
    End If

    Call Inclui_Agendamento
    Call Inclui_Corretagem_Endosso

    'Luciana - 23/06/2005
    If Est_Cliente_ID > 0 Then
        Call Inclui_Corretagem_Endosso_Estipulante
    End If

    Exit Sub

Erro:
    MensagemBatch "Rotina: Grava_Cobranca_Reintegracao. Programa ser� cancelado.", vbOKOnly, "Erro ao gravar cobran�a de reintegra��o", True
    Call TerminaSEGBR

End Sub
Private Sub Le_Custo_Endosso()

    Dim rc As Recordset

    On Error GoTo Erro

    SQL = "SELECT isnull(perc_custo_endosso,0) perc_custo_endosso, "
    SQL = SQL & " isnull(val_custo_endosso_max,0) val_custo_endosso_max "
    SQL = SQL & " FROM custo_tb "
    SQL = SQL & " WHERE produto_id = " & Produto_Atual
    SQL = SQL & "   AND dt_fim_vigencia is null "

    If rdocn Is Nothing Then
        Call Conexao
    Else
        If rdocn.State = 0 Then Call Conexao
    End If

    Set rc = rdocn.Execute(SQL)

    If ConfiguracaoBrasil Then
        Custo_Endosso_Max = Val(rc!val_custo_endosso_max)
        perc_custo_endosso = Val(rc!perc_custo_endosso)
    Else
        Custo_Endosso_Max = rc!val_custo_endosso_max
        perc_custo_endosso = rc!perc_custo_endosso
    End If
    rc.Close

    Exit Sub

Erro:
    MensagemBatch "Rotina: Le Custo Apolice. Programa ser� cancelado.", vbOKOnly, "Erro c�lculo do custo", True
    Call TerminaSEGBR

End Sub
Private Sub Le_IOF(Subramo_id As Integer, ByVal dt_referencia As String)

    Dim rc As Recordset
    On Error GoTo Erro

    If IsDate(dt_referencia) Then
        dt_referencia = Format(dt_referencia, "yyyymmdd")
    End If

    SQL = "select valor = dbo.AliquotaIOF_fn("
    SQL = SQL & Subramo_id
    SQL = SQL & ",'"
    SQL = SQL & dt_referencia
    SQL = SQL & "')/100"

    If rdocn Is Nothing Then
        Call Conexao
    Else
        If rdocn.State = 0 Then Call Conexao
    End If

    Set rc = rdocn.Execute(SQL)

    If Not IsNull(rc!valor) Then
        If ConfiguracaoBrasil Then
            perc_iof = Val(rc!valor)
        Else
            perc_iof = rc!valor
        End If
    End If
    rc.Close

    Exit Sub

Erro:
    MensagemBatch "Rotina: Le Custo IOF. Programa ser� cancelado.", vbOKOnly, "Erro c�lculo do IOF", True
    Call TerminaSEGBR


End Sub
Private Sub Obtem_Taxa_Juros()

    Dim rc As Recordset

    On Error GoTo Erro

    SQL = "SELECT isnull(fat_fracionamento,1) "
    SQL = SQL & " FROM produto_juros_tb a, juros_parcelamento_tb b"
    SQL = SQL & " WHERE a.produto_id  = " & Produto_Atual
    SQL = SQL & "   AND a.juros_id  = b.juros_id "
    SQL = SQL & "   AND tp_juros = 'p' "
    SQL = SQL & "   AND qtd_parcelas = 1 "
    SQL = SQL & "   AND a.dt_fim_vigencia is null "

    If rdocn Is Nothing Then
        Call Conexao
    Else
        If rdocn.State = 0 Then Call Conexao
    End If

    Set rc = rdocn.Execute(SQL)

    If Not rc.EOF Then
        Taxa_Juros = Val(rc(0))
    Else
        MensagemBatch "Taxa Juros Inexistente.", vbOKOnly, "Erro obtendo taxa de Juros", True
        rc.Close
        Exit Sub
    End If
    rc.Close

    Exit Sub

Erro:
    MensagemBatch "Rotina: Obtem Taxa Juros. Programa ser� cancelado.", vbOKOnly, "Erro obtendo taxa de Juros", True
    Call TerminaSEGBR

End Sub

Private Sub Obtem_Corretagem()

    Dim rc As Recordset

    On Error GoTo Erro

    SQL = "SELECT isnull(b.perc_corretagem,0) perc_corretagem, a.corretor_id, b.sucursal_corretor_id "
    SQL = SQL & " FROM corretagem_tb a,"
    SQL = SQL & "      corretagem_pj_tb b "
    SQL = SQL & " WHERE a.proposta_id = " & Proposta_Atual
    SQL = SQL & "   AND a.proposta_id = b.proposta_id "
    SQL = SQL & "   AND a.dt_fim_corretagem is null"

    If rdocn Is Nothing Then
        Call Conexao
    Else
        If rdocn.State = 0 Then Call Conexao
    End If

    Set rc = rdocn.Execute(SQL)

    If Not rc.EOF Then
        Perc_Corretagem = Val(rc!Perc_Corretagem)
        Corretor_ID = rc!Corretor_ID
        Sucursal_Corretor_id = rc!Sucursal_Corretor_id
        Tp_Corretagem = "1"
    Else
        rc.Close
        SQL = "SELECT isnull(b.perc_corretagem,0) perc_corretagem, a.corretor_id, 0000 sucursal_corretor_id "
        SQL = SQL & " FROM corretagem_tb a,"
        SQL = SQL & "      corretagem_pf_tb b "
        SQL = SQL & " WHERE a.proposta_id = " & Proposta_Atual
        SQL = SQL & "   AND a.proposta_id = b.proposta_id "
        SQL = SQL & "   AND a.dt_fim_corretagem is null"

        If rdocn Is Nothing Then
            Call Conexao
        Else
            If rdocn.State = 0 Then Call Conexao
        End If

        Set rc = rdocn.Execute(SQL)

        If Not rc.EOF Then
            Perc_Corretagem = Val(rc!Perc_Corretagem)
            Corretor_ID = rc!Corretor_ID
            Sucursal_Corretor_id = rc!Sucursal_Corretor_id
            Tp_Corretagem = "2"
        Else
            MensagemBatch "Percentual de corretagem n�o encontrado", vbOKOnly, "Erro obtendo corretagem", True
            rc.Close
            Exit Sub
        End If
    End If

    rc.Close

    Exit Sub

Erro:
    MensagemBatch "Rotina: Obtem Dados Corretagem. Programa ser� cancelado", vbOKOnly, "Erro obtendo corretagem", True
    Call TerminaSEGBR

End Sub

Private Sub Obtem_Corretagem_estipulante()

    Dim rc As Recordset

    On Error GoTo Erro

    SQL = "SELECT perc_comissao_estipulante, est_cliente_id "
    SQL = SQL & " FROM representacao_tp_seguro_tb "
    SQL = SQL & " WHERE produto_id = " & Produto_Atual
    SQL = SQL & "   AND dt_fim_representacao is null"

    If rdocn Is Nothing Then
        Call Conexao
    Else
        If rdocn.State = 0 Then Call Conexao
    End If

    Set rc = rdocn.Execute(SQL)


    If Not rc.EOF Then
        If ConfiguracaoBrasil Then
            Perc_Comissao_Est = Val(rc!Perc_Comissao_Estipulante)
        Else
            Perc_Comissao_Est = rc!Perc_Comissao_Estipulante
        End If
        Est_Cliente_ID = rc!Est_Cliente_ID
    Else
        Perc_Comissao_Est = 0
        Est_Cliente_ID = 0
    End If
    rc.Close

    Exit Sub

Erro:
    MensagemBatch "Rotina: Obtem Dados Corretagem Estipulante. Programa ser� cancelado", vbOKOnly, "Erro obtendo corretagem estipulante", True
    Call TerminaSEGBR

End Sub

Private Sub Obtem_Taxa_Cobertura_Reintegracao()

    Dim rc As Recordset

    On Error GoTo Erro

    SQL = ""
    SQL = SQL & " SELECT fat_taxa " & vbNewLine
    SQL = SQL & " FROM escolha_tp_cob_generico_tb  WITH (NOLOCK)  " & vbNewLine
    SQL = SQL & " JOIN proposta_tb  WITH (NOLOCK)  " & vbNewLine
    SQL = SQL & "   ON proposta_tb.proposta_id = escolha_tp_cob_generico_tb.proposta_id " & vbNewLine
    SQL = SQL & " WHERE escolha_tp_cob_generico_tb.proposta_id = " & Proposta_Atual & vbNewLine
    SQL = SQL & "   AND escolha_tp_cob_generico_tb.num_endosso = " & 0 & vbNewLine
    SQL = SQL & "   AND escolha_tp_cob_generico_tb.tp_cobertura_id = " & sCoberturaId & vbNewLine
    SQL = SQL & "   AND escolha_tp_cob_generico_tb.produto_id = " & Produto_Atual & vbNewLine

    If rdocn Is Nothing Then
        Call Conexao
    Else
        If rdocn.State = 0 Then Call Conexao
    End If

    Set rc = rdocn.Execute(SQL)

    If Not rc.EOF Then
        dFatTaxa = rc!fat_taxa
    End If
    rc.Close

    Exit Sub

Erro:
    MensagemBatch "Rotina: Obtem Taxa Cobertura Reintegra��o. Programa ser� cancelado", vbOKOnly, "Erro Obtem Taxa Cobertura Reintegra��o", True
    Call TerminaSEGBR

End Sub

Private Sub Obtem_Ultima_Parcela()

    Dim rc As Recordset

    On Error GoTo Erro

    SQL = "SELECT isnull(max(num_cobranca),0) "
    SQL = SQL & " FROM agendamento_cobranca_tb "
    SQL = SQL & " WHERE proposta_id = " & Proposta_Atual


    If rdocn Is Nothing Then
        Call Conexao
    Else
        If rdocn.State = 0 Then Call Conexao
    End If

    Set rc = rdocn.Execute(SQL)

    If Not rc.EOF Then
        Num_parcela = rc(0) + 1
    Else
        MensagemBatch "Agendamento Inexistente ou j� Processado. Programa ser� cancelado.", vbOKOnly, "Erro obtendo �ltima parcela", True
        rc.Close
        Call TerminaSEGBR
    End If
    rc.Close

    Exit Sub

Erro:
    MensagemBatch "Rotina : Obtem Ultima parcela. Programa ser� cancelado", vbOKOnly, "Erro obtendo �ltima parcela", True
    Call TerminaSEGBR

End Sub

Private Sub Inclui_Endosso_Financeiro()

    Dim rc As Recordset
    Dim valor1 As String
    Dim valor2 As String
    Dim valor3 As String
    Dim valor4 As String
    Dim valor5 As String
    Dim valor6 As String
    Dim valor7 As String
    Dim valor8 As String

    On Error GoTo Erro


    valor1 = MudaVirgulaParaPonto(CStr(Valor_premio))
    valor2 = MudaVirgulaParaPonto(CStr(Val_Comissao_bruta))
    valor3 = MudaVirgulaParaPonto(CStr(Val_IOF))
    valor4 = MudaVirgulaParaPonto(CStr(Val_IR))
    valor5 = MudaVirgulaParaPonto(CStr(Val_Ad_Fracionamento))
    valor6 = MudaVirgulaParaPonto(CStr(Val_Comissao_Est))
    valor7 = MudaVirgulaParaPonto(CStr(Custo_Endosso))
    valor8 = MudaVirgulaParaPonto(CStr(Valor_premio_liquido))


    SQL = "exec endosso_financ_spi "
    SQL = SQL & Proposta_Atual & ", "                           '@proposta_id
    SQL = SQL & Endosso_Reintegracao_id & ", "                  '@endosso_id
    SQL = SQL & "'" & cUserName & "', "                         '@usuario
    SQL = SQL & valor1 & ", "                                   '@val_financeiro
    SQL = SQL & "1, "                                           '@num_parcelas
    SQL = SQL & valor8 & ", "                                   '@val_premio_tarifa
    SQL = SQL & valor7 & ", "                                   '@custo_apolice
    SQL = SQL & valor2 & ", "                                   '@val_comissao"
    SQL = SQL & valor6 & ", "                                   '@val_comissao_est
    SQL = SQL & valor3 & ", "                                   '@val_iof
    SQL = SQL & valor4 & ", "                                   '@val_ir
    SQL = SQL & "0, "                                           '@val_desconto_comercial
    SQL = SQL & valor5 & ", "                                   '@val_adic_frac
    SQL = SQL & "'" & Format(Data_Endosso, "yyyymmdd") & "', "  '@dt_pgto
    SQL = SQL & "'n'"                                           '@situacao_re_seguro

    If rdocn Is Nothing Then
        Call Conexao
    Else
        If rdocn.State = 0 Then Call Conexao
    End If

    Set rc = rdocn.Execute(SQL)

    'rc.Close

    Exit Sub

Erro:
    MensagemBatch "Rotina : Inclui Endosso Financeiro ", vbOKOnly, "Erro incluindo endosso financeiro", True
    Call TerminaSEGBR

End Sub
Private Sub Inclui_Endosso_Financeiro_Reintegracao()

    Dim rc As Recordset
    Dim valor1 As String
    Dim valor2 As String
    Dim valor3 As String
    Dim valor4 As String
    Dim valor5 As String
    Dim valor6 As String
    Dim valor7 As String
    Dim valor8 As String

    On Error GoTo Erro


    valor1 = MudaVirgulaParaPonto(CStr(Valor_premio))
    valor2 = MudaVirgulaParaPonto(CStr(Val_Comissao_bruta))
    valor3 = MudaVirgulaParaPonto(CStr(Val_IOF))
    valor4 = MudaVirgulaParaPonto(CStr(Val_IR))
    valor5 = MudaVirgulaParaPonto(CStr(Val_Ad_Fracionamento))
    valor6 = MudaVirgulaParaPonto(CStr(Val_Comissao_Est))
    valor7 = MudaVirgulaParaPonto(CStr(Custo_Endosso))
    valor8 = MudaVirgulaParaPonto(CStr(Valor_premio_liquido))



    SQL = "exec endosso_financ_spi "
    SQL = SQL & Proposta_Atual & ", "                           '@proposta_id
    SQL = SQL & Endosso_Reintegracao_id & ", "                  '@endosso_id
    SQL = SQL & "'" & cUserName & "', "                         '@usuario
    SQL = SQL & valor1 & ", "                                   '@val_financeiro
    'alterado o numero de parcelas de 1 para 5 de acordo com a solicita��o do banco - fadao
    'SQL = SQL & "1, "                                           '@num_parcelas
    SQL = SQL & "5, "                                           '@num_parcelas
    SQL = SQL & valor8 & ", "                                   '@val_premio_tarifa
    SQL = SQL & valor7 & ", "                                   '@custo_apolice
    SQL = SQL & valor2 & ", "                                   '@val_comissao"
    SQL = SQL & valor6 & ", "                                   '@val_comissao_est
    SQL = SQL & valor3 & ", "                                   '@val_iof
    SQL = SQL & valor4 & ", "                                   '@val_ir
    SQL = SQL & "0, "                                           '@val_desconto_comercial
    SQL = SQL & valor5 & ", "                                   '@val_adic_frac
    SQL = SQL & "'" & Format(Data_Endosso, "yyyymmdd") & "', "  '@dt_pgto
    SQL = SQL & "'n'"                                           '@situacao_re_seguro

    If rdocn Is Nothing Then
        Call Conexao
    Else
        If rdocn.State = 0 Then Call Conexao
    End If

    Set rc = rdocn.Execute(SQL)

    'rc.Close

    Exit Sub

Erro:
    MensagemBatch "Rotina : Inclui Endosso Financeiro Reintegra��o ", vbOKOnly, "Erro incluindo endosso financeiro reintegra��o", True
    Call TerminaSEGBR

End Sub
Private Sub Inclui_Agendamento()

    Dim rc As Recordset
    Dim Data_vencimento As Date

    On Error GoTo Erro

    'Data de vencimento, 30 dias ap�s a data do sistema
    Data_vencimento = DateAdd("d", 30, Data_Sistema)

    'Obt�m o conv�nio para montar o nosso n�mero
    Convenio = Obtem_Numero_do_Convenio(CInt(Produto_Atual), CInt(Ramo_Atual), 2, CInt(Seguro_Moeda_id))
    Call Obtem_Nosso_Numero
    Nosso_Numero_dv = calcula_dv_nosso_numero(Nosso_Numero)

    If CInt(Produto_Atual) = 1204 Or CInt(Produto_Atual) = 1152 Or CInt(Produto_Atual) = 1240 Then  '16/07/2018 - Schoralick (ntendencia) - 00421597-seguro-faturamento-pecuario [add produto 1240]

        SQL = "exec documento_agendar_spi "
        SQL = SQL & Proposta_Atual
        SQL = SQL & ", " & Endosso_Reintegracao_id
        SQL = SQL & ", " & "null"
        SQL = SQL & ", " & "'" & Trim(cUserName) & "'"

    Else

        SQL = "exec agendamento_cobranca_RE_spi "
        SQL = SQL & Proposta_Atual                                          '@proposta_id
        SQL = SQL & ", " & Num_parcela                                      '@num_cobranca
        SQL = SQL & ", " & Num_Cartao                                       '@num_cartao
        SQL = SQL & ", '" & Format(Dt_Validade_Cartao, "yyyymmdd") & "'"    '@dt_validade_cartao
        SQL = SQL & ", " & Apolice_Atual                                    '@apolice_id
        SQL = SQL & ", " & Sucursal_Atual                                   '@sucursal_seguradora_id
        SQL = SQL & ", " & Seguradora_Atual                                 '@seguradora_cod_susep
        SQL = SQL & ", " & Ramo_Atual                                       '@ramo_id
        SQL = SQL & ", " & IIf((IsNull(Banco) Or Banco = ""), "null", Banco)    '@deb_banco_id
        SQL = SQL & ", " & IIf((IsNull(Agencia) Or Agencia = ""), "null", Agencia)          '@deb_agencia_id
        SQL = SQL & ", " & IIf((IsNull(Conta_Corrente) Or Conta_Corrente = ""), "null", Conta_Corrente)    '@deb_conta_corrente_id
        SQL = SQL & ", '" & Format(Data_vencimento, "yyyymmdd") & "'"       '@dt_agendamento
        SQL = SQL & ", " & MudaVirgulaParaPonto(Valor_premio)               '@val_parcela
        SQL = SQL & ", " & MudaVirgulaParaPonto(Val_IOF)                    '@val_iof
        SQL = SQL & ", " & 1                                                '@qtd_parcelas
        SQL = SQL & ", " & Nosso_Numero                                     '@nosso_numero
        SQL = SQL & ", '" & Nosso_Numero_dv & "'"                           '@nosso_numero_dv
        SQL = SQL & ", '" & cUserName & "' "                                '@usuario
        SQL = SQL & ", " & Endosso_Reintegracao_id                          '@num_endosso
        SQL = SQL & ", " & MudaVirgulaParaPonto(Val_IR)                     '@val_ir
        SQL = SQL & ", " & MudaVirgulaParaPonto(Val_Comissao_bruta)         '@val_comissao
        SQL = SQL & ", " & MudaVirgulaParaPonto(Val_Comissao_Est)           '@val_comissao_est
        SQL = SQL & ", " & "null"                                           '@val_desconto
        SQL = SQL & ", " & MudaVirgulaParaPonto(Val_Ad_Fracionamento)       '@val_adic_fracionamento
        SQL = SQL & ", 1"                                                   '@num_parcela_endosso
    End If

    If rdocn Is Nothing Then
        Call Conexao
    Else
        If rdocn.State = 0 Then Call Conexao
    End If

    Set rc = rdocn.Execute(SQL)

    If CInt(Produto_Atual) <> 1204 And CInt(Produto_Atual) <> 1152 Then

        SQL = "exec emissao_CBR_spi "
        SQL = SQL & Proposta_Atual
        SQL = SQL & ", " & Num_parcela
        SQL = SQL & ", 1"    'Via de emiss�o - sempre 1 no momento da cria��o da parcela
        SQL = SQL & ", Null"    'data que foi emitida o boleto - Preenchido pelo programa de emiss�o
        SQL = SQL & ", '" & cUserName & "'"

        If rdocn Is Nothing Then
            Call Conexao
        Else
            If rdocn.State = 0 Then Call Conexao
        End If

        Set rc = rdocn.Execute(SQL)

    End If

    'rc.Close

    Exit Sub

Erro:
    MensagemBatch "Rotina Inclui Agendamento. Programa ser� Cancelado", vbOKOnly, "Erro incluindo agendamento", True
    Call TerminaSEGBR

End Sub

Private Sub Obtem_Convenio()

    Dim rc As rdoResultset

    On Error GoTo Erro

    SQL = "SELECT num_convenio "
    SQL = SQL & " FROM tp_movimentacao_financ_tb "
    SQL = SQL & " WHERE ramo_id = " & Ramo_Atual
    SQL = SQL & "   AND produto_id = " & Produto_Atual
    SQL = SQL & "   AND tp_operacao_financ_id = 2 "
    Set rc = rdocn.OpenResultset(SQL)

    If Not rc.EOF Then
        Convenio = rc(0)
    Else
        MensagemBatch "Convenio n�o cadastrado para produto " & Produto_Atual & " .Programa ser� cancelado.", vbOKOnly, "Erro obtendo convenio", True
        Call TerminaSEGBR
    End If
    rc.Close

    Exit Sub

Erro:
    MensagemBatch "Rotina: Buscar Conv�nio. Programa ser� Cancelado", vbOKOnly, "Erro obtendo convenio", True
    Call TerminaSEGBR

End Sub

Private Sub Obtem_Nosso_Numero()

    Dim rc As Recordset

    On Error GoTo Erro

    SQL = "exec convenio_chave_spu "
    SQL = SQL & "'" & Convenio & "'"
    SQL = SQL & ", '" & cUserName & "'"
    SQL = SQL & ", 'B'"    'Como esta rotina � batch o par�metro tem que ser informado para n�o ocorrer erro de sistema bloqueado

    If rdocn Is Nothing Then
        Call Conexao
    Else
        If rdocn.State = 0 Then Call Conexao
    End If

    Set rc = rdocn.Execute(SQL)
    'rc.Close


    If Not rc.EOF Then
        Nosso_Numero = Convenio & Format(rc(0), "0000000")
    Else
        MensagemBatch "Rotina: Obtem Nosso Numero. N�o consegui gerar Nosso N�mero.", vbOKOnly, "Erro obtendo nosso n�mero", True
        Call TerminaSEGBR
    End If
    rc.Close

    Exit Sub

Erro:
    MensagemBatch "Rotina : Obtem Nosso Numero. Programa ser� Cancelado", vbOKOnly, "Erro obtendo nosso n�mero", True
    Call TerminaSEGBR

End Sub

Private Function calcula_dv_nosso_numero(ByVal Parte As String) As String

    Dim Peso As Integer
    Dim Soma As Integer
    Dim Parcela As Integer
    Dim dv As Integer
    Dim result As String
    Dim i As Integer

    On Error GoTo Erro

    Peso = 9
    Soma = 0
    For i = Len(Parte) To 1 Step -1
        Parcela = Peso * Val(Mid(Parte, i, 1))
        Soma = Soma + Parcela
        Peso = Peso - 1
        If Peso < 2 Then Peso = 9
    Next i

    dv = (Soma Mod 11)
    If dv = 10 Then
        result = "X"
    Else
        result = Format(dv, "0")
    End If

    calcula_dv_nosso_numero = result

    Exit Function

Erro:
    MensagemBatch "Rotina: calcula_dv_nosso_numero", vbOKOnly, "Erro calculando d�gito do nosso n�mero", True
    Call TerminaSEGBR
End Function



Private Sub Inclui_Corretagem_Endosso()

    Dim rc As Recordset
    Dim valor1 As String
    Dim valor2 As String
    Dim valor3 As String    'Percentual Corretagem

    On Error GoTo Erro

    ' If ConfiguracaoBrasil Then
    valor1 = MudaVirgulaParaPonto(CDbl(Val_Comissao_bruta - Val_Comissao_Est))
    valor2 = MudaVirgulaParaPonto(CDbl(Val_IR))
    valor3 = MudaVirgulaParaPonto(CDbl(Perc_Corretagem))
    '    Else
    '       valor1 = Val_Comissao_bruta - Val_Comissao_Est
    '       valor2 = Val_IR
    '       valor3 = Perc_Corretagem
    '    End If

    SQL = "Exec corretagem_endosso_fin_spi "
    SQL = SQL & Proposta_Atual                      '@proposta_id
    SQL = SQL & " , " & Endosso_Reintegracao_id     '@endosso_id
    SQL = SQL & " , " & Tp_Corretagem               '@tipo
    SQL = SQL & " , " & Corretor_ID                 '@corretor_id
    SQL = SQL & " , '" & Sucursal_Corretor_id & "'"    '@sucursal_corretor_id
    SQL = SQL & " , " & valor3                      '@perc_corretagem
    SQL = SQL & " , " & valor1                      '@val_comissao
    SQL = SQL & " , " & valor2                      '@val_ir
    SQL = SQL & " ,'" & cUserName & "'"             '@usuario

    If rdocn Is Nothing Then
        Call Conexao
    Else
        If rdocn.State = 0 Then Call Conexao
    End If

    Set rc = rdocn.Execute(SQL)
    'rc.Close

    Exit Sub

Erro:
    MensagemBatch "Rotina: Inclui_Corretagem_Endosso", vbOKOnly, "Erro incluindo corretagem", True
    Call TerminaSEGBR
End Sub

Private Sub Inclui_Corretagem_Endosso_Estipulante()

    Dim rc As Recordset
    Dim valor1 As String

    On Error GoTo Erro

    'If ConfiguracaoBrasil Then
    valor1 = MudaVirgulaParaPonto(CStr(Perc_Comissao_Est))
    '    Else
    '       valor1 = Perc_Comissao_Est
    '    End If

    SQL = "Exec pro_labore_endosso_fin_spi "
    SQL = SQL & Proposta_Atual                      '@proposta_id
    SQL = SQL & " , " & Endosso_Reintegracao_id     '@endosso_id
    SQL = SQL & " , " & Est_Cliente_ID              '@cliente_id
    SQL = SQL & " , " & valor1                      '@perc_pro_labore
    SQL = SQL & " ,'" & cUserName & "'"             '@usuario

    If rdocn Is Nothing Then
        Call Conexao
    Else
        If rdocn.State = 0 Then Call Conexao
    End If

    Set rc = rdocn.Execute(SQL)
    'rc.Close
    Exit Sub

Erro:
    MensagemBatch "Rotina: Inclui_Corretagem_Endosso_Estipulante", vbOKOnly, "Erro incluindo corretagem estipulante", True
    Call TerminaSEGBR
End Sub

Function Verificar_promocao_empresarial(prop_id As String) As Boolean

    Dim SQL As String
    Dim rs As rdoResultset

    On Error GoTo Erro

    SQL = " Select proposta_id from emi_re_proposta_tb "
    SQL = SQL & " where proposta_id = " & prop_id
    SQL = SQL & " and tp_registro = '01' "
    SQL = SQL & " and dt_contratacao >= '20040601' and dt_contratacao <= '20040630' "

    Set rs = rdocn.OpenResultset(SQL)

    If Not rs.EOF Then
        Verificar_promocao_empresarial = True
    Else
        Verificar_promocao_empresarial = False
    End If

    rs.Close

    Exit Function

Erro:
    MensagemBatch "Rotina: Verificar_promocao_empresarial", vbOKOnly, "Erro verificando promo��o empresarial", True
    Call TerminaSEGBR
End Function

'Public Function _
 'ExecutarSQL(ByVal eSistema As String, _
 '            ByVal eAmbiente As Integer, _
 '            ByVal eSigla_Recurso As String, _
 '            ByVal eDescricaoArquivo As String, _
 '            ByVal eSQL As String, _
 '   Optional ByVal eRetRecordSet As Boolean = True, _
 '   Optional ByVal eExecASincrona As Boolean = False, _
 '   Optional ByVal eTimeOut As Long = 30000, _
 '   Optional ByVal eLockType As LockTypeEnum = adLockReadOnly, _
 '   Optional ByVal eCursorLocation As CursorLocationEnum = adUseClient, _
 '   Optional ByVal eCursorType As CursorTypeEnum = adLockReadOnly) As Variant
'
'On Error GoTo ErrExecutarQuery
'Dim Ambiente As String
'Ambiente = ConvAmbiente(eAmbiente)
'
'          ' Verifica se o usu�rio enviou o onjeto ja construido
'          If Not IsMissing(eObjSABL0100) Then
'             Set eObjSABL0100 = CreateObject("SABL0100.cls000100")
'          End If   'createobject("SABL0100.cls000100")
'                 If eRetRecordSet = True Then
'                 Set ExecutarSQL = New ADODB.Recordset
'                 Set ExecutarSQL = eObjSABL0100.ExecutarSQL _
                  '                 (eSistema, Ambiente, eSigla_Recurso, eDescricaoArquivo, _
                  '                 eSQL, eChave, eRetRecordSet, eExecASincrona, eTimeOut, _
                  '                 eLockType, eCursorLocation, eCursorType)
'                 Set eObjSABL0100 = Nothing
'              Else
'                 ExecutarSQL = eObjSABL0100.ExecutarSQL _
                  '                 (eSistema, Ambiente, eSigla_Recurso, eDescricaoArquivo, _
                  '                 eSQL, eChave, eRetRecordSet, eExecASincrona, eTimeOut, _
                  '                 eLockType, eCursorLocation, eCursorType)
'                 Set eObjSABL0100 = Nothing
'              End If
'
'       Exit Function
'ErrExecutarQuery:
'
'    'MsgBox "A Conex�o ao banco de dados n�o foi poss�vel, o tempo para a conex�o foi expirado."
'    ErrCode = Err.Number
'    ErrDesc = Err.Description
'
'    Call Err.Raise(ErrCode, , "BancodeDados.ExecutarSQL - " & ErrDesc)
'
'End Function





