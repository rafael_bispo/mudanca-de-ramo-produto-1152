VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.2#0"; "MSCOMCTL.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.ocx"
Begin VB.Form FrmConCorretores 
   Caption         =   "SEG6084 - Consulta de Corretores -"
   ClientHeight    =   5880
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   8340
   LinkTopic       =   "Form1"
   ScaleHeight     =   5880
   ScaleWidth      =   8340
   StartUpPosition =   3  'Windows Default
   Begin VB.Frame FraCriterios 
      Caption         =   "Crit�rios"
      Height          =   1485
      Left            =   90
      TabIndex        =   0
      Top             =   90
      Width           =   8115
      Begin VB.TextBox Txtconteudo 
         Height          =   315
         Left            =   3000
         TabIndex        =   4
         Top             =   570
         Width           =   4875
      End
      Begin VB.ComboBox CmbCampo 
         Height          =   315
         ItemData        =   "FrmConCorretores.frx":0000
         Left            =   180
         List            =   "FrmConCorretores.frx":0007
         Style           =   2  'Dropdown List
         TabIndex        =   2
         Top             =   570
         Width           =   2625
      End
      Begin VB.CommandButton Btnpesquisa 
         Caption         =   "Pesquisar"
         Height          =   345
         Left            =   6555
         TabIndex        =   5
         Top             =   1050
         Width           =   1305
      End
      Begin VB.Label Label2 
         Caption         =   "Conte�do:"
         Height          =   225
         Left            =   3030
         TabIndex        =   3
         Top             =   300
         Width           =   975
      End
      Begin VB.Label Label1 
         Caption         =   "Pesquisar por:"
         Height          =   225
         Left            =   210
         TabIndex        =   1
         Top             =   300
         Width           =   1185
      End
   End
   Begin VB.CommandButton BtnCancelar 
      Caption         =   "Cancelar"
      Height          =   375
      Left            =   6750
      TabIndex        =   8
      Top             =   5130
      Width           =   1395
   End
   Begin VB.CommandButton Btn_Ok 
      Caption         =   "Ok"
      Height          =   375
      Left            =   5250
      TabIndex        =   7
      Top             =   5130
      Width           =   1395
   End
   Begin MSComctlLib.StatusBar StatusBar1 
      Align           =   2  'Align Bottom
      Height          =   300
      Left            =   0
      TabIndex        =   9
      Top             =   5580
      Width           =   8340
      _ExtentX        =   14711
      _ExtentY        =   529
      Style           =   1
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSFlexGridLib.MSFlexGrid GridCorretor 
      Height          =   3345
      Left            =   90
      TabIndex        =   6
      Top             =   1680
      Width           =   8145
      _ExtentX        =   14367
      _ExtentY        =   5900
      _Version        =   393216
      Rows            =   1
      Cols            =   4
      FixedCols       =   0
      ScrollBars      =   2
      FormatString    =   $"FrmConCorretores.frx":0011
   End
End
Attribute VB_Name = "FrmConCorretores"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Sub Monta_GridCorretor(rc As Recordset)
    Dim Linha As String
    ''
    GridCorretor.Rows = 1
    ''
    Do While Not rc.EOF
        Linha = Format(rc!Corretor_ID, "000000-000")
        Linha = Linha & vbTab & IIf(rc!tp_pessoa = 1, "J", "F")
        Linha = Linha & vbTab & Format(rc!Sucursal_id, "@@@@")
        Linha = Linha & vbTab & Trim(rc!Nome)
        '
        GridCorretor.AddItem Linha
        GridCorretor.RowData(GridCorretor.Rows - 1) = rc!Corretor_ID
        '
        rc.MoveNext
    Loop
    '
    GridCorretor.Refresh

End Sub

Private Sub Btn_Ok_Click()

    If GridCorretor.RowSel = 0 Then
        mensagem_erro 6, "Selecione um Corretor."
        GridCorretor.SetFocus
        Exit Sub
    End If

    With frmEndosso
        .TxtCodCorretor(0).Text = GridCorretor.RowData(GridCorretor.Row)
        .TxtCodCorretor(1).Text = GridCorretor.TextMatrix(GridCorretor.Row, 3)
        .TxtSucCorretor.Text = GridCorretor.TextMatrix(GridCorretor.Row, 2)
        .TxtTpCorretor.Text = IIf(GridCorretor.TextMatrix(GridCorretor.Row, 1) = "J", "1", "2")
        .mskPercCorretagem.Text = "0,0000"
        .cmdCorretorAdicionar.Enabled = True
    End With

    Unload Me

End Sub

Private Sub BtnCancelar_Click()

    Unload Me

End Sub


Private Sub Btnpesquisa_Click()
    Dim Rst As Recordset
    Dim vSql As String
    Dim oCorretagem As Object

    On Error GoTo Erro_Le_Corretor
    '
    GridCorretor.Rows = 1
    '
    If Cmbcampo.Text = "" Or Cmbcampo.ListIndex = -1 Then
        mensagem_erro 6, "Informe o campo a ser pesquisado."
        Cmbcampo.SetFocus
        Exit Sub
    End If
    '
    If txtConteudo.Text = "" Then
        mensagem_erro 6, "Informe o conte�do a ser pesquisado."
        txtConteudo.SetFocus
        Exit Sub
    End If
    '
    MousePointer = vbHourglass
    '
    Select Case Cmbcampo.ItemData(Cmbcampo.ListIndex)
    Case 0    'C�d. SUSEP

        Set oCorretagem = CreateObject("SEGL0022.cls00117")

        Set Rst = oCorretagem.ConsultarCorretor(glAmbiente_id, App.Title, _
                                                App.FileDescription, gsSIGLASISTEMA, _
                                                0, MudaAspaSimples(txtConteudo.Text))

        Set oCorretagem = Nothing

    End Select

    If Not Rst.EOF Then
        Call Monta_GridCorretor(Rst)
    End If
    ''
    Rst.Close
    Set Rst = Nothing
    '
    MousePointer = vbDefault
    '
    If GridCorretor.Rows > 1 Then
        StatusBar1.SimpleText = "Clique no corretor desejado."
    Else
        mensagem_erro 6, "N�o foram encontrados registros para a condi��o especificada."
        StatusBar1.SimpleText = "Escolha o campo de pesquisa e informe o  conte�do a ser pesquisado."
    End If
    '
    Exit Sub
    ''
Erro_Le_Corretor:
    TrataErroGeral "Erro Carregando Dados dos Corretores."
    MousePointer = vbDefault

End Sub

Private Sub CmbCampo_Click()

    txtConteudo.SetFocus

End Sub


Private Sub Form_Activate()

    Cmbcampo.SetFocus

    StatusBar1.SimpleText = "Escolha o campo de pesquisa e informe o  conte�do a ser pesquisado."

End Sub




Private Sub GridCorretor_Click()

    GridCorretor.Row = GridCorretor.RowSel

End Sub

Private Sub GridCorretor_DblClick()

    GridCorretor.Row = GridCorretor.RowSel
    '
    Call Btn_Ok_Click

End Sub


Private Sub Form_Load()

    Me.Caption = "SEG20777 - Consulta de Corretores " & Ambiente

End Sub
