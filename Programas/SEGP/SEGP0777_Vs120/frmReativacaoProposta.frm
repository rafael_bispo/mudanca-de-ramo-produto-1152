VERSION 5.00
Object = "{1CB70356-FEA2-11D4-87FA-00805F396245}#1.0#0"; "mask2s.ocx"
Object = "{C932BA88-4374-101B-A56C-00AA003668DC}#1.1#0"; "MSMASK32.ocx"
Begin VB.Form frmReativacaoProposta 
   BorderStyle     =   5  'Sizable ToolWindow
   ClientHeight    =   8985
   ClientLeft      =   1215
   ClientTop       =   450
   ClientWidth     =   12690
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8985
   ScaleMode       =   0  'User
   ScaleWidth      =   35314.93
   ShowInTaskbar   =   0   'False
   Begin VB.Frame Frame1 
      Caption         =   "Parcelas a vencer"
      Height          =   4575
      Index           =   2
      Left            =   6840
      TabIndex        =   6
      Top             =   2520
      Width           =   5295
      Begin VB.ListBox lstParcelasVencer 
         Height          =   3375
         Left            =   240
         TabIndex        =   17
         Top             =   360
         Width           =   4695
      End
      Begin VB.TextBox txtTotalVencer 
         Enabled         =   0   'False
         Height          =   375
         Left            =   3720
         TabIndex        =   8
         Top             =   3960
         Width           =   1215
      End
      Begin VB.Label Label1 
         Caption         =   "Quantidade de parecelas � vencer"
         Height          =   255
         Index           =   2
         Left            =   1200
         TabIndex        =   13
         Top             =   4080
         Width           =   2535
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Parcelas vencidas"
      Height          =   4575
      Index           =   1
      Left            =   480
      TabIndex        =   5
      Top             =   2520
      Width           =   5295
      Begin VB.ListBox lstParcelasVencidas 
         Height          =   3375
         Left            =   240
         TabIndex        =   16
         Top             =   360
         Width           =   4695
      End
      Begin VB.TextBox txtTotalVencidas 
         Enabled         =   0   'False
         Height          =   375
         Left            =   3720
         TabIndex        =   7
         Top             =   3960
         Width           =   1215
      End
      Begin VB.Label Label1 
         Caption         =   "Quantidade de parecelas vencidas"
         Height          =   255
         Index           =   3
         Left            =   1200
         TabIndex        =   14
         Top             =   4080
         Width           =   2655
      End
   End
   Begin VB.Frame Frame1 
      Height          =   8175
      Index           =   0
      Left            =   240
      TabIndex        =   0
      Top             =   600
      Width           =   12135
      Begin Mask2S.ConMask2S txtValorParcela 
         Height          =   375
         Left            =   9600
         TabIndex        =   18
         Top             =   480
         Width           =   1455
         _ExtentX        =   2566
         _ExtentY        =   661
         mask            =   ""
         text            =   "0,00"
         locked          =   0   'False
         enabled         =   -1  'True
      End
      Begin MSMask.MaskEdBox txtDTReativacao 
         Height          =   375
         Left            =   1920
         TabIndex        =   15
         Top             =   480
         Width           =   1455
         _ExtentX        =   2566
         _ExtentY        =   661
         _Version        =   393216
         Enabled         =   0   'False
         MaxLength       =   10
         Mask            =   "##/##/####"
         PromptChar      =   "_"
      End
      Begin VB.CommandButton cmdAplicarEndosso 
         Caption         =   "&Aplicar"
         Height          =   390
         Left            =   10200
         TabIndex        =   10
         Top             =   7320
         Width           =   1365
      End
      Begin VB.CommandButton cmdSair 
         Caption         =   "&Sair"
         Height          =   390
         Left            =   8400
         TabIndex        =   9
         Top             =   7320
         Width           =   1365
      End
      Begin VB.CheckBox ckbAgendarParcelasVencer 
         Caption         =   "Agendar parcelas � vencer"
         Enabled         =   0   'False
         Height          =   255
         Left            =   8040
         TabIndex        =   4
         Top             =   6600
         Value           =   1  'Checked
         Width           =   2295
      End
      Begin VB.CheckBox ckbAgendarParcelasVencidas 
         Caption         =   "Agendar parcelas vencidas"
         Enabled         =   0   'False
         Height          =   195
         Left            =   1680
         TabIndex        =   3
         Top             =   6600
         Value           =   1  'Checked
         Width           =   2415
      End
      Begin VB.OptionButton optSemCobranca 
         Caption         =   "Sem cobran�a"
         Enabled         =   0   'False
         Height          =   375
         Left            =   3960
         TabIndex        =   2
         Top             =   840
         Width           =   1695
      End
      Begin VB.OptionButton optComCobranca 
         Caption         =   "Com cobran�a"
         Height          =   375
         Left            =   3960
         TabIndex        =   1
         Top             =   480
         Value           =   -1  'True
         Width           =   1815
      End
      Begin VB.Label Label1 
         Caption         =   "Valor da parcela:"
         Height          =   255
         Index           =   1
         Left            =   8310
         TabIndex        =   12
         Top             =   600
         Width           =   1245
      End
      Begin VB.Label Label1 
         Caption         =   "Data Reativa��o:"
         Height          =   255
         Index           =   0
         Left            =   590
         TabIndex        =   11
         Top             =   600
         Width           =   1335
      End
   End
End
Attribute VB_Name = "frmReativacaoProposta"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim ValorParcela As String
Dim CodMovimentacaoFinanceira As Integer


Private Sub cmdAplicarEndosso_Click()

    If Not VerificaData2(txtDTReativacao.Text) Then
        MsgBox ("Data de reativa��o inv�lida!")
        txtDTReativacao.SetFocus
        Exit Sub
    End If

    'JOAO.MACHADO
    'CORRECAO PROVISORIA
    'AGUARDANDO O BB  DESENVOLVER A FUNCIONALIDADE
    'If optComCobranca.Value = False Then
    CodMovimentacaoFinanceira = 1
    'Else
    'CodMovimentacaoFinanceira = 2
    'End If

    If ckbAgendarParcelasVencidas = False Then
        If (MsgBox("Deseja gerar o endosso SEM a cobran�a das parcelas vencidas?", vbYesNo, "Aten��o")) = vbYes Then
            Call ReativacaoProposta(gsSIGLASISTEMA, _
                                    App.Title, _
                                    App.FileDescription, _
                                    glAmbiente_id, _
                                    cUserName, _
                                    frmEndosso.txtPropostaid(0).Text, _
                                    Format(txtDTReativacao.Text, "yyyymmdd"), _
                                    CodMovimentacaoFinanceira, _
                                    frmEndosso.txtDescEndosso.Text)
        End If
    Else
        Call ReativacaoProposta(gsSIGLASISTEMA, _
                                App.Title, _
                                App.FileDescription, _
                                glAmbiente_id, _
                                cUserName, _
                                frmEndosso.txtPropostaid(0).Text, _
                                Format(txtDTReativacao.Text, "yyyymmdd"), _
                                CodMovimentacaoFinanceira, _
                                frmEndosso.txtDescEndosso.Text)
    End If
    Me.Hide
    Call Unload(Me)
    frmEndosso.cmdSairProposta_Click



End Sub

Private Sub cmdSair_Click()
    frmEndosso.Show
    Me.Hide
    Call Unload(Me)
End Sub

Private Sub Form_Load()
    Call InterfaceConstroi
    '12/02/2016 - schoralick (nova consultoria) - 18234489 - Novos produtos para substituir Ouro Vida e outros (inicio)
    Select Case frmEndosso.oDadosEndosso.produtoId
        'In�cio - Isabeli Silva - Confitec SP - Flow 00411015  - Novo Prote��o Ouro - 20180-07-10
    Case 1196, 1235, 1236, 1237, 1239
        'Fim - Isabeli Silva - Confitec SP - Flow 00411015  - Novo Prote��o Ouro - 20180-07-10
        'fim - 18234489
        Call CarregarSemParcelas
    Case Else
        Call CarregarParcelas
    End Select
End Sub

Private Sub Form_Unload(Cancel As Integer)
    frmEndosso.Show
    Call Unload(Me)
End Sub
Private Sub InterfaceConstroi()
    Me.Caption = "SEGP0777 - Endosso On-Line"
    txtDTReativacao.Text = frmEndosso.mskDtEndosso.Text
    txtValorParcela.mask = "5V2"
End Sub

Private Sub CarregarSemParcelas()

    optSemCobranca.Value = True
    optComCobranca.Enabled = False
    txtValorParcela.Visible = False
    Label1(1).Visible = False

End Sub

Private Sub CarregarParcelas()
    Dim rs As Recordset
    Dim TotalVencer As Integer
    Dim TotalVencido As Integer

    On Error GoTo TrataErro


    Set oApolice = CreateObject("SEGL0026.cls00126")

    Set rs = oApolice.RetornarParcelas(gsSIGLASISTEMA, _
                                       App.Title, _
                                       App.FileDescription, _
                                       glAmbiente_id, _
                                       CLng(frmEndosso.txtPropostaid(0).Text))

    ValorParcela = ""
    Do While Not rs.EOF
        'If IsDate(rs("dt_baixa")) Then
        ValorParcela = rs("val_cobranca")
        'Else
        If (CDate(rs("dt_agendamento")) >= CDate(Data_Sistema)) Then
            lstParcelasVencer.AddItem (rs("dt_agendamento"))
            TotalVencer = TotalVencer + 1
        Else
            lstParcelasVencidas.AddItem (rs("dt_agendamento"))
            TotalVencido = TotalVencido + 1
        End If
        'End If
        rs.MoveNext
    Loop
    Set rs = Nothing
    txtTotalVencer.Text = TotalVencer
    txtTotalVencidas.Text = TotalVencido
    txtValorParcela.Text = ValorParcela
    Exit Sub

TrataErro:
    Call TratarErro("ConsultarProposta", Me.name)
    Call FinalizarAplicacao
End Sub


Private Sub optComCobranca_Click()
    Frame1(1).Visible = True
    Frame1(2).Visible = True
    ckbAgendarParcelasVencer.Visible = True
    ckbAgendarParcelasVencidas.Visible = True
    txtValorParcela.Text = ValorParcela

End Sub

Private Sub optSemCobranca_Click()
    Frame1(1).Visible = False
    Frame1(2).Visible = False
    ckbAgendarParcelasVencer.Visible = False
    ckbAgendarParcelasVencidas.Visible = False
    ValorParcela = txtValorParcela.Text
    txtValorParcela.Text = "0,00"

End Sub

Public Sub ReativacaoProposta(ByVal sSiglaSistema As String, _
                              ByVal sSiglaRecurso As String, _
                              ByVal sDescricaoRecurso As String, _
                              ByVal lAmbienteID As Long, _
                              ByVal sUsuario As String, _
                              ByVal lPropostaId As Long, _
                              ByVal lDataReativacao As String, _
                              ByVal lCodMovimentacaoFinanceira As Integer, _
                              ByVal lDescricaoEndosso As String)

    Dim rs As Recordset

    On Error GoTo TrataErro

    Set oApolice = CreateObject("SEGL0026.cls00126")

    Set rs = oApolice.GerarEndossoReativacao(sSiglaSistema, _
                                             sSiglaRecurso, _
                                             sDescricaoRecurso, _
                                             lAmbienteID, _
                                             sUsuario, _
                                             lPropostaId, _
                                             lDataReativacao, _
                                             lCodMovimentacaoFinanceira, _
                                             lDescricaoEndosso)

    If Not rs.EOF Then
        MsgBox (rs(0))
    End If

    Exit Sub

TrataErro:
    Call TratarErro("GerarEndossoReativacao", Me.name)
    Call FinalizarAplicacao
End Sub
