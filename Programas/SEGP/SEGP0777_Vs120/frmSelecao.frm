VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.2#0"; "MSCOMCTL.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form frmSelecao 
   Caption         =   "Form1"
   ClientHeight    =   6240
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   9120
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   6240
   ScaleWidth      =   9120
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame Frame2 
      Caption         =   "Resultado da Pesquisa"
      Height          =   3645
      Left            =   60
      TabIndex        =   15
      Top             =   1650
      Width           =   9015
      Begin MSFlexGridLib.MSFlexGrid grdSelecao 
         Height          =   3135
         Left            =   150
         TabIndex        =   7
         Top             =   300
         Width           =   8775
         _ExtentX        =   15478
         _ExtentY        =   5530
         _Version        =   393216
         Cols            =   17
         FixedCols       =   0
         HighLight       =   2
         SelectionMode   =   1
         AllowUserResizing=   1
         FormatString    =   $"frmSelecao.frx":0000
      End
   End
   Begin VB.CommandButton cmdSair 
      Caption         =   "&Sair"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   435
      Left            =   7710
      TabIndex        =   8
      Top             =   5400
      Width           =   1365
   End
   Begin VB.Frame Frame1 
      Caption         =   " Crit�rios de Pesquisa "
      Height          =   1515
      Left            =   60
      TabIndex        =   0
      Top             =   60
      Width           =   9015
      Begin VB.CommandButton cmdPesquisar 
         Caption         =   "Pesquisar"
         Default         =   -1  'True
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   7560
         TabIndex        =   6
         Top             =   960
         Width           =   1305
      End
      Begin VB.ComboBox cboTipoSelecao 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         ItemData        =   "frmSelecao.frx":0104
         Left            =   180
         List            =   "frmSelecao.frx":0106
         Sorted          =   -1  'True
         Style           =   2  'Dropdown List
         TabIndex        =   1
         Top             =   480
         Width           =   1515
      End
      Begin VB.TextBox txtArgumento 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   7470
         MaxLength       =   9
         TabIndex        =   5
         Top             =   480
         Width           =   1395
      End
      Begin VB.ComboBox CboSucursal 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   4410
         Style           =   2  'Dropdown List
         TabIndex        =   3
         Top             =   480
         Width           =   1575
      End
      Begin VB.ComboBox CboSeguradora 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         ItemData        =   "frmSelecao.frx":0108
         Left            =   1800
         List            =   "frmSelecao.frx":010A
         Style           =   2  'Dropdown List
         TabIndex        =   2
         Top             =   480
         Width           =   2505
      End
      Begin VB.ComboBox CboRamo 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   6090
         Style           =   2  'Dropdown List
         TabIndex        =   4
         Top             =   480
         Width           =   1275
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "Conte�do :"
         Height          =   195
         Left            =   7500
         TabIndex        =   13
         Top             =   240
         Width           =   780
      End
      Begin VB.Label Label1 
         Caption         =   "Tipo de Sele��o :"
         Height          =   255
         Left            =   180
         TabIndex        =   12
         Top             =   240
         Width           =   1425
      End
      Begin VB.Label lblSucursal 
         Caption         =   "Sucursal :"
         Height          =   255
         Left            =   4440
         TabIndex        =   11
         Top             =   240
         Width           =   1575
      End
      Begin VB.Label lblRamo 
         Caption         =   "Ramo :"
         Height          =   255
         Left            =   6120
         TabIndex        =   10
         Top             =   240
         Width           =   855
      End
      Begin VB.Label lblSeguradora 
         Caption         =   "Seguradora Lider :"
         Height          =   255
         Left            =   1830
         TabIndex        =   9
         Top             =   240
         Visible         =   0   'False
         Width           =   1575
      End
   End
   Begin MSComctlLib.StatusBar StbRodape 
      Align           =   2  'Align Bottom
      Height          =   315
      Left            =   0
      TabIndex        =   14
      Top             =   5925
      Width           =   9120
      _ExtentX        =   16087
      _ExtentY        =   556
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   3
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   8997
            MinWidth        =   8997
            Text            =   "Mensagem"
            TextSave        =   "Mensagem"
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   6
            Alignment       =   1
            AutoSize        =   2
            Object.Width           =   1535
            MinWidth        =   88
            Text            =   "Data do sistema"
            TextSave        =   "24/5/2020"
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            AutoSize        =   2
            Object.Width           =   1111
            MinWidth        =   88
            Text            =   "Usu�rio"
            TextSave        =   "Usu�rio"
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "frmSelecao"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public glLinhaSel As Long
Public glLinhaAnterior As Long
Public gl_Ambiente_Original

Private Sub CboSeguradora_Click()

    If CboSeguradora.ListIndex <> -1 Then
        Call MontarComboSucursal
    End If

End Sub

Private Sub cmdPesquisar_Click()

    Call Pesquisar

End Sub

Private Sub cmdSair_Click()

    Call FinalizarAplicacao

End Sub

Private Sub Form_Load()

    On Error GoTo TrataErro

    Call CentraFrm(Me)

    'Verificando permiss�o de acesso � aplica��o''''''''''''''''''''''''''''''''''''''''''

    'Descomentar para liberar
    If Not Trata_Parametros(Command) Then
      ' Call FinalizarAplicacao
    End If

    '�REA DE TESTES ---------------------
    'Teste - Comentar para liberar      '
    cUserName = "NOVA"
    glAmbiente_id = 3
    ' ------------------------------------
    gl_Ambiente_Original = glAmbiente_id
    '-------------------------------------------------------------------'

    Call ObterDataSistema(gsSIGLASISTEMA)

    grdSelecao.ColWidth(16) = 0
    '---------------------------------------------------------------------------------'

    'Configurando interface ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

    Me.Caption = "SEGP0777 - Endosso por Iniciativa da Seguradora"

    StbRodape.Panels(1).Text = "Selecione o Tipo de Sele��o e digite os Argumentos de Pesquisa... "

    Call MontarComboTipoSelecao
    Call MontarComboSeguradora
    Call MontarComboSucursal
    Call MontarComboRamo

    grdSelecao.Rows = 1

    Exit Sub

TrataErro:

    Call TratarErro("Form_Load", Me.name)
    Call FinalizarAplicacao

End Sub


''Modifica��o Realizada em 03/02/2010 - Confitec (Rodrigo Souza)
''Descri��o: Inclus�o de fun��o de busca de par�metro de consulta em BD's m�ltiplos.
'Public Sub ObterParamBDsMultiplos(ByVal sSiglaSistema As String)
'
'Dim objDadosBasicos As Object
'Dim rs As Recordset
'
'    Set objDadosBasicos = CreateObject("SEGL0026.cls00126")
'    Set rs = objDadosBasicos.ObterParamBDsMultiplos(sSiglaSistema, App.Title, App.FileDescription, glAmbiente_id)
'
'    ParamBDsMultiplos = IIf(IsNull(rs("trata_abs")), "", Trim(rs("trata_abs")))
'
'    Set objDadosBasicos = Nothing
'    Set rs = Nothing
'
'End Sub
''------------------------------------------------------------------------------------'

Private Sub MontarComboTipoSelecao()

    On Error GoTo TrataErro

    cboTipoSelecao.AddItem "N� da Ap�lice"
    cboTipoSelecao.AddItem "N� da Proposta"
    cboTipoSelecao.AddItem "N� da Proposta BB"
    cboTipoSelecao.AddItem "N� de Ordem"
    cboTipoSelecao.ListIndex = 0

    Exit Sub

TrataErro:
    Call TratarErro("MontarComboTipoSelecao", Me.name)
    Call FinalizarAplicacao

End Sub

Private Sub cboTipoSelecao_Click()

    Call SelecionarDadosPesquisa

End Sub

Private Sub SelecionarDadosPesquisa()

    Dim iCont As Integer

    On Error GoTo TrataErro

    CboSeguradora.ListIndex = -1
    CboSucursal.ListIndex = -1
    CboRamo.ListIndex = -1
    txtArgumento.Text = ""

    'Luciana - 31/01/2005
    grdSelecao.ColWidth(14) = 0
    grdSelecao.ColWidth(15) = 0

    If cboTipoSelecao.Text = "N� da Ap�lice" Then

        lblSeguradora.Visible = True
        CboSeguradora.Visible = True
        lblSucursal.Visible = True
        CboSucursal.Visible = True
        lblRamo.Visible = True
        CboRamo.Visible = True

        Call MontarComboSeguradora
        Call MontarComboSucursal
        Call MontarComboRamo

        grdSelecao.ColWidth(0) = 1380
        grdSelecao.ColWidth(1) = 0
        grdSelecao.ColWidth(2) = 0

    ElseIf cboTipoSelecao.Text = "N� da Proposta" Then

        lblSeguradora.Visible = False
        CboSeguradora.Visible = False
        lblSucursal.Visible = False
        CboSucursal.Visible = False
        lblRamo.Visible = False
        CboRamo.Visible = False

        grdSelecao.ColWidth(0) = 0
        grdSelecao.ColWidth(1) = 0
        grdSelecao.ColWidth(2) = 0

    ElseIf cboTipoSelecao.Text = "N� da Proposta BB" Then

        lblSeguradora.Visible = False
        CboSeguradora.Visible = False
        lblSucursal.Visible = False
        CboSucursal.Visible = False
        lblRamo.Visible = False
        CboRamo.Visible = False

        grdSelecao.ColWidth(0) = 0
        grdSelecao.ColWidth(1) = 1245
        grdSelecao.ColWidth(2) = 0

    ElseIf cboTipoSelecao.Text = "N� de Ordem" Then

        lblSeguradora.Visible = True
        CboSeguradora.Visible = True
        CboSeguradora.ListIndex = -1
        lblSucursal.Visible = False
        CboSucursal.Visible = False
        lblRamo.Visible = False
        CboRamo.Visible = False

        Call MontarComboSeguradora

        grdSelecao.ColWidth(0) = 0
        grdSelecao.ColWidth(1) = 0
        grdSelecao.ColWidth(2) = 960

    End If

    StbRodape.Panels(1).Text = "Selecione Tipo de Sele��o e digite os Argumentos de Pesquisa !"

    Exit Sub

TrataErro:

    Call TratarErro("SelecionarDadosPesquisa", Me.name)
    Call FinalizarAplicacao

End Sub

Sub MontarComboSeguradora()

    Dim lSeguradoraId As Long

    Dim oDadosBasicos As Object
    Dim rs As Recordset
    Dim iCont As Integer

    On Error GoTo TrataErro

    CboSeguradora.Clear

    MousePointer = vbHourglass

    Set oDadosBasicos = CreateObject("SEGL0022.cls00117")

    lSeguradoraId = CLng(oDadosBasicos.ConsultarParametro(gsSIGLASISTEMA, _
                                                          App.Title, _
                                                          App.FileDescription, _
                                                          glAmbiente_id, _
                                                          "COD_ALIANCA"))

    Set rs = oDadosBasicos.ConsultarSeguradora(gsSIGLASISTEMA, _
                                               App.Title, _
                                               App.FileDescription, _
                                               glAmbiente_id)

    Set oDadosBasicos = Nothing

    If Not rs.EOF Then
        While Not rs.EOF
            CboSeguradora.AddItem Format(rs("seguradora_cod_susep"), "0000") & " - " & Trim(rs("nome"))
            CboSeguradora.ItemData(CboSeguradora.NewIndex) = rs("seguradora_cod_susep")

            rs.MoveNext
        Wend
    End If

    Set rs = Nothing

    For iCont = 0 To CboSeguradora.ListCount - 1
        If CboSeguradora.ItemData(iCont) = lSeguradoraId Then
            CboSeguradora.ListIndex = iCont
            CboSeguradora.Locked = True
        End If
    Next

    MousePointer = vbDefault

    Exit Sub

TrataErro:

    Call TratarErro("MontarComboSeguradora", Me.name)
    Call FinalizarAplicacao

End Sub

Private Sub MontarComboSucursal()

    Dim oDadosBasicos As Object
    Dim rs As Recordset

    On Error GoTo TrataErro

    CboSucursal.Clear

    MousePointer = vbHourglass

    Set oDadosBasicos = CreateObject("SEGL0022.cls00117")

    Set rs = oDadosBasicos.ConsultarSucursal(gsSIGLASISTEMA, _
                                             App.Title, _
                                             App.FileDescription, _
                                             glAmbiente_id, _
                                             CboSeguradora.ItemData(CboSeguradora.ListIndex))

    Set oDadosBasicos = Nothing

    If Not rs.EOF Then
        While Not rs.EOF
            CboSucursal.AddItem Format(rs("sucursal_seguradora_id"), "000") & " - " & Trim(rs("Nome"))
            CboSucursal.ItemData(CboSucursal.NewIndex) = rs("sucursal_seguradora_id")

            rs.MoveNext
        Wend

    End If

    Set rs = Nothing

    MousePointer = vbDefault

    Exit Sub

TrataErro:

    Call TratarErro("MontarComboSucursal", Me.name)
    Call FinalizarAplicacao

End Sub

Sub MontarComboRamo()

    Dim oDadosBasicos As Object
    Dim rs As Recordset

    On Error GoTo TrataErro

    MousePointer = vbHourglass

    Set oDadosBasicos = CreateObject("SEGL0022.cls00117")

    Set rs = oDadosBasicos.ConsultarRamo(gsSIGLASISTEMA, _
                                         App.Title, _
                                         App.FileDescription, _
                                         glAmbiente_id)

    Set oDadosBasicos = Nothing
    CboRamo.Clear
    If Not rs.EOF Then
        While Not rs.EOF
            CboRamo.AddItem rs("ramo_id") & " - " & Trim(rs("Nome"))
            CboRamo.ItemData(CboRamo.NewIndex) = rs("ramo_id")

            rs.MoveNext
        Wend
    End If

    Set rs = Nothing

    MousePointer = vbDefault

    Exit Sub

TrataErro:

    Call TratarErro("MontarComboRamo", Me.name)
    Call FinalizarAplicacao

End Sub

Private Sub Form_Resize()

    On Error GoTo TrataErro

    Call InicializarRodape

    Exit Sub

TrataErro:

    Call TratarErro("Form_Resize", Me.name)
    Call FinalizarAplicacao

End Sub

Private Sub Pesquisar()

    Dim oApolice As Object
    Dim rs As Recordset

    Dim lPropostaId As Long
    Dim lApoliceId As Long
    Dim lSucursalId As Long
    Dim lCodSusep As Long
    Dim LramoId As Long
    Dim lPropostaBB As Long
    Dim lNumOrdem As Long

    On Error GoTo TrataErro

    'Modifica��o realizada em 17/02/2010 - Confitec (Rodrigo Souza)
    'Descri��o: Recupera a vari�vel original de ambiente.
    glAmbiente_id = gl_Ambiente_Original
    '--------------------------------------------------------------'

    MousePointer = vbHourglass

    'Validando Dados '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

    If (CboSeguradora.Visible) And (CboSeguradora.ListIndex = -1) Then
        mensagem_erro 3, "Seguradora Lider"
        CboSeguradora.SetFocus
        MousePointer = vbDefault
        Exit Sub
    End If

    If CboSucursal.Visible And CboSucursal.ListIndex = -1 Then
        mensagem_erro 3, "Sucursal Seguradora"
        CboSucursal.SetFocus
        MousePointer = vbDefault
        Exit Sub
    End If

    If (CboRamo.Visible) And (CboRamo.ListIndex = -1) Then
        mensagem_erro 3, "Ramo"
        CboRamo.SetFocus
        MousePointer = vbDefault
        Exit Sub
    End If

    If Not IsNumeric(txtArgumento.Text) Then
        mensagem_erro 3, "N� da Proposta"
        txtArgumento.SelStart = 0
        txtArgumento.SelLength = Len(txtArgumento)
        txtArgumento.SetFocus
        MousePointer = vbDefault
        Exit Sub
    End If

    If (Not IsNumeric(txtArgumento.Text)) Or (Trim(txtArgumento.Text) = "") Then
        mensagem_erro 3, "Conte�do"
        txtArgumento.SelStart = 0
        txtArgumento.SelLength = Len(txtArgumento)
        txtArgumento.SetFocus
        MousePointer = vbDefault
        Exit Sub
    End If



    'Modifica��o realizada em 10/02/2010 -- Rodrigo Souza
    'Descri��o: Recebe o par�metro que indica se dois bancos ser�o utilizados.
    'Descri��o: Inclus�o de fun��o de busca de par�metro de consulta em BD's m�ltiplos.
    Call ObterTratamentoAbs(gsSIGLASISTEMA)
    ' ParamBDsMultiplos = bUsarConexaoABS
    '--------------------------------------------------------------------------

    Set oApolice = CreateObject("SEGL0026.cls00126")

    Select Case cboTipoSelecao.Text

    Case "N� da Ap�lice"

        lApoliceId = Trim(txtArgumento.Text)
        lSucursalId = CboSucursal.ItemData(CboSucursal.ListIndex)
        lCodSusep = CboSeguradora.ItemData(CboSeguradora.ListIndex)
        LramoId = CboRamo.ItemData(CboRamo.ListIndex)

        Set rs = oApolice.ConsultarDados(gsSIGLASISTEMA, _
                                         App.Title, _
                                         App.FileDescription, _
                                         glAmbiente_id, _
               , _
                                         lApoliceId, _
                                         lSucursalId, _
                                         lCodSusep, _
                                         LramoId, _
               , _
             , _
                                         bUsarConexaoABS, _
                                         glAmbiente_id_seg2)    '<--- 'Modifica��o Realizada em 03/02/2010 - Confitec (Rodrigo Souza)



    Case "N� da Proposta"

        lPropostaId = Trim(txtArgumento.Text)

        Set rs = oApolice.ConsultarDados(gsSIGLASISTEMA, _
                                         App.Title, _
                                         App.FileDescription, _
                                         glAmbiente_id, _
                                         lPropostaId, _
               , _
             , _
           , _
         , _
       , _
     , _
                                         bUsarConexaoABS, _
                                         glAmbiente_id_seg2)    '<--- 'Modifica��o Realizada em 03/02/2010 - Confitec (Rodrigo Souza)

    Case "N� da Proposta BB"

        lPropostaBB = Trim(txtArgumento.Text)

        Set rs = oApolice.ConsultarDados(gsSIGLASISTEMA, _
                                         App.Title, _
                                         App.FileDescription, _
                                         glAmbiente_id, _
               , _
             , _
           , _
         , _
       , _
                                         lPropostaBB, _
               , _
                                         bUsarConexaoABS, _
                                         glAmbiente_id_seg2)    '<--- 'Modifica��o Realizada em 03/02/2010 - Confitec (Rodrigo Souza)

    Case "N� de Ordem"

        lNumOrdem = Trim(txtArgumento.Text)
        lCodSusep = CboSeguradora.ItemData(CboSeguradora.ListIndex)

        Set rs = oApolice.ConsultarDados(gsSIGLASISTEMA, _
                                         App.Title, _
                                         App.FileDescription, _
                                         glAmbiente_id, _
               , _
             , _
           , _
         , _
       , _
     , _
                                         lNumOrdem, _
                                         bUsarConexaoABS, _
                                         glAmbiente_id_seg2)    '<--- 'Modifica��o Realizada em 03/02/2010 - Confitec (Rodrigo Souza)

    End Select

    Set oApolice = Nothing

    If Not rs.EOF Then

        grdSelecao.Rows = 1
        grdSelecao.ColWidth(8) = 0
        grdSelecao.ColWidth(9) = 0
        grdSelecao.ColWidth(10) = 0
        grdSelecao.ColWidth(11) = 0
        grdSelecao.ColWidth(12) = 0
        grdSelecao.ColWidth(13) = 0

        While Not rs.EOF

            grdSelecao.AddItem Format(rs("apolice_id"), "000000000") & vbTab & _
                               rs("proposta_bb") & vbTab & _
                               rs("num_ordem") & vbTab & _
                               rs("proposta_id") & vbTab & _
                               rs("produto_id") & vbTab & _
                               rs("nome") & vbTab & _
                               Format(rs("dt_contratacao"), "dd/mm/yyyy") & vbTab & _
                               rs("descr_situacao") & vbTab & _
                               rs("prop_cliente_id") & vbTab & _
                               rs("ramo_id") & vbTab & _
                               rs("nome_ramo") & vbTab & _
                               rs("nome_produto") & vbTab & _
                               rs("subramo_id") & vbTab & _
                               rs("nome_subramo") & vbTab & _
                               rs("seguro_moeda_id") & vbTab & _
                               rs("premio_moeda_id") & vbTab & _
                               rs("Empresa")

            'Modifica��o Realizada em 04/02/2010 - Confitec (Rodrigo Souza)
            'Descri��o: Inclus�o do campo (BD), par�metro que direciona a consulta para um servidor/banco espec�fico.
            '--> rs("empresa")
            '----------------------------------------------------------------------------------------------'

            'Luciana - 31/01/2005
            'Acrescentei rs("premio_moeda_id") e rs("seguro_moeda_id")

            rs.MoveNext

        Wend

        StbRodape.Panels(1).Text = "Clique 2 vezes na Ap�lice desejada para mais informa��es"

    Else
        StbRodape.Panels(1).Text = ""
        Call MsgBox("Nenhuma proposta foi encontrada !")
        grdSelecao.Rows = 1
    End If

    MousePointer = vbDefault

    If grdSelecao.Rows = 2 Then
        frmEndosso.Show
        Me.Hide
    End If

    Exit Sub

TrataErro:

    Call TratarErro("Pesquisar", Me.name)
    Call FinalizarAplicacao

End Sub

Private Sub grdSelecao_Click()

    Dim i As Integer

    On Error GoTo Trata_Erro

    '# guarda a linha selecionada
    glLinhaSel = grdSelecao.RowSel

    '# restaura a linha anterior
    For i = (grdSelecao.Cols - 1) To 0 Step -1
        If glLinhaAnterior <> 0 Then
            grdSelecao.Row = glLinhaAnterior
            grdSelecao.Col = i
            grdSelecao.CellForeColor = vbBlack
        End If
    Next

    '# marca a linha selecionada
    For i = (grdSelecao.Cols - 1) To 0 Step -1
        grdSelecao.Row = glLinhaSel
        grdSelecao.Col = i
        grdSelecao.CellForeColor = vbBlue
        glLinhaAnterior = glLinhaSel
    Next

    Exit Sub

Trata_Erro:

    Call TratarErro("grdselecao_Click", Me.name)
    Call FinalizarAplicacao

End Sub

Private Sub grdSelecao_DblClick()

'HOLIVEIRA: Verifica se n�o existe linha no grid DT: 26/02/2007
    If grdSelecao.Rows = 1 Then Exit Sub

    frmEndosso.Show
    Me.Hide

End Sub

