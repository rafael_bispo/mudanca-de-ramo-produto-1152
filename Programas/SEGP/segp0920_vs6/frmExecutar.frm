VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.2#0"; "MSCOMCTL.OCX"
Begin VB.Form frmExecutar 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "SEGP0920 - Emiss�o de Ap�lice ALS"
   ClientHeight    =   3930
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6870
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3930
   ScaleWidth      =   6870
   StartUpPosition =   3  'Windows Default
   Begin VB.Frame fraResultados 
      Caption         =   "  Resultado do Processamento  "
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2940
      Left            =   120
      TabIndex        =   3
      Top             =   120
      Width           =   6645
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Certificados com erro:"
         Height          =   195
         Index           =   1
         Left            =   2010
         TabIndex        =   13
         Top             =   945
         Width           =   1530
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Emiss�es com erro:"
         Height          =   195
         Index           =   2
         Left            =   2175
         TabIndex        =   12
         Top             =   1410
         Width           =   1365
      End
      Begin VB.Label txtErros 
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   3630
         TabIndex        =   11
         Top             =   1350
         Width           =   2355
      End
      Begin VB.Label txtCertificado 
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   3630
         TabIndex        =   10
         Top             =   870
         Width           =   2355
      End
      Begin VB.Label txtEmitidas 
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   3630
         TabIndex        =   9
         Top             =   420
         Width           =   2355
      End
      Begin VB.Label Label3 
         Caption         =   "Fim do processamento:"
         Height          =   255
         Left            =   690
         TabIndex        =   8
         Top             =   2460
         Width           =   1665
      End
      Begin VB.Label Label2 
         Caption         =   "In�cio do processamento:"
         Height          =   255
         Left            =   540
         TabIndex        =   7
         Top             =   2040
         Width           =   1815
      End
      Begin VB.Label lblFim 
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   2430
         TabIndex        =   6
         Top             =   2430
         Width           =   3555
      End
      Begin VB.Label lblInicio 
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   2430
         TabIndex        =   5
         Top             =   2010
         Width           =   3555
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Ap�lices emitidas:"
         Height          =   195
         Index           =   0
         Left            =   2280
         TabIndex        =   4
         Top             =   480
         Width           =   1260
      End
   End
   Begin VB.CommandButton cmdSair 
      Caption         =   "Sair"
      Height          =   400
      Left            =   5490
      TabIndex        =   1
      Top             =   3090
      Width           =   1275
   End
   Begin VB.CommandButton cmdAtivar 
      Caption         =   "Ativar"
      Default         =   -1  'True
      Height          =   400
      Left            =   4080
      TabIndex        =   0
      Top             =   3090
      Width           =   1275
   End
   Begin MSComctlLib.StatusBar StbRodape 
      Align           =   2  'Align Bottom
      Height          =   345
      Left            =   0
      TabIndex        =   2
      Top             =   3585
      Width           =   6870
      _ExtentX        =   12118
      _ExtentY        =   609
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   3
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   8290
            MinWidth        =   8290
            Text            =   "Mensagem"
            TextSave        =   "Mensagem"
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            AutoSize        =   2
            Object.Width           =   2170
            MinWidth        =   1764
            Text            =   "Data do sistema"
            TextSave        =   "Data do sistema"
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            AutoSize        =   2
            Object.Width           =   2117
            MinWidth        =   2117
            Text            =   "Usu�rio"
            TextSave        =   "Usu�rio"
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "frmExecutar"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Const TIPO_AVALIACAO_ACEITE_CONTRATO_SEGURO = 1392
Const ATIVIDADE_EM_USO = 4
Const TIPO_ATIVIDADE = 8
Const TIPO_WORKFLOW = 73
Const TIPO_FORM_ID = "NULL"


Dim sUltimoProcessamento As String
Dim lWorkFlowId As String


Sub EmitirApoliceALS()
                            
Dim rsPropostas As Recordset
Dim oProposta As Object
Dim lErros As Long
Dim lEmitidas As Long
Dim lCertificados As Long

On Error GoTo Trata_Erro
    
'Tratamento para o Scheduler ''''''''''''''''''''''''''''''
'InicializaParametrosExecucaoBatch Me

'Atualizando interface ''''''''''''''''''''''''''''''''''''
MousePointer = vbHourglass
cmdAtivar.Enabled = False

Set oProposta = CreateObject("SEGL0281.cls00477")

oProposta.CTMArquivoLog = CTMArquivoLog

Call oProposta.ProcessarEmissaoApoliceALS(App.ProductName, _
                                          App.Title, _
                                          App.FileDescription, _
                                          glAmbiente_id, _
                                          TIPO_AVALIACAO_ACEITE_CONTRATO_SEGURO, _
                                          Data_Sistema, _
                                          cUserName, _
                                          lEmitidas, _
                                          lCertificados, _
                                          lErros)
 
Set oProposta = Nothing
 
Set oALS = Nothing

txtEmitidas.Caption = lEmitidas
txtErros.Caption = lErros
txtCertificado.Caption = lCertificados

goProducao.AdicionaLog 1, lEmitidas, 1
goProducao.AdicionaLog 2, lCertificados, 1
goProducao.AdicionaLog 3, lErros, 1

'grava log arquivo CTM
If CTM = True Then
    Call GravaLogCTM("OK - ", lEmitidas, "", "")
    Call GravaLogCTM("OK - ", lCertificados, "", "")
    Call GravaLogCTM("OK - ", lErros, "", "")
End If

               
If lErros > 0 Then
    Call Err.Raise(1, , "Propostas com erro: " & lErros)
End If

'Finalizando agenda''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    'Pula de for executado pelo Control-M
    If CTM = False Then
        Call goProducao.finaliza
    End If


'Atualizando interface ''''''''''''''''''''''''''''''''''''
MousePointer = 0
cmdAtivar.Enabled = True
    
Exit Sub
                 
Trata_Erro:

    Call TratarErro("Processar", Me.name)
    Call FinalizarAplicacao

End Sub



Sub EmitirApolice()
                            
 Dim rsPropostas As Recordset
 Dim oProposta As Object
 Dim lErros As Long
 Dim lEmitidas As Long

On Error GoTo Trata_Erro
    
'Tratamento para o Scheduler ''''''''''''''''''''''''''''''
InicializaParametrosExecucaoBatch Me

'Atualizando interface ''''''''''''''''''''''''''''''''''''
MousePointer = vbHourglass
cmdAtivar.Enabled = False

Set oProposta = CreateObject("SGSL0001.cls00213")

Call oProposta.ProcessarEmissaoApolice(App.ProductName, _
                                       App.Title, _
                                       App.FileDescription, _
                                       glAmbiente_id, _
                                       TIPO_AVALIACAO_ACEITE_CONTRATO_SEGURO, _
                                       Data_Sistema, _
                                       cUserName, _
                                       lEmitidas, _
                                       lErros)

 
Set oProposta = Nothing
 
Set oALS = Nothing
    
txtProcessadas = lEmitidas
txtErros = lErros

goProducao.AdicionaLog 1, lEmitidas, 1
goProducao.AdicionaLog 2, lErros, 1
               
'grava log arquivo CTM
If CTM = True Then
    Call GravaLogCTM("OK - ", lEmitidas, "", "")
    Call GravaLogCTM("OK - ", lErros, "", "")
End If
               
               
If lErros > 0 Then
  
    Call Err.Raise(1, , "Propostas com erro: " & lErros)
    
End If

'Finalizando agenda''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'Pula de for executado pelo Control-M
If CTM = False Then
    Call goProducao.finaliza
End If


'Atualizando interface ''''''''''''''''''''''''''''''''''''
MousePointer = 0
cmdAtivar.Enabled = True
    
Exit Sub
                 
Trata_Erro:

    Call TratarErro("Processar", Me.name)
    Call FinalizarAplicacao

End Sub
Function ObterPropostaId(ByVal lWfId As String) As Long

    Dim oEmissaoALS As Object

    On Error GoTo Erro

    Set oEmissaoALS = CreateObject("SGSL0001.cls00213")

    ObterPropostaId = oEmissaoALS.ObterPropostaId(gsSIGLASISTEMA, _
                                                  App.Title, _
                                                  App.FileDescription, _
                                                  glAmbiente_id, _
                                                  lWfId)

    Set oEmissaoALS = Nothing
    
    Exit Function
    
Erro:
    Call TratarErro("ObterPropostaId", Me.name)
    Call FinalizarAplicacao
End Function

Private Sub tmrConsultaEntrada_Timer()
Dim Tempo As String
Dim Segundos As Integer, Minutos As Integer, Horas As Integer
    
    Tempo = sUltimoProcessamento
    Segundos = Second(Tempo)
    Minutos = Minute(Tempo)
    Horas = Hour(Tempo)

    Segundos = Segundos - 1

    If Segundos = -1 Then
        Segundos = 59
        Minutos = Minutos - 1
        If Minutos = -1 Then
            Horas = Horas - 1
        End If
    End If
    sUltimoProcessamento = Format(Horas & ":" & Minutos & ":" & Segundos, "hh:mm:ss")

End Sub

Sub AtualizarTela(ByVal lEmitidas As Long, ByVal lNEmitidas As Long)
    
    lEmitidas = lEmitidas + Val(txtEmitidas)
    lNEmitidas = lNEmitidas + Val(txtNEmitidas)
    
    txtEmitidas = lEmitidas
    txtNEmitidas = lNEmitidas
    
End Sub


Private Sub cmdAtivar_Click()

    lblInicio.Caption = Now

    Call EmitirApoliceALS
    
    lblFim.Caption = Now

    MensagemBatch "Processamento finalizou com sucesso!", vbOKOnly + vbInformation, , False

End Sub


Private Sub cmdSair_Click()
    
    Unload Me
End Sub


Private Sub Form_Load()
'Verifica se foi executado pelo Control-M
CTM = Verifica_Origem_ControlM(Command)

    'Pula se for executado pelo control-m
    If CTM = False Then
      'Verificando permiss�o de acesso � aplica��o'''''''''''''''''''''''''''''''''
      If Not Trata_Parametros(Command) Then
        ' Call FinalizarAplicacao
      End If
    End If

  'Retirar antes da libera��o '''''''''''''''''''''''''''''''''''''''''''''''''
cUserName = "producao3"
glAmbiente_id = 3

  'Obtendo a data operacional '''''''''''''''''''''''''''''''''''''''''''''''''
  Call ObterDataSistema("SEGBR")

  'Configurando interface''''''''''''''''''''''''''''''''''''''''''''''''''''''
  Call CentraFrm(Me)
  
  
  'Atualizando Interface ''''''''''''''''''''''''''''''''''''''''''''''''''''''
  Me.Caption = "SEGP0920 - Emiss�o de Ap�lice ALS"
  StbRodape.Panels(1).Text = "Selecione o bot�o Ativar para iniciar o processamento"
  StbRodape.Panels(2) = Data_Sistema
  StbRodape.Panels(3) = cUserName
  
  ' Iniciando o processo automaticamente caso seja uma chamada do Scheduler '''''''''''''''
  If Obtem_agenda_diaria_id(Command) > 0 Or CTM = True Then
    
    Me.Show
    Call cmdAtivar_Click
    FinalizarAplicacao
    
  End If
  
  Exit Sub

Trata_Erro:

    Call TratarErro("Form_Load", Me.name)
  
End Sub


Private Sub Form_Unload(Cancel As Integer)
Call FinalizarAplicacao
End Sub


 Private Sub Timer_Timer()
    Call tmrConsultaEntrada_Timer
    If Minute(sUltimoProcessamento) = 0 And Second(sUltimoProcessamento) = 0 Then
        Call EmitirApolice
    End If
End Sub


