Imports System.Configuration
Imports IBM.WMQ
Imports Alianca.Seguranca.BancoDados
Imports System.Reflection
Imports System.Data.SqlClient
Imports System.Threading
Imports SMQL0001

Public Class Service1
    Dim configs As Dictionary(Of String, String)
    Dim doneList As New List(Of ManualResetEvent)
    Dim tmr As System.Timers.Timer

#Region "Construtor"
    Public Sub New()
        InitializeComponent()
        Me.ServiceName = ConfigurationManager.AppSettings.Get("ServiceName")

        TraceFile.Configurar("C:\log_mq\services\Trace", Me.ServiceName, True)
        ErrorFile.Configurar("C:\log_mq\services\Erro", Me.ServiceName, True)
        TraceFile.InserirDivis�o("Servico Iniciado")

        Try
            'Configurando conex�o com o banco
            If Not cCon.configurado Then 'Verificando se existe conex�o com o banco
                If ConfigurationManager.AppSettings.Get("AmbienteBD") = 2 Then
                    cCon.ConfiguraConexao(Assembly.GetExecutingAssembly(), cCon.Ambientes.Produ��o)
                    cCon.ConnectionTimeout = cCon.TimeTimeout.Infinito
                    configs = Helper.GetServicesConfigs(ConfigurationManager.AppSettings("ServiceName"), ConfigurationManager.AppSettings("AmbienteBD"))
                    If configs.Count = 0 Then
                        Throw New Exception("N�o foi possivel recuperar as configura��es do servi�o.")
                    End If
                    TraceFile.Configurar(configs("TraceFilePath"), configs("ServiceName"), configs("Trace"))
                    TraceFile.Save("Ambiente: " + configs("AmbienteBD"))
                    TraceFile.Save("Status: Conectado")
                Else
                    cCon.ConfiguraConexao(Assembly.GetExecutingAssembly(), cCon.Ambientes.Qualidade)
                    cCon.ConnectionTimeout = cCon.TimeTimeout.Infinito
                    configs = Helper.GetServicesConfigs(ConfigurationManager.AppSettings("ServiceName"), ConfigurationManager.AppSettings("AmbienteBD"))
                    If configs.Count = 0 Then
                        Throw New Exception("N�o foi possivel recuperar as configura��es do servi�o.")
                    End If
                    TraceFile.Configurar(configs("TraceFilePath"), configs("ServiceName"), configs("Trace"))
                    TraceFile.Save("Ambiente: " + configs("AmbienteBD"))
                    TraceFile.Save("Status: Conectado")
                End If
            End If

        Catch ex As Exception
            TraceFile.Erro("Falha ao configurar servi�o")
            ErrorFile.Save("Falha ao configurar servi�o")
            ErrorFile.SaveException(ex)
        End Try
        Importacao()
    End Sub
#End Region

#Region "M�todos"

#Region "OnStart"
    Protected Overrides Sub OnStart(ByVal args() As String)
        Try
            TraceFile.InserirDivis�o("Inicio - OnStart")
            TraceFile.Save("Ligando o timer...")
            TraceFile.Save("Tempo: " + configs("Timer"))
            tmr = New System.Timers.Timer(configs("Timer"))
            tmr.Enabled = True
            tmr.Start()
            AddHandler tmr.Elapsed, AddressOf OnTimerEvent

            TraceFile.Save("Timer ligado com sucesso...")
            TraceFile.InserirDivis�o("Fim - OnStart")
        Catch ex As Exception
            TraceFile.Erro("N�o foi possivel iniciar o servi�o.")
            ErrorFile.SaveException(ex)
        End Try
    End Sub
#End Region

#Region "OnStop"
    Protected Overrides Sub OnStop()
        Try
            TraceFile.InserirDivis�o("Inicio - OnStop")
            tmr.Stop()
            TraceFile.Save("Timer parado")
            TraceFile.Save("Servi�o parado")
            TraceFile.InserirDivis�o("Fim - OnStop")
        Catch ex As Exception
            TraceFile.Erro("N�o foi possivel parar o servi�o.")
            ErrorFile.SaveException(ex)
        End Try
    End Sub
#End Region

#Region "OnTimerEvent"
    Public Sub OnTimerEvent(ByVal source As Object, ByVal e As System.Timers.ElapsedEventArgs)
        Try
            TraceFile.InserirDivis�o("Inicio - OnTimerEvent")

            'Configurando conex�o com o banco
            TraceFile.Save("Verificando se existe conexao com o Banco de Dados...")
            Try
                If Not cCon.configurado Then
                    TraceFile.Save("N�o existe conex�o com o banco de dados...conectando...")
                    If ConfigurationManager.AppSettings.Get("AmbienteBD") = 2 Then
                        TraceFile.Save("Ambiente: Produ��o")
                        cCon.ConfiguraConexao(Assembly.GetExecutingAssembly(), cCon.Ambientes.Produ��o)
                        cCon.ConnectionTimeout = cCon.TimeTimeout.Infinito
                        TraceFile.Save("Conex�o estabelecida com sucesso")
                    Else
                        TraceFile.Save("Ambiente: Qualidade")
                        cCon.ConfiguraConexao(Assembly.GetExecutingAssembly(), cCon.Ambientes.Qualidade)
                        cCon.ConnectionTimeout = cCon.TimeTimeout.Infinito
                        TraceFile.Save("Conex�o estabelecida com sucesso")
                    End If
                End If
            Catch ex As Exception
                TraceFile.Erro("N�o foi poss�vel estabelecer a conex�o...")
                ErrorFile.SaveException(ex)
            End Try

            TraceFile.Save("Verificando se existe conex�o com o banco de dados...")
            If cCon.configurado() Then
                TraceFile.Save("Status da Conex�o: True")

                'Verifica se no hor�rio atual � permitido 
                TraceFile.Save("Recuperando par�metros da base...")
                configs = Helper.GetServicesConfigs(ConfigurationManager.AppSettings("ServiceName"), ConfigurationManager.AppSettings("AmbienteBD"))
                TraceFile.Save("Par�metros recuperados")
                TraceFile.Save("Verificando hor�rio de execu��o do servi�o...")
                TraceFile.Save("Inicio: " + Me.configs("TimeStart"))
                TraceFile.Save("Fim: " + Me.configs("TimeEnd"))
                If Helper.ValidaHorario(Me.configs("TimeStart"), Me.configs("TimeEnd")) Then
                    TraceFile.Save("Hor�rio de execu��o permitido")
                    TraceFile.Save("Verificando o servi�o a ser executado...")
                    Dim Servico As Integer = Me.configs("TipoServico")
                    TraceFile.Save("Servi�o do tipo: " + Me.configs("TipoServico"))
                    TraceFile.Save("Iniciando Importa��o da fila de MCI...")

                    Me.LogExecution(1, ServiceName, "N")
                    Importacao()
                    Me.LogExecution(2, ServiceName, "N")
                    TraceFile.Save("Finalizando Importa��o da fila de MCI...")
                Else
                    TraceFile.Save("Hor�rio de execu��o n�o permitido")
                End If
            Else
            TraceFile.Save("Status da Conex�o: False")
            TraceFile.Save("Aguardando pr�xima execu��o para reconectar...")
            End If

            TraceFile.InserirDivis�o("Fim - OnTimerEvent")
        Catch ex As Exception
            TraceFile.Erro("Falha na execu��o do servi�o.")
            ErrorFile.SaveException(ex)
        End Try
    End Sub
#End Region

#Region "Importa��o"
    Public Sub Importacao()
        TraceFile.InserirDivis�o("Inicio - Importacao()")

        'Para o timer para realizar a importa��o
        TraceFile.Save("Parando o timer...")
        Me.tmr.Stop()
        TraceFile.Save("Timer parado...")

        'Criando o objeto mensagem
        Dim m As New Mensagem
        'Criando as structs
        Dim structCodTransacao As New CodTransacao
        Dim structEventoBB As New EventoBB
        Dim structQtdRegistros As New QtdRegistros

        'Setando o valor das structs
        structCodTransacao.SetValues()
        structEventoBB.SetValues()
        structQtdRegistros.SetValues()

        TraceFile.Save("Structs iniciadas com sucesso...")

        'Variaveis para o arquivo de log
        Dim gerarLog As Boolean = True

        gerarLog = Me.configs("GerarLog")
        TraceFile.Save("Gerar log de importa��o: " + Me.configs("GerarLog"))

        TraceFile.Save("Configurando conex�o com o MQ...")
        'CONEXAO COM O MQ

        TraceFile.Save("HostName: " + Me.configs("Hostname"))
        TraceFile.Save("Channel: " + Me.configs("Channel"))
        TraceFile.Save("QueueManager: " + Me.configs("QueueManager"))
        TraceFile.Save("QueueName: " + Me.configs("QueueName"))

        Dim mqObject As New MQ(Me.configs("Hostname"), Me.configs("Channel"))
        Dim qMgr As MQQueueManager
        Dim queue As MQQueue
        Dim mqConectado As Boolean = False

        Try
            'Setando o QueueManager
            TraceFile.Save("Iniciando conex�o...")
            TraceFile.Save("Conectando ao QueueManager...")
            qMgr = New MQQueueManager(configs("QueueManager"))
            TraceFile.Save("QueueManager conectado com sucesso.")
            'Setando as configuracoes da fila que ser� aberta
            Dim openOptions As Integer = MQC.MQOO_INPUT_AS_Q_DEF Xor MQC.MQOO_OUTPUT Xor MQC.MQOO_INQUIRE
            'Setando o nome da fila que deseja ser aberta e abrindo a fila
            TraceFile.Save("Acessando fila...")
            queue = qMgr.AccessQueue(configs("QueueName"), openOptions)
            TraceFile.Save("Fila acessada com sucesso.")
            mqConectado = True
        Catch ex As Exception
            mqConectado = False
            TraceFile.Erro("Falha ao setar conex�o com o MQ")
            ErrorFile.SaveException(ex)
        End Try

        'Se a conex�o com MQ estiver estabelecida
        If mqConectado Then
            TraceFile.Save("Quantidade de msgs na fila: " + queue.CurrentDepth.ToString())

            TraceFile.QuebrarLinha()
            Dim contadorTrace As Integer = 1

            Try
                While queue.CurrentDepth > 0
                    TraceFile.InserirDivis�o("Itera��o: " + contadorTrace.ToString())
                    TraceFile.QuebrarLinha()
                    TraceFile.Save("Iniciando itera��o de importa��o")
                    Try
                        'Recuperando as mensagens do MQ
                        Dim messageMQ As New MQMessage
                        messageMQ = mqObject.GetMessage(qMgr, queue)
                        TraceFile.Save("Mensagem retirada da fila do MQ")

                        Dim msg As String = "" '= messageMQ.ReadString(messageMQ.MessageLength)

                        If (messageMQ IsNot Nothing) Then
                            If (messageMQ.MessageLength <> 0) Then msg = messageMQ.ReadString(messageMQ.MessageLength)
                        End If

                        Dim registro As New Mensagem

                        TraceFile.Save("Verificando se a mensagem possui conteudo...")
                        If String.IsNullOrEmpty(msg) Then
                            TraceFile.Save("Mensagem possui conteudo: False")
                            qMgr.Commit()
                            TraceFile.Save("Realizado o Commit na fila do MQ, mensagem vazia retirada")
                        Else
                            TraceFile.Save("Mensagem possui conteudo: True")
                            TraceFile.Save("Conteudo da mensagem: " + msg)
                            TraceFile.Save("Aplicando o layout na mensagem")
                            If registro.Interpretar(structCodTransacao, structEventoBB, structQtdRegistros, msg, messageMQ.CorrelationId, configs("grupo_evento_als_id")) Then
                                TraceFile.Save("Layout aplicado com sucesso: True")
                                TraceFile.Save("Salvando a mensagem no BD...")
                                If registro.Salvar() Then
                                    TraceFile.Save("Mensagem salva com sucesso: True")
                                    Try
                                        TraceFile.Save("Realizado o Commit na fila do MQ")
                                        qMgr.Commit()
                                        TraceFile.Save("Commit realizado com sucesso")
                                        If gerarLog Then
                                            'Salva o log
                                            TraceFile.Save("Mensagem adicionada no arquivo de log")
                                            m.SalvarLog(msg, configs("CaminhoLog"), configs("Secao"))
                                            TraceFile.Save("Mensagem salva no Log com sucesso")
                                        End If
                                    Catch ex As Exception
                                        TraceFile.Save("Falha ao realizar o Commit da fila do MQ")
                                        ErrorFile.SaveException(ex)
                                    End Try
                                Else
                                    Try
                                        TraceFile.Save("Realizando o backout na fila do MQ")
                                        qMgr.Backout()
                                        TraceFile.Save("Backout realizado com sucesso")
                                    Catch ex As Exception
                                        TraceFile.Save("Falha ao realizar o backout")
                                        ErrorFile.SaveException(ex)
                                    End Try
                                End If
                            Else
                                TraceFile.Save("Layout aplicado com sucesso: False")
                                TraceFile.Save("Inserindo mensagem na fila do MQ novamente")
                                registro.PutMessageInMQ(qMgr, queue, messageMQ)
                                TraceFile.Save("Mensagem inserida na fila com sucesso: True")
                                qMgr.Commit()
                                TraceFile.Save("Realizado o Commit na fila do MQ, mensagem inserida no final da fila")
                            End If
                        End If
                    Catch ex As Exception
                        TraceFile.InserirDivis�o("Erro inesperado no m�todo Importacao()")
                        ErrorFile.SaveException(ex)
                    End Try

                    TraceFile.InserirDivis�o()
                    contadorTrace += 1
                End While
            Catch ex As Exception
                TraceFile.Save("Falha na inst�ncia da fila do MQ")
            End Try

            TraceFile.QuebrarLinha()
            TraceFile.Save("Verificando se existe filas do MQ aberta...")

            'Try
            '    If queue.OpenInputCount > 0 Then
            '        TraceFile.Save("Conex�es abertas: " + queue.OpenInputCount.ToString())
            '        TraceFile.Save("Fechando Queue...")
            '        queue.Close()
            '        TraceFile.Save("Conex�es abertas: " + queue.OpenInputCount.ToString())
            '    End If
            'Catch ex As Exception
            '    TraceFile.Save("Falha ao tentar fechar conex�o com a Queue")
            '    ErrorFile.SaveException(ex)
            'End Try

            Try
                If qMgr.IsConnected Then
                    TraceFile.Save("Conex�o com o QueueManager aberto: " + qMgr.IsConnected.ToString())
                    TraceFile.Save("Desconectando QueueManager...")
                    qMgr.Disconnect()
                    TraceFile.Save("Conex�o com o QueueManager aberto: " + qMgr.IsConnected.ToString())
                End If
            Catch ex As Exception
                TraceFile.Save("Falha ao tentar fechar conex�o com o QueueManager")
                ErrorFile.SaveException(ex)
            End Try

            TraceFile.Save("Executando m�todo InvalidaOpsInconsistentes()")
            If Helper.InvalidaOpsInconsistentes(Integer.Parse(Me.configs("grupo_evento_als_id"))) Then
                TraceFile.Save("InvalidaOpsInconsistentes executado com sucesso: True")
            Else
                TraceFile.Save("InvalidaOpsInconsistentes executado com sucesso: False")
            End If
        End If
        'Inicia o timer novamente
        TraceFile.InserirDivis�o("Fim - Importacao()")
        Me.tmr.Start()
    End Sub
#End Region


#Region "LogExecution"
    Public Sub LogExecution(ByVal tpLog As Integer, ByVal serviceName As String, ByVal abs As String)
        Try

            Dim params As SqlParameter() = New SqlParameter() { _
                                                                New SqlParameter("@TP_LOG", tpLog), _
                                                                New SqlParameter("@NOME_SERVICO", serviceName), _
                                                                New SqlParameter("@ABS", abs) _
                                                                }
            cCon.InternalExecuteNonQuery(CommandType.StoredProcedure, "INTERFACE_DADOS_DB..SMQS0133_SPU", params)
        Catch ex As Exception
            TraceFile.Save("Classe: Service")
            TraceFile.Save("M�todo: LogExecution")
            TraceFile.Erro("Falha ao salvar log de execucao.")
            ErrorFile.SaveException(ex)
        End Try
    End Sub
#End Region

#End Region

End Class