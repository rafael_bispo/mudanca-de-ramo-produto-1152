﻿Imports IBM.WMQ
Imports SMQL0001

Public Class MQ
    Shared _h As String
    Shared _c As String

#Region "Propriedades"
    Shared Property HostName() As String
        Get
            Return _h
        End Get
        Set(ByVal value As String)
            _h = value
        End Set
    End Property

    Shared Property Channel() As String
        Get
            Return _c
        End Get
        Set(ByVal value As String)
            _c = value
        End Set
    End Property

    Public Sub New(ByVal _hostName As String, ByVal _channel As String)
        MQ.Channel = _channel
        MQ.HostName = _hostName
        MQEnvironment.Hostname = _hostName
        MQEnvironment.Channel = _channel
    End Sub

    Public Sub New()
        MQEnvironment.Hostname = MQ.HostName
        MQEnvironment.Channel = MQ.Channel
    End Sub
#End Region

#Region "Métodos"

#Region "Retrieve Message"
    Public Function GetMessage(ByVal qMgr As MQQueueManager, ByVal queue As MQQueue) As MQMessage
        Dim message As New MQMessage
        Dim gmo As New MQGetMessageOptions
        gmo.Options = MQC.MQPMO_SYNCPOINT

        Try
            'Recuperando a mensagem da fila
            queue.Get(message, gmo)
        Catch e As MQException
            If e.CompCode = 2 Then
                If Not e.Reason = 2033 Then
                    TraceFile.Save("Classe: MQ")
                    TraceFile.Save("Método: GetMessage")
                    TraceFile.Save("Falha na instância da fila do MQ")
                    ErrorFile.SaveException(e)
                End If
            Else
                TraceFile.Save("Classe: MQ")
                TraceFile.Save("Método: GetMessage")
                TraceFile.Save("Falha na instância da fila do MQ")
                ErrorFile.SaveException(e)
            End If
        Catch ex As Exception
            TraceFile.Save("Classe: MQ")
            TraceFile.Save("Método: GetMessage")
            TraceFile.Erro("Falha na instância da fila do MQ")
            ErrorFile.SaveException(ex)
        End Try
        Return message
    End Function
#End Region

#Region "Colocar Mensagem na Fila"
    Public Sub PutMessage(ByVal queue As MQQueue, ByVal qMgr As MQQueueManager, ByVal message As MQMessage)
        Dim pmo As New MQPutMessageOptions
        pmo.Options = MQC.MQPMO_SYNCPOINT

        'Setando as configuracoes da fila que será aberta
        Dim openOptions As Integer = MQC.MQOO_INPUT_AS_Q_DEF Xor MQC.MQOO_OUTPUT Xor MQC.MQOO_INQUIRE

        Try
            'Colocando a mensagem na fila
            queue.Put(message, pmo)
        Catch ex As Exception
            TraceFile.Save("Classe: MQ")
            TraceFile.Save("Método: PutMessage")
            TraceFile.Erro("Falha ao colocar mensagem na fila")
            ErrorFile.SaveException(ex)
        End Try
    End Sub
#End Region

#End Region

End Class
