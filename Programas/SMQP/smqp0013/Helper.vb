Imports System.Reflection
Imports Alianca.Seguranca.BancoDados
Imports System.Data.SqlClient
Imports SMQL0001

Public Class Helper

#Region "Valida Hor�rio"
    Public Shared Function ValidaHorario(ByVal horaInicio As Date, ByVal horaTermino As Date) As Boolean
        Dim horaAtual As Date = TimeOfDay
        If horaAtual > horaInicio And horaAtual < horaTermino Then
            Return True
        Else
            Return False
        End If
    End Function
#End Region

#Region "GetServicesConfigs"
    Public Shared Function GetServicesConfigs(ByVal service_name As String, ByVal ambiente As Integer) As Dictionary(Of String, String)
        Dim configs As New Dictionary(Of String, String)
        Try
            Dim params As SqlParameter() = New SqlParameter() { _
                                                                    New SqlParameter("@SERVICO", service_name), _
                                                                    New SqlParameter("@AMBIENTE", ambiente) _
                                                                }

            Dim reader As SqlDataReader = cCon.ExecuteReader(CommandType.StoredProcedure, "interface_dados_db..SEGS9197_SPS", params)

            While reader.Read
                configs.Add("Trace", reader("Trace"))
                configs.Add("TraceFilePath", reader("TraceFilePath"))
                configs.Add("ServiceName", reader("ServiceName"))
                configs.Add("AmbienteBD", reader("AmbienteBD"))
                configs.Add("Timer", reader("Timer"))
                configs.Add("TimeStart", reader("TimeStart"))
                configs.Add("TimeEnd", reader("TimeEnd"))
                configs.Add("TipoServico", reader("TipoServico"))
                configs.Add("CaminhoLog", reader("CaminhoLog"))
                configs.Add("Secao", reader("Secao"))
                configs.Add("GerarLog", reader("GerarLog"))
                configs.Add("grupo_evento_als_id", reader("grupo_evento_als_id"))
                configs.Add("Hostname", reader("Hostname"))
                configs.Add("Channel", reader("Channel"))
                configs.Add("QueueManager", reader("QueueManager"))
                configs.Add("QueueName", reader("QueueName"))
                configs.Add("MaxWorkerThreads", reader("MaxWorkerThreads"))
                configs.Add("ThreadPool", reader("ThreadPool"))
                configs.Add("QtdRegistrosParaProcessar", reader("QtdRegistrosParaProcessar"))
                configs.Add("Error", reader("Error"))
                configs.Add("ErrorFilePath", reader("ErrorFilePath"))
            End While
            Return configs
        Catch ex As Exception
            TraceFile.Save("Classe: Helper")
            TraceFile.Save("M�todo: GetServicesConfigs")
            TraceFile.Erro("Falha ao recuperar as informa��es sobre o servi�o")
            ErrorFile.SaveException(ex)
            Return Nothing
        End Try
    End Function
#End Region

#Region "InvalidaOpsInconsistentes"
    Public Shared Function InvalidaOpsInconsistentes(ByVal grupo_evento_als_id As Integer) As Boolean

        Try
            Dim params As SqlParameter() = New SqlParameter() {New SqlParameter("@GrupoEventoAlsId", grupo_evento_als_id)}

            cCon.InternalExecuteNonQuery(CommandType.StoredProcedure, "interface_dados_db..InvalidaOPsInconsistentes_spu", params)
            Return True
        Catch ex As Exception
            TraceFile.Save("Classe: Helper")
            TraceFile.Save("M�todo: InvalidaOpsInconsistentes")
            TraceFile.Erro("Falha ao colocar mensagem na fila")
            ErrorFile.SaveException(ex)
            Return False
        End Try
    End Function
#End Region

End Class
