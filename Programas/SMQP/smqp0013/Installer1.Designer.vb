<System.ComponentModel.RunInstaller(True)> Partial Class Installer1
    Inherits System.Configuration.Install.Installer

    'Installer overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Component Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Component Designer
    'It can be modified using the Component Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.ServiceInstaller1 = New System.ServiceProcess.ServiceInstaller
        Me.ServiceProcessInstaller1 = New System.ServiceProcess.ServiceProcessInstaller
        '
        'ServiceInstaller1
        '
        Me.ServiceInstaller1.Description = "Servi�o de importa��o das mensagens do MQ da fila de MCI."
        Me.ServiceInstaller1.DisplayName = "SMQP0013"
        Me.ServiceInstaller1.ServiceName = "SMQP0013"
        '
        'ServiceProcessInstaller1
        '
        Me.ServiceProcessInstaller1.Account = System.ServiceProcess.ServiceAccount.LocalSystem
        Me.ServiceProcessInstaller1.Password = Nothing
        Me.ServiceProcessInstaller1.Username = Nothing
        '
        'Installer1
        '
        Me.Installers.AddRange(New System.Configuration.Install.Installer() {Me.ServiceInstaller1, Me.ServiceProcessInstaller1})

    End Sub
    Friend WithEvents ServiceProcessInstaller1 As System.ServiceProcess.ServiceProcessInstaller
    Private WithEvents ServiceInstaller1 As System.ServiceProcess.ServiceInstaller

End Class
