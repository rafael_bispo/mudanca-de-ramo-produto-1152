Attribute VB_Name = "MOD_MQ_PADRAO"
Option Explicit
Global rdocnTelao      As New rdoConnection
Global rdocnHelp      As New rdoConnection
Global numTelao As Integer
Global SituacaoTelao As Integer

'Barney - 30/12/2003 - In�cio (Declara��o das fun��es e constante _____________________________
Public Const LOCALE_SLANGUAGE = &H2

Declare Function GetLocaleInfo Lib "kernel32" Alias "GetLocaleInfoA" (ByVal Locale As Long, ByVal LCType As Long, ByVal lpLCData As String, ByVal cchData As Long) As Long
Declare Function SetLocaleInfo Lib "kernel32" Alias "SetLocaleInfoA" (ByVal Locale As Long, ByVal LCType As Long, ByVal lpLCData As String) As Boolean
Declare Function GetUserDefaultLCID% Lib "kernel32" ()

Public Function Get_locale(lngLocale As Long) As String ' Retrieve the regional setting
      Dim Symbol As String
      Dim iRet1 As Long
      Dim iRet2 As Long
      Dim lpLCDataVar As String
      Dim pos As Integer
      Dim Locale As Long
      
      Locale = GetUserDefaultLCID()
    
      iRet1 = GetLocaleInfo(Locale, lngLocale, lpLCDataVar, 0)
      Symbol = String$(iRet1, 0)
      
      iRet2 = GetLocaleInfo(Locale, lngLocale, Symbol, iRet1)
      pos = InStr(Symbol, Chr$(0))
      If pos > 0 Then
           Symbol = Left$(Symbol, pos - 1)
           Get_locale = Symbol
      End If

End Function
 

Public Function validaOpcoesRegionais() As Boolean
validaOpcoesRegionais = False
    If (UCase(Get_locale(LOCALE_SLANGUAGE)) <> UCase("Portugu�s (Brasil)")) Then
        If (UCase(Get_locale(LOCALE_SLANGUAGE)) <> UCase("Portuguese (Brazil)")) Then
            MSGERRO = "SMQP0002 - Erro!" & Chr(13) & _
            "O sistema n�o est� operando no padr�o " + UCase("Portugu�s (Brasil).") & _
            vbNewLine & "Por favor, alter�-lo no Painel de Controle. " & Chr(13) & _
            "Fun��o: validaOpcoesRegionais"
            GravarErroArquivo "SMQP0002"
            Exit Function
        End If
    End If
    Dim boleano As Boolean
    boleano = SetLocaleInfo(1040, 2, "")
    validaOpcoesRegionais = True
End Function

Public Sub ColocaNoTelao(servico As Integer, situacao As Integer)
'ativa o processo no telao
 Dim sql As String
    ConexaoTelao
    sql = "EXEC atualiza_status_servico_spu " & situacao & "," & servico
    rdocnTelao.OpenResultset (sql)
    rdocnHelp.OpenResultset (sql)
    
End Sub

Sub ConexaoTelao(Optional TestaBDLiberado As Boolean = False, Optional FechaConexao As Boolean = False)
  
Dim rc As rdoResultset

On Local Error GoTo TrataErro
'TELAO PRINCIPAL
With rdocnTelao
   .Connect = "UID=service_infra;PWD=service_infra;server=SISAB068;driver={SQL Server};database=service_infra_db;"
   .QueryTimeout = 30000
   .CursorDriver = rdUseNone
   .EstablishConnection rdDriverNoPrompt
End With
'TELAO DA SALA DO HELPDESK
With rdocnHelp
   .Connect = "UID=service_infra;PWD=service_infra;server=SISAB056;driver={SQL Server};database=service_infra_db;"
   .QueryTimeout = 30000
   .CursorDriver = rdUseNone
   .EstablishConnection rdDriverNoPrompt
End With
      
Exit Sub
Resume
TrataErro:
  
End Sub




