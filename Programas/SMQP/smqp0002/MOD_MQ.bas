Attribute VB_Name = "MOD_MQ"
Option Explicit
'Fun��es para registrar Fonts - Por Daniel Romualdo(16/04/2003)
Public Const WM_FONTCHANGE = &H1D
Public Const HWND_BROADCAST = &HFFFF&
Declare Function AddFontResource& Lib "gdi32" Alias "AddFontResourceA" (ByVal lpFileName As String)
Declare Function SendMessageBynum& Lib "user32" Alias "SendMessageA" (ByVal hwnd As Long, ByVal wMsg As Long, ByVal wParam As Integer, ByVal lparam As Long)
Declare Function ShellExecute Lib "shell32.dll" Alias "ShellExecuteA" (ByVal hwnd As Long, ByVal lpOperation As String, ByVal lpFile As String, ByVal lpParameters As String, ByVal lpDirectory As String, ByVal nShowCmd As Long) As Long

' vari�veis
Public gsPastaLocalSegbr      As String
Public gsPastaServidorSegbr   As String
Public gsHost                 As String
Public gsMac                  As String
Public gsIp                   As String
Public gsDHEntrada            As String
Public gsCodAplicacao         As String
Public gsParamAplicacao       As String
Public gsMensagem             As String
Public glTempoDeEsperaSEGBR   As Long
Public glTempoDeEsperaUsuario As Long
Public gsCPF                  As String
Public glAmbiente_id          As Long
Public glEmpresa_id           As Long

Public traySegbr              As NOTIFYICONDATA
Public sDecimal               As String
Public SQS                    As String
Public lpUserName             As String
Public cUserName              As String
Public nameu                  As String
Public cpermissao             As String
Public arquivoArquivo         As String
Public InicioSenha            As Integer
Public FimSenha               As Integer
Public InicioUsuario          As Integer
Public FimUsuario             As Integer
Public Usuario                As String
Public UsuarioWeb             As String
Public SenhaWeb               As String
Public senha                  As String
Public Status_Sistema         As String
Public Data_Sistema           As String
Public Data_Contabilizacao    As Date
Public Ambiente               As String
Public SIS_server             As String
Public SIS_banco              As String
Public SIS_usuario            As String
Public SIS_senha              As String
Public INT_server             As String
Public INT_banco              As String
Public INT_usuario            As String
Public INT_senha              As String
Public BKP_usuario            As String
Public BKP_senha              As String
Public SP_usuario             As String
Public IndentFlag             As Boolean
Public ftpServidorSISE        As String
Public ftpUsuarioServidorSISE As String
Public ftpSenhaServidorSISE   As String
Public goProducao             As Object

' constantes
Public Const gsSIGLAEMPRESA = "SoftRoad"
'* ALTERADO POR JUNIOR PARA TESTES DO MQ
Public Const gsSIGLASISTEMA = "SMQ"
'Public Const gsSIGLASISTEMA = "SMQ"
Public Const gsCODPRINCIPAL = "SEGP0007"
Public Const glINTERVALOATUALIZACAO = 60000 ' milissegundos

' constantes de erro
Public Const glERRO_VALIDA_PARAMETRO = 1
Public Const gsERRO_VALIDA_PARAMETRO = "S� � poss�vel abrir o projeto ?? pelo " & gsSIGLASISTEMA & "."
Public Const glERRO_MONTA_PARAMETRO = 2
Public Const gsERRO_MONTA_PARAMETRO = "N�o foi poss�vel autenticar o programa com o c�digo ??."
Public Const glERRO_ATUALIZA_ARQUIVO = 3
Public Const gsERRO_ATUALIZA_ARQUIVO = "Erro na tentativa de atualizar o arquivo ??."
Public Const glERRO_TELA_USUARIO_EM_SESSAO = 4
Public Const gsERRO_TELA_USUARIO_EM_SESSAO = "XXX"
Public Const glERRO_USUARIO_SEM_PERMISSAO = 5
Public Const gsERRO_USUARIO_SEM_PERMISSAO = "Usu�rio sem acesso � fun��o: ??."
Public Const glERRO_FUNCAO_INDISPONIVEL = 6
Public Const gsERRO_FUNCAO_INDISPONIVEL = "Fun��o n�o dispon�vel: ??."
Public Const glERRO_FECHA_APLICATIVO = 7
Public Const gsERRO_FECHA_APLICATIVO = "Aplicativo ?? n�o p�de ser fechado."
Public Const glERRO_TIPO_NAO_CORRETO = 8
Public Const gsERRO_TIPO_NAO_CORRETO = "Tipo n�o esperado ??."
Public Const glERRO_GERENCIADOR_MENSAGEM = 9
Public Const gsERRO_GERENCIADOR_MENSAGEM = "XXX"
Public Const glERRO_ARQUIVO_NAO_ENCONTRADO = 10
Public Const gsERRO_ARQUIVO_NAO_ENCONTRADO = "Arquivo n�o encontrado ??."
Public Const glERRO_USUARIO_COM_VARIAS_CONEXOES = 11
Public Const gsERRO_USUARIO_COM_VARIAS_CONEXOES = "?? j� est� usando o " & gsSIGLASISTEMA & " na m�quina ??."

' constantes de API
Public Const CB_ERR = -1
Public Const CB_FINDSTRING = &H14C
Public Const dwWindows95 = &H1&
Public Const dwWindowsNT = &H2&
Public Const SYNCHRONIZE = &H100000
Public Const PROCESS_QUERY_INFORMATION = &H400
Public Const PROCESS_TERMINATE = &H1
Public Const STILL_ACTIVE = &H103
Public Const NORMAL_PRIORITY_CLASS = &H20&
Public Const INFINITE = -1&
Public Const SW_SHOWNORMAL = 1
Public Const SW_SHOWMINIMIZED = 2
Public Const SW_SHOWMAXIMIZED = 3
Public Const SW_SHOWNOACTIVATE = 4
Public Const SW_SHOW = 5
Public Const SW_SHOWMINNOACTIVE = 7
Public Const SW_SHOWNA = 8
Public Const SW_RESTORE = 9
Public Const SW_SHOWDEFAULT = 10
Public Const GW_HWNDPREV = 3
Public Const GW_HWNDNEXT = 2
Public Const GWW_HINSTANCE = (-6)
Public Const HKEY_CLASSES_ROOT = &H80000000
Public Const HKEY_CURRENT_USER = &H80000001
Public Const HKEY_LOCAL_MACHINE = &H80000002
Public Const HKEY_USERS = &H80000003
Public Const REG_SZ As Long = 1
Public Const REG_DWORD As Long = 4
Public Const READ_CONTROL = &H20000
Public Const KEY_QUERY_VALUE = &H1
Public Const KEY_SET_VALUE = &H2
Public Const KEY_CREATE_SUB_KEY = &H4
Public Const KEY_ENUMERATE_SUB_KEYS = &H8
Public Const KEY_NOTIFY = &H10
Public Const KEY_CREATE_LINK = &H20
Public Const KEY_ALL_ACCESS = KEY_QUERY_VALUE + KEY_SET_VALUE + KEY_CREATE_SUB_KEY + KEY_ENUMERATE_SUB_KEYS + KEY_NOTIFY + KEY_CREATE_LINK + READ_CONTROL
Public Const KEY_LEITURA = KEY_QUERY_VALUE + KEY_ENUMERATE_SUB_KEYS
Public Const NIM_ADD = &H0
Public Const NIM_MODIFY = &H1
Public Const NIM_DELETE = &H2
Public Const NIF_MESSAGE = &H1
Public Const NIF_ICON = &H2
Public Const NIF_TIP = &H4
Public Const WM_MOUSEMOVE = &H200
Public Const MAX_LANA As Byte = 254  '  lana's in range 0 to MAX_LANA
Public Const NCBENUM As Long = &H37
Public Const NCBASTAT As Long = &H33
Public Const NCBNAMSZ As Long = 16
Public Const HEAP_ZERO_MEMORY As Long = &H8
Public Const HEAP_GENERATE_EXCEPTIONS As Long = &H4
Public Const NCBRESET As Long = &H32
Public Const MAX_WSADescription = 256
Public Const MAX_WSASYSStatus = 128
Public Const ERROR_SUCCESS       As Long = 0
Public Const WS_VERSION_REQD     As Long = &H101
Public Const WS_VERSION_MAJOR    As Long = WS_VERSION_REQD \ &H100 And &HFF&
Public Const WS_VERSION_MINOR    As Long = WS_VERSION_REQD And &HFF&
Public Const MIN_SOCKETS_REQD    As Long = 1
Public Const SOCKET_ERROR        As Long = -1
Public Const MAX_COMPUTERNAME_LENGTH As Long = 15&
Public Const gREGKEYSYSINFOLOC = "SOFTWARE\Microsoft\Shared Tools Location"
Public Const gREGVALSYSINFOLOC = "MSINFO"
Public Const gREGKEYSYSINFO = "SOFTWARE\Microsoft\Shared Tools\MSINFO"
Public Const gREGVALSYSINFO = "PATH"

' tipos de API

Public Type OSVERSIONINFO 'for GetVersionEx API call
  dwOSVersionInfoSize As Long
  dwMajorVersion As Long
  dwMinorVersion As Long
  dwBuildNumber As Long
  dwPlatformId As Long
  szCSDVersion As String * 128
End Type

Public Type STARTUPINFO
  cb As Long
  lpReserved As String
  lpDesktop As String
  lpTitle As String
  dwX As Long
  dwY As Long
  dwXSize As Long
  dwYSize As Long
  dwXCountChars As Long
  dwYCountChars As Long
  dwFillAttribute As Long
  dwFlags As Long
  wShowWindow As Integer
  cbReserved2 As Integer
  lpReserved2 As Long
  hStdInput As Long
  hStdOutput As Long
  hStdError As Long
End Type

Public Type PROCESS_INFORMATION
  hProcess As Long
  hThread As Long
  dwProcessId As Long
  dwThreadID As Long
End Type

Public Type WNDCLASS
  style As Long
  lpfnwndproc As Long
  cbClsextra As Long
  cbWndExtra2 As Long
  hInstance As Long
  hIcon As Long
  hCursor As Long
  hbrBackground As Long
  lpszMenuName As String
  lpszClassName As String
End Type

Public Type NET_CONTROL_BLOCK  'NCB
  ncb_command    As Byte
  ncb_retcode    As Byte
  ncb_lsn        As Byte
  ncb_num        As Byte
  ncb_buffer     As Long
  ncb_length     As Integer
  ncb_callname   As String * NCBNAMSZ
  ncb_name       As String * NCBNAMSZ
  ncb_rto        As Byte
  ncb_sto        As Byte
  ncb_post       As Long
  ncb_lana_num   As Byte
  ncb_cmd_cplt   As Byte
  ncb_reserve(9) As Byte ' Reserved, must be 0
  ncb_event      As Long
End Type

Public Type ADAPTER_STATUS
  adapter_address(5) As Byte
  rev_major         As Byte
  reserved0         As Byte
  adapter_type      As Byte
  rev_minor         As Byte
  duration          As Integer
  frmr_recv         As Integer
  frmr_xmit         As Integer
  iframe_recv_err   As Integer
  xmit_aborts       As Integer
  xmit_success      As Long
  recv_success      As Long
  iframe_xmit_err   As Integer
  recv_buff_unavail As Integer
  t1_timeouts       As Integer
  ti_timeouts       As Integer
  Reserved1         As Long
  free_ncbs         As Integer
  max_cfg_ncbs      As Integer
  max_ncbs          As Integer
  xmit_buf_unavail  As Integer
  max_dgram_size    As Integer
  pending_sess      As Integer
  max_cfg_sess      As Integer
  max_sess          As Integer
  max_sess_pkt_size As Integer
  name_count        As Integer
End Type

Public Type NAME_BUFFER
  name        As String * NCBNAMSZ
  name_num    As Integer
  name_flags  As Integer
End Type

Public Type ASTAT
  adapt          As ADAPTER_STATUS
  NameBuff(30)   As NAME_BUFFER
End Type

Public Type LANA_ENUM
  Length As Integer
  lana(MAX_LANA) As Integer
End Type

Public Type NOTIFYICONDATA
  cbSize As Long
  hwnd As Long
  uID As Long
  uFlags As Long
  uCallbackMessage As Long
  hIcon As Long
  szTip As String * 64
End Type

Public Type HOSTENT
  hName      As Long
  hAliases   As Long
  hAddrType  As Integer
  hLen       As Integer
  hAddrList  As Long
End Type

Public Type WSADATA
  wVersion      As Integer
  wHighVersion  As Integer
  szDescription(0 To MAX_WSADescription)   As Byte
  szSystemStatus(0 To MAX_WSASYSStatus)    As Byte
  wMaxSockets   As Integer
  wMaxUDPDG     As Integer
  dwVendorInfo  As Long
End Type

Public Declare Function GetForegroundWindow Lib "user32" () As Long

' declara��es de API
Public Declare Function EnableWindow Lib "user32" (ByVal hwnd As Long, ByVal benable As Boolean) As Long
Public Declare Function CreateEllipticRgn Lib "gdi32" (ByVal X1 As Long, ByVal Y1 As Long, ByVal X2 As Long, ByVal Y2 As Long) As Long
Public Declare Function SetWindowRgn Lib "user32" (ByVal hwnd As Long, ByVal hRgn As Long, ByVal bRedraw As Boolean) As Long
Public Declare Function GetActiveWindow Lib "user32" () As Long
Public Declare Function SetActiveWindow Lib "user32" (ByVal hwnd As Long) As Long
Public Declare Function SetFocus Lib "user32" (ByVal hwnd As Long) As Long
Public Declare Function IsWindowEnabled Lib "user32" (ByVal hwnd As Long) As Long
Public Declare Function IsWindowVisible Lib "user32" (ByVal hwnd As Long) As Long
Public Declare Function OpenIcon Lib "user32" (ByVal hwnd As Long) As Long
Public Declare Function SetForegroundWindow Lib "user32" (ByVal hwnd As Long) As Long
Public Declare Function GetParent Lib "user32" (ByVal hwnd As Long) As Long
Public Declare Function IsIconic Lib "user32" (ByVal hwnd As Long) As Long
Public Declare Function Shell_NotifyIcon Lib "shell32.dll" Alias "Shell_NotifyIconA" (ByVal dwMessage As Long, lpData As NOTIFYICONDATA) As Long
Public Declare Function GetWindowThreadProcessId Lib "user32" (ByVal hwnd As Long, lpdwProcessId As Long) As Long
Public Declare Function GetWindow Lib "user32" (ByVal hwnd As Long, ByVal wCmd As Long) As Long
Public Declare Function GetWindowWord Lib "user32" (ByVal hwnd As Long, ByVal nIndex As Long) As Integer
Public Declare Function GetWindowText Lib "user32" Alias "GetWindowTextA" (ByVal hwnd As Long, ByVal lpString As String, ByVal cch As Long) As Long
Public Declare Function GetClassInfo Lib "user32" Alias "GetClassInfoA" (ByVal hInstance As Long, ByVal lpClassName As String, lpWndClass As WNDCLASS) As Long
Public Declare Function ShowWindow Lib "user32" (ByVal hwnd As Long, ByVal nCmdShow As Long) As Long
Public Declare Function FindWindow Lib "user32" Alias "FindWindowA" (ByVal lpClassName As String, ByVal lpWindowName As String) As Long
Public Declare Function SendMessage Lib "user32" Alias "SendMessageA" (ByVal hwnd As Long, ByVal wMsg As Long, ByVal wParam As Long, lparam As Any) As Long

Public Declare Function GetVersionEx Lib "kernel32" Alias "GetVersionExA" (lpVersionInformation As OSVERSIONINFO) As Long
Public Declare Function GetPrivateProfileString Lib "kernel32" Alias "GetPrivateProfileStringA" (ByVal lpApplicationName As String, ByVal lpKeyName As Any, ByVal lpDefault As String, ByVal lpReturnedString As String, ByVal nSize As Long, ByVal lpFileName As String) As Long
Public Declare Sub CopyMemory Lib "kernel32" Alias "RtlMoveMemory" (hpvDest As Any, ByVal hpvSource As Long, ByVal cbCopy As Long)
Public Declare Function GetProcessHeap Lib "kernel32" () As Long
Public Declare Function HeapAlloc Lib "kernel32" (ByVal hHeap As Long, ByVal dwFlags As Long, ByVal dwBytes As Long) As Long
Public Declare Function HeapFree Lib "kernel32" (ByVal hHeap As Long, ByVal dwFlags As Long, lpMem As Any) As Long
Public Declare Function GetComputerName Lib "kernel32" Alias "GetComputerNameA" (ByVal lpBuffer As String, nSize As Long) As Long
Public Declare Sub Sleep Lib "kernel32" (ByVal dwMilliseconds As Long)
Public Declare Function CloseHandle Lib "kernel32" (ByVal hObject As Long) As Long
Public Declare Function OpenProcess Lib "kernel32" (ByVal dwDesiredAccess As Long, ByVal bInheritHandle As Long, ByVal dwProcessId As Long) As Long
Public Declare Function GetExitCodeProcess Lib "kernel32" (ByVal hProcess As Long, lpExitCode As Long) As Long
Public Declare Function TerminateProcess Lib "kernel32" (ByVal hProcess As Long, ByVal uExitCode As Long) As Long
Public Declare Function WaitForSingleObject Lib "kernel32" (ByVal hHandle As Long, ByVal dwMilliseconds As Long) As Long
Public Declare Function CreateProcessA Lib "kernel32" (ByVal lpApplicationName As Long, ByVal lpCommandLine As String, ByVal lpProcessAttributes As Long, ByVal lpThreadAttributes As Long, ByVal bInheritHandles As Long, ByVal dwCreationFlags As Long, ByVal lpEnvironment As Long, ByVal lpCurrentDirectory As Long, lpStartupInfo As STARTUPINFO, lpProcessInformation As PROCESS_INFORMATION) As Long

Public Declare Function GetUserName Lib "advapi32.dll" Alias "GetUserNameA" (ByVal lpBuffer As String, nSize As Long) As Long
Public Declare Function RegOpenKeyEx Lib "advapi32.dll" Alias "RegOpenKeyExA" (ByVal hKey As Long, ByVal lpSubKey As String, ByVal ulOptions As Long, ByVal samDesired As Long, phkResult As Long) As Long
Public Declare Function RegSetValueExString Lib "advapi32.dll" Alias "RegSetValueExA" (ByVal hKey As Long, ByVal lpValueName As String, ByVal Reserved As Long, ByVal dwType As Long, ByVal lpValue As String, ByVal cbData As Long) As Long
Public Declare Function RegSetValueExLong Lib "advapi32.dll" Alias "RegSetValueExA" (ByVal hKey As Long, ByVal lpValueName As String, ByVal Reserved As Long, ByVal dwType As Long, lpValue As Long, ByVal cbData As Long) As Long
Public Declare Function RegQueryValueEx Lib "advapi32" Alias "RegQueryValueExA" (ByVal hKey As Long, ByVal lpValueName As String, ByVal lpReserved As Long, ByRef lpType As Long, ByVal lpData As String, ByRef lpcbData As Long) As Long
Public Declare Function RegCloseKey Lib "advapi32.dll" (ByVal hKey As Long) As Long

Public Declare Function WSAGetLastError Lib "WSOCK32.DLL" () As Long
Public Declare Function WSAStartup Lib "WSOCK32.DLL" (ByVal wVersionRequired As Long, lpWSADATA As WSADATA) As Long
Public Declare Function WSACleanup Lib "WSOCK32.DLL" () As Long
Public Declare Function gethostname Lib "WSOCK32.DLL" (ByVal szHost As String, ByVal dwHostLen As Long) As Long
Public Declare Function gethostbyname Lib "WSOCK32.DLL" (ByVal szHost As String) As Long

Public Declare Function WNetGetUser Lib "mpr.dll" Alias "WNetGetUserA" (ByVal lpName As String, ByVal lpUserName As String, lpnLength As Long) As Long

Public Declare Function Netbios Lib "netapi32.dll" (pncb As NET_CONTROL_BLOCK) As Byte

Public MSGERRO     As String
Public Const ErrCode As Long = 1

Global rdocn      As New Connection
Global rdoCnWEB   As New Connection
'
'Private Const SYNCHRONIZE = &H100000
'Private Const INFINITE = -1&
'Private Declare Function OpenProcess Lib "kernel32" (ByVal dwDesiredAccess As Long, ByVal bInheriHandle As Long, ByVal dwProcessId As Long) As Long
'Private Declare Function WaitForSingleObject Lib "kernel32" (ByVal hHandle As Long, ByVal dwMiliseconds As Long) As Long
'Private Declare Function CloseHandle Lib "kernel32" (ByVal hObject As Long) As Long
Declare Function CopyFile Lib "kernel32" Alias "CopyFileA" (ByVal IpExistingFileName As String, ByVal IpNewFileName As String, ByVal bFailifExists As Boolean) As Long

Public Sub enviarMSG(programa As String)

    Dim RetVal
    Dim lpBuff  As String * 25
    Dim Retorno As Long
    Dim caminho As String
    Dim Usuario As String
    Dim sql  As String
    Dim email As String
    
    On Error GoTo TRATARERRO
    
    If glAmbiente_id = 2 Then
        
        email = "cxproducao@aliancadobrasil.com.br;"
        sql = "exec seguros_db..envia_email_sp '" & email & "', '" & "Erro no " & programa & "', '" & MSGERRO & "'"
        Call ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, sql, False)
        
    End If
    
Exit Sub

TRATARERRO:
    MSGERRO = CStr(Err.Number) & CStr(Err.Description)
    Call GravarErroArquivo(programa, "N")

Exit Sub

End Sub

Public Sub VerificarPermissaoDeExecucao(programa As String, Arquivo As String, Secao As String)
    On Error GoTo Trata_Erro
    
    Dim rsTB        As ADODB.Recordset
    Dim sql         As String
    Dim i           As Integer
    Dim ArqOrigem   As String
    Dim num         As Integer
    
    'foi gerado um arquivo de erro e o programa n�o dever� ser executado
    'at� que o analista libere o arquivo
    frmPrincipal.File1.Path = "C:\log_mq\services\Erro\"
    For i = 1 To frmPrincipal.File1.ListCount
        frmPrincipal.File1.ListIndex = i - 1
        If UCase(frmPrincipal.File1.FileName) = UCase(Arquivo) Then
            MSGERRO = programa & " - Arquivo de erro gerado."
            enviarMSG programa
        End If
    Next i
    
'    SQL = "SELECT situacao"
'    SQL = SQL & " FROM interface_db..parametro_mq_tb"
'    SQL = SQL & " WHERE DESCRICAO = '" & Secao & "'"
'    SQL = SQL & " AND OBJETO = '" & programa & "'"
'    Set rsTB = ExecutarSQL("SEGBR", glAmbiente_id, App.Title, App.FileDescription, SQL, True)
'    If rsTB.EOF Then Exit Sub
'
'    If rsTB("SITUACAO") = "B" Then
'        MSGERRO = programa & " - PROGRAMA SINALIZADO COM ERRO."
'        enviarMSG
'        Exit Sub
'    End If
'    If programa = "SMQP0005" And UCase(rsTB("SITUACAO")) = "B" Then End
    
Exit Sub
Trata_Erro:
    MSGERRO = programa & " - Erro na Sub VerificarPermissaoDeExecucao!"
    GravarErroArquivo programa
End Sub

Public Sub GravarErroArquivo(programa As String, Optional ByVal email As String = "S")
    'criar tabela temporaria
    Dim ArqOrigem As String
    Dim num As Integer
    Dim Diretorio As String
    
    Diretorio = "C:\log_mq\services\Erro\"
    
'    ArqOrigem = "C:\log_mq\erro\" & programa & "_" & frmPrincipal.Secao & "." & Format(Date, "yyyymmdd") & ".ERRO.text"
'    num = FreeFile
'    Open ArqOrigem For Append As #num
'    Print #num, Now() & " - " & MSGERRO
'    Close #num
'    If email = "S" Then
'            enviarMSG programa
'    End If
    'myoshimura
    '20/08/2005 retirar a mensagem do telao
    'inclusao do telao ser� um servi�o separado
    'If glAmbiente_id = 2 Then ColocaNoTelao numTelao, 1
    'End
    ArqOrigem = Diretorio & "Erro_SMQP0002" & "_" & Format(Date, "yyyymmdd") & ".text"
    num = FreeFile
    Open ArqOrigem For Append As #num
    Print #num, Now() & " - " & MSGERRO
    Close #num
    
    If email = "S" Then
            enviarMSG programa
    End If
    
End Sub

Public Sub GravarTrace(MsgTrace As String)
    'criar tabela temporaria
    Dim ArqOrigem As String
    Dim num As Integer
    Dim Diretorio As String
    
    Diretorio = "C:\log_mq\services\Trace\"
    
    'Trace_SMQP0002_20160316.Text
    ArqOrigem = Diretorio & "Trace_SMQP0002" & "_" & Format(Date, "yyyymmdd") & ".text"
    num = FreeFile
    Open ArqOrigem For Append As #num
    Print #num, Now() & " - " & MsgTrace
    Close #num
End Sub

Function Obtem_Secao(OParametro As String, programa As String) As String
'Raphael Luiz Gagliardi 22/05/2001
'Obt�m agenda_diaria_id a partir do par�metro passado
'Se retornar 0 --> Manual
Dim lposicao1 As Long
Dim lposicao2 As Long
Dim parametro As String
On Error GoTo Trata_Erro
  lposicao1 = InStr(1, OParametro, "||")
  lposicao2 = InStr(lposicao1 + 1, OParametro, "||")
  If lposicao1 > 0 And lposicao2 > 0 Then
     parametro = Mid(OParametro, lposicao1 + 2, lposicao2 - lposicao1 - 2)
  Else
     GoTo Trata_Erro
  End If
  glAmbiente_id = Mid(parametro, Len(parametro), 1)
  Obtem_Secao = Mid(parametro, 1, Len(parametro) - 2)
  Exit Function
Trata_Erro:
    MSGERRO = programa & " - Erro na Fun�ao: Obtem_Secao!"
    GravarErroArquivo programa
  
End Function
