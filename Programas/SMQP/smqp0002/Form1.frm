VERSION 5.00
Begin VB.Form frmPrincipal 
   Caption         =   "SMQP0002 - MQ de envio AB para BB"
   ClientHeight    =   6105
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   11235
   LinkTopic       =   "Form1"
   ScaleHeight     =   6105
   ScaleWidth      =   11235
   StartUpPosition =   3  'Windows Default
   Begin VB.FileListBox File1 
      Height          =   285
      Left            =   7380
      TabIndex        =   44
      Top             =   5625
      Visible         =   0   'False
      Width           =   195
   End
   Begin VB.Timer Timer1 
      Interval        =   60000
      Left            =   1890
      Top             =   -135
   End
   Begin VB.PictureBox Picture1 
      Height          =   510
      Left            =   2655
      Picture         =   "Form1.frx":0000
      ScaleHeight     =   450
      ScaleWidth      =   495
      TabIndex        =   43
      Top             =   -90
      Visible         =   0   'False
      Width           =   555
   End
   Begin VB.ComboBox cboAmbiente 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      ItemData        =   "Form1.frx":0442
      Left            =   8505
      List            =   "Form1.frx":0444
      TabIndex        =   23
      Top             =   4725
      Width           =   2580
   End
   Begin VB.CommandButton cmdTrocarSecao 
      Caption         =   "Trocar Se��o"
      Height          =   465
      Left            =   5760
      TabIndex        =   22
      Top             =   5445
      Width           =   1410
   End
   Begin VB.TextBox txtSecao 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   5760
      TabIndex        =   20
      Text            =   "MQ_ENVIO_SINISTRO"
      Top             =   4725
      Width           =   2535
   End
   Begin VB.Frame Frame4 
      Caption         =   "Banco de Dados"
      Enabled         =   0   'False
      Height          =   4290
      Left            =   0
      TabIndex        =   13
      Top             =   0
      Width           =   5595
      Begin VB.TextBox txtSelecao 
         Height          =   330
         Left            =   135
         TabIndex        =   45
         Text            =   "selecao_saida_gtr_sps"
         Top             =   1005
         Width           =   5325
      End
      Begin VB.TextBox txtSPEnvio 
         Height          =   330
         Left            =   135
         TabIndex        =   39
         Text            =   "sinistro_envio_spu"
         Top             =   3780
         Width           =   5325
      End
      Begin VB.TextBox txtTpRegistro 
         Height          =   330
         Left            =   135
         TabIndex        =   37
         Text            =   "sinistro_bb"
         Top             =   3240
         Width           =   5325
      End
      Begin VB.TextBox txtCampoID 
         Height          =   330
         Left            =   135
         TabIndex        =   35
         Text            =   "saida_gtr_id"
         Top             =   2715
         Width           =   5325
      End
      Begin VB.TextBox txtTabelaRegistro 
         Height          =   330
         Left            =   135
         TabIndex        =   18
         Text            =   "saida_gtr_registro_tb"
         Top             =   2175
         Width           =   5325
      End
      Begin VB.TextBox txtTabelaControle 
         Height          =   330
         Left            =   135
         TabIndex        =   16
         Text            =   "saida_gtr_tb"
         Top             =   1590
         Width           =   5325
      End
      Begin VB.TextBox txtBancoDados 
         Height          =   330
         Left            =   135
         TabIndex        =   14
         Text            =   "interface_db"
         Top             =   420
         Width           =   5280
      End
      Begin VB.Label Label19 
         AutoSize        =   -1  'True
         Caption         =   "Stored Procedure de sele��o:"
         Height          =   195
         Left            =   135
         TabIndex        =   46
         Top             =   810
         Width           =   2115
      End
      Begin VB.Label Label17 
         AutoSize        =   -1  'True
         Caption         =   "Stored Procedure para marcar o envio:"
         Height          =   195
         Left            =   135
         TabIndex        =   40
         Top             =   3600
         Width           =   2745
      End
      Begin VB.Label Label16 
         AutoSize        =   -1  'True
         Caption         =   "Tipo de registro de envio:"
         Height          =   195
         Left            =   135
         TabIndex        =   38
         Top             =   3060
         Width           =   1800
      End
      Begin VB.Label Label15 
         AutoSize        =   -1  'True
         Caption         =   "Campo Identificador da Tabela de Controle:"
         Height          =   195
         Left            =   135
         TabIndex        =   36
         Top             =   2520
         Width           =   3075
      End
      Begin VB.Label Label12 
         AutoSize        =   -1  'True
         Caption         =   "Tabela de Registro:"
         Height          =   195
         Left            =   135
         TabIndex        =   19
         Top             =   1980
         Width           =   1395
      End
      Begin VB.Label Label11 
         AutoSize        =   -1  'True
         Caption         =   "Tabela de controle:"
         Height          =   195
         Left            =   135
         TabIndex        =   17
         Top             =   1395
         Width           =   1380
      End
      Begin VB.Label Label10 
         AutoSize        =   -1  'True
         Caption         =   "Banco de Dados:"
         Height          =   195
         Left            =   135
         TabIndex        =   15
         Top             =   225
         Width           =   1245
      End
   End
   Begin VB.Frame Frame3 
      Caption         =   "Destino"
      Enabled         =   0   'False
      Height          =   2535
      Left            =   5625
      TabIndex        =   8
      Top             =   1755
      Width           =   5595
      Begin VB.TextBox txtFilaSaidaBB 
         Height          =   330
         Left            =   135
         TabIndex        =   41
         Text            =   "ALQM028196889A"
         Top             =   1260
         Width           =   5325
      End
      Begin VB.TextBox txtQueueManagerDestino 
         Height          =   330
         Left            =   135
         TabIndex        =   10
         Text            =   "ALMQS0A"
         Top             =   555
         Width           =   5325
      End
      Begin VB.TextBox txtFilaLocalDestino 
         Height          =   330
         Left            =   135
         TabIndex        =   9
         Text            =   "QE.TESTE"
         Top             =   1905
         Width           =   5325
      End
      Begin VB.Label Label18 
         AutoSize        =   -1  'True
         Caption         =   "Nome do Gerenciador de Fila de Sa�da do BB"
         Height          =   195
         Left            =   135
         TabIndex        =   42
         Top             =   1035
         Width           =   3255
      End
      Begin VB.Label Label7 
         AutoSize        =   -1  'True
         Caption         =   "Nome do Gerenciador de Fila"
         Height          =   195
         Left            =   135
         TabIndex        =   12
         Top             =   360
         Width           =   2070
      End
      Begin VB.Label Label9 
         AutoSize        =   -1  'True
         Caption         =   "Nome da Fila Local"
         Height          =   195
         Left            =   135
         TabIndex        =   11
         Top             =   1710
         Width           =   1365
      End
   End
   Begin VB.Frame Frame2 
      Caption         =   "Origem"
      Enabled         =   0   'False
      Height          =   1725
      Left            =   5625
      TabIndex        =   3
      Top             =   0
      Width           =   5595
      Begin VB.TextBox NomeQueueManager 
         Height          =   330
         Left            =   135
         TabIndex        =   5
         Text            =   "QM028196889"
         Top             =   540
         Width           =   5325
      End
      Begin VB.TextBox txtResposta 
         Height          =   330
         Left            =   135
         TabIndex        =   4
         TabStop         =   0   'False
         Text            =   "QE.EXT.028196889.SINISTRO.RECEBE.REPLY"
         Top             =   1185
         Width           =   5325
      End
      Begin VB.Label Label4 
         AutoSize        =   -1  'True
         Caption         =   "Nome do Gerenciador de Fila"
         Height          =   195
         Left            =   135
         TabIndex        =   7
         Top             =   360
         Width           =   2070
      End
      Begin VB.Label Label6 
         AutoSize        =   -1  'True
         Caption         =   "Nome da Fila de Resposta (COA e COD)"
         Height          =   195
         Left            =   135
         TabIndex        =   6
         Top             =   990
         Width           =   2865
      End
   End
   Begin VB.Frame Frame1 
      Enabled         =   0   'False
      Height          =   1770
      Left            =   0
      TabIndex        =   2
      Top             =   4320
      Width           =   5595
      Begin VB.TextBox txtHorarioTermino 
         Height          =   330
         Left            =   1530
         TabIndex        =   33
         Top             =   1185
         Width           =   1185
      End
      Begin VB.TextBox txtData 
         Height          =   330
         Left            =   225
         TabIndex        =   28
         Top             =   465
         Width           =   1185
      End
      Begin VB.TextBox txtHora 
         Height          =   330
         Left            =   1530
         TabIndex        =   27
         Top             =   465
         Width           =   1185
      End
      Begin VB.TextBox txtEnviado 
         Height          =   330
         Left            =   2970
         TabIndex        =   26
         Top             =   1230
         Width           =   2490
      End
      Begin VB.TextBox txtDataTermino 
         Height          =   330
         Left            =   225
         TabIndex        =   25
         Top             =   1185
         Width           =   1185
      End
      Begin VB.Label Label5 
         AutoSize        =   -1  'True
         Caption         =   "Hor�rio de t�rmino:"
         Height          =   195
         Left            =   1530
         TabIndex        =   34
         Top             =   990
         Width           =   1335
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Data de in�cio:"
         Height          =   195
         Left            =   225
         TabIndex        =   32
         Top             =   270
         Width           =   1050
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "Hor�rio de in�cio:"
         Height          =   195
         Left            =   1530
         TabIndex        =   31
         Top             =   270
         Width           =   1215
      End
      Begin VB.Label Label3 
         AutoSize        =   -1  'True
         Caption         =   "Qtde registros enviados:"
         Height          =   195
         Left            =   2970
         TabIndex        =   30
         Top             =   1035
         Width           =   1710
      End
      Begin VB.Label Label8 
         AutoSize        =   -1  'True
         Caption         =   "Data de t�rmino:"
         Height          =   195
         Left            =   225
         TabIndex        =   29
         Top             =   990
         Width           =   1170
      End
   End
   Begin VB.CommandButton cmdSair 
      Caption         =   "Sair"
      Height          =   465
      Left            =   9720
      TabIndex        =   1
      Top             =   5445
      Width           =   1410
   End
   Begin VB.CommandButton cmdIniciar 
      Caption         =   "Iniciar"
      Height          =   465
      Left            =   7785
      TabIndex        =   0
      Top             =   5445
      Width           =   1410
   End
   Begin VB.Label Label14 
      AutoSize        =   -1  'True
      Caption         =   "Ambiente Execu��o"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   8505
      TabIndex        =   24
      Top             =   4410
      Width           =   1800
   End
   Begin VB.Label Label13 
      AutoSize        =   -1  'True
      Caption         =   "Se��o do MQ"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   5805
      TabIndex        =   21
      Top             =   4410
      Width           =   1245
   End
   Begin VB.Menu mnuArquivo 
      Caption         =   "Arquivo"
      Visible         =   0   'False
      Begin VB.Menu mnuAbrir 
         Caption         =   "Abrir"
      End
      Begin VB.Menu mnuArquivoSep 
         Caption         =   "-"
      End
      Begin VB.Menu mnuSair 
         Caption         =   "&Sair"
      End
   End
End
Attribute VB_Name = "frmPrincipal"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'* **************** *'
'*    STATUS MQ     *'
'* **************** *'
'* N - A PROCESSAR  *'
'* E - ERRO         *'
'* P - PENDENTE     *'
'* R - RECEBIDO     *'
'* L - LIDO         *'
'* T - PROCESSADO   *'
'* **************** *'

' Vari�veis para uso do MQSeries
Dim gHcon As Long
Dim gHobj As Long
Public Secao As String

'
'Declara��o de Constantes para utiliza��o do Systray
Private Const TRAY_MSG_MOUSEMOVE = 7680
Private Const TRAY_MSG_LEFTBTN_DOWN = 7695
Private Const TRAY_MSG_LEFTBTN_UP = 7710
Private Const TRAY_MSG_LEFTBTN_DBLCLICK = 7725
Private Const TRAY_MSG_RIGHTBTN_DOWN = 7740
Private Const TRAY_MSG_RIGHTBTN_UP = 7755
Private Const TRAY_MSG_RIGHTBTN_DBLCLICK = 7770
'

' Variaveis de parametros para fila
Private Sub cmdIniciar_Click()
    On Error GoTo TRATARERRO
    txtData.Text = Format(Date, "dd/mm/yyyy")
    txtHora.Text = Format(Time, "hh:mm:ss")
    Timer1.Enabled = False
    cmdIniciar.Enabled = False
    Call Processar
    
    txtDataTermino.Text = Format(Date, "dd/mm/yyyy")
    txtHorarioTermino.Text = Format(Time, "hh:mm:ss")
    Exit Sub
TRATARERRO:
    MSGERRO = "SMQP0002 - Erro na Sub cmdIniciar_Click!" & _
    vbNewLine & Err.Description
    GravarErroArquivo "SMQP0002"
End Sub

Private Sub BuscaParametro_MQ_Envio()
    On Error GoTo Trata_Erro
    Dim vSQL    As String
    Dim rs      As ADODB.Recordset
    
    Call GravarTrace("Busca Parametro MQ Envio")
    
    vSQL = ""
    vSQL = " SELECT campo, valor"
    vSQL = vSQL & " FROM controle_sistema_db..parametro_tb"
    vSQL = vSQL & " WHERE sigla_sistema = 'SMQ'"
    vSQL = vSQL & " AND secao = '" & Secao & "'"
    vSQL = vSQL & " AND ambiente_id =" & glAmbiente_id
    Set rs = ExecutarSQL("SEGBR", glAmbiente_id, App.Title, App.FileDescription, vSQL, True)
    
    If rs.EOF Then GoTo Trata_Erro
        
    Do While Not rs.EOF
        Select Case rs!CAMPO
            Case "GERENCIADOR_AB"                       'GERENCIADOR MQ DA ALIAN�A DO BRASIL
                NomeQueueManager.Text = rs!valor
            Case "GERENCIADOR_BB"                       'GERENCIADOR MQ DO BANCO DO BRASIL
                txtQueueManagerDestino.Text = rs!valor
            Case "GERENCIADOR_BB_SAIDA"                 'GERENCIADOR MQ DO BANCO DO BRASIL
                txtFilaSaidaBB.Text = rs!valor
            Case "FILA_LOCAL_BB"                        'FILA DE RESPOSTA AB
                txtFilaLocalDestino.Text = rs!valor
            Case "FILA_REPLY_AB"                        'FILA DE RESPOSTA AB
                txtResposta.Text = rs!valor
            Case "BANCODADOS"                           'BANCO DE DADOS ONDE EST� A FILA AB
                txtBancoDados.Text = rs!valor
            Case "TABELACONTROLE"                       'TABELA DE CONTROLE DA MENSAGEM A SER ENVIADA
                txtTabelaControle.Text = rs!valor
            Case "TABELAREGISTRO"                       'TABELA DO REGISTRO DA MENSAGEM
                txtTabelaRegistro.Text = rs!valor
            Case "REGISTROID"                           'IDENTIFICADOR DO REGISTRO DA MENSAGEM
                txtCampoID.Text = rs!valor
            Case "TPREGISTRO"                           'TIPO DE REGISTRO DA MENSAGEM
                txtTpRegistro.Text = rs!valor
            Case "STOREDPROCEDURE"                      'STORED PROCEDURE PARA MARCAR O ENVIO
                txtSPEnvio.Text = rs!valor
            Case "SELECAO"                              'STORED PROCEDURE PARA SELECAO DOS DADOS A SEREM ENVIADOS
                txtSelecao.Text = rs!valor
            Case "TELAO"                              'STORED PROCEDURE PARA SELECAO DOS DADOS A SEREM ENVIADOS
                numTelao = rs!valor
            End Select
            
            rs.MoveNext
    Loop
    rs.Close
    
    Exit Sub
    
Trata_Erro:
    MSGERRO = "SMQP0002 - Erro na Sub BuscaParametro_MQ_Envio!" & _
    vbNewLine & Err.Description & _
    vbNewLine & vSQL
    
    GravarErroArquivo "SMQP0002"
End Sub

Private Sub cmdSair_Click()
    Call FechaObjetosMQSeries
    Call DesconectaMQSeries
    End
End Sub

Private Sub cboAmbiente_LostFocus()
    glAmbiente_id = Mid(cboAmbiente.Text, 1, 2)
End Sub

Private Sub cmdTrocarSecao_Click()
    'Timer1.Enabled = False
    
    'busca configura��o para acesso
    Secao = txtSecao.Text
    Call BuscaParametro_MQ_Envio
    
    'Timer1.Enabled = True
    
End Sub

Private Sub Form_Load()
    On Error GoTo Trata_Erro
    'Defini��es necess�rias para conex�o com o MQSeries
    Dim CompCode As Long      ' Completion code
    Dim Reason As Long        ' Reason code
    MSGERRO = ""
    numTelao = 0
    'Setup constants
    MQ_SETDEFAULTS
    
    cUserName = "MQ_SMQP0002"
    glAmbiente_id = Right(Command, 1)
    'glAmbiente_id = 3
'    If Not Trata_Parametros(Command) Then
'        Call FinalizarAplicacao
'    End If
'
'    'Obtendo data operacional do sistema
'    Call ObterDataSistema(gsSIGLASISTEMA)
    '"MQ_ENVIO_SINISTRO"
    'se��o de configura��o do programa
    Secao = Mid(Command, 1, Len(Command) - 2)
    txtSecao.Text = Secao
    
    Call GravarTrace("In�cio - Execu��o SMQP0002")
    
    'carrega combo de ambiente de execu��o
    Call GravarTrace("carrega combo de ambiente de execu��o")
    Call carregaCombo
    
    
    Call GravarTrace("Verificar Permissao De Execucao")
    VerificarPermissaoDeExecucao "SMQP0002", "SMQP0002_" & Secao & "_ERRO.TXT", Secao
    cmdTrocarSecao_Click
    
    Call GravarTrace("valida Opcoes Regionais")
    validaOpcoesRegionais
    
    cmdIniciar_Click
    
    txtEnviado.Text = 0
    Exit Sub
    
Trata_Erro:
    MSGERRO = "SMQP0002 - Erro na Sub Form_Load!" & _
    vbNewLine & Err.Description

    GravarErroArquivo "SMQP0002"
    
    MsgBox ("N�o foi poss�vel Iniciar a Aplica��o, Consulte o log")
    End
End Sub

Private Sub Processar()

    Dim msg_id   As Variant
    Dim vSQL     As String
    Dim registro As String
    Dim rs       As ADODB.Recordset
    Dim boleano  As Boolean
    Dim i        As Integer
    
    Dim ArqOrigem As String
    Dim num As Integer
    Dim ArqOrigem1 As String
    Dim num1 As Integer
    
    On Error GoTo Trata_Erro
    
    ArqOrigem = "C:\log_mq\services\Erro\SMQP0002." & Secao & "." & Format(Date, "yyyymmdd") & ".Text"
    ArqOrigem1 = "C:\log_mq\services\Erro\SMQP0002.log." & Secao & "." & Format(Date, "yyyymmdd") & ".Text"
    
    
    num = FreeFile
    Open ArqOrigem For Append As #num
    num1 = FreeFile
    Open ArqOrigem1 For Append As #num1

    Print #num1, Now() & " - conectar"
    
    If ConectaMQ(NomeQueueManager.Text, txtFilaLocalDestino.Text) = False Then
        GoTo Trata_Erro
    End If
    
    Print #num1, Now() & " - conectou"
    Dim CONTADOR As Integer
    CONTADOR = 0
    'carrega os dados a serem enviados para o BB
    Call GravarTrace("Processar")
    
    vSQL = "SET NOCOUNT ON EXEC " & txtBancoDados.Text & ".." & txtSelecao.Text
    
    Set rs = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, vSQL, True)
    If Not rs.EOF Then
        Do While Not rs.EOF
            'obtem o registro para envio / registro = registro a ser enviado para o BB
            
            registro = ObtemRemessa(rs(0))
            'EnviaRegistroBB(Registro, Fila de Resposta, Queue Manager de Resposta)
            'registro = "PP10  TESTE DE CERTIFICACAO""        "
            'registro = cifra(registro)
            Print #num1, Now() & " - enviar msg"
            msg_id = EnviaRegistroBB(registro, txtResposta.Text, txtFilaSaidaBB.Text)
            Print #num1, Now() & " - msg enviada"
            Print #num, Now() & " - " & registro & " - " & msg_id
            txtEnviado.Text = Val(txtEnviado.Text) + 1
            'DoEvents
            'grava o msg_id retornado na tabela do mq e altera a situa��o do registro para 'T'
            'par�metros sp (@saida_GTR_id, @situacao_MQ, @situacao, @msg_id, @usuario)
            Print #num1, Now() & " - gravar banco"
            
            vSQL = "EXEC " & txtBancoDados.Text & ".." & txtSPEnvio.Text
            vSQL = vSQL & " " & rs(0)
            
            If (Mid(registro, 1, 2) = "SN") Or (Mid(registro, 1, 2) = "ST") Then vSQL = vSQL & ", 'P', 'T'"
            vSQL = vSQL & ",'" & msg_id & "', '" & cUserName & "'"
            
            boleano = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, vSQL, False)
            
            Print #num1, Now() & " - gravado banco"
            
            CONTADOR = CONTADOR + 1
            'If CONTADOR = 50 Then MsgBox "50"
            'atualiza contador
            rs.MoveNext
         
        Loop
        Close #num
        Close #num1
        FechaObjetosMQSeries
        
        'Anderson Fiuza FLOW:2508013 24/11/2009 Adicionado o fechamento da conex�o.

        Timer1.Interval = 2000
        Timer1.Enabled = True
        rs.Close
        
        Set rs = Nothing
             
        
        'Processar
        Exit Sub
        
    Else
        Close #num
        Close #num1
        FechaObjetosMQSeries
        Timer1.Interval = 2000
        Timer1.Enabled = True

    End If
    rs.Close
    Set rs = Nothing
    
    'Call FechaObjetosMQSeries
    'Call DesconectaMQSeries
    
    Exit Sub
    Resume
Trata_Erro:
    Close #num1
    Close #num
    MSGERRO = "SMQP0002 - Erro" & Chr(13) & _
              "Fun��o: Processar - Descri��o: " & Err.Description & _
              vbNewLine & vSQL
    
    GravarErroArquivo "SMQP0002"
    Timer1.Interval = 2000
    Timer1.Enabled = True
    FechaObjetosMQSeries

End Sub

Private Function EnviaRegistroBB(registro As String, FilaResposta As String, QMGRResposta As String) As Variant
       
    Dim md As MQMD            ' Message descriptor
    Dim pmo As MQPMO          ' MQPUT message options
    Dim buflen As Long        ' Length of message to be put
    Dim Buffer As String      ' Message to be put
    Dim CompCode As Long      ' Completion code
    Dim Reason As Long        ' Reason code
    Dim i As Integer
    
    On Error GoTo Trata_Erro
    
    ' Setup MQMD and MQPMO to their respective
    ' initial values
    MQMD_DEFAULTS md
    MQPMO_DEFAULTS pmo
    'liberando a op��o de identificador (Olga IBM)
    pmo.Options = MQPMO_SET_IDENTITY_CONTEXT
    
    'Requisitando o envio de COA e COD
    md.Report = MQRO_COA + MQRO_COD
    md.ReplyToQ = FilaResposta
    md.ReplyToQMgr = QMGRResposta
    md.Format = MQFMT_STRING
    
    buflen = Len(registro)
    Buffer = registro
    'envia a mensagem
    MQPUT gHcon, gHobj, md, pmo, buflen, Buffer, CompCode, Reason
    
    'ID da mensagen
    EnviaRegistroBB = ""
    
    'captura o msg_id de retorno
    For i = 0 To 23
        EnviaRegistroBB = EnviaRegistroBB & Right("00" & Hex(md.MsgId.MQByte(i)), 2)
    Next i
    
    If CompCode <> MQCC_OK Then
        MSGERRO = "SMQP0002 - Erro: EnviaRegistroBB - " & Buffer
        Call Err.Raise(ErrCode, , MSGERRO)
        'GravarErroArquivo "SMQP0002"
    End If
    
    Exit Function

Trata_Erro:
    MSGERRO = "SMQP0002 - Erro: EnviaRegistroBB - " & Buffer & Chr(13) & _
    "Erro:" & Err.Description
    
    Call Err.Raise(ErrCode, , MSGERRO)
    
End Function

Private Function ObtemRemessa(prSaida_MQ_ID As String) As String
        
    Dim rsTB        As ADODB.Recordset
    Dim sql         As String
    
    On Error GoTo Trata_Erro
    
    ObtemRemessa = ""
            
    sql = "SELECT reg_cd_tip, registro"
    sql = sql & " FROM " & txtBancoDados.Text & ".." & txtTabelaRegistro.Text & ""
    sql = sql & " WHERE " & txtCampoID.Text & " = " & prSaida_MQ_ID
    sql = sql & " ORDER BY registro_id"
    Set rsTB = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, sql, True)
    
    If Not rsTB.EOF Then
       Do While Not rsTB.EOF
            If Mid(rsTB!registro, 1, 1) = "U" And Len(rsTB!registro) <> 65 Then
                ObtemRemessa = Replace(rsTB!registro, Chr(32), "")
            Else
                ObtemRemessa = ObtemRemessa & rsTB!registro
            End If
          rsTB.MoveNext
       Loop
       
       rsTB.Close
       
       '* ALTERADO POR JUNIOR EM 22/10/2003 - PARA GERAR A REMESSA COM OS DADOS INICIAIS
       If txtBancoDados.Text = "INTERFACE_DADOS_DB" Then
            sql = " SELECT cod_transacao, qtd_registros"
       Else
            sql = " SELECT cod_transacao, qtde_registros"
       End If
       sql = sql & " FROM " & txtBancoDados.Text & ".." & txtTabelaControle.Text & ""
       sql = sql & " WHERE " & txtCampoID.Text & " = " & prSaida_MQ_ID
       Set rsTB = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, sql, True)
       If Not rsTB.EOF Then
          If txtBancoDados.Text = "INTERFACE_DADOS_DB" Then
                ObtemRemessa = rsTB!cod_transacao & Format(rsTB!qtd_registros, "00000") & ObtemRemessa
          Else
                ObtemRemessa = rsTB!cod_transacao & Format(rsTB!qtde_registros, "00000") & ObtemRemessa
          End If
       End If
       ObtemRemessa = Replace(ObtemRemessa, Chr(9), "")
       
    End If
    rsTB.Close
    
    Exit Function
    
Trata_Erro:
    MSGERRO = "SMQP0002 - Erro" & Chr(13) & _
              "Fun��o: ObtemRemessa - Descri��o: " & Err.Description & _
              vbNewLine & sql
    
    Call Err.Raise(ErrCode, , MSGERRO)

End Function

Public Function ConectaMQ(NomeQueueManager As String, Fila As String) As Boolean
    On Error GoTo Trata_Erro
    Dim CompCode As Long      ' Completion code
    Dim Reason As Long        ' Reason code
    Dim O_options As Long     ' MQOPEN options
    Dim od As MQOD            ' Object descriptor
    
    ConectaMQ = True
    
    Call GravarTrace("Conecta MQ")
  
    If gHcon = 0 Then
        '================================================
        '  1 - Abre o Queue Manager
        '================================================
        MQCONN NomeQueueManager, gHcon, CompCode, Reason
        
        ' CompCode = MQCC_WARNING, then queue manager was already connected
        ' CompCode = MQCC_OK, then queue manager connected OK
        ' otherwise it's an error
        If CompCode = MQCC_FAILED Then
            MSGERRO = "SMQP0002 - Erro abrindo conex�o com o WebSphere MQ" & Chr(13) & "Reason: " & Reason
            ConectaMQ = False
            Call Err.Raise(ErrCode, , MSGERRO)
            
        End If
    End If

  ' Open the queue specified in the Queue textbox
  ' If the connect was successful
    If gHcon Then
        MQOD_DEFAULTS od
        'FILA LOCAL DE DESTINO (N�O � UTILIZADO A FILA REMOTA)
        od.ObjectName = Fila
        od.ObjectQMgrName = txtQueueManagerDestino.Text
        
        'Message Descriptor
        'MQRO_COD MQRO_COA
        O_options = MQOO_OUTPUT + MQOO_SET_IDENTITY_CONTEXT
        
        '================================================
        '  2 - Abre a Fila
        '================================================
        MQOPEN gHcon, od, O_options, gHobj, CompCode, Reason
        
        
        If CompCode <> MQCC_OK Then
            MSGERRO = "SMQP0002 - Erro abrindo MQ"
            
            'enviarMSG
            ConectaMQ = False
            If Reason = 2085 Then
                MSGERRO = "SMQP0002 - Erro ao tentar conectar com o MQ" & Chr(13) & _
                "HINT: Check the spelling of the Queue name, which is Case-Sensitive. " & Chr(13) & _
                "ReasonCode = (2085) - MQRC_UNKNOWN_OBJECT_NAME"
                Call Err.Raise(ErrCode, , MSGERRO)
            End If
            
            If Reason = 2087 Then
                MSGERRO = "SMQP0002 - Erro ao tentar conectar com o MQ" & Chr(13) & _
                "HINT: Check the spelling of the Queue Manager name, which is Case-Sensitive." & Chr(13) & _
                "ReasonCode = (2087) - MQRC_REMOTE_Q_MGR"
                Call Err.Raise(ErrCode, , MSGERRO)
            End If
            Call Err.Raise(ErrCode, , MSGERRO)
        End If
    End If
    Exit Function
    
Trata_Erro:
    MSGERRO = "SMQP0002 - Erro na fun��o ConectaMQ" & Chr(13) & _
              "Gerenciador: " & NomeQueueManager & Chr(13) & _
              "Fila: " & Fila & Chr(13) & _
              "ERRO: " & Err.Description
              
    Call Err.Raise(ErrCode, , MSGERRO)
End Function

Sub FechaObjetosMQSeries()
    
    Dim C_options As Long     ' MQCLOSE options
    Dim CompCode As Long      ' Completion code
    Dim Reason As Long        ' Reason code

  ' If queue was opened successfully - then close it
    If gHobj Then
        C_options = 0
        
        '================================================
        '  Fecha a Fila
        '================================================
        MQCLOSE gHcon, gHobj, C_options, CompCode, Reason
        If Reason = 2018 Then Exit Sub
        If CompCode <> MQCC_OK Then
            MSGERRO = "SMQP0002 - Erro fechando o objeto"
            enviarMSG "SMQP0002"
        End If
    End If
    
 End Sub

Public Sub DesconectaMQSeries()
    
    Dim CompCode As Long      ' Completion code
    Dim Reason As Long        ' Reason code
    
    ' If queue manager connected successfully - then disconnect
    If gHcon Then
        '================================================
        '  Desconecta do MQ
        '================================================
        MQDISC gHcon, CompCode, Reason
    End If
    
    gHcon = 0
    
End Sub

Public Sub carregaCombo()
    
    With Me.cboAmbiente
        .AddItem ("00 - TESTE"), 0
        .AddItem ("01 - TESTE"), 1
        .AddItem ("02 - Produ��o"), 2
        .AddItem ("03 - Qualidade"), 3
        .AddItem ("04 - Desenvolvimento"), 4
        .ListIndex = glAmbiente_id
    End With
    
End Sub

Private Sub mnuAbrir_Click()
        'Exibe o Programa
        Me.WindowState = vbNormal
        Me.Show
        Me.SetFocus
End Sub

Private Sub mnuSair_Click()
        'Finaliza o Sistema
        'Call TerminaSEGBR
        End
End Sub

Private Sub Timer1_Timer()
On Error GoTo Erro
    Timer1.Enabled = False
    'genjunior - log de execu��o
    Call LogExecution(1, "SMQP0002", "N")
    Processar
    Call LogExecution(2, "SMQP0002", "N")
    Timer1.Enabled = True
Erro:
    Timer1.Enabled = True
End Sub

'GENJUNIOR - M�TODO DE LOG DE EXECU��O
Public Sub LogExecution(ByVal tpLog As Integer, ByVal ServiceName As String, ByVal tpABS As String)

Dim bool As Boolean
Dim sql As String

On Error GoTo TrataErro

sql = ""
sql = sql & " EXEC INTERFACE_DADOS_DB.DBO.SMQS0133_SPU " & tpLog & ", '" & ServiceName & "','" & tpABS & "' " & vbNewLine

bool = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, sql, False)

Exit Sub

TrataErro:

MSGERRO = "SMQP0002 - Erro na Sub LogExecution!" & _
    vbNewLine & Err.Description

    GravarErroArquivo "SMQP0002"
End Sub
