Imports System.ComponentModel
Imports System.Configuration.Install
Imports System.Configuration
Imports System.Reflection

Public Class Installer1
    Public Sub New()
        MyBase.New()

        'This call is required by the Component Designer.
        InitializeComponent()
        ServiceInstaller1.ServiceName = GetConfigurationValue("ServiceName")
        'Add initialization code after the call to InitializeComponent

    End Sub

    Public Function GetConfigurationValue(ByVal key As String) As String
        Dim service As Assembly = Assembly.GetAssembly(GetType(Installer1))
        Dim config As Configuration = ConfigurationManager.OpenExeConfiguration(service.Location)

        Return config.AppSettings.Settings(key).Value
    End Function

End Class
