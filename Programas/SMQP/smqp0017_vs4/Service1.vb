Imports System.Configuration
Imports IBM.WMQ
Imports Alianca.Seguranca.BancoDados
Imports System.Reflection
Imports System.Data.SqlClient
Imports System.Threading
Imports SMQL0001

Public Class Service1
    Dim configs As Dictionary(Of String, String)
    Dim doneList As New List(Of ManualResetEvent)
    Dim tmr As System.Timers.Timer

#Region "Construtor"
    Public Sub New()
        InitializeComponent()
        Me.ServiceName = ConfigurationManager.AppSettings.Get("ServiceName")

        TraceFile.Configurar("C:\log_mq\services\Trace", Me.ServiceName, True)
        ErrorFile.Configurar("C:\log_mq\services\Erro", Me.ServiceName, True)
        TraceFile.InserirDivis�o("Servico Iniciado")

        Try
            'Configurando conex�o com o banco
            If Not cCon.configurado Then 'Verificando se existe conex�o com o banco
                If ConfigurationManager.AppSettings.Get("AmbienteBD") = 2 Then
                    cCon.ConfiguraConexao(Assembly.GetExecutingAssembly(), cCon.Ambientes.Produ��o)
                    cCon.ConnectionTimeout = cCon.TimeTimeout.Infinito
                    configs = Helper.GetServicesConfigs(ConfigurationManager.AppSettings("ServiceName"), ConfigurationManager.AppSettings("AmbienteBD"))
                    If configs.Count = 0 Then
                        Throw New Exception("N�o foi possivel recuperar as configura��es do servi�o.")
                    End If
                    TraceFile.Configurar(configs("TraceFilePath"), configs("ServiceName"), configs("Trace"))
                    TraceFile.Save("Ambiente: " + configs("AmbienteBD"))
                    TraceFile.Save("Status: Conectado")
                Else
                    cCon.ConfiguraConexao(Assembly.GetExecutingAssembly(), cCon.Ambientes.Qualidade)
                    cCon.ConnectionTimeout = cCon.TimeTimeout.Infinito
                    configs = Helper.GetServicesConfigs(ConfigurationManager.AppSettings("ServiceName"), ConfigurationManager.AppSettings("AmbienteBD"))
                    If configs.Count = 0 Then
                        Throw New Exception("N�o foi possivel recuperar as configura��es do servi�o.")
                    End If
                    TraceFile.Configurar(configs("TraceFilePath"), configs("ServiceName"), configs("Trace"))
                    TraceFile.Save("Ambiente: " + configs("AmbienteBD"))
                    TraceFile.Save("Status: Conectado")
                End If
            End If

        Catch ex As Exception
            TraceFile.Erro("Falha ao configurar servi�o")
            ErrorFile.Save("Falha ao configurar servi�o")
            ErrorFile.SaveException(ex)
        End Try
    End Sub
#End Region

#Region "M�todos"

#Region "OnStart"
    Protected Overrides Sub OnStart(ByVal args() As String)
        Try
            TraceFile.InserirDivis�o("Inicio - OnStart")
            TraceFile.Save("Ligando o timer...")
            TraceFile.Save("Tempo: " + configs("Timer"))
            tmr = New System.Timers.Timer(configs("Timer"))
            tmr.Enabled = True
            tmr.Start()
            AddHandler tmr.Elapsed, AddressOf OnTimerEvent

            TraceFile.Save("Timer ligado com sucesso...")
            TraceFile.InserirDivis�o("Fim - OnStart")
        Catch ex As Exception
            TraceFile.Erro("N�o foi possivel iniciar o servi�o.")
            ErrorFile.SaveException(ex)
        End Try
    End Sub
#End Region

#Region "OnStop"
    Protected Overrides Sub OnStop()
        Try
            TraceFile.InserirDivis�o("Inicio - OnStop")
            tmr.Stop()
            TraceFile.Save("Timer parado")
            TraceFile.Save("Servi�o parado")
            TraceFile.InserirDivis�o("Fim - OnStop")
        Catch ex As Exception
            TraceFile.Erro("N�o foi possivel parar o servi�o.")
            ErrorFile.SaveException(ex)
        End Try
    End Sub
#End Region

#Region "OnTimerEvent"
    Public Sub OnTimerEvent(ByVal source As Object, ByVal e As System.Timers.ElapsedEventArgs)
        Try
            TraceFile.InserirDivis�o("Inicio - OnTimerEvent")

            'Configurando conex�o com o banco
            TraceFile.Save("Verificando se existe conexao com o Banco de Dados...")
            Try
                If Not cCon.configurado Then
                    TraceFile.Save("N�o existe conex�o com o banco de dados...conectando...")
                    If ConfigurationManager.AppSettings.Get("AmbienteBD") = 2 Then
                        TraceFile.Save("Ambiente: Produ��o")
                        cCon.ConfiguraConexao(Assembly.GetExecutingAssembly(), cCon.Ambientes.Produ��o)
                        cCon.ConnectionTimeout = cCon.TimeTimeout.Infinito
                        TraceFile.Save("Conex�o estabelecida com sucesso")
                    Else
                        TraceFile.Save("Ambiente: Qualidade")
                        cCon.ConfiguraConexao(Assembly.GetExecutingAssembly(), cCon.Ambientes.Qualidade)
                        cCon.ConnectionTimeout = cCon.TimeTimeout.Infinito
                        TraceFile.Save("Conex�o estabelecida com sucesso")
                    End If
                End If
            Catch ex As Exception
                TraceFile.Erro("N�o foi poss�vel estabelecer a conex�o...")
                ErrorFile.SaveException(ex)
            End Try

            TraceFile.Save("Verificando se existe conex�o com o banco de dados...")
            If cCon.configurado() Then
                TraceFile.Save("Status da Conex�o: True")

                'Verifica se no hor�rio atual � permitido 
                TraceFile.Save("Verificando hor�rio de execu��o do servi�o...")
                If Helper.ValidaHorario(Me.configs("TimeStart"), Me.configs("TimeEnd")) Then
                    TraceFile.Save("Inicio: " + Me.configs("TimeStart"))
                    TraceFile.Save("Fim: " + Me.configs("TimeEnd"))
                    TraceFile.Save("Hor�rio de execu��o permitido")

                    TraceFile.Save("Verificando o servi�o a ser executado...")
                    Dim Servico As Integer = Me.configs("TipoServico")
                    TraceFile.Save("Servi�o do tipo: " + Me.configs("TipoServico"))
                    TraceFile.Save("Iniciando Processamento...")

                    Me.LogExecution(1, ServiceName, "N")
                    Processamento()
                    Me.LogExecution(2, ServiceName, "N")
                    TraceFile.Save("Finalizando Processamento...")
                End If
            Else
            TraceFile.Save("Status da Conex�o: False")
            TraceFile.Save("Aguardando pr�xima execu��o para reconectar...")
            End If

            TraceFile.InserirDivis�o("Fim - OnTimerEvent")
        Catch ex As Exception
            TraceFile.Erro("Falha na execu��o do servi�o.")
            ErrorFile.SaveException(ex)
        End Try
    End Sub
#End Region

#Region "Processamento"

    Public Sub Processamento()
        'Para o timer para realizar a importa��o
        TraceFile.InserirDivis�o("Inicio - Processamento")
        TraceFile.Save("Parando timer...")
        Me.tmr.Stop()
        TraceFile.Save("Timer parado com sucesso...")

        TraceFile.Save("Executando procedure 'interface_dados_db..ProcessaFilaMQ_spi'...")
        TraceFile.Save("@GrupoEventoId: " + Me.configs("grupo_evento_als_id"))
        TraceFile.Save("@QtdeReg: " + Me.configs("QtdRegistrosParaProcessar"))
        TraceFile.Save("@usuario: SMQP")

        Try
            Dim params As SqlParameter() = New SqlParameter() { _
                                                                New SqlParameter("@GrupoEventoId", Integer.Parse(Me.configs("grupo_evento_als_id"))), _
                                                                New SqlParameter("@QtdeReg", Integer.Parse(Me.configs("QtdRegistrosParaProcessar"))), _
                                                                New SqlParameter("@usuario", "SMQP") _
                                                                }
            cCon.InternalExecuteNonQuery(CommandType.StoredProcedure, "interface_dados_db..ProcessaFilaMQ_spi", params)
            TraceFile.Save("Procedure executada com sucesso.")
        Catch ex As Exception
            TraceFile.Erro("Falha no m�todo Processamento")
            ErrorFile.SaveException(ex)
        End Try

        'Inicia o timer novamente
        TraceFile.Save("Iniciando timer...")
        Me.tmr.Start()
        TraceFile.Save("Timer iniciado com sucesso...")
        TraceFile.InserirDivis�o("Fim - Processamento")
    End Sub

#End Region


#Region "LogExecution"
    Public Sub LogExecution(ByVal tpLog As Integer, ByVal serviceName As String, ByVal abs As String)
        Try

            Dim params As SqlParameter() = New SqlParameter() { _
                                                                New SqlParameter("@TP_LOG", tpLog), _
                                                                New SqlParameter("@NOME_SERVICO", serviceName), _
                                                                New SqlParameter("@ABS", abs) _
                                                                }
            cCon.InternalExecuteNonQuery(CommandType.StoredProcedure, "INTERFACE_DADOS_DB..SMQS0133_SPU", params)
        Catch ex As Exception
            TraceFile.Save("Classe: Service")
            TraceFile.Save("M�todo: LogExecution")
            TraceFile.Erro("Falha ao salvar log de execucao.")
            ErrorFile.SaveException(ex)
        End Try
    End Sub
#End Region

#End Region

End Class