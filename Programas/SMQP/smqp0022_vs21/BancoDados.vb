Namespace ObjetoBancoDados
    Public Class StoredProcedures
        Public Const SP_OBTER_PARAMETROS_SPS = "controle_sistema_db..obter_parametros_sps"
        Public Const SP_VALOR_AMBIENTE_SPS = "controle_sistema_db..valor_ambiente_sps"

        Public Const SP_LOG_ERRO_SPI = "controle_sistema_db..log_erro_spi"
    End Class
End Namespace
