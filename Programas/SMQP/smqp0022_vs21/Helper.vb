Imports System.IO
Imports Alianca.Seguranca.BancoDados
Imports System.Data.SqlClient
Imports System.Reflection

Public Class Helper

#Region "Lista de arquivos a serem importados"
    Public Shared Function ListaArquivos() As List(Of FileInfo)
        Dim filesList As New List(Of FileInfo)
        Dim param As New SqlParameter("@nome_layout", "ALS470")


        Dim caminhoArquivo As String = cCon.ExecuteScalar(CommandType.StoredProcedure, "interface_db..SMQS0054_SPS", param)
        caminhoArquivo = "\\10.0.35.107\users\temporario IT\JOAO\1152"

        Dim directory As New DirectoryInfo(caminhoArquivo)
        Dim filesTxt() As FileInfo = directory.GetFiles()



        filesList.AddRange(filesTxt)
        'tira os arquivos que não quero processar
        'filesList.Remove(filesList.Item(0))
        'filesList.Remove(filesList.Item(1))
        'filesList.Remove(filesList.Item(1))
        'filesList.Remove(filesList.Item(1))
        'filesList.Remove(filesList.Item(1))
        'filesList.Remove(filesList.Item(1))

        Return filesList
    End Function
#End Region

#Region "Busca última sequencia de remessa"
    Public Shared Function UltimaRemessa() As String
        'Busca ultimo arquivo salvo
        Dim sql = "SET NOCOUNT ON SELECT max(SEQ_REM)  FROM interface_dados_db..CONTROLE_ARQUIVO_CONTRATO_TB with (nolock)"
        Dim ultimaRem As Integer = cCon.ExecuteScalar(CommandType.Text, sql)
        UltimaRemessa = "ALS470." & (ultimaRem + 1).ToString("0000")

    End Function

#End Region

#Region "GravaErro"
    'Grava os erros na tabela controle_sistema_db..log_processamento_tarefa_tb com a sigla_tarefa = "SMQP0022"
    Public Shared Sub GravaErro(ByVal descricaoErro As String, ByVal ambiente As Integer)

        If Not cCon.configurado Then
            If ambiente = 0 Then
                cCon.ConfiguraConexao(Assembly.GetExecutingAssembly(), cCon.Ambientes.Qualidade)
                cCon.ConnectionTimeout = cCon.TimeTimeout.Infinito
            Else
                cCon.ConfiguraConexao(Assembly.GetExecutingAssembly(), ambiente)
                cCon.ConnectionTimeout = cCon.TimeTimeout.Infinito
            End If

        End If

        Dim params As SqlParameter() = New SqlParameter() { _
                                                                                New SqlParameter("@sigla_tarefa", "SMQP0022"), _
                                                                                New SqlParameter("@dt_agenda", DateTime.Now), _
                                                                                New SqlParameter("@atividade", descricaoErro), _
                                                                                New SqlParameter("@horario_inicio_tarefa", DateTime.Now), _
                                                                                New SqlParameter("@horario_inicio_atividade", DateTime.Now), _
                                                                                New SqlParameter("@usuario", "SMQP0022") _
                                                                                }
        cCon.ExecuteNonQuery(CommandType.StoredProcedure, "controle_sistema_db..SEGS8602_SPI", params)
    End Sub
#End Region

End Class
