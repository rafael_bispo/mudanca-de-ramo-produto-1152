Imports System.Configuration
Imports System.IO
Imports Alianca.Seguranca.BancoDados
Imports System.Reflection
Imports Alianca.Sistema.Funcoes

Public Class Principal
    Dim erro As Boolean = False
    Shared _arquivosErro As Integer = 0
    Shared _arquivosProcessados As Integer = 0
    Shared _AmbienteId As Integer = 3
    Public Shared indice As Integer = 1
    Public Shared producao As Object = Nothing
    Public arrParm() As String 'Array que vai receber os parametros do Control-M Fabio 18/02/2013
    Public CTM As Boolean ' Variavel que determina se foi executado pelo Control-M Fabio 18/02/2013
    Public CTMArquivoLog As String ' Variavel que grava o log se foi executado pelo Control-M Fabio 18/02/2013

#Region "Propriedades"
    Public ReadOnly Property StartupName() As String
        Get
            Return IO.Path.GetFileName(Application.ExecutablePath)
        End Get
    End Property
    Property ArquivosErro() As Integer
        Get
            Return _arquivosErro
        End Get
        Set(ByVal value As Integer)
            _arquivosErro = value
        End Set
    End Property

    Property AmbienteId() As Integer
        Get
            Return _AmbienteId
        End Get
        Set(ByVal value As Integer)
            _AmbienteId = value
        End Set
    End Property
    Property ArquivosProcessados() As Integer
        Get
            Return _arquivosProcessados
        End Get
        Set(ByVal value As Integer)
            _arquivosProcessados = value
        End Set
    End Property
#End Region

#Region "Construtor"
    Public Sub New()
        InitializeComponent()
    End Sub
#End Region

#Region "Load"
    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        ' Try


        'Helper.GravaErro("Programa Iniciado", AmbienteId)
        'producao = CreateObject("Producao.ClasseProducao")
        'Helper.GravaErro("DLL Instanciada", cCon.Ambiente)

        ''Verifica se foi executado pelo Control-M
        'CTM = Verifica_Origem_ControlM(Command)

        'Dim lagenda_diaria_id As Long

        'lagenda_diaria_id = TrataParametros.Obtem_agenda_diaria_id(Command)
        'Helper.GravaErro("lagenda_diaria_id =" + lagenda_diaria_id.ToString(), cCon.Ambiente)
        'Helper.GravaErro("Comando: " & Microsoft.VisualBasic.Command, cCon.Ambiente)

        ''Pula se for executado pelo control-m
        'If Verifica_Origem_ControlM(Command) = False Then
        '    Helper.GravaErro("Tratando parametros", cCon.Ambiente)
        '    If Not TrataParametros.Trata_Parametros(Microsoft.VisualBasic.Command, System.Reflection.Assembly.GetExecutingAssembly) Then
        '        Helper.GravaErro("Erro no TrataParametros", cCon.Ambiente)
        '        Helper.GravaErro("Finalizando aplicacao", cCon.Ambiente)

        '        indice = indice + 1
        '        producao.AdicionaLog(1, "Comando: " & Microsoft.VisualBasic.Command, indice)

        '        indice = indice + 1
        '        producao.AdicionaLog(1, "Erro na funcao TrataParametros, a aplica��o sera finalizada.", indice)

        '        Application.Exit()
        '    End If
        'End If

        'If Process.GetProcessesByName(Process.GetCurrentProcess.ProcessName).Length > 1 Then

        '    indice = indice + 1
        '    producao.AdicionaLog(1, "J� existe uma inst�ncia da aplica��o sendo executada no Autoprod.", indice)
        '    Helper.GravaErro("J� existe uma inst�ncia da aplica��o sendo executada no Autoprod.", cCon.Ambiente)

        '    'grava log arquivo CTM
        '    If CTM = True Then
        '        Call GravaLogCTM("OK - ", "J� existe uma inst�ncia da aplica��o sendo executada no Autoprod.", "", indice)
        '    End If

        '    Application.Exit()
        'End If

        'If lagenda_diaria_id > 0 Or CTM = True Then
        '    indice = indice + 1
        '    Helper.GravaErro("InicializaParametrosExecucaoBatch - Configurando execucao", cCon.Ambiente)
        '    producao.AdicionaLog(1, "InicializaParametrosExecucaoBatch - Configurando execucao", indice)

        '    'grava log arquivo CTM
        '    If CTM = True Then
        '        Call GravaLogCTM("OK - ", "InicializaParametrosExecucaoBatch - Configurando execucao", "", indice)
        '    End If

        '    Me.InicializaParametrosExecucaoBatch(Me)
        '    indice = indice + 1
        '    Helper.GravaErro("InicializaParametrosExecucaoBatch - Parametros configurados", cCon.Ambiente)
        '    producao.AdicionaLog(1, "InicializaParametrosExecucaoBatch - Parametros configurados", indice)


        '    'grava log arquivo CTM
        '    If CTM = True Then
        '        Call GravaLogCTM("OK - ", "InicializaParametrosExecucaoBatch - Parametros configurados", "", indice)
        '    End If

        '    indice = indice + 1
        '    Helper.GravaErro("Inicia Processamento automatico", cCon.Ambiente)
        '    producao.AdicionaLog(1, "Inicia Processamento automatico", indice)

        '    'grava log arquivo CTM
        '    If CTM = True Then
        '        Call GravaLogCTM("OK - ", "Inicia Processamento automatico", "", indice)
        '    End If

        IniciaExecucao(True)

        '    indice = indice + 1
        '    Helper.GravaErro("Fim do Processamento automatico", cCon.Ambiente)
        '    producao.AdicionaLog(1, "Fim do Processamento automatico", indice)

        '    'grava log arquivo CTM
        '    If CTM = True Then
        '        Call GravaLogCTM("OK - ", "Fim do Processamento automatico", "", indice)
        '    End If

        '    producao.Finaliza()

        '    Helper.GravaErro("Finalizando aplicacao", cCon.Ambiente)
        '    Application.Exit()

        '    Else

        '    indice = indice + 1
        '    Helper.GravaErro("Inicio do processamento manual", cCon.Ambiente)
        '    producao.AdicionaLog(1, "Inicio do processamento manual", indice)

        '    IniciaExecucao(False)

        '    Helper.GravaErro("Fim do processamento manual", cCon.Ambiente)

        '    Helper.GravaErro("Finalizando aplicacao", cCon.Ambiente)
        '    Application.Exit()

        '    End If

        'Catch ex As Exception
        '    Helper.GravaErro("M�todo: Form1_Load ---" + ex.Message, cCon.Ambiente)

        '    If Not ex.InnerException Is Nothing Then
        '        Helper.GravaErro("M�todo: Form1_Load ---" + ex.InnerException.Message, cCon.Ambiente)
        '    End If

        '    erro = True
        '    indice = indice + 1
        '    producao.AdicionaLog(1, "ERRO - Form1_Load: " + ex.InnerException.Message.ToString(), indice)

        '    'grava log arquivo CTM
        '    If CTM = True Then
        '        Call GravaLogCTM("NOTOK - ", "ERRO - Form1_Load: " + ex.InnerException.Message.ToString(), "", indice)
        '    End If

        'End Try
    End Sub
#End Region

#Region "IniciaExecucao"
    Public Sub IniciaExecucao(ByVal inicia As Boolean)
        Try
            'Helper.GravaErro("Verificando conex�o com a base de dados", cCon.Ambiente)
            If Not cCon.configurado Then 'Verificando se existe conex�o com o banco
                'Descomentar para qualidade
                'Helper.GravaErro("Realizando conex�o com a base de dados", cCon.Ambiente)
                cCon.ConfiguraConexao(Assembly.GetExecutingAssembly(), cCon.Ambientes.Qualidade)
                ' cCon.ConfiguraConexao(Assembly.GetExecutingAssembly(), cCon.Ambientes.Produ��o)

                cCon.ConnectionTimeout = cCon.TimeTimeout.Infinito

                'Helper.GravaErro("Conex�o realizada com sucesso", cCon.Ambiente)

                indice = indice + 1
                'producao.AdicionaLog(1, "Conectado em produ��o", indice)
                'grava log arquivo CTM
                If CTM = True Then
                    Call GravaLogCTM("OK - ", "Conectado em produ��o", "", indice)
                End If
            End If

            If inicia Then
                ' Helper.GravaErro("Execu��o automatica: " & inicia.ToString(), cCon.Ambiente)

                indice = indice + 1
                'producao.AdicionaLog(1, "Execu��o automatica: " & inicia.ToString(), indice)
                'grava log arquivo CTM
                'If CTM = True Then
                'Call GravaLogCTM("OK - ", "Execu��o automatica: " & inicia.ToString(), "", indice)
                'End If
                btnIniciar.Enabled = False
                ImportacaoArquivo()
            End If

        Catch ex As Exception
            erro = True

            Helper.GravaErro("M�todo: IniciaExecucao ---" + ex.Message, cCon.Ambiente)

            If Not ex.InnerException Is Nothing Then
                Helper.GravaErro("M�todo: IniciaExecucao ---" + ex.InnerException.Message, cCon.Ambiente)
            End If

            indice = indice + 1
            producao.AdicionaLog(1, "ERRO IniciaExecucao: " + ex.InnerException.Message.ToString(), indice)

            'grava log arquivo CTM
            If CTM = True Then
                Call GravaLogCTM("NOTOK - ", "ERRO IniciaExecucao: " + ex.InnerException.Message.ToString(), "", indice)
            End If

        End Try
    End Sub
#End Region

#Region "ImportacaoArquivo"
    Public Sub ImportacaoArquivo()
        Try
            'Helper.GravaErro("Iniciando importa��o", cCon.Ambiente)
            indice = indice + 1
            'producao.AdicionaLog(1, "Verificando se existe arquivo para processar", indice)

            'grava log arquivo CTM
            'If CTM = True Then
            'Call GravaLogCTM("OK - ", "Verificando se existe arquivo para processar", "", indice)
            'End If

            'Recupera uma lista com os arquivos contidos na pasta especificada
            'Helper.GravaErro("Procurando arquivos", cCon.Ambiente)
            Dim files As List(Of FileInfo) = Helper.ListaArquivos()

            indice = indice + 1
            'Helper.GravaErro("Arquivos encontrados: " + files.Count.ToString(), cCon.Ambiente)
            'producao.AdicionaLog(1, "Arquivos encontrados: " + files.Count.ToString(), indice)

            'grava log arquivo CTM
            'If CTM = True Then
            'Call GravaLogCTM("OK - ", "Arquivos encontrados: " + files.Count.ToString(), "", indice)
            'End If

            'Realiza a rotina de importa��o para cada arquivo
            'Helper.GravaErro("Processando os arquivos", cCon.Ambiente)
            For Each file As FileInfo In files
                Dim _arquivo As New Arquivo()
                _arquivo.file = file

                indice = indice + 1
                'Helper.GravaErro("Validando se o arquivo � o pr�ximo da sequencia.", cCon.Ambiente)

                If Helper.UltimaRemessa() <> file.Name Then
                    Helper.GravaErro("Arquivo n�o � o pr�ximo da sequ�ncia.", cCon.Ambiente)
                    Helper.GravaErro("Finalizando aplicacao", cCon.Ambiente)

                    indice = indice + 1
                    ' producao.AdicionaLog(1, "Comando: " & Microsoft.VisualBasic.Command, indice)

                    indice = indice + 1
                    'producao.AdicionaLog(1, "Erro na funcao Valida �ltima vers�o, a aplica��o sera finalizada.", indice)

                    'grava log arquivo CTM
                    'If CTM = True Then
                    'Call GravaLogCTM("NOTOK - ", "ERRO - ImportacaoArquivo: Arquivo n�o � o pr�ximo da sequencia.", "", indice)
                    'End If

                    'Application.Exit()
                    Exit Sub


                End If
                'Verifica se o arquivo possui conte�do
                If _arquivo.file.Length > 0 Then

                    indice = indice + 1
                    'Helper.GravaErro("Inicio Importa��o do Arquivo", cCon.Ambiente)
                    'producao.AdicionaLog(1, "Inicio importa��o do Arquivo", indice)

                    'grava log arquivo CTM
                    If CTM = True Then
                        Call GravaLogCTM("OK - ", "Inicio importa��o do Arquivo", "", indice)
                    End If

                    _arquivo.Importar()

                    indice = indice + 1
                    'Helper.GravaErro("Fim importa��o do Arquivo", cCon.Ambiente)
                    ' producao.AdicionaLog(1, "Fim Importa��o do Arquivo", indice)

                    'grava log arquivo CTM
                    'If CTM = True Then
                    '    Call GravaLogCTM("OK - ", "Fim Importa��o do Arquivo", "", indice)
                    'End If

                End If
            Next
        Catch ex As Exception
            '  Helper.GravaErro("M�todo: ImportacaoArquivo ---" + ex.Message, cCon.Ambiente)

            '  If Not ex.InnerException Is Nothing Then
            'Helper.GravaErro("M�todo: ImportacaoArquivo ---" + ex.InnerException.Message, cCon.Ambiente)
            '   End If

            erro = True
            indice = indice + 1
            '  producao.AdicionaLog(1, "ERRO - ImportacaoArquivo: " + ex.InnerException.Message.ToString(), indice)

            'grava log arquivo CTM
            If CTM = True Then
                Call GravaLogCTM("NOTOK - ", "ERRO - ImportacaoArquivo: " + ex.InnerException.Message.ToString(), "", indice)
            End If

        End Try
    End Sub
#End Region

#Region "btnIniciar"
    Private Sub btnIniciar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnIniciar.Click
        Dim horaInicio As TimeSpan = DateTime.Now.TimeOfDay
        txtInicio.Text = horaInicio.ToString()
        lblStatus.Text = "Importando..."
        Me.InicializaParametrosExecucaoBatch(Me)
        ImportacaoArquivo()

        Dim horaFim As TimeSpan = DateTime.Now.TimeOfDay
        txtFim.Text = horaFim.ToString()

        lblTempo.Text = (horaFim - horaInicio).ToString()

        lblStatus.Text = "Importa��o Finalizada"
        producao.Finaliza()
    End Sub
#End Region

#Region "btnFechar"
    Private Sub btnFechar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnFechar.Click
        Application.Exit()
    End Sub
#End Region

#Region "Configura��es para o AutProd"
    Sub InicializaParametrosExecucaoBatch(ByVal PObjeto As Object)

        Try
            producao.Usuario = "SMQP0022"
            'Obtendo no n�mero do processo agendado para o programa. Caso o programa esteja agen-
            'dado para ser executado automaticamente, a fun��o Obtem_agenda_diaria_id retornar�
            'um valor maior que 0 '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Dim lagenda_diaria_id As Long
            lagenda_diaria_id = TrataParametros.Obtem_agenda_diaria_id(Command)
            If lagenda_diaria_id > 0 Then
                'Gravando os dados iniciais do processamento, esta��o, hora de in�cio etc. ''''''''''
                Helper.GravaErro("InicializaParametrosExecucaoBatch: metodo - InicializaAutomatico", cCon.Ambiente)
                If Not producao.InicializaAutomatico(lagenda_diaria_id) Then
                    Application.Exit()
                End If
            Else
                'Gravando dados dados iniciais do processamento esta��o, hora de in�cio etc. ''''''''
                Dim OArquivo
                OArquivo = Application.ExecutablePath & "\" & Me.StartupName
                If Not producao.InicializaManual(OArquivo, Alianca.Sistema.Funcoes.TrataParametros.Data_Sistema) Then
                    Application.Exit()
                End If
            End If
        Catch ex As Exception
            Helper.GravaErro("M�todo: InicializaParametrosExecucaoBatch ---" + ex.Message, cCon.Ambiente)

            If Not ex.InnerException Is Nothing Then
                Helper.GravaErro("M�todo: InicializaParametrosExecucaoBatch ---" + ex.InnerException.Message, cCon.Ambiente)
            End If

            Dim ExceptionPai As Exception
            ExceptionPai = ex
            While Not IsNothing(ExceptionPai)
                MsgBox(ExceptionPai.Message & " - " & ExceptionPai.ToString())
                ExceptionPai = ExceptionPai.InnerException
            End While

        End Try
    End Sub

    Private Function TabTempUsr(ByVal ParamArray OsFormularios()) As String
        Dim sUserName As String = ""
        Dim sMac As String = ""

        If sUserName = "" Then sUserName = "SMQP0022"
        If sMac = "" Then sMac = Alianca.Sistema.Funcoes.TrataParametros.gsMac
        Return Mid("##$" & sUserName & "$" + sMac, 1, 30)
    End Function
#End Region

#Region "Configura��es Control-M"

    '######################################### Separa Argumentos CTM
    Private Function SplitArgs(ByVal sParametros) As String
        Dim sArgs As String
        Dim sChar As String
        Dim nCount As Long
        Dim bQuotes As Boolean
        sArgs = ""
        For nCount = 1 To Len(sParametros)
            sChar = Mid$(sParametros, nCount, 1)
            If sChar = Chr(34) Then
                bQuotes = Not bQuotes
            End If
            If sChar = Chr(32) Then
                If bQuotes Then
                    sArgs = sArgs & sChar
                Else
                    sArgs = sArgs & Chr(0)
                End If
            Else
                If sChar <> Chr(34) Then
                    sArgs = sArgs & sChar
                End If
            End If
        Next
        SplitArgs = sArgs
    End Function

    '######################################### Verifica se foi chamado pelo Control-M FABIO MORETTI 18/02/2013
    Function Verifica_Origem_ControlM(ByVal sParametros As String) As Boolean
        If sParametros <> "" Then
            arrParm = Split(SplitArgs(sParametros), Chr(0))
            If UBound(arrParm) >= 3 Then
                If arrParm(1) = "CTM" Then
                    Verifica_Origem_ControlM = True
                    CTM = True
                    _AmbienteId = Int(arrParm(2))
                    producao.Usuario = arrParm(3)
                    CTMArquivoLog = arrParm(0)
                Else
                    Verifica_Origem_ControlM = False
                    CTM = False
                End If
            Else
                Verifica_Origem_ControlM = False
                CTM = False
            End If
        Else
            Verifica_Origem_ControlM = False
            CTM = False
        End If
    End Function

    'Grava no arquivo de log do Control-M
    Sub GravaLogCTM(ByVal sSts As String, _
                          ByVal sMsg As String, _
                          ByVal sDesc As String, _
                          ByVal sVal As String)

        On Error Resume Next

        If File.Exists(CTMArquivoLog) Then
            Using txt As StreamWriter = New StreamWriter(CTMArquivoLog)
                txt.Write(sSts & " - " & sMsg & " - " & sDesc & " - " & sVal)
            End Using
        Else
            Dim fs As FileStream = Nothing
            fs = File.Create(CTMArquivoLog)
            Using fs
                Using txt As StreamWriter = New StreamWriter(CTMArquivoLog)
                    txt.Write(sSts & " - " & sMsg & " - " & sDesc & " - " & sVal)
                End Using
            End Using
        End If
    End Sub

#End Region


End Class
