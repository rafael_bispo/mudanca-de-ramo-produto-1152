Imports System.IO
Imports System.Text
Imports System.Configuration
Imports System.Data.SqlClient
Imports Alianca.Seguranca.BancoDados
Imports SMQL0001

Public Class Arquivo

#Region "Atributos"
    Dim _file As FileInfo
    Dim _nome_arquivo As String
    Dim _idArquivo As Integer
    Dim _qtdRegistrosTrailer As Integer
#End Region

#Region "Propriedades"
    Property file() As FileInfo
        Get
            Return _file
        End Get
        Set(ByVal value As FileInfo)
            _file = value
        End Set
    End Property

    Property qtdRegistrosTrailer() As Integer
        Get
            Return _qtdRegistrosTrailer
        End Get
        Set(ByVal value As Integer)
            _qtdRegistrosTrailer = value
        End Set
    End Property
#End Region

#Region "M�todos"

#Region "Importar"
    Public Sub Importar()
        Try
            Me._nome_arquivo = _file.Name

            Dim erroTratamento As Boolean = False

            If cCon.configurado Then

                'Cadastra o arquivo na tabela de controle
                Principal.indice = Principal.indice + 1
                ' Helper.GravaErro("ARQUIVO: " + Me._nome_arquivo.ToString(), cCon.Ambiente)
                'Principal.producao.AdicionaLog(1, "ARQUIVO: " + Me._nome_arquivo.ToString(), Principal.indice)

                'Helper.GravaErro("Cadastrando na tabela controle_arquivo_contrato_tb", cCon.Ambiente)
                CadastrarArquivo()
                'Helper.GravaErro("Arquivo cadastrado", cCon.Ambiente)

                'Helper.GravaErro("Validando arquivo", cCon.Ambiente)
                If ValidarArquivo() Then
                    ' Helper.GravaErro("Arquivo validado", cCon.Ambiente)
                    Principal.indice = Principal.indice + 1
                    'Principal.producao.AdicionaLog(1, "Inicio do processamento do arquivo...", Principal.indice)

                    'Helper.GravaErro("Importando para a base", cCon.Ambiente)
                    Processar()
                    'Helper.GravaErro("Importacao realizada", cCon.Ambiente)

                    Principal.indice = Principal.indice + 1
                    'Principal.producao.AdicionaLog(1, "Fim do processamento do arquivo...", Principal.indice)

                    Principal.indice = Principal.indice + 1
                    'Principal.producao.AdicionaLog(1, "Movendo o arquivo...", Principal.indice)
                    'Helper.GravaErro("Movendo arquivo", cCon.Ambiente)
                    MoverArquivo(False)
                    'Helper.GravaErro("Arquivo movido", cCon.Ambiente)
                    Principal.indice = Principal.indice + 1
                    'Principal.producao.AdicionaLog(1, "Arquivo movido com sucesso...", Principal.indice)
                Else
                    Principal.indice = Principal.indice + 1
                    'Principal.producao.AdicionaLog(1, "Movendo o arquivo...", Principal.indice)
                    'Helper.GravaErro("Movendo arquivo", cCon.Ambiente)
                    MoverArquivo(True)
                    'Helper.GravaErro("Arquivo movido", cCon.Ambiente)
                    Principal.indice = Principal.indice + 1
                    'Principal.producao.AdicionaLog(1, "Arquivo movido com sucesso...", Principal.indice)
                End If
            End If
        Catch ex As Exception
            Helper.GravaErro("M�todo: Importar ---" + ex.Message, cCon.Ambiente)

            If Not ex.InnerException Is Nothing Then
                Helper.GravaErro("M�todo: Importar ---" + ex.InnerException.Message, cCon.Ambiente)
            End If

            Principal.indice = Principal.indice + 1
            Principal.producao.AdicionaLog(1, "ERRO - Importa��o: " + ex.InnerException.Message.ToString(), Principal.indice)

            MoverArquivo(True)
        End Try
    End Sub
#End Region

#Region "Move Arquivo"
    Private Sub MoverArquivo(ByVal erro As Boolean)
        Try
            Dim caminho As String = ""
            Dim nome As String = ""

            If erro = False Then
                Dim param As New SqlParameter("@nome_layout", "ALS470")
                caminho = cCon.ExecuteScalar(CommandType.StoredProcedure, "interface_db..SMQS0056_SPS", param)

                If Not caminho.EndsWith("\") Then
                    caminho = caminho + "\"
                End If

                Me.file.MoveTo(caminho + Me.file.Name)

                Principal.ArquivosProcessados += 1
            Else
                Dim param As New SqlParameter("@nome_layout", "ALS470")
                caminho = cCon.ExecuteScalar(CommandType.StoredProcedure, "interface_db..SMQS0055_SPS", param)

                If Not caminho.EndsWith("\") Then
                    caminho = caminho + "\"
                End If

                Me.file.MoveTo(caminho + Me.file.Name)

                Principal.ArquivosErro += 1
            End If
        Catch ex As Exception
            'TraceFile.Erro("Erro ao mover arquivo.")
            ' ErrorFile.Save("Erro ao mover arquivo.")
            ' ErrorFile.SaveException(ex)
            Principal.indice = Principal.indice + 1
            Principal.producao.AdicionaLog(1, "ERRO - MoverArquivo: " + ex.InnerException.Message.ToString(), Principal.indice)
        End Try
    End Sub
#End Region

#Region "Cadastra arquivo na tabela de controle"

    Public Sub CadastrarArquivo()
        Dim usuario As String = "SMQP0022"

        Dim params As SqlParameter() = New SqlParameter() { _
                                                                    New SqlParameter("@nome_arquivo", _nome_arquivo), _
                                                                    New SqlParameter("@usuario", usuario) _
                                                                    }
        _idArquivo = Convert.ToInt32(cCon.ExecuteScalar(CommandType.StoredProcedure, "interface_dados_db..SMQS0050_SPI", params))
    End Sub

#End Region

#Region "Salva header/trailer"
#Region "Salvar Header"
    Public Sub SalvarHeader(ByVal header As String)

        Dim params As SqlParameter() = New SqlParameter() { _
                                                            New SqlParameter("@entrada_arquivo_contrato_id", _idArquivo), _
                                                            New SqlParameter("@header", header) _
                                                            }

        cCon.ExecuteNonQuery(CommandType.StoredProcedure, "interface_dados_db..SMQS0049_SPU", params)
    End Sub
#End Region

#Region "Salvar Trailer"
    Public Sub SalvarTrailer(ByVal trailer As String)

        Dim params As SqlParameter() = New SqlParameter() { _
                                                            New SqlParameter("@entrada_arquivo_contrato_id", _idArquivo), _
                                                            New SqlParameter("@trailer", trailer) _
                                                            }

        cCon.ExecuteNonQuery(CommandType.StoredProcedure, "interface_dados_db..SMQS0051_SPU", params)
    End Sub
#End Region
#End Region

#Region "Atualiza Qtd Remessas"
    Public Sub UpdateQtdRemessas(ByVal qtdRemessas As Integer)
        Dim params As SqlParameter() = New SqlParameter() { _
                                                                    New SqlParameter("@entrada_arquivo_contrato_id", _idArquivo), _
                                                                    New SqlParameter("@qtd_remessas", qtdRemessas) _
                                                                    }

        cCon.ExecuteNonQuery(CommandType.StoredProcedure, "interface_dados_db..SMQS0052_SPU", params)
    End Sub
#End Region

#Region "Valida��es do arquivo"

#Region "Validar arquivo"
    Public Function ValidarArquivo() As Boolean
        Dim arquivoValido As Boolean = False
        Dim fStream As FileStream = Me._file.Open(FileMode.Open, FileAccess.Read)

        Using reader As New StreamReader(fStream)
            Dim possuiHeader As Boolean = ValidarHeader(reader.ReadLine())
            Dim possuiTrailer As Boolean = ValidarTrailer(reader)

            If Not possuiHeader And Not possuiTrailer Then
                CadastrarErroArquivo(0, _idArquivo)
                arquivoValido = False
            ElseIf Not possuiHeader And possuiTrailer Then
                CadastrarErroArquivo(1, _idArquivo)
                arquivoValido = False
            ElseIf possuiHeader And Not possuiTrailer Then
                CadastrarErroArquivo(2, _idArquivo)
                arquivoValido = False
            Else
                arquivoValido = True
            End If
        End Using

        fStream.Close()

        Return arquivoValido
    End Function
#End Region

#Region "Validar Header"
    Public Function ValidarHeader(ByVal header As String) As Boolean
        Dim identificador As String = header.Substring(0, 10)
        If identificador = "0000000000" Then
            SalvarHeader(header)
            Return True
        Else
            Return False
        End If
    End Function
#End Region

#Region "Validar Trailer"
    Public Function ValidarTrailer(ByVal reader As StreamReader) As Boolean
        Dim trailer As String = ""

        'Niuarque Batista - INC000004571823 - Tratando casos de arquivos acima de 250mb
        'Corre��o para que o processo comece do final do arquivo na busca pelo trailler
        'reader.BaseStream.Position = reader.BaseStream.Length - 200
        reader.BaseStream.Position = Math.Abs(reader.BaseStream.Length * 0.99)

        trailer = reader.ReadLine()
        Dim identificador As String = trailer.Substring(0, 10)

        If identificador = "9999999999" Then
            SalvarTrailer(trailer)
            _qtdRegistrosTrailer = trailer.Substring(13, 9)
            Return True
        Else
            While Not reader.EndOfStream()
                trailer = reader.ReadLine()
                identificador = trailer.Substring(0, 10)
                If identificador = "9999999999" Then
                    SalvarTrailer(trailer)
                    _qtdRegistrosTrailer = trailer.Substring(13, 9)
                    Return True
                End If
            End While

            Return True
        End If
    End Function
#End Region

#Region "Cadastrar Erro no arquivo"
    Public Sub CadastrarErroArquivo(ByVal erro As Integer, ByVal entrada_arquivo_contrato_id As Integer)
        Dim params As SqlParameter() = New SqlParameter() { _
                                                            New SqlParameter("@erro", erro), _
                                                            New SqlParameter("@entrada_arquivo_contrato_id", entrada_arquivo_contrato_id) _
                                                            }

        cCon.ExecuteNonQuery(CommandType.StoredProcedure, "interface_dados_db..SMQS0053_SPU", params)
    End Sub
#End Region

#End Region

#Region "Processar"
    Public Sub Processar()
        Dim params As SqlParameter() = New SqlParameter() { _
                                                                        New SqlParameter("@entrada_arquivo_contrato_id", _idArquivo), _
                                                                        New SqlParameter("@usuario", "SMQP0022"), _
                                                                        New SqlParameter("@debug", 0) _
                                                                        }
        cCon.ExecuteNonQuery(CommandType.StoredProcedure, "desenv_db..SEGS10778_SPI", params)
    End Sub
#End Region

#End Region

End Class



