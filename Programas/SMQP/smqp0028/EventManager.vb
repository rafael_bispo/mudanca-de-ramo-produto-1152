﻿Public Class EventManager
    Public Shared Sub RaiseError(ByVal ex As Exception, ByVal classe As String, ByVal metodo As String)
        Dim stringBuilder As New Text.StringBuilder()
        stringBuilder.Append("Serviço:" + Configuration.ConfigurationManager.AppSettings("ServiceName").ToString())
        stringBuilder.Append(Environment.NewLine)
        stringBuilder.Append("Classe: " + classe)
        stringBuilder.Append(Environment.NewLine)
        stringBuilder.Append("Método: " + metodo)
        stringBuilder.Append(Environment.NewLine)
        stringBuilder.Append("Mensagem de erro: " + ex.Message)
        stringBuilder.Append(Environment.NewLine)
        stringBuilder.Append("Método: " + metodo)
        stringBuilder.Append(Environment.NewLine)
        If Not ex.InnerException Is Nothing Then
            stringBuilder.Append("InnerException: ")
            stringBuilder.Append(Environment.NewLine)
            stringBuilder.Append(ex.InnerException.ToString())
            stringBuilder.Append(Environment.NewLine)
        End If
        stringBuilder.Append("StackTrace: ")
        stringBuilder.Append(Environment.NewLine)
        stringBuilder.Append(ex.StackTrace)

        Dim _Eventlog = New EventLog
        _Eventlog.Source = Configuration.ConfigurationManager.AppSettings("ServiceName").ToString()
        _Eventlog.WriteEntry(stringBuilder.ToString(), EventLogEntryType.Error)

    End Sub

    Public Shared Sub RaiseError(ByVal ex As Exception, ByVal classe As String, ByVal metodo As String, ByVal complemento As String)
        Dim stringBuilder As New Text.StringBuilder()
        stringBuilder.Append("Serviço:" + Configuration.ConfigurationManager.AppSettings("ServiceName").ToString())
        stringBuilder.Append(Environment.NewLine)
        stringBuilder.Append("Classe: " + classe)
        stringBuilder.Append(Environment.NewLine)
        stringBuilder.Append("Método: " + metodo)
        stringBuilder.Append(Environment.NewLine)
        stringBuilder.Append("Mensagem de erro: " + ex.Message)
        stringBuilder.Append(Environment.NewLine)
        stringBuilder.Append("Método: " + metodo)
        stringBuilder.Append(Environment.NewLine)
        stringBuilder.Append("Informações adicionais: " + complemento)
        stringBuilder.Append(Environment.NewLine)
        If Not ex.InnerException Is Nothing Then
            stringBuilder.Append("InnerException: ")
            stringBuilder.Append(Environment.NewLine)
            stringBuilder.Append(ex.InnerException.ToString())
            stringBuilder.Append(Environment.NewLine)
        End If
        stringBuilder.Append("StackTrace: ")
        stringBuilder.Append(Environment.NewLine)
        stringBuilder.Append(ex.StackTrace)

        Dim _Eventlog = New EventLog()
        _Eventlog.Source = Configuration.ConfigurationManager.AppSettings("ServiceName").ToString()
        _Eventlog.WriteEntry(stringBuilder.ToString(), EventLogEntryType.Error)

    End Sub

End Class
