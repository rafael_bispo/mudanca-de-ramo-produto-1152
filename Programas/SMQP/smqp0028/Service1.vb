Imports System.Configuration
Imports Alianca.Seguranca.BancoDados
Imports System.Reflection
Imports System.Data.SqlClient
Imports System.Threading
Imports SMQL0001

Public Class Service1
    Dim configs As Dictionary(Of String, String)
    Dim doneList As New List(Of ManualResetEvent)
    Dim tmr As System.Timers.Timer
    Dim bUsarConexaoABS As Char

#Region "Construtor"
    Public Sub New()
        InitializeComponent()
        Me.ServiceName = ConfigurationManager.AppSettings.Get("ServiceName")

        TraceFile.Configurar("C:\log_mq\services\Trace", Me.ServiceName, True)
        ErrorFile.Configurar("C:\log_mq\services\Erro", Me.ServiceName, True)
        TraceFile.InserirDivis�o("Servico Iniciado")

        Try
            'Configurando conex�o com o banco
            If Not cCon.configurado Then 'Verificando se existe conex�o com o banco
                If ConfigurationManager.AppSettings.Get("AmbienteBD") = 2 Then
                    cCon.ConfiguraConexao(Assembly.GetExecutingAssembly(), cCon.Ambientes.Produ��o)
                    cCon.ConnectionTimeout = cCon.TimeTimeout.Infinito
                    configs = Helper.GetServicesConfigs(ConfigurationManager.AppSettings("ServiceName"), ConfigurationManager.AppSettings("AmbienteBD"))
                    If configs.Count = 0 Then
                        Throw New Exception("N�o foi possivel recuperar as configura��es do servi�o.")
                    End If
                    TraceFile.Configurar(configs("TraceFilePath"), configs("ServiceName"), configs("Trace"))
                    TraceFile.Save("Ambiente: " + configs("AmbienteBD"))
                    TraceFile.Save("Status: Conectado")
                Else
                    cCon.ConfiguraConexao(Assembly.GetExecutingAssembly(), cCon.Ambientes.Qualidade)
                    cCon.ConnectionTimeout = cCon.TimeTimeout.Infinito
                    configs = Helper.GetServicesConfigs(ConfigurationManager.AppSettings("ServiceName"), ConfigurationManager.AppSettings("AmbienteBD"))
                    If configs.Count = 0 Then
                        Throw New Exception("N�o foi possivel recuperar as configura��es do servi�o.")
                    End If
                    TraceFile.Configurar(configs("TraceFilePath"), configs("ServiceName"), configs("Trace"))
                    TraceFile.Save("Ambiente: " + configs("AmbienteBD"))
                    TraceFile.Save("Status: Conectado")
                End If
            End If
        Catch ex As Exception
            TraceFile.Erro("Falha ao configurar servi�o")
            ErrorFile.Save("Falha ao configurar servi�o")
            ErrorFile.SaveException(ex)
        End Try
    End Sub
#End Region

#Region "M�todos"

#Region "OnStart"
    Protected Overrides Sub OnStart(ByVal args() As String)
        Try
            TraceFile.InserirDivis�o("Inicio - OnStart")
            TraceFile.Save("Ligando o timer...")
            TraceFile.Save("Tempo: " + configs("Timer"))
            tmr = New System.Timers.Timer(configs("Timer"))
            tmr.Enabled = True
            tmr.Start()
            AddHandler tmr.Elapsed, AddressOf OnTimerEvent

            TraceFile.Save("Timer ligado com sucesso...")
            TraceFile.InserirDivis�o("Fim - OnStart")
        Catch ex As Exception
            TraceFile.Erro("N�o foi possivel iniciar o servi�o.")
            ErrorFile.SaveException(ex)
        End Try
    End Sub
#End Region

#Region "OnStop"
    Protected Overrides Sub OnStop()
        Try
            TraceFile.InserirDivis�o("Inicio - OnStop")
            tmr.Stop()
            TraceFile.Save("Timer parado")
            TraceFile.Save("Servi�o parado")
            TraceFile.InserirDivis�o("Fim - OnStop")
        Catch ex As Exception
            TraceFile.Erro("N�o foi possivel parar o servi�o.")
            ErrorFile.SaveException(ex)
        End Try
    End Sub
#End Region

#Region "OnTimerEvent"
    Public Sub OnTimerEvent(ByVal source As Object, ByVal e As System.Timers.ElapsedEventArgs)
        Try
            TraceFile.InserirDivis�o("Inicio - OnTimerEvent")

            'Configurando conex�o com o banco
            TraceFile.Save("Verificando se existe conexao com o Banco de Dados...")
            Try
                If Not cCon.configurado Then
                    TraceFile.Save("N�o existe conex�o com o banco de dados...conectando...")
                    If ConfigurationManager.AppSettings.Get("AmbienteBD") = 2 Then
                        TraceFile.Save("Ambiente: Produ��o")
                        cCon.ConfiguraConexao(Assembly.GetExecutingAssembly(), cCon.Ambientes.Produ��o)
                        cCon.ConnectionTimeout = cCon.TimeTimeout.Infinito
                        TraceFile.Save("Conex�o estabelecida com sucesso")
                    Else
                        TraceFile.Save("Ambiente: Qualidade")
                        cCon.ConfiguraConexao(Assembly.GetExecutingAssembly(), cCon.Ambientes.Qualidade)
                        cCon.ConnectionTimeout = cCon.TimeTimeout.Infinito
                        TraceFile.Save("Conex�o estabelecida com sucesso")
                    End If
                End If
            Catch ex As Exception
                TraceFile.Erro("N�o foi poss�vel estabelecer a conex�o...")
                ErrorFile.SaveException(ex)
            End Try

            TraceFile.Save("Verificando se existe conex�o com o banco de dados...")
            If cCon.configurado() Then
                TraceFile.Save("Status da Conex�o: True")

                'Verifica se no hor�rio atual � permitido 
                TraceFile.Save("Recuperando par�metros da base...")
                configs = Helper.GetServicesConfigs(ConfigurationManager.AppSettings("ServiceName"), ConfigurationManager.AppSettings("AmbienteBD")) '*
                TraceFile.Save("Par�metros recuperados") '*
                TraceFile.Save("Verificando hor�rio de execu��o do servi�o...") '*
                TraceFile.Save("Inicio: " + Me.configs("TimeStart")) '*
                TraceFile.Save("Fim: " + Me.configs("TimeEnd")) '*

                If Helper.ValidaHorario(Me.configs("TimeStart"), Me.configs("TimeEnd")) Then
                   
                    TraceFile.Save("Hor�rio de execu��o permitido")
                    TraceFile.Save("Iniciando processamento dos registros...")
                    TraceFile.Save("Gravando Log do inicio da execu��o")
                    TraceFile.Save("Verificando o servi�o a ser executado...")
                    Dim Servico As Integer = Me.configs("TipoServico")
                    TraceFile.Save("Servi�o do tipo: " + Me.configs("TipoServico"))
                    TraceFile.Save("Iniciando Processamento...")

                    Me.LogExecution(1, ServiceName, "N")
                    ProcessaRegistro()
                    Me.LogExecution(2, ServiceName, "N")
                    TraceFile.Save("Finalizando processamento dos registros...")
                Else
                    TraceFile.Save("Hor�rio de execu��o n�o permitido") '*
                End If


            Else
                TraceFile.Save("Status da Conex�o: False")
                TraceFile.Save("Aguardando pr�xima execu��o para reconectar...")
            End If

            TraceFile.InserirDivis�o("Fim - OnTimerEvent")
        Catch ex As Exception
            TraceFile.Erro("Falha na execu��o do servi�o.")
            ErrorFile.SaveException(ex)
        End Try
    End Sub
#End Region

#Region "Processa Registro"
    Public Sub ProcessaRegistro()
        Try
            TraceFile.InserirDivis�o("Inicio - ProcessaRegistro()")

            tmr.Stop()
            Dim params As SqlParameter() = New SqlParameter() {New SqlParameter("@GrupoEventoId", Integer.Parse(Me.configs("grupo_evento_als_id")))}

            ''Verifica qtd registros ainda n�o processados
            TraceFile.Save("Executando a procedure 'interface_dados_db..segs8491_sps'...")
            Dim totalRegistros As Object = cCon.ExecuteScalar(CommandType.StoredProcedure, "interface_dados_db..segs8491_sps", params)
            Dim qtdRegistrosParaProcessar As Integer = Integer.Parse(configs("QtdRegistrosParaProcessar"))
            TraceFile.Save("Quantidade de registros para processar por lote: " + configs("QtdRegistrosParaProcessar"))

            TraceFile.Save("Executou a stored procedure segs8491_sps. Existem " + totalRegistros.ToString() + " para processar")
            TraceFile.Save("O app.config esta configurado para processar " + qtdRegistrosParaProcessar.ToString() + " por vez")
            TraceFile.Save("Executando a procedure interface_dados_db..segs8494_spi")

            While totalRegistros > 0
                Dim paramsB As SqlParameter() = New SqlParameter() { _
                                                    New SqlParameter("@GrupoEventoId", Integer.Parse(Me.configs("grupo_evento_als_id"))), _
                                                    New SqlParameter("@QtdeReg", Integer.Parse(Me.configs("QtdRegistrosParaProcessar"))), _
                                                    New SqlParameter("@usuario", "SMQP0028"), _
                                                    New SqlParameter("@ambiente_destino_id", 0), _
                                                    New SqlParameter("@usar_conexao_abs", "X"), _
                                                    New SqlParameter("@flag_empresa_eh_abs", "X") _
                                                    }
                Try
                    cCon.InternalExecuteNonQuery(CommandType.StoredProcedure, "interface_dados_db..segs8494_spi", paramsB)
                Catch ex As Exception
                    TraceFile.Erro("Erro na execu��o da PROCEDURE.")
                    ErrorFile.SaveException(ex)
                Finally
                    totalRegistros = totalRegistros - Integer.Parse(Me.configs("QtdRegistrosParaProcessar"))
                End Try
            End While

            TraceFile.Save("Saiu do la�o While")

            tmr.Start()

            TraceFile.Save("Reiniciou o timer")
        Catch ex As Exception
            TraceFile.Erro("Falha no m�todo ProcessaRegistro().")
            ErrorFile.SaveException(ex)
            tmr.Start()
            TraceFile.Save("Reiniciou o timer")
        End Try
    End Sub

#Region "ThreadProcedure"
    'Public Sub ThreadProcedure(ByVal state As Object)

    '    Dim params As SqlParameter() = New SqlParameter() { _
    '                                                New SqlParameter("@GrupoEventoId", Integer.Parse(Me.configs("grupo_evento_als_id"))), _
    '                                                New SqlParameter("@QtdeReg", Integer.Parse(Me.configs("QtdRegistrosParaProcessar"))), _
    '                                                New SqlParameter("@usuario", "SMQP0007"), _
    '                                                New SqlParameter("@ambiente_destino_id", Integer.Parse(Me.configs("AmbienteBD"))), _
    '                                                New SqlParameter("@usar_conexao_abs", bUsarConexaoABS), _
    '                                                New SqlParameter("@flag_empresa_eh_abs", "N") _
    '                                                }
    '    Try
    '        cCon.InternalExecuteNonQuery(CommandType.StoredProcedure, "interface_dados_db..segs8494_spi", params)
    '        doneList(Convert.ToInt32(state)).Set()
    '        TraceFile.Save("Thread n�: " + state.ToString() + " - Finalizada. Processo do windows ID: " + Thread.CurrentThread.ManagedThreadId.ToString())
    '    Catch ex As Exception
    '        doneList(Convert.ToInt32(state)).Set()
    '        TraceFile.Save("Thread n�: " + state.ToString() + " - ERRO POREM FINALIZADA. Processo do windows ID: " + Thread.CurrentThread.ManagedThreadId.ToString())
    '        TraceFile.Erro("Falha na execucao do metodo ThreadProcedure " + state.ToString() + ".")
    '        ErrorFile.SaveException(ex)
    '    End Try

    'End Sub
#End Region

#End Region

#Region "Processa Registro ABS"
    'Public Sub ProcessaRegistroABS()
    '    Try
    '        TraceFile.InserirDivis�o("Executando m�todo ProcessaRegistroABS()")

    '        tmr.Stop()
    '        Dim params As SqlParameter() = New SqlParameter() {New SqlParameter("@GrupoEventoId", Integer.Parse(Me.configs("grupo_evento_als_id")))}

    '        ''Verifica qtd registros ainda n�o processados
    '        Dim totalRegistros As Object = cCon.ExecuteScalar(CommandType.StoredProcedure, "interface_dados_db..segs8491_sps", params)
    '        Dim qtdRegistrosParaProcessar As Integer = Integer.Parse(configs("QtdRegistrosParaProcessar"))

    '        TraceFile.Save("Executou a stored procedure segs8491_sps. Existem " + totalRegistros.ToString() + " para processar")
    '        TraceFile.Save("O app.config esta configurado para processar " + qtdRegistrosParaProcessar.ToString() + " por vez")

    '        While totalRegistros > 0
    '            Dim paramsB As SqlParameter() = New SqlParameter() { _
    '                                                               New SqlParameter("@GrupoEventoId", Integer.Parse(Me.configs("grupo_evento_als_id"))), _
    '                                                               New SqlParameter("@QtdeReg", Integer.Parse(Me.configs("QtdRegistrosParaProcessar"))), _
    '                                                               New SqlParameter("@usuario", "SMQP0007"), _
    '                                                               New SqlParameter("@ambiente_destino_id", Integer.Parse("6")), _
    '                                                               New SqlParameter("@usar_conexao_abs", bUsarConexaoABS), _
    '                                                               New SqlParameter("@flag_empresa_eh_abs", "S") _
    '                                                               }
    '            Try
    '                cCon.InternalExecuteNonQuery(CommandType.StoredProcedure, "interface_dados_db..segs8494_spi", paramsB)
    '            Catch ex As Exception
    '                TraceFile.Erro("Erro na execu��o da PROCEDURE.")
    '                ErrorFile.SaveException(ex)
    '            End Try
    '        End While

    '        TraceFile.Save("Saiu do la�o While")

    '        tmr.Start()

    '        TraceFile.Save("Reiniciou o timer")
    '    Catch ex As Exception
    '        TraceFile.Erro("Falha no processamento.")
    '        ErrorFile.SaveException(ex)
    '        TraceFile.InserirDivis�o()
    '        tmr.Start()
    '    End Try
    'End Sub
#End Region

#End Region


#Region "LogExecution"
    Public Sub LogExecution(ByVal tpLog As Integer, ByVal serviceName As String, ByVal abs As String)
        Try

            Dim params As SqlParameter() = New SqlParameter() { _
                                                                New SqlParameter("@TP_LOG", tpLog), _
                                                                New SqlParameter("@NOME_SERVICO", serviceName), _
                                                                New SqlParameter("@ABS", abs) _
                                                                }
            cCon.InternalExecuteNonQuery(CommandType.StoredProcedure, "INTERFACE_DADOS_DB..SMQS0133_SPU", params)
        Catch ex As Exception
            TraceFile.Save("Classe: Service")
            TraceFile.Save("M�todo: LogExecution")
            TraceFile.Erro("Falha ao salvar log de execucao.")
            ErrorFile.SaveException(ex)
        End Try
    End Sub
#End Region

End Class