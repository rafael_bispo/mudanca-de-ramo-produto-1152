Attribute VB_Name = "Local"
Public Sub adSQL(ByRef sSQL As String, ByVal sCadeia As String)
'Jorfilho 04/11/2002 - Concatena a query com vbNewLine
   sSQL = sSQL & sCadeia & vbNewLine
End Sub


'#################################### Grava no arquivo de log do Control-M
Sub GravaLogCTM(ByVal sSts As String, ByVal sMsg As String, ByVal sDesc As String, ByVal sVal As String, Optional pStrArqLog As String = "")
        
    On Error Resume Next
    
    If Dir(pStrArqLog) = "" Then
        Open pStrArqLog For Output As #100
        
        Print #100, sSts & " - " & sMsg & " - " & sDesc & " - " & sVal
        Close #100
    Else
        Open pStrArqLog For Append As #100
        
        Print #100, sSts & " - " & sMsg & " - " & sDesc & " - " & sVal
        Close #100
    End If
 End Sub

