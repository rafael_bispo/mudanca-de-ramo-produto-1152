Attribute VB_Name = "Local"
Option Explicit

Public Function Preenche_Caracter(sString As String, lTamanho As Long, sCaracter As String, Optional bDireita As Boolean = True)

'## Completa com o caracter passado

    If IsNull(sString) Then
        sString = ""
    Else
        sString = CStr(sString)
    End If

    Do While Len(sString) < lTamanho
        If bDireita = True Then
            sString = sString & sCaracter
        Else
            sString = sCaracter & sString
        End If
    Loop

    sString = Mid(sString, 1, lTamanho)

    Preenche_Caracter = sString

End Function

Function MudaVirgulaParaPonto(ByVal valor As String) As String

    If InStr(valor, ",") = 0 Then
        MudaVirgulaParaPonto = valor
    Else
        valor = Mid$(valor, 1, InStr(valor, ",") - 1) + "." + Mid$(valor, InStr(valor, ",") + 1)
        MudaVirgulaParaPonto = valor
    End If

End Function
