VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cls00118"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Public Function ObterDtContabilizacao(ByVal sSiglaSistema As String, _
                                      ByVal sSiglaRecurso As String, _
                                      ByVal sDescricaoRecurso As String, _
                                      ByVal iAmbienteId As Integer) As String

    Dim sSql As String
    Dim oConexao As Object
    Dim rs As Recordset

    On Error GoTo Trata_Erro

    sSql = ""
    sSql = sSql & "SELECT dt_contabilizacao " & vbNewLine
    sSql = sSql & "  FROM parametro_geral_tb  WITH(NOLOCK) " & vbNewLine

    Set rs = ExecutarSQL(sSiglaSistema, iAmbienteId, sSiglaRecurso, sDescricaoRecurso, sSql, True, False, 60000)

    ObterDtContabilizacao = Format(rs(0), "dd/mm/yyyy")

    Exit Function

Trata_Erro:

    Call Err.Raise(Err.Number, , "SEGL0022.cls00118.ObterDtContabilizacao - " & Err.Description)

End Function

Public Function ObterDtOperacional(ByVal sSiglaSistema As String, _
                                   ByVal sSiglaRecurso As String, _
                                   ByVal sDescricaoRecurso As String, _
                                   ByVal iAmbienteId As Integer) As String

    Dim sSql As String
    Dim rsOperacional As Recordset

    On Error GoTo Trata_Erro

    sSql = ""
    sSql = sSql & "SELECT dt_operacional " & vbNewLine
    sSql = sSql & "  FROM parametro_geral_tb  WITH(NOLOCK) " & vbNewLine

    Set rsOperacional = ExecutarSQL(sSiglaSistema, iAmbienteId, sSiglaRecurso, sDescricaoRecurso, sSql, True, False, 60000)

    ObterDtOperacional = Format(rsOperacional(0), "dd/mm/yyyy")

    Exit Function

Trata_Erro:

    Call Err.Raise(Err.Number, , "SEGL0022.cls00118.ObterDtOperacional - " & Err.Description)

End Function

Public Sub ConsultarVariaveisAmbiente(ByVal sSiglaSistema As String, _
                                      ByVal sSecao As String, _
                                      ByVal iAmbienteId As Integer, _
                                      ByVal sChaveSistema As String, _
                                      ByRef sServidor As String, _
                                      ByRef sBanco As String, _
                                      ByRef sUsuario As String, _
                                      ByRef sSenha As String)

'## Rotina de consulta �s vari�veis de ambiente

    Dim oAmbiente As Object

    On Error GoTo Trata_Erro

    Set oAmbiente = CreateObject("SABL0010.cls00009")

    sServidor = oAmbiente.RetornaValorAmbiente(sSiglaSistema, sSecao, "Servidor", sChaveSistema, iAmbienteId)
    sBanco = oAmbiente.RetornaValorAmbiente(sSiglaSistema, sSecao, "Banco", sChaveSistema, iAmbienteId)
    sUsuario = oAmbiente.RetornaValorAmbiente(sSiglaSistema, sSecao, "Usuario", sChaveSistema, iAmbienteId)
    sSenha = oAmbiente.RetornaValorAmbiente(sSiglaSistema, sSecao, "Senha", sChaveSistema, iAmbienteId)

    Exit Sub

Trata_Erro:

    Call Err.Raise(Err.Number, , "SEGL0022.cls00118.ConsultarVariaveisAmbiente - " & Err.Description)

End Sub

'mathayde
Public Function ObterTratamentoAbs(ByVal sSiglaSistema As String, _
                                   ByVal sSiglaRecurso As String, _
                                   ByVal sDescricaoRecurso As String, _
                                   ByVal iAmbienteId As Integer, _
                                   ByRef iAmbienteIdSeg2 As Long) As Boolean

    Dim sSql As String
    Dim oConexao As Object
    Dim rs As Recordset
    Dim rs2 As Recordset

    On Error GoTo Trata_Erro

    ObterTratamentoAbs = False

    sSql = ""
    sSql = sSql & "SELECT isnull(trata_abs,'n') " & vbNewLine
    sSql = sSql & "  FROM seguros_db..parametro_geral_tb " & vbNewLine

    Set rs = ExecutarSQL(sSiglaSistema, iAmbienteId, sSiglaRecurso, sDescricaoRecurso, sSql, True, False, 60000)

    If UCase(rs(0)) = "S" Then

        sSql = "       SELECT cod_empresa " & vbNewLine
        sSql = sSql & "     , ambiente_id " & vbNewLine
        sSql = sSql & "     , ambiente_destino_id " & vbNewLine
        sSql = sSql & "  FROM controle_sistema_db..empresa_ambiente_tb WITH(NOLOCK) " & vbNewLine
        sSql = sSql & " WHERE ambiente_id = " & iAmbienteId & vbNewLine
        sSql = sSql & "   AND ambiente_id <> ambiente_destino_id " & vbNewLine

        Set rs2 = ExecutarSQL(sSiglaSistema, iAmbienteId, sSiglaRecurso, sDescricaoRecurso, sSql, True, False, 60000)

        If Not rs2.EOF Then
            iAmbienteIdSeg2 = rs2("ambiente_destino_id")
            ObterTratamentoAbs = True
        End If
    End If

    Exit Function

Trata_Erro:

    Call Err.Raise(Err.Number, , "SEGL0022.cls00118.ObterTratamentoAbs - " & Err.Description)

End Function

'carolina.marinho
Public Function ObterCodSeguradoraSusep(ByVal sSiglaSistema As String, _
                                        ByVal sSiglaRecurso As String, _
                                        ByVal sDescricaoRecurso As String, _
                                        ByVal iAmbienteId As Integer) As Integer

    Dim sSql As String
    Dim rs As Recordset

    On Error GoTo Trata_Erro

    If sDescricaoRecurso = "" Then
        sDescricaoRecurso = "SISBB"
    End If

    sSql = ""
    sSql = sSql & "SELECT cod_seguradora_susep " & vbNewLine
    sSql = sSql & "  FROM seguros_db..parametro_geral_tb  WITH(NOLOCK) " & vbNewLine

    Set rs = ExecutarSQL(sSiglaSistema, iAmbienteId, sSiglaRecurso, sDescricaoRecurso, sSql, True, False, 60000)

    If rs.EOF Then
        ObterCodSeguradoraSusep = 0
    Else
        ObterCodSeguradoraSusep = rs(0)
    End If

    Exit Function

Trata_Erro:

    Call Err.Raise(Err.Number, , "SEGL0022.cls00118.ObterCodSeguradoraSusep - " & Err.Description)

End Function


