VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cls00127"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Const PRODUTO_ID_COBAN = 719

Public Function IncluirGestor(ByVal sSiglaSistema As String, _
                              ByVal sSiglaRecurso As String, _
                              ByVal sDescricaoRecurso As String, _
                              ByVal lAmbienteId As Long, _
                              ByVal sNome As String, _
                              ByVal iQtdMinima As Integer, _
                              ByVal iQtdMaxima As Integer, _
                              ByVal sEndereco As String, _
                              ByVal sBairro As String, _
                              ByVal sMunicipio As String, _
                              ByVal sCep As String, _
                              ByVal sEstado As String, _
                              ByVal sUsuario As String, _
                              ByVal sCnpj As String, _
                              ByVal lCodInstituicao As Long, _
                              ByVal sNomeInstituicao As String, _
                              ByVal lCodCorrespondente As Long, _
                              ByVal sNomeCorrespondente As String, _
                              ByVal lCodTipoCorrespondente As Long, _
                              ByVal lCodGestorCorrespondente As Long, _
                              ByVal sNomeLoja As String, _
                              ByVal lCodLoja As Long) As Long

    Dim sSql As String
    Dim rs As Recordset

    On Error GoTo Trata_Erro

    sSql = "SET NOCOUNT ON " & vbNewLine
    sSql = sSql & "EXEC gestor_spi " & vbNewLine
    sSql = sSql & " '" & sNome & "'" & vbNewLine
    sSql = sSql & ", " & iQtdMinima & vbNewLine
    sSql = sSql & ", " & iQtdMaxima & vbNewLine
    sSql = sSql & ",'" & sEndereco & "'" & vbNewLine
    sSql = sSql & ",'" & sBairro & "'" & vbNewLine
    sSql = sSql & ",'" & sMunicipio & "'" & vbNewLine
    sSql = sSql & ",'" & sCep & "'" & vbNewLine
    sSql = sSql & ",'" & sEstado & "'" & vbNewLine
    sSql = sSql & ",'" & sUsuario & "'" & vbNewLine
    sSql = sSql & ",'" & sCnpj & "'" & vbNewLine
    sSql = sSql & ", " & lCodInstituicao & vbNewLine
    sSql = sSql & ",'" & sNomeInstituicao & "'" & vbNewLine
    sSql = sSql & ", " & lCodCorrespondente & vbNewLine
    sSql = sSql & ",'" & sNomeCorrespondente & "'" & vbNewLine
    sSql = sSql & ", " & lCodTipoCorrespondente & vbNewLine
    sSql = sSql & ", " & lCodGestorCorrespondente & vbNewLine
    sSql = sSql & ", " & lCodLoja & vbNewLine
    sSql = sSql & ",'" & sNomeLoja & "'" & vbNewLine

    Set rs = ExecutarSQL(sSiglaSistema, _
                         lAmbienteId, _
                         sSiglaRecurso, _
                         sDescricaoRecurso, _
                         sSql, _
                         True, False, 60000)


    IncluirGestor = rs(0)

    Set rs = Nothing

    Exit Function

Trata_Erro:

    Call Err.Raise(Err.Number, , "SEGL0022.cls00127.IncluirGestor - " & Err.Description)

End Function

Public Sub IncluirGestorPedido(ByVal sSiglaSistema As String, _
                               ByVal sSiglaRecurso As String, _
                               ByVal sDescricaoRecurso As String, _
                               ByVal lAmbienteId As Long, _
                               ByVal lGestorId As Long, _
                               ByVal sDtPedido As String, _
                               ByVal sOperacao As String, _
                               ByVal sUsuario As String, _
                               Optional ByVal iCertificadoIni As Integer, _
                               Optional ByVal iCertificadoFinal As Integer, _
                               Optional ByVal iQuantidade As Integer, _
                               Optional ByVal sPedidoEspecial As String)


    Dim sSql As String
    Dim rs As Recordset

    On Error GoTo Trata_Erro

    sSql = sSql & "EXEC gestor_pedido_spi "
    sSql = sSql & lGestorId
    sSql = sSql & ", '" & sDtPedido & "'"
    sSql = sSql & ", '" & sOperacao & "'"
    sSql = sSql & ",'" & sUsuario & "'"
    sSql = sSql & "," & iCertificadoIni
    sSql = sSql & "," & iCertificadoFinal
    sSql = sSql & "," & iQuantidade
    sSql = sSql & ",'" & sPedidoEspecial & "'"

    Call ExecutarSQL(sSiglaSistema, _
                     lAmbienteId, _
                     sSiglaRecurso, _
                     sDescricaoRecurso, _
                     sSql, _
                     False, False, 60000)

    Exit Sub

Trata_Erro:

    Call Err.Raise(Err.Number, , "SEGL0022.cls00127.IncluirGestorPedido - " & Err.Description)

End Sub

Public Sub ProcessarArquivoGestores(ByVal sSiglaSistema As String, _
                                    ByVal sSiglaRecurso As String, _
                                    ByVal sDescricaoRecurso As String, _
                                    ByVal lAmbienteId As Long, _
                                    ByVal sPathArquivo As String, _
                                    ByVal sNomeArquivo As String, _
                                    ByVal sDataSistema As String, _
                                    ByVal sUsuario As String)

    Const RECEBIDO = "R"

    Dim oArquivo As Object

    Dim sArquivo As String
    Dim sVersao As String
    Dim iCodRetorno As Integer
    Dim iArqInput As Integer
    Dim lTamArquivo As Long
    Dim sHeader As String
    Dim sVersaoHeader As String
    Dim lQtdReg As Long
    Dim lPosicao As Long
    Dim sRegistro As String
    Dim lTotRegistros As Long

    Dim lGestorId As Long
    Dim sNome As String
    Dim iQtdMinima As Integer
    Dim iQtdMaxima As Integer
    Dim sEndereco As String
    Dim sBairro As String
    Dim sMunicipio As String
    Dim sCep As String
    Dim sEstado As String
    Dim sCnpj As String
    Dim lCodInstituicao As Long
    Dim sNomeInstituicao As String
    Dim lCodCorrespondente As Long
    Dim sNomeCorrespondente As String
    Dim lCodTipoCorrespondente As Long
    Dim lCodGestorCorrespondente As Long
    Dim sNomeLoja As String
    Dim lCodLoja As Long

    Dim sSequencial As String

    Dim lConexao As Long

    On Error GoTo TrataErro

    sVersao = Mid(sNomeArquivo, InStr(1, sNomeArquivo, ".") + 1)
    sArquivo = Mid(sNomeArquivo, 1, InStr(1, sNomeArquivo, ".") - 1)

    'Abrindo Arquivo''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

    iArqInput = FreeFile

    Open sPathArquivo & "\" & sNomeArquivo For Input As iArqInput

    lTamArquivo = LOF(iArqInput)

    Line Input #iArqInput, sHeader

    'Validando remessa''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

    sVersaoHeader = Mid(sHeader, 9, 4)

    If Val(sVersaoHeader) <> Val(sVersao) Then
        Close #iArqInput
        Call Err.Raise(vbObjectError + 1000, , "SEGL0022.cls00127.ProcessarArquivoGestores - O Header difere da extens�o do arquivo. Importa��o ser� abortada!")
    End If

    'Inicializando vari�veis de controle ''''''''''''''''''''''''''''''''''''''''''''''''''''

    lQtdReg = 0
    lPosicao = 1

    ' Abrindo Conexao ''''''''''''''''''''''''''''''''''''''''''''

    lConexao = AbrirConexao(sSiglaSistema, _
                            lAmbienteId, _
                            sSiglaRecurso, _
                            sDescricaoRecurso)

    ' Abrindo Transacao ''''''''''''''''''''''''''''''''''''''''''

    If Not AbrirTransacao(lConexao) Then
        Call Err.Raise(vbObjectError + 1000, , "SEGL0022.cls00127.ProcessarArquivoGestores - N�o foi poss�vel abrir transa��o")
    End If

    Do

        DoEvents

        'Lendo registro do arquivo''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        lPosicao = lPosicao + 1
        Line Input #iArqInput, sRegistro

        sSequencial = Mid(sRegistro, 1, 6)

        'Verificando se o registro � o trailler do arquivo '''''''''''''''''''''''''''''''''

        If sSequencial = "999999" Then

            lTotRegistros = Val(Mid(sRegistro, 20, 9))

            'Verificando trailler ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            If lTotRegistros <> lQtdReg Then

                Close #iArqInput

                ' Retornando Transacao '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

                If Not RetornarTransacao(lConexao) Then
                    Call Err.Raise(vbObjectError + 1000, , "SEGL0022.cls00127.ProcessarArquivoGestores - N�o foi poss�vel retornar transa��o")
                End If

                ' Fechando Conexao '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

                If Not FecharConexao(lConexao) Then
                    Call Err.Raise(vbObjectError + 1000, , "SEGL0022.cls00127.ProcessarArquivoGestores - N�o foi poss�vel fechar conex�o")
                End If

                Call Err.Raise(vbObjectError + 1000, , "SEGL0022.cls00127.ProcessarArquivoGestores - Erro no Trailler. Importa��o ser� abortada!")

            End If

            ' Registrando importa��o do arquivo '''''''''''''''''''''''''''''''''''''''''''''''

            Set oArquivo = CreateObject("SEGL0023.cls00119")

            Call oArquivo.RegistrarArquivoProcessado(sSiglaSistema, _
                                                     sSiglaRecurso, _
                                                     sDescricaoRecurso, _
                                                     lAmbienteId, _
                                                     sArquivo, _
                                                     Val(sVersao), _
                                                     lQtdReg, _
                                                     lPosicao, _
                                                     sDataSistema, _
                                                     sUsuario, _
                                                     RECEBIDO)

            Set oArquivo = Nothing

            Exit Do

        End If

        lCodInstituicao = Val(Mid(sRegistro, 9, 6))
        sNomeInstituicao = Trim(Mid(sRegistro, 15, 50))
        lCodCorrespondente = Val(Mid(sRegistro, 65, 6))
        sNomeCorrespondente = Trim(Mid(sRegistro, 71, 50))
        lCodTipoCorrespondente = Val(Mid(sRegistro, 121, 2))
        lCodGestorCorrespondente = Val(Mid(sRegistro, 123, 4))
        sNome = Trim(Mid(sRegistro, 127, 50))
        sEndereco = Trim(Mid(sRegistro, 177, 50))
        sBairro = Trim(Mid(sRegistro, 227, 30))
        sCep = Trim(Mid(sRegistro, 257, 8))
        sEstado = Trim(Mid(sRegistro, 265, 2))
        sCnpj = Trim(Mid(sRegistro, 267, 14))
        lCodLoja = Val(Mid(sRegistro, 281, 4))
        sNomeLoja = Trim(Mid(sRegistro, 285, 50))

        'Incluindo em gestor_tb '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        lGestorId = IncluirGestor(sSiglaSistema, _
                                  sSiglaRecurso, _
                                  sDescricaoRecurso, _
                                  lAmbienteId, _
                                  sNome, _
                                  iQtdMinima, _
                                  iQtdMaxima, _
                                  sEndereco, _
                                  sBairro, _
                                  sMunicipio, _
                                  sCep, _
                                  sEstado, _
                                  sUsuario, _
                                  sCnpj, _
                                  lCodInstituicao, _
                                  sNomeInstituicao, _
                                  lCodCorrespondente, _
                                  sNomeCorrespondente, _
                                  lCodTipoCorrespondente, _
                                  lCodGestorCorrespondente, _
                                  sNomeLoja, _
                                  lCodLoja)

        'Atualizando vari�veis de controle ''''''''''''''''''''''''''''''''''''''''''''''''''''

        lQtdReg = lQtdReg + 1

    Loop Until EOF(iArqInput)

    If sSequencial <> "999999" Then

        Close #iArqInput

        ' Retornando Transacao '''''''''''''''''''''''''''''''''''''''''''''

        If Not RetornarTransacao(lConexao) Then
            Call Err.Raise(vbObjectError + 1000, , "SEGL0022.cls00127.ProcessarArquivoGestores - N�o foi poss�vel retornar transa��o")
        End If

        ' Fechando Conexao '''''''''''''''''''''''''''''''''''''''''''''''

        If Not FecharConexao(lConexao) Then
            Call Err.Raise(vbObjectError + 1000, , "SEGL0022.cls00127.ProcessarArquivoGestores - N�o foi poss�vel fechar conex�o")
        End If

        Call Err.Raise(vbObjectError + 1000, , "SEGL0022.cls00127.ProcessarArquivoGestores - Arquivo sem trailler. Programa ser� cancelado.")

    End If

    Close #iArqInput

    ' Fechando Transacao '''''''''''''''''''''''''''''''''''''''''''''

    If Not ConfirmarTransacao(lConexao) Then
        Call Err.Raise(vbObjectError + 1000, , "SEGL0022.cls00127.ProcessarArquivoGestores - N�o foi poss�vel confirmar transa��o")
    End If

    ' Fechando Conexao '''''''''''''''''''''''''''''''''''''''''''''''

    If Not FecharConexao(lConexao) Then
        Call Err.Raise(vbObjectError + 1000, , "SEGL0022.cls00127.ProcessarArquivoGestores - N�o foi poss�vel fechar conex�o")
    End If

    Exit Sub

TrataErro:

    Close #iArqInput

    ' Retornando Transacao '''''''''''''''''''''''''''''''''''''''''''''

    If Not RetornarTransacao(lConexao) Then
        Call Err.Raise(vbObjectError + 1000, , "SEGL0022.cls00127.ProcessarArquivoGestores - N�o foi poss�vel retornar transa��o")
    End If

    ' Fechando Conexao '''''''''''''''''''''''''''''''''''''''''''''''

    If Not FecharConexao(lConexao) Then
        Call Err.Raise(vbObjectError + 1000, , "SEGL0022.cls00127.ProcessarArquivoGestores - N�o foi poss�vel fechar conex�o")
    End If

    Call Err.Raise(Err.Number, , "SEGL0022.cls00127.ProcessarArquivoGestores - " & Err.Description)

End Sub

Public Function ConsultarGestor(ByVal sSiglaSistema As String, _
                                ByVal sSiglaRecurso As String, _
                                ByVal sDescricaoRecurso As String, _
                                ByVal lAmbienteId As Long, _
                                Optional ByVal lGestorId As Long = 0, _
                                Optional ByVal sNome As String = "") As Object

    Dim sSql As String

    On Error GoTo TrataErro

    sSql = ""
    sSql = sSql & "SELECT *"
    sSql = sSql & "  FROM gestor_tb WITH(NOLOCK) "

    If lGestorId <> 0 Then
        sSql = sSql & " WHERE gestor_id = " & lGestorId
    End If

    If sNome <> "" Then
        sSql = sSql & " WHERE Nome LIKE '" & sNome & "%'"
    End If

    sSql = sSql & " ORDER BY gestor_id"

    Set ConsultarGestor = ExecutarSQL(sSiglaSistema, _
                                      lAmbienteId, _
                                      sSiglaRecurso, _
                                      sDescricaoRecurso, _
                                      sSql, _
                                      True, False, 60000)

    Exit Function

TrataErro:

    Call Err.Raise(Err.Number, , "SEGL0022.cls00127.ConsultarGestor - " & Err.Description)

End Function

Public Function VerificarEstoque(ByVal sSiglaSistema As String, _
                                 ByVal sSiglaRecurso As String, _
                                 ByVal sDescricaoRecurso As String, _
                                 ByVal lAmbienteId As Long, _
                                 ByVal lGestorId As Long, _
                                 ByVal iQuantidade As Integer) As Boolean

    Dim sSql As String
    Dim rs As Recordset
    Dim iQuantidadeEstoque As Integer, iQtdMaxima As Integer

    On Error GoTo TrataErro

    sSql = ""
    sSql = sSql & "SELECT quantidade_estoque, qtd_maxima " & vbNewLine
    sSql = sSql & "  FROM gestor_tb WITH(NOLOCK) " & vbNewLine
    sSql = sSql & " WHERE gestor_id = " & lGestorId & vbNewLine

    Set rs = ExecutarSQL(sSiglaSistema, _
                         lAmbienteId, _
                         sSiglaRecurso, _
                         sDescricaoRecurso, _
                         sSql, _
                         True, False, 60000)

    If rs.EOF = False Then
        iQuantidadeEstoque = rs("quantidade_estoque")
        iQtdMaxima = rs("qtd_maxima")
    End If

    Set rs = Nothing

    If iQuantidade + iQuantidadeEstoque > iQtdMaxima Then
        VerificarEstoque = False
    Else
        VerificarEstoque = True
    End If

    Exit Function

TrataErro:

    Call Err.Raise(Err.Number, , "SEGL0022.cls00127.VerificarEstoque - " & Err.Description)

End Function

Public Function VerificarPropostasCPF(ByVal sSiglaSistema As String, _
                                      ByVal sSiglaRecurso As String, _
                                      ByVal sDescricaoRecurso As String, _
                                      ByVal lAmbienteId As Long, _
                                      ByVal sCPF As String) As Boolean

    Dim sSql As String
    Dim rs As Recordset

    Dim iQtdPropostasCOBAN As Integer

    On Error GoTo TrataErro

    sSql = "" & vbNewLine
    sSql = sSql & "SELECT COUNT(*) " & vbNewLine
    sSql = sSql & "  FROM proposta_tb WITH(NOLOCK) " & vbNewLine
    sSql = sSql & "  JOIN cliente_tb WITH(NOLOCK) " & vbNewLine
    sSql = sSql & "    ON cliente_tb.cliente_id = proposta_tb.prop_cliente_id " & vbNewLine
    sSql = sSql & " WHERE proposta_tb.produto_id = " & PRODUTO_ID_COBAN & vbNewLine
    sSql = sSql & "   AND cliente_tb.cpf_cnpj = '" & sCPF & "'" & vbNewLine

    Set rs = ExecutarSQL(sSiglaSistema, _
                         lAmbienteId, _
                         sSiglaRecurso, _
                         sDescricaoRecurso, _
                         sSql, _
                         True, False, 60000)

    iQtdPropostasCOBAN = rs(0)

    Set rs = Nothing

    If iQtdPropostasCOBAN < 5 Then
        VerificarPropostasCPF = True
    Else
        VerificarPropostasCPF = False
    End If

    Exit Function

TrataErro:

    Call Err.Raise(Err.Number, , "SEGL0022.cls00127.VerificarPropostasCPF - " & Err.Description)

End Function

Public Sub AlterarGestor(ByVal sSiglaSistema As String, _
                         ByVal sSiglaRecurso As String, _
                         ByVal sDescricaoRecurso As String, _
                         ByVal lAmbienteId As Long, _
                         ByVal lGestorId As Long, _
                         ByVal iQtdMinima As Integer, _
                         ByVal iQtdMaxima As Integer, _
                         ByVal sNome As String, _
                         ByVal sEndereco As String, _
                         ByVal sBairro As String, _
                         ByVal sMunicipio As String, _
                         ByVal sCep As String, _
                         ByVal sEstado As String, _
                         ByVal sUsuario As String, _
                         ByVal sCnpj As String, _
                         ByVal sCodInstituicao As String, _
                         ByVal sNomeInstituicao As String, _
                         ByVal sCodCorresp As String, _
                         ByVal sNomeCorresp As String, _
                         ByVal sCodTipoCorresp As String, _
                         ByVal sCodGestorCorresp As String, _
                         ByVal sCodLoja As String, _
                         ByVal sNomeLoja As String)

    Dim sSql As String

    On Error GoTo TrataErro

    sSql = "EXEC gestor_spu " & lGestorId & ","
    sSql = sSql & "'" & sNome & "',"
    sSql = sSql & iQtdMinima & ","
    sSql = sSql & iQtdMaxima & ","
    sSql = sSql & "'" & sEndereco & "',"
    sSql = sSql & "'" & sBairro & "',"
    sSql = sSql & "'" & sMunicipio & "',"
    sSql = sSql & "'" & sCep & "',"
    sSql = sSql & "'" & sEstado & "',"
    sSql = sSql & "'" & sUsuario & "',"
    sSql = sSql & "'" & sCnpj & "',"
    sSql = sSql & sCodInstituicao & ","
    sSql = sSql & "'" & sNomeInstituicao & "',"
    sSql = sSql & sCodCorresp & ","
    sSql = sSql & "'" & sNomeCorresp & "',"
    sSql = sSql & sCodTipoCorresp & ","
    sSql = sSql & sCodGestorCorresp & ","
    sSql = sSql & sCodLoja & ","
    sSql = sSql & "'" & sNomeLoja & "'"


    Call ExecutarSQL(sSiglaSistema, _
                     lAmbienteId, _
                     sSiglaRecurso, _
                     sDescricaoRecurso, _
                     sSql, _
                     False, False, 60000)

    Exit Sub

TrataErro:

    Call Err.Raise(Err.Number, , "SEGL0022.cls00127.AlterarGestor - " & Err.Description)

End Sub

Public Function GravarHistorico(ByVal sSiglaSistema As String, _
                                ByVal sSiglaRecurso As String, _
                                ByVal sDescricaoRecurso As String, _
                                ByVal lAmbienteId As Long, _
                                ByVal lGestorId As Long, _
                                ByVal iQtdMinima As Integer, _
                                ByVal iQtdMaxima As Integer, _
                                ByVal sNome As String, _
                                ByVal sEndereco As String, _
                                ByVal sBairro As String, _
                                ByVal sMunicipio As String, ByVal sCep As String, ByVal sEstado As String, _
                                ByVal sCnpj As String, sCodInstituicao As String, sNomeInstituicao As String, sCodCorresp As String, _
                                ByVal sNomeCorresp As String, sCodTipoCorresp As String, sCodGestorCorresp As String, _
                                ByVal sCodLoja As String, ByVal sNomeLoja As String, _
                                ByVal sUsuario As String, _
                                ByVal sNomeAnt As String, _
                                ByVal iQtdMinimaAnt As Integer, _
                                ByVal iQtdMaximaAnt As Integer, _
                                ByVal sEnderecoAnt As String, _
                                ByVal sBairroAnt As String, _
                                ByVal sMunicipioAnt As String, ByVal sCepAnt As String, ByVal sEstadoAnt As String, _
                                ByVal sCnpjAnt As String, ByVal sCodInstituicaoAnt As String, ByVal sNomeInstituicaoAnt As String, ByVal sCodCorrespAnt As String, _
                                ByVal sNomeCorrespAnt As String, ByVal sCodTipoCorrespAnt As String, ByVal sCodGestorCorrespAnt As String, _
                                ByVal sCodLojaAnt As String, ByVal sNomeLojaAnt As String)


    Dim sSql As String

    On Error GoTo TrataErro

    sSql = "EXEC historico_gestor_spi " & lGestorId & ","
    sSql = sSql & IIf(sNome = sNomeAnt, "NULL", "'" & sNome & "'") & ","
    sSql = sSql & IIf(iQtdMinima = iQtdMinimaAnt, "NULL", iQtdMinima) & ","
    sSql = sSql & IIf(iQtdMaxima = iQtdMaximaAnt, "NULL", iQtdMaxima) & ","
    sSql = sSql & IIf(sEndereco = sEnderecoAnt, "NULL", "'" & sEndereco & "'") & ","
    sSql = sSql & IIf(sBairro = sBairroAnt, "NULL", "'" & sBairro & "'") & ","
    sSql = sSql & IIf(sMunicipio = sMunicipioAnt, "NULL", "'" & sMunicipio & "'") & ","
    sSql = sSql & IIf(sCep = sCepAnt, "NULL", "'" & sCep & "'") & ","
    sSql = sSql & IIf(sEstado = sEstadoAnt, "NULL", "'" & sEstado & "'") & ","
    sSql = sSql & "'" & sUsuario & "',"
    sSql = sSql & IIf(sCnpj = sCnpjAnt, "NULL", "'" & sCnpj & "'") & ","
    sSql = sSql & IIf(sCodInstituicao = sCodInstituicaoAnt, "NULL", sCodInstituicao) & ","
    sSql = sSql & IIf(sNomeInstituicao = sNomeInstituicaoAnt, "NULL", "'" & sNomeInstituicao & "'") & ","
    sSql = sSql & IIf(sCodCorresp = sCodCorrespAnt, "NULL", sCodCorresp) & ","
    sSql = sSql & IIf(sNomeCorresp = sNomeCorrespAnt, "NULL", "'" & sNomeCorresp & "'") & ","
    sSql = sSql & IIf(sCodTipoCorresp = sCodTipoCorrespAnt, "NULL", sCodTipoCorresp) & ","
    sSql = sSql & IIf(sCodGestorCorresp = sCodGestorCorrespAnt, "NULL", sCodGestorCorresp) & ","
    sSql = sSql & IIf(sCodLoja = sCodLojaAnt, "NULL", sCodLoja) & ","
    sSql = sSql & IIf(sNomeLoja = sNomeLojaAnt, "NULL", "'" & sNomeLoja & "'")

    Call ExecutarSQL(sSiglaSistema, _
                     lAmbienteId, _
                     sSiglaRecurso, _
                     sDescricaoRecurso, _
                     sSql, _
                     False, False, 60000)

    Exit Function

TrataErro:

    Call Err.Raise(Err.Number, , "SEGL0022.cls00127.GravarHistorico - " & Err.Description)

End Function

Public Function ConsultarHistorico(ByVal sSiglaSistema As String, _
                                   ByVal sSiglaRecurso As String, _
                                   ByVal sDescricaoRecurso As String, _
                                   ByVal lAmbienteId As Long, _
                                   Optional ByVal lGestorId As Long, _
                                   Optional ByVal sNome As String, _
                                   Optional ByVal sDataHistorico As String) As Object
    Dim sSql As String

    On Error GoTo TrataErro

    sSql = ""
    sSql = sSql & "SELECT historico_gestor_tb.*, gestor_tb.nome AS Nome_Gestor"
    sSql = sSql & "  FROM historico_gestor_tb WITH(NOLOCK) "
    sSql = sSql & " INNER JOIN gestor_tb WITH(NOLOCK) "
    sSql = sSql & "    ON historico_gestor_tb.gestor_id = gestor_tb.gestor_id "

    If lGestorId <> 0 And sDataHistorico <> "" Then
        sSql = sSql & " WHERE historico_gestor_tb.gestor_id = " & lGestorId
        sSql = sSql & " AND dt_historico = '" & Format(sDataHistorico, "yyyymmdd") & "'"
    Else
        If lGestorId <> 0 Then
            sSql = sSql & " WHERE historico_gestor_tb.gestor_id = " & lGestorId
        Else
            If sDataHistorico <> "" Then
                sSql = sSql & " WHERE Nome LIKE '" & Format(sDataHistorico, "yyyymmdd") & "'"
            End If
        End If
        If sNome <> "" Then
            sSql = sSql & " WHERE gestor_tb.Nome LIKE '" & sNome & "%'"
        End If
    End If


    sSql = sSql & " ORDER BY historico_gestor_tb.gestor_id"


    Set ConsultarHistorico = ExecutarSQL(sSiglaSistema, _
                                         lAmbienteId, _
                                         sSiglaRecurso, _
                                         sDescricaoRecurso, _
                                         sSql, _
                                         True, False, 60000)

    Exit Function

TrataErro:

    Call Err.Raise(Err.Number, , "SEGL0022.cls00127.ConsultarHistorico - " & Err.Description)

End Function

Public Function ConsultarGestorPedido(ByVal sSiglaSistema As String, _
                                      ByVal sSiglaRecurso As String, _
                                      ByVal sDescricaoRecurso As String, _
                                      ByVal lAmbienteId As Long, _
                                      ByVal lGestorId As Long, _
                                      Optional ByVal sOperacao As String = "") As Object

    Dim sSql As String

    On Error GoTo TrataErro

    sSql = ""
    sSql = sSql & "SELECT pedido_id, "
    sSql = sSql & "       quantidade, "
    sSql = sSql & "       dt_pedido, "
    sSql = sSql & "       operacao, "
    sSql = sSql & "       ISNULL(pedido_especial, 'n') AS pedido_especial, "
    sSql = sSql & "       certificado_cancelamento_ini, "
    sSql = sSql & "       certificado_cancelamento_final, "
    sSql = sSql & "       usuario "
    sSql = sSql & "  FROM gestor_pedido_tb  WITH(NOLOCK) "
    sSql = sSql & " WHERE gestor_id = " & lGestorId

    If sOperacao <> "" Then
        sSql = sSql & "   AND operacao = '" & sOperacao & "'"
    End If

    sSql = sSql & " ORDER BY dt_pedido DESC"

    Set ConsultarGestorPedido = ExecutarSQL(sSiglaSistema, _
                                            lAmbienteId, _
                                            sSiglaRecurso, _
                                            sDescricaoRecurso, _
                                            sSql, _
                                            True, False, 60000)

    Exit Function

TrataErro:

    Call Err.Raise(Err.Number, , "SEGL0022.cls00127.ConsultarGestorPedido - " & Err.Description)

End Function



Public Function ConsultarEmiPropostaCOBAN(ByVal sSiglaSistema As String, _
                                          ByVal sSiglaRecurso As String, _
                                          ByVal sDescricaoRecurso As String, _
                                          ByVal lAmbienteId As Long, _
                                          ByVal lEmiPropostaId As Long) As Object

    Dim sSql As String

    On Error GoTo TrataErro

    sSql = "" & vbNewLine
    sSql = sSql & " SELECT emi_proposta_id, " & vbNewLine
    sSql = sSql & "        num_certificado, " & vbNewLine
    sSql = sSql & "        num_transacao_loja, " & vbNewLine
    sSql = sSql & "        cod_agencia_coban, " & vbNewLine
    sSql = sSql & "        ddd_contato, " & vbNewLine
    sSql = sSql & "        telefone_contato, " & vbNewLine
    sSql = sSql & "        cod_gestor, " & vbNewLine
    sSql = sSql & "        cnpj_gestor, " & vbNewLine
    sSql = sSql & "        cod_loja_gestor, " & vbNewLine
    sSql = sSql & "        cod_pdv, " & vbNewLine
    sSql = sSql & "        autenticacao_contratacao, " & vbNewLine
    sSql = sSql & "        autenticacao_cancelamento, " & vbNewLine
    sSql = sSql & "        cod_instituicao, " & vbNewLine
    sSql = sSql & "        dt_movimento, " & vbNewLine
    sSql = sSql & "        dt_cancelamento, " & vbNewLine
    sSql = sSql & "        num_sorteio, " & vbNewLine
    sSql = sSql & "        nome_vendedor, " & vbNewLine

    ' bcarneiro - 05/02/2004 - COBAN - Correspondente Banc�rio
    sSql = sSql & "        ISNULL(val_restituicao,0) AS val_restituicao," & vbNewLine
    sSql = sSql & "        ISNULL(val_iof,0) AS val_iof," & vbNewLine
    sSql = sSql & "        ISNULL(val_comissao,0) AS val_comissao," & vbNewLine
    sSql = sSql & "        ISNULL(val_comissao_estipulante,0) AS val_comissao_estipulante," & vbNewLine
    sSql = sSql & "        ISNULL(val_repasse,0) AS val_repasse," & vbNewLine
    sSql = sSql & "        ISNULL(custo_apolice,0) AS custo_apolice," & vbNewLine
    sSql = sSql & "        ISNULL(val_desconto_comercial,0) AS val_desconto_comercial," & vbNewLine
    sSql = sSql & "        ISNULL(val_adic_fracionamento,0) AS val_adic_fracionamento" & vbNewLine

    sSql = sSql & "   FROM emi_proposta_coban_tb  WITH(NOLOCK) " & vbNewLine
    sSql = sSql & "  WHERE emi_proposta_id = " & lEmiPropostaId

    Set ConsultarEmiPropostaCOBAN = ExecutarSQL(sSiglaSistema, _
                                                lAmbienteId, _
                                                sSiglaRecurso, _
                                                sDescricaoRecurso, _
                                                sSql, _
                                                True, False, 60000)

    Exit Function

TrataErro:

    Call Err.Raise(Err.Number, , "SEGL0022.cls00127.ConsultarEmiPropostaCOBAN - " & Err.Description)

End Function

Public Sub IncluirSeguroCOBAN(ByVal sSiglaSistema As String, ByVal sSiglaRecurso As String, _
                              ByVal sDescricaoRecurso As String, ByVal lAmbienteId As Long, _
                              ByVal lPropostaId As Long, ByVal lProdutoId As Long, _
                              ByVal iPlanoId As Integer, ByVal sDtInicioVigenciaSeg As String, _
                              ByVal lNumCertificado As Long, ByVal iRamoId As Integer, ByVal iSubRamoId As Integer, _
                              ByVal lNumTransacaoLoja As Long, ByVal iCodAgenciaCoban As Integer, _
                              ByVal sDDDContato As String, ByVal sTelefoneContato As String, _
                              ByVal lCodGestor As Long, ByVal sCNPJGestor As String, _
                              ByVal lCodLojaGestor As Long, ByVal lCodPDV As Long, _
                              ByVal sAutenticacaoContratacao As String, _
                              ByVal sAutenticacaoCancelamento As String, _
                              ByVal lCodInstituicao As Long, ByVal sDtMovimento As String, _
                              ByVal sDtCancelamento As String, ByVal sNumSorteio As String, _
                              ByVal sNomeVendedor As String, ByVal sUsuario As String)


    Dim sSql As String

    On Error GoTo TrataErro

    'Incluindo em seguro_tb e seguro_vida_tb ''''''''''''''''''''''''''''''

    sSql = "SET NOCOUNT ON " & vbNewLine
    sSql = sSql & "EXEC seguro_vida_coban_spi "
    sSql = sSql & "  " & lPropostaId
    sSql = sSql & ",'" & sDtInicioVigenciaSeg & "'"
    sSql = sSql & ",NULL"
    sSql = sSql & ", " & lProdutoId
    sSql = sSql & ", " & iPlanoId
    sSql = sSql & ",'" & sUsuario & "'"
    sSql = sSql & ", " & iRamoId
    sSql = sSql & ", " & iSubRamoId

    Call ExecutarSQL(sSiglaSistema, _
                     lAmbienteId, _
                     sSiglaRecurso, _
                     sDescricaoRecurso, _
                     sSql, _
                     False, False, 60000)

    'Incluindo em seguro_coban_tb '''''''''''''''''''''''''''''''''''''''''

    sSql = "SET NOCOUNT ON " & vbNewLine
    sSql = sSql & "EXEC seguro_coban_spi"
    sSql = sSql & "  " & lPropostaId
    sSql = sSql & ", " & lProdutoId
    sSql = sSql & ", " & iPlanoId
    sSql = sSql & ",'" & sDtInicioVigenciaSeg & "'"
    sSql = sSql & ", " & lNumCertificado
    sSql = sSql & ", " & lNumTransacaoLoja
    sSql = sSql & ", " & iCodAgenciaCoban
    sSql = sSql & ",'" & sDDDContato & "'"
    sSql = sSql & ",'" & sTelefoneContato & "'"
    sSql = sSql & ", " & lCodGestor
    sSql = sSql & ",'" & sCNPJGestor & "'"
    sSql = sSql & ", " & lCodLojaGestor
    sSql = sSql & ", " & lCodPDV
    sSql = sSql & ",'" & sAutenticacaoContratacao & "'"
    sSql = sSql & ",'" & sAutenticacaoCancelamento & "'"
    sSql = sSql & ", " & lCodInstituicao

    If Trim(sDtMovimento) <> "" Then
        sSql = sSql & ",'" & sDtMovimento & "'"
    Else
        sSql = sSql & ",NULL"
    End If

    If Trim(sDtCancelamento) <> "" Then
        sSql = sSql & ",'" & sDtCancelamento & "'"
    Else
        sSql = sSql & ",NULL"
    End If

    sSql = sSql & ",'" & sNumSorteio & "'"
    sSql = sSql & ",'" & sNomeVendedor & "'"
    sSql = sSql & ",'" & sUsuario & "'"

    Call ExecutarSQL(sSiglaSistema, _
                     lAmbienteId, _
                     sSiglaRecurso, _
                     sDescricaoRecurso, _
                     sSql, _
                     False, False, 60000)

    Exit Sub

TrataErro:

    Call Err.Raise(Err.Number, , "SEGL0022.cls00127.IncluirSeguroCOBAN - " & Err.Description)

End Sub

Public Sub AlterarSituacaoCertificado(ByVal sSiglaSistema As String, _
                                      ByVal sSiglaRecurso As String, _
                                      ByVal sDescricaoRecurso As String, _
                                      ByVal lAmbienteId As Long, _
                                      ByVal sUsuario As String, _
                                      ByVal sSituacao As String, _
                                      ByVal lCertificadoId As Long, _
                                      Optional ByVal lCertificadoFimId As Long = 0, _
                                      Optional ByVal lPropostaId As Long = 0, _
                                      Optional ByVal sDtCancelamento As String)

    Dim lCertificadoAtualId As Long
    Dim lConexao As Long
    Dim sSql As String

    On Error GoTo TrataErro

    If lCertificadoFimId = 0 Then
        lCertificadoFimId = lCertificadoId
    End If

    ' Abrindo Conexao ''''''''''''''''''''''''''''''''''''''''''''

    lConexao = AbrirConexao(sSiglaSistema, _
                            lAmbienteId, _
                            sSiglaRecurso, _
                            sDescricaoRecurso)

    ' Abrindo Transacao '''''''''''''''''''''''''''''''''''''''''''

    If Not AbrirTransacao(lConexao) Then
        Call Err.Raise(vbObjectError + 1000, , "SEGL0022.cls00127.AlterarSituacaoCertificado - N�o foi poss�vel abrir transa��o")
    End If

    For lCertificadoAtualId = lCertificadoId To lCertificadoFimId

        sSql = "SET NOCOUNT ON "
        sSql = sSql & "EXEC certificado_pre_impresso_spu"
        sSql = sSql & "  " & lCertificadoAtualId

        If lPropostaId > 0 Then
            sSql = sSql & ", " & lPropostaId
        Else
            sSql = sSql & ",NULL"
        End If

        sSql = sSql & ",'" & sSituacao & "'"
        sSql = sSql & ",'" & sUsuario & "'"
        sSql = sSql & ",'" & sDtCancelamento & "'"


        Call Conexao_ExecutarSQL(sSiglaSistema, _
                                 lAmbienteId, _
                                 sSiglaRecurso, _
                                 sDescricaoRecurso, _
                                 sSql, _
                                 lConexao, _
                                 False, False, 60000)

    Next

    ' Fechando Transacao '''''''''''''''''''''''''''''''''''''''''''''

    If Not ConfirmarTransacao(lConexao) Then
        Call Err.Raise(vbObjectError + 1000, , "SEGL0022.cls00127.AlterarSituacaoCertificado - N�o foi poss�vel confirmar transa��o")
    End If

    ' Fechando Conexao '''''''''''''''''''''''''''''''''''''''''''''''

    If Not FecharConexao(lConexao) Then
        Call Err.Raise(vbObjectError + 1000, , "SEGL0022.cls00127.AlterarSituacaoCertificado - N�o foi poss�vel fechar conex�o")
    End If

    Exit Sub

TrataErro:

    Call Err.Raise(Err.Number, , "SEGL0022.cls00127.IncluirSeguroCOBAN - " & Err.Description)

End Sub

Public Sub AtualizarEstoqueGestor(ByVal sSiglaSistema As String, _
                                  ByVal sSiglaRecurso As String, _
                                  ByVal sDescricaoRecurso As String, _
                                  ByVal lAmbienteId As Long, _
                                  ByVal lGestorId As Long, _
                                  ByVal sUsuario As String)

    Dim lConexao As Long
    Dim sSql As String

    On Error GoTo TrataErro

    ' Abrindo Conexao ''''''''''''''''''''''''''''''''''''''''''''

    lConexao = AbrirConexao(sSiglaSistema, _
                            lAmbienteId, _
                            sSiglaRecurso, _
                            sDescricaoRecurso)

    ' Abrindo Transacao '''''''''''''''''''''''''''''''''''''''''''

    If Not AbrirTransacao(lConexao) Then
        Call Err.Raise(vbObjectError + 1000, , "SEGL0022.cls00127.AtualizarEstoqueGestor - N�o foi poss�vel abrir transa��o")
    End If

    sSql = sSql & "EXEC atualiza_estoque_gestor_spu "
    sSql = sSql & lGestorId
    sSql = sSql & ",'" & sUsuario & "'"

    Call Conexao_ExecutarSQL(sSiglaSistema, _
                             lAmbienteId, _
                             sSiglaRecurso, _
                             sDescricaoRecurso, _
                             sSql, _
                             lConexao, _
                             False, False, 60000)

    ' Fechando Transacao '''''''''''''''''''''''''''''''''''''''''''''

    If Not ConfirmarTransacao(lConexao) Then
        Call Err.Raise(vbObjectError + 1000, , "SEGL0022.cls00127.AtualizarEstoqueGestor - N�o foi poss�vel confirmar transa��o")
    End If

    ' Fechando Conexao '''''''''''''''''''''''''''''''''''''''''''''''

    If Not FecharConexao(lConexao) Then
        Call Err.Raise(vbObjectError + 1000, , "SEGL0022.cls00127.AtualizarEstoqueGestor - N�o foi poss�vel fechar conex�o")
    End If

    Exit Sub

TrataErro:

    Call Err.Raise(Err.Number, , "SEGL0022.cls00127.AtualizarEstoqueGestor - " & Err.Description)

End Sub

