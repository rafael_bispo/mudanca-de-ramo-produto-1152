VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cls00165"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
Public sPeriodoInicio As String
Public sPeriodoFim As String

Public Function ConsultarQtdedePropostas(ByVal sSiglaSistema As String, _
                                         ByVal sSiglaRecurso As String, _
                                         ByVal sDescricaoRecurso As String, _
                                         ByVal lAmbienteId As Long, _
                                         ByVal sDtContratacao As String, _
                                         ByVal lNumConexao As Long)

    Dim sSql As String
    Dim rsProposta As Recordset

    On Error GoTo TrataErro

    sSql = ""
    sSql = sSql & "SELECT count(1) as contratadas_VidaMulher, "
    sSql = sSql & "       ISNULL(sum(premio), 0) as premio, "
    sSql = sSql & "       ISNULL(sum(corretagem), 0) as corretagem "
    'sSql = sSql & "  FROM desenv_db.dbo.consulta_vida WITH(nolock) "
    sSql = sSql & "  FROM ##consulta_vida "
    sSql = sSql & " WHERE dt_contratacao = '" & Format(sDtContratacao, "yyyymmdd") & "'"


    Set ConsultarQtdedePropostas = Conexao_ExecutarSQL(sSiglaSistema, _
                                                       lAmbienteId, _
                                                       sSiglaRecurso, _
                                                       sDescricaoRecurso, _
                                                       sSql, _
                                                       lNumConexao, _
                                                       True, False, 60000)


    Exit Function

TrataErro:

    Call Err.Raise(Err.Number, , "SEGL0022.cls00165.ConsultarQtdedePropostas - " & Err.Description)

End Function

Public Function ConsultarQtdedePropostasCapital(ByVal sSiglaSistema As String, _
                                                ByVal sSiglaRecurso As String, _
                                                ByVal sDescricaoRecurso As String, _
                                                ByVal lAmbienteId As Long, _
                                                ByVal sDtContratacao As String, _
                                                ByVal lNumConexao As Long)

    Dim sSql As String

    On Error GoTo TrataErro

    sSql = ""
    sSql = sSql & "SELECT count(1) as val_is,"
    sSql = sSql & "       ISNULL(sum(premio), 0) as premio, "
    sSql = sSql & "       ISNULL(sum(corretagem), 0) as corretagem "
    'sSql = sSql & "  FROM desenv_db.dbo.consulta_vida WITH(nolock)"
    sSql = sSql & "  FROM ##consulta_vida "
    sSql = sSql & " WHERE val_is >= 75000 "
    sSql = sSql & "   AND dt_contratacao = '" & Format(sDtContratacao, "yyyymmdd") & "'"

    Set ConsultarQtdedePropostasCapital = Conexao_ExecutarSQL(sSiglaSistema, _
                                                              lAmbienteId, _
                                                              sSiglaRecurso, _
                                                              sDescricaoRecurso, _
                                                              sSql, _
                                                              lNumConexao, _
                                                              True, False, 60000)

    Exit Function

TrataErro:

    Call Err.Raise(Err.Number, , "SEGL0022.cls00165.ConsultarQtdedePropostasCapital - " & Err.Description)

End Function

Public Function ConsultarQtdedePropostasAceitas(ByVal sSiglaSistema As String, _
                                                ByVal sSiglaRecurso As String, _
                                                ByVal sDescricaoRecurso As String, _
                                                ByVal lAmbienteId As Long, _
                                                ByVal sDtContratacao As String, _
                                                ByVal lNumConexao As Long)

    Dim sSql As String

    On Error GoTo TrataErro

    sSql = ""
    sSql = sSql & "SELECT count(1) as aceitas,"
    sSql = sSql & "       ISNULL(sum(premio), 0) as premio, "
    sSql = sSql & "       ISNULL(sum(corretagem), 0) as corretagem "
    'sSql = sSql & "  FROM desenv_db.dbo.consulta_re WITH(nolock) "
    sSql = sSql & "  FROM ##consulta_re "
    sSql = sSql & " WHERE dt_contratacao = '" & Format(sDtContratacao, "yyyymmdd") & "'"

    Set ConsultarQtdedePropostasAceitas = Conexao_ExecutarSQL(sSiglaSistema, _
                                                              lAmbienteId, _
                                                              sSiglaRecurso, _
                                                              sDescricaoRecurso, _
                                                              sSql, _
                                                              lNumConexao, _
                                                              True, False, 60000)
    Exit Function

TrataErro:

    Call Err.Raise(Err.Number, , "SEGL0022.cls00165.ConsultarQtdedePropostasAceitas - " & Err.Description)

End Function

Public Function ConsultarQtdedePropostasImpressao(ByVal sSiglaSistema As String, _
                                                  ByVal sSiglaRecurso As String, _
                                                  ByVal sDescricaoRecurso As String, _
                                                  ByVal lAmbienteId As Long, _
                                                  ByVal sDtContratacao As String, _
                                                  ByVal lNumConexao As Long)

    Dim sSql As String

    On Error GoTo TrataErro

    sSql = ""
    sSql = sSql & "SELECT count(1) as impressao ,"
    sSql = sSql & "       ISNULL(sum(premio), 0) as premio, "
    sSql = sSql & "       ISNULL(sum(corretagem), 0) as corretagem "
    'sSql = sSql & "  FROM desenv_db.dbo.consulta_re proposta_tb WITH(nolock)"
    sSql = sSql & "  FROM ##consulta_re proposta_tb "
    sSql = sSql & " INNER JOIN certificado_tb WITH(nolock)"
    sSql = sSql & "    ON certificado_tb.proposta_id = proposta_tb.proposta_id"
    sSql = sSql & " WHERE certificado_tb.dt_emissao IS NOT NULL"
    sSql = sSql & "   AND proposta_tb.dt_contratacao = '" & Format(sDtContratacao, "yyyymmdd") & "'"


    Set ConsultarQtdedePropostasImpressao = Conexao_ExecutarSQL(sSiglaSistema, _
                                                                lAmbienteId, _
                                                                sSiglaRecurso, _
                                                                sDescricaoRecurso, _
                                                                sSql, _
                                                                lNumConexao, _
                                                                True, False, 60000)

    Exit Function

TrataErro:

    Call Err.Raise(Err.Number, , "SEGL0022.cls00165.ConsultarQtdedePropostasImpressao - " & Err.Description)

End Function

Public Function ConsultarQtdedeBrindeCliente(ByVal sSiglaSistema As String, _
                                             ByVal sSiglaRecurso As String, _
                                             ByVal sDescricaoRecurso As String, _
                                             ByVal lAmbienteId As Long, _
                                             ByVal sDtContratacao As String, _
                                             ByVal lNumConexao As Long)

    Dim sSql As String

    On Error GoTo TrataErro

    '    sSql = ""
    '    sSql = sSql & "SELECT count(*) as Brinde"
    '    sSql = sSql & "  FROM proposta_brinde_tb WITH(nolock)"
    '    sSql = sSql & "  JOIN proposta_tb WITH(nolock)"
    '    sSql = sSql & "    ON proposta_brinde_tb.proposta_id = proposta_tb.proposta_id"
    '    sSql = sSql & " WHERE proposta_tb.dt_contratacao = '" & Format(sDtContratacao, "yyyymmdd") & "'"

    sSql = ""
    sSql = sSql & "SELECT COUNT(1) Brinde," & vbNewLine
    sSql = sSql & "       brinde_id" & vbNewLine
    sSql = sSql & "  FROM proposta_brinde_tb WITH(NOLOCK)" & vbNewLine
    sSql = sSql & " WHERE DATEDIFF(DAY, dt_inclusao, '" & Format(sDtContratacao, "yyyymmdd") & "') = 0" & vbNewLine
    sSql = sSql & "   AND brinde_id IN (1, 3)" & vbNewLine
    sSql = sSql & " GROUP BY brinde_id" & vbNewLine

    Set ConsultarQtdedeBrindeCliente = Conexao_ExecutarSQL(sSiglaSistema, _
                                                           lAmbienteId, _
                                                           sSiglaRecurso, _
                                                           sDescricaoRecurso, _
                                                           sSql, _
                                                           lNumConexao, _
                                                           True, False, 60000)

    Exit Function

TrataErro:

    Call Err.Raise(Err.Number, , "SEGL0022.cls00165.ConsultarQtdedeBrindeCliente - " & Err.Description)

End Function

Public Function ConsultarQtdedeCapitalEnviado(ByVal sSiglaSistema As String, _
                                              ByVal sSiglaRecurso As String, _
                                              ByVal sDescricaoRecurso As String, _
                                              ByVal lAmbienteId As Long, _
                                              ByVal iCampanha As Integer, _
                                              ByVal lNumConexao As Long)
    Dim sSql As String

    On Error GoTo TrataErro

    sSql = ""
    'sSql = sSql & "SELECT numero_capitalizacao_atual,numero_capitalizacao_final - numero_capitalizacao_atual AS Capital_Dispon�vel "
    sSql = sSql & "SELECT (SELECT count(1) FROM proposta_capitalizacao_tb WITH(nolock)) AS numero_capitalizacao_atual, "
    sSql = sSql & "       (SELECT numero_capitalizacao_final - count(1) FROM proposta_capitalizacao_tb WITH(nolock))  AS Capital_Dispon�vel "
    sSql = sSql & "  FROM faixa_cap�talizacao_tb WITH(nolock)"
    sSql = sSql & "  JOIN campanha_faixa_capitalizacao_tb WITH(nolock)"
    sSql = sSql & "    ON faixa_cap�talizacao_tb.faixa_id = campanha_faixa_capitalizacao_tb.faixa_id"
    sSql = sSql & " WHERE campanha_id = " & iCampanha


    Set ConsultarQtdedeCapitalEnviado = Conexao_ExecutarSQL(sSiglaSistema, _
                                                            lAmbienteId, _
                                                            sSiglaRecurso, _
                                                            sDescricaoRecurso, _
                                                            sSql, _
                                                            lNumConexao, _
                                                            True, False, 60000)

    Exit Function

TrataErro:

    Call Err.Raise(Err.Number, , "SEGL0022.cls00165.ConsultarQtdedeCapitalEnviado - " & Err.Description)

End Function

Public Function ConsultarClienteCentralAtendimento(ByVal sSiglaSistema As String, _
                                                   ByVal sSiglaRecurso As String, _
                                                   ByVal sDescricaoRecurso As String, _
                                                   ByVal iAmbienteId As Integer, _
                                                   ByVal sConteudo As String, _
                                                   ByVal iSelecao As Integer, _
                                                   ByVal sCampanhaId As String) As Object


    Dim sSql As String
    Dim rsCampanha As Recordset


    On Error GoTo Erro


    If iSelecao = 0 Then  'Segurados n�o contatados

        sSql = "SET NOCOUNT ON EXEC segurados_nao_contatados_sps " & sCampanhaId

    Else

        Set rsCampanha = ConsultarCampanha(sSiglaSistema, _
                                           sSiglaRecurso, _
                                           sDescricaoRecurso, _
                                           iAmbienteId, _
                                           sCampanhaId)

        If rsCampanha.EOF = False Then

            sSql = ""
            sSql = sSql & "SELECT proposta_adesao_tb.proposta_bb," & vbNewLine
            sSql = sSql & "       proposta_tb.proposta_id," & vbNewLine
            sSql = sSql & "       cliente_tb.nome," & vbNewLine
            sSql = sSql & "       proposta_tb.dt_contratacao," & vbNewLine
            sSql = sSql & "       produto_tb.produto_id," & vbNewLine
            sSql = sSql & "       produto_tb.nome nome_produto," & vbNewLine
            sSql = sSql & "       cpf_cnpj = CASE" & vbNewLine
            sSql = sSql & "                     WHEN pessoa_fisica_tb.pf_cliente_id IS NULL THEN pessoa_juridica_tb.cgc" & vbNewLine
            sSql = sSql & "                     ELSE pessoa_fisica_tb.cpf" & vbNewLine
            sSql = sSql & "                  END," & vbNewLine
            sSql = sSql & "       ISNULL(endereco_corresp_tb.endereco, '') endereco," & vbNewLine
            sSql = sSql & "       ISNULL(endereco_corresp_tb.bairro, '') bairro," & vbNewLine
            sSql = sSql & "       ISNULL(endereco_corresp_tb.municipio, '') municipio," & vbNewLine
            sSql = sSql & "       ISNULL(endereco_corresp_tb.estado, '') estado," & vbNewLine
            sSql = sSql & "       ISNULL(endereco_corresp_tb.cep, '') cep," & vbNewLine
            sSql = sSql & "       ISNULL(endereco_corresp_tb.ddd, '') ddd," & vbNewLine
            sSql = sSql & "       ISNULL(endereco_corresp_tb.telefone, '') telefone," & vbNewLine
            sSql = sSql & "       CASE WHEN EXISTS (SELECT 1" & vbNewLine
            sSql = sSql & "                           FROM proposta_brinde_tb WITH(NOLOCK)" & vbNewLine
            sSql = sSql & "                          WHERE proposta_brinde_tb.proposta_id = proposta_tb.proposta_id) THEN 's' " & vbNewLine
            sSql = sSql & "       ELSE 'n' END possui_brinde, " & vbNewLine
            sSql = sSql & "       CASE WHEN EXISTS (SELECT 1" & vbNewLine
            sSql = sSql & "                           FROM evento_seguros_db..evento_tb evento_tb WITH(NOLOCK)" & vbNewLine
            sSql = sSql & "                          WHERE evento_tb.proposta_id = proposta_tb.proposta_id " & vbNewLine
            sSql = sSql & "                            AND evento_tb.tp_evento_id = 04010) THEN 's' " & vbNewLine
            sSql = sSql & "       ELSE 'n' END contato " & vbNewLine
            sSql = sSql & "  FROM proposta_adesao_tb WITH(NOLOCK)" & vbNewLine
            sSql = sSql & " INNER JOIN proposta_tb WITH(NOLOCK)" & vbNewLine
            sSql = sSql & "    ON proposta_tb.proposta_id = proposta_adesao_tb.proposta_id" & vbNewLine
            sSql = sSql & " INNER JOIN produto_tb WITH(NOLOCK)" & vbNewLine
            sSql = sSql & "    ON produto_tb.produto_id = proposta_tb.produto_id" & vbNewLine
            sSql = sSql & " INNER JOIN cliente_tb WITH(NOLOCK)" & vbNewLine
            sSql = sSql & "    ON cliente_tb.cliente_id = proposta_tb.prop_cliente_id" & vbNewLine
            sSql = sSql & "  LEFT JOIN endereco_corresp_tb WITH(NOLOCK)" & vbNewLine
            sSql = sSql & "    ON endereco_corresp_tb.proposta_id = proposta_tb.proposta_id" & vbNewLine
            sSql = sSql & "  LEFT JOIN pessoa_fisica_tb WITH(NOLOCK)" & vbNewLine
            sSql = sSql & "    ON pessoa_fisica_tb.pf_cliente_id = cliente_tb.cliente_id" & vbNewLine
            sSql = sSql & "  LEFT JOIN pessoa_juridica_tb WITH(NOLOCK)" & vbNewLine
            sSql = sSql & "    ON pessoa_juridica_tb.pj_cliente_id = cliente_tb.cliente_id" & vbNewLine
            sSql = sSql & "  JOIN escolha_plano_tb WITH(NOLOCK) " & vbNewLine
            sSql = sSql & "    ON escolha_plano_tb.proposta_id = proposta_tb.proposta_id " & vbNewLine

            If iSelecao = 1 Then  'proposta_id
                sSql = sSql & " WHERE proposta_tb.proposta_id = " & sConteudo & vbNewLine
            ElseIf iSelecao = 2 Then  'Proposta_BB
                sSql = sSql & " WHERE proposta_adesao_tb.proposta_bb = " & sConteudo & vbNewLine

            ElseIf iSelecao = 3 Then    'CPF/CNPJ

                sSql = sSql & "  LEFT JOIN pessoa_fisica_tb WITH(NOLOCK) " & vbNewLine
                sSql = sSql & "    ON pessoa_fisica_tb.pf_cliente_id = cliente_tb.cliente_id" & vbNewLine
                sSql = sSql & "  LEFT JOIN pessoa_juridica_tb WITH(NOLOCK) " & vbNewLine
                sSql = sSql & "    ON pessoa_juridica_tb.pj_cliente_id = cliente_tb.cliente_id" & vbNewLine

                sSql = sSql & " WHERE (pessoa_fisica_tb.cpf = '" & sConteudo & "' " & vbNewLine
                sSql = sSql & "    OR pessoa_juridica_tb.cgc = '" & sConteudo & "') " & vbNewLine

            ElseIf iSelecao = 4 Then    'nome
                sSql = sSql & " WHERE cliente_tb.nome like '" & sConteudo & "%'" & vbNewLine

            ElseIf iSelecao = 5 Then    'Segurado contatado

                sSql = sSql & "  JOIN evento_seguros_db..evento_tb evento_tb WITH(NOLOCK) " & vbNewLine
                sSql = sSql & "    ON evento_tb.proposta_id = proposta_tb.proposta_id " & vbNewLine
                sSql = sSql & " WHERE evento_tb.tp_evento_id = 04010" & vbNewLine

            ElseIf iSelecao = 6 Then    'Segurado contatado e com op��o de escolha

                sSql = sSql & "  JOIN evento_seguros_db..evento_tb evento_tb WITH(NOLOCK) " & vbNewLine
                sSql = sSql & "    ON evento_tb.proposta_id = proposta_tb.proposta_id " & vbNewLine
                sSql = sSql & " WHERE evento_tb.tp_evento_id = 04011" & vbNewLine

            ElseIf iSelecao = 7 Then    'Segurado contatado e com op��o de escolha e sem registro de recebimento

                sSql = sSql & "  JOIN evento_seguros_db..evento_tb evento_tb WITH(NOLOCK) " & vbNewLine
                sSql = sSql & "    ON evento_tb.proposta_id = proposta_tb.proposta_id " & vbNewLine
                sSql = sSql & " WHERE evento_tb.tp_evento_id = 04011" & vbNewLine
                sSql = sSql & "   AND NOT EXISTS (SELECT 1 " & vbNewLine
                sSql = sSql & "                     FROM evento_seguros_db..evento_tb evento_tb2 WITH(NOLOCK) " & vbNewLine
                sSql = sSql & "                    WHERE evento_tb2.proposta_id = proposta_tb.proposta_id " & vbNewLine
                sSql = sSql & "                      AND evento_tb2.tp_evento_id = 01001) " & vbNewLine

            End If

            sSql = sSql & "   AND proposta_tb.produto_id IN (716,135) " & vbNewLine
            sSql = sSql & "   AND proposta_adesao_tb.val_is >= 75000 " & vbNewLine
            sSql = sSql & "   AND proposta_tb.canal_venda = 3 " & vbNewLine
            sSql = sSql & "   AND proposta_tb.situacao = 'i' " & vbNewLine
            sSql = sSql & "   AND escolha_plano_tb.dt_fim_vigencia IS NULL " & vbNewLine
            sSql = sSql & "   AND DATEDIFF(DAY, proposta_tb.dt_contratacao, '" & Format(rsCampanha("dt_inicio_vigencia"), "yyyymmdd") & "') <= 0 " & vbNewLine
            sSql = sSql & "   AND DATEDIFF(DAY, proposta_tb.dt_contratacao, '" & Format(rsCampanha("dt_fim_vigencia"), "yyyymmdd") & "') >= 0 " & vbNewLine

            Set rsCampanha = Nothing

        End If

    End If


    Set ConsultarClienteCentralAtendimento = ExecutarSQL(sSiglaSistema, _
                                                         iAmbienteId, _
                                                         sSiglaRecurso, _
                                                         sDescricaoRecurso, _
                                                         sSql, _
                                                         True, False, 60000)

    Exit Function

Erro:

    Call Err.Raise(Err.Number, , "SEGL0022.cls00165.ConsultarClienteCentralAtendimento - " & Err.Description)

End Function


Public Function ConsultarEvento(ByVal sSiglaSistema As String, _
                                ByVal sSiglaRecurso As String, _
                                ByVal sDescricaoRecurso As String, _
                                ByVal iAmbienteId As Integer, _
                                ByVal sPropostaId As String) As Object

    Dim sSql As String

    On Error GoTo Erro

    sSql = ""
    sSql = sSql & "SELECT tp_evento_tb.tp_evento_id," & vbNewLine
    sSql = sSql & "       tp_evento_tb.nome," & vbNewLine
    sSql = sSql & "       ISNULL(evento_tb.valor1, '') +"
    sSql = sSql & "              ISNULL(evento_tb.valor2, '') +"
    sSql = sSql & "              ISNULL(evento_tb.valor3, '') +"
    sSql = sSql & "              ISNULL(evento_tb.valor4, '') +"
    sSql = sSql & "              ISNULL(evento_tb.valor5, '') +"
    sSql = sSql & "              ISNULL(evento_tb.valor6, '') +"
    sSql = sSql & "              ISNULL(evento_tb.valor7, '') +"
    sSql = sSql & "              ISNULL(evento_tb.valor8, '') +"
    sSql = sSql & "              ISNULL(evento_tb.valor9, '') +"
    sSql = sSql & "              ISNULL(evento_tb.valor10, '') AS valor"
    sSql = sSql & "  FROM proposta_tb WITH(nolock) " & vbNewLine
    sSql = sSql & "  JOIN evento_seguros_db..evento_tb evento_tb WITH(nolock) " & vbNewLine
    sSql = sSql & "    ON proposta_tb.proposta_id = evento_tb.proposta_id" & vbNewLine
    sSql = sSql & "  JOIN evento_seguros_db..tp_evento_tb tp_evento_tb WITH(nolock) " & vbNewLine
    sSql = sSql & "    ON evento_tb.tp_evento_id = tp_evento_tb.tp_evento_id" & vbNewLine
    sSql = sSql & " WHERE proposta_tb.proposta_id = " & sPropostaId

    Set ConsultarEvento = ExecutarSQL(sSiglaSistema, _
                                      iAmbienteId, _
                                      sSiglaRecurso, _
                                      sDescricaoRecurso, _
                                      sSql, _
                                      True, False, 60000)

    Exit Function

Erro:

    Call Err.Raise(Err.Number, , "SEGL0022.cls00165.ConsultarEvento - " & Err.Description)

End Function

Public Sub IncluirEvento(ByVal sSiglaSistema As String, _
                         ByVal sSiglaRecurso As String, _
                         ByVal sDescricaoRecurso As String, _
                         ByVal lAmbienteId As Long, _
                         ByVal sPropostaId As String, _
                         ByVal sPropostaBB As String, _
                         ByVal sTpEventoId As String, _
                         ByVal sProdutoId As String, _
                         ByVal sUsuario As String, _
                         ByVal sDtEvento As String, _
                         ByVal sValor1 As String, _
                         Optional ByVal sValor2 As String = "", _
                         Optional ByVal sValor3 As String = "", _
                         Optional ByVal sValor4 As String = "", _
                         Optional ByVal sValor5 As String = "", _
                         Optional ByVal sValor6 As String = "")

    Dim sSql As String

    On Error GoTo Trata_Erro

    '@evento_padrao int,

    sSql = "SET NOCOUNT ON " & vbNewLine
    sSql = sSql & "EXEC evento_seguros_db..evento_spi "
    sSql = sSql & sPropostaId
    sSql = sSql & ", " & sPropostaBB
    sSql = sSql & ", " & sTpEventoId
    sSql = sSql & ", " & sProdutoId
    sSql = sSql & ", " & "NULL"
    sSql = sSql & ", " & "NULL"
    sSql = sSql & ",'" & sValor1 & "'"
    sSql = sSql & ",'" & sValor2 & "'"
    sSql = sSql & ",'" & sValor3 & "'"
    sSql = sSql & ",'" & sValor4 & "'"
    sSql = sSql & ",'" & sValor5 & "'"
    sSql = sSql & ",'" & sValor6 & "'"
    sSql = sSql & ",'" & sUsuario & "'"
    sSql = sSql & ",'" & Format(sDtEvento, "yyyymmdd") & "'"

    Call ExecutarSQL(sSiglaSistema, _
                     lAmbienteId, _
                     sSiglaRecurso, _
                     sDescricaoRecurso, _
                     sSql, _
                     False, False, 60000)


    Exit Sub

Trata_Erro:

    Call Err.Raise(Err.Number, , "SEGL0022.cls00165.IncluirEvento - " & Err.Description)

End Sub


Public Sub ApagarTemporariaConsultaSintetica(ByVal sSiglaSistema As String, _
                                             ByVal sSiglaRecurso As String, _
                                             ByVal sDescricaoRecurso As String, _
                                             ByVal lAmbienteId As Long)

    Dim sSql As String

    On Error GoTo Trata_Erro

    sSql = "SET NOCOUNT ON " & vbNewLine
    sSql = sSql & "IF EXISTS(SELECT 1 "
    sSql = sSql & "            FROM tempdb..sysobjects"
    sSql = sSql & "           WHERE NAME = '##consulta_vida') DROP TABLE ##consulta_vida"

    Call ExecutarSQL(sSiglaSistema, _
                     lAmbienteId, _
                     sSiglaRecurso, _
                     sDescricaoRecurso, _
                     sSql, _
                     False, False, 60000)


    sSql = "SET NOCOUNT ON " & vbNewLine
    sSql = sSql & "IF EXISTS(SELECT 1 "
    sSql = sSql & "            FROM tempdb..sysobjects"
    sSql = sSql & "           WHERE NAME = '##consulta_re') DROP TABLE ##consulta_re"

    Call ExecutarSQL(sSiglaSistema, _
                     lAmbienteId, _
                     sSiglaRecurso, _
                     sDescricaoRecurso, _
                     sSql, _
                     False, False, 60000)


    sSql = "SET NOCOUNT ON " & vbNewLine
    sSql = sSql & "IF EXISTS(SELECT 1 "
    sSql = sSql & "            FROM tempdb..sysobjects"
    sSql = sSql & "           WHERE NAME = '##consulta_grid') DROP TABLE ##consulta_grid"

    Call ExecutarSQL(sSiglaSistema, _
                     lAmbienteId, _
                     sSiglaRecurso, _
                     sDescricaoRecurso, _
                     sSql, _
                     False, False, 60000)

    Exit Sub

Trata_Erro:

    Call Err.Raise(Err.Number, , "SEGL0022.cls00165.ApagarTemporariaConsultaSintetica - " & Err.Description)

End Sub

Public Sub CriarTemporariaConsultaSintetica(ByVal sSiglaSistema As String, _
                                            ByVal sSiglaRecurso As String, _
                                            ByVal sDescricaoRecurso As String, _
                                            ByVal lAmbienteId As Long, _
                                            ByVal sDtInicio As String, _
                                            ByVal sDtFim As String, _
                                            ByVal lNumConexao As Long)

    Dim sSql As String

    On Error GoTo Trata_Erro

    sSql = "SET NOCOUNT ON " & vbNewLine
    sSql = sSql & "EXEC consulta_sintetica_campanha_sps "
    sSql = sSql & "'" & Format(sDtInicio, "yyyymmdd") & "'"
    sSql = sSql & ",'" & Format(sDtFim, "yyyymmdd") & "'"

    Call Conexao_ExecutarSQL(sSiglaSistema, _
                             lAmbienteId, _
                             sSiglaRecurso, _
                             sDescricaoRecurso, _
                             sSql, _
                             lNumConexao, _
                             False, False, 60000)

    Exit Sub

Trata_Erro:

    Call Err.Raise(Err.Number, , "SEGL0022.cls00165.CriarTemporariaConsultaSintetica - " & Err.Description)

End Sub

Public Function abrirConexaoInicial(ByVal sSiglaSistema As String, _
                                    ByVal sSiglaRecurso As String, _
                                    ByVal sDescricaoRecurso As String, _
                                    ByVal lAmbienteId As Long) As Long



    On Error GoTo Trata_Erro

    abrirConexaoInicial = AbrirConexao(sSiglaSistema, _
                                       lAmbienteId, _
                                       sSiglaRecurso, _
                                       sDescricaoRecurso)

    Exit Function

Trata_Erro:

    Call Err.Raise(Err.Number, , "SEGL0022.cls00165.abrirConexaoInicial - " & Err.Description)

End Function


Public Sub fecharConexaoInicial(ByVal lNumConexao As Long)

    Dim bTesteConexao

    On Error GoTo Trata_Erro

    bTesteConexao = FecharConexao(lNumConexao)

    Exit Sub

Trata_Erro:

    Call Err.Raise(Err.Number, , "SEGL0022.cls00165.fecharConexaoInicial - " & Err.Description)

End Sub

Public Function ConsultarDadosSinteticos(ByVal sSiglaSistema As String, _
                                         ByVal sSiglaRecurso As String, _
                                         ByVal sDescricaoRecurso As String, _
                                         ByVal lAmbienteId As Long, _
                                         ByVal sDtInicio As String, _
                                         ByVal sDtFim As String, _
                                         ByVal lNumConexao As Long, _
                                         ByVal bCorretagem As Boolean) As Object

    Dim sSql As String

    On Error GoTo Trata_Erro

    If bCorretagem Then
        sSql = "SET NOCOUNT ON " & vbNewLine
        sSql = sSql & "EXEC consulta_sintetica_campanha_sps "
        sSql = sSql & "'" & Format(sDtInicio, "yyyymmdd") & "'"
        sSql = sSql & ",'" & Format(sDtFim, "yyyymmdd") & "'"
    Else
        sSql = "SET NOCOUNT ON " & vbNewLine
        sSql = sSql & "EXEC consulta_sintetica_campanha_sem_corretagem_sps "
        sSql = sSql & "'" & Format(sDtInicio, "yyyymmdd") & "'"
        sSql = sSql & ",'" & Format(sDtFim, "yyyymmdd") & "'"
    End If

    Set ConsultarDadosSinteticos = Conexao_ExecutarSQL(sSiglaSistema, _
                                                       lAmbienteId, _
                                                       sSiglaRecurso, _
                                                       sDescricaoRecurso, _
                                                       sSql, _
                                                       lNumConexao, _
                                                       True, False, 60000)

    Exit Function

Trata_Erro:

    Call Err.Raise(Err.Number, , "SEGL0022.cls00165.ConsultarDadosSinteticos - " & Err.Description)

End Function

Public Sub IncluirBrinde(ByVal sSiglaSistema As String, _
                         ByVal sSiglaRecurso As String, _
                         ByVal sDescricaoRecurso As String, _
                         ByVal lAmbienteId As Long, _
                         ByVal sPropostaId As String, _
                         ByVal sBrindeId As String, _
                         ByVal sUsuario As String, _
                         Optional ByVal sDtBrinde As String)

    Dim sSql As String

    On Error GoTo Trata_Erro


    sSql = "SET NOCOUNT ON " & vbNewLine
    sSql = sSql & "EXEC proposta_brinde_spi "
    sSql = sSql & sPropostaId
    sSql = sSql & ", " & sBrindeId
    sSql = sSql & ",'" & sUsuario & "'"

    If sDtBrinde <> "" Then
        sSql = sSql & ",'" & Format(sDtBrinde, "yyyymmdd") & "'"
    End If

    Call ExecutarSQL(sSiglaSistema, _
                     lAmbienteId, _
                     sSiglaRecurso, _
                     sDescricaoRecurso, _
                     sSql, _
                     False, False, 60000)


    Exit Sub

Trata_Erro:

    Call Err.Raise(Err.Number, , "SEGL0022.cls00165.IncluirBrinde - " & Err.Description)

End Sub

Public Function ConsultarCampanha(ByVal sSiglaSistema As String, _
                                  ByVal sSiglaRecurso As String, _
                                  ByVal sDescricaoRecurso As String, _
                                  ByVal iAmbienteId As Integer, _
                                  ByVal sCampanhaId As String) As Object

    Dim sSql As String

    On Error GoTo Erro

    sSql = ""
    sSql = sSql & "SELECT descricao, " & vbNewLine
    sSql = sSql & "       dt_inicio_vigencia, " & vbNewLine
    sSql = sSql & "       dt_fim_vigencia " & vbNewLine
    sSql = sSql & "  FROM campanha_promocional_tb WITH(nolock) " & vbNewLine
    sSql = sSql & " WHERE campanha_id = " & sCampanhaId

    Set ConsultarCampanha = ExecutarSQL(sSiglaSistema, _
                                        iAmbienteId, _
                                        sSiglaRecurso, _
                                        sDescricaoRecurso, _
                                        sSql, _
                                        True, False, 60000)

    Exit Function

Erro:

    Call Err.Raise(Err.Number, , "SEGL0022.cls00165.ConsultarCampanha - " & Err.Description)

End Function
