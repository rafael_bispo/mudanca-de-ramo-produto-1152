VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cls00393"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private lConexao As Long

Public Function ConsultarTipoEventoEmail(ByVal sSiglaSistema As String, _
                                         ByVal sSiglaRecurso As String, _
                                         ByVal sDescricaoRecurso As String, _
                                         ByVal iAmbienteId As Integer, _
                                         ByVal iTipoPesquisa As Byte, _
                                         ByVal sConteudo As String) As Recordset

'## Rotina para Consultar em tp_evento_email_tb

    Dim sSql As String

    On Error GoTo Trata_Erro

    sSql = ""
    sSql = sSql & "    SELECT tp_evento_email_id, " & vbNewLine
    sSql = sSql & "           nome " & vbNewLine
    sSql = sSql & "     FROM tp_evento_email_tb  WITH(NOLOCK) " & vbNewLine

    If iTipoPesquisa = 1 Then
        sSql = sSql & "    WHERE tp_evento_email_id = " & sConteudo & vbNewLine
    ElseIf iTipoPesquisa = 2 Then
        sSql = sSql & "    WHERE nome LIKE '%" & sConteudo & "%' " & vbNewLine
    End If

    Set ConsultarTipoEventoEmail = ExecutarSQL(sSiglaSistema, _
                                               iAmbienteId, _
                                               sSiglaRecurso, _
                                               sDescricaoRecurso, _
                                               sSql, _
                                               True, False, 60000)

    Exit Function

Trata_Erro:
    Call Err.Raise(Err.Number, , "SEGL0022.cls00393.ConsultarTipoEventoEmail - " & Err.Description)

End Function

Public Function VerificarExistenciaEmailAlerta(ByVal sSiglaSistema As String, _
                                               ByVal sSiglaRecurso As String, _
                                               ByVal sDescricaoRecurso As String, _
                                               ByVal iAmbienteId As Integer, _
                                               ByVal iTpEventoId As Integer) As Boolean

'## Rotina para Verificar a exist�ncia de e-mail em subramo_produto_email_tb

    Dim sSql As String
    Dim rs As Recordset

    On Error GoTo Trata_Erro

    sSql = ""
    sSql = sSql & "    SELECT COUNT(1) " & vbNewLine
    sSql = sSql & "      FROM subramo_produto_email_tb  WITH(NOLOCK) " & vbNewLine
    sSql = sSql & "     WHERE tp_evento_email_id = " & iTpEventoId & vbNewLine

    Set rs = ExecutarSQL(sSiglaSistema, _
                         iAmbienteId, _
                         sSiglaRecurso, _
                         sDescricaoRecurso, _
                         sSql, _
                         True, False, 60000)

    If rs(0) = 0 Then
        VerificarExistenciaEmailAlerta = False
    Else
        VerificarExistenciaEmailAlerta = True
    End If

    Set rs = Nothing

    Exit Function

Trata_Erro:
    Call Err.Raise(Err.Number, , "SEGL0022.cls00393.VerificarExistenciaEmailAlerta - " & Err.Description)

End Function

Public Sub ExcluirEventoEmail(ByVal sSiglaSistema As String, _
                              ByVal sSiglaRecurso As String, _
                              ByVal sDescricaoRecurso As String, _
                              ByVal iAmbienteId As Integer, _
                              ByVal iTpEventoEmailId As Integer)

'## M�todo para excluir em tp_evento_email_tb

    Dim sSql As String

    On Error GoTo Trata_Erro

    sSql = ""
    sSql = sSql & "EXEC tp_evento_email_spd " & vbNewLine
    sSql = sSql & iTpEventoEmailId

    Call ExecutarSQL(sSiglaSistema, _
                     iAmbienteId, _
                     sSiglaRecurso, _
                     sDescricaoRecurso, _
                     sSql, _
                     False, False, 60000)

    Exit Sub

Trata_Erro:

    Call Err.Raise(Err.Number, , "SEGL0022.cls00393.ExcluirEventoEmail - " & Err.Description)

End Sub

Public Function ObterSubramoProduto(ByVal sSiglaSistema As String, _
                                    ByVal sSiglaRecurso As String, _
                                    ByVal sDescricaoRecurso As String, _
                                    ByVal iAmbienteId As Integer, _
                                    Optional ByVal sEnviaAlerta As String = "") As Recordset

'## Rotina para obter todos os subramos_produtos

    Dim sSql As String

    On Error GoTo Trata_Erro

    sSql = ""
    sSql = sSql & "SELECT subramo_produto_tb.produto_id," & vbNewLine
    sSql = sSql & "       subramo_produto_tb.ramo_id," & vbNewLine
    sSql = sSql & "       subramo_produto_tb.subramo_id," & vbNewLine
    sSql = sSql & "       subramo_produto_tb.dt_inicio_vigencia_sbr," & vbNewLine
    sSql = sSql & "       subramo_produto_tb.nome," & vbNewLine
    sSql = sSql & "       subramo_produto_tb.envia_email_alerta," & vbNewLine
    sSql = sSql & "       subramo_produto_tb.qtd_dias_alerta" & vbNewLine
    sSql = sSql & "  FROM subramo_produto_tb WITH(NOLOCK) " & vbNewLine
    sSql = sSql & " INNER JOIN subramo_tb WITH(NOLOCK) " & vbNewLine
    sSql = sSql & "    ON subramo_tb.ramo_id = subramo_produto_tb.ramo_id" & vbNewLine
    sSql = sSql & "   AND subramo_tb.subramo_id = subramo_produto_tb.subramo_id" & vbNewLine
    sSql = sSql & "   AND subramo_tb.dt_inicio_vigencia_sbr = subramo_produto_tb.dt_inicio_vigencia_sbr" & vbNewLine
    sSql = sSql & " WHERE subramo_tb.dt_fim_vigencia_sbr IS NULL" & vbNewLine

    If sEnviaAlerta <> "" Then
        sSql = sSql & "   AND subramo_produto_tb.envia_email_alerta = '" & sEnviaAlerta & "'" & vbNewLine
    End If

    sSql = sSql & "     ORDER BY subramo_produto_tb.produto_id," & vbNewLine
    sSql = sSql & "              subramo_produto_tb.subramo_id" & vbNewLine

    Set ObterSubramoProduto = ExecutarSQL(sSiglaSistema, _
                                          iAmbienteId, _
                                          sSiglaRecurso, _
                                          sDescricaoRecurso, _
                                          sSql, _
                                          True, False, 60000)

    Exit Function

Trata_Erro:
    Call Err.Raise(Err.Number, , "SEGL0022.cls00393.ObterSubramoProduto - " & Err.Description)

End Function

Public Function ObterProximoEventoEmailId(ByVal sSiglaSistema As String, _
                                          ByVal sSiglaRecurso As String, _
                                          ByVal sDescricaoRecurso As String, _
                                          ByVal iAmbienteId As Integer) As Integer

'## Rotina para obter o proximo valor Id de tp_evento_email_tb

    Dim sSql As String
    Dim rs As Recordset


    On Error GoTo Trata_Erro

    sSql = ""
    sSql = sSql & "    SELECT CASE " & vbNewLine
    sSql = sSql & "               WHEN IDENT_CURRENT('tp_evento_email_tb') IS NULL " & vbNewLine
    sSql = sSql & "               THEN 1 " & vbNewLine
    sSql = sSql & "               ELSE IDENT_CURRENT('tp_evento_email_tb') + 1 " & vbNewLine
    sSql = sSql & "            END " & vbNewLine

    Set rs = ExecutarSQL(sSiglaSistema, _
                         iAmbienteId, _
                         sSiglaRecurso, _
                         sDescricaoRecurso, _
                         sSql, _
                         True, False, 60000)

    ObterProximoEventoEmailId = rs(0)

    Exit Function

Trata_Erro:
    Call Err.Raise(Err.Number, , "SEGL0022.cls00393.ObterProximoEventoEmailId - " & Err.Description)

End Function

Public Sub IncluirEventoEmail(ByVal sSiglaSistema As String, _
                              ByVal sSiglaRecurso As String, _
                              ByVal sDescricaoRecurso As String, _
                              ByVal iAmbienteId As Integer, _
                              ByVal sNome As String, _
                              ByVal sUsuario As String)

'## M�todo para incluir em tp_evento_email_tb

    Dim sSql As String

    On Error GoTo Trata_Erro

    sSql = ""
    sSql = sSql & " EXEC tp_evento_email_spi " & vbNewLine
    sSql = sSql & "'" & sNome & "'," & vbNewLine
    sSql = sSql & "'" & sUsuario & "'" & vbNewLine

    Call ExecutarSQL(sSiglaSistema, _
                     iAmbienteId, _
                     sSiglaRecurso, _
                     sDescricaoRecurso, _
                     sSql, _
                     False, False, 60000)

    Exit Sub

Trata_Erro:

    Call Err.Raise(Err.Number, , "SEGL0022.cls00393.IncluirEventoEmail - " & Err.Description)

End Sub

Public Sub AlterarEventoEmail(ByVal sSiglaSistema As String, _
                              ByVal sSiglaRecurso As String, _
                              ByVal sDescricaoRecurso As String, _
                              ByVal iAmbienteId As Integer, _
                              ByVal iTpEventoEmailId As Integer, _
                              ByVal sNome As String, _
                              ByVal sUsuario As String)

'## M�todo para alterar em tp_evento_email_tb

    Dim sSql As String

    On Error GoTo Trata_Erro

    sSql = ""
    sSql = sSql & " EXEC tp_evento_email_spu " & vbNewLine
    sSql = sSql & iTpEventoEmailId & "," & vbNewLine
    sSql = sSql & "'" & sNome & "'," & vbNewLine
    sSql = sSql & "'" & sUsuario & "'" & vbNewLine

    Call ExecutarSQL(sSiglaSistema, _
                     iAmbienteId, _
                     sSiglaRecurso, _
                     sDescricaoRecurso, _
                     sSql, _
                     False, False, 60000)

    Exit Sub

Trata_Erro:

    Call Err.Raise(Err.Number, , "SEGL0022.cls00393.IncluirEventoEmail - " & Err.Description)

End Sub

Public Sub AtualizarSubramoProduto(ByVal sSiglaSistema As String, _
                                   ByVal sSiglaRecurso As String, _
                                   ByVal sDescricaoRecurso As String, _
                                   ByVal iAmbienteId As Integer, _
                                   ByVal lProdutoId As Long, _
                                   ByVal lRamoId As Long, _
                                   ByVal lSubRamoId As Long, _
                                   ByVal sDataInicioVigencia As String, _
                                   ByVal sUsuario As String, _
                                   ByRef sEnviaEmailAlerta As String, _
                                   ByVal iQtdDiasAlerta As Integer)

'## M�todo para atualizar em subramo_produto_tb

    Dim sSql As String

    On Error GoTo Trata_Erro

    sSql = ""
    sSql = sSql & "EXEC subramo_produto_alerta_spu" & vbNewLine
    sSql = sSql & lProdutoId & "," & vbNewLine
    sSql = sSql & lRamoId & "," & vbNewLine
    sSql = sSql & lSubRamoId & "," & vbNewLine
    sSql = sSql & "'" & Format(sDataInicioVigencia, "yyyymmdd") & "'," & vbNewLine
    sSql = sSql & "'" & sUsuario & "'," & vbNewLine
    sSql = sSql & "'" & sEnviaEmailAlerta & "'," & vbNewLine
    sSql = sSql & iQtdDiasAlerta & vbNewLine

    Call Conexao_ExecutarSQL(sSiglaSistema, _
                             iAmbienteId, _
                             sSiglaRecurso, _
                             sDescricaoRecurso, _
                             sSql, _
                             lConexao, _
                             False, False, 60000)

    Exit Sub

Trata_Erro:

    Call Err.Raise(Err.Number, , "SEGL0022.cls00393.IncluirEventoEmail - " & Err.Description)

End Sub

Public Function ConsultarSubRamoProdutoEmail(ByVal sSiglaSistema As String, _
                                             ByVal sSiglaRecurso As String, _
                                             ByVal sDescricaoRecurso As String, _
                                             ByVal iAmbienteId As Integer, _
                                             ByVal lProdutoId As Long, _
                                             ByVal lRamoId As Long, _
                                             ByVal lSubRamoId As Long, _
                                             ByVal sDataInicioVigencia As String, _
                                             ByVal iTpEventoEmail As Integer) As Recordset

'## Rotina para Consultar em subramo_produto_email_tb

    Dim sSql As String

    On Error GoTo Trata_Erro

    sSql = ""
    sSql = sSql & "SELECT subramo_produto_email_id," & vbNewLine
    sSql = sSql & "       email" & vbNewLine
    sSql = sSql & "  FROM subramo_produto_email_tb WITH(NOLOCK) " & vbNewLine
    sSql = sSql & " WHERE produto_id = " & lProdutoId & vbNewLine
    sSql = sSql & "   AND ramo_id = " & lRamoId & vbNewLine
    sSql = sSql & "   AND subramo_id = " & lSubRamoId & vbNewLine
    sSql = sSql & "   AND dt_inicio_vigencia_sbr = '" & Format(sDataInicioVigencia, "yyyymmdd") & "'" & vbNewLine
    sSql = sSql & "   AND tp_evento_email_id = " & iTpEventoEmail & vbNewLine
    sSql = sSql & " ORDER BY subramo_produto_email_id" & vbNewLine

    Set ConsultarSubRamoProdutoEmail = ExecutarSQL(sSiglaSistema, _
                                                   iAmbienteId, _
                                                   sSiglaRecurso, _
                                                   sDescricaoRecurso, _
                                                   sSql, _
                                                   True, False, 60000)

    Exit Function

Trata_Erro:
    Call Err.Raise(Err.Number, , "SEGL0022.cls00393.ConsultarSubRamoProdutoEmail - " & Err.Description)

End Function

Public Sub IncluirSubramoProdutoEmail(ByVal sSiglaSistema As String, _
                                      ByVal sSiglaRecurso As String, _
                                      ByVal sDescricaoRecurso As String, _
                                      ByVal iAmbienteId As Integer, _
                                      ByVal lProdutoId As Long, _
                                      ByVal lRamoId As Long, _
                                      ByVal lSubRamoId As Long, _
                                      ByVal sDataInicioVigencia As String, _
                                      ByVal iTpEventoEmailId As Integer, _
                                      ByVal sEmail As String, _
                                      ByVal sUsuario As String)

'## M�todo para incluir em subramo_produto_email_tb

    Dim sSql As String

    On Error GoTo Trata_Erro

    sSql = ""
    sSql = sSql & "EXEC subramo_produto_email_spi" & vbNewLine
    sSql = sSql & lProdutoId & "," & vbNewLine
    sSql = sSql & lRamoId & "," & vbNewLine
    sSql = sSql & lSubRamoId & "," & vbNewLine
    sSql = sSql & "'" & Format(sDataInicioVigencia, "yyyymmdd") & "'," & vbNewLine
    sSql = sSql & iTpEventoEmailId & "," & vbNewLine
    sSql = sSql & "'" & sEmail & "'," & vbNewLine
    sSql = sSql & "'" & sUsuario & "'" & vbNewLine

    Call Conexao_ExecutarSQL(sSiglaSistema, _
                             iAmbienteId, _
                             sSiglaRecurso, _
                             sDescricaoRecurso, _
                             sSql, _
                             lConexao, _
                             False, False, 60000)

    Exit Sub

Trata_Erro:

    Call Err.Raise(Err.Number, , "SEGL0022.cls00393.IncluirSubramoProdutoEmail - " & Err.Description)

End Sub

Public Sub AlterarSubramoProdutoEmail(ByVal sSiglaSistema As String, _
                                      ByVal sSiglaRecurso As String, _
                                      ByVal sDescricaoRecurso As String, _
                                      ByVal iAmbienteId As Integer, _
                                      ByVal lSubRamoProdutoEmailId As Long, _
                                      ByVal sEmail As String, _
                                      ByVal sUsuario As String)

'## M�todo para altera em subramo_produto_email_tb

    Dim sSql As String

    On Error GoTo Trata_Erro

    sSql = ""
    sSql = sSql & "EXEC subramo_produto_email_spu" & vbNewLine
    sSql = sSql & lSubRamoProdutoEmailId & "," & vbNewLine
    sSql = sSql & "'" & sEmail & "'," & vbNewLine
    sSql = sSql & "'" & sUsuario & "'" & vbNewLine

    Call Conexao_ExecutarSQL(sSiglaSistema, _
                             iAmbienteId, _
                             sSiglaRecurso, _
                             sDescricaoRecurso, _
                             sSql, _
                             lConexao, _
                             False, False, 60000)

    Exit Sub

Trata_Erro:

    Call Err.Raise(Err.Number, , "SEGL0022.cls00393.AlterarSubramoProdutoEmail - " & Err.Description)

End Sub

Public Sub ExcluirSubramoProdutoEmail(ByVal sSiglaSistema As String, _
                                      ByVal sSiglaRecurso As String, _
                                      ByVal sDescricaoRecurso As String, _
                                      ByVal iAmbienteId As Integer, _
                                      ByVal lSubRamoProdutoEmailId As Long)

'## M�todo para excluir em subramo_produto_email_tb

    Dim sSql As String

    On Error GoTo Trata_Erro

    sSql = ""
    sSql = sSql & "EXEC subramo_produto_email_spd" & vbNewLine
    sSql = sSql & lSubRamoProdutoEmailId & vbNewLine

    Call Conexao_ExecutarSQL(sSiglaSistema, _
                             iAmbienteId, _
                             sSiglaRecurso, _
                             sDescricaoRecurso, _
                             sSql, _
                             lConexao, _
                             False, False, 60000)

    Exit Sub

Trata_Erro:

    Call Err.Raise(Err.Number, , "SEGL0022.cls00393.ExcluirSubramoProdutoEmail - " & Err.Description)

End Sub

Public Function ObterApolicesAlertaRenovacao(ByVal sSiglaSistema As String, _
                                             ByVal sSiglaRecurso As String, _
                                             ByVal sDescricaoRecurso As String, _
                                             ByVal iAmbienteId As Integer, _
                                             ByVal sDataSistema As String, _
                                             ByVal lSubRamoId As Long, _
                                             ByVal lProdutoId As Long, _
                                             ByVal iRamoId As Integer, _
                                             ByVal sDtInicioVigenciaSbr As String, _
                                             ByVal iQtdDiasAlerta As Integer) As Recordset

'Rotina para obter as ap�lices que v�o vencer de um determinado subramo_produto

    Dim sSql As String

    On Error GoTo Trata_Erro

    sSql = ""
    sSql = sSql & "SELECT apolice_tb.apolice_id," & vbNewLine
    sSql = sSql & "       apolice_tb.proposta_id," & vbNewLine
    sSql = sSql & "       ramo_tb.nome nome_ramo," & vbNewLine
    sSql = sSql & "       produto_tb.nome nome_produto," & vbNewLine
    sSql = sSql & "       cliente_tb.nome estipulante" & vbNewLine
    sSql = sSql & "  FROM apolice_tb WITH(NOLOCK)" & vbNewLine
    sSql = sSql & " INNER JOIN proposta_tb WITH(NOLOCK)" & vbNewLine
    sSql = sSql & "    ON proposta_tb.proposta_id = apolice_tb.proposta_id" & vbNewLine
    sSql = sSql & " INNER JOIN ramo_tb WITH(NOLOCK)" & vbNewLine
    sSql = sSql & "    ON ramo_tb.ramo_id = apolice_tb.ramo_id" & vbNewLine
    sSql = sSql & " INNER JOIN produto_tb WITH(NOLOCK)" & vbNewLine
    sSql = sSql & "    ON produto_tb.produto_id = proposta_tb.produto_id" & vbNewLine
    sSql = sSql & " INNER JOIN administracao_apolice_tb WITH(NOLOCK)" & vbNewLine
    sSql = sSql & "    ON administracao_apolice_tb.proposta_id = proposta_tb.proposta_id" & vbNewLine
    sSql = sSql & " INNER JOIN cliente_tb WITH(NOLOCK)" & vbNewLine
    sSql = sSql & "    ON cliente_tb.cliente_id = administracao_apolice_tb.est_cliente_id" & vbNewLine
    sSql = sSql & " WHERE CASE WHEN apolice_tb.dt_fim_vigencia IS NOT NULL" & vbNewLine
    sSql = sSql & "            THEN DATEDIFF(DAY, '" & sDataSistema & "', apolice_tb.dt_fim_vigencia)" & vbNewLine
    sSql = sSql & "            ELSE DATEDIFF(DAY, '" & sDataSistema & "', CONVERT(SMALLDATETIME, CONVERT(VARCHAR(4), '" & sDataSistema & "', 112) + " & vbNewLine
    sSql = sSql & "                 RIGHT(CONVERT(VARCHAR(8), apolice_tb.dt_inicio_vigencia, 112), 4)))" & vbNewLine
    sSql = sSql & "       END = " & iQtdDiasAlerta & vbNewLine
    sSql = sSql & "   AND proposta_tb.ramo_id = " & iRamoId & vbNewLine
    sSql = sSql & "   AND proposta_tb.produto_id = " & lProdutoId & vbNewLine
    sSql = sSql & "   AND proposta_tb.subramo_id = " & lSubRamoId & vbNewLine
    sSql = sSql & "   AND proposta_tb.dt_inicio_vigencia_sbr = '" & sDtInicioVigenciaSbr & "'" & vbNewLine
    sSql = sSql & "   AND ramo_tb.tp_ramo_id = 1" & vbNewLine

    Set ObterApolicesAlertaRenovacao = ExecutarSQL(sSiglaSistema, _
                                                   iAmbienteId, _
                                                   sSiglaRecurso, _
                                                   sDescricaoRecurso, _
                                                   sSql, _
                                                   True, False, 60000)

    Exit Function

Trata_Erro:

    Call Err.Raise(Err.Number, , "SEGL0022.cls00393.ObterApolicesAlertaRenovacao - " & Err.Description)

End Function

Public Sub EnviarEmail(ByVal sSiglaSistema As String, _
                       ByVal sSiglaRecurso As String, _
                       ByVal sDescricaoRecurso As String, _
                       ByVal iAmbienteId As Integer, _
                       ByVal sDestinatario As String, _
                       ByVal sAssunto As String, _
                       ByVal sMsg As String)

    Dim sSql As String

    On Error GoTo Trata_Erro

    sSql = ""
    sSql = sSql & " SET NOCOUNT ON EXEC seguros_db..envia_email_sp "
    sSql = sSql & "'" & sDestinatario & "',"
    sSql = sSql & "'" & sAssunto & "',"
    sSql = sSql & "'" & sMsg & "'"

    Call ExecutarSQL(sSiglaSistema, _
                     iAmbienteId, _
                     sSiglaRecurso, _
                     sDescricaoRecurso, _
                     sSql, _
                     False, False, 60000)

    Exit Sub

Trata_Erro:
    Call Err.Raise(Err.Number, , "SEGL0022.cls00393.EnviarEmail - " & Err.Description)

End Sub

Public Function ConsultarApolicesRenovacao(ByVal sSiglaSistema As String, _
                                           ByVal sSiglaRecurso As String, _
                                           ByVal sDescricaoRecurso As String, _
                                           ByVal iAmbienteId As Integer, _
                                           ByVal sDataInicio As String, _
                                           ByVal sDataFim As String, _
                                           ByVal sDataSistema As String) As Recordset

' Rotina para obter as ap�lices que devem ser renovadas

    Dim sSql As String

    On Error GoTo Trata_Erro

    sSql = ""
    sSql = sSql & "SELECT apolice_tb.apolice_id, " & vbNewLine
    sSql = sSql & "       apolice_tb.ramo_id, " & vbNewLine
    sSql = sSql & "       proposta_tb.subramo_id," & vbNewLine
    sSql = sSql & "       proposta_tb.produto_id, " & vbNewLine
    sSql = sSql & "       apolice_tb.dt_inicio_vigencia, " & vbNewLine
    sSql = sSql & "       CASE WHEN dt_fim_vigencia IS NULL " & vbNewLine
    'sSql = sSql & "            THEN DATEADD(DAY, 365, apolice_tb.dt_inicio_vigencia) " & vbNewLine
    sSql = sSql & "            THEN CONVERT(SMALLDATETIME, CONVERT(VARCHAR(4), '" & sDataSistema & "', 112) + " & vbNewLine
    sSql = sSql & "                 RIGHT(CONVERT(VARCHAR(8), apolice_tb.dt_inicio_vigencia, 112), 4))" & vbNewLine
    sSql = sSql & "            ELSE apolice_tb.dt_fim_vigencia " & vbNewLine
    sSql = sSql & "        END dt_fim_vigencia, " & vbNewLine
    sSql = sSql & "       cliente_tb.nome nome_cliente, " & vbNewLine
    sSql = sSql & "       ramo_tb.nome nome_ramo, " & vbNewLine
    sSql = sSql & "       produto_tb.nome nome_produto " & vbNewLine
    sSql = sSql & "  FROM apolice_tb  WITH(NOLOCK) " & vbNewLine
    sSql = sSql & " INNER JOIN proposta_tb  WITH(NOLOCK) " & vbNewLine
    sSql = sSql & "    ON proposta_tb.proposta_id = apolice_tb.proposta_id " & vbNewLine
    sSql = sSql & " INNER JOIN ramo_tb  WITH(NOLOCK) " & vbNewLine
    sSql = sSql & "    ON ramo_tb.ramo_id = apolice_tb.ramo_id " & vbNewLine
    sSql = sSql & " INNER JOIN cliente_tb  WITH(NOLOCK) " & vbNewLine
    sSql = sSql & "    ON cliente_tb.cliente_id = proposta_tb.prop_cliente_id " & vbNewLine
    sSql = sSql & " INNER JOIN produto_tb  WITH(NOLOCK) " & vbNewLine
    sSql = sSql & "    ON produto_tb.produto_id = proposta_tb.produto_id " & vbNewLine
    sSql = sSql & " WHERE CASE WHEN dt_fim_vigencia IS NULL " & vbNewLine
    'sSql = sSql & "            THEN DATEADD(DAY, 365, apolice_tb.dt_inicio_vigencia) " & vbNewLine
    sSql = sSql & "            THEN CONVERT(SMALLDATETIME, CONVERT(VARCHAR(4), '" & sDataSistema & "', 112) + " & vbNewLine
    sSql = sSql & "                 RIGHT(CONVERT(VARCHAR(8), apolice_tb.dt_inicio_vigencia, 112), 4))" & vbNewLine
    sSql = sSql & "            ELSE apolice_tb.dt_fim_vigencia " & vbNewLine
    sSql = sSql & "        END BETWEEN '" & Format(sDataInicio, "yyyymmdd") & "'" & vbNewLine
    sSql = sSql & "            AND '" & Format(sDataFim, "yyyymmdd") & "'" & vbNewLine
    sSql = sSql & "   AND ramo_tb.tp_ramo_id = 1 " & vbNewLine

    Set ConsultarApolicesRenovacao = ExecutarSQL(sSiglaSistema, _
                                                 iAmbienteId, _
                                                 sSiglaRecurso, _
                                                 sDescricaoRecurso, _
                                                 sSql, _
                                                 True, False, 60000)

    Exit Function

Trata_Erro:
    Call Err.Raise(Err.Number, , "SEGL0022.cls00393.ConsultarApolicesRenovacao - " & Err.Description)

End Function

Public Sub AbrirTransacaoAtual(ByVal sSiglaSistema As String, _
                               ByVal sSiglaRecurso As String, _
                               ByVal sDescricaoRecurso As String, _
                               ByVal iAmbienteId As Integer)

    On Error GoTo Trata_Erro

    ' Abrindo Conexao ''''''''''''''''''''''''''''''''''''''''''''

    lConexao = AbrirConexao(sSiglaSistema, _
                            iAmbienteId, _
                            sSiglaRecurso, _
                            sDescricaoRecurso)

    ' Abrindo Transacao '''''''''''''''''''''''''''''''''''''''''''

    If Not AbrirTransacao(lConexao) Then
        Call Err.Raise(vbObjectError + 1000, , "CLIL0002.cls00112.AbrirTransacaoAtual - N�o foi poss�vel abrir transa��o")
    End If

    Exit Sub

Trata_Erro:

    Call Err.Raise(Err.Number, , "SEGL002.cls00393.AbrirTransacaoAtual - " & Err.Description)

End Sub

Public Sub ConfirmarTransacaoAtual()

    On Error GoTo Trata_Erro

    ' Fechando Transacao '''''''''''''''''''''''''''''''''''''''''''''

    If Not ConfirmarTransacao(lConexao) Then
        Call Err.Raise(vbObjectError + 1000, , "CLIL0002.cls00112.ConfirmarTransacaoAtual - N�o foi poss�vel confirmar transa��o")
    End If

    ' Fechando Conexao '''''''''''''''''''''''''''''''''''''''''''''''

    If Not FecharConexao(lConexao) Then
        Call Err.Raise(vbObjectError + 1000, , "CLIL0002.cls00112.ConfirmarTransacaoAtual - N�o foi poss�vel fechar conex�o")
    End If

    Exit Sub

Trata_Erro:

    Call Err.Raise(Err.Number, , "SEGL0022.cls00393.ConfirmarTransacaoAtual - " & Err.Description)

End Sub
