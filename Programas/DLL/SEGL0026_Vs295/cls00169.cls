VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cls00169"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Const EVENTO_IMPORTACAO As Integer = 1

Public Function ExtrairDadosRenovacaoVida(ByVal sSiglaSistema As String, _
                                          ByVal sSiglaRecurso As String, _
                                          ByVal sDescricaoRecurso As String, _
                                          ByVal iAmbienteId As Integer, _
                                          ByVal sDtProcessamento As String, _
                                          ByVal sUsuario As String) As Long

    On Error GoTo Trata_Erro

    Dim sSql As String
    Dim rsExtracao As Recordset



    sSql = ""
    sSql = sSql & "EXEC renovacao_monitorada_vida_spi" & vbNewLine
    sSql = sSql & EVENTO_IMPORTACAO & vbNewLine
    sSql = sSql & ", '" & Format(sDtProcessamento, "yyyymmdd") & "'" & vbNewLine
    sSql = sSql & ", '" & sUsuario & "'" & vbNewLine


    Set rsExtracao = ExecutarSQL(sSiglaSistema, _
                                 iAmbienteId, _
                                 sSiglaRecurso, _
                                 sDescricaoRecurso, _
                                 sSql, _
                                 True)

    If Not rsExtracao.EOF Then
        ExtrairDadosRenovacaoVida = rsExtracao(0)
        rsExtracao.Close
    Else
        ExtrairDadosRenovacaoVida = 0
    End If

    Set rsExtracao = Nothing

    Exit Function

Trata_Erro:

    '-- +--------------------------------------------------+
    '-- CONFITEC Sistemas - Niuarque Rosa - 27/10/2011
    '-- INC000002826351 - A proposta AB 23876171 venceu em 10102011
    '-- Para que o erro seja externado tambem pelo retorno da fun��o
    '-- +--------------------------------------------------+
    ExtrairDadosRenovacaoVida = -1

    'Call Err.Raise(Err.Number, , "SEGL0026.cls00169.ExtrairDadosRenovacaoVida - " & Err.Description)

End Function
