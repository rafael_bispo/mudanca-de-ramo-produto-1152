VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cls00468"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private mvarpENDOSSO_ID As Long
Private mvarSiglaSistema As String
Private mvarAmbienteId As String
Private mvarSiglaRecurso As String
Private mvarDescricaoRecurso As String
Private mvarpdt_inicio_vigencia As String
Private mvarpdt_fim_vigencia As String
Private mvarpTp_Componente_id As String
Private mvarpPropostaId As Long
Private mvarpseq_canc_endosso_seg As Integer
Private mvarpDataSistema As String
Private mvarpUsuario As String
'carolina.marinho 27/05/2010 - Tratamento ABS
Private mvarpEmpresa As Integer

Public Property Let pUsuario(ByVal vData As String)
    mvarpUsuario = vData
End Property

Public Property Get pUsuario() As String
    pUsuario = mvarpUsuario
End Property

Public Property Let pDataSistema(ByVal vData As String)
    mvarpDataSistema = vData
End Property

Public Property Get pDataSistema() As String
    pDataSistema = mvarpDataSistema
End Property

Public Property Let pPropostaId(ByVal vData As Long)
    mvarpPropostaId = vData
End Property

Public Property Get pPropostaId() As Long
    pPropostaId = mvarpPropostaId
End Property

Public Property Let pTp_Componente_id(ByVal vData As String)
    mvarpTp_Componente_id = vData
End Property

Public Property Get pTp_Componente_id() As String
    pTp_Componente_id = mvarpTp_Componente_id
End Property

Public Property Let pdt_inicio_vigencia(ByVal vData As String)
    mvarpdt_inicio_vigencia = vData
End Property

Public Property Get pdt_inicio_vigencia() As String
    pdt_inicio_vigencia = mvarpdt_inicio_vigencia
End Property

Public Property Let pdt_fim_vigencia(ByVal vData As String)
    mvarpdt_fim_vigencia = vData
End Property

Public Property Get pdt_fim_vigencia() As String
    pdt_fim_vigencia = mvarpdt_fim_vigencia
End Property

Public Property Let pSiglaSistema(ByVal vData As String)
    mvarSiglaSistema = vData
End Property

Public Property Get pSiglaSistema() As String
    pSiglaSistema = mvarSiglaSistema
End Property

Public Property Let pAmbienteId(ByVal vData As String)
    mvarAmbienteId = vData
End Property

Public Property Get pAmbienteId() As String
    pAmbienteId = mvarAmbienteId
End Property

Public Property Let pSiglaRecurso(ByVal vData As String)
    mvarSiglaRecurso = vData
End Property

Public Property Get pSiglaRecurso() As String
    pSiglaRecurso = mvarSiglaRecurso
End Property

Public Property Let pDescricaoRecurso(ByVal vData As String)
    mvarDescricaoRecurso = vData
End Property

Public Property Get pDescricaoRecurso() As String
    pDescricaoRecurso = mvarDescricaoRecurso
End Property

Public Property Let pENDOSSO_ID(ByVal vData As Long)
    mvarpENDOSSO_ID = vData
End Property
Public Property Get pENDOSSO_ID() As Long
    pENDOSSO_ID = mvarpENDOSSO_ID
End Property

'carolina.marinho 27/05/2010 - Tratamento ABS
Public Property Let pEmpresa(ByVal vData As Long)
    mvarpEmpresa = vData
End Property

'carolina.marinho 27/05/2010 - Tratamento ABS
Public Property Get pEmpresa() As Long
    pEmpresa = mvarpEmpresa
End Property

Public Function Consultar(ByVal sSiglaSistema As String, _
                          ByVal sSiglaRecurso As String, _
                          ByVal sDescricaoRecurso As String, _
                          ByVal lAmbienteID As Long, _
                          ByVal lPropostaId As Long, _
                          ByVal lApoliceId As Long, _
                          ByVal LramoId As Long, _
                          ByVal lSeguradoraLider As Long, _
                          ByVal lSucursal As Long, _
                          Optional ByVal bUsarConexaoABS As Boolean, _
                          Optional ByVal glAmbiente_id As Integer, _
                          Optional ByVal glAmbiente_id_Seg2 As Integer) As Object
'carolina.marinho 26/05/2010 - Tratamento ABS

    Dim sSql As String

    On Error GoTo TrataErro

    ' Consultando ap�lice '''''''''''''''''''''''''''''''''''''''''''''''''''''''''

    sSql = ""
    sSql = sSql & "SELECT proposta_adesao_tb.apolice_id," & vbNewLine
    sSql = sSql & "       proposta_adesao_tb.sucursal_seguradora_id," & vbNewLine
    sSql = sSql & "       proposta_adesao_tb.seguradora_cod_susep," & vbNewLine
    sSql = sSql & "       proposta_adesao_tb.ramo_id," & vbNewLine
    sSql = sSql & "       dt_inicio_vigencia_sbg = proposta_adesao_tb.dt_inicio_vigencia," & vbNewLine
    sSql = sSql & "       dt_fim_vigencia_sbg = proposta_adesao_tb.dt_fim_vigencia," & vbNewLine
    sSql = sSql & "       proposta_adesao_tb.proposta_id," & vbNewLine
    sSql = sSql & "       proposta_tb.produto_id," & vbNewLine
    sSql = sSql & "       proposta_tb.prop_cliente_id," & vbNewLine
    sSql = sSql & "       sub_grupo_id = 0," & vbNewLine
    sSql = sSql & "       cliente_tb.nome," & vbNewLine
    sSql = sSql & "       proposta_tb.produto_id," & vbNewLine
    sSql = sSql & "       'Empresa' = " & glAmbiente_id & vbNewLine
    sSql = sSql & "  FROM proposta_adesao_tb  WITH (NOLOCK) " & vbNewLine
    sSql = sSql & "  JOIN proposta_tb  WITH (NOLOCK) " & vbNewLine
    sSql = sSql & "    ON proposta_tb.proposta_id = proposta_adesao_tb.proposta_id" & vbNewLine
    sSql = sSql & "  JOIN cliente_tb  WITH (NOLOCK) " & vbNewLine
    sSql = sSql & "    ON cliente_tb.cliente_id = proposta_tb.prop_cliente_id" & vbNewLine
    sSql = sSql & " WHERE "
    If lPropostaId > 0 Then
        sSql = sSql & "     proposta_adesao_tb.proposta_id = " & lPropostaId & vbNewLine
    Else
        sSql = sSql & "     proposta_adesao_tb.apolice_id = " & lApoliceId & vbNewLine
        sSql = sSql & " AND proposta_adesao_tb.ramo_id = " & LramoId & vbNewLine
        sSql = sSql & " AND proposta_adesao_tb.sucursal_seguradora_id = " & lSucursal & vbNewLine
        sSql = sSql & " AND proposta_adesao_tb.seguradora_cod_susep = " & lSeguradoraLider & vbNewLine
    End If
    sSql = sSql & "   AND proposta_tb.produto_id = 1144" & vbNewLine
    'carolina.marinho 07/06/2010 - Tratamento ABS
    sSql = sSql & "   AND proposta_tb.situacao <> 't'" & vbNewLine


    'carolina.marinho 26/05/2010 - Tratamento ABS
    If bUsarConexaoABS Then

        sSql = sSql & "UNION ALL " & vbNewLine
        sSql = sSql & "SELECT proposta_adesao_tb.apolice_id," & vbNewLine
        sSql = sSql & "       proposta_adesao_tb.sucursal_seguradora_id," & vbNewLine
        sSql = sSql & "       proposta_adesao_tb.seguradora_cod_susep," & vbNewLine
        sSql = sSql & "       proposta_adesao_tb.ramo_id," & vbNewLine
        sSql = sSql & "       dt_inicio_vigencia_sbg = proposta_adesao_tb.dt_inicio_vigencia," & vbNewLine
        sSql = sSql & "       dt_fim_vigencia_sbg = proposta_adesao_tb.dt_fim_vigencia," & vbNewLine
        sSql = sSql & "       proposta_adesao_tb.proposta_id," & vbNewLine
        sSql = sSql & "       proposta_tb.produto_id," & vbNewLine
        sSql = sSql & "       proposta_tb.prop_cliente_id," & vbNewLine
        sSql = sSql & "       sub_grupo_id = 0," & vbNewLine
        sSql = sSql & "       cliente_tb.nome," & vbNewLine
        sSql = sSql & "       proposta_tb.produto_id," & vbNewLine
        sSql = sSql & "       'Empresa' = " & glAmbiente_id_Seg2 & vbNewLine
        sSql = sSql & "  FROM ABSS.seguros_db.DBO.proposta_adesao_tb proposta_adesao_tb  WITH (NOLOCK) " & vbNewLine
        sSql = sSql & "  JOIN ABSS.seguros_db.DBO.proposta_tb proposta_tb  WITH (NOLOCK) " & vbNewLine
        sSql = sSql & "    ON proposta_tb.proposta_id = proposta_adesao_tb.proposta_id" & vbNewLine
        sSql = sSql & "  JOIN ABSS.seguros_db.DBO.cliente_tb cliente_tb  WITH (NOLOCK) " & vbNewLine
        sSql = sSql & "    ON cliente_tb.cliente_id = proposta_tb.prop_cliente_id" & vbNewLine
        sSql = sSql & " WHERE "
        If lPropostaId > 0 Then
            sSql = sSql & "     proposta_adesao_tb.proposta_id = " & lPropostaId & vbNewLine
        Else
            sSql = sSql & "     proposta_adesao_tb.apolice_id = " & lApoliceId & vbNewLine
            sSql = sSql & " AND proposta_adesao_tb.ramo_id = " & LramoId & vbNewLine
            sSql = sSql & " AND proposta_adesao_tb.sucursal_seguradora_id = " & lSucursal & vbNewLine
            sSql = sSql & " AND proposta_adesao_tb.seguradora_cod_susep = " & lSeguradoraLider & vbNewLine
        End If
        sSql = sSql & "   AND proposta_tb.produto_id = 1144" & vbNewLine
        sSql = sSql & "   AND proposta_tb.situacao <> 't'" & vbNewLine
    End If

    Set Consultar = ExecutarSQL(sSiglaSistema, _
                                lAmbienteID, _
                                sSiglaRecurso, _
                                sDescricaoRecurso, _
                                sSql, _
                                True)

    Exit Function

TrataErro:
    Call Err.Raise(Err.Number, , "SEGL0026.cls00468.Consultar - " & Err.Description)

End Function

Public Function ConsultarBeneficiarios(ByVal sSiglaSistema As String, _
                                       ByVal sSiglaRecurso As String, _
                                       ByVal sDescricaoRecurso As String, _
                                       ByVal lAmbienteID As Long, _
                                       ByVal lPropostaId As Long, _
                                       Optional ByVal bUsarConexaoABS As Boolean, _
                                       Optional ByVal glAmbiente_id As Integer, _
                                       Optional ByVal glAmbiente_id_Seg2 As Integer, _
                                       Optional ByVal iEmpresa As Integer) As Object
'carolina.marinho 27/05/2010 - Tratamento ABS

    Dim sSql As String

    On Error GoTo TrataErro

    ' Consultando beneficiarios de uma proposta  '''''''''''''''''''''''''''''''''''''''''''''''''''''''''

    sSql = ""
    sSql = sSql & "SELECT beneficiario_tb.proposta_id," & vbNewLine
    sSql = sSql & "       beneficiario_tb.seq_beneficiario," & vbNewLine
    sSql = sSql & "       beneficiario_tb.dt_inicio_vigencia," & vbNewLine
    sSql = sSql & "       beneficiario_tb.dt_fim_vigencia," & vbNewLine
    sSql = sSql & "       beneficiario_tb.endosso_id," & vbNewLine
    sSql = sSql & "       beneficiario_tb.tp_componente_id," & vbNewLine
    sSql = sSql & "       beneficiario_tb.nome," & vbNewLine
    sSql = sSql & "       beneficiario_tb.cpf_cnpj," & vbNewLine
    sSql = sSql & "       beneficiario_tb.perc_participacao," & vbNewLine
    sSql = sSql & "       beneficiario_tb.endereco," & vbNewLine
    sSql = sSql & "       beneficiario_tb.bairro," & vbNewLine
    sSql = sSql & "       beneficiario_tb.cidade," & vbNewLine
    sSql = sSql & "       beneficiario_tb.cep," & vbNewLine
    sSql = sSql & "       beneficiario_tb.pais," & vbNewLine
    sSql = sSql & "       beneficiario_tb.estado," & vbNewLine
    sSql = sSql & "       beneficiario_tb.doc_identidade," & vbNewLine
    sSql = sSql & "       beneficiario_tb.tp_doc_identidade," & vbNewLine
    sSql = sSql & "       beneficiario_tb.exp_doc_identidade," & vbNewLine
    sSql = sSql & "       beneficiario_tb.grau_parentesco," & vbNewLine
    sSql = sSql & "       beneficiario_tb.dt_emissao_identidade" & vbNewLine
    sSql = sSql & "  FROM beneficiario_tb  WITH (NOLOCK)  " & vbNewLine
    sSql = sSql & " WHERE beneficiario_tb.proposta_id = " & lPropostaId & vbNewLine
    sSql = sSql & "   AND beneficiario_tb.dt_fim_vigencia IS NULL"

    'carolina.marinho 27/05/2010 - Tratamento ABS
    If bUsarConexaoABS Then
        If pEmpresa = glAmbiente_id_Seg2 Then
            lAmbienteID = pEmpresa
        End If
    End If

    Set ConsultarBeneficiarios = ExecutarSQL(sSiglaSistema, _
                                             lAmbienteID, _
                                             sSiglaRecurso, _
                                             sDescricaoRecurso, _
                                             sSql, _
                                             True)

    'carolina.marinho 27/05/2010 - Tratamento ABS
    If lAmbienteID <> glAmbiente_id Then
        lAmbienteID = glAmbiente_id
    End If

    Exit Function

TrataErro:
    Call Err.Raise(Err.Number, , "SEGL0026.cls00468.Consultar - " & Err.Description)

End Function

Public Sub MovimentarVidasBancoPopular(ByVal sSiglaSistema As String, _
                                       ByVal sSiglaRecurso As String, _
                                       ByVal sDescricaoRecurso As String, _
                                       ByVal lAmbienteID As Long, _
                                       ByVal colBeneficiarios As Collection)

    Dim sSql As String
    Dim lConexao As Long
    Dim oBeneficiario As Object
    Dim rs As Object
    On Error GoTo TrataErro

    ' Abrindo Conexao ''''''''''''''''''''''''''''''''''''''''''''

    lConexao = AbrirConexao(sSiglaSistema, _
                            lAmbienteID, _
                            sSiglaRecurso, _
                            sDescricaoRecurso)

    ' Abrindo Transacao '''''''''''''''''''''''''''''''''''''''''''

    If Not AbrirTransacao(lConexao) Then
        Call Err.Raise(vbObjectError + 1000, , "SEGL0026.cls00468.MovimentarVidasBancoPopular - N�o foi poss�vel abrir transa��o")
    End If

    Set rs = GravarEndosso(sSiglaSistema, _
                           sSiglaRecurso, _
                           sDescricaoRecurso, _
                           lAmbienteID)

    If Not rs.EOF Then
        pENDOSSO_ID = rs(0)
    End If
    rs.Close

    For Each oBeneficiario In colBeneficiarios

        Select Case oBeneficiario.pTpOperacao

        Case "I"

            sSql = ""
            sSql = sSql & "EXEC SEGS4666_SPI" & vbNewLine
            sSql = sSql & " " & pENDOSSO_ID & "," & vbNewLine
            sSql = sSql & " " & pPropostaId & "," & vbNewLine
            sSql = sSql & " " & pTp_Componente_id & "," & vbNewLine
            sSql = sSql & "'" & Format(pDataSistema, "yyyyMMdd") & "'," & vbNewLine
            sSql = sSql & " " & MudaVirgulaParaPonto(oBeneficiario.pPorcentagem) & "," & vbNewLine
            sSql = sSql & "'" & MudaAspaSimples(oBeneficiario.pNome) & "'," & vbNewLine
            sSql = sSql & " " & IIf(oBeneficiario.pEndereco = "", "NULL", "'" & MudaAspaSimples(oBeneficiario.pEndereco) & "'") & "," & vbNewLine
            sSql = sSql & " " & IIf(oBeneficiario.pCidade = "", "NULL", "'" & MudaAspaSimples(oBeneficiario.pCidade) & "'") & "," & vbNewLine
            sSql = sSql & " " & IIf(oBeneficiario.pEstado = "", "NULL", "'" & MudaAspaSimples(oBeneficiario.pEstado) & "'") & "," & vbNewLine
            sSql = sSql & " " & IIf(oBeneficiario.pDoc_Identidade = "", "NULL", "'" & MudaAspaSimples(oBeneficiario.pDoc_Identidade) & "'") & "," & vbNewLine
            sSql = sSql & " " & IIf(oBeneficiario.pTp_doc_identidade = "", "NULL", "'" & MudaAspaSimples(oBeneficiario.pTp_doc_identidade) & "'") & "," & vbNewLine
            sSql = sSql & " " & IIf(oBeneficiario.pExp_doc_identidade = "", "NULL", "'" & MudaAspaSimples(oBeneficiario.pExp_doc_identidade) & "'") & "," & vbNewLine
            sSql = sSql & " " & IIf(oBeneficiario.pGrauParentesco = -1, "NULL", oBeneficiario.pGrauParentesco) & "," & vbNewLine
            sSql = sSql & " " & IIf(oBeneficiario.pDt_emissao_identidade = "__/__/____", "NULL", "'" & Format(oBeneficiario.pDt_emissao_identidade, "yyyymmdd") & "'") & "," & vbNewLine
            sSql = sSql & "'" & pUsuario & "'" & vbNewLine

            Call Conexao_ExecutarSQL(sSiglaSistema, _
                                     lAmbienteID, _
                                     sSiglaRecurso, _
                                     sDescricaoRecurso, _
                                     sSql, _
                                     lConexao, _
                                     False)

        Case "A"

            sSql = ""
            sSql = sSql & "EXEC SEGS4667_SPU" & vbNewLine
            sSql = sSql & " " & oBeneficiario.pSeqBeneficiario & "," & vbNewLine
            sSql = sSql & " " & pENDOSSO_ID & "," & vbNewLine
            sSql = sSql & " " & pPropostaId & "," & vbNewLine
            sSql = sSql & " " & pTp_Componente_id & "," & vbNewLine
            sSql = sSql & "'" & pdt_inicio_vigencia & "'," & vbNewLine
            sSql = sSql & " " & MudaVirgulaParaPonto(oBeneficiario.pPorcentagem) & "," & vbNewLine
            sSql = sSql & "'" & MudaAspaSimples(oBeneficiario.pNome) & "'," & vbNewLine
            sSql = sSql & " " & IIf(oBeneficiario.pEndereco = "", "NULL", "'" & MudaAspaSimples(oBeneficiario.pEndereco) & "'") & "," & vbNewLine
            sSql = sSql & " " & IIf(oBeneficiario.pCidade = "", "NULL", "'" & MudaAspaSimples(oBeneficiario.pCidade) & "'") & "," & vbNewLine
            sSql = sSql & " " & IIf(oBeneficiario.pEstado = "", "NULL", "'" & MudaAspaSimples(oBeneficiario.pEstado) & "'") & "," & vbNewLine
            sSql = sSql & " " & IIf(oBeneficiario.pDoc_Identidade = "", "NULL", "'" & MudaAspaSimples(oBeneficiario.pDoc_Identidade) & "'") & "," & vbNewLine
            sSql = sSql & " " & IIf(oBeneficiario.pTp_doc_identidade = "", "NULL", "'" & MudaAspaSimples(oBeneficiario.pTp_doc_identidade) & "'") & "," & vbNewLine
            sSql = sSql & " " & IIf(oBeneficiario.pExp_doc_identidade = "", "NULL", "'" & MudaAspaSimples(oBeneficiario.pExp_doc_identidade) & "'") & "," & vbNewLine
            sSql = sSql & " " & IIf(oBeneficiario.pGrauParentesco = -1, "NULL", oBeneficiario.pGrauParentesco) & "," & vbNewLine
            sSql = sSql & " " & IIf(oBeneficiario.pDt_emissao_identidade = "__/__/____", "NULL", "'" & Format(oBeneficiario.pDt_emissao_identidade, "yyyymmdd") & "'") & "," & vbNewLine
            sSql = sSql & "'" & pUsuario & "'" & vbNewLine

            Call Conexao_ExecutarSQL(pSiglaSistema, _
                                     pAmbienteId, _
                                     pSiglaRecurso, _
                                     pDescricaoRecurso, _
                                     sSql, _
                                     lConexao, _
                                     False)

        Case "E"

            sSql = ""
            sSql = sSql & "EXEC SEGS4668_SPD" & vbNewLine
            sSql = sSql & " " & oBeneficiario.pSeqBeneficiario & "," & vbNewLine
            sSql = sSql & " " & pENDOSSO_ID & "," & vbNewLine
            sSql = sSql & " " & pPropostaId & "," & vbNewLine
            sSql = sSql & "'" & Format(DateAdd("d", -1, pDataSistema), "yyyyMMdd") & "'," & vbNewLine
            sSql = sSql & "'" & pUsuario & "'" & vbNewLine


            Call Conexao_ExecutarSQL(pSiglaSistema, _
                                     pAmbienteId, _
                                     pSiglaRecurso, _
                                     pDescricaoRecurso, _
                                     sSql, _
                                     lConexao, _
                                     False)

        End Select

    Next

    Set oBeneficiario = Nothing

    ' Fechando Transacao '''''''''''''''''''''''''''''''''''''''''''''

    If Not ConfirmarTransacao(lConexao) Then
        Call Err.Raise(vbObjectError + 1000, , "SEGL0026.cls00468.Cancelar - N�o foi poss�vel confirmar transa��o")
    End If

    ' Fechando Conexao '''''''''''''''''''''''''''''''''''''''''''''''

    If Not FecharConexao(lConexao) Then
        Call Err.Raise(vbObjectError + 1000, , "SEGL0026.cls00468.Cancelar - N�o foi poss�vel fechar conex�o")
    End If

    Exit Sub

TrataErro:

    Call Err.Raise(Err.Number, , "SEGL0026.cls00468.MovimentarVidasBancoPopular - " & Err.Description)

End Sub

Public Function GravarEndosso(ByVal sSiglaSistema As String, _
                              ByVal sSiglaRecurso As String, _
                              ByVal sDescricaoRecurso As String, _
                              ByVal lAmbienteID As Long) As Object


    Const ENDOSSO_BENEFICIARIO = 99

    Dim sSql As String

    On Error GoTo TrataErro

    ' ALTERANDO beneficiarios de uma proposta  '''''''''''''''''''''''''''''''''''''''''''''''''''''''''

    sSql = "exec endosso_spi "
    sSql = sSql & " " & pPropostaId & "," & vbNewLine     '''@proposta_id           numeric(9,0),
    sSql = sSql & " null," & vbNewLine                    '''@val_financeiro        numeric(15,2)  = null,
    sSql = sSql & " null," & vbNewLine                    '''@dt_pgto               smalldatetime  = null,
    sSql = sSql & " null," & vbNewLine                    '''@num_endosso_bb        numeric(9,0)   = null,
    sSql = sSql & ENDOSSO_BENEFICIARIO & " ," & vbNewLine    '''@tp_endosso_bb         int,
    sSql = sSql & SeEspacoNull(Format(pDataSistema, "yyyyMMdd")) & " ," & vbNewLine    '''@dt_pedido_endosso     smalldatetime,
    sSql = sSql & SeEspacoNull(Format(pDataSistema, "yyyyMMdd")) & " ," & vbNewLine   '''@dt_emissao            smalldatetime,
    sSql = sSql & " null," & vbNewLine                    '''@val_comissao          numeric(15,2)  = null,
    sSql = sSql & " null," & vbNewLine                    '''@val_iof               numeric(15,2)  = null,
    sSql = sSql & " null," & vbNewLine                    '''@val_ir                numeric(15,2)   = null,
    sSql = sSql & " null," & vbNewLine                    '''@val_desconto          numeric(15,2)   = null,
    sSql = sSql & " null," & vbNewLine                    '''@val_adic_frac         numeric(15,2)   = null,
    sSql = sSql & " null," & vbNewLine                    '''@val_premio_tarifa     numeric(15,2)  = null,
    sSql = sSql & " null," & vbNewLine                    '''@num_parcelas          int        = null,
    sSql = sSql & " null," & vbNewLine                    '''@custo_apolice         numeric(15,2)   = null,
    sSql = sSql & " " & SeEspacoNull(pUsuario) & ", " & vbNewLine                      '''@usuario               varchar(20),
    sSql = sSql & " null," & vbNewLine                    '''@val_comissao_estipulante numeric(15,2)  = null,
    sSql = sSql & " null," & vbNewLine                    '''@situacao_re_seguro      varchar(1)    = 'P',
    sSql = sSql & " null," & vbNewLine                    '''@desc_endosso            varchar(255)  = null,
    sSql = sSql & " null," & vbNewLine                    '''@num_vidas               int     =  null,
    sSql = sSql & " null," & vbNewLine                    '''@val_is                  numeric(16,2)  = null,
    sSql = sSql & " null," & vbNewLine                    '''@seguro_moeda_id         numeric(3,0)  = null,
    sSql = sSql & " null," & vbNewLine                    '''@premio_moeda_id         numeric(3,0)  = null,
    sSql = sSql & " null," & vbNewLine                    '''@val_paridade            numeric(9,6)  = null,
    sSql = sSql & " 'm'," & vbNewLine                     '''@origem_endosso          char(1)  = null,
    sSql = sSql & " null," & vbNewLine                    '''@dt_recebimento_processo smalldatetime  = null,
    sSql = sSql & " null," & vbNewLine                    '''@endosso_gerado          int            = null OUTPUT   ,
    sSql = sSql & " null " & vbNewLine                    '''@retornadoBB             char(1)        = null -- Rafael Oshiro 19/11/04

    Set GravarEndosso = ExecutarSQL(sSiglaSistema, _
                                    lAmbienteID, _
                                    sSiglaRecurso, _
                                    sDescricaoRecurso, _
                                    sSql, _
                                    True)

    Exit Function

TrataErro:
    Call Err.Raise(Err.Number, , "SEGL0026.cls00468.GravarEndosso - " & Err.Description)

End Function

Private Function SeEspacoNull(ByRef Variavel As String) As String
    If Len(Variavel) = 0 Then
        SeEspacoNull = "Null "
    Else
        SeEspacoNull = Chr(34) & Trim(Variavel) & Chr(34)
    End If
End Function

