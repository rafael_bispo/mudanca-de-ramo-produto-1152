VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cls00166"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit

Public Function ConsultarProdutoCampanha(ByVal sSiglaSistema As String, _
                                         ByVal sSiglaRecurso As String, _
                                         ByVal sDescricaoRecurso As String, _
                                         ByVal iAmbienteId As Integer) As Object

    Dim sSql As String

    On Error GoTo Erro

    sSql = ""
    sSql = sSql & "SELECT produto_id, nome"
    sSql = sSql & "  FROM produto_tb  WITH (NOLOCK) "
    sSql = sSql & " WHERE produto_id IN (116, 119)"

    Set ConsultarProdutoCampanha = ExecutarSQL(sSiglaSistema, _
                                               iAmbienteId, _
                                               sSiglaRecurso, _
                                               sDescricaoRecurso, _
                                               sSql, _
                                               True)

    Exit Function

Erro:

    Call Err.Raise(Err.Number, , "SEGL0026.cls00166.ConsultarProdutoCampanha - " & Err.Description)

End Function

Public Function ConsultarDadosSinteticosGrupoEspecial(ByVal sSiglaSistema As String, _
                                                      ByVal sSiglaRecurso As String, _
                                                      ByVal sDescricaoRecurso As String, _
                                                      ByVal lAmbienteID As Long, _
                                                      ByVal sDtInicio As String, _
                                                      ByVal sDtFim As String, _
                                                      ByVal bCorretagem As Boolean) As Object

    Dim sSql As String

    On Error GoTo Trata_Erro

    If bCorretagem Then
        sSql = "SET NOCOUNT ON " & vbNewLine
        sSql = sSql & "EXEC consulta_sintetica_grupo_especial_sps "
        sSql = sSql & "'" & Format(sDtInicio, "yyyymmdd") & "'"
        sSql = sSql & ",'" & Format(sDtFim, "yyyymmdd") & "'"
    Else
        sSql = "SET NOCOUNT ON " & vbNewLine
        sSql = sSql & "EXEC consulta_sintetica_grupo_especial_sem_corretagem_sps "
        sSql = sSql & "'" & Format(sDtInicio, "yyyymmdd") & "'"
        sSql = sSql & ",'" & Format(sDtFim, "yyyymmdd") & "'"
    End If

    Set ConsultarDadosSinteticosGrupoEspecial = ExecutarSQL(sSiglaSistema, _
                                                            lAmbienteID, _
                                                            sSiglaRecurso, _
                                                            sDescricaoRecurso, _
                                                            sSql, _
                                                            True)

    Exit Function

Trata_Erro:

    Call Err.Raise(Err.Number, , "SEGL0026.cls00166.ConsultarDadosSinteticosGrupoEspecial - " & Err.Description)

End Function

Public Function ConsultarEmissaoFuturaSintetico(ByVal sSiglaSistema As String, _
                                                ByVal sSiglaRecurso As String, _
                                                ByVal sDescricaoRecurso As String, _
                                                ByVal iAmbienteId As Integer, _
                                                ByVal sDataSistema As String) As Object

    Dim sSql As String

    On Error GoTo Erro

    '    sSql = ""
    '    sSql = sSql & "SELECT proposta_tb.produto_id,                                                           " & vbNewLine
    '    sSql = sSql & "       produto_tb.nome,                                                                  " & vbNewLine
    '    sSql = sSql & "       DATEDIFF(DAY, '" & sDataSistema & "', emi_re_proposta_tb.dt_inicio_vigencia) dias," & vbNewLine
    '    sSql = sSql & "       COUNT(1) quantidade                                                               " & vbNewLine
    '    sSql = sSql & "  FROM proposta_tb  WITH (NOLOCK)                                                               " & vbNewLine
    '    sSql = sSql & " INNER JOIN produto_tb  WITH (NOLOCK)                                                           " & vbNewLine
    '    sSql = sSql & "    ON produto_tb.produto_id = proposta_tb.produto_id                                    " & vbNewLine
    '    sSql = sSql & " INNER JOIN emi_re_proposta_tb  WITH (NOLOCK)                                                   " & vbNewLine
    '    sSql = sSql & "    ON emi_re_proposta_tb.proposta_id = proposta_tb.proposta_id                          " & vbNewLine
    '    sSql = sSql & " WHERE proposta_tb.situacao = 'a'                                                        " & vbNewLine
    '    sSql = sSql & "   AND emi_re_proposta_tb.dt_inicio_vigencia > '" & sDataSistema & "'                    " & vbNewLine
    '    sSql = sSql & " GROUP BY proposta_tb.produto_id,                                                        " & vbNewLine
    '    sSql = sSql & "          produto_tb.nome,                                                               " & vbNewLine
    '    sSql = sSql & "          DATEDIFF(DAY, '" & sDataSistema & "', emi_re_proposta_tb.dt_inicio_vigencia)   " & vbNewLine
    '    sSql = sSql & " ORDER BY proposta_tb.produto_id                                                         " & vbNewLine

    ''emi_re_proposta_tb
    sSql = ""
    'sSql = sSql & "SELECT arquivo_produto_tb.produto_id," & vbNewLine
    sSql = sSql & "SELECT produto_tb.produto_id," & vbNewLine
    sSql = sSql & "       produto_tb.nome," & vbNewLine
    sSql = sSql & "       DATEDIFF(DAY, '" & sDataSistema & "', emi_re_proposta_tb.dt_inicio_vigencia) dias," & vbNewLine
    sSql = sSql & "       COUNT(1) quantidade" & vbNewLine
    sSql = sSql & "  FROM emi_re_proposta_tb WITH (NOLOCK) " & vbNewLine
    'sSql = sSql & " INNER JOIN arquivo_produto_tb  WITH (NOLOCK) " & vbNewLine
    'sSql = sSql & "    ON SUBSTRING(emi_re_proposta_tb.arquivo, 1, CHARINDEX('.', emi_re_proposta_tb.arquivo) - 1) = arquivo_produto_tb.arquivo" & vbNewLine
    'sSql = sSql & "   AND emi_re_proposta_tb.produto_externo = arquivo_produto_tb.produto_externo_id" & vbNewLine
    'Alessandra - Incluindo proposta_adesao_tb
    sSql = sSql & " INNER JOIN proposta_adesao_tb  WITH (NOLOCK) " & vbNewLine
    sSql = sSql & "    ON proposta_adesao_tb.proposta_bb = emi_re_proposta_tb.proposta_bb" & vbNewLine
    sSql = sSql & " INNER JOIN proposta_tb  WITH (NOLOCK) " & vbNewLine
    sSql = sSql & "    ON proposta_tb.proposta_id = proposta_adesao_tb.proposta_id" & vbNewLine
    'FIM
    sSql = sSql & " INNER JOIN produto_tb  WITH (NOLOCK) " & vbNewLine
    'sSql = sSql & "    ON produto_tb.produto_id = arquivo_produto_tb.produto_id" & vbNewLine
    sSql = sSql & "    ON produto_tb.produto_id = proposta_tb.produto_id" & vbNewLine
    sSql = sSql & " WHERE /*emi_re_proposta_tb.situacao = 'n'" & vbNewLine
    sSql = sSql & "   AND */proposta_tb.situacao in ('a', 'o') " & vbNewLine
    '-------------------------------------------------------------------------------------------------
    'Luciana - 09/03/2005
    '-------------------------------------------------------------------------------------------------
    'sSql = sSql & "   AND emi_re_proposta_tb.dt_inicio_vigencia > '" & sDataSistema & "'" & vbNewLine
    sSql = sSql & "   AND emi_re_proposta_tb.dt_inicio_vigencia >= '" & sDataSistema & "'" & vbNewLine
    '-------------------------------------------------------------------------------------------------
    'sSql = sSql & " GROUP BY arquivo_produto_tb.produto_id," & vbNewLine
    sSql = sSql & " GROUP BY produto_tb.produto_id," & vbNewLine
    sSql = sSql & "          produto_tb.nome," & vbNewLine
    sSql = sSql & "          DATEDIFF(DAY, '" & sDataSistema & "', emi_re_proposta_tb.dt_inicio_vigencia)" & vbNewLine
    sSql = sSql & " UNION " & vbNewLine

    'sSql = sSql & "SELECT arquivo_produto_tb.produto_id," & vbNewLine
    sSql = sSql & "SELECT produto_tb.produto_id," & vbNewLine
    sSql = sSql & "       produto_tb.nome," & vbNewLine
    sSql = sSql & "       DATEDIFF(DAY, '" & sDataSistema & "', emi_re_proposta_tb.dt_inicio_vigencia) dias," & vbNewLine
    sSql = sSql & "       COUNT(1) quantidade" & vbNewLine
    sSql = sSql & "  FROM emi_re_proposta_tb WITH (NOLOCK) " & vbNewLine
    'sSql = sSql & " INNER JOIN arquivo_produto_tb  WITH (NOLOCK) " & vbNewLine
    'sSql = sSql & "    ON SUBSTRING(emi_re_proposta_tb.arquivo, 1, CHARINDEX('.', emi_re_proposta_tb.arquivo) - 1) = arquivo_produto_tb.arquivo" & vbNewLine
    'sSql = sSql & "   AND emi_re_proposta_tb.produto_externo = arquivo_produto_tb.produto_externo_id" & vbNewLine
    'Alessandra - Incluindo proposta_fechada_tb
    sSql = sSql & " INNER JOIN proposta_fechada_tb  WITH (NOLOCK) " & vbNewLine
    sSql = sSql & "    ON proposta_fechada_tb.proposta_bb = emi_re_proposta_tb.proposta_bb" & vbNewLine
    sSql = sSql & " INNER JOIN proposta_tb  WITH (NOLOCK) " & vbNewLine
    sSql = sSql & "    ON proposta_tb.proposta_id = proposta_fechada_tb.proposta_id" & vbNewLine
    'FIM
    sSql = sSql & " INNER JOIN produto_tb  WITH (NOLOCK) " & vbNewLine
    sSql = sSql & "    ON produto_tb.produto_id = proposta_tb.produto_id" & vbNewLine
    sSql = sSql & " WHERE /*emi_re_proposta_tb.situacao = 'n'" & vbNewLine
    sSql = sSql & "   AND */proposta_tb.situacao in ('a', 'o')  " & vbNewLine
    '-------------------------------------------------------------------------------------------------
    'Luciana - 09/03/2005
    '-------------------------------------------------------------------------------------------------
    'sSql = sSql & "   AND emi_re_proposta_tb.dt_inicio_vigencia > '" & sDataSistema & "'" & vbNewLine
    sSql = sSql & "   AND emi_re_proposta_tb.dt_inicio_vigencia >= '" & sDataSistema & "'" & vbNewLine
    '-------------------------------------------------------------------------------------------------
    'sSql = sSql & " GROUP BY arquivo_produto_tb.produto_id," & vbNewLine
    sSql = sSql & " GROUP BY produto_tb.produto_id," & vbNewLine
    sSql = sSql & "          produto_tb.nome," & vbNewLine
    sSql = sSql & "          DATEDIFF(DAY, '" & sDataSistema & "', emi_re_proposta_tb.dt_inicio_vigencia)" & vbNewLine
    sSql = sSql & " UNION " & vbNewLine

    ''''' emi_proposta_tb
    'sSql = sSql & "SELECT arquivo_produto_tb.produto_id," & vbNewLine
    sSql = sSql & "SELECT produto_tb.produto_id," & vbNewLine
    sSql = sSql & "       produto_tb.nome," & vbNewLine
    sSql = sSql & "       DATEDIFF(DAY, '" & sDataSistema & "', emi_proposta_tb.dt_inicio_vigencia) dias," & vbNewLine
    sSql = sSql & "       COUNT(1) quantidade" & vbNewLine
    sSql = sSql & "  FROM emi_proposta_tb WITH (NOLOCK) " & vbNewLine
    'sSql = sSql & " INNER JOIN arquivo_produto_tb  WITH (NOLOCK) " & vbNewLine
    'sSql = sSql & "    ON SUBSTRING(emi_proposta_tb.arquivo, 1, CHARINDEX('.', emi_proposta_tb.arquivo) - 1) = arquivo_produto_tb.arquivo" & vbNewLine
    'sSql = sSql & "   AND emi_proposta_tb.produto_externo = arquivo_produto_tb.produto_externo_id" & vbNewLine
    'Alessandra - Incluindo proposta_adesao_tb
    sSql = sSql & " INNER JOIN proposta_adesao_tb  WITH (NOLOCK) " & vbNewLine
    sSql = sSql & "    ON proposta_adesao_tb.proposta_bb = emi_proposta_tb.proposta_bb" & vbNewLine
    sSql = sSql & " INNER JOIN proposta_tb  WITH (NOLOCK) " & vbNewLine
    sSql = sSql & "    ON proposta_tb.proposta_id = proposta_adesao_tb.proposta_id" & vbNewLine
    'FIM
    sSql = sSql & " INNER JOIN produto_tb  WITH (NOLOCK) " & vbNewLine
    sSql = sSql & "    ON produto_tb.produto_id = proposta_tb.produto_id" & vbNewLine
    sSql = sSql & " WHERE /*emi_proposta_tb.situacao = 'n'" & vbNewLine
    sSql = sSql & "   AND */proposta_tb.situacao in ('a', 'o')  " & vbNewLine
    '-------------------------------------------------------------------------------------------------
    'Luciana - 09/03/2005
    '-------------------------------------------------------------------------------------------------
    'sSql = sSql & "   AND emi_proposta_tb.dt_inicio_vigencia > '" & sDataSistema & "'" & vbNewLine
    sSql = sSql & "   AND emi_proposta_tb.dt_inicio_vigencia >= '" & sDataSistema & "'" & vbNewLine
    '-------------------------------------------------------------------------------------------------
    'sSql = sSql & " GROUP BY arquivo_produto_tb.produto_id," & vbNewLine
    sSql = sSql & " GROUP BY produto_tb.produto_id," & vbNewLine
    sSql = sSql & "          produto_tb.nome," & vbNewLine
    sSql = sSql & "          DATEDIFF(DAY, '" & sDataSistema & "', emi_proposta_tb.dt_inicio_vigencia)" & vbNewLine
    sSql = sSql & " UNION   " & vbNewLine

    'sSql = sSql & "SELECT arquivo_produto_tb.produto_id," & vbNewLine
    sSql = sSql & "SELECT produto_tb.produto_id," & vbNewLine
    sSql = sSql & "       produto_tb.nome," & vbNewLine
    sSql = sSql & "       DATEDIFF(DAY, '" & sDataSistema & "', emi_proposta_tb.dt_inicio_vigencia) dias," & vbNewLine
    sSql = sSql & "       COUNT(1) quantidade" & vbNewLine
    sSql = sSql & "  FROM emi_proposta_tb WITH (NOLOCK) " & vbNewLine
    'sSql = sSql & " INNER JOIN arquivo_produto_tb  WITH (NOLOCK) " & vbNewLine
    'sSql = sSql & "    ON SUBSTRING(emi_proposta_tb.arquivo, 1, CHARINDEX('.', emi_proposta_tb.arquivo) - 1) = arquivo_produto_tb.arquivo" & vbNewLine
    'sSql = sSql & "   AND emi_proposta_tb.produto_externo = arquivo_produto_tb.produto_externo_id" & vbNewLine
    'Alessandra - Incluindo proposta_fechada_tb
    sSql = sSql & " INNER JOIN proposta_fechada_tb  WITH (NOLOCK) " & vbNewLine
    sSql = sSql & "    ON proposta_fechada_tb.proposta_bb = emi_proposta_tb.proposta_bb" & vbNewLine
    sSql = sSql & " INNER JOIN proposta_tb  WITH (NOLOCK) " & vbNewLine
    sSql = sSql & "    ON proposta_tb.proposta_id = proposta_fechada_tb.proposta_id" & vbNewLine
    'FIM
    sSql = sSql & " INNER JOIN produto_tb  WITH (NOLOCK) " & vbNewLine
    sSql = sSql & "    ON produto_tb.produto_id = proposta_tb.produto_id" & vbNewLine
    sSql = sSql & " WHERE /*emi_proposta_tb.situacao = 'n'" & vbNewLine
    sSql = sSql & "   AND */proposta_tb.situacao in ('a', 'o')  " & vbNewLine
    '-------------------------------------------------------------------------------------------------
    'Luciana - 09/03/2005
    '-------------------------------------------------------------------------------------------------
    'sSql = sSql & "   AND emi_proposta_tb.dt_inicio_vigencia > '" & sDataSistema & "'" & vbNewLine
    sSql = sSql & "   AND emi_proposta_tb.dt_inicio_vigencia >= '" & sDataSistema & "'" & vbNewLine
    '-------------------------------------------------------------------------------------------------
    'sSql = sSql & " GROUP BY arquivo_produto_tb.produto_id," & vbNewLine
    sSql = sSql & " GROUP BY produto_tb.produto_id," & vbNewLine
    sSql = sSql & "          produto_tb.nome," & vbNewLine
    sSql = sSql & "          DATEDIFF(DAY, '" & sDataSistema & "', emi_proposta_tb.dt_inicio_vigencia)" & vbNewLine

    'sSql = sSql & " ORDER BY arquivo_produto_tb.produto_id" & vbNewLine
    sSql = sSql & " ORDER BY produto_tb.produto_id" & vbNewLine

    Set ConsultarEmissaoFuturaSintetico = ExecutarSQL(sSiglaSistema, _
                                                      iAmbienteId, _
                                                      sSiglaRecurso, _
                                                      sDescricaoRecurso, _
                                                      sSql, _
                                                      True)

    Exit Function

Erro:

    Call Err.Raise(Err.Number, , "SEGL0026.cls00166.ConsultarEmissaoFuturaSintetico - " & Err.Description)

End Function

Public Function ConsultarEmissaoFuturaAnalitico(ByVal sSiglaSistema As String, _
                                                ByVal sSiglaRecurso As String, _
                                                ByVal sDescricaoRecurso As String, _
                                                ByVal iAmbienteId As Integer, _
                                                ByVal sDataSistema As String, _
                                                ByVal iProdutoId As Integer) As Object

    Dim sSql As String

    On Error GoTo Erro

    '    sSql = ""
    '    sSql = sSql & "SELECT proposta_tb.proposta_id,                                                " & vbNewLine
    '    sSql = sSql & "       proposta_tb.dt_contratacao,                                             " & vbNewLine
    '    sSql = sSql & "       ISNULL(emi_re_proposta_tb.dt_inicio_vigencia, '') dt_inicio_vigencia,   " & vbNewLine
    '    sSql = sSql & "       proposta_bb = CASE WHEN proposta_fechada_tb.proposta_id IS NULL         " & vbNewLine
    '    sSql = sSql & "                          THEN proposta_adesao_tb.proposta_bb                  " & vbNewLine
    '    sSql = sSql & "                          ELSE proposta_fechada_tb.proposta_bb                 " & vbNewLine
    '    sSql = sSql & "                     END,                                                      " & vbNewLine
    '    sSql = sSql & "       val_is = CASE WHEN proposta_fechada_tb.proposta_id IS NULL              " & vbNewLine
    '    sSql = sSql & "                     THEN ISNULL(proposta_adesao_tb.val_is, 0)                 " & vbNewLine
    '    sSql = sSql & "                     ELSE ISNULL(proposta_fechada_tb.val_is, 0)                " & vbNewLine
    '    sSql = sSql & "                END,                                                           " & vbNewLine
    '    sSql = sSql & "       premio_bruto = CASE WHEN proposta_fechada_tb.proposta_id IS NULL        " & vbNewLine
    '    sSql = sSql & "                           THEN ISNULL(proposta_adesao_tb.val_premio_bruto, 0) " & vbNewLine
    '    sSql = sSql & "                           ELSE ISNULL(proposta_fechada_tb.val_premio_bruto, 0)" & vbNewLine
    '    sSql = sSql & "                      END                                                      " & vbNewLine
    '    sSql = sSql & "  FROM proposta_tb  WITH (NOLOCK)                                                     " & vbNewLine
    '    sSql = sSql & " INNER JOIN emi_re_proposta_tb  WITH (NOLOCK)                                         " & vbNewLine
    '    sSql = sSql & "    ON emi_re_proposta_tb.proposta_id = proposta_tb.proposta_id                " & vbNewLine
    '    sSql = sSql & "  LEFT JOIN proposta_fechada_tb  WITH (NOLOCK)                                        " & vbNewLine
    '    sSql = sSql & "    ON proposta_fechada_tb.proposta_id = proposta_tb.proposta_id               " & vbNewLine
    '    sSql = sSql & "  LEFT JOIN proposta_adesao_tb  WITH (NOLOCK)                                         " & vbNewLine
    '    sSql = sSql & "    ON proposta_adesao_tb.proposta_id = proposta_tb.proposta_id                " & vbNewLine
    '    sSql = sSql & " WHERE proposta_tb.situacao = 'a'                                              " & vbNewLine
    '    sSql = sSql & "   AND emi_re_proposta_tb.dt_inicio_vigencia > '" & sDataSistema & "'          " & vbNewLine
    '    sSql = sSql & "   AND proposta_tb.produto_id = " & iProdutoId & "                             " & vbNewLine
    '    sSql = sSql & " ORDER BY proposta_tb.proposta_id                                              " & vbNewLine

    sSql = ""
    sSql = sSql & "SELECT emi_re_proposta_tb.dt_contratacao," & vbNewLine
    sSql = sSql & "       ISNULL(emi_re_proposta_tb.dt_inicio_vigencia, '') dt_inicio_vigencia," & vbNewLine
    sSql = sSql & "       ISNULL(emi_re_proposta_tb.dt_fim_vigencia, '') dt_fim_vigencia," & vbNewLine
    sSql = sSql & "       emi_re_proposta_tb.proposta_bb," & vbNewLine
    sSql = sSql & "       ISNULL(emi_re_proposta_tb.proponente, '') proponente" & vbNewLine
    sSql = sSql & "  FROM emi_re_proposta_tb WITH (NOLOCK) " & vbNewLine
    'sSql = sSql & " INNER JOIN arquivo_produto_tb  WITH (NOLOCK) " & vbNewLine
    'sSql = sSql & "    ON SUBSTRING(emi_re_proposta_tb.arquivo, 1, CHARINDEX('.', emi_re_proposta_tb.arquivo) - 1) = arquivo_produto_tb.arquivo" & vbNewLine
    'sSql = sSql & "   AND emi_re_proposta_tb.produto_externo = arquivo_produto_tb.produto_externo_id" & vbNewLine
    'Alessandra - Incluindo proposta_adesao_tb
    sSql = sSql & " INNER JOIN proposta_adesao_tb  WITH (NOLOCK) "
    sSql = sSql & "    ON proposta_adesao_tb.proposta_bb = emi_re_proposta_tb.proposta_bb "
    sSql = sSql & " INNER JOIN proposta_tb  WITH (NOLOCK) "
    sSql = sSql & "    ON proposta_tb.proposta_id = proposta_adesao_tb.proposta_id "
    'FIM
    sSql = sSql & " INNER JOIN produto_tb  WITH (NOLOCK) " & vbNewLine
    'sSql = sSql & "    ON produto_tb.produto_id = arquivo_produto_tb.produto_id" & vbNewLine
    sSql = sSql & "    ON produto_tb.produto_id = proposta_tb.produto_id" & vbNewLine
    sSql = sSql & " WHERE /*emi_re_proposta_tb.situacao = 'n'" & vbNewLine
    sSql = sSql & "   AND */proposta_tb.situacao in ('a', 'o') "
    sSql = sSql & "   AND emi_re_proposta_tb.dt_inicio_vigencia > '" & sDataSistema & "'" & vbNewLine
    sSql = sSql & "   AND produto_tb.produto_id = " & iProdutoId & vbNewLine
    sSql = sSql & " UNION "
    sSql = sSql & "SELECT emi_re_proposta_tb.dt_contratacao," & vbNewLine
    sSql = sSql & "       ISNULL(emi_re_proposta_tb.dt_inicio_vigencia, '') dt_inicio_vigencia," & vbNewLine
    sSql = sSql & "       ISNULL(emi_re_proposta_tb.dt_fim_vigencia, '') dt_fim_vigencia," & vbNewLine
    sSql = sSql & "       emi_re_proposta_tb.proposta_bb," & vbNewLine
    sSql = sSql & "       ISNULL(emi_re_proposta_tb.proponente, '') proponente" & vbNewLine
    sSql = sSql & "  FROM emi_re_proposta_tb WITH (NOLOCK) " & vbNewLine
    'sSql = sSql & " INNER JOIN arquivo_produto_tb  WITH (NOLOCK) " & vbNewLine
    'sSql = sSql & "    ON SUBSTRING(emi_re_proposta_tb.arquivo, 1, CHARINDEX('.', emi_re_proposta_tb.arquivo) - 1) = arquivo_produto_tb.arquivo" & vbNewLine
    'sSql = sSql & "   AND emi_re_proposta_tb.produto_externo = arquivo_produto_tb.produto_externo_id" & vbNewLine
    'Alessandra - Incluindo proposta_fechada_tb
    sSql = sSql & " INNER JOIN proposta_fechada_tb  WITH (NOLOCK) "
    sSql = sSql & "    ON proposta_fechada_tb.proposta_bb = emi_re_proposta_tb.proposta_bb "
    sSql = sSql & " INNER JOIN proposta_tb  WITH (NOLOCK) "
    sSql = sSql & "    ON proposta_tb.proposta_id = proposta_fechada_tb.proposta_id "
    'FIM
    sSql = sSql & " INNER JOIN produto_tb  WITH (NOLOCK) " & vbNewLine
    'sSql = sSql & "    ON produto_tb.produto_id = arquivo_produto_tb.produto_id" & vbNewLine
    sSql = sSql & "    ON produto_tb.produto_id = proposta_tb.produto_id" & vbNewLine
    sSql = sSql & " WHERE /*emi_re_proposta_tb.situacao = 'n'" & vbNewLine
    sSql = sSql & "   AND */proposta_tb.situacao in ('a', 'o') "
    sSql = sSql & "   AND emi_re_proposta_tb.dt_inicio_vigencia > '" & sDataSistema & "'" & vbNewLine
    sSql = sSql & "   AND produto_tb.produto_id = " & iProdutoId & vbNewLine

    sSql = sSql & " UNION" & vbNewLine
    sSql = sSql & "SELECT emi_proposta_tb.dt_contratacao," & vbNewLine
    sSql = sSql & "       ISNULL(emi_proposta_tb.dt_inicio_vigencia, '') dt_inicio_vigencia," & vbNewLine
    sSql = sSql & "       ISNULL(emi_proposta_tb.dt_fim_vigencia, '') dt_fim_vigencia," & vbNewLine
    sSql = sSql & "       emi_proposta_tb.proposta_bb," & vbNewLine
    sSql = sSql & "       ISNULL(emi_proposta_tb.proponente, '') proponente" & vbNewLine
    sSql = sSql & "  FROM emi_proposta_tb WITH (NOLOCK) " & vbNewLine
    'sSql = sSql & " INNER JOIN arquivo_produto_tb  WITH (NOLOCK) " & vbNewLine
    'sSql = sSql & "    ON SUBSTRING(emi_proposta_tb.arquivo, 1, CHARINDEX('.', emi_proposta_tb.arquivo) - 1) = arquivo_produto_tb.arquivo" & vbNewLine
    'sSql = sSql & "   AND emi_proposta_tb.produto_externo = arquivo_produto_tb.produto_externo_id" & vbNewLine
    'Alessandra - Incluindo proposta_adesao_tb
    sSql = sSql & " INNER JOIN proposta_adesao_tb  WITH (NOLOCK) "
    sSql = sSql & "    ON proposta_adesao_tb.proposta_bb = emi_proposta_tb.proposta_bb"
    sSql = sSql & " INNER JOIN proposta_tb  WITH (NOLOCK) "
    sSql = sSql & "    ON proposta_tb.proposta_id = proposta_adesao_tb.proposta_id"
    'FIM
    sSql = sSql & " INNER JOIN produto_tb  WITH (NOLOCK) " & vbNewLine
    sSql = sSql & "    ON produto_tb.produto_id = proposta_tb.produto_id" & vbNewLine
    sSql = sSql & " WHERE /*emi_proposta_tb.situacao = 'n'" & vbNewLine
    sSql = sSql & "   AND */proposta_tb.situacao in ('a', 'o') "
    sSql = sSql & "   AND emi_proposta_tb.dt_inicio_vigencia > '" & sDataSistema & "'" & vbNewLine
    sSql = sSql & "   AND produto_tb.produto_id = " & iProdutoId & vbNewLine
    sSql = sSql & " UNION" & vbNewLine
    sSql = sSql & "SELECT emi_proposta_tb.dt_contratacao," & vbNewLine
    sSql = sSql & "       ISNULL(emi_proposta_tb.dt_inicio_vigencia, '') dt_inicio_vigencia," & vbNewLine
    sSql = sSql & "       ISNULL(emi_proposta_tb.dt_fim_vigencia, '') dt_fim_vigencia," & vbNewLine
    sSql = sSql & "       emi_proposta_tb.proposta_bb," & vbNewLine
    sSql = sSql & "       ISNULL(emi_proposta_tb.proponente, '') proponente" & vbNewLine
    sSql = sSql & "  FROM emi_proposta_tb WITH (NOLOCK) " & vbNewLine
    'sSql = sSql & " INNER JOIN arquivo_produto_tb  WITH (NOLOCK) " & vbNewLine
    'sSql = sSql & "    ON SUBSTRING(emi_proposta_tb.arquivo, 1, CHARINDEX('.', emi_proposta_tb.arquivo) - 1) = arquivo_produto_tb.arquivo" & vbNewLine
    'sSql = sSql & "   AND emi_proposta_tb.produto_externo = arquivo_produto_tb.produto_externo_id" & vbNewLine
    'Alessandra - Incluindo proposta_fechada_tb
    sSql = sSql & " INNER JOIN proposta_fechada_tb  WITH (NOLOCK) "
    sSql = sSql & "    ON proposta_fechada_tb.proposta_bb = emi_proposta_tb.proposta_bb"
    sSql = sSql & " INNER JOIN proposta_tb  WITH (NOLOCK) "
    sSql = sSql & "    ON proposta_tb.proposta_id = proposta_fechada_tb.proposta_id"
    'FIM
    sSql = sSql & " INNER JOIN produto_tb  WITH (NOLOCK) " & vbNewLine
    sSql = sSql & "    ON produto_tb.produto_id = proposta_tb.produto_id" & vbNewLine
    sSql = sSql & " WHERE /*emi_proposta_tb.situacao = 'n'" & vbNewLine
    sSql = sSql & "   AND */proposta_tb.situacao in ('a', 'o')  "
    sSql = sSql & "   AND emi_proposta_tb.dt_inicio_vigencia > '" & sDataSistema & "'" & vbNewLine
    sSql = sSql & "   AND produto_tb.produto_id = " & iProdutoId & vbNewLine
    'sSql = sSql & " ORDER BY proposta_bb" & vbNewLine
    sSql = sSql & " ORDER BY 4     /*  ordenar pela proposta_bb, que � a 4� coluna  */" & vbNewLine

    Set ConsultarEmissaoFuturaAnalitico = ExecutarSQL(sSiglaSistema, _
                                                      iAmbienteId, _
                                                      sSiglaRecurso, _
                                                      sDescricaoRecurso, _
                                                      sSql, _
                                                      True)

    Exit Function

Erro:

    Call Err.Raise(Err.Number, , "SEGL0026.cls00166.ConsultarEmissaoFuturaAnalitico - " & Err.Description)

End Function



