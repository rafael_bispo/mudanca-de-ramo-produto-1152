VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cls00406"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Public Function ConsultarComentarioProposta(ByVal sSiglaSistema As String, _
                                            ByVal sSiglaRecurso As String, _
                                            ByVal sDescricaoRecurso As String, _
                                            ByVal iAmbienteId As Integer, _
                                            ByVal iTipoPesquisa As Byte, _
                                            ByVal lConteudo As Long, _
                                            ByVal sUsuario As String, _
                                            ByVal lSeguradoraId As Long, _
                                            ByVal lSucursalId As Long, _
                                            ByVal LramoId As Long, _
                                            Optional ByVal bDescricaoParcial As Boolean = True, _
                                            Optional ByVal bSomenteAtivos As Boolean = True, _
                                            Optional ByVal bMostrarTodosComentarios As Boolean = False) As Recordset

    Dim sSql As String
    Dim lUnidadeUsuario As Long

    On Error GoTo Trata_Erro

    If iTipoPesquisa = 1 Then    'pelo c�digo do coment�rio

        sSql = ""
        sSql = sSql & "SELECT proposta_comentario_tb.comentario_id," & vbNewLine
        sSql = sSql & "       proposta_comentario_tb.proposta_id," & vbNewLine

        If bDescricaoParcial Then
            sSql = sSql & "       SUBSTRING(proposta_comentario_tb.descricao, 1, 50) descricao," & vbNewLine
        Else
            sSql = sSql & "       proposta_comentario_tb.descricao," & vbNewLine
        End If

        sSql = sSql & "       proposta_comentario_tb.situacao," & vbNewLine
        sSql = sSql & "       proposta_comentario_tb.restrito," & vbNewLine
        sSql = sSql & "       proposta_comentario_tb.dt_inclusao," & vbNewLine
        sSql = sSql & "       proposta_comentario_tb.dt_alteracao," & vbNewLine
        sSql = sSql & "       proposta_comentario_tb.usuario_inclusao usuario," & vbNewLine
        sSql = sSql & "       usuario_tb.nome," & vbNewLine
        sSql = sSql & "       proposta_comentario_tb.assunto" & vbNewLine
        sSql = sSql & "  FROM seguros_db..proposta_comentario_tb proposta_comentario_tb  WITH (NOLOCK)  " & vbNewLine

        'Inicio:INC000004240028 Autor:Cibele Pereira Data:21/01/2014
        Select Case iAmbienteId

        Case 6, 7
            sSql = sSql & " INNER JOIN abss.segab_db.dbo.usuario_tb usuario_tb  WITH (NOLOCK)  " & vbNewLine
        Case 2, 3
            sSql = sSql & " INNER JOIN segab_db..usuario_tb usuario_tb  WITH (NOLOCK)  " & vbNewLine

        End Select
        'Fim:INC000004240028 Autor:Cibele Pereira Data:21/01/2014

        sSql = sSql & "    ON usuario_tb.cpf = proposta_comentario_tb.usuario_inclusao" & vbNewLine
        sSql = sSql & " INNER JOIN seguros_db..proposta_tb proposta_tb   WITH (NOLOCK)  " & vbNewLine
        sSql = sSql & "    ON proposta_tb.proposta_id = proposta_comentario_tb.proposta_id " & vbNewLine
        sSql = sSql & " WHERE proposta_comentario_tb.comentario_id = " & lConteudo
        sSql = sSql & " and proposta_tb.situacao <> 't' "

    Else

        lUnidadeUsuario = ObterUnidadeUsuario(sSiglaSistema, _
                                              sSiglaRecurso, _
                                              sDescricaoRecurso, _
                                              iAmbienteId, _
                                              sUsuario)

        sSql = ""
        sSql = sSql & "SELECT proposta_comentario_tb.comentario_id," & vbNewLine
        sSql = sSql & "       proposta_comentario_tb.proposta_id," & vbNewLine

        If bDescricaoParcial Then
            sSql = sSql & "       SUBSTRING(proposta_comentario_tb.descricao, 1, 50) descricao," & vbNewLine
        Else
            sSql = sSql & "       proposta_comentario_tb.descricao," & vbNewLine
        End If

        ''pcarvalho 18/10/2005
        If bMostrarTodosComentarios Then
            If lUnidadeUsuario = -1 Then
                sSql = sSql & "       usuario_unidade_permitida = 'N'," & vbNewLine
            Else
                sSql = sSql & "       usuario_unidade_permitida = 'S'," & vbNewLine
            End If
        End If

        sSql = sSql & "       proposta_comentario_tb.situacao," & vbNewLine
        sSql = sSql & "       proposta_comentario_tb.restrito," & vbNewLine
        sSql = sSql & "       proposta_comentario_tb.dt_inclusao," & vbNewLine
        sSql = sSql & "       proposta_comentario_tb.dt_alteracao," & vbNewLine
        sSql = sSql & "       proposta_comentario_tb.usuario_inclusao usuario," & vbNewLine
        sSql = sSql & "       usuario_tb.nome," & vbNewLine
        sSql = sSql & "       proposta_comentario_tb.assunto," & vbNewLine
        sSql = sSql & "       apolice_tb.apolice_id," & vbNewLine
        sSql = sSql & "       CASE WHEN proposta_fechada_tb.proposta_bb IS NULL" & vbNewLine
        sSql = sSql & "            THEN proposta_adesao_tb.proposta_bb" & vbNewLine
        sSql = sSql & "            ELSE proposta_fechada_tb.proposta_bb" & vbNewLine
        sSql = sSql & "       END proposta_bb," & vbNewLine
        sSql = sSql & "       proposta_tb.ramo_id," & vbNewLine
        sSql = sSql & "       ramo_tb.nome nome_ramo" & vbNewLine
        sSql = sSql & "  FROM seguros_db..proposta_comentario_tb proposta_comentario_tb  WITH (NOLOCK)  " & vbNewLine
        sSql = sSql & " INNER JOIN segab_db..usuario_tb usuario_tb  WITH (NOLOCK)  " & vbNewLine
        sSql = sSql & "    ON usuario_tb.cpf = proposta_comentario_tb.usuario_inclusao and usuario_tb.situacao = 'A' " & vbNewLine
        sSql = sSql & "  LEFT JOIN seguros_db..apolice_tb apolice_tb  WITH (NOLOCK)  " & vbNewLine
        sSql = sSql & "    ON apolice_tb.proposta_id = proposta_comentario_tb.proposta_id" & vbNewLine
        sSql = sSql & "  LEFT JOIN seguros_db..proposta_fechada_tb proposta_fechada_tb  WITH (NOLOCK)  " & vbNewLine
        sSql = sSql & "    ON proposta_fechada_tb.proposta_id = proposta_comentario_tb.proposta_id" & vbNewLine
        sSql = sSql & "  LEFT JOIN seguros_db..proposta_adesao_tb proposta_adesao_tb  WITH (NOLOCK)  " & vbNewLine
        sSql = sSql & "    ON proposta_adesao_tb.proposta_id = proposta_comentario_tb.proposta_id" & vbNewLine
        sSql = sSql & " INNER JOIN seguros_db..proposta_tb proposta_tb  WITH (NOLOCK)  " & vbNewLine
        sSql = sSql & "    ON proposta_tb.proposta_id = proposta_comentario_tb.proposta_id" & vbNewLine
        sSql = sSql & " INNER JOIN seguros_db..ramo_tb ramo_tb  WITH (NOLOCK)  " & vbNewLine
        sSql = sSql & "    ON ramo_tb.ramo_id = proposta_tb.ramo_id" & vbNewLine

        If iTipoPesquisa = 2 Then    'por Ap�lice

            sSql = sSql & " WHERE apolice_tb.apolice_id = " & lConteudo & vbNewLine
            sSql = sSql & "   AND apolice_tb.sucursal_seguradora_id = " & lSucursalId & vbNewLine
            sSql = sSql & "   AND apolice_tb.seguradora_cod_susep = " & lSeguradoraId & vbNewLine
            sSql = sSql & "   AND apolice_tb.ramo_id = " & LramoId & vbNewLine

        ElseIf iTipoPesquisa = 3 Then    'por Proposta

            sSql = sSql & " WHERE proposta_comentario_tb.proposta_id = " & lConteudo & vbNewLine

        ElseIf iTipoPesquisa = 4 Then    'por PropostaBB

            sSql = sSql & " WHERE CASE WHEN proposta_fechada_tb.proposta_bb IS NULL" & vbNewLine
            sSql = sSql & "            THEN proposta_adesao_tb.proposta_bb" & vbNewLine
            sSql = sSql & "            ELSE proposta_fechada_tb.proposta_bb" & vbNewLine
            sSql = sSql & "       END = " & lConteudo & vbNewLine

        End If

        If bSomenteAtivos Then
            sSql = sSql & "   AND proposta_comentario_tb.situacao = 'a'" & vbNewLine
        End If

        ''pcarvalho 18/10/2005
        If lUnidadeUsuario = -1 And bMostrarTodosComentarios = False Then
            sSql = sSql & "   AND proposta_comentario_tb.restrito = 'n'" & vbNewLine
        End If

        sSql = sSql & " and proposta_tb.situacao <> 't' "

        sSql = sSql & " ORDER BY proposta_comentario_tb.comentario_id" & vbNewLine

    End If

    Set ConsultarComentarioProposta = ExecutarSQL(sSiglaSistema, _
                                                  iAmbienteId, _
                                                  sSiglaRecurso, _
                                                  sDescricaoRecurso, _
                                                  sSql, _
                                                  True)

    Exit Function

Trata_Erro:

    Call Err.Raise(Err.Number, , "SEGL0026.cls00406.ConsultarComentarioProposta - " & Err.Description)

End Function

Public Function ObterPropostaComentario(ByVal sSiglaSistema As String, _
                                        ByVal sSiglaRecurso As String, _
                                        ByVal sDescricaoRecurso As String, _
                                        ByVal iAmbienteId As Integer, _
                                        ByVal iTipoPesquisa As Byte, _
                                        ByVal lConteudo As Long, _
                                        ByVal lSeguradoraId As Long, _
                                        ByVal lSucursalId As Long, _
                                        ByVal LramoId As Long) As Long

    Dim sSql As String
    Dim rs As Recordset

    On Error GoTo Trata_Erro

    sSql = ""
    sSql = sSql & "SELECT proposta_tb.proposta_id" & vbNewLine
    sSql = sSql & "  FROM proposta_tb  WITH (NOLOCK)  " & vbNewLine

    If iTipoPesquisa = 1 Then    'pelo c�digo do coment�rio

        sSql = sSql & "  LEFT JOIN proposta_comentario_tb  WITH (NOLOCK)  "
        sSql = sSql & "    ON proposta_comentario_tb.proposta_id = proposta_tb.proposta_id"
        sSql = sSql & " WHERE proposta_comentario_tb.comentario_id = " & lConteudo

    ElseIf iTipoPesquisa = 2 Then    'por Ap�lice

        sSql = sSql & "  INNER JOIN apolice_tb  WITH (NOLOCK)  " & vbNewLine
        sSql = sSql & "     ON apolice_tb.proposta_id = proposta_tb.proposta_id" & vbNewLine
        sSql = sSql & "  WHERE apolice_tb.apolice_id = " & lConteudo & vbNewLine
        sSql = sSql & "    AND apolice_tb.sucursal_seguradora_id = " & lSucursalId & vbNewLine
        sSql = sSql & "    AND apolice_tb.seguradora_cod_susep = " & lSeguradoraId & vbNewLine
        sSql = sSql & "    AND apolice_tb.ramo_id = " & LramoId & vbNewLine

    ElseIf iTipoPesquisa = 3 Then    'por Proposta

        sSql = sSql & "  WHERE proposta_tb.proposta_id = " & lConteudo & vbNewLine

    ElseIf iTipoPesquisa = 4 Then    'por PropostaBB

        sSql = sSql & "  LEFT JOIN proposta_fechada_tb  WITH (NOLOCK)  " & vbNewLine
        sSql = sSql & "    ON proposta_fechada_tb.proposta_id = proposta_tb.proposta_id" & vbNewLine
        sSql = sSql & "  LEFT JOIN proposta_adesao_tb  WITH (NOLOCK)  " & vbNewLine
        sSql = sSql & "    ON proposta_adesao_tb.proposta_id = proposta_tb.proposta_id" & vbNewLine
        sSql = sSql & " WHERE CASE WHEN proposta_fechada_tb.proposta_bb IS NULL" & vbNewLine
        sSql = sSql & "            THEN proposta_adesao_tb.proposta_bb" & vbNewLine
        sSql = sSql & "            ELSE proposta_fechada_tb.proposta_bb" & vbNewLine
        sSql = sSql & "       END = " & lConteudo & vbNewLine

    End If

    Set rs = ExecutarSQL(sSiglaSistema, _
                         iAmbienteId, _
                         sSiglaRecurso, _
                         sDescricaoRecurso, _
                         sSql, _
                         True)

    If rs.EOF Then
        ObterPropostaComentario = -1
    Else
        ObterPropostaComentario = rs(0)
    End If

    rs.Close

    Set rs = Nothing

    Exit Function

Trata_Erro:

    Call Err.Raise(Err.Number, , "SEGL0026.cls00406.ObterProposta - " & Err.Description)

End Function

Public Function ObterUnidadeUsuario(ByVal sSiglaSistema As String, _
                                    ByVal sSiglaRecurso As String, _
                                    ByVal sDescricaoRecurso As String, _
                                    ByVal iAmbienteId As Integer, _
                                    ByVal sUsuario As String) As Long

    Dim sSql As String
    Dim rs As Recordset
    'Confitec RJ - jessica.adao - 21/10/2011 - INC000002814012
    'Enxergar somente lado AB para a base segab_db
    Dim sLinkedServer As String
    sLinkedServer = "segab_db.dbo."

    Select Case (iAmbienteId)
    Case 2, 3
        sLinkedServer = "segab_db.dbo."
    Case 6, 7
        sLinkedServer = "ABSS.segab_db.dbo."
    End Select

    On Error GoTo Trata_Erro

    sSql = ""
    sSql = sSql & "SELECT unidade_negocio_permitida_tb.unidade_id" & vbNewLine
    sSql = sSql & "  FROM seguros_db..unidade_negocio_permitida_tb unidade_negocio_permitida_tb  WITH (NOLOCK)  " & vbNewLine
    'Confitec RJ - jessica.adao - 21/10/2011 - INC000002814012
    'Enxergar somente lado AB para a base segab_db
    'sSQL = sSQL & " INNER JOIN segab_db..unidade_tb unidade_tb" & vbNewLine
    sSql = sSql & "  JOIN " & sLinkedServer & "unidade_tb unidade_tb  WITH (NOLOCK)  " & vbNewLine
    sSql = sSql & "    ON unidade_tb.unidade_id = unidade_negocio_permitida_tb.unidade_id" & vbNewLine

    'Inicio da altera��o - raandre 28/03/07
    'A partir desta altera��o o sistema ir� checar a unidade_id do usuario na tab. segab_db..usuario_unidade_tb
    'sSql = sSql & " INNER JOIN segab_db..usuario_tb usuario_tb" & vbNewLine
    'sSql = sSql & "    ON usuario_tb.unidade_id = unidade_negocio_permitida_tb.unidade_id" & vbNewLine

    'Confitec RJ - jessica.adao - 21/10/2011 - INC000002814012
    'Enxergar somente lado AB para a base segab_db
    'sSQL = sSQL & " INNER JOIN segab_db..usuario_unidade_tb usuario_unidade_tb" & vbNewLine
    sSql = sSql & "  JOIN " & sLinkedServer & "usuario_unidade_tb usuario_unidade_tb  WITH (NOLOCK)  " & vbNewLine
    sSql = sSql & " ON usuario_unidade_tb.unidade_id = unidade_negocio_permitida_tb.unidade_id" & vbNewLine

    'Confitec RJ - jessica.adao - 21/10/2011 - INC000002814012
    'Enxergar somente lado AB para a base segab_db
    'sSQL = sSQL & " INNER JOIN segab_db..usuario_tb usuario_tb" & vbNewLine
    sSql = sSql & "  JOIN " & sLinkedServer & "usuario_tb usuario_tb  WITH (NOLOCK)  " & vbNewLine
    sSql = sSql & " ON usuario_tb.usuario_id = usuario_unidade_tb.usuario_id" & vbNewLine
    'Inicio da altera��o - raandre 28/03/07

    sSql = sSql & " WHERE unidade_tb.dt_fim_vigencia IS NULL" & vbNewLine
    sSql = sSql & "   AND unidade_tb.situacao = 'a'" & vbNewLine
    '''' bneves em 07/03/2008 - FLOW 280845 ''''''''''''''''''''''''''
    sSql = sSql & "   AND usuario_tb.cpf = '" & sUsuario & "'" & vbNewLine
    sSql = sSql & "   AND usuario_tb.dt_inicio_vigencia < = GETDATE()"
    sSql = sSql & "   AND (    usuario_tb.dt_fim_vigencia > = GETDATE()"
    sSql = sSql & "        OR  usuario_tb.dt_fim_vigencia IS NULL)"
    sSql = sSql & "   AND usuario_tb.situacao = 'A'"
    ''''''' fim '''''''''''''''''''''
    Set rs = ExecutarSQL(sSiglaSistema, _
                         iAmbienteId, _
                         sSiglaRecurso, _
                         sDescricaoRecurso, _
                         sSql, _
                         True)

    If rs.EOF Then
        ObterUnidadeUsuario = -1
    Else
        ObterUnidadeUsuario = rs(0)
    End If

    rs.Close

    Set rs = Nothing

    Exit Function

Trata_Erro:

    Call Err.Raise(Err.Number, , "SEGL0026.cls00406.ObterUnidadeUsuario - " & Err.Description)

End Function

Public Sub IncluirComentarioProposta(ByVal sSiglaSistema As String, _
                                     ByVal sSiglaRecurso As String, _
                                     ByVal sDescricaoRecurso As String, _
                                     ByVal lAmbienteID As Long, _
                                     ByVal lProposta_id As Long, _
                                     ByVal sDescricao As String, _
                                     ByVal sRestrito As String, _
                                     ByVal sSituacao As String, _
                                     ByVal sUsuario As String, _
                                     ByVal sAssunto As String)

    Dim sSql As String

    On Error GoTo Trata_Erro

    sSql = "EXEC SEGS4121_SPI "
    sSql = sSql & lProposta_id & "," & vbNewLine
    sSql = sSql & "'" & sDescricao & "'," & vbNewLine
    sSql = sSql & "'" & sRestrito & "'," & vbNewLine
    sSql = sSql & "'" & sSituacao & "'," & vbNewLine
    sSql = sSql & "'" & sUsuario & "'," & vbNewLine
    sSql = sSql & "'" & sAssunto & "'" & vbNewLine

    Call ExecutarSQL(sSiglaSistema, _
                     lAmbienteID, _
                     sSiglaRecurso, _
                     sDescricaoRecurso, _
                     sSql, _
                     False)

    Exit Sub

Trata_Erro:

    Call Err.Raise(Err.Number, , "SEGL0026.cls00406.IncluirComentarioProposta - " & Err.Description)

End Sub

Public Sub AlterarSituacaoComentario(ByVal sSiglaSistema As String, _
                                     ByVal sSiglaRecurso As String, _
                                     ByVal sDescricaoRecurso As String, _
                                     ByVal lAmbienteID As Long, _
                                     ByVal lComentarioId As Long, _
                                     ByVal sSituacao As String, _
                                     ByVal sUsuario As String, _
                                     ByVal sRestrito As String)

    Dim sSql As String

    On Error GoTo Trata_Erro

    sSql = "EXEC SEGS4122_SPU "
    sSql = sSql & lComentarioId & "," & vbNewLine
    sSql = sSql & "'" & sSituacao & "'," & vbNewLine
    sSql = sSql & "'" & sUsuario & "'," & vbNewLine
    sSql = sSql & "'" & sRestrito & "'" & vbNewLine

    Call ExecutarSQL(sSiglaSistema, _
                     lAmbienteID, _
                     sSiglaRecurso, _
                     sDescricaoRecurso, _
                     sSql, _
                     False)

    Exit Sub

Trata_Erro:

    Call Err.Raise(Err.Number, , "SEGL0026.cls00406.AlterarSituacaoComentario - " & Err.Description)

End Sub

Public Function ConsultarUnidadeNegocioPermitida(ByVal sSiglaSistema As String, _
                                                 ByVal sSiglaRecurso As String, _
                                                 ByVal sDescricaoRecurso As String, _
                                                 ByVal iAmbienteId As Integer, _
                                                 ByVal iTipoPesquisa As Byte, _
                                                 ByVal sConteudo As String) As Recordset

'M�todo para consultar em unidade_negocio_permitida_tb

    Dim sSql As String

    On Error GoTo Trata_Erro

    sSql = ""
    sSql = sSql & "SELECT unidade_id," & vbNewLine
    sSql = sSql & "       sigla," & vbNewLine
    sSql = sSql & "       nome," & vbNewLine
    sSql = sSql & "       descricao," & vbNewLine
    sSql = sSql & "       dt_inclusao," & vbNewLine
    sSql = sSql & "       dt_alteracao," & vbNewLine
    sSql = sSql & "       usuario" & vbNewLine
    sSql = sSql & "  FROM unidade_negocio_permitida_tb  WITH (NOLOCK)  " & vbNewLine

    Select Case iTipoPesquisa

    Case 1    'por C�digo
        sSql = sSql & "WHERE unidade_id = " & sConteudo & vbNewLine

    Case 2    'por Nome
        sSql = sSql & "WHERE nome LIKE '%" & sConteudo & "%'" & vbNewLine

    Case 3    'por Sigla
        sSql = sSql & "WHERE sigla LIKE '%" & sConteudo & "%'" & vbNewLine

    End Select

    sSql = sSql & " ORDER BY unidade_id" & vbNewLine

    Set ConsultarUnidadeNegocioPermitida = ExecutarSQL(sSiglaSistema, _
                                                       iAmbienteId, _
                                                       sSiglaRecurso, _
                                                       sDescricaoRecurso, _
                                                       sSql, _
                                                       True)

    Exit Function

Trata_Erro:

    Call Err.Raise(Err.Number, , "SEGL0026.cls00406.ConsultarUnidadeNegocioPermitida - " & Err.Description)

End Function

Public Function ConsultarUnidadeNegocio(ByVal sSiglaSistema As String, _
                                        ByVal sSiglaRecurso As String, _
                                        ByVal sDescricaoRecurso As String, _
                                        ByVal iAmbienteId As Integer, _
                                        ByVal iTipoPesquisa As Byte, _
                                        ByVal lConteudo As Long) As Recordset

'M�todo para obter as unidade de neg�cio em segab_db..unidade_tb

    Dim sSql As String

    On Error GoTo Trata_Erro

    sSql = ""
    sSql = sSql & "SELECT unidade_id," & vbNewLine
    sSql = sSql & "       sigla," & vbNewLine
    sSql = sSql & "       nome," & vbNewLine
    sSql = sSql & "       descricao" & vbNewLine
    sSql = sSql & "  FROM segab_db..unidade_tb  WITH (NOLOCK)  " & vbNewLine
    sSql = sSql & " WHERE situacao = 'a'" & vbNewLine
    sSql = sSql & "   AND dt_fim_vigencia IS NULL" & vbNewLine

    If iTipoPesquisa = 1 Then
        sSql = sSql & "   AND unidade_id = " & lConteudo & vbNewLine
    End If

    Set ConsultarUnidadeNegocio = ExecutarSQL(sSiglaSistema, _
                                              iAmbienteId, _
                                              sSiglaRecurso, _
                                              sDescricaoRecurso, _
                                              sSql, _
                                              True)

    Exit Function

Trata_Erro:

    Call Err.Raise(Err.Number, , "SEGL0026.cls00406.ConsultarUnidadeNegocio - " & Err.Description)

End Function

Public Sub ExcluirPermissaoUnidade(ByVal sSiglaSistema As String, _
                                   ByVal sSiglaRecurso As String, _
                                   ByVal sDescricaoRecurso As String, _
                                   ByVal lAmbienteID As Long, _
                                   ByVal lUnidadeId As Long)

    Dim sSql As String

    On Error GoTo Trata_Erro

    sSql = "EXEC SEGS4169_SPD "
    sSql = sSql & lUnidadeId & vbNewLine

    Call ExecutarSQL(sSiglaSistema, _
                     lAmbienteID, _
                     sSiglaRecurso, _
                     sDescricaoRecurso, _
                     sSql, _
                     False)

    Exit Sub

Trata_Erro:

    Call Err.Raise(Err.Number, , "SEGL0026.cls00406.ExcluirPermissaoUnidade - " & Err.Description)

End Sub

Public Sub IncluirPermissaoUnidade(ByVal sSiglaSistema As String, _
                                   ByVal sSiglaRecurso As String, _
                                   ByVal sDescricaoRecurso As String, _
                                   ByVal lAmbienteID As Long, _
                                   ByVal lUnidadeId As Long, _
                                   ByVal sSigla As String, _
                                   ByVal sNome As String, _
                                   ByVal sDescricao As String, _
                                   ByVal sUsuario As String)

    Dim sSql As String

    On Error GoTo Trata_Erro

    sSql = "EXEC SEGS4168_SPI "
    sSql = sSql & lUnidadeId & "," & vbNewLine
    sSql = sSql & "'" & sSigla & "'," & vbNewLine
    sSql = sSql & "'" & sNome & "'," & vbNewLine
    sSql = sSql & "'" & sDescricao & "'," & vbNewLine
    sSql = sSql & "'" & sUsuario & "'" & vbNewLine

    Call ExecutarSQL(sSiglaSistema, _
                     lAmbienteID, _
                     sSiglaRecurso, _
                     sDescricaoRecurso, _
                     sSql, _
                     False)

    Exit Sub

Trata_Erro:

    Call Err.Raise(Err.Number, , "SEGL0026.cls00406.IncluirPermissaoUnidade - " & Err.Description)

End Sub

