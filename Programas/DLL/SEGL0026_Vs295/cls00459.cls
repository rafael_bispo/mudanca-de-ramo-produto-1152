VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cls00459"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
'local variable(s) to hold property value(s)
Private mvarNoOrdem As String    'local copy
Private mvarPersistente As Boolean    'local copy
Private mvarOperacao As String    'local copy
Private mvarDtIniParticipacao As Date    'local copy
Private mvarNome As String    'local copy
Private mvarSeguradora As String    'local copy
Private mvarPerc_Participacao As String    'local copy
Private mvarPerc_Despesa As String    'local copy
Public Property Let Perc_Despesa(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Perc_Despesa = 5
    mvarPerc_Despesa = vData
End Property


Public Property Get Perc_Despesa() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Perc_Despesa
    Perc_Despesa = mvarPerc_Despesa
End Property



Public Property Let Perc_Participacao(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Perc_Participacao = 5
    mvarPerc_Participacao = vData
End Property


Public Property Get Perc_Participacao() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Perc_Participacao
    Perc_Participacao = mvarPerc_Participacao
End Property



Public Property Let Seguradora(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Seguradora = 5
    mvarSeguradora = vData
End Property


Public Property Get Seguradora() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Seguradora
    Seguradora = mvarSeguradora
End Property



Public Property Let Nome(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Nome = 5
    mvarNome = vData
End Property


Public Property Get Nome() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Nome
    Nome = mvarNome
End Property



Public Property Let DtIniParticipacao(ByVal vData As Date)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.DtIniParticipacao = 5
    mvarDtIniParticipacao = vData
End Property


Public Property Get DtIniParticipacao() As Date
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.DtIniParticipacao
    DtIniParticipacao = mvarDtIniParticipacao
End Property



Public Property Let Operacao(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Operacao = 5
    mvarOperacao = vData
End Property


Public Property Get Operacao() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Operacao
    Operacao = mvarOperacao
End Property



Public Property Let Persistente(ByVal vData As Boolean)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Persistente = 5
    mvarPersistente = vData
End Property


Public Property Get Persistente() As Boolean
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Persistente
    Persistente = mvarPersistente
End Property



Public Property Let NoOrdem(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.NoOrdem = 5
    mvarNoOrdem = vData
End Property


Public Property Get NoOrdem() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.NoOrdem
    NoOrdem = mvarNoOrdem
End Property



