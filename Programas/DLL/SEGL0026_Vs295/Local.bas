Attribute VB_Name = "Local"
Public Declare Function GetPrivateProfileString Lib "kernel32" Alias "GetPrivateProfileStringA" (ByVal lpApplicationName As String, ByVal lpKeyName As Any, ByVal lpDefault As String, ByVal lpReturnedString As String, ByVal nSize As Long, ByVal lpFileName As String) As Long

Function TruncaDecimal(ByVal valor As Double, Optional ByVal posicao As Integer = 2) As Double

    Dim SinalDecimal As String, pos, ConfiguracaoBrasil
    SinalDecimal = LeArquivoIni("WIN.INI", "intl", "sDecimal")

    If SinalDecimal = "." Then
        ConfiguracaoBrasil = False
    Else
        ConfiguracaoBrasil = True
    End If

    If ConfiguracaoBrasil Then
        pos = InStr(valor, ",")
    Else
        pos = InStr(valor, ".")
    End If

    If pos <> 0 Then
        valor = Mid(valor, 1, pos + posicao)
    End If

    TruncaDecimal = valor

End Function

Public Function ExtrairIOF(ByRef cValorComIof As Currency, ByRef dPercIOF As Double) As Currency
' Retorna o valor sem o iof
    Dim valor As Currency

    valor = Trunca(cValorComIof * (dPercIOF / 100) / (1 + (dPercIOF / 100)))
    ExtrairIOF = valor

End Function

Function LeArquivoIni(ByVal FileName As String, ByVal SectionName As String, ByVal Item As String) As String

    Dim Retorno As String * 100, RetornoDefault, nc

    RetornoDefault = "*"
    nc = GetPrivateProfileString(SectionName, Item, RetornoDefault, Retorno, Len(Retorno), FileName)
    LeArquivoIni = Left$(Retorno, nc)

End Function

Function MudaVirgulaParaPonto(ByVal valor As String) As String

    If InStr(valor, ",") = 0 Then
        MudaVirgulaParaPonto = valor
    Else
        valor = Mid$(valor, 1, InStr(valor, ",") - 1) + "." + Mid$(valor, InStr(valor, ",") + 1)
        MudaVirgulaParaPonto = valor
    End If

End Function

Function Trunca(valor) As Currency

    Dim SinalDecimal As String
    SinalDecimal = LeArquivoIni("WIN.INI", "intl", "sDecimal")

    If SinalDecimal = "." Then
        ConfiguracaoBrasil = False
    Else
        ConfiguracaoBrasil = True
    End If

    If ConfiguracaoBrasil Then
        pos = InStr(valor, ",")
    Else
        pos = InStr(valor, ".")
    End If

    If pos <> 0 Then
        valor = Mid(valor, 1, pos + 2)
    End If

    Trunca = valor

End Function

Public Sub adSQL(ByRef sSql As String, ByVal sCadeia As String)
    sSql = sSql & sCadeia & vbNewLine
End Sub
Public Function ObterRegistroEventoALS(ByVal sUsuario As String, _
                                       ByVal sSiglaSistema As String, _
                                       ByVal sSiglaRecurso As String, _
                                       ByVal sDescricaoRecurso As String, _
                                       ByVal lAmbienteID As Long, _
                                       ByVal lCodEventoBB As Long, _
                                       ParamArray sDefinicaoCampo()) As Variant

    Dim sSql As String
    Dim rs As Recordset
    Dim iIndice As Integer
    Dim sRegistro As String

    On Error GoTo TrataErro

    iIndice = 0

    'Obtendo os campos para forma��o do registro do evento ALS

    sSql = " "
    sSql = sSql & " SELECT coluna," & vbNewLine
    sSql = sSql & " tamanho_coluna" & vbNewLine
    sSql = sSql & "FROM interface_dados_db..evento_als_layout_tb evento_als_layout_tb  WITH (NOLOCK)  " & vbNewLine
    sSql = sSql & "JOIN interface_dados_db..evento_als_layout_coluna_tb evento_als_layout_coluna_tb  WITH (NOLOCK)  " & vbNewLine
    sSql = sSql & "  ON evento_als_layout_coluna_tb.evento_bb = evento_als_layout_tb.evento_bb" & vbNewLine
    sSql = sSql & "WHERE evento_als_layout_tb.evento_bb = " & lCodEventoBB & vbNewLine
    sSql = sSql & "ORDER BY seq_coluna"

    Set rs = ExecutarSQL(sSiglaSistema, _
                         lAmbienteID, _
                         sSiglaRecurso, _
                         sDescricaoRecurso, _
                         sSql, _
                         True)

    'Concatenando os campos para formar o registro
    While Not rs.EOF

        sRegistro = sRegistro & Format(sDefinicaoCampo(iIndice), String(rs("tamanho_coluna"), "0"))

        iIndice = iIndice + 1
        rs.MoveNext
    Wend

    ObterRegistroEventoALS = sRegistro

    Exit Function

TrataErro:
    Call Err.Raise(Err.Number, , "SEGL0026.cls00125.ObterRegistroEventoALS - " & Err.Description)

End Function


Public Function ObterDadosContratoALS(ByVal sSiglaSistema As String, _
                                      ByVal sSiglaRecurso As String, _
                                      ByVal sDescricaoRecurso As String, _
                                      ByVal lAmbiente As Long, _
                                      ByVal lPropostaId As Long) As Recordset

    Dim sSql As String

    On Error GoTo TrataErro

    sSql = ""
    sSql = sSql & "SELECT cod_produto, " & vbNewLine
    sSql = sSql & "       cod_modalidade," & vbNewLine
    sSql = sSql & "       cod_item_modalidade," & vbNewLine
    sSql = sSql & "       num_contrato_seguro," & vbNewLine
    sSql = sSql & "       num_versao_endosso = ISNULL((SELECT MAX(NR_VRS_EDS)" & vbNewLine
    sSql = sSql & "                                    FROM als_operacao_db..eds_ctr_sgro  WITH (NOLOCK)  " & vbNewLine
    sSql = sSql & "                                    WHERE cd_prd = cod_produto" & vbNewLine
    sSql = sSql & "                                      AND cd_mdld = cod_modalidade" & vbNewLine
    sSql = sSql & "                                      AND cd_item_mdld = cod_item_modalidade " & vbNewLine
    sSql = sSql & "                                      AND nr_ctr_sgro = num_contrato_seguro),1) " & vbNewLine
    sSql = sSql & "FROM seguros_db..proposta_processo_susep_tb  WITH (NOLOCK)  " & vbNewLine
    sSql = sSql & "WHERE proposta_id = " & lPropostaId

    Set ObterDadosContratoALS = ExecutarSQL(sSiglaSistema, _
                                            lAmbiente, _
                                            sSiglaRecurso, _
                                            sDescricaoRecurso, _
                                            sSql)

    Exit Function

TrataErro:
    Call Err.Raise(Err.Number, , "SEGL0026.cls00125.ObterDadosContratoALS - " & Err.Description)

End Function


Sub GravarEventoALS(ByVal sUsuario As String, _
                    ByVal sSiglaSistema As String, _
                    ByVal sSiglaRecurso As String, _
                    ByVal sDescricaoRecurso As String, _
                    ByVal lAmbienteID As Long, _
                    ByVal lCodProduto As Long, _
                    ByVal lCodModalidade As Long, _
                    ByVal lCodItemModalidade As Long, _
                    ByVal lCodEventoBB As Long, _
                    ByVal sRegistro As String, _
                    ByVal sDtEvento As String, _
                    ByVal lConexao As Long)

    On Error GoTo TrataErro

    Dim sSql As String

    sSql = "EXEC interface_dados_db..incluir_evento_als_contrato_spi " & vbNewLine
    sSql = sSql & lCodProduto & vbNewLine
    sSql = sSql & "," & lCodModalidade & vbNewLine
    sSql = sSql & "," & lCodItemModalidade & vbNewLine
    sSql = sSql & "," & lCodEventoBB & vbNewLine
    sSql = sSql & ", '" & sRegistro & "'" & vbNewLine
    sSql = sSql & ", '" & sUsuario & "'" & vbNewLine
    If sDtEvento <> "" Then
        sSql = sSql & ", '" & Format(sDtEvento, "yyyymmdd") & "'" & vbNewLine
    End If

    'Alterado pois 0 tamb�m � um lConexao v�lido
    'If lConexao > 0 Then
    If lConexao > -1 Then

        Call Conexao_ExecutarSQL(sSiglaSistema, _
                                 lAmbienteID, _
                                 sSiglaRecurso, _
                                 sDescricaoRecurso, _
                                 sSql, _
                                 lConexao, _
                                 False)

    Else
        Call ExecutarSQL(sSiglaSistema, lAmbienteID, sSiglaRecurso, sDescricaoRecurso, sSql, False)
    End If

    Exit Sub

TrataErro:
    Call Err.Raise(Err.Number, , "SEGL0026.cls00125.GravarEventoALS - " & Err.Description)

End Sub

Function ObterSubramoProposta(lPropostaId) As Long

    Dim sSql As String
    Dim rs As Recordset

    sSql = "SELECT subramo_id" & vbNewLine
    sSql = sSql & "FROM proposta_tb" & vbNewLine
    sSql = sSql & "WHERE proposta_id = " & lPropostaId


    Set rs = ExecutarSQL(sSiglaSistema, _
                         lAmbienteID, _
                         sSiglaRecurso, _
                         sDescricaoRecurso, _
                         sSql, _
                         True)

    ObterSubramoProposta = rs(0)

End Function
'Renato Silva - 17/01/2011 - Tratamento para duplicidade para o nosso_numero
'Flow 8250387
'Verifica se o n�mero do conv�nio � utilizado para uma ou duas empresas.'
Function ObterQtdeEmpresasPorConvenio(ByVal sSiglaSistema As String, _
                                      ByVal sSiglaRecurso As String, _
                                      ByVal sDescricaoRecurso As String, _
                                      ByVal lAmbiente As Long, _
                                      ByVal sNumConvenio As String) As Integer
    Dim sSql As String
    Dim rs As Recordset

    On Error GoTo TrataErro

    sSql = ""
    sSql = sSql & "  SELECT cod_empresa " & vbNewLine
    'sSQL = sSQL & "    INTO #verifica_convenio " & vbNewLine
    sSql = sSql & "    FROM tp_movimentacao_financ_tb " & vbNewLine
    sSql = sSql & "    JOIN ramo_tb " & vbNewLine
    sSql = sSql & "      ON ramo_tb.ramo_id = tp_movimentacao_financ_tb.ramo_id " & vbNewLine
    sSql = sSql & "   WHERE num_convenio = '" & sNumConvenio & "'" & vbNewLine
    sSql = sSql & "GROUP BY cod_empresa " & vbNewLine

    sSql = sSql & "UNION " & vbNewLine

    sSql = sSql & "  SELECT cod_empresa " & vbNewLine
    sSql = sSql & "    FROM abss.seguros_db.dbo.tp_movimentacao_financ_tb tp_movimentacao_financ_tb " & vbNewLine
    sSql = sSql & "    JOIN abss.seguros_db.dbo.ramo_tb ramo_tb " & vbNewLine
    sSql = sSql & "      ON ramo_tb.ramo_id = tp_movimentacao_financ_tb.ramo_id " & vbNewLine
    sSql = sSql & "   WHERE num_convenio = '" & sNumConvenio & "'" & vbNewLine
    sSql = sSql & "GROUP BY cod_empresa " & vbNewLine

    'sSQL = sSQL & "SELECT COUNT(cod_empresa) as qtd_empresas FROM #verifica_convenio "

    Set rs = ExecutarSQL(sSiglaSistema, _
                         lAmbiente, _
                         sSiglaRecurso, _
                         sDescricaoRecurso, _
                         sSql, _
                         True)

    If Not rs.EOF Then
        ObterQtdeEmpresasPorConvenio = rs.RecordCount
        'ObterQtdeEmpresasPorConvenio = rs("qtd_empresas")
    End If

    rs.Close
    Set rs = Nothing

    Exit Function

TrataErro:
    Call Err.Raise(Err.Number, , "SEGL0026.cls00125.ObterQtdeEmpresasPorConvenio - " & Err.Description)

End Function
Function MudaAspaSimples(ByVal valor As String) As String

    Do Until InStr(valor, "'") = 0
        valor = Mid$(valor, 1, InStr(valor, "'") - 1) + "�" + Mid$(valor, InStr(valor, "'") + 1)
    Loop

    MudaAspaSimples = UCase(valor)

End Function

'Camila Kume 06.07.2009 - Passa a gravar os eventos em uma tabela de controle ao inv�s de ger�-los
'direto para sa�da - Endossos - BB Prote��o
Sub GravarControleEventoALS(ByVal sUsuario As String, _
                            ByVal sSiglaSistema As String, _
                            ByVal sSiglaRecurso As String, _
                            ByVal sDescricaoRecurso As String, _
                            ByVal lAmbienteID As Long, _
                            ByVal lCodProduto As Long, _
                            ByVal lCodModalidade As Long, _
                            ByVal lCodItemModalidade As Long, _
                            ByVal lCodEventoBB As Long, _
                            ByVal sRegistro As String, _
                            ByVal sDtEvento As String, _
                            ByVal lConexao As Long)

    On Error GoTo TrataErro

    Dim sSql As String

    sSql = "EXEC interface_dados_db..segs7685_spi " & vbNewLine
    sSql = sSql & lCodProduto & vbNewLine
    sSql = sSql & "," & lCodModalidade & vbNewLine
    sSql = sSql & "," & lCodItemModalidade & vbNewLine
    sSql = sSql & "," & lCodEventoBB & vbNewLine
    sSql = sSql & ", '" & sRegistro & "'" & vbNewLine
    sSql = sSql & ", '" & Format(sDtEvento, "yyyymmdd") & "'" & vbNewLine
    sSql = sSql & ", '" & sUsuario & "'" & vbNewLine

    'Alterado pois 0 tamb�m � um lConexao v�lido
    'If lConexao > 0 Then
    If lConexao > -1 Then

        Call Conexao_ExecutarSQL(sSiglaSistema, _
                                 lAmbienteID, _
                                 sSiglaRecurso, _
                                 sDescricaoRecurso, _
                                 sSql, _
                                 lConexao, _
                                 False)

    Else
        Call ExecutarSQL(sSiglaSistema, lAmbienteID, sSiglaRecurso, sDescricaoRecurso, sSql, False)
    End If

    Exit Sub

TrataErro:
    Call Err.Raise(Err.Number, , "SEGL0026.cls00125.GravarControleEventoALS - " & Err.Description)

End Sub
'Camila Kume - 06.07.2009 - Fim


