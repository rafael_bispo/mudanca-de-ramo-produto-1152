VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cls00149"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Classe de atributo de corretagem (corretagem.cls)
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''


'local variable(s) to hold property value(s)
Private mvarProposta_Id As Long    'local copy
Private mvarCorretor_Id As Long    'local copy
Private mvarPercComissao As Currency    'local copy
Private mvarVal_Comissao As Currency    'local copy
Private mvarNome As String    'local copy
Private mvarVal_IR As Currency    'local copy
Private mvarTipo As String    'local copy
Private mvarSucursal As String    'local copy
Public Property Let Sucursal(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Sucursal = 5
    mvarSucursal = vData
End Property
Public Property Get Sucursal() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Sucursal
    Sucursal = mvarSucursal
End Property
Public Property Let Tipo(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Tipo = 5
    mvarTipo = vData
End Property
Public Property Get Tipo() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Tipo
    Tipo = mvarTipo
End Property
Public Property Let Val_IR(ByVal vData As Currency)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Val_IR = 5
    mvarVal_IR = vData
End Property
Public Property Get Val_IR() As Currency
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Val_IR
    Val_IR = mvarVal_IR
End Property
Public Property Let Nome(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Nome = 5
    mvarNome = vData
End Property
Public Property Get Nome() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Nome
    Nome = mvarNome
End Property
Public Property Let Val_Comissao(ByVal vData As Currency)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Val_Comissao = 5
    mvarVal_Comissao = vData
End Property
Public Property Get Val_Comissao() As Currency
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Val_Comissao
    Val_Comissao = mvarVal_Comissao
End Property
Public Property Let PercComissao(ByVal vData As Currency)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.PercComissao = 5
    mvarPercComissao = vData
End Property
Public Property Get PercComissao() As Currency
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.PercComissao
    PercComissao = mvarPercComissao
End Property
Public Property Let Corretor_Id(ByVal vData As Long)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Corretor_Id = 5
    mvarCorretor_Id = vData
End Property
Public Property Get Corretor_Id() As Long
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Corretor_Id
    Corretor_Id = mvarCorretor_Id
End Property
Public Property Let Proposta_Id(ByVal vData As Long)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Proposta_id = 5
    mvarProposta_Id = vData
End Property
Public Property Get Proposta_Id() As Long
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Proposta_id
    Proposta_Id = mvarProposta_Id
End Property

