VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cls00167"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit

Private lConexao As Long

Private Const ENDOSSO_REENQUADRAMENTO = 202
Private Const ENDOSSO_REAJUSTE = 203

Public Sub AbrirNovaTransacao(ByVal sSiglaSistema As String, _
                              ByVal sSiglaRecurso As String, _
                              ByVal sDescricaoRecurso As String, _
                              ByVal iAmbienteId As Integer)

    On Error GoTo Trata_Erro

    ' Abrindo Conexao ''''''''''''''''''''''''''''''''''''''''''''

    lConexao = AbrirConexao(sSiglaSistema, _
                            iAmbienteId, _
                            sSiglaRecurso, _
                            sDescricaoRecurso)

    ' Abrindo Transacao '''''''''''''''''''''''''''''''''''''''''''

    If Not AbrirTransacao(lConexao) Then
        Call Err.Raise(vbObjectError + 1000, , "SEGL0026.cls00124.AbrirTransacao - N�o foi poss�vel abrir transa��o")
    End If

    Exit Sub

Trata_Erro:

    Call Err.Raise(Err.Number, , "SEGL0999.AbrirTransacao - " & Err.Description)

End Sub

Public Sub ConfirmarTransacaoAtual()

    On Error GoTo Trata_Erro

    ' Fechando Transacao '''''''''''''''''''''''''''''''''''''''''''''

    If Not ConfirmarTransacao(lConexao) Then
        Call Err.Raise(vbObjectError + 1000, , "SEGL0026.cls00124.ConfirmarTransacao - N�o foi poss�vel confirmar transa��o")
    End If

    ' Fechando Conexao '''''''''''''''''''''''''''''''''''''''''''''''

    If Not FecharConexao(lConexao) Then
        Call Err.Raise(vbObjectError + 1000, , "SEGL0026.cls00124.ConfirmarTransacao - N�o foi poss�vel fechar conex�o")
    End If

    Exit Sub

Trata_Erro:

    Call Err.Raise(Err.Number, , "SEGL0999.ConfirmarTransacao - " & Err.Description)

End Sub

Public Sub RetornarTransacaoAtual()

    On Error GoTo Trata_Erro

    ' Retornando Transacao '''''''''''''''''''''''''''''''''''''''''''''

    If Not RetornarTransacao(lConexao) Then
        Call Err.Raise(vbObjectError + 1000, , "SEGL0026.cls00124.RetornarTransacao - N�o foi poss�vel retornar transa��o")
    End If

    ' Fechando Conexao '''''''''''''''''''''''''''''''''''''''''''''''

    If Not FecharConexao(lConexao) Then
        Call Err.Raise(vbObjectError + 1000, , "SEGL0026.cls00124.RetornarTransacao - N�o foi poss�vel fechar conex�o")
    End If

    Exit Sub

Trata_Erro:

    Call Err.Raise(Err.Number, , "SEGL0999.ConfirmarTransacao - " & Err.Description)

End Sub


Public Function ImportarArquivoReajuste(ByVal sSiglaSistema As String, _
                                        ByVal sSiglaRecurso As String, _
                                        ByVal sDescricaoRecurso As String, _
                                        ByVal iAmbienteId As Integer, _
                                        ByVal sNomeArqReajuste As String, _
                                        ByVal sUsuario As String) As Object


    Dim sSql As String

    On Error GoTo Trata_Erro

    sSql = "SET NOCOUNT ON "
    sSql = "EXEC importa_arquivo_atualizacao_grupo_especial_sps"
    sSql = sSql & " '" & sNomeArqReajuste & "'"
    sSql = sSql & ",'" & sUsuario & "'"

    Set ImportarArquivoReajuste = ExecutarSQL(sSiglaSistema, _
                                              iAmbienteId, _
                                              sSiglaRecurso, _
                                              sDescricaoRecurso, _
                                              sSql, _
                                              True)

    Exit Function

Trata_Erro:

    Call Err.Raise(Err.Number, , "SEGL0999.ImportarArquivoReajuste - " & Err.Description)

End Function

Public Function ObterDadosProposta(ByVal sSiglaSistema As String, _
                                   ByVal sSiglaRecurso As String, _
                                   ByVal sDescricaoRecurso As String, _
                                   ByVal iAmbienteId As Integer, _
                                   ByVal sPropostaBB As String) As Object

    Dim sSql As String

    On Error GoTo TrataErro

    sSql = ""
    sSql = sSql & "SELECT a.proposta_id, " & vbNewLine
    sSql = sSql & "       a.situacao, " & vbNewLine
    sSql = sSql & "       ISNULL(c.dt_nascimento, '19000101') as dt_nascimento, " & vbNewLine
    sSql = sSql & "       a.produto_id, " & vbNewLine
    sSql = sSql & "       d.proposta_id AS proposta_basica_id " & vbNewLine
    sSql = sSql & "  FROM proposta_tb a  WITH (NOLOCK)  " & vbNewLine
    sSql = sSql & " INNER JOIN proposta_adesao_tb b  WITH (NOLOCK)  " & vbNewLine
    sSql = sSql & "         ON a.proposta_id = b.proposta_id " & vbNewLine
    sSql = sSql & " INNER JOIN apolice_tb d  WITH (NOLOCK)  " & vbNewLine
    sSql = sSql & "         ON d.apolice_id = b.apolice_id " & vbNewLine
    sSql = sSql & "        AND d.ramo_id = b.ramo_id " & vbNewLine
    sSql = sSql & "        AND d.sucursal_seguradora_id = b.sucursal_seguradora_id " & vbNewLine
    sSql = sSql & "        AND d.seguradora_cod_susep = b.seguradora_cod_susep " & vbNewLine
    sSql = sSql & "  LEFT JOIN pessoa_fisica_tb c  WITH (NOLOCK)  " & vbNewLine
    sSql = sSql & "         ON c.pf_cliente_id = a.prop_cliente_id " & vbNewLine
    sSql = sSql & " WHERE a.produto_id = 136 " & vbNewLine
    sSql = sSql & "   AND b.proposta_bb = " & sPropostaBB

    Set ObterDadosProposta = Conexao_ExecutarSQL(sSiglaSistema, _
                                                 iAmbienteId, _
                                                 sSiglaRecurso, _
                                                 sDescricaoRecurso, _
                                                 sSql, _
                                                 lConexao, _
                                                 True)

    Exit Function

TrataErro:

    Call Err.Raise(Err.Number, , "SEGL0999.ObterDadosProposta - " & Err.Description)

End Function

Public Function LerEscolhaPlano(ByVal sSiglaSistema As String, _
                                ByVal sSiglaRecurso As String, _
                                ByVal sDescricaoRecurso As String, _
                                ByVal iAmbienteId As Integer, _
                                ByVal lPropostaId As Long) As Object

    Dim sSql As String

    On Error GoTo TrataErro

    sSql = ""
    sSql = sSql & "SELECT plano_id, " & vbNewLine
    sSql = sSql & "       dt_inicio_vigencia, dt_fim_vigencia, " & vbNewLine
    sSql = sSql & "       imp_segurada, " & vbNewLine
    sSql = sSql & "       val_premio, " & vbNewLine
    sSql = sSql & "       produto_id, " & vbNewLine
    sSql = sSql & "       dt_escolha, " & vbNewLine
    sSql = sSql & "       ISNULL(val_custo_uss,0) as val_custo_uss " & vbNewLine
    sSql = sSql & "  FROM escolha_plano_tb  WITH (NOLOCK)  " & vbNewLine
    sSql = sSql & " WHERE proposta_id = " & lPropostaId & vbNewLine
    sSql = sSql & "   AND dt_fim_vigencia IS NULL "

    Set LerEscolhaPlano = Conexao_ExecutarSQL(sSiglaSistema, _
                                              iAmbienteId, _
                                              sSiglaRecurso, _
                                              sDescricaoRecurso, _
                                              sSql, _
                                              lConexao, _
                                              True)

    Exit Function

TrataErro:

    Call Err.Raise(Err.Number, , "SEGL0999.LerEscolhaPlano - " & Err.Description)

End Function

Public Function ObterFatorAnual(ByVal sSiglaSistema As String, _
                                ByVal sSiglaRecurso As String, _
                                ByVal sDescricaoRecurso As String, _
                                ByVal iAmbienteId As Integer, _
                                ByVal lProdutoId As Long, _
                                ByVal iIdade As Integer) As Object

    Dim sSql As String
    Dim rs As Recordset

    On Error GoTo TrataErro

    sSql = ""
    sSql = sSql & " SELECT plano_id,"
    sSql = sSql & "        fator_renovacao " & vbNewLine
    sSql = sSql & "   FROM plano_tb  WITH (NOLOCK)  " & vbNewLine
    sSql = sSql & "  WHERE dt_fim_vigencia IS NULL " & vbNewLine
    sSql = sSql & "    AND produto_id = " & lProdutoId & vbNewLine
    sSql = sSql & "    AND idade_min <= " & iIdade & vbNewLine
    sSql = sSql & "    AND idade_max >= " & iIdade & vbNewLine

    Set ObterFatorAnual = Conexao_ExecutarSQL(sSiglaSistema, _
                                              iAmbienteId, _
                                              sSiglaRecurso, _
                                              sDescricaoRecurso, _
                                              sSql, _
                                              lConexao, _
                                              True)

    Exit Function

TrataErro:

    Call Err.Raise(Err.Number, , "SEGL0999.ObterFatorAnual- " & Err.Description)

End Function

Public Function ObterValorIGPM(ByVal sSiglaSistema As String, _
                               ByVal sSiglaRecurso As String, _
                               ByVal sDescricaoRecurso As String, _
                               ByVal iAmbienteId As Integer, _
                               ByRef lIndiceId As Long) As Currency

    Dim sSql As String
    Dim rs As Recordset

    On Error GoTo TrataErro

    sSql = ""
    sSql = sSql & "  SELECT indice_tb.indice_id, " & vbNewLine
    sSql = sSql & "         val_indice_tb.val_indice " & vbNewLine
    sSql = sSql & "    FROM indice_tb  WITH (NOLOCK)  " & vbNewLine
    sSql = sSql & "    JOIN val_indice_tb  WITH (NOLOCK)  " & vbNewLine
    sSql = sSql & "      ON val_indice_tb.indice_id = indice_tb.indice_id " & vbNewLine
    sSql = sSql & "     AND val_indice_tb.dt_fim_vigencia IS NULL " & vbNewLine
    sSql = sSql & "   WHERE nome = 'IGP-M' " & vbNewLine

    Set rs = Conexao_ExecutarSQL(sSiglaSistema, _
                                 iAmbienteId, _
                                 sSiglaRecurso, _
                                 sDescricaoRecurso, _
                                 sSql, _
                                 lConexao, _
                                 True)

    If Not rs.EOF Then
        ObterValorIGPM = CCur(rs("val_indice"))
        lIndiceId = Val(rs("indice_id"))
    Else
        ObterValorIGPM = 0
        lIndiceId = 0
    End If

    Set rs = Nothing

    Exit Function

TrataErro:

    Call Err.Raise(Err.Number, , "SEGL0999.ObterValorIGPM- " & Err.Description)

End Function

Public Sub AtualizarEscolhaPlano(ByVal sSiglaSistema As String, _
                                 ByVal sSiglaRecurso As String, _
                                 ByVal sDescricaoRecurso As String, _
                                 ByVal iAmbienteId As Integer, _
                                 ByVal sDtInicioVigencia As String, _
                                 ByVal sDtOperacional As String, _
                                 ByVal lPropostaId As Long, _
                                 ByVal lProdutoId As Long, _
                                 ByVal lPlano As Long, _
                                 ByVal sValIS As String, _
                                 ByVal sValPremio As String, _
                                 ByVal lTipoProc As Long, _
                                 ByVal sUsuario As String, _
                                 ByVal vDtAux As Variant, _
                                 ByVal sValCustoUss As String)

    Dim sSql As String

    On Error GoTo TrataErro

    sSql = "SET NOCOUNT ON "
    sSql = sSql & "EXEC escolha_plano_spi "
    sSql = sSql & " '" & sDtInicioVigencia & "'"
    sSql = sSql & ",'" & sDtOperacional & "'"
    sSql = sSql & ", " & lPropostaId
    sSql = sSql & ", " & lProdutoId
    sSql = sSql & ", " & lPlano
    sSql = sSql & ", " & sValIS
    sSql = sSql & ", " & sValPremio
    sSql = sSql & ", " & lTipoProc
    sSql = sSql & ",'" & sUsuario & "'"
    sSql = sSql & ", " & vDtAux
    sSql = sSql & ",NULL,NULL,NULL"
    sSql = sSql & ", " & sValCustoUss

    Call Conexao_ExecutarSQL(sSiglaSistema, _
                             iAmbienteId, _
                             sSiglaRecurso, _
                             sDescricaoRecurso, _
                             sSql, _
                             lConexao, _
                             False)

    Exit Sub

TrataErro:

    Call Err.Raise(Err.Number, , "SEGL0999.AtualizarEscolhaPlano - " & Err.Description)

End Sub

Public Function ObterValorCustoUSS(ByVal sSiglaSistema As String, _
                                   ByVal sSiglaRecurso As String, _
                                   ByVal sDescricaoRecurso As String, _
                                   ByVal iAmbienteId As Integer, _
                                   ByVal lProdutoId As Long) As Object

    Dim sSql As String

    On Error GoTo TrataErro

    sSql = ""
    sSql = sSql & "SELECT ISNULL(val_custo_uss,0) as val_custo_uss " & vbNewLine
    sSql = sSql & "  FROM custo_tb  WITH (NOLOCK)  " & vbNewLine
    sSql = sSql & " WHERE produto_id = " & lProdutoId & vbNewLine
    sSql = sSql & "   AND dt_fim_vigencia IS NULL " & vbNewLine

    Set ObterValorCustoUSS = Conexao_ExecutarSQL(sSiglaSistema, _
                                                 iAmbienteId, _
                                                 sSiglaRecurso, _
                                                 sDescricaoRecurso, _
                                                 sSql, _
                                                 lConexao, _
                                                 True)

    Exit Function

TrataErro:
    Call Err.Raise(Err.Number, , "SEGL0999.ObterValorCustoUSS - " & Err.Description)

End Function

Public Function ObterVersaoArquivo(ByVal sSiglaSistema As String, _
                                   ByVal sSiglaRecurso As String, _
                                   ByVal sDescricaoRecurso As String, _
                                   ByVal iAmbienteId As Integer) As Object

    Dim sSql As String

    On Error GoTo TrataErro

    sSql = "SELECT num_vigente + 1 FROM chave_tb  WITH (NOLOCK)  WHERE chave = 'REAJUSTE'"

    Set ObterVersaoArquivo = ExecutarSQL(sSiglaSistema, _
                                         iAmbienteId, _
                                         sSiglaRecurso, _
                                         sDescricaoRecurso, _
                                         sSql, _
                                         True)

    Exit Function

TrataErro:
    Call Err.Raise(Err.Number, , "SEGL0999.ObterVersaoArquivo - " & Err.Description)

End Function

Public Sub AtualizarVersaoArquivo(ByVal sSiglaSistema As String, _
                                  ByVal sSiglaRecurso As String, _
                                  ByVal sDescricaoRecurso As String, _
                                  ByVal iAmbienteId As Integer, _
                                  ByVal sUsuario As String)

    Dim sSql As String

    On Error GoTo TrataErro

    sSql = "SET NOCOUNT ON EXEC chave_spu 'REAJUSTE','" & sUsuario & "'"

    Call Conexao_ExecutarSQL(sSiglaSistema, _
                             iAmbienteId, _
                             sSiglaRecurso, _
                             sDescricaoRecurso, _
                             sSql, _
                             lConexao, _
                             True)

    Exit Sub

TrataErro:
    Call Err.Raise(Err.Number, , "SEGL0999.AtualizarVersaoArquivo - " & Err.Description)

End Sub

Public Function ObterErroAceitavel(ByVal sSiglaSistema As String, _
                                   ByVal sSiglaRecurso As String, _
                                   ByVal sDescricaoRecurso As String, _
                                   ByVal iAmbienteId As Integer, _
                                   ByVal lProdutoId As Long) As Currency

    Dim sSql As String
    Dim rs As Recordset

    On Error GoTo TrataErro

    sSql = "" & vbNewLine
    sSql = sSql & " SELECT ISNULL(val_premio,0) AS val_premio " & vbNewLine
    sSql = sSql & "   FROM erro_aceitavel_pgto_tb  WITH (NOLOCK)  " & vbNewLine
    sSql = sSql & "  WHERE produto_id = " & lProdutoId

    Set rs = Conexao_ExecutarSQL(sSiglaSistema, _
                                 iAmbienteId, _
                                 sSiglaRecurso, _
                                 sDescricaoRecurso, _
                                 sSql, _
                                 lConexao, _
                                 True)

    If Not rs.EOF Then
        ObterErroAceitavel = CCur(rs("val_premio"))
    Else
        ObterErroAceitavel = 0
    End If

    Set rs = Nothing

    Exit Function

TrataErro:
    Call Err.Raise(Err.Number, , "SEGL0999.ObterErroAceitavel - " & Err.Description)

End Function

Public Function IncluirEndossoReajuste(ByVal sSiglaSistema As String, _
                                       ByVal sSiglaRecurso As String, _
                                       ByVal sDescricaoRecurso As String, _
                                       ByVal iAmbienteId As Integer, _
                                       ByVal lEndossoId As Long, _
                                       ByVal lPropostaBasicaId As Long, _
                                       ByVal lPropostaId As Long, _
                                       ByVal sPercReajuste As String, _
                                       ByVal lIndiceId As Long, _
                                       ByVal sValPremioAnterior As String, _
                                       ByVal sValPremioAtual As String, _
                                       ByVal sValISAnterior As String, _
                                       ByVal sValISAtual As String, _
                                       ByVal sValCustoUSSAnterior As String, _
                                       ByVal sValCustoUSSAtual As String, _
                                       ByVal sUsuario As String, _
                                       ByVal sDtPedidoEndosso As String) As Long

    Dim sSql As String
    Dim rs As Recordset

    On Error GoTo TrataErro

    sSql = "SET NOCOUNT ON "
    sSql = sSql & "EXEC endosso_reajuste_spi"
    sSql = sSql & "  " & lEndossoId
    sSql = sSql & ", " & lPropostaBasicaId
    sSql = sSql & ", " & lPropostaId
    sSql = sSql & ", " & sPercReajuste
    sSql = sSql & ", " & lIndiceId
    sSql = sSql & ", " & sValPremioAnterior
    sSql = sSql & ", " & sValPremioAtual
    sSql = sSql & ", " & sValISAnterior
    sSql = sSql & ", " & sValISAtual
    sSql = sSql & ", " & sValCustoUSSAnterior
    sSql = sSql & ", " & sValCustoUSSAtual
    sSql = sSql & ",'" & sUsuario & "'"
    sSql = sSql & ", " & ENDOSSO_REAJUSTE
    sSql = sSql & ",'" & Format(sDtPedidoEndosso, "yyyymmdd") & "'"

    Set rs = Conexao_ExecutarSQL(sSiglaSistema, _
                                 iAmbienteId, _
                                 sSiglaRecurso, _
                                 sDescricaoRecurso, _
                                 sSql, _
                                 lConexao, _
                                 True)

    If Not rs.EOF Then
        IncluirEndossoReajuste = Val(rs(0))
    Else
        IncluirEndossoReajuste = 0
    End If

    Set rs = Nothing

    Exit Function

TrataErro:

    Call Err.Raise(Err.Number, , "SEGL0999.IncluirEndossoReajuste - " & Err.Description)

End Function

Public Function IncluirEndossoReenquadramento(ByVal sSiglaSistema As String, _
                                              ByVal sSiglaRecurso As String, _
                                              ByVal sDescricaoRecurso As String, _
                                              ByVal iAmbienteId As Integer, _
                                              ByVal lEndossoId As Long, _
                                              ByVal lPropostaBasicaId As Long, _
                                              ByVal lPropostaId As Long, _
                                              ByVal lPlanoId As Long, _
                                              ByVal sDtInicioVigenciaPlano As String, _
                                              ByVal lProdutoId As Long, _
                                              ByVal sFatorRenovacao As String, _
                                              ByVal sValPremioAnterior As String, _
                                              ByVal sValPremioAtual As String, _
                                              ByVal sValISAnterior As String, _
                                              ByVal sValISAtual As String, _
                                              ByVal sValCustoUSSAnterior As String, _
                                              ByVal sValCustoUSSAtual As String, _
                                              ByVal sUsuario As String, _
                                              ByVal sDtPedidoEndosso As String) As Long

    Dim sSql As String
    Dim rs As Recordset

    On Error GoTo TrataErro

    sSql = "SET NOCOUNT ON "
    sSql = sSql & "EXEC endosso_reenquadramento_spi"
    sSql = sSql & "  " & lEndossoId
    sSql = sSql & ", " & lPropostaBasicaId
    sSql = sSql & ", " & lPropostaId
    sSql = sSql & ", " & lPlanoId
    sSql = sSql & ",'" & sDtInicioVigenciaPlano & "'"
    sSql = sSql & ", " & lProdutoId
    sSql = sSql & ", " & sFatorRenovacao
    sSql = sSql & ", " & sValPremioAnterior
    sSql = sSql & ", " & sValPremioAtual
    sSql = sSql & ", " & sValISAnterior
    sSql = sSql & ", " & sValISAtual
    sSql = sSql & ", " & sValCustoUSSAnterior
    sSql = sSql & ", " & sValCustoUSSAtual
    sSql = sSql & ",'" & sUsuario & "'"
    sSql = sSql & ", " & ENDOSSO_REENQUADRAMENTO
    sSql = sSql & ",'" & Format(sDtPedidoEndosso, "yyyymmdd") & "'"

    Set rs = Conexao_ExecutarSQL(sSiglaSistema, _
                                 iAmbienteId, _
                                 sSiglaRecurso, _
                                 sDescricaoRecurso, _
                                 sSql, _
                                 lConexao, _
                                 True)

    If Not rs.EOF Then
        IncluirEndossoReenquadramento = Val(rs(0))
    Else
        IncluirEndossoReenquadramento = 0
    End If

    Set rs = Nothing

    Exit Function

TrataErro:

    Call Err.Raise(Err.Number, , "SEGL0999.IncluirEndossoReenquadramento - " & Err.Description)

End Function

Public Sub IncluirReajusteDivergencia(ByVal sSiglaSistema As String, _
                                      ByVal sSiglaRecurso As String, _
                                      ByVal sDescricaoRecurso As String, _
                                      ByVal iAmbienteId As Integer, _
                                      ByVal lPropostaBB As Long, _
                                      ByVal sDtProcessamento As String, _
                                      ByVal lPropostaBasicaId As Long, _
                                      ByVal lPropostaId As Long, _
                                      ByVal lPlanoId As Long, _
                                      ByVal sDtInicioVigenciaPlano As String, _
                                      ByVal lProdutoId As Long, _
                                      ByVal sFatorRenovacao As String, _
                                      ByVal sPercReajuste As String, _
                                      ByVal lIndiceId As Long, _
                                      ByVal sValPremioAtual As String, _
                                      ByVal sValPremioArquivo As String, _
                                      ByVal sValPremioCalculado As String, _
                                      ByVal sValISAtual As String, _
                                      ByVal sValISArquivo As String, _
                                      ByVal sValISCalculado As String, _
                                      ByVal sValCustoUSSAtual As String, _
                                      ByVal sValCustoUSSArquivo As String, _
                                      ByVal sValCustoUSSCalculado As String, _
                                      ByVal sUsuario As String, _
                                      ByVal sTextoDivergencia As String)

    Dim sSql As String

    On Error GoTo TrataErro

    sSql = "SET NOCOUNT ON "
    sSql = sSql & "EXEC reajuste_divergencia_spi"
    sSql = sSql & "  " & lPropostaBB
    sSql = sSql & ",'" & Format(sDtProcessamento, "yyyymmdd") & "'"
    sSql = sSql & ", " & lPropostaBasicaId
    sSql = sSql & ", " & lPropostaId
    sSql = sSql & ", " & lPlanoId
    sSql = sSql & ",'" & sDtInicioVigenciaPlano & "'"
    sSql = sSql & ", " & lProdutoId
    sSql = sSql & ", " & sFatorRenovacao
    sSql = sSql & ", " & sPercReajuste
    sSql = sSql & ", " & lIndiceId
    sSql = sSql & ", " & sValPremioAtual
    sSql = sSql & ", " & sValPremioArquivo
    sSql = sSql & ", " & sValPremioCalculado
    sSql = sSql & ", " & sValISAtual
    sSql = sSql & ", " & sValISArquivo
    sSql = sSql & ", " & sValISCalculado
    sSql = sSql & ", " & sValCustoUSSAtual
    sSql = sSql & ", " & sValCustoUSSArquivo
    sSql = sSql & ", " & sValCustoUSSCalculado
    sSql = sSql & ",'" & sTextoDivergencia & "'"
    sSql = sSql & ",'" & sUsuario & "'"

    Call Conexao_ExecutarSQL(sSiglaSistema, _
                             iAmbienteId, _
                             sSiglaRecurso, _
                             sDescricaoRecurso, _
                             sSql, _
                             lConexao, _
                             False)

    Exit Sub

TrataErro:

    Call Err.Raise(Err.Number, , "SEGL0999.IncluirReajusteDivergencia - " & Err.Description)

End Sub

