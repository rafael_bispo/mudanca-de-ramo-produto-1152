VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cls00127"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Public Function GerarCertificado(ByVal sSiglaSistema As String, _
                                 ByVal sSiglaRecurso As String, _
                                 ByVal sDescricaoRecurso As String, _
                                 ByVal lAmbienteID As Long, _
                                 ByVal lGestorId As Long, _
                                 ByVal sUsuario As String)


    Dim sSql As String

    On Error GoTo TrataErro

    sSql = "EXEC certificado_pre_impresso_spi " & lGestorId & ","
    sSql = sSql & "'n',"
    sSql = sSql & "'" & sUsuario & "'"


    Call ExecutarSQL(sSiglaSistema, _
                     lAmbienteID, _
                     sSiglaRecurso, _
                     sDescricaoRecurso, _
                     sSql, _
                     False)

    Exit Function


TrataErro:

    Call Err.Raise(Err.Number, , "SEGL0026.cls00127. GerarCertificado - " & Err.Description)
End Function


