VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cls00148"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Classe de atributo de Agendamento (Agendamento.cls)
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''

'local variable(s) to hold property value(s)
Private mvarNumCobranca As Integer    'local copy
Private mvarNumCartao As Integer    'local copy
Private mvarDtValidadeCartao As String    'local copy
Private mvarBancoId As Long    'local copy
Private mvarAgenciaId As Long    'local copy
Private mvarContaCorrente As Long    'local copy
Private mvarDtAgendamento As String    'local copy
Private mvarValParcela As Currency    'local copy
Private mvarQtdParcelas As Integer    'local copy
Private mvarNossoNumero As String    'local copy
Private mvarNossoNumeroDV As String    'local copy
Private mvarSituacao As String    'local copy
Private mvarOrigemBaixa As String    'local copy
Private mvarValIOF As Currency    'local copy
Private mvarValComissao As Currency    'local copy
Private mvarValIRComissaoCorretor As Currency    'local copy
Private mvarValComissaoEstipulante As Currency    'local copy
Private mvarValJuros As Currency    'local copy
Private mvarNumParcelaEndosso As Integer    'local copy
Private mvarValDespesaLider As Currency    'local copy
Private mvarFaturaId As Long    'local copy
Private mvarValDesconto As Currency    'local copy
Private mvarAnoMes As String    'local copy
Private mvarDtContabilizacao As String    'local copy
Private mvarValPago As Currency    'local copy
Private mvarValIOFBB As Currency    'local copy
Private mvarValIRBB As Currency    'local copy
Private mvarValComissaoBB As Currency    'local copy
Private mvarValComissaoEstipulanteBB As Currency    'local copy
Private mvarValRepasse As Currency    'local copy
Private mvarDtBaixa As String    'local copy
Private mvarArquivoBaixa As String    'local copy
Private mvarDtRecebimento As String    'local copy
Private mvarObservacao As String    'local copy
Private mvarValRemuneracao As Currency    'local copy
Public Property Let ValRemuneracao(ByVal vData As Currency)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.ValRemuneracao = 5
    mvarValRemuneracao = vData
End Property


Public Property Get ValRemuneracao() As Currency
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.ValRemuneracao
    ValRemuneracao = mvarValRemuneracao
End Property



Public Property Let Observacao(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Observacao = 5
    mvarObservacao = vData
End Property


Public Property Get Observacao() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Observacao
    Observacao = mvarObservacao
End Property



Public Property Let DtRecebimento(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.DtRecebimento = 5
    mvarDtRecebimento = vData
End Property


Public Property Get DtRecebimento() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.DtRecebimento
    DtRecebimento = mvarDtRecebimento
End Property



Public Property Let ArquivoBaixa(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.ArquivoBaixa = 5
    mvarArquivoBaixa = vData
End Property


Public Property Get ArquivoBaixa() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.ArquivoBaixa
    ArquivoBaixa = mvarArquivoBaixa
End Property



Public Property Let DtBaixa(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.DtBaixa = 5
    mvarDtBaixa = vData
End Property


Public Property Get DtBaixa() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.DtBaixa
    DtBaixa = mvarDtBaixa
End Property



Public Property Let ValRepasse(ByVal vData As Currency)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.ValRepasse = 5
    mvarValRepasse = vData
End Property


Public Property Get ValRepasse() As Currency
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.ValRepasse
    ValRepasse = mvarValRepasse
End Property



Public Property Let ValComissaoEstipulanteBB(ByVal vData As Currency)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.ValComissaoEstipulanteBB = 5
    mvarValComissaoEstipulanteBB = vData
End Property


Public Property Get ValComissaoEstipulanteBB() As Currency
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.ValComissaoEstipulanteBB
    ValComissaoEstipulanteBB = mvarValComissaoEstipulanteBB
End Property



Public Property Let ValComissaoBB(ByVal vData As Currency)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.ValComissaoBB = 5
    mvarValComissaoBB = vData
End Property


Public Property Get ValComissaoBB() As Currency
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.ValComissaoBB
    ValComissaoBB = mvarValComissaoBB
End Property



Public Property Let ValIRBB(ByVal vData As Currency)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.ValIRBB = 5
    mvarValIRBB = vData
End Property


Public Property Get ValIRBB() As Currency
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.ValIRBB
    ValIRBB = mvarValIRBB
End Property



Public Property Let ValIOFBB(ByVal vData As Currency)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.ValIOFBB = 5
    mvarValIOFBB = vData
End Property


Public Property Get ValIOFBB() As Currency
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.ValIOFBB
    ValIOFBB = mvarValIOFBB
End Property



Public Property Let ValPago(ByVal vData As Currency)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.ValPago = 5
    mvarValPago = vData
End Property


Public Property Get ValPago() As Currency
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.ValPago
    ValPago = mvarValPago
End Property



Public Property Let DtContabilizacao(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.DtContabilizacao = 5
    mvarDtContabilizacao = vData
End Property


Public Property Get DtContabilizacao() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.DtContabilizacao
    DtContabilizacao = mvarDtContabilizacao
End Property



Public Property Let AnoMes(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.AnoMes = 5
    mvarAnoMes = vData
End Property


Public Property Get AnoMes() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.AnoMes
    AnoMes = mvarAnoMes
End Property



Public Property Let ValDesconto(ByVal vData As Currency)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.ValDesconto = 5
    mvarValDesconto = vData
End Property


Public Property Get ValDesconto() As Currency
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.ValDesconto
    ValDesconto = mvarValDesconto
End Property



Public Property Let FaturaId(ByVal vData As Long)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.FaturaId = 5
    mvarFaturaId = vData
End Property


Public Property Get FaturaId() As Long
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.FaturaId
    FaturaId = mvarFaturaId
End Property



Public Property Let ValDespesaLider(ByVal vData As Currency)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.ValDespesaLider = 5
    mvarValDespesaLider = vData
End Property


Public Property Get ValDespesaLider() As Currency
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.ValDespesaLider
    ValDespesaLider = mvarValDespesaLider
End Property



Public Property Let NumParcelaEndosso(ByVal vData As Integer)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.NumParcelaEndosso = 5
    mvarNumParcelaEndosso = vData
End Property


Public Property Get NumParcelaEndosso() As Integer
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.NumParcelaEndosso
    NumParcelaEndosso = mvarNumParcelaEndosso
End Property



Public Property Let ValJuros(ByVal vData As Currency)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.ValJuros = 5
    mvarValJuros = vData
End Property


Public Property Get ValJuros() As Currency
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.ValJuros
    ValJuros = mvarValJuros
End Property



Public Property Let ValComissaoEstipulante(ByVal vData As Currency)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.ValComissaoEstipulante = 5
    mvarValComissaoEstipulante = vData
End Property


Public Property Get ValComissaoEstipulante() As Currency
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.ValComissaoEstipulante
    ValComissaoEstipulante = mvarValComissaoEstipulante
End Property



Public Property Let ValIRComissaoCorretor(ByVal vData As Currency)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.ValIRComissaoCorretor = 5
    mvarValIRComissaoCorretor = vData
End Property


Public Property Get ValIRComissaoCorretor() As Currency
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.ValIRComissaoCorretor
    ValIRComissaoCorretor = mvarValIRComissaoCorretor
End Property



Public Property Let ValComissao(ByVal vData As Currency)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.ValComissao = 5
    mvarValComissao = vData
End Property


Public Property Get ValComissao() As Currency
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.ValComissao
    ValComissao = mvarValComissao
End Property



Public Property Let ValIOF(ByVal vData As Currency)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.ValIOF = 5
    mvarValIOF = vData
End Property


Public Property Get ValIOF() As Currency
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.ValIOF
    ValIOF = mvarValIOF
End Property



Public Property Let OrigemBaixa(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.OrigemBaixa = 5
    mvarOrigemBaixa = vData
End Property


Public Property Get OrigemBaixa() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.OrigemBaixa
    OrigemBaixa = mvarOrigemBaixa
End Property



Public Property Let Situacao(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Situacao = 5
    mvarSituacao = vData
End Property


Public Property Get Situacao() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Situacao
    Situacao = mvarSituacao
End Property



Public Property Let NossoNumeroDV(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.NossoNumeroDV = 5
    mvarNossoNumeroDV = vData
End Property


Public Property Get NossoNumeroDV() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.NossoNumeroDV
    NossoNumeroDV = mvarNossoNumeroDV
End Property



Public Property Let NossoNumero(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.NossoNumero = 5
    mvarNossoNumero = vData
End Property


Public Property Get NossoNumero() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.NossoNumero
    NossoNumero = mvarNossoNumero
End Property



Public Property Let QtdParcelas(ByVal vData As Integer)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.QtdParcelas = 5
    mvarQtdParcelas = vData
End Property


Public Property Get QtdParcelas() As Integer
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.QtdParcelas
    QtdParcelas = mvarQtdParcelas
End Property



Public Property Let ValParcela(ByVal vData As Currency)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.ValParcela = 5
    mvarValParcela = vData
End Property


Public Property Get ValParcela() As Currency
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.ValParcela
    ValParcela = mvarValParcela
End Property



Public Property Let DtAgendamento(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.DtAgendamento = 5
    mvarDtAgendamento = vData
End Property


Public Property Get DtAgendamento() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.DtAgendamento
    DtAgendamento = mvarDtAgendamento
End Property



Public Property Let ContaCorrente(ByVal vData As Long)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.ContaCorrente = 5
    mvarContaCorrente = vData
End Property


Public Property Get ContaCorrente() As Long
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.ContaCorrente
    ContaCorrente = mvarContaCorrente
End Property



Public Property Let AgenciaId(ByVal vData As Long)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.AgenciaId = 5
    mvarAgenciaId = vData
End Property


Public Property Get AgenciaId() As Long
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.AgenciaId
    AgenciaId = mvarAgenciaId
End Property



Public Property Let BancoId(ByVal vData As Long)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.BancoId = 5
    mvarBancoId = vData
End Property


Public Property Get BancoId() As Long
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.BancoId
    BancoId = mvarBancoId
End Property



Public Property Let DtValidadeCartao(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.DtValidadeCartao = 5
    mvarDtValidadeCartao = vData
End Property


Public Property Get DtValidadeCartao() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.DtValidadeCartao
    DtValidadeCartao = mvarDtValidadeCartao
End Property



Public Property Let NumCartao(ByVal vData As Integer)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.NumCartao = 5
    mvarNumCartao = vData
End Property


Public Property Get NumCartao() As Integer
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.NumCartao
    NumCartao = mvarNumCartao
End Property



Public Property Let NumCobranca(ByVal vData As Integer)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.NumCobranca = 5
    mvarNumCobranca = vData
End Property


Public Property Get NumCobranca() As Integer
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.NumCobranca
    NumCobranca = mvarNumCobranca
End Property



