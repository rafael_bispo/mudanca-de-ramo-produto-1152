VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cls00999"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit

'local variable(s) to hold property value(s)
Private mvarlBeneficiarioId As Long    'local copy
Private mvarsNome As String    'local copy
Private mvarsEndereco As String    'local copy
Private mvarsBairro As String    'local copy
Private mvarsCidade As String    'local copy
Private mvarsEstado As String    'local copy
Private mvarsCep As String    'local copy
Private mvarsPais As String    'local copy
'local variable(s) to hold property value(s)
Private mvarsOperacao As String    'local copy
Public Property Let sOperacao(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.sOperacao = 5
    mvarsOperacao = vData
End Property


Public Property Get sOperacao() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.sOperacao
    sOperacao = mvarsOperacao
End Property



Public Property Let sPais(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.sPais = 5
    mvarsPais = vData
End Property


Public Property Get sPais() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.sPais
    sPais = mvarsPais
End Property



Public Property Let sCep(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.sCep = 5
    mvarsCep = vData
End Property


Public Property Get sCep() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.sCep
    sCep = mvarsCep
End Property



Public Property Let sEstado(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.sEstado = 5
    mvarsEstado = vData
End Property


Public Property Get sEstado() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.sEstado
    sEstado = mvarsEstado
End Property



Public Property Let sCidade(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.sCidade = 5
    mvarsCidade = vData
End Property


Public Property Get sCidade() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.sCidade
    sCidade = mvarsCidade
End Property



Public Property Let sBairro(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.sBairro = 5
    mvarsBairro = vData
End Property


Public Property Get sBairro() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.sBairro
    sBairro = mvarsBairro
End Property



Public Property Let sEndereco(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.sEndereco = 5
    mvarsEndereco = vData
End Property


Public Property Get sEndereco() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.sEndereco
    sEndereco = mvarsEndereco
End Property



Public Property Let sNome(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.sNome = 5
    mvarsNome = vData
End Property


Public Property Get sNome() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.sNome
    sNome = mvarsNome
End Property



Public Property Let lBeneficiarioId(ByVal vData As Long)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.lBeneficiarioId = 5
    mvarlBeneficiarioId = vData
End Property


Public Property Get lBeneficiarioId() As Long
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.lBeneficiarioId
    lBeneficiarioId = mvarlBeneficiarioId
End Property



