Attribute VB_Name = "Local"
Public Declare Function GetPrivateProfileString Lib "kernel32" Alias "GetPrivateProfileStringA" (ByVal lpApplicationName As String, ByVal lpKeyName As Any, ByVal lpDefault As String, ByVal lpReturnedString As String, ByVal nSize As Long, ByVal lpFileName As String) As Long
Function LeArquivoIni(ByVal FileName As String, ByVal SectionName As String, ByVal Item As String) As String

    Dim Retorno As String * 100, RetornoDefault, nc

    RetornoDefault = "*"
    nc = GetPrivateProfileString(SectionName, Item, RetornoDefault, Retorno, Len(Retorno), FileName)
    LeArquivoIni = Left$(Retorno, nc)

End Function

Function MudaVirgulaParaPonto(ByVal valor As String) As String

    If InStr(valor, ",") = 0 Then
        MudaVirgulaParaPonto = valor
    Else
        valor = Mid$(valor, 1, InStr(valor, ",") - 1) + "." + Mid$(valor, InStr(valor, ",") + 1)
        MudaVirgulaParaPonto = valor
    End If

End Function

Function Trunca(valor) As Currency

    Dim SinalDecimal As String
    SinalDecimal = LeArquivoIni("WIN.INI", "intl", "sDecimal")

    If SinalDecimal = "." Then
        ConfiguracaoBrasil = False
    Else
        ConfiguracaoBrasil = True
    End If

    If ConfiguracaoBrasil Then
        pos = InStr(valor, ",")
    Else
        pos = InStr(valor, ".")
    End If

    If pos <> 0 Then
        valor = Mid(valor, 1, pos + 2)
    End If

    Trunca = valor

End Function

Function RetiraQualquerCaracter(sEntrada As String) As String

'***********************************************************
'** Fun��o que retira qualquer caracter n�o num�rico      **
'** Respons�vel: Paulo Carvalho                           **
'** Data Desenvolvimento: 22/11/2002                      **
'***********************************************************

    Dim iCont As Integer

    For iCont = 1 To Len(sEntrada)
        If IsNumeric(Mid(sEntrada, iCont, 1)) Then
            RetiraQualquerCaracter = RetiraQualquerCaracter & Mid(sEntrada, iCont, 1)
        End If
    Next

End Function

