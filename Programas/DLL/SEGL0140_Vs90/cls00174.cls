VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cls00174"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "SavedWithClassBuilder" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
'local variable(s) to hold property value(s)
Private mvarId As String    'local copy
Private mvarNome As String    'local copy
Private mvarPercProLabore As Double    'local copy
Private mvarIdEstipulante As String    'local copy
Private mvarDtIniAdm As Date    'local copy
Private mvarDtIniRep As Date    'local copy
Private mvarDtFimAdm As Date    'local copy
Private mvarOperacao As String    'local copy
Private mvarPersistente As Boolean    'local copy

Public Property Let Persistente(ByVal vData As Boolean)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Persistente = 5
    mvarPersistente = vData
End Property

Public Property Get Persistente() As Boolean
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Persistente
    Persistente = mvarPersistente
End Property

Public Property Let Operacao(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Operacao = 5
    mvarOperacao = vData
End Property

Public Property Get Operacao() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Operacao
    Operacao = mvarOperacao
End Property

Public Property Let DtFimAdm(ByVal vData As Date)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.DtFimAdm = 5
    mvarDtFimAdm = vData
End Property

Public Property Get DtFimAdm() As Date
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.DtFimAdm
    DtFimAdm = mvarDtFimAdm
End Property

Public Property Let DtIniRep(ByVal vData As Date)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.DtIniRep = 5
    mvarDtIniRep = vData
End Property

Public Property Get DtIniRep() As Date
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.DtIniRep
    DtIniRep = mvarDtIniRep
End Property

Public Property Let DtIniAdm(ByVal vData As Date)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.DtIniAdm = 5
    mvarDtIniAdm = vData
End Property

Public Property Get DtIniAdm() As Date
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.DtIniAdm
    DtIniAdm = mvarDtIniAdm
End Property

Public Property Let IdEstipulante(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.IdEstipulante = 5
    mvarIdEstipulante = vData
End Property

Public Property Get IdEstipulante() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.IdEstipulante
    IdEstipulante = mvarIdEstipulante
End Property

Public Property Let PercProLabore(ByVal vData As Double)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.PercProLabore = 5
    mvarPercProLabore = vData
End Property

Public Property Get PercProLabore() As Double
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.PercProLabore
    PercProLabore = mvarPercProLabore
End Property

Public Property Let Nome(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Nome = 5
    mvarNome = vData
End Property

Public Property Get Nome() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Nome
    Nome = mvarNome
End Property

Public Property Let Id(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Id = 5
    mvarId = vData
End Property

Public Property Get Id() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Id
    Id = mvarId
End Property



