VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cls00175"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "SavedWithClassBuilder" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Attribute VB_Ext_KEY = "Member0" ,"CartaoProposta"
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Member1" ,"Estipulante"
Attribute VB_Ext_KEY = "Member2" ,"LimiteIdade"
Option Explicit

'local variable(s) to hold property value(s)
Private mvarId As Integer    'local copy
Private mvarNome As String    'local copy
Private mvarEstipulantes As Collection    'local copy
'local variable(s) to hold property value(s)
Private mvarCorretores As Collection    'local copy
Public Property Set Corretores(ByVal vData As Collection)
'used when assigning an Object to the property, on the left side of a Set statement.
'Syntax: Set x.Corretores = Form1
    Set mvarCorretores = vData
End Property


Public Property Get Corretores() As Collection
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Corretores
    Set Corretores = mvarCorretores
End Property




Public Property Set Estipulantes(ByVal vData As Object)
'used when assigning an Object to the property, on the left side of a Set statement.
'Syntax: Set x.Estipulantes = Form1
    Set mvarEstipulantes = vData
End Property

Public Property Get Estipulantes() As Collection
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Estipulantes
    Set Estipulantes = mvarEstipulantes
End Property

Public Property Let Nome(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Nome = 5
    mvarNome = vData
End Property

Public Property Get Nome() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Nome
    Nome = mvarNome
End Property

Public Property Let Id(ByVal vData As Integer)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Id = 5
    mvarId = vData
End Property

Public Property Get Id() As Integer
Attribute Id.VB_UserMemId = 0
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Id
    Id = mvarId
End Property



