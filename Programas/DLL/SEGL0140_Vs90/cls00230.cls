VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cls00230"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit

Public Function BuscarLimiteEmissaoAtual(ByVal sUsuario As String, _
                                         ByVal sSiglaSistema As String, _
                                         ByVal sSiglaRecurso As String, _
                                         ByVal sDescricaoRecurso As String, _
                                         ByVal lAmbienteId As Long, _
                                         ByVal lPropostaId As Long) As Object

    Dim sSql As String

    On Error GoTo TrataErro

    sSql = "" & vbNewLine
    sSql = sSql & "SELECT val_lim_is_apolice, " & vbNewLine
    sSql = sSql & "       dt_inicio_contratacao," & vbNewLine
    sSql = sSql & "       dt_fim_contratacao" & vbNewLine
    sSql = sSql & "  FROM produto_proposta_basica_tb " & vbNewLine
    sSql = sSql & " WHERE proposta_id = " & lPropostaId & vbNewLine
    sSql = sSql & "   AND dt_inicio_contratacao = (SELECT MAX(dt_inicio_contratacao)"
    sSql = sSql & "                                  FROM produto_proposta_basica_tb"
    sSql = sSql & "                                 WHERE Proposta_Id = " & lPropostaId & ")"

    Set BuscarLimiteEmissaoAtual = ExecutarSQL(sSiglaSistema, _
                                               lAmbienteId, _
                                               sSiglaRecurso, _
                                               sDescricaoRecurso, _
                                               sSql, _
                                               True)

    Exit Function

TrataErro:

    Call Err.Raise(Err.Number, , "SEGL0140.cls00230.BuscarLimiteEmissaoAtual - " & Err.Description)

End Function


Private Sub AtualizarLimiteEmissao(ByVal sSiglaSistema As String, _
                                   ByVal sSiglaRecurso As String, _
                                   ByVal sDescricaoRecurso As String, _
                                   ByVal lAmbienteId As Long, _
                                   ByVal lConexao As Long, _
                                   ByVal lPropostaId As Long, _
                                   ByVal iProdutoId As Integer, _
                                   ByVal sDtInicioContratacao As String, _
                                   ByVal cValLimIsApolice As Currency, _
                                   ByVal sUsuario As String, _
                                   ByVal sDtNovoInicioContratacao As String, _
                                   ByVal iEndossoId As Integer)

    Dim sSql As String

    'On Error GoTo TrataErro

    sSql = ""
    sSql = sSql & "SET NOCOUNT ON" & vbNewLine
    sSql = sSql & "EXEC produto_proposta_basica_spu "
    sSql = sSql & lPropostaId
    sSql = sSql & ", " & iProdutoId
    sSql = sSql & ", '" & Format(sDtInicioContratacao, "yyyymmdd") & "'"
    sSql = sSql & ", " & MudaVirgulaParaPonto(cValLimIsApolice)
    sSql = sSql & ", '" & sUsuario & "'"
    sSql = sSql & ", '" & Format(sDtNovoInicioContratacao, "yyyymmdd") & "'"
    sSql = sSql & ", " & iEndossoId

    Call Conexao_ExecutarSQL(sSiglaSistema, _
                             lAmbienteId, _
                             sSiglaRecurso, _
                             sDescricaoRecurso, _
                             sSql, _
                             lConexao, _
                             False)

End Sub

Public Sub GerarEndossoLimiteEmissao(ByVal sSiglaSistema As String, _
                                     ByVal sSiglaRecurso As String, _
                                     ByVal sDescricaoRecurso As String, _
                                     ByVal lAmbienteId As Long, _
                                     ByVal lPropostaId As Long, _
                                     ByVal iProdutoId As Integer, _
                                     ByVal lTpEndossoId As Long, _
                                     ByVal sDtPedidoEndosso As String, _
                                     ByVal sDtEmissao As String, _
                                     ByVal sTextoEndosso As String, _
                                     ByVal sDtAtualLimite As String, _
                                     ByVal cLimiteNovo As Currency, _
                                     ByVal sDtNovoLimite As String, _
                                     ByVal sDataSistema As String, _
                                     ByVal sUsuario As String)

    Dim lConexao As Long
    Dim lEndossoId As Long
    Dim oEndosso As Object

    Dim colVazio As New Collection

    On Error GoTo TrataErro

    ' Criando conex�o '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

    lConexao = AbrirConexao(sSiglaSistema, _
                            lAmbienteId, _
                            sSiglaRecurso, _
                            sDescricaoRecurso)

    ' Abrindo transa��o '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

    If Not AbrirTransacao(lConexao) Then
        Call Err.Raise(vbObjectError + 1015, , "SEGL0140.cls00230.GerarEndossoLimiteEmissao - A Transa��o n�o p�de ser aberta")
        Exit Sub
    End If

    ' Emitindo endosso ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

    Set oEndosso = CreateObject("SEGL0140.cls00172")

    lEndossoId = oEndosso.EmitirEndosso(sUsuario, _
                                        sSiglaSistema, sSiglaRecurso, _
                                        sDescricaoRecurso, lAmbienteId, _
                                        0, 0, _
                                        0, lPropostaId, _
                                        0, 0, _
                                        lTpEndossoId, "N", _
                                        0, 0, _
                                        1, 0, _
                                        "", "", _
                                        "", "", _
                                        0, _
                                        0, 0, _
                                        0, 0, _
                                        0, 0, _
                                        0, "", _
                                        0, 0, _
                                        0, 0, _
                                        sDtPedidoEndosso, sDtEmissao, _
                                        0, 0, 0, _
                                        0, 0, _
                                        0, colVazio, _
                                        colVazio, 0, lConexao, , _
                                        "", sTextoEndosso)

    Set oEndosso = Nothing


    ' Atualizando o Limite de Emiss�o '''''''''''''''''''''''''''''''''''''''''''''''''''''

    Call AtualizarLimiteEmissao(sSiglaSistema, _
                                sSiglaRecurso, _
                                sDescricaoRecurso, _
                                lAmbienteId, _
                                lConexao, _
                                lPropostaId, _
                                iProdutoId, _
                                sDtAtualLimite, _
                                cLimiteNovo, _
                                sUsuario, _
                                sDtNovoLimite, _
                                lEndossoId)

    ' Confirmando transa��o '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

    If Not ConfirmarTransacao(lConexao) Then
        Call Err.Raise(vbObjectError + 1321, , "SEGL0140.cls00230.GerarEndossoLimiteEmissao - N�o foi poss�vel confirmar a transa��o")
    End If

    ' Fechando conex�o ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

    If Not FecharConexao(lConexao) Then
        Call Err.Raise(vbObjectError + 1322, , "SEGL0140.cls00230.GerarEndossoLimiteEmissao - N�o foi poss�vel fechar conex�o")
    End If

    Exit Sub

TrataErro:

    Call Err.Raise(Err.Number, , "SEGL0140.cls00230.GerarEndossoLimiteEmissao - " & Err.Description)

End Sub

