VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cls00133"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
Public Function SelecionarVoucher(ByVal sSiglaSistema As String, _
                                  ByVal sSiglaRecurso As String, _
                                  ByVal sDescricaoRecurso As String, _
                                  ByVal iAmbienteId As Integer, _
                                  Optional ByVal iTipoPesquisa As Integer, _
                                  Optional ByVal sConteudo As String, _
                                  Optional ByVal sOrigem As String) As Object

'

Dim sSql As String

On Error GoTo Trata_Erro

    sSql = ""
    sSql = "SELECT DISTINCT ps_voucher_tb.voucher_id, " & vbNewLine
    sSql = sSql & "       cliente_tb.nome, " & vbNewLine
    sSql = sSql & "       ps_voucher_distribuicao_tb.val_voucher, " & vbNewLine
    sSql = sSql & "       ps_voucher_distribuicao_tb.sucursal_seguradora_id, " & vbNewLine
    sSql = sSql & "       proposta_tb.produto_id, " & vbNewLine
    sSql = sSql & "       proposta_tb.ramo_id " & vbNewLine
    sSql = sSql & "FROM ps_voucher_tb " & vbNewLine
    sSql = sSql & " JOIN ps_acerto_pagamento_tb WITH (NOLOCK) " & vbNewLine
    sSql = sSql & "   ON ps_acerto_pagamento_tb.voucher_id = ps_voucher_tb.voucher_id " & vbNewLine
    sSql = sSql & "  AND ps_acerto_pagamento_tb.cod_origem = ps_voucher_tb.cod_origem " & vbNewLine
    sSql = sSql & " JOIN ps_acerto_cliente_tb WITH (NOLOCK) " & vbNewLine
    sSql = sSql & "   ON ps_acerto_pagamento_tb.acerto_id = ps_acerto_cliente_tb.acerto_id " & vbNewLine
    sSql = sSql & " JOIN ps_acerto_tb WITH (NOLOCK) " & vbNewLine
    sSql = sSql & "   ON ps_acerto_tb.acerto_id = ps_acerto_pagamento_tb.acerto_id " & vbNewLine
    sSql = sSql & " JOIN ps_mov_cliente_tb WITH (NOLOCK) " & vbNewLine
    sSql = sSql & "   ON ps_mov_cliente_tb.acerto_id = ps_acerto_tb.acerto_id " & vbNewLine
    sSql = sSql & " JOIN ps_movimentacao_tb WITH (NOLOCK) " & vbNewLine
    sSql = sSql & "   ON ps_mov_cliente_tb.movimentacao_id = ps_movimentacao_tb.movimentacao_id " & vbNewLine
    sSql = sSql & " JOIN ps_mov_endosso_financeiro_tb WITH (NOLOCK) " & vbNewLine
    sSql = sSql & "   ON ps_mov_endosso_financeiro_tb.movimentacao_id = ps_movimentacao_tb.movimentacao_id " & vbNewLine
    sSql = sSql & " JOIN proposta_tb WITH (NOLOCK) " & vbNewLine
    sSql = sSql & "   ON proposta_tb.proposta_id = ps_mov_endosso_financeiro_tb.proposta_id " & vbNewLine
    sSql = sSql & " JOIN proposta_fechada_tb WITH (NOLOCK) " & vbNewLine
    sSql = sSql & "   ON proposta_fechada_tb.proposta_id = proposta_tb.proposta_id " & vbNewLine
    sSql = sSql & " JOIN cliente_tb WITH (NOLOCK) " & vbNewLine
    sSql = sSql & "   ON cliente_tb.cliente_id = proposta_tb.prop_cliente_id " & vbNewLine
    sSql = sSql & " JOIN ps_voucher_distribuicao_tb WITH (NOLOCK) " & vbNewLine
    sSql = sSql & "   ON ps_voucher_distribuicao_tb.voucher_id = ps_voucher_tb.voucher_id " & vbNewLine
    sSql = sSql & "  AND ps_voucher_distribuicao_tb.cod_origem = ps_voucher_tb.cod_origem " & vbNewLine
    
    'Verificando o c�digo de origem
    If sOrigem <> "" Then
        sSql = sSql & "WHERE ps_voucher_tb.cod_origem = '" & sOrigem & "'" & vbNewLine
    Else
        sSql = sSql & "WHERE ps_voucher_tb.cod_origem = ps_voucher_tb.cod_origem" & vbNewLine
    End If

    Select Case iTipoPesquisa
  
      Case 1 'Proposta
        sSql = sSql & "AND ps_mov_endosso_financeiro_tb.proposta_id = " & sConteudo & vbNewLine
      
      Case 2 'Proposta BB
        sSql = sSql & "AND ISNULL(proposta_fechada_tb.proposta_bb,0) = " & sConteudo & vbNewLine
        
      Case 3 'Voucher
        sSql = sSql & "AND ps_voucher_tb.voucher_id = " & sConteudo & vbNewLine
        
      Case 4 'CPF ou CNPJ
        sSql = sSql & "AND cliente_tb.cpf_cnpj = '" & sConteudo & "'" & vbNewLine
    
    End Select
        
    sSql = sSql & "UNION " & vbNewLine
    
    sSql = sSql & "SELECT DISTINCT ps_voucher_tb.voucher_id, " & vbNewLine
    sSql = sSql & "       cliente_tb.nome, " & vbNewLine
    sSql = sSql & "       ps_voucher_distribuicao_tb.val_voucher, " & vbNewLine
    sSql = sSql & "       ps_voucher_distribuicao_tb.sucursal_seguradora_id, " & vbNewLine
    sSql = sSql & "       proposta_tb.produto_id, " & vbNewLine
    sSql = sSql & "       proposta_tb.ramo_id " & vbNewLine
    sSql = sSql & "FROM ps_voucher_tb " & vbNewLine
    sSql = sSql & " JOIN ps_acerto_pagamento_tb WITH (NOLOCK) " & vbNewLine
    sSql = sSql & "   ON ps_acerto_pagamento_tb.voucher_id = ps_voucher_tb.voucher_id " & vbNewLine
    sSql = sSql & "  AND ps_acerto_pagamento_tb.cod_origem = ps_voucher_tb.cod_origem " & vbNewLine
    sSql = sSql & " JOIN ps_acerto_cliente_tb WITH (NOLOCK) " & vbNewLine
    sSql = sSql & "   ON ps_acerto_pagamento_tb.acerto_id = ps_acerto_cliente_tb.acerto_id " & vbNewLine
    sSql = sSql & " JOIN ps_acerto_tb WITH (NOLOCK) " & vbNewLine
    sSql = sSql & "   ON ps_acerto_tb.acerto_id = ps_acerto_pagamento_tb.acerto_id " & vbNewLine
    sSql = sSql & " JOIN ps_mov_cliente_tb WITH (NOLOCK) " & vbNewLine
    sSql = sSql & "   ON ps_mov_cliente_tb.acerto_id = ps_acerto_tb.acerto_id " & vbNewLine
    sSql = sSql & " JOIN ps_movimentacao_tb WITH (NOLOCK) " & vbNewLine
    sSql = sSql & "   ON ps_mov_cliente_tb.movimentacao_id = ps_movimentacao_tb.movimentacao_id " & vbNewLine
    sSql = sSql & " JOIN ps_mov_endosso_financeiro_tb WITH (NOLOCK) " & vbNewLine
    sSql = sSql & "   ON ps_mov_endosso_financeiro_tb.movimentacao_id = ps_movimentacao_tb.movimentacao_id " & vbNewLine
    sSql = sSql & " JOIN proposta_tb WITH (NOLOCK) " & vbNewLine
    sSql = sSql & "   ON proposta_tb.proposta_id = ps_mov_endosso_financeiro_tb.proposta_id " & vbNewLine
    sSql = sSql & " JOIN proposta_adesao_tb " & vbNewLine
    sSql = sSql & "   ON proposta_adesao_tb.proposta_id = proposta_tb.proposta_id " & vbNewLine
    sSql = sSql & " JOIN cliente_tb WITH (NOLOCK) " & vbNewLine
    sSql = sSql & "   ON cliente_tb.cliente_id = proposta_tb.prop_cliente_id " & vbNewLine
    sSql = sSql & " JOIN ps_voucher_distribuicao_tb WITH (NOLOCK) " & vbNewLine
    sSql = sSql & "   ON ps_voucher_distribuicao_tb.voucher_id = ps_voucher_tb.voucher_id " & vbNewLine
    sSql = sSql & "  AND ps_voucher_distribuicao_tb.cod_origem = ps_voucher_tb.cod_origem " & vbNewLine
    
        'Verificando o c�digo de origem
    If sOrigem <> "" Then
        sSql = sSql & "WHERE ps_voucher_tb.cod_origem = '" & sOrigem & "'" & vbNewLine
    Else
        sSql = sSql & "WHERE ps_voucher_tb.cod_origem = ps_voucher_tb.cod_origem" & vbNewLine
    End If

    Select Case iTipoPesquisa
  
      Case 1 'Proposta
        sSql = sSql & "AND ps_mov_endosso_financeiro_tb.proposta_id = " & sConteudo & vbNewLine
      
      Case 2 'Proposta BB
        sSql = sSql & "AND ISNULL(proposta_adesao_tb.proposta_bb,0) = " & sConteudo & vbNewLine
        
      Case 3 'Voucher
        sSql = sSql & "AND ps_voucher_tb.voucher_id = " & sConteudo & vbNewLine
        
      Case 4 'CPF ou CNPJ
        sSql = sSql & "AND cliente_tb.cpf_cnpj = '" & sConteudo & "'" & vbNewLine
             
    End Select

    Set SelecionarVoucher = ExecutarSQL(sSiglaSistema, _
                                        iAmbienteId, _
                                        sSiglaRecurso, _
                                        sDescricaoRecurso, _
                                        sSql, _
                                        True)
                                        

    Exit Function
    
Trata_Erro:

    Call Err.Raise(Err.Number, , "SEGL0064.cls00133.SelecionarVoucher - " & Err.Description)

End Function

Public Function PagamentoMovimentacao(ByVal sSiglaSistema As String, _
                                      ByVal sSiglaRecurso As String, _
                                      ByVal sDescricaoRecurso As String, _
                                      ByVal iAmbienteId As Integer, _
                                      Optional ByVal iTipoPesquisa As Integer, _
                                      Optional ByVal sConteudo As String) As Object

Dim sSql As String

On Error GoTo Trata_Erro

    sSql = ""
    sSql = sSql & "SELECT DISTINCT ps_acerto_tb.acerto_id, " & vbNewLine
    sSql = sSql & "       ps_acerto_tb.dt_acerto, " & vbNewLine
    sSql = sSql & "       ps_acerto_tb.val_total_acerto, " & vbNewLine
    sSql = sSql & "       ps_acerto_tb.dt_aprovacao, " & vbNewLine
    sSql = sSql & "       ps_acerto_tb.cod_origem, " & vbNewLine
    sSql = sSql & "       ps_mov_cliente_tb.movimentacao_id, " & vbNewLine
    sSql = sSql & "       ps_mov_cliente_tb.val_movimentacao, " & vbNewLine
    sSql = sSql & "       ps_mov_cliente_tb.retido_banco, " & vbNewLine
    sSql = sSql & "       ps_mov_cliente_tb.arquivo_retorno_bb, " & vbNewLine
    sSql = sSql & "       cliente_tb.cpf_cnpj, " & vbNewLine
    sSql = sSql & "       dt_movimentacao = SUBSTRING(CONVERT(VARCHAR(8), ps_movimentacao_tb.dt_movimentacao, 112), 7, 2) + '/' + " & vbNewLine
    sSql = sSql & "       SUBSTRING(CONVERT(VARCHAR(8), ps_movimentacao_tb.dt_movimentacao, 112), 5, 2) + '/' + " & vbNewLine
    sSql = sSql & "       SUBSTRING(CONVERT(VARCHAR(8), ps_movimentacao_tb.dt_movimentacao, 112), 1, 4), " & vbNewLine
    sSql = sSql & "       val_pro_labore = ISNULL(ps_mov_estipulante_tb.val_movimentacao,0), " & vbNewLine
    sSql = sSql & "       val_comissao = ISNULL(ISNULL(ps_mov_corretor_pf_tb.val_movimentacao,ps_mov_corretor_pj_tb.val_movimentacao),0), " & vbNewLine
    sSql = sSql & "       ps_mov_endosso_financeiro_tb.proposta_id, " & vbNewLine
    sSql = sSql & "       ps_mov_endosso_financeiro_tb.endosso_id " & vbNewLine
    sSql = sSql & "FROM ps_voucher_tb " & vbNewLine
    sSql = sSql & " JOIN ps_acerto_pagamento_tb WITH (NOLOCK) " & vbNewLine
    sSql = sSql & "   ON ps_acerto_pagamento_tb.voucher_id = ps_voucher_tb.voucher_id " & vbNewLine
    sSql = sSql & "  AND ps_acerto_pagamento_tb.cod_origem = ps_voucher_tb.cod_origem " & vbNewLine
    sSql = sSql & " JOIN ps_acerto_cliente_tb WITH (NOLOCK) " & vbNewLine
    sSql = sSql & "   ON ps_acerto_pagamento_tb.acerto_id = ps_acerto_cliente_tb.acerto_id " & vbNewLine
    sSql = sSql & " JOIN ps_acerto_tb WITH (NOLOCK) " & vbNewLine
    sSql = sSql & "   ON ps_acerto_tb.acerto_id = ps_acerto_pagamento_tb.acerto_id " & vbNewLine
    sSql = sSql & " JOIN ps_mov_cliente_tb WITH (NOLOCK) " & vbNewLine
    sSql = sSql & "   ON ps_mov_cliente_tb.acerto_id = ps_acerto_tb.acerto_id " & vbNewLine
    sSql = sSql & " JOIN ps_movimentacao_tb WITH (NOLOCK) " & vbNewLine
    sSql = sSql & "   ON ps_mov_cliente_tb.movimentacao_id = ps_movimentacao_tb.movimentacao_id " & vbNewLine
    sSql = sSql & " JOIN ps_mov_endosso_financeiro_tb WITH (NOLOCK) " & vbNewLine
    sSql = sSql & "   ON ps_mov_endosso_financeiro_tb.movimentacao_id = ps_movimentacao_tb.movimentacao_id " & vbNewLine
    sSql = sSql & " JOIN proposta_tb WITH (NOLOCK) " & vbNewLine
    sSql = sSql & "   ON proposta_tb.proposta_id = ps_mov_endosso_financeiro_tb.proposta_id " & vbNewLine
    sSql = sSql & " JOIN proposta_fechada_tb WITH (NOLOCK) " & vbNewLine
    sSql = sSql & "   ON proposta_fechada_tb.proposta_id = proposta_tb.proposta_id " & vbNewLine
    sSql = sSql & " JOIN cliente_tb WITH (NOLOCK) " & vbNewLine
    sSql = sSql & "   ON cliente_tb.cliente_id = proposta_tb.prop_cliente_id " & vbNewLine
    sSql = sSql & " LEFT JOIN ps_mov_estipulante_tb WITH (NOLOCK) " & vbNewLine
    sSql = sSql & "   ON ps_mov_estipulante_tb.movimentacao_id = ps_movimentacao_tb.movimentacao_id " & vbNewLine
    sSql = sSql & " LEFT JOIN ps_mov_item_financeiro_tb WITH (NOLOCK) " & vbNewLine
    sSql = sSql & "   ON ps_mov_item_financeiro_tb.movimentacao_id = ps_movimentacao_tb.movimentacao_id " & vbNewLine
    sSql = sSql & " JOIN ps_voucher_distribuicao_tb WITH (NOLOCK) " & vbNewLine
    sSql = sSql & "   ON ps_voucher_distribuicao_tb.voucher_id = ps_voucher_tb.voucher_id " & vbNewLine
    sSql = sSql & "  AND ps_voucher_distribuicao_tb.cod_origem = ps_voucher_tb.cod_origem " & vbNewLine
    sSql = sSql & " LEFT JOIN ps_mov_corretor_pf_tb WITH (NOLOCK) " & vbNewLine
    sSql = sSql & "   ON ps_mov_corretor_pf_tb.movimentacao_id = ps_movimentacao_tb.movimentacao_id " & vbNewLine
    sSql = sSql & " LEFT JOIN ps_mov_corretor_pj_tb WITH (NOLOCK) " & vbNewLine
    sSql = sSql & "   ON ps_mov_corretor_pj_tb.movimentacao_id = ps_movimentacao_tb.movimentacao_id " & vbNewLine

    Select Case iTipoPesquisa
  
      Case 1 'Proposta
        sSql = sSql & "WHERE ps_mov_endosso_financeiro_tb.proposta_id = " & sConteudo & vbNewLine
      
      Case 2 'Proposta BB
        sSql = sSql & "WHERE ISNULL(proposta_fechada_tb.proposta_bb,0) = " & sConteudo & vbNewLine
        
      Case 3 'Voucher
        sSql = sSql & "WHERE ps_voucher_tb.voucher_id = " & sConteudo & vbNewLine
        
      Case 4 'CPF ou CNPJ
        sSql = sSql & "WHERE cliente_tb.cpf_cnpj = '" & sConteudo & "'" & vbNewLine
    
    End Select
        
    sSql = sSql & "UNION " & vbNewLine
    
    sSql = sSql & "SELECT DISTINCT ps_acerto_tb.acerto_id, " & vbNewLine
    sSql = sSql & "       ps_acerto_tb.dt_acerto, " & vbNewLine
    sSql = sSql & "       ps_acerto_tb.val_total_acerto, " & vbNewLine
    sSql = sSql & "       ps_acerto_tb.dt_aprovacao, " & vbNewLine
    sSql = sSql & "       ps_acerto_tb.cod_origem, " & vbNewLine
    sSql = sSql & "       ps_mov_cliente_tb.movimentacao_id, " & vbNewLine
    sSql = sSql & "       ps_mov_cliente_tb.val_movimentacao, " & vbNewLine
    sSql = sSql & "       ps_mov_cliente_tb.retido_banco, " & vbNewLine
    sSql = sSql & "       ps_mov_cliente_tb.arquivo_retorno_bb, " & vbNewLine
    sSql = sSql & "       cliente_tb.cpf_cnpj, " & vbNewLine
    sSql = sSql & "       dt_movimentacao = SUBSTRING(CONVERT(VARCHAR(8), ps_movimentacao_tb.dt_movimentacao, 112), 7, 2) + '/' + " & vbNewLine
    sSql = sSql & "       SUBSTRING(CONVERT(VARCHAR(8), ps_movimentacao_tb.dt_movimentacao, 112), 5, 2) + '/' + " & vbNewLine
    sSql = sSql & "       SUBSTRING(CONVERT(VARCHAR(8), ps_movimentacao_tb.dt_movimentacao, 112), 1, 4), " & vbNewLine
    sSql = sSql & "       val_pro_labore = ISNULL(ps_mov_estipulante_tb.val_movimentacao,0), " & vbNewLine
    sSql = sSql & "       val_comissao = ISNULL(ISNULL(ps_mov_corretor_pf_tb.val_movimentacao,ps_mov_corretor_pj_tb.val_movimentacao),0), " & vbNewLine
    sSql = sSql & "       ps_mov_endosso_financeiro_tb.proposta_id, " & vbNewLine
    sSql = sSql & "       ps_mov_endosso_financeiro_tb.endosso_id " & vbNewLine
    sSql = sSql & "FROM ps_voucher_tb " & vbNewLine
    sSql = sSql & " JOIN ps_acerto_pagamento_tb WITH (NOLOCK) " & vbNewLine
    sSql = sSql & "   ON ps_acerto_pagamento_tb.voucher_id = ps_voucher_tb.voucher_id " & vbNewLine
    sSql = sSql & "  AND ps_acerto_pagamento_tb.cod_origem = ps_voucher_tb.cod_origem " & vbNewLine
    sSql = sSql & " JOIN ps_acerto_cliente_tb WITH (NOLOCK) " & vbNewLine
    sSql = sSql & "   ON ps_acerto_pagamento_tb.acerto_id = ps_acerto_cliente_tb.acerto_id " & vbNewLine
    sSql = sSql & " JOIN ps_acerto_tb WITH (NOLOCK) " & vbNewLine
    sSql = sSql & "   ON ps_acerto_tb.acerto_id = ps_acerto_pagamento_tb.acerto_id " & vbNewLine
    sSql = sSql & " JOIN ps_mov_cliente_tb WITH (NOLOCK) " & vbNewLine
    sSql = sSql & "   ON ps_mov_cliente_tb.acerto_id = ps_acerto_tb.acerto_id " & vbNewLine
    sSql = sSql & " JOIN ps_movimentacao_tb WITH (NOLOCK) " & vbNewLine
    sSql = sSql & "   ON ps_mov_cliente_tb.movimentacao_id = ps_movimentacao_tb.movimentacao_id " & vbNewLine
    sSql = sSql & " JOIN ps_mov_endosso_financeiro_tb WITH (NOLOCK) " & vbNewLine
    sSql = sSql & "   ON ps_mov_endosso_financeiro_tb.movimentacao_id = ps_movimentacao_tb.movimentacao_id " & vbNewLine
    sSql = sSql & " JOIN proposta_tb WITH (NOLOCK) " & vbNewLine
    sSql = sSql & "   ON proposta_tb.proposta_id = ps_mov_endosso_financeiro_tb.proposta_id " & vbNewLine
    sSql = sSql & " JOIN proposta_adesao_tb " & vbNewLine
    sSql = sSql & "   ON proposta_adesao_tb.proposta_id = proposta_tb.proposta_id " & vbNewLine
    sSql = sSql & " JOIN cliente_tb WITH (NOLOCK) " & vbNewLine
    sSql = sSql & "   ON cliente_tb.cliente_id = proposta_tb.prop_cliente_id " & vbNewLine
    sSql = sSql & " LEFT JOIN ps_mov_estipulante_tb WITH (NOLOCK) " & vbNewLine
    sSql = sSql & "   ON ps_mov_estipulante_tb.movimentacao_id = ps_movimentacao_tb.movimentacao_id " & vbNewLine
    sSql = sSql & " LEFT JOIN ps_mov_item_financeiro_tb WITH (NOLOCK) " & vbNewLine
    sSql = sSql & "   ON ps_mov_item_financeiro_tb.movimentacao_id = ps_movimentacao_tb.movimentacao_id " & vbNewLine
    sSql = sSql & " JOIN ps_voucher_distribuicao_tb WITH (NOLOCK) " & vbNewLine
    sSql = sSql & "   ON ps_voucher_distribuicao_tb.voucher_id = ps_voucher_tb.voucher_id " & vbNewLine
    sSql = sSql & "  AND ps_voucher_distribuicao_tb.cod_origem = ps_voucher_tb.cod_origem " & vbNewLine
    sSql = sSql & " LEFT JOIN ps_mov_corretor_pf_tb WITH (NOLOCK) " & vbNewLine
    sSql = sSql & "   ON ps_mov_corretor_pf_tb.movimentacao_id = ps_movimentacao_tb.movimentacao_id " & vbNewLine
    sSql = sSql & " LEFT JOIN ps_mov_corretor_pj_tb WITH (NOLOCK) " & vbNewLine
    sSql = sSql & "   ON ps_mov_corretor_pj_tb.movimentacao_id = ps_movimentacao_tb.movimentacao_id " & vbNewLine

    Select Case iTipoPesquisa
  
      Case 1 'Proposta
        sSql = sSql & "WHERE ps_mov_endosso_financeiro_tb.proposta_id = " & sConteudo & vbNewLine
      
      Case 2 'Proposta BB
        sSql = sSql & "WHERE ISNULL(proposta_adesao_tb.proposta_bb,0) = " & sConteudo & vbNewLine
        
      Case 3 'Voucher
        sSql = sSql & "WHERE ps_voucher_tb.voucher_id = " & sConteudo & vbNewLine
        
      Case 4 'CPF ou CNPJ
        sSql = sSql & "WHERE cliente_tb.cpf_cnpj = '" & sConteudo & "'" & vbNewLine
             
    End Select

    Set PagamentoMovimentacao = ExecutarSQL(sSiglaSistema, _
                                            iAmbienteId, _
                                            sSiglaRecurso, _
                                            sDescricaoRecurso, _
                                            sSql, _
                                            True)
                                        

    Exit Function
    
Trata_Erro:

    Call Err.Raise(Err.Number, , "SEGL0064.cls00133.PagamentoMovimentacao - " & Err.Description)

End Function


Public Function CancelamentoVoucher(ByVal sSiglaSistema As String, _
                                    ByVal sSiglaRecurso As String, _
                                    ByVal sDescricaoRecurso As String, _
                                    ByVal iAmbienteId As Integer, _
                                    ByVal lVoucherId As Long, _
                                    ByVal sCodOrigem As String, _
                                    ByVal sDtSistema As String, _
                                    ByVal sUsuario As String, _
                                    Optional bCancelaEndosso As Boolean)
'Cancela Voucher

Dim sSql As String

On Error GoTo Trata_Erro

  sSql = " EXEC Segs6068_spu" & vbNewLine
  sSql = sSql & lVoucherId & vbNewLine
  sSql = sSql & ",'" & sCodOrigem & "'" & vbNewLine
  sSql = sSql & ",'" & Format(sDtSistema, "yyyymmdd") & "'," & vbNewLine
  sSql = sSql & "'" & sUsuario & "'," & vbNewLine
  If bCancelaEndosso Then
    sSql = sSql & "'N'"
  Else
      sSql = sSql & "'S'"
  End If
    
  Call ExecutarSQL(sSiglaSistema, _
                   iAmbienteId, _
                   sSiglaRecurso, _
                   sDescricaoRecurso, _
                   sSql, _
                   False)

  Exit Function
    
Trata_Erro:

    Call Err.Raise(Err.Number, , "SEGL0064.cls00133.CancelamentoVoucher - " & Err.Description)

End Function

Public Sub CancelarEndossoRestituicao(ByVal sSiglaSistema As String, _
                                      ByVal sSiglaRecurso As String, _
                                      ByVal sDescricaoRecurso As String, _
                                      ByVal iAmbienteId As Integer, _
                                      ByVal sDtSistema As String, _
                                      ByVal sUsuario As String, _
                                      ByVal lPropostaId As Long, _
                                      ByVal lEndossoId As Long)
Dim sSql As String
Dim rsEndossoRestituicao As Recordset
Dim lVoucherId As Long

On Error GoTo Trata_Erro

'Verificando se o endosso � de restitui��o
'Ralves - 28/08/2008 - FLOW 499942 - Inserindo WITH (NOLOCK) nas tabelas da Query.
sSql = "SELECT 1" & vbNewLine
sSql = sSql & "FROM avaliacao_restituicao_tb WITH (NOLOCK)" & vbNewLine  'Ralves
sSql = sSql & "JOIN endosso_tb WITH (NOLOCK)" & vbNewLine                'Ralves
sSql = sSql & "  ON endosso_tb.proposta_id = avaliacao_restituicao_tb.proposta_id" & vbNewLine
sSql = sSql & " AND endosso_tb.endosso_id = avaliacao_restituicao_tb.endosso_id" & vbNewLine
sSql = sSql & "WHERE avaliacao_restituicao_tb.proposta_id = " & lPropostaId & vbNewLine
sSql = sSql & "  AND avaliacao_restituicao_tb.endosso_id = " & lEndossoId & vbNewLine
sSql = sSql & "  AND avaliacao_restituicao_tb.situacao <> 'R'" & vbNewLine 'CIBELE PEREIRA - INC000004537942 - 11/03/2015

Set rsEndossoRestituicao = ExecutarSQL(sSiglaSistema, _
                                       iAmbienteId, _
                                       sSiglaRecurso, _
                                       sDescricaoRecurso, _
                                       sSql, _
                                       True)
                                       
If Not rsEndossoRestituicao.EOF Then

    rsEndossoRestituicao.Close
    
    sSql = ""
    sSql = sSql & "SELECT isnull(MAX(voucher_id),0) AS voucher_id" & vbNewLine
    sSql = sSql & "FROM ps_mov_endosso_financeiro_tb" & vbNewLine
    sSql = sSql & "JOIN ps_mov_cliente_tb" & vbNewLine
    sSql = sSql & "  ON ps_mov_cliente_tb.movimentacao_id = ps_mov_endosso_financeiro_tb.movimentacao_id" & vbNewLine
    sSql = sSql & " AND ISNULL(ps_mov_cliente_tb.flag_cancelamento,'N') = 'N'" & vbNewLine
    sSql = sSql & "JOIN ps_acerto_pagamento_tb" & vbNewLine
    sSql = sSql & "  ON ps_acerto_pagamento_tb.acerto_id = ps_mov_cliente_tb.acerto_id" & vbNewLine
    sSql = sSql & " AND ps_acerto_pagamento_tb.cod_origem = 'RP'" & vbNewLine
    sSql = sSql & "WHERE ps_mov_endosso_financeiro_tb.proposta_id = " & lPropostaId & vbNewLine
    sSql = sSql & "  AND ps_mov_endosso_financeiro_tb.endosso_id = " & lEndossoId
    
    Set rsEndossoRestituicao = ExecutarSQL(sSiglaSistema, _
                                           iAmbienteId, _
                                           sSiglaRecurso, _
                                           sDescricaoRecurso, _
                                           sSql, _
                                           True)
                                            
    lVoucherId = rsEndossoRestituicao(0)
    
    Call CancelamentoVoucher(sSiglaSistema, _
                             sSiglaRecurso, _
                             sDescricaoRecurso, _
                             iAmbienteId, _
                             lVoucherId, _
                             "RP", _
                             sDtSistema, _
                             sUsuario, _
                             True)
    
End If

Exit Sub
    
Trata_Erro:

    Call Err.Raise(Err.Number, , "SEGL0064.cls00133.CancelarEndossoRestituicao - " & Err.Description)


End Sub
