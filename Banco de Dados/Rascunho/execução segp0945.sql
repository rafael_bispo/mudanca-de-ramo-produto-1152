begin tran

EXEC seguros_db.dbo.SEGS5276_SPU '11111111111'
, 'N'

 CREATE TABLE #proposta  (dt_inicio_vigencia SMALLDATETIME,
                         seq_proposta INT IDENTITY(1,1),
                         cod_produto SMALLINT,
                         cod_modalidade SMALLINT,
                         cod_item_modalidade INT,
                         num_contrato_seguro INT,
                         dado_als_emissao_id INT,
                         num_versao_endosso SMALLINT,
                         dt_fim_vigencia SMALLDATETIME,
                         dt_contratacao SMALLDATETIME,
                         banco_id INT,
                         canal_venda SMALLINT,
                         seguro_anterior VARCHAR(1),
                         tp_operacao SMALLINT,
                         num_contrato_renovado_seguro INT,
                         cont_agencia_id INT,
                         num_cotacao INT,
                         num_versao_cotacao SMALLINT,
                         dt_solicitacao_cotacao SMALLDATETIME,
                         link_inspecao VARCHAR(200),
                         produto_id INT,
                         apolice_coletiva CHAR(1),
                         ramo_id INT,
                         subramo_id NUMERIC(5),
                         dt_inicio_vigencia_sbr SMALLDATETIME,
                         funcionario_bb VARCHAR(20),
                         resseguro_facultativo CHAR(1),
                         periodo_pgto_id TINYINT,
                         tipo_ramo_id TINYINT,
                         proposta_id NUMERIC(9),
                         situacao CHAR(1),
                         cliente_id INT,
                         tp_wf_id INT,
                         produto_externo_id INT,
                         proposta_id_anterior NUMERIC(9),
                         e_mail VARCHAR(60),
                         origem_proposta_id INT,
                         canal_id INT,
                         proposta_bb NUMERIC(9),
                         usuario_id VARCHAR(20),
                         texto_condicao VARCHAR(60), 
                         wf_id BIGINT, 
                         num_processo_susep VARCHAR(25), 
                         num_contrato_provisorio_irb VARCHAR(20), 
                         proposta_basica_id NUMERIC(9), 
                         num_contrato_renovado NUMERIC(9),
                         val_remuneracao_servico NUMERIC(15,2),
                         num_agrupador INT, 
                         DT_ADESAO_INICIAL SMALLDATETIME,
                         PROPOSTA_INTERNET_ID int,
                         perc_remuneracao NUMERIC(5, 2), 
                         pin char(1), 
                         dt_confirma_contratacao SMALLDATETIME, 
                         proposta_oferta_agrupada VARCHAR(9)
) 
  CREATE UNIQUE INDEX UK_PROPOSTA_0001 ON #PROPOSTA(SEQ_PROPOSTA)
  CREATE INDEX AK_PROPOSTA_0001 ON #PROPOSTA(cod_produto, cod_modalidade, cod_item_modalidade, num_contrato_seguro, num_versao_endosso)
  CREATE INDEX AK_PROPOSTA_0002 ON #PROPOSTA(canal_id)


CREATE TABLE #cliente(proponente VARCHAR(60),
                      seq_proposta INT,
                      seq_cliente INT ,
                      cliente_id INT,
                      flag_alteracao CHAR(1),
                      pessoa_id TINYINT,
                      e_mail VARCHAR(60),
                      cod_mci NUMERIC(9, 0),
                      seq_end_mci SMALLINT,
                      tp_pessoa_id INT,
                      cpf VARCHAR(11),
                      dt_nascimento VARCHAR(8),
                      sexo CHAR(1),
                      estado_civil CHAR(1),
                      profissao_cbo INT,
                      doc_identidade VARCHAR(20),
                      tp_doc_identidade TINYINT,
                      exp_doc_identidade VARCHAR(15),
                      dt_emissao_identidade VARCHAR(8),
                      cnpj VARCHAR(14),
                      cod_atividade_pj INT,
                      endereco VARCHAR(60),
                      bairro VARCHAR(30),
                      cep VARCHAR(8),
                      municipio VARCHAR(30),
                      estado CHAR(2),
                      ddd VARCHAR(3),
                      telefone VARCHAR(9),  
                      num_cartao INT,
                      mes_vigencia_cartao SMALLINT,
                      ano_vigencia_cartao SMALLINT,
                      num_seql_proponente TINYINT)
  CREATE UNIQUE INDEX UK_cliente_0001 ON #cliente(SEQ_PROPOSTA)

CREATE TABLE #cliente_complementar(seq_proposta INT,
                                       num_seql_proponente TINYINT,
                                       proponente VARCHAR(60),
                                       tipo_proponente_id INT,
                                       seq_cliente INT ,
                                       cliente_id INT,
                                       flag_alteracao CHAR(1),
                                       pessoa_id TINYINT,
                                       e_mail VARCHAR(60),
                                       cod_mci NUMERIC(9, 0),
                                       seq_end_mci tinyINT,
                                       tp_pessoa_id INT,
                                       cpf VARCHAR(11),
                                       dt_nascimento VARCHAR(8),
                                       sexo CHAR(1),
                                       estado_civil CHAR(1),
                                       profissao_cbo INT,
                                       doc_identidade VARCHAR(20),
                                       tp_doc_identidade TINYINT,
                                       exp_doc_identidade VARCHAR(15),
                                       dt_emissao_identidade VARCHAR(8),
                                       cnpj VARCHAR(14),
                                       cod_atividade_pj INT,
                                       endereco VARCHAR(60),
                                       bairro VARCHAR(30),
                                       cep VARCHAR(8),
                                       municipio VARCHAR(30),
                                       estado CHAR(2),
                                       ddd VARCHAR(3),
                                       telefone VARCHAR(9),
                                       num_cartao INT,
                                       mes_vigencia_cartao SMALLINT,
                                       ano_vigencia_cartao SMALLINT)
CREATE UNIQUE INDEX UK_cliente_complementar_0001 ON #cliente_complementar(SEQ_PROPOSTA,num_seql_proponente)


CREATE TABLE #dados_financeiros(forma_pgto_id INT,
                                seq_proposta INT,
                                seguro_moeda_id INT,
                                num_parcelas INT,
                                conta_corrente NUMERIC(11, 0),
                                dia_debito INT,
                                apolice_id NUMERIC(11),
                                val_premio_tarifa NUMERIC(15, 2),
                                val_juros NUMERIC(15, 2),
                                perc_comissao NUMERIC(11,7),
                                custo_apolice NUMERIC(15, 2),
                                val_desconto_tecnico NUMERIC(15, 2),
                                val_desconto_comercial NUMERIC(15, 2),
                                perc_estipulante NUMERIC(11,7),
                                val_is NUMERIC(15, 2),
                                val_pgto_ato NUMERIC(15, 2),
                                val_parcela NUMERIC(15, 2),
                                val_bruto NUMERIC(15, 2),
                                val_iof NUMERIC(15, 2),
                                val_comissao NUMERIC(15, 2),
                                val_comissao_estipulante NUMERIC(15, 2),
                                val_ir_estipulante NUMERIC(11,7),
                                val_paridade_moeda NUMERIC(15, 2),
                                val_bonificacao NUMERIC(15, 2),
                                taxa_juros NUMERIC(11,7),
                                seguradora_cod_susep INT,
                                sucursal_seguradora_id INT,
                                perc_corretagem NUMERIC(11,7),
                                perc_iof NUMERIC(11,7),
                                perc_desconto_tecnico NUMERIC(11,7),
                                val_servico_assistencia NUMERIC(15,2),
                                agencia_id numeric(4,0),
                                val_desc_progressivo NUMERIC(4, 2),
                                val_desc_renovacao NUMERIC(4, 2),
                                val_desc_fidelidade NUMERIC(4, 2),
                                val_desc_shopping NUMERIC(4, 2),
                                val_assistencia_facul NUMERIC(15,2),
                                val_premio_liquido NUMERIC(15,2),
                                valor_desconto_progessivo NUMERIC(15,2),
                                valor_desconto_bonus NUMERIC(15,2),
                                valor_desconto_fidelidade NUMERIC(15,2),
                                deb_agencia_id NUMERIC(4,0),
                                cd_tip_periodo_pag TINYINT)
  CREATE UNIQUE INDEX UK_dados_financeiros_0001 ON #dados_financeiros(SEQ_PROPOSTA)


CREATE TABLE #corretagem(corretor_id NUMERIC(9),
                         seq_proposta INT)
  CREATE INDEX AK_corretagem_0001 ON #corretagem(SEQ_PROPOSTA)

CREATE TABLE #estipulacao(perc_pro_labore NUMERIC(11,7),
                          seq_proposta INT ,
                          est_cliente_id INT ,
                          est_nome varchar (60) ,
                          est_cod_mci int,
                          est_seq_end_mci int,
                          est_tp_pessoa_id int  ,
                          est_cpf varchar (11) ,
                          est_dt_nascimento smalldatetime  ,
                          est_sexo char (1) ,
                          est_estado_civil char (1) ,
                          est_profissao_cbo int  ,
                          est_doc_identidade varchar (20) ,
                          est_tp_doc_identidade varchar (30) ,
                          est_exp_doc_identidade varchar (15) ,
                          est_dt_emissao_identidade smalldatetime  ,
                          est_cnpj varchar (14) ,
                          est_cod_atividade_pj int  ,
                          est_endereco varchar (60) ,
                          est_bairro varchar (30) ,
                          est_cep varchar (8) ,
                          est_ddd VARCHAR(3),
                          est_telefone VARCHAR(9),  
                          est_municipio varchar (60) ,
                          est_estado char (2)) 
  CREATE INDEX AK_estipulacao_0001 ON #estipulacao(SEQ_PROPOSTA)

CREATE TABLE #cobertura(tp_cobertura_id int  ,
                        seq_proposta int,
                        val_risco_cobertura NUMERIC(15, 2) ,
                        fat_taxa NUMERIC(11,7)  ,
                        taxa_net NUMERIC(11,7)  ,
                        val_premio_cobertura NUMERIC(15, 2) ,
                        fat_desconto NUMERIC(11,7)   ,
                        fat_franquia NUMERIC(11,7)   ,
                        texto_franquia varchar (60)  ,
                        cod_objeto_segurado smallint  ,
                        dt_inicio_vigencia_plano smalldatetime  ,
                        plano_id smallint)
  CREATE INDEX AK_cobertura_0001 ON #cobertura(SEQ_PROPOSTA)


CREATE TABLE #beneficiario (seq_beneficiario int ,
                            seq_proposta INT ,
                            nome_beneficiario varchar (60),
                            cpf_cnpj_beneficiario varchar (14),
                            perc_particip_beneficiario NUMERIC(11,7)  ,
                            endereco_beneficiario varchar (60),
                            bairro_beneficiario varchar (60),
                            cidade_beneficiario varchar (60),
                            cep_beneficiario varchar (8),
                            pais_beneficiario varchar (45),
                            estado_beneficiario char (2), 
                            tp_componente_id tinyint, 
                            grau_parentesco tinyint) 
  CREATE INDEX AK_beneficiario_0001 ON #beneficiario(SEQ_PROPOSTA)

CREATE TABLE #clausula (seq_proposta INT,
                        seq_clausula INT,
                        sequencial INT IDENTITY (1, 1),
                        cod_clausula INT ,
                        texto TEXT,
                        endosso_id INT,
                        dt_inicio_vigencia_cl_original SMALLDATETIME ,
                        dt_inicio_vigencia SMALLDATETIME,
                        DESCR_CLAUSULA VARCHAR(255)) 
  CREATE INDEX AK_clausula_0001 ON #clausula(SEQ_PROPOSTA)

CREATE TABLE #questionario (sequencial INT IDENTITY (1, 1),
                            num_resposta int,
                            pergunta_id int,
                            seq_proposta INT,
                            tipo_questionario CHAR(1),
                            pergunta varchar (240),
                            resposta_id smallint,
                            resposta varchar (600),
                            questionario_id smallint,
                            questionario varchar (60),
                            cod_objeto_segurado INT,
                            tp_cobertura_id INT,
                            texto_complementar varchar(40),
                            num_ordem_pergunta smallint)
  CREATE INDEX AK_questionario_0001 ON #questionario(SEQ_PROPOSTA)


CREATE TABLE #questionario_determinado (num_resposta int,
                            pergunta_id int,
                            seq_proposta INT,
                            tipo_questionario CHAR(1),
                            pergunta varchar (240),
                            resposta_id smallint,
                            resposta varchar (600),
                            questionario_id smallint,
                            questionario varchar (60),
                            cod_objeto_segurado INT,
                            tp_cobertura_id INT,
                            texto_complementar varchar(40),
                            num_ordem_pergunta smallint,
                            grupo_dominio INT)
  CREATE INDEX AK_questionario_determinado_0001 ON #questionario_determinado(SEQ_PROPOSTA)

CREATE TABLE #questionario_indeterminado (num_resposta int,
                            pergunta_id int,
                            seq_proposta INT,
                            tipo_questionario CHAR(1),
                            pergunta varchar (240),
                            resposta_id smallint,
                            resposta varchar (600),
                            questionario_id smallint,
                            questionario varchar (60),
                            cod_objeto_segurado INT,
                            tp_cobertura_id INT,
                            texto_complementar varchar(40),
                            num_ordem_pergunta smallint)
  CREATE INDEX AK_questionario_indeterminado_0001 ON #questionario_indeterminado(SEQ_PROPOSTA)

CREATE TABLE #evento (seq_proposta INT,
                      layout_id   INT,
                      tp_operacao INT,
                      remessa_id INT,
                      projeto_id  INT)


CREATE TABLE #cobertura_vida (seq_proposta INT,
                              tp_cliente CHAR(1),
                              class_tp_cobertura VARCHAR(1),
                              dt_ini_vig_comp SMALLDATETIME,
                              tp_cobertura_id INT,
                              tp_cob_comp_id  INT,
                              tp_componente_id INT,
                              cod_objeto_segurado INT,
                              pc_prem_cbt_total NUMERIC(10,7) NULL DEFAULT(0),
                              pc_fat_cob NUMERIC(10,7) NULL DEFAULT(0))
  CREATE INDEX AK_cobertura_vida_0001 ON #cobertura_vida(SEQ_PROPOSTA)

CREATE TABLE #recusa (seq_proposta INT,
                      tp_Recusa_id INT,
                      cod_produto SMALLINT,
                      cod_modalidade SMALLINT,
                      cod_item_modalidade INT,
                      num_contrato_seguro INT,
                      num_versao_endosso INT,
                      dado_als_emissao_id INT,
                      proposta_id NUMERIC(9),
                      endosso_id INT,
                      recurso VARCHAR(8),
                      mensagem VARCHAR(60))

CREATE TABLE #declaracao(seq_proposta INT,
                         texto_declaracao VARCHAR(1000),
                         sequencial_declaracao INT)
  CREATE INDEX AK_declaracao_0001 ON #declaracao(SEQ_PROPOSTA)


CREATE TABLE #objeto (seq_proposta INT,
                      cod_objeto_segurado INT)
  CREATE INDEX AK_objeto_0001 ON #objeto(SEQ_PROPOSTA)

CREATE TABLE #historico (seq_historico INT IDENTITY (1, 1), 
                         texto TEXT,
                         seq_proposta INT)
  CREATE INDEX AK_historico_0001 ON #historico(SEQ_PROPOSTA)

CREATE TABLE #proposta_modular(seq_proposta INT, 
                               modulo_id INT, 
                               num_agrupador INT, 
                               cpf_cnpj VARCHAR(14)) 

CREATE TABLE #proposta_complementar(seq_proposta INT, 
                                   num_seql_proponente TINYINT, 
                                   prop_cliente_id INT,
                                   profissao_cbo INT,
                                   profissao_desc VARCHAR(60))


CREATE TABLE #dados_agricola_faturamento (    
       seq_proposta       INT            NULL,
       cultura            VARCHAR(100)   NULL,
       UF_propriedade     VARCHAR(2)     NULL,
       prod_esperada      NUMERIC(20,6)  NULL,
       perc_nivel_cobert  NUMERIC(20,14) NULL,
       area_segurada      NUMERIC(20,6)  NULL,
       val_custeio        NUMERIC(20,6)  NULL,
       dt_execucao        VARCHAR(10)    NULL,
       preco_base         NUMERIC(20,6)  NULL,
       perc_max_resp      NUMERIC(20,14) NULL,
       area_total         NUMERIC(20,6)  NULL,
       irrigada           VARCHAR(20)    NULL,
       cd_prd_agrl        INT            NULL,
       municipio_BACEN_id INT            NULL,
       fator_replantio    NUMERIC(20,6)  NULL,
       val_preco_bmf      NUMERIC(15,4)  NULL,
       perc_desagio       NUMERIC(20,6)  NULL,
       preco_base_calc    NUMERIC(15,4)  NULL,
       fat_esperado_calc  NUMERIC(15,2)  NULL 
)
CREATE INDEX AK_dados_agricola_faturamento ON #dados_agricola_faturamento (seq_proposta)

CREATE TABLE #dados_cobertura_agricola_faturamento (   
       seq_proposta                INT            NULL,
       tp_cobertura_id             INT            NULL,
       cod_objeto_segurado         INT            NULL,
       plano_id                    INT            NULL,
       cd_prd_agrl                 INT            NULL,
       dt_execucao                 SMALLDATETIME  NULL,
       UF_propriedade              VARCHAR(2)     NULL,
       municipio_BACEN_id          INT            NULL,
       perc_nivel_cobert           NUMERIC(20,14) NULL,
       prod_esperada               NUMERIC(20,6)  NULL,
       fator_replantio             NUMERIC(20,6)  NULL,
       val_preco_bmf               NUMERIC(15,4)  NULL,
       perc_desagio                NUMERIC(20,6)  NULL,
       preco_base_calc             NUMERIC(15,4)  NULL,
       fat_esperado_calc           NUMERIC(15,2)  NULL,
       area_segurada               NUMERIC(20,6)  NULL,
       val_custeio                 NUMERIC(20,6)  NULL,
       perc_max_resp               NUMERIC(20,14) NULL,
       taxa                        NUMERIC(20,14) NULL,
       val_cobertura_calc          NUMERIC(15,2)  NULL,
       val_premio_cobertura_calc   NUMERIC(15,2)  NULL,
       IN_IRG_PRD_AGRL             CHAR(1)        NULL,
       lim_max_cbt_fat_calc        NUMERIC(15,2)  NULL,
       lim_max_cbt_repl_calc       NUMERIC(15,2)  NULL 
)
CREATE INDEX AK_dados_cobertura_agricola_faturamento ON #dados_cobertura_agricola_faturamento (seq_proposta)

CREATE TABLE #avaliacao_proposta_SEGP0945 (seq_proposta INT, 
                                         tp_avaliacao_id INT, 
                                         num_avaliacao INT, 
                                         cod_produto SMALLINT, 
                                         cod_modalidade SMALLINT, 
                                         cod_item_modalidade INT, 
                                         num_contrato_seguro INT, 
                                         num_versao_endosso INT, 
                                         dado_als_emissao_id INT, 
                                         proposta_id NUMERIC(9), 
                                         campo_tratado VARCHAR(200), 
                                         valor_campo VARCHAR(200)) 
CREATE INDEX AK_avaliacao_proposta_SEGP0945 ON #avaliacao_proposta_SEGP0945 (seq_proposta)

CREATE TABLE #escolha_plano(seq_proposta INT,
                            dt_inicio_vigencia smalldatetime,
                            dt_escolha smalldatetime,
                            proposta_id numeric(9, 0),
                            plano_id numeric(5, 0),
                            imp_segurada numeric(9, 2),
                            produto_id int,
                            val_premio numeric(9, 2),
                            dt_fim_vigencia smalldatetime,
                            qtd_salarios int,
                            endosso_id int,
                            val_custo_uss numeric(15, 2),
                            taxa numeric(9, 7),
                            variacao_IGPM numeric(12, 9),
                            fator_renovacao numeric(12, 9),
                            dt_impressao_certificado smalldatetime,
                            val_premio_bruto numeric(15, 2),
                            val_premio_liquido numeric(15, 2),
                            val_premio_net_total numeric(15, 2),
                            val_comissao numeric(15, 2),
                            perc_comissao numeric(10, 7),
                            val_comissao_estipulante numeric(15, 2),
                            perc_comissao_estipulante numeric(10, 7),
                            val_remuneracao numeric(15, 2),
                            perc_remuneracao numeric(10, 7),
                            val_iof numeric(15, 2),
                            perc_iof numeric(10, 7),
                            val_juros numeric(15, 2),
                            perc_juros numeric(10, 7),
                            val_desconto numeric(15, 2),
                            val_assistencia_compulsoria_net numeric(15, 2),
                            val_assistencia_compulsoria numeric(15, 2),
                            val_assistencia_facultativa_net numeric(15, 2),
                            val_assistencia_facultativa numeric(15, 2),
                            val_primeira_parcela numeric(15, 2),
                            val_demais_parcelas numeric(15, 2),
                            num_parcelas int,
                            forma_pgto_id tinyint,
                            periodo_pgto_id tinyint,
                            deb_banco_id numeric(3, 0),
                            deb_agencia_id numeric(4, 0),
                            deb_conta_corrente_id numeric(11, 0),
                            dia_cobranca numeric(2, 0))
CREATE INDEX AK_escolha_plano ON #escolha_plano (seq_proposta)

CREATE TABLE #escolha_plano_tp_cob(seq_proposta INT,
                               proposta_id numeric(9, 0),
                               produto_id int,
                               plano_id numeric(5, 0),
                               dt_inicio_vigencia smalldatetime,
                               dt_escolha smalldatetime,
                               tp_cob_comp_id smallint,
                               dt_inicio_vigencia_cob smalldatetime,
                               val_is numeric(15, 2),
                               taxa numeric(15, 7),
                               val_premio numeric(15, 2),
                               dt_fim_vigencia_cob smalldatetime,
                               endosso_id int,
                               val_premio_net numeric(15, 4),
                               taxa_net numeric(15, 7),
                               val_premio_liquido numeric(15, 2),
                               val_premio_liquido_sem_assist_facul numeric(15, 2),
                               val_comissao numeric(15, 2),
                               val_comissao_estipulante numeric(15, 2),
                               val_remuneracao numeric(15, 2),
                               val_desconto numeric(15, 2),
                               val_assistencia_compulsoria_net numeric(15, 2),
                               val_assistencia_compulsoria numeric(15, 2),
                               val_assistencia_facultativa_net numeric(15, 2),
                               val_assistencia_facultativa numeric(15, 2))
CREATE INDEX AK_escolha_plano_tp_cob ON #escolha_plano_tp_cob (seq_proposta)

CREATE TABLE #DADO_FNCO(seq_proposta INT,
                               CD_FMA_PGTO int,
                               QT_PCL_PGTO_CTR int,
                               NR_AG_DEB int,
                               NR_CC_DEB decimal(11, 0),
                               DD_DEB_CBR_PREM int,
                               PC_IOF_CTR decimal(10, 7),
                               VL_IOF_CTR decimal(15, 4),
                               PC_CRE_CTR decimal(10, 7),
                               VL_CRE_AVLD decimal(15, 4),
                               PC_ETLE_CTR decimal(10, 7),
                               VL_PREM_MOEN_CTR decimal(15, 4),
                               VL_PRMO_MOEN_CTR decimal(15, 4),
                               VL_PCL_MOEN_CTR decimal(15, 4),
                               VL_LQDO_MOEN_CTR decimal(15, 4),
                               PC_JUR_PCLT_CTR decimal(10, 7),
                               VL_JUR_PCLT_CTR decimal(15, 4),
                               CD_TIP_PERC_PGTO tinyint)
CREATE INDEX AK_dado_fnco ON #DADO_FNCO (seq_proposta)

CREATE TABLE #CBT(seq_proposta INT,
                      CD_CBT int,
                      NR_SEQL_CBT_CTR smallint,
                      PC_PREM_CBT_CTR decimal(10, 7),
                      VL_PREM_CBT_CTR decimal(15, 4))
CREATE INDEX AK_cbt ON #CBT (seq_proposta)

CREATE TABLE #RMNC_SGRO(seq_proposta INT,
                       PC_RMNC_SGRO_CTR decimal(10, 7),
                       VL_RMNC_SGRO_CTR decimal(15, 4))
CREATE INDEX AK_rmnc_sgro ON #RMNC_SGRO (seq_proposta)

CREATE TABLE #PLN_AST(seq_proposta INT,
                           CD_PLN_AST int,
                           VL_SRVC_AST_CTR decimal(15, 4),
                           NR_SEQL_PLN_CTR smallint)
CREATE INDEX AK_pln_ast ON #PLN_AST (seq_proposta)

EXEC als_operacao_db..SEGS4777_SPS 'P', 'N', 100


EXEC SEGS4779_SPD

SELECT DISTINCT #proposta.produto_id
FROM #proposta
JOIN produto_parametro_emissao_tb
  ON produto_parametro_emissao_tb.produto_id = #proposta.produto_id


EXEC SEGS4780_SPU

EXEC SEGS5345_SPU

EXEC SEGS4781_SPI
'20200520','11111111111'

SEGS5322_SPI

EXEC SEGS9363_SPI 
'11111111111'

EXEC SEGS4782_SPI
'20200520','11111111111'

EXEC interface_dados_db..SEGS4783_SPU
'20200520','11111111111'
, 'N'


SET NOCOUNT ON EXEC workflow_contratacao_als_db..executar_atividade_lote_spu 

rollback