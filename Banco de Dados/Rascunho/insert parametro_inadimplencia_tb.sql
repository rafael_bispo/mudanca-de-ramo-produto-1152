DECLARE @total_registro INT
DECLARE @total_registro_afetados INT
DECLARE @mensagem VARCHAR(500)

SET @total_registro = 1
SET @total_registro_afetados = 0





INSERT INTO [dbo].[parametro_inadimplencia_tb]([ramo_id], [subramo_id], [dt_inicio_vigencia_sbr], [dt_inicio_vigencia], [dt_fim_vigencia], [aguarda_solicitacao_cancelamento_bb], [emite_aviso_inadimplencia_primeira_parc], [emite_aviso_inadimplencia_demais_parc], [dias_para_emissao_aviso], [cancela_documento], [dias_para_canc_primeira_parc], [dias_para_canc_demais_parc], [canc_requer_aviso_inadimplencia], [canc_aguarda_periodo_cobertura], [emite_carta_cancelamento], [arquivo_aviso_inadimplencia], [arquivo_carta_cancelamento], [dias_registro_inadimplencia], [canc_parcelas_sem_pagamento], [identificacao_inadimplencia_bb], [inadimplencia_requer_retorno_bb], [tp_criterio_cancelamento], [tp_cancelamento], [tp_calc_periodo_cobertura], [dt_inclusao], [dt_alteracao], [usuario], [produto_id], [ident_inadimplencia_bb_primeira_parc], [ident_inadimplencia_bb_demais_parc], [cancela_agencia_corporate], [cancela_corretor_independente], [cancela_cliente_private], [cancela_endosso], [docs_a_cancelar], [gera_workflow], [motivo_alteracao], [qtde_parcelas_carta_inadimplencia], [parcelas_consecutivas_inadimplencia], [DATA_CORTE], [enviar_aviso_grid_com_retorno], [enviar_aviso_grid_sem_retorno], [enviar_para_grid], [cbr_debito_apos_recusa_cc], [dias_para_canc_apos_recusa_cc])
SELECT  1, 102, '20190123 00:00:00.000', '20190531 00:00:00.000', NULL, N'N', N'S', N'S', 5, N'S', 60, 90, N'S', N'S', N'S', N'Null', N'Null', 15, 0, N'N', N'N', N'D', N'9', N'C', '20190531 11:54:00.000', NULL, N'25630054856   ', 1152, N'N', N'N', N'S', N'S', N'S', N'S', N'T', N'N', N'31.05.2019 - Alterações a pedido da Unidade Agricola - Bruno/Mercie. Ponto de Auditoria.', 0, N'N', '20160505 00:00:00.000', 0, 0, 0, NULL, NULL
SET @total_registro_afetados = @total_registro_afetados + @@ROWCOUNT


-- Verificação
SELECT @total_registro_afetados

SELECT @total_registro

IF ( (@@SERVERNAME = 'SISAB003' or @@SERVERNAME = 'SISAS003\ABS') AND @total_registro <> @total_registro_afetados )
BEGIN
SET @mensagem = 'qtd de registros que deveriam ser afetados: ' + CONVERT(VARCHAR(10), @total_registro) + 'qtd de registros afetados: ' + CONVERT(VARCHAR(10), @total_registro_afetados)

RAISERROR (@mensagem, 16, 1)
END
GO



