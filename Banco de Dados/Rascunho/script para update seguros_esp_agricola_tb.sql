use desenv_db
go

alter proc dbo.update_esp_1152
as


CREATE TABLE #dados_financeiros(forma_pgto_id INT,
                                seq_proposta INT,
                                seguro_moeda_id INT,
                                num_parcelas INT,
                                conta_corrente NUMERIC(11, 0),
                                dia_debito INT,
                                apolice_id NUMERIC(11),
                                val_premio_tarifa NUMERIC(15, 2),
                                val_juros NUMERIC(15, 2),
                                perc_comissao NUMERIC(11,7),
                                custo_apolice NUMERIC(15, 2),
                                val_desconto_tecnico NUMERIC(15, 2),
                                val_desconto_comercial NUMERIC(15, 2),
                                perc_estipulante NUMERIC(11,7),
                                val_is NUMERIC(15, 2),
                                val_pgto_ato NUMERIC(15, 2),
                                val_parcela NUMERIC(15, 2),
                                val_bruto NUMERIC(15, 2),
                                val_iof NUMERIC(15, 2),
                                val_comissao NUMERIC(15, 2),
                                val_comissao_estipulante NUMERIC(15, 2),
                                val_ir_estipulante NUMERIC(11,7),
                                val_paridade_moeda NUMERIC(15, 2),
                                val_bonificacao NUMERIC(15, 2),
                                taxa_juros NUMERIC(11,7),
                                seguradora_cod_susep INT,
                                sucursal_seguradora_id INT,
                                perc_corretagem NUMERIC(11,7),
                                perc_iof NUMERIC(11,7),
                                perc_desconto_tecnico NUMERIC(11,7),
                                val_servico_assistencia NUMERIC(15,2),
                                agencia_id numeric(4,0),
                                val_desc_progressivo NUMERIC(4, 2),
                                val_desc_renovacao NUMERIC(4, 2),
                                val_desc_fidelidade NUMERIC(4, 2),
                                val_desc_shopping NUMERIC(4, 2),
                                val_assistencia_facul NUMERIC(15,2),
                                deb_agencia_id NUMERIC(4,0),
                                val_premio_liquido numeric(15, 2),
                                cd_tip_periodo_pag TINYINT)
  CREATE UNIQUE INDEX UK_dados_financeiros_0001 ON #dados_financeiros(SEQ_PROPOSTA)


 CREATE TABLE #cobertura(tp_cobertura_id int  ,
                        seq_proposta int,
                        val_risco_cobertura NUMERIC(15, 2) ,
                        fat_taxa NUMERIC(11,7)  ,
                        taxa_net NUMERIC(11,7)  ,
                        val_premio_cobertura NUMERIC(15, 2) ,
                        fat_desconto NUMERIC(11,7)   ,
                        fat_franquia NUMERIC(11,7)   ,
                        texto_franquia varchar (60)  ,
                        cod_objeto_segurado smallint  ,
                        dt_inicio_vigencia_plano smalldatetime  ,
                        plano_id smallint)
  CREATE INDEX AK_cobertura_0001 ON #cobertura(SEQ_PROPOSTA)
  
  
 CREATE TABLE #dados_cobertura_agricola_faturamento (   
       seq_proposta                INT            NULL,
       tp_cobertura_id             INT            NULL,
       cod_objeto_segurado         INT            NULL,
       plano_id                    INT            NULL,
       cd_prd_agrl                 INT            NULL,
       dt_execucao                 SMALLDATETIME  NULL,
       UF_propriedade              VARCHAR(2)     NULL,
       municipio_BACEN_id          INT            NULL,
       perc_nivel_cobert           NUMERIC(20,14) NULL,
       prod_esperada               NUMERIC(20,6)  NULL,
       fator_replantio             NUMERIC(20,6)  NULL,
       val_preco_bmf               NUMERIC(15,4)  NULL,
       perc_desagio                NUMERIC(20,6)  NULL,
       preco_base_calc             NUMERIC(15,4)  NULL,
       fat_esperado_calc           NUMERIC(15,2)  NULL,
       area_segurada               NUMERIC(20,6)  NULL,
       val_custeio                 NUMERIC(20,6)  NULL,
       perc_max_resp               NUMERIC(20,14) NULL,
       taxa                        NUMERIC(20,14) NULL,
       val_cobertura_calc          NUMERIC(15,2)  NULL,
       val_premio_cobertura_calc   NUMERIC(15,2)  NULL,
       IN_IRG_PRD_AGRL             CHAR(1)        NULL,
       lim_max_cbt_fat_calc        NUMERIC(15,2)  NULL,
       lim_max_cbt_repl_calc       NUMERIC(15,2)  NULL 
)
CREATE INDEX AK_dados_cobertura_agricola_faturamento ON #dados_cobertura_agricola_faturamento (seq_proposta)


CREATE TABLE #estipulacao(perc_pro_labore NUMERIC(11,7),
                          seq_proposta INT ,
                          est_cliente_id INT ,
                          est_nome varchar (60) ,
                          est_cod_mci int,
                          est_seq_end_mci int,
                          est_tp_pessoa_id int  ,
                          est_cpf varchar (11) ,
                          est_dt_nascimento smalldatetime  ,
                          est_sexo char (1) ,
                          est_estado_civil char (1) ,
                          est_profissao_cbo int  ,
                          est_doc_identidade varchar (20) ,
                          est_tp_doc_identidade varchar (30) ,
                          est_exp_doc_identidade varchar (15) ,
                          est_dt_emissao_identidade smalldatetime  ,
                          est_cnpj varchar (14) ,
                          est_cod_atividade_pj int  ,
                          est_endereco varchar (60) ,
                          est_bairro varchar (30) ,
                          est_cep varchar (8) ,
                          est_ddd VARCHAR(3),
                          est_telefone VARCHAR(9),  
                          est_municipio varchar (60) ,
                          est_estado char (2)) 
  CREATE INDEX AK_estipulacao_0001 ON #estipulacao(SEQ_PROPOSTA)


CREATE TABLE #questionario (sequencial INT IDENTITY (1, 1),
                            num_resposta int,
                            pergunta_id int,
                            seq_proposta INT,
                            tipo_questionario CHAR(1),
                            pergunta varchar (240),
                            resposta_id smallint,
                            resposta varchar (600),
                            questionario_id smallint,
                            questionario varchar (60),
                            cod_objeto_segurado INT,
                            tp_cobertura_id INT,
                            texto_complementar varchar(40),
                            num_ordem_pergunta smallint)
  CREATE INDEX AK_questionario_0001 ON #questionario(SEQ_PROPOSTA)


CREATE TABLE #questionario_determinado (num_resposta int,
                            pergunta_id int,
                            seq_proposta INT,
                            tipo_questionario CHAR(1),
                            pergunta varchar (240),
                            resposta_id smallint,
                            resposta varchar (600),
                            questionario_id smallint,
                            questionario varchar (60),
                            cod_objeto_segurado INT,
                            tp_cobertura_id INT,
                            texto_complementar varchar(40),
                            num_ordem_pergunta smallint,
                            grupo_dominio INT)
  CREATE INDEX AK_questionario_determinado_0001 ON #questionario_determinado(SEQ_PROPOSTA)

CREATE TABLE #questionario_indeterminado (num_resposta int,
                            pergunta_id int,
                            seq_proposta INT,
                            tipo_questionario CHAR(1),
                            pergunta varchar (240),
                            resposta_id smallint,
                            resposta varchar (600),
                            questionario_id smallint,
                            questionario varchar (60),
                            cod_objeto_segurado INT,
                            tp_cobertura_id INT,
                            texto_complementar varchar(40),
                            num_ordem_pergunta smallint)
  CREATE INDEX AK_questionario_indeterminado_0001 ON #questionario_indeterminado(SEQ_PROPOSTA)
  
  
CREATE TABLE #dados_agricola_faturamento (    
       seq_proposta       INT            NULL,
       cultura            VARCHAR(100)   NULL,
       UF_propriedade     VARCHAR(2)     NULL,
       prod_esperada      NUMERIC(20,6)  NULL,
       perc_nivel_cobert  NUMERIC(20,14) NULL,
       area_segurada      NUMERIC(20,6)  NULL,
       val_custeio        NUMERIC(20,6)  NULL,
       dt_execucao        VARCHAR(10)    NULL,
       preco_base         NUMERIC(20,6)  NULL,
       perc_max_resp      NUMERIC(20,14) NULL,
       area_total         NUMERIC(20,6)  NULL,
       irrigada           VARCHAR(20)    NULL,
       cd_prd_agrl        INT            NULL,
       municipio_BACEN_id INT            NULL,
       fator_replantio    NUMERIC(20,6)  NULL,
       val_preco_bmf      NUMERIC(15,4)  NULL,
       perc_desagio       NUMERIC(20,6)  NULL,
       preco_base_calc    NUMERIC(15,4)  NULL,
       fat_esperado_calc  NUMERIC(15,2)  NULL 
)
CREATE INDEX AK_dados_agricola_faturamento ON #dados_agricola_faturamento (seq_proposta)

    
select b.*, a.canal_id, c.cont_agencia_id, a.produto_id,c.dt_inicio_vig as dt_inicio_vigencia      
  into #proposta
  from seguros_db..proposta_tb a with (nolock) 
  inner join seguros_db..proposta_processo_susep_tb b with (nolock)  
  on a.proposta_id = b.proposta_id
  inner join seguros_db..proposta_fechada_tb c with (nolock)  
  on a.proposta_id = c.proposta_Id
 where a.proposta_id in (255706015,255706016,255706017,255706018,255706019,255706020,255706021,255706022,255706023,
255706024,255706025,255706026,255706027,255706028,255706029,255706030,255706031,255706032,255706033,255706034,
255706035,255706036,255706037,255706038,255706039) 

alter table #proposta add seq_proposta INT IDENTITY(1,1)


INSERT INTO #questionario_determinado (  
 seq_proposta  
 ,tipo_questionario  
 ,pergunta_id  
 ,pergunta  
 ,resposta_id  
 ,resposta  
 ,questionario_id  
 ,questionario  
 ,num_ordem_pergunta  
 ,cod_objeto_segurado  
 ,texto_complementar  
 ,tp_cobertura_id  
 ,grupo_dominio  
 )  
SELECT seq_proposta  
 ,tipo_questionario = 'P'  
 ,pergunta_id = als_produto_db..pergunta_concatenada_vw.pergunta_id  
 ,pergunta = als_produto_db..pergunta_concatenada_vw.nome  
 ,resposta_id = NULL    
 ,resposta = als_operacao_db..PRTD_RSCO_CTR_CONCATENADO_VW.TX_DCR_ATB_CTR   
 ,questionario_id = als_produto_db..questionario_tb.questionario_id  
 ,questionario = als_produto_db..questionario_tb.nome  
 ,num_ordem_pergunta  
 ,NULL  
 ,''  
 ,NULL  
 ,grupo_dominio = als_produto_db..pergunta_concatenada_vw.grupo_dominio_resposta_id  
FROM #proposta  
JOIN als_operacao_db..PRTD_RSCO_CTR_CONCATENADO_VW WITH (NOLOCK) ON als_operacao_db..PRTD_RSCO_CTR_CONCATENADO_VW.CD_PRD = cod_produto  
 AND als_operacao_db..PRTD_RSCO_CTR_CONCATENADO_VW.CD_MDLD = cod_modalidade  
 AND als_operacao_db..PRTD_RSCO_CTR_CONCATENADO_VW.CD_ITEM_MDLD = cod_item_modalidade  
 AND als_operacao_db..PRTD_RSCO_CTR_CONCATENADO_VW.NR_CTR_SGRO = num_contrato_seguro  
 AND als_operacao_db..PRTD_RSCO_CTR_CONCATENADO_VW.NR_VRS_EDS = num_versao_endosso  
JOIN als_produto_db..pergunta_concatenada_vw WITH (NOLOCK) ON als_produto_db..pergunta_concatenada_vw.pergunta_id = als_operacao_db..PRTD_RSCO_CTR_CONCATENADO_VW.NR_QST  
 AND als_produto_db..pergunta_concatenada_vw.grupo_dominio_resposta_id IS NOT NULL  
JOIN als_produto_db..questionario_tb WITH (NOLOCK) ON als_produto_db..questionario_tb.questionario_id = als_operacao_db..PRTD_RSCO_CTR_CONCATENADO_VW.NR_QSTN  
JOIN als_produto_db..questionario_pergunta_tb WITH (NOLOCK) ON als_produto_db..questionario_pergunta_tb.questionario_id = als_produto_db..questionario_tb.questionario_id  
 AND als_produto_db..questionario_pergunta_tb.pergunta_id = als_produto_db..pergunta_concatenada_vw.pergunta_id  
WHERE (als_operacao_db..PRTD_RSCO_CTR_CONCATENADO_VW.CD_TIP_QSTN = 1)  
 OR (  
  als_operacao_db..PRTD_RSCO_CTR_CONCATENADO_VW.CD_TIP_QSTN IN (  
   91  
   ,92  
   ) --BNB  
  AND canal_id = 3  
  )  
  
UNION  
  
--Questionrio do questionrio do objeto de risco (JR)  
SELECT seq_proposta  
 ,tipo_questionario = 'O'  
 ,pergunta_id = als_produto_db..pergunta_concatenada_vw.pergunta_id  
 ,pergunta = als_produto_db..pergunta_concatenada_vw.nome  
 ,resposta_id = NULL  
 ,resposta = als_operacao_db..PRTD_RSCO_CTR_CONCATENADO_VW.TX_DCR_ATB_CTR  
 ,questionario_id = als_produto_db..questionario_tb.questionario_id  
 ,questionario = als_produto_db..questionario_tb.nome  
 ,num_ordem_pergunta  
 ,als_operacao_db..PRTD_RSCO_CTR_CONCATENADO_VW.NR_SEQL_QSTN_CTR  
 ,als_operacao_db..PRTD_RSCO_CTR_CONCATENADO_VW.TX_CMPT_RPST_CTR  
 ,NULL  
 ,grupo_dominio = als_produto_db..pergunta_concatenada_vw.grupo_dominio_resposta_id  
FROM #proposta  
JOIN als_operacao_db..PRTD_RSCO_CTR_CONCATENADO_VW WITH (NOLOCK) ON als_operacao_db..PRTD_RSCO_CTR_CONCATENADO_VW.CD_PRD = cod_produto  
 AND als_operacao_db..PRTD_RSCO_CTR_CONCATENADO_VW.CD_MDLD = cod_modalidade  
 AND als_operacao_db..PRTD_RSCO_CTR_CONCATENADO_VW.CD_ITEM_MDLD = cod_item_modalidade  
 AND als_operacao_db..PRTD_RSCO_CTR_CONCATENADO_VW.NR_CTR_SGRO = num_contrato_seguro  
 AND als_operacao_db..PRTD_RSCO_CTR_CONCATENADO_VW.NR_VRS_EDS = num_versao_endosso  
JOIN als_produto_db..pergunta_concatenada_vw WITH (NOLOCK) ON als_produto_db..pergunta_concatenada_vw.pergunta_id = als_operacao_db..PRTD_RSCO_CTR_CONCATENADO_VW.NR_QST  
 AND ISNULL(als_produto_db..pergunta_concatenada_vw.grupo_dominio_resposta_id, 0) > 0  
JOIN als_produto_db..questionario_tb WITH (NOLOCK) ON als_produto_db..questionario_tb.questionario_id = als_operacao_db..PRTD_RSCO_CTR_CONCATENADO_VW.NR_QSTN  
JOIN als_produto_db..questionario_pergunta_tb WITH (NOLOCK) ON als_produto_db..questionario_pergunta_tb.questionario_id = als_produto_db..questionario_tb.questionario_id  
 AND als_produto_db..questionario_pergunta_tb.pergunta_id = als_produto_db..pergunta_concatenada_vw.pergunta_id  
WHERE (  
  als_operacao_db..PRTD_RSCO_CTR_CONCATENADO_VW.CD_TIP_QSTN IN (  
   2  
   ,4  
   )  
  )  
 OR (  
  als_operacao_db..PRTD_RSCO_CTR_CONCATENADO_VW.CD_TIP_QSTN = 94 --BNB  
  AND canal_id = 3  
  )  
  
UNION  
  
--Questionrio da cobertura  
SELECT seq_proposta  
 ,tipo_questionario = 'C'  
 ,pergunta_id = als_produto_db..pergunta_concatenada_vw.pergunta_id  
 ,pergunta = als_produto_db..pergunta_concatenada_vw.nome  
 ,resposta_id = NULL  
 ,resposta = als_operacao_db..PRTD_RSCO_CTR_CONCATENADO_VW.TX_DCR_ATB_CTR   
 ,questionario_id = als_produto_db..questionario_tb.questionario_id  
 ,questionario = als_produto_db..questionario_tb.nome  
 ,num_ordem_pergunta  
 ,als_operacao_db..PRTD_RSCO_CTR_CONCATENADO_VW.NR_SEQL_QSTN_CTR  
 ,''  
 ,als_operacao_db..PRTD_RSCO_CTR_CONCATENADO_VW.NR_CD_TIP_QSTN_CTR  
 ,grupo_dominio = als_produto_db..pergunta_concatenada_vw.grupo_dominio_resposta_id  
FROM #proposta  
JOIN als_operacao_db..PRTD_RSCO_CTR_CONCATENADO_VW WITH (NOLOCK) ON als_operacao_db..PRTD_RSCO_CTR_CONCATENADO_VW.CD_PRD = cod_produto  
 AND als_operacao_db..PRTD_RSCO_CTR_CONCATENADO_VW.CD_MDLD = cod_modalidade  
 AND als_operacao_db..PRTD_RSCO_CTR_CONCATENADO_VW.CD_ITEM_MDLD = cod_item_modalidade  
 AND als_operacao_db..PRTD_RSCO_CTR_CONCATENADO_VW.NR_CTR_SGRO = num_contrato_seguro  
 AND als_operacao_db..PRTD_RSCO_CTR_CONCATENADO_VW.NR_VRS_EDS = num_versao_endosso  
JOIN als_produto_db..pergunta_concatenada_vw WITH (NOLOCK) ON als_produto_db..pergunta_concatenada_vw.pergunta_id = als_operacao_db..PRTD_RSCO_CTR_CONCATENADO_VW.NR_QST  
 AND als_produto_db..pergunta_concatenada_vw.grupo_dominio_resposta_id IS NOT NULL  
JOIN als_produto_db..questionario_tb WITH (NOLOCK) ON als_produto_db..questionario_tb.questionario_id = als_operacao_db..PRTD_RSCO_CTR_CONCATENADO_VW.NR_QSTN  
JOIN als_produto_db..questionario_pergunta_tb WITH (NOLOCK) ON als_produto_db..questionario_pergunta_tb.questionario_id = als_produto_db..questionario_tb.questionario_id  
 AND als_produto_db..questionario_pergunta_tb.pergunta_id = als_produto_db..pergunta_concatenada_vw.pergunta_id  
WHERE als_operacao_db..PRTD_RSCO_CTR_CONCATENADO_VW.CD_TIP_QSTN = 3  
  
UNION  
  
--Questionrio do Proponente  
SELECT seq_proposta  
 ,tipo_questionario = 'R'  
 ,pergunta_id = als_produto_db..pergunta_concatenada_vw.pergunta_id  
 ,pergunta = als_produto_db..pergunta_concatenada_vw.nome  
 ,resposta_id = NULL  
 ,resposta = als_operacao_db..PRTD_RSCO_CTR_CONCATENADO_VW.TX_DCR_ATB_CTR  
 ,questionario_id = als_produto_db..questionario_tb.questionario_id  
 ,questionario = als_produto_db..questionario_tb.nome  
 ,num_ordem_pergunta  
 ,als_operacao_db..PRTD_RSCO_CTR_CONCATENADO_VW.NR_SEQL_QSTN_CTR  
 ,als_operacao_db..PRTD_RSCO_CTR_CONCATENADO_VW.TX_CMPT_RPST_CTR  
 ,als_operacao_db..PRTD_RSCO_CTR_CONCATENADO_VW.NR_CD_TIP_QSTN_CTR  
 ,grupo_dominio = als_produto_db..pergunta_concatenada_vw.grupo_dominio_resposta_id  
FROM #proposta  
JOIN als_operacao_db..PRTD_RSCO_CTR_CONCATENADO_VW WITH (NOLOCK) ON als_operacao_db..PRTD_RSCO_CTR_CONCATENADO_VW.CD_PRD = cod_produto  
 AND als_operacao_db..PRTD_RSCO_CTR_CONCATENADO_VW.CD_MDLD = cod_modalidade  
 AND als_operacao_db..PRTD_RSCO_CTR_CONCATENADO_VW.CD_ITEM_MDLD = cod_item_modalidade  
 AND als_operacao_db..PRTD_RSCO_CTR_CONCATENADO_VW.NR_CTR_SGRO = num_contrato_seguro  
 AND als_operacao_db..PRTD_RSCO_CTR_CONCATENADO_VW.NR_VRS_EDS = num_versao_endosso  
JOIN als_produto_db..pergunta_concatenada_vw WITH (NOLOCK) ON als_produto_db..pergunta_concatenada_vw.pergunta_id = als_operacao_db..PRTD_RSCO_CTR_CONCATENADO_VW.NR_QST  
 AND als_produto_db..pergunta_concatenada_vw.grupo_dominio_resposta_id IS NOT NULL  
JOIN als_produto_db..questionario_tb WITH (NOLOCK) ON als_produto_db..questionario_tb.questionario_id = als_operacao_db..PRTD_RSCO_CTR_CONCATENADO_VW.NR_QSTN  
JOIN als_produto_db..questionario_pergunta_tb WITH (NOLOCK) ON als_produto_db..questionario_pergunta_tb.questionario_id = als_produto_db..questionario_tb.questionario_id  
 AND als_produto_db..questionario_pergunta_tb.pergunta_id = als_produto_db..pergunta_concatenada_vw.pergunta_id  
WHERE als_operacao_db..PRTD_RSCO_CTR_CONCATENADO_VW.CD_TIP_QSTN = 5  
ORDER BY seq_proposta  
 ,tipo_questionario  
 ,als_produto_db..questionario_pergunta_tb.num_ordem_pergunta  
  

--indeterminadas  
INSERT INTO #questionario_indeterminado (  
 seq_proposta  
 ,tipo_questionario  
 ,pergunta_id  
 ,pergunta  
 ,resposta_id  
 ,resposta  
 ,questionario_id  
 ,questionario  
 ,num_ordem_pergunta  
 ,cod_objeto_segurado  
 ,texto_complementar  
 ,tp_cobertura_id  
 )  
SELECT seq_proposta  
 ,tipo_questionario = 'P'  
 ,pergunta_id = als_produto_db..pergunta_concatenada_vw.pergunta_id  
 ,pergunta = als_produto_db..pergunta_concatenada_vw.nome  
 ,resposta_id = 0  
 ,resposta = als_operacao_db..PRTD_RSCO_CTR_CONCATENADO_VW.TX_DCR_ATB_CTR  
 ,questionario_id = als_produto_db..questionario_tb.questionario_id  
 ,questionario = als_produto_db..questionario_tb.nome  
 ,num_ordem_pergunta  
 ,NULL  
 ,''  
 ,NULL  
FROM #proposta  
JOIN als_operacao_db..PRTD_RSCO_CTR_CONCATENADO_VW WITH (NOLOCK) ON als_operacao_db..PRTD_RSCO_CTR_CONCATENADO_VW.CD_PRD = cod_produto  
 AND als_operacao_db..PRTD_RSCO_CTR_CONCATENADO_VW.CD_MDLD = cod_modalidade  
 AND als_operacao_db..PRTD_RSCO_CTR_CONCATENADO_VW.CD_ITEM_MDLD = cod_item_modalidade  
 AND als_operacao_db..PRTD_RSCO_CTR_CONCATENADO_VW.NR_CTR_SGRO = num_contrato_seguro  
 AND als_operacao_db..PRTD_RSCO_CTR_CONCATENADO_VW.NR_VRS_EDS = num_versao_endosso  
JOIN als_produto_db..pergunta_concatenada_vw WITH (NOLOCK) ON als_produto_db..pergunta_concatenada_vw.pergunta_id = als_operacao_db..PRTD_RSCO_CTR_CONCATENADO_VW.NR_QST  
 AND als_produto_db..pergunta_concatenada_vw.grupo_dominio_resposta_id IS NULL  
JOIN als_produto_db..questionario_tb WITH (NOLOCK) ON als_produto_db..questionario_tb.questionario_id = als_operacao_db..PRTD_RSCO_CTR_CONCATENADO_VW.NR_QSTN  
JOIN als_produto_db..questionario_pergunta_tb WITH (NOLOCK) ON als_produto_db..questionario_pergunta_tb.questionario_id = als_produto_db..questionario_tb.questionario_id  
 AND als_produto_db..questionario_pergunta_tb.pergunta_id = als_produto_db..pergunta_concatenada_vw.pergunta_id  
WHERE (als_operacao_db..PRTD_RSCO_CTR_CONCATENADO_VW.CD_TIP_QSTN = 1)  
 OR (  
  als_operacao_db..PRTD_RSCO_CTR_CONCATENADO_VW.CD_TIP_QSTN IN (  
   91  
   ,92  
   ) --BNB  
  AND canal_id = 3  
  )  
  
UNION  
  
SELECT seq_proposta  
 ,tipo_questionario = 'O'  
 ,pergunta_id = als_produto_db..pergunta_concatenada_vw.pergunta_id  
 ,pergunta = als_produto_db..pergunta_concatenada_vw.nome  
 ,resposta_id = 0  
 ,resposta = als_operacao_db..PRTD_RSCO_CTR_CONCATENADO_VW.TX_DCR_ATB_CTR  
 ,questionario_id = als_produto_db..questionario_tb.questionario_id  
 ,questionario = als_produto_db..questionario_tb.nome  
 ,num_ordem_pergunta  
 ,als_operacao_db..PRTD_RSCO_CTR_CONCATENADO_VW.NR_SEQL_QSTN_CTR  
 ,als_operacao_db..PRTD_RSCO_CTR_CONCATENADO_VW.TX_CMPT_RPST_CTR  
 ,NULL  
FROM #proposta  
JOIN als_operacao_db..PRTD_RSCO_CTR_CONCATENADO_VW WITH (NOLOCK) ON als_operacao_db..PRTD_RSCO_CTR_CONCATENADO_VW.CD_PRD = cod_produto  
 AND als_operacao_db..PRTD_RSCO_CTR_CONCATENADO_VW.CD_MDLD = cod_modalidade  
 AND als_operacao_db..PRTD_RSCO_CTR_CONCATENADO_VW.CD_ITEM_MDLD = cod_item_modalidade  
 AND als_operacao_db..PRTD_RSCO_CTR_CONCATENADO_VW.NR_CTR_SGRO = num_contrato_seguro  
 AND als_operacao_db..PRTD_RSCO_CTR_CONCATENADO_VW.NR_VRS_EDS = num_versao_endosso  
JOIN als_produto_db..pergunta_concatenada_vw WITH (NOLOCK) ON als_produto_db..pergunta_concatenada_vw.pergunta_id = als_operacao_db..PRTD_RSCO_CTR_CONCATENADO_VW.NR_QST  
 AND ISNULL(als_produto_db..pergunta_concatenada_vw.grupo_dominio_resposta_id, 0) = 0  
JOIN als_produto_db..questionario_tb WITH (NOLOCK) ON als_produto_db..questionario_tb.questionario_id = als_operacao_db..PRTD_RSCO_CTR_CONCATENADO_VW.NR_QSTN  
JOIN als_produto_db..questionario_pergunta_tb WITH (NOLOCK) ON als_produto_db..questionario_pergunta_tb.questionario_id = als_produto_db..questionario_tb.questionario_id  
 AND als_produto_db..questionario_pergunta_tb.pergunta_id = als_produto_db..pergunta_concatenada_vw.pergunta_id  
WHERE (  
  als_operacao_db..PRTD_RSCO_CTR_CONCATENADO_VW.CD_TIP_QSTN IN (  
   2  
   ,4  
   )  
  )  
 OR (  
  als_operacao_db..PRTD_RSCO_CTR_CONCATENADO_VW.CD_TIP_QSTN = 94 --BNB  
  AND canal_id = 3  
  )  
  
UNION  
  
SELECT seq_proposta  
 ,tipo_questionario = 'C'  
 ,pergunta_id = als_produto_db..pergunta_concatenada_vw.pergunta_id  
 ,pergunta = als_produto_db..pergunta_concatenada_vw.nome  
 ,resposta_id = 0  
 ,resposta = als_operacao_db..PRTD_RSCO_CTR_CONCATENADO_VW.TX_DCR_ATB_CTR  
 ,questionario_id = als_produto_db..questionario_tb.questionario_id  
 ,questionario = als_produto_db..questionario_tb.nome  
 ,num_ordem_pergunta  
 ,als_operacao_db..PRTD_RSCO_CTR_CONCATENADO_VW.NR_SEQL_QSTN_CTR  
 ,''  
 ,als_operacao_db..PRTD_RSCO_CTR_CONCATENADO_VW.NR_CD_TIP_QSTN_CTR  
FROM #proposta  
JOIN als_operacao_db..PRTD_RSCO_CTR_CONCATENADO_VW WITH (NOLOCK) ON als_operacao_db..PRTD_RSCO_CTR_CONCATENADO_VW.CD_PRD = cod_produto  
 AND als_operacao_db..PRTD_RSCO_CTR_CONCATENADO_VW.CD_MDLD = cod_modalidade  
 AND als_operacao_db..PRTD_RSCO_CTR_CONCATENADO_VW.CD_ITEM_MDLD = cod_item_modalidade  
 AND als_operacao_db..PRTD_RSCO_CTR_CONCATENADO_VW.NR_CTR_SGRO = num_contrato_seguro  
 AND als_operacao_db..PRTD_RSCO_CTR_CONCATENADO_VW.NR_VRS_EDS = num_versao_endosso  
JOIN als_produto_db..pergunta_concatenada_vw WITH (NOLOCK) ON als_produto_db..pergunta_concatenada_vw.pergunta_id = als_operacao_db..PRTD_RSCO_CTR_CONCATENADO_VW.NR_QST  
 AND als_produto_db..pergunta_concatenada_vw.grupo_dominio_resposta_id IS NULL  
JOIN als_produto_db..questionario_tb WITH (NOLOCK) ON als_produto_db..questionario_tb.questionario_id = als_operacao_db..PRTD_RSCO_CTR_CONCATENADO_VW.NR_QSTN  
JOIN als_produto_db..questionario_pergunta_tb WITH (NOLOCK) ON als_produto_db..questionario_pergunta_tb.questionario_id = als_produto_db..questionario_tb.questionario_id  
 AND als_produto_db..questionario_pergunta_tb.pergunta_id = als_produto_db..pergunta_concatenada_vw.pergunta_id  
WHERE als_operacao_db..PRTD_RSCO_CTR_CONCATENADO_VW.CD_TIP_QSTN = 3  
  
UNION  
  
SELECT seq_proposta  
 ,tipo_questionario = 'R'  
 ,pergunta_id = als_produto_db..pergunta_concatenada_vw.pergunta_id  
 ,pergunta = als_produto_db..pergunta_concatenada_vw.nome  
 ,resposta_id = 0  
 ,resposta = als_operacao_db..PRTD_RSCO_CTR_CONCATENADO_VW.TX_DCR_ATB_CTR  
 ,questionario_id = als_produto_db..questionario_tb.questionario_id  
 ,questionario = als_produto_db..questionario_tb.nome  
 ,num_ordem_pergunta  
 ,als_operacao_db..PRTD_RSCO_CTR_CONCATENADO_VW.NR_SEQL_QSTN_CTR  
 ,als_operacao_db..PRTD_RSCO_CTR_CONCATENADO_VW.TX_CMPT_RPST_CTR  
 ,als_operacao_db..PRTD_RSCO_CTR_CONCATENADO_VW.NR_CD_TIP_QSTN_CTR  
FROM #proposta  
JOIN als_operacao_db..PRTD_RSCO_CTR_CONCATENADO_VW WITH (NOLOCK) ON als_operacao_db..PRTD_RSCO_CTR_CONCATENADO_VW.CD_PRD = cod_produto  
 AND als_operacao_db..PRTD_RSCO_CTR_CONCATENADO_VW.CD_MDLD = cod_modalidade  
 AND als_operacao_db..PRTD_RSCO_CTR_CONCATENADO_VW.CD_ITEM_MDLD = cod_item_modalidade  
 AND als_operacao_db..PRTD_RSCO_CTR_CONCATENADO_VW.NR_CTR_SGRO = num_contrato_seguro  
 AND als_operacao_db..PRTD_RSCO_CTR_CONCATENADO_VW.NR_VRS_EDS = num_versao_endosso  
JOIN als_produto_db..pergunta_concatenada_vw WITH (NOLOCK) ON als_produto_db..pergunta_concatenada_vw.pergunta_id = als_operacao_db..PRTD_RSCO_CTR_CONCATENADO_VW.NR_QST  
 AND als_produto_db..pergunta_concatenada_vw.grupo_dominio_resposta_id IS NULL  
JOIN als_produto_db..questionario_tb WITH (NOLOCK) ON als_produto_db..questionario_tb.questionario_id = als_operacao_db..PRTD_RSCO_CTR_CONCATENADO_VW.NR_QSTN  
JOIN als_produto_db..questionario_pergunta_tb WITH (NOLOCK) ON als_produto_db..questionario_pergunta_tb.questionario_id = als_produto_db..questionario_tb.questionario_id  
 AND als_produto_db..questionario_pergunta_tb.pergunta_id = als_produto_db..pergunta_concatenada_vw.pergunta_id  
WHERE als_operacao_db..PRTD_RSCO_CTR_CONCATENADO_VW.CD_TIP_QSTN = 5  
ORDER BY seq_proposta  
 ,tipo_questionario  
 ,als_produto_db..questionario_pergunta_tb.num_ordem_pergunta  
  

INSERT INTO #questionario (    
 seq_proposta    
 ,tipo_questionario    
 ,pergunta_id    
 ,pergunta    
 ,resposta_id    
 ,resposta    
 ,questionario_id    
 ,questionario    
 ,num_ordem_pergunta    
 ,cod_objeto_segurado    
 ,texto_complementar    
 ,tp_cobertura_id    
 )    
SELECT seq_proposta    
 ,tipo_questionario    
 ,pergunta_id    
 ,pergunta    
 ,resposta_id    
 ,resposta    
 ,questionario_id    
 ,questionario    
 ,num_ordem_pergunta    
 ,cod_objeto_segurado    
 ,texto_complementar    
 ,tp_cobertura_id    
FROM #questionario_determinado    
    
UNION    
    
SELECT seq_proposta    
 ,tipo_questionario    
 ,pergunta_id    
 ,pergunta    
 ,resposta_id    
 ,resposta    
 ,questionario_id    
 ,questionario    
 ,num_ordem_pergunta    
 ,cod_objeto_segurado    
 ,texto_complementar    
 ,tp_cobertura_id    
FROM #questionario_indeterminado    
ORDER BY seq_proposta    
 ,tipo_questionario    
 ,num_ordem_pergunta      
 
 
 INSERT INTO #dados_financeiros (    
 forma_pgto_id    
 ,seq_proposta    
 ,seguro_moeda_id    
 ,num_parcelas    
 ,conta_corrente    
 ,dia_debito    
 ,val_premio_tarifa    
 ,val_juros    
 ,perc_comissao    
 ,custo_apolice    
 ,val_desconto_tecnico    
 ,val_desconto_comercial    
 ,perc_estipulante    
 ,val_is    
 ,val_bruto    
 ,val_pgto_ato    
 ,val_parcela    
 ,val_paridade_moeda    
 ,taxa_juros    
 ,seguradora_cod_susep    
 ,sucursal_seguradora_id    
 ,perc_corretagem    
 ,perc_iof    
 ,perc_desconto_tecnico    
 ,agencia_id    
 ,deb_agencia_id    
 ,cd_tip_periodo_pag     
 ,val_premio_liquido    
 )    
SELECT CD_FMA_PGTO forma_pgto_id    
 ,seq_proposta    
 ,ISNULL(CD_MOE_SGRO_CTR, 790) seguro_moeda_id --temporrio retirar      
 ,QT_PCL_PGTO_CTR num_parcelas    
 ,NR_CC_DEB conta_corrente    
 ,DD_DEB_CBR_PREM dia_debito    
 ,ISNULL(round((VL_LQDO_MOEN_CTR - isnull(VL_JUR_PCLT_CTR, 0)), 2, 1), 0) val_premio_tarifa    
 ,round(isnull(VL_JUR_PCLT_CTR, 0), 2, 1) val_juros    
 ,PC_CRE_CTR / 100 perc_comissao    
 ,ISNULL(round(VL_CST_APLC_CTR, 2, 1), 0) custo_apolice    
 ,ISNULL(VL_DSC_TCN_CTR, 0) val_desconto_tecnico    
 ,ISNULL(VL_DSC_CML_CTR, 0) val_desconto_comercial    
 ,PC_ETLE_CTR / 100 perc_estipulante    
 ,ISNULL(round(VL_IPTC_MOEN_CTR, 2, 1), 0) val_is    
 ,ISNULL(round(VL_PREM_MOEN_CTR, 2, 1), 0) val_bruto    
 ,CASE     
  WHEN ISNULL(IN_PG_ATO_CTR, 'N') = 'N'    
   THEN 0    
  ELSE ROUND(VL_PRMO_MOEN_CTR, 2, 1)    
  END val_pgto_ato    
 ,CASE     
  WHEN ISNULL(IN_PG_ATO_CTR, 'N') = 'N'    
   AND QT_PCL_PGTO_CTR = 1    
   THEN ISNULL(round(VL_PRMO_MOEN_CTR, 2, 1), 0)    
  ELSE ISNULL(round(VL_PCL_MOEN_CTR, 2, 1), 0)    
  END val_parcela    
 ,ISNULL(round(VL_CTC_USD_CTR, 7, 1), 0) val_paridade_moeda    
 ,ISNULL(round(PC_JUR_PCLT_CTR, 4, 1), 0) taxa_juros    
 ,6785 seguradora_cod_susep    
 ,0 sucursal_seguradora_id    
 ,PC_CRE_CTR perc_corretagem    
 ,ISNULL(round(PC_IOF_CTR, 2, 1) / 100, 0) perc_iof    
 ,PC_DSC_TCN_CTR perc_desconto_tecnico    
 ,CASE     
  WHEN CD_FMA_PGTO = 1    
   THEN #proposta.cont_agencia_id    
  ELSE NULL    
  END agencia_id    
    ,DADO_FNCO_CTR.NR_AG_DEB deb_agencia_id    
 ,DADO_FNCO_CTR.CD_TIP_PERC_PGTO cd_tip_periodo_pag -- Periodicidade de pagamento     
 ,ISNULL(ROUND((VL_LQDO_MOEN_CTR - ISNULL(VL_JUR_PCLT_CTR, 0)), 2, 1), 0) val_premio_liquido    
FROM #proposta    
LEFT JOIN als_operacao_db.dbo.DADO_FNCO_CTR DADO_FNCO_CTR WITH (NOLOCK) --, INDEX = PK_DADO_FNCO_CTR)      
 ON DADO_FNCO_CTR.CD_PRD = #proposta.cod_produto    
 AND DADO_FNCO_CTR.CD_MDLD = #proposta.cod_modalidade    
 AND DADO_FNCO_CTR.CD_ITEM_MDLD = #proposta.cod_item_modalidade    
 AND DADO_FNCO_CTR.NR_CTR_SGRO = #proposta.num_contrato_seguro    
 AND DADO_FNCO_CTR.NR_VRS_EDS = #proposta.num_versao_endosso  
 
 INSERT INTO #estipulacao (    
 est_cliente_id    
 ,perc_pro_labore    
 ,seq_proposta    
 )    
SELECT representacao_tp_seguro_tb.est_cliente_id    
 ,ISNULL(representacao_tp_seguro_tb.perc_comissao_estipulante, 0) perc_pro_labore    
 ,#proposta.seq_proposta    
FROM #proposta          
JOIN seguros_db.dbo.representacao_tp_seguro_tb representacao_tp_seguro_tb WITH (NOLOCK)     
ON representacao_tp_seguro_tb.produto_id = #proposta.produto_id    
  
    
 
 
INSERT INTO #dados_agricola_faturamento (    
seq_proposta    
, cultura               
, UF_propriedade        
, prod_esperada         
, perc_nivel_cobert     
, area_segurada         
, val_custeio           
, dt_execucao           
, preco_base            
, perc_max_resp         
, area_total            
, irrigada              
, cd_prd_agrl           
, municipio_BACEN_id    
, fator_replantio       
, val_preco_bmf         
, perc_desagio          
, preco_base_calc       
, fat_esperado_calc     
)       
SELECT #proposta.seq_proposta     
     , cultura              = CAST(MAX(CASE WHEN pergunta_id = 3546 THEN resposta ELSE NULL END) AS VARCHAR(100))    
     , UF_propriedade       = CAST(MAX(CASE WHEN pergunta_id = 5824 THEN resposta ELSE NULL END) AS VARCHAR(2))    
     , prod_esperada        = CAST(MAX(CASE WHEN pergunta_id = 5825 THEN resposta ELSE NULL END) AS NUMERIC(20,6))    
     , perc_nivel_cobert    = CAST(MAX(CASE WHEN pergunta_id = 7738 THEN REPLACE(REPLACE(resposta,',','.'),'%','') ELSE NULL END) AS NUMERIC(20,14))    
     , area_segurada        = CAST(MAX(CASE WHEN pergunta_id = 3057 THEN REPLACE(resposta,',','.') ELSE NULL END) AS NUMERIC(20,6))    
     , val_custeio          = CAST(MAX(CASE WHEN pergunta_id = 2884 THEN REPLACE(resposta,',','.') ELSE NULL END) AS NUMERIC(20,6))    
     , dt_execucao          = CAST(MAX(CASE WHEN pergunta_id = 7737 THEN REPLACE(REPLACE(REPLACE(resposta,'.',''),'/',''),'-','') ELSE NULL END) AS VARCHAR(10))    
     , preco_base           = CAST(MAX(CASE WHEN pergunta_id = 7740 THEN REPLACE(resposta,',','.') ELSE NULL END) AS NUMERIC(20,6))    
     , perc_max_resp        = CAST(MAX(CASE WHEN pergunta_id = 7739 THEN REPLACE(resposta,',','.') ELSE NULL END) AS NUMERIC(20,14))    
     , area_total           = CAST(MAX(CASE WHEN pergunta_id = 3058 THEN REPLACE(resposta,',','.') ELSE NULL END) AS NUMERIC(20,6))    
     , irrigada             = CAST(MAX(CASE WHEN pergunta_id = 2817 THEN resposta ELSE NULL END) AS VARCHAR(20))    
     , cd_prd_agrl          = CAST(NULL AS INT)    
     , municipio_BACEN_id             = CAST(MAX(CASE WHEN pergunta_id = 3055 THEN texto_complementar ELSE NULL END) AS VARCHAR(20))    
     , fator_replantio      = CAST(NULL AS NUMERIC(20,6))    
     , val_preco_bmf        = CAST(NULL AS NUMERIC(15,2))    
     , perc_desagio         = CAST(NULL AS NUMERIC(20,6))    
     , preco_base_calc      = CAST(NULL AS NUMERIC(15,2))    
     , fat_esperado_calc    = CAST(NULL AS NUMERIC(15,2))     
FROM #questionario      
JOIN #proposta      
  ON #questionario.seq_proposta = #proposta.seq_proposta      
WHERE pergunta_id IN (3546,5824,5825,7738,3057,2884,7737,7740,7739,3058,2817,3055)    
GROUP BY #proposta.seq_proposta   


 
 INSERT INTO #cobertura (    
 tp_cobertura_id    
 ,seq_proposta    
 ,val_risco_cobertura    
 ,fat_taxa    
 ,val_premio_cobertura    
 ,fat_desconto    
 ,fat_franquia    
 ,texto_franquia    
 ,cod_objeto_segurado    
 ,plano_id    
 ,dt_inicio_vigencia_plano    
 )    
SELECT DISTINCT CBT_CTR.CD_CBT    
 ,#proposta.seq_proposta    
 ,CBT_CTR.VL_IPTC_CBT_CTR    
 ,ROUND((CBT_CTR.PC_PREM_CBT_CTR / (1 - ((ISNULL(perc_corretagem, 0) + ISNULL(perc_pro_labore, 0)) / 100))) / 100, 7, 1)    
 ,ROUND(CBT_CTR.VL_PREM_CBT_CTR / (1 - ((ISNULL(perc_corretagem, 0) + ISNULL(perc_pro_labore, 0)) / 100)), 2, 1)    
 ,CBT_CTR.PC_DSC_TCN_CBT_CTR    
 ,CBT_CTR.PC_FRQU_CTR    
 ,CBT_CTR.TX_FRQU_CTR    
 ,NR_SEQL_QSTN_CTR = ISNULL(PRTD_RSCO_CTR.NR_SEQL_QSTN_CTR, 1)    
 ,CBT_CTR.CD_CJT_CBT    
 ,plano_tb.dt_inicio_vigencia dt_inicio_vigencia_plano    
FROM #proposta    
LEFT JOIN #dados_financeiros     
ON #dados_financeiros.seq_proposta = #proposta.seq_proposta    
LEFT JOIN #estipulacao     
ON #estipulacao.seq_proposta = #proposta.seq_proposta    
JOIN als_operacao_db.dbo.CBT_CTR CBT_CTR WITH (NOLOCK)     
ON CBT_CTR.CD_PRD = cod_produto    
 AND CBT_CTR.CD_MDLD = cod_modalidade    
 AND CBT_CTR.CD_ITEM_MDLD = cod_item_modalidade    
 AND CBT_CTR.NR_CTR_SGRO = num_contrato_seguro    
 AND CBT_CTR.NR_VRS_EDS = num_versao_endosso    
LEFT JOIN seguros_db.dbo.plano_tb plano_tb WITH (NOLOCK)     
ON plano_tb.plano_id = CBT_CTR.CD_CJT_CBT    
 AND plano_tb.produto_id = #proposta.produto_id    
 AND plano_tb.dt_fim_vigencia IS NULL    
LEFT JOIN als_operacao_db.dbo.PRTD_RSCO_CTR PRTD_RSCO_CTR WITH (NOLOCK)     
ON PRTD_RSCO_CTR.CD_PRD = CBT_CTR.CD_PRD    
 AND PRTD_RSCO_CTR.CD_MDLD = CBT_CTR.CD_MDLD    
 AND PRTD_RSCO_CTR.CD_ITEM_MDLD = CBT_CTR.CD_ITEM_MDLD    
 AND PRTD_RSCO_CTR.NR_CTR_SGRO = CBT_CTR.NR_CTR_SGRO    
 AND PRTD_RSCO_CTR.NR_VRS_EDS = CBT_CTR.NR_VRS_EDS    
 AND PRTD_RSCO_CTR.CD_TIP_QSTN = 2    
 AND PRTD_RSCO_CTR.NR_SEQL_QSTN_CTR = CASE     
  WHEN CBT_CTR.NR_SEQL_CBT_CTR = 9999    
   THEN PRTD_RSCO_CTR.NR_SEQL_QSTN_CTR    
  ELSE CBT_CTR.NR_SEQL_CBT_CTR    
  END    
WHERE #proposta.produto_id NOT IN (1241, 1220, 1221, 1222, 1223, 1224,1242) -- Incluindo produtos 1220, 1221, 1222, 1223 e 1224 para calculo diferenciado    
  
  
INSERT INTO #dados_cobertura_agricola_faturamento (    
       seq_proposta,     
       tp_cobertura_id,    
       cod_objeto_segurado,    
       plano_id,    
       cd_prd_agrl,    
       dt_execucao,    
       UF_propriedade,    
       municipio_bacen_id,    
       perc_nivel_cobert,    
       prod_esperada,    
       fator_replantio,    
   val_preco_bmf,    
  perc_desagio,    
       preco_base_calc,    
       fat_esperado_calc,    
       area_segurada,    
       val_custeio,    
       taxa,    
       val_cobertura_calc,    
       val_premio_cobertura_calc,    
       IN_IRG_PRD_AGRL,    
       lim_max_cbt_fat_calc,    
       lim_max_cbt_repl_calc    
)    
SELECT c.seq_proposta,     
       c.tp_cobertura_id,    
       c.cod_objeto_segurado,    
       c.plano_id,    
       cd_prd_agrl,    
       dt_execucao = CAST(SUBSTRING(dt_execucao,5,4)+SUBSTRING(dt_execucao,3,2)+SUBSTRING(dt_execucao,1,2) AS SMALLDATETIME),    
       UF_propriedade,    
       municipio_bacen_id,    
       perc_nivel_cobert,    
       prod_esperada,    
       fator_replantio   = CAST(NULL AS NUMERIC(20,14)),    
       val_preco_bmf     = CAST(NULL AS NUMERIC(15,4)),    
       perc_desagio      = CAST(NULL AS NUMERIC(20,6)),    
       preco_base_calc   = CAST(NULL AS NUMERIC(15,2)),    
       fat_esperado_calc = CAST(NULL AS NUMERIC(15,2)),    
       area_segurada,    
       val_custeio,    
       taxa = CAST(NULL AS NUMERIC(20,14)),    
       val_cobertura_calc = CAST(NULL AS NUMERIC(15,2)),    
       val_premio_cobertura_calc = CAST(NULL AS NUMERIC(15,2)),    
       IN_IRG_PRD_AGRL = CASE WHEN UPPER(irrigada) = 'SIM' THEN 'S' ELSE 'N' END,    
       lim_max_cbt_fat_calc = CAST(NULL AS NUMERIC(15,2)),    
       lim_max_cbt_repl_calc = CAST(NULL AS NUMERIC(15,2))      
FROM #cobertura c    
JOIN #dados_agricola_faturamento f    
  ON f.seq_proposta = c.seq_proposta   
 
 
 
UPDATE #dados_cobertura_agricola_faturamento    
SET taxa = a.PC_CLC_SGRO_AGRL,    
    fator_replantio = a.DET_FT_CBT,    
    perc_desagio = a.DET_PC_DSG  --SELECT a.*    
FROM #dados_cobertura_agricola_faturamento f    
JOIN #proposta p    
  ON p.seq_proposta = f.seq_proposta    
JOIN als_produto_db..PC_CLC_SGRO_AGRL a WITH(NOLOCK)   
  ON a.CD_CBT = f.tp_cobertura_id    
 AND a.IN_IRG_PRD_AGRL = f.IN_IRG_PRD_AGRL    
 AND a.CD_ITEM_MDLD    = p.cod_item_modalidade    
 AND a.CD_MUN_PRD_AGRL = f.municipio_bacen_id      
 AND a.CD_PC_PRDD_SGRV = f.perc_nivel_cobert    
    
UPDATE #dados_cobertura_agricola_faturamento    
SET taxa = (taxa/100.00) / (1 - (ISNULL(perc_corretagem, 0) + ISNULL(perc_pro_labore, 0)) / 100.00)    
FROM #dados_cobertura_agricola_faturamento c    
LEFT JOIN #dados_financeiros f    
  ON c.seq_proposta = f.seq_proposta    
LEFT JOIN #estipulacao e    
  ON e.seq_proposta = f.seq_proposta    
     
UPDATE a   
SET a.preco_base_calc = isnull(f.preco_base,0) 
from #dados_cobertura_agricola_faturamento a  
JOIN #dados_agricola_faturamento f    
  ON f.seq_proposta = a.seq_proposta       
  
    
UPDATE #dados_cobertura_agricola_faturamento    
SET fat_esperado_calc = (prod_esperada/60) * area_segurada * CASE WHEN preco_base_calc = 0 THEN 1 ELSE preco_base_calc END    
    
update e
set e.val_fat_esperado = f.fat_esperado_calc
from seguros_db.dbo.seguro_esp_agricola_tb e
inner join #proposta a 
on e.proposta_id = a.proposta_id
inner join #dados_cobertura_agricola_faturamento f
ON a.seq_proposta = f.seq_proposta    
