DECLARE @total_registro INT
DECLARE @total_registro_afetados INT
DECLARE @mensagem VARCHAR(500)

SET @total_registro = 1
SET @total_registro_afetados = 0


update seguros_Db.dbo.tp_cob_item_prod_tb 
set cod_ramo_contab = 1
where produto_id = 1152
and ramo_Id = 1
SET @total_registro_afetados = @total_registro_afetados + @@ROWCOUNT


-- Verificação
SELECT @total_registro_afetados

SELECT @total_registro

IF ( (@@SERVERNAME = 'SISAB003' or @@SERVERNAME = 'SISAS003\ABS') AND @total_registro <> @total_registro_afetados )
BEGIN
SET @mensagem = 'qtd de registros que deveriam ser afetados: ' + CONVERT(VARCHAR(10), @total_registro) + 'qtd de registros afetados: ' + CONVERT(VARCHAR(10), @total_registro_afetados)

RAISERROR (@mensagem, 16, 1)
END