DECLARE @total_registro INT
DECLARE @total_registro_afetados  INT
DECLARE @mensagem VARCHAR(500)

SET @total_registro_afetados= 0

/*qtd registro que serao afetados*/
SELECT @total_registro = 1

insert into seguros_db.dbo.parametro_inadimplencia_tb
 (  ramo_id
  ,subramo_id
  ,dt_inicio_vigencia_sbr
  ,dt_inicio_vigencia
  ,dt_fim_vigencia
  ,aguarda_solicitacao_cancelamento_bb
  ,emite_aviso_inadimplencia_primeira_parc
  ,emite_aviso_inadimplencia_demais_parc
  ,dias_para_emissao_aviso
  ,cancela_documento
  ,dias_para_canc_primeira_parc
  ,dias_para_canc_demais_parc
  ,canc_requer_aviso_inadimplencia
  ,canc_aguarda_periodo_cobertura
  ,emite_carta_cancelamento
  ,arquivo_aviso_inadimplencia
  ,arquivo_carta_cancelamento
  ,dias_registro_inadimplencia
  ,canc_parcelas_sem_pagamento
  ,identificacao_inadimplencia_bb
  ,inadimplencia_requer_retorno_bb
  ,tp_criterio_cancelamento
  ,tp_cancelamento
  ,tp_calc_periodo_cobertura
  ,dt_inclusao
  ,dt_alteracao
  ,usuario
  --,lock
  ,produto_id
  ,ident_inadimplencia_bb_primeira_parc
  ,ident_inadimplencia_bb_demais_parc
  ,cancela_agencia_corporate
  ,cancela_corretor_independente
  ,cancela_cliente_private
  ,cancela_endosso
  ,docs_a_cancelar
  ,gera_workflow
  ,motivo_alteracao
  ,qtde_parcelas_carta_inadimplencia
  ,parcelas_consecutivas_inadimplencia
  ,DATA_CORTE
  ,enviar_aviso_grid_com_retorno
  ,enviar_aviso_grid_sem_retorno
  ,enviar_para_grid
  ,cbr_debito_apos_recusa_cc
  ,dias_para_canc_apos_recusa_cc
 
 )
 
select 
  1 ramo_id
 ,102 subramo_id
 ,(SELECT dt_inicio_vigencia_sbr FROM seguros_Db.dbo.subramo_tb WITH (NOLOCK) WHERE subramo_id = 102 and ramo_id = 1 and dt_fim_vigencia_sbr is null) --dt_inicio_vigencia_sbr
 ,(SELECT dt_operacional FROM seguros_db.dbo.parametro_geral_tb WITH (NOLOCK)) --dt_inicio_vigencia
 ,NULL--dt_fim_vigencia
 ,aguarda_solicitacao_cancelamento_bb
 ,emite_aviso_inadimplencia_primeira_parc
 ,emite_aviso_inadimplencia_demais_parc
 ,dias_para_emissao_aviso
 ,cancela_documento
 ,dias_para_canc_primeira_parc
 ,dias_para_canc_demais_parc
 ,canc_requer_aviso_inadimplencia
 ,canc_aguarda_periodo_cobertura
 ,emite_carta_cancelamento
 ,arquivo_aviso_inadimplencia
 ,arquivo_carta_cancelamento
 ,dias_registro_inadimplencia
 ,canc_parcelas_sem_pagamento
 ,identificacao_inadimplencia_bb
,inadimplencia_requer_retorno_bb
,tp_criterio_cancelamento
,tp_cancelamento
,tp_calc_periodo_cobertura
,GETDATE()dt_inclusao
,NULL--dt_alteracao
,'dem_ramo_1152' usuario
---,lock
,produto_id
,ident_inadimplencia_bb_primeira_parc
,ident_inadimplencia_bb_demais_parc
,cancela_agencia_corporate
,cancela_corretor_independente
,cancela_cliente_private
,cancela_endosso
,docs_a_cancelar
,gera_workflow
,motivo_alteracao
,qtde_parcelas_carta_inadimplencia
,parcelas_consecutivas_inadimplencia
,DATA_CORTE
,enviar_aviso_grid_com_retorno
,enviar_aviso_grid_sem_retorno
,enviar_para_grid
,cbr_debito_apos_recusa_cc
,dias_para_canc_apos_recusa_cc
from seguros_Db.dbo.parametro_inadimplencia_tb with(nolock)
where produto_id = 1152
and dt_fim_vigencia is null

SET @total_registro_afetados = @total_registro_afetados + @@rowcount


 IF ((@@servername = 'SISAB003' or @@servername = 'SISAS003\ABS')  and @total_registro <> @total_registro_afetados) 
  BEGIN
    SET @mensagem='qtd de registros que deveriam ser afetados: ' + convert(varchar(10),@total_registro) + 
									 ' qtd de registros afetados: ' + convert(varchar(10),@total_registro_afetados)
    RAISERROR (@mensagem,16,1) 

    END	