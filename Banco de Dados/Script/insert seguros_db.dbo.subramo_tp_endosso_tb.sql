
DECLARE @total_registro INT
DECLARE @total_registro_afetados INT
DECLARE @mensagem VARCHAR(500)
DECLARE @dt_inicio_vigencia_sbr smalldatetime

SET @total_registro = 19
SET @total_registro_afetados = 0

 select @dt_inicio_vigencia_sbr = dt_inicio_vigencia_sbr from subramo_tb
 where subramo_id = 102
 and dt_fim_vigencia_sbr is null


INSERT INTO [dbo].[subramo_tp_endosso_tb]([ramo_id], [subramo_id], [dt_inicio_vigencia_sbr], [tp_endosso_id], [dt_inclusao], [dt_alteracao], [usuario], [permite_restituicao], [dt_inicio_vigencia], [dt_fim_vigencia], [origem_endosso], [automatico], [produto_id])
SELECT 1, 102, @dt_inicio_vigencia_sbr, 266, getdate(), NULL, N'MUDANCARAMOPROD1152', N'S', getdate(), NULL, N'AB', N'N', 1152 UNION ALL
SELECT 1, 102, @dt_inicio_vigencia_sbr, 304, getdate(), NULL, N'MUDANCARAMOPROD1152', N'S', getdate(), NULL, N'AB', N'N', 1152 UNION ALL
SELECT 1, 102, @dt_inicio_vigencia_sbr, 305, getdate(), NULL, N'MUDANCARAMOPROD1152', N'S', getdate(), NULL, N'AB', N'N', 1152 UNION ALL
SELECT 1, 102, @dt_inicio_vigencia_sbr, 262, getdate(), NULL, N'MUDANCARAMOPROD1152', N'N', getdate(), NULL, N'AB', N'N', 1152 UNION ALL
SELECT 1, 102, @dt_inicio_vigencia_sbr, 269, getdate(), NULL, N'MUDANCARAMOPROD1152', N'S', getdate(), NULL, N'AB', N'N', 1152 UNION ALL
SELECT 1, 102, @dt_inicio_vigencia_sbr, 51, getdate(), NULL, N'MUDANCARAMOPROD1152', N'N', getdate(), NULL, N'AB', N'N', 1152 UNION ALL
SELECT 1, 102, @dt_inicio_vigencia_sbr, 61, getdate(), NULL, N'MUDANCARAMOPROD1152', N'N', getdate(), NULL, N'AB', N'N', 1152 UNION ALL
SELECT 1, 102, @dt_inicio_vigencia_sbr, 64, getdate(), NULL, N'MUDANCARAMOPROD1152', N'S', getdate(), NULL, N'AB', N'N', 1152 UNION ALL
SELECT 1, 102, @dt_inicio_vigencia_sbr, 80, getdate(), NULL, N'MUDANCARAMOPROD1152', N'S', getdate(), NULL, N'AB', N'N', 1152 UNION ALL
SELECT 1, 102, @dt_inicio_vigencia_sbr, 90, getdate(), NULL, N'MUDANCARAMOPROD1152', N'S', getdate(), NULL, N'AB', N'N', 1152 UNION ALL
SELECT 1, 102, @dt_inicio_vigencia_sbr, 92, getdate(), NULL, N'MUDANCARAMOPROD1152', N'S', getdate(), NULL, N'AB', N'N', 1152 UNION ALL
SELECT 1, 102, @dt_inicio_vigencia_sbr, 94, getdate(), NULL, N'MUDANCARAMOPROD1152', N'N', getdate(), NULL, N'AB', N'N', 1152 UNION ALL
SELECT 1, 102, @dt_inicio_vigencia_sbr, 101, getdate(), NULL, N'MUDANCARAMOPROD1152', N'N', getdate(), NULL, N'AB', N'N', 1152 UNION ALL
SELECT 1, 102, @dt_inicio_vigencia_sbr, 201, getdate(), NULL, N'MUDANCARAMOPROD1152', N'N', getdate(), NULL, N'AB', N'N', 1152 UNION ALL
SELECT 1, 102, @dt_inicio_vigencia_sbr, 270, getdate(), NULL, N'MUDANCARAMOPROD1152', N'S', getdate(), NULL, N'AB', N'N', 1152 UNION ALL
SELECT 1, 102, @dt_inicio_vigencia_sbr, 300, getdate(), NULL, N'MUDANCARAMOPROD1152', N'N', getdate(), NULL, N'AB', N'N', 1152 UNION ALL
SELECT 1, 102, @dt_inicio_vigencia_sbr, 361, getdate(), NULL, N'MUDANCARAMOPROD1152', N'S', getdate(), NULL, N'AB', N'N', 1152 UNION ALL
SELECT 1, 102, @dt_inicio_vigencia_sbr, 362, getdate(), NULL, N'MUDANCARAMOPROD1152', N'S', getdate(), NULL, N'AB', N'N', 1152 UNION ALL
SELECT 1, 102, @dt_inicio_vigencia_sbr, 370, getdate(), NULL, N'MUDANCARAMOPROD1152', N'N', getdate(), NULL, N'AB', N'N', 1152
SET @total_registro_afetados = @total_registro_afetados + @@ROWCOUNT


-- Verificação
SELECT @total_registro_afetados

SELECT @total_registro

IF ( (@@SERVERNAME = 'SISAB003' or @@SERVERNAME = 'SISAS003\ABS') AND @total_registro <> @total_registro_afetados )
BEGIN
SET @mensagem = 'qtd de registros que deveriam ser afetados: ' + CONVERT(VARCHAR(10), @total_registro) + 'qtd de registros afetados: ' + CONVERT(VARCHAR(10), @total_registro_afetados)

RAISERROR (@mensagem, 16, 1)
END