CREATE PROCEDURE dbo.SEGS5345_SPU
  
@produto_id INT,  
@dt_sistema SMALLDATETIME,  
@usuario VARCHAR(20)  
  
AS   

BEGIN TRY  
  
--Teste  
--DECLARE @produto_id INT,  
--        @dt_sistema SMALLDATETIME,  
--        @usuario VARCHAR(20)  
--  
--SET @produto_id = 1152  
--SET @dt_sistema = GETDATE()  
--SET @usuario = 'PRODUCAO3'  
----------------------------------  
SELECT val_pgto_ato = CASE WHEN ISNULL(IN_PG_ATO_CTR, 'N') = 'N' THEN 0 ELSE ROUND(ROUND(VL_PRMO_MOEN_CTR,3,0),2,0) END,    
       custo_apolice = ISNULL(ROUND(ROUND(VL_CST_APLC_CTR,3,0),2,0),0),  
       seq_proposta,  
       dado_fnco_ctr.cd_prd,  
       dado_fnco_ctr.cd_mdld,  
       dado_fnco_ctr.cd_item_mdld,  
       dado_fnco_ctr.vl_prem_moen_ctr, --lrocha 07/08/2008  
	   produtividade_segurada	= 0
INTO #financeiro_temp1  
FROM #proposta  
JOIN als_operacao_db..dado_fnco_ctr dado_fnco_ctr WITH(NOLOCK)  
  ON dado_fnco_ctr.cd_prd = cod_produto  
 AND dado_fnco_ctr.cd_mdld = cod_modalidade  
 AND dado_fnco_ctr.cd_item_mdld = cod_item_modalidade  
 AND dado_fnco_ctr.nr_ctr_sgro = num_contrato_seguro  
 AND dado_fnco_ctr.nr_vrs_eds = num_versao_endosso  
WHERE produto_id = @produto_id  
  
--bcarneiro - 02/05/2007 - Parametriza��o do percentual de subven��o  
--SELECT val_parcela = val_pgto_ato - custo_apolice,  
--       val_bruto = val_pgto_ato + val_pgto_ato - custo_apolice,  
--       val_premio_tarifa = val_pgto_ato + val_pgto_ato - custo_apolice - custo_apolice,  
--       val_pgto_ato,  
--       custo_apolice,  
--       seq_proposta  
--INTO #financeiro_temp2         
--FROM #financeiro_temp1  
  
--lrocha 14/07/2008 Subven��o Estadual e Federal - IN�CIO  
SELECT #financeiro_temp1.val_pgto_ato,    
       #financeiro_temp1.custo_apolice,  
       #financeiro_temp1.seq_proposta,  
       #financeiro_temp1.cd_prd,  
       #financeiro_temp1.cd_mdld,  
       #financeiro_temp1.cd_item_mdld,  
       cd_prd_agrl = tp_cultura_tb.tp_cultura_id, -- bmoraes 20.08.2009  
       #financeiro_temp1.vl_prem_moen_ctr, --lrocha 07/08/2008  
	   produtividade_segurada	= 0 -- JOAO.MACHADO
INTO #financeiro_temp2  
FROM #financeiro_temp1  
JOIN #objeto    
  ON #objeto.seq_proposta = #financeiro_temp1.seq_proposta    
JOIN #questionario    
  ON #questionario.seq_proposta = #objeto.seq_proposta    
JOIN seguros_db..tp_cultura_tb tp_cultura_tb WITH(NOLOCK) 
  ON RTRIM(tp_cultura_tb.nome) = RTRIM(#questionario.resposta)  
 AND tp_cultura_tb.ativo = 'S'  
WHERE #questionario.tipo_questionario = 'O'     
  -- bcarneiro - 12/08/2008 - A pedido de Eduardo Costa  
  --AND #questionario.pergunta_id = 3278    
  AND #questionario.pergunta_id = 3546    
  ---------------------  
  
---19279696 - ADEQUAR O PROCESSO DE SUBVEN��O - MAPA - JOAO.MACHADO - INICIO
declare @questionario_id_1152 smallint  
declare @questionario_id_1204 smallint  


-- mudanca-de-ramo-produto-1152 - RAFAEL MARTINS (NOVA) 16/06/2020
select @questionario_id_1152 = isnull(max(nr_qstn),0)   
  from als_produto_db..QSTN_ITEM_MDLD_HIST_TB   
 where cd_item_mdld = 102 
   and cd_PRD = 1152
   and dt_fim_qstn is null  

select @questionario_id_1204 = isnull(max(nr_qstn),0)   
  from als_produto_db..QSTN_ITEM_MDLD_HIST_TB   
 where cd_item_mdld = 102 
   and cd_PRD = 1204
   and dt_fim_qstn is null  

UPDATE #financeiro_temp2
   SET produtividade_segurada = convert(int,replace(LTRIM(RTRIM(#questionario.resposta)),'%',''))
  FROM #financeiro_temp2  
  JOIN #objeto    
    ON #objeto.seq_proposta = #financeiro_temp2.seq_proposta    
  JOIN #questionario    
    ON #questionario.seq_proposta = #objeto.seq_proposta    
 WHERE #questionario.pergunta_id in (3280, 2881, 5826) -- produtividade segurada                 
   AND #questionario.questionario_id in (604, 611, 613, 619, 624, 750, 784, @questionario_id_1152, @questionario_id_1204)

---19279696 - ADEQUAR O PROCESSO DE SUBVEN��O - MAPA - JOAO.MACHADO - FIM
				   
--Financeiro Federal  
SELECT #financeiro_temp2.val_pgto_ato,    
       #financeiro_temp2.custo_apolice,  
       #financeiro_temp2.seq_proposta,  
       #financeiro_temp2.cd_prd,  
       #financeiro_temp2.cd_mdld,  
       #financeiro_temp2.cd_item_mdld,  
       #financeiro_temp2.cd_prd_agrl,  
       fator_subvencao_federal = ISNULL(SBS_SGRO_AGRL_HIST_TB.pc_sbs_sgro_agrl / 100, 0), --bmoraes 20.08.2009  
       fator_cliente_federal = (100 - ISNULL(SBS_SGRO_AGRL_HIST_TB.pc_sbs_sgro_agrl, 0)) / 100,  
       #financeiro_temp2.vl_prem_moen_ctr, --lrocha 07/08/2008
	   #financeiro_temp2.produtividade_segurada  -- JOAO.MACHADO
INTO #financeiro_temp3  
FROM #financeiro_temp2  
JOIN #objeto    
  ON #objeto.seq_proposta = #financeiro_temp2.seq_proposta    
JOIN #questionario  
  ON #questionario.seq_proposta = #objeto.seq_proposta    
JOIN als_produto_db..SBS_SGRO_AGRL_HIST_TB SBS_SGRO_AGRL_HIST_TB WITH(NOLOCK)  
  ON SBS_SGRO_AGRL_HIST_TB.cd_prd_bb = #financeiro_temp2.cd_prd  
 AND SBS_SGRO_AGRL_HIST_TB.cd_mdld_bb = #financeiro_temp2.cd_mdld  
 AND SBS_SGRO_AGRL_HIST_TB.cd_item_mdld = #financeiro_temp2.cd_item_mdld  
 AND SBS_SGRO_AGRL_HIST_TB.cd_prd_agrl = #financeiro_temp2.cd_prd_agrl  
 AND SBS_SGRO_AGRL_HIST_TB.cd_tip_sbs_sgro = 1     
 AND SBS_SGRO_AGRL_HIST_TB.DT_INI_SBS_AGRL <= @dt_sistema  
 AND (SBS_SGRO_AGRL_HIST_TB.DT_FIM_SBS_AGRL >= @dt_sistema OR SBS_SGRO_AGRL_HIST_TB.DT_FIM_SBS_AGRL IS NULL)  
 AND SBS_SGRO_AGRL_HIST_TB.sg_uf_sgro_agrl = RTRIM(#questionario.resposta)  
---19279696 - ADEQUAR O PROCESSO DE SUBVEN��O - MAPA - MARCIOMS - INICIO
 AND SBS_SGRO_AGRL_HIST_TB.PERC_NIV_COB = #financeiro_temp2.produtividade_segurada
---19279696 - ADEQUAR O PROCESSO DE SUBVEN��O - MAPA - MARCIOMS - FIM
WHERE #questionario.tipo_questionario = 'O'      
  AND #questionario.pergunta_id in ( 3547, 5824 )  
----------------------------------------------------------------------------  
  
--Financeiro Estadual  
SELECT #financeiro_temp3.val_pgto_ato,    
       #financeiro_temp3.custo_apolice,  
       #financeiro_temp3.seq_proposta,  
       sg_uf_sgro_agrl = RTRIM(#questionario.resposta),  
       #financeiro_temp3.fator_subvencao_federal,  
       #financeiro_temp3.fator_cliente_federal,  
       fator_subvencao_estadual = ISNULL(SBS_SGRO_AGRL_HIST_TB.pc_sbs_sgro_agrl / 100, 0),  
  fator_cliente_estadual = (100 - ISNULL(SBS_SGRO_AGRL_HIST_TB.pc_sbs_sgro_agrl, 0)) / 100,  
       #financeiro_temp3.vl_prem_moen_ctr, --lrocha 07/08/2008  
	   #financeiro_temp3.produtividade_segurada
INTO #financeiro_temp1_perc_subvencao  
FROM #financeiro_temp3  
JOIN #objeto    
  ON #objeto.seq_proposta = #financeiro_temp3.seq_proposta    
JOIN #questionario  
  ON #questionario.seq_proposta = #objeto.seq_proposta    
LEFT JOIN als_produto_db..SBS_SGRO_AGRL_HIST_TB SBS_SGRO_AGRL_HIST_TB WITH(NOLOCK) 
  ON SBS_SGRO_AGRL_HIST_TB.cd_prd_bb = #financeiro_temp3.cd_prd  
 AND SBS_SGRO_AGRL_HIST_TB.cd_mdld_bb = #financeiro_temp3.cd_mdld  
 AND SBS_SGRO_AGRL_HIST_TB.cd_item_mdld = #financeiro_temp3.cd_item_mdld  
 AND SBS_SGRO_AGRL_HIST_TB.cd_prd_agrl = #financeiro_temp3.cd_prd_agrl  
 AND SBS_SGRO_AGRL_HIST_TB.cd_tip_sbs_sgro = 2   
 AND SBS_SGRO_AGRL_HIST_TB.sg_uf_sgro_agrl = RTRIM(#questionario.resposta)  
 AND SBS_SGRO_AGRL_HIST_TB.DT_INI_SBS_AGRL <= @dt_sistema  
 AND (SBS_SGRO_AGRL_HIST_TB.DT_FIM_SBS_AGRL >= @dt_sistema OR SBS_SGRO_AGRL_HIST_TB.DT_FIM_SBS_AGRL IS NULL)  
---19279696 - ADEQUAR O PROCESSO DE SUBVEN��O - MAPA - MARCIOMS - INICIO
 AND SBS_SGRO_AGRL_HIST_TB.PERC_NIV_COB = #financeiro_temp3.produtividade_segurada
---19279696 - ADEQUAR O PROCESSO DE SUBVEN��O - MAPA - MARCIOMS - FIM
WHERE #questionario.tipo_questionario = 'O'     
  -- bcarneiro - 12/08/2008 - A pedido de Eduardo Costa  
  --AND #questionario.pergunta_id = 3279    
  AND #questionario.pergunta_id in ( 3547, 5824 )     
  -----------------------------  
  
--Calculando subven��o federal  
SELECT val_parcela = CONVERT(NUMERIC(9,2),NULL), --((val_pgto_ato - custo_apolice) * (1 / fator_cliente_federal)) * fator_subvencao_federal,  
       val_bruto = ISNULL(vl_prem_moen_ctr, 0), --lrocha 07/08/2008    --#financeiro_temp3.CONVERT(NUMERIC(9,2),NULL), --((val_pgto_ato - custo_apolice) * (1 / fator_cliente_federal)) + custo_apolice,  
       val_premio_tarifa = CONVERT(NUMERIC(9,2),NULL), --(val_pgto_ato - custo_apolice) * (1 / fator_cliente_federal),  
       val_pgto_ato,  
       custo_apolice,  
       seq_proposta,  
       sg_uf_sgro_agrl,  
       fator_subvencao_federal,  
       fator_subvencao_estadual,  
       fator_cliente_federal,  
       fator_cliente_estadual  
INTO #financeiro_temp  
FROM #financeiro_temp1_perc_subvencao  
  
----Calculando subven��o   
UPDATE #financeiro_temp  
   SET val_premio_tarifa = (val_bruto - custo_apolice)  
  
UPDATE #financeiro_temp  
   SET val_parcela = val_premio_tarifa - (val_pgto_ato - custo_apolice)  
  
  
----Calculando subven��o MG  
--UPDATE #financeiro_temp  
--   SET val_parcela = ((val_pgto_ato - custo_apolice) * (1 / fator_cliente_federal + fator_cliente_estadual)) * (fator_subvencao_federal + fator_subvencao_estadual),  
--       val_bruto = ((val_pgto_ato - custo_apolice) * (1 / fator_cliente_federal + fator_cliente_estadual)) + custo_apolice,  
--       val_premio_tarifa = (val_pgto_ato - custo_apolice) * (1 / fator_cliente_federal + fator_cliente_estadual)  
-- WHERE sg_uf_sgro_agrl = 'MG'  
--  
----Calculando subven��o SP  
--UPDATE #financeiro_temp  
--   SET val_parcela = (((val_pgto_ato - custo_apolice) * (1 / fator_cliente_federal) * (1 / fator_cliente_estadual)) * fator_subvencao_federal) * fator_subvencao_estadual,  
--       val_bruto = ((val_pgto_ato - custo_apolice) * (1 / fator_cliente_federal) * (1 / fator_cliente_estadual)) + custo_apolice,  
--       val_premio_tarifa = (val_pgto_ato - custo_apolice) * (1 / fator_cliente_federal) * (1 / fator_cliente_estadual)  
-- WHERE sg_uf_sgro_agrl = 'SP'  
 --lrocha 14/07/2008 Subven��o Estadual e Federal - FIM ----------------------------------------------------------  
 --taxa antiga 0.9107 alterada por 0.8623 em 05/11/09 - ecosta


UPDATE #dados_financeiros  
  SET #dados_financeiros.val_premio_tarifa = #financeiro_temp.val_premio_tarifa,  
      #dados_financeiros.val_bruto = #financeiro_temp.val_bruto,  
      #dados_financeiros.val_pgto_ato = #financeiro_temp.val_pgto_ato,  
      #dados_financeiros.val_parcela = #financeiro_temp.val_parcela,  
      #dados_financeiros.custo_apolice = #financeiro_temp.custo_apolice,  
	  -- Demanda 19745644 - IN�CIO
      --#dados_financeiros.val_bonificacao = (#financeiro_temp.val_premio_tarifa - #financeiro_temp.val_parcela) * 0.8623 * 0.05
	  #dados_financeiros.val_bonificacao = Case When @produto_id = 1152
												Then 0
												Else (#financeiro_temp.val_premio_tarifa - #financeiro_temp.val_parcela) * 0.8623 * 0.05
											End
	  -- Demanda 19745644 - FIM
FROM #financeiro_temp  
JOIN #dados_financeiros  
  ON #dados_financeiros.seq_proposta = #financeiro_temp.seq_proposta  
  
RETURN

END TRY

BEGIN CATCH  
      DECLARE @ErrorMessage  NVARCHAR(4000)        
      DECLARE @ErrorSeverity INT        
      DECLARE @ErrorState    INT  
    
       SELECT @ErrorMessage  = 'Linha ' + CONVERT(VARCHAR(15),ERROR_LINE()) + ' - ' + ERROR_MESSAGE()
	        , @ErrorSeverity = ERROR_SEVERITY()
		    , @ErrorState    = ERROR_STATE()        
  
    RAISERROR ( @ErrorMessage
              , @ErrorSeverity
              , @ErrorState )  
END CATCH


