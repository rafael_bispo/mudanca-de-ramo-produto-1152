CREATE PROCEDURE SEGS5322_SPI
   
 @produto_id INT,                  
 @dt_inclusao SMALLDATETIME,                  
 @usuario VARCHAR(20)                  
                   
AS                    
                  
/*                  
Defini��o: Importa��o ALS para o SEGBR                  
Procedure espec�fica para o Produto Ouro Agr�cola          
    
TESTE    
 
    
select TOP 10 dt_inicio_vigencia = dt_contratacao, *    
  into #proposta     
  from seguros_db..proposta_tb with (nolock)
 where produto_id = 1204    
 UNION    
select TOP 10 dt_inicio_vigencia = dt_contratacao, *    
  from seguros_db..proposta_tb with (nolock)
 where produto_id = 1152    

-- drop table #proposta
 Select * From #proposta
    
alter table #proposta add seq_proposta int identity    
    
CREATE TABLE #questionario (sequencial INT IDENTITY (1, 1),    
                            num_resposta int,    
                            pergunta_id int,    
                            seq_proposta INT,    
                            tipo_questionario CHAR(1),    
                            pergunta varchar (240),    
                            resposta_id smallint,    
                            resposta varchar (600),    
                            questionario_id smallint,    
                            questionario varchar (60),    
                            cod_objeto_segurado INT,    
                            tp_cobertura_id INT,    
                            texto_complementar varchar(40),    
                            num_ordem_pergunta smallint)    
    
CREATE TABLE #objeto (seq_proposta INT,    
                      cod_objeto_segurado INT)    
    
CREATE TABLE #dados_cobertura_agricola_faturamento (       
       seq_proposta                INT            NULL,    
       tp_cobertura_id             INT            NULL,    
       cod_objeto_segurado         INT            NULL,    
       plano_id                    INT            NULL,    
       cd_prd_agrl                 INT            NULL,    
       dt_execucao                 SMALLDATETIME  NULL,    
       UF_propriedade              VARCHAR(2)     NULL,    
       municipio_BACEN_id          INT            NULL,    
       perc_nivel_cobert           NUMERIC(20,14) NULL,    
       prod_esperada               NUMERIC(20,6)  NULL,    
       fator_replantio             NUMERIC(20,6)  NULL,    
       val_preco_bmf               NUMERIC(15,4)  NULL,    
       perc_desagio                NUMERIC(20,6)  NULL,    
       preco_base_calc             NUMERIC(15,4)  NULL,    
       fat_esperado_calc           NUMERIC(15,2)  NULL,    
       area_segurada               NUMERIC(20,6)  NULL,    
       val_custeio                 NUMERIC(20,6)  NULL,    
       perc_max_resp               NUMERIC(20,14) NULL,    
       taxa                        NUMERIC(20,14) NULL,    
       val_cobertura_calc          NUMERIC(15,2)  NULL,    
       val_premio_cobertura_calc   NUMERIC(15,2)  NULL,    
       IN_IRG_PRD_AGRL             CHAR(1)        NULL,    
       lim_max_cbt_fat_calc        NUMERIC(15,2)  NULL,    
       lim_max_cbt_repl_calc       NUMERIC(15,2)  NULL     
)    

Altera��es:
Petrauskas - 11/2019 
  linhas 923 e 924 - inclu�do filtro de aceite do banco do arquivo de desconto/agravo

*/                
            
BEGIN TRY       
      
   DECLARE @msg AS VARCHAR(4000)    
  
 /* [Inicio] - Jessica.Adao - Confitec sistemas - 20160317 - Projeto 19279696 : Subvencao Mapa */         
 DECLARE @SiglaTarefa VARCHAR(10)          
 DECLARE @Atividade VARCHAR(255)          
 DECLARE @Horario_Inicio_Tarefa DATETIME   
  
 SET @SiglaTarefa = 'SEGX0695'  

 SET @Atividade = 'PRODUTO: ' + CONVERT(VARCHAR, @produto_id) + ' - INICIO PROCESSAMENTO'
 SET @Horario_Inicio_Tarefa = GETDATE()   
 EXEC controle_sistema_db.dbo.SEGS8602_SPI @SiglaTarefa, @dt_inclusao, @Atividade, @Horario_Inicio_Tarefa, NULL, @Usuario                 

 SET @Atividade = 'PRODUTO: ' + CONVERT(VARCHAR, @produto_id) + ' - SELECIONANDO PROPOSTAS'          
 SET @Horario_Inicio_Tarefa = GETDATE()          
  
 EXEC controle_sistema_db.dbo.SEGS8602_SPI @SiglaTarefa, @dt_inclusao, @Atividade, @Horario_Inicio_Tarefa, NULL, @Usuario    
 /* [Fim] - Jessica.Adao - Confitec sistemas - 20160317 - Projeto 19279696 : Subvencao Mapa */         
       
    --Selecionando as propostas                  
    SELECT distinct #proposta.proposta_id,                  
           #objeto.cod_objeto_segurado,                  
           #proposta.dt_inicio_vigencia,                  
           tp_cultura_id = tp_cultura_tb.tp_cultura_id,                
           -- bmoraes 17/08/2008 novos campos                
           #objeto.seq_proposta,                
           zoneamento = CAST(NULL AS VARCHAR(10)),                 
           area_segurada = CAST(NULL AS VARCHAR(15)),                  
           produtividade_esperada = CAST(NULL AS VARCHAR(10)),          
           produtividade_segurada = CAST(NULL AS VARCHAR(10)),                 
           nome_propriedade = CAST(NULL AS VARCHAR(60)),                 
           nr_contrato_financiamento = CAST(NULL AS VARCHAR(20)),                
           dt_vencimento_contrato = CAST(NULL AS VARCHAR(10)),                 
           cultivo = CAST(NULL AS VARCHAR(30)),                   
           irrigacao = CAST(NULL AS VARCHAR(10)),                  
           val_orcamento = CAST(NULL AS VARCHAR(15)),                  
           area_tot_a_ser_plantada = CAST(NULL AS VARCHAR(15)),                 
           dt_inicio_plantio = CAST(NULL AS VARCHAR(10)),              
           -- Ronaldo - BSI 07/07/2010 novos campos                
           grau_lat_propriedade_1 = CAST(NULL AS SMALLINT),              
           min_lat_propriedade_1 = CAST(NULL AS SMALLINT),              
           seg_lat_propriedade_1 = CAST(NULL AS SMALLINT),              
           grau_long_propriedade_1 = CAST(NULL AS SMALLINT),              
           min_long_propriedade_1 = CAST(NULL AS SMALLINT),              
           seg_long_propriedade_1 = CAST(NULL AS SMALLINT),            
                
           dt_execucao_seguro = CAST(NULL AS SMALLDATETIME),            
           perc_nivel_cobertura = CAST(NULL AS NUMERIC(14,11)),            
           val_preco_base_bmf = CAST(NULL AS NUMERIC(15,4)),            
           perc_max_resp = CAST(NULL AS NUMERIC(14,11)),          
              
           lmi_cob_fat       = CAST(NULL AS NUMERIC(15,2)),                                  
           lmi_cob_repl      = CAST(NULL AS NUMERIC(15,2)),                 
           val_fat_esperado  = CAST(NULL AS NUMERIC(15,2)),                                  
           fat_taxa_cob_fat  = CAST(NULL AS NUMERIC(8,7)),                                  
           fat_taxa_cob_repl = CAST(NULL AS NUMERIC(8,7)),          
           mdld_certificacao_organica = CAST(NULL AS VARCHAR(60)),  -- Demanda 14934473 - Andrea.Pontes - 18.07.2012                 
           beneficiario_pronamp = CAST(NULL AS VARCHAR(10)),        -- Demanda 14934473 - Andrea.Pontes - 18.07.2012      
           municipio_critico    = CAST(NULL AS varchar(3)),    
           pc_sub_mun_critico   = CAST(NULL AS numeric(7,4)),       -- FLOW 17854738 - petrauskas - 05.2013    
           pc_sub_pronamp       = CAST(NULL AS numeric(7,4)),       -- FLOW 17854738 - petrauskas - 05.2013    
           pc_sub_organico      = CAST(NULL AS numeric(7,4))        -- FLOW 17854738 - petrauskas - 05.2013    
               
    --------------------------------------------------------------------              
    INTO #seguro_agricola                  
    FROM #proposta                  
    JOIN #objeto                  
      ON #objeto.seq_proposta = #proposta.seq_proposta                  
    JOIN #questionario                  
      ON #questionario.seq_proposta = #objeto.seq_proposta    
    --bcarneiro - 02/05/2007 - Parametriza��o do percentual de subven��o                
    JOIN seguros_db..tp_cultura_tb tp_cultura_tb                
      ON RTRIM(tp_cultura_tb.nome) = RTRIM(#questionario.resposta)                
     AND tp_cultura_tb.ativo = 'S'                
    --------------------------------------------------------------------                
    WHERE #questionario.tipo_questionario = 'O'                   
                
    -- FLOW 233632 - GMARQUES - Confitec Sistemas - 22-08-2008              
    -- ADICIONANDO A PERGUNTA (3427 - Qual � a cultura da lavoura?)              
    -- IN�CIO              
                  
    --   -- FLOW 233632 - MFerreira - Confitec Sistemas - 2007-11-12                
    --   -- As propostas com a pergunta ID 3278 n�o estavam sendo carregadas            
    --   -------------------------------------------------------------------                
    --   --  AND #questionario.pergunta_id = 3277                  
    -- ---> ALTERADO POR JUNIOR EM 24/07/2008                 
    --   AND #questionario.pergunta_id IN (3546, 3277, 3278)                
    -- -------------------------------------------------------------                
                  
      AND #questionario.pergunta_id IN (3546, 3277, 3278, 3427)                
      AND #proposta.produto_id = @produto_id           
      
    -- FIM              
           
 /* Jessica.Adao - Confitec sistemas - 20160317 - Projeto 19279696 : Subvencao Mapa */          
 SET @Atividade = 'PRODUTO: ' + CONVERT(VARCHAR, @produto_id) + ' - PROPOSTAS SELECIONADAS: ' + CONVERT(VARCHAR, @@ROWCOUNT)  
 SET @Horario_Inicio_Tarefa = GETDATE()   
 EXEC controle_sistema_db.dbo.SEGS8602_SPI @SiglaTarefa, @dt_inclusao, @Atividade, @Horario_Inicio_Tarefa, NULL, @Usuario             
            
    -- AGRICOLA FATURAMENTO ----------------------------------------        
                
 UPDATE #seguro_agricola            
    SET dt_execucao_seguro = f.dt_execucao,            
        perc_nivel_cobertura = f.perc_nivel_cobert,            
        val_preco_base_bmf = f.preco_base_calc,            
        perc_max_resp = f.perc_max_resp,          
        val_fat_esperado = f.fat_esperado_calc                                
   FROM #seguro_agricola s            
   JOIN #dados_cobertura_agricola_faturamento f            
     ON s.seq_proposta = f.seq_proposta    
  
 /* Jessica.Adao - Confitec sistemas - 20160317 - Projeto 19279696 : Subvencao Mapa */          
 SET @Atividade = 'PRODUTO: ' + CONVERT(VARCHAR, @produto_id) + ' - PROPOSTAS ATUALIZADAS: ' + CONVERT(VARCHAR, @@ROWCOUNT)  
 SET @Horario_Inicio_Tarefa = GETDATE()   
 EXEC controle_sistema_db.dbo.SEGS8602_SPI @SiglaTarefa, @dt_inclusao, @Atividade, @Horario_Inicio_Tarefa, NULL, @Usuario   
              
 UPDATE #seguro_agricola            
    SET lmi_cob_fat = f.val_cobertura_calc,            
        fat_taxa_cob_fat = CAST(f.taxa AS NUMERIC(8,7))          
   FROM #seguro_agricola s            
   JOIN #dados_cobertura_agricola_faturamento f            
     ON s.seq_proposta = f.seq_proposta            
  WHERE f.tp_cobertura_id = 835         
  
 /* Jessica.Adao - Confitec sistemas - 20160317 - Projeto 19279696 : Subvencao Mapa */          
 SET @Atividade = 'PRODUTO: ' + CONVERT(VARCHAR, @produto_id) + ' - PROPOSTAS ATUALIZADAS - COB 835: ' + CONVERT(VARCHAR, @@ROWCOUNT)  
 SET @Horario_Inicio_Tarefa = GETDATE()   
 EXEC controle_sistema_db.dbo.SEGS8602_SPI @SiglaTarefa, @dt_inclusao, @Atividade, @Horario_Inicio_Tarefa, NULL, @Usuario     
              
 UPDATE #seguro_agricola            
    SET lmi_cob_repl = f.val_cobertura_calc,            
        fat_taxa_cob_repl = CAST(f.taxa AS NUMERIC(8,7))          
   FROM #seguro_agricola s            
   JOIN #dados_cobertura_agricola_faturamento f            
     ON s.seq_proposta = f.seq_proposta            
  WHERE f.tp_cobertura_id = 834    
  
 /* Jessica.Adao - Confitec sistemas - 20160317 - Projeto 19279696 : Subvencao Mapa */          
 SET @Atividade = 'PRODUTO: ' + CONVERT(VARCHAR, @produto_id) + ' - PROPOSTAS ATULIZADAS - COB 834: ' + CONVERT(VARCHAR, @@ROWCOUNT)  
 SET @Horario_Inicio_Tarefa = GETDATE()   
 EXEC controle_sistema_db.dbo.SEGS8602_SPI @SiglaTarefa, @dt_inclusao, @Atividade, @Horario_Inicio_Tarefa, NULL, @Usuario         
              
    -- Obter o id do questionario do Agr�cola - Andrea.Pontes - 18.07.2012      
    declare @questionario_id_1152 smallint      
    select @questionario_id_1152 = isnull(max(nr_qstn),0)       
      from als_produto_db..QSTN_ITEM_MDLD_HIST_TB       
     where cd_item_mdld = 102 
	   and cd_PRD = 1152
       and dt_fim_qstn is null      
      
    declare @questionario_id_1204 smallint      
    select @questionario_id_1204 = isnull(max(nr_qstn),0)       
      from als_produto_db..QSTN_ITEM_MDLD_HIST_TB       
     where cd_item_mdld = 102 
       and cd_PRD = 1204
       and dt_fim_qstn is null      
      
    -- AGRICOLA FATURAMENTO ----------------------------------------        
                 
    -- bmoraes 14/08/2008 - atualizando novos campos                
    update #seguro_agricola                
    set zoneamento = LTRIM(RTRIM(q1.resposta)),                  
        area_segurada = LTRIM(RTRIM(q2.resposta)),                  
        produtividade_esperada = LTRIM(RTRIM(q3.resposta)),                 
        produtividade_segurada = LTRIM(RTRIM(q4.resposta)),                 
        nome_propriedade = LTRIM(RTRIM(q5.resposta)),        
        nr_contrato_financiamento = LTRIM(RTRIM(q6.resposta)),                
        dt_vencimento_contrato = LTRIM(RTRIM(q7.resposta)),                 
        cultivo = LTRIM(RTRIM(q8.resposta)),                   
        irrigacao = LTRIM(RTRIM(q9.resposta)),                  
        val_orcamento = LTRIM(RTRIM(q10.resposta)),                  
        area_tot_a_ser_plantada = LTRIM(RTRIM(q11.resposta)),                 
        dt_inicio_plantio = LTRIM(RTRIM(q12.resposta)),                 
        -- Ronaldo - BSI 07/07/2010 novos campos                
        grau_lat_propriedade_1 = LTRIM(RTRIM(q13.resposta)),                 
        min_lat_propriedade_1 = LTRIM(RTRIM(q14.resposta)),                 
        seg_lat_propriedade_1 = LTRIM(RTRIM(q15.resposta)),                 
        grau_long_propriedade_1 = LTRIM(RTRIM(q16.resposta)),                 
        min_long_propriedade_1 = LTRIM(RTRIM(q17.resposta)),                 
        seg_long_propriedade_1 = LTRIM(RTRIM(q18.resposta)),          
        mdld_certificacao_organica = LTRIM(RTRIM(q19.resposta)),      
        beneficiario_pronamp = LTRIM(RTRIM(q20.resposta))          
    FROM #questionario q1                
    LEFT JOIN (SELECT q2.*                       
               FROM #questionario q2 (NOLOCK)                      
               WHERE q2.pergunta_id = 3057 -- area segurada                     
                 AND q2.questionario_id in (604, 611, 613, 619, 624, 750, 784, @questionario_id_1152, @questionario_id_1204)) q2                       
      ON q2.seq_proposta = q1.seq_proposta                      
    LEFT JOIN (SELECT q3.*                       
               FROM #questionario q3 (NOLOCK)                      
               WHERE q3.pergunta_id in (3056, 5825) -- produtividade esperada                 
                 AND q3.questionario_id in (604, 611, 613, 619, 624, 750, 784, @questionario_id_1152, @questionario_id_1204)) q3                       
      ON q3.seq_proposta = q1.seq_proposta                  
    LEFT JOIN (SELECT q4.*                       
               FROM #questionario q4 (NOLOCK)                      
               WHERE q4.pergunta_id in (3280, 2881, 5826) -- produtividade segurada                     
                 AND q4.questionario_id in (604, 611, 613, 619, 624, 750, 784, @questionario_id_1152, @questionario_id_1204)) q4                       
      ON q4.seq_proposta = q1.seq_proposta                
    LEFT JOIN (SELECT q5.*                     
               FROM #questionario q5 (NOLOCK)                      
               WHERE q5.pergunta_id in (3053, 2784) -- nome propriedade                     
                 AND q5.questionario_id in (604, 611, 613, 619, 624, 750, 784, @questionario_id_1152, @questionario_id_1204)) q5                       
      ON q5.seq_proposta = q1.seq_proposta                
    LEFT JOIN (SELECT q6.*                 
               FROM #questionario q6 (NOLOCK)                      
               WHERE q6.pergunta_id = 3059 -- numero contrato financiamento                    
                 AND q6.questionario_id in (604, 611, 613, 619, 624, 750, 784, @questionario_id_1152, @questionario_id_1204)) q6                       
      ON q6.seq_proposta = q1.seq_proposta                
    LEFT JOIN (SELECT q7.*                       
               FROM #questionario q7 (NOLOCK)                      
               WHERE q7.pergunta_id = 2826 -- data vencimento contrato                
                 AND q7.questionario_id in (604, 611, 613, 619, 624, 750, 784, @questionario_id_1152, @questionario_id_1204)) q7                       
      ON q7.seq_proposta = q1.seq_proposta                
    LEFT JOIN (SELECT q8.*                       
               FROM #questionario q8 (NOLOCK)                      
               WHERE q8.pergunta_id = 2816 -- cultivo                  
                 AND q8.questionario_id in (604, 611, 613, 619, 624, 750, 784, @questionario_id_1152, @questionario_id_1204)) q8                       
      ON q8.seq_proposta = q1.seq_proposta                
 LEFT JOIN (SELECT q9.*                       
               FROM #questionario q9 (NOLOCK)                      
               WHERE q9.pergunta_id = 2817 -- irrigacao                  
                 AND q9.questionario_id in (604, 611, 613, 619, 624, 750, 784, @questionario_id_1152, @questionario_id_1204)) q9                       
      ON q9.seq_proposta = q1.seq_proposta                
    LEFT JOIN (SELECT q10.*                       
               FROM #questionario q10 (NOLOCK)                      
               WHERE q10.pergunta_id = 2884 -- valor orcamento                  
                 AND q10.questionario_id in (604, 611, 613, 619, 624, 750, 784, @questionario_id_1152, @questionario_id_1204)) q10                       
      ON q10.seq_proposta = q1.seq_proposta                
    LEFT JOIN (SELECT q11.*                       
               FROM #questionario q11 (NOLOCK)                      
               WHERE q11.pergunta_id = 3058 -- area total a ser plantada                  
                 AND q11.questionario_id in (604, 611, 613, 619, 624, 750, 784, @questionario_id_1152, @questionario_id_1204)) q11                       
      ON q11.seq_proposta = q1.seq_proposta                
    LEFT JOIN (SELECT q12.*                       
               FROM #questionario q12 (NOLOCK)                      
               WHERE q12.pergunta_id = 2829 -- data inicio plantio                  
                 AND q12.questionario_id in (604, 611, 613, 619, 624)) q12                       
      ON q12.seq_proposta = q1.seq_proposta                
    LEFT JOIN (SELECT q13.*                       
               FROM #questionario q13 (NOLOCK)                      
               WHERE q13.pergunta_id = 7538 -- grau da latitude do ponto georefernciado da propriedade 1              
                 AND q13.questionario_id IN (@questionario_id_1152, @questionario_id_1204)) q13                       
      ON q13.seq_proposta = q1.seq_proposta                
    LEFT JOIN (SELECT q14.*                       
               FROM #questionario q14 (NOLOCK)                      
               WHERE q14.pergunta_id = 7539 -- minuto da latitude do ponto georefernciado da propriedade 1              
                 AND q14.questionario_id IN (@questionario_id_1152, @questionario_id_1204)) q14                       
      ON q14.seq_proposta = q1.seq_proposta                
    LEFT JOIN (SELECT q15.*                       
               FROM #questionario q15 (NOLOCK)                      
               WHERE q15.pergunta_id = 7540 -- segundo da latitude do ponto georefernciado da propriedade 1              
                 AND q15.questionario_id IN (@questionario_id_1152, @questionario_id_1204)) q15                       
      ON q15.seq_proposta = q1.seq_proposta                
    LEFT JOIN (SELECT q16.*                       
               FROM #questionario q16 (NOLOCK)                      
               WHERE q16.pergunta_id = 7541 -- grau da longitude do ponto georefernciado da propriedade 1              
                 AND q16.questionario_id IN (@questionario_id_1152, @questionario_id_1204)) q16                       
      ON q16.seq_proposta = q1.seq_proposta                
    LEFT JOIN (SELECT q17.*                       
               FROM #questionario q17 (NOLOCK)                      
               WHERE q17.pergunta_id = 7542 -- minuto da longitude do ponto georefernciado da propriedade 1              
                 AND q17.questionario_id IN (@questionario_id_1152, @questionario_id_1204)) q17                       
      ON q17.seq_proposta = q1.seq_proposta                
    LEFT JOIN (SELECT q18.*                       
               FROM #questionario q18 (NOLOCK)                      
               WHERE q18.pergunta_id = 7543 -- segundo da longitude do ponto georefernciado da propriedade 1              
                 AND q18.questionario_id IN (@questionario_id_1152, @questionario_id_1204)) q18                       
      ON q18.seq_proposta = q1.seq_proposta                
    LEFT JOIN (SELECT q19.*                       
               FROM #questionario q19 (NOLOCK)                      
               WHERE q19.pergunta_id = 11142 -- Informe a modalidade de certificac�o org�nica:  - Demanda 14934473 - Andrea.Pontes - 18.07.2012      
                 AND q19.questionario_id IN (@questionario_id_1152, @questionario_id_1204)) q19                       
      ON q19.seq_proposta = q1.seq_proposta              
    LEFT JOIN (SELECT q20.*                       
               FROM #questionario q20 (NOLOCK)                      
               WHERE q20.pergunta_id = 11143 -- O produtor � benefici�rio do PRONAMP - Programa Nacional de Apoio ao M�dio Produtor Rural? - Demanda 14934473 - Andrea.Pontes - 18.07.2012      
                 AND q20.questionario_id IN (@questionario_id_1152, @questionario_id_1204)) q20                       
      ON q20.seq_proposta = q1.seq_proposta             
    JOIN #seguro_agricola (NOLOCK)                      
      ON q1.seq_proposta = #seguro_agricola.seq_proposta                      
    WHERE q1.pergunta_id = 2771 -- zoneamento                      
      AND q1.questionario_id in (604, 611, 613, 619, 624, 750, 784, @questionario_id_1152, @questionario_id_1204)   
  
 /* Jessica.Adao - Confitec sistemas - 20160317 - Projeto 19279696 : Subvencao Mapa */          
 SET @Atividade = 'PRODUTO: ' + CONVERT(VARCHAR, @produto_id) + ' - NOVOS CAMPOS ATUALIZADOS: ' + CONVERT(VARCHAR, @@ROWCOUNT)  
 SET @Horario_Inicio_Tarefa = GETDATE()   
 EXEC controle_sistema_db.dbo.SEGS8602_SPI @SiglaTarefa, @dt_inclusao, @Atividade, @Horario_Inicio_Tarefa, NULL, @Usuario           
            
/***********************************************    
 *** FLOW 17854738 - petrauskas - 05.2013    
 ***/    
     
--IF @produto_id = 1152 OR @produto_id =1204    
-- BEGIN    
 /*    
 JOAO.MACHADO    
 07/03/2016    
 DEMANDA: 19279696 - Adequar o processo de Subven��o - MAPA    
 DESCRI��O: O nivel de cobertura vem de diferente lugares para os produtos 1204 e 1152    
 */  

if (object_id('tempdb.dbo.#ajuste_webserver') > 0)
begin
	drop table #ajuste_webserver
end

create 
 table #ajuste_webserver
	 ( proposta_id numeric(9)
	 , seq_proposta int
	 , pc_sbs_critico numeric(10,7)
	 , pc_sbs_pronamp numeric(10,7)
	 , pc_sbs_organico numeric(10,7) )
  
IF @produto_id = 1152
BEGIN
	INSERT
	  INTO #ajuste_webserver 
		 ( proposta_id
		 , seq_proposta
		 , pc_sbs_critico
		 , pc_sbs_pronamp
		 , pc_sbs_organico )
    SELECT distinct #proposta.proposta_id,                    
           #proposta.seq_proposta,                    
           prdd.PC_SBS_CRITICO,      
           prdd.PC_SBS_PRONAMP,      
           prdd.PC_SBS_ORGANICO      
--    INTO #ajuste_webserver    
    FROM #proposta    
    join #seguro_agricola seg_esp (nolock)                  on #proposta.proposta_id = seg_esp.proposta_id                
    join als_produto_db..SBS_SGRO_AGRL_HIST_TB sbs (nolock) on sbs.CD_PRD_AGRL = seg_esp.tp_cultura_id                
                                                           and sbs.DT_FIM_SBS_AGRL is null                
                                                           and sbs.CD_TIP_SBS_SGRO = 1              
                                                           AND sbs.CD_PRD = #proposta.produto_id --petrauskas 18/06/2013 -- seg_esp.produto_id     
                 and sbs.PERC_NIV_COB = convert(int,replace(seg_esp.produtividade_segurada,'%',''))    
    join seguros_db..endereco_risco_tb er (nolock)                      on er.proposta_id = seg_esp.proposta_id              
                                                           and er.estado = sbs.SG_UF_SGRO_AGRL              
                                                           and end_risco_id = 1       
    JOIN als_produto_db..PRDD_SGRV_PRD_AGRL prdd (nolock)   ON sbs.CD_PRD_BB = prdd.CD_PRD      
                                                           AND sbs.CD_MDLD = prdd.CD_MDLD      
                                                           AND sbs.CD_ITEM_MDLD = prdd.CD_ITEM_MDLD      
                                                           AND sbs.CD_PRD_AGRL = prdd.CD_PRD_AGRL      
    JOIN #questionario q                                    on q.seq_proposta = #proposta.seq_proposta      
                                                           and prdd.CD_MUN_PRD_AGRL = LTRIM(RTRIM(q.texto_complementar))      
                                                           and q.pergunta_id = 3055      
                                                           and q.questionario_id in (@questionario_id_1152)      
    JOIN seguros_db..proposta_processo_susep_tb pps (nolock)            on pps.proposta_id = #proposta.proposta_id      
    JOIN als_operacao_db..cbt_ctr ctr (nolock)              on ctr.nr_ctr_sgro = pps.num_contrato_seguro      
                                                           and ctr.cd_mdld = pps.cod_modalidade      
                                                           and ctr.cd_item_mdld = pps.cod_item_modalidade      
                                                           and ctr.cd_prd = pps.cod_produto       
                                                           AND ctr.CD_CBT = prdd.CD_CBT      
    WHERE #proposta.produto_id = @produto_id    
END   
 
IF @produto_id = 1204
BEGIN
     
	INSERT
	  INTO #ajuste_webserver 
		 ( proposta_id
		 , seq_proposta
		 , pc_sbs_critico
		 , pc_sbs_pronamp
		 , pc_sbs_organico )
	SELECT distinct #proposta.proposta_id,                    
           #proposta.seq_proposta,                    
           prdd.PC_SBS_CRITICO,      
           prdd.PC_SBS_PRONAMP,      
           prdd.PC_SBS_ORGANICO 
    FROM #proposta    
    join #seguro_agricola seg_esp (nolock)                  on #proposta.proposta_id = seg_esp.proposta_id                
    join als_produto_db..SBS_SGRO_AGRL_HIST_TB sbs (nolock) on sbs.CD_PRD_AGRL = seg_esp.tp_cultura_id                
                                                           and sbs.DT_FIM_SBS_AGRL is null                
                                                           and sbs.CD_TIP_SBS_SGRO = 1              
                                                           AND sbs.CD_PRD = #proposta.produto_id --petrauskas 18/06/2013 -- seg_esp.produto_id     
                 and sbs.PERC_NIV_COB = convert(int,seg_esp.perc_nivel_cobertura)    
    join seguros_db..endereco_risco_tb er (nolock)          on er.proposta_id = seg_esp.proposta_id              
                                                           and er.estado = sbs.SG_UF_SGRO_AGRL              
                                and end_risco_id = 1       
    JOIN als_produto_db..PRDD_SGRV_PRD_AGRL prdd (nolock)   ON sbs.CD_PRD_BB = prdd.CD_PRD      
                                                           AND sbs.CD_MDLD = prdd.CD_MDLD      
                                                           AND sbs.CD_ITEM_MDLD = prdd.CD_ITEM_MDLD      
                                                           AND sbs.CD_PRD_AGRL = prdd.CD_PRD_AGRL      
    JOIN #questionario q                                    on q.seq_proposta = #proposta.seq_proposta      
                                                           and prdd.CD_MUN_PRD_AGRL = LTRIM(RTRIM(q.texto_complementar))      
                                                           and q.pergunta_id = 3055      
                                                           and q.questionario_id in (@questionario_id_1204)      
    JOIN seguros_db..proposta_processo_susep_tb pps (nolock) on pps.proposta_id = #proposta.proposta_id      
    JOIN als_operacao_db..cbt_ctr ctr (nolock)              on ctr.nr_ctr_sgro = pps.num_contrato_seguro      
                                                           and ctr.cd_mdld = pps.cod_modalidade      
                                                           and ctr.cd_item_mdld = pps.cod_item_modalidade      
                                                           and ctr.cd_prd = pps.cod_produto       
                                                           AND ctr.CD_CBT = prdd.CD_CBT      
    WHERE #proposta.produto_id = @produto_id 
END     
  
 /* Jessica.Adao - Confitec sistemas - 20160317 - Projeto 19279696 : Subvencao Mapa */          
 SET @Atividade = 'PRODUTO: ' + CONVERT(VARCHAR, @produto_id) + ' - NIVEIS DE COB. SELECIONADAS: ' + CONVERT(VARCHAR, @@ROWCOUNT)  
 SET @Horario_Inicio_Tarefa = GETDATE()   
 EXEC controle_sistema_db.dbo.SEGS8602_SPI @SiglaTarefa, @dt_inclusao, @Atividade, @Horario_Inicio_Tarefa, NULL, @Usuario     
        
    UPDATE #seguro_agricola     
    SET municipio_critico  = CASE WHEN ISNULL(ws.PC_SBS_CRITICO,0)=0 THEN 'N�O' ELSE 'SIM' END,    
        pc_sub_mun_critico = ws.PC_SBS_CRITICO,    
        pc_sub_pronamp     = CASE WHEN s.beneficiario_pronamp = 'SIM' THEN ws.PC_SBS_PRONAMP ELSE 0 END,    
        pc_sub_organico    = CASE WHEN s.cultivo = 'Org�nico' THEN ws.PC_SBS_ORGANICO ELSE 0 END    
    FROM #seguro_agricola s    
    INNER JOIN #ajuste_webserver ws ON s.seq_proposta = ws.seq_proposta    
  
 /* Jessica.Adao - Confitec sistemas - 20160317 - Projeto 19279696 : Subvencao Mapa */          
 SET @Atividade = 'PRODUTO: ' + CONVERT(VARCHAR, @produto_id) + ' - NIVEIS DE COB. ATUALIZADOS: ' + CONVERT(VARCHAR, @@ROWCOUNT)  
 SET @Horario_Inicio_Tarefa = GETDATE()   
 EXEC controle_sistema_db.dbo.SEGS8602_SPI @SiglaTarefa, @dt_inclusao, @Atividade, @Horario_Inicio_Tarefa, NULL, @Usuario    
    
-- END    
/***    
 *** FIM - FLOW 17854738 - petrauskas - 05.2013    
 ***********************************************/    
    
                    
    --Incluindo em seguro_esp_agricola_tb                  
    INSERT INTO seguros_db.dbo.seguro_esp_agricola_tb (          
           proposta_id,           
  cod_objeto_segurado,                  
           dt_inicio_vigencia_seg,                  
           tp_cultura_id,                       
           seq_canc_endosso_seg,                  
           -- Luciana - 26/12/2007                
           produto_id,                
           dt_inclusao,                  
           usuario,                
           -- bmoraes 14/08/2008                
           zoneamento,                  
           area_segurada,                  
           produtividade_esperada,                 
           produtividade_segurada,                 
           nome_propriedade,                 
           nr_contrato_financiamento,                
           dt_vencimento_contrato,                 
           cultivo,                   
           irrigacao,                  
           val_orcamento,                  
           area_tot_a_ser_plantada,                 
           dt_inicio_plantio,                 
           -- Ronaldo - BSI 07/07/2010 novos campos                
           grau_lat_propriedade_1,                 
           min_lat_propriedade_1,                 
           seg_lat_propriedade_1,                 
           grau_long_propriedade_1,                 
           min_long_propriedade_1,                 
           seg_long_propriedade_1,          
           --pfiche - agricola_faturamento            
           dt_execucao_seguro,            
           perc_nivel_cobertura,            
           val_preco_base_bmf,            
           perc_max_resp,            
           lmi_cob_fat,          
           lmi_cob_repl,          
           val_fat_esperado,          
           fat_taxa_cob_fat,          
           fat_taxa_cob_repl,               
           mdld_certificacao_organica,      
           beneficiario_pronamp,    
           municipio_critico,   -- FLOW 17854738 - petrauskas - 05.2013    
           pc_sub_mun_critico,  -- FLOW 17854738 - petrauskas - 05.2013    
           pc_sub_pronamp,      -- FLOW 17854738 - petrauskas - 05.2013    
           pc_sub_organico      -- FLOW 17854738 - petrauskas - 05.2013    
               
    )                  
    SELECT distinct proposta_id,                  
           cod_objeto_segurado,                  
           dt_inicio_vigencia,                  
           tp_cultura_id,                
           0,                  
           -- Luciana - 26/12/2007                
           @produto_id,                
           @dt_inclusao,                  
           @usuario,                
           -- bmoraes 14/08/2008                
           zoneamento,                  
           area_segurada,                  
           produtividade_esperada,                 
           produtividade_segurada,                 
           nome_propriedade,                 
           nr_contrato_financiamento,                
           dt_vencimento_contrato,                
           cultivo,                   
           irrigacao,                  
           val_orcamento,                  
           area_tot_a_ser_plantada,                 
           dt_inicio_plantio,              
           -- Ronaldo - BSI 07/07/2010 novos campos      
           grau_lat_propriedade_1,                 
           min_lat_propriedade_1,                 
           seg_lat_propriedade_1,                 
           grau_long_propriedade_1,                 
           min_long_propriedade_1,                 
           seg_long_propriedade_1,          
           --pfiche - agricola_faturamento            
           dt_execucao_seguro,            
           perc_nivel_cobertura,            
           val_preco_base_bmf,            
           perc_max_resp,            
           lmi_cob_fat,          
           lmi_cob_repl,          
           val_fat_esperado,          
  fat_taxa_cob_fat,          
           fat_taxa_cob_repl,                 
           mdld_certificacao_organica,      
           beneficiario_pronamp,    
           municipio_critico,           -- FLOW 17854738 - petrauskas - 05.2013    
           pc_sub_mun_critico,          -- FLOW 17854738 - petrauskas - 05.2013    
           pc_sub_pronamp,              -- FLOW 17854738 - petrauskas - 05.2013    
           pc_sub_organico              -- FLOW 17854738 - petrauskas - 05.2013    
    FROM  #seguro_agricola     
  
 /* Jessica.Adao - Confitec sistemas - 20160317 - Projeto 19279696 : Subvencao Mapa */          
 SET @Atividade = 'PRODUTO: ' + CONVERT(VARCHAR, @produto_id) + ' - PROPOSTAS INCLUSAS EM SEGURO_ESP_AGRICOLA_TB: ' + CONVERT(VARCHAR, @@ROWCOUNT)  
 SET @Horario_Inicio_Tarefa = GETDATE()   
 EXEC controle_sistema_db.dbo.SEGS8602_SPI @SiglaTarefa, @dt_inclusao, @Atividade, @Horario_Inicio_Tarefa, NULL, @Usuario                 
               
    --- SUBVENCAO                
    
 /*    
 JOAO.MACHADO    
 07/03/2016    
 DEMANDA: 19279696 - Adequar o processo de Subven��o - MAPA    
 DESCRI��O: O nivel de cobertura vem de diferente lugares para os produtos 1204 e 1152    
 */   

if (object_id('tempdb.dbo.#subvencao_pc') > 0)
begin
	drop table #subvencao_pc
end

create 
 table #subvencao_pc
	 ( proposta_id numeric(9)
	 , seq_proposta int
	 , val_premio_tarifa numeric(15, 2)
	 , pc_sbs_sgro_agrl numeric (10,7)
	 , pc_sbs_critico numeric(10,7)
	 , pc_sbs_pronamp numeric(10,7)
	 , pc_sbs_organico numeric(10,7)
	 , beneficiario_pronamp varchar(10)
	 , cultivo varchar(30) )      

IF @produto_id = 1152
BEGIN                
    --Selecionando propostas do Agr�cola
insert 
  into #subvencao_pc
	 ( proposta_id 
	 , seq_proposta 
	 , val_premio_tarifa 
	 , pc_sbs_sgro_agrl 
	 , pc_sbs_critico 
	 , pc_sbs_pronamp 
	 , pc_sbs_organico 
	 , beneficiario_pronamp 
	 , cultivo )                     
  SELECT distinct #proposta.proposta_id,                    
         #proposta.seq_proposta,                    
         --#dados_financeiros.val_parcela                    
         -- Autor: pablo.dias (Nova Consultoria) - Data da Altera��o: 22/08/2012      
         -- Demanda: 15839547 - Item: Defini��o do novo C�lculo da Subven��o Federal      
         -- Alterar c�lculo do percentual de subven��o      
         #dados_financeiros.val_premio_tarifa,      
         sbs.PC_SBS_SGRO_AGRL,      
         prdd.PC_SBS_CRITICO,      
         prdd.PC_SBS_PRONAMP,      
         prdd.PC_SBS_ORGANICO,      
         seg_esp.beneficiario_pronamp,      
         seg_esp.cultivo      
         -- FIM - pablo.dias - 22/08/2012      
--    INTO #subvencao_pc                    
    FROM #proposta                    
    JOIN #dados_financeiros                    
      ON #dados_financeiros.seq_proposta = #proposta.seq_proposta                    
    join seguros_db..seguro_esp_agricola_tb seg_esp (nolock)                
      on #proposta.proposta_id = seg_esp.proposta_id                
    join als_produto_db..SBS_SGRO_AGRL_HIST_TB sbs (nolock)      
      on sbs.CD_PRD_AGRL = seg_esp.tp_cultura_id                
     and sbs.DT_FIM_SBS_AGRL is null                
     and sbs.CD_TIP_SBS_SGRO = 1              
  AND sbs.CD_PRD = #proposta.produto_id            
---Demanda 19279696 - Processo de Subven��o - MAPA - MARCIOMS - INICIO    
  and sbs.PERC_NIV_COB = convert(int,replace(seg_esp.produtividade_segurada,'%',''))    
---Demanda 19279696 - Processo de Subven��o - MAPA - MARCIOMS - FIM    
    --bmoraes 03.03.2009 - selecionar o percentual de subven��o federal de acordo com o estado              
    join seguros_db..endereco_risco_tb er (nolock)               
      on er.proposta_id = seg_esp.proposta_id              
     and er.estado = sbs.SG_UF_SGRO_AGRL              
     and end_risco_id = 1       
 -- Autor: pablo.dias (Nova Consultoria) - Data da Altera��o: 22/08/2012      
 -- Demanda: 15839547 - Item: Defini��o do novo C�lculo da Subven��o Federal      
 -- Retornar dados da subven��o federal               
       
    -- pablo.dias (Nova Consultoria) - 10/09/2012 - Demanda: 15839547      
    -- Utilizar tabela PRDD_SGRV_PRD_AGRL no lugar da PRDD_SGRV_PRD_AGRL_HIST_TB      
    JOIN als_produto_db..PRDD_SGRV_PRD_AGRL prdd (nolock)      
      ON sbs.CD_PRD_BB = prdd.CD_PRD      
     AND sbs.CD_MDLD = prdd.CD_MDLD      
     AND sbs.CD_ITEM_MDLD = prdd.CD_ITEM_MDLD      
     AND sbs.CD_PRD_AGRL = prdd.CD_PRD_AGRL      
     --and prdd.DT_FIM_PRDD_AGRL is null      
     -- FIM - pablo.dias - 10/09/2012      
           
         -- Obter a cobertura      
    JOIN seguros_db..proposta_processo_susep_tb pps (nolock)      
      on pps.proposta_id = #proposta.proposta_id      
    join als_operacao_db..cbt_ctr ctr (nolock)      
      on ctr.nr_ctr_sgro = pps.num_contrato_seguro      
     and ctr.cd_mdld = pps.cod_modalidade      
     and ctr.cd_item_mdld = pps.cod_item_modalidade      
     and ctr.cd_prd = pps.cod_produto       
     AND ctr.CD_CBT = prdd.CD_CBT      
         -- Obter o munic�pio BACEN      
    join #questionario q      
      on q.seq_proposta = #proposta.seq_proposta      
     and prdd.CD_MUN_PRD_AGRL = LTRIM(RTRIM(q.texto_complementar))      
     and q.pergunta_id = 3055      
     and q.questionario_id in (@questionario_id_1152)      
         -- FIM - pablo.dias - 22/08/2012      
   WHERE #proposta.produto_id = @produto_id
END                    
    
--   union    
IF @produto_id = 1204
BEGIN   
insert 
  into #subvencao_pc
	 ( proposta_id 
	 , seq_proposta 
	 , val_premio_tarifa 
	 , pc_sbs_sgro_agrl 
	 , pc_sbs_critico 
	 , pc_sbs_pronamp 
	 , pc_sbs_organico 
	 , beneficiario_pronamp 
	 , cultivo )     
   SELECT distinct #proposta.proposta_id,                    
         #proposta.seq_proposta,                    
         --#dados_financeiros.val_parcela                    
         -- Autor: pablo.dias (Nova Consultoria) - Data da Altera��o: 22/08/2012      
         -- Demanda: 15839547 - Item: Defini��o do novo C�lculo da Subven��o Federal      
         -- Alterar c�lculo do percentual de subven��o      
         #dados_financeiros.val_premio_tarifa,      
         sbs.PC_SBS_SGRO_AGRL,      
         prdd.PC_SBS_CRITICO,      
         prdd.PC_SBS_PRONAMP,      
         prdd.PC_SBS_ORGANICO,      
         seg_esp.beneficiario_pronamp,      
         seg_esp.cultivo      
         -- FIM - pablo.dias - 22/08/2012      
    FROM #proposta                    
    JOIN #dados_financeiros                    
      ON #dados_financeiros.seq_proposta = #proposta.seq_proposta                    
    join seguros_db..seguro_esp_agricola_tb seg_esp (nolock)                
      on #proposta.proposta_id = seg_esp.proposta_id                
    join als_produto_db..SBS_SGRO_AGRL_HIST_TB sbs (nolock)      
      on sbs.CD_PRD_AGRL = seg_esp.tp_cultura_id                
     and sbs.DT_FIM_SBS_AGRL is null                
     and sbs.CD_TIP_SBS_SGRO = 1              
  AND sbs.CD_PRD = #proposta.produto_id            
---Demanda 19279696 - Processo de Subven��o - MAPA - MARCIOMS - INICIO    
  and sbs.PERC_NIV_COB = convert(int, seg_esp.perc_nivel_cobertura)    
---Demanda 19279696 - Processo de Subven��o - MAPA - MARCIOMS - FIM    
    --bmoraes 03.03.2009 - selecionar o percentual de subven��o federal de acordo com o estado              
    join seguros_db..endereco_risco_tb er (nolock)               
      on er.proposta_id = seg_esp.proposta_id              
     and er.estado = sbs.SG_UF_SGRO_AGRL              
     and end_risco_id = 1       
 -- Autor: pablo.dias (Nova Consultoria) - Data da Altera��o: 22/08/2012      
 -- Demanda: 15839547 - Item: Defini��o do novo C�lculo da Subven��o Federal      
 -- Retornar dados da subven��o federal 
       
    -- pablo.dias (Nova Consultoria) - 10/09/2012 - Demanda: 15839547      
    -- Utilizar tabela PRDD_SGRV_PRD_AGRL no lugar da PRDD_SGRV_PRD_AGRL_HIST_TB      
    JOIN als_produto_db..PRDD_SGRV_PRD_AGRL prdd (nolock)      
      ON sbs.CD_PRD_BB = prdd.CD_PRD      
     AND sbs.CD_MDLD = prdd.CD_MDLD      
     AND sbs.CD_ITEM_MDLD = prdd.CD_ITEM_MDLD      
     AND sbs.CD_PRD_AGRL = prdd.CD_PRD_AGRL      
     --and prdd.DT_FIM_PRDD_AGRL is null      
     -- FIM - pablo.dias - 10/09/2012      
           
         -- Obter a cobertura      
    JOIN seguros_db..proposta_processo_susep_tb pps (nolock)      
      on pps.proposta_id = #proposta.proposta_id      
    join als_operacao_db..cbt_ctr ctr (nolock)      
      on ctr.nr_ctr_sgro = pps.num_contrato_seguro      
     and ctr.cd_mdld = pps.cod_modalidade      
     and ctr.cd_item_mdld = pps.cod_item_modalidade      
     and ctr.cd_prd = pps.cod_produto       
     AND ctr.CD_CBT = prdd.CD_CBT      
         -- Obter o munic�pio BACEN      
    join #questionario q      
      on q.seq_proposta = #proposta.seq_proposta      
     and prdd.CD_MUN_PRD_AGRL = LTRIM(RTRIM(q.texto_complementar))      
     and q.pergunta_id = 3055      
     and q.questionario_id in (@questionario_id_1204)      
         -- FIM - pablo.dias - 22/08/2012      
   WHERE #proposta.produto_id = @produto_id
END   
  
 /* Jessica.Adao - Confitec sistemas - 20160317 - Projeto 19279696 : Subvencao Mapa */          
 SET @Atividade = 'PRODUTO: ' + CONVERT(VARCHAR, @produto_id) + ' - PROPOSTAS DO AGRICOLA: ' + CONVERT(VARCHAR, @@ROWCOUNT)  
 SET @Horario_Inicio_Tarefa = GETDATE()   
 EXEC controle_sistema_db.dbo.SEGS8602_SPI @SiglaTarefa, @dt_inclusao, @Atividade, @Horario_Inicio_Tarefa, NULL, @Usuario     
                   
-- Autor: pablo.dias (Nova Consultoria) - Data da Altera��o: 22/08/2012      
-- Demanda: 15839547 - Item: Defini��o do novo C�lculo da Subven��o Federal      
-- Obter valores dos percentuais de subven��o      
SELECT distinct proposta_id,      
       seq_proposta,      
       val_premio_tarifa,      
       PC_SBS_SGRO_AGRL,      
       PC_SBS_CRITICO,      
       PC_SBS_PRONAMP = CASE       
            WHEN beneficiario_pronamp = 'SIM'       
            THEN PC_SBS_PRONAMP       
            ELSE 0.0       
       END,      
       PC_SBS_ORGANICO = CASE       
            WHEN cultivo = 'Org�nico'       
            THEN PC_SBS_ORGANICO       
            ELSE 0.0       
       END      
  INTO #subvencao_quest      
  FROM #subvencao_pc      
  
 /* Jessica.Adao - Confitec sistemas - 20160317 - Projeto 19279696 : Subvencao Mapa */          
 SET @Atividade = 'PRODUTO: ' + CONVERT(VARCHAR, @produto_id) + ' - VALORES DOS PERC. SUBVENCAO SELECIONADOS - #SUBVENCAO_QUEST: ' + CONVERT(VARCHAR, @@ROWCOUNT)  
 SET @Horario_Inicio_Tarefa = GETDATE()   
 EXEC controle_sistema_db.dbo.SEGS8602_SPI @SiglaTarefa, @dt_inclusao, @Atividade, @Horario_Inicio_Tarefa, NULL, @Usuario   
        
SELECT distinct proposta_id,      
       seq_proposta,      
       val_premio_tarifa,      
       PC_SBS_SGRO_AGRL,           
       modalidade_subvencao_id = CASE -- Valores obtidos na tabela modalidade_subvencao_tb      
            WHEN (PC_SBS_CRITICO >= PC_SBS_PRONAMP) AND (PC_SBS_CRITICO >= PC_SBS_ORGANICO)      
            THEN 3 -- MUNIC�PIO CR�TICO      
            ELSE (CASE WHEN PC_SBS_PRONAMP >= PC_SBS_ORGANICO       
                       THEN 1 -- PRONAMP      
                       ELSE 2 -- ORGANICO      
                  END)       
       END,      
       pc_subvencao_federal = CASE       
            WHEN (PC_SBS_CRITICO >= PC_SBS_PRONAMP) AND (PC_SBS_CRITICO >= PC_SBS_ORGANICO)      
            THEN PC_SBS_CRITICO       
            ELSE (CASE WHEN PC_SBS_PRONAMP >= PC_SBS_ORGANICO       
                       THEN PC_SBS_PRONAMP       
               ELSE PC_SBS_ORGANICO  
                  END)       
       END      
  INTO #subvencao_pc_sbs      
  FROM #subvencao_quest      
  
 /* Jessica.Adao - Confitec sistemas - 20160317 - Projeto 19279696 : Subvencao Mapa */          
 SET @Atividade = 'PRODUTO: ' + CONVERT(VARCHAR, @produto_id) + ' - VALORES DOS PERC. SUBVENCAO SELECIONADOS - #SUBVENCAO_PC_SBS: ' + CONVERT(VARCHAR, @@ROWCOUNT)  
 SET @Horario_Inicio_Tarefa = GETDATE()   
 EXEC controle_sistema_db.dbo.SEGS8602_SPI @SiglaTarefa, @dt_inclusao, @Atividade, @Horario_Inicio_Tarefa, NULL, @Usuario                 
        
SELECT distinct proposta_id,      
       val_parcela = (val_premio_tarifa * ((PC_SBS_SGRO_AGRL / 100) + (pc_subvencao_federal / 100))),      
       -- pablo.dias (Nova Consultoria) - 10/09/2012 - Demanda: 15839547      
       -- N�o gravar modalidade de subven��o quando percentual for igual a zero      
       modalidade_subvencao_id = CASE WHEN pc_subvencao_federal > 0 THEN modalidade_subvencao_id ELSE NULL END,      
       pc_subvencao_federal = CASE WHEN pc_subvencao_federal > 0 THEN pc_subvencao_federal ELSE NULL END      
       -- FIM - pablo.dias - 10/09/2012      
  INTO #subvencao      
  FROM #subvencao_pc_sbs      
-- FIM - pablo.dias - 22/08/2012       
                
 /* Jessica.Adao - Confitec sistemas - 20160317 - Projeto 19279696 : Subvencao Mapa */          
 SET @Atividade = 'PRODUTO: ' + CONVERT(VARCHAR, @produto_id) + ' - SUBVENCOES SELECIONADAS: ' + CONVERT(VARCHAR, @@ROWCOUNT)  
 SET @Horario_Inicio_Tarefa = GETDATE()   
 EXEC controle_sistema_db.dbo.SEGS8602_SPI @SiglaTarefa, @dt_inclusao, @Atividade, @Horario_Inicio_Tarefa, NULL, @Usuario                 
               
    --lrocha 15/07/2008 - Subven��o federal e estadual                  
    --Incluindo subven��o                    
    INSERT INTO seguros_db.dbo.proposta_subvencao_tb(proposta_id,                    
               val_subvencao_estimado,                    
                                      --val_subvencao_informado,                    
                                      endosso_id,                     
                                      dt_inclusao,                
                                      usuario,             
                                      -- Autor: pablo.dias (Nova Consultoria) - Data da Altera��o: 22/08/2012      
                                      -- Demanda: 15839547 - Item: Defini��o do novo C�lculo da Subven��o Federal      
                                      -- Incluir valores dos percentuais de subven��o           
                                      tipo_subvencao_id,      
                                      modalidade_subvencao_id,      
                pc_subvencao_federal)                    
                                      -- FIM - pablo.dias - 22/08/2012      
    SELECT proposta_id,                    
           val_subvencao_estimado = val_parcela,                    
           --val_subvencao_informado = val_parcela,                    
           0,                    
           @dt_inclusao,                    
           @usuario,                  
           1, -- 1: Subvenc�o Federal / 2: Subven��o Estadual      
           -- Autor: pablo.dias (Nova Consultoria) - Data da Altera��o: 22/08/2012      
           -- Demanda: 15839547 - Item: Defini��o do novo C�lculo da Subven��o Federal      
           -- Novos campos em proposta_subvencao_tb      
           modalidade_subvencao_id,      
           pc_subvencao_federal      
           -- FIM - pablo.dias - 22/08/2012      
    FROM #subvencao              
                  
 /* Jessica.Adao - Confitec sistemas - 20160317 - Projeto 19279696 : Subvencao Mapa */          
 SET @Atividade = 'PRODUTO: ' + CONVERT(VARCHAR, @produto_id) + ' - SUBVENCOES GRAVADAS: ' + CONVERT(VARCHAR, @@ROWCOUNT)  
 SET @Horario_Inicio_Tarefa = GETDATE()   
 EXEC controle_sistema_db.dbo.SEGS8602_SPI @SiglaTarefa, @dt_inclusao, @Atividade, @Horario_Inicio_Tarefa, NULL, @Usuario                 
            

/************************************ JGASOUZA - Confitec sistemas - 20190517 ****************************************/ 
/*		verifica se as propostas novas tem desconto ou agravo importados - e salva o desconto pra cada proposta      */

 SET @Atividade = 'PRODUTO: ' + CONVERT(VARCHAR, @produto_id) + ' - INSERE DADOS DE DESCONTO OU AGRAVO '
 SET @Horario_Inicio_Tarefa = GETDATE()   
 EXEC controle_sistema_db.dbo.SEGS8602_SPI @SiglaTarefa, @dt_inclusao, @Atividade, @Horario_Inicio_Tarefa, NULL, @Usuario     

    if object_id('tempdb.dbo.#desc_agravo') is not null
     begin
        drop table #desc_agravo
     end

    create table #desc_agravo
        ( proposta_id   numeric(9)  null
         ,cpf_cnpj      varchar(14) null
         ,rem_desc      int         null
         ,produto_id    int         null
        )

    insert into #desc_agravo(proposta_id, cpf_cnpj, produto_id, rem_desc)
    select p.proposta_id, d.cpf_cnpj, p.produto_id, max(d.remessa_desconto_cpf_id) as rem_desc
	From #proposta p with (nolock)
	Inner Join seguros_db.dbo.cliente_tb c				 with (nolock) on c.cliente_id = p.cliente_id
	Inner Join seguros_db.dbo.desconto_cpf_detalhes_tb d with (nolock) on c.cpf_cnpj = d.cpf_cnpj And p.produto_id = d.produto_id			
                                                                      and p.dt_contratacao between d.dt_inicio_vigencia and d.dt_fim_vigencia
--petrauskas 11/2019 - incluido filtro de aceite do banco
    inner join seguros_db.dbo.remessa_desconto_cpf_tb  r with (nolock) on d.remessa_desconto_cpf_id = r.remessa_desconto_cpf_id
    where isnull(r.st_retorno_ok,'n') = 's'
    group by p.proposta_id, d.cpf_cnpj, p.produto_id
    
	Insert into seguros_db.dbo.proposta_desconto_tb
			(  remessa_desconto_cpf_id 
			 , proposta_id 
			 , fator_produtividade 
			 , fator_taxa 
			 , dt_inclusao 	
			 , usuario 
			)			
	Select d.remessa_desconto_cpf_id
		 , p.proposta_id
		 , d.fator_produtividade
		 , d.fator_taxa
		 , GetDate()
		 , @Usuario
	From #proposta                                     p with (nolock)
	Inner Join #desc_agravo                            c with (nolock) on p.proposta_id = c.proposta_id 
	Inner Join seguros_db.dbo.desconto_cpf_detalhes_tb d with (nolock) on c.cpf_cnpj = d.cpf_cnpj 
                                                                      And c.produto_id = d.produto_id
                                                                      AND c.rem_desc = d.remessa_desconto_cpf_id 

/****************************** FIM JGASOUZA - Confitec sistemas - 20190517 ****************************************/     


 SET @Atividade = 'PRODUTO: ' + CONVERT(VARCHAR, @produto_id) + ' - FIM PROCESSAMENTO'
 SET @Horario_Inicio_Tarefa = GETDATE()   
 EXEC controle_sistema_db.dbo.SEGS8602_SPI @SiglaTarefa, @dt_inclusao, @Atividade, @Horario_Inicio_Tarefa, NULL, @Usuario                 

            
 END TRY                          
                              
 BEGIN CATCH     

 DECLARE @ErrorMessage NVARCHAR(4000)            
 DECLARE @ErrorSeverity INT            
 DECLARE @ErrorState INT       
   
    SELECT             
       @ErrorMessage = ERROR_PROCEDURE() + ' - Linha ' + CONVERT(VARCHAR(15),ERROR_LINE()) + ' - ' + ERROR_MESSAGE(),            
       @ErrorSeverity = ERROR_SEVERITY(),            
       @ErrorState = ERROR_STATE()            
       
    RAISERROR (    
       @ErrorMessage,            
       @ErrorSeverity,            
       @ErrorState )
               
 END CATCH  

