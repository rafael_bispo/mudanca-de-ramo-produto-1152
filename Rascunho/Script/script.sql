﻿--SELECT * FROM SEGUROS_dB.DBO.PARAMETRO_gERAL_tB
USE desenv_db
--rollback
--COMMIT

--atualizar na mão
  --444 GETDATE()
  --562 AND NOT EXISTS (SELECT 1 FROM [seguros_db].dbo.[subramo_tp_endosso_tb] a WITH (NOLOCK)
  --												                    WHERE a.tp_endosso_id = qualidade.tp_endosso_id
  --											                       
  --													                    )
													                    


--ALTER PROCEDURE DBO.RDG
--AS
--UPDATE seguros_db.dbo.parametro_geral_tb
--set dt_operacional = '2020-05-20'


begin tran
select @@trancount
go
--set transaction isolation level read uncommitted exec dbo.rdg
exec dbo.rdg
go
  
  
 /*   
select * from als_produto_criacao_db.dbo.dados_publicacao_tb dp WITH (NOLOCK)    
WHERE cd_prd = 1152      
     
select * from seguros_db.dbo.produto_tb where produto_id = 1152
select * from als_produto_db.dbo.ITEM_MDLD_PRD_HIST_TB where CD_PRD = 1152
select * from als_produto_db.dbo.ITEM_MDLD_PRD where CD_PRD = 1152
select * from seguros_db.dbo.item_produto_tb WHERE produto_id = 1152
select * from seguros_db.dbo.produto_forma_pgto_tb WHERE produto_id = 1152
select * from ls_sus.seguros_db.dbo.produto_forma_pgto_tb WHERE produto_id = 1152
select * from seguros_db.dbo.produto_pgto_tb WHERE produto_id = 1152
select * from ls_sus.seguros_db.dbo.produto_pgto_tb WHERE produto_id = 1152
select * from seguros_db.dbo.forma_pgto_tb 
select * from seguros_db.dbo.subramo_tb where subramo_id = 102
select * from seguros_db.dbo.erro_aceitavel_pgto_tb WHERE produto_id = 1152
select * from seguros_db.dbo.produto_juros_tb WHERE produto_id = 1152
select * from seguros_db.dbo.produto_corretagem_tb WHERE produto_id = 1152
select * from seguros_db.dbo.tp_cob_item_prod_tb WHERE produto_id = 1152
select * from seguros_db.dbo.atualizacao_item_produto_tb  WHERE produto_id = 1152
select * from seguros_db.dbo.tp_movimentacao_financ_tb  WHERE produto_id = 1152
select * from seguros_db.dbo.custo_tb where produto_id = 1152
select * from seguros_db.dbo.limite_produto_tb where produto_id = 1152
select * from seguros_db.dbo.produto_tp_carta_tb where produto_id = 1152 
select * from seguros_db.dbo.produto_tp_avaliacao_tb where produto_id = 1152 
select * from seguros_db.dbo.parametro_movimentacao_pre_analise_tb where produto_id = 1152
select * from seguros_db.dbo.produto_metodo_tb where produto_id = 1152
select * from seguros_db.dbo.limite_is_faixa_tb  where produto_id = 1152
select * from seguros_db.dbo.arquivo_produto_tb where produto_id = 1152
select * from als_produto_db.dbo.produto_workflow_contrato_tb  where cd_mdld = 5

*/

ALTER PROCEDURE dbo.rdg

AS

DECLARE	@data_dia VARCHAR(8)
  
SELECT @data_dia     = CONVERT(VARCHAR(8),GETDATE(),112)
 
INSERT INTO [als_produto_db].dbo.[ramo_tb] (ramo_id, 
										    tp_ramo_id, 
										    tp_objeto_id, 
										    nome, 
										    dt_inclusao, 
										    usuario, 
										    obriga_fim_vigencia,	
										    critica_custo,
										    nome_ate_31122002,
										    ramo_anterior_id,
										    situacao,
										    grupo_ramo)  
								     SELECT PRODUCAO.ramo_id, 
											PRODUCAO.tp_ramo_id, 
											PRODUCAO.tp_objeto_id, 
											PRODUCAO.nome,
											@data_dia,
											'14271121851',
											PRODUCAO.obriga_fim_vigencia,	
											PRODUCAO.critica_custo,
											PRODUCAO.nome_ate_31122002,
											PRODUCAO.ramo_anterior_id,
											PRODUCAO.situacao,
											PRODUCAO.grupo_ramo
									   FROM [LS_SUS].[als_produto_db].dbo.[ramo_tb] PRODUCAO WITH(NOLOCK)      
									   LEFT JOIN [als_produto_db].dbo.[ramo_tb] QUALIDADE WITH(NOLOCK)
									     ON PRODUCAO.ramo_id = QUALIDADE.ramo_id
									  WHERE PRODUCAO.ramo_id = 1
									    AND QUALIDADE.ramo_id IS NULL
INSERT INTO [seguros_db].dbo.[ramo_tb] (ramo_id,
									    tp_ramo_id,
									    tp_objeto_id,
									    nome,
									    dt_inclusao,
									    usuario,
									    obriga_fim_vigencia,	
									    critica_custo,
									    nome_ate_31122002,
									    ramo_anterior_id,
									    limite_retencao,
									    grupo_ramo,
									    cod_empresa,
									    tp_area)     
								 SELECT PRODUCAO.ramo_id,
									    PRODUCAO.tp_ramo_id,
									    PRODUCAO.tp_objeto_id,
									    PRODUCAO.nome,
									    @data_dia,
									    '14271121851',
									    PRODUCAO.obriga_fim_vigencia,	
									    PRODUCAO.critica_custo,
									    PRODUCAO.nome_ate_31122002,
									    PRODUCAO.ramo_anterior_id,
									    PRODUCAO.limite_retencao,
									    PRODUCAO.grupo_ramo,
									    PRODUCAO.cod_empresa,
									    PRODUCAO.tp_area
								   FROM [LS_SUS].[seguros_db].dbo.[ramo_tb] PRODUCAO WITH(NOLOCK)      
								   LEFT JOIN [seguros_db].dbo.[ramo_tb] QUALIDADE WITH(NOLOCK)      
								     ON PRODUCAO.ramo_id = QUALIDADE.ramo_id
								  WHERE PRODUCAO.ramo_id = 1
								    AND QUALIDADE.ramo_id IS NULL
INSERT INTO [resseg_db].dbo.[ramo_tb] (ramo_id,
									   dt_inclusao,
									   usuario,
									   nome,
									   tp_ramo_id,
									   grupo_ramo)     
								SELECT PRODUCAO.ramo_id,
									   @data_dia,
									   '14271121851',
									   PRODUCAO.nome,
									   PRODUCAO.tp_ramo_id,
									   PRODUCAO.grupo_ramo
								 FROM [LS_SUS].[resseg_db].dbo.[ramo_tb] PRODUCAO WITH(NOLOCK)
								 LEFT JOIN [resseg_db].dbo.[ramo_tb] QUALIDADE WITH(NOLOCK)
								   ON PRODUCAO.ramo_id = QUALIDADE.ramo_id
								WHERE PRODUCAO.ramo_id = 1
								  AND QUALIDADE.ramo_id IS NULL
INSERT INTO [als_produto_db].dbo.[atualizacao_ramo_tb](dt_inicio_vigencia,
													   ramo_id,
													   indice_id,
													   tp_uso,
													   periodo_atualizacao,
													   situacao,
													   dt_inclusao,
													   usuario)
												SELECT PRODUCAO.dt_inicio_vigencia,
													   PRODUCAO.ramo_id,
													   PRODUCAO.indice_id,
													   PRODUCAO.tp_uso,
													   PRODUCAO.periodo_atualizacao,
													   PRODUCAO.situacao,
													   @data_dia,
													   '14271121851'
												  FROM [LS_SUS].[als_produto_db].dbo.[atualizacao_ramo_tb] PRODUCAO WITH(NOLOCK)
												  LEFT JOIN [als_produto_db].dbo.[atualizacao_ramo_tb] QUALIDADE WITH(NOLOCK)
												    ON PRODUCAO.dt_inicio_vigencia = QUALIDADE.dt_inicio_vigencia  
												   AND PRODUCAO.ramo_id = QUALIDADE.ramo_id 
												   AND PRODUCAO.indice_id = QUALIDADE.indice_id
												   AND PRODUCAO.tp_uso = QUALIDADE.tp_uso
												 WHERE PRODUCAO.ramo_id = 1
												   AND PRODUCAO.dt_fim_vigencia IS NULL
												   AND QUALIDADE.ramo_id IS NULL	
	
INSERT INTO [seguros_db].dbo.[atualizacao_ramo_tb](dt_inicio_vigencia,
												   ramo_id,
												   indice_id,
												   tp_uso,
												   periodo_atualizacao,
												   dt_inclusao,
												   usuario)
											SELECT PRODUCAO.dt_inicio_vigencia,
												   PRODUCAO.ramo_id,
												   PRODUCAO.indice_id,
												   PRODUCAO.tp_uso,
												   PRODUCAO.periodo_atualizacao,
												   @data_dia,
												   '14271121851'
											  FROM [LS_SUS].[seguros_db].dbo.[atualizacao_ramo_tb] PRODUCAO WITH(NOLOCK)
											  LEFT JOIN [seguros_db].dbo.[atualizacao_ramo_tb] QUALIDADE WITH(NOLOCK)
											    ON PRODUCAO.dt_inicio_vigencia = QUALIDADE.dt_inicio_vigencia
											   AND PRODUCAO.ramo_id = QUALIDADE.ramo_id
											   AND PRODUCAO.indice_id = QUALIDADE.indice_id
											   AND PRODUCAO.tp_uso = QUALIDADE.tp_uso
											   WHERE PRODUCAO.ramo_id = 1
												 AND PRODUCAO.dt_fim_vigencia IS NULL	
												 AND QUALIDADE.ramo_id IS NULL
   
INSERT INTO [als_produto_db].dbo.[val_item_financeiro_ramo_tb](cod_item_financeiro,
															   dt_inicio_vigencia,
															   ramo_id,
															   dt_inclusao,
															   usuario,
															   valor,
															   situacao)
														SELECT PRODUCAO.cod_item_financeiro,
															   PRODUCAO.dt_inicio_vigencia,
															   PRODUCAO.ramo_id,
															   @data_dia,
															   '14271121851',
															   PRODUCAO.valor,
															   PRODUCAO.situacao
														  FROM [LS_SUS].[als_produto_db].dbo.[val_item_financeiro_ramo_tb] PRODUCAO WITH(NOLOCK)
														  LEFT JOIN [als_produto_db].dbo.[val_item_financeiro_ramo_tb] QUALIDADE WITH(NOLOCK)
														    ON PRODUCAO.cod_item_financeiro = QUALIDADE.cod_item_financeiro
														   AND PRODUCAO.dt_inicio_vigencia = QUALIDADE.dt_inicio_vigencia
														   AND PRODUCAO.ramo_id = QUALIDADE.ramo_id
														 WHERE PRODUCAO.ramo_id = 1
						   								   AND PRODUCAO.dt_fim_vigencia IS NULL 
						   								   AND QUALIDADE.ramo_id IS NULL
										
INSERT INTO [seguros_db].dbo.[val_item_financeiro_ramo_tb](cod_item_financeiro,
														   dt_inicio_vigencia,
														   ramo_id,
														   dt_inclusao,
														   usuario,
														   valor)
													SELECT PRODUCAO.cod_item_financeiro,
														   PRODUCAO.dt_inicio_vigencia,
														   PRODUCAO.ramo_id,
														   @data_dia,
														   '14271121851',
														   PRODUCAO.valor
													  FROM [LS_SUS].[seguros_db].dbo.[val_item_financeiro_ramo_tb] PRODUCAO WITH(NOLOCK)
													  LEFT JOIN [seguros_db].dbo.[val_item_financeiro_ramo_tb] QUALIDADE WITH(NOLOCK)
													    ON PRODUCAO.cod_item_financeiro = QUALIDADE.cod_item_financeiro
													   AND PRODUCAO.dt_inicio_vigencia = QUALIDADE.dt_inicio_vigencia
													   AND PRODUCAO.ramo_id = QUALIDADE.ramo_id
													 WHERE PRODUCAO.ramo_id = 1
						   							   AND PRODUCAO.dt_fim_vigencia IS NULL
						   							   AND QUALIDADE.ramo_id IS NULL
						   
INSERT INTO [als_produto_db].dbo.[subramo_tb](ramo_id,
											  subramo_id,
											  dt_inicio_vigencia_sbr,
											  modalidade_seguro_id,
											  nome,
											  dt_inclusao,
											  dt_inicio_vigencia_mod,
											  class_risco,
											  usuario,
											  num_proc_susep,
											  tp_subramo,
											  ativo,
											  ramo_irb_id,
											  restituicao_unica,
											  qtd_dias_renovacao,
											  qtd_dias_aceite,
											  situacao,
											  monitoracao_renovacao)
									   SELECT PRODUCAO.ramo_id,
											  PRODUCAO.subramo_id,
											  PRODUCAO.dt_inicio_vigencia_sbr,
											  PRODUCAO.modalidade_seguro_id,
											  PRODUCAO.nome,
											  @data_dia,
											  PRODUCAO.dt_inicio_vigencia_mod,
											  PRODUCAO.class_risco,
											  '14271121851',
											  PRODUCAO.num_proc_susep,
											  PRODUCAO.tp_subramo,
											  PRODUCAO.ativo,
											  PRODUCAO.ramo_irb_id,
											  PRODUCAO.restituicao_unica,
											  PRODUCAO.qtd_dias_renovacao,
											  PRODUCAO.qtd_dias_aceite,
											  PRODUCAO.situacao,
											  PRODUCAO.monitoracao_renovacao
										FROM [LS_SUS].[als_produto_db].dbo.[subramo_tb] PRODUCAO WITH(NOLOCK)
										LEFT JOIN [als_produto_db].dbo.[subramo_tb] QUALIDADE WITH(NOLOCK)
										  ON PRODUCAO.ramo_id = QUALIDADE.ramo_id
										 AND PRODUCAO.subramo_id = QUALIDADE.subramo_id
										 AND PRODUCAO.dt_inicio_vigencia_sbr = QUALIDADE.dt_inicio_vigencia_sbr
									   WHERE PRODUCAO.ramo_id = 1 
										 AND PRODUCAO.subramo_id = 102
										 AND PRODUCAO.dt_fim_vigencia_sbr IS NULL    
										 AND QUALIDADE.ramo_id IS NULL
   
INSERT INTO [seguros_db].dbo.[subramo_tb](ramo_id,
										  subramo_id,
										  dt_inicio_vigencia_sbr,
										  modalidade_seguro_id,
										  nome,
										  dt_inclusao,
										  dt_inicio_vigencia_mod,
										  class_risco,
										  usuario,
										  num_proc_susep,
										  tp_subramo,
										  ativo,
										  ramo_irb_id,
										  restituicao_unica,
										  qtd_dias_renovacao,
										  qtd_dias_aceite,
										  iof_cobertura)
								   SELECT PRODUCAO.ramo_id,
								          PRODUCAO.subramo_id,
										  PRODUCAO.dt_inicio_vigencia_sbr,
										  PRODUCAO.modalidade_seguro_id,
										  PRODUCAO.nome,
										  @data_dia,
										  PRODUCAO.dt_inicio_vigencia_mod,
										  PRODUCAO.class_risco,
										  '14271121851',
										  PRODUCAO.num_proc_susep,
										  PRODUCAO.tp_subramo,
										  PRODUCAO.ativo,
										  PRODUCAO.ramo_irb_id,
										  PRODUCAO.restituicao_unica,
										  PRODUCAO.qtd_dias_renovacao,
										  PRODUCAO.qtd_dias_aceite,
										  PRODUCAO.iof_cobertura
									 FROM [LS_SUS].[seguros_db].dbo.[subramo_tb] PRODUCAO WITH(NOLOCK)
									 LEFT JOIN [seguros_db].dbo.[subramo_tb] QUALIDADE WITH(NOLOCK)
									   ON PRODUCAO.ramo_id = QUALIDADE.ramo_id
									  AND PRODUCAO.subramo_id = QUALIDADE.subramo_id
									  AND PRODUCAO.dt_inicio_vigencia_sbr = QUALIDADE.dt_inicio_vigencia_sbr
									WHERE PRODUCAO.ramo_id = 1
									  AND PRODUCAO.subramo_id = 102
									  AND PRODUCAO.dt_fim_vigencia_sbr IS NULL 
									  AND QUALIDADE.ramo_id IS NULL
   
INSERT INTO [resseg_db].dbo.[subramo_tb] (ramo_id,
										  subramo_id,
										  dt_inclusao,
										  usuario,
										  nome,
										  vincular_documento_contrato,
										  status)     
								   SELECT PRODUCAO.ramo_id,
								   	      PRODUCAO.subramo_id,
										  @data_dia,
										  '14271121851',
										  PRODUCAO.nome,
										  PRODUCAO.vincular_documento_contrato,
										  PRODUCAO.status
									FROM [LS_SUS].[resseg_db].dbo.[subramo_tb] PRODUCAO WITH(NOLOCK)
									LEFT JOIN [resseg_db].dbo.[subramo_tb] QUALIDADE WITH(NOLOCK)
									  ON PRODUCAO.ramo_id = QUALIDADE.ramo_id
									 AND PRODUCAO.subramo_id = QUALIDADE.subramo_id
								   WHERE PRODUCAO.ramo_id = 1
									 AND PRODUCAO.subramo_id = 102
									 AND QUALIDADE.ramo_id IS NULL
    
INSERT INTO [als_produto_db].dbo.[subramo_limite_tb](ramo_id,
													 subramo_id,
													 dt_inicio_vigencia_sbr,
													 dt_inicio_vigencia_lim,
													 moeda_lim_tecnico,
													 moeda_lim_emissao,
													 val_lim_tecnico,
													 val_lim_emissao,
													 usuario,
													 dt_inclusao,
													 situacao)
											  SELECT PRODUCAO.ramo_id,
													 PRODUCAO.subramo_id,
													 PRODUCAO.dt_inicio_vigencia_sbr,
													 PRODUCAO.dt_inicio_vigencia_lim,
													 PRODUCAO.moeda_lim_tecnico,
													 PRODUCAO.moeda_lim_emissao,
													 PRODUCAO.val_lim_tecnico,
													 PRODUCAO.val_lim_emissao,
												     '14271121851',
												     @data_dia,
												     PRODUCAO.situacao
											    FROM [LS_SUS].[als_produto_db].dbo.[subramo_limite_tb] PRODUCAO WITH(NOLOCK)
											    LEFT JOIN [als_produto_db].dbo.[subramo_limite_tb] QUALIDADE WITH(NOLOCK)
											      ON PRODUCAO.ramo_id = QUALIDADE.ramo_id
											     AND PRODUCAO.subramo_id = QUALIDADE.subramo_id
											     AND PRODUCAO.dt_inicio_vigencia_sbr = QUALIDADE.dt_inicio_vigencia_sbr
											     AND PRODUCAO.dt_inicio_vigencia_lim = QUALIDADE.dt_inicio_vigencia_lim
											   WHERE PRODUCAO.ramo_id = 1
												 AND PRODUCAO.subramo_id = 102
												 AND PRODUCAO.dt_fim_vigencia_lim IS NULL 
												 AND QUALIDADE.ramo_id IS NULL
												 
   
INSERT INTO [seguros_db].dbo.[subramo_limite_tb](ramo_id,
												 subramo_id,
												 dt_inicio_vigencia_sbr,
												 dt_inicio_vigencia_lim,
												 moeda_lim_tecnico,
												 moeda_lim_emissao,
												 val_lim_tecnico,
												 val_lim_emissao,
												 usuario,
												 dt_inclusao)
										  SELECT PRODUCAO.ramo_id,
												 PRODUCAO.subramo_id,
												 PRODUCAO.dt_inicio_vigencia_sbr,
												 PRODUCAO.dt_inicio_vigencia_lim,
												 PRODUCAO.moeda_lim_tecnico,
												 PRODUCAO.moeda_lim_emissao,
												 PRODUCAO.val_lim_tecnico,
												 PRODUCAO.val_lim_emissao,
											     '14271121851',
											     @data_dia
										    FROM [LS_SUS].[seguros_db].dbo.[subramo_limite_tb] PRODUCAO WITH(NOLOCK)
										    LEFT JOIN [seguros_db].dbo.[subramo_limite_tb] QUALIDADE WITH(NOLOCK)
										      ON PRODUCAO.ramo_id = QUALIDADE.ramo_id
										     AND PRODUCAO.subramo_id = QUALIDADE.subramo_id
										     AND PRODUCAO.dt_inicio_vigencia_sbr = QUALIDADE.dt_inicio_vigencia_sbr
										     AND PRODUCAO.dt_inicio_vigencia_lim = QUALIDADE.dt_inicio_vigencia_lim
										   WHERE PRODUCAO.ramo_id = 1
											 AND PRODUCAO.subramo_id = 102
											 AND PRODUCAO.dt_fim_vigencia_lim IS NULL
											 AND QUALIDADE.ramo_id IS NULL
											    
INSERT INTO [seguros_db].dbo.[SUBRAMO_IOF_TB](ramo_id,
											  subramo_id,
											  dt_inicio_vigencia_sbr,
											  dt_inicio_vigencia,
											  dt_fim_vigencia,
											  perc_iof,
											  usuario,
											  dt_inclusao)
									   SELECT PRODUCAO.ramo_id,
											  PRODUCAO.subramo_id,
											  PRODUCAO.dt_inicio_vigencia_sbr,
											  PRODUCAO.dt_inicio_vigencia,
											  PRODUCAO.dt_fim_vigencia,
											  PRODUCAO.perc_iof,
											  '14271121851',
											  @data_dia
										 FROM [LS_SUS].[seguros_db].dbo.[SUBRAMO_IOF_TB] PRODUCAO WITH(NOLOCK)
										 LEFT JOIN [seguros_db].dbo.[SUBRAMO_IOF_TB] QUALIDADE WITH(NOLOCK)
										   ON PRODUCAO.ramo_id = QUALIDADE.ramo_id
										  AND PRODUCAO.subramo_id = QUALIDADE.subramo_id
										  AND PRODUCAO.dt_inicio_vigencia_sbr = QUALIDADE.dt_inicio_vigencia_sbr
										  AND PRODUCAO.dt_inicio_vigencia = QUALIDADE.dt_inicio_vigencia
										WHERE PRODUCAO.ramo_id = 1
										  AND PRODUCAO.subramo_id = 102
										  AND QUALIDADE.ramo_id IS NULL
										   AND PRODUCAO.dt_fim_vigencia > GETDATE()
	INSERT INTO [als_produto_db].dbo.[tp_endosso_tb](tp_endosso_id,
													 descricao,
													 tp_endosso_cosseguro,
													 movimentacao_financeira,
													 usuario,
													 dt_inclusao,
													 situacao,
													 classe_endosso_id)
											  SELECT PRODUCAO.tp_endosso_id,
										   			 PRODUCAO.descricao,
													 PRODUCAO.tp_endosso_cosseguro,
													 PRODUCAO.movimentacao_financeira,
													 '14271121851',
													 @data_dia,
													 PRODUCAO.situacao,
													 PRODUCAO.classe_endosso_id
												FROM [LS_SUS].[als_produto_db].dbo.[tp_endosso_tb] PRODUCAO WITH(NOLOCK)
												LEFT JOIN [als_produto_db].dbo.[tp_endosso_tb] QUALIDADE WITH(NOLOCK)
												  ON PRODUCAO.tp_endosso_id = QUALIDADE.tp_endosso_id
											   WHERE QUALIDADE.tp_endosso_id IS NULL
											    											   
											   
	INSERT INTO [seguros_db].dbo.[tp_endosso_tb](tp_endosso_id,
												 descricao,
												 dt_inclusao,
												 usuario,
												 tp_endosso_cosseguro,
												 movimentacao_financeira,
												 classe_endosso_id,
												 cod_evento_als,
												 gera_movimentacao_restituicao,
												 descricao_SEGA,
												 altera_vigencia,
												 flag_enviar)
										  SELECT PRODUCAO.tp_endosso_id,
												 PRODUCAO.descricao,
												 @data_dia,
												 '14271121851',
												 PRODUCAO.tp_endosso_cosseguro,
												 PRODUCAO.movimentacao_financeira,
												 PRODUCAO.classe_endosso_id,
												 PRODUCAO.cod_evento_als,
												 PRODUCAO.gera_movimentacao_restituicao,
												 PRODUCAO.descricao_SEGA,
												 PRODUCAO.altera_vigencia,
												 PRODUCAO.flag_enviar
											FROM [LS_SUS].[seguros_db].dbo.[tp_endosso_tb] PRODUCAO WITH (NOLOCK)
											LEFT JOIN [seguros_db].dbo.[tp_endosso_tb] QUALIDADE
											  ON PRODUCAO.tp_endosso_id = QUALIDADE.tp_endosso_id
										   WHERE QUALIDADE.tp_endosso_id IS NULL
										   
INSERT INTO [als_produto_db].dbo.[subramo_tp_endosso_tb](ramo_id,
														 subramo_id,
														 dt_inicio_vigencia_sbr,
														 tp_endosso_id,
														 dt_inclusao,
														 usuario,
														 permite_restituicao,
														 dt_inicio_vigencia,
														 dt_fim_vigencia,
														 situacao)
												  SELECT PRODUCAO.ramo_id,
												   	     PRODUCAO.subramo_id,
														 PRODUCAO.dt_inicio_vigencia_sbr,
														 PRODUCAO.tp_endosso_id,
														 @data_dia,
														 '14271121851',
														 PRODUCAO.permite_restituicao,
														 PRODUCAO.dt_inicio_vigencia,
														 PRODUCAO.dt_fim_vigencia,
														 PRODUCAO.situacao
													FROM [LS_SUS].[als_produto_db].dbo.[subramo_tp_endosso_tb] PRODUCAO WITH(NOLOCK)
													LEFT JOIN [als_produto_db].dbo.[subramo_tp_endosso_tb] QUALIDADE WITH(NOLOCK)
													  ON PRODUCAO.ramo_id = QUALIDADE.ramo_id
													 AND PRODUCAO.subramo_id = QUALIDADE.subramo_id
													 AND PRODUCAO.dt_inicio_vigencia_sbr = QUALIDADE.dt_inicio_vigencia_sbr
													 AND PRODUCAO.tp_endosso_id = QUALIDADE.tp_endosso_id
													 AND PRODUCAO.dt_inicio_vigencia = QUALIDADE.dt_inicio_vigencia
												   WHERE PRODUCAO.ramo_id = 1
													 AND PRODUCAO.subramo_id = 102
													 AND QUALIDADE.ramo_id IS NULL
													 AND PRODUCAO.dt_FIM_vigencia is null
													 
													 --rollback
													 
	INSERT INTO [seguros_db].dbo.[subramo_tp_endosso_tb](ramo_id,
														 subramo_id,
														 dt_inicio_vigencia_sbr,
														 tp_endosso_id,
														 dt_inclusao,
														 usuario,
														 permite_restituicao,
														 dt_inicio_vigencia,
														 dt_fim_vigencia,
														 origem_endosso,
														 automatico)
												  SELECT PRODUCAO.ramo_id,
			   											 PRODUCAO.subramo_id,
														 PRODUCAO.dt_inicio_vigencia_sbr,
														 PRODUCAO.tp_endosso_id,
														 @data_dia,
														 '14271121851', 
														 PRODUCAO.permite_restituicao,
														 PRODUCAO.dt_inicio_vigencia,
														 PRODUCAO.dt_fim_vigencia,
														 PRODUCAO.origem_endosso,
														 PRODUCAO.automatico
													FROM [LS_SUS].[seguros_db].dbo.[subramo_tp_endosso_tb] PRODUCAO WITH (NOLOCK)
													LEFT JOIN [seguros_db].dbo.[subramo_tp_endosso_tb] QUALIDADE
													  ON PRODUCAO.ramo_id = QUALIDADE.ramo_id
													 AND PRODUCAO.subramo_id = QUALIDADE.subramo_id 
													 AND PRODUCAO.dt_inicio_vigencia_sbr = QUALIDADE.dt_inicio_vigencia_sbr 
													 AND PRODUCAO.tp_endosso_id = QUALIDADE.tp_endosso_id 
													 AND PRODUCAO.dt_inicio_vigencia = QUALIDADE.dt_inicio_vigencia 
												   WHERE PRODUCAO.ramo_id = 1
													 AND PRODUCAO.subramo_id = 102
													  AND PRODUCAO.dt_FIM_vigencia is null
													  AND NOT EXISTS (SELECT 1 FROM [seguros_db].dbo.[subramo_tp_endosso_tb] a WITH (NOLOCK)
													                    WHERE a.tp_endosso_id = qualidade.tp_endosso_id
													                       
													                    )
													 
INSERT INTO [canal_db].dbo.[canal_tb](canal_id,
									  nm_canal,
									  usuario,
									  dt_inclusao,
									  nm_rdz_canal)
							   SELECT PRODUCAO.canal_id,
									  PRODUCAO.nm_canal,
									  '14271121851',
									  @data_dia,
									  PRODUCAO.nm_rdz_canal
								 FROM [LS_SUS].[canal_db].dbo.[canal_tb] PRODUCAO WITH(NOLOCK)
								INNER JOIN [LS_SUS].[als_produto_db].dbo.[SGRO_CNL_VND] als WITH (NOLOCK)
								   ON PRODUCAO.canal_id = als.CANAL_ID
								 LEFT JOIN [canal_db].dbo.[canal_tb] QUALIDADE WITH(NOLOCK)
								   ON PRODUCAO.canal_id = QUALIDADE.canal_id
								WHERE als.CD_PRD = '1152'
								  AND als.CD_MDLD = 1   
								  AND als.CD_ITEM_MDLD = 102
								  AND QUALIDADE.canal_id IS NULL
								GROUP BY PRODUCAO.canal_id,
									     PRODUCAO.nm_canal,
									     PRODUCAO.nm_rdz_canal
INSERT INTO [canal_db].dbo.[tp_meio_canal_tb](canal_id,
											  tp_meio_canal_id,
										      nm_tp_meio_canal,
										      ic_administrador,
										      banco,
										      tabela,
										      coluna,
										      coluna_nome,
										      usuario,
										      dt_inclusao,
										      ic_menu,
										      tp_meio_canal_id_bb)
									   SELECT PRODUCAO.canal_id,
											  PRODUCAO.tp_meio_canal_id,
										      PRODUCAO.nm_tp_meio_canal,
										      PRODUCAO.ic_administrador,
										      PRODUCAO.banco,
										      PRODUCAO.tabela,
										      PRODUCAO.coluna,
										      PRODUCAO.coluna_nome,
										      '14271121851',
										      @data_dia,
										      PRODUCAO.ic_menu,
										      PRODUCAO.tp_meio_canal_id_bb
									     FROM [LS_SUS].[canal_db].dbo.[tp_meio_canal_tb] PRODUCAO WITH(NOLOCK)
									    INNER JOIN [LS_SUS].[als_produto_db].dbo.[SGRO_CNL_VND] als WITH (NOLOCK)
									       ON PRODUCAO.canal_id = als.CANAL_ID
									      AND PRODUCAO.tp_meio_canal_id = als.TP_MEIO_CANAL_ID 
									     LEFT JOIN [canal_db].dbo.[tp_meio_canal_tb] QUALIDADE WITH(NOLOCK) 
									       ON PRODUCAO.canal_id = QUALIDADE.canal_id
									      AND PRODUCAO.tp_meio_canal_id = QUALIDADE.tp_meio_canal_id
									    WHERE als.CD_PRD = '1152'
									      AND als.CD_MDLD = 1
									      AND als.CD_ITEM_MDLD = 102
									      AND QUALIDADE.canal_id IS NULL  
INSERT INTO [als_produto_criacao_db].dbo.[CBT](CD_CBT,
											   NOME,
											   DESCRICAO,
											   PALAVRA_CHAVE,
											   USUARIO,
											   DT_INCLUSAO,
											   IN_LC_CST)
										SELECT PRODUCAO.CD_CBT,
											   PRODUCAO.NOME,
											   PRODUCAO.DESCRICAO,
											   PRODUCAO.PALAVRA_CHAVE,
											   '14271121851',
											   @data_dia,
											   PRODUCAO.IN_LC_CST
										  FROM [LS_SUS].[als_produto_criacao_db].dbo.[CBT] PRODUCAO WITH(NOLOCK)
										 INNER JOIN [LS_SUS].[als_produto_criacao_db].dbo.[CJT_CBT_ITEM_MDLD] als WITH (NOLOCK) 
										    ON PRODUCAO.CD_CBT = als.CD_CBT
										  LEFT JOIN [als_produto_criacao_db].dbo.[CBT] QUALIDADE WITH(NOLOCK)  
										    ON PRODUCAO.CD_CBT = QUALIDADE.CD_CBT
										 WHERE als.CD_PRD = '1152'
									       AND als.CD_MDLD = 1
									       AND als.CD_ITEM_MDLD = 102
									       AND QUALIDADE.CD_CBT IS NULL
										 GROUP BY PRODUCAO.CD_CBT,
												  PRODUCAO.NOME,
												  PRODUCAO.DESCRICAO,
												  PRODUCAO.PALAVRA_CHAVE,
												  PRODUCAO.IN_LC_CST
INSERT INTO [als_produto_criacao_db].dbo.[CBT_HIST_TB](CD_CBT,
													   DT_INICIO_VIGENCIA,
													   DT_FIM_VIGENCIA,
													   NOME,
													   DESCRICAO,
													   PALAVRA_CHAVE,
													   USUARIO,
													   DT_INCLUSAO,
													   NR_VERSAO,
													   tp_cob_comp_id,
													   ENVIADO_BB,
													   DT_ENVIO,
													   IN_LC_CST)
												SELECT PRODUCAO.CD_CBT,
													   PRODUCAO.DT_INICIO_VIGENCIA,
													   PRODUCAO.DT_FIM_VIGENCIA,
													   PRODUCAO.NOME,
													   PRODUCAO.DESCRICAO,
													   PRODUCAO.PALAVRA_CHAVE,
													   '14271121851',
													   @data_dia,
													   PRODUCAO.NR_VERSAO,
													   PRODUCAO.tp_cob_comp_id,
													   PRODUCAO.ENVIADO_BB,
													   PRODUCAO.DT_ENVIO,
													   PRODUCAO.IN_LC_CST
												  FROM [LS_SUS].[als_produto_criacao_db].dbo.[CBT_HIST_TB] PRODUCAO WITH(NOLOCK)
												 INNER JOIN [LS_SUS].[als_produto_criacao_db].dbo.[CJT_CBT_ITEM_MDLD] als WITH (NOLOCK)
												    ON PRODUCAO.CD_CBT = als.CD_CBT
												  LEFT JOIN [als_produto_criacao_db].dbo.[CBT_HIST_TB] QUALIDADE WITH(NOLOCK)
												    ON PRODUCAO.CD_CBT = QUALIDADE.CD_CBT
												   AND PRODUCAO.NR_VERSAO = QUALIDADE.NR_VERSAO
												 WHERE als.CD_PRD = '1152'
												   AND als.CD_MDLD = 1
												   AND als.CD_ITEM_MDLD = 102
												   AND QUALIDADE.CD_CBT IS NULL
												 GROUP BY PRODUCAO.CD_CBT,
														  PRODUCAO.DT_INICIO_VIGENCIA,
														  PRODUCAO.DT_FIM_VIGENCIA,
														  PRODUCAO.NOME,
														  PRODUCAO.DESCRICAO,
														  PRODUCAO.PALAVRA_CHAVE,
														  PRODUCAO.NR_VERSAO,
														  PRODUCAO.tp_cob_comp_id,
														  PRODUCAO.ENVIADO_BB,
														  PRODUCAO.DT_ENVIO,
														  PRODUCAO.IN_LC_CST  
												  
	    
	
INSERT INTO [als_produto_criacao_db].dbo.[TX_RDZ_CBT](CD_CBT,
													  DT_INC_VGC_CBT,
													  CD_EST_VCLC,
													  DT_FIM_VGC_CBT,
													  TX_RDZ_CBT,
													  TX_INFM_MTV_RSA,
													  ENVIADO_BB,
													  DT_ENVIO)
											   SELECT PRODUCAO.CD_CBT,
													  PRODUCAO.DT_INC_VGC_CBT,
													  PRODUCAO.CD_EST_VCLC,
													  PRODUCAO.DT_FIM_VGC_CBT,
													  PRODUCAO.TX_RDZ_CBT,
													  PRODUCAO.TX_INFM_MTV_RSA,
													  PRODUCAO.ENVIADO_BB,
													  PRODUCAO.DT_ENVIO
												 FROM [LS_SUS].[als_produto_criacao_db].dbo.[TX_RDZ_CBT] PRODUCAO WITH(NOLOCK)
												INNER JOIN [LS_SUS].[als_produto_criacao_db].dbo.[CJT_CBT_ITEM_MDLD] als WITH (NOLOCK) 
												   ON PRODUCAO.CD_CBT = als.CD_CBT
												 LEFT JOIN [als_produto_criacao_db].dbo.[TX_RDZ_CBT] QUALIDADE WITH(NOLOCK)
												   ON PRODUCAO.CD_CBT = QUALIDADE.CD_CBT
												  AND PRODUCAO.DT_INC_VGC_CBT = QUALIDADE.DT_INC_VGC_CBT   
												WHERE als.CD_PRD = '1152'
												  AND als.CD_MDLD = 1
												  AND als.CD_ITEM_MDLD = 102 
												  AND QUALIDADE.CD_CBT IS NULL
												GROUP BY PRODUCAO.CD_CBT,
														 PRODUCAO.DT_INC_VGC_CBT,
														 PRODUCAO.CD_EST_VCLC,
														 PRODUCAO.DT_FIM_VGC_CBT,
														 PRODUCAO.TX_RDZ_CBT,
														 PRODUCAO.TX_INFM_MTV_RSA,
														 PRODUCAO.ENVIADO_BB,
														 PRODUCAO.DT_ENVIO    
	
INSERT INTO [als_produto_criacao_db].dbo.[TX_RDZ_CBT_HIST_TB](CD_CBT,
															  DT_INC_VGC_CBT,
															  NR_VERSAO,
															  CD_EST_VCLC,
															  DT_FIM_VGC_CBT,
															  TX_RDZ_CBT,
															  USUARIO,
															  DT_INCLUSAO,
															  TX_INFM_MTV_RSA)
													   SELECT PRODUCAO.CD_CBT,
															  PRODUCAO.DT_INC_VGC_CBT,
															  PRODUCAO.NR_VERSAO,
															  PRODUCAO.CD_EST_VCLC,
															  PRODUCAO.DT_FIM_VGC_CBT,
															  PRODUCAO.TX_RDZ_CBT,
															  '14271121851',
															  @data_dia,
															  PRODUCAO.TX_INFM_MTV_RSA
														 FROM [LS_SUS].[als_produto_criacao_db].dbo.[TX_RDZ_CBT_HIST_TB] PRODUCAO WITH(NOLOCK)
														 INNER JOIN [LS_SUS].[als_produto_criacao_db].dbo.[CJT_CBT_ITEM_MDLD] als WITH (NOLOCK)
															ON PRODUCAO.CD_CBT = als.CD_CBT
														  LEFT JOIN [als_produto_criacao_db].dbo.[TX_RDZ_CBT_HIST_TB] QUALIDADE WITH(NOLOCK)
														    ON PRODUCAO.CD_CBT = QUALIDADE.CD_CBT
														   AND PRODUCAO.DT_INC_VGC_CBT = QUALIDADE.DT_INC_VGC_CBT
														   AND PRODUCAO.NR_VERSAO = QUALIDADE.NR_VERSAO
														 WHERE als.CD_PRD = '1152'
														   AND als.CD_MDLD = 1
														   AND als.CD_ITEM_MDLD = 102 	
														   AND QUALIDADE.CD_CBT IS NULL
													     GROUP BY PRODUCAO.CD_CBT,
																  PRODUCAO.DT_INC_VGC_CBT,
																  PRODUCAO.NR_VERSAO,
																  PRODUCAO.CD_EST_VCLC,
																  PRODUCAO.DT_FIM_VGC_CBT,
																  PRODUCAO.TX_RDZ_CBT,
																  PRODUCAO.TX_INFM_MTV_RSA	 
	
INSERT INTO [als_produto_criacao_db].dbo.[TX_LNH_CBT](CD_CBT,
													  NR_SEQL_LNH_CBT,
													  DT_INC_LNH_CBT,
													  CD_EST_VCLC,
													  DT_FIM_LNH_CBT,
													  TX_CTU_LNH_CBT,
													  TX_INFM_MTV_RSA,
													  ENVIADO_BB,
													  DT_ENVIO)
											   SELECT PRODUCAO.CD_CBT,
													  PRODUCAO.NR_SEQL_LNH_CBT,
													  PRODUCAO.DT_INC_LNH_CBT,
													  PRODUCAO.CD_EST_VCLC,
													  PRODUCAO.DT_FIM_LNH_CBT,
													  PRODUCAO.TX_CTU_LNH_CBT,
													  PRODUCAO.TX_INFM_MTV_RSA,
													  PRODUCAO.ENVIADO_BB,
													  PRODUCAO.DT_ENVIO
												 FROM [LS_SUS].[als_produto_criacao_db].dbo.[TX_LNH_CBT] PRODUCAO WITH(NOLOCK)
												INNER JOIN [LS_SUS].[als_produto_criacao_db].dbo.[CJT_CBT_ITEM_MDLD] als WITH (NOLOCK)
												   ON PRODUCAO.CD_CBT = als.CD_CBT
												 LEFT JOIN [als_produto_criacao_db].dbo.[TX_LNH_CBT] QUALIDADE WITH(NOLOCK)  
												   ON PRODUCAO.CD_CBT = QUALIDADE.CD_CBT
												  AND PRODUCAO.NR_SEQL_LNH_CBT = QUALIDADE.NR_SEQL_LNH_CBT
												  AND PRODUCAO.DT_INC_LNH_CBT = QUALIDADE.DT_INC_LNH_CBT
												WHERE als.CD_PRD = '1152'
												  AND als.CD_MDLD = 1
												  AND als.CD_ITEM_MDLD = 102   
												  AND QUALIDADE.CD_CBT IS NULL
												GROUP BY PRODUCAO.CD_CBT,
														 PRODUCAO.NR_SEQL_LNH_CBT,
														 PRODUCAO.DT_INC_LNH_CBT,
														 PRODUCAO.CD_EST_VCLC,
														 PRODUCAO.DT_FIM_LNH_CBT,
														 PRODUCAO.TX_CTU_LNH_CBT,
														 PRODUCAO.TX_INFM_MTV_RSA,
														 PRODUCAO.ENVIADO_BB,
														 PRODUCAO.DT_ENVIO 
	
INSERT INTO [als_produto_criacao_db].dbo.[TX_LNH_CBT_HIST_TB](CD_CBT,
															  NR_SEQL_LNH_CBT,
															  DT_INC_LNH_CBT,
															  NR_VERSAO,
															  CD_EST_VCLC,
															  DT_FIM_LNH_CBT,
															  TX_CTU_LNH_CBT,
															  USUARIO,
															  DT_INCLUSAO,
															  TX_INFM_MTV_RSA)
													   SELECT PRODUCAO.CD_CBT,
															  PRODUCAO.NR_SEQL_LNH_CBT,
															  PRODUCAO.DT_INC_LNH_CBT,
															  PRODUCAO.NR_VERSAO,
															  PRODUCAO.CD_EST_VCLC,
															  PRODUCAO.DT_FIM_LNH_CBT,
															  PRODUCAO.TX_CTU_LNH_CBT,
															  '14271121851',
															  @data_dia,
															  PRODUCAO.TX_INFM_MTV_RSA
														 FROM [LS_SUS].[als_produto_criacao_db].dbo.[TX_LNH_CBT_HIST_TB] PRODUCAO WITH(NOLOCK)
														INNER JOIN [LS_SUS].[als_produto_criacao_db].dbo.[CJT_CBT_ITEM_MDLD] als WITH (NOLOCK)
														   ON PRODUCAO.CD_CBT = als.CD_CBT
														 LEFT JOIN [als_produto_criacao_db].dbo.[TX_LNH_CBT_HIST_TB] QUALIDADE WITH(NOLOCK)  
														   ON PRODUCAO.CD_CBT = QUALIDADE.CD_CBT
														  AND PRODUCAO.NR_SEQL_LNH_CBT = QUALIDADE.NR_SEQL_LNH_CBT
														  AND PRODUCAO.DT_INC_LNH_CBT = QUALIDADE.DT_INC_LNH_CBT
														  AND PRODUCAO.NR_VERSAO = QUALIDADE.NR_VERSAO
														WHERE als.CD_PRD = '1152'
														  AND als.CD_MDLD = 1
														  AND als.CD_ITEM_MDLD = 102   
														  AND QUALIDADE.CD_CBT IS NULL  
														GROUP BY PRODUCAO.CD_CBT,
																 PRODUCAO.NR_SEQL_LNH_CBT,
																 PRODUCAO.DT_INC_LNH_CBT,
																 PRODUCAO.NR_VERSAO,
																 PRODUCAO.CD_EST_VCLC,
																 PRODUCAO.DT_FIM_LNH_CBT,
																 PRODUCAO.TX_CTU_LNH_CBT,
																 PRODUCAO.TX_INFM_MTV_RSA
	
INSERT INTO [als_produto_db].dbo.[tp_cobertura_tb](tp_cobertura_id,
												   nome,
												   dt_inclusao,
												   usuario,
												   descricao,
												   tipo_cobertura_basica,
												   tipo_cobertura_IEA,
												   tipo_cobertura_IPA,
												   tipo_cobertura_IPD,
												   cobertura_bb,
												   situacao,
												   palavra_chave,
												   dt_inicio_vigencia,
												   dt_fim_vigencia,
												   NR_VERSAO,
												   ENVIADO_BB,
												   DT_ENVIO,
												   IN_LC_CST)
											SELECT PRODUCAO.tp_cobertura_id,
												   PRODUCAO.nome,
												   @data_dia,
												   '14271121851',
												   PRODUCAO.descricao,
												   PRODUCAO.tipo_cobertura_basica,
												   PRODUCAO.tipo_cobertura_IEA,
												   PRODUCAO.tipo_cobertura_IPA,
												   PRODUCAO.tipo_cobertura_IPD,
												   PRODUCAO.cobertura_bb,
												   PRODUCAO.situacao,
												   PRODUCAO.palavra_chave,
												   PRODUCAO.dt_inicio_vigencia,
												   PRODUCAO.dt_fim_vigencia,
												   PRODUCAO.NR_VERSAO,
												   PRODUCAO.ENVIADO_BB,
												   PRODUCAO.DT_ENVIO,
												   PRODUCAO.IN_LC_CST
											  FROM [LS_SUS].[als_produto_db].dbo.[tp_cobertura_tb] PRODUCAO WITH(NOLOCK)
											 INNER JOIN [LS_SUS].[als_produto_criacao_db].dbo.[CJT_CBT_ITEM_MDLD] als WITH (NOLOCK) 
											    ON PRODUCAO.tp_cobertura_id = als.CD_CBT
											  LEFT JOIN [als_produto_db].dbo.[tp_cobertura_tb] QUALIDADE WITH(NOLOCK)  
											    ON PRODUCAO.tp_cobertura_id = QUALIDADE.tp_cobertura_id
											   AND PRODUCAO.dt_inicio_vigencia = QUALIDADE.dt_inicio_vigencia
											   AND PRODUCAO.NR_VERSAO = QUALIDADE.NR_VERSAO
											 WHERE als.CD_PRD = '1152'
											   AND als.CD_MDLD = 1
											   AND als.CD_ITEM_MDLD = 102
											   AND QUALIDADE.tp_cobertura_id IS NULL      
											 GROUP BY PRODUCAO.tp_cobertura_id,
													  PRODUCAO.nome,
													  PRODUCAO.descricao,
													  PRODUCAO.tipo_cobertura_basica,
													  PRODUCAO.tipo_cobertura_IEA,
													  PRODUCAO.tipo_cobertura_IPA,
													  PRODUCAO.tipo_cobertura_IPD,
													  PRODUCAO.cobertura_bb,
													  PRODUCAO.situacao,
													  PRODUCAO.palavra_chave,
													  PRODUCAO.dt_inicio_vigencia,
													  PRODUCAO.dt_fim_vigencia,
													  PRODUCAO.NR_VERSAO,
													  PRODUCAO.ENVIADO_BB,
													  PRODUCAO.DT_ENVIO,
													  PRODUCAO.IN_LC_CST 
	
INSERT INTO [seguros_db].dbo.[tp_cobertura_tb](tp_cobertura_id,
											   nome,
											   dt_inclusao,
											   usuario,
											   descricao,
											   tipo_cobertura_basica,
											   tipo_cobertura_IEA,
											   tipo_cobertura_IPA,
											   tipo_cobertura_IPD,
											   cobertura_bb,
											   nome_SEGA)
										SELECT PRODUCAO.tp_cobertura_id,
											   PRODUCAO.nome,
											   @data_dia,
											   '14271121851',
											   PRODUCAO.descricao,
											   PRODUCAO.tipo_cobertura_basica,
											   PRODUCAO.tipo_cobertura_IEA,
											   PRODUCAO.tipo_cobertura_IPA,
											   PRODUCAO.tipo_cobertura_IPD,
											   PRODUCAO.cobertura_bb,
											   PRODUCAO.nome_SEGA
										  FROM [LS_SUS].[seguros_db].dbo.[tp_cobertura_tb] PRODUCAO WITH(NOLOCK)
										 INNER JOIN [LS_SUS].[als_produto_criacao_db].dbo.[CJT_CBT_ITEM_MDLD] als WITH (NOLOCK)
										    ON PRODUCAO.tp_cobertura_id = als.CD_CBT
										  LEFT JOIN [seguros_db].dbo.[tp_cobertura_tb] QUALIDADE WITH(NOLOCK)  
										    ON PRODUCAO.tp_cobertura_id = QUALIDADE.tp_cobertura_id
										 WHERE als.CD_PRD = '1152'
										   AND als.CD_MDLD = 1
										   AND als.CD_ITEM_MDLD = 102
										   AND QUALIDADE.tp_cobertura_id IS NULL
										 GROUP BY PRODUCAO.tp_cobertura_id,
												  PRODUCAO.nome,
												  PRODUCAO.descricao,
												  PRODUCAO.tipo_cobertura_basica,
												  PRODUCAO.tipo_cobertura_IEA,
												  PRODUCAO.tipo_cobertura_IPA,
												  PRODUCAO.tipo_cobertura_IPD,
												  PRODUCAO.cobertura_bb,
												  PRODUCAO.nome_SEGA     
	
	
INSERT INTO [als_produto_db].dbo.[clausula_tb](cod_clausula,
											   dt_inicio_vigencia,
											   descr_clausula,
											   texto,
											   dt_fim_vigencia,
											   dt_inclusao,
											   usuario,
											   cod_clausula_irb,
											   situacao,
											   NR_VERSAO,
											   NR_PROC_SUSEP,
											   ENVIADO_BB,
											   DT_ENVIO,
											   texto_HTML,
											   ramo_id,
											   cod_empresa)
										SELECT PRODUCAO.cod_clausula,
											   PRODUCAO.dt_inicio_vigencia,
											   PRODUCAO.descr_clausula,
											   '', --PRODUCAO.texto,
											   PRODUCAO.dt_fim_vigencia,
											   @data_dia,
											   '14271121851',
											   PRODUCAO.cod_clausula_irb,
											   PRODUCAO.situacao,
											   PRODUCAO.NR_VERSAO,
											   PRODUCAO.NR_PROC_SUSEP,
											   PRODUCAO.ENVIADO_BB,
											   PRODUCAO.DT_ENVIO,
											   NULL, --PRODUCAO.texto_HTML,
											   PRODUCAO.ramo_id,
											   PRODUCAO.cod_empresa
										  FROM [LS_SUS].[als_produto_db].dbo.[clausula_tb] PRODUCAO WITH(NOLOCK)
										 INNER JOIN [LS_SUS].[als_produto_db].dbo.[CLSL_ITEM_MDLD] als WITH (NOLOCK) 
										    ON PRODUCAO.cod_clausula = als.CD_CLSL
										  LEFT JOIN [als_produto_db].dbo.[clausula_tb] QUALIDADE WITH(NOLOCK)  
										    ON PRODUCAO.cod_clausula = QUALIDADE.cod_clausula
										   AND PRODUCAO.NR_VERSAO = QUALIDADE.NR_VERSAO
										 WHERE als.CD_PRD = '1152'
										   AND als.CD_MDLD = 1
										   AND als.CD_ITEM_MDLD = 102   
										   AND QUALIDADE.cod_clausula IS NULL
										 GROUP BY PRODUCAO.cod_clausula,
												  PRODUCAO.dt_inicio_vigencia,
												  PRODUCAO.descr_clausula,
												  PRODUCAO.dt_fim_vigencia,
												  PRODUCAO.cod_clausula_irb,
												  PRODUCAO.situacao,
												  PRODUCAO.NR_VERSAO,
												  PRODUCAO.NR_PROC_SUSEP,
												  PRODUCAO.ENVIADO_BB,
												  PRODUCAO.DT_ENVIO,
												  PRODUCAO.ramo_id,
												  PRODUCAO.cod_empresa 
												  
UPDATE QUALIDADE
   SET QUALIDADE.texto = PRODUCAO.texto,
	   QUALIDADE.texto_HTML = PRODUCAO.texto_HTML
  FROM [als_produto_db].dbo.[clausula_tb] QUALIDADE
 INNER JOIN [LS_SUS].[als_produto_db].dbo.[clausula_tb] PRODUCAO WITH(NOLOCK)
    ON QUALIDADE.cod_clausula = PRODUCAO.cod_clausula
   AND QUALIDADE.NR_VERSAO = PRODUCAO.NR_VERSAO
WHERE QUALIDADE.texto_HTML IS NULL AND 1=2
	
INSERT INTO [seguros_db].dbo.[clausula_tb](cod_clausula,
										   dt_inicio_vigencia,
										   descr_clausula,
										   texto,
										   dt_fim_vigencia,
										   dt_inclusao,
										   usuario,
										   cod_clausula_irb,
										   ramo_id,
										   cod_empresa)
									SELECT PRODUCAO.cod_clausula,
										   PRODUCAO.dt_inicio_vigencia,
										   PRODUCAO.descr_clausula,
										   PRODUCAO.texto,
										   PRODUCAO.dt_fim_vigencia,
										   @data_dia,
										   '14271121851',
										   PRODUCAO.cod_clausula_irb,
										   PRODUCAO.ramo_id,
										   PRODUCAO.cod_empresa
									   FROM [LS_SUS].[seguros_db].dbo.[clausula_tb] PRODUCAO WITH(NOLOCK)
									  INNER JOIN [LS_SUS].[als_produto_db].dbo.[CLSL_ITEM_MDLD] als WITH (NOLOCK) 
									     ON PRODUCAO.cod_clausula = als.CD_CLSL
									   LEFT JOIN [seguros_db].dbo.[clausula_tb] QUALIDADE WITH(NOLOCK)  
									     ON PRODUCAO.cod_clausula = QUALIDADE.cod_clausula
									    AND PRODUCAO.dt_inicio_vigencia = QUALIDADE.dt_inicio_vigencia
									  WHERE als.CD_PRD = '1152'
									    AND als.CD_MDLD = 1
									    AND als.CD_ITEM_MDLD = 102   
									    AND QUALIDADE.cod_clausula IS NULL
	
INSERT INTO [als_produto_db].dbo.[CLSL](CD_CLSL,
										DESCR_CLAUSULA,
										COD_CLAUSULA_IRB,
										TEXTO,
										TIPO,
										TX_PRC_SUSP,
										USUARIO,
										DT_INCLUSAO,
										ramo_id,
										cod_empresa)
								 SELECT PRODUCAO.CD_CLSL,
										PRODUCAO.DESCR_CLAUSULA,
										PRODUCAO.COD_CLAUSULA_IRB,
										PRODUCAO.TEXTO,
										PRODUCAO.TIPO,
										PRODUCAO.TX_PRC_SUSP,
									    '14271121851',
									    @data_dia,
									    PRODUCAO.ramo_id,
										PRODUCAO.cod_empresa
								   FROM [LS_SUS].[als_produto_db].dbo.[CLSL] PRODUCAO WITH(NOLOCK)
								  INNER JOIN [LS_SUS].[als_produto_db].dbo.[CLSL_ITEM_MDLD] als WITH (NOLOCK) 
								     ON PRODUCAO.CD_CLSL = als.CD_CLSL
								   LEFT JOIN [als_produto_db].dbo.[CLSL] QUALIDADE WITH(NOLOCK)
								     ON PRODUCAO.CD_CLSL = QUALIDADE.CD_CLSL
								  WHERE als.CD_PRD = '1152'
									AND als.CD_MDLD = 1
									AND als.CD_ITEM_MDLD = 102    
									AND QUALIDADE.CD_CLSL IS NULL  
	
INSERT INTO [als_produto_criacao_db].dbo.[CLSL](CD_CLSL,
												DESCR_CLAUSULA,
												COD_CLAUSULA_IRB,
												TEXTO,
												TIPO,
												USUARIO,
												DT_INCLUSAO,
												TX_PRC_SUSP,
												ramo_id,
												cod_empresa)
										 SELECT PRODUCAO.CD_CLSL,
												PRODUCAO.DESCR_CLAUSULA,
												PRODUCAO.COD_CLAUSULA_IRB,
												PRODUCAO.TEXTO,
												PRODUCAO.TIPO,
												'14271121851',
												@data_dia,
												PRODUCAO.TX_PRC_SUSP,
												PRODUCAO.ramo_id,
												PRODUCAO.cod_empresa
										   FROM [LS_SUS].[als_produto_criacao_db].dbo.[CLSL] PRODUCAO WITH(NOLOCK)
										  INNER JOIN [LS_SUS].[als_produto_db].dbo.[CLSL_ITEM_MDLD] als WITH (NOLOCK) 
										     ON PRODUCAO.CD_CLSL = als.CD_CLSL
										   LEFT JOIN [als_produto_criacao_db].dbo.[CLSL] QUALIDADE WITH(NOLOCK)  
										     ON PRODUCAO.CD_CLSL = QUALIDADE.CD_CLSL
										  WHERE als.CD_PRD = '1152'
											AND als.CD_MDLD = 1
											AND als.CD_ITEM_MDLD = 102   										     
											AND QUALIDADE.CD_CLSL IS NULL
	
INSERT INTO [als_produto_db].dbo.[clsl_hist_tb](CD_CLSL,
												NR_VERSAO,
												DT_INICIO_VIGENCIA,
												DT_FIM_VIGENCIA,
												DESCR_CLAUSULA,
												COD_CLAUSULA_IRB,
												TEXTO,
												TIPO,
												TX_PRC_SUSP,
												USUARIO,
												DT_INCLUSAO,
												ramo_id,
												cod_empresa)
										 SELECT PRODUCAO.CD_CLSL,
												PRODUCAO.NR_VERSAO,
												PRODUCAO.DT_INICIO_VIGENCIA,
												PRODUCAO.DT_FIM_VIGENCIA,
												PRODUCAO.DESCR_CLAUSULA,
												PRODUCAO.COD_CLAUSULA_IRB,
												PRODUCAO.TEXTO,
												PRODUCAO.TIPO,
												PRODUCAO.TX_PRC_SUSP,
												'14271121851',
												@data_dia,
												PRODUCAO.ramo_id,
												PRODUCAO.cod_empresa
										   FROM [LS_SUS].[als_produto_db].dbo.[clsl_hist_tb] PRODUCAO WITH(NOLOCK)
										  INNER JOIN [LS_SUS].[als_produto_db].dbo.[CLSL_ITEM_MDLD] als WITH (NOLOCK) 
										     ON PRODUCAO.CD_CLSL = als.CD_CLSL
										   LEFT JOIN [als_produto_db].dbo.[clsl_hist_tb] QUALIDADE WITH(NOLOCK)  
										     ON PRODUCAO.CD_CLSL = QUALIDADE.CD_CLSL
										    AND PRODUCAO.NR_VERSAO = QUALIDADE.NR_VERSAO
										  WHERE als.CD_PRD = '1152'
											AND als.CD_MDLD = 1
											AND als.CD_ITEM_MDLD = 102
											AND QUALIDADE.CD_CLSL IS NULL
	
INSERT INTO [als_produto_criacao_db].dbo.[clsl_hist_tb](CD_CLSL,
														NR_VERSAO,
														DT_INICIO_VIGENCIA,
														DT_FIM_VIGENCIA,
														DESCR_CLAUSULA,
														COD_CLAUSULA_IRB,
														TEXTO,
														TIPO,
														USUARIO,
														DT_INCLUSAO,
														TX_PRC_SUSP,
														ramo_id,
														cod_empresa)
												 SELECT PRODUCAO.CD_CLSL,
														PRODUCAO.NR_VERSAO,
														PRODUCAO.DT_INICIO_VIGENCIA,
														PRODUCAO.DT_FIM_VIGENCIA,
														PRODUCAO.DESCR_CLAUSULA,
														PRODUCAO.COD_CLAUSULA_IRB,
														PRODUCAO.TEXTO,
														PRODUCAO.TIPO,
														'14271121851',
														@data_dia,
														PRODUCAO.TX_PRC_SUSP,
														PRODUCAO.ramo_id,
														PRODUCAO.cod_empresa
												   FROM [LS_SUS].[als_produto_criacao_db].dbo.[clsl_hist_tb] PRODUCAO WITH(NOLOCK)
												  INNER JOIN [LS_SUS].[als_produto_db].dbo.[CLSL_ITEM_MDLD] als WITH (NOLOCK)
													 ON PRODUCAO.CD_CLSL = als.CD_CLSL
												   LEFT JOIN [als_produto_criacao_db].dbo.[clsl_hist_tb] QUALIDADE WITH(NOLOCK)
												     ON PRODUCAO.CD_CLSL = QUALIDADE.CD_CLSL
												    AND PRODUCAO.NR_VERSAO = QUALIDADE.NR_VERSAO
												   WHERE als.CD_PRD = '1152'
													 AND als.CD_MDLD = 1
													 AND als.CD_ITEM_MDLD = 102
													 AND QUALIDADE.CD_CLSL IS NULL	 
	
INSERT INTO [als_produto_db].dbo.[tx_lnh_clsl](CD_CLSL,
											   NR_SEQL_LNH_CLSL,
											   DT_INC_LNH_CLSL,
											   DT_FIM_LNH_CLSL,
											   CD_EST_LNH_CLSL,
											   TX_CTU_LNH_CLSL,
											   TX_INFM_MTV_RSA,
											   ENVIADO_BB,
											   DT_ENVIO)
										SELECT PRODUCAO.CD_CLSL,
											   PRODUCAO.NR_SEQL_LNH_CLSL,
											   PRODUCAO.DT_INC_LNH_CLSL,
											   PRODUCAO.DT_FIM_LNH_CLSL,
											   PRODUCAO.CD_EST_LNH_CLSL,
											   PRODUCAO.TX_CTU_LNH_CLSL,
											   PRODUCAO.TX_INFM_MTV_RSA,
											   PRODUCAO.ENVIADO_BB,
											   PRODUCAO.DT_ENVIO
										  FROM [LS_SUS].[als_produto_db].dbo.[tx_lnh_clsl] PRODUCAO WITH(NOLOCK)
										 INNER JOIN [LS_SUS].[als_produto_db].dbo.[CLSL_ITEM_MDLD] als WITH (NOLOCK) 
										    ON PRODUCAO.CD_CLSL = als.CD_CLSL
										  LEFT JOIN [als_produto_db].dbo.[tx_lnh_clsl] QUALIDADE WITH(NOLOCK)  
										    ON PRODUCAO.CD_CLSL = QUALIDADE.CD_CLSL
										   AND PRODUCAO.NR_SEQL_LNH_CLSL = QUALIDADE.NR_SEQL_LNH_CLSL
										 WHERE als.CD_PRD = '1152'
										   AND als.CD_MDLD = 1
										   AND als.CD_ITEM_MDLD = 102
										   AND QUALIDADE.CD_CLSL IS NULL   
	
INSERT INTO [als_produto_criacao_db].dbo.[tx_lnh_clsl](CD_CLSL,
													   NR_SEQL_LNH_CLSL,
													   DT_INC_LNH_CLSL,
													   DT_FIM_LNH_CLSL,
													   CD_EST_LNH_CLSL,
													   TX_CTU_LNH_CLSL,
													   TX_INFM_MTV_RSA,
													   ENVIADO_BB,
													   DT_ENVIO)
												SELECT PRODUCAO.CD_CLSL,
													   PRODUCAO.NR_SEQL_LNH_CLSL,
													   PRODUCAO.DT_INC_LNH_CLSL,
													   PRODUCAO.DT_FIM_LNH_CLSL,
													   PRODUCAO.CD_EST_LNH_CLSL,
													   PRODUCAO.TX_CTU_LNH_CLSL,
													   PRODUCAO.TX_INFM_MTV_RSA,
													   PRODUCAO.ENVIADO_BB,
													   PRODUCAO.DT_ENVIO
												 FROM [LS_SUS].[als_produto_criacao_db].dbo.[tx_lnh_clsl] PRODUCAO WITH(NOLOCK)
												INNER JOIN [LS_SUS].[als_produto_db].dbo.[CLSL_ITEM_MDLD] als WITH (NOLOCK) 
												   ON PRODUCAO.CD_CLSL = als.CD_CLSL
												 LEFT JOIN [als_produto_criacao_db].dbo.[tx_lnh_clsl] QUALIDADE WITH(NOLOCK)  
												   ON PRODUCAO.CD_CLSL = QUALIDADE.CD_CLSL
												  AND PRODUCAO.NR_SEQL_LNH_CLSL = QUALIDADE.NR_SEQL_LNH_CLSL
												  AND PRODUCAO.DT_INC_LNH_CLSL = QUALIDADE.DT_INC_LNH_CLSL
												WHERE als.CD_PRD = '1152'
												  AND als.CD_MDLD = 1
												  AND als.CD_ITEM_MDLD = 102   
												  AND QUALIDADE.CD_CLSL IS NULL
	
INSERT INTO [als_produto_db].dbo.[tx_lnh_clsl_hist_tb](CD_CLSL,
													   NR_SEQL_LNH_CLSL,
													   NR_VERSAO,
													   DT_INC_LNH_CLSL,
													   CD_EST_LNH_CLSL,
													   DT_FIM_LNH_CLSL,
													   TX_CTU_LNH_CLSL,
													   TX_INFM_MTV_RSA,
													   USUARIO,
													   DT_INCLUSAO)
												SELECT PRODUCAO.CD_CLSL,
													   PRODUCAO.NR_SEQL_LNH_CLSL,
													   PRODUCAO.NR_VERSAO,
													   PRODUCAO.DT_INC_LNH_CLSL,
													   PRODUCAO.CD_EST_LNH_CLSL,
													   PRODUCAO.DT_FIM_LNH_CLSL,
													   PRODUCAO.TX_CTU_LNH_CLSL,
													   PRODUCAO.TX_INFM_MTV_RSA,
													   '14271121851',
													   @data_dia
												 FROM [LS_SUS].[als_produto_db].dbo.[tx_lnh_clsl_hist_tb] PRODUCAO WITH(NOLOCK)
												INNER JOIN [LS_SUS].[als_produto_db].dbo.[CLSL_ITEM_MDLD] als WITH (NOLOCK) 
												   ON PRODUCAO.CD_CLSL = als.CD_CLSL
												 LEFT JOIN [als_produto_db].dbo.[tx_lnh_clsl_hist_tb] QUALIDADE WITH(NOLOCK)  
												   ON PRODUCAO.CD_CLSL = QUALIDADE.CD_CLSL
												  AND PRODUCAO.NR_SEQL_LNH_CLSL = QUALIDADE.NR_SEQL_LNH_CLSL
												  AND PRODUCAO.NR_VERSAO = QUALIDADE.NR_VERSAO
												WHERE als.CD_PRD = '1152'
												  AND als.CD_MDLD = 1
												  AND als.CD_ITEM_MDLD = 102
												  AND QUALIDADE.CD_CLSL IS NULL    
	
INSERT INTO [als_produto_criacao_db].dbo.[tx_lnh_clsl_hist_tb](CD_CLSL,
															   NR_SEQL_LNH_CLSL,
															   DT_INC_LNH_CLSL,
															   NR_VERSAO,
															   CD_EST_LNH_CLSL,
															   DT_FIM_LNH_CLSL,
															   TX_CTU_LNH_CLSL,
															   TX_INFM_MTV_RSA,
															   USUARIO,
															   DT_INCLUSAO,
															   dt_envio,
															   enviado_bb)
														SELECT PRODUCAO.CD_CLSL,
															   PRODUCAO.NR_SEQL_LNH_CLSL,
															   PRODUCAO.DT_INC_LNH_CLSL,
															   PRODUCAO.NR_VERSAO,
															   PRODUCAO.CD_EST_LNH_CLSL,
															   PRODUCAO.DT_FIM_LNH_CLSL,
															   PRODUCAO.TX_CTU_LNH_CLSL,
															   PRODUCAO.TX_INFM_MTV_RSA,
															   '14271121851',
															   @data_dia,
															   PRODUCAO.dt_envio,
															   PRODUCAO.enviado_bb
														  FROM [LS_SUS].[als_produto_criacao_db].dbo.[tx_lnh_clsl_hist_tb] PRODUCAO WITH(NOLOCK)
														 INNER JOIN [LS_SUS].[als_produto_db].dbo.[CLSL_ITEM_MDLD] als WITH (NOLOCK) 
														    ON PRODUCAO.CD_CLSL = als.CD_CLSL
														  LEFT JOIN [als_produto_criacao_db].dbo.[tx_lnh_clsl_hist_tb] QUALIDADE WITH(NOLOCK)  
														    ON PRODUCAO.CD_CLSL = QUALIDADE.CD_CLSL
														   AND PRODUCAO.NR_SEQL_LNH_CLSL = QUALIDADE.NR_SEQL_LNH_CLSL
														   AND PRODUCAO.DT_INC_LNH_CLSL = QUALIDADE.DT_INC_LNH_CLSL
														   AND PRODUCAO.NR_VERSAO = QUALIDADE.NR_VERSAO
														 WHERE als.CD_PRD = '1152'
														   AND als.CD_MDLD = 1
														   AND als.CD_ITEM_MDLD = 102
														   AND QUALIDADE.CD_CLSL IS NULL   
	
INSERT INTO [als_produto_db].dbo.[questionario_tb](questionario_id,
												   nome,
												   situacao,
												   dt_inclusao,
												   usuario,
												   ENVIADO_BB,
												   DT_ENVIO,
												   tipo,
												   tp_sistema_id)
											SELECT PRODUCAO.questionario_id,
												   PRODUCAO.nome,
												   PRODUCAO.situacao,
												   @data_dia,
												   '14271121851',
												   PRODUCAO.ENVIADO_BB,
												   PRODUCAO.DT_ENVIO,
												   PRODUCAO.tipo,
												   PRODUCAO.tp_sistema_id
									 FROM [LS_SUS].[als_produto_db].dbo.[questionario_tb] PRODUCAO WITH(NOLOCK)
									INNER JOIN [LS_SUS].[als_produto_db].dbo.[qstn_item_mdld] als WITH (NOLOCK) 
									   ON PRODUCAO.questionario_id = als.NR_QSTN
									 LEFT JOIN [als_produto_db].dbo.[questionario_tb] QUALIDADE WITH(NOLOCK)  
									   ON PRODUCAO.questionario_id = QUALIDADE.questionario_id
									WHERE als.CD_PRD = '1152'
									  AND als.CD_MDLD = 1
									  AND als.CD_ITEM_MDLD = 102   
									  AND QUALIDADE.questionario_id IS NULL
	
INSERT INTO [als_produto_db].dbo.[questionario_pergunta_tb](questionario_id,
															pergunta_id,
															num_ordem_pergunta,
															num_pergunta_precedente,
															grupo_dominio_resposta_id,
															dominio_resposta_id,
															dt_inclusao,
															usuario,
															ENVIADO_BB,
															DT_ENVIO,
															situacao,
															resposta_padrao,
															imprimir,
															ind_obrigatoriedade,
															fl_permitir_alteracao)
													 SELECT PRODUCAO.questionario_id,
															PRODUCAO.pergunta_id,
															PRODUCAO.num_ordem_pergunta,
															PRODUCAO.num_pergunta_precedente,
															PRODUCAO.grupo_dominio_resposta_id,
															PRODUCAO.dominio_resposta_id,
															@data_dia,
															'14271121851',
															PRODUCAO.ENVIADO_BB,
															PRODUCAO.DT_ENVIO,
															PRODUCAO.situacao,
															PRODUCAO.resposta_padrao,
															PRODUCAO.imprimir,
															PRODUCAO.ind_obrigatoriedade,
															PRODUCAO.fl_permitir_alteracao
											 FROM [LS_SUS].[als_produto_db].dbo.[questionario_pergunta_tb] PRODUCAO WITH(NOLOCK)
											INNER JOIN [LS_SUS].[als_produto_db].dbo.[qstn_item_mdld] als WITH (NOLOCK) 
											   ON PRODUCAO.questionario_id = als.NR_QSTN
											 LEFT JOIN [als_produto_db].dbo.[questionario_pergunta_tb] QUALIDADE WITH(NOLOCK)  
											   ON PRODUCAO.questionario_id = QUALIDADE.questionario_id
											  AND PRODUCAO.pergunta_id = QUALIDADE.pergunta_id
											WHERE als.CD_PRD = '1152'
											  AND als.CD_MDLD = 1
											  AND als.CD_ITEM_MDLD = 102      
											  AND QUALIDADE.questionario_id IS NULL
											 
	
INSERT INTO [als_produto_db].dbo.[pergunta_tb](pergunta_id,
											   nome,
											   ind_obrigatoriedade,
											   validacao_objeto_banco_id,
											   grupo_dominio_resposta_id,
											   formato_resposta_id,
											   tamanho_resposta,
											   dt_inclusao,
											   usuario,
											   situacao,
											   tp_questao_referencia_id,
											   tabela_destino,
											   campo_destino,
											   procedure_calcular_resposta,
											   seq_pergunta,
											   ind_posicao,
											   tipo,
											   no_linhas_resposta,
											   campo_habilitado,
											   tp_sistema_id,
											   tabela_destino_endosso,
											   coluna_destino_endosso,
											   coluna_texto_complementar_endosso)
										SELECT PRODUCAO.pergunta_id,		
											   PRODUCAO.nome,
											   PRODUCAO.ind_obrigatoriedade,
											   PRODUCAO.validacao_objeto_banco_id,
											   PRODUCAO.grupo_dominio_resposta_id,
											   PRODUCAO.formato_resposta_id,
											   PRODUCAO.tamanho_resposta,
											   @data_dia,
											   '14271121851',
											   PRODUCAO.situacao,
											   PRODUCAO.tp_questao_referencia_id,
											   PRODUCAO.tabela_destino,
											   PRODUCAO.campo_destino,
											   PRODUCAO.procedure_calcular_resposta,
											   PRODUCAO.seq_pergunta,
											   PRODUCAO.ind_posicao,
											   PRODUCAO.tipo,
											   PRODUCAO.no_linhas_resposta,
											   PRODUCAO.campo_habilitado,
											   PRODUCAO.tp_sistema_id,
											   PRODUCAO.tabela_destino_endosso,
											   PRODUCAO.coluna_destino_endosso,
											   PRODUCAO.coluna_texto_complementar_endosso
										  FROM [LS_SUS].[als_produto_db].dbo.[pergunta_tb] PRODUCAO WITH (NOLOCK)
										 INNER JOIN [LS_SUS].[als_produto_db].dbo.[questionario_pergunta_tb] liga WITH (NOLOCK)
											ON PRODUCAO.pergunta_id = liga.pergunta_id
										 INNER JOIN [LS_SUS].[als_produto_db].dbo.[qstn_item_mdld] als WITH (NOLOCK)
											ON liga.questionario_id = als.NR_QSTN 	
										  LEFT JOIN [als_produto_db].dbo.[pergunta_tb] QUALIDADE WITH (NOLOCK)	
										    ON PRODUCAO.pergunta_id = QUALIDADE.pergunta_id
										   AND PRODUCAO.seq_pergunta = QUALIDADE.seq_pergunta 
										 WHERE als.CD_PRD = '1152'
										   AND als.CD_MDLD = 1
										   AND als.CD_ITEM_MDLD = 102
										   AND QUALIDADE.pergunta_id IS NULL				  
INSERT INTO [seguros_db].dbo.[convenio_tb](num_convenio,
										   agencia_id,
										   conta_corrente_id,
										   banco_id,
										   tp_repasse,
										   seq_nosso_numero,
										   dt_inclusao,
										   usuario,
										   retencao_iof,
										   retencao_comissao,
										   gerar_nosso_numero,
										   num_carteira,
										   canal_id,
										   retencao_pro_labore,
										   substituido,
										   TAMANHO_NOSSO_NUMERO)
									SELECT PRODUCAO.num_convenio,
										   PRODUCAO.agencia_id,
										   PRODUCAO.conta_corrente_id,
										   PRODUCAO.banco_id,
										   PRODUCAO.tp_repasse,
										   PRODUCAO.seq_nosso_numero,
										   @data_dia,
										   '14271121851',
										   PRODUCAO.retencao_iof,
										   PRODUCAO.retencao_comissao,
										   PRODUCAO.gerar_nosso_numero,
										   PRODUCAO.num_carteira,
										   PRODUCAO.canal_id,
										   PRODUCAO.retencao_pro_labore,
										   PRODUCAO.substituido,
										   PRODUCAO.TAMANHO_NOSSO_NUMERO
									  FROM [LS_SUS].[seguros_db].dbo.[convenio_tb] PRODUCAO WITH (NOLOCK)
									 INNER JOIN [LS_SUS].[seguros_db].dbo.[tp_movimentacao_financ_tb] als WITH (NOLOCK)
									    ON PRODUCAO.num_convenio = als.num_convenio
									  LEFT JOIN [seguros_db].dbo.[convenio_tb] QUALIDADE WITH (NOLOCK)  
									    ON PRODUCAO.num_convenio = QUALIDADE.num_convenio
									 WHERE als.produto_id = '1152'
									   AND QUALIDADE.num_convenio IS NULL
									   
/*Retirar partes comentadas abaixo para subir fix repair apos a implantação da demanda*/
	
   IF NOT EXISTS(SELECT 1
				   FROM [als_produto_criacao_db].dbo.[dados_publicacao_tb] WITH (NOLOCK)
				  WHERE CD_PRD = '1152'
					AND CD_MDLD = 1
					AND CD_ITEM_MDLD = 102)
	  BEGIN
			INSERT INTO [als_produto_criacao_db].dbo.[dados_publicacao_tb] (cd_prd,
																			cd_mdld,
																			cd_item_mdld,
																			seguradora_cod_susep,
																			nome,
																			acerto_co_seguro,
																			num_max_parcela,
																			tp_renovacao,
																			prazo_seguro,
																			apolice_coletiva,
																			prazo_recusa_proposta,
																			prazo_doc_comp,
																			tolerancia_doc_comp,
																			produto_ativo,
																			assistencia,
																			produto_anterior_id,
																			dt_migracao,
																			processa_cobranca,
																			processa_cancelamento,
																			max_parcelas_atraso,
																			taxa_servico,
																			aceite_bb,
																			tp_calc_restituicao,
																			max_dias_atraso,
																			tp_emissao,
																			reintegracao_automatica,
																			emissao_on_line,
																			num_proc_susep,
																			apol_envia_cliente,
																			apol_num_vias,
																			apol_envia_congenere,
																			prazo_restituicao_integral,
																			prazo_aceite,
																			processa_restituicao_automatica,
																			restituicao_tp_beneficiario,
																			restituicao_cliente_id,
																			endosso_on_line,
																			cod_tp_reserva,
																			cod_tp_baixa,
																			retorno_endosso,
																			origem,
																			tp_fatura_web,
																			limite_idade_renov,
																			sigla_sistema,
																			cobrar_parc_inadimplentes_com_atual,
																			bloquear_cobranca_enquanto_inadimplente,
																			est_cliente_id,
																			perc_comissao_est,
																			moeda_id,
																			validade_premio,
																			validade_capital,
																			erro_aceitavel_val_parcela_1,
																			erro_aceitavel_val_parcela_demais,
																			erro_aceitavel_val_premio,
																			cod_seguradora_congenere,
																			perc_participacao_congenere,
																			perc_despesa_lider_congenere,
																			perc_juros,
																			perc_min_corretagem,
																			perc_max_corretagem,
																			perc_padrao_corretagem,
																			objetivo,
																			participacao,
																			val_minimo_premio,
																			qtd_min_cobertura,
																			cod_produto_bb,
																			val_minimo_endosso,
																			processa_reducao_is,
																			processa_reintegracao_is,
																			gera_retorno_bb_alteracao_is,
																			processa_cancelamento_ITEM,
																			max_dias_atraso_ITEM,
																			permite_aviso_proposta_basica,
																			val_lim_auto_is,
																			tp_calculo_iof,
																			max_dias_atraso_primeira,
																			processa_pre_analise,
																			max_dias_atraso_endosso,
																			processa_cancelamento_endosso,
																			indice_atualizacao_sinistro_id,
																			periodo_atualizacao_sinistro,
																			perc_custo_endosso,
																			val_custo_endosso_max,
																			perc_custo_apolice,
																			val_custo_apolice_max,
																			val_custo_uss,
																			perc_remuneracao_servico,
																			dt_inicio_vigencia,
																			limite_is,
																			idade_min,
																			idade_max,
																			limite_is_faixa,
																			produto_id_bb,
																			ramo_id_bb,
																			subramo_id_bb,
																			tp_wf_id,
																			tp_operacao_financ_id_pmro,
																			moeda_operacao_financ_id_pmro,
																			numero_convenio_pmro,
																			tp_operacao_financ_id_otr,
																			moeda_operacao_financ_id_otr,
																			numero_convenio_otr,
																			dt_inclusao,
																			usuario,
																			termo_quitacao_anual,
																			alterar_profissao_cliente,
																			tp_operacao_financ_id_dbt,
																			moeda_operacao_financ_id_dbt,
																			numero_convenio_dbt,
																			perc_erro_aceitavel,
																			perc_erro_aceitavel_pgto,
																			indice_atualizacao_capital_id,
																			periodo_atualizacao_capital,
																			atualiza_psl,
																			indice_atualizacao_psl_id,
																			tp_ativ_id_pre_analise,
																			tp_tarefa_id_aceite,
																			tp_tarefa_id_recusa,
																			enviar_bb_reativacao_baixa_cancelados,
																			calcula_restituicao_baixa_cancelados)
																	 SELECT cd_prd,
   1 AS cd_mdld,
   102 AS cd_item_mdld,
																			seguradora_cod_susep,
																			nome,
																			acerto_co_seguro,
																			num_max_parcela,
																			tp_renovacao,
																			prazo_seguro,
																			apolice_coletiva,
																			prazo_recusa_proposta,
																			prazo_doc_comp,
																			tolerancia_doc_comp,
																			produto_ativo,
																			assistencia,
																			produto_anterior_id,
																			dt_migracao,
																			processa_cobranca,
																			processa_cancelamento,
																			max_parcelas_atraso,
																			taxa_servico,
																			aceite_bb,
																			tp_calc_restituicao,
																			max_dias_atraso,
																			tp_emissao,
																			reintegracao_automatica,
																			emissao_on_line,
																			num_proc_susep,
																			apol_envia_cliente,
																			apol_num_vias,
																			apol_envia_congenere,
																			prazo_restituicao_integral,
																			prazo_aceite,
																			processa_restituicao_automatica,
																			restituicao_tp_beneficiario,
																			restituicao_cliente_id,
																			endosso_on_line,
																			cod_tp_reserva,
																			cod_tp_baixa,
																			retorno_endosso,
																			origem,
																			tp_fatura_web,
																			limite_idade_renov,
																			sigla_sistema,
																			cobrar_parc_inadimplentes_com_atual,
																			bloquear_cobranca_enquanto_inadimplente,
																			est_cliente_id,
																			perc_comissao_est,
																			moeda_id,
																			validade_premio,
																			validade_capital,
																			erro_aceitavel_val_parcela_1,
																			erro_aceitavel_val_parcela_demais,
																			erro_aceitavel_val_premio,
																			cod_seguradora_congenere,
																			perc_participacao_congenere,
																			perc_despesa_lider_congenere,
																			perc_juros,
																			perc_min_corretagem,
																			perc_max_corretagem,
																			perc_padrao_corretagem,
																			objetivo,
																			participacao,
																			val_minimo_premio,
																			qtd_min_cobertura,
																			cod_produto_bb,
																			val_minimo_endosso,
																			processa_reducao_is,
																			processa_reintegracao_is,
																			gera_retorno_bb_alteracao_is,
																			processa_cancelamento_ITEM,
																			max_dias_atraso_ITEM,
																			permite_aviso_proposta_basica,
																			val_lim_auto_is,
																			tp_calculo_iof,
																			max_dias_atraso_primeira,
																			processa_pre_analise,
																			max_dias_atraso_endosso,
																			processa_cancelamento_endosso,
																			indice_atualizacao_sinistro_id,
																			periodo_atualizacao_sinistro,
																			perc_custo_endosso,
																			val_custo_endosso_max,
																			perc_custo_apolice,
																			val_custo_apolice_max,
																			val_custo_uss,
																			perc_remuneracao_servico,
																			dt_inicio_vigencia,
																			limite_is,
																			idade_min,
																			idade_max,
																			limite_is_faixa,
																			produto_id_bb,
																			ramo_id_bb,
																			subramo_id_bb,
																			tp_wf_id,
																			tp_operacao_financ_id_pmro,
																			moeda_operacao_financ_id_pmro,
																			numero_convenio_pmro,
																			tp_operacao_financ_id_otr,
																			moeda_operacao_financ_id_otr,
																			numero_convenio_otr,
																			@data_dia,
																			'14271121851',
																			termo_quitacao_anual,
																			alterar_profissao_cliente,
																			tp_operacao_financ_id_dbt,
																			moeda_operacao_financ_id_dbt,
																			numero_convenio_dbt,
																			perc_erro_aceitavel,
																			perc_erro_aceitavel_pgto,
																			indice_atualizacao_capital_id,
																			periodo_atualizacao_capital,
																			atualiza_psl,
																			indice_atualizacao_psl_id,
																			tp_ativ_id_pre_analise,
																			tp_tarefa_id_aceite,
																			tp_tarefa_id_recusa,
																			enviar_bb_reativacao_baixa_cancelados,
																			calcula_restituicao_baixa_cancelados
																	   FROM [LS_SUS].[als_produto_criacao_db].dbo.[dados_publicacao_tb] WITH (NOLOCK)
																	  WHERE CD_PRD = '1152'
																		AND CD_MDLD = 2 
																		AND CD_ITEM_MDLD = 202 
	    END
	  ELSE
	    BEGIN
	      UPDATE QUALID 
	         SET QUALID.seguradora_cod_susep = PRODUCAO.seguradora_cod_susep,
				 QUALID.nome = PRODUCAO.nome,
				 QUALID.acerto_co_seguro = PRODUCAO.acerto_co_seguro,
				 QUALID.num_max_parcela = PRODUCAO.num_max_parcela,
				 QUALID.tp_renovacao = PRODUCAO.tp_renovacao,
				 QUALID.prazo_seguro = PRODUCAO.prazo_seguro, 
				 QUALID.apolice_coletiva = PRODUCAO.apolice_coletiva,
				 QUALID.prazo_recusa_proposta = PRODUCAO.prazo_recusa_proposta,
				 QUALID.prazo_doc_comp = PRODUCAO.prazo_doc_comp,
				 QUALID.tolerancia_doc_comp = PRODUCAO.tolerancia_doc_comp,
				 QUALID.produto_ativo = PRODUCAO.produto_ativo,
				 QUALID.assistencia = PRODUCAO.assistencia,
				 QUALID.produto_anterior_id = PRODUCAO.produto_anterior_id,
				 QUALID.dt_migracao = PRODUCAO.dt_migracao,
				 QUALID.processa_cobranca = PRODUCAO.processa_cobranca,
				 QUALID.processa_cancelamento = PRODUCAO.processa_cancelamento,
				 QUALID.max_parcelas_atraso = PRODUCAO.max_parcelas_atraso,
				 QUALID.taxa_servico = PRODUCAO.taxa_servico,
				 QUALID.aceite_bb = PRODUCAO.aceite_bb,
				 QUALID.tp_calc_restituicao = PRODUCAO.tp_calc_restituicao,
				 QUALID.max_dias_atraso = PRODUCAO.max_dias_atraso,
				 QUALID.tp_emissao = PRODUCAO.tp_emissao,
				 QUALID.reintegracao_automatica = PRODUCAO.reintegracao_automatica,
				 QUALID.emissao_on_line = PRODUCAO.emissao_on_line,
				 QUALID.num_proc_susep = PRODUCAO.num_proc_susep,
				 QUALID.apol_envia_cliente = PRODUCAO.apol_envia_cliente,
				 QUALID.apol_num_vias = PRODUCAO.apol_num_vias,
				 QUALID.apol_envia_congenere = PRODUCAO.apol_envia_congenere,
				 QUALID.prazo_restituicao_integral = PRODUCAO.prazo_restituicao_integral,
				 QUALID.prazo_aceite = PRODUCAO.prazo_aceite,
				 QUALID.processa_restituicao_automatica = PRODUCAO.processa_restituicao_automatica,
				 QUALID.restituicao_tp_beneficiario = PRODUCAO.restituicao_tp_beneficiario,
				 QUALID.restituicao_cliente_id = PRODUCAO.restituicao_cliente_id,
				 QUALID.endosso_on_line = PRODUCAO.endosso_on_line,
				 QUALID.cod_tp_reserva = PRODUCAO.cod_tp_reserva,
				 QUALID.cod_tp_baixa = PRODUCAO.cod_tp_baixa,
				 QUALID.retorno_endosso = PRODUCAO.retorno_endosso,
				 QUALID.origem = PRODUCAO.origem,
				 QUALID.tp_fatura_web = PRODUCAO.tp_fatura_web,
				 QUALID.limite_idade_renov = PRODUCAO.limite_idade_renov,
				 QUALID.sigla_sistema = PRODUCAO.sigla_sistema,
				 QUALID.cobrar_parc_inadimplentes_com_atual = PRODUCAO.cobrar_parc_inadimplentes_com_atual,
				 QUALID.bloquear_cobranca_enquanto_inadimplente = PRODUCAO.bloquear_cobranca_enquanto_inadimplente,
				 QUALID.est_cliente_id = PRODUCAO.est_cliente_id,
				 QUALID.perc_comissao_est = PRODUCAO.perc_comissao_est,
				 QUALID.moeda_id = PRODUCAO.moeda_id,
				 QUALID.validade_premio = PRODUCAO.validade_premio,
				 QUALID.validade_capital = PRODUCAO.validade_capital,
				 QUALID.erro_aceitavel_val_parcela_1 = PRODUCAO.erro_aceitavel_val_parcela_1,
				 QUALID.erro_aceitavel_val_parcela_demais = PRODUCAO.erro_aceitavel_val_parcela_demais,
				 QUALID.erro_aceitavel_val_premio = PRODUCAO.erro_aceitavel_val_premio,
				 QUALID.cod_seguradora_congenere = PRODUCAO.cod_seguradora_congenere,
				 QUALID.perc_participacao_congenere = PRODUCAO.perc_participacao_congenere,
				 QUALID.perc_despesa_lider_congenere = PRODUCAO.perc_despesa_lider_congenere,
				 QUALID.perc_juros = PRODUCAO.perc_juros,
				 QUALID.perc_min_corretagem = PRODUCAO.perc_min_corretagem,
				 QUALID.perc_max_corretagem = PRODUCAO.perc_max_corretagem,
				 QUALID.perc_padrao_corretagem = PRODUCAO.perc_padrao_corretagem,
				 QUALID.objetivo = PRODUCAO.objetivo,
				 QUALID.participacao = PRODUCAO.participacao,
				 QUALID.val_minimo_premio = PRODUCAO.val_minimo_premio,
				 QUALID.qtd_min_cobertura = PRODUCAO.qtd_min_cobertura,
				 QUALID.cod_produto_bb = PRODUCAO.cod_produto_bb,
				 QUALID.val_minimo_endosso = PRODUCAO.val_minimo_endosso,
				 QUALID.processa_reducao_is = PRODUCAO.processa_reducao_is,
				 QUALID.processa_reintegracao_is = PRODUCAO.processa_reintegracao_is,
				 QUALID.gera_retorno_bb_alteracao_is = PRODUCAO.gera_retorno_bb_alteracao_is,
				 QUALID.processa_cancelamento_ITEM = PRODUCAO.processa_cancelamento_ITEM,
				 QUALID.max_dias_atraso_ITEM = PRODUCAO.max_dias_atraso_ITEM,
				 QUALID.permite_aviso_proposta_basica = PRODUCAO.permite_aviso_proposta_basica,
				 QUALID.val_lim_auto_is = PRODUCAO.val_lim_auto_is,
				 QUALID.tp_calculo_iof = PRODUCAO.tp_calculo_iof,
				 QUALID.max_dias_atraso_primeira = PRODUCAO.max_dias_atraso_primeira,
				 QUALID.processa_pre_analise = PRODUCAO.processa_pre_analise,
				 QUALID.max_dias_atraso_endosso = PRODUCAO.max_dias_atraso_endosso,
				 QUALID.processa_cancelamento_endosso = PRODUCAO.processa_cancelamento_endosso,
				 QUALID.indice_atualizacao_sinistro_id = PRODUCAO.indice_atualizacao_sinistro_id,
				 QUALID.periodo_atualizacao_sinistro = PRODUCAO.periodo_atualizacao_sinistro,
				 QUALID.perc_custo_endosso = PRODUCAO.perc_custo_endosso,
				 QUALID.val_custo_endosso_max = PRODUCAO.val_custo_endosso_max,
				 QUALID.perc_custo_apolice = PRODUCAO.perc_custo_apolice,
				 QUALID.val_custo_apolice_max = PRODUCAO.val_custo_apolice_max,
				 QUALID.val_custo_uss = PRODUCAO.val_custo_uss,
				 QUALID.perc_remuneracao_servico = PRODUCAO.perc_remuneracao_servico,
				 QUALID.dt_inicio_vigencia = PRODUCAO.dt_inicio_vigencia,
				 QUALID.limite_is = PRODUCAO.limite_is,
				 QUALID.idade_min = PRODUCAO.idade_min,
				 QUALID.idade_max = PRODUCAO.idade_max,
				 QUALID.limite_is_faixa = PRODUCAO.limite_is_faixa,
				 QUALID.produto_id_bb = PRODUCAO.produto_id_bb,
				 QUALID.ramo_id_bb = PRODUCAO.ramo_id_bb,
				 QUALID.subramo_id_bb = PRODUCAO.subramo_id_bb,
				 QUALID.tp_wf_id = PRODUCAO.tp_wf_id,
				 QUALID.tp_operacao_financ_id_pmro = PRODUCAO.tp_operacao_financ_id_pmro,
				 QUALID.moeda_operacao_financ_id_pmro = PRODUCAO.moeda_operacao_financ_id_pmro,
				 QUALID.numero_convenio_pmro = PRODUCAO.numero_convenio_pmro,
				 QUALID.tp_operacao_financ_id_otr = PRODUCAO.tp_operacao_financ_id_otr,
				 QUALID.moeda_operacao_financ_id_otr = PRODUCAO.moeda_operacao_financ_id_otr,
				 QUALID.numero_convenio_otr = PRODUCAO.numero_convenio_otr,
				 QUALID.dt_alteracao = @data_dia,
				 QUALID.usuario = '14271121851',
				 QUALID.termo_quitacao_anual = PRODUCAO.termo_quitacao_anual,
				 QUALID.alterar_profissao_cliente = PRODUCAO.alterar_profissao_cliente,
				 QUALID.tp_operacao_financ_id_dbt = PRODUCAO.tp_operacao_financ_id_dbt,
				 QUALID.moeda_operacao_financ_id_dbt = PRODUCAO.moeda_operacao_financ_id_dbt,
				 QUALID.numero_convenio_dbt = PRODUCAO.numero_convenio_dbt,
				 QUALID.perc_erro_aceitavel = PRODUCAO.perc_erro_aceitavel,
				 QUALID.perc_erro_aceitavel_pgto = PRODUCAO.perc_erro_aceitavel_pgto,
				 QUALID.indice_atualizacao_capital_id = PRODUCAO.indice_atualizacao_capital_id,
				 QUALID.periodo_atualizacao_capital = PRODUCAO.periodo_atualizacao_capital,
				 QUALID.atualiza_psl = PRODUCAO.atualiza_psl,
				 QUALID.indice_atualizacao_psl_id = PRODUCAO.indice_atualizacao_psl_id,
				 QUALID.tp_ativ_id_pre_analise = PRODUCAO.tp_ativ_id_pre_analise,
				 QUALID.tp_tarefa_id_aceite = PRODUCAO.tp_tarefa_id_aceite,
				 QUALID.tp_tarefa_id_recusa = PRODUCAO.tp_tarefa_id_aceite,
				 QUALID.enviar_bb_reativacao_baixa_cancelados = PRODUCAO.enviar_bb_reativacao_baixa_cancelados,
				 QUALID.calcula_restituicao_baixa_cancelados = PRODUCAO.calcula_restituicao_baixa_cancelados
			FROM [als_produto_criacao_db].dbo.[dados_publicacao_tb] QUALID
		   INNER JOIN [LS_SUS].[als_produto_criacao_db].dbo.[dados_publicacao_tb] PRODUCAO
			  ON QUALID.cd_prd = PRODUCAO.cd_prd
			 AND QUALID.cd_mdld = PRODUCAO.cd_mdld
			 AND QUALID.cd_item_mdld = PRODUCAO.cd_item_mdld
		   WHERE QUALID.cd_prd = '1152'
			 AND QUALID.cd_mdld = 2
			 AND QUALID.cd_item_mdld = 202
	      END  
	INSERT INTO [als_produto_criacao_db].dbo.[dados_publicacao_forma_pagamento_tb](cd_prd,
																				  cd_mdld,
																				  cd_item_mdld,
																				  forma_pgto_id,
																				  parcela_1,
																				  parcela_demais,
																				  dt_inclusao,
																				  usuario)
																		   SELECT PRODUCAO.cd_prd,
   1 AS cd_mdld,
   102 AS cd_item_mdld,
																				  PRODUCAO.forma_pgto_id,
																				  PRODUCAO.parcela_1,
																				  PRODUCAO.parcela_demais,
																				  @data_dia,
																				  '14271121851'
																		     FROM [LS_SUS].[als_produto_criacao_db].dbo.[dados_publicacao_forma_pagamento_tb] PRODUCAO WITH(NOLOCK)
																		     LEFT JOIN [als_produto_criacao_db].dbo.[dados_publicacao_forma_pagamento_tb] QUALIDADE WITH (NOLOCK)
																		       ON PRODUCAO.cd_prd = QUALIDADE.cd_prd
																		      AND PRODUCAO.cd_mdld = QUALIDADE.cd_mdld
																		      AND PRODUCAO.cd_item_mdld = QUALIDADE.cd_item_mdld
																		      AND PRODUCAO.forma_pgto_id = QUALIDADE.forma_pgto_id  
																		    WHERE PRODUCAO.CD_PRD = '1152'
																			  AND PRODUCAO.CD_MDLD = 2
																		      AND PRODUCAO.CD_ITEM_MDLD = 202
--AND QUALIDADE.cd_prd IS NULL
																									  
	INSERT INTO [als_produto_criacao_db].dbo.[dados_publicacao_coberturas_tb](cd_prd,
																			 cd_mdld,
																			 cd_item_mdld,
																			 tp_cobertura_id,
																			 class_tp_cobertura,
																			 perc_lim_min_cob,
																			 lim_min_cobertura,
																			 perc_lim_max_cob,
																			 lim_max_cobertura,
																			 acumula_resseguro,
																			 acumula_is,
																			 acumula_desc_cob,
																			 lim_indenizacao,
																			 perc_lim_indenizacao,
																			 grupo_ramo,
																			 cod_ramo_contab,
																			 dt_inclusao,
																			 usuario)
																	  SELECT PRODUCAO.cd_prd,
   1 AS cd_mdld,
   102 AS cd_item_mdld,
																			 PRODUCAO.tp_cobertura_id,
																			 PRODUCAO.class_tp_cobertura,
																			 PRODUCAO.perc_lim_min_cob,
																			 PRODUCAO.lim_min_cobertura,
																			 PRODUCAO.perc_lim_max_cob,
																			 PRODUCAO.lim_max_cobertura,
																			 PRODUCAO.acumula_resseguro,
																			 PRODUCAO.acumula_is,
																			 PRODUCAO.acumula_desc_cob,
																			 PRODUCAO.lim_indenizacao,
																			 PRODUCAO.perc_lim_indenizacao,
																			 PRODUCAO.grupo_ramo,
																			 PRODUCAO.cod_ramo_contab,
																			 @data_dia,
																			 '14271121851'
																		FROM [LS_SUS].[als_produto_criacao_db].dbo.[dados_publicacao_coberturas_tb] PRODUCAO WITH(NOLOCK)
																		LEFT JOIN [als_produto_criacao_db].dbo.[dados_publicacao_coberturas_tb] QUALIDADE WITH (NOLOCK)
																		  ON PRODUCAO.cd_prd = QUALIDADE.cd_prd
																		 AND PRODUCAO.cd_mdld = QUALIDADE.cd_mdld
																		 AND PRODUCAO.cd_item_mdld = QUALIDADE.cd_item_mdld
																		 AND PRODUCAO.tp_cobertura_id = QUALIDADE.tp_cobertura_id  
																	   WHERE PRODUCAO.CD_PRD = '1152'
																		 AND PRODUCAO.CD_MDLD = 2
																		 AND PRODUCAO.CD_ITEM_MDLD = 202
--AND QUALIDADE.cd_prd IS NULL
	INSERT INTO [als_produto_criacao_db].dbo.[dados_publicacao_tipo_plano_tb](tipo_plano_id,
																			  cd_prd,
																			  cd_mdld,
																			  cd_item_mdld,
																			  nome_tp_plano,
																			  cod_regra_calculo_premio,
																			  dt_inclusao,
																			  usuario)
																	   SELECT PRODUCAO.tipo_plano_id,
																			  PRODUCAO.cd_prd,
   1 AS cd_mdld,
   102 AS cd_item_mdld,
																			  PRODUCAO.nome_tp_plano,
																			  PRODUCAO.cod_regra_calculo_premio,
																			  @data_dia,
																			  '14271121851'
																		 FROM [LS_SUS].[als_produto_criacao_db].dbo.[dados_publicacao_tipo_plano_tb] PRODUCAO WITH(NOLOCK)
																		 LEFT JOIN [als_produto_criacao_db].dbo.[dados_publicacao_tipo_plano_tb] QUALIDADE WITH (NOLOCK)
																		   ON PRODUCAO.cd_prd = QUALIDADE.cd_prd
																		  AND PRODUCAO.cd_mdld = QUALIDADE.cd_mdld
																		  AND PRODUCAO.cd_item_mdld = QUALIDADE.cd_item_mdld
																		  AND PRODUCAO.nome_tp_plano = QUALIDADE.nome_tp_plano  
																	    WHERE PRODUCAO.CD_PRD = '1152'
																		  AND PRODUCAO.CD_MDLD = 2
																		  AND PRODUCAO.CD_ITEM_MDLD = 202
--AND QUALIDADE.cd_prd IS NULL
	INSERT INTO [als_produto_criacao_db].dbo.[dados_publicacao_tipo_plano_componente_tb](tp_plano_id,
																						tp_componente_id,
																						dt_inclusao,
																						usuario)
																				 SELECT tipo_plano.tipo_plano_id,
																						PRODUCAO.tp_componente_id,
																						@data_dia,
																						'14271121851'
																				   FROM [LS_SUS].[als_produto_criacao_db].dbo.[dados_publicacao_tipo_plano_componente_tb] PRODUCAO WITH(NOLOCK)
																				  INNER JOIN [LS_SUS].[als_produto_criacao_db].dbo.[dados_publicacao_tipo_plano_tb] tipo WITH(NOLOCK)
																					 ON PRODUCAO.tp_plano_id = tipo.tipo_plano_id
																				  INNER JOIN [als_produto_criacao_db].dbo.[dados_publicacao_tipo_plano_tb] tipo_plano WITH (NOLOCK)
																				     ON tipo.cd_prd = tipo_plano.cd_prd
																				    AND tipo.cd_mdld = tipo_plano.cd_mdld
																				    AND tipo.cd_item_mdld = tipo_plano.cd_item_mdld
																				    AND tipo.nome_tp_plano = tipo_plano.nome_tp_plano 
																				   LEFT JOIN [als_produto_criacao_db].dbo.[dados_publicacao_tipo_plano_componente_tb] QUALIDADE WITH (NOLOCK)
																					 ON PRODUCAO.tp_componente_id = QUALIDADE.tp_componente_id 																				    
																					AND tipo_plano.tipo_plano_id = QUALIDADE.tp_plano_id 
																				  WHERE tipo.CD_PRD = '1152'
																					AND tipo.CD_MDLD = 2
																					AND tipo.CD_ITEM_MDLD = 202
--AND QUALIDADE.cd_prd IS NULL
	INSERT INTO [als_produto_criacao_db].dbo.[dados_publicacao_plano_tb](tp_plano_id,
																		 nome_plano,
																		 val_premio_plano,
																		 imp_segurada_plano,
																		 idade_min_plano,
																		 idade_max_plano,
																		 taxa_net,
																		 fator_renovacao,
																		 qtd_salarios,
																		 lim_min_sm,
																		 lim_max_sm,
																		 coef_custo,
																		 dt_inclusao,
																		 usuario)
																  SELECT tipo_plano.tipo_plano_id,
																		 PRODUCAO.nome_plano,
																		 PRODUCAO.val_premio_plano,
																		 PRODUCAO.imp_segurada_plano,
																		 PRODUCAO.idade_min_plano,
																		 PRODUCAO.idade_max_plano,
																		 PRODUCAO.taxa_net,
																		 PRODUCAO.fator_renovacao,
																		 PRODUCAO.qtd_salarios,
																		 PRODUCAO.lim_min_sm,
																		 PRODUCAO.lim_max_sm,
																		 PRODUCAO.coef_custo,
																		 @data_dia,
																		 '14271121851'
																    FROM [LS_SUS].[als_produto_criacao_db].dbo.[dados_publicacao_plano_tb] PRODUCAO WITH(NOLOCK)
																   INNER JOIN [LS_SUS].[als_produto_criacao_db].dbo.[dados_publicacao_tipo_plano_tb] tipo WITH(NOLOCK)
																	  ON PRODUCAO.tp_plano_id = tipo.tipo_plano_id
																   INNER JOIN [als_produto_criacao_db].dbo.[dados_publicacao_tipo_plano_tb] tipo_plano WITH (NOLOCK)
																      ON tipo.cd_prd = tipo_plano.cd_prd
																     AND tipo.cd_mdld = tipo_plano.cd_mdld
																     AND tipo.cd_item_mdld = tipo_plano.cd_item_mdld
																     AND tipo.nome_tp_plano = tipo_plano.nome_tp_plano 
																    LEFT JOIN [als_produto_criacao_db].dbo.[dados_publicacao_plano_tb] QUALIDADE WITH (NOLOCK)
																	  ON tipo_plano.tipo_plano_id = QUALIDADE.tp_plano_id
																	 AND PRODUCAO.nome_plano = QUALIDADE.nome_plano  
																   WHERE tipo.CD_PRD = '1152'
																	 AND tipo.CD_MDLD = 2
																	 AND tipo.CD_ITEM_MDLD = 202
--AND QUALIDADE.cd_prd IS NULL
	INSERT INTO [als_produto_criacao_db].dbo.[dados_publicacao_tipo_cobertura_tb](tp_cobertura_id,
																				 tp_componente_id,
																				 tp_plano_id,
																				 carencia_cobertura,
																				 limite_minimo_idade,
																				 limite_maximo_idade,
																				 dt_inclusao,
																				 usuario)
																		  SELECT PRODUCAO.tp_cobertura_id,
																				 PRODUCAO.tp_componente_id,
																				 tipo_plano.tipo_plano_id,
																				 PRODUCAO.carencia_cobertura,
																				 PRODUCAO.limite_minimo_idade,
																				 PRODUCAO.limite_maximo_idade,
																				 @data_dia,
																				 '14271121851'
																			FROM [LS_SUS].[als_produto_criacao_db].dbo.[dados_publicacao_tipo_cobertura_tb] PRODUCAO WITH(NOLOCK)
																		   INNER JOIN [LS_SUS].[als_produto_criacao_db].dbo.[dados_publicacao_tipo_plano_tb] tipo WITH(NOLOCK)
																			  ON PRODUCAO.tp_plano_id = tipo.tipo_plano_id
																		   INNER JOIN [als_produto_criacao_db].dbo.[dados_publicacao_tipo_plano_tb] tipo_plano WITH (NOLOCK)
																			  ON tipo.cd_prd = tipo_plano.cd_prd
																			 AND tipo.cd_mdld = tipo_plano.cd_mdld
																			 AND tipo.cd_item_mdld = tipo_plano.cd_item_mdld
																			 AND tipo.nome_tp_plano = tipo_plano.nome_tp_plano 
																		    LEFT JOIN [als_produto_criacao_db].dbo.[dados_publicacao_tipo_cobertura_tb] QUALIDADE WITH (NOLOCK)
																			  ON PRODUCAO.tp_cobertura_id = QUALIDADE.tp_cobertura_id
																			 AND PRODUCAO.tp_componente_id = QUALIDADE.tp_componente_id 
																		   WHERE tipo.CD_PRD = '1152'
																			 AND tipo.CD_MDLD = 2
																			 AND tipo.CD_ITEM_MDLD = 202
--AND QUALIDADE.cd_prd IS NULL
	INSERT INTO [als_produto_criacao_db].dbo.[dados_publicacao_periodo_pagto_tb](cd_prd,
																				 cd_mdld,
																				 cd_item_mdld,
																				 periodo_pgto_id,
																				 dt_inclusao,
																				 usuario)
																		  SELECT PRODUCAO.cd_prd,
   1 AS cd_mdld,
   102 AS cd_item_mdld,
																				 PRODUCAO.periodo_pgto_id,
																				 @data_dia,
																				 '14271121851'
																		    FROM [LS_SUS].[als_produto_criacao_db].dbo.[dados_publicacao_periodo_pagto_tb] PRODUCAO WITH(NOLOCK)
																		    LEFT JOIN [als_produto_criacao_db].dbo.[dados_publicacao_periodo_pagto_tb] QUALIDADE WITH (NOLOCK)
																		      ON PRODUCAO.cd_prd = QUALIDADE.cd_prd
																		     AND PRODUCAO.cd_mdld = QUALIDADE.cd_mdld
																		     AND PRODUCAO.cd_item_mdld = QUALIDADE.cd_item_mdld
																		     AND PRODUCAO.periodo_pgto_id = QUALIDADE.periodo_pgto_id  
																		   WHERE PRODUCAO.CD_PRD = '1152'
																			 AND PRODUCAO.CD_MDLD = 2
																		     AND PRODUCAO.CD_ITEM_MDLD = 202
--AND QUALIDADE.cd_prd IS NULL
	INSERT INTO [als_produto_criacao_db].dbo.[dados_publicacao_produto_tp_carta_tb](cd_prd,
																					cd_mdld,
																					cd_item_mdld,
																					tp_carta_id,
																					prazo_emissao,
																					dt_inclusao,
																					usuario)
																			 SELECT PRODUCAO.cd_prd,
   1 AS cd_mdld,
   102 AS cd_item_mdld,
																					PRODUCAO.tp_carta_id,
																					PRODUCAO.prazo_emissao,
																					@data_dia,
																					'14271121851'
																			   FROM [LS_SUS].[als_produto_criacao_db].dbo.[dados_publicacao_produto_tp_carta_tb] PRODUCAO WITH(NOLOCK)
																			   LEFT JOIN [als_produto_criacao_db].dbo.[dados_publicacao_produto_tp_carta_tb] QUALIDADE WITH (NOLOCK)
																				 ON PRODUCAO.cd_prd = QUALIDADE.cd_prd
																				AND PRODUCAO.cd_mdld = QUALIDADE.cd_mdld
																				AND PRODUCAO.cd_item_mdld = QUALIDADE.cd_item_mdld
																				AND PRODUCAO.tp_carta_id = QUALIDADE.tp_carta_id  
																			  WHERE PRODUCAO.CD_PRD = '1152'
																				AND PRODUCAO.CD_MDLD = 2
																				AND PRODUCAO.CD_ITEM_MDLD = 202
--AND QUALIDADE.cd_prd IS NULL
																				
--****COPIA DOS DADOS PROCEDURE PRDS02147_SPI***
--****DELETANDO OS DADOS***
         
    DELETE FROM        
     [VBR008002-016\QUALID].[ALS_PRODUTO_CRIACAO_DB].DBO.[PRD_MDLD_ITEM_TB]        
    WHERE (cd_prd = 1152 AND cd_mdld = 2 AND cd_item_mdld = 202 AND ISNULL(DT_FIM_VGC,GETDATE()+1) > GETDATE() AND 2 = 1 )        
    DELETE FROM        
     [VBR008002-016\QUALID].[ALS_PRODUTO_CRIACAO_DB].DBO.[FRQU_CBT_ITEM_MDLD_HIST_TB]        
    WHERE (cd_prd = 1152 AND cd_mdld = 2 AND cd_item_mdld = 202 AND nr_versao = 19 AND 2 = 1 )        
    DELETE FROM        
     [VBR008002-016\QUALID].[ALS_PRODUTO_CRIACAO_DB].DBO.[FRQU_CBT_ITEM_MDLD]        
    WHERE (cd_prd = 1152 AND cd_mdld = 2 AND cd_item_mdld = 202 AND ISNULL(DT_FIM_VGC_FRQU,GETDATE()+1) > GETDATE() AND 2 = 1 )        
    DELETE FROM        
     [VBR008002-016\QUALID].[ALS_PRODUTO_CRIACAO_DB].DBO.[CJT_CBT_CNL_VND_HIST_TB]        
    WHERE (cd_prd = 1152 AND cd_mdld = 2 AND cd_item_mdld = 202 AND nr_versao = 19 AND 2 = 1 )        
    DELETE FROM        
     [VBR008002-016\QUALID].[ALS_PRODUTO_CRIACAO_DB].DBO.[CJT_CBT_CNL_VND]        
    WHERE (cd_prd = 1152 AND cd_mdld = 2 AND cd_item_mdld = 202 AND 2 = 1 )        
    DELETE FROM        
     [VBR008002-016\QUALID].[ALS_PRODUTO_CRIACAO_DB].DBO.[RSCO_CBT_CLS_RSCO_HIST_TB]        
    WHERE (cd_prd = 1152 AND cd_mdld = 2 AND cd_item_mdld = 202 AND nr_versao = 19 AND 2 = 1 )        
    DELETE FROM        
     [VBR008002-016\QUALID].[ALS_PRODUTO_CRIACAO_DB].DBO.[RSCO_CBT_CLS_RSCO]        
    WHERE (cd_prd = 1152 AND cd_mdld = 2 AND cd_item_mdld = 202 AND 2 = 1 )        
    DELETE FROM        
     [VBR008002-016\QUALID].[ALS_PRODUTO_CRIACAO_DB].DBO.[CJT_CBT_ITEM_MDLD_HIST_TB]        
    WHERE (cd_prd = 1152 AND cd_mdld = 2 AND cd_item_mdld = 202 AND nr_versao = 19 AND 2 = 1 )        
    DELETE FROM        
     [VBR008002-016\QUALID].[ALS_PRODUTO_CRIACAO_DB].DBO.[CJT_CBT_ITEM_MDLD]        
    WHERE (cd_prd = 1152 AND cd_mdld = 2 AND cd_item_mdld = 202 AND ISNULL(DT_FIM_CBT,GETDATE()+1) > GETDATE() AND 2 = 1 )        
    DELETE FROM        
     [VBR008002-016\QUALID].[ALS_PRODUTO_CRIACAO_DB].DBO.[CLSL_ITEM_MDLD_HIST_TB]        
    WHERE (cd_prd = 1152 AND cd_mdld = 2 AND cd_item_mdld = 202 AND nr_versao = 19 AND 2 = 1 )        
    DELETE FROM        
     [VBR008002-016\QUALID].[ALS_PRODUTO_CRIACAO_DB].DBO.[CLSL_ITEM_MDLD]        
    WHERE (cd_prd = 1152 AND cd_mdld = 2 AND cd_item_mdld = 202 AND ISNULL(DT_FIM_CLSL,GETDATE()+1) > GETDATE() AND 2 = 1 )        
    DELETE FROM        
     [VBR008002-016\QUALID].[ALS_PRODUTO_CRIACAO_DB].DBO.[SGRO_CNL_VND_HIST_TB]        
    WHERE (cd_prd = 1152 AND cd_mdld = 2 AND cd_item_mdld = 202 AND nr_versao = 19 AND 2 = 1 )        
    DELETE FROM        
     [VBR008002-016\QUALID].[ALS_PRODUTO_CRIACAO_DB].DBO.[SGRO_CNL_VND]        
    WHERE (cd_prd = 1152 AND cd_mdld = 2 AND cd_item_mdld = 202 AND 2 = 1 )        
    DELETE FROM        
     [VBR008002-016\QUALID].[ALS_PRODUTO_CRIACAO_DB].DBO.[CLSL_ITEM_MDLD_CBT_HIST_TB]        
    WHERE (cd_prd = 1152 AND cd_mdld = 2 AND cd_item_mdld = 202 AND nr_versao = 19 AND 2 = 1 )        
    DELETE FROM        
     [VBR008002-016\QUALID].[ALS_PRODUTO_CRIACAO_DB].DBO.[CLSL_ITEM_MDLD_CBT]        
    WHERE (cd_prd = 1152 AND cd_mdld = 2 AND cd_item_mdld = 202 AND ISNULL(DT_FIM_CLSL,GETDATE()+1) > GETDATE() AND 2 = 1 )        
    DELETE FROM        
     [VBR008002-016\QUALID].[ALS_PRODUTO_CRIACAO_DB].DBO.[EDS_ITEM_MDLD_HIST_TB]        
    WHERE (cd_prd = 1152 AND cd_mdld = 2 AND cd_item_mdld = 202 AND nr_versao = 19 AND 2 = 1 )        
    DELETE FROM        
     [VBR008002-016\QUALID].[ALS_PRODUTO_CRIACAO_DB].DBO.[EDS_ITEM_MDLD]        
    WHERE (cd_prd = 1152 AND cd_mdld = 2 AND cd_item_mdld = 202 AND ISNULL(DT_FIM_EDS,GETDATE()+1) > GETDATE() AND 2 = 1 )        
    DELETE FROM        
     [VBR008002-016\QUALID].[ALS_PRODUTO_CRIACAO_DB].DBO.[ETLE_ITEM_MDLD_HIST_TB]        
    WHERE (cd_prd = 1152 AND cd_mdld = 2 AND cd_item_mdld = 202 AND nr_versao = 19 AND 2 = 1 )        
    DELETE FROM        
     [VBR008002-016\QUALID].[ALS_PRODUTO_CRIACAO_DB].DBO.[ETLE_ITEM_MDLD]        
    WHERE (cd_prd = 1152 AND cd_mdld = 2 AND cd_item_mdld = 202 AND ISNULL(DT_FIM_ETLE,GETDATE()+1) > GETDATE() AND 2 = 1 )        
    DELETE FROM        
     [VBR008002-016\QUALID].[ALS_PRODUTO_CRIACAO_DB].DBO.[FMA_PGTO_ITEM_MDLD_HIST_TB]        
    WHERE (cd_prd = 1152 AND cd_mdld = 2 AND cd_item_mdld = 202 AND nr_versao = 19 AND 2 = 1 )        
    DELETE FROM        
     [VBR008002-016\QUALID].[ALS_PRODUTO_CRIACAO_DB].DBO.[FMA_PGTO_ITEM_MDLD]        
    WHERE (cd_prd = 1152 AND cd_mdld = 2 AND cd_item_mdld = 202 AND ISNULL(DT_FIM_FMA_PGTO,GETDATE()+1) > GETDATE() AND 2 = 1 )        
    DELETE FROM        
     [VBR008002-016\QUALID].[ALS_PRODUTO_CRIACAO_DB].DBO.[QSTN_ITEM_MDLD_HIST_TB]        
    WHERE (cd_prd = 1152 AND cd_mdld = 2 AND cd_item_mdld = 202 AND nr_versao = 19 AND 2 = 1 )        
    DELETE FROM        
     [VBR008002-016\QUALID].[ALS_PRODUTO_CRIACAO_DB].DBO.[QSTN_ITEM_MDLD]        
    WHERE (cd_prd = 1152 AND cd_mdld = 2 AND cd_item_mdld = 202 AND ISNULL(DT_FIM_QSTN,GETDATE()+1) > GETDATE() AND 2 = 1 )        
    DELETE FROM        
     [VBR008002-016\QUALID].[ALS_PRODUTO_CRIACAO_DB].DBO.[SUBRAMO_HIST_TB]        
    WHERE (cd_prd = 1152 AND cd_mdld = 2 AND cd_item_mdld = 202 AND nr_versao = 19 AND 2 = 1 )        
    DELETE FROM        
     [VBR008002-016\QUALID].[ALS_PRODUTO_CRIACAO_DB].DBO.[SUBRAMO_TB]        
    WHERE (cd_prd = 1152 AND cd_mdld = 2 AND cd_item_mdld = 202 AND 2 = 1 )        
    DELETE FROM        
     [VBR008002-016\QUALID].[ALS_PRODUTO_CRIACAO_DB].DBO.[TX_LNH_CBT_HIST_TB]        
    WHERE (cd_cbt in (339) AND nr_versao = 19 AND 2 = 1 )        
    DELETE FROM        
     [VBR008002-016\QUALID].[ALS_PRODUTO_CRIACAO_DB].DBO.[TX_LNH_CBT]        
    WHERE (cd_cbt in (339) AND ISNULL(DT_FIM_LNH_CBT,GETDATE()+1) > GETDATE() AND 2 = 1 )        
    DELETE FROM        
     [VBR008002-016\QUALID].[ALS_PRODUTO_CRIACAO_DB].DBO.[TX_RDZ_CBT_HIST_TB]        
    WHERE (cd_cbt in (339) AND nr_versao = 19 AND 2 = 1 )        
    DELETE FROM        
     [VBR008002-016\QUALID].[ALS_PRODUTO_CRIACAO_DB].DBO.[TX_RDZ_CBT]        
    WHERE (cd_cbt in (339) AND ISNULL(DT_FIM_VGC_CBT,GETDATE()+1) > GETDATE() AND 2 = 1 )        
    DELETE FROM        
     [VBR008002-016\QUALID].[ALS_PRODUTO_CRIACAO_DB].DBO.[TX_LNH_CLSL_HIST_TB]        
    WHERE (cd_clsl in (339,100652,100653,100654,100655,100656,100657,100658,100659,100660,100661,100869,100870,100871,100877,100942,100943,101017,101101,101102,101103,101104,101105,101106,101301,101302,101411,101523) AND nr_versao = 19 AND 2 = 1 )        
    DELETE FROM        
     [VBR008002-016\QUALID].[ALS_PRODUTO_CRIACAO_DB].DBO.[TX_LNH_CLSL]        
    WHERE (cd_clsl in (339,100652,100653,100654,100655,100656,100657,100658,100659,100660,100661,100869,100870,100871,100877,100942,100943,101017,101101,101102,101103,101104,101105,101106,101301,101302,101411,101523) AND ISNULL(DT_FIM_LNH_CLSL,GETDATE()+1) > GETDATE() AND 2 = 1 )        
    DELETE FROM        
     [VBR008002-016\QUALID].[ALS_PRODUTO_CRIACAO_DB].DBO.[RSA_SGRO_HIST_TB]        
    WHERE (cd_prd = 1152 AND cd_mdld = 2 AND cd_item_mdld = 202 AND nr_versao = 19 AND 2 = 1 )        
    DELETE FROM        
     [VBR008002-016\QUALID].[ALS_PRODUTO_CRIACAO_DB].DBO.[RSA_SGRO]        
    WHERE (cd_prd = 1152 AND cd_mdld = 2 AND cd_item_mdld = 202 AND ISNULL(DT_FIM_RSA_SGRO,GETDATE()+1) > GETDATE() AND 2 = 1 )        
    DELETE FROM        
     [VBR008002-016\QUALID].[ALS_PRODUTO_CRIACAO_DB].DBO.[PCLT_SGRO_HIST_TB]        
    WHERE (cd_prd = 1152 AND cd_mdld = 2 AND cd_item_mdld = 202 AND nr_versao = 19 AND 2 = 1 )        
    DELETE FROM        
     [VBR008002-016\QUALID].[ALS_PRODUTO_CRIACAO_DB].DBO.[PCLT_SGRO]        
    WHERE (cd_prd = 1152 AND cd_mdld = 2 AND cd_item_mdld = 202 AND ISNULL(DT_FIM_VGC_PCLT,GETDATE()+1) > GETDATE() AND 2 = 1 )        
    DELETE FROM        
     [VBR008002-016\QUALID].[ALS_PRODUTO_CRIACAO_DB].DBO.[PLN_AST_ITEM_MDLD_HIST_TB]        
    WHERE (cd_prd = 1152 AND cd_mdld = 2 AND cd_item_mdld = 202 AND nr_versao = 19 AND 2 = 1 )        
    DELETE FROM        
     [VBR008002-016\QUALID].[ALS_PRODUTO_CRIACAO_DB].DBO.[PLN_AST_ITEM_MDLD]        
    WHERE (cd_prd = 1152 AND cd_mdld = 2 AND cd_item_mdld = 202 AND ISNULL(DT_FIM_PLN_AST,GETDATE()+1) > GETDATE() AND 2 = 1 )        
    DELETE FROM        
     [VBR008002-016\QUALID].[ALS_PRODUTO_CRIACAO_DB].DBO.[CJT_CBT_FXA_ETAR_HIST_TB]        
    WHERE (cd_prd = 1152 AND cd_mdld = 2 AND cd_item_mdld = 202 AND nr_versao = 19 AND 2 = 1 )        
    DELETE FROM        
     [VBR008002-016\QUALID].[ALS_PRODUTO_CRIACAO_DB].DBO.[CJT_CBT_FXA_ETAR]        
    WHERE (cd_prd = 1152 AND cd_mdld = 2 AND cd_item_mdld = 202 AND ISNULL(DT_FIM_VGC_FXA,GETDATE()+1) > GETDATE() AND 2 = 1 )        
    DELETE FROM        
     [VBR008002-016\QUALID].[ALS_PRODUTO_CRIACAO_DB].DBO.[FXA_ETAR_ITEM_MDLD_HIST_TB]        
    WHERE (cd_prd = 1152 AND cd_mdld = 2 AND cd_item_mdld = 202 AND nr_versao = 19 AND 2 = 1 )        
    DELETE FROM        
     [VBR008002-016\QUALID].[ALS_PRODUTO_CRIACAO_DB].DBO.[FXA_ETAR_ITEM_MDLD]        
    WHERE (cd_prd = 1152 AND cd_mdld = 2 AND cd_item_mdld = 202 AND ISNULL(DT_FIM_VGC_FXA,GETDATE()+1) > GETDATE() AND 2 = 1 )        
    DELETE FROM        
     [VBR008002-016\QUALID].[ALS_PRODUTO_CRIACAO_DB].DBO.[RMNC_ITEM_MDLD_HIST_TB]        
    WHERE (cd_prd = 1152 AND cd_mdld = 2 AND cd_item_mdld = 202 AND ISNULL(DT_FIM_VGC_RMNC,GETDATE()+1) > GETDATE() AND 2 = 1 )        
    DELETE FROM        
     [VBR008002-016\QUALID].[ALS_PRODUTO_CRIACAO_DB].DBO.[RMNC_ITEM_MDLD]        
    WHERE (cd_prd = 1152 AND cd_mdld = 2 AND cd_item_mdld = 202 AND ISNULL(DT_FIM_VGC_RMNC,GETDATE()+1) > GETDATE() AND 2 = 1 )        
    DELETE FROM        
     [VBR008002-016\QUALID].[ALS_PRODUTO_CRIACAO_DB].DBO.[TIP_DSC_ITEM_MDLD_HIST_TB]        
    WHERE (cd_prd = 1152 AND cd_mdld = 2 AND cd_item_mdld = 202 AND nr_versao = 19 AND 2 = 1 )        
    DELETE FROM        
     [VBR008002-016\QUALID].[ALS_PRODUTO_CRIACAO_DB].DBO.[TIP_DSC_ITEM_MDLD]        
    WHERE (cd_prd = 1152 AND cd_mdld = 2 AND cd_item_mdld = 202 AND ISNULL(DT_FIM_VGC_DSC,GETDATE()+1) > GETDATE() AND 2 = 1 )        
    DELETE FROM        
     [VBR008002-016\QUALID].[ALS_PRODUTO_CRIACAO_DB].DBO.[RSCO_ESPC_BEM_HIST_TB]        
    WHERE (cd_prd = 1152 AND cd_mdld = 2 AND cd_item_mdld = 202 AND nr_versao = 19 AND 2 = 1 )        
    DELETE FROM        
     [VBR008002-016\QUALID].[ALS_PRODUTO_CRIACAO_DB].DBO.[RSCO_ESPC_BEM]        
    WHERE (cd_prd = 1152 AND cd_mdld = 2 AND cd_item_mdld = 202 AND ISNULL(DT_FIM_VGC_BEM,GETDATE()+1) > GETDATE() AND 2 = 1 )        
    DELETE FROM        
     [VBR008002-016\QUALID].[ALS_PRODUTO_CRIACAO_DB].DBO.[ITEM_MDLD_PRD_HIST_TB]        
    WHERE (cd_prd = 1152 AND cd_mdld = 2 AND cd_item_mdld = 202 AND nr_versao = 19 AND 2 = 1 )        
    DELETE FROM        
     [VBR008002-016\QUALID].[ALS_PRODUTO_CRIACAO_DB].DBO.[ITEM_MDLD_PRD]        
    WHERE (cd_prd = 1152 AND cd_mdld = 2 AND cd_item_mdld = 202 AND ISNULL(DT_FIM_VGC_APLC,GETDATE()+1) > GETDATE() AND 2 = 1 )
--****INSERINDO OS DADOS***
        
    INSERT INTO [VBR008002-016\QUALID].[ALS_PRODUTO_CRIACAO_DB].DBO.[ITEM_MDLD_PRD]        
       ([CD_PRD], [CD_MDLD], [CD_ITEM_MDLD], [CD_CTGR_ITEM_MDLD], [CD_EXG_ETLE], [NM_OGNL_ITEM_MDLD], [NM_FANT_ITEM_MDLD], [TX_PRC_SUSP], [IN_CMZC_MOEE], [IN_CTC_SGRO], [IN_AVB], [PC_CRE_PDRO], [PC_CRE_MIN], [PC_CRE_MAX], [VL_PREM_MIN_PRPT], [VL_PREM_MIN_EDS], [PC_IOF], [PC_JUR_PCLT], [PC_CST_APLC], [VL_MAX_CST_APLC], [QT_MIN_DD_PZ_SGRO], [QT_MAX_DD_PZ_SGRO], [PC_CST_APLC_EDS], [NR_MAX_PCL_PMT], [VL_MAX_CST_APLC_EDS], [VL_MIN_PCL_PRPT], [QT_DD_VLD_CTC], [VL_MIN_PCL_EDS], [NR_MAX_PCL_PMT_EDS], [CD_USU_RSP_VGC_PRTD], [VL_MAX_GRT], [VL_MIN_GRT], [PZ_RPSS], [TIP_RPSS], [PERC_RPSS], [PZ_FLOT_RPSS], [PC_IR_CRE], [PC_IR_ETLE], [DD_RPSS_MSL], [CTRA_UTLZD], [CVN_CBR_PRMO], [CVN_CBR_OTR], [CVN_CBR_AVB], [CVN_DBT], [IN_SGR], [VL_MIN_FATURAMENTO], [CLSL_BNFC], [TP_SEL_CBT], [LMI_UNCO], [QT_PDRO_DD_PZ_SGRO], [IN_RSCO_PTRR], [IN_FXA_ETAR], [CD_TIP_AGDT], [IN_RMNC_SGRO], [NM_SP_CALC_CTC], [IOF_COBERTURA], [PC_CST_CERT], [LMT_IDD_MIN], [LMT_IDD_MAX], [NR_APLC], [QT_DD_PRZ_APLC], [DT_INC_VGC_APLC], [DT_FIM_VGC_APLC], [ENV_EVT_3030], [COPIA_REALIZADA_QLD])        
            
    SELECT '1152', '1', '102', [CD_CTGR_ITEM_MDLD], [CD_EXG_ETLE], [NM_OGNL_ITEM_MDLD], [NM_FANT_ITEM_MDLD], [TX_PRC_SUSP], [IN_CMZC_MOEE], [IN_CTC_SGRO], [IN_AVB], [PC_CRE_PDRO], [PC_CRE_MIN], [PC_CRE_MAX], [VL_PREM_MIN_PRPT], [VL_PREM_MIN_EDS], [PC_IOF], [PC_JUR_PCLT], [PC_CST_APLC], [VL_MAX_CST_APLC], [QT_MIN_DD_PZ_SGRO], [QT_MAX_DD_PZ_SGRO], [PC_CST_APLC_EDS], [NR_MAX_PCL_PMT], [VL_MAX_CST_APLC_EDS], [VL_MIN_PCL_PRPT], [QT_DD_VLD_CTC], [VL_MIN_PCL_EDS], [NR_MAX_PCL_PMT_EDS], [CD_USU_RSP_VGC_PRTD], [VL_MAX_GRT], [VL_MIN_GRT], [PZ_RPSS], [TIP_RPSS], [PERC_RPSS], [PZ_FLOT_RPSS], [PC_IR_CRE], [PC_IR_ETLE], [DD_RPSS_MSL], [CTRA_UTLZD], [CVN_CBR_PRMO], [CVN_CBR_OTR], [CVN_CBR_AVB], [CVN_DBT], [IN_SGR], [VL_MIN_FATURAMENTO], [CLSL_BNFC], [TP_SEL_CBT], [LMI_UNCO], [QT_PDRO_DD_PZ_SGRO], [IN_RSCO_PTRR], [IN_FXA_ETAR], [CD_TIP_AGDT], [IN_RMNC_SGRO], [NM_SP_CALC_CTC], [IOF_COBERTURA], [PC_CST_CERT], [LMT_IDD_MIN], [LMT_IDD_MAX], [NR_APLC], [QT_DD_PRZ_APLC], [DT_INC_VGC_APLC], [DT_FIM_VGC_APLC], [ENV_EVT_3030], [COPIA_REALIZADA_QLD]        
     FROM [LS_SUS].[ALS_PRODUTO_CRIACAO_DB].DBO.[ITEM_MDLD_PRD]  WITH(NOLOCK)        
    WHERE (cd_prd = 1152 AND cd_mdld = 2 AND cd_item_mdld = 202 AND ISNULL(DT_FIM_VGC_APLC,GETDATE()+1) > GETDATE())         
            
    INSERT INTO [VBR008002-016\QUALID].[ALS_PRODUTO_CRIACAO_DB].DBO.[ITEM_MDLD_PRD_HIST_TB]        
       ([CD_PRD], [CD_MDLD], [CD_ITEM_MDLD], [NR_VERSAO], [DT_INICIO_VIGENCIA], [DT_FIM_VIGENCIA], [CD_CTGR_ITEM_MDLD], [CD_EXG_ETLE], [NM_OGNL_ITEM_MDLD], [NM_FANT_ITEM_MDLD], [TX_PRC_SUSP], [IN_CMZC_MOEE], [IN_CTC_SGRO], [IN_AVB], [PC_CRE_PDRO], [PC_CRE_MIN], [PC_CRE_MAX], [VL_PREM_MIN_PRPT], [VL_PREM_MIN_EDS], [PC_IOF], [PC_JUR_PCLT], [PC_CST_APLC], [VL_MAX_CST_APLC], [QT_MIN_DD_PZ_SGRO], [QT_MAX_DD_PZ_SGRO], [PC_CST_APLC_EDS], [NR_MAX_PCL_PMT], [VL_MAX_CST_APLC_EDS], [VL_MIN_PCL_PRPT], [QT_DD_VLD_CTC], [VL_MIN_PCL_EDS], [NR_MAX_PCL_PMT_EDS], [CD_USU_RSP_VGC_PRTD], [TX_INFM_MTV_RSA], [CD_EST_ITEM_MDLD], [CD_PRD_BB], [CD_MDLD_BB], [ENVIADO_BB], [DT_ENVIO], [VL_MAX_GRT], [VL_MIN_GRT], [PZ_RPSS], [TIP_RPSS], [PERC_RPSS], [PZ_FLOT_RPSS], [PC_IR_CRE], [PC_IR_ETLE], [DD_RPSS_MSL], [CTRA_UTLZD], [CVN_CBR_PRMO], [CVN_CBR_OTR], [CVN_CBR_AVB], [CVN_DBT], [DT_INICIO_COMERCIALIZACAO], [DT_FIM_COMERCIALIZACAO], [DT_HOMOLOGACAO], [IN_SGR], [USUARIO], [DT_INCLUSAO], [DT_ALTERACAO], [IN_CJT_CBT_PLANO], [VL_MIN_FATURAMENTO], [DT_ENVIO_ENCERRAMENTO], [CLSL_BNFC], [TP_SEL_CBT], [LMI_UNCO], [dt_publicacao], [ic_permite_vistoria], [QT_PDRO_DD_PZ_SGRO], [IN_RSCO_PTRR], [IN_FXA_ETAR], [CD_TIP_AGDT], [IN_RMNC_SGRO], [NM_SP_CALC_CTC], [IOF_COBERTURA], [PC_CST_CERT], [LMT_IDD_MIN], [LMT_IDD_MAX], [NR_APLC], [QT_DD_PRZ_APLC], [DT_INC_VGC_APLC], [DT_FIM_VGC_APLC], [ENV_EVT_3030], [COPIA_REALIZADA_QLD])        
            
    SELECT '1152', '1', '102', '20', [DT_INICIO_VIGENCIA], [DT_FIM_VIGENCIA], [CD_CTGR_ITEM_MDLD], [CD_EXG_ETLE], [NM_OGNL_ITEM_MDLD], [NM_FANT_ITEM_MDLD], [TX_PRC_SUSP], [IN_CMZC_MOEE], [IN_CTC_SGRO], [IN_AVB], [PC_CRE_PDRO], [PC_CRE_MIN], [PC_CRE_MAX], [VL_PREM_MIN_PRPT], [VL_PREM_MIN_EDS], [PC_IOF], [PC_JUR_PCLT], [PC_CST_APLC], [VL_MAX_CST_APLC], [QT_MIN_DD_PZ_SGRO], [QT_MAX_DD_PZ_SGRO], [PC_CST_APLC_EDS], [NR_MAX_PCL_PMT], [VL_MAX_CST_APLC_EDS], [VL_MIN_PCL_PRPT], [QT_DD_VLD_CTC], [VL_MIN_PCL_EDS], [NR_MAX_PCL_PMT_EDS], [CD_USU_RSP_VGC_PRTD], [TX_INFM_MTV_RSA], [CD_EST_ITEM_MDLD], [CD_PRD_BB], '5', [ENVIADO_BB], [DT_ENVIO], [VL_MAX_GRT], [VL_MIN_GRT], [PZ_RPSS], [TIP_RPSS], [PERC_RPSS], [PZ_FLOT_RPSS], [PC_IR_CRE], [PC_IR_ETLE], [DD_RPSS_MSL], [CTRA_UTLZD], [CVN_CBR_PRMO], [CVN_CBR_OTR], [CVN_CBR_AVB], [CVN_DBT], '2020-05-20 00:00:00.000', [DT_FIM_COMERCIALIZACAO], '2020-05-10 00:00:00.000', [IN_SGR], '14271121851', [DT_INCLUSAO], '20200520', [IN_CJT_CBT_PLANO], [VL_MIN_FATURAMENTO], [DT_ENVIO_ENCERRAMENTO], [CLSL_BNFC], [TP_SEL_CBT], [LMI_UNCO], [dt_publicacao], [ic_permite_vistoria], [QT_PDRO_DD_PZ_SGRO], [IN_RSCO_PTRR], [IN_FXA_ETAR], [CD_TIP_AGDT], [IN_RMNC_SGRO], [NM_SP_CALC_CTC], [IOF_COBERTURA], [PC_CST_CERT], [LMT_IDD_MIN], [LMT_IDD_MAX], [NR_APLC], [QT_DD_PRZ_APLC], [DT_INC_VGC_APLC], [DT_FIM_VGC_APLC], [ENV_EVT_3030], [COPIA_REALIZADA_QLD]        
     FROM [LS_SUS].[ALS_PRODUTO_CRIACAO_DB].DBO.[ITEM_MDLD_PRD_HIST_TB]  WITH(NOLOCK)        
    WHERE (cd_prd = 1152 AND cd_mdld = 2 AND cd_item_mdld = 202 AND nr_versao = 19)         
            
    INSERT INTO [VBR008002-016\QUALID].[ALS_PRODUTO_CRIACAO_DB].DBO.[RSCO_ESPC_BEM]        
       ([CD_PRD], [CD_MDLD], [CD_ITEM_MDLD], [CD_ESPC_GRT], [CD_PRTD_BEM], [DT_INC_VGC_BEM], [DT_FIM_VGC_BEM], [CD_ENQ_BEM_SGRV], [CD_LCLZ_TAXA_SGRA], [PC_TAXA_RSCO_BEM])        
            
    SELECT '1152', '1', '102', [CD_ESPC_GRT], [CD_PRTD_BEM], [DT_INC_VGC_BEM], [DT_FIM_VGC_BEM], [CD_ENQ_BEM_SGRV], [CD_LCLZ_TAXA_SGRA], [PC_TAXA_RSCO_BEM]        
     FROM [LS_SUS].[ALS_PRODUTO_CRIACAO_DB].DBO.[RSCO_ESPC_BEM]  WITH(NOLOCK)        
    WHERE (cd_prd = 1152 AND cd_mdld = 2 AND cd_item_mdld = 202 AND ISNULL(DT_FIM_VGC_BEM,GETDATE()+1) > GETDATE())         
            
    INSERT INTO [VBR008002-016\QUALID].[ALS_PRODUTO_CRIACAO_DB].DBO.[RSCO_ESPC_BEM_HIST_TB]        
       ([CD_PRD], [CD_MDLD], [CD_ITEM_MDLD], [CD_ESPC_GRT], [CD_PRTD_BEM], [DT_INC_VGC_BEM], [NR_VERSAO], [DT_INICIO_VIGENCIA], [DT_FIM_VIGENCIA], [DT_FIM_VGC_BEM], [CD_ENQ_BEM_SGRV], [CD_LCLZ_TAXA_SGRA], [PC_TAXA_RSCO_BEM], [ENVIADO_BB], [DT_ENVIO], [USUARIO], [DT_INCLUSAO], [DT_ALTERACAO])        
            
    SELECT '1152', '1', '102', [CD_ESPC_GRT], [CD_PRTD_BEM], [DT_INC_VGC_BEM], '20', [DT_INICIO_VIGENCIA], [DT_FIM_VIGENCIA], [DT_FIM_VGC_BEM], [CD_ENQ_BEM_SGRV], [CD_LCLZ_TAXA_SGRA], [PC_TAXA_RSCO_BEM], [ENVIADO_BB], [DT_ENVIO], '14271121851', [DT_INCLUSAO], '20200520'        
     FROM [LS_SUS].[ALS_PRODUTO_CRIACAO_DB].DBO.[RSCO_ESPC_BEM_HIST_TB]  WITH(NOLOCK)        
    WHERE (cd_prd = 1152 AND cd_mdld = 2 AND cd_item_mdld = 202 AND nr_versao = 19)         
            
    INSERT INTO [VBR008002-016\QUALID].[ALS_PRODUTO_CRIACAO_DB].DBO.[TIP_DSC_ITEM_MDLD]        
       ([CD_PRD], [CD_MDLD], [CD_ITEM_MDLD], [CD_TIP_DSC], [DT_INC_VGC_DSC], [DT_FIM_VGC_DSC], [QT_OCR_DSC], [PC_DSC])        
            
    SELECT '1152', '1', '102', [CD_TIP_DSC], [DT_INC_VGC_DSC], [DT_FIM_VGC_DSC], [QT_OCR_DSC], [PC_DSC]        
     FROM [LS_SUS].[ALS_PRODUTO_CRIACAO_DB].DBO.[TIP_DSC_ITEM_MDLD]  WITH(NOLOCK)        
    WHERE (cd_prd = 1152 AND cd_mdld = 2 AND cd_item_mdld = 202 AND ISNULL(DT_FIM_VGC_DSC,GETDATE()+1) > GETDATE())         
            
    INSERT INTO [VBR008002-016\QUALID].[ALS_PRODUTO_CRIACAO_DB].DBO.[TIP_DSC_ITEM_MDLD_HIST_TB]        
       ([CD_PRD], [CD_MDLD], [CD_ITEM_MDLD], [CD_TIP_DSC], [DT_INC_VGC_DSC], [NR_VERSAO], [DT_INICIO_VIGENCIA], [DT_FIM_VIGENCIA], [DT_FIM_VGC_DSC], [QT_OCR_DSC], [PC_DSC], [ENVIADO_BB], [DT_ENVIO], [USUARIO], [DT_INCLUSAO], [DT_ALTERACAO])        
            
    SELECT '1152', '1', '102', [CD_TIP_DSC], [DT_INC_VGC_DSC], '20', [DT_INICIO_VIGENCIA], [DT_FIM_VIGENCIA], [DT_FIM_VGC_DSC], [QT_OCR_DSC], [PC_DSC], [ENVIADO_BB], [DT_ENVIO], '14271121851', [DT_INCLUSAO], '20200520'        
     FROM [LS_SUS].[ALS_PRODUTO_CRIACAO_DB].DBO.[TIP_DSC_ITEM_MDLD_HIST_TB]  WITH(NOLOCK)        
    WHERE (cd_prd = 1152 AND cd_mdld = 2 AND cd_item_mdld = 202 AND nr_versao = 19)         
            
    INSERT INTO [VBR008002-016\QUALID].[ALS_PRODUTO_CRIACAO_DB].DBO.[RMNC_ITEM_MDLD]        
       ([CD_PRD], [CD_MDLD], [CD_ITEM_MDLD], [CD_RMNC_SGRO], [CD_TIP_RMNC_SGRO], [CD_PSS_RMNC_SGRO], [DT_INC_VGC_RMNC], [CD_EST_VCLC], [CD_TIP_BASE_CLC], [PC_RMNC_SGRO_MIN], [PC_RMNC_SGRO_PDRO], [PC_RMNC_SGRO_MAX], [PC_IR_RMNC_SGRO], [VL_RMNC_SGRO_MIN], [VL_RMNC_SGRO_PDRO], [VL_RMNC_SGRO_MAX], [DT_FIM_VGC_RMNC])        
            
    SELECT '1152', '1', '102', [CD_RMNC_SGRO], [CD_TIP_RMNC_SGRO], [CD_PSS_RMNC_SGRO], [DT_INC_VGC_RMNC], [CD_EST_VCLC], [CD_TIP_BASE_CLC], [PC_RMNC_SGRO_MIN], [PC_RMNC_SGRO_PDRO], [PC_RMNC_SGRO_MAX], [PC_IR_RMNC_SGRO], [VL_RMNC_SGRO_MIN], [VL_RMNC_SGRO_PDRO], [VL_RMNC_SGRO_MAX], [DT_FIM_VGC_RMNC]        
     FROM [LS_SUS].[ALS_PRODUTO_CRIACAO_DB].DBO.[RMNC_ITEM_MDLD]  WITH(NOLOCK)        
    WHERE (cd_prd = 1152 AND cd_mdld = 2 AND cd_item_mdld = 202 AND ISNULL(DT_FIM_VGC_RMNC,GETDATE()+1) > GETDATE())         
            
    INSERT INTO [VBR008002-016\QUALID].[ALS_PRODUTO_CRIACAO_DB].DBO.[RMNC_ITEM_MDLD_HIST_TB]        
       ([CD_PRD], [CD_MDLD], [CD_ITEM_MDLD], [CD_RMNC_SGRO], [CD_TIP_RMNC_SGRO], [CD_PSS_RMNC_SGRO], [DT_INC_VGC_RMNC], [CD_EST_VCLC], [CD_TIP_BASE_CLC], [PC_RMNC_SGRO_MIN], [PC_RMNC_SGRO_PDRO], [PC_RMNC_SGRO_MAX], [PC_IR_RMNC_SGRO], [VL_RMNC_SGRO_MIN], [VL_RMNC_SGRO_PDRO], [VL_RMNC_SGRO_MAX], [DT_FIM_VGC_RMNC], [DT_INCLUSAO], [DT_ALTERACAO], [USUARIO], [ENVIADO_BB], [DT_ENVIO], [CD_PRD_BB], [CD_MDLD_BB], [DT_ENVIO_ENCERRAMENTO])        
            
    SELECT '1152', '1', '102', [CD_RMNC_SGRO], [CD_TIP_RMNC_SGRO], [CD_PSS_RMNC_SGRO], [DT_INC_VGC_RMNC], [CD_EST_VCLC], [CD_TIP_BASE_CLC], [PC_RMNC_SGRO_MIN], [PC_RMNC_SGRO_PDRO], [PC_RMNC_SGRO_MAX], [PC_IR_RMNC_SGRO], [VL_RMNC_SGRO_MIN], [VL_RMNC_SGRO_PDRO], [VL_RMNC_SGRO_MAX], [DT_FIM_VGC_RMNC], [DT_INCLUSAO], '20200520', '14271121851', [ENVIADO_BB], [DT_ENVIO], [CD_PRD_BB], '5', [DT_ENVIO_ENCERRAMENTO]        
     FROM [LS_SUS].[ALS_PRODUTO_CRIACAO_DB].DBO.[RMNC_ITEM_MDLD_HIST_TB]  WITH(NOLOCK)        
    WHERE (cd_prd = 1152 AND cd_mdld = 2 AND cd_item_mdld = 202 AND ISNULL(DT_FIM_VGC_RMNC,GETDATE()+1) > GETDATE())         
            
    INSERT INTO [VBR008002-016\QUALID].[ALS_PRODUTO_CRIACAO_DB].DBO.[FXA_ETAR_ITEM_MDLD]        
       ([CD_PRD], [CD_MDLD], [CD_ITEM_MDLD], [NR_FXA_ETAR_ITEM], [DT_INC_VGC_FXA], [QT_IDD_INFR_FXA], [QT_IDD_SUPR_FXA], [DT_FIM_VGC_FXA])        
            
    SELECT '1152', '1', '102', [NR_FXA_ETAR_ITEM], [DT_INC_VGC_FXA], [QT_IDD_INFR_FXA], [QT_IDD_SUPR_FXA], [DT_FIM_VGC_FXA]        
     FROM [LS_SUS].[ALS_PRODUTO_CRIACAO_DB].DBO.[FXA_ETAR_ITEM_MDLD]  WITH(NOLOCK)        
    WHERE (cd_prd = 1152 AND cd_mdld = 2 AND cd_item_mdld = 202 AND ISNULL(DT_FIM_VGC_FXA,GETDATE()+1) > GETDATE())         
            
    INSERT INTO [VBR008002-016\QUALID].[ALS_PRODUTO_CRIACAO_DB].DBO.[FXA_ETAR_ITEM_MDLD_HIST_TB]        
       ([CD_PRD], [CD_MDLD], [CD_ITEM_MDLD], [NR_FXA_ETAR_ITEM], [NR_VERSAO], [DT_INC_VGC_FXA], [QT_IDD_INFR_FXA], [QT_IDD_SUPR_FXA], [DT_FIM_VGC_FXA], [DT_INCLUSAO], [DT_ALTERACAO], [USUARIO], [CD_PRD_BB], [CD_MDLD_BB], [ENVIADO_BB], [DT_ENVIO], [DT_ENVIO_ENCERRAMENTO], [NR_VERSAO_REGISTRO], [CD_EST_FXA_ETAR])        
            
    SELECT '1152', '1', '102', [NR_FXA_ETAR_ITEM], '20', [DT_INC_VGC_FXA], [QT_IDD_INFR_FXA], [QT_IDD_SUPR_FXA], [DT_FIM_VGC_FXA], [DT_INCLUSAO], '20200520', '14271121851', [CD_PRD_BB], '5', [ENVIADO_BB], [DT_ENVIO], [DT_ENVIO_ENCERRAMENTO], [NR_VERSAO_REGISTRO], [CD_EST_FXA_ETAR]        
     FROM [LS_SUS].[ALS_PRODUTO_CRIACAO_DB].DBO.[FXA_ETAR_ITEM_MDLD_HIST_TB]  WITH(NOLOCK)        
    WHERE (cd_prd = 1152 AND cd_mdld = 2 AND cd_item_mdld = 202 AND nr_versao = 19)         
            
    INSERT INTO [VBR008002-016\QUALID].[ALS_PRODUTO_CRIACAO_DB].DBO.[CJT_CBT_FXA_ETAR]        
       ([CD_PRD], [CD_MDLD], [CD_ITEM_MDLD], [NR_FXA_ETAR_ITEM], [CD_CJT_CBT], [CD_CBT], [DT_INC_CJT_FXA], [DT_FIM_VGC_FXA], [CD_EST_CJT_FXA], [VL_MIN_IPTC_FXA], [VL_MAX_IPTC_FXA], [VL_PDRO_IPTC_FXA], [PC_CLC_PREM_FXA], [IN_CBT_BASC], [IN_CBT_OBGT], [CD_TIP_PLN_CBT], [NR_APSC_TLA_CJT], [NR_APSC_TLA_CBT], [PC_CLC_PREM_FXA_RNV])        
            
    SELECT '1152', '1', '102', [NR_FXA_ETAR_ITEM], [CD_CJT_CBT], [CD_CBT], [DT_INC_CJT_FXA], [DT_FIM_VGC_FXA], [CD_EST_CJT_FXA], [VL_MIN_IPTC_FXA], [VL_MAX_IPTC_FXA], [VL_PDRO_IPTC_FXA], [PC_CLC_PREM_FXA], [IN_CBT_BASC], [IN_CBT_OBGT], [CD_TIP_PLN_CBT], [NR_APSC_TLA_CJT], [NR_APSC_TLA_CBT], [PC_CLC_PREM_FXA_RNV]        
     FROM [LS_SUS].[ALS_PRODUTO_CRIACAO_DB].DBO.[CJT_CBT_FXA_ETAR]  WITH(NOLOCK)        
    WHERE (cd_prd = 1152 AND cd_mdld = 2 AND cd_item_mdld = 202 AND ISNULL(DT_FIM_VGC_FXA,GETDATE()+1) > GETDATE())         
            
    INSERT INTO [VBR008002-016\QUALID].[ALS_PRODUTO_CRIACAO_DB].DBO.[CJT_CBT_FXA_ETAR_HIST_TB]        
       ([CD_PRD], [CD_MDLD], [CD_ITEM_MDLD], [NR_FXA_ETAR_ITEM], [CD_CJT_CBT], [CD_CBT], [NR_VERSAO], [DT_INC_CJT_FXA], [DT_FIM_VGC_FXA], [CD_EST_CJT_FXA], [VL_MIN_IPTC_FXA], [VL_MAX_IPTC_FXA], [VL_PDRO_IPTC_FXA], [PC_CLC_PREM_FXA], [IN_CBT_BASC], [IN_CBT_OBGT], [CD_TIP_PLN_CBT], [NR_APSC_TLA_CJT], [NR_APSC_TLA_CBT], [PC_CLC_PREM_FXA_RNV], [DT_INCLUSAO], [DT_ALTERACAO], [USUARIO], [CD_PRD_BB], [CD_MDLD_BB], [ENVIADO_BB], [DT_ENVIO], [DT_ENVIO_ENCERRAMENTO], [NR_VERSAO_REGISTRO])        
            
    SELECT '1152', '1', '102', [NR_FXA_ETAR_ITEM], [CD_CJT_CBT], [CD_CBT], '20', [DT_INC_CJT_FXA], [DT_FIM_VGC_FXA], [CD_EST_CJT_FXA], [VL_MIN_IPTC_FXA], [VL_MAX_IPTC_FXA], [VL_PDRO_IPTC_FXA], [PC_CLC_PREM_FXA], [IN_CBT_BASC], [IN_CBT_OBGT], [CD_TIP_PLN_CBT], [NR_APSC_TLA_CJT], [NR_APSC_TLA_CBT], [PC_CLC_PREM_FXA_RNV], [DT_INCLUSAO], '20200520', '14271121851', [CD_PRD_BB], '5', [ENVIADO_BB], [DT_ENVIO], [DT_ENVIO_ENCERRAMENTO], [NR_VERSAO_REGISTRO]        
     FROM [LS_SUS].[ALS_PRODUTO_CRIACAO_DB].DBO.[CJT_CBT_FXA_ETAR_HIST_TB]  WITH(NOLOCK)        
    WHERE (cd_prd = 1152 AND cd_mdld = 2 AND cd_item_mdld = 202 AND nr_versao = 19)         
            
    INSERT INTO [VBR008002-016\QUALID].[ALS_PRODUTO_CRIACAO_DB].DBO.[PLN_AST_ITEM_MDLD]        
       ([CD_PRD], [CD_MDLD], [CD_ITEM_MDLD], [CD_PLN_AST], [DT_INC_PLN_AST], [IN_OBGD_PLN_AST], [CD_EST_PLN_AST], [VL_SRVC_AST], [DT_FIM_PLN_AST], [TX_DCR_PLN_AST])        
            
    SELECT '1152', '1', '102', [CD_PLN_AST], [DT_INC_PLN_AST], [IN_OBGD_PLN_AST], [CD_EST_PLN_AST], [VL_SRVC_AST], [DT_FIM_PLN_AST], [TX_DCR_PLN_AST]        
     FROM [LS_SUS].[ALS_PRODUTO_CRIACAO_DB].DBO.[PLN_AST_ITEM_MDLD]  WITH(NOLOCK)        
    WHERE (cd_prd = 1152 AND cd_mdld = 2 AND cd_item_mdld = 202 AND ISNULL(DT_FIM_PLN_AST,GETDATE()+1) > GETDATE())         
            
    INSERT INTO [VBR008002-016\QUALID].[ALS_PRODUTO_CRIACAO_DB].DBO.[PLN_AST_ITEM_MDLD_HIST_TB]        
       ([CD_PRD], [CD_MDLD], [CD_ITEM_MDLD], [CD_PLN_AST], [NR_VERSAO], [DT_INC_PLN_AST], [IN_OBGD_PLN_AST], [CD_EST_PLN_AST], [VL_SRVC_AST], [DT_FIM_PLN_AST], [USUARIO], [DT_INCLUSAO], [DT_ALTERACAO], [CD_PRD_BB], [CD_MDLD_BB], [ENVIADO_BB], [DT_ENVIO], [DT_ENVIO_ENCERRAMENTO], [TX_DCR_PLN_AST])        
            
    SELECT '1152', '1', '102', [CD_PLN_AST], '20', [DT_INC_PLN_AST], [IN_OBGD_PLN_AST], [CD_EST_PLN_AST], [VL_SRVC_AST], [DT_FIM_PLN_AST], '14271121851', [DT_INCLUSAO], '20200520', [CD_PRD_BB], '5', [ENVIADO_BB], [DT_ENVIO], [DT_ENVIO_ENCERRAMENTO], [TX_DCR_PLN_AST]        
     FROM [LS_SUS].[ALS_PRODUTO_CRIACAO_DB].DBO.[PLN_AST_ITEM_MDLD_HIST_TB]  WITH(NOLOCK)        
    WHERE (cd_prd = 1152 AND cd_mdld = 2 AND cd_item_mdld = 202 AND nr_versao = 19)         
            
    INSERT INTO [VBR008002-016\QUALID].[ALS_PRODUTO_CRIACAO_DB].DBO.[PCLT_SGRO]        
       ([CD_PRD], [CD_MDLD], [CD_ITEM_MDLD], [CD_TIP_PCLT_SGRO], [CD_CTGR_CLI], [NR_PCL_SGRO], [DT_INC_VGC_PCLT], [PC_JUR_PCLT_SGRO], [DT_FIM_VGC_PCLT])        
            
    SELECT '1152', '1', '102', [CD_TIP_PCLT_SGRO], [CD_CTGR_CLI], [NR_PCL_SGRO], [DT_INC_VGC_PCLT], [PC_JUR_PCLT_SGRO], [DT_FIM_VGC_PCLT]        
     FROM [LS_SUS].[ALS_PRODUTO_CRIACAO_DB].DBO.[PCLT_SGRO]  WITH(NOLOCK)        
    WHERE (cd_prd = 1152 AND cd_mdld = 2 AND cd_item_mdld = 202 AND ISNULL(DT_FIM_VGC_PCLT,GETDATE()+1) > GETDATE())         
            
    INSERT INTO [VBR008002-016\QUALID].[ALS_PRODUTO_CRIACAO_DB].DBO.[PCLT_SGRO_HIST_TB]        
       ([CD_PRD], [CD_MDLD], [CD_ITEM_MDLD], [CD_TIP_PCLT_SGRO], [CD_CTGR_CLI], [NR_PCL_SGRO], [NR_VERSAO], [DT_INC_VGC_PCLT], [PC_JUR_PCLT_SGRO], [DT_FIM_VGC_PCLT], [ENVIADO_BB], [DT_ENVIO], [CD_PRD_BB], [CD_MDLD_BB], [USUARIO], [dt_inclusao], [dt_alteracao], [DT_ENVIO_ENCERRAMENTO], [CD_EST_PCLT_SGRO])        
            
    SELECT '1152', '1', '102', [CD_TIP_PCLT_SGRO], [CD_CTGR_CLI], [NR_PCL_SGRO], '20', [DT_INC_VGC_PCLT], [PC_JUR_PCLT_SGRO], [DT_FIM_VGC_PCLT], [ENVIADO_BB], [DT_ENVIO], [CD_PRD_BB], '5', '14271121851', [dt_inclusao], '20200520', [DT_ENVIO_ENCERRAMENTO], [CD_EST_PCLT_SGRO]        
     FROM [LS_SUS].[ALS_PRODUTO_CRIACAO_DB].DBO.[PCLT_SGRO_HIST_TB]  WITH(NOLOCK)        
    WHERE (cd_prd = 1152 AND cd_mdld = 2 AND cd_item_mdld = 202 AND nr_versao = 19)         
            
    INSERT INTO [VBR008002-016\QUALID].[ALS_PRODUTO_CRIACAO_DB].DBO.[RSA_SGRO]        
       ([CD_PRD], [CD_MDLD], [CD_ITEM_MDLD], [CD_CTGR_RSA_SGRO], [CD_MTV_RSA_SGRO], [DT_INC_RSA_SGRO], [DT_FIM_RSA_SGRO], [CD_VCLC_RSA], [TX_RSA_SGRO])        
            
    SELECT '1152', '1', '102', [CD_CTGR_RSA_SGRO], [CD_MTV_RSA_SGRO], [DT_INC_RSA_SGRO], [DT_FIM_RSA_SGRO], [CD_VCLC_RSA], [TX_RSA_SGRO]        
     FROM [LS_SUS].[ALS_PRODUTO_CRIACAO_DB].DBO.[RSA_SGRO]  WITH(NOLOCK)        
    WHERE (cd_prd = 1152 AND cd_mdld = 2 AND cd_item_mdld = 202 AND ISNULL(DT_FIM_RSA_SGRO,GETDATE()+1) > GETDATE())         
            
    INSERT INTO [VBR008002-016\QUALID].[ALS_PRODUTO_CRIACAO_DB].DBO.[RSA_SGRO_HIST_TB]        
       ([CD_PRD], [CD_MDLD], [CD_ITEM_MDLD], [CD_CTGR_RSA_SGRO], [CD_MTV_RSA_SGRO], [NR_VERSAO], [DT_INC_RSA_SGRO], [DT_FIM_RSA_SGRO], [CD_VCLC_RSA], [TX_RSA_SGRO], [ENVIADO_BB], [DT_ENVIO], [CD_PRD_BB], [CD_MDLD_BB], [USUARIO], [DT_INCLUSAO], [DT_ALTERACAO], [DT_ENVIO_ENCERRAMENTO], [CD_EST_RSA_SGRO])        
            
    SELECT '1152', '1', '102', [CD_CTGR_RSA_SGRO], [CD_MTV_RSA_SGRO], '20', [DT_INC_RSA_SGRO], [DT_FIM_RSA_SGRO], [CD_VCLC_RSA], [TX_RSA_SGRO], [ENVIADO_BB], [DT_ENVIO], [CD_PRD_BB], '5', '14271121851', [DT_INCLUSAO], '20200520', [DT_ENVIO_ENCERRAMENTO], [CD_EST_RSA_SGRO]        
     FROM [LS_SUS].[ALS_PRODUTO_CRIACAO_DB].DBO.[RSA_SGRO_HIST_TB]  WITH(NOLOCK)        
    WHERE (cd_prd = 1152 AND cd_mdld = 2 AND cd_item_mdld = 202 AND nr_versao = 19)         
            
    INSERT INTO [VBR008002-016\QUALID].[ALS_PRODUTO_CRIACAO_DB].DBO.[TX_LNH_CLSL]        
       ([CD_CLSL], [NR_SEQL_LNH_CLSL], [DT_INC_LNH_CLSL], [DT_FIM_LNH_CLSL], [CD_EST_LNH_CLSL], [TX_CTU_LNH_CLSL], [TX_INFM_MTV_RSA], [ENVIADO_BB], [DT_ENVIO])        
            
    SELECT [CD_CLSL], [NR_SEQL_LNH_CLSL], [DT_INC_LNH_CLSL], [DT_FIM_LNH_CLSL], [CD_EST_LNH_CLSL], [TX_CTU_LNH_CLSL], [TX_INFM_MTV_RSA], [ENVIADO_BB], [DT_ENVIO]        
     FROM [LS_SUS].[ALS_PRODUTO_CRIACAO_DB].DBO.[TX_LNH_CLSL]  WITH(NOLOCK)        
    WHERE (cd_clsl in (339,100652,100653,100654,100655,100656,100657,100658,100659,100660,100661,100869,100870,100871,100877,100942,100943,101017,101101,101102,101103,101104,101105,101106,101301,101302,101411,101523) AND ISNULL(DT_FIM_LNH_CLSL,GETDATE()+1) > GETDATE() AND 2 = 1)         
            
    INSERT INTO [VBR008002-016\QUALID].[ALS_PRODUTO_CRIACAO_DB].DBO.[TX_LNH_CLSL_HIST_TB]        
       ([CD_CLSL], [NR_SEQL_LNH_CLSL], [DT_INC_LNH_CLSL], [NR_VERSAO], [CD_EST_LNH_CLSL], [DT_FIM_LNH_CLSL], [TX_CTU_LNH_CLSL], [TX_INFM_MTV_RSA], [USUARIO], [DT_INCLUSAO], [DT_ALTERACAO], [dt_envio], [enviado_bb])        
            
    SELECT [CD_CLSL], [NR_SEQL_LNH_CLSL], [DT_INC_LNH_CLSL], '20', [CD_EST_LNH_CLSL], [DT_FIM_LNH_CLSL], [TX_CTU_LNH_CLSL], [TX_INFM_MTV_RSA], '14271121851', [DT_INCLUSAO], '20200520', [dt_envio], [enviado_bb]        
     FROM [LS_SUS].[ALS_PRODUTO_CRIACAO_DB].DBO.[TX_LNH_CLSL_HIST_TB]  WITH(NOLOCK)        
    WHERE (cd_clsl in (339,100652,100653,100654,100655,100656,100657,100658,100659,100660,100661,100869,100870,100871,100877,100942,100943,101017,101101,101102,101103,101104,101105,101106,101301,101302,101411,101523) AND nr_versao = 19 AND 2 = 1)         
            
    INSERT INTO [VBR008002-016\QUALID].[ALS_PRODUTO_CRIACAO_DB].DBO.[TX_RDZ_CBT]        
       ([CD_CBT], [DT_INC_VGC_CBT], [CD_EST_VCLC], [DT_FIM_VGC_CBT], [TX_RDZ_CBT], [TX_INFM_MTV_RSA], [ENVIADO_BB], [DT_ENVIO])        
            
    SELECT [CD_CBT], [DT_INC_VGC_CBT], [CD_EST_VCLC], [DT_FIM_VGC_CBT], [TX_RDZ_CBT], [TX_INFM_MTV_RSA], [ENVIADO_BB], [DT_ENVIO]        
     FROM [LS_SUS].[ALS_PRODUTO_CRIACAO_DB].DBO.[TX_RDZ_CBT]  WITH(NOLOCK)        
    WHERE (cd_cbt in (339) AND ISNULL(DT_FIM_VGC_CBT,GETDATE()+1) > GETDATE() AND 2 = 1)         
            
    INSERT INTO [VBR008002-016\QUALID].[ALS_PRODUTO_CRIACAO_DB].DBO.[TX_RDZ_CBT_HIST_TB]        
       ([CD_CBT], [DT_INC_VGC_CBT], [NR_VERSAO], [CD_EST_VCLC], [DT_FIM_VGC_CBT], [TX_RDZ_CBT], [USUARIO], [DT_INCLUSAO], [DT_ALTERACAO], [TX_INFM_MTV_RSA])        
            
    SELECT [CD_CBT], [DT_INC_VGC_CBT], '20', [CD_EST_VCLC], [DT_FIM_VGC_CBT], [TX_RDZ_CBT], '14271121851', [DT_INCLUSAO], '20200520', [TX_INFM_MTV_RSA]        
     FROM [LS_SUS].[ALS_PRODUTO_CRIACAO_DB].DBO.[TX_RDZ_CBT_HIST_TB]  WITH(NOLOCK)        
    WHERE (cd_cbt in (339) AND nr_versao = 19 AND 2 = 1)         
            
    INSERT INTO [VBR008002-016\QUALID].[ALS_PRODUTO_CRIACAO_DB].DBO.[TX_LNH_CBT]        
       ([CD_CBT], [NR_SEQL_LNH_CBT], [DT_INC_LNH_CBT], [CD_EST_VCLC], [DT_FIM_LNH_CBT], [TX_CTU_LNH_CBT], [TX_INFM_MTV_RSA], [ENVIADO_BB], [DT_ENVIO])        
            
    SELECT [CD_CBT], [NR_SEQL_LNH_CBT], [DT_INC_LNH_CBT], [CD_EST_VCLC], [DT_FIM_LNH_CBT], [TX_CTU_LNH_CBT], [TX_INFM_MTV_RSA], [ENVIADO_BB], [DT_ENVIO]        
     FROM [LS_SUS].[ALS_PRODUTO_CRIACAO_DB].DBO.[TX_LNH_CBT]  WITH(NOLOCK)        
    WHERE (cd_cbt in (339) AND ISNULL(DT_FIM_LNH_CBT,GETDATE()+1) > GETDATE() AND 2 = 1)         
            
    INSERT INTO [VBR008002-016\QUALID].[ALS_PRODUTO_CRIACAO_DB].DBO.[TX_LNH_CBT_HIST_TB]        
       ([CD_CBT], [NR_SEQL_LNH_CBT], [DT_INC_LNH_CBT], [NR_VERSAO], [CD_EST_VCLC], [DT_FIM_LNH_CBT], [TX_CTU_LNH_CBT], [USUARIO], [DT_INCLUSAO], [DT_ALTERACAO], [TX_INFM_MTV_RSA])        
            
    SELECT [CD_CBT], [NR_SEQL_LNH_CBT], [DT_INC_LNH_CBT], '20', [CD_EST_VCLC], [DT_FIM_LNH_CBT], [TX_CTU_LNH_CBT], '14271121851', [DT_INCLUSAO], '20200520', [TX_INFM_MTV_RSA]        
     FROM [LS_SUS].[ALS_PRODUTO_CRIACAO_DB].DBO.[TX_LNH_CBT_HIST_TB]  WITH(NOLOCK)        
    WHERE (cd_cbt in (339) AND nr_versao = 19 AND 2 = 1)         
            
    INSERT INTO [VBR008002-016\QUALID].[ALS_PRODUTO_CRIACAO_DB].DBO.[SUBRAMO_TB]        
       ([CD_PRD], [CD_MDLD], [CD_ITEM_MDLD], [permite_aviso_basica], [processa_reintegracao_is], [processa_reducao_is], [max_dias_atraso_primeira], [max_dias_atraso], [processa_cancelamento], [qtd_min_cob], [tp_calculo_iof], [processa_pre_analise], [ind_situacao], [usuario], [dt_inclusao], [dt_alteracao])        
            
    SELECT '1152', '1', '102', [permite_aviso_basica], [processa_reintegracao_is], [processa_reducao_is], [max_dias_atraso_primeira], [max_dias_atraso], [processa_cancelamento], [qtd_min_cob], [tp_calculo_iof], [processa_pre_analise], [ind_situacao], '14271121851', [dt_inclusao], '20200520'        
     FROM [LS_SUS].[ALS_PRODUTO_CRIACAO_DB].DBO.[SUBRAMO_TB]  WITH(NOLOCK)        
    WHERE (cd_prd = 1152 AND cd_mdld = 2 AND cd_item_mdld = 202)         
            
    INSERT INTO [VBR008002-016\QUALID].[ALS_PRODUTO_CRIACAO_DB].DBO.[SUBRAMO_HIST_TB]        
       ([CD_PRD], [CD_MDLD], [CD_ITEM_MDLD], [nr_versao], [permite_aviso_basica], [processa_reintegracao_is], [processa_reducao_is], [max_dias_atraso_primeira], [max_dias_atraso], [processa_cancelamento], [qtd_min_cob], [tp_calculo_iof], [processa_pre_analise], [ind_situacao], [usuario], [dt_inclusao], [dt_alteracao])        
            
    SELECT '1152', '1', '102', '20', [permite_aviso_basica], [processa_reintegracao_is], [processa_reducao_is], [max_dias_atraso_primeira], [max_dias_atraso], [processa_cancelamento], [qtd_min_cob], [tp_calculo_iof], [processa_pre_analise], [ind_situacao], '14271121851', [dt_inclusao], '20200520'        
     FROM [LS_SUS].[ALS_PRODUTO_CRIACAO_DB].DBO.[SUBRAMO_HIST_TB]  WITH(NOLOCK)        
    WHERE (cd_prd = 1152 AND cd_mdld = 2 AND cd_item_mdld = 202 AND nr_versao = 19)         
            
    INSERT INTO [VBR008002-016\QUALID].[ALS_PRODUTO_CRIACAO_DB].DBO.[QSTN_ITEM_MDLD]        
       ([CD_PRD], [CD_MDLD], [CD_ITEM_MDLD], [NR_QSTN], [DT_INC_QSTN], [DT_FIM_QSTN], [CD_EST_QSTN], [CD_TIP_QSTN], [NR_CD_TIP_QSTN])        
            
    SELECT '1152', '1', '102', [NR_QSTN], [DT_INC_QSTN], [DT_FIM_QSTN], [CD_EST_QSTN], [CD_TIP_QSTN], [NR_CD_TIP_QSTN]        
     FROM [LS_SUS].[ALS_PRODUTO_CRIACAO_DB].DBO.[QSTN_ITEM_MDLD]  WITH(NOLOCK)        
    WHERE (cd_prd = 1152 AND cd_mdld = 2 AND cd_item_mdld = 202 AND ISNULL(DT_FIM_QSTN,GETDATE()+1) > GETDATE())         
            
    INSERT INTO [VBR008002-016\QUALID].[ALS_PRODUTO_CRIACAO_DB].DBO.[QSTN_ITEM_MDLD_HIST_TB]        
       ([CD_PRD], [CD_MDLD], [CD_ITEM_MDLD], [NR_QSTN], [NR_VERSAO], [DT_INC_QSTN], [CD_EST_QSTN], [CD_TIP_QSTN], [NR_CD_TIP_QSTN], [DT_FIM_QSTN], [TX_INFM_MTV_RSA], [CD_PRD_BB], [CD_MDLD_BB], [ENVIADO_BB], [DT_ENVIO], [USUARIO], [DT_INCLUSAO], [DT_ALTERACAO], [DT_ENVIO_ENCERRAMENTO])        
            
    SELECT '1152', '1', '102', [NR_QSTN], '20', [DT_INC_QSTN], [CD_EST_QSTN], [CD_TIP_QSTN], [NR_CD_TIP_QSTN], [DT_FIM_QSTN], [TX_INFM_MTV_RSA], [CD_PRD_BB], '5', [ENVIADO_BB], [DT_ENVIO], '14271121851', [DT_INCLUSAO], '20200520', [DT_ENVIO_ENCERRAMENTO]        
     FROM [LS_SUS].[ALS_PRODUTO_CRIACAO_DB].DBO.[QSTN_ITEM_MDLD_HIST_TB]  WITH(NOLOCK)        
    WHERE (cd_prd = 1152 AND cd_mdld = 2 AND cd_item_mdld = 202 AND nr_versao = 19)         
            
    INSERT INTO [VBR008002-016\QUALID].[ALS_PRODUTO_CRIACAO_DB].DBO.[FMA_PGTO_ITEM_MDLD]        
       ([CD_PRD], [CD_MDLD], [CD_ITEM_MDLD], [CD_FMA_PGTO], [DT_INC_FMA_PGTO], [CD_EST_VCLC], [DT_FIM_FMA_PGTO])        
            
    SELECT '1152', '1', '102', [CD_FMA_PGTO], [DT_INC_FMA_PGTO], [CD_EST_VCLC], [DT_FIM_FMA_PGTO]        
     FROM [LS_SUS].[ALS_PRODUTO_CRIACAO_DB].DBO.[FMA_PGTO_ITEM_MDLD]  WITH(NOLOCK)        
    WHERE (cd_prd = 1152 AND cd_mdld = 2 AND cd_item_mdld = 202 AND ISNULL(DT_FIM_FMA_PGTO,GETDATE()+1) > GETDATE())         
            
    INSERT INTO [VBR008002-016\QUALID].[ALS_PRODUTO_CRIACAO_DB].DBO.[FMA_PGTO_ITEM_MDLD_HIST_TB]        
       ([CD_PRD], [CD_MDLD], [CD_ITEM_MDLD], [CD_FMA_PGTO], [NR_VERSAO], [DT_INC_FMA_PGTO], [DT_FIM_FMA_PGTO], [CD_EST_VCLC], [ENVIADO_BB], [DT_ENVIO], [CD_PRD_BB], [CD_MDLD_BB], [TX_INFM_MTV_RSA], [USUARIO], [DT_INCLUSAO], [DT_ALTERACAO], [DT_ENVIO_ENCERRAMENTO])        
            
    SELECT '1152', '1', '102', [CD_FMA_PGTO], '20', [DT_INC_FMA_PGTO], [DT_FIM_FMA_PGTO], [CD_EST_VCLC], [ENVIADO_BB], [DT_ENVIO], [CD_PRD_BB], '5', [TX_INFM_MTV_RSA], '14271121851', [DT_INCLUSAO], '20200520', [DT_ENVIO_ENCERRAMENTO]        
     FROM [LS_SUS].[ALS_PRODUTO_CRIACAO_DB].DBO.[FMA_PGTO_ITEM_MDLD_HIST_TB]  WITH(NOLOCK)        
    WHERE (cd_prd = 1152 AND cd_mdld = 2 AND cd_item_mdld = 202 AND nr_versao = 19)         
            
    INSERT INTO [VBR008002-016\QUALID].[ALS_PRODUTO_CRIACAO_DB].DBO.[ETLE_ITEM_MDLD]        
       ([CD_PRD], [CD_MDLD], [CD_ITEM_MDLD], [CD_PSS_ETLE], [DT_INC_ETLE], [PC_ETLE_PDRO], [CD_EST_ETLE], [PC_ETLE_MIN], [DT_FIM_ETLE], [PC_ETLE_MAX], [NR_AG_ETLE], [NR_CC_ETLE], [NR_APLC_ETLE])        
            
    SELECT '1152', '1', '102', [CD_PSS_ETLE], [DT_INC_ETLE], [PC_ETLE_PDRO], [CD_EST_ETLE], [PC_ETLE_MIN], [DT_FIM_ETLE], [PC_ETLE_MAX], [NR_AG_ETLE], [NR_CC_ETLE], [NR_APLC_ETLE]        
     FROM [LS_SUS].[ALS_PRODUTO_CRIACAO_DB].DBO.[ETLE_ITEM_MDLD]  WITH(NOLOCK)        
    WHERE (cd_prd = 1152 AND cd_mdld = 2 AND cd_item_mdld = 202 AND ISNULL(DT_FIM_ETLE,GETDATE()+1) > GETDATE())         
            
    INSERT INTO [VBR008002-016\QUALID].[ALS_PRODUTO_CRIACAO_DB].DBO.[ETLE_ITEM_MDLD_HIST_TB]        
       ([CD_PRD], [CD_MDLD], [CD_ITEM_MDLD], [NR_VERSAO], [CD_PSS_ETLE], [DT_INC_ETLE], [PC_ETLE_PDRO], [CD_EST_ETLE], [PC_ETLE_MIN], [DT_FIM_ETLE], [PC_ETLE_MAX], [CD_PRD_BB], [CD_MDLD_BB], [ENVIADO_BB], [DT_ENVIO], [NR_AG_ETLE], [NR_CC_ETLE], [USUARIO], [DT_INCLUSAO], [DT_ALTERACAO], [DT_ENVIO_ENCERRAMENTO], [NR_APLC_ETLE], [cliente_id])        
            
    SELECT '1152', '1', '102', '20', [CD_PSS_ETLE], [DT_INC_ETLE], [PC_ETLE_PDRO], [CD_EST_ETLE], [PC_ETLE_MIN], [DT_FIM_ETLE], [PC_ETLE_MAX], [CD_PRD_BB], '5', [ENVIADO_BB], [DT_ENVIO], [NR_AG_ETLE], [NR_CC_ETLE], '14271121851', [DT_INCLUSAO], '20200520', [DT_ENVIO_ENCERRAMENTO], [NR_APLC_ETLE], [cliente_id]        
     FROM [LS_SUS].[ALS_PRODUTO_CRIACAO_DB].DBO.[ETLE_ITEM_MDLD_HIST_TB]  WITH(NOLOCK)        
    WHERE (cd_prd = 1152 AND cd_mdld = 2 AND cd_item_mdld = 202 AND nr_versao = 19)         
            
    INSERT INTO [VBR008002-016\QUALID].[ALS_PRODUTO_CRIACAO_DB].DBO.[EDS_ITEM_MDLD]        
       ([CD_PRD], [CD_MDLD], [CD_ITEM_MDLD], [CD_TIP_EDS], [DT_INC_EDS], [CD_EST_VCLC], [DT_FIM_EDS], [IN_OBGD_CTC_EDS], [CD_OGM_EDS], [CD_ORG_CLC_DVLC])        
            
    SELECT '1152', '1', '102', [CD_TIP_EDS], [DT_INC_EDS], [CD_EST_VCLC], [DT_FIM_EDS], [IN_OBGD_CTC_EDS], [CD_OGM_EDS], [CD_ORG_CLC_DVLC]        
     FROM [LS_SUS].[ALS_PRODUTO_CRIACAO_DB].DBO.[EDS_ITEM_MDLD]  WITH(NOLOCK)        
    WHERE (cd_prd = 1152 AND cd_mdld = 2 AND cd_item_mdld = 202 AND ISNULL(DT_FIM_EDS,GETDATE()+1) > GETDATE())         
            
    INSERT INTO [VBR008002-016\QUALID].[ALS_PRODUTO_CRIACAO_DB].DBO.[EDS_ITEM_MDLD_HIST_TB]        
       ([CD_PRD], [CD_MDLD], [CD_ITEM_MDLD], [CD_TIP_EDS], [NR_VERSAO], [DT_INC_EDS], [DT_FIM_EDS], [CD_EST_VCLC], [ENVIADO_BB], [DT_ENVIO], [CD_PRD_BB], [CD_MDLD_BB], [TX_INFM_MTV_RSA], [IN_OBGD_CTC_EDS], [USUARIO], [DT_INCLUSAO], [DT_ALTERACAO], [DT_ENVIO_ENCERRAMENTO], [CD_OGM_EDS], [CD_ORG_CLC_DVLC])        
            
    SELECT '1152', '1', '102', [CD_TIP_EDS], '20', [DT_INC_EDS], [DT_FIM_EDS], [CD_EST_VCLC], [ENVIADO_BB], [DT_ENVIO], [CD_PRD_BB], '5', [TX_INFM_MTV_RSA], [IN_OBGD_CTC_EDS], '14271121851', [DT_INCLUSAO], '20200520', [DT_ENVIO_ENCERRAMENTO], [CD_OGM_EDS], [CD_ORG_CLC_DVLC]        
     FROM [LS_SUS].[ALS_PRODUTO_CRIACAO_DB].DBO.[EDS_ITEM_MDLD_HIST_TB]  WITH(NOLOCK)        
    WHERE (cd_prd = 1152 AND cd_mdld = 2 AND cd_item_mdld = 202 AND nr_versao = 19)         
            
    INSERT INTO [VBR008002-016\QUALID].[ALS_PRODUTO_CRIACAO_DB].DBO.[CLSL_ITEM_MDLD_CBT]        
       ([CD_PRD], [CD_MDLD], [CD_ITEM_MDLD], [CD_CBT], [CD_CLSL], [DT_INC_CLSL], [IN_EXG_CLSL], [CD_EST_CLSL], [DT_FIM_CLSL])        
            
    SELECT '1152', '1', '102', [CD_CBT], [CD_CLSL], [DT_INC_CLSL], [IN_EXG_CLSL], [CD_EST_CLSL], [DT_FIM_CLSL]        
     FROM [LS_SUS].[ALS_PRODUTO_CRIACAO_DB].DBO.[CLSL_ITEM_MDLD_CBT]  WITH(NOLOCK)        
    WHERE (cd_prd = 1152 AND cd_mdld = 2 AND cd_item_mdld = 202 AND ISNULL(DT_FIM_CLSL,GETDATE()+1) > GETDATE())         
            
    INSERT INTO [VBR008002-016\QUALID].[ALS_PRODUTO_CRIACAO_DB].DBO.[CLSL_ITEM_MDLD_CBT_HIST_TB]        
       ([CD_PRD], [CD_MDLD], [CD_ITEM_MDLD], [CD_CBT], [CD_CLSL], [NR_VERSAO], [DT_INC_CLSL], [IN_EXG_CLSL], [CD_EST_CLSL], [DT_FIM_CLSL], [TX_INFM_MTV_RSA], [CD_PRD_BB], [CD_MDLD_BB], [ENVIADO_BB], [DT_ENVIO], [USUARIO], [DT_INCLUSAO], [DT_ALTERACAO], [DT_ENVIO_ENCERRAMENTO], [ORDEM_IMPRESSAO])        
            
    SELECT '1152', '1', '102', [CD_CBT], [CD_CLSL], '20', [DT_INC_CLSL], [IN_EXG_CLSL], [CD_EST_CLSL], [DT_FIM_CLSL], [TX_INFM_MTV_RSA], [CD_PRD_BB], '5', [ENVIADO_BB], [DT_ENVIO], '14271121851', [DT_INCLUSAO], '20200520', [DT_ENVIO_ENCERRAMENTO], [ORDEM_IMPRESSAO]        
     FROM [LS_SUS].[ALS_PRODUTO_CRIACAO_DB].DBO.[CLSL_ITEM_MDLD_CBT_HIST_TB]  WITH(NOLOCK)        
    WHERE (cd_prd = 1152 AND cd_mdld = 2 AND cd_item_mdld = 202 AND nr_versao = 19)         
            
    INSERT INTO [VBR008002-016\QUALID].[ALS_PRODUTO_CRIACAO_DB].DBO.[SGRO_CNL_VND]        
       ([CD_PRD], [CD_MDLD], [CD_ITEM_MDLD], [CD_TIP_CTRC_CBT], [DT_INC_VGC_CNL], [CD_EST_VCLC_CNL], [CANAL_ID], [TP_MEIO_CANAL_ID])        
            
    SELECT '1152', '1', '102', [CD_TIP_CTRC_CBT], [DT_INC_VGC_CNL], [CD_EST_VCLC_CNL], [CANAL_ID], [TP_MEIO_CANAL_ID]        
     FROM [LS_SUS].[ALS_PRODUTO_CRIACAO_DB].DBO.[SGRO_CNL_VND]  WITH(NOLOCK)        
    WHERE (cd_prd = 1152 AND cd_mdld = 2 AND cd_item_mdld = 202)         
            
    INSERT INTO [VBR008002-016\QUALID].[ALS_PRODUTO_CRIACAO_DB].DBO.[SGRO_CNL_VND_HIST_TB]        
       ([CD_PRD], [CD_MDLD], [CD_ITEM_MDLD], [CD_TIP_CTRC_CBT], [DT_INC_VGC_CNL], [CD_EST_VCLC_CNL], [NR_VERSAO], [DT_INCLUSAO], [DT_ALTERACAO], [USUARIO], [CANAL_ID], [TP_MEIO_CANAL_ID], [DT_FIM_VGC_CNL], [DT_ENVIO], [ENVIADO_BB], [DT_ENVIO_ENCERRAMENTO], [CD_PRD_BB], [CD_MDLD_BB])        
            
    SELECT '1152', '1', '102', [CD_TIP_CTRC_CBT], [DT_INC_VGC_CNL], [CD_EST_VCLC_CNL], '20', [DT_INCLUSAO], '20200520', '14271121851', [CANAL_ID], [TP_MEIO_CANAL_ID], [DT_FIM_VGC_CNL], [DT_ENVIO], [ENVIADO_BB], [DT_ENVIO_ENCERRAMENTO], [CD_PRD_BB], '5'        
     FROM [LS_SUS].[ALS_PRODUTO_CRIACAO_DB].DBO.[SGRO_CNL_VND_HIST_TB]  WITH(NOLOCK)        
    WHERE (cd_prd = 1152 AND cd_mdld = 2 AND cd_item_mdld = 202 AND nr_versao = 19)         
            
    INSERT INTO [VBR008002-016\QUALID].[ALS_PRODUTO_CRIACAO_DB].DBO.[CLSL_ITEM_MDLD]        
       ([CD_PRD], [CD_MDLD], [CD_ITEM_MDLD], [CD_CLSL], [DT_INC_CLSL], [IN_EXG_CLSL], [CD_EST_CLSL], [DT_FIM_CLSL])        
            
    SELECT '1152', '1', '102', [CD_CLSL], [DT_INC_CLSL], [IN_EXG_CLSL], [CD_EST_CLSL], [DT_FIM_CLSL]        
     FROM [LS_SUS].[ALS_PRODUTO_CRIACAO_DB].DBO.[CLSL_ITEM_MDLD]  WITH(NOLOCK)        
    WHERE (cd_prd = 1152 AND cd_mdld = 2 AND cd_item_mdld = 202 AND ISNULL(DT_FIM_CLSL,GETDATE()+1) > GETDATE())         
            
    INSERT INTO [VBR008002-016\QUALID].[ALS_PRODUTO_CRIACAO_DB].DBO.[CLSL_ITEM_MDLD_HIST_TB]        
       ([CD_PRD], [CD_MDLD], [CD_ITEM_MDLD], [CD_CLSL], [NR_VERSAO], [DT_INC_CLSL], [IN_EXG_CLSL], [CD_EST_CLSL], [DT_FIM_CLSL], [TX_INFM_MTV_RSA], [CD_PRD_BB], [CD_MDLD_BB], [ENVIADO_BB], [DT_ENVIO], [USUARIO], [DT_INCLUSAO], [DT_ALTERACAO], [DT_ENVIO_ENCERRAMENTO], [ORDEM_IMPRESSAO])        
            
    SELECT '1152', '1', '102', [CD_CLSL], '20', [DT_INC_CLSL], [IN_EXG_CLSL], [CD_EST_CLSL], [DT_FIM_CLSL], [TX_INFM_MTV_RSA], [CD_PRD_BB], '5', [ENVIADO_BB], [DT_ENVIO], '14271121851', [DT_INCLUSAO], '20200520', [DT_ENVIO_ENCERRAMENTO], [ORDEM_IMPRESSAO]        
     FROM [LS_SUS].[ALS_PRODUTO_CRIACAO_DB].DBO.[CLSL_ITEM_MDLD_HIST_TB]  WITH(NOLOCK)        
    WHERE (cd_prd = 1152 AND cd_mdld = 2 AND cd_item_mdld = 202 AND nr_versao = 19)         
            
    INSERT INTO [VBR008002-016\QUALID].[ALS_PRODUTO_CRIACAO_DB].DBO.[CJT_CBT_ITEM_MDLD]        
       ([CD_PRD], [CD_MDLD], [CD_ITEM_MDLD], [CD_CJT_CBT], [CD_CBT], [DT_INC_CBT], [DT_FIM_CBT], [CD_EST_CBT], [CD_CBT_ANT], [VL_MIN_IPTC_SGRD], [VL_MAX_IPTC_SGRD], [PC_MIN_IPTC_SGRD], [PC_MAX_IPTC_SGRD], [PC_CLC_PREM], [QT_MIN_SEL], [QT_MAX_SEL], [IN_CJT_REF], [CD_CJT_CBT_ANT], [IN_CBT_BASC], [NR_APSC_TLA_CJT], [IN_DSC_TCN_PGRS], [VL_PDRO_IPTC_SGRD], [CD_TIPO_SEL_CBT], [NR_APSC_TLA_CBT], [IN_ACMC_CBT], [IN_LC_CST], [IN_ACE_LCGEN], [IN_ACEITA_PI], [IN_AGRAV_NUM_LOCAIS], [PC_IOF], [IN_DANOS_MORAIS], [IN_ACEITA_DANOS_MORAIS], [IN_GARAGISTA], [IN_EXTENSAO], [grupo_ramo], [cod_ramo_contab], [cd_tip_taxa_sgro], [cd_est_vclc_taxa], [CD_CJT_CBT_ANT_SEG], [CD_CBT_ANT_SEG])        
            
    SELECT '1152', '1', '102', [CD_CJT_CBT], [CD_CBT], [DT_INC_CBT], [DT_FIM_CBT], [CD_EST_CBT], [CD_CBT_ANT], [VL_MIN_IPTC_SGRD], [VL_MAX_IPTC_SGRD], [PC_MIN_IPTC_SGRD], [PC_MAX_IPTC_SGRD], [PC_CLC_PREM], [QT_MIN_SEL], [QT_MAX_SEL], [IN_CJT_REF], [CD_CJT_CBT_ANT], [IN_CBT_BASC], [NR_APSC_TLA_CJT], [IN_DSC_TCN_PGRS], [VL_PDRO_IPTC_SGRD], [CD_TIPO_SEL_CBT], [NR_APSC_TLA_CBT], [IN_ACMC_CBT], [IN_LC_CST], [IN_ACE_LCGEN], [IN_ACEITA_PI], [IN_AGRAV_NUM_LOCAIS], [PC_IOF], [IN_DANOS_MORAIS], [IN_ACEITA_DANOS_MORAIS], [IN_GARAGISTA], [IN_EXTENSAO], [grupo_ramo], [cod_ramo_contab], [cd_tip_taxa_sgro], [cd_est_vclc_taxa], [CD_CJT_CBT_ANT_SEG], [CD_CBT_ANT_SEG]        
     FROM [LS_SUS].[ALS_PRODUTO_CRIACAO_DB].DBO.[CJT_CBT_ITEM_MDLD]  WITH(NOLOCK)        
    WHERE (cd_prd = 1152 AND cd_mdld = 2 AND cd_item_mdld = 202 AND ISNULL(DT_FIM_CBT,GETDATE()+1) > GETDATE())         
            
    INSERT INTO [VBR008002-016\QUALID].[ALS_PRODUTO_CRIACAO_DB].DBO.[CJT_CBT_ITEM_MDLD_HIST_TB]        
       ([CD_PRD], [CD_MDLD], [CD_ITEM_MDLD], [CD_CJT_CBT], [CD_CBT], [NR_VERSAO], [DT_INC_CBT], [DT_FIM_CBT], [CD_EST_CBT], [CD_CBT_ANT], [VL_MIN_IPTC_SGRD], [VL_MAX_IPTC_SGRD], [PC_MIN_IPTC_SGRD], [PC_MAX_IPTC_SGRD], [PC_CLC_PREM], [ACUMULA_IS], [TX_INFM_MTV_RSA], [CD_PRD_BB], [CD_MDLD_BB], [ENVIADO_BB], [DT_ENVIO], [QT_MIN_SEL], [QT_MAX_SEL], [IN_CJT_REF], [CD_CJT_CBT_ANT], [NR_APSC_TLA_CJT], [IN_CBT_BASC], [USUARIO], [DT_INCLUSAO], [DT_ALTERACAO], [tp_cob_comp_id], [DT_ENVIO_ENCERRAMENTO], [IN_DSC_TCN_PGRS], [VL_PDRO_IPTC_SGRD], [CD_TIPO_SEL_CBT], [NR_APSC_TLA_CBT], [IN_ACMC_CBT], [ACUMULA_RESSEG], [IN_LC_CST], [IN_ACE_LCGEN], [IN_ACEITA_PI], [IN_AGRAV_NUM_LOCAIS], [PC_IOF], [IN_DANOS_MORAIS], [IN_ACEITA_DANOS_MORAIS], [IN_GARAGISTA], [IN_EXTENSAO], [grupo_ramo], [cod_ramo_contab], [cd_tip_taxa_sgro], [cd_est_vclc_taxa], [CD_CJT_CBT_ANT_SEG], [CD_CBT_ANT_SEG])        
            
    SELECT '1152', '1', '102', [CD_CJT_CBT], [CD_CBT], '20', [DT_INC_CBT], [DT_FIM_CBT], [CD_EST_CBT], [CD_CBT_ANT], [VL_MIN_IPTC_SGRD], [VL_MAX_IPTC_SGRD], [PC_MIN_IPTC_SGRD], [PC_MAX_IPTC_SGRD], [PC_CLC_PREM], [ACUMULA_IS], [TX_INFM_MTV_RSA], [CD_PRD_BB], '5', [ENVIADO_BB], [DT_ENVIO], [QT_MIN_SEL], [QT_MAX_SEL], [IN_CJT_REF], [CD_CJT_CBT_ANT], [NR_APSC_TLA_CJT], [IN_CBT_BASC], '14271121851', [DT_INCLUSAO], '20200520', [tp_cob_comp_id], [DT_ENVIO_ENCERRAMENTO], [IN_DSC_TCN_PGRS], [VL_PDRO_IPTC_SGRD], [CD_TIPO_SEL_CBT], [NR_APSC_TLA_CBT], [IN_ACMC_CBT], [ACUMULA_RESSEG], [IN_LC_CST], [IN_ACE_LCGEN], [IN_ACEITA_PI], [IN_AGRAV_NUM_LOCAIS], [PC_IOF], [IN_DANOS_MORAIS], [IN_ACEITA_DANOS_MORAIS], [IN_GARAGISTA], [IN_EXTENSAO], [grupo_ramo], [cod_ramo_contab], [cd_tip_taxa_sgro], [cd_est_vclc_taxa], [CD_CJT_CBT_ANT_SEG], [CD_CBT_ANT_SEG]        
     FROM [LS_SUS].[ALS_PRODUTO_CRIACAO_DB].DBO.[CJT_CBT_ITEM_MDLD_HIST_TB]  WITH(NOLOCK)        
    WHERE (cd_prd = 1152 AND cd_mdld = 2 AND cd_item_mdld = 202 AND nr_versao = 19)         
            
    INSERT INTO [VBR008002-016\QUALID].[ALS_PRODUTO_CRIACAO_DB].DBO.[RSCO_CBT_CLS_RSCO]        
       ([CD_PRD], [CD_MDLD], [CD_ITEM_MDLD], [CD_CJT_CBT], [CD_CBT], [CD_CLS_RSCO_ATVD], [DT_INC_VGC_CBT], [PC_TAXA_RSCO_CBT])        
            
    SELECT '1152', '1', '102', [CD_CJT_CBT], [CD_CBT], [CD_CLS_RSCO_ATVD], [DT_INC_VGC_CBT], [PC_TAXA_RSCO_CBT]        
     FROM [LS_SUS].[ALS_PRODUTO_CRIACAO_DB].DBO.[RSCO_CBT_CLS_RSCO]  WITH(NOLOCK)        
    WHERE (cd_prd = 1152 AND cd_mdld = 2 AND cd_item_mdld = 202)         
            
    INSERT INTO [VBR008002-016\QUALID].[ALS_PRODUTO_CRIACAO_DB].DBO.[RSCO_CBT_CLS_RSCO_HIST_TB]        
       ([CD_PRD], [CD_MDLD], [CD_ITEM_MDLD], [CD_CJT_CBT], [CD_CBT], [CD_CLS_RSCO_ATVD], [DT_INC_VGC_CBT], [PC_TAXA_RSCO_CBT], [NR_VERSAO], [DT_INICIO_VIGENCIA], [DT_FIM_VIGENCIA], [ENVIADO_BB], [DT_ENVIO], [USUARIO], [DT_INCLUSAO], [DT_ALTERACAO])        
            
    SELECT '1152', '1', '102', [CD_CJT_CBT], [CD_CBT], [CD_CLS_RSCO_ATVD], [DT_INC_VGC_CBT], [PC_TAXA_RSCO_CBT], '20', [DT_INICIO_VIGENCIA], [DT_FIM_VIGENCIA], [ENVIADO_BB], [DT_ENVIO], '14271121851', [DT_INCLUSAO], '20200520'        
     FROM [LS_SUS].[ALS_PRODUTO_CRIACAO_DB].DBO.[RSCO_CBT_CLS_RSCO_HIST_TB]  WITH(NOLOCK)        
    WHERE (cd_prd = 1152 AND cd_mdld = 2 AND cd_item_mdld = 202 AND nr_versao = 19)         
            
    INSERT INTO [VBR008002-016\QUALID].[ALS_PRODUTO_CRIACAO_DB].DBO.[CJT_CBT_CNL_VND]        
       ([CD_PRD], [CD_MDLD], [CD_ITEM_MDLD], [CD_CJT_CBT], [DT_INC_VGC_CJT], [CD_EST_VCLC_CJT], [CANAL_ID], [TP_MEIO_CANAL_ID], [CD_FMA_VND])        
            
    SELECT '1152', '1', '102', [CD_CJT_CBT], [DT_INC_VGC_CJT], [CD_EST_VCLC_CJT], [CANAL_ID], [TP_MEIO_CANAL_ID], [CD_FMA_VND]        
     FROM [LS_SUS].[ALS_PRODUTO_CRIACAO_DB].DBO.[CJT_CBT_CNL_VND]  WITH(NOLOCK)        
    WHERE (cd_prd = 1152 AND cd_mdld = 2 AND cd_item_mdld = 202)         
            
    INSERT INTO [VBR008002-016\QUALID].[ALS_PRODUTO_CRIACAO_DB].DBO.[CJT_CBT_CNL_VND_HIST_TB]        
       ([CD_PRD], [CD_MDLD], [CD_ITEM_MDLD], [CD_CJT_CBT], [DT_INC_VGC_CJT], [CD_EST_VCLC_CJT], [NR_VERSAO], [DT_INCLUSAO], [DT_ALTERACAO], [USUARIO], [CANAL_ID], [TP_MEIO_CANAL_ID], [DT_FIM_VGC_CNL], [DT_ENVIO], [ENVIADO_BB], [DT_ENVIO_ENCERRAMENTO], [CD_PRD_BB], [CD_MDLD_BB], [CD_FMA_VND])        
            
    SELECT '1152', '1', '102', [CD_CJT_CBT], [DT_INC_VGC_CJT], [CD_EST_VCLC_CJT], '20', [DT_INCLUSAO], '20200520', '14271121851', [CANAL_ID], [TP_MEIO_CANAL_ID], [DT_FIM_VGC_CNL], [DT_ENVIO], [ENVIADO_BB], [DT_ENVIO_ENCERRAMENTO], [CD_PRD_BB], '5', [CD_FMA_VND]        
     FROM [LS_SUS].[ALS_PRODUTO_CRIACAO_DB].DBO.[CJT_CBT_CNL_VND_HIST_TB]  WITH(NOLOCK)        
    WHERE (cd_prd = 1152 AND cd_mdld = 2 AND cd_item_mdld = 202 AND nr_versao = 19)         
            
    INSERT INTO [VBR008002-016\QUALID].[ALS_PRODUTO_CRIACAO_DB].DBO.[FRQU_CBT_ITEM_MDLD]        
       ([CD_PRD], [CD_MDLD], [CD_ITEM_MDLD], [CD_CBT], [CD_CJT_CBT], [NR_SEQL_FRQU], [DT_INC_VGC_FRQU], [IN_FRQU_PDRO], [CD_EST_FRQU], [PC_FRQU], [TX_RGR_FRQU], [DT_FIM_VGC_FRQU])        
            
    SELECT '1152', '1', '102', [CD_CBT], [CD_CJT_CBT], [NR_SEQL_FRQU], [DT_INC_VGC_FRQU], [IN_FRQU_PDRO], [CD_EST_FRQU], [PC_FRQU], [TX_RGR_FRQU], [DT_FIM_VGC_FRQU]        
     FROM [LS_SUS].[ALS_PRODUTO_CRIACAO_DB].DBO.[FRQU_CBT_ITEM_MDLD]  WITH(NOLOCK)        
    WHERE (cd_prd = 1152 AND cd_mdld = 2 AND cd_item_mdld = 202 AND ISNULL(DT_FIM_VGC_FRQU,GETDATE()+1) > GETDATE())         
            
    INSERT INTO [VBR008002-016\QUALID].[ALS_PRODUTO_CRIACAO_DB].DBO.[FRQU_CBT_ITEM_MDLD_HIST_TB]        
       ([CD_PRD], [CD_MDLD], [CD_ITEM_MDLD], [CD_CBT], [CD_CJT_CBT], [NR_SEQL_FRQU], [NR_VERSAO], [DT_INC_VGC_FRQU], [IN_FRQU_PDRO], [CD_EST_FRQU], [PC_FRQU], [TX_RGR_FRQU], [DT_FIM_VGC_FRQU], [TX_INFM_MTV_RSA], [CD_PRD_BB], [CD_MDLD_BB], [ENVIADO_BB], [DT_ENVIO], [USUARIO], [DT_INCLUSAO], [DT_ALTERACAO], [DT_ENVIO_ENCERRAMENTO])        
            
    SELECT '1152', '1', '102', [CD_CBT], [CD_CJT_CBT], [NR_SEQL_FRQU], '20', [DT_INC_VGC_FRQU], [IN_FRQU_PDRO], [CD_EST_FRQU], [PC_FRQU], [TX_RGR_FRQU], [DT_FIM_VGC_FRQU], [TX_INFM_MTV_RSA], [CD_PRD_BB], '5', [ENVIADO_BB], [DT_ENVIO], '14271121851', [DT_INCLUSAO], '20200520', [DT_ENVIO_ENCERRAMENTO]        
     FROM [LS_SUS].[ALS_PRODUTO_CRIACAO_DB].DBO.[FRQU_CBT_ITEM_MDLD_HIST_TB]  WITH(NOLOCK)        
    WHERE (cd_prd = 1152 AND cd_mdld = 2 AND cd_item_mdld = 202 AND nr_versao = 19)         
            
    INSERT INTO [VBR008002-016\QUALID].[ALS_PRODUTO_CRIACAO_DB].DBO.[PRD_MDLD_ITEM_TB]        
       ([CD_PRD], [CD_MDLD], [CD_ITEM_MDLD], [DT_INC_VGC], [DT_FIM_VGC], [CD_EST], [TX_INFM_MTV_RSA], [USUARIO], [DT_INCLUSAO], [DT_ALTERACAO])        
            
    SELECT '1152', '1', '102', [DT_INC_VGC], [DT_FIM_VGC], [CD_EST], [TX_INFM_MTV_RSA], '14271121851', [DT_INCLUSAO], '20200520'        
     FROM [LS_SUS].[ALS_PRODUTO_CRIACAO_DB].DBO.[PRD_MDLD_ITEM_TB]  WITH(NOLOCK)        
    WHERE (cd_prd = 1152 AND cd_mdld = 2 AND cd_item_mdld = 202 AND ISNULL(DT_FIM_VGC,GETDATE()+1) > GETDATE())         
    
--****ATUALIZANDO OS DADOS***
        
    CREATE TABLE #CD_CBT469576711  (CD_CBT VARCHAR(4000))        
    DECLARE @CD_CBT469576711 VARCHAR(4000)        
               
    DECLARE CD_CBT469576711  cursor for        
    SELECT [CD_CBT]        
     FROM [LS_SUS].[ALS_PRODUTO_CRIACAO_DB].dbo.[CBT]  WITH(NOLOCK)        
    WHERE (cd_cbt in (339))        
    OPEN CD_CBT469576711        
    FETCH NEXT FROM CD_CBT469576711 INTO @CD_CBT469576711        
    WHILE @@FETCH_STATUS = 0        
    BEGIN        
     IF EXISTS(SELECT * FROM [VBR008002-016\QUALID].[ALS_PRODUTO_CRIACAO_DB].DBO.[CBT]  WITH(NOLOCK) WHERE (CD_CBT = @CD_CBT469576711))        
     BEGIN        
      UPDATE a       
       SET CD_CBT = b.CD_CBT, NOME = b.NOME, DESCRICAO = b.DESCRICAO, PALAVRA_CHAVE = b.PALAVRA_CHAVE, USUARIO = '14271121851', DT_INCLUSAO = b.DT_INCLUSAO, DT_ALTERACAO = '20200520', IN_LC_CST = b.IN_LC_CST        
       FROM [VBR008002-016\QUALID].[ALS_PRODUTO_CRIACAO_DB].DBO.[CBT] AS a  WITH(NOLOCK)        
       INNER JOIN [LS_SUS].[ALS_PRODUTO_CRIACAO_DB].DBO.[CBT] AS b  WITH(NOLOCK) ON (a.[CD_CBT] = b.[CD_CBT])        
      WHERE (b.CD_CBT = @CD_CBT469576711)        
     END        
     ELSE        
     BEGIN             
      INSERT INTO [VBR008002-016\QUALID].[ALS_PRODUTO_CRIACAO_DB].DBO.[CBT]        
         ([CD_CBT], [NOME], [DESCRICAO], [PALAVRA_CHAVE], [USUARIO], [DT_INCLUSAO], [DT_ALTERACAO], [IN_LC_CST])        
         
      SELECT [CD_CBT], [NOME], [DESCRICAO], [PALAVRA_CHAVE], '14271121851', [DT_INCLUSAO], '20200520', [IN_LC_CST]        
       FROM [LS_SUS].[ALS_PRODUTO_CRIACAO_DB].DBO.[CBT]  WITH(NOLOCK)        
      WHERE (CD_CBT = @CD_CBT469576711)        
     END        
    FETCH NEXT FROM CD_CBT469576711 INTO @CD_CBT469576711        
    END        
    CLOSE CD_CBT469576711           
    DEALLOCATE CD_CBT469576711        
    DROP TABLE #CD_CBT469576711         
            
    CREATE TABLE #CD_CBT485576768#NR_VERSAO485576768  (CD_CBT VARCHAR(4000), NR_VERSAO VARCHAR(4000))        
    DECLARE @CD_CBT485576768 VARCHAR(4000), @NR_VERSAO485576768 VARCHAR(4000)        
               
    DECLARE CD_CBT485576768NR_VERSAO485576768  cursor for        
    SELECT [CD_CBT], [NR_VERSAO]        
     FROM [LS_SUS].[ALS_PRODUTO_CRIACAO_DB].dbo.[CBT_HIST_TB]  WITH(NOLOCK)        
    WHERE (cd_cbt in (339))        
    OPEN CD_CBT485576768NR_VERSAO485576768        
    FETCH NEXT FROM CD_CBT485576768NR_VERSAO485576768 INTO @CD_CBT485576768, @NR_VERSAO485576768        
    WHILE @@FETCH_STATUS = 0        
    BEGIN        
     IF EXISTS(SELECT * FROM [VBR008002-016\QUALID].[ALS_PRODUTO_CRIACAO_DB].DBO.[CBT_HIST_TB]  WITH(NOLOCK) WHERE (CD_CBT = @CD_CBT485576768 AND NR_VERSAO = @NR_VERSAO485576768))        
     BEGIN        
      UPDATE a       
       SET CD_CBT = b.CD_CBT, DT_INICIO_VIGENCIA = b.DT_INICIO_VIGENCIA, DT_FIM_VIGENCIA = b.DT_FIM_VIGENCIA, NOME = b.NOME, DESCRICAO = b.DESCRICAO, PALAVRA_CHAVE = b.PALAVRA_CHAVE, USUARIO = '14271121851', DT_INCLUSAO = b.DT_INCLUSAO, DT_ALTERACAO = '20200520', NR_VERSAO = b.NR_VERSAO, tp_cob_comp_id = b.tp_cob_comp_id, ENVIADO_BB = b.ENVIADO_BB, DT_ENVIO = b.DT_ENVIO, IN_LC_CST = b.IN_LC_CST        
       FROM [VBR008002-016\QUALID].[ALS_PRODUTO_CRIACAO_DB].DBO.[CBT_HIST_TB] AS a  WITH(NOLOCK)        
       INNER JOIN [LS_SUS].[ALS_PRODUTO_CRIACAO_DB].DBO.[CBT_HIST_TB] AS b  WITH(NOLOCK) ON (a.[CD_CBT] = b.[CD_CBT] AND a.[NR_VERSAO] = b.[NR_VERSAO])        
      WHERE (b.CD_CBT = @CD_CBT485576768 AND b.NR_VERSAO = @NR_VERSAO485576768)        
     END        
     ELSE        
     BEGIN             
      INSERT INTO [VBR008002-016\QUALID].[ALS_PRODUTO_CRIACAO_DB].DBO.[CBT_HIST_TB]        
         ([CD_CBT], [DT_INICIO_VIGENCIA], [DT_FIM_VIGENCIA], [NOME], [DESCRICAO], [PALAVRA_CHAVE], [USUARIO], [DT_INCLUSAO], [DT_ALTERACAO], [NR_VERSAO], [tp_cob_comp_id], [ENVIADO_BB], [DT_ENVIO], [IN_LC_CST])        
         
      SELECT [CD_CBT], [DT_INICIO_VIGENCIA], [DT_FIM_VIGENCIA], [NOME], [DESCRICAO], [PALAVRA_CHAVE], '14271121851', [DT_INCLUSAO], '20200520', [NR_VERSAO], [tp_cob_comp_id], [ENVIADO_BB], [DT_ENVIO], [IN_LC_CST]        
       FROM [LS_SUS].[ALS_PRODUTO_CRIACAO_DB].DBO.[CBT_HIST_TB]  WITH(NOLOCK)        
      WHERE (CD_CBT = @CD_CBT485576768 AND NR_VERSAO = @NR_VERSAO485576768)        
     END        
    FETCH NEXT FROM CD_CBT485576768NR_VERSAO485576768 INTO @CD_CBT485576768, @NR_VERSAO485576768        
    END        
    CLOSE CD_CBT485576768NR_VERSAO485576768           
    DEALLOCATE CD_CBT485576768NR_VERSAO485576768        
    DROP TABLE #CD_CBT485576768#NR_VERSAO485576768         
            
    CREATE TABLE #CD_CLSL581577110#NR_VERSAO581577110  (CD_CLSL VARCHAR(4000), NR_VERSAO VARCHAR(4000))        
    DECLARE @CD_CLSL581577110 VARCHAR(4000), @NR_VERSAO581577110 VARCHAR(4000)        
               
    DECLARE CD_CLSL581577110NR_VERSAO581577110  cursor for        
    SELECT [CD_CLSL], [NR_VERSAO]        
     FROM [LS_SUS].[ALS_PRODUTO_CRIACAO_DB].dbo.[CLSL_HIST_TB]  WITH(NOLOCK)        
    WHERE (cd_clsl in (339,100652,100653,100654,100655,100656,100657,100658,100659,100660,100661,100869,100870,100871,100877,100942,100943,101017,101101,101102,101103,101104,101105,101106,101301,101302,101411,101523))        
    OPEN CD_CLSL581577110NR_VERSAO581577110        
    FETCH NEXT FROM CD_CLSL581577110NR_VERSAO581577110 INTO @CD_CLSL581577110, @NR_VERSAO581577110        
    WHILE @@FETCH_STATUS = 0        
    BEGIN        
     IF EXISTS(SELECT * FROM [VBR008002-016\QUALID].[ALS_PRODUTO_CRIACAO_DB].DBO.[CLSL_HIST_TB]  WITH(NOLOCK) WHERE (CD_CLSL = @CD_CLSL581577110 AND NR_VERSAO = @NR_VERSAO581577110))        
     BEGIN        
      UPDATE a       
       SET CD_CLSL = b.CD_CLSL, NR_VERSAO = b.NR_VERSAO, DT_INICIO_VIGENCIA = b.DT_INICIO_VIGENCIA, DT_FIM_VIGENCIA = b.DT_FIM_VIGENCIA, DESCR_CLAUSULA = b.DESCR_CLAUSULA, COD_CLAUSULA_IRB = b.COD_CLAUSULA_IRB, TEXTO = b.TEXTO, TIPO = b.TIPO, USUARIO = '14271121851', DT_INCLUSAO = b.DT_INCLUSAO, DT_ALTERACAO = '20200520', TX_PRC_SUSP = b.TX_PRC_SUSP, ramo_id = b.ramo_id, cod_empresa = b.cod_empresa        
       FROM [VBR008002-016\QUALID].[ALS_PRODUTO_CRIACAO_DB].DBO.[CLSL_HIST_TB] AS a  WITH(NOLOCK)        
       INNER JOIN [LS_SUS].[ALS_PRODUTO_CRIACAO_DB].DBO.[CLSL_HIST_TB] AS b  WITH(NOLOCK) ON (a.[CD_CLSL] = b.[CD_CLSL] AND a.[NR_VERSAO] = b.[NR_VERSAO])        
      WHERE (b.CD_CLSL = @CD_CLSL581577110 AND b.NR_VERSAO = @NR_VERSAO581577110)        
     END        
     ELSE        
     BEGIN             
      INSERT INTO [VBR008002-016\QUALID].[ALS_PRODUTO_CRIACAO_DB].DBO.[CLSL_HIST_TB]        
         ([CD_CLSL], [NR_VERSAO], [DT_INICIO_VIGENCIA], [DT_FIM_VIGENCIA], [DESCR_CLAUSULA], [COD_CLAUSULA_IRB], [TEXTO], [TIPO], [USUARIO], [DT_INCLUSAO], [DT_ALTERACAO], [TX_PRC_SUSP], [ramo_id], [cod_empresa])        
         
      SELECT [CD_CLSL], [NR_VERSAO], [DT_INICIO_VIGENCIA], [DT_FIM_VIGENCIA], [DESCR_CLAUSULA], [COD_CLAUSULA_IRB], [TEXTO], [TIPO], '14271121851', [DT_INCLUSAO], '20200520', [TX_PRC_SUSP], [ramo_id], [cod_empresa]        
       FROM [LS_SUS].[ALS_PRODUTO_CRIACAO_DB].DBO.[CLSL_HIST_TB]  WITH(NOLOCK)        
      WHERE (CD_CLSL = @CD_CLSL581577110 AND NR_VERSAO = @NR_VERSAO581577110)        
     END        
    FETCH NEXT FROM CD_CLSL581577110NR_VERSAO581577110 INTO @CD_CLSL581577110, @NR_VERSAO581577110        
    END        
    CLOSE CD_CLSL581577110NR_VERSAO581577110           
    DEALLOCATE CD_CLSL581577110NR_VERSAO581577110        
    DROP TABLE #CD_CLSL581577110#NR_VERSAO581577110         
            
    CREATE TABLE #CD_CLSL533576939  (CD_CLSL VARCHAR(4000))        
    DECLARE @CD_CLSL533576939 VARCHAR(4000)        
               
    DECLARE CD_CLSL533576939  cursor for        
    SELECT [CD_CLSL]        
     FROM [LS_SUS].[ALS_PRODUTO_CRIACAO_DB].dbo.[CLSL]  WITH(NOLOCK)        
    WHERE (cd_clsl in (339,100652,100653,100654,100655,100656,100657,100658,100659,100660,100661,100869,100870,100871,100877,100942,100943,101017,101101,101102,101103,101104,101105,101106,101301,101302,101411,101523))        
    OPEN CD_CLSL533576939        
    FETCH NEXT FROM CD_CLSL533576939 INTO @CD_CLSL533576939        
    WHILE @@FETCH_STATUS = 0        
    BEGIN        
     IF EXISTS(SELECT * FROM [VBR008002-016\QUALID].[ALS_PRODUTO_CRIACAO_DB].DBO.[CLSL]  WITH(NOLOCK) WHERE (CD_CLSL = @CD_CLSL533576939))        
     BEGIN        
      UPDATE a       
       SET CD_CLSL = b.CD_CLSL, DESCR_CLAUSULA = b.DESCR_CLAUSULA, COD_CLAUSULA_IRB = b.COD_CLAUSULA_IRB, TEXTO = b.TEXTO, TIPO = b.TIPO, USUARIO = '14271121851', DT_INCLUSAO = b.DT_INCLUSAO, DT_ALTERACAO = '20200520', TX_PRC_SUSP = b.TX_PRC_SUSP, ramo_id = b.ramo_id, cod_empresa = b.cod_empresa        
       FROM [VBR008002-016\QUALID].[ALS_PRODUTO_CRIACAO_DB].DBO.[CLSL] AS a  WITH(NOLOCK)        
       INNER JOIN [LS_SUS].[ALS_PRODUTO_CRIACAO_DB].DBO.[CLSL] AS b  WITH(NOLOCK) ON (a.[CD_CLSL] = b.[CD_CLSL])        
      WHERE (b.CD_CLSL = @CD_CLSL533576939)        
     END        
     ELSE        
     BEGIN             
      INSERT INTO [VBR008002-016\QUALID].[ALS_PRODUTO_CRIACAO_DB].DBO.[CLSL]        
         ([CD_CLSL], [DESCR_CLAUSULA], [COD_CLAUSULA_IRB], [TEXTO], [TIPO], [USUARIO], [DT_INCLUSAO], [DT_ALTERACAO], [TX_PRC_SUSP], [ramo_id], [cod_empresa])        
         
      SELECT [CD_CLSL], [DESCR_CLAUSULA], [COD_CLAUSULA_IRB], [TEXTO], [TIPO], '14271121851', [DT_INCLUSAO], '20200520', [TX_PRC_SUSP], [ramo_id], [cod_empresa]        
       FROM [LS_SUS].[ALS_PRODUTO_CRIACAO_DB].DBO.[CLSL]  WITH(NOLOCK)        
      WHERE (CD_CLSL = @CD_CLSL533576939)        
     END        
    FETCH NEXT FROM CD_CLSL533576939 INTO @CD_CLSL533576939        
    END        
    CLOSE CD_CLSL533576939           
    DEALLOCATE CD_CLSL533576939        
    DROP TABLE #CD_CLSL533576939         
    
    
    UPDATE ITEM_MDLD_PRD_HIST_TB  
        SET CD_EST_ITEM_MDLD = 'T',  
   COPIA_REALIZADA_QLD = 'S'  
     FROM [VBR008002-016\QUALID].als_produto_criacao_db.dbo.ITEM_MDLD_PRD_HIST_TB ITEM_MDLD_PRD_HIST_TB  
     OUTER APPLY (SELECT MAX(NR_VERSAO) NR_VERSAO  
        FROM [VBR008002-016\QUALID].als_produto_criacao_db.dbo.ITEM_MDLD_PRD_HIST_TB interno  
       WHERE interno.CD_PRD = ITEM_MDLD_PRD_HIST_TB.CD_PRD  
       AND interno.CD_MDLD = ITEM_MDLD_PRD_HIST_TB.CD_MDLD  
       AND interno.CD_ITEM_MDLD = ITEM_MDLD_PRD_HIST_TB.CD_ITEM_MDLD ) versao  
    WHERE ITEM_MDLD_PRD_HIST_TB.CD_PRD = 1152   
      AND ITEM_MDLD_PRD_HIST_TB.CD_MDLD = 1   
      AND ITEM_MDLD_PRD_HIST_TB.CD_ITEM_MDLD = 102   
      AND ITEM_MDLD_PRD_HIST_TB.NR_VERSAO = versao.NR_VERSAO  
    
EXEC seguros_db..PRDS02138_SPI @cd_prd = 1152, @cd_mdld = 1, @cd_item_mdld = 102, @usuario = 'lromero', @EMPRESA = 'AB' 
     
     IF NOT EXISTS(SELECT 1  
      FROM als_produto_db.dbo.produto_workflow_contrato_tb  
     WHERE CD_PRD = 960  
       AND CD_MDLD = '5'  
       AND CD_ITEM_MDLD = '102'  
       AND TP_WF_ID = '202')    
  BEGIN           
    INSERT INTO als_produto_db.dbo.produto_workflow_contrato_tb(CD_PRD,    
                   CD_MDLD,    
                   CD_ITEM_MDLD,    
                   TP_WF_ID,    
                   usuario,    
                   dt_inclusao,    
                   dt_inicio_vigencia)    
                  SELECT 960,  
                   '5',    
                   '102',    
                   '202',    
                   '14271121851',    
                   GETDATE(),    
                   '2020-05-20 00:00:00.000'     
      
   END 
   
   INSERT INTO als_produto_db.dbo.PRDD_SGRV_PRD_AGRL
    ( CD_PRD
     ,CD_MDLD
     ,CD_ITEM_MDLD
    ,CD_MUN_PRD_AGRL
	,CD_PRD_AGRL
	,DT_INC_PRDD_AGRL
	,DT_FIM_PRDD_AGRL
	,DT_INC_PRDC_AGRL
	,DT_FIM_PRDC_AGRL
	,QT_PRDD_SGRV_AGRL
	,IN_IRG_PRD_AGRL
	,CD_CBT
	,DET_PC_MIN_CBT
	,DET_PC_DSG
	,DET_PC_MAX_RSP
	,DET_FT_CBT
	,VL_MIN_CUSTEIO
	,VL_MAX_CUSTEIO
	,PC_SBS_CRITICO
	,PC_SBS_PRONAMP
	,PC_SBS_ORGANICO
	,exibir_calculo_simulador)
	SELECT 
		960
	   ,5
	   ,102
		,CD_MUN_PRD_AGRL
		,CD_PRD_AGRL
		,DT_INC_PRDD_AGRL
		,DT_FIM_PRDD_AGRL
		,DT_INC_PRDC_AGRL
		,DT_FIM_PRDC_AGRL
		,QT_PRDD_SGRV_AGRL
		,IN_IRG_PRD_AGRL
		,CD_CBT
		,DET_PC_MIN_CBT
		,DET_PC_DSG
		,DET_PC_MAX_RSP
		,DET_FT_CBT
		,VL_MIN_CUSTEIO
		,VL_MAX_CUSTEIO
		,PC_SBS_CRITICO
		,PC_SBS_PRONAMP
		,PC_SBS_ORGANICO
		,exibir_calculo_simulador
	FROM als_produto_db.dbo.PRDD_SGRV_PRD_AGRL WITH (NOLOCK)
    WHERE CD_PRD = 960 AND CD_MDLD = 2 AND CD_ITEM_MDLD = 202
    AND dt_fim_prdd_agrl IS NULL
    
    
INSERT INTO als_produto_db.dbo.PRDD_SGRV_PRD_AGRL_HIST_TB
    (CD_PRD
	,CD_MDLD
	,CD_ITEM_MDLD
	,NR_VERSAO
	,CD_MUN_PRD_AGRL
	,CD_PRD_AGRL
	,DT_INC_PRDD_AGRL
	,DT_FIM_PRDD_AGRL
	,DT_INC_PRDC_AGRL
	,DT_FIM_PRDC_AGRL
	,QT_PRDD_SGRV_AGRL
	,CD_PRD_BB
	,CD_MDLD_BB
	,ENVIADO_BB
	,DT_ENVIO
	,dt_inclusao
	,dt_alteracao
	,usuario
	--,lock
	,IN_IRG_PRD_AGRL
	,CD_CBT
	,PC_MIN_CBT
	,PC_DSG
	,PC_MAX_RSP
	,DET_FT_CBT
	,Vl_Min_Custeio
	,Vl_Max_Custeio
	,Produto_id
	,PC_SBS_CRITICO
	,PC_SBS_PRONAMP
	,PC_SBS_ORGANICO
	,exibir_calculo_simulador ) 
    
SELECT 
	960
    ,5
    ,102
	,NR_VERSAO
	,CD_MUN_PRD_AGRL
	,CD_PRD_AGRL
	,DT_INC_PRDD_AGRL
	,DT_FIM_PRDD_AGRL
	,DT_INC_PRDC_AGRL
	,DT_FIM_PRDC_AGRL
	,QT_PRDD_SGRV_AGRL
	,CD_PRD_BB
	,CD_MDLD_BB
	,ENVIADO_BB
	,DT_ENVIO
	,dt_inclusao
	,dt_alteracao
	,usuario
	--,lock
	,IN_IRG_PRD_AGRL
	,CD_CBT
	,PC_MIN_CBT
	,PC_DSG
	,PC_MAX_RSP
	,DET_FT_CBT
	,Vl_Min_Custeio
	,Vl_Max_Custeio
	,Produto_id
	,PC_SBS_CRITICO
	,PC_SBS_PRONAMP
	,PC_SBS_ORGANICO
	,exibir_calculo_simulador
 FROM als_produto_db.dbo.PRDD_SGRV_PRD_AGRL_HIST_TB WITH (NOLOCK)
    WHERE CD_PRD = 960 AND CD_MDLD = 2 AND CD_ITEM_MDLD = 202
    AND dt_fim_prdd_agrl IS NULL
    
 
  UPDATE a
  SET cd_prd = 960
     ,CD_MDLD = 5
     ,CD_ITEM_MDLD = 102
  FROM seguros_db.dbo.produto_tb a with (nolock)
 WHERE produto_id = 1152
 
 
 /*
 
(0 linha(s) afetadas)

(0 linha(s) afetadas)

(0 linha(s) afetadas)

(0 linha(s) afetadas)

(0 linha(s) afetadas)

(0 linha(s) afetadas)

(0 linha(s) afetadas)

(1 linha(s) afetadas)

(1 linha(s) afetadas)

(0 linha(s) afetadas)

(1 linha(s) afetadas)

(1 linha(s) afetadas)

(1 linha(s) afetadas)

(0 linha(s) afetadas)

(0 linha(s) afetadas)

(15 linha(s) afetadas)

(16 linha(s) afetadas)

(0 linha(s) afetadas)

(0 linha(s) afetadas)

(0 linha(s) afetadas)

(0 linha(s) afetadas)

(0 linha(s) afetadas)

(0 linha(s) afetadas)

(0 linha(s) afetadas)

(0 linha(s) afetadas)

(0 linha(s) afetadas)

(0 linha(s) afetadas)

(0 linha(s) afetadas)

(0 linha(s) afetadas)

(0 linha(s) afetadas)

(0 linha(s) afetadas)

(0 linha(s) afetadas)

(0 linha(s) afetadas)

(0 linha(s) afetadas)

(0 linha(s) afetadas)

(0 linha(s) afetadas)

(0 linha(s) afetadas)

(0 linha(s) afetadas)

(0 linha(s) afetadas)

(0 linha(s) afetadas)

(0 linha(s) afetadas)

(0 linha(s) afetadas)

(1 linha(s) afetadas)

(2 linha(s) afetadas)

(1 linha(s) afetadas)

(0 linha(s) afetadas)

(0 linha(s) afetadas)

(0 linha(s) afetadas)

(0 linha(s) afetadas)

(1 linha(s) afetadas)

(1 linha(s) afetadas)

(0 linha(s) afetadas)

(0 linha(s) afetadas)

(0 linha(s) afetadas)

(0 linha(s) afetadas)

(0 linha(s) afetadas)

(0 linha(s) afetadas)

(0 linha(s) afetadas)

(0 linha(s) afetadas)

(0 linha(s) afetadas)

(0 linha(s) afetadas)

(0 linha(s) afetadas)

(0 linha(s) afetadas)

(0 linha(s) afetadas)

(0 linha(s) afetadas)

(0 linha(s) afetadas)

(0 linha(s) afetadas)

(0 linha(s) afetadas)

(0 linha(s) afetadas)

(0 linha(s) afetadas)

(0 linha(s) afetadas)

(0 linha(s) afetadas)

(0 linha(s) afetadas)

(0 linha(s) afetadas)

(0 linha(s) afetadas)

(0 linha(s) afetadas)

(0 linha(s) afetadas)

(0 linha(s) afetadas)

(0 linha(s) afetadas)

(0 linha(s) afetadas)

(0 linha(s) afetadas)

(0 linha(s) afetadas)

(0 linha(s) afetadas)

(0 linha(s) afetadas)

(0 linha(s) afetadas)

(0 linha(s) afetadas)

(0 linha(s) afetadas)

(0 linha(s) afetadas)

(0 linha(s) afetadas)

(0 linha(s) afetadas)

(0 linha(s) afetadas)

(0 linha(s) afetadas)

(0 linha(s) afetadas)

(0 linha(s) afetadas)

(0 linha(s) afetadas)

(0 linha(s) afetadas)

(0 linha(s) afetadas)

(0 linha(s) afetadas)

(0 linha(s) afetadas)

(0 linha(s) afetadas)

(1 linha(s) afetadas)

(1 linha(s) afetadas)

(0 linha(s) afetadas)

(0 linha(s) afetadas)

(0 linha(s) afetadas)

(0 linha(s) afetadas)

(0 linha(s) afetadas)

(0 linha(s) afetadas)

(0 linha(s) afetadas)

(0 linha(s) afetadas)

(0 linha(s) afetadas)

(0 linha(s) afetadas)

(0 linha(s) afetadas)

(0 linha(s) afetadas)

(10 linha(s) afetadas)

(10 linha(s) afetadas)

(16 linha(s) afetadas)

(16 linha(s) afetadas)

(0 linha(s) afetadas)

(0 linha(s) afetadas)

(0 linha(s) afetadas)

(0 linha(s) afetadas)

(0 linha(s) afetadas)

(0 linha(s) afetadas)

(1 linha(s) afetadas)

(0 linha(s) afetadas)

(2 linha(s) afetadas)

(2 linha(s) afetadas)

(2 linha(s) afetadas)

(2 linha(s) afetadas)

(0 linha(s) afetadas)

(0 linha(s) afetadas)

(19 linha(s) afetadas)

(19 linha(s) afetadas)

(0 linha(s) afetadas)

(0 linha(s) afetadas)

(6 linha(s) afetadas)

(6 linha(s) afetadas)

(1 linha(s) afetadas)

(1 linha(s) afetadas)

(1 linha(s) afetadas)

(1 linha(s) afetadas)

(0 linha(s) afetadas)

(0 linha(s) afetadas)

(5 linha(s) afetadas)

(5 linha(s) afetadas)

(1 linha(s) afetadas)

(1 linha(s) afetadas)

(0 linha(s) afetadas)

(1 linha(s) afetadas)

(1 linha(s) afetadas)

(1 linha(s) afetadas)

(1 linha(s) afetadas)

(1 linha(s) afetadas)

(1 linha(s) afetadas)

(1 linha(s) afetadas)

(1 linha(s) afetadas)

(1 linha(s) afetadas)

(1 linha(s) afetadas)

(1 linha(s) afetadas)

(1 linha(s) afetadas)

(1 linha(s) afetadas)

(1 linha(s) afetadas)

(1 linha(s) afetadas)

(1 linha(s) afetadas)

(1 linha(s) afetadas)

(1 linha(s) afetadas)

(1 linha(s) afetadas)

(1 linha(s) afetadas)

(1 linha(s) afetadas)

(1 linha(s) afetadas)

(1 linha(s) afetadas)

(1 linha(s) afetadas)

(1 linha(s) afetadas)

(1 linha(s) afetadas)

(1 linha(s) afetadas)

(1 linha(s) afetadas)

(1 linha(s) afetadas)

(1 linha(s) afetadas)

(1 linha(s) afetadas)

(1 linha(s) afetadas)

(1 linha(s) afetadas)

(1 linha(s) afetadas)

(1 linha(s) afetadas)

(1 linha(s) afetadas)

(1 linha(s) afetadas)

(1 linha(s) afetadas)

(1 linha(s) afetadas)

(1 linha(s) afetadas)

(1 linha(s) afetadas)

(1 linha(s) afetadas)

(1 linha(s) afetadas)

(1 linha(s) afetadas)

(1 linha(s) afetadas)

(1 linha(s) afetadas)

(1 linha(s) afetadas)

(1 linha(s) afetadas)

(1 linha(s) afetadas)

(1 linha(s) afetadas)

(1 linha(s) afetadas)

(1 linha(s) afetadas)

(1 linha(s) afetadas)

(1 linha(s) afetadas)

(1 linha(s) afetadas)

(1 linha(s) afetadas)

(1 linha(s) afetadas)

(1 linha(s) afetadas)

(1 linha(s) afetadas)

(1 linha(s) afetadas)

(1 linha(s) afetadas)

(1 linha(s) afetadas)

(1 linha(s) afetadas)

(1 linha(s) afetadas)

(1 linha(s) afetadas)

(1 linha(s) afetadas)

(1 linha(s) afetadas)

(1 linha(s) afetadas)

(1 linha(s) afetadas)

(1 linha(s) afetadas)

(1 linha(s) afetadas)

(1 linha(s) afetadas)

(1 linha(s) afetadas)

(1 linha(s) afetadas)

(1 linha(s) afetadas)

(1 linha(s) afetadas)

(1 linha(s) afetadas)

(1 linha(s) afetadas)

(1 linha(s) afetadas)

(1 linha(s) afetadas)

(1 linha(s) afetadas)

(1 linha(s) afetadas)

(1 linha(s) afetadas)

(1 linha(s) afetadas)

(1 linha(s) afetadas)

(1 linha(s) afetadas)

(1 linha(s) afetadas)

(1 linha(s) afetadas)

(1 linha(s) afetadas)

(1 linha(s) afetadas)

(1 linha(s) afetadas)

(1 linha(s) afetadas)

(1 linha(s) afetadas)

(1 linha(s) afetadas)

(1 linha(s) afetadas)

(1 linha(s) afetadas)

(1 linha(s) afetadas)

(1 linha(s) afetadas)

(1 linha(s) afetadas)

(1 linha(s) afetadas)

(1 linha(s) afetadas)

(1 linha(s) afetadas)

(1 linha(s) afetadas)

(1 linha(s) afetadas)

(1 linha(s) afetadas)

(1 linha(s) afetadas)

(1 linha(s) afetadas)

(1 linha(s) afetadas)

(1 linha(s) afetadas)

(1 linha(s) afetadas)

(1 linha(s) afetadas)

(1 linha(s) afetadas)

(1 linha(s) afetadas)

(1 linha(s) afetadas)

(1 linha(s) afetadas)

(1 linha(s) afetadas)

(1 linha(s) afetadas)

(1 linha(s) afetadas)

(1 linha(s) afetadas)

(1 linha(s) afetadas)

(1 linha(s) afetadas)

(1 linha(s) afetadas)

(1 linha(s) afetadas)

(1 linha(s) afetadas)

(1 linha(s) afetadas)

(1 linha(s) afetadas)

(1 linha(s) afetadas)

(1 linha(s) afetadas)

(1 linha(s) afetadas)

(1 linha(s) afetadas)

(13536 linha(s) afetadas)

(4985 linha(s) afetadas)

(1 linha(s) afetadas)
*/