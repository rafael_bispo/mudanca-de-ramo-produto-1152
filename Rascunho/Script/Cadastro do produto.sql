--rollback
begin tran
select @@TRANCOUNT


---- use als_produto_criacao_db        
---- go    
    
-- PARA TESTE       
DECLARE        
    @cd_prd   SMALLINT        
  , @cd_mdld  SMALLINT        
  , @cd_item_mdld INT        
  , @nr_versao  INT        
  , @usuario  VARCHAR(14) 
 
  
---rodrigo.moreira gerar vers�o
  Declare
    @Acd_mdld_bb VARCHAR(30)
  , @Ncd_prd   VARCHAR(30)  
  , @cd_prd_bb VARCHAR(30)      
  , @Ncd_mdld  VARCHAR(30)        
  , @Ncd_mdld_BB VARCHAR(30) 
  , @Ncd_item_mdld VARCHAR(30)        
  , @Nnr_versao  VARCHAR(30)
  
  DECLARE @dt_op VARCHAR(30) = (SELECT CONVERT(VARCHAR(30),dt_operacional,121) FROM seguros_db.dbo.parametro_geral_tb with (nolock))
  
  DECLARE 
    @versao_temP INT --atribuir direto tava dando erro  
  , @filtro  VARCHAR(300)  
  , @dt_ini_com VARCHAR(30) = @dt_op
  , @dt_hml VARCHAR(30) = CONVERT(VARCHAR(30),CONVERT(SMALLDATETIME,@dt_op)-10,121 )
  --SELECT @dt_hml, @dt_op
  
  , @tipo_wf_id VARCHAR(30) = '202'
  
  SELECT        
   @cd_prd  = 1152,        
   @cd_mdld = 2,        
   @cd_item_mdld = 202        
  ,@usuario     ='14271121851' 
  ,@Acd_mdld_bb = 2
  ,@cd_prd_bb = 960

  SET @versao_temP = 
  ( select CONVERT(INT,nr_versao) from als_produto_CRIACAO_db.DBO.ITEM_MDLD_PRD_HIST_TB WITH (NOLOCK) WHERE cd_prd = @cd_prd AND CD_ITEM_MDLD = @cd_item_mdld AND CD_MDLD_bb = @cd_mdld
                AND isnull(DT_FIM_VIGENCIA, getdate()+1) > GETDATE()       
                ) 
   SET @nr_versao = @versao_temP 
    
SELECT 
    @Ncd_prd  = @cd_prd       
  , @Ncd_mdld = 1 --novo ramo
  , @Ncd_mdld_BB = 5 --no cd_mdld          
  , @Ncd_item_mdld = 102 --novo subramo         
  , @Nnr_versao = @nr_versao + 1
  
--GRANT SELECT,UPDATE,INSERT,DELETE ON als_produto_criacao_db.dbo.RSCO_CBT_CLS_RSCO_HIST_TB TO usegbr;  
  
--GRANT SELECT,UPDATE,INSERT,DELETE ON als_produto_criacao_db.dbo.RSCO_CBT_CLS_RSCO TO usegbr;  
    
--Vari�veis   
DECLARE        
 @debug BIT,         
 @Origem NVARCHAR(4000), @Destino NVARCHAR(4000),        
 @WHERE NVARCHAR(4000),        
 @WHERECD NVARCHAR(4000), @WHERECDV NVARCHAR(4000),        
 @WHERECBT NVARCHAR(4000), @WHERECBTV NVARCHAR(4000),        
 @WHERECJT NVARCHAR(4000), @WHERECJTV NVARCHAR(4000),         
 @WHERECLSL NVARCHAR(4000), @WHERECLSLV NVARCHAR(4000),         
 @IdTabela INT, @Colunas NVARCHAR(4000), @pks NVARCHAR(4000),        
 @errMsg VARCHAR(4000),         
 @Ordem INT, @Banco NVARCHAR(4000), @Tabela NVARCHAR(4000), @SiglaWHERE AS NVARCHAR(4000),        
 @varD NVARCHAR(4000), @camD NVARCHAR(4000), @WHERED NVARCHAR(4000), @setD NVARCHAR(4000), @onD NVARCHAR(4000)        
         
DECLARE @sql AS NVARCHAR(4000)         
DECLARE @parametros AS NVARCHAR(4000)         
         
DECLARE @CODERRO INT;              
DECLARE @ERRORMESSAGE NVARCHAR(4000);              
DECLARE @ERRORPROCEDURE NVARCHAR(4000);              
DECLARE @ERRORSEVERITY INT;              
DECLARE @ERRORSTATE INT;     
     

--Defini��o de Vari�veis        
 SET @debug = 0       
         
 IF @debug = 1     
 BEGIN     
  PRINT 'Retorno da base de SELECT @cd_prd=' + CAST(@cd_prd AS VARCHAR(4000)) + ', @cd_mdld=' + CAST(@cd_mdld AS VARCHAR(4000))     
         + ', @cd_item_mdld=' + CAST(@cd_item_mdld AS VARCHAR(4000)) + ', @nr_versao=' + CAST(@nr_versao AS VARCHAR(4000))    
         + ', @usuario=''' + @usuario + '''' PRINT ''     
 END        
         
 /* Definindo Or�gem e Destino */        
 IF @debug = 1 PRINT 'Definindo or�gem e Destino | O Destino sempre � [VBR008002-016\QUALID]'        
 IF @@SERVERNAME = 'VBR008002-016\QUALID'        
  SET @Origem = 'LS_SUS' --'SISAB051\DESENV'        
 ELSE        
  SET @Origem = @@SERVERNAME        
     
 SET @Destino = 'VBR008002-016\QUALID'        
     
     
 IF @debug = 1 BEGIN PRINT 'Definindo or�gem e Destino | Servidor Atual: ' + @@servername + '; Or�gem: ' + @Origem + '; Destino: ' + @Destino + '' PRINT '' END        
         
 /* tabelas Tempor�riAS */        
 CREATE TABLE #t2 (r VARCHAR(4000))        
         
         
 /* Validar Usu�rio */        
 IF ISNUMERIC(@usuario) = 0        
 BEGIN        
  IF @debug = 1 PRINT 'Validar Usu�rio | Localizar CPF'        
      
  SELECT @usuario = cpf        
   FROM segab_db.dbo.usuario_tb  WITH(NOLOCK)        
  WHERE login_rede = @usuario        
    AND situacao = 'A'        
      
  IF ISNUMERIC(@usuario) = 0        
  BEGIN        
   SELECT @usuario = cpf        
    FROM segab_db.dbo.usuario_tb  WITH(NOLOCK)        
   WHERE login_web = @usuario        
     AND situacao = 'A'        
  END  
        
      
  IF @usuario is not null AND @debug = 1     
  BEGIN     
   PRINT 'Validar Usu�rio | ' + @usuario PRINT ''     
  END        
 END        
         
 IF ISNUMERIC(@usuario) = 0        
 BEGIN        
  SET @errMsg = 'Validar Usu�rio'        
  RAISERROR (@errMsg, 16, 1)       
 END        
 
 
 /* Realizando a copia dos dados b�sicos */        
 IF @debug = 1 PRINT 'Copiando dados basicos'        
  
 --Rodrigo.Moreira
 --EXEC als_produto_criacao_db.dbo.PRDS02147_SPI  @cd_prd, @cd_mdld, @cd_item_mdld, @usuario 
  --DROP TABLE  #prds02147_spi
  CREATE TABLE #prds02147_spi(id INT IDENTITY(1,1), csql varchar(max))
  INSERT INTO #prds02147_spi (csql)  
  EXEC ALS_PRODUTO_CRIACAO_DB.DBO.SP_HELPTEXT 'PRDS02147_SPI'
  
  DECLARE @corte INT
  SELECT TOP 1 @corte = id
  FROM #prds02147_spi
  WHERE csql LIKE 'DECLARE%'
  ORDER BY id asc
  
  DELETE a
  FROM #prds02147_spi a
  WHERE id < @corte
  
  
 -- DECLARE @Nsql VARCHAR(MAX) = ''
 -- DECLARE @Nsqltemp VARCHAR(MAX)
 -- DEClARE @cont INT = 0
  
  
 -- DECLARE nC CURSOR LOCAL FOR        
 -- SELECT csql       
 --  FROM #prds02147_spi        
 -- ORDER BY id ASC       

 -- OPEN nC        
 -- FETCH NEXT FROM nC INTO @Nsqltemp 
          
 -- WHILE @@FETCH_STATUS = 0        
 -- BEGIN  
 --  SET @Nsql =  @Nsql + @Nsqltemp 
 --FETCH NEXT FROM nC INTO @Nsqltemp
 --END        
 -- CLOSE nC        
 -- DEALLOCATE nC   
  
 -- select @Nsql
  
  --select REPLACE(REPLACE(REPLACE(REPLACE(csql,'@cd_mdld',@Ncd_mdld),'@cd_item_mdld',@Ncd_item_mdld),'@usuario',''''+ @usuario+ ''''),'[SISAB051\EXTRATOR]','[LS_SUS]')    
  --from #prds02147_spi
  
 /* Definindo WHERE */        
 IF @debug = 1 PRINT 'Definindo WHERE | Diversos'        
         
 --CD (cd_prd, cd_mdld, cd_item_mdld)        
 SET @WHERECD = 'cd_prd = ' + CAST(@cd_prd AS VARCHAR(4000)) + ' AND cd_mdld = ' + CAST(@cd_mdld AS VARCHAR(4000)) + ' AND cd_item_mdld = ' + CAST(@cd_item_mdld AS VARCHAR(4000)) + ''        
         
 --CDV (CD + nr_versao)        
 SET @WHERECDV = @WHERECD + ' AND nr_versao = ' + CAST(@nr_versao AS VARCHAR(4000)) + ''        
         
 --CBT (cd_cbt)        
 SET @WHERE = @WHERECD        
     
 SET @sql = '        
  SELECT DISTINCT CAST(cd_cbt AS VARCHAR(4000))        
   FROM [' + @Origem + '].[als_produto_criacao_db].dbo.[cjt_cbt_item_mdld_hist_tb]  WITH(NOLOCK)        
  WHERE (' + @WHERE + ')        
  '        
 SET @parametros = '@Origem NVARCHAR(100), @WHERE NVARCHAR(1000)'    
 --truncate TABLE #t2        
 SET NOCOUNT ON     
 INSERT INTO #t2     
 EXECUTE sp_executeSql @sql, @parametros, @Origem = @Origem, @WHERE = @WHERE     
 SET NOCOUNT OFF        
 SET @WHERECBT = NULL        
 SELECT @WHERECBT = COALESCE(@WHERECBT + ',' + r, r) FROM #t2        
 SET @WHERECBT = 'cd_cbt in (' + @WHERECBT + ')'        
         
 --CBTV (cd_cbt, nr_versao)        
 SET @WHERECBTV = @WHERECBT + ' AND nr_versao = ' + CAST(@nr_versao AS VARCHAR(4000)) + ''        
         
 --CJT (CD + CBT)        
 SET @WHERECJT = @WHERECD -- + ' AND ' + @WHERECBT        
         
 --CJTV (CDV + CBT)        
 SET @WHERECJTV = @WHERECDV + ' AND ' + @WHERECBT        
         
 --CLSL (CD_CLSL)        
 SET @sql = '        
  SELECT DISTINCT CAST(cd_clsl AS VARCHAR(4000))        
   FROM [' + @Origem + '].[als_produto_criacao_db].dbo.[clsl_item_mdld]  WITH(NOLOCK)        
  WHERE (' + @WHERECD + ')        
  UNION        
  SELECT DISTINCT CAST(cd_clsl AS VARCHAR(4000))        
   FROM [' + @Origem + '].[als_produto_criacao_db].dbo.[clsl_item_mdld_cbt]  WITH(NOLOCK)        
  WHERE (' + @WHERECD + ')        
  '        
 SET @parametros = '@Origem NVARCHAR(100), @WHERECD NVARCHAR(1000)'      
 --truncate TABLE #t2        
 SET NOCOUNT ON     
 INSERT INTO #t2     
 EXECUTE sp_executeSql @sql, @parametros, @Origem = @Origem, @WHERECD = @WHERECD     
 SET NOCOUNT OFF      
     
 SET @WHERECLSL = null        
 SELECT @WHERECLSL = COALESCE(@WHERECLSL + ',' + r, r) FROM #t2        
 SET @WHERECLSL = 'cd_clsl in (' + @WHERECLSL + ')'        
         
 --CLSLV (CD_CLSL + nr_versao)        
 SET @WHERECLSLV = @WHERECLSL +  ' AND nr_versao = ' + CAST(@nr_versao AS VARCHAR(4000)) + ''        
         
 IF @debug = 1 BEGIN PRINT 'Definindo WHERE | Ok' PRINT '' END        
         
 /* tabelas a sincronizar (sentido �nico) */        
 --truncate TABLE #t1        
 SET NOCOUNT ON        
 /*OBS.:        
 tabelas com Ordem 00 pASsam obrigatoriamente pelo processo de Insert e Update, pois compartilham dados entre produtos,        
 DIFerentes dAS demais tabelas que pASsam pelo processo de Delete e Insert diretos.        
  */        

 --Rodrigo.Moreira - Filtrar por vers�o
 -- PARAMETRIZADO      
 SELECT Banco, Tabela, Sigla_WHERE, Ordem, CONVERT(VARCHAR(300),NULL) filtro       
  INTO #t1       
  FROM als_produto_criacao_db.dbo.parametrizacao_copia_produto_tb       
 WHERE local = 'PRDS1800_SPI'       
 ORDER BY Ordem      
       
  SELECT  
      a.name tabela, b.name coluna, CONVERT(VARCHAR(300),NULL) filtro  
  INTO #filtro
  FROM als_produto_criacao_db.dbo.SYSOBJECTS a
               INNER JOIN  als_produto_criacao_db.dbo.syscolumns b
                    ON a.id = b.id
               LEFT JOIN  #t1 x
                  ON x.tabela = a.name     
                  AND B.NAME LIKE 'DT_FIM%' 
               LEFT JOIN #t1 y 
			      ON y.tabela = a.name     
                  AND b.NAME IN ('NR_VRS','NR_VERSAO')   
    WHERE x.banco IS NOT NULL OR y.banco is NOT NULL
  
    DELETE a
     FROM #filtro a
      INNER JOIN #filtro b
        ON a.tabela = b.tabela
        AND a.coluna <> b.coluna
        AND b.coluna LIKE 'NR_V%'
        AND a.coluna LIKE 'DT_FIM%'
        
  
 UPDATE a
 SET filtro =  CASE WHEN coluna like 'N%' THEN ' AND nr_versao = ' + CAST(@nr_versao AS VARCHAR(4000))  
               ELSE ' AND ' +   'ISNULL(' + a.coluna + ',GETDATE()+1) > GETDATE()' END     
 FROM #filtro a


 UPDATE a
 SET filtro =  b.filtro   
 FROM #filtro b
   INNER JOIN #t1  a
     ON a.tabela = b.tabela
 
 SET NOCOUNT OFF        
         
 /* Transa��o */        
 IF @debug = 1 PRINT 'Transa��o | BEGIN Transaction'        
 SET XACT_ABORT ON        
     
 --BEGIN TRANSACTION  
   DECLARE @sqldel VARCHAR(MAX) = ' '      
     
  IF @debug = 1 BEGIN PRINT 'Transa��o | ok' PRINT '' END        
         
  /* Sincroniza��o Unidirecional de Registros */        
         
  IF @debug = 1 PRINT 'Sincroniza��o Unidirecional de Registros | Inicio'        
         
  /* Delete */        
  DECLARE t1 CURSOR LOCAL FOR        
  SELECT Banco, Tabela, Sigla_WHERE, filtro        
   FROM #t1        
  WHERE Ordem > 0        
  ORDER BY Ordem ASC        
  OPEN t1        
  FETCH NEXT FROM t1 INTO @Banco, @Tabela, @SiglaWHERE
  --rodrigo.moreira, 
  ,@filtro
  
          
  WHILE @@FETCH_STATUS = 0        
  BEGIN        
   IF @debug = 1 PRINT 'DELETE FROM | ' + @Tabela        
       
   -- WHERE        
   IF @SiglaWHERE = 'CD' SET @WHERE = @WHERECD        
   ELSE IF @SiglaWHERE = 'CDV' SET @WHERE = @WHERECDV        
   ELSE IF @SiglaWHERE = 'CBT' SET @WHERE = @WHERECBT        
   ELSE IF @SiglaWHERE = 'CBTV' SET @WHERE = @WHERECBTV        
   ELSE IF @SiglaWHERE = 'CJT' SET @WHERE = @WHERECJT        
   ELSE IF @SiglaWHERE = 'CJTV' SET @WHERE = @WHERECJTV        
   ELSE IF @SiglaWHERE = 'CLSL' SET @WHERE = @WHERECLSL        
   ELSE IF @SiglaWHERE = 'CLSLV' SET @WHERE = @WHERECLSLV        
    
   
   IF @filtro IS NOT NULL    
   SET @WHERE = @WHERE + @filtro   
    
   --Rodrigo.Moreira - N�o deletar versao atual - Pois vai gerar uma vers�o nova
   SET @sql = '        
    DELETE FROM        
     [' + @Destino + '].[' + @Banco + '].DBO.[' + @Tabela + ']        
    WHERE (' + @WHERE + ' AND 2 = 1 )'        
            
   SET @parametros = '@Destino NVARCHAR(100), @Banco NVARCHAR(100), @Tabela NVARCHAR(100), @WHERE NVARCHAR(1000)'    
       
   IF @debug = 1 SET @errMsg = 'DELETE FROM | ' + @Tabela + ' | ' + @sql    
        
       SET @sqldel = @sqldel + @sql
   --print (@sql)    
        
   --EXECUTE sp_executeSql @sql, @parametros, @Destino = @Destino, @Banco = @Banco, @Tabela = @Tabela, @WHERE = @WHERE     
          
   FETCH NEXT FROM t1 INTO @Banco, @Tabela, @SiglaWHERE
  --rodrigo.moreira, 
  ,@filtro      
  END        
  CLOSE t1        
  DEALLOCATE t1        
    
  /* INSERT */   
  DECLARE @sqlins VARCHAR(MAX) = ''        
  DECLARE @ncoluna VARCHAR(MAX) = ''
  DECLARE @Wtemp VARCHAR(50) = ''
  
  
  DECLARE t1 CURSOR LOCAL FOR 
  SELECT Banco, Tabela, Sigla_WHERE
  --RODRIGO.MOREIRA
  , filtro        
   FROM #t1        
  WHERE ordem > 0        
  ORDER BY ordem DESC        
  OPEN t1        
  FETCH NEXT FROM t1 INTO @Banco, @Tabela, @SiglaWHERE
  --rodrigo.moreira, 
  ,@filtro        
  WHILE @@FETCH_STATUS = 0        
  BEGIN        
   IF @debug = 1 PRINT 'Insert INTo | ' + @Tabela        
       
   -- WHERE        
   IF @SiglaWHERE = 'CD' SET @WHERE = @WHERECD        
   ELSE IF @SiglaWHERE = 'CDV' SET @WHERE = @WHERECDV        
   ELSE IF @SiglaWHERE = 'CBT' SET @WHERE = @WHERECBT        
   ELSE IF @SiglaWHERE = 'CBTV' SET @WHERE = @WHERECBTV        
   ELSE IF @SiglaWHERE = 'CJT' SET @WHERE = @WHERECJT        
   ELSE IF @SiglaWHERE = 'CJTV' SET @WHERE = @WHERECJTV        
   ELSE IF @SiglaWHERE = 'CLSL' SET @WHERE = @WHERECLSL        
   ELSE IF @SiglaWHERE = 'CLSLV' SET @WHERE = @WHERECLSLV        
    
  --Rodrigo.Moreira
   IF @filtro IS NOT NULL    
   SET @WHERE = @WHERE + @filtro
    
   -- Valida as colunas que existem no ambiente de origem para posteriormente validar com qualidade    
   CREATE TABLE #tabela_controle_colunas    
   (    
    id_origem BIGINT    
     , colunas_origem VARCHAR(4000)    
     , id_destino BIGINT    
   )    
       
   SET @sql = '    
    SELECT id FROM [' + @Origem + '].als_produto_criacao_db.dbo.SYSOBJECTS WITH(NOLOCK) WHERE name = '''+ @Tabela + '''    
    '    
   SET @parametros = '@Origem NVARCHAR(100), @Tabela NVARCHAR(100)'    
       
   INSERT INTO #tabela_controle_colunas (id_origem)    
   EXECUTE sp_executeSql @sql, @parametros, @Origem = @Origem, @Tabela = @Tabela     
        
        
   SET @sql = '    
    SELECT name     
     FROM [' + @Origem + '].als_produto_criacao_db.dbo.SYSCOLUMNS WITH(NOLOCK)     
    WHERE id = (SELECT id_origem FROM #tabela_controle_colunas WHERE id_origem is not null)    
      AND xtype <> 189    
    '    
   
    
   SET @parametros = '@Origem NVARCHAR(100)'    
         
   INSERT INTO #tabela_controle_colunas (colunas_origem)    
   EXECUTE sp_executeSql @sql, @parametros, @Origem = @Origem     
    
   SET @sql = '    
    SELECT id FROM [' + @Destino + '].als_produto_criacao_db.dbo.SYSOBJECTS WITH(NOLOCK) WHERE name = '''+ @Tabela + '''    
    '    
   SET @parametros = '@Destino NVARCHAR(100), @Tabela NVARCHAR(100)'    
   INSERT INTO #tabela_controle_colunas (id_destino)    
   EXECUTE sp_executeSql @sql, @parametros, @Destino = @Destino, @Tabela = @Tabela    
       
           
           
   -- Colunas (apenas colunas que existem em qualidade e no ambiente de origem)       
   SET @Colunas = NULL        
   SELECT @Colunas = COALESCE(@Colunas+ ', [' + name + ']', '[' + name + ']')        
    FROM als_produto_criacao_db.dbo.SYSCOLUMNS  WITH(NOLOCK)        
   WHERE id = (SELECT id_destino FROM #tabela_controle_colunas WHERE id_destino is not null)    
     AND xtype <> 189      
     AND name IN (SELECT colunas_origem FROM #tabela_controle_colunas WHERE colunas_origem is not null)     
      
   SET @sql = '        
    INSERT INTO [' + @Destino + '].[' + @Banco + '].DBO.[' + @Tabela + ']        
       (' + @Colunas + ')        
    '        
   SET @Colunas = REPLACE(@Colunas, '[DT_ALTERACAO]', '''' + CONVERT(VARCHAR(10), GETDATE(), 112) + '''')     
   SET @Colunas = REPLACE(@Colunas, '[USUARIO]', '''' + @usuario + '''')  
   
   --PRINT(@colunas)
 
    --Rodrigo.Moreira
    --n�o inserir tabela que n�o tem cd_mdld, cd_item_mdld, cd_prd
    SET @ncoluna = @Colunas
    SET @ncoluna = REPLACE(REPLACE(REPLACE(@ncoluna,'CD_PRD',''),'CD_MDLD',''),'CD_ITEM_MDLD','')
    IF LEN(@ncoluna) = LEN(@Colunas)
    SET @Wtemp = ' AND 2 = 1' 
   
   --rodrigo.moreira
   SET @Colunas = REPLACE(@Colunas, '[CD_PRD]', '''' + @Ncd_prd + '''') 
   SET @Colunas = REPLACE(@Colunas, '[CD_MDLD]', '''' + @Ncd_mdld + '''') 
   SET @Colunas = REPLACE(@Colunas, '[CD_MDLD_BB]', '''' + @Ncd_mdld_BB + '''') 
   SET @Colunas = REPLACE(@Colunas, '[CD_ITEM_MDLD]', '''' + @Ncd_item_mdld + '''') 
   SET @Colunas = REPLACE(@Colunas, '[NR_VERSAO]', '''' + @Nnr_versao + '''') 
   SET @Colunas = REPLACE(@Colunas, '[DT_INICIO_COMERCIALIZACAO]', '''' + @dt_ini_com + '''') 
   SET @Colunas = REPLACE(@Colunas, '[DT_HOMOLOGACAO]', '''' + @dt_hml + '''') 
   
   
    SET @sql = @sql + '        
    SELECT ' + @Colunas + '        
     FROM [' + @Origem + '].[' + @Banco + '].DBO.[' + @Tabela + ']  WITH(NOLOCK)        
    WHERE (' + @WHERE +  @WTEMP + ')         
    '            
    
   SET @wtemp = ''
   SET @parametros = '@Destino NVARCHAR(100), @Banco NVARCHAR(100), @Tabela NVARCHAR(100), @Colunas NVARCHAR(4000), @Origem NVARCHAR(100), @WHERE NVARCHAR(1000)'        
      
   IF @debug = 1 SET @errMsg = 'INSERT INTO | ' + @Tabela + ' | ' + @sql        
       
   --print (@sql) 
   SET @sqlins = @sqlins + @sql   
       
   --EXECUTE sp_executeSql @sql, @parametros, @Destino = @Destino, @Banco = @Banco, @Tabela = @Tabela, @Colunas = @Colunas, @Origem = @Origem, @WHERE = @WHERE    
     --   select * from  #tabela_controle_colunas
       -- select @sql
               
        DROP TABLE #tabela_controle_colunas    
               
 FETCH NEXT FROM t1 INTO @Banco, @Tabela, @SiglaWHERE
  --rodrigo.moreira, 
  ,@filtro      
  END       
  CLOSE t1        
  DEALLOCATE t1        
        
  /* INSERT E UPDATE */   
  DECLARE @sqlatu VARCHAR(MAX) = ''     
  DECLARE t1 CURSOR LOCAL FOR        
  SELECT Banco, Tabela, Sigla_WHERE        
   FROM #t1        
  WHERE Ordem = 0        
  OPEN t1        
  FETCH NEXT FROM t1 INTO @Banco, @Tabela, @SiglaWHERE        
  WHILE @@FETCH_STATUS = 0        
  BEGIN        
   IF @debug = 1 PRINT 'INSERT E UPDATE Din�mico (sem DELETE) | ' + @Tabela        
    
   -- Id da Tabela        
   SELECT @IdTabela = id FROM als_produto_criacao_db.dbo.SYSOBJECTS  WITH(NOLOCK) WHERE name = @Tabela        
       
   -- Campos Chave        
   SET @pks = NULL        
   SELECT @pks = COALESCE(@pks + ', [' + SYSCOLUMNS.name + ']', '[' + SYSCOLUMNS.name + ']')        
    FROM als_produto_criacao_db.dbo.SYSINDEXKEYS  WITH(NOLOCK)        
    INNER JOIN als_produto_criacao_db.dbo.SYSCOLUMNS  WITH(NOLOCK)    
     ON  SYSINDEXKEYS.id = SYSCOLUMNS.id         
     AND SYSINDEXKEYS.colid = SYSCOLUMNS.colid        
   WHERE SYSINDEXKEYS.id = @IdTabela AND ((@Tabela = 'CBT' and SYSINDEXKEYS.indid = 2) or @Tabela <> 'CBT')       
       
   -- Variaveis Din�micAS        
   SET @varD = NULL        
   SELECT @varD = COALESCE(@varD + ', @' + SYSCOLUMNS.name + CONVERT(VARCHAR(100),SYSCOLUMNS.id) + ' VARCHAR(4000)', '@' + SYSCOLUMNS.name +  CONVERT(VARCHAR(100),SYSCOLUMNS.id) + ' VARCHAR(4000)')        
    FROM als_produto_criacao_db.dbo.SYSINDEXKEYS  WITH(NOLOCK)        
    INNER JOIN als_produto_criacao_db.dbo.SYSCOLUMNS  WITH(NOLOCK)     
     ON  SYSINDEXKEYS.id = SYSCOLUMNS.id         
     AND SYSINDEXKEYS.colid = SYSCOLUMNS.colid        
   WHERE SYSINDEXKEYS.id = @IdTabela AND ((@Tabela = 'CBT' and SYSINDEXKEYS.indid = 2) or @Tabela <> 'CBT')       
       
   -- Campos Din�micos        
   SET @camD = NULL        
   SELECT @camD = COALESCE(@camD + ', ' + SYSCOLUMNS.name + ' VARCHAR(4000)', SYSCOLUMNS.name + ' VARCHAR(4000)')        
    FROM als_produto_criacao_db.dbo.SYSINDEXKEYS  WITH(NOLOCK)        
    INNER JOIN als_produto_criacao_db.dbo.SYSCOLUMNS  WITH(NOLOCK)     
    ON  SYSINDEXKEYS.id = SYSCOLUMNS.id         
    AND SYSINDEXKEYS.colid = SYSCOLUMNS.colid        
   WHERE SYSINDEXKEYS.id = @IdTabela AND ((@Tabela = 'CBT' and SYSINDEXKEYS.indid = 2) or @Tabela <> 'CBT')          
       
   -- WHERE        
   IF @SiglaWHERE = 'CD' SET @WHERE = @WHERECD        
   ELSE IF @SiglaWHERE = 'CDV' SET @WHERE = @WHERECDV        
   ELSE IF @SiglaWHERE = 'CBT' SET @WHERE = @WHERECBT        
   ELSE IF @SiglaWHERE = 'CBTV' SET @WHERE = @WHERECBTV        
   ELSE IF @SiglaWHERE = 'CJT' SET @WHERE = @WHERECJT        
   ELSE IF @SiglaWHERE = 'CJTV' SET @WHERE = @WHERECJTV        
   ELSE IF @SiglaWHERE = 'CLSL' SET @WHERE = @WHERECLSL        
   ELSE IF @SiglaWHERE = 'CLSLV' SET @WHERE = @WHERECLSLV        
       
   -- WHERE Din�mico        
   SET @WHERED = NULL        
   SELECT @WHERED = COALESCE(@WHERED + ' AND ?' + SYSCOLUMNS.name + ' = @' + SYSCOLUMNS.name + CONVERT(VARCHAR(100),SYSCOLUMNS.id), '?' + SYSCOLUMNS.name + ' = @' + SYSCOLUMNS.name + CONVERT(VARCHAR(100),SYSCOLUMNS.id))        
    FROM als_produto_criacao_db.dbo.SYSINDEXKEYS  WITH(NOLOCK)        
    INNER JOIN als_produto_criacao_db.dbo.SYSCOLUMNS  WITH(NOLOCK)     
     ON  SYSINDEXKEYS.id = SYSCOLUMNS.id         
     AND SYSINDEXKEYS.colid = SYSCOLUMNS.colid        
   WHERE SYSINDEXKEYS.id = @IdTabela AND ((@Tabela = 'CBT' and SYSINDEXKEYS.indid = 2) or @Tabela <> 'CBT')              
            
         -- Valida as colunas que existem no ambiente de origem para posteriormente validar com qualidade    
   CREATE TABLE #tabela_controle_colunas_2    
   (    
    id_origem BIGINT    
     , colunas_origem VARCHAR(4000)    
     , id_destino BIGINT    
   )    
       
   SET @sql = '    
    SELECT id FROM [' + @Origem + '].als_produto_criacao_db.dbo.SYSOBJECTS WITH(NOLOCK) WHERE name = '''+ @Tabela + '''    
    '    
   SET @parametros = '@Origem NVARCHAR(100), @Tabela NVARCHAR(100)'    
       
   INSERT INTO #tabela_controle_colunas_2 (id_origem)    
   EXECUTE sp_executeSql @sql, @parametros, @Origem = @Origem, @Tabela = @Tabela     
        
        
   SET @sql = '    
    SELECT name     
     FROM [' + @Origem + '].als_produto_criacao_db.dbo.SYSCOLUMNS WITH(NOLOCK)     
    WHERE id = (SELECT id_origem FROM #tabela_controle_colunas_2 WHERE id_origem is not null)    
      AND xtype <> 189    
    '    
   SET @parametros = '@Origem NVARCHAR(100)'    
         
   INSERT INTO #tabela_controle_colunas_2 (colunas_origem)    
   EXECUTE sp_executeSql @sql, @parametros, @Origem = @Origem     
           
       
   SET @sql = '    
    SELECT id FROM [' + @Destino + '].als_produto_criacao_db.dbo.SYSOBJECTS WITH(NOLOCK) WHERE name = '''+ @Tabela + '''    
    '    
   SET @parametros = '@Destino NVARCHAR(100), @Tabela NVARCHAR(100)'    
   INSERT INTO #tabela_controle_colunas_2 (id_destino)    
   EXECUTE sp_executeSql @sql, @parametros, @Destino = @Destino, @Tabela = @Tabela    
            
   -- SET Din�mico        
   SET @setD = NULL        
   SELECT @setD = COALESCE(@setD + ', ' + name + ' = b.' + name, name + ' = b.' + name)        
    FROM als_produto_criacao_db.dbo.SYSCOLUMNS  WITH(NOLOCK)        
   WHERE id = (SELECT id_destino FROM #tabela_controle_colunas_2 WHERE id_destino is not null)    
     AND xtype <> 189      
     AND name IN (SELECT colunas_origem FROM #tabela_controle_colunas_2 WHERE colunas_origem is not null)     
         
   -- On Din�mico        
   SET @onD = NULL        
   SELECT @onD = COALESCE(@onD + ' AND a.[' +  SYSCOLUMNS.name + '] = b.[' +  SYSCOLUMNS.name + ']', 'a.[' +  SYSCOLUMNS.name + '] = b.[' +  SYSCOLUMNS.name + ']')        
    FROM als_produto_criacao_db.dbo.SYSINDEXKEYS  WITH(NOLOCK)        
    INNER JOIN als_produto_criacao_db.dbo.SYSCOLUMNS  WITH(NOLOCK)     
     ON  SYSINDEXKEYS.id = SYSCOLUMNS.id         
     AND SYSINDEXKEYS.colid = SYSCOLUMNS.colid        
   WHERE SYSINDEXKEYS.id = @IdTabela         
           
   -- Colunas (apenas colunas que existem em qualidade e no ambiente de origem)       
   SET @Colunas = NULL        
   SELECT @Colunas = COALESCE(@Colunas+ ', [' + name + ']', '[' + name + ']')        
    FROM als_produto_criacao_db.dbo.SYSCOLUMNS  WITH(NOLOCK)        
   WHERE id = (SELECT id_destino FROM #tabela_controle_colunas_2 WHERE id_destino is not null)    
     AND xtype <> 189      
     AND name IN (SELECT colunas_origem FROM #tabela_controle_colunas_2 WHERE colunas_origem is not null)     
       
   SET @setD = REPLACE(@setD, 'b.DT_ALTERACAO', '''' + CONVERT(VARCHAR(10), GETDATE(), 112) + '''')     
   SET @setD = REPLACE(@setD, 'b.USUARIO', '''' + @usuario + '''')     
       
   SET @sql = '        
    CREATE TABLE ' + REPLACE(REPLACE(REPLACE(REPLACE(@varD,'@','#'),'VARCHAR(4000)',''),' ',''),',','') + '  (' + @camD + ')        
    DECLARE ' + @varD + '        
               
    DECLARE ' + REPLACE(REPLACE(REPLACE(REPLACE(@varD,'@',''),'VARCHAR(4000)',''),' ',''),',','') + '  cursor for        
    SELECT ' + @pks + '        
     FROM [' + @Origem + '].[' + @Banco + '].dbo.[' + @Tabela + ']  WITH(NOLOCK)        
    WHERE (' + @WHERE + ')        
    OPEN ' + REPLACE(REPLACE(REPLACE(REPLACE(@varD,'@',''),'VARCHAR(4000)',''),' ',''),',','') + '        
    FETCH NEXT FROM ' + REPLACE(REPLACE(REPLACE(REPLACE(@varD,'@',''),'VARCHAR(4000)',''),' ',''),',','') + ' INTO ' + replace(@varD,' VARCHAR(4000)','') + '        
    WHILE @@FETCH_STATUS = 0        
    BEGIN        
     IF EXISTS(SELECT * FROM [' + @Destino + '].[' + @Banco + '].DBO.[' + @Tabela + ']  WITH(NOLOCK) WHERE (' + replace(@WHERED,'?','') + '))        
     BEGIN        
      UPDATE a       
       SET ' + @setD + '        
       FROM [' + @Destino + '].[' + @Banco + '].DBO.[' + @Tabela + '] AS a  WITH(NOLOCK)        
       INNER JOIN [' + @Origem + '].[' + @Banco + '].DBO.[' + @Tabela + '] AS b  WITH(NOLOCK) ON (' + @onD + ')        
      WHERE (' + REPLACE(@WHERED,'?','b.') + ')        
     END        
     ELSE        
     BEGIN             
      INSERT INTO [' + @Destino + '].[' + @Banco + '].DBO.[' + @Tabela + ']        
         (' + @Colunas+ ')        
     '     
     SET @Colunas = REPLACE(@Colunas, '[DT_ALTERACAO]', '''' + CONVERT(VARCHAR(10), GETDATE(), 112) + '''')     
     SET @Colunas = REPLACE(@Colunas, '[USUARIO]', '''' + @usuario + '''')                
     SET @sql = @sql + '    
      SELECT ' + @Colunas+ '        
       FROM [' + @Origem + '].[' + @Banco + '].DBO.[' + @Tabela + ']  WITH(NOLOCK)        
      WHERE (' + REPLACE(@WHERED,'?','') + ')        
     END        
    FETCH NEXT FROM ' + REPLACE(REPLACE(REPLACE(REPLACE(@varD,'@',''),'VARCHAR(4000)',''),' ',''),',','') + ' INTO ' + REPLACE(@varD,' VARCHAR(4000)','') + '        
    END        
    CLOSE ' + REPLACE(REPLACE(REPLACE(REPLACE(@varD,'@',''),'VARCHAR(4000)',''),' ',''),',','') + '           
    DEALLOCATE ' + REPLACE(REPLACE(REPLACE(REPLACE(@varD,'@',''),'VARCHAR(4000)',''),' ',''),',','') + '        
    DROP TABLE ' + REPLACE(REPLACE(REPLACE(REPLACE(@varD,'@','#'),'VARCHAR(4000)',''),' ',''),',','') + '         
    '        
   SET @parametros = '@camD NVARCHAR(4000), @varD NVARCHAR(4000), @pks NVARCHAR(4000), @Origem NVARCHAR(100), @Banco NVARCHAR(100), @Tabela NVARCHAR(100), @WHERE NVARCHAR(1000), @Destino NVARCHAR(100), @WHERED NVARCHAR(1000), @setD NVARCHAR(4000), @onD NVARCHAR(4000), @Colunas NVARCHAR(4000)'    
       
   IF @debug = 1 SET @errMsg = 'INSERT e UPDATE Din�mico (sem DELETE) | ' + @Tabela + ' | ' + @sql        
       
   --print (@sql)    
    SET @sqlatu = @sqlatu + @sql
       
   --EXECUTE sp_executeSql @sql, @parametros, @camD=@camD, @varD=@varD, @pks=@pks, @Origem=@Origem, @Banco=@Banco, @Tabela=@Tabela, @WHERE=@WHERE, @Destino=@Destino, @WHERED=@WHERED, @setD=@setD, @onD=@onD, @Colunas=@Colunas         
       
   DROP TABLE #tabela_controle_colunas_2    
       
  FETCH NEXT FROM t1 INTO @Banco, @Tabela, @SiglaWHERE        
  END        
  CLOSE t1        
  DEALLOCATE t1        
   
  IF @debug = 1 BEGIN PRINT 'Sincroniza��o Unidirecional de Registros | T�rmino' PRINT '' END        
         
  /* Question�rio */        
  /*        
   @cd_prd smallINT,        
   @cd_mdld smallINT,        
   @cd_item_mdld INT,        
   @nr_versao INT        
  */        
  IF @debug = 1 PRINT 'Question�rio | als_produto_db.dbo.PRDS1801_SPI'        
    
  
 ---**************************************************
 ---**************************
 ----************************************************
 
    Print 'als_produto_db.dbo.PRDS1801_SPI @cd_prd, @cd_mdld, @cd_item_mdld, @nr_versao, @usuario        ' 
    --RODRIGO.MOREIRA - Para o contexto, n�o � necess�rioooo...
    --Abaixo execu��o que a proc faria
    
    /*
    Retorno da base de SELECT @cd_prd=1152, @cd_mdld=2, @cd_item_mdld=202, @nr_versao=1
 
Definindo or�gem e Destino | O Destino sempre � [VBR008002-016\QUALID]
Definindo or�gem e Destino | Servidor Atual: VBR008002-016\QUALID; Or�gem: LS_SUS; Destino: VBR008002-016\QUALID
 
Definindo WHERE | Diversos

(0 linha(s) afetadas)

(15 linha(s) afetadas)

(254 linha(s) afetadas)

(35 linha(s) afetadas)

(9 linha(s) afetadas)

(9 linha(s) afetadas)

(1 linha(s) afetadas)
Definindo WHERE | Ok
 
Sincroniza��o Unidirecional de Registros | Inicio
DELETE FROM | GRUPO_DOMINIO_RESPOSTA_TB
            
   DELETE FROM [VBR008002-016\QUALID].[ALS_PRODUTO_DB].dbo.[GRUPO_DOMINIO_RESPOSTA_TB]            
   WHERE (1=1)            
   
DELETE FROM | DOMINIO_RESPOSTA_TB
            
   DELETE FROM [VBR008002-016\QUALID].[ALS_PRODUTO_DB].dbo.[DOMINIO_RESPOSTA_TB]            
   WHERE (1=1)            
   
DELETE FROM | QUESTIONARIO_TB
            
   DELETE FROM [VBR008002-016\QUALID].[ALS_PRODUTO_DB].dbo.[QUESTIONARIO_TB]            
   WHERE (questionario_id in (497,604,609,610,611,613,619,624,750,784,794,803,857,869,985))            
   
DELETE FROM | PERGUNTA_TB
            
   DELETE FROM [VBR008002-016\QUALID].[ALS_PRODUTO_DB].dbo.[PERGUNTA_TB]            
   WHERE (pergunta_id in (11142,11143,13681,13682,13698,13699,13700,13701,13702,13703,13704,13705,13706,13707,13708,13709,13710,13711,13712,13713,13714,13716,13718,13719,13720,13721,13726,2767,2771,2778,2779,2780,2781,2782,2784,2785,2786,2787,2788,2789,2790,2791,2792,2793,2794,2795,2797,2803,2804,2813,2816,2817,2825,2826,2829,2880,2881,2882,2884,2885,3053,3054,3055,3056,3057,3058,3059,3060,3275,3276,3277,3278,3279,3280,3427,3546,3547,4880,5617,5824,5825,5826,5827,5828,5829,5830,5831,6123,6200,6597,6748,6920,7176,7177,7455,7456,7457,7458,7459,7460,7538,7539,7540,7541,7542,7543,7544,7548,7549,7550,7551,7552,7553,7554,7555,7556,7557,7558,7559,7560,7561,7562,7563,7564,7565,7568,7569,7570,7573,7574,7575,7576,7577,7578,7579,7580,7581,7582,7583,7584,7585,7586,7587,7589,7590,7591,7593,7594,7595,7596,7597,7598,7599,7600,7601,7602,7603,7604,7605,7606,7607,7608,7609,7610,7611,7612,7613,7614,7615,7616,7617,7618,7619,7621,7622,7623,7624,7625,7626,7627,7628,7629,7630,7631,7632,7633,7634,7635,7636,7637,7638,7639,7640,7642,7643,7644,7645,7646,7647,7648,7765,8129,8300,8302,8303,8584,8586,8588,8590,8592,8696,8698,8700,8701,8702,8705,8706,8707,8708,8711,8712,8713,8790,8791,8792,8940,8941,8942,8943,8944,8945,8946,8947,8948,8949,8950,8951,8952,8953,8954,8955,8956,8957,8958,8959,8960,8961,8962,8963,9017,9018,9339,9340,9341))            
   
DELETE FROM | QUESTIONARIO_PERGUNTA_TB
            
   DELETE FROM [VBR008002-016\QUALID].[ALS_PRODUTO_DB].dbo.[QUESTIONARIO_PERGUNTA_TB]            
   WHERE (questionario_id in (497,604,609,610,611,613,619,624,750,784,794,803,857,869,985))            
   
DELETE FROM | TP_ENDOSSO_TB
            
   DELETE FROM [VBR008002-016\QUALID].[ALS_PRODUTO_DB].dbo.[TP_ENDOSSO_TB]            
   WHERE (tp_endosso_id in (100,101,107,2,201,250,253,260,262,265,266,269,270,300,304,305,306,361,362,38,51,53,57,61,63,64,65,66,68,71,80,90,92,93,94))            
   
DELETE FROM | SUBRAMO_TP_ENDOSSO_HIST_TB
            
   DELETE FROM [VBR008002-016\QUALID].[ALS_PRODUTO_DB].dbo.[SUBRAMO_TP_ENDOSSO_HIST_TB]            
   WHERE (tp_endosso_id in (100,101,107,2,201,250,253,260,262,265,266,269,270,300,304,305,306,361,362,38,51,53,57,61,63,64,65,66,68,71,80,90,92,93,94))            
   
DELETE FROM | SUBRAMO_TP_ENDOSSO_TB
            
   DELETE FROM [VBR008002-016\QUALID].[ALS_PRODUTO_DB].dbo.[SUBRAMO_TP_ENDOSSO_TB]            
   WHERE (tp_endosso_id in (100,101,107,2,201,250,253,260,262,265,266,269,270,300,304,305,306,361,362,38,51,53,57,61,63,64,65,66,68,71,80,90,92,93,94))            
   
DELETE FROM | TP_RECUSA_TB
            
   DELETE FROM [VBR008002-016\QUALID].[ALS_PRODUTO_DB].dbo.[TP_RECUSA_TB]            
   WHERE (tp_recusa_id in (1320,1417,1421,1422,1423,1426,1427,1434,1438))            
   
Insert INTO | TP_RECUSA_TB

(1 linha(s) afetadas)

(6 linha(s) afetadas)

(1 linha(s) afetadas)
        
    INSERT INTO [VBR008002-016\QUALID].[ALS_PRODUTO_DB].DBO.[TP_RECUSA_TB]        
       ([tp_recusa_id], [descricao], [situacao], [dt_inclusao], [dt_alteracao], [usuario])        
            
    SELECT [tp_recusa_id], [descricao], [situacao], [dt_inclusao], '20200517', '14271121851'        
     FROM [LS_SUS].[ALS_PRODUTO_DB].DBO.[TP_RECUSA_TB]  WITH(NOLOCK)        
    WHERE (tp_recusa_id in (1320,1417,1421,1422,1423,1426,1427,1434,1438))         
    
Insert INTO | SUBRAMO_TP_ENDOSSO_TB

(1 linha(s) afetadas)

(11 linha(s) afetadas)

(1 linha(s) afetadas)
        
    INSERT INTO [VBR008002-016\QUALID].[ALS_PRODUTO_DB].DBO.[SUBRAMO_TP_ENDOSSO_TB]        
       ([ramo_id], [subramo_id], [dt_inicio_vigencia_sbr], [tp_endosso_id], [dt_inclusao], [dt_alteracao], [usuario], [permite_restituicao], [dt_inicio_vigencia], [dt_fim_vigencia], [situacao])        
            
    SELECT [ramo_id], [subramo_id], [dt_inicio_vigencia_sbr], [tp_endosso_id], [dt_inclusao], '20200517', '14271121851', [permite_restituicao], [dt_inicio_vigencia], [dt_fim_vigencia], [situacao]        
     FROM [LS_SUS].[ALS_PRODUTO_DB].DBO.[SUBRAMO_TP_ENDOSSO_TB]  WITH(NOLOCK)        
    WHERE (tp_endosso_id in (100,101,107,2,201,250,253,260,262,265,266,269,270,300,304,305,306,361,362,38,51,53,57,61,63,64,65,66,68,71,80,90,92,93,94))         
    
Insert INTO | SUBRAMO_TP_ENDOSSO_HIST_TB

(1 linha(s) afetadas)

(14 linha(s) afetadas)

(1 linha(s) afetadas)
        
    INSERT INTO [VBR008002-016\QUALID].[ALS_PRODUTO_DB].DBO.[SUBRAMO_TP_ENDOSSO_HIST_TB]        
       ([CD_PRD], [CD_MDLD], [CD_ITEM_MDLD], [tp_endosso_id], [dt_inicio_vigencia], [nr_versao], [permite_restituicao], [dt_fim_vigencia], [usuario], [dt_inclusao], [dt_alteracao], [ramo_id], [subramo_id], [dt_inicio_vigencia_sbr])        
            
    SELECT [CD_PRD], [CD_MDLD], [CD_ITEM_MDLD], [tp_endosso_id], [dt_inicio_vigencia], [nr_versao], [permite_restituicao], [dt_fim_vigencia], '14271121851', [dt_inclusao], '20200517', [ramo_id], [subramo_id], [dt_inicio_vigencia_sbr]        
     FROM [LS_SUS].[ALS_PRODUTO_DB].DBO.[SUBRAMO_TP_ENDOSSO_HIST_TB]  WITH(NOLOCK)        
    WHERE (tp_endosso_id in (100,101,107,2,201,250,253,260,262,265,266,269,270,300,304,305,306,361,362,38,51,53,57,61,63,64,65,66,68,71,80,90,92,93,94))         
    
Insert INTO | TP_ENDOSSO_TB

(1 linha(s) afetadas)

(9 linha(s) afetadas)

(1 linha(s) afetadas)
        
    INSERT INTO [VBR008002-016\QUALID].[ALS_PRODUTO_DB].DBO.[TP_ENDOSSO_TB]        
       ([tp_endosso_id], [descricao], [tp_endosso_cosseguro], [movimentacao_financeira], [usuario], [dt_inclusao], [dt_alteracao], [situacao], [classe_endosso_id])        
            
    SELECT [tp_endosso_id], [descricao], [tp_endosso_cosseguro], [movimentacao_financeira], '14271121851', [dt_inclusao], '20200517', [situacao], [classe_endosso_id]        
     FROM [LS_SUS].[ALS_PRODUTO_DB].DBO.[TP_ENDOSSO_TB]  WITH(NOLOCK)        
    WHERE (tp_endosso_id in (100,101,107,2,201,250,253,260,262,265,266,269,270,300,304,305,306,361,362,38,51,53,57,61,63,64,65,66,68,71,80,90,92,93,94))         
    
Insert INTO | QUESTIONARIO_PERGUNTA_TB

(1 linha(s) afetadas)

(16 linha(s) afetadas)

(1 linha(s) afetadas)
        
    INSERT INTO [VBR008002-016\QUALID].[ALS_PRODUTO_DB].DBO.[QUESTIONARIO_PERGUNTA_TB]        
       ([questionario_id], [pergunta_id], [num_ordem_pergunta], [num_pergunta_precedente], [grupo_dominio_resposta_id], [dominio_resposta_id], [dt_inclusao], [dt_alteracao], [usuario], [ENVIADO_BB], [DT_ENVIO], [situacao], [resposta_padrao], [imprimir], [ind_obrigatoriedade], [fl_permitir_alteracao])        
            
    SELECT [questionario_id], [pergunta_id], [num_ordem_pergunta], [num_pergunta_precedente], [grupo_dominio_resposta_id], [dominio_resposta_id], [dt_inclusao], '20200517', '14271121851', [ENVIADO_BB], [DT_ENVIO], [situacao], [resposta_padrao], [imprimir], [ind_obrigatoriedade], [fl_permitir_alteracao]        
     FROM [LS_SUS].[ALS_PRODUTO_DB].DBO.[QUESTIONARIO_PERGUNTA_TB]  WITH(NOLOCK)        
    WHERE (questionario_id in (497,604,609,610,611,613,619,624,750,784,794,803,857,869,985))         
    
Insert INTO | PERGUNTA_TB

(1 linha(s) afetadas)

(26 linha(s) afetadas)

(1 linha(s) afetadas)
        
    INSERT INTO [VBR008002-016\QUALID].[ALS_PRODUTO_DB].DBO.[PERGUNTA_TB]        
       ([pergunta_id], [nome], [ind_obrigatoriedade], [validacao_objeto_banco_id], [grupo_dominio_resposta_id], [formato_resposta_id], [tamanho_resposta], [dt_inclusao], [dt_alteracao], [usuario], [ENVIADO_BB], [DT_ENVIO], [situacao], [tp_questao_referencia_id], [tabela_destino], [campo_destino], [procedure_calcular_resposta], [seq_pergunta], [ind_posicao], [tipo], [no_linhas_resposta], [campo_habilitado], [tp_sistema_id], [tabela_destino_endosso], [coluna_destino_endosso], [coluna_texto_complementar_endosso])        
            
    SELECT [pergunta_id], [nome], [ind_obrigatoriedade], [validacao_objeto_banco_id], [grupo_dominio_resposta_id], [formato_resposta_id], [tamanho_resposta], [dt_inclusao], '20200517', '14271121851', [ENVIADO_BB], [DT_ENVIO], [situacao], [tp_questao_referencia_id], [tabela_destino], [campo_destino], [procedure_calcular_resposta], [seq_pergunta], [ind_posicao], [tipo], [no_linhas_resposta], [campo_habilitado], [tp_sistema_id], [tabela_destino_endosso], [coluna_destino_endosso], [coluna_texto_complementar_endosso]        
     FROM [LS_SUS].[ALS_PRODUTO_DB].DBO.[PERGUNTA_TB]  WITH(NOLOCK)        
    WHERE (pergunta_id in (11142,11143,13681,13682,13698,13699,13700,13701,13702,13703,13704,13705,13706,13707,13708,13709,13710,13711,13712,13713,13714,13716,13718,13719,13720,13721,13726,2767,2771,2778,2779,2780,2781,2782,2784,2785,2786,2787,2788,2789,2790,2791,2792,2793,2794,2795,2797,2803,2804,2813,2816,2817,2825,2826,2829,2880,2881,2882,2884,2885,3053,3054,3055,3056,3057,3058,3059,3060,3275,3276,3277,3278,3279,3280,3427,3546,3547,4880,5617,5824,5825,5826,5827,5828,5829,5830,5831,6123,6200,6597,6748,6920,7176,7177,7455,7456,7457,7458,7459,7460,7538,7539,7540,7541,7542,7543,7544,7548,7549,7550,7551,7552,7553,7554,7555,7556,7557,7558,7559,7560,7561,7562,7563,7564,7565,7568,7569,7570,7573,7574,7575,7576,7577,7578,7579,7580,7581,7582,7583,7584,7585,7586,7587,7589,7590,7591,7593,7594,7595,7596,7597,7598,7599,7600,7601,7602,7603,7604,7605,7606,7607,7608,7609,7610,7611,7612,7613,7614,7615,7616,7617,7618,7619,7621,7622,7623,7624,7625,7626,7627,7628,7629,7630,7631,7632,7633,7634,7635,7636,7637,7638,7639,7640,7642,7643,7644,7645,7646,7647,7648,7765,8129,8300,8302,8303,8584,8586,8588,8590,8592,8696,8698,8700,8701,8702,8705,8706,8707,8708,8711,8712,8713,8790,8791,8792,8940,8941,8942,8943,8944,8945,8946,8947,8948,8949,8950,8951,8952,8953,8954,8955,8956,8957,8958,8959,8960,8961,8962,8963,9017,9018,9339,9340,9341))         
    
Insert INTO | QUESTIONARIO_TB

(1 linha(s) afetadas)

(10 linha(s) afetadas)

(1 linha(s) afetadas)
        
    INSERT INTO [VBR008002-016\QUALID].[ALS_PRODUTO_DB].DBO.[QUESTIONARIO_TB]        
       ([questionario_id], [nome], [situacao], [dt_inclusao], [dt_alteracao], [usuario], [ENVIADO_BB], [DT_ENVIO], [tipo], [tp_sistema_id])        
            
    SELECT [questionario_id], [nome], [situacao], [dt_inclusao], '20200517', '14271121851', [ENVIADO_BB], [DT_ENVIO], [tipo], [tp_sistema_id]        
     FROM [LS_SUS].[ALS_PRODUTO_DB].DBO.[QUESTIONARIO_TB]  WITH(NOLOCK)        
    WHERE (questionario_id in (497,604,609,610,611,613,619,624,750,784,794,803,857,869,985))         
    
Insert INTO | DOMINIO_RESPOSTA_TB

(1 linha(s) afetadas)

(7 linha(s) afetadas)

(1 linha(s) afetadas)
        
    INSERT INTO [VBR008002-016\QUALID].[ALS_PRODUTO_DB].DBO.[DOMINIO_RESPOSTA_TB]        
       ([grupo_dominio_resposta_id], [dominio_resposta_id], [resposta_determinada], [dt_inclusao], [dt_alteracao], [usuario], [nota])        
            
    SELECT [grupo_dominio_resposta_id], [dominio_resposta_id], [resposta_determinada], [dt_inclusao], '20200517', '14271121851', [nota]        
     FROM [LS_SUS].[ALS_PRODUTO_DB].DBO.[DOMINIO_RESPOSTA_TB]  WITH(NOLOCK)        
    WHERE (1=1)         
    
Insert INTO | GRUPO_DOMINIO_RESPOSTA_TB

(1 linha(s) afetadas)

(9 linha(s) afetadas)

(1 linha(s) afetadas)
        
    INSERT INTO [VBR008002-016\QUALID].[ALS_PRODUTO_DB].DBO.[GRUPO_DOMINIO_RESPOSTA_TB]        
       ([grupo_dominio_resposta_id], [nome], [dt_inclusao], [dt_alteracao], [usuario], [ENVIADO_BB], [DT_ENVIO], [situacao], [tp_sistema_id])        
            
    SELECT [grupo_dominio_resposta_id], [nome], [dt_inclusao], '20200517', '14271121851', [ENVIADO_BB], [DT_ENVIO], [situacao], [tp_sistema_id]        
     FROM [LS_SUS].[ALS_PRODUTO_DB].DBO.[GRUPO_DOMINIO_RESPOSTA_TB]  WITH(NOLOCK)        
    WHERE (1=1)         
    
Insert e Update Din�mico (sem DELETE) | TP_AVALIACAO_TB

(1 linha(s) afetadas)

(12 linha(s) afetadas)

(1 linha(s) afetadas)
            
    CREATE TABLE #td1 (tp_avaliacao_id VARCHAR(4000))            
    DECLARE @tp_avaliacao_id VARCHAR(4000)            
                    
    DECLARE td1 CURSOR FOR            
    SELECT [tp_avaliacao_id]            
     FROM [LS_SUS].[ALS_PRODUTO_DB].DBO.[TP_AVALIACAO_TB]  WITH(NOLOCK)            
    WHERE (tp_avaliacao_id in (1320,1417,1421,1422,1423,1426,1427,1434,1438))            
    OPEN td1            
    FETCH NEXT FROM td1 INTO @tp_avaliacao_id            
    WHILE @@FETCH_STATUS = 0            
    BEGIN            
     IF EXISTS(SELECT 1 FROM [VBR008002-016\QUALID].[ALS_PRODUTO_DB].DBO.[TP_AVALIACAO_TB]  WITH(NOLOCK) WHERE (tp_avaliacao_id = @tp_avaliacao_id))      
     BEGIN            
      SET NOCOUNT ON            
      UPDATE [VBR008002-016\QUALID].[ALS_PRODUTO_DB].DBO.[TP_AVALIACAO_TB]            
       SET tp_avaliacao_id = b.tp_avaliacao_id, nome = b.nome, tratamento = b.tratamento, origem = b.origem, situacao = b.situacao, dt_inclusao = b.dt_inclusao, dt_alteracao = '20200517', usuario = '14271121851', tp_ramo = b.tp_ramo, tp_motivo = b.tp_motivo, motivo_restricao_id = b.motivo_restricao_id, impeditivo = b.impeditivo            
       FROM [VBR008002-016\QUALID].[ALS_PRODUTO_DB].DBO.[TP_AVALIACAO_TB] AS a  WITH(NOLOCK)            
       INNER JOIN [LS_SUS].[ALS_PRODUTO_DB].DBO.[TP_AVALIACAO_TB] AS b  WITH(NOLOCK) ON (a.[tp_avaliacao_id] = b.[tp_avaliacao_id])            
      WHERE (b.tp_avaliacao_id = @tp_avaliacao_id)            
      SET NOCOUNT OFF            
     END            
     ELSE            
     BEGIN            
      SET NOCOUNT ON            
      INSERT INTO [VBR008002-016\QUALID].[ALS_PRODUTO_DB].DBO.[TP_AVALIACAO_TB]            
         ([tp_avaliacao_id], [nome], [tratamento], [origem], [situacao], [dt_inclusao], [dt_alteracao], [usuario], [tp_ramo], [tp_motivo], [motivo_restricao_id], [impeditivo])            
                       
      SELECT [tp_avaliacao_id], [nome], [tratamento], [origem], [situacao], [dt_inclusao], '20200517', '14271121851', [tp_ramo], [tp_motivo], [motivo_restricao_id], [impeditivo]            
       FROM [LS_SUS].[ALS_PRODUTO_DB].DBO.[TP_AVALIACAO_TB]  WITH(NOLOCK)            
      WHERE (tp_avaliacao_id = @tp_avaliacao_id)            
      SET NOCOUNT off            
     END            
    FETCH NEXT FROM td1 INTO @tp_avaliacao_id            
    END            
    CLOSE td1            
    DEALLOCATE td1            
    DROP TABLE #td1            
    
Insert e Update Din�mico (sem DELETE) | CBT_HIST_TB

(1 linha(s) afetadas)

(11 linha(s) afetadas)

(1 linha(s) afetadas)
            
    CREATE TABLE #td1 (CD_CBT VARCHAR(4000), NR_VERSAO VARCHAR(4000))            
    DECLARE @CD_CBT VARCHAR(4000), @NR_VERSAO VARCHAR(4000)            
                    
    DECLARE td1 CURSOR FOR            
    SELECT [CD_CBT], [NR_VERSAO]            
     FROM [LS_SUS].[ALS_PRODUTO_DB].DBO.[CBT_HIST_TB]  WITH(NOLOCK)            
    WHERE (cd_cbt in (339))            
    OPEN td1            
    FETCH NEXT FROM td1 INTO @CD_CBT, @NR_VERSAO            
    WHILE @@FETCH_STATUS = 0            
    BEGIN            
     IF EXISTS(SELECT 1 FROM [VBR008002-016\QUALID].[ALS_PRODUTO_DB].DBO.[CBT_HIST_TB]  WITH(NOLOCK) WHERE (CD_CBT = @CD_CBT AND NR_VERSAO = @NR_VERSAO))      
     BEGIN            
      SET NOCOUNT ON            
      UPDATE [VBR008002-016\QUALID].[ALS_PRODUTO_DB].DBO.[CBT_HIST_TB]            
       SET CD_CBT = b.CD_CBT, NR_VERSAO = b.NR_VERSAO, DT_INICIO_VIGENCIA = b.DT_INICIO_VIGENCIA, DT_FIM_VIGENCIA = b.DT_FIM_VIGENCIA, NOME = b.NOME, DESCRICAO = b.DESCRICAO, PALAVRA_CHAVE = b.PALAVRA_CHAVE, USUARIO = '14271121851', DT_INCLUSAO = b.DT_INCLUSAO, DT_ALTERACAO = '20200517', tp_cob_comp_id = b.tp_cob_comp_id            
       FROM [VBR008002-016\QUALID].[ALS_PRODUTO_DB].DBO.[CBT_HIST_TB] AS a  WITH(NOLOCK)            
       INNER JOIN [LS_SUS].[ALS_PRODUTO_DB].DBO.[CBT_HIST_TB] AS b  WITH(NOLOCK) ON (a.[CD_CBT] = b.[CD_CBT] AND a.[NR_VERSAO] = b.[NR_VERSAO])            
      WHERE (b.CD_CBT = @CD_CBT AND b.NR_VERSAO = @NR_VERSAO)            
      SET NOCOUNT OFF            
     END            
     ELSE            
     BEGIN            
      SET NOCOUNT ON            
      INSERT INTO [VBR008002-016\QUALID].[ALS_PRODUTO_DB].DBO.[CBT_HIST_TB]            
         ([CD_CBT], [NR_VERSAO], [DT_INICIO_VIGENCIA], [DT_FIM_VIGENCIA], [NOME], [DESCRICAO], [PALAVRA_CHAVE], [USUARIO], [DT_INCLUSAO], [DT_ALTERACAO], [tp_cob_comp_id])            
                       
      SELECT [CD_CBT], [NR_VERSAO], [DT_INICIO_VIGENCIA], [DT_FIM_VIGENCIA], [NOME], [DESCRICAO], [PALAVRA_CHAVE], '14271121851', [DT_INCLUSAO], '20200517', [tp_cob_comp_id]            
       FROM [LS_SUS].[ALS_PRODUTO_DB].DBO.[CBT_HIST_TB]  WITH(NOLOCK)            
      WHERE (CD_CBT = @CD_CBT AND NR_VERSAO = @NR_VERSAO)            
      SET NOCOUNT off            
     END            
    FETCH NEXT FROM td1 INTO @CD_CBT, @NR_VERSAO            
    END            
    CLOSE td1            
    DEALLOCATE td1            
    DROP TABLE #td1            
    
Insert e Update Din�mico (sem DELETE) | CBT

(1 linha(s) afetadas)

(7 linha(s) afetadas)

(1 linha(s) afetadas)
            
    CREATE TABLE #td1 (CD_CBT VARCHAR(4000))            
    DECLARE @CD_CBT VARCHAR(4000)            
                    
    DECLARE td1 CURSOR FOR            
    SELECT [CD_CBT]            
     FROM [LS_SUS].[ALS_PRODUTO_DB].DBO.[CBT]  WITH(NOLOCK)            
    WHERE (cd_cbt in (339))            
    OPEN td1            
    FETCH NEXT FROM td1 INTO @CD_CBT            
    WHILE @@FETCH_STATUS = 0            
    BEGIN            
     IF EXISTS(SELECT 1 FROM [VBR008002-016\QUALID].[ALS_PRODUTO_DB].DBO.[CBT]  WITH(NOLOCK) WHERE (CD_CBT = @CD_CBT))      
     BEGIN            
      SET NOCOUNT ON            
      UPDATE [VBR008002-016\QUALID].[ALS_PRODUTO_DB].DBO.[CBT]            
       SET CD_CBT = b.CD_CBT, NOME = b.NOME, DESCRICAO = b.DESCRICAO, PALAVRA_CHAVE = b.PALAVRA_CHAVE, USUARIO = '14271121851', DT_INCLUSAO = b.DT_INCLUSAO, DT_ALTERACAO = '20200517'            
       FROM [VBR008002-016\QUALID].[ALS_PRODUTO_DB].DBO.[CBT] AS a  WITH(NOLOCK)            
       INNER JOIN [LS_SUS].[ALS_PRODUTO_DB].DBO.[CBT] AS b  WITH(NOLOCK) ON (a.[CD_CBT] = b.[CD_CBT])            
      WHERE (b.CD_CBT = @CD_CBT)            
      SET NOCOUNT OFF            
     END            
     ELSE            
     BEGIN            
      SET NOCOUNT ON            
      INSERT INTO [VBR008002-016\QUALID].[ALS_PRODUTO_DB].DBO.[CBT]            
         ([CD_CBT], [NOME], [DESCRICAO], [PALAVRA_CHAVE], [USUARIO], [DT_INCLUSAO], [DT_ALTERACAO])            
                       
      SELECT [CD_CBT], [NOME], [DESCRICAO], [PALAVRA_CHAVE], '14271121851', [DT_INCLUSAO], '20200517'            
       FROM [LS_SUS].[ALS_PRODUTO_DB].DBO.[CBT]  WITH(NOLOCK)            
      WHERE (CD_CBT = @CD_CBT)            
      SET NOCOUNT off            
     END            
    FETCH NEXT FROM td1 INTO @CD_CBT            
    END            
    CLOSE td1            
    DEALLOCATE td1            
    DROP TABLE #td1            
    
Sincroniza��o Unidirecional de Registros | T�rmino
 
    */
    
    
    
  --EXEC als_produto_db.dbo.PRDS1801_SPI @cd_prd, @cd_mdld, @cd_item_mdld, @nr_versao, @usuario        
     
  IF @debug = 1 BEGIN PRINT 'Question�rio | Ok' PRINT '' END        
     
  /* Workflow */        
  IF @debug = 1 PRINT 'Workflow | VerIFicar e Iniciar'        
  PRINT 'Workflow | Inicio'        
     
     
  --   sp_linkedservers
     
   print 'EXEC [VBR008002-016\QUALID].interface_dados_db.dbo.inicio_produto_spi' + cast(@cd_item_mdld as varchar)+ ', ' + cast(@usuario as varchar)+ ', ' + cast(@debug as varchar) + ',  ''N'''        
  --EXEC [VBR008002-016\QUALID].interface_dados_db.dbo.inicio_produto_spi @cd_item_mdld, @usuario, @debug, 'N'        
     
  IF @debug = 1 BEGIN PRINT 'Workflow | Ok' PRINT '' END       
  
  /* Setando o estado inical do produto para T Cria��o e marcando como copiado */            
   SET @sql = '    
    UPDATE ITEM_MDLD_PRD_HIST_TB  
        SET CD_EST_ITEM_MDLD = ''T'',  
   COPIA_REALIZADA_QLD = ''S''  
     FROM [' + @Destino + '].als_produto_criacao_db.dbo.ITEM_MDLD_PRD_HIST_TB ITEM_MDLD_PRD_HIST_TB  
     OUTER APPLY (SELECT MAX(NR_VERSAO) NR_VERSAO  
        FROM [' + @Destino + '].als_produto_criacao_db.dbo.ITEM_MDLD_PRD_HIST_TB interno  
       WHERE interno.CD_PRD = ITEM_MDLD_PRD_HIST_TB.CD_PRD  
       AND interno.CD_MDLD = ITEM_MDLD_PRD_HIST_TB.CD_MDLD  
       AND interno.CD_ITEM_MDLD = ITEM_MDLD_PRD_HIST_TB.CD_ITEM_MDLD ) versao  
    WHERE ITEM_MDLD_PRD_HIST_TB.CD_PRD = ' + CAST (@ncd_prd AS VARCHAR) + '   
      AND ITEM_MDLD_PRD_HIST_TB.CD_MDLD = ' + CAST (@ncd_mdld AS VARCHAR) + '   
      AND ITEM_MDLD_PRD_HIST_TB.CD_ITEM_MDLD = ' + CAST(@ncd_item_mdld AS VARCHAR) + '   
      AND ITEM_MDLD_PRD_HIST_TB.NR_VERSAO = versao.NR_VERSAO  
    '    
   SET @parametros = '@Destino NVARCHAR(100), @cd_prd SMALLINT, @cd_mdld SMALLINT, @cd_item_mdld INT'    
     
     --PRINT (@SQL)    
   --EXECUTE sp_executeSql @sql, @parametros, @Destino = @Destino, @cd_prd = @cd_prd, @cd_mdld = @cd_mdld, @cd_item_mdld = @cd_item_mdld  
   
   --Rodrigo Moreira --Gerar workflow das contrata��es
    DECLARE @sqlwf AS VARCHAR(MAX) = ''
   
    SET @sqlwf = '
     
     IF NOT EXISTS(SELECT 1  
      FROM als_produto_db.dbo.produto_workflow_contrato_tb  
     WHERE CD_PRD = 960,  
       AND CD_MDLD = ''' + @Ncd_mdld_BB + '''  
       AND CD_ITEM_MDLD = ''' + @Ncd_item_mdld + '''  
       AND TP_WF_ID = ''' + @tipo_wf_id + ''')    
  BEGIN           
    INSERT INTO als_produto_db.dbo.produto_workflow_contrato_tb(CD_PRD,    
                   CD_MDLD,    
                   CD_ITEM_MDLD,    
                   TP_WF_ID,    
                   usuario,    
                   dt_inclusao,    
                   dt_inicio_vigencia)    
                  SELECT 960,  
                   '''+ @Ncd_mdld_BB +''',    
                   '''+ @Ncd_item_mdld+''',    
                   '''+ @tipo_wf_id +''',    
                   '''+@usuario+''',    
                   GETDATE(),    
                   '''+ @dt_op +'''     
      
   END 
   ' 
   
   --RODRIGO MOREIRA
   --COPIAR AS TAXAS PARA O NOVO CD_MDLD_BB --AGR�COLA 
   --USE ALS_PRODUTO_dB
   DECLARE @sqltx VARCHAR(MAX) = ''
   
 SET @sqltx =  
   '
   INSERT INTO als_produto_db.dbo.PRDD_SGRV_PRD_AGRL
    ( CD_PRD
     ,CD_MDLD
     ,CD_ITEM_MDLD
    ,CD_MUN_PRD_AGRL
	,CD_PRD_AGRL
	,DT_INC_PRDD_AGRL
	,DT_FIM_PRDD_AGRL
	,DT_INC_PRDC_AGRL
	,DT_FIM_PRDC_AGRL
	,QT_PRDD_SGRV_AGRL
	,IN_IRG_PRD_AGRL
	,CD_CBT
	,DET_PC_MIN_CBT
	,DET_PC_DSG
	,DET_PC_MAX_RSP
	,DET_FT_CBT
	,VL_MIN_CUSTEIO
	,VL_MAX_CUSTEIO
	,PC_SBS_CRITICO
	,PC_SBS_PRONAMP
	,PC_SBS_ORGANICO
	,exibir_calculo_simulador)
	SELECT 
		'+@CD_PRD_bb+'
	   ,'+@nCD_MDLD_bb+'
	   ,'+@nCD_ITEM_MDLD+'
		,CD_MUN_PRD_AGRL
		,CD_PRD_AGRL
		,DT_INC_PRDD_AGRL
		,DT_FIM_PRDD_AGRL
		,DT_INC_PRDC_AGRL
		,DT_FIM_PRDC_AGRL
		,QT_PRDD_SGRV_AGRL
		,IN_IRG_PRD_AGRL
		,CD_CBT
		,DET_PC_MIN_CBT
		,DET_PC_DSG
		,DET_PC_MAX_RSP
		,DET_FT_CBT
		,VL_MIN_CUSTEIO
		,VL_MAX_CUSTEIO
		,PC_SBS_CRITICO
		,PC_SBS_PRONAMP
		,PC_SBS_ORGANICO
		,exibir_calculo_simulador
	FROM als_produto_db.dbo.PRDD_SGRV_PRD_AGRL WITH (NOLOCK)
    WHERE CD_PRD = '+ CONVERT(VARCHAR(30),@cd_prd_bb) +' AND CD_MDLD = '+ @Acd_mdld_BB +' AND CD_ITEM_MDLD = '+ CONVERT(VARCHAR(30),@cd_item_mdld) +'
    AND dt_fim_prdd_agrl IS NULL
    
    
INSERT INTO als_produto_db.dbo.PRDD_SGRV_PRD_AGRL_HIST_TB
    (CD_PRD
	,CD_MDLD
	,CD_ITEM_MDLD
	,NR_VERSAO
	,CD_MUN_PRD_AGRL
	,CD_PRD_AGRL
	,DT_INC_PRDD_AGRL
	,DT_FIM_PRDD_AGRL
	,DT_INC_PRDC_AGRL
	,DT_FIM_PRDC_AGRL
	,QT_PRDD_SGRV_AGRL
	,CD_PRD_BB
	,CD_MDLD_BB
	,ENVIADO_BB
	,DT_ENVIO
	,dt_inclusao
	,dt_alteracao
	,usuario
	--,lock
	,IN_IRG_PRD_AGRL
	,CD_CBT
	,PC_MIN_CBT
	,PC_DSG
	,PC_MAX_RSP
	,DET_FT_CBT
	,Vl_Min_Custeio
	,Vl_Max_Custeio
	,Produto_id
	,PC_SBS_CRITICO
	,PC_SBS_PRONAMP
	,PC_SBS_ORGANICO
	,exibir_calculo_simulador ) 
    
SELECT 
	'+@cd_prd_bb+'
    ,'+@nCD_MDLD_bb+'
    ,'+@nCD_ITEM_MDLD+'
	,NR_VERSAO
	,CD_MUN_PRD_AGRL
	,CD_PRD_AGRL
	,DT_INC_PRDD_AGRL
	,DT_FIM_PRDD_AGRL
	,DT_INC_PRDC_AGRL
	,DT_FIM_PRDC_AGRL
	,QT_PRDD_SGRV_AGRL
	,CD_PRD_BB
	,CD_MDLD_BB
	,ENVIADO_BB
	,DT_ENVIO
	,dt_inclusao
	,dt_alteracao
	,usuario
	--,lock
	,IN_IRG_PRD_AGRL
	,CD_CBT
	,PC_MIN_CBT
	,PC_DSG
	,PC_MAX_RSP
	,DET_FT_CBT
	,Vl_Min_Custeio
	,Vl_Max_Custeio
	,Produto_id
	,PC_SBS_CRITICO
	,PC_SBS_PRONAMP
	,PC_SBS_ORGANICO
	,exibir_calculo_simulador
 FROM als_produto_db.dbo.PRDD_SGRV_PRD_AGRL_HIST_TB WITH (NOLOCK)
    WHERE CD_PRD = '+ CONVERT(VARCHAR(30),@cd_prd_bb) +' AND CD_MDLD = '+ @Acd_mdld_BB +' AND CD_ITEM_MDLD = '+ CONVERT(VARCHAR(30),@cd_item_mdld) +'
    AND dt_fim_prdd_agrl IS NULL
    
 ' 
 
 --atualizar produto_tb
DECLARE @sqlprd AS VARCHAR(MAX) = ''
 
 SET @sqlprd =
 '
  UPDATE a
  SET cd_prd = '+ @cd_prd_bb +'
     ,CD_MDLD = '+ @ncd_mdld_bb +'
     ,CD_ITEM_MDLD = '+ @ncd_item_mdld +'
  FROM seguros_db.dbo.produto_tb a with (nolock)
 WHERE produto_id = '+ @Ncd_prd +'
 '
 
   --SUBVEN��O,SBS_SGRO_AGRL,SBS_SGRO_AGRL_HIST_TB,UF_SBS_SGRO_AGRL_TB
   --UPDATE SUBRAMO
   
   
   --GERA��O DO SCRIPT
   DECLARE @id_dp INT = (
   SELECT TOP 1 id
   FROM #prds02147_spi
   WHERE CONVERT(VARCHAR(200),csql) LIKE '%dados_publicacao_tb%'
    AND CONVERT(VARCHAR(200),csql)  LIKE  '%insert%'
   )
    
   
  UPDATE a
  SET csql = REPLACE(REPLACE(csql,'@usuario',''''+ @usuario+ ''''),'[SISAB051\EXTRATOR]','[LS_SUS]')
  from #prds02147_spi a

  
  UPDATE a
  SET csql = REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(csql,'@cd_mdld',@Ncd_mdld),'@cd_item_mdld',@Ncd_item_mdld),'@usuario',''''+ @usuario+ ''''),'[SISAB051\EXTRATOR]','[LS_SUS]'),'@cd_prd',''''+ @Ncd_prd + '''')    
  from #prds02147_spi a
  WHERE id <= @id_dp
  
  UPDATE a
  SET csql = REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(csql,'@cd_mdld',CONVERT(VARCHAR(10),@cd_mdld)),'@cd_item_mdld',CONVERT(VARCHAR(10),@cd_item_mdld)),'@usuario',''''+ @usuario+ ''''),'[SISAB051\EXTRATOR]','[LS_SUS]'),'@cd_prd',''''+ CONVERT(VARCHAR(10),@cd_prd) + '''')    
  from #prds02147_spi a
  WHERE id > @id_dp
  
   --SELECT @id_dp 
   
  UPDATE a
  SET csql = '   102 AS cd_item_mdld,'
  FROM #prds02147_spi a
  WHERE id > @id_dp
  AND LEN(REPLACE(REPLACE(REPLACE(RTRIM(LTRIM(CSQL)),CHAR(10),''),CHAR(13),''),' ','')) > 35 
  AND csql like '%producao.cd_item_mdld,%' 
 
  UPDATE a
  SET csql = '   1 AS cd_mdld,'
  FROM #prds02147_spi a
  WHERE id > @id_dp
  AND LEN(REPLACE(REPLACE(REPLACE(RTRIM(LTRIM(CSQL)),CHAR(10),''),CHAR(13),''),' ','')) > 35 
  AND csql like '%producao.cd_mdld,%' 
  
   UPDATE a
  SET csql = '   1 AS cd_mdld,'
  FROM #prds02147_spi a
  WHERE id > @id_dp + 20
  AND LEN(REPLACE(REPLACE(REPLACE(RTRIM(LTRIM(CSQL)),CHAR(10),''),CHAR(13),''),' ','')) < 35 
  AND csql like '%cd_mdld,%' 
  AND csql NOT LIKE '1%'
   AND NOT (EXISTS (SELECT 1 FROM #prds02147_spi b
                  WHERE a.id -1 = b.id
                   AND b.csql LIKE '%INSERT%')
           OR
                  
           EXISTS (SELECT 1 FROM #prds02147_spi b
                  WHERE a.id -2 = b.id
                   AND b.csql LIKE '%INSERT%') 
                   
            
            OR
                  
            EXISTS (SELECT 1 FROM #prds02147_spi b
                  WHERE a.id -3 = b.id
                   AND b.csql LIKE '%INSERT%') 
                           
            OR
                  
             EXISTS (SELECT 1 FROM #prds02147_spi b
                  WHERE a.id -4 = b.id
                   AND b.csql LIKE '%INSERT%') 
           )   
 
   UPDATE a
  SET csql = '   102 AS cd_item_mdld,'
  FROM #prds02147_spi a
  WHERE id > @id_dp + 20
  AND LEN(REPLACE(REPLACE(REPLACE(RTRIM(LTRIM(CSQL)),CHAR(10),''),CHAR(13),''),' ','')) < 35 
  AND csql like '%cd_item_mdld,%' 
  AND csql NOT LIKE '1%'
  AND NOT (EXISTS (SELECT 1 FROM #prds02147_spi b
                  WHERE a.id -1 = b.id
                   AND b.csql LIKE '%INSERT%')
           OR
                  
           EXISTS (SELECT 1 FROM #prds02147_spi b
                  WHERE a.id -2 = b.id
                   AND b.csql LIKE '%INSERT%') 
                   
            
            OR
                  
            EXISTS (SELECT 1 FROM #prds02147_spi b
                  WHERE a.id -3 = b.id
                   AND b.csql LIKE '%INSERT%') 
                           
            OR
                  
             EXISTS (SELECT 1 FROM #prds02147_spi b
                  WHERE a.id -4 = b.id
                   AND b.csql LIKE '%INSERT%') 
           )        
 
  UPDATE a
  SET csql = '--AND QUALIDADE.cd_prd IS NULL'
  FROM #prds02147_spi a
  WHERE id > @id_dp
  AND csql like '%AND QUALIDADE%' 
  AND csql like '%IS NULL%'
  
  UPDATE a
  SET csql = 'WHERE QUALIDADE.texto_HTML IS NULL AND 1=2'
  FROM #prds02147_spi a
  WHERE  csql like '%WHERE QUALIDADE.texto_HTML IS NULL%' 
  
  INSERT INTO #prds02147_spi (csql)
  SELECT '--****COPIA DOS DADOS PROCEDURE PRDS02147_SPI***'
 
  INSERT INTO #prds02147_spi (csql)
  SELECT '--****DELETANDO OS DADOS***' 

  INSERT INTO #prds02147_spi (csql)
  SELECT @sqldel AS DEL
  
  INSERT INTO #prds02147_spi
  SELECT '--****INSERINDO OS DADOS***' 

  INSERT INTO #prds02147_spi (csql)
  SELECT @sqlins AS INS
  
  INSERT INTO #prds02147_spi (csql)
  SELECT '--****ATUALIZANDO OS DADOS***' 

  INSERT INTO #prds02147_spi (csql)
  SELECT @sqlatu AS ATU
  
  INSERT INTO #prds02147_spi (csql)
  SELECT @sql
  
  --Rodrigo.moreira
  ---gerar a publica��o do produto
  INSERT INTO #prds02147_spi (csql)
  SELECT 'EXEC seguros_db..PRDS02138_SPI @cd_prd = '+ @Ncd_prd + ', @cd_mdld = '+ @Ncd_mdld + ', @cd_item_mdld = '+ @Ncd_item_mdld + ', @usuario = ''lromero'', @EMPRESA = ''AB'' '  
  
  --rodrigo moreira
  --Gerar WF novo CD_MDLD
  INSERT INTO #prds02147_spi (csql)
  SELECT @sqlwf
  
  --Gerar TAXAS
  INSERT INTO #prds02147_spi (csql)
  SELECT @sqltx
  
   --aTUALIZANDO PRODUTO
  INSERT INTO #prds02147_spi (csql)
  SELECT @sqlprd
  
  SELECT csql FROM #prds02147_spi 
  ORDER BY id ASC
  
  
  --SELECT TOP 0 * INTO #sql  FROM #prds02147_spi 
  
  --INSERT INTO #sql (csql)
  --SELECT '--****DELETANDO OS DADOS***' 

  --INSERT INTO #sql (csql)
  --SELECT @sqldel AS DEL
  
  --INSERT INTO #sql
  --SELECT '--****INSERINDO OS DADOS***' 

  --INSERT INTO #sql (csql)
  --SELECT @sqlins AS INS
  
  --INSERT INTO #sql (csql)
  --SELECT '--****COPIA DOS DADOS PROCEDURE PRDS02147_SPI***'
  
  --INSERT INTO #sql (csql)
  --SELECT csql FROM #prds02147_spi
  --ORDER BY id asc
  
  --INSERT INTO #sql (csql)
  --SELECT '--****ATUALIZANDO OS DADOS***' 

  --INSERT INTO #sql (csql)
  --SELECT @sqlatu AS ATU
  
  --SELECT csql FROM #sql 
  --ORDER BY id ASC
  
  
  /* descarte de tabelas */        
  DROP TABLE #t1        
  DROP TABLE #t2        
     
  /* Transa��o */        
  IF @debug = 1 PRINT 'Transa��o | Commit Transaction'        
 
 
 
 -----SCRIPT COMPLEMENTAR -----------------
 /*
 go
 USE desenv_db
 GO
 ALTER PROCEDURE DBO.RDG
 AS  
 
	update a
	set dt_fim_vigencia_sbr = GETDATE()
	from seguros_Db.dbo.subramo_tb a
   where subramo_id = 102 and dt_fim_vigencia_sbr is null
   and dt_inicio_vigencia_sbr < '2020-01-01'
 
 begin tran 
 --select @@TRANCOUNT
 --exec dbo.rdg
 --COMMIT
 
*/
 
--------SCRIPT COMPLEMENTAR ----------

 /*
 go
 USE desenv_db
 GO
 ALTER PROCEDURE DBO.RDG
 AS  
 
	update a
	set dt_fim_qstn = GETDATE()
	from als_produto_db.dbo.QSTN_ITEM_MDLD_HIST_TB a
   where cd_prd = 1152 and cd_mdld = 1 and cd_item_mdld = 102 and nr_qstn = 985

   
 begin tran 
 select @@TRANCOUNT
 exec dbo.rdg
 COMMIT
 
*/


--------SCRIPT COMPLEMENTAR ----------

 /*
 go
 USE desenv_db
 GO
 ALTER PROCEDURE DBO.RDG
 AS  
 

INSERT INTO  als_produto_db.dbo.SBS_SGRO_AGRL_HIST_TB (
CD_PRD_BB
,CD_MDLD_BB
,CD_PRD
,CD_MDLD
,CD_ITEM_MDLD
,CD_PRD_AGRL
,SG_UF_SGRO_AGRL
,CD_TIP_SBS_SGRO
,DT_INI_SBS_AGRL 
,DT_FIM_SBS_AGRL
,PC_SBS_SGRO_AGRL
,DT_INCLUSAO
,DT_ALTERACAO
,USUARIO
--,LOCK
,ENVIADO
,DT_ENVIO
,DIREITO_SUBSIDIO
,PERC_NIV_COB
,PC_SBS_SGRO_AGRL_BB
)

--CD_PRD, CD_PRD_AGRL, CD_TIP_SBS_SGRO, SG_UF_SGRO_AGRL, DT_INI_SBS_AGRL, PERC_NIV_COB

 select CD_PRD_BB
,5 AS CD_MDLD_BB
,CD_PRD
,5 AS CD_MDLD
,102 AS CD_ITEM_MDLD
,CD_PRD_AGRL
,SG_UF_SGRO_AGRL
,CD_TIP_SBS_SGRO
,DATEADD(MONTH,1,DT_INI_SBS_AGRL)
,DT_FIM_SBS_AGRL
,PC_SBS_SGRO_AGRL
,GETDATE()
,DT_ALTERACAO
,USUARIO
--,LOCK
,ENVIADO
,DT_ENVIO
,DIREITO_SUBSIDIO
,PERC_NIV_COB
,PC_SBS_SGRO_AGRL_BB
 from als_produto_db.dbo.SBS_SGRO_AGRL_HIST_TB WITH (NOLOCK) 
 WHERE cd_prd = 1152 and cd_mdld = 2
 and dt_fim_sbs_Agrl is null
 
 INSERT INTO als_produto_db.dbo.sbs_sgro_agrl
 
      (CD_PRD
		,CD_MDLD
		,CD_ITEM_MDLD
		,CD_PRD_AGRL
		,SG_UF_SGRO_AGRL
		,CD_TIP_SBS_SGRO
		,DT_INI_SBS_AGRL
		,DT_FIM_SBS_AGRL
		,PC_SBS_SGRO_AGRL
		,DT_INCLUSAO
		,DT_ALTERACAO
		,USUARIO
		--,LOCK
		,PERC_NIV_COB
		,PC_SBS_SGRO_AGRL_BB)
 
 select  CD_PRD
		,5 AS CD_MDLD
		,102 AS CD_ITEM_MDLD
		,CD_PRD_AGRL
		,SG_UF_SGRO_AGRL
		,CD_TIP_SBS_SGRO
		,DATEADD(MONTH,1,DT_INI_SBS_AGRL)
		,DT_FIM_SBS_AGRL
		,PC_SBS_SGRO_AGRL
		,GETDATE()--DT_INCLUSAO
		,DT_ALTERACAO
		,USUARIO
		--,LOCK
		,PERC_NIV_COB
		,PC_SBS_SGRO_AGRL_BB
 from als_produto_db.dbo.sbs_sgro_agrl with (nolock)
 WHERE cd_prd = 1152 and cd_mdld = 2
 and dt_fim_sbs_Agrl is null
 ORDER BY SG_UF_SGRO_aGRL
 
  begin tran 
  --ROLLBACK
 select @@TRANCOUNT
 exec dbo.rdg
 COMMIT
 
 */
 
 
 
--------SCRIPT COMPLEMENTAR ----------

 /*
 go
 USE desenv_db
 GO
 ALTER PROCEDURE DBO.RDG
 AS  
 
insert into SEGUROS_dB.DBO.ARQUIVO_PRODUTO_TB
(
 arquivo
,produto_externo_id
,produto_id
,usuario
,dt_inclusao
,dt_alteracao
--,lock
,origem
,path
,ind_funcao
,corretor_id
,sucursal_seguradora_id
,ramo_id
,subramo_id
,dt_inicio_vigencia_sbr
,ajustavel
,processa_generico
) 
 
 
SELECT --arquivo_produto_id
  arquivo
,102 AS produto_externo_id
,produto_id
,usuario
,GETDATE() AS dt_inclusao
,dt_alteracao
--,lock
,origem
,path
,ind_funcao
,corretor_id
,sucursal_seguradora_id
,1 AS ramo_id
,102 AS subramo_id
,'2020-04-28 00:00:00' AS dt_inicio_vigencia_sbr
,ajustavel
,processa_generico
FROM SEGUROS_dB.DBO.ARQUIVO_PRODUTO_TB WITH (NOLOCK)
WHERE PRODUTO_ID = 1152

  
 begin tran 
 select @@TRANCOUNT
 exec dbo.rdg
 COMMIT
 
*/


 
--------SCRIPT COMPLEMENTAR ----------

 /*
 go
 USE desenv_db
 GO
 ALTER PROCEDURE DBO.RDG
 AS  

INSERT INTO als_produto_Db.dbo.produto_tb
(
CD_PRD
, CD_MDLD
,CD_ITEM_MDLD
,seguradora_cod_susep
,nome
,acerto_co_seguro
,num_max_parcela
,tp_renovacao
,prazo_seguro
,apolice_coletiva
,ativo
,prazo_recusa_proposta
,prazo_doc_comp
,tolerancia_doc_comp
,assistencia
,produto_anterior_id
,dt_migracao
,processa_cobranca
,processa_cancelamento
,max_parcelas_atraso
,taxa_servico
,aceite_bb
,tp_calc_restituicao
,max_dias_atraso
,reintegracao_automatica
,tp_emissao
,emissao_on_line
,num_proc_susep
,cod_produto_ps
,apolice_envia_cliente
,apolice_num_vias
,apolice_envia_congenere
,prazo_restituicao_integral
,restituicao_tp_beneficiario
,restituicao_cliente_id
,prazo_aceite
,endosso_on_line
,cod_tp_reserva
,cod_tp_baixa
,retorno_endosso
,origem_produto
,tp_fatura_web
,limite_idade_renov
,pessoa_id
,usuario
,dt_inclusao
,dt_alteracao
--lock
,produto_id
,processa_restituicao_automatica
,limite_produto
,gera_apolice
,gera_certificado
,gera_fatura
,procedure_calcular_prejuizo
,procedure_gravar_detalhamento
,ir_tributavel_estipulante
)

select 
 CD_PRD
,5 as CD_MDLD
,102 as CD_ITEM_MDLD
,seguradora_cod_susep
,nome
,acerto_co_seguro
,num_max_parcela
,tp_renovacao
,prazo_seguro
,apolice_coletiva
,ativo
,prazo_recusa_proposta
,prazo_doc_comp
,tolerancia_doc_comp
,assistencia
,produto_anterior_id
,dt_migracao
,processa_cobranca
,processa_cancelamento
,max_parcelas_atraso
,taxa_servico
,aceite_bb
,tp_calc_restituicao
,max_dias_atraso
,reintegracao_automatica
,tp_emissao
,emissao_on_line
,num_proc_susep
,cod_produto_ps
,apolice_envia_cliente
,apolice_num_vias
,apolice_envia_congenere
,prazo_restituicao_integral
,restituicao_tp_beneficiario
,restituicao_cliente_id
,prazo_aceite
,endosso_on_line
,cod_tp_reserva
,cod_tp_baixa
,retorno_endosso
,origem_produto
,tp_fatura_web
,limite_idade_renov
,pessoa_id
,usuario
,dt_inclusao
,dt_alteracao
--lock
,produto_id
,processa_restituicao_automatica
,limite_produto
,gera_apolice
,gera_certificado
,gera_fatura
,procedure_calcular_prejuizo
,procedure_gravar_detalhamento
,ir_tributavel_estipulante
from als_produto_db.dbo.produto_tb where produto_id = 1152

  
 begin tran 
 select @@TRANCOUNT
 exec dbo.rdg
 COMMIT
 
*/



 
--------SCRIPT COMPLEMENTAR ----------

 /*
 go
 USE desenv_db
 GO
 ALTER PROCEDURE DBO.RDG
 AS  

INSERT INTO seguros_Db.dbo.parametro_agendamento_tb
(
produto_id
,ramo_id
,subramo_id
,parc_1_utilizar_dia_debito
,parc_1_tp_dt_base
,parc_1_dias_para_agendamento
,parc_1_mes_seguinte
,gerar_nosso_numero
,dt_inicio_vigencia
,dt_fim_vigencia
,tp_documento
,permitir_2_parcelas_mesmo_mes
,dt_inclusao
,dt_alteracao
,usuario
--,lock
,parc_1_utilizar_nosso_numero_BB
,agenda_pgto_ato_dt_sistema
,ajusta_antecedencia_cobranca
,periodicidade_agendamento
,tp_periodicidade_agendamento
,parc_1_dias_para_agendamento_ato
,canal_id
,parc_2_dias_para_agendamento
,parc_1_considera_dias_corridos
,parc_2_considera_dias_corridos
)

SELECT produto_id
,1 as ramo_id
,102 as subramo_id
,parc_1_utilizar_dia_debito
,parc_1_tp_dt_base
,parc_1_dias_para_agendamento
,parc_1_mes_seguinte
,gerar_nosso_numero
,dt_inicio_vigencia
,dt_fim_vigencia
,tp_documento
,permitir_2_parcelas_mesmo_mes
,GETDATE() AS dt_inclusao
,dt_alteracao
,usuario
--,lock
,parc_1_utilizar_nosso_numero_BB
,agenda_pgto_ato_dt_sistema
,ajusta_antecedencia_cobranca
,periodicidade_agendamento
,tp_periodicidade_agendamento
,parc_1_dias_para_agendamento_ato
,canal_id
,parc_2_dias_para_agendamento
,parc_1_considera_dias_corridos
,parc_2_considera_dias_corridos 
FROM seguros_Db.dbo.parametro_agendamento_tb with (nolock)
WHERE produto_id = 1152 and ramo_id = 2 and tp_documento = 'c'

INSERT INTO seguros_Db.dbo.parametro_agendamento_parcelamento_tb
(produto_id
 ,ramo_id
,subramo_id
,dt_inicio_vigencia
,tp_documento
,canal_id
,num_parcela
,periodo_agendamento_apos_parc_1
,dt_inclusao
,dt_alteracao
,usuario
--,lock
)
select produto_id
,1 AS ramo_id
,102 AS subramo_id
,dt_inicio_vigencia
,tp_documento
,canal_id
,num_parcela
,periodo_agendamento_apos_parc_1
,GETDATE() AS dt_inclusao
,dt_alteracao
,usuario
--,lock
from seguros_Db.dbo.parametro_agendamento_parcelamento_tb with (nolock)
WHERE produto_id = 1152 AND ramo_id = 2 and tp_documento = 'c'

GO 
USE DESENV_dB 
GO
 begin tran 
 select @@TRANCOUNT
 --ROLLBACK
 exec dbo.rdg
 COMMIT
 
*/



--------SCRIPT COMPLEMENTAR ----------

 /*
 go
 USE desenv_db
 GO
 ALTER PROCEDURE DBO.RDG
 AS  

INSERT INTO resseg_db.dbo.contrato_subramo_tb 
(
ramo_id
,subramo_id
,dt_inicio_vigencia_sbr
,resseguro_manual
,utilizar_IS_maior_cobertura
,produto_id
,contrato_id
,num_versao
,dt_inclusao
,dt_alteracao
,usuario
--,lock
,tp_cultura_id
,resseguro_manual_endosso
,permitir_contrato_sem_vigencia
,calcula_endosso_subvencao

)

select 
 1 AS ramo_id
,102 AS subramo_id
,'2020-04-28 00:00:00' AS dt_inicio_vigencia_sbr
,resseguro_manual
,utilizar_IS_maior_cobertura
,produto_id
,contrato_id
,num_versao
,GETDATE() AS dt_inclusao
,dt_alteracao
,usuario
--,lock
,tp_cultura_id
,resseguro_manual_endosso
,permitir_contrato_sem_vigencia
,calcula_endosso_subvencao
FROM resseg_Db.dbo.contrato_subramo_tb  WITH (NOLOCK)
WHERE PRODUTO_ID = 1152 
 AND SUBRAMO_ID = 202
 AND RAMO_ID = 2
 AND DT_ALTERACAO IS NULL




GO 
USE DESENV_dB 
GO
ALTER PROCEDURE
DBO.RDG AS
UPDATE a 
set subramo_id = 102
FROM resseg_Db.dbo.contrato_subramo_tb  A WITH (NOLOCK) 
WHERE PRODUTO_ID = 1152 
 AND SUBRAMO_ID = 2
 AND RAMO_ID = 1
 AND DT_ALTERACAO IS NULL
 AND dt_inicio_vigencia_sbr = '2020-04-28 00:00:00' 


ALTER PROCEDURE
DBO.RDG AS
insert into resseg_Db.dbo.produto_subramo_agricola_tb
(
produto_id
,ramo_id
,subramo_id
,fl_ativo
,dt_inclusao
,dt_alteracao
,usuario
--lock
)
SELECT

produto_id
,1 as ramo_id
,102 as subramo_id
,fl_ativo
,getdate() as dt_inclusao
,dt_alteracao
,usuario
--lock
 from resseg_Db.dbo.produto_subramo_agricola_tb WHERE produto_id = 1152


 begin tran 
 select @@TRANCOUNT
 --ROLLBACK
 exec dbo.rdg
 COMMIT
 
*/

/*  

ALTER PROCEDURE
DBO.RDG AS

UPDATE A
SET dt_fim_vigencia = DATEADD(MONTH,1,dt_fim_vigencia)
   ,dt_fim_cobertura = DATEADD(MONTH,1,dt_fim_cobertura)
   ,dt_limite_renovacao = DATEADD(MONTH,1,dt_limite_renovacao)
  FROM resseg_Db.dbo.contrato_Versao_tb A 
  where contrato_id = 799

 begin tran 
 select @@TRANCOUNT
 --ROLLBACK
 exec dbo.rdg
 COMMIT
 
*/
*/

/*  

USE DESENV_DB
GO
ALTER PROCEDURE
DBO.RDG AS

---inserir taxas 

INSERT INTO als_produto_db.DBO.PRDD_SGRV_PRD_AGRL
(
 CD_PRD
,CD_MDLD
,CD_ITEM_MDLD
,CD_MUN_PRD_AGRL
,CD_PRD_AGRL
,DT_INC_PRDD_AGRL
,DT_FIM_PRDD_AGRL
,DT_INC_PRDC_AGRL
,DT_FIM_PRDC_AGRL
,QT_PRDD_SGRV_AGRL
,IN_IRG_PRD_AGRL
,CD_CBT
,DET_PC_MIN_CBT
,DET_PC_DSG
,DET_PC_MAX_RSP
,DET_FT_CBT
,VL_MIN_CUSTEIO
,VL_MAX_CUSTEIO
,PC_SBS_CRITICO
,PC_SBS_PRONAMP
,PC_SBS_ORGANICO
,exibir_calculo_simulador)

select 

     960
     ,5
     ,102
     ,b.municipio_id
     ,c.tp_cultura_id
     ,'2020-02-05 00:00:00'
     ,NULL
     ,'2020-01-01 00:00:00'
     ,'2020-12-31 00:00:00'
     ,3250.00
     ,'N'
     ,339
     ,0
     ,0
     ,0
     ,0
     ,0
     ,0
     ,0
     ,0
     ,0
     ,'N'
from seguros_db.dbo.endereco_risco_tb a with (nolock)
  inner join seguros_db.dbo.municipio_tb b with (nolock)
    on a.municipio = b.nome
  inner join seguros_Db.dbo.seguro_esp_agricola_tb c with (nolock)
    on a.proposta_id = c.proposta_id
  left join als_produto_db..PRDD_SGRV_PRD_AGRL d with (nolock)
     on C.tp_cultura_id = D.CD_PRD_AGRL
     and CD_MDLD = 5
     and CD_ITEM_MDLD = 102
     and CD_MUN_PRD_AGRL = b.municipio_id
where a.proposta_id in 
(select proposta_id 
 from seguros_Db.dbo.proposta_tb with (nolock) 
 where produto_id = 1152 and ramo_id = 1 and situacao = 'e' and dt_contratacao > '2019-01-01' )
and d.CD_CBT is  null


 begin tran 
 select @@TRANCOUNT
 --ROLLBACK
 exec dbo.rdg
 COMMIT
 
*/



/*  

ALTER PROCEDURE
DBO.RDG AS

UPDATE A
SET dt_fim_vigencia = DATEADD(MONTH,1,dt_fim_vigencia)
   ,dt_fim_cobertura = DATEADD(MONTH,1,dt_fim_cobertura)
   ,dt_limite_renovacao = DATEADD(MONTH,1,dt_limite_renovacao)
  FROM resseg_Db.dbo.contrato_Versao_tb A 
  where contrato_id = 799

 begin tran 
 select @@TRANCOUNT
 --ROLLBACK
 exec dbo.rdg
 COMMIT
 
*/


/*  

USE DESENV_DB
GO
ALTER PROCEDURE
DBO.RDG AS

---inserir taxas 

INSERT INTO als_produto_db.DBO.PRDD_SGRV_PRD_AGRL
(
 CD_PRD
,CD_MDLD
,CD_ITEM_MDLD
,CD_MUN_PRD_AGRL
,CD_PRD_AGRL
,DT_INC_PRDD_AGRL
,DT_FIM_PRDD_AGRL
,DT_INC_PRDC_AGRL
,DT_FIM_PRDC_AGRL
,QT_PRDD_SGRV_AGRL
,IN_IRG_PRD_AGRL
,CD_CBT
,DET_PC_MIN_CBT
,DET_PC_DSG
,DET_PC_MAX_RSP
,DET_FT_CBT
,VL_MIN_CUSTEIO
,VL_MAX_CUSTEIO
,PC_SBS_CRITICO
,PC_SBS_PRONAMP
,PC_SBS_ORGANICO
,exibir_calculo_simulador)

select 

     960
     ,5
     ,102
     ,b.municipio_id
     ,c.tp_cultura_id
     ,'2020-02-05 00:00:00'
     ,NULL
     ,'2020-01-01 00:00:00'
     ,'2020-12-31 00:00:00'
     ,3250.00
     ,'N'
     ,339
     ,0
     ,0
     ,0
     ,0
     ,0
     ,0
     ,0
     ,0
     ,0
     ,'N'
from 
select d.* from seguros_db.dbo.endereco_risco_tb a with (nolock)
  inner join seguros_db.dbo.municipio_tb b with (nolock)
    on a.municipio = b.nome
  inner join seguros_Db.dbo.seguro_esp_agricola_tb c with (nolock)
    on a.proposta_id = c.proposta_id
  left join als_produto_db..PRDD_SGRV_PRD_AGRL d with (nolock)
     on C.tp_cultura_id = D.CD_PRD_AGRL
     and CD_MDLD = 5
     and CD_ITEM_MDLD = 102
     and CD_MUN_PRD_AGRL = b.municipio_id
where a.proposta_id in 
(select proposta_id 
 from seguros_Db.dbo.proposta_tb with (nolock) 
 where produto_id = 1152 and ramo_id = 1 and situacao = 'e' and dt_contratacao > '2019-01-01' )
and d.CD_CBT is not null


 begin tran 
 select @@TRANCOUNT
 --ROLLBACK
 exec dbo.rdg
 COMMIT
 
*/

/*
--***********N�o precisou *****----
Precisa � atualizar a tabela de subven��o

USE DESENV_DB
GO

ALTER PROCEDURE
DBO.RDG 
AS

INSERT INTO als_produto_db.dbo.SBS_SGRO_AGRL_HIST_TB 
(CD_PRD_BB
,CD_MDLD_BB
,CD_PRD
,CD_MDLD
,CD_ITEM_MDLD
,CD_PRD_AGRL
,SG_UF_SGRO_AGRL
,CD_TIP_SBS_SGRO
,DT_INI_SBS_AGRL
,DT_FIM_SBS_AGRL
,PC_SBS_SGRO_AGRL
,DT_INCLUSAO
,DT_ALTERACAO
,USUARIO
--,LOCK
,ENVIADO
,DT_ENVIO
,DIREITO_SUBSIDIO
,PERC_NIV_COB
,PC_SBS_SGRO_AGRL_BB)

select  
  960
  ,5
  ,1152
  ,5
  ,102
  ,c.tp_cultura_id
  ,b.estado
  ,1
  ,'2020-02-01 00:00:00'
  ,null
  ,90.00
  ,GETDATE()
  ,null
  ,'rsm'
  ,'N'
  ,null
  ,null
  ,convert(int, c.perc_nivel_cobertura)   
  --,convert(int, replace(e.texto_resposta,'%',''))  
  ,25.00
from seguros_db.dbo.endereco_risco_tb a with (nolock)
  inner join seguros_db.dbo.municipio_tb b with (nolock)
    on a.municipio = b.nome
  inner join seguros_Db.dbo.seguro_esp_agricola_tb c with (nolock)
    on a.proposta_id = c.proposta_id
  inner join als_produto_db.dbo.PRDD_SGRV_PRD_AGRL d with (nolock)
     on C.tp_cultura_id = D.CD_PRD_AGRL
     and CD_MDLD = 5
     and CD_ITEM_MDLD = 102
     and CD_MUN_PRD_AGRL = b.municipio_id
  inner join seguros_Db.dbo.questionario_objeto_tb e with (nolock)
     on a.proposta_id = e.proposta_id
     and e.pergunta_id =  5826 
   left join als_produto_db.dbo.SBS_SGRO_AGRL_HIST_TB  sbs 
      on sbs.CD_PRD_AGRL = c.tp_cultura_id                  
     and sbs.DT_FIM_SBS_AGRL is null                  
     and sbs.CD_TIP_SBS_SGRO = 1                
  AND sbs.CD_PRD = 1152            
  and sbs.PERC_NIV_COB = convert(int, c.perc_nivel_cobertura)      
where a.proposta_id in 
(select proposta_id 
 from seguros_Db.dbo.proposta_tb with (nolock) 
 where produto_id = 1152 and ramo_id = 1 and situacao = 'i'  and dt_contratacao > '2019-01-01' )
and sbs.CD_ITEM_MDLD is  null

 begin tran 
 select @@TRANCOUNT
 --ROLLBACK
 exec dbo.rdg
 COMMIT
 
  
*/


/*  

USE DESENV_DB
GO
ALTER PROCEDURE
DBO.RDG AS

---atualizar taxas cd_mun estava errado

UPDATE d
SET d.cd_mun_prd_agrl =  CONVERT(INT,LTRIM(RTRIM(q.texto_complementar)))
  from seguros_db.dbo.endereco_risco_tb a with (nolock)
  inner join seguros_db.dbo.municipio_tb b with (nolock)
    on a.municipio = b.nome
  inner join seguros_Db.dbo.seguro_esp_agricola_tb c with (nolock)
    on a.proposta_id = c.proposta_id
  INNER join als_produto_db..PRDD_SGRV_PRD_AGRL d with (nolock)
     on C.tp_cultura_id = D.CD_PRD_AGRL
     and CD_MDLD = 5
     and CD_ITEM_MDLD = 102
     and CD_MUN_PRD_AGRL = b.municipio_id
   join seguros_db.dbo.questionario_objeto_tb q        
      on q.proposta_id = a.proposta_id        
    -- and d.CD_MUN_PRD_AGRL = LTRIM(RTRIM(q.texto_complementar))        
     and q.pergunta_id = 3055     
where a.proposta_id in 
(select proposta_id 
 from seguros_Db.dbo.proposta_tb with (nolock) 
 where produto_id = 1152 and ramo_id = 1 and situacao = 'e' and dt_contratacao > '2019-01-01' )


 begin tran 
 select @@TRANCOUNT
 --ROLLBACK
 exec dbo.rdg
 COMMIT
 
*/

/*

--INSERT NA PROPOSTA SUBVEN��O

USE DESENV_DB
GO
ALTER PROCEDURE
DBO.RDG AS


if (object_id('tempdb.dbo.#subvencao_pc') > 0)  
begin  
 drop table #subvencao_pc  
end  
  
create   
 table #subvencao_pc  
  ( proposta_id numeric(9)  
 -- , seq_proposta int  
  , val_premio_tarifa numeric(15, 2)  
  , pc_sbs_sgro_agrl numeric (10,7)  
  , pc_sbs_critico numeric(10,7)  
  , pc_sbs_pronamp numeric(10,7)  
  , pc_sbs_organico numeric(10,7)  
  , beneficiario_pronamp varchar(10)  
  , cultivo varchar(30) )        
  
                
    --Selecionando propostas do Agr�cola  
insert   
  into #subvencao_pc  
  ( proposta_id   
  --, seq_proposta   
  , val_premio_tarifa   
  , pc_sbs_sgro_agrl   
  , pc_sbs_critico   
  , pc_sbs_pronamp   
  , pc_sbs_organico   
  , beneficiario_pronamp   
  , cultivo )                       
  SELECT distinct #proposta.proposta_id,                      
        -- #proposta.seq_proposta,                      
         --#dados_financeiros.val_parcela                      
         -- Autor: pablo.dias (Nova Consultoria) - Data da Altera��o: 22/08/2012        
         -- Demanda: 15839547 - Item: Defini��o do novo C�lculo da Subven��o Federal        
         -- Alterar c�lculo do percentual de subven��o        
         #dados_financeiros.val_premio_tarifa,        
         sbs.PC_SBS_SGRO_AGRL,        
         prdd.PC_SBS_CRITICO,        
         prdd.PC_SBS_PRONAMP,        
         prdd.PC_SBS_ORGANICO,        
         seg_esp.beneficiario_pronamp,        
         seg_esp.cultivo        
         -- FIM - pablo.dias - 22/08/2012        
--    INTO #subvencao_pc                      
    FROM seguros_db.dbo.proposta_tb #proposta (nolock)                        
    JOIN seguros_db.dbo.proposta_fechada_tb #dados_financeiros (nolock)
      ON #dados_financeiros.proposta_id = #proposta.proposta_id                      
    join seguros_db.dbo.seguro_esp_agricola_tb seg_esp (nolock)                  
      on #proposta.proposta_id = seg_esp.proposta_id                  
     join als_produto_db..SBS_SGRO_AGRL_HIST_TB sbs (nolock)        
      on sbs.CD_PRD_AGRL = seg_esp.tp_cultura_id                  
     and sbs.DT_FIM_SBS_AGRL is null                  
     and sbs.CD_TIP_SBS_SGRO = 1                
  AND sbs.CD_PRD = #proposta.produto_id              
  and sbs.PERC_NIV_COB = convert(int,replace(seg_esp.produtividade_segurada,'%',''))      
    join seguros_db.dbo.endereco_risco_tb er (nolock)                 
      on er.proposta_id = seg_esp.proposta_id                
     and er.estado = sbs.SG_UF_SGRO_AGRL                
     and end_risco_id = 1         
    JOIN als_produto_db.dbo.PRDD_SGRV_PRD_AGRL prdd (nolock)        
      ON sbs.CD_PRD_BB = prdd.CD_PRD        
     AND sbs.CD_MDLD = prdd.CD_MDLD        
     AND sbs.CD_ITEM_MDLD = prdd.CD_ITEM_MDLD        
     AND sbs.CD_PRD_AGRL = prdd.CD_PRD_AGRL        
    JOIN seguros_db..proposta_processo_susep_tb pps (nolock)        
      on pps.proposta_id = #proposta.proposta_id        
    join als_operacao_db..cbt_ctr ctr (nolock)        
      on ctr.nr_ctr_sgro = pps.num_contrato_seguro        
     and ctr.cd_mdld = pps.cod_modalidade        
     and ctr.cd_item_mdld = pps.cod_item_modalidade        
     and ctr.cd_prd = pps.cod_produto         
     AND ctr.CD_CBT = prdd.CD_CBT        
    join seguros_Db.dbo.questionario_objeto_Tb q (nolock)        
      on q.proposta_id = #proposta.proposta_id        
     and prdd.CD_MUN_PRD_AGRL = LTRIM(RTRIM(q.texto_complementar))        
     and q.pergunta_id = 3055        
     and q.questionario_id in (869)        
    WHERE #proposta.produto_id = 1152
   and  #proposta.ramo_id = 1
   and  #proposta.situacao = 'e'
   
 
                      
-- Autor: pablo.dias (Nova Consultoria) - Data da Altera��o: 22/08/2012        
-- Demanda: 15839547 - Item: Defini��o do novo C�lculo da Subven��o Federal        
-- Obter valores dos percentuais de subven��o        
SELECT distinct proposta_id,        
      -- seq_proposta,        
       val_premio_tarifa,        
       PC_SBS_SGRO_AGRL,        
       PC_SBS_CRITICO,        
       PC_SBS_PRONAMP = CASE         
            WHEN beneficiario_pronamp = 'SIM'         
            THEN PC_SBS_PRONAMP         
            ELSE 0.0         
       END,        
       PC_SBS_ORGANICO = CASE         
            WHEN cultivo = 'Org�nico'         
            THEN PC_SBS_ORGANICO         
            ELSE 0.0         
       END        
  INTO #subvencao_quest        
  FROM #subvencao_pc        
       
SELECT distinct proposta_id,        
      -- seq_proposta,        
       val_premio_tarifa,        
       PC_SBS_SGRO_AGRL,             
       modalidade_subvencao_id = CASE -- Valores obtidos na tabela modalidade_subvencao_tb        
            WHEN (PC_SBS_CRITICO >= PC_SBS_PRONAMP) AND (PC_SBS_CRITICO >= PC_SBS_ORGANICO)        
            THEN 3 -- MUNIC�PIO CR�TICO        
            ELSE (CASE WHEN PC_SBS_PRONAMP >= PC_SBS_ORGANICO         
                       THEN 1 -- PRONAMP        
                       ELSE 2 -- ORGANICO        
                  END)         
       END,        
       pc_subvencao_federal = CASE         
            WHEN (PC_SBS_CRITICO >= PC_SBS_PRONAMP) AND (PC_SBS_CRITICO >= PC_SBS_ORGANICO)        
            THEN PC_SBS_CRITICO         
            ELSE (CASE WHEN PC_SBS_PRONAMP >= PC_SBS_ORGANICO         
                       THEN PC_SBS_PRONAMP         
               ELSE PC_SBS_ORGANICO    
                  END)         
       END        
  INTO #subvencao_pc_sbs        
  FROM #subvencao_quest        
     
SELECT distinct proposta_id,        
       val_parcela = (val_premio_tarifa * ((PC_SBS_SGRO_AGRL / 100) + (pc_subvencao_federal / 100))),        
       -- pablo.dias (Nova Consultoria) - 10/09/2012 - Demanda: 15839547        
       -- N�o gravar modalidade de subven��o quando percentual for igual a zero        
       modalidade_subvencao_id = CASE WHEN pc_subvencao_federal > 0 THEN modalidade_subvencao_id ELSE NULL END,        
       pc_subvencao_federal = CASE WHEN pc_subvencao_federal > 0 THEN pc_subvencao_federal ELSE NULL END        
       -- FIM - pablo.dias - 10/09/2012        
  INTO #subvencao        
  FROM #subvencao_pc_sbs        
-- FIM - pablo.dias - 22/08/2012         
     
    --lrocha 15/07/2008 - Subven��o federal e estadual                    
    --Incluindo subven��o                      
    INSERT INTO seguros_db.dbo.proposta_subvencao_tb(proposta_id,                      
               val_subvencao_estimado,                      
                                      --val_subvencao_informado,                      
                                      endosso_id,                       
                                      dt_inclusao,                  
                                      usuario,               
                                      -- Autor: pablo.dias (Nova Consultoria) - Data da Altera��o: 22/08/2012        
                                      -- Demanda: 15839547 - Item: Defini��o do novo C�lculo da Subven��o Federal        
                                      -- Incluir valores dos percentuais de subven��o             
                                      tipo_subvencao_id,        
                                      modalidade_subvencao_id,        
                pc_subvencao_federal)                      
                                      -- FIM - pablo.dias - 22/08/2012        
    SELECT proposta_id,                      
           val_subvencao_estimado = val_parcela,                      
           --val_subvencao_informado = val_parcela,                      
           0,                      
           GETDATE(),                      
           'RSM',                    
           1, -- 1: Subvenc�o Federal / 2: Subven��o Estadual        
           -- Autor: pablo.dias (Nova Consultoria) - Data da Altera��o: 22/08/2012        
           -- Demanda: 15839547 - Item: Defini��o do novo C�lculo da Subven��o Federal        
           -- Novos campos em proposta_subvencao_tb        
           modalidade_subvencao_id,        
           pc_subvencao_federal        
           -- FIM - pablo.dias - 22/08/2012        
    FROM #subvencao                
         
   begin tran 
 select @@TRANCOUNT
 --ROLLBACK
 exec dbo.rdg
 COMMIT
 

*/



/*  

USE DESENV_DB
GO
ALTER PROCEDURE
DBO.RDG AS

--PARAMETRIZAR SINISTRO----

--SELECT * FROM 

INSERT INTO SEGUROS_dB.DBO.produto_estimativa_sinistro_tb
(
produto_id
,ramo_id
,tp_cobertura_id
,evento_sinistro_id
,dt_inicio_vigencia
,dt_fim_vigencia
,perc_estimativa
,val_inicial
,dt_inclusao
,dt_alteracao
,usuario
--,lock
,subramo_id
,situacao
,tp_componente_id
,origem_percentual
,cobertura_bb
,utiliza_percentual_subevento
,carencia
,tp_franquia
,dias_franquia
,valor_franquia
,percentual_franquia
,valor_percentual_franquia
,encerramento_sinistro
,dt_inicio_encerramento_sinistro
,dt_inicio_encerramento_automatico
)
SELECT
produto_id
,1 as ramo_id
,tp_cobertura_id
,evento_sinistro_id
,dt_inicio_vigencia
,dt_fim_vigencia
,perc_estimativa
,val_inicial
,getdate() as dt_inclusao
,dt_alteracao
,usuario
--,lock
,102 as subramo_id
,situacao
,tp_componente_id
,origem_percentual
,cobertura_bb
,utiliza_percentual_subevento
,carencia
,tp_franquia
,dias_franquia
,valor_franquia
,percentual_franquia
,valor_percentual_franquia
,encerramento_sinistro
,dt_inicio_encerramento_sinistro
,dt_inicio_encerramento_automatico
from SEGUROS_dB.DBO.produto_estimativa_sinistro_tb WHERE PRODUTO_ID = 1152 AND RAMO_ID = 2
and dt_fim_vigencia is null


 begin tran 
 select @@TRANCOUNT
 --ROLLBACK
 exec dbo.rdg
 COMMIT
 
*/


/*  

USE DESENV_DB
GO
ALTER PROCEDURE
DBO.RDG AS

---inserir produto nas tabelas de corretagem

UPDATE a
set  cd_prd = 960
    ,cd_mdld = 5
FROM als_produto_db.dbo.produto_corretagem_tb a
where produto_id = 1152
and cd_item_mdld = 102


 begin tran 
 select @@TRANCOUNT
 --ROLLBACK
 exec dbo.rdg
 COMMIT
 
*/

/* ---*****AJUSTANDO CORREETAGEM DA PROPOSTA

USE DESENV_DB
GO
ALTER PROCEDURE
DBO.RDG AS


INSERT INTO seguros_db.dbo.corretagem_tb
(proposta_id,
 dt_inicio_corretagem,
 corretor_id,
 dt_inclusao,
 usuario)
select a.proposta_id
      ,a.dt_contratacao
      ,100067199
      ,a.dt_contratacao
      ,'rsm'
from seguros_db.dbo.proposta_tb a 
where produto_id = 1152 and ramo_id = 1 and situacao = 'i' 


INSERT seguros_db.dbo.corretagem_pj_tb
(proposta_id,
 corretor_id,
 dt_inicio_corretagem,
 sucursal_corretor_id,
 perc_corretagem,
 perc_angariacao,
 dt_inclusao,
 usuario)
select a.proposta_id
      ,100067199
      ,a.dt_contratacao
      ,0000
      ,8.000000
      ,0.000000
      ,a.dt_contratacao
      ,'rsm'
from seguros_db.dbo.proposta_tb a 
where produto_id = 1152 and ramo_id = 1 and situacao = 'i' 

 begin tran 
 select @@TRANCOUNT
 --ROLLBACK
 exec dbo.rdg
 COMMIT

*/


/* ---*****AJUSTANDO SINISTRO

USE DESENV_DB
GO
ALTER PROCEDURE
DBO.RDG AS

INSERT INTO interface_Db.dbo.GTR_emissao_layout_tb 
(evento_id
,layout_id
,dt_emissao
,usuario
,dt_inclusao)
select 
 45452719
,879
,null
,'SMQP0069'
,GETDATE()


USE DESENV_DB
GO
ALTER PROCEDURE
DBO.RDG AS

UPDATE A 
set situacao_sinistro = 0
   ,situacao_aviso = 'N'
   ,BANCO_AVISO_ID = 1
FROM seguros_Db.dbo.evento_segbr_sinistro_Atual_tb a WHERE EVENTO_ID = 45452719

 begin tran 
 select @@TRANCOUNT
 --ROLLBACK
 exec dbo.rdg
 COMMIT


*/

 /*
 go
 USE desenv_db
 GO
 ALTER PROCEDURE DBO.RDG
 AS  
 
	update a
	set dt_operacional = '2020-05-24'
	    ,dt_contabilizacao = '2020-05-23'
     from seguros_Db.dbo.parametro_geral_tb a	    

   
 begin tran 
 select @@TRANCOUNT
 exec dbo.rdg
 COMMIT
 
*/