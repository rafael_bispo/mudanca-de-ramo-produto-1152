Attribute VB_Name = "FlexCombo"
Option Explicit

      Declare Function CallWindowProc Lib "user32" Alias _
         "CallWindowProcA" (ByVal lpPrevWndFunc As Long, _
         ByVal hwnd As Long, ByVal msg As Long, _
         ByVal wParam As Long, ByVal lparam As Long) As Long

      Declare Function SetWindowLong Lib "user32" Alias _
         "SetWindowLongA" (ByVal hwnd As Long, _
         ByVal nIndex As Long, ByVal dwNewLong As Long) As Long

      Private Const GWL_WNDPROC = -4
      Private IsHooked As Boolean
      Private Const WM_SIZE = &H5
      Private Const WM_PAINT = &HF
      Private lpPrevWndProc As Long
      Public gHW As Long

      Public Sub Hook()
          If IsHooked Then
          ' Do not hook it twice without unhooking,
          ' or you will not be able to unhook it.
          Else
          lpPrevWndProc = SetWindowLong(gHW, GWL_WNDPROC, _
             AddressOf WindowProc)
          IsHooked = True
          End If
      End Sub

      Public Sub Unhook()
          Dim temp As Long
          temp = SetWindowLong(gHW, GWL_WNDPROC, lpPrevWndProc)
          IsHooked = False
      End Sub

      Function WindowProc(ByVal hw As Long, ByVal uMsg As _
         Long, ByVal wParam As Long, ByVal lparam As Long) As Long
          WindowProc = CallWindowProc(lpPrevWndProc, hw, _
             uMsg, wParam, lparam)
          ' The interior of the control is repainted, but not resized.
        If uMsg = WM_SIZE Or uMsg = WM_PAINT Then
            
            If frmQuestionarioPntAlerta.form_quest = "pnt" Then
          
                frmQuestionarioPntAlerta.cmbResposta.Width = frmQuestionarioPntAlerta.flexPntAlerta.CellWidth
                frmQuestionarioPntAlerta.cmbResposta.Left = frmQuestionarioPntAlerta.flexPntAlerta.CellLeft + _
                frmQuestionarioPntAlerta.flexPntAlerta.Left
                frmQuestionarioPntAlerta.cmbResposta.Top = frmQuestionarioPntAlerta.flexPntAlerta.CellTop + _
                frmQuestionarioPntAlerta.flexPntAlerta.Top


            Else
            
                frmQuestionarioSinistro.cmbResposta.Width = frmQuestionarioSinistro.flexQuest.CellWidth
                frmQuestionarioSinistro.cmbResposta.Left = frmQuestionarioSinistro.flexQuest.CellLeft + _
                frmQuestionarioSinistro.flexQuest.Left
                frmQuestionarioSinistro.cmbResposta.Top = frmQuestionarioSinistro.flexQuest.CellTop + _
                frmQuestionarioSinistro.flexQuest.Top
                            
            End If
        End If
      End Function

