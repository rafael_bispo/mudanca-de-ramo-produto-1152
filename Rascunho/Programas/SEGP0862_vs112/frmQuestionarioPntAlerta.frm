VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "msflxgrd.ocx"
Begin VB.Form frmQuestionarioPntAlerta 
   BackColor       =   &H0000FFFF&
   Caption         =   "Question�rio de Ponto de Alerta"
   ClientHeight    =   7335
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   10875
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   ScaleHeight     =   7335
   ScaleWidth      =   10875
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame frBotoes 
      Height          =   975
      Left            =   240
      TabIndex        =   1
      Top             =   6120
      Width           =   10335
      Begin VB.CommandButton btnAplicar 
         Caption         =   "Aplicar"
         Height          =   360
         Left            =   9000
         TabIndex        =   2
         Top             =   360
         Width           =   990
      End
   End
   Begin VB.Frame frQuestionario 
      Caption         =   "Question�rio"
      Height          =   5655
      Left            =   240
      TabIndex        =   0
      Top             =   240
      Width           =   10335
      Begin VB.ComboBox cmbResposta 
         Height          =   315
         Left            =   2205
         Style           =   2  'Dropdown List
         TabIndex        =   4
         Top             =   1530
         Width           =   1935
      End
      Begin MSFlexGridLib.MSFlexGrid flexPntAlerta 
         Height          =   5055
         Left            =   240
         TabIndex        =   3
         Top             =   360
         Width           =   9855
         _ExtentX        =   17383
         _ExtentY        =   8916
         _Version        =   393216
         Cols            =   7
         FormatString    =   "Seq | C�digo | Pergunta | Resposta"
      End
   End
End
Attribute VB_Name = "frmQuestionarioPntAlerta"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Option Explicit

Private iTecnicoId As Integer
Private rsQuest As Recordset
Public form_quest As String


Private Sub btnAplicar_Click()

    AplicarPontoAlertaSinistro

End Sub

Private Sub cmbResposta_LostFocus()
    cmbResposta.Visible = False
End Sub

Private Sub Form_Load()


    form_quest = "pnt"
'    gHW = flexPntAlerta.hwnd
'    Hook   ' Start checking messages.
    flexPntAlerta.AllowUserResizing = flexResizeNone
    flexPntAlerta.Cols = 7
    
              
    flexPntAlerta.RowHeightMin = cmbResposta.Height
    cmbResposta.Visible = False
    cmbResposta.ZOrder (0)
    cmbResposta.Width = flexPntAlerta.CellWidth
    
    ' Load the ComboBox's list.
    cmbResposta.AddItem "V"
    cmbResposta.AddItem "F"
      
    
    ConsultarQuestionarioAlerta
    
End Sub


Private Sub ConsultarQuestionarioAlerta()

        Dim i As Integer
        Dim SQL As String
        Dim rsQuest As ADODB.Recordset
        
           'Cleber Sardo - INC000004254383 - Erro no agravamento - 29/01/2014 - In�cio
           'SQL = "exec SEGURO_FRAUDE_DB..SPFS0247_SPS " & sinistro_id
            SQL = "exec SEGURO_FRAUDE_DB..SPFS0247_SPS " & IIf(Not IsNumeric(Trim(sinistro_id)), 0, sinistro_id)
           'Cleber Sardo - INC000004254383 - Erro no agravamento - 29/01/2014 - Fim
        
               
        Set rsQuest = ExecutarSQL(gsSIGLASISTEMA, _
                                                glAmbiente_id, _
                                                App.Title, _
                                                App.FileDescription, _
                                                SQL, _
                                                True)
            
              
                
                i = 0
                
            Do While Not rsQuest.EOF
                
                rsQuest.MoveNext
                i = i + 1
                
            Loop
                
            If i > 0 Then
                rsQuest.MoveFirst
            End If
            
            flexPntAlerta.Rows = i + 1
            
            i = 1
                
            Do While Not rsQuest.EOF
            
                flexPntAlerta.TextMatrix(i, 0) = i
                flexPntAlerta.TextMatrix(i, 1) = rsQuest("CODIGO")
                flexPntAlerta.TextMatrix(i, 2) = rsQuest("DESCRICAO")
                flexPntAlerta.TextMatrix(i, 3) = ""
                flexPntAlerta.TextMatrix(i, 4) = rsQuest("PONTO_ALERTA_ID")
                flexPntAlerta.TextMatrix(i, 5) = rsQuest("PESO_RAMO")
                flexPntAlerta.TextMatrix(i, 6) = rsQuest("HIERARQUIA")
                
                flexPntAlerta.WordWrap = True
                flexPntAlerta.RowHeight(i) = 700
                
                i = i + 1
                
                rsQuest.MoveNext
            Loop
            
            flexPntAlerta.ColWidth(1) = 1380
            flexPntAlerta.ColWidth(2) = 7130
            flexPntAlerta.ColWidth(4) = 0
            flexPntAlerta.ColWidth(5) = 0
            flexPntAlerta.ColWidth(6) = 0
            
                      
        
                     
    End Sub

    
     Private Sub AplicarPontoAlertaSinistro()

        Dim i As Integer

            If flexPntAlerta.Rows - 1 > 0 Then

              For i = 1 To flexPntAlerta.Rows - 1
              
                If flexPntAlerta.TextMatrix(i, 3) = "" Then
                      MsgBox ("Favor Responder a Pergunta " & flexPntAlerta.TextMatrix(i, 1) & "!")
                      Exit Sub
                End If

                          InserirPontoAlertaSinistro flexPntAlerta.TextMatrix(i, 1), _
                                                    1, _
                                                    flexPntAlerta.TextMatrix(i, 4), _
                                                    flexPntAlerta.TextMatrix(i, 3), _
                                                    flexPntAlerta.TextMatrix(i, 5), _
                                                    flexPntAlerta.TextMatrix(i, 6)
                                                    
                                                    

                Next

                 
            End If

      
      Me.Hide
      
        Dim SQL As String
        Dim rsConsultarQuestionario As ADODB.Recordset
        SQL = "exec SEGS9411_SPS " & frmDocumentos.Produto_id & ", " & frmDocumentos.evento_sinistro_id
                
        Set rsConsultarQuestionario = ExecutarSQL(gsSIGLASISTEMA, _
                                                glAmbiente_id, _
                                                App.Title, _
                                                App.FileDescription, _
                                                SQL, _
                                                True)
                
        For i = 1 To flexPntAlerta.Rows - 1
        
            flexPntAlerta.TextMatrix(i, 3) = ""
                
        Next
        
        'If rsConsultarQuestionario.EOF Then
            
            Me.Hide
            frmAvisosRealizados.cmdOK.Enabled = True
            frmAvisosRealizados.cmdPontoDeAlerta.Enabled = False
            frmAvisosRealizados.Show
        'Else
        
        '    Me.Hide
        '    frmQuestionarioSinistro.Show
            
        'End If
      
    End Sub
    
     Private Sub InserirPontoAlertaSinistro(strCodigo As String, intEtapaId As Integer, _
                                            lngPontoAlertaId As Integer, strResposta As String, _
                                            intPeso As Integer, strHierarquia As String)
     
     Dim intValor As Integer
     Dim intNota As Integer
 
 
     If strResposta = "V" Then
         intValor = intPeso
         intNota = intPeso
     Else
         intValor = 0
         intNota = 0
     End If
     
     
'     Select Case strCodigo
'
'            Case "AL_CS_SI_084"
'
'                If strResposta = "V" Then
'                    intValor = 3
'                    intNota = intValor * intPeso
'                Else
'                    intValor = 0
'                    intNota = 0
'                End If

'            Case "AL_CS_SI_093"

'                If strResposta = "V" Then
'                    intValor = 5
'                    intNota = intValor * intPeso
'                Else
'                    intValor = 0
'                    intNota = 0
'                End If

'            Case "AL_CS_SI_094"

'                If strResposta = "V" Then
'                    intValor = 1
'                    intNota = intValor * intPeso
'                Else
'                    intValor = 0
'                    intNota = 0
'                End If

'            Case "AL_CS_SI_095"

'                If strResposta = "V" Then
'                    intValor = 3
'                    intNota = 0
'                Else
'                    intValor = 0
'                    intNota = 0
'                End If
                
'                Case "AL_CS_SI_096"

'               If strResposta = "V" Then
'                    intValor = 5
'                    intNota = 0
'                Else
'                    intValor = 0
'                    intNota = 0
'                End If
               
'    End Select
    
    Dim rsQuest As New ADODB.Recordset
    Dim sSQL As String
    
    sSQL = "exec SEGURO_FRAUDE_DB..SPFS0246_SPI " & CStr(intEtapaId)
    sSQL = sSQL & ", " & lngPontoAlertaId
    sSQL = sSQL & ", " & sinistro_id
    sSQL = sSQL & ", " & intValor
    sSQL = sSQL & ", " & intNota
    sSQL = sSQL & ", '" & strHierarquia & "'"
    sSQL = sSQL & ", NULL "
    sSQL = sSQL & ", '" & cUserName & "'"
    sSQL = sSQL & ", 'S'"
    
    
    ExecutarSQL gsSIGLASISTEMA, _
                    glAmbiente_id, _
                    App.Title, _
                    App.FileDescription, _
                    sSQL, _
                    False
     
     End Sub
     
      Private Sub flexPntAlerta_MouseUp(Button As Integer, _
         Shift As Integer, x As Single, y As Single)
          If flexPntAlerta.CellWidth < 0 Then Exit Sub
          Static CurrentWidth As Single
          ' Check to see if the Cell's width has changed.
          If flexPntAlerta.CellWidth <> CurrentWidth Then
              cmbResposta.Width = flexPntAlerta.CellWidth
              CurrentWidth = flexPntAlerta.CellWidth
              
          End If
      End Sub
      
      Private Sub flexPntAlerta_Click()
          ' Position and size the ComboBox, then show it.
          If flexPntAlerta.CellWidth < 0 Then Exit Sub
          cmbResposta.Visible = False
          cmbResposta.Width = flexPntAlerta.CellWidth
          cmbResposta.Left = flexPntAlerta.CellLeft + flexPntAlerta.Left
          cmbResposta.Top = flexPntAlerta.CellTop + flexPntAlerta.Top
          
            If flexPntAlerta.CellLeft = "8955" Then
            
                cmbResposta.Visible = True
            
            Else
                
                cmbResposta.Visible = False
            
            End If
        
      End Sub

Private Sub cmbResposta_Click()

          ' Place the selected item into the Cell and hide the ComboBox.
          flexPntAlerta.Text = cmbResposta.Text
          cmbResposta.Visible = False
          
End Sub

