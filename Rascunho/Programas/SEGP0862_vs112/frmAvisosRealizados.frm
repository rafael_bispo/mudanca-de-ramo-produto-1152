VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "mscomctl.ocx"
Begin VB.Form frmAvisosRealizados 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Sinistro Avisado"
   ClientHeight    =   3360
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6600
   Icon            =   "frmAvisosRealizados.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3360
   ScaleWidth      =   6600
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdPontoDeAlerta 
      Caption         =   "Ponto de Alerta"
      Height          =   420
      Left            =   3600
      TabIndex        =   5
      Top             =   2550
      Width           =   1500
   End
   Begin VB.Frame Frame1 
      Height          =   2025
      Left            =   120
      TabIndex        =   3
      Top             =   480
      Width           =   6405
      Begin MSFlexGridLib.MSFlexGrid grdPropAvis 
         Height          =   1785
         Left            =   90
         TabIndex        =   4
         Top             =   180
         Width           =   6255
         _ExtentX        =   11033
         _ExtentY        =   3149
         _Version        =   393216
         Cols            =   3
         FormatString    =   "Sinistro AB           |Proposta AB    |Produto                                                           "
      End
   End
   Begin MSComctlLib.StatusBar stbQtd 
      Align           =   2  'Align Bottom
      Height          =   345
      Left            =   0
      TabIndex        =   1
      Top             =   3015
      Width           =   6600
      _ExtentX        =   11642
      _ExtentY        =   609
      Style           =   1
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
         EndProperty
      EndProperty
   End
   Begin VB.CommandButton cmdOK 
      Caption         =   "Documentos ..."
      Height          =   420
      Left            =   5190
      TabIndex        =   0
      Top             =   2520
      Width           =   1275
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      Caption         =   "Aviso de Sinistro efetuado com sucesso!"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   150
      TabIndex        =   2
      Top             =   120
      Width           =   6375
   End
End
Attribute VB_Name = "frmAvisosRealizados"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public sinistro_id As String
Public Proposta_id As String
Public produto_nome As String


Private Sub cmdPontoDeAlerta_Click()
    Me.Hide
    frmQuestionarioPntAlerta.Show
End Sub

Private Sub cmdOK_Click()
    Me.Hide
    frmDocumentos.Show
End Sub

Public Sub Inicia()
    If sProdutoAgricola = "INC_OCORRENCIA" Then 'asouza
        Label4 = "Inclus�o de Ocorr�ncia de Sinistro gerado "
    Else
        Label4 = "Aviso de Sinistro AB gerado "
    End If
    lblSinistro.Caption = CStr(sinistro_id)

End Sub

Private Sub Form_Load()

    Dim SQLl As String
    Dim rsPntAlerta As ADODB.Recordset
   
   'Cleber Sardo - INC000004254383 - Erro no agravamento - 29/01/2014 - In�cio
   'SQLl = "exec SEGURO_FRAUDE_DB..SPFS0247_SPS " & sinistro_id
    SQLl = "exec SEGURO_FRAUDE_DB..SPFS0247_SPS " & IIf(Not IsNumeric(Trim(sinistro_id)), 0, sinistro_id)
   'Cleber Sardo - INC000004254383 - Erro no agravamento - 29/01/2014 - Fim
    
    Set rsPntAlerta = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, SQLl, True)
    
    If Not rsPntAlerta.EOF Then
        cmdPontoDeAlerta.Enabled = True
        cmdOK.Enabled = False
    End If
    
End Sub

Private Sub Form_Unload(Cancel As Integer)
    If Not ForcaUnload Then
        Cancel = 1
    End If
End Sub

