VERSION 5.00
Object = "{C932BA88-4374-101B-A56C-00AA003668DC}#1.1#0"; "MSMASK32.OCX"
Object = "{629074EB-AE57-4B76-8AF4-1B62557ED9A6}#1.0#0"; "GridDinamico.ocx"
Begin VB.Form frmConsulta 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "SEGP0862 - Aviso de Sinistro pela CA"
   ClientHeight    =   8085
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   11385
   Icon            =   "frmConsulta.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8085
   ScaleWidth      =   11385
   StartUpPosition =   1  'CenterOwner
   Begin VB.CommandButton cmdVoltar 
      Caption         =   "<< Voltar"
      Height          =   420
      Left            =   7245
      TabIndex        =   7
      Top             =   7605
      Width           =   1275
   End
   Begin VB.CommandButton cmdContinuar 
      Caption         =   "Continuar >>"
      Height          =   420
      Left            =   8640
      TabIndex        =   6
      Top             =   7605
      Width           =   1275
   End
   Begin VB.CommandButton cmdSair 
      Caption         =   "Sair"
      Height          =   420
      Left            =   10035
      TabIndex        =   5
      Top             =   7605
      Width           =   1275
   End
   Begin VB.Frame Frame2 
      Height          =   6165
      Left            =   90
      TabIndex        =   4
      Top             =   1230
      Width           =   11115
      Begin GridFrancisco.GridDinamico grdResultadoPesquisa 
         Height          =   5745
         Left            =   120
         TabIndex        =   8
         Top             =   240
         Width           =   10845
         _ExtentX        =   19129
         _ExtentY        =   10134
         BorderStyle     =   1
         AllowUserResizing=   3
         EditData        =   0   'False
         Highlight       =   1
         ShowTip         =   0   'False
         SortOnHeader    =   0   'False
         BackColor       =   -2147483643
         BackColorBkg    =   -2147483633
         BackColorFixed  =   -2147483633
         BackColorSel    =   -2147483635
         FixedCols       =   1
         FixedRows       =   1
         FocusRect       =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty FontFixed {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   -2147483640
         ForeColorFixed  =   -2147483630
         ForeColorSel    =   -2147483634
         GridColor       =   -2147483630
         GridColorFixed  =   12632256
         GridLine        =   1
         GridLinesFixed  =   2
         MousePointer    =   0
         Redraw          =   -1  'True
         Rows            =   2
         TextStyle       =   0
         TextStyleFixed  =   0
         Cols            =   2
         RowHeightMin    =   0
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Pesquisa do Sinistrado"
      Height          =   1050
      Left            =   90
      TabIndex        =   1
      Top             =   120
      Width           =   11100
      Begin MSMask.MaskEdBox txtValorPesquisa 
         Height          =   315
         Left            =   2280
         TabIndex        =   9
         Top             =   480
         Width           =   7215
         _ExtentX        =   12726
         _ExtentY        =   556
         _Version        =   393216
         PromptChar      =   "_"
      End
      Begin VB.ComboBox cboTipoPesquisa 
         Height          =   315
         ItemData        =   "frmConsulta.frx":0442
         Left            =   135
         List            =   "frmConsulta.frx":0458
         Style           =   2  'Dropdown List
         TabIndex        =   0
         Top             =   480
         Width           =   1995
      End
      Begin VB.CommandButton cmdPesquisar 
         Caption         =   "Pesquisar"
         Height          =   420
         Left            =   9690
         TabIndex        =   2
         Top             =   405
         Width           =   1275
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Tipo de Pesquisa"
         Height          =   195
         Left            =   135
         TabIndex        =   3
         Top             =   240
         Width           =   1230
      End
   End
End
Attribute VB_Name = "frmConsulta"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub cboTipoPesquisa_Click()

If cboTipoPesquisa.Text = "C.P.F." Then
    txtValorPesquisa.Mask = "###########"
End If

If cboTipoPesquisa.Text = "C.N.P.J." Then
    txtValorPesquisa.Mask = "##############"
End If

If cboTipoPesquisa.Text <> "C.P.F." And cboTipoPesquisa.Text <> "C.N.P.J." Then
    txtValorPesquisa.Mask = ""
    txtValorPesquisa.Text = ""
End If

If cboTipoPesquisa.ListIndex <> -1 Then
    txtValorPesquisa.SetFocus
End If

End Sub

Private Sub cmdContinuar_Click()
    
    If grdResultadoPesquisa.Rows > 1 Then
    
        'CARREGAR O COMBO DE EVENTO
        Call MontaComboEvento(frmEvento.cboEvento)
        Me.Hide
        frmEvento.Show
    Else
        bSemProposta = True
        frmDadosCliente.Show
    End If
End Sub



Private Sub cmdPesquisar_Click()
    'MousePointer = 11
    
    'LimparGrid
    If cboTipoPesquisa.ListIndex = -1 Then
        MsgBox "Escolha um crit�rio de pesquisa!", vbCritical
        cboTipoPesquisa.SetFocus
        Exit Sub
    End If
    If Len(Trim(txtValorPesquisa.Text)) = 0 Then
        txtValorPesquisa.SetFocus
        Exit Sub
    End If
    
    Pesquisar
    
    MousePointer = 0
    If grdResultadoPesquisa.Rows = 1 Then
        Me.Hide
        frmClienteNaoEncontrado.Show
    End If
    
End Sub

Private Sub Form_Activate()
    bSemProposta = False

End Sub

Private Sub Form_Load()
    
    CentraFrm Me
    montacabecalhoGrid
    txtValorPesquisa.Text = ""
    cboTipoPesquisa.ListIndex = -1
End Sub

Private Sub Form_Unload(Cancel As Integer)
    End
End Sub

Private Sub cmdVoltar_Click()
    Shell "segp0794.exe " & Command
    End
End Sub

Private Sub cmdSair_Click()
    Me.Hide
    Sair Me
End Sub

Private Sub Pesquisar()
    On Error GoTo TratarErro
    Dim boleano As Boolean
    Dim Rs As ADODB.Recordset
    Dim SQL As String
    
    SQL = ""
    Select Case cboTipoPesquisa.Text
        
        Case "C.P.F."
            If Not IsNumeric(txtValorPesquisa.Text) Then
                MsgBox "O valor deve ser num�rico e com 11 d�gitos."
                txtValorPesquisa.SetFocus
                Exit Sub
            End If
            
            'SQL = SQL & " select    'Nome' = c.nome, "
            'SQL = SQL & "           'CPF/CGC' = pf.cpf, "
            'SQL = SQL & "           c.cliente_id, "
            'SQL = SQL & "           c.seg_cliente"
            'SQL = SQL & " from      cliente_tb c (nolock), "
            'SQL = SQL & "           pessoa_fisica_tb pf (nolock)"
            'SQL = SQL & " where     c.cliente_id = pf.pf_cliente_id"
            'SQL = SQL & " and       pf.cpf = '" & txtValorPesquisa.Text & "' "
            'SQL = SQL & " group by c.nome, pf.cpf, c.cliente_id,c.seg_cliente"
            
        
        Case "C.N.P.J."
            If Not IsNumeric(txtValorPesquisa.Text) Then
                MsgBox "O valor deve ser num�rico e com 14 d�gitos."
                txtValorPesquisa.SetFocus
                Exit Sub
            End If
            'SQL = SQL & " select    'Nome' = c.nome, "
            'SQL = SQL & "           'CPF/CGC' = pj.cgc, "
            'SQL = SQL & "           c.cliente_id, "
            'SQL = SQL & "           c.seg_cliente"
            'SQL = SQL & " from      cliente_tb c (nolock), "
            'SQL = SQL & "           pessoa_juridica_tb pj (nolock)"
            'SQL = SQL & " where     c.cliente_id = pj.pj_cliente_id"
            'SQL = SQL & " and       pj.cgc = '" & txtValorPesquisa.Text & "' "
            'SQL = SQL & "  group by c.nome, c.cliente_id, pj.cgc,c.seg_cliente"
        
        Case "Nome do Cliente"
            If Len(Trim(txtValorPesquisa.Text)) = 0 Then
                MsgBox "Informar o valor de pesquisa."
                txtValorPesquisa.SetFocus
                Exit Sub
            End If
            'SQL = SQL & " select    'Nome' = c.nome, "
            'SQL = SQL & "           'CPF/CGC' = pf.cpf, "
            'SQL = SQL & "           c.cliente_id, "
            'SQL = SQL & "           c.seg_cliente"
            'SQL = SQL & " from      cliente_tb c (nolock), "
            'SQL = SQL & "           pessoa_fisica_tb pf (nolock)"
            'SQL = SQL & " where     c.cliente_id = pf.pf_cliente_id"
            'SQL = SQL & " and       c.nome like '" & txtValorPesquisa.Text & "%' "
            'SQL = SQL & " group by c.nome, c.cliente_id, pf.cpf,c.seg_cliente"
            'SQL = SQL & " UNION"
            'SQL = SQL & " select    'Nome' = c.nome, "
            'SQL = SQL & "           'CPF/CGC' = pj.cgc, "
            'SQL = SQL & "           c.cliente_id, "
            'SQL = SQL & "           c.seg_cliente"
            'SQL = SQL & " from      cliente_tb c (nolock), "
            'SQL = SQL & "           pessoa_juridica_tb pj (nolock)"
            'SQL = SQL & " where     c.cliente_id = pj.pj_cliente_id"
            'SQL = SQL & " and       c.nome like '" & txtValorPesquisa.Text & "%' "
            'SQL = SQL & " group by c.nome, c.cliente_id, pj.cgc,c.seg_cliente"
        
        Case "Proposta AB"
            If Len(Trim(txtValorPesquisa.Text)) = 0 Then
                MsgBox "Informar a proposta AB."
                txtValorPesquisa.SetFocus
                Exit Sub
            End If
            
            'SQL = SQL & " select    'Nome' = c.nome, "
            'SQL = SQL & "           'CPF/CGC' = pf.cpf, "
            'SQL = SQL & "           c.cliente_id, "
            'SQL = SQL & "           c.seg_cliente"
            'SQL = SQL & " from      cliente_tb c (nolock), "
            'SQL = SQL & "           pessoa_fisica_tb pf (nolock),"
            'SQL = SQL & "           proposta_tb p (nolock)"
            'SQL = SQL & " where     c.cliente_id = pf.pf_cliente_id"
            'SQL = SQL & " and       c.cliente_id = p.prop_cliente_id"
            'SQL = SQL & " and       p.proposta_id = " & txtValorPesquisa.Text
            'SQL = SQL & " group by c.nome, pf.cpf, c.cliente_id, c.seg_cliente"
            'SQL = SQL & " UNION "
            'SQL = SQL & " select    'Nome' = c.nome, "
            'SQL = SQL & "           'CPF/CGC' = pj.cgc, "
            'SQL = SQL & "           c.cliente_id, "
            'SQL = SQL & "           c.seg_cliente"
            'SQL = SQL & " from      cliente_tb c (nolock), "
            'SQL = SQL & "           pessoa_juridica_tb pj (nolock),"
            'SQL = SQL & "           proposta_tb p (nolock)"
            'SQL = SQL & " where     c.cliente_id = pj.pj_cliente_id"
            'SQL = SQL & " and       c.cliente_id = p.prop_cliente_id"
            'SQL = SQL & " and       p.proposta_id = " & txtValorPesquisa.Text
            'SQL = SQL & " group by c.nome, pj.cgc, c.cliente_id, c.seg_cliente"
            
        Case "Proposta BB"
            MsgBox "Essa op��o ainda n�o foi implantada!", vbCritical
            Exit Sub
            
        Case "Ap�lice"
            MsgBox "Essa op��o ainda n�o foi implantada!", vbCritical
            Exit Sub
            
        Case "Aviso Sinistro AB"
        
            If Len(Trim(txtValorPesquisa.Text)) = 0 Then
                MsgBox "Informar o sinistro AB."
                txtValorPesquisa.SetFocus
                Exit Sub
            End If
            
            'SQL = SQL & " select    'Nome' = c.nome, "
            'SQL = SQL & "           'CPF/CGC' = pf.cpf, "
            'SQL = SQL & "           c.cliente_id, "
            'SQL = SQL & "           c.seg_cliente"
            'SQL = SQL & " from      cliente_tb c (nolock), "
            'SQL = SQL & "           pessoa_fisica_tb pf (nolock),"
            'SQL = SQL & "           proposta_tb p (nolock), "
            'SQL = SQL & "           sinistro_tb s (nolock) "
            'SQL = SQL & " where     c.cliente_id = pf.pf_cliente_id"
            'SQL = SQL & " and       c.cliente_id = p.prop_cliente_id"
            'SQL = SQL & " and       p.proposta_id = s.proposta_id "
            'SQL = SQL & " and       s.sinistro_id = " & txtValorPesquisa.Text
            'SQL = SQL & " group by c.nome, pf.cpf, c.cliente_id, c.seg_cliente"

        Case "Aviso Sinistro BB"
        
            If Len(Trim(txtValorPesquisa.Text)) = 0 Then
                MsgBox "Informar o sinistro BB."
                txtValorPesquisa.SetFocus
                Exit Sub
            End If
            
            'SQL = SQL & " select    'Nome' = c.nome, "
            'SQL = SQL & "           'CPF/CGC' = pf.cpf, "
            'SQL = SQL & "           c.cliente_id, "
            'SQL = SQL & "           c.seg_cliente"
            'SQL = SQL & " from      cliente_tb c (nolock), "
            'SQL = SQL & "           pessoa_fisica_tb pf (nolock),"
            'SQL = SQL & "           proposta_tb p (nolock), "
            'SQL = SQL & "           sinistro_tb s (nolock), "
            'SQL = SQL & "           sinistro_bb_tb d (nolock)" & vbCrLf
            'SQL = SQL & " where     c.cliente_id = pf.pf_cliente_id"
            'SQL = SQL & " and       c.cliente_id = p.prop_cliente_id"
            'SQL = SQL & " and       p.proposta_id = s.proposta_id "
            'SQL = SQL & " and       s.sinistro_id = d.sinistro_id "
            'SQL = SQL & " and       d.dt_fim_vigencia is null "
            'SQL = SQL & " and       d.sinistro_bb = " & txtValorPesquisa.Text
            'SQL = SQL & " group by c.nome, pf.cpf, c.cliente_id, c.seg_cliente"
    End Select
    
        SQL = "exec segs8394_sps '" & txtValorPesquisa.Text & "', '" & cboTipoPesquisa.Text & "'"
    
     Set Rs = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, SQL, True)
    
    boleano = grdResultadoPesquisa.PopulaGrid(Rs)
        
    
    Exit Sub
    
TratarErro:
    Call TratarErro("PesquisarPorCPF", "M�dulo de Aviso de Sinistro")
End Sub

Public Sub montacabecalhoGrid()
    With grdResultadoPesquisa
        .Cols = 3
        
        .TextMatrix(0, 0) = "Nome"
        .TextMatrix(0, 1) = "CPF/CGC"
        .TextMatrix(0, 2) = "Cliente_id"
        '.TextMatrix(0, 4) = "Data de Nascimento"
        '.TextMatrix(0, 5) = "Documento"
        '.TextMatrix(0, 6) = "Tipo Doc."
        '.TextMatrix(0, 7) = "Expedi��o Doc."
        '.TextMatrix(0, 8) = "Data Emiss�o Doc."
        .AutoFit H�brido
    End With
    
End Sub

