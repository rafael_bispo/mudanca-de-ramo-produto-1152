Attribute VB_Name = "modLocal"
Option Explicit

Global glConexao As Long
Global oSABL0100 As Object

Global bTemporariaAviso As Boolean
Global bTemporariaQuestionario  As Boolean

Global sProdutoAgricola As String 'asouza
Global iEventoId As Integer

Global Const sAbertura = "ABERTURA DE SINISTRO"
Global Const sInclusaoOcorrencia = "INCLUS�O DE OCORR�NCIA DE SEGURO AGR�COLA"
Global Const sReabertura = "REABERTURA"

Public Type CoberturaAtingida
    tp_cobertura_id As Long
    Descricao As String
    valor_prejuizo As Double
    valor_estimativa As Double
    IS As Double
    texto_franquia As String
End Type

Public Type Franquia
    tp_cobertura_id As Long
    texto_franquia As String
    
End Type

Global Const SIGLA_PROGRAMA = "SEGP0862"
Global Const tipo_ramo = 2
Global Const SEM_PRODUTO = "Sem produto"

Global Const Evento_bb_id = 1100 'evento da CA
Global Const entidade_id = 12
Global Const canal_comunicacao = 3
Global processa_reintegracao_is As String
Global ssituacao_aviso As String
Global lista_avisos As String
Const REGRA_SINISTRO_AGRICOLA = 1

Public iSequenciaObjetos As Integer  'Vari�vel para definir a sequ�ncia dos objetos
Public bRespostaSaida As Boolean

Public bSemProposta As Boolean
Public bCorretorBB As Boolean

Public sinistro_id As String
Public sinistro_bb_id As String

Public ForcaUnload As Boolean
Public sCPF_CNPJ_Cliente As String
Public dProposta_id As Double
Public lCliente_id As Long
Public lSubramo_id As Long
Public iRamo_id As Integer

Public sDescricaoRamo As String
Public Apolice_id As String
Public iCod_Obj_segurado As Integer

Public iCD_PRD As Integer
Public iCD_MDLD As Integer
Public lCD_ITEM_MDLD As Long
Public lNR_CTR_SGRO As Long
Public lNR_VRS_EDS As Long
Public dVal_Informado As Double   ' Total das coberturas estimadas

Public sSinistro_id As String
Public sSinistro_bb As String
Public lEvento_Id As Long

Public coberturas() As CoberturaAtingida
Public iTp_Log_Sinistro() As String  'Array que guarda o porque do sinistro estar gravado com situa��o "L" (Log)
Public bExisteTpLogSinistro As Boolean ' verifica se foi feito algum log

Public Evento_Segbr_id As Long
Public Localizacao As String

Public CobertAtingidas() As Integer
Public AvisoSinistro As Object

'Public iQuestionarioProdutoId As Integer
Public produtoQuestionario As Integer
Public Const TEMPORARIAAVISO = "#aviso_sinistro_tb"
Public Const TEMPORARIAQUESTIONARIO = "#questionario_aviso_sinistro_tb"


Public propostas_nao_avisar As String
Public bSinistro As Boolean
Public click_tela As Boolean
 ' inicio Aviso de Sinistro APP (Agro Digital)
 Public num_protocolo As String
 ' fim Aviso de Sinistro APP (Agro Digital)

Public Sub FechaForm(Formulario As Form)
    ForcaUnload = True
    Unload Formulario
    ForcaUnload = False
End Sub
Public Sub Sair(Formulario As Form)
    
    frmSair.Show vbModal
    If bRespostaSaida = True Then
        FinalizarAplicacao
    Else
        Formulario.Show
    End If
    
End Sub


Public Sub CarregaMunicipio(cmbMunicipio As Object)
    Dim SQL As String
    Dim Rs As ADODB.Recordset
    
    SQL = ""
    SQL = SQL & " select   municipio_id, estado, nome"
    SQL = SQL & " from     municipio_tb with (nolock)"
    SQL = SQL & " order by  nome"
    
    Set Rs = ExecutarSQL(gsSIGLASISTEMA, _
                        glAmbiente_id, _
                        App.Title, _
                        App.FileDescription, _
                        SQL, True)
    
    With cmbMunicipio
        .Clear
        If Not Rs.EOF Then
           While Not Rs.EOF
                 .AddItem Trim(Rs(2))
                 .ItemData(.NewIndex) = Rs(0)
                 Rs.MoveNext
           Wend
        End If
        Rs.Close
    End With

End Sub

Public Sub CarregaUF(cmbUF As Object)

    Dim SQL As String
    Dim Rs As ADODB.Recordset
    
'AKIO.OKUNO - INICIO - 17860335 - 10/06/2013
'    SQL = ""
'    SQL = SQL & " select   distinct(estado)"
'    SQL = SQL & " from     municipio_tb with (nolock)"
'    SQL = SQL & " order by estado"
    SQL = ""
    SQL = SQL & "Select Estado                                        " & vbNewLine
    SQL = SQL & "  From Seguros_Db.Dbo.UF_Tb      UF_Tb with (NoLock) " & vbNewLine
'AKIO.OKUNO - FIM - 17860335 - 10/06/2013
    
    Set Rs = ExecutarSQL(gsSIGLASISTEMA, _
                        glAmbiente_id, _
                        App.Title, _
                        App.FileDescription, _
                        SQL, True)
    
    With cmbUF
        .Clear
        
        If Not Rs.EOF Then
           While Not Rs.EOF
              If Rs(0) <> "--" Then  'Cleber Sardo - In�cio - INC000004428002 - Data 23/10/2014 - Inibe a sele��o
                 .AddItem Rs(0)
              End If                 'Cleber Sardo - Fim - INC000004428002 - Data 23/10/2014
                 Rs.MoveNext
           Wend
        End If
        Rs.Close
    End With
    
End Sub
Public Sub CarregaTipoTel(cmbTelefone As Object)
    
    With cmbTelefone
        .Clear
        .AddItem "Residencial"
        .ItemData(.NewIndex) = 1
        .AddItem "Comercial"
        .ItemData(.NewIndex) = 2
        .AddItem "Celular"
        .ItemData(.NewIndex) = 3
        .AddItem "Fax"
        .ItemData(.NewIndex) = 4
        .AddItem "Recado"
        .ItemData(.NewIndex) = 5
    End With
    
End Sub

'Public Sub CarregaLocalRisco(grdLocalRisco As Object, proposta_id As Double, dtOcorrencia As String)
'Dim bRetornoPesquisa As Boolean
'Dim sql As String
'Dim endosso_id As Integer
'Dim rc As ADODB.Recordset
'
'       sql = "set nocount on exec obtem_local_risco_sps " & proposta_id & ", '" & dtOcorrencia & "'"
'
'       Set rc = ExecutarSQL(gsSIGLASISTEMA, _
'                            glAmbiente_id, _
'                            App.Title, _
'                            App.FileDescription, _
'                            sql, _
'                            True)
'
'       bRetornoPesquisa = grdLocalRisco.PopulaGrid(rc)
'
'
'End Sub

Public Sub LimpaTudo()
Dim i As Long

    frmAvisosRealizados.cmdOk.Enabled = False
    frmAvisosRealizados.cmdPontoDeAlerta.Enabled = True

    dProposta_id = 0
    lCliente_id = 0
    lSubramo_id = 0
    iRamo_id = 0
    bSemProposta = False
    bCorretorBB = False
    lEvento_Id = 0
    Apolice_id = ""
    sDescricaoRamo = ""
    
    With frmConsulta
        .cboTipoPesquisa.ListIndex = -1
        .txtValorPesquisa.Text = ""
       
    End With
    

    With frmEvento
        .mskDataOcorrencia.Text = "__/__/____"
        .mskHora.Text = "__:__"
        .cboEvento.ListIndex = -1
        For i = 0 To 9
            .txtExigencia(i).Text = ""
        Next
        
        .txtContato.Text = ""
        .txtLocalRisco.Text = ""
        .txtBairro.Text = ""
        .cmbMunicipio.ListIndex = -1
        .txtCEP.Text = "_____-___"
        .cmbUF.ListIndex = -1
        For i = 0 To 1
            .mskDDD(i).Text = "__"
'24/05/12 : Nova-Mauro Vianna: Alterado para 9 d�gitos de telefone
'            .mskTelefone(i).Text = "____-____"
             .mskTelefone(i).Text = "_____-____"
            .cmbTipoTelefone(i).ListIndex = -1
        Next
    End With
    Call FechaForm(frmEvento)
    
       
    With frmMotivoReanalise
        For i = 0 To 4
            .txtMotivo(i).Text = ""
        Next
    End With
    Call FechaForm(frmMotivoReanalise)
    
    With frmSolicitante
        .txtNomeSolicitante.Text = ""
        .txtEnderecoSolicitante.Text = ""
        .txtBairroSolicitante.Text = ""
        .cmbMunicipio.ListIndex = -1
        .txtCEPSolicitante.Text = "_____-___"
        .txtEmailSolicitante.Text = ""
        .cmbUF.ListIndex = -1
        .Municipio_id = 0
        For i = 0 To 4
          .mskDDD(i).Text = "__"
          '30/05/12 : Nova-Mauro Vianna: Alterado para 9 d�gitos de telefone
          '.mskTelefone(i).Text = "____-____"
          .mskTelefone(i).Text = "_____-____"
          .txtRamal(i).Text = ""
          .cmbTipoTelefone(i).ListIndex = -1
        Next i
    End With
    Call FechaForm(frmSolicitante)
    
    With frmAgencia
        .txtPesquisaAgencia.Text = ""
        .cboEstado.ListIndex = -1
        .cboMunicipio.ListIndex = -1
        .txtCodigoAgencia.Text = ""
        .txtDV.Text = ""
        .txtNomeAgencia.Text = ""
        .txtEndereco.Text = ""
        .txtBairro.Text = ""
        .txtMunicipio.Text = ""
        .txtUF.Text = ""
        '.cmdPesquisarAgencia_Click
    End With
    Call FechaForm(frmAgencia)
    
    With frmOutrosSeguros
        .txtApolice.Text = ""
        .txtNomeSeguradora.Text = ""
        .grdOutrosSeg.Clear
    End With
    Call FechaForm(frmOutrosSeguros)
    
    With frmAviso
       '.txtProposta = ""
       '.txtProduto = ""
       '.txtProposta_bb = ""
       .txtReanalise = ""
       '.txtNome = ""
       .txtNomeAgencia = ""
       .txtCodigoAgencia = ""
       .txtDV = ""
       '.txtCPF = ""
       '.txtNomeProduto = ""
       .txtEnderecoAgencia = ""
       .txtBairroAgencia = ""
       .txtMunicipioAgencia = ""
       .txtUFAgencia = ""
       '.txtApolice = ""
       '.txtRamo = ""
       .txtDtOcorrencia.Text = ""
       .mskHora.Text = ""
       .txtReanalise = ""
       .lblSinistro.Visible = True
       .txtSinistro = ""
       '.cboEvento.ListIndex = -1
       '.cboSubEvento.ListIndex = -1
       For i = 0 To 4
            .txtExigencia(i).Text = ""
       Next
       
       .txtNomeSolicitante.Text = ""
       .txtEnderecoSolicitante.Text = ""
       .txtBairroSolicitante.Text = ""
       .txtCEPSolicitante.Text = ""
       .txtEmailSolicitante.Text = ""
       '.txtGrauParentesco = ""
       .txtMunicipioSolicitante = ""
       .txtUFSolicitante = ""
       
        For i = 0 To 4
          .txtDDD(i).Text = ""
          .txtTelefone(i).Text = ""
          .txtRamal(i).Text = ""
          .txtTipoTelefone(i).Text = ""
        Next i
    End With
    Call FechaForm(frmAviso)
    
    With frmAvisosRealizados
        .sinistro_id = ""
        .Proposta_id = ""
        .produto_nome = ""
    End With
    Call FechaForm(frmAvisosRealizados)
    
    With frmDocumentos
        .produto_id = ""
        .ramo_id = ""
        .subramo_id = ""
        .evento_sinistro_id = 0
    End With
    Call FechaForm(frmDocumentos)
    iSequenciaObjetos = 1
End Sub




Public Sub MontaComboEvento(cboEvento As Object)
Dim SQL As String
Dim rsPesquisa As ADODB.Recordset
    
'AKIO.OKUNO - INICIO - 17860335 - 10/06/2013
'        SQL = ""
'        SQL = SQL & " select    distinct es.evento_sinistro_id, es.nome"
'        SQL = SQL & " from      evento_sinistro_ramo_tb esr with (nolock),"
'        SQL = SQL & "           ramo_tb r with (nolock),"
'        SQL = SQL & "           evento_sinistro_tb es with (nolock),"
'        SQL = SQL & "           produto_estimativa_sinistro_tb pes with (nolock)"
'        SQL = SQL & " Where     esr.ramo_id = r.ramo_id"
'        SQL = SQL & " and       esr.evento_sinistro_id = es.evento_sinistro_id"
'        SQL = SQL & " and       pes.evento_sinistro_id = esr.evento_sinistro_id"
'        SQL = SQL & " and       r.tp_ramo_id = " & tipo_ramo
'        SQL = SQL & " order by  es.nome"
        SQL = ""
        SQL = SQL & "Set NoCount On                                                                                               " & vbNewLine
        SQL = SQL & "Declare @DataHoraServidor                DateTime                                                            " & vbNewLine
        SQL = SQL & "Declare @Evento_Sinistro_Ramo            Table                                                               " & vbNewLine
        SQL = SQL & "      ( Evento_Sinistro_ID               Int)                                                                " & vbNewLine
        SQL = SQL & "                                                                                                             " & vbNewLine
        SQL = SQL & "Declare @Produto_Estimativa_Sinistro     Table                                                               " & vbNewLine
        SQL = SQL & "      ( Evento_Sinistro_ID               Int)                                                                " & vbNewLine
        SQL = SQL & "                                                                                                             " & vbNewLine
        SQL = SQL & "                                                                                                             " & vbNewLine
        SQL = SQL & "Insert into @Evento_Sinistro_Ramo                                                                            " & vbNewLine
        SQL = SQL & "Select Distinct Evento_Sinistro_ID                                                                           " & vbNewLine
        SQL = SQL & "  From Seguros_db.Dbo.Evento_Sinistro_Ramo_Tb            Evento_Sinistro_Ramo_Tb with (NoLock)               " & vbNewLine
        SQL = SQL & "  Join Seguros_db.Dbo.Ramo_Tb                            Ramo_Tb with (NoLock)                               " & vbNewLine
        SQL = SQL & "    on Ramo_Tb.Ramo_ID                                   = Evento_Sinistro_Ramo_Tb.Ramo_ID                   " & vbNewLine
        SQL = SQL & " Where Ramo_Tb.Tp_Ramo_ID                                = " & tipo_ramo & "                                 " & vbNewLine
        SQL = SQL & "                                                                                                             " & vbNewLine
        SQL = SQL & "Insert into @Produto_Estimativa_Sinistro                                                                     " & vbNewLine
        SQL = SQL & "Select Distinct Evento_Sinistro_Ramo.Evento_Sinistro_ID                                                      " & vbNewLine
        SQL = SQL & "  From Seguros_Db.Dbo.Produto_Estimativa_Sinistro_Tb     Produto_Estimativa_Sinistro_Tb With (NoLock)        " & vbNewLine
        SQL = SQL & "  Join @Evento_Sinistro_Ramo                             Evento_Sinistro_Ramo                                " & vbNewLine
        SQL = SQL & "    on Evento_Sinistro_Ramo.Evento_Sinistro_ID           = Produto_Estimativa_Sinistro_Tb.Evento_Sinistro_ID " & vbNewLine
        SQL = SQL & "                                                                                                             " & vbNewLine
        SQL = SQL & "Select Evento_Sinistro_Tb.Evento_Sinistro_ID                                                                 " & vbNewLine
        SQL = SQL & "     , Evento_Sinistro_Tb.Nome                                                                               " & vbNewLine
        SQL = SQL & "  From Seguros_Db.Dbo.Evento_Sinistro_Tb                 Evento_Sinistro_Tb with (NoLock)                    " & vbNewLine
        SQL = SQL & "  Join @Produto_Estimativa_Sinistro                      Produto_Estimativa_Sinistro                         " & vbNewLine
        SQL = SQL & "    on Produto_Estimativa_Sinistro.Evento_Sinistro_ID    = Evento_Sinistro_Tb.Evento_Sinistro_ID             " & vbNewLine
        SQL = SQL & " Order by Nome                                                                                               " & vbNewLine
'AKIO.OKUNO - FIM - 17860335 - 10/06/2013
        
        
        Set rsPesquisa = ExecutarSQL(gsSIGLASISTEMA, _
                                    glAmbiente_id, _
                                    App.Title, _
                                    App.FileDescription, _
                                    SQL, True)

  
    With cboEvento
        .Clear
        
        If Not rsPesquisa.EOF Then
           While Not rsPesquisa.EOF
            'INICIO MU00416967 - Melhorias_SEGP0794_Aviso_Sinistro
            If (rsPesquisa(0) <> 28 And rsPesquisa(0) <> 99 And rsPesquisa(0) <> 100 And rsPesquisa(0) <> 104 _
                And rsPesquisa(0) <> 278 And rsPesquisa(0) <> 426 And rsPesquisa(0) <> 9301 And rsPesquisa(0) <> 277) Then
                 .AddItem UCase(rsPesquisa(1))
                 .ItemData(.NewIndex) = rsPesquisa(0)
            End If 'FIM MU00416967 - Melhorias_SEGP0794_Aviso_Sinistro
                 rsPesquisa.MoveNext
        Wend
           'Inclui evento generico
           '.AddItem "CAUSA GEN�RICA PARA SINISTRO"
           '.ItemData(.NewIndex) = 9999
        End If
        rsPesquisa.Close
    End With
End Sub

Public Sub ConsultarDadosApolice(Proposta As Double, _
                        ByRef apolice As String, _
                        ByRef Ramo As Integer, _
                        ByRef sucursal As String, _
                        ByRef seguradora As String, _
                        ByRef Moeda As Integer, _
                        Optional ByRef PropostaBB As Long, _
                        Optional ByRef produtoId As Integer)
Dim SQL As String
Dim Rs As ADODB.Recordset

'Demanda 17905311 - 15/04/2014 - In�cio
Dim iProduto_id As Integer
Dim sSituacao As String
'Demanda 17905311 - 15/04/2014 - Fim
    
    If Proposta = 0 Then
        apolice = ""
        Ramo = 0
        sucursal = 0
        seguradora = 6785
        Moeda = 790
        PropostaBB = 0
        produtoId = 0
        Exit Sub
    End If
    
    SQL = ""
    SQL = SQL & " select a.dt_inicio_vigencia, a.dt_fim_vigencia, a.apolice_id, a.ramo_id, a.sucursal_seguradora_id, a.seguradora_cod_susep, a.seguro_moeda_id, a.proposta_bb, b.produto_id "
    SQL = SQL & " from  proposta_adesao_tb a with (nolock), proposta_tb b with (nolock)"
    SQL = SQL & " where a.proposta_id = b.proposta_id and a.proposta_id = " & Proposta
        
    Set Rs = ExecutarSQL(gsSIGLASISTEMA, _
                        glAmbiente_id, _
                        App.Title, _
                        App.FileDescription, _
                        SQL, True)
    If Rs.EOF Then
        Rs.Close
        
        SQL = ""
        SQL = SQL & " select a.dt_inicio_vigencia, a.dt_fim_vigencia, a.apolice_id, a.ramo_id, a.sucursal_seguradora_id, a.seguradora_cod_susep, b.seguro_moeda_id, b.proposta_bb, c.produto_id"
        SQL = SQL & " from  apolice_tb a with (nolock)"
        SQL = SQL & " inner join proposta_fechada_tb b with (nolock) "
        SQL = SQL & " on a.proposta_id = b.proposta_id "
        SQL = SQL & " inner join proposta_tb c with  (nolock) "
        SQL = SQL & " on b.proposta_id = c.proposta_id "
        SQL = SQL & " where a.proposta_id = " & Proposta
            
        Set Rs = ExecutarSQL(gsSIGLASISTEMA, _
                            glAmbiente_id, _
                            App.Title, _
                            App.FileDescription, _
                            SQL, True)
    End If
    
    If Not Rs.EOF Then
        apolice = Rs(2)
        Ramo = Rs(3)
        sucursal = Rs(4)
        seguradora = Rs(5)
        Moeda = Rs(6)
        PropostaBB = IIf(IsNull(Rs(7)), 0, Rs(7))
        produtoId = Rs(8)
    Else
        'Demanda 17905311 - 15/04/2014 - In�cio
        Rs.Close
        
        SQL = ""
        SQL = SQL & "Select A.produto_id, Upper(A.situacao), A.ramo_id, IsNull(B.proposta_bb,0)"
        SQL = SQL & " From proposta_tb A with (nolock)"
        SQL = SQL & " Left Join proposta_fechada_tb B with (nolock)"
        SQL = SQL & "     On A.proposta_id = B.proposta_id"
        SQL = SQL & " Where A.proposta_id = " & Proposta
        
        Set Rs = ExecutarSQL(gsSIGLASISTEMA, _
                            glAmbiente_id, _
                            App.Title, _
                            App.FileDescription, _
                            SQL, True)
        
        If Not Rs.EOF Then
            iProduto_id = Rs(0)
            sSituacao = Rs(1)
        End If
    
        'add produto 1240 -> 00421597-seguro-faturamento-pecuario
        If (iProduto_id = 1152 Or iProduto_id = 1204 Or iProduto_id = 1240) And (sSituacao = "E" Or sSituacao = "A") Then
            Ramo = Rs(2)
            PropostaBB = Rs(3)
            apolice = ""
            sucursal = 0
            seguradora = 6785
            Moeda = 790
            produtoId = iProduto_id
        Else
        'Demanda 17905311 - 15/04/2014 - Fim

            apolice = ""
            sucursal = 0
            seguradora = 6785
            Moeda = 790
            PropostaBB = ""
            produtoId = ""
            'Exit Sub

        'Demanda 17905311 - 15/04/2014 - In�cio
        End If
        'Demanda 17905311 - 15/04/2014 - Fim

    End If
    
    Rs.Close
End Sub

Public Sub ConsultarDadosProposta(nProposta As Double, oObjeto As Object, ByRef nSinistro As String, indice As Integer)
Dim SQL As String
Dim Rs As ADODB.Recordset
Dim rsALS As ADODB.Recordset
Dim sCoberturasJaInseridas As String
Dim cod_sinistrado_atual As Integer
Dim i As Long
'Dim indice As Long
Dim lVlrEstimativa As Double
Dim Obs(10) As String

    
   SQL = ""
   SQL = " select a.apolice_id, " & vbCrLf & _
         "     a.ramo_id, a.sucursal_seguradora_id, a.seguradora_cod_susep, " & vbCrLf & _
         "     a.seguro_moeda_id, a.proposta_bb, b.produto_id, 'nome_produto' = p.nome, b.subramo_id, b.situacao " & vbCrLf & _
         " from  proposta_adesao_tb a with (nolock), proposta_tb b with (nolock), produto_tb p with (nolock) " & vbCrLf & _
         " where a.proposta_id = b.proposta_id and b.produto_id = p.produto_id and a.proposta_id = " & nProposta & vbCrLf & _
         " union " & vbCrLf
   SQL = SQL & " select a.apolice_id, " & vbCrLf & _
         "     a.ramo_id, a.sucursal_seguradora_id, a.seguradora_cod_susep, " & vbCrLf & _
         "     b.seguro_moeda_id, b.proposta_bb, c.produto_id, 'nome_produto' = p.nome, c.subramo_id, c.situacao " & vbCrLf & _
         " from  apolice_tb a with (nolock)" & vbCrLf & _
         " inner join proposta_fechada_tb b with (nolock) " & vbCrLf & _
         "     on a.proposta_id = b.proposta_id " & vbCrLf & _
         " inner join proposta_tb c with (nolock) " & vbCrLf & _
         "     on b.proposta_id = c.proposta_id " & vbCrLf & _
         " inner join produto_tb p with (nolock) " & vbCrLf & _
         "     on c.produto_id = p.produto_id " & vbCrLf & _
         " where a.proposta_id = " & nProposta & vbCrLf
        
   Set Rs = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, SQL, True)

   If Not Rs.EOF Then
      With oObjeto
         .Proposta_id = nProposta
         .Apolice_id = Rs("apolice_id")
         .ramo_id = Rs("ramo_id")
         .Sucursal_id = Rs("sucursal_seguradora_id")
         .Moeda = Rs("seguro_moeda_id")
         .PropostaBB = Rs("proposta_bb")
         .produto_id = Rs("produto_id")
         .NomeProduto = Rs("nome_produto")
         .subramo_id = Rs("subramo_id")
        'Ricardo Toledo (Confitec) : 22/07/2011 : busca os campos CD_PRD, CD_MDLD e CD_ITEM_MDLD diretamente da ALS_OPERACAO_DB..CTR_SGRO : inicio
         'Set rsALS = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, "exec obtem_produto_sps 1, " & Rs("produto_id"), True)
         'If Not rsALS.EOF Then
         '   .CD_PRD = rsALS("CD_PRD")
         '   .CD_MDLD = rsALS("CD_MDLD")
         '   .CD_ITEM_MDLD = rsALS("CD_ITEM_MDLD")
         'End If
         .CD_PRD = iCD_PRD
         .CD_MDLD = iCD_MDLD
         .CD_ITEM_MDLD = lCD_ITEM_MDLD
        'Ricardo Toledo (Confitec) : 22/07/2011 : busca os campos CD_PRD, CD_MDLD e CD_ITEM_MDLD diretamente da ALS_OPERACAO_DB..CTR_SGRO : fim
         .NR_CTR_SGRO = lNR_CTR_SGRO
         .NR_VRS_EDS = lNR_VRS_EDS
         
         If bSemProposta Then
            ssituacao_aviso = "N"
            .tp_log_sinistro_id = 2
         End If
         
         If Rs("situacao") = "E" Then ''em estudo
            ssituacao_aviso = "L"
            .tp_log_sinistro_id = 1
         Else
            ssituacao_aviso = "N"
         End If
         
         If Not bSinistro Then
            .sinistro_id = .Busca_Numero_Sinistro(CStr(.ramo_id), CStr(.Sucursal_id), .Seguradora_id)
            .NumRemessa = .Busca_Numero_Remessa
         End If
         nSinistro = .sinistro_id
         
         sCoberturasJaInseridas = ""
         dVal_Informado = 0
         '---Insere o objeto sinistrado e as coberturas afetadas
         .ReiniciaObjetoSinistrado
         cod_sinistrado_atual = .IncluirObjetoSinistrado(Avisos(indice).ObjetoSegurado, "")
         
        'Se  n�o, soma todos os prejuizos por cobertura para guardar no tabel�o
        lVlrEstimativa = Avisos(indice).valor_prejuizo
        dVal_Informado = dVal_Informado + Avisos(indice).valor_prejuizo

    
        'carrega as coberturas na DLL
        If Not .IncluirCobertura(Avisos(indice).Cobertura, _
                                Avisos(indice).DescricaoCobertura, _
                                CDbl(lVlrEstimativa), _
                                cod_sinistrado_atual, _
                                CDbl(lVlrEstimativa), _
                                Avisos(indice).DescricaoCobertura) Then
          'GoTo TrataErro
        End If
        
         'ReDim Preserve CobertAtingidas(1 To i) As Integer
      
         
         .Val_Informado = dVal_Informado
         
         Call CarregaObs(Obs)
         Call .IncluirObservacoes(Obs)
         
         If dProposta_id = nProposta Then
            'Para as informa��es finais das propostas
            frmAvisosRealizados.sinistro_id = .sinistro_id
            frmAvisosRealizados.Proposta_id = IIf(bSemProposta, "Sem proposta", .Proposta_id)
            frmAvisosRealizados.produto_nome = IIf(bSemProposta, "Sem produto espec�fico", .NomeProduto)
            
            frmDocumentos.produto_id = IIf(.produto_id = "", 0, .produto_id)
            frmDocumentos.ramo_id = .ramo_id
            frmDocumentos.subramo_id = lSubramo_id
            frmDocumentos.evento_sinistro_id = .Causa_id
            frmDocumentos.sCoberturas = sCoberturasJaInseridas
         End If
         
         .SituacaoAviso = ssituacao_aviso
      
      End With
       
   End If
   Rs.Close
End Sub

Private Sub CarregaObs(ByRef Obs() As String)
Dim i As Integer
    For i = 0 To 9
        Obs(i) = frmAviso.txtExigencia(i)
    Next i
End Sub


Public Sub ConsultarDadosApoliceRamo(dProposta_id As Double, ByRef apolice As String, ByRef Ramo As String)
Dim SQL As String
Dim Rs As ADODB.Recordset
    
    SQL = ""
    SQL = SQL & " select a.apolice_id, a.ramo_id,b.nome"
    SQL = SQL & " from  proposta_adesao_tb a with (nolock)"
    SQL = SQL & " inner join ramo_tb b with (nolock) "
    SQL = SQL & " on a.ramo_id = b.ramo_id "
    SQL = SQL & " where a.proposta_id = " & dProposta_id
        
    Set Rs = ExecutarSQL(gsSIGLASISTEMA, _
                        glAmbiente_id, _
                        App.Title, _
                        App.FileDescription, _
                        SQL, True)
    If Rs.EOF Then
        Rs.Close
        
        SQL = ""
        SQL = SQL & " select a.apolice_id, a.ramo_id, c.nome"
        SQL = SQL & " from  apolice_tb a with (nolock)"
        SQL = SQL & " inner join ramo_tb c with (nolock) "
        SQL = SQL & " on a.ramo_id = c.ramo_id "
        SQL = SQL & " inner join proposta_fechada_tb b with (nolock) "
        SQL = SQL & " on a.proposta_id = b.proposta_id "
        SQL = SQL & " where a.proposta_id = " & dProposta_id
            
        Set Rs = ExecutarSQL(gsSIGLASISTEMA, _
                            glAmbiente_id, _
                            App.Title, _
                            App.FileDescription, _
                            SQL, True)
    End If
    
    If Not Rs.EOF Then
        apolice = Rs(0)
        Ramo = Rs(1) & " - " & Rs(2)
    Else
        apolice = ""
        Ramo = 0 & " - " & "Sem ramo"
    End If
    
    Rs.Close
End Sub

Public Function ExisteProdutoBB(produto_id As Long) As Boolean
Dim SQL As String
Dim Rs As ADODB.Recordset

SQL = "set nocount on exec obtem_produto_sps 1," & produto_id

Set Rs = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, SQL, True)

If Not Rs.EOF Then
    ExisteProdutoBB = True
    iCD_PRD = Rs(0)
    iCD_MDLD = Rs(1)
    lCD_ITEM_MDLD = Rs(2)
Else
    ExisteProdutoBB = False
    iCD_PRD = 0
    iCD_MDLD = 0
    lCD_ITEM_MDLD = 0
End If
Rs.Close

End Function

Public Sub SairAplicacao()
    bRespostaSaida = True
    End
End Sub

Public Function VerificaPropostaBB(Proposta_bb As Long, dt_Ocorrencia As String) As Boolean
Dim SQL As String
Dim Rs As ADODB.Recordset


SQL = "set nocount on exec obtem_Versao_Endosso_sps " & Proposta_bb & ", " & dt_Ocorrencia

Set Rs = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, SQL, True)

If Not Rs.EOF Then
    VerificaPropostaBB = True
    lNR_VRS_EDS = Rs("NR_VRS_EDS")
    'Alterado por Cleber - Data: 28/11/2006 - Flow 189587
    'Segundo o Junior estava buscando o campo errado.
    'lNR_CTR_SGRO = rs("NR_CTR_SGRO")
    lNR_CTR_SGRO = Rs("NR_CTR_SGRO_BB")
   
   'Ricardo Toledo (Confitec) : 22/07/2011 : busca os campos CD_PRD, CD_MDLD e CD_ITEM_MDLD diretamente da ALS_OPERACAO_DB..CTR_SGRO : inicio
    iCD_PRD = Rs("CD_PRD")
    iCD_MDLD = Rs("CD_MDLD")
    lCD_ITEM_MDLD = Rs("CD_ITEM_MDLD")
    'Ricardo Toledo (Confitec) : 22/07/2011 : busca os campos CD_PRD, CD_MDLD e CD_ITEM_MDLD diretamente da ALS_OPERACAO_DB..CTR_SGRO : fim

Else
    VerificaPropostaBB = False
    lNR_VRS_EDS = 0
    lNR_CTR_SGRO = 0

   'Ricardo Toledo (Confitec) : 22/07/2011 : busca os campos CD_PRD, CD_MDLD e CD_ITEM_MDLD diretamente da ALS_OPERACAO_DB..CTR_SGRO : inicio
    iCD_PRD = 0
    iCD_MDLD = 0
    lCD_ITEM_MDLD = 0
    'Ricardo Toledo (Confitec) : 22/07/2011 : busca os campos CD_PRD, CD_MDLD e CD_ITEM_MDLD diretamente da ALS_OPERACAO_DB..CTR_SGRO : fim

End If
Rs.Close

End Function


Public Sub PreencheDadosSinistro(sinistro As String)
Dim SQL As String
Dim Rs As ADODB.Recordset
Dim rsPesquisa As ADODB.Recordset
Dim rsDecEvento As ADODB.Recordset

Dim i As Long

    SQL = " set nocount on exec consultar_aviso_sinistro_re_sps " & sinistro
        
    Set Rs = ExecutarSQL(gsSIGLASISTEMA, _
                        glAmbiente_id, _
                        App.Title, _
                        App.FileDescription, _
                        SQL)
    If Not Rs.EOF Then
        With frmSolicitante
            .txtNomeSolicitante.Text = IIf(IsNull(Rs("solicitante")), "", UCase(Rs("solicitante")))
            .txtNomeSolicitante.Text = IIf(IsNull(Rs("solicitante")), "", UCase(Rs("solicitante")))
            .txtEnderecoSolicitante.Text = IIf(IsNull(Rs("endereco")), "", UCase(Rs("endereco")))
            .txtBairroSolicitante.Text = IIf(IsNull(Rs("bairro")), "", UCase(Rs("bairro")))
            If IsNull(Rs("municipio")) Then
                .cmbMunicipio.ListIndex = -1
            Else
                '.cmbMunicipio.Text = UCase(rs("municipio"))
                For i = 0 To .cmbMunicipio.ListCount - 1
                    If .cmbMunicipio.List(i) = Trim(UCase(Rs("municipio"))) Then
                        .cmbMunicipio.ListIndex = i
                        'Flow 199152 - Nakata - 29/12/06
                        'Exit For
                        If .cmbUF.Text = UCase(Rs("estado")) Then
                            Exit For
                        End If
                    End If
                Next
                
            End If
            .cmbUF.Text = IIf(IsNull(Rs("estado")), "", UCase(Rs("estado")))
            
            'Raimundo Belas da Silva Junior
            'GPTI - 03/08/2009
            'Tira a formata��o do campo e insere nulo quando o valor for nulo
            If IsNull(Rs("cep")) Then
            .txtCEPSolicitante.Mask = ""
            .txtCEPSolicitante.Text = ""
            Else
            .txtCEPSolicitante.Text = Format(Rs("cep"), "00000-000")
            End If
            '.txtCEPSolicitante.Text = IIf(IsNull(rs("cep")), "", Format(rs("cep"), "00000-000"))
            'Raimundo - Fim

            
            .mskDDD(0).Text = RetornarDDD(Rs("ddd"))
            .mskDDD(1).Text = RetornarDDD(Rs("ddd1"))
            .mskDDD(2).Text = RetornarDDD(Rs("ddd2"))
            .mskDDD(3).Text = RetornarDDD(Rs("ddd3"))
            .mskDDD(4).Text = RetornarDDD(Rs("ddd4"))
            
            
            '.mskTelefone(0).Text = IIf(IsNull(Rs("telefone")), "____-____", Format(Rs("telefone"), "0000-0000"))
            ' 30/05/12 : Nova - Mauro Vianna : Altera��o para 9 d�gitos de telefone
'            .mskTelefone(0).Text = Format(RetornarTelefone(Rs("telefone")), "0000-0000")
'            .mskTelefone(1).Text = Format(RetornarTelefone(Rs("telefone1")), "0000-0000")
'            .mskTelefone(2).Text = Format(RetornarTelefone(Rs("telefone2")), "0000-0000")
'            .mskTelefone(3).Text = Format(RetornarTelefone(Rs("telefone3")), "0000-0000")
'            .mskTelefone(4).Text = Format(RetornarTelefone(Rs("telefone4")), "0000-0000")
            .mskTelefone(0).Text = Right(" " & Format(RetornarTelefone(Rs("telefone")), "#0000-0000"), 10)
            .mskTelefone(1).Text = Right(" " & Format(RetornarTelefone(Rs("telefone1")), "#0000-0000"), 10)
            .mskTelefone(2).Text = Right(" " & Format(RetornarTelefone(Rs("telefone2")), "#0000-0000"), 10)
            .mskTelefone(3).Text = Right(" " & Format(RetornarTelefone(Rs("telefone3")), "#0000-0000"), 10)
            .mskTelefone(4).Text = Right(" " & Format(RetornarTelefone(Rs("telefone4")), "#0000-0000"), 10)
            
            .txtRamal(0) = IIf(IsNull(Rs("ramal")), "", Rs("ramal"))
            .txtRamal(1) = IIf(IsNull(Rs("ramal1")), "", Rs("ramal1"))
            .txtRamal(2) = IIf(IsNull(Rs("ramal2")), "", Rs("ramal2"))
            .txtRamal(3) = IIf(IsNull(Rs("ramal3")), "", Rs("ramal3"))
            .txtRamal(4) = IIf(IsNull(Rs("ramal4")), "", Rs("ramal4"))
            
            'petrauskas/renato confitec 21/08/2013
            'tratamento para evitar "type mismatch" quando tiver null_string ("")
Dim sAux As String
            sAux = IIf(IsNull(Rs("tp_telefone")), "", Rs("tp_telefone"))
            If sAux <> "" Then .cmbTipoTelefone(0).ListIndex = Val(sAux) Else .cmbTipoTelefone(0).ListIndex = -1
            
            sAux = IIf(IsNull(Rs("tp_telefone")), "", Rs("tp_telefone1"))
            If sAux <> "" Then .cmbTipoTelefone(1).ListIndex = Val(sAux) Else .cmbTipoTelefone(1).ListIndex = -1
            
            sAux = IIf(IsNull(Rs("tp_telefone")), "", Rs("tp_telefone2"))
            If sAux <> "" Then .cmbTipoTelefone(2).ListIndex = Val(sAux) Else .cmbTipoTelefone(2).ListIndex = -1
            
            sAux = IIf(IsNull(Rs("tp_telefone")), "", Rs("tp_telefone3"))
            If sAux <> "" Then .cmbTipoTelefone(3).ListIndex = Val(sAux) Else .cmbTipoTelefone(3).ListIndex = -1
            
            sAux = IIf(IsNull(Rs("tp_telefone")), "", Rs("tp_telefone4"))
            If sAux <> "" Then .cmbTipoTelefone(4).ListIndex = Val(sAux) Else .cmbTipoTelefone(4).ListIndex = -1
            
'            .cmbTipoTelefone(0).ListIndex = IIf(IsNull(Rs("tp_telefone")), -1, Rs("tp_telefone"))
'            .cmbTipoTelefone(1).ListIndex = IIf(IsNull(Rs("tp_telefone1")), -1, Rs("tp_telefone1"))
'            .cmbTipoTelefone(2).ListIndex = IIf(IsNull(Rs("tp_telefone2")), -1, Rs("tp_telefone2"))
'            .cmbTipoTelefone(3).ListIndex = IIf(IsNull(Rs("tp_telefone3")), -1, Rs("tp_telefone3"))
'            .cmbTipoTelefone(4).ListIndex = IIf(IsNull(Rs("tp_telefone4")), -1, Rs("tp_telefone4"))
            
            .txtEmailSolicitante = IIf(IsNull(Rs("email")), "", Rs("email"))
            'If IsNull(Rs("grau_parentesco")) Then
            '    .cmbGrauParentesco.ListIndex = -1
            'Else
                '.cmbGrauParentesco.ItemData = rs("grau_parentesco")
            'End If
        End With
        
        
        With frmAgencia
            If Rs("banco_id") = 1 Then
                If Not IsNull(Rs("agencia_id")) Then
                    .txtPesquisaAgencia.Text = Rs("agencia_id")
                    .cmdPesquisarAgencia_Click
                End If
            End If
        End With
        
        With frmAviso
            '.txtProposta.Text = Rs("proposta_id")
            '.txtProduto.Text = Rs("produto_id")
            '.txtNomeProduto.Text = UCase(Rs("nome_produto"))
            '.txtNome.Text = UCase(Rs("nome_cliente"))
            '.txtCPF.Text = IIf(IsNull(Rs("cpf")), MasCGC(Rs("cgc")), MasCPF(Rs("cpf")))
            
            .txtDtOcorrencia.Text = Format(Rs("dt_ocorrencia_sinistro"), "dd/mm/yyyy")
            '.cboEvento.Text = Rs("evento_sinistro")
            .txtReanalise.Text = "Sim"
            .txtSinistro.Text = sinistro
            .txtSinistro.Visible = True
            .lblSinistro.Visible = True
            
        End With
    End If
    
    
    With frmAviso
        '.GridObjetoSinistrado.Rows = 1
        While Not Rs.EOF
            '.GridObjetoSinistrado.Rows = .GridObjetoSinistrado.Rows + 1
            '.GridObjetoSinistrado.TextMatrix(.GridObjetoSinistrado.Rows - 1, 1) = Rs("cod_objeto_segurado")
            
            SQL = ""
            SQL = SQL & " set nocount on exec obtem_objetos_segurados_sps " & dProposta_id
                
            Set rsPesquisa = ExecutarSQL(gsSIGLASISTEMA, _
                                glAmbiente_id, _
                                App.Title, _
                                App.FileDescription, _
                                SQL, True)
   
    
'            While Not rsPesquisa.EOF
'                If rsPesquisa("cod_objeto_segurado") = rs("cod_objeto_segurado") Then
'
'                    .GridObjetoSinistrado.TextMatrix(.GridObjetoSinistrado.Rows - 1, 3) = rsPesquisa("endereco")
'                    .GridObjetoSinistrado.TextMatrix(.GridObjetoSinistrado.Rows - 1, 4) = rsPesquisa("municipio")
'                    .GridObjetoSinistrado.TextMatrix(.GridObjetoSinistrado.Rows - 1, 5) = rsPesquisa("Bairro")
'                    .GridObjetoSinistrado.TextMatrix(.GridObjetoSinistrado.Rows - 1, 6) = rsPesquisa("CEP")
'                    .GridObjetoSinistrado.TextMatrix(.GridObjetoSinistrado.Rows - 1, 7) = Trim(rsPesquisa("estado"))
'
'                    Exit Sub
'                End If
'                rsPesquisa.MoveNext
'            Wend
'
'            rsPesquisa.Close
'
'            .grdEstimativas.TextMatrix(.GridObjetoSinistrado.Rows - 1, 8) = rs("tp_cobertura_id")
'            .grdEstimativas.TextMatrix(.GridObjetoSinistrado.Rows - 1, 9) = rs("nome_cobertura")
'            .grdEstimativas.TextMatrix(.GridObjetoSinistrado.Rows - 1, 10) = rs("val_estimativa")
'
            Rs.MoveNext
        Wend
        
        Set Rs = Rs.NextRecordset
        
        For i = 0 To 9
            If Rs.EOF Then
                .txtExigencia(i) = ""
                Exit For
            Else
                .txtExigencia(i) = UCase(Rs(0))
            End If
            Rs.MoveNext
        Next i
        
    End With
    Rs.Close
    
End Sub


'Public Sub PreencheConsulta(sinistro As String)
'Dim SQL As String
'Dim rs As ADODB.Recordset
'Dim rsPesquisa As ADODB.Recordset
'Dim rsDecEvento As ADODB.Recordset
'
'Dim i As Long
'
'    SQL = " set nocount on exec consultar_aviso_sinistro_re_sps " & sinistro
'
'    Set rs = ExecutarSQL(gsSIGLASISTEMA, _
'                        glAmbiente_id, _
'                        App.Title, _
'                        App.FileDescription, _
'                        SQL)
'    If Not rs.EOF Then
'        With SEGP0862_7
'
'            .txtNomeSolicitante.Text = IIf(IsNull(rs("solicitante")), "", UCase(rs("solicitante")))
'            .txtEnderecoSolicitante.Text = IIf(IsNull(rs("endereco")), "", UCase(rs("endereco")))
'            .txtBairroSolicitante.Text = IIf(IsNull(rs("bairro")), "", UCase(rs("bairro")))
'            If IsNull(rs("municipio")) Then
'                '.cmbMunicipio.ListIndex = -1
'            Else
'                .txtMunicipioAgencia.Text = UCase(rs("municipio"))
'            End If
'            .txtUFAgencia.Text = IIf(IsNull(rs("estado")), "", UCase(rs("estado")))
'            .txtCEPSolicitante.Text = IIf(IsNull(rs("cep")), "", Format(rs("cep"), "00000-000"))
'
'            .mskDDD(0).Text = IIf(IsNull(rs("ddd")), "__", rs("ddd"))
'            .mskDDD(1).Text = IIf(IsNull(rs("ddd1")), "__", rs("ddd1"))
'            .mskDDD(2).Text = IIf(IsNull(rs("ddd2")), "__", rs("ddd2"))
'            .mskDDD(3).Text = IIf(IsNull(rs("ddd3")), "__", rs("ddd3"))
'            .mskDDD(4).Text = IIf(IsNull(rs("ddd4")), "__", rs("ddd4"))
'
'            .mskTelefone(0).Text = IIf(IsNull(rs("telefone")), "____-____", Format(rs("telefone"), "0000-0000"))
'            .mskTelefone(1) = IIf(IsNull(rs("telefone1")), "____-____", Format(rs("telefone1"), "0000-0000"))
'            .mskTelefone(2) = IIf(IsNull(rs("telefone2")), "____-____", Format(rs("telefone2"), "0000-0000"))
'            .mskTelefone(3) = IIf(IsNull(rs("telefone3")), "____-____", Format(rs("telefone3"), "0000-0000"))
'            .mskTelefone(4) = IIf(IsNull(rs("telefone4")), "____-____", Format(rs("telefone4"), "0000-0000"))
'
'            .txtRamal(0) = IIf(IsNull(rs("ramal")), "", rs("ramal"))
'            .txtRamal(1) = IIf(IsNull(rs("ramal1")), "", rs("ramal1"))
'            .txtRamal(2) = IIf(IsNull(rs("ramal2")), "", rs("ramal2"))
'            .txtRamal(3) = IIf(IsNull(rs("ramal3")), "", rs("ramal3"))
'            .txtRamal(4) = IIf(IsNull(rs("ramal4")), "", rs("ramal4"))
'
'            .cmbTipoTelefone(0).ListIndex = IIf(IsNull(rs("tp_telefone")), -1, rs("tp_telefone"))
'            .cmbTipoTelefone(1).ListIndex = IIf(IsNull(rs("tp_telefone1")), -1, rs("tp_telefone1"))
'            .cmbTipoTelefone(2).ListIndex = IIf(IsNull(rs("tp_telefone2")), -1, rs("tp_telefone2"))
'            .cmbTipoTelefone(3).ListIndex = IIf(IsNull(rs("tp_telefone3")), -1, rs("tp_telefone3"))
'            .cmbTipoTelefone(4).ListIndex = IIf(IsNull(rs("tp_telefone4")), -1, rs("tp_telefone4"))
'
'            .txtEmailSolicitante = IIf(IsNull(rs("email")), "", rs("email"))
'            If IsNull(rs("grau_parentesco")) Then
'                .cmbGrauParentesco.ListIndex = -1
'            Else
'                '.cmbGrauParentesco.ItemData = rs("grau_parentesco")
'            End If
'        End With
'
'
'        With SEGP0862_6
'            If rs("banco_id") = 1 Then
'                If Not IsNull(rs("agencia_id")) Then
'                    .txtPesquisaAgencia.Text = rs("agencia_id")
'                    .cmdPesquisarAgencia_Click
'                End If
'            End If
'        End With
'
'        With SEGP0862_7
'            .txtProposta.Text = rs("proposta_id")
'            .txtProduto.Text = rs("produto_id")
'            .txtNomeProduto.Text = UCase(rs("nome_produto"))
'            .txtNome.Text = UCase(rs("nome_cliente"))
'            .txtCPF.Text = IIf(IsNull(rs("cpf")), MasCGC(rs("cgc")), MasCPF(rs("cpf")))
'
'            .txtDtOcorrencia.Text = Format(rs("dt_ocorrencia_sinistro"), "dd/mm/yyyy")
'            .cboEvento.Text = rs("evento_sinistro")
'            .txtReanalise.Text = "Sim"
'            .txtSinistro.Text = sinistro
'            .txtSinistro.Visible = True
'            .lblSinistro.Visible = True
'
'        End With
'    End If
'
'    'asouza 13.02
'    If sProdutoAgricola = "REANALISE" Then
'        With SEGP0862_2
'            SEGP0862_2.cboCodObjeto.Text = rs("cod_objeto_segurado")
'            SEGP0862_3.mskDataOcorrencia.Text = Format(rs("dt_ocorrencia_sinistro"), "dd/mm/yyyy")
'            If Not IsNull(rs("dt_ocorrencia_sinistro_fim")) Then
'                SEGP0862_3.mskDataOcorrenciaFim.Text = Format(rs("dt_ocorrencia_sinistro_fim"), "dd/mm/yyyy")
'            End If
'        End With
'    End If
'
'    With SEGP0862_7
'        .GridObjetoSinistrado.Rows = 1
'        While Not rs.EOF
'            .GridObjetoSinistrado.Rows = .GridObjetoSinistrado.Rows + 1
'            .GridObjetoSinistrado.TextMatrix(.GridObjetoSinistrado.Rows - 1, 1) = rs("cod_objeto_segurado")
'
'            SQL = ""
'            SQL = SQL & " set nocount on exec obtem_objetos_segurados_sps " & dProposta_id
'
'            Set rsPesquisa = ExecutarSQL(gsSIGLASISTEMA, _
'                                glAmbiente_id, _
'                                App.Title, _
'                                App.FileDescription, _
'                                SQL, True)
'
'
''            While Not rsPesquisa.EOF
''                If rsPesquisa("cod_objeto_segurado") = rs("cod_objeto_segurado") Then
''
''                    .GridObjetoSinistrado.TextMatrix(.GridObjetoSinistrado.Rows - 1, 3) = rsPesquisa("endereco")
''                    .GridObjetoSinistrado.TextMatrix(.GridObjetoSinistrado.Rows - 1, 4) = rsPesquisa("municipio")
''                    .GridObjetoSinistrado.TextMatrix(.GridObjetoSinistrado.Rows - 1, 5) = rsPesquisa("Bairro")
''                    .GridObjetoSinistrado.TextMatrix(.GridObjetoSinistrado.Rows - 1, 6) = rsPesquisa("CEP")
''                    .GridObjetoSinistrado.TextMatrix(.GridObjetoSinistrado.Rows - 1, 7) = Trim(rsPesquisa("estado"))
''
''                    Exit Sub
''                End If
''                rsPesquisa.MoveNext
''            Wend
''
''            rsPesquisa.Close
''
''            .grdEstimativas.TextMatrix(.GridObjetoSinistrado.Rows - 1, 8) = rs("tp_cobertura_id")
''            .grdEstimativas.TextMatrix(.GridObjetoSinistrado.Rows - 1, 9) = rs("nome_cobertura")
''            .grdEstimativas.TextMatrix(.GridObjetoSinistrado.Rows - 1, 10) = rs("val_estimativa")
''
'            rs.MoveNext
'        Wend
'
'        Set rs = rs.NextRecordset
'
'        For i = 0 To 9
'            If rs.EOF Then
'                .txtExigencia(i) = ""
'                Exit For
'            Else
'                .txtExigencia(i) = UCase(rs(0))
'            End If
'            rs.MoveNext
'        Next i
'
'    End With
'    rs.Close
'
'End Sub

Public Sub Obtem_Evento_SEGBR()
Dim rc As ADODB.Recordset
Dim vSql As String

    
    On Error GoTo Trata_Erro
    
    'Preenche as vari�veis para o aviso de sinistro
    vSql = " select evento_SEGBR_id, prox_localizacao_processo " & _
           " from evento_segbr_tb with (nolock)" & _
           " where evento_bb_id = " & Evento_bb_id
    
    Set rc = ExecutarSQL(gsSIGLASISTEMA, _
                            glAmbiente_id, _
                            App.Title, _
                            App.FileDescription, _
                            vSql, _
                            True)
    If Not rc.EOF Then
         Evento_Segbr_id = rc(0)
         Localizacao = rc(1)
    End If
    rc.Close
    
    Exit Sub

Trata_Erro:
    TrataErroGeral "Obtem_Evento_SEGBR", "Aviso Sinistro"
    Exit Sub
End Sub

Public Function AchaIndice(tp_cobertura_id As Integer) As Long
Dim i As Integer
    AchaIndice = 0
    For i = 1 To UBound(coberturas)
        If coberturas(i).tp_cobertura_id = tp_cobertura_id Then
            AchaIndice = i
            Exit For
        End If
    Next i
    
End Function

Public Function verificaSinistro(sinistro As String) As Boolean
Dim rc As ADODB.Recordset
Dim vSql As String

    
    On Error GoTo Trata_Erro
    
    'Preenche as vari�veis para o aviso de sinistro
    vSql = " select situacao " & _
           " from sinistro_tb with (nolock)" & _
           " where sinistro_id = " & sinistro
    
    Set rc = ExecutarSQL(gsSIGLASISTEMA, _
                            glAmbiente_id, _
                            App.Title, _
                            App.FileDescription, _
                            vSql, _
                            True)
    If Not rc.EOF Then
         If rc(0) >= 2 Then verificaSinistro = True
    End If
    
    rc.Close
    
    Exit Function

Trata_Erro:
    TrataErroGeral "verificaSinistro", "Aviso Sinistro"
    Exit Function

End Function
'
''asouza novo
'Public Function VerificaPropostaAgricola(ByVal dProposta As Double, ByVal iProduto As Integer, Optional iIndice As Integer)
'Dim SQL As String
'Dim Rs As ADODB.Recordset
'Dim Rs2 As ADODB.Recordset ' Diego 24/04/2008
'Dim oRegras As Object
'
'Debug.Print "SEGP0862 - modLocal.VerificaPropostaAgricola"
'Debug.Print "Inicio: " & Format(Now(), "hh:mm:ss:ms")
'    sProdutoAgricola = "ABERTURA" 'asouza 16/05/06
'
'    Set oRegras = CreateObject("SEGL0144.cls00385")
'
'    If oRegras.VerificaRegraSinistro(gsSIGLASISTEMA, _
'                                     App.Title, _
'                                     App.FileDescription, _
'                                     glAmbiente_id, _
'                                     cUserName, _
'                                     iProduto, _
'                                     REGRA_SINISTRO_AGRICOLA) Then
'
'        SQL = ""
'        SQL = SQL & " SELECT sinistro_id, situacao,evento_sinistro_id," & vbNewLine
'        SQL = SQL & "        dt_ocorrencia_sinistro, dt_ocorrencia_sinistro_fim" & vbNewLine
'        SQL = SQL & " FROM sinistro_tb with (nolock)"
'        SQL = SQL & " WHERE proposta_id = " & dProposta
'
'
'        Set Rs = ExecutarSQL(gsSIGLASISTEMA, _
'                             glAmbiente_id, _
'                             App.Title, _
'                             App.FileDescription, _
'                             SQL, _
'                             True)
'
'        If Not Rs.EOF Then
'            If Not IsNull(Rs("situacao")) Then
'                If Rs("situacao") = 0 Or Rs("situacao") = 1 Then ' Aberto / Reaberto
'                    sProdutoAgricola = "INC_OCORRENCIA"
'                ElseIf Rs("situacao") = 2 Then ' Encerrado
'                    sProdutoAgricola = "REANALISE"
'                    iEventoId = Rs("evento_sinistro_id")
'                End If
'                sSinistro_id = Rs("sinistro_id")
'            End If
'        ' G&P - Diego Flow 332195 - 24/04/2008
'        'Verifica se existe um aviso na evento_segbr_sinistro_atual_tb
'        Else
'            SQL = ""
'            SQL = SQL & " SELECT *" & vbNewLine        'AKIO.OKUNO - 17860335 - 25/06/2013
'            SQL = SQL & " FROM evento_segbr_sinistro_atual_tb with (nolock) "
'            SQL = SQL & " WHERE proposta_id = " & dProposta
'
'            Set Rs2 = ExecutarSQL(gsSIGLASISTEMA, _
'                             glAmbiente_id, _
'                             App.Title, _
'                             App.FileDescription, _
'                             SQL, _
'                             True)
'            If Not Rs2.EOF Then
'                sProdutoAgricola = "ERRO"
'                MsgBox "J� existe um aviso de sinistro para a proposta " & dProposta & ". Sinistro " & Rs2("sinistro_id"), vbCritical, "Aviso Sinistro  RE"
'                'Caso exista exibe a mensagem de erro.
'            End If
'            Rs2.Close
'        End If
'        Rs.Close
'
'    Else
'        sProdutoAgricola = frmAvisoPropostas.grdResultadoPesquisa.TextMatrix(CInt(iIndice), 1)
'
'    End If
'Debug.Print "Fim   : " & Format(Now(), "hh:mm:ss:ms")
'
'End Function


'AKIO.OKUNO - VerificaPropostaAgricola - 17860335 - 25/06/2013
Public Function VerificaPropostaAgricola(ByVal dProposta As Double, ByVal iProduto As Integer, Optional iIndice As Integer)
    Dim sSQL As String
    Dim Rs As ADODB.Recordset
    Dim RS2 As ADODB.Recordset
    Dim oRegras As Object
    
    Debug.Print "SEGP0862 - modLocal.VerificaPropostaAgricola"
    Debug.Print "Inicio: " & Format(Now(), "hh:mm:ss:ms")
    
    
    'inicio - Aviso de Sinistro APP (Agro Digital)
        sSQL = ""
        
        sSQL = sSQL & "Declare @Dominio             VarChar(1)" & vbNewLine
        sSQL = sSQL & "      , @Produto_ID          Int" & vbNewLine
        sSQL = sSQL & "      , @Tp_Regra_ID         Int" & vbNewLine
        sSQL = sSQL & "      , @Sinistro_ID         Numeric(11)" & vbNewLine
        sSQL = sSQL & "      , @Situacao            Char(1)" & vbNewLine
        sSQL = sSQL & "      , @Evento_Sinistro_ID  Int" & vbNewLine
        sSQL = sSQL & "      , @sProdutoAgricola    Char(20)" & vbNewLine
        sSQL = sSQL & "      , @Proposta_ID         Numeric(9)" & vbNewLine
        sSQL = sSQL & "      , @num_protocolo       Varchar(15)" & vbNewLine
        
        sSQL = sSQL & "Set @Produto_ID          = " & iProduto & vbNewLine
        sSQL = sSQL & "Set @Tp_Regra_ID         = " & REGRA_SINISTRO_AGRICOLA & vbNewLine
        sSQL = sSQL & "Set @Proposta_ID         = " & dProposta & vbNewLine
        sSQL = sSQL & "Set @sProdutoAgricola    = 'ABERTURA'" & vbNewLine
        sSQL = sSQL & "Set @Sinistro_ID         = 0" & vbNewLine
        sSQL = sSQL & "Set @Evento_Sinistro_ID  = 0" & vbNewLine
        sSQL = sSQL & "" & vbNewLine
        sSQL = sSQL & "" & vbNewLine
        sSQL = sSQL & "Select @Dominio                                      = IsNull(Tp_Regra_Dominio_Tb.Codigo_Dominio, Dominio_Padrao) " & vbNewLine
        sSQL = sSQL & "  From Seguros_Db.Dbo.Produto_Regra_Tb               Produto_Regra_Tb With (NoLock)" & vbNewLine
        sSQL = sSQL & "  Join Dado_Basico_Db.Dbo.Tp_regra_Dominio_Tb        Tp_regra_Dominio_Tb With (NoLock)" & vbNewLine
        sSQL = sSQL & "    on Tp_Regra_Dominio_Tb.Tp_Regra_ID               = Produto_Regra_Tb.Tp_Regra_ID" & vbNewLine
        sSQL = sSQL & "   and Tp_Regra_Dominio_Tb.Tp_Regra_Dominio_ID       = Produto_Regra_Tb.Tp_Regra_Dominio_Id" & vbNewLine
        sSQL = sSQL & " Where Produto_ID                                    = @Produto_ID" & vbNewLine
        sSQL = sSQL & "   and Produto_Regra_Tb.Tp_Regra_ID                  = @Tp_Regra_ID" & vbNewLine
        sSQL = sSQL & "" & vbNewLine
        sSQL = sSQL & "If @Dominio = 'S'" & vbNewLine
        sSQL = sSQL & " Begin" & vbNewLine
        sSQL = sSQL & "     Select @num_protocolo =  num_protocolo , @Evento_Sinistro_ID =  Evento_Sinistro_ID" & vbNewLine
        sSQL = sSQL & "     From interface_dados_db..aviso_web_comunicacao_sinistro_tb aviso_web  With (NoLock)" & vbNewLine
        sSQL = sSQL & "     Where aviso_web.Proposta_ID = @Proposta_ID  and" & vbNewLine
        sSQL = sSQL & "     aviso_web.tp_processamento_id = 0 " & vbNewLine
        sSQL = sSQL & "" & vbNewLine
        sSQL = sSQL & "     if (@num_protocolo is not null   or  len(@num_protocolo) > 1 )" & vbNewLine
        sSQL = sSQL & "     Begin" & vbNewLine
        sSQL = sSQL & "         Set @sProdutoAgricola = 'ERRO'" & vbNewLine
        sSQL = sSQL & "     End" & vbNewLine
        sSQL = sSQL & "" & vbNewLine
        sSQL = sSQL & " End" & vbNewLine
        sSQL = sSQL & "Select @sProdutoAgricola" & vbNewLine
        sSQL = sSQL & "     , @Sinistro_ID" & vbNewLine
        sSQL = sSQL & "     , @Evento_Sinistro_ID" & vbNewLine
        sSQL = sSQL & "     , isnull(@num_protocolo, '')" & vbNewLine
        
        Set Rs = ExecutarSQL(gsSIGLASISTEMA _
                            , glAmbiente_id _
                            , App.Title _
                            , App.FileDescription _
                            , sSQL _
                            , True)
    
        sProdutoAgricola = Trim(Rs.Fields(0))
        sSinistro_id = Rs.Fields(1)
        iEventoId = Rs.Fields(2)
        num_protocolo = Rs.Fields(3)
        
        If sProdutoAgricola = "ERRO" Then
            MsgBox "J� existe um aviso de sinistro para a proposta ID " & dProposta & vbNewLine & "N�mero de protocolo : " & Rs.Fields(3), vbExclamation, "Aviso Sinistro RE"
            Rs.Close
            
        Else 'Aviso de Sinistro APP (Agro Digital)
        
            sSQL = ""
            
            sSQL = sSQL & "Declare @Dominio             VarChar(1)" & vbNewLine
            sSQL = sSQL & "      , @Produto_ID          Int" & vbNewLine
            sSQL = sSQL & "      , @Tp_Regra_ID         Int" & vbNewLine
            sSQL = sSQL & "      , @Sinistro_ID         Numeric(11)" & vbNewLine
            sSQL = sSQL & "      , @Situacao            Char(1)" & vbNewLine
            sSQL = sSQL & "      , @Evento_Sinistro_ID  Int" & vbNewLine
            sSQL = sSQL & "      , @sProdutoAgricola    Char(20)" & vbNewLine
            sSQL = sSQL & "      , @Proposta_ID         Numeric(9)" & vbNewLine
            sSQL = sSQL & "" & vbNewLine
            sSQL = sSQL & "" & vbNewLine
            sSQL = sSQL & "Set @Produto_ID          = " & iProduto & vbNewLine
            sSQL = sSQL & "Set @Tp_Regra_ID         = " & REGRA_SINISTRO_AGRICOLA & vbNewLine
            sSQL = sSQL & "Set @Proposta_ID         = " & dProposta & vbNewLine
            sSQL = sSQL & "Set @sProdutoAgricola    = 'ABERTURA'" & vbNewLine
            sSQL = sSQL & "Set @Sinistro_ID         = 0" & vbNewLine
            sSQL = sSQL & "Set @Evento_Sinistro_ID  = 0" & vbNewLine
            sSQL = sSQL & "" & vbNewLine
            sSQL = sSQL & "Select @Dominio                                      = IsNull(Tp_Regra_Dominio_Tb.Codigo_Dominio, Dominio_Padrao)" & vbNewLine
            sSQL = sSQL & "  From Seguros_Db.Dbo.Produto_Regra_Tb               Produto_Regra_Tb With (NoLock)" & vbNewLine
            sSQL = sSQL & "  Join Dado_Basico_Db.Dbo.Tp_regra_Dominio_Tb        Tp_regra_Dominio_Tb With (NoLock)" & vbNewLine
            sSQL = sSQL & "    on Tp_Regra_Dominio_Tb.Tp_Regra_ID               = Produto_Regra_Tb.Tp_Regra_ID" & vbNewLine
            sSQL = sSQL & "   and Tp_Regra_Dominio_Tb.Tp_Regra_Dominio_ID       = Produto_Regra_Tb.Tp_Regra_Dominio_Id" & vbNewLine
            sSQL = sSQL & " Where Produto_ID                                    = @Produto_ID" & vbNewLine
            sSQL = sSQL & "   and Produto_Regra_Tb.Tp_Regra_ID                  = @Tp_Regra_ID" & vbNewLine
            sSQL = sSQL & "" & vbNewLine
            sSQL = sSQL & "If @Dominio = 'S' Begin" & vbNewLine
            sSQL = sSQL & "" & vbNewLine
            sSQL = sSQL & "    Select @Sinistro_ID                              = Sinistro_ID" & vbNewLine
            sSQL = sSQL & "         , @Situacao                                 = Situacao" & vbNewLine
            sSQL = sSQL & "         , @Evento_Sinistro_ID                       = Evento_Sinistro_ID" & vbNewLine
            sSQL = sSQL & "      From Seguros_Db.Dbo.Sinistro_Tb                Sinistro_Tb With (NoLock)" & vbNewLine
            sSQL = sSQL & "     Where Proposta_ID                               = @Proposta_ID" & vbNewLine
            sSQL = sSQL & "" & vbNewLine
            sSQL = sSQL & "    If @@RowCount <> 0 Begin" & vbNewLine
            sSQL = sSQL & "        If @Situacao = '0' or @Situacao = '1' Begin" & vbNewLine
            sSQL = sSQL & "            Set @sProdutoAgricola    = 'INC_OCORRENCIA'" & vbNewLine
            sSQL = sSQL & "            Set @Evento_Sinistro_ID = 0" & vbNewLine
            sSQL = sSQL & "        End" & vbNewLine
            sSQL = sSQL & "        Else Begin" & vbNewLine
            sSQL = sSQL & "            If @Situacao = '2' Begin" & vbNewLine
            sSQL = sSQL & "                Set @sProdutoAgricola = 'REANALISE'" & vbNewLine
            sSQL = sSQL & "            End" & vbNewLine
            sSQL = sSQL & "        End" & vbNewLine
            sSQL = sSQL & "    End" & vbNewLine
            sSQL = sSQL & "    Else Begin" & vbNewLine
            sSQL = sSQL & "        if Exists (Select * From Seguros_Db.Dbo.Evento_SegBR_Sinistro_Atual_Tb Evento_SegBR_Sinistro_Atual_Tb With (NoLock) " & vbNewLine
            sSQL = sSQL & "                      inner join interface_dados_db..aviso_web_comunicacao_sinistro_tb aviso_web With (NoLock) " & vbNewLine
            sSQL = sSQL & "                       on Evento_SegBR_Sinistro_Atual_Tb.proposta_id = aviso_web.proposta_id and aviso_web.tp_processamento_id = 0 " & vbNewLine
            sSQL = sSQL & "                             Where Evento_SegBR_Sinistro_Atual_Tb.proposta_id  = @Proposta_ID ) Begin " & vbNewLine
            sSQL = sSQL & "            Set @sProdutoAgricola = 'ERRO'" & vbNewLine
            sSQL = sSQL & "        End" & vbNewLine
            sSQL = sSQL & "    End" & vbNewLine
            sSQL = sSQL & "End" & vbNewLine
            sSQL = sSQL & "Else Begin" & vbNewLine
            sSQL = sSQL & "    Set @sProdutoAgricola = '" & frmAvisoPropostas.grdResultadoPesquisa.TextMatrix(CInt(iIndice), 1) & "'" & vbNewLine
            sSQL = sSQL & "End" & vbNewLine
            sSQL = sSQL & "Select @sProdutoAgricola" & vbNewLine
            sSQL = sSQL & "     , @Sinistro_ID" & vbNewLine
            sSQL = sSQL & "     , @Evento_Sinistro_ID" & vbNewLine
        
            Set Rs = ExecutarSQL(gsSIGLASISTEMA _
                                , glAmbiente_id _
                                , App.Title _
                                , App.FileDescription _
                                , sSQL _
                                , True)
        
            sProdutoAgricola = Trim(Rs.Fields(0))
            sSinistro_id = Rs.Fields(1)
            iEventoId = Rs.Fields(2)
                        
            If sProdutoAgricola = "ERRO" Then
                MsgBox "J� existe um aviso de sinistro para a proposta " & dProposta & ". Sinistro " & Rs.Fields(1), vbCritical, "Aviso Sinistro  RE"
            End If
            Rs.Close
        End If
    'fim - Aviso de Sinistro APP (Agro Digital)
   

Debug.Print "Fim   : " & Format(Now(), "hh:mm:ss:ms")

End Function

Public Function ExisteEventoSinistroProduto(produto_id As Integer) As Boolean
Dim SQL As String
Dim Rs As ADODB.Recordset

    'Verifica se o evento de sinistro est� cadastrado para o produto
    ExisteEventoSinistroProduto = False
    
    SQL = ""
    SQL = SQL & " select 1 from produto_estimativa_sinistro_tb with (nolock) "
    SQL = SQL & " where produto_id = " & produto_id
        
    Set Rs = ExecutarSQL(gsSIGLASISTEMA, _
                         glAmbiente_id, _
                         App.Title, _
                         App.FileDescription, _
                         SQL, _
                         True)
    
    If Not Rs.EOF Then
        ExisteEventoSinistroProduto = True
    End If
    
    Rs.Close
End Function
Public Function obtemEstado(Municipio_id As Integer, Municipio As String) As String
Dim SQL As String
Dim Rs As ADODB.Recordset

        obtemEstado = ""
        SQL = ""
        SQL = SQL & "SELECT estado "
        SQL = SQL & "FROM municipio_tb with (nolock) "
        SQL = SQL & "WHERE rtrim(nome) = '" & Trim(Municipio) & "'"
        'FLOW 5998569 - Cleiton Silva - Confitec Sistemas - 15/10/2010
        'Alguns Municipios ambiguos possuem municipio id 0, esta retornando UF errada.
        'If municipio_id <> 0 Then
            SQL = SQL & "AND municipio_id = " & Municipio_id
        'End If
        
        Set Rs = ExecutarSQL(gsSIGLASISTEMA, _
                  glAmbiente_id, _
                  App.Title, _
                  App.FileDescription, _
                  SQL, _
                  True)
        If Not Rs.EOF Then
            obtemEstado = Trim(Rs!estado)
        End If
End Function



Public Function ObtemEstimativaAberturaRE(Proposta As Long, _
                                val_is As Double, _
                                val_prejuizo As Double, _
                                evento_sinistro_id As Integer, _
                                SubEvento_sinistro_id As Integer, _
                                tp_cobertura_id As Integer, _
                                dtOcorrenciaSinistro As String) As Currency

Dim SQL                   As String
Dim Rs                    As ADODB.Recordset
Dim ramo_id               As Integer
Dim subramo_id            As Long
Dim produto_id            As Integer

Dim oCalculaEstimativa    As Object

    'Seleciona as informa��es da proposta
SQL = ""
SQL = SQL & " select a.ramo_id, a.subramo_id, a.produto_id "
SQL = SQL & " from proposta_tb a with (nolock) "
SQL = SQL & " where a.proposta_id = " & Proposta
    
                                  
Set Rs = ExecutarSQL(gsSIGLASISTEMA, _
                     glAmbiente_id, _
                     App.Title, _
                     App.FileDescription, _
                     SQL, _
                     True)
If Not Rs.EOF Then
   ramo_id = Rs!ramo_id
   subramo_id = IIf(IsNull(Rs!subramo_id), 0, Rs!subramo_id)
   produto_id = Rs!produto_id
End If
  
Rs.Close


Set oCalculaEstimativa = CreateObject("SEGL0144.cls00326")
   
oCalculaEstimativa.mvarSiglaSistema = gsSIGLASISTEMA
oCalculaEstimativa.mvarSiglaRecurso = App.Title
oCalculaEstimativa.mvarDescricaoRecurso = App.FileDescription
oCalculaEstimativa.mvarAmbienteId = glAmbiente_id

    
 ObtemEstimativaAberturaRE = oCalculaEstimativa.ObtemEstimativaAberturaRE(produto_id, _
                                                               ramo_id, _
                                                               subramo_id, _
                                                               tp_cobertura_id, _
                                                               evento_sinistro_id, _
                                                               SubEvento_sinistro_id, _
                                                               val_is, _
                                                               val_prejuizo)


Set oCalculaEstimativa = Nothing
    

End Function


Public Function Obtem_CPF_CNPJ_Cliente(dProposta_id As Double) As String
Dim SQL As String
Dim Rs As Recordset

Obtem_CPF_CNPJ_Cliente = ""

SQL = "Select isnull(pf.cpf, pj.cgc) from proposta_tb p with (nolock) "
SQL = SQL & " left join pessoa_fisica_tb pf with (nolock)"
SQL = SQL & "  on p.prop_cliente_id = pf.pf_cliente_id "
SQL = SQL & " left join pessoa_juridica_tb pj with (nolock)"
SQL = SQL & "  on p.prop_cliente_id = pj.pj_cliente_id "
SQL = SQL & " where p.proposta_id = " & dProposta_id

Set Rs = ExecutarSQL(gsSIGLASISTEMA, _
                  glAmbiente_id, _
                  App.Title, _
                  App.FileDescription, _
                  SQL, _
                  True)


If Not Rs.EOF Then
        Obtem_CPF_CNPJ_Cliente = Rs(0)
End If
Set Rs = Nothing

End Function



Public Function DescricaoMotivo(motivo_reanalise_sinistro_id As Integer) As String
Dim SQL As String
Dim Rs As Recordset

DescricaoMotivo = ""

SQL = "select descricao from motivo_reanalise_sinistro_tb with (nolock) "
SQL = SQL & " where motivo_reanalise_sinistro_id = " & motivo_reanalise_sinistro_id

Set Rs = ExecutarSQL(gsSIGLASISTEMA, _
                  glAmbiente_id, _
                  App.Title, _
                  App.FileDescription, _
                  SQL, _
                  True)
If Not Rs.EOF Then
    DescricaoMotivo = Rs(0)
End If

End Function

Public Function ObtemMotivosReanalise(tp_ramo_id As Integer) As Recordset
Dim SQL As String
Dim Rs As Recordset


SQL = " select motivo_reanalise_sinistro_id, descricao from motivo_reanalise_sinistro_tb  with (nolock) "
'DEMANDA 3965704
SQL = SQL & " where (tp_ramo_id = " & tipo_ramo
SQL = SQL & " or tp_ramo_id is null) "
SQL = SQL & " and dt_inicio_vigencia <= getdate() "
SQL = SQL & " and (dt_fim_vigencia >= getdate() or dt_fim_vigencia is null)"
SQL = SQL & " order by descricao"

Set ObtemMotivosReanalise = ExecutarSQL(gsSIGLASISTEMA, _
                  glAmbiente_id, _
                  App.Title, _
                  App.FileDescription, _
                  SQL, _
                  True)

End Function

Public Function VerificaReintegracaoIS(produto_id As Integer, ramo_id As Integer) As Boolean
Dim SQL As String
Dim Rs As Recordset


    VerificaReintegracaoIS = False
    
SQL = "select isnull(processa_reintegracao_is, 'N') "
SQL = SQL & " from seguros_db..item_produto_tb  with (nolock) "
SQL = SQL & " where produto_id = " & produto_id
SQL = SQL & " and ramo_id = " & ramo_id

Set Rs = ExecutarSQL(gsSIGLASISTEMA, _
                  glAmbiente_id, _
                  App.Title, _
                  App.FileDescription, _
                  SQL, _
                  True)
If Not Rs.EOF Then

    VerificaReintegracaoIS = IIf(UCase(Rs(0)) = "S", True, False)
End If

End Function
Public Function existeProposta(Proposta_id As Double) As Boolean
Dim Proposta As cProposta
existeProposta = False
For Each Proposta In Propostas
    If Proposta.Proposta = Proposta_id Then
        existeProposta = True
        Exit For
    End If
Next Proposta
End Function

Private Function RetornarDDD(ByVal p_sDDD As Variant) As String
    
    If IsNull(p_sDDD) = True Then
        RetornarDDD = "__"
    ElseIf Val(p_sDDD) = 0 Then
        RetornarDDD = "__"
    Else
        p_sDDD = Format(p_sDDD, "00")
        RetornarDDD = p_sDDD
    
    End If
    
End Function
Private Function RetornarTelefone(ByVal p_sTel As Variant) As String
    
    If IsNull(p_sTel) = True Then
        '30/05/2012 Nova - Mauro Vianna : altera��o de telefone para 9 d�gitos
'        RetornarTelefone = "____-____"
        RetornarTelefone = "_____-____"
    ElseIf Val(p_sTel) = 0 Then
        '30/05/2012 Nova - Mauro Vianna : altera��o de telefone para 9 d�gitos
'        RetornarTelefone = "____-____"
        RetornarTelefone = "_____-____"
    Else
        '30/05/2012 Nova - Mauro Vianna : altera��o de telefone para 9 d�gitos
'        p_sTel = Format(p_sTel, "00000000")
        p_sTel = Format(p_sTel, "#00000000")
        RetornarTelefone = p_sTel
    
    End If
    
End Function

