VERSION 5.00
Object = "{C932BA88-4374-101B-A56C-00AA003668DC}#1.1#0"; "MSMASK32.OCX"
Begin VB.Form frmSolicitante 
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   7155
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   7275
   Icon            =   "frmSolicitante.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7155
   ScaleWidth      =   7275
   StartUpPosition =   2  'CenterScreen
   Begin MSMask.MaskEdBox txtCEPSolicitante 
      Height          =   330
      Left            =   240
      TabIndex        =   5
      Top             =   2520
      Width           =   1575
      _ExtentX        =   2778
      _ExtentY        =   582
      _Version        =   393216
      MaxLength       =   9
      Mask            =   "#####-###"
      PromptChar      =   "_"
   End
   Begin VB.CommandButton cmdVoltar 
      Caption         =   "<< Voltar"
      Height          =   420
      Left            =   3075
      TabIndex        =   29
      Top             =   6600
      Width           =   1275
   End
   Begin VB.CommandButton cmdSair 
      Caption         =   "Sair"
      Height          =   420
      Left            =   5865
      TabIndex        =   31
      Top             =   6600
      Width           =   1275
   End
   Begin VB.CommandButton cmdContinuar 
      Caption         =   "Continuar >>"
      Height          =   420
      Left            =   4440
      TabIndex        =   30
      Top             =   6600
      Width           =   1275
   End
   Begin VB.Frame Frame1 
      Caption         =   "Dados do Solicitante"
      Height          =   6480
      Left            =   0
      TabIndex        =   32
      Top             =   0
      Width           =   7260
      Begin VB.CommandButton cmdSegurado 
         Caption         =   "Segurado"
         Height          =   420
         Left            =   5880
         TabIndex        =   1
         Top             =   480
         Width           =   1155
      End
      Begin VB.CommandButton cmdPesquisarCEP 
         Caption         =   "..."
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   380
         Left            =   1920
         TabIndex        =   6
         Top             =   2520
         Width           =   440
      End
      Begin VB.TextBox txtNomeSolicitante 
         Height          =   330
         Left            =   240
         MaxLength       =   60
         TabIndex        =   0
         Top             =   540
         Width           =   5490
      End
      Begin VB.TextBox txtEmailSolicitante 
         Height          =   330
         Left            =   225
         MaxLength       =   60
         TabIndex        =   28
         ToolTipText     =   "Este email possibilita o envio da lista de documentos para o solicitante."
         Top             =   5280
         Width           =   6810
      End
      Begin VB.TextBox txtEnderecoSolicitante 
         Height          =   330
         Left            =   240
         MaxLength       =   60
         TabIndex        =   2
         Top             =   1165
         Width           =   6810
      End
      Begin VB.TextBox txtBairroSolicitante 
         Height          =   330
         Left            =   240
         MaxLength       =   30
         TabIndex        =   3
         Top             =   1800
         Width           =   2625
      End
      Begin VB.TextBox txtRamal 
         Enabled         =   0   'False
         Height          =   330
         Index           =   0
         Left            =   3045
         MaxLength       =   4
         TabIndex        =   10
         Top             =   3165
         Width           =   915
      End
      Begin VB.ComboBox cmbTipoTelefone 
         Enabled         =   0   'False
         Height          =   315
         Index           =   0
         ItemData        =   "frmSolicitante.frx":0442
         Left            =   4395
         List            =   "frmSolicitante.frx":0455
         Style           =   2  'Dropdown List
         TabIndex        =   11
         Top             =   3165
         Width           =   2670
      End
      Begin VB.TextBox txtRamal 
         Enabled         =   0   'False
         Height          =   330
         Index           =   1
         Left            =   3045
         MaxLength       =   4
         TabIndex        =   14
         Top             =   3525
         Width           =   915
      End
      Begin VB.TextBox txtRamal 
         Enabled         =   0   'False
         Height          =   330
         Index           =   2
         Left            =   3045
         MaxLength       =   4
         TabIndex        =   18
         Top             =   3885
         Width           =   915
      End
      Begin VB.TextBox txtRamal 
         Enabled         =   0   'False
         Height          =   330
         Index           =   3
         Left            =   3045
         MaxLength       =   4
         TabIndex        =   22
         Top             =   4245
         Width           =   915
      End
      Begin VB.TextBox txtRamal 
         Enabled         =   0   'False
         Height          =   330
         Index           =   4
         Left            =   3045
         MaxLength       =   4
         TabIndex        =   26
         Top             =   4605
         Width           =   915
      End
      Begin VB.ComboBox cmbTipoTelefone 
         Enabled         =   0   'False
         Height          =   315
         Index           =   1
         ItemData        =   "frmSolicitante.frx":0487
         Left            =   4395
         List            =   "frmSolicitante.frx":049A
         Style           =   2  'Dropdown List
         TabIndex        =   15
         Top             =   3525
         Width           =   2670
      End
      Begin VB.ComboBox cmbTipoTelefone 
         Enabled         =   0   'False
         Height          =   315
         Index           =   2
         ItemData        =   "frmSolicitante.frx":04CC
         Left            =   4395
         List            =   "frmSolicitante.frx":04DF
         Style           =   2  'Dropdown List
         TabIndex        =   19
         Top             =   3885
         Width           =   2670
      End
      Begin VB.ComboBox cmbTipoTelefone 
         Enabled         =   0   'False
         Height          =   315
         Index           =   3
         ItemData        =   "frmSolicitante.frx":0511
         Left            =   4395
         List            =   "frmSolicitante.frx":0524
         Style           =   2  'Dropdown List
         TabIndex        =   23
         Top             =   4245
         Width           =   2670
      End
      Begin VB.ComboBox cmbTipoTelefone 
         Enabled         =   0   'False
         Height          =   315
         Index           =   4
         ItemData        =   "frmSolicitante.frx":0556
         Left            =   4395
         List            =   "frmSolicitante.frx":0569
         Style           =   2  'Dropdown List
         TabIndex        =   27
         Top             =   4605
         Width           =   2670
      End
      Begin VB.ComboBox cmbMunicipio 
         Height          =   315
         Left            =   3000
         TabIndex        =   7
         Text            =   "cmbMunicipio"
         Top             =   2520
         Width           =   4065
      End
      Begin VB.ComboBox cmbUF 
         Height          =   315
         Left            =   3000
         Sorted          =   -1  'True
         Style           =   2  'Dropdown List
         TabIndex        =   4
         Top             =   1800
         Width           =   825
      End
      Begin MSMask.MaskEdBox mskDDD 
         Height          =   330
         Index           =   0
         Left            =   255
         TabIndex        =   8
         Top             =   3165
         Width           =   555
         _ExtentX        =   979
         _ExtentY        =   582
         _Version        =   393216
         MaxLength       =   2
         Mask            =   "##"
         PromptChar      =   "_"
      End
      Begin MSMask.MaskEdBox mskTelefone 
         Height          =   330
         Index           =   0
         Left            =   1200
         TabIndex        =   9
         Top             =   3165
         Width           =   1410
         _ExtentX        =   2487
         _ExtentY        =   582
         _Version        =   393216
         Enabled         =   0   'False
         MaxLength       =   10
         Mask            =   "9####-####"
         PromptChar      =   "_"
      End
      Begin MSMask.MaskEdBox mskDDD 
         Height          =   330
         Index           =   1
         Left            =   255
         TabIndex        =   12
         Top             =   3525
         Width           =   555
         _ExtentX        =   979
         _ExtentY        =   582
         _Version        =   393216
         MaxLength       =   2
         Mask            =   "##"
         PromptChar      =   "_"
      End
      Begin MSMask.MaskEdBox mskDDD 
         Height          =   330
         Index           =   2
         Left            =   255
         TabIndex        =   16
         Top             =   3885
         Width           =   555
         _ExtentX        =   979
         _ExtentY        =   582
         _Version        =   393216
         MaxLength       =   2
         Mask            =   "##"
         PromptChar      =   "_"
      End
      Begin MSMask.MaskEdBox mskDDD 
         Height          =   330
         Index           =   3
         Left            =   255
         TabIndex        =   20
         Top             =   4245
         Width           =   555
         _ExtentX        =   979
         _ExtentY        =   582
         _Version        =   393216
         MaxLength       =   2
         Mask            =   "##"
         PromptChar      =   "_"
      End
      Begin MSMask.MaskEdBox mskDDD 
         Height          =   330
         Index           =   4
         Left            =   255
         TabIndex        =   24
         Top             =   4605
         Width           =   555
         _ExtentX        =   979
         _ExtentY        =   582
         _Version        =   393216
         MaxLength       =   2
         Mask            =   "##"
         PromptChar      =   "_"
      End
      Begin MSMask.MaskEdBox mskTelefone 
         Height          =   330
         Index           =   1
         Left            =   1200
         TabIndex        =   13
         Top             =   3525
         Width           =   1410
         _ExtentX        =   2487
         _ExtentY        =   582
         _Version        =   393216
         Enabled         =   0   'False
         MaxLength       =   10
         Mask            =   "9####-####"
         PromptChar      =   "_"
      End
      Begin MSMask.MaskEdBox mskTelefone 
         Height          =   330
         Index           =   2
         Left            =   1200
         TabIndex        =   17
         Top             =   3885
         Width           =   1410
         _ExtentX        =   2487
         _ExtentY        =   582
         _Version        =   393216
         Enabled         =   0   'False
         MaxLength       =   10
         Mask            =   "9####-####"
         PromptChar      =   "_"
      End
      Begin MSMask.MaskEdBox mskTelefone 
         Height          =   330
         Index           =   3
         Left            =   1200
         TabIndex        =   21
         Top             =   4245
         Width           =   1410
         _ExtentX        =   2487
         _ExtentY        =   582
         _Version        =   393216
         Enabled         =   0   'False
         MaxLength       =   10
         Mask            =   "9####-####"
         PromptChar      =   "_"
      End
      Begin MSMask.MaskEdBox mskTelefone 
         Height          =   330
         Index           =   4
         Left            =   1215
         TabIndex        =   25
         Top             =   4605
         Width           =   1410
         _ExtentX        =   2487
         _ExtentY        =   582
         _Version        =   393216
         Enabled         =   0   'False
         MaxLength       =   10
         Mask            =   "9####-####"
         PromptChar      =   "_"
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Nome:"
         Height          =   195
         Left            =   225
         TabIndex        =   43
         Top             =   315
         Width           =   465
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "D.D.D.:"
         Height          =   195
         Left            =   240
         TabIndex        =   42
         Top             =   2940
         Width           =   555
      End
      Begin VB.Label Label3 
         AutoSize        =   -1  'True
         Caption         =   "Telefone:"
         Height          =   195
         Left            =   1245
         TabIndex        =   41
         Top             =   2940
         Width           =   675
      End
      Begin VB.Label Label5 
         AutoSize        =   -1  'True
         Caption         =   "E-mail:"
         Height          =   195
         Left            =   225
         TabIndex        =   40
         Top             =   5055
         Width           =   465
      End
      Begin VB.Label Label6 
         AutoSize        =   -1  'True
         Caption         =   "Endere�o:"
         Height          =   195
         Left            =   225
         TabIndex        =   39
         Top             =   920
         Width           =   735
      End
      Begin VB.Label Label7 
         AutoSize        =   -1  'True
         Caption         =   "Bairro:"
         Height          =   195
         Left            =   225
         TabIndex        =   38
         Top             =   1580
         Width           =   450
      End
      Begin VB.Label Label8 
         AutoSize        =   -1  'True
         Caption         =   "Munic�pio:"
         Height          =   195
         Left            =   3000
         TabIndex        =   37
         Top             =   2295
         Width           =   750
      End
      Begin VB.Label Label9 
         AutoSize        =   -1  'True
         Caption         =   "U.F.:"
         Height          =   195
         Left            =   3000
         TabIndex        =   36
         Top             =   1580
         Width           =   345
      End
      Begin VB.Label Label10 
         AutoSize        =   -1  'True
         Caption         =   "CEP:"
         Height          =   195
         Left            =   240
         TabIndex        =   35
         Top             =   2280
         Width           =   360
      End
      Begin VB.Label Label12 
         AutoSize        =   -1  'True
         Caption         =   "Ramal:"
         Height          =   195
         Left            =   3045
         TabIndex        =   34
         Top             =   2940
         Width           =   495
      End
      Begin VB.Label Label13 
         AutoSize        =   -1  'True
         Caption         =   "Tipo Telefone:"
         Height          =   195
         Left            =   4395
         TabIndex        =   33
         Top             =   2940
         Width           =   1035
      End
   End
End
Attribute VB_Name = "frmSolicitante"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public Municipio_id As Integer  'Para armazenar o municipio_id do solicitante

Private Sub cmbMunicipio_Click()
'    If cmbMunicipio.ListIndex <> -1 Then                               'AKIO.OKUNO - 17860335 - 10/06/2013
    If cmbMunicipio.ListIndex <> -1 And cmbUF.ListIndex = -1 Then      'AKIO.OKUNO - 17860335 - 10/06/2013
        Municipio_id = cmbMunicipio.ItemData(cmbMunicipio.ListIndex)
        cmbUF.Text = obtemEstado(Municipio_id, cmbMunicipio.Text)
    End If
End Sub



Private Sub cmbUF_Click()
'    CarregaMunicipio (Me.cmbMunicipio) 'AKIO.OKUNO - 17860335 - 10/06/2013

    Dim SQL As String
    Dim Rs As ADODB.Recordset
    
    Debug.Print "cmbUF_Click"
    Debug.Print "Inicio: " & Format(Now(), "hh:mm:ss:ms")
    SQL = ""
    SQL = SQL & " select   municipio_id, estado, nome"
    SQL = SQL & " from     municipio_tb with (nolock)"
    SQL = SQL & " Where    Estado = '" & cmbUF & "'"        'AKIO.OKUNO - 17860335 - 11/06/2013
    SQL = SQL & " order by  nome"


    Set Rs = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, SQL, True)
    
    With cmbMunicipio
        .Clear
        If Not Rs.EOF Then
            While Not Rs.EOF
                 .AddItem Trim(Rs(2))
                 .ItemData(.NewIndex) = Rs(0)
                Rs.MoveNext
            Wend
        End If
        Rs.Close
    End With
    Debug.Print "Fim   : " & Format(Now(), "hh:mm:ss:ms")
End Sub

Private Sub cmdContinuar_Click()
    Dim cont As Integer
    
    'AFONSO FILHO - GPTI
    'DEMANDA: 554094 - DATA: 05/11/2008
    'verificar se o UF est� preenchido

    Debug.Print "cmdContinuar_Click"
    Debug.Print "Inicio: " & Format(Now(), "hh:mm:ss:ms")
    With frmAviso
            
        
'Ricardo Toledo (Confitec) : INC000004521789 : 25/02/2015 : Inicio
        'If txtNomeSolicitante.Text <> "" Then
        If Trim(txtNomeSolicitante.Text) <> "" Then
'Ricardo Toledo (Confitec) : INC000004521789 : 25/02/2015 : fim
            .txtNomeSolicitante.Text = txtNomeSolicitante.Text
        Else
            MsgBox "� necess�rio indicar o nome do Solicitante.", vbInformation
            txtNomeSolicitante.SetFocus
            Exit Sub
        End If
        
        If txtEnderecoSolicitante.Text <> "" Then
            .txtEnderecoSolicitante.Text = txtEnderecoSolicitante.Text
        Else
            MsgBox "� necess�rio indicar o endere�o do Solicitante.", vbInformation
            txtEnderecoSolicitante.SetFocus
            Exit Sub
        End If
        
        If txtBairroSolicitante.Text <> "" Then
            .txtBairroSolicitante.Text = txtBairroSolicitante.Text
        Else
            MsgBox "� necess�rio indicar o bairro do Solicitante.", vbInformation
            txtBairroSolicitante.SetFocus
            Exit Sub
        End If
        
'AKIO.OKUNO - INICIO - 17860335 - 20/06/2013
'        If cmbMunicipio.ListIndex <> -1 Then
        If cmbMunicipio.ListIndex <> -1 And LenB(Trim(cmbMunicipio)) <> 0 Then
'AKIO.OKUNO - FIM - 17860335 - 20/06/2013
           .txtMunicipioSolicitante.Text = cmbMunicipio.Text
        Else
            MsgBox "� necess�rio indicar o municipio do Solicitante.", vbInformation
            cmbMunicipio.SetFocus
            Exit Sub
        End If
            
        If txtCEPSolicitante.Text <> "" Then
            If Val(txtCEPSolicitante.Text) <> 0 Then
                If Len(txtCEPSolicitante.Text) = 8 Then
                    txtCEPSolicitante.Text = Format(txtCEPSolicitante.Text, "00000-000")
                Else
                    If Mid(txtCEPSolicitante.Text, 6, 1) <> "-" Then
                        MsgBox "O CEP informado � inv�lido. Deve conter 8 n�meros.", vbInformation
                        txtCEPSolicitante.SetFocus
                    End If
                    If Len(Replace(txtCEPSolicitante.Text, "_", "")) <> 9 Then
                        MsgBox "O CEP informado � inv�lido.", vbInformation
                        txtCEPSolicitante.SetFocus
                        Exit Sub
                    End If
                End If
            Else
                MsgBox "O CEP informado � inv�lido.", vbInformation
                txtCEPSolicitante.SetFocus
                Exit Sub
            End If
        End If
        
        If txtCEPSolicitante.Text <> "" Then
            .txtCEPSolicitante.Text = txtCEPSolicitante.Text
        Else
            MsgBox "� necess�rio indicar o CEP do Solicitante.", vbInformation
            txtCEPSolicitante.SetFocus
            Exit Sub
        End If
        
        If mskDDD(0).Text <> "__" And Len(Replace(mskDDD(0).Text, "_", "")) = 2 Then
            .txtDDD(0).Text = mskDDD(0).Text
        Else
            MsgBox "� necess�rio indicar o DDD v�lido de contato de Solicitante.", vbInformation
            mskDDD(0).SetFocus
            Exit Sub
        End If
        
        '24/05/2012 Nova - Mauro Vianna : telefone 9 d�gitos
        'If mskTelefone(0).Text <> "____-____" Then
        If mskTelefone(0).Text <> "_____-____" Then
            .txtTelefone(0).Text = mskTelefone(0).Text
        Else
            MsgBox "� necess�rio indicar o telefone de contato de Solicitante.", vbInformation
            mskTelefone(0).SetFocus
            Exit Sub
        End If
        
               
        If cmbTipoTelefone(0).ListIndex <> -1 Then
            .txtTipoTelefone(0).Text = cmbTipoTelefone(0).Text
        Else
            MsgBox "� necess�rio indicar o tipo de telefone de contato de Solicitante.", vbInformation
            cmbTipoTelefone(0).Enabled = True 'asouza
            cmbTipoTelefone(0).SetFocus
            Exit Sub
        End If
        
        For cont = 0 To 4
            .txtDDD(cont).Text = Replace(mskDDD(cont).Text, "_", "")
            .txtTelefone(cont).Text = Replace(mskTelefone(cont).Text, "_", "")
            .txtRamal(cont).Text = txtRamal(cont).Text
            .txtTipoTelefone(cont).Text = cmbTipoTelefone(cont).Text
        Next cont
        
        Municipio_id = VerificaMunicipio(Trim(cmbMunicipio.Text), Trim(cmbUF.Text))
        .txtEmailSolicitante.Text = txtEmailSolicitante.Text
        .txtUFSolicitante.Text = cmbUF.Text
       
    End With
    
    If cmbUF.Text = "" Then
        MsgBox "Favor escolher a UF.", vbInformation + vbOKOnly, "Aviso"
        Exit Sub
    End If
    
    Me.Hide
    Debug.Print "Inicio: " & Format(Now(), "hh:mm:ss:ms")
    frmAgencia.Show
    
End Sub

Private Sub cmdPesquisarCEP_Click()
Dim SQL As String
Dim Rs As ADODB.Recordset

Dim i As Integer
Dim Municipio_id As String
Dim Municipio As String
Dim Uf As String
Dim chvlocal_dne As String
Dim municipio_descricao As String


 If txtCEPSolicitante.ClipText <> "" Then
     Me.MousePointer = vbHourglass
     
     'Obtem o endere�o, de acordo com o CEP
     SQL = ""
     SQL = SQL & " SELECT uf_log UF, nome_local municipio, chvlocal_dne chave_correio,  "
     SQL = SQL & "  tp_log tp_logra, nome_log logra,"
     SQL = SQL & "      cep8_log CEP, nombai1_log bairro"
     SQL = SQL & " FROM seguros_temp_db..CEP_LOg with (nolock) "
     SQL = SQL & " WHERE CEP8_LOg = '" & Replace(txtCEPSolicitante.Text, "-", "") & "'"
     
     Set Rs = ExecutarSQL(gsSIGLASISTEMA, _
                          glAmbiente_id, _
                          App.Title, _
                          App.FileDescription, _
                          SQL, _
                          True)
     If Not Rs.EOF Then
        txtEnderecoSolicitante.Text = Trim(Rs!tp_logra) & " " & Trim(Rs!logra)
        If AchouBase(Trim(Rs!Municipio), Trim(Rs!Uf), municipio_descricao) Then
            cmbMunicipio.Text = Trim(municipio_descricao)
        Else
            Municipio_id = InsereMunicipio(Rs!Municipio, Rs!Uf, Rs!chave_correio)
            Call CarregaMunicipio(cmbMunicipio)
            cmbMunicipio.Text = Trim(Rs!Municipio)
        End If
        
        txtBairroSolicitante.Text = IIf(IsNull(Rs!Bairro), "", Trim(Rs!Bairro))
        cmbUF.Text = Rs!Uf
        Municipio = Rs!Municipio
        Uf = Rs!Uf
        chvlocal_dne = Rs!chave_correio
        
        txtNomeSolicitante.SetFocus
        
        'Verifica se existe na municpio_tb
        SQL = ""
        SQL = SQL & " SELECT municipio_id, estado, nome, tp_pessoa_id, nome, municipio_ibge_id, "
        SQL = SQL & "        municipio_BACEN_id, municipio_BACEN_dv, chvlocal_dne"
        SQL = SQL & " FROM municipio_tb with (nolock)"
        SQL = SQL & " WHERE (nome = '" & Trim(Municipio) & "' "
        SQL = SQL & " AND estado = '" & Trim(Uf) & "') "
        SQL = SQL & " OR (chvlocal_dne = '" & chvlocal_dne & "')"
        
        Set Rs = ExecutarSQL(gsSIGLASISTEMA, _
                          glAmbiente_id, _
                          App.Title, _
                          App.FileDescription, _
                          SQL, _
                          True)
        If Not Rs.EOF Then
            
            'Caso n�o tenha o ID da base dos Correios
            If IsNull(Rs!chvlocal_dne) Then
               
                SQL = ""
                SQL = SQL & "EXEC municipio_spu " & Rs!Municipio_id & "," & Rs!tp_pessoa_id & ","
                SQL = SQL & "'" & Trim(Rs!Nome) & "','" & Trim(Rs!estado) & "','" & cUserName & "',"
                SQL = SQL & IIf(IsNull(Rs!municipio_ibge_id), "null", Rs!municipio_ibge_id) & "," & IIf(IsNull(Rs!municipio_bacen_id), "null", Rs!municipio_bacen_id) & ","
                SQL = SQL & IIf(IsNull(Rs!municipio_bacen_dv), "null", Rs!municipio_bacen_dv) & ", '" & Format(chvlocal_dne, "00000000") & "'"
                
                Call ExecutarSQL(gsSIGLASISTEMA, _
                          glAmbiente_id, _
                          App.Title, _
                          App.FileDescription, _
                          SQL, _
                          False)
            
            End If
            
        Else
            'Caso n�o exista em municipio_tb
            Municipio_id = InsereMunicipio(Municipio, Uf, chvlocal_dne)
            Call CarregaMunicipio(cmbMunicipio)
            cmbMunicipio.Text = Municipio
        End If
        
        ' Flow 203212 - Marcelo - 30/01/2007
        For i = 0 To cmbMunicipio.ListCount - 1
            If cmbMunicipio.List(i) = Trim(Municipio) Then
                cmbMunicipio.ListIndex = i
                If cmbUF.Text = Trim(Uf) Then
                    Exit For
                End If
            End If
        Next
        
     End If
     Me.MousePointer = vbDefault
 End If
 
End Sub

Public Function AchouBase(Municipio As String, Uf As String, ByRef municipio_descricao As String) As Boolean
Dim SQL As String
Dim Rs As ADODB.Recordset

    SQL = ""
    SQL = SQL & " SELECT nome "
    SQL = SQL & " FROM municipio_tb with (nolock)"
    SQL = SQL & " WHERE rtrim(nome) = '" & Trim(Municipio) & "' "
    SQL = SQL & " AND rtrim(estado) = '" & Trim(Uf) & "'"
        
    Set Rs = ExecutarSQL(gsSIGLASISTEMA, _
                      glAmbiente_id, _
                      App.Title, _
                      App.FileDescription, _
                      SQL, _
                      True)
    If Not Rs.EOF Then
        AchouBase = True
        municipio_descricao = Rs!Nome
    Else
        AchouBase = False
    End If
    
    Rs.Close
    
End Function

Public Function InsereMunicipio(Municipio As String, Uf As String, chvlocal_dne As String) As Integer
Dim SQL As String
Dim Rs As ADODB.Recordset
Dim Municipio_id As Integer

        SQL = ""
        SQL = SQL & "SELECT isnull(max(municipio_id),0) ultimo "
        SQL = SQL & "FROM municipio_tb with (nolock)"
        SQL = SQL & "WHERE estado = '" & Uf & "'"
        SQL = SQL & "AND municipio_id <> 999"
         
        Set Rs = ExecutarSQL(gsSIGLASISTEMA, _
                  glAmbiente_id, _
                  App.Title, _
                  App.FileDescription, _
                  SQL, _
                  True)
        If Not Rs.EOF Then
            Municipio_id = CLng(Rs!ultimo) + 1
            If Len(CStr(Municipio_id)) > 3 Then
                Municipio_id = 999
            End If
        Else
            Municipio_id = 1
        End If

        SQL = ""
        SQL = SQL & "EXEC municipio_spi " & Municipio_id & "," & 1 & ","
        SQL = SQL & "'" & Trim(Municipio) & "','" & Uf & "','" & cUserName & "',"
        SQL = SQL & "null" & "," & "null" & ","
        SQL = SQL & "null" & ", '" & Format(chvlocal_dne, "00000000") & "'"
        
        Call ExecutarSQL(gsSIGLASISTEMA, _
                  glAmbiente_id, _
                  App.Title, _
                  App.FileDescription, _
                  SQL, _
                  False)
                  
        InsereMunicipio = Municipio_id
End Function


Private Sub cmdSegurado_Click()
    If frmConsulta.grdResultadoPesquisa.Rows > 1 Then
        Call ObtemDadosSegurado(frmConsulta.grdResultadoPesquisa.TextMatrix(frmConsulta.grdResultadoPesquisa.RowSel, 3))
    End If
End Sub

Private Sub Form_Load()
Dim Index As Integer
    
'AKIO.OKUNO - INICIO - 17860335 - 26/06/2013
'    Me.Caption = SIGLA_PROGRAMA & " - Aviso de Sinistro RE - " & IIf(glAmbiente_id = 2, "Produ��o", "Qualidade")
    Me.Caption = SIGLA_PROGRAMA & " - Aviso de Sinistro RE - " & IIf(glAmbiente_id = 2 Or glAmbiente_id = 6, "Produ��o", "Qualidade")
'AKIO.OKUNO - FIM - 17860335 - 26/06/2013

    CentraFrm Me
'AKIO.OKUNO - INICIO - 17860335 - 10/06/2013
'    For Index = 0 To 4
'        Call CarregaTipoTel(cmbTipoTelefone(Index))
'    Next Index
'
'    Call CarregaMunicipio(cmbMunicipio)
    cmbMunicipio = ""
'AKIO.OKUNO - FIM - 17860335 - 10/06/2013
    Call CarregaUF(cmbUF)

End Sub

Private Sub Form_Unload(Cancel As Integer)
    If Not ForcaUnload Then
        Cancel = 1
    End If
End Sub

Private Sub cmdVoltar_Click()

Me.Hide
frmEvento.Show

End Sub

Private Sub cmdSair_Click()
    Me.Hide
    Call Sair(Me)
End Sub

Private Sub mskDDD_LostFocus(Index As Integer)
    If mskDDD(Index).Text <> "__" And Len(mskDDD(Index).Text) = 2 Then
        mskTelefone(Index).Enabled = True
        txtRamal(Index).Enabled = True
        cmbTipoTelefone(Index).Enabled = True
        mskTelefone(Index).SetFocus
    Else
        mskTelefone(Index).Enabled = False
        txtRamal(Index).Enabled = False
        cmbTipoTelefone(Index).Enabled = False
    End If
End Sub


Private Sub mskTelefone_LostFocus(Index As Integer)
    '30/05/2012 : Nova - Mauro Vianna : Altera��o de telefone para 9 d�gitos : tratamento de 8 d�gitos
    If Right(mskTelefone(Index).Text, 1) = "_" And Right(mskTelefone(Index).Text, 2) <> "__" Then
        mskTelefone(Index).Text = " " & Left(mskTelefone(Index).Text, 4) + "-" + Mid(mskTelefone(Index).Text, 5, 1) + Mid(mskTelefone(Index).Text, 7, 3)
    End If

End Sub

Private Sub txtBairroSolicitante_LostFocus()
    If txtBairroSolicitante.Text <> "" Then
        txtBairroSolicitante.Text = UCase(txtBairroSolicitante.Text)
    End If
End Sub

'Private Sub txtCEPSolicitante_KeyPress(KeyAscii As Integer)
'  If KeyAscii = 13 And txtCEPSolicitante.Text <> "" Then
'        SendKeys "{TAB}"
'  End If
'End Sub

Private Sub txtCEPSolicitante_LostFocus()
    'If txtCEPSolicitante.Text <> "" Then
    '    If Val(txtCEPSolicitante.Text) <> 0 Then
    '        If Len(txtCEPSolicitante.Text) = 8 Then
    '            txtCEPSolicitante.Text = Format(txtCEPSolicitante.Text, "00000-000")
    '        Else
    '            If Mid(txtCEPSolicitante.Text, 6, 1) <> "-" Then
    '                MsgBox "O CEP informado � inv�lido. Deve conter 8 n�meros.", vbInformation
    '                txtCEPSolicitante.SetFocus
    '            End If
    '        End If
    '    Else
    '        MsgBox "O CEP informado � inv�lido. O valor n�o � num�rico.", vbInformation
    '        txtCEPSolicitante.SetFocus
    '    End If
    'End If
End Sub

Private Sub txtEnderecoSolicitante_LostFocus()
    If txtEnderecoSolicitante.Text <> "" Then
        txtEnderecoSolicitante.Text = UCase(txtEnderecoSolicitante.Text)
    End If
End Sub

Private Sub txtNomeSolicitante_LostFocus()
    If txtNomeSolicitante.Text <> "" Then
        txtNomeSolicitante.Text = UCase(txtNomeSolicitante.Text)
    End If
End Sub

Public Function VerificaMunicipio(Municipio As String, Uf As String) As Integer
Dim SQL As String
Dim Rs As ADODB.Recordset
    
    SQL = ""
    SQL = SQL & " select   municipio_id"
    SQL = SQL & " from     municipio_tb with (nolock)"
    SQL = SQL & " where rtrim(nome) = '" & Trim(Municipio) & "'"
    SQL = SQL & " and rtrim(estado) = '" & Trim(Uf) & "'"
    
    Set Rs = ExecutarSQL(gsSIGLASISTEMA, _
                        glAmbiente_id, _
                        App.Title, _
                        App.FileDescription, _
                        SQL, True)
    If Not Rs.EOF Then
        VerificaMunicipio = Rs(0)
    Else
        VerificaMunicipio = 0
    End If
    
End Function

Public Sub ObtemDadosSegurado(Cliente_id As Double)
Dim SQL As String
Dim Rs As ADODB.Recordset
Dim i As Integer
    
    SQL = ""
    SQL = SQL & " select cliente_tb.nome,  endereco, bairro, Cep, Municipio, estado, ddd, telefone"
    SQL = SQL & " from    proposta_tb  with (nolock)"
    SQL = SQL & " inner join endereco_corresp_tb with (nolock)"
    SQL = SQL & " on endereco_corresp_tb.proposta_id = proposta_tb.proposta_id"
    SQL = SQL & " inner join cliente_tb with (nolock)"
    SQL = SQL & " on cliente_tb.cliente_id = proposta_tb.prop_cliente_id"
    SQL = SQL & " where proposta_tb.prop_cliente_id = " & Cliente_id
    'Cl�udio Gomes   -   Confitec   -   17/02/2012 ***************************************************************************************
    '12921844   -   Pesquisa pelos dados do mutu�rio do sinistro habitacional tamb�m
    SQL = SQL & " UNION "
    SQL = SQL & " select cliente_tb.nome,  endereco, bairro, Cep, Municipio, estado, ddd, telefone"
    SQL = SQL & " From cliente_tb(nolock)"
    SQL = SQL & " INNER JOIN cliente_habitacional_tb (nolock)"
    SQL = SQL & " ON cliente_tb.cliente_Id  =  cliente_habitacional_tb.cliente_id"
    SQL = SQL & " INNER JOIN endereco_corresp_tb with (nolock)"
    SQL = SQL & " on endereco_corresp_tb.proposta_id = cliente_habitacional_tb.proposta_id"
    SQL = SQL & " Where cliente_tb.Cliente_id = " & Cliente_id
    '-----------------------------------------------------------------------------------------------
        
    Set Rs = ExecutarSQL(gsSIGLASISTEMA, _
                        glAmbiente_id, _
                        App.Title, _
                        App.FileDescription, _
                        SQL, True)
    If Not Rs.EOF Then
        txtNomeSolicitante = Rs("nome")
        'txtNomeInspecao = rs("nome")
        txtEnderecoSolicitante = Trim(Rs("endereco"))
        txtBairroSolicitante = Trim(Rs("bairro"))
        txtCEPSolicitante = IIf(Trim(Rs("cep")) = "", "_____-___", Format(Rs("cep"), "00000-000"))
        
        ' Flow 199152 - Nakata - 29/12/06
        'If Not Trim(Rs("municipio")) = "" Then
        '    cmbMunicipio.Text = Trim(Rs("municipio"))
        'End If
        cmbUF.Text = Rs("estado")   'AKIO.OKUNO - 17860335 - 20/06/2013
        For i = 0 To cmbMunicipio.ListCount - 1
            If cmbMunicipio.List(i) = Trim(UCase(Rs("municipio"))) Then
                cmbMunicipio.ListIndex = i
                If cmbUF.Text = UCase(Rs("estado")) Then
                    Exit For
                End If
            End If
        Next
        'Fim 199152
        
'        cmbUF.Text = Rs("estado")  'AKIO.OKUNO - 17860335 - 20/06/2013
        If Not IsNull(Rs("ddd")) Then
'            mskDDD(0).Text = IIf(Trim(Rs("ddd")) = "", "__", Right("__" + Trim(Rs("ddd")), 2)) 'AKIO.OKUNO - 17860335 - 20/06/2013
            mskDDD(0).Text = IIf(Trim(Rs("ddd")) = "", "__", Right("00" & Trim(Right(Rs("ddd"), 2)), 2))
        Else
            mskDDD(0).Mask = "##"
            mskDDD(0).Text = "__"
        End If
        mskTelefone(0).Enabled = True
        
' 24/05/2012 Nova - Mauro Vianna : mudan�a de telefone para 9 d�gitos
'        If Not IsNull(Rs("telefone")) Then
'            mskTelefone(0).Text = IIf(Trim(Rs("telefone")) = "", "____-____", Format(Rs("telefone"), "0###-####"))
'        Else
'            mskTelefone(0).Text = "____-____"
'        End If
        If Not IsNull(Rs("telefone")) Then
            mskTelefone(0).Text = IIf(Trim(Rs("telefone")) = "", "_____-____", Format(Rs("telefone"), "0####-####"))
        Else
            mskTelefone(0).Text = "_____-____"
        End If
        
        txtRamal(0).Enabled = True
        txtRamal(0).Text = ""
        cmbTipoTelefone(0).Enabled = True
        cmbTipoTelefone(0).ListIndex = -1
        
        mskDDD(1).Text = "__"
        '30/05/2012 Nova -Mauro Vianna : altera��o de telefone para 9 d�gitos
'        mskTelefone(1).Text = "____-____"
        mskTelefone(1).Text = "_____-____"
        txtRamal(1).Text = ""
        cmbTipoTelefone(1).ListIndex = -1
        
        mskDDD(2).Text = "__"
        '30/05/2012 Nova -Mauro Vianna : altera��o de telefone para 9 d�gitos
'        mskTelefone(2).Text = "____-____"
        mskTelefone(2).Text = "_____-____"
        txtRamal(2).Text = ""
        cmbTipoTelefone(2).ListIndex = -1
        
        mskDDD(3).Text = "__"
        '30/05/2012 Nova -Mauro Vianna : altera��o de telefone para 9 d�gitos
'        mskTelefone(3).Text = "____-____"
        mskTelefone(3).Text = "_____-____"
        txtRamal(3).Text = ""
        cmbTipoTelefone(3).ListIndex = -1
        
        mskDDD(4).Text = "__"
        '30/05/2012 Nova -Mauro Vianna : altera��o de telefone para 9 d�gitos
'        mskTelefone(4).Text = "____-____"
        mskTelefone(4).Text = "_____-____"
        txtRamal(4).Text = ""
        cmbTipoTelefone(4).ListIndex = -1
        
    End If
End Sub

