VERSION 5.00
Begin VB.Form FrmEncerramento 
   Caption         =   "SEGP1294 - Encerramento de Sinistro "
   ClientHeight    =   6855
   ClientLeft      =   4110
   ClientTop       =   2625
   ClientWidth     =   9255
   LinkTopic       =   "FrmEncerramento"
   ScaleHeight     =   6855
   ScaleWidth      =   9255
   Begin VB.TextBox TxtAuxiliar 
      Height          =   285
      Left            =   120
      TabIndex        =   25
      Text            =   "Cancelado"
      Top             =   6360
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.CommandButton BtnAplicar 
      Caption         =   "A&plicar"
      Height          =   375
      Left            =   6000
      TabIndex        =   13
      Top             =   6240
      Width           =   1452
   End
   Begin VB.CommandButton BtnVoltar 
      Caption         =   "&Cancelar"
      Height          =   375
      Left            =   7560
      TabIndex        =   12
      Top             =   6240
      Width           =   1452
   End
   Begin VB.Frame Frame1 
      Caption         =   "Selecione o Motivo de Encerramento"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2760
      Left            =   230
      TabIndex        =   8
      Top             =   240
      Width           =   8790
      Begin VB.OptionButton optEncerramento_Tipo_Liquidado 
         Caption         =   "Liquidado"
         Height          =   255
         Left            =   240
         TabIndex        =   15
         Top             =   360
         Width           =   1095
      End
      Begin VB.Frame FmeTipoEncerramento 
         Caption         =   " Encerramento sem comunica��o ao SEGUR "
         Height          =   1815
         Left            =   240
         TabIndex        =   14
         Top             =   720
         Width           =   4035
         Begin VB.OptionButton optEncerramento_Cosseguro_Inferior 
            Caption         =   "Cosseguro Valor Inferior"
            Height          =   255
            Left            =   240
            TabIndex        =   18
            Top             =   1080
            Width           =   3015
         End
         Begin VB.OptionButton optEncerramento_Indeferimento 
            Caption         =   "Indeferimento "
            Height          =   255
            Left            =   240
            TabIndex        =   23
            Top             =   1080
            Width           =   1515
         End
         Begin VB.OptionButton optEncerramento_Indeferimento_Administrativo 
            Caption         =   "Indeferimento (Administrativo)"
            Height          =   255
            Left            =   240
            TabIndex        =   19
            Top             =   1440
            Visible         =   0   'False
            Width           =   2385
         End
         Begin VB.OptionButton optEncerramento_Pedido_Lider 
            Caption         =   "A Pedido da L�der"
            Height          =   255
            Left            =   240
            TabIndex        =   17
            Top             =   720
            Width           =   1755
         End
         Begin VB.OptionButton optEncerramento_Administrativo 
            Caption         =   "Administrativo"
            Height          =   255
            Left            =   240
            TabIndex        =   16
            Top             =   360
            Width           =   1575
         End
      End
      Begin VB.OptionButton optEncerramento_Tipo_Indeferimento 
         Caption         =   "Indeferimento "
         Height          =   255
         Left            =   4440
         TabIndex        =   10
         Top             =   360
         Width           =   3075
      End
      Begin VB.Frame FmeEncerramento_Sinistro_Solicitacao_Cancelamento 
         Caption         =   " Solicita��o de cancelamento de aviso ao SEGUR "
         Height          =   1815
         Left            =   4440
         TabIndex        =   9
         Top             =   720
         Width           =   4035
         Begin VB.OptionButton optEncerramento_Falta_Documento 
            Caption         =   "Por Falta de Documenta��o"
            Height          =   255
            Left            =   240
            TabIndex        =   24
            Top             =   360
            Width           =   2340
         End
         Begin VB.OptionButton optEncerramento_Duplicidade_Registro 
            Caption         =   "Por Duplicidade de Registro"
            Height          =   255
            Left            =   240
            TabIndex        =   22
            Top             =   1440
            Visible         =   0   'False
            Width           =   2370
         End
         Begin VB.OptionButton optEncerramento_Cobertura_Indevida 
            Caption         =   "Aviso em Cobertura Indevida"
            Height          =   255
            Left            =   240
            TabIndex        =   21
            Top             =   1080
            Width           =   2385
         End
         Begin VB.OptionButton optEncerramento_Desistencia_Segurado 
            Caption         =   "Desist�ncia do Segurado"
            Height          =   255
            Left            =   240
            TabIndex        =   20
            Top             =   720
            Width           =   2340
         End
      End
   End
   Begin VB.TextBox txtDetalhe 
      BeginProperty Font 
         Name            =   "Courier"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Index           =   2
      Left            =   230
      MaxLength       =   70
      TabIndex        =   7
      Top             =   3900
      Width           =   8800
   End
   Begin VB.TextBox txtDetalhe 
      BeginProperty Font 
         Name            =   "Courier"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Index           =   1
      Left            =   230
      MaxLength       =   70
      TabIndex        =   6
      Top             =   3600
      Width           =   8800
   End
   Begin VB.TextBox txtDetalhe 
      BeginProperty Font 
         Name            =   "Courier"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Index           =   0
      Left            =   230
      MaxLength       =   70
      TabIndex        =   5
      Top             =   3300
      Width           =   8800
   End
   Begin VB.TextBox txtDetalhe 
      BeginProperty Font 
         Name            =   "Courier"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Index           =   3
      Left            =   230
      MaxLength       =   5000
      TabIndex        =   4
      Top             =   4200
      Width           =   8800
   End
   Begin VB.Frame FmePessoa_Exposta 
      Caption         =   "Pessoa Politicamente Exposta"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1335
      Left            =   230
      TabIndex        =   0
      Top             =   4680
      Width           =   8800
      Begin VB.OptionButton optSim 
         Caption         =   "Sim"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   3600
         TabIndex        =   2
         Top             =   600
         Width           =   795
      End
      Begin VB.OptionButton optNao 
         Caption         =   "N�o"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   4560
         TabIndex        =   1
         Top             =   660
         Width           =   795
      End
      Begin VB.Label Label2 
         Caption         =   "H� ind�cios de Irregularidade?"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   270
         TabIndex        =   3
         Top             =   600
         Width           =   3165
      End
   End
   Begin VB.Label LblDetalhe 
      AutoSize        =   -1  'True
      Caption         =   "Indeferimento para o SEGUR :"
      Height          =   195
      Left            =   230
      TabIndex        =   11
      Top             =   3030
      Width           =   2160
   End
End
Attribute VB_Name = "FrmEncerramento"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private lConexaoLocal                           As Integer

 '1-Inclusao / 2-Alteracao/Exclus�o / 3-Consulta
Private intTipoInterface                        As Integer
Private Sinistro_Situacao                       As String
Private Localizacao_Processo                    As String
Private Sinistro_Catastrofe_Contador           As Integer 'AKIO.OKUNO - 29/09/2012 - RETIRADO DO MODULOLOCAL.BAS E CONVERTIDO PARA INT

Private vSitSel As Integer 'cristovao.rodrigues 16/10/2012
Public bPossuiMotivoEncerramentoCobertura As Boolean 'cristovao.rodrigues 23/10/2012

Private bolVerifica_Protocolacao            As Boolean      'AKIO.OKUNO - MP - 17/02/2013
Private bolDefiniu_GTR_Implantado           As Boolean      'AKIO.OKUNO - MP - 17/02/2013
Private bolDefiniu_Retorno_Entrada_GTR      As Boolean      'AKIO.OKUNO - MP - 17/02/2013
Private bolDefiniu_Verifica_Protocolacao    As Boolean      'AKIO.OKUNO - MP - 17/02/2013

Private Sub BtnAplicar_Click()
   
    Dim rs As ADODB.Recordset, SQL As String
    Dim vAvisou As Boolean
    Dim linha As String, vAcertou As Integer
    Dim iCntSinCat As Integer
    Dim vsql                As String
    Dim SQLAuxi                As String
    Dim rsAuxi              As ADODB.Recordset
    Dim rsRamoCat              As ADODB.Recordset
    Dim vDetalhou As Boolean, Itens_historico() As String
    Dim i, hist_i As Integer
    Dim Sinistro_BB As String
    '18225203 - ajuste no m�dulo de exigencia-
    Dim rsExigencia_Pendente As ADODB.Recordset
    Dim iContaExig As Integer
    Dim iExigencia As Integer
    
     '18225203 - ajuste no m�dulo de exigencia-
    'Demanda 17922837 - IN�CIO
    Dim iEvento As Long
    Dim sRemessa As String
    Dim iSaida As Long
    'Demanda 17922837 - FIM
           
    On Error GoTo Trata_Erro
    
    gbldSinistro_ID = dSinistro_ID          'AKIO.OKUNO - 29/09/2012
    
    ' Marcio.Nogueira - confitec/rj - Demanda MU-2018-029707 - Melhorias Web Service Mapa - Atualiza��o Campos Sinistro - [Subven��o Federal] - 04/072018
    ' Validando para somente Produtos 1152 e 1204
        If Campos_Validos_Subvencao = False Then
                MsgBox "N�o � poss�vel encerrar este aviso por campos de Subven��o n�o v�lidos.", vbInformation, "Aviso..."
                Exit Sub
        End If
'mathayde ????
'criar rotina para validacao

        If optEncerramento_Indeferimento_Administrativo.Value = True Then
            If Encerramento_Indeferido() = True Then
                MsgBox "N�o � poss�vel encerrar este aviso por indeferimento.", vbInformation, "Aviso..."
                Exit Sub
            End If
        End If
       
        '18225203 - ajuste no m�dulo de exigencia-
        Set rsExigencia_Pendente = Nothing
                        
       ' GENJUNIOR - CONFITEC SP - TRATAMENTO PARA N�O IMPEDIR O ENCERRAMENTO DO SINISTRO SE A EXIG�NCIA B�SICA ESTIVER PENDENTE - 06/10/2016
        vsql = ""
        vsql = vsql & " SELECT b.seq_exigencia " & vbNewLine
        vsql = vsql & "   FROM seguros_db.dbo.exigencia_tb b WITH (NOLOCK)" & vbNewLine
        vsql = vsql & "  INNER JOIN seguros_db.dbo.sinistro_tb a WITH (NOLOCK) " & vbNewLine
        vsql = vsql & "     ON b.sinistro_id = a.sinistro_id " & vbNewLine
        vsql = vsql & "  INNER JOIN seguros_db.dbo.exigencia_documento_tb c WITH (NOLOCK) " & vbNewLine
        vsql = vsql & "     ON b.sinistro_id = c.sinistro_id " & vbNewLine
        vsql = vsql & "    AND b.seq_exigencia = c.seq_exigencia " & vbNewLine
        vsql = vsql & "    AND c.documento_id <> 1076 " & vbNewLine
        vsql = vsql & "  WHERE a.sinistro_id = " & gbldSinistro_ID & vbNewLine
        vsql = vsql & "    AND LOWER(b.situacao) = 'p' " & vbNewLine
        vsql = vsql & "    AND b.seq_exigencia > 1 " & vbNewLine
    
        Set rsExigencia_Pendente = Conexao_ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, vsql, lConexaoLocal, True)
        
        If Not rsExigencia_Pendente.EOF Then
            iExigencia = rsExigencia_Pendente(0)
            
            If iExigencia > 0 Then
                
                rsExigencia_Pendente.Close
                Set rsExigencia_Pendente = Nothing
                
                vsql = ""
                vsql = vsql & " SELECT 1 FROM seguros_db.dbo.sinistro_bb_tb WITH (NOLOCK) WHERE sinistro_id = " & gbldSinistro_ID & vbNewLine
                
                Set rsExigencia_Pendente = Conexao_ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, vsql, lConexaoLocal, True)
                
                If Not rsExigencia_Pendente.EOF Then
                    MsgBox "N�o � poss�vel encerrar o sinistro, existe exig�ncia complementar pendente", vbOKOnly + vbCritical
                    RetornarTransacao (lConexaoLocal) '
                    MousePointer = vbNormal  'cristovao.rodrigues
                    rsExigencia_Pendente.Close
                    Set rsExigencia_Pendente = Nothing
                    Exit Sub
                Else
                    rsExigencia_Pendente.Close
                    Set rsExigencia_Pendente = Nothing
                                                                
                    vsql = ""
                    vsql = vsql & " SET NOCOUNT ON EXEC seguros_db.dbo.SEGS9915_SPU " & gbldSinistro_ID & ", NULL, " & iExigencia & ", NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'c', NULL "
                    
                    Set rsExigencia_Pendente = Conexao_ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, vsql, lConexaoLocal, True)
                End If

            End If
        End If
        'FIM GENJUNIOR
        '18225203 - ajuste no m�dulo de exigencia-
        
        rsExigencia_Pendente.Close
        Set rsExigencia_Pendente = Nothing

        If (optEncerramento_Tipo_Indeferimento.Value) Or (optEncerramento_Indeferimento.Value) Or (optEncerramento_Indeferimento_Administrativo.Value) Then
           vTp_detalhamento = 3
                      
        ElseIf (optEncerramento_Duplicidade_Registro.Value) Or _
               (optEncerramento_Falta_Documento.Value) Or _
               (optEncerramento_Desistencia_Segurado.Value) Or _
               (optEncerramento_Cobertura_Indevida.Value) Then
            vTp_detalhamento = 4
           
        Else
            vTp_detalhamento = 0
           
        
        End If
        
'            SQL = "SELECT distinct sinistro_tb.apolice_id,sinistro_tb.dt_aviso_sinistro,sinistro_tb.dt_ocorrencia_sinistro " & vbNewLine
'            SQL = SQL & " ,sinistro_tb.dt_inclusao,Agencia_id = isnull(sinistro_tb.Agencia_id,0),solicitante_sinistro_tb.nome,solicitante_sinistro_tb.endereco" & vbNewLine
'            SQL = SQL & " ,solicitante_sinistro_tb.ddd,solicitante_sinistro_tb.telefone,banco_id=isnull(sinistro_tb.banco_id,0),sinistro_tb.endereco as sinistro_endereco" & vbNewLine
'            SQL = SQL & " , cliente_tb.cliente_id, cliente_tb.nome as cliente_nome, proposta_tb.proposta_id, proposta_tb.produto_id, municipio_tb.nome as sinistro_municipio" & vbNewLine
'            SQL = SQL & " ,sinistro_tb.estado as sinistro_uf,sinistro_tb.sucursal_seguradora_id,sinistro_tb.seguradora_cod_susep,sinistro_tb.ramo_id" & vbNewLine
'            SQL = SQL & " FROM seguros_db.dbo.sinistro_tb sinistro_tb WITH (NOLOCK)" & vbNewLine
'            SQL = SQL & " join seguros_db.dbo.solicitante_sinistro_tb solicitante_sinistro_tb WITH (NOLOCK)" & vbNewLine
'            SQL = SQL & " on sinistro_tb.solicitante_id = solicitante_sinistro_tb.solicitante_id" & vbNewLine
'            SQL = SQL & " join seguros_db.dbo.cliente_tb cliente_tb WITH (NOLOCK)" & vbNewLine
'            SQL = SQL & " on cliente_tb.cliente_id = sinistro_tb.cliente_id" & vbNewLine
'            SQL = SQL & " join seguros_db.dbo.proposta_tb  proposta_tb WITH (NOLOCK)" & vbNewLine
'            SQL = SQL & " on proposta_tb.proposta_id = sinistro_tb.proposta_id" & vbNewLine
'            SQL = SQL & " join seguros_db.dbo.municipio_tb  municipio_tb WITH (NOLOCK)" & vbNewLine
'            SQL = SQL & " on municipio_tb.municipio_id = sinistro_tb.municipio_id " & vbNewLine
'            SQL = SQL & " WHERE sinistro_tb.sinistro_id = " & dSinistro_ID & vbNewLine

            'FLOW 19427059 - GENJUNIOR - CONFITEC SP
            SQL = ""
            SQL = SQL & " SET NOCOUNT ON EXEC seguros_db.dbo.SEGS13149_SPS " & dSinistro_ID
            
            Set rs = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                        glAmbiente_id, _
                                         App.Title, _
                                         App.FileDescription, _
                                         SQL, _
                                         lConexaoLocal, True)
        
        
            If Not rs.EOF Then
                Apolice_id = rs("apolice_id")
                Sucursal_id = rs("sucursal_seguradora_id")
                Seguradora_id = rs("seguradora_cod_susep")
                ramo_id = rs("ramo_id")
                Dt_aviso_sinistro = Format(rs("dt_aviso_sinistro"), "dd/mm/yyyy")
                Dt_ocorrencia_sinistro = Format(rs("dt_ocorrencia_sinistro"), "dd/mm/yyy")
                Agencia_id = rs("Agencia_id")
                Solicitante_Sinistro_Nome = rs("nome")
                Solicitante_Sinistro_Endereco = IIf(IsNull(rs("endereco")), "", rs("endereco"))
                'AKIO.OKUNO - INICIO - 01/10/2012
                'Solicitante_Sinistro_ddd = rs("ddd")
                'Solicitante_Sinistro_telefone = rs("telefone")
                Solicitante_Sinistro_ddd = rs("ddd") & ""
                Solicitante_Sinistro_telefone = rs("telefone") & ""
                'AKIO.OKUNO - INICIO - 01/10/2012
                
                Banco_id = rs("banco_id")
                Cliente_id = rs("cliente_id")
                Cliente_Nome = rs("cliente_nome")
                Proposta_id = rs("proposta_id")
                Produto_id = rs("produto_id")
                Sinistro_Endereco = IIf(IsNull(rs("sinistro_endereco")), "", rs("sinistro_endereco"))
                Dt_inclusao = rs("Dt_inclusao")
                'AKIO.OKUNO - INICIO - 01/10/2012
'                Sinistro_Municipio = rs("sinistro_municipio")
'                Sinistro_UF = rs("Sinistro_UF")
                Sinistro_Municipio = rs("sinistro_municipio") & ""
                Sinistro_UF = rs("Sinistro_UF") & ""
                'AKIO.OKUNO - INICIO - 01/10/2012
            Else
                'cristovao.rodrigues 22/10/2012 n�o retornou dados do sinistro
                mensagem_erro 6, "Dasos inv�lidos para o sinsitro !"
                FinalizaAuxiliar "CANCELADO"
            End If
            
            rs.Close
    
    
        AbrirTransacao (lConexaoLocal) 'cristovao.rodrigues 23/10/2012
        MousePointer = vbHourglass 'cristovao.rodrigues
     
'        If Sinistro_Situacao = 5 Then      'AKIO.OKUNO - 29/09/2012
        If Sinistro_Situacao = "5" Then
            
            Sinistro_BB = Nro_BB(dSinistro_ID)
            
            If Sinistro_BB <> "" Then
                
                SQL = " select  e.evento_segbr_id "
                SQL = SQL & " from    evento_segbr_sinistro_tb e WITH (NOLOCK) "
                SQL = SQL & " Where e.Sinistro_BB = " & Sinistro_BB
                SQL = SQL & " and     evento_id = ( "
                SQL = SQL & " select  max(e2.evento_id) "
                SQL = SQL & " from    evento_segbr_sinistro_tb e2 WITH (NOLOCK) "
                SQL = SQL & " inner join evento_segbr_tb es WITH (NOLOCK) on e2.evento_segbr_id = es.evento_segbr_id "
                SQL = SQL & " Where e2.Sinistro_BB = e.Sinistro_BB "
                SQL = SQL & " and es.prox_localizacao_processo is not null) "
                
                Set rs = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                            glAmbiente_id, _
                                             App.Title, _
                                             App.FileDescription, _
                                             SQL, _
                                             lConexaoLocal, True)
                
      
      
                If Not rs.EOF Then
                    If rs("evento_segbr_id") <> 10130 And rs("evento_segbr_id") <> 10134 Then 'evento de reabertura 9999
                        If bGTR_Implantado And Verifica_Protocolacao = False Then
                        'If vObtem_GTR_Implantado And Verifica_Protocolacao = False Then
                            MsgBox "N�o � poss�vel encerrar o sinistro antes de realizar o Protocolo de Homologa��o", vbOKOnly + vbCritical
                            RetornarTransacao (lConexaoLocal) 'cristovao.rodrigues 23/10/2012
                            MousePointer = vbNormal  'cristovao.rodrigues
                            Exit Sub
                        End If
                    End If
                End If
      
                 
            End If
          
            rs.Close
        Else
            
            If bGTR_Implantado And Verifica_Protocolacao = False Then
            'If vObtem_GTR_Implantado And Verifica_Protocolacao = False Then
                MousePointer = vbNormal  'cristovao.rodrigues
                MsgBox "N�o � poss�vel encerrar o sinistro antes de realizar o Protocolo de Homologa��o", vbOKOnly + vbCritical
                RetornarTransacao (lConexaoLocal) 'cristovao.rodrigues 23/10/2012
                MousePointer = vbNormal  'cristovao.rodrigues
                Exit Sub
            End If
        End If
        
        'IM00061533 - Permitir o encerramento Administrativo - Cleber Sardo - 13/07/2017
        If (bGTR_Implantado And Localizacao_Processo <> 2) And Sinistro_Situacao <> 5 Then
        'If vObtem_GTR_Implantado And Localizacao_Processo <> 2 Then
            MousePointer = vbNormal  'cristovao.rodrigues
            MsgBox "N�o � poss�vel encerrar o sinistro pois a localiza��o n�o est� na Seguradora.", vbOKOnly + vbCritical
            RetornarTransacao (lConexaoLocal) 'cristovao.rodrigues 23/10/2012
            MousePointer = vbNormal  'cristovao.rodrigues
            Exit Sub
        End If
    
        vDetalhamento_Encerra = ""
        
'cristovao.rodrigues 23/10/2012
'        Dim SQLAux As String
'        Dim rsaux As ADODB.Recordset
'
'        SQLAux = "SELECT DISTINCT * " & vbNewLine
'        SQLAux = SQLAux & "From " & vbNewLine
'        SQLAux = SQLAux & "sinistro_cobertura_tb WITH (NOLOCK) LEFT OUTER JOIN " & vbNewLine
'        SQLAux = SQLAux & "sinistro_motivo_encerramento_tb WITH (NOLOCK) ON " & vbNewLine
'        SQLAux = SQLAux & "sinistro_cobertura_tb.sinistro_motivo_encerramento_id = sinistro_motivo_encerramento_tb.sinistro_motivo_encerramento_id LEFT OUTER JOIN " & vbNewLine
'        SQLAux = SQLAux & "tp_cobertura_tb WITH (NOLOCK) ON sinistro_cobertura_tb.tp_cobertura_id = tp_cobertura_tb.tp_cobertura_id " & vbNewLine
'        SQLAux = SQLAux & "Where " & vbNewLine
'        SQLAux = SQLAux & "(sinistro_motivo_encerramento_tb.sinistro_motivo_encerramento_id IS NULL) AND" & vbNewLine
'        SQLAux = SQLAux & " (sinistro_cobertura_tb.tp_cobertura_id IS NOT NULL) " & vbNewLine
'        SQLAux = SQLAux & " AND (sinistro_cobertura_tb.sinistro_id = " & dSinistro_ID & ") " & vbNewLine
'        SQLAux = SQLAux & " AND (sinistro_cobertura_tb.dt_fim_vigencia is null or " & vbNewLine
'        SQLAux = SQLAux & " (CONVERT(VARCHAR,sinistro_cobertura_tb.dt_fim_vigencia,112) > " & vbNewLine
'        SQLAux = SQLAux & " CONVERT(VARCHAR,getdate(),112)))" & vbNewLine
'
'        Set rsaux = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
'                                        glAmbiente_id, _
'                                         App.Title, _
'                                         App.FileDescription, _
'                                         SQLAux, _
'                                         lConexaoLocal, True)
                                         
'        If Not rsaux.EOF Then
         bPossuiMotivoEncerramentoCobertura = VerificaMotivoEncerramentoCobertura
         If Not bPossuiMotivoEncerramentoCobertura Then
            MousePointer = vbNormal  'cristovao.rodrigues
            If MsgBox("N�o � poss�vel encerrar o aviso de sinistro." & vbCrLf & "Existem coberturas ativas sem motivo de encerramento cadastrado. Cadastrar?", vbYesNo, "Motivos de Encerramento de Coberturas") = vbYes Then
'                rsaux.Close
                FrmMotivoEncerramento.Show vbModal
            Else
'                rsaux.Close
                RetornarTransacao (lConexaoLocal) 'cristovao.rodrigues 23/10/2012
                MousePointer = vbNormal  'cristovao.rodrigues
                Exit Sub
            End If
            
            'cristovao.rodrigues 23/10/2012
            If Not bPossuiMotivoEncerramentoCobertura Then
                RetornarTransacao (lConexaoLocal)
                MousePointer = vbNormal  'cristovao.rodrigues
                Exit Sub
            End If
            
'        Else
'            rsaux.Close
        End If
    
        vDetalhamento_Encerra = ""
         
        vEncerrou = True
        If vSitSel < 0 Then
            mensagem_erro 2, "Tipo Encerramento"
            Exit Sub
        End If
        
        vDetalhou = False
     
        For i = 0 To 3
            If Trim(txtDetalhe(i).Text) <> "" Then
                vDetalhou = True
                Exit For
            End If
        Next i
             
        If (optEncerramento_Indeferimento_Administrativo.Value) Or (optEncerramento_Duplicidade_Registro.Value) Or _
           (optEncerramento_Falta_Documento.Value) Or (optEncerramento_Desistencia_Segurado.Value) Or _
           (optEncerramento_Cobertura_Indevida.Value) Or (optEncerramento_Tipo_Indeferimento.Value) Or _
           (optEncerramento_Indeferimento.Value) Then
            
            If Not vDetalhou Then
                MousePointer = vbNormal  'cristovao.rodrigues
                MsgBox "� obrigat�rio o preenchimento do detalhamento !", vbOKOnly + vbCritical
                'mathayde
                If txtDetalhe(0).Visible = True Then
                    txtDetalhe(0).SetFocus
                End If
                Exit Sub
            End If
        End If
        
        If FmePessoa_Exposta.Visible = True And (optSim.Value = False And optNao.Value = False) Then
            MousePointer = vbNormal  'cristovao.rodrigues
            MsgBox "A informa��o de ind�cio de Irregularidade n�o est� preenchida!", vbOKOnly + vbCritical
            RetornarTransacao (lConexaoLocal) 'cristovao.rodrigues 23/10/2012
            Exit Sub
        End If
                
                'Jose Soares - Sala �gil Sprint 24 - H4
        If Not VerificaCoberturarExcluirNPV(dSinistro_ID) Then
                RetornarTransacao (lConexaoLocal)
                MousePointer = vbNormal
                Exit Sub
        End If
        
        MousePointer = vbNormal  'cristovao.rodrigues
        If MsgBox("Confirma o motivo " & UCase(vOption_Escolhida) & " para o encerramento do sinistro " & _
                    dSinistro_ID & "?", vbYesNo + vbQuestion + vbDefaultButton2) = vbNo Then
'            BtnVoltar_Click
            RetornarTransacao (lConexaoLocal) 'cristovao.rodrigues 23/10/2012
            FinalizaAuxiliar "CANCELADO"        'AKIO.OKUNO - 02/10/2012
            Exit Sub
        End If
    
'        vsql = "SELECT sinistro_estimativa_tb.item_val_estimativa, " & vbNewLine
'        vsql = vsql & "isnull(sinistro_estimativa_tb.val_estimado,0) as val_estimado, " & vbNewLine
'        vsql = vsql & "isnull(sinistro_estimativa_tb.val_pago,0) as val_pago " & vbNewLine
'        vsql = vsql & "FROM sinistro_estimativa_tb WITH (NOLOCK) " & vbNewLine
'        vsql = vsql & "WHERE sinistro_estimativa_tb.sinistro_id = " & dSinistro_ID & vbNewLine
'        'mathayde
'        vsql = vsql & " and sinistro_estimativa_tb.item_val_estimativa not in (4,6)" & vbNewLine
''        vsql = vsql & " and (isnull(sinistro_estimativa_tb.val_estimado,0) - isnull(sinistro_estimativa_tb.val_pago,0)) > 0 " & vbNewLine  'AKIO.OKUNO - 13/09/2013 - REF.EMAIL 10/09-15:58
'        vsql = vsql & " and Sinistro_Estimativa_Tb.Dt_Fim_Estimativa Is Null"                                                               'AKIO.OKUNO - 13/09/2013 - REF.EMAIL 10/09-15:58
        
        'FLOW 19427059 - GENJUNIOR - CONFITEC SP
        vsql = ""
        vsql = vsql & " SET NOCOUNT ON EXEC seguros_db.dbo.SEGS13152_SPS " & dSinistro_ID
       
        Set rs = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                        glAmbiente_id, _
                                         App.Title, _
                                         App.FileDescription, _
                                         vsql, _
                                         lConexaoLocal, True)
'''mathayde
''        While Not rs.EOF
        If Not rs.EOF Then
            If rs("Item_Val_Estimativa") <> "4" And rs("Item_Val_Estimativa") <> "6" And _
                CDbl(rs("Val_Estimado")) - CDbl(rs("Val_Pago")) > 0 Then
                MousePointer = vbNormal  'cristovao.rodrigues
                If MsgBox("Existe(m) estimativa(s) com saldo a pagar " & _
                          "(Indeniza��o, Honor�rios, Despesas, Desp.Ressarciamento ou Desp. Salvados). " & _
                          "Confirma ajuste de saldo autom�tico?", _
                          vbYesNo + vbQuestion + vbDefaultButton2) = vbNo Then
                          
                    RetornarTransacao (lConexaoLocal) 'cristovao.rodrigues 23/10/2012
                    MousePointer = vbNormal  'cristovao.rodrigues
                    Exit Sub
                
                End If
            
            End If
''mathayde
''            rs.MoveNext
''        Wend
        End If
        
        rs.Close
            
'        SQL = "SELECT count(*) FROM pgto_sinistro_tb WITH (NOLOCK) " & vbNewLine
'        SQL = SQL & "WHERE pgto_sinistro_tb.sinistro_id             = " & dSinistro_ID & vbNewLine
'        SQL = SQL & "  AND pgto_sinistro_tb.situacao_op             = 'n'" & vbNewLine

        'FLOW 19427059 - GENJUNIOR - CONFITEC SP
        SQL = ""
        SQL = SQL & " SET NOCOUNT ON EXEC seguros_db.dbo.SEGS13153_SPS " & dSinistro_ID

        Set rs = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                    glAmbiente_id, _
                                     App.Title, _
                                     App.FileDescription, _
                                     SQL, _
                                     lConexaoLocal, True)
        
        If rs(0) > 0 Then
            MousePointer = vbNormal  'cristovao.rodrigues
            If MsgBox("Este sinistro possui pagamentos que n�o foram aprovados. " & _
                "Confirma encerramento aguardando aprova��o?", _
                vbYesNo + vbQuestion + vbDefaultButton2) = vbNo Then
                Exit Sub
                RetornarTransacao (lConexaoLocal) 'cristovao.rodrigues 23/10/2012
            Else
                Sinistro_Situacao = "3"
            End If
        Else
            rs.Close
            
'            SQL = "SELECT count(*) FROM pgto_sinistro_tb WITH (NOLOCK) "
'            SQL = SQL & "WHERE pgto_sinistro_tb.sinistro_id = " & dSinistro_ID
'            SQL = SQL & "  AND pgto_sinistro_tb.situacao_op = 'a' "
'            SQL = SQL & "  AND pgto_sinistro_tb.item_val_estimativa <> 4 "
'            SQL = SQL & "  AND pgto_sinistro_tb.item_val_estimativa <> 6 "
'            SQL = SQL & "  AND pgto_sinistro_tb.acerto_id is null "

            'FLOW 19427059 - GENJUNIOR - CONFITEC SP
            SQL = ""
            SQL = SQL & " SET NOCOUNT ON EXEC seguros_db.dbo.SEGS13154_SPS " & dSinistro_ID
            
            Set rs = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                    glAmbiente_id, _
                                     App.Title, _
                                     App.FileDescription, _
                                     SQL, _
                                     lConexaoLocal, True)
            If rs(0) > 0 Then
                MousePointer = vbNormal  'cristovao.rodrigues
                If MsgBox("Este sinistro possui pagamentos aguardando transfer�ncia para " & _
                    "o financeiro. Confirma encerramento aguardando transmiss�o para o financeiro?", _
                    vbYesNo + vbQuestion + vbDefaultButton2) = vbNo Then
                    RetornarTransacao (lConexaoLocal) 'cristovao.rodrigues 23/10/2012
                    Exit Sub
                Else
                    Sinistro_Situacao = "4"
                End If
            End If
        End If
        rs.Close
        
        If bytTipoRamo = "2" Or bytTipoRamo = "3" Then
            
            If ramo_id <> 60 And ramo_id <> 68 And ramo_id <> 70 Then
                
              'mathayde
                vsql = "SELECT qld_tecnica_vistoria = isnull(vistoria_tb.qld_tecnica_vistoria,0), " & vbNewLine
                vsql = vsql & "vistoria_tb.dt_parecer_vistoria, " & vbNewLine
                vsql = vsql & " cump_procedimentos_vistoria = isnull(vistoria_tb.cump_procedimentos_vistoria,0) " & vbNewLine
                vsql = vsql & " FROM vistoria_tb WITH (NOLOCK) INNER JOIN regulador_tb WITH (NOLOCK) " & vbNewLine
                vsql = vsql & " ON vistoria_tb.regulador_id = regulador_tb.regulador_id " & vbNewLine
                vsql = vsql & " LEFT JOIN Regulador_pj_tb WITH (NOLOCK) " & vbNewLine
                vsql = vsql & " ON Regulador_tb.Regulador_id = Regulador_pj_tb.Regulador_id " & vbNewLine
                vsql = vsql & " LEFT JOIN Regulador_pf_tb WITH (NOLOCK) " & vbNewLine
                vsql = vsql & " ON Regulador_tb.Regulador_id = Regulador_pf_tb.Regulador_id " & vbNewLine
                vsql = vsql & " INNER JOIN Municipio_tb WITH (NOLOCK) " & vbNewLine
                vsql = vsql & " ON Regulador_tb.municipio_id = Municipio_tb.municipio_id " & vbNewLine
                vsql = vsql & " AND Regulador_tb.estado = Municipio_tb.estado " & vbNewLine
                vsql = vsql & " WHERE vistoria_tb.sinistro_id = " & dSinistro_ID & vbNewLine
                vsql = vsql & " ORDER BY regulador_tb.regulador_id"
                
                Set rs = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                    glAmbiente_id, _
                                     App.Title, _
                                     App.FileDescription, _
                                     vsql, _
                                     lConexaoLocal, True)
                While Not rs.EOF
                    If Trim(rs("dt_parecer_vistoria")) = "" Or _
                    rs("qld_tecnica_vistoria") = 0 Or _
                    rs("cump_procedimentos_vistoria") = 0 Then
                        MousePointer = vbNormal  'cristovao.rodrigues
                        MsgBox "As informa��es da vistoria n�o est�o preenchidas !", vbOKOnly + vbCritical
                        rs.Close
                        RetornarTransacao (lConexaoLocal) 'cristovao.rodrigues 23/10/2012
                        Exit Sub
                    End If
                    rs.MoveNext
                Wend
                
                rs.Close
                
            End If

        Else
            
           
            
'            vsql = "SELECT dt_parecer_vistoria = isnull(vistoria_tb.dt_parecer_vistoria,'') " & vbNewLine
'            vsql = vsql & " FROM vistoria_tb WITH (NOLOCK) INNER JOIN regulador_tb WITH (NOLOCK) " & vbNewLine
'            vsql = vsql & " ON vistoria_tb.regulador_id = regulador_tb.regulador_id " & vbNewLine
'            vsql = vsql & " LEFT JOIN Regulador_pj_tb WITH (NOLOCK) " & vbNewLine
'            vsql = vsql & " ON Regulador_tb.Regulador_id = Regulador_pj_tb.Regulador_id " & vbNewLine
'            vsql = vsql & " LEFT JOIN Regulador_pf_tb WITH (NOLOCK) " & vbNewLine
'            vsql = vsql & " ON Regulador_tb.Regulador_id = Regulador_pf_tb.Regulador_id " & vbNewLine
'            vsql = vsql & " INNER JOIN Municipio_tb WITH (NOLOCK) " & vbNewLine
'            vsql = vsql & " ON Regulador_tb.municipio_id = Municipio_tb.municipio_id " & vbNewLine
'            vsql = vsql & " AND Regulador_tb.estado = Municipio_tb.estado " & vbNewLine
'            vsql = vsql & " WHERE vistoria_tb.sinistro_id = " & dSinistro_ID & vbNewLine
'            'mathayde
'            vsql = vsql & " and isnull(vistoria_tb.dt_parecer_vistoria,'')  ='' " & vbNewLine
            'vSql = vSql & " ORDER BY regulador_tb.regulador_id"
            
            'FLOW 19427059 - GENJUNIOR - CONFITEC SP
            vsql = ""
            vsql = vsql & " SET NOCOUNT ON EXEC seguros_db.dbo.SEGS13155_SPS " & dSinistro_ID
            
            Set rs = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                    glAmbiente_id, _
                                     App.Title, _
                                     App.FileDescription, _
                                     vsql, _
                                     lConexaoLocal, True)
            'mathayde
            'While Not rs.EOF
            If Not rs.EOF Then
                'If rs("dt_parecer_vistoria") = "" Then
                    If Not vAvisou Then
                        vAvisou = True
                        MousePointer = vbNormal  'cristovao.rodrigues
                        If MsgBox("Este sinistro possui vistorias n�o encerradas." & _
                                "Confirma a finaliza��o autom�tica?", _
                                vbYesNo + vbQuestion + vbDefaultButton2) = vbNo Then
                            RetornarTransacao (lConexaoLocal) 'cristovao.rodrigues 23/10/2012
                            Exit Sub
                        End If
                    End If
                    Data_Fim_Vistoria = Format(gsDHEntrada, "dd/mm/yyyy")
                    Resultado_Fim_Vistoria = " "
                    Usuario_Vistoria = "SEGBR ENCERRAM."
                    Tipo_Vistoria = "A"
                'End If
            'Wend
            End If

            rs.Close
        End If
        
        MousePointer = vbHourglass  'cristovao.rodrigues
       'AbrirTransacao (lConexaoLocal) 'cristovao.rodrigues 23/10/2012
       
       'cristovao.rodrigues 16/10/2012
       ' Index:    0 = Administrativo (n�o comunica ao SEGUR)
       '           1 = Sem cobertura t�cnica (Comunica��o de Indeferimento ao SEGUR)
       '           2 = Duplicidade de registro (Solicita��o de Cancelamento do Aviso ao SEGUR)
       '           3 = Falta de documenta��o (Solicita��o de Cancelamento do Aviso ao SEGUR)
       '           4 = A pedido da l�der (n�o comunica ao SEGUR)
       '           5 = Cosseguro com valor inferior (n�o comunica ao SEGUR)
       '           6 = Liquidado
       '           7 = Desist�ncia do segurado (Solicita��o de Cancelamento do Aviso ao SEGUR)
       '           8 = Aviso em Cobertura Indevida
       '           9 = indeferimento (adminstrativo)
       If optEncerramento_Administrativo Then
        vSitSel = 0
       ElseIf optEncerramento_Tipo_Indeferimento Then
        vSitSel = 1
       ElseIf optEncerramento_Duplicidade_Registro Then
        vSitSel = 2
       ElseIf optEncerramento_Falta_Documento Then
        vSitSel = 3
       ElseIf optEncerramento_Pedido_Lider Then
        vSitSel = 4
       ElseIf optEncerramento_Cosseguro_Inferior Then
        vSitSel = 5
       ElseIf optEncerramento_Tipo_Liquidado Then
        vSitSel = 6
       ElseIf optEncerramento_Desistencia_Segurado Then
        vSitSel = 7
       ElseIf optEncerramento_Cobertura_Indevida Then
        vSitSel = 8
       ElseIf optEncerramento_Indeferimento_Administrativo Then
        vSitSel = 9
       End If
       
       
       'cristovao.rodrigues 01/11/2012 inicio
       Dim rsExigencia As ADODB.Recordset
       Dim rsExig_IDBB As ADODB.Recordset
       Dim Itens_Historico_exi() As String
       
        '235270 / v 521  Corre��o do processo de encerramento de sinistro.
        'Francisco Berrocal (Confitec) : 25/09/2012 : INC000003720227 : inicio
        'Na vers�o anterior (519) ocorria o seguinte erro:
        'Se existisse mais de 1 registro dava erro no resultset (j� estava sendo utilizado)
        'A corre��o abaixo foi feita pelo Genesco (Confitec)
        'Francisco Berrocal (Confitec) : 25/09/2012 : INC000003720227 : fim
        
        
'        vsql = ""
'        vsql = vsql & " SELECT ISNULL(EVENTO_SEGBR_ID, 0) " & vbNewLine
'        vsql = vsql & "   FROM SEGUROS_DB.DBO.EVENTO_SEGBR_SINISTRO_ATUAL_TB WITH (NOLOCK) " & vbNewLine
'        vsql = vsql & "  WHERE EVENTO_ID IN (SELECT MAX(EVENTO_ID) FROM SEGUROS_DB.DBO.EVENTO_SEGBR_SINISTRO_ATUAL_TB WITH (NOLOCK) " & vbNewLine
'        vsql = vsql & "                       WHERE SINISTRO_ID = " & gbldSinistro_ID & ") " & vbNewLine
        
        'FLOW 19427059 - GENJUNIOR - CONFITEC SP
        vsql = ""
        vsql = vsql & " SET NOCOUNT ON EXEC seguros_db.dbo.SEGS13156_SPS " & gbldSinistro_ID
        
        Set rsExigencia = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                    glAmbiente_id, _
                                     App.Title, _
                                     App.FileDescription, _
                                     vsql, _
                                     lConexaoLocal, True)
        
        If Not rsExigencia.EOF Then
       
                If rsExigencia(0) = 12004 Or rsExigencia(0) = 12006 Then
                
                        rsExigencia.Close
                        
                        Set rsExigencia = Nothing
                        
                        vsql = ""
                        vsql = vsql & " SELECT A.DT_AVISO_SINISTRO, B.SEQ_EXIGENCIA, B.DT_EXIGENCIA " & vbNewLine
                        vsql = vsql & "      , B.LISTA_RESUMIDA, B.ANEXO, B.EMAIL_SOLICITANTE, B.EMAIL_ESTIPULANTE, B.EMAIL_CORRETOR " & vbNewLine
                        vsql = vsql & "      , B.CORRESPONDENCIA_SOLICITANTE, B.CORRESPONDENCIA_ESTIPULANTE, B.CORRESPONDENCIA_CORRETOR " & vbNewLine
                        vsql = vsql & "      , B.SMS_SOLICITANTE, B.SMS_ESTIPULANTE, B.SMS_CORRETOR " & vbNewLine
                        vsql = vsql & "   FROM SEGUROS_DB.DBO.EXIGENCIA_TB B WITH (NOLOCK)" & vbNewLine
                        vsql = vsql & "   JOIN SEGUROS_DB.DBO.SINISTRO_TB A WITH (NOLOCK) " & vbNewLine
                        vsql = vsql & "     ON B.SINISTRO_ID = A.SINISTRO_ID " & vbNewLine
                        vsql = vsql & "  WHERE A.SINISTRO_ID = " & gbldSinistro_ID & vbNewLine
                        vsql = vsql & "    AND LOWER(B.SITUACAO) = 'p' "
                    
                        Set rsExigencia = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                                            glAmbiente_id, _
                                                             App.Title, _
                                                             App.FileDescription, _
                                                             vsql, _
                                                             lConexaoLocal, True)
                        
                        If Not rsExigencia.EOF Then
                        
                                    vsql = ""
                                    vsql = vsql & "EXEC SEGUROS_DB..SEGS9915_SPU " & gbldSinistro_ID & ", '" & _
                                                    Format(rsExigencia("DT_AVISO_SINISTRO"), "yyyymmDD") & "', " & _
                                                    rsExigencia("SEQ_EXIGENCIA") & ", '" & _
                                                    Format(rsExigencia("DT_EXIGENCIA"), "yyyymmDD") & "', '" & _
                                                    Replace(rsExigencia("LISTA_RESUMIDA"), "'", "''") & "', " & _
                                                    rsExigencia("ANEXO") & ", '" & _
                                                    rsExigencia("EMAIL_SOLICITANTE") & "', '" & _
                                                    rsExigencia("EMAIL_ESTIPULANTE") & "', '" & _
                                                    rsExigencia("EMAIL_CORRETOR") & "', '" & _
                                                    rsExigencia("CORRESPONDENCIA_SOLICITANTE") & "', '" & _
                                                    rsExigencia("CORRESPONDENCIA_ESTIPULANTE") & "', '" & _
                                                    rsExigencia("CORRESPONDENCIA_CORRETOR") & "', '" & _
                                                    rsExigencia("SMS_SOLICITANTE") & "', '" & _
                                                    rsExigencia("SMS_ESTIPULANTE") & "', '" & _
                                                    rsExigencia("SMS_CORRETOR") & "', 'c'"
                                                   ' rsExigencia("SMS_CORRETOR") & "', 'z'" '18225203 - ajuste no m�dulo de exigencia-
                                                    
                                 Call Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                                            glAmbiente_id, _
                                                             App.Title, _
                                                             App.FileDescription, _
                                                             vsql, _
                                                             lConexaoLocal, False)
                                                    
                                'rsExigencia.MoveNext
                                
                                rsExigencia.Close
                                
                                Set rsExigencia = Nothing
                                
                                vsql = ""
                                vsql = vsql & " SELECT SINISTRO_BB FROM SINISTRO_BB_TB WHERE SINISTRO_ID = " & gbldSinistro_ID & vbNewLine
                                vsql = vsql & "    AND (DT_FIM_VIGENCIA >= GETDATE() OR DT_FIM_VIGENCIA IS NULL) " & vbNewLine
                                
                                Set rsExig_IDBB = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                                                    glAmbiente_id, _
                                                                     App.Title, _
                                                                     App.FileDescription, _
                                                                     vsql, _
                                                                     lConexaoLocal, True)
                                
                                Itens_Historico_exi = Split("CANCELAMENTO DE EXIG�NCIA " & vbCrLf, vbCrLf)
                                
                                If rsExig_IDBB.EOF Then
                                    If Not Inclui_Evento("12008", Sinistro_Situacao, "'" & CStr(vSitSel) & "'", Itens_Historico_exi) Then
                                       MsgBox "Erro Inclus�o Evento Encerramento", vbOKOnly + vbCritical
                                       RetornarTransacao (lConexaoLocal)
                                       MousePointer = vbNormal
                                       Exit Sub
                                    End If
                                Else
                                    If Not Inclui_Evento("10139", Sinistro_Situacao, "'" & CStr(vSitSel) & "'", Itens_Historico_exi) Then
                                       MsgBox "Erro Inclus�o Evento Encerramento", vbOKOnly + vbCritical
                                       RetornarTransacao (lConexaoLocal)
                                       MousePointer = vbNormal
                                       Exit Sub
                                    End If
                                End If
                                
                                rsExig_IDBB.Close
                                
                                Set rsExig_IDBB = Nothing
                        
                        End If
                
                End If
        
        End If
        
        If Not rsExigencia Is Nothing Then
        
            rsExigencia.Close
            Set rsExigencia = Nothing
            
        End If
       
       'cristovao.rodrigues 01/11/2012 fim
        
        If dSinistro_ID <> "" Then
'            SQLAuxi = "select  count(*)"
'            SQLAuxi = SQLAuxi & " from sinistro_estimativa_tb WITH (NOLOCK) " & vbNewLine
'            SQLAuxi = SQLAuxi & " where sinistro_id =  " & dSinistro_ID & vbNewLine
'            SQLAuxi = SQLAuxi & " and seq_estimativa = (select max(seq_estimativa)" & vbNewLine
'            SQLAuxi = SQLAuxi & " from    sinistro_estimativa_tb se2 WITH (NOLOCK) " & vbNewLine
'            SQLAuxi = SQLAuxi & " where   se2.sinistro_id = sinistro_estimativa_tb.sinistro_id)" & vbNewLine

            'FLOW 19427059 - GENJUNIOR - CONFITEC SP
            SQLAuxi = ""
            SQLAuxi = SQLAuxi & " SET NOCOUNT ON EXEC seguros_db.dbo.SEGS13157_SPS " & dSinistro_ID
            
            Set rsAuxi = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                    glAmbiente_id, _
                                     App.Title, _
                                     App.FileDescription, _
                                     SQLAuxi, _
                                     lConexaoLocal, True)
            
            iQtdItemEstimativa = IIf(IsNull(rsAuxi(0)), 0, rsAuxi(0))
            rsAuxi.Close
        End If
        
        If Sinistro_Situacao = "3" Then
            linha = "AGUARDANDO APROVA��O" & Chr(10)
        ElseIf Sinistro_Situacao = "4" Then
            linha = "AGUARDANDO TRANSMISS�O PARA O FINANCEIRO" & Chr(10)
        Else
            Sinistro_Situacao = "2"
            linha = UCase(vOption_Escolhida)
        End If
        
        vAcertou = 0
        SQL = "EXEC sinistro_encerramento_spu " & dSinistro_ID & ","
        SQL = SQL & Apolice_id & ","
        SQL = SQL & Sucursal_id & ","
        SQL = SQL & Seguradora_id & ","
        SQL = SQL & ramo_id & ",'"
        SQL = SQL & Format(gsDHEntrada, "yyyymmdd") & "','"
        SQL = SQL & cUserName & "'"
        
        Call Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                    glAmbiente_id, _
                                     App.Title, _
                                     App.FileDescription, _
                                     SQL, _
                                     lConexaoLocal, False)
      
        
        If (optEncerramento_Tipo_Indeferimento.Value) Or (optEncerramento_Indeferimento.Value) Or (optEncerramento_Indeferimento_Administrativo.Value) Then
            vTp_detalhamento = 3
        ElseIf (optEncerramento_Duplicidade_Registro.Value) Or _
               (optEncerramento_Falta_Documento.Value) Or _
               (optEncerramento_Desistencia_Segurado.Value) Or _
               (optEncerramento_Cobertura_Indevida.Value) Then
            vTp_detalhamento = 4
        Else
            vTp_detalhamento = 0
        End If

        If vTp_detalhamento = 0 Then

            If (optEncerramento_Administrativo.Value) Or _
                (optEncerramento_Pedido_Lider.Value) Or _
                (optEncerramento_Cosseguro_Inferior.Value) Then
               
               ReDim Preserve Itens_historico(1)

               Itens_historico(0) = "ENCERRAMENTO"
            Else

               ReDim Preserve Itens_historico(1)

               Itens_historico(0) = UCase(vOption_Escolhida)
            End If
            If linha <> "" Then

               ReDim Preserve Itens_historico(1)

                Itens_historico(1) = linha
            End If

           
            If optEncerramento_Tipo_Liquidado.Value = True Then 'LIQUIDADO
  
                   Call ObterSinistrosCatastrofe(dSinistro_ID)
                   
                   If Sinistro_Catastrofe_Contador > 0 Then
                   
                        FrmDialogoEncerramento.Mensagem = ObterMensagemCatastrofe()
                        FrmDialogoEncerramento.Show vbModal
                        
                        If FrmDialogoEncerramento.Resposta = vbCancel Then
                            Exit Sub
                        ElseIf FrmDialogoEncerramento.Resposta = vbYes Then
                        
                            If MsgBox("Este processo foi caracterizado como sinistro cat�strofe?", _
                                        vbYesNo + vbQuestion) = vbYes Then
                                        
                                Sinistro_Catastrofe = Split(Mid(Colecao_Sinistro_id, 2), ",")
        
                                For i = 0 To Sinistro_Catastrofe_Contador
                                
                                        SQL = "SEGS7830_SPI "
                                        SQL = SQL & Sinistro_Catastrofe(i) & ","
                                        SQL = SQL & "'" & cUserName & "'"

                                        Call Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                                                    glAmbiente_id, _
                                                                    App.Title, _
                                                                    App.FileDescription, _
                                                                    SQL, _
                                                                    lConexaoLocal, _
                                                                    False)
                                Next
                                
                                MsgBox "Sinistros de cat�strofe gravados com �xito.", vbInformation + vbOKOnly
                                
                            End If
                        
                        End If
                   End If

                
                Select Case Verifica_Encerramento_ADM
                   
                   Case 0
                      'If Not Inclui_Evento("10104", Sinistro_Situacao, "'" & vOption_Escolhida & "'", Itens_historico) Then 'cristovao.rodrigues 16/10/2012
                      If Not Inclui_Evento("10104", Sinistro_Situacao, "'" & CStr(vSitSel) & "'", Itens_historico) Then
                         MsgBox "Erro Inclus�o Evento Encerramento", vbOKOnly + vbCritical
                         RetornarTransacao (lConexaoLocal)
                         Exit Sub
                      End If
                      
                   Case 1
                      'If Not Inclui_Evento("10120", Sinistro_Situacao, "'" & vOption_Escolhida & "'", Itens_historico) Then 'cristovao.rodrigues 16/10/2012
                      If Not Inclui_Evento("10120", Sinistro_Situacao, "'" & CStr(vSitSel) & "'", Itens_historico) Then
                         MsgBox "Erro Inclus�o Evento Encerramento", vbOKOnly + vbCritical
                         RetornarTransacao (lConexaoLocal)
                         Exit Sub
                     End If
                     
                   Case Else
                      RetornarTransacao (lConexaoLocal)
                      Exit Sub
                    
                End Select
                   

               
            Else

               'If Not Inclui_Evento("10120", Sinistro_Situacao, "'" & vOption_Escolhida & "'", Itens_historico) Then 'cristovao.rodrigues 16/10/2012
               If Not Inclui_Evento("10120", Sinistro_Situacao, "'" & CStr(vSitSel) & "'", Itens_historico) Then
                  MsgBox "Erro Inclus�o Evento Encerramento", vbOKOnly + vbCritical
                  RetornarTransacao (lConexaoLocal)
                  Exit Sub
               End If
            End If
        Else ' Indeferimento ou solicita��o de cancelamento
        
            vDetalhamento = ""
                        
                        ' CONFITEC - INCIDENTE (IM00898843)- inicio
                        vDetalhou = False
     
            For i = 0 To 3
                If Trim(txtDetalhe(i).Text) <> "" Then
                    vDetalhou = True
                    Exit For
                End If
            Next i
                        
            If vDetalhou Then
                        ' CONFITEC - INCIDENTE (IM00898843)- fim
                For i = 0 To 3
                    If Trim(txtDetalhe(i).Text) <> "" Then
                       vDetalhamento = vDetalhamento & UCase(txtDetalhe(i).Text & Chr(10))
                    End If
                Next i
                

                If FmePessoa_Exposta.Visible = True Then
                    If optSim.Value = True Then
                        vDetalhamento = vDetalhamento & "Pessoa Politicamente Exposta, h� ind�cios de irregularidade? SIM"
                    End If
                
                    If optNao.Value = True Then
                        vDetalhamento = vDetalhamento & "Pessoa Politicamente Exposta, h� ind�cios de irregularidade? N�O"
                    End If
                End If
                
                vRestrito = "N"

                'If Not Manutencao_Detalhes(CStr(vOption_Escolhida)) Then cristovao.rodrigues 16/10/2012 substituida
                If Not Manutencao_Detalhes(CStr(vSitSel)) Then
                    RetornarTransacao (lConexaoLocal)
                    MsgBox "Erro Inclus�o Detalhe Encerramento", vbOKOnly + vbCritical
                    'RetornarTransacao (lConexaoLocal) 'cristovao.rodrigues 23/10/2012 comentei 03/04/2013
                    Exit Sub
                End If
             End If
        End If
                
        If Not Envia_Proposta_Cloud(Proposta_id, dSinistro_ID) Then 'Anderson Molina Silva - Confitec SP - 07/10/2019
            RetornarTransacao (lConexaoLocal)
            MsgBox "Erro Inclus�o Proposta Cloud", vbOKOnly + vbCritical
            Exit Sub
        End If
        
       If optEncerramento_Duplicidade_Registro.Value Then
            
                SinBB_Tipo = "R"
                SinBB_Situacao = "Inativo"
                SQL = "EXEC sinistro_bb_spd "
                SQL = SQL & dSinistro_ID
                SQL = SQL & ", " & Apolice_id
                SQL = SQL & ", " & Sucursal_id
                SQL = SQL & ", " & Seguradora_id
                SQL = SQL & ", " & ramo_id
                SQL = SQL & ", " & Nro_BB(dSinistro_ID)
                SQL = SQL & ", '" & cUserName & "'"
                rdocn.OpenResultset (SQL)
            
        End If
    
        'If Not Manutencao_Vistorias("A", "'" & CStr(vOption_Escolhida) & "'") Then 'cristovao.rodrigues 16/10/2012
        If Not Manutencao_Vistorias("A", "'" & CStr(vSitSel) & "'") Then
            RetornarTransacao (lConexaoLocal)
            MsgBox "Erro Encerramento de Vistorias", vbOKOnly + vbCritical
            RetornarTransacao (lConexaoLocal) 'cristovao.rodrigues 23/10/2012
            Exit Sub
        End If
        
        If Produto_id = 716 Then 'ProdIdOuroVidaMulher
            Call VerificarExclusaoCoberturaDGN(dSinistro_ID, Sucursal_id, Apolice_id, Seguradora_id, ramo_id)
        End If
        
    
            
    vTp_detalhamento = 0
    vRestrito = "N"
    'Anota no detalhe o encerramento
    If vAcertou > 0 And vAvisou Then
        If vAcertou > 0 Then linha = linha & "ACERTO DE SALDO DE ESTIMATIVAS PELO FECHAMENTO." & Chr(10)
        If vAvisou Then linha = linha & "VISTORIA(S) ENCERRADA(S) PELO SISTEMA." & Chr(10)
    End If
    
    vDetalhamento = ""
    

    vDetalhamento = FrmMotivoEncerramento.Detalhe_Encerra & Chr(10) & "ENCERRAMENTO" & Chr(10) & linha

    If FrmMotivoEncerramento.Detalhe_Encerra <> "" Then
        FrmMotivoEncerramento.Detalhe_Encerra = ""
    End If

    If Not Manutencao_Detalhes Then
        RetornarTransacao (lConexaoLocal)
        MsgBox "Erro Inclus�o Detalhe Encerramento", vbOKOnly + vbCritical
        RetornarTransacao (lConexaoLocal) 'cristovao.rodrigues 23/10/2012
        Exit Sub
    End If
    
    bytModoOperacao = "5"
    
    If bGTR_Implantado Then
    'If GTR_Implantado Then
       '* Altera o status do evento_visualizado para 's'
      
       If Atualiza_Visualizacao_Eventos(dSinistro_ID) = False Then
          RetornarTransacao (lConexaoLocal)
          MsgBox "Erro na Atualiza��o da visualiza��o de eventos", vbOKOnly + vbCritical
          RetornarTransacao (lConexaoLocal) 'cristovao.rodrigues 23/10/2012
          Exit Sub
       End If
    End If
    
    If dSinistro_ID <> "" Then
        SQLAuxi = "select  count(*)"
        SQLAuxi = SQLAuxi & " from sinistro_estimativa_tb WITH (NOLOCK) "
        SQLAuxi = SQLAuxi & " where sinistro_id =  " & dSinistro_ID
        SQLAuxi = SQLAuxi & " and seq_estimativa = (select max(seq_estimativa)"
        SQLAuxi = SQLAuxi & " from    sinistro_estimativa_tb se2 WITH (NOLOCK) "
        SQLAuxi = SQLAuxi & " where   se2.sinistro_id = sinistro_estimativa_tb.sinistro_id)"
        
        Set rsAuxi = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                    glAmbiente_id, _
                                     App.Title, _
                                     App.FileDescription, _
                                     SQLAuxi, _
                                     lConexaoLocal, True)
        
        If iQtdItemEstimativa > rsAuxi(0) Then
            MsgBox "Sequencial de estimativas incorreto !" & vbCrLf _
                    & "Por favor, abra um FLOWBR relatando este problema com o print da tela!" & vbCrLf _
                    & "Altera��o n�o efetuada!", vbExclamation
            RetornarTransacao (lConexaoLocal)
            Unload Me
        End If
        
        rsAuxi.Close
    
    End If
    

    If FmePessoa_Exposta.Visible = True Then
        Dim emails As String, assuntoEmail As String, mensagemEmail As String
        Dim msgSegurado As String, msgBeneficiario As String, msgPPE As String
        Dim valImportanciaSegurada As String
        Dim dtContratacao As Date
        
        Dim rsPPE As ADODB.Recordset
        Dim SqlPPE As String
        
        ' Obt�m o e-mail dos gestores
        SqlPPE = "EXEC SEGS7714_SPS"
        Set rsPPE = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                    glAmbiente_id, _
                                     App.Title, _
                                     App.FileDescription, _
                                     SqlPPE, _
                                     lConexaoLocal, True)
        If Not rsPPE.EOF Then
            While rsPPE.EOF = False
                emails = emails & rsPPE("email") & ";"
                rsPPE.MoveNext
            Wend
        Else
            MsgBox "Nenhum gestor encontrado.", vbExclamation
            RetornarTransacao (lConexaoLocal)
            Unload Me
        End If
        rsPPE.Close
        Set rsPPE = Nothing
        
        'Recupera o(s) segurado(s)
        SqlPPE = "EXEC SEGS7709_SPS " & vbNewLine
        SqlPPE = SqlPPE & " @sinistro_id = " & dSinistro_ID & ", " & vbNewLine
        SqlPPE = SqlPPE & " @tp_pesquisa = 'segurado'"
        
        Set rsPPE = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                    glAmbiente_id, _
                                     App.Title, _
                                     App.FileDescription, _
                                     SqlPPE, _
                                     lConexaoLocal, True)
        If Not rsPPE.EOF Then
            While rsPPE.EOF = False
                msgSegurado = msgSegurado & "Nome do Segurado: " & rsPPE("Nome") & vbNewLine & "CPF / CNPJ do Segurado: " & rsPPE("CPF") & vbNewLine
                rsPPE.MoveNext
            Wend
        End If
        rsPPE.Close
        Set rsPPE = Nothing
        
        ' Recupera o(s) beneficiario(s)
        SqlPPE = "EXEC SEGS7709_SPS " & vbNewLine
        SqlPPE = SqlPPE & " @sinistro_id = " & dSinistro_ID & ", " & vbNewLine
        SqlPPE = SqlPPE & " @tp_pesquisa = 'beneficiario'"
        
        Set rsPPE = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                    glAmbiente_id, _
                                     App.Title, _
                                     App.FileDescription, _
                                     SqlPPE, _
                                     lConexaoLocal, True)
        If Not rsPPE.EOF Then
            While rsPPE.EOF = False
                msgBeneficiario = msgBeneficiario & "Nome do Benefici�rio: " & rsPPE("Nome") & vbNewLine & "CPF / CNPJ do Benefici�rio: " & rsPPE("CPF") & vbNewLine
                rsPPE.MoveNext
            Wend
        End If
        rsPPE.Close
        Set rsPPE = Nothing
        
        ' Recupera o CPF Politicamente Exposto
        SqlPPE = "EXEC SEGS7709_SPS " & vbNewLine
        SqlPPE = SqlPPE & " @sinistro_id = " & dSinistro_ID & ", " & vbNewLine
        SqlPPE = SqlPPE & " @tp_pesquisa = 'ppe'"
        
        Set rsPPE = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                    glAmbiente_id, _
                                     App.Title, _
                                     App.FileDescription, _
                                     SqlPPE, _
                                     lConexaoLocal, True)
        If Not rsPPE.EOF Then
            msgPPE = "CPF politicamente exposto: " & rsPPE("CPF") & vbNewLine
        End If
        rsPPE.Close
        Set rsPPE = Nothing
        
        ' Recupera o valor da Import�ncia Segurada e a data de contrata��o
        
        SqlPPE = "EXEC SEGS7715_SPS " & vbNewLine
        SqlPPE = SqlPPE & " @sinistro_id = " & dSinistro_ID
        
        Set rsPPE = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                    glAmbiente_id, _
                                     App.Title, _
                                     App.FileDescription, _
                                     SqlPPE, _
                                     lConexaoLocal, True)
        If Not rsPPE.EOF Then
           'INC000004306122 - HENRIQUE H. HENRIQUES CONFITEC (28/04/2014) - INICIO
            'valImportanciaSegurada = Format(Replace(rsPPE("importancia_segurada"), ".", ","), "#,##0.00")
            valImportanciaSegurada = Format(Replace(IIf(IsNull(rsPPE("importancia_segurada")) = True, "0,00", rsPPE("importancia_segurada")), ".", ","), "#,##0.00")
           'INC000004306122 - HENRIQUE H. HENRIQUES CONFITEC (28/04/2014) - FIM
            dtContratacao = CDate(rsPPE("dt_contratacao"))
        End If
        rsPPE.Close
        Set rsPPE = Nothing
        
        ' Gera o t�tulo do e-mail
        assuntoEmail = "Processo de sinistro " & dSinistro_ID & " - Pessoa Politicamente Exposta"

        ' Gera o corpo do e-mail
        mensagemEmail = "Sinistro: " & dSinistro_ID & vbNewLine
        mensagemEmail = mensagemEmail & msgSegurado
        mensagemEmail = mensagemEmail & msgBeneficiario
        mensagemEmail = mensagemEmail & "Capital / IS: " & valImportanciaSegurada & vbNewLine
        mensagemEmail = mensagemEmail & "Data de Contrata��o: " & Format(dtContratacao, "dd/mm/yyyy") & vbNewLine
        mensagemEmail = mensagemEmail & msgPPE
        mensagemEmail = mensagemEmail & "Trata-se de processo de pessoa politicamente exposto." & vbNewLine
        mensagemEmail = mensagemEmail & "Ap�s an�lise do processo," & vbNewLine
        If optSim.Value = True Then
            mensagemEmail = mensagemEmail & "h� ind�cio de irregularidade."
        ElseIf optNao.Value = True Then
            mensagemEmail = mensagemEmail & "n�o h� ind�cio de irregularidade."
        End If
               
        enviaEmail emails, assuntoEmail, mensagemEmail
        
    End If
    
    
    'PPELEGRINI   17919477 - INICIO
    'Marcio.Nogueira - Demanda 18319298 - BB Cr�dito Protegido Empresa Fora do Cronograma - Fase 3 - Inclus�o Produto 1231 - 23/07/2015
    'Marcio.Nogueira - Demanda 18319298 - BB Cr�dito Protegido Empresa Fora do Cronograma - Fase 3 - remo��o da op��o de encerramento administrativo - 23/07/2015
    If (Produto_id = 1225 Or Produto_id = 1231) And ((optEncerramento_Tipo_Indeferimento.Value) Or (optEncerramento_Indeferimento.Value)) Then  ' Or (optEncerramento_Administrativo.Value)) Then
                
        SQL = "SEGS11867_SPI "
        SQL = SQL & Proposta_id & ","
        SQL = SQL & gbldSinistro_ID & "," 'Marcio.Nogueira - Demanda 18319298 - BB Cr�dito Protegido Empresa Fora do Cronograma - Fase 3 - Parametro do Sinistro_ID -
        'SQL = SQL & Cliente_id & ","
        SQL = SQL & "'" & cUserName & "'"
        
        Call Conexao_ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, SQL, lConexaoLocal, False)
    
    End If
    'PPELEGRINI   17919477 - FIM
    'MARCIO.NOGUEIRA 18319298 - FIM
    'Demanda 17922837 - IN�CIO
    Sinistro_BB = Nro_BB(dSinistro_ID)
    
    vsql = "Select Count(1) From evento_segbr_sinistro_atual_tb with(nolock) "
    vsql = vsql & " Where sinistro_id = " & dSinistro_ID
    vsql = vsql & " And evento_bb_id = 1100 "
    vsql = vsql & " And IsNull(sinistro_BB,0) = 0 "
    vsql = vsql & " And banco_aviso_id = 1 "

    Set rs = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                 glAmbiente_id, _
                                 App.Title, _
                                 App.FileDescription, _
                                 vsql, _
                                 lConexaoLocal, True)

    If rs(0) > 0 And bytTipoRamo = 1 Then
    
        vsql = "Select IsNull(evento_id,0), IsNull(Convert(Char, num_remessa),'') From evento_segbr_sinistro_atual_tb With (nolock) Where sinistro_id = " & dSinistro_ID & " And evento_bb_id = 1100"
        
        Set rs = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                 glAmbiente_id, _
                 App.Title, _
                 App.FileDescription, _
                 vsql, _
                 lConexaoLocal, True)
        
        If Not rs.EOF Then
            iEvento = rs(0)
            sRemessa = Trim(rs(1))
            
            rs.Close
            
            If iEvento > 0 Then
                'Atualizar a situa��o do sinistro no tabel�o
                vsql = "EXEC evento_segbr_sinistro_situacao_aviso_spu "
                vsql = vsql & iEvento & ","
                vsql = vsql & "'SB',"
                vsql = vsql & "'" & cUserName & "',"
                vsql = vsql & "'A',"
                vsql = vsql & "Null,"
                vsql = vsql & "Null"
                
                Call Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                    glAmbiente_id, _
                                    App.Title, _
                                    App.FileDescription, _
                                    vsql, _
                                    vNunCx, _
                                    False, _
                                    False)
                
                'Atualizar a tabela sinistro_tb para sem_comunicacao_bb = null
                vsql = "EXEC SEGS10268_SPU "
                vsql = vsql & dSinistro_ID & ","
                vsql = vsql & "Null"
                
                Call Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                    glAmbiente_id, _
                                    App.Title, _
                                    App.FileDescription, _
                                    vsql, _
                                    vNunCx, _
                                    False, _
                                    False)
                                    
                                    
            End If
            
            If sRemessa <> "" Then
            
                'Dar baixa no log
                vsql = "Select IsNull(saida_gtr_id,0) "
                vsql = vsql & " From interface_db.dbo.saida_gtr_atual_tb With (nolock) "
                vsql = vsql & " Where cod_remessa = '" & sRemessa & "' "
                vsql = vsql & " And evento_BB = 1100 "
                vsql = vsql & " And situacao_mq in ('N','P','E') "
                
                Set rs = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                         glAmbiente_id, _
                         App.Title, _
                         App.FileDescription, _
                         vsql, _
                         lConexaoLocal, True)
                
                If Not rs.EOF Then
                    iSaida = rs(0)
                    
                    rs.Close
                    
                    If iSaida > 0 Then
                    
                        vsql = "EXEC interface_db.dbo.correcao_remessa_spu "
                        vsql = vsql & iSaida & ","
                        vsql = vsql & "'t',"
                        vsql = vsql & "1,"
                        vsql = vsql & "'" & cUserName & "'"
                        
                        Call Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                            glAmbiente_id, _
                                            App.Title, _
                                            App.FileDescription, _
                                            vsql, _
                                            vNunCx, _
                                            False, _
                                            False)
                        
                    End If
                End If
            End If
        End If
    End If
    'Demanda 17922837 - FIM

    ConfirmarTransacao (lConexaoLocal)
    
    

   
    MsgBox "Processo realizado com sucesso!", vbOKOnly + vbInformation
    
    
    
    
    ''Alterado por Junior em 30/01/2003
    If bGTR_Implantado Then
    'If GTR_Implantado Then
       If (optEncerramento_Tipo_Indeferimento.Value) Or (optEncerramento_Indeferimento.Value) Or (optEncerramento_Administrativo.Value) Then 'INDEFERIMENTO
          MsgBox "O Indeferimento ser� emitido Autom�ticamente pelo GTR.", vbOKOnly + vbInformation
       
       ElseIf (optEncerramento_Duplicidade_Registro.Value) Or (optEncerramento_Falta_Documento.Value) Or (optEncerramento_Desistencia_Segurado.Value) Or (optEncerramento_Cobertura_Indevida.Value) Then 'CANCELAMENTO
          MsgBox "O Cancelamento ser� emitido Autom�ticamente pelo GTR.", vbOKOnly + vbInformation
       
       End If
    Else
       If (optEncerramento_Tipo_Indeferimento.Value) Or (optEncerramento_Indeferimento.Value) Or (optEncerramento_Administrativo.Value) Then 'INDEFERIMENTO
          MsgBox "O Indeferimento dever� ser emitido Manualmente no SEGUR.", vbOKOnly + vbInformation
       
       ElseIf (optEncerramento_Duplicidade_Registro.Value) Or (optEncerramento_Falta_Documento.Value) Or (optEncerramento_Desistencia_Segurado.Value) Or (optEncerramento_Cobertura_Indevida.Value) Then 'CANCELAMENTO
          MsgBox "O Cancelamento dever� ser emitido Manualmente no SEGUR.", vbOKOnly + vbInformation
       
       End If
    End If
    
    'R.FOUREAUX - 24/05/2020
    If (FrmEncerramento.optEncerramento_Tipo_Liquidado.Value) Then
        ExecutaAplicacao "SEGP1521", 0, gbldSinistro_ID & " 1" & " " & MotivoEncerramento
    End If
    
    If (FrmEncerramento.optEncerramento_Indeferimento.Value) Or (FrmEncerramento.optEncerramento_Tipo_Indeferimento.Value) Then
        ExecutaAplicacao "SEGP1521", 0, gbldSinistro_ID & " 2" & " " & MotivoEncerramento
    End If
    
    If (FrmEncerramento.optEncerramento_Administrativo.Value) Or (FrmEncerramento.optEncerramento_Indeferimento_Administrativo.Value) Then
        ExecutaAplicacao "SEGP1521", 0, gbldSinistro_ID & " 3" & " " & MotivoEncerramento
    End If
    
    If ((FrmEncerramento.optEncerramento_Falta_Documento.Value) Or (FrmEncerramento.optEncerramento_Desistencia_Segurado.Value) Or (FrmEncerramento.optEncerramento_Cobertura_Indevida.Value) Or (FrmEncerramento.optEncerramento_Duplicidade_Registro.Value)) Then
        ExecutaAplicacao "SEGP1521", 0, gbldSinistro_ID & " 4" & " " & MotivoEncerramento
    End If
    'R.FOUREAUX - 24/05/2020
    
    'mathayde
    Dim Retorno As Boolean
    TxtAuxiliar.Text = "OK"
    Retorno = EnvioSaidaAuxiliar(TxtAuxiliar.Text, FrmEncerramento)
    
    Unload Me
    Exit Sub
'Resume
Trata_Erro:

     MousePointer = 0
     vEncerrou = False
     RetornarTransacao (lConexaoLocal) 'cristovao.rodrigues 23/10/2012
     mensagem_erro 6, "Erro no encerramento!" & vbNewLine & Err.Description & vbNewLine & Err.Source & vbNewLine & Err.Number
     Unload Me
     Exit Sub
     'Resume
End Sub


Private Sub VerificarExclusaoCoberturaDGN(vSinistro_ID As String, vSucursal_id As String, vApolice_id As String, vSeguradora_id As String, vramo_id As String)


Dim sSQL As String
Dim sExclusao_Diagnostico As String

On Error GoTo Trata_Erro

    If MsgBox("Solicitar exclus�o de cobertura de diagn�stico de c�ncer?" & vbNewLine & _
              " Sinistro n� " & dSinistro_ID & vbNewLine & _
              " Ramo " & ramo_id & vbNewLine & _
              " Sucursal n� " & Sucursal_id & vbNewLine & _
              " C�d. Seguradora " & Seguradora_id & vbNewLine & _
              " Ap�lice n� " & Apolice_id, vbYesNo + vbQuestion) = vbYes Then
        
        sExclusao_Diagnostico = "S"
    
    Else
    
        sExclusao_Diagnostico = "N"
    
    End If
    
    sSQL = "exec atualiza_exclusao_diagnostico_sinistro_vida_spu "
    sSQL = sSQL & vSinistro_ID
    sSQL = sSQL & "," & vSucursal_id
    sSQL = sSQL & "," & vApolice_id
    sSQL = sSQL & "," & vSeguradora_id
    sSQL = sSQL & "," & vramo_id
    sSQL = sSQL & ",'" & sExclusao_Diagnostico & "'"
    sSQL = sSQL & ",'" & cUserName & "'"
    
    Call Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                            glAmbiente_id, _
                            App.Title, _
                            App.FileDescription, _
                            sSQL, _
                            lConexaoLocal, False)
    
    Exit Sub
    
Trata_Erro:
    RetornarTransacao (lConexaoLocal)
    Screen.MousePointer = 0
    TrataErroGeral ("VerificarExclusaoCoberturaDGN")

End Sub


Public Function Manutencao_Vistorias(ByVal Fl_Tipo As String, _
                                     ByVal Motivo_encerramento As String) As Boolean
    
    Dim rs As ADODB.Recordset, rsaux As ADODB.Recordset, vsql As String, SQL_AUX As String
    Dim Dt_temp As String
    Dim Mensagem As String, Destinatario As String
    Dim Itens_historico(0) As String
    Dim Formulario As Form
    Dim OEvento As String

    Mensagem = ""
    Destinatario = ""
    
    On Error GoTo Trata_Erro
    Manutencao_Vistorias = True
    
        vsql = "SELECT vistoria_tb.dt_pedido_vistoria, " & vbNewLine
        vsql = vsql & "vistoria_tb.regulador_id, " & vbNewLine
        vsql = vsql & "vistoria_tb.parecer_preliminar, " & vbNewLine
        vsql = vsql & "vistoria_tb.resultado_preliminar, " & vbNewLine
        vsql = vsql & "vistoria_tb.dt_fim_preliminar, " & vbNewLine
        vsql = vsql & "vistoria_tb.parecer_vistoria, " & vbNewLine
        vsql = vsql & "vistoria_tb.resultado_vistoria, " & vbNewLine
        vsql = vsql & "vistoria_tb.dt_parecer_vistoria, " & vbNewLine
        vsql = vsql & "vistoria_tb.usuario, " & vbNewLine
        vsql = vsql & "regulador_tb.nome as nome_regulador, " & vbNewLine
        vsql = vsql & "regulador_tb.banco, " & vbNewLine
        vsql = vsql & "regulador_tb.agencia, " & vbNewLine
        vsql = vsql & "regulador_tb.agencia_dv, " & vbNewLine
        vsql = vsql & "regulador_tb.conta_corrente, " & vbNewLine
        vsql = vsql & "regulador_tb.conta_corrente_dv, " & vbNewLine
        vsql = vsql & "regulador_pj_tb.cgc, " & vbNewLine
        vsql = vsql & "regulador_pf_tb.cpf, " & vbNewLine
        vsql = vsql & "regulador_tb.endereco, regulador_tb.bairro, municipio_tb.nome, " & vbNewLine
        vsql = vsql & "regulador_tb.estado, regulador_tb.cep, regulador_tb.ddd_1, regulador_tb.telefone_1, " & vbNewLine
        vsql = vsql & "vistoria_tb.qld_tecnica_preliminar, " & vbNewLine
        vsql = vsql & "vistoria_tb.cump_procedimentos_preliminar, " & vbNewLine
        vsql = vsql & "vistoria_tb.qld_tecnica_vistoria, " & vbNewLine
        vsql = vsql & "vistoria_tb.cump_procedimentos_vistoria, " & vbNewLine
        vsql = vsql & "regulador_tb.variacao_poupanca " & vbNewLine
        vsql = vsql & " FROM vistoria_tb WITH (NOLOCK) INNER JOIN regulador_tb WITH (NOLOCK) " & vbNewLine
        vsql = vsql & " ON vistoria_tb.regulador_id = regulador_tb.regulador_id " & vbNewLine
        vsql = vsql & " LEFT JOIN Regulador_pj_tb WITH (NOLOCK) " & vbNewLine
        vsql = vsql & " ON Regulador_tb.Regulador_id = Regulador_pj_tb.Regulador_id " & vbNewLine
        vsql = vsql & " LEFT JOIN Regulador_pf_tb WITH (NOLOCK) " & vbNewLine
        vsql = vsql & " ON Regulador_tb.Regulador_id = Regulador_pf_tb.Regulador_id " & vbNewLine
        vsql = vsql & " INNER JOIN Municipio_tb WITH (NOLOCK) " & vbNewLine
        vsql = vsql & " ON Regulador_tb.municipio_id = Municipio_tb.municipio_id " & vbNewLine
        vsql = vsql & " AND Regulador_tb.estado = Municipio_tb.estado " & vbNewLine
        vsql = vsql & " WHERE vistoria_tb.sinistro_id = " & dSinistro_ID & vbNewLine
        vsql = vsql & " ORDER BY regulador_tb.regulador_id"
        
        Set rs = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                    glAmbiente_id, _
                                     App.Title, _
                                     App.FileDescription, _
                                     vsql, _
                                     lConexaoLocal, True)

 If Not rs.EOF Then
        
            If Tipo_Vistoria <> "E" Then
 
                If Tipo_Vistoria = "N" Then
                    Itens_historico(0) = rs("nome_regulador")
                    vRegulador_id = rs("Regulador_id")
                    If Sinistro_Situacao = "5" Then
                       OEvento = "10132"    '* ACIONAMENTO DE REGULADOR SEM COMUNICA��O AO SEGUR
                    Else
                       OEvento = "10018"    '* ACIONAMENTO DE REGULADOR
                    End If
                    
                    If Not Inclui_Evento(OEvento, Sinistro_Situacao, Motivo_encerramento, Itens_historico) Then
                        Manutencao_Vistorias = False
                        MsgBox "Erro Inclus�o Evento In�cio Regula��o", vbOKOnly + vbCritical
                        Exit Function
                    End If
                
                End If
                If Tipo_Vistoria = "R" Then
                    Itens_historico(0) = rs("nome_regulador")
                    If Not Inclui_Evento("10019", Sinistro_Situacao, Motivo_encerramento, Itens_historico) Then
                        Manutencao_Vistorias = False
                        MsgBox "Erro Inclus�o Evento Exclus�o de Regulador", vbOKOnly + vbCritical
                        Exit Function
                    End If
                End If
                
                If (bytModoOperacao = "1" And Data_Fim_Vistoria <> "") Or (Trim(Data_Fim_Vistoria) <> "" And rs("dt_parecer_vistoria") = "N") Then
                    Itens_historico(0) = rs("nome_regulador")
                    If Not Inclui_Evento("10020", Sinistro_Situacao, Motivo_encerramento, Itens_historico) Then
                        Manutencao_Vistorias = False
                        MsgBox "Erro Inclus�o Evento Fim de Regula��o", vbOKOnly + vbCritical
                        Exit Function
                    End If
                End If
            End If
            If Tipo_Vistoria <> "E" Then
                If Tipo_Vistoria = "A" Then     'Alterada
                    vsql = "EXEC vistoria_spu " & _
                            dSinistro_ID & "," & _
                            Apolice_id & "," & _
                            Sucursal_id & "," & _
                            Seguradora_id & "," & _
                            ramo_id & ",'" & _
                            Format(rs("dt_pedido_vistoria"), "yyyymmdd") & "'," & _
                            rs("Regulador_id") & "," & _
                            IIf(rs("dt_fim_preliminar") <> "", "'" & _
                                Format(rs("dt_fim_preliminar"), "yyyymmdd") & "'", "NULL") & "," & _
                            IIf(rs("Resultado_Preliminar") <> "", "'" & _
                                rs("Resultado_Preliminar") & "'", "NULL") & "," & _
                            IIf(rs("Parecer_Preliminar") <> "", "'" & rs("Parecer_Preliminar") & "'", "NULL") & "," & _
                            IIf(rs("dt_parecer_vistoria") <> "", "'" & _
                                Format(rs("dt_parecer_vistoria"), "yyyymmdd") & "'", "NULL") & "," & _
                            IIf(rs("resultado_vistoria") <> "", "'" & _
                                rs("resultado_vistoria") & "'", "NULL") & "," & _
                            IIf(rs("parecer_vistoria") <> "", "'" & rs("Parecer_vistoria") & "'", "NULL") & "," & _
                            IIf(Fl_Tipo = "M", "'" & cUserName & "'", "'SEGBR ENCERRAMENTO'")
           
                            If bytTipoRamo = "2" Then
                                vsql = vsql & "," & IIf(rs("qld_tecnica_preliminar") = 0, "NULL", rs("qld_tecnica_preliminar"))
                                vsql = vsql & "," & IIf(rs("qld_tecnica_vistoria") = 0, "NULL", rs("qld_tecnica_vistoria"))
                                vsql = vsql & "," & IIf(rs("cump_procedimentos_preliminar") = 0, "NULL", rs("cump_procedimentos_preliminar"))
                                vsql = vsql & "," & IIf(rs("cump_procedimentos_vistoria") = 0, "NULL", rs("cump_procedimentos_vistoria"))
                            End If
                  
                ElseIf Tipo_Vistoria = "N" Then
                    vsql = "EXEC vistoria_spi " & _
                            dSinistro_ID & "," & _
                            Apolice_id & "," & _
                            Sucursal_id & "," & _
                            Seguradora_id & "," & _
                            ramo_id & "," & _
                            IIf(rs("Dt_Pedido_vistoria") <> "", "'" & _
                                Format(rs("Dt_Pedido_vistoria"), "yyyymmdd") & "'", "NULL") & "," & _
                            rs("Regulador_id") & "," & _
                            IIf(rs("Dt_Fim_Preliminar") <> "", "'" & _
                                Format(rs("Dt_Fim_Preliminar"), "yyyymmdd") & "'", "NULL") & "," & _
                            IIf(rs("Resultado_Preliminar") <> "", "'" & _
                                rs("Resultado_Preliminar") & "'", "NULL") & "," & _
                            IIf(rs("Parecer_Preliminar") <> "", "'" & rs("Parecer_Preliminar") & "'", "NULL") & "," & _
                            IIf(rs("dt_parecer_vistoria") <> "", "'" & _
                                Format(rs("dt_parecer_vistoria"), "yyyymmdd") & "'", "NULL") & "," & _
                            IIf(rs("Resultado_vistoria") <> "", "'" & _
                                rs("Resultado_vistoria") & "'", "NULL") & "," & _
                            IIf(rs("Parecer_vistoria") <> "", "'" & rs("Parecer_vistoria") & "'", "NULL") & "," & _
                            IIf(Fl_Tipo = "M", "'" & cUserName & "'", "'SEGBR ENCERRAMENTO'")
                     
                            If bytTipoRamo = "2" Then
                                vsql = vsql & "," & IIf(rs("qld_tecnica_preliminar") = 0, "NULL", rs("qld_tecnica_preliminar"))
                                vsql = vsql & "," & IIf(rs("qld_tecnica_vistoria") = 0, "NULL", rs("qld_tecnica_vistoria"))
                                vsql = vsql & "," & IIf(rs("cump_procedimentos_preliminar") = 0, "NULL", rs("cump_procedimentos_preliminar"))
                                vsql = vsql & "," & IIf(rs("cump_procedimentos_vistoria") = 0, "NULL", rs("cump_procedimentos_vistoria"))
                            End If
                            
                    Dt_temp = Right(Dt_inclusao, 4) & Mid(Dt_inclusao, 4, 2) & Left(Dt_inclusao, 2)
  
                        SQL_AUX = "SELECT isnull(email,'') email FROM regulador_tb WITH (NOLOCK) "
                        SQL_AUX = SQL_AUX & " WHERE regulador_id = " & rs("regulador_id")
                        
                        Set rsaux = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                        glAmbiente_id, _
                                         App.Title, _
                                         App.FileDescription, _
                                         SQL_AUX, _
                                         lConexaoLocal, True)
                        If Trim(rsaux(0)) <> "" Then
                            

                            If Monta_ReguladorEmail(LCase(Trim(rsaux(0)))) = True Then
                               MsgBox "E-mail com detalhamento do sinistro n� " & dSinistro_ID & " enviado com sucesso para " & Trim(rsaux(0)), vbInformation
                            End If
     
                            
                            Destinatario = LCase(Trim(rsaux(0)))
                            rs.Close
                            
                            Mensagem = Mensagem_VistPrest("IR", rs("NOME_REGULADOR"))
                            
'                            If vOperVisual Then
'                                Set Formulario = frmVisualSinistroCon
'                            Else
'                                Set Formulario = frmAvisoSinistro
'                            End If
'                            If Formulario.MAPISess.SessionID = 0 Then
'                               Logon_e_mail Formulario
'                            End If
                            
                            If SendMsg("IR", Mensagem, Destinatario, Formulario) = False Then
                                If MsgBox("N�o foi poss�vel enviar e-mail ao regulador " & rs("NOME_REGULADOR") & " . Continua ?", vbYesNo) = vbNo Then
                                    Manutencao_Vistorias = False
                                    Exit Function
                                End If
                            End If
                        Else
                            rs.Close
                            MsgBox "N�o foi poss�vel enviar e-mail ao regulador " & rs("NOME_REGULADOR") & " . E-mail n�o cadastrado !"
                        End If
                        rsaux.Close
        
                ElseIf Tipo_Vistoria = "R" Then

                    vsql = "EXEC vistoria_spd " & _
                            dSinistro_ID & "," & _
                            Apolice_id & "," & _
                            Sucursal_id & "," & _
                            Seguradora_id & "," & _
                            ramo_id & "," & _
                            IIf(rs("Dt_Pedido_Vistoria") <> "", "'" & _
                                Format(rs("dt_pedido_vistoria"), "yyyymmdd") & "'", "NULL") & "," & _
                            rs("nome_Regulador") & "," & _
                            IIf(Fl_Tipo = "M", "'" & cUserName & "'", "'SEGBR ENCERRAMENTO'")
 
                    
                    Dt_temp = Right(Dt_inclusao, 4) & Mid(Dt_inclusao, 4, 2) & Left(Dt_inclusao, 2)
    
                        SQL_AUX = "SELECT email FROM regulador_tb WITH (NOLOCK) "
                        SQL_AUX = SQL_AUX & " WHERE regulador_id = " & rs("Regulador_id")
                        
                        Set rsaux = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                                        glAmbiente_id, _
                                                         App.Title, _
                                                         App.FileDescription, _
                                                         SQL_AUX, _
                                                         lConexaoLocal, True)
                        If Trim(rsaux(0)) <> "" Then
                        
                            If Monta_ReguladorEmail(LCase(Trim(rsaux(0)))) = True Then
                               MsgBox "E-mail com detalhamento do sinistro n� " & dSinistro_ID & " enviado com sucesso para " & Trim(rsaux(0)), vbInformation
                            End If
                          
                            
                            Destinatario = LCase(Trim(rsaux(0)))
                            rs.Close
                            
                            Mensagem = Mensagem_VistPrest("FR", rs("nome_regulador"))
                            
                            If SendMsg("FR", Mensagem, Destinatario, Formulario) = False Then
                                Manutencao_Vistorias = False
                                Exit Function
                            End If
                        Else
                            rs.Close
                            MsgBox "N�o foi poss�vel enviar e-mail ao regulador " & rs("nome_regulador") & " . E-mail n�o cadastrado !"
                        End If
                        rsaux.Close
    
                End If
                Call Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                        glAmbiente_id, _
                                         App.Title, _
                                         App.FileDescription, _
                                         vsql, _
                                         lConexaoLocal, False)
            End If
    
        End If
 
    Exit Function
    
Trata_Erro:
'Resume
    TrataErroGeral "Manuten��o de Vistorias"
    Manutencao_Vistorias = False

End Function


Private Sub BtnVoltar_Click()
    FinalizaAuxiliar "CANCELADO"        'AKIO.OKUNO - 02/10/2012
End Sub

'AKIO.OKUNO - FInalizarAuxiliar - 02/10/2012
Private Sub FinalizaAuxiliar(sValor As String)
    Dim Retorno                                 As Boolean
    On Error GoTo Erro
    
    If BtnVoltar.Caption = "Cancelar" Then
        MsgBox " A opera��o de Encerramento do Sinistro foi Cancelada!"
        vEncerrou = False
        Unload Me
    End If
    
    If EstadoConexao(lConexaoLocal) = adStateOpen Then
        Retorno = FecharConexao(lConexaoLocal)
    End If
    
'    TxtAuxiliar.Text = "Cancelar"          'AKIO.OKUNO - 02/10/2012
    TxtAuxiliar.Text = "CANCELADO"          'AKIO.OKUNO - 02/10/2012
    
    End
    Exit Sub

Erro:
    Call TrataErroGeral("IniciarConexao", Me.name)
    Call FinalizarAplicacao
End Sub
Private Sub Form_Load()
    
    On Error GoTo Erro

    
    InicializaVariaveisLocal
    IniciarConexao
    InicializaInterfaceLocal
    

    Exit Sub
    
Erro:
    Call TrataErroGeral("Form_Load", Me.name)
    Call FinalizarAplicacao

Resume
End Sub

Private Sub InicializaVariaveisLocal()

Dim aParametros()                           As String
Dim bItemParametro                          As Byte
Dim sParametroLocal                         As String

    
    If Not Trata_Parametros(Command) Or InStr(1, Command, " ") = 0 Then
        FinalizarAplicacao
    Else
        Ambiente = ConvAmbiente(glAmbiente_id)
        
        sParametroLocal = Left(Command, InStrRev(Command, " "))
        aParametros = Split(sParametroLocal, ".")
        
        bytTipoRamo = aParametros(0)
        gsParamAplicacao = aParametros(1)
        sOperacao = gsParamAplicacao
        bytModoOperacao = Trim(gsParamAplicacao)
        dSinistro_ID = aParametros(2)
'AKIO.OKUNO - MP - INICIO - 17/02/2013
        If UBound(aParametros) >= 3 Then
            Data_Sistema = aParametros(3)
        Else
            Data_Sistema = ""
        End If
        If UBound(aParametros) >= 4 Then
            GTR_Implantado = CBool(aParametros(4))
            bolDefiniu_GTR_Implantado = True
        Else
            bolDefiniu_GTR_Implantado = False
        End If
        If UBound(aParametros) >= 5 Then
            retorno_entrada_GTR_tb = CBool(aParametros(5))
            bolDefiniu_Retorno_Entrada_GTR = True
        Else
            bolDefiniu_Retorno_Entrada_GTR = False
        End If
        If UBound(aParametros) >= 6 Then
            Localizacao_Processo = aParametros(6)
        Else
            Localizacao_Processo = ""
        End If
        If UBound(aParametros) >= 7 Then
            bolVerifica_Protocolacao = CBool(aParametros(7))
            bolDefiniu_Verifica_Protocolacao = True
        Else
            bolDefiniu_Verifica_Protocolacao = False
        End If
        
'        Data_Sistema = "17/02/2013"
'        GTR_Implantado = CBool(0)
'        bolDefiniu_GTR_Implantado = True
'        Localizacao_Processo = 2
'        bolVerifica_Protocolacao = CBool(0)
'        bolDefiniu_Verifica_Protocolacao = True
'        retorno_entrada_GTR_tb = True
'        bolDefiniu_Retorno_Entrada_GTR = True
        
'AKIO.OKUNO - MP - FIM - 17/02/2013
        
    End If
End Sub
Private Sub InicializaInterfaceLocal()
    On Error GoTo Erro
    
    Screen.MousePointer = vbHourglass
    
    If ValidaSegurancaLocal Then
        
        Call CentraFrm(Me)
        
        LimpaCamposTelaLocal Me
        
        If Data_Sistema = "" Then   'AKIO.OKUNO - MP - 17/02/2013
            ObterDataSistema (gsSIGLASISTEMA) 'cristovao.rodrigues 22/10/2012
        End If 'AKIO.OKUNO - MP - 17/02/2013
        
        InicializaModoOperacaoLocal
        
        InicializaInterfaceLocal_Form
        
    Else
        
        FinalizarAplicacao
    
    End If
    
    Screen.MousePointer = vbNormal
    Exit Sub
Resume
Erro:
'Resume
    Call TrataErroGeral("InicializaInterfaceLocal", Me.name)
    Call FinalizarAplicacao
    
End Sub


Private Sub IniciarConexao()
    On Error GoTo Erro

    'Tempor�rio - Confirmar se a conex�o vir� pelo par�metro ou n�o.
    'mathayde
    'vNunCx = 0
    

    If EstadoConexao(vNunCx) = adStateClosed Then
        lConexaoLocal = AbrirConexao(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription)
    End If
    
    'mathayde
    'lConexaoLocal = vNunCx
    vNunCx = lConexaoLocal
    
    Exit Sub

Erro:
    Call TrataErroGeral("IniciarConexao", Me.name)
    Call FinalizarAplicacao

End Sub


Private Sub LimpaCamposTelaLocal(objObjeto As Object)
    On Error GoTo Erro
    
    If Not objObjeto Is Nothing Then
        LimpaCamposTela objObjeto
    End If
    
    Exit Sub

Erro:
    Call TrataErroGeral("LimpaCamposTelaLocal", Me.name)
    Call FinalizarAplicacao

End Sub

Private Sub InicializaInterfaceLocal_Form()
    Dim sTipoProcesso_Descricao                 As String
    Dim i As Integer
    Dim Nro_Aviso As Double
    Dim vPode_Liquidar As Boolean
    Dim vsql As String
    Dim rs As ADODB.Recordset
    
    Call CentraFrm(Me)
    
    
    If Sinistro_Situacao = "" Then  'AKIO.OKUNO - MP - 17/02/2013
        vsql = "Select situacao from seguros_db.dbo.sinistro_tb WITH (NOLOCK) where sinistro_id = " & dSinistro_ID
        
        Set rs = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                        glAmbiente_id, _
                                        App.Title, _
                                        App.FileDescription, _
                                        vsql, _
                                        vNunCx, _
                                        True, _
                                        False)
                                        
        If Not rs.EOF Then
            Sinistro_Situacao = rs("situacao")
        End If
        rs.Close
    End If                          'AKIO.OKUNO - MP - 17/02/2013
    
    If Localizacao_Processo = "" Then   'AKIO.OKUNO - MP - 17/02/2013
        Localizacao_Processo = Obtem_Localizacao_Processo(dSinistro_ID)
    End If                          'AKIO.OKUNO - MP - 17/02/2013
    
    If Not bolDefiniu_GTR_Implantado Then   'AKIO.OKUNO - MP - 17/02/2013
        bGTR_Implantado = Obtem_GTR_Implantado(dSinistro_ID)
        bolDefiniu_GTR_Implantado = True    'AKIO.OKUNO - MP - 17/02/2013
    End If                                  'AKIO.OKUNO - MP - 17/02/2013
    
    'vObtem_GTR_Implantado = Obtem_GTR_Implantado(dSinistro_ID)
    
    Nro_Aviso = Nro_BB(dSinistro_ID)
    
    If Not bolDefiniu_Retorno_Entrada_GTR Then   'AKIO.OKUNO - MP - 17/02/2013
        Call Obtem_retorno_entrada_GTR_tb 'cristovao.rodrigues 09/10/2012
        bolDefiniu_Retorno_Entrada_GTR = True    'AKIO.OKUNO - MP - 17/02/2013
    End If                                  'AKIO.OKUNO - MP - 17/02/2013
    
''mathayde
''    BtnAplicar.Visible = (bytModoOperacao = 5)
''    BtnAplicar.Enabled = (bytModoOperacao = 5 And Trim(Localizacao_Processo) = 2)
''    BtnVoltar.Visible = (bytModoOperacao = 5)
''    BtnVoltar.Enabled = (bytModoOperacao = 5)
    BtnAplicar.Visible = (bytModoOperacao = 1)
    
    If bGTR_Implantado Then
    'If vObtem_GTR_Implantado Then
    'IM00061533 - Permitir o encerramento Administrativo - Cleber Sardo - 13/07/2017
        BtnAplicar.Enabled = (bytModoOperacao = 1 And Trim(Localizacao_Processo) = 2) Or Sinistro_Situacao = 5
    Else
        BtnAplicar.Enabled = (bytModoOperacao = 1)
    End If
    BtnVoltar.Visible = (bytModoOperacao = 1)
    BtnVoltar.Enabled = (bytModoOperacao = 1)


    
    If Nro_Aviso <> 0 Then
        If Existe_Doctos_Pendentes(Nro_Aviso) Then
            MsgBox "Existem Documentos pendentes para Protocolo.", vbInformation, "Aviso..."
            Unload Me
            Exit Sub
        End If
    End If
    
    If Pode_Liquidar Then
      vPode_Liquidar = True
    End If

    
   
    If bGTR_Implantado Then
    'If vObtem_GTR_Implantado Then
        
        Select Case Sinistro_Situacao
            Case 5
                
                FmeTipoEncerramento.Caption = " Encerramento sem comunica��o ao SEGUR "
                optEncerramento_Administrativo.Enabled = True
                optEncerramento_Administrativo.Value = True 'cristovao.rodrigues 07/11/2012
                optEncerramento_Pedido_Lider.Enabled = True
                optEncerramento_Cosseguro_Inferior.Enabled = True
                optEncerramento_Indeferimento_Administrativo.Visible = False
                
                optEncerramento_Tipo_Liquidado.Enabled = False
                optEncerramento_Tipo_Indeferimento.Enabled = False
                optEncerramento_Indeferimento.Visible = False
                optEncerramento_Falta_Documento.Enabled = False
                optEncerramento_Desistencia_Segurado.Enabled = False
                optEncerramento_Cobertura_Indevida.Enabled = False
                optEncerramento_Duplicidade_Registro.Visible = False
                
                
            Case Else
           
                FmeTipoEncerramento.Caption = " Solicita��o de Indeferimento de aviso ao SEGUR "
                optEncerramento_Administrativo.Visible = False
                optEncerramento_Pedido_Lider.Visible = False
                optEncerramento_Cosseguro_Inferior.Visible = False
                optEncerramento_Indeferimento_Administrativo.Visible = True
                optEncerramento_Indeferimento_Administrativo.Enabled = True
                Call optEncerramento_Indeferimento_Administrativo.Move(240, 720)
                
                optEncerramento_Tipo_Indeferimento.Visible = False
                optEncerramento_Indeferimento.Visible = True
                optEncerramento_Indeferimento.Enabled = True
                Call optEncerramento_Indeferimento.Move(240, 360)
                
                
                
                
                If Trim(Localizacao_Processo) = "2" Then
                    
                                        
                    optEncerramento_Tipo_Liquidado.Enabled = vPode_Liquidar
                    optEncerramento_Indeferimento.Enabled = Not vPode_Liquidar
                    optEncerramento_Indeferimento_Administrativo.Enabled = Not vPode_Liquidar
                    optEncerramento_Duplicidade_Registro.Enabled = Not vPode_Liquidar
                    optEncerramento_Falta_Documento.Enabled = Not vPode_Liquidar
                    optEncerramento_Desistencia_Segurado.Enabled = Not vPode_Liquidar
                    optEncerramento_Cobertura_Indevida.Enabled = Not vPode_Liquidar
                
                Else
                    optEncerramento_Tipo_Liquidado.Visible = True
                    optEncerramento_Tipo_Liquidado.Enabled = False
                    'optEncerramento_Indeferimento.Visible = False 'cristovao.rodrigues 17/10/2012
                    optEncerramento_Indeferimento.Enabled = False
                    optEncerramento_Indeferimento_Administrativo.Enabled = False
                    optEncerramento_Duplicidade_Registro.Enabled = False
                    optEncerramento_Falta_Documento.Enabled = False
                    optEncerramento_Desistencia_Segurado.Enabled = False
                    optEncerramento_Cobertura_Indevida.Enabled = False
                    
                End If
                
                
                
                
        End Select
    
    Else
    
    ' Index:    0 = Administrativo (n�o comunica ao SEGUR)
    '           1 = Sem cobertura t�cnica (Comunica��o de Indeferimento ao SEGUR)
    '           2 = Duplicidade de registro (Solicita��o de Cancelamento do Aviso ao SEGUR)
    '           3 = Falta de documenta��o (Solicita��o de Cancelamento do Aviso ao SEGUR)
    '           4 = A pedido da l�der (n�o comunica ao SEGUR)
    '           5 = Cosseguro com valor inferior (n�o comunica ao SEGUR)
    '           6 = Liquidado
    '           7 = Desist�ncia do segurado (Solicita��o de Cancelamento do Aviso ao SEGUR)
    '           8 = Aviso em Cobertura Indevida
    '           9 = Indeferimento (Administrativo)
        
        If retorno_entrada_GTR_tb = False Then vPode_Liquidar = False

        optEncerramento_Administrativo.Enabled = Not vPode_Liquidar
        optEncerramento_Pedido_Lider.Enabled = Not vPode_Liquidar
        optEncerramento_Cosseguro_Inferior.Enabled = Not vPode_Liquidar
        
        optEncerramento_Tipo_Liquidado.Enabled = vPode_Liquidar
        optEncerramento_Indeferimento_Administrativo.Enabled = Not vPode_Liquidar
        optEncerramento_Duplicidade_Registro.Enabled = Not vPode_Liquidar
        optEncerramento_Falta_Documento.Enabled = Not vPode_Liquidar
        optEncerramento_Desistencia_Segurado.Enabled = Not vPode_Liquidar
        optEncerramento_Cobertura_Indevida.Enabled = Not vPode_Liquidar
        
        optEncerramento_Tipo_Indeferimento.Enabled = Not vPode_Liquidar
        optEncerramento_Indeferimento.Visible = Not vPode_Liquidar
        
    End If
    
    If vPode_Liquidar And Sinistro_Situacao <> "5" Then
        optEncerramento_Tipo_Liquidado.Value = True
    End If
            
    For i = 0 To 3
        txtDetalhe(i).Visible = False
    Next i
    
    LblDetalhe.Visible = False
    
    
    Dim SQL          As String
    Dim rcsRecordSet As ADODB.Recordset
    Dim Politicamente_Exposta As Boolean
    
    SQL = "EXEC SEGS7709_SPS " & vbNewLine
    SQL = SQL & " @sinistro_id = " & dSinistro_ID & "," & vbNewLine
    SQL = SQL & " @tp_pesquisa = NULL"
    
    Set rcsRecordSet = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                        glAmbiente_id, _
                                        App.Title, _
                                        App.FileDescription, _
                                        SQL, _
                                        lConexaoLocal, True)
    
    If Not rcsRecordSet.EOF Then
        Politicamente_Exposta = True
    Else
        Politicamente_Exposta = False
    End If
    rcsRecordSet.Close
    
    If Politicamente_Exposta Then
        FmePessoa_Exposta.Visible = True
    Else
        FmePessoa_Exposta.Visible = False
    End If
    
    
    
    
    '----------------------------------------RETORNO DA VERS�O-----------------------------------------------------------INICIO
    '--- SOLICITADO PELO DEMANDANTE THIAGO, O RETORNO DA REGRA SOLICITADA, PARA QUE O RAMO RE POSSA PROSSEGIR COM O FLUXO
    '--- INFORMANDO QUE AP�S UMA NOVA ANALISE POR PARTE DO MESMO, VOLTAREMOS A REALIZAR O TRATAMENTO DESTA REGRA.
    '--- POR AGORA O TRATAMENTO FICAR� COMENTADO PARA QUE OS OUTROS RAMOS POSSAM PROSSEGUIR O TRABALHO.
    
    'DIFERENCIA��O DE SINISTRO COM OU SEM COMUNICA��O - HENRIQUE HORICH HENRIQUES (CONFITEC SP) - INC000004275599 - INICIO
'    Dim RSValid As ADODB.Recordset
'    SQL = ""
'    SQL = "SELECT COUNT(1) COMUNICADO FROM SEGUROS_DB.DBO.SINISTRO_BB_TB WITH (NOLOCK) WHERE SINISTRO_ID = " & dSinistro_ID
'    'SELECIONA SE EXISTE SINISTRO_BB PARA O SINISTRO, SE EXISTIR O SINISTRO POSSUI COMUNICA���O CASO CONTRARIO N�O.
'    Set RSValid = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
'                                            glAmbiente_id, _
'                                            App.Title, _
'                                            App.FileDescription, _
'                                            SQL, _
'                                            lConexaoLocal, True)
'    If RSValid.EOF Then 'RS TEM QUE RETORNAR PELO MENOS UM VALOR
'       MsgBox "Erro ao identificar tipo de comunica��o do sinsitro", vbCritical + vbOKOnly, "Encerramento do Sinistro"
'       FmeTipoEncerramento.Enabled = False
'       FmeEncerramento_Sinistro_Solicitacao_Cancelamento.Enabled = False
'       Exit Sub
'    Else
'       If RSValid("COMUNICADO").Value = 0 Then 'SINISTRO SEM COMUNICA��O
'          'INC000004334163 - CORRE��O DO TRATAMENTO - HENRIQUE H. HENRIQUES (CONFITEC) - INICIO
'          'FmeTipoEncerramento.Enabled = True
'          'FmeEncerramento_Sinistro_Solicitacao_Cancelamento.Enabled = False
'          optEncerramento_Indeferimento.Enabled = False
'          optEncerramento_Indeferimento_Administrativo.Enabled = True
'          If optEncerramento_Administrativo.Visible = True Then
'                optEncerramento_Administrativo.Enabled = True
'          End If
'          'INC000004334163 - CORRE��O DO TRATAMENTO - HENRIQUE H. HENRIQUES (CONFITEC) - FIM
'          For i = 0 To 3
'             txtDetalhe(i).Enabled = False
'          Next i
'       Else 'SINISTRO COM COMUNICA��O
'          'INC000004334163 - CORRE��O DO TRATAMENTO - HENRIQUE H. HENRIQUES (CONFITEC) - INICIO
'          'FmeTipoEncerramento.Enabled = False
'          'FmeEncerramento_Sinistro_Solicitacao_Cancelamento.Enabled = True
'          If Sinistro_Situacao = 5 Then
'             optEncerramento_Indeferimento_Administrativo.Enabled = True
'             If optEncerramento_Administrativo.Visible = True Then
'                optEncerramento_Administrativo.Enabled = True
'             End If
'          Else
'             optEncerramento_Indeferimento_Administrativo.Enabled = False
'             If optEncerramento_Administrativo.Visible = True Then
'                optEncerramento_Administrativo.Enabled = False
'             End If
'          End If
'          'INC000004334163 - CORRE��O DO TRATAMENTO - HENRIQUE H. HENRIQUES (CONFITEC) - FIM
'          For i = 0 To 3
'             txtDetalhe(i).Enabled = True
'          Next i
'       End If
'       RSValid.Close
'    End If
    'DIFERENCIA��O DE SINISTRO COM OU SEM COMUNICA��O - HENRIQUE HORICH HENRIQUES (CONFITEC SP) - INC000004275599 - FINAL
    
    '--- SOLICITADO PELO DEMANDANTE THIAGO, O RETORNO DA REGRA SOLICITADA, PARA QUE O RAMO RE POSSA PROSSEGIR COM O FLUXO
    '--- INFORMANDO QUE AP�S UMA NOVA ANALISE POR PARTE DO MESMO, VOLTAREMOS A REALIZAR O TRATAMENTO DESTA REGRA.
    '--- POR AGORA O TRATAMENTO FICAR� COMENTADO PARA QUE OS OUTROS RAMOS POSSAM PROSSEGUIR O TRABALHO.
    '----------------------------------------RETORNO DA VERS�O-----------------------------------------------------------FIM
    'mathayde
    'impossivel executar isso
    sTipoProcesso_Descricao = "Encerramento de Sinistro"
'    Select Case bytModoOperacao
'        Case "5"
'            sTipoProcesso_Descricao = "Encerramento de Sinistro"
'
'        Case "A"
'            sTipoProcesso_Descricao = "Encerramento de Sinistro Cosseguro"
'
'    End Select
        
    
    Me.Caption = App.EXEName & " - " & sTipoProcesso_Descricao & " - " & Ambiente
    

End Sub

Private Sub InicializaModoOperacaoLocal()
    On Error GoTo Erro
    
    InicializaModoOperacao Me, bytModoOperacao
        
    Exit Sub
    
Erro:
    Call TrataErroGeral("InicializaModoOperacao", Me.name)
    Call FinalizarAplicacao

End Sub

Private Function ValidaSegurancaLocal() As Boolean
    ValidaSegurancaLocal = True
End Function

Private Function Pode_Liquidar() As Boolean
    
    Dim rSQL                As String
    Dim rsRecordSet         As ADODB.Recordset
    Dim Dt_inicio_vigencia  As String
    
    On Error GoTo Erro
    
    Pode_Liquidar = False

    If bGTR_Implantado = False Then
    'If GTR_Implantado = False Then
       If CDbl(Valor_Pago_Estimativa_Indenizacao(dSinistro_ID)) > 0 Then
          Pode_Liquidar = True
       End If
       
       Exit Function
    
    End If

    rSQL = " "
    rSQL = "      SELECT dt_inicio_vigencia = ISNULL(MAX(sb.dt_inicio_vigencia), MAX(s.dt_aviso_sinistro))"
    rSQL = rSQL & " FROM sinistro_bb_tb sb WITH (NOLOCK) "
    rSQL = rSQL & "INNER JOIN sinistro_tb s WITH (NOLOCK) "
    rSQL = rSQL & "   ON (s.sinistro_id = sb.sinistro_id)"
    rSQL = rSQL & "WHERE sb.sinistro_id = " & dSinistro_ID
    rSQL = rSQL & "  AND sb.dt_fim_vigencia IS NULL"
    
    Set rsRecordSet = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                            glAmbiente_id, _
                                            App.Title, _
                                            App.FileDescription, _
                                            rSQL, _
                                            lConexaoLocal, True)
    If Not rsRecordSet.EOF Then
       Dt_inicio_vigencia = Format(rsRecordSet("dt_inicio_vigencia"), "YYYYMMDD")
    End If
    rsRecordSet.Close
    
    'verifica se existe algum pagamento para este sinistro bb a partir da data de inicio de vig�ncia
    rSQL = " "
    rSQL = "      SELECT COUNT(*)"
    rSQL = rSQL & " FROM pgto_sinistro_tb WITH (NOLOCK) "
    rSQL = rSQL & "WHERE sinistro_id             = " & dSinistro_ID
    rSQL = rSQL & "  AND item_val_estimativa      = 1"
    rSQL = rSQL & "  AND situacao_op              NOT IN ('c', 'e')"
    rSQL = rSQL & "  AND dt_solicitacao_pgto      >= '" & Dt_inicio_vigencia & "'"
    
    Set rsRecordSet = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                            glAmbiente_id, _
                                            App.Title, _
                                            App.FileDescription, _
                                            rSQL, _
                                            lConexaoLocal, True)
    If Not rsRecordSet.EOF Then
       If rsRecordSet(0) > 0 Then
          Pode_Liquidar = True
       End If
    End If
    rsRecordSet.Close
    
'---------------------------------------------------'
' INICIO - INC000004192943 - EDNETO - 14/10/2013    '
'    CONSULTA DESNECESS�RIA, ESTAVA IMPACTANDO      '
'        NO ENCERRAMENTO POR LIQUIDA��O.            '
'---------------------------------------------------'
'    rSQL = " "
'    rSQL = "SELECT "
'    rSQL = rSQL & " (SELECT COUNT(EVENTO_SEGBR_ID) FROM EVENTO_SEGBR_SINISTRO_TB E1 WITH (NOLOCK) "
'    rSQL = rSQL & " WHERE E1.SINISTRO_ID = " & dSinistro_ID & " AND E1.EVENTO_SEGBR_ID IN (10041) "
'    rSQL = rSQL & " and e1.sinistro_bb =  " & Nro_BB(dSinistro_ID) & ") PAGAMENTO,"
'    rSQL = rSQL & " (SELECT COUNT(EVENTO_SEGBR_ID) EVENTO_SEGBR_ID FROM EVENTO_SEGBR_SINISTRO_TB E1 WITH (NOLOCK)"
'    rSQL = rSQL & " WHERE E1.SINISTRO_ID = " & dSinistro_ID & " AND E1.EVENTO_SEGBR_ID IN (10105, 10062, 10076) "
'    rSQL = rSQL & " and e1.sinistro_bb = " & Nro_BB(dSinistro_ID) & ") ESTORNO"
'
'    Set rsRecordSet = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
'                                            glAmbiente_id, _
'                                            App.Title, _
'                                            App.FileDescription, _
'                                            rSQL, _
'                                            lConexaoLocal, True)
'    If Not rsRecordSet.EOF Then
'
'       If rsRecordSet(0) > rsRecordSet(1) Then
'          Pode_Liquidar = True
'       Else
'          Pode_Liquidar = False
'       End If
'    End If
'    rsRecordSet.Close
'---------------------------------------------------'
' FIM - INC000004192943 - EDNETO - 14/10/2013       '
'---------------------------------------------------'
    
    Exit Function

Erro:
    TrataErroGeral ("Pode Liquidar")
    
End Function

Private Function Nro_BB(sinistro_id)
Dim rSQL        As String
Dim rsRecordSet As ADODB.Recordset

'    rSQL = ""
'    rSQL = rSQL & "Select sinistro_bb "
'    rSQL = rSQL & " From sinistro_bb_tb WITH (NOLOCK) "
'    rSQL = rSQL & " Where Sinistro_id = " & sinistro_id
'    rSQL = rSQL & " And dt_fim_vigencia Is Null"

    'FLOW 19427059 - GENJUNIOR - CONFITEC SP
    rSQL = ""
    rSQL = rSQL & " SET NOCOUNT ON EXEC seguros_db.dbo.SEGS13150_SPS " & sinistro_id
    
    Set rsRecordSet = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                            glAmbiente_id, _
                                            App.Title, _
                                            App.FileDescription, _
                                            rSQL, _
                                            lConexaoLocal, True)
    
    If Not rsRecordSet.EOF Then
        Nro_BB = rsRecordSet(0)
    Else
        Nro_BB = 0
    End If

End Function




Public Function Existe_Doctos_Pendentes(ByVal OSinistro_BB As String) As Boolean
    
Dim rSQL As String
Dim rsRecordSet As ADODB.Recordset
    
    rSQL = "EXEC SEGS5978_SPS " & OSinistro_BB

    Set rsRecordSet = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                            glAmbiente_id, _
                                            App.Title, _
                                            App.FileDescription, _
                                            rSQL, _
                                            lConexaoLocal, True)
    If Not rsRecordSet.EOF Then
       Existe_Doctos_Pendentes = IIf(rsRecordSet("qtde_doctos") > 0, True, False)
    End If
    rsRecordSet.Close
    
End Function


Public Function Valor_Pago_Estimativa_Indenizacao(Numero_Sinistro As String) As Double
Dim sSQL As String
Dim rsRecordSet As ADODB.Recordset
    
    On Error GoTo Erro


'    sSQL = "Select top 1 Val_Pago                                             = IsNull(Val_Pago,0)              " & vbNewLine
'    sSQL = sSQL & " From Seguros_Db.Dbo.Sinistro_Estimativa_Tb          Sinistro_Estimativa_Tb WITH (NOLOCK)         " & vbNewLine
'    sSQL = sSQL & " Join Seguros_Db.Dbo.Sinistro_Tb                     Sinistro_Tb WITH (NOLOCK)                    " & vbNewLine
'    sSQL = sSQL & "   on Sinistro_Tb.Sinistro_ID                        = Sinistro_Estimativa_Tb.Sinistro_ID    " & vbNewLine
'    sSQL = sSQL & "Where Sinistro_Tb.Sinistro_ID = " & Numero_Sinistro & vbCrLf & vbNewLine
'    sSQL = sSQL & "  and Sinistro_Estimativa_Tb.item_val_estimativa = 1                                         " & vbNewLine
'    sSQL = sSQL & "  and seq_estimativa = (select max(seq_estimativa)                                            " & vbNewLine
'    sSQL = sSQL & "                          From Seguros_Db.Dbo.Sinistro_Estimativa_Tb                          " & vbNewLine
'    sSQL = sSQL & "                         where sinistro_id = '" & dSinistro_ID & "')                          " & vbNewLine
'    sSQL = sSQL & "Order By Seq_Estimativa desc                                                                  " & vbNewLine
    
    sSQL = ""
    sSQL = sSQL & " SET NOCOUNT ON EXEC seguros_db.dbo.SEGS13148_SPS " & Numero_Sinistro
    
    Set rsRecordSet = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                        glAmbiente_id, _
                                        App.Title, _
                                        App.FileDescription, _
                                        sSQL, _
                                        lConexaoLocal, True)
                                        
    If Not rsRecordSet.EOF Then
        
        Valor_Pago_Estimativa_Indenizacao = rsRecordSet(0)
        
    End If
    
    rsRecordSet.Close
    
    Exit Function
    
Erro:
    Call TrataErroGeral("Valor_Pago_Estimativa_Indenizacao", Me.name)
    Call FinalizarAplicacao

End Function

Private Sub optEncerramento_Administrativo_Click()
    Dim i As Integer
    
    optEncerramento_Tipo_Liquidado = False
    optEncerramento_Tipo_Indeferimento = False
    'optEncerramento_Administrativo = False
    optEncerramento_Pedido_Lider = False
    optEncerramento_Indeferimento = False
    optEncerramento_Cosseguro_Inferior = False
    optEncerramento_Indeferimento_Administrativo = False
    optEncerramento_Falta_Documento = False
    optEncerramento_Desistencia_Segurado = False
    optEncerramento_Cobertura_Indevida = False
    optEncerramento_Duplicidade_Registro = False
    
    
    For i = 0 To 3
    
        txtDetalhe(i).Visible = False
    
    Next i
    
    LblDetalhe.Visible = False
    optEncerramento_Falta_Documento.Value = False
    optEncerramento_Desistencia_Segurado.Value = False
    optEncerramento_Cobertura_Indevida.Value = False
    optEncerramento_Duplicidade_Registro.Value = False
    
    vSitSel = 1
    vOption_Escolhida = optEncerramento_Administrativo.Caption
End Sub

'mathayde
Sub ReconfiguraCampos()
    
    LblDetalhe.Visible = False
    optEncerramento_Falta_Documento.Value = False
    optEncerramento_Desistencia_Segurado.Value = False
    optEncerramento_Cobertura_Indevida.Value = False
    optEncerramento_Duplicidade_Registro.Value = False
    
End Sub

Private Sub optEncerramento_Cobertura_Indevida_Click()
    Dim i As Integer
    
    optEncerramento_Tipo_Liquidado = False
    optEncerramento_Tipo_Indeferimento = False
    optEncerramento_Administrativo = False
    optEncerramento_Pedido_Lider = False
    optEncerramento_Indeferimento = False
    optEncerramento_Cosseguro_Inferior = False
    optEncerramento_Indeferimento_Administrativo = False
    optEncerramento_Falta_Documento = False
    optEncerramento_Desistencia_Segurado = False
    'optEncerramento_Cobertura_Indevida = False
    optEncerramento_Duplicidade_Registro = False


    For i = 0 To 3
    
        txtDetalhe(i).Visible = True
    
    Next i
    
    LblDetalhe.Caption = "Solicita��o de cancelamento ao SEGUR :"
    LblDetalhe.Visible = True
    vSitSel = 1
    vOption_Escolhida = optEncerramento_Cobertura_Indevida.Caption
End Sub

Private Sub optEncerramento_Cosseguro_Inferior_Click()
    Dim i As Integer
    
    optEncerramento_Tipo_Liquidado = False
    optEncerramento_Tipo_Indeferimento = False
    optEncerramento_Administrativo = False
    optEncerramento_Pedido_Lider = False
    optEncerramento_Indeferimento = False
    'optEncerramento_Cosseguro_Inferior = False
    optEncerramento_Indeferimento_Administrativo = False
    optEncerramento_Falta_Documento = False
    optEncerramento_Desistencia_Segurado = False
    optEncerramento_Cobertura_Indevida = False
    optEncerramento_Duplicidade_Registro = False


    For i = 0 To 3
    
        txtDetalhe(i).Visible = False
    
    Next i
    
    LblDetalhe.Visible = False
    optEncerramento_Falta_Documento.Value = False
    optEncerramento_Desistencia_Segurado.Value = False
    optEncerramento_Cobertura_Indevida.Value = False
    optEncerramento_Duplicidade_Registro.Value = False
    
    vSitSel = 1
    vOption_Escolhida = optEncerramento_Cosseguro_Inferior.Caption
End Sub

Private Sub optEncerramento_Desistencia_Segurado_Click()
    Dim i As Integer
    
    optEncerramento_Tipo_Liquidado = False
    optEncerramento_Tipo_Indeferimento = False
    optEncerramento_Administrativo = False
    optEncerramento_Pedido_Lider = False
    optEncerramento_Indeferimento = False
    optEncerramento_Cosseguro_Inferior = False
    optEncerramento_Indeferimento_Administrativo = False
    optEncerramento_Falta_Documento = False
    'optEncerramento_Desistencia_Segurado = False
    optEncerramento_Cobertura_Indevida = False
    optEncerramento_Duplicidade_Registro = False


    For i = 0 To 3
    
        txtDetalhe(i).Visible = True
    
    Next i
    
    LblDetalhe.Caption = "Solicita��o de cancelamento ao SEGUR :"
    LblDetalhe.Visible = True
    vSitSel = 1
    vOption_Escolhida = optEncerramento_Desistencia_Segurado.Caption
End Sub

Private Sub optEncerramento_Duplicidade_Registro_Click()
    Dim i As Integer
    
    optEncerramento_Tipo_Liquidado = False
    optEncerramento_Tipo_Indeferimento = False
    optEncerramento_Administrativo = False
    optEncerramento_Pedido_Lider = False
    optEncerramento_Indeferimento = False
    optEncerramento_Cosseguro_Inferior = False
    optEncerramento_Indeferimento_Administrativo = False
    optEncerramento_Falta_Documento = False
    optEncerramento_Desistencia_Segurado = False
    optEncerramento_Cobertura_Indevida = False
    'optEncerramento_Duplicidade_Registro = False


    For i = 0 To 3
    
        txtDetalhe(i).Visible = True
    
    Next i
    
    LblDetalhe.Caption = "Solicita��o de cancelamento ao SEGUR :"
    LblDetalhe.Visible = True
    vSitSel = 1
    vOption_Escolhida = optEncerramento_Duplicidade_Registro.Caption
End Sub

Private Sub optEncerramento_Falta_Documento_Click()
    Dim i As Integer
    
    optEncerramento_Tipo_Liquidado = False
    optEncerramento_Tipo_Indeferimento = False
    optEncerramento_Administrativo = False
    optEncerramento_Pedido_Lider = False
    optEncerramento_Indeferimento = False
    optEncerramento_Cosseguro_Inferior = False
    optEncerramento_Indeferimento_Administrativo = False
    'optEncerramento_Falta_Documento = False
    optEncerramento_Desistencia_Segurado = False
    optEncerramento_Cobertura_Indevida = False
    optEncerramento_Duplicidade_Registro = False


    For i = 0 To 3
    
        txtDetalhe(i).Visible = True
    
    Next i
    
    LblDetalhe.Caption = "Solicita��o de cancelamento ao SEGUR :"
    LblDetalhe.Visible = True
    vSitSel = 1
    vOption_Escolhida = optEncerramento_Falta_Documento.Caption
End Sub

Private Sub optEncerramento_Indeferimento_Administrativo_Click()
    Dim i As Integer
    
    optEncerramento_Tipo_Liquidado = False
    optEncerramento_Tipo_Indeferimento = False
    optEncerramento_Administrativo = False
    optEncerramento_Pedido_Lider = False
    optEncerramento_Indeferimento = False
    optEncerramento_Cosseguro_Inferior = False
    'optEncerramento_Indeferimento_Administrativo = False
    optEncerramento_Falta_Documento = False
    optEncerramento_Desistencia_Segurado = False
    optEncerramento_Cobertura_Indevida = False
    optEncerramento_Duplicidade_Registro = False


    For i = 0 To 3
    
        txtDetalhe(i).Visible = True
    
    Next i
    
    LblDetalhe.Caption = "Comunica��o de Indeferimento ao SEGUR :"
    LblDetalhe.Visible = True
    optEncerramento_Falta_Documento.Value = False
    optEncerramento_Desistencia_Segurado.Value = False
    optEncerramento_Cobertura_Indevida.Value = False
    optEncerramento_Duplicidade_Registro.Value = False
    
    vSitSel = 1
    vOption_Escolhida = optEncerramento_Indeferimento_Administrativo.Caption

End Sub

Private Sub optEncerramento_Indeferimento_Click()
    Dim i As Integer
    
    optEncerramento_Tipo_Liquidado = False
    optEncerramento_Tipo_Indeferimento = False
    optEncerramento_Administrativo = False
    optEncerramento_Pedido_Lider = False
    'optEncerramento_Indeferimento = False
    optEncerramento_Cosseguro_Inferior = False
    optEncerramento_Indeferimento_Administrativo = False
    optEncerramento_Falta_Documento = False
    optEncerramento_Desistencia_Segurado = False
    optEncerramento_Cobertura_Indevida = False
    optEncerramento_Duplicidade_Registro = False


    For i = 0 To 3
    
        txtDetalhe(i).Visible = True
    
    Next i
    
    LblDetalhe.Caption = "Comunica��o de Indeferimento ao SEGUR :"
    LblDetalhe.Visible = True
    optEncerramento_Falta_Documento.Value = False
    optEncerramento_Desistencia_Segurado.Value = False
    optEncerramento_Cobertura_Indevida.Value = False
    optEncerramento_Duplicidade_Registro.Value = False
    
    vSitSel = 1
    vOption_Escolhida = optEncerramento_Indeferimento.Caption
    
End Sub

Private Sub optEncerramento_Pedido_Lider_Click()
    Dim i As Integer
    
    optEncerramento_Tipo_Liquidado = False
    optEncerramento_Tipo_Indeferimento = False
    optEncerramento_Administrativo = False
    'optEncerramento_Pedido_Lider = False
    optEncerramento_Indeferimento = False
    optEncerramento_Cosseguro_Inferior = False
    optEncerramento_Indeferimento_Administrativo = False
    optEncerramento_Falta_Documento = False
    optEncerramento_Desistencia_Segurado = False
    optEncerramento_Cobertura_Indevida = False
    optEncerramento_Duplicidade_Registro = False
    
   
    For i = 0 To 3
    
        txtDetalhe(i).Visible = False
    
    Next i
    
    LblDetalhe.Visible = False
    optEncerramento_Falta_Documento.Value = False
    optEncerramento_Desistencia_Segurado.Value = False
    optEncerramento_Cobertura_Indevida.Value = False
    optEncerramento_Duplicidade_Registro.Value = False
    
    vSitSel = 1
    vOption_Escolhida = optEncerramento_Pedido_Lider.Caption
End Sub

Private Sub optEncerramento_Tipo_Indeferimento_Click()
    Dim i As Integer
    
    optEncerramento_Tipo_Liquidado = False
    'optEncerramento_Tipo_Indeferimento = False
    optEncerramento_Administrativo = False
    optEncerramento_Pedido_Lider = False
    optEncerramento_Indeferimento = False
    optEncerramento_Cosseguro_Inferior = False
    optEncerramento_Indeferimento_Administrativo = False
    optEncerramento_Falta_Documento = False
    optEncerramento_Desistencia_Segurado = False
    optEncerramento_Cobertura_Indevida = False
    optEncerramento_Duplicidade_Registro = False
    
    For i = 0 To 3
    
        txtDetalhe(i).Visible = True
    
    Next i
    
    LblDetalhe.Caption = "Comunica��o de Indeferimento ao SEGUR :"
    LblDetalhe.Visible = True
    
    vSitSel = 1
    vOption_Escolhida = optEncerramento_Tipo_Indeferimento.Caption
End Sub

Private Sub optEncerramento_Tipo_Liquidado_Click()
    Dim i As Integer
    
    'optEncerramento_Tipo_Liquidado = False
    optEncerramento_Tipo_Indeferimento = False
    optEncerramento_Administrativo = False
    optEncerramento_Pedido_Lider = False
    optEncerramento_Indeferimento = False
    optEncerramento_Cosseguro_Inferior = False
    optEncerramento_Indeferimento_Administrativo = False
    optEncerramento_Falta_Documento = False
    optEncerramento_Desistencia_Segurado = False
    optEncerramento_Cobertura_Indevida = False
    optEncerramento_Duplicidade_Registro = False
    
    For i = 0 To 3
    
        txtDetalhe(i).Visible = False
    
    Next i
    

    LblDetalhe.Visible = False
    
    vSitSel = 1
    
    vOption_Escolhida = optEncerramento_Tipo_Liquidado.Caption
    
End Sub

Private Function Encerramento_Indeferido() As Boolean
Dim SQL         As String
Dim Sin_BB      As String
Dim rsBB        As ADODB.Recordset


   
    Sin_BB = Nro_BB(dSinistro_ID)
    
    SQL = ""
    SQL = SQL & " select  *"
    SQL = SQL & " from    interface_db..entrada_gtr_tb WITH (NOLOCK) "
    SQL = SQL & " Where Sinistro_BB = " & Sin_BB
    SQL = SQL & "     and evento_bb in (2003)"
    SQL = SQL & "     and not exists (select  *"
    SQL = SQL & "             from    interface_db..entrada_gtr_tb WITH (NOLOCK) "
    SQL = SQL & "             Where Sinistro_BB = " & Sin_BB
    SQL = SQL & "                 and evento_bb in (2014))"
    
    Set rsBB = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                    glAmbiente_id, _
                                     App.Title, _
                                     App.FileDescription, _
                                     SQL, _
                                     lConexaoLocal, True)
                                     
    
    If rsBB.EOF Then
        Encerramento_Indeferido = True
    Else
        Encerramento_Indeferido = False
    End If

End Function

Public Function Verifica_Protocolacao() As Boolean
Dim rs As ADODB.Recordset

Dim vsql As String
Dim OSinistro_BB As String


On Error GoTo Trata_Erro

''mathayde
''    If bytModoOperacao = "1" Then
''        Exit Function
''    End If
    
    Verifica_Protocolacao = False
    
    OSinistro_BB = Nro_BB(dSinistro_ID)
    
    If OSinistro_BB = "0" Then
         Exit Function
    End If
    
'    vsql = " SELECT COUNT(*) FROM INTERFACE_DB..DOCUMENTOS_RECEBIDOS_GTR_TB WITH (NOLOCK) "
'    vsql = vsql & " Where SINISTRO_BB = " & OSinistro_BB
'    vsql = vsql & " AND EVENTO_BB = 2003"
'    vsql = vsql & " AND DT_ENVIO IS NOT NULL"

    'FLOW 19427059 - GENJUNIOR - CONFITEC SP
    vsql = ""
    vsql = vsql & " SET NOCOUNT ON EXEC seguros_db.dbo.SEGS13158_SPS " & OSinistro_BB
    
    Set rs = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                    glAmbiente_id, _
                                     App.Title, _
                                     App.FileDescription, _
                                     vsql, _
                                     lConexaoLocal, True)
    
    If rs(0) > 0 Then
        rs.Close
       
'        vsql = ""
'        vsql = " SELECT num_recibo = CASE er.reg_cd_tip WHEN 'C' THEN SUBSTRING(er.registro,26,2)"
'        vsql = vsql & " WHEN   'A' THEN SUBSTRING(er.registro,44,2) ELSE SUBSTRING(er.registro,53,2) end "
'        vsql = vsql & " FROM INTERFACE_DB..DOCUMENTOS_RECEBIDOS_GTR_TB D WITH (NOLOCK) "
'        vsql = vsql & " INNER JOIN interface_db..entrada_gtr_tb E WITH (NOLOCK) "
'        vsql = vsql & " ON D.SINISTRO_BB = e.SINISTRO_BB"
'        vsql = vsql & " AND D.cod_remessa = E.cod_remessa"
'        vsql = vsql & " INNER JOIN interface_db..entrada_gtr_REGISTRO_tb ER WITH (NOLOCK) "
'        vsql = vsql & " ON E.ENTRADA_GTR_ID = ER.ENTRADA_GTR_ID"
'        vsql = vsql & " WHERE e.Sinistro_BB = " & OSinistro_BB
'        vsql = vsql & " AND D.EVENTO_BB = 2011"
'        vsql = vsql & " AND ER.REG_CD_TIP IN ('C', 'U','A')"
'        vsql = vsql & " AND d.dt_envio is null"
        
        'FLOW 19427059 - GENJUNIOR - CONFITEC SP
        vsql = ""
        vsql = vsql & " SET NOCOUNT ON EXEC seguros_db.dbo.SEGS13159_SPS " & OSinistro_BB
    
        Set rs = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                    glAmbiente_id, _
                                     App.Title, _
                                     App.FileDescription, _
                                     vsql, _
                                     lConexaoLocal, True)
        
        If Not rs.EOF Then
            If rs("num_recibo") <> 0 Then
                Verifica_Protocolacao = True
            Else
                Exit Function
            End If
        Else
            Verifica_Protocolacao = True
        End If
    End If
    
    rs.Close
    Exit Function
    
Trata_Erro:
    TrataErroGeral "Verifica_Protocolacao"
    Verifica_Protocolacao = False


End Function
'Private Sub ObterSinistrosCatastrofe(ByVal objSin As Integer)      'AKIO.OKUNO - 29/09/2012
Private Sub ObterSinistrosCatastrofe(ByVal objSin As Double)
Dim rsSinistroCat   As ADODB.Recordset
Dim sSQL            As String
Dim iEventoSinistro As Integer
Dim Dt_Ocorrencia   As Date

     sSQL = "SELECT dt_ocorrencia_sinistro,evento_sinistro_id FROM seguros_db.dbo.sinistro_tb WITH (NOLOCK) WHERE SINISTRO_ID = " & objSin
    
    Set rsSinistroCat = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                            glAmbiente_id, _
                                             App.Title, _
                                             App.FileDescription, _
                                             sSQL, _
                                             lConexaoLocal, True)

    If Not rsSinistroCat.EOF Then
    
        iEventoSinistro = rsSinistroCat("evento_sinistro_id")
        Dt_Ocorrencia = rsSinistroCat("dt_ocorrencia_sinistro")
        
        rsSinistroCat.Close
        
        sSQL = "EXEC SEGS7829_SPS "
        sSQL = sSQL & iEventoSinistro
        sSQL = sSQL & ",'" & Format(Dt_Ocorrencia, "yyyy-mm-dd") & "'"
        sSQL = sSQL & "," & objSin
        
        Set rsSinistroCat = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                                    glAmbiente_id, _
                                                     App.Title, _
                                                     App.FileDescription, _
                                                     sSQL, _
                                                     lConexaoLocal, True)
    
        Sinistro_Catastrofe_Contador = 0
        
        While Not rsSinistroCat.EOF
        
            Colecao_Sinistro_id = Colecao_Sinistro_id & "," & rsSinistroCat("sinistro_id")
            Sinistro_Catastrofe_Contador = Sinistro_Catastrofe_Contador + 1
            
            rsSinistroCat.MoveNext
        Wend
        rsSinistroCat.Close
        Set rsSinistroCat = Nothing
    End If
    
End Sub

Private Function ObterMensagemCatastrofe() As String
Dim sMsg    As String
Dim i       As Integer


    sMsg = "H� suspeita de Cat�strofe" & vbCrLf & vbCrLf
    
    For i = 1 To Sinistro_Catastrofe_Contador
    
        
        sMsg = sMsg & Sinistro_Catastrofe(i) & " "
        
        If (i Mod 3 = 0) Then
            sMsg = sMsg & vbCrLf
        End If
    
    Next
    
    sMsg = sMsg & vbCrLf & vbCrLf
    sMsg = sMsg & "Deseja continuar?"
    
    ObterMensagemCatastrofe = sMsg

End Function

Public Function Verifica_Encerramento_ADM() As Integer

Dim rs As ADODB.Recordset
Dim Sinbb As String, Sinistro_BB As String, SQL As String
Dim Max_Evento_9999 As Long 'evento de abertura administrativa
Dim Max_Evento_1190 As Long 'evento de reativacao

    On Error GoTo Trata_Erro
    
    Verifica_Encerramento_ADM = 2
        
    Sinistro_BB = "0"
    
    Sinbb = Nro_BB(dSinistro_ID)

    If (SinBB_Tipo <> "R") And (SinBB_Tipo <> "I") And (UCase(SinBB_Situacao) <> "INATIVO") Then ' Pega o maior sinistro_bb que tiver.
        If CDbl(Sinistro_BB) < CDbl(Sinbb) Then
            Sinistro_BB = Sinbb
        End If
    End If
  
    
    Max_Evento_9999 = 0
    Max_Evento_1190 = 0
    
    SQL = " select isnull(max(evento_id), 0) 'Max_Envento_9999' "
    SQL = SQL & " From evento_Segbr_sinistro_tb WITH (NOLOCK) "
    SQL = SQL & " Where Sinistro_BB = " & Sinistro_BB
    SQL = SQL & " and evento_segbr_id in (10130, 10134) "
    
    Set rs = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                    glAmbiente_id, _
                                     App.Title, _
                                     App.FileDescription, _
                                     SQL, _
                                     lConexaoLocal, True)
    
    If Not rs.EOF Then
        Max_Evento_9999 = rs("Max_Envento_9999")
        rs.Close
    End If
    
    SQL = " select isnull(max(evento_id), 0) 'Max_Envento_1190' "
    SQL = SQL & " From evento_Segbr_sinistro_tb WITH (NOLOCK) "
    SQL = SQL & " Where Sinistro_BB = " & Sinistro_BB
    SQL = SQL & " and evento_segbr_id = 10103 "
    
    Set rs = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                    glAmbiente_id, _
                                     App.Title, _
                                     App.FileDescription, _
                                     SQL, _
                                     lConexaoLocal, True)
    
    If Not rs.EOF Then
        Max_Evento_1190 = rs("Max_Envento_1190")
        rs.Close
    End If
    
    If Max_Evento_9999 = 0 And Max_Evento_1190 = 0 Then
        Verifica_Encerramento_ADM = 0
    Else
        If Max_Evento_9999 = 0 Then
            Verifica_Encerramento_ADM = 0
        ElseIf Max_Evento_1190 = 0 Then
            Verifica_Encerramento_ADM = 1
        Else
            If Max_Evento_9999 > Max_Evento_1190 Then
                Verifica_Encerramento_ADM = 1
            Else
                Verifica_Encerramento_ADM = 0
            End If
        End If
    End If
    
    Set rs = Nothing
    
Exit Function


Trata_Erro:
    Verifica_Encerramento_ADM = 2
    MsgBox "Erro na fun��o Verifica_Encerramento_ADM. Erro: " & Err.Description
    
End Function



Public Function Sinistro_GTR(ByVal OSinistro_BB) As Boolean
    
    Dim rs As ADODB.Recordset
    Dim vsql As String
    
    On Error GoTo Trata_Erro
    
    vsql = "SELECT COUNT(*) "
    vsql = vsql & " FROM INTERFACE_DB..entrada_GTR_tb WITH (NOLOCK) "
    vsql = vsql & " WHERE sinistro_BB = " & OSinistro_BB
    vsql = vsql & "   AND evento_BB   = 2000"
    Set rs = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                glAmbiente_id, _
                                App.Title, _
                                App.FileDescription, _
                                vsql, _
                                lConexaoLocal, True)
        Sinistro_GTR = rs(0) > 0
    rs.Close
    
    Exit Function
    
Trata_Erro:
    TrataErroGeral "Sinistro_GTR"
    Sinistro_GTR = False
    
End Function


Public Function Manutencao_Detalhes(Optional Motivo_encerramento As String) As Boolean
    
   
    Dim rs As ADODB.Recordset
    Dim vsql As String, Linha_id As Integer
    Dim linha As String
    Dim Pos_final As Integer, Pos_inicial As Integer
    Dim Itens_historico(4) As String, Evento_SEGBR_id As String
    Dim Testa_Detalhe As String

    On Error GoTo Trata_Erro
    Manutencao_Detalhes = True
    
   

    If Produto_id = 711 Or Produto_id = 712 Or Produto_id = 713 Or Produto_id = 714 Or Produto_id = 715 Then
       vDetalhamento = vDetalhamento & "Proposta Contratada Via Internet"
    End If
   
    
  
    Testa_Detalhe = Replace(vDetalhamento, Chr(13), "")
    Testa_Detalhe = Replace(Testa_Detalhe, Chr(10), "") 'Tira o LF
    If Trim(Testa_Detalhe) = "" Then ' Se n�o sobrou nada

        If Motivo_encerramento <> "" Then
            MsgBox "� obrigat�rio o preenchimento do detalhamento !", vbOKOnly + vbCritical
            Manutencao_Detalhes = False
        End If

        Exit Function ' Sai da rotina
    End If

        vsql = "set nocount on " & vbNewLine
        vsql = vsql & "EXEC sinistro_detalhamento_spi " & vbNewLine
        vsql = vsql & dSinistro_ID & vbNewLine
        vsql = vsql & "," & Apolice_id & vbNewLine
        vsql = vsql & "," & Sucursal_id & vbNewLine
        vsql = vsql & "," & Seguradora_id & vbNewLine
        vsql = vsql & "," & ramo_id & vbNewLine
        vsql = vsql & ",'" & Format(gsDHEntrada, "yyyymmdd")
        vsql = vsql & "', " & vTp_detalhamento & vbNewLine
        vsql = vsql & ", '" & vRestrito
        vsql = vsql & "','" & cUserName & "'" & vbNewLine
        
        Set rs = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                        glAmbiente_id, _
                                         App.Title, _
                                         App.FileDescription, _
                                         vsql, _
                                         lConexaoLocal, True)
         
'mathayde
'         vSql = "select detalhamento_id from sinistro_detalhamento_tb WITH (NOLOCK) where sinistro_id = " & dSinistro_ID
'
'        Set rs = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
'                               glAmbiente_id, _
'                                App.Title, _
'                                App.FileDescription, _
'                                vSql, _
'                                lConexaoLocal, True)
                                         
        Detalhamento_id = rs(0)
        
        rs.Close

        Converte_vbNewLine_para_10 vDetalhamento
        
        If Right(vDetalhamento, 1) <> Chr(10) Then
            vDetalhamento = vDetalhamento & Chr(10)
        End If
        
    
        Pos_final = InStr(Pos_inicial + 1, vDetalhamento, Chr(10))
        Pos_inicial = 0
        Linha_id = 1
        
        While Pos_final <> 0
            linha = Mid(vDetalhamento, Pos_inicial + 1, Pos_final - Pos_inicial - 1)
            Pos_inicial = Pos_final
            Pos_final = InStr(Pos_inicial + 1, vDetalhamento, Chr(10))
                
            ' GRAVA A LINHA
            vsql = "EXEC sinistro_linha_detalhe_spi " & vbNewLine
            vsql = vsql & dSinistro_ID & vbNewLine
            vsql = vsql & ", " & Apolice_id & vbNewLine
            vsql = vsql & ", " & Sucursal_id & vbNewLine
            vsql = vsql & ", " & Seguradora_id & vbNewLine
            vsql = vsql & ", " & ramo_id & vbNewLine
            vsql = vsql & ", " & Detalhamento_id & vbNewLine
            vsql = vsql & ", " & Linha_id & vbNewLine
            vsql = vsql & ", '" & MudaAspaSimples(linha) & vbNewLine
            vsql = vsql & "','" & cUserName & "'"
            
            Call Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                    glAmbiente_id, _
                                     App.Title, _
                                     App.FileDescription, _
                                     vsql, _
                                     lConexaoLocal, False)
            Linha_id = Linha_id + 1
        Wend
        

        If (vTp_detalhamento = 1) Or (vTp_detalhamento = 3) Or (vTp_detalhamento = 4) Then
            If vTp_detalhamento = 1 Then
                If vTp_exigencia = 0 Then
                 
                   Evento_SEGBR_id = "10032"
                Else
                 
                   Evento_SEGBR_id = "10127"
                End If
                
              
                If bGTR_Implantado Then
                'If GTR_Implantado Then
                    MsgBox "A Exig�ncia ser� emitido Autom�ticamente pelo GTR.", vbOKOnly + vbInformation
                Else
                    Manutencao_Detalhes = False
                    MsgBox "A Exig�ncia dever� ser emitida Manualmente no SEGUR.", vbOKOnly + vbInformation
                    Exit Function
                End If
                
                Motivo_encerramento = "NULL"
            ElseIf vTp_detalhamento = 3 Then
                'If Motivo_encerramento = "Indeferimento (Administrativo)" Then 'cristovao.rodrigues 03/04/2013 linha substituida
                If Motivo_encerramento = "9" Then
                  
                   Evento_SEGBR_id = "10131"
                Else
                  
                   Evento_SEGBR_id = "10027"
                End If
            Else
                Evento_SEGBR_id = "10102"
            End If
            
            If bGTR_Implantado Or Evento_SEGBR_id = "10027" Or Evento_SEGBR_id = "10131" Then
            'If GTR_Implantado Then
        
                If Not Inclui_Evento(Evento_SEGBR_id, Sinistro_Situacao, Motivo_encerramento, Itens_historico, "", Detalhamento_id) Then
                    Manutencao_Detalhes = False
                    MsgBox "Erro na Inclus�o do Evento de Exig�ncia", vbOKOnly + vbCritical
                    Exit Function
                End If
            End If
        End If

    
    vDetalhamento = ""

    Exit Function
    
Trata_Erro:
    TrataErroGeral "Manuten��o de Detalhes"
    Manutencao_Detalhes = False
Resume
End Function


Public Sub Converte_vbNewLine_para_10(ByRef Texto As String)

    Dim pos As Integer

    While InStr(Texto, vbNewLine) > 0
        pos = InStr(Texto, vbNewLine)
        
        Texto = Left(Texto, pos - 1) & Mid(Texto, pos + 1)
    Wend

End Sub

Public Function Atualiza_Visualizacao_Eventos(aSinist As String) As Boolean
        
        Dim SQL As String
        
        On Error GoTo TrataErro
        
        '* Verifica se existem novos eventos para esse sinistro
        If Existem_Novos_Eventos(aSinist) Then
           '* Executa SP que altera o status de evento_visualizado para 's'
           SQL = "EXEC altera_evento_visualizado_spu "
           SQL = SQL & aSinist & ", "
           SQL = SQL & Apolice_id & ", "
           SQL = SQL & Seguradora_id & ", "
           SQL = SQL & Sucursal_id & ", "
           SQL = SQL & ramo_id

            Call Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                    glAmbiente_id, _
                                     App.Title, _
                                     App.FileDescription, _
                                     SQL, _
                                     lConexaoLocal, False)
        
        End If
        
        Atualiza_Visualizacao_Eventos = True
        
        Exit Function
        
TrataErro:
    MsgBox "Ocorreu o erro n�. " & Err.Number & vbCrLf & _
           "Descri��o: " & Err.Description
    Atualiza_Visualizacao_Eventos = False
    Exit Function
        
End Function

Public Function Existem_Novos_Eventos(eSinist As String) As Boolean

        On Error GoTo TrataErro
        
        Existem_Novos_Eventos = False
        
        Dim SQL As String
'AKIO.OKUNO - 29/09/2012
'        Dim rs  As rdoResultset
        Dim rsRecordSet     As ADODB.Recordset
        
        SQL = "SELECT  COUNT(e.evento_BB_id)"
        SQL = SQL & " FROM    sinistro_historico_tb s WITH (NOLOCK) "
        SQL = SQL & "   INNER JOIN evento_SEGBR_tb e WITH (NOLOCK) "
        SQL = SQL & "       ON (e.evento_SEGBR_id = s.evento_SEGBR_id)"
        SQL = SQL & " WHERE s.Sinistro_id               = " & eSinist
        SQL = SQL & " AND     s.evento_visualizado      = 'n'"
'AKIO.OKUNO - 29/09/2012
'        Set rs = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                    glAmbiente_id, _
                                     App.Title, _
                                     App.FileDescription, _
                                     SQL, _
                                     lConexaoLocal, True)
                                     
        Set rsRecordSet = Conexao_ExecutarSQL(gsSIGLASISTEMA _
                                            , glAmbiente_id _
                                            , App.Title _
                                            , App.FileDescription _
                                            , SQL _
                                            , lConexaoLocal _
                                            , True)  'AKIO.OKUNO - 29/09/2012
        
'AKIO.OKUNO - 29/09/2012
'        If rs(0) > 0 Then
        If rsRecordSet(0) > 0 Then
           Existem_Novos_Eventos = True
        End If
'AKIO.OKUNO - 29/09/2012
'        rs.Close
        rsRecordSet.Close
        
        Exit Function
        
TrataErro:
        MsgBox "Ocorreu o erro n�. " & Err.Number & vbCrLf & "Descri��o: " & Err.Description
        Exit Function
        Resume
End Function

Private Sub enviaEmail(ByVal email As String, ByVal Assunto As String, ByVal Mensagem As String)

On Error GoTo TrataErroEmail
    Dim sSQLEmail As String
    Dim oRsEmail As ADODB.Recordset

    'Verifica se existem gestores para a diretoria
    sSQLEmail = ""
    adSQL sSQLEmail, "set nocount on EXEC email_db..sgss0782_spi "
    '----------------------INC000004306122 - HENRIQUE H. HENRIQUES (CONFITEC 30/04/2014) INICIO
    '           FOI CORRIGIDO A SIGLA DO RECURSO POIS A SIGLA SEGP1294 N�O ESTA CADASTRADA NA CHAVE_TB
    'adSQL sSQLEmail, " @sigla_recurso = '" & App.Title & "', "
    adSQL sSQLEmail, " @sigla_recurso = 'SEGS976X', "
    '----------------------INC000004306122 - HENRIQUE H. HENRIQUES (CONFITEC 30/04/2014) FIM
    ' Verificar se est� correto
    adSQL sSQLEmail, " @sigla_sistema = 'SISBR', "
    ' Verificar se est� correto
    adSQL sSQLEmail, " @chave = 'SEGP0246', "
    ' De
    adSQL sSQLEmail, " @de = 'alianca@aliancadobrasil.com.br', "
    ' Para
    adSQL sSQLEmail, " @para = '" & email & "', "
    ' Assunto
    adSQL sSQLEmail, " @assunto = 'Relat�rios com posi��o das demandas',"
    ' Corpo
    adSQL sSQLEmail, " @mensagem = '" & Mensagem & "', "

    adSQL sSQLEmail, " @cc = '', "
    adSQL sSQLEmail, " @anexo = '', "
    adSQL sSQLEmail, " @formato = '1', "
    adSQL sSQLEmail, " @usuario = '" & cUserName & "'"

    '------------------------------------------------------------------------
    'Ricardo Toledo (Confitec) : 06/12/2016 : INC000005243180 : INI
    'Inclus�o do parametro vNunCx pois estava ocorrendo erro de Type Mismatch
    'Set oRsEmail = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                        glAmbiente_id, _
                                        App.Title, _
                                        App.FileDescription, _
                                        sSQLEmail, _
                                        True)
    Set oRsEmail = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                        glAmbiente_id, _
                                        App.Title, _
                                        App.FileDescription, _
                                        sSQLEmail, _
                                        vNunCx, _
                                        True)
    'Ricardo Toledo (Confitec) : 06/12/2016 : INC000005243180 : FIM
    '------------------------------------------------------------------------


    Set oRsEmail = Nothing
     
    Exit Sub
TrataErroEmail:
     Call TratarErro("Envio do e-mail")
End Sub

Public Function Monta_ReguladorEmail(Destinatario As String) As Boolean

    Dim rs As ADODB.Recordset
    Dim RS2 As ADODB.Recordset
    Dim texto_detalhe As String
    Dim SQL As String, tp_ramo As Integer
    Dim vsql As String
    Dim Assunto As String
    Dim Mensagem As String
        
    Monta_ReguladorEmail = False
    
                
        SQL = "SELECT tp_ramo_id FROM ramo_tb WITH (NOLOCK) where ramo_id = " & ramo_id
        Set rs = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                    glAmbiente_id, _
                                     App.Title, _
                                     App.FileDescription, _
                                     SQL, _
                                     lConexaoLocal, True)
        If Not rs.EOF Then
            tp_ramo = rs(0)
        End If
        rs.Close
    
        If tp_ramo = 2 Then
            
           texto_detalhe = ""
            
            vsql = "select distinct descricao from evento_segbr_sinistro_tb evs WITH (NOLOCK) "
            vsql = vsql & " join evento_segbr_sinistro_documento_tb esd WITH (NOLOCK) "
            vsql = vsql & " on evs.evento_id = esd.evento_id "
            vsql = vsql & " and evs.sinistro_id = " & dSinistro_ID
            vsql = vsql & " and evento_bb_id = 1100 "
            vsql = vsql & " order by descricao"
            
            Set RS2 = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                    glAmbiente_id, _
                                     App.Title, _
                                     App.FileDescription, _
                                     vsql, _
                                     lConexaoLocal, True)
    
            If Not RS2.EOF Then
                texto_detalhe = "Documentos Solicitados:" & vbNewLine
                texto_detalhe = texto_detalhe & String(70, "-") & vbNewLine & vbNewLine
            End If
    
            While Not RS2.EOF
                texto_detalhe = texto_detalhe & RS2("descricao") & vbNewLine & vbNewLine
                RS2.MoveNext
            Wend
            RS2.Close
            
             Mensagem = texto_detalhe

            If Mensagem <> "" Then
                Assunto = "Detalhamento do sinistro n� " & dSinistro_ID
                 
                SQL = "EXEC seguros_db..envia_email_sp "
                SQL = SQL & "'" & Destinatario & "',"
                SQL = SQL & "'" & Assunto & "',"
                SQL = SQL & "'" & Replace(Mensagem, "'", "�") & "'"
                      
                Set rs = rdocn.OpenResultset(SQL)
                rs.Close
                
                Monta_ReguladorEmail = True
            Else
                MsgBox "N�o foi poss�vel enviar e-mail para o regulador. E-mail sem mensagem.", vbInformation, "Monta_ReguladorEmail"
                Monta_ReguladorEmail = False
                Exit Function
            End If
    
        End If
  
    
End Function

Public Function Mensagem_VistPrest(Operacao As String, NomeVistoriadorPrestador As String) As String

    Dim Msg As String
    Dim SQL As String, rs As ADODB.Recordset
    Dim corretor As String, Estimativa As String
    Dim tel As String, end_cliente As String
    Dim cidade_cliente As String, estado_cliente As String
    Dim est_temp As Double, moeda As String, Dt_temp As String
    Dim vProposta_BB As String
    Dim segurado As String
    Dim Ini_Vig_Apolice As Date
    Dim Fim_Vig_Apolice As Date
    Dim vsql As String
    Dim Causa As String
    Dim Nome_Produto As String
    
    If bytTipoRamo = 1 Then
        SQL = "SELECT SEGURADO FROM SINISTRO_VIDA_TB WITH (NOLOCK) WHERE SINISTRO_ID = " & dSinistro_ID
        
        Set rs = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                    glAmbiente_id, _
                                     App.Title, _
                                     App.FileDescription, _
                                     SQL, _
                                     lConexaoLocal, True)
        If Not rs.EOF Then
            segurado = rs("NOME")
        End If
        rs.Close
    End If
    
    vsql = " SELECT  pa.dt_inicio_vigencia, " & vbNewLine
    vsql = vsql & " a.dt_fim_vigencia_end " & vbNewLine
    vsql = vsql & " FROM    endosso_tb a WITH (NOLOCK) " & vbNewLine
    vsql = vsql & " inner join proposta_adesao_tb pa WITH (NOLOCK) on pa.proposta_id = a.proposta_id " & vbNewLine
    vsql = vsql & " Where a.Proposta_id = " & Proposta_id & vbNewLine
    vsql = vsql & " AND     a.tp_endosso_id = 250 " & vbNewLine
    vsql = vsql & " AND     a.endosso_id = (SELECT  MAX(b.endosso_id) " & vbNewLine
    vsql = vsql & "              FROM    endosso_tb b WITH (NOLOCK) " & vbNewLine
    vsql = vsql & "              Where b.Proposta_id = a.Proposta_id " & vbNewLine
    vsql = vsql & "              AND     b.tp_endosso_id = 250 )" & vbNewLine
    
    Set rs = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                glAmbiente_id, _
                                 App.Title, _
                                 App.FileDescription, _
                                 SQL, _
                                 lConexaoLocal, True)
    
    If Not rs.EOF Then
        Ini_Vig_Apolice = rs("dt_inicio_vigencia")
        Fim_Vig_Apolice = rs("dt_fim_vigencia_end")
    End If
    rs.Close
    
    
    vsql = " select b.nome as causa,d.nome as nome_produto from sinistro_tb a WITH (NOLOCK)"
    vsql = vsql & " join evento_sinistro_tb b WITH (NOLOCK)" & vbNewLine
    vsql = vsql & " on a.evento_sinistro_id = b.evento_sinistro_id " & vbNewLine
    vsql = vsql & " join proposta_tb c WITH (NOLOCK) " & vbNewLine
    vsql = vsql & " on a.proposta_id = c.proposta_id " & vbNewLine
    vsql = vsql & " join produto_tb d" & vbNewLine
    vsql = vsql & " on d.produto_id = c.produto_id " & vbNewLine
    vsql = vsql & " Where a.sinistro_id =  " & dSinistro_ID & vbNewLine
 
    
    Set rs = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                glAmbiente_id, _
                                 App.Title, _
                                 App.FileDescription, _
                                 SQL, _
                                 lConexaoLocal, True)
    
    If Not rs.EOF Then
        Causa = rs("causa")
        Nome_Produto = rs("nome_produto")
    End If
    rs.Close
    
     
    SQL = "SELECT b.telefone_1, "
    SQL = SQL & " isnull(a.endereco,'') as endereco, "
    SQL = SQL & " isnull(a.estado,'') as estado, "
    SQL = SQL & " isnull(a.municipio,'') as municipio "
    SQL = SQL & " FROM proposta_tb p WITH (NOLOCK) "
    SQL = SQL & " inner join cliente_tb b WITH (NOLOCK) "
    SQL = SQL & "   on p.prop_cliente_id = b.cliente_id    "
    SQL = SQL & " left join endereco_corresp_tb a WITH (NOLOCK) "
    SQL = SQL & "   on p.proposta_id    = a.proposta_id   "
    SQL = SQL & " WHERE "
    SQL = SQL & " And p.Proposta_id = " & Proposta_id
    SQL = SQL & " And b.cliente_id  = " & Cliente_id
    
    Set rs = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                    glAmbiente_id, _
                                     App.Title, _
                                     App.FileDescription, _
                                     SQL, _
                                     lConexaoLocal, True)
    
    If Not rs.EOF Then 'eSantos Flow 125300 18/05/2006
        tel = "" & rs("telefone_1")
        If Trim(rs("endereco")) <> "" Then
           end_cliente = "" & rs("endereco")
           estado_cliente = "" & rs("estado")
           cidade_cliente = "" & rs("municipio")
        Else
           MsgBox "Endere�o do Cliente n�o cadastrado", vbCritical
        End If
    End If              'eSantos Flow 125300 18/05/2006
    
    rs.Close
    
    SQL = "select nome "
    SQL = SQL & "FROM corretagem_tb a WITH (NOLOCK) INNER JOIN corretor_tb b WITH (NOLOCK) ON "
    SQL = SQL & " a.corretor_id = b.corretor_id "
    SQL = SQL & "WHERE Proposta_id = " & Proposta_id
    SQL = SQL & "  AND dt_fim_corretagem is null"
    
    Set rs = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                    glAmbiente_id, _
                                     App.Title, _
                                     App.FileDescription, _
                                     SQL, _
                                     lConexaoLocal, True)
    If Not rs.EOF Then
        corretor = "" & rs("nome")
    Else
        corretor = "N�O INDICADO"
    End If
    rs.Close
    
    SQL = "SELECT a.val_estimado, a.dt_inicio_estimativa, s.moeda_id "
    SQL = SQL & "FROM sinistro_tb s WITH (NOLOCK), sinistro_estimativa_tb a WITH (NOLOCK) "
    SQL = SQL & "WHERE a.sinistro_id = " & dSinistro_ID
    SQL = SQL & "  AND a.sinistro_id = s.Sinistro_id "
    SQL = SQL & "  AND a.item_val_estimativa = 1 "
    SQL = SQL & "  AND a.seq_estimativa = "
    SQL = SQL & "   (SELECT MAX(b.seq_estimativa) "
    SQL = SQL & "    FROM sinistro_estimativa_tb b WITH (NOLOCK) "
    SQL = SQL & "    WHERE b.sinistro_id = " & dSinistro_ID & ") "
    
    Set rs = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                    glAmbiente_id, _
                                     App.Title, _
                                     App.FileDescription, _
                                     SQL, _
                                     lConexaoLocal, True)
    If Not rs.EOF Then
        est_temp = Val(rs("val_estimado"))
        moeda = rs("moeda_id")
        Dt_temp = Format(rs("dt_inicio_estimativa"), "yyyymmdd")
    
        rs.Close
        
        SQL = "SELECT val_paridade_moeda "
        SQL = SQL & " FROM paridade_tb WITH (NOLOCK) "
        SQL = SQL & " WHERE dt_conversao = '" & Dt_temp & "'"
        SQL = SQL & " AND origem_moeda_id = " & moeda
        SQL = SQL & " AND destino_moeda_id = 790 "
        
        Set rs = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                    glAmbiente_id, _
                                     App.Title, _
                                     App.FileDescription, _
                                     SQL, _
                                     lConexaoLocal, True)
        If rs.EOF Then
            Estimativa = Format(est_temp, "###,###,###,##0.00")
        Else
            Estimativa = Format(Val(rs("val_paridade_moeda")) * est_temp, "###,###,###,##0.00")
        End If
    Else
        Estimativa = "0,00"
    End If
    rs.Close
    
    'Obt�m a proposta BB (a pedido do Usu�rio)
    SQL = "SELECT  proposta_bb"
    SQL = SQL & " FROM  proposta_fechada_tb WITH (NOLOCK) "
    SQL = SQL & " WHERE Proposta_id = " & Proposta_id
    SQL = SQL & " UNION "
    SQL = SQL & " SELECT  proposta_bb "
    SQL = SQL & " FROM  proposta_adesao_tb WITH (NOLOCK) "
    SQL = SQL & " WHERE Proposta_id = " & Proposta_id
    Set rs = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                    glAmbiente_id, _
                                     App.Title, _
                                     App.FileDescription, _
                                     SQL, _
                                     lConexaoLocal, True)
    If Not rs.EOF Then
       vProposta_BB = IIf(IsNull(rs!proposta_bb), "", rs!proposta_bb)
    End If
    rs.Close
    
    If Operacao = "IR" Then ' In�cio de vistoria
        Msg = NomeVistoriadorPrestador & vbNewLine & vbNewLine
        Msg = Msg & "Solicitamos suas provid�ncias para iniciar a regula��o e/ou sindic�ncia " & vbNewLine
        Msg = Msg & "do sinistro N� " & dSinistro_ID & " referente ao segurado : " & vbNewLine
        Msg = Msg & vbNewLine
    ElseIf Operacao = "IP" Then ' Cancelamento de presta��o de servi�os
        Msg = NomeVistoriadorPrestador & vbNewLine & vbNewLine
        Msg = Msg & "Solicitamos suas provid�ncias para in�cio de servi�os " & vbNewLine
        Msg = Msg & "no sinistro N� " & dSinistro_ID & " referente ao segurado : " & vbNewLine
        Msg = Msg & vbNewLine
    ElseIf Operacao = "FR" Then ' Cancelamento de vistoria
        Msg = NomeVistoriadorPrestador & vbNewLine & vbNewLine
        Msg = Msg & "Informamos que a partir da data de hoje o sinistro listado " & vbNewLine
        Msg = Msg & "abaixo estar� sendo regulado por esta Companhia. " & vbNewLine
        Msg = Msg & vbNewLine
        Msg = Msg & "Sinistro N�: " & dSinistro_ID & " referente ao segurado :" & vbNewLine
        Msg = Msg & vbNewLine
    ElseIf Operacao = "FP" Then ' Cancelamento de presta��o de servi�os
        Msg = NomeVistoriadorPrestador & vbNewLine & vbNewLine
        Msg = Msg & "Informamos que a partir da data de hoje o sinistro listado " & vbNewLine
        Msg = Msg & "abaixo estar� sendo avaliado por esta Companhia. " & vbNewLine
        Msg = Msg & vbNewLine
        Msg = Msg & "Sinistro N�: " & dSinistro_ID & " referente ao segurado :" & vbNewLine
        Msg = Msg & vbNewLine
    End If
    
    If bytTipoRamo = 1 Then
        Msg = Msg & "Nome: " & UCase(segurado) & vbNewLine
    Else
        Msg = Msg & "Nome: " & UCase(Cliente_Nome) & vbNewLine
    End If
    
    Msg = Msg & "Telefone: " & tel & vbNewLine
    Msg = Msg & "Endere�o: " & end_cliente & vbNewLine
    Msg = Msg & "Munic�pio: " & cidade_cliente & " - UF: " & estado_cliente & vbNewLine
    Msg = Msg & vbNewLine
    Msg = Msg & "Dados da ap�lice: " & vbNewLine
    Msg = Msg & "Ap�lice N�: " & Apolice_id & " - Proposta N�: " & Proposta_id & vbNewLine
    Msg = Msg & "Proposta BB: " & vProposta_BB & vbNewLine
    
    Dt_temp = Ini_Vig_Apolice
    SQL = "SELECT dt_inicio_vigencia "
    SQL = SQL & " FROM proposta_adesao_tb WITH (NOLOCK) "
    SQL = SQL & " WHERE proposta_id = " & Proposta_id
    
    Set rs = rdocn.OpenResultset(SQL)
    If Not rs.EOF Then
        If Not IsNull(rs(0)) Then
            Dt_temp = Format(rs(0), "dd/mm/yyyy")
        End If
    End If
    rs.Close
    
    Msg = Msg & "In�cio de Vig�ncia: " & Ini_Vig_Apolice & "   Fim de Vig�ncia: " & Fim_Vig_Apolice & vbNewLine
    Msg = Msg & "Produto: " & Nome_Produto & vbNewLine
    Msg = Msg & "Corretor: " & corretor & vbNewLine
    Msg = Msg & vbNewLine
    Msg = Msg & "Dados do sinistro: " & vbNewLine
    Msg = Msg & "Data de ocorr�ncia: " & Dt_ocorrencia_sinistro & vbNewLine
    Msg = Msg & "Causa : " & Causa & vbNewLine
    Msg = Msg & "Estimativa: R$ " & Estimativa & vbNewLine
    If Right(Operacao, 1) = "R" Then
        Msg = Msg & "Local da Vistoria: " & Sinistro_Endereco & vbNewLine
    Else
        Msg = Msg & "Local do sinistro: " & Sinistro_Endereco & vbNewLine
    End If
    Msg = Msg & "Munic�pio: " & Sinistro_Municipio & " - UF: " & Sinistro_UF & vbNewLine
    Msg = Msg & vbNewLine
    Msg = Msg & vbNewLine
    Msg = Msg & "S�o Paulo, " & Format(gsDHEntrada, "dd/mm/yyyy") & vbNewLine
    Msg = Msg & "Alian�a do Brasil"
    
    Mensagem_VistPrest = Msg

End Function


Public Function SendMsg(vTipo As String, Mensagem As String, _
                        Destinatario As String, Form_origem As Form, _
                        Optional num_sinistro As String) As Boolean
                        'carolina.marinho 10/10/2011 - Demanda 5446988 - Lista de Aprova��o de Pagamento de sinistro - Indeniza��o/Honor�rio/Despesa
    On Error GoTo Erro
    
    SendMsg = False
    Form_origem.MAPIMess.Compose
    If vTipo = "IR" Then ' In�cio de regula��o
        Form_origem.MAPIMess.MsgSubject = "Solicita��o de vistoria"
        Form_origem.MAPIMess.MsgNoteText = Mensagem
    ElseIf vTipo = "IP" Then ' In�cio de presta��o
        Form_origem.MAPIMess.MsgSubject = "Solicita��o de servi�o"
        Form_origem.MAPIMess.MsgNoteText = Mensagem
    ElseIf vTipo = "FR" Then  ' Fim de regula��o
        Form_origem.MAPIMess.MsgSubject = "Cancelamento de vistoria"
        Form_origem.MAPIMess.MsgNoteText = Mensagem
    ElseIf vTipo = "FP" Then  ' Fim de presta��o
        Form_origem.MAPIMess.MsgSubject = "Cancelamento de servi�o"
        Form_origem.MAPIMess.MsgNoteText = Mensagem
    ElseIf vTipo = "AP" Then 'Aprova��o de pagamento
        Form_origem.MAPIMess.MsgSubject = "Aprova��o do processo de sinistro " & num_sinistro
        Form_origem.MAPIMess.MsgNoteText = Mensagem
    End If
    Form_origem.MAPIMess.MsgReceiptRequested = ReturnRequest
    Call KillRecips(Form_origem.MAPIMess)
    Call SetRCList(Destinatario, Form_origem.MAPIMess, vbRecipTypeTo, False)
                  
    On Error Resume Next
    Form_origem.MAPIMess.Action = vbMessageSend
    If Err Then
        MsgBox "Erro mandando mensagem: " & vbNewLine + Str$(Err) + Error
        If MsgBox("N�o foi poss�vel enviar a mesagem, pois " & _
               "seu programa de e-mail n�o est� aberto. Continua ?", vbYesNo, "Aten��o") = vbYes Then
            SendMsg = True
        End If
        Exit Function
    End If
    SendMsg = True

    Exit Function

Erro:
    If Err.Number = 32053 Then
        If MsgBox("N�o foi poss�vel enviar a mesagem, pois " & _
               "seu programa de e-mail n�o est� aberto. Continua ?", vbYesNo, "Aten��o") = vbYes Then
            SendMsg = True
        End If
    End If

End Function

Sub KillRecips(MsgControl As Control)
    ' Delete each recipient.  Loop until no recipients exist.
    While MsgControl.RecipCount
        MsgControl.Action = vbRecipientDelete
    Wend
End Sub

Sub SetRCList(ByVal NameList As String, Msg As Control, RCType As Integer, fResolveNames As Integer)

    Dim i As Integer
    
    If NameList = "" Then
        Exit Sub
    End If

    i = Msg.RecipCount
    Do
        Msg.RecipIndex = i
        Msg.RecipDisplayName = Trim$(Token(NameList, "; regulador"))
        If fResolveNames Then
            Msg.Action = vbMessageResolveName
        End If
        Msg.RecipType = RCType
        i = i + 1
    Loop Until (NameList = "")

End Sub

Function Token$(tmp$, search$)
    
    Dim X As Integer
    
    X = InStr(1, tmp$, search$)
    If X Then
       Token$ = Mid$(tmp$, 1, X - 1)
       tmp$ = Mid$(tmp$, X + 1)
    Else
       Token$ = tmp$
       tmp$ = ""
    End If

End Function

Public Function Obtem_Localizacao_Processo( _
        ByVal OSinistro_id As String) As Integer
    
  
    Dim RS_AUX          As ADODB.Recordset
    Dim vsql            As String
    Dim OSinistro_BB    As String
    
    On Error GoTo Trata_Erro
    
    vsql = "SELECT COUNT(*)"
    vsql = vsql & " FROM sinistro_BB_tb WITH (NOLOCK) "
    vsql = vsql & " WHERE sinistro_id            = " & OSinistro_id
    vsql = vsql & "   AND dt_fim_vigencia        IS NULL"
    
    Set RS_AUX = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                        glAmbiente_id, _
                                        App.Title, _
                                        App.FileDescription, _
                                        vsql, _
                                        vNunCx, _
                                        True, _
                                        False)
    
    If RS_AUX(0) = 0 Then
       RS_AUX.Close
    ElseIf RS_AUX(0) > 1 Then
       Obtem_Localizacao_Processo = 4
       RS_AUX.Close
    Else
       RS_AUX.Close
       
    
    OSinistro_BB = Nro_BB(OSinistro_id)
       
       If OSinistro_BB <> "0" Then
          
            vsql = " select  prox_localizacao_processo, evento_segbr_tb.evento_bb_id, sinistro_bb_tb.sinistro_bb                        " & vbNewLine
            vsql = vsql & " from    sinistro_tb WITH (NOLOCK)                                                                                " & vbNewLine
            vsql = vsql & " join    sinistro_bb_tb WITH (NOLOCK)                                                                             " & vbNewLine
            vsql = vsql & " on      sinistro_bb_tb.sinistro_id = sinistro_tb.sinistro_id                                                " & vbNewLine
            vsql = vsql & " and     sinistro_bb_tb.dt_fim_vigencia is null                                                              " & vbNewLine
            vsql = vsql & " join    evento_segbr_sinistro_tb WITH (NOLOCK)                                                                   " & vbNewLine
            vsql = vsql & " on      evento_segbr_sinistro_tb.sinistro_id = sinistro_bb_tb.sinistro_id                                   " & vbNewLine
            vsql = vsql & " and     evento_segbr_sinistro_tb.sinistro_bb = sinistro_bb_tb.sinistro_bb                                   " & vbNewLine
            vsql = vsql & " and     evento_segbr_sinistro_tb.evento_id in                                                               " & vbNewLine
            vsql = vsql & "                     (                                                                                       " & vbNewLine
            vsql = vsql & "                     select  max(evento_segbr_sinistro_tb_2.evento_id)                                       " & vbNewLine
            vsql = vsql & "                     from    evento_segbr_sinistro_tb evento_segbr_sinistro_tb_2 WITH (NOLOCK)                     " & vbNewLine
            vsql = vsql & "                     join    evento_segbr_tb evento_segbr_tb_2 WITH (NOLOCK)                                      " & vbNewLine
            vsql = vsql & "                     on      evento_segbr_sinistro_tb_2.evento_segbr_id = evento_segbr_tb_2.evento_segbr_id  " & vbNewLine
            vsql = vsql & "                     where   evento_segbr_sinistro_tb_2.sinistro_id = sinistro_tb.sinistro_id                " & vbNewLine
            vsql = vsql & "                     and     evento_segbr_sinistro_tb_2.sinistro_bb = sinistro_bb_tb.sinistro_bb             " & vbNewLine
            vsql = vsql & "                     and     evento_segbr_sinistro_tb_2.evento_bb_id is not null                             " & vbNewLine
            vsql = vsql & "                     and     (evento_segbr_sinistro_tb_2.num_recibo is null                                  " & vbNewLine
            vsql = vsql & "                             or evento_segbr_sinistro_tb_2.num_recibo = 0                                    " & vbNewLine
            vsql = vsql & "                             or evento_segbr_sinistro_tb_2.evento_bb_id = 2009)                              " & vbNewLine
            vsql = vsql & "                     and     evento_segbr_sinistro_tb_2.evento_bb_id not in(1150,1151,1152,1153,1154,1181)   " & vbNewLine
            vsql = vsql & "                     and     evento_segbr_tb_2.prox_localizacao_processo is not null                         " & vbNewLine
            vsql = vsql & "                     )                                                                                       " & vbNewLine
            vsql = vsql & " join    evento_segbr_tb WITH (NOLOCK)                                                                            " & vbNewLine
            vsql = vsql & " on      evento_segbr_tb.evento_segbr_id = evento_segbr_sinistro_tb.evento_segbr_id                          " & vbNewLine
            vsql = vsql & " where   sinistro_tb.sinistro_id = " & OSinistro_id

             
          
          Set RS_AUX = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                            glAmbiente_id, _
                                            App.Title, _
                                            App.FileDescription, _
                                            vsql, _
                                            vNunCx, _
                                            True, _
                                            False)
          
          If Not RS_AUX.EOF Then
             Obtem_Localizacao_Processo = RS_AUX("prox_localizacao_processo")
          End If
       End If
       RS_AUX.Close
    End If
    
    Exit Function
    
Trata_Erro:
    TrataErroGeral "OBTEM_LOCALIZACAO_PROCESSO"
    
End Function

'cristovao.rodrigues 09/10/2012  --- retorno_entrada_GTR_tb
Public Sub Obtem_retorno_entrada_GTR_tb()
        
Dim SQL                     As String
Dim rs                      As ADODB.Recordset
Dim Sinistro_BB             As String

On Error GoTo TrataErro

retorno_entrada_GTR_tb = True

SQL = "SELECT sinistro_BB "
SQL = SQL & " FROM sinistro_BB_tb WITH (NOLOCK) "
SQL = SQL & " WHERE sinistro_id = " & dSinistro_ID
SQL = SQL & "   AND dt_fim_vigencia IS NULL"

Set rs = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                            glAmbiente_id, _
                             App.Title, _
                             App.FileDescription, _
                             SQL, _
                             lConexaoLocal, True)

If Not rs.EOF Then
    Sinistro_BB = rs(0)
End If

rs.Close
        
If Sinistro_BB <> "" Then
    SQL = "SELECT COUNT(*) "
    SQL = SQL & " FROM INTERFACE_DB..entrada_GTR_tb WITH (NOLOCK) "
    SQL = SQL & " WHERE sinistro_BB = " & Sinistro_BB
    SQL = SQL & "   AND evento_BB   = 2000"
    
    Set rs = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                    glAmbiente_id, _
                     App.Title, _
                     App.FileDescription, _
                     SQL, _
                     lConexaoLocal, True)
        
    If rs(0) > 0 Then
        'Obtem_GTR_Implantado = True
    Else
        retorno_entrada_GTR_tb = False
    End If
    
    rs.Close
End If

Exit Sub
        
TrataErro:
    Call TrataErroGeral("Erro na Rotina: Obtem_retorno_entrada_GTR_tb. Descri��o do Erro: " & Err.Description)
            
End Sub


'cristovao.rodrigues 23/10/2012
Private Function VerificaMotivoEncerramentoCobertura() As Boolean

Dim SQLAux As String
Dim rsaux As ADODB.Recordset

VerificaMotivoEncerramentoCobertura = True

'SQLAux = "SELECT DISTINCT * " & vbNewLine
'SQLAux = SQLAux & "From " & vbNewLine
'SQLAux = SQLAux & "sinistro_cobertura_tb WITH (NOLOCK) LEFT OUTER JOIN " & vbNewLine
'SQLAux = SQLAux & "sinistro_motivo_encerramento_tb WITH (NOLOCK) ON " & vbNewLine
'SQLAux = SQLAux & "sinistro_cobertura_tb.sinistro_motivo_encerramento_id = sinistro_motivo_encerramento_tb.sinistro_motivo_encerramento_id LEFT OUTER JOIN " & vbNewLine
'SQLAux = SQLAux & "tp_cobertura_tb WITH (NOLOCK) ON sinistro_cobertura_tb.tp_cobertura_id = tp_cobertura_tb.tp_cobertura_id " & vbNewLine
'SQLAux = SQLAux & "Where " & vbNewLine
'SQLAux = SQLAux & "(sinistro_motivo_encerramento_tb.sinistro_motivo_encerramento_id IS NULL) AND" & vbNewLine
'SQLAux = SQLAux & " (sinistro_cobertura_tb.tp_cobertura_id IS NOT NULL) " & vbNewLine
'SQLAux = SQLAux & " AND (sinistro_cobertura_tb.sinistro_id = " & dSinistro_ID & ") " & vbNewLine
'SQLAux = SQLAux & " AND (sinistro_cobertura_tb.dt_fim_vigencia is null or " & vbNewLine
'SQLAux = SQLAux & " (CONVERT(VARCHAR,sinistro_cobertura_tb.dt_fim_vigencia,112) > " & vbNewLine
'SQLAux = SQLAux & " CONVERT(VARCHAR,getdate(),112)))" & vbNewLine

'FLOW 19427059 - GENJUNIOR - CONFITEC SP
SQLAux = ""
SQLAux = SQLAux & " SET NOCOUNT ON EXEC seguros_db.dbo.SEGS13151_SPS " & dSinistro_ID

Set rsaux = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                glAmbiente_id, _
                                 App.Title, _
                                 App.FileDescription, _
                                 SQLAux, _
                                 lConexaoLocal, True)
                                 
If Not rsaux.EOF Then
    VerificaMotivoEncerramentoCobertura = False
End If

rsaux.Close

End Function

Private Function Campos_Validos_Subvencao()
' Marcio.Nogueira - confitec/rj - Demanda MU-2018-029707 - Melhorias Web Service Mapa - Atualiza��o Campos Sinistro - [Subven��o Federal] - 04/072018
' Validando para somente Produtos 1152 e 1204
   Dim SQL As String
   Dim rsSet As ADODB.Recordset
   Dim i As Integer
   i = 0
   Campos_Validos_Subvencao = True
   
  SQL = ""
    
  SQL = SQL & "  select " & vbCrLf
  SQL = SQL & "  isnull(AREA_SINISTRADA, 0) As AREA_SINISTRADA" & vbCrLf
  SQL = SQL & "  , isnull(PCT_PERDA,0) as PCT_PERDA" & vbCrLf
  SQL = SQL & "  , isnull (NRO_PROD_OBTIDA,0) as NRO_PROD_OBTIDA" & vbCrLf
  SQL = SQL & "  , isnull(FEC_PERITO,NULL) as FEC_PERITO" & vbCrLf
  SQL = SQL & "  , isnull(CNPJCPF_VISTORIADOR,0) as CNPJCPF_VISTORIADOR" & vbCrLf
  SQL = SQL & "  , produto_id" & vbCrLf
  SQL = SQL & "   from seguros_db.dbo.vistoria_tb r" & vbCrLf
  SQL = SQL & "   inner join seguros_db.dbo.apolice_tb a with(nolock)" & vbCrLf
  SQL = SQL & "   on r.apolice_id = a.apolice_id" & vbCrLf
  SQL = SQL & "   and r.ramo_id = a.ramo_id" & vbCrLf
  SQL = SQL & "   and r.sucursal_seguradora_id = a.sucursal_seguradora_id" & vbCrLf
  SQL = SQL & "   and r.seguradora_cod_susep = a.seguradora_cod_susep" & vbCrLf
  SQL = SQL & "   inner join seguros_db.dbo.proposta_tb p with(nolock)" & vbCrLf
  SQL = SQL & "   on p.proposta_id = a.proposta_id" & vbCrLf
  SQL = SQL & "  Where sinistro_id = " & dSinistro_ID & vbCrLf
    
    
   Set rsSet = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                            glAmbiente_id, _
                                            App.Title, _
                                            App.FileDescription, _
                                            SQL, _
                                            lConexaoLocal, True)
    
    If Not rsSet.EOF Then
     
     If rsSet("produto_id") = 1152 Or rsSet("produto_id") = 1204 Then
        
        While Not rsSet.EOF
             If rsSet("CNPJCPF_VISTORIADOR") <> 0 Then
               i = i + 1
             End If
             rsSet.MoveNext
        Wend
     
        If i = 0 Then Campos_Validos_Subvencao = False
     
     Else
           Campos_Validos_Subvencao = True
     End If
        
     rsSet.Close
        
    End If

End Function

Private Function VerificaCoberturarExcluirNPV(vSinistro_ID As String) As Boolean

'Jose Soares - Sala �gil Sprint 24 - H4

Dim sSQL As String
Dim rsCoberturas As ADODB.Recordset
Dim msgCobertura As String

On Error GoTo Trata_Erro

    VerificaCoberturarExcluirNPV = True
    
'    sSQL = "SELECT 'Titular' AS tipo " & vbNewLine
'    sSQL = sSQL & " , cliente_tb.nome AS nome" & vbNewLine
'    sSQL = sSQL & " , tp_cobertura_tb.nome AS nome_cobertura" & vbNewLine
'    sSQL = sSQL & " , sinistro_cobertura_tb.tp_cobertura_id AS tp_cobertura_id" & vbNewLine
'    sSQL = sSQL & " FROM seguros_db.dbo.proposta_tb proposta_tb WITH (NOLOCK)" & vbNewLine
'    sSQL = sSQL & " JOIN seguros_db.dbo.sinistro_tb sinistro_tb WITH (NOLOCK)" & vbNewLine
'    sSQL = sSQL & " ON proposta_tb.proposta_id = sinistro_tb.proposta_id" & vbNewLine
'    sSQL = sSQL & " JOIN seguros_db.dbo.cliente_tb cliente_tb WITH (NOLOCK)" & vbNewLine
'    sSQL = sSQL & " ON cliente_tb.cliente_id = proposta_tb.prop_cliente_id" & vbNewLine
'    sSQL = sSQL & " JOIN seguros_db.dbo.sinistro_cobertura_tb sinistro_cobertura_tb WITH (NOLOCK)" & vbNewLine
'    sSQL = sSQL & " ON sinistro_tb.sinistro_id = sinistro_cobertura_tb.sinistro_id" & vbNewLine
'    sSQL = sSQL & " AND sinistro_cobertura_tb.sinistro_motivo_encerramento_id = 10" & vbNewLine
'    sSQL = sSQL & " AND sinistro_cobertura_tb.tp_cobertura_id IN (1021, 1022, 1023, 1024)" & vbNewLine
'    sSQL = sSQL & " JOIN seguros_db.dbo.tp_cobertura_tb tp_cobertura_tb WITH (NOLOCK)" & vbNewLine
'    sSQL = sSQL & " ON tp_cobertura_tb.tp_cobertura_id = sinistro_cobertura_tb.tp_cobertura_id" & vbNewLine
'    sSQL = sSQL & " Where sinistro_tb.sinistro_id = " & vSinistro_id & vbNewLine
'    sSQL = sSQL & " AND proposta_tb.produto_id IN (1235, 1236, 1237)" & vbNewLine
'    sSQL = sSQL & " Union" & vbNewLine
'    sSQL = sSQL & " SELECT 'C�njuge' AS tipo" & vbNewLine
'    sSQL = sSQL & " , cliente_tb.nome AS nome" & vbNewLine
'    sSQL = sSQL & " , tp_cobertura_tb.nome AS nome_cobertura" & vbNewLine
'    sSQL = sSQL & " , sinistro_cobertura_tb.tp_cobertura_id AS tp_cobertura_id" & vbNewLine
'    sSQL = sSQL & " FROM seguros_db.dbo.proposta_tb proposta_tb WITH (NOLOCK)" & vbNewLine
'    sSQL = sSQL & " JOIN seguros_db.dbo.sinistro_tb sinistro_tb WITH (NOLOCK)" & vbNewLine
'    sSQL = sSQL & " ON proposta_tb.proposta_id = sinistro_tb.proposta_id" & vbNewLine
'    sSQL = sSQL & " JOIN seguros_db.dbo.proposta_complementar_tb proposta_complementar_tb WITH (NOLOCK)" & vbNewLine
'    sSQL = sSQL & " ON proposta_tb.proposta_id = proposta_complementar_tb.proposta_id" & vbNewLine
'    sSQL = sSQL & " AND proposta_complementar_tb.dt_fim_vigencia IS NULL" & vbNewLine
'    sSQL = sSQL & " JOIN seguros_db.dbo.cliente_tb cliente_tb WITH (NOLOCK)" & vbNewLine
'    sSQL = sSQL & " ON proposta_complementar_tb.prop_cliente_id = cliente_tb.cliente_id" & vbNewLine
'    sSQL = sSQL & " JOIN seguros_db.dbo.sinistro_cobertura_tb sinistro_cobertura_tb WITH (NOLOCK)" & vbNewLine
'    sSQL = sSQL & " ON sinistro_tb.sinistro_id = sinistro_cobertura_tb.sinistro_id" & vbNewLine
'    sSQL = sSQL & " AND sinistro_cobertura_tb.sinistro_motivo_encerramento_id = 10" & vbNewLine
'    sSQL = sSQL & " AND sinistro_cobertura_tb.tp_cobertura_id IN (1021, 1022, 1023, 1024)" & vbNewLine
'    sSQL = sSQL & " JOIN seguros_db.dbo.tp_cobertura_tb tp_cobertura_tb WITH (NOLOCK)" & vbNewLine
'    sSQL = sSQL & " ON tp_cobertura_tb.tp_cobertura_id = sinistro_cobertura_tb.tp_cobertura_id" & vbNewLine
'    sSQL = sSQL & " Where sinistro_tb.sinistro_id = " & vSinistro_id & vbNewLine
'    sSQL = sSQL & " AND proposta_tb.produto_id IN (1235, 1236, 1237)" & vbNewLine

        'Ajustando sele��o para trazer segurado que sofreu o sinistro - Jose Soares - 20/09/2019
    sSQL = "SELECT sinistro_vida_tb.nome AS nome " & vbNewLine
    sSQL = sSQL & " , tp_cobertura_tb.nome AS nome_cobertura" & vbNewLine
    sSQL = sSQL & " , sinistro_cobertura_tb.tp_cobertura_id AS tp_cobertura_id" & vbNewLine
    sSQL = sSQL & " FROM seguros_db.dbo.proposta_tb proposta_tb WITH (NOLOCK)" & vbNewLine
    sSQL = sSQL & " JOIN seguros_db.dbo.sinistro_tb sinistro_tb WITH (NOLOCK)" & vbNewLine
    sSQL = sSQL & " ON proposta_tb.proposta_id = sinistro_tb.proposta_id" & vbNewLine
    sSQL = sSQL & " JOIN seguros_db.dbo.sinistro_vida_tb sinistro_vida_tb WITH (NOLOCK)" & vbNewLine
    sSQL = sSQL & " ON sinistro_vida_tb.sinistro_id = sinistro_tb.sinistro_id" & vbNewLine
    sSQL = sSQL & " JOIN seguros_db.dbo.sinistro_cobertura_tb sinistro_cobertura_tb WITH (NOLOCK)" & vbNewLine
    sSQL = sSQL & " ON sinistro_tb.sinistro_id = sinistro_cobertura_tb.sinistro_id" & vbNewLine
    sSQL = sSQL & " AND sinistro_cobertura_tb.sinistro_motivo_encerramento_id = 10" & vbNewLine
    sSQL = sSQL & " AND sinistro_cobertura_tb.tp_cobertura_id IN (1021, 1022, 1023, 1024)" & vbNewLine
    sSQL = sSQL & " JOIN seguros_db.dbo.tp_cobertura_tb tp_cobertura_tb WITH (NOLOCK)" & vbNewLine
    sSQL = sSQL & " ON tp_cobertura_tb.tp_cobertura_id = sinistro_cobertura_tb.tp_cobertura_id" & vbNewLine
    sSQL = sSQL & " Where sinistro_tb.sinistro_id = " & vSinistro_ID & vbNewLine
    sSQL = sSQL & " AND proposta_tb.produto_id IN (1235, 1236, 1237)" & vbNewLine
 
        
    Set rsCoberturas = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                            glAmbiente_id, _
                            App.Title, _
                            App.FileDescription, _
                            sSQL, _
                            lConexaoLocal, True)
                            
    
    If Not rsCoberturas.EOF Then
        msgCobertura = "Est� de acordo com a exclus�o da cobertura " & rsCoberturas("nome_cobertura") & ", para o componente " & rsCoberturas("nome") & "?"
        msgCobertura = MsgBox(msgCobertura, vbYesNoCancel + vbQuestion)

        If msgCobertura = vbYes Then
            
            sSQL = ""
            sSQL = sSQL & " SET NOCOUNT ON EXEC seguros_db.dbo.SEGS14283_SPU " & vSinistro_ID & ", " & rsCoberturas("tp_cobertura_id") & ", '" & cUserName & "'"
            
            Call Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                glAmbiente_id, _
                App.Title, _
                App.FileDescription, _
                sSQL, _
                lConexaoLocal, False)
        ElseIf msgCobertura = vbCancel Then
            VerificaCoberturarExcluirNPV = False
        End If
             
    End If
        
    rsCoberturas.Close
    
    Set rsCoberturas = Nothing

    Exit Function
    
Trata_Erro:
    RetornarTransacao (lConexaoLocal)
    Screen.MousePointer = 0
    TrataErroGeral ("VerificaCoberturarExcluirNPV")


End Function

Public Function Envia_Proposta_Cloud(vProposta_id As String, vSinistro_ID As String) As Boolean
'Anderson Molina Silva - Confitec SP - 07/10/2019

Dim sSQL As String

On Error GoTo Trata_Erro
    
    sSQL = "exec interface_dados_db.dbo.SEGS14420_SPI "
    sSQL = sSQL & vProposta_id
    sSQL = sSQL & "," & vSinistro_ID
    sSQL = sSQL & ",'" & cUserName & "'"
    
    Call Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                glAmbiente_id, _
                                App.Title, _
                                App.FileDescription, _
                                sSQL, _
                                lConexaoLocal, False)


Envia_Proposta_Cloud = True

Exit Function

Trata_Erro:

    Envia_Proposta_Cloud = False

    RetornarTransacao (lConexaoLocal)
    Screen.MousePointer = 0
    TrataErroGeral ("Envia_Proposta_Cloud")

End Function
