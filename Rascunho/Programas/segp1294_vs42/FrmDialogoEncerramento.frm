VERSION 5.00
Begin VB.Form FrmDialogoEncerramento 
   Caption         =   "SEGP1294 - Encerramento de Sinistro "
   ClientHeight    =   3165
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   6000
   LinkTopic       =   "Form1"
   ScaleHeight     =   3165
   ScaleWidth      =   6000
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton cmd 
      Caption         =   "Cancelar"
      Height          =   375
      Index           =   2
      Left            =   3540
      TabIndex        =   2
      Top             =   2610
      Width           =   1215
   End
   Begin VB.CommandButton cmd 
      Caption         =   "N�o"
      Height          =   375
      Index           =   1
      Left            =   2235
      TabIndex        =   1
      Top             =   2610
      Width           =   1215
   End
   Begin VB.CommandButton cmd 
      Caption         =   "Sim"
      Height          =   375
      Index           =   0
      Left            =   960
      TabIndex        =   0
      Top             =   2610
      Width           =   1215
   End
   Begin VB.Label lblMensagem 
      BackStyle       =   0  'Transparent
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2025
      Left            =   240
      TabIndex        =   3
      Top             =   120
      Width           =   5250
   End
End
Attribute VB_Name = "FrmDialogoEncerramento"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private mResposta As VBA.VbMsgBoxResult
Private mMensagem As String

Public Property Get Resposta() As VBA.VbMsgBoxResult
    Resposta = mResposta
End Property

Public Property Get Mensagem() As String
    Mensagem = lblMensagem.Caption
End Property

Public Property Let Mensagem(ByVal Valor As String)
    lblMensagem.Caption = Valor
End Property

Private Sub cmd_Click(Index As Integer)
    If Index = 0 Then
        mResposta = vbYes
    ElseIf Index = 1 Then
        mResposta = vbNo
    ElseIf Index = 2 Then
        mResposta = vbCancel
    End If
    
    Unload Me
End Sub


Private Sub Form_Load()
    InicializaInterfaceLocal_Form
End Sub

Private Sub InicializaInterfaceLocal_Form()
    Dim sTipoProcesso_Descricao                 As String

   
    Call CentraFrm(Me)
    
    'mathayde
'    Select Case bytModoOperacao
'        Case "5"
'            sTipoProcesso_Descricao = "Encerramento de Sinistro"
'
'        Case "A"
'            sTipoProcesso_Descricao = "Encerramento de Sinistro Cosseguro"
'    End Select
        
    
    Me.Caption = App.EXEName & " - " & sTipoProcesso_Descricao & " - " & Ambiente
    

End Sub

