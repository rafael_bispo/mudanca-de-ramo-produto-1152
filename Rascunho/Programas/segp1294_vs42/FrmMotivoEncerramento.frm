VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form FrmMotivoEncerramento 
   Caption         =   "SEGP1294 - Motivo de Encerramento de Cobertura Sinistro - VIDA"
   ClientHeight    =   5160
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   11535
   LinkTopic       =   "Form1"
   ScaleHeight     =   5160
   ScaleWidth      =   11535
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton BtnCancelar 
      Caption         =   "&Cancelar"
      Height          =   375
      Left            =   9840
      TabIndex        =   7
      Top             =   4560
      Width           =   1452
   End
   Begin VB.CommandButton BtnOk 
      Caption         =   "Aplicar"
      Height          =   375
      Left            =   8280
      TabIndex        =   6
      Top             =   4560
      Width           =   1452
   End
   Begin VB.Frame FrameDados 
      Height          =   1575
      Left            =   240
      TabIndex        =   1
      Top             =   2760
      Width           =   11055
      Begin VB.CommandButton BtnAlterar 
         Caption         =   "Alterar"
         Height          =   375
         Left            =   9360
         TabIndex        =   8
         Top             =   1000
         Width           =   1452
      End
      Begin VB.TextBox txtCobertura 
         Enabled         =   0   'False
         Height          =   285
         Left            =   120
         TabIndex        =   3
         Top             =   480
         Width           =   10695
      End
      Begin VB.ComboBox cmbMotivo_Encerramento 
         Height          =   315
         ItemData        =   "FrmMotivoEncerramento.frx":0000
         Left            =   120
         List            =   "FrmMotivoEncerramento.frx":0002
         Style           =   2  'Dropdown List
         TabIndex        =   2
         Top             =   1080
         Width           =   9015
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Motivo de Encerramento"
         Height          =   195
         Left            =   120
         TabIndex        =   5
         Top             =   840
         Width           =   1740
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "Cobertura"
         Height          =   195
         Left            =   120
         TabIndex        =   4
         Top             =   240
         Width           =   690
      End
   End
   Begin MSFlexGridLib.MSFlexGrid GrdCobertura_Encerramento 
      Height          =   2415
      Left            =   240
      TabIndex        =   0
      TabStop         =   0   'False
      Top             =   240
      Width           =   11055
      _ExtentX        =   19500
      _ExtentY        =   4260
      _Version        =   393216
      Cols            =   5
      FillStyle       =   1
      SelectionMode   =   1
      FormatString    =   $"FrmMotivoEncerramento.frx":0004
   End
End
Attribute VB_Name = "FrmMotivoEncerramento"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim intMotivo As Integer
Dim intsinistro_id As Double
Dim rs As ADODB.Recordset

Public strCobertura_Motivo As String
Public hist_y As Integer
Public Detalhe_Encerra As String


Private Sub BtnAlterar_Click()

'    GrdCobertura_Encerramento.TextMatrix(GrdCobertura_Encerramento.Row, 3) = cmbMotivo_Encerramento.Text
'    GrdCobertura_Encerramento.TextMatrix(GrdCobertura_Encerramento.Row, 1) = cmbMotivo_Encerramento.ItemData(cmbMotivo_Encerramento.ListIndex)

    'FLAVIO.BEZERRA - INICIO - 19/08/2013
    If cmbMotivo_Encerramento.ListIndex >= 0 Then
        GrdCobertura_Encerramento.TextMatrix(GrdCobertura_Encerramento.Row, 3) = cmbMotivo_Encerramento.Text
        GrdCobertura_Encerramento.TextMatrix(GrdCobertura_Encerramento.Row, 1) = cmbMotivo_Encerramento.ItemData(cmbMotivo_Encerramento.ListIndex)
    End If
    'FLAVIO.BEZERRA - FIM
        
End Sub

Private Sub BtnCancelar_Click()
     FrmEncerramento.bPossuiMotivoEncerramentoCobertura = False 'cristovao.rodrigues 23/10/2012
     Unload Me
End Sub

Private Sub btnOK_Click()
    
    Dim SQLAux As String
    Dim rcsRecordSet As ADODB.Recordset
    Dim Cobertura As Integer
    Dim sinistro As Double
    Dim Motivo As Integer
    Dim i As Integer
    
    On Error GoTo Trata_Erro
   
   For i = 1 To GrdCobertura_Encerramento.Rows - 1
      If GrdCobertura_Encerramento.TextMatrix(i, 1) = "" Then
         If MsgBox("Existem coberturas sem motivos de encerramento cadastrado!", vbCritical) Then
            Exit Sub
         End If
      End If
   Next

      
    hist_y = 0
    vDetalhamento_Encerra = ""
    For i = 1 To GrdCobertura_Encerramento.Rows - 1
       If GrdCobertura_Encerramento.TextMatrix(i, 1) <> "" Then
           Cobertura = GrdCobertura_Encerramento.TextMatrix(i, 0)
           sinistro = GrdCobertura_Encerramento.TextMatrix(i, 4)
           Motivo = GrdCobertura_Encerramento.TextMatrix(i, 1)
           MotivoEncerramento = GrdCobertura_Encerramento.TextMatrix(i, 1) 'R.FOUREAUX - 25/05/2020
           vsql = "EXEC atualiza_cobertura_encerramento_spu " & _
                                               sinistro & "," & _
                                               Cobertura & "," & _
                                               " 'I' " & "," & _
                                               Motivo & ""

                 
            
            Call Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                        glAmbiente_id, _
                                        App.Title, _
                                        App.FileDescription, _
                                        vsql, _
                                        lConexaoLocal, _
                                        False)
                 

                SQLAux = "SELECT tp_cobertura_tb.nome + ' - ' + sinistro_motivo_encerramento_tb.descricao AS LINHA "
                SQLAux = SQLAux & "FROM sinistro_cobertura_tb WITH (NOLOCK) INNER JOIN "
                SQLAux = SQLAux & "tp_cobertura_tb WITH (NOLOCK) ON sinistro_cobertura_tb.tp_cobertura_id = tp_cobertura_tb.tp_cobertura_id INNER JOIN "
                SQLAux = SQLAux & "sinistro_motivo_encerramento_tb WITH (NOLOCK) ON "
                SQLAux = SQLAux & "sinistro_cobertura_tb.sinistro_motivo_encerramento_id = sinistro_motivo_encerramento_tb.sinistro_motivo_encerramento_id "
                SQLAux = SQLAux & "WHERE (sinistro_cobertura_tb.sinistro_id = " & sinistro & ") "
                SQLAux = SQLAux & "AND   (sinistro_cobertura_tb.tp_cobertura_id = " & Cobertura & ") "
                SQLAux = SQLAux & "AND   (sinistro_cobertura_tb.sinistro_motivo_encerramento_id = " & Motivo & ") "
                SQLAux = SQLAux & "AND   (sinistro_cobertura_tb.DT_FIM_VIGENCIA IS NULL) "
      
                
                Set rcsRecordSet = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                                glAmbiente_id, _
                                                App.Title, _
                                                App.FileDescription, _
                                                SQLAux, _
                                                lConexaoLocal, True)
                
                While Not rcsRecordSet.EOF
                    


                    ReDim Preserve hist_Motivo_encerramento(hist_y)
                    hist_Motivo_encerramento(hist_y) = "COBERTURA - " & rcsRecordSet(0)
                    hist_y = hist_y + 1
                    

                    If vDetalhamento_Encerra = "" Then
                        vDetalhamento_Encerra = rcsRecordSet(0)
                    Else
                        vDetalhamento_Encerra = vDetalhamento_Encerra & vbCrLf & rcsRecordSet(0)
                    End If
                    rcsRecordSet.MoveNext
                Wend
                rcsRecordSet.Close
       End If
    Next
    
    Qtde_Motivo_encerramento = hist_y
    


    vDetalhamento_Encerra = Format("Aviso de Sinistro encerrado" & _
                            Chr(10) & Chr(10) & _
                            vDetalhamento_Encerra & Chr(10))


    Detalhe_Encerra = vDetalhamento_Encerra

    FrmEncerramento.bPossuiMotivoEncerramentoCobertura = True 'cristovao.rodrigues 23/10/2012
    
   
    
    
    Unload Me
    
'cristovao.rodrigues 23/10/2012
Exit Sub
Trata_Erro:

MousePointer = 0
Call TrataErroGeral("Erro no preenchimento motivo encerramento da cobertura!", Me.name)
'mensagem_erro 6, "Erro no preenchimento motivo encerramento da cobertura!" & vbNewLine & Err.Description & vbNewLine & Err.Source & vbNewLine & Err.Number
BtnCancelar_Click

End Sub



Private Sub carrega_motivo()

    Dim SQLAux As String
    Dim rsaux As ADODB.Recordset
    
    SQLAux = "SELECT DISTINCT "
    SQLAux = SQLAux & "sinistro_motivo_encerramento_tb.descricao, sinistro_motivo_encerramento_tb.sinistro_motivo_encerramento_id,sinistro_cobertura_tb.Sinistro_id "
    SQLAux = SQLAux & "From sinistro_cobertura_tb WITH (NOLOCK) INNER JOIN "
    SQLAux = SQLAux & "ramo_tb WITH (NOLOCK) ON sinistro_cobertura_tb.ramo_id = ramo_tb.ramo_id INNER JOIN "
    SQLAux = SQLAux & "sinistro_motivo_encerramento_tb WITH (NOLOCK) INNER JOIN "
    SQLAux = SQLAux & "tp_ramo_tb WITH (NOLOCK) ON sinistro_motivo_encerramento_tb.tp_ramo_id = tp_ramo_tb.tp_ramo_id ON "
    SQLAux = SQLAux & "ramo_tb.tp_ramo_id = tp_ramo_tb.tp_ramo_id "
    SQLAux = SQLAux & "Where "
    SQLAux = SQLAux & "sinistro_cobertura_tb.sinistro_motivo_encerramento_id IS NULL AND "
    If Not IsNull(intsinistro_id) Then
        SQLAux = SQLAux & "sinistro_cobertura_tb.SINISTRO_ID = " & intsinistro_id & " AND "
    End If

    SQLAux = SQLAux & " sinistro_motivo_encerramento_tb.situacao = 'A'"
    
    'Henrique H Henriques - Ordena��o do motivo - INC000005178363 - CONFITEC SP - INI
    SQLAux = SQLAux & " order by sinistro_motivo_encerramento_tb.descricao"
    'Henrique H Henriques - Ordena��o do motivo - INC000005178363 - CONFITEC SP - FIM

    Set rsaux = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                    glAmbiente_id, _
                                    App.Title, _
                                    App.FileDescription, _
                                    SQLAux, _
                                    lConexaoLocal, True)
    If Not rsaux.EOF Then
        While Not rsaux.EOF
            Me.cmbMotivo_Encerramento.AddItem rsaux(0)
            Me.cmbMotivo_Encerramento.ItemData(cmbMotivo_Encerramento.NewIndex) = rsaux(1)
            rsaux.MoveNext
        Wend
    End If
    rsaux.Close

End Sub

Private Sub grdEncerramento_Motivo_RowColChange()
    
    txtCobertura.Text = GrdCobertura_Encerramento.TextMatrix(GrdCobertura_Encerramento.Row, 2)

End Sub

Private Sub Form_Load()
    
       
    Carrega_GrdMotivo_Cobertura
    
    carrega_motivo
    
    InicializaInterfaceLocal_Form
    
End Sub

Private Sub Carrega_GrdMotivo_Cobertura()
          
    Dim rsRecordSet As ADODB.Recordset
    Dim sSQL As String
    
    On Error GoTo Trata_Erros
           
    txtCobertura.Text = ""
     
    sSQL = "SELECT DISTINCT "
    sSQL = sSQL & "tp_cobertura_tb.nome AS Cobertura, "
    sSQL = sSQL & "sinistro_cobertura_tb.sinistro_motivo_encerramento_id, "
    sSQL = sSQL & "sinistro_motivo_encerramento_tb.descricao AS DescricaoSinistro, "
    sSQL = sSQL & "sinistro_cobertura_tb.tp_Cobertura_id,"
    sSQL = sSQL & "sinistro_cobertura_tb.Sinistro_id "
    sSQL = sSQL & "From "
    sSQL = sSQL & "sinistro_cobertura_tb WITH (NOLOCK) LEFT OUTER JOIN "
    sSQL = sSQL & "sinistro_motivo_encerramento_tb WITH (NOLOCK) ON "
    sSQL = sSQL & "sinistro_cobertura_tb.sinistro_motivo_encerramento_id = sinistro_motivo_encerramento_tb.sinistro_motivo_encerramento_id LEFT OUTER JOIN "
    sSQL = sSQL & "tp_cobertura_tb WITH (NOLOCK) ON sinistro_cobertura_tb.tp_cobertura_id = tp_cobertura_tb.tp_cobertura_id "
    sSQL = sSQL & "Where "
    sSQL = sSQL & "(sinistro_motivo_encerramento_tb.sinistro_motivo_encerramento_id IS NULL) AND "
    sSQL = sSQL & "(sinistro_cobertura_tb.tp_cobertura_id IS NOT NULL) "
    sSQL = sSQL & "AND (sinistro_cobertura_tb.dt_fim_vigencia is null OR "
    sSQL = sSQL & "(CONVERT(VARCHAR,sinistro_cobertura_tb.dt_fim_vigencia,112) > "
    sSQL = sSQL & "CONVERT(VARCHAR,getdate(),112))) "
    sSQL = sSQL & " AND (sinistro_cobertura_tb.sinistro_id = cast(" & dSinistro_ID & " as varchar)) "
    sSQL = sSQL & "Order By tp_cobertura_tb.Nome"

    Set rsRecordSet = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                            glAmbiente_id, _
                                            App.Title, _
                                            App.FileDescription, _
                                            sSQL, _
                                            lConexaoLocal, True)

    GrdCobertura_Encerramento.ColWidth(0) = 0 'tp_Cobertura_id
    GrdCobertura_Encerramento.ColWidth(1) = 0 'sinistro_motivo_encerramento_id
    GrdCobertura_Encerramento.ColWidth(4) = 0 'sinistro_id
    intsinistro_id = dSinistro_ID
    

    GrdCobertura_Encerramento.FormatString = "||Cobertura                                                                                                       |Motivo de Encerramento                                                                              |"
    GrdCobertura_Encerramento.Rows = 1
    
    While Not rsRecordSet.EOF
        With GrdCobertura_Encerramento
            .AddItem rsRecordSet(3) & vbTab & rsRecordSet(1) & vbTab & rsRecordSet(0) & vbTab & rsRecordSet(2) & vbTab & rsRecordSet(4)
            .Col = 1
            .Row = .Rows - 1
            .CellAlignment = 1
            .ColAlignment(2) = 1
        End With
        rsRecordSet.MoveNext
    Wend
    rsRecordSet.Close
    
    Exit Sub
Trata_Erros:

    Call TrataErroGeral("Carrega_GrdMotivo_Encerramento", Me.name)
    RetornarTransacao (lConexaoLocal) 'cristovao.rodrigues 23/10/2012
    Call FinalizarAplicacao
    
End Sub

Private Sub GrdCobertura_Encerramento_Click()

    txtCobertura.Text = GrdCobertura_Encerramento.TextMatrix(GrdCobertura_Encerramento.Row, 2)
    intsinistro_id = GrdCobertura_Encerramento.TextMatrix(GrdCobertura_Encerramento.Row, 4)
 
End Sub

Private Sub InicializaInterfaceLocal_Form()
    Dim sTipoProcesso_Descricao                 As String

   
    Call CentraFrm(Me)
        
'    'mathayde
'    Select Case bytModoOperacao
'        Case "5"
'            sTipoProcesso_Descricao = "Encerramento de Sinistro"
'
'        Case "A"
'            sTipoProcesso_Descricao = "Encerramento de Sinistro Cosseguro"
'    End Select
        

    
    Me.Caption = App.EXEName & " - " & sTipoProcesso_Descricao & " - " & Ambiente
    

End Sub
