Attribute VB_Name = "ModuloLocal"
Global MotivoEncerramento As Integer 'R.FOUREAUX - 25/05/2020
Global dSinistro_ID         As String
Global vEncerrou            As Boolean
Global vSitSel              As Integer
Global vOption_Escolhida    As String
Global Colecao_Sinistro_id  As String
'Global Sinistro_Catastrofe_Contador As String  'AKIO.OKUNO - 29/09/2012 - ALTERADO PARA INT
Global Sinistro_Catastrofe() As String
Global SinBB_Situacao       As String
Global SinBB_Tipo           As String
Global Apolice_id As String
Global Sucursal_id As String
Global Seguradora_id As String
Global ramo_id As String
Global Qtde_Motivo_encerramento As Integer
Global Dt_aviso_sinistro As String
Global Dt_ocorrencia_sinistro As String
Global Agencia_id As Integer
Global Solicitante_Sinistro_Nome As String
Global Solicitante_Sinistro_Endereco As String
Global Solicitante_Sinistro_ddd As String
Global Solicitante_Sinistro_telefone As String
Global Banco_id As Integer
Global Cliente_id As String
Global Cliente_Nome As String
Global Proposta_id As String
Global Produto_id As String
Global Sinistro_Endereco As String
Global Sinistro_Municipio As String
Global Sinistro_UF As String
Global GTR_Implantado           As Boolean
Global bytModoOperacao                         As Byte
Global vTp_detalhamento As Integer
Global vDetalhamento_Encerra  As String
Global iQtdItemEstimativa As Integer
Global vDetalhamento As String
Global vRestrito As String
Global Detalhe_Encerra As String
Global Data_Fim_Vistoria As String
Global Resultado_Fim_Vistoria  As String
Global Usuario_Vistoria  As String
Global Tipo_Vistoria As String
Global Dt_inclusao As Date
Global vRegulador_id            As Integer
Global retorno_entrada_GTR_tb   As Boolean
Global Evento_SEGBR_id As String
Global Procedure As String
Global Incluiu_Novo_SinistroBB  As Boolean
Global Detalhamento_id          As String
Global vTp_exigencia            As Integer
Global ReturnRequest            As Integer
'Global Const vbRecipTypeTo = 1
Global Const vbRecipTypeCc = 2
'Global Const vbMessageSendDlg = 2
'Global Const vbMessageSend = 3
Global Const vbRecipientDelete = 14
Global Const vbMessageResolveName = 13
Global vObtem_GTR_Implantado As Boolean
