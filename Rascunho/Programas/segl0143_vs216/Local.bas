Attribute VB_Name = "Local"
'Constantes de Produto_id, usando o nome do produto''''''''''''''
Public Const PROD_ID_CONSORCIO_PRESTAMISTA = 718
Public Const PROD_ID_QUEBRA_GARANTIA = 719
Public Const PROD_ID_CDC_CREDIARIO = 723
Public Const PROD_ID_CONSORCIO_IMOB = 680
Public Const gsOBJETOAMBIENTE = "SABL0010.cls00009"
Public Const PROD_ID_QUEBRA_GARANTIA_PJ = 819

Public Declare Function GetPrivateProfileString Lib "kernel32" Alias "GetPrivateProfileStringA" (ByVal lpApplicationName As String, ByVal lpKeyName As Any, ByVal lpDefault As String, ByVal lpReturnedString As String, ByVal nSize As Long, ByVal lpFileName As String) As Long

'MATHAYDE
Global perc_comissao                As Double
Global perc_comissao_estipulante    As Double
Global val_comissao                 As Currency
Global val_comissao_estipulante     As Currency
'RMesquita
Global vlPremioTarifa As Double
Global vDt_Recebimeto As String

Public Sub adSQL(ByRef sSQL As String, ByVal sCadeia As String)
'Jorfilho 04/11/2002 - Concatena a query com vbNewLine
   sSQL = sSQL & sCadeia & vbNewLine
End Sub

Function MudaVirgulaParaPonto(ByVal valor As String) As String

    If InStr(valor, ",") = 0 Then
       MudaVirgulaParaPonto = valor
    Else
      valor = Mid$(valor, 1, InStr(valor, ",") - 1) + "." + Mid$(valor, InStr(valor, ",") + 1)
      MudaVirgulaParaPonto = valor
    End If

End Function

'RMesquita
Public Function ObterProjeto(sNomeProjeto, sSiglaSistema As String, iAmbienteId As Integer, sSiglaRecurso As String, sDescricaoRecurso As String, lConexao As Long) As Long

Dim sSQL  As String
Dim oRs   As Object

On Error GoTo TrataErro
  
  sSQL = Space(8000)

  sSQL = ""
  sSQL = sSQL & "SELECT ISNULL(projeto_id,0)" & vbNewLine
  sSQL = sSQL & "  FROM controle_projeto_db..projeto_tb" & vbNewLine
  sSQL = sSQL & " WHERE nome LIKE '" & sNomeProjeto & "'"
  
  Set oRs = Conexao_ExecutarSQL(sSiglaSistema, _
                               iAmbienteId, _
                               sSiglaRecurso, _
                               sDescricaoRecurso, _
                               sSQL, _
                               lConexao, _
                               True, _
                               False, _
                               30000, _
                               adLockOptimistic, _
                               adUseClient)

  
  If Not oRs.EOF Then
    ObterProjeto = oRs(0)
  Else
    ObterProjeto = 0
  End If
                                 
  'Barney - 06/02/2004
  oRs.Close
  Set oRs = Nothing
                                 
Exit Function

TrataErro:
   Call Err.Raise(Err.Number, , "SEGL0143.Local.ObterProjeto - " & Err.Description)
   
End Function

'RMesquita
Private Function ObterClasseEndosso(iTpEndossoId As Integer, sSiglaSistema As String, iAmbienteId As Integer, sSiglaRecurso As String, sDescricaoRecurso As String, lConexao As Long) As Integer

Dim oRs  As Object
Dim sSQL As String

On Error GoTo TrataErro

  If iTpEndossoId = 0 Then
    ObterClasseEndosso = 0
    Exit Function
  End If
    
  'Obtendo a classe do endosso
  
  sSQL = ""
  sSQL = sSQL & "SELECT ISNULL(classe_endosso_id,0) " & vbNewLine
  sSQL = sSQL & "  FROM seguros_db..tp_endosso_tb tp_endosso_tb" & vbNewLine
  sSQL = sSQL & " WHERE tp_endosso_id = " & iTpEndossoId & vbNewLine
  
   Set oRs = Conexao_ExecutarSQL(sSiglaSistema, _
                               iAmbienteId, _
                               sSiglaRecurso, _
                               sDescricaoRecurso, _
                               sSQL, _
                               lConexao, _
                               True, _
                               False, _
                               30000, _
                               adLockOptimistic, _
                               adUseClient)
  
  If Not oRs.EOF Then
    ObterClasseEndosso = oRs(0)
  Else
    ObterClasseEndosso = 0
  End If
  
  'Barney - 06/02/2004
  oRs.Close
  Set oRs = Nothing
                                 
Exit Function

TrataErro:
   Call Err.Raise(Err.Number, , "SEGL0143.Local.ObterClasseEndosso - " & Err.Description)

End Function

'RMesquita
Public Sub IncluirEventoEmissao(lPropostaId As Long, _
                                lEndossoId As Long, _
                                iProdutoId As Integer, _
                                iRamoId As Integer, _
                                iTpEndossoId As Integer, _
                                sNomeProjeto As String, _
                                DtEvento As String, _
                                sUsuario As String, sSiglaSistema As String, iAmbienteId As Integer, sSiglaRecurso As String, sDescricaoRecurso As String, lConexao As Long)

Dim sSQL       As String
Dim iProjetoId As Integer
Dim iClasseEndossoId As Integer

On Error GoTo TrataErro

  'Obtendo o projeto_id
  iProjetoId = ObterProjeto(sNomeProjeto, sSiglaSistema, iAmbienteId, sSiglaRecurso, sDescricaoRecurso, lConexao)
  
  'Obtendo a classe do endosso
  iClasseEndossoId = ObterClasseEndosso(iTpEndossoId, sSiglaSistema, iAmbienteId, sSiglaRecurso, sDescricaoRecurso, lConexao)
  
  
  'Inserindo informa��es em evento_emissao_tb
  sSQL = ""
  sSQL = "EXEC evento_seguros_db..evento_emissao_spi "
  sSQL = sSQL & IIf(Val(lPropostaId) = 0, "null", lPropostaId) & ", "
  sSQL = sSQL & IIf(Val(lEndossoId) = 0, "null", lEndossoId) & ", "
  sSQL = sSQL & IIf(Val(iProdutoId) = 0, "null", iProdutoId) & ", "
  sSQL = sSQL & IIf(Val(iRamoId) = 0, "null", iRamoId) & ", "
  sSQL = sSQL & IIf(Val(iProjetoId) = 0, "null", iProjetoId) & ", "
  sSQL = sSQL & IIf(Val(iClasseEndossoId) = 0, "null", iClasseEndossoId) & ", "
  sSQL = sSQL & "'" & Format(DtEvento, "yyyymmdd") & "', "
  sSQL = sSQL & "'" & sUsuario & "'"
  
  
  Call Conexao_ExecutarSQL(sSiglaSistema, _
                               iAmbienteId, _
                               sSiglaRecurso, _
                               sDescricaoRecurso, _
                               sSQL, _
                               lConexao, _
                               False, _
                               False, _
                               30000, _
                               adLockOptimistic, _
                               adUseClient)


Exit Sub

TrataErro:
   Call Err.Raise(Err.Number, , "SEGL0143.Local.IncluirEventoEmissao - " & Err.Description)

End Sub

'RMesquita
Public Function IncluirEvento(ByVal ltpEventoid As Long, _
                              ByVal lApoliceId As Long, _
                              ByVal LramoId As Long, _
                              ByVal lProdutoId As Long, _
                              ByVal lPropostaId As Long, _
                              ByVal lEndossoId As Long, _
                              ByVal sdtEvento As String, _
                              ByVal sUsuario As String, ByVal sSiglaSistema As String, ByVal iAmbienteId As Integer, ByVal sSiglaRecurso As String, ByVal sDescricaoRecurso As String, ByVal lConexao As Long) As Boolean

Dim sSQL As String

IncluirEvento = True

On Error GoTo TrataErro

sSQL = "exec evento_seguros_db..historico_evento_spi "
sSQL = sSQL & " Null  "                   '@layout_id
sSQL = sSQL & ",Null  "                   '@remessa_id
sSQL = sSQL & ",Null  "                   '@dt_ini_vigencia
sSQL = sSQL & ", " & ltpEventoid          '@tp_evento_id
sSQL = sSQL & ",Null  "                   '@estado_id
sSQL = sSQL & ",Null  "                   '@proposta_bb
sSQL = sSQL & ",Null  "                   '@endosso_bb
sSQL = sSQL & ", " & lApoliceId           '@apolice_id
sSQL = sSQL & ", " & LramoId              '@ramo_id
sSQL = sSQL & ", " & lProdutoId           '@produto_id
sSQL = sSQL & ", " & lPropostaId          '@proposta_id
If lEndossoId = 0 Then
   sSQL = sSQL & ", Null "                '@endosso_id                   ENDOSSO (PARA OS CASOS DE ENDOSSO, NULL PARA EMISSAO,
Else
   sSQL = sSQL & ", " & lEndossoId        '@endosso_id                   ENDOSSO (PARA OS CASOS DE ENDOSSO, NULL PARA EMISSAO,
End If
sSQL = sSQL & ",'" & sdtEvento & "'"      '@dt_evento                    ATRIBUTO INFORMADO NA TELA (DATA DE RECEBIMENTO DO PROCESSO),
sSQL = sSQL & ",Null  "                   '@valor1
sSQL = sSQL & ",Null  "                   '@valor2
sSQL = sSQL & ",Null  "                   '@valor3
sSQL = sSQL & ",Null  "                   '@valor4
sSQL = sSQL & ",Null  "                   '@valor5
sSQL = sSQL & ",Null  "                   '@valor6
sSQL = sSQL & ",Null  "                   '@valor7
sSQL = sSQL & ",Null  "                   '@valor8
sSQL = sSQL & ",Null  "                   '@valor9
sSQL = sSQL & ",Null  "                   '@valor10
sSQL = sSQL & ",Null  "                   '@certificado_id
sSQL = sSQL & ",Null  "                   '@cod_ramo_bb
sSQL = sSQL & ",Null  "                   '@entidade_id
sSQL = sSQL & ",Null  "                   '@origem_destino
sSQL = sSQL & ",'" & sUsuario & "'"      '@usuario                             USU�RIO
                                                                '    @dt_recebimento_processo

  Call Conexao_ExecutarSQL(sSiglaSistema, _
                               iAmbienteId, _
                               sSiglaRecurso, _
                               sDescricaoRecurso, _
                               sSQL, _
                               lConexao, _
                               False, _
                               False, _
                               30000, _
                               adLockOptimistic, _
                               adUseClient)

Exit Function

TrataErro:
   Call Err.Raise(Err.Number, , "SEGL0143.Local.IncluirEvento - " & Err.Description)

End Function

'RMesquita
Public Sub IncluirEndossoCancelamentoApolice(ByVal lApoliceId As Long, ByVal lSucursal As Long, _
                                              ByVal lSeguradora As Long, ByVal lPropostaId As Long, _
                                              ByVal LramoId As Long, ByVal cValFinanceiro As Currency, _
                                              ByVal lTpEndossoId As Long, ByVal cValPremioTarifa As Currency, _
                                              ByVal cValCustoEmissao As Currency, ByVal cValCorretagem As Currency, _
                                              ByVal cValProLabore As Currency, ByVal cValIOF As Currency, _
                                              ByVal cValIR As Currency, ByVal cValDesconto As Currency, _
                                              ByVal cValAdicFracionamento As Currency, _
                                              ByVal lSeguroMoedaId As Long, ByVal lPremioMoedaId As Long, _
                                              ByVal cValParidadeMoeda As Currency, ByVal cValIs As Currency, _
                                              ByVal sDtPedidoEndosso As String, ByVal sDtEmissao As String, _
                                              ByVal lBeneficiarioRestituicaoClienteId As Long, ByVal cPercIR As Currency, _
                                              ByVal cValCliente As Currency, ByVal sFlagAprovacao As String, _
                                              ByVal sTextoEndosso As String, ByVal sDtBaixa As String, _
                                              ByVal sDtInicioCancelamento, ByVal sTpCancelamento As String, _
                                              ByVal sMotivoCancelamento As String, ByVal sSiglaSistema As String, ByVal iAmbienteId As Integer, ByVal sUsuario As String, ByVal sSiglaRecurso As String, ByVal sDescricaoRecurso As String)
 
Dim oCancelamento As Object
Dim colCorretagem As Collection
Dim colAgendamento As New Collection
 
On Error GoTo TrataErro
 
    Set colCorretagem = ConsultarCorretagem(lPropostaId, sSiglaSistema, iAmbienteId, sSiglaRecurso, sDescricaoRecurso)
 
    Set oCancelamento = CreateObject("SEGL0140.cls00171")
 
    Call oCancelamento.CancelarApolice(sSiglaSistema, sSiglaRecurso, _
                                       sDescricaoRecurso, iAmbienteId, _
                                       sUsuario, lApoliceId, lSucursal, _
                                       lSeguradora, lPropostaId, _
                                       LramoId, cValFinanceiro, _
                                       lTpEndossoId, cValPremioTarifa, _
                                       cValCustoEmissao, colAgendamento, _
                                       colCorretagem, cValCorretagem, _
                                       cValProLabore, cValIOF, _
                                       cValIR, cValDesconto, _
                                       cValAdicFracionamento, _
                                       lSeguroMoedaId, lPremioMoedaId, _
                                       cValParidadeMoeda, cValIs, _
                                       sDtPedidoEndosso, sDtEmissao, _
                                       lBeneficiarioRestituicaoClienteId, cPercIR, cValCliente, _
                                       sFlagAprovacao, _
                                       sTextoEndosso, _
                                       sDtBaixa, sDtInicioCancelamento, _
                                       sTpCancelamento, sMotivoCancelamento, , , _
                                       IIf(vlPremioTarifa <> 0, True, False))
 
 

    Set oCancelamento = Nothing
    
    Exit Sub
 
TrataErro:
    Call Err.Raise(Err.Number, , "SEGL0143.Local.IncluirEndossoCancelamentoApolice - " & Err.Description)
 
End Sub

'RMesquita
Public Sub IncluirAvaliacaoRestituicao(ByVal lPropostaId As Long, _
                                        ByVal cValRestituicao As Currency, _
                                        ByVal cValComissao As Currency, _
                                        ByVal cValIOF As Currency, _
                                        ByVal cValAdicFracionamento As Currency, _
                                        ByVal cValDescontoComercial As Currency, _
                                        ByVal cValPremioTarifa As Currency, _
                                        ByVal cValCustoApolice As Currency, _
                                        ByVal cValComissaoEstipulante As Currency, _
                                        ByVal dValCambio As Double, _
                                        ByVal sDtPedidoAvaliacao As String, _
                                        ByVal sDescricao As String, _
                                        ByVal sSiglaSistema As String, _
                                        ByVal iAmbienteId As Integer, _
                                        ByVal sUsuario As String, _
                                        ByVal sSiglaRecurso As String, _
                                        ByVal sDescricaoRecurso As String, _
                                                                                ByVal lConexao As Long, _
                                        Optional ByVal lEndossoId As Long = 0, _
                                        Optional ByVal lTpEndossoId As Long = 0, _
                                        Optional ByVal bPossuiSubvencao As Boolean = False, _
                                        Optional ByVal cValSubvencaoFederal As Currency = 0, _
                                        Optional ByVal cValSubvencaoEstadual As Currency = 0)
 

Dim colCorretagem As Collection
 
On Error GoTo TrataErro
 
    Set colCorretagem = ConsultarCorretagem(lPropostaId, sSiglaSistema, iAmbienteId, sSiglaRecurso, sDescricaoRecurso)
    
    For Each Item In colCorretagem
        If Item.Tipo = "PJ" Then
            Item.Tipo = 1
        ElseIf Item.Tipo = "PF" Then
            Item.Tipo = 2
        End If
    Next
 
    Call IncluirAvaliacaoRestituicaoCredProt(sSiglaSistema, _
                                                 sSiglaRecurso, _
                                                 sDescricaoRecurso, _
                                                 iAmbienteId, _
                                                 sUsuario, _
                                                 lPropostaId, _
                                                 cValRestituicao, cValRestituicao * 0.05, _
                                                 cValComissao, _
                                                 cValIOF, _
                                                 cValAdicFracionamento, _
                                                 cValDescontoComercial, _
                                                 cValPremioTarifa, _
                                                 cValCustoApolice, _
                                                 cValComissaoEstipulante, _
                                                 dValCambio, _
                                                 sDtPedidoAvaliacao, _
                                                 sDescricao, _
                                                 colCorretagem, lConexao, _
                                                 lEndossoId, _
                                                 lTpEndossoId, _
                                                 0, 0, 0, 0, 0, 0, 0, 0, "N", _
                                                 0, _
                                                 bPossuiSubvencao, _
                                                 cValSubvencaoFederal, _
                                                 cValSubvencaoEstadual)
 
 
 
 
 
     
    Exit Sub
 
TrataErro:
    Call Err.Raise(Err.Number, , "SEGL0143.Local.IncluirAvaliacaoRestituicao - " & Err.Description)
 
End Sub

'RMesquita
Public Function ConsultarCorretagem(ByVal lPropostaId As Long, ByVal sSiglaSistema As String, ByVal iAmbienteId As Integer, ByVal sSiglaRecurso As String, ByVal sDescricaoRecurso As String) As Collection
        
Dim oApolice As Object
Dim colCorretagem As New Collection
Dim rs As Recordset

On Error GoTo TrataErro
    
    Set oApolice = CreateObject("SEGL0026.cls00126")
    
    Set rs = oApolice.ConsultarDados(sSiglaSistema, _
                                     sSiglaRecurso, _
                                     sDescricaoRecurso, _
                                     iAmbienteId, _
                                     lPropostaId)
    
    Set colCorretagem = oApolice.ObterCorretagem(sSiglaSistema, _
                                                 sSiglaRecurso, _
                                                 sDescricaoRecurso, _
                                                 iAmbienteId, _
                                                 lPropostaId, _
                                                 Format(rs("dt_inicio_vigencia"), "dd/mm/yyyy"), _
                                                 0, _
                                                 0, _
                                                 0, _
                                                 0)
    
    Set oApolice = Nothing
    
    If colCorretagem.Count <= 0 Then

        Set oApolice = CreateObject("SEGL0026.cls00126")
        
        Set colCorretagem = Nothing
        Set colCorretagem = oApolice.ObterCorretagem(sSiglaSistema, _
                                             sSiglaRecurso, _
                                             sDescricaoRecurso, _
                                             iAmbienteId, _
                                             lPropostaId, _
                                             Format(rs("dt_contratacao"), "dd/mm/yyyy"), _
                                             0, _
                                             0, _
                                             0, _
                                             0)
                                             
        Set oApolice = Nothing
        
    End If
    
    rs.Close
    Set rs = Nothing
     
    Set ConsultarCorretagem = colCorretagem
    
    Set colCorretagem = Nothing
              
    Exit Function
   
TrataErro:
    Call Err.Raise(Err.Number, , "SEGL0143.Local.ConsultarCorretagem - " & Err.Description)

End Function

'RMesquita
Public Function ObterValoresRestituicao(ByVal lPropostaId As Long, ByVal iAmbienteId As Integer, ByVal sSiglaSistema As String, ByVal sSiglaRecurso As String, ByVal sDescricaoRecurso As String, ByVal lConexao As Long) As Double

'Dim oFinanceiro As Object
Dim rs As Recordset

Dim cValPago As Currency
Dim cValJuros As Currency
Dim cValDesconto As Currency
Dim cValIOF As Currency
Dim cPremioTotal As Currency
Dim cValRestituicao As Currency
Dim oApolice As Object
Dim valCustoApoliceCert As Double

'HOLIVEIRA: Se bRestIntegralALS = TRUE ent�o � Restitui��o Integral ALS. DT: 12/02/2007
Dim bRestIntegralALS As Boolean

On Error GoTo TrataErro
    
    cValPago = 0
    cValJuros = 0
    cValDesconto = 0
    cPremioTotal = 0
    cValIOF = 0
    vDt_Recebimeto = ""
    
    'bcarneiro - 02/03/2007
    bRestIntegral = False
    ''''''''''''''''''''''''
    
    Set rs = ObterPremioPagoCredProt(lPropostaId, _
                                     iAmbienteId, _
                                     sSiglaRecurso, _
                                     sDescricaoRecurso, _
                                     lConexao)
    
    If Not rs.EOF Then
        While Not rs.EOF
            cPremioTotal = cPremioTotal + CCur(rs("valor_pago"))
            cValJuros = cValJuros + CCur(rs("val_adic_fracionamento"))
            cValDesconto = cValDesconto + CCur(rs("val_desconto"))
            cValIOF = cValIOF + CCur(rs("val_iof"))
            If UCase(rs("situacao")) = "A" Then
                cValPago = cValPago + CCur(rs("valor_pago"))
                If Not IsNull(rs("dt_recebimento")) Then
                    vDt_Recebimeto = Format(rs("dt_recebimento"), "dd/mm/yyyy")
                End If
            End If
            
            rs.MoveNext
        Wend
    End If
    
    Set rs = Nothing
    
    Set oApolice = CreateObject("SEGL0026.cls00126")

    Set rs = oApolice.ConsultarDados(sSiglaSistema, _
                                     sSiglaRecurso, _
                                     sDescricaoRecurso, _
                                     iAmbienteId, _
                                     lPropostaId)
    
    valCustoApoliceCert = rs("custo")
    
    Set oApolice = Nothing
    Set rs = Nothing
    
    If cValPago > 0 Then
        cValPago = cValPago - valCustoApoliceCert
        cPremioTotal = cPremioTotal - valCustoApoliceCert
    End If
    
    cValRestituicao = CalcularRestituicao(cPremioTotal, _
                                          cValJuros, _
                                          cValDesconto, _
                                          cValIOF, _
                                          cValPago, _
                                          lPropostaId, iAmbienteId, sSiglaSistema, sSiglaRecurso, sDescricaoRecurso, _
                                          bRestIntegralALS, vDt_Recebimeto) 'HOLIVEIRA: Retorna TRUE em bRestIntegralALS quando for Restitui��o Integral ALS. DT: 12/02/2007
        
    ObterValoresRestituicao = cValRestituicao
    
     
    
    Exit Function
    
TrataErro:
   Call Err.Raise(Err.Number, , "SEGL0143.Local.ObterValoresRestituicao - " & Err.Description)
   
End Function

'RMesquita
Public Function CalcularRestituicao(ByVal cPremioTotal As Currency, _
                                    ByVal cValorJuros As Currency, _
                                    ByVal cValDesconto As Currency, _
                                    ByVal cValIOF As Currency, _
                                    ByVal cValPago As Currency, _
                                    ByVal lPropostaId As Long, _
                                    ByVal iAmbienteId As Integer, _
                                    ByVal sSiglaSistema As String, _
                                    ByVal sSiglaRecurso As String, _
                                    ByVal sDescricaoRecurso As String, _
                                    Optional ByRef bRestIntegralALS As Boolean = False, Optional sDt_Recebimento As String = "") As Currency 'HOLIVEIRA: Retorna bRestIntegralALS = TRUE quando for Restitui��o Integral ALS. DT: 12/02/2007

Dim sDataInicio As String
Dim sDataCanc As String
Dim sDataFim As String

Dim dFatorDias As Double
Dim dDias As Double

Dim cValRestituicao As Currency

Dim oProposta As Object  'Rmarins 13/12/2006
Dim oApolice As Object
Dim oRsProposta As Object
Dim iOrigemProposta As Integer

'(INI) Bruno Faria - 19/01/2011
Dim oCancelamentoProposta As Object
Dim oRsCancelamentoProposta As Object
'(FIM) Bruno Faria - 19/01/2011

Dim rs As Recordset

On Error GoTo TrataErro

    Set oApolice = CreateObject("SEGL0026.cls00126")

    Set rs = oApolice.ConsultarDados(sSiglaSistema, _
                                     sSiglaRecurso, _
                                     sDescricaoRecurso, _
                                     iAmbienteId, _
                                     lPropostaId)
    
    sDataInicio = Format(rs("dt_inicio_vigencia"), "dd/mm/yyyy")
    sDataFim = Format(rs("dt_fim_vigencia"), "dd/mm/yyyy")
    If sDt_Recebimento <> "" Then
        sDataCanc = sDt_Recebimento
    Else
        sDataCanc = Format(Date, "dd/mm/yyyy")
    End If
        
    cValRestituicao = cValPago - (cPremioTotal / (DateDiff("d", sDataInicio, sDataFim)) * (DateDiff("d", sDataInicio, sDataCanc)))
    
    Set oApolice = Nothing
    Set rs = Nothing
   
    If cValRestituicao < 0 Then
       cValRestituicao = 0
    End If
    
    CalcularRestituicao = cValRestituicao
    
       
    Exit Function

TrataErro:
    Call Err.Raise(Err.Number, , "SEGL0143.Local.CalcularRestituicao - " & Err.Description)

End Function

'RMesquita
Public Function CalcularRestituicaoCurtoPrazo(ByVal dDias As Double, _
                                              ByVal cValPago As Currency, _
                                              ByVal cPremioTotal As Currency, _
                                              ByVal iAmbienteId As Integer, _
                                              ByVal sSiglaSistema As String, ByVal sSiglaRecurso As String, ByVal sDescricaoRecurso As String) As Currency

Dim oFinanceiro As Object
Dim rs As Recordset
Dim dPercRestituicao As Double
Dim cValRestituicao As Currency

On Error GoTo TrataErro

    Set oFinanceiro = CreateObject("SEGL0022.cls00117")
    
    Set rs = oFinanceiro.ConsultarCurtoPrazo(sSiglaSistema, _
                                             sSiglaRecurso, _
                                             sDescricaoRecurso, _
                                             iAmbienteId, _
                                             dDias)
        
    Set oFinanceiro = Nothing
        
    If Not rs.EOF Then
        dPercRestituicao = Val(rs("perc_restituicao"))
    End If
    
    
    cValRestituicao = cValPago - ((cPremioTotal * (dPercRestituicao / 100)))
    
    cValRestituicao = TruncaDecimal(cValRestituicao)
    
    CalcularRestituicaoCurtoPrazo = cValRestituicao
    
    Set rs = Nothing
        
    Exit Function

TrataErro:
    Call Err.Raise(Err.Number, , "SEGL0143.Local.CalcularRestituicaoCurtoPrazo - " & Err.Description)

End Function

'RMesquita
Function TruncaDecimal(ByVal valor As Double, Optional ByVal posicao As Integer = 2) As Double
'
' Respons�vel : Simonne Almeida
' Dt Desenv : 21/01/2000
'
' Recebe um valor como par�metro, quantidade de casas decimais
' Retorna :
' valor truncado na casa decimal informada
'
' Respons�vel: Jo�o Mac-Cormick
' Altera��o  : 15/11/2000
'
' Permite n�o passar a posi��o, assumindo duas casas decimais
'
Dim SinalDecimal As String, pos, ConfiguracaoBrasil
SinalDecimal = LeArquivoIni("WIN.INI", "intl", "sDecimal")

If SinalDecimal = "." Then
   ConfiguracaoBrasil = False
Else
   ConfiguracaoBrasil = True
End If

If ConfiguracaoBrasil Then
   pos = InStr(valor, ",")
Else
   pos = InStr(valor, ".")
End If
         
If pos <> 0 Then
    valor = Mid(valor, 1, pos + posicao)
End If

TruncaDecimal = valor

End Function

'RMesquita
Function LeArquivoIni(ByVal FileName As String, ByVal SectionName As String, ByVal Item As String) As String
    
     Dim Retorno As String * 100, RetornoDefault, nc
     
     RetornoDefault = "*"
     nc = GetPrivateProfileString(SectionName, Item, RetornoDefault, Retorno, Len(Retorno), FileName)
     LeArquivoIni = Left$(Retorno, nc)

End Function

'RMesquita
Public Function CalcularPercSubvencaoFederal(ByVal cValSubvencaoInformado As Currency, _
                                             ByVal cValPremioBruto As Currency) As Double


Dim dPercSubvencaoFederal As Double

On Error GoTo TrataErro

    If cValPremioBruto = 0 Then
        cValSubvencaoInformado = 0
        CalcularPercSubvencaoFederal = cValSubvencaoInformado
        Exit Function
    End If
                
    dPercSubvencaoFederal = ((cValSubvencaoInformado / cValPremioBruto) * 100)
    
    If dPercSubvencaoFederal < 0 Then
        dPercSubvencaoFederal = 0
    End If
    
    CalcularPercSubvencaoFederal = dPercSubvencaoFederal
    
    Exit Function

TrataErro:
    Call Err.Raise(Err.Number, , "SEGL0143.Local.CalcularPercSubvencaoFederal - " & Err.Description)

End Function

'RMesquita
Public Function ObterPremioPagoCredProt(lPropostaId As Long, _
                                iAmbienteId As Integer, sSiglaRecurso As String, _
                                sDescricaoRecurso As String, lConexao As Long) As Recordset

Dim sSQL       As String

On Error GoTo TrataErro

  
    sSQL = "SET NOCOUNT ON EXEC SEGS9481_SPS " & lPropostaId
  
  
   Set ObterPremioPagoCredProt = New Recordset
   
   Set ObterPremioPagoCredProt = Conexao_ExecutarSQL(sSiglaSistema, _
                               iAmbienteId, _
                               sSiglaRecurso, _
                               sDescricaoRecurso, _
                               sSQL, _
                               lConexao, _
                               True, _
                               False, _
                               30000, _
                               adLockOptimistic, _
                               adUseClient)


Exit Function

TrataErro:
   Call Err.Raise(Err.Number, , "SEGL0143.Local.ObterPremioPagoCredProt - " & Err.Description)

End Function

'RMesquita
Public Sub IncluirAvaliacaoRestituicaoCredProt(ByVal sSiglaSistema As String, _
                                       ByVal sSiglaRecurso As String, _
                                       ByVal sDescricaoRecurso As String, _
                                       ByVal lAmbienteID As Long, _
                                       ByVal sUsuario As String, _
                                       ByVal lPropostaId As Long, ByVal cValRestituicao As Currency, ByVal cValRemuneracao As Currency, _
                                       ByVal cValComissao As Currency, ByVal cValIOF As Currency, _
                                       ByVal cValAdicFracionamento As Currency, ByVal cValDescontoComercial As Currency, _
                                       ByVal cValPremioTarifa As Currency, ByVal cValCustoApolice As Currency, _
                                       ByVal cValComissaoEstipulante As Currency, ByVal dValCambio As Double, _
                                       ByVal sDtPedidoAvaliacao As String, ByVal sDescricao As String, _
                                       ByRef colCorretagem As Collection, ByVal lConexao As Long, _
                                       Optional ByVal lEndossoId As Long = 0, _
                                       Optional ByVal iTpEndossoId As Integer = 0, _
                                       Optional ByVal cValRestituicaoBB As Currency = 0, _
                                       Optional ByVal cValIOFBB As Currency = 0, _
                                       Optional ByVal cValComissaoBB As Currency = 0, _
                                       Optional ByVal cValComissaoEstipulanteBB As Currency = 0, _
                                       Optional ByVal cValRepasseBB As Currency = 0, _
                                       Optional ByVal cValCustoApoliceBB As Currency = 0, _
                                       Optional ByVal cValDescontoComercialBB As Currency = 0, _
                                       Optional ByVal cValAdicFracionamentoBB As Currency = 0, _
                                       Optional ByVal sRestituicaoIntegral As String = "N", _
                                       Optional ByVal lEndossoBB As Long = 0, Optional ByVal bPossuiSubvencao As Boolean = False, _
                                       Optional ByVal cValSubvencaoFederal As Currency = 0, Optional ByVal cValSubvencaoEstadual As Currency = 0)
                                       
'## M�todo de inclus�o em avaliacao_restituicao_tb

Dim sSQL As String
Dim rs As Recordset
Dim rs2 As Recordset
Dim lNumAvaliacao As Long

On Error GoTo TrataErro

    
    ' Incluindo em avaliacao_restituicao_tb ''''''''''''''''''''''''

    sSQL = ""
    sSQL = sSQL & "SET NOCOUNT ON EXEC SEGS9498_SPI "
    sSQL = sSQL & "  " & lPropostaId                                   'proposta_id
    sSQL = sSQL & ",'" & sUsuario & "'"                                'usuario
    
    'abosco @ 2004 mar 31
    If lEndossoBB = 0 Then
      sSQL = sSQL & ",NULL"                                            'num_endosso_bb
    Else
      sSQL = sSQL & "," & lEndossoBB                                   'num_endosso_bb
    End If
    
    'vmuniz em 24/03/2004
    sSQL = sSQL & ",'" & sRestituicaoIntegral & "'"                    'restituicao_integral
    
    sSQL = sSQL & ",NULL"                                              'dt_avaliacao_tecnica smalldatetime
    sSQL = sSQL & ",NULL"                                              'usuario_avaliacao_tecnica
    sSQL = sSQL & ",NULL"                                              'dt_avaliacao_gerencial
    sSQL = sSQL & ",NULL"                                              'usuario_avaliacao_gerencial
    sSQL = sSQL & ",NULL"                                              'motivo_recusa_gerencial
    sSQL = sSQL & ",'P'"                                               'situacao
    sSQL = sSQL & ",NULL"                                              'tp_avaliacao_id
    sSQL = sSQL & ",NULL"                                              'val_restituicao
    sSQL = sSQL & ",NULL"                                              'val_comissao
    sSQL = sSQL & ",NULL"                                              'val_iof
    sSQL = sSQL & ",NULL"                                              'val_adic_fracionamento
    sSQL = sSQL & ",NULL"                                              'val_desconto_comercial
    sSQL = sSQL & ",NULL"                                              'val_premio_tarifa
    sSQL = sSQL & ",NULL"                                              'custo_apolice
    sSQL = sSQL & ", " & MudaVirgulaParaPonto(cValRestituicao)         'val_restituicao_estimado
    sSQL = sSQL & ",NULL"                                              'val_remuneracao
    sSQL = sSQL & ", " & MudaVirgulaParaPonto(cValRemuneracao)         'val_remuneracao_estimado
    sSQL = sSQL & ",NULL"                                              'val_comissao_estipulante
    sSQL = sSQL & ", " & MudaVirgulaParaPonto(cValComissao)            'val_comissao_estimado
    sSQL = sSQL & ", " & MudaVirgulaParaPonto(cValIOF)                 'val_iof_estimado
    sSQL = sSQL & ", " & MudaVirgulaParaPonto(cValAdicFracionamento)   'val_adic_fracionamento_estimado
    sSQL = sSQL & ", " & MudaVirgulaParaPonto(cValDescontoComercial)   'val_desconto_comercial_estimado
    sSQL = sSQL & ", " & MudaVirgulaParaPonto(cValPremioTarifa)        'val_premio_tarifa_estimado
    sSQL = sSQL & ", " & MudaVirgulaParaPonto(cValCustoApolice)        'custo_apolice_estimado
    sSQL = sSQL & ", " & MudaVirgulaParaPonto(cValComissaoEstipulante) 'val_comissao_estipulante_estimado
    
    If Val(dValCambio) = 0 Then                                        'val_cambio
        sSQL = sSQL & ",NULL"
    Else
        sSQL = sSQL & ", " & MudaVirgulaParaPonto(dValCambio)
    End If
    
    sSQL = sSQL & ",'" & Format(sDtPedidoAvaliacao, "yyyymmdd") & "'"  'dt_pedido_endosso
    sSQL = sSQL & ",'" & sDescricao & "'"                              'descricao
    
    If lEndossoId <> 0 Then
        sSQL = sSQL & "," & lEndossoId                                 'endosso_id
    Else
        sSQL = sSQL & ",NULL"
    End If

    If iTpEndossoId <> 0 Then
        sSQL = sSQL & "," & iTpEndossoId                               'tp_endosso_id
    Else
        sSQL = sSQL & ",NULL"
    End If
    
    
    ' bcarneiro - 05/02/2004 - COBAN - Correspondente Banc�rio
    If cValRestituicaoBB > 0 Then
        sSQL = sSQL & ", " & MudaVirgulaParaPonto(cValRestituicaoBB)           'val_restituicao_bb
        sSQL = sSQL & ", " & MudaVirgulaParaPonto(cValIOFBB)                   'val_iof_bb
        sSQL = sSQL & ", " & MudaVirgulaParaPonto(cValComissaoBB)              'val_comissao_bb
        sSQL = sSQL & ", " & MudaVirgulaParaPonto(cValComissaoEstipulanteBB)   'val_comissao_estipulante
        sSQL = sSQL & ", " & MudaVirgulaParaPonto(cValRepasseBB)               'val_repasse_bb
        sSQL = sSQL & ", " & MudaVirgulaParaPonto(cValCustoApoliceBB)          'custo_apolice_bb
        sSQL = sSQL & ", " & MudaVirgulaParaPonto(cValDescontoComercialBB)     'val_desconto_comercial
        sSQL = sSQL & ", " & MudaVirgulaParaPonto(cValAdicFracionamentoBB)     'val_adic_fracionamento
    Else
        sSQL = sSQL & ", NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL "
    End If

    'Rmarins - 18/10/06 - Valores com subven��o (Agr�cola)
    If bPossuiSubvencao = True Then
        sSQL = sSQL & ", NULL "
        sSQL = sSQL & ", NULL "
        sSQL = sSQL & ", " & MudaVirgulaParaPonto(cValSubvencaoFederal)        'val_subvencao_federal
        sSQL = sSQL & ", " & MudaVirgulaParaPonto(cValSubvencaoEstadual)       'val_subvencao_estadual
    End If

    Set rs = Conexao_ExecutarSQL(sSiglaSistema, _
                                 lAmbienteID, _
                                 sSiglaRecurso, _
                                 sDescricaoRecurso, _
                                 sSQL, _
                                 lConexao, _
                                 True)

    If Not rs.EOF Then
        lNumAvaliacao = Val(rs(0))
    End If

    Set rs = Nothing

    ' Incluindo em avaliacao_restituicao_corretagem_tb ''''''''''''''''

    Call IncluirAvaliacaoRestituicaoCorretagem(sUsuario, _
                                               sSiglaSistema, _
                                               sSiglaRecurso, _
                                               sDescricaoRecurso, _
                                               lAmbienteID, _
                                               lPropostaId, _
                                               lNumAvaliacao, _
                                               colCorretagem, _
                                               lConexao)


    
'daniel.menandro - Nova Consultoria - 27/08/2013 - Demanda 16351367 - Extrato de Restitui��o
' Inicio

  sSQL = ""
    sSQL = sSQL & " SELECT endosso_tb.tp_endosso_id " & vbNewLine
    sSQL = sSQL & " FROM endosso_tb WITH (NOLOCK) " & vbNewLine 'HOLIVEIRA: (NOLOCK) para tabela em transa��o DT: 05/02/2007
    sSQL = sSQL & " JOIN tp_endosso_tb WITH (NOLOCK) " & vbNewLine 'HOLIVEIRA: (NOLOCK) para tabela em transa��o DT: 05/02/2007
    sSQL = sSQL & "   ON endosso_tb.tp_endosso_id = tp_endosso_tb.tp_endosso_id "
    sSQL = sSQL & " WHERE endosso_tb.proposta_id = " & lPropostaId & vbNewLine
    sSQL = sSQL & "   AND endosso_tb.endosso_id = " & lEndossoId & vbNewLine
                    
    Set rs2 = Conexao_ExecutarSQL(sSiglaSistema, iAmbienteId, sSiglaRecurso, sDescricaoRecurso, sSQL, lConexao, True, False, 30000, adLockOptimistic, adUseClient)

    Call InserirExtratoRestituicao(sSiglaSistema, _
                                               sSiglaRecurso, _
                                               sDescricaoRecurso, _
                                               lAmbienteID, _
                                               "Sistema Autom�tico", _
                                               lPropostaId, _
                                               cValRestituicao, _
                                               lEndossoId, _
                                               rs2("tp_endosso_id"), _
                                               "Cria��o do Endosso: ", _
                                               "Aguardando avalia��o t�cnica de restitui��o ", _
                                               lConexao)
'Fim
    Exit Sub

TrataErro:
    Call Err.Raise(Err.Number, , "SEGL0143.Local.IncluirAvaliacaoRestituicaoCredProt - " & sSQL & " - " & Err.Description)

End Sub

'RMesquita
Private Sub IncluirAvaliacaoRestituicaoCorretagem(ByVal sUsuario As String, _
                                                  ByVal sSiglaSistema As String, _
                                                  ByVal sSiglaRecurso As String, _
                                                  ByVal sDescricaoRecurso As String, _
                                                  ByVal lAmbienteID As Long, _
                                                  ByVal lPropostaId As Long, _
                                                  ByVal lNumAvaliacao As Long, _
                                                  ByRef colCorretagem As Collection, _
                                                  ByVal lConexao As Long)

Dim oCorretagem As Object
Dim sSQL As String

On Error GoTo TrataErro
  
    ' Inserindo em avaliacao_restituicao_corretagem_tb ''''''''''''''''''''''''''''''''''''''''''''
    
    For Each oCorretagem In colCorretagem
    
        sSQL = ""
        sSQL = "SET NOCOUNT ON EXEC avaliacao_restituicao_corretagem_spi"
        sSQL = sSQL & "  " & lPropostaId
        sSQL = sSQL & ", " & lNumAvaliacao
        sSQL = sSQL & ", " & oCorretagem.Corretor_Id
        sSQL = sSQL & ",'" & oCorretagem.Sucursal & "'"
        sSQL = sSQL & ", " & MudaVirgulaParaPonto(oCorretagem.PercComissao)
        sSQL = sSQL & ", " & oCorretagem.Tipo
        sSQL = sSQL & ",'" & sUsuario & "'"

        Call Conexao_ExecutarSQL(sSiglaSistema, _
                                 lAmbienteID, _
                                 sSiglaRecurso, _
                                 sDescricaoRecurso, _
                                 sSQL, _
                                 lConexao, _
                                 False)
      
    Next
  
    Exit Sub

TrataErro:
    Call Err.Raise(Err.Number, , "SEGL0143.Local.IncluirAvaliacaoRestituicaoCorretagem - " & sSQL & " - " & Err.Description)

End Sub

'daniel.menandro - Nova Consultoria - 27/08/2013 - Demanda 16351367 - Extrato de Restitui��o
' Inicio
Public Sub InserirExtratoRestituicao(ByVal sSiglaSistema As String, _
                                       ByVal sSiglaRecurso As String, _
                                       ByVal sDescricaoRecurso As String, _
                                       ByVal lAmbienteID As Long, _
                                       ByVal sUsuario As String, _
                                       ByVal lPropostaId As Long, _
                                       ByVal cValRestituicao As Currency, _
                                       ByVal lEndossoId As Long, _
                                       ByVal lTpEndossoId As Long, _
                                       ByVal sEvento As String, _
                                       ByVal sSituacao As String, _
                                       ByVal lConexao As Long)

Dim sSQL As String
Dim rs As Recordset


On Error GoTo TrataErro

  
    ' Incluindo em extrato_restituicao_tb ''''''''''''''''''''''''

    sSQL = ""
    sSQL = sSQL & "SET NOCOUNT ON EXEC SEGS11299_SPI"
    sSQL = sSQL & " '" & sSiglaRecurso & "'"
    sSQL = sSQL & ", " & lPropostaId
    sSQL = sSQL & "," & lEndossoId
    sSQL = sSQL & ", " & MudaVirgulaParaPonto(cValRestituicao)
    sSQL = sSQL & ",'" & sUsuario & "'"
    If lTpEndossoId <> 0 Then
    sSQL = sSQL & ",'" & sEvento & lTpEndossoId & "'"
    Else
    sSQL = sSQL & ",'" & sEvento & "'"
    End If
    sSQL = sSQL & ",'" & sSituacao & "'"
    
      Call Conexao_ExecutarSQL(sSiglaSistema, _
                                 lAmbienteID, _
                                 sSiglaRecurso, _
                                 sDescricaoRecurso, _
                                 sSQL, _
                                 lConexao, _
                                 False)
     
     Exit Sub

TrataErro:

    Call Err.Raise(Err.Number, , "SEGL0143.Local.InserirExtratoRestituicao - " & sSQL & " - " & Err.Description)

End Sub
'Fim
Sub GravaLogCTM(ByVal sSts As String, ByVal sMsg As String, ByVal sDesc As String, ByVal sVal As String, Optional pStrArqLog As String = "")
        
    On Error Resume Next
    
    If Dir(pStrArqLog) = "" Then
        Open pStrArqLog For Output As #100
        
        Print #100, sSts & " - " & sMsg & " - " & sDesc & " - " & sVal
        Close #100
    Else
        Open pStrArqLog For Append As #100
        
        Print #100, sSts & " - " & sMsg & " - " & sDesc & " - " & sVal
        Close #100
    End If
 End Sub

