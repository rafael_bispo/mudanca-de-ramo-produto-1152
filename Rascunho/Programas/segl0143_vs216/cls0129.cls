VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cls0129"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'Propriedades de conex�o para executar em transa��o ''''''''''''''''''''''''''''''''''''''''''

Public Property Get AbrirConexao(ByVal eSistema As String, _
                                 ByVal eAmbiente As Integer, _
                                 ByVal eSigla_Recurso As String, _
                                 ByVal eDescricaoArquivo As String) As Long
    
    AbrirConexao = BancodeDados.AbrirConexao(eSistema, eAmbiente, eSigla_Recurso, eDescricaoArquivo)
    
End Property

Public Function FecharConexao(ByVal iCont As Long) As Boolean

    FecharConexao = BancodeDados.FecharConexao(iCont)
    
End Function

Public Function AbrirTransacao(ByVal iCont As Long) As Boolean

    AbrirTransacao = BancodeDados.AbrirTransacao(iCont)
    
End Function

Public Function ConfirmarTransacao(ByVal iCont As Long) As Boolean

    ConfirmarTransacao = BancodeDados.ConfirmarTransacao(iCont)
    
End Function

Public Function RetornarTransacao(ByVal iCont As Long) As Boolean

    RetornarTransacao = BancodeDados.RetornarTransacao(iCont)
    
End Function
'Fim das propriedades de conex�o para execu��o em transa��o

Public Function ConsultarAgendamentoCancelado(ByVal sSiglaSistema As String, _
                                              ByVal sSiglaRecurso As String, _
                                              ByVal sDescricaoRecurso As String, _
                                              ByVal lAmbienteId As Long, _
                                              ByVal sUsuario As String, _
                                              Optional ByVal iProdutoId As Integer = 0, _
                                              Optional ByVal lPropostaBB As Long = 0) As Recordset

Dim sSQL As String
    
On Error GoTo Trata_Erro
    
    sSQL = ""
    sSQL = "EXEC SEGS5929_SPS "
    adSQL sSQL, "'" & sUsuario & "'"
    
    If iProdutoId <> 0 Then
        adSQL sSQL, ", " & iProdutoId
    Else
        adSQL sSQL, ", NULL"
    End If
    
    If lPropostaBB <> 0 Then
        adSQL sSQL, ", " & lPropostaBB
    End If

    'sSQL = "SELECT * from vanessa_teste_baixa"
    
    Set ConsultarAgendamentoCancelado = ExecutarSQL(sSiglaSistema, _
                                                    lAmbienteId, _
                                                    sSiglaRecurso, _
                                                    sDescricaoRecurso, _
                                                    sSQL, _
                                                    True)

    Exit Function
    
Trata_Erro:
      Call Err.Raise(Err.Number, , "SEGL0143.cls00129.ConsultarAgendamentoCancelado" & Err.Description)
      
End Function

Public Sub ObterPropostaBBProduto(ByVal sSiglaSistema As String, _
                                  ByVal sSiglaRecurso As String, _
                                  ByVal sDescricaoRecurso As String, _
                                  ByVal lAmbienteId As Long, _
                                  ByVal sUsuario As String, _
                                  Optional ByVal lPropostaId As Long = 0, _
                                  Optional ByVal lApoliceId As Long = 0, _
                                  Optional ByVal iRamoId As Integer = 0, _
                                  Optional ByVal lSucursal As Long = 0, _
                                  Optional ByVal lSeguradoraCodSusep As Long = 0, _
                                  Optional ByRef iProdutoId As Integer = 0, _
                                  Optional ByRef lPropostaBB As Long = 0)
Dim sSQL As String
Dim rsAuxiliar As Recordset

On Error GoTo Trata_Erro

If (lPropostaId <> 0) Or (lApoliceId <> 0 And iRamoId <> 0) Then

    sSQL = ""
    adSQL sSQL, "SELECT proposta_fechada_tb.proposta_bb,"
    adSQL sSQL, "       proposta_tb.produto_id"
    adSQL sSQL, "FROM proposta_fechada_tb"
    adSQL sSQL, "JOIN apolice_tb"
    adSQL sSQL, "  ON apolice_tb.proposta_id = proposta_fechada_tb.proposta_id"
    adSQL sSQL, "JOIN proposta_tb"
    adSQL sSQL, "  ON proposta_tb.proposta_id = proposta_fechada_tb.proposta_id"
    
    If lPropostaId <> 0 Then
        adSQL sSQL, "WHERE proposta_fechada_tb.proposta_id = " & lPropostaId
    Else
        adSQL sSQL, "WHERE apolice_tb.apolice_id = " & lApoliceId
        adSQL sSQL, "  AND apolice_tb.ramo_id = " & iRamoId
        adSQL sSQL, "  AND apolice_tb.sucursal_seguradora_id = " & lSucursal
        adSQL sSQL, "  AND apolice_tb.seguradora_cod_susep = " & lSeguradoraCodSusep
    End If
    
    adSQL sSQL, "UNION"
    
    adSQL sSQL, "SELECT proposta_adesao_tb.proposta_bb,"
    adSQL sSQL, "       proposta_tb.produto_id"
    adSQL sSQL, "FROM proposta_adesao_tb"
    adSQL sSQL, "JOIN apolice_tb"
    adSQL sSQL, "  ON apolice_tb.apolice_id = proposta_adesao_tb.apolice_id"
    adSQL sSQL, " AND apolice_tb.ramo_id = proposta_adesao_tb.ramo_id"
    adSQL sSQL, " AND apolice_tb.sucursal_seguradora_id = proposta_adesao_tb.sucursal_seguradora_id"
    adSQL sSQL, " AND apolice_tb.seguradora_cod_susep = proposta_adesao_tb.seguradora_cod_susep"
    adSQL sSQL, "JOIN proposta_tb"
    adSQL sSQL, "  ON proposta_tb.proposta_id = proposta_adesao_tb.proposta_id"

    If lPropostaId <> 0 Then
        adSQL sSQL, "WHERE proposta_adesao_tb.proposta_id = " & lPropostaId
    Else
        adSQL sSQL, "WHERE apolice_tb.apolice_id = " & lApoliceId
        adSQL sSQL, "  AND apolice_tb.ramo_id = " & iRamoId
        adSQL sSQL, "  AND apolice_tb.sucursal_seguradora_id = " & lSucursal
        adSQL sSQL, "  AND apolice_tb.seguradora_cod_susep = " & lSeguradoraCodSusep
    End If

    Set rsAuxiliar = ExecutarSQL(sSiglaSistema, _
                                 lAmbienteId, _
                                 sSiglaRecurso, _
                                 sDescricaoRecurso, _
                                 sSQL, _
                                 True)
    
    If Not rsAuxiliar.EOF Then
        lPropostaBB = rsAuxiliar("proposta_bb")
        iProdutoId = rsAuxiliar("produto_id")
    End If
    
    rsAuxiliar.Close
    Set rsAuxiliar = Nothing

End If
        
    Exit Sub
    
Trata_Erro:
      Call Err.Raise(Err.Number, , "SEGL0143.cls00129.ObterPropostaBBProduto" & Err.Description)

End Sub

Public Sub ReativarAgendamento(ByVal sSiglaSistema As String, _
                               ByVal sSiglaRecurso As String, _
                               ByVal sDescricaoRecurso As String, _
                               ByVal lAmbienteId As Long, _
                               ByVal Documento As Collection, _
                               ByVal sDtSistema As String, _
                               ByVal sUsuario As String)

Dim sSQL As String
Dim Documentos As Object
Dim lConexao As Long
Dim rsDocumento As Recordset

On Error GoTo Trata_Erro

    'Obtendo conex�o ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    lConexao = AbrirConexao(sSiglaSistema, lAmbienteId, sSiglaRecurso, sDescricaoRecurso)
    
    'Iniciando a transa��o ''''''''''''''''''''''''''''''''''''''''''''''''''''
    If Not AbrirTransacao(lConexao) Then
      Call Err.Raise(vbObjectError + 1000, , "RSGL0002.cls00158.InserirResseguroAnalisado - N�o foi poss�vel abrir transa��o")
    End If
             
    'Populando tabela tempor�ria com os documentos a serem reativados '''''''''''''''''''''''''
    For Each Documentos In Documento

        sSQL = ""
        adSQL sSQL, "SELECT 1"
        adSQL sSQL, "FROM documento_cancelar_tb (NOLOCK)"
        adSQL sSQL, "WHERE proposta_id = " & Documentos.lPropostaId
        adSQL sSQL, "  AND endosso_id = ISNULL(" & Documentos.iEndossoQueCancelou & ",0)"
        adSQL sSQL, "  AND tp_endosso_id = 101"
        
        Set rsDocumento = Conexao_ExecutarSQL(sSiglaSistema, _
                                              lAmbienteId, _
                                              sSiglaRecurso, _
                                              sDescricaoRecurso, _
                                              sSQL, _
                                              lConexao, _
                                              True)
        If rsDocumento.EOF Then
        
            sSQL = ""
            adSQL sSQL, "EXEC SEGS5940_SPI "
            adSQL sSQL, Documentos.lPropostaId & ","
            adSQL sSQL, Documentos.iEndossoQueCancelou & ","
            adSQL sSQL, Documentos.lPropostaBB & ","
            adSQL sSQL, "'" & Format(sDtSistema, "yyyymmdd") & "',"
            adSQL sSQL, "4,"
            adSQL sSQL, "'P',"
            adSQL sSQL, "101,"
            adSQL sSQL, "'" & sUsuario & "'"

            Call Conexao_ExecutarSQL(sSiglaSistema, _
                                     lAmbienteId, _
                                     sSiglaRecurso, _
                                     sDescricaoRecurso, _
                                     sSQL, _
                                     lConexao, _
                                     False)
        End If
            
        Set rsDocumento = Nothing
                       
    Next
                                                                        
    'Descancelando os documentos '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    sSQL = ""
    adSQL sSQL, "EXEC SEGS5930_SPI"
    adSQL sSQL, "     101,"
    adSQL sSQL, "     '" & sUsuario & "'"
                               
    Call Conexao_ExecutarSQL(sSiglaSistema, _
                             lAmbienteId, _
                             sSiglaRecurso, _
                             sDescricaoRecurso, _
                             sSQL, _
                             lConexao, _
                             False)
                                                                                               
    'Finalizando transa��o '''''''''''''''''''''''''''''''''''''''''''''''''''''''''
      
    If Not ConfirmarTransacao(lConexao) Then GoTo Trata_Erro
    If Not FecharConexao(lConexao) Then GoTo Trata_Erro
            
    Exit Sub
    
Trata_Erro:
      Call Err.Raise(Err.Number, , "SEGL0143.cls00129.ReativarAgendamento" & Err.Description)

End Sub

Public Function ObterDadosReativacaoDocumentosPendentes(ByVal sSiglaSistema As String, _
                                                        ByVal sSiglaRecurso As String, _
                                                        ByVal sDescricaoRecurso As String, _
                                                        ByVal iAmbienteId As Integer, _
                                                        ByVal lPropostaId As Long) As Object


Dim sSQL As String

sSQL = ""
sSQL = sSQL & " SELECT proposta_tb.proposta_id, " & vbNewLine
sSQL = sSQL & "        val_is = isnull(proposta_adesao_tb.val_is, 0), " & vbNewLine
sSQL = sSQL & "        proposta_adesao_tb.apolice_id, " & vbNewLine
sSQL = sSQL & "        proposta_adesao_tb.ramo_id, " & vbNewLine
sSQL = sSQL & "        val_premio_bruto = isnull(proposta_adesao_tb.val_premio_bruto, 0), " & vbNewLine
sSQL = sSQL & "        situacao = CASE WHEN proposta_tb.situacao = 'a' THEN 'Aceita' " & vbNewLine
sSQL = sSQL & "                        WHEN proposta_tb.situacao = 'c' THEN 'Cancelada' " & vbNewLine
sSQL = sSQL & "                        WHEN proposta_tb.situacao = 'i' THEN 'Ativa' " & vbNewLine
sSQL = sSQL & "                        WHEN proposta_tb.situacao = 'r' THEN 'Recusada' " & vbNewLine
sSQL = sSQL & "                        WHEN proposta_tb.situacao = 'p' THEN 'Pendente' " & vbNewLine
sSQL = sSQL & "                        END, " & vbNewLine
sSQL = sSQL & "        dt_contratacao, " & vbNewLine
sSQL = sSQL & "        inicio_vigencia = proposta_adesao_tb.dt_inicio_vigencia , " & vbNewLine
sSQL = sSQL & "        fim_vigencia = proposta_adesao_tb.dt_fim_vigencia , " & vbNewLine
sSQL = sSQL & "        proposta_adesao_tb.proposta_bb, " & vbNewLine
sSQL = sSQL & "        proposta_adesao_tb.cont_agencia_id, " & vbNewLine
sSQL = sSQL & "        nome_agencia = agencia_tb.nome, " & vbNewLine
sSQL = sSQL & "        produto_tb.produto_id, " & vbNewLine
sSQL = sSQL & "        nome_produto = produto_tb.nome, " & vbNewLine
sSQL = sSQL & "        nome_ramo = ramo_tb.nome " & vbNewLine
sSQL = sSQL & "   FROM produto_tb    " & vbNewLine
sSQL = sSQL & "   JOIN proposta_tb " & vbNewLine
sSQL = sSQL & "     ON produto_tb.produto_id = proposta_tb.produto_id " & vbNewLine
sSQL = sSQL & "   JOIN proposta_adesao_tb " & vbNewLine
sSQL = sSQL & "     ON proposta_adesao_tb.proposta_id = proposta_tb.proposta_id " & vbNewLine
sSQL = sSQL & "   JOIN agencia_tb " & vbNewLine
sSQL = sSQL & "     ON agencia_tb.agencia_id = proposta_adesao_tb.cont_agencia_id " & vbNewLine
sSQL = sSQL & "   JOIN ramo_tb " & vbNewLine
sSQL = sSQL & "     ON ramo_tb.ramo_id = proposta_tb.ramo_id " & vbNewLine
sSQL = sSQL & "  WHERE proposta_tb.proposta_id = " & lPropostaId & vbNewLine

sSQL = sSQL & " UNION "

sSQL = sSQL & " SELECT proposta_tb.proposta_id, " & vbNewLine
sSQL = sSQL & "        val_is = isnull(proposta_fechada_tb.val_is, 0), " & vbNewLine
sSQL = sSQL & "        apolice_tb.apolice_id, " & vbNewLine
sSQL = sSQL & "        apolice_tb.ramo_id, " & vbNewLine
sSQL = sSQL & "        val_premio_bruto = isnull(proposta_fechada_tb.val_premio_bruto, 0), " & vbNewLine
sSQL = sSQL & "        situacao = CASE WHEN proposta_tb.situacao = 'a' THEN 'Aceita' " & vbNewLine
sSQL = sSQL & "                        WHEN proposta_tb.situacao = 'c' THEN 'Cancelada' " & vbNewLine
sSQL = sSQL & "                        WHEN proposta_tb.situacao = 'i' THEN 'Ativa' " & vbNewLine
sSQL = sSQL & "                        WHEN proposta_tb.situacao = 'r' THEN 'Recusada' " & vbNewLine
sSQL = sSQL & "                        WHEN proposta_tb.situacao = 'p' THEN 'Pendente' " & vbNewLine
sSQL = sSQL & "                        END, " & vbNewLine
sSQL = sSQL & "        dt_contratacao, " & vbNewLine
sSQL = sSQL & "        inicio_vigencia = apolice_tb.dt_inicio_vigencia , " & vbNewLine
sSQL = sSQL & "        fim_vigencia = apolice_tb.dt_fim_vigencia, " & vbNewLine
sSQL = sSQL & "        proposta_fechada_tb.proposta_bb, " & vbNewLine
sSQL = sSQL & "        proposta_fechada_tb.cont_agencia_id, " & vbNewLine
sSQL = sSQL & "        nome_agencia = agencia_tb.nome, " & vbNewLine
sSQL = sSQL & "        produto_tb.produto_id, " & vbNewLine
sSQL = sSQL & "        nome_produto = produto_tb.nome, " & vbNewLine
sSQL = sSQL & "        nome_ramo = ramo_tb.nome " & vbNewLine
sSQL = sSQL & "   FROM produto_tb  " & vbNewLine
sSQL = sSQL & "   JOIN proposta_tb " & vbNewLine
sSQL = sSQL & "     ON produto_tb.produto_id = proposta_tb.produto_id  " & vbNewLine
sSQL = sSQL & "   JOIN proposta_fechada_tb " & vbNewLine
sSQL = sSQL & "     ON proposta_fechada_tb.proposta_id = proposta_tb.proposta_id " & vbNewLine
sSQL = sSQL & "   JOIN apolice_tb " & vbNewLine
sSQL = sSQL & "     ON apolice_tb.proposta_id = proposta_fechada_tb.proposta_id " & vbNewLine
sSQL = sSQL & "   JOIN agencia_tb " & vbNewLine
sSQL = sSQL & "     ON agencia_tb.agencia_id = proposta_fechada_tb.cont_agencia_id " & vbNewLine
sSQL = sSQL & "   JOIN ramo_tb " & vbNewLine
sSQL = sSQL & "     ON ramo_tb.ramo_id = proposta_tb.ramo_id " & vbNewLine
sSQL = sSQL & "  WHERE proposta_tb.proposta_id = " & lPropostaId & vbNewLine


Set ObterDadosReativacaoDocumentosPendentes = ExecutarSQL(sSiglaSistema, _
                                                          iAmbienteId, _
                                                          sSiglaRecurso, _
                                                          sDescricaoRecurso, _
                                                          sSQL, _
                                                          True)

End Function
Public Function ObterDadosReativacaoCobranca(ByVal sSiglaSistema As String, _
                                             ByVal sSiglaRecurso As String, _
                                             ByVal sDescricaoRecurso As String, _
                                             ByVal iAmbienteId As Integer, _
                                             ByVal lPropostaId As Long, _
                                             ByVal iEndosso As Integer) As Object

Dim sSQL As String

sSQL = ""
sSQL = sSQL & " SELECT num_cobranca, " & vbNewLine
sSQL = sSQL & "        num_endosso, " & vbNewLine
sSQL = sSQL & "        situacao = CASE WHEN (situacao = 'p' AND canc_endosso_id is null)" & vbNewLine
sSQL = sSQL & "                           THEN 'Pendente' " & vbNewLine
sSQL = sSQL & "                        WHEN (situacao = 'a' AND canc_endosso_id is null)" & vbNewLine
sSQL = sSQL & "                           THEN 'Baixada' " & vbNewLine
sSQL = sSQL & "                        WHEN canc_endosso_id is not null THEN 'Cancelada' " & vbNewLine
sSQL = sSQL & "                        END, " & vbNewLine
sSQL = sSQL & "        val_cobranca, " & vbNewLine
sSQL = sSQL & "        val_comissao " & vbNewLine
sSQL = sSQL & "   FROM agendamento_cobranca_atual_tb    " & vbNewLine
sSQL = sSQL & "  WHERE proposta_id = " & lPropostaId & vbNewLine
sSQL = sSQL & "    AND num_endosso = " & iEndosso & vbNewLine

Set ObterDadosReativacaoCobranca = ExecutarSQL(sSiglaSistema, _
                                               iAmbienteId, _
                                               sSiglaRecurso, _
                                               sDescricaoRecurso, _
                                               sSQL, _
                                               True)

End Function

Public Function ObterDadosReativacaoFatura(ByVal sSiglaSistema As String, _
                                           ByVal sSiglaRecurso As String, _
                                           ByVal sDescricaoRecurso As String, _
                                           ByVal iAmbienteId As Integer, _
                                           ByVal lFaturaId As Long, _
                                           ByVal lApoliceId As Long, _
                                           ByVal iRamo As Integer) As Object

Dim sSQL As String

sSQL = ""
sSQL = sSQL & " SELECT fatura_tb.fatura_id, " & vbNewLine
sSQL = sSQL & "        fatura_tb.endosso_id, " & vbNewLine
sSQL = sSQL & "        situacao = CASE WHEN fatura_tb.situacao = 'c' THEN 'Cancelada' " & vbNewLine
sSQL = sSQL & "                        WHEN situacao = 'b' THEN 'Baixada' " & vbNewLine
sSQL = sSQL & "                        WHEN situacao = 't' THEN 'Pendente' " & vbNewLine
sSQL = sSQL & "                        END, " & vbNewLine
sSQL = sSQL & "        fatura_tb.val_bruto, " & vbNewLine
sSQL = sSQL & "        fatura_tb.val_comissao, " & vbNewLine
sSQL = sSQL & "        fatura_tb.apolice_id, " & vbNewLine
sSQL = sSQL & "        fatura_tb.ramo_id, " & vbNewLine
sSQL = sSQL & "        fatura_tb.sucursal_seguradora_id, " & vbNewLine
sSQL = sSQL & "        nome_sucursal = sucursal_seguradora_tb.nome, " & vbNewLine
sSQL = sSQL & "        fatura_tb.seguradora_cod_susep, " & vbNewLine
sSQL = sSQL & "        nome_seguradora = seguradora_tb.nome, " & vbNewLine
sSQL = sSQL & "        nome_ramo = ramo_tb.nome " & vbNewLine
sSQL = sSQL & "   FROM fatura_tb    " & vbNewLine
sSQL = sSQL & "   JOIN sucursal_seguradora_tb " & vbNewLine
sSQL = sSQL & "     ON sucursal_seguradora_tb.sucursal_seguradora_id = fatura_tb.sucursal_seguradora_id " & vbNewLine
sSQL = sSQL & "    AND sucursal_seguradora_tb.seguradora_cod_susep = fatura_tb.seguradora_cod_susep " & vbNewLine
sSQL = sSQL & "   JOIN seguradora_tb " & vbNewLine
sSQL = sSQL & "     ON seguradora_tb.seguradora_cod_susep = fatura_tb.seguradora_cod_susep " & vbNewLine
sSQL = sSQL & "   JOIN ramo_tb " & vbNewLine
sSQL = sSQL & "     ON ramo_tb.ramo_id = fatura_tb.ramo_id " & vbNewLine
sSQL = sSQL & "  WHERE fatura_id = " & lFaturaId & vbNewLine
sSQL = sSQL & "    AND fatura_tb.apolice_id = " & lApoliceId & vbNewLine
sSQL = sSQL & "    AND fatura_tb.ramo_id = " & iRamo & vbNewLine

Set ObterDadosReativacaoFatura = ExecutarSQL(sSiglaSistema, _
                                             iAmbienteId, _
                                             sSiglaRecurso, _
                                             sDescricaoRecurso, _
                                             sSQL, _
                                             True)

End Function




