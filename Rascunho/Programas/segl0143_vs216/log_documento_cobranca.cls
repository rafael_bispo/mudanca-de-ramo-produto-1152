VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "log_documento_cobranca"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
'local variable(s) to hold property value(s)
Private mvarproduto_id As Integer 'local copy
Private mvarqtd_agendamentos As Integer 'local copy
Private mvarqtd_documentos As Integer 'local copy
Private mvardt_execucao As Date 'local copy

Public Property Let dt_execucao(ByVal vData As Date)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.dt_execucao = 5
    mvardt_execucao = vData
End Property


Public Property Get dt_execucao() As Date
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.dt_execucao
    dt_execucao = mvardt_execucao
End Property



Public Property Let qtd_documentos(ByVal vData As Integer)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.qtd_documentos = 5
    mvarqtd_documentos = vData
End Property


Public Property Get qtd_documentos() As Integer
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.qtd_documentos
    qtd_documentos = mvarqtd_documentos
End Property



Public Property Let qtd_agendamentos(ByVal vData As Integer)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.qtd_agendamentos = 5
    mvarqtd_agendamentos = vData
End Property


Public Property Get qtd_agendamentos() As Integer
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.qtd_agendamentos
    qtd_agendamentos = mvarqtd_agendamentos
End Property



Public Property Let produto_id(ByVal vData As Integer)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.produto_id = 5
    mvarproduto_id = vData
End Property


Public Property Get produto_id() As Integer
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.produto_id
    produto_id = mvarproduto_id
End Property
