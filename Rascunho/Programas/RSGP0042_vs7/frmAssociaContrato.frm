VERSION 5.00
Begin VB.Form frmAssociaContrato 
   ClientHeight    =   2700
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   6300
   Icon            =   "frmAssociaContrato.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   2700
   ScaleWidth      =   6300
   StartUpPosition =   3  'Windows Default
   Begin VB.Frame Frame1 
      Caption         =   "Logs"
      Height          =   2055
      Left            =   60
      TabIndex        =   2
      Top             =   60
      Width           =   6135
      Begin VB.Label lblApolices 
         BorderStyle     =   1  'Fixed Single
         Height          =   345
         Left            =   2775
         TabIndex        =   10
         Top             =   1470
         Width           =   2055
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         AutoSize        =   -1  'True
         Caption         =   "Ap�lices Associadas:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   915
         TabIndex        =   9
         Top             =   1530
         Width           =   1815
      End
      Begin VB.Label lblLog1 
         Alignment       =   1  'Right Justify
         AutoSize        =   -1  'True
         Caption         =   "Registros Processados:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   720
         TabIndex        =   8
         Top             =   1140
         Width           =   2010
      End
      Begin VB.Label lblRegistros 
         BorderStyle     =   1  'Fixed Single
         Height          =   345
         Left            =   2775
         TabIndex        =   7
         Top             =   1080
         Width           =   2055
      End
      Begin VB.Label Label2 
         Alignment       =   1  'Right Justify
         AutoSize        =   -1  'True
         Caption         =   "Hora In�cio:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   1695
         TabIndex        =   6
         Top             =   375
         Width           =   1035
      End
      Begin VB.Label Label3 
         Alignment       =   1  'Right Justify
         AutoSize        =   -1  'True
         Caption         =   "Hora Fim:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   1905
         TabIndex        =   5
         Top             =   765
         Width           =   825
      End
      Begin VB.Label lblInicio 
         BorderStyle     =   1  'Fixed Single
         Height          =   345
         Left            =   2775
         TabIndex        =   4
         Top             =   300
         Width           =   2055
      End
      Begin VB.Label lblFim 
         BorderStyle     =   1  'Fixed Single
         Height          =   345
         Left            =   2775
         TabIndex        =   3
         Top             =   690
         Width           =   2055
      End
   End
   Begin VB.CommandButton cmdProcessar 
      Caption         =   "Processar"
      Height          =   375
      Left            =   3060
      TabIndex        =   0
      Top             =   2250
      Width           =   1545
   End
   Begin VB.CommandButton cmdSair 
      Caption         =   "Sair"
      Height          =   375
      Left            =   4680
      TabIndex        =   1
      Top             =   2250
      Width           =   1545
   End
End
Attribute VB_Name = "frmAssociaContrato"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmdProcessar_Click()

Dim oApolices             As Object
Dim rsApolices            As Recordset
Dim lRegistrosProcessados As Long
Dim lApolicesAssociadas   As Long

On Error GoTo Trata_Erro:
      
    ' Tratamento para o Scheduler ''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'    InicializaParametrosExecucaoBatch Me
    
    Screen.MousePointer = vbHourglass
    
    ' Atualizando a interface ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    lblInicio.Caption = Now
    
    ' Associando ap�lices a contratos  '''''''''''''''''''''''''''''''''''''''''''''''''''
    Set oApolices = CreateObject("RSGL0002.cls00127")
    
    Set rsApolices = oApolices.AssociarApoliceContrato("RESSEG", _
                                                       App.Title, _
                                                       App.FileDescription, _
                                                       glAmbiente_id, _
                                                       cUserName)
    
    lApolicesAssociadas = rsApolices(0)
    lRegistrosProcessados = rsApolices(1)
    
    ' Atualizando a interface ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    lblFim.Caption = Now
    lblApolices.Caption = rsApolices(0)
    lblRegistros.Caption = rsApolices(1)
    
    ' Gravando log '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'    Call GravarLog
    
    cmdProcessar.Enabled = False
    
    Screen.MousePointer = vbDefault
    
    Exit Sub

Trata_Erro:
    Call TratarErro("cmdProcessar_Click", Me.name)
    FinalizarAplicacao
    
End Sub

Sub GravarLog()
  
On Error GoTo Trata_Erro:

  ' Logando o n�mero de registros processados - Scheduler '''''''''''''''''''''''''''
    
  Call goProducao.AdicionaLog(1, Val(lblRegistros.Caption))
  Call goProducao.AdicionaLog(2, Val(lblApolices.Caption))
  
    '###########################grava log arquivo CTM
    If CTM = True Then
      Call GravaLogCTM("OK - ", Val(lblRegistros.Caption), Val(lblApolices.Caption), "")
    End If
    '################################################
  
'pula caso executado pelo control-m
  If CTM = False Then
    Call goProducao.finaliza
  End If

  Exit Sub

Trata_Erro:
    Call TratarErro("GravarLog", Me.name)
    Call FinalizarAplicacao
    
End Sub

Private Sub cmdSair_Click()

    Call FinalizarAplicacao

End Sub

Private Sub Form_Load()

On Local Error GoTo Trata_Erro
  
  'Pula se for executado pelo control-m
'If Verifica_Origem_ControlM(Command) = False Then
  'Verificando permiss�o de acesso � aplica��o''''''''''''''''''''''''''''''''''''''''''
  'If Not Trata_Parametros(Command) Then
'    FinalizarAplicacao
 ' End If
'End If
  
  'Retirar antes da libera��o
  cUserName = "99988877714"
  glAmbiente_id = 3
  
  'Obtendo a data operacional ''''''''''''''''''''''''''''''''''''''''''''
  Call ObterDataSistema("SEGBR")

  ' Configurando interface'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  Call CentraFrm(Me)
  
  Me.Caption = App.Title & " - Associa��o de Contratos de Resseguro - " & ExibirAmbiente(glAmbiente_id)
          
  ' Iniciando o processo automaticamente caso seja uma chamada do Scheduler '''''''''''''''

  If Obtem_agenda_diaria_id(Command) > 0 Or Verifica_Origem_ControlM(Command) = True Then
      Me.Show
      Call cmdProcessar_Click
      FinalizarAplicacao
  End If

  Exit Sub
  
Trata_Erro:
    Call TratarErro("Form_Load", Me.name)
    Call FinalizarAplicacao
    
End Sub



