VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.2#0"; "MSCOMCTL.OCX"
Begin VB.Form FrmInadimplente 
   ClientHeight    =   3030
   ClientLeft      =   1605
   ClientTop       =   390
   ClientWidth     =   6945
   Icon            =   "FrmInadimplente.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   3030
   ScaleWidth      =   6945
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame Frame2 
      Height          =   2055
      Left            =   150
      TabIndex        =   3
      Top             =   90
      Width           =   6675
      Begin VB.Label Label5 
         AutoSize        =   -1  'True
         Caption         =   "Baixa de Inadimplencias:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   1020
         TabIndex        =   11
         Top             =   1560
         Visible         =   0   'False
         Width           =   2130
      End
      Begin VB.Label lblBaixas 
         BorderStyle     =   1  'Fixed Single
         Height          =   315
         Left            =   3330
         TabIndex        =   10
         Top             =   1500
         Visible         =   0   'False
         Width           =   2565
      End
      Begin VB.Label lblNovos 
         BorderStyle     =   1  'Fixed Single
         Height          =   315
         Left            =   3330
         TabIndex        =   9
         Top             =   1050
         Width           =   2565
      End
      Begin VB.Label Label4 
         AutoSize        =   -1  'True
         Caption         =   "Novas Inadimplecias:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   1320
         TabIndex        =   8
         Top             =   1110
         Width           =   1830
      End
      Begin VB.Label lblFim 
         BorderStyle     =   1  'Fixed Single
         Height          =   315
         Left            =   3330
         TabIndex        =   7
         Top             =   720
         Width           =   2565
      End
      Begin VB.Label lblInicio 
         BorderStyle     =   1  'Fixed Single
         Height          =   315
         Left            =   3330
         TabIndex        =   6
         Top             =   330
         Width           =   2565
      End
      Begin VB.Label Label3 
         Alignment       =   1  'Right Justify
         AutoSize        =   -1  'True
         Caption         =   "Fim :"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   2730
         TabIndex        =   5
         Top             =   780
         Width           =   420
      End
      Begin VB.Label Label2 
         Alignment       =   1  'Right Justify
         AutoSize        =   -1  'True
         Caption         =   "In�cio :"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   2520
         TabIndex        =   4
         Top             =   360
         Width           =   630
      End
   End
   Begin VB.CommandButton cmdSair 
      Caption         =   "Sair"
      Height          =   375
      Left            =   5220
      TabIndex        =   1
      Top             =   2280
      Width           =   1635
   End
   Begin VB.CommandButton cmdProcessar 
      Caption         =   "Processar"
      Height          =   375
      Left            =   3480
      TabIndex        =   0
      Top             =   2280
      Width           =   1635
   End
   Begin MSComctlLib.StatusBar StbRodape 
      Align           =   2  'Align Bottom
      Height          =   315
      Left            =   0
      TabIndex        =   2
      Top             =   2715
      Width           =   6945
      _ExtentX        =   12250
      _ExtentY        =   556
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   3
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   8997
            MinWidth        =   8997
            Text            =   "Mensagem"
            TextSave        =   "Mensagem"
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   6
            Alignment       =   1
            AutoSize        =   2
            Object.Width           =   1376
            MinWidth        =   88
            Text            =   "Data do sistema"
            TextSave        =   "26/06/20"
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            AutoSize        =   2
            Object.Width           =   1111
            MinWidth        =   88
            Text            =   "Usu�rio"
            TextSave        =   "Usu�rio"
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "FrmInadimplente"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Sub Finaliza_Scheduler()

'## Rotina de finaliza��o de Scheduler

    'goProducao.AdicionaLog 1, IIf(Trim(lblBaixas.Caption) <> "", lblBaixas.Caption, 0)
    goProducao.AdicionaLog 2, IIf(Trim(lblNovos.Caption) <> "", lblNovos.Caption, 0)
    
    'grava log arquivo CTM
    If CTM = True Then
        Call GravaLogCTM("OK - ", IIf(Trim(lblNovos.Caption) <> "", lblNovos.Caption, 0), "", "")
    End If

    'Pula de for executado pelo Control-M
    If CTM = False Then
        Call goProducao.finaliza
    End If
    
End Sub

Sub IdentificarInadimplentes()

'## Rotina de identifica��o de inadimplentes

Dim objCancelamento As Object
Dim oRs As Recordset
Dim lQtdProcessados As Long

On Error GoTo Trata_Erro
    
    StbRodape.Panels(1).Text = "Processando Identifica��o de Inadimplencias..."
    
    DoEvents
    
    Set objCancelamento = CreateObject("SEGL0019.cls00106")
    
    ' Identificando e gravando parcelas Inadimplentes ''''''''''''''''''''''''''''''''
    
    Call objCancelamento.IdentificarParcInadimplente(cUserName, _
                                                     gsSIGLASISTEMA, _
                                                     App.Title, _
                                                     App.FileDescription, _
                                                     glAmbiente_id, _
                                                     Data_Sistema, _
                                                     lQtdProcessados)
    
    ' Atualizando interface ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    
    lblNovos.Caption = lQtdProcessados
    
    Set objCancelamento = Nothing
    
    Exit Sub
 
Trata_Erro:
    
    TratarErro "IdentificarInadimplentes", Me.name
    Call FinalizarAplicacao

End Sub

Private Sub cmdProcessar_Click()

'## Rotina de inicializa��o do processo

On Local Error GoTo Erro
    
    'Incializando parametros de execu��o batch''''''''''''''''''''''''''''
    
    InicializaParametrosExecucaoBatch Me
    
    'Atualizando interface''''''''''''''''''''''''''''''''''''''''''''''''
       
    Screen.MousePointer = vbHourglass
    lblInicio.Caption = Now
    
    cmdProcessar.Enabled = False
        
    DoEvents
    
    'Processando''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    
   ' Call BaixarInadimplentes
    
    Call IdentificarInadimplentes
    
    'Atualizando interface''''''''''''''''''''''''''''''''''''''''''''''''
 
    lblFim.Caption = Now
    
    'Finalizando Scheduler''''''''''''''''''''''''''''''''''''''''''''''''
    

    
'DEMANDA 13856609
'XxXxX
    StbRodape.Panels(1).Text = "Gerando Planilha Excel"
    DoEvents
    
    Gera_planilhaXLS
'XxXxX

    Finaliza_Scheduler
    
    Screen.MousePointer = vbDefault
    
    StbRodape.Panels(1).Text = "Fim de Processamento"
    MensagemBatch "Fim de Processamento", , , False
 
 Exit Sub
 
Erro:
    TratarErro "cmdok_click", Me.name
    Call FinalizarAplicacao

End Sub

Private Sub Gera_planilhaXLS()
Dim rc As Recordset
Dim sSql As String
Dim SCaminho As String
Dim sArquivo As String

Dim Sdestinatario As String
Dim sMensagem As String
Dim STitulo As String

'''Produ��o
SCaminho = "\\SISAB005\APLICACOES\SISBR\Arquivos Parcelas Inadimplentes\Gerados\"
'''Qualidade
'SCaminho = "\\sisab005\users\Temporario\SEGP0742"
'Nome do Arquivo
sArquivo = "Parc_Inadimplente" & Format(Now, "ddMMyyhhmm") & ".xls"

'Realiza a consulta para buscar os dados que ser�o inseridos na planilha

sSql = "SELECT CASE WHEN COBR.produto_id IS NULL THEN 'N�o Informado'" & Chr(13)
sSql = sSql & "            ELSE CAST(COBR.produto_id AS VARCHAR(255))" & Chr(13)
sSql = sSql & "        END AS 'Produto'" & Chr(13)
sSql = sSql & "     , CASE WHEN PROD.nome IS NULL THEN 'N�o Informado'" & Chr(13)
sSql = sSql & "            ELSE PROD.nome" & Chr(13)
sSql = sSql & "        END AS 'Desc_Produto'" & Chr(13)
sSql = sSql & "     , COUNT(COBR.proposta_id)  AS 'qtd_Propostas'" & Chr(13)
sSql = sSql & "  FROM cobranca_inad_nao_cancelada_tb COBR WITH (NOLOCK)" & Chr(13)
sSql = sSql & "  LEFT JOIN produto_tb                PROD WITH (NOLOCK)" & Chr(13)
sSql = sSql & "    ON COBR.produto_id = PROD.produto_id" & Chr(13)
sSql = sSql & " GROUP BY COBR.produto_id" & Chr(13)
sSql = sSql & "        , PROD.nome" & Chr(13)
sSql = sSql & " ORDER BY Desc_Produto" & Chr(13)
    
Set rc = rdocn.Execute(sSql)
  
If (rc.BOF And rc.EOF) Then
    MsgBox "Aten��o: N�o encontramos Registros de inadimplentes para gerar e enviar a Planilha. Caso o problema persista Favor comunicar o departamento de TI", vbCritical + vbOKOnly
Else
    Dim xlApp As Object
    Dim xlWb As Object
    Dim xlWs As Object
    
    Set xlApp = CreateObject("Excel.Application")
    Set xlWb = xlApp.Workbooks.Add
    Set xlWs = xlWb.Worksheets(1)
    
    xlApp.Visible = False
    xlApp.UserControl = True
    
    Dim i As Integer
    i = 1
    
    xlWs.Cells(i, 1).Value = "Produto"
    xlWs.Cells(i, 1).ColumnWidth = 15
    xlWs.Cells(i, 2).Value = "Descri��o do Produto"
    xlWs.Cells(i, 2).ColumnWidth = 53
    xlWs.Cells(i, 3).Value = "Quantidade de Propostas"
    xlWs.Cells(i, 3).ColumnWidth = 25
    
    Dim j As Integer
    
    For j = 1 To 3
        xlWs.Cells(i, j).Font.Bold = True
    Next
    
   'Dim Item As ListItem
    Dim y As Integer
    
    rc.MoveFirst
    'If rc.BOF Then rc.MoveNext
    Do While Not rc.EOF
        i = i + 1
        xlWs.Cells(i, 1).Value = rc("Produto")
        xlWs.Cells(i, 2).Value = rc("Desc_Produto")
        xlWs.Cells(i, 3).Value = rc("qtd_Propostas")
        rc.MoveNext
    Loop
    
    xlApp.DisplayAlerts = False
    xlApp.ActiveWorkbook.SaveAs SCaminho & sArquivo

    xlApp.quit

End If


' Envio por E-Mail
    'Atualiza status
    StbRodape.Panels(1).Text = "Enviando planilha por E-Mail"
    DoEvents

    'Titulo
    STitulo = "SEGP0742 - Identifica��o de Parcelas Inadimplentes"
    
    'Destinat�rio
    'Sdestinatario = "GRUPO_PARCELAS_INADIMPLENTES@aliancadobrasil.com.br"
    
    'Alterado conforme e-mail de Sergey do dia 24/02/2012
    Sdestinatario = "GRUPO_PARCELAS_INADIMPLENTES@bbmapfre.com.br"
    
    'Mensagem
    sMensagem = "Mensagem enviada por SEGP0742" & Chr(13) & Chr(13) & "Segue planilha em Anexo contendo relat�rio sint�tico das parcelas inadimplentes agrupadas por Produto." & Chr(13) & Chr(13) & "Atenciosamente."

    sSql = "exec email_db..SGSS0782_SPI  'SEGP0742'  , 'SEGBR'  , 'SEGP0742'  , 'SEGP0742' " 'SEGP e Emitente
    sSql = sSql & " , '" & Sdestinatario & "'  "   'Destinat�rio
    sSql = sSql & " ,'" & STitulo & "'"            'Assunto
    sSql = sSql & " , '" & sMensagem & "'"         'Corpo da Mensagem
    sSql = sSql & " ,'' "                          'CC
    sSql = sSql & " ,'" & SCaminho & sArquivo & "'" 'Caminho do Arquivo
    sSql = sSql & " , null "
    sSql = sSql & " ,'PRODUCAO3'"    'Usu�rio
    
    Set rc = Nothing
    Set rc = rdocn.Execute(sSql)
    
End Sub


Private Sub cmdSair_Click()

'## Rotina de encerramento do programa

On Local Error GoTo Erro

    Call FinalizarAplicacao
       
    Exit Sub
 
Erro:
    TratarErro "cmdSair_Click", Me.name
    Call FinalizarAplicacao
    
End Sub


Private Sub Form_Load()

On Local Error GoTo Erro
'Verifica se foi executado pelo Control-M
'CTM = Verifica_Origem_ControlM(Command)

        'Pula se for executado pelo control-m
        If CTM = False Then
            ' Verificando permiss�o de acesso � aplica��o ''''''''''''''''''''''''''
            If Not Trata_Parametros(Command) Then
               'FinalizarAplicacao
            End If
        End If
'
     cUserName = "Teste_Inadimplencia"
     glAmbiente_id = 3

    'Obtendo a data operacional ''''''''''''''''''''''''''''''''''''''''''''
    
Call Conexao
Call ObterDataSistema(gsSIGLASISTEMA)
    
    'Configurando interface'''''''''''''''''''''''''''''''''''''''''''''''''
      
    Call CentraFrm(Me)
      
    Call InicializarInterface
    
    Me.Caption = "SEGP0742 - Identifica��o de Inadimplentes - " & Ambiente
    
    'Inciando processo autom�ticamente caso seja uma chamada do scheduler'''
  
    If Obtem_agenda_diaria_id(Command) > 0 Or CTM = True Then
        Me.Show
        Call cmdProcessar_Click
        Call FinalizarAplicacao
    End If
     
    Exit Sub

Erro:
    TratarErro "Form_Load", Me.name
    Call FinalizarAplicacao

End Sub

Sub InicializarInterface()
    
    '## Rotina de inicializa��o de interface
      
    lblInicio.Caption = ""
    lblFim.Caption = ""
    
    StbRodape.Panels(1).Text = "Selecione Processar para efetuar os cancelamentos."
    
End Sub

Private Sub Form_Resize()

On Error GoTo Trata_Erro

    Call InicializarRodape
    
    Exit Sub

Trata_Erro:
  
    Call TratarErro("Form_Resize", Me.name)

End Sub

Sub BaixarInadimplentes()
    
'## Rotina de baixa de inadimplentes
    
Dim objCancelamento As Object
Dim lQtdProcessados As Long
    
On Error GoTo Trata_Erro
    
   
    StbRodape.Panels(1).Text = "Processando Baixa de Inadimplencias..."
    
    DoEvents
    
    Set objCancelamento = CreateObject("SEGL0019.cls00106")
    
    ' Baixando parcelas Inadimplentes ''''''''''''''''''''''''''''''''''''''''''''''''
    
    Call objCancelamento.BaixarInadimplencia(cUserName, _
                                             gsSIGLASISTEMA, _
                                             App.Title, _
                                             App.FileDescription, _
                                             glAmbiente_id, _
                                             Data_Sistema, _
                                             lQtdProcessados)
    
    
   ' Atualizando interface ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    
    lblBaixas.Caption = lQtdProcessados

    Set objCancelamento = Nothing
    
    Exit Sub
 
Trata_Erro:
    
    TratarErro "BaixarInadimplentes", Me.name
    Call FinalizarAplicacao
    
End Sub

