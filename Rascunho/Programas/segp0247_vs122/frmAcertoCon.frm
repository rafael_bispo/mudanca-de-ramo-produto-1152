VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "msflxgrd.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Begin VB.Form frmAcertoCon 
   Caption         =   "SEG12018 - Sele��o de Acertos de Sinistro - "
   ClientHeight    =   6645
   ClientLeft      =   180
   ClientTop       =   1485
   ClientWidth     =   11370
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   6645
   ScaleWidth      =   11370
   Begin MSFlexGridLib.MSFlexGrid flexSelecao 
      Height          =   3255
      Left            =   90
      TabIndex        =   7
      Top             =   2565
      Width           =   11280
      _ExtentX        =   19897
      _ExtentY        =   5741
      _Version        =   393216
      Cols            =   11
      FillStyle       =   1
      SelectionMode   =   1
      FormatString    =   $"frmAcertoCon.frx":0000
   End
   Begin VB.CommandButton BtnCancelar 
      Caption         =   "&Sair"
      Height          =   375
      Left            =   8400
      TabIndex        =   9
      Top             =   5880
      Width           =   1452
   End
   Begin VB.CommandButton BtnOk 
      Caption         =   "&Ok"
      Height          =   375
      Left            =   6840
      TabIndex        =   8
      Top             =   5880
      Width           =   1452
   End
   Begin MSComctlLib.StatusBar StatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   14
      Top             =   6360
      Width           =   11370
      _ExtentX        =   20055
      _ExtentY        =   503
      Style           =   1
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Frame Framecriterios 
      Caption         =   "Crit�rios"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2295
      Left            =   90
      TabIndex        =   10
      Top             =   135
      Width           =   11235
      Begin VB.ComboBox cmbSitSinistroAvaliador 
         Height          =   315
         ItemData        =   "frmAcertoCon.frx":0128
         Left            =   6840
         List            =   "frmAcertoCon.frx":0135
         Style           =   2  'Dropdown List
         TabIndex        =   5
         Top             =   1320
         Visible         =   0   'False
         Width           =   2535
      End
      Begin VB.TextBox txtConteudo 
         Height          =   315
         Left            =   3360
         TabIndex        =   4
         Top             =   1320
         Width           =   6015
      End
      Begin VB.ComboBox cmbCampo 
         Height          =   315
         ItemData        =   "frmAcertoCon.frx":0156
         Left            =   3360
         List            =   "frmAcertoCon.frx":0158
         Style           =   2  'Dropdown List
         TabIndex        =   2
         Top             =   600
         Width           =   3135
      End
      Begin VB.CommandButton btnPesquisar 
         Caption         =   "&Pesquisar"
         Height          =   372
         Left            =   8040
         TabIndex        =   6
         Top             =   1800
         Width           =   1332
      End
      Begin VB.Frame Frame1 
         Caption         =   "Ramo Para Sele��o"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1335
         Left            =   240
         TabIndex        =   11
         Top             =   360
         Width           =   2655
         Begin VB.OptionButton optVida 
            Caption         =   "Vida"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Left            =   240
            TabIndex        =   0
            Top             =   360
            Width           =   1815
         End
         Begin VB.OptionButton optRE 
            Caption         =   "Ramos Elementares"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   495
            Left            =   240
            TabIndex        =   1
            Top             =   720
            Width           =   2295
         End
      End
      Begin VB.Frame frameSituacao 
         BorderStyle     =   0  'None
         Caption         =   "Frame2"
         Height          =   855
         Left            =   6720
         TabIndex        =   16
         Top             =   120
         Visible         =   0   'False
         Width           =   2895
         Begin VB.ComboBox cmbSituacao 
            Height          =   315
            ItemData        =   "frmAcertoCon.frx":015A
            Left            =   120
            List            =   "frmAcertoCon.frx":0170
            Style           =   2  'Dropdown List
            TabIndex        =   3
            Top             =   480
            Width           =   2535
         End
         Begin VB.Label Label3 
            Caption         =   "Situa��o Pagamento/Recebimento:"
            Height          =   255
            Index           =   0
            Left            =   120
            TabIndex        =   17
            Top             =   240
            Width           =   2820
         End
      End
      Begin VB.Label LblSitSinistroAvaliador 
         Caption         =   "Situa��o do Sinistro:"
         Height          =   255
         Left            =   6840
         TabIndex        =   18
         Top             =   1080
         Visible         =   0   'False
         Width           =   2295
      End
      Begin VB.Label lblConteudo 
         Caption         =   "Conte�do:"
         Height          =   255
         Left            =   3360
         TabIndex        =   13
         Top             =   1080
         Width           =   975
      End
      Begin VB.Label Label1 
         Caption         =   "Pesquisar por:"
         Height          =   255
         Left            =   3360
         TabIndex        =   12
         Top             =   360
         Width           =   1215
      End
   End
   Begin MSFlexGridLib.MSFlexGrid flexAutorizacao 
      Height          =   3255
      Left            =   60
      TabIndex        =   19
      Top             =   2535
      Visible         =   0   'False
      Width           =   11280
      _ExtentX        =   19897
      _ExtentY        =   5741
      _Version        =   393216
      Rows            =   1
      Cols            =   6
      FixedCols       =   0
      AllowBigSelection=   0   'False
      SelectionMode   =   1
      FormatString    =   $"frmAcertoCon.frx":01BC
   End
   Begin VB.Label lblSelecionados 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   120
      TabIndex        =   15
      Top             =   5880
      Width           =   4215
   End
End
Attribute VB_Name = "frmAcertoCon"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim selecionou As Boolean
Dim vSql As String
Dim vTipo As String, vData As String

'Alan Neves - MP - 12/03/2013 - Inicio
Public cod_tecnico As Integer
'Alan Neves - MP - 12/03/2013 - Fim

Private Sub BtnCancelar_Click()
    Unload Me
End Sub

Private Sub InicializaCargaDados_LimpezaTemporario()
'Alan Neves - MP - 18/03/2013
    Dim sSQL As String
    Dim rs As rdoResultset
    
    sSQL = ""
    '' GENESCO SILVA - FLOW 18313521 - MIGRA��O SQL SERVER
    'sSQL = sSQL & " if isnull(OBJECT_ID('tempdb..#sinistro_tb'),0) >0" & vbNewLine
    sSQL = sSQL & " if OBJECT_ID('tempdb..#sinistro_tb') IS NOT NULL " & vbNewLine
    sSQL = sSQL & " BEGIN" & vbNewLine
    sSQL = sSQL & "     DROP TABLE #sinistro_tb" & vbNewLine
    sSQL = sSQL & " END" & vbNewLine

    '' GENESCO SILVA - FLOW 18313521 - MIGRA��O SQL SERVER
    'sSQL = sSQL & " if isnull(OBJECT_ID('tempdb..#apolice_tb'),0) >0" & vbNewLine
    sSQL = sSQL & " if OBJECT_ID('tempdb..#apolice_tb') IS NOT NULL " & vbNewLine
    sSQL = sSQL & " BEGIN" & vbNewLine
    sSQL = sSQL & "     DROP TABLE #apolice_tb" & vbNewLine
    sSQL = sSQL & " END" & vbNewLine

    sSQL = sSQL & "Create Table #sinistro_tb "
    sSQL = sSQL & "     ( Sinistro_ID                                   Numeric(11)         " & vbNewLine   ' -- Sinistro_Tb
    sSQL = sSQL & "     , Sucursal_Seguradora_ID                        Numeric(5)          " & vbNewLine  ' -- Sinistro_Tb
    sSQL = sSQL & "     , Seguradora_Cod_Susep                          Numeric(5)          " & vbNewLine  ' -- Sinistro_Tb
    sSQL = sSQL & "     , Apolice_ID                                    Numeric(9)          " & vbNewLine  ' -- Sinistro_Tb
    sSQL = sSQL & "     , Ramo_ID                                       Int                 " & vbNewLine  ' -- Sinistro_Tb
    sSQL = sSQL & "     , Ramo_Nome                                     VarChar(60)         " & vbNewLine  ' -- Ramo_Tb
    sSQL = sSQL & "     , Proposta_ID                                   Numeric(9)          " & vbNewLine  ' -- Sinistro_Tb
    sSQL = sSQL & "     , Produto_ID                                    Int                 " & vbNewLine  ' -- Proposta_Tb
    sSQL = sSQL & "     , Produto_Nome                                  VarChar(60)         " & vbNewLine  ' -- Produto_Tb
    sSQL = sSQL & "     , Moeda_ID                                      Numeric(3)          " & vbNewLine  ' -- Moeda_TB
    sSQL = sSQL & "     , pgto_parcial_co_seguro                        Char(1)             " & vbNewLine  ' -- Sinistro_TB
    sSQL = sSQL & "     , tp_emissao                                    Char(1)             " & vbNewLine  ' -- Apolice_TB
    sSQL = sSQL & "     , situacao                                      Char(1)             " & vbNewLine  ' -- Sinistro_TB
    sSQL = sSQL & "     ) " & vbNewLine
    sSQL = sSQL & "     " & vbNewLine
    sSQL = sSQL & "Create Index #IDX_Sinistro_TB                        on #sinistro_tb (Sinistro_ID, Proposta_ID, Sucursal_Seguradora_ID, Seguradora_Cod_Susep, Apolice_ID, Ramo_ID)" & vbNewLine
    sSQL = sSQL & "" & vbNewLine

    Set rs = rdocn1.OpenResultset(sSQL)
    rs.Close
    
End Sub

'Alan Neves - MP - 19/03/2013 - Inicio
Sub SelecionaDadosPgtoSinistro()
    Dim vSql As String
    Dim rs As rdoResultset
    
    '' GENESCO SILVA - FLOW 18313521 - MIGRA��O SQL SERVER
    'vSql = vSql & " if isnull(OBJECT_ID('tempdb..#pgto_sinistro_tb'),0) >0" & vbNewLine
    vSql = vSql & " if OBJECT_ID('tempdb..#pgto_sinistro_tb') IS NOT NULL " & vbNewLine
    vSql = vSql & " BEGIN" & vbNewLine
    vSql = vSql & "     DROP TABLE #pgto_sinistro_tb" & vbNewLine
    vSql = vSql & " END" & vbNewLine
    
    '' GENESCO SILVA - FLOW 18313521 - MIGRA��O SQL SERVER
    'vSql = vSql & " if isnull(OBJECT_ID('tempdb..#sinistro_estimativa_tb'),0) >0" & vbNewLine
    vSql = vSql & " if OBJECT_ID('tempdb..#sinistro_estimativa_tb') IS NOT NULL " & vbNewLine
    vSql = vSql & " BEGIN" & vbNewLine
    vSql = vSql & "     DROP TABLE #sinistro_estimativa_tb" & vbNewLine
    vSql = vSql & " END" & vbNewLine
    
    vSql = vSql & " select pgto_sinistro_tb.* into #pgto_sinistro_tb" & vbNewLine
    vSql = vSql & "   from #sinistro_tb sinistro_tb                                                                       " & vbNewLine
    vSql = vSql & "   join seguros_db..pgto_sinistro_tb pgto_sinistro_tb with (nolock) " & vbNewLine
    vSql = vSql & "     on pgto_sinistro_tb.sinistro_id               = sinistro_tb.sinistro_id                           " & vbNewLine
    vSql = vSql & "    and pgto_sinistro_tb.apolice_id                = sinistro_tb.apolice_id                            " & vbNewLine
    vSql = vSql & "    and pgto_sinistro_tb.sucursal_seguradora_id    = sinistro_tb.sucursal_seguradora_id                " & vbNewLine
    vSql = vSql & "    and pgto_sinistro_tb.seguradora_cod_susep      = sinistro_tb.seguradora_cod_susep                  " & vbNewLine
    vSql = vSql & "    and pgto_sinistro_tb.ramo_id                   = sinistro_tb.ramo_id                               " & vbNewLine
    vSql = vSql & "Create Index #IDX_Pgto_Sinistro_TB                on #Pgto_Sinistro_TB (sinistro_id, apolice_id, sucursal_seguradora_id, seguradora_cod_susep, ramo_id, seq_estimativa, item_val_estimativa, num_parcela, seq_pgto, beneficiario_id)" & vbNewLine
    
    vSql = vSql & " select sinistro_estimativa_tb.sinistro_id,  sinistro_estimativa_tb.apolice_id, sinistro_estimativa_tb.sucursal_seguradora_id, sinistro_estimativa_tb.seguradora_cod_susep, sinistro_estimativa_tb.ramo_id, " & vbNewLine
    vSql = vSql & " sinistro_estimativa_tb.item_val_estimativa, sinistro_estimativa_tb.seq_estimativa, sinistro_estimativa_tb.num_carta_seguro_aceito, sinistro_estimativa_tb.dt_fim_estimativa " & vbNewLine
    vSql = vSql & "   into #sinistro_estimativa_tb                                                                        " & vbNewLine
    vSql = vSql & "   from #sinistro_tb sinistro_tb                                                                       " & vbNewLine
    vSql = vSql & "   join seguros_db..sinistro_Estimativa_tb sinistro_Estimativa_tb with (nolock) " & vbNewLine
    vSql = vSql & "     on sinistro_estimativa_tb.sinistro_id               = sinistro_tb.sinistro_id                           " & vbNewLine
    vSql = vSql & "Create Index #IDX_sinistro_estimativa_tb                on #sinistro_estimativa_tb (sinistro_id, apolice_id, sucursal_seguradora_id, seguradora_cod_susep, ramo_id, item_val_estimativa, seq_estimativa)" & vbNewLine
    
    Set rs = rdocn1.OpenResultset(vSql)
    rs.Close
    'Alan Neves - MP - 19/03/2013 - Fim

End Sub

Private Sub BtnOk_Click()

    Dim sinsel As Sinist
    Dim Sin As New Sinist
    Dim rs As rdoResultset


    If Not selecionou Then
       mensagem_erro 6, "Selecione um registro."
       Exit Sub
    End If
    
    'Alan Neves - MP - 19/03/2013 - Inicio
    InicializaCargaDados_LimpezaTemporario
    'Alan Neves - MP - 19/03/2013 - Fim
   
    '---------------------------
    'Luciana - 03/10/2003
    '---------------------------
    'If flexSelecao.Visible Then
    If bFlexSelecao Then
    '---------------------------
        Set sinsel = SinistrosSelecionados.Item(flexSelecao.Row)
                
        'Confitec - Renato Silva 03/12/2010: Verificar se o benefici�rio � o Banco - Projeto Cr�dito Protegido.
        If Hour(DataHoraAmbiente) >= 15 And LocalizarBeneficiarioBanco(sinsel.sinistro_id, CInt(sinsel.produto_id)) Then
            mensagem_erro 6, "Aprova��o de pagamento n�o autorizado ap�s as 15 horas, para benefici�rio Banco do Brasil."
            Exit Sub
        End If

        
        '-----------------------------------------------------------
        'Luciana - 19/01/2004 - Inclus�o de: And chamado_246 = False
        '-----------------------------------------------------------
        If voper <> "C" And chamado_246 = False Then
        '-----------------------------------------------------------
            vSql = "SELECT lock_aviso FROM sinistro_tb with(nolock) " & _
                    "WHERE sinistro_id = " & sinsel.sinistro_id & _
                    "  AND apolice_id = " & sinsel.Apolice_id & _
                    "  AND sucursal_seguradora_id = " & sinsel.Sucursal_id & _
                    "  AND seguradora_cod_susep = " & sinsel.Seguradora_id & _
                    "  AND ramo_id = " & sinsel.ramo_id
            Set rs = rdocn.OpenResultset(vSql)
            If rs(0) = "s" Then
                mensagem_erro 6, "Este sinistro est� sendo atualizado por outro usu�rio!"
                If voper <> "IS" Then
                  btnPesquisar.SetFocus
                End If
                Desmonta_Selecao
                Exit Sub
            End If
            rs.Close
        End If
        
        vSinistro = sinsel.sinistro_id
        Set Sin = SinistrosSelecionados.Item(flexSelecao.Row)
        
        'Alan Neves - MP - 19/03/2013 - Inicio
        vSql = "set nocount on" & vbNewLine
        vSql = vSql & " insert into #sinistro_tb" & vbNewLine
        vSql = vSql & " (sinistro_id , Sucursal_Seguradora_ID, Seguradora_Cod_Susep, Apolice_id, ramo_id, Proposta_ID, Produto_ID, Moeda_ID , Tp_emissao, pgto_parcial_co_seguro, situacao, ramo_nome, produto_nome )" & vbNewLine
        vSql = vSql & " select sinistro_tb.sinistro_id, sinistro_tb.sucursal_seguradora_id, sinistro_tb.Seguradora_cod_susep, sinistro_tb.Apolice_id, sinistro_tb.ramo_id, " & vbNewLine
        vSql = vSql & " sinistro_tb.Proposta_id, " & sinsel.produto_id & ", sinistro_tb.Moeda_id, Apolice_tb.Tp_Emissao, Sinistro_tb.pgto_parcial_co_seguro, Sinistro_TB.situacao, Ramo_TB.nome, Produto_TB.nome " & vbNewLine
        vSql = vSql & "   from seguros_db..sinistro_tb sinistro_tb with(nolock)                                         " & vbNewLine
        vSql = vSql & "   join seguros_db..apolice_tb  apolice_tb  with(nolock)                                         " & vbNewLine
        vSql = vSql & "     on apolice_tb.apolice_id                = sinistro_tb.apolice_id                            " & vbNewLine
        vSql = vSql & "    and apolice_tb.sucursal_seguradora_id    = sinistro_tb.sucursal_seguradora_id                " & vbNewLine
        vSql = vSql & "    and apolice_tb.seguradora_cod_susep      = sinistro_tb.seguradora_cod_susep                  " & vbNewLine
        vSql = vSql & "    and apolice_tb.ramo_id                   = sinistro_tb.ramo_id                               " & vbNewLine
        vSql = vSql & "   join seguros_db..proposta_tb  proposta_tb with(nolock)                                        " & vbNewLine
        vSql = vSql & "     on sinistro_tb.proposta_id              = proposta_tb.proposta_id                           " & vbNewLine
        vSql = vSql & "   join seguros_db..produto_tb  produto_tb   with(nolock)                                        " & vbNewLine
        vSql = vSql & "     on produto_tb.produto_id                = proposta_tb.produto_id                            " & vbNewLine
        vSql = vSql & "   join seguros_db..ramo_tb ramo_tb         with(nolock)                                         " & vbNewLine
        vSql = vSql & "     on ramo_tb.ramo_id                      = sinistro_tb.ramo_id                               " & vbNewLine
        vSql = vSql & "  where sinistro_tb.sinistro_id              = " & sinsel.sinistro_id & vbNewLine
        vSql = vSql & "    and sinistro_tb.apolice_id               = " & sinsel.Apolice_id & vbNewLine
        vSql = vSql & "    and sinistro_tb.sucursal_seguradora_id   = " & sinsel.Sucursal_id & vbNewLine
        vSql = vSql & "    and sinistro_tb.seguradora_cod_susep     = " & sinsel.Seguradora_id & vbNewLine
        vSql = vSql & "    and sinistro_tb.ramo_id                  = " & sinsel.ramo_id & vbNewLine
        
        vSql = vSql & " select " & vbNewLine
        vSql = vSql & " Apolice_tb.apolice_id , Apolice_tb.Tp_Emissao, sinistro_tb.sucursal_seguradora_id, sinistro_tb.seguradora_cod_susep, " & vbNewLine
        vSql = vSql & " sinistro_tb.ramo_id, Apolice_tb.Proposta_id " & vbNewLine
        vSql = vSql & "   into #apolice_tb " & vbNewLine
        vSql = vSql & "   from #sinistro_tb sinistro_tb with(nolock)                                         " & vbNewLine
        vSql = vSql & "   join seguros_db..apolice_tb  apolice_tb  with(nolock)                                         " & vbNewLine
        vSql = vSql & "     on apolice_tb.apolice_id                = sinistro_tb.apolice_id                            " & vbNewLine
        vSql = vSql & "    and apolice_tb.sucursal_seguradora_id    = sinistro_tb.sucursal_seguradora_id                " & vbNewLine
        vSql = vSql & "    and apolice_tb.seguradora_cod_susep      = sinistro_tb.seguradora_cod_susep                  " & vbNewLine
        vSql = vSql & "    and apolice_tb.ramo_id                   = sinistro_tb.ramo_id                               " & vbNewLine
        
        Set rs = rdocn1.OpenResultset(vSql)
        rs.Close
        
        SelecionaDadosPgtoSinistro
        'Alan Neves - MP - 19/03/2013 - Fim
        
        '-----------------------------------------------------------------
        'Demanda 689336 - emaior - 10/09/2009 - Indeniza��o superior IS.
        '-----------------------------------------------------------------
        
        If VerificaAlcadaTecnico And voper = "I" Then
            MsgBox "Sua al�ada n�o permite aprovar solicita��o de pagamento superior a Import�ncia Segurada da cobertura."
            Exit Sub
        End If
        
       
        '-----------------------------------------------------------------

        ' Avisos de Inadimpl�ncia
        'Alan Neves - Inicio - MP - 06/02/2013 - na proc foi substitui o count(*) por count(1). No VB substituido select pelo achou_paridade
        ' vSQL = "EXEC verifica_inadimplencia_sps " & _

        vSql = "EXEC verifica_inadimplencia_sps " & _
            Sin.Proposta_id & ", '" & Format(Data_Sistema, "yyyymmdd") & "', " & _
            Sin.Apolice_id & ", " & Sin.Sucursal_id & ", " & Sin.Seguradora_id & ", " & _
            Sin.ramo_id & ", '" & Format(Sin.dt_Ocorrencia, "yyyymmdd") & "'"
        
        'Alan Neves - Fim
        
        Set rs = rdocn.OpenResultset(vSql)
        If rs(0) > 0 Then
            MsgBox "Existe(m) parcela(s) em atraso para esta proposta."
        End If
        rs.Close

'''        vSql = "SELECT COUNT(num_cobranca) FROM agendamento_cobranca_tb " & _
'''               "WHERE proposta_id = " & Sin.Proposta_id & _
'''               "  AND dt_agendamento <= '" & Format(Data_Sistema, "yyyymmdd") & "'" & _
'''               "  AND dt_recebimento is NULL "
'''        Set rs = rdocn.OpenResultset(vSql)
'''        If rs(0) > 0 Then
'''            MsgBox "Existe(m) parcela(s) em atraso para esta proposta."
'''        Else
'''            rs.Close
'''            vSql = "SELECT COUNT(fatura_id) FROM fatura_tb (nolock) " & _
'''                   " WHERE apolice_id = " & Sin.Apolice_id & _
'''                   " AND sucursal_seguradora_id = " & Sin.Sucursal_id & _
'''                   " AND seguradora_cod_susep = " & Sin.Seguradora_id & _
'''                   " AND ramo_id = " & Sin.ramo_id & _
'''                   " AND situacao = 'b'" & _
'''                   " AND dt_recebimento <= '" & Format(Sin.dt_Ocorrencia, "yyyymmdd") & "'"
'''            Set rs = rdocn.OpenResultset(vSql)
'''            If rs(0) > 0 Then
'''                MsgBox "Existe(m) parcela(s) em atraso para esta proposta."
'''            End If
'''        End If
'''        rs.Close
        
        SinistroEmUso.Add Sin
        
        MousePointer = 11
        
        If voper <> "C" Then
            'vsql = "EXEC sinistro_marca_spu " & _  'C_ANEVES / msalema - 21/06/2013
            vSql = "EXEC SEGS11074_SPU " & _
                    sinsel.sinistro_id & "," & sinsel.Apolice_id & "," & _
                    sinsel.Sucursal_id & "," & sinsel.Seguradora_id & "," & sinsel.ramo_id
                    
            Set rs = rdocn.OpenResultset(vSql)
            rs.Close
        End If
    
    Else ' flexAutorizacao
        
        vSql = "SELECT distinct a.sinistro_id "
        vSql = vSql & " FROM sinistro_tb a with(nolock) "
        vSql = vSql & " INNER JOIN pgto_sinistro_tb b with(nolock) "
        vSql = vSql & "    ON a.sinistro_id = b.sinistro_id "
        vSql = vSql & "   AND a.apolice_id = b.apolice_id "
        vSql = vSql & "   AND a.seguradora_cod_susep = b.seguradora_cod_susep "
        vSql = vSql & "   AND a.sucursal_seguradora_id = b.sucursal_seguradora_id "
        vSql = vSql & "   AND a.ramo_id = b.ramo_id "
        vSql = vSql & " WHERE chave_autorizacao_sinistro = " & Left(flexAutorizacao.TextMatrix(flexAutorizacao.Row, 0), Len(flexAutorizacao.TextMatrix(flexAutorizacao.Row, 0)) - 2)
        vSql = vSql & "   AND a.lock_aviso = 's' "
        
        Set rs = rdocn.OpenResultset(vSql)
        If Not rs.EOF Then
            mensagem_erro 6, "O sinistro " & rs("sinistro_id") & " que faz parte dessa autoriza��o est� sendo atualizado por outro usu�rio!"
            rs.Close
            btnPesquisar.SetFocus
            flexAutorizacao.Rows = 1
            Exit Sub
        End If
        rs.Close
    
        'Alan Neves - 18/03/2013 - Inicio - MP - Acrescentado join proposta_tb, ramo_tb, produto_tb
'        vsql = "SELECT distinct a.sinistro_id, a.apolice_id, "
'        vsql = vsql & " a.seguradora_cod_susep, a.sucursal_seguradora_id, "
'        vsql = vsql & " a.ramo_id, a.situacao "
'        vsql = vsql & " FROM seguros_db..sinistro_tb a with(nolock) "
'        vsql = vsql & " INNER JOIN seguros_db..pgto_sinistro_tb b with(nolock) "
'        vsql = vsql & "    ON a.sinistro_id = b.sinistro_id "
'        vsql = vsql & "   AND a.apolice_id = b.apolice_id "
'        vsql = vsql & "   AND a.seguradora_cod_susep = b.seguradora_cod_susep "
'        vsql = vsql & "   AND a.sucursal_seguradora_id = b.sucursal_seguradora_id "
'        vsql = vsql & "   AND a.ramo_id = b.ramo_id "
'        vsql = vsql & " WHERE chave_autorizacao_sinistro = " & Left(flexAutorizacao.TextMatrix(flexAutorizacao.Row, 0), Len(flexAutorizacao.TextMatrix(flexAutorizacao.Row, 0)) - 2)
        
        vSql = "set nocount on" & vbNewLine
        vSql = vSql & " insert into #sinistro_tb" & vbNewLine
        vSql = vSql & " (sinistro_id , Sucursal_Seguradora_ID, Seguradora_Cod_Susep, Apolice_id, ramo_id, Proposta_ID, Produto_ID, Moeda_ID , Tp_emissao, pgto_parcial_co_seguro, situacao, ramo_nome, produto_nome )" & vbNewLine
        'Cleber Sardo - INC000005184084 - Data 20/10/2016 - Corrigir duplica��o
        'vSql = vSql & " select sinistro_tb.sinistro_id, sinistro_tb.sucursal_seguradora_id, sinistro_tb.Seguradora_cod_susep, sinistro_tb.Apolice_id, sinistro_tb.ramo_id, " & vbNewLine
        vSql = vSql & " select distinct sinistro_tb.sinistro_id, sinistro_tb.sucursal_seguradora_id, sinistro_tb.Seguradora_cod_susep, sinistro_tb.Apolice_id, sinistro_tb.ramo_id, " & vbNewLine
        vSql = vSql & " sinistro_tb.Proposta_id, produto_tb.produto_id, sinistro_tb.Moeda_id, Apolice_tb.Tp_Emissao, Sinistro_tb.pgto_parcial_co_seguro, Sinistro_TB.situacao, Ramo_TB.nome, Produto_TB.nome " & vbNewLine
        vSql = vSql & "   from seguros_db..sinistro_tb sinistro_tb with(nolock)                                         " & vbNewLine
        vSql = vSql & "   join seguros_db..pgto_sinistro_tb  pgto_sinistro_tb  with(nolock)                             " & vbNewLine
        vSql = vSql & "     on pgto_sinistro_tb.sinistro_id               = sinistro_tb.sinistro_id                     " & vbNewLine
        vSql = vSql & "    and pgto_sinistro_tb.apolice_id                = sinistro_tb.apolice_id                      " & vbNewLine
        vSql = vSql & "    and pgto_sinistro_tb.sucursal_seguradora_id    = sinistro_tb.sucursal_seguradora_id          " & vbNewLine
        vSql = vSql & "    and pgto_sinistro_tb.seguradora_cod_susep      = sinistro_tb.seguradora_cod_susep            " & vbNewLine
        vSql = vSql & "    and pgto_sinistro_tb.ramo_id                   = sinistro_tb.ramo_id                         " & vbNewLine
        vSql = vSql & "   join seguros_db..apolice_tb  apolice_tb  with(nolock)                                         " & vbNewLine
        vSql = vSql & "     on apolice_tb.apolice_id                = sinistro_tb.apolice_id                            " & vbNewLine
        vSql = vSql & "    and apolice_tb.sucursal_seguradora_id    = sinistro_tb.sucursal_seguradora_id                " & vbNewLine
        vSql = vSql & "    and apolice_tb.seguradora_cod_susep      = sinistro_tb.seguradora_cod_susep                  " & vbNewLine
        vSql = vSql & "    and apolice_tb.ramo_id                   = sinistro_tb.ramo_id                               " & vbNewLine
        vSql = vSql & "   join seguros_db..Proposta_tb  Proposta_tb  with(nolock)                                       " & vbNewLine
        vSql = vSql & "     on Proposta_tb.Proposta_id                = sinistro_tb.Proposta_id                         " & vbNewLine
        vSql = vSql & "   join seguros_db..Produto_tb  Produto_tb  with(nolock)                                         " & vbNewLine
        vSql = vSql & "     on Proposta_tb.produto_id                = Produto_tb.produto_id                            " & vbNewLine
        vSql = vSql & "   join seguros_db..Ramo_tb  Ramo_tb with(nolock)                                                " & vbNewLine
        vSql = vSql & "     on sinistro_tb.ramo_id = Ramo_tb.ramo_id                                                    " & vbNewLine
        vSql = vSql & "  where chave_autorizacao_sinistro = " & Left(flexAutorizacao.TextMatrix(flexAutorizacao.Row, 0), Len(flexAutorizacao.TextMatrix(flexAutorizacao.Row, 0)) - 2) & vbNewLine
        
        'Alan Neves - C_ANEVES - 29/06/2013 - Inicio
        'vsql = vsql & " insert into #apolice_tb" & vbNewLine
        'vsql = vsql & " (Apolice_tb.apolice_id , Apolice_tb.Tp_Emissao, sinistro_tb.sucursal_seguradora_id, sinistro_tb.sucursal_seguradora_id, sinistro_tb.seguradora_cod_susep, " & vbNewLine
        'vsql = vsql & " (sinistro_tb.ramo_id, Apolice_tb.Proposta_id " & vbNewLine
        
        vSql = vSql & " select " & vbNewLine
        vSql = vSql & " Apolice_tb.apolice_id , Apolice_tb.Tp_Emissao, sinistro_tb.sucursal_seguradora_id, sinistro_tb.seguradora_cod_susep, " & vbNewLine
        vSql = vSql & " sinistro_tb.ramo_id, Apolice_tb.Proposta_id " & vbNewLine
        vSql = vSql & "   into #apolice_tb " & vbNewLine
        'Alan Neves - C_ANEVES - 29/06/2013 - Fim
        
        vSql = vSql & "   from #sinistro_tb sinistro_tb with(nolock)                                         " & vbNewLine
        vSql = vSql & "   join seguros_db..apolice_tb  apolice_tb  with(nolock)                                         " & vbNewLine
        vSql = vSql & "     on apolice_tb.apolice_id                = sinistro_tb.apolice_id                            " & vbNewLine
        vSql = vSql & "    and apolice_tb.sucursal_seguradora_id    = sinistro_tb.sucursal_seguradora_id                " & vbNewLine
        vSql = vSql & "    and apolice_tb.seguradora_cod_susep      = sinistro_tb.seguradora_cod_susep                  " & vbNewLine
        vSql = vSql & "    and apolice_tb.ramo_id                   = sinistro_tb.ramo_id                               " & vbNewLine
        
        MousePointer = 11
        
        Set rs = rdocn1.OpenResultset(vSql)
        rs.Close
        
        SelecionaDadosPgtoSinistro
        
        vSql = ""
        vSql = vSql & "  SELECT distinct a.sinistro_id, a.apolice_id, " & vbNewLine
        vSql = vSql & " a.seguradora_cod_susep, a.sucursal_seguradora_id, " & vbNewLine
        vSql = vSql & " a.ramo_id, a.situacao, a.proposta_id, a.produto_id, a.produto_nome, a.ramo_id , a.ramo_nome" & vbNewLine
        vSql = vSql & "  FROM #sinistro_tb a with(nolock) " & vbNewLine
        'Alan Neves - 18/03/2013 - Fim
        
        Set rs = rdocn1.OpenResultset(vSql)
        
        While Not rs.EOF
            Set Sin = New Sinist
            Sin.sinistro_id = rs("sinistro_id")
            Sin.Apolice_id = rs("apolice_id")
            Sin.Sucursal_id = rs("sucursal_seguradora_id")
            Sin.Seguradora_id = rs("seguradora_cod_susep")
            Sin.ramo_id = rs("ramo_id")
            Sin.Situacao = rs("situacao")
            
            'Alan Neves - MP - Inicio - 18/03/2013
            Sin.NomeRamo = rs("ramo_nome")
            Sin.produto_id = rs("produto_id")
            Sin.NomeProduto = rs("produto_nome")
            Sin.Proposta_id = rs("proposta_id")
            'Alan Neves - MP - Fim
            
            SinistroEmUso.Add Sin
            rs.MoveNext
        Wend
        rs.Close

        For Each Sin In SinistroEmUso
            If voper <> "C" Then
                'vsql = "EXEC sinistro_marca_spu " & _ 'C_ANEVES / msalema - 21/06/2013
                vSql = "EXEC SEGS11074_SPU " & _
                        Sin.sinistro_id & "," & Sin.Apolice_id & "," & _
                        Sin.Sucursal_id & "," & Sin.Seguradora_id & "," & Sin.ramo_id
                Set rs = rdocn.OpenResultset(vSql)
                rs.Close
            End If
        Next
    
    End If
    
    frmAcerto.Show
    
    MousePointer = 0
    If (frmAcerto.flexPgtoSinistro.Rows = 1) And _
       (vopercoseguro = "C") And _
       (flexSelecao.Visible) Then
        MsgBox "Todos os pagamentos deste sinistro pertencem a autoriza��es em grupo. Entre com o n�mero da autoriza��o."
        frmAcerto.BtnCancelar_Click
    Else
        Me.Hide
    End If
    
    If flexSelecao.Visible Or voper = "IS" Then
        Desmonta_Selecao
    Else ' flexAutorizacao
        flexAutorizacao.Rows = 1
    End If
    selecionou = False

End Sub

Private Sub btnPesquisar_Click()
    
    Dim rs As rdoResultset
    
    If Not optVida.Value And Not optRE.Value Then
       mensagem_erro 6, "Selecione o ramo a ser pesquisado!"
       txtConteudo.Enabled = True
       cmbCampo.SetFocus
       Exit Sub
    End If
    If cmbCampo.Text = "" And voper <> "I" Then
       mensagem_erro 6, "Informe o campo a ser pesquisado!"
       txtConteudo.Enabled = True
       cmbCampo.SetFocus
       Exit Sub
    End If
    If cmbCampo.Text <> "Pendentes de Aprova��o" Then
        If txtConteudo.Text = "" And cmbCampo.Text <> "" And frameSituacao.Visible = False Then
           mensagem_erro 6, "Informe o conte�do a ser pesquisado!"
           txtConteudo.Enabled = True
           txtConteudo.SetFocus
           Exit Sub
        End If
    End If
    If txtConteudo.Text = "" And cmbSitSinistroAvaliador.Visible Then
       mensagem_erro 6, "Informe o conte�do a ser pesquisado!"
       txtConteudo.Enabled = True
       txtConteudo.SetFocus
       Exit Sub
    End If
    If cmbCampo.Text = "Ap�lice" Or cmbCampo.Text = "CNPJ" Or cmbCampo.Text = "CPF" Or _
        cmbCampo.Text = "Proposta" Or cmbCampo.Text = "Proposta BB" Or _
        cmbCampo.Text = "N�mero Sinistro" Or cmbCampo.Text = "N�mero Sinistro BB" Or _
        cmbCampo.Text = "Voucher" Or cmbCampo.Text = "N� da Autoriza��o" Then
       If IsNumeric(txtConteudo.Text) = False Then
          mensagem_erro 6, "O conte�do informado n�o � um n�mero!"
          txtConteudo.Enabled = True
          txtConteudo.SetFocus
          Exit Sub
       End If
    End If
    
    MousePointer = 11
    
    If optVida.Value Then
        vTpRamo = "1"
    ElseIf optRE.Value Then
        vTpRamo = "2"
    Else
        vTpRamo = "0"
    End If
    
    Select Case cmbCampo.Text
        Case "Pendentes de Aprova��o"
        'Alan Neves - MP - 08/03/2013
'            vSql = "SELECT count(*) FROM pgto_sinistro_tb with(nolock) " & _

            vSql = "SELECT count(1) FROM pgto_sinistro_tb with(nolock) " & _
                    "WHERE situacao_op = 'n' "
        'Alan Neves - MP - 08/03/2013
                    
            vTipo = "0"

        Case "N�mero Sinistro"
            If Len(txtConteudo) > 11 Then
              mensagem_erro 6, "N�mero maior que o permitido"
              txtConteudo.Enabled = True
              txtConteudo.SetFocus
              MousePointer = 0
              Exit Sub
            End If
            
            vSql = "SELECT count(sinistro_id) FROM sinistro_tb with(nolock) " & _
                    "WHERE sinistro_tb.sinistro_id = " & txtConteudo
            vTipo = "2"

        Case "N�mero Sinistro BB"
            If Len(txtConteudo) > 13 Then
                mensagem_erro 6, "N�mero maior que o permitido"
              txtConteudo.Enabled = True
              txtConteudo.SetFocus
              MousePointer = 0
              Exit Sub
            End If
            
            vSql = "SELECT count(sinistro_id) FROM sinistro_bb_tb with(nolock) " & _
                    "WHERE sinistro_bb_tb.sinistro_bb = " & txtConteudo
            vTipo = "3"

        Case "Ap�lice"
            If Len(txtConteudo) > 9 Then
                mensagem_erro 6, "N�mero maior que o permitido"
              txtConteudo.Enabled = True
              txtConteudo.SetFocus
              MousePointer = 0
              Exit Sub
            End If
            
            vSql = "SELECT count(apolice_id) FROM apolice_tb with(nolock) " & _
                    "WHERE apolice_tb.apolice_id = " & txtConteudo
            vTipo = "4"

        Case "Proposta"
            If Len(txtConteudo) > 9 Then
                mensagem_erro 6, "N�mero maior que o permitido"
              txtConteudo.Enabled = True
              txtConteudo.SetFocus
              MousePointer = 0
              Exit Sub
            End If
            
            vSql = "SELECT count(proposta_id) FROM proposta_tb with(nolock) " & _
                    "WHERE proposta_tb.proposta_id = " & txtConteudo
            vTipo = "5"

        Case "Proposta BB"
            If Len(txtConteudo) > 9 Then
                mensagem_erro 6, "N�mero maior que o permitido"
              txtConteudo.Enabled = True
              txtConteudo.SetFocus
              MousePointer = 0
              Exit Sub
            End If
            
            If vTpRamo = "1" Then
                vSql = "SELECT count(proposta_id) "
                vSql = vSql & " FROM proposta_adesao_tb p with(nolock) INNER JOIN ramo_tb r with(nolock) ON "
                vSql = vSql & "     p.ramo_id = r.ramo_id "
                vSql = vSql & " WHERE p.proposta_bb = " & txtConteudo
                vSql = vSql & "   AND r.tp_ramo_id = 1 "
            Else
                vSql = "SELECT "
                vSql = vSql & "(SELECT count(proposta_id) "
                vSql = vSql & " FROM proposta_fechada_tb with(nolock) "
                vSql = vSql & " WHERE proposta_fechada_tb.proposta_bb = " & txtConteudo
                vSql = vSql & ") + "
                vSql = vSql & "(SELECT count(proposta_id) "
                vSql = vSql & " FROM proposta_adesao_tb p with(nolock) INNER JOIN ramo_tb r with(nolock) ON "
                vSql = vSql & "     p.ramo_id = r.ramo_id "
                vSql = vSql & " WHERE p.proposta_bb = " & txtConteudo
                vSql = vSql & "   AND r.tp_ramo_id = 2 "
                vSql = vSql & ")"
            End If
            vTipo = "6"

        Case "�ltimo Avaliador"
        
            If Len(txtConteudo) > 20 Then
                mensagem_erro 6, "Nome maior que o permitido"
                txtConteudo.Enabled = True
                txtConteudo.SetFocus
                MousePointer = 0
                Exit Sub
            End If
            If cmbSitSinistroAvaliador.ListIndex = -1 Then
                mensagem_erro 3, "Situa��o do Sinistro"
                cmbSitSinistroAvaliador.SetFocus
                MousePointer = 0
                Exit Sub
            End If
            
            txtConteudo = MudaAspaSimples(txtConteudo)
            vSql = "SELECT count(sinistro_id) FROM sinistro_tb with(nolock) " & _
                    "WHERE sinistro_tb.usuario = '" & txtConteudo & "'" & _
                    "and Situacao = '" & Left(cmbSitSinistroAvaliador.Text, 1) & "'"
            vTipo = "7"
            
        Case "CNPJ"
            If Len(txtConteudo) > 14 Then
                mensagem_erro 6, "N�mero maior que o permitido!"
              txtConteudo.Enabled = True
              txtConteudo.SetFocus
              MousePointer = 0
              Exit Sub
            End If
            If Not CGC_OK(txtConteudo) Then
                mensagem_erro 6, "cgc inv�lido!"
              txtConteudo.Enabled = True
              txtConteudo.SetFocus
              MousePointer = 0
              Exit Sub
            End If
            
            vSql = "SELECT count(cgc) FROM pessoa_juridica_tb with(nolock) " & _
                    "WHERE pessoa_juridica_tb.cgc = '" & txtConteudo & "'"
            vTipo = "8"

        Case "CPF"
            If Len(txtConteudo) > 11 Then
                mensagem_erro 6, "N�mero maior que o permitido"
              txtConteudo.Enabled = True
              txtConteudo.SetFocus
              MousePointer = 0
              Exit Sub
            End If
            If Not CPF_Ok(txtConteudo) Then
                mensagem_erro 6, "cpf inv�lido"
              txtConteudo.Enabled = True
              txtConteudo.SetFocus
              MousePointer = 0
              Exit Sub
            End If
            
            vSql = "SELECT count(cpf) FROM pessoa_fisica_tb with(nolock) " & _
                    "WHERE pessoa_fisica_tb.cpf = '" & txtConteudo & "'"
            vTipo = "9"

        Case "Situa��o Pagamento/Recebimento"
            If cmbSituacao.ListIndex < 1 Then
                mensagem_erro 3, "Situa��o Pagamento/Recebimento"
                cmbSituacao.SetFocus
                MousePointer = 0
                Exit Sub
            End If
            vSql = "SELECT count(sinistro_id) FROM pgto_sinistro_tb with(nolock) " & _
                    "WHERE pgto_sinistro_tb.situacao_op = '" & _
                    cmbSituacao.ItemData(cmbSituacao.ListIndex) & "'"
            vTipo = "10"
    
        Case "Data Acerto"
            If IsNumeric(txtConteudo.Text) = False Or _
                Len(txtConteudo.Text) <> 8 Then
                mensagem_erro 6, "Informe a Data com formato 'ddmmyyyy'!"
                txtConteudo.Enabled = True
                txtConteudo.SetFocus
                MousePointer = 0
                Exit Sub
            End If
            vData = "'" & Right(txtConteudo.Text, 4) & Mid(txtConteudo.Text, 3, 2) & _
                    Left(txtConteudo.Text, 2) & "'"
            vSql = "SELECT count(sinistro_id) FROM pgto_sinistro_tb with(nolock) " & _
                    "WHERE pgto_sinistro_tb.dt_acerto_contas_sinistro = " & _
                    vData
            vTipo = "11"
            
        Case "Voucher"
            If Len(txtConteudo.Text) > 6 Then
                mensagem_erro 6, "N�mero maior que o permitido!"
                txtConteudo.Enabled = True
                txtConteudo.SetFocus
                MousePointer = 0
                Exit Sub
            End If
            vSql = "SELECT count(sinistro_id) " & _
                   "FROM pgto_sinistro_tb with(nolock), ps_acerto_pagamento_tb with(nolock) " & _
                    "WHERE pgto_sinistro_tb.acerto_id = ps_acerto_pagamento_tb.acerto_id " & _
                    "  AND ps_acerto_pagamento_tb.voucher_id = " & txtConteudo
            vTipo = "12"
            
        Case "N� da Autoriza��o"
            If Len(txtConteudo) > 8 Then
              mensagem_erro 6, "N�mero maior que o permitido"
              txtConteudo.Enabled = True
              txtConteudo.SetFocus
              MousePointer = 0
              Exit Sub
            End If
            
            vSql = "SELECT count(sinistro_id) FROM pgto_sinistro_tb with(nolock) " & _
                    "WHERE chave_autorizacao_sinistro = " & txtConteudo
            vTipo = "13"
        
        Case Else               's� vale para voper="I"
            If vTpRamo = "0" Then
                vTipo = "0"     'Est� selecionando tudo
            Else
            vSql = "SELECT count(sinistro_id) FROM pgto_sinistro_tb with(nolock) " & _
                    "WHERE pgto_sinistro_tb.ramo_id  = " & vTpRamo
                    
            vTipo = "1"     'Est� selecionando s� por tipo ramo
            End If
    End Select

    Set rs = rdocn.OpenResultset(vSql)
    If rs.EOF Then
       StatusBar1.SimpleText = _
                "N�o foram encontrados registros para condi��o especificada."
       cmbCampo.SetFocus
       rs.Close
       flexSelecao.Rows = 1
       MousePointer = 0
       Exit Sub
    End If
    rs.Close
            
    If vTipo = "13" Then
        Executa_Selecao_Autorizacao
    Else
        Executa_Selecao
    End If
    MousePointer = 0

End Sub

Private Sub cmbCampo_Click()
    
    cmbSituacao.ListIndex = 0
    txtConteudo.Width = 6015
    
    flexSelecao.Visible = True
    '--------------------
    'Luciana - 03/10/2003
    '--------------------
    bFlexSelecao = True
    '--------------------
    flexAutorizacao.Visible = False
    LblSitSinistroAvaliador.Visible = False
    cmbSitSinistroAvaliador.Visible = False
    cmbSitSinistroAvaliador.ListIndex = -1
    cmbSituacao.Visible = True
    Label3(0).Visible = True
    If cmbCampo.Text = "Pendentes de Aprova��o" Then
        Label3(0).Visible = False
        cmbSituacao.Visible = False
        txtConteudo.Visible = False
        lblConteudo.Visible = False
    ElseIf cmbCampo.Text = "Situa��o Pagamento/Recebimento" Then
        cmbSituacao.List(0) = ""
        txtConteudo.Visible = False
        lblConteudo.Visible = False
    ElseIf cmbCampo.Text = "N� da Autoriza��o" Then
        flexAutorizacao.Visible = True
        flexAutorizacao.ColWidth(5) = 0
        flexSelecao.Visible = False
        '--------------------
        'Luciana - 03/10/2003
        '--------------------
        bFlexSelecao = False
        '--------------------
        txtConteudo.Visible = True
        lblConteudo.Visible = True
        cmbSituacao.Visible = False
        Label3(0).Visible = False
    Else
        cmbSituacao.List(0) = "Todas"
        txtConteudo.Visible = True
        lblConteudo.Visible = True
        If cmbCampo.Text = "�ltimo Avaliador" Then
            LblSitSinistroAvaliador.Visible = True
            cmbSitSinistroAvaliador.Visible = True
            txtConteudo.Width = 3135
        End If
    End If

End Sub

Private Sub flexAutorizacao_Click()

    selecionou = True

End Sub

Private Sub flexAutorizacao_DblClick()

    If flexAutorizacao.Rows > 1 Then
        selecionou = True
        BtnOk_Click
    End If

End Sub

Private Sub flexSelecao_DblClick()

    If flexSelecao.Rows > 1 Then
        If SinistrosSelecionados.Count > 0 Then
            selecionou = True
        Else
            selecionou = False
        End If
        BtnOk_Click
    End If

End Sub

Private Sub Form_Activate()

    Dim rs As rdoResultset
    Dim i As Byte

    sDecimal = LeArquivoIni("WIN.INI", "intl", "sDecimal")
    If sDecimal = "." Then
        'A parte de pagamento n�o est� preparada para ponto decimal
        mensagem_erro 6, "Este m�dulo n�o pode ser usado com configura��o diferente da brasileira."
        Unload Me
        Exit Sub
    End If
    
    voper = Left(gsParamAplicacao, 1)
    'Como o par�metro teve que ser incrementado com mais uma letra devido ao co-seguro,
    'foi criado mais uma vari�vel pra guardar se � co-seguro ou n�o.
    If Len(gsParamAplicacao) = 2 Then
        vopercoseguro = Right(gsParamAplicacao, 1)
    ElseIf Len(gsParamAplicacao) > 2 Then
          voper = "IS"
          For i = 0 To cmbCampo.ListCount - 1
            If UCase(cmbCampo.Text) = UCase("N�MERO SINISTRO") Then
              Exit For
            End If
            cmbCampo.ListIndex = i
          Next i
          txtConteudo.Text = Trim(Mid(gsParamAplicacao, 4, 11))
          
          '--------------------------------------------------------------------------------------------------------------
          'Luciana - 19/05/2003 - In�cio - Estava obtendo o tipo de ramo incorreto, fun��o inclu�da para buscar o correto
          '--------------------------------------------------------------------------------------------------------------
          SQS = "select tp_ramo_id from ramo_tb with(nolock) where ramo_id = " & Trim(Mid(gsParamAplicacao, 15, 3))
          Set rs = rdocn.OpenResultset(SQS)
          If Not rs.EOF Then
            If rs(0) = 1 Then
                optVida.Value = True
            Else
                optRE.Value = True
            End If
          End If
          '-------------------------------
          'Luciana - 19/05/2003 - Fim
          '-------------------------------
    End If
    
    vData = "NULL"
    If voper = "I" Or voper = "IS" Then
        'Executa_Selecao
        If voper = "I" Then
            'optVida.Value = False
            'optRE.Value = False
            'btnPesquisar.SetFocus
            With txtConteudo
              .Text = Left(Trim(.Text), 6)
              .SelStart = 7
              If .Visible And .Enabled Then
                .SetFocus
              End If
            End With
        ElseIf voper <> "IS" Then
            optVida.SetFocus
        End If
    End If
    cmbSituacao.ListIndex = 0
    
    flexSelecao.Row = 0
    flexAutorizacao.Row = 0
    
    If voper = "IS" Then
      btnPesquisar.Value = True
      flexSelecao.Visible = True
      '--------------------
      'Luciana - 03/10/2003
      '--------------------
      bFlexSelecao = True
      '--------------------
      flexSelecao_DblClick
    End If
    
End Sub

Private Sub Form_Load()
    
    CentraFrm frmAcertoCon
    
    cmbCampo.Clear
    selecionou = False
    FlagRetorno = False
    flexSelecao.Rows = 1
    
    cmbCampo.AddItem "Pendentes de Aprova��o"
    cmbCampo.AddItem "Data Acerto"
    cmbCampo.AddItem "N�mero Sinistro"
    cmbCampo.AddItem "N�mero Sinistro BB"
    cmbCampo.AddItem "Ap�lice"
    cmbCampo.AddItem "Proposta"
    cmbCampo.AddItem "Proposta BB"
    cmbCampo.AddItem "CPF"
    cmbCampo.AddItem "CNPJ"
    cmbCampo.AddItem "�ltimo Avaliador"
    
    Select Case gsParamAplicacao
        Case "I"
            Caption = "SEG12018 - Aprovar Pagamento - " & Ambiente
            
        Case "R"
            Caption = "SEG12018 - Reapresentar Pagamento - " & Ambiente
            
        Case "A"
            Caption = "SEG12018 - Alterar Situa��o Pagamento/Recebimento - " & Ambiente
            cmbCampo.AddItem "Situa��o Pagamento/Recebimento"
            frameSituacao.Visible = True
            
        Case "C"
            Caption = "SEG12018 - Consultar Pagamentos/Recebimentos - " & Ambiente
            cmbCampo.AddItem "Situa��o Pagamento/Recebimento"
            cmbCampo.AddItem "Voucher"
            frameSituacao.Visible = True
        
        Case "IC"
            cmbCampo.AddItem "N� da Autoriza��o"
            Caption = "SEG12018 - Aprovar Pagamento Co-seguro - " & Ambiente
            
        Case "RC"
            cmbCampo.AddItem "N� da Autoriza��o"
            Caption = "SEG12018 - Reapresentar Pagamento Co-seguro - " & Ambiente
            
        Case "AC"
            cmbCampo.AddItem "N� da Autoriza��o"
            Caption = "SEG12018 - Alterar Situa��o Pagamento/Recebimento Co-seguro - " & Ambiente
            cmbCampo.AddItem "Situa��o Pagamento/Recebimento"
            frameSituacao.Visible = True
            
        Case "CC"
            Caption = "SEG12018 - Consultar Pagamentos/Recebimentos Co-seguro - " & Ambiente
            cmbCampo.AddItem "Situa��o Pagamento/Recebimento"
            cmbCampo.AddItem "Voucher"
            cmbCampo.AddItem "N� da Autoriza��o"
            frameSituacao.Visible = True
            
    End Select
    cmbCampo.ListIndex = -1
    
    vTipo = "0"
    vTpRamo = "0"
    vSituacao = "'0'"
    
    If Left(gsParamAplicacao, 1) = "I" Then
        flexSelecao.FormatString = "   |Sinistro           |Segurado                                                       |Ap�lice|Proposta|Valor total                  |Produto                                         |Ramo                              |Situa��o Sinistro                           |Endosso  | Sinistro da L�der"
    Else
        flexSelecao.FormatString = "   |Sinistro           |Segurado                                                       |Ap�lice|Proposta|Sucursal  |Produto                                         |Ramo                              |Situa��o Sinistro                           |Endosso  | Sinistro da L�der"
    End If
    
    cmbSituacao.ListIndex = 0
    
    vData = "NULL"
    voper = Left(gsParamAplicacao, 1)
    
    'Obtendo o c�digo do t�cnico.
    cod_tecnico = Obtem_Codigo_Tecnico
    'Alan Neves - MP - 12/03/2013 - Fim
End Sub

Private Sub flexSelecao_Click()
    If SinistrosSelecionados.Count > 0 Then
        selecionou = True
    Else
        selecionou = False
    End If
End Sub

Public Sub Desmonta_Selecao()

    Dim i As Integer

    'Desmonta a colecao se selecionados
    i = SinistrosSelecionados.Count
    While SinistrosSelecionados.Count > 0
        SinistrosSelecionados.Remove i
        i = i - 1
    Wend
    flexSelecao.Rows = 1
    lblSelecionados = ""
    
End Sub

Private Sub Executa_Selecao_Autorizacao()

    Dim rs As rdoResultset, vSql As String
    Dim linha As String
    Dim sinsel As New Sinist
    Dim i As Integer, temp As Integer

' cristovao.rodrigues 28/01/2013 passar a chamar a proc

''    vSql = "SELECT pgto.chave_autorizacao_sinistro, pgto.chave_autorizacao_sinistro_dv, benef.cong_cod_susep, " & vbNewLine
''    vSql = vSql & " seg.nome seg_nome, SUM(pgto.val_acerto + pgto.val_correcao) val_total, m.sigla, forma.nome forma_nome, m.moeda_id" & vbNewLine
''    vSql = vSql & " FROM sinistro_tb sin with(nolock) " & vbNewLine
''    vSql = vSql & " INNER JOIN (pgto_sinistro_tb pgto with(nolock) " & vbNewLine
''    vSql = vSql & "     INNER JOIN (sinistro_benef_congenere_tb benef with(nolock) " & vbNewLine
''    vSql = vSql & "          INNER JOIN seguradora_tb seg with(nolock) " & vbNewLine
''    vSql = vSql & "              ON seg.seguradora_cod_susep = benef.cong_cod_susep ) " & vbNewLine
''    vSql = vSql & "         ON benef.sinistro_id = pgto.sinistro_id " & vbNewLine
''    vSql = vSql & "         AND benef.apolice_id = pgto.apolice_id " & vbNewLine
''    vSql = vSql & "         AND benef.seguradora_cod_susep = pgto.seguradora_cod_susep " & vbNewLine
''    vSql = vSql & "         AND benef.sucursal_seguradora_id = pgto.sucursal_seguradora_id " & vbNewLine
''    vSql = vSql & "         AND benef.ramo_id = pgto.ramo_id " & vbNewLine
''    vSql = vSql & "         AND benef.beneficiario_id = pgto.beneficiario_id " & vbNewLine
''    vSql = vSql & "     INNER JOIN forma_pgto_tb forma with(nolock) " & vbNewLine
''    vSql = vSql & "         ON pgto.forma_pgto_id = forma.forma_pgto_id ) " & vbNewLine
''    vSql = vSql & "     ON sin.sinistro_id = pgto.sinistro_id " & vbNewLine
''    vSql = vSql & "    AND sin.apolice_id = pgto.apolice_id " & vbNewLine
''    vSql = vSql & "    AND sin.seguradora_cod_susep = pgto.seguradora_cod_susep " & vbNewLine
''    vSql = vSql & "    AND sin.sucursal_seguradora_id = pgto.sucursal_seguradora_id " & vbNewLine
''    vSql = vSql & "    AND sin.ramo_id = pgto.ramo_id " & vbNewLine
''    vSql = vSql & " INNER JOIN ramo_tb ramo with(nolock) " & vbNewLine
''    vSql = vSql & "     ON ramo.ramo_id = sin.ramo_id " & vbNewLine
''    vSql = vSql & " INNER JOIN moeda_tb m with(nolock) " & vbNewLine
''    vSql = vSql & "     ON m.moeda_id = sin.moeda_id " & vbNewLine
''    vSql = vSql & " WHERE pgto.chave_autorizacao_sinistro = " & IIf(txtConteudo = "", 0, txtConteudo) & vbNewLine
''    vSql = vSql & "   AND ramo.tp_ramo_id = " & vTpRamo & vbNewLine
''    If voper = "R" Then
''        vSql = vSql & "   AND pgto.situacao_op = 'r' " & vbNewLine
''    ElseIf voper = "I" Then
''        vSql = vSql & "   AND pgto.situacao_op = 'n' " & vbNewLine
''    End If
''    vSql = vSql & " GROUP BY pgto.chave_autorizacao_sinistro, pgto.chave_autorizacao_sinistro_dv, " & vbNewLine
''    vSql = vSql & " benef.cong_cod_susep, seg.nome, m.sigla, forma.nome, m.moeda_id" & vbNewLine
    
    'cristovao.rodrigues 28/01/2013 select substituido pela proc
             
     vSql = "EXEC seguros_db.dbo.segs10828_sps " & _
            IIf(txtConteudo = "", 0, txtConteudo) & "," & vTpRamo & ",'" & voper & "'"
    
    Set rs = rdocn.OpenResultset(vSql)
    If Not rs.EOF Then
       StatusBar1.SimpleText = "Clique no registro desejado para exibi��o dos dados."
    Else
       StatusBar1.SimpleText = "N�o foram encontrados registros para condi��o especificada."
       cmbCampo.SetFocus
    End If

    vSituacao = "'0'"
    
    flexAutorizacao.Rows = 1

    While Not rs.EOF
        i = i + 1
        linha = Str(rs("chave_autorizacao_sinistro")) & "-" & rs("chave_autorizacao_sinistro_dv") & vbTab
        linha = linha & Str(rs("cong_cod_susep")) & "-" & rs("seg_nome") & vbTab
        linha = linha & Format(Val(rs("val_total")), "###,###,###,##0.00") & vbTab
        linha = linha & rs("sigla") & vbTab
        linha = linha & rs("forma_nome") & vbTab
        linha = linha & rs("moeda_id")
        
        flexAutorizacao.AddItem linha
        
        rs.MoveNext
    Wend
    
    If Str(flexAutorizacao.Rows - 1) > 0 Then
        lblSelecionados = Str(flexAutorizacao.Rows - 1) & " Registro(s) Selecionado(s)"
    Else
        If flexAutorizacao.Rows = 1 Then
           StatusBar1.SimpleText = _
                    "N�o exite(m) item(s) para esta sele��o."
           cmbCampo.SetFocus
        End If
    End If
    flexAutorizacao.Refresh
    MousePointer = 0

    selecionou = False

End Sub

Private Sub Executa_Selecao()

    Dim rs As rdoResultset, Rs1 As rdoResultset
    Dim linha As String, valor_total As String
    Dim sinsel As New Sinist
    Dim i As Integer, temp As Integer

    If voper <> "R" Then
        ' Se nenhuma Situacao de Pgto foi informada vSituacao = 0
        ' e dentro da SP faz filtro de acordo com o Tipo de operacao
        If frameSituacao.Visible = False And voper <> "IS" Then
            vSituacao = "'0'"
        Else
            If voper = "IS" Then
                vSituacao = "'n'"
            ElseIf cmbSituacao.ListIndex < 1 Then
                vSituacao = "'0'"
            Else
                Select Case cmbSituacao.ListIndex
                Case 1
                    vSituacao = "'A'"
                Case 2
                    vSituacao = "'N'"
                Case 3
                    vSituacao = "'C'"
                Case 4
                    vSituacao = "'E'"
                Case 5
                    vSituacao = "'R'"
                End Select
            End If
        End If
    Else
        vSituacao = "'R'"
    End If
    
    If cmbSitSinistroAvaliador.Visible Then
        vSituacao_Sinistro = cmbSitSinistroAvaliador.ListIndex
    Else
        'Nao aplica o filtro de Situacao do Sinistro
        vSituacao_Sinistro = "n"
    End If
    
' Alan Neves - Inicio - Demanda: 17770127 - Melhoria de performance - MP
'    vSql = "EXEC sinistro_sel_acerto_sps " & _
'            vTipo & "," & vTpRamo & ",'"
    
    vSql = "EXEC SEGUROS_DB.dbo.SEGS10815_SPS " & _
            vTipo & "," & vTpRamo & ",'"
' Alan Neves - Fim
    
    If cmbCampo.Text <> "Voucher" Then
        vSql = vSql & IIf(txtConteudo = "", 0, txtConteudo) & "', 0,"
    Else
        vSql = vSql & "', " & IIf(txtConteudo = "", 0, txtConteudo) & ","
    End If
    
    ' Alan Neves - Inicio - Demanda: 17770127 - Melhoria de performance - MP
    'vSql = vSql & vSituacao & "," & vData & ",'" & vSituacao_Sinistro & "','" & voper & "'"
    vSql = vSql & vSituacao & "," & vData & ",'" & vSituacao_Sinistro & "','" & voper & "','" & vopercoseguro & "'"
    ' Alan Neves - Fim
    
    Set rs = rdocn.OpenResultset(vSql)
    
    If Not rs.EOF Then
       StatusBar1.SimpleText = "Clique no registro desejado para exibi��o dos dados."
    Else
       StatusBar1.SimpleText = "N�o foram encontrados registros para condi��o especificada."
       cmbCampo.SetFocus
    End If
    
    i = 0
    If SinistrosSelecionados.Count > 0 Then
        Desmonta_Selecao
    End If
    
' Alan Neves - Inicio - Demanda: 17770127 - Melhoria de performance - MP
'    Dim blnNaoEncontrouValorTotal As Boolean
     
    flexSelecao.Rows = 1
    While Not rs.EOF
        i = i + 1
        
        If vopercoseguro = "C" Then
           flexSelecao.Row = flexSelecao.Rows - 1
             If voper = "I" Or voper = "IS" Then
                valor_total = rs(22) & " " & Format(Val(rs(23)), "###,###,###,##0.00")
                
                linha = IIf(rs(0) <> "s", "", "*") & vbTab & rs(14) & vbTab & rs(12) & " " & rs(18) & vbTab & _
                        rs(1) & vbTab & rs(6) & vbTab & valor_total & vbTab & rs(9) & "-" & rs(10) & vbTab & rs(4) & "-" & rs(5) & vbTab & rs(16) & _
                        vbTab & IIf(IsNull(rs(19)), "0", rs(19)) & vbTab & rs(20)
             Else
                linha = IIf(rs(0) <> "s", "", "*") & vbTab & rs(14) & vbTab & rs(12) & " " & rs(18) & vbTab & _
                        rs(1) & vbTab & rs(6) & vbTab & rs(2) & vbTab & rs(9) & "-" & rs(10) & vbTab & rs(4) & "-" & rs(5) & vbTab & rs(16) & _
                        vbTab & IIf(IsNull(rs(19)), "0", rs(19)) & vbTab & rs(20)
             End If
        
             flexSelecao.AddItem linha
                
            For temp = 0 To flexSelecao.Cols - 1
                flexSelecao.Row = flexSelecao.Rows - 1
                flexSelecao.Col = temp
                flexSelecao.CellAlignment = 1
            Next temp
    
            Set sinsel = New Sinist
            With sinsel
                .Apolice_id = rs(1)
                .Sucursal_id = rs(2)
                .Seguradora_id = rs(3)
                .ramo_id = rs(4)
                .NomeRamo = rs(5)
                .Proposta_id = rs(6)
                .NumEndosso = IIf(IsNull(rs(7)), "", rs(7))
                .SitProposta = rs(8)
                .produto_id = rs(9)
                .NomeProduto = rs(10)
                .Cliente_id = rs(11)
                .NomeCliente = rs(12)
                .Tipo = rs(13)
                .sinistro_id = rs(14)
                .Situacao = rs(15)
                .SituacaoDesc = rs(16)
            End With
            SinistrosSelecionados.Add sinsel
        Else
           flexSelecao.Row = flexSelecao.Rows - 1
            
            If voper = "I" Or voper = "IS" Then
               valor_total = rs(22) & " " & Format(Val(rs(23)), "###,###,###,##0.00")
       
               linha = IIf(rs(0) <> "s", "", "*") & vbTab & rs(14) & vbTab & rs(12) & " " & rs(18) & vbTab & _
                       rs(1) & vbTab & rs(6) & vbTab & valor_total & vbTab & rs(9) & "-" & rs(10) & vbTab & rs(4) & "-" & rs(5) & vbTab & rs(16) & _
                       vbTab & IIf(IsNull(rs(19)), "0", rs(19)) & vbTab & rs(20)
            Else
               linha = IIf(rs(0) <> "s", "", "*") & vbTab & rs(14) & vbTab & rs(12) & " " & rs(18) & vbTab & _
                       rs(1) & vbTab & rs(6) & vbTab & rs(2) & vbTab & rs(9) & "-" & rs(10) & vbTab & rs(4) & "-" & rs(5) & vbTab & rs(16) & _
                       vbTab & IIf(IsNull(rs(19)), "0", rs(19)) & vbTab & rs(20)
            End If
        
            flexSelecao.AddItem linha
                
            For temp = 0 To flexSelecao.Cols - 1
                flexSelecao.Row = flexSelecao.Rows - 1
                flexSelecao.Col = temp
                flexSelecao.CellAlignment = 1
            Next temp
            'Monta cole��o com os selecionados
            Set sinsel = New Sinist
            With sinsel
                .Apolice_id = rs(1)
                .Sucursal_id = rs(2)
                .Seguradora_id = rs(3)
                .ramo_id = rs(4)
                .NomeRamo = rs(5)
                .Proposta_id = rs(6)
                .NumEndosso = IIf(IsNull(rs(7)), "", rs(7))
                .SitProposta = rs(8)
                .produto_id = rs(9)
                .NomeProduto = rs(10)
                .Cliente_id = rs(11)
                .NomeCliente = rs(12)
                .Tipo = rs(13)
                .sinistro_id = rs(14)
                .Situacao = rs(15)
                .SituacaoDesc = rs(16)
            End With
            SinistrosSelecionados.Add sinsel
        End If
        rs.MoveNext
    Wend
  
' Alan Neves - Fim
        
' Alan Neves - Inicio - Demanda: 17770127 - Melhoria de performance - MP

'        blnNaoEncontrouValorTotal = False
'        If vopercoseguro = "C" Then
'            If ValidaSinistroCoseguro(rs(1), rs(2), rs(3), rs(4)) Then
'                flexSelecao.Row = flexSelecao.Rows - 1
'                If voper = "I" Or voper = "IS" Then
'                    vSql = "SELECT m.sigla, SUM(p.val_acerto + p.val_correcao) val_total "
'                    vSql = vSql & " FROM sinistro_tb s with(nolock) "
'                    vSql = vSql & " INNER JOIN pgto_sinistro_tb p with(nolock) ON "
'                    vSql = vSql & "       p.sinistro_id = s.sinistro_id "
'                    vSql = vSql & "   AND p.apolice_id = s.apolice_id "
'                    vSql = vSql & "   AND p.seguradora_cod_susep = s.seguradora_cod_susep "
'                    vSql = vSql & "   AND p.sucursal_seguradora_id = s.sucursal_seguradora_id "
'                    vSql = vSql & "   AND p.ramo_id = s.ramo_id "
'                    vSql = vSql & " INNER JOIN moeda_tb m with(nolock) ON "
'                    vSql = vSql & "       s.moeda_id = m.moeda_id "
'                    vSql = vSql & " WHERE s.sinistro_id = " & rs(14)
'                    vSql = vSql & "   AND s.apolice_id = " & rs(1)
'                    vSql = vSql & "   AND s.seguradora_cod_susep = " & rs(3)
'                    vSql = vSql & "   AND s.sucursal_seguradora_id = " & rs(2)
'                    vSql = vSql & "   AND s.ramo_id = " & rs(4)
'                    vSql = vSql & "   AND p.situacao_op = 'n' "
'                    vSql = vSql & "   AND p.chave_autorizacao_sinistro IS NOT NULL "
'                    vSql = vSql & " GROUP BY m.sigla "
'
'                    Set Rs1 = rdocn1.OpenResultset(vSql)
'                    'Flow 275932 - Inibi��o da mensagem no current row
'                    'Joana Agapito - G&P
'                    'valor_total = Rs1("sigla") & " " & Format(Val(Rs1("val_total")), "###,###,###,##0.00")
'                    If Not Rs1.EOF Then
'                        valor_total = Rs1("sigla") & " " & Format(Val(Rs1("val_total")), "###,###,###,##0.00")
'                    Else
'                        blnNaoEncontrouValorTotal = True
'                    End If
'                    Rs1.Close
'
'                    linha = IIf(rs(0) <> "s", "", "*") & vbTab & rs(14) & vbTab & rs(12) & " " & rs(18) & vbTab & _
'                            rs(1) & vbTab & rs(6) & vbTab & valor_total & vbTab & rs(9) & "-" & rs(10) & vbTab & rs(4) & "-" & rs(5) & vbTab & rs(16) & _
'                            vbTab & IIf(IsNull(rs(19)), "0", rs(19)) & vbTab & rs(20)
'                Else
'                    linha = IIf(rs(0) <> "s", "", "*") & vbTab & rs(14) & vbTab & rs(12) & " " & rs(18) & vbTab & _
'                            rs(1) & vbTab & rs(6) & vbTab & rs(2) & vbTab & rs(9) & "-" & rs(10) & vbTab & rs(4) & "-" & rs(5) & vbTab & rs(16) & _
'                            vbTab & IIf(IsNull(rs(19)), "0", rs(19)) & vbTab & rs(20)
'                End If

'                If (blnNaoEncontrouValorTotal = False) Then
'                    flexSelecao.AddItem linha
'                    For temp = 0 To flexSelecao.Cols - 1
'                        flexSelecao.Row = flexSelecao.Rows - 1
'                        flexSelecao.Col = temp
'                        flexSelecao.CellAlignment = 1
'                    Next temp
'                    'Monta cole��o com os selecionados
'                    Set sinsel = New Sinist
'                    With sinsel
'                        .Apolice_id = rs(1)
'                        .Sucursal_id = rs(2)
'                        .Seguradora_id = rs(3)
'                        .ramo_id = rs(4)
'                        .NomeRamo = rs(5)
'                        .Proposta_id = rs(6)
'                        .NumEndosso = IIf(IsNull(rs(7)), "", rs(7))
'                        .SitProposta = rs(8)
'                        .produto_id = rs(9)
'                        .NomeProduto = rs(10)
'                        .Cliente_id = rs(11)
'                        .NomeCliente = rs(12)
'                        .Tipo = rs(13)
'                        .sinistro_id = rs(14)
'                        .Situacao = rs(15)
'                        .SituacaoDesc = rs(16)
'                    End With
'                    SinistrosSelecionados.Add sinsel
'                End If
'            End If
'        Else
'            If Not ValidaSinistroCoseguro(rs(1), rs(2), rs(3), rs(4)) Then
'                flexSelecao.Row = flexSelecao.Rows - 1
'
'                If voper = "I" Or voper = "IS" Then
' O comentario abaixo j� existia


' Data     : 07/01/2009     Analista : Eric Marcel Bernardo - GPTI
                    ' Objetivo : FLOW 684336 - Remover registros Ramo Vida que est�o sendo apresentados ao buscar Ramos Elementares
                    ' 1 - Adicionado as condi��es para fazer os selects dependendo do tipo de ramo(Vida ou RE)
                    ' If vTpRamo = 1 Then
                    '   SQL
                    ' ElseIf vTpRamo = 2 Then
                    '   SQL
                    ' Else
                    '   SQL
                    ' End If
                    ' 2 - Adicionado o filtro no SQL abaixo, contemplando somente o que for tp_ramo_id = 2 (Ramos Elementares)
                    ' vSql = vSql & " INNER JOIN ramo_tb r (nolock) ON "
                    ' vSql = vSql & " s.ramo_id = r.ramo_id "
                    ' vSql = vSql & " AND r.tp_ramo_id = 2 "
                    
' Alan Neves  - Demanda: 17770127 - Melhoria de performance
'                    If vTpRamo = "1" Then
'                        vSql = "SELECT m.sigla, SUM(p.val_acerto + p.val_correcao) val_total "
'                        vSql = vSql & " FROM sinistro_tb s with(nolock) "
'                        vSql = vSql & " INNER JOIN pgto_sinistro_tb p with(nolock) ON "
'                        vSql = vSql & "       p.sinistro_id = s.sinistro_id "
'                        vSql = vSql & "   AND p.apolice_id = s.apolice_id "
'                        vSql = vSql & "   AND p.seguradora_cod_susep = s.seguradora_cod_susep "
'                        vSql = vSql & "   AND p.sucursal_seguradora_id = s.sucursal_seguradora_id "
'                        vSql = vSql & "   AND p.ramo_id = s.ramo_id "
'                        vSql = vSql & " INNER JOIN moeda_tb m with(nolock) ON "
'                        vSql = vSql & "       s.moeda_id = m.moeda_id "
'                        vSql = vSql & " INNER JOIN ramo_tb r with(nolock) ON "
'                        vSql = vSql & " s.ramo_id = r.ramo_id "
'                        vSql = vSql & " WHERE s.sinistro_id = " & rs(14)
'                        vSql = vSql & "   AND s.apolice_id = " & rs(1)
'                        vSql = vSql & "   AND s.seguradora_cod_susep = " & rs(3)
'                        vSql = vSql & "   AND s.sucursal_seguradora_id = " & rs(2)
'                        vSql = vSql & "   AND s.ramo_id = " & rs(4)
'                        vSql = vSql & "   AND p.situacao_op = 'n' "
'                        vSql = vSql & " AND r.tp_ramo_id = 1 "
'                        vSql = vSql & "   AND p.chave_autorizacao_sinistro IS NOT NULL "
'                        vSql = vSql & " GROUP BY m.sigla "
'                    ElseIf vTpRamo = "2" Then
'                         vSql = "SELECT m.sigla, SUM(p.val_acerto + p.val_correcao) val_total "
'                        vSql = vSql & " FROM sinistro_tb s with(nolock) "
'                        vSql = vSql & " INNER JOIN pgto_sinistro_tb p with(nolock) ON "
'                        vSql = vSql & "       p.sinistro_id = s.sinistro_id "
'                        vSql = vSql & "   AND p.apolice_id = s.apolice_id "
'                        vSql = vSql & "   AND p.seguradora_cod_susep = s.seguradora_cod_susep "
'                        vSql = vSql & "   AND p.sucursal_seguradora_id = s.sucursal_seguradora_id "
'                        vSql = vSql & "   AND p.ramo_id = s.ramo_id "
'                        vSql = vSql & " INNER JOIN moeda_tb m with(nolock) ON "
'                        vSql = vSql & "       s.moeda_id = m.moeda_id "
'                        vSql = vSql & " INNER JOIN ramo_tb r with(nolock) ON "
'                        vSql = vSql & " s.ramo_id = r.ramo_id "
'                        vSql = vSql & " WHERE s.sinistro_id = " & rs(14)
'                        vSql = vSql & "   AND s.apolice_id = " & rs(1)
'                        vSql = vSql & "   AND s.seguradora_cod_susep = " & rs(3)
'                        vSql = vSql & "   AND s.sucursal_seguradora_id = " & rs(2)
'                        vSql = vSql & "   AND s.ramo_id = " & rs(4)
'                        vSql = vSql & "   AND p.situacao_op = 'n' "
'                        vSql = vSql & " AND r.tp_ramo_id = 2 "
'                        vSql = vSql & "   AND p.chave_autorizacao_sinistro IS NOT NULL "
'                        vSql = vSql & " GROUP BY m.sigla "
'                    Else
'                         vSql = "SELECT m.sigla, SUM(p.val_acerto + p.val_correcao) val_total "
'                        vSql = vSql & " FROM sinistro_tb s with(nolock) "
'                        vSql = vSql & " INNER JOIN pgto_sinistro_tb p with(nolock) ON "
'                        vSql = vSql & "       p.sinistro_id = s.sinistro_id "
'                        vSql = vSql & "   AND p.apolice_id = s.apolice_id "
'                        vSql = vSql & "   AND p.seguradora_cod_susep = s.seguradora_cod_susep "
'                        vSql = vSql & "   AND p.sucursal_seguradora_id = s.sucursal_seguradora_id "
'                        vSql = vSql & "   AND p.ramo_id = s.ramo_id "
'                        vSql = vSql & " INNER JOIN moeda_tb m with(nolock) ON "
'                        vSql = vSql & "       s.moeda_id = m.moeda_id "
'                        vSql = vSql & " WHERE s.sinistro_id = " & rs(14)
'                        vSql = vSql & "   AND s.apolice_id = " & rs(1)
'                        vSql = vSql & "   AND s.seguradora_cod_susep = " & rs(3)
'                        vSql = vSql & "   AND s.sucursal_seguradora_id = " & rs(2)
'                        vSql = vSql & "   AND s.ramo_id = " & rs(4)
'                        vSql = vSql & "   AND p.situacao_op = 'n' "
'                        vSql = vSql & "   AND p.chave_autorizacao_sinistro IS NOT NULL "
'                        vSql = vSql & " GROUP BY m.sigla "
'                    End If
'                    Set Rs1 = rdocn1.OpenResultset(vSql)



                    'Flow 275932 - Inibi��o da mensagem no current row
                    'Joana Agapito - G&P
                    'valor_total = Rs1("sigla") & " " & Format(Val(Rs1("val_total")), "###,###,###,##0.00")
                    
                    
' Alan Neves - Inicio - Demanda: 17770127 - Melhoria de performance - MP
'                    If Not Rs1.EOF Then
'                        valor_total = Rs1("sigla") & " " & Format(Val(Rs1("val_total")), "###,###,###,##0.00")
'                    Else
'                        blnNaoEncontrouValorTotal = True
'                    End If
'                    Rs1.Close
'
'                    linha = IIf(rs(0) <> "s", "", "*") & vbTab & rs(14) & vbTab & rs(12) & " " & rs(18) & vbTab & _
'                            rs(1) & vbTab & rs(6) & vbTab & valor_total & vbTab & rs(9) & "-" & rs(10) & vbTab & rs(4) & "-" & rs(5) & vbTab & rs(16) & _
'                            vbTab & IIf(IsNull(rs(19)), "0", rs(19)) & vbTab & rs(20)
'                Else
'                    linha = IIf(rs(0) <> "s", "", "*") & vbTab & rs(14) & vbTab & rs(12) & " " & rs(18) & vbTab & _
'                            rs(1) & vbTab & rs(6) & vbTab & rs(2) & vbTab & rs(9) & "-" & rs(10) & vbTab & rs(4) & "-" & rs(5) & vbTab & rs(16) & _
'                            vbTab & IIf(IsNull(rs(19)), "0", rs(19)) & vbTab & rs(20)
'                End If
'                If blnNaoEncontrouValorTotal = False Then
'                    flexSelecao.AddItem linha
'                    For temp = 0 To flexSelecao.Cols - 1
'                        flexSelecao.Row = flexSelecao.Rows - 1
'                        flexSelecao.Col = temp
'                        flexSelecao.CellAlignment = 1
'                    Next temp
'                    'Monta cole��o com os selecionados
'                    Set sinsel = New Sinist
'                    With sinsel
'                        .Apolice_id = rs(1)
'                        .Sucursal_id = rs(2)
'                        .Seguradora_id = rs(3)
'                        .ramo_id = rs(4)
'                        .NomeRamo = rs(5)
'                        .Proposta_id = rs(6)
'                        .NumEndosso = IIf(IsNull(rs(7)), "", rs(7))
'                        .SitProposta = rs(8)
'                        .produto_id = rs(9)
'                        .NomeProduto = rs(10)
'                        .Cliente_id = rs(11)
'                        .NomeCliente = rs(12)
'                        .Tipo = rs(13)
'                        .sinistro_id = rs(14)
'                        .Situacao = rs(15)
'                        .SituacaoDesc = rs(16)
'                    End With
'                    SinistrosSelecionados.Add sinsel
'                End If
'            End If
'        End If
'        rs.MoveNext


'    Wend
' Alan Neves - Fim
    
    If voper = "IS" Then
      For i = 1 To SinistrosSelecionados.Count
        If Not (SinistrosSelecionados(i).Apolice_id = Val(Mid(gsParamAplicacao, 18, 9)) And _
          SinistrosSelecionados(i).sinistro_id = Val(Mid(gsParamAplicacao, 4, 11)) And _
          SinistrosSelecionados(i).ramo_id = Val(Mid(gsParamAplicacao, 15, 3)) And _
          SinistrosSelecionados(i).Sucursal_id = Val(Mid(gsParamAplicacao, 27, 5)) And _
          SinistrosSelecionados(i).Seguradora_id = Val(Mid(gsParamAplicacao, 32, 5))) Then
            SinistrosSelecionados.Remove (i)
        End If
      Next i
      If SinistrosSelecionados.Count < 1 Then
        End
      End If
    End If
    
    If Str(flexSelecao.Rows - 1) > 0 Then
        lblSelecionados = Str(flexSelecao.Rows - 1) & " Registro(s) Selecionado(s)"
    Else
        If flexSelecao.Rows = 1 Then
           StatusBar1.SimpleText = _
                    "N�o existe(m) item(s) para esta sele��o."
           If voper <> "IS" Then
             cmbCampo.SetFocus
           End If
        End If
    End If
    flexSelecao.Refresh
    MousePointer = 0
    
    selecionou = False

End Sub


Private Sub optRE_Click()
'    Desmonta_Selecao
    
'    cmbCampo.ListIndex = -1
'    cmbSituacao.ListIndex = 0
'    txtConteudo = ""
End Sub

Private Sub optVida_Click()
 '   Desmonta_Selecao
    
 '   cmbCampo.ListIndex = -1
 '   cmbSituacao.ListIndex = 0
 '   txtConteudo = ""
End Sub

Private Sub txtConteudo_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        btnPesquisar_Click
    End If
End Sub

Private Function ValidaSinistroCoseguro(Apolice As String, Sucursal As String, Seguradora As String, ramo As Integer) As Boolean
Dim vSql As String
Dim rs As rdoResultset

    ValidaSinistroCoseguro = False

    vSql = "SELECT tp_emissao FROM Apolice_tb with(nolock) " & _
            "WHERE apolice_id = " & Apolice & _
            " AND sucursal_seguradora_id = " & Sucursal & _
            " AND seguradora_cod_susep = " & Seguradora & _
            " AND ramo_id = " & ramo
            
    Set rs = rdocn1.OpenResultset(vSql)
    If Not rs.EOF Then
        If rs(0) = "A" Then
            ValidaSinistroCoseguro = True
            rs.Close
        Else
            rs.Close
            Exit Function
        End If
    Else
        rs.Close
        Exit Function
    End If
        
End Function

'Demanda 689336 - emaior - 10/09/2009 - Indeniza��o superior IS.
Function VerificaAlcadaTecnico() As Boolean

Dim sinsel As Sinist
Dim rs As rdoResultset
Dim tp_cobertura As Integer
Dim Cod_Objeto_Segurado As Integer
Dim valor_indenizacao As String

'Alan Neves - MP - 12/03/2013 - Inicio
'Dim cod_tecnico As Integer
'Alan Neves - MP - Fim

Dim rc_aux As rdoResultset
Dim bVerifica As Boolean
'Diego
Dim tp_ramo_id As Integer

    Set sinsel = SinistrosSelecionados.Item(flexSelecao.Row)

    VerificaAlcadaTecnico = False
    Cod_Objeto_Segurado = 0
        
    'Obtendo o c�digo do objeto segurado pelo sinistro.
    If optVida.Value = True Then
        Cod_Objeto_Segurado = 1
    Else
        vSql = "SELECT cod_objeto_segurado "
        vSql = vSql & "FROM sinistro_re_tb WITH(NOLOCK) "
        vSql = vSql & "WHERE sinistro_id = " & sinsel.sinistro_id
        vSql = vSql & "  AND apolice_id = " & sinsel.Apolice_id
        vSql = vSql & "  AND sucursal_seguradora_id = " & sinsel.Sucursal_id
        vSql = vSql & "  AND seguradora_cod_susep = " & sinsel.Seguradora_id
        vSql = vSql & "  AND ramo_id = " & sinsel.ramo_id
                
        Set rs = rdocn.OpenResultset(vSql)
        
        If Not rs.EOF Then
            Cod_Objeto_Segurado = rs("cod_objeto_segurado")
        End If
        rs.Close
    End If
    
    'Obtendo o c�digo do t�cnico.
    'Alan Neves - MP - 12/03/2013 - Inicio - Atribui��o realizada no carregamento do form
    'cod_tecnico = Obtem_Codigo_Tecnico
    'Alan Neves - MP - 12/03/2013 - Fim
    
    If cod_tecnico = 0 Then
        VerificaAlcadaTecnico = True
        Exit Function
    End If
    
    'Recuperando o tipo do ramo
    'Alan Neves - MP - Inicio - 18/03/2013 - Execu��o em apenas um processo.
'    vsql = "SELECT tp_ramo_id "
'    vsql = vsql & "FROM ramo_tb WITH(NOLOCK) "
'    vsql = vsql & "WHERE ramo_id = " & sinsel.ramo_id
'
'    Set rs = rdocn.OpenResultset(vsql)
'
'    If Not rs.EOF Then
'        tp_ramo_id = rs("tp_ramo_id")
'    End If
            
    vSql = "set nocount on" & vbCrLf
    vSql = vSql & "declare @tp_ramo_id int" & vbCrLf
    vSql = vSql & "SELECT @tp_ramo_id = tp_ramo_id "
    vSql = vSql & "  FROM seguros_db..ramo_tb WITH(NOLOCK) "
    vSql = vSql & " WHERE ramo_id = " & sinsel.ramo_id & vbNewLine
'    'Alan Neves - MP - Inicio - 18/03/2013 - Fim
    
    
    'VERIFICAR O TIPO DO RAMO E NO CASO DE VIDA BUSCAR O VALOR DA ESTIMATIVA DA INDENIZA��O
    'Obtendo o c�digo do tipo de cobertura.
    
    'Alan Neves - MP - Inicio - 18/03/2013 - Execu��o em apenas um processo. Comentado para executar no proximo vsql
'    If tp_ramo_id = 1 Then
'        vsql = ""
'        vsql = vsql & "select distinct sc.tp_cobertura_id, sc.val_estimativa as valor_indenizacao "
'        vsql = vsql & "  from sinistro_cobertura_tb sc with(nolock)"
'        vsql = vsql & " inner join sinistro_estimativa_tb se with(nolock)"
'        vsql = vsql & "    on sc.sinistro_id = se.sinistro_id"
'        vsql = vsql & " INNER JOIN pgto_sinistro_tb pc WITH(NOLOCK) "
'        vsql = vsql & "    ON pc.sinistro_id = sc.sinistro_id"
'        vsql = vsql & " Where sc.sinistro_id = " & sinsel.sinistro_id
'        vsql = vsql & " and sc.dt_fim_vigencia is null"
'        vsql = vsql & " and se.dt_fim_estimativa is null"
'        vsql = vsql & " and se.item_val_estimativa = 1"
'        vsql = vsql & " and pc.situacao_op = 'n'"
'   Else
'        vsql = ""
'        'Diego Galizoni Caversan - GPTI - 28/12/2009 - demanda 1391917
'        'somando os valores devido ao agrupamento
'        vsql = vsql & "SELECT DISTINCT tp_cobertura_id,  sum(pc.val_acerto) + sum(pc.val_correcao) AS valor_indenizacao "
'        vsql = vsql & "  FROM sinistro_cobertura_tb sc WITH(NOLOCK) "
'        vsql = vsql & " INNER JOIN pgto_sinistro_tb pc WITH(NOLOCK) "
'        vsql = vsql & "    ON pc.sinistro_id = sc.sinistro_id"
'        vsql = vsql & " WHERE sc.sinistro_id = " & sinsel.sinistro_id
'        vsql = vsql & "   AND sc.apolice_id = " & sinsel.Apolice_id
'        vsql = vsql & "   AND sc.sucursal_seguradora_id = " & sinsel.Sucursal_id
'        vsql = vsql & "   AND sc.seguradora_cod_susep = " & sinsel.Seguradora_id
'        vsql = vsql & "   AND sc.ramo_id = " & sinsel.ramo_id
'        vsql = vsql & "   AND sc.dt_fim_vigencia IS NULL "
'        vsql = vsql & "   AND pc.item_val_estimativa = 1"
'        'Diego Galizoni Caversan - GPTI - 28/12/2009 - demanda 1391917
'        'considerar apenas a situa��o "n" e agrupar por cobertura
'        vsql = vsql & "   AND pc.situacao_op = 'n'"
'        vsql = vsql & "   group by tp_cobertura_id"
'    End If
    
    
    vSql = vSql & "if @tp_ramo_id = 1 "
        vSql = vSql & "select distinct sc.tp_cobertura_id, sc.val_estimativa as valor_indenizacao "
        vSql = vSql & "  from sinistro_cobertura_tb sc with(nolock)"
        vSql = vSql & " inner join sinistro_estimativa_tb se with(nolock)"
        vSql = vSql & "    on sc.sinistro_id = se.sinistro_id"
        
    'Alan Neves - 19/03/2013 - MP - Inicio
    ' vsql = vsql & " inner join pgto_sinistro_tb pc WITH(NOLOCK) "
    vSql = vSql & " inner join #pgto_sinistro_tb pc WITH(NOLOCK) "
    'Alan Neves - 19/03/2013 - MP - Inicio
        vSql = vSql & "    ON pc.sinistro_id = sc.sinistro_id"
        vSql = vSql & " Where sc.sinistro_id = " & sinsel.sinistro_id
        vSql = vSql & " and sc.dt_fim_vigencia is null"
        vSql = vSql & " and se.dt_fim_estimativa is null"
        vSql = vSql & " and se.item_val_estimativa = 1"
    vSql = vSql & " and pc.situacao_op = 'n'" & vbNewLine
    
    vSql = vSql & " Else " & vbNewLine
    
        'Diego Galizoni Caversan - GPTI - 28/12/2009 - demanda 1391917
        'somando os valores devido ao agrupamento
        'Cleber Sardo - In�cio - INC000004552017 - Data 26/03/2015 - Corrigindo o valor da cobertura selecionada
        vSql = vSql & "SELECT sc.tp_cobertura_id,  sum(pc.val_acerto) + sum(pc.val_correcao) AS valor_indenizacao "
        vSql = vSql & "  FROM sinistro_cobertura_tb sc WITH(NOLOCK) "
    
    'Alan Neves - 19/03/2013 - MP - Inicio
    'vsql = vsql & " INNER JOIN pgto_sinistro_tb pc WITH(NOLOCK) "
    vSql = vSql & " INNER JOIN #pgto_sinistro_tb pc WITH(NOLOCK) "
    'Alan Neves - 19/03/2013 - MP - Fim
    
        vSql = vSql & "    ON pc.sinistro_id = sc.sinistro_id"
        vSql = vSql & " INNER JOIN pgto_sinistro_cobertura_tb psc WITH(NOLOCK)"
        vSql = vSql & " ON psc.Item_val_estimativa = pc.Item_val_estimativa"
        vSql = vSql & " and psc.seq_pgto = pc.seq_pgto"
        vSql = vSql & " and psc.seq_estimativa = pc.seq_estimativa"
        vSql = vSql & " and psc.beneficiario_id = pc.beneficiario_id"
        vSql = vSql & " and psc.sinistro_id = pc.sinistro_id"
        vSql = vSql & " and psc.tp_cobertura_id = sc.tp_cobertura_id"
        vSql = vSql & " WHERE sc.sinistro_id = " & sinsel.sinistro_id
        vSql = vSql & "   AND sc.apolice_id = " & sinsel.Apolice_id
        vSql = vSql & "   AND sc.sucursal_seguradora_id = " & sinsel.Sucursal_id
        vSql = vSql & "   AND sc.seguradora_cod_susep = " & sinsel.Seguradora_id
        vSql = vSql & "   AND sc.ramo_id = " & sinsel.ramo_id
        vSql = vSql & "   AND sc.dt_fim_vigencia IS NULL "
        vSql = vSql & "   AND pc.item_val_estimativa = 1"
        'Diego Galizoni Caversan - GPTI - 28/12/2009 - demanda 1391917
        'considerar apenas a situa��o "n" e agrupar por cobertura
        vSql = vSql & "   AND pc.situacao_op = 'n'"
        vSql = vSql & "   group by sc.tp_cobertura_id"
        'Cleber Sardo - Fim - INC000004552017 - Data 26/03/2015 - Corrigindo o valor da cobertura selecionada
    'Alan Neves - MP - Inicio - 18/03/2013 - Fim
    
    Set rs = rdocn1.OpenResultset(vSql)
    
    Do While Not rs.EOF
        tp_cobertura = rs("tp_cobertura_id")
        valor_indenizacao = rs("valor_indenizacao")
        
        'Verifica se o t�cnico tem al�ada para aprovar o sinistro.
        vSql = "EXEC SEGS7442_SPS " & vSinistro & ", " & _
                                      tp_cobertura & ", " & _
                                      Cod_Objeto_Segurado & ", " & _
                                      valor_indenizacao & ", " & _
                                      cod_tecnico & ", '" & _
                                      cUserName & "'"
        
        Set rc_aux = rdocn.OpenResultset(vSql)
        
        If rc_aux(0) = 0 Then
            VerificaAlcadaTecnico = True
            rc_aux.Close
            Exit Do
        End If
        
        rc_aux.Close
        rs.MoveNext
    Loop
    rs.Close

End Function

Public Function LocalizarBeneficiarioBanco(ByVal pSinistro_id As String, pProduto_id As Integer) As Boolean

Dim rs As rdoResultset
Dim SQL As String
Dim intSitSinistroAux As Integer



' Demanda: 12967997 - COMECO '
' Anderson Teixeira - Permitir pagamento para Beneficiarios quando a situacao na sinistro_solicita_saldo_tb = 5 '
        
intSitSinistroAux = 0

If pProduto_id = 1198 Then

    SQL = ""
    SQL = SQL & "SELECT COUNT(1) " & vbCrLf
    SQL = SQL & "  FROM sinistro_solicita_saldo_tb WITH(NOLOCK) " & vbCrLf
    SQL = SQL & " WHERE sinistro_id = " & pSinistro_id
    SQL = SQL & "       AND situacao = 5"
    
    Set rs = rdocn1.OpenResultset(SQL)
    intSitSinistroAux = CInt(rs(0))
    rs.Close
    
    If intSitSinistroAux > 0 Then
        LocalizarBeneficiarioBanco = False
        Exit Function
    End If

End If
    
' Demanda: 12967997 - FIM '




'SQL = ""
'SQL = SQL & " SELECT evento_segbr_sinistro_atual_tb.tipo_pagamento,  " & vbNewLine
'SQL = SQL & "        produto_tb.sinistro_solicita_saldo  " & vbNewLine
'SQL = SQL & "    FROM evento_segbr_sinistro_atual_tb WITH(NOLOCK) " & vbNewLine
'SQL = SQL & "    JOIN produto_tb WITH(NOLOCK) " & vbNewLine
'SQL = SQL & "      ON produto_tb.produto_id = evento_segbr_sinistro_atual_tb.produto_id  " & vbNewLine
'SQL = SQL & "    JOIN evento_segbr_tb WITH(NOLOCK) " & vbNewLine
'SQL = SQL & "      ON evento_segbr_tb.evento_bb_id = evento_segbr_sinistro_atual_tb.evento_bb_id  " & vbNewLine
'SQL = SQL & "   WHERE evento_segbr_sinistro_atual_tb.sinistro_id = " & pSinistro_id & vbNewLine
'SQL = SQL & "     AND evento_segbr_sinistro_atual_tb.evento_bb_id = 1151  " & vbNewLine
'SQL = SQL & "     AND evento_segbr_sinistro_atual_tb.tipo_pagamento = 9  " & vbNewLine
'SQL = SQL & "     AND produto_tb.sinistro_solicita_saldo = 's'  " & vbNewLine
'SQL = SQL & "     AND evento_segbr_sinistro_atual_tb.num_recibo = (SELECT MAX(evento_segbr_aux.num_recibo)  " & vbNewLine
'SQL = SQL & "                                                        FROM evento_segbr_sinistro_atual_tb evento_segbr_aux WITH(NOLOCK) " & vbNewLine
'SQL = SQL & "                                                        JOIN evento_segbr_sinistro_atual_tb evento_segbr_aux2 WITH(NOLOCK) " & vbNewLine
'SQL = SQL & "                                                          ON evento_segbr_aux2.sinistro_id = evento_segbr_aux.sinistro_id  " & vbNewLine
'SQL = SQL & "                                                         AND evento_segbr_aux2.num_recibo = evento_segbr_aux.num_recibo  " & vbNewLine
'SQL = SQL & "                                                         AND evento_segbr_aux2.tipo_pagamento = evento_segbr_aux.tipo_pagamento  " & vbNewLine
'SQL = SQL & "                                                       WHERE evento_segbr_aux.sinistro_id = evento_segbr_sinistro_atual_tb.sinistro_id  " & vbNewLine
'SQL = SQL & "                                                         AND evento_segbr_aux.evento_bb_id = 1110  " & vbNewLine
'SQL = SQL & "                                                         AND evento_segbr_aux2.evento_bb_id = 2014 " & vbNewLine
'SQL = SQL & "                                                         AND evento_segbr_aux.tipo_pagamento = evento_segbr_sinistro_atual_tb.tipo_pagamento) "

SQL = ""
SQL = SQL & " SET NOCOUNT ON EXEC seguros_db.dbo.SEGS13099_SPS " & pSinistro_id & ", 1151, 9, 'S', 1110, 2014 "

LocalizarBeneficiarioBanco = False

Set rs = rdocn1.OpenResultset(SQL)
While Not rs.EOF
    If rs("tipo_pagamento") = 9 And rs("sinistro_solicita_saldo") = "s" Then
        LocalizarBeneficiarioBanco = True
    End If
Wend

rs.Close

End Function

'Demanda 689336 - emaior - 10/09/2009 - Indeniza��o superior IS.
Private Function Obtem_Codigo_Tecnico() As Integer
    
    Dim SQL         As String
    Dim rs          As rdoResultset
    Dim OLogin      As String
    
    On Error GoTo TrataErro
    
    Obtem_Codigo_Tecnico = 0
    
    '* Obt�m o login do usu�rio
    'Ricardo Toledo - Confitec : 28/06/2012 : INC000003578005 : inicio
    'OLogin = BuscaLogin(cUserName)
    If glAmbiente_id = 2 Or glAmbiente_id = 3 Then
        OLogin = BuscaLogin(cUserName)
    Else
        OLogin = BuscaLogin(cUserName, 1)
    End If
    'Ricardo Toledo - Confitec : 28/06/2012 : INC000003578005 : fim
    
    If voper = "I" Or voper = "IC" Or voper = "IS" Then
        SQL = "SELECT distinct(tecnico_tb.tecnico_id)"
        SQL = SQL & " FROM tecnico_tb  with(nolock) "
        SQL = SQL & " JOIN limite_aprovacao_pgto_tb with(nolock) "
        SQL = SQL & "   ON limite_aprovacao_pgto_tb.tecnico_id = tecnico_tb.tecnico_id "
        SQL = SQL & " WHERE eMail = '" & OLogin & "'"
        SQL = SQL & " AND limite_aprovacao_pgto_tb.dt_fim_vigencia IS NULL "
        
        Set rs = rdocn1.OpenResultset(SQL)
        
        If Not rs.EOF Then
            Obtem_Codigo_Tecnico = rs("tecnico_id")
        End If
        rs.Close
    Else
        SQL = "SELECT distinct(tecnico_tb.tecnico_id)"
        SQL = SQL & " FROM tecnico_tb  with(nolock) "
        SQL = SQL & " WHERE eMail = '" & OLogin & "'"
        
        Set rs = rdocn1.OpenResultset(SQL)
        
        If Not rs.EOF Then
            Obtem_Codigo_Tecnico = rs("tecnico_id")
        Else
            MsgBox "T�cnico n�o encontrado. E-mail: " & OLogin
        End If
        rs.Close
    End If
    
    Exit Function
    
TrataErro:
    Call TrataErroGeral("Obtem_Codigo_Tecnico", Me.name)
    
End Function
