VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.2#0"; "MSCOMCTL.OCX"
Begin VB.Form frmAtivaEmissao 
   ClientHeight    =   2490
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   5715
   Icon            =   "frmAtivaEmissao.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   2490
   ScaleWidth      =   5715
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdProcessar 
      Caption         =   "Processar"
      Height          =   375
      Left            =   2160
      TabIndex        =   0
      Top             =   1770
      Width           =   1695
   End
   Begin VB.CommandButton cmdSair 
      Caption         =   "Sair"
      Height          =   375
      Left            =   3930
      TabIndex        =   1
      Top             =   1770
      Width           =   1695
   End
   Begin MSComctlLib.StatusBar StbRodape 
      Align           =   2  'Align Bottom
      Height          =   315
      Left            =   0
      TabIndex        =   2
      Top             =   2175
      Width           =   5715
      _ExtentX        =   10081
      _ExtentY        =   556
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   3
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   8820
            MinWidth        =   8820
            Text            =   "Mensagem"
            TextSave        =   "Mensagem"
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   6
            Alignment       =   1
            AutoSize        =   2
            Object.Width           =   1376
            MinWidth        =   88
            Text            =   "Data do sistema"
            TextSave        =   "21/05/20"
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            AutoSize        =   2
            Object.Width           =   2646
            MinWidth        =   2647
            Text            =   "Usu�rio"
            TextSave        =   "Usu�rio"
         EndProperty
      EndProperty
   End
   Begin VB.Frame Frame2 
      Height          =   1635
      Left            =   90
      TabIndex        =   3
      Top             =   60
      Width           =   5535
      Begin VB.Label lblQtdPropostas 
         Alignment       =   1  'Right Justify
         BorderStyle     =   1  'Fixed Single
         Height          =   315
         Left            =   2010
         TabIndex        =   9
         Top             =   1140
         Width           =   2055
      End
      Begin VB.Label label1 
         Alignment       =   1  'Right Justify
         AutoSize        =   -1  'True
         Caption         =   "Propostas Ativadas:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   165
         TabIndex        =   8
         Top             =   1215
         Width           =   1710
      End
      Begin VB.Label Label2 
         Alignment       =   1  'Right Justify
         AutoSize        =   -1  'True
         Caption         =   "Hora de In�cio:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   600
         TabIndex        =   7
         Top             =   345
         Width           =   1305
      End
      Begin VB.Label Label3 
         Alignment       =   1  'Right Justify
         AutoSize        =   -1  'True
         Caption         =   "Hora de Fim:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   780
         TabIndex        =   6
         Top             =   780
         Width           =   1095
      End
      Begin VB.Label lblInicio 
         Alignment       =   1  'Right Justify
         BorderStyle     =   1  'Fixed Single
         Height          =   315
         Left            =   2010
         TabIndex        =   5
         Top             =   270
         Width           =   2055
      End
      Begin VB.Label lblFim 
         Alignment       =   1  'Right Justify
         BorderStyle     =   1  'Fixed Single
         Height          =   315
         Left            =   2010
         TabIndex        =   4
         Top             =   720
         Width           =   2055
      End
   End
End
Attribute VB_Name = "frmAtivaEmissao"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmdProcessar_Click()

    Call AtivarEmissao

End Sub

Private Sub cmdSair_Click()

On Error GoTo Trata_Erro

  Call FinalizarAplicacao
  Exit Sub

Trata_Erro:
  Call TratarErro("cmdSair_Click", Me.name)
  Call FinalizarAplicacao
End Sub

Private Sub Form_Load()
   
On Local Error GoTo Trata_Erro
'Verifica se foi executado pelo Control-M
CTM = Verifica_Origem_ControlM(Command)

  'Pula se for executado pelo control-m
    If CTM = False Then
      'Verificando permiss�o de acesso � aplica��o''''''''''''''''''''''''''''''''''''''''''
      If Not Trata_Parametros(Command) Then
'         FinalizarAplicacao
      End If
    End If

'
  cUserName = "99988877714"
  glAmbiente_id = 3

  'Obtendo a data operacional ''''''''''''''''''''''''''''''''''''''''''''
  Call ObterDataSistema("SEGBR")
      
  'Configurando interface'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  Me.Caption = "SEGP0927 - Ativa��o de Emiss�o" & ExibirAmbiente(glAmbiente_id)
  
  Call InicializarInterface
  
  'Iniciando o processo automaticamente caso seja uma chamada do Scheduler '''''''''''''''
  If Obtem_agenda_diaria_id(Command) > 0 Or CTM = True Then
    Me.Show
    Call cmdProcessar_Click
    FinalizarAplicacao
  End If
     
  Exit Sub
  
Trata_Erro:
  Call TratarErro("Form_Load", Me.name)
  Call FinalizarAplicacao

End Sub


Private Sub Form_Unload(Cancel As Integer)

On Error GoTo Trata_Erro

  Call FinalizarAplicacao
  Exit Sub

Trata_Erro:
  Call TratarErro("Form_Unload", Me.name)
  Call FinalizarAplicacao

End Sub

Sub InicializarInterface()
    
  lblInicio.Caption = ""
  lblFim.Caption = ""
  
  StbRodape.Panels(1).Text = "Selecione Processar..."
        
End Sub

Sub AtivarEmissao()

Dim sMensagem As String
Dim oDocumento As Object
Dim rsLog As Recordset
Dim sGerarDocUnico As String
Dim iProdutoId As Integer

On Error GoTo Trata_Erro
    
  ' Tratamento para o Scheduler ''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'  InicializaParametrosExecucaoBatch Me
  
  ' Atualizando interface ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  MousePointer = vbHourglass
  cmdProcessar.Enabled = False
  lblInicio = Now
  sMensagem = "Ativando Propostas..."
  StbRodape.Panels(1).Text = sMensagem
  DoEvents
  
  ' Processamento dos documentos '''''''''''''''''''''''''''''''''''''''''''''''''''''''
  Set oDocumento = CreateObject("SEGL0143.cls0128")
  
'  oDocumento.CTMArquivoLog = CTMArquivoLog
  
  'Rotina de gera��o da fatura''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  Set rsLog = oDocumento.AtivarEmissao("SEGBR", _
                                       App.Title, _
                                       App.FileDescription, _
                                       glAmbiente_id, _
                                       Data_Sistema, _
                                       cUserName)
                                       
  'Atualizando a Interface''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  Call AtualizarInterface(rsLog("qtd_propostas"))
                                                  
  Set oDocumento = Nothing
  Set rsLog = Nothing

  'Encerrando AutProd'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'  Call GravarLog
    'Pula de for executado pelo Control-M
    If CTM = False Then
'        Call goProducao.finaliza
    End If

  
  'Atualizando interface''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  Me.Show
  
  Exit Sub
    
Trata_Erro:
  Call TratarErro("AtivarEmissao", Me.name)
  Call FinalizarAplicacao

End Sub

Private Sub GravarLog()

'Logando o n�mero de registros processados - Scheduler '''''''''''''''''''''''''''
Dim i As Integer
Dim iIndice As Integer

 Call goProducao.AdicionaLog(1, lblQtdPropostas.Caption, iIndice)
 
 'grava log arquivo CTM
If CTM = True Then
    Call GravaLogCTM("OK - ", lblQtdPropostas.Caption, "", "")
End If


Exit Sub
  
Trata_Erro:
  Call TratarErro("GravarLog", Me.name)
  Call FinalizarAplicacao

End Sub


Private Sub AtualizarInterface(ByVal iQtdPropostas As Long)

Dim sMensagem As String

On Error GoTo Trata_Erro
    
  lblFim = Now
  
  lblQtdPropostas = iQtdPropostas

  If Val(lblQtdPropostas.Caption) > 0 Then
    sMensagem = "Processamento realizado com sucesso."
  Else
    sMensagem = "Nenhum documento foi gerado."
  End If
          
  MousePointer = vbNormal
  StbRodape.Panels(1).Text = sMensagem
  
  Exit Sub

Trata_Erro:
  Call TratarErro("AtualizarInterface", Me.name)
  Call FinalizarAplicacao
    
End Sub

