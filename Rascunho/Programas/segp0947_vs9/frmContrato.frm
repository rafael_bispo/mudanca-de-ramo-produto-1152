VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.2#0"; "MSCOMCTL.OCX"
Begin VB.Form frmContrato 
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   2070
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   5340
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2070
   ScaleWidth      =   5340
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmbSair 
      Caption         =   "Sair"
      Height          =   405
      Left            =   4140
      TabIndex        =   5
      Top             =   1230
      Width           =   1155
   End
   Begin VB.CommandButton cmdAtivar 
      Caption         =   "Ativar"
      Height          =   405
      Left            =   2850
      TabIndex        =   4
      Top             =   1230
      Width           =   1245
   End
   Begin VB.Frame fraResultados 
      Caption         =   "  Resultado do Processamento "
      Height          =   915
      Left            =   120
      TabIndex        =   1
      Top             =   120
      Width           =   5145
      Begin VB.TextBox txtProcessadas 
         Alignment       =   1  'Right Justify
         Height          =   345
         Left            =   2430
         Locked          =   -1  'True
         TabIndex        =   2
         Text            =   "0"
         Top             =   360
         Width           =   1845
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Processados:"
         Height          =   195
         Index           =   0
         Left            =   720
         TabIndex        =   3
         Top             =   480
         Width           =   960
      End
   End
   Begin MSComctlLib.StatusBar StbRodape 
      Align           =   2  'Align Bottom
      Height          =   315
      Left            =   0
      TabIndex        =   0
      Top             =   1755
      Width           =   5340
      _ExtentX        =   9419
      _ExtentY        =   556
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   3
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   8997
            MinWidth        =   8997
            Text            =   "Mensagem"
            TextSave        =   "Mensagem"
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   6
            Alignment       =   1
            AutoSize        =   2
            Object.Width           =   1376
            MinWidth        =   88
            Text            =   "Data do sistema"
            TextSave        =   "21/05/20"
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            AutoSize        =   2
            Object.Width           =   1111
            MinWidth        =   88
            Text            =   "Usu�rio"
            TextSave        =   "Usu�rio"
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "frmContrato"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim sFlagTrataABS As Boolean

Private Sub cmbSair_Click()

    'Call FinalizarAplicacao
    Unload Me
    
End Sub

Private Sub cmdAtivar_Click()

    Call Processar

End Sub

Private Sub Form_Load()

    On Error GoTo Trata_Erro
    'Verifica se foi executado pelo Control-M
    CTM = Verifica_Origem_ControlM(Command)

    ' Verificando permiss�o de acesso � aplica��o '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    'Pula se for executado pelo control-m
   'If CTM = False Then
    '    If Not Trata_Parametros(Command) Then
     '       Call FinalizarAplicacao
     '   End If
    'End If
    
    If glAmbiente_id = 3 Or glAmbiente_id = 7 Then 'luissantos - 11/09/2010 (Quando for ABS, seta vari�vel como True)
        sFlagTrataABS = True
    Else
        sFlagTrataABS = False
    End If

    glAmbiente_id = 3
    cUserName = "11111111111"

    ' Obtendo a data operacional do sistema '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

    Call ObterDataSistema(gsSIGLASISTEMA)

    Me.Caption = "SEGP0947 - Verifica��o de distribui��o de cosseguro "

    StbRodape.Panels(1).Text = "Clique para ativar o processamento"

   'Iniciando o processo automaticamente caso seja uma chamada do Scheduler '''''''''''''''
    'If Obtem_agenda_diaria_id(Command) > 0 Or CTM = True Then
       Me.Show
    '   Call cmdAtivar_Click
     '  Call Unload(Me)
    'End If
    
    Exit Sub

Trata_Erro:

    Call TratarErro("Form_Load", Me.name)
    Call FinalizarAplicacao

End Sub

Private Sub Processar()

Dim oALS As Object
Dim lProcessados As Long
Dim rsProcessados As Recordset

Const EVENTO_EMISSAO As Integer = 4700 '9001
Const TIPO_AVALIACAO_ACEITE_PRE_ANALISE = 1397

    On Error GoTo Trata_Erro
    
    'Tratamento para o Scheduler ''''''''''''''''''''''''''''''''''''''''''''''''
'    Call InicializaParametrosExecucaoBatch(Me)
    
    Set oALS = CreateObject("SEGL0281.cls00477")
    
    lProcessados = txtProcessadas.Text
                                                                                          
    Set rsProcessados = oALS.EfetuarVerificacaoDistribuicaoCosseguro(App.ProductName, _
                                                                     App.Title, _
                                                                     App.FileDescription, _
                                                                     glAmbiente_id, _
                                                                     Data_Sistema, _
                                                                     cUserName, _
                                                                     sFlagTrataABS)
    
    If Not rsProcessados.EOF Then lProcessados = rsProcessados(0)
    
    Set oALS = Nothing
        
    txtProcessadas = lProcessados
    
'    Call goProducao.AdicionaLog(1, lProcessados, 1)
    
    'grava log arquivo CTM
    If CTM = True Then
        Call GravaLogCTM("OK - ", lProcessados, "", "")
    End If

                      
    'Finalizando agenda''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

    'Pula de for executado pelo Control-M
'    If CTM = False Then
   '     Call goProducao.finaliza
    'End If
    
    'Atualizando interface ''''''''''''''''''''''''''''''''''''
    MousePointer = vbDefault
    
    Exit Sub

Trata_Erro:

    Call TratarErro("VerificarDadoAlsEmissao", Me.name)
    Call FinalizarAplicacao

End Sub

