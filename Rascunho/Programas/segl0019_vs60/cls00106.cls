VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cls00106"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit
Private ErrCode As Long
Private ErrDesc As String

Function ObterEntidadeId(ByVal sSiglaSistema As String, _
                         ByVal sSiglaRecurso As String, _
                         ByVal sDescricaoRecurso As String, _
                         ByVal iAmbienteId As Integer, _
                         ByVal sArquivo As String) As Long

    Dim sSql As String
    Dim rs   As Recordset

    sSql = ""
    adSQL sSql, "SELECT entidade_id "
    adSQL sSql, "FROM controle_proposta_db..layout_tb "
    adSQL sSql, "WHERE nome = '" & sArquivo & "'"

    Set rs = ExecutarSQL(sSiglaSistema, _
       iAmbienteId, _
       sSiglaRecurso, _
       sDescricaoRecurso, _
       sSql, _
       True)
                     
    ObterEntidadeId = rs(0)

End Function

Private Sub RegistrarControleArquivo(ByVal sUsuario As String, _
                                     ByVal sSiglaSistema As String, _
                                     ByVal sSiglaRecurso As String, _
                                     ByVal sDescricaoRecurso As String, _
                                     ByVal lAmbienteID As Long, _
                                     ByVal sArquivo As String, _
                                     ByVal lVersao As Long, _
                                     ByVal lLinhas As Long, _
                                     ByVal lRegistros As Long, _
                                     ByVal sDataOperacional As String, _
                                     ByVal sTpArquivo As String)
   
    Dim sSql        As String
    Dim objArquivo  As Object
    Dim lEntidadeId As Long

    On Error GoTo Trata_Erro

    'Obtendo a Entidade_id ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    
    lEntidadeId = ObterEntidadeId(sSiglaSistema, _
       sSiglaRecurso, _
       sDescricaoRecurso, _
       lAmbienteID, _
       sArquivo)

    ' instanciando a classe de tratamento de arquivos '''''''''''''''''''''''''''''''''''''
    
    Set objArquivo = CreateObject("SEGL0023.cls00119")
    
    ' Atualizando dados de importacao '''''''''''''''''''''''''''''''

    Call objArquivo.RegistrarArquivoProcessado(sSiglaSistema, _
       sSiglaRecurso, _
       sDescricaoRecurso, _
       lAmbienteID, _
       sArquivo, _
       lVersao, _
       lLinhas, _
       lRegistros, _
       sDataOperacional, _
       sUsuario, _
       sTpArquivo, _
       lEntidadeId)
    
    Set objArquivo = Nothing
                      
    Exit Sub

Trata_Erro:
   
    Call Err.Raise(Err.Number, , "SEGL0019.cls00106.Atualizar_Controle_Arquivo - " & Err.Description)
        
End Sub

Private Function MudaVirgulaParaPonto(sValor As String) As String

    If InStr(sValor, ",") = 0 Then
        MudaVirgulaParaPonto = sValor
    Else
        sValor = Mid$(sValor, 1, InStr(sValor, ",") - 1) + "." + Mid$(sValor, InStr(sValor, ",") + 1)
        MudaVirgulaParaPonto = sValor
    End If

End Function

Public Function ReceberInadimplenciaBB(ByVal sUsuario As String, _
                                       ByVal sSiglaSistema As String, _
                                       ByVal sSiglaRecurso As String, _
                                       ByVal sDescricaoRecurso As String, _
                                       ByVal lAmbienteID As Long, _
                                       ByVal sNomeArquivo As String, _
                                       ByVal lVersao As Long, _
                                       ByVal sPath As String, _
                                       ByVal sDataOperacional As String, _
                                       Optional ByVal lAmbienteID_seg2 As Long) As Integer

    ' C�digos de Erro
    ' 0 - Nenhum erro ocorrido
    ' 1 - O Header difere da extens�o do arquivo - SEG444
    ' 2 - O Header difere da extens�o do arquivo - SEG497D
    ' 3 - Erro no Trailler
    ' 999 - Indefinido

    'Vari�veis auxiliares '''''''''''''''''''''''''''''''''''''''''''''''''''''''

    Dim sSql           As String
    Dim sReg           As String
    Dim sSequencial    As String
    Dim sArqinput      As String
    Dim lqtdReg        As Long
    Dim lPosition      As Long
    Dim sHeader        As String
    Dim sArquivo       As String

    'Vari�veis para inclus�o da cobranca inadimplente''''''''''''''''''''''''''''

    Dim sProdutoExtId  As String
    Dim sPropostaBB    As String
    Dim sDtCobranca    As String
    Dim sVlDebito      As String
    Dim sAgencia       As String
    Dim sContaCorrente As String
    Dim sTpRetorno     As String
    Dim sNumCobranca   As String
    Dim sNossoNumero   As String
    Dim sNumEndossoBB  As String
    Dim sBloqueioBB    As String
    Dim sNossoNumeroDv As String

    Dim iCodErro       As Integer
    Dim lConexaoAB     As Long
    Dim lConexaoABS    As Long
    Dim lAmbienteExec  As Long
    
    Const RECEBIDO = "R"

    On Error GoTo Trata_Erro
    
    iCodErro = 0
     
    'Iniciando transa��o '''''''''''''''''''''''''''''''''''''''''''''''''''''''''
      
    'oConexao.BeginTrans
    lConexaoAB = AbrirConexao(sSiglaSistema, _
       lAmbienteID, _
       sSiglaRecurso, _
       sDescricaoRecurso)
                        
    If Not AbrirTransacao(lConexaoAB) Then GoTo Trata_Erro
     
    'Se tratar ABS passa o ambiente, sen�o � zero.
    If lAmbienteID_seg2 > 0 Then
        lConexaoABS = AbrirConexao(sSiglaSistema, _
           lAmbienteID_seg2, _
           sSiglaRecurso, _
           sDescricaoRecurso)
        
        If Not AbrirTransacao(lConexaoABS) Then GoTo Trata_Erro
    End If
     
    'Obtendo nome do arquivo
    sArquivo = UCase(Mid(sNomeArquivo, 1, InStr(1, sNomeArquivo, ".") - 1))
     
    'Abrindo arquivo '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    
    sArqinput = FreeFile
    Open sPath & "\" & sNomeArquivo For Input As sArqinput
    Line Input #sArqinput, sHeader
    
    'Validando Header''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    
    Select Case sArquivo
        
        Case "SEG444"
        
            If Val(Mid(sHeader, 24, 4)) <> lVersao Then
            
                ReceberInadimplenciaBB = 1
                If Not RetornarTransacao(lConexaoAB) Then GoTo Trata_Erro
                If Not FecharConexao(lConexaoAB) Then GoTo Trata_Erro
                If lAmbienteID_seg2 > 0 Then
                    If Not RetornarTransacao(lConexaoABS) Then GoTo Trata_Erro
                    If Not FecharConexao(lConexaoABS) Then GoTo Trata_Erro
                End If
                Exit Function
    
            End If
    
        Case "SEG497D"
      
            If Val(Mid(sHeader, 32, 6)) <> lVersao Then
            
                ReceberInadimplenciaBB = 2
                If Not RetornarTransacao(lConexaoAB) Then GoTo Trata_Erro
                If Not FecharConexao(lConexaoAB) Then GoTo Trata_Erro
                If lAmbienteID_seg2 > 0 Then
                    If Not RetornarTransacao(lConexaoABS) Then GoTo Trata_Erro
                    If Not FecharConexao(lConexaoABS) Then GoTo Trata_Erro
                End If
                Exit Function
    
            End If
    
    End Select
    
    lqtdReg = 1
    
    'Processando registros do arquivo '''''''''''''''''''''''''''''''''''''''''''
    
    Do
      
        DoEvents
        Line Input #sArqinput, sReg
        lqtdReg = lqtdReg + 1
      
        sSequencial = Mid(sReg, 1, 6)
        
        If sSequencial <> "999999" Then
         
            Select Case sArquivo
    
                    'Obtendo dados do detalhe
            
                Case "SEG497D" 'Retorno Cobran�a Garantia
            
                    sPropostaBB = Mid(sReg, 19, 9)
                    sNumEndossoBB = ""
                    sAgencia = Mid(sReg, 30, 4)
                    sNossoNumero = Mid(sReg, 7, 11)
                    sNossoNumeroDv = Mid(sReg, 18, 1)
                    sTpRetorno = Mid(sReg, 53, 2)
                    sNumCobranca = Mid(sReg, 28, 2)
                    sVlDebito = CSng(Mid(sReg, 30, 15)) / 100
                    sContaCorrente = ""
                    sDtCobranca = Mid(sReg, 45, 2) & "/" & Mid(sReg, 47, 2) & "/" & Mid(sReg, 49, 4)
                    sBloqueioBB = UCase(Mid(sReg, 55, 1))
                
                    'indicador de bloqueio de cobran�a B indica Sim e branco indica N�o
                    If sBloqueioBB = "B" Then
                        sBloqueioBB = "s"
                    Else
                        sBloqueioBB = "n"
                    End If
                
                Case "SEG444"
            
                    sProdutoExtId = Val(Mid(sReg, 7, 9))
                    sPropostaBB = Mid(sReg, 16, 9)
                    sNumEndossoBB = Mid(sReg, 25, 9)
                    sAgencia = Mid(sReg, 108, 4)
                    sNossoNumero = Mid(sReg, 136, 17)
                    sTpRetorno = Mid(sReg, 153, 4)
                    sNumCobranca = "1"
                    sVlDebito = ""
                    sContaCorrente = ""
                    sDtCobranca = ""
            
            End Select
    
            'Atualizando tabela de cobranca inadimplente
        
            sSql = ""
            adSQL sSql, "EXEC segs3956_spi "
            adSQL sSql, sPropostaBB                                     '@proposta_bb"
        
            If sDtCobranca <> "" Then
                adSQL sSql, ",'" & Format(sDtCobranca, "yyyymmdd") & "'"  '@dt_cobranca
            Else
                adSQL sSql, ",NULL"
            End If
        
            If sVlDebito <> "" Then
                adSQL sSql, "," & MudaVirgulaParaPonto(sVlDebito)         '@vl_debito
            Else
                adSQL sSql, ",NULL"
            End If
        
            adSQL sSql, ",'" & sNomeArquivo & "'"                      '@versao_arquivo
        
            If sAgencia <> "" Then
                adSQL sSql, "," & sAgencia                               '@agencia
            Else
                adSQL sSql, "',NULL"
            End If
        
            If sContaCorrente <> "" Then
                adSQL sSql, "," & sContaCorrente                          '@conta_corrente
            Else
                adSQL sSql, ",NULL"
            End If
        
            If Val(sTpRetorno) <> 0 Then
                adSQL sSql, "," & sTpRetorno                              '@tp_retorno
            Else
                adSQL sSql, ",NULL"
            End If
        
            If sNumCobranca <> "" Then
                adSQL sSql, "," & sNumCobranca                           '@num_cobranca
            Else
                adSQL sSql, ",NULL"
            End If
        
            If Val(sNumEndossoBB) <> 0 Then
                adSQL sSql, "," & sNumEndossoBB                          '@num_endosso_bb
            Else
                adSQL sSql, ",NULL"
            End If
        
            If sNossoNumero <> "" Then
                adSQL sSql, "," & sNossoNumero                            '@nosso_numero
            Else
                adSQL sSql, ",NULL"
            End If
        
            adSQL sSql, ",'N'"                                          '@carta_emitida
        
            adSQL sSql, ",'" & sUsuario & "'"                           '@usuario
        
            'Retorno Cobran�a Garantia
            If sArquivo = "SEG497D" Then
                adSQL sSql, ",'" & sBloqueioBB & "'"    '@bloqueio_bb
            Else
                adSQL sSql, ",'NULL'"
            End If
        
            If sArquivo = "SEG444" Then
                adSQL sSql, "," & sProdutoExtId     '@produto_ext_id
            Else
                adSQL sSql, ",NULL"
            End If
                        
            '*<Modificado por: Alexandre Debouch - ConfitecSP - 19/3/2010-10:11:29>
            '*Se tratar Ambiente ABS
            If lAmbienteID_seg2 > 0 Then
                '* Verifica qual o ambiente de execu��o devemos executar a proc de inclusao
                lAmbienteExec = fObterAmbienteExecucao(sSiglaSistema, _
                                                        sSiglaRecurso, _
                                                        sDescricaoRecurso, _
                                                        lAmbienteID_seg2, _
                                                        lAmbienteID, _
                                                        sArquivo, _
                                                        sPropostaBB, _
                                                        IIf(sArquivo = "SEG444", sProdutoExtId, ""))
                If lAmbienteExec = lAmbienteID_seg2 Then
                    '* Se ambiente retornado � o ABS
                    Call Conexao_ExecutarSQL(sSiglaSistema, _
                       lAmbienteID_seg2, _
                       sSiglaRecurso, _
                       sDescricaoRecurso, _
                       sSql, _
                       lConexaoABS, _
                       False)
                Else
                    Call Conexao_ExecutarSQL(sSiglaSistema, _
                       lAmbienteID, _
                       sSiglaRecurso, _
                       sDescricaoRecurso, _
                       sSql, _
                       lConexaoAB, _
                       False)
                End If
            Else
                Call Conexao_ExecutarSQL(sSiglaSistema, _
                   lAmbienteID, _
                   sSiglaRecurso, _
                   sDescricaoRecurso, _
                   sSql, _
                   lConexaoAB, _
                   False)
            End If
            '*</Modificado por: Alexandre Debouch - ConfitecSP - 19/3/2010-10:11:29>
        
            'vbarbosa - 30/03/2005
            'comentada a atualiza��o do bloqueio_bb, pois est� sendo feito na ades�o da inadimpl�ncia
            '         If sArquivo = "SEG497D" Then 'Retorno Cobran�a Garantia
            '            Call AtualizarBloqueioBB(sUsuario, sSiglaSistema, sSiglaRecurso, sDescricaoRecurso, lAmbienteID, CLng(sPropostaBB), sArquivo, CInt(sNumCobranca), sBloqueioBB, lConexao)
            '         End If
            '
        Else 'Verificando se o arquivo foi processado corretamente '''''''''''''''
             
            Select Case sArquivo
        
                    'Obtendo dados do detalhe''''''''''''''''''''''''''''''''''''''''''''''''
            
                Case "SEG497D" 'Retorno Cobran�a Garantia
            
                    If lqtdReg <> Val(Mid(sReg, 32, 6)) + 2 Then
              
                        ReceberInadimplenciaBB = 3
                        If Not RetornarTransacao(lConexaoAB) Then GoTo Trata_Erro
                        If Not FecharConexao(lConexaoAB) Then GoTo Trata_Erro
                        If lAmbienteID_seg2 > 0 Then
                            If Not RetornarTransacao(lConexaoABS) Then GoTo Trata_Erro
                            If Not FecharConexao(lConexaoABS) Then GoTo Trata_Erro
                        End If
                        Exit Function
            
                    End If
          
                Case "SEG444"
             
                    If lqtdReg <> Val(Mid(sReg, 24, 6)) + 2 Then
            
                        ReceberInadimplenciaBB = 3
                        If Not RetornarTransacao(lConexaoAB) Then GoTo Trata_Erro
                        If Not FecharConexao(lConexaoAB) Then GoTo Trata_Erro
                        If lAmbienteID_seg2 > 0 Then
                            If Not RetornarTransacao(lConexaoABS) Then GoTo Trata_Erro
                            If Not FecharConexao(lConexaoABS) Then GoTo Trata_Erro
                        End If
                        Exit Function
            
                    End If
        
            End Select
      
            Exit Do
      
        End If
              
    Loop Until EOF(sArqinput)
    
    'Atualizar remessa'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    
    Call RegistrarControleArquivo(sUsuario, _
       sSiglaSistema, _
       sSiglaRecurso, _
       sDescricaoRecurso, _
       lAmbienteID, _
       sArquivo, _
       lVersao, _
       lqtdReg, _
       lqtdReg - 2, _
       sDataOperacional, _
       RECEBIDO)
    If lAmbienteID_seg2 > 0 Then
        Call RegistrarControleArquivo(sUsuario, _
           sSiglaSistema, _
           sSiglaRecurso, _
           sDescricaoRecurso, _
           lAmbienteID_seg2, _
           sArquivo, _
           lVersao, _
           lqtdReg, _
           lqtdReg - 2, _
           sDataOperacional, _
           RECEBIDO)
    End If
    'Finalizando transa��o '''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    If Not ConfirmarTransacao(lConexaoAB) Then GoTo Trata_Erro
    If Not FecharConexao(lConexaoAB) Then GoTo Trata_Erro
    If lAmbienteID_seg2 > 0 Then
        If Not ConfirmarTransacao(lConexaoABS) Then GoTo Trata_Erro
        If Not FecharConexao(lConexaoABS) Then GoTo Trata_Erro
    End If
    Close #sArqinput
           
    Exit Function

Trata_Erro:
    
    ErrCode = Err.Number
    ErrDesc = Err.Description
    Close #sArqinput
    Call Err.Raise(ErrCode, , "SEGL0019.cls00106.ReceberInadimplenciaBB - " & ErrDesc)
    
End Function

Private Function fObterAmbienteExecucao(ByVal sSiglaSistema As String, _
                                        ByVal sSiglaRecurso As String, _
                                        ByVal sDescricaoRecurso As String, _
                                        ByVal lAmbiente_id_seg2 As Long, _
                                        ByVal lAmbiente_id As Long, _
                                        ByVal sVersao As String, _
                                        ByVal sProposta_BB As String, _
                                        Optional ByVal sProduto_Ext_Id As String) As Long

    Dim sSql        As String
    Dim rs          As ADODB.Recordset
    Dim lConexaoABS As Long
    
    sSql = ""
    Select Case sVersao
        Case "SEG444"
            sSql = sSql & "SELECT  proposta_tb.proposta_id                          " & vbNewLine
            sSql = sSql & "  FROM  proposta_tb                                      " & vbNewLine
            sSql = sSql & "  JOIN  proposta_fechada_tb                     ON proposta_fechada_tb.proposta_id  = proposta_tb.proposta_id " & vbNewLine
            sSql = sSql & "  JOIN  item_produto_tb                         ON item_produto_tb.produto_id       = proposta_tb.produto_id  " & vbNewLine
            sSql = sSql & "                                               AND item_produto_tb.ramo_id          = proposta_tb.ramo_id     " & vbNewLine
            sSql = sSql & " WHERE  proposta_tb.situacao <> 'x'                      " & vbNewLine
            sSql = sSql & "   AND  proposta_fechada_tb.proposta_bb =                " & sProposta_BB & vbNewLine
            sSql = sSql & "   AND  item_produto_tb.cod_produto_bb =                 " & sProduto_Ext_Id & vbNewLine
            sSql = sSql & "   AND  proposta_tb.situacao <> 'T'                      " & vbNewLine
            sSql = sSql & " UNION                                                   " & vbNewLine
            sSql = sSql & "SELECT  proposta_tb.proposta_id                          " & vbNewLine
            sSql = sSql & "  FROM  proposta_tb                                      " & vbNewLine
            sSql = sSql & "  JOIN  proposta_adesao_tb                      ON proposta_adesao_tb.proposta_id   = proposta_tb.proposta_id " & vbNewLine
            sSql = sSql & "  JOIN  item_produto_tb                         ON item_produto_tb.produto_id       = proposta_tb.produto_id  " & vbNewLine
            sSql = sSql & "                                               AND item_produto_tb.ramo_id          = proposta_tb.ramo_id     " & vbNewLine
            sSql = sSql & " WHERE  proposta_tb.situacao <> 'x'                      " & vbNewLine
            sSql = sSql & "   AND  proposta_adesao_tb.proposta_bb =                 " & sProposta_BB & vbNewLine
            sSql = sSql & "   AND  item_produto_tb.cod_produto_bb =                 " & sProduto_Ext_Id & vbNewLine
            sSql = sSql & "   AND  proposta_tb.situacao <> 'T'                      " & vbNewLine
        Case "SEG497D"
            sSql = sSql & "SELECT  proposta_adesao_tb.proposta_id                                  " & vbNewLine
            sSql = sSql & "  FROM  proposta_tb proposta_tb, proposta_adesao_tb proposta_adesao_tb  " & vbNewLine
            sSql = sSql & " Where  proposta_tb.proposta_id = proposta_adesao_tb.proposta_id        " & vbNewLine
            sSql = sSql & "   AND  proposta_adesao_tb.proposta_bb =                                " & sProposta_BB & vbNewLine
            sSql = sSql & "   AND  proposta_tb.produto_id = 11                                     " & vbNewLine
            sSql = sSql & "   AND  proposta_tb.situacao <> 'T'                                     " & vbNewLine
    
    End Select

    'Abrir uma nova conexao COM ABS
    lConexaoABS = AbrirConexao(sSiglaSistema, _
       lAmbiente_id_seg2, _
       sSiglaRecurso, _
       sDescricaoRecurso)
    
    'Pesquisa se a proposta_bb esta no ABS
    Set rs = Conexao_ExecutarSQL(sSiglaSistema, _
       lAmbiente_id_seg2, _
       sSiglaRecurso, _
       sDescricaoRecurso, _
       sSql, _
       lConexaoABS, _
       True)
    
    'Se estiver on ABS ja retorna o ambiente ABS
    fObterAmbienteExecucao = lAmbiente_id_seg2
    
    If rs.EOF Then
        'Retorna o ambiente AB se n�o estiver em ABS
        fObterAmbienteExecucao = lAmbiente_id
    End If
    
    'Limpa objetos e variaveis
    rs.Close
    If Not FecharConexao(lConexaoABS) Then GoTo Trata_ErrofObterAmbienteExecucao
    Set rs = Nothing
    Exit Function

Trata_ErrofObterAmbienteExecucao:
    
    ErrCode = Err.Number
    ErrDesc = Err.Description
    Call Err.Raise(ErrCode, , "SEGL0019.cls00106.fObterAmbienteExecucao - " & ErrDesc)
        
End Function

Public Sub IdentificarParcInadimplente(ByVal sUsuario As String, _
                                       ByVal sSiglaSistema As String, _
                                       ByVal sSiglaRecurso As String, _
                                       ByVal sDescricaoRecurso As String, _
                                       ByVal iAmbienteId As Integer, _
                                       ByVal sDataOperacional As String, _
                                       ByRef lRegProcessados As Long)

    Dim sSql As String
    Dim rs   As Recordset

    On Error GoTo Trata_Erro
    
    sSql = ""
    sSql = "EXEC ident_parcela_inadimplente_spi '" & Format(sDataOperacional, "yyyymmdd") & "','" & sUsuario & "'"
    
    Set rs = ExecutarSQL(sSiglaSistema, _
       iAmbienteId, _
       sSiglaRecurso, _
       sDescricaoRecurso, _
       sSql, _
       True)
    
    lRegProcessados = rs(0)
    
    Set rs = Nothing
            
    Exit Sub

Trata_Erro:
    
    Call Err.Raise(Err.Number, , "SEGL0019.cls00106.IdentificarParcInadimplente - " & Err.Description)
    
End Sub

Private Sub AtualizarBloqueioBB(ByVal sUsuario As String, _
                                ByVal sSiglaSistema As String, _
                                ByVal sSiglaRecurso As String, _
                                ByVal sDescricaoRecurso As String, _
                                ByVal iAmbienteId As Integer, _
                                ByVal lPropostaBB As Long, _
                                ByVal sArquivo As String, _
                                ByVal iNumCobranca As Integer, _
                                ByVal sBloqueioBB As String, _
                                ByVal lConexao As Long)
                                            
    '## Rotina de atualiza��o de bloqueio BB em agendamento cobran�a

    Dim sSql As String

    On Error GoTo Trata_Erro

    sSql = ""
    sSql = "EXEC segs3957_spu " & lPropostaBB & ","
    sSql = sSql & "'" & sArquivo & "',"
    sSql = sSql & iNumCobranca & ","
    sSql = sSql & "'" & sBloqueioBB & "',"
    sSql = sSql & "'" & sUsuario & "'"
        
    Call Conexao_ExecutarSQL(sSiglaSistema, _
       iAmbienteId, _
       sSiglaRecurso, _
       sDescricaoRecurso, _
       sSql, _
       lConexao, _
       False)
    
    Exit Sub

Trata_Erro:
    
    Call Err.Raise(Err.Number, , "SEGL0019.cls00106.AtualizarBloqueioBB - " & Err.Description)

End Sub

Public Sub BaixarInadimplencia(ByVal sUsuario As String, _
                               ByVal sSiglaSistema As String, _
                               ByVal sSiglaRecurso As String, _
                               ByVal sDescricaoRecurso As String, _
                               ByVal iAmbienteId As Integer, _
                               ByVal sDataOperacional As String, _
                               ByRef lRegProcessados As Long)

    '## Rotina de baixa de inadimpl�ncias

    Dim sSql As String
    Dim rs   As Recordset

    On Error GoTo Trata_Erro
    
    sSql = "EXEC segs3952_spu '" & sUsuario & "','" & Format(sDataOperacional, "yyyymmdd") & "'"
    
    Set rs = ExecutarSQL(sSiglaSistema, _
       iAmbienteId, _
       sSiglaRecurso, _
       sDescricaoRecurso, _
       sSql, _
       True)
                     
    lRegProcessados = rs(0)
    
    Exit Sub
    
Trata_Erro:
    
    Call Err.Raise(Err.Number, , "SEGL0019.cls00106.BaixarInadimplencia - " & Err.Description)
    
End Sub

Public Sub AderirInadimplencia(ByVal sUsuario As String, _
                               ByVal sSiglaSistema As String, _
                               ByVal sSiglaRecurso As String, _
                               ByVal sDescricaoRecurso As String, _
                               ByVal iAmbienteId As Integer, _
                               ByRef lRegProcessados As Long)

    '## Rotina de ades�o de inadimpl�ncias

    Dim sSql As String
    Dim rs   As Recordset

    On Error GoTo Trata_Erro
    
    sSql = "EXEC segs3958_spi '" & sUsuario & "'"
    
    Set rs = ExecutarSQL(sSiglaSistema, _
       iAmbienteId, _
       sSiglaRecurso, _
       sDescricaoRecurso, _
       sSql, _
       True)
    
    lRegProcessados = rs(0)
    
    Set rs = Nothing
       
    Exit Sub
    
Trata_Erro:
    
    Call Err.Raise(Err.Number, , "SEGL0019.cls00106.AderirInadimplencia - " & Err.Description)
    
End Sub
