VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cls00107"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Const CHAVE_FATURA_CANCELAMENTO = "FATURA_CANCELAMENTO"

Public Function GerarFatura(ByVal sUsuario As String, _
                            ByVal sSiglaSistema As String, _
                            ByVal sSiglaRecurso As String, _
                            ByVal sDescricaoRecurso As String, _
                            ByVal iAmbienteId As Integer, _
                            ByVal sDataOperacional As String) As Long

    '## Rotina de gera��o de fatura de cancelamento

    Dim rsFaturas                        As Recordset
    Dim lContFaturas                     As Long

    Dim lApolice                         As Long
    Dim lSucursal                        As Long
    Dim lSeguradora                      As Long
    Dim lRamo                            As Long
    Dim lFaturaEmissao                   As Long
    Dim lPropostaBasica                  As Long
    Dim lNrEndossoFinancGerado           As Long
    Dim lProduto                         As Long
    Dim sDtRecebimento                   As String
    Dim sDtIniPeriodo                    As String
    Dim sDtFimPeriodo                    As String
    Dim lMoeda                           As Long
    Dim sDtUltExecucao                   As String

    Dim cTotalImpSegurada                As Currency
    Dim cTotalValorCobranca              As Currency
    Dim cTotalValorPago                  As Currency
    Dim cTotalValorIof                   As Currency
    Dim cTotalValorIofBB                 As Currency
    Dim cTotalValorComissao              As Currency
    Dim cTotalValorComissaoBB            As Currency
    Dim cTotalValorIr                    As Currency
    Dim cTotalValorIrBB                  As Currency
    Dim cTotalValorComissaoEstipulante   As Currency
    Dim cTotalValorComissaoEstipulanteBB As Currency
    Dim cTotalValorRemuneracao           As Currency
    Dim cTotalValorRemuneracaoBB         As Currency
    Dim cTotalValorAdicFracionamento     As Currency
    Dim cTotalValorDesconto              As Currency
    Dim cPercCorretagem                  As Currency
    Dim cPercProLabore                   As Currency

    Dim lFaturaCanc                      As Long

    On Error GoTo TrataErro

    lContFaturas = 0
    
    ' Obtendo Data da �ltima execu��o ''''''''''''''''''''''''''''''''''''''''''''''''''
    
    sDtUltExecucao = ObterUltimaExecucao(sSiglaSistema, sSiglaRecurso, iAmbienteId, sDescricaoRecurso, CHAVE_FATURA_CANCELAMENTO)
    
    ' Selecionando Faturas '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    
    Set rsFaturas = ObterValores(sSiglaSistema, sSiglaRecurso, sDescricaoRecurso, iAmbienteId, sDtUltExecucao, sDataOperacional)
        
    While Not rsFaturas.EOF
     
        'Armazenando dados retornados '''''''''''''''''''''''''''''''''''''''''''''''''''
    
        lApolice = rsFaturas!apolice_id
        lSucursal = rsFaturas!Sucursal_seguradora_id
        lSeguradora = rsFaturas!Seguradora_cod_susep
        lRamo = rsFaturas!ramo_id
        lFaturaEmissao = rsFaturas!fatura_id
        lPropostaBasica = rsFaturas!proposta_basica_id
        lNrEndossoFinancGerado = rsFaturas!endosso_id
        lProduto = rsFaturas!produto_id
        sDtRecebimento = Format(sDataOperacional, "yyyymmdd")
        sDtIniPeriodo = Format(rsFaturas!dt_inicio_vigencia, "yyyymmdd")
        sDtFimPeriodo = Format(rsFaturas!Dt_Fim_Vigencia, "yyyymmdd")
        lMoeda = rsFaturas!seguro_moeda_id
        
        cTotalImpSegurada = Val(rsFaturas!Imp_Segurada)
        cTotalValorCobranca = Val(rsFaturas!val_cobranca)
        cTotalValorPago = Val(rsFaturas!val_pago)
        cTotalValorIof = Val(rsFaturas!val_iof)
        cTotalValorIofBB = Val(rsFaturas!val_iof_bb)
        cTotalValorComissao = Val(rsFaturas!Val_Comissao)
        cTotalValorComissaoBB = Val(rsFaturas!Val_Comissao_BB)
        cTotalValorIr = Val(rsFaturas!Val_ir)
        cTotalValorIrBB = Val(rsFaturas!Val_ir_bb)
        cTotalValorComissaoEstipulante = Val(rsFaturas!val_comissao_estipulante)
        cTotalValorComissaoEstipulanteBB = Val(rsFaturas!val_comissao_estipulante_bb)
        cTotalValorRemuneracao = Val(rsFaturas!Val_Remuneracao)
        cTotalValorRemuneracaoBB = Val(rsFaturas!Val_Remuneracao_bb)
        cTotalValorAdicFracionamento = Val(rsFaturas!Val_Adic_Fracionamento)
        cTotalValorDesconto = Val(rsFaturas!Val_Desconto)
        cPercProLabore = Val(rsFaturas!PercProLabore)
        cPercCorretagem = Val(rsFaturas!perc_corretagem)
            
        ' Obtendo o n�mero da fatura a ser gerada '''''''''''''''''''''''''''''''''''''''''''''''
        
        lFaturaCanc = ObterNumero(sUsuario, sSiglaSistema, sSiglaRecurso, sDescricaoRecurso, iAmbienteId, lApolice, lSucursal, lSeguradora, lRamo)
            
        ' Inserindo fatura de cancelamento ''''''''''''''''''''''''''''''''''''''''''''''''''''''
            
        Call Inserir(sUsuario, _
           sSiglaSistema, _
           sSiglaRecurso, _
           sDescricaoRecurso, _
           iAmbienteId, _
           lFaturaCanc, _
           lApolice, _
           lSucursal, _
           lSeguradora, _
           lRamo, _
           sDtRecebimento, _
           sDtIniPeriodo, _
           sDtFimPeriodo, _
           cPercCorretagem, _
           "c", _
           lPropostaBasica, _
           lNrEndossoFinancGerado, _
           cPercProLabore, _
           sDataOperacional)
            
        ' Atualizando valores de emissao ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        
        Call AbaterValorFaturaEmissao(sUsuario, _
           sSiglaSistema, _
           sSiglaRecurso, _
           sDescricaoRecurso, _
           iAmbienteId, _
           lApolice, _
           lRamo, _
           lFaturaEmissao, _
           lFaturaCanc, _
           lPropostaBasica, _
           lNrEndossoFinancGerado, _
           cTotalValorCobranca, _
           cTotalValorComissao, _
           cTotalValorIof, _
           cTotalValorComissaoEstipulante, _
           cTotalValorRemuneracao, _
           cTotalImpSegurada, _
           cTotalValorIr, _
           cTotalValorAdicFracionamento, _
           cTotalValorDesconto, _
           cTotalValorPago, _
           cTotalValorIrBB, _
           cTotalValorComissaoBB, _
           cTotalValorIofBB, _
           cTotalValorComissaoEstipulanteBB, cTotalValorRemuneracaoBB)
        
        ' Atualizando Agendamento '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        
        Call AtualizarAgendamento(sUsuario, _
           sSiglaSistema, _
           sSiglaRecurso, _
           sDescricaoRecurso, _
           iAmbienteId, _
           lApolice, _
           lRamo, _
           lFaturaEmissao, _
           lFaturaCanc, _
           lProduto, _
           sDtUltExecucao, _
           sDataOperacional)
        
        lContFaturas = lContFaturas + 1
        
        rsFaturas.MoveNext
        
    Wend
     
    Set rsFaturas = Nothing
    
    Call AlterarUltimaExecucao(sUsuario, sSiglaSistema, sSiglaRecurso, sDescricaoRecurso, iAmbienteId, CHAVE_FATURA_CANCELAMENTO)
    
    GerarFatura = lContFaturas

    Exit Function

TrataErro:
    
    Call Err.Raise(Err.Number, , "SEGL0019.cls00107.GerarFatura - " & Err.Description)

End Function

Private Function ObterValores(ByVal sSiglaSistema As String, _
                              ByVal sSiglaRecurso As String, _
                              ByVal sDescricaoRecurso As String, _
                              ByVal iAmbienteId As Integer, _
                              ByVal sDtUltExecucao As String, _
                              ByVal sDataOperacional As String) As Recordset

    '## Rotina de obten��o de valores de faturas

    Dim sSql           As String
    Dim rsObterValores As New Recordset

    On Error GoTo TrataErro
    
    sSql = ""
    sSql = sSql & "EXEC fatura_cancelamento_spi "
    sSql = sSql & "'" & Format(sDataOperacional, "yyyymmdd") & "',"
    sSql = sSql & "'" & Format(sDtUltExecucao, "yyyymmdd") & "'"
    
    Set rsObterValores = ExecutarSQL(sSiglaSistema, _
       iAmbienteId, _
       sSiglaRecurso, _
       sDescricaoRecurso, _
       sSql, _
       True)

    Set ObterValores = rsObterValores
           
    Exit Function

TrataErro:
    
    Call Err.Raise(Err.Number, , "SEGL0019.cls00107.ObterValores - " & Err.Description)
    
End Function

Private Function ObterNumero(ByVal sUsuario As String, _
                             ByVal sSiglaSistema As String, _
                             ByVal sSiglaRecurso As String, _
                             ByVal sDescricaoRecurso As String, _
                             ByVal iAmbienteId As Integer, _
                             ByVal lApolice As Long, _
                             ByVal lSucursal As Long, _
                             ByVal lSeguradora As Long, _
                             ByVal lRamo As Long) As Long

    Dim rs   As Recordset
    Dim sSql As String

    On Error GoTo TrataErro
   
    sSql = "exec chave_fatura_spu "
    sSql = sSql & lApolice & "," & lSucursal & "," & lSeguradora & "," & lRamo & ",'" & sUsuario & "'"
    
    Set rs = ExecutarSQL(sSiglaSistema, _
       iAmbienteId, _
       sSiglaRecurso, _
       sDescricaoRecurso, _
       sSql, _
       True)

    If Not rs.EOF Then
        ObterNumero = Format(rs(0), "00000000")
    Else
        ObterNumero = 1
    End If
       
    Set rs = Nothing

    Exit Function
    
TrataErro:
    
    Call Err.Raise(Err.Number, , "SEGL0019.cls00107.ObterNumero - " & Err.Description)
    
End Function

Private Sub Inserir(ByVal sUsuario As String, _
                    ByVal sSiglaSistema As String, _
                    ByVal sSiglaRecurso As String, _
                    ByVal sDescricaoRecurso As String, _
                    ByVal iAmbienteId As Integer, _
                    ByVal lFatura As Long, _
                    ByVal lApolice As Long, _
                    ByVal lSucursal As Long, _
                    ByVal lSeguradora As Long, _
                    ByVal lRamo As Long, _
                    ByVal sDtRecebimento As String, _
                    ByVal sDt_Ini_Vigencia As String, _
                    ByVal sDt_Fim_Vigencia As String, _
                    ByVal cPercCorretagem As Currency, _
                    ByVal sSituacao As String, _
                    ByVal lPropostaBasica As Long, _
                    ByVal lNrEndossoFinancGerado As Long, _
                    ByVal cPercProlaboreAnt, _
                    ByVal sDataOperacional As String)

    '## Rotina de inser��o de fatura de cancelamento

    Dim sSql As String

    On Error GoTo TrataErro
 
    sSql = "EXEC Fatura_spi "
    sSql = sSql & lFatura & ", "
    sSql = sSql & lApolice & ", "
    sSql = sSql & lSucursal & ", "
    sSql = sSql & lSeguradora & ", "
    sSql = sSql & lRamo & ", "
    sSql = sSql & "'" & sDtRecebimento & "', '"
    sSql = sSql & sDt_Ini_Vigencia & "', '"
    sSql = sSql & sDt_Fim_Vigencia & "', "
    sSql = sSql & "0.00,"
    sSql = sSql & "0.00,"
    sSql = sSql & "0.00,"
    sSql = sSql & MudaVirgulaParaPonto(cPercCorretagem) & ", '"
    sSql = sSql & sUsuario & "', '"
    sSql = sSql & sSituacao & "', "
    sSql = sSql & lPropostaBasica & ", "
    sSql = sSql & lNrEndossoFinancGerado & ", '"
    sSql = sSql & "FATURA AUTOM�TICA" & "', "
    sSql = sSql & "NULL, "
    sSql = sSql & MudaVirgulaParaPonto(cPercProlaboreAnt) & ","
    sSql = sSql & "'" & Format(sDataOperacional, "yyyymmdd") & "',"
    sSql = sSql & "''"
    
    Call ExecutarSQL(sSiglaSistema, _
       iAmbienteId, _
       sSiglaRecurso, _
       sDescricaoRecurso, _
       sSql, _
       False)
        
    Exit Sub

TrataErro:
    
    Call Err.Raise(Err.Number, , "SEGL0019.cls00107.Inserir - " & Err.Description)
   
End Sub

Private Function MudaVirgulaParaPonto(ByVal valor As String) As String

    '## Rotina que troca virgula por ponto em um n�mero

    If InStr(valor, ",") = 0 Then
        MudaVirgulaParaPonto = valor
    Else
        valor = Mid$(valor, 1, InStr(valor, ",") - 1) + "." + Mid$(valor, InStr(valor, ",") + 1)
        MudaVirgulaParaPonto = valor
    End If

End Function

Private Sub AbaterValorFaturaEmissao(ByVal sUsuario As String, _
                                     ByVal sSiglaSistema As String, _
                                     ByVal sSiglaRecurso As String, _
                                     ByVal sDescricaoRecurso As String, _
                                     ByVal iAmbienteId As Integer, _
                                     ByVal lApolice As Long, _
                                     ByVal lRamo As Long, _
                                     ByVal lFaturaEmissao As Long, _
                                     ByVal lFaturaCanc As Long, _
                                     ByVal lPropostaBasica As Long, _
                                     ByVal lNrEndossoFinancGerado As Long, _
                                     ByVal cTotalValorCobranca As Currency, _
                                     ByVal cTotalValorComissao As Currency, _
                                     ByVal cTotalValorIof As Currency, _
                                     ByVal cTotalValorComissaoEstipulante As Currency, _
                                     ByVal cTotalValorRemuneracao As Currency, _
                                     ByVal cTotalImpSegurada As Currency, _
                                     ByVal cTotalValorIr As Currency, _
                                     ByVal cTotalValorAdicFracionamento As Currency, _
                                     ByVal cTotalValorDesconto As Currency, _
                                     ByVal cTotalValorPago As Currency, _
                                     ByVal cTotalValorIrBB As Currency, _
                                     ByVal cTotalValorComissaoBB As Currency, _
                                     ByVal cTotalValorIofBB As Currency, _
                                     ByVal cTotalValorComissaoEstipulanteBB As Currency, ByVal cTotalValorRemuneracaoBB As Currency)
                                    
    '## Rotina de abatimento de valores da fatura de emiss�o

    Dim sSql As String

    On Error GoTo TrataErro

    sSql = "EXEC Migra_Valores_Emissao_Baixa_spu "
    sSql = sSql & lApolice & ","
    sSql = sSql & lRamo & ","
    sSql = sSql & lFaturaEmissao & ","
    sSql = sSql & lFaturaCanc & ","
    sSql = sSql & lPropostaBasica & ","
    sSql = sSql & lNrEndossoFinancGerado & ","
    sSql = sSql & MudaVirgulaParaPonto(cTotalValorCobranca) & ","
    sSql = sSql & MudaVirgulaParaPonto(cTotalValorComissao) & ","
    sSql = sSql & MudaVirgulaParaPonto(cTotalValorIof) & ","
    sSql = sSql & MudaVirgulaParaPonto(cTotalValorComissaoEstipulante) & ", "
    sSql = sSql & MudaVirgulaParaPonto(cTotalValorRemuneracao) & ", "
    sSql = sSql & MudaVirgulaParaPonto(cTotalImpSegurada) & ", '"
    sSql = sSql & sUsuario & "',"
    sSql = sSql & MudaVirgulaParaPonto(cTotalValorIr) & ", "
    sSql = sSql & MudaVirgulaParaPonto(cTotalValorAdicFracionamento) & ", "
    sSql = sSql & MudaVirgulaParaPonto(cTotalValorDesconto) & ", "
    sSql = sSql & MudaVirgulaParaPonto(cTotalValorPago) & ", "
    sSql = sSql & MudaVirgulaParaPonto(cTotalValorIrBB) & ", "
    sSql = sSql & MudaVirgulaParaPonto(cTotalValorComissaoBB) & ", "
    sSql = sSql & MudaVirgulaParaPonto(cTotalValorIofBB) & ", "
    sSql = sSql & MudaVirgulaParaPonto(cTotalValorComissaoEstipulanteBB) & ", "
    sSql = sSql & MudaVirgulaParaPonto(cTotalValorRemuneracaoBB)
    
    Call ExecutarSQL(sSiglaSistema, _
       iAmbienteId, _
       sSiglaRecurso, _
       sDescricaoRecurso, _
       sSql, _
       False)
    
    Exit Sub

TrataErro:
    
    Call Err.Raise(Err.Number, , "SEGL0019.cls00107.AbaterValorFaturaEmissao - " & Err.Description)
   
End Sub

Private Sub AtualizarAgendamento(ByVal sUsuario As String, _
                                 ByVal sSiglaSistema As String, _
                                 ByVal sSiglaRecurso As String, _
                                 ByVal sDescricaoRecurso As String, _
                                 ByVal iAmbienteId As Integer, _
                                 ByVal lApolice As Long, _
                                 ByVal lRamo As Long, _
                                 ByVal lFaturaEmissao As Long, _
                                 ByVal lFaturaCanc As Long, _
                                 ByVal lProduto As Long, _
                                 ByVal sDtUltExecucao As String, _
                                 ByVal sDataOperacional As String)

    '## Rotina de atualiza��o de agendamento

    Dim sSql As String

    On Error GoTo TrataErro

    sSql = "EXEC agendamento_fatura_spu "
    sSql = sSql & lApolice & ","
    sSql = sSql & lRamo & ","
    sSql = sSql & lFaturaEmissao & ","
    sSql = sSql & lFaturaCanc & ","
    sSql = sSql & lProduto & ","
    sSql = sSql & "NULL,"
    sSql = sSql & "NULL,"
    sSql = sSql & "NULL,"
    sSql = sSql & "'" & sUsuario & "',"
    sSql = sSql & "'c',"
    sSql = sSql & "'" & Format(sDataOperacional, "yyyymmdd") & "',"
    sSql = sSql & "'" & Format(sDtUltExecucao, "yyyymmdd") & "',"
    sSql = sSql & "NULL"
    
    Call ExecutarSQL(sSiglaSistema, _
       iAmbienteId, _
       sSiglaRecurso, _
       sDescricaoRecurso, _
       sSql, _
       False)
    
    Exit Sub

TrataErro:

    Call Err.Raise(Err.Number, , "SEGL0019.cls00107.AtualizarAgendamento - " & Err.Description)
   
End Sub

Private Function ObterUltimaExecucao(ByVal sSiglaSistema As String, _
                                     ByVal sSiglaRecurso As String, _
                                     ByVal iAmbienteId As Integer, _
                                     ByVal sDescricaoRecurso As String, _
                                     ByVal sChave As String) As String

    '## Rotina de obten��o da data da �ltima execu��o

    Dim rs   As Recordset
    Dim sSql As String

    On Error GoTo TrataErro
    
    sSql = ""
    sSql = sSql & "SELECT num_vigente " & vbNewLine
    sSql = sSql & "  FROM chave_tb"
    sSql = sSql & " WHERE chave = '" & sChave & "'"
    
    Set rs = ExecutarSQL(sSiglaSistema, _
       iAmbienteId, _
       sSiglaRecurso, _
       sDescricaoRecurso, _
       sSql, _
       True)
    
    If rs.EOF Then
        ObterUltimaExecucao = "01/07/2000"
    Else
        ObterUltimaExecucao = rs("num_vigente")
        ObterUltimaExecucao = Mid(ObterUltimaExecucao, 7, 2) + "/" + Mid(ObterUltimaExecucao, 5, 2) + "/" + Mid(ObterUltimaExecucao, 1, 4)
    End If
    
    Set rs = Nothing

    Exit Function

TrataErro:
    
    Call Err.Raise(Err.Number, , "SEGL0019.cls00107.ObterUltimaExecucao - " & Err.Description)

End Function

Private Sub AlterarUltimaExecucao(ByVal sUsuario As String, _
                                  ByVal sSiglaSistema As String, _
                                  ByVal sSiglaRecurso As String, _
                                  ByVal sDescricaoRecurso As String, _
                                  ByVal iAmbienteId As Integer, _
                                  ByVal sChave As String)

    '## Rotina de altera��o da data da �ltima execu��o

    Dim sSql As String

    On Error GoTo TrataErro
    
    sSql = "EXEC chave_execucao_spu " & "'" & sChave & "','" & sUsuario & "'"

    Call ExecutarSQL(sSiglaSistema, _
       iAmbienteId, _
       sSiglaRecurso, _
       sDescricaoRecurso, _
       sSql, _
       False)
    
    Exit Sub

TrataErro:
    
    Call Err.Raise(Err.Number, , "SEGL0019.cls00107.AlterarUltimaExecucao - " & Err.Description)

End Sub
