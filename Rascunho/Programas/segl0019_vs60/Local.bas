Attribute VB_Name = "Local"
Public Const gsCHAVESISTEMA = "*&�#{}[]��@&%ç���?;�"
Public Const gsSIGLASISTEMA = "SEGBR"

Public Sub adSQL(ByRef sSql As String, ByVal sCadeia As String)
    'Jorfilho 04/11/2002 - Concatena a query com vbNewLine
    sSql = sSql & sCadeia & vbNewLine
End Sub

Function MudaVirgulaParaPonto(ByVal valor As String) As String

    If InStr(valor, ",") = 0 Then
        MudaVirgulaParaPonto = valor
    Else
        valor = Mid$(valor, 1, InStr(valor, ",") - 1) + "." + Mid$(valor, InStr(valor, ",") + 1)
        MudaVirgulaParaPonto = valor
    End If

End Function
Function LerArquivoIni(ByVal Secao As String, _
                       ByVal Item As String, _
                       ByVal iAmbiente_id As Integer, _
                       Optional ByVal oSABL0010 As Variant) As String
  
    If IsMissing(oSABL0010) Then
        Set oSABL0010 = CreateObject("SABL0010.cls00009")
    End If
    LerArquivoIni = oSABL0010.RetornaValorAmbiente(gsSIGLASISTEMA, Secao, Item, gsCHAVESISTEMA, glAmbiente_id)

End Function

'------------------------------------------------------------------------------------------
'
' Respons�vel : Jo�o Mac-Cormick
' Dt Desenv : 03/06/2000
'
' retorna o dv de uma conta corrente ou de uma ag�ncia conforme regra do BB
Public Function calcula_dv_cc_ou_agencia(ByVal Parte As String) As String
 
    Dim Peso As Integer
    Dim Soma As Integer
    Dim Parcela As Integer
    Dim dv As Integer, i
    Dim result As String
     
    Peso = 9
    Soma = 0
    For i = Len(Parte) To 1 Step -1
        Parcela = Peso * Val(Mid(Parte, i, 1))
        Soma = Soma + Parcela
        Peso = Peso - 1
        If Peso < 2 Then Peso = 9
    Next i
    
    dv = (Soma Mod 11)
    If dv = 10 Then
        result = "X"
    Else
        result = Format(dv, "0")
    End If
    calcula_dv_cc_ou_agencia = result
End Function
