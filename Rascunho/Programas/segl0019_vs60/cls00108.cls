VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cls00108"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit

Public Sub Incluir(ByVal sUsuario As String, _
                   ByVal sSiglaSistema As String, _
                   ByVal sSiglaRecurso As String, _
                   ByVal sDescricaoRecurso As String, _
                   ByVal iAmbienteId As Integer, _
                   ByVal iRamoId As Integer, _
                   ByVal lSubramoId As Long, _
                   ByVal lProdutoId As Long, _
                   ByVal sDtInicioVigenciaSbr As String, _
                   ByVal sAguardaSolicitacaoCancelamentoBB As String, _
                   ByVal iDiasRegistroInadimplencia As Integer, _
                   ByVal sEmiteAvisoInadimplenciaPrimeiraParc As String, _
                   ByVal sEmiteAvisoInadimplenciaDemaisParc As String, _
                   ByVal iDiasParaEmissaoAviso As Integer, _
                   ByVal sCancelaDocumento As String, _
                   ByVal sTpCriterioCancelamento As String, _
                   ByVal iDiasParaCancPrimeiraParc As Integer, _
                   ByVal iDiasParaCancDemaisParc As Integer, _
                   ByVal sInadimplenciaRequerRetornoBB As String, _
                   ByVal sCancRequerAvisoInadimplencia As String, _
                   ByVal sCancelaEndosso As String, _
                   ByVal sCancAguardaPeriodoCobertura As String, _
                   ByVal sEmiteCartaCancelamento As String, _
                   ByVal iParcelasSemPagamento As Integer, _
                   ByVal sIdentificacaoInadimplenciaBB As String, ByVal sIdentInadimplenciaBBParc1 As String, ByVal sIdentInadimplenciaBBDemaisParc As String, ByVal sCancClientePrivate As String, ByVal sCancCorrIndependente As String, ByVal sCancAgenciaCorporate As String, ByVal lTipoCancelamento As Long, ByVal sArquivoAvisoInadimplencia As String, ByVal sArquivoCartaCancelamento As String, ByVal sTpCalcPeriodoCobertura As String, ByVal sDocsACancelar As String, ByVal sDataOperacional As String, ByVal sWorkflow As String, Optional ByVal colRegras As Object, Optional ByVal motivo_alteracao As String, Optional ByVal qtde_parcelas_carta_inadimplencia As Integer, Optional ByVal parcelas_consecutivas_inadimplencia As String, Optional strDataCorte As String, Optional enviarAvisoGridComRetorno As String, Optional enviarAvisoGridSemRetorno As String, Optional enviarParaGrid As String)

    'Alterado por Ren�n/G&P - Demanda 207875 - 17/05/2007 - Inclus�o do campo docs_a_cancelar, par�metro sDocsACancelar

    '## Rotina de inclus�o em parametro_inadimplencia_tb

    Dim sSql                           As String
    Dim rs                             As Recordset
    Dim objRegras                      As Object
    Dim lSeqParametroInadimplenciaNovo As Long
    Dim iConexao                       As Integer

    On Error GoTo TrataErro
        
    iConexao = AbrirConexao(sSiglaSistema, _
       iAmbienteId, _
       sSiglaRecurso, _
       sDescricaoRecurso)
                      
    If Not AbrirTransacao(iConexao) Then GoTo TrataErro
    
    'Fechando vig�ncia do registro corrente '''''''''''''''''''''''''''
    
    Call FecharVigencia(sUsuario, _
       sSiglaSistema, _
       sSiglaRecurso, _
       sDescricaoRecurso, _
       iAmbienteId, _
       iRamoId, _
       lSubramoId, _
       lProdutoId, _
       sDataOperacional, _
       iConexao)
        
    'Incluindo os novos parametros de inadimpl�ncia do subramo''''''''''''
    sSql = ""
    sSql = sSql & "EXEC parametro_inadimplencia_spi " & vbNewLine
    sSql = sSql & iRamoId & vbNewLine
    sSql = sSql & "," & lSubramoId & vbNewLine
    sSql = sSql & "," & lProdutoId & vbNewLine
    sSql = sSql & ",'" & Format(sDtInicioVigenciaSbr, "yyyymmdd") & "'" & vbNewLine
    sSql = sSql & ",'" & Format(sDataOperacional, "yyyymmdd") & "'" & vbNewLine
    sSql = sSql & ",NULL" & vbNewLine
    sSql = sSql & ",'" & sAguardaSolicitacaoCancelamentoBB & "'" & vbNewLine
    sSql = sSql & "," & iDiasRegistroInadimplencia & vbNewLine
    sSql = sSql & ",'" & sEmiteAvisoInadimplenciaPrimeiraParc & "'" & vbNewLine
    sSql = sSql & ",'" & sEmiteAvisoInadimplenciaDemaisParc & "'" & vbNewLine
    sSql = sSql & "," & iDiasParaEmissaoAviso & vbNewLine
    sSql = sSql & ",'" & sCancelaDocumento & "'" & vbNewLine
    
    If Trim(sTpCriterioCancelamento) <> "" Then
        sSql = sSql & ",'" & sTpCriterioCancelamento & "'" & vbNewLine
    Else
        sSql = sSql & ",NULL" & vbNewLine
    End If
    
    sSql = sSql & "," & iDiasParaCancPrimeiraParc & vbNewLine
    sSql = sSql & "," & iDiasParaCancDemaisParc & vbNewLine
    sSql = sSql & ",'" & sInadimplenciaRequerRetornoBB & "'" & vbNewLine
    sSql = sSql & ",'" & sCancRequerAvisoInadimplencia & "'" & vbNewLine
    sSql = sSql & ",'" & sCancAguardaPeriodoCobertura & "'" & vbNewLine
    sSql = sSql & ",'" & sEmiteCartaCancelamento & "'" & vbNewLine
    sSql = sSql & "," & iParcelasSemPagamento & vbNewLine
    sSql = sSql & ",'" & sIdentificacaoInadimplenciaBB & "'" & vbNewLine
    sSql = sSql & ",'" & sIdentInadimplenciaBBParc1 & "'" & vbNewLine
    sSql = sSql & ",'" & sIdentInadimplenciaBBDemaisParc & "'" & vbNewLine
    sSql = sSql & ",'" & sCancAgenciaCorporate & "'" & vbNewLine
    sSql = sSql & ",'" & sCancCorrIndependente & "'" & vbNewLine
    sSql = sSql & ",'" & sCancClientePrivate & "'" & vbNewLine
    sSql = sSql & ",'" & sCancelaEndosso & "'" & vbNewLine
    If lTipoCancelamento > 0 Then
        sSql = sSql & "," & lTipoCancelamento & vbNewLine
    Else
        sSql = sSql & ",NULL" & vbNewLine
    End If
    sSql = sSql & ",'" & sArquivoAvisoInadimplencia & "'" & vbNewLine
    sSql = sSql & ",'" & sArquivoCartaCancelamento & "'" & vbNewLine
    If Trim(sTpCalcPeriodoCobertura) <> "" Then
        sSql = sSql & ",'" & sTpCalcPeriodoCobertura & "'" & vbNewLine
    Else
        sSql = sSql & ",NULL" & vbNewLine
    End If
    sSql = sSql & ",'" & sDocsACancelar & "'" & vbNewLine 'Incluido por Ren�n/G&P - Demanda 207875
    sSql = sSql & ",'" & sWorkflow & "'" & vbNewLine
    sSql = sSql & ",'" & sUsuario & "'" & vbNewLine
    'Demanda 163198 - Marcelo Ferreira - Confitec Sistemas - 2008-07-04
    'Inclus�o de novos campos para hist�rico e outros dados
    '-------------------------------------------------------------------------
    sSql = sSql & ",'" & motivo_alteracao & "'" & vbNewLine
    sSql = sSql & ", " & qtde_parcelas_carta_inadimplencia & vbNewLine
    sSql = sSql & ",'" & parcelas_consecutivas_inadimplencia & "'"

    ' Demanda 15474469 - Felipe Marinho - Confitec
    If Not IsEmpty(strDataCorte) And Not IsNull(strDataCorte) And UCase(strDataCorte) <> "NULL" Then
        sSql = sSql & ",'" & strDataCorte & "'" & vbNewLine
    End If
    
    'Demanda 16255536 - Revis�o do processo de cancelamento por inadimpl�ncia - Rogerson - Nova
    If Not IsEmpty(enviarAvisoGridComRetorno) And Not IsNull(enviarAvisoGridComRetorno) And UCase(enviarAvisoGridComRetorno) <> "NULL" Then
        sSql = sSql & ",@enviar_aviso_grid_com_retorno = " & enviarAvisoGridComRetorno & vbNewLine
    Else
        sSql = sSql & ",0" & vbNewLine
    End If
    
    If Not IsEmpty(enviarAvisoGridSemRetorno) And Not IsNull(enviarAvisoGridSemRetorno) And UCase(enviarAvisoGridSemRetorno) <> "NULL" Then
        sSql = sSql & ",@enviar_aviso_grid_sem_retorno = " & enviarAvisoGridSemRetorno & vbNewLine
    Else
        sSql = sSql & ",0" & vbNewLine
    End If
    
    If Not IsEmpty(enviarParaGrid) And Not IsNull(enviarParaGrid) And UCase(enviarParaGrid) <> "NULL" Then
        sSql = sSql & ",@enviar_para_grid = " & enviarParaGrid & vbNewLine
    Else
        sSql = sSql & ",0" & vbNewLine
    End If
    
    'FIM Demanda 16255536 - Revis�o do processo de cancelamento por inadimpl�ncia - Rogerson - Nova

    Set rs = Conexao_ExecutarSQL(sSiglaSistema, _
       iAmbienteId, _
       sSiglaRecurso, _
       sDescricaoRecurso, _
       sSql, _
       iConexao)

    lSeqParametroInadimplenciaNovo = rs(0)
          
    If Not IsMissing(colRegras) Then
          
        For Each objRegras In colRegras
            Call AssociarRegra(sUsuario, _
               sSiglaSistema, _
               sSiglaRecurso, _
               sDescricaoRecurso, _
               iAmbienteId, _
               lSeqParametroInadimplenciaNovo, _
               objRegras.Regra_id, _
               iConexao)
        Next
    
    End If
    
    Set objRegras = Nothing
    
    If Not ConfirmarTransacao(iConexao) Then GoTo TrataErro
    
    If Not FecharConexao(iConexao) Then GoTo TrataErro
    
    Exit Sub
    
TrataErro:
    
    Call Err.Raise(Err.Number, , "SEGL0019.cls00108.Incluir - " & Err.Description)
    
End Sub

Public Function Consultar(ByVal sSiglaSistema As String, _
                          ByVal sSiglaRecurso As String, _
                          ByVal sDescricaoRecurso As String, _
                          ByVal iAmbienteId As Integer, _
                          Optional ByVal iTipoPesquisa As Integer, _
                          Optional ByVal sConteudo As String, _
                          Optional ByVal sConteudo2 As String, _
                          Optional ByVal sConteudo3 As String) As Recordset

    '## Rotina de consulta em parametro_inadimplencia_tb

    Dim sSql As String

    On Error GoTo Trata_Erro

    sSql = ""
    sSql = "      SELECT " & vbNewLine
    sSql = sSql & "      parametro_inadimplencia_tb.seq_parametro_inadimplencia," & vbNewLine
    sSql = sSql & "      parametro_inadimplencia_tb.ramo_id," & vbNewLine
    sSql = sSql & "      parametro_inadimplencia_tb.subramo_id," & vbNewLine
    sSql = sSql & "      parametro_inadimplencia_tb.produto_id," & vbNewLine
    sSql = sSql & "      parametro_inadimplencia_tb.dt_inicio_vigencia_sbr," & vbNewLine
    sSql = sSql & "      parametro_inadimplencia_tb.dt_inicio_vigencia," & vbNewLine
    sSql = sSql & "      parametro_inadimplencia_tb.dt_fim_vigencia," & vbNewLine
    sSql = sSql & "      parametro_inadimplencia_tb.aguarda_solicitacao_cancelamento_bb," & vbNewLine
    sSql = sSql & "      ISNULL(parametro_inadimplencia_tb.dias_registro_inadimplencia,0) AS dias_registro_inadimplencia," & vbNewLine
    sSql = sSql & "      parametro_inadimplencia_tb.emite_aviso_inadimplencia_primeira_parc," & vbNewLine
    sSql = sSql & "      parametro_inadimplencia_tb.emite_aviso_inadimplencia_demais_parc," & vbNewLine
    sSql = sSql & "      ISNULL(parametro_inadimplencia_tb.dias_para_emissao_aviso,0) AS dias_para_emissao_aviso," & vbNewLine
    sSql = sSql & "      parametro_inadimplencia_tb.cancela_documento," & vbNewLine
    sSql = sSql & "      parametro_inadimplencia_tb.tp_criterio_cancelamento," & vbNewLine
    sSql = sSql & "      ISNULL(parametro_inadimplencia_tb.dias_para_canc_primeira_parc,0) AS dias_para_canc_primeira_parc," & vbNewLine
    sSql = sSql & "      ISNULL(parametro_inadimplencia_tb.dias_para_canc_demais_parc,0) AS dias_para_canc_demais_parc," & vbNewLine
    sSql = sSql & "      parametro_inadimplencia_tb.inadimplencia_requer_retorno_bb," & vbNewLine
    sSql = sSql & "      parametro_inadimplencia_tb.canc_requer_aviso_inadimplencia," & vbNewLine
    sSql = sSql & "      parametro_inadimplencia_tb.canc_aguarda_periodo_cobertura," & vbNewLine
    sSql = sSql & "      parametro_inadimplencia_tb.emite_carta_cancelamento," & vbNewLine
    sSql = sSql & "      ISNULL(parametro_inadimplencia_tb.canc_parcelas_sem_pagamento,0) AS parcelas_sem_pagamento," & vbNewLine
    sSql = sSql & "      parametro_inadimplencia_tb.identificacao_inadimplencia_bb," & vbNewLine
    sSql = sSql & "      parametro_inadimplencia_tb.tp_cancelamento,"
    sSql = sSql & "      parametro_inadimplencia_tb.arquivo_aviso_inadimplencia," & vbNewLine
    sSql = sSql & "      parametro_inadimplencia_tb.arquivo_carta_cancelamento," & vbNewLine
    sSql = sSql & "      parametro_inadimplencia_tb.tp_calc_periodo_cobertura," & vbNewLine
    sSql = sSql & "      ramo_tb.nome as nome_ramo, " & vbNewLine
    sSql = sSql & "      subramo_tb.nome as nome_subramo," & vbNewLine
    sSql = sSql & "      produto_tb.nome as nome_produto," & vbNewLine
    sSql = sSql & "      parametro_inadimplencia_tb.ident_inadimplencia_bb_primeira_parc," & vbNewLine
    sSql = sSql & "      parametro_inadimplencia_tb.ident_inadimplencia_bb_demais_parc," & vbNewLine
    sSql = sSql & "      parametro_inadimplencia_tb.cancela_agencia_corporate," & vbNewLine
    sSql = sSql & "      parametro_inadimplencia_tb.cancela_corretor_independente," & vbNewLine
    sSql = sSql & "      parametro_inadimplencia_tb.cancela_cliente_private," & vbNewLine
    sSql = sSql & "      parametro_inadimplencia_tb.cancela_endosso," & vbNewLine
    sSql = sSql & "      parametro_inadimplencia_tb.docs_a_cancelar," & vbNewLine
    sSql = sSql & "      parametro_inadimplencia_tb.gera_workflow" & vbNewLine
    'Demanda 163198 - Marcelo Ferreira - Confitec Sistemas - 2008-07-04
    'Inclus�o de novos campos para hist�ricos e outros dados
    '-------------------------------------------------------------------------
    sSql = sSql & "    , parametro_inadimplencia_tb.motivo_alteracao" & vbNewLine
    sSql = sSql & "    , parametro_inadimplencia_tb.qtde_parcelas_carta_inadimplencia " & vbNewLine
    sSql = sSql & "    , parametro_inadimplencia_tb.parcelas_consecutivas_inadimplencia" & vbNewLine
    
    ' Demanda 15474469 - Felipe Marinho - Confitec
    sSql = sSql & "    , parametro_inadimplencia_tb.data_corte" & vbNewLine
    
    '16255536 - Revis�o do processo de cancelamento por inadimpl�ncia - Rogerson - Nova Consultoria
    sSql = sSql & "    , parametro_inadimplencia_tb.enviar_aviso_grid_com_retorno" & vbNewLine
    sSql = sSql & "    , parametro_inadimplencia_tb.enviar_aviso_grid_sem_retorno" & vbNewLine
    sSql = sSql & "    , parametro_inadimplencia_tb.enviar_para_grid" & vbNewLine
    
    sSql = sSql & "    , ramo_tb.tp_ramo_id" & vbNewLine
    
    'Incluido por Ren�n/G&P - Demanda 207875
    sSql = sSql & " FROM parametro_inadimplencia_tb parametro_inadimplencia_tb" & vbNewLine
    sSql = sSql & " JOIN ramo_tb " & vbNewLine
    sSql = sSql & "   ON ramo_tb.ramo_id = parametro_inadimplencia_tb.ramo_id " & vbNewLine
    sSql = sSql & " JOIN subramo_tb " & vbNewLine
    sSql = sSql & "   ON subramo_tb.ramo_id = parametro_inadimplencia_tb.ramo_id " & vbNewLine
    sSql = sSql & "  AND subramo_tb.subramo_id = parametro_inadimplencia_tb.subramo_id " & vbNewLine
    sSql = sSql & "  AND subramo_tb.dt_fim_vigencia_sbr IS NULL" & vbNewLine
    sSql = sSql & " JOIN produto_tb " & vbNewLine
    sSql = sSql & "   ON produto_tb.produto_id = parametro_inadimplencia_tb.produto_id " & vbNewLine
    sSql = sSql & "WHERE parametro_inadimplencia_tb.dt_fim_vigencia IS NULL" & vbNewLine
    
    Select Case iTipoPesquisa
  
        Case 1 'Sequencial
            sSql = sSql & "AND parametro_inadimplencia_tb.seq_parametro_inadimplencia = " & sConteudo & vbNewLine
            sSql = sSql & "ORDER BY parametro_inadimplencia_tb.seq_parametro_inadimplencia"
      
        Case 2 'Ramo
            sSql = sSql & "AND ramo_tb.ramo_id = " & sConteudo & vbNewLine
            sSql = sSql & "ORDER BY parametro_inadimplencia_tb.ramo_id,parametro_inadimplencia_tb.subramo_id "
    
        Case 3 'Subramo
            sSql = sSql & "AND subramo_tb.subramo_id = " & sConteudo & vbNewLine
            sSql = sSql & "ORDER BY parametro_inadimplencia_tb.ramo_id,parametro_inadimplencia_tb.subramo_id "
        
        Case 4 'Produto
            sSql = sSql & "AND produto_tb.produto_id = " & sConteudo & vbNewLine
            sSql = sSql & "ORDER BY parametro_inadimplencia_tb.ramo_id,parametro_inadimplencia_tb.subramo_id "
      
        Case 5 'Produto e subramo 'asouza - 08/03/2006
            sSql = sSql & "AND subramo_tb.subramo_id = " & sConteudo & vbNewLine
            sSql = sSql & "AND produto_tb.produto_id = " & sConteudo & vbNewLine
            sSql = sSql & "ORDER BY parametro_inadimplencia_tb.ramo_id,parametro_inadimplencia_tb.subramo_id "
        
        Case 6 'Produto, subramo  e ramo' vapinto - 15/09/2008

            sSql = sSql & "AND parametro_inadimplencia_tb.produto_id = " & sConteudo & vbNewLine
            sSql = sSql & "AND parametro_inadimplencia_tb.ramo_id = " & sConteudo2 & vbNewLine
            sSql = sSql & "AND parametro_inadimplencia_tb.subramo_id = " & sConteudo3 & vbNewLine
            sSql = sSql & "ORDER BY parametro_inadimplencia_tb.ramo_id,parametro_inadimplencia_tb.subramo_id "
        
        Case Else
            sSql = sSql & "ORDER BY parametro_inadimplencia_tb.ramo_id,parametro_inadimplencia_tb.subramo_id "
      
    End Select

    Set Consultar = ExecutarSQL(sSiglaSistema, _
       iAmbienteId, _
       sSiglaRecurso, _
       sDescricaoRecurso, _
       sSql, _
       True)

    Exit Function
    
Trata_Erro:

    Call Err.Raise(Err.Number, , "SEGL0019.cls00108.Consultar - " & Err.Description)

End Function
Public Function Historico(ByVal sSiglaSistema As String, _
                          ByVal sSiglaRecurso As String, _
                          ByVal sDescricaoRecurso As String, _
                          ByVal iAmbienteId As Integer, _
                          ByVal ramo_id As Integer, _
                          ByVal subramo_id As String, _
                          ByVal produto_id As String) As Recordset

    '## Rotina de consulta em parametro_inadimplencia_tb

    Dim sSql As String

    On Error GoTo Trata_Erro

    sSql = " EXEC SEGS7045_SPS "
    sSql = sSql & "                   " & ramo_id & vbNewLine
    sSql = sSql & "                 , " & subramo_id & vbNewLine
    sSql = sSql & "                 , " & produto_id & vbNewLine

    Set Historico = ExecutarSQL(sSiglaSistema, _
       iAmbienteId, _
       sSiglaRecurso, _
       sDescricaoRecurso, _
       sSql, _
       True)

    Exit Function
    
Trata_Erro:

    Call Err.Raise(Err.Number, , "SEGL0019.cls00108.Historico - " & Err.Description)

End Function

Public Sub AssociarRegra(ByVal sUsuario As String, _
                         ByVal sSiglaSistema As String, _
                         ByVal sSiglaRecurso As String, _
                         ByVal sDescricaoRecurso As String, _
                         ByVal iAmbienteId As Integer, _
                         ByVal iSeqParametroInadimplencia As Integer, _
                         ByVal iRegraId As Integer, _
                         Optional ByVal iConexao As Integer = -1)

    '## Rotina de inclus�o em parametro_inadimplencia_regra_tb que representa a associa��o
    '## entre par�metros de inadimplencia e regras especiais

    Dim sSql As String

    On Error GoTo Trata_Erro
    
    sSql = ""
    sSql = sSql & "EXEC parametro_inadimplencia_regra_spi "
    sSql = sSql & iSeqParametroInadimplencia & ", "
    sSql = sSql & iRegraId & ", "
    sSql = sSql & "'" & sUsuario & "'"
    
    If iConexao = -1 Then
      
        Call ExecutarSQL(sSiglaSistema, _
           iAmbienteId, _
           sSiglaRecurso, _
           sDescricaoRecurso, _
           sSql, _
           False)
    
    Else
    
        Call Conexao_ExecutarSQL(sSiglaSistema, _
           iAmbienteId, _
           sSiglaRecurso, _
           sDescricaoRecurso, _
           sSql, _
           iConexao, _
           False)
    
    End If
    
    Exit Sub
    
Trata_Erro:

    Call Err.Raise(Err.Number, , "SEGL0019.cls00108.AssociarRegra - " & Err.Description)

End Sub

Public Sub FecharVigencia(ByVal sUsuario As String, _
                          ByVal sSiglaSistema As String, _
                          ByVal sSiglaRecurso As String, _
                          ByVal sDescricaoRecurso As String, _
                          ByVal iAmbienteId As Integer, _
                          ByVal iRamoId As Integer, _
                          ByVal lSubramoId As Long, _
                          ByVal lProdutoId As Long, _
                          ByVal sDataOperacional As String, _
                          Optional ByVal iConexao As Integer = -1)
    
    '## Rotina que fecha a vig�ncia alterando a data de fim de vig�ncia da tabela
    '## parametro_inadimplencia_tb

    Dim sSql As String

    On Error GoTo Trata_Erro
    
    sSql = ""
    sSql = sSql & "EXEC fechar_vigencia_parametro_inadimplencia_spu "
    sSql = sSql & iRamoId
    sSql = sSql & "," & lSubramoId
    sSql = sSql & "," & lProdutoId
    sSql = sSql & ", '" & sUsuario & "'"
    
    If iConexao = -1 Then
    
        Call ExecutarSQL(sSiglaSistema, _
           iAmbienteId, _
           sSiglaRecurso, _
           sDescricaoRecurso, _
           sSql, _
           False)
    
    Else
    
        Call Conexao_ExecutarSQL(sSiglaSistema, _
           iAmbienteId, _
           sSiglaRecurso, _
           sDescricaoRecurso, _
           sSql, _
           iConexao, _
           False)
        
    End If
    
    Exit Sub
    
Trata_Erro:

    Call Err.Raise(Err.Number, , "SEGL0019.cls00108.FecharVigencia - " & Err.Description)

End Sub
Public Sub ExcluirParametro(ByVal sUsuario As String, _
                            ByVal sSiglaSistema As String, _
                            ByVal sSiglaRecurso As String, _
                            ByVal sDescricaoRecurso As String, _
                            ByVal iAmbienteId As Integer, _
                            ByVal iRamoId As Integer, _
                            ByVal lSubramoId As Long, _
                            ByVal lProdutoId As Long, _
                            ByVal sDataOperacional As String, _
                            Optional ByVal iConexao As Integer = -1, _
                            Optional ByVal sMotivoAlteracao As String = "")
    
    '## Rotina que fecha a vig�ncia alterando a data de fim de vig�ncia da tabela
    '## parametro_inadimplencia_tb

    Dim sSql As String

    On Error GoTo Trata_Erro
    
    sSql = ""
    sSql = sSql & "EXEC SEGS7120_SPU "
    sSql = sSql & "   " & iRamoId
    sSql = sSql & ",  " & lSubramoId
    sSql = sSql & ",  " & lProdutoId
    sSql = sSql & ", '" & sUsuario & "'"
    sSql = sSql & ", '" & sMotivoAlteracao & "'"
    
    If iConexao = -1 Then
    
        Call ExecutarSQL(sSiglaSistema, _
           iAmbienteId, _
           sSiglaRecurso, _
           sDescricaoRecurso, _
           sSql, _
           False)
    
    Else
    
        Call Conexao_ExecutarSQL(sSiglaSistema, _
           iAmbienteId, _
           sSiglaRecurso, _
           sDescricaoRecurso, _
           sSql, _
           iConexao, _
           False)
        
    End If
    
    Exit Sub
    
Trata_Erro:

    Call Err.Raise(Err.Number, , "SEGL0019.cls00108.ExcluirParametro - " & Err.Description)

End Sub


Public Function ConsultarDiasFormaPgto(ByVal sSiglaSistema As String, _
                          ByVal sSiglaRecurso As String, _
                          ByVal sDescricaoRecurso As String, _
                          ByVal iAmbienteId As Integer, _
                          ByVal lProdutoId As Long) As Recordset

  
    Dim sSql As String

    On Error GoTo Trata_Erro
  
    sSql = ""
    sSql = "      SELECT " & vbNewLine
    sSql = sSql & "      p.produto_id, p.nome, " & vbNewLine
    sSql = sSql & "      p3.nome As [Forma de pagamento], " & vbNewLine
    sSql = sSql & "      isnull(p2.dias_para_emissao_aviso, 0) As [Dias para emissao de aviso], " & vbNewLine
    sSql = sSql & "      CASE isnull(p2.ativo, 's') WHEN 'S' THEN 'Sim' else 'N�o' END As [Ativo], " & vbNewLine
    sSql = sSql & "      p2.forma_pgto_id " & vbNewLine
    sSql = sSql & "      FROM produto_tb p " & vbNewLine
    sSql = sSql & "      JOIN produto_forma_pgto_tb p2" & vbNewLine
    sSql = sSql & "      ON  p.produto_id = p2.produto_id " & vbNewLine
    sSql = sSql & "      AND p2.dt_fim_vigencia IS NULL " & vbNewLine
    sSql = sSql & "      JOIN forma_pgto_tb p3 " & vbNewLine
    sSql = sSql & "      ON p2.forma_pgto_id = p3.forma_pgto_id " & vbNewLine
    sSql = sSql & "      AND p.produto_id =  " & lProdutoId & vbNewLine
  
        

    Set ConsultarDiasFormaPgto = ExecutarSQL(sSiglaSistema, _
       iAmbienteId, _
       sSiglaRecurso, _
       sDescricaoRecurso, _
       sSql, _
       True)

    Exit Function
    
Trata_Erro:

    Call Err.Raise(Err.Number, , "SEGL0019.cls00108.ConsultarDiasFormaPgto - " & Err.Description)

End Function

Public Function AlterarDiasFormaPgto(ByVal sSiglaSistema As String, _
                          ByVal sSiglaRecurso As String, _
                          ByVal sDescricaoRecurso As String, _
                          ByVal iAmbienteId As Integer, _
                          ByVal lProdutoId As Long, _
                          ByVal iFormaPgto_id As Integer, _
                          ByVal iDias As Integer, _
                          ByVal sAtivo As String) As Recordset

  
    Dim sSql As String

    On Error GoTo Trata_Erro
    
    sSql = ""
    sSql = " EXEC seguros_db.dbo.SEGS12848_SPU " & iDias & ", " & sAtivo & ", " & lProdutoId & ", " & iFormaPgto_id

    Call ExecutarSQL(sSiglaSistema, _
       iAmbienteId, _
       sSiglaRecurso, _
       sDescricaoRecurso, _
       sSql, _
       False)

    Exit Function
    
Trata_Erro:

    Call Err.Raise(Err.Number, , "SEGL0019.cls00108.AlterarDiasFormaPgto - " & Err.Description)

End Function


Public Function ConsultarTpRamo(ByVal sSiglaSistema As String, _
                          ByVal sSiglaRecurso As String, _
                          ByVal sDescricaoRecurso As String, _
                          ByVal iAmbienteId As Integer, _
                          ByVal lTpRamoId As Long) As Recordset

  
    Dim sSql As String

    On Error GoTo Trata_Erro
  

    sSql = ""
    sSql = "      SELECT " & vbNewLine
    sSql = sSql & "      ramo_tb.tp_ramo_id " & vbNewLine
    sSql = sSql & "      FROM ramo_tb  " & vbNewLine
    sSql = sSql & "     WHERE ramo_tb.ramo_id =  " & lTpRamoId & vbNewLine
        

    Set ConsultarTpRamo = ExecutarSQL(sSiglaSistema, _
       iAmbienteId, _
       sSiglaRecurso, _
       sDescricaoRecurso, _
       sSql, _
       True)

    Exit Function
    
Trata_Erro:

    Call Err.Raise(Err.Number, , "SEGL0019.cls00108.ConsultarTpRamo - " & Err.Description)

End Function

Public Function ConsultarProduto(ByVal sSiglaSistema As String, _
                          ByVal sSiglaRecurso As String, _
                          ByVal sDescricaoRecurso As String, _
                          ByVal iAmbienteId As Integer, _
                          ByVal lTpRamoId As Long) As Recordset

  
    Dim sSql As String

    On Error GoTo Trata_Erro
  
  
  
    sSql = ""
    sSql = "      SELECT " & vbNewLine
    sSql = sSql & "      produto.produto_id, produto.nome " & vbNewLine
    sSql = sSql & "      from produto_tb produto  " & vbNewLine
    sSql = sSql & "      inner join item_produto_tb item_produto " & vbNewLine
    sSql = sSql & "      on produto.produto_id = item_produto.produto_id " & vbNewLine
    sSql = sSql & "      and item_produto.ramo_id =" & lTpRamoId & vbNewLine

    Set ConsultarProduto = ExecutarSQL(sSiglaSistema, _
       iAmbienteId, _
       sSiglaRecurso, _
       sDescricaoRecurso, _
       sSql, _
       True)

    Exit Function
    
Trata_Erro:

    Call Err.Raise(Err.Number, , "SEGL0019.cls00108.ConsultarTpRamo - " & Err.Description)

End Function

