VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cls00109"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit

Public Sub Excluir(ByVal sSiglaSistema As String, _
                   ByVal sSiglaRecurso As String, _
                   ByVal sDescricaoRecurso As String, _
                   ByVal iAmbienteId As Integer, _
                   ByVal iRegraId As Integer)

    '## Rotina de exclus�o em regra_especial_tb

    Dim sSql As String

    On Error GoTo Trata_Erro
    
    sSql = ""
    sSql = sSql & "EXEC regra_especial_spd "
    sSql = sSql & iRegraId

    Call ExecutarSQL(sSiglaSistema, _
       iAmbienteId, _
       sSiglaRecurso, _
       sDescricaoRecurso, _
       sSql, _
       False)

    Exit Sub
    
Trata_Erro:

    Call Err.Raise(Err.Number, , "SEGL0019.cls00109.Excluir - " & Err.Description)

End Sub

Public Function Consultar(ByVal sSiglaSistema As String, _
                          ByVal sSiglaRecurso As String, _
                          ByVal sDescricaoRecurso As String, _
                          ByVal iAmbienteId As Integer, _
                          Optional ByVal iTipoPesquisa As Integer, _
                          Optional ByVal sConteudo As String) As Recordset

    '## Rotina de consulta em regra_especial_tb

    Dim sSql As String

    On Error GoTo Trata_Erro

    sSql = ""
    sSql = "SELECT regra_id, " & vbNewLine
    sSql = sSql & "nome, " & vbNewLine
    sSql = sSql & "objetivo, " & vbNewLine
    sSql = sSql & "dt_solicitacao, " & vbNewLine
    sSql = sSql & "situacao, " & vbNewLine
    sSql = sSql & "dt_criacao_sp, " & vbNewLine
    sSql = sSql & "sp " & vbNewLine
    sSql = sSql & "FROM regra_especial_tb " & vbNewLine

    Select Case iTipoPesquisa
  
        Case 1 'C�digo
            sSql = sSql & "WHERE " & vbNewLine
            sSql = sSql & "regra_id = " & sConteudo & vbNewLine
      
        Case 2 'Nome - para pesquisa
            sSql = sSql & "WHERE " & vbNewLine
            sSql = sSql & "nome Like '%" & sConteudo & "%'" & vbNewLine
        
        Case 3 'Situa��o
            sSql = sSql & "WHERE " & vbNewLine
            sSql = sSql & "situacao = '" & sConteudo & "'" & vbNewLine
        
        Case 99 'Nome - para Inclus�o
            sSql = sSql & "WHERE " & vbNewLine
            sSql = sSql & "nome = '" & sConteudo & "'" & vbNewLine
             
    End Select

    sSql = sSql & "ORDER BY regra_id"

    Set Consultar = ExecutarSQL(sSiglaSistema, _
       iAmbienteId, _
       sSiglaRecurso, _
       sDescricaoRecurso, _
       sSql, _
       True)

    Exit Function
    
Trata_Erro:

    Call Err.Raise(Err.Number, , "SEGL0019.cls00109.Consultar - " & Err.Description)

End Function

Public Sub Alterar(ByVal sUsuario As String, _
                   ByVal sSiglaSistema As String, _
                   ByVal sSiglaRecurso As String, _
                   ByVal sDescricaoRecurso As String, _
                   ByVal iAmbienteId As Integer, _
                   ByVal iRegraId As Integer, _
                   ByVal sDtSistema As String, _
                   Optional ByVal sSituacao As String, _
                   Optional ByVal sDtCriacaoSp As String, _
                   Optional ByVal sSp As String, _
                   Optional ByVal sNome As String, _
                   Optional ByVal sObjetivo As String, _
                   Optional ByVal sDtSolicitacao As String, _
                   Optional ByVal sUsuarioSolicitante As String)

    '## Rotina de altera��o em regra_especial_tb

    Dim sSql As String

    On Error GoTo Trata_Erro
    
    sSql = ""
    sSql = sSql & "EXEC regra_especial_spu "
    sSql = sSql & iRegraId
    sSql = sSql & ",'" & Format(sDtSistema, "yyyymmdd") & "'"
    
    If Trim(sNome) = "" Then
        sSql = sSql & ",NULL "
    Else
        sSql = sSql & ",'" & sNome & "'"
    End If
    
    If Trim(sObjetivo) = "" Then
        sSql = sSql & ",NULL "
    Else
        sSql = sSql & ",'" & sObjetivo & "'"
    End If
    
    If Trim(sUsuarioSolicitante) = "" Then
        sSql = sSql & ",NULL "
    Else
        sSql = sSql & ",'" & sUsuarioSolicitante & "'"
    End If
    
    If Trim(sDtSolicitacao) = "" Then
        sSql = sSql & ",NULL "
    Else
        sSql = sSql & ",'" & Format(sDtSolicitacao, "yyyymmdd") & "'"
    End If
    
    If Trim(sSituacao) = "" Then
        sSql = sSql & ",NULL "
    Else
        sSql = sSql & ",'" & sSituacao & "'"
    End If
    
    If Trim(sDtCriacaoSp) = "" Then
        sSql = sSql & ",NULL "
    Else
        sSql = sSql & ",'" & Format(sDtCriacaoSp, "yyyymmdd") & "'"
    End If
    
    If Trim(sSp) = "" Then
        sSql = sSql & ",NULL "
    Else
        sSql = sSql & ",'" & sSp & "'"
    End If
    
    sSql = sSql & ",'" & sUsuario & "'"
    
    Call ExecutarSQL(sSiglaSistema, _
       iAmbienteId, _
       sSiglaRecurso, _
       sDescricaoRecurso, _
       sSql, _
       False)

    Exit Sub
    
Trata_Erro:

    Call Err.Raise(Err.Number, , "SEGL0019.cls00109.Alterar - " & Err.Description)

End Sub

Public Sub Incluir(ByVal sUsuario As String, _
                   ByVal sSiglaSistema As String, _
                   ByVal sSiglaRecurso As String, _
                   ByVal sDescricaoRecurso As String, _
                   ByVal iAmbienteId As Integer, _
                   ByVal sNome As String, _
                   ByVal sObjetivo As String, _
                   ByVal sDtSolicitacao As String, _
                   ByVal sSituacao As String, _
                   Optional ByVal sDtCriacaoSp As String, _
                   Optional ByVal sSp As String)

    Dim sSql As String

    '## Rotina de inclus�o em regra_especial_tb

    On Error GoTo Trata_Erro
    
    sSql = ""
    sSql = sSql & "EXEC regra_especial_spi "
    sSql = sSql & "'" & sNome & "'"
    sSql = sSql & ",'" & sObjetivo & "'"
    sSql = sSql & ",'" & sUsuario & "'"
    sSql = sSql & ",'" & Format(sDtSolicitacao, "yyyymmdd") & "'"
    sSql = sSql & ",'" & sSituacao & "'"
    
    If sDtCriacaoSp <> "" Then
        sSql = sSql & ",'" & Format(sDtCriacaoSp, "yyyymmdd") & "'"
    Else
        sSql = sSql & ",Null"
    End If
    
    If sSp <> "" Then
        sSql = sSql & ",'" & sSp & "'"
    Else
        sSql = sSql & ",Null"
    End If
    
    sSql = sSql & ",'" & sUsuario & "'"
    
    Call ExecutarSQL(sSiglaSistema, _
       iAmbienteId, _
       sSiglaRecurso, _
       sDescricaoRecurso, _
       sSql, _
       False)
    
    Exit Sub
    
Trata_Erro:

    Call Err.Raise(Err.Number, , "SEGL0019.cls00109.Incluir - " & Err.Description)

End Sub

Public Function ConsultarAplicacoes(ByVal sSiglaSistema As String, _
                                    ByVal sSiglaRecurso As String, _
                                    ByVal sDescricaoRecurso As String, _
                                    ByVal iAmbienteId As Integer, _
                                    Optional ByVal iTipoPesquisa As Integer, _
                                    Optional ByVal sConteudo As String) As Recordset

    '## Rotina de consulta as regras especiais j� associadas a um parametro de inadimplencia

    Dim sSql As String

    On Error GoTo Trata_Erro

    sSql = ""
    sSql = "SELECT regra_especial_tb.regra_id, " & vbNewLine
    sSql = sSql & "parametro_inadimplencia_tb.ramo_id, " & vbNewLine
    sSql = sSql & "parametro_inadimplencia_tb.subramo_id, " & vbNewLine
    sSql = sSql & "regra_especial_tb.nome, " & vbNewLine
    sSql = sSql & "regra_especial_tb.objetivo, " & vbNewLine
    sSql = sSql & "regra_especial_tb.dt_solicitacao, " & vbNewLine
    sSql = sSql & "regra_especial_tb.situacao, " & vbNewLine
    sSql = sSql & "regra_especial_tb.dt_criacao_sp, " & vbNewLine
    sSql = sSql & "regra_especial_tb.sp " & vbNewLine
    sSql = sSql & "FROM parametro_inadimplencia_regra_tb " & vbNewLine
    sSql = sSql & "JOIN parametro_inadimplencia_tb " & vbNewLine
    sSql = sSql & "  ON parametro_inadimplencia_tb.seq_parametro_inadimplencia = parametro_inadimplencia_regra_tb.seq_parametro_inadimplencia" & vbNewLine
    sSql = sSql & "JOIN regra_especial_tb " & vbNewLine
    sSql = sSql & "  ON regra_especial_tb.regra_id = parametro_inadimplencia_regra_tb.regra_id " & vbNewLine
    
    Select Case iTipoPesquisa
  
        Case 1 'Regra
            sSql = sSql & "WHERE " & vbNewLine
            sSql = sSql & "parametro_inadimplencia_regra_tb.regra_id = " & sConteudo & vbNewLine
    
        Case 2 'Sequencial
            sSql = sSql & "WHERE " & vbNewLine
            sSql = sSql & "parametro_inadimplencia_regra_tb.seq_parametro_inadimplencia = " & sConteudo & vbNewLine
    
    End Select
    
    sSql = sSql & "ORDER BY regra_especial_tb.regra_id,parametro_inadimplencia_tb.ramo_id,parametro_inadimplencia_tb.subramo_id"
    
    Set ConsultarAplicacoes = ExecutarSQL(sSiglaSistema, _
       iAmbienteId, _
       sSiglaRecurso, _
       sDescricaoRecurso, _
       sSql)

    Exit Function
    
Trata_Erro:

    Call Err.Raise(Err.Number, , "SEGL0019.cls00109.ConsultarAplicacoes - " & Err.Description)

End Function
