VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cls00421"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Public Function Consultar(ByVal sSiglaSistema As String, _
                          ByVal sSiglaRecurso As String, _
                          ByVal sDescricaoRecurso As String, _
                          ByVal lAmbienteID As Long, _
                          ByVal iTpPesquisa As Integer, _
                          ByVal sTexto As String) As Object
    Dim sSQL As String

    'itpPesquisa
    '0 - Todos
    '1 - C�digo
    '2 - Nome
    On Error GoTo Trata_Erro
    sSQL = ""
    adSQL sSQL, "SELECT *"
    adSQL sSQL, "FROM TABUA_CALCULO_tb "

    Select Case iTpPesquisa
        Case 1
            adSQL sSQL, "WHERE tabua_calculo_id = " & Val(sTexto)
        Case 2
            adSQL sSQL, "WHERE nome like '" & MudaAspaSimples(sTexto) & "%'"
    End Select

    adSQL sSQL, "ORDER BY Nome"
    Set Consultar = ExecutarSQL(sSiglaSistema, lAmbienteID, sSiglaRecurso, sDescricaoRecurso, sSQL, True)
    Exit Function
Trata_Erro:
    Call Err.Raise(Err.Number, , "RSGL0002.cls00421.Consultar - " & Err.Description)
End Function

Public Function Excluir(ByVal sSiglaSistema As String, _
                        ByVal sSiglaRecurso As String, _
                        ByVal sDescricaoRecurso As String, _
                        ByVal lAmbienteID As Long, _
                        ByVal lTabua_calculo_id As Long) As Boolean
    'Excluir dados da tabela
    Dim sSQL As String

    sSQL = "EXEC tabua_calculo_spd " & lTabua_calculo_id
    Call ExecutarSQL(sSiglaSistema, lAmbienteID, sSiglaRecurso, sDescricaoRecurso, sSQL, False)
    Excluir = True
    Exit Function
Trata_Erro:
    Call Err.Raise(Err.Number, , "RSGL0002.cls00421.Excluir - " & Err.Description)
End Function

Public Function Incluir(ByVal sSiglaSistema As String, _
                        ByVal sSiglaRecurso As String, _
                        ByVal sDescricaoRecurso As String, _
                        ByVal lAmbienteID As Long, _
                        ByVal sNome As String, _
                        ByVal sCaracteristica As String, _
                        ByVal sAdequacao As String, _
                        ByVal sUsuario As String) As Long

    ' Inclus�o de dados da tabua
    On Error GoTo Trata_Erro
    Dim sSQL As String

    Dim rs   As Recordset

    sSQL = "EXEC Tabua_calculo_spi "
    adSQL sSQL, "'" & sNome & "', "
    adSQL sSQL, "'" & sCaracteristica & "', "
    adSQL sSQL, "'" & sAdequacao & "', "
    adSQL sSQL, "'" & sUsuario & "' "
    Set rs = ExecutarSQL(sSiglaSistema, lAmbienteID, sSiglaRecurso, sDescricaoRecurso, sSQL, True)
    Incluir = rs(0)
    Exit Function
Trata_Erro:
    Call Err.Raise(Err.Number, , "RSGL0002.cls00421.Incluir - " & Err.Description)
End Function

Public Function Alterar(ByVal sSiglaSistema As String, _
                        ByVal sSiglaRecurso As String, _
                        ByVal sDescricaoRecurso As String, _
                        ByVal lAmbienteID As Long, _
                        ByVal iTabua_calculo_id As Integer, _
                        ByVal sNome As String, _
                        ByVal sCaracteristica As String, _
                        ByVal sAdequacao As String, _
                        ByVal sUsuario As String) As Boolean

    ' Inclus�o de dados do ressegurador
    On Error GoTo Trata_Erro
    Dim sSQL   As String

    Dim oTabua As Object

    sSQL = "EXEC Tabua_calculo_spu "
    adSQL sSQL, "'" & iTabua_calculo_id & "', "
    adSQL sSQL, "'" & sNome & "', "
    adSQL sSQL, "'" & sCaracteristica & "', "
    adSQL sSQL, "'" & sAdequacao & "', "
    adSQL sSQL, "'" & sUsuario & "' "
    Call ExecutarSQL(sSiglaSistema, lAmbienteID, sSiglaRecurso, sDescricaoRecurso, sSQL, False)
    Alterar = True
    Exit Function
Trata_Erro:
    Call Err.Raise(Err.Number, , "RSGL0002.cls00421.Alterar - " & Err.Description)
End Function

Public Function IncluirMortalidade(ByVal sSiglaSistema As String, _
                                   ByVal sSiglaRecurso As String, _
                                   ByVal sDescricaoRecurso As String, _
                                   ByVal lAmbienteID As Long, _
                                   ByVal iTabua_calculo_id As Integer, _
                                   ByVal iIdade As Integer, _
                                   ByVal dFator_qx As Double, _
                                   ByVal sUsuario As String) As Boolean

    ' Inclus�o de dados de mortalidade
    On Error GoTo Trata_Erro
    Dim sSQL   As String

    Dim oTabua As Object

    sSQL = "EXEC Tabua_Mortalidade_spi "
    adSQL sSQL, iTabua_calculo_id & ", "
    adSQL sSQL, iIdade & ", "
    adSQL sSQL, TrocaVirgulaPorPonto(dFator_qx) & ", "
    adSQL sSQL, "'" & sUsuario & "' "
    Call ExecutarSQL(sSiglaSistema, lAmbienteID, sSiglaRecurso, sDescricaoRecurso, sSQL, False)
    IncluirMortalidade = True
    Exit Function
Trata_Erro:
    Call Err.Raise(Err.Number, , "RSGL0002.cls00421.IncluirMortalidade - " & Err.Description)
End Function

Public Function AlterarMortalidade(ByVal sSiglaSistema As String, _
                                   ByVal sSiglaRecurso As String, _
                                   ByVal sDescricaoRecurso As String, _
                                   ByVal lAmbienteID As Long, _
                                   ByVal iTabua_Mortalidade_id As Integer, _
                                   ByVal iTabua_calculo_id As Integer, _
                                   ByVal iIdade As Integer, _
                                   ByVal dFator_qx As Double, _
                                   ByVal sUsuario As String) As Boolean

    ' Inclus�o de dados de mortalidade
    On Error GoTo Trata_Erro
    Dim sSQL   As String

    Dim oTabua As Object

    sSQL = "EXEC Tabua_Mortalidade_spu "
    adSQL sSQL, "'" & iTabua_Mortalidade_id & "', "
    adSQL sSQL, "'" & iTabua_calculo_id & "', "
    adSQL sSQL, "'" & iIdade & "', "
    adSQL sSQL, "'" & dFator_qx & "', "
    adSQL sSQL, "'" & sUsuario & "' "
    Call ExecutarSQL(sSiglaSistema, lAmbienteID, sSiglaRecurso, sDescricaoRecurso, sSQL, False)
    AlterarMortalidade = True
    Exit Function
Trata_Erro:
    Call Err.Raise(Err.Number, , "RSGL0002.cls00421.AlterarMortalidade - " & Err.Description)
End Function

Public Function ExcluirMortalidade(ByVal sSiglaSistema As String, _
                                   ByVal sSiglaRecurso As String, _
                                   ByVal sDescricaoRecurso As String, _
                                   ByVal lAmbienteID As Long, _
                                   ByVal lTabua_calculo_id As Long) As Boolean
    'Excluir dados da tabela
    Dim sSQL As String

    sSQL = "EXEC tabua_mortalidade_spd " & lTabua_calculo_id
    Call ExecutarSQL(sSiglaSistema, lAmbienteID, sSiglaRecurso, sDescricaoRecurso, sSQL, False)
    ExcluirMortalidade = True
    Exit Function
Trata_Erro:
    Call Err.Raise(Err.Number, , "RSGL0002.cls00421.ExcluirMortalidade - " & Err.Description)
End Function

Public Function ConsultarMortalidade(ByVal sSiglaSistema As String, _
                                     ByVal sSiglaRecurso As String, _
                                     ByVal sDescricaoRecurso As String, _
                                     ByVal lAmbienteID As Long, _
                                     ByVal ltabuaCalculoID As Long) As Object
    Dim sSQL As String

    On Error GoTo Trata_Erro
    sSQL = ""
    adSQL sSQL, "SELECT *"
    adSQL sSQL, "FROM TABUA_MORTALIDADE_tb "
    adSQL sSQL, "WHERE tabua_calculo_id = " & Val(ltabuaCalculoID)
    adSQL sSQL, "ORDER BY idade"
    Set ConsultarMortalidade = ExecutarSQL(sSiglaSistema, lAmbienteID, sSiglaRecurso, sDescricaoRecurso, sSQL, True)
    Exit Function
Trata_Erro:
    Call Err.Raise(Err.Number, , "RSGL0002.cls00421.ConsultarMortalidade - " & Err.Description)
End Function
