Attribute VB_Name = "Funcoes"
Option Explicit
'Fun��es para registrar Fonts - Por Daniel Romualdo(16/04/2003)
Public Const WM_FONTCHANGE = &H1D

Public Const HWND_BROADCAST = &HFFFF&

Declare Function AddFontResource& _
        Lib "gdi32" _
        Alias "AddFontResourceA" (ByVal lpFileName As String)
Declare Function SendMessageBynum& _
        Lib "user32" _
        Alias "SendMessageA" (ByVal hwnd As Long, _
                              ByVal wMsg As Long, _
                              ByVal wParam As Integer, _
                              ByVal lparam As Long)
Declare Function ShellExecute _
        Lib "shell32.dll" _
        Alias "ShellExecuteA" (ByVal hwnd As Long, _
                               ByVal lpOperation As String, _
                               ByVal lpFile As String, _
                               ByVal lpParameters As String, _
                               ByVal lpDirectory As String, _
                               ByVal nShowCmd As Long) As Long
'Vari�veis
Public gsPastaLocalSegbr      As String

Public gsPastaServidorSegbr   As String

Public gsHost                 As String

Public gsMac                  As String

Public gsIp                   As String

Public gsDHEntrada            As String

Public gsCodAplicacao         As String

Public gsParamAplicacao       As String

Public gsMensagem             As String

Public glTempoDeEsperaSEGBR   As Long

Public glTempoDeEsperaUsuario As Long

Public gsCPF                  As String

Public glAmbiente_id          As Long

Public glEmpresa_id           As Long

Public traySegbr              As NOTIFYICONDATA

Public sDecimal               As String

Public SQS                    As String

Public lpUserName             As String

Public cUserName              As String

Public nameu                  As String

Public cpermissao             As String

Public arquivoArquivo         As String

Public InicioSenha            As Integer

Public FimSenha               As Integer

Public InicioUsuario          As Integer

Public FimUsuario             As Integer

Public Usuario                As String

Public UsuarioWeb             As String

Public SenhaWeb               As String

Public Senha                  As String

Public Status_Sistema         As String

Public Data_Sistema           As String

Public Data_Contabilizacao    As Date

Public Ambiente               As String

Public SIS_server             As String

Public SIS_banco              As String

Public SIS_usuario            As String

Public SIS_senha              As String

Public INT_server             As String

Public INT_banco              As String

Public INT_usuario            As String

Public INT_senha              As String

Public BKP_usuario            As String

Public BKP_senha              As String

Public SP_usuario             As String

Public IndentFlag             As Boolean

Public ftpServidorSISE        As String

Public ftpUsuarioServidorSISE As String

Public ftpSenhaServidorSISE   As String

Public goProducao             As Object

' constantes
Public Const gsSIGLAEMPRESA = "CONFITEC"

Public Const gsSIGLASISTEMA = "RESSEG"

Public Const gsCODPRINCIPAL = "RSGP0001"

Public Const glINTERVALOATUALIZACAO = 60000 ' milissegundos

' constantes de erro
Public Const glERRO_VALIDA_PARAMETRO = 1

Public Const gsERRO_VALIDA_PARAMETRO = "S� � poss�vel abrir o projeto ?? pelo " & gsSIGLASISTEMA & "."

Public Const glERRO_MONTA_PARAMETRO = 2

Public Const gsERRO_MONTA_PARAMETRO = "N�o foi poss�vel autenticar o programa com o c�digo ??."

Public Const glERRO_ATUALIZA_ARQUIVO = 3

Public Const gsERRO_ATUALIZA_ARQUIVO = "Erro na tentativa de atualizar o arquivo ??."

Public Const glERRO_TELA_USUARIO_EM_SESSAO = 4

Public Const gsERRO_TELA_USUARIO_EM_SESSAO = "XXX"

Public Const glERRO_USUARIO_SEM_PERMISSAO = 5

Public Const gsERRO_USUARIO_SEM_PERMISSAO = "Usu�rio sem acesso � fun��o: ??."

Public Const glERRO_FUNCAO_INDISPONIVEL = 6

Public Const gsERRO_FUNCAO_INDISPONIVEL = "Fun��o n�o dispon�vel: ??."

Public Const glERRO_FECHA_APLICATIVO = 7

Public Const gsERRO_FECHA_APLICATIVO = "Aplicativo ?? n�o p�de ser fechado."

Public Const glERRO_TIPO_NAO_CORRETO = 8

Public Const gsERRO_TIPO_NAO_CORRETO = "Tipo n�o esperado ??."

Public Const glERRO_GERENCIADOR_MENSAGEM = 9

Public Const gsERRO_GERENCIADOR_MENSAGEM = "XXX"

Public Const glERRO_ARQUIVO_NAO_ENCONTRADO = 10

Public Const gsERRO_ARQUIVO_NAO_ENCONTRADO = "Arquivo n�o encontrado ??."

Public Const glERRO_USUARIO_COM_VARIAS_CONEXOES = 11

Public Const gsERRO_USUARIO_COM_VARIAS_CONEXOES = "?? j� est� usando o " & gsSIGLASISTEMA & " na m�quina ??."

' constantes de API
Public Const CB_ERR = -1

Public Const CB_FINDSTRING = &H14C

Public Const dwWindows95 = &H1&

Public Const dwWindowsNT = &H2&

Public Const SYNCHRONIZE = &H100000

Public Const PROCESS_QUERY_INFORMATION = &H400

Public Const PROCESS_TERMINATE = &H1

Public Const STILL_ACTIVE = &H103

Public Const NORMAL_PRIORITY_CLASS = &H20&

Public Const INFINITE = -1&

Public Const SW_SHOWNORMAL = 1

Public Const SW_SHOWMINIMIZED = 2

Public Const SW_SHOWMAXIMIZED = 3

Public Const SW_SHOWNOACTIVATE = 4

Public Const SW_SHOW = 5

Public Const SW_SHOWMINNOACTIVE = 7

Public Const SW_SHOWNA = 8

Public Const SW_RESTORE = 9

Public Const SW_SHOWDEFAULT = 10

Public Const GW_HWNDPREV = 3

Public Const GW_HWNDNEXT = 2

Public Const GWW_HINSTANCE = (-6)

Public Const HKEY_CLASSES_ROOT = &H80000000

Public Const HKEY_CURRENT_USER = &H80000001

Public Const HKEY_LOCAL_MACHINE = &H80000002

Public Const HKEY_USERS = &H80000003

Public Const REG_SZ    As Long = 1

Public Const REG_DWORD As Long = 4

Public Const READ_CONTROL = &H20000

Public Const KEY_QUERY_VALUE = &H1

Public Const KEY_SET_VALUE = &H2

Public Const KEY_CREATE_SUB_KEY = &H4

Public Const KEY_ENUMERATE_SUB_KEYS = &H8

Public Const KEY_NOTIFY = &H10

Public Const KEY_CREATE_LINK = &H20

Public Const KEY_ALL_ACCESS = KEY_QUERY_VALUE + KEY_SET_VALUE + KEY_CREATE_SUB_KEY + KEY_ENUMERATE_SUB_KEYS + KEY_NOTIFY + KEY_CREATE_LINK + READ_CONTROL

Public Const KEY_LEITURA = KEY_QUERY_VALUE + KEY_ENUMERATE_SUB_KEYS

Public Const NIM_ADD = &H0

Public Const NIM_MODIFY = &H1

Public Const NIM_DELETE = &H2

Public Const NIF_MESSAGE = &H1

Public Const NIF_ICON = &H2

Public Const NIF_TIP = &H4

Public Const WM_MOUSEMOVE = &H200

Public Const MAX_LANA                 As Byte = 254  '  lana's in range 0 to MAX_LANA

Public Const NCBENUM                  As Long = &H37

Public Const NCBASTAT                 As Long = &H33

Public Const NCBNAMSZ                 As Long = 16

Public Const HEAP_ZERO_MEMORY         As Long = &H8

Public Const HEAP_GENERATE_EXCEPTIONS As Long = &H4

Public Const NCBRESET                 As Long = &H32

Public Const MAX_WSADescription = 256

Public Const MAX_WSASYSStatus = 128

Public Const ERROR_SUCCESS           As Long = 0

Public Const WS_VERSION_REQD         As Long = &H101

Public Const WS_VERSION_MAJOR        As Long = WS_VERSION_REQD \ &H100 And &HFF&

Public Const WS_VERSION_MINOR        As Long = WS_VERSION_REQD And &HFF&

Public Const MIN_SOCKETS_REQD        As Long = 1

Public Const SOCKET_ERROR            As Long = -1

Public Const MAX_COMPUTERNAME_LENGTH As Long = 15&

Public Const gREGKEYSYSINFOLOC = "SOFTWARE\Microsoft\Shared Tools Location"

Public Const gREGVALSYSINFOLOC = "MSINFO"

Public Const gREGKEYSYSINFO = "SOFTWARE\Microsoft\Shared Tools\MSINFO"

Public Const gREGVALSYSINFO = "PATH"

' tipos de API
Public Type OSVERSIONINFO 'for GetVersionEx API call
    dwOSVersionInfoSize As Long
    dwMajorVersion As Long
    dwMinorVersion As Long
    dwBuildNumber As Long
    dwPlatformId As Long
    szCSDVersion As String * 128
End Type

Public Type STARTUPINFO
    cb As Long
    lpReserved As String
    lpDesktop As String
    lpTitle As String
    dwX As Long
    dwY As Long
    dwXSize As Long
    dwYSize As Long
    dwXCountChars As Long
    dwYCountChars As Long
    dwFillAttribute As Long
    dwFlags As Long
    wShowWindow As Integer
    cbReserved2 As Integer
    lpReserved2 As Long
    hStdInput As Long
    hStdOutput As Long
    hStdError As Long
End Type

Public Type PROCESS_INFORMATION
    hProcess As Long
    hThread As Long
    dwProcessId As Long
    dwThreadID As Long
End Type

Public Type WNDCLASS
    style As Long
    lpfnwndproc As Long
    cbClsextra As Long
    cbWndExtra2 As Long
    hInstance As Long
    hIcon As Long
    hCursor As Long
    hbrBackground As Long
    lpszMenuName As String
    lpszClassName As String
End Type

Public Type NET_CONTROL_BLOCK  'NCB
    ncb_command    As Byte
    ncb_retcode    As Byte
    ncb_lsn        As Byte
    ncb_num        As Byte
    ncb_buffer     As Long
    ncb_length     As Integer
    ncb_callname   As String * NCBNAMSZ
    ncb_name       As String * NCBNAMSZ
    ncb_rto        As Byte
    ncb_sto        As Byte
    ncb_post       As Long
    ncb_lana_num   As Byte
    ncb_cmd_cplt   As Byte
    ncb_reserve(9) As Byte ' Reserved, must be 0
    ncb_event      As Long
End Type

Public Type ADAPTER_STATUS
    adapter_address(5) As Byte
    rev_major         As Byte
    reserved0         As Byte
    adapter_type      As Byte
    rev_minor         As Byte
    duration          As Integer
    frmr_recv         As Integer
    frmr_xmit         As Integer

    iframe_recv_err   As Integer
    xmit_aborts       As Integer
    xmit_success      As Long
    recv_success      As Long

    iframe_xmit_err   As Integer
    recv_buff_unavail As Integer
    t1_timeouts       As Integer
    ti_timeouts       As Integer
    Reserved1         As Long
    free_ncbs         As Integer
    max_cfg_ncbs      As Integer
    max_ncbs          As Integer
    xmit_buf_unavail  As Integer
    max_dgram_size    As Integer
    pending_sess      As Integer
    max_cfg_sess      As Integer
    max_sess          As Integer
    max_sess_pkt_size As Integer
    name_count        As Integer
End Type

Public Type NAME_BUFFER
    name        As String * NCBNAMSZ
    name_num    As Integer
    name_flags  As Integer
End Type

Public Type ASTAT
    adapt          As ADAPTER_STATUS
    NameBuff(30)   As NAME_BUFFER
End Type

Public Type LANA_ENUM
    Length As Integer
    lana(MAX_LANA) As Integer
End Type

Public Type NOTIFYICONDATA
    cbSize As Long
    hwnd As Long
    uID As Long
    uFlags As Long
    uCallbackMessage As Long
    hIcon As Long
    szTip As String * 64
End Type

Public Type HOSTENT
    hName      As Long
    hAliases   As Long
    hAddrType  As Integer
    hLen       As Integer
    hAddrList  As Long
End Type

Public Type WSADATA
    wVersion      As Integer
    wHighVersion  As Integer
    szDescription(0 To MAX_WSADescription)   As Byte
    szSystemStatus(0 To MAX_WSASYSStatus)    As Byte
    wMaxSockets   As Integer
    wMaxUDPDG     As Integer
    dwVendorInfo  As Long
End Type

Public Declare Function GetForegroundWindow Lib "user32" () As Long

' declara��es de API
Public Declare Function EnableWindow _
               Lib "user32" (ByVal hwnd As Long, _
                             ByVal benable As Boolean) As Long

Public Declare Function CreateEllipticRgn _
               Lib "gdi32" (ByVal X1 As Long, _
                            ByVal Y1 As Long, _
                            ByVal X2 As Long, _
                            ByVal Y2 As Long) As Long

Public Declare Function SetWindowRgn _
               Lib "user32" (ByVal hwnd As Long, _
                             ByVal hRgn As Long, _
                             ByVal bRedraw As Boolean) As Long

Public Declare Function GetActiveWindow Lib "user32" () As Long

Public Declare Function SetActiveWindow Lib "user32" (ByVal hwnd As Long) As Long

Public Declare Function SetFocus Lib "user32" (ByVal hwnd As Long) As Long

Public Declare Function IsWindowEnabled Lib "user32" (ByVal hwnd As Long) As Long

Public Declare Function IsWindowVisible Lib "user32" (ByVal hwnd As Long) As Long

Public Declare Function OpenIcon Lib "user32" (ByVal hwnd As Long) As Long

Public Declare Function SetForegroundWindow Lib "user32" (ByVal hwnd As Long) As Long

Public Declare Function GetParent Lib "user32" (ByVal hwnd As Long) As Long

Public Declare Function IsIconic Lib "user32" (ByVal hwnd As Long) As Long

Public Declare Function Shell_NotifyIcon _
               Lib "shell32.dll" _
               Alias "Shell_NotifyIconA" (ByVal dwMessage As Long, _
                                          lpData As NOTIFYICONDATA) As Long

Public Declare Function GetWindowThreadProcessId _
               Lib "user32" (ByVal hwnd As Long, _
                             lpdwProcessId As Long) As Long

Public Declare Function GetWindow _
               Lib "user32" (ByVal hwnd As Long, _
                             ByVal wCmd As Long) As Long

Public Declare Function GetWindowWord _
               Lib "user32" (ByVal hwnd As Long, _
                             ByVal nIndex As Long) As Integer

Public Declare Function GetWindowText _
               Lib "user32" _
               Alias "GetWindowTextA" (ByVal hwnd As Long, _
                                       ByVal lpString As String, _
                                       ByVal cch As Long) As Long

Public Declare Function GetClassInfo _
               Lib "user32" _
               Alias "GetClassInfoA" (ByVal hInstance As Long, _
                                      ByVal lpClassName As String, _
                                      lpWndClass As WNDCLASS) As Long

Public Declare Function ShowWindow _
               Lib "user32" (ByVal hwnd As Long, _
                             ByVal nCmdShow As Long) As Long

Public Declare Function FindWindow _
               Lib "user32" _
               Alias "FindWindowA" (ByVal lpClassName As String, _
                                    ByVal lpWindowName As String) As Long

Public Declare Function SendMessage _
               Lib "user32" _
               Alias "SendMessageA" (ByVal hwnd As Long, _
                                     ByVal wMsg As Long, _
                                     ByVal wParam As Long, _
                                     lparam As Any) As Long

Public Declare Function GetVersionEx _
               Lib "kernel32" _
               Alias "GetVersionExA" (lpVersionInformation As OSVERSIONINFO) As Long

Public Declare Function GetPrivateProfileString _
               Lib "kernel32" _
               Alias "GetPrivateProfileStringA" (ByVal lpApplicationName As String, _
                                                 ByVal lpKeyName As Any, _
                                                 ByVal lpDefault As String, _
                                                 ByVal lpReturnedString As String, _
                                                 ByVal nSize As Long, _
                                                 ByVal lpFileName As String) As Long

Public Declare Sub CopyMemory _
               Lib "kernel32" _
               Alias "RtlMoveMemory" (hpvDest As Any, _
                                      ByVal hpvSource As Long, _
                                      ByVal cbCopy As Long)

Public Declare Function GetProcessHeap Lib "kernel32" () As Long

Public Declare Function HeapAlloc _
               Lib "kernel32" (ByVal hHeap As Long, _
                               ByVal dwFlags As Long, _
                               ByVal dwBytes As Long) As Long

Public Declare Function HeapFree _
               Lib "kernel32" (ByVal hHeap As Long, _
                               ByVal dwFlags As Long, _
                               lpMem As Any) As Long

Public Declare Function GetComputerName _
               Lib "kernel32" _
               Alias "GetComputerNameA" (ByVal lpBuffer As String, _
                                         nSize As Long) As Long

Public Declare Sub Sleep Lib "kernel32" (ByVal dwMilliseconds As Long)

Public Declare Function CloseHandle Lib "kernel32" (ByVal hObject As Long) As Long

Public Declare Function OpenProcess _
               Lib "kernel32" (ByVal dwDesiredAccess As Long, _
                               ByVal bInheritHandle As Long, _
                               ByVal dwProcessId As Long) As Long

Public Declare Function GetExitCodeProcess _
               Lib "kernel32" (ByVal hProcess As Long, _
                               lpExitCode As Long) As Long

Public Declare Function TerminateProcess _
               Lib "kernel32" (ByVal hProcess As Long, _
                               ByVal uExitCode As Long) As Long

Public Declare Function WaitForSingleObject _
               Lib "kernel32" (ByVal hHandle As Long, _
                               ByVal dwMilliseconds As Long) As Long

Public Declare Function CreateProcessA _
               Lib "kernel32" (ByVal lpApplicationName As Long, _
                               ByVal lpCommandLine As String, _
                               ByVal lpProcessAttributes As Long, _
                               ByVal lpThreadAttributes As Long, _
                               ByVal bInheritHandles As Long, _
                               ByVal dwCreationFlags As Long, _
                               ByVal lpEnvironment As Long, _
                               ByVal lpCurrentDirectory As Long, _
                               lpStartupInfo As STARTUPINFO, _
                               lpProcessInformation As PROCESS_INFORMATION) As Long

Public Declare Function GetUserName _
               Lib "advapi32.dll" _
               Alias "GetUserNameA" (ByVal lpBuffer As String, _
                                     nSize As Long) As Long

Public Declare Function RegOpenKeyEx _
               Lib "advapi32.dll" _
               Alias "RegOpenKeyExA" (ByVal hKey As Long, _
                                      ByVal lpSubKey As String, _
                                      ByVal ulOptions As Long, _
                                      ByVal samDesired As Long, _
                                      phkResult As Long) As Long

Public Declare Function RegSetValueExString _
               Lib "advapi32.dll" _
               Alias "RegSetValueExA" (ByVal hKey As Long, _
                                       ByVal lpValueName As String, _
                                       ByVal Reserved As Long, _
                                       ByVal dwType As Long, _
                                       ByVal lpValue As String, _
                                       ByVal cbData As Long) As Long

Public Declare Function RegSetValueExLong _
               Lib "advapi32.dll" _
               Alias "RegSetValueExA" (ByVal hKey As Long, _
                                       ByVal lpValueName As String, _
                                       ByVal Reserved As Long, _
                                       ByVal dwType As Long, _
                                       lpValue As Long, _
                                       ByVal cbData As Long) As Long

Public Declare Function RegQueryValueEx _
               Lib "advapi32" _
               Alias "RegQueryValueExA" (ByVal hKey As Long, _
                                         ByVal lpValueName As String, _
                                         ByVal lpReserved As Long, _
                                         ByRef lpType As Long, _
                                         ByVal lpData As String, _
                                         ByRef lpcbData As Long) As Long

Public Declare Function RegCloseKey Lib "advapi32.dll" (ByVal hKey As Long) As Long

Public Declare Function WSAGetLastError Lib "WSOCK32.DLL" () As Long

Public Declare Function WSAStartup _
               Lib "WSOCK32.DLL" (ByVal wVersionRequired As Long, _
                                  lpWSADATA As WSADATA) As Long

Public Declare Function WSACleanup Lib "WSOCK32.DLL" () As Long

Public Declare Function gethostname _
               Lib "WSOCK32.DLL" (ByVal szHost As String, _
                                  ByVal dwHostLen As Long) As Long

Public Declare Function gethostbyname Lib "WSOCK32.DLL" (ByVal szHost As String) As Long

Public Declare Function WNetGetUser _
               Lib "mpr.dll" _
               Alias "WNetGetUserA" (ByVal lpName As String, _
                                     ByVal lpUserName As String, _
                                     lpnLength As Long) As Long

Public Declare Function Netbios Lib "netapi32.dll" (pncb As NET_CONTROL_BLOCK) As Byte

Function FormataValorParaPadraoBrasil(valor As String) As String
    Dim ConfDecimal As String

    valor = MudaVirgulaParaPonto(valor)
    ConfDecimal = LeArquivoIni("WIN.INI", "intl", "sDecimal")

    'Convertendo valor do d�bito para o padr�o brasileiro
    If ConfDecimal = "." Then
        valor = TrocaValorAmePorBras(Format(Val(valor), "###,###,##0.00"))
    Else
        valor = Format(Val(valor), "###,###,##0.00")
    End If

    FormataValorParaPadraoBrasil = valor
End Function

Function CompletarCampo(ByVal sValor As String, _
                        ByVal sTipo As String, _
                        ByVal iTamanho As Integer) As String

    'Preenche o caracter a (esquerda(default) ou direita) do valor
    Select Case sTipo
        Case "C"

            If Trim(sValor) = "" Then
                CompletarCampo = Space(iTamanho)
            Else
                CompletarCampo = Left(sValor & Space(iTamanho), iTamanho)
            End If

        Case "N"
            CompletarCampo = Format(sValor, String(iTamanho, "0"))
    End Select

End Function

Function TrocaVirgulaPorPonto(ByVal ValorAux As Variant) As String

    '
    ' Respons�vel : Rubens Lima
    ' Dt Desenv : 06/07/2001
    ' C�pia da Fun��o "TrocaPontoPorVirgula" de Carlos Rosa de 23/04/98
    ' Recebe um valor como par�metro
    ' Retorna :
    ' Valor com o ponto como separador de decimal
    '
    If InStr(ValorAux, ",") = 0 Then
        TrocaVirgulaPorPonto = ValorAux
    Else
        ValorAux = Mid(ValorAux, 1, InStr(ValorAux, ",") - 1) + "." + Mid(ValorAux, InStr(ValorAux, ",") + 1)
        TrocaVirgulaPorPonto = ValorAux
    End If

End Function

'------------------------------------------------------------------------------------------
'
' Respons�vel : Jo�o Mac-Cormick
' Dt Desenv : 03/06/2000
'
' fornece o sistema operacional ou testa qual � o sistema operacional
Public Function SistemaOperacional(Optional ByVal dwSO) As Variant
    Dim osvi As OSVERSIONINFO

    osvi.dwOSVersionInfoSize = Len(osvi)

    If GetVersionEx(osvi) = 0 Then
        Exit Function
    End If

    If IsMissing(dwSO) Then
        SistemaOperacional = osvi.dwPlatformId
    Else
        SistemaOperacional = osvi.dwPlatformId And dwSO
    End If

End Function

'------------------------------------------------------------------------------------------
'
' Respons�vel : Jo�o Mac-Cormick
' Dt Desenv : 03/06/2000
'
' fornece as informa��es do sistema
Public Sub PegaInfoSistema(Computador As String, _
                           PlacaRede As String, _
                           NumeroIP As String, _
                           Host As String, _
                           UsuarioLocal As String, _
                           UsuarioRede As String, _
                           Optional ByVal Servidor As String = "")
    PlacaRede = GetMACAddress
    UsuarioLocal = GetMachineUserName

    If Servidor = "" Then
        UsuarioRede = ""
    Else

        If SistemaOperacional(dwWindows95) Then
            UsuarioRede = GetMachineUserName(Servidor)
        ElseIf SistemaOperacional(dwWindowsNT) Then

            If UCase(Environ("USERDOMAIN")) = UCase(Servidor) Then
                UsuarioRede = Trim(LCase(Environ("USERNAME")))
            Else
                UsuarioRede = ""
            End If

        Else
            UsuarioRede = ""
        End If
    End If

    Computador = Trim(LCase(GetMachineName))
    NumeroIP = Trim(GetIPAddress)
    Host = Trim(LCase(GetIPHostName))
    PlacaRede = Trim(UCase(AjustaBrEntre(GetMACAddress, 0)))
End Sub

'------------------------------------------------------------------------------------------
'
' Respons�vel : Jo�o Mac-Cormick
' Dt Desenv : 03/06/2000
'
' rotina de informa��es da m�quina
Public Sub StartSysInfo()
    On Local Error GoTo SysInfoErr
    Dim rc          As Long

    Dim SysInfoPath As String

    ' Try To Get System Info Program Path\Name From Registry...
    If GetKeyValue(HKEY_LOCAL_MACHINE, gREGKEYSYSINFO, gREGVALSYSINFO, SysInfoPath) Then
        ' Try To Get System Info Program Path Only From Registry...
    ElseIf GetKeyValue(HKEY_LOCAL_MACHINE, gREGKEYSYSINFOLOC, gREGVALSYSINFOLOC, SysInfoPath) Then

        ' Validate Existance Of Known 32 Bit File Version
        If (Dir(SysInfoPath & "\MSINFO32.EXE") <> "") Then
            SysInfoPath = SysInfoPath & "\MSINFO32.EXE"
            ' Error - File Can Not Be Found...
        Else
            GoTo SysInfoErr
        End If

        ' Error - Registry Entry Can Not Be Found...
    Else
        GoTo SysInfoErr
    End If

    Call Shell(SysInfoPath, vbNormalFocus)
    Exit Sub
SysInfoErr:
    MsgBox "System Information Is Unavailable At This Time", vbOKOnly
End Sub

'------------------------------------------------------------------------------------------
'
' Respons�vel : Jo�o Mac-Cormick
' Dt Desenv : 03/06/2000
'
' retorna o valor de uma chave de registro
Public Function GetKeyValue(KeyRoot As Long, _
                            KeyName As String, _
                            SubKeyRef As String, _
                            ByRef KeyVal As String) As Boolean
    Dim i          As Long                                           ' Loop Counter

    Dim rc         As Long                                          ' Return Code

    Dim hKey       As Long                                        ' Handle To An Open Registry Key

    Dim hDepth     As Long                                      '

    Dim KeyValType As Long                                  ' Data Type Of A Registry Key

    Dim tmpVal     As String                                    ' Tempory Storage For A Registry Key Value

    Dim KeyValSize As Long                                  ' Size Of Registry Key Variable

    '------------------------------------------------------------
    ' Open RegKey Under KeyRoot {HKEY_LOCAL_MACHINE...}
    '------------------------------------------------------------
    rc = RegOpenKeyEx(KeyRoot, KeyName, 0, KEY_LEITURA, hKey) ' Open Registry Key

    If (rc <> ERROR_SUCCESS) Then GoTo GetKeyError          ' Handle Error...
    tmpVal = String$(1024, 0)                             ' Allocate Variable Space
    KeyValSize = 1024                                       ' Mark Variable Size
    '------------------------------------------------------------
    ' Retrieve Registry Key Value...
    '------------------------------------------------------------
    rc = RegQueryValueEx(hKey, SubKeyRef, 0, KeyValType, tmpVal, KeyValSize)    ' Get/Create Key Value

    If (rc <> ERROR_SUCCESS) Then GoTo GetKeyError          ' Handle Errors
    If (Asc(Mid(tmpVal, KeyValSize, 1)) = 0) Then           ' Win95 Adds NULL Terminated String...
        tmpVal = Left(tmpVal, KeyValSize - 1)               ' NULL Found, Extract From String
    Else                                                    ' WinNT Does NOT NULL Terminate String...
        tmpVal = Left(tmpVal, KeyValSize)                   ' NULL Not Found, Extract String Only
    End If

    '------------------------------------------------------------
    ' Determine Key Value Type For Conversion...
    '------------------------------------------------------------
    Select Case KeyValType                                  ' Search Data Types...
        Case REG_SZ                                             ' String Registry Key Data Type
            KeyVal = tmpVal                                     ' Copy String Value
        Case REG_DWORD                                          ' Double Word Registry Key Data Type

            For i = Len(tmpVal) To 1 Step -1                    ' Convert Each Bit
                KeyVal = KeyVal + Hex(Asc(Mid(tmpVal, i, 1)))   ' Build Value Char. By Char.
            Next

            KeyVal = Format$("&h" + KeyVal)                     ' Convert Double Word To String
    End Select

    GetKeyValue = True                                      ' Return Success
    rc = RegCloseKey(hKey)                                  ' Close Registry Key
    Exit Function                                           ' Exit
GetKeyError:      ' Cleanup After An Error Has Occured...
    KeyVal = ""                                             ' Set Return Val To Empty String
    GetKeyValue = False                                     ' Return Failure
    rc = RegCloseKey(hKey)                                  ' Close Registry Key
End Function

'------------------------------------------------------------------------------------------
'
' Respons�vel : Jo�o Mac-Cormick
' Dt Desenv : 03/06/2000
'
' grava um valor em uma chave de registro
Private Function SetValueEx(ByVal hKey As Long, _
                            sValueName As String, _
                            lType As Long, _
                            vValue As Variant) As Long
    Dim lValue As Long

    Dim sValue As String

    Select Case lType
        Case REG_SZ
            sValue = vValue & Chr$(0)
            SetValueEx = RegSetValueExString(hKey, sValueName, 0&, lType, sValue, Len(sValue))
        Case REG_DWORD
            lValue = vValue
            SetValueEx = RegSetValueExLong(hKey, sValueName, 0&, lType, lValue, 4)
    End Select

End Function

'------------------------------------------------------------------------------------------
'
' Respons�vel : Jo�o Mac-Cormick
' Dt Desenv : 03/06/2000
'
' seleciona uma chave de registro e grava um valor
Public Function SetKeyValue(lKeyRoot As Long, _
                            sKeyName As String, _
                            sValueName As String, _
                            vValueSetting As Variant, _
                            lValueType As Long) As Long
    Dim lRetVal As Long      'result of the SetValueEx function

    Dim hKey    As Long         'handle of open key

    'lRetVal = RegOpenKeyEx(HKEY_LOCAL_MACHINE, "Software", 0, KEY_ALL_ACCESS, hKey)
    'lRetVal = RegOpenKeyEx(hKey, "Microsoft", 0, KEY_ALL_ACCESS, hKey)
    'lRetVal = RegOpenKeyEx(hKey, "Windows", 0, KEY_ALL_ACCESS, hKey)
    'lRetVal = RegOpenKeyEx(hKey, "CurrentVersion", 0, KEY_ALL_ACCESS, hKey)
    'lRetVal = RegOpenKeyEx(hKey, "Setup", 0, KEY_ALL_ACCESS, hKey)
    ' abre uma chave espec�fica
    lRetVal = RegOpenKeyEx(lKeyRoot, sKeyName, 0, KEY_SET_VALUE, hKey)
    SetKeyValue = SetValueEx(hKey, sValueName, lValueType, vValueSetting)
    RegCloseKey (hKey)
End Function

'------------------------------------------------------------------------------------------
'
' Respons�vel : Jo�o Mac-Cormick
' Dt Desenv : 03/06/2000
'
' rotina da bolha
Public Sub Bubble(frm As Form)
    frm.Show
    Dim a       As Integer

    Dim b       As Integer

    Dim C       As Integer

    Dim d       As Integer

    Dim e       As Integer

    Dim F       As Integer

    Dim W       As Integer

    Dim x       As Integer

    Dim y       As Integer

    Dim Z       As Integer

    Dim current As Double

    frm.Hide
    Call frm.Move(0, 0)
    W = frm.Height
    x = frm.Width
    y = frm.Top
    Z = frm.Left
    a = 0
    b = 0
    C = W
    d = x
    e = y
    F = Z

    Do While a < frm.Height / 15 - 10 Or b < frm.Width / 15 - 10
        a = a + 25
        b = b + 25
        e = e + 70
        F = F + 70

        If a > frm.Height / 15 Then a = a - 24
        If b > frm.Width / 15 Then b = b - 24
        Call frm.Move(F, e, d, C)
        current = Timer

        Do While Timer - current < 0.1
            DoEvents
        Loop

        frm.Show
        Call SetWindowRgn(frm.hwnd, CreateEllipticRgn(0, 0, b, a), True)
    Loop

    current = Timer

    Do While Timer - current < 1
        DoEvents
    Loop

    Call SetWindowRgn(frm.hwnd, CreateEllipticRgn(0, 0, 0, 0), True)
End Sub

'------------------------------------------------------------------------------------------
' rotina de informa��es de rede
Public Function GetMACAddress() As String
    'retrieve the MAC Address for the network controller
    'installed, returning a formatted string
    Dim tmp      As String

    Dim pASTAT   As Long

    Dim NCB      As NET_CONTROL_BLOCK

    Dim AST      As ASTAT

    Dim EnumLANA As LANA_ENUM

    Dim Indice   As Byte

    Indice = 0

    Do
        'The IBM NetbIOS 3.0 specifications defines four basic
        'NetbIOS environments under the NCBRESET command. Win32
        'follows the OS/2 Dynamic Link Routine (DLR) environment.
        'This means that the first NCB issued by an application
        'must be a NCBRESET, with the exception of NCBENUM.
        'The Windows NT implementation differs from the IBM
        'NetbIOS 3.0 specifications in the NCB_CALLNAME field.
        NCB.ncb_command = NCBRESET
        Call Netbios(NCB)
        'NCB.ncb_command = NCBENUM
        'NCB.ncb_buffer = 2000000000 'EnumLANA
        'NCB.ncb_length = 32500 ' int EnumLANA
        'Call Netbios(NCB)
        'To get the Media Access Control (MAC) address for an
        'ethernet adapter programmatically, use the Netbios()
        'NCBASTAT command and provide a "*" as the name in the
        'NCB.ncb_CallName field (in a 16-chr string).
        NCB.ncb_callname = "*               "
        NCB.ncb_command = NCBASTAT
        'For machines with multiple network adapters you need to
        'enumerate the LANA numbers and perform the NCBASTAT
        ' Command on each. Even when you have a single network
        'adapter, it is a good idea to enumerate valid LANA numbers
        'first and perform the NCBASTAT on one of the valid LANA
        'numbers. It is considered bad programming to hardcode the
        'LANA number to 0 (see the comments section below).
        NCB.ncb_lana_num = Indice
        NCB.ncb_length = Len(AST)
        pASTAT = HeapAlloc(GetProcessHeap(), HEAP_GENERATE_EXCEPTIONS Or HEAP_ZERO_MEMORY, NCB.ncb_length)

        If pASTAT = 0 Then
            Debug.Print "memory allocation failed!"
            Exit Function
        End If

        NCB.ncb_buffer = pASTAT
        Call Netbios(NCB)
        CopyMemory AST, NCB.ncb_buffer, Len(AST)
        tmp = Format$(Hex(AST.adapt.adapter_address(0)), "00") & " " & Format$(Hex(AST.adapt.adapter_address(1)), "00") & " " & Format$(Hex(AST.adapt.adapter_address(2)), "00") & " " & Format$(Hex(AST.adapt.adapter_address(3)), "00") & " " & Format$(Hex(AST.adapt.adapter_address(4)), "00") & " " & Format$(Hex(AST.adapt.adapter_address(5)), "00")
        HeapFree GetProcessHeap(), 0, pASTAT
        Inc Indice
    Loop Until tmp <> "00 00 00 00 00 00" Or Indice = MAX_LANA

    GetMACAddress = tmp
    '--end block--'
End Function

'------------------------------------------------------------------------------------------
' rotina de informa��es de rede
Public Function GetIPAddress() As String
    Dim sHostName   As String * 256

    Dim lpHost      As Long

    Dim Host        As HOSTENT

    Dim dwIPAddr    As Long

    Dim tmpIPAddr() As Byte

    Dim i           As Integer

    Dim sIPAddr     As String

    If Not SocketsInitialize() Then
        GetIPAddress = ""
        Exit Function
    End If

    'gethostname returns the name of the local host into
    'the buffer specified by the name parameter. The host
    'name is returned as a NULL-terminated string. The
    'form of the host name is dependent on the Windows
    'Sockets provider - it can be a simple host name, or
    'it can be a fully qualified domain name. However, it
    'is guaranteed that the name returned will be successfully
    'parsed by gethostbyname and WSAAsyncGetHostbyName.
    'In actual application, if no local host name has been
    ' Configured, gethostname must succeed and return a token
    'host name that gethostbyname or WSAAsyncGetHostbyName
    ' Can resolve.
    If gethostname(sHostName, 256) = SOCKET_ERROR Then
        GetIPAddress = ""
        MsgBox "Windows Sockets error " & Str$(WSAGetLastError()) & " has occurred. Unable to successfully get Host Name."
        SocketsCleanup
        Exit Function
    End If

    'gethostbyname returns a pointer to a HOSTENT structure
    '- a structure allocated by Windows Sockets. The HOSTENT
    'structure contains the results of a successful search
    'for the host specified in the name parameter.
    'The application must never attempt to modify this
    'structure or to free any of its components. Furthermore,
    'only one copy of this structure is allocated per thread,
    'so the application should copy any information it needs
    'before issuing any other Windows Sockets function calls.
    'gethostbyname function cannot resolve IP address strings
    'passed to it. Such a request is treated exactly as if an
    'unknown host name were passed. Use inet_addr to convert
    'an IP address string the string to an actual IP address,
    'then use another function, gethostbyaddr, to obtain the
    ' Contents of the HOSTENT structure.
    sHostName = Trim$(sHostName)
    lpHost = gethostbyname(sHostName)

    If lpHost = 0 Then
        GetIPAddress = ""
        MsgBox "Windows Sockets are not responding. " & "Unable to successfully get Host Name."
        SocketsCleanup
        Exit Function
    End If

    'to extract the returned IP address, we have to copy
    'the HOST structure and its members
    CopyMemory Host, lpHost, Len(Host)
    CopyMemory dwIPAddr, Host.hAddrList, 4
    ' Create an array to hold the result
    ReDim tmpIPAddr(1 To Host.hLen)
    CopyMemory tmpIPAddr(1), dwIPAddr, Host.hLen

    'and with the array, build the actual address,
    'appending a period between members
    For i = 1 To Host.hLen
        sIPAddr = sIPAddr & tmpIPAddr(i) & "."
    Next

    'the routine adds a period to the end of the
    'string, so remove it here
    GetIPAddress = Mid$(sIPAddr, 1, Len(sIPAddr) - 1)
    SocketsCleanup
End Function

'------------------------------------------------------------------------------------------
' rotina de informa��es de rede
Public Function GetIPHostName() As String
    Dim sHostName As String * 256

    If Not SocketsInitialize() Then
        GetIPHostName = ""
        Exit Function
    End If

    If gethostname(sHostName, 256) = SOCKET_ERROR Then
        GetIPHostName = ""
        MsgBox "Windows Sockets error " & Str$(WSAGetLastError()) & " has occurred.  Unable to successfully get Host Name."
        SocketsCleanup
        Exit Function
    End If

    GetIPHostName = Left$(sHostName, InStr(sHostName, Chr(0)) - 1)
    SocketsCleanup
End Function

'------------------------------------------------------------------------------------------
' rotina de informa��es de rede
Private Function HiByte(ByVal wParam As Integer) As Byte
    'note... VB4-32 users should declare this function As Integer
    HiByte = (wParam And &HFF00&) \ (&H100)
End Function

'------------------------------------------------------------------------------------------
' rotina de informa��es de rede
Private Function LoByte(ByVal wParam As Integer) As Byte
    'note... VB4-32 users should declare this function As Integer
    LoByte = wParam And &HFF&
End Function

'------------------------------------------------------------------------------------------
' rotina de informa��es de rede
Private Sub SocketsCleanup()

    If WSACleanup() <> ERROR_SUCCESS Then
        MsgBox "Socket error occurred in Cleanup."
    End If

End Sub

'------------------------------------------------------------------------------------------
' rotina de informa��es de rede
Private Function SocketsInitialize() As Boolean
    Dim WSAD    As WSADATA

    Dim sLoByte As String

    Dim sHiByte As String

    If WSAStartup(WS_VERSION_REQD, WSAD) <> ERROR_SUCCESS Then
        MsgBox "The 32-bit Windows Socket is not responding."
        SocketsInitialize = False
        Exit Function
    End If

    If WSAD.wMaxSockets < MIN_SOCKETS_REQD Then
        MsgBox "This application requires a minimum of " & CStr(MIN_SOCKETS_REQD) & " supported sockets."
        SocketsInitialize = False
        Exit Function
    End If

    If LoByte(WSAD.wVersion) < WS_VERSION_MAJOR Or (LoByte(WSAD.wVersion) = WS_VERSION_MAJOR And HiByte(WSAD.wVersion) < WS_VERSION_MINOR) Then
        sHiByte = CStr(HiByte(WSAD.wVersion))
        sLoByte = CStr(LoByte(WSAD.wVersion))
        MsgBox "Sockets version " & sLoByte & "." & sHiByte & " is not supported by 32-bit Windows Sockets."
        SocketsInitialize = False
        Exit Function
    End If

    'must be OK, so lets do it
    SocketsInitialize = True
    '--end block--'
End Function

'------------------------------------------------------------------------------------------
' rotina de informa��es de rede
Public Function GetMachineName() As String
    Dim plngSize   As Long

    Dim pstrBuffer As String

    pstrBuffer = Space$(MAX_COMPUTERNAME_LENGTH + 1)
    plngSize = Len(pstrBuffer)

    If GetComputerName(pstrBuffer, plngSize) Then
        GetMachineName = Left$(pstrBuffer, plngSize)
    End If

End Function

'------------------------------------------------------------------------------------------
'
' Respons�vel : Jo�o Mac-Cormick
' Dt Altera��o : 03/06/2000
'
' rotina de informa��es de rede
Public Function GetMachineUserName(Optional ByVal Servidor = "0&") As String
    Dim pstrUserName    As String

    Dim plngUserNameLen As Long

    Dim plngReturn      As Long

    GetMachineUserName = ""
    pstrUserName = Space(256)
    plngUserNameLen = Len(pstrUserName)

    If Servidor = "0&" Then
        plngReturn = GetUserName(pstrUserName, plngUserNameLen)
    Else
        plngReturn = WNetGetUser(Servidor, pstrUserName, plngUserNameLen)

        'plngReturn = WNetGetUser(ByVal 0&, pstrUserName, plngUserNameLen)
        If plngReturn <> 0 Then Exit Function
    End If

    ' Success - strip off the NULL.
    GetMachineUserName = Left(pstrUserName, InStr(pstrUserName, Chr(0)) - 1)
End Function

Public Sub execCmd(cmdline$)
    Dim proc  As PROCESS_INFORMATION

    Dim start As STARTUPINFO

    'initialize the startupinfo structure
    start.cb = Len(start)
    'start the shelled application
    Call CreateProcessA(0&, cmdline$, 0&, 0&, 1&, NORMAL_PRIORITY_CLASS, 0&, 0&, start, proc)
    'wait for the shelled application to finish
    Call WaitForSingleObject(proc.hProcess, INFINITE)
    Call CloseHandle(proc.hProcess)
End Sub

Function LimpaCampo(ByRef ValorAux As Variant) As Variant
    '
    ' Respons�vel : Carlos Rosa
    ' Dt Desenv : 13/05/98
    '
    ' Recebe um valor qualquer com m�scara
    ' Retorna :
    ' Campo limpo e sem m�scara
    '
    ValorAux.SelStart = 0
    ValorAux.SelLength = Len(ValorAux)
    ValorAux.SelText = ""
    ValorAux.SelLength = 0
    LimpaCampo = ValorAux
End Function

Function CompletaComZeros(ByVal ValorAux As Variant, _
                          ParteInteira As Integer, _
                          ParteDecimal As Integer)
    Dim posponto, ParteInteiraAux, ParteDecimalAux, i

    '
    ' Respons�vel : Carlos Rosa
    ' Dt Desenv : 30/04/98
    '
    ' Recebe um valor como par�metro
    ' Retorna :
    ' Valor com zeros � esquerda e � direita
    '
    posponto = InStr(ValorAux, ".")

    If posponto = 0 Then
        ParteInteiraAux = Len(ValorAux)
        ParteDecimalAux = 0
    Else
        ParteInteiraAux = Len(Left(ValorAux, posponto - 1))
        ParteDecimalAux = Len(ValorAux) - posponto
    End If

    For i = 1 To ParteInteira - ParteInteiraAux
        ValorAux = "0" & ValorAux
    Next

    If posponto = 0 Then ValorAux = ValorAux & "."

    For i = 1 To ParteDecimal - ParteDecimalAux
        ValorAux = ValorAux & "0"
    Next

    CompletaComZeros = ValorAux
End Function

Function TrocaValorBrasPorAme(ByVal Variavel As Variant) As Variant

    '
    ' Respons�vel : Carlos Rosa
    ' Dt Desenv : 27/04/98
    '
    ' Recebe um valor qualquer
    ' Retorna :
    ' Valor sem v�rgulas, em letras mai�sculas e sem espa�os vazios
    '
    ' troca "." por "P"
    '
    While InStr(Variavel, ".") <> 0
        Variavel = Mid(Variavel, 1, InStr(Variavel, ".") - 1) + "P" + Mid(Variavel, InStr(Variavel, ".") + 1)
    Wend

    '
    ' troca "," por "."
    '
    While InStr(Variavel, ",") <> 0
        Variavel = Mid(Variavel, 1, InStr(Variavel, ",") - 1) + "." + Mid(Variavel, InStr(Variavel, ",") + 1)
    Wend

    '
    ' troca "P" por ","
    '
    While InStr(Variavel, "P") <> 0
        Variavel = Mid(Variavel, 1, InStr(Variavel, "P") - 1) + "," + Mid(Variavel, InStr(Variavel, "P") + 1)
    Wend

    TrocaValorBrasPorAme = UCase(Trim(Variavel))
End Function

Function TrocaValorAmePorBras(ByVal Variavel As Variant) As Variant

    '
    ' Respons�vel : Carlos Rosa
    ' Dt Desenv : 27/04/98
    '
    ' Recebe um valor qualquer
    ' Retorna :
    ' Valor sem v�rgulas, em letras mai�sculas e sem espa�os vazios
    '
    ' troca "," por "V"
    '
    While InStr(Variavel, ",") <> 0
        Variavel = Mid(Variavel, 1, InStr(Variavel, ",") - 1) + "V" + Mid(Variavel, InStr(Variavel, ",") + 1)
    Wend

    '
    ' troca "." por ","
    '
    While InStr(Variavel, ".") <> 0
        Variavel = Mid(Variavel, 1, InStr(Variavel, ".") - 1) + "," + Mid(Variavel, InStr(Variavel, ".") + 1)
    Wend

    '
    ' troca "V" por "."
    '
    While InStr(Variavel, "V") <> 0
        Variavel = Mid(Variavel, 1, InStr(Variavel, "V") - 1) + "." + Mid(Variavel, InStr(Variavel, "V") + 1)
    Wend

    TrocaValorAmePorBras = UCase(Trim(Variavel))
End Function

Function TrocaPontoPorVirgula(ByVal ValorAux As Variant) As String

    '
    ' Respons�vel : Carlos Rosa
    ' Dt Desenv : 23/04/98
    '
    ' Recebe um valor como par�metro
    ' Retorna :
    ' Valor com a v�rgula como separador de decimal
    '
    If InStr(ValorAux, ".") = 0 Then
        TrocaPontoPorVirgula = ValorAux
    Else
        ValorAux = Mid(ValorAux, 1, InStr(ValorAux, ".") - 1) + "," + Mid(ValorAux, InStr(ValorAux, ".") + 1)
        TrocaPontoPorVirgula = ValorAux
    End If

End Function

Function MudaCaracParaZero(ByVal valor As String) As String

    '
    ' Alterado em : 22/04/1998
    ' Por : Carlos Rosa
    ' Solicitado por : -
    '
    If InStr(valor, "_") = 0 Then
        MudaCaracParaZero = valor
        Exit Function
    End If

    While InStr(valor, "_") <> 0
        valor = Mid(valor, 1, InStr(valor, "_") - 1) + "0" + Mid(valor, InStr(valor, "_") + 1)
    Wend

    MudaCaracParaZero = valor
End Function

Function LimpaMascara(ByVal Variavel As Variant) As Variant

    '
    ' Respons�vel : Carlos Rosa
    ' Dt Desenv : 09/04/98
    '
    ' Recebe um valor qualquer
    ' Retorna :
    ' Valor sem formata��o, em letras mai�sculas e sem espa�os vazios
    '
    ' troca "_" por ""
    '
    While InStr(Variavel, "_") <> 0
        Variavel = Mid(Variavel, 1, InStr(Variavel, "_") - 1) + "" + Mid(Variavel, InStr(Variavel, "_") + 1)
    Wend

    '
    ' troca "-" por ""
    '
    While InStr(Variavel, "-") <> 0
        Variavel = Mid(Variavel, 1, InStr(Variavel, "-") - 1) + "" + Mid(Variavel, InStr(Variavel, "-") + 1)
    Wend

    '
    ' troca "/" por ""
    '
    While InStr(Variavel, "/") <> 0
        Variavel = Mid(Variavel, 1, InStr(Variavel, "/") - 1) + "" + Mid(Variavel, InStr(Variavel, "/") + 1)
    Wend

    '
    ' troca "." por ""
    '
    While InStr(Variavel, ".") <> 0
        Variavel = Mid(Variavel, 1, InStr(Variavel, ".") - 1) + "" + Mid(Variavel, InStr(Variavel, ".") + 1)
    Wend

    '
    ' troca "(" por ""
    '
    While InStr(Variavel, "(") <> 0
        Variavel = Mid(Variavel, 1, InStr(Variavel, "(") - 1) + "" + Mid(Variavel, InStr(Variavel, "(") + 1)
    Wend

    LimpaMascara = Variavel

    '
    ' troca ")" por ""
    '
    While InStr(Variavel, ")") <> 0
        Variavel = Mid(Variavel, 1, InStr(Variavel, ")") - 1) + "" + Mid(Variavel, InStr(Variavel, ")") + 1)
    Wend

    '
    ' troca "'" por ""
    '
    While InStr(Variavel, "'") <> 0
        Variavel = Mid(Variavel, 1, InStr(Variavel, "'") - 1) + "" + Mid(Variavel, InStr(Variavel, "'") + 1)
    Wend

    LimpaMascara = UCase(Trim(Variavel))
End Function

Function MontaMascaraData(ByVal DataBase As Variant) As Variant
    Dim dataaux, PosicaoBarra

    '
    ' Respons�vel : Carlos Rosa
    ' Dt Desenv : 09/04/98
    '
    ' Recebe uma data como par�metro
    ' Retorna :
    ' Data alterada para formato "dd/mm/yyyy"
    '
    If DataBase = "__/__/____" Then
        MontaMascaraData = DataBase
        Exit Function
    End If

    '
    ' troca "_" por ""
    '
    While InStr(DataBase, "_") <> 0
        DataBase = Mid(DataBase, 1, InStr(DataBase, "_") - 1) + "0" + Mid(DataBase, InStr(DataBase, "_") + 1)
    Wend

    '
    ' localiza "/" separadora de ano
    '
    dataaux = DataBase

    While InStr(dataaux, "/") <> 0
        PosicaoBarra = InStr(dataaux, "/")
        dataaux = Mid(dataaux, 1, InStr(dataaux, "/") - 1) + " " + Mid(dataaux, InStr(dataaux, "/") + 1)
    Wend

    '
    ' usa ano corrente para ano n�o preenchido
    '
    If Right(DataBase, 4) <= "0000" Then
        DataBase = Left(DataBase, 6) & Year(Date)
    End If

    MontaMascaraData = Format(DataBase, "dd/mm/yyyy")
End Function

Function VerificaData2(ByVal DataBase As Variant) As Boolean
    Dim dia, mes, ano

    '
    ' Respons�vel : Marcos Esteves
    '
    ' Recebe uma data como par�metro no formato dd/mm/aaaa
    ' Retorna :
    ' "true" para data v�lida
    ' "false" para data inv�lida
    '
    If Not IsDate(DataBase) Then
        VerificaData2 = False
        Exit Function
    End If

    dia = Mid(DataBase, 1, 2)
    mes = Mid(DataBase, 4, 2)
    ano = Mid(DataBase, 7, 4)

    Select Case mes
        Case 1, 3, 5, 7, 8, 10, 12

            If dia > 31 Then
                VerificaData2 = False
                Exit Function
            End If

        Case 4, 6, 9, 11

            If dia > 30 Then
                VerificaData2 = False
                Exit Function
            End If

        Case 2

            If (ano Mod 4) = 0 Then
                If dia > 29 Then
                    VerificaData2 = False
                    Exit Function
                End If

            Else

                If dia > 28 Then
                    VerificaData2 = False
                    Exit Function
                End If
            End If

        Case Else
            VerificaData2 = False
            Exit Function
    End Select

    If ano < 1900 Or ano > 2099 Then
        VerificaData2 = False
        Exit Function
    End If

    VerificaData2 = True
End Function

Function VerificaData(ByVal DataBase As Variant) As Boolean

    '
    ' Respons�vel : Carlos Rosa
    ' Dt Desenv : 09/04/98
    '
    ' Recebe uma data como par�metro
    ' Retorna :
    ' "true" para data v�lida
    ' "false" para data inv�lida
    '
    If Not IsDate(DataBase) Then
        VerificaData = False
        Exit Function
    Else

        If CVDate(DataBase) < CVDate("01/01/1900") Or CVDate(DataBase) > CVDate("06/06/2079") Then
            VerificaData = False
            Exit Function
        End If
    End If

    VerificaData = True
End Function

Function FormataValor(ByVal ValorAux As Variant) As Variant
    Dim pos, aux

    '
    ' Respons�vel : Carlos Rosa
    ' Dt Desenv : 09/04/98
    '
    ' Recebe um valor como par�metro
    ' Retorna :
    ' Valor formatado para opera��es no banco de dados
    ' "Null"
    '
    If Not IsNumeric(ValorAux) Or Trim(ValorAux) = "" Then

        FormataValor = "Null"
        Exit Function
    End If

    aux = Format(ValorAux, "General Number")
    pos = InStr(1, aux, ",")

    If pos > 0 Then
        aux = Mid(aux, 1, pos - 1) & "." & Mid(aux, pos + 1, Len(aux) - pos)
    End If

    FormataValor = aux
End Function

Function FormataData(ByVal DataBase As Variant, _
                     TipoSQL As String, _
                     TipoDado As String) As String
    Dim dado

    '
    ' Respons�vel : Carlos Rosa
    ' Dt Desenv : 09/04/98
    '
    ' Recebe uma data, tipo de opera��o a ser efetuado no bco
    ' de dados e o tipo de dados do registro como par�metros
    ' Retorna :    '
    ' Data formatada para Inclus�o, Altera��o, Dele��o ou Sele��o
    ' "" para Erro de par�metro
    '
    ' TipoSQL   TipoDado
    '---------  ---------------
    ' "D"elete  "D"atetime
    ' "I"nsert  "S"mallDateTime
    ' "S"elect  Date"T"imn
    ' "U"pdate
    '
    If TipoSQL <> "D" And TipoSQL <> "I" And TipoSQL <> "S" And TipoSQL <> "U" Then
        MsgBox "Par�metro incorreto !", 16

        FormataData = ""
        Exit Function
    End If

    If TipoDado <> "D" And TipoDado <> "S" And TipoDado <> "T" Then
        MsgBox "Par�metro incorreto !", 16

        FormataData = ""
        Exit Function
    End If

    DataBase = Format(DataBase, "yyyymmdd")

    If UCase(TipoSQL) = "S" Then
        If UCase(TipoDado) = "D" Then
            dado = "datetime"
        ElseIf UCase(TipoDado) = "S" Then
            dado = "smalldatetime"
        Else
            dado = "datetimn"
        End If

        FormataData = " Convert( " & dado & ", ' " & DataBase & "') "
    Else ' "D", "I" ou "U"

        FormataData = DataBase
    End If

End Function

Sub CentraFrm(Formulario As Form)

    '
    ' Respons�vel : Carlos Rosa
    ' Dt Desenv : 09/04/98
    '
    Formulario.Move (Screen.Width - Formulario.Width) \ 2, (Screen.Height - Formulario.Height) \ 2
End Sub

Function VerificaSabDom(ByRef DataBase As Variant) As Boolean

    '
    ' Respons�vel : Carlos Rosa
    ' Dt Desenv : 09/04/98
    '
    ' Recebe uma data como par�metro
    ' Retorna :
    ' Data alterada para pr�ximo dia �til sem levar em conta
    ' feriados
    ' Data como foi enviada
    '
    If Weekday(DataBase) = 1 Then     ' Domingo
        MsgBox "Data Informada � um Domingo"
        VerificaSabDom = True
    Else

        If Weekday(DataBase) = 7 Then  ' S�bado
            MsgBox "Data Informada � um S�bado"
            VerificaSabDom = True
        Else
            VerificaSabDom = False
        End If
    End If

End Function

Public Function MudaValorParaGrid(Campo As String) As String
    Dim StrCampo As String

    StrCampo = Campo & " = substring(convert(varchar(12)," & Campo & "),1,(charindex('.',convert(varchar(12)," & Campo & "))-1)) + ',' +"
    StrCampo = StrCampo & " substring(convert(varchar(12)," & Campo & "),(charindex('.',convert(varchar(12)," & Campo & "))+1),12)"
    MudaValorParaGrid = StrCampo
End Function

Function TruncaDecimal(ByVal valor As Double, _
                       Optional ByVal posicao As Integer = 2) As Double
    '
    ' Respons�vel : Simonne Almeida
    ' Dt Desenv : 21/01/2000
    '
    ' Recebe um valor como par�metro, quantidade de casas decimais
    ' Retorna :
    ' valor truncado na casa decimal informada
    '
    ' Respons�vel: Jo�o Mac-Cormick
    ' Altera��o  : 15/11/2000
    '
    ' Permite n�o passar a posi��o, assumindo duas casas decimais
    '
    Dim SinalDecimal As String, pos, ConfiguracaoBrasil

    SinalDecimal = LeArquivoIni("WIN.INI", "intl", "sDecimal")

    If SinalDecimal = "." Then
        ConfiguracaoBrasil = False
    Else
        ConfiguracaoBrasil = True
    End If

    If ConfiguracaoBrasil Then
        pos = InStr(valor, ",")
    Else
        pos = InStr(valor, ".")
    End If

    If pos <> 0 Then
        valor = Mid(valor, 1, pos + posicao)
    End If

    TruncaDecimal = valor
End Function

'------------------------------------------------------------------------------------------
'
' Respons�vel : Jo�o Mac-Cormick
' Dt Desenv : 03/06/2000
'
' retorna o dv de uma conta corrente ou de uma ag�ncia conforme regra do BB
Public Function calcula_dv_cc_ou_agencia(ByVal Parte As String) As String
    Dim Peso As Integer

    Dim Soma As Integer

    Dim Parcela As Integer

    Dim dv As Integer, i

    Dim result As String

    Peso = 9
    Soma = 0

    For i = Len(Parte) To 1 Step -1
        Parcela = Peso * Val(Mid(Parte, i, 1))
        Soma = Soma + Parcela
        Peso = Peso - 1

        If Peso < 2 Then Peso = 9
    Next i

    dv = (Soma Mod 11)

    If dv = 10 Then
        result = "X"
    Else
        result = Format(dv, "0")
    End If

    calcula_dv_cc_ou_agencia = result
End Function

Public Sub adSQL(ByRef sSQL As String, ByVal sCadeia As String)
    'Jorfilho 04/11/2002 - Concatena a query com vbNewLine
    sSQL = sSQL & sCadeia & vbNewLine
End Sub

'------------------------------------------------------------------------------------------
'
' Respons�vel : Jo�o Mac-Cormick
' Dt Desenv : 03/06/2000
'
Public Sub adSQS(ByVal CadeiaSQL As String)
    Inc SQS, vbNewLine & CadeiaSQL
End Sub

'------------------------------------------------------------------------------------------
'
' Respons�vel : Jo�o Mac-Cormick
' Dt Desenv : 03/06/2000
'
' faz a express�o aumentar de um determinado
' incremento seja cadeia, num�rica ou l�gica
Public Sub Inc(Expressao As Variant, Optional ByVal Incremento As Variant = 1)

    If Not IsNull(Incremento) Then Expressao = Expressao + Incremento
End Sub

'------------------------------------------------------------------------------------------
'
' Respons�vel : Jo�o Mac-Cormick
' Dt Desenv : 03/06/2000
'
' retorna o segundo par�metro se a condi��o for verdadeira,
' caso contr�rio, o terceiro
Public Function Se(CondicaoVerdadeira As Boolean, _
                   ParametroVerdadeiro As Variant, _
                   ParametroFalso As Variant) As Variant
    Se = IIf(CondicaoVerdadeira, ParametroVerdadeiro, ParametroFalso)
End Function

'------------------------------------------------------------------------------------------
'
' Respons�vel : Jo�o Mac-Cormick
' Dt Desenv : 03/06/2000
'
' retorna o valor l�gico de uma vari�vel
Public Function Logico(Optional Variavel As Variant) As Boolean

    If IsNull(Variavel) Or IsMissing(Variavel) Or IsEmpty(Variavel) Then
        Logico = False
    ElseIf VarType(Variavel) = vbString Then
        Logico = (LCase(Mid(Variavel, 1, 1)) = "s") Or (LCase(Mid(Variavel, 1, 1)) = "y")
    Else
        Logico = CBool(Variavel)
    End If

End Function

'------------------------------------------------------------------------------------------
'
' Respons�vel : Jo�o Mac-Cormick
' Dt Desenv : 03/06/2000
'
' marca o texto; deve ser usado ao ganhar o foco; outros objetos podem ser adicionados
Public Sub MarcaTexto()

    If TypeOf Screen.ActiveControl Is TextBox Then
        Screen.ActiveControl.SelStart = 0
        Screen.ActiveControl.SelLength = Len(Screen.ActiveControl.Text)
    End If

End Sub

'------------------------------------------------------------------------------------------
'
' Respons�vel : Jo�o Mac-Cormick
' Dt Desenv : 03/06/2000
'
' retorna a data por extenso conforme a NORMA ORTOGR�FICA BRASILEIRA
Public Function DataExtenso(ByVal AData, Optional ByVal ABNT As Boolean = False) As String

    If Not IsDate(AData) Then Exit Function
    Dim ODia As Byte, OMes As String, OAno As Integer

    ODia = Day(AData)

    Select Case Month(AData)
        Case 1
            OMes = "janeiro"
        Case 2
            OMes = "fevereiro"
        Case 3
            OMes = "mar�o"
        Case 4
            OMes = "abril"
        Case 5
            OMes = "maio"
        Case 6
            OMes = "junho"
        Case 7
            OMes = "julho"
        Case 8
            OMes = "agosto"
        Case 9
            OMes = "setembro"
        Case 10
            OMes = "outubro"
        Case 11
            OMes = "novembro"
        Case 12
            OMes = "dezembro"
    End Select

    OAno = Year(AData)

    If ABNT Then
        ' padr�o da ABNT
        DataExtenso = IIf(ODia = 1, "1�", ODia) & " de " & OMes & " de " & OAno
    Else
        ' padr�o Jailson
        DataExtenso = Format(ODia, "00") & " de " & OMes & " de " & OAno
    End If

End Function

'------------------------------------------------------------------------------------------
'
' Respons�vel : Jo�o Mac-Cormick
' Dt Desenv : 03/06/2000
'
' essa fun��o pega o CGC sem m�scara (tam14) e o devolve com m�scara (tam18)
Public Function MasCGC(CGC) As String
    MasCGC = Format(IIf(Trim(Mid(CGC, 1, 2)) = "", 0, Mid(CGC, 1, 2)), "00") & "." & Format(IIf(Trim(Mid(CGC, 3, 3)) = "", 0, Mid(CGC, 3, 3)), "000") & "." & Format(IIf(Trim(Mid(CGC, 6, 3)) = "", 0, Mid(CGC, 6, 3)), "000") & "/" & Format(IIf(Trim(Mid(CGC, 9, 4)) = "", 0, Mid(CGC, 9, 4)), "0000") & "-" & Format(IIf(Trim(Mid(CGC, 13, 2)) = "", 0, Mid(CGC, 13, 2)), "00")
End Function

'------------------------------------------------------------------------------------------
'
' Respons�vel : Jo�o Mac-Cormick
' Dt Desenv : 03/06/2000
'
' essa fun��o pega o CGC com m�scara (tam18) e retorna apenas os algarismos (tam14)
Public Function DatCGC(CGC) As String
    DatCGC = Mid(CGC, 1, 2) & Mid(CGC, 4, 3) & Mid(CGC, 8, 3) & Mid(CGC, 12, 4) & Mid(CGC, 17, 2)
End Function

'------------------------------------------------------------------------------------------
'
' Respons�vel : Jo�o Mac-Cormick
' Dt Desenv : 03/06/2000
'
' essa fun��o pega o CPF sem m�scara (tam11) e o devolve com m�scara (tam14)
Public Function MasCPF(CPF) As String
    MasCPF = Format(IIf(Trim(Mid(CPF, 1, 3)) = "", 0, Mid(CPF, 1, 3)), "000") & "." & Format(IIf(Trim(Mid(CPF, 4, 3)) = "", 0, Mid(CPF, 4, 3)), "000") & "." & Format(IIf(Trim(Mid(CPF, 7, 3)) = "", 0, Mid(CPF, 7, 3)), "000") & "-" & Format(IIf(Trim(Mid(CPF, 10, 2)) = "", 0, Mid(CPF, 10, 2)), "00")
End Function

'------------------------------------------------------------------------------------------
'
' Respons�vel : Jo�o Mac-Cormick
' Dt Desenv : 03/06/2000
'
' essa fun��o pega o CPF com m�scara (tam14) e retorna apenas os algarismos (tam11)
Public Function DatCPF(CPF) As String
    DatCPF = Mid(CPF, 1, 3) & Mid(CPF, 5, 3) & Mid(CPF, 9, 3) & Mid(CPF, 13, 2)
End Function

'------------------------------------------------------------------------------------------
'
' Respons�vel : Jo�o Mac-Cormick
' Dt Desenv : 03/06/2000
'
' essa fun��o pega o CEP sem m�scara (tam8) e o devolve com m�scara (tam11)
Public Function MasCEP(Cep) As String
    MasCEP = Format(IIf(Trim(Mid(Cep, 1, 2)) = "", 0, Mid(Cep, 1, 2)), "00") & "." & Format(IIf(Trim(Mid(Cep, 3, 3)) = "", 0, Mid(Cep, 3, 3)), "000") & "-" & Format(IIf(Trim(Mid(Cep, 6, 3)) = "", 0, Mid(Cep, 6, 3)), "000")
End Function

'------------------------------------------------------------------------------------------
'
' Respons�vel : Jo�o Mac-Cormick
' Dt Desenv : 03/06/2000
'
' essa fun��o pega o CEP com m�scara (tam11) e retorna apenas os algarismos (tam8)
Public Function DatCEP(Cep) As String
    DatCEP = Mid(Cep, 1, 2) & Mid(Cep, 4, 3) & Mid(Cep, 7, 3)
End Function

'------------------------------------------------------------------------------------------
'
' Respons�vel : Jo�o Mac-Cormick
' Dt Desenv : 03/06/2000
'
' retorna a cadeia separado por CRLF e identada pela margem esquerda
Public Function Identa(ByVal OTexto, _
                       ByVal Margem As Long, _
                       ByVal Tamanho As Long) As String
    Dim Auxiliar As String

    Dim Inicio   As Long

    Auxiliar = ""

    If Not IsNull(OTexto) Then
        Inicio = 1

        Do
            Inc Auxiliar, Mid(OTexto, Inicio, Tamanho)
            Inc Inicio, Tamanho

            If Mid(OTexto, Inicio, Tamanho) <> "" Then
                Inc Auxiliar, vbCrLf & Space(Margem)

                While Mid(OTexto, Inicio, 1) = " "
                    Inc Inicio
                Wend

            End If

        Loop Until Mid(OTexto, Inicio, Tamanho) = ""

    End If

    Identa = Auxiliar
End Function

'------------------------------------------------------------------------------------------
'
' Respons�vel : Jo�o Mac-Cormick
' Dt Desenv : 03/06/2000
'
' retorna a quantidade de repeti��es de uma subcadeia numa cadeia
Public Function Repeticao(Subcadeia As String, Cadeia As String) As Long
    Dim QtdRepeticoes As Long

    QtdRepeticoes = 0

    If Not IsNull(Cadeia) And Not IsNull(Subcadeia) Then
        Dim Indice As Long

        Indice = 1

        While InStr(Indice, Cadeia, Subcadeia) > 0
            Inc QtdRepeticoes
            Indice = InStr(Indice, Cadeia, Subcadeia) + Len(Subcadeia)
        Wend

    End If

    Repeticao = QtdRepeticoes
End Function

'------------------------------------------------------------------------------------------
'
' Respons�vel : Jo�o Mac-Cormick
' Dt Desenv : 03/06/2000
'
' retorna a cadeia repetida n vezes
Public Function Repete(ByVal QTD As Long, ByVal Cadeia As String)
    Dim Auxiliar As String

    Auxiliar = ""

    If QTD > 0 Then
        If Len(Cadeia) = 1 Then
            Auxiliar = String(QTD, Cadeia)
        Else
            Dim Indice As Long

            For Indice = 1 To QTD
                Inc Auxiliar, Cadeia
            Next Indice

        End If
    End If

    Repete = Auxiliar
End Function

'------------------------------------------------------------------------------------------
'
' Respons�vel : Jo�o Mac-Cormick
' Dt Desenv : 03/06/2000
'
' troca o valor de dois elementos
Public Sub Troca(Elemento1, Elemento2)
    Dim a, b

    a = Elemento1
    b = Elemento2
    Elemento1 = b
    Elemento2 = a
End Sub

'------------------------------------------------------------------------------------------
'
' Respons�vel : Jo�o Mac-Cormick
' Dt Desenv : 03/06/2000
'
' retorna a cadeia sem acentua��o
Public Function TiraAcento(ByVal Cadeia As String) As String
    Const CadeiaComAcento = "��������������������������������ܟ�������������������Ū��"

    Const CadeiaSemAcento = "aeiouyAEIOUYaeiouAEIOUaeiouyAEIOUYaeiouAEIOUcCnNaoAOaAaoo"

    Dim IndiceCadeia As Integer

    Dim IndiceAcento As Integer

    For IndiceCadeia = 1 To Len(Cadeia)
        IndiceAcento = InStr(CadeiaComAcento, Mid(Cadeia, IndiceCadeia, 1))

        If Logico(IndiceAcento) Then
            Mid(Cadeia, IndiceCadeia) = Mid(CadeiaSemAcento, IndiceAcento, 1)
        End If

    Next IndiceCadeia

    TiraAcento = Cadeia
End Function

'------------------------------------------------------------------------------------------
'
' Respons�vel : Jo�o Mac-Cormick
' Dt Desenv : 03/06/2000
'
' retorna o n�mero com uma m�scara, obedecendo a configura��o regional
Public Function MascaraValor(ByVal OValor As String, _
                             Optional ByVal OTamanho As Byte = 0, _
                             Optional ByVal AMascara As String = "###,###,###,##0.00") As String
    Dim Auxiliar As String

    OValor = Replace(OValor, ",", ".")

    If LeArquivoIni("WIN.INI", "intl", "sDecimal") = "," Then
        Auxiliar = Format(Val(OValor), AMascara)
    Else
        Auxiliar = TrocaValorAmePorBras(Format(Val(OValor), AMascara))
    End If

    If OTamanho > Len(Auxiliar) Then
        Auxiliar = Space(OTamanho - Len(Auxiliar)) & Auxiliar
    End If

    MascaraValor = Auxiliar
End Function

'------------------------------------------------------------------------------------------
'
' Respons�vel : Jo�o Mac-Cormick
' Dt Desenv : 03/06/2000
'
' retorna o campo com espa�os � direita se for cadeia, com zeros � esquerda se num�rico
' ou com formata��o de um campo l�gico conforme o tamanho
Public Function ExportaCampo(ByVal OCampo, _
                             ByVal OTamanho As Long, _
                             Optional ByVal QtdCasasDecimais As Integer = -1) As Variant
    OTamanho = Abs(OTamanho)

    If VarType(OCampo) = vbBoolean Then
        If OTamanho > 3 Then
            ExportaCampo = IIf(OCampo, " true", "false")
        ElseIf OTamanho = 3 Then
            ExportaCampo = IIf(OCampo, "sim", "n�o")
        ElseIf OTamanho = 2 Then
            ExportaCampo = IIf(OCampo, "-1", " 0")
        ElseIf OTamanho = 1 Then

            If QtdCasasDecimais > 0 Then
                ExportaCampo = IIf(OCampo, "1", "0")
            Else
                ExportaCampo = IIf(OCampo, "s", "n")
            End If
        End If

    ElseIf QtdCasasDecimais < 0 Then
        ExportaCampo = Mid(OCampo & Repete(OTamanho, " "), 1, OTamanho)
    Else

        If VarType(OCampo) = vbString Then
            ExportaCampo = Format(Val("1" & Repete(QtdCasasDecimais, "0")) * Val(OCampo), Repete(OTamanho - IIf(Val(OCampo) < 0, 1, 0), "0"))
        Else

            If IsNull(OCampo) Then OCampo = 0
            ExportaCampo = Format(Val("1" & Repete(QtdCasasDecimais, "0")) * OCampo, Repete(OTamanho - IIf(OCampo < 0, 1, 0), "0"))
        End If
    End If

End Function

'------------------------------------------------------------------------------------------
'
' Respons�vel : Jo�o Mac-Cormick
' Dt Desenv : 03/06/2000
'
' separa uma cadeia de n�meros pelos caracteres n�o num�ricos
Public Function SeparaNumeros(ByVal Cadeia As String) As Variant
    Dim CadeiaAux As String, posicao As Long

    CadeiaAux = ""

    For posicao = 1 To Len(Cadeia)

        If IsNumeric(Mid(Cadeia, posicao, 1)) Then
            Inc CadeiaAux, Mid(Cadeia, posicao, 1)
        ElseIf Len(CadeiaAux) > 0 Then

            If IsNumeric(Mid(CadeiaAux, Len(CadeiaAux), 1)) Then
                Inc CadeiaAux, ";"
            End If
        End If

    Next posicao

    If Not IsNumeric(Mid(CadeiaAux, Len(CadeiaAux), 1)) Then
        CadeiaAux = Mid(CadeiaAux, 1, Len(CadeiaAux) - 1)
    End If

    SeparaNumeros = Split(CadeiaAux, ";")
End Function

'------------------------------------------------------------------------------------------
'
' Respons�vel : Jo�o Mac-Cormick
' Dt Desenv : 03/06/2000
'
' retorna a pr�xima posi��o da cadeia que n�o � valor num�rico
Public Function PegaProximo(ByVal Cadeia As String, ByVal Inicio As Long) As Long
    Dim Indice As Long

    For Indice = Inicio To Len(Cadeia)

        If Not Logico(InStr("0123456789", Mid(Cadeia, Indice, 1))) Then Exit For
    Next

    PegaProximo = Indice
End Function

'------------------------------------------------------------------------------------------
'
' Respons�vel : Jo�o Mac-Cormick
' Dt Desenv : 03/06/2000
'
' retorna o SQL passados uma cadeia, um vetor e opcionalmente um marcador
Public Function MontaSQL(ByVal Cadeia As String, _
                         ByVal Vetor As Variant, _
                         Optional ByVal FormataSQL As Boolean = True, _
                         Optional ByVal Marcador As String = "#") As String
    Dim TamCad   As Long

    Dim TamBusca As Long

    Dim Indice   As Long

    Dim Auxiliar As String

    Dim Erro     As Boolean

    TamCad = Len(Cadeia)
    TamBusca = Len(Marcador)
    Indice = InStr(Cadeia, Marcador)

    If TamBusca = 0 Or TamBusca > TamCad Or Indice = 0 Then
        Auxiliar = Cadeia
    Else
        Auxiliar = ""
        Dim IndiceAnterior As Long

        Dim Proximo        As Long

        Dim Substitui

        IndiceAnterior = 1

        Do
            Proximo = PegaProximo(Cadeia, Indice + TamBusca)

            If Proximo = Indice + TamBusca Then
                Substitui = Marcador
            Else
                Erro = False
                On Local Error GoTo TrataErro
                Substitui = Vetor(Mid(Cadeia, Indice + TamBusca, Proximo - Indice - TamBusca))
                On Local Error GoTo 0

                If Erro Then
                    Substitui = Mid(Cadeia, Indice, Proximo - Indice)
                Else

                    If FormataSQL Then Substitui = FormatoSQL(Substitui)
                End If
            End If

            Inc Auxiliar, Mid(Cadeia, IndiceAnterior, Indice - IndiceAnterior) + Substitui
            IndiceAnterior = Proximo
            Indice = InStr(IndiceAnterior, Cadeia, Marcador)
        Loop Until Indice = 0

        Inc Auxiliar, Mid(Cadeia, IndiceAnterior)
    End If

    MontaSQL = Auxiliar
    Exit Function
TrataErro:
    Erro = True

    Resume Next
End Function

'------------------------------------------------------------------------------------------
'
' Respons�vel : Jo�o Mac-Cormick
' Dt Desenv : 03/06/2000
'
' converte para um valor se for poss�vel
Public Function ValN(ByVal OValor As Variant) As Variant

    If VarType(OValor) <> vbDataObject And VarType(OValor) <> vbDate And VarType(OValor) <> vbString And VarType(OValor) <> vbArray And VarType(OValor) <> vbError And VarType(OValor) <> vbObject Then
        ValN = OValor
    ElseIf IsNumeric(OValor) Or (VarType(OValor) = vbString And IsNumeric(Val(OValor))) Then
        ValN = Val(OValor)
    Else
        ValN = Null
    End If

End Function

'------------------------------------------------------------------------------------------
'
' Respons�vel : Jo�o Mac-Cormick
' Dt Desenv : 03/06/2000
'
' retorna o caracter divisor de milhar
Public Function RegionalMilhar() As String
    RegionalMilhar = LeArquivoIni("WIN.INI", "intl", "sThousand")
End Function

'------------------------------------------------------------------------------------------
'
' Respons�vel : Jo�o Mac-Cormick
' Dt Desenv : 03/06/2000
'
' retorna o caracter divisor de decimal
Public Function RegionalDecimal() As String
    RegionalDecimal = LeArquivoIni("WIN.INI", "intl", "sDecimal")
End Function

'------------------------------------------------------------------------------------------
'
' Respons�vel : Jo�o Mac-Cormick
' Dt Desenv : 03/06/2000
'
' retorna se o dia vem antes do m�s
Public Function DiaMes() As Boolean
    Dim AData As String

    AData = Format("03/06/1999", "dd/mm/yyyy")
    DiaMes = InStr(AData, "3") < InStr(AData, "6")
End Function

'------------------------------------------------------------------------------------------
'
' Respons�vel : Jo�o Mac-Cormick
' Dt Desenv : 03/06/2000
'
' retorna o valor no formato regional da m�quina para data e n�mero passados em portugu�s
' Portugu�s (Brasil), Espanhol (Tradicional), Alem�o (Padr�o), Franc�s (Padr�o) e Ingl�s (EUA)
Public Function Regional(ByVal Data_Numero As String) As String

    If InStr(Data_Numero, "/") > 1 And IsDate(Data_Numero) Then
        ''If DiaMes Then
        Regional = Format(Data_Numero, "dd/mm/yyyy Hh:Nn:Ss")
        ''Else
        ''Regional = Format(Data_Numero, "mm/dd/yyyy Hh:Nn:Ss")
        ''End If
    Else
        Data_Numero = Replace(Data_Numero, ".", "")
        Data_Numero = Replace(Data_Numero, ",", RegionalDecimal)

        If IsNumeric(Data_Numero) Then
            Regional = Data_Numero
        Else
            Regional = ""
        End If
    End If

End Function

'------------------------------------------------------------------------------------------
'
' Respons�vel : Jo�o Mac-Cormick
' Dt Desenv : 03/06/2000
'
' retorna o valor num�rico acrescentado de um marcador usado
' para ser passado com um par�metro string que � num�rico na fun��o FormatoSQL
' e pode retornar se o valor foi acrescentado de um marcador num�rico
Public Function numero(ByVal ONumero, _
                       Optional ByRef NaoPasseEsteParametro As Variant) As String
    Dim Auxiliar As String

    Const Marcador = "#!@#$%^&*()_+|~{#}[#]1234567890#"

    If IsMissing(NaoPasseEsteParametro) Then
        Auxiliar = Marcador & IIf(IsNull(ONumero), 0, ONumero)
    Else
        Auxiliar = Se(Logico(InStr(ONumero, Marcador)), "sim", "n�o")
        NaoPasseEsteParametro = Se(Logico(Auxiliar), Len(Marcador) + 1, 0)
    End If

    numero = Auxiliar
End Function

'------------------------------------------------------------------------------------------
'
' Respons�vel : Jo�o Mac-Cormick
' Dt Desenv : 03/06/2000
'
' retorna o campo verificando o delimitador MSSQL
Public Function DelimitadorSQL(ByVal Campo As String) As String
    DelimitadorSQL = "'" & Replace(Campo, "'", "''") & "'"
End Function

'------------------------------------------------------------------------------------------
'
' Respons�vel : Jo�o Mac-Cormick
' Dt Desenv : 03/06/2000
'
' retorna o campo com delimitador de acordo com o seu tipo
Private Function FormatoCampoSQL(Optional Campo As Variant) As String
    On Local Error GoTo TrataErro

    If IsNull(Campo) Or IsMissing(Campo) Or IsEmpty(Campo) Then

        FormatoCampoSQL = "NULL"
        Exit Function
    End If

    If VarType(Campo) = vbString Or VarType(Campo) = vbDate Then
        Dim CampoStr As String

        Dim Inicio   As Byte

        CampoStr = CStr(Campo)

        If UCase(CampoStr) = "NULL" Then

            FormatoCampoSQL = "NULL"
        ElseIf Logico(numero(CampoStr, Inicio)) Then
            CampoStr = Meio(Campo, Inicio)

            FormatoCampoSQL = Str(Regional(CampoStr))
        ElseIf IsDate(CampoStr) Then

            'FormatoCampoSQL = DelimitadorSQL(Format(Regional(CampoStr), "yyyymmdd hh:nn:ss"))
            FormatoCampoSQL = DelimitadorSQL(Format(CampoStr, "yyyymmdd hh:nn:ss"))
        Else

            FormatoCampoSQL = DelimitadorSQL(CampoStr)
        End If

    ElseIf VarType(Campo) = vbBoolean Then

        FormatoCampoSQL = Abs(CInt(Campo))
    ElseIf IsNumeric(Campo) Then

        FormatoCampoSQL = Str(Campo)
    ElseIf VarType(Campo) >= vbArray Then
        Dim Auxiliar As String

        Dim OElemento

        Auxiliar = ""

        For Each OElemento In Campo
            Inc Auxiliar, ", " & FormatoCampoSQL(OElemento)
        Next OElemento

        FormatoCampoSQL = Mid(Auxiliar, 3)
    Else

        FormatoCampoSQL = "NULL"
    End If

    Exit Function
TrataErro:

    FormatoCampoSQL = "0"
    MsgBox "O campo passado ==>" & Se(CampoStr = "", Campo, CampoStr) & "<== n�o p�de ser convertido corretamente.", vbCritical, App.Title
End Function

'------------------------------------------------------------------------------------------
'
' Respons�vel : Jo�o Mac-Cormick
' Dt Desenv : 03/06/2000
'
' retorna a fun��o em MSSQL que transforma um campo 0/1 em SIM/N�O
Public Function SQLSimNao(ByVal CampoSQL As String) As String
    CampoSQL = AjustaBrEntre(CampoSQL, 0)
    SQLSimNao = "ISNULL( NULLIF( ISNULL( CONVERT( CHAR(5), NULLIF( CONVERT( TINYINT, " & CampoSQL & ") ,0)), " & FormatoSQL("n�o") & "), " & FormatoSQL("1") & "), " & FormatoSQL("sim") & ") " & CampoSQL
End Function

'------------------------------------------------------------------------------------------
'
' Respons�vel : Jo�o Mac-Cormick
' Dt Desenv : 03/06/2000
'
' retorna os campos com v�rgula sem a �ltima v�rgula
Public Function FormatoSQL(ParamArray OsCampos()) As String

    If IsMissing(OsCampos) Then

        FormatoSQL = ""
        Exit Function
    End If

    Dim Cadeia As String

    Dim OCampo As Variant

    Cadeia = ""

    For Each OCampo In OsCampos
        Inc Cadeia, ", " & FormatoCampoSQL(OCampo)
    Next

    FormatoSQL = Mid(Cadeia, 3)
End Function

'------------------------------------------------------------------------------------------
'
' Respons�vel : Jo�o Mac-Cormick
' Dt Desenv : 03/06/2000
'
' retorna os campos com uma cadeia no final
Public Function FormatoSQLF(ByVal ACadeia As String, ParamArray OsCampos()) As String

    If IsMissing(OsCampos) Then

        FormatoSQLF = ""
        Exit Function
    End If

    Dim Cadeia As String

    Dim OCampo As Variant

    Cadeia = ""

    For Each OCampo In OsCampos
        Inc Cadeia, ", " & FormatoCampoSQL(OCampo)
    Next

    FormatoSQLF = Mid(Cadeia, 3) & ACadeia
End Function

'------------------------------------------------------------------------------------------
'
' Respons�vel : Jo�o Mac-Cormick
' Dt Desenv : 03/06/2000
'
' retorna os campos com uma cadeia no in�cio
Public Function FormatoSQLI(ByVal ACadeia As String, ParamArray OsCampos()) As String

    If IsMissing(OsCampos) Then

        FormatoSQLI = ""
        Exit Function
    End If

    Dim Cadeia As String

    Dim OCampo As Variant

    Cadeia = ""

    For Each OCampo In OsCampos
        Inc Cadeia, ", " & FormatoCampoSQL(OCampo)
    Next

    FormatoSQLI = ACadeia & Mid(Cadeia, 3)
End Function

'------------------------------------------------------------------------------------------
'
' Respons�vel : Jo�o Mac-Cormick
' Dt Desenv : 03/06/2000
'
' retorna se o elemento est� contido em algum dos elementos
Public Function EstaContido(ByVal OElementoTeste, ParamArray OsElementos()) As Boolean

    If IsNull(OElementoTeste) Then
        EstaContido = False
        Exit Function
    End If

    Dim OElemento

    Dim Achou As Boolean

    Achou = False

    For Each OElemento In OsElementos
        Achou = EstaContidoCampo(OElementoTeste, OElemento)

        If Achou Then Exit For
    Next

    EstaContido = Achou
End Function

'------------------------------------------------------------------------------------------
'
' Respons�vel : Jo�o Mac-Cormick
' Dt Desenv : 03/06/2000
'
' retorna se o elemento est� contido em algum dos elementos
Private Function EstaContidoCampo(ByVal OTeste, ByVal OCampo) As Boolean

    If IsNull(OCampo) Or IsMissing(OCampo) Or IsEmpty(OCampo) Or IsNull(OTeste) Or IsMissing(OTeste) Or IsEmpty(OTeste) Or VarType(OTeste) >= vbArray Then
        EstaContidoCampo = False
        Exit Function
    End If

    Dim Achou As Boolean

    Achou = False

    If VarType(OCampo) >= vbArray Then
        Dim OElemento

        For Each OElemento In OCampo
            Achou = EstaContidoCampo(OTeste, OElemento)

            If Achou Then Exit For
        Next

    Else
        Achou = (OTeste = OCampo) And (VarType(OTeste) = VarType(OCampo))
    End If

    EstaContidoCampo = Achou
End Function

'------------------------------------------------------------------------------------------
'
' Respons�vel : Jo�o Mac-Cormick
' Dt Desenv : 03/06/2000
'
' retorna o maior elemento entre os elementos
Public Function Maior(ParamArray OsElementos()) As Variant
    Dim Elemento As Variant

    Dim Anterior As Variant

    For Each Elemento In OsElementos

        If IsEmpty(Anterior) Or IsNull(Anterior) Or IsMissing(Anterior) Then
            Anterior = Elemento
        ElseIf Not IsNull(Elemento) And Not IsMissing(Elemento) Then
            Anterior = Se(Anterior > Elemento, Anterior, Elemento)
        End If

    Next

    Maior = Anterior
End Function

'------------------------------------------------------------------------------------------
'
' Respons�vel : Jo�o Mac-Cormick
' Dt Desenv : 03/06/2000
'
' retorna o menor elemento entre os elementos
Public Function Menor(ParamArray OsElementos()) As Variant
    Dim Elemento As Variant

    Dim Anterior As Variant

    For Each Elemento In OsElementos

        If IsEmpty(Anterior) Or IsNull(Anterior) Or IsMissing(Anterior) Then
            Anterior = Elemento
        ElseIf Not IsNull(Elemento) And Not IsMissing(Elemento) Then
            Anterior = Se(Anterior < Elemento, Anterior, Elemento)
        End If

    Next

    Menor = Anterior
End Function

'------------------------------------------------------------------------------------------
'
' Respons�vel : Jo�o Mac-Cormick
' Dt Desenv : 03/06/2000
'
' retorna uma cadeia intermedi�ria entre duas partes que pode ser
' o trecho de uma cadeia ou a posi��o na cadeia
Public Function Meio(ByVal Cadeia As String, PalavraInicio, Optional PalavraFim) As String
    Dim Inicio As Long, Tamanho As Long, TamInicio As Long

    Meio = ""

    If VarType(PalavraInicio) = vbString Then
        Inicio = InStr(1, Cadeia, PalavraInicio)

        If Inicio = 0 Then Exit Function
        TamInicio = Len(PalavraInicio)
    ElseIf IsNumeric(PalavraInicio) Then
        Inicio = CInt(PalavraInicio)

        If Inicio < 1 Then Inicio = 1
        TamInicio = 0
    Else
        Exit Function
    End If

    If VarType(PalavraFim) = vbString Then
        Tamanho = InStr(Inicio + TamInicio, Cadeia, PalavraFim) - Inicio
    ElseIf IsNumeric(PalavraFim) Then
        Tamanho = CInt(PalavraFim) - Inicio

        If Tamanho > Len(Cadeia) Then
            Tamanho = Len(Cadeia)
        End If

    ElseIf IsMissing(PalavraFim) Then
        Tamanho = Len(Cadeia) - Inicio + 1
    Else
        Exit Function
    End If

    If Tamanho < 1 Then Exit Function
    Meio = Mid(Cadeia, Inicio, Tamanho)
End Function

'------------------------------------------------------------------------------------------
'
' Respons�vel : Jo�o Mac-Cormick
' Dt Desenv : 03/06/2000
'
' retorna a cadeia com a quantidade solicitada de espa�os em branco dentro da cadeia
Public Function AjustaBrEntre(ByVal Cadeia As String, _
                              Optional ByVal QtdBrancos As Integer = 1) As String
    Dim Auxiliar As String

    Dim Tamanho  As Integer

    Dim Indice   As Integer

    Auxiliar = ""
    Cadeia = Trim(Cadeia)
    Tamanho = Len(Cadeia)
    Indice = InStr(Cadeia, " ")

    While (Indice < Tamanho) And (Indice > 0)
        Auxiliar = Auxiliar & Mid(Cadeia, 1, Indice - 1) & Space(QtdBrancos)
        Cadeia = Trim(Mid(Cadeia, Indice))
        Indice = InStr(Cadeia, " ")
    Wend

    AjustaBrEntre = Auxiliar & Cadeia
End Function

'------------------------------------------------------------------------------------------
'
' Respons�vel : Jo�o Mac-Cormick
' Dt Desenv : 03/06/2000
'
' retorna um n�mero aleat�rio num determinado intervalo
Public Function Aleatorio(InicioIntervalo As Variant, FimIntervalo As Variant) As Variant

    If FimIntervalo > InicioIntervalo Then
        Aleatorio = Int((FimIntervalo - InicioIntervalo + 1) * Rnd + InicioIntervalo)
    Else
        Aleatorio = 0
    End If

End Function

'------------------------------------------------------------------------------------------
'
' Respons�vel : Jo�o Mac-Cormick
' Dt Desenv : 03/06/2000
'
' retorna a data somada, usando a fun��o em VB com uma op��o de formato opcional em portugu�s
Public Function SomaData(ByVal OIntervalo As String, _
                         ByVal AQuantidade As Double, _
                         ByVal AData As Date, _
                         Optional ByVal OFormatoData As String = "dd/mm/yyyy hh:nn:ss") As Date
    SomaData = Format(DateAdd(OIntervalo, AQuantidade, AData), OFormatoData)
End Function

'------------------------------------------------------------------------------------------
'
' Respons�vel : Jo�o Mac-Cormick
' Dt Desenv : 03/06/2000
'
' faz uma pausa de n segundos
Public Sub Pause(ByVal Segundo As Single)
    Call Sleep(Int(Segundo * 1000#))
End Sub

'------------------------------------------------------------------------------------------
'
' Respons�vel : Jo�o Mac-Cormick
' Dt Desenv : 03/06/2000
'
' recebe uma inst�ncia e retorna a janela
Function PegaJanela(ByVal AInstanciaProcesso As Double) As Double
    On Local Error GoTo TrataErro
    Dim AInstanciaJanela As Long, AInstanciaProcessoJanela As Long

    ' pega a primeira janela que achar
    AInstanciaJanela = FindWindow(vbNullString, vbNullString)
    ' busca em todas as janelas at� n�o encontrar mais
    PegaJanela = 0

    Do Until AInstanciaJanela = 0

        ' verifica se � a janela principal de uma aplica��o
        If GetParent(AInstanciaJanela) = 0 Then
            ' verifica se � a inst�ncia da aplica��o passada
            Call GetWindowThreadProcessId(AInstanciaJanela, AInstanciaProcessoJanela)

            If AInstanciaProcesso = AInstanciaProcessoJanela Then
                ' achou a janela do processo
                PegaJanela = AInstanciaJanela
                Exit Do
            End If
        End If

        AInstanciaJanela = GetWindow(AInstanciaJanela, GW_HWNDNEXT)
    Loop

    Exit Function
TrataErro:
    Call GravaMensagem("Erro na fun��o ==> " & Err.Description, "PROC", "PegaJanela", "Funcoes.bas")
End Function

'------------------------------------------------------------------------------------------
'
' Respons�vel : Jo�o Mac-Cormick
' Dt Desenv : 03/06/2000
'
' Verifica se existe uma janela associada �quela aplica��o
Function AplicacaoAtiva(ByVal AInstanciaProcesso As Double) As Boolean
    AplicacaoAtiva = PegaJanela(AInstanciaProcesso) <> 0
End Function

'------------------------------------------------------------------------------------------
'
' Respons�vel : Jo�o Mac-Cormick
' Dt Desenv : 03/06/2000
'
' substitui ?? pelos par�metros passados
Public Function MontaDescricao(ByVal ADescricaoErro As String, ParamArray OsParametros())
    Dim Descricao As String, IndiceVetor As Long, IndiceCadeia As Long, IndiceAnterior As Long

    Descricao = ""
    IndiceVetor = 0
    IndiceAnterior = 1
    IndiceCadeia = InStr(ADescricaoErro, "??")

    While (IndiceCadeia > 0) And IndiceVetor <= UBound(OsParametros)
        Inc Descricao, Mid(ADescricaoErro, IndiceAnterior, IndiceCadeia - IndiceAnterior)
        IndiceAnterior = IndiceCadeia + 2
        IndiceCadeia = InStr(IndiceAnterior, ADescricaoErro, "??")
        Inc Descricao, CStr(OsParametros(IndiceVetor))
        Inc IndiceVetor
    Wend

    Inc Descricao, Mid(ADescricaoErro, IndiceAnterior)
    MontaDescricao = Descricao
End Function

'------------------------------------------------------------------------------------------
'
' Respons�vel : Jo�o Mac-Cormick
' Dt Desenv : 03/06/2000
'
' substitui ?? pelos par�metros passados no objeto Err
Public Sub MontaErro(ByVal ADescricaoErro As String, ParamArray OsParametros())
    Dim NumErro As Long

    NumErro = Err.Number + 14000
    Dim Descricao As String, IndiceVetor As Long, IndiceCadeia As Long, IndiceAnterior As Long

    Descricao = ""
    IndiceVetor = 0
    IndiceAnterior = 1
    IndiceCadeia = InStr(ADescricaoErro, "??")

    While (IndiceCadeia > 0) And IndiceVetor <= UBound(OsParametros)
        Inc Descricao, Mid(ADescricaoErro, IndiceAnterior, IndiceCadeia - IndiceAnterior)
        IndiceAnterior = IndiceCadeia + 2
        IndiceCadeia = InStr(IndiceAnterior, ADescricaoErro, "??")
        Inc Descricao, CStr(OsParametros(IndiceVetor))
        Inc IndiceVetor
    Wend

    Inc Descricao, Mid(ADescricaoErro, IndiceAnterior)
    Err.Description = Descricao
    Err.Number = NumErro
    Exit Sub
End Sub

'------------------------------------------------------------------------------------------
'
' Respons�vel : Jo�o Mac-Cormick
' Dt Desenv : 03/06/2000
'
' retorna o valor de uma chave do registro do windows
Public Function PegaRegistro(ARaiz As Long, AChave As String) As Variant
    Dim Chave As String, SubChave As String, valor As String

    PegaRegistro = vbNull

    If InStr(AChave, "\") = 0 Then Exit Function
    Chave = Mid(AChave, 1, InStrRev(AChave, "\") - 1)
    SubChave = Mid(AChave, InStrRev(AChave, "\") + 1)

    If GetKeyValue(ARaiz, Chave, SubChave, valor) Then PegaRegistro = valor
End Function

'------------------------------------------------------------------------------------------
'
' Respons�vel : Jo�o Mac-Cormick
' Dt Desenv : 03/06/2000
'
' grava o valor em uma chave do registro do windows mesmo que a chave n�o exista
' n�o est� dispon�vel
Public Function GravaRegistro(ARaiz As Long, _
                              AChave As String, _
                              OValor As Variant) As Variant
    Dim Chave As String, SubChave As String, valor As String

    GravaRegistro = Null
End Function

'------------------------------------------------------------------------------------------
'
' Respons�vel : Jo�o Mac-Cormick
' Dt Desenv : 03/06/2000
'
' carrega um �cone na bandeja da barra de tarefas do windows
Public Sub CarregaBandeja(Bandeja As NOTIFYICONDATA, _
                          ByVal Icone As PictureBox, _
                          ByVal Mensagem As String)
    Bandeja.cbSize = Len(Bandeja)
    Bandeja.hwnd = Icone.hwnd
    Bandeja.uID = 1&
    Bandeja.uFlags = NIF_MESSAGE Or NIF_ICON Or NIF_TIP
    Bandeja.uCallbackMessage = WM_MOUSEMOVE
    Bandeja.hIcon = Icone.Picture
    Bandeja.szTip = Mensagem & Chr$(0)
    Call Shell_NotifyIcon(NIM_ADD, Bandeja)
End Sub

'------------------------------------------------------------------------------------------
'
' Respons�vel : Jo�o Mac-Cormick
' Dt Desenv : 03/06/2000
'
' descarrega a bandeja criada na barra de tarefas do windows
Public Sub DescarregaBandeja(Bandeja As NOTIFYICONDATA)
    Call Shell_NotifyIcon(NIM_DELETE, Bandeja)
End Sub

'------------------------------------------------------------------------------------------
'
' Respons�vel : Jo�o Mac-Cormick
' Dt Desenv : 03/06/2000
'
' exibe a ajuda do SEGBR
Public Sub ExibeAjudaSEGBR()
    Call Shell(Environ("WINDIR") & "\winhlp32.exe " & gsPastaLocalSegbr & "seguros.hlp", vbNormalFocus)
End Sub

Public Sub Mensagem(ByVal AMensagem)
    MsgBox AMensagem, , App.Title
End Sub

'------------------------------------------------------------------------------------------
'
' Respons�vel : Jo�o Mac-Cormick
' Dt Desenv : 03/06/2000
'
' faz a restri��o de tamanho do formul�rio ativo e centraliza-o
Public Sub Restringe(Optional LarguraMinima As Integer = -1, _
                     Optional AlturaMinima As Integer = -1, _
                     Optional LarguraMaxima As Integer = -1, _
                     Optional AlturaMaxima As Integer = -1)
    Dim Formul As Form

    Set Formul = Screen.ActiveForm

    If Not (Formul Is Nothing) Then
        If Not ((Formul.WindowState = vbMaximized) Or (Formul.WindowState = vbMinimized)) Then
            If LarguraMinima > -1 Then
                If Formul.Width < LarguraMinima Then Formul.Width = LarguraMinima
            End If

            If AlturaMinima > -1 Then
                If Formul.Height < AlturaMinima Then Formul.Height = AlturaMinima
            End If

            If LarguraMaxima > -1 Then
                If Formul.Width > LarguraMaxima Then Formul.Width = LarguraMaxima
            End If

            If AlturaMaxima > -1 Then
                If Formul.Height > AlturaMaxima Then Formul.Height = AlturaMaxima
            End If

            Formul.Move (Screen.Width - Formul.Width) / 2, (Screen.Height - Formul.Height) / 2
        End If
    End If

End Sub

'------------------------------------------------------------------------------------------
'
' Respons�vel : Jo�o Mac-Cormick
' Dt Desenv : 03/06/2000
'
' tira preenchimento dos campos em um formul�rio
' outros controles podem ser adicionados
Public Sub LimpaCampos(Optional Formulario)
    Dim Controle    As Object

    Dim OFormulario As Form

    If IsMissing(Formulario) Then
        Set OFormulario = Screen.ActiveForm
    Else
        Set OFormulario = Formulario
    End If

    For Each Controle In OFormulario.Controls

        If TypeOf Controle Is ComboBox Then
            Controle.ListIndex = -1
        ElseIf TypeOf Controle Is ListBox Then
            Controle.ListIndex = -1
        ElseIf TypeOf Controle Is CheckBox Then
            Controle.Value = 0
        ElseIf TypeOf Controle Is OptionButton Then
            Controle.Value = False
        ElseIf TypeOf Controle Is TextBox Then
            Controle.Text = ""
        ElseIf TypeName(Controle) = "MaskEdBox" Then
            Controle.PromptInclude = False
            Controle.Text = ""
            Controle.PromptInclude = True
        End If

    Next

End Sub

'------------------------------------------------------------------------------------------
'
' Respons�vel : Jo�o Mac-Cormick
' Dt Desenv : 03/06/2000
'
' retorna o nome da tabela tempor�ria
Public Function TabTempUsr(Optional sUserName = "", Optional sMac = "") As String
    ' haver� supress�o do MAC se o nome do usu�rio for maior que 14
    ' a supress�o � necess�ria pois o MSSQL 6.5 aceita somente 30 caracteres
    Dim sNaoUsado  As String

    Dim sDominio   As String

    Dim oSABL0010  As Object

    Dim sLoginRede As String

    Set oSABL0010 = CreateObject("SABL0010.cls00009")
    sDominio = oSABL0010.RetornaValorAmbiente(gsSIGLASISTEMA, "Sistema", "Dominio", gsCHAVESISTEMA, glAmbiente_id)
    Call PegaInfoSistema(sNaoUsado, sNaoUsado, sNaoUsado, sNaoUsado, sNaoUsado, sLoginRede, sDominio)

    '  If sUserName = "" Then sUserName = cUserName
    If sUserName = "" Then sUserName = sLoginRede
    If sMac = "" Then sMac = gsMac
    TabTempUsr = Mid("##$" & sUserName & "$" + sMac, 1, 30)
End Function

'------------------------------------------------------------------------------------------
'
' Respons�vel : Jo�o Mac-Cormick
' Dt Desenv : 03/06/2000
'
' retorna a data e a hora do banco de dados MSSQL no ambiente
Public Function DataHoraAmbiente(Optional ByVal AForma As String = "dd/mm/yyyy hh:nn:ss", _
                                 Optional ByVal oSABL0010 As Variant) As Date
    Dim RX As Object

    If IsMissing(oSABL0010) Then
        Set oSABL0010 = CreateObject("SABL0010.cls00009")
    End If

    Set RX = oSABL0010.ExecutaAmbiente("SELECT CONVERT(DATETIME, GETDATE())")
    DataHoraAmbiente = Format(RX(0), AForma)
    RX.Close
End Function

'------------------------------------------------------------------------------------------
'
' Respons�vel : Jo�o Mac-Cormick
' Dt Desenv : 03/06/2000
'
' retorna os c�digos ASCII da cadeia separados por v�rgulas
Public Function ConvStringASCII(Cadeia As String) As String
    Dim Auxiliar As String

    Dim posicao  As Integer

    Auxiliar = ""

    For posicao = 1 To Len(Cadeia)
        Auxiliar = Auxiliar & "," & Asc(Mid(Cadeia, posicao, 1))
    Next posicao

    ConvStringASCII = Auxiliar
End Function

'------------------------------------------------------------------------------------------
'
' Respons�vel : Jo�o Mac-Cormick
' Dt Desenv : 03/06/2000
'
' retorna a cadeia original atrav�s dos c�digos ASCII que vieram separados por v�rgulas
Public Function ConvASCIIString(ByVal CadeiaASCII As String) As String
    Dim Auxiliar    As String

    Dim VetorNumero As Variant

    Dim ONumero     As Variant

    Auxiliar = ""
    VetorNumero = Split(Mid(CadeiaASCII, 2), ",")

    For Each ONumero In VetorNumero

        If IsNumeric(ONumero) Then
            If Val(ONumero) < 256 Then
                Inc Auxiliar, Chr(Val(ONumero))
            Else
                ConvASCIIString = ""
                Exit Function
            End If

        Else
            ConvASCIIString = ""
            Exit Function
        End If

    Next ONumero

    ConvASCIIString = Auxiliar
End Function

'------------------------------------------------------------------------------------------
'
' Respons�vel : Jo�o Mac-Cormick
' Dt Desenv : 03/06/2000
'
' retorna o ASCII do XOR entre uma letra e sua anterior na cadeia
Private Function FazXOR(Cadeia As String, Indice As Integer) As Integer

    If Indice > 1 Then
        FazXOR = Asc(Mid(Cadeia, Indice, 1)) Xor Asc(Mid(Cadeia, Indice - 1, 1))
    Else
        FazXOR = Asc(Mid(Cadeia, Indice, 1)) Xor Len(Cadeia)
    End If

End Function

'------------------------------------------------------------------------------------------
'
' Respons�vel : Jo�o Mac-Cormick
' Dt Desenv : 03/06/2000
'
' retorna a criptografia de uma cadeia
Private Function Criptografia(ByVal Cadeia As String, _
                              Optional ByVal Chave As String, _
                              Optional ByVal Passo As Integer = 1) As String

    If Passo = 0 Then
        Passo = 1
    End If

    Dim Inicio As Integer

    Dim Fim    As Integer

    Inicio = Se(Passo > 0, 1, Len(Cadeia))
    Fim = Se(Passo > 0, Len(Cadeia), 1)
    Dim Indice As Integer

    If Chave = "" Then

        For Indice = Inicio To Fim Step Passo
            Mid(Cadeia, Indice) = Chr(FazXOR(Cadeia, Indice))
        Next Indice

    Else
        Dim posicao

        For Indice = Inicio To Fim Step Passo
            posicao = ((Indice - 1) Mod Len(Chave)) + 1
            Mid(Cadeia, Indice) = Chr(FazXOR(Cadeia, Indice) Xor Asc(Mid(Chave, posicao, 1)))
        Next Indice

    End If

    Criptografia = Cadeia
End Function

'------------------------------------------------------------------------------------------
'
' Respons�vel : Jo�o Mac-Cormick
' Dt Desenv : 03/06/2000
'
' retorna uma cadeia cifrada por uma chave (opcional)
Public Function Cifra(ByVal Cadeia As String, Optional ByVal Chave As String) As String
    Cifra = Criptografia(Cadeia, Chave)
End Function

'------------------------------------------------------------------------------------------
'
' Respons�vel : Jo�o Mac-Cormick
' Dt Desenv : 03/06/2000
'
' retorna uma cadeia decifrada por uma chave (opcional)
Public Function Decifra(Cadeia As String, Optional Chave As String) As String
    Decifra = Criptografia(Cadeia, Chave, -1)
End Function

'------------------------------------------------------------------------------------------
'
' Respons�vel : Jo�o Mac-Cormick
' Dt Desenv : 03/06/2000
'
' retorna a cadeia criptografada por uma chave; se aparece aspas, troca para @r
Public Function Crypto(Pass As String, uID As String) As String

    If Pass = "" Or uID = "" Then
        Crypto = ""
        Exit Function
    End If

    Dim Key As String

    Dim i   As Integer

    Key = ""

    If Len(Pass) > Len(uID) Then

        For i = 1 To Len(uID)
            Inc Key, Chr((Asc(Mid(Pass, i, 1)) + Asc(Mid(uID, i, 1))) Mod 255)
        Next i

        Inc Key, Right(Pass, Len(Pass) - (i - 1))
    Else

        For i = 1 To Len(Pass)
            Inc Key, Chr((Asc(Mid(Pass, i, 1)) + Asc(Mid(uID, i, 1))) Mod 255)
        Next i

        Inc Key, Right(uID, Len(Pass) - (i - 1))
    End If

    Dim aux As String

    aux = Chr(Asc(Left(Pass, 1)) Xor Asc(Left(Key, 1)))

    If aux = "'" Then aux = "�"

    For i = 2 To Len(Pass)

        If Mid(Pass, i, 1) <> Mid(Key, i, 1) Then
            Dim temp As String

            temp = Chr((Asc(Mid(Pass, i, 1)) Xor Asc(Mid(Key, i, 1))))

            If temp <> Right(aux, 1) Then
                Inc aux, Chr(Asc(temp) Xor Asc(Right(aux, 1)))
            Else
                Inc aux, temp
            End If

        Else

            If Mid(Pass, i, 1) <> Right(aux, 1) Then
                Inc aux, Chr(Asc(Mid(Pass, i, 1)) Xor Asc(Right(aux, 1)))
            End If
        End If

        If Right(aux, 1) = "'" Then
            aux = Left(aux, Len(aux) - 1) & "�"
        End If

    Next i

    If Len(aux) = 0 Then
        MsgBox "A senha n�o pode ser vazia.", vbExclamation, "Seguran�a"
        Crypto = ""
    Else
        Crypto = aux
    End If

End Function

Function LinhaEncrypt(ByVal Str As String, ByVal Inicio As Integer) As String
    Dim aux     As String

    Dim posicao As Integer

    aux = Mid$(Str, Inicio)
    posicao = InStr(aux, Chr(13))

    If posicao > 2 Then
        aux = Mid$(aux, 1, posicao - 1)
    Else
        aux = ""
    End If

    If aux = Chr(10) Or aux = Chr(13) Then
        LinhaEncrypt = ""
    Else
        LinhaEncrypt = aux
    End If

End Function

Public Function Decriptar(FileName As String) As String
    ReDim aPassword(0) As Byte
    ReDim aCryptBuffer(0) As Byte
    Dim lCryptLength   As Long

    Dim lCryptBufLen   As Long

    Dim lCryptPoint    As Long

    Dim lPasswordPoint As Long

    Dim lPasswordCount As Long

    Dim CALG_MD5       As Long

    Dim calg_rc4       As Long

    Dim arquivo1, arquivo2 As Integer

    Dim tempname  As String

    Dim outbuffer As String

    '    On Local Error GoTo ErrDecrypt
    arquivo1 = FreeFile
    Open FileName For Binary Access Read As arquivo1
    lCryptLength = 360
    lCryptBufLen = lCryptLength + 16
    Dim i                As Integer

    Dim buffer           As String * 1

    Dim minhaLocalizacao As Long

    i = 1
    minhaLocalizacao = Loc(arquivo1)

    While minhaLocalizacao < LOF(arquivo1)
        ReDim aCryptBuffer(lCryptBufLen)
        lCryptPoint = 1

        While lCryptPoint <= lCryptLength And minhaLocalizacao < LOF(arquivo1)
            buffer = Input(1, #arquivo1)
            aCryptBuffer(lCryptPoint - 1) = Asc(buffer)
            lCryptPoint = lCryptPoint + 1
            minhaLocalizacao = Loc(arquivo1)
        Wend 'Decrypt data

        Dim fimArquivo As Long

        If minhaLocalizacao < LOF(arquivo1) Then
            fimArquivo = 1
        Else
            fimArquivo = 0
        End If

        lCryptPoint = 0

        While lCryptPoint <= lCryptBufLen
            ' mvarOutBuffer = mvarOutBuffer & Chr$(aCryptBuffer(lCryptPoint))
            outbuffer = outbuffer & Chr$(8 Xor aCryptBuffer(lCryptPoint))
            ' Put #arquivo2, , Chr$(aCryptBuffer(lCryptPoint))
            lCryptPoint = lCryptPoint + 1
        Wend

    Wend

    Close #arquivo1
    Decriptar = outbuffer
    Exit Function
    'ErrDecrypt:
    '    MsgBox ("ErrDecrypt " & Error$)
End Function

Public Sub Encriptar(FileName As String)
    Dim CALG_MD5, calg_rc4

    Dim BNULL As Byte

    BNULL = 0&
    ReDim aCryptBuffer(0) As Byte
    Dim lCryptLength   As Long

    Dim lCryptBufLen   As Long

    Dim lCryptPoint    As Long

    Dim lPasswordPoint As Long

    Dim lPasswordCount As Long

    Dim arquivo1, arquivo2 As Integer

    arquivo1 = FreeFile
    Open FileName For Binary Access Read As arquivo1
    Dim tempname As String

    arquivo2 = FreeFile
    tempname = Left(FileName, InStr(FileName, ".")) & "crp"
    Open tempname For Binary Access Write As arquivo2
    CALG_MD5 = 32771
    calg_rc4 = 26625
    '    On Local Error GoTo ErrEncrypt
    'Switch Status property.lStatus = CFB_BUSY
    'Get handle to the default provider.sContainer = Chr$(0)sProvider = Chr$(0)
    Dim i                As Integer

    Dim buffer           As String * 1

    Dim bufferAux        As String

    Dim minhaLocalizacao As Long

    i = 1
    lCryptLength = 360
    lCryptBufLen = lCryptLength + 16
    minhaLocalizacao = Loc(arquivo1)

    While minhaLocalizacao < LOF(arquivo1)
        ReDim aCryptBuffer(lCryptBufLen)
        lCryptPoint = 1

        'enche o buffer
        While lCryptPoint <= lCryptLength And minhaLocalizacao < LOF(arquivo1)
            buffer = Input(1, #arquivo1)
            bufferAux = bufferAux & buffer
            aCryptBuffer(lCryptPoint - 1) = Asc(buffer)
            lCryptPoint = lCryptPoint + 1
            minhaLocalizacao = Loc(arquivo1)
        Wend

        Dim fimArquivo As Long

        If minhaLocalizacao < LOF(arquivo1) Then
            fimArquivo = 1
        Else
            fimArquivo = 0
        End If

        'Encrypt data
        For i = 0 To lCryptLength
            aCryptBuffer(i) = aCryptBuffer(i) Xor 8
        Next

        lCryptPoint = 0
        Dim buffer_aux_out As String

        While lCryptPoint <= lCryptBufLen

            If aCryptBuffer(lCryptPoint) = 0 And aCryptBuffer(lCryptPoint + 1) = 0 Then
                lCryptPoint = lCryptBufLen
            Else
                '     mvarOutBuffer = mvarOutBuffer & Chr$(aCryptBuffer(lCryptPoint))
                buffer_aux_out = buffer_aux_out & Chr(aCryptBuffer(lCryptPoint))
                Put #arquivo2, , aCryptBuffer(lCryptPoint)
            End If

            lCryptPoint = lCryptPoint + 1
        Wend

    Wend

    Close arquivo1
    Close arquivo2
    Kill FileName
    Name tempname As FileName
    Exit Sub
    'ErrEncrypt:
    '    MsgBox ("ErrEncrypt " & Error$)
End Sub

Function VerificaWinIni() As Boolean
    Dim sDate, sshortdate, sDecimal, sThousand

    '
    ' Verifica formato de data e n�mero do Windows
    '
    sDate = LeArquivoIni("WIN.INI", "intl", "sDate")
    sshortdate = LeArquivoIni("WIN.INI", "intl", "sShortDate")
    sDecimal = LeArquivoIni("WIN.INI", "intl", "sDecimal")
    sThousand = LeArquivoIni("WIN.INI", "intl", "sThousand")
    VerificaWinIni = True

    If sshortdate <> "dd/MM/aaaa" And sshortdate <> "dd/MM/aa" And sshortdate <> "dd/MM/yyyy" And sshortdate <> "dd/MM/yy" Then
        VerificaWinIni = False
    End If

    If sDecimal <> "," Then VerificaWinIni = False
    If sThousand <> "." Then VerificaWinIni = False
End Function

Function LeArquivoIni(ByVal FileName As String, _
                      ByVal SectionName As String, _
                      ByVal Item As String) As String
    Dim Retorno As String * 100, RetornoDefault, nc

    RetornoDefault = "*"
    nc = GetPrivateProfileString(SectionName, Item, RetornoDefault, Retorno, Len(Retorno), FileName)
    LeArquivoIni = Left$(Retorno, nc)
End Function

Function LerArquivoIni(ByVal Secao As String, _
                       ByVal Item As String, _
                       Optional ByVal oSABL0010 As Variant) As String

    If IsMissing(oSABL0010) Then
        Set oSABL0010 = CreateObject("SABL0010.cls00009")
    End If

    LerArquivoIni = oSABL0010.RetornaValorAmbiente(gsSIGLASISTEMA, Secao, Item, gsCHAVESISTEMA, glAmbiente_id)
End Function

Public Sub LimpaObjetosComuns()
    Dim i  As Integer

    Dim I2 As Integer

    For i = 0 To Screen.ActiveForm.Controls.Count - 1

        Select Case Trim(TypeName(Screen.ActiveForm.Controls(i)))
            Case "TextBox"
                Screen.ActiveForm.Controls(i).Text = ""
            Case "MaskEdBox"

                If Screen.ActiveForm.Controls(i).Mask = "##/##/####" Then
                    Screen.ActiveForm.Controls(i) = "__/__/____"
                End If

            Case "ComboBox"
                Screen.ActiveForm.Controls(i).ListIndex = -1
            Case "OptionButton"
                Screen.ActiveForm.Controls(i).Value = False
            Case "CheckBox"
                Screen.ActiveForm.Controls(i).Value = 0
        End Select

    Next i

End Sub

Sub inicializa_variaveis()
    Dim gLinha As String, PATH, arqinput

    '    On Local Error GoTo errorHandler
    PATH = LerArquivoIni("PRODUCAO", "PRODUCAO_PATH")
    arqinput = FreeFile
    Open PATH & "..\config\configura.ini" For Input As arqinput
    Line Input #arqinput, gLinha

    Do While Not EOF(arqinput)

        Select Case gLinha
            Case "[SISTEMA]"
                Line Input #arqinput, SIS_server
                SIS_server = Mid(SIS_server, InStr(SIS_server, "=") + 1)
                Line Input #arqinput, SIS_banco
                SIS_banco = Mid(SIS_banco, InStr(SIS_banco, "=") + 1)
                Line Input #arqinput, SIS_usuario
                SIS_usuario = Mid(SIS_usuario, InStr(SIS_usuario, "=") + 1)
                Line Input #arqinput, SIS_senha
                SIS_senha = Mid(SIS_senha, InStr(SIS_senha, "=") + 1)
            Case "[INTRANET]"
                Line Input #arqinput, INT_server
                INT_server = Mid(INT_server, InStr(INT_server, "=") + 1)
                Line Input #arqinput, INT_banco
                INT_banco = Mid(INT_banco, InStr(INT_banco, "=") + 1)
                Line Input #arqinput, INT_usuario
                INT_usuario = Mid(INT_usuario, InStr(INT_usuario, "=") + 1)
                Line Input #arqinput, INT_senha
                INT_senha = Mid(INT_senha, InStr(INT_senha, "=") + 1)
            Case "[STORED PROCEDURES]"
                Line Input #arqinput, SP_usuario
                SP_usuario = Mid(SP_usuario, InStr(SP_usuario, "=") + 1)
        End Select

        Line Input #arqinput, gLinha
    Loop

    Close #arqinput
    'errorHandler:
    '   MsgBox "ERRO ao abrir arquivo"
End Sub

Sub inicializa_variaveis_seguro()
    Dim PATH As String

    PATH = LerArquivoIni("PRODUCAO", "PRODUCAO_PATH")
    Dim Resultado, FileName As String

    Dim gLinhas(12) As String

    Dim i           As Integer

    Dim posicao     As Integer

    FileName = PATH & "..\config\config.ini"

    '  FileName = "\\brcapfs\sisbr\config\config.ini"
    If Dir(FileName) <> "" Then
        'Open filename For Input As arqinput
        Resultado = Decriptar(FileName)
        gLinhas(0) = LinhaEncrypt(Resultado, 1)

        For i = 1 To 11
            posicao = posicao + Len(gLinhas(i - 1)) + 2
            gLinhas(i) = LinhaEncrypt(Resultado, posicao)
        Next

        SIS_server = gLinhas(1)
        SIS_server = Mid(SIS_server, InStr(SIS_server, "=") + 1)
        SIS_banco = gLinhas(2)
        SIS_banco = Mid(SIS_banco, InStr(SIS_banco, "=") + 1)
        SIS_usuario = gLinhas(3)
        SIS_usuario = Mid(SIS_usuario, InStr(SIS_usuario, "=") + 1)
        SIS_senha = gLinhas(4)
        SIS_senha = Mid(SIS_senha, InStr(SIS_senha, "=") + 1)
        INT_server = gLinhas(6)
        INT_server = Mid(INT_server, InStr(INT_server, "=") + 1)
        INT_banco = gLinhas(7)
        INT_banco = Mid(INT_banco, InStr(INT_banco, "=") + 1)
        INT_usuario = gLinhas(8)
        INT_usuario = Mid(INT_usuario, InStr(INT_usuario, "=") + 1)
        INT_senha = gLinhas(9)
        INT_senha = Mid(INT_senha, InStr(INT_senha, "=") + 1)
        SP_usuario = gLinhas(11)
        SP_usuario = Mid(SP_usuario, InStr(SP_usuario, "=") + 1)
    End If

End Sub

Sub inicializa_variaveis_seguro_c_bckp()
    Dim PATH As String

    PATH = LerArquivoIni("PRODUCAO", "PRODUCAO_PATH")
    Dim Resultado, FileName As String

    Dim gLinhas(15) As String

    Dim i           As Integer

    Dim posicao     As Integer

    FileName = PATH & "..\config\config.ini"

    '  FileName = "c:\windows\system\config.ini"
    If Dir(FileName) <> "" Then
        '  Open filename For Input As arqinput
        Resultado = Decriptar(FileName)
        gLinhas(0) = LinhaEncrypt(Resultado, 1)

        For i = 1 To 14
            posicao = posicao + Len(gLinhas(i - 1)) + 2
            gLinhas(i) = LinhaEncrypt(Resultado, posicao)
        Next

        SIS_server = gLinhas(1)
        SIS_server = Mid(SIS_server, InStr(SIS_server, "=") + 1)
        SIS_banco = gLinhas(2)
        SIS_banco = Mid(SIS_banco, InStr(SIS_banco, "=") + 1)
        SIS_usuario = gLinhas(3)
        SIS_usuario = Mid(SIS_usuario, InStr(SIS_usuario, "=") + 1)
        SIS_senha = gLinhas(4)
        SIS_senha = Mid(SIS_senha, InStr(SIS_senha, "=") + 1)
        INT_server = gLinhas(6)
        INT_server = Mid(INT_server, InStr(INT_server, "=") + 1)
        INT_banco = gLinhas(7)
        INT_banco = Mid(INT_banco, InStr(INT_banco, "=") + 1)
        INT_usuario = gLinhas(8)
        INT_usuario = Mid(INT_usuario, InStr(INT_usuario, "=") + 1)
        INT_senha = gLinhas(9)
        INT_senha = Mid(INT_senha, InStr(INT_senha, "=") + 1)
        SP_usuario = gLinhas(11)
        SP_usuario = Mid(SP_usuario, InStr(SP_usuario, "=") + 1)
        BKP_usuario = gLinhas(13)
        BKP_usuario = Mid(BKP_usuario, InStr(BKP_usuario, "=") + 1)
        BKP_senha = gLinhas(14)
        BKP_senha = Mid(BKP_senha, InStr(BKP_senha, "=") + 1)
    End If

End Sub

Sub mensagem_erro(ByVal op As Integer, ByVal Campo As String)
    Campo = UCase(Campo)

    Select Case op
        Case 1
            MsgBox "Item " & Campo & " j� cadastrado!", vbCritical, Screen.ActiveForm.Caption
        Case 2
            MsgBox "Item " & Campo & " n�o selecionado!", vbCritical, Screen.ActiveForm.Caption
        Case 3
            MsgBox "Campo " & Campo & " inv�lido ou n�o informado!", vbCritical, Screen.ActiveForm.Caption
        Case 4
            MsgBox "Existem valores em " & Campo & " relacionados", vbCritical, Screen.ActiveForm.Caption
        Case 5
            MsgBox "Opera��o inv�lida para datas retroativas em " & Campo, vbCritical, Screen.ActiveForm.Caption
        Case 6
            MsgBox Campo, vbCritical, Screen.ActiveForm.Caption
        Case 7
            MsgBox "Usu�rio " & UCase(cUserName) & " n�o tem acesso � fun��o " & UCase(Campo), vbCritical, Screen.ActiveForm.Caption
    End Select

End Sub

Function MudaVirgulaParaPonto(ByVal valor As String) As String

    If InStr(valor, ",") = 0 Then
        MudaVirgulaParaPonto = valor
    Else
        valor = Mid$(valor, 1, InStr(valor, ",") - 1) + "." + Mid$(valor, InStr(valor, ",") + 1)
        MudaVirgulaParaPonto = valor
    End If

End Function

Function CGCModulo11(ByVal numero As String) As String
    Dim D1, D2, Digito_Verificador

    Dim Ponteiro_Numero As Integer

    Dim Multiplicador   As Integer

    Dim Somatoria       As Integer

    Dim resto           As Integer

    numero = Right(String(13, "*") + Trim(numero), 13)
    Ponteiro_Numero = 13
    Multiplicador = 2
    Somatoria = 0

    While Ponteiro_Numero > 0 And Mid$(numero, Ponteiro_Numero, 1) <> "*"
        Somatoria = Somatoria + (Val(Mid$(numero, Ponteiro_Numero, 1)) * Multiplicador)
        Ponteiro_Numero = Ponteiro_Numero - 1
        Multiplicador = Multiplicador + 1

        If Multiplicador = 10 Then
            Multiplicador = 2
        End If

    Wend

    resto = Somatoria Mod 11
    D1 = 11 - resto

    If D1 = 10 Or D1 = 11 Or D1 = 0 Then
        D1 = 0
    End If

    numero = numero & Trim(Str(D1))
    Ponteiro_Numero = 14
    Multiplicador = 2
    Somatoria = 0

    While Ponteiro_Numero > 0 And Mid$(numero, Ponteiro_Numero, 1) <> "*"
        Somatoria = Somatoria + (Val(Mid$(numero, Ponteiro_Numero, 1)) * Multiplicador)
        Ponteiro_Numero = Ponteiro_Numero - 1
        Multiplicador = Multiplicador + 1

        If Multiplicador = 10 Then
            Multiplicador = 2
        End If

    Wend

    resto = Somatoria Mod 11
    D2 = 11 - resto

    If D2 = 10 Or D2 = 11 Or D2 = 0 Then
        D2 = 0
    End If

    Digito_Verificador = (D1 * 10) + D2
    CGCModulo11 = Format(Digito_Verificador, "00")
End Function

Function ultimaString(longstring, target)
    Dim position, Count

    position = 1

    Do While InStr(position, longstring, target)
        position = InStr(position, longstring, target) + 1
    Loop

    ultimaString = position - 1
End Function

Function MudaAspaSimples(ByVal valor As String) As String

    Do Until InStr(valor, "'") = 0
        valor = Mid$(valor, 1, InStr(valor, "'") - 1) + "�" + Mid$(valor, InStr(valor, "'") + 1)
    Loop

    MudaAspaSimples = UCase(valor)
End Function

Function montaMascaraNumerica(ByVal valor As String, _
                              ByVal posicaoVirgula As Integer) As String
    Dim aux, i, j

    aux = posicaoVirgula

    If InStr(valor, "_") = 0 Then
        montaMascaraNumerica = valor
        Exit Function
    End If

    While InStr(valor, "_") <> 0

        If InStr(valor, "_") < aux And InStr(valor, ",") <> 1 Then
            valor = Mid$(valor, 1, InStr(valor, "_") - 1) + "" + Mid$(valor, InStr(valor, "_") + 1)
            aux = aux - 1
        Else
            valor = Mid$(valor, 1, InStr(valor, "_") - 1) + "0" + Mid$(valor, InStr(valor, "_") + 1)
        End If

    Wend

    If InStr(valor, ",") < posicaoVirgula Then
        i = InStr(valor, ",")

        For j = i To posicaoVirgula - 1
            valor = "0" & valor
        Next j

    End If

    montaMascaraNumerica = valor
End Function

Public Sub InicializaVariaveisFTP()
    Dim PATH As String

    PATH = LerArquivoIni("PRODUCAO", "PRODUCAO_PATH")
    Dim Resultado, FileName As String

    Dim gLinhas(19) As String

    Dim i           As Integer

    Dim posicao     As Integer

    FileName = PATH & "..\config\config.ini"

    'FileName = "c:\windows\system\config.ini"
    If Dir(FileName) <> "" Then
        Resultado = Decriptar(FileName)
        gLinhas(0) = LinhaEncrypt(Resultado, 1)

        For i = 1 To 18
            posicao = posicao + Len(gLinhas(i - 1)) + 2
            gLinhas(i) = LinhaEncrypt(Resultado, posicao)
        Next

        ftpServidorSISE = gLinhas(16)
        ftpUsuarioServidorSISE = gLinhas(17)
        ftpSenhaServidorSISE = gLinhas(18)
        ftpServidorSISE = Mid(ftpServidorSISE, InStr(ftpServidorSISE, "=") + 1)
        ftpUsuarioServidorSISE = Mid(ftpUsuarioServidorSISE, InStr(ftpUsuarioServidorSISE, "=") + 1)
        ftpSenhaServidorSISE = Mid(ftpSenhaServidorSISE, InStr(ftpSenhaServidorSISE, "=") + 1)
    End If

End Sub

Public Function Valida_Parametros(sParametro As String) As Boolean
    ' valida os par�metros passados, para verificar se foi chamado do menu ou n�o
    Valida_Parametros = False
    On Local Error GoTo TrataErro
    Dim vValoresPassados As Variant

    Dim lErro            As Long

    Dim sHostName        As String

    Dim sIp              As String

    gsParamAplicacao = Left(sParametro, InStrRev(sParametro, " "))
    ' pega ap�s o �ltimo espa�o em branco, onde est� a cadeia de criptografia
    sParametro = Mid(sParametro, Len(gsParamAplicacao) + 1)
    gsParamAplicacao = Trim(gsParamAplicacao)
    'sParametro = Right(sParametro, Len(sParametro) - InStrRev(sParametro, " "))
    ' criptografa 2 vezes, um com chave e depois volta de Ascii para String
    sParametro = ConvASCIIString(sParametro)
    sParametro = Decifra(sParametro, gsCHAVESISTEMA)
    sParametro = Decifra(sParametro)
    'pega os par�metros passados
    vValoresPassados = Split(sParametro, "|")
    gsDHEntrada = DataHoraAmbiente

    'Verifica se o Computador, Mac, Ip, Usu�rio s�o v�lidos e
    'se a data est� dentro de 5 minutos
    If UBound(vValoresPassados) > 4 Then
        gsCodAplicacao = vValoresPassados(4)
        glAmbiente_id = CLng(Trim(vValoresPassados(6)))
        glEmpresa_id = CLng(Trim(vValoresPassados(7)))
        gsCPF = CStr(vValoresPassados(8))

        If cUserName <> CStr(vValoresPassados(0)) Then
            gsMensagem = "N�o validou nome da rede " & cUserName & " com o par�metro " & CStr(vValoresPassados(0))
            Call GravaMensagem(gsMensagem, "proc", "TrataParametros", "modGlobais")
            Exit Function
        End If

        If gsHost <> CStr(vValoresPassados(1)) Then
            gsMensagem = "N�o validou nome do computador " & gsHost & " com o par�metro " & CStr(vValoresPassados(1))
            Call GravaMensagem(gsMensagem, "proc", "TrataParametros", "modGlobais")
            Exit Function
        End If

        If gsIp <> CStr(vValoresPassados(3)) Then
            gsMensagem = "N�o validou IP " & gsIp & " com o par�metro " & CStr(vValoresPassados(3))
            Call GravaMensagem(gsMensagem, "proc", "TrataParametros", "modGlobais")
            Exit Function
        End If

        If gsMac <> CStr(vValoresPassados(2)) Then
            gsMensagem = "N�o validou MAC " & gsMac & " com o par�metro " & CStr(vValoresPassados(2))
            Call GravaMensagem(gsMensagem, "proc", "TrataParametros", "modGlobais")
            Exit Function
        End If

        Dim Diferenca As Long

        Diferenca = DateDiff("n", CDate(vValoresPassados(5)), gsDHEntrada)

        If Diferenca > 5 Then
            gsMensagem = "N�o validou DHEntrada " & gsDHEntrada & " com o par�metro " & CStr(vValoresPassados(5))
            Call GravaMensagem(gsMensagem, "proc", "TrataParametros", "modGlobais")
            Exit Function
        End If

        'TROCA O USERNAME
        cUserName = gsCPF
        Valida_Parametros = True
    End If

TrataErro:
End Function

Function Trata_Parametros(sParametro As String) As Boolean
    Dim vValoresPassados As Variant

    Dim lErro            As Long

    Dim sHostName        As String

    Dim sIp              As String

    Dim Diferenca        As Long

    On Local Error GoTo TrataErro
    Trata_Parametros = False
    gsParamAplicacao = Left(sParametro, InStrRev(sParametro, " "))
    ' pega ap�s o �ltimo espa�o em branco, onde est� a cadeia de criptografia
    sParametro = Mid(sParametro, Len(gsParamAplicacao) + 1)
    gsParamAplicacao = Trim(gsParamAplicacao)
    'sParametro = Right(sParametro, Len(sParametro) - InStrRev(sParametro, " "))
    ' criptografa 2 vezes, um com chave e depois volta de Ascii para String
    sParametro = ConvASCIIString(sParametro)
    sParametro = Decifra(sParametro, gsCHAVESISTEMA)
    sParametro = Decifra(sParametro)
    'pega os par�metros passados
    vValoresPassados = Split(sParametro, "|")
    gsDHEntrada = DataHoraAmbiente

    'Verifica se o Computador, Mac, Ip, Usu�rio s�o v�lidos e
    'se a data est� dentro de 5 minutos
    If UBound(vValoresPassados) > 2 Then
        gsCodAplicacao = vValoresPassados(0)
        glAmbiente_id = CLng(Trim(vValoresPassados(1)))
        glEmpresa_id = CLng(Trim(vValoresPassados(2)))
        gsCPF = CStr(vValoresPassados(3))

        If Len(Trim(gsCPF)) = 0 Then
            Call GravaMensagem("N�o passou o CPF.", "exec", "trata_parametros", "Funcoes.bas")
            Exit Function
        End If

        'TROCA O USERNAME
        cUserName = gsCPF
        Trata_Parametros = True
    End If

TrataErro:
End Function

Public Function Monta_Parametros(ByVal OCodPrograma As String) As String
    Dim sParametro As String

    gsDHEntrada = DataHoraAmbiente
    ' agrupa os dados para a criptografia (Usu�rio, Computador, MAC, IP, Id_prog, Data e Hora)
    sParametro = OCodPrograma & "|" & glAmbiente_id & "|" & glEmpresa_id & "|" & gsCPF
    'Criptografa 2 vezes, um com chave, e coloca os valores em Ascii separados por virgulas
    sParametro = Cifra(sParametro)
    sParametro = Cifra(sParametro, gsCHAVESISTEMA)
    sParametro = ConvStringASCII(sParametro)
    Monta_Parametros = sParametro
    Exit Function
End Function

Sub Coloca_Efeito_3D(Controle As Object, _
                     sTexto As String, _
                     Xpos As Single, _
                     Ypos As Single, _
                     lCor As Long, _
                     Optional lTamanhoFont As Long = 0, _
                     Optional lCorDeFundo As Long = 0)
    'Coloca Fundo 3D no Controle Passado (Form, PictureBox) e quarda o valor Texto na Tag
    'OBS.: N�o s�o todos os controles que aceitam esta rotina
    'Raphael Luiz Gagliardi     Data: 22/02/2001
    Dim StartColor As Long

    Dim OldDWVal   As Long

    Const WHITE = &HFFFFFF

    Const GRAY = &H808080

    If lTamanhoFont > 0 Then
        Controle.FontSize = lTamanhoFont
    End If

    If lCorDeFundo > 0 Then
        Controle.BackColor = lCorDeFundo
    End If

    Controle.Tag = sTexto
    OldDWVal = Controle.DrawWidth
    Controle.AutoRedraw = True
    StartColor = Controle.ForeColor
    Controle.CurrentX = Xpos - 15
    Controle.CurrentY = Ypos - 15
    Controle.ForeColor = WHITE
    Controle.Print sTexto
    Controle.CurrentX = Xpos + 15
    Controle.CurrentY = Ypos + 15
    Controle.ForeColor = GRAY
    Controle.Print sTexto
    Controle.CurrentX = Xpos
    Controle.CurrentY = Ypos
    Controle.ForeColor = lCor
    Controle.Print sTexto
    Controle.ForeColor = StartColor
    Controle.DrawWidth = OldDWVal
End Sub     ' RaisedFrmText()

Function Obtem_agenda_diaria_id(OParametro As String) As Long
    'Raphael Luiz Gagliardi 22/05/2001
    'Obt�m agenda_diaria_id a partir do par�metro passado
    'Se retornar 0 --> Manual
    Dim lposicao1 As Long

    Dim lposicao2 As Long

    lposicao1 = InStr(1, OParametro, "||")
    lposicao2 = InStr(lposicao1 + 1, OParametro, "||")

    If lposicao1 > 0 And lposicao2 > 0 Then
        Obtem_agenda_diaria_id = Mid(OParametro, lposicao1 + 2, lposicao2 - lposicao1 - 2)
    Else
        Obtem_agenda_diaria_id = 0
    End If

End Function

Function GravaMensagem(ByVal PMensagem As String, _
                       ByVal PTipo As String, _
                       Optional ByVal PRotina As String = "", _
                       Optional ByVal PArquivo As String = "")
    On Local Error GoTo TrataErro
    Dim Erro As Object

    Set Erro = CreateObject("Erro.Log")
    Erro.Usuario = IIf(Trim(cUserName) = "", gsIp & "~" & gsHost, cUserName)
    Erro.NmProjeto = gsSIGLASISTEMA
    Erro.NmAplicacao = App.EXEName
    Erro.NmForm = PArquivo
    'Erro.NmArquivo = PArquivo
    Erro.NmRotina = PRotina

    If goProducao Is Nothing Then
        Call Erro.Grava_Log_Erro(0, PMensagem, PTipo, Obtem_agenda_diaria_id(gsParamAplicacao))
    Else
        Call Erro.Grava_Log_Erro(0, PMensagem, PTipo, goProducao.agenda_diaria_id)
    End If

TrataErro:
    ' se houver erro, sai da rotina
End Function

Public Function MontaListView(ByRef lRec As Object, _
                              ByRef LView As Object, _
                              Optional ByRef bGridLines As Boolean = False, _
                              Optional ByRef bCheckBoxes As Boolean = False) As Boolean
    'Funcao p/ montar listview a partir de um rdoresultser, utilizando os nomes dos
    'atributos como cabechalhos, e podendo optar por ter ou nao checkbox e linhas de grid
    'joconceicao - 28-jun-01
    ' esta funcao n�o deve ser chamada antes do load do form completo
    Dim Itm As Object, Colunas As Long, TamCol() As Long, Cor As String

    Cor = "vbBlack"
    Colunas = lRec.rdoColumns.Count - 1
    ReDim TamCol(0 To Colunas)

    For Colunas = 0 To UBound(TamCol)
        TamCol(Colunas) = 0
    Next Colunas

    With LView
        .Visible = False
        .Gridlines = bGridLines
        .CheckBoxes = bCheckBoxes
        .FullRowSelect = True
        .View = 3                 'lvwReport
        .LabelEdit = 1            ' lvwManual
        .ColumnHeaders.Clear

        For Colunas = 0 To lRec.rdoColumns.Count - 1

            If UCase(CStr(lRec(Colunas).name)) <> "COR" Then
                .ColumnHeaders.Add , , lRec(CLng(Colunas)).name
            Else
                .ColumnHeaders.Add , , ""
            End If

        Next Colunas

        .ListItems.Clear

        If lRec.EOF Then GoTo MontaListView_FIM

        Do

            If lRec(0).Type = 11 Then
                Set Itm = .ListItems.Add(, , Format(lRec(0), "dd/mm/yyyy"))
            Else
                Set Itm = .ListItems.Add(, , lRec(0))
            End If

            If TamCol(0) < Screen.ActiveForm.TextWidth(CStr(.ListItems.Item(.ListItems.Count))) Then
                TamCol(0) = Screen.ActiveForm.TextWidth(.ListItems.Item(.ListItems.Count))
            End If

            For Colunas = 1 To lRec.rdoColumns.Count - 1

                If IsNull(lRec(Colunas)) Then
                    Itm.SubItems(Colunas) = " "
                ElseIf lRec(Colunas).Type = 11 Then

                    If lRec(Colunas).Value = Null Then
                        Itm.SubItems(Colunas) = ""
                    Else
                        Itm.SubItems(Colunas) = Format(lRec(Colunas), "dd/mm/yyyy")
                    End If

                ElseIf InStr(UCase(CStr(lRec(Colunas).name)), "CPF") Or InStr(UCase(CStr(lRec(Colunas).name)), "CNPJ") Or InStr(UCase(CStr(lRec(Colunas).name)), "CGC") Then

                    If Len(Trim(lRec(Colunas).Value)) = 11 Then
                        Itm.SubItems(Colunas) = MasCPF(lRec(Colunas))
                    ElseIf Len(Trim(lRec(Colunas).Value)) = 14 Then
                        Itm.SubItems(Colunas) = MasCGC(lRec(Colunas))
                    End If

                ElseIf InStr(UCase(CStr(lRec(Colunas).name)), "CEP") Or InStr(UCase(CStr(lRec(Colunas).name)), "C.E.P") Then
                    Itm.SubItems(Colunas) = MasCEP(lRec(Colunas))
                ElseIf UCase(CStr(lRec(Colunas).name)) = "COR" Then
                    Cor = lRec(Colunas)
                Else
                    Itm.SubItems(Colunas) = Trim(lRec(Colunas)) & " "
                End If

                If TamCol(Colunas) < Screen.ActiveForm.TextWidth(Trim(.ListItems.Item(.ListItems.Count).SubItems(Colunas))) Then
                    TamCol(Colunas) = Screen.ActiveForm.TextWidth(Trim(.ListItems.Item(.ListItems.Count).SubItems(Colunas)))
                End If

            Next Colunas

            Call SetForecolorforListItem(Itm, ConvertEmCor(CStr(Cor)))
            lRec.MoveNext
        Loop Until lRec.EOF

        For Colunas = 1 To .ColumnHeaders.Count

            If TamCol(Colunas - 1) > 1000 Then
                .ColumnHeaders.Item(Colunas).Width = (TamCol(Colunas - 1) * 1.5)
            Else
                .ColumnHeaders.Item(Colunas).Width = 1500
            End If

        Next Colunas

MontaListView_FIM:
        .Visible = True
    End With

End Function

Public Function MontaComboBox(ByRef cCombo As Object, _
                              ByRef rRs As Object, _
                              Optional lPosicao As Variant) As Boolean
    ' joconceicao - 28-jun-2001
    ' lposicao inicializa o combo no parametro passado
    ' se o numero passado for maior que o combo, posiciona na ultima
    MontaComboBox = False
    Dim TemItemData As Boolean

    Dim Indice      As Single

    With cCombo
        .Clear

        If .style = 0 Then
            .Text = ""
        End If

        If rRs.EOF Then
            MsgBox "N�o h� informa��es para preencher o Combobox", vbInformation, ""
            Exit Function
        End If

        If rRs.rdoColumns.Count > 1 Then
            TemItemData = True
        Else
            TemItemData = False
        End If

        Do
            .AddItem rRs(0)

            If TemItemData Then .ItemData(.NewIndex) = rRs(1)
            rRs.MoveNext
        Loop Until rRs.EOF

        If Not IsMissing(lPosicao) Then
            If IsNumeric(lPosicao) Then
                If Not (Val(lPosicao) < .ListCount) Then
                    lPosicao = .ListCount - 1
                End If

                If lPosicao > -1 Then
                    .ListIndex = lPosicao
                End If

            ElseIf Trim(lPosicao) <> "" Then

                For Indice = 0 To .ListCount - 1

                    If UCase(Left$(.List(Indice), Len(lPosicao))) = UCase(lPosicao) Then
                        .ListIndex = Indice
                    End If

                Next Indice

            End If
        End If

    End With

End Function

Public Sub DesChecaListview(lListview As Object, lItem As Object)
    ' Joconceicao 13/Jul/01
    ' Monte sempre um unico item com check True
    ' Commando Call DesChecaListview(ListView1, Item)
    Dim Itens As Object

    With lListview

        For Each Itens In .ListItems

            If Itens <> lItem Then
                Itens.Checked = False
            End If

        Next Itens

    End With

End Sub

Public Sub SetForecolorforListItem(xListItem As Object, ColorIn As Long)
    ' joconceicao - 13/jul/01
    ' set a cor de uma linha num listview , basta passar
    '                                       xListitem as Listitem
    '                                       ColorIn --> cor a ser usada
    Dim xSubItem As Object

    On Error Resume Next

    With xListItem
        .ForeColor = ColorIn
        .Bold = True

        For Each xSubItem In .ListSubItems
            xSubItem.ForeColor = ColorIn
        Next

    End With

End Sub

Public Sub OrdenaListView(xListView As Object, xColuna As Long)

    ' joconceicao - 13-jul/2001
    ' ordena o listview basta incluir a chamada desta funcao no evento ColumnClick
    With xListView

        If .SortOrder = 0 Then
            .SortOrder = 1
        Else
            .SortOrder = 1
        End If

        .Sorted = True
        .SortKey = xColuna
    End With

End Sub
    
Public Function TiraCaracter(ByVal Str As String, Optional Caracter) As String

    If IsMissing(Caracter) Then
        Caracter = "|"
    End If

    While InStr(1, Str, Caracter) <> 0
        Str = Left(Str, InStr(1, Str, Caracter) - 1) & Right(Str, Len(Str) - InStr(1, Str, Caracter))
    Wend

    TiraCaracter = Str
End Function

Public Function SeEspacoNull(ByRef Variavel As Variant) As String

    If Len(Variavel) = 0 Then
        SeEspacoNull = "Null "
    Else
        SeEspacoNull = Variavel
    End If

End Function

Public Function EstadoValido(ByRef sEstados As String) As Boolean
    '----------------------------------------------------------------
    ' joconceicao - Mar 2002, 26 - consistir estado informado
    '
    Dim sEstadosValidos As String, Indice As Long

    sEstadosValidos = "ACALAMAPBACEDFESGOMAMGMSMTPAPBPEPIPRRJRNRORRRSSESPTOXXSCEX"

    For Indice = 1 To Len(sEstadosValidos) Step 2

        If UCase(sEstados) = Mid(sEstadosValidos, Indice, 2) Then
            EstadoValido = True
            Exit Function
        End If

    Next Indice

    EstadoValido = False
End Function

Public Function SeZeroNull(ByRef valor As String) As String

    If Val(valor) = 0 Then
        SeZeroNull = "Null "
    Else
        SeZeroNull = valor
    End If

End Function

Public Function ConvertEmCor(ByRef lCor As String) As Long

    Select Case LCase(lCor)
        Case "vbgreen"
            ConvertEmCor = 49152 ' 65280
        Case "vbblue"
            ConvertEmCor = 12582912
        Case "vbred"
            ConvertEmCor = 192
        Case "vbYellow"
            ConvertEmCor = 65280
        Case "vbWhite"
            ConvertEmCor = 16777215
        Case "vbBlack"
            ConvertEmCor = 0
    End Select

End Function

'------------------------------------------------------------------------------------------
'
' Respons�vel : Jo�o Mac-Cormick
' Dt Desenv : 03/06/2002
'
' esta rotina coloca uma coluna qualquer no tamanho 0
Public Sub EsconderColuna(GrdAGrade As Object, Optional ByRef lAColuna As Long = 0)

    If GrdAGrade.ColWidth(lAColuna) <> 0 Then GrdAGrade.ColWidth(lAColuna) = 0
End Sub

'------------------------------------------------------------------------------------------
'
' Respons�vel : Jo�o Mac-Cormick
' Dt Desenv : 03/06/2002
'
' esta fun��o retorna a linha da grade que cont�m um determinado valor
Public Function AchaLinha(ByRef ObjUmObjeto As Object, _
                          ByRef sOValor As String, _
                          Optional ByRef lAColuna As Long = 0) As Long
    Dim lALinha As Long

    lALinha = 0

    While (sOValor <> ObjUmObjeto.TextMatrix(lALinha, lAColuna)) And (lALinha < ObjUmObjeto.Rows - 1)
        Inc lALinha
    Wend

    If sOValor = ObjUmObjeto.TextMatrix(lALinha, lAColuna) Then
        AchaLinha = lALinha
    Else
        AchaLinha = -1
    End If

End Function

'------------------------------------------------------------------------------------------
'
' Respons�vel : Jo�o Mac-Cormick
' Dt Desenv : 03/06/2002
'
' implementar futuramente esta fun��o para fazer com
' que a coluna fique no tamanho da c�lula
' a fun��o textwidth n�o calcula corretamente
Public Sub AcertaTamanhoColuna(ByRef ObjUmObjeto As Object, _
                               ByRef lALinha As Long, _
                               Optional ByRef lAColunaInicial As Long = 1)
    Exit Sub
    Dim lAColuna As Long

    If lALinha > ObjUmObjeto.Rows - 1 Then Exit Sub

    For lAColuna = lAColunaInicial To ObjUmObjeto.Cols - 1

        If ObjUmObjeto.ColWidth(lAColuna) < Screen.ActiveForm.TextWidth(ObjUmObjeto.TextMatrix(lALinha, lAColuna)) Then
            ObjUmObjeto.ColWidth(lAColuna) = Screen.ActiveForm.TextWidth(ObjUmObjeto.TextMatrix(lALinha, lAColuna))
        End If

    Next lAColuna

End Sub

'------------------------------------------------------------------------------------------
'
' Respons�vel : Jo�o Mac-Cormick
' Dt Desenv : 03/06/2002
'
' testa antes de mudar o foco para um objeto
Public Function MoveFoco(ObjUmObjeto As Object) As Boolean

    If ObjUmObjeto.Enabled Then
        ObjUmObjeto.SetFocus
        MoveFoco = True
    Else
        MoveFoco = False
    End If

End Function

'------------------------------------------------------------------------------------------
'
' Respons�vel : Jo�o Mac-Cormick
' Dt Desenv : 03/06/2002
'
' inicializa o rodap� padr�o
' precisa ser chamado de StbRodape no formul�rio
Public Sub InicializarRodape(Optional FrmOFormulario, Optional ByVal sUmaMensagem)
    Dim OFormulario As Form, sAMensagem As String

    If IsMissing(FrmOFormulario) Then
        Set OFormulario = Screen.ActiveForm
    Else
        Set OFormulario = FrmOFormulario
    End If

    If IsMissing(sUmaMensagem) Then
        If Trim(OFormulario.StbRodape.Panels(1).Text) = "" Then
            sAMensagem = App.EXEName
        Else
            sAMensagem = OFormulario.StbRodape.Panels(1).Text
        End If

    Else
        Set OFormulario = FrmOFormulario
    End If

    Dim lQtdPainel As Long, lTamanhoU As Long, lTamanhoD As Long

    lQtdPainel = OFormulario.StbRodape.Panels.Count
    OFormulario.StbRodape.Panels(1).Text = sAMensagem
    OFormulario.StbRodape.Panels(lQtdPainel - 1).Text = Data_Sistema
    OFormulario.StbRodape.Panels(lQtdPainel).Text = cUserName
    lTamanhoD = OFormulario.StbRodape.Panels(lQtdPainel - 1).Width
    lTamanhoU = OFormulario.StbRodape.Panels(lQtdPainel).Width
    OFormulario.StbRodape.Panels(1).Width = OFormulario.Width - 400 - lTamanhoD - lTamanhoU
End Sub

'##ModelId=3CFD88790280
Public Function PosicionaCombo(ByRef oCombo As Object, ByRef sString As String) As Long
    Dim Indice As Long

    With oCombo

        For Indice = 0 To .ListCount - 1

            'Debug.Print sString, Mid(.List(indice), 1, Len(sString))
            If Trim(sString) = Trim(Mid(.List(Indice), 1, Len(sString))) Then
                PosicionaCombo = Indice
                Exit For
            End If

        Next Indice

    End With

End Function

'##ModelId=3CFD887902BD
Public Function PosicionaComboItemData(ByRef oCombo As Object, _
                                       ByRef sString As Long) As Long
    Dim Indice As Long

    With oCombo

        For Indice = 0 To .ListCount - 1

            If sString = .ItemData(Indice) Then
                PosicionaComboItemData = Indice
                Exit For
            End If

        Next Indice

    End With

End Function

'------------------------------------------------------------------------------------------
'
' Respons�vel : Jo�o Mac-Cormick
' Dt Desenv : 15/06/2002
'
' retorna verdadeiro se o caminho � uma pasta
Public Function VerificaPasta(sOCaminho As String)
    VerificaPasta = (GetAttr(sOCaminho) And vbDirectory) = vbDirectory
End Function

'------------------------------------------------------------------------------------------
'
' Respons�vel : Jo�o Mac-Cormick
' Dt Desenv : 15/06/2002
'
' retorna uma cole�ao com os arquivos e subpastas de uma pasta
' pode ser passado um filtro de atributos
Public Sub MontaPasta(ColecaoPasta As Collection, _
                      ByVal sOCaminho As String, _
                      Optional ByVal vOFiltro As VbFileAttribute = vbNormal, _
                      Optional SubDiretorio As Boolean = False)
    Dim sElemento As String, sElementoAuxiliar As String

    sOCaminho = LTrim(Replace(Replace(sOCaminho, "*", ""), "?", ""))

    If VerificaPasta(sOCaminho) And Right(sOCaminho, 1) <> "\" Then
        sOCaminho = sOCaminho & "\"
    End If

    sElemento = Dir(sOCaminho)

    While sElemento <> ""

        If sElemento <> "." And sElemento <> ".." Then
            If (GetAttr(sOCaminho & sElemento) And vOFiltro) = vOFiltro Then
                ColecaoPasta.Add sOCaminho & sElemento
            End If

            If SubDiretorio = True Then
                If VerificaPasta(sOCaminho & sElemento) Then
                    Call MontaPasta(ColecaoPasta, sOCaminho & sElemento, vOFiltro)
                End If
            End If
        End If

        sElementoAuxiliar = Dir(sOCaminho, vbDirectory)

        Do While sElementoAuxiliar <> ""

            If sElemento = sElementoAuxiliar Then Exit Do
            sElementoAuxiliar = Dir
        Loop

        sElementoAuxiliar = Dir
        sElemento = sElementoAuxiliar
    Wend

End Sub
'------------------------------------------------------------------------------------------
'
' Respons�vel : Antonio Bosco
' Dt Desenv : 02/07/2002
'
' atribui as dimensoes iniciais para as variaveis globais

Public Sub MoveDimensoesForm(frmForm As Form, _
                             sgFormHeight As Single, _
                             sgFormWidth As Single)
    sgFormHeight = frmForm.Height
    sgFormWidth = frmForm.Width
End Sub
'------------------------------------------------------------------------------------------
'
' Respons�vel : Antonio Bosco
' Dt Desenv : 02/07/2002
'
' Redimensiona e/ou Move os objetos da tela conforme ela � redimensionada

Public Sub Redimensiona_Objetos(ByRef frmForm As Form, _
                                sgAltura As Single, _
                                sgLargura As Single, _
                                Optional sgMinHeigth As Single, _
                                Optional sgMinWidth As Single)
    Dim dpercent_x As Double

    Dim dpercent_y As Double

    Dim OBJETO     As Object

    Dim bResize    As Boolean

    Dim bAlterado  As Boolean

    Dim bVisivel   As Boolean

    bResize = True

    If Not frmForm.WindowState = vbMinimized Then
        If sgMinHeigth = 0 Then sgMinHeigth = 5000
        If sgMinWidth = 0 Then sgMinWidth = 5000
        If frmForm.Height <= sgMinHeigth Then
            frmForm.Height = sgMinHeigth + 1
            bResize = False
        End If

        If frmForm.Width <= sgMinWidth Then
            frmForm.Width = sgMinWidth + 1
            bResize = False
        End If

        If bResize Then
            dpercent_x = frmForm.Width / sgLargura
            dpercent_y = frmForm.Height / sgAltura
            sgAltura = frmForm.Height
            sgLargura = frmForm.Width

            For Each OBJETO In frmForm.Controls

                If Not (TypeName(OBJETO) = "Timer" Or TypeName(OBJETO) = "CommonDialog" Or TypeName(OBJETO) = "ImageList" Or TypeName(OBJETO) = "MAPISession" Or TypeName(OBJETO) = "MAPIMessages" Or TypeName(OBJETO) = "Menu") Then
                    bVisivel = OBJETO.Visible
                    OBJETO.Visible = False

                    If (TypeName(OBJETO) = "Frame") Or (TypeName(OBJETO) = "ListView") Or (TypeName(OBJETO) = "TreeView") Or (TypeName(OBJETO) = "TabStrip") Or (TypeName(OBJETO) = "SSTab") Or (TypeName(OBJETO) = "RichTextBox") Or (TypeName(OBJETO) = "MSFlexGrid") Then
                        OBJETO.Height = Round((OBJETO.Height * dpercent_y), 0)
                        OBJETO.Width = Round((OBJETO.Width * dpercent_x), 0)
                    Else

                        If (TypeName(OBJETO) = "TextBox") Or (TypeName(OBJETO) = "HScrollBar") Or (TypeName(OBJETO) = "ListBox") Or (TypeName(OBJETO) = "ProgressBar") Or (TypeName(OBJETO) = "Slider") Or (TypeName(OBJETO) = "MaskEdBox") Or (TypeName(OBJETO) = "ComboBox") Or (TypeName(OBJETO) = "Label") Then
                            If (TypeName(OBJETO) = "Label") Then
                                If OBJETO.BorderStyle = vbFixedSingle Then
                                    OBJETO.Width = Round((OBJETO.Width * dpercent_x), 0)
                                End If

                            Else
                                OBJETO.Width = Round((OBJETO.Width * dpercent_x), 0)
                            End If

                        Else

                            If TypeName(OBJETO) = "VScrollBar" Then
                                OBJETO.Height = Round((OBJETO.Height * dpercent_y), 0)
                            End If
                        End If
                    End If

                    bAlterado = False

                    If Not TypeName(OBJETO) = "StatusBar" And Not TypeName(OBJETO) = "Line" Then
                        If (TypeName(OBJETO.Container) = "SSTab") And OBJETO.Left < 0 Then
                            OBJETO.Visible = False
                            OBJETO.Left = OBJETO.Left + 75000
                            bAlterado = True
                        End If

                        OBJETO.Left = Round((OBJETO.Left * dpercent_x), 0)
                        OBJETO.Top = Round((OBJETO.Top * dpercent_y), 0)

                        If bAlterado Then
                            OBJETO.Left = OBJETO.Left - 75000
                            OBJETO.Visible = True
                        End If
                    End If '

                    OBJETO.Visible = bVisivel
                End If '

            Next

        End If
    End If

    Exit Sub
End Sub

Function RetiraCaracterEspecial(sStringEntrada As String) As String
    '@'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    '@Fun��o que retira caracteres especiais como tabula��o
    '@Na tabela ASCII eles possuem c�digo menor do que 32
    '@
    '@Respons�vel: Anderson Rodrigues
    '@Data Desenvolvimento: 19/08/2002
    '@'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    Dim iIndice As Integer

    For iIndice = 1 To Len(sStringEntrada)

        If Asc(Mid(sStringEntrada, iIndice, 1)) >= 32 Then
            RetiraCaracterEspecial = RetiraCaracterEspecial + Mid(sStringEntrada, iIndice, 1)
        End If

    Next

End Function

'------------------------------------------------------------------------------------------
'
' Respons�vel : Bruno Carneiro
' Dt Desenv : 09/08/2002
'
' Habilita ou desabilita entradas de mouse e teclado 'a janela especificada
' hwnd - handle da janela
' benable - flag para habilitar(true) ou desabilitar(false) as entradas a janela
Public Function HabilitarJanela(ByVal hwnd As Long, ByVal benable As Boolean) As Long
    HabilitarJanela = EnableWindow(hwnd, benable)
End Function

Sub Habilitar_Todas_Janelas(ByVal AInstanciaProcesso As Double, ByVal benable As Boolean)
    Dim AInstanciaJanela         As Long

    Dim AInstanciaProcessoJanela As Long

    On Local Error GoTo TrataErro
    ' pega a primeira janela que achar
    AInstanciaJanela = FindWindow(vbNullString, vbNullString)

    ' busca em todas as janelas at� n�o encontrar mais
    Do Until AInstanciaJanela = 0

        ' verifica se � a janela principal de uma aplica��o
        If GetParent(AInstanciaJanela) = 0 Then
            ' verifica se � a inst�ncia da aplica��o passada
            Call GetWindowThreadProcessId(AInstanciaJanela, AInstanciaProcessoJanela)

            If AInstanciaProcesso = AInstanciaProcessoJanela Then
                ' achou a janela do processo
                Call HabilitarJanela(AInstanciaJanela, benable)
            End If
        End If

        AInstanciaJanela = GetWindow(AInstanciaJanela, GW_HWNDNEXT)
    Loop

    Exit Sub
TrataErro:
    Call GravaMensagem("Erro na fun��o ==> " & Err.Description, "PROC", "PegaJanela", "Funcoes.bas")
End Sub

Function VerificarJanelaAtiva(ByVal AInstanciaProcesso As Double) As Boolean
    Dim AInstanciaJanela         As Long

    Dim AInstanciaProcessoJanela As Long

    Dim lInstaciaJanelaAtiva     As Long

    On Local Error GoTo TrataErro
    'Pega a instacia do processo da janela ativa
    lInstaciaJanelaAtiva = GetForegroundWindow
    Call GetWindowThreadProcessId(lInstaciaJanelaAtiva, AInstanciaProcessoJanela)
    VerificarJanelaAtiva = False

    'Veirifica se faz parte dos processos do SEGBR
    If AInstanciaProcesso = AInstanciaProcessoJanela Then
        VerificarJanelaAtiva = True
    End If

    Exit Function
TrataErro:
    Call GravaMensagem("Erro na fun��o ==> " & Err.Description, "PROC", "VerificarJanelaAtiva", "Funcoes.bas")
End Function

Function RetiraQualquerCaracter(sEntrada As String) As String
    '***********************************************************
    '** Fun��o que retira qualquer caracter n�o num�rico      **
    '** Respons�vel: Paulo Carvalho                           **
    '** Data Desenvolvimento: 22/11/2002                      **
    '***********************************************************
    Dim iCont As Integer

    For iCont = 1 To Len(sEntrada)

        If IsNumeric(Mid(sEntrada, iCont, 1)) Then
            RetiraQualquerCaracter = RetiraQualquerCaracter & Mid(sEntrada, iCont, 1)
        End If

    Next

End Function

Sub LimparColecao(ByRef oColecao As Collection)
    'Jorfilho 07-mar-2003
    'Retira todos os elementos de uma cole��o
    Dim i As Long

    'Limpando a cole��o
    For i = 1 To oColecao.Count
        oColecao.Remove 1
    Next

End Sub

Public Function ValidarData(ByVal dtData As Date, _
                            Optional ByVal bDataLonga As Boolean = False) As Boolean
    '***********************************************************
    '** Fun��o valida SMALLDATETIME ou DATETIME do SQL Server **
    '** Respons�vel: Paulo Carvalho                           **
    '** Data Desenvolvimento: 25/06/2003                      **
    '***********************************************************
    Dim iDia As Integer

    Dim iMes As Integer

    Dim iAno As Integer

    ValidarData = False

    If Not IsDate(dtData) Then Exit Function
    iDia = Mid(CStr(dtData), 1, 2)
    iMes = Mid(CStr(dtData), 4, 2)
    iAno = Mid(CStr(dtData), 7, 4)

    Select Case iMes
        Case 1, 3, 5, 7, 8, 10, 12

            If iDia > 31 Then Exit Function
        Case 4, 6, 9, 11

            If iDia > 30 Then Exit Function
        Case 2

            If ((iAno Mod 4 = 0) And (iAno Mod 100 <> 0)) Or (iAno Mod 400 = 0) Then
                If iDia > 29 Then Exit Function
            Else

                If iDia > 28 Then Exit Function
            End If

        Case Else
            Exit Function
    End Select

    If Not bDataLonga Then
        If iAno < 1900 Or iAno > 2079 Then Exit Function
    Else

        If iAno < 1753 Then Exit Function
    End If

    ValidarData = True
End Function

Function Null2Num(vCampo)

    If IsNull(vCampo) Then
        Null2Num = 0
    ElseIf InStr(vCampo, ".") > 0 Then
        Null2Num = Val(vCampo)
    ElseIf InStr(vCampo, ",") > 0 Then
        Null2Num = CDbl(vCampo)
    Else
        Null2Num = Val(vCampo)
    End If

End Function
