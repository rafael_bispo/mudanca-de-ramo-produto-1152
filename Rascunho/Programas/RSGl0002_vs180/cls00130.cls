VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cls00130"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True

Public Function Consultar(ByVal sSiglaSistema As String, _
                          ByVal sSiglaRecurso As String, _
                          ByVal sDescricaoRecurso As String, _
                          ByVal iAmbienteId As Integer, _
                          Optional ByVal iTpConsulta As Integer, _
                          Optional ByVal sConteudo As String) As Object
    '## Rotina de consulta em broker_tb
    'Tipo Consulta '''''''''''''''''''''''''''''''''''''''''''''''''
    '0 - Todos
    '1 - C�digo
    '2 - Nome
    Dim sSQL As String

    On Error GoTo Trata_Erro
    sSQL = ""
    sSQL = sSQL & "  SELECT broker_tb.nome," & vbNewLine
    sSQL = sSQL & "         broker_tb.broker_id," & vbNewLine
    sSQL = sSQL & "         broker_tb.cnpj," & vbNewLine
    sSQL = sSQL & "         broker_tb.endereco," & vbNewLine
    sSQL = sSQL & "         broker_tb.empresa_rep," & vbNewLine
    sSQL = sSQL & "         broker_tb.estado," & vbNewLine
    sSQL = sSQL & "         broker_tb.municipio," & vbNewLine
    sSQL = sSQL & "         broker_tb.cep," & vbNewLine
    sSQL = sSQL & "         broker_tb.bairro," & vbNewLine
    sSQL = sSQL & "         broker_tb.DDD," & vbNewLine
    sSQL = sSQL & "         broker_tb.telefone," & vbNewLine
    sSQL = sSQL & "         broker_tb.contato" & vbNewLine
    sSQL = sSQL & "    FROM broker_tb" & vbNewLine

    Select Case iTpConsulta
        Case 1
            sSQL = sSQL & "   WHERE broker_tb.broker_id = " & sConteudo & vbNewLine
        Case 2
            sSQL = sSQL & "   WHERE broker_tb.nome LIKE '" & sConteudo & "%'" & vbNewLine
    End Select

    sSQL = sSQL & "ORDER BY broker_tb.broker_id"
    Set Consultar = ExecutarSQL(sSiglaSistema, iAmbienteId, sSiglaRecurso, sDescricaoRecurso, sSQL, True)
    Exit Function
Trata_Erro:
    Call Err.Raise(Err.Number, , "RSGL0001.cls0130.Consultar - " & Err.Description)
End Function

'Rotina de inclus�o de Broker '''''''''''''''''''''''''''''''''''
Public Function Incluir(ByVal sSiglaSistema As String, _
                        ByVal sSiglaRecurso As String, _
                        ByVal sDescricaoRecurso As String, _
                        ByVal lAmbienteID As Long, _
                        ByVal sNome As String, _
                        ByVal sCNPJ As String, _
                        ByVal sEndereco As String, _
                        ByVal sEmpresa_rep As String, _
                        ByVal sMunicipio As String, _
                        ByVal sBairro As String, _
                        ByVal sEstado As String, _
                        ByVal sCEP As String, _
                        ByVal sDDD As String, _
                        ByVal sTelefone As String, _
                        ByVal sContato As String, _
                        ByVal sUsuario As String) As Long

    On Error GoTo Trata_Erro
    Dim sSQL       As String

    Dim oResultado As Object

    sSQL = "EXEC broker_spi '" & sNome & "', "
    adSQL sSQL, "'" & sCNPJ & "', "
    adSQL sSQL, "'" & sEndereco & "', "
    adSQL sSQL, "'" & sEmpresa_rep & "', "
    adSQL sSQL, "'" & sMunicipio & "',"
    adSQL sSQL, "'" & sBairro & "',"
    adSQL sSQL, "'" & sEstado & "',"
    adSQL sSQL, "'" & sCEP & "',"
    adSQL sSQL, "'" & sDDD & "', "
    adSQL sSQL, "'" & sTelefone & "', "
    adSQL sSQL, "'" & sContato & "', "
    adSQL sSQL, "'" & sUsuario & "'"
    Set oResultado = ExecutarSQL(sSiglaSistema, lAmbienteID, sSiglaRecurso, sDescricaoRecurso, sSQL, True)
    Incluir = oResultado(0)
    Exit Function
Trata_Erro:
    Call Err.Raise(Err.Number, , "RSGL0001.cls00130 - Incluir - " & Err.Description)
End Function

' Rotina de altera��o de Broker ''''''''''''''''''''''''''''''
Public Function Alterar(ByVal sSiglaSistema As String, _
                        ByVal sSiglaRecurso As String, _
                        ByVal sDescricaoRecurso As String, _
                        ByVal lAmbienteID As Long, _
                        ByVal iBrokerId As Integer, _
                        ByVal sNome As String, _
                        ByVal sCNPJ As String, _
                        ByVal sEndereco As String, _
                        ByVal sEmpresa_rep As String, _
                        ByVal sMunicipio As String, _
                        ByVal sBairro As String, _
                        ByVal sEstado As String, _
                        ByVal sCEP As String, _
                        ByVal sDDD As String, _
                        ByVal sTelefone As String, _
                        ByVal sContato As String, _
                        ByVal sUsuario As String) As Boolean

    On Error GoTo Trata_Erro
    Dim sSQL       As String

    Dim oResultado As Object

    sSQL = "EXEC broker_spu " & iBrokerId & ", "
    adSQL sSQL, "'" & sNome & "', "
    adSQL sSQL, "'" & sCNPJ & "', "
    adSQL sSQL, "'" & sEndereco & "', "
    adSQL sSQL, "'" & sEmpresa_rep & "',"
    adSQL sSQL, "'" & sMunicipio & "',"
    adSQL sSQL, "'" & sBairro & "',"
    adSQL sSQL, "'" & sEstado & "',"
    adSQL sSQL, "'" & sCEP & "',"
    adSQL sSQL, "'" & sDDD & "', "
    adSQL sSQL, "'" & sTelefone & "', "
    adSQL sSQL, "'" & sContato & "', "
    adSQL sSQL, "'" & sUsuario & "'"
    Call ExecutarSQL(sSiglaSistema, lAmbienteID, sSiglaRecurso, sDescricaoRecurso, sSQL, False)
    Alterar = True
    Exit Function
Trata_Erro:
    Call Err.Raise(Err.Number, , "RSGL0001.cls00130.Alterar - " & Err.Description)
End Function

' Rotina de exclus�o de Broker '''''''''''''''''''''''''''''''''''
Public Function Excluir(ByVal sSiglaSistema As String, _
                        ByVal sSiglaRecurso As String, _
                        ByVal sDescricaoRecurso As String, _
                        ByVal lAmbienteID As Long, _
                        ByVal iBrokerId As Long) As Boolean

    On Error GoTo Trata_Erro
    Dim sSQL       As String

    Dim oResultado As Object

    sSQL = "EXEC broker_spd " & iBrokerId
    Call ExecutarSQL(sSiglaSistema, lAmbienteID, sSiglaRecurso, sDescricaoRecurso, sSQL, False)
    Excluir = True
    Exit Function
Trata_Erro:
    Call Err.Raise(Err.Number, , "RSGL0001.cls0130.Excluir - " & Err.Description)
End Function
