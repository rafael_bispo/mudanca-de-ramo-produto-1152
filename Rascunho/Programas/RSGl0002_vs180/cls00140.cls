VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cls00140"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Public Function ConsultarApoliceSubGrupoResseguro(ByVal sSiglaSistema As String, _
                                                  ByVal sSiglaRecurso As String, _
                                                  ByVal sDescricaoRecurso As String, _
                                                  ByVal iAmbienteId As Integer, _
                                                  ByVal iTpPesquisa As Integer, _
                                                  Optional ByVal iSeguradoraCodSusep As Integer, _
                                                  Optional ByVal iSucursalSeguradoraId As Integer, _
                                                  Optional ByVal iRamoId As Integer, _
                                                  Optional ByVal lApoliceId As Long, _
                                                  Optional ByVal lPropostaId As Long) As Object
    Dim sSQL As String

    On Error GoTo Trata_Erro
    sSQL = ""
    adSQL sSQL, "   SELECT apolice_tb.apolice_id,"
    adSQL sSQL, "          apolice_tb.ramo_id,"
    adSQL sSQL, "          apolice_tb.seguradora_cod_susep,"
    adSQL sSQL, "          apolice_tb.sucursal_seguradora_id,"
    adSQL sSQL, "          apolice_tb.dt_inicio_vigencia dt_inicio_vigencia_apolice,"
    adSQL sSQL, "          apolice_tb.dt_emissao dt_emissao_apolice,"
    adSQL sSQL, "          proposta_tb.proposta_id,"
    adSQL sSQL, "          proposta_tb.subramo_id,"
    adSQL sSQL, "          produto_tb.produto_id,"
    adSQL sSQL, "          produto_tb.nome nome_produto,"
    adSQL sSQL, "          sub_grupo_apolice_tb.nome nome_subgrupo,"
    adSQL sSQL, "          sub_grupo_apolice_tb.sub_grupo_id,"
    adSQL sSQL, "          sub_grupo_apolice_resseguro_tb.val_limite_retencao,"
    adSQL sSQL, "          sub_grupo_apolice_resseguro_tb.dt_inicio_vigencia dt_inicio_vigencia_subgrupo,"
    adSQL sSQL, "          sub_grupo_apolice_resseguro_tb.sub_grupo_apolice_resseguro_id,"
    adSQL sSQL, "          sub_grupo_apolice_resseguro_tb.tratamento_manual,"
    adSQL sSQL, "          'CPF_CGC' = CASE"
    adSQL sSQL, "                         WHEN IsNull(pessoa_fisica_tb.pf_cliente_id, 0) <> 0"
    adSQL sSQL, "                         THEN pessoa_fisica_tb.cpf"
    adSQL sSQL, "                         ELSE pessoa_juridica_tb.CGC"
    adSQL sSQL, "                      END,"
    adSQL sSQL, "          'tipo_pessoa' = CASE"
    adSQL sSQL, "                             WHEN IsNull(pessoa_fisica_tb.pf_cliente_id, 0) <> 0"
    adSQL sSQL, "                             THEN 0" 'Pessoa F�sica
    adSQL sSQL, "                             ELSE 1" 'Pessoa Juridica
    adSQL sSQL, "                          END"
    adSQL sSQL, "     FROM seguros_db..apolice_tb apolice_tb"
    adSQL sSQL, "     JOIN seguros_db..proposta_tb proposta_tb"
    adSQL sSQL, "       ON proposta_tb.proposta_id = apolice_tb.proposta_id"
    adSQL sSQL, "     JOIN seguros_db..produto_tb produto_tb"
    adSQL sSQL, "       ON produto_tb.produto_id = proposta_tb.produto_id"
    adSQL sSQL, "     JOIN seguros_db..sub_grupo_apolice_tb sub_grupo_apolice_tb"
    adSQL sSQL, "       ON sub_grupo_apolice_tb.apolice_id = apolice_tb.apolice_id"
    adSQL sSQL, "      AND sub_grupo_apolice_tb.seguradora_cod_susep = apolice_tb.seguradora_cod_susep"
    adSQL sSQL, "      AND sub_grupo_apolice_tb.sucursal_seguradora_id = apolice_tb.sucursal_seguradora_id"
    adSQL sSQL, "      AND sub_grupo_apolice_tb.ramo_id = apolice_tb.ramo_id"
    adSQL sSQL, "LEFT JOIN sub_grupo_apolice_resseguro_tb"
    adSQL sSQL, "       ON sub_grupo_apolice_resseguro_tb.apolice_id = sub_grupo_apolice_tb.apolice_id"
    adSQL sSQL, "      AND sub_grupo_apolice_resseguro_tb.sucursal_seguradora_id = sub_grupo_apolice_tb.sucursal_seguradora_id"
    adSQL sSQL, "      AND sub_grupo_apolice_resseguro_tb.seguradora_cod_susep = sub_grupo_apolice_tb.seguradora_cod_susep"
    adSQL sSQL, "      AND sub_grupo_apolice_resseguro_tb.ramo_id = sub_grupo_apolice_tb.ramo_id"
    adSQL sSQL, "      AND sub_grupo_apolice_resseguro_tb.sub_grupo_id = sub_grupo_apolice_tb.sub_grupo_id"
    adSQL sSQL, "      AND sub_grupo_apolice_resseguro_tb.dt_fim_vigencia IS NULL"
    adSQL sSQL, "LEFT JOIN seguros_db..pessoa_fisica_tb pessoa_fisica_tb"
    adSQL sSQL, "       ON pessoa_fisica_tb.pf_cliente_id = proposta_tb.prop_cliente_id"
    adSQL sSQL, "LEFT JOIN seguros_db..pessoa_juridica_tb pessoa_juridica_tb"
    adSQL sSQL, "       ON pessoa_juridica_tb.pj_cliente_id = proposta_tb.prop_cliente_id"

    ' Se o tipo de pesquisa for por ap�lice '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    If iTpPesquisa = 0 Then
        adSQL sSQL, "    WHERE apolice_tb.apolice_id = " & lApoliceId
        adSQL sSQL, "      AND apolice_tb.seguradora_cod_susep = " & iSeguradoraCodSusep
        adSQL sSQL, "      AND apolice_tb.sucursal_seguradora_id = " & iSucursalSeguradoraId
        adSQL sSQL, "      AND apolice_tb.ramo_id = " & iRamoId
        ' Se o tipo de pesquisa for por proposta ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ElseIf iTpPesquisa = 1 Then
        adSQL sSQL, "    WHERE proposta_tb.proposta_id = " & lPropostaId
        ' Se o tipo de pesquisa for sem informa��o de resseguro ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ElseIf iTpPesquisa = 2 Then
        adSQL sSQL, "    WHERE sub_grupo_apolice_resseguro_tb.apolice_id IS NULL"
    End If

    adSQL sSQL, "    ORDER BY sub_grupo_apolice_tb.apolice_id,"
    adSQL sSQL, "             sub_grupo_apolice_tb.ramo_id,"
    adSQL sSQL, "             sub_grupo_apolice_tb.seguradora_cod_susep,"
    adSQL sSQL, "             sub_grupo_apolice_tb.sucursal_seguradora_id,"
    adSQL sSQL, "             sub_grupo_apolice_tb.sub_grupo_id"
    Set ConsultarApoliceSubGrupoResseguro = ExecutarSQL(sSiglaSistema, iAmbienteId, sSiglaRecurso, sDescricaoRecurso, sSQL)
    Exit Function
Trata_Erro:
    Call Err.Raise(Err.Number, , "RSGL0002.cls0140.ConsultarApoliceSubGrupoResseguro - " & Err.Description)
End Function

Public Function ConsultarContratoPlanoVida(ByVal sSiglaSistema As String, _
                                           ByVal sSiglaRecurso As String, _
                                           ByVal sDescricaoRecurso As String, _
                                           ByVal iAmbienteId As Integer, _
                                           ByVal iSeguradoraCodSusep As Integer, _
                                           ByVal iSucursalSeguradoraId As Integer, _
                                           ByVal iRamoId As Integer, _
                                           ByVal lApoliceId As Long) As Object
    Dim sSQL As String

    On Error GoTo Trata_Erro
    sSQL = ""
    adSQL sSQL, "SELECT contrato_plano_vida_tb.tabua_calculo_id,"
    adSQL sSQL, "       contrato_plano_vida_tb.perc_desconto,"
    adSQL sSQL, "       contrato_plano_vida_tb.limite_idade,"
    adSQL sSQL, "       contrato_plano_vida_tb.fator_iea,"
    adSQL sSQL, "       contrato_plano_vida_tb.fator_ipa,"
    adSQL sSQL, "       contrato_plano_vida_tb.perc_ipd,"
    adSQL sSQL, "       contrato_plano_vida_tb.limite_retencao_minimo,"
    adSQL sSQL, "       contrato_plano_vida_tb.limite_retencao_maximo"
    adSQL sSQL, "  FROM seguros_db..apolice_tb apolice_tb"
    adSQL sSQL, "  JOIN contrato_apolice_tb"
    adSQL sSQL, "    ON contrato_apolice_tb.apolice_id = apolice_tb.apolice_id"
    adSQL sSQL, "   AND contrato_apolice_tb.seguradora_cod_susep = apolice_tb.seguradora_cod_susep"
    adSQL sSQL, "   AND contrato_apolice_tb.sucursal_seguradora_id = apolice_tb.sucursal_seguradora_id"
    adSQL sSQL, "   AND contrato_apolice_tb.ramo_id = apolice_tb.ramo_id"
    adSQL sSQL, "  JOIN contrato_tb"
    adSQL sSQL, "    ON contrato_tb.contrato_id = contrato_apolice_tb.contrato_id"
    adSQL sSQL, "  JOIN contrato_versao_tb"
    adSQL sSQL, "    ON contrato_versao_tb.contrato_id = contrato_tb.contrato_id"
    adSQL sSQL, "   AND contrato_versao_tb.num_versao = contrato_tb.num_versao_vigente"
    adSQL sSQL, "  JOIN contrato_plano_tb"
    adSQL sSQL, "    ON contrato_plano_tb.contrato_id = contrato_versao_tb.contrato_id"
    adSQL sSQL, "   AND contrato_plano_tb.num_versao = contrato_versao_tb.num_versao"
    adSQL sSQL, "  JOIN contrato_plano_vida_tb"
    adSQL sSQL, "    ON contrato_plano_vida_tb.contrato_plano_id = contrato_plano_tb.contrato_plano_id"
    adSQL sSQL, " WHERE apolice_tb.apolice_id = " & lApoliceId
    adSQL sSQL, "   AND apolice_tb.seguradora_cod_susep = " & iSeguradoraCodSusep
    adSQL sSQL, "   AND apolice_tb.sucursal_seguradora_id = " & iSucursalSeguradoraId
    adSQL sSQL, "   AND apolice_tb.ramo_id = " & iRamoId
    Set ConsultarContratoPlanoVida = ExecutarSQL(sSiglaSistema, iAmbienteId, sSiglaRecurso, sDescricaoRecurso, sSQL)
    Exit Function
Trata_Erro:
    Call Err.Raise(Err.Number, , "RSGL0002.cls0140.ConsultarContratoPlanoVida - " & Err.Description)
End Function

Public Function AtualizarApoliceSubGrupoResseguro(ByVal sSiglaSistema As String, _
                                                  ByVal sSiglaRecurso As String, _
                                                  ByVal sDescricaoRecurso As String, _
                                                  ByVal iAmbienteId As Integer, _
                                                  ByVal lConexao As Long, _
                                                  ByVal lSubGrupoApoliceResseguroID As Long, _
                                                  ByVal lApoliceId As Long, _
                                                  ByVal iRamoId As Integer, _
                                                  ByVal iSucursalSeguradoraId As Integer, _
                                                  ByVal iSeguradoraCodSusep As Integer, _
                                                  ByVal iSubGrupoId As Integer, _
                                                  ByVal cValLimiteRetencao As Currency, _
                                                  ByVal sDtInicioVigencia As String, _
                                                  ByVal sTratamentoManual As String, _
                                                  ByVal sUsuario As String) As Long
    Dim sSQL As String

    Dim rs   As Object

    On Error GoTo Trata_Erro
    'Atualizando SubGrupo Ap�lice Resseguro '''''''''''''''''''''''''''
    sSQL = ""
    adSQL sSQL, "EXEC sub_grupo_apolice_resseguro_cont_spu"
    adSQL sSQL, "   " & lSubGrupoApoliceResseguroID
    adSQL sSQL, "   ," & lApoliceId
    adSQL sSQL, "   ," & iRamoId
    adSQL sSQL, "   ," & iSucursalSeguradoraId
    adSQL sSQL, "   ," & iSeguradoraCodSusep
    adSQL sSQL, "   ," & iSubGrupoId
    adSQL sSQL, "   ," & TrocaVirgulaPorPonto(cValLimiteRetencao)
    adSQL sSQL, "   ,'" & Format(sDtInicioVigencia, "yyyymmdd") & "'"
    adSQL sSQL, "   ,NULL"
    adSQL sSQL, "   ,'" & sTratamentoManual & "'"
    adSQL sSQL, "   ,'" & sUsuario & "'"
    Set rs = Conexao_ExecutarSQL(sSiglaSistema, iAmbienteId, sSiglaRecurso, sDescricaoRecurso, sSQL, lConexao, True, False, 30000, adLockOptimistic, adUseClient)

    If Not rs.EOF Then
        AtualizarApoliceSubGrupoResseguro = rs(0)
    End If

    Exit Function
Trata_Erro:
    Call Err.Raise(Err.Number, , "RSGL0002.cls0140.AtualizarApoliceSubGrupoResseguro - " & Err.Description)
End Function

Public Sub ExcluirApoliceSubGrupoResseguro(ByVal sSiglaSistema As String, _
                                           ByVal sSiglaRecurso As String, _
                                           ByVal sDescricaoRecurso As String, _
                                           ByVal iAmbienteId As Integer, _
                                           ByVal lConexao As Long, _
                                           ByVal lSubGrupoApoliceResseguroID As Long, _
                                           ByVal sdtFimVigencia As String, _
                                           ByVal sUsuario As String)
    Dim sSQL As String

    On Error GoTo Trata_Erro
    sSQL = ""
    adSQL sSQL, "EXEC sub_grupo_apolice_resseguro_spd"
    adSQL sSQL, "   " & lSubGrupoApoliceResseguroID
    adSQL sSQL, "   ,'" & Format(sdtFimVigencia, "yyyymmdd") & "'"
    adSQL sSQL, "   ,'" & sUsuario & "'"
    Call Conexao_ExecutarSQL(sSiglaSistema, iAmbienteId, sSiglaRecurso, sDescricaoRecurso, sSQL, lConexao, False, False, 30000, adLockOptimistic, adUseClient)
    Exit Sub
Trata_Erro:
    Call Err.Raise(Err.Number, , "RSGL0002.cls0140.ExcluirApoliceSubGrupoResseguro - " & Err.Description)
End Sub

Public Function ValidarLimiteRetencaoSubGrupo(ByVal sSiglaSistema As String, _
                                              ByVal sSiglaRecurso As String, _
                                              ByVal sDescricaoRecurso As String, _
                                              ByVal iAmbienteId As Integer, _
                                              ByVal iSeguradoraCodSusep As Integer, _
                                              ByVal iSucursalSeguradoraId As Integer, _
                                              ByVal iRamoId As Integer, _
                                              ByVal lApoliceId As Long, _
                                              ByVal cLimiteRetencaoSubGrupo As Currency) As Boolean
    Dim rsContrato            As Recordset

    Dim cLimiteRetencaoMinimo As Currency

    Dim cLimiteRetencaoMaxino As Currency

    On Error GoTo Trata_Erro
    'Iniciando valor da fun��o ''''''''''''''''''''''''''''''''''''''''
    ValidarLimiteRetencaoSubGrupo = False
    'Obtendo valor do limite de retencao ''''''''''''''''''''''''''''''
    Set rsContrato = ConsultarContratoPlanoVida(sSiglaSistema, sSiglaRecurso, sDescricaoRecurso, iAmbienteId, iSeguradoraCodSusep, iSucursalSeguradoraId, iRamoId, lApoliceId)

    If Not rsContrato.EOF Then
        cLimiteRetencaoMinimo = Val(MudaVirgulaParaPonto(TiraCaracter(rsContrato("limite_retencao_minimo"), ".")))
        cLimiteRetencaoMaxino = Val(MudaVirgulaParaPonto(TiraCaracter(rsContrato("limite_retencao_maximo"), ".")))

        'Validando limite de reten��o '''''''''''''''''''''''''''''''''''
        If cLimiteRetencaoSubGrupo >= cLimiteRetencaoMinimo And cLimiteRetencaoSubGrupo <= cLimiteRetencaoMaxino Then
            ValidarLimiteRetencaoSubGrupo = True
        End If
    End If

    rsContrato.Close
    Set rsContrato = Nothing
    Exit Function
Trata_Erro:
    Call Err.Raise(Err.Number, , "RSGL0002.cls0140.ValidarLimiteRetencaoSubGrupo - " & Err.Description)
End Function

Public Function ConsultarContratoApoliceSubGrupo(ByVal sSiglaSistema As String, _
                                                 ByVal sSiglaRecurso As String, _
                                                 ByVal sDescricaoRecurso As String, _
                                                 ByVal iAmbienteId As Integer, _
                                                 ByVal lContratoApoliceSubgrupoId As Long, _
                                                 ByVal lContratoId As Long, _
                                                 ByVal lNumVersao As Long, _
                                                 ByVal lSubGrupoApoliceResseguroID As Long) As Object
    Dim sSQL As String

    On Error GoTo Trata_Erro
    sSQL = ""
    adSQL sSQL, "   SELECT contrato_apolice_sub_grupo_tb.*,"
    adSQL sSQL, "          contrato_tb.processo_irb,"
    adSQL sSQL, "          contrato_tb.documento_referencia"
    adSQL sSQL, "     FROM contrato_apolice_sub_grupo_tb"
    adSQL sSQL, "     JOIN contrato_tb"
    adSQL sSQL, "       ON contrato_tb.contrato_id = contrato_apolice_sub_grupo_tb.contrato_id"
    adSQL sSQL, "      AND contrato_tb.num_versao_vigente = contrato_apolice_sub_grupo_tb.num_versao"
    adSQL sSQL, "    WHERE 1 = 1"

    If lContratoApoliceSubgrupoId <> 0 Then
        adSQL sSQL, "      AND contrato_apolice_sub_grupo_tb.contrato_apolice_sub_grupo_id = " & lContratoApoliceSubgrupoId
    End If

    If lContratoId <> 0 Then
        adSQL sSQL, "      AND contrato_apolice_sub_grupo_tb.contrato_id = " & lContratoId
    End If

    If lNumVersao <> 0 Then
        adSQL sSQL, "      AND contrato_apolice_sub_grupo_tb.num_versao = " & lNumVersao
    End If

    If lSubGrupoApoliceResseguroID <> 0 Then
        adSQL sSQL, "      AND contrato_apolice_sub_grupo_tb.sub_grupo_apolice_resseguro_id = " & lSubGrupoApoliceResseguroID
    End If

    Set ConsultarContratoApoliceSubGrupo = ExecutarSQL(sSiglaSistema, iAmbienteId, sSiglaRecurso, sDescricaoRecurso, sSQL)
    Exit Function
Trata_Erro:
    Call Err.Raise(Err.Number, , "RSGL0002.cls0140.ConsultarApoliceSubGrupoResseguro - " & Err.Description)
End Function

Public Sub AtualizarContratoApoliceSubGrupo(ByVal sSiglaSistema As String, _
                                            ByVal sSiglaRecurso As String, _
                                            ByVal sDescricaoRecurso As String, _
                                            ByVal iAmbienteId As Integer, _
                                            ByVal lConexao As Long, _
                                            ByVal lSubGrupoApoliceResseguroNovo As Long, _
                                            ByVal sUsuario As String, _
                                            ByVal ContratosSubGrupo As Collection)
    Dim ContratoSubGrupo As Object

    On Error GoTo Trata_Erro

    For Each ContratoSubGrupo In ContratosSubGrupo

        Select Case ContratoSubGrupo.operacao
                'Inclus�o
            Case "I"

                If lSubGrupoApoliceResseguroNovo <> ContratoSubGrupo.sub_grupo_apolice_resseguro_id Then
                    Call AlterarContratoApoliceSubGrupo(sSiglaSistema, sSiglaRecurso, sDescricaoRecurso, iAmbienteId, lConexao, ContratoSubGrupo.sub_grupo_apolice_resseguro_id, lSubGrupoApoliceResseguroNovo, ContratoSubGrupo.contrato_id, ContratoSubGrupo.num_versao, sUsuario)
                End If

                If ContratoSubGrupo.origem_base = "N" Then
                    Call IncluirContratoApoliceSubGrupo(sSiglaSistema, sSiglaRecurso, sDescricaoRecurso, iAmbienteId, lConexao, lSubGrupoApoliceResseguroNovo, ContratoSubGrupo.contrato_id, ContratoSubGrupo.num_versao, sUsuario)
                End If

                'Branco - N�o foram feitas movimenta��es
            Case "B"

                If lSubGrupoApoliceResseguroNovo <> ContratoSubGrupo.sub_grupo_apolice_resseguro_id Then
                    Call AlterarContratoApoliceSubGrupo(sSiglaSistema, sSiglaRecurso, sDescricaoRecurso, iAmbienteId, lConexao, ContratoSubGrupo.sub_grupo_apolice_resseguro_id, lSubGrupoApoliceResseguroNovo, ContratoSubGrupo.contrato_id, ContratoSubGrupo.num_versao, sUsuario)
                End If

                'Exclus�o
            Case "E"

                If ContratoSubGrupo.origem_base = "S" Then
                    Call ExcluirContratoApoliceSubGrupo(sSiglaSistema, sSiglaRecurso, sDescricaoRecurso, iAmbienteId, lConexao, ContratoSubGrupo.contrato_id, ContratoSubGrupo.num_versao, ContratoSubGrupo.sub_grupo_apolice_resseguro_id)
                End If

        End Select

    Next

    Exit Sub
Trata_Erro:
    Call Err.Raise(Err.Number, , "RSGL0002.cls0140.AtualizarContratoApoliceSubGrupo - " & Err.Description)
End Sub

Sub IncluirContratoApoliceSubGrupo(ByVal sSiglaSistema As String, _
                                   ByVal sSiglaRecurso As String, _
                                   ByVal sDescricaoRecurso As String, _
                                   ByVal iAmbienteId As Integer, _
                                   ByVal lConexao As Long, _
                                   ByVal lSubGrupoApoliceResseguro As Long, _
                                   ByVal lContratoId As Long, _
                                   ByVal lNumVersao As Long, _
                                   ByVal sUsuario As String)
    '## Rotina de inclus�o em contrato_apolice_sub_grupo_tb
    Dim sSQL As String

    On Error GoTo Trata_Erro:
    sSQL = ""
    sSQL = "EXEC contrato_apolice_sub_grupo_spi "
    sSQL = sSQL & lSubGrupoApoliceResseguro & ", "
    sSQL = sSQL & lContratoId & ", "
    sSQL = sSQL & lNumVersao & ", '"
    sSQL = sSQL & sUsuario & "' "
    Call Conexao_ExecutarSQL(sSiglaSistema, iAmbienteId, sSiglaRecurso, sDescricaoRecurso, sSQL, lConexao, False, False, 30000, adLockOptimistic, adUseClient)
    Exit Sub
Trata_Erro:
    Call Err.Raise(Err.Number, , "RSGL0002.cls00127.IncluirContratoApoliceSubGrupo - " & Err.Description)
End Sub

Sub AlterarContratoApoliceSubGrupo(ByVal sSiglaSistema As String, _
                                   ByVal sSiglaRecurso As String, _
                                   ByVal sDescricaoRecurso As String, _
                                   ByVal iAmbienteId As Integer, _
                                   ByVal lConexao As Long, _
                                   ByVal lSubGrupoApoliceResseguroAtual As Long, _
                                   ByVal lSubGrupoApoliceResseguroNovo As Long, _
                                   ByVal lContratoId As Long, _
                                   ByVal lNumVersao As Long, _
                                   ByVal sUsuario As String)
    '## Rotina de inclus�o em contrato_apolice_sub_grupo_tb
    Dim sSQL As String

    On Error GoTo Trata_Erro:
    sSQL = ""
    sSQL = "EXEC contrato_apolice_sub_grupo_spu "
    sSQL = sSQL & lSubGrupoApoliceResseguroAtual & ", "
    sSQL = sSQL & lSubGrupoApoliceResseguroNovo & ", "
    sSQL = sSQL & lContratoId & ", "
    sSQL = sSQL & lNumVersao & ", '"
    sSQL = sSQL & sUsuario & "' "
    Call Conexao_ExecutarSQL(sSiglaSistema, iAmbienteId, sSiglaRecurso, sDescricaoRecurso, sSQL, lConexao, False, False, 30000, adLockOptimistic, adUseClient)
    Exit Sub
Trata_Erro:
    Call Err.Raise(Err.Number, , "RSGL0002.cls00127.AlterarContratoApoliceSubGrupo - " & Err.Description)
End Sub

Sub ExcluirContratoApoliceSubGrupo(ByVal sSiglaSistema As String, _
                                   ByVal sSiglaRecurso As String, _
                                   ByVal sDescricaoRecurso As String, _
                                   ByVal iAmbienteId As Integer, _
                                   ByVal lConexao As Long, _
                                   ByVal lContratoId As Long, _
                                   ByVal lNumVersao As Long, _
                                   ByVal lSubGrupoApoliceResseguroID As Long)
    '## Rotina de exclus�o em contrato_apolice_sub_grupo_tb
    Dim sSQL As String

    On Error GoTo Trata_Erro:
    sSQL = ""
    sSQL = "EXEC contrato_apolice_sub_grupo_spd "
    sSQL = sSQL & lContratoId & ", "
    sSQL = sSQL & lNumVersao & ", "
    sSQL = sSQL & lSubGrupoApoliceResseguroID
    Call Conexao_ExecutarSQL(sSiglaSistema, iAmbienteId, sSiglaRecurso, sDescricaoRecurso, sSQL, lConexao, False, False, 30000, adLockOptimistic, adUseClient)
    Exit Sub
Trata_Erro:
    Call Err.Raise(Err.Number, , "RSGL0002.cls00127.ExcluirContratoApoliceSubGrupo - " & Err.Description)
End Sub

Public Sub AlterarApoliceSubGrupoResseguro(ByVal sSiglaSistema As String, _
                                           ByVal sSiglaRecurso As String, _
                                           ByVal sDescricaoRecurso As String, _
                                           ByVal iAmbienteId As Integer, _
                                           ByVal lSubGrupoApoliceResseguroID As Long, _
                                           ByVal lApoliceId As Long, _
                                           ByVal iRamoId As Integer, _
                                           ByVal iSucursalSeguradoraId As Integer, _
                                           ByVal iSeguradoraCodSusep As Integer, _
                                           ByVal iSubGrupoId As Integer, _
                                           ByVal cValLimiteRetencao As Currency, _
                                           ByVal sDtInicioVigencia As String, _
                                           ByVal sdtFimVigencia As String, _
                                           ByVal sTratamentoManual As String, _
                                           ByVal sTpOperacao As String, _
                                           ByVal sUsuario As String, _
                                           ByVal ContratosSubGrupo As Collection)
    Dim sSQL                            As String

    Dim lConexao                        As Long

    Dim lNovoSubGrupoApoliceResseguroID As Long

    Dim ContratoSubGrupo                As Object

    On Error GoTo Trata_Erro
    'Obtendo Conexao ''''''''''''''''''''''''''''''''''''''''''''''''''''
    lConexao = AbrirConexao(sSiglaSistema, iAmbienteId, sSiglaRecurso, sDescricaoRecurso)

    'Abrindo Transa��o ''''''''''''''''''''''''''''''''''''''''''''''''''
    If Not AbrirTransacao(lConexao) Then
        Call Err.Raise(vbObjectError + 1000, , "RSGL0001.cls00138.AtualizaResseguroVida_Premio - N�o foi poss�vel abrir transa��o")
    End If

    If sTpOperacao = "A" Then
        'Atualizando SubGrupo Apolice Resseguro '''''''''''''''''''''''''''
        lNovoSubGrupoApoliceResseguroID = AtualizarApoliceSubGrupoResseguro(sSiglaSistema, sSiglaRecurso, sDescricaoRecurso, iAmbienteId, lConexao, lSubGrupoApoliceResseguroID, lApoliceId, iRamoId, iSucursalSeguradoraId, iSeguradoraCodSusep, iSubGrupoId, cValLimiteRetencao, sDtInicioVigencia, sTratamentoManual, sUsuario)
        'Atualizando Contrato Apolice SubGrupo ''''''''''''''''''''''''''''
        Call AtualizarContratoApoliceSubGrupo(sSiglaSistema, sSiglaRecurso, sDescricaoRecurso, iAmbienteId, lConexao, lNovoSubGrupoApoliceResseguroID, sUsuario, ContratosSubGrupo)
    ElseIf sTpOperacao = "E" Then
        'Excluindo SubGrupo Apolice Resseguro ''''''''''''''''''''''''''''''
        Call ExcluirApoliceSubGrupoResseguro(sSiglaSistema, sSiglaRecurso, sDescricaoRecurso, iAmbienteId, lConexao, lSubGrupoApoliceResseguroID, sdtFimVigencia, sUsuario)

        'Excluindo Contrato Apolice SubGrupo ''''''''''''''''''''''''''''''
        For Each ContratoSubGrupo In ContratosSubGrupo
            Call ExcluirContratoApoliceSubGrupo(sSiglaSistema, sSiglaRecurso, sDescricaoRecurso, iAmbienteId, lConexao, ContratoSubGrupo.contrato_id, ContratoSubGrupo.num_versao, ContratoSubGrupo.sub_grupo_apolice_resseguro_id)
        Next

    Else
        MsgBox "Tipo de opera��o inv�lida"
    End If

    'Fechando Transacao '''''''''''''''''''''''''''''''''''''''''''''''''
    Call ConfirmarTransacao(lConexao)
    Exit Sub
Trata_Erro:
    Call Err.Raise(Err.Number, , "RSGL0002.cls0140.AlterarApoliceSubGrupoResseguro - " & Err.Description)
End Sub
