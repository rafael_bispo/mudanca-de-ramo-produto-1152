VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsBaixaParcelamento"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True

Private mvarResseguroParcelamentoId As Long
Private mvarBaixaParcelamentoId As Long
Private mvarValParcelaPago As Currency
Private marValParcelaJurosPago As Currency
Private mvarValComissaoPago As Currency
Private mvarValComissaoJurosPago As Currency



Private mvarValDiferencaPremio As Currency
Private mvarValDiferencaPremioJuros As Currency
Private mvarValDiferencaComissao As Currency
Private mvarValDiferencaComissaoJuros As Currency



Private mvarTpDiferencaRepasseId As Integer
Private mvarUsuario As String
Private mvarDtPagamento As String
Private mvarEstorno As String
 
 
Private mvarValParcela As Currency
Private marValParcelaJuros As Currency
Private mvarValComissao As Currency
Private mvarValComissaoJuros As Currency



Public Property Get BaixaParcelamentoId() As Long
    BaixaParcelamentoId = mvarBaixaParcelamentoId
End Property

Public Property Let BaixaParcelamentoId(ByVal vData As Long)
   mvarBaixaParcelamentoId = vData
End Property


Public Property Get ValComissaoJuros() As Currency
     ValComissaoJuros = mvarValComissaoJuros
End Property

Public Property Let ValComissaoJuros(ByVal vData As Currency)
   mvarValComissaoJuros = vData
End Property
 
 
Public Property Get ValComissao() As Currency
     ValComissao = mvarValComissao
End Property

Public Property Let ValComissao(ByVal vData As Currency)
   mvarValComissao = vData
End Property
 
Public Property Get ValParcelaJuros() As Currency
     ValParcelaJuros = mvarValParcelaJuros
End Property

Public Property Let ValParcelaJuros(ByVal vData As Currency)
   mvarValParcelaJuros = vData
End Property
 
Public Property Get ValParcela() As Currency
     ValParcela = mvarValParcela
End Property

Public Property Let ValParcela(ByVal vData As Currency)
   mvarValParcela = vData
End Property

 
Public Property Get Estorno() As String
   Estorno = mvarEstorno
End Property

Public Property Let Estorno(ByVal vData As String)
    mvarEstorno = vData
End Property
  
 
  
Public Property Get DtPagamento() As String
    DtPagamento = mvarDtPagamento
End Property

Public Property Let DtPagamento(ByVal vData As String)
    mvarDtPagamento = vData
End Property
  
 
 
Public Property Get Usuario() As String
   Usuario = mvarUsuario
End Property

Public Property Let Usuario(ByVal vData As String)
    mvarUsuario = vData
End Property
  
 
 
Public Property Get TpDiferencaRepasseId() As Integer
    TpDiferencaRepasseId = mvarTpDiferencaRepasseId
End Property

Public Property Let TpDiferencaRepasseId(ByVal vData As Integer)
   mvarTpDiferencaRepasseId = vData
End Property
 
 
 
Public Property Get ValDiferencaComissaoJuros() As Currency
    ValDiferencaComissaoJuros = mvarValDiferencaComissaoJuros
End Property

Public Property Let ValDiferencaComissaoJuros(ByVal vData As Currency)
   mvarValDiferencaComissaoJuros = vData
End Property
 
 
 
Public Property Get ValDiferencaComissao() As Currency
    ValDiferencaComissao = mvarValDiferencaComissao
End Property

Public Property Let ValDiferencaComissao(ByVal vData As Currency)
   mvarValDiferencaComissao = vData
End Property
 
  
 
 
Public Property Get ValDiferencaPremioJuros() As Currency
    ValDiferencaPremioJuros = mvarValDiferencaPremioJuros
End Property

Public Property Let ValDiferencaPremioJuros(ByVal vData As Currency)
   mvarValDiferencaPremioJuros = vData
End Property
 
 
 
 
Public Property Get ValDiferencaPremio() As Currency
     ValDiferencaPremio = mvarValDiferencaPremio
End Property

Public Property Let ValDiferencaPremio(ByVal vData As Currency)
   mvarValDiferencaPremio = vData
End Property

 
Public Property Get ValComissaoJurosPago() As Currency
    ValComissaoJurosPago = mvarValComissaoJurosPago
End Property

Public Property Let ValComissaoJurosPago(ByVal vData As Currency)
   mvarValComissaoJurosPago = vData
End Property

 
 
Public Property Get ValComissaoPago() As Currency
    ValComissaoPago = mvarValComissaoPago
End Property

Public Property Let ValComissaoPago(ByVal vData As Currency)
   mvarValComissaoPago = vData
End Property

 


Public Property Get ResseguroParcelamentoId() As Long
   ResseguroParcelamentoId = mvarResseguroParcelamentoId
End Property

Public Property Let ResseguroParcelamentoId(ByVal vData As Long)
   mvarResseguroParcelamentoId = vData
End Property



Public Property Get ValParcelaPago() As Currency
    ValParcelaPago = mvarValParcelaPago
End Property

Public Property Let ValParcelaPago(ByVal vData As Currency)
   mvarValParcelaPago = vData
End Property


 
Public Property Get ValParcelaJurosPago() As Currency
    ValParcelaJurosPago = marValParcelaJurosPago
End Property

Public Property Let ValParcelaJurosPago(ByVal vData As Currency)
   marValParcelaJurosPago = vData
End Property


