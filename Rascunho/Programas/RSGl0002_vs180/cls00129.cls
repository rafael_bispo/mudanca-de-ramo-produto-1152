VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cls00129"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Public Function Incluir(ByVal sSiglaSistema As String, _
                        ByVal sSiglaRecurso As String, _
                        ByVal sDescricaoRecurso As String, _
                        ByVal lAmbienteID As Long, _
                        ByVal sCNPJ As String, _
                        ByVal sNome As String, _
                        ByVal sEndereco As String, _
                        ByVal sBairro As String, _
                        ByVal sMunicipio As String, _
                        ByVal sEstado As String, _
                        ByVal sCEP As String, _
                        ByVal sDDD As String, _
                        ByVal sTelefone As String, _
                        ByVal sContato As String, _
                        ByVal sUsuario As String, _
                        ByVal lCodigoFip As Long, _
                        ByVal sCodContabilResseg As String, _
                        ByVal lCategoriaId As Integer) As Boolean

    ' Inclus�o de dados do ressegurador
    On Error GoTo Trata_Erro
    Dim sSQL          As String

    Dim oRessegurador As Object

    sSQL = "EXEC dados_ressegurador_spi '" & sCNPJ & "', "
    adSQL sSQL, "'" & sNome & "', "
    adSQL sSQL, "'" & sEndereco & "', "
    adSQL sSQL, "'" & sBairro & "', "
    adSQL sSQL, "'" & sMunicipio & "', "
    adSQL sSQL, "'" & sEstado & "', "
    adSQL sSQL, "'" & sCEP & "', "
    adSQL sSQL, "'" & sDDD & "', "
    adSQL sSQL, "'" & sTelefone & "', "
    adSQL sSQL, "'" & sContato & "', "
    adSQL sSQL, "'" & sUsuario & "' , "
    adSQL sSQL, " " & lCodigoFip & " , "
    adSQL sSQL, "'" & sCodContabilResseg & "' , "
    adSQL sSQL, " " & lCategoriaId & " "
    Call ExecutarSQL(sSiglaSistema, lAmbienteID, sSiglaRecurso, sDescricaoRecurso, sSQL, False)
    Incluir = True
    Exit Function
Trata_Erro:
    Call Err.Raise(Err.Number, , "RSGL0002.cls00129.Incluir - " & Err.Description)
End Function

Public Function Alterar(ByVal sSiglaSistema As String, _
                        ByVal sSiglaRecurso As String, _
                        ByVal sDescricaoRecurso As String, _
                        ByVal lAmbienteID As Long, _
                        ByVal lResseguradorId As Long, _
                        ByVal sCNPJ As String, _
                        ByVal sNome As String, _
                        ByVal sEndereco As String, _
                        ByVal sBairro As String, _
                        ByVal sMunicipio As String, _
                        ByVal sEstado As String, _
                        ByVal sCEP As String, _
                        ByVal sDDD As String, _
                        ByVal sTelefone As String, _
                        ByVal sContato As String, _
                        ByVal sUsuario As String, _
                        ByVal lCodigoFip As Long, _
                        ByVal sCodContabilResseg As String, _
                        ByVal lCategoriaId As Integer) As Boolean

    'Atualizar dados do ressegurador
    On Error GoTo Trata_Erro
    Dim sSQL As String

    sSQL = "EXEC dados_ressegurador_spu " & lResseguradorId & ", "
    adSQL sSQL, "'" & sCNPJ & "', "
    adSQL sSQL, "'" & sNome & "', "
    adSQL sSQL, "'" & sEndereco & "', "
    adSQL sSQL, "'" & sBairro & "', "
    adSQL sSQL, "'" & sMunicipio & "', "
    adSQL sSQL, "'" & sEstado & "', "
    adSQL sSQL, "'" & sCEP & "', "
    adSQL sSQL, "'" & sDDD & "', "
    adSQL sSQL, "'" & sTelefone & "', "
    adSQL sSQL, "'" & sContato & "', "
    adSQL sSQL, "'" & sUsuario & "', "
    adSQL sSQL, " " & lCodigoFip & " , "
    adSQL sSQL, "'" & sCodContabilResseg & "' , "
    adSQL sSQL, " " & lCategoriaId & " "
    Call ExecutarSQL(sSiglaSistema, lAmbienteID, sSiglaRecurso, sDescricaoRecurso, sSQL, False)
    Alterar = True
    Exit Function
Trata_Erro:
    Call Err.Raise(Err.Number, , "RSGL0002.cls00129.Alterar - " & Err.Description)
End Function

Public Function Excluir(ByVal sSiglaSistema As String, _
                        ByVal sSiglaRecurso As String, _
                        ByVal sDescricaoRecurso As String, _
                        ByVal lAmbienteID As Long, _
                        ByVal lResseguradorId As Long) As Boolean
    'Excluir dados do ressegurador
    Dim sSQL As String

    sSQL = "EXEC dados_ressegurador_spd " & lResseguradorId
    Call ExecutarSQL(sSiglaSistema, lAmbienteID, sSiglaRecurso, sDescricaoRecurso, sSQL, False)
    Excluir = True
    Exit Function
Trata_Erro:
    Call Err.Raise(Err.Number, , "RSGL0002.cls00129.Excluir - " & Err.Description)
End Function

Public Function Consultar(ByVal sSiglaSistema As String, _
                          ByVal sSiglaRecurso As String, _
                          ByVal sDescricaoRecurso As String, _
                          ByVal lAmbienteID As Long, _
                          ByVal iTpPesquisa As Integer, _
                          ByVal sTexto As String) As Object
    Dim sSQL As String

    'itpPesquisa
    '0 - Todos
    '1 - C�digo
    '2 - Nome
    On Error GoTo Trata_Erro
    sSQL = ""
    adSQL sSQL, "SELECT *"
    adSQL sSQL, "FROM ressegurador_tb "

    Select Case iTpPesquisa
        Case 1
            adSQL sSQL, "WHERE ressegurador_id = " & Val(sTexto)
        Case 2
            adSQL sSQL, "WHERE nome like '" & MudaAspaSimples(sTexto) & "%'"
    End Select

    adSQL sSQL, "ORDER BY nome"
    Set Consultar = ExecutarSQL(sSiglaSistema, lAmbienteID, sSiglaRecurso, sDescricaoRecurso, sSQL, True)
    Exit Function
Trata_Erro:
    Call Err.Raise(Err.Number, , "RSGL0002.cls00129.Consultar - " & Err.Description)
End Function

Public Function ConsultarCategoria(ByVal sSiglaSistema As String, _
                                   ByVal sSiglaRecurso As String, _
                                   ByVal sDescricaoRecurso As String, _
                                   ByVal lAmbienteID As Long, _
                                   ByVal lCategoriaId As Integer, _
                                   ByVal sCategoriaNome As String) As Object
    Dim sSQL As String

    On Error GoTo Trata_Erro
    sSQL = ""
    adSQL sSQL, "SELECT * "
    adSQL sSQL, "FROM categoria_ressegurador_tb "

    If lCategoriaId <> 0 Then
        adSQL sSQL, "WHERE categoria_id = " & Val(lCategoriaId)
    End If

    If sCategoriaNome <> "" Then
        adSQL sSQL, "WHERE categoria_nome like '" & MudaAspaSimples(sCategoriaNome) & "%'"
    End If

    adSQL sSQL, "ORDER BY categoria_nome"
    Set ConsultarCategoria = ExecutarSQL(sSiglaSistema, lAmbienteID, sSiglaRecurso, sDescricaoRecurso, sSQL, True)
    Exit Function
Trata_Erro:
    Call Err.Raise(Err.Number, , "RSGL0002.cls00129.ConsultarCategoria - " & Err.Description)
End Function
