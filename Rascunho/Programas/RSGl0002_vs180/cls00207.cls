VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cls00207"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True

Public Function Alterar(ByVal sSiglaSistema As String, _
                        ByVal sSiglaRecurso As String, _
                        ByVal sDescricaoRecurso As String, _
                        ByVal lAmbienteID As Long, _
                        ByVal iModalidadeId As Integer, _
                        ByVal sNome As String, _
                        ByVal iRamoIRBID As Integer, _
                        ByVal sCodModalidade As String, _
                        ByVal sClassificacao As String, _
                        ByVal sUsuario As String) As Boolean

    'Rotina destinada a alterar dados da tabela modalidade_tb'''''
    On Error GoTo TratarErro
    Dim sSQL As String

    sSQL = "EXEC modalidade_spu "
    adSQL sSQL, iModalidadeId & ", "
    adSQL sSQL, iRamoIRBID & ", "
    adSQL sSQL, "'" & sCodModalidade & "',"
    adSQL sSQL, "'" & sNome & "', "
    adSQL sSQL, "'" & sClassificacao & "', "
    adSQL sSQL, "'" & sUsuario & "'"
    Call ExecutarSQL(sSiglaSistema, lAmbienteID, sSiglaRecurso, sDescricaoRecurso, sSQL, False)
    Alterar = True
    Exit Function
TratarErro:
    Call Err.Raise(Err.Number, , "RSGL0002.cls00207.AlterarModalidade- " & Err.Description)
End Function

Public Function Consultar(ByVal sSiglaSistema As String, _
                          ByVal sSiglaRecurso As String, _
                          ByVal sDescricaoRecurso As String, _
                          ByVal lAmbienteID As Long, _
                          ByVal iRamoIRB As Integer, _
                          ByVal sCodModalidade As String, _
                          ByVal sClassificacao As String, _
                          ByVal sNomeMod As String, _
                          ByVal iModalidadeId As Integer) As Object
    'Rotina para consulta da tabela modalidade_tb
    Dim sSQL      As String

    Dim sNomeRamo As String

    On Error GoTo TratarErro
    sSQL = ""
    adSQL sSQL, "SELECT ramo_irb_tb.ramo_irb_id, "
    adSQL sSQL, "       sNomeRamo = ramo_irb_tb.nome, "
    adSQL sSQL, "       modalidade_tb.cod_modalidade , "
    adSQL sSQL, "       modalidade_tb.nome, "
    adSQL sSQL, "       modalidade_tb.classificacao_risco, "
    adSQL sSQL, "       modalidade_tb.modalidade_id "
    adSQL sSQL, "  FROM modalidade_tb "
    adSQL sSQL, "  JOIN ramo_irb_tb "
    adSQL sSQL, "    ON ramo_irb_tb.ramo_irb_id = modalidade_tb.ramo_irb_id"
    adSQL sSQL, " WHERE 1 = 1 "

    If iRamoIRB > 0 Then
        adSQL sSQL, " AND modalidade_tb.ramo_irb_id = " & iRamoIRB
    End If

    If sCodModalidade <> "" Then
        adSQL sSQL, " AND modalidade_tb.cod_modalidade like '" & sCodModalidade & "%'"
    End If

    If sNomeMod <> "" Then
        adSQL sSQL, " AND modalidade_tb.nome like '" & sNomeMod & "%'"
    End If

    If iModalidadeId > 0 Then
        adSQL sSQL, " AND modalidade_tb.modalidade_id = " & iModalidadeId
    End If

    If sClassificacao <> "" Then
        adSQL sSQL, " AND modalidade_tb.classificacao_risco like '" & sClassificacao & "%'"
    End If

    adSQL sSQL, "   ORDER BY ramo_irb_tb.ramo_irb_id"
    Set Consultar = ExecutarSQL(sSiglaSistema, lAmbienteID, sSiglaRecurso, sDescricaoRecurso, sSQL, True)
    Exit Function
TratarErro:
    Call Err.Raise(Err.Number, , "RSGL0002.cls00207.Consultar - " & Err.Description)
End Function

Public Function Excluir(ByVal sSiglaSistema As String, _
                        ByVal sSiglaRecurso As String, _
                        ByVal sDescricaoRecurso As String, _
                        ByVal lAmbienteID As Long, _
                        ByVal iModalidadeId As Integer) As Boolean

    ' Rotina de dele��o de dados da tabela modalidade_tb
    On Error GoTo TratarErro
    Dim sSQL As String

    sSQL = "EXEC modalidade_spd " & iModalidadeId
    Call ExecutarSQL(sSiglaSistema, lAmbienteID, sSiglaRecurso, sDescricaoRecurso, sSQL, False)
    Excluir = True
    Exit Function
TratarErro:
    Call Err.Raise(Err.Number, , "RSGL0002.cls00207.ExcluirModalidade" & Err.Description)
End Function

Public Function Incluir(ByVal sSiglaSistema As String, _
                        ByVal sSiglaRecurso As String, _
                        ByVal sDescricaoRecurso As String, _
                        ByVal lAmbienteID As Long, _
                        ByVal sCodModalidade As String, _
                        ByVal sNome As String, _
                        ByVal iRamoId As Integer, _
                        ByVal sClassificacao As String, _
                        ByVal sUsuario As String)

    'Rotina de inclus�o de dados na modalidade_tb
    On Error GoTo TratarErro
    Dim sSQL       As String

    Dim oResultado As Object

    sSQL = "EXEC modalidade_spi "
    adSQL sSQL, iRamoId & ", "
    adSQL sSQL, "'" & sCodModalidade & "', "
    adSQL sSQL, "'" & sNome & "', "
    adSQL sSQL, "'" & sClassificacao & "', "
    adSQL sSQL, "'" & sUsuario & "'"
    Call ExecutarSQL(sSiglaSistema, lAmbienteID, sSiglaRecurso, sDescricaoRecurso, sSQL, False)
    Exit Function
TratarErro:
    Call Err.Raise(Err.Number, , "RSGL0002.cls00207.IncluirModalidade - " & Err.Description)
End Function
