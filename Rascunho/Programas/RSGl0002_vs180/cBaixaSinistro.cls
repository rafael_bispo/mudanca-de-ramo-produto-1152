VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cBaixaSinistro"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True

Private mvarSinistroRecuperadoId As Long
Private mvarSinistroPrevisaoRecuperacaoId As Long
Private mvarValRecuperado As Double
Private mvarValDiferenca As Double
Private mvarTpDiferencaRepasseId As Integer
Private mvarEstorno As String
Private mvarDtPagamento As String


Public Property Get DtPagamento() As String
      DtPagamento = mvarDtPagamento
End Property

Public Property Let DtPagamento(ByVal vData As String)
   mvarDtPagamento = vData
End Property
 
Public Property Get Estorno() As String
      Estorno = mvarEstorno
End Property

Public Property Let Estorno(ByVal vData As String)
   mvarEstorno = vData
End Property
 

Public Property Get ValDiferenca() As Double
      ValDiferenca = mvarValDiferenca
End Property

Public Property Let ValDiferenca(ByVal vData As Double)
   mvarValDiferenca = vData
End Property
 
Public Property Get TpDiferencaRepasseId() As Integer
    TpDiferencaRepasseId = mvarTpDiferencaRepasseId
End Property

Public Property Let TpDiferencaRepasseId(ByVal vData As Integer)
   mvarTpDiferencaRepasseId = vData
End Property
 

Public Property Get ValRecuperado() As Double
      ValRecuperado = mvarValRecuperado
End Property

Public Property Let ValRecuperado(ByVal vData As Double)
   mvarValRecuperado = vData
End Property

Public Property Get SinistroPrevisaoRecuperacaoId() As Long
     SinistroPrevisaoRecuperacaoId = mvarSinistroPrevisaoRecuperacaoId
End Property

Public Property Let SinistroPrevisaoRecuperacaoId(ByVal vData As Long)
   mvarSinistroPrevisaoRecuperacaoId = vData
End Property

Public Property Get SinistroRecuperadoId() As Long
    SinistroRecuperadoId = mvarSinistroRecuperadoId
End Property

Public Property Let SinistroRecuperadoId(ByVal vData As Long)
   mvarSinistroRecuperadoId = vData
End Property

