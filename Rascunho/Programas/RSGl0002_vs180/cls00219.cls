VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cls00219"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True

Public Function ConsultarModalidadeRamoIRB(ByVal sSiglaSistema As String, _
                                           ByVal sSiglaRecurso As String, _
                                           ByVal sDescricaoRecurso As String, _
                                           ByVal lAmbienteID As Long, _
                                           Optional ByVal lRamoIRBID As Long, _
                                           Optional ByVal lModalidadeID As Long) As Object
    '## Rotina de consulta das modalidades para um ramo IRB
    Dim sSQL As String

    On Error GoTo Trata_Erro
    sSQL = ""
    adSQL sSQL, "   SELECT ramo_irb_tb.ramo_irb_id,"
    adSQL sSQL, "          ramo_irb_tb.nome nome_ramo,"
    adSQL sSQL, "          modalidade_tb.modalidade_id,"
    adSQL sSQL, "          modalidade_tb.cod_modalidade,"
    adSQL sSQL, "          modalidade_tb.classificacao_risco,"
    adSQL sSQL, "          modalidade_tb.nome nome_modalidade"
    adSQL sSQL, "     FROM modalidade_tb"
    adSQL sSQL, "LEFT JOIN ramo_irb_tb"
    adSQL sSQL, "       ON ramo_irb_tb.ramo_irb_id = modalidade_tb.ramo_irb_id"

    If lRamoIRBID <> 0 Then
        adSQL sSQL, "    WHERE ramo_irb_tb.ramo_irb_id = " & lRamoIRBID

        If lModalidadeID > 0 Then
            adSQL sSQL, "      AND modalidade_tb.modalidade_id = " & lModalidadeID
        End If
    End If

    adSQL sSQL, "ORDER BY ramo_irb_tb.ramo_irb_id"
    Set ConsultarModalidadeRamoIRB = ExecutarSQL(sSiglaSistema, lAmbienteID, sSiglaRecurso, sDescricaoRecurso, sSQL, True)
    Exit Function
Trata_Erro:
    Call Err.Raise(Err.Number, , "RESL0001.cls00000.ConsultarModalidadesRamoIRB - " & Err.Description)
End Function

Public Function ConsultarModalidade(ByVal sSiglaSistema As String, _
                                    ByVal sSiglaRecurso As String, _
                                    ByVal sDescricaoRecurso As String, _
                                    ByVal iAmbienteId As Integer, _
                                    Optional ByVal lModalidadeID As Long) As Object
    '## Rotina de consulta em modalidade_tb
    Dim sSQL As String

    On Error GoTo Trata_Erro
    sSQL = ""
    sSQL = sSQL & "SELECT * " & vbNewLine
    sSQL = sSQL & "  FROM modalidade_tb " & vbNewLine

    If lModalidadeID > 0 Then
        sSQL = sSQL & " WHERE modalidade_id = " & lModalidadeID & vbNewLine
    End If

    sSQL = sSQL & " ORDER BY modalidade_id"
    Set ConsultarModalidade = ExecutarSQL(sSiglaSistema, iAmbienteId, sSiglaRecurso, sDescricaoRecurso, sSQL, True)
    Exit Function
Trata_Erro:
    Call Err.Raise(Err.Number, , "SEGL0022.cls00117.ConsultarRamoIRB - " & Err.Description)
End Function

Public Function ConsultarFormaPagamento(ByVal sSiglaSistema As String, _
                                        ByVal sSiglaRecurso As String, _
                                        ByVal sDescricaoRecurso As String, _
                                        ByVal lAmbienteID As Long, _
                                        ByVal iTpPesquisa As Integer, _
                                        Optional ByVal lFormaPagamentoId As Long, _
                                        Optional ByVal lRamoIRBID As Long, _
                                        Optional ByVal lModalidadeID As Long, _
                                        Optional ByVal lCodPlano As Long) As Object
    '## Rotina de consulta em forma_pagamento
    Dim sSQL As String

    On Error GoTo Trata_Erro
    'Tipo de consulta
    '1 - Por forma_pagamento_id
    '2 - Por ramo_irb_id, modalidade_id, cod_plano
    sSQL = ""
    adSQL sSQL, "   SELECT *"
    adSQL sSQL, "     FROM forma_pagamento_tb (NOLOCK)"

    If iTpPesquisa = 1 Then
        adSQL sSQL, "    WHERE forma_pagamento_tb.forma_pagamento_id = " & lFormaPagamentoId
    Else
        adSQL sSQL, "    WHERE forma_pagamento_tb.ramo_irb_id = " & lRamoIRBID
        adSQL sSQL, "      AND ISNULL(forma_pagamento_tb.modalidade_id,0) = " & lModalidadeID

        If lCodPlano > 0 Then
            adSQL sSQL, "      AND forma_pagamento_tb.cod_plano = " & lCodPlano
        End If
    End If

    adSQL sSQL, "ORDER BY forma_pagamento_tb.ramo_irb_id"
    Set ConsultarFormaPagamento = ExecutarSQL(sSiglaSistema, lAmbienteID, sSiglaRecurso, sDescricaoRecurso, sSQL, True)
    Exit Function
Trata_Erro:
    Call Err.Raise(Err.Number, , "RESL0001.cls00000.ConsultarFormaPagamento - " & Err.Description)
End Function

Public Function ConsultarRamoModalidadeComFormaPagamento(ByVal sSiglaSistema As String, _
                                                         ByVal sSiglaRecurso As String, _
                                                         ByVal sDescricaoRecurso As String, _
                                                         ByVal lAmbienteID As Long, _
                                                         ByVal iTpPesquisa As Integer, _
                                                         Optional ByVal sDataSistema As String, _
                                                         Optional ByVal lFormaPagamentoId As Long, _
                                                         Optional ByVal lRamoIRBID As Long, _
                                                         Optional ByVal lModalidadeID As Long, _
                                                         Optional ByVal sCodModalidade As String, _
                                                         Optional ByVal lCodPlano As Long) As Object
    '## Rotina de consulta em forma_pagamento
    Dim sSQL As String

    On Error GoTo Trata_Erro
    'Tipo de consulta:
    '1 - Por forma_pagamento_id
    '2 - Por ramo_irb_id, modalidade_id, cod_modalidade, cod_plano
    sSQL = ""
    adSQL sSQL, "SELECT DISTINCT forma_pagamento_tb.ramo_irb_id,"
    adSQL sSQL, "          forma_pagamento_tb.modalidade_id,"
    adSQL sSQL, "          ramo_irb_tb.nome nome_ramo,"
    adSQL sSQL, "          modalidade_tb.nome nome_modalidade,"
    adSQL sSQL, "          modalidade_tb.cod_modalidade,"
    adSQL sSQL, "          modalidade_tb.classificacao_risco"
    adSQL sSQL, "     FROM forma_pagamento_tb(NOLOCK)"
    adSQL sSQL, "     JOIN ramo_irb_tb(NOLOCK)"
    adSQL sSQL, "       ON ramo_irb_tb.ramo_irb_id = forma_pagamento_tb.ramo_irb_id"
    adSQL sSQL, "LEFT JOIN modalidade_tb (NOLOCK)"
    adSQL sSQL, "       ON modalidade_tb.modalidade_id = forma_pagamento_tb.modalidade_id"
    adSQL sSQL, "    WHERE (dt_inicio_vigencia <= '" & sDataSistema & "'"
    adSQL sSQL, "           AND (dt_fim_vigencia IS NULL"
    adSQL sSQL, "                OR dt_fim_vigencia >= '" & sDataSistema & "'))"

    If iTpPesquisa = 1 Then
        adSQL sSQL, "          AND forma_pagamento_tb.forma_pagamento_id =  & lFormaPagamentoId"
    Else

        If lRamoIRBID > 0 Then
            adSQL sSQL, "        AND forma_pagamento_tb.ramo_irb_id = " & lRamoIRBID
        End If

        If lModalidadeID > O Then
            adSQL sSQL, "        AND forma_pagamento_tb.modalidade_id = " & lModalidadeID
        End If

        If sCodModalidade <> "" Then
            adSQL sSQL, "        AND modalidade_tb.cod_modalidade = '" & sCodModalidade & "'"
        End If

        If lCodPlano > 0 Then
            adSQL sSQL, "     AND forma_pagamento_tb.cod_plano = " & lCodPlano
        End If
    End If

    adSQL sSQL, "ORDER BY forma_pagamento_tb.ramo_irb_id"
    Set ConsultarRamoModalidadeComFormaPagamento = ExecutarSQL(sSiglaSistema, lAmbienteID, sSiglaRecurso, sDescricaoRecurso, sSQL, True)
    Exit Function
Trata_Erro:
    Call Err.Raise(Err.Number, , "RESL0001.cls00000.ConsultarRamoModalidadeComFormaPagamento - " & Err.Description)
End Function

Public Function Consultar(ByVal sSiglaSistema As String, _
                          ByVal sSiglaRecurso As String, _
                          ByVal sDescricaoRecurso As String, _
                          ByVal lAmbienteID As Long, _
                          ByVal iTpPesquisa As Integer, _
                          Optional ByVal sDataSistema As String, _
                          Optional ByVal lFormaPagamentoId As Long, _
                          Optional ByVal lRamoIRBID As Long, _
                          Optional ByVal lModalidadeID As Long, _
                          Optional ByVal lCodPlano As Long) As Object
    '## Rotina de consulta em forma_pagamento
    Dim sSQL As String

    On Error GoTo Trata_Erro
    'Tipo de consulta:
    '1 - Por forma_pagamento_id
    '2 - Por ramo_irb_id, modalidade_id, cod_plano
    sSQL = ""
    adSQL sSQL, "   SELECT forma_pagamento_tb.*,"
    adSQL sSQL, "          ramo_irb_tb.nome nome_ramo,"
    adSQL sSQL, "          modalidade_tb.nome nome_modalidade,"
    adSQL sSQL, "          modalidade_tb.cod_modalidade,"
    adSQL sSQL, "          modalidade_tb.classificacao_risco, "
    adSQL sSQL, "           plano_tb.nome nome_plano "
    adSQL sSQL, "     FROM forma_pagamento_tb "
    adSQL sSQL, "     JOIN ramo_irb_tb "
    adSQL sSQL, "       ON ramo_irb_tb.ramo_irb_id = forma_pagamento_tb.ramo_irb_id "
    adSQL sSQL, "LEFT JOIN modalidade_tb "
    adSQL sSQL, "       ON modalidade_tb.modalidade_id = forma_pagamento_tb.modalidade_id"
    adSQL sSQL, "     JOIN plano_tb "
    adSQL sSQL, "       ON plano_tb.cod_plano = forma_pagamento_tb.cod_plano"
    adSQL sSQL, "    WHERE (dt_inicio_vigencia <= '" & sDataSistema & "'"
    adSQL sSQL, "      AND (dt_fim_vigencia IS NULL"
    adSQL sSQL, "          OR dt_fim_vigencia >= '" & sDataSistema & "'))"

    If iTpPesquisa = 1 Then
        adSQL sSQL, "   AND forma_pagamento_tb.forma_pagamento_id = " & lFormaPagamentoId
    Else

        If lRamoIRBID <> 0 Then
            adSQL sSQL, "      AND forma_pagamento_tb.ramo_irb_id = " & lRamoIRBID

            If lModalidadeID > 0 Then
                adSQL sSQL, "     AND forma_pagamento_tb.modalidade_id = " & lModalidadeID
            Else
                adSQL sSQL, "     AND forma_pagamento_tb.modalidade_id Is Null"
            End If

            If lCodPlano > 0 Then
                adSQL sSQL, "     AND forma_pagamento_tb.cod_plano =  " & lCodPlano
            End If
        End If
    End If

    adSQL sSQL, "ORDER BY forma_pagamento_tb.ramo_irb_id,"
    adSQL sSQL, "         forma_pagamento_tb.modalidade_id,"
    adSQL sSQL, "         forma_pagamento_tb.cod_plano"
    Set Consultar = ExecutarSQL(sSiglaSistema, lAmbienteID, sSiglaRecurso, sDescricaoRecurso, sSQL, True)
    Exit Function
Trata_Erro:
    Call Err.Raise(Err.Number, , "RESL0001.cls00000.Consultar - " & Err.Description)
End Function

Sub Excluir(ByVal sSiglaSistema As String, _
            ByVal sSiglaRecurso As String, _
            ByVal sDescricaoRecurso As String, _
            ByVal iAmbienteId As Integer, _
            ByVal Pagamentos As Collection, _
            ByVal sDtInicioVigencia As String, _
            ByVal lRamoIRBID As Long, _
            ByVal lModalidadeID As Long, _
            ByVal sUsuario As String)
    Dim Pagamento As Object

    Dim lConexao  As Long

    On Error GoTo Trata_Erro:
    lConexao = AbrirConexao(sSiglaSistema, iAmbienteId, sSiglaRecurso, sDescricaoRecurso)

    If Not AbrirTransacao(lConexao) Then
        Call Err.Raise(vbObjectError + 1000, , "RSGL0001.cls00138.AtualizaResseguroVida_Premio - N�o foi poss�vel abrir transa��o")
    End If

    For Each Pagamento In Pagamentos

        If lRamoIRBID = Pagamento.ramo_irb_id And lModalidadeID = Pagamento.modalidade_id Then
            Call FecharVigencia(sSiglaSistema, sSiglaRecurso, sDescricaoRecurso, iAmbienteId, lConexao, Pagamento.ramo_irb_id, Pagamento.modalidade_id, Pagamento.cod_plano, CStr(CLng(sDtInicioVigencia) - 1), sUsuario)
        End If

    Next

    'Fechando Transacao ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    Call ConfirmarTransacao(lConexao)
    Exit Sub
Trata_Erro:
    Call Err.Raise(Err.Number, , "RSGL0001.cls00127.Excluir - " & Err.Description)
End Sub

Sub InserirPagamento(ByVal sSiglaSistema As String, _
                     ByVal sSiglaRecurso As String, _
                     ByVal sDescricaoRecurso As String, _
                     ByVal iAmbienteId As Integer, _
                     ByVal lConexao As Long, _
                     ByVal lRamoIRBID As Long, _
                     ByVal lModalidadeID As Long, _
                     ByVal lCodPlano As Long, _
                     ByVal sFormaPagamento As String, _
                     ByVal iNumMesesPagamento As Integer, _
                     ByVal iNumMaxparcelas As Integer, _
                     ByVal sDtInicioVigencia As String, _
                     ByVal sUsuario As String)
    '## Rotina de inclus�o em forma_pagamento_tb
    Dim sSQL As String

    On Error GoTo Trata_Erro:
    sSQL = ""
    sSQL = "EXEC forma_pagamento_spi "
    sSQL = sSQL & lRamoIRBID & ", "

    If lModalidadeID > 0 Then
        sSQL = sSQL & lModalidadeID & ", "
    Else
        sSQL = sSQL & "NULL, "
    End If

    sSQL = sSQL & lCodPlano & ", '"
    sSQL = sSQL & sFormaPagamento & "', "
    sSQL = sSQL & iNumMesesPagamento & ", "
    sSQL = sSQL & iNumMaxparcelas & ", '"
    sSQL = sSQL & sDtInicioVigencia & "', '"
    sSQL = sSQL & sUsuario & "' "
    Call Conexao_ExecutarSQL(sSiglaSistema, iAmbienteId, sSiglaRecurso, sDescricaoRecurso, sSQL, lConexao, False, False, 30000, adLockOptimistic, adUseClient)
    Exit Sub
Trata_Erro:
    Call Err.Raise(Err.Number, , "RSGL0001.forma_pagamento.InserirPagamento - " & Err.Description)
End Sub

Sub Alterar(ByVal sSiglaSistema As String, _
            ByVal sSiglaRecurso As String, _
            ByVal sDescricaoRecurso As String, _
            ByVal iAmbienteId As Integer, _
            ByVal Pagamentos As Collection, _
            ByVal sDtInicioVigencia As String, _
            ByVal sUsuario As String)
    '## Rotina para atualizar os dados de forma de pagamento de um ramo_irb / modalidade
    Dim Pagamento As Object

    Dim lConexao  As Long

    On Error GoTo Trata_Erro:
    lConexao = AbrirConexao(sSiglaSistema, iAmbienteId, sSiglaRecurso, sDescricaoRecurso)

    If Not AbrirTransacao(lConexao) Then
        Call Err.Raise(vbObjectError + 1000, , "RSGL0001.cls00138.AtualizaResseguroVida_Premio - N�o foi poss�vel abrir transa��o")
    End If

    For Each Pagamento In Pagamentos

        Select Case Pagamento.operacao
            Case "I"
                Call InserirPagamento(sSiglaSistema, sSiglaRecurso, sDescricaoRecurso, iAmbienteId, lConexao, Pagamento.ramo_irb_id, Pagamento.modalidade_id, Pagamento.cod_plano, Pagamento.forma_pagamento, Pagamento.num_meses_pagamento, Pagamento.num_maximo_parcelas, sDtInicioVigencia, sUsuario)
            Case "A"

                Select Case Pagamento.origem_base
                    Case "S" 'Forma de pagamento j� existente

                        'Verificando se foi feita alguma altera��o na forma de pagamento '''
                        If VerificarAlteracao(sSiglaSistema, sSiglaRecurso, sDescricaoRecurso, iAmbienteId, Pagamento.ramo_irb_id, Pagamento.modalidade_id, Pagamento.cod_plano, Pagamento.forma_pagamento, Pagamento.num_meses_pagamento, Pagamento.num_maximo_parcelas) Then
                            'Fechando vig�ncia da forma de pagamento ''''''''''''''''''''''
                            Call FecharVigencia(sSiglaSistema, sSiglaRecurso, sDescricaoRecurso, iAmbienteId, lConexao, Pagamento.ramo_irb_id, Pagamento.modalidade_id, Pagamento.cod_plano, CStr(CLng(sDtInicioVigencia) - 1), sUsuario)
                            'Incluindo nova forma de pagamento '''''''''''''''''''''''''''
                            Call InserirPagamento(sSiglaSistema, sSiglaRecurso, sDescricaoRecurso, iAmbienteId, lConexao, Pagamento.ramo_irb_id, Pagamento.modalidade_id, Pagamento.cod_plano, Pagamento.forma_pagamento, Pagamento.num_meses_pagamento, Pagamento.num_maximo_parcelas, sDtInicioVigencia, sUsuario)
                        End If

                    Case "N" 'Nova forma de pagamento
                        Call InserirPagamento(sSiglaSistema, sSiglaRecurso, sDescricaoRecurso, iAmbienteId, lConexao, Pagamento.ramo_irb_id, Pagamento.modalidade_id, Pagamento.cod_plano, Pagamento.forma_pagamento, Pagamento.num_meses_pagamento, Pagamento.num_maximo_parcelas, sDtInicioVigencia, sUsuario)
                End Select

            Case "E"

                If Pagamento.origem_base = "S" Then
                    'Fechando vig�ncia da forma de pagamento ''''''''''''''''''''''
                    Call FecharVigencia(sSiglaSistema, sSiglaRecurso, sDescricaoRecurso, iAmbienteId, lConexao, Pagamento.ramo_irb_id, Pagamento.modalidade_id, Pagamento.cod_plano, CStr(CLng(sDtInicioVigencia) - 1), sUsuario)
                End If

        End Select

    Next

    'Fechando Transacao ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    Call ConfirmarTransacao(lConexao)
    Exit Sub
Trata_Erro:
    Call Err.Raise(Err.Number, , "RSGL0001.cls00127.AlterarContratoPagamento - " & Err.Description)
End Sub

Function VerificarAlteracao(ByVal sSiglaSistema As String, _
                            ByVal sSiglaRecurso As String, _
                            ByVal sDescricaoRecurso As String, _
                            ByVal iAmbienteId As Integer, _
                            ByVal lRamoIRBID As Long, _
                            ByVal lModalidadeID As Long, _
                            ByVal lPlanoID As Long, _
                            ByVal sFormaPagamento As String, _
                            ByVal iNumMesesPagamento As Integer, _
                            ByVal iNumMaximoParcelas As Integer) As Boolean
    Dim rs As Recordset

    'True  - Alterou algum dado do pagamento
    'False - N�o alterou os dados do pagamento
    On Error GoTo Trata_Erro
    'Iniciando valor da fun��o '''''''''''''''''''''''''
    VerificarAlteracao = False
    Set rs = ConsultarFormaPagamento(sSiglaSistema, sSiglaRecurso, sDescricaoRecurso, iAmbienteId, 2, , lRamoIRBID, lModalidadeID, lPlanoID)

    If Not rs.EOF Then
        If sFormaPagamento <> rs("forma_pagamento") Or iNumMesesPagamento <> rs("num_meses_para_pagamento") Or iNumMaximoParcelas <> rs("num_maximo_parcelas") Then
            VerificarAlteracao = True
        End If
    End If

    Exit Function
Trata_Erro:
    Call Err.Raise(Err.Number, , "RSGL0001.forma_pagamento.VerificarAlteracao - " & Err.Description)
End Function

Sub FecharVigencia(ByVal sSiglaSistema As String, _
                   ByVal sSiglaRecurso As String, _
                   ByVal sDescricaoRecurso As String, _
                   ByVal iAmbienteId As Integer, _
                   ByVal lConexao As Long, _
                   ByVal lRamoIRBID As Long, _
                   ByVal lModalidadeID As Long, _
                   ByVal lPlanoID As Long, _
                   ByVal sdtFimVigencia As String, _
                   ByVal sUsuario As String)
    '## Rotina de inclus�o em forma_pagamento_tb
    Dim sSQL As String

    On Error GoTo Trata_Erro:
    sSQL = ""
    sSQL = "EXEC fechar_vigencia_forma_pagamento_spu "
    sSQL = sSQL & lRamoIRBID & ", "
    sSQL = sSQL & lModalidadeID & ", "
    sSQL = sSQL & lPlanoID & ", '"
    sSQL = sSQL & sdtFimVigencia & "', '"
    sSQL = sSQL & sUsuario & "' "
    Call Conexao_ExecutarSQL(sSiglaSistema, iAmbienteId, sSiglaRecurso, sDescricaoRecurso, sSQL, lConexao, False, False, 30000, adLockOptimistic, adUseClient)
    Exit Sub
Trata_Erro:
    Call Err.Raise(Err.Number, , "RSGL0001.forma_pagamento.FecharVigencia - " & Err.Description)
End Sub
