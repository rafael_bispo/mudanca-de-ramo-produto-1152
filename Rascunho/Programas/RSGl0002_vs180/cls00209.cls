VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cls00209"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Public Function Pesquisar(ByVal sSiglaSistema As String, _
                          ByVal sSiglaRecurso As String, _
                          ByVal sDescricaoRecurso As String, _
                          ByVal iAmbienteId As Integer, _
                          ByVal iTpConsulta As Integer, _
                          ByVal sConteudo As String) As Object
    Dim sSQL As String

    On Error GoTo Trata_Erro
    'iTpConsulta
    '0 - Todos
    '1 - C�digo
    '2 - Ramo IRB
    sSQL = ""
    adSQL sSQL, "  SELECT ramo_irb_id,"
    adSQL sSQL, "         nome"
    adSQL sSQL, "    FROM ramo_irb_tb"

    Select Case iTpConsulta
        Case 1 'C�digo
            adSQL sSQL, "   WHERE ramo_irb_id = " & Val(sConteudo)
        Case 2 'Ramo IRB
            adSQL sSQL, "   WHERE nome like '" & sConteudo & "%'"
    End Select

    adSQL sSQL, "ORDER BY ramo_irb_id"
    Set Pesquisar = ExecutarSQL(sSiglaSistema, iAmbienteId, sSiglaRecurso, sDescricaoRecurso, sSQL)
    Exit Function
Trata_Erro:
    Call Err.Raise(Err.Number, , "RSGL0002.cls00209.Pesquisar - " & Err.Description)
End Function
                            
Public Sub Incluir(ByVal sSiglaSistema As String, _
                   ByVal sSiglaRecurso As String, _
                   ByVal sDescricaoRecurso As String, _
                   ByVal iAmbienteId As Integer, _
                   ByVal lCodigoRamoIRB As Long, _
                   ByVal sNomeRamoIRB As String, _
                   ByVal sUsuario As String)
    Dim sSQL As String

    On Error GoTo Trata_Erro
    sSQL = ""
    adSQL sSQL, "EXEC ramo_irb_spi"
    adSQL sSQL, "" & lCodigoRamoIRB
    adSQL sSQL, ",'" & sNomeRamoIRB & "'"
    adSQL sSQL, ",'" & sUsuario & "'"
    Call ExecutarSQL(sSiglaSistema, iAmbienteId, sSiglaRecurso, sDescricaoRecurso, sSQL, False)
    Exit Sub
Trata_Erro:
    Call Err.Raise(Err.Number, , "RSGL0002.cls00209.Incluir - " & Err.Description)
End Sub

Public Sub Alterar(ByVal sSiglaSistema As String, _
                   ByVal sSiglaRecurso As String, _
                   ByVal sDescricaoRecurso As String, _
                   ByVal iAmbienteId As Integer, _
                   ByVal lCodigoRamoIRB As Long, _
                   ByVal sNomeRamoIRB As String, _
                   ByVal sUsuario As String)
    Dim sSQL As String

    On Error GoTo Trata_Erro
    sSQL = ""
    adSQL sSQL, "EXEC ramo_irb_spu"
    adSQL sSQL, "" & lCodigoRamoIRB
    adSQL sSQL, ",'" & sNomeRamoIRB & "'"
    adSQL sSQL, ",'" & sUsuario & "'"
    Call ExecutarSQL(sSiglaSistema, iAmbienteId, sSiglaRecurso, sDescricaoRecurso, sSQL, False)
    Exit Sub
Trata_Erro:
    Call Err.Raise(Err.Number, , "RSGL0002.cls002090.Alterar - " & Err.Description)
End Sub

Public Sub Excluir(ByVal sSiglaSistema As String, _
                   ByVal sSiglaRecurso As String, _
                   ByVal sDescricaoRecurso As String, _
                   ByVal iAmbienteId As Integer, _
                   ByVal lCodigoRamoIRB As Long)
    Dim sSQL As String

    On Error GoTo Trata_Erro
    sSQL = ""
    adSQL sSQL, "EXEC ramo_irb_spd"
    adSQL sSQL, "" & lCodigoRamoIRB
    Call ExecutarSQL(sSiglaSistema, iAmbienteId, sSiglaRecurso, sDescricaoRecurso, sSQL, False)
    Exit Sub
Trata_Erro:
    Call Err.Raise(Err.Number, , "RSGL0002.cls002090.Excluir - " & Err.Description)
End Sub
