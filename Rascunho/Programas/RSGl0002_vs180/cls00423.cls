VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cls00423"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Public Function CriarTabelasTemporariaCalculo(ByVal sSiglaSistema As String, _
                                              ByVal sSiglaRecurso As String, _
                                              ByVal sDescricaoRecurso As String, _
                                              ByVal iAmbienteId As Integer, _
                                              ByRef lConexao As Long) As Object
                                 
    Dim sSQL As String
    Dim oRs As Recordset
    Dim proposta_id As Long
    Dim num_endosso As Integer
    
    On Error GoTo Trata_Erro

    lConexao = AbrirConexao("SEGBR", iAmbienteId, "RSGP0006", sSiglaRecurso)
    
    sSQL = ""
    adSQL sSQL, "CREATE TABLE #Objeto_Contrato("
    adSQL sSQL, "   proposta_id numeric(9, 0) NULL,"
    adSQL sSQL, "   endosso_id int NULL,"
    adSQL sSQL, "   dt_emissao smalldatetime NULL,"
    adSQL sSQL, "   dt_inicio_vigencia smalldatetime NULL,"
    adSQL sSQL, "   dt_ini_vigencia_proposta smalldatetime NULL,"
    adSQL sSQL, "   dt_fim_vigencia_proposta smalldatetime NULL,"
    adSQL sSQL, "   seguro_moeda_id numeric(3, 0) NULL,"
    adSQL sSQL, "   tp_documento varchar(20) NULL,"
    adSQL sSQL, "   cod_objeto_segurado int NULL,"
    adSQL sSQL, "   val_premio_tarifa numeric(38, 2) NULL,"
    adSQL sSQL, "   val_premio_net numeric(38, 6) NULL,"
    adSQL sSQL, "   val_is numeric(38, 2) NULL,"
    adSQL sSQL, "   val_is_qualquer numeric(38, 2) NULL,"
    adSQL sSQL, "   apolice_id numeric(9, 0) NULL,"
    adSQL sSQL, "   ramo_id int NULL,"
    adSQL sSQL, "   sucursal_seguradora_id numeric(5, 0) NULL,"
    adSQL sSQL, "   seguradora_cod_susep numeric(5, 0) NULL,"
    adSQL sSQL, "   ramo_irb_id int NULL,"
    adSQL sSQL, "   contrato_id int NOT NULL,"
    adSQL sSQL, "   num_versao int NOT NULL,"
    adSQL sSQL, "   moeda_contrato numeric(3, 0) NOT NULL,"
    adSQL sSQL, "   premio_a_ser_utilizado varchar(2) NULL,"
    adSQL sSQL, "   permite_er_quota_simultaneamente char(1) NULL,"
    adSQL sSQL, "   val_IS_maior_cobertura numeric(15, 2) NULL,"
    adSQL sSQL, "   val_premio_diferenciado numeric(38, 8) NULL,"
    adSQL sSQL, "   classificacao_contrato char(1) NULL,"
    adSQL sSQL, "   perc_participacao_contrato numeric(9, 6) NULL,"
    adSQL sSQL, "   perc_participacao numeric(38, 6) NULL,"
    adSQL sSQL, "   val_cosseguro_cedido int NOT NULL,"
    adSQL sSQL, "   val_bonificacao numeric(38, 4) NULL,"
    adSQL sSQL, "   tp_endosso_id int NULL,"
    adSQL sSQL, "   val_financeiro numeric(38, 2) NULL,"
    adSQL sSQL, "   val_juros numeric(38, 4) NULL,"
    adSQL sSQL, "   perc_corretagem NUMERIC(9,6),"
    adSQL sSQL, "   tipo_calculo_contrato INT,"
    adSQL sSQL, "   tp_cobertura_id INT,"
    adSQL sSQL, "   usada_IS_maior_cobertura varchar(1) NOT NULL,"
    adSQL sSQL, "   val_is_contrato numeric(38, 2) NULL,"
    adSQL sSQL, "    Resseguro_Manual    CHAR(1),"
    adSQL sSQL, "    Resseguro_Manual_endosso    CHAR(1)," 'Cibele 03/09/2019
    adSQL sSQL, "    Calcula_Endosso_Subvencao    CHAR(1)," 'Cibele 03/09/2019
    adSQL sSQL, "    Modalidade_ID       INT,"
    adSQL sSQL, "    Limite_Retencao     NUMERIC(15,2),"
    adSQL sSQL, "    existe_er char(1),"
    adSQL sSQL, "    Existe_Resseguro  CHAR(1)    ,"
    adSQL sSQL, "    Existe_Sinistro   CHAR(1),"
    adSQL sSQL, "    Situacao_Item     CHAR(1),"
    adSQL sSQL, "    Gravar_resseguro  CHAR(1),"
    adSQL sSQL, "    ID int IDENTITY(1,1) NOT NULL)"
    
    Call Conexao_ExecutarSQL(sSiglaSistema, iAmbienteId, sSiglaRecurso, sDescricaoRecurso, sSQL, lConexao, False, False, 30000, adLockOptimistic, adUseClient)
    
    sSQL = ""
    adSQL sSQL, "CREATE UNIQUE INDEX IDX001_#Objeto_Contrato ON #Objeto_Contrato (Proposta_ID, Endosso_ID, Cod_Objeto_Segurado,contrato_id,num_versao, TP_COBERTURA_ID)  "
    adSQL sSQL, "CREATE INDEX IDX002_#Objeto_Contrato ON #Objeto_Contrato (Proposta_ID, Endosso_ID)     "
    
    Call Conexao_ExecutarSQL(sSiglaSistema, iAmbienteId, sSiglaRecurso, sDescricaoRecurso, sSQL, lConexao, False, False, 30000, adLockOptimistic, adUseClient)
    
    sSQL = ""
    adSQL sSQL, "CREATE TABLE #documento_emitido_temp                            "
    adSQL sSQL, "( proposta_id              NUMERIC(9),                            "
    adSQL sSQL, "  endosso_id               INT,                            "
    adSQL sSQL, "  dt_emissao               SMALLDATETIME,                             "
    adSQL sSQL, "  dt_inicio_vigencia       SMALLDATETIME,                             "
    adSQL sSQL, "  dt_ini_vigencia_proposta SMALLDATETIME,                            "
    adSQL sSQL, "  dt_fim_vigencia_proposta SMALLDATETIME NULL,                             "
    adSQL sSQL, "  seguro_moeda_id NUMERIC(3),                            "
    adSQL sSQL, "  tp_documento VARCHAR(20),               "
    adSQL sSQL, "  apolice_id NUMERIC(9),                            "
    adSQL sSQL, "  ramo_id INT,                            "
    adSQL sSQL, "  sucursal_seguradora_id NUMERIC(5),                            "
    adSQL sSQL, "  seguradora_cod_susep NUMERIC(5),             "
    adSQL sSQL, "  ramo_irb_id INT,                     "
    adSQL sSQL, "  subramo_id INT,                    "
    adSQL sSQL, "  num_meses INT,                      "
    adSQL sSQL, "  cod_bem INT,                        "
    adSQL sSQL, "  val_financeiro NUMERIC(15,2) NULL,                "
    adSQL sSQL, "  situacao_resseguro CHAR(1),            "
    adSQL sSQL, "  dt_avaliacao_contrato_resseguro SMALLDATETIME NULL,            "
    adSQL sSQL, "  val_bonificacao NUMERIC(17,4),          "
    adSQL sSQL, "  TP_ENDOSSO_ID INT,        "
    adSQL sSQL, "  val_juros NUMERIC(17,4)    ,"
    adSQL sSQL, "  produto_id INT,"
    adSQL sSQL, "  canal_id INT,"
    adSQL sSQL, "  TP_EMISSAO CHAR(1))      "
    
    Call Conexao_ExecutarSQL(sSiglaSistema, iAmbienteId, sSiglaRecurso, sDescricaoRecurso, sSQL, lConexao, False, False, 30000, adLockOptimistic, adUseClient)
    
    sSQL = ""
    adSQL sSQL, "CREATE UNIQUE INDEX UK_001_#documento_emitido_temp ON #documento_emitido_temp(PROPOSTA_ID,ENDOSSO_ID)"
    adSQL sSQL, ""
    adSQL sSQL, "CREATE TABLE #Objeto_Contrato_Plano"
    adSQL sSQL, "("
    adSQL sSQL, "       ID                  INT"
    adSQL sSQL, "     , Proposta_ID         numeric(9, 0)"
    adSQL sSQL, "     , Endosso_ID          INT"
    adSQL sSQL, "     , Cod_Objeto_Segurado INT"
    adSQL sSQL, ""
    adSQL sSQL, "     , Perc_Comissao_Resseguro   NUMERIC(5,2)"
    adSQL sSQL, "     , Contrato_Plano_ID         INT"
    adSQL sSQL, "     , cod_plano                 INT"
    adSQL sSQL, "     , ressegurador_id           INT"
    adSQL sSQL, "     , Pagamento_Por_Documento   CHAR(1)"
    adSQL sSQL, "     , contrato_ressegurador_id  INT"
    adSQL sSQL, "     , Existe_ER                 CHAR(1)"
    adSQL sSQL, ""
    adSQL sSQL, "     , Perc_premio_resseguro   NUMERIC(6,3)"
    adSQL sSQL, "     , Val_premio_Resseguro    NUMERIC(15,2)"
    adSQL sSQL, "     , val_Comissao_Resseguro  NUMERIC(15,2)"
    adSQL sSQL, "     , val_premio_Bonificacao  NUMERIC(15,2)"
    adSQL sSQL, ")"
    
    Call Conexao_ExecutarSQL(sSiglaSistema, iAmbienteId, sSiglaRecurso, sDescricaoRecurso, sSQL, lConexao, False, False, 30000, adLockOptimistic, adUseClient)
    
    sSQL = ""
    adSQL sSQL, "CREATE UNIQUE INDEX IDX001_#Objeto_Contrato_Plano ON #Objeto_Contrato_Plano (Proposta_ID, Endosso_ID, Cod_Objeto_Segurado,Contrato_Plano_ID,contrato_ressegurador_id)            "
    adSQL sSQL, "CREATE INDEX IDX002_#Objeto_Contrato_Plano ON #Objeto_Contrato_Plano (Proposta_ID, Endosso_ID)     "
    adSQL sSQL, "CREATE INDEX IDX003_#Objeto_Contrato_Plano ON #Objeto_Contrato_Plano (ID)  "
    
    Call Conexao_ExecutarSQL(sSiglaSistema, iAmbienteId, sSiglaRecurso, sDescricaoRecurso, sSQL, lConexao, False, False, 30000, adLockOptimistic, adUseClient)
        
    sSQL = ""
    adSQL sSQL, "CREATE TABLE #Premio_Resseguro                                 "
    adSQL sSQL, "("
    adSQL sSQL, "       ID                        INT"
    adSQL sSQL, "    , ID_CONTRATO_PLANO         INT IDENTITY(1,1)"
    adSQL sSQL, "     , resseguro_objeto_id       INT"
    adSQL sSQL, "     , contrato_plano_id         INT"
    adSQL sSQL, "    , contrato_ressegurador_id int     "
    adSQL sSQL, "     , val_premio                NUMERIC(15,2)"
    adSQL sSQL, "     , val_premio_juros          NUMERIC(15,2)"
    adSQL sSQL, "     , val_comissao              NUMERIC(15,2)"
    adSQL sSQL, "     , val_comissao_juros        NUMERIC(15,2)"
    adSQL sSQL, "     , perc_resseguro            NUMERIC(6,3)"
    adSQL sSQL, "     , perc_comissao             NUMERIC(15,2)"
    adSQL sSQL, "     , val_bonificacao           NUMERIC(15,2)"
    adSQL sSQL, "     , resseguro_forma_pagamento CHAR(01)"
    adSQL sSQL, "     , resseguro_num_maximo_parcelas      INT"
    adSQL sSQL, "     , resseguro_num_meses_para_pagamento TINYINT"
    adSQL sSQL, "     , cod_plano                   INT"
    adSQL sSQL, "     , ano_mes_emissao             CHAR(6)"
    adSQL sSQL, "     , ano_mes_ini_vigencia        CHAR(6)"
    adSQL sSQL, "     , linhas_agendamento_cobranca INT"
    adSQL sSQL, "     , seguro_moeda_id             NUMERIC(3)"
    adSQL sSQL, "     , parm_usuario                VARCHAR(20)"
    adSQL sSQL, "     , situacao_item               CHAR(1)"
    adSQL sSQL, "     , pagamento_por_documento     CHAR(1)"
    adSQL sSQL, "    , ramo_irb_id                 INT"
    adSQL sSQL, "     , modalidade_id               INT"
    adSQL sSQL, "      , ressegurador_id            INT"
    adSQL sSQL, "     , Calcular_Juros              CHAR(1))"
    
    Call Conexao_ExecutarSQL(sSiglaSistema, iAmbienteId, sSiglaRecurso, sDescricaoRecurso, sSQL, lConexao, False, False, 30000, adLockOptimistic, adUseClient)
       
    sSQL = ""
    adSQL sSQL, "CREATE UNIQUE INDEX UK001_#Premio_Resseguro ON #Premio_Resseguro(ID,contrato_plano_id)"
    adSQL sSQL, ""
    adSQL sSQL, "CREATE TABLE #Premio_Ressegurador"
    adSQL sSQL, "(    ID_Premio_Ressegurador INT IDENTITY(1,1)"
    adSQL sSQL, "   , ID_CONTRATO_PLANO INT "
    adSQL sSQL, "   , contrato_plano_id INT"
    adSQL sSQL, "   , contrato_ressegurador_id INT"
    adSQL sSQL, "   , ressegurador_lider char(1)"
    adSQL sSQL, "   , perc_participacao numeric(9, 6) "
    adSQL sSQL, "   , Val_premio NUMERIC(15,2)"
    adSQL sSQL, "   , Val_comissao NUMERIC(15,2)"
    adSQL sSQL, "   , Val_bonificacao NUMERIC(15,2)"
    adSQL sSQL, "   , Perc_resseguro numeric(9, 6) "
    adSQL sSQL, "   , Perc_comissao numeric(9, 6) "
    adSQL sSQL, "   , resseguro_objeto_plano_id INT     "
    adSQL sSQL, "   , Contrato_ID INT"
    adSQL sSQL, "  , Num_Versao INT"
    adSQL sSQL, "   , Ressegurador_ID INT)"
    
    Call Conexao_ExecutarSQL(sSiglaSistema, iAmbienteId, sSiglaRecurso, sDescricaoRecurso, sSQL, lConexao, False, False, 30000, adLockOptimistic, adUseClient)
    
    sSQL = ""
    adSQL sSQL, "CREATE UNIQUE INDEX UK001_#Premio_ressegurador ON #Premio_Ressegurador(ID_CONTRATO_PLANO,contrato_plano_id,contrato_ressegurador_id)   "
    adSQL sSQL, ""
    adSQL sSQL, "CREATE TABLE #documento_cobertura "
    adSQL sSQL, "(  "
    adSQL sSQL, "    proposta_id numeric(9, 0) NULL,"
    adSQL sSQL, "   endosso_id int NULL,"
    adSQL sSQL, "    tp_cobertura_id INT,"
    adSQL sSQL, "    cod_objeto_segurado INT ,     "
    adSQL sSQL, "    val_premio_tarifa NUMERIC(15,2),"
    adSQL sSQL, "    val_premio_net NUMERIC(15,2),"
    adSQL sSQL, "    val_is NUMERIC(15,2),"
    adSQL sSQL, "    val_is_qualquer NUMERIC(15,2))"
  
    Call Conexao_ExecutarSQL(sSiglaSistema, iAmbienteId, sSiglaRecurso, sDescricaoRecurso, sSQL, lConexao, False, False, 30000, adLockOptimistic, adUseClient)
  
    Exit Function
    
Trata_Erro:
    Call Err.Raise(Err.Number, , "RSGL0002.cls00423.CriarTabelasTemporariaCalculo - " & Err.Description)
    
End Function


Public Function GerarCalculodeResseguro(ByVal sSiglaSistema As String, _
                                        ByVal sSiglaRecurso As String, _
                                        ByVal sDescricaoRecurso As String, _
                                        ByVal iAmbienteId As Integer, _
                                        ByVal lPropostaId As Long, _
                                        ByVal lEndossoId As Long, _
                                        ByVal sUsuario As String, _
                                        ByVal sDataSistema As String) As Object

    Dim lConexao As Long

    On Error GoTo Trata_Erro
    
    'Criando as tempor�rias''''''''''''''''''''''''''''''''
    Call CriarTabelasTemporariaCalculo(sSiglaSistema, sSiglaRecurso, sDescricaoRecurso, iAmbienteId, lConexao)
  
    'Seleciona registros disponiveis para calculo '''''''''''''''''''''''''''''''''''''''
    Call SelecionarRegistrosCalculoResseguro(sSiglaSistema, sSiglaRecurso, sDescricaoRecurso, iAmbienteId, lPropostaId, lEndossoId, lConexao)
  
    'Seleciona dados complementares dos documentos '''''''''''''''''''''''''''''''''''''''
    Call SelecionarDadosCalculoResseguro(sSiglaSistema, sSiglaRecurso, sDescricaoRecurso, iAmbienteId, lConexao, sUsuario)
    
     'Seleciona registros de contrato '''''''''''''''''''''''''''''''''''''''
    Call SelecionarDadosContrato(sSiglaSistema, sSiglaRecurso, sDescricaoRecurso, iAmbienteId, lConexao, sUsuario)
    
    If Not AbrirTransacao(lConexao) Then
        Call Err.Raise(vbObjectError + 1000, , "RSGL0002.cls00423.GerarCalculodeResseguro - N�o foi poss�vel abrir transa��o")
    End If
    
    'Inclui registros do calculo'''''''''''''''''''''''''''''''''''''''''
    Call RegistrarCalculoResseguro(sSiglaSistema, sSiglaRecurso, sDescricaoRecurso, iAmbienteId, lConexao, sUsuario)

    Call ConfirmarTransacao(lConexao)
    
    'Inclui registros do calculo'''''''''''''''''''''''''''''''''''''''''
    Call RegistrarParcelamentoResseguro(sSiglaSistema, sSiglaRecurso, sDescricaoRecurso, iAmbienteId, lConexao, sUsuario, sDataSistema)
    
    'Inclui registros do calculo'''''''''''''''''''''''''''''''''''''''''
    Call RegistrarAgendamentoResseguro(sSiglaSistema, sSiglaRecurso, sDescricaoRecurso, iAmbienteId, lConexao, sUsuario, sDataSistema)
      
    'Fechando a transa��o '''''''''''''''''''''''''''''''''''''''''''''''''''''''

    
    Exit Function
  
Trata_Erro:
    Call Err.Raise(Err.Number, , "RSGL0002.cls00423.GerarCalculodeResseguro - " & Err.Description)
    
End Function

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'Demanda 19566551 - Calcular Resseguro em Endosso de Ap�lice Aberta.
'Confitec Sistemas - 25/08/2017 - Eduardo.Maior
'
Public Function GerarCalculoEndossodeResseguro(ByVal sSiglaSistema As String, _
                                               ByVal sSiglaRecurso As String, _
                                               ByVal sDescricaoRecurso As String, _
                                               ByVal iAmbienteId As Integer, _
                                               ByVal sUsuario As String, _
                                               ByVal sDataSistema As String) As Object

    Dim lConexao As Long

    On Error GoTo Trata_Erro
    
    'Criando as tempor�rias'''''''''''''''''''''''''''''''''''''''''''''''''''''
    Call CriarTabelasTemporariaCalculo(sSiglaSistema, sSiglaRecurso, sDescricaoRecurso, iAmbienteId, lConexao)
  
    'Seleciona registros disponiveis para calculo ''''''''''''''''''''''''''''''
    Call SelecionarRegistrosCalculoEndosso(sSiglaSistema, sSiglaRecurso, sDescricaoRecurso, iAmbienteId, lConexao)
  
    'Seleciona dados complementares dos documentos '''''''''''''''''''''''''''''
    Call SelecionarDadosCalculoEndosso(sSiglaSistema, sSiglaRecurso, sDescricaoRecurso, iAmbienteId, lConexao, sUsuario)
    
    'Seleciona registros de contrato ''''''''''''''''''''''''''''''''''''''''''
    Call SelecionarDadosContrato(sSiglaSistema, sSiglaRecurso, sDescricaoRecurso, iAmbienteId, lConexao, sUsuario)
    
    If Not AbrirTransacao(lConexao) Then
        Call Err.Raise(vbObjectError + 1000, , "RSGL0002.cls00423.GerarCalculoEndossodeResseguro - N�o foi poss�vel abrir transa��o")
    End If
    
    'Inclui registros do calculo'''''''''''''''''''''''''''''''''''''''''''''''''
    Call RegistrarCalculoResseguro(sSiglaSistema, sSiglaRecurso, sDescricaoRecurso, iAmbienteId, lConexao, sUsuario)

    Call ConfirmarTransacao(lConexao)
    
    'Inclui registros do calculo'''''''''''''''''''''''''''''''''''''''''''''''''
    Call RegistrarParcelamentoResseguro(sSiglaSistema, sSiglaRecurso, sDescricaoRecurso, iAmbienteId, lConexao, sUsuario, sDataSistema)
    
    'Inclui registros do calculo'''''''''''''''''''''''''''''''''''''''''''''''''
    Call RegistrarAgendamentoResseguro(sSiglaSistema, sSiglaRecurso, sDescricaoRecurso, iAmbienteId, lConexao, sUsuario, sDataSistema)
      
    'Fechando a transa��o '''''''''''''''''''''''''''''''''''''''''''''''''''''''

    
    Exit Function
  
Trata_Erro:
    Call Err.Raise(Err.Number, , "RSGL0002.cls00423.GerarCalculoEndossodeResseguro - " & Err.Description)
    
End Function

Private Sub SelecionarRegistrosCalculoResseguro(ByVal sSiglaSistema As String, _
                                                ByVal sSiglaRecurso As String, _
                                                ByVal sDescricaoRecurso As String, _
                                                ByVal iAmbienteId As Integer, _
                                                ByVal lPropostaId As Long, _
                                                ByVal lEndossoId As Long, _
                                                ByVal lConexao As Long)
    Dim sSQL As String

    On Error GoTo Trata_Erro
    
    If lPropostaId > 0 Then
    
        sSQL = "EXEC resseg_db..RSGS00378_SPS " & lPropostaId & "," & lEndossoId
    
    Else
        
        sSQL = "EXEC resseg_db..RSGS00377_SPS "
        
    End If
    
    Call Conexao_ExecutarSQL(sSiglaSistema, iAmbienteId, sSiglaRecurso, sDescricaoRecurso, sSQL, lConexao, False, False, 30000, adLockOptimistic, adUseClient)

    Exit Sub
  
Trata_Erro:
    Call Err.Raise(Err.Number, , "RSGL0002.cls00423.SelecionarRegistrosCalculoResseguro - " & Err.Description)
End Sub

Private Sub SelecionarDadosCalculoResseguro(ByVal sSiglaSistema As String, _
                                            ByVal sSiglaRecurso As String, _
                                            ByVal sDescricaoRecurso As String, _
                                            ByVal iAmbienteId As Integer, _
                                            ByVal lConexao As Long, _
                                            ByVal sUsuario As String)
                                            
    Dim sSQL As String

    On Error GoTo Trata_Erro
    
   
    sSQL = "EXEC resseg_db..RSGS00379_SPS '" & sUsuario & "'"
    
    Call Conexao_ExecutarSQL(sSiglaSistema, iAmbienteId, sSiglaRecurso, sDescricaoRecurso, sSQL, lConexao, False, False, 30000, adLockOptimistic, adUseClient)

    Exit Sub
  
Trata_Erro:
    Call Err.Raise(Err.Number, , "RSGL0002.cls00423.SelecionarDadosCalculoResseguro - " & Err.Description)
End Sub

Private Sub SelecionarDadosContrato(ByVal sSiglaSistema As String, _
                                    ByVal sSiglaRecurso As String, _
                                    ByVal sDescricaoRecurso As String, _
                                    ByVal iAmbienteId As Integer, _
                                    ByVal lConexao As Long, _
                                    ByVal sUsuario As String)
                                            
    Dim sSQL As String

    On Error GoTo Trata_Erro
    
   
    sSQL = "EXEC resseg_db..RSGS00380_SPS '" & sUsuario & "'"
    
    Call Conexao_ExecutarSQL(sSiglaSistema, iAmbienteId, sSiglaRecurso, sDescricaoRecurso, sSQL, lConexao, False, False, 30000, adLockOptimistic, adUseClient)

    Exit Sub
  
Trata_Erro:
    Call Err.Raise(Err.Number, , "RSGL0002.cls00423.SelecionarDadosContrato - " & Err.Description)
End Sub


Private Sub RegistrarCalculoResseguro(ByVal sSiglaSistema As String, _
                                    ByVal sSiglaRecurso As String, _
                                    ByVal sDescricaoRecurso As String, _
                                    ByVal iAmbienteId As Integer, _
                                    ByVal lConexao As Long, _
                                    ByVal sUsuario As String)
                                            
    Dim sSQL As String

    On Error GoTo Trata_Erro
    
   
    sSQL = "EXEC resseg_db..RSGS00381_SPI '" & sUsuario & "'"
    
    Call Conexao_ExecutarSQL(sSiglaSistema, iAmbienteId, sSiglaRecurso, sDescricaoRecurso, sSQL, lConexao, False, False, 30000, adLockOptimistic, adUseClient)

    Exit Sub
  
Trata_Erro:
    Call Err.Raise(Err.Number, , "RSGL0002.cls00423.RegistrarCalculoResseguro - " & Err.Description)
End Sub


Private Sub RegistrarParcelamentoResseguro(ByVal sSiglaSistema As String, _
                                            ByVal sSiglaRecurso As String, _
                                            ByVal sDescricaoRecurso As String, _
                                            ByVal iAmbienteId As Integer, _
                                            ByVal lConexao As Long, _
                                            ByVal sUsuario As String, _
                                            ByVal sData_Sistema As String)
                                                    
    Dim sSQL As String

    On Error GoTo Trata_Erro
    
   
    sSQL = "EXEC resseg_db..RSGS00382_SPI '" & Format(sData_Sistema, "yyyymmdd") & "','" & sUsuario & "'"
    
    Call Conexao_ExecutarSQL(sSiglaSistema, iAmbienteId, sSiglaRecurso, sDescricaoRecurso, sSQL, lConexao, False, False, 30000, adLockOptimistic, adUseClient)

    Exit Sub
  
Trata_Erro:
    Call Err.Raise(Err.Number, , "RSGL0002.cls00423.RegistrarParcelamentoResseguro - " & Err.Description)
End Sub

Private Sub RegistrarAgendamentoResseguro(ByVal sSiglaSistema As String, _
                                            ByVal sSiglaRecurso As String, _
                                            ByVal sDescricaoRecurso As String, _
                                            ByVal iAmbienteId As Integer, _
                                            ByVal lConexao As Long, _
                                            ByVal sUsuario As String, _
                                            ByVal sData_Sistema As String)
                                                    
    Dim sSQL As String

    On Error GoTo Trata_Erro
    
   
    sSQL = "EXEC resseg_db..RSGS00384_SPI '" & sUsuario & "','" & Format(sData_Sistema, "yyyymmdd") & "'"
    
    Call Conexao_ExecutarSQL(sSiglaSistema, iAmbienteId, sSiglaRecurso, sDescricaoRecurso, sSQL, lConexao, False, False, 30000, adLockOptimistic, adUseClient)

    Exit Sub
  
Trata_Erro:
    Call Err.Raise(Err.Number, , "RSGL0002.cls00423.RegistrarAgendamentoResseguro - " & Err.Description)
End Sub

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'Demanda 19566551 - Calcular Resseguro em Endosso de Ap�lice Aberta.
'Confitec Sistemas - 25/08/2017 - Eduardo.Maior
'
Private Sub SelecionarRegistrosCalculoEndosso(ByVal sSiglaSistema As String, _
                                              ByVal sSiglaRecurso As String, _
                                              ByVal sDescricaoRecurso As String, _
                                              ByVal iAmbienteId As Integer, _
                                              ByVal lConexao As Long)
                                              
    Dim sSQL As String

    On Error GoTo Trata_Erro
    
    
    sSQL = "EXEC resseg_db.dbo.RSGS00415_SPS "
    
    Call Conexao_ExecutarSQL(sSiglaSistema, iAmbienteId, sSiglaRecurso, sDescricaoRecurso, sSQL, lConexao, False, False, 30000, adLockOptimistic, adUseClient)

    Exit Sub
  
Trata_Erro:
    Call Err.Raise(Err.Number, , "RSGL0002.cls00423.SelecionarRegistrosCalculoEndosso - " & Err.Description)
End Sub

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'Demanda 19566551 - Calcular Resseguro em Endosso de Ap�lice Aberta.
'Confitec Sistemas - 25/08/2017 - Eduardo.Maior
'
Private Sub SelecionarDadosCalculoEndosso(ByVal sSiglaSistema As String, _
                                          ByVal sSiglaRecurso As String, _
                                          ByVal sDescricaoRecurso As String, _
                                          ByVal iAmbienteId As Integer, _
                                          ByVal lConexao As Long, _
                                          ByVal sUsuario As String)
                                            
    Dim sSQL As String

    On Error GoTo Trata_Erro
    
   
    sSQL = "EXEC resseg_db.dbo.RSGS00416_SPS '" & sUsuario & "'"
    
    Call Conexao_ExecutarSQL(sSiglaSistema, iAmbienteId, sSiglaRecurso, sDescricaoRecurso, sSQL, lConexao, False, False, 30000, adLockOptimistic, adUseClient)

    Exit Sub
  
Trata_Erro:
    Call Err.Raise(Err.Number, , "RSGL0002.cls00423.SelecionarDadosCalculoEndosso - " & Err.Description)
End Sub

