VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cls00323"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Public Function Consultar(ByVal sSiglaSistema As String, _
                          ByVal sSiglaRecurso As String, _
                          ByVal sDescricaoRecurso As String, _
                          ByVal iAmbienteId As Integer, _
                          Optional ByVal iRamoId As Integer, _
                          Optional ByVal iTpRamoId As Integer) As Object
    'Rotina de consulta em ramo_tb
    Dim sSQL As String

    On Error GoTo Trata_Erro
    ' Instanciando a classe de conex�o ''''''''''''''''''''''''''''''''''''''''''
    sSQL = ""
    sSQL = sSQL & "SELECT ramo_id, " & vbNewLine
    sSQL = sSQL & "       nome " & vbNewLine
    sSQL = sSQL & "  FROM ramo_tb " & vbNewLine

    If iRamoId > 0 Then
        sSQL = sSQL & " WHERE ramo_id = " & iRamoId & vbNewLine

        If iTpRamoId > 0 Then
            sSQL = sSQL & "   AND tp_ramo_id = " & iTpRamoId & vbNewLine
        End If

    Else

        If iTpRamoId > 0 Then
            sSQL = sSQL & "WHERE tp_ramo_id = " & iTpRamoId & vbNewLine
        End If
    End If

    sSQL = sSQL & " ORDER BY ramo_id"
    Set Consultar = ExecutarSQL(sSiglaSistema, iAmbienteId, sSiglaRecurso, sDescricaoRecurso, sSQL, True)
    Exit Function
Trata_Erro:
    Call Err.Raise(Err.Number, , "RSGL0002.cls00323.ConsultarRamo - " & Err.Description)
End Function


'Modifica��o: AB-ABS
'(IN�CIO) -- RSouza -- 10/06/2010 -------------------------------------------------------------------------------'
Public Function ExecutarSQL2(ByVal sSiglaSistema As String, _
                            ByVal iAmbienteId As Integer, _
                            ByVal sSiglaRecurso As String, _
                            ByVal sDescricaoRecurso As String, _
                            ByVal sSQL As String, _
                            Optional Opcao As Boolean = True) As Object


    Set ExecutarSQL2 = ExecutarSQL(sSiglaSistema, iAmbienteId, sSiglaRecurso, sDescricaoRecurso, sSQL, True)
    
    
    Exit Function
Trata_Erro:
    Call Err.Raise(Err.Number, , "RSGL0002.cls00323.ConsultarRamo - " & Err.Description)
End Function
'(FIM)  -- Rsouza  -- 10/06/2010 ---------------------------------------------------------------------------------'
