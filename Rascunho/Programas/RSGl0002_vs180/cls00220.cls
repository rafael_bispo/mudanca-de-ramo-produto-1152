VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cls00220"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True

Public Function Consultar(ByVal sSiglaSistema As String, _
                          ByVal sSiglaRecurso As String, _
                          ByVal sDescricaoRecurso As String, _
                          ByVal iAmbienteId As Integer, _
                          Optional ByVal iTpConsulta As Integer, _
                          Optional ByVal lCodPlanoId As Long, _
                          Optional ByVal sNome As String) As Object
    '## Rotina de consulta em plano_tb
    'Tipo Consulta '''''''''''''''''''''''''''''''''''''''''''''''''
    '0 - Todos
    '1 - C�digo do plano
    '2 - Nome
    Dim sSQL As String

    On Error GoTo Trata_Erro
    sSQL = ""
    sSQL = sSQL & "  SELECT *" & vbNewLine
    sSQL = sSQL & "    FROM plano_tb" & vbNewLine

    Select Case iTpConsulta
        Case 1
            sSQL = sSQL & "   WHERE plano_tb.cod_plano = " & lCodPlanoId & vbNewLine
        Case 2
            sSQL = sSQL & "   WHERE plano_tb.nome LIKE '" & sNome & "%'" & vbNewLine
    End Select

    sSQL = sSQL & "ORDER BY plano_tb.cod_plano"
    Set Consultar = ExecutarSQL(sSiglaSistema, iAmbienteId, sSiglaRecurso, sDescricaoRecurso, sSQL, True)
    Exit Function
Trata_Erro:
    Call Err.Raise(Err.Number, , "RSGL0001.plano_resseguro.Consultar - " & Err.Description)
End Function
