VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cls00270"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True

Public Function AlterarValoresAjustadosPrevisao(ByVal sSiglaSistema As String, _
                                                ByVal sSiglaRecurso As String, _
                                                ByVal sDescricaoRecurso As String, _
                                                ByVal lAmbienteID As Long, _
                                                ByVal iContratoAjustamentoPrevisaoId As Integer, _
                                                ByVal iContratoId As Integer, _
                                                ByVal iCodPlano As Integer, _
                                                ByVal sMesAno As String, _
                                                ByVal cValPremioEmitido As Currency, _
                                                ByVal cValPremioAjustado As Currency, _
                                                ByVal dTaxaAjustamento As Double, _
                                                ByVal sUserName As String)

    On Error GoTo Tratar_Erro
    'Rotina para altera��o dos dados da tabela contrato_ajustamento_previsao_tb ''''''''''''''''''''''''
    Dim sSQL As String

    sSQL = ""
    adSQL sSQL, "EXEC contrato_ajustamento_previsao_spu"
    adSQL sSQL, iContratoAjustamentoPrevisaoId & ", "
    adSQL sSQL, iContratoId & ", "
    adSQL sSQL, iCodPlano & ", "
    adSQL sSQL, "'" & Format(sMesAno, "yyyymm") & "', "
    adSQL sSQL, MudaVirgulaParaPonto(cValPremioEmitido) & ", "
    adSQL sSQL, MudaVirgulaParaPonto(cValPremioAjustado) & ", "
    adSQL sSQL, MudaVirgulaParaPonto(dTaxaAjustamento) & ", "
    adSQL sSQL, "'" & sUserName & "' "
    Call ExecutarSQL(sSiglaSistema, lAmbienteID, sSiglaRecurso, sDescricaoRecurso, sSQL, False)
    Exit Function
Tratar_Erro:
    Call Err.Raise(Err.Number, "cls00270 - AlterarValoresAjustadosPrevisao" & Err.Description)
End Function

Public Function ConsultarPrevisaoAjustamento(ByVal sSiglaSistema As String, _
                                             ByVal sSiglaRecurso As String, _
                                             ByVal sDescricaoRecurso As String, _
                                             ByVal iAmbienteId As Integer, _
                                             Optional ByVal sAnoMes As String, _
                                             Optional ByVal iContratoId As Integer) As Object

    On Error GoTo Trata_Erro
    Dim sSQL As String

    sSQL = ""
    adSQL sSQL, "SELECT contrato_ajustamento_previsao_tb.contrato_ajustamento_previsao_id , "
    adSQL sSQL, "       contrato_ajustamento_previsao_tb.contrato_id, "
    adSQL sSQL, "       contrato_ajustamento_previsao_tb.cod_plano, "
    adSQL sSQL, "       contrato_ajustamento_previsao_tb.ano_mes, "
    adSQL sSQL, "       contrato_ajustamento_previsao_tb.val_premio_emitido, "
    adSQL sSQL, "       contrato_ajustamento_previsao_tb.val_premio_ajustado, "
    adSQL sSQL, "       contrato_ajustamento_previsao_tb.taxa_ajustamento, "
    adSQL sSQL, "       contrato_tb.documento_referencia, "
    adSQL sSQL, "       'nome_plano' = plano_tb.nome "
    adSQL sSQL, "  FROM contrato_ajustamento_previsao_tb "
    adSQL sSQL, "  JOIN contrato_tb "
    adSQL sSQL, "    ON contrato_tb.contrato_id = contrato_ajustamento_previsao_tb.contrato_id "
    adSQL sSQL, "  JOIN contrato_plano_tb "
    adSQL sSQL, "    ON contrato_plano_tb.contrato_id = contrato_ajustamento_previsao_tb.contrato_id "
    adSQL sSQL, "   AND contrato_plano_tb.cod_plano = contrato_ajustamento_previsao_tb.cod_plano "
    adSQL sSQL, "  JOIN plano_tb "
    adSQL sSQL, "    ON plano_tb.cod_plano = contrato_plano_tb.cod_plano "

    If sAnoMes <> "" Then
        adSQL sSQL, "WHERE contrato_ajustamento_previsao_tb.ano_mes = '" & sAnoMes & "'"
    End If

    If iContratoId <> 0 Then
        adSQL sSQL, "WHERE contrato_ajustamento_previsao_tb.contrato_id = " & iContratoId
    End If

    adSQL sSQL, "ORDER BY contrato_ajustamento_previsao_tb.ano_mes"
    Set ConsultarPrevisaoAjustamento = ExecutarSQL(sSiglaSistema, iAmbienteId, sSiglaRecurso, sDescricaoRecurso, sSQL, True)
    Exit Function
Trata_Erro:
    Call Err.Raise(Err.Number, , "RSGL0002.cls00270.ConsultarPrevisaoAjustamento - " & Err.Description)
End Function

Public Function ConsultarAjustamento(ByVal sSiglaSistema As String, _
                                     ByVal sSiglaRecurso As String, _
                                     ByVal sDescricaoRecurso As String, _
                                     ByVal iAmbienteId As Integer, _
                                     Optional ByVal sAnoMes As String, _
                                     Optional ByVal iContratoId As Integer) As Object

    On Error GoTo Trata_Erro
    Dim sSQL As String

    sSQL = ""
    adSQL sSQL, "SELECT contrato_ajustamento_previsao_tb.contrato_ajustamento_previsao_id, "
    adSQL sSQL, "       contrato_ajustamento_previsao_tb.contrato_id, "
    adSQL sSQL, "       contrato_ajustamento_previsao_tb.ano_mes, "
    adSQL sSQL, "       contrato_ajustamento_previsao_tb.val_premio_emitido, "
    adSQL sSQL, "       contrato_ajustamento_previsao_tb.val_premio_ajustado, "
    adSQL sSQL, "       contrato_ajustamento_previsao_tb.taxa_ajustamento, "
    adSQL sSQL, "       contrato_tb.documento_referencia, "
    adSQL sSQL, "       'nome_plano' = plano_tb.nome "
    adSQL sSQL, "  FROM contrato_ajustamento_previsao_tb "
    adSQL sSQL, "  JOIN plano_tb "
    adSQL sSQL, "    ON plano_tb.cod_plano = contrato_ajustamento_previsao_tb.cod_plano "

    If Trim(sAnoMes) <> "" Then
        adSQL sSQL, "WHERE contrato_ajustamento_previsao_tb.ano_mes = '" & sAnoMes & "'"
    End If

    If iContratoId <> 0 Then
        adSQL sSQL, "WHERE contrato_ajustamento_previsao_tb.contrato_id = " & iContratoId
    End If

    adSQL sSQL, "ORDER BY contrato_ajustamento_previsao_tb.ano_mes"
    Set ConsultarAjustamento = ExecutarSQL(sSiglaSistema, iAmbienteId, sSiglaRecurso, sDescricaoRecurso, sSQL, True)
    Exit Function
Trata_Erro:
    Call Err.Raise(Err.Number, , "RSGL0002.cls00270.ConsultarAjustamento - " & Err.Description)
End Function

Public Function ExcluirValoresAjustadosPrevisao(ByVal sSiglaSistema As String, _
                                                ByVal sSiglaRecurso As String, _
                                                ByVal sDescricaoRecurso As String, _
                                                ByVal lAmbienteID As Long, _
                                                ByVal iContratoAjustPrevisaoId As Integer)

    On Error GoTo Tratar_Erro
    'Rotina de exclus�o de dados da tabela contrato_ajustamento_previsao_tb''''''''''''''''
    Dim sSQL As String

    sSQL = "EXEC contrato_ajustamento_previsao_spd "
    adSQL sSQL, iContratoAjustPrevisaoId
    Call ExecutarSQL(sSiglaSistema, lAmbienteID, sSiglaRecurso, sDescricaoRecurso, sSQL, False)
    Exit Function
Tratar_Erro:
    Call Err.Raise(Err.Number, , "cls00270 - ExcluirValoresAjustadosPrevisao - " & Err.Description)
End Function

Public Function IncluirValoresAjustadosPrevisao(ByVal sSiglaSistema As String, _
                                                ByVal sSiglaRecurso As String, _
                                                ByVal sDescricaoRecurso As String, _
                                                ByVal lAmbienteID As Long, _
                                                ByVal iContratoId As Integer, _
                                                ByVal iCodPlano As Integer, _
                                                ByVal sMesAno As String, _
                                                ByVal cValPremioEmitido As Currency, _
                                                ByVal cValPremioAjustado As Currency, _
                                                ByVal dTaxaAjustamento As Double, _
                                                ByVal sUserName As String)

    'Rotina de inclus�o de dados para os contratos de ajustamento previstos''''''''''''''''''''''''''''
    On Error GoTo Tratar_Erro
    Dim sSQL As String

    sSQL = ""
    adSQL sSQL, "EXEC contrato_ajustamento_previsao_spi"
    adSQL sSQL, iContratoId & ", "
    adSQL sSQL, iCodPlano & ", "
    adSQL sSQL, "'" & sMesAno & "', "
    adSQL sSQL, MudaVirgulaParaPonto(cValPremioEmitido) & ", "
    adSQL sSQL, MudaVirgulaParaPonto(cValPremioAjustado) & ", "
    adSQL sSQL, MudaVirgulaParaPonto(dTaxaAjustamento) & ", "
    adSQL sSQL, "'" & sUserName & "' "
    Call ExecutarSQL(sSiglaSistema, lAmbienteID, sSiglaRecurso, sDescricaoRecurso, sSQL, False)
    Exit Function
Tratar_Erro:
    Call Err.Raise(Err.Number, , "cls00270 - IncluirValoresAjustadosPrevisao - " & Err.Description)
End Function
