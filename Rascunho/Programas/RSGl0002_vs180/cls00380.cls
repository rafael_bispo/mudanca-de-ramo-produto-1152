VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cls00380"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True

Public Function ConsultarJurosParcelamento(ByVal sSiglaSistema As String, _
                                           ByVal sSiglaRecurso As String, _
                                           ByVal sDescricaoRecurso As String, _
                                           ByVal lAmbienteID As Long, _
                                           ByVal iTpConsulta As Integer, _
                                           ByVal sConteudo As String) As Object
    'iTpConsulta:
    ' 0 - Todos
    ' 1 - M�s / Ano
    ' 2 - C�digo
    'Rotina para consulta da tabela juros_parcelamento_tb
    Dim sSQL    As String

    Dim sAnoMes As String

    On Error GoTo TratarErro

    If iTpConsulta = 1 Then
        sAnoMes = Mid(sConteudo, 4, 4) & Mid(sConteudo, 1, 2)
    End If

    sSQL = ""
    adSQL sSQL, "SELECT * "
    adSQL sSQL, "  FROM juros_parcelamento_tb "
    adSQL sSQL, "  JOIN ressegurador_tb "
    adSQL sSQL, "    ON ressegurador_tb.ressegurador_id = juros_parcelamento_tb.ressegurador_id "

    Select Case iTpConsulta
        Case 1
            adSQL sSQL, "WHERE ano_mes = '" & sAnoMes & "'"
        Case 2
            adSQL sSQL, "WHERE juros_parcelamento_id = " & Val(sConteudo)
    End Select

    adSQL sSQL, " ORDER BY ano_mes "
    Set ConsultarJurosParcelamento = ExecutarSQL(sSiglaSistema, lAmbienteID, sSiglaRecurso, sDescricaoRecurso, sSQL, True)
    Exit Function
TratarErro:
    Call Err.Raise(Err.Number, , "RSGL0002.cls00380.ConsultarJurosParcelamento - " & Err.Description)
End Function

Public Sub ExcluirJurosParcelamento(ByVal sSiglaSistema As String, _
                                    ByVal sSiglaRecurso As String, _
                                    ByVal sDescricaoRecurso As String, _
                                    ByVal iAmbienteId As Integer, _
                                    ByVal lJurosParcelamentoId As Long)
    Dim sSQL As String

    On Error GoTo Trata_Erro
    sSQL = ""
    adSQL sSQL, "EXEC RSGS0181_spd "
    adSQL sSQL, lJurosParcelamentoId
    Call ExecutarSQL(sSiglaSistema, iAmbienteId, sSiglaRecurso, sDescricaoRecurso, sSQL, False)
    Exit Sub
Trata_Erro:
    Call Err.Raise(Err.Number, , "RSGL0002.cls00380.ExcluirJurosParcelamento - " & Err.Description)
End Sub

Public Function AlterarJurosParcelamento(ByVal sSiglaSistema As String, _
                                         ByVal sSiglaRecurso As String, _
                                         ByVal sDescricaoRecurso As String, _
                                         ByVal lAmbienteID As Long, _
                                         ByVal lJurosParcelamentoId As Long, _
                                         ByVal lResseguradorId As Long, _
                                         ByVal sAnoMes As String, _
                                         ByVal iMesesPag As Integer, _
                                         ByVal iNumParc As Integer, _
                                         ByVal dFatJuros As Double, _
                                         ByVal sUsuario As String) As Boolean
    'Atualizar dados do juros parcelamento
    Dim sSQL As String

    On Error GoTo Trata_Erro
    sSQL = "EXEC RSGS0180_spu "
    adSQL sSQL, lJurosParcelamentoId & ", "
    adSQL sSQL, iMesesPag & ", "
    adSQL sSQL, lResseguradorId & ", "
    adSQL sSQL, "'" & sAnoMes & "', "
    adSQL sSQL, iNumParc & ", "
    adSQL sSQL, MudaVirgulaParaPonto(dFatJuros) & ", "
    adSQL sSQL, "'" & sUsuario & "'"
    Call ExecutarSQL(sSiglaSistema, lAmbienteID, sSiglaRecurso, sDescricaoRecurso, sSQL, False)
    Alterar = True
    Exit Function
Trata_Erro:
    Call Err.Raise(Err.Number, , "RSGL0002.cls00380.AlterarJurosParcelamento - " & Err.Description)
End Function

Public Function IncluirJurosParcelamento(ByVal sSiglaSistema As String, _
                                         ByVal sSiglaRecurso As String, _
                                         ByVal sDescricaoRecurso As String, _
                                         ByVal lAmbienteID As Long, _
                                         ByVal iResseguradorId As Integer, _
                                         ByVal sAnoMes As String, _
                                         ByVal iMesesPag As Integer, _
                                         ByVal iNumParc As Integer, _
                                         ByVal dFatJuros As Double, _
                                         ByVal sUsuario As String) As Boolean
    'Inclus�o de dados do juros parcelamento
    Dim sSQL As String

    On Error GoTo Trata_Erro
    sSQL = "EXEC RSGS0179_spi "
    adSQL sSQL, iMesesPag & ", "
    adSQL sSQL, iResseguradorId & ", "
    adSQL sSQL, "'" & sAnoMes & "', "
    adSQL sSQL, iNumParc & ", "
    adSQL sSQL, MudaVirgulaParaPonto(dFatJuros) & ", "
    adSQL sSQL, "'" & sUsuario & "'"
    Call ExecutarSQL(sSiglaSistema, lAmbienteID, sSiglaRecurso, sDescricaoRecurso, sSQL, False)
    Incluir = True
    Exit Function
Trata_Erro:
    Call Err.Raise(Err.Number, , "RSGL0002.cls00380.IncluirJurosParcelamento - " & Err.Description)
End Function
