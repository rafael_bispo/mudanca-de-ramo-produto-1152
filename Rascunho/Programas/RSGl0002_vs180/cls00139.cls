VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cls00139"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Public Function IncluirTabuaCalculo(ByVal sSiglaSistema As String, _
                                    ByVal sSiglaRecurso As String, _
                                    ByVal sDescricaoRecurso As String, _
                                    ByVal iAmbienteId As Integer, _
                                    ByVal sNome As String, _
                                    ByVal sCaracteristica As String, _
                                    ByVal sAdequacao As String, _
                                    ByVal sUsuario As String) As Integer
    Dim sSQL    As String

    Dim rsTabua As Recordset

    On Error GoTo Erro
    sSQL = "EXEC tabua_calculo_spi" & vbNewLine
    sSQL = sSQL & " '" & sNome & "'" & vbNewLine
    sSQL = sSQL & ",'" & sCaracteristica & "'" & vbNewLine
    sSQL = sSQL & ",'" & sAdequacao & "'" & vbNewLine
    sSQL = sSQL & ",'" & sUsuario & "'" & vbNewLine
    Set rsTabua = ExecutarSQL(sSiglaSistema, iAmbienteId, sSiglaRecurso, sDescricaoRecurso, sSQL, True)

    If Not rsTabua.EOF Then
        IncluirTabuaCalculo = rsTabua("tabua_calculo_id")
        rsTabua.Close
    Else
        GoTo Erro
    End If

    Set rsTabua = Nothing
    Exit Function
Erro:
    Call Err.Raise(Err.Number, , "SEGL0027.cls00127.IncluirTabuaCalculo - " & Err.Description)
End Function

Public Sub AlterarTabuaCalculo(ByVal sSiglaSistema As String, _
                               ByVal sSiglaRecurso As String, _
                               ByVal sDescricaoRecurso As String, _
                               ByVal iAmbienteId As Integer, _
                               ByVal iTabuaCalculoId As Integer, _
                               ByVal sNome As String, _
                               ByVal sCaracteristica As String, _
                               ByVal sAdequacao As String, _
                               ByVal sUsuario As String)
    Dim sSQL As String

    On Error GoTo Erro
    sSQL = "EXEC tabua_calculo_spu" & vbNewLine
    sSQL = sSQL & "  " & iTabuaCalculoId & vbNewLine
    sSQL = sSQL & ",'" & sNome & "'" & vbNewLine
    sSQL = sSQL & ",'" & sCaracteristica & "'" & vbNewLine
    sSQL = sSQL & ",'" & sAdequacao & "'" & vbNewLine
    sSQL = sSQL & ",'" & sUsuario & "'" & vbNewLine
    Call ExecutarSQL(sSiglaSistema, iAmbienteId, sSiglaRecurso, sDescricaoRecurso, sSQL, False)
    Exit Sub
Erro:
    Call Err.Raise(Err.Number, , "SEGL0027.cls00127.AlterarTabuaCalculo - " & Err.Description)
End Sub

Public Function PesquisarTabuaCalculo(ByVal sSiglaSistema As String, _
                                      ByVal sSiglaRecurso As String, _
                                      ByVal sDescricaoRecurso As String, _
                                      ByVal iAmbienteId As Integer, _
                                      ByVal byTipoPesquisa As Byte, _
                                      ByVal sConteudo As String) As Object
    Dim sSQL As String

    On Error GoTo Erro
    sSQL = ""
    sSQL = sSQL & "SELECT tabua_calculo_id," & vbNewLine
    sSQL = sSQL & "       nome," & vbNewLine
    sSQL = sSQL & "       caracteristica," & vbNewLine
    sSQL = sSQL & "       adequacao" & vbNewLine
    sSQL = sSQL & "  FROM tabua_calculo_tb" & vbNewLine

    If byTipoPesquisa = 1 Then
        sSQL = sSQL & "WHERE nome like '%" & Trim(sConteudo) & "%'" & vbNewLine
    End If

    sSQL = sSQL & "ORDER BY nome" & vbNewLine
    Set PesquisarTabuaCalculo = ExecutarSQL(sSiglaSistema, iAmbienteId, sSiglaRecurso, sDescricaoRecurso, sSQL)
    Exit Function
Erro:
    Call Err.Raise(Err.Number, , "SEGL0027.cls00127.PesquisarTabuaCalculo - " & Err.Description)
End Function

'-- (IN�CIO) -- RSOUZA -- 13/08/2010 ---------------------------------------------------------------------------------'
Public Function PesquisarComponenteCalculo(ByVal sSiglaSistema As String, _
                                      ByVal sSiglaRecurso As String, _
                                      ByVal sDescricaoRecurso As String, _
                                      ByVal iAmbienteId As Integer, _
                                      ByVal byTipoPesquisa As Byte, _
                                      ByVal sConteudo As String) As Object
    Dim sSQL As String

    On Error GoTo Erro
    
    sSQL = ""
    sSQL = sSQL & "SELECT tp_componente_id," & vbNewLine
    sSQL = sSQL & "       nome    " & vbNewLine
    sSQL = sSQL & "  FROM TP_COMPONENTE_TB" & vbNewLine
    
    If Trim(sConteudo) <> "" Then
       sSQL = sSQL & "  WHERE tp_componente_id = " & CInt(sConteudo) & vbNewLine
    End If

    Set PesquisarComponenteCalculo = ExecutarSQL(sSiglaSistema, iAmbienteId, sSiglaRecurso, sDescricaoRecurso, sSQL)
    Exit Function
Erro:
    Call Err.Raise(Err.Number, , "SEGL0027.cls00127.PesquisarTabuaCalculo - " & Err.Description)
End Function


Public Function PesquisarTipoTaxaCalculo(ByVal sSiglaSistema As String, _
                                      ByVal sSiglaRecurso As String, _
                                      ByVal sDescricaoRecurso As String, _
                                      ByVal iAmbienteId As Integer, _
                                      ByVal byTipoPesquisa As Byte, _
                                      ByVal sConteudo As String) As Object
    Dim sSQL As String

    On Error GoTo Erro
    
    sSQL = ""
    sSQL = sSQL & "SELECT classe_cobertura_id," & vbNewLine
    sSQL = sSQL & "       descricao    " & vbNewLine
    sSQL = sSQL & "  FROM classe_cobertura_tb" & vbNewLine
    
    If Trim(sConteudo) <> "" Then
       sSQL = sSQL & "  WHERE classe_cobertura_id = " & CInt(sConteudo) & vbNewLine
    End If
    

    Set PesquisarTipoTaxaCalculo = ExecutarSQL(sSiglaSistema, iAmbienteId, sSiglaRecurso, sDescricaoRecurso, sSQL)
    Exit Function
Erro:
    Call Err.Raise(Err.Number, , "SEGL0027.cls00127.PesquisarTabuaCalculo - " & Err.Description)
End Function


Public Function PesquisarTabuaCalculoPorID(ByVal sSiglaSistema As String, _
                                      ByVal sSiglaRecurso As String, _
                                      ByVal sDescricaoRecurso As String, _
                                      ByVal iAmbienteId As Integer, _
                                      ByVal byTipoPesquisa As Byte, _
                                      ByVal sConteudo As String) As Object
    Dim sSQL As String

    On Error GoTo Erro
    sSQL = ""
    sSQL = sSQL & "SELECT tabua_calculo_id," & vbNewLine
    sSQL = sSQL & "       nome," & vbNewLine
    sSQL = sSQL & "       caracteristica," & vbNewLine
    sSQL = sSQL & "       adequacao" & vbNewLine
    sSQL = sSQL & "  FROM tabua_calculo_tb" & vbNewLine

    If Trim(sConteudo) <> "" Then
        sSQL = sSQL & "WHERE tabua_calculo_id =" & Trim(sConteudo) & vbNewLine
    End If

    sSQL = sSQL & "ORDER BY nome" & vbNewLine
    Set PesquisarTabuaCalculoPorID = ExecutarSQL(sSiglaSistema, iAmbienteId, sSiglaRecurso, sDescricaoRecurso, sSQL)
    Exit Function
Erro:
    Call Err.Raise(Err.Number, , "SEGL0027.cls00127.PesquisarTabuaCalculo - " & Err.Description)
End Function

'-- (FIM) -- RSOUZA  --- 13/08/2010 ---------------------------------------------------------------------------------'



Public Function TestarNomeIgual(ByVal sSiglaSistema As String, _
                                ByVal sSiglaRecurso As String, _
                                ByVal sDescricaoRecurso As String, _
                                ByVal iAmbienteId As Integer, _
                                ByVal sNome As String) As Boolean
    Dim sSQL   As String

    Dim oTabua As Object

    On Error GoTo Erro
    sSQL = ""
    sSQL = sSQL & "SELECT COUNT(*)" & vbNewLine
    sSQL = sSQL & "  FROM tabua_calculo_tb" & vbNewLine
    sSQL = sSQL & " WHERE nome = '" & sNome & "'" & vbNewLine
    Set oTabua = ExecutarSQL(sSiglaSistema, iAmbienteId, sSiglaRecurso, sDescricaoRecurso, sSQL)

    If oTabua(0) > 0 Then
        TestarNomeIgual = True
    Else
        TestarNomeIgual = False
    End If

    oTabua.Close
    Set oTabua = Nothing
    Exit Function
Erro:
    Call Err.Raise(Err.Number, , "SEGL0027.cls00127.TestarNomeIgual - " & Err.Description)
End Function

Public Sub ExcluirTabuaCalculo(ByVal sSiglaSistema As String, _
                               ByVal sSiglaRecurso As String, _
                               ByVal sDescricaoRecurso As String, _
                               ByVal iAmbienteId As Integer, _
                               ByVal iTabuaCalculoId As Integer)
    Dim sSQL As String

    sSQL = "EXEC tabua_calculo_spd " & iTabuaCalculoId
    Call ExecutarSQL(sSiglaSistema, iAmbienteId, sSiglaRecurso, sDescricaoRecurso, sSQL, False)
    Exit Sub
Erro:
    Call Err.Raise(Err.Number, , "SEGL0027.cls00127.ExcluirTabuaCalculo - " & Err.Description)
End Sub

Public Function PesquisarTabuaMortalidade(ByVal sSiglaSistema As String, _
                                          ByVal sSiglaRecurso As String, _
                                          ByVal sDescricaoRecurso As String, _
                                          ByVal iAmbienteId As Integer, _
                                          ByVal iTabuaMortalidadeId As Integer) As Object
    Dim sSQL As String

    On Error GoTo Erro
    sSQL = ""
    sSQL = sSQL & "SELECT tabua_mortalidade_id," & vbNewLine
    sSQL = sSQL & "       idade," & vbNewLine
    sSQL = sSQL & "       fator_qx" & vbNewLine
    sSQL = sSQL & "  FROM tabua_mortalidade_tb" & vbNewLine
    sSQL = sSQL & " WHERE tabua_calculo_id = " & iTabuaMortalidadeId & vbNewLine
    sSQL = sSQL & " ORDER BY idade" & vbNewLine
    Set PesquisarTabuaMortalidade = ExecutarSQL(sSiglaSistema, iAmbienteId, sSiglaRecurso, sDescricaoRecurso, sSQL)
    Exit Function
Erro:
    Call Err.Raise(Err.Number, , "SEGL0027.cls00127.PesquisarTabuaMortalidade - " & Err.Description)
End Function

Public Function IncluirTabuaMortalidade(ByVal sSiglaSistema As String, _
                                        ByVal sSiglaRecurso As String, _
                                        ByVal sDescricaoRecurso As String, _
                                        ByVal iAmbienteId As Integer, _
                                        ByVal iTabuaCalculoId As Integer, _
                                        ByVal iIdade As Integer, _
                                        ByVal dFatorQX As Double, _
                                        ByVal sUsuario As String)
    Dim sSQL As String

    On Error GoTo Erro
    sSQL = ""
    sSQL = sSQL & "EXEC tabua_mortalidade_spi"
    sSQL = sSQL & "   " & iTabuaCalculoId                       '@Tabua_Calculo_Id
    sSQL = sSQL & ",  " & iIdade                                '@Idade
    sSQL = sSQL & ", '" & Replace(dFatorQX, ",", ".") & "'"                      '@Fator_QX
    sSQL = sSQL & ", '" & sUsuario & "'"                        '@usuario
    Call ExecutarSQL(sSiglaSistema, iAmbienteId, sSiglaRecurso, sDescricaoRecurso, sSQL, False)
    Exit Function
Erro:
    Call Err.Raise(Err.Number, , "SEGL0027.cls00127.IncluirTabuaMortalidade - " & Err.Description)
End Function

Public Function ExcluirTabuaMortalidade(ByVal sSiglaSistema As String, _
                                        ByVal sSiglaRecurso As String, _
                                        ByVal sDescricaoRecurso As String, _
                                        ByVal iAmbienteId As Integer, _
                                        ByVal iTabuaCalculoId As Integer)
    Dim sSQL As String

    sSQL = "EXEC tabua_mortalidade_spd " & iTabuaCalculoId '@Tabua_Calculo_Id
    Call ExecutarSQL(sSiglaSistema, iAmbienteId, sSiglaRecurso, sDescricaoRecurso, sSQL, False)
    Exit Function
Erro:
    Call Err.Raise(Err.Number, , "SEGL0027.cls00127.ExcluirTabuaMortalidade - " & Err.Description)
End Function
