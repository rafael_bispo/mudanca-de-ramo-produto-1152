VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cls00157"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True

Public Function Consultar(ByVal sSiglaSistema As String, _
                          ByVal sSiglaRecurso As String, _
                          ByVal sDescricaoRecurso As String, _
                          ByVal lAmbienteID As Long, _
                          ByVal iTpConsulta As Integer, _
                          ByVal sConteudo As String, _
                          Optional ByVal sTipoCobranca As String) As Object
    'iTpConsulta:
    ' 0 - Todos
    ' 1 - C�digo
    ' 2 - Nome
    'Rotina para consulta da tabela tp_diferenca_repasse_tb
    Dim sSQL As String

    On Error GoTo TratarErro
    sSQL = ""
    adSQL sSQL, "SELECT tp_diferenca_id, "
    adSQL sSQL, "       nome,"
    adSQL sSQL, "       recuperavel, "
    adSQL sSQL, "       tipo_sinistro, "
    adSQL sSQL, "       tipo_cobranca "
    adSQL sSQL, " FROM tp_diferenca_repasse_tb "
    adSQL sSQL, "WHERE 1 = 1 "

    Select Case iTpConsulta
        Case 1
            adSQL sSQL, "AND tp_diferenca_id = " & Val(sConteudo)
        Case 2
            adSQL sSQL, "AND nome like '%" & (sConteudo) & "%'"
    End Select

    If sTipoCobranca <> "" Then
        adSQL sSQL, "AND tipo_cobranca = '" & sTipoCobranca & "'"
    End If

    adSQL sSQL, " ORDER BY nome"
    Set Consultar = ExecutarSQL(sSiglaSistema, lAmbienteID, sSiglaRecurso, sDescricaoRecurso, sSQL, True)
    Exit Function
TratarErro:
    Call Err.Raise(Err.Number, , "cls00157.Consultar - " & Err.Description)
End Function

Public Function Incluir(ByVal sSiglaSistema As String, _
                        ByVal sSiglaRecurso As String, _
                        ByVal sDescricaoRecurso As String, _
                        ByVal lAmbienteID As Long, _
                        ByVal sNome As String, _
                        ByVal sRecuperavel As String, _
                        ByVal sTipoSinistro As String, _
                        ByVal sTipoCobranca As String, _
                        ByVal sUsuario As String)

    'Rotina de inclus�o de dados na tabela tp_diferenca_repasse_tb
    On Error GoTo TratarErro
    Dim sSQL       As String

    Dim oResultado As Object

    sSQL = "EXEC diferenca_repasse_spi '" & sNome & "', "
    adSQL sSQL, "'" & sRecuperavel & "', "
    adSQL sSQL, "'" & sTipoSinistro & "', "
    adSQL sSQL, "'" & sTipoCobranca & "', "
    adSQL sSQL, "'" & sUsuario & "'"
    Call ExecutarSQL(sSiglaSistema, lAmbienteID, sSiglaRecurso, sDescricaoRecurso, sSQL, False)
    Exit Function
TratarErro:
    Call Err.Raise(Err.Number, , "cls00157 - IncluirDiferencaRep - " & Err.Description)
End Function

Public Function Excluir(ByVal sSiglaSistema As String, _
                        ByVal sSiglaRecurso As String, _
                        ByVal sDescricaoRecurso As String, _
                        ByVal lAmbienteID As Long, _
                        ByVal iDiferencaId As Integer) As Boolean

    ' Rotina de dele��o de dados da tabela tp_diferenca_repasse_tb
    On Error GoTo TratarErro
    Dim sSQL As String

    sSQL = "EXEC diferenca_repasse_spd " & iDiferencaId
    Call ExecutarSQL(sSiglaSistema, lAmbienteID, sSiglaRecurso, sDescricaoRecurso, sSQL, False)
    Excluir = True
    Exit Function
TratarErro:
    Call Err.Raise(Err.Number, , "cls00157 - ExcluirDiferencaRep" & Err.Description)
End Function

Public Function Alterar(ByVal sSiglaSistema As String, _
                        ByVal sSiglaRecurso As String, _
                        ByVal sDescricaoRecurso As String, _
                        ByVal lAmbienteID As Long, _
                        ByVal iDiferencaId As Integer, _
                        ByVal sNome As String, _
                        ByVal sRecuperavel As String, _
                        ByVal sTipoSinistro As String, _
                        ByVal sTipoCobranca As String, _
                        ByVal sUsuario As String) As Boolean

    'Rotina destinada a alterar dados da tabela tp_diferenca_repasse_tb'''''
    On Error GoTo TratarErro
    Dim sSQL As String

    sSQL = "EXEC diferenca_repasse_spu '" & sNome & "', "
    adSQL sSQL, "'" & sRecuperavel & "', "
    adSQL sSQL, "'" & sTipoSinistro & "', "
    adSQL sSQL, "'" & sTipoCobranca & "', "
    adSQL sSQL, iDiferencaId & ", "
    adSQL sSQL, "'" & sUsuario & "'"
    Call ExecutarSQL(sSiglaSistema, lAmbienteID, sSiglaRecurso, sDescricaoRecurso, sSQL, False)
    Alterar = True
    Exit Function
TratarErro:
    Call Err.Raise(Err.Number, , "cls00157 - AlterarDiferencaRep- " & Err.Description)
End Function
