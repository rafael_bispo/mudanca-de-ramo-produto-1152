VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cls00136"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit

Public Function ConsultarMapaRamoIRB(ByVal sSiglaSistema As String, _
                                     ByVal sSiglaRecurso As String, _
                                     ByVal sDescricaoRecurso As String, _
                                     ByVal iAmbienteId As Integer, _
                                     Optional ByVal iMapaResseguroId As Integer, _
                                     Optional ByVal iRamoIRBID As Integer) As Object
    Dim sSQL As String

    On Error GoTo Trata_Erro
    sSQL = ""
    sSQL = sSQL & "SELECT ramo_irb_tb.ramo_irb_id, " & vbNewLine
    sSQL = sSQL & "       ramo_irb_tb.nome, " & vbNewLine
    sSQL = sSQL & "       mapa_ramo_tb.mapa_ramo_id " & vbNewLine
    sSQL = sSQL & "  FROM mapa_ramo_tb "
    sSQL = sSQL & "  JOIN ramo_irb_tb " & vbNewLine
    sSQL = sSQL & "    ON ramo_irb_tb.ramo_irb_id = mapa_ramo_tb.ramo_irb_id " & vbNewLine
    sSQL = sSQL & " WHERE 1 = 1"

    If iMapaResseguroId > 0 Then
        sSQL = sSQL & "   AND mapa_ramo_tb.mapa_resseguro_id = " & iMapaResseguroId & vbNewLine
    End If

    If iRamoIRBID > 0 Then
        sSQL = sSQL & "   AND ramo_irb_tb.ramo_irb_id = " & iRamoIRBID & vbNewLine
    End If

    sSQL = sSQL & " ORDER BY ramo_irb_tb.ramo_irb_id"
    Set ConsultarMapaRamoIRB = ExecutarSQL(sSiglaSistema, iAmbienteId, sSiglaRecurso, sDescricaoRecurso, sSQL, True)
    Exit Function
Trata_Erro:
    Call Err.Raise(Err.Number, , "RSGL0002.cls00136.ConsultarMapaRamoIRB - " & Err.Description)
End Function

Public Function Consultar(ByVal sSiglaSistema As String, _
                          ByVal sSiglaRecurso As String, _
                          ByVal sDescricaoRecurso As String, _
                          ByVal lAmbienteID As Long, _
                          ByVal iTpConsulta As Integer, _
                          ByVal sConteudo As String, _
                          Optional ByVal iTipoRamo As Integer) As Object
    'iTpConsulta:
    ' 0 - Todos
    ' 1 - C�digo
    ' 2 - Nome
    ' 3 - Sigla
    '## Rotina de consulta dos Mapas de Resseguro
    Dim sSQL As String

    On Error GoTo Trata_Erro
    sSQL = ""
    adSQL sSQL, "   SELECT mapa_resseguro_tb.mapa_resseguro_id, "
    adSQL sSQL, "          sigla,"
    adSQL sSQL, "          mapa_resseguro_tb.nome nome, "
    adSQL sSQL, "          layout_tb.nome nome_layout, "
    adSQL sSQL, "          tp_inicializacao_num_ordem, "
    adSQL sSQL, "          mapa_resseguro_tb.layout_id, "
    adSQL sSQL, "          tp_repete_num_ordem_aviso, "
    adSQL sSQL, "          tp_agrupamento, "
    adSQL sSQL, "          tp_origem_mapa"
    adSQL sSQL, "     FROM mapa_resseguro_tb "
    adSQL sSQL, "     JOIN controle_proposta_db..layout_tb layout_tb"
    adSQL sSQL, "       ON layout_tb.layout_id = mapa_resseguro_tb.layout_id"

    ' Tipo de Ramo (1 - Vida / 2 - RE)
    If iTipoRamo > 0 Then
        adSQL sSQL, "     JOIN mapa_ramo_tb "
        adSQL sSQL, "       ON mapa_ramo_tb.mapa_resseguro_id = mapa_resseguro_tb.mapa_resseguro_id"
        adSQL sSQL, "     JOIN ramo_tb "
        adSQL sSQL, "       ON ramo_tb.ramo_irb_id = mapa_ramo_tb.ramo_irb_id"
        adSQL sSQL, "      AND ramo_tb.tp_ramo_id = " & iTipoRamo
    End If

    Select Case iTpConsulta
        Case 1
            adSQL sSQL, "    WHERE mapa_resseguro_id = " & Val(sConteudo)
        Case 2
            adSQL sSQL, "    WHERE mapa_resseguro_tb.nome like '" & MudaAspaSimples(sConteudo) & "%'"
        Case 3
            adSQL sSQL, "    WHERE sigla like '" & MudaAspaSimples(sConteudo) & "%'"
    End Select

    adSQL sSQL, "GROUP BY mapa_resseguro_tb.mapa_resseguro_id,"
    adSQL sSQL, "         sigla,"
    adSQL sSQL, "         mapa_resseguro_tb.nome,"
    adSQL sSQL, "         layout_tb.nome,"
    adSQL sSQL, "         tp_inicializacao_num_ordem,"
    adSQL sSQL, "         mapa_resseguro_tb.layout_id,"
    adSQL sSQL, "         tp_repete_num_ordem_aviso,"
    adSQL sSQL, "         tp_agrupamento,"
    adSQL sSQL, "         tp_origem_mapa"
    adSQL sSQL, "ORDER BY sigla"
    Set Consultar = ExecutarSQL(sSiglaSistema, lAmbienteID, sSiglaRecurso, sDescricaoRecurso, sSQL, True)
    Exit Function
Trata_Erro:
    Call Err.Raise(Err.Number, , "RSGL0002.cls00136.Consultar - " & Err.Description)
End Function

Public Function ConsultarRamo(ByVal sSiglaSistema As String, _
                              ByVal sSiglaRecurso As String, _
                              ByVal sDescricaoRecurso As String, _
                              ByVal lAmbienteID As Long, _
                              ByVal lMapaResseguroId As Long, _
                              ByVal iRamoIRBID As Integer) As Object
    '## Rotina de consulta dos Ramos para um Mapa de Resseguro
    Dim sSQL As String

    On Error GoTo Trata_Erro
    ' Instanciando a classe de conex�o ''''''''''''''''''''''''''''''''''''''''''
    sSQL = ""
    adSQL sSQL, "SELECT mapa_ramo_tb.ramo_irb_id, "
    adSQL sSQL, "       mapa_ramo_id, "
    adSQL sSQL, "       nome,"
    adSQL sSQL, "       ultimo_num_ordem, "
    adSQL sSQL, "       trata_aceito, trata_cedido, trata_direto,"
    adSQL sSQL, "       tp_valor_a_ser_enviado, "
    adSQL sSQL, "       lim_min_val_estimativa, "
    adSQL sSQL, "       lim_max_val_estimativa, "
    adSQL sSQL, "       reestimativa "
    adSQL sSQL, "FROM mapa_ramo_tb "
    adSQL sSQL, "JOIN ramo_irb_tb"
    adSQL sSQL, "  ON ramo_irb_tb.ramo_irb_id = mapa_ramo_tb.ramo_irb_id "
    adSQL sSQL, "WHERE 1 = 1"

    If lMapaResseguroId <> 0 Then
        adSQL sSQL, "  AND mapa_resseguro_id = " & lMapaResseguroId
    End If

    If iRamoIRBID <> 0 Then
        adSQL sSQL, "  AND mapa_ramo_tb.ramo_irb_id = " & iRamoIRBID
    End If

    adSQL sSQL, "ORDER BY mapa_ramo_tb.ramo_irb_id"
    Set ConsultarRamo = ExecutarSQL(sSiglaSistema, lAmbienteID, sSiglaRecurso, sDescricaoRecurso, sSQL, True)
    Exit Function
Trata_Erro:
    Call Err.Raise(Err.Number, , "RSGL0002.cls00136.ConsultarRamo - " & Err.Description)
End Function

Public Function ConsultarModalidadeMapaRamo(ByVal sSiglaSistema As String, _
                                            ByVal sSiglaRecurso As String, _
                                            ByVal sDescricaoRecurso As String, _
                                            ByVal lAmbienteID As Long, _
                                            ByVal lMapaRamoID As Long, _
                                            Optional ByVal lMapaResseguroId As Long) As Object
    '## Rotina de consulta dos Ramos para um Mapa de Resseguro
    Dim sSQL As String

    On Error GoTo Trata_Erro

    If lMapaRamoID > 0 Then
        ' Instanciando a classe de conex�o ''''''''''''''''''''''''''''''''''''''''''
        sSQL = ""
        adSQL sSQL, "SELECT mapa_ramo_modalidade_tb.modalidade_id,  "
        adSQL sSQL, "       mapa_ramo_id, "
        adSQL sSQL, "       nome,"
        adSQL sSQL, "       modalidade_tb.ramo_irb_id"
        adSQL sSQL, "FROM mapa_ramo_modalidade_tb "
        adSQL sSQL, "JOIN modalidade_tb"
        adSQL sSQL, "  ON modalidade_tb.modalidade_id = mapa_ramo_modalidade_tb.modalidade_id "
        adSQL sSQL, "WHERE mapa_ramo_id = " & lMapaRamoID
        adSQL sSQL, "ORDER BY mapa_ramo_modalidade_tb.modalidade_id"
    Else
        sSQL = ""
        adSQL sSQL, "SELECT DISTINCT"
        adSQL sSQL, "       modalidade_tb.modalidade_id, "
        adSQL sSQL, "       mapa_ramo_tb.mapa_ramo_id, "
        adSQL sSQL, "       modalidade_tb.nome,"
        adSQL sSQL, "       modalidade_tb.ramo_irb_id"
        adSQL sSQL, "  FROM mapa_ramo_tb "
        adSQL sSQL, "  JOIN mapa_ramo_modalidade_tb "
        adSQL sSQL, "    ON mapa_ramo_modalidade_tb.mapa_ramo_id = mapa_ramo_tb.mapa_ramo_id"
        adSQL sSQL, "  JOIN modalidade_tb"
        adSQL sSQL, "    ON modalidade_tb.modalidade_id = mapa_ramo_modalidade_tb.modalidade_id"
        adSQL sSQL, " WHERE mapa_ramo_tb.mapa_resseguro_id = " & lMapaResseguroId
        adSQL sSQL, " ORDER BY modalidade_tb.modalidade_id"
    End If

    Set ConsultarModalidadeMapaRamo = ExecutarSQL(sSiglaSistema, lAmbienteID, sSiglaRecurso, sDescricaoRecurso, sSQL, True)
    Exit Function
Trata_Erro:
    Call Err.Raise(Err.Number, , "RSGL0002.cls00136.ConsultarModalidadeMapaRamo - " & Err.Description)
End Function

Public Function ConsultarModalidadeRamo(ByVal sSiglaSistema As String, _
                                        ByVal sSiglaRecurso As String, _
                                        ByVal sDescricaoRecurso As String, _
                                        ByVal lAmbienteID As Long, _
                                        ByVal lRamoIRBID As Long, _
                                        ByVal lMapaRamoID As Long) As Object
    '## Rotina de consulta dos Ramos para um Mapa de Resseguro
    Dim sSQL As String

    On Error GoTo Trata_Erro
    ' Instanciando a classe de conex�o ''''''''''''''''''''''''''''''''''''''''''
    sSQL = ""
    adSQL sSQL, "SELECT modalidade_tb.modalidade_id,  "
    adSQL sSQL, "       nome,"
    adSQL sSQL, "       modalidade_tb.ramo_irb_id "
    adSQL sSQL, "FROM modalidade_tb"

    If lMapaRamoID <> 0 Then
        adSQL sSQL, "LEFT JOIN mapa_ramo_modalidade_tb"
        adSQL sSQL, "  ON mapa_ramo_modalidade_tb.mapa_ramo_id = " & lMapaRamoID
        adSQL sSQL, " AND mapa_ramo_modalidade_tb.modalidade_id = modalidade_tb.modalidade_id"
    End If

    adSQL sSQL, "WHERE modalidade_tb.ramo_irb_id = " & lRamoIRBID

    If lMapaRamoID <> 0 Then
        adSQL sSQL, "  AND mapa_ramo_modalidade_tb.mapa_ramo_modalidade_id IS NULL"
    End If

    adSQL sSQL, "ORDER BY modalidade_tb.modalidade_id"
    Set ConsultarModalidadeRamo = ExecutarSQL(sSiglaSistema, lAmbienteID, sSiglaRecurso, sDescricaoRecurso, sSQL, True)
    Exit Function
Trata_Erro:
    Call Err.Raise(Err.Number, , "RSGL0002.cls00136.ConsultarModalidadeRamo - " & Err.Description)
End Function

Public Function IncluirMapaRamo(ByVal sSiglaSistema As String, _
                                ByVal sSiglaRecurso As String, _
                                ByVal sDescricaoRecurso As String, _
                                ByVal lAmbienteID As Long, _
                                ByVal sTrataAceito As String, _
                                ByVal sTrataCedido As String, _
                                ByVal sTrataDireto As String, _
                                ByVal lMapaResseguroId As Long, _
                                ByVal lRamoIRBID As Long, _
                                ByVal sTpValorASerInformado As String, _
                                ByVal cMinValEstimativa As Currency, _
                                ByVal cMaxValEstimativa As Currency, _
                                ByVal sReestimativa As String, _
                                ByVal sUsuario As String) As Long

    '## Rotina de inclus�o de Ramos para um Mapa de Resseguro
    On Error GoTo Trata_Erro
    Dim sSQL       As String

    Dim oResultado As Object

    sSQL = "EXEC mapa_ramo_spi '" & sTrataAceito & "', "
    adSQL sSQL, "'" & sTrataCedido & "', "
    adSQL sSQL, "'" & sTrataDireto & "', "
    adSQL sSQL, lMapaResseguroId & ", "
    adSQL sSQL, lRamoIRBID & ", "
    adSQL sSQL, "'" & sTpValorASerInformado & "', "
    adSQL sSQL, MudaVirgulaParaPonto(cMinValEstimativa) & ", "
    adSQL sSQL, MudaVirgulaParaPonto(cMaxValEstimativa) & ", "
    adSQL sSQL, "'" & sReestimativa & "', "
    adSQL sSQL, "'" & sUsuario & "'"
    Set oResultado = ExecutarSQL(sSiglaSistema, lAmbienteID, sSiglaRecurso, sDescricaoRecurso, sSQL, True)
    IncluirMapaRamo = oResultado(0)
    Exit Function
Trata_Erro:
    Call Err.Raise(Err.Number, , "RSGL0002.cls00136.IncluirMapaRamo - " & Err.Description)
End Function

Public Function Incluir(ByVal sSiglaSistema As String, _
                        ByVal sSiglaRecurso As String, _
                        ByVal sDescricaoRecurso As String, _
                        ByVal lAmbienteID As Long, _
                        ByVal sNome As String, _
                        ByVal sSigla As String, _
                        ByVal lLayoutID As Long, _
                        ByVal sTpInicializacaoNumOrdem As String, _
                        ByVal sTpRepeteNumOrdemAviso As String, _
                        ByVal sTpAgrupamento As String, _
                        ByVal sOrigem As String, _
                        ByVal sUsuario As String) As Long

    '## Rotina de inclus�o de Mapas de Resseguro
    On Error GoTo Trata_Erro
    Dim sSQL       As String

    Dim oResultado As Object

    sSQL = "EXEC mapa_resseguro_spi '" & sNome & "', "
    adSQL sSQL, "'" & sSigla & "', "
    adSQL sSQL, lLayoutID & ", "
    adSQL sSQL, "'" & sTpInicializacaoNumOrdem & "', "
    adSQL sSQL, "'" & sUsuario & "',"
    adSQL sSQL, "'" & sTpRepeteNumOrdemAviso & "',"
    adSQL sSQL, "'" & sTpAgrupamento & "',"
    adSQL sSQL, "'" & sOrigem & "'"
    Set oResultado = ExecutarSQL(sSiglaSistema, lAmbienteID, sSiglaRecurso, sDescricaoRecurso, sSQL, True)
    Incluir = oResultado(0)
    Exit Function
Trata_Erro:
    Call Err.Raise(Err.Number, , "RSGL0002.cls00136.Incluir - " & Err.Description)
End Function

Public Function IncluirMapaRamoModalidade(ByVal sSiglaSistema As String, _
                                          ByVal sSiglaRecurso As String, _
                                          ByVal sDescricaoRecurso As String, _
                                          ByVal lAmbienteID As Long, _
                                          ByVal lModalidadeID As Long, _
                                          ByVal lMapaRamoID As String, _
                                          ByVal sUsuario As String) As Long

    '## Rotina de inclus�o de Ramos para um Mapa de Resseguro
    On Error GoTo Trata_Erro
    Dim sSQL            As String

    Dim oResultado      As Object

    Dim sInicioVigencia As String

    sSQL = "EXEC mapa_ramo_modalidade_spi "
    adSQL sSQL, lMapaRamoID & ", "
    adSQL sSQL, lModalidadeID & ", "
    adSQL sSQL, "'" & sUsuario & "'"
    Set oResultado = ExecutarSQL(sSiglaSistema, lAmbienteID, sSiglaRecurso, sDescricaoRecurso, sSQL, True)
    IncluirMapaRamoModalidade = oResultado(0)
    oResultado.Close
    Set oResultado = Nothing
    Exit Function
Trata_Erro:
    Call Err.Raise(Err.Number, , "RSGL0002.cls00136.IncluirMapaRamoModalidade - " & Err.Description)
End Function

Public Function Excluir(ByVal sSiglaSistema As String, _
                        ByVal sSiglaRecurso As String, _
                        ByVal sDescricaoRecurso As String, _
                        ByVal lAmbienteID As Long, _
                        ByVal lMapaResseguroId As Long) As Boolean

    '## Rotina de inclus�o de Mapas de Resseguro
    On Error GoTo Trata_Erro
    Dim sSQL       As String

    Dim oResultado As Object

    sSQL = "EXEC mapa_resseguro_spd " & lMapaResseguroId
    Call ExecutarSQL(sSiglaSistema, lAmbienteID, sSiglaRecurso, sDescricaoRecurso, sSQL, False)
    Excluir = True
    Exit Function
Trata_Erro:
    Call Err.Raise(Err.Number, , "RSGL0002.cls00136.Excluir - " & Err.Description)
End Function

Public Function ExcluirMapaRamo(ByVal sSiglaSistema As String, _
                                ByVal sSiglaRecurso As String, _
                                ByVal sDescricaoRecurso As String, _
                                ByVal lAmbienteID As Long, _
                                ByVal lMapaResseguroId As Long, _
                                ByVal lRamoID As Integer, _
                                ByVal lModalidadeID As Long) As Boolean

    '## Rotina de inclus�o de Mapas de Resseguro
    On Error GoTo Trata_Erro
    Dim sSQL       As String

    Dim oResultado As Object

    sSQL = "EXEC mapa_resseguro_ramo_spd " & lMapaResseguroId & ", " & lRamoID & ", " & lModalidadeID
    Call ExecutarSQL(sSiglaSistema, lAmbienteID, sSiglaRecurso, sDescricaoRecurso, sSQL, False)
    ExcluirMapaRamo = True
    Exit Function
Trata_Erro:
    Call Err.Raise(Err.Number, , "RSGL0002.cls00136.ExcluirMapaRamo - " & Err.Description)
End Function

Public Function Alterar(ByVal sSiglaSistema As String, _
                        ByVal sSiglaRecurso As String, _
                        ByVal sDescricaoRecurso As String, _
                        ByVal lAmbienteID As Long, _
                        ByVal lMapaResseguroId As Long, _
                        ByVal sNome As String, _
                        ByVal sSigla As String, _
                        ByVal lLayoutID As Long, _
                        ByVal sTpInicializacaoNumOrdem As String, _
                        ByVal sTpRepeteNumOrdemAviso As String, _
                        ByVal sTpAgrupamento As String, _
                        ByVal sOrigem As String, _
                        ByVal sUsuario As String) As Boolean

    '## Rotina de inclus�o de Mapas de Resseguro
    On Error GoTo Trata_Erro
    Dim sSQL       As String

    Dim oResultado As Object

    sSQL = "EXEC mapa_resseguro_spu " & lMapaResseguroId & ", "
    adSQL sSQL, "'" & sNome & "', "
    adSQL sSQL, "'" & sSigla & "', "
    adSQL sSQL, lLayoutID & ", "
    adSQL sSQL, "'" & sTpInicializacaoNumOrdem & "', "
    adSQL sSQL, "'" & sUsuario & "',"
    adSQL sSQL, "'" & sTpRepeteNumOrdemAviso & "',"
    adSQL sSQL, "'" & sTpAgrupamento & "',"
    adSQL sSQL, "'" & sOrigem & "'"
    Call ExecutarSQL(sSiglaSistema, lAmbienteID, sSiglaRecurso, sDescricaoRecurso, sSQL, False)
    Alterar = True
    Exit Function
Trata_Erro:
    Call Err.Raise(Err.Number, , "RSL0001.cls00136.Alterar - " & Err.Description)
End Function

Sub IncluirMapaContrato(ByVal sSiglaSistema As String, _
                        ByVal sSiglaRecurso As String, _
                        ByVal sDescricaoRecurso As String, _
                        ByVal lAmbienteID As Long, _
                        ByVal lMapaRamoID As Long, _
                        ByVal lModalidadeID As Long, _
                        ByVal sNumContrato As String, _
                        ByVal sUsuario As String)

    On Error GoTo Trata_Erro
    Dim sSQL As String

    sSQL = "EXEC mapa_ramo_modalidade_contrato_spi "
    adSQL sSQL, lModalidadeID & ", "
    adSQL sSQL, lMapaRamoID & ", "
    adSQL sSQL, "'" & sNumContrato & "', "
    adSQL sSQL, "'" & sUsuario & "'"
    Call ExecutarSQL(sSiglaSistema, lAmbienteID, sSiglaRecurso, sDescricaoRecurso, sSQL, False)
    Exit Sub
Trata_Erro:
    Call Err.Raise(Err.Number, , "RSGL0002.cls00138.IncluirMapaContrato - " & Err.Description)
End Sub

Sub AlterarMapaContrato(ByVal sSiglaSistema As String, _
                        ByVal sSiglaRecurso As String, _
                        ByVal sDescricaoRecurso As String, _
                        ByVal lAmbienteID As Long, _
                        ByVal lMapaRamoID As Long, _
                        ByVal lModalidadeID As Long, _
                        ByVal sNumContrato As String, _
                        ByVal sNumContratoAnterior As String, _
                        ByVal sUsuario As String)

    On Error GoTo Trata_Erro
    Dim sSQL                  As String

    Dim rs                    As Recordset

    Dim lMapaRamoModalidadeID As Long

    'Obtendo item a ser alterado ''''''''''''''''''''''''''''''''
    Set rs = ConsultarMapaContrato(sSiglaSistema, sSiglaRecurso, sDescricaoRecurso, lAmbienteID, 0, lMapaRamoID, lModalidadeID, sNumContratoAnterior)

    If Not rs.EOF Then
        lMapaRamoModalidadeID = rs("mapa_ramo_modalidade_contrato_id")
        sSQL = "EXEC mapa_ramo_modalidade_contrato_spu "
        adSQL sSQL, lMapaRamoModalidadeID & ", "
        adSQL sSQL, lModalidadeID & ", "
        adSQL sSQL, lMapaRamoID & ", "
        adSQL sSQL, "'" & sNumContrato & "', "
        adSQL sSQL, "'" & sUsuario & "'"
        Call ExecutarSQL(sSiglaSistema, lAmbienteID, sSiglaRecurso, sDescricaoRecurso, sSQL, False)
    End If

    rs.Close
    Set rs = Nothing
    Exit Sub
Trata_Erro:
    Call Err.Raise(Err.Number, , "RSGL0002.cls00136.AlterarMapaContrato - " & Err.Description)
End Sub

Sub ExcluirMapaContrato(ByVal sSiglaSistema As String, _
                        ByVal sSiglaRecurso As String, _
                        ByVal sDescricaoRecurso As String, _
                        ByVal lAmbienteID As Long, _
                        ByVal lMapaRamoID As Long, _
                        ByVal lModalidadeID As Long, _
                        ByVal sNumContrato As String)

    On Error GoTo Trata_Erro
    Dim sSQL                  As String

    Dim rs                    As Recordset

    Dim lMapaRamoModalidadeID As Long

    'Obtendo item a ser excluido ''''''''''''''''''''''''''''''''
    Set rs = ConsultarMapaContrato(sSiglaSistema, sSiglaRecurso, sDescricaoRecurso, lAmbienteID, 0, lMapaRamoID, lModalidadeID, sNumContrato)

    If Not rs.EOF Then
        lMapaRamoModalidadeID = rs("mapa_ramo_modalidade_contrato_id")
        sSQL = "EXEC mapa_ramo_modalidade_contrato_spd "
        adSQL sSQL, lMapaRamoModalidadeID
        Call ExecutarSQL(sSiglaSistema, lAmbienteID, sSiglaRecurso, sDescricaoRecurso, sSQL, False)
    End If

    Exit Sub
Trata_Erro:
    Call Err.Raise(Err.Number, , "RSGL0002.cls00XXX.ExcluirMapaContrato - " & Err.Description)
End Sub

Public Function ConsultarMapaContrato(ByVal sSiglaSistema As String, _
                                      ByVal sSiglaRecurso As String, _
                                      ByVal sDescricaoRecurso As String, _
                                      ByVal lAmbienteID As Long, _
                                      Optional ByVal lMapaRamoModalidadeID As Long, _
                                      Optional ByVal lMapaRamoID As Long, _
                                      Optional ByVal lModalidadeID As Long, _
                                      Optional ByVal sNumContrato As String) As Object
    Dim sSQL As String

    On Error GoTo Trata_Erro
    sSQL = ""
    adSQL sSQL, "SELECT *"
    adSQL sSQL, "  FROM mapa_ramo_modalidade_contrato_tb "
    adSQL sSQL, " WHERE 1 = 1 "

    If lMapaRamoModalidadeID > 0 Then
        adSQL sSQL, "   AND mapa_ramo_modalidade_contrato_id = " & lMapaRamoModalidadeID
    End If

    If lMapaRamoID > 0 Then
        adSQL sSQL, "   AND mapa_ramo_id = " & lMapaRamoID
    End If

    If lModalidadeID > 0 Then
        adSQL sSQL, "   AND modalidade_id = " & lModalidadeID
    End If

    If sNumContrato <> "" Then
        adSQL sSQL, "   AND num_contrato LIKE '%" & sNumContrato & "%'"
    End If

    adSQL sSQL, "ORDER BY mapa_ramo_modalidade_contrato_id"
    Set ConsultarMapaContrato = ExecutarSQL(sSiglaSistema, lAmbienteID, sSiglaRecurso, sDescricaoRecurso, sSQL, True)
    Exit Function
Trata_Erro:
    Call Err.Raise(Err.Number, , "RSGL0002.cls00129.ConsultarMapaContrato - " & Err.Description)
End Function

Public Function ConsultarRamoModalidadeMapa(ByVal sSiglaSistema As String, _
                                            ByVal sSiglaRecurso As String, _
                                            ByVal sDescricaoRecurso As String, _
                                            ByVal lAmbienteID As Long, _
                                            Optional ByVal lMapaRamoModalidadeID As Long, _
                                            Optional ByVal lRamoIRBID As Long, _
                                            Optional ByVal lModalidadeID As Long, _
                                            Optional ByVal lMapaResseguroId As Long, _
                                            Optional ByVal sNumContrato As String) As Object
    Dim sSQL As String

    On Error GoTo Trata_Erro
    sSQL = ""
    adSQL sSQL, "SELECT mapa_ramo_modalidade_contrato_tb.*,"
    adSQL sSQL, "       ramo_irb_tb.ramo_irb_id,"
    adSQL sSQL, "       mapa_ramo_tb.mapa_resseguro_id,"
    adSQL sSQL, "       modalidade_tb.cod_modalidade,"
    adSQL sSQL, "       modalidade_tb.nome nome_modalidade,"
    adSQL sSQL, "       modalidade_tb.classificacao_risco,"
    adSQL sSQL, "       mapa_resseguro_tb.nome nome_mapa,"
    adSQL sSQL, "       mapa_resseguro_tb.sigla,"
    adSQL sSQL, "       ramo_irb_tb.nome nome_ramo_irb"
    adSQL sSQL, "  FROM mapa_ramo_modalidade_contrato_tb "
    adSQL sSQL, "  JOIN modalidade_tb "
    adSQL sSQL, "    ON modalidade_tb.modalidade_id = mapa_ramo_modalidade_contrato_tb.modalidade_id"
    adSQL sSQL, "  JOIN ramo_irb_tb "
    adSQL sSQL, "    ON ramo_irb_tb.ramo_irb_id = modalidade_tb.ramo_irb_id "
    adSQL sSQL, "  JOIN mapa_ramo_tb "
    adSQL sSQL, "    ON mapa_ramo_tb.mapa_ramo_id = mapa_ramo_modalidade_contrato_tb.mapa_ramo_id "
    adSQL sSQL, "  JOIN mapa_resseguro_tb "
    adSQL sSQL, "    ON mapa_resseguro_tb.mapa_resseguro_id = mapa_ramo_tb.mapa_resseguro_id "
    adSQL sSQL, " WHERE 1 = 1 "

    If lMapaRamoModalidadeID > 0 Then
        adSQL sSQL, "   AND mapa_ramo_modalidade_contrato_tb.mapa_ramo_modalidade_contrato_id = " & lMapaRamoModalidadeID
    End If

    If lRamoIRBID > 0 Then
        adSQL sSQL, "   AND ramo_irb_tb.ramo_irb_id = " & lRamoIRBID
    End If

    If lModalidadeID > 0 Then
        adSQL sSQL, "   AND modalidade_tb.modalidade_id = " & lModalidadeID
    End If

    If lMapaResseguroId > 0 Then
        adSQL sSQL, "   AND mapa_resseguro_tb.mapa_resseguro_id = " & lMapaResseguroId
    End If

    If sNumContrato <> "" Then
        adSQL sSQL, "   AND mapa_ramo_modalidade_contrato_tb.num_contrato LIKE '%" & sNumContrato & "%'"
    End If

    adSQL sSQL, "ORDER BY mapa_ramo_modalidade_contrato_id"
    Set ConsultarRamoModalidadeMapa = ExecutarSQL(sSiglaSistema, lAmbienteID, sSiglaRecurso, sDescricaoRecurso, sSQL, True)
    Exit Function
Trata_Erro:
    Call Err.Raise(Err.Number, , "RSGL0002.cls00136.ConsultarRamoMapaModalidade - " & Err.Description)
End Function
