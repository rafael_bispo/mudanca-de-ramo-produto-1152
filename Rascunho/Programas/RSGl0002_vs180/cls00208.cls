VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cls00208"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True

Public Function Alterar(ByVal sSiglaSistema As String, _
                        ByVal sSiglaRecurso As String, _
                        ByVal sDescricaoRecurso As String, _
                        ByVal lAmbienteID As Long, _
                        ByVal lRamoID As Long, _
                        ByVal lSubRamoId As Long, _
                        ByVal lModalidadeID As Long, _
                        ByVal sUsuario As String) As Boolean

    'Rotina destinada a alterar modalidade da tabela subramo_tb'''''
    On Error GoTo TratarErro
    Dim sSQL As String

    sSQL = "EXEC subramo_spu "
    adSQL sSQL, lRamoID & ", "
    adSQL sSQL, lSubRamoId & ", "
    adSQL sSQL, lModalidadeID & ", "
    adSQL sSQL, "'" & sUsuario & "'"
    Call ExecutarSQL(sSiglaSistema, lAmbienteID, sSiglaRecurso, sDescricaoRecurso, sSQL, False)
    Alterar = True
    Exit Function
TratarErro:
    Call Err.Raise(Err.Number, , "cls00208 - AlterarSubRamo - " & Err.Description)
End Function

Public Function Consultar(ByVal sSiglaSistema As String, _
                          ByVal sSiglaRecurso As String, _
                          ByVal sDescricaoRecurso As String, _
                          ByVal lAmbienteID As Long, _
                          Optional ByVal iRamo As Integer, _
                          Optional ByVal iSubRamo As Integer, _
                          Optional ByVal iRamoIRB As Integer, _
                          Optional ByVal sCodModalidade As String, _
                          Optional ByVal iModalidadeId As Integer, _
                          Optional ByVal sClassificacao As String) As Object
    'Rotina para consulta da tabela SubRamo_tb
    Dim sSQL      As String

    Dim sNomeRamo As String

    On Error GoTo Trata_Erro
    sSQL = ""
    adSQL sSQL, "   SELECT ramo_tb.ramo_id, "
    adSQL sSQL, "          sNomeRamo = ramo_tb.nome, "
    adSQL sSQL, "          subramo_tb.subramo_id, "
    adSQL sSQL, "          sNomeSubRamo = subramo_tb.nome, "
    adSQL sSQL, "          subramo_tb.vincular_documento_contrato, "
    adSQL sSQL, "          ramo_irb_tb.ramo_irb_id , "
    adSQL sSQL, "          sNomeRamoIrb = ramo_irb_tb.nome, "
    adSQL sSQL, "          modalidade_tb.cod_modalidade , "
    adSQL sSQL, "          sNomeModalidade = modalidade_tb.nome, "
    adSQL sSQL, "          modalidade_tb.classificacao_risco, "
    adSQL sSQL, "          modalidade_tb.modalidade_id "
    adSQL sSQL, "     FROM subramo_tb"
    adSQL sSQL, "     JOIN ramo_tb "
    adSQL sSQL, "       ON ramo_tb.ramo_id = subramo_tb.ramo_id "
    adSQL sSQL, "LEFT JOIN modalidade_tb "
    adSQL sSQL, "       ON modalidade_tb.modalidade_id = subramo_tb.modalidade_id "
    adSQL sSQL, "     JOIN ramo_irb_tb "
    adSQL sSQL, "       ON ramo_irb_tb.ramo_irb_id = ramo_tb.ramo_irb_id"
    adSQL sSQL, "    WHERE 1 = 1 "

    If iRamo > 0 Then
        adSQL sSQL, "    AND ramo_tb.ramo_id = " & iRamo
    End If

    If iSubRamo > 0 Then
        adSQL sSQL, "    AND subramo_tb.subramo_id = " & iSubRamo
    End If

    If iRamoIRB > 0 Then
        adSQL sSQL, "    AND ramo_irb_tb.ramo_irb_id = " & iRamoIRB
    End If

    If sCodModalidade <> "" Then
        adSQL sSQL, "    AND modalidade_tb.cod_modalidade like '" & sCodModalidade & "%'"
    End If

    If iModalidadeId > 0 Then
        adSQL sSQL, "    AND modalidade_tb.modalidade_id = " & iModalidadeId
    End If

    If sClassificacao <> "" Then
        adSQL sSQL, "    AND modalidade_tb.classificacao_risco like '" & sClassificacao & "%'"
    End If

    adSQL sSQL, "       ORDER BY ramo_tb.ramo_id"
    Set Consultar = ExecutarSQL(sSiglaSistema, lAmbienteID, sSiglaRecurso, sDescricaoRecurso, sSQL, True)
    Exit Function
Trata_Erro:
    Call Err.Raise(Err.Number, , "RSGL0002.cls00208.Consultar - " & Err.Description)
End Function
