VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cls00267"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Public Function GerarPendenteResseguro(ByVal sSiglaSistema As String, _
                                       ByVal sSiglaRecurso As String, _
                                       ByVal sDescricaoRecurso As String, _
                                       ByVal iAmbienteId As Integer, _
                                       ByVal sDtVencimento As String, _
                                       ByVal sUsuario As String) As Long

    On Error GoTo Trata_Erro
    Dim lConexao As Long

    Dim sSQL     As String

    Dim oRs      As Recordset

    lConexao = AbrirConexao(sSiglaSistema, iAmbienteId, sSiglaRecurso, sDescricaoRecurso)

    'Abrindo Transa��o '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    If Not AbrirTransacao(lConexao) Then
        Call Err.Raise(vbObjectError + 1000, , "RSGL0001.cls00138.AtualizaResseguroVida_Premio - N�o foi poss�vel abrir transa��o")
    End If

    sSQL = ""
    adSQL sSQL, "EXEC gera_resseguro_pendente_spi"
    adSQL sSQL, "'" & sDtVencimento & "',"
    adSQL sSQL, "'" & sUsuario & "'"
    Set oRs = Conexao_ExecutarSQL(sSiglaSistema, iAmbienteId, sSiglaRecurso, sDescricaoRecurso, sSQL, lConexao, True, False, 30000, adLockOptimistic, adUseClient)
    GerarPendenteResseguro = oRs(0)
    'Fechando Transacao ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    Call ConfirmarTransacao(lConexao)
    Exit Function
Trata_Erro:
    Call Err.Raise(Err.Number, , "RSGL0002.cls00267.GerarPendenteResseguro - " & Err.Description)
End Function
