VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cls00422"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit

Public Enum HorizontalAlignmentExcelEnum
    xlLeft = &HFFFFEFDD
    xlCenter = &HFFFFEFF4
    xlRight = &HFFFFEFC8
End Enum

Public Function ObterPendenteResseguroSinistro(ByVal sSiglaSistema As String, _
                                               ByVal sSiglaRecurso As String, _
                                               ByVal sDescricaoRecurso As String, _
                                               ByVal lAmbiente As Long, _
                                               Optional ByVal sAnoMes As String, _
                                               Optional ByVal sItemEstimativa As String, _
                                               Optional ByVal iRamoId As Integer, _
                                               Optional ByVal iMoedaId As Integer) As Object
    Dim sSQL As String

    On Error GoTo TrataErro
    'fmonteiro em 16/03/2007
    'OBS: Este m�todo n�o deve ter os campos do select alterados, nem seus labels e ordem
    'de apresenta��o dos campos, pois os mesmos fazem parte da montagem do grid de uma
    'aplica��o que baseia-se na ordem em que os campos s�o retornados, os labels e seus
    'devidos campos. A altera��o da query implicar� no correto funcionamento da aplica��o.
    sSQL = ""
    sSQL = sSQL & "SELECT 'Ramo' = resseguro_fechamento_sinistro_sintetico_tb.ramo_id," & vbNewLine
    sSQL = sSQL & "       'Item de Estimativa' = item_val_estimativa_tb.nome," & vbNewLine
    sSQL = sSQL & "       'Pendente Anterior' = resseguro_fechamento_sinistro_sintetico_tb.val_resseguro_anterior," & vbNewLine
    sSQL = sSQL & "       'Emiss�o' = resseguro_fechamento_sinistro_sintetico_tb.val_resseguro_pgto," & vbNewLine
    sSQL = sSQL & "       'BSI' = resseguro_fechamento_sinistro_sintetico_tb.val_resseguro_bsi," & vbNewLine
    sSQL = sSQL & "       'Estorno Pgto' = resseguro_fechamento_sinistro_sintetico_tb.val_resseguro_estorno," & vbNewLine
    sSQL = sSQL & "       'Varia��o Cambial' = resseguro_fechamento_sinistro_sintetico_tb.val_resseguro_variacao_cambial," & vbNewLine
    sSQL = sSQL & "       'Pendente Calculado' = resseguro_fechamento_sinistro_sintetico_tb.val_resseguro_calculado," & vbNewLine
    sSQL = sSQL & "       'Pendente Real' = resseguro_fechamento_sinistro_sintetico_tb.val_resseguro_atual," & vbNewLine
    sSQL = sSQL & "       'Diferen�a' = resseguro_fechamento_sinistro_sintetico_tb.val_diferenca," & vbNewLine
    sSQL = sSQL & "       resseguro_fechamento_sinistro_sintetico_tb.ano_mes , " & vbNewLine
    sSQL = sSQL & "       resseguro_fechamento_sinistro_sintetico_tb.moeda_id," & vbNewLine
    sSQL = sSQL & "       resseguro_fechamento_sinistro_sintetico_tb.item_val_estimativa," & vbNewLine
    sSQL = sSQL & "       'moeda_nome' = moeda_tb.nome" & vbNewLine
    sSQL = sSQL & "FROM seguros_temp_db..resseguro_fechamento_sinistro_sintetico_tb resseguro_fechamento_sinistro_sintetico_tb (NOLOCK)" & vbNewLine
    sSQL = sSQL & "JOIN seguros_db..moeda_tb moeda_tb (NOLOCK)" & vbNewLine
    sSQL = sSQL & "  ON moeda_tb.moeda_id = resseguro_fechamento_sinistro_sintetico_tb.moeda_id" & vbNewLine
    sSQL = sSQL & "JOIN gds_db..item_val_estimativa_tb item_val_estimativa_tb (NOLOCK)" & vbNewLine
    sSQL = sSQL & "  ON item_val_estimativa_tb.item_val_estimativa = resseguro_fechamento_sinistro_sintetico_tb.item_val_estimativa" & vbNewLine
    sSQL = sSQL & "WHERE resseguro_fechamento_sinistro_sintetico_tb.ano_mes = '" & sAnoMes & "'" & vbNewLine
    sSQL = sSQL & "  AND resseguro_fechamento_sinistro_sintetico_tb.item_val_estimativa IN (" & sItemEstimativa & ")" & vbNewLine

    If iRamoId <> 9999 Then
        sSQL = sSQL & "  AND resseguro_fechamento_sinistro_sintetico_tb.ramo_id = " & iRamoId & vbNewLine
    End If

    If iMoedaId <> 9999 Then
        sSQL = sSQL & "  AND resseguro_fechamento_sinistro_sintetico_tb.moeda_id = " & iMoedaId & vbNewLine
    End If

    sSQL = sSQL & "ORDER BY resseguro_fechamento_sinistro_sintetico_tb.moeda_id," & vbNewLine
    sSQL = sSQL & "         resseguro_fechamento_sinistro_sintetico_tb.ramo_id," & vbNewLine
    sSQL = sSQL & "         resseguro_fechamento_sinistro_sintetico_tb.item_val_estimativa" & vbNewLine
    Set ObterPendenteResseguroSinistro = ExecutarSQL(sSiglaSistema, lAmbiente, sSiglaRecurso, sDescricaoRecurso, sSQL, True)
    Exit Function
TrataErro:
    Call Err.Raise(Err.Number, , "RSGL0002.cls00422.ObterPendenteResseguroSinistro - " & Err.Description)
End Function

Public Function ObterDiferencasPendenteResseguroSinistro(ByVal sSiglaSistema As String, _
                                                         ByVal sSiglaRecurso As String, _
                                                         ByVal sDescricaoRecurso As String, _
                                                         ByVal lAmbiente As Long, _
                                                         Optional ByVal sAnoMes As String, _
                                                         Optional ByVal sItemEstimativa As String, _
                                                         Optional ByVal iRamoId As Integer, _
                                                         Optional ByVal iMoedaId As Integer) As Object
    Dim sSQL As String

    On Error GoTo TrataErro
    'fmonteiro em 16/03/2007
    'OBS: Este m�todo n�o deve ter os campos do select alterados, nem seus labels e ordem
    'de apresenta��o dos campos, pois os mesmos fazem parte da montagem do grid de uma
    'aplica��o que baseia-se na ordem em que os campos s�o retornados, os labels e seus
    'devidos campos. A altera��o da query implicar� no correto funcionamento da aplica��o.
    sSQL = ""
    sSQL = sSQL & "SELECT 'Ramo' = resseguro_fechamento_sinistro_analitico_tb.ramo_id," & vbNewLine
    sSQL = sSQL & "       'Item de Estimativa' = item_val_estimativa_tb.nome," & vbNewLine
    sSQL = sSQL & "       'Sinistro' = resseguro_fechamento_sinistro_analitico_tb.sinistro_id," & vbNewLine
    sSQL = sSQL & "       'Estimativa' = resseguro_fechamento_sinistro_analitico_tb.seq_estimativa," & vbNewLine
    sSQL = sSQL & "       'C�d. Pgto' = resseguro_fechamento_sinistro_analitico_tb.seq_pgto," & vbNewLine
    sSQL = sSQL & "       'C�d. Benefici�rio' = resseguro_fechamento_sinistro_analitico_tb.beneficiario_id," & vbNewLine
    sSQL = sSQL & "       'Pendente Anterior' = resseguro_fechamento_sinistro_analitico_tb.val_resseguro_anterior," & vbNewLine
    sSQL = sSQL & "       'Emiss�o' = resseguro_fechamento_sinistro_analitico_tb.val_resseguro_pgto," & vbNewLine
    sSQL = sSQL & "       'BSI' = resseguro_fechamento_sinistro_analitico_tb.val_resseguro_bsi," & vbNewLine
    sSQL = sSQL & "       'Estorno Pgto' = resseguro_fechamento_sinistro_analitico_tb.val_resseguro_estorno," & vbNewLine
    sSQL = sSQL & "       'Varia��o Cambial' = resseguro_fechamento_sinistro_analitico_tb.val_resseguro_variacao_cambial," & vbNewLine
    sSQL = sSQL & "       'Pendente Calculado' = resseguro_fechamento_sinistro_analitico_tb.val_resseguro_calculado," & vbNewLine
    sSQL = sSQL & "       'Pendente Real' = resseguro_fechamento_sinistro_analitico_tb.val_resseguro_atual," & vbNewLine
    sSQL = sSQL & "       'Diferen�a' = resseguro_fechamento_sinistro_analitico_tb.val_diferenca," & vbNewLine
    sSQL = sSQL & "       resseguro_fechamento_sinistro_analitico_tb.ano_mes , " & vbNewLine
    sSQL = sSQL & "       resseguro_fechamento_sinistro_analitico_tb.moeda_id," & vbNewLine
    sSQL = sSQL & "       resseguro_fechamento_sinistro_analitico_tb.item_val_estimativa," & vbNewLine
    sSQL = sSQL & "       'moeda_nome' = moeda_tb.nome" & vbNewLine
    sSQL = sSQL & "FROM seguros_temp_db..resseguro_fechamento_sinistro_analitico_tb resseguro_fechamento_sinistro_analitico_tb (NOLOCK)" & vbNewLine
    sSQL = sSQL & "JOIN seguros_db..moeda_tb moeda_tb (NOLOCK)" & vbNewLine
    sSQL = sSQL & "  ON moeda_tb.moeda_id = resseguro_fechamento_sinistro_analitico_tb.moeda_id" & vbNewLine
    sSQL = sSQL & "JOIN gds_db..item_val_estimativa_tb item_val_estimativa_tb (NOLOCK)" & vbNewLine
    sSQL = sSQL & "  ON item_val_estimativa_tb.item_val_estimativa = resseguro_fechamento_sinistro_analitico_tb.item_val_estimativa" & vbNewLine
    sSQL = sSQL & "WHERE resseguro_fechamento_sinistro_analitico_tb.ano_mes = '" & sAnoMes & "'" & vbNewLine
    sSQL = sSQL & "  AND resseguro_fechamento_sinistro_analitico_tb.item_val_estimativa IN (" & sItemEstimativa & ")" & vbNewLine

    If iRamoId <> 9999 Then
        sSQL = sSQL & "  AND resseguro_fechamento_sinistro_analitico_tb.ramo_id = " & iRamoId & vbNewLine
    End If

    If iMoedaId <> 9999 Then
        sSQL = sSQL & "  AND resseguro_fechamento_sinistro_analitico_tb.moeda_id = " & iMoedaId & vbNewLine
    End If

    sSQL = sSQL & "ORDER BY resseguro_fechamento_sinistro_analitico_tb.moeda_id," & vbNewLine
    sSQL = sSQL & "         resseguro_fechamento_sinistro_analitico_tb.ramo_id," & vbNewLine
    sSQL = sSQL & "         resseguro_fechamento_sinistro_analitico_tb.item_val_estimativa" & vbNewLine
    Set ObterDiferencasPendenteResseguroSinistro = ExecutarSQL(sSiglaSistema, lAmbiente, sSiglaRecurso, sDescricaoRecurso, sSQL, True)
    Exit Function
TrataErro:
    Call Err.Raise(Err.Number, , "RSGL0002.cls00422.ObterDiferencasPendenteResseguroSinistro - " & Err.Description)
End Function

Public Function ExportarPendenteSinistroParaExcel(ByRef oMSFlexGrid As Object, _
                                                  ByVal sNomePlanilha As String, _
                                                  ByVal sTitulo As String, _
                                                  ByVal sParametro As String, _
                                                  ByVal sCaminhoArquivo As String)
    Dim oExcel     As Object 'Excel.Application

    Dim oWorkbook  As Object 'Excel.Workbook

    Dim oWorksheet As Object 'Excel.Worksheet

    Dim lRow       As Long

    Dim lCol       As Long

    Dim sCelula    As String

    Dim lColIniRel As Long

    Dim lRowIniRel As Long

    On Local Error GoTo Trata_Erro
    Set oExcel = CreateObject("Excel.Application")
    Set oWorkbook = oExcel.Workbooks.Add
    Set oWorksheet = oWorkbook.Worksheets("Plan1")
    oExcel.DisplayAlerts = False
    oWorksheet.name = sNomePlanilha
    lColIniRel = 65
    lRowIniRel = 1

    With oMSFlexGrid
        'HOLIVEIRA: Adiciona os parametros.
        sCelula = Chr(lColIniRel) & (lRowIniRel + lRow) & ":" & Chr(lColIniRel + (.Cols - 1)) & (lRowIniRel + lRow)
        oWorksheet.Range(sCelula).Merge
        oWorksheet.Range(sCelula) = sParametro
        oWorksheet.Range(sCelula).Font.Bold = .CellFontBold
        lRowIniRel = lRowIniRel + 2

        For lRow = 0 To .Rows - 1
            For lCol = 0 To .Cols - 1
                .Row = lRow
                .Col = lCol
                sCelula = Chr(lColIniRel + lCol) & ":" & Chr(lColIniRel + lCol)
                'oWorksheet.Columns(sCelula).EntireColumn.AutoFit
                oWorksheet.Columns(sCelula).ColumnWidth = (.ColWidth(lCol) / 100)
                sCelula = Chr(lColIniRel + lCol) & (lRowIniRel + lRow)
                oWorksheet.Range(sCelula) = .TextMatrix(lRow, lCol)
                oWorksheet.Range(sCelula).Interior.Color = IIf(CLng(.CellBackColor) = CLng(.BackColorFixed), 12632256, 16777215)
                oWorksheet.Range(sCelula).Borders.LineStyle = xlContinuous
                oWorksheet.Range(sCelula).HorizontalAlignment = HorizontalAlignmentExcel(.CellAlignment)
                oWorksheet.Range(sCelula).Font.Bold = .CellFontBold
            Next lCol

            If .MergeRow(.Row) = True Then
                sCelula = Chr(lColIniRel) & (lRowIniRel + lRow) & ":" & Chr(lColIniRel + (.Cols - 1)) & (lRowIniRel + lRow)
                oWorksheet.Range(sCelula).Merge
            End If

        Next lRow

    End With

    oWorksheet.PageSetup.Zoom = False
    oWorksheet.PageSetup.FitToPagesTall = 1
    oWorksheet.PageSetup.FitToPagesWide = 1

    'HOLIVEIRA: Configura para impress�o.
    With oWorksheet.PageSetup
        .Orientation = xlLandscape
        .CenterHorizontally = True
        .CenterHeader = "&""Arial,Negrito""&14" & sTitulo
        .RightFooter = "Gerado em " & Format(Now, "dd/mm/yyyy")
        .CenterFooter = "P�gina &P de &N"
    End With

    oWorkbook.Worksheets("Plan2").Delete
    oWorkbook.Worksheets("Plan3").Delete
    oWorkbook.SaveAs sCaminhoArquivo
    'fmonteiro: Exibe a planilha excel.
    oExcel.Visible = True
    Set oWorksheet = Nothing
    Set oWorkbook = Nothing
    Set oExcel = Nothing
    Exit Function

    Resume
Trata_Erro:
    Call Err.Raise(Err.Number, , "RSGL0002.cls00422.ExportarPendenteSinistroParaExcel - " & Err.Description)
End Function

Public Function HorizontalAlignmentExcel(ByVal lCellAlignment As Long) As HorizontalAlignmentExcelEnum

    Select Case lCellAlignment
        Case 0
            HorizontalAlignmentExcel = xlLeft
        Case 6
            HorizontalAlignmentExcel = xlRight
        Case 3
            HorizontalAlignmentExcel = xlCenter
    End Select

End Function

Public Function ObterPendenteResseguroSintetico(ByVal sSiglaSistema As String, _
                                                ByVal sSiglaRecurso As String, _
                                                ByVal sDescricaoRecurso As String, _
                                                ByVal lAmbiente As Long, _
                                                ByVal sAnoMes As String, _
                                                ByVal iRamoId As Integer, _
                                                ByVal iMoedaId As Integer, _
                                                ByVal iVisualiza As Integer, _
                                                ByVal iTipoFechamento As Integer) As Object
    Dim sSQL As String

    On Error GoTo TrataErro
    sSQL = ""
    sSQL = "EXEC resseg_db..RSGS0294_SPS "
    sSQL = sSQL & iRamoId
    sSQL = sSQL & ", " & iMoedaId
    sSQL = sSQL & ", " & sAnoMes
    sSQL = sSQL & ", " & iVisualiza
    sSQL = sSQL & ", " & iTipoFechamento
    Set ObterPendenteResseguroSintetico = ExecutarSQL(sSiglaSistema, lAmbiente, sSiglaRecurso, sDescricaoRecurso, sSQL, True)
    Exit Function
TrataErro:
    Call Err.Raise(Err.Number, , "RSGL0002.cls00422.ObterPendenteResseguroSintetico - " & Err.Description)
End Function

Public Function ObterPendenteResseguroAnalitico(ByVal sSiglaSistema As String, _
                                                ByVal sSiglaRecurso As String, _
                                                ByVal sDescricaoRecurso As String, _
                                                ByVal lAmbiente As Long, _
                                                ByVal sAnoMes As String, _
                                                ByVal iRamoId As Integer, _
                                                ByVal iMoedaId As Integer, _
                                                ByVal iVisualiza As Integer, _
                                                ByVal iTipoFechamento As Integer) As Object
    Dim sSQL As String

    On Error GoTo TrataErro
    sSQL = ""
    sSQL = "EXEC resseg_db..RSGS0295_SPS "
    sSQL = sSQL & iRamoId
    sSQL = sSQL & ", " & iMoedaId
    sSQL = sSQL & ", " & sAnoMes
    sSQL = sSQL & ", " & iVisualiza
    sSQL = sSQL & ", " & iTipoFechamento
    Set ObterPendenteResseguroAnalitico = ExecutarSQL(sSiglaSistema, lAmbiente, sSiglaRecurso, sDescricaoRecurso, sSQL, True)
    Exit Function
TrataErro:
    Call Err.Raise(Err.Number, , "RSGL0002.cls00422.ObterPendenteResseguroAnalitico - " & Err.Description)
End Function

'----------------------------------------------------------------------------------
' Justificativa de Diferen�as do Fechamento de Resseguro
'----------------------------------------------------------------------------------
'fsa - 04/05/2007
Public Function ConsultarJustificativa(ByVal sSiglaSistema As String, _
                                       ByVal sSiglaRecurso As String, _
                                       ByVal sDescricaoRecurso As String, _
                                       ByVal iAmbienteId As Integer, _
                                       ByVal sAnoMes As String, _
                                       ByVal iTpFechamento As Integer, _
                                       ByVal iMoeda As Integer, _
                                       Optional ByVal iRamo As Integer, _
                                       Optional ByVal iProduto As Integer, _
                                       Optional ByVal cValor As Currency) As Object
    'Rotina para consulta da tabela reseguros_justificativa_fechamento_tb
    Dim sSQL As String

    On Error GoTo Trata_Erro
    sSQL = ""
    adSQL sSQL, "SELECT justificativa_id,ano_mes,"
    adSQL sSQL, "       tp_fechamento,"
    adSQL sSQL, "       ramo_id,"
    adSQL sSQL, "       produto_id,"
    adSQL sSQL, "       cod_moeda,"
    adSQL sSQL, "       val_diferenca,"
    adSQL sSQL, "       descricao"
    adSQL sSQL, "  FROM seguros_temp_db..resseguro_justificativa_fechamento_tb"
    adSQL sSQL, " WHERE 1 = 1"

    If sAnoMes <> 0 Then
        adSQL sSQL, "  AND ano_mes = '" & sAnoMes & "'"
    End If

    If iTpFechamento <> 0 Then
        adSQL sSQL, "  AND tp_fechamento = " & iTpFechamento
    End If

    If iMoeda <> 2 Then
        adSQL sSQL, "  AND cod_moeda = " & iMoeda
    End If

    If iRamo <> 0 Then
        adSQL sSQL, "  AND ramo_id = " & iRamo
    End If

    If iProduto <> 0 Then
        adSQL sSQL, "  AND produto_id = " & iProduto
    End If

    If cValor <> 0 Then
        adSQL sSQL, "  AND val_diferenca = " & cValor
    End If

    adSQL sSQL, " ORDER BY ano_mes"
    Set ConsultarJustificativa = ExecutarSQL(sSiglaSistema, iAmbienteId, sSiglaRecurso, sDescricaoRecurso, sSQL)
    Exit Function
Trata_Erro:
    Call Err.Raise(Err.Number, , "RSGL0002.cls00422 - ConsultarJustificativa - " & Err.Description)
End Function

Public Function VerificarJustificativa(ByVal sSiglaSistema As String, _
                                       ByVal sSiglaRecurso As String, _
                                       ByVal sDescricaoRecurso As String, _
                                       ByVal iAmbienteId As Integer, _
                                       ByVal iJustificativaId As Integer) As Object
    'Rotina para consulta da tabela reseguros_justificativa_fechamento_tb
    Dim sSQL As String

    On Error GoTo Trata_Erro
    sSQL = ""
    adSQL sSQL, "SELECT justificativa_id, ano_mes,"
    adSQL sSQL, "       tp_fechamento,"
    adSQL sSQL, "       ramo_id,"
    adSQL sSQL, "       produto_id,"
    adSQL sSQL, "       cod_moeda,"
    adSQL sSQL, "       val_diferenca,"
    adSQL sSQL, "       descricao"
    adSQL sSQL, "  FROM seguros_temp_db..resseguro_justificativa_fechamento_tb"
    adSQL sSQL, " WHERE justificativa_id = " & iJustificativaId
    Set VerificarJustificativa = ExecutarSQL(sSiglaSistema, iAmbienteId, sSiglaRecurso, sDescricaoRecurso, sSQL)
    Exit Function
Trata_Erro:
    Call Err.Raise(Err.Number, , "RSGL0002.cls00422 - ConsultarJustificativa - " & Err.Description)
End Function

Public Function ConsultarRamo(ByVal sSiglaSistema As String, _
                              ByVal sSiglaRecurso As String, _
                              ByVal sDescricaoRecurso As String, _
                              ByVal iAmbienteId As Integer) As Object
    'Rotina para consulta da tabela ramo_tb
    Dim sSQL As String

    On Error GoTo Trata_Erro
    sSQL = ""
    adSQL sSQL, "SELECT ramo_id, " & vbNewLine
    adSQL sSQL, "          nome " & vbNewLine
    adSQL sSQL, "  FROM seguros_db..ramo_tb" & vbNewLine
    adSQL sSQL, " ORDER BY ramo_id" & vbNewLine
    Set ConsultarRamo = ExecutarSQL(sSiglaSistema, iAmbienteId, sSiglaRecurso, sDescricaoRecurso, sSQL)
    Exit Function
Trata_Erro:
    Call Err.Raise(Err.Number, , "RSGL0002.cls00422 - ConsultarRamo - " & Err.Description)
End Function

Public Function ConsultarProduto(ByVal sSiglaSistema As String, _
                                 ByVal sSiglaRecurso As String, _
                                 ByVal sDescricaoRecurso As String, _
                                 ByVal iAmbienteId As Integer, _
                                 ByVal iRamoId As Integer) As Object
    'Rotina para consulta da tabela produto_tb
    Dim sSQL As String

    On Error GoTo Trata_Erro
    sSQL = ""
    adSQL sSQL, "SELECT produto_tb.produto_id, " & vbNewLine
    adSQL sSQL, "       produto_tb.nome" & vbNewLine
    adSQL sSQL, "FROM seguros_db..produto_tb produto_tb" & vbNewLine
    adSQL sSQL, "JOIN seguros_db..item_produto_tb item_produto_tb" & vbNewLine
    adSQL sSQL, "  ON item_produto_tb.produto_id = produto_tb.produto_id " & vbNewLine

    If iRamoId <> 0 Then
        adSQL sSQL, " WHERE item_produto_tb.ramo_id = " & iRamoId & vbNewLine
    End If

    adSQL sSQL, "ORDER BY produto_tb.produto_id" & vbNewLine
    Set ConsultarProduto = ExecutarSQL(sSiglaSistema, iAmbienteId, sSiglaRecurso, sDescricaoRecurso, sSQL)
    Exit Function
Trata_Erro:
    Call Err.Raise(Err.Number, , "RSGL0002.cls00422 - ConsultarProduto - " & Err.Description)
End Function

Public Function IncluirJustificativa(ByVal sSiglaSistema As String, _
                                     ByVal sSiglaRecurso As String, _
                                     ByVal sDescricaoRecurso As String, _
                                     ByVal lAmbienteID As Long, _
                                     ByVal sAnoMes As String, _
                                     ByVal iTpFechamento As Integer, _
                                     ByVal iRamo As Integer, _
                                     ByVal iProduto As Integer, _
                                     ByVal iMoeda As Integer, _
                                     ByVal lValDiferenca As Currency, _
                                     ByVal sDescricao As String, _
                                     ByVal sUsuario As String) As Long

    On Error GoTo Trata_Erro
    Dim sSQL As String

    IncluirJustificativa = False
    sSQL = ""
    adSQL sSQL, "EXEC RESSEG_DB..RSGS0298_SPI "
    adSQL sSQL, "'" & sAnoMes & "',"
    adSQL sSQL, "" & iTpFechamento & ","
    adSQL sSQL, "" & iRamo & ","
    adSQL sSQL, "" & iProduto & ","
    adSQL sSQL, "" & iMoeda & ","
    adSQL sSQL, "" & MudaVirgulaParaPonto(lValDiferenca) & ","
    adSQL sSQL, "'" & sDescricao & "',"
    adSQL sSQL, "'" & sUsuario & "'"
    Call ExecutarSQL(sSiglaSistema, lAmbienteID, sSiglaRecurso, sDescricaoRecurso, sSQL, False)
    IncluirJustificativa = True
    Exit Function
Trata_Erro:
    ''Call Err.Raise(Err.Number, , "RSGL0002.cls00422 - IncluirTipoSinistro - " & Err.Description)
End Function

Public Function ExcluirJustificativa(ByVal sSiglaSistema As String, _
                                     ByVal sSiglaRecurso As String, _
                                     ByVal sDescricaoRecurso As String, _
                                     ByVal lAmbienteID As Long, _
                                     ByVal iJustificativaId As Integer) As Boolean

    On Error GoTo Trata_Erro
    Dim sSQL As String

    sSQL = ""
    adSQL sSQL, "EXEC RESSEG_DB..RSGS0300_SPD"
    adSQL sSQL, "'" & iJustificativaId & "'"
    Call ExecutarSQL(sSiglaSistema, lAmbienteID, sSiglaRecurso, sDescricaoRecurso, sSQL, False)
    Exit Function
Trata_Erro:
    Call Err.Raise(Err.Number, , "RSGL0002.cls00422 - ExcluirJustificativa - " & Err.Description)
End Function

Public Function AlterarJustificativa(ByVal sSiglaSistema As String, _
                                     ByVal sSiglaRecurso As String, _
                                     ByVal sDescricaoRecurso As String, _
                                     ByVal lAmbienteID As Long, _
                                     ByVal sAnoMes As String, _
                                     ByVal iTpFechamento As Integer, _
                                     ByVal iRamo As Integer, _
                                     ByVal iProduto As Integer, _
                                     ByVal iMoeda As Integer, _
                                     ByVal cValDiferenca As Currency, _
                                     ByVal sDescricao As String, _
                                     ByVal iJustificativa As Integer, _
                                     ByVal sUsuario As String) As Long

    On Error GoTo Trata_Erro
    Dim sSQL As String

    AlterarJustificativa = False
    sSQL = ""
    adSQL sSQL, "EXEC RESSEG_DB..RSGS0299_SPU "
    adSQL sSQL, "'" & sAnoMes & "',"
    adSQL sSQL, "" & iTpFechamento & ","
    adSQL sSQL, "" & iRamo & ","
    adSQL sSQL, "" & iProduto & ","
    adSQL sSQL, "" & iMoeda & ","
    adSQL sSQL, "" & MudaVirgulaParaPonto(cValDiferenca) & ","
    adSQL sSQL, "'" & sDescricao & "',"
    adSQL sSQL, "'" & iJustificativa & "',"
    adSQL sSQL, "'" & sUsuario & "'"
    Call ExecutarSQL(sSiglaSistema, lAmbienteID, sSiglaRecurso, sDescricaoRecurso, sSQL, False)
    AlterarJustificativa = True
    Exit Function
Trata_Erro:
    Call Err.Raise(Err.Number, , "RSGL0002.cls00422 - AlterarJustificativa - " & Err.Description)
End Function

'----------------------------------------------------------------------------------
' LIBERA��O PARA FECHAMENTO DE RESSEGURO
'----------------------------------------------------------------------------------
'fsa
Public Function ConsultaCancEstudo(ByVal sSiglaSistema As String, _
                                   ByVal sSiglaRecurso As String, _
                                   ByVal sDescricaoRecurso As String, _
                                   ByVal lAmbienteID As Long, _
                                   ByVal sAnoMes As String, _
                                   ByVal sUsuario As String) As Object
    'Consulta resseguro cancelados em estudo
    Dim sSQL As String

    On Error GoTo Trata_Erro
    sSQL = ""
    adSQL sSQL, "EXEC resseg_db..RSGS0304_SPI"
    adSQL sSQL, "'" & sAnoMes & "',"
    adSQL sSQL, "'" & sUsuario & "'"
    Set ConsultaCancEstudo = ExecutarSQL(sSiglaSistema, lAmbienteID, sSiglaRecurso, sDescricaoRecurso, sSQL, True)
    Exit Function
Trata_Erro:
    Call Err.Raise(Err.Number, , "RSGL0002.cls00421.ConsultaCancEstudo - " & Err.Description)
End Function

Public Function ObterDadosCanEstudo(ByVal sSiglaSistema As String, _
                                    ByVal sSiglaRecurso As String, _
                                    ByVal sDescricaoRecurso As String, _
                                    ByVal lAmbienteID As Long, _
                                    ByVal sAnoMes As String) As Object
    Dim sSQL As String

    On Error GoTo TrataErro
    sSQL = ""
    adSQL sSQL, "EXEC resseg_db..RSGS0301_SPS"
    sSQL = sSQL & "'" & sAnoMes & "'"
    Set ObterDadosCanEstudo = ExecutarSQL(sSiglaSistema, lAmbienteID, sSiglaRecurso, sDescricaoRecurso, sSQL, True)
    Exit Function
TrataErro:
    Call Err.Raise(Err.Number, , "RSGL0002.cls00422.ObterDadosCanEstudo - " & Err.Description)
End Function

Public Function ObterDadosCotaEstudo(ByVal sSiglaSistema As String, _
                                     ByVal sSiglaRecurso As String, _
                                     ByVal sDescricaoRecurso As String, _
                                     ByVal lAmbienteID As Long, _
                                     ByVal sAnoMes As String) As Object
    Dim sSQL As String

    On Error GoTo TrataErro
    sSQL = ""
    adSQL sSQL, "EXEC resseg_db..RSGS0302_SPS"
    sSQL = sSQL & "'" & sAnoMes & "'"
    Set ObterDadosCotaEstudo = ExecutarSQL(sSiglaSistema, lAmbienteID, sSiglaRecurso, sDescricaoRecurso, sSQL, True)
    Exit Function
TrataErro:
    Call Err.Raise(Err.Number, , "RSGL0002.cls00422.ObterDadosCotaEstudo - " & Err.Description)
End Function

Public Function ValidaLiberacao(ByVal sSiglaSistema As String, _
                                ByVal sSiglaRecurso As String, _
                                ByVal sDescricaoRecurso As String, _
                                ByVal lAmbienteID As Long, _
                                ByVal sAnoMes As String, _
                                ByVal sUsuario As String) As Object
    Dim sSQL As String

    On Error GoTo Trata_Erro
    sSQL = ""
    adSQL sSQL, "EXEC resseg_db..RSGS0303_SPS"
    adSQL sSQL, "'" & sAnoMes & "'"
    Set ValidaLiberacao = ExecutarSQL(sSiglaSistema, lAmbienteID, sSiglaRecurso, sDescricaoRecurso, sSQL, True)
    Exit Function
Trata_Erro:
    Call Err.Raise(Err.Number, , "RSGL0002.cls00421.ValidaLiberacao - " & Err.Description)
End Function

Public Function ConsultaCobCotaEstudo(ByVal sSiglaSistema As String, _
                                      ByVal sSiglaRecurso As String, _
                                      ByVal sDescricaoRecurso As String, _
                                      ByVal lAmbienteID As Long, _
                                      ByVal sAnoMes As String, _
                                      ByVal sUsuario As String) As Object
    'Consulta Documentos com Cobertura de Cota em Estudo
    Dim sSQL As String

    On Error GoTo Trata_Erro
    sSQL = ""
    adSQL sSQL, "EXEC resseg_db..RSGS0287_SPS"
    adSQL sSQL, "2,"
    adSQL sSQL, "'" & sUsuario & "'"
    Call ExecutarSQL(sSiglaSistema, lAmbienteID, sSiglaRecurso, sDescricaoRecurso, sSQL, False)
    Exit Function
Trata_Erro:
    Call Err.Raise(Err.Number, , "RSGL0002.cls00421.ConsultaCobCotaEstudo - " & Err.Description)
End Function

Public Function LiberaFechamentoResseguro(ByVal sSiglaSistema As String, _
                                          ByVal sSiglaRecurso As String, _
                                          ByVal sDescricaoRecurso As String, _
                                          ByVal lAmbienteID As Long, _
                                          ByVal sAnoMes As String, _
                                          ByVal sUsuario As String, _
                                          ByVal Dt_Liberacao As String) As Object
    Dim sSQL As String

    On Error GoTo Trata_Erro
    sSQL = ""
    adSQL sSQL, "EXEC resseg_db..RSGS0305_SPI "
    adSQL sSQL, "'" & sAnoMes & "',"
    adSQL sSQL, "'" & sUsuario & "',"
    adSQL sSQL, "'" & Dt_Liberacao & "'"
    Call ExecutarSQL(sSiglaSistema, lAmbienteID, sSiglaRecurso, sDescricaoRecurso, sSQL, False)
    Exit Function
Trata_Erro:
    Call Err.Raise(Err.Number, , "RSGL0002.cls00421.LiberaFechamentoResseguro - " & Err.Description)
End Function

Public Function ObterComparativoSinistroMOIRB(ByVal sSiglaSistema As String, _
                                              ByVal sSiglaRecurso As String, _
                                              ByVal sDescricaoRecurso As String, _
                                              ByVal lAmbiente As Long, _
                                              ByVal MoInicial As String, _
                                              ByVal MoFinal As String, _
                                              ByVal iMoedaId As Integer) As Object
    Dim sSQL As String

    On Error GoTo TrataErro
    sSQL = ""
    sSQL = "EXEC resseg_db..RSGS0317_SPS"
    sSQL = sSQL & " " & MoInicial & ", " & MoFinal & ", " & iMoedaId
    Set ObterComparativoSinistroMOIRB = ExecutarSQL(sSiglaSistema, lAmbiente, sSiglaRecurso, sDescricaoRecurso, sSQL, True)
    Exit Function
TrataErro:
    Call Err.Raise(Err.Number, , "RSGL0002.cls00422.ObterComparativoSinistroMOIRB - " & Err.Description)
End Function

Public Function ObterTipoRelatorioResseguro(ByVal sSiglaSistema As String, _
                                            ByVal sSiglaRecurso As String, _
                                            ByVal sDescricaoRecurso As String, _
                                            ByVal lAmbiente As Long, _
                                            ByVal MoInicial As String, _
                                            ByVal MoFinal As String, _
                                            ByVal iMoedaId As Integer, _
                                            ByVal iTipoRelatorio As Integer) As Object
    Dim sSQL As String

    On Error GoTo TrataErro
    sSQL = ""
    sSQL = "EXEC resseg_db..RSGS0315_SPS"
    sSQL = sSQL & " " & MoInicial & ", " & MoFinal & ", " & iMoedaId & ", " & iTipoRelatorio
    Set ObterTipoRelatorioResseguro = ExecutarSQL(sSiglaSistema, lAmbiente, sSiglaRecurso, sDescricaoRecurso, sSQL, True)
    Exit Function
TrataErro:
    Call Err.Raise(Err.Number, , "RSGL0002.cls00422.ObterTipoRelatorioResseguro - " & Err.Description)
End Function

Public Function ExportarPremioOuComissaoMOParaExcel(ByRef oMSFlexGrid As Object, _
                                                    ByVal sNomePlanilha As String, _
                                                    ByVal sTitulo As String, _
                                                    ByVal sParametro As String, _
                                                    ByVal sCaminhoArquivo As String)
    Dim oExcel     As Object 'Excel.Application

    Dim oWorkbook  As Object 'Excel.Workbook

    Dim oWorksheet As Object 'Excel.Worksheet

    Dim lRow       As Long

    Dim lCol       As Long

    Dim sCelula    As String

    Dim lColIniRel As Long

    Dim lRowIniRel As Long

    On Local Error GoTo Trata_Erro
    Set oExcel = CreateObject("Excel.Application")
    Set oWorkbook = oExcel.Workbooks.Add
    Set oWorksheet = oWorkbook.Worksheets("Plan1")
    oExcel.DisplayAlerts = False
    oWorksheet.name = sNomePlanilha
    lColIniRel = 65
    lRowIniRel = 1

    With oMSFlexGrid
        'HOLIVEIRA: Adiciona os parametros.
        sCelula = Chr(lColIniRel) & (lRowIniRel + lRow) & ":" & Chr(lColIniRel + (.Cols - 1)) & (lRowIniRel + lRow)
        oWorksheet.Range(sCelula).Merge
        oWorksheet.Range(sCelula) = sParametro
        oWorksheet.Range(sCelula).Font.Bold = .CellFontBold
        lRowIniRel = lRowIniRel + 2

        For lRow = 0 To .Rows - 1
            For lCol = 0 To .Cols - 1
                .Row = lRow
                .Col = lCol
                sCelula = Chr(lColIniRel + lCol) & ":" & Chr(lColIniRel + lCol)
                'oWorksheet.Columns(sCelula).EntireColumn.AutoFit
                oWorksheet.Columns(sCelula).ColumnWidth = (.ColWidth(lCol) / 85)
                sCelula = Chr(lColIniRel + lCol) & (lRowIniRel + lRow)
                oWorksheet.Range(sCelula) = .TextMatrix(lRow, lCol)
                oWorksheet.Range(sCelula).Interior.Color = IIf(CLng(.CellBackColor) = CLng(.BackColorFixed), 12632256, 16777215)
                oWorksheet.Range(sCelula).Borders.LineStyle = xlContinuous
                oWorksheet.Range(sCelula).HorizontalAlignment = HorizontalAlignmentExcel(.CellAlignment)
                oWorksheet.Range(sCelula).Font.Bold = .CellFontBold
            Next lCol

            If .MergeRow(.Row) = True Then
                sCelula = Chr(lColIniRel) & (lRowIniRel + lRow) & ":" & Chr(lColIniRel + (.Cols - 1)) & (lRowIniRel + lRow)
                oWorksheet.Range(sCelula).Merge
            End If

        Next lRow

    End With

    oWorksheet.PageSetup.Zoom = False
    oWorksheet.PageSetup.FitToPagesTall = 1
    oWorksheet.PageSetup.FitToPagesWide = 1

    'HOLIVEIRA: Configura para impress�o.
    With oWorksheet.PageSetup
        .Orientation = xlLandscape
        .CenterHorizontally = True
        .CenterHeader = "&""Arial,Negrito""&14" & sTitulo
        .RightFooter = "Gerado em " & Format(Now, "dd/mm/yyyy")
        .CenterFooter = "P�gina &P de &N"
    End With

    oWorkbook.Worksheets("Plan2").Delete
    oWorkbook.Worksheets("Plan3").Delete
    oWorkbook.SaveAs sCaminhoArquivo
    'fmonteiro: Exibe a planilha excel.
    oExcel.Visible = True
    Set oWorksheet = Nothing
    Set oWorkbook = Nothing
    Set oExcel = Nothing
    Exit Function

    Resume
Trata_Erro:
    Call Err.Raise(Err.Number, , "RSGL0002.cls00422.ExportarPremioOuComissaoMOParaExcel - " & Err.Description)
End Function

Public Function ExportarSinistroParaExcel(ByRef oMSFlexGrid As Object, _
                                          ByVal sNomePlanilha As String, _
                                          ByVal sTitulo As String, _
                                          ByVal sParametro As String, _
                                          ByVal sCaminhoArquivo As String)
    Dim oExcel     As Object 'Excel.Application

    Dim oWorkbook  As Object 'Excel.Workbook

    Dim oWorksheet As Object 'Excel.Worksheet

    Dim lRow       As Long

    Dim lCol       As Long

    Dim sCelula    As String

    Dim lColIniRel As Long

    Dim lRowIniRel As Long

    On Local Error GoTo Trata_Erro
    Set oExcel = CreateObject("Excel.Application")
    Set oWorkbook = oExcel.Workbooks.Add
    Set oWorksheet = oWorkbook.Worksheets("Plan1")
    oExcel.DisplayAlerts = False
    oWorksheet.name = sNomePlanilha
    lColIniRel = 65
    lRowIniRel = 1

    With oMSFlexGrid
        'HOLIVEIRA: Adiciona os parametros.
        sCelula = Chr(lColIniRel) & (lRowIniRel + lRow) & ":" & Chr(lColIniRel + (.Cols - 1)) & (lRowIniRel + lRow)
        oWorksheet.Range(sCelula).Merge
        oWorksheet.Range(sCelula) = sParametro
        oWorksheet.Range(sCelula).Font.Bold = .CellFontBold
        lRowIniRel = lRowIniRel + 2

        For lRow = 0 To .Rows - 1
            For lCol = 0 To .Cols - 1
                .Row = lRow
                .Col = lCol
                sCelula = Chr(lColIniRel + lCol) & ":" & Chr(lColIniRel + lCol)
                oWorksheet.Columns(sCelula).EntireColumn.AutoFit
                oWorksheet.Columns(sCelula).ColumnWidth = (.ColWidth(lCol) / 85)
                sCelula = Chr(lColIniRel + lCol) & (lRowIniRel + lRow)
                oWorksheet.Range(sCelula) = .TextMatrix(lRow, lCol)
                oWorksheet.Range(sCelula).Interior.Color = IIf(CLng(.CellBackColor) = CLng(.BackColorFixed), 12632256, 16777215)
                oWorksheet.Range(sCelula).Borders.LineStyle = xlContinuous
                oWorksheet.Range(sCelula).HorizontalAlignment = HorizontalAlignmentExcel(.CellAlignment)
                oWorksheet.Range(sCelula).Font.Bold = .CellFontBold
            Next lCol

            If .MergeRow(.Row) = True Then
                sCelula = Chr(lColIniRel) & (lRowIniRel + lRow) & ":" & Chr(lColIniRel + (.Cols - 1)) & (lRowIniRel + lRow)
                oWorksheet.Range(sCelula).Merge
            End If

        Next lRow

    End With

    oWorksheet.PageSetup.Zoom = False
    oWorksheet.PageSetup.FitToPagesTall = 1
    oWorksheet.PageSetup.FitToPagesWide = 1

    'HOLIVEIRA: Configura para impress�o.
    With oWorksheet.PageSetup
        .Orientation = xlLandscape
        .CenterHorizontally = True
        .CenterHeader = "&""Arial,Negrito""&14" & sTitulo
        .RightFooter = "Gerado em " & Format(Now, "dd/mm/yyyy")
        .CenterFooter = "P�gina &P de &N"
    End With

    oWorkbook.Worksheets("Plan2").Delete
    oWorkbook.Worksheets("Plan3").Delete
    oWorkbook.SaveAs sCaminhoArquivo
    'fmonteiro: Exibe a planilha excel.
    oExcel.Visible = True
    Set oWorksheet = Nothing
    Set oWorkbook = Nothing
    Set oExcel = Nothing
    Exit Function

    Resume
Trata_Erro:
    Call Err.Raise(Err.Number, , "RSGL0002.cls00422.ExportarSinistroParaExcel - " & Err.Description)
End Function
