VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 1  'vbSimpleBound
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cls00385"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Public mvarSiglaSistema As String
Public mvarSiglaRecurso As String
Public mvarDescricaoRecurso As String
Public mvarAmbienteId As Integer
Public mvarUsuario As String


Public Function ConsultarCoberturas(ByVal lPropostaID As Long, _
                                    ByVal sDtOcorrencia As String, _
                                    ByVal lTpComponente As Long, _
                                    Optional ByVal lSubGrupoId As Long) As Recordset
Dim SQL As String
Dim Rs As ADODB.Recordset

    On Error GoTo Error
    
    'Seleciona as coberturas da proposta
    SQL = ""
    SQL = SQL & " exec consultar_coberturas_sps '" & sDtOcorrencia & "'," & _
                                                     lPropostaID & "," & lTpComponente
                
    If lSubGrupoId > 0 Then
        SQL = SQL & " ," & lSubGrupoId
        SQL = SQL & " , 1"
    Else
        SQL = SQL & " ,null "
        SQL = SQL & " , 1"
    End If
    
      Set ConsultarCoberturas = ExecutarSQL(mvarSiglaSistema, _
                                            mvarAmbienteId, _
                                            mvarSiglaRecurso, _
                                            mvarDescricaoRecurso, _
                                            SQL, _
                                            True)

    Exit Function
    
Error:
Trata_Erro:
    Call Err.Raise(Err.Number, , "SEGL0144.cls00385.ConsultarCoberturas - " & Err.Description)
End Function

'inclu�do por asouza - 10.01.06
Public Function ConsultarQuestionario(ByVal lEventoID As Long) As Recordset
Dim SQL As String
Dim Rs As ADODB.Recordset

    On Error GoTo Error
    
    'Seleciona as coberturas da proposta
    SQL = ""
      
    SQL = SQL & "SELECT  Pergunta = pergunta_tb.nome," & vbNewLine
    SQL = SQL & "        Resposta = CASE WHEN evento_segbr_sinistro_questionario_tb.dominio_resposta_id is NULL THEN" & vbNewLine
    SQL = SQL & "                           evento_segbr_sinistro_questionario_tb.texto_resposta" & vbNewLine
    SQL = SQL & "                      Else" & vbNewLine
    SQL = SQL & "                           dominio_resposta_tb.resposta_determinada" & vbNewLine
    SQL = SQL & "                 End" & vbNewLine
    SQL = SQL & "From evento_segbr_sinistro_questionario_tb" & vbNewLine
    SQL = SQL & "JOIN als_produto_db..pergunta_tb pergunta_tb" & vbNewLine
    SQL = SQL & "  ON pergunta_tb.pergunta_id = evento_segbr_sinistro_questionario_tb.pergunta_id" & vbNewLine
    SQL = SQL & "left JOIN als_produto_db..dominio_resposta_tb dominio_resposta_tb" & vbNewLine
    SQL = SQL & "  ON dominio_resposta_tb.grupo_dominio_resposta_id = pergunta_tb.grupo_dominio_resposta_id" & vbNewLine
    SQL = SQL & "WHERE evento_segbr_sinistro_questionario_tb.evento_id = " & lEventoID & vbNewLine
      
      Set ConsultarQuestionario = ExecutarSQL(mvarSiglaSistema, _
                                            mvarAmbienteId, _
                                            mvarSiglaRecurso, _
                                            mvarDescricaoRecurso, _
                                            SQL, _
                                            True)

    Exit Function
    
Error:
Trata_Erro:
    Call Err.Raise(Err.Number, , "SEGL0144.cls00385.ConsultarCoberturas - " & Err.Description)
End Function


Public Function ConsultarTabelao(ByVal sSinistro_id As String) As Recordset

Dim SQL As String

On Error GoTo Trata_Erro:
    
     SQL = ""
     SQL = SQL & " SELECT evento_id, "
     SQL = SQL & "        banco_aviso_id , " & vbCrLf
     SQL = SQL & "        agencia_aviso_id , " & vbCrLf
     SQL = SQL & "        situacao_aviso , " & vbCrLf
     SQL = SQL & "        dt_aviso_sinistro , " & vbCrLf
     SQL = SQL & "        dt_ocorrencia_sinistro , " & vbCrLf
     SQL = SQL & "        evento_sinistro_id , " & vbCrLf
     SQL = SQL & "        subevento_sinistro_id , " & vbCrLf
     SQL = SQL & "        descricao_evento , " & vbCrLf
     SQL = SQL & "        localizacao , " & vbCrLf
     SQL = SQL & "        nome_sinistrado , " & vbCrLf
     SQL = SQL & "        cpf_sinistrado , " & vbCrLf
     SQL = SQL & "        sexo_sinistrado = case " & vbCrLf
     SQL = SQL & "              when sexo_sinistrado = 'F' then 'Feminino'"
     SQL = SQL & "              when sexo_sinistrado = 'M' then 'Masculino'"
     SQL = SQL & "              else 'Indefinido' end, "
     SQL = SQL & "        dt_nascimento_sinistrado , " & vbCrLf
     SQL = SQL & "        nome_solicitante , " & vbCrLf
     SQL = SQL & "        endereco_solicitante , " & vbCrLf
     SQL = SQL & "        solicitante_bairro , " & vbCrLf
     SQL = SQL & "        solicitante_municipio_id , " & vbCrLf
     SQL = SQL & "        solicitante_municipio , " & vbCrLf
     SQL = SQL & "        solicitante_estado , " & vbCrLf
     SQL = SQL & "        solicitante_CEP , " & vbCrLf
     SQL = SQL & "        solicitante_email , " & vbCrLf
     SQL = SQL & "        tp.nome solicitante_grau_parentesco , " & vbCrLf
     SQL = SQL & "        e.solicitante_grau_parentesco solicitante_grau_parentesco_id, " & vbCrLf
     SQL = SQL & "        ddd_solicitante , " & vbCrLf
     SQL = SQL & "        telefone_solicitante , " & vbCrLf
     SQL = SQL & "        ramal , " & vbCrLf
     SQL = SQL & "        tp_telefone , " & vbCrLf
     
     SQL = SQL & "        ddd1 , " & vbCrLf
     SQL = SQL & "        telefone1 , " & vbCrLf
     SQL = SQL & "        ramal1, " & vbCrLf
     SQL = SQL & "        tp_telefone1 , " & vbCrLf
     
     SQL = SQL & "        ddd2 , " & vbCrLf
     SQL = SQL & "        telefone2 , " & vbCrLf
     SQL = SQL & "        ramal2, " & vbCrLf
     SQL = SQL & "        tp_telefone2 , " & vbCrLf
     
     SQL = SQL & "        ddd3 , " & vbCrLf
     SQL = SQL & "        telefone3 , " & vbCrLf
     SQL = SQL & "        ramal3, " & vbCrLf
     SQL = SQL & "        tp_telefone3 , " & vbCrLf
     
     SQL = SQL & "        ddd4 , " & vbCrLf
     SQL = SQL & "        telefone4 , " & vbCrLf
     SQL = SQL & "        ramal4, " & vbCrLf
     SQL = SQL & "        tp_telefone4 , " & vbCrLf
         
     SQL = SQL & "        sinistro_id , " & vbCrLf
     SQL = SQL & "        sinistro_bb = isnull(sinistro_bb,0) , " & vbCrLf
     SQL = SQL & "        apolice_id = isnull(e.apolice_id,0), " & vbCrLf
     SQL = SQL & "        e.seguradora_cod_susep, " & vbCrLf
     SQL = SQL & "        e.sucursal_seguradora_id, " & vbCrLf
     SQL = SQL & "        e.ramo_id, " & vbCrLf
     SQL = SQL & "        produto_id = isnull(e.produto_id,0), " & vbCrLf
     SQL = SQL & "        isnull(pr.nome,'') nome_produto, " & vbCrLf
     SQL = SQL & "        e.proposta_id, " & vbCrLf
     SQL = SQL & "        tp_ramo = case when r.tp_ramo_id = 1 then 'Vida'"
     SQL = SQL & "                         when r.tp_ramo_id = 2 then 'Elementar'"
     SQL = SQL & "                         Else '' end," & vbCrLf
     SQL = SQL & "        situacao = case when p.situacao IN( 'A','I','O') then 'Emitida'" & vbCrLf
     SQL = SQL & "                   when p.situacao in( 'E', 'P', 'L', 'M', 'V') then 'Em Estudo'" & vbCrLf
     SQL = SQL & "                   when p.situacao = 'C' then 'Cancelada'" & vbCrLf
     SQL = SQL & "                   when p.situacao in( 'S', 'R') then 'Recusada'" & vbCrLf
     SQL = SQL & "                   end," & vbCrLf
     SQL = SQL & "        proposta_bb = isnull(e.proposta_bb,0), " & vbCrLf
     SQL = SQL & "        cliente_id = isnull(e.cliente_id,0), " & vbCrLf
     SQL = SQL & "        nome_segurado, " & vbCrLf
     SQL = SQL & "        cpf_cgc_segurado, " & vbCrLf
     SQL = SQL & "        dt_nascimento_segurado, " & vbCrLf
     SQL = SQL & "        sexo_segurado, " & vbCrLf
     SQL = SQL & "        ind_reanalise, " & vbCrLf
     SQL = SQL & "        ind_tp_sinistrado = case when e.ind_tp_sinistrado = 'T' then 'Titular'" & vbCrLf
     SQL = SQL & "                                 when e.ind_tp_sinistrado = 'C' then 'Conjuge'" & vbCrLf
     SQL = SQL & "                                 when e.ind_tp_sinistrado = 'F' then 'Filhos'" & vbCrLf
     SQL = SQL & "                                 else '' end," & vbCrLf
     SQL = SQL & "        moeda_id= isnull(moeda_id,0), " & vbCrLf
     SQL = SQL & "        e.sub_grupo_id," & vbCrLf
     SQL = SQL & "        sub_grupo_apolice_tb.nome as nome_subgrupo," & vbCrLf
     SQL = SQL & "        e.motivo_reanalise_sinistro_id," & vbCrLf
     SQL = SQL & "        e.val_informado," & vbCrLf
     SQL = SQL & "        cd_prd = isnull(e.cd_prd,0)," & vbCrLf
     SQL = SQL & "        cd_mdld = isnull(e.cd_mdld,0)," & vbCrLf
     SQL = SQL & "        cd_item_mdld = isnull(e.cd_item_mdld,0)," & vbCrLf
     SQL = SQL & "        e.processa_reintegracao_is" & vbCrLf
     SQL = SQL & " FROM evento_segbr_sinistro_tb e with(nolock)" & vbCrLf
     SQL = SQL & " LEFT JOIN proposta_tb p with(nolock) " & vbCrLf
     SQL = SQL & " ON    e.proposta_id = p.proposta_id " & vbCrLf
     SQL = SQL & " LEFT JOIN produto_tb pr with(nolock) " & vbCrLf
     SQL = SQL & " ON    p.produto_id = pr.produto_id " & vbCrLf
     SQL = SQL & " LEFT JOIN ramo_tb r with(nolock)" & vbCrLf
     SQL = SQL & " ON    e.ramo_id = r.ramo_id" & vbCrLf
     SQL = SQL & " LEFT JOIN tp_componente_tb tp with(nolock)" & vbCrLf
     SQL = SQL & " ON    e.solicitante_grau_parentesco = tp.tp_componente_id" & vbCrLf
     SQL = SQL & " LEFT JOIN seguro_vida_sub_grupo_tb svsg with(nolock)" & vbCrLf
     SQL = SQL & " ON e.proposta_id = svsg.proposta_id " & vbCrLf
     SQL = SQL & " AND e.sub_grupo_id = svsg.sub_grupo_id" & vbCrLf
     SQL = SQL & " LEFT JOIN sub_grupo_apolice_tb with(nolock)" & vbCrLf
     SQL = SQL & " ON e.apolice_id = sub_grupo_apolice_tb.apolice_id " & vbCrLf
     SQL = SQL & " AND e.sub_grupo_id = sub_grupo_apolice_tb.sub_grupo_id" & vbCrLf
     SQL = SQL & " AND e.ramo_id = sub_grupo_apolice_tb.ramo_id" & vbCrLf
     SQL = SQL & " WHERE e.sinistro_id = " & sSinistro_id & vbCrLf
     SQL = SQL & " and evento_id = (select max(evento_id) " & vbCrLf
     SQL = SQL & "                  from evento_segbr_sinistro_tb ev with(nolock)" & vbCrLf
     SQL = SQL & "                  where ev.evento_bb_id in (1100,1101)" & vbCrLf
     SQL = SQL & "                    and ev.sinistro_id = " & sSinistro_id & ")"
     
      Set ConsultarTabelao = ExecutarSQL(mvarSiglaSistema, _
                                         mvarAmbienteId, _
                                         mvarSiglaRecurso, _
                                         mvarDescricaoRecurso, _
                                         SQL, _
                                         True)
                                         
Exit Function
                                         
Trata_Erro:
    Call Err.Raise(Err.Number, , "SEGL0144.cls00385.ConsultarTabelao - " & Err.Description)
    
End Function

Public Function ObterDescricao(ByVal lEventoID As Long) As Recordset

Dim sSQL As String

On Error GoTo Trata_Erro

    sSQL = ""
    sSQL = sSQL & " SELECT evento_SEGBR_sinistro_descricao_tb.descricao_evento " & vbNewLine
    sSQL = sSQL & "   FROM evento_SEGBR_sinistro_descricao_tb " & vbNewLine
    sSQL = sSQL & "  WHERE evento_SEGBR_sinistro_descricao_tb.evento_id = " & lEventoID & vbNewLine
    sSQL = sSQL & "  ORDER BY evento_SEGBR_sinistro_descricao_tb.sequencial " & vbNewLine
    
    Set ObterDescricao = ExecutarSQL(mvarSiglaSistema, _
                                     mvarAmbienteId, _
                                     mvarSiglaRecurso, _
                                     mvarDescricaoRecurso, _
                                     sSQL, _
                                     True)
                                     
Exit Function
                                       
Trata_Erro:
    Call Err.Raise(Err.Number, , "SEGL0144.cls00385.ObterDescricao - " & Err.Description)
    
End Function

Public Function ObterQuestionarioSinistro(ByVal lProduto_id As Long) As Integer

Dim sSQL As String
Dim Rs As Recordset

On Error GoTo erro

If lProduto_id = 1152 Or lProduto_id = 1204 Or lProduto_id = 1240 Then '17/05/2018 - Schoralick (ntendencia) - 00421597-seguro-faturamento-pecuario [(sprint 5)]
    
    sSQL = "SELECT nr_qstn" & vbNewLine
    sSQL = sSQL & " from  als_produto_db..qstn_item_mdld qstn_item_mdld " & vbNewLine
    sSQL = sSQL & " Where CD_PRD = " & lProduto_id & vbNewLine
    sSQL = sSQL & "    and cd_tip_qstn = 9 " & vbNewLine
    sSQL = sSQL & "    and dt_fim_qstn is null" & vbNewLine

Else
    sSQL = ""
    sSQL = sSQL & "SELECT isnull(nr_qstn,0)" & vbNewLine
    sSQL = sSQL & "FROM als_produto_db..produto_tb produto_tb" & vbNewLine
    sSQL = sSQL & "JOIN als_produto_db..item_mdld_prd item_mdld_prd" & vbNewLine
    sSQL = sSQL & "  ON item_mdld_prd.cd_prd = produto_tb.produto_id" & vbNewLine
    sSQL = sSQL & "JOIN als_produto_db..qstn_item_mdld qstn_item_mdld" & vbNewLine
    sSQL = sSQL & "  ON qstn_item_mdld.cd_prd = item_mdld_prd.cd_prd" & vbNewLine
    sSQL = sSQL & " AND qstn_item_mdld.cd_mdld = item_mdld_prd.cd_mdld" & vbNewLine
    sSQL = sSQL & " AND qstn_item_mdld.cd_item_mdld = item_mdld_prd.cd_item_mdld" & vbNewLine
    sSQL = sSQL & "WHERE produto_id = " & lProduto_id & vbNewLine
    sSQL = sSQL & "  AND CD_TIP_QSTN = 9" & vbNewLine
End If



Set Rs = ExecutarSQL(mvarSiglaSistema, _
                     mvarAmbienteId, _
                     mvarSiglaRecurso, _
                     mvarDescricaoRecurso, _
                     sSQL, _
                     True)
                     
If Not Rs.EOF Then
    ObterQuestionarioSinistro = Rs(0)
End If
                                              
Exit Function
    
erro:

Trata_Erro:
    Call Err.Raise(Err.Number, , "SEGL0144.cls00385.ObterQuestionarioSinistro - " & Err.Description)
End Function



Public Function ObterQuestionarioProduto(ByVal lProduto_id As Long) As Integer

Dim sSQL As String
Dim Rs As Recordset

On Error GoTo erro

sSQL = ""
sSQL = sSQL & "SELECT isnull(nr_qstn,0)" & vbNewLine
sSQL = sSQL & "FROM als_produto_db..produto_tb produto_tb" & vbNewLine
sSQL = sSQL & "JOIN als_produto_db..qstn_item_mdld qstn_item_mdld" & vbNewLine
sSQL = sSQL & "  ON qstn_item_mdld.cd_prd = produto_tb.cd_prd" & vbNewLine
sSQL = sSQL & " AND qstn_item_mdld.cd_mdld = produto_tb.cd_mdld" & vbNewLine
sSQL = sSQL & " AND qstn_item_mdld.cd_item_mdld = produto_tb.cd_item_mdld" & vbNewLine
sSQL = sSQL & "WHERE produto_id = " & lProduto_id & vbNewLine

Set Rs = ExecutarSQL(mvarSiglaSistema, _
                     mvarAmbienteId, _
                     mvarSiglaRecurso, _
                     mvarDescricaoRecurso, _
                     sSQL, _
                     True)
                     
If Not Rs.EOF Then
    ObterQuestionarioProduto = Rs(0)
End If
                                              
Exit Function
    
erro:

Trata_Erro:
    Call Err.Raise(Err.Number, , "SEGL0144.cls00385.ObterQuestionarioProduto - " & Err.Description)
End Function

Public Function ObterSubEvento(ByVal lEventoOcorrencia As Long, _
                               Optional ByVal lSubEventoId As Long) As Recordset

Dim sSQL As String

On Error GoTo Trata_Erro

    sSQL = ""
    sSQL = sSQL & "SELECT subevento_sinistro_tb.subevento_sinistro_id, subevento_sinistro_tb.nome" & vbNewLine
    sSQL = sSQL & "FROM evento_subevento_sinistro_tb   with(nolock)" & vbNewLine
    sSQL = sSQL & "JOIN subevento_sinistro_tb  with(nolock)" & vbNewLine
    sSQL = sSQL & " ON subevento_sinistro_tb.subevento_sinistro_id = evento_subevento_sinistro_tb.subevento_sinistro_id " & vbNewLine
    sSQL = sSQL & "WHERE evento_subevento_sinistro_tb.evento_sinistro_id = " & lEventoOcorrencia & vbNewLine
    
    If lSubEventoId <> 0 Then
        sSQL = sSQL & "  AND subevento_sinistro_tb.subevento_sinistro_id = " & lSubEventoId & vbNewLine
    End If
    
    sSQL = sSQL & "ORDER BY subevento_sinistro_tb.nome"
    
    Set ObterSubEvento = ExecutarSQL(mvarSiglaSistema, _
                                       mvarAmbienteId, _
                                       mvarSiglaRecurso, _
                                       mvarDescricaoRecursos, _
                                       sSQL, _
                                       True)
                                       
Exit Function
                                       
Trata_Erro:
    Call Err.Raise(Err.Number, , "SEGL0144.cls00385.ObterSubEvento - " & Err.Description)
        
End Function

Public Function PesquisarAgencia(ByVal lCodigoAgencia As Long) As Recordset

Dim SQL As String

On Error GoTo Trata_Erro

    SQL = ""
    SQL = SQL & " SELECT distinct a.agencia_id, nome = isnull(a.nome,''), estado = isnull(a.estado,''), a.prefixo, " & vbNewLine
    SQL = SQL & " dv_prefixo = isnull(a.dv_prefixo,0), endereco = isnull(a.endereco,''), " & vbNewLine
    SQL = SQL & "bairro = isnull(a.bairro,''),  'municipio' = m.nome, pilar_atacado" & vbNewLine
    SQL = SQL & " FROM agencia_tb a with(nolock), municipio_tb m  with(nolock)" & vbNewLine
    SQL = SQL & " WHERE a.municipio_id = m.municipio_id" & vbNewLine
    SQL = SQL & " AND  a.estado = m.estado" & vbNewLine
    SQL = SQL & " AND agencia_id = " & lCodigoAgencia
        
    Set PesquisarAgencia = ExecutarSQL(mvarSiglaSistema, _
                                       mvarAmbienteId, _
                                       mvarSiglaRecurso, _
                                       mvarDescricaoRecursos, _
                                       SQL, _
                                       True)
                                       
Exit Function
                                       
Trata_Erro:
    Call Err.Raise(Err.Number, , "SEGL0144.cls00385.PesquisarAgencia - " & Err.Description)
    
End Function
Public Function ObterIsVida(ByVal lPropostaID As Long, _
                            ByVal lSubGrupoId As Long, _
                            ByVal sDtOcorrencia As String, _
                            ByVal lTpComponente As Long, _
                            ByVal lClienteId As Long, _
                            ByVal lTpCoberturaId As Long) As Currency
                         
Dim Rs As Recordset
Dim sSQL As String

On Error GoTo erro:

    sSQL = ""
    sSQL = sSQL & "SELECT round(seguro_vida_sub_grupo_tb.val_capital_segurado * (perc_basica / 100),2,0) Val_is" & vbNewLine
    sSQL = sSQL & "FROM escolha_sub_grp_tp_cob_comp_tb " & vbNewLine
    sSQL = sSQL & "JOIN seguro_vida_sub_grupo_tb" & vbNewLine
    sSQL = sSQL & "  ON seguro_vida_sub_grupo_tb.apolice_id = escolha_sub_grp_tp_cob_comp_tb.apolice_id" & vbNewLine
    sSQL = sSQL & " AND seguro_vida_sub_grupo_tb.ramo_id = escolha_sub_grp_tp_cob_comp_tb.ramo_id" & vbNewLine
    sSQL = sSQL & " AND seguro_vida_sub_grupo_tb.sub_grupo_id = escolha_sub_grp_tp_cob_comp_tb.sub_grupo_id" & vbNewLine
    sSQL = sSQL & "JOIN tp_cob_comp_tb" & vbNewLine
    sSQL = sSQL & "  ON tp_cob_comp_tb.tp_componente_id = seguro_vida_sub_grupo_tb.tp_componente_id" & vbNewLine
    sSQL = sSQL & " AND tp_cob_comp_tb.tp_cob_comp_id = escolha_sub_grp_tp_cob_comp_tb.tp_cob_comp_id" & vbNewLine
    sSQL = sSQL & "WHERE seguro_vida_sub_grupo_tb.proposta_id = " & lPropostaID & vbNewLine
    sSQL = sSQL & "  AND seguro_vida_sub_grupo_tb.sub_grupo_id = " & lSubGrupoId & vbNewLine
    sSQL = sSQL & "  AND seguro_vida_sub_grupo_tb.prop_cliente_id = " & lClienteId & vbNewLine
    sSQL = sSQL & "  AND seguro_vida_sub_grupo_tb.tp_componente_id = " & lTpComponente & vbNewLine
    sSQL = sSQL & "  AND seguro_vida_sub_grupo_tb.dt_inicio_vigencia_seg <= '" & sDtOcorrencia & "'" & vbNewLine
    sSQL = sSQL & "  AND (seguro_vida_sub_grupo_tb.dt_fim_vigencia_sbg >='" & sDtOcorrencia & "'" & vbNewLine
    sSQL = sSQL & "       OR seguro_vida_sub_grupo_tb.dt_fim_vigencia_sbg IS NULL)" & vbNewLine
    sSQL = sSQL & "  AND tp_cob_comp_tb.tp_cobertura_id = " & lTpCoberturaId & vbNewLine
    
    Set Rs = ExecutarSQL(mvarSiglaSistema, _
                         mvarAmbienteId, _
                         mvarSiglaRecurso, _
                         mvarDescricaoRecurso, _
                         sSQL, _
                         True)
                         
    If Rs.EOF Then Exit Function
                         
    ObterIsVida = Rs(0)
    
    Exit Function
    
erro:

Trata_Erro:
    Call Err.Raise(Err.Number, , "SEGL0144.cls00385.ObterIsVida - " & Err.Description)
End Function


Public Function ObtemISRE(proposta_id As String, Tp_Cobertura_id As Integer, cod_objeto_segurado As Integer, ByVal dt_ocorrencia As String) As Double
Dim SQL As String
Dim rsPesquisa As ADODB.Recordset

    ObtemIS = 0
    SQL = ""
    SQL = SQL & " exec consultar_coberturas_sps '" & Format(dt_ocorrencia, "yyyymmdd") & "',"
    SQL = SQL & proposta_id & ", "
    SQL = SQL & " null, "    'tp_componente_id
    SQL = SQL & " null, "    'sub_grupo_id
    SQL = SQL & " 2,"        'tipo de ramo
    SQL = SQL & cod_objeto_segurado 'Cod_objeto_segurado
    
    Set rsPesquisa = ExecutarSQL(mvarSiglaSistema, _
                         mvarAmbienteId, _
                         mvarSiglaRecurso, _
                         mvarDescricaoRecurso, _
                         SQL, _
                         True)
                                
    While Not rsPesquisa.EOF
        If rsPesquisa("tp_cobertura_id") = Tp_Cobertura_id Then
                ObtemISRE = rsPesquisa("val_is")
                Exit Function
        End If
        rsPesquisa.MoveNext
    Wend
    rsPesquisa.Close

End Function


Public Function Obtem_Objeto_segurado(proposta_id As String, parametro As Integer, Optional cod_objeto_segurado As Integer, Optional ByVal lPerguntaId As Long) As Recordset
Dim SQL As String

On Error GoTo Trata_Erro

    SQL = ""
    SQL = SQL & " set nocount on exec obtem_objetos_segurados_questionario_sps " & proposta_id
    SQL = SQL & ", " & parametro
    If cod_objeto_segurado <> 0 Then
        SQL = SQL & ", " & cod_objeto_segurado
    End If
    If lPerguntaId <> 0 Then
        SQL = SQL & ", " & lPerguntaId
    End If

    
    Set Obtem_Objeto_segurado = ExecutarSQL(mvarSiglaSistema, _
                                            mvarAmbienteId, _
                                            mvarSiglaRecurso, _
                                            mvarDescricaoRecurso, _
                                            SQL, True)
                                            
Exit Function

Trata_Erro:
    Call Err.Raise(Err.Number, , "SEGL0144.cls00385.Obtem_objeto_segurado - " & Err.Description)
    
End Function


Public Function ObterEnderecoRisco(dProposta As Double, cod_objeto_segurado As Integer) As Recordset
Dim SQL As String
    
    SQL = ""
    SQL = SQL & " set nocount on exec obtem_end_risco_sps " & dProposta & ", " & cod_objeto_segurado
    
    Set ObterEnderecoRisco = ExecutarSQL(mvarSiglaSistema, _
                                            mvarAmbienteId, _
                                            mvarSiglaRecurso, _
                                            mvarDescricaoRecurso, _
                                            SQL, True)
End Function


Public Function VerificaReanaliseRE(proposta_id As Double, _
                                   dtOcorrencia As String, _
                                   Evento_sinistro_id As Integer, _
                                   CPF_CNPJ_segurado As String, _
                                   ByRef sSinistro_id As String, _
                                   ByRef sSinistro_bb_id As String) As String

Dim SQL As String
Dim rc As ADODB.Recordset
Dim situacao As String

SQL = ""

SQL = "set nocount on exec verifica_reanalise_RE_sps " & proposta_id & ", '" & CPF_CNPJ_segurado & "','" & _
    dtOcorrencia & "'," & Evento_sinistro_id


Set rc = ExecutarSQL(mvarSiglaSistema, _
                    mvarAmbienteId, _
                    mvarSiglaRecurso, _
                    mvarDescricaoRecurso, _
                    SQL, True)
                            
If Not rc.EOF Then 'se existe aviso, verifica se est� aberto

    If IsNull(rc!situacao) Then
        VerificaReanaliseRE = "NAVS"
    Else
        Select Case Trim(rc!situacao)
            Case "0", "1", ""
                VerificaReanaliseRE = "NAVS"
            Case "2"
                VerificaReanaliseRE = "RNLS"
            Case Else
                VerificaReanaliseRE = "RNLS"
        End Select
    End If
    
    sSinistro_id = rc!sinistro_id
    sSinistro_bb = IIf(IsNull(rc!Sinistro_bb), 0, rc!Sinistro_bb)

Else 'if n�o existe aviso
    VerificaReanaliseRE = "AVSR"
    sSinistro_id = 0
    sSinistro_bb = 0
End If

End Function
Public Function VerificaReanaliseALS(ByVal proposta_id As Long, _
                                     ByVal dtOcorrencia As String, _
                                     ByVal Evento_sinistro_id As Integer, _
                                     ByVal cpf_segurado As String, _
                                     ByVal dt_nascimento As String, _
                                     ByRef sinistro_id As String, _
                                     ByRef sinistro_bb_id As String) As String

Dim SQL As String
Dim rc As ADODB.Recordset
Dim eventos_equivalentes As String
Dim situacao As String


SQL = ""
SQL = "set nocount on exec verifica_reanalise_ALS_sps " & proposta_id & ", " & Evento_sinistro_id & ",'" & _
    dtOcorrencia & "','" & cpf_segurado & "','" & dt_nascimento & "'"


Set rc = ExecutarSQL(mvarSiglaSistema, _
                     mvarAmbienteId, _
                     mvarSiglaRecurso, _
                     mvarDescricaoRecurso, _
                     SQL, _
                     True)
                            
If Not rc.EOF Then 'se existe aviso, verifica se est� aberto

    If IsNull(rc!situacao) Then
        VerificaReanalise = "NAVS"
    Else
        Select Case Trim(rc!situacao)
            Case "0", "1", ""
                VerificaReanalise = "NAVS"
            Case "2"
                VerificaReanalise = "RNLS"
            Case Else
                VerificaReanalise = "RNLS"
        End Select
    End If
    
    sinistro_id = IIf(IsNull(rc!sinistro_id), "", rc!sinistro_id)
    sinistro_bb_id = IIf(IsNull(rc!Sinistro_bb), "", rc!Sinistro_bb)

Else 'if n�o existe aviso
    VerificaReanalise = ""
    sinistro_id = ""
    sinistro_bb_id = ""
End If

End Function



Public Sub GravarAgencia(ByVal sSinistroId As String, _
                         ByVal lAgenciaAvisoId As Long, _
                         ByVal lBancoId As Long, _
                         ByVal sUsuario As String)
Dim SQL As String
Dim Rs As ADODB.Recordset

On Error GoTo Trata_Erro

    SQL = " exec sinistro_agencia_spu "
    SQL = SQL & sSinistroId & ", "
    SQL = SQL & lAgenciaAvisoId & ","
    SQL = SQL & lBancoId & ","
    SQL = SQL & "'" & sUsuario & "'"
    
    
    Call ExecutarSQL(mvarSiglaSistema, _
                    mvarAmbienteId, _
                    mvarSiglaRecurso, _
                    mvarDescricaoRecurso, _
                    SQL, _
                    False)
                    
Exit Sub
                    
Trata_Erro:
    Call Err.Raise(Err.Number, , "SEGL0144.cls00385.GravarAgencia - " & Err.Description)
    
End Sub


Public Sub GravarSolicitante(ByVal lSinistroId As String, _
                             ByVal sNome As String, _
                             ByVal sEnder As String, _
                             ByVal sBairro As String, _
                             ByVal lMunicipioId As Long, _
                             ByVal sMunicipio As String, _
                             ByVal sEstado As String, _
                             ByVal sddd1 As String, _
                             ByVal sTelefone As String, _
                             ByVal sddd2 As String, _
                             ByVal sTelefone2 As String)
Dim SQL As String
Dim Rs As ADODB.Recordset
Dim solicitante_id As Long

On Error GoTo Trata_Erro

solicitante_id = 0

SQL = "select solicitante_id from sinistro_tb with(nolock) where sinistro_id = " & lSinistroId

Set Rs = ExecutarSQL(mvarSiglaSistema, _
                    mvarAmbienteId, _
                    mvarSiglaRecurso, _
                    mvarDescricaoRecurso, _
                    SQL, True)
                             
If Not Rs.EOF Then
    solicitante_id = Rs(0)
End If

Rs.Close

If solicitante_id <> 0 Then

    SQL = " exec solicitante_sinistro_spu "
    SQL = SQL & solicitante_id & ", "
    SQL = SQL & "'" & sNome & "',"
    SQL = SQL & "'" & sEnder & "',"
    SQL = SQL & "'" & sBairro & "',"
    SQL = SQL & lMunicipioId & ","
    SQL = SQL & "'" & sMunicipio & "',"
    SQL = SQL & "'" & sEstado & "',"
    SQL = SQL & "'" & sddd1 & "',"
    SQL = SQL & "'" & sTelefone & "',"
    SQL = SQL & IIf(sddd2 = "", "null,", "'" & sddd2 & "',")
    SQL = SQL & IIf(sTelefone2 = "", "null, ", "'" & sTelefone2 & "',")
    SQL = SQL & "'" & cUserName & "'"
    
    
    Call ExecutarSQL(mvarSiglaSistema, _
                     mvarAmbienteId, _
                     mvarSiglaRecurso, _
                     mvarDescricaoRecurso, _
                     SQL, _
                     False)
                        
End If

Exit Sub

Trata_Erro:
    Call Err.Raise(Err.Number, , "SEGL0144.cls00385.GravarSolicitante - " & Err.Description)
    
End Sub


Private Function ExisteProdutoBB(ByVal lPropostaID As Long) As Boolean
Dim SQL As String
Dim Rs As ADODB.Recordset

On Error GoTo TrataErro

SQL = SQL & " SELECT produto_bb "
SQL = SQL & " FROM produto_bb_sinistro_tb b"
SQL = SQL & " INNER JOIN proposta_tb a "
SQL = SQL & " ON a.proposta_id = " & lPropostaID
SQL = SQL & " WHERE b.produto_id = a.produto_id "
SQL = SQL & " AND b.dt_inicio_vigencia <= a.dt_contratacao "
SQL = SQL & " AND (dt_fim_vigencia >= a.dt_contratacao OR dt_fim_vigencia is null)"

Set Rs = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, SQL, True)

If Not Rs.EOF Then
    ExisteProdutoBB = True
Else
    ExisteProdutoBB = False
End If
Rs.Close

Exit Function

TrataErro:
    Call Err.Raise(Err.Number, , "SEGL0144.cls00385.GravarSolicitante - " & Err.Description)

End Function
Private Function ExisteEventoSinistroProduto(ByVal lEventoSinistroId As Integer, _
                                             ByVal lProdutoId As Integer) As Boolean
Dim SQL As String
Dim Rs As ADODB.Recordset

On Error GoTo TrataErro

    'Verifica se o evento de sinistro est� cadastrado para o produto
    ExisteEventoSinistroProduto = False
    
    SQL = ""
    SQL = SQL & " select 1 from produto_estimativa_sinistro_tb"
    SQL = SQL & " where produto_id = " & lProdutoId
    SQL = SQL & " and evento_sinistro_id = " & lEventoSinistroId
        
    Set Rs = ExecutarSQL(gsSIGLASISTEMA, _
                         glAmbiente_id, _
                         App.Title, _
                         App.FileDescription, _
                         SQL, _
                         True)
    
    If Not Rs.EOF Then
        ExisteEventoSinistroProduto = True
    End If
    
    Rs.Close
    
Exit Function
    
TrataErro:
    Call Err.Raise(Err.Number, , "SEGL0144.cls00385.GravarSolicitante - " & Err.Description)
    
End Function

Public Function PesquisarSubGrupos(ByVal lApoliceId As Long, _
                                   ByVal lRamoId As Long) As Recordset
                            
Dim sSQL As String

On Error GoTo TratarErro

sSQL = ""
sSQL = sSQL & "SELECT sub_grupo_apolice_tb.sub_grupo_id, " & vbNewLine
sSQL = sSQL & "       sub_grupo_apolice_tb.nome," & vbNewLine
sSQL = sSQL & "       cliente_nome = cliente_tb.nome" & vbNewLine
sSQL = sSQL & "FROM sub_grupo_apolice_tb" & vbNewLine
sSQL = sSQL & "JOIN representacao_sub_grupo_tb" & vbNewLine
sSQL = sSQL & "  ON representacao_sub_grupo_tb.apolice_id = sub_grupo_apolice_tb.apolice_id" & vbNewLine
sSQL = sSQL & " AND representacao_sub_grupo_tb.ramo_id = sub_grupo_apolice_tb.ramo_id" & vbNewLine
sSQL = sSQL & " AND representacao_sub_grupo_tb.sub_grupo_id = sub_grupo_apolice_tb.sub_grupo_id" & vbNewLine
sSQL = sSQL & "  AND representacao_sub_grupo_tb.dt_inicio_representacao <= '" & Format(mskDtOcorrencia, "yyyymmdd") & "'" & vbNewLine
sSQL = sSQL & "  AND (representacao_sub_grupo_tb.dt_fim_representacao >= '" & Format(mskDtOcorrencia, "yyyymmdd") & "'" & vbNewLine
sSQL = sSQL & "       OR representacao_sub_grupo_tb.dt_fim_representacao IS NULL)" & vbNewLine
sSQL = sSQL & "JOIN cliente_tb" & vbNewLine
sSQL = sSQL & "  ON cliente_tb.cliente_id = representacao_sub_grupo_tb.est_cliente_id" & vbNewLine
sSQL = sSQL & "WHERE sub_grupo_apolice_tb.apolice_id = " & lApoliceId & vbNewLine
sSQL = sSQL & "  AND sub_grupo_apolice_tb.ramo_id = " & lRamoId & vbNewLine
sSQL = sSQL & "  AND sub_grupo_apolice_tb.dt_inicio_vigencia_sbg <= '" & Format(mskDtOcorrencia, "yyyymmdd") & "'" & vbNewLine
sSQL = sSQL & "  AND (sub_grupo_apolice_tb.dt_fim_vigencia_sbg >= '" & Format(mskDtOcorrencia, "yyyymmdd") & "'" & vbNewLine
sSQL = sSQL & "      OR sub_grupo_apolice_tb.dt_fim_vigencia_sbg IS NULL)" & vbNewLine

Set PesquisarSubGrupos = ExecutarSQL(mvarSiglaSistema, _
                                     mvarAmbienteId, _
                                     mvarSiglaRecurso, _
                                     mvarDescricaoRecurso, _
                                     sSQL, _
                                     True)
                                     
Exit Function
TratarErro:
    Call Err.Raise(Err.Number, , "SEGL0144.cls00385.PesquisarSubGrupos - " & Err.Description)
End Function

Public Sub ConsultarDadosApolice(ByVal Proposta As Double, _
                                 ByRef apolice As String, _
                                 Optional ByRef ramo As Integer, _
                                 Optional ByRef sucursal As String, _
                                 Optional ByRef seguradora As String, _
                                 Optional ByRef Moeda As Integer)
Dim SQL As String
Dim Rs As ADODB.Recordset

'carolina.marinho - 18/08/2010 Demanda 3105507 - Tratamento ABS
Dim sCodSusep As String
Dim oSeguradoraSusep As Object

Set oSeguradoraSusep = CreateObject("SEGL0022.cls00118")
sCodSusep = oSeguradoraSusep.ObterCodSeguradoraSusep("SEGBR", _
                                                     mvarSiglaRecurso, _
                                                     mvarDescricaoRecurso, _
                                                     mvarAmbienteId)


Set oSeguradoraSusep = Nothing

    If Proposta = 0 Then
        apolice = ""
        ramo = 0
        sucursal = 0
        'carolina.marinho - 18/08/2010 Demanda 3105507 - Tratamento ABS
        'seguradora = 6785
        seguradora = sCodSusep
        Moeda = 790
        Exit Sub
    End If
    
    SQL = ""
    SQL = SQL & " select a.dt_inicio_vigencia, a.dt_fim_vigencia, a.apolice_id, a.ramo_id, a.sucursal_seguradora_id, a.seguradora_cod_susep, a.seguro_moeda_id"
    SQL = SQL & " from  proposta_adesao_tb a with(nolock)"
    SQL = SQL & " where a.proposta_id = " & Proposta
        
    Set Rs = ExecutarSQL(mvarSiglaSistema, _
                         mvarAmbienteId, _
                         mvarSiglaRecurso, _
                         mvarDescricaoRecurso, _
                         SQL, True)
    If Rs.EOF Then
        Rs.Close
        
        SQL = ""
        SQL = SQL & " select a.dt_inicio_vigencia, a.dt_fim_vigencia, a.apolice_id, a.ramo_id, a.sucursal_seguradora_id, a.seguradora_cod_susep, b.seguro_moeda_id"
        SQL = SQL & " from  apolice_tb a with(nolock)"
        SQL = SQL & " inner join proposta_fechada_tb b"
        SQL = SQL & " on a.proposta_id = b.proposta_id "
        SQL = SQL & " where a.proposta_id = " & Proposta
            
        Set Rs = ExecutarSQL(mvarSiglaSistema, _
                             mvarAmbienteId, _
                             mvarSiglaRecurso, _
                             mvarDescricaoRecurso, _
                             SQL, True)
    End If
    
    If Not Rs.EOF Then
        apolice = Rs(2)
        ramo = Rs(3)
        sucursal = Rs(4)
        seguradora = Rs(5)
        Moeda = Rs(6)
    Else
        apolice = ""
        'ramo = 0
        sucursal = 0
        'carolina.marinho - 18/08/2010 Demanda 3105507 - Tratamento ABS
        'seguradora = 6785
        seguradora = sCodSusep
        Moeda = 790
        'Exit Sub
    End If
    
    Rs.Close
End Sub







Public Function ObterCoberturasProposta(ByVal sDtOcorrencia As String, _
                                        ByVal lproposta_id As Long, _
                                        ByVal ltp_componente_id As Long, _
                                        Optional ByVal lSubGrupoId As Long) As Recordset

Dim SQL As String

    On Error GoTo Error
    
    'Seleciona as coberturas da proposta
    SQL = ""
    SQL = SQL & " exec consultar_coberturas_sps '" & sDtOcorrencia & "'," & _
                                                     lproposta_id & "," & ltp_componente_id
                
    If lSubGrupoId > 0 Then
        SQL = SQL & " ," & lSubGrupoId
        SQL = SQL & " , 1"
    Else
        SQL = SQL & " ,null "
        SQL = SQL & " , 1"
    End If
    
    Set ObterCoberturasProposta = ExecutarSQL(mvarSiglaSistema, _
                                              mvarAmbienteId, _
                                              mvarSiglaRecurso, _
                                              mvarDescricaoRecurso, _
                                              SQL, _
                                              True)
                                              
Exit Function
                                        
Error:

    Call Err.Raise(Err.Number, , "SEGL0144.cls00385.ObterCoberturasProposta - " & Err.Description)
    
End Function


Public Function VerificaRegraSinistro(ByVal sSiglaSistema As String, _
                                      ByVal sSiglaRecurso As String, _
                                      ByVal sDescricaoRecurso As String, _
                                      ByVal iAmbienteId As Long, _
                                      ByVal Usuario As String, _
                                      ByVal lProdutoId As Long, _
                                      ByVal lTpRegraId As Long) As Boolean

Dim sSQL As String
Dim Rs As Recordset

On Error GoTo TrataErro
    
    sSQL = " "
    sSQL = sSQL & "SELECT dominio = ISNULL(tp_regra_dominio_tb.codigo_dominio, dominio_padrao)" & vbNewLine
    sSQL = sSQL & "FROM produto_regra_tb" & vbNewLine
    sSQL = sSQL & "JOIN dado_basico_db..tp_regra_dominio_tb tp_regra_dominio_tb" & vbNewLine
    sSQL = sSQL & "  ON tp_regra_dominio_tb.tp_Regra_id = produto_regra_tb.tp_Regra_id" & vbNewLine
    sSQL = sSQL & " AND tp_regra_dominio_tb.tp_regra_dominio_id = produto_regra_tb.tp_regra_dominio_id" & vbNewLine
    sSQL = sSQL & "WHERE produto_id = " & lProdutoId & vbNewLine
    sSQL = sSQL & "  AND produto_regra_tb.tp_regra_id = " & lTpRegraId
    
    Set Rs = ExecutarSQL(sSiglaSistema, _
                         iAmbienteId, _
                         sSiglaRecurso, _
                         sDescricaoRecurso, _
                         sSQL, True)
                         
If Not Rs.EOF Then
                         
    If Rs("dominio") = "S" Then VerificaRegraSinistro = True
    
End If



Exit Function

TrataErro:
    Call Err.Raise(Err.Number, , "SEGL0144.cls00385.PesquisarSubGrupos - " & Err.Description)
    
End Function


