VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.2#0"; "MSCOMCTL.OCX"
Begin VB.Form frmContrato 
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   3465
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   8370
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3465
   ScaleWidth      =   8370
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdSair 
      Caption         =   "Sair"
      Height          =   405
      Left            =   7020
      TabIndex        =   7
      Top             =   2720
      Width           =   1155
   End
   Begin VB.CommandButton cmdOk 
      Caption         =   "OK"
      Height          =   405
      Left            =   5700
      TabIndex        =   6
      Top             =   2720
      Width           =   1245
   End
   Begin VB.Frame fraResultados 
      Caption         =   "  Resultado do Processamento "
      Height          =   1995
      Left            =   120
      TabIndex        =   1
      Top             =   435
      Width           =   8055
      Begin VB.TextBox txtProd 
         BackColor       =   &H80000004&
         Height          =   375
         Left            =   1440
         MaxLength       =   6
         TabIndex        =   8
         Tag             =   "1"
         Top             =   480
         Width           =   1125
      End
      Begin VB.TextBox txtErros 
         Alignment       =   1  'Right Justify
         Height          =   345
         Left            =   5880
         Locked          =   -1  'True
         TabIndex        =   3
         Text            =   "0"
         Top             =   1350
         Width           =   1845
      End
      Begin VB.TextBox txtProcessadas 
         Alignment       =   1  'Right Justify
         Height          =   345
         Left            =   5880
         Locked          =   -1  'True
         TabIndex        =   2
         Text            =   "0"
         Top             =   840
         Width           =   1845
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Produto:......."
         Height          =   195
         Index           =   2
         Left            =   360
         TabIndex        =   9
         Top             =   480
         Width           =   915
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Processados com erro: .................................................................................."
         Height          =   195
         Index           =   1
         Left            =   360
         TabIndex        =   5
         Top             =   1440
         Width           =   5355
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Processados: ................................................................................................."
         Height          =   195
         Index           =   0
         Left            =   360
         TabIndex        =   4
         Top             =   960
         Width           =   5370
      End
   End
   Begin MSComctlLib.StatusBar StbRodape 
      Align           =   2  'Align Bottom
      Height          =   315
      Left            =   0
      TabIndex        =   0
      Top             =   3150
      Width           =   8370
      _ExtentX        =   14764
      _ExtentY        =   556
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   3
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   8997
            MinWidth        =   8997
            Text            =   "Mensagem"
            TextSave        =   "Mensagem"
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   6
            Alignment       =   1
            AutoSize        =   2
            Object.Width           =   1693
            MinWidth        =   88
            Text            =   "Data do sistema"
            TextSave        =   "17/04/2019"
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            AutoSize        =   2
            Object.Width           =   1111
            MinWidth        =   88
            Text            =   "Usu�rio"
            TextSave        =   "Usu�rio"
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "frmContrato"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'****************************************************************************************
'*                         Instru��es para futuras implementa��es                       *
'*                                                                                      *
'*  Para futuras implementa��es (inclus�o de valida��o de novos produtos no ALS) seguir *
'*  os seguintes passos:                                                                *
'*  1 - Criar a procedure de valida��o espec�fica do produto de acordo com as regras de *
'*  neg�cio pr�-definidas. Atentar apenas para n�o inserir a valida��o da lista de pr�- *
'*  an�lise, que j� est� inclu�do na SGSL0001, nem os procedimentos-padr�o que existiam *
'*  em todas as procedures de valida��o (gera��o dos eventos ALS, atualiza��o do        *
'*  workflow) que por serem iguais em todas as procedures tamb�m foram desenvolvidos    *
'*  direto na DLL. Todas as procedures de valida��o tamb�m dever�o se atentar para o    *
'*  uso as tabelas tempor�rias "gerais" (##proposta_workflow,                           *
'*  ##avaliacao_proposta_workflow, ##proposta_num_avaliacao) que est�o sendo criadas    *
'*  para uso comum.                                                                     *
'*                                                                                      *
'*  2 - Incluir o produto na tabela produto_parametro_aceitacao_tb do banco seguros_db  *
'*  informando a procedure de tratamento espec�fico, e as informa��es do tipo de flow,  *
'*  atividade e tarefas de aceita��o e recusa da proposta. O campo situacao da tabela   *
'*  dever� ser preenchido com o valor "a" para garantir que o produto seja processado,  *
'*  e com qualquer outro valor (com exce��o de nulo) para que o mesmo seja "desligado"  *
'*  e portanto passe a ser ignorado pelo programa. Para "produtos promocionais" utili-  *
'*  zar os campos dt_inicio_vigencia e dt_fim_vigencia.                                 *
'*                                                                                      *
'*  Considera��es:                                                                      *
'*  1 - N�o � mais necess�rio criar uma tarefa por produto, o programa j� est� pronto   *
'*  para fazer a valida��o de todos os produtos que surgirem na tabela proposta_tb.     *
'*  2 - Os produtos n�o cadastrados na tabela produto_parametro_aceitacao_tb gerar�o    *
'*  log de erro para que a manuten��o possa dar o devido tratamento. O mesmo trata-     *
'*  mento ser� dado para informa��es ausentes e que se fa�am necess�rias para a com-    *
'*  pleta execu��o do programa.                                                         *
'*                                                                                      *
'*  Procedures para procedimentos-padr�o:                                               *
'*  SEGS7300_SPS - Cria��o das tabelas tempor�rias "gerais" e sele��o dos produtos e    *
'*  seus par�metros para execu��o.                                                      *
'*  SEGS7342_SPI - Valida��o da lista de pr�-an�lise.                                   *
'*  SEGS7345_SPI - Inclus�o dos eventos e dos registros para atualiza��o em lote do     *
'*  workflow.                                                                           *
'****************************************************************************************


Private Sub cmdOk_Click()

    Call Processar

End Sub

Private Sub cmdSair_Click()

    Call Unload(Me)

End Sub

Private Sub Form_Load()

    On Error GoTo Trata_Erro
    'Verifica se foi executado pelo Control-M
    CTM = Verifica_Origem_ControlM(Command)
    

    'Verificando permiss�o de acesso � aplica��o'''''''''''''''''''''''''''''''''
    'Pula se for executado pelo control-m
    If CTM = False Then
        If Not Trata_Parametros(Command) Then
            Call Unload(Me)
        End If
      Else
          Call GravaLogCTM("OK - Iniciando Processamento", " - ", App.EXEName, "")
    End If

    'Retirar antes da libera��o '''''''''''''''''''''''''''''''''''''''''''''''''
    'cUserName = "99988877714"
    'glAmbiente_id = 3

    Call ConfigurarInterface

    'Iniciando o processo automaticamente caso seja uma chamada do Scheduler '''''''''''''''

    If Obtem_agenda_diaria_id(Command) > 0 Or CTM = True Then
       Me.Show
       Call cmdOk_Click
       Call Unload(Me)
    End If

Exit Sub

Trata_Erro:
    Call GravaLogCTM("NOT OK - Iniciando tratamento de erro", Err.Description, App.EXEName, "")
    Call TratarErro("Form_Load", Me.name)
    Call FinalizarAplicacao
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Call FinalizarAplicacao
End Sub

Private Sub ConfigurarInterface()
    Call ObterDataSistema(gsSIGLASISTEMA)
    Call CentraFrm(Me)
    Me.Caption = "SEGP0946 - Verifica��o da lista de pr�-an�lise ALS"
End Sub


Private Sub Processar()

Dim oALS As Object
Dim vProcessados As Variant
Dim lCount       As Long
Dim vSplit       As Variant
Dim bErro        As Boolean
Dim sProduto     As Integer
    On Error GoTo Trata_Erro

    'Tratamento para o Scheduler ''''''''''''''''''''''''''''''''''''''''''''''''

    Call InicializaParametrosExecucaoBatch(Me)

    'Atualizando interface ''''''''''''''''''''''''''''''''''''''''''''''''''''''
    sProduto = 0
    If txtProd.Text <> "" Then
     sProduto = CInt(txtProd.Text)
    End If
    
    MousePointer = vbHourglass
    cmdOk.Enabled = False

    Set oALS = CreateObject("SGSL0001.cls00213")


'' fsa - Criticas por produto
'SEGX0856 AB (produtos)= 1180 - 1204 - 1178 - 1177 - 1152 - 1198 - 1179 - 1201 - 1181 - 1182
'SEGX1386 ABS(produtos)= 1017 - 1167 - 1176 - 1178 - 1207 - 1220 - 1221 - 1222 - 1223 - 1224 - 1239
'Antes sproduto = 0. Para atender a Demanda, ATENDER A RCA475 - PARAMETRIZA��O PARA PROCESSAMENTO DA SEGX0856 E SEGX1386.
If sProduto = 0 Or sProduto = 1180 Or sProduto = 1204 Or sProduto = 1178 Or sProduto = 1177 Or _
   sProduto = 1152 Or sProduto = 1198 Or sProduto = 1179 Or sProduto = 1201 Or sProduto = 1181 Or _
   sProduto = 1182 Or sProduto = 1017 Or sProduto = 1167 Or sProduto = 1176 Or sProduto = 1178 Or _
   sProduto = 1207 Or sProduto = 1220 Or sProduto = 1221 Or sProduto = 1222 Or sProduto = 1223 Or _
   sProduto = 1224 Or sProduto = 1239 Then
   
    Call oALS.EfetuarVerificacaoCriticasAceite(App.ProductName, _
                                               App.Title, _
                                               App.FileDescription, _
                                               glAmbiente_id, _
                                               Data_Sistema, _
                                               cUserName, _
                                               vProcessados, _
                                               sProduto) ' Inclus�o do produto
Else

    Call oALS.EfetuarVerificacaoCriticasAceitePorProduto(App.ProductName, _
                                                         App.Title, _
                                                         App.FileDescription, _
                                                         glAmbiente_id, _
                                                         Data_Sistema, _
                                                         cUserName, _
                                                         vProcessados, _
                                                         sProduto)
End If


    If IsEmpty(vProcessados) Then
        Call MensagemBatch("Nenhuma proposta foi processada", vbExclamation)
    Else
        Call MensagemBatch("Processamento efetuado com sucesso", vbExclamation)
        
        ReDim vErro(0)
        For lCount = 1 To UBound(vProcessados)
            vSplit = Split(vProcessados(lCount), "|")
            
            If vSplit(1) <> "-1" And vSplit(1) <> "-2" Then
                Call goProducao.AdicionaLog(1, vSplit(0), lCount)
                Call goProducao.AdicionaLog(2, vSplit(1), lCount)
                Call goProducao.AdicionaLog(3, 0, lCount)
                               
                'grava log arquivo CTM
                If CTM = True Then
                    Call GravaLogCTM("OK - ", vSplit(0), "", "")
                    Call GravaLogCTM("OK - ", vSplit(1), "", "")
                    Call GravaLogCTM("OK - ", "0", "", "")
                End If
                
            ElseIf vSplit(1) = "-1" Then
                bErro = True
                Call MensagemBatch("Produto " & vSplit(0) & " n�o cadastrado!", vbExclamation)
            ElseIf vSplit(1) = "-2" Then
                bErro = True
                Call MensagemBatch("Erro na execu��o do produto " & vSplit(0), vbExclamation)
            End If
            
        Next lCount
        
    End If

    Set oALS = Nothing

    If bErro Then
        Err.Raise 9999, Me.name, "Problemas na execu��o do programa!"
    End If
    

    'Finalizando agenda''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    'grava log arquivo CTM
                If CTM = True Then
                    Call GravaLogCTM("OK - ", "", "", "Processo finalizado com Sucesso!")
                End If

    'Pula de for executado pelo Control-M
    If CTM = False Then
        Call goProducao.finaliza
    End If

    'Atualizando interface ''''''''''''''''''''''''''''''''''''
    
    MousePointer = vbDefault
    cmdOk.Enabled = True

    Exit Sub

Trata_Erro:

    Call TratarErro("Processar", Me.name)
    Call FinalizarAplicacao

End Sub

