VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form frmDocProtocolo 
   Caption         =   "Documenta��o"
   ClientHeight    =   9180
   ClientLeft      =   1245
   ClientTop       =   1905
   ClientWidth     =   13965
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   9180
   ScaleWidth      =   13965
   Begin VB.Frame Frame2 
      Caption         =   "Documentos Entregues"
      Height          =   3600
      Left            =   120
      TabIndex        =   3
      Top             =   4980
      Width           =   13695
      Begin MSFlexGridLib.MSFlexGrid grd_doc_entregue 
         Height          =   3315
         Left            =   60
         TabIndex        =   4
         Top             =   240
         Width           =   13575
         _ExtentX        =   23945
         _ExtentY        =   5847
         _Version        =   393216
         Cols            =   3
         FixedCols       =   0
         HighLight       =   2
         SelectionMode   =   1
         AllowUserResizing=   3
         FormatString    =   $"frmDocProtocolo.frx":0000
      End
   End
   Begin VB.CommandButton cmdRetornar 
      Caption         =   "&Retornar"
      Height          =   375
      Left            =   11880
      TabIndex        =   2
      Top             =   8700
      Width           =   1935
   End
   Begin VB.Frame Frame1 
      Caption         =   "Documentos a Entregar"
      Height          =   4755
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   13695
      Begin MSFlexGridLib.MSFlexGrid grdListaDocumentos 
         Height          =   4455
         Left            =   60
         TabIndex        =   1
         Top             =   240
         Width           =   13575
         _ExtentX        =   23945
         _ExtentY        =   7858
         _Version        =   393216
         FixedCols       =   0
         HighLight       =   2
         SelectionMode   =   1
         AllowUserResizing=   3
         FormatString    =   $"frmDocProtocolo.frx":00F5
      End
   End
End
Attribute VB_Name = "frmDocProtocolo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private lConexaoLocal                           As Integer
Private Sub cmdRetornar_Click()
Unload Me
End Sub

Private Sub Form_Load()
    Me.Caption = "SEGP1285 - Documenta��o a Entregar"
    Call CentraFrm(Me)
    Me.Caption = Me.Caption & " - " & sSegmentoCliente
    Carrega_grid_Documentos
End Sub

Public Sub Carrega_grid_Documentos()
        
    Dim strNumProtocolo As String, sAux As String
    Dim iCodSusepSeguradora As Integer
    Dim SQL As String
    Dim rsRecordSet As ADODB.Recordset

    If EstadoConexao(lConexaoLocal) = adStateClosed Then
        lConexaoLocal = AbrirConexao(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription)
    End If

    strNumProtocolo = Right(String$(15, "0") & FrmConsultaSinistrocon.grdSelecao.TextMatrix(FrmConsultaSinistrocon.grdSelecao.RowSel, 2), 15)
        
    
    SQL = ""
    SQL = SQL & " select seguradora_cod_susep " & vbNewLine
    SQL = SQL & "   from interface_dados_db.dbo.aviso_web_comunicacao_sinistro_tb with (nolock) " & vbNewLine
    SQL = SQL & "  where num_protocolo = '" & Trim(strNumProtocolo) & "' " & vbNewLine

    Set rsRecordSet = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                          glAmbiente_id, _
                                          App.Title, _
                                          App.FileDescription, _
                                          SQL, _
                                          lConexaoLocal, _
                                          True)
                                          
    If Not rsRecordSet.EOF Then
        iCodSusepSeguradora = rsRecordSet("seguradora_cod_susep")
    Else
        iCodSusepSeguradora = 6785
    End If
    
    rsRecordSet.Close
    Set rsRecordSet = Nothing

    SQL = " EXEC interface_dados_db.dbo.SEGS13738_SPS "
    SQL = SQL & iCodSusepSeguradora & vbNewLine
    SQL = SQL & ", '" & strNumProtocolo & "' " & vbNewLine
    
'    SQL = SQL & "  FROM sinistro_historico_tb s  WITH (NOLOCK)  "
'    SQL = SQL & " INNER JOIN evento_SEGBR_tb e  WITH (NOLOCK)  "
'    SQL = SQL & "    ON (e.evento_SEGBR_id = s.evento_SEGBR_id)"
'    SQL = SQL & " WHERE s.Sinistro_id        = " & cSinist
'    SQL = SQL & "   AND s.evento_visualizado = 'n'"
'    SQL = SQL & "   AND e.evento_BB_id IS NOT NULL"
'    SQL = SQL & " ORDER BY s.dt_inclusao, s.dt_evento, s.seq_evento"

    Set rsRecordSet = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                          glAmbiente_id, _
                                          App.Title, _
                                          App.FileDescription, _
                                          SQL, _
                                          lConexaoLocal, _
                                          True)
                                    
    grd_doc_entregue.Rows = 1
    grdListaDocumentos.Rows = 1
    'grdListaDocumentos.FixedRows = 1
    While Not rsRecordSet.EOF
        If rsRecordSet!status_documento = 1 Then
            sAux = rsRecordSet!dt_envio & ""
            sAux = Mid(sAux, 9, 2) & "/" & Mid(sAux, 6, 2) & "/" & Left(sAux, 4)
            grd_doc_entregue.AddItem rsRecordSet!nome_documento & vbTab & rsRecordSet!Descricao & vbTab & sAux
        Else
            grdListaDocumentos.AddItem rsRecordSet!nome_documento & vbTab & rsRecordSet!Descricao
        End If
        'grdListaDocumentos.AddItem rsRecordSet!nome_documento & Chr(9) & rsRecordSet!Descricao & Chr(9) & IIf(rsRecordSet!status_documento = 0, "N�O", "SIM")
        rsRecordSet.MoveNext
    Wend
    rsRecordSet.Close
    Set rsRecordSet = Nothing
        
End Sub

Private Sub Form_Resize()
    If Me.WindowState <> vbMinimized Then
        If Me.Width < 8000 Then Me.Width = 8000
        If Me.Height < 8000 Then Me.Height = 8000
        
        Frame1.Width = Me.Width - 510
        Frame2.Width = Me.Width - 510
        Frame2.Height = Me.Height - 6150
        
        cmdRetornar.Top = Me.Height - 1050
        cmdRetornar.Left = Me.Width - 2325
        
        grdListaDocumentos.Width = Frame1.Width - 120
        grd_doc_entregue.Width = Frame2.Width - 120
        grd_doc_entregue.Height = Frame2.Height - 285
        
        Dim Laux As Long
        grd_doc_entregue.ColWidth(2) = 1000
        grd_doc_entregue.ColAlignment(2) = 3
        
        Laux = grd_doc_entregue.Width - 375 - 1000
        grd_doc_entregue.ColWidth(0) = Int(Laux * 0.35)
        If grd_doc_entregue.ColWidth(0) < 4500 Then grd_doc_entregue.ColWidth(0) = 4500
        grd_doc_entregue.ColWidth(1) = Laux - grd_doc_entregue.ColWidth(0)
        If grd_doc_entregue.ColWidth(1) < 7000 Then grd_doc_entregue.ColWidth(1) = 7000
        
        Laux = grd_doc_entregue.Width - 375
        grdListaDocumentos.ColWidth(0) = grd_doc_entregue.ColWidth(0)
        grdListaDocumentos.ColWidth(1) = Laux - grd_doc_entregue.ColWidth(0)
        If grdListaDocumentos.ColWidth(1) < 8000 Then grdListaDocumentos.ColWidth(1) = 8000
        
    End If
End Sub

