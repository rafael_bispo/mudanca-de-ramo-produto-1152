VERSION 5.00
Begin VB.Form FrmAlteraTelefoneSolicitante 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "SEGP0001 - Telefones do solicitante - VIDA"
   ClientHeight    =   4545
   ClientLeft      =   6315
   ClientTop       =   3135
   ClientWidth     =   5655
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4545
   ScaleWidth      =   5655
   ShowInTaskbar   =   0   'False
   Begin VB.Frame fmeSolicitante_Sinistro_Telefone 
      Caption         =   "Telefone/Fax"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4095
      Left            =   240
      TabIndex        =   24
      Top             =   240
      Width           =   5205
      Begin VB.TextBox txtSolicitante_Sinistro_DDD_Fax 
         Height          =   315
         Left            =   240
         MaxLength       =   4
         TabIndex        =   20
         Top             =   3480
         Width           =   615
      End
      Begin VB.TextBox txtSolicitante_Sinistro_Telefone_Fax 
         Height          =   315
         Left            =   1080
         MaxLength       =   10
         TabIndex        =   21
         Top             =   3480
         Width           =   1335
      End
      Begin VB.TextBox txtSolicitante_Sinistro_DDD5 
         Height          =   315
         Left            =   240
         MaxLength       =   4
         TabIndex        =   16
         Top             =   2880
         Width           =   615
      End
      Begin VB.TextBox txtSolicitante_Sinistro_Telefone5 
         Height          =   315
         Left            =   1080
         MaxLength       =   10
         TabIndex        =   17
         Top             =   2880
         Width           =   1335
      End
      Begin VB.TextBox txtSolicitante_Sinistro_DDD4 
         Height          =   315
         Left            =   240
         MaxLength       =   4
         TabIndex        =   12
         Top             =   2280
         Width           =   615
      End
      Begin VB.TextBox txtSolicitante_Sinistro_Telefone4 
         Height          =   315
         Left            =   1080
         MaxLength       =   10
         TabIndex        =   13
         Top             =   2280
         Width           =   1335
      End
      Begin VB.TextBox txtSolicitante_Sinistro_DDD3 
         Height          =   315
         Left            =   240
         MaxLength       =   4
         TabIndex        =   8
         Top             =   1680
         Width           =   615
      End
      Begin VB.TextBox txtSolicitante_Sinistro_Telefone3 
         Height          =   315
         Left            =   1080
         MaxLength       =   10
         TabIndex        =   9
         Top             =   1680
         Width           =   1335
      End
      Begin VB.TextBox txtSolicitante_Sinistro_DDD2 
         Height          =   315
         Left            =   240
         MaxLength       =   4
         TabIndex        =   4
         Top             =   1080
         Width           =   615
      End
      Begin VB.TextBox txtSolicitante_Sinistro_Telefone2 
         Height          =   315
         Left            =   1080
         MaxLength       =   10
         TabIndex        =   5
         Top             =   1080
         Width           =   1335
      End
      Begin VB.TextBox txtSolicitante_Sinistro_DDD1 
         Height          =   315
         Left            =   240
         MaxLength       =   4
         TabIndex        =   0
         Top             =   480
         Width           =   615
      End
      Begin VB.TextBox txtSolicitante_Sinistro_Telefone1 
         Height          =   315
         Left            =   1080
         MaxLength       =   10
         TabIndex        =   1
         Top             =   480
         Width           =   1335
      End
      Begin VB.TextBox txtTp_Telefone1 
         Height          =   315
         Left            =   3600
         TabIndex        =   3
         Top             =   480
         Width           =   1335
      End
      Begin VB.TextBox txtTp_Telefone2 
         Height          =   315
         Left            =   3600
         TabIndex        =   7
         Top             =   1080
         Width           =   1335
      End
      Begin VB.TextBox txtTp_Telefone3 
         Height          =   315
         Left            =   3600
         TabIndex        =   11
         Top             =   1680
         Width           =   1335
      End
      Begin VB.TextBox txtTp_Telefone4 
         Height          =   315
         Left            =   3600
         TabIndex        =   15
         Top             =   2280
         Width           =   1335
      End
      Begin VB.TextBox txtTp_Telefone5 
         Height          =   315
         Left            =   3600
         TabIndex        =   19
         Top             =   2880
         Width           =   1335
      End
      Begin VB.TextBox txtSolicitante_Sinistro_Ramal5 
         Height          =   315
         Left            =   2640
         MaxLength       =   4
         TabIndex        =   18
         Top             =   2880
         Width           =   735
      End
      Begin VB.TextBox txtSolicitante_Sinistro_Ramal4 
         Height          =   315
         Left            =   2640
         MaxLength       =   4
         TabIndex        =   14
         Top             =   2280
         Width           =   735
      End
      Begin VB.TextBox txtSolicitante_Sinistro_Ramal3 
         Height          =   315
         Left            =   2640
         MaxLength       =   4
         TabIndex        =   10
         Top             =   1680
         Width           =   735
      End
      Begin VB.TextBox txtSolicitante_Sinistro_Ramal2 
         Height          =   315
         Left            =   2640
         MaxLength       =   4
         TabIndex        =   6
         Top             =   1080
         Width           =   735
      End
      Begin VB.TextBox txtSolicitante_Sinistro_Ramal1 
         Height          =   315
         Left            =   2640
         MaxLength       =   4
         TabIndex        =   2
         Top             =   480
         Width           =   735
      End
      Begin VB.CommandButton btnAlterar 
         Caption         =   "Alterar"
         Height          =   375
         Left            =   2640
         TabIndex        =   22
         Top             =   3435
         Visible         =   0   'False
         Width           =   1095
      End
      Begin VB.CommandButton btnRetornar 
         Caption         =   "Voltar"
         Height          =   375
         Left            =   3840
         TabIndex        =   23
         Top             =   3435
         Width           =   1095
      End
      Begin VB.Label lblTelefoneSolicitante 
         Caption         =   "Ramal 5"
         Height          =   255
         Index           =   18
         Left            =   2640
         TabIndex        =   46
         Top             =   2640
         Width           =   855
      End
      Begin VB.Label lblTelefoneSolicitante 
         Caption         =   "Ramal 4"
         Height          =   255
         Index           =   14
         Left            =   2640
         TabIndex        =   45
         Top             =   2040
         Width           =   855
      End
      Begin VB.Label lblTelefoneSolicitante 
         Caption         =   "Ramal 3"
         Height          =   255
         Index           =   10
         Left            =   2640
         TabIndex        =   44
         Top             =   1440
         Width           =   855
      End
      Begin VB.Label lblTelefoneSolicitante 
         Caption         =   "Ramal 2"
         Height          =   255
         Index           =   6
         Left            =   2640
         TabIndex        =   43
         Top             =   840
         Width           =   855
      End
      Begin VB.Label lblTelefoneSolicitante 
         Caption         =   "Ramal 1"
         Height          =   255
         Index           =   2
         Left            =   2640
         TabIndex        =   42
         Top             =   240
         Width           =   855
      End
      Begin VB.Label lblTelefoneSolicitante 
         Caption         =   "Fax  N�mero"
         Height          =   255
         Index           =   21
         Left            =   1080
         TabIndex        =   41
         Top             =   3240
         Width           =   975
      End
      Begin VB.Label lblTelefoneSolicitante 
         Caption         =   "Fax DDD"
         Height          =   255
         Index           =   20
         Left            =   240
         TabIndex        =   40
         Top             =   3240
         Width           =   735
      End
      Begin VB.Label lblTelefoneSolicitante 
         Caption         =   "Tipo de telefone 5"
         Height          =   255
         Index           =   19
         Left            =   3600
         TabIndex        =   39
         Top             =   2640
         Width           =   1455
      End
      Begin VB.Label lblTelefoneSolicitante 
         Caption         =   "5� N�mero"
         Height          =   255
         Index           =   17
         Left            =   1080
         TabIndex        =   38
         Top             =   2640
         Width           =   855
      End
      Begin VB.Label lblTelefoneSolicitante 
         Caption         =   "5� DDD"
         Height          =   255
         Index           =   16
         Left            =   240
         TabIndex        =   37
         Top             =   2640
         Width           =   615
      End
      Begin VB.Label lblTelefoneSolicitante 
         Caption         =   "Tipo de telefone 4"
         Height          =   255
         Index           =   15
         Left            =   3600
         TabIndex        =   36
         Top             =   2040
         Width           =   1455
      End
      Begin VB.Label lblTelefoneSolicitante 
         Caption         =   "Tipo de telefone 2"
         Height          =   255
         Index           =   7
         Left            =   3600
         TabIndex        =   35
         Top             =   840
         Width           =   1455
      End
      Begin VB.Label lblTelefoneSolicitante 
         Caption         =   "2� N�mero"
         Height          =   255
         Index           =   5
         Left            =   1080
         TabIndex        =   34
         Top             =   840
         Width           =   855
      End
      Begin VB.Label lblTelefoneSolicitante 
         Caption         =   "2� DDD"
         Height          =   255
         Index           =   4
         Left            =   240
         TabIndex        =   33
         Top             =   840
         Width           =   615
      End
      Begin VB.Label lblTelefoneSolicitante 
         Caption         =   "4� N�mero"
         Height          =   255
         Index           =   13
         Left            =   1080
         TabIndex        =   32
         Top             =   2040
         Width           =   855
      End
      Begin VB.Label lblTelefoneSolicitante 
         Caption         =   "4� DDD"
         Height          =   255
         Index           =   12
         Left            =   240
         TabIndex        =   31
         Top             =   2040
         Width           =   615
      End
      Begin VB.Label lblTelefoneSolicitante 
         Caption         =   "Tipo de telefone 3"
         Height          =   255
         Index           =   11
         Left            =   3600
         TabIndex        =   30
         Top             =   1440
         Width           =   1455
      End
      Begin VB.Label lblTelefoneSolicitante 
         Caption         =   "Tipo de telefone 1"
         Height          =   255
         Index           =   3
         Left            =   3600
         TabIndex        =   29
         Top             =   240
         Width           =   1455
      End
      Begin VB.Label lblTelefoneSolicitante 
         Caption         =   "3� DDD"
         Height          =   255
         Index           =   8
         Left            =   240
         TabIndex        =   28
         Top             =   1440
         Width           =   615
      End
      Begin VB.Label lblTelefoneSolicitante 
         Caption         =   "3� N�mero"
         Height          =   255
         Index           =   9
         Left            =   1080
         TabIndex        =   27
         Top             =   1440
         Width           =   855
      End
      Begin VB.Label lblTelefoneSolicitante 
         Caption         =   "1� DDD"
         Height          =   255
         Index           =   0
         Left            =   240
         TabIndex        =   26
         Top             =   240
         Width           =   615
      End
      Begin VB.Label lblTelefoneSolicitante 
         Caption         =   "1� N�mero"
         Height          =   255
         Index           =   1
         Left            =   1080
         TabIndex        =   25
         Top             =   240
         Width           =   855
      End
   End
End
Attribute VB_Name = "FrmAlteraTelefoneSolicitante"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'Variaveis
Dim bCarregou_Form                          As Boolean
Dim bytModoOperacao                         As Byte '1-Inclusao / 2-Alteracao / 3-Consulta

Private Sub DestroiInterface()
    On Error GoTo Erro
    
    Set rsRecordSet = Nothing
    
    Exit Sub

Erro:
    Call TrataErroGeral("DestroiInterface", Me.name)
    Call FinalizarAplicacao

End Sub


Private Sub btnRetornar_Click()
    
    DestroiInterface
    
    Unload Me

End Sub

Private Sub Form_Activate()
    On Error GoTo Erro
    
    If Not bCarregou_AbaForm Then
        InicializaVariaveisLocal
        
        InicializaInterface
    End If
    Exit Sub
    
Erro:
    Call TrataErroGeral("Form_Activate", Me.name)
    Call FinalizarAplicacao
    
End Sub



Private Sub InicializaModoOperacaoLocal()
    Dim iCampos As Integer
    Dim sMascara As String
    
    On Error GoTo Erro
    
    '1-Inclusao / 2-Altera��o / 3-Consulta
    InicializaModoOperacao Me, bytModoOperacao
           
    Exit Sub
    
Erro:
    Call TrataErroGeral("InicializaModoOperacao", Me.name)
    Call FinalizarAplicacao


End Sub

Private Sub InicializaVariaveisLocal()

    bCarregou_Form = True
    
    'Inicializa em Modo Consulta
    bytModoOperacao = 3

End Sub


Private Sub InicializaInterface()
    On Error GoTo Erro
    
    Screen.MousePointer = vbHourglass
    
    'Ajustando Caption do Form
    Me.Caption = Me.Caption & " - " & ConvAmbiente(glAmbiente_id)
    
    'Configurando interface''''''''''''''''''''''''''''''''''''''''''''''''''''''
    Call CentraFrm(Me)
    
    InicializaInterfaceLocal_Form
    
    'Limpando Campos
    LimpaCamposTelaLocal                                         'SERA ALTERADO
    
    CarregaDadosTela
    
    InicializaModoOperacao Me, 3
    
    Screen.MousePointer = vbNormal
    Exit Sub
    
Erro:
    Call TrataErroGeral("InicializaInterface", "frmTelefoneAlteraSolicitante")
    Call FinalizarAplicacao

End Sub

Private Sub InicializaInterfaceLocal_Form()
    
    Me.Caption = App.EXEName & " - Dados do Solicitante / Telefone - " & Ambiente
    
End Sub

Private Sub LimpaCamposTelaLocal()
    Dim iCampos As Integer
    Dim sMascara As String
    
    On Error GoTo Erro
    
    LimpaCamposTela Me
    
    Exit Sub

Erro:
    Call TrataErroGeral("LimpaCamposTelaLocal", Me.name)
    Call FinalizarAplicacao

End Sub

Private Sub CarregaDadosTela()

    Dim rsRecordSet                             As ADODB.Recordset
    Dim sSQL                                    As String

    On Error GoTo Erro

    MousePointer = vbHourglass

    sSQL = ""
    sSQL = sSQL & "Select Solicitante_Sinistro_DDD1                 " & vbNewLine
    sSQL = sSQL & "     , Solicitante_Sinistro_Telefone1            " & vbNewLine
    sSQL = sSQL & "     , Solicitante_Sinistro_Ramal1               " & vbNewLine
    sSQL = sSQL & "     , Solicitante_Sinistro_Tp_Telefone1         " & vbNewLine
    sSQL = sSQL & "     , Solicitante_Sinistro_DDD2                 " & vbNewLine
    sSQL = sSQL & "     , Solicitante_Sinistro_Telefone2            " & vbNewLine
    sSQL = sSQL & "     , Solicitante_Sinistro_Ramal2               " & vbNewLine
    sSQL = sSQL & "     , Solicitante_Sinistro_Tp_Telefone2         " & vbNewLine
    sSQL = sSQL & "     , Solicitante_Sinistro_DDD3                 " & vbNewLine
    sSQL = sSQL & "     , Solicitante_Sinistro_Telefone3            " & vbNewLine
    sSQL = sSQL & "     , Solicitante_Sinistro_Ramal3               " & vbNewLine
    sSQL = sSQL & "     , Solicitante_Sinistro_Tp_Telefone3         " & vbNewLine
    sSQL = sSQL & "     , Solicitante_Sinistro_DDD4                 " & vbNewLine
    sSQL = sSQL & "     , Solicitante_Sinistro_Telefone4            " & vbNewLine
    sSQL = sSQL & "     , Solicitante_Sinistro_Ramal4               " & vbNewLine
    sSQL = sSQL & "     , Solicitante_Sinistro_Tp_Telefone4         " & vbNewLine
    sSQL = sSQL & "     , Solicitante_Sinistro_DDD5                 " & vbNewLine
    sSQL = sSQL & "     , Solicitante_Sinistro_Telefone5            " & vbNewLine
    sSQL = sSQL & "     , Solicitante_Sinistro_Ramal5               " & vbNewLine
    sSQL = sSQL & "     , Solicitante_Sinistro_Tp_Telefone5         " & vbNewLine
    'cristovao.rodrigues 21/08/2012
    sSQL = sSQL & "     , Solicitante_Sinistro_DDD_Fax              " & vbNewLine
    sSQL = sSQL & "     , Solicitante_Sinistro_Telefone_Fax         " & vbNewLine
    
    sSQL = sSQL & "  From #Sinistro_Principal"

    Set rsRecordSet = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                          glAmbiente_id, _
                                          App.Title, _
                                          App.FileDescription, _
                                          sSQL, _
                                          lConexaoLocal, True)
    With rsRecordSet
        If Not .EOF Then
            'Telefone 1
            txtSolicitante_Sinistro_DDD1 = .Fields!Solicitante_Sinistro_DDD1 & ""
            If LenB(Trim(txtSolicitante_Sinistro_DDD1)) <> 0 Then txtSolicitante_Sinistro_DDD1 = "(" & txtSolicitante_Sinistro_DDD1 & ")"
            txtSolicitante_Sinistro_Telefone1 = .Fields!Solicitante_Sinistro_Telefone1 & ""
            txtSolicitante_Sinistro_Ramal1 = .Fields!Solicitante_Sinistro_Ramal1 & ""
            txtTp_Telefone1 = Seleciona_Descricao_Tp_Telefone(.Fields!Solicitante_Sinistro_Tp_Telefone1 & "")
            
            'Telefone 2
            txtSolicitante_Sinistro_DDD2 = .Fields!Solicitante_Sinistro_DDD2 & ""
            If LenB(Trim(txtSolicitante_Sinistro_DDD1)) <> 0 Then Solicitante_Sinistro_DDD2 = "(" & Solicitante_Sinistro_DDD2 & ")"
            txtSolicitante_Sinistro_Telefone2 = .Fields!Solicitante_Sinistro_Telefone2 & ""
            txtSolicitante_Sinistro_Ramal2 = .Fields!Solicitante_Sinistro_Ramal2 & ""
            txtTp_Telefone2 = Seleciona_Descricao_Tp_Telefone(.Fields!Solicitante_Sinistro_Tp_Telefone2 & "")
            
            'Telefone 3
            txtSolicitante_Sinistro_DDD3 = .Fields!Solicitante_Sinistro_DDD3 & ""
            If LenB(Trim(txtSolicitante_Sinistro_DDD1)) <> 0 Then Solicitante_Sinistro_DDD3 = "(" & Solicitante_Sinistro_DDD3 & ")"
            txtSolicitante_Sinistro_Telefone3 = .Fields!Solicitante_Sinistro_Telefone3 & ""
            txtSolicitante_Sinistro_Ramal3 = .Fields!Solicitante_Sinistro_Ramal3 & ""
            txtTp_Telefone3 = Seleciona_Descricao_Tp_Telefone(.Fields!Solicitante_Sinistro_Tp_Telefone3 & "")
            
            'Telefone 4
            txtSolicitante_Sinistro_DDD4 = .Fields!Solicitante_Sinistro_DDD4 & ""
            If LenB(Trim(txtSolicitante_Sinistro_DDD1)) <> 0 Then Solicitante_Sinistro_DDD4 = "(" & Solicitante_Sinistro_DDD4 & ")"
            txtSolicitante_Sinistro_Telefone4 = .Fields!Solicitante_Sinistro_Telefone4 & ""
            txtSolicitante_Sinistro_Ramal4 = .Fields!Solicitante_Sinistro_Ramal4 & ""
            txtTp_Telefone4 = Seleciona_Descricao_Tp_Telefone(.Fields!Solicitante_Sinistro_Tp_Telefone4 & "")
            
            'Telefone 5
            txtSolicitante_Sinistro_DDD5 = .Fields!Solicitante_Sinistro_DDD5 & ""
            If LenB(Trim(txtSolicitante_Sinistro_DDD1)) <> 0 Then Solicitante_Sinistro_DDD5 = "(" & Solicitante_Sinistro_DDD5 & ")"
            txtSolicitante_Sinistro_Telefone5 = .Fields!Solicitante_Sinistro_Telefone5 & ""
            txtSolicitante_Sinistro_Ramal5 = .Fields!Solicitante_Sinistro_Ramal5 & ""
            txtTp_Telefone5 = Seleciona_Descricao_Tp_Telefone(.Fields!Solicitante_Sinistro_Tp_Telefone5 & "")
            
            'Fax
            txtSolicitante_Sinistro_DDD_Fax = .Fields!Solicitante_Sinistro_DDD_Fax & ""
            If LenB(Trim(txtSolicitante_Sinistro_DDD1)) <> 0 Then Solicitante_Sinistro_DDD_Fax = "(" & Solicitante_Sinistro_DDD_Fax & ")"
            txtSolicitante_Sinistro_Telefone_Fax = .Fields!Solicitante_Sinistro_Telefone_Fax & ""
            
        
        End If
    End With

    bCarregou_AbaDadosGerais = True

    MousePointer = vbNormal

    Set rsRecordSet = Nothing
    Exit Sub

Erro:
    Call TrataErroGeral("CarregaDadosTela", Me.name)
    Call FinalizarAplicacao


End Sub

Private Sub CarregaDadosCombo()
    Dim rsRecordSet                             As ADODB.Recordset
    Dim sSQL                                    As String
    Dim iCampos As Integer
    Dim sMascara As String

    On Error GoTo Erro

    With FrmAlteraTelefoneSolicitante
        For iCampos = 0 To .Controls.Count - 1
            If TypeOf .Controls(iCampos) Is ComboBox Then
                .Controls(iCampos).Clear
                .Controls(iCampos).AddItem "Residencial"
                .Controls(iCampos).ItemData(.Controls(iCampos).NewIndex) = 1
                .Controls(iCampos).AddItem "Comercial"
                .Controls(iCampos).ItemData(.Controls(iCampos).NewIndex) = 2
                .Controls(iCampos).AddItem "Celular"
                .Controls(iCampos).ItemData(.Controls(iCampos).NewIndex) = 3
                .Controls(iCampos).AddItem "Fax"
                .Controls(iCampos).ItemData(.Controls(iCampos).NewIndex) = 4
                .Controls(iCampos).AddItem "Recado"
                .Controls(iCampos).ItemData(.Controls(iCampos).NewIndex) = 5
            End If
        Next
    End With
    Exit Sub
    
Erro:
    Call TrataErroGeral("CarregaDadosCombo_Municipio", Me.name)
    Call FinalizarAplicacao

End Sub

