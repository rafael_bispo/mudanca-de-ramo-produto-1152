VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "mscomctl.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Object = "{B848631F-FD13-44DC-8AD2-1E790664626D}#5.0#0"; "eMail.ocx"
Begin VB.Form frmCarregaSEGBR 
   Caption         =   "SMQP0061 - GTR Exporta��o"
   ClientHeight    =   6210
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   6165
   Icon            =   "frmCarregaSEGBR.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   6210
   ScaleWidth      =   6165
   StartUpPosition =   3  'Windows Default
   Begin eMail.ctleMail ctleMail 
      Left            =   4920
      Top             =   120
      _ExtentX        =   1085
      _ExtentY        =   661
   End
   Begin VB.PictureBox Picture1 
      Height          =   540
      Index           =   0
      Left            =   1326
      Picture         =   "frmCarregaSEGBR.frx":0442
      ScaleHeight     =   480
      ScaleWidth      =   375
      TabIndex        =   13
      Top             =   5310
      Visible         =   0   'False
      Width           =   442
   End
   Begin VB.PictureBox Picture1 
      Height          =   525
      Index           =   1
      Left            =   1729
      Picture         =   "frmCarregaSEGBR.frx":0884
      ScaleHeight     =   465
      ScaleWidth      =   450
      TabIndex        =   12
      Top             =   5310
      Visible         =   0   'False
      Width           =   510
   End
   Begin VB.PictureBox picBotao 
      Height          =   390
      Index           =   0
      Left            =   4185
      Picture         =   "frmCarregaSEGBR.frx":0CC6
      ScaleHeight     =   330
      ScaleWidth      =   360
      TabIndex        =   11
      Top             =   5400
      Width           =   420
   End
   Begin VB.PictureBox picBotao 
      Height          =   390
      Index           =   1
      Left            =   4185
      Picture         =   "frmCarregaSEGBR.frx":0E50
      ScaleHeight     =   330
      ScaleWidth      =   360
      TabIndex        =   10
      Top             =   5400
      Visible         =   0   'False
      Width           =   420
   End
   Begin VB.CommandButton cmdCarregarAgora 
      Caption         =   "&Carregar Agora"
      Height          =   525
      Left            =   0
      TabIndex        =   7
      Top             =   5400
      Width           =   1260
   End
   Begin VB.CommandButton cmdIniciar 
      Caption         =   "&Iniciar            "
      Height          =   525
      Left            =   3480
      TabIndex        =   4
      Top             =   5325
      Width           =   1260
   End
   Begin VB.CommandButton cmdSair 
      Caption         =   "&Sair"
      Height          =   525
      Left            =   4815
      TabIndex        =   3
      Top             =   5325
      Width           =   1260
   End
   Begin VB.Frame frmRemessas 
      Caption         =   " Carga de Remessas "
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   5085
      Left            =   120
      TabIndex        =   1
      Top             =   120
      Width           =   5970
      Begin VB.Timer tmrConsultaEntrada 
         Enabled         =   0   'False
         Interval        =   1000
         Left            =   3120
         Top             =   210
      End
      Begin MSFlexGridLib.MSFlexGrid flexEntrada 
         Height          =   4290
         Left            =   120
         TabIndex        =   2
         Top             =   675
         Width           =   5745
         _ExtentX        =   10134
         _ExtentY        =   7567
         _Version        =   393216
         Rows            =   1
         Cols            =   12
         FixedCols       =   0
         FormatString    =   "Status | QTD | Sequencial | Evento                              | Sinistro_BB     | Cod. Remessa"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin MSComctlLib.ImageList ImageList1 
         Left            =   3600
         Top             =   120
         _ExtentX        =   1005
         _ExtentY        =   1005
         BackColor       =   -2147483643
         ImageWidth      =   24
         ImageHeight     =   22
         MaskColor       =   12632256
         _Version        =   393216
         BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
            NumListImages   =   4
            BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmCarregaSEGBR.frx":0FDA
               Key             =   ""
            EndProperty
            BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmCarregaSEGBR.frx":1174
               Key             =   ""
            EndProperty
            BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmCarregaSEGBR.frx":172E
               Key             =   ""
            EndProperty
            BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmCarregaSEGBR.frx":18C8
               Key             =   ""
            EndProperty
         EndProperty
      End
      Begin VB.Label lblCodTrans 
         Alignment       =   1  'Right Justify
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "XXXX"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   5280
         TabIndex        =   9
         Top             =   405
         Visible         =   0   'False
         Width           =   495
      End
      Begin VB.Label Label2 
         Caption         =   "C�d. Trans.:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   4140
         TabIndex        =   8
         Top             =   405
         Visible         =   0   'False
         Width           =   1095
      End
      Begin VB.Label lblTempo 
         Caption         =   "00:00:05 h"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Left            =   2250
         TabIndex        =   6
         Top             =   390
         Width           =   1170
      End
      Begin VB.Label Label1 
         Caption         =   "Pr�xima carga em :"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   270
         Left            =   180
         TabIndex        =   5
         Top             =   375
         Width           =   2115
      End
   End
   Begin MSComctlLib.StatusBar StatusBar1 
      Align           =   2  'Align Bottom
      Height          =   270
      Left            =   0
      TabIndex        =   0
      Top             =   5940
      Width           =   6165
      _ExtentX        =   10874
      _ExtentY        =   476
      Style           =   1
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
         EndProperty
      EndProperty
   End
   Begin VB.Menu mnuArquivo 
      Caption         =   "&Arquivo"
      Visible         =   0   'False
      Begin VB.Menu mnuAbrir 
         Caption         =   "&Abrir"
      End
      Begin VB.Menu mnuArquivoSep1 
         Caption         =   "-"
      End
      Begin VB.Menu mnuSair 
         Caption         =   "Sai&r"
      End
   End
End
Attribute VB_Name = "frmCarregaSEGBR"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
'Public rdocnAB         As New rdoConnection

Dim Sinistros_Invalidos As New Collection

'Constantes do Grid de Entrada
Const GRID_ENT_STATUS = 0
Const GRID_ENT_QTD = 1
Const GRID_ENT_SEQ = 2
Const GRID_ENT_EVENTO = 3
Const GRID_ENT_SINISTRO_BB = 4
Const GRID_ENT_COD_REMESSA = 5
Const GRID_COD_TRANSACAO = 6

Dim interfaceOrig As New rdoConnection
Dim rdocn_SEG1 As New rdoConnection
Const Tempo_Default As String = "00:02:00"

'In�cio - Melhorias do SEGBR - Jorge Benko - 18/06/2013
Const GRID_SIN_CRIT_LOCAL = 7
Const GRID_EVE_LAYOUT = 8
Const GRID_SP_TRATAMENTO = 9
Const GRID_COD_EMPRESA = 10

Private Enum enStatusConexao
    cxFechada = 0
    cxAberta = 1
    cxTransacao = 2
End Enum

Dim vSsinistro_id As String
Dim vSsinistro_bb As String
Dim vEvento_bb As Integer
Dim vSpTratamento As String

Dim numconexao As Long
Dim intStatusConexao As Integer

Dim cl_Local_Layout As clLayout
Dim co_Local_Layout As New Collection
Dim co_Detalhe As New Collection
Dim co_Detalhe_Filho As New Collection
Dim COD_EMPRESA As String
Dim qtd_layout As Integer

Dim Detalhe As clDetalhe, Detalhe_Pai As clDetalhe, Detalhe_filho1 As clDetalhe
Dim Identificador As clIdentificador
Dim TTLayout As New clLayout
Dim co_campos As New Collection
Dim co_dependentes As New Collection
Dim cl_campos As clEvento_Layout
Dim x As Integer
Dim ambiente_id As Integer

Private Type Tp_Campo
    Nome_SEGBR As String
    Conteudo As String
    Obrigatorio As String
    Tamanho As Integer
    Tipo As String
    Formato As String
    Constante As String
    Separador_Decimal As String
    Separador_Milhar As String
End Type

Dim Carrega As Boolean
Dim Critica As Boolean

Dim rs As rdoResultset 'jorge benko ficara com RDO ate defini��o da base em ADO
Dim Rs2 As rdoResultset

Private Sub cmdCarregarAgora_Click()
1      On Error GoTo TrataErro
       
2         ambiente_id = Right(Command, 1)
          
3         tmrConsultaEntrada.Enabled = False
          
4         Call LogExecution(1, "SMQP0061", "N", "ABS")
5         Call Grava_Trace(0, "Chama a Carrega_SEGBR_NEW(AB)", "cmdCarregarAgora_Click()", ambiente_id)
6         Call Carrega_SEGBR_NEW("AB", ambiente_id)
          
7         Call LogExecution(2, "SMQP0061", "N", "AB")
              
8         If glAmbiente_id = 2 Then
9             ambiente_id = 6
10        Else
11            ambiente_id = 7
12        End If
              
13        frmCarregaSEGBR.Caption = "SMQP0061 - GTR Exporta��o - Rodando ABS"
          
14        Call LogExecution(1, "SMQP0061", "N", "ABS")
15        Call Grava_Trace(0, "Chama a Carrega_SEGBR_NEW(ABS)", "cmdCarregarAgora_Click()", ambiente_id)
16        Call Carrega_SEGBR_NEW("ABS", ambiente_id)  'Jorge Benko
17        Call LogExecution(2, "SMQP0061", "N", "ABS")
          
18        lblTempo = Tempo_Default & " h"
19        If Left(cmdIniciar.Caption, 2) = "&P" Then
20            tmrConsultaEntrada.Enabled = True
21        End If
          
22        Exit Sub
          
TrataErro:
23        Call Grava_Trace(0, "Chama a Carrega_SEGBR_NEW(ABS)", "cmdGerarAgora_Click - Linha: " & Erl() & " - " & Err.Description, ambiente_id)
24        lblTempo = Tempo_Default & " h"
          
25        If Left(cmdIniciar.Caption, 2) = "&P" Then tmrConsultaEntrada.Enabled = True
          
End Sub

Private Sub cmdSair_Click()
    rdocn_Interface.Close
    Set rdocn_Interface = Nothing
    Call TerminaSEGBR
End Sub

Private Sub cmdiniciar_Click()

    If cmdIniciar.Caption = "&Iniciar" Then
        cmdIniciar.Caption = "&Parar"
        picBotao(0).Visible = False
        picBotao(1).Visible = True
        Me.Icon = Picture1(0).Picture
        tmrConsultaEntrada.Enabled = True
    Else
        cmdIniciar.Caption = "&Iniciar"
        picBotao(0).Visible = True
        picBotao(1).Visible = False
        Me.Icon = Picture1(1).Picture
        tmrConsultaEntrada.Enabled = False
    End If

End Sub

Private Sub Form_Load()
On Error GoTo TrataErro
    
    'Verifica se o Programa j� est� rodando
    'If App.PrevInstance Then Call TerminaSEGBR
    Dim Returno As String
    Dim strComputer As String
    Dim colProcesses As Variant
    Dim objProcess As Variant
    Dim strNameOfUser As String
    Dim Aa As String
    
    'In�cio - Melhorias do SEGBR - Jorge Benko - 18/06/2013
    Me.Caption = "SMQP0061 - GTR Exporta��o"
    frmCarregaSEGBR.Caption = "SMQP0061 - GTR Exporta��o - Rodando AB"
    
    glAmbiente_id = Right(Command, 1)
 
    'Abre Conex�o com o SEGBR
    Conexao
    
    Set rdocn_Interface = Nothing
    
    Set rdocn_Interface = rdocn 'conex�o AB
    Set rdocn = rdocn 'conex�o AB para tabelas tempor�rias ##

    strComputer = "."
    Set colProcesses = GetObject("winmgmts:" & "{impersonationLevel=impersonate}!\\" & strComputer & _
                            "\root\cimv2").ExecQuery("Select * from Win32_Process Where Name = 'SMQP0061.exe'")
                            
    If colProcesses.Count > 1 Then 'J� tem um programa aberto
        MsgBox "Programa SMQP0061.exe aberto pelo usuario: " & strNameOfUser, vbExclamation
        Call Grava_Trace(0, "Programa SMQP0061.exe aberto pelo usuario: " & strNameOfUser, "Form_Load()")
        Call TerminaSEGBR
    End If
    
    Call Grava_Trace(0, "In�cio - Execu��o SMQP0061", "Form_Load()")
    
    'Monta a rela��o entre os eventos e os layouts
    Call Grava_Trace(0, "MontaColecaoEventos 'E'", "Form_Load()")
    MontaColecaoEventos "E"
 
    Call Grava_Trace(0, "Carrega_Tipos_Detalhe_NEW", "Form_Load()")
    Call Carrega_Tipos_Detalhe_NEW
    
    cUserName = "SMQP0061"
    
    ' Configura a tela
    CentraFrm Me
    Me.Icon = Picture1(1)
    lblTempo = Tempo_Default & " h"
    
    With flexEntrada
    .ColWidth(GRID_ENT_QTD) = 0
    .ColWidth(GRID_ENT_COD_REMESSA) = 0
    .ColAlignment(GRID_ENT_EVENTO) = 1
    .ColWidth(GRID_COD_TRANSACAO) = 0
    
    'In�cio - Melhorias do SEGBR - Jorge Benko - 18/06/2013
    .ColWidth(GRID_SIN_CRIT_LOCAL) = 0
    .ColWidth(GRID_EVE_LAYOUT) = 0
    .ColWidth(GRID_SP_TRATAMENTO) = 0
    .ColWidth(GRID_COD_EMPRESA) = 0
    'Fim - Melhorias do SEGBR - Jorge Benko - 18/06/2013
  
    .Rows = 1
    .Row = 0
    End With
    
    'In�cio - Melhorias do SEGBR - Jorge Benko - 18/06/2013
    'C�d. da Transa��o
    lblCodTrans = ""
    
    'If Obtem_Dados_Conexao_GTR(GTR_servidor, GTR_banco, GTR_usuario, GTR_senha) = False Then Call GravaLogErro("", "Erro ao carregar par�metros. Quantidade de par�metros necess�rios diferente do esperado.", "SQL", "OBTEM_DADOS_CONEXAO_GTR", Err.Number): Call TerminaSEGBR
    
    lblCodTrans = ObtemConteudo("BB_COD_TRANS_ENTRADA") '"SN00"
    'Fim - Melhorias do SEGBR - Jorge Benko - 18/06/2013
    
    'Verifica se inicia autom�ticamente
    If Trim(Command()) <> "" Then
        cmdiniciar_Click
        Me.WindowState = vbMinimized
    End If
    
    'Coloca o Sistema no System Tray
    If Left(cmdIniciar.Caption, 5) = "&Inic" Then
        Call CarregaBandeja(traySegbr, Picture1(1), "GTR Exporta��o - Parado" & Chr$(0))
    Else
        Call CarregaBandeja(traySegbr, Picture1(0), "GTR Exporta��o - Executando" & Chr$(0))
    End If
    
    cmdiniciar_Click
    
    Call Grava_Trace(0, "Formul�rio Carregado", "Form_Load()")
    Exit Sub
TrataErro:
    'Dim msgErro As String
    'msgErro = "SMQP0061 - Sub Form_Load() na linha - " & Erl() & " - " & Err.Description
    Call Grava_Trace(Err.Number, Err.Description, "Load")
    'MsgBox msgErro, vbCritical, Caption
    End
End Sub

Private Sub Form_Resize()
        
    'Reposiciona a Lista de Mensagens
    If WindowState <> vbMinimized Then
        Call DescarregaBandeja(traySegbr)
    Else
        'Coloca o Sistema no System Tray
        If Left(cmdIniciar.Caption, 5) = "&Inic" Then
           Call CarregaBandeja(traySegbr, Picture1(1), "GTR Exporta��o - Parado" & Chr$(0))
        Else
           Call CarregaBandeja(traySegbr, Picture1(0), "GTR Exporta��o - Executando" & Chr$(0))
        End If
        Me.Hide
    End If
        
End Sub

Private Sub Form_Unload(Cancel As Integer)
    
    'Descarrega o Sistema
    Call DescarregaBandeja(traySegbr)
    rdocn_Interface.Close
    Set rdocn_Interface = Nothing
    Call TerminaSEGBR
    
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)

    If cmdSair.Enabled = False Then
        Cancel = 1
    End If

End Sub

Private Sub lblTempo_Change()
1     On Error GoTo Erro

2         ambiente_id = Right(Command, 1)

3         If lblTempo.Caption = "00:00:00 h" Then
4             lblTempo.Refresh
5             tmrConsultaEntrada.Enabled = False
                      
6             Set rdocn_Interface = rdocn
7             Set rdocn = rdocn 'conex�o AB para tabelas tempor�rias ##
8             frmCarregaSEGBR.Caption = "SMQP0061 - GTR Exporta��o - Rodando AB"
              

9             Call LogExecution(1, "SMQP0061", "N", "AB")
10            Call Grava_Trace(0, "Chama a Carrega_SEGBR_NEW(AB)", "cmdCarregarAgora_Click()", ambiente_id)
11            Call Carrega_SEGBR_NEW("AB", ambiente_id)  'Jorge Benko
12            Call LogExecution(2, "SMQP0061", "N", "AB")
              
              'troca conexao
13            If glAmbiente_id = 2 Then
14                ambiente_id = 6
15            Else
16                ambiente_id = 7
17            End If
              
18            frmCarregaSEGBR.Caption = "SMQP0061 - GTR Exporta��o - Rodando ABS"
              
19            Call LogExecution(1, "SMQP0061", "N", "ABS")
20            Call Grava_Trace(0, "Chama a Carrega_SEGBR_NEW(ABS)", "cmdCarregarAgora_Click()", ambiente_id)
21            Call Carrega_SEGBR_NEW("ABS", ambiente_id)  'Jorge Benko
22            Call LogExecution(2, "SMQP0061", "N", "ABS")
                 
23            lblTempo = Tempo_Default & " h"
24            tmrConsultaEntrada.Enabled = True
25        End If

26    Exit Sub
Erro:
27        Call Grava_Trace(Err.Number, "Linha: " & Erl() & " - " & Err.Description, "Load", ambiente_id)

28        lblTempo = Tempo_Default & " h"
29        tmrConsultaEntrada.Enabled = True
End Sub

Private Sub mnuAbrir_Click()
        'Exibe o Programa
        Me.WindowState = vbNormal
        Me.Show
        Me.SetFocus
End Sub

Private Sub mnuSair_Click()
        'Finaliza o Sistema
        rdocn_Interface.Close
        Set rdocn_Interface = Nothing
        Call TerminaSEGBR
End Sub

Private Sub picBotao_Click(Index As Integer)

    cmdiniciar_Click

End Sub

Private Sub Picture1_MouseMove(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)
    
    Select Case x
           Case TRAY_MSG_MOUSEMOVE
                
           Case TRAY_MSG_LEFTBTN_DOWN
                
           Case TRAY_MSG_LEFTBTN_UP
                'aqui pode utilizar um menu
                
           Case TRAY_MSG_LEFTBTN_DBLCLICK
                Me.WindowState = vbNormal
                Me.Show
                Me.SetFocus
           
           Case TRAY_MSG_RIGHTBTN_DOWN
                
           Case TRAY_MSG_RIGHTBTN_UP
                PopupMenu mnuArquivo
                
           Case TRAY_MSG_RIGHTBTN_DBLCLICK
                
    End Select

End Sub

Private Sub tmrConsultaEntrada_Timer()

    Dim Tempo As String
    Dim Segundos As Integer, Minutos As Integer, Horas As Integer
    
    Tempo = Left(lblTempo.Caption, InStr(lblTempo.Caption, " h"))
'    Segundos = Second(Tempo)
'    Minutos = Minute(Tempo)
'    Horas = Hour(Tempo)
'
'    Segundos = Segundos - 1
'    If Segundos = -1 Then
'        Segundos = 59
'        Minutos = Minutos - 1
'        If Minutos = -1 Then
'            Horas = Horas - 1
'        End If
'    End If
'    lblTempo.Caption = Format(Horas & ":" & Minutos & ":" & Segundos, "hh:mm:ss") & " h"
    lblTempo.Caption = Format(DateAdd("s", -1, Tempo), "hh:mm:ss") & " h"
End Sub
Private Sub Carrega_SEGBR()

    Dim Cont_remessas   As Long, Erro_Carga         As Long, i  As Integer
    Dim Qtd_Registros   As Integer, Entrada_GTR_ID  As String
    Dim Evento_BB       As String, Cont_Sucesso     As Integer
    Dim Cod_transacao   As String
    Dim iLayout         As New clLayout, ErroGTR    As clErro
    Dim Sinistro_BB     As String, OAssunto         As String
    Dim AMensagem       As String, AMensagem_Aux    As String
    Dim ALocalizacao_Aviso  As String, ODestinatario    As String
    Dim OCod_Remessa    As String
    Dim SQL As String
    Dim rs As rdoResultset
    Dim sinistro_bb_temp As String
    On Error GoTo Erro
    
    cmdSair.Enabled = False
    cmdCarregarAgora.Enabled = False
    cmdIniciar.Enabled = False
    picBotao(0).Enabled = False
    picBotao(1).Enabled = False
    
    Cont_remessas = 0
    StatusBar1.SimpleText = "Procurando novas remessas ..."
    
    ' Carrega uma cole��o onde est�o os sinistros em uso pelos t�cnicos.
    ' Estes sinistros s� poder�o ser atualizados ap�s a conclus�o da
    ' altera��o do t�cnico.
    Monta_Sinistros_Bloqueados
    
    ' Limpa a cole��o de sinistros inv�lidos
    Set Sinistros_Invalidos = Nothing
    
    '*
    'EMAIL_ALERTA_LOG'
    
    If Not Monta_Flex_Entrada Then
        StatusBar1.SimpleText = "Nenhuma remessa encontrada !"
    Else
        Cont_Sucesso = 0
        For i = 1 To flexEntrada.Rows - 1
            Sinistro_BB = ""
        
            'Limpando vari�veis
            ALocalizacao_Aviso = ""
            Erro_Carga = 0
            
            Set iLayout = New clLayout
            Set ErroGTR = New clErro
            'dicaversan - SQG
            If Left(flexEntrada.TextMatrix(i, GRID_ENT_EVENTO), 4) = 2301 Then
                Sinistro_BB = Busca_sinistro_bb(flexEntrada.TextMatrix(i, GRID_ENT_SEQ))
            End If
            If Left(flexEntrada.TextMatrix(i, GRID_ENT_EVENTO), 4) = 2300 Then
                Sinistro_BB = "00000000000"
            End If
            'FIM
            
            ' Ajusta o grid na tela.
            If i > 6 Then flexEntrada.TopRow = i - 5
            
               If Sinistro_Invalido(flexEntrada.TextMatrix(i, GRID_ENT_SINISTRO_BB)) Then
'''            If Sinistro_Invalido(flexEntrada.TextMatrix(i, GRID_ENT_SINISTRO_BB)) Or Mid(flexEntrada.TextMatrix(i, GRID_ENT_EVENTO), 1, 4) = 2000 Then
                Atualiza_FlexEntrada i, "BLOQUEADO"
                'Grava_Log_GTR Num_Arquivo, Data_Hora, Entrada_GTR_id, Evento_BB, "Ok"
               'Adicionado por Maur�cio (Stefanini), em 14/09/2005 para verificar se j� existe o evento 2000. (O sinBB 20050800828 chegou 2003 antes do 2000)
                 'LSA - 02/03/2010 - Verificar se j� existe o evento 2300.
               'DICAVERSAN - SQG
               ElseIf Left(flexEntrada.TextMatrix(i, GRID_ENT_EVENTO), 4) = "2003" And _
                      Not Sinistro_Tem_2000(IIf(flexEntrada.TextMatrix(i, GRID_ENT_SINISTRO_BB) = "", Sinistro_BB, flexEntrada.TextMatrix(i, GRID_ENT_SINISTRO_BB))) And _
                      Not Sinistro_Tem_2300(IIf(flexEntrada.TextMatrix(i, GRID_ENT_SINISTRO_BB) = "", Sinistro_BB, flexEntrada.TextMatrix(i, GRID_ENT_SINISTRO_BB))) Then
               'FIM
             
                Atualiza_FlexEntrada i, "BLOQUEADO"
            Else
                Atualiza_FlexEntrada i, "TEMPO"
                Cont_remessas = Cont_remessas + 1
                StatusBar1.SimpleText = "Processando remessas recebidas (" & Cont_remessas & " de " & (flexEntrada.Rows - 1) & ") ..."
                
                Qtd_Registros = Val(flexEntrada.TextMatrix(i, GRID_ENT_QTD))
                Evento_BB = Left(flexEntrada.TextMatrix(i, GRID_ENT_EVENTO), 4)
                Cod_transacao = Left(flexEntrada.TextMatrix(i, GRID_COD_TRANSACAO), 4)
                iLayout.Layout_Id = Obtem_Layout(Evento_BB, Cod_transacao)
                Entrada_GTR_ID = flexEntrada.TextMatrix(i, GRID_ENT_SEQ)
                'DICAVERSAN - SQG
                                
                If Sinistro_BB = "" Then
                    Sinistro_BB = IIf(flexEntrada.TextMatrix(i, GRID_ENT_SINISTRO_BB) = "", "0", flexEntrada.TextMatrix(i, GRID_ENT_SINISTRO_BB))
                End If
                'FIM
                OCod_Remessa = flexEntrada.TextMatrix(i, GRID_ENT_COD_REMESSA)
            
                'Critica a localiza��o do AVISO se o evento for diferente do evento de AVISO
                If Critica_Localizacao(Evento_BB, Sinistro_BB, Entrada_GTR_ID, ALocalizacao_Aviso) Then
                    If Cod_transacao = "SN00" Then
                        ' Monta o vetor com os registros da entrada_gtr_registro_tb
                        Call Carrega_Registros(Entrada_GTR_ID, iLayout)
                    End If

                    
                    ' Carrega SEGBR
'                    rdocn_Interface.BeginTrans
'                    rdocn_InterfaceAB.BeginTrans
'                    rdocn.BeginTrans
'                    rdocn_SEG1.BeginTrans
                                        
                    'Alterado por Gustavo Machado Costa em 12/06/2003
                    If Evento_BB = 2003 Then
                        If Not Verifica_LOG_Robo(Sinistro_BB) Then
                           Atualiza_Situacao_Aviso Sinistro_BB
                           Atualiza_FlexEntrada i, "BLOQUEADO"
                        End If
                    End If
                    
                    If iLayout.Layout_Id = "" Then
                         Erro_Carga = 14 ' evento n�o cadastrado.
                    Else
                         If Cod_transacao = "SN00" Then
                            ' Carrega as tabelas tempor�rias.
                            Erro_Carga = Carrega_Critica_Layout(1, rdocn, iLayout, ErroGTR)
                         End If
                    End If
                    
                    If Erro_Carga > 0 Then
                        'MsgBox Erro_Carga & "-" & Error(Erro_Carga)
                        Atualiza_FlexEntrada i, ""
                        ' Adiciona a cole��o de sinistros que
                        ' n�o podem ser tratados. (se deu erro, os pr�ximos
                        ' eventos desse sinistro n�o podem ser processados).
                        Sinistros_Invalidos.Add flexEntrada.TextMatrix(i, GRID_ENT_SINISTRO_BB)
                        
                        cmdIniciar.Caption = "&Iniciar            "
                        picBotao(0).Visible = True
                        picBotao(1).Visible = False
                        tmrConsultaEntrada.Enabled = False
                        'Grava_Log_GTR Num_Arquivo, Data_Hora, Entrada_GTR_id, Evento_BB, Erro_carga
'                        rdocn_Interface.RollbackTrans
'                        rdocn_InterfaceAB.RollbackTrans
'                        rdocn.RollbackTrans
'                        rdocn_SEG1.RollbackTrans
                        String_tabelas_Temp = ""
                    Else
                        If Evento_BB = 2000 Then
                           SQL = "SELECT proposta_bb FROM ##D_GTR_SN00"
                           Set rs = rdocn.OpenResultset(SQL)
                           If Not rs.EOF Then
                              rs.Close
                              GoTo processa
                           Else
                              'N�O FORAM CARREGADOS OS DADOS DO SINISTRO NO LOG
                              Atualiza_FlexEntrada i, "BLOQUEADO"
                           End If
                           rs.Close
                        Else
processa:
                            If Cod_transacao = "SN00" Then
                                    SQL = "select sinistro_bb from ##chave_GTR_SN00"
                                    Set rs = rdocn.OpenResultset(SQL)
                                    sinistro_bb_temp = rs(0)
                                
                                'RSilva - 23/11/2010
                                'validando sinistro informado pelo arquivo para processamento
                                If Val(Sinistro_BB) = Val(sinistro_bb_temp) Or Val(flexEntrada.TextMatrix(i, GRID_ENT_SINISTRO_BB)) = Val(sinistro_bb_temp) Then
                                    
                                    'Executa_SP_Inclusao_SEGBR Layout_Id
                                    If Executa_SP_Inclusao_SEGBR(iLayout.Layout_Id, Sinistro_BB) Then
                                    
                                        '* Verifica se recebeu um evento de documenta��o, se receber um dos eventos
                                        '* (2003, 2011, 2014) ent�o grava o registro na tabela documentos_recebidos_GTR_tb
                                        If (Evento_BB = 2003) Or (Evento_BB = 2011) Or (Evento_BB = 2014) Then
                                           Call Grava_Documento_Recebido_GTR(OCod_Remessa, Sinistro_BB, Evento_BB)
                                        End If
                                        
                                        'Alterado por Gustavo Machado Costa em 14/07/2003
                                        'Rotina que envia email para o tecnico ao importar os eventos 2001,2002,2004,2008,2009,2010,2012 ou 2013.
                                        If (Evento_BB = 2001) Or (Evento_BB = 2002) Or (Evento_BB = 2004) Or (Evento_BB = 2008) Or _
                                           (Evento_BB = 2009) Or (Evento_BB = 2010) Or (Evento_BB = 2012) Or (Evento_BB = 2013) Then
        ''                                   Envia_Email_Tecnico Sinistro_BB, rdocn, Evento_BB
                                        End If
                                        
                                        '* Verifica se � aviso ou cancelamento e se a localiza��o � igual ao LOG
                                        If (Evento_BB = 2000) Or (Evento_BB = 2005 And ALocalizacao_Aviso = "LOG") Then 'SE FOR AVISO OU CANCELAMENTO, VERIFICA A LOCALIZA��O
                                           'Verifica se o aviso foi gravado no LOG, se "SIM" ent�o envia e-MAIL pra CAVIS
                                           If Aviso_LOG(Sinistro_BB) Then
                                              If Evento_BB = 2000 Then
                                                 OAssunto = "Aviso de Sinistro no LOG - N� " & Sinistro_BB
                                                 AMensagem = "CAVIS, " & vbCrLf & vbCrLf
                                                 AMensagem = AMensagem & "Aviso de Sinistro n�. " & Sinistro_BB & " presente no LOG." & vbCrLf
                                                 AMensagem = AMensagem & "" & vbCrLf
                                                 AMensagem = AMensagem & "------------------------------------------------------------------" & vbCrLf
                                                 AMensagem = AMensagem & "Comunica��o Corporativa da Alian�a do Brasil"
                                              Else
                                                 OAssunto = "Cancelamento do Aviso de Sinistro no LOG - N� " & Sinistro_BB
                                                 AMensagem = "CAVIS, " & vbCrLf & vbCrLf
                                                 AMensagem = AMensagem & "Cancelamento do Aviso de Sinistro n�. " & Sinistro_BB & " presente no LOG." & vbCrLf
                                                 AMensagem = AMensagem & "" & vbCrLf
                                                 AMensagem = AMensagem & "------------------------------------------------------------------" & vbCrLf
                                                 AMensagem = AMensagem & "Comunica��o Corporativa da Alian�a do Brasil"
                                              End If
                                              '* Carrega o destinat�rio do eMail
                                              'ODestinatario = "CAVIS" 'ObtemConteudo("EMAIL_ALERTA_LOG")
                                              ODestinatario = "CAVIS@aliancadobrasil.com.br"
                                              '* Carrega todos os dados da tela de LOG do rob�
                                              AMensagem_Aux = Monta_Tela_LOG(Sinistro_BB)
                                              AMensagem = AMensagem & vbNewLine & vbNewLine & vbNewLine & AMensagem_Aux
                                              '* Envia e-Mail
                                              'ctleMail.Enviar_Email cUserName, ODestinatario, OAssunto, AMensagem
                                              SQL = "EXEC envia_email_sp "
                                              SQL = SQL & "'" & ODestinatario & "',"
                                              SQL = SQL & "'" & OAssunto & "',"
                                              SQL = SQL & "'" & Replace(AMensagem, "'", "�") & "'"
                                              
                                              rdocn.Execute (SQL)
                                              
                                           End If
                                        End If
                                        Atualiza_FlexEntrada i, "OK"
                                        ' Atualiza status do registro na tabela de
                                        ' entrada para n�o processar mais.
                                        Atualiza_Entrada Entrada_GTR_ID
                                        Cont_Sucesso = Cont_Sucesso + 1
                                        'Grava_Log_GTR Num_Arquivo, Data_Hora, Entrada_GTR_id, Evento_BB, "Ok"
                                        'Debug.Print Err.Description
'                                        rdocn_Interface.CommitTrans
'                                        rdocn_InterfaceAB.CommitTrans
'                                        rdocn.CommitTrans
'                                        rdocn_SEG1.CommitTrans
                                    Else
                                       'N�O FORAM CARREGADOS OS DADOS DO SINISTRO NO LOG
                                       Atualiza_FlexEntrada i, "BLOQUEADO"
'                                        rdocn_Interface.RollbackTrans
'                                        rdocn_InterfaceAB.RollbackTrans
'                                        rdocn.RollbackTrans
'                                        rdocn_SEG1.RollbackTrans
                                       String_tabelas_Temp = ""
                                    End If '* Executa_SP_Inclusao_SEGBR
                                
                                Else
                                        Atualiza_FlexEntrada i, "BLOQUEADO"
'                                        rdocn_Interface.RollbackTrans
'                                        rdocn_InterfaceAB.RollbackTrans
'                                        rdocn.RollbackTrans
'                                        rdocn_SEG1.RollbackTrans
                                        String_tabelas_Temp = ""
                                End If
                                
                            Else  'Cod_transacao = 'ST00' Kylme 26/11/2004
                                If Executa_Entrada_Registro(Entrada_GTR_ID, Evento_BB) Then
                                    If (Evento_BB = 2003) Or (Evento_BB = 2011) Or (Evento_BB = 2014) Then
                                       Call Grava_Documento_Recebido_GTR(OCod_Remessa, Sinistro_BB, Evento_BB)
                                    End If
                                    
                                    Atualiza_FlexEntrada i, "OK"
                                    ' Atualiza status do registro na tabela de
                                    ' entrada para n�o processar mais.
                                    Atualiza_Entrada Entrada_GTR_ID
                                    Cont_Sucesso = Cont_Sucesso + 1
                                    'Grava_Log_GTR Num_Arquivo, Data_Hora, Entrada_GTR_id, Evento_BB, "Ok"
'                                    rdocn_Interface.CommitTrans
'                                    rdocn_InterfaceAB.CommitTrans
'                                    rdocn.CommitTrans
'                                    rdocn_SEG1.CommitTrans

                                Else
                                    'N�O FORAM CARREGADOS OS DADOS DO SINISTRO NO LOG
                                    Atualiza_FlexEntrada i, "BLOQUEADO"
'                                    rdocn_Interface.RollbackTrans
'                                    rdocn_InterfaceAB.RollbackTrans
'                                    rdocn.RollbackTrans
'                                    rdocn_SEG1.RollbackTrans
                                    String_tabelas_Temp = ""
                                End If
                            End If '
                            
                        End If '* Evento_BB = 2000
                    End If '* Erro_Carga
                Else
                    'O AVISO SE ENCONTRA NO ROB�
                    Atualiza_FlexEntrada i, "BLOQUEADO"
                End If '* Critica_Localizacao
            End If '* Sinistro_Invalido
Continue:
        Next i
        
        StatusBar1.SimpleText = "Total de " & Cont_Sucesso & " remessa(s) processada(s) com sucesso !"
    End If
    
    cmdSair.Enabled = True
    cmdCarregarAgora.Enabled = True
    cmdIniciar.Enabled = True
    picBotao(0).Enabled = True
    picBotao(1).Enabled = True
    
    Exit Sub
Resume
Erro:
'                                rdocn_Interface.RollbackTrans
'                                rdocn_InterfaceAB.RollbackTrans
'                                rdocn.RollbackTrans
'                                rdocn_SEG1.RollbackTrans

    String_tabelas_Temp = ""
    Call Enviar_Notificacao("SMQP0061", "SMQP0061 - Carrega_SEGBR" & vbCrLf & " Erro: " & Err.Number & " Descri��o: " & Err.Description)
    'Call TrataErroGeral("Carrega_SEGBR", Me.name)
    Call GravaMensagem("Rotina: Carrega_SEGBR." & vbCrLf & "Descri��o Erro: " & Err.Description & " .", "VB", "Carrega_SEGBR")
    'Call TerminaSEGBR
    GoTo Continue

End Sub


Private Function Executa_Entrada_Registro(Entrada_GTR_ID As String, Evento_BB As String) As Boolean
'**************************************************************************************************
'Data Altera��o: 18/06/2013
'Projeto       : Melhorias do SEGBR
'Autores       : Jorge Benko
'Descri��o     : Alterado o nome da tabela interface_db..entrada_GTR_tb para interface_db.dbo.entrada_GTR_atual_tb
'                Alterado o nome da tabela seguros_db.dbo.evento_segbr_sinistro_tb para seguros_db.dbo.evento_segbr_sinistro_atual_tb
'                Inserido o comanda With em todos os (NOLOCK)
'                Alterado a conex�o de Rdo para Ado utilizando a mesma variavel de record Rs e RS2
'                Alterado o select do Evento_BB = 8199 pois estava sendo repetido com as mesmas condi��es
'**************************************************************************************************

Dim rs As rdoResultset
Dim Rs2 As rdoResultset
Dim COD_EMPRESA As String
Dim SQL As String
Dim evento_gerado As String
    On Error GoTo Erro
    
    Executa_Entrada_Registro = False
   
    If Evento_BB = 8199 Then
        SQL = " exec interface_db.dbo.segs9155_sps " & Entrada_GTR_ID
    Else
        SQL = " select distinct c.cod_empresa from interface_db.dbo.entrada_gtr_atual_tb a With(nolock)"
        SQL = SQL & " inner join seguros_db.dbo.evento_segbr_sinistro_atual_tb b With(nolock)"
        SQL = SQL & " on a.sinistro_bb = b.sinistro_bb"
        SQL = SQL & " inner join seguros_db.dbo.ramo_tb c With(nolock) on"
        SQL = SQL & " C.ramo_id = b.ramo_id"
        SQL = SQL & " where b.ramo_id is not null and entrada_gtr_id = " & Entrada_GTR_ID
 
    End If
    
    Set Rs2 = rdocn.OpenResultset(SQL)
    
    If Rs2.EOF Then
        Rs2.Close
        Executa_Entrada_Registro = False
    Else
        COD_EMPRESA = Trim(Rs2(0))
        
        Rs2.MoveNext
        If Not Rs2.EOF Then
            COD_EMPRESA = "XXX"
        End If
        
        Rs2.Close
    
        If ((glAmbiente_id = 2 Or glAmbiente_id = 3) And COD_EMPRESA = "ABS") Or ((glAmbiente_id = 6 Or glAmbiente_id = 7) And COD_EMPRESA = "AB") Then
            SQL = ""
            SQL = " set xact_abort on exec " & GTR_banco & ".dbo.processa_entrada_registro_spi " & Entrada_GTR_ID
            
            Set rs = rdocn.OpenResultset(SQL) 'jorge benko ficara com RDO ate defini��o da base em ADO
            
            If Evento_BB = 8199 And rs(0) = 0 Then
                Executa_Entrada_Registro = True
            Else
                evento_gerado = rs(0)
                SQL = "select evento_id from seguros_db.dbo.evento_segbr_sinistro_atual_tb With(nolock) where evento_id = " & evento_gerado

                rs.Close

                Set rs = rdocn.OpenResultset(SQL) 'jorge benko ficara com RDO ate defini��o da base em ADO

                If Not rs.EOF Then
                    Executa_Entrada_Registro = True
                End If
            End If
      
        Else
            Executa_Entrada_Registro = False
            COD_EMPRESA = "XXX"
        End If
        
        
    End If
    Exit Function

Erro:
    'MsgBox "Erro: " & Err.Number & " - " & Err.Description
    Call Grava_Trace(Err.Number, Err.Description, "Executa_Entrada_Registro")
     Exit Function
End Function
Private Function Executa_SP_Inclusao_SEGBR(ByVal Layout_Id As String, ByVal OSinistro_BB As String) As Boolean
    
    '* ************************************************************************************ *'
    '* Alterado por Junior em 21/02/2003                                                    *'
    '* O par�metro OSinistro_BB s� � utilizado em caso de erro para informar o n�mero do    *'
    '* sinistro_BB com erro.                                                                *'
    '* ************************************************************************************ *'
    
    Dim SQL As String, rs As rdoResultset
    Dim SP As String
    Dim conta   As Integer
    Dim Rs2 As rdoResultset
    Dim COD_EMPRESA As String
    Dim vSsinistro_id As String
    Dim vSsinistro_bb As String
    Dim vEvento_bb As Integer
    Dim vEvento_original As Integer
    On Error GoTo Erro
    
    conta = 0
    
    Executa_SP_Inclusao_SEGBR = False
    
    SQL = "SELECT sp_tratamento FROM interface_db.dbo.GTR_layout_tb WITH (NOLOCK) "
    SQL = SQL & " WHERE layout_id = " & Layout_Id
    Set rs = rdocn_Interface.OpenResultset(SQL)
    SP = rs("sp_tratamento")
    'rs.Close
    
    SQL = "select sinistro_id, sinistro_bb, evento_bb_id from ##chave_GTR_SN00"
    Set Rs2 = rdocn.OpenResultset(SQL)
    
    vSsinistro_id = Rs2("sinistro_id")
    vSsinistro_bb = Rs2("sinistro_bb")
    vEvento_bb = Rs2("evento_bb_id")
    
    Rs2.Close
    
    If vEvento_bb <> "2300" Then
    
        SQL = "SELECT sinistro_id, sinistro_bb" & vbNewLine
        SQL = SQL & " FROM seguros_db..sinistro_bb_tb  sinistro_bb_tb WITH (NOLOCK)"
        SQL = SQL & " WHERE sinistro_id = " & vSsinistro_bb
        SQL = SQL & "    OR sinistro_bb = " & vSsinistro_bb
    
        Set Rs2 = rdocn.OpenResultset(SQL)
        
        If Not Rs2.EOF Then
            vSsinistro_id = Rs2("sinistro_id")
            vSsinistro_bb = Rs2("sinistro_bb")
        End If
        Rs2.Close
 
    End If
    
    
    SQL = ""
    If vEvento_bb = 2000 Then
        'DEMANDA 15134427 - flavio.abreu - Inclusao de with(nolock)
        SQL = "select distinct r.COD_EMPRESA from ##D_GTR_SN00 c"
        SQL = SQL & " inner join seguros_db..proposta_adesao_tb pa with(nolock) on pa.PROPOSTA_BB = c.PROPOSTA_bb"
        'Francisco H. Berrocal - INC000003790574 - N�o processava eventos 2000
        'SQL = SQL & " inner join seguros_db..ramo_tb r on with(nolock) r.ramo_id = pa.ramo_id"
        SQL = SQL & " inner join seguros_db..ramo_tb r with(nolock) on r.ramo_id = pa.ramo_id"
        SQL = SQL & " Union All" 'flavio.abreu - inclusao de clausula All
        SQL = SQL & " select distinct r.COD_EMPRESA from ##D_GTR_SN00 c"
        SQL = SQL & " inner join seguros_db..proposta_fechada_tb pf with(nolock) on pf.PROPOSTA_BB = c.PROPOSTA_bb"
        SQL = SQL & " inner join seguros_db..proposta_tb p with(nolock) on pf.proposta_id = p.proposta_id"
        SQL = SQL & " inner join seguros_db..ramo_tb r with(nolock) on r.ramo_id = p.ramo_id"
        
        
    Else
        If vEvento_bb = 2299 Or vEvento_bb = 2003 Then
            
            If vEvento_bb <> 2003 Then
                SQL = "select evento_bb_original from ##T_GTR_SN00"
                Set Rs2 = rdocn.OpenResultset(SQL)
                vEvento_original = Rs2("evento_bb_original")
            End If
            If vEvento_original = 1100 And vEvento_bb <> 2003 And Val(vSsinistro_bb) <> Val("0") Then
                'DEMANDA 15134427 - flavio.abreu - Inclusao de with(nolock)
                SQL = "select distinct r.COD_EMPRESA from ##T_GTR_SN00 c "
                SQL = SQL & " inner join seguros_db..evento_segbr_sinistro_atual_tb s with(nolock) on"
                SQL = SQL & " (s.sinistro_id = " & vSsinistro_id & ") "
                SQL = SQL & " and num_remessa = c.num_remessa_original"
                SQL = SQL & " inner join seguros_db..ramo_tb r with(nolock) on r.ramo_id = s.ramo_id "
            ElseIf Val(vSsinistro_bb) <> Val("0") Then
                SQL = "select distinct r.COD_EMPRESA from ##chave_GTR_SN00 c "
                SQL = SQL & " inner join seguros_db..sinistro_bb_tb s with(nolock) on"
                SQL = SQL & " (s.sinistro_bb = " & vSsinistro_bb & ") "
                SQL = SQL & " inner join seguros_db..sinistro_tb si with(nolock) on si.sinistro_id = s.sinistro_id"
                SQL = SQL & " and situacao <> 7"
                SQL = SQL & " inner join seguros_db..ramo_tb r with(nolock) on r.ramo_id = s.ramo_id "
            ElseIf vEvento_original = 1300 Then
                SQL = "update ##chave_GTR_SN00 set sinistro_bb = null where sinistro_bb = 0"
                Call rdocn.Execute(SQL)
                SQL = "update ##chave_GTR_SN00 set sinistro_id = null where sinistro_id = 0"
                Call rdocn.Execute(SQL)
                SQL = "SELECT 'ABS' cod_empresa  union select 'AB' cod_empresa "
            ElseIf Val(vSsinistro_bb) = Val("0") Then
                SQL = "select distinct r.COD_EMPRESA from ##chave_GTR_SN00 c "
                SQL = SQL & " inner join seguros_db..sinistro_bb_tb s with(nolock) on"
                SQL = SQL & " (s.sinistro_bb = " & vSsinistro_bb & ") "
                SQL = SQL & " inner join seguros_db..sinistro_tb si with(nolock) on si.sinistro_id = s.sinistro_id"
                SQL = SQL & " and situacao <> 7"
                SQL = SQL & " inner join seguros_db..ramo_tb r with(nolock) on r.ramo_id = s.ramo_id "
            End If
        
        'MFrasca - 27/12/2010 - Flow 540443 - SQG - Ajuste para identificar empresa para eventos do SQG, pois estava apresentando erro
        'para os eventos que n�o possuiam no tabel�o o evento de aviso (1100 / 2000) e por isso identificava a empresa errada
        'no join com o tabel�o. Para esse produto o aviso � criado automaticamente quando recebemos o 2300.
        ElseIf vEvento_bb = 2300 Or vEvento_bb = 2301 Or vEvento_bb = 2302 Or vEvento_bb = 2303 Or vEvento_bb = 2304 Then
            
            SQL = "select cod_empresa = 'ABS'"
        '-----------------------------------------------
        Else
                'DEMANDA 15134427 - flavio.abreu - Inclusao de with(nolock)
            If (vEvento_bb = 2011 Or vEvento_bb = 2014 Or vEvento_bb = 2015 Or vEvento_bb = 2005 Or vEvento_bb = 2007) And Val(vSsinistro_bb) <> Val("0") Then
                SQL = "select distinct r.COD_EMPRESA from ##chave_GTR_SN00 c "
                SQL = SQL & " inner join seguros_db..evento_segbr_sinistro_atual_tb s with(nolock) on"
                SQL = SQL & " (s.sinistro_bb = " & vSsinistro_bb & ") "
                SQL = SQL & " and (s.evento_bb_id = 1100 or s.evento_bb_id = 2000) "
                SQL = SQL & " inner join seguros_db..ramo_tb r with(nolock) on r.ramo_id = s.ramo_id "
            
            ElseIf Val(vSsinistro_bb) <> Val("0") Then
                'DEMANDA 15134427 - flavio.abreu - Inclusao de with(nolock)
                SQL = "select distinct isnull(cod_empresa, 'XXX') COD_EMPRESA from seguros_db.dbo.ramo_tb where ramo_id in("
                SQL = SQL & " select distinct isnull(s.ramo_id,0) from ##chave_GTR_SN00 c "
                SQL = SQL & " inner join seguros_db..evento_segbr_sinistro_atual_tb s with(nolock) on"
                SQL = SQL & " (s.sinistro_bb = " & vSsinistro_bb & ") "
                SQL = SQL & " and (s.evento_bb_id = 1100 or s.evento_bb_id = 2000) )"
                'SQL = SQL & " inner join seguros_db..ramo_tb r on r.ramo_id = s.ramo_id "
            End If
        End If
    End If
    
        If SQL <> "" Then
            Set Rs2 = rdocn.OpenResultset(SQL)
        
        If Rs2.EOF Then
            COD_EMPRESA = "XXX"
            
        Else
            If Rs2(0) <> 0 Then
            
                COD_EMPRESA = Trim(Rs2("COD_EMPRESA"))
                Rs2.MoveNext
                
                If Not Rs2.EOF Then
                    COD_EMPRESA = "XXX"
                    
                    Rs2.Close
                    SQL = "select cod_produto_bb from ##chave_GTR_SN00 "
                    Set Rs2 = rdocn.OpenResultset(SQL)
                    
                    If Rs2("cod_produto_bb") = 100006 Then
                        COD_EMPRESA = "AB"
                    End If
                    If (Rs2("cod_produto_bb") = 100005) Or (Rs2("cod_produto_bb") = 8229156) Then
                        COD_EMPRESA = "ABS"
                    End If
                    
                    If COD_EMPRESA = "XXX" And Val(vSsinistro_bb) <> Val("0") Then
                        Rs2.Close
                        'DEMANDA 15134427 - flavio.abreu - Inclusao de with(nolock)
                        'SQL = "select * from seguros_db..evento_segbr_sinistro_tb with(nolock) where dt_inclusao >= '20100904' and sinistro_bb = " & vSsinistro_bb
                        SQL = "select evento_id, sinistro_id, cod_produto_bb from seguros_db..evento_segbr_sinistro_tb with(nolock) where dt_inclusao >= '20100904' and sinistro_bb = " & vSsinistro_bb
                        Set Rs2 = rdocn.OpenResultset(SQL)
                        If Rs2.EOF Then
                            COD_EMPRESA = "XXX"
                        Else
                            If glAmbiente_id = 6 Or glAmbiente_id = 7 Then
                            '#If glAmbiente_id = 7 Then
                                COD_EMPRESA = "ABS"
                            Else
                                COD_EMPRESA = "AB"
                            End If
                        End If
                    
                        Rs2.Close
                    
                    End If
                    
                End If
            Else
                COD_EMPRESA = "AB"
            End If
    
        End If
     Else
        COD_EMPRESA = "XXX"
     
     End If
    
    If COD_EMPRESA = "XXX" And Val(vSsinistro_bb) <> Val("0") Then
        'DEMANDA 15134427 - flavio.abreu - Inclusao de with(nolock)/Substitui��o de Select *
        'SQL = "select * from seguros_db..evento_segbr_sinistro_tb where dt_inclusao >= '20100904' and sinistro_bb = " & vSsinistro_bb
        SQL = "select evento_id, sinistro_id, cod_produto_bb from seguros_db..evento_segbr_sinistro_tb with(nolock) where dt_inclusao >= '20100904' and sinistro_bb = " & vSsinistro_bb
        Set Rs2 = rdocn.OpenResultset(SQL)
        
        If Rs2.EOF Then
            COD_EMPRESA = "XXX"
        Else
            
            If Rs2("cod_produto_bb") = 100006 Then
                COD_EMPRESA = "AB"
            ElseIf (Rs2("cod_produto_bb") = 100005) Or (Rs2("cod_produto_bb") = 8229156) Then
                COD_EMPRESA = "ABS"
            
            ElseIf Not IsNull(Rs2("sinistro_id")) Then
                'DEMANDA 15134427 - flavio.abreu - Inclus�o de with(nolock)/Substitui��o de Select * p/Select campo(sinistro_id)
                'SQL = "select * from sinistro_tb where situacao <> 7 and sinistro_id = " & RS2("sinistro_id")
                SQL = "select sinistro_id from sinistro_tb with(nolock) where situacao <> 7 and sinistro_id = " & Rs2("sinistro_id")
                Rs2.Close
                Set Rs2 = rdocn.OpenResultset(SQL)
                If Not Rs2.EOF Then
                    If glAmbiente_id = 6 Or glAmbiente_id = 7 Then
                    '#If glAmbiente_id = 7 Then
                        COD_EMPRESA = "ABS"
                    Else
                        COD_EMPRESA = "AB"
                    End If
                End If
            ElseIf glAmbiente_id = 6 Or glAmbiente_id = 7 Then
            '#ElseIf glAmbiente_id = 7 Then
                    COD_EMPRESA = "ABS"
            ElseIf glAmbiente_id = 2 Or glAmbiente_id = 3 Then
            '#ElseIf glAmbiente_id = 3 Then
                    COD_EMPRESA = "AB"
            End If
        End If
    Rs2.Close
    End If
    
    If COD_EMPRESA = "XXX" Then
    
      SQL = "select cod_produto_bb from ##chave_GTR_SN00 "
      Set Rs2 = rdocn.OpenResultset(SQL)
      
      If Rs2("cod_produto_bb") = 100006 Then
            COD_EMPRESA = "AB"
      End If
      If (Rs2("cod_produto_bb") = 100005) Or (Rs2("cod_produto_bb") = 8229156) Then
            COD_EMPRESA = "ABS"
      End If
    
    
    End If
    
    If (glAmbiente_id = 6 Or glAmbiente_id = 7) And COD_EMPRESA = "ABS" Then
    '#If glAmbiente_id = 7 And COD_EMPRESA = "ABS" Then
       
        SQL = "set xact_abort on EXEC " & SP & " "
        SQL = SQL & "'" & Format(Data_Sistema, "yyyymmdd") & "'" ' @dt_evento
        SQL = SQL & ", " & 5 ' @entidade_id (5 = GTR)
        SQL = SQL & ", '" & cUserName & "'" ' @usuario
        Call rdocn.Execute(SQL)

Executa_SP_Inclusao_SEGBR = True
             
    '#ElseIf glAmbiente_id = 3 And COD_EMPRESA = "AB" Then
    ElseIf (glAmbiente_id = 2 Or glAmbiente_id = 3) And COD_EMPRESA = "AB" Then
    
        If Val(vSsinistro_bb) = Val("0") Then
            GoTo Erro
        End If
            
        SQL = "EXEC " & SP & " "
        SQL = SQL & "'" & Format(Data_Sistema, "yyyymmdd") & "'" ' @dt_evento
        SQL = SQL & ", " & 5 ' @entidade_id (5 = GTR)
        SQL = SQL & ", '" & cUserName & "'" ' @usuario
        Call rdocn.Execute(SQL)

Executa_SP_Inclusao_SEGBR = True
       
    Else
        Executa_SP_Inclusao_SEGBR = False
        Exit Function
    
     End If
    
    
    
'    'Alterado por Gustavo Machado em 29/09/2003
'    If rs(0) = 0 Then
'       rdocn.RollbackTrans
'    Else
     Executa_SP_Inclusao_SEGBR = True
'    End If
    Exit Function
    
Erro:
    'Inclu�do este tratamento por Junior em 18/06/2003
    'If conta < 2 Then
    '   conta = conta + 1
    '   Resume
    'Else
    '  rdocn.RollbackTrans
    '  String_tabelas_Temp = ""
    'Francisco H. Berrocal - enviar email no caso de erro
    Call Enviar_Notificacao("SMQP0061", "SMQP0061 - Executa_SP_Inclusao_SEGBR" & vbCrLf & " Erro: " & Err.Number & " Descri��o: " & Err.Description)
    Executa_SP_Inclusao_SEGBR = False
    'Call MensagemBatch("Rotina: Executa_SP_Inclusao_SEGBR." & vbCrLf & "Sinistro_BB: " & OSinistro_BB & vbCrLf & "Stored Procedure: " & SP & vbCrLf & "Descri��o Erro: " & rdoErrors(1) & vbCrLf & " O programa ser� cancelado.", vbOKOnly, "Erro incluindo registro SEGBR", True, Me)
    'Call TerminaSEGBR
    'End If
    
End Function

Private Function Obtem_Layout(Evento_BB As String, Cod_transacao As String) As String

    Dim Evento_Layout As clEvento_Layout

    Obtem_Layout = ""
    
    If Cod_transacao = "SN00" Then
        For Each Evento_Layout In Eventos
            If Evento_Layout.evento_BB_ID = Evento_BB Then
                Obtem_Layout = Evento_Layout.Layout_Id
                Exit For
            End If
        Next
    Else
        For Each Evento_Layout In Eventos_ALS
            If Evento_Layout.evento_BB_ID = Evento_BB Then
                Obtem_Layout = Evento_Layout.Layout_Id
                Exit For
            End If
        Next
    End If
    
End Function
Private Function Monta_Flex_Entrada() As Boolean

    ' Se n�o existir registro para ser carregado, retorna False.
    Dim SQL As String, rs As rdoResultset
    Dim Linha As String

    'Anderson Fiuza 07/07/2010 - N�o buscar os sinistros_bb�s que estejem nulos.
    
    'C�d. da Transa��o
    lblCodTrans = ObtemConteudo("BB_COD_TRANS_ENTRADA") '"SN00"
    
    flexEntrada.Rows = 1

    SQL = ""
    SQL = "SELECT e.cod_remessa, e.qtd_registros, e.entrada_GTR_id, e.evento_BB, s.descricao, e.sinistro_bb, e.cod_transacao"

    SQL = SQL & " FROM interface_db..entrada_GTR_ATUAL_tb e With(NOLOCK) "

    SQL = SQL & "   INNER JOIN SEGUROS_DB.dbo.evento_SEGBR_tb s With(NOLOCK) "
    SQL = SQL & "       ON (s.evento_BB_id = e.evento_BB)"
    SQL = SQL & " WHERE situacao      = 'n' "
    SQL = SQL & " AND cod_transacao = 'SN00'"
    SQL = SQL & " AND   evento_BB not in(8199,9999) "

    SQL = SQL & " union all " 'flavio.abreu - 2012-07-24 - Inclusao de "All"

    SQL = SQL & "SELECT e.cod_remessa, e.qtd_registros, e.entrada_GTR_id, e.evento_BB, s.descricao, e.sinistro_bb, e.cod_transacao"

    SQL = SQL & " FROM interface_db..entrada_GTR_ATUAL_tb e With(NOLOCK) "

    SQL = SQL & " INNER JOIN SEGUROS_DB.dbo.evento_SEGBR_tb s With(NOLOCK) "
    SQL = SQL & "  ON (s.evento_BB_id = e.evento_BB)"
    SQL = SQL & " WHERE situacao      = 'n'  "
    SQL = SQL & " AND  cod_transacao = 'ST00'"
    SQL = SQL & " AND   evento_BB not in (2299,9999) "

    SQL = SQL & " ORDER BY e.entrada_GTR_id"

    Set rs = rdocn_Interface.OpenResultset(SQL)
    
    While Not rs.EOF
        Linha = vbTab & rs("qtd_registros") & vbTab
        Linha = Linha & rs("entrada_gtr_id") & vbTab
        Linha = Linha & rs("evento_bb") & " - " & rs("descricao") & vbTab
        Linha = Linha & rs("sinistro_bb") & vbTab
        Linha = Linha & rs("cod_remessa") & vbTab
        Linha = Linha & rs("cod_transacao")
        flexEntrada.AddItem Linha

        rs.MoveNext
    Wend
    rs.Close
    
    If flexEntrada.Rows = 1 Then
        Monta_Flex_Entrada = False
    Else
        Monta_Flex_Entrada = True
    End If

End Function

Private Sub Monta_Sinistros_Bloqueados()

'**************************************************************************************************
'Data Altera��o: 18/06/2013
'Projeto       : Melhorias do SEGBR
'Autores       : Jorge Benko
'Descri��o     : Alterado a conex�o de Rdo para Ado utilizando a mesma variavel de record Rs
'**************************************************************************************************
       
    'In�cio - Melhorias do SEGBR - Jorge Benko - 18/06/2013
    Dim SQL As String, rs As rdoResultset, Sinistro_BB As String
'    Dim SQL As String, Sinistro_BB As String
'    Dim rs As New ADODB.Recordset
    'Fim - Melhorias do SEGBR - Jorge Benko - 18/06/2013

    
    Set Sinistros_Invalidos = Nothing
    'DEMANDA 15134427 - flavio.abreu - Utiliza��o de Group By/Inclus�o de with(nolock)/Substitui��o de Select * p/Select campo(sinistro_id)
    'SQL = "SELECT DISTINCT sinistro_bb "
    'SQL = SQL & " FROM sinistro_tb a (NOLOCK) INNER JOIN sinistro_bb_tb b (NOLOCK) "
    'SQL = SQL & "  ON a.sinistro_id = b.sinistro_id "
    'SQL = SQL & " AND a.apolice_id = b.apolice_id "
    'SQL = SQL & " AND a.seguradora_cod_susep = b.seguradora_cod_susep "
    'SQL = SQL & " AND a.sucursal_seguradora_id = b.sucursal_seguradora_id "
    'SQL = SQL & " AND a.ramo_id = b.ramo_id "
    'SQL = SQL & " WHERE a.lock_aviso = 's' and situacao <> 7"
    SQL = "SELECT sinistro_bb = b.sinistro_bb "
    SQL = SQL & "  FROM seguros_db.dbo.sinistro_tb a    with(nolock) "
    SQL = SQL & "  JOIN seguros_db.dbo.sinistro_bb_tb b with(nolock)"
    SQL = SQL & "    ON a.sinistro_id             = b.sinistro_id "
    SQL = SQL & "   AND a.apolice_id              = b.apolice_id  "
    SQL = SQL & "   AND a.seguradora_cod_susep    = b.seguradora_cod_susep  "
    SQL = SQL & "   AND a.sucursal_seguradora_id  = b.sucursal_seguradora_id "
    SQL = SQL & "   AND a.ramo_id                 = b.ramo_id "
    SQL = SQL & " WHERE a.lock_aviso = 's' "
    SQL = SQL & "   AND situacao <> 7"
    SQL = SQL & "GROUP BY b.sinistro_bb"
            
    'In�cio - Melhorias do SEGBR - Jorge Benko - 18/06/2013
    Set rs = rdocn.OpenResultset(SQL)
    'Set rs = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, SQL, True)
    'Fim - Melhorias do SEGBR - Jorge Benko - 18/06/2013
    
    While Not rs.EOF
        Sinistro_BB = rs("sinistro_bb")
        Sinistros_Invalidos.Add Sinistro_BB
        rs.MoveNext
    Wend
    rs.Close

End Sub

Private Sub Atualiza_Entrada(Entrada_GTR_ID As String)
    Dim SQL As String

    ' Grava o registro como 't' (transmitido)
    SQL = "set XACT_ABORT ON EXEC " & GTR_banco & ".dbo.altera_situacao_entrada_gtr_spu "
    SQL = SQL & Entrada_GTR_ID
    SQL = SQL & ", 'T'"
    SQL = SQL & ", '" & cUserName & "'"

    Call rdocn_Interface.OpenResultset(SQL)

End Sub

Private Sub Carrega_Registros(ByVal Entrada_GTR_ID As String, ByRef crLayout As clLayout)
    Dim Rs_GTR As rdoResultset

    Dim SQL As String, crRegistro As String
    'DEMANDA 15134427 - flavio.abreu - Inclus�o de with(nolock)/Substitui��o de Select * p/Select campo(sinistro_id)
    'SQL = "SELECT * FROM entrada_gtr_registro_tb (NOLOCK) "
    
    'In�cio - Melhorias do SEGBR - Jorge Benko - 18/06/2013
    'SQL = "SELECT registro FROM interface_db.dbo.entrada_gtr_registro_tb With(NOLOCK) " 'flavio.abreu - substitui��o do * pelo campo registro
    SQL = "SELECT registro FROM interface_db.dbo.entrada_gtr_registro_atual_tb With(NOLOCK) "
    SQL = SQL & " WHERE entrada_gtr_id = " & Entrada_GTR_ID

    Set Rs_GTR = rdocn_Interface.OpenResultset(SQL)
    'Set Rs_GTR = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, SQL, True)
    'Fim - Melhorias do SEGBR - Jorge Benko - 18/06/2013
    
    While Not Rs_GTR.EOF
        crRegistro = Rs_GTR("registro")
        crLayout.registros.Add crRegistro
        Rs_GTR.MoveNext
    Wend
    Rs_GTR.Close
    
End Sub

Private Sub Atualiza_FlexEntrada(Linha As Integer, Tipo As String)

    flexEntrada.Col = GRID_ENT_STATUS
    'flexEntrada.Row = Linha
    flexEntrada.Row = Linha
    flexEntrada.CellPictureAlignment = 3
    If Tipo = "TEMPO" Then
        Set flexEntrada.CellPicture = ImageList1.ListImages(1).Picture
    ElseIf Tipo = "OK" Then
        Set flexEntrada.CellPicture = ImageList1.ListImages(2).Picture
    ElseIf Tipo = "ERRO" Then
        Set flexEntrada.CellPicture = ImageList1.ListImages(3).Picture
    ElseIf Tipo = "BLOQUEADO" Then
        Set flexEntrada.CellPicture = ImageList1.ListImages(4).Picture
    End If
    DoEvents
    flexEntrada.Refresh

End Sub

Private Sub Grava_Log_GTR(ByVal AMensagem As String, ByVal OTipo As String, ByVal ARotina As String)
    
    Call GravaMensagem(AMensagem, OTipo, ARotina)
    
End Sub

Private Function Sinistro_Invalido(ByVal Sinistro_BB As String) As Boolean

    Dim i As Integer
    
    Sinistro_Invalido = False
    
    If Sinistros_Invalidos.Count > 0 Then
        For i = 1 To Sinistros_Invalidos.Count
            If Sinistros_Invalidos(i) = Sinistro_BB Then
                Sinistro_Invalido = True
                Exit For
            End If
        Next i
    End If

End Function

Private Function Aviso_LOG(ByVal OSinistro_BB As String) As Boolean
    Dim rs As rdoResultset
   
    Dim SQL As String
    
    On Error GoTo TrataErro
    'DEMANDA 15134427 - flavio.abreu - ' flavio.abreu - Substitui��o de count(*) --> count(1)
    SQL = "SELECT count(1) "
    SQL = SQL & " FROM WEB_SEGUROS_DB.dbo.robo_aviso_sinistro_LOG with(nolock) " 'flavio.abreu - inclus�o de with(nolock)
    SQL = SQL & " WHERE num_aviso = '" & OSinistro_BB & "'"
    Set rs = rdocn_Interface.OpenResultset(SQL)
    
    Aviso_LOG = rs(0) > 0
    rs.Close
    
    Exit Function
    
TrataErro:
    rdocn_Interface.RollbackTrans
    rdocn_InterfaceAB.RollbackTrans
    rdocn.RollbackTrans
    rdocn_SEG1.RollbackTrans
    
    String_tabelas_Temp = ""
    Call MensagemBatch("Rotina: Aviso_LOG. O programa ser� cancelado.", vbOKOnly, "Erro", True, Me)
    Call TerminaSEGBR
    
End Function

Private Function Monta_Tela_LOG(ByVal OSinistro_BB As String) As String
    Dim rs As rdoResultset
    
    Dim SQL As String
    
    On Error GoTo TrataErro
    
    SQL = "SELECT descr_linha"
    SQL = SQL & " FROM WEB_SEGUROS_DB.dbo.robo_tela_log with(nolock)"
    SQL = SQL & " WHERE num_aviso = '" & OSinistro_BB & "'"
    SQL = SQL & " ORDER BY linha_id"
    
'    Set rs = rdocn_Interface.OpenResultset(SQL)
    
    If glAmbiente_id = 2 Or glAmbiente_id = 3 Or glAmbiente_id = 5 Then
        Set rs = rdocn_Interface.OpenResultset(SQL)
    Else
        Set rs = rdocn_Seg2.OpenResultset(SQL)
    End If

    While Not rs.EOF
       Monta_Tela_LOG = Monta_Tela_LOG & rs!descr_linha & vbNewLine
       rs.MoveNext
    Wend
    rs.Close
    
    Exit Function
    
TrataErro:

    rdocn_Interface.RollbackTrans
'    rdocn_InterfaceAB.RollbackTrans
    rdocn.RollbackTrans
    rdocn_Seg2.RollbackTrans
'    rdocn_SEG1.RollbackTrans
    
    String_tabelas_Temp = ""
    Call MensagemBatch("Rotina: Monta_Tela_LOG. O programa ser� cancelado.", vbOKOnly, "Erro", True, Me)
    Call TerminaSEGBR
    
End Function

Private Sub Grava_Documento_Recebido_GTR(ByVal OCod_Remessa As String, ByVal OSinistro_BB As String, ByVal OEvento_BB As String)
    Dim SQL As String
    
    On Error GoTo Erro
    
    SQL = "EXEC documentos_recebidos_GTR_spi "
    SQL = SQL & "'" & OCod_Remessa & "'"    '@cod_remessa
    SQL = SQL & ", " & OSinistro_BB         '@sinistro_bb
    SQL = SQL & ", " & OEvento_BB           '@evento_BB
    SQL = SQL & ", '" & cUserName & "'"     '@usuario
    Call rdocn_Interface.Execute(SQL)

    Exit Sub
    
Erro:
    rdocn_Interface.RollbackTrans
    rdocn_InterfaceAB.RollbackTrans
    rdocn.RollbackTrans
    rdocn_SEG1.RollbackTrans
    
    String_tabelas_Temp = ""
    Call MensagemBatch("Rotina: Grava_Documento_Recebido_GTR. O programa ser� cancelado.", vbOKOnly, "Erro incluindo registro GTR", True, Me)
    Call TerminaSEGBR
    
End Sub

Private Function Verifica_LOG_Robo(ByVal Sinistro_BB As String) As Boolean

    Dim rs As rdoResultset

    Dim SQL As String
    
    On Error GoTo TrataErro
    
    Verifica_LOG_Robo = False
    
    'Procura no LOG
    'DEMANDA 15134427 - flavio.abreu - Substitui��o do "Select *" p/"Select campo(num_aviso)"
    'SQL = "SELECT * "
    SQL = "SELECT num_aviso "
    SQL = SQL & " FROM web_seguros_db.dbo.robo_aviso_sinistro_log with(nolock) " 'flavio.abreu - inclusao de nolock
    SQL = SQL & " WHERE num_aviso = '" & Sinistro_BB & "'"

    Set rs = rdocn.OpenResultset(SQL)

    If Not rs.EOF Then
       Verifica_LOG_Robo = True
       rs.Close
    Else
       rs.Close
       'Procura no ROB�
       'DEMANDA 15134427 - flavio.abreu - Substitui��o do "Select *" p/"Select campo(num_aviso)"
       'SQL = "SELECT * "
       SQL = "SELECT num_aviso " 'flavio.abreu - Substitui��o do "Select *" p/"Select campo(num_aviso)"
       SQL = SQL & " FROM web_seguros_db.dbo.SEGBR_sinistro_bb_tb With(NOLOCK) "
       SQL = SQL & " WHERE num_aviso = '" & Sinistro_BB & "'"

       Set rs = rdocn.OpenResultset(SQL)
      
       If Not rs.EOF Then
          Verifica_LOG_Robo = True
       Else
          rs.Close
          'Procura no SEGBR
          'DEMANDA 15134427 - flavio.abreu - Substitui��o do "Select *" p/"Select campo(num_aviso)"
          'SQL = "SELECT * "
          SQL = "SELECT sinistro_bb " 'flavio.abreu - Substitui��o do "Select *" p/"Select campo(sinistro_bb)"
          SQL = SQL & " FROM seguros_db.dbo.sinistro_bb_tb With(NOLOCK) "
          SQL = SQL & " WHERE sinistro_bb = " & Sinistro_BB
          
        Set rs = rdocn.OpenResultset(SQL)
          
          If Not rs.EOF Then
             Verifica_LOG_Robo = True
          End If
       End If
       rs.Close
    End If

    Exit Function

TrataErro:
    'Grava o Log do Erro
    'MsgBox "Descri��o: " & Err.Description, "N�mero: " & Err.Number
    Call Grava_Trace(Err.Number, Err.Description, "Verifica_Log_robo")
    Call TerminaSEGBR

End Function

Private Sub Atualiza_Situacao_Aviso(ByVal Sinistro_BB As String)
    Dim rs As rdoResultset
   
    Dim SQL As String
    
    On Error GoTo TrataErro
    'DEMANDA 15134427 - flavio.abreu - inclus�o with(nolock)
    
    'In�cio - Melhorias do SEGBR - Jorge Benko - 18/06/2013
    'SQL = "SELECT entrada_gtr_id FROM interface_db..entrada_gtr_tb with(nolock) "
    SQL = "SELECT entrada_gtr_id FROM interface_db.dbo.entrada_gtr_atual_tb with(nolock) "
    'Fim - Melhorias do SEGBR - Jorge Benko - 18/06/2013
        
    SQL = SQL & " WHERE sinistro_bb = " & Sinistro_BB
    SQL = SQL & "   AND evento_bb   = 2000"

    Set rs = rdocn.OpenResultset(SQL)
   
    If Not rs.EOF Then
        
        SQL = "EXEC interface_db.dbo.altera_situacao_entrada_gtr_spu "
        SQL = SQL & "'" & rs("entrada_gtr_id") & "'" ' @dt_evento
        SQL = SQL & ", 'N'"
        SQL = SQL & ", '" & cUserName & "'" ' @usuario
        rs.Close
        'Call rdocnAB.OpenResultset(SQL)
        
         If glAmbiente_id = 2 Or glAmbiente_id = 3 Or glAmbiente_id = 5 Then
            Call rdocn_Interface.OpenResultset(SQL)
            Call rdocn.OpenResultset(SQL)
        Else
            Call rdocn_Seg2.OpenResultset(SQL)
        End If
       
    Else
        rs.Close
    End If
       
    Exit Sub

TrataErro:
    'Grava o Log do Erro
    'MsgBox "Descri��o: " & Err.Description, "N�mero: " & Err.Number
    Call Grava_Trace(Err.Number, Err.Description, "Atualiza_Situacao_Aviso")
    Call TerminaSEGBR
    
End Sub

Public Sub Envia_Email_Tecnico(Sinistro_BB As String, ByVal Mod_rdocn As rdoConnection, Evento_BB As String)

    Dim rs As rdoResultset
    Dim Rs1 As rdoResultset
    Dim Rs2 As rdoResultset
    Dim rs3 As rdoResultset

    Dim SQL As String
    
    Dim Nome As String
    Dim destinatario As String
    Dim AMensagem As String
    Dim OAssunto As String
    
    'DEMANDA 15134427 - flavio.abreu - inclus�o with(nolock)
    SQL = ""
    SQL = " SELECT NOME, EMAIL,* "
    SQL = SQL & "  FROM seguros_db.dbo.sinistro_tecnico_tb ST with(nolock) " 'Inclusao de With(nolock) - flavio.abreu
    SQL = SQL & "  JOIN seguros_db.dbo.tecnico_tb           T with(nolock) " 'Inclusao de With(nolock) - flavio.abreu
    SQL = SQL & "    ON ST.tecnico_id = T.tecnico_id "
    SQL = SQL & "  JOIN seguros_db.dbo.sinistro_bb_tb S with(nolock) " 'Inclusao de With(nolock) - flavio.abreu
    SQL = SQL & "    ON ST.sinistro_id = S.sinistro_id"
    SQL = SQL & " WHERE S.sinistro_bb  = " & Sinistro_BB
    SQL = SQL & "   AND ST.dt_fim_vigencia IS NULL"
      
    Set rs = Mod_rdocn.OpenResultset(SQL)

    If Not rs.EOF Then
        destinatario = rs("email")
        Nome = rs("nome")
    Else

        'Procura primeiro no SEGBR
        'DEMANDA 15134427 - flavio.abreu - inclus�o with(nolock)
        SQL = ""
        SQL = " SELECT r.tp_ramo_id "
        SQL = SQL & "  FROM ramo_tb r with(nolock) "
        SQL = SQL & "  JOIN sinistro_bb_tb s with(nolock) "
        SQL = SQL & "    ON r.ramo_id = s.ramo_id"
        SQL = SQL & " Where Sinistro_BB = " & Sinistro_BB

        Set Rs1 = Mod_rdocn.OpenResultset(SQL)
        
        If Not Rs1.EOF Then
            Select Case Rs1("tp_ramo_id")
                Case "1"
                   destinatario = "GERSI"
                   Nome = "GERSI"
                Case "2"
                   destinatario = "GEORE"
                   Nome = "GEORE"
            End Select
        Else
        'Procura no LOG
        'DEMANDA 15134427 - flavio.abreu - inclus�o with(nolock)
            SQL = " SELECT DISTINCT tp_ramo_id"
            SQL = SQL & " FROM seguros_db.dbo.ramo_tb a with(nolock) " 'Inclusao de With(nolock) - flavio.abreu
            SQL = SQL & " JOIN seguros_db.dbo.item_produto_tb b with(nolock) " 'Inclusao de With(nolock) - flavio.abreu
            SQL = SQL & "   ON (b.ramo_id = a.ramo_id)"
            SQL = SQL & " WHERE b.cod_produto_bb = (select tipo_produto from WEB_SEGUROS_DB.dbo.ROBO_AVISO_SINISTRO_LOG R"
            SQL = SQL & " WHERE NUM_AVISO = " & "'" & Sinistro_BB & "')"

            Set Rs2 = Mod_rdocn.OpenResultset(SQL)
             
            If Not Rs2.EOF Then
                  Select Case Rs2("tp_ramo_id")
                    Case "1"
                       destinatario = "GERSI"
                       Nome = "GERSI"
                    Case "2"
                       destinatario = "GEORE"
                       Nome = "GEORE"
                  End Select
    
            Else 'Procura no ROB�
                 'DEMANDA 15134427 - flavio.abreu - inclus�o with(nolock)
                 SQL = " SELECT  R.tp_ramo_id FROM ramo_tb R with(nolock)" 'Inclusao de With(nolock) - flavio.abreu
                SQL = SQL & " INNER JOIN WEB_SEGUROS_DB.dbo.segbr_sinistro_bb_tb ROBO 'Inclusao de With(nolock) - flavio.abreu"
                SQL = SQL & " ON R.ramo_id = ROBO.ramo_id"
                SQL = SQL & " WHERE NUM_AVISO = " & "'" & Sinistro_BB & "'"

                Set rs3 = Mod_rdocn.OpenResultset(SQL)
                
                If Not rs3.EOF Then
                    Select Case rs3("tp_ramo_id")
                        Case "1"
                           destinatario = "gersi"
                           Nome = "GERSI"
                        Case "2"
                           destinatario = "geore"
                           Nome = "GEORE"
                    End Select
                    rs3.Close
                End If
            End If
            Rs2.Close
        End If
        Rs1.Close
    End If


'* Carrega o destinat�rio do eMail

    AMensagem = Nome & "," & vbCrLf & vbCrLf
    Select Case Evento_BB
       Case 2001
          OAssunto = "Evento de Aviso de Sinistro Alterado pelo BB recebido."
          AMensagem = AMensagem & "Evento de Aviso de Sinistro Alterado pelo BB recebido. " & vbCrLf & vbCrLf
          
       Case 2002
          OAssunto = "Evento de Inclus�o de Ocorr�ncia de Seguro Agr�cola recebido."
          AMensagem = AMensagem & "Evento de Inclus�o de Ocorr�ncia de Seguro Agr�cola recebido. " & vbCrLf & vbCrLf
       Case 2004
          OAssunto = "Evento de Altera��o de Ocorr�ncia de Seguro Agr�cola recebido."
          AMensagem = AMensagem & "Evento de Altera��o de Ocorr�ncia de Seguro Agr�cola recebido. " & vbCrLf & vbCrLf
       
       Case 2008
          OAssunto = "Evento de Inclus�o de Ocorr�ncia do C�dula Rural recebido."
          AMensagem = AMensagem & "Evento de Inclus�o de Ocorr�ncia do C�dula Rural recebido.  " & vbCrLf & vbCrLf
       
       Case 2009
          OAssunto = "Evento de Aviso Reativado pelo BB recebido."
          AMensagem = AMensagem & "Evento de Aviso Reativado pelo BB recebido.  " & vbCrLf & vbCrLf
          
       Case 2010
          OAssunto = "Evento de Cancelamento de Ocorr�ncia de Seguro Agr�cola recebido."
          AMensagem = AMensagem & "Evento de Cancelamento de Ocorr�ncia de Seguro Agr�cola recebido. " & vbCrLf & vbCrLf
          
       Case 2012
          OAssunto = "Evento de Altera��o de Ocorr�ncia do C�dula Rural recebido."
          AMensagem = AMensagem & "Evento de Altera��o de Ocorr�ncia do C�dula Rural recebido.  " & vbCrLf & vbCrLf
          
       Case 2013
          OAssunto = "Evento de Cancelamento de Ocorr�ncia do C�dula Rural recebido."
          AMensagem = AMensagem & "Evento de Cancelamento de Ocorr�ncia do C�dula Rural recebido.  " & vbCrLf & vbCrLf
          
    End Select
    AMensagem = AMensagem & "Sinistro_bb: " & Sinistro_BB
    AMensagem = AMensagem & "" & vbCrLf
    AMensagem = AMensagem & "------------------------------------------------------------------" & vbCrLf
    AMensagem = AMensagem & "Comunica��o Corporativa da Alian�a do Brasil"
    '* Envia e-Mail
    ctleMail.Enviar_Email cUserName, destinatario, OAssunto, AMensagem
    destinatario = ""
    OAssunto = ""
    AMensagem = ""
    
    rs.Close

End Sub

Private Function Sinistro_Tem_2000(ByVal Sinistro_BB As String) As Boolean

    Dim rs As rdoResultset
    
    Dim SQL As String

    Sinistro_Tem_2000 = False
    'DEMANDA 15134427 - flavio.abreu - inclus�o with(nolock)
    
    'In�cio - Melhorias do SEGBR - Jorge Benko - 18/06/2013
    'SQL = "SELECT entrada_gtr_id FROM interface_db.entrada_gtr_tb with(nolock)"
    SQL = "SELECT entrada_gtr_id FROM interface_db.dbo.entrada_gtr_atual_tb with(nolock)"
    'Fim - Melhorias do SEGBR - Jorge Benko - 18/06/2013
        
    SQL = SQL & " WHERE sinistro_bb = " & Sinistro_BB
    SQL = SQL & "   AND evento_bb   = 2000"
    
'    If (Sinistro_BB <> "0") And (Sinistro_BB <> "") Then
'
'        Set rs = rdocn.OpenResultset(SQL)
'
'
'        If Not rs.EOF Then
'            Sinistro_Tem_2000 = True
'        End If
'    End If
    
    If (Sinistro_BB <> "0") And (Sinistro_BB <> "") Then
    
        Set rs = rdocn.OpenResultset(SQL)

        If Not rs.EOF Then
           Sinistro_Tem_2000 = True
        End If
        
        rs.Close
        
        'dicaversan - para os casos que o BB manda o numero do sinistro id no lugar do sinistro bb
        '22/11/2010 - SQG
        If Sinistro_Tem_2000 = False Then
            'DEMANDA 15134427 - flavio.abreu - inclus�o with(nolock)
            
            'In�cio - Melhorias do SEGBR - Jorge Benko - 18/06/2013
            'SQL = "SELECT evento_id FROM seguros_db.dbo.evento_segbr_sinistro_tb with(nolock)"
            SQL = "SELECT evento_id FROM seguros_db.dbo.evento_segbr_sinistro_atual_tb with(nolock)"
            'Fim - Melhorias do SEGBR - Jorge Benko - 18/06/2013
            
            SQL = SQL & " WHERE sinistro_id  = " & Sinistro_BB
            SQL = SQL & "   AND evento_bb_id = 2000"
            'AJUSTAR PRA AB E ABS
            Set rs = rdocn.OpenResultset(SQL)
        
            If Not rs.EOF Then
                Sinistro_Tem_2000 = True
            End If
            rs.Close
        End If
    End If

    
End Function
    
Private Function Sinistro_Tem_2300(ByVal Sinistro_BB As String) As Boolean
    Dim rs As rdoResultset

    Dim SQL As String

    Sinistro_Tem_2300 = False
    'DEMANDA 15134427 - flavio.abreu - inclus�o with(nolock)
    SQL = ""
    
    'In�cio - Melhorias do SEGBR - Jorge Benko - 18/06/2013
    'SQL = SQL & "SELECT evento_id FROM seguros_db.dbo.evento_segbr_sinistro_tb with(nolock)"
     SQL = SQL & "SELECT evento_id FROM seguros_db.dbo.evento_segbr_sinistro_atual_tb with(nolock)"
     'Fim - Melhorias do SEGBR - Jorge Benko - 18/06/2013
        
    SQL = SQL & " WHERE sinistro_bb  = " & Sinistro_BB
    SQL = SQL & "   AND evento_bb_id = 2300"

' RSilva -23 / 11 / 2010
'    If (Sinistro_BB <> "0") And (Sinistro_BB <> "") Then
'        Set rs = rdocn.OpenResultset(SQL)
'        If Not rs.EOF Then
'
'            Sinistro_Tem_2300 = True
'        End If
'    End If
    
    If (Sinistro_BB <> "0") And (Sinistro_BB <> "") Then
        Set rs = rdocn.OpenResultset(SQL)
        
        If Not rs.EOF Then
           Sinistro_Tem_2300 = True
        End If
        rs.Close
        
        'dicaversan - para os casos que o BB manda o numero do sinistro id no lugar do sinistro bb
        '22/11/2010 - SQG
        If Sinistro_Tem_2300 = False Then
            'DEMANDA 15134427 - flavio.abreu - inclus�o with(nolock)
            
            'In�cio - Melhorias do SEGBR - Jorge Benko - 18/06/2013
            'SQL = "SELECT evento_id FROM seguros_db.dbo.evento_segbr_sinistro_tb with(nolock)"
            SQL = "SELECT evento_id FROM seguros_db.dbo.evento_segbr_sinistro_atual_tb with(nolock)"
            'Fim - Melhorias do SEGBR - Jorge Benko - 18/06/2013
                
            SQL = SQL & " WHERE sinistro_id  = " & Sinistro_BB
            SQL = SQL & "   AND evento_bb_id = 2300"
            
            Set rs = rdocn.OpenResultset(SQL)
           
            If Not rs.EOF Then
                Sinistro_Tem_2300 = True
            End If
            rs.Close
        End If
    End If

    
    
End Function
    
'dicaversan - sqg
Private Function Busca_sinistro_bb(ByVal Entrada_GTR_ID As String) As String

    Dim SQL As String
    Dim rs  As rdoResultset

'    SQL = "select distinct b.sinistro_bb from interface_db..entrada_gtr_registro_atual_tb a"
'    SQL = SQL & " inner join seguros_db..evento_segbr_sinistro_atual_tb b"
'    SQL = SQL & " on b.sinistro_id = substring(a.registro,55,11)"
'    SQL = SQL & " Where a.entrada_gtr_id = " & Entrada_GTR_ID
'    SQL = SQL & " and a.registro_id = 1"

    SQL = ""
    SQL = SQL & "SELECT sinistro_bb, evento_bb "
    SQL = SQL & "  FROM interface_db..entrada_gtr_tb entrada_gtr_tb With(nolock)"
    SQL = SQL & " WHERE entrada_gtr_tb.entrada_gtr_id = " & Entrada_GTR_ID


    Set rs = rdocn.OpenResultset(SQL)
    
    If Not rs.EOF Then
        If rs("evento_bb") = 2300 Then
            Busca_sinistro_bb = 0
        Else
            SQL = ""
            SQL = SQL & "SELECT Sinistro_bb" & vbNewLine
            SQL = SQL & "  FROM seguros_db.dbo.sinistro_bb_tb sinistro_bb_tb With(nolock)" & vbNewLine
            SQL = SQL & " WHERE sinistro_bb = " & rs("sinistro_bb") & vbNewLine
            SQL = SQL & "    OR sinistro_id = " & rs("sinistro_bb") & vbNewLine

            rs.Close

            Set rs = rdocn.OpenResultset(SQL)
            If Not rs.EOF Then
                Busca_sinistro_bb = IIf(IsNull(rs(0)), 0, rs(0))
            Else
                Busca_sinistro_bb = 0
            End If

        End If
    End If
        
End Function
'fim
Private Function Monta_Flex_Entrada_New() As Boolean

'**************************************************************************************************
'Data Altera��o: 18/06/2013
'Projeto       : Melhorias do SEGBR
'Autores       : Jorge Benko
'Descri��o     : Executar proc "SELECAO_ENTRADA_GTR_SPS_jj para carregar o Grid
'                Cria��o da tabela tempor�ria ##REGISTROS (Global) para carregar a classe clLayout
'                Mudan�a de conex�o adaptada a ExecutarSQL
'                Alterado a conex�o de Rdo para Ado utilizando a mesma variavel de record Rs
'**************************************************************************************************

    Call Grava_Trace(0, "In�cio - Monta_Flex_Entrada_New", "Monta_Flex_Entrada_New()")
    
    Dim SQL As String
    Dim Linha As String
    Dim vl_registros As String

    flexEntrada.Rows = 1
    On Error GoTo Erro
    
    Call Grava_Trace(0, "EXEC seguros_db..SMQS00178_SPS", "Monta_Flex_Entrada_New()")
    
    SQL = "EXEC seguros_db..SMQS00178_SPS"
    '#SQL = "EXEC desenv_db..SMQS00178_SPS"
    '#ALTERACAO CHAMADA
    If glAmbiente_id = 2 Or glAmbiente_id = 3 Or glAmbiente_id = 5 Then
        Set rs = rdocn_Interface.OpenResultset(SQL)
    Else
         Set rs = rdocn_Seg2.OpenResultset(SQL)
    End If
    
    If Not rs.EOF() Then
        While Not rs.EOF
            Linha = vbTab & rs("qtd_registros") & vbTab
            Linha = Linha & rs("entrada_gtr_id") & vbTab
            Linha = Linha & rs("evento_bb") & " - " & rs("descricao") & vbTab
            Linha = Linha & rs("sinistro_bb") & vbTab
            Linha = Linha & rs("cod_remessa") & vbTab
            Linha = Linha & rs("cod_transacao") & vbTab
'            Linha = Linha & rs("Evento_2301") & vbTab
            Linha = Linha & rs("Localizacao_Aviso") & vbTab
            Linha = Linha & rs("Layout_id") & vbTab
            Linha = Linha & rs("Sp_Tratamento") & vbTab
            Linha = Linha & rs("Cod_Empresa")
            
            flexEntrada.AddItem Linha
            rs.MoveNext
        Wend
       
    End If
       
    Set cl_Local_Layout = New clLayout

    Call Grava_Trace(0, "Montar a cole��o de Registros", "Monta_Flex_Entrada_New()")
    SQL = "SELECT * FROM ##entrada_registros"
    '#ALTERACAO CHAMADA
    If glAmbiente_id = 2 Or glAmbiente_id = 3 Or glAmbiente_id = 5 Then
        Set rs = rdocn_Interface.OpenResultset(SQL)
    Else
        Set rs = rdocn_Seg2.OpenResultset(SQL)
    End If
    
    'Set rs = rdocn_Interface.OpenResultset(SQL)

    While Not rs.EOF
        Set cl_Local_Layout = New clLayout
        cl_Local_Layout.Entrada_GTR = rs("Entrada_GTR_Id")
        vl_registros = rs("registro")
        cl_Local_Layout.registros.Add vl_registros
        co_Local_Layout.Add cl_Local_Layout
        rs.MoveNext
    Wend
    
    rs.Close
    
    If flexEntrada.Rows = 1 Then
        Monta_Flex_Entrada_New = False
    Else
        Monta_Flex_Entrada_New = True
    End If
    
    Call Grava_Trace(0, "Fim - Monta_Flex_Entrada_New", "Monta_Flex_Entrada_New()")
    Exit Function

Erro:
    Call Grava_Trace(Err.Number, Err.Description, "Monta_Flex_Entrada_New()")
    'MsgBox "Erro: " & Err.Number & " - " & Err.Description, , "SMQP0061 - GTR Exporta��o"
End Function
'In�cio - Melhorias do SEGBR - Jorge Benko - 18/06/2013
Private Sub Carrega_SEGBR_NEW(strAmbiente As String, ByRef glAmbiente_id As Integer)
'**************************************************************************************************
'Data Altera��o: 18/06/2013
'Projeto       : Melhorias do SEGBR
'Autores       : Jorge Benko
'Descri��o     : Receba da chamada Carrega_SEGBR() o Ambiente "AB" ou "ABS"
'                Enviar como parametro o GRID_ENT_SINISTRO_BB para tirar a valida��o da tabela evento_segbr_sinistro_atual_tb
'                pois somente carrega esta fun��o se o mesmo ja existir
'                Abrir a transa��o somente quando for executar a carga nas tabelas
'                Carregado da tabela ##chave_GTR_SN00 as variaveis vSsinistro_id, vSsinistro_bb, vEvento_bb
'                para n�o carregar mais na fun��o Executa_SP_Inclusao_SEGBR
'                Alterado a conex�o de Rdo para Ado utilizando a mesma variavel de record Rs
'**************************************************************************************************

    Call Grava_Trace(0, "In�cio - Carrega_SEGBR_NEW", "Carrega_SEGBR_NEW()")
    
    Dim Cont_remessas       As Long, Erro_Carga         As Long, i  As Integer
    Dim Qtd_Registros       As Integer, Entrada_GTR_ID  As String
    Dim Evento_BB           As String, Cont_Sucesso     As Integer
    Dim Cod_transacao       As String
    Dim iLayout             As New clLayout, ErroGTR    As clErro
    Dim Sinistro_BB         As String, OAssunto         As String
    Dim AMensagem           As String, AMensagem_Aux    As String
    Dim ALocalizacao_Aviso  As String, ODestinatario    As String
    Dim OCod_Remessa        As String
    Dim SQL                 As String
    Dim sinistro_bb_temp    As String

    On Error GoTo Erro
    
    'Define o Ambiente de acordo com o par�metro informado (2 = AB / 6 = ABS)
'    Select Case strAmbiente
'        Case "AB"
'            'glAmbiente_id = 2   'Alterado para teste em qualidade
'            glAmbiente_id = 3
'        Case "ABS"
'            'glAmbiente_id = 6
'            glAmbiente_id = 7
'    End Select
   
    String_tabelas_Temp = ""
    
    cmdSair.Enabled = False
    cmdCarregarAgora.Enabled = False
    cmdIniciar.Enabled = False
    picBotao(0).Enabled = False
    picBotao(1).Enabled = False
    
    Cont_remessas = 0
    StatusBar1.SimpleText = "Procurando novas remessas ..."
    Set Sinistros_Invalidos = Nothing

    If Not (Monta_Flex_Entrada_New) Then
        StatusBar1.SimpleText = "Nenhuma remessa encontrada !"
    Else
    
        Call Grava_Trace(0, "In�cio do Processamento - " & Now, "Carrega_SEGBR_NEW() - Ambiente_id:" & glAmbiente_id)

        Cont_Sucesso = 0
        For i = 1 To flexEntrada.Rows - 1
            
            COD_EMPRESA = ""
            ALocalizacao_Aviso = ""
            Erro_Carga = 0

            Set iLayout = New clLayout
            Set ErroGTR = New clErro
   
            ' Ajusta o grid na tela.
            If i > 6 Then
                flexEntrada.TopRow = i - 5
            End If

            Atualiza_FlexEntrada i, "TEMPO"
            Cont_remessas = Cont_remessas + 1
            StatusBar1.SimpleText = "Processando remessas recebidas (" & Cont_remessas & " de " & (flexEntrada.Rows - 1) & ") ..."

            Qtd_Registros = Val(flexEntrada.TextMatrix(i, GRID_ENT_QTD))
            Evento_BB = Left(flexEntrada.TextMatrix(i, GRID_ENT_EVENTO), 4)
            Cod_transacao = Left(flexEntrada.TextMatrix(i, GRID_COD_TRANSACAO), 4)
            iLayout.Layout_Id = Left(flexEntrada.TextMatrix(i, GRID_EVE_LAYOUT), 4)
            Entrada_GTR_ID = flexEntrada.TextMatrix(i, GRID_ENT_SEQ)
            iLayout.Entrada_GTR = Entrada_GTR_ID
            vSpTratamento = (flexEntrada.TextMatrix(i, GRID_SP_TRATAMENTO))
            Sinistro_BB = IIf(flexEntrada.TextMatrix(i, GRID_ENT_SINISTRO_BB) = "", "0", flexEntrada.TextMatrix(i, GRID_ENT_SINISTRO_BB))
            OCod_Remessa = flexEntrada.TextMatrix(i, GRID_ENT_COD_REMESSA)
            COD_EMPRESA = RTrim(flexEntrada.TextMatrix(i, GRID_COD_EMPRESA))
         
            Call Grava_Trace(1, "PROCESSANDO - Entrada_GTR_ID: " & Entrada_GTR_ID & " Evento_BB: " & Evento_BB, "Carrega_SEGBR_NEW()")
         
            If (flexEntrada.TextMatrix(i, GRID_SIN_CRIT_LOCAL) <> "") Then 'jorge benko - Encontrou a localiza��o

                If Cod_transacao = "SN00" Then
                    Call Grava_Trace(1, "Cod_transacao = 'SN00'", "Carrega_SEGBR_NEW()")
                    
                    'Monta o vetor com os registros da entrada_gtr_registro_tb
                    Call Carrega_Registros_NEW(Entrada_GTR_ID, iLayout)
                                                            
                    '#ALTERACAO CHAMADA
                    If glAmbiente_id = 2 Or glAmbiente_id = 3 Or glAmbiente_id = 5 Then
                         rdocn_Interface.BeginTrans
                         rdocn.BeginTrans
                    Else
                         rdocn_Seg2.BeginTrans
                    End If
                                                            
'                    rdocn_Interface.BeginTrans
'                    rdocn.BeginTrans
                    
                    
                    '#ALTERACAO CHAMADA
                    If glAmbiente_id = 2 Or glAmbiente_id = 3 Or glAmbiente_id = 5 Then
                         Erro_Carga = Carrega_Critica_Layout_NEW(1, rdocn, iLayout, ErroGTR, iLayout.Layout_Id)
                    Else
                         Erro_Carga = Carrega_Critica_Layout_NEW(1, rdocn_Seg2, iLayout, ErroGTR, iLayout.Layout_Id)
                    End If
                   
                End If
                                    
                If Erro_Carga > 0 Then
                    
                    'rdocn_Interface.RollbackTrans
                    'rdocn.RollbackTrans
                    
                    '#ALTERACAO CHAMADA
                    If glAmbiente_id = 2 Or glAmbiente_id = 3 Or glAmbiente_id = 5 Then
                        rdocn_Interface.RollbackTrans
                        rdocn.RollbackTrans
                    Else
                        rdocn_Seg2.RollbackTrans
                    End If
                    
                    Atualiza_FlexEntrada i, "ERRO"
                    
                    Call Grava_Trace(2, "ERRO - Carrega_Critica_Layout_NEW - Entrada_GTR_ID: " & Entrada_GTR_ID & " Evento_BB: " & Evento_BB, "Carrega_SEGBR_NEW()")
                                        
                    cmdIniciar.Caption = "&Iniciar            "
                    picBotao(0).Visible = True
                    picBotao(1).Visible = False
                    tmrConsultaEntrada.Enabled = False
                    String_tabelas_Temp = ""
                    GoTo Continue 'avan�a proxima linha do grid
                End If
                
                If Evento_BB = 2000 Then
                    SQL = "SELECT proposta_bb FROM ##D_GTR_SN00"
                    'Set rs = rdocn_Interface.OpenResultset(SQL)
                    
                    '#ALTERACAO CHAMADA
                    If glAmbiente_id = 2 Or glAmbiente_id = 3 Or glAmbiente_id = 5 Then
                        Set rs = rdocn_Interface.OpenResultset(SQL)
                    Else
                        Set rs = rdocn_Seg2.OpenResultset(SQL)
                    End If

                    If Not rs.EOF Then
                        rs.Close
                        Set rs = Nothing
                        GoTo processa
                    Else
                        'N�O FORAM CARREGADOS OS DADOS DO SINISTRO NO LOG
                        Atualiza_FlexEntrada i, "BLOQUEADO"
                        
                        rs.Close
                        Set rs = Nothing
                        
                        Call Grava_Trace(2, "BLOQUEADO - Evento 2000 n�o encontrou proposta_bb na ##D_GTR_SN00 - Entrada_GTR_ID: " & Entrada_GTR_ID & " Evento_BB: " & Evento_BB, "Carrega_SEGBR_NEW()")

                    End If
                        
                Else
processa:
                    If Cod_transacao = "SN00" Then
                        SQL = "select sinistro_bb, sinistro_id, evento_bb_id from ##chave_GTR_SN00"
                        'Set rs = rdocn_Interface.OpenResultset(SQL)
                        
                        '#ALTERACAO CHAMADA
                        If glAmbiente_id = 2 Or glAmbiente_id = 3 Or glAmbiente_id = 5 Then
                             Set rs = rdocn_Interface.OpenResultset(SQL)
                        Else
                             Set rs = rdocn_Seg2.OpenResultset(SQL)
                        End If

                        
                        If Not rs.EOF Then
                            sinistro_bb_temp = rs("sinistro_bb")
                            vSsinistro_id = rs("sinistro_id")
                            vSsinistro_bb = rs("sinistro_bb")
                            vEvento_bb = rs("evento_bb_id")
                            
                            rs.Close
                            Set rs = Nothing
                        End If
                        
                        If Val(Sinistro_BB) = Val(sinistro_bb_temp) Or Val(flexEntrada.TextMatrix(i, GRID_ENT_SINISTRO_BB)) = Val(sinistro_bb_temp) Then
                            If Len(flexEntrada.TextMatrix(i, GRID_COD_EMPRESA)) = 0 Then
                                If Localiza_Empresa_NEW(iLayout.Layout_Id, Sinistro_BB) = False Then
                                    Atualiza_FlexEntrada i, "BLOQUEADO"
                                    
                                    Call Grava_Trace(2, "BLOQUEADO - Localiza_Empresa_NEW - Entrada_GTR_ID: " & Entrada_GTR_ID & " Evento_BB: " & Evento_BB, "Carrega_SEGBR_NEW()")
                                    
                                    String_tabelas_Temp = ""
                                    GoTo Continue
                                                                    
                                End If
                            End If
                            
                            Call Grava_Trace(1, "Chama a Atualiza_SN", "Carrega_SEGBR_NEW()")
                            
                            If Atualiza_SN(iLayout.Layout_Id, Sinistro_BB, OCod_Remessa, Evento_BB, Entrada_GTR_ID) Then

                                If (Evento_BB = 2000) Or (Evento_BB = 2005 And (flexEntrada.TextMatrix(i, GRID_SIN_CRIT_LOCAL) = "LOG")) Then 'SE FOR AVISO OU CANCELAMENTO, VERIFICA A LOCALIZA��O
                                 
                                    'Verifica se o aviso foi gravado no LOG, se "SIM" ent�o envia e-MAIL pra CAVIS
                                    If flexEntrada.TextMatrix(i, GRID_SIN_CRIT_LOCAL) = "LOG" Then
                                     
                                        Call Grava_Trace(1, "Aviso foi gravado no LOG, se 'SIM' ent�o envia e-MAIL pra CAVIS", "Carrega_SEGBR_NEW()")

                                        If Evento_BB = 2000 Then
                                            OAssunto = "Aviso de Sinistro no LOG - N� " & Sinistro_BB
                                            AMensagem = "CAVIS, " & vbCrLf & vbCrLf
                                            AMensagem = AMensagem & "Aviso de Sinistro n�. " & Sinistro_BB & " presente no LOG." & vbCrLf
                                            AMensagem = AMensagem & "" & vbCrLf
                                            AMensagem = AMensagem & "------------------------------------------------------------------" & vbCrLf
                                            AMensagem = AMensagem & "Comunica��o Corporativa da Alian�a do Brasil"
                                        Else
                                            OAssunto = "Cancelamento do Aviso de Sinistro no LOG - N� " & Sinistro_BB
                                            AMensagem = "CAVIS, " & vbCrLf & vbCrLf
                                            AMensagem = AMensagem & "Cancelamento do Aviso de Sinistro n�. " & Sinistro_BB & " presente no LOG." & vbCrLf
                                            AMensagem = AMensagem & "" & vbCrLf
                                            AMensagem = AMensagem & "------------------------------------------------------------------" & vbCrLf
                                            AMensagem = AMensagem & "Comunica��o Corporativa da Alian�a do Brasil"
                                        End If

                                        ODestinatario = "CAVIS@aliancadobrasil.com.br"
                                        
                                        '* Carrega todos os dados da tela de LOG do rob�
                                        AMensagem_Aux = Monta_Tela_LOG(Sinistro_BB)
                                        AMensagem = AMensagem & vbNewLine & vbNewLine & vbNewLine & AMensagem_Aux
                                        
                                        SQL = "EXEC envia_email_sp "
                                        SQL = SQL & "'" & ODestinatario & "',"
                                        SQL = SQL & "'" & OAssunto & "',"
                                        SQL = SQL & "'" & Replace(AMensagem, "'", "�") & "'"
                                        
                                        Call rdocn_Interface.Execute(SQL)
                                        
'                                        '#ALTERACAO CHAMADA
'                                        If glAmbiente_id = 2 Or glAmbiente_id = 3 Or glAmbiente_id = 5 Then
'                                            Call rdocn_Interface.Execute(SQL)
'                                        Else
'                                            Call rdocn_Seg2.Execute(SQL)
'                                        End If
                                    End If
                                End If
                                
                                Atualiza_FlexEntrada i, "OK"
                                Cont_Sucesso = Cont_Sucesso + 1
                                
                            Else
                                'N�O FORAM CARREGADOS OS DADOS DO SINISTRO NO LOG
                                Atualiza_FlexEntrada i, "BLOQUEADO"
                                String_tabelas_Temp = ""
                            End If
                        Else
                            Atualiza_FlexEntrada i, "BLOQUEADO"
                            
                            Call Grava_Trace(2, "BLOQUEADO - N�o encontrou sinistro_bb, sinistro_id, evento_bb_id na ##chave_GTR_SN00 - Entrada_GTR_ID: " & Entrada_GTR_ID & " Evento_BB: " & Evento_BB, "Carrega_SEGBR_NEW()")
                            
                            String_tabelas_Temp = ""
                        End If '* Atualiza_SN
                    Else  'Cod_transacao = 'ST00'
                    
                        Call Grava_Trace(1, "Chama a Atualiza_ST", "Carrega_SEGBR_NEW()")
                        If Atualiza_ST(Sinistro_BB, OCod_Remessa, Evento_BB, Entrada_GTR_ID) Then
                        
                            Atualiza_FlexEntrada i, "OK"
                            Cont_Sucesso = Cont_Sucesso + 1
                        
                        Else
                            'N�O FORAM CARREGADOS OS DADOS DO SINISTRO NO LOG
                            Atualiza_FlexEntrada i, "BLOQUEADO"
                                                                
                            Call Grava_Trace(2, "BLOQUEADO - N�o atualizou COD_EMPRESA <> AB e ABS na Atualiza_ST - Entrada_GTR_ID: " & Entrada_GTR_ID & " Evento_BB: " & Evento_BB, "Carrega_SEGBR_NEW()")

                            String_tabelas_Temp = ""
                        End If
                    End If
                End If '* Evento_BB = 2000
            Else
                'In�cio - Melhorias do SEGBR - Jorge Benko - 18/06/2013
                If Evento_BB = 2003 And Cod_transacao = "SN00" Then
                    Call Grava_Trace(1, "Chama a Atualiza_Situacao_Aviso_NEW para atualizar a situa��o do Sinistro para 'N' se o evento = 2003 e transa��o='SN'", "Carrega_SEGBR_NEW()")
                    Atualiza_Situacao_Aviso_NEW Sinistro_BB
                End If
                'Fim - Melhorias do SEGBR - Jorge Benko - 18/06/2013
                
                Atualiza_FlexEntrada i, "BLOQUEADO"
                
                Call Grava_Trace(2, "BLOQUEADO - N�o retornou a localiza��o do Sinistro na proc do Grid - Entrada_GTR_ID: " & Entrada_GTR_ID & " Evento_BB: " & Evento_BB, "Carrega_SEGBR_NEW()")
                
            End If '* Critica_Localizacao
Continue:
       
        
'        If glAmbiente_id = 2 Or glAmbiente_id = 3 Or glAmbiente_id = 5 Then
 '            rdocn_Interface.CommitTrans
 '            rdocn.CommitTrans
 '       Else
  '          rdocn_Seg2.CommitTrans
  '      End If
        
        Call Grava_Trace(3, "PROCESSADO - Entrada_GTR_ID: " & Entrada_GTR_ID & " Evento_BB: " & Evento_BB, "Carrega_SEGBR_NEW()")
        
        Next i
        StatusBar1.SimpleText = "Total de " & Cont_Sucesso & " remessa(s) processada(s) com sucesso !"
        Call Grava_Trace(0, "Fim do Processamento - " & Now, "Carrega_SEGBR_NEW()")

    End If

    cmdSair.Enabled = True
    cmdCarregarAgora.Enabled = True
    cmdIniciar.Enabled = True
    picBotao(0).Enabled = True
    picBotao(1).Enabled = True
    
    If intStatusConexao = enStatusConexao.cxAberta Then
        If FecharConexao(numconexao) = True Then
            intStatusConexao = enStatusConexao.cxFechada
            Exit Sub
        End If
    End If
    
        If glAmbiente_id = 2 Or glAmbiente_id = 3 Or glAmbiente_id = 5 Then
             rdocn_Interface.CommitTrans
             rdocn.CommitTrans
        Else
            rdocn_Seg2.CommitTrans
        End If
    
    Call Grava_Trace(0, "Fim - Carrega_SEGBR_NEW", "Carrega_SEGBR_NEW()")
    Exit Sub
    Resume
    
Erro:

    String_tabelas_Temp = ""
    Call Enviar_Notificacao("SMQP0061", "SMQP0061 - Carrega_SEGBR_NEW" & vbCrLf & " Erro: " & Err.Number & " Descri��o: " & Err.Description)
    Call GravaMensagem("Rotina: Carrega_SEGBR." & vbCrLf & "Descri��o Erro: " & Err.Description & " .", "VB", "Carrega_SEGBR")
    
    Call Grava_Trace(Err.Number, Err.Description, "Carrega_SEGBR_NEW() - Entrada_GTR_ID: " & Entrada_GTR_ID)

    GoTo Continue
'Fim - Melhorias do SEGBR - Jorge Benko - 18/06/2013

End Sub

Private Function Executa_Entrada_Registro_NEW(Entrada_GTR_ID As String, Evento_BB As String) As Boolean
'**************************************************************************************************
'Data Altera��o: 18/06/2013
'Projeto       : Melhorias do SEGBR
'Autores       : Jorge Benko
'Descri��o     : Alterado o nome da tabela interface_db..entrada_GTR_tb para interface_db.dbo.entrada_GTR_atual_tb
'                Alterado o nome da tabela seguros_db.dbo.evento_segbr_sinistro_tb para seguros_db.dbo.evento_segbr_sinistro_atual_tb
'                Inserido o comanda With em todos os (NOLOCK)
'                Alterado a conex�o de Rdo para Ado utilizando a mesma variavel de record Rs e RS2
'                Alterado o select do Evento_BB = 8199 pois estava sendo repetido com as mesmas condi��es
'                Alterado os parametros (glAmbiente_id = 6 And COD_EMPRESA = "ABS") Or (glAmbiente_id = 2 And COD_EMPRESA = "AB")
'**************************************************************************************************

'Dim QrRDO As New rdoQuery

'In�cio - Melhorias do SEGBR - Jorge Benko - 18/06/2013
'Dim rs As rdoResultset 'jorge benko ficara com RDO ate defini��o da base em ADO
'Dim RS2 As rdoResultset
'Dim Rs As New ADODB.Recordset 'jorge benko ficara com RDO ate defini��o da base em ADO
'Dim RS2 As New ADODB.Recordset
'Fim - Melhorias do SEGBR - Jorge Benko - 18/06/2013

Dim SQL As String
Dim evento_gerado As String

'    AbrirTransacao (numconexao)
'    rdocn_Interface.BeginTrans
    
    On Error GoTo Erro
    
    Executa_Entrada_Registro_NEW = False
    
    'In�cio - Melhorias do SEGBR - Jorge Benko - 18/06/2013
    If Evento_BB = 8199 Then
        
        Executa_Entrada_Registro_NEW = False
        COD_EMPRESA = "XXX"
      
        SQL = " exec interface_db.dbo.segs9155_sps " & Entrada_GTR_ID
        
        Set Rs2 = rdocn_Interface.OpenResultset(SQL)
        
        If Rs2.EOF Then
            Rs2.Close
            Executa_Entrada_Registro_NEW = False
            COD_EMPRESA = "XXX"
            Exit Function
        Else
            COD_EMPRESA = Trim(Rs2(0))
            Rs2.Close
        End If
        
    ElseIf COD_EMPRESA = "" Then 'n�o voltou da proc do carrega grid
          
        SQL = " select distinct entrada_gtr_id "
        SQL = SQL & " from interface_db.dbo.entrada_gtr_atual_tb a With(nolock)"
        SQL = SQL & " inner join seguros_db.dbo.evento_segbr_sinistro_atual_tb b With(nolock)"
        SQL = SQL & " on a.sinistro_bb = b.sinistro_bb"
        SQL = SQL & " where entrada_gtr_id = " & Entrada_GTR_ID
    
        Set Rs2 = rdocn_Interface.OpenResultset(SQL)
    'Set RS2 = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, SQL, True)
    
        If Rs2.EOF Then
            Rs2.Close
            Executa_Entrada_Registro_NEW = False
            COD_EMPRESA = "XXX"
            Exit Function
        Else
            If glAmbiente_id = 6 Or glAmbiente_id = 7 Then
            '#If glAmbiente_id = 7 Then
                COD_EMPRESA = "ABS"
            ElseIf glAmbiente_id = 2 Or glAmbiente_id = 3 Then
            '#ElseIf glAmbiente_id = 3 Then
                COD_EMPRESA = "AB"
            End If
                
        End If
    'Fim - Melhorias do SEGBR - Jorge Benko - 18/06/2013
    End If
    If ((glAmbiente_id = 6 Or glAmbiente_id = 7) And COD_EMPRESA = "ABS") Or ((glAmbiente_id = 2 Or glAmbiente_id = 3) And COD_EMPRESA = "AB") Then
    '#If (glAmbiente_id = 7 And COD_EMPRESA = "ABS") Or (glAmbiente_id = 3 And COD_EMPRESA = "AB") Then
        SQL = ""
        SQL = " set xact_abort on exec " & GTR_banco & ".dbo.processa_entrada_registro_spi " & Entrada_GTR_ID ' JORGE RETORNAR POIS O BANCO QUE ESTA HJ � interface_db
        Set rs = rdocn_Interface.OpenResultset(SQL) 'jorge benko ficara com RDO ate defini��o da base em ADO

        If Evento_BB = 8199 And rs(0) = 0 Then
            Executa_Entrada_Registro_NEW = True
            
            'rdocn_Interface.CommitTrans
        Else
            evento_gerado = rs(0)
            'In�cio - Melhorias do SEGBR - Jorge Benko - 18/06/2013
            'SQL = "select evento_id from seguros_db.dbo.evento_segbr_sinistro_tb (nolock) where evento_id = " & evento_gerado
            SQL = "select evento_id from seguros_db.dbo.evento_segbr_sinistro_atual_tb With(nolock) where evento_id = " & evento_gerado
            'Fim - Melhorias do SEGBR - Jorge Benko - 18/06/2013

            rs.Close

            'In�cio - Melhorias do SEGBR - Jorge Benko - 18/06/2013
            Set rs = rdocn_Interface.OpenResultset(SQL) 'jorge benko ficara com RDO ate defini��o da base em ADO
            'Set Rs = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, SQL, True) 'jorge benko ficara com RDO ate defini��o da base em ADO
            'Fim - Melhorias do SEGBR - Jorge Benko - 18/06/2013

            If Not rs.EOF Then
                Executa_Entrada_Registro_NEW = True
                
                'rdocn_Interface.CommitTrans
            End If
        End If
        
        'Fim Jorge Benko alterado pois esta repetindo o select
  
    Else
        Executa_Entrada_Registro_NEW = False
        COD_EMPRESA = "XXX"
    End If

Exit Function
    
Erro:
    'MsgBox "Erro: " & Err.Number & " - " & Err.Description
    Call Grava_Trace(Err.Number, Err.Description, "Executa_Entrada_Registro_NEW")
    'rdocn_Interface.RollbackTrans
     Exit Function
End Function

Private Sub Grava_Documento_Recebido_GTR_NEW(ByVal OCod_Remessa As String, ByVal OSinistro_BB As String, ByVal OEvento_BB As String)

'**************************************************************************************************
'Data Altera��o: 18/06/2013
'Projeto       : Melhorias do SEGBR
'Autores       : Jorge Benko
'Descri��o     : Alterado a conex�o de Rdo para Ado utilizando a mesma variavel de record Rs
'**************************************************************************************************
    
    Dim SQL As String
    
   ' AbrirTransacao (numconexao)
    'rdocn_Interface.BeginTrans
    
    On Error GoTo Erro
    
    SQL = "EXEC documentos_recebidos_GTR_spi "
    SQL = SQL & "'" & OCod_Remessa & "'"    '@cod_remessa
    SQL = SQL & ", " & OSinistro_BB         '@sinistro_bb
    SQL = SQL & ", " & OEvento_BB           '@evento_BB
    SQL = SQL & ", '" & cUserName & "'"     '@usuario
         
    'In�cio - Melhorias do SEGBR - Jorge Benko - 18/06/2013
    'Call rdocn_Interface.Execute(SQL)
'''    Call ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, SQL, False)
    'Fim - Melhorias do SEGBR - Jorge Benko - 18/06/2013
    
    'rdocn_Interface.CommitTrans

    Exit Sub

    
Erro:
    'In�cio - Melhorias do SEGBR - Jorge Benko - 18/06/2013
    'rdocn_Interface.RollbackTrans
    'Fim - Melhorias do SEGBR - Jorge Benko - 18/06/2013
    
    String_tabelas_Temp = ""
    Call MensagemBatch("Rotina: Grava_Documento_Recebido_GTR. O programa ser� cancelado.", vbOKOnly, "Erro incluindo registro GTR", True, Me)
    Call TerminaSEGBR
    
End Sub

Private Sub Atualiza_Entrada_NEW(Entrada_GTR_ID As String)
'**************************************************************************************************
'Data Altera��o: 18/06/2013
'Projeto       : Melhorias do SEGBR
'Autores       : Jorge Benko
'Descri��o     : Alterado a conex�o de Rdo para Ado utilizando a mesma variavel de record Rs_GTR
'**************************************************************************************************

    Dim SQL As String

   ' AbrirTransacao (numconexao)
    'rdocn_Interface.BeginTrans
    
    On Error GoTo Erro
    
    ' Grava o registro como 't' (transmitido)
    SQL = "set XACT_ABORT ON EXEC " & GTR_banco & ".dbo.altera_situacao_entrada_gtr_spu "
    SQL = SQL & Entrada_GTR_ID
    SQL = SQL & ", 'T'"
    SQL = SQL & ", '" & cUserName & "'"
    
    'Call rdocn_Interface.Execute(SQL)
    
    'rdocn_Interface.CommitTrans
    Exit Sub
Erro:
    
    String_tabelas_Temp = ""
    'Grava o Log do Erro
    'rdocn_Interface.RollbackTrans
    Call Grava_Trace(Err.Number, Err.Description, "Atualiza_Entrada_NEW() - Entrada_GTR_ID: " & Entrada_GTR_ID)
    'MsgBox "Descri��o: " & Err.Description, "N�mero: " & Err.Number
    Call TerminaSEGBR

End Sub

Private Sub Atualiza_Situacao_Aviso_NEW(ByVal Sinistro_BB As String)
'**************************************************************************************************
'Data Altera��o: 18/06/2013
'Projeto       : Melhorias do SEGBR
'Autores       : Jorge Benko
'Descri��o     : Alterado o nome da tabela interface_db..entrada_GTR_tb para interface_db.dbo.entrada_GTR_atual_tb
'**************************************************************************************************
    
    Dim SQL As String
    
    rdocn_Interface.BeginTrans
    
    On Error GoTo Erro
    
    'In�cio - Melhorias do SEGBR - Jorge Benko - 18/06/2013
    'SQL = "SELECT entrada_gtr_id FROM interface_db..entrada_gtr_tb with(nolock) "
    SQL = "SELECT entrada_gtr_id FROM interface_db.dbo.entrada_gtr_atual_tb with(nolock) "
    'Fim - Melhorias do SEGBR - Jorge Benko - 18/06/2013
        
    SQL = SQL & " WHERE sinistro_bb = " & Sinistro_BB
    SQL = SQL & "   AND evento_bb   = 2000"

    Set rs = rdocn_Interface.OpenResultset(SQL)
'
'    '#ALTERACAO CHAMADA
'    If glAmbiente_id = 2 Or glAmbiente_id = 3 Or glAmbiente_id = 5 Then
'        Set rs = rdocn_Interface.OpenResultset(SQL)
'    Else
'        Set rs = rdocn_Seg2.OpenResultset(SQL)
'    End If
    
    If Not rs.EOF Then
        
        SQL = "EXEC interface_db.dbo.altera_situacao_entrada_gtr_spu "
        SQL = SQL & "'" & rs("entrada_gtr_id") & "'" ' @dt_evento
        SQL = SQL & ", 'N'"
        SQL = SQL & ", '" & cUserName & "'" ' @usuario
        rs.Close
        
        'Call rdocnAB.OpenResultset(SQL)
        
        '#ALTERACAO CHAMADA
        If glAmbiente_id = 2 Or glAmbiente_id = 3 Or glAmbiente_id = 5 Then
            Call rdocn_Interface.OpenResultset(SQL)
        Else
            Call rdocn_Seg2.OpenResultset(SQL)
        End If
        
        
        '#ALTERACAO CHAMADA
        If glAmbiente_id = 2 Or glAmbiente_id = 3 Or glAmbiente_id = 5 Then
            rdocn_Interface.CommitTrans
        Else
            rdocn_Seg2.CommitTrans
        End If
        
        'rdocn_Interface.CommitTrans
    Else
        '#ALTERACAO CHAMADA
        If glAmbiente_id = 2 Or glAmbiente_id = 3 Or glAmbiente_id = 5 Then
            rdocn_Interface.RollbackTrans
        Else
            rdocn_Seg2.RollbackTrans
        End If
        'rdocn_Interface.RollbackTrans
        rs.Close
    End If
           
    Exit Sub

Erro:
    'Grava o Log do Erro
    'rdocn_Interface.RollbackTrans
    '#ALTERACAO CHAMADA
     If glAmbiente_id = 2 Or glAmbiente_id = 3 Or glAmbiente_id = 5 Then
         rdocn_Interface.RollbackTrans
     Else
         rdocn_Seg2.RollbackTrans
     End If
    
    Call Grava_Trace(Err.Number, Err.Description, "Atualiza_Situacao_Aviso_NEW()")
    'MsgBox "Descri��o: " & Err.Description, "N�mero: " & Err.Number
    Call TerminaSEGBR
    
End Sub

Private Sub Carrega_Registros_NEW(ByVal Entrada_GTR_ID As String, ByRef crLayout As clLayout)
On Error GoTo Erro
'**************************************************************************************************
'Data Altera��o: 18/06/2013
'Projeto       : Melhorias do SEGBR
'Autores       : Jorge Benko
'Descri��o     : Carrega as variaveis da Classe de Registros
'**************************************************************************************************
    
    Dim crRegistro As String
 
    For Each cl_Local_Layout In co_Local_Layout
        If cl_Local_Layout.Entrada_GTR = Entrada_GTR_ID Then
            crRegistro = cl_Local_Layout.registros(1)
            crLayout.registros.Add crRegistro
        End If
    Next
        
    Exit Sub
    
Erro:
    Call Grava_Trace(Err.Number, Err.Description, "Carrega_Registros_NEW() - Entrada_GTR_ID: " & Entrada_GTR_ID)
End Sub
Private Function Executa_SP_Inclusao_SEGBR_NEW(ByVal Layout_Id As String, ByVal OSinistro_BB As String) As Boolean
    
'**************************************************************************************************
'Data Altera��o: 18/06/2013
'Projeto       : Melhorias do SEGBR
'Autores       : Jorge Benko
'Descri��o     : Fun��o divida da Executa_SP_Inclusa_SEGBR onde Localiza_Empresa_NEW validara a Empresa e a Fun��o Executa_SP_Inclusa_SEGBR_NEW
'                executara e inser��o nas tabelas
'                Alterado o nome da tabela seguros_db.dbo.evento_segbr_sinistro_tb para seguros_db.dbo.evento_segbr_sinistro_atual_tb
'                Inserido o comanda With em todos os (NOLOCK)
'                A sele��o do campo sp_tratamento da GTR_layout_tb pois o mesmo ja vira carregado na Cole��o de Eventos
'                Retirado a sele��o dos campos sinistro_id, sinistro_bb, evento_bb_id da tabela ##chave_GTR_SN00
'                pois os mesmo ja viram carregados antes da chamada da fun��o
'                Alterado a conex�o de Rdo para Ado utilizando a mesma variavel de record rs e Rs2
'
'**************************************************************************************************
    
    'In�cio - Melhorias do SEGBR - Jorge Benko - 18/06/2013
    'Dim rs as rdoResultset, RS2 As rdoResultset
    'Dim rs As New ADODB.Recordset
    'Dim RS2 As New ADODB.Recordset
    'Fim - Melhorias do SEGBR - Jorge Benko - 18/06/2013

    
    Dim SQL As String
    Dim SP As String
    Dim conta   As Integer
    Dim vEvento_original As Integer
    
    On Error GoTo Erro
    
    conta = 0
    
    Executa_SP_Inclusao_SEGBR_NEW = False

    SP = vSpTratamento 'Codigo do tramento do layout Sp_Tratamento

    SQL = ""
    
    If (glAmbiente_id = 6 Or glAmbiente_id = 7) And COD_EMPRESA = "ABS" Then  'Jorge voltar para 6 apos testes produ��o
    '#If glAmbiente_id = 7 And COD_EMPRESA = "ABS" Then 'Jorge voltar para 6 apos testes produ��o
   
        SQL = "set xact_abort on EXEC " & SP & " "
        SQL = SQL & "'" & Format(Data_Sistema, "yyyymmdd") & "'" ' @dt_evento
        SQL = SQL & ", " & 5 ' @entidade_id (5 = GTR)
        SQL = SQL & ", '" & cUserName & "'" ' @usuario
        Call rdocn_Interface.Execute(SQL)
        Executa_SP_Inclusao_SEGBR_NEW = True
    ElseIf (glAmbiente_id = 2 Or glAmbiente_id = 3) And COD_EMPRESA = "AB" Then
    '#ElseIf glAmbiente_id = 3 And COD_EMPRESA = "AB" Then
        If Val(vSsinistro_bb) = Val("0") Then
            GoTo Erro
        End If
    
        SQL = "EXEC " & SP & " "
        SQL = SQL & "'" & Format(Data_Sistema, "yyyymmdd") & "'" ' @dt_evento
        SQL = SQL & ", " & 5 ' @entidade_id (5 = GTR)
        SQL = SQL & ", '" & cUserName & "'" ' @usuario
        
        Call rdocn_Interface.Execute(SQL)
        Executa_SP_Inclusao_SEGBR_NEW = True
    Else
        Executa_SP_Inclusao_SEGBR_NEW = False
    End If
    Exit Function
    
Erro:

    Call Enviar_Notificacao("SMQP0061", "SMQP0061 - Executa_SP_Inclusao_SEGBR_NEW" & vbCrLf & " Erro: " & Err.Number & " Descri��o: " & Err.Description)
    Executa_SP_Inclusao_SEGBR_NEW = False
    Call MensagemBatch("Rotina: Executa_SP_Inclusao_SEGBR_NEW" & vbCrLf & "Sinistro_BB: " & OSinistro_BB & vbCrLf & "Stored Procedure: " & SP & vbCrLf & "Descri��o Erro: " & rdoErrors(1) & vbCrLf & " O programa ser� cancelado.", vbOKOnly, "Erro incluindo registro SEGBR", True, Me)
    'Call TerminaSEGBR
    'End If
    
End Function

Private Function Localiza_Empresa_NEW(ByVal Layout_Id As String, ByVal OSinistro_BB As String) As Boolean
    
'**************************************************************************************************
'Data Altera��o: 18/06/2013
'Projeto       : Melhorias do SEGBR
'Autores       : Jorge Benko
'Descri��o     : Localiza a Empresa se no retorno da Proc SELECAO_ENTRADA_GTR_SPS_jj do Monta_Flex_Entrada_New estiver vazia
'**************************************************************************************************
    
    'In�cio - Melhorias do SEGBR - Jorge Benko - 18/06/2013
    'Dim rs as rdoResultset, RS2 As rdoResultset
    'Dim rs As New ADODB.Recordset
    'Dim RS2 As New ADODB.Recordset
    'Fim - Melhorias do SEGBR - Jorge Benko - 18/06/2013
    
    Dim rs3 As New ADODB.Recordset

    
    Dim SQL As String
    Dim SP As String
    Dim conta   As Integer

    Dim vEvento_original As Integer
    
    On Error GoTo Erro
    
    conta = 0
    
    Localiza_Empresa_NEW = False

    SQL = ""
    If vEvento_bb = 2000 Then
    
        Call Grava_Trace(1, "Se o Evento for 2000 seleciona o Cod_Empresa na tabela ##D_GTR_SN00", "Localiza_Empresa_NEW()")

        'DEMANDA 15134427 - flavio.abreu - Inclusao de with(nolock)
        SQL = "select distinct r.COD_EMPRESA from ##D_GTR_SN00 c"
        SQL = SQL & " inner join seguros_db.dbo.proposta_adesao_tb pa with(nolock) on pa.PROPOSTA_BB = c.PROPOSTA_bb"
        'Francisco H. Berrocal - INC000003790574 - N�o processava eventos 2000
        'SQL = SQL & " inner join seguros_db..ramo_tb r on with(nolock) r.ramo_id = pa.ramo_id"
        SQL = SQL & " inner join seguros_db.dbo.ramo_tb r with(nolock) on r.ramo_id = pa.ramo_id"
        SQL = SQL & " Union All" 'flavio.abreu - inclusao de clausula All
        SQL = SQL & " select distinct r.COD_EMPRESA from ##D_GTR_SN00 c"
        SQL = SQL & " inner join seguros_db.dbo.proposta_fechada_tb pf with(nolock) on pf.PROPOSTA_BB = c.PROPOSTA_bb"
        SQL = SQL & " inner join seguros_db.dbo.proposta_tb p with(nolock) on pf.proposta_id = p.proposta_id"
        SQL = SQL & " inner join seguros_db.dbo.ramo_tb r with(nolock) on r.ramo_id = p.ramo_id"
        
        
    Else
        If vEvento_bb = 2299 Or vEvento_bb = 2003 Then
            
            If vEvento_bb <> 2003 Then
            
                Call Grava_Trace(1, "Se o Evento for <> 2003 seleciona o evento original na tabela ##T_GTR_SN00", "Localiza_Empresa_NEW()")

                SQL = "select evento_bb_original from ##T_GTR_SN00"
                
                'In�cio - Melhorias do SEGBR - Jorge Benko - 18/06/2013
                'Set Rs2 = rdocn_Interface.OpenResultset(SQL)
                
                '#ALTERACAO CHAMADA
                If glAmbiente_id = 2 Or glAmbiente_id = 3 Or glAmbiente_id = 5 Then
                    Set Rs2 = rdocn_Interface.OpenResultset(SQL)
                Else
                    Set Rs2 = rdocn_Seg2.OpenResultset(SQL)
                End If
'                Set Rs2 = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, SQL, True)
                'Fim - Melhorias do SEGBR - Jorge Benko - 18/06/2013
    
                vEvento_original = Rs2("evento_bb_original")
                Rs2.Close
                Set Rs2 = Nothing
            End If
            If vEvento_original = 1100 And vEvento_bb <> 2003 And Val(vSsinistro_bb) <> Val("0") Then
            
                Call Grava_Trace(1, "Se o Evento original for 1100 e o evento_bb for <> 2003 seleciona o COD_EMPRESA na tabela ##T_GTR_SN00", "Localiza_Empresa_NEW()")
            
                'DEMANDA 15134427 - flavio.abreu - Inclusao de with(nolock)
                SQL = "select distinct r.COD_EMPRESA from ##T_GTR_SN00 c "
                SQL = SQL & " inner join seguros_db.dbo.evento_segbr_sinistro_atual_tb s with(nolock) on"
                SQL = SQL & " (s.sinistro_id = " & vSsinistro_id & ") "
                SQL = SQL & " and num_remessa = c.num_remessa_original"
                SQL = SQL & " inner join seguros_db.dbo.ramo_tb r with(nolock) on r.ramo_id = s.ramo_id "
            ElseIf Val(vSsinistro_bb) <> Val("0") Then
            
                Call Grava_Trace(1, "Se o Sinistro original for <> '' seleciona o COD_EMPRESA na tabela ##chave_GTR_SN00", "Localiza_Empresa_NEW()")
            
                SQL = "select distinct r.COD_EMPRESA from ##chave_GTR_SN00 c "
                SQL = SQL & " inner join seguros_db.dbo.sinistro_bb_tb s with(nolock) on"
                SQL = SQL & " (s.sinistro_bb = " & vSsinistro_bb & ") "
                SQL = SQL & " inner join seguros_db.dbo.sinistro_tb si with(nolock) on si.sinistro_id = s.sinistro_id"
                SQL = SQL & " and situacao <> 7"
                SQL = SQL & " inner join seguros_db.dbo.ramo_tb r with(nolock) on r.ramo_id = s.ramo_id "
            ElseIf vEvento_original = 1300 Then
            
                Call Grava_Trace(1, "Se o Evento original for 1300 atualiza sinistro_bb = null para os sinistro_bb = 0 na tabela ##chave_GTR_SN00", "Localiza_Empresa_NEW()")

                SQL = "update ##chave_GTR_SN00 set sinistro_bb = null where sinistro_bb = 0"
                
                'In�cio - Melhorias do SEGBR - Jorge Benko - 18/06/2013
                'Call rdocn_Interface.Execute(SQL)
                
                '#ALTERACAO CHAMADA
                If glAmbiente_id = 2 Or glAmbiente_id = 3 Or glAmbiente_id = 5 Then
                    Call rdocn_Interface.Execute(SQL)
                Else
                    Call rdocn_Seg2.Execute(SQL)
                End If
                'Call ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, SQL, False)
                'Fim - Melhorias do SEGBR - Jorge Benko - 18/06/2013
    
                Call Grava_Trace(1, "Se o Evento original for 1300 atualiza sinistro_id = null para os sinistro_id = 0 na tabela ##chave_GTR_SN00", "Localiza_Empresa_NEW()")

                SQL = "update ##chave_GTR_SN00 set sinistro_id = null where sinistro_id = 0"
                
                'In�cio - Melhorias do SEGBR - Jorge Benko - 18/06/2013
                'Call rdocn_Interface.Execute(SQL)
                
                '#ALTERACAO CHAMADA
                If glAmbiente_id = 2 Or glAmbiente_id = 3 Or glAmbiente_id = 5 Then
                    Call rdocn_Interface.Execute(SQL)
                Else
                    Call rdocn_Seg2.Execute(SQL)
                End If
                'Call ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, SQL, False)
                'Fim - Melhorias do SEGBR - Jorge Benko - 18/06/2013
                
                SQL = "SELECT 'ABS' cod_empresa  union select 'AB' cod_empresa "
            ElseIf Val(vSsinistro_bb) = Val("0") Then
            
                Call Grava_Trace(1, "Se o Sinistro original for <> '' seleciona o COD_EMPRESA na tabela ##chave_GTR_SN00", "Localiza_Empresa_NEW()")
            
                SQL = "select distinct r.COD_EMPRESA from ##chave_GTR_SN00 c "
                SQL = SQL & " inner join seguros_db.dbo.sinistro_bb_tb s with(nolock) on"
                SQL = SQL & " (s.sinistro_bb = " & vSsinistro_bb & ") "
                SQL = SQL & " inner join seguros_db.dbo.sinistro_tb si with(nolock) on si.sinistro_id = s.sinistro_id"
                SQL = SQL & " and situacao <> 7"
                SQL = SQL & " inner join seguros_db.dbo.ramo_tb r with(nolock) on r.ramo_id = s.ramo_id "
            End If
        
        'MFrasca - 27/12/2010 - Flow 540443 - SQG - Ajuste para identificar empresa para eventos do SQG, pois estava apresentando erro
        'para os eventos que n�o possuiam no tabel�o o evento de aviso (1100 / 2000) e por isso identificava a empresa errada
        'no join com o tabel�o. Para esse produto o aviso � criado automaticamente quando recebemos o 2300.
        ElseIf vEvento_bb = 2300 Or vEvento_bb = 2301 Or vEvento_bb = 2302 Or vEvento_bb = 2303 Or vEvento_bb = 2304 Then
            
            Call Grava_Trace(1, "Se o Evento original for 2300,2301,2302,2303,2304 seleciona o COD_EMPRESA  = 'ABS'", "Localiza_Empresa_NEW()")

            SQL = "select cod_empresa = 'ABS'"
        '-----------------------------------------------
        Else
                'DEMANDA 15134427 - flavio.abreu - Inclusao de with(nolock)
            If (vEvento_bb = 2011 Or vEvento_bb = 2014 Or vEvento_bb = 2015 Or vEvento_bb = 2005 Or vEvento_bb = 2007) And Val(vSsinistro_bb) <> Val("0") Then
            
                Call Grava_Trace(1, "Se o Evento original for 2011,2014,2015,2005,2007 seleciona o COD_EMPRESA da tabela ##chave_GTR_SN00 quando o evento_bb_id = 1100 ou evento_bb_id = 2000 da tabela seguros_db.dbo.evento_segbr_sinistro_atual_tb", "Localiza_Empresa_NEW()")
            
                SQL = "select distinct r.COD_EMPRESA from ##chave_GTR_SN00 c "
                SQL = SQL & " inner join seguros_db.dbo.evento_segbr_sinistro_atual_tb s with(nolock) on"
                SQL = SQL & " (s.sinistro_bb = " & vSsinistro_bb & ") "
                SQL = SQL & " and (s.evento_bb_id = 1100 or s.evento_bb_id = 2000) "
                SQL = SQL & " inner join seguros_db.dbo.ramo_tb r with(nolock) on r.ramo_id = s.ramo_id "
            
            ElseIf Val(vSsinistro_bb) <> Val("0") Then
                'DEMANDA 15134427 - flavio.abreu - Inclusao de with(nolock)
                
                Call Grava_Trace(1, "Se o Sinistro original for <> '' seleciona o COD_EMPRESA da tabela ##chave_GTR_SN00 quando o evento_bb_id = 1100 ou evento_bb_id = 2000 da tabela seguros_db.dbo.evento_segbr_sinistro_atual_tb", "Localiza_Empresa_NEW()")
                
                SQL = "select distinct isnull(cod_empresa, 'XXX') COD_EMPRESA from seguros_db.dbo.ramo_tb where ramo_id in("
                SQL = SQL & " select distinct isnull(s.ramo_id,0) from ##chave_GTR_SN00 c "
                SQL = SQL & " inner join seguros_db.dbo.evento_segbr_sinistro_atual_tb s with(nolock) on"
                SQL = SQL & " (s.sinistro_bb = " & vSsinistro_bb & ") "
                SQL = SQL & " and (s.evento_bb_id = 1100 or s.evento_bb_id = 2000) )"
                'SQL = SQL & " inner join seguros_db..ramo_tb r on r.ramo_id = s.ramo_id "
            End If
        End If
    End If
    
    If SQL <> "" Then
   
       'In�cio - Melhorias do SEGBR - Jorge Benko - 18/06/2013
       'Set RS2 = rdocn.OpenResultset(SQL)
       'Set Rs2 = rdocn_Interface.OpenResultset(SQL)
       
       '#ALTERACAO CHAMADA
        If glAmbiente_id = 2 Or glAmbiente_id = 3 Or glAmbiente_id = 5 Then
            Set Rs2 = rdocn_Interface.OpenResultset(SQL)
        Else
            Set Rs2 = rdocn_Seg2.OpenResultset(SQL)
        End If
       'Set RS2 = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, SQL, True)
       'Fim - Melhorias do SEGBR - Jorge Benko - 18/06/2013
   
       If Rs2.EOF Then
           COD_EMPRESA = "XXX"
           
       Else
           If Rs2(0) <> 0 Then
           
               COD_EMPRESA = Trim(Rs2("COD_EMPRESA"))
               Rs2.MoveNext
               
               If Not Rs2.EOF Then
                   COD_EMPRESA = "XXX"
                   
                   Rs2.Close
                   SQL = "select cod_produto_bb from ##chave_GTR_SN00 "
               
                   'In�cio - Melhorias do SEGBR - Jorge Benko - 18/06/2013
                   'Set Rs2 = rdocn_Interface.OpenResultset(SQL)
                   
                    '#ALTERACAO CHAMADA
                    If glAmbiente_id = 2 Or glAmbiente_id = 3 Or glAmbiente_id = 5 Then
                        Set Rs2 = rdocn_Interface.OpenResultset(SQL)
                    Else
                        Set Rs2 = rdocn_Seg2.OpenResultset(SQL)
                    End If
                   'Set RS2 = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, SQL, True)
                   'Fim - Melhorias do SEGBR - Jorge Benko - 18/06/2013
                   
                   If Rs2("cod_produto_bb") = 100006 Then
                   
                        Call Grava_Trace(1, "Se o cod_produto_bb da tabela ##chave_GTR_SN00 for 100006 ent�o COD_EMPRESA = 'AB'", "Localiza_Empresa_NEW()")
                   
                        COD_EMPRESA = "AB"
                   End If
                   
                   If (Rs2("cod_produto_bb") = 100005) Or (Rs2("cod_produto_bb") = 8229156) Then
                   
                        Call Grava_Trace(1, "Se o cod_produto_bb da tabela ##chave_GTR_SN00 for 100005 ou 8229156 ent�o COD_EMPRESA = 'ABS'", "Localiza_Empresa_NEW()")

                        COD_EMPRESA = "ABS"
                   End If
                   
                   If COD_EMPRESA = "XXX" And Val(vSsinistro_bb) <> Val("0") Then
                       Rs2.Close
                       
                       'In�cio - Melhorias do SEGBR - Jorge Benko - 18/06/2013
                       'SQL = "select evento_id, sinistro_id, cod_produto_bb from seguros_db.dbo.evento_segbr_sinistro_tb with(nolock) where dt_inclusao >= '20100904' and sinistro_bb = " & vSsinistro_bb
                        SQL = "select evento_id, sinistro_id, cod_produto_bb from seguros_db.dbo.evento_segbr_sinistro_atual_tb with(nolock) where dt_inclusao >= '20100904' and sinistro_bb = " & vSsinistro_bb
    
                       'Set Rs2 = rdocn_Interface.OpenResultset(SQL)
                       
                        '#ALTERACAO CHAMADA
                        If glAmbiente_id = 2 Or glAmbiente_id = 3 Or glAmbiente_id = 5 Then
                            Set Rs2 = rdocn_Interface.OpenResultset(SQL)
                        Else
                            Set Rs2 = rdocn_Seg2.OpenResultset(SQL)
                        End If
                        'Set RS2 = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, SQL, True)
                       'Fim - Melhorias do SEGBR - Jorge Benko - 18/06/2013
    
                       If Rs2.EOF Then
                           COD_EMPRESA = "XXX"
                       Else
                        If glAmbiente_id = 6 Or glAmbiente_id = 7 Then
                        '#If glAmbiente_id = 7 Then
                               COD_EMPRESA = "ABS"
                           Else
                               COD_EMPRESA = "AB"
                           End If
                       End If
                                    
                       Rs2.Close
                       
                       Call Grava_Trace(1, "Se a dt_inclusao >= '20100904' do sinistro na tabela evento_segbr_sinistro_atual_tb e ambiente = 7 COD_EMPRESA = 'ABS' caso contrario 'AB'", "Localiza_Empresa_NEW()")
                   
                   End If
                   
               End If
           Else
               COD_EMPRESA = "AB"
           End If
    
       End If
    Else
        COD_EMPRESA = "XXX"

    End If
    
    If COD_EMPRESA = "XXX" And Val(vSsinistro_bb) <> Val("0") Then
        'In�cio - Melhorias do SEGBR - Jorge Benko - 18/06/2013
        'SQL = "select evento_id, sinistro_id, cod_produto_bb from seguros_db.dbo.evento_segbr_sinistro_tb with(nolock) where dt_inclusao >= '20100904' and sinistro_bb = " & vSsinistro_bb
        SQL = "select evento_id, sinistro_id, cod_produto_bb from seguros_db.dbo.evento_segbr_sinistro_atual_tb with(nolock) where dt_inclusao >= '20100904' and sinistro_bb = " & vSsinistro_bb
        
'        Set Rs2 = rdocn_Interface.OpenResultset(SQL)
        '#ALTERACAO CHAMADA
        If glAmbiente_id = 2 Or glAmbiente_id = 3 Or glAmbiente_id = 5 Then
            Set Rs2 = rdocn_Interface.OpenResultset(SQL)
        Else
            Set Rs2 = rdocn_Seg2.OpenResultset(SQL)
        End If
        'Set RS2 = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, SQL, True)
        'Fim - Melhorias do SEGBR - Jorge Benko - 18/06/2013
        
        If Rs2.EOF Then
            COD_EMPRESA = "XXX"
        Else
            
            If Rs2("cod_produto_bb") = 100006 Then
                COD_EMPRESA = "AB"
            ElseIf (Rs2("cod_produto_bb") = 100005) Or (Rs2("cod_produto_bb") = 8229156) Then
                COD_EMPRESA = "ABS"
            
            ElseIf Not IsNull(Rs2("sinistro_id")) Then
                SQL = "select sinistro_id from sinistro_tb with(nolock) where situacao <> 7 and sinistro_id = " & Rs2("sinistro_id")
                Rs2.Close
                
                'In�cio - Melhorias do SEGBR - Jorge Benko - 18/06/2013
'                Set Rs2 = rdocn_Interface.OpenResultset(SQL)
                '#ALTERACAO CHAMADA
                If glAmbiente_id = 2 Or glAmbiente_id = 3 Or glAmbiente_id = 5 Then
                    Set Rs2 = rdocn_Interface.OpenResultset(SQL)
                Else
                    Set Rs2 = rdocn_Seg2.OpenResultset(SQL)
                End If
                'Set RS2 = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, SQL, True)
                'Fim - Melhorias do SEGBR - Jorge Benko - 18/06/2013

                If Not Rs2.EOF Then
                    If glAmbiente_id = 6 Or glAmbiente_id = 7 Then
                    '#If glAmbiente_id = 7 Then
                        COD_EMPRESA = "ABS"
                    Else
                        COD_EMPRESA = "AB"
                    End If
                End If
            ElseIf glAmbiente_id = 6 Or glAmbiente_id = 7 Then
            '#ElseIf glAmbiente_id = 7 Then
                    COD_EMPRESA = "ABS"
            ElseIf glAmbiente_id = 2 Or glAmbiente_id = 3 Then
            '#ElseIf glAmbiente_id = 3 Then
                    COD_EMPRESA = "AB"
            End If
        End If
        Rs2.Close
    End If
    
    If COD_EMPRESA = "XXX" Then
    
      SQL = "select cod_produto_bb from ##chave_GTR_SN00 "
                
        'In�cio - Melhorias do SEGBR - Jorge Benko - 18/06/2013
'        Set Rs2 = rdocn_Interface.OpenResultset(SQL)
        '#ALTERACAO CHAMADA
        If glAmbiente_id = 2 Or glAmbiente_id = 3 Or glAmbiente_id = 5 Then
            Set Rs2 = rdocn_Interface.OpenResultset(SQL)
        Else
            Set Rs2 = rdocn_Seg2.OpenResultset(SQL)
        End If
        'Set RS2 = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, SQL, True)
        'Fim - Melhorias do SEGBR - Jorge Benko - 18/06/2013
      
      If Rs2("cod_produto_bb") = 100006 Then
            COD_EMPRESA = "AB"
      End If
      If (Rs2("cod_produto_bb") = 100005) Or (Rs2("cod_produto_bb") = 8229156) Then
            COD_EMPRESA = "ABS"
      End If
      Rs2.Close
    End If
    
    If COD_EMPRESA = "XXX" Then
        Localiza_Empresa_NEW = False
    Else
        Localiza_Empresa_NEW = True
    End If

    Exit Function
    
Erro:
        '#ALTERACAO CHAMADA
        If glAmbiente_id = 2 Or glAmbiente_id = 3 Or glAmbiente_id = 5 Then
             rdocn_Interface.RollbackTrans
             rdocn.RollbackTrans
        Else
            rdocn_Seg2.RollbackTrans
        End If
    
    
    Localiza_Empresa_NEW = False
    
    Call Grava_Trace(Err.Number, Err.Description, "Localiza_Empresa_NEW()")

End Function
Public Function Carrega_Tipos_Detalhe_NEW() As Long
    
    Dim SQL As String
    Dim Chave_Primaria_Pai, Chave_Primaria As String
    Dim vl_Layout_id_Old, vl_Detalhe_id_Old As String
        
    On Error GoTo Erro
    
    Set TTLayout.Tipos_Detalhe = Nothing
    Set Detalhe_filho1 = New clDetalhe
        
    Chave_Primaria_Pai = ""
        
    ' Carrega os tipos dispon�veis.
    SQL = "SELECT a.detalhe_id, a.dependencia_detalhe_id, a.Layout_Id, "
    SQL = SQL & " a.nome_tabela_temporaria , tamanho = SUM(tamanho), qtde_layout = count(a.layout_id), "
    
    ' Header
    SQL = SQL & " Case WHEN "
    SQL = SQL & " (SELECT COUNT(1) FROM interface_db.dbo.GTR_layout_detalhe_tb c With(NOLOCK) "
    SQL = SQL & " Where C.Detalhe_id = 0 "
    SQL = SQL & " AND c.layout_id = a.layout_id) = 0 "
    SQL = SQL & " THEN 'N' ELSE 'S' END HEADER, "

    ' Trailler
    SQL = SQL & " Case WHEN "
    SQL = SQL & " (SELECT COUNT(1) FROM interface_db.dbo.GTR_layout_detalhe_tb d With(NOLOCK) "
    SQL = SQL & " Where d.Detalhe_id = 9999 "
    SQL = SQL & " AND d.layout_id = a.layout_id) = 0 "
    SQL = SQL & " THEN 'N' ELSE 'S' END TRAILLER "
    
    SQL = SQL & " FROM interface_db.dbo.GTR_layout_detalhe_tb a WITH (NOLOCK) "
    SQL = SQL & "   INNER JOIN interface_db.dbo.GTR_layout_campo_tb b WITH (NOLOCK) "
    SQL = SQL & "       ON a.layout_id = b.layout_id "
    SQL = SQL & "       AND a.detalhe_id = b.detalhe_id "
    SQL = SQL & " group by a.detalhe_id, a.dependencia_detalhe_id, a.Layout_Id, "
    SQL = SQL & " a.Nome_Tabela_Temporaria "
    SQL = SQL & " ORDER BY a.Layout_Id,a.detalhe_id "
    
    Set rs = rdocn_Interface.OpenResultset(SQL)
   
    While Not rs.EOF

        Set Detalhe = New clDetalhe
        Set TTLayout = New clLayout
        
        Detalhe.Dependencia_Detalhe_id = "" & rs("dependencia_detalhe_id")
        Detalhe.Detalhe_id = rs("detalhe_id")
        Detalhe.Tamanho = rs("tamanho")
        
        Detalhe.Layout_Id = rs("Layout_Id")
        
        If IsNull(rs("nome_tabela_temporaria")) Then
            Detalhe.Nome_Tabela_Temporaria = ""
        Else
            Detalhe.Nome_Tabela_Temporaria = "##" & Trim(rs("nome_tabela_temporaria"))
            
            If rs("detalhe_id") = "1" Then
                Chave_Primaria_Pai = "chave_" & Format(Val(rs("detalhe_id")), "0000") & " int "
            Else
                If rs("dependencia_detalhe_id") <> vl_Detalhe_id_Old Then
                    Chave_Primaria = Chave_Primaria_Pai & ", chave_" & Format(Val(rs("detalhe_id")), "0000") & " int "
                Else
                    If vl_Detalhe_id_Old = "1" Then
                       Chave_Primaria = Chave_Primaria_Pai & ", chave_" & Format(Val(rs("detalhe_id")), "0000") & " int "
                    Else
                        Chave_Primaria = Chave_Primaria & ", chave_" & Format(Val(rs("detalhe_id")), "0000") & " int "
                    End If
                End If
                
            End If
           
        End If

        TTLayout.Layout_Id = rs("Layout_Id")
        TTLayout.Tem_Header = IIf(rs("HEADER") = "S", True, False)
        TTLayout.Tem_Trailler = IIf(rs("TRAILLER") = "S", True, False)

        vl_Layout_id_Old = rs("Layout_Id")
        vl_Detalhe_id_Old = rs("detalhe_id")
        
        Set Detalhe_filho1 = New clDetalhe
        Detalhe_filho1.Nome_Tabela_Temporaria = Detalhe.Nome_Tabela_Temporaria
        Detalhe_filho1.Layout_Id = rs("Layout_Id")
        Detalhe_filho1.Detalhe_id = Detalhe.Detalhe_id
        
        If rs("detalhe_id") = "1" Then
            Detalhe_filho1.Chave_Primaria = Chave_Primaria_Pai
            Detalhe.Chave_Primaria = Chave_Primaria_Pai
        Else
            Detalhe_filho1.Chave_Primaria = Chave_Primaria
            Detalhe.Chave_Primaria = Chave_Primaria
        End If
        
        co_Detalhe_Filho.Add Detalhe_filho1
        
        rs.MoveNext
                       
        If Not rs.EOF() Then
            If vl_Layout_id_Old <> rs("Layout_Id") Then
                Chave_Primaria_Pai = ""
            Else
                If rs("dependencia_detalhe_id") <> vl_Detalhe_id_Old Then
                    Chave_Primaria = ""
                End If
            End If
               
        End If
        co_Detalhe.Add Detalhe
        TTLayout.Tipos_Detalhe.Add Detalhe
    Wend
    rs.Close
   
    For Each Detalhe In co_Detalhe 'TTLayout.Tipos_Detalhe
        Set Detalhe.Identificadores = Nothing
        ' Carrega os tipos dispon�veis.
        SQL = "SELECT identificador, tamanho, constante, seq_campo, tipo,Separador_Milhar, Separador_Decimal, Nome_SEGBR, Formato, nome"
        SQL = SQL & " FROM interface_db.dbo.GTR_layout_campo_tb WITH (NOLOCK) "
        SQL = SQL & "   WHERE layout_id = " & Detalhe.Layout_Id
        SQL = SQL & "   AND detalhe_id = " & Detalhe.Detalhe_id
        SQL = SQL & " ORDER BY seq_campo "
        
        Set rs = rdocn_Interface.OpenResultset(SQL)
        
        While Not rs.EOF
            If UCase(rs("identificador")) = "S" Then
                Set Identificador = New clIdentificador
                Identificador.Tamanho = rs("tamanho")
                Identificador.Conteudo = "" & rs("constante")
                Identificador.Seq_Campo = rs("seq_campo")
                Identificador.Layout_Id = Detalhe.Layout_Id
                Identificador.Detalhe_id = Detalhe.Detalhe_id
                Detalhe.Identificadores.Add Identificador
            End If
            Set cl_campos = New clEvento_Layout
            cl_campos.Tamanho = rs("tamanho")
            cl_campos.Conteudo = "" & rs("constante")
            cl_campos.Seq_Campo = rs("seq_campo")
            cl_campos.Layout_Id = Detalhe.Layout_Id
            cl_campos.Detalhe_id = Detalhe.Detalhe_id

            cl_campos.Tipo = UCase(rs("tipo"))
            cl_campos.Separador_Milhar = Trim("" & rs("separador_milhar"))
            cl_campos.Separador_Decimal = Trim("" & rs("separador_decimal"))
            cl_campos.Nome_Campo = LCase("" & rs("nome_SEGBR"))
            cl_campos.Formato = "" & rs("formato")
            cl_campos.Conteudo = "" & rs("constante")
            cl_campos.Nome = LCase("" & rs("nome"))
            co_campos.Add cl_campos
                        
            rs.MoveNext
        Wend
        rs.Close
                
        For Each Identificador In Detalhe.Identificadores
            SQL = "SELECT isnull(SUM(tamanho),0) posicao_indentificador "
            SQL = SQL & " FROM interface_db.dbo.GTR_layout_campo_tb WITH (NOLOCK) "
            SQL = SQL & " WHERE layout_id = " & Detalhe.Layout_Id
            SQL = SQL & "   AND detalhe_id = " & Detalhe.Detalhe_id
            SQL = SQL & "   AND seq_campo < " & Identificador.Seq_Campo
            
            Set rs = rdocn_Interface.OpenResultset(SQL)
            Identificador.posicao = Val(rs(0)) + 1
            rs.Close
        Next ' Identificador
    
    Next
    
    'Carrega os dependentes
    For Each Detalhe_Pai In co_Detalhe
        For Each Detalhe In co_Detalhe
            If Detalhe_Pai.Detalhe_id = Detalhe.Dependencia_Detalhe_id And Detalhe.Layout_Id = Detalhe_Pai.Layout_Id Then
                Detalhe_Pai.Layout_Id = Detalhe.Layout_Id
                Detalhe_Pai.Dependentes.Add Detalhe.Detalhe_id
                co_dependentes.Add Detalhe_Pai
            End If
        Next ' Detalhe_Filho
    Next ' Detalhe_Pai
        
   'Obt�m qtde de dependentes por layout
    qtd_layout = 0
    For Each Detalhe In co_dependentes
        For Each Detalhe_Pai In co_dependentes
            If Detalhe.Layout_Id = Detalhe_Pai.Layout_Id And Detalhe.Detalhe_id = Detalhe_Pai.Detalhe_id Then
                qtd_layout = qtd_layout + 1
            End If
        Next
        Detalhe.Qtde_Layout = qtd_layout
        qtd_layout = 0
    Next
    
    Exit Function
    
Erro:
    Carrega_Tipos_Detalhe_NEW = Err.Number
    
    Call Grava_Trace(Err.Number, Err.Description, "Carrega_Tipos_Detalhe_NEW")
    Call GravaLogErro("", GTR.strRemessa, "SQL", "Carrega_Tipos_Detalhe_NEW", Err.Description)
    
    rdocn_Interface.Close
    Set rdocn_Interface = Nothing
    Call TerminaSEGBR
    
End Function

Public Function Carrega_Critica_Layout_NEW( _
                                            ByVal Acao As Integer, _
                                            ByVal Mod_rdocn As rdoConnection, _
                                            ByRef Layout As clLayout, _
                                            ByRef Erro As clErro, _
                                            ByRef vl_ID_Layout As String, _
                                            Optional ByVal Chaves_Procuradas As Variant, _
                                            Optional ByVal Tabela_Chaves_Encontradas As String) _
                                        As Long
    
    ' Acao: Indica se � para carregar e/ou criticar o layout.
    ' Mod_rdocn: Recebe a conex�o onde as tempor�rias ser�o criadas.
    ' Layout: Objeto Layout, com layout_id, tipos de detalhes e registros.
    ' Erro: objeto contendo informa��es sobre os erros de layout.
    ' Chaves_Procuradas: As chaves que dever�o ser buscadas nos registros
    ' Tabela_Chaves_Encontradas: Nome da tabela tempor�ria que foi criada que cont�m
    '     as chaves.
    
    Dim Erro_interno As Long, SQL As String, Registro As Variant
    Dim Qtd_Registros As Integer, i As Integer
    ' Tratamento do Header e Trailler.
    Dim Layout_Tem_Header As Boolean, Layout_Tem_Trailler As Boolean
    Dim Header As String, Trailler As String
    Dim Header_Trailler As Integer
    ' Tratamento da tabela tempor�ria.
    Dim Primeiro_Registro As Boolean, Detalhe As clDetalhe
    Dim Chaves_Encontradas() As String, Chaves_Temp() As String
    Dim Chave_Primaria As String, Valores_Chave_Primaria As String
    Dim Nome_Tabela As String
              
    On Error GoTo Erro
    
'    Carrega_Critica_Layout = MOD_OK_APLICACAO ' Por enquanto, est� tudo Ok.
    ' Critica os par�metros de a��o
    Select Case Acao
    Case 1: Carrega = True
            Critica = False
    Case 2: Carrega = False
            Critica = True
    Case 4: Carrega = True
            Critica = True
    Case Else: Erro.Descricao_Erro = "Par�metro ACAO inv�lido !"
                Err.Raise 6666
    End Select
    
     
    While InStr(String_tabelas_Temp, "***") <> 0 Or String_tabelas_Temp <> ""
        If InStr(String_tabelas_Temp, "***") = 0 Then
            SQL = " drop table " & String_tabelas_Temp
        Else
            SQL = " drop table " & Left(String_tabelas_Temp, InStr(String_tabelas_Temp, "***") - 1)
        End If
        On Error Resume Next
        Call Mod_rdocn.OpenResultset(SQL)
        On Error GoTo Erro
        
        'Retira o nome tabela que foi "dropada" da string
        
        If InStr(String_tabelas_Temp, "***") = 0 Then
            String_tabelas_Temp = ""
        Else
            String_tabelas_Temp = Right(String_tabelas_Temp, Len(String_tabelas_Temp) - (InStr(String_tabelas_Temp, "***") + 2))
        End If
    Wend
     
    Set Erro = New clErro
    
    ' Inicializa vari�veis
    Erro.Cod_Erro = "0"  ' Registros Ok = 0
    Erro.Campo_Erro = ""
    Header = ""
    Trailler = ""
    Qtd_Registros = 0
    
    Header_Trailler = 2 '' vari�vel carregada na fun��o Procura_Header_Trailler
  ''Procura_Header_Trailler vl_ID_Layout, Header_Trailler '' fun��o comentada pq header/trailler passa a ser carregado no carrega_tipo_detalhe_new
    
    If Not IsMissing(Chaves_Procuradas) Then  ' Se procura por alguma chave
        ' Critica campos duplicados (se encontrar, vai levantar erro)
        Critica_Campos_Iguais_NEW Chaves_Procuradas
    
        ' Encontradas com mesmo tamanho das procuradas
        ReDim Chaves_Encontradas(UBound(Chaves_Procuradas)) ' Encontrado no grupo de depend�ncia (acumulado da Chaves_Temp)
        ReDim Chaves_Temp(UBound(Chaves_Procuradas)) ' Encontrado por linha
        ' Cria tabela tempor�ria que ir� conter os campos desejados pela aplica��o
        Call Cria_Tabela_Temp_Chaves_NEW(Mod_rdocn, Chaves_Procuradas, Tabela_Chaves_Encontradas)
        
    End If
    
''Fun��o comentada pq ser�o carregados todos os layouts no carrega_tipo_detalhe_new
''''    Erro_interno = Carrega_Tipos_Detalhe_NEW()
''''    If Erro_interno > 0 Then
''''        Carrega_Critica_Layout_NEW = Erro_interno
''''        Exit Function
''''    End If
    
    If Carrega Then
        'teste Detalhes
        
        Cria_Tabela_Temp_Registros_NEW _
                Mod_rdocn, vl_ID_Layout

        For Each Detalhe In co_Detalhe
            Detalhe.Ult_Chave = 0
            ' Limpa o 'int' usado na cria��o da tabela
            ' Agora a chave prim�ria ser� usada no insert.
            Detalhe.Chave_Primaria = Replace(Detalhe.Chave_Primaria, " int ", "")
        Next
    End If
    
    Primeiro_Registro = True
 
    x = 1
    For Each Registro In Layout.registros
    
        Qtd_Registros = Qtd_Registros + 1
        
        Set Detalhe = Nothing
        Erro_interno = Identifica_Detalhe_NEW(Registro, Detalhe, vl_ID_Layout)
        
        If Erro_interno > 0 Then
            Carrega_Critica_Layout_NEW = Erro_interno
            Exit Function
        End If
        If Detalhe Is Nothing Then
            Erro.Cod_Erro = "9" ' Registro inv�lido.
            Erro.Campo_Erro = "IDENTIFICADOR DO REGISTRO"
            Erro.Linha_Erro = Qtd_Registros
            Exit Function
        End If
        
        ' Tratamento da tabela temporaria:
        ' Ela cont�m os campos pedidos no par�metro chaves_procuradas.
        If Not IsMissing(Chaves_Procuradas) Then
            ' S� inclui a cada registro pai.
            If (Detalhe.Dependencia_Detalhe_id = "NULL") And (Not Primeiro_Registro) Then
                ' Inclui na tabela tempor�ria de chaves encontradas
                Inclui_Chave_NEW Mod_rdocn, Tabela_Chaves_Encontradas, Detalhe.Detalhe_id, _
                             Chaves_Procuradas, Chaves_Encontradas
        
                ' Limpa os vetores
                ReDim Chaves_Encontradas(UBound(Chaves_Procuradas))
                ReDim Chaves_Temp(UBound(Chaves_Procuradas))
            End If
        End If
                
        ' A partir de agora n�o � mais primeiro registro
        Primeiro_Registro = False
        
        ' Critica cada registro :
        ' Header e Trailler s�o criticados no final.
        If Detalhe.Detalhe_id = "0" Then  ' Header
            If Qtd_Registros <> 1 Then ' Se o Header n�o est� na primeira linha
                Erro.Cod_Erro = "19" ' Header/Trailler inv�lido
                Erro.Campo_Erro = "IDENTIFICADOR DO REGISTRO"
                Erro.Linha_Erro = 1
                Exit Function
            End If
            Header = Registro
        ElseIf Detalhe.Detalhe_id = "9999" Then  ' Trailler
            If Qtd_Registros <> Layout.registros.Count + 1 Then ' Se o Trailler n�o est� na �ltima linha
                Erro.Cod_Erro = "19" ' Header/Trailler inv�lido
                Erro.Campo_Erro = "IDENTIFICADOR DO REGISTRO"
                Erro.Linha_Erro = Layout.registros.Count
                Exit Function
            End If
            Trailler = Registro
        Else ' Pode praticar a a��o
                       
''comentada a chamada pq obtem_chave foi substitu�da pelo bloco abaixo
''            If Carrega Then
''                Obtem_Chave co_Detalhe, Detalhe, Valores_Chave_Primaria, layout
''            End If

            Dim posicao_registro_anterior As String  ''obt�m primeira letra do registro
            If Carrega Then
                                             
                  If Left(Registro, 1) = posicao_registro_anterior Then
                     x = x + 1
                  Else
                     x = 1
                  End If
                  
                  i = 1
                  Valores_Chave_Primaria = "1"
                  For Each Detalhe_Pai In co_dependentes
                      If Detalhe_Pai.Layout_Id = Detalhe.Layout_Id Then
                        If i <= Detalhe_Pai.Qtde_Layout Then
                            If Detalhe_Pai.Dependentes(i) = Detalhe.Detalhe_id Then
                                Valores_Chave_Primaria = "0, " & x
                                Exit For
                            End If
                        Else
                            If Detalhe.Detalhe_id <> 1 Then Valores_Chave_Primaria = "0, 1, " & x
                            Exit For
                        End If
                        i = i + 1
                      Else
                        i = 1
                      End If
                  Next

                  Erro_interno = _
                        Carrega_Critica_Registro_NEW(Acao, Mod_rdocn, Registro, _
                        Layout.Layout_Id, Detalhe, Erro, Detalhe.Detalhe_id, _
                        Valores_Chave_Primaria, Header_Trailler, _
                        Layout.registros.Count, Chaves_Procuradas, Chaves_Temp)
                        
                  If Erro_interno > 0 Then
                      Carrega_Critica_Layout_NEW = Erro_interno
                      Exit Function
                  End If
                
                  posicao_registro_anterior = Left(Registro, 1)

            End If
            
            
            If Not IsMissing(Chaves_Procuradas) Then
                ' Se encontrou mais alguma chave, descarrega nas chaves encontradas.
                For i = LBound(Chaves_Encontradas) To UBound(Chaves_Encontradas)
                    If (Chaves_Encontradas(i) = "") And (Chaves_Temp(i) <> "") Then
                        Chaves_Encontradas(i) = Chaves_Temp(i)
                    End If
                Next i
            End If
            
        End If
        
        If Erro.Cod_Erro <> "0" Then
            Erro.Linha_Erro = Qtd_Registros
            Exit Function
        End If
    Next
            
    If Layout.registros.Count > 0 Then
        ' Tratamento da tabela temporaria (�ltimo registro)
        If Not IsMissing(Chaves_Procuradas) Then
            ' Inclui na tabela tempor�ria de chaves encontradas
            Inclui_Chave_NEW Mod_rdocn, Tabela_Chaves_Encontradas, Detalhe.Detalhe_id, _
                         Chaves_Procuradas, Chaves_Encontradas
                         
        End If
    End If

    If Layout_Tem_Header Then
        If Header <> "" Then
            Erro_interno = _
                Carrega_Critica_Registro_NEW(Acao, Mod_rdocn, Header, _
                                            Layout.Layout_Id, Detalhe, Erro, Detalhe.Detalhe_id, _
                                            Chave_Primaria, Valores_Chave_Primaria, _
                                            Header_Trailler, Layout.registros.Count)
                
            If (Erro_interno > 0) Or (Erro.Cod_Erro <> "0") Then
                Carrega_Critica_Layout_NEW = Erro_interno
                Erro.Linha_Erro = 1
                Exit Function
            End If
        Else
            Erro.Cod_Erro = "19" ' Layout sem Header/Trailler.
            Erro.Campo_Erro = "IDENTIFICADOR DO REGISTRO"
            Erro.Linha_Erro = Qtd_Registros
            Exit Function
        End If
    End If

    If Layout_Tem_Trailler Then
        If Trailler <> "" Then
            Erro_interno = _
                Carrega_Critica_Registro_NEW(Acao, Mod_rdocn, Trailler, _
                    Layout.Layout_Id, Detalhe, Erro, Detalhe.Detalhe_id, Chave_Primaria, _
                    Valores_Chave_Primaria, Header_Trailler, Layout.registros.Count)
                    
            If (Erro_interno > 0) Or (Erro.Cod_Erro <> "0") Then
                Carrega_Critica_Layout_NEW = Erro_interno
                Erro.Linha_Erro = Layout.registros.Count
                Exit Function
            End If
        Else
            Erro.Cod_Erro = "19" ' Layout sem Header/Trailler.
            Erro.Campo_Erro = "IDENTIFICADOR DO REGISTRO"
            Erro.Linha_Erro = Qtd_Registros
            Exit Function
        End If
    End If
    
    Exit Function
    Resume
    
Erro:

    Call Grava_Trace(Err.Number, Err.Description, "Carrega_Critica_Layout_NEW()")

    Carrega_Critica_Layout_NEW = Err.Number
    GravaLogErro Str(GTR.CodigoErroRemessa), GTR.strRemessa, "PROC", "CARREGA_CRITICA_LAYOUT", Err.Number
    
End Function

Public Function Carrega_Critica_Registro_NEW( _
ByVal Acao As Integer, _
ByVal Mod_rdocn As rdoConnection, _
ByVal Registro As String, _
ByVal Layout_Id As String, _
ByVal Detalhe As clDetalhe, _
ByRef Erro As clErro, _
ByRef Detalhe_id As Integer, _
Optional ByVal Valores_Chave_Primaria As String, _
Optional ByVal Header_Trailler As Integer, _
Optional ByVal Qtd_Registros As Long, _
Optional ByRef Chaves_Procuradas As Variant, _
Optional ByRef Chaves_Temp As Variant) As Long

    Dim SQL As String, rs As rdoResultset
    Dim Conteudo As String
    Dim SMilhar As String, sDecimal As String
    Dim i As Integer, Obrigatorio As Boolean, Seq_Campo As Integer
    Dim SQL_INSERT As String, SQL_VALUES As String
    Dim Campo_Formatado As String
    Dim Campo() As Tp_Campo
    
    On Error GoTo Erro
    'Carrega_Critica_Registro_NEW = MOD_OK_APLICACAO ' Por enquanto, est� tudo Ok.
    
    ' Critica os par�metros de a��o
    Select Case Acao
    Case 1: Carrega = True
            Critica = False
    Case 2: Carrega = False
            Critica = True
    Case 4: Carrega = True
            Critica = True
    Case Else: Erro.Descricao_Erro = "Par�metro ACAO inv�lido !"
                Err.Raise 6666
    End Select
    
    If Not IsMissing(Chaves_Procuradas) Then
        ReDim Chaves_Encontradas(UBound(Chaves_Procuradas))
    End If
    
    If Critica Then
        If Len(Registro) < Detalhe.Tamanho Then
            Erro.Cod_Erro = "17"  ' Erro no tamanho do registro.
            Erro.Campo_Erro = "TAMANHO DO REGISTRO"
            Exit Function
        End If
    End If
    
    Seq_Campo = 0
        
    For Each cl_campos In co_campos
        If cl_campos.Layout_Id = Layout_Id And cl_campos.Detalhe_id = Detalhe_id Then
        
            Seq_Campo = Seq_Campo + 1
            ReDim Preserve Campo(Seq_Campo) As Tp_Campo

            With Campo(Seq_Campo)
                
                .Obrigatorio = UCase("S")
                .Tamanho = Val(cl_campos.Tamanho)
                .Tipo = UCase(cl_campos.Tipo)
                .Conteudo = Left(Registro, .Tamanho)
                .Separador_Milhar = Trim("" & cl_campos.Separador_Milhar)   ' Separador de milhar (campo num�rico)
                .Separador_Decimal = Trim("" & cl_campos.Separador_Decimal)  ' Separador de decimal (campo num�rico)
                .Nome_SEGBR = LCase("" & cl_campos.Nome_Campo)
                .Formato = "" & cl_campos.Formato
                .Constante = "" & cl_campos.Conteudo
                
            End With
                        
            ' Posiciona registro para pr�ximo campo.
            Registro = Mid(Registro, cl_campos.Tamanho + 1)
            
            If Critica Then
                Erro.Cod_Erro = "0"
                Select Case cl_campos.Tipo
                Case "AN" ' Alfanum�rico
                    Critica_Campo_Alfanumerico_NEW cl_campos.Conteudo, Erro.Cod_Erro
                Case "NM" ' Num�rico
                    Critica_Campo_Numerico_NEW cl_campos.Conteudo, cl_campos.Formato, cl_campos.Separador_Decimal, cl_campos.Separador_Milhar, Erro.Cod_Erro
                Case "DT" ' Data
                    Critica_Campo_Data_NEW cl_campos.Conteudo, cl_campos.Formato, Erro.Cod_Erro
                Case "CT" ' Constante
                    Critica_Campo_Constante_NEW cl_campos.Conteudo, cl_campos.Conteudo, Erro.Cod_Erro
                Case "C1", "C2" ' Contadores (com ou sem Header e Trailler)
                    If (IsMissing(Header_Trailler)) Or (IsMissing(Qtd_Registros)) Then
                        ' S� pode criticar Quantidade de registros se
                        ' informou o Header e Trailler e a quantidade
                        ' de registros (Levanta erro para a aplica��o)
                        Err.Raise 22222, "Aplica��o", "Indicador de Header e Trailler n�o informado !"
                    End If
                    Critica_Campo_Contador_NEW cl_campos.Conteudo, cl_campos.Tipo, Qtd_Registros, Header_Trailler, Erro.Cod_Erro
                End Select
                
                If Erro.Cod_Erro <> "0" Then
                    Erro.Campo_Erro = cl_campos.Nome
                    Exit Function
                End If
            End If
                               
            ' Se est� procurando por uma chave, grava nas chaves encontradas (mem�ria)
            If Not IsMissing(Chaves_Procuradas) Then
                For i = LBound(Chaves_Procuradas) To UBound(Chaves_Procuradas)
                    If Chaves_Procuradas(i) = cl_campos.Nome_Campo Then
                        Chaves_Temp(i) = cl_campos.Conteudo
                    End If
                Next i
            End If
            
        End If
    Next

    SQL_INSERT = ""
    SQL_VALUES = ""
    
    If (Carrega) And (Detalhe.Nome_Tabela_Temporaria <> "") Then
        If (Detalhe.Chave_Primaria <> "") Then
            SQL_INSERT = "(" & Detalhe.Chave_Primaria
            SQL_VALUES = "(" & Valores_Chave_Primaria
            
            For i = 1 To UBound(Campo)
                If Campo(i).Nome_SEGBR <> "" Then
                    Campo_Formatado = Formata_Campo(Campo(i))
                    SQL_INSERT = SQL_INSERT & ", " & Campo(i).Nome_SEGBR
                    SQL_VALUES = SQL_VALUES & ", '" & Replace(Campo_Formatado, "'", "�") & "'"
                End If
            Next i

            SQL_INSERT = SQL_INSERT & ")"
            SQL_VALUES = SQL_VALUES & ")"

            SQL = "INSERT INTO " & Detalhe.Nome_Tabela_Temporaria
            SQL = SQL & " " & SQL_INSERT
            SQL = SQL & " VALUES "
            SQL = SQL & SQL_VALUES
            ' Inclui o registro
            'Debug.Print SQL
            Mod_rdocn.OpenResultset (SQL)

        End If
    End If
    
    Exit Function

Erro:
    Carrega_Critica_Registro_NEW = Err.Number
    
    Call Grava_Trace(Err.Number, Err.Description, "Carrega_Critica_Registro_NEW()")

End Function
Private Sub Critica_Campos_Iguais_NEW(Chaves_Procuradas As Variant)

    Dim i As Integer, j As Integer
    
    On Error GoTo TrataErro
    
    For i = LBound(Chaves_Procuradas) To UBound(Chaves_Procuradas)
        For j = LBound(Chaves_Procuradas) To UBound(Chaves_Procuradas)
            If (Chaves_Procuradas(i) = Chaves_Procuradas(j)) And (i <> j) Then
                ' Levanta erro para a aplica��o.
                Err.Raise 22222, "Aplica��o", "Campos procurados (�ndices: " & i & " e " & j & ") repetidos !"
            End If
        Next j
    Next i
    
    Exit Sub
    
TrataErro:
    Call GravaLogErro("", GTR.strRemessa, "SQL", "CRITICA_CAMPOS_IGUAIS", Err.Description)
    rdocn_Interface.Close
    Set rdocn_Interface = Nothing
    Call TerminaSEGBR
    
End Sub

Private Sub Cria_Tabela_Temp_Registros_NEW( _
ByVal Mod_rdocn As rdoConnection, _
ByVal vl_ID_Layout As String _
)
    
    Dim Recurso_temp As String
   ' Dim rs As rdoResultset,
    Dim SQL As String
    Dim i As Integer
    Dim Detalhe_id_Aux As String
    
    On Error GoTo TrataErro
    
    For Each Detalhe_filho1 In co_Detalhe_Filho '
    
       If vl_ID_Layout = Detalhe_filho1.Layout_Id Then
            If Detalhe_filho1.Nome_Tabela_Temporaria <> "" Then
                ' Cria a tabela
                SQL = Gera_Script_Tabela_NEW(Mod_rdocn, vl_ID_Layout, Detalhe_filho1.Detalhe_id, _
                                         Detalhe_filho1.Chave_Primaria, Detalhe_filho1.Nome_Tabela_Temporaria)
                If SQL <> "" Then
                    'Debug.Print SQL
                    Mod_rdocn.OpenResultset (SQL)
                End If
            End If ' Se tem nome de tabela tempor�ria.
        End If
    Next ' Detalhe_Filho
    
    Exit Sub
    
TrataErro:

    Call Grava_Trace(Err.Number, Err.Description, "Cria_Tabela_Temp_Registros_NEW()")

    Call GravaLogErro("", GTR.strRemessa, "SQL", "CRIA_TABELA_TEMP_REGISTROS", Err.Description)
    rdocn_Interface.Close
    Set rdocn_Interface = Nothing
    Call TerminaSEGBR
    
End Sub

Public Function Identifica_Detalhe_NEW( _
ByVal Registro As String, _
ByRef Detalhe_Externo As clDetalhe, _
ByRef vl_ID_Layout As String) _
As Long

    Dim SQL As String, rs As rdoResultset, Maior As Integer
    Dim i As Integer
    Dim Encontrou_Detalhe As Boolean, j As Integer
    'Dim Detalhe_Local As clDetalhe
    Dim Identificador As clIdentificador

    On Error GoTo Erro

    'Identifica_Detalhe_NEW = MOD_OK_APLICACAO ' Por enquanto, est� Ok.
    Encontrou_Detalhe = False
    Set Identificador = New clIdentificador
        
    For Each Detalhe In co_Detalhe
        If Detalhe.Layout_Id = vl_ID_Layout Then
            Encontrou_Detalhe = True
            For Each Identificador In Detalhe.Identificadores
                If Identificador.Detalhe_id = Detalhe.Detalhe_id And Identificador.Layout_Id = Detalhe.Layout_Id Then
                    If Mid(Registro, Identificador.posicao, Identificador.Tamanho) <> Identificador.Conteudo Then
        '                        ' Se algum identificador deste detalhe n�o bateu,
        '                        ' ent�o n�o encontrou.
                        Encontrou_Detalhe = False
                    End If
                End If
                If Encontrou_Detalhe Then
                    Set Detalhe_Externo = Detalhe
                    Exit Function
                End If
            Next
        End If
    Next

    Exit Function
      

Erro:
    Identifica_Detalhe_NEW = Err.Number
    
    Call Grava_Trace(Err.Number, Err.Description, "Identifica_Detalhe_NEW()")
    
    Call GravaLogErro("", GTR.strRemessa, "SQL", "IDENTIFICA_DETALHE", Err.Description)
    rdocn_Interface.Close
    Set rdocn_Interface = Nothing
    Call TerminaSEGBR
    
End Function

Private Sub Inclui_Chave_NEW(ByVal Mod_rdocn As rdoConnection, _
                         ByVal Tabela_Chaves_Encontradas As String, _
                         ByVal PK_DETALHE_ID As String, _
                         ByVal Chaves_Procuradas As Variant, _
                         ByVal Chaves_Encontradas As Variant)
    
    Dim SQL As String, i As Integer
    Dim Existe_Campo_Preenchido As Boolean
    
    On Error GoTo TrataErro
    
    Existe_Campo_Preenchido = False

    SQL = "INSERT INTO " & Tabela_Chaves_Encontradas
    SQL = SQL & " (PK_DETALHE_ID"
    For i = LBound(Chaves_Procuradas) To UBound(Chaves_Procuradas)
        SQL = SQL & ", " & Chaves_Procuradas(i)
    Next i
    SQL = SQL & ")"
    
    SQL = SQL & " VALUES (" & PK_DETALHE_ID
    For i = LBound(Chaves_Encontradas) To UBound(Chaves_Encontradas)
        If Chaves_Encontradas(i) = "" Then
            SQL = SQL & ", NULL"
        Else
            Existe_Campo_Preenchido = True
            SQL = SQL & ", " & Chaves_Encontradas(i)
        End If
    Next i
    SQL = SQL & ")"
    
    If Existe_Campo_Preenchido Then
        Mod_rdocn.OpenResultset (SQL)
    End If
    
    Exit Sub
    
TrataErro:

    Call Grava_Trace(Err.Number, Err.Description, "Inclui_Chave_NEW()")

    Call GravaLogErro("", GTR.strRemessa, "SQL", "INCLUI_CHAVE", Err.Description)
    rdocn_Interface.Close
    Set rdocn_Interface = Nothing
    Call TerminaSEGBR
    
End Sub

Private Sub Cria_Tabela_Temp_Chaves_NEW(ByVal Mod_rdocn As rdoConnection, _
                                    ByVal Chaves_Procuradas As Variant, _
                                    ByVal Tabela_Chaves_Procuradas As String)

    'Dim rs As rdoResultset,
    Dim SQL As String, i As Integer
    
    On Error GoTo Erro
    
    While True ' Mant'em o loop ate ele entrar no tratamento do erro.
        ' Testa se tabela informada existe no tempdb.
        SQL = "SELECT * FROM " & Tabela_Chaves_Procuradas
        Set rs = Mod_rdocn.OpenResultset(SQL)
        ' Se nao deu erro, eh porque existe, entao tem que apagar.
        rs.Close
        SQL = "DROP TABLE " & Tabela_Chaves_Procuradas
        Mod_rdocn.OpenResultset (SQL)
    Wend
    
    Exit Sub

Erro:
    If rdoErrors(0).Number = 208 Then     ' Se n�o existe, j� pode criar a tabela.
        ' Cria��o da tabela tempor�ria
        SQL = "CREATE TABLE " & Tabela_Chaves_Procuradas
        SQL = SQL & " (PK_SEQ int IDENTITY, PK_DETALHE_ID int"
        For i = LBound(Chaves_Procuradas) To UBound(Chaves_Procuradas)
            If Trim(Chaves_Procuradas(i)) = "" Then
                ' Levanta erro para a aplica��o.
                Err.Raise 22222, "Aplica��o", "Campo procurado (�ndice: " & i & ") em branco !"
            ElseIf (Chaves_Procuradas(i) = "PK_SEQ") Or (Chaves_Procuradas(i) = "PK_DETALHE_ID") Then
                ' Levanta erro para a aplica��o.
                Err.Raise 22222, "Aplica��o", "Campo reservado 'PK_SEQ' ou 'PK_DETALHE_ID' usado como busca !"
            End If
            
            SQL = SQL & ", " & Chaves_Procuradas(i) & " VARCHAR(2000) NULL"
        Next i
        SQL = SQL & ")"
        '**************************************************************
        'Alterado por Cleber - data: 23/05/2005
        'Controle das tempor�rias
        If String_tabelas_Temp = "" Then
            String_tabelas_Temp = Tabela_Chaves_Procuradas
        Else
            String_tabelas_Temp = String_tabelas_Temp & "***" & Tabela_Chaves_Procuradas
        End If
        '**************************************************************
        Mod_rdocn.OpenResultset (SQL)
    Else
    
        Call Grava_Trace(Err.Number, Err.Description, "Cria_Tabela_Temp_Chaves_NEW()")
    
        Call GravaLogErro("", GTR.strRemessa, "SQL", "CRIA_TABELA_TEMP_CHAVES", Err.Description)
        rdocn_Interface.Close
        Set rdocn_Interface = Nothing
        Call TerminaSEGBR
'        Err.Raise Err.Number
    End If

End Sub
Private Function Gera_Script_Tabela_NEW(ByVal Mod_rdocn As rdoConnection, _
                                    ByVal Layout_Id As String, _
                                    ByVal Detalhe_id As String, _
                                    ByVal Chave_Primaria As String, _
                                    ByVal Nome_Tabela As String) As String
    
    Dim Lista_Campos As String, SQL As String
    'DIm rs As rdoResultset
    
    On Error GoTo TrataErro
    
    Lista_Campos = ""
    
    For Each cl_campos In co_campos
      If cl_campos.Layout_Id = Layout_Id And cl_campos.Detalhe_id = Detalhe_id Then
          If cl_campos.Nome_Campo <> "" Then
              Lista_Campos = Lista_Campos & ", " & cl_campos.Nome_Campo & " varchar(" & (Val(cl_campos.Tamanho) + 1) & ") NULL "
          End If
      End If
    Next

    If Lista_Campos <> "" Then
        
    ' Se j� existe, apaga.
    SQL = "SELECT COUNT(*) FROM tempdb..sysobjects "
    SQL = SQL & " WHERE id = object_id(N'tempdb.." & Nome_Tabela & "') "
    Set rs = Mod_rdocn.OpenResultset(SQL)

    If rs(0) = 0 Then ' Se n�o existe, j� pode criar a tabela.
        Gera_Script_Tabela_NEW = "CREATE TABLE " & Nome_Tabela & " (" & Chave_Primaria & Lista_Campos & ")"
        '**************************************************************
        'Alterado por Cleber - data: 23/05/2005
        'Controle das tempor�rias
        If String_tabelas_Temp = "" Then
            String_tabelas_Temp = Nome_Tabela
        Else
            String_tabelas_Temp = String_tabelas_Temp & "***" & Nome_Tabela
        End If
        '**************************************************************
    Else ' Se existe, apaga todos os registros.
        Gera_Script_Tabela_NEW = "DELETE FROM " & Nome_Tabela
    End If
    rs.Close
        
    End If
    
    Exit Function
    
TrataErro:
    'Grava o Log do Erro
    
    Call Grava_Trace(Err.Number, Err.Description, "Gera_Script_Tabela_NEW()")
        
    Call GravaLogErro("", "Descri��o: " & Err.Description, "SQL", "GERA_SCRIPT_TABELA", Err.Number)
    rdocn_Interface.Close
    Set rdocn_Interface = Nothing
    Call TerminaSEGBR
    
End Function

Private Sub Critica_Campo_Alfanumerico_NEW(ByVal Conteudo As String, ByRef Cod_Erro As String)

    If Trim(Conteudo) = "" Then
        Cod_Erro = "2" ' Campo com preenchimento obrigat�rio
        Exit Sub
    End If

End Sub

Private Sub Critica_Campo_Numerico_NEW(ByVal Conteudo As String, _
                                   ByVal Formato As String, _
                                   ByVal Separador_Decimal As String, _
                                   ByVal Separador_Milhar As String, _
                                   ByRef Cod_Erro As String)

    Dim i As Integer, Conteudo_Temp As String
    Dim Inteiro As Integer, Fracao As Integer
    Dim Formato_Cadastrado As String
    Dim Parte_Inteira As String, Parte_Decimal As String
    
    ' Critica se � um n�mero.
    If Not IsNumeric(Conteudo) Then
        Cod_Erro = "1" ' Campo n�o num�rico
        Exit Sub
    End If

    ' Critica o formato.
    Conteudo_Temp = ""
    For i = 1 To Len(Conteudo)
        If (Mid(Conteudo, i, 1) <> ".") And _
           (Mid(Conteudo, i, 1) <> ",") Then
            Conteudo_Temp = Conteudo_Temp & "##"
        Else
            Conteudo_Temp = Conteudo_Temp & Mid(Conteudo, i, 1)
        End If
    Next i
    
    ' O formato cont�m "<Inteiro>,<Fra��o>"
    Inteiro = Val(Left(Formato, InStr(Formato, ",") - 1))
    Fracao = Val(Mid(Formato, InStr(Formato, ",") + 1))
    Parte_Inteira = ""
    While Inteiro > 0
        If Parte_Inteira = "" Then
            Parte_Inteira = Parte_Inteira & Left(String(Inteiro, "##"), 3)
        Else
            Parte_Inteira = Left(String(Inteiro, "##"), 3) & Separador_Milhar & Parte_Inteira
        End If
        Inteiro = Inteiro - 3
    Wend
    If Fracao > 0 Then
        Parte_Decimal = Separador_Decimal & String(Fracao, "##")
    Else
        Parte_Decimal = ""
    End If
    Formato_Cadastrado = Parte_Inteira & Parte_Decimal
    
    If Formato_Cadastrado <> Conteudo_Temp Then
        Cod_Erro = "1" ' Campo num�rico inv�lido
        Exit Sub
    End If
    
    ' O campo num�rico obrigat�rio tem que ser diferente de zero.
    If Val(Conteudo) = 0 Then
        Cod_Erro = "2" ' Campo com preenchimento obrigat�rio
        Exit Sub
    End If

End Sub
Private Sub Critica_Campo_Data_NEW(ByVal Conteudo As String, _
                               ByVal Formato As String, _
                               ByRef Cod_Erro As String)

    Dim ano As String, mes As String, dia As String
    
    If Len(Conteudo) <> Len(Formato) Then
        Cod_Erro = "3" ' Data Inv�lida
        Exit Sub
    Else
        If InStr(Formato, "YYYY") > 0 Then ' Ano com 4 d�gitos.
            ano = Mid(Conteudo, InStr(Formato, "Y"), 4)
        Else ' Ano com 2 d�gitos
            ano = Mid(Conteudo, InStr(Formato, "Y"), 2)
            If Val(ano) < 10 Then ' Bug do ano 2000 s� coberto at� 2010.
                ano = "20" & ano
            Else
                ano = "19" & ano
            End If
        End If
        mes = Mid(Conteudo, InStr(Formato, "M"), 2)
        dia = Mid(Conteudo, InStr(Formato, "D"), 2)
        
        If Not IsNumeric(ano) Or _
           Not IsNumeric(ano) Or _
           Not IsNumeric(ano) Then
            Cod_Erro = "3" ' Data Inv�lida
            Exit Sub
        Else
            If (Val(dia) < 1 Or Val(dia) > 31) Or _
               (Val(mes) < 1 Or Val(mes) > 12) Or _
               (Val(ano) < 1900 Or Val(ano) > (Year(Data_Sistema) + 30)) Then ' 30 anos de folga
                Cod_Erro = "3" ' Data Inv�lida
                Exit Sub
            End If
        End If
    End If

End Sub
Private Sub Critica_Campo_Constante_NEW(ByVal Conteudo As String, _
                                    ByVal Constante As String, _
                                    ByRef Cod_Erro As String)

    If Conteudo <> Constante Then
        Cod_Erro = "8" ' Dados incompat�veis
        Exit Sub
    End If

End Sub

Private Sub Critica_Campo_Contador_NEW(ByVal Conteudo As String, _
                                   ByVal Tipo As String, _
                                   ByVal Qtd_Registros As Integer, _
                                   ByVal Header_Trailler As Integer, _
                                   ByRef Cod_Erro As String)

    ' criticar
    If Not IsNumeric(Conteudo) Then
        Cod_Erro = "1" ' Campo n�o num�rico
        Exit Sub
    Else
        If Tipo = "C1" Then
            If Val(Conteudo) <> (Qtd_Registros - Header_Trailler) Then
                Cod_Erro = "16" ' Quantidade de registros difere do informado
                Exit Sub
            End If
        Else ' "C2"
            If Val(Conteudo) <> Qtd_Registros Then
                Cod_Erro = "16" ' Quantidade de registros difere do informado
                Exit Sub
            End If
        End If
    End If

End Sub

Private Sub Zera_Ult_Chave_Dependentes_NEW( _
ByRef Detalhe_Pai As clDetalhe, _
ByRef Tipos_Detalhe As Collection)

    ' Recursiva, zera as chaves prim�rias dos filhos, pois
    ' o detalhe pai foi incrementado.
    Dim i As Integer, Detalhe_Filho As clDetalhe

    If Detalhe_Pai.Dependentes.Count > 0 Then
        For i = 1 To Detalhe_Pai.Dependentes.Count
            For Each Detalhe_Filho In Tipos_Detalhe
                If Detalhe_Filho.Detalhe_id = Detalhe_Pai.Dependentes(i) Then
                    Detalhe_Filho.Ult_Chave = 0
                    Zera_Ult_Chave_Dependentes_NEW Detalhe_Filho, Tipos_Detalhe
                End If
            Next ' Detalhe_Filho
        Next i ' Loop dos dependentes do Detalhe_Pai
    End If

End Sub

Private Sub Obtem_Chave_NEW( _
ByVal Tipos_Detalhe As Collection, _
ByRef Detalhe As clDetalhe, _
ByRef Valores_Chave_Primaria As String, _
ByRef vl_ID_Layout As String)
    
    On Error GoTo TrataErro
    
    Detalhe.Ult_Chave = Detalhe.Ult_Chave + 1

    ' Zera a chave de cada tabela filha
    Zera_Ult_Chave_Dependentes_NEW Detalhe, co_Detalhe

    ' Monta os valores da chave prim�ria
    Valores_Chave_Primaria = Detalhe.Ult_Chave
    Obtem_Valores_Chaves_Dos_Pais_NEW Tipos_Detalhe, Detalhe, Valores_Chave_Primaria, vl_ID_Layout
            
    Exit Sub
    
TrataErro:

    Call Grava_Trace(Err.Number, Err.Description, "Obtem_Chave_NEW()")
    
    Call GravaLogErro("", GTR.strRemessa, "SQL", "OBTEM_CHAVE", Err.Description)
    rdocn_Interface.Close
    Set rdocn_Interface = Nothing
    Call TerminaSEGBR
    
End Sub

Private Sub Obtem_Valores_Chaves_Dos_Pais_NEW( _
ByVal Tipos_Detalhe As Collection, _
ByRef Detalhe_Filho As clDetalhe, _
ByRef Valores_Chave_Primaria As String, _
ByRef vl_ID_Layout As String)

    ' Recursiva, monta a chave prim�ria de acordo com
    ' os detalhes-pais.

    Set Detalhe_Filho = New clDetalhe
    Set Detalhe_Pai = New clDetalhe

''    For Each Detalhe_Pai In co_dependentes
''       If Detalhe_Pai.Layout_ID = vl_ID_Layout Then 'And Detalhe_Pai.Dependentes(x) > vl_Detalhe_id Then
''
''            Valores_Chave_Primaria = Detalhe_Pai.Ult_Chave & ", " & _
''            Valores_Chave_Primaria
''            vl_Detalhe_id = Detalhe_Pai.Dependentes(x)
''            x = x + 1
''            Exit For ' Pode sair, pois j� encontrou o pai.
''
''       End If
''
''    Next

End Sub
Private Function Formata_Campo(ByRef Campo As Tp_Campo) As String

    Dim Inteiro As Integer, Fracao As Integer
    Dim ano As Integer, mes As Integer, dia As Integer
    Dim Qtd_decimal As Integer
    
    On Error GoTo TrataErro
    
    With Campo
    
    .Tipo = UCase(Campo.Tipo)
    .Formato = UCase(Campo.Formato)
    
    If (.Tipo = "CT") Or (.Tipo = "AN") Or (.Tipo = "C1") Or (.Tipo = "C2") Then
        Formata_Campo = .Conteudo
    ElseIf .Tipo = "DT" Then
        If Trim(.Conteudo) = "" Then
           Formata_Campo = ""
        Else
            If InStr(.Formato, "YYYY") > 0 Then  ' Ano com 4 d�gitos.
                ano = Mid(.Conteudo, InStr(.Formato, "Y"), 4)
            Else ' Ano com 2 d�gitos
                ano = Mid(.Conteudo, InStr(.Formato, "Y"), 2)
                If Val(ano) < 10 Then ' Bug do ano 2000 s� coberto at� 2010.
                    ano = "20" & ano
                Else
                    ano = "19" & ano
                End If
            End If
            mes = Mid(.Conteudo, InStr(.Formato, "M"), 2)
            dia = Mid(.Conteudo, InStr(.Formato, "D"), 2)
            ' As datas no SQL Server est�o no formato 'YYYYMMDD' .
            Formata_Campo = Format(ano, "0000") & Format(mes, "00") & Format(dia, "00")
        End If
    ElseIf .Tipo = "NM" Then
        ' Primeiro converte o n�mero para o formato "<Inteiro>.<Fra��o>"
        ' (� o padr�o usado no SQL)
        Formata_Campo = Replace(.Conteudo, .Separador_Milhar, "")
        Formata_Campo = Replace(Formata_Campo, .Separador_Decimal, "")
        Qtd_decimal = Val(Mid(.Formato, InStr(.Formato, ",") + 1))
        If Qtd_decimal > 0 Then
            Formata_Campo = Left(Formata_Campo, Len(Formata_Campo) - Qtd_decimal) _
                            & "." & Right(Formata_Campo, Qtd_decimal)
        End If
    End If

    End With
    
    Exit Function
    
TrataErro:
    'Grava o Log do Erro
    Call GravaLogErro("", "Descri��o: " & Err.Description, "SQL", "FORMATA_CAMPO", Err.Number)
    rdocn_Interface.Close
    Set rdocn_Interface = Nothing
    Call TerminaSEGBR
    
End Function

Private Function Atualiza_SN(ByVal Layout_Id As String, ByVal OSinistro_BB As String, ByVal OCod_Remessa As String, ByVal OEvento_BB As String, ByVal Entrada_GTR_ID As String) As Boolean
       
    Dim SQL As String
    Dim SP As String
    Dim conta   As Integer
    Dim vEvento_original As Integer
    
    On Error GoTo Erro
    
    conta = 0
    
    Atualiza_SN = False

    SP = vSpTratamento 'Codigo do tramento do layout Sp_Tratamento

    SQL = ""
    
    'Atualiza sinistro - antiga fun��o Executa_SP_Inclusao_SEGBR_NEW
    If (glAmbiente_id = 6 Or glAmbiente_id = 7) And COD_EMPRESA = "ABS" Then
    '#If glAmbiente_id = 7 And COD_EMPRESA = "ABS" Then
    
        SQL = "set xact_abort on EXEC " & SP & " "
        SQL = SQL & "'" & Format(Data_Sistema, "yyyymmdd") & "'" ' @dt_evento
        SQL = SQL & ", " & 5 ' @entidade_id (5 = GTR)
        SQL = SQL & ", '" & cUserName & "'" ' @usuario

        Call Grava_Trace(1, "ABS - Executando: " & SQL, "Atualiza_SN() - Entrada_GTR_ID: " & Entrada_GTR_ID)
        '#ALTERACAO CHAMADA
        If glAmbiente_id = 2 Or glAmbiente_id = 3 Or glAmbiente_id = 5 Then
            Call rdocn_Interface.Execute(SQL)
        Else
            Call rdocn_Seg2.Execute(SQL)
        End If
        'Call rdocn_Interface.Execute(SQL)
        Atualiza_SN = True
        
    '#ElseIf glAmbiente_id = 3 And COD_EMPRESA = "AB" Then
    ElseIf (glAmbiente_id = 2 Or glAmbiente_id = 3) And COD_EMPRESA = "AB" Then
        If Val(vSsinistro_bb) = Val("0") Then
            GoTo Erro
        End If
    
        SQL = "set xact_abort on EXEC " & SP & " "
        SQL = SQL & "'" & Format(Data_Sistema, "yyyymmdd") & "'" ' @dt_evento
        SQL = SQL & ", " & 5 ' @entidade_id (5 = GTR)
        SQL = SQL & ", '" & cUserName & "'" ' @usuario

        Call Grava_Trace(1, "AB - Executando: " & SQL, "Atualiza_SN() - Entrada_GTR_ID: " & Entrada_GTR_ID)
        
        'Call rdocn_Interface.Execute(SQL)
        '#ALTERACAO CHAMADA
        If glAmbiente_id = 2 Or glAmbiente_id = 3 Or glAmbiente_id = 5 Then
            Call rdocn_Interface.Execute(SQL)
        Else
            Call rdocn_Seg2.Execute(SQL)
        End If
        Atualiza_SN = True
        
    Else
        Atualiza_SN = False
        
        Call Grava_Trace(1, "N�o atualizou os dados na " & SP & " COD_EMPRESA = ''", "Atualiza_SN() - Entrada_GTR_ID: " & Entrada_GTR_ID)
        
    End If
    
    If Atualiza_SN = True Then
        'Grava documento recebido antiga fun��o Grava_Documento_Recebido_GTR_NEW
        If (OEvento_BB = 2003) Or (OEvento_BB = 2011) Or (OEvento_BB = 2014) Then
           
            SQL = "set XACT_ABORT ON EXEC interface_db.dbo.documentos_recebidos_GTR_spi "
            SQL = SQL & "'" & OCod_Remessa & "'"    '@cod_remessa
            SQL = SQL & ", " & OSinistro_BB         '@sinistro_bb
            SQL = SQL & ", " & OEvento_BB           '@evento_BB
            SQL = SQL & ", '" & cUserName & "'"     '@usuario
            
            Call Grava_Trace(1, "Executando: " & SQL, "Atualiza_SN() - Entrada_GTR_ID: " & Entrada_GTR_ID)
            
            'Call rdocn_Interface.Execute(SQL)
            
            '#ALTERACAO CHAMADA
            If glAmbiente_id = 2 Or glAmbiente_id = 3 Or glAmbiente_id = 5 Then
                Call rdocn_Interface.Execute(SQL)
            Else
                Call rdocn_Seg2.Execute(SQL)
            End If
 
            'Merge da solicita��o 263250
            SQL = "EXEC SEGS11526_SPI "
            SQL = SQL & OSinistro_BB         '@sinistro_bb
            SQL = SQL & ", " & OEvento_BB           '@evento_BB
            SQL = SQL & ", '" & cUserName & "'"     '@usuario
            
            Call Grava_Trace(1, "Executando: " & SQL, "Atualiza_SN() - Entrada_GTR_ID: " & Entrada_GTR_ID)
            
'            Call rdocn_Interface.Execute(SQL)
            '#ALTERACAO CHAMADA
            If glAmbiente_id = 2 Or glAmbiente_id = 3 Or glAmbiente_id = 5 Then
                Call rdocn_Interface.Execute(SQL)
            Else
                Call rdocn_Seg2.Execute(SQL)
            End If

        End If
    
        ' Grava o registro como 't' (transmitido) antiga fun��o Atualiza_Entrada_New
        'SQL = "set XACT_ABORT ON EXEC " & GTR_banco & ".dbo.altera_situacao_entrada_gtr_spu "
        SQL = "set XACT_ABORT ON EXEC interface_db.dbo.altera_situacao_entrada_gtr_spu "
        SQL = SQL & Entrada_GTR_ID
        SQL = SQL & ", 'T'"
        SQL = SQL & ", '" & cUserName & "'"
        
        Call Grava_Trace(1, "Executando: " & SQL, "Atualiza_SN() - Entrada_GTR_ID: " & Entrada_GTR_ID)
        
        'In�cio - Melhorias do SEGBR - Jorge Benko - 18/06/2013
        'Call rdocnAB.Execute(SQL)
        '#ALTERACAO CHAMADA
        If glAmbiente_id = 2 Or glAmbiente_id = 3 Or glAmbiente_id = 5 Then
            Call rdocn_Interface.Execute(SQL)
        Else
            Call rdocn_Seg2.Execute(SQL)
        End If
        'Fim - Melhorias do SEGBR - Jorge Benko - 18/06/2013
 
        '#ALTERACAO CHAMADA
        If glAmbiente_id = 2 Or glAmbiente_id = 3 Or glAmbiente_id = 5 Then
            rdocn_Interface.CommitTrans
            rdocn.CommitTrans
        Else
            rdocn_Seg2.CommitTrans
        End If
'        rdocn_Interface.CommitTrans
'       rdocn.CommitTrans
    Else
        '#ALTERACAO CHAMADA
        If glAmbiente_id = 2 Or glAmbiente_id = 3 Or glAmbiente_id = 5 Then
            rdocn_Interface.RollbackTrans
            rdocn.RollbackTrans
        Else
            rdocn_Seg2.RollbackTrans
        End If
        
        'rdocn_Interface.RollbackTrans
        'rdocn.RollbackTrans
    End If
    
    Exit Function
    
Erro:
    Atualiza_SN = False
    '#ALTERACAO CHAMADA
    If glAmbiente_id = 2 Or glAmbiente_id = 3 Or glAmbiente_id = 5 Then
        rdocn_Interface.RollbackTrans
        rdocn.RollbackTrans
    Else
        rdocn_Seg2.RollbackTrans
    End If
    
    Call Grava_Trace(Err.Number, Err.Description, "Atualiza_SN() - Entrada_GTR_ID: " & Entrada_GTR_ID)

    Call Enviar_Notificacao("SMQP0061", "SMQP0061 - Atualiza_SN()" & vbCrLf & " Erro: " & Err.Number & " Descri��o: " & Err.Description)
    
End Function

Private Function Atualiza_ST(ByVal OSinistro_BB As String, ByVal OCod_Remessa As String, ByVal OEvento_BB As String, ByVal Entrada_GTR_ID As String) As Boolean

Dim SQL As String
Dim evento_gerado As String

'Ricardo Toledo (Confitec) : 03/06/2014 : Inicio
Dim remessa_saida As String
Dim evento_saida As Integer
'Ricardo Toledo (Confitec) : 03/06/2014 : fim
    
    'GENJUNIOR - ALTERANDO TRANSA��ES PARA AB OU ABS
    '    AbrirTransacao (numconexao)
    'rdocn_Interface.BeginTrans
    If glAmbiente_id = 2 Or glAmbiente_id = 3 Or glAmbiente_id = 5 Then
        rdocn_Interface.BeginTrans
    Else
        rdocn_Seg2.BeginTrans
    End If
    'FIM GENJUNIOR
    
    On Error GoTo Erro
    
    Atualiza_ST = False
    
    'In�cio - Melhorias do SEGBR - Jorge Benko - 18/06/2013
    If OEvento_BB = 8199 Then
        
        Atualiza_ST = False
        COD_EMPRESA = "XXX"
      
        SQL = " exec interface_db.dbo.segs9155_sps " & Entrada_GTR_ID
        
        'Set Rs2 = rdocn_Interface.OpenResultset(SQL)
        
        '#ALTERACAO CHAMADA
        If glAmbiente_id = 2 Or glAmbiente_id = 3 Or glAmbiente_id = 5 Then
            Set Rs2 = rdocn_Interface.OpenResultset(SQL)
        Else
            Set Rs2 = rdocn_Seg2.OpenResultset(SQL)
        End If
        
        If Rs2.EOF Then
        
            '------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
            'Ricardo Toledo (Confitec) : 03/06/2014 : Inicio
            'Quando ocorre duplicidade o evento n�o � processado, pois perde a referencia (numero de remessa) no tabel�o, ent�o:
            
            Rs2.Close
            
            'seleciona a remessa e o evento original referente � esse retorno (tabela de saida)
            SQL = "SELECT RTRIM(LTRIM(SUBSTRING(registro, 6, 16))),RTRIM(LTRIM(SUBSTRING(registro, 26, 4))) FROM interface_db.dbo.entrada_gtr_registro_atual_tb WITH (NOLOCK) WHERE entrada_gtr_id = " & Entrada_GTR_ID & " AND reg_cd_tip = 'T'"
            If glAmbiente_id = 2 Or glAmbiente_id = 3 Or glAmbiente_id = 5 Then
                Set Rs2 = rdocn_Interface.OpenResultset(SQL)
            Else
                Set Rs2 = rdocn_Seg2.OpenResultset(SQL)
            End If
            
            remessa_saida = Rs2(0) 'remessa da saida
            evento_saida = Rs2(1) 'evento da saida
            
            Rs2.Close
            
            'Somente efetua esse procedimento se o evento n�o for 1100
            If evento_saida <> 1100 Then
                
                'Marca o evento como cancelado na saida
                SQL = "set XACT_ABORT ON exec interface_db.dbo.SEGS9154_SPU 't', '" & remessa_saida & "', " & evento_saida & ", '" & cUserName & "'"
                If glAmbiente_id = 2 Or glAmbiente_id = 3 Or glAmbiente_id = 5 Then
                    Call rdocn_Interface.Execute(SQL)
                Else
                    Call rdocn_Seg2.Execute(SQL)
                End If
                
                Call Grava_Trace(1, "Cancelando o evento na saida:" & SQL, "Atualiza_SN() - Cod_remessa: " & remessa_saida)
                
                'marcar como processado na entrada
                SQL = "set XACT_ABORT ON EXEC interface_db.dbo.altera_situacao_entrada_gtr_spu "
                SQL = SQL & Entrada_GTR_ID
                SQL = SQL & ", 'T'"
                SQL = SQL & ", '" & cUserName & "'"
                
                Call Grava_Trace(1, "Marcando como processado na Entrada: " & SQL, "Atualiza_SN() - Entrada_GTR_ID: " & Entrada_GTR_ID)
                
                If glAmbiente_id = 2 Or glAmbiente_id = 3 Or glAmbiente_id = 5 Then
                    Call rdocn_Interface.Execute(SQL)
                Else
                    Call rdocn_Seg2.Execute(SQL)
                End If
                
                'Grava um LOG
                Call Grava_Trace(0, "Processado o evento 8199 de remessa em duplicidade (que fez perder a referencia no tabelao) ", "Atualiza_ST() - Entrada_GTR_ID: " & Entrada_GTR_ID)
                
                'atualiza para desbloquear o registro e n�o processar novamente
                Atualiza_ST = True
                
                'commit na transacao, para o registro n�o ficar bloqueado
                If glAmbiente_id = 2 Or glAmbiente_id = 3 Or glAmbiente_id = 5 Then
                    rdocn_Interface.CommitTrans
                Else
                    rdocn_Seg2.CommitTrans
                End If
    
                Exit Function
            Else
                Rs2.Close
                Atualiza_ST = False
                
    '            rdocn_Interface.RollbackTrans
                '#ALTERACAO CHAMADA
                If glAmbiente_id = 2 Or glAmbiente_id = 3 Or glAmbiente_id = 5 Then
                    rdocn_Interface.RollbackTrans
                Else
                    rdocn_Seg2.RollbackTrans
                End If
                
    '            Call Grava_Trace(1, "N�o atualizou Evento 8199 buscou Cod_Empresa na interface_db.dbo.segs9155_sps retornou '' ", "Atualiza_ST() -  Entrada_GTR_ID: " & Entrada_GTR_ID)
                
                COD_EMPRESA = "XXX"
                Exit Function
            End If
            
            'Ricardo Toledo (Confitec) : 03/06/2014 : fim
            '------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        Else
            COD_EMPRESA = Trim(Rs2(0))
            Rs2.Close
        End If
        
        Call Grava_Trace(1, "Evento 8199 buscou Cod_Empresa na interface_db.dbo.segs9155_sps ", "Atualiza_ST() - Entrada_GTR_ID: " & Entrada_GTR_ID)
        
    ElseIf COD_EMPRESA = "" Then 'n�o voltou da proc do carrega grid
          
        SQL = " select distinct entrada_gtr_id "
        SQL = SQL & " from interface_db.dbo.entrada_gtr_atual_tb a With(nolock)"
        SQL = SQL & " inner join seguros_db.dbo.evento_segbr_sinistro_atual_tb b With(nolock)"
        SQL = SQL & " on a.sinistro_bb = b.sinistro_bb"
        SQL = SQL & " where entrada_gtr_id = " & Entrada_GTR_ID
    
        'Set Rs2 = rdocn_Interface.OpenResultset(SQL)
        
        '#ALTERACAO CHAMADA
        If glAmbiente_id = 2 Or glAmbiente_id = 3 Or glAmbiente_id = 5 Then
            Set Rs2 = rdocn_Interface.OpenResultset(SQL)
        Else
            Set Rs2 = rdocn_Seg2.OpenResultset(SQL)
        End If
    
        If Rs2.EOF Then
            Rs2.Close
            Atualiza_ST = False
            
            'rdocn_Interface.RollbackTrans
            
            '#ALTERACAO CHAMADA
            If glAmbiente_id = 2 Or glAmbiente_id = 3 Or glAmbiente_id = 5 Then
                rdocn_Interface.RollbackTrans
            Else
                rdocn_Seg2.RollbackTrans
            End If
            
            Call Grava_Trace(0, "N�O atualizou Evento <> 8199 Cod_Empresa retornou vazio da Proc do Grid buscou na interface_db.dbo.entrada_gtr_atual_tb retornou vazio", "Atualiza_ST() - Entrada_GTR_ID: " & Entrada_GTR_ID)
            
            COD_EMPRESA = "XXX"
            
            Exit Function
        Else
            If glAmbiente_id = 6 Or glAmbiente_id = 7 Then
            '#If glAmbiente_id = 7 Then
                COD_EMPRESA = "ABS"
            ElseIf glAmbiente_id = 2 Or glAmbiente_id = 3 Then
            '#ElseIf glAmbiente_id = 3 Then
                COD_EMPRESA = "AB"
            End If
                
        End If
        
        Call Grava_Trace(1, "Evento <> 8199 Cod_Empresa retornou vazio da Proc do Grid buscou na interface_db.dbo.entrada_gtr_atual_tb", "Atualiza_ST() -  Entrada_GTR_ID: " & Entrada_GTR_ID)
    'Fim - Melhorias do SEGBR - Jorge Benko - 18/06/2013
    End If
    
    If ((glAmbiente_id = 6 Or glAmbiente_id = 7) And COD_EMPRESA = "ABS") Or ((glAmbiente_id = 2 Or glAmbiente_id = 3) And COD_EMPRESA = "AB") Then
    '#If (glAmbiente_id = 7 And COD_EMPRESA = "ABS") Or (glAmbiente_id = 3 And COD_EMPRESA = "AB") Then
        SQL = ""
        'SQL = " set xact_abort on exec " & GTR_banco & ".dbo.processa_entrada_registro_spi " & Entrada_GTR_ID ' JORGE RETORNAR POIS O BANCO QUE ESTA HJ � interface_db
        SQL = " set xact_abort on exec interface_db.dbo.processa_entrada_registro_spi " & Entrada_GTR_ID ' JORGE RETORNAR POIS O BANCO QUE ESTA HJ � interface_db
        '#SQL = " set xact_abort on exec desenv_db..processa_entrada_registro_spi " & Entrada_GTR_ID ' JORGE RETORNAR POIS O BANCO QUE ESTA HJ � interface_db
        
        'Set rs = rdocn_Interface.OpenResultset(SQL) 'jorge benko ficara com RDO ate defini��o da base em ADO
        
        '#ALTERACAO CHAMADA
        If glAmbiente_id = 2 Or glAmbiente_id = 3 Or glAmbiente_id = 5 Then
            Set rs = rdocn_Interface.OpenResultset(SQL)
        Else
            Set rs = rdocn_Seg2.OpenResultset(SQL)
        End If

        If OEvento_BB = 8199 And rs(0) = 0 Then
            Atualiza_ST = True
            
            Call Grava_Trace(1, "Atualizou os dados na interface_db.dbo.processa_entrada_registro_spi", "Atualiza_ST() - Entrada_GTR_ID: " & Entrada_GTR_ID)
        Else
            evento_gerado = rs(0)
            'HENRIQUE H. HENRIQUES - CONFITEC SP - ALTERA��O PARA GARANTIR O PROCESSAMENTO DOS EVENTOS 8199 DE AVISO DE SINISTRO - INI
            
            'SQL = "select evento_id from seguros_db.dbo.evento_segbr_sinistro_atual_tb With(nolock) where evento_id = " & evento_gerado
            
            rs.Close
            
            SQL = "SELECT EVENTO_BB FROM INTERFACE_DB.DBO.ENTRADA_GTR_ATUAL_TB WITH(NOLOCK) WHERE ENTRADA_GTR_ID = " & evento_gerado
            
            If glAmbiente_id = 2 Or glAmbiente_id = 3 Or glAmbiente_id = 5 Then
                Set rs = rdocn_Interface.OpenResultset(SQL)
            Else
                Set rs = rdocn_Seg2.OpenResultset(SQL)
            End If
            
            If Not rs.EOF Then
                If rs(0) = "2000" Then
                   Atualiza_ST = True
                   'Call Grava_Trace(1, "Gerou evento na entrada: " & evento_gerado & " na interface_db.dbo.entrada_gtr_atual_tb", "Atualiza_ST() - Entrada_GTR_ID: " & Entrada_GTR_ID)
                End If
                rs.Close
                Set rs = Nothing
            End If
                        
            If Atualiza_ST = False Then
                SQL = "select evento_id from seguros_db.dbo.evento_segbr_sinistro_atual_tb With(nolock) where evento_id = " & evento_gerado
                
                If glAmbiente_id = 2 Or glAmbiente_id = 3 Or glAmbiente_id = 5 Then
                    Set rs = rdocn_Interface.OpenResultset(SQL)
                Else
                    Set rs = rdocn_Seg2.OpenResultset(SQL)
                End If
            
                If Not rs.EOF Then
                    Atualiza_ST = True
                    rs.Close
                    Set rs = Nothing
                    Call Grava_Trace(1, "Gerou o Evento_BB: " & evento_gerado & " na seguros_db.dbo.evento_segbr_sinistro_atual_tb", "Atualiza_ST() - Entrada_GTR_ID: " & Entrada_GTR_ID)
                Else
                    Call Grava_Trace(1, "Retornou erro na processa_entrada_registro_spi", "Atualiza_ST() - Entrada_GTR_ID: " & Entrada_GTR_ID)
                End If
            End If
            'HENRIQUE H. HENRIQUES - CONFITEC SP - ALTERA��O PARA GARANTIR O PROCESSAMENTO DOS EVENTOS 8199 DE AVISO DE SINISTRO - FIM
        End If
    Else
        Atualiza_ST = False
        
        COD_EMPRESA = "XXX"
        
    End If
    
    If Atualiza_ST = True Then
        'Grava documento recebido antiga fun��o Grava_Documento_Recebido_GTR_NEW
        If (OEvento_BB = 2003) Or (OEvento_BB = 2011) Or (OEvento_BB = 2014) Then
           
            If glAmbiente_id = 2 Or glAmbiente_id = 3 Or glAmbiente_id = 5 Then
                SQL = " set xact_abort on EXEC interface_db.dbo.documentos_recebidos_GTR_spi "
                SQL = SQL & "'" & OCod_Remessa & "'"    '@cod_remessa
                SQL = SQL & ", " & OSinistro_BB         '@sinistro_bb
                SQL = SQL & ", " & OEvento_BB           '@evento_BB
                SQL = SQL & ", '" & cUserName & "'"     '@usuario
            Else
                SQL = " set xact_abort on EXEC abss.interface_db.dbo.documentos_recebidos_GTR_spi "
                SQL = SQL & "'" & OCod_Remessa & "'"    '@cod_remessa
                SQL = SQL & ", " & OSinistro_BB         '@sinistro_bb
                SQL = SQL & ", " & OEvento_BB           '@evento_BB
                SQL = SQL & ", '" & cUserName & "'"     '@usuario
            End If
            
            'Call rdocn_Interface.Execute(SQL)
            
'            '#ALTERACAO CHAMADA
            If glAmbiente_id = 2 Or glAmbiente_id = 3 Or glAmbiente_id = 5 Then
                Call rdocn_Interface.Execute(SQL)
            Else
                Call rdocn_Seg2.Execute(SQL)
            End If
'
            Call Grava_Trace(1, "Atualizou os dados na documentos_recebidos_GTR_spi para os eventos (2003,2011,2014)", "Atualiza_ST() - Entrada_GTR_ID: " & Entrada_GTR_ID)
                            
            'Merge da solicita��o 263250
            SQL = "EXEC SEGS11526_SPI "
            SQL = SQL & OSinistro_BB         '@sinistro_bb
            SQL = SQL & ", " & OEvento_BB           '@evento_BB
            SQL = SQL & ", '" & cUserName & "'"     '@usuario
            'Call rdocn_Interface.Execute(SQL)
            
            '#ALTERACAO CHAMADA
            If glAmbiente_id = 2 Or glAmbiente_id = 3 Or glAmbiente_id = 5 Then
                Call rdocn_Interface.Execute(SQL)
            Else
                Call rdocn_Seg2.Execute(SQL)
            End If

            Call Grava_Trace(1, "Executou a Proc SEGS11526_SPI - da Solicita��o 263250", "Atualiza_SN() - Entrada_GTR_ID: " & Entrada_GTR_ID)

        End If
    
        ' Grava o registro como 't' (transmitido) antiga fun��o Atualiza_Entrada_New
        'SQL = "set XACT_ABORT ON EXEC " & GTR_banco & ".dbo.altera_situacao_entrada_gtr_spu "
        SQL = "set XACT_ABORT ON EXEC interface_db.dbo.altera_situacao_entrada_gtr_spu "
        SQL = SQL & Entrada_GTR_ID
        SQL = SQL & ", 'T'"
        SQL = SQL & ", '" & cUserName & "'"
        
        'In�cio - Melhorias do SEGBR - Jorge Benko - 18/06/2013
        '#ALTERACAO CHAMADA
        If glAmbiente_id = 2 Or glAmbiente_id = 3 Or glAmbiente_id = 5 Then
            Call rdocn_Interface.Execute(SQL)
        Else
            Call rdocn_Seg2.Execute(SQL)
        End If
        'Fim - Melhorias do SEGBR - Jorge Benko - 18/06/2013

        Call Grava_Trace(1, "Atualizou a situa��o 'T' na altera_situacao_entrada_gtr_spu", "Atualiza_ST() - Entrada_GTR_ID: " & Entrada_GTR_ID)
        
        'rdocn_Interface.CommitTrans
        
        '#ALTERACAO CHAMADA
        If glAmbiente_id = 2 Or glAmbiente_id = 3 Or glAmbiente_id = 5 Then
            rdocn_Interface.CommitTrans
        Else
            rdocn_Seg2.CommitTrans
        End If
    Else
        'rdocn_Interface.RollbackTrans
        
        '#ALTERACAO CHAMADA
        If glAmbiente_id = 2 Or glAmbiente_id = 3 Or glAmbiente_id = 5 Then
            rdocn_Interface.RollbackTrans
        Else
            rdocn_Seg2.RollbackTrans
        End If
        
    End If
    
    Exit Function
    
Erro:

    Atualiza_ST = False
    'rdocn_Interface.RollbackTrans
    
    '#ALTERACAO CHAMADA
    If glAmbiente_id = 2 Or glAmbiente_id = 3 Or glAmbiente_id = 5 Then
        rdocn_Interface.RollbackTrans
    Else
        rdocn_Seg2.RollbackTrans
    End If
    
    Call Grava_Trace(Err.Number, Err.Description, "Atualiza_ST() - Entrada_GTR_ID: " & Entrada_GTR_ID)

    Call Enviar_Notificacao("SMQP0061", "SMQP0061 - Atualiza_ST()" & vbCrLf & " Erro: " & Err.Number & " Descri��o: " & Err.Description)

End Function
'Gera o controle de erros na tabela log_erro_tb
Private Function Grava_Trace(vl_Codigo_Erro As Long, vl_Mensagem As String, vl_Rotina As String, Optional ambiente_id As Integer) As Boolean
    Dim SQL As String
    Dim num As Integer
    Dim Diretorio As String
    Dim ArqOrigem As String
    
    Diretorio = "C:\log_mq\services\Trace\"
    '\\10.0.35.49\log_mq\services\Trace\

    If Valida_Trace Then
        SQL = "set xact_abort on EXEC controle_sistema_db.dbo.log_erro_spi "
        SQL = SQL & FormatoSQL(vl_Codigo_Erro)
        SQL = SQL & ", " & FormatoSQL(vl_Mensagem)
        SQL = SQL & ", 'SEGBR', 'SMQP0061', 'SMQP0061' "
        SQL = SQL & ", 'VB'"
        SQL = SQL & ", " & FormatoSQL(vl_Rotina)
        SQL = SQL & ", 'SMQP0061'"
    
        'Call rdocn_Interface.Execute(SQL)
        
        '#ALTERACAO CHAMADA
        If glAmbiente_id = 2 Or glAmbiente_id = 3 Or glAmbiente_id = 5 Then
            Call rdocn_Interface.Execute(SQL)
        Else
            Call rdocn_Seg2.Execute(SQL)
        End If
    End If
    
    'Trace_SMQP0061_20160316.Text
    ArqOrigem = Diretorio & "Trace_SMQP0061" & "_" & Format(Date, "yyyymmdd") & ".text"
    num = FreeFile
    Open ArqOrigem For Append As #num
    If ambiente_id = 0 Then
        Print #num, Now() & " - " & vl_Mensagem & " - Ambiente_id: " & glAmbiente_id
    Else
        Print #num, Now() & " - " & vl_Mensagem & " - Ambiente_id: " & ambiente_id
    End If
    Close #num

End Function
'Valida se � para gerar o controle de erro na log_erro_tb
Private Function Valida_Trace() As Boolean
    Dim SQL As String
    
    SQL = "select trace "
    SQL = SQL & "from interface_dados_db.dbo.parametrizacao_servico_tb With(nolock)"
    SQL = SQL & " where ServiceName = 'SMQP0061'"
    SQL = SQL & " and trace='TRUE'"
    'Set rs = rdocn_Interface.OpenResultset(SQL)
    
        If glAmbiente_id = 2 Or glAmbiente_id = 3 Or glAmbiente_id = 5 Then
            Set rs = rdocn_Interface.OpenResultset(SQL)
        Else
            Set rs = rdocn_Seg2.OpenResultset(SQL)
        End If
    
    If Not rs.EOF Then
        Valida_Trace = True
    Else
        Valida_Trace = False
    End If
    
    rs.Close
    
End Function

'GENJUNIOR - M�TODO DE LOG DE EXECU��O
Public Sub LogExecution(ByVal tpLog As Integer, ByVal serviceName As String, ByVal tpABS As String, Optional Ambiente As String)

Dim SQL As String

On Error GoTo TrataErro

    SQL = ""
    SQL = SQL & " EXEC INTERFACE_DADOS_DB.DBO.SMQS0133_SPU " & tpLog & ", '" & serviceName & "','" & tpABS & "' " & vbNewLine
    
    Set rs = rdocn_Interface.OpenResultset(SQL)
    
    Call Grava_Trace(0, "Tipo Processamento: " & tpLog & " - Ambiente: " & Ambiente, "SMQP0061")
    'Print #num, 16/03/2016 18:17:55 - Tipo Processamento: " & tpLog & " -  Ambiente: " & Ambiente

Exit Sub

TrataErro:

    Call MensagemBatch("Rotina: LogExecution. O programa ser� cancelado.", vbOKOnly, "Erro", True, Me)
    Call TerminaSEGBR

End Sub

Private Sub TerminateProcess(app_exe As String)
    Dim Process As Object
    For Each Process In GetObject("winmgmts:").ExecQuery("Select Name from Win32_Process Where Name = '" & app_exe & "'")
        Process.Terminate
    Next
End Sub

