VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.2#0"; "MSCOMCTL.OCX"
Begin VB.Form frmAgendamento 
   ClientHeight    =   2730
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   5685
   Icon            =   "frmAgendamento.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   2730
   ScaleWidth      =   5685
   StartUpPosition =   3  'Windows Default
   Begin VB.Frame Frame2 
      Caption         =   "Logs"
      Height          =   1560
      Left            =   90
      TabIndex        =   3
      Top             =   135
      Width           =   5490
      Begin VB.Label lblFim 
         BorderStyle     =   1  'Fixed Single
         Height          =   345
         Left            =   2640
         TabIndex        =   9
         Top             =   690
         Width           =   2055
      End
      Begin VB.Label lblInicio 
         BorderStyle     =   1  'Fixed Single
         Height          =   345
         Left            =   2640
         TabIndex        =   8
         Top             =   300
         Width           =   2055
      End
      Begin VB.Label Label3 
         Alignment       =   1  'Right Justify
         Caption         =   "Fim:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   1920
         TabIndex        =   7
         Top             =   765
         Width           =   675
      End
      Begin VB.Label Label2 
         Alignment       =   1  'Right Justify
         Caption         =   "In�cio:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   1860
         TabIndex        =   6
         Top             =   375
         Width           =   735
      End
      Begin VB.Label lblAgendamentos 
         BorderStyle     =   1  'Fixed Single
         Height          =   345
         Left            =   2640
         TabIndex        =   5
         Top             =   1080
         Width           =   2055
      End
      Begin VB.Label label1 
         Alignment       =   1  'Right Justify
         Caption         =   "Agendamentos Efetuados:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   270
         TabIndex        =   4
         Top             =   1140
         Width           =   2325
      End
   End
   Begin VB.CommandButton cmdAgendar 
      Caption         =   "Agendar"
      Height          =   375
      Left            =   1845
      TabIndex        =   1
      Top             =   1890
      Width           =   1770
   End
   Begin VB.CommandButton cmdSair 
      Caption         =   "Sair"
      Height          =   375
      Left            =   3810
      TabIndex        =   0
      Top             =   1890
      Width           =   1770
   End
   Begin MSComctlLib.StatusBar StbRodape 
      Align           =   2  'Align Bottom
      Height          =   315
      Left            =   0
      TabIndex        =   2
      Top             =   2415
      Width           =   5685
      _ExtentX        =   10028
      _ExtentY        =   556
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   3
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   6174
            MinWidth        =   6174
            Text            =   "Mensagem"
            TextSave        =   "Mensagem"
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   6
            Alignment       =   1
            AutoSize        =   2
            Object.Width           =   1376
            MinWidth        =   88
            Text            =   "Data do sistema"
            TextSave        =   "21/05/20"
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            AutoSize        =   2
            Object.Width           =   1111
            MinWidth        =   88
            Text            =   "Usu�rio"
            TextSave        =   "Usu�rio"
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "frmAgendamento"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmdAgendar_Click()

Call AgendarParcelas

End Sub

Private Sub Form_Load()

On Local Error GoTo Trata_Erro
'Verifica se foi executado pelo Control-M
CTM = Verifica_Origem_ControlM(Command)


  'Verificando permiss�o de acesso � aplica��o'''''''''''''''''''''''''''''''''
  'Pula se for executado pelo control-m
  If CTM = False Then
    If Not Trata_Parametros(Command) Then
'       Call FinalizarAplicacao
    End If
  End If

'  'Retirar antes da libera��o '''''''''''''''''''''''''''''''''''''''''''''''''
  cUserName = "99988877714"
 glAmbiente_id = 3

  'Obtendo a data operacional '''''''''''''''''''''''''''''''''''''''''''''''''
  Call ObterDataSistema("SEGBR")

  'Configurando interface''''''''''''''''''''''''''''''''''''''''''''''''''''''
  Call CentraFrm(Me)
  
  'Atualizando Interface ''''''''''''''''''''''''''''''''''''''''''''''''''''''
  Me.Caption = "Agendamento de documentos com cobran�as pendentes " & ExibirAmbiente(glAmbiente_id)
  
  ' Iniciando o processo automaticamente caso seja uma chamada do Scheduler '''''''''''''''
  If Obtem_agenda_diaria_id(Command) > 0 Or CTM = True Then
    Me.Show
    Call cmdAgendar_Click
    FinalizarAplicacao
  End If
  
  Exit Sub

Trata_Erro:

    Call TratarErro("Form_Load", Me.name)

End Sub

Private Sub cmdSair_Click()

On Error GoTo Trata_Erro

    Call FinalizarAplicacao
    
    Exit Sub

Trata_Erro:
    Call TratarErro("cmdSair_Click", Me.name)
    
End Sub

Private Sub Form_Unload(Cancel As Integer)

On Error GoTo Trata_Erro

    Call FinalizarAplicacao
    
    Exit Sub

Trata_Erro:
    Call TratarErro("Form_Unload", Me.name)

End Sub

Sub AgendarParcelas()

Dim oFaturamento    As Object
Dim lConexao        As Long
  
Dim sMensagem       As String
Dim lQtdAgendamento As Long
  
On Error GoTo Trata_Erro
  
  'Tratamento para o Scheduler ''''''''''''''''''''''''''''''
  'InicializaParametrosExecucaoBatch Me
  
  'Atualizando interface ''''''''''''''''''''''''''''''''''''
  MousePointer = vbHourglass
  cmdAgendar.Enabled = False
  lblInicio.Caption = Now
  sMensagem = "Agendando..."
  StbRodape.Panels(1).Text = sMensagem
  DoEvents
  
  'Instanciando a classe de faturamento '''''''''''''''''''''
  Set oFaturamento = CreateObject("SEGL0143.cls0128")
  
  'Gerando Agendamentos '''''''''''''''''''''''''''''''''''''
  Call oFaturamento.GerarAgendamentoCobranca("SEGBR", _
                                             App.Title, _
                                             App.FileDescription, _
                                             glAmbiente_id, _
                                             lQtdAgendamento, _
                                             cUserName, _
                                             0, _
                                             0, _
                                             0, _
                                             "", _
                                             Data_Sistema)
    
  'Destruindo objetos '''''''''''''''''''''''''''''''''''''''
  Set oFaturamento = Nothing

  ' Atualizando interface '''''''''''''''''''''''''''''''''''
  lblFim.Caption = Now
  lblAgendamentos.Caption = lQtdAgendamento
  MousePointer = vbNormal
  
  If lQtdAgendamento > 0 Then
    sMensagem = "Processamento realizado com sucesso."
  Else
    sMensagem = "Nenhum documento agendado."
  End If
  
  StbRodape.Panels(1).Text = sMensagem
  
  'Logando o n�mero de registros processados - Scheduler ''''
'  Call goProducao.AdicionaLog(1, lQtdAgendamento)
  
  'grava log arquivo CTM
    If CTM = True Then
        Call GravaLogCTM("OK - ", lQtdAgendamento, "", "")
    End If

  
    'Pula de for executado pelo Control-M
    If CTM = False Then
'        Call goProducao.finaliza
    End If


Exit Sub

Trata_Erro:
    Call TratarErro("AgendarParcelas", Me.name)
    Call FinalizarAplicacao

End Sub


