Attribute VB_Name = "Local"
Public Sub adSQL(ByRef sSQL As String, ByVal sCadeia As String)
   sSQL = sSQL & sCadeia & vbNewLine
End Sub

Function MudaVirgulaParaPonto(ByVal valor As String) As String

    If InStr(valor, ",") = 0 Then
       MudaVirgulaParaPonto = valor
    Else
      valor = Mid$(valor, 1, InStr(valor, ",") - 1) + "." + Mid$(valor, InStr(valor, ",") + 1)
      MudaVirgulaParaPonto = valor
    End If

End Function
