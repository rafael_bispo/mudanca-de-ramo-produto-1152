VERSION 5.00
Begin VB.Form frmGeraArquivo 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "PRDP0002 - Gerar Arquivo MAPA"
   ClientHeight    =   2715
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   7260
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   ScaleHeight     =   2715
   ScaleWidth      =   7260
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame Frame1 
      Height          =   1815
      Left            =   120
      TabIndex        =   2
      Top             =   120
      Width           =   6975
      Begin VB.ComboBox cmbArquivo 
         Height          =   315
         ItemData        =   "frmGeraArquivo.frx":0000
         Left            =   2040
         List            =   "frmGeraArquivo.frx":0019
         Style           =   2  'Dropdown List
         TabIndex        =   11
         Tag             =   "1"
         Top             =   240
         Width           =   3735
      End
      Begin VB.Label lblVersao 
         BackColor       =   &H80000009&
         BorderStyle     =   1  'Fixed Single
         Height          =   285
         Left            =   2040
         TabIndex        =   7
         Top             =   780
         Width           =   1425
      End
      Begin VB.Label lblProcessados 
         BackColor       =   &H80000009&
         BorderStyle     =   1  'Fixed Single
         Height          =   285
         Left            =   2040
         TabIndex        =   6
         Top             =   1260
         Width           =   1425
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Arquivo.............................."
         Height          =   195
         Left            =   150
         TabIndex        =   5
         Top             =   390
         Width           =   1890
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "Vers�o..............................."
         Height          =   195
         Left            =   150
         TabIndex        =   4
         Top             =   870
         Width           =   1890
      End
      Begin VB.Label Label3 
         AutoSize        =   -1  'True
         Caption         =   "Registros Processados........."
         Height          =   195
         Left            =   150
         TabIndex        =   3
         Top             =   1350
         Width           =   1965
      End
   End
   Begin VB.CommandButton cmdOk 
      Caption         =   "Ok"
      Default         =   -1  'True
      Height          =   375
      Left            =   6045
      TabIndex        =   0
      Top             =   2160
      Width           =   1095
   End
   Begin VB.CommandButton cmdSair 
      Caption         =   "&Sair"
      Height          =   375
      Left            =   4935
      TabIndex        =   1
      Top             =   2160
      Width           =   1095
   End
   Begin VB.DriveListBox Drive1 
      Height          =   315
      Left            =   1080
      TabIndex        =   8
      Top             =   1440
      Width           =   975
   End
   Begin VB.DirListBox dirArquivo 
      Height          =   315
      Left            =   2640
      TabIndex        =   9
      Top             =   1560
      Width           =   615
   End
   Begin VB.FileListBox lstArquivo 
      Height          =   285
      Left            =   3240
      TabIndex        =   10
      Top             =   1560
      Width           =   1575
   End
End
Attribute VB_Name = "frmGeraArquivo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Const ARQUIVO_PLANILHA_SUBVENCAO As String = "ALIANCA_SEGA9031_"
Private Const ARQUIVO_RETORNO_PLANILHA_SUBVENCAO As String = "ALIANCA_SEGA9034_"

'bcarneiro - 11/10/2007 - Planilha de Cancelamento - Demanda 223914
Private Const ARQUIVO_CANCELAMENTO_SUBVENCAO As String = "ALIANCA_SEGA9064_"
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

'lrocha 03/07/2008 - Demanda 380818
Private Const ARQUIVO_PLANILHA_SUBVENCAO_ESTADUAL As String = "ALIANCA_SEGA9065_"
Private Const ARQUIVO_RETORNO_PLANILHA_SUBVENCAO_ESTADUAL As String = "ALIANCA_SEGA9066_"
Private Const ARQUIVO_CANCELAMENTO_SUBVENCAO_ESTADUAL As String = "ALIANCA_SEGA9067_"
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

'mathayde  19/06/2009
'Demanda 930288 - Gerar Arquivo Mapa (Sinistro Agr�cola)

Private Const ARQUIVO_AVISO_SUBVENCAO_ESTADUAL As String = "ALIANCA_SEGA9084"




Private Sub Processar()

Dim OArquivo As Object
Dim sCaminhoArquivo As String
Dim sCaminhoArquivoAux As String
Dim sProcedure As String
Dim lTotalRegistros As Long
Dim iRemessa As Integer
Dim bArquivoGerado As Boolean
Dim sArquivo As String
Dim sCaminhoGerado As String
Dim iContArquivo As Integer

Dim iLinhaComboArquivo As Integer

'mathayde  19/06/2009
'Demanda 930288 - Gerar Arquivo Mapa (Sinistro Agr�cola)
Dim sDtProcessamento As String
Dim sDtUltimoProcessamento As String
'sDtProcessamento = Format(Date, "yyyymm") - 1

On Error GoTo Trata_Erro

'    Call InicializaParametrosExecucaoBatch(Me)
        
    'Obtendo a data operacional ''''''''''''''''''''''''''''''''''''''''''''
    Call ObterDataSistema("SEGBR")
        
        'JFILHO - CONFITEC - FLOW 2839253 - Corrigindo erro de l�gica ao tentar obter o ano/m�s imediatamente anterior ao atual
        sDtProcessamento = Format(DateAdd("m", -1, Data_Sistema), "yyyymm")


    Screen.MousePointer = vbHourglass
    
    'lrocha 04/07/2008 - vari�vel recebe index da combo para passar como par�metro
    If cmbArquivo.ListIndex = -1 Then
        Call MensagemBatch("N�o foi selecionado o tipo do arquivo")
        Exit Sub
    Else
        iLinhaComboArquivo = cmbArquivo.ListIndex
    End If
    
    
    bArquivoGerado = False
    
    'bcarneiro - 11/10/2007 - Planilha de Cancelamento - Demanda 223914
    'If cmbArquivo.ListIndex = 0 Then
    '    sCaminhoArquivo = LerArquivoIni("PRODUCAO", "ARQUIVO_PLANILHA_SUBVENCAO")
    'Else
    '    sCaminhoArquivo = LerArquivoIni("PRODUCAO", "ARQUIVO_PLANILHA_RETORNO_SUBVENCAO")
    'End If
        
    'SELECIONA CAMINHO PARA ARQUIVO
    Select Case iLinhaComboArquivo 'cmbArquivo.ListIndex
    
        Case 0 'SEGA9031
            sCaminhoArquivo = LerArquivoIni("PRODUCAO", "ARQUIVO_PLANILHA_SUBVENCAO")
            
        Case 1 'SEGA9034
            sCaminhoArquivo = LerArquivoIni("PRODUCAO", "ARQUIVO_PLANILHA_RETORNO_SUBVENCAO")
            
        Case 2 'SEGA9064 - Arquivo Cancelamento
            sCaminhoArquivo = LerArquivoIni("PRODUCAO", "ARQUIVO_PLANILHA_CANCELAMENTO_SUBVENCAO")
            
        Case 3 'SEGA9065 - Propostas consulta subven��o estadual
            sCaminhoArquivo = LerArquivoIni("PRODUCAO", "ARQUIVO_PLANILHA_SUBVENCAO")
            
        Case 4 'SEGA9066 - Propostas emitidas - SUBVEN��O ESTADUAL
            sCaminhoArquivo = LerArquivoIni("PRODUCAO", "ARQUIVO_PLANILHA_RETORNO_SUBVENCAO")
            
        Case 5 'SEGA9067 - Propostas Canceladas - SUBVEN��O ESTADUAL
            sCaminhoArquivo = LerArquivoIni("PRODUCAO", "ARQUIVO_PLANILHA_CANCELAMENTO_SUBVENCAO")
        
        'mathayde  19/06/2009
        'Demanda 930288 - Gerar Arquivo Mapa (Sinistro Agr�cola)
        Case 6 'SEGA9084 - Arquivo sinistro agr�cola
            sCaminhoArquivo = LerArquivoIni("PRODUCAO", "ARQUIVO_AVISO_SUBVENCAO_ESTADUAL")
            
    End Select
    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    'sCaminhoArquivo = "D:\ecosta\Desktop\Exe\Prdp0002\arq_teste"
    
    Set OArquivo = CreateObject("SEGL0284.Cls00459")
     
    Call OArquivo.SetAmbiente(gsSIGLASISTEMA, _
                              glAmbiente_id, _
                              App.Title, _
                              App.FileDescription)
                              
    'VERIFICA SE O ARQUIVO J� FOI ENVIADO, CASO SIM, MOVE O MESMO PARA PASTA ENVIADOS
    'TESTE
    sCaminhoArquivo = "C:\"
    If Trim(sCaminhoArquivo) <> "" Then
    
        sCaminhoGerado = sCaminhoArquivo
        dirArquivo.PATH = sCaminhoArquivo
        'lstArquivo.Pattern = ARQUIVO_PLANILHA_SUBVENCAO & "*.xls"
        lstArquivo.PATH = sCaminhoGerado
                
        For iContArquivo = 0 To lstArquivo.ListCount - 1

            Call OArquivo.MoverArquivoGerado(gsSIGLASISTEMA, _
                                App.Title, _
                                App.FileDescription, _
                                glAmbiente_id, _
                                sCaminhoArquivo, _
                                lstArquivo.List(iContArquivo))

        Next iContArquivo

    Else

        Call MensagemBatch("N�o foi encontrada nenhuma refer�ncia ao path de " & _
                           "cria��o dessa planilha!. O programa ser� abortado.")
        Call FinalizarAplicacao

    End If
                 
                 
    'bcarneiro - 11/10/2007 - Planilha de Cancelamento - Demanda 223914
'    If cmbArquivo.ListIndex = 0 Then
'
'        Call OArquivo.GerarPlanilha(gsSIGLASISTEMA, _
'                                    App.Title, _
'                                    App.FileDescription, _
'                                    glAmbiente_id, _
'                                    ARQUIVO_PLANILHA_SUBVENCAO, _
'                                    sCaminhoArquivo, _
'                                    cUserName, _
'                                    Data_Sistema, _
'                                    lTotalRegistros, _
'                                    iRemessa, _
'                                    bArquivoGerado)
'
'    Else
'
'        Call OArquivo.GerarPlanilhaRetorno(gsSIGLASISTEMA, _
'                                    App.Title, _
'                                    App.FileDescription, _
'                                    glAmbiente_id, _
'                                    ARQUIVO_RETORNO_PLANILHA_SUBVENCAO, _
'                                    sCaminhoArquivo, _
'                                    cUserName, _
'                                    Data_Sistema, _
'                                    lTotalRegistros, _
'                                    iRemessa, _
'                                    bArquivoGerado)
'
'    End If
    
    'GERA ARQVUIO
    Select Case iLinhaComboArquivo  'cmbArquivo.ListIndex
    
        Case 0 'SEGA9031
        
            Call OArquivo.GerarPlanilha(gsSIGLASISTEMA, _
                                        App.Title, _
                                        App.FileDescription, _
                                        glAmbiente_id, _
                                        ARQUIVO_PLANILHA_SUBVENCAO, _
                                        sCaminhoArquivo, _
                                        cUserName, _
                                        Data_Sistema, _
                                        lTotalRegistros, _
                                        iRemessa, _
                                        bArquivoGerado)
        
            'GerarRemessaPecuario: subven��o 1 = Federal / 2 = Estadual
            Call OArquivo.GerarRemessaPecuario(gsSIGLASISTEMA, _
                                        App.Title, _
                                        App.FileDescription, _
                                        glAmbiente_id, _
                                        "SEGA9031", _
                                        1, _
                                        cUserName)
                                        
            'IOBEICA - NTENDENCIA - C00205807
            Call OArquivo.RecusaSubvencaoAutomatica(gsSIGLASISTEMA, _
                                        App.Title, _
                                        App.FileDescription, _
                                        glAmbiente_id, _
                                        1, _
                                        cUserName)
        
        Case 1 'SEGA9034
        
            Call OArquivo.GerarPlanilhaRetorno(gsSIGLASISTEMA, _
                                               App.Title, _
                                               App.FileDescription, _
                                               glAmbiente_id, _
                                               ARQUIVO_RETORNO_PLANILHA_SUBVENCAO, _
                                               sCaminhoArquivo, _
                                               cUserName, _
                                               Data_Sistema, _
                                               lTotalRegistros, _
                                               iRemessa, _
                                               bArquivoGerado)
        
        'GerarRemessaPecuario: subven��o 1 = Federal / 2 = Estadual
            Call OArquivo.GerarRemessaPecuario(gsSIGLASISTEMA, _
                                        App.Title, _
                                        App.FileDescription, _
                                        glAmbiente_id, _
                                        "SEGA9034", _
                                        1, _
                                        cUserName)
        
        
        Case 2 'SEGA9064 - Arquivo Cancelamento
    
            Call OArquivo.GerarPlanilhaCancelamento(gsSIGLASISTEMA, _
                                                    App.Title, _
                                                    App.FileDescription, _
                                                    glAmbiente_id, _
                                                    ARQUIVO_CANCELAMENTO_SUBVENCAO, _
                                                    sCaminhoArquivo, _
                                                    cUserName, _
                                                    Data_Sistema, _
                                                    lTotalRegistros, _
                                                    iRemessa, _
                                                    bArquivoGerado)
                                                    
        Case 3, 4, 5 'SEGA9065 - Propostas consulta subven��o estadual
        
            Call GerarPlanilhaEstadualSiglaUF(OArquivo, _
                                              iLinhaComboArquivo, _
                                              sCaminhoArquivo, _
                                              lTotalRegistros, _
                                              iRemessa, _
                                              bArquivoGerado)
                                              
            'GerarRemessaPecuario: subven��o 1 = Federal / 2 = Estadual
            Call OArquivo.GerarRemessaPecuario(gsSIGLASISTEMA, _
                                        App.Title, _
                                        App.FileDescription, _
                                        glAmbiente_id, _
                                        "SEGA9065", _
                                        2, _
                                        cUserName)
            
            'IOBEICA - NTENDENCIA - C00205807
            Call OArquivo.RecusaSubvencaoAutomatica(gsSIGLASISTEMA, _
                                        App.Title, _
                                        App.FileDescription, _
                                        glAmbiente_id, _
                                        2, _
                                        cUserName)
                                                                                            
        
        'mathayde  19/06/2009
        'Demanda 930288 - Gerar Arquivo Mapa (Sinistro Agr�cola)

        Case 6 'SEGA9084 - Arquivo sinistro agr�cola
            
            'consulta �ltimo processamento
            sDtUltimoProcessamento = OArquivo.ConsultaDataProcessamento
            
            If sDtProcessamento > sDtUltimoProcessamento Then
                
                Call OArquivo.GerarPlanilhaSinistroAgricola(gsSIGLASISTEMA, _
                                                            App.Title, _
                                                            App.FileDescription, _
                                                            glAmbiente_id, _
                                                            ARQUIVO_AVISO_SUBVENCAO_ESTADUAL, _
                                                            sCaminhoArquivo, _
                                                            cUserName, _
                                                            Data_Sistema, _
                                                            lTotalRegistros, _
                                                            iRemessa, _
                                                            bArquivoGerado, _
                                                            sDtProcessamento)
            
            End If
            
        
    End Select
    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    
    lblVersao.Caption = Format(iRemessa, "0000")
    lblProcessados.Caption = lTotalRegistros
     
    If Not bArquivoGerado Then
        'mathayde  19/06/2009
        'Demanda 930288 - Gerar Arquivo Mapa (Sinistro Agr�cola)

        If iLinhaComboArquivo = 6 Then
            Call goProducao.AdicionaLog(1, "Remessa do per�odo " & Mid(sDtProcessamento, 5, 2) & "/" & Mid(sDtProcessamento, 1, 4) & " j� foi processada")
            Call goProducao.AdicionaLog(2, "0")
            '###########################grava log arquivo CTM
            If CTM = True Then
                Call GravaLogCTM("OK - ", "Remessa do per�odo " & Mid(sDtProcessamento, 5, 2) & "/" & Mid(sDtProcessamento, 1, 4) & " j� foi processada", "", "")
            End If
            '################################################
        Else
'            Call goProducao.AdicionaLog(1, "Arquivo n�o gerado")
'            Call goProducao.AdicionaLog(2, "0")
 '           '###########################grava log arquivo CTM
  '          If CTM = True Then
   '             Call GravaLogCTM("OK - ", "Arquivo n�o gerado", "0", "")
    '        End If
            '################################################
        End If
    Else
        
        lblProcessados.Caption = lTotalRegistros
        
        'bcarneiro - 11/10/2007 - Planilha de Cancelamento - Demanda 223914
        'If cmbArquivo.ListIndex = 0 Then
        '    sArquivo = ARQUIVO_PLANILHA_SUBVENCAO + Format(iRemessa, "0000") + ".XLS"
        'Else
        '    sArquivo = ARQUIVO_RETORNO_PLANILHA_SUBVENCAO + Format(iRemessa, "0000") + ".XLS"
        'End If
        
        Select Case cmbArquivo.ListIndex
    
            Case 0 'SEGA9031
                sArquivo = ARQUIVO_PLANILHA_SUBVENCAO + Format(iRemessa, "0000") + ".XLS"
                
            Case 1 'SEGA9034
                sArquivo = ARQUIVO_RETORNO_PLANILHA_SUBVENCAO + Format(iRemessa, "0000") + ".XLS"
                
            Case 2 'SEGA9064 - Arquivo Cancelamento
                sArquivo = ARQUIVO_CANCELAMENTO_SUBVENCAO + Format(iRemessa, "0000") + ".XLS"
                
            Case 3 'SEGA9065 - Propostas consulta subven��o estadual
                sArquivo = ARQUIVO_PLANILHA_SUBVENCAO_ESTADUAL + Format(iRemessa, "0000") + ".XLS"
                
            Case 4 'SEGA9066 - Propostas emitidas - SUBVEN��O ESTADUAL
                sArquivo = ARQUIVO_RETORNO_PLANILHA_SUBVENCAO_ESTADUAL + Format(iRemessa, "0000") + ".XLS"
                
            Case 5 'SEGA9067 - Propostas Canceladas - SUBVEN��O ESTADUAL
                sArquivo = ARQUIVO_CANCELAMENTO_SUBVENCAO_ESTADUAL + Format(iRemessa, "0000") + ".XLS"

            'mathayde  19/06/2009
            'Demanda 930288 - Gerar Arquivo Mapa (Sinistro Agr�cola)

            Case 6 'SEGA9084 - Arquivo de sinistro agricola
                sArquivo = ARQUIVO_AVISO_SUBVENCAO_ESTADUAL + Format(iRemessa, "0000") + ".XLS"
                
                Call OArquivo.AtualizaDataProcessamento(gsSIGLASISTEMA, _
                                                        App.Title, _
                                                        App.FileDescription, _
                                                        glAmbiente_id, _
                                                        sDtProcessamento, _
                                                        cUserName)
                                                        
                                                        
        End Select
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        
'        Call goProducao.AdicionaLog(1, sArquivo, 1)
 '       Call goProducao.AdicionaLog(2, lblVersao.Caption, 1)
  '      Call goProducao.AdicionaLog(3, lblProcessados.Caption, 1)
   '     '###########################grava log arquivo CTM
    '    If CTM = True Then
     '      Call GravaLogCTM("OK - ", sArquivo, lblVersao.Caption, lblProcessados.Caption)
      '  End If
        '################################################
        
        
        
    
    End If
        
        '#####################Pula caso executado via Control-M
'    If CTM = False Then
 '   Call MensagemBatch("Fim de Processamento", vbInformation, , False)
  '  Call goProducao.Finaliza
   ' End If
    '#####################
    
    Screen.MousePointer = vbNormal
           
    Exit Sub

Trata_Erro:

    Call TratarErro("Processar", Me.name)
    Call FinalizarAplicacao
    
End Sub

Private Sub cmdOk_Click()

    Call Processar

End Sub

Private Sub cmdSair_Click()
  
  Call FinalizarAplicacao
  
End Sub

Private Sub Form_Load()

'On Error GoTo Trata_Erro
glAmbiente_id = 3

    'Verificando permiss�o de acesso � aplica��o / pula caso seja executado via CTM
'    If Verifica_Origem_ControlM(Command) = False Then
 '   If Not Trata_Parametros(Command) Then
  '      Call FinalizarAplicacao
   ' End If
    'End If
    
    'If Obtem_agenda_diaria_id(Command) > 0 Or Verifica_Origem_ControlM(Command) = True Then
'        Me.Show
    '    Call cmdOk_Click
    '    Call cmdSair_Click
    'End If

    'Exit Sub

'Trata_Erro:

 '   Call TratarErro("Form_Load", Me.name)
  '  Call FinalizarAplicacao

End Sub

'lrocha 04/07/2008
Private Sub GerarPlanilhaEstadualSiglaUF(ByVal OArquivo As Object, _
                                         ByVal iLinhaArquivo As Integer, _
                                         ByVal sCaminhoArquivo As String, _
                                         ByVal lTotalRegistros As Long, _
                                         ByVal iRemessa As Integer, _
                                         ByRef bArquivoGerado As Boolean)

Dim oSigla As Object
Dim oRs As Recordset
Dim oPlanilha As Object

' JVALADARES - 26/06/2015 - INC000004632324: Incluindo tratamento de erro
On Error GoTo Trata_Erro


Set oSigla = CreateObject("SEGL0284.cls00459")

Set oRs = oSigla.ObterSiglasAtivas(gsSIGLASISTEMA, _
                                   App.Title, _
                                   App.FileDescription, _
                                   glAmbiente_id)


If Not oRs.EOF Then

    Set oPlanilha = CreateObject("SEGL0284.cls00459")
    
    While Not oRs.EOF
        Select Case iLinhaArquivo
            Case 3 'SEGA9065 - Propostas consulta subven��o estadual
            
                Call OArquivo.GerarPlanilhaEstadual(gsSIGLASISTEMA, _
                                                    App.Title, _
                                                    App.FileDescription, _
                                                    glAmbiente_id, _
                                                    ARQUIVO_PLANILHA_SUBVENCAO_ESTADUAL, _
                                                    sCaminhoArquivo, _
                                                    cUserName, _
                                                    Data_Sistema, _
                                                    lTotalRegistros, _
                                                    iRemessa, _
                                                    bArquivoGerado, _
                                                    oRs("SG_UF_SGRO_AGRL"))
                                                    
            Case 4 'SEGA9066 - Propostas emitidas - SUBVEN��O ESTADUAL
            
                Call OArquivo.GerarPlanilhaRetornoEstadual(gsSIGLASISTEMA, _
                                                            App.Title, _
                                                            App.FileDescription, _
                                                            glAmbiente_id, _
                                                            ARQUIVO_RETORNO_PLANILHA_SUBVENCAO_ESTADUAL, _
                                                            sCaminhoArquivo, _
                                                            cUserName, _
                                                            Data_Sistema, _
                                                            lTotalRegistros, _
                                                            iRemessa, _
                                                            bArquivoGerado, _
                                                            oRs("SG_UF_SGRO_AGRL"))
                                                            
               'GerarRemessaPecuario: subven��o 1 = Federal / 2 = Estadual
               Call OArquivo.GerarRemessaPecuario(gsSIGLASISTEMA, _
                                           App.Title, _
                                           App.FileDescription, _
                                           glAmbiente_id, _
                                           "SEGA9066", _
                                           2, _
                                           cUserName)
                                                            
            Case 5 'SEGA9067 - Propostas Canceladas - SUBVEN��O ESTADUAL
            
                Call OArquivo.GerarPlanilhaCancelamentoEstadual(gsSIGLASISTEMA, _
                                                                App.Title, _
                                                                App.FileDescription, _
                                                                glAmbiente_id, _
                                                                ARQUIVO_CANCELAMENTO_SUBVENCAO_ESTADUAL, _
                                                                sCaminhoArquivo, _
                                                                cUserName, _
                                                                Data_Sistema, _
                                                                lTotalRegistros, _
                                                                iRemessa, _
                                                                bArquivoGerado, _
                                                                oRs("SG_UF_SGRO_AGRL"))
        End Select

    oRs.MoveNext
    Wend

Else
    MsgBox ("N�o h� siglas ativas para este arquivo.")
End If


oRs.Close
Set oRs = Nothing

Set oRs = Nothing

Exit Sub

' JVALADARES - 26/06/2015 - INC000004632324: Incluindo tratamento de erro
Trata_Erro:

    Call TratarErro("GerarPlanilhaEstadualSiglaUF", Me.name)
    Call FinalizarAplicacao
    

End Sub


