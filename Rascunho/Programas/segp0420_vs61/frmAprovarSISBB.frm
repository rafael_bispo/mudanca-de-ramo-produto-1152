VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Object = "{B848631F-FD13-44DC-8AD2-1E790664626D}#5.0#0"; "eMail.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Begin VB.Form frmAprovarSISBB 
   Caption         =   "Aprova��o de Pagamentos de Sinistros para o SISBB"
   ClientHeight    =   8565
   ClientLeft      =   1635
   ClientTop       =   1530
   ClientWidth     =   12705
   Icon            =   "frmAprovarSISBB.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   8565
   ScaleWidth      =   12705
   Begin VB.CommandButton cmdTodos 
      Caption         =   "Selecionar Todos"
      Height          =   375
      Left            =   7200
      TabIndex        =   14
      Top             =   7860
      Width           =   1695
   End
   Begin eMail.ctleMail ctleMail 
      Left            =   6350
      Top             =   7800
      _ExtentX        =   1508
      _ExtentY        =   873
   End
   Begin MSFlexGridLib.MSFlexGrid flexSelecao 
      Height          =   5745
      Left            =   135
      TabIndex        =   5
      TabStop         =   0   'False
      Top             =   2055
      Width           =   12500
      _ExtentX        =   22040
      _ExtentY        =   10134
      _Version        =   393216
      Rows            =   1
      Cols            =   28
      FixedCols       =   0
      WordWrap        =   -1  'True
      HighLight       =   2
      FillStyle       =   1
      MergeCells      =   1
      FormatString    =   $"frmAprovarSISBB.frx":0442
   End
   Begin VB.CommandButton btnAplicar 
      Caption         =   "&Confirmar Autoriza��o"
      Height          =   375
      Left            =   10850
      TabIndex        =   7
      Top             =   7860
      Width           =   1770
   End
   Begin VB.CommandButton btnSair 
      Caption         =   "&Sair"
      Height          =   375
      Left            =   9000
      TabIndex        =   6
      Top             =   7860
      Width           =   1770
   End
   Begin VB.Frame frameCriterios 
      Caption         =   " Crit�rios para Sele��o "
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1890
      Left            =   120
      TabIndex        =   8
      Top             =   120
      Width           =   12500
      Begin VB.TextBox txtConteudo 
         Height          =   315
         Left            =   3360
         TabIndex        =   3
         Top             =   1350
         Width           =   6495
      End
      Begin VB.Frame fraRamo 
         Caption         =   " Ramo "
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1335
         Left            =   240
         TabIndex        =   12
         Top             =   360
         Width           =   2655
         Begin VB.OptionButton optRE 
            Caption         =   "Ramos Elementares"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   495
            Left            =   225
            TabIndex        =   1
            Top             =   720
            Width           =   2250
         End
         Begin VB.OptionButton optVida 
            Caption         =   "Vida"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Left            =   240
            TabIndex        =   0
            Top             =   360
            Width           =   1770
         End
      End
      Begin VB.CommandButton btnPesquisar 
         Caption         =   "&Pesquisar"
         Height          =   375
         Left            =   10080
         TabIndex        =   4
         Top             =   1305
         Width           =   1332
      End
      Begin VB.ComboBox cmbCampo 
         Height          =   315
         ItemData        =   "frmAprovarSISBB.frx":062E
         Left            =   3360
         List            =   "frmAprovarSISBB.frx":0630
         Style           =   2  'Dropdown List
         TabIndex        =   2
         Top             =   600
         Width           =   3765
      End
      Begin VB.Label Label1 
         Caption         =   "Pesquisar por:"
         Height          =   255
         Left            =   3360
         TabIndex        =   10
         Top             =   360
         Width           =   1215
      End
      Begin VB.Label Label2 
         Caption         =   "Conte�do:"
         Height          =   255
         Left            =   3360
         TabIndex        =   9
         Top             =   1080
         Width           =   975
      End
   End
   Begin MSComctlLib.StatusBar StatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   11
      Top             =   8280
      Width           =   12705
      _ExtentX        =   22410
      _ExtentY        =   503
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   2
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   14377
            MinWidth        =   14377
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   7056
            MinWidth        =   7056
            Text            =   "Autenticado para o CPF.:"
            TextSave        =   "Autenticado para o CPF.:"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSComctlLib.ImageList imglstAcao 
      Left            =   5700
      Top             =   7740
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   20
      ImageHeight     =   18
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   2
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmAprovarSISBB.frx":0632
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmAprovarSISBB.frx":0BD8
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin VB.Label lblSelecionados 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   120
      TabIndex        =   13
      Top             =   7920
      Width           =   5475
   End
End
Attribute VB_Name = "frmAprovarSISBB"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'Option Explicit

Dim rdocn_Aux            As New ADODB.Recordset
Dim rs                   As ADODB.Recordset
Dim rsTB                 As ADODB.Recordset
Dim rsAUX                 As ADODB.Recordset
Dim vSql                 As String
Dim vTpRamo              As String
Dim tb_Temp_Criada       As Boolean
Dim Data_Implantacao_GTR As String
Dim descricao_moeda      As String
Dim descricao_moeda_id   As Integer

Dim Destinatario         As String

'* Constantes para utiliza��o do GRID
Const GRID_SELECAO_ID_ACAO = 0
Const GRID_SELECAO_ACAO = 1
Const GRID_SELECAO_VOUCHER = 2
Const GRID_SELECAO_NOME_BENEFICIARIO = 3        '* ANTIGO 17
Const GRID_SELECAO_VALOR_TOTAL_GERAL = 4        '* ANTIGO 16
Const GRID_SELECAO_AGENCIA = 5
Const GRID_SELECAO_SINISTRO_ID = 6
Const GRID_SELECAO_SINISTRO_BB = 7
Const GRID_SELECAO_APOLICE_ID = 8
Const GRID_SELECAO_NOME_SEGURADO = 9
Const GRID_SELECAO_DT_PGTO = 10
Const GRID_SELECAO_MOEDA = 11
Const GRID_SELECAO_VALOR_PGTO = 12
Const GRID_SELECAO_CORR_MONET = 13
'* Inclu�do por Junior em 07/02/2003
Const GRID_SELECAO_VALOR_FRANQUIA = 14
Const GRID_SELECAO_VALOR_DEPRECIACAO = 15
Const GRID_SELECAO_VALOR_SALVADOS = 16
Const GRID_SELECAO_VALOR_PREJUIZO_TOTAL = 17
'Const GRID_SELECAO_VALOR_TOTAL_GERAL = 16
'Const GRID_SELECAO_NOME_BENEFICIARIO = 17       '* ANTIGO 12
Const GRID_SELECAO_NUM_AUTORIZACAO = 18         '* ANTIGO 13
Const GRID_SELECAO_NUM_LISTA = 19               '* ANTIGO 14
Const GRID_SELECAO_NUM_REMESSA = 20             '* ANTIGO 15
Const GRID_SELECAO_DT_REMESSA = 21              '* ANTIGO 16
Const GRID_SELECAO_SUCURSAL_SEG_ID = 22         '* ANTIGO 17
Const GRID_SELECAO_SEGURADORA_COD_SUSEP = 23    '* ANTIGO 18
Const GRID_SELECAO_RAMO_ID = 24                 '* ANTIGO 19
Const GRID_SELECAO_BENEFICIARIO_ID = 25         '* ANTIGO 20
Const GRID_SELECAO_NUM_RECIBO = 26              '* ANTIGO 21
Const GRID_SELECAO_DESTINATARIO = 27            '* ANTIGO 22

'*Alterado por Alexandre Debouch - Confitec SP - 26/03/2010
Const GRID_SELECAO_AMBIENTE = 28
Const GRID_SELECAO_PRODUTO_ID = 29


'MATHAYDE - 08/10/2009 - Aprova��o CDC
'Define o tipo de query a retornar
Const CDC_SINTETICO = 0
Const CDC_ANALITICO = 1

'* FIM DA ALTERA��O

Private Sub BtnSair_Click()
    vLog = CancelarConexoes(vNunCx)
    Call TerminaSEGBR
End Sub

Private Sub btnAplicar_Click()
        
    Dim i               As Long, j As Long, iPos As Integer
    Dim Evento_SEGBR_id As String
    Dim Descricao       As String
    Dim Count_Remove    As Integer
    Dim E_Cabecalho     As Boolean
    
    
        '* Alterado por Alexandre Debouch
    Dim glAmbiente_id_Old As Integer
    Dim vNuncxOld         As Long
    
    Dim sSQL            As String
    Dim rs              As ADODB.Recordset
    
    Dim HoraFixa As Date
    Dim HoraAtual As Date
    Dim PgtoHoraRestrita As Boolean 'Para indicar se houve algum pagamento n�o aprovado por limite de hor�rio
            
    If Selecionou_Registro = False Then
        mensagem_erro 6, "Nenhuma altera��o foi efetuada !"
        Exit Sub
    End If
        
    Screen.MousePointer = 11
        
        
    '* Alterado por Alexandre Debouch - ConfitecSP - 05/03/2010
    'Manter o Ambiente atual
'    glAmbiente_id_Old = glAmbiente_id
'    vNuncxOld = vNunCx
        
    Count_Remove = 0
    E_Cabecalho = False
    For i = 1 To flexSelecao.Rows - 1
            
        If Val(flexSelecao.TextMatrix(i, GRID_SELECAO_ID_ACAO)) <> 0 Then
            
            '*<Modificado por: Alexandre Debouch - ConfitecSP - 11/3/2010-14:24:58>
'            If bUsarConexaoABS Then
'                If glAmbiente_id <> flexSelecao.TextMatrix(i, GRID_SELECAO_AMBIENTE) Then
'                   glAmbiente_id = flexSelecao.TextMatrix(i, GRID_SELECAO_AMBIENTE)
'                End If
'            End If
            
'            vNunCx = AbrirConexao("SEGBR", glAmbiente_id, App.Title, App.FileDescription)
            
            '*</Modificado por: Alexandre Debouch - ConfitecSP - 11/3/2010-14:24:58>
            'rdocn.BeginTrans
                 
            Select Case flexSelecao.TextMatrix(i, GRID_SELECAO_PRODUTO_ID)
                Case 719
                    HoraFixa = "11:00:00"
                    HoraAtual = Time
                Case 1198, 1140, 1205, 1183, 1225 'cristovao.rodrigues 25/04/2014 - 17919477
                    HoraFixa = "17:30:00"
                    HoraAtual = Time
                Case Else
                    HoraFixa = "00:00:00"
                    HoraAtual = "00:00:00"
            End Select

            If HoraAtual > HoraFixa Then
                PgtoHoraRestrita = True
                flexSelecao.TextMatrix(i, GRID_SELECAO_ID_ACAO) = ""
            Else
                vLog = AbrirTransacao(vNunCx)
            
                'A grava��o dos eventos est� nas respectivas SP
                If Val(flexSelecao.TextMatrix(i, GRID_SELECAO_ID_ACAO)) = 1 Then
                    Call Aprovar_SISBB(flexSelecao.TextMatrix(i, GRID_SELECAO_SINISTRO_ID), flexSelecao.TextMatrix(i, GRID_SELECAO_APOLICE_ID), flexSelecao.TextMatrix(i, GRID_SELECAO_SUCURSAL_SEG_ID), flexSelecao.TextMatrix(i, GRID_SELECAO_SEGURADORA_COD_SUSEP), flexSelecao.TextMatrix(i, GRID_SELECAO_RAMO_ID), flexSelecao.TextMatrix(i, GRID_SELECAO_BENEFICIARIO_ID), flexSelecao.TextMatrix(i, GRID_SELECAO_SINISTRO_BB), flexSelecao.TextMatrix(i, GRID_SELECAO_VOUCHER))
                ElseIf Val(flexSelecao.TextMatrix(i, GRID_SELECAO_ID_ACAO)) = 2 Then
                    Call Estornar_Voucher(Val(flexSelecao.TextMatrix(i, GRID_SELECAO_VOUCHER)))
                    Call Enviar_Email_Estorno(i)
                End If
                                
                'vLog = RetornarTransacao(vNunCx)
                vLog = ConfirmarTransacao(vNunCx)
            End If
            
        End If
            
    Next
        
    '* Alterado por Alexandre Debouch - ConfitecSP - 05/03/2010
    'retornar o Ambiente atual
'    glAmbiente_id = glAmbiente_id_Old
'    If vNunCx <> vNuncxOld Then
'        vNunCx = vNuncxOld
'        vLog = CancelarConexoes(vNuncxOld)
'        tb_Temp_Criada = False
'    End If
            
        
    i = i - 1
    For j = i To 1 Step -1
        If Val(flexSelecao.TextMatrix(j, GRID_SELECAO_ID_ACAO)) <> 0 Then
            flexSelecao.RemoveItem j
            Count_Remove = Count_Remove + 1
        Else
            'Verifica se � cabe�alho
            If (flexSelecao.TextMatrix(j, GRID_SELECAO_ACAO) = "") And (flexSelecao.TextMatrix(j, GRID_SELECAO_VOUCHER) = "" And (Len(flexSelecao.TextMatrix(j, GRID_SELECAO_SINISTRO_ID)) > 11)) Then
                'Verifica se o cabe�alho � o �ltimo registro da lista
                If (flexSelecao.Rows - 1) = j Then
                    If j = 1 Then
                        flexSelecao.Rows = 1
                    Else
                        flexSelecao.RemoveItem j
                    End If
                Else
                    If (flexSelecao.TextMatrix(j + 1, GRID_SELECAO_ACAO) = "") And (flexSelecao.TextMatrix(j + 1, GRID_SELECAO_VOUCHER) = "" And (Len(flexSelecao.TextMatrix(j + 1, GRID_SELECAO_SINISTRO_ID)) > 11)) Then
                        flexSelecao.RemoveItem j
                    End If
                End If
            End If
        End If
    Next
        
    iPos = InStr(1, lblSelecionados, "I")
    lblSelecionados = Val(Mid(lblSelecionados, 1, iPos - 1)) - Count_Remove & " Iten(s) Encontrado(s)"
        
    Screen.MousePointer = 0
    If PgtoHoraRestrita Then
        MsgBox "Alguns pagamentos n�o foram aprovados devido a restri��o de hor�rio.", vbOKOnly + vbInformation, "Aprova��o SISBB"
    Else
        MsgBox "Pagamentos efetuados com sucesso !", vbOKOnly + vbInformation, "Aprova��o SISBB"
    End If
        
End Sub

Private Sub btnPesquisar_Click()
        
    Dim Linha      As String, Count     As Long
    Dim prod_atual As Integer, prod_ant As Integer
        
    On Error GoTo TrataErro
    
    'jose.souza - Confitec - Em 16/08/2013
    bUsarConexaoABS = False
        
    If Selecionou_Registro = True Then
        If MsgBox("Existem altera��es, deseja executar uma nova pesquisa ?", vbYesNo + vbQuestion + vbDefaultButton2, "!!! Aten��o !!!") = vbNo Then Exit Sub
    End If
        
    txtConteudo = Trim(txtConteudo)
    flexSelecao.Rows = 1
        
    If Not optVida.Value And Not optRE.Value Then
        mensagem_erro 6, "Selecione o ramo a ser pesquisado!"
        txtConteudo.Enabled = True
        cmbCampo.SetFocus
        Exit Sub
    End If
        
    If cmbCampo.Text = "" Then
        mensagem_erro 6, "Informe o crit�rio de pesquisa!"
        txtConteudo.Enabled = True
        cmbCampo.SetFocus
        Exit Sub
    End If
        
    If txtConteudo.Text = "" And txtConteudo.Enabled Then
        mensagem_erro 6, "Informe o conte�do a ser pesquisado!"
        txtConteudo.Enabled = True
        txtConteudo.SetFocus
        Exit Sub
    End If
        
    If cmbCampo.Text = "Sinistro BB" Or cmbCampo.Text = "N� do Sinistro" Or cmbCampo.Text = "N� da Lista de Autoriza��o" Or cmbCampo.Text = "Voucher" Or cmbCampo.Text = "N� da Autoriza��o" Then
        If IsNumeric(txtConteudo.Text) = False Then
            mensagem_erro 6, "O conte�do informado n�o � um n�mero!"
            txtConteudo.Enabled = True
            txtConteudo.SetFocus
            Exit Sub
        End If
    End If
        
    Screen.MousePointer = 11
        
    StatusBar1.SimpleText = "Aguarde !!! Procurando registros . . ."
        
    If optVida.Value Then
        vTpRamo = "1"
    Else
        vTpRamo = "2"
    End If
        
    'Cria tabela temporaria
    Call Cria_Temp_Lista
        
    'Anderson Fiuza GPTI - 11/02/2010
    'Estava trazendo em duplicidade a o voucher_id.Inserido o distinct para retornar apenas um pagamento.
        
    'Monta a Query para incluir na tempor�ria
    vSql = "INSERT INTO #temp_lista             " & vbNewLine
    vSql = vSql & "    (voucher_id,             " & vbNewLine
    vSql = vSql & "     agencia_id,             " & vbNewLine
    vSql = vSql & "     nome_agencia,           " & vbNewLine
    vSql = vSql & "     sinistro_id,            " & vbNewLine
    vSql = vSql & "     sucursal_seguradora_id, " & vbNewLine
    vSql = vSql & "     seguradora_cod_susep,   " & vbNewLine
    vSql = vSql & "     ramo_id,                " & vbNewLine
    vSql = vSql & "     sinistro_bb,            " & vbNewLine
    vSql = vSql & "     apolice_id,             " & vbNewLine
    vSql = vSql & "     segurado_id,            " & vbNewLine
    vSql = vSql & "     nome_segurado,          " & vbNewLine
    vSql = vSql & "     dt_pagamento,           " & vbNewLine
    vSql = vSql & "     moeda_id,               " & vbNewLine
    vSql = vSql & "     valor_pagamento,        " & vbNewLine
    vSql = vSql & "     valor_correcao,         " & vbNewLine
    vSql = vSql & "     valor_franquia,         " & vbNewLine
    vSql = vSql & "     valor_depreciacao,      " & vbNewLine
    vSql = vSql & "     valor_salvados,         " & vbNewLine
    vSql = vSql & "     valor_prejuizo_total,   " & vbNewLine
    vSql = vSql & "     valor_total_geral,      " & vbNewLine
    vSql = vSql & "     beneficiario_id,        " & vbNewLine
    vSql = vSql & "     nome_beneficiario,      " & vbNewLine
    vSql = vSql & "     num_autorizacao,        " & vbNewLine
    vSql = vSql & "     num_lista,              " & vbNewLine
    vSql = vSql & "     num_remessa,            " & vbNewLine
    vSql = vSql & "     dt_remessa,             " & vbNewLine
    vSql = vSql & "     produto_id,             " & vbNewLine
    vSql = vSql & "     nome_produto,           " & vbNewLine
    vSql = vSql & "     num_recibo,             " & vbNewLine
    
    '* Alterado por Alexandre Debouch - ConfitecSP - 05/03/2010
    '* vSql = vSql &  "     destinatario_eMail)     " & vbNewLine
    vSql = vSql & "     destinatario_eMail,     " & vbNewLine
    vSql = vSql & "     ambiente)               " & vbNewLine
    '* FIM
    
    vSql = vSql & " SELECT DISTINCT                     " & vbNewLine
    vSql = vSql & "     (PS_AC_PG.voucher_id),          " & vbNewLine
    vSql = vSql & "     SN.agencia_id,                  " & vbNewLine
    vSql = vSql & "     nome_agencia=AG.nome,           " & vbNewLine
    vSql = vSql & "     PG_SN.sinistro_id,              " & vbNewLine
    vSql = vSql & "     PG_SN.sucursal_seguradora_id,   " & vbNewLine
    vSql = vSql & "     PG_SN.seguradora_cod_susep,     " & vbNewLine
    vSql = vSql & "     PG_SN.Ramo_id,                  " & vbNewLine
    vSql = vSql & "     PG_SN.sinistro_bb,              " & vbNewLine
    vSql = vSql & "     PG_SN.apolice_id,               " & vbNewLine
    
    If vTpRamo = "1" Then       'VIDA
        vSql = vSql & "      SN_VD.cpf,                              " & vbNewLine
        vSql = vSql & "      nome_segurado=SN_VD.nome,               " & vbNewLine
    ElseIf vTpRamo = "2" Then   'RE
        vSql = vSql & "     SN.cliente_id,                          " & vbNewLine
        vSql = vSql & "     nome_segurado=CLI.nome,                 " & vbNewLine
    End If
    
    vSql = vSql & "     PG_SN.dt_acerto_contas_sinistro,        " & vbNewLine
    vSql = vSql & "     SN.moeda_id,                            " & vbNewLine
    vSql = vSql & "     PG_SN.val_acerto,                       " & vbNewLine
    vSql = vSql & "     PG_SN.val_correcao,                     " & vbNewLine
    vSql = vSql & "     ISNULL(PG_SN.val_franquia_SISBB, 0),    " & vbNewLine
    vSql = vSql & "     ISNULL(PG_SN.val_depreciacao_SISBB, 0), " & vbNewLine
    vSql = vSql & "     ISNULL(PG_SN.val_salvado_SISBB, 0),     " & vbNewLine
    vSql = vSql & "     ISNULL(EV.val_prejuizo_SISBB, 0),       " & vbNewLine
    
    '* ALTERADO POR JUNIOR EM 09/09/2003
    If vTpRamo = "1" Then       'VIDA
        vSql = vSql & "     ISNULL(EV.val_prejuizo_SISBB, 0) + ISNULL(PG_SN.val_correcao, 0) + ISNULL(PG_SN.val_franquia_SISBB, 0), " & vbNewLine
    ElseIf vTpRamo = "2" Then   'RE
        vSql = vSql & "     ISNULL(EV.val_prejuizo_SISBB, 0) + ISNULL(PG_SN.val_correcao, 0), " & vbNewLine
    End If
    '* FIM
    
    vSql = vSql & "     PG_SN.beneficiario_id,                  " & vbNewLine
    vSql = vSql & "     nome_beneficiario=BF.nome,              " & vbNewLine
    vSql = vSql & "     num_autorizacao = CONVERT(varchar(10),PG_SN.chave_autorizacao_sinistro) + '-' + PG_SN.chave_autorizacao_sinistro_dv,    " & vbNewLine
    vSql = vSql & "     PG_SN.num_lista,                        " & vbNewLine
    vSql = vSql & "     PS_RM_VC.remessa_id,                    " & vbNewLine
    vSql = vSql & "     PS_RM.dt_remessa ,                      " & vbNewLine
    vSql = vSql & "     produto_id=PR.produto_id,               " & vbNewLine
    vSql = vSql & "     nome_produto=PR.nome,                   " & vbNewLine
    vSql = vSql & "     PG_SN.num_recibo,                       " & vbNewLine
    
    '* Alterado por Alexandre Debouch  - ConfitecSP - 05/03/2010
    '* vSql = vSql & "             destinatario = PG_SN.usuario            " & vbNewLine
    vSql = vSql & "     destinatario = PG_SN.usuario,           " & vbNewLine
    vSql = vSql & "     'Ambiente'   = " & glAmbiente_id & vbNewLine
    '*FIM
    vSql = vSql & "   FROM                                                                                               " & vbNewLine
    vSql = vSql & "                 pgto_sinistro_tb PG_SN with (nolock)                                                 " & vbNewLine
    vSql = vSql & "  INNER  JOIN    sinistro_benef_tb BF  with (nolock) ON (BF.sinistro_id           = PG_SN.sinistro_id            " & vbNewLine
    vSql = vSql & "                                         AND BF.apolice_id             = PG_SN.apolice_id             " & vbNewLine
    vSql = vSql & "                                         AND BF.seguradora_cod_susep   = PG_SN.seguradora_cod_susep   " & vbNewLine
    vSql = vSql & "                                         AND BF.sucursal_seguradora_id = PG_SN.sucursal_seguradora_id " & vbNewLine
    vSql = vSql & "                                         AND BF.ramo_id                = PG_SN.ramo_id                " & vbNewLine
    vSql = vSql & "                                         AND BF.beneficiario_id        = PG_SN.beneficiario_id)       " & vbNewLine
    vSql = vSql & "  INNER  JOIN    sinistro_tb SN  with (nolock)           ON (SN.sinistro_id           = PG_SN.sinistro_id            " & vbNewLine
    vSql = vSql & "                                         AND SN.apolice_id             = PG_SN.apolice_id             " & vbNewLine
    vSql = vSql & "                                         AND SN.seguradora_cod_susep   = PG_SN.seguradora_cod_susep   " & vbNewLine
    vSql = vSql & "                                         AND SN.sucursal_seguradora_id = PG_SN.sucursal_seguradora_id " & vbNewLine
    vSql = vSql & "                                         AND SN.ramo_id                = PG_SN.ramo_id)               " & vbNewLine
    vSql = vSql & "  INNER  JOIN    proposta_tb PP  with (nolock)           ON (PP.proposta_id           = SN.proposta_id)              " & vbNewLine
    vSql = vSql & "  INNER  JOIN    produto_tb PR  with (nolock)            ON (PP.produto_id            = PR.produto_id                " & vbNewLine
    vSql = vSql & "                                         AND SN.seguradora_cod_susep   = PR.seguradora_cod_susep)     " & vbNewLine
    vSql = vSql & "   LEFT  JOIN    agencia_tb AG  with (nolock)            ON (AG.agencia_id            = SN.agencia_id                " & vbNewLine
    vSql = vSql & "                                         AND AG.banco_id               = 1)                           " & vbNewLine
    If vTpRamo = "1" Then       'VIDA
        vSql = vSql & "  INNER  JOIN    sinistro_vida_tb SN_VD  with (nolock)   ON (PG_SN.sinistro_id           = SN_VD.sinistro_id            " & vbNewLine
        vSql = vSql & "                                         AND PG_SN.apolice_id             = SN_VD.apolice_id             " & vbNewLine
        vSql = vSql & "                                         AND PG_SN.seguradora_cod_susep   = SN_VD.seguradora_cod_susep   " & vbNewLine
        vSql = vSql & "                                         AND PG_SN.sucursal_seguradora_id = SN_VD.sucursal_seguradora_id " & vbNewLine
        vSql = vSql & "                                         AND PG_SN.ramo_id                = SN_VD.ramo_id)               " & vbNewLine
    ElseIf vTpRamo = "2" Then   'RE
        vSql = vSql & "  INNER  JOIN    cliente_tb CLI  with (nolock)           ON (CLI.cliente_id              = SN.cliente_id)               " & vbNewLine
    End If
    vSql = vSql & "  INNER  JOIN    ramo_tb RAMO  with (nolock)                    ON (RAMO.ramo_id                 = PG_SN.ramo_id)           " & vbNewLine
    vSql = vSql & "  INNER  JOIN    ps_acerto_pagamento_tb PS_AC_PG  with (nolock) ON (PG_SN.acerto_id              = PS_AC_PG.acerto_id       " & vbNewLine
    vSql = vSql & "                                                 AND PS_AC_PG.cod_origem          = 'SN')                    " & vbNewLine
    
    '-- MFrasca - Flow 6223748 - 19/10/2010 - Altera��o para que seja poss�vel realizar a aprova��o antes de
    '-- se gerar a remessa ao PAGNET que ficar� condicionada a aprova��o do financeiro --
    'vSql = vSql & "  INNER  JOIN    ps_remessa_voucher_tb PS_RM_VC  ON (PS_AC_PG.voucher_id          = PS_RM_VC.voucher_id      " & vbNewLine
    vSql = vSql & "  LEFT  JOIN    ps_remessa_voucher_tb PS_RM_VC  with (nolock)  ON (PS_AC_PG.voucher_id          = PS_RM_VC.voucher_id      " & vbNewLine
    vSql = vSql & "                                                 AND PS_RM_VC.cod_origem          = 'SN')                    " & vbNewLine
    'vSql = vSql & "  INNER  JOIN    ps_remessa_tb PS_RM             ON (PS_RM_VC.remessa_id          = PS_RM.remessa_id         " & vbNewLine
    vSql = vSql & "  LEFT  JOIN    ps_remessa_tb PS_RM            with (nolock)   ON (PS_RM_VC.remessa_id          = PS_RM.remessa_id         " & vbNewLine
    vSql = vSql & "                                                 AND PS_RM.cod_sistema_ps         = 'AP')                    " & vbNewLine
    '-------------------------------------------------------------------------------------------------------------
    
    '' Alterado por Junior em 05/02/2003
    '' Foi inclu�do as linhas abaixo para obter somente os pagamentos que dever�o ser enviados via GTR
    vSql = vSql & "  INNER  JOIN    evento_SEGBR_sinistro_tb EV    with (nolock)   ON (EV.sinistro_BB  = PG_SN.sinistro_BB                     " & vbNewLine
    vSql = vSql & "                                                 AND EV.num_recibo   = PG_SN.num_recibo                      " & vbNewLine
    vSql = vSql & "                                                 AND EV.evento_BB_id = 1152)                                 " & vbNewLine
        
    'MATHAYDE - 08/10/2009 - Aprova��o CDC
    'Join para � exibir o sinistro do CDC junto dos demais
    'vSql = vSql & "   LEFT  JOIN    seguros_db..pgto_sinistro_lote_tb pgto_sinistro_lote_tb (NOLOCK)                            " & vbNewLine
    'vSql = vSql & "           ON    pgto_sinistro_lote_tb.sinistro_id  = PG_SN.sinistro_id                                      " & vbNewLine
    '' FIM
    
    '-- MFrasca - 24/11/2010 - Flow 6223748 - Ajuste para utilizar flag de aprova��o autom�tica do financeiro --
    vSql = vSql & "  INNER  JOIN   seguros_db..forma_pgto_tb AS forma_pgto_tb WITH(NOLOCK) "
    vSql = vSql & "  ON     forma_pgto_tb.forma_pgto_id = PG_SN.forma_pgto_id "
    '-----------------------------------------------------------------------------------------------------------
    
    vSql = vSql & "  WHERE  PS_AC_PG.voucher_id       IS NOT NULL                                                               " & vbNewLine
    vSql = vSql & "    AND  PG_SN.situacao_op         = 'a'                                                                     " & vbNewLine
    vSql = vSql & "    AND  PG_SN.autorizado_SISBB    = 'n'                                                                     " & vbNewLine
    vSql = vSql & "    AND  PG_SN.retorno_recibo_BB   = 'n'                                                                     " & vbNewLine
    vSql = vSql & "    AND  PG_SN.item_val_estimativa = 1                                                                       " & vbNewLine
    'vSql = vSql & "    AND  RAMO.tp_ramo_id           = " & vTpRamo & vbNewLine
    
	'nova consultoria - gleison.pimentel - 03/10/2014
    '18149812 � Novo produto Habitacional � Im�veis Comerciais - produtos 1226,1227 est�o cadastrados como tipo Vida, porem tem que ser tratado como tipo Danos.
    vSql = vSql & "    AND ((" & vTpRamo & " = 1 and (RAMO.tp_ramo_id = 1 and PP.produto_id not in (1226, 1227)) )       " & vbNewLine
    vSql = vSql & "    Or ( " & vTpRamo & " = 2 and ((RAMO.tp_ramo_id = 2) or (RAMO.tp_ramo_id = 1 and PP.produto_id in (1226, 1227)))))"
	
    '-- MFrasca - 24/11/2010 - Flow 6223748 - Ajuste para utilizar flag de aprova��o autom�tica do financeiro --
    '-- Caso seja 'S' ser� aprovado automaticamente pela tarefa da agenda que roda a procedure SEGS8042_SPI   --
    vSql = vSql & "    AND  ISNULL(forma_pgto_tb.aprovacao_automatica_financeiro, 'N') = 'N' "
    '-----------------------------------------------------------------------------------------------------------

    'vSql = vSql & " AND     val_resseguro_recuperado         IS NULL"
        
    '* ALTERADO POR JUNIOR EM 09/09/2003
    '        vSql = vSql & "   AND  EXISTS (SELECT * FROM INTERFACE_DB..entrada_GTR_tb e"
    '        vSql = vSql & "                WHERE e.sinistro_BB = PG_SN.sinistro_bb"
    '        vSql = vSql & "                  AND e.evento_BB   = 2000)"

    'LSA 11/03/2010
    'ALTERA��O PARA O SINISNTRO DO SQG
    '        vSql = vSql & "     AND EXISTS (SELECT * FROM INTERFACE_DB..entrada_GTR_tb e"
    '        vSql = vSql & "                 WHERE e.sinistro_BB = PG_SN.sinistro_BB"
    '        vSql = vSql & "                   AND evento_BB = 2000"
    '        vSql = vSql & "                   AND NOT EXISTS (SELECT * FROM evento_SEGBR_sinistro_tb evento_SEGBR_sinistro_tb"
    '        vSql = vSql & "                                   WHERE evento_SEGBR_sinistro_tb.evento_SEGBR_id = 10130" '* EVENTO REABERTURA SEM COMUNICA��O
    '        vSql = vSql & "                                     AND evento_SEGBR_sinistro_tb.sinistro_BB = e.sinistro_BB"
    '        vSql = vSql & "                                     AND evento_SEGBR_sinistro_tb.dt_evento <= PG_SN.dt_solicitacao_pgto"
    '        vSql = vSql & "                                     AND evento_SEGBR_sinistro_tb.EVENTO_ID < EV.evento_id" 'Inclu�do por Gustavo Machado em 02/06/2004
    '        vSql = vSql & "                                     AND NOT EXISTS (SELECT * FROM evento_SEGBR_sinistro_tb ev1" 'Inclu�do por Gustavo Machado em 05/07/2004
    '        vSql = vSql & "                                                        WHERE ev1.sinistro_BB = e.sinistro_BB"
    '        vSql = vSql & "                                                        AND ev1.evento_bb_id = 1151"
    '        vSql = vSql & "                                                        AND ev1.evento_id > evento_SEGBR_sinistro_tb.evento_id)"
    '        vSql = vSql & "                                     AND NOT EXISTS (SELECT * FROM evento_SEGBR_sinistro_tb evento_SEGBR_sinistro_tb"
    '        vSql = vSql & "                                                     WHERE evento_SEGBR_sinistro_tb.evento_SEGBR_id = 10120" '* EVENTO REABERTURA SEM COMUNICA��O
    '        vSql = vSql & "                                                       AND evento_SEGBR_sinistro_tb.sinistro_BB = e.sinistro_BB"
    '        vSql = vSql & "                                                       AND evento_SEGBR_sinistro_tb.EVENTO_ID < EV.evento_id" 'Inclu�do por Gustavo Machado em 02/06/2004
    '        vSql = vSql & "                                                       AND evento_SEGBR_sinistro_tb.dt_evento <= PG_SN.dt_solicitacao_pgto)))"
    '* FIM
    'vSQL = vSQL & " AND     SN.dt_aviso_sinistro             >= '" & Data_Implantacao_GTR & "'"
        
    vSql = vSql & "    AND  PG_SN.dt_solicitacao_pgto >= '" & Data_Implantacao_GTR & "'                                         " & vbNewLine
    
    vSql = vSql & "    AND  ISNULL(EV.val_prejuizo_SISBB, 0) <=                                                                 "
    vSql = vSql & "                     (SELECT DISTINCT ISNULL(vl_max_aprovacao,0)                                             "
    vSql = vSql & "                        FROM seguros_db..usuario_sisbb_tb  with (nolock)                                     "
    vSql = vSql & "                       WHERE chave ='" & AChave_SISBB & "')                                                  " & vbNewLine
        
        
    'MATHAYDE - 08/10/2009 - Aprova��o CDC
    '� exibir o sinistro do CDC junto dos demais
    'vSql = vSql & "    AND  pgto_sinistro_lote_tb.sinistro_id  IS NULL                                                          " & vbNewLine
    
    'Alterado Alexandre Debouch - ConfitecSP 03/03/2010
    'If bUsarConexaoABS Then
        'Para situacao de sinistro que s� � usado em caso de Ramo = VIDA
        '        If vTpRamo = "1" Then
        '            vSql = vSql & "   AND       SN.situacao <> 7                                                    " & vbNewLine
        '        End If
        'Para situacao de proposta
        vSql = vSql & "   AND       PP.situacao <> 'T'                                                  " & vbNewLine
    'End If
    
    Select Case cmbCampo.Text
               
        Case "Sinistro BB" 'Sinistro BB
            If Len(txtConteudo) > 13 Then
                mensagem_erro 6, "N�mero maior que o permitido!"
                txtConteudo.Enabled = True
                txtConteudo.SetFocus
                Screen.MousePointer = 0
                Exit Sub
            End If
                    
            vSql = vSql & "    AND  PG_SN.sinistro_bb = " & txtConteudo & vbNewLine
                    
        Case "N� do Sinistro" 'N�mero do Sinistro
            If Len(txtConteudo) > 11 Then
                mensagem_erro 6, "N�mero maior que o permitido!"
                txtConteudo.Enabled = True
                txtConteudo.SetFocus
                Screen.MousePointer = 0
                Exit Sub
            End If
                    
            vSql = vSql & "    AND  PG_SN.sinistro_id = " & txtConteudo & vbNewLine
                    
        Case "Data da Remessa" 'Data da Remessa
            If (Len(txtConteudo) > 10 Or (Not IsDate(txtConteudo))) Then
                mensagem_erro 6, "N�mero maior que o permitido!"
                txtConteudo.Enabled = True
                txtConteudo.SetFocus
                Screen.MousePointer = 0
                Exit Sub
            End If
                    
            vSql = vSql & "    AND  PS_RM.dt_remessa  = " & txtConteudo & vbNewLine
                    
        Case "N� da Lista de Autoriza��o" 'N�mero da Lista de Autoriza��o
            If Len(txtConteudo) > 9 Then
                mensagem_erro 6, "N�mero maior que o permitido!"
                txtConteudo.Enabled = True
                txtConteudo.SetFocus
                Screen.MousePointer = 0
                Exit Sub
            End If
                    
            vSql = vSql & "    AND  PG_SN.num_lista   = " & txtConteudo & vbNewLine
                    
        Case "Voucher" 'C�digo do Voucher
            If Len(txtConteudo) > 9 Then
                mensagem_erro 6, "N�mero maior que o permitido!"
                txtConteudo.Enabled = True
                txtConteudo.SetFocus
                Screen.MousePointer = 0
                Exit Sub
            End If
                    
            vSql = vSql & "    AND  PS_AC_PG.voucher_id = " & txtConteudo & vbNewLine
                    
        Case "N� da Autoriza��o" 'N�mero da Autoriza��o
            If Len(txtConteudo) > 9 Then
                mensagem_erro 6, "N�mero maior que o permitido!"
                txtConteudo.Enabled = True
                txtConteudo.SetFocus
                Screen.MousePointer = 0
                Exit Sub
            End If
                    
            vSql = vSql & "    AND  PG_SN.chave_autorizacao_sinistro = " & txtConteudo & vbNewLine
                  
    End Select

    '''' BNEVES 14/03/2008 - FLOW 278709
    '''' TROCA DA CLAUSULA PG_SN.sinistro_id DO ORDER BY DE LUGAR
    '''' vSql = vSql & " ORDER BY PS_AC_PG.voucher_id, PG_SN.sinistro_id"
    '''' vSql = vSql & " ORDER BY PR.Produto_id, PG_SN.sinistro_id, PS_AC_PG.voucher_id, PG_SN.sinistro_id"
    vSql = vSql & "  ORDER  BY  PR.Produto_id,      " & vbNewLine
    vSql = vSql & "             PG_SN.sinistro_id,  " & vbNewLine
    vSql = vSql & "             PS_AC_PG.voucher_id " & vbNewLine
    '''' FIM

    vLog = Conexao_ExecutarSQL("SEGBR", glAmbiente_id, App.Title, App.FileDescription, vSql, vNunCx, False, False)
    'rdocn.Execute (vSQL)


    If bUsarConexaoABS Then
        'Monta a Query para incluir na tempor�ria
        vSql = "INSERT INTO #temp_lista             " & vbNewLine
        vSql = vSql & "    (voucher_id,             " & vbNewLine
        vSql = vSql & "     agencia_id,             " & vbNewLine
        vSql = vSql & "     nome_agencia,           " & vbNewLine
        vSql = vSql & "     sinistro_id,            " & vbNewLine
        vSql = vSql & "     sucursal_seguradora_id, " & vbNewLine
        vSql = vSql & "     seguradora_cod_susep,   " & vbNewLine
        vSql = vSql & "     ramo_id,                " & vbNewLine
        vSql = vSql & "     sinistro_bb,            " & vbNewLine
        vSql = vSql & "     apolice_id,             " & vbNewLine
        vSql = vSql & "     segurado_id,            " & vbNewLine
        vSql = vSql & "     nome_segurado,          " & vbNewLine
        vSql = vSql & "     dt_pagamento,           " & vbNewLine
        vSql = vSql & "     moeda_id,               " & vbNewLine
        vSql = vSql & "     valor_pagamento,        " & vbNewLine
        vSql = vSql & "     valor_correcao,         " & vbNewLine
        vSql = vSql & "     valor_franquia,         " & vbNewLine
        vSql = vSql & "     valor_depreciacao,      " & vbNewLine
        vSql = vSql & "     valor_salvados,         " & vbNewLine
        vSql = vSql & "     valor_prejuizo_total,   " & vbNewLine
        vSql = vSql & "     valor_total_geral,      " & vbNewLine
        vSql = vSql & "     beneficiario_id,        " & vbNewLine
        vSql = vSql & "     nome_beneficiario,      " & vbNewLine
        vSql = vSql & "     num_autorizacao,        " & vbNewLine
        vSql = vSql & "     num_lista,              " & vbNewLine
        vSql = vSql & "     num_remessa,            " & vbNewLine
        vSql = vSql & "     dt_remessa,             " & vbNewLine
        vSql = vSql & "     produto_id,             " & vbNewLine
        vSql = vSql & "     nome_produto,           " & vbNewLine
        vSql = vSql & "     num_recibo,             " & vbNewLine
        
        '* Alterado por Alexandre Debouch - ConfitecSP - 05/03/2010
        '* vSql = vSql &  "     destinatario_eMail)     " & vbNewLine
        vSql = vSql & "     destinatario_eMail,     " & vbNewLine
        vSql = vSql & "     ambiente)               " & vbNewLine
        '* FIM
        
        vSql = vSql & " SELECT DISTINCT                     " & vbNewLine
        vSql = vSql & "     (PS_AC_PG.voucher_id),          " & vbNewLine
        vSql = vSql & "     SN.agencia_id,                  " & vbNewLine
        vSql = vSql & "     nome_agencia=AG.nome,           " & vbNewLine
        vSql = vSql & "     PG_SN.sinistro_id,              " & vbNewLine
        vSql = vSql & "     PG_SN.sucursal_seguradora_id,   " & vbNewLine
        vSql = vSql & "     PG_SN.seguradora_cod_susep,     " & vbNewLine
        vSql = vSql & "     PG_SN.Ramo_id,                  " & vbNewLine
        vSql = vSql & "     PG_SN.sinistro_bb,              " & vbNewLine
        vSql = vSql & "     PG_SN.apolice_id,               " & vbNewLine
        
        If vTpRamo = "1" Then       'VIDA
            vSql = vSql & "      SN_VD.cpf,                              " & vbNewLine
            vSql = vSql & "      nome_segurado=SN_VD.nome,               " & vbNewLine
        ElseIf vTpRamo = "2" Then   'RE
            vSql = vSql & "     SN.cliente_id,                          " & vbNewLine
            vSql = vSql & "     nome_segurado=CLI.nome,                 " & vbNewLine
        End If
        
        vSql = vSql & "     PG_SN.dt_acerto_contas_sinistro,        " & vbNewLine
        vSql = vSql & "     SN.moeda_id,                            " & vbNewLine
        vSql = vSql & "     PG_SN.val_acerto,                       " & vbNewLine
        vSql = vSql & "     PG_SN.val_correcao,                     " & vbNewLine
        vSql = vSql & "     ISNULL(PG_SN.val_franquia_SISBB, 0),    " & vbNewLine
        vSql = vSql & "     ISNULL(PG_SN.val_depreciacao_SISBB, 0), " & vbNewLine
        vSql = vSql & "     ISNULL(PG_SN.val_salvado_SISBB, 0),     " & vbNewLine
        vSql = vSql & "     ISNULL(EV.val_prejuizo_SISBB, 0),       " & vbNewLine
        
        '* ALTERADO POR JUNIOR EM 09/09/2003
        If vTpRamo = "1" Then       'VIDA
            vSql = vSql & "     ISNULL(EV.val_prejuizo_SISBB, 0) + ISNULL(PG_SN.val_correcao, 0) + ISNULL(PG_SN.val_franquia_SISBB, 0), " & vbNewLine
        ElseIf vTpRamo = "2" Then   'RE
            vSql = vSql & "     ISNULL(EV.val_prejuizo_SISBB, 0) + ISNULL(PG_SN.val_correcao, 0), " & vbNewLine
        End If
        '* FIM
        
        vSql = vSql & "     PG_SN.beneficiario_id,                  " & vbNewLine
        vSql = vSql & "     nome_beneficiario=BF.nome,              " & vbNewLine
        vSql = vSql & "     num_autorizacao = CONVERT(varchar(10),PG_SN.chave_autorizacao_sinistro) + '-' + PG_SN.chave_autorizacao_sinistro_dv,    " & vbNewLine
        vSql = vSql & "     PG_SN.num_lista,                        " & vbNewLine
        vSql = vSql & "     PS_RM_VC.remessa_id,                    " & vbNewLine
        vSql = vSql & "     PS_RM.dt_remessa ,                      " & vbNewLine
        vSql = vSql & "     produto_id=PR.produto_id,               " & vbNewLine
        vSql = vSql & "     nome_produto=PR.nome,                   " & vbNewLine
        vSql = vSql & "     PG_SN.num_recibo,                       " & vbNewLine
        
        '* Alterado por Alexandre Debouch  - ConfitecSP - 05/03/2010
        '* vSql = vSql & "             destinatario = PG_SN.usuario            " & vbNewLine
        vSql = vSql & "     destinatario = PG_SN.usuario,           " & vbNewLine
        vSql = vSql & "     'Ambiente'   = " & glAmbiente_id_seg2 & vbNewLine
        '*FIM
        vSql = vSql & "   FROM                                                                                               " & vbNewLine
        vSql = vSql & "                 abss.seguros_db.dbo.pgto_sinistro_tb PG_SN  with (nolock)                                                               " & vbNewLine
        vSql = vSql & "  INNER  JOIN    abss.seguros_db.dbo.sinistro_benef_tb BF  with (nolock)     ON (BF.sinistro_id           = PG_SN.sinistro_id            " & vbNewLine
        vSql = vSql & "                                         AND BF.apolice_id             = PG_SN.apolice_id             " & vbNewLine
        vSql = vSql & "                                         AND BF.seguradora_cod_susep   = PG_SN.seguradora_cod_susep   " & vbNewLine
        vSql = vSql & "                                         AND BF.sucursal_seguradora_id = PG_SN.sucursal_seguradora_id " & vbNewLine
        vSql = vSql & "                                         AND BF.ramo_id                = PG_SN.ramo_id                " & vbNewLine
        vSql = vSql & "                                         AND BF.beneficiario_id        = PG_SN.beneficiario_id)       " & vbNewLine
        vSql = vSql & "  INNER  JOIN    abss.seguros_db.dbo.sinistro_tb SN  with (nolock)           ON (SN.sinistro_id           = PG_SN.sinistro_id            " & vbNewLine
        vSql = vSql & "                                         AND SN.apolice_id             = PG_SN.apolice_id             " & vbNewLine
        vSql = vSql & "                                         AND SN.seguradora_cod_susep   = PG_SN.seguradora_cod_susep   " & vbNewLine
        vSql = vSql & "                                         AND SN.sucursal_seguradora_id = PG_SN.sucursal_seguradora_id " & vbNewLine
        vSql = vSql & "                                         AND SN.ramo_id                = PG_SN.ramo_id)               " & vbNewLine
        vSql = vSql & "  INNER  JOIN    abss.seguros_db.dbo.proposta_tb PP  with (nolock)           ON (PP.proposta_id           = SN.proposta_id)              " & vbNewLine
        vSql = vSql & "  INNER  JOIN    abss.seguros_db.dbo.produto_tb PR  with (nolock)            ON (PP.produto_id            = PR.produto_id                " & vbNewLine
        vSql = vSql & "                                         AND SN.seguradora_cod_susep   = PR.seguradora_cod_susep)     " & vbNewLine
        vSql = vSql & "   LEFT  JOIN    abss.seguros_db.dbo.agencia_tb AG  with (nolock)            ON (AG.agencia_id            = SN.agencia_id                " & vbNewLine
        vSql = vSql & "                                         AND AG.banco_id               = 1)                           " & vbNewLine
        If vTpRamo = "1" Then       'VIDA
            vSql = vSql & "  INNER  JOIN    abss.seguros_db.dbo.sinistro_vida_tb SN_VD  with (nolock)   ON (PG_SN.sinistro_id           = SN_VD.sinistro_id            " & vbNewLine
            vSql = vSql & "                                         AND PG_SN.apolice_id             = SN_VD.apolice_id             " & vbNewLine
            vSql = vSql & "                                         AND PG_SN.seguradora_cod_susep   = SN_VD.seguradora_cod_susep   " & vbNewLine
            vSql = vSql & "                                         AND PG_SN.sucursal_seguradora_id = SN_VD.sucursal_seguradora_id " & vbNewLine
            vSql = vSql & "                                         AND PG_SN.ramo_id                = SN_VD.ramo_id)               " & vbNewLine
        ElseIf vTpRamo = "2" Then   'RE
            vSql = vSql & "  INNER  JOIN    abss.seguros_db.dbo.cliente_tb CLI  with (nolock) ON (CLI.cliente_id              = SN.cliente_id)               " & vbNewLine
        End If
        vSql = vSql & "  INNER  JOIN    abss.seguros_db.dbo.ramo_tb RAMO  with (nolock) ON (RAMO.ramo_id                 = PG_SN.ramo_id)           " & vbNewLine
        vSql = vSql & "  INNER  JOIN    abss.seguros_db.dbo.ps_acerto_pagamento_tb PS_AC_PG  with (nolock) ON (PG_SN.acerto_id              = PS_AC_PG.acerto_id       " & vbNewLine
        vSql = vSql & "                                                 AND PS_AC_PG.cod_origem          = 'SN')                    " & vbNewLine
        '-- MFrasca - Flow 6223748 - 19/10/2010 - Altera��o para que seja poss�vel realizar a aprova��o antes de
        '-- se gerar a remessa ao PAGNET que ficar� condicionada a aprova��o do financeiro --
        'vSql = vSql & "  INNER  JOIN    abss.seguros_db.dbo.ps_remessa_voucher_tb PS_RM_VC  ON (PS_AC_PG.voucher_id          = PS_RM_VC.voucher_id      " & vbNewLine
        vSql = vSql & "  LEFT  JOIN    abss.seguros_db.dbo.ps_remessa_voucher_tb PS_RM_VC  with (nolock)  ON (PS_AC_PG.voucher_id          = PS_RM_VC.voucher_id      " & vbNewLine
        vSql = vSql & "                                                 AND PS_RM_VC.cod_origem          = 'SN')                    " & vbNewLine
        'vSql = vSql & "  INNER  JOIN    abss.seguros_db.dbo.ps_remessa_tb PS_RM             ON (PS_RM_VC.remessa_id          = PS_RM.remessa_id         " & vbNewLine
        vSql = vSql & "  LEFT  JOIN    abss.seguros_db.dbo.ps_remessa_tb PS_RM  with (nolock)             ON (PS_RM_VC.remessa_id          = PS_RM.remessa_id         " & vbNewLine
        vSql = vSql & "                                                 AND PS_RM.cod_sistema_ps         = 'AP')                    " & vbNewLine
        '-------------------------------------------------------------------------------------------------------------
        
        '' Alterado por Junior em 05/02/2003
        '' Foi inclu�do as linhas abaixo para obter somente os pagamentos que dever�o ser enviados via GTR
        vSql = vSql & "  INNER  JOIN    abss.seguros_db.dbo.evento_SEGBR_sinistro_tb EV  with (nolock)     ON (EV.sinistro_BB  = PG_SN.sinistro_BB                     " & vbNewLine
        vSql = vSql & "                                                 AND EV.num_recibo   = PG_SN.num_recibo                      " & vbNewLine
        vSql = vSql & "                                                 AND EV.evento_BB_id = 1152)                                 " & vbNewLine
            
        'MATHAYDE - 08/10/2009 - Aprova��o CDC
        'Join para � exibir o sinistro do CDC junto dos demais
        'vSql = vSql & "   LEFT  JOIN    abss.seguros_db.dbo.pgto_sinistro_lote_tb pgto_sinistro_lote_tb (NOLOCK)                            " & vbNewLine
        'vSql = vSql & "           ON    pgto_sinistro_lote_tb.sinistro_id  = PG_SN.sinistro_id                                      " & vbNewLine
        '' FIM
        
        '-- MFrasca - 24/11/2010 - Flow 6223748 - Ajuste para utilizar flag de aprova��o autom�tica do financeiro --
        vSql = vSql & "  INNER  JOIN   abss.seguros_db.dbo.forma_pgto_tb AS forma_pgto_tb WITH (NOLOCK) "
        vSql = vSql & "  ON     forma_pgto_tb.forma_pgto_id = PG_SN.forma_pgto_id "
        '-----------------------------------------------------------------------------------------------------------
        
        vSql = vSql & "  WHERE  PS_AC_PG.voucher_id       IS NOT NULL                                                               " & vbNewLine
        vSql = vSql & "    AND  PG_SN.situacao_op         = 'a'                                                                     " & vbNewLine
        vSql = vSql & "    AND  PG_SN.autorizado_SISBB    = 'n'                                                                     " & vbNewLine
        vSql = vSql & "    AND  PG_SN.retorno_recibo_BB   = 'n'                                                                     " & vbNewLine
        vSql = vSql & "    AND  PG_SN.item_val_estimativa = 1                                                                       " & vbNewLine
        vSql = vSql & "    AND  RAMO.tp_ramo_id           = " & vTpRamo & vbNewLine
        
        '-- MFrasca - 24/11/2010 - Flow 6223748 - Ajuste para utilizar flag de aprova��o autom�tica do financeiro --
        '-- Caso seja 'S' ser� aprovado automaticamente pela tarefa da agenda que roda a procedure SEGS8042_SPI   --
        vSql = vSql & "    AND  ISNULL(forma_pgto_tb.aprovacao_automatica_financeiro, 'N') = 'N' "
        '-----------------------------------------------------------------------------------------------------------
        
        'vSql = vSql & " AND     val_resseguro_recuperado         IS NULL"
            
        '* ALTERADO POR JUNIOR EM 09/09/2003
        '        vSql = vSql & "   AND  EXISTS (SELECT * FROM INTERFACE_DB..entrada_GTR_tb e"
        '        vSql = vSql & "                WHERE e.sinistro_BB = PG_SN.sinistro_bb"
        '        vSql = vSql & "                  AND e.evento_BB   = 2000)"
    
        'LSA 11/03/2010
        'ALTERA��O PARA O SINISNTRO DO SQG
        '        vSql = vSql & "     AND EXISTS (SELECT * FROM INTERFACE_DB..entrada_GTR_tb e"
        '        vSql = vSql & "                 WHERE e.sinistro_BB = PG_SN.sinistro_BB"
        '        vSql = vSql & "                   AND evento_BB = 2000"
        '        vSql = vSql & "                   AND NOT EXISTS (SELECT * FROM evento_SEGBR_sinistro_tb evento_SEGBR_sinistro_tb"
        '        vSql = vSql & "                                   WHERE evento_SEGBR_sinistro_tb.evento_SEGBR_id = 10130" '* EVENTO REABERTURA SEM COMUNICA��O
        '        vSql = vSql & "                                     AND evento_SEGBR_sinistro_tb.sinistro_BB = e.sinistro_BB"
        '        vSql = vSql & "                                     AND evento_SEGBR_sinistro_tb.dt_evento <= PG_SN.dt_solicitacao_pgto"
        '        vSql = vSql & "                                     AND evento_SEGBR_sinistro_tb.EVENTO_ID < EV.evento_id" 'Inclu�do por Gustavo Machado em 02/06/2004
        '        vSql = vSql & "                                     AND NOT EXISTS (SELECT * FROM evento_SEGBR_sinistro_tb ev1" 'Inclu�do por Gustavo Machado em 05/07/2004
        '        vSql = vSql & "                                                        WHERE ev1.sinistro_BB = e.sinistro_BB"
        '        vSql = vSql & "                                                        AND ev1.evento_bb_id = 1151"
        '        vSql = vSql & "                                                        AND ev1.evento_id > evento_SEGBR_sinistro_tb.evento_id)"
        '        vSql = vSql & "                                     AND NOT EXISTS (SELECT * FROM evento_SEGBR_sinistro_tb evento_SEGBR_sinistro_tb"
        '        vSql = vSql & "                                                     WHERE evento_SEGBR_sinistro_tb.evento_SEGBR_id = 10120" '* EVENTO REABERTURA SEM COMUNICA��O
        '        vSql = vSql & "                                                       AND evento_SEGBR_sinistro_tb.sinistro_BB = e.sinistro_BB"
        '        vSql = vSql & "                                                       AND evento_SEGBR_sinistro_tb.EVENTO_ID < EV.evento_id" 'Inclu�do por Gustavo Machado em 02/06/2004
        '        vSql = vSql & "                                                       AND evento_SEGBR_sinistro_tb.dt_evento <= PG_SN.dt_solicitacao_pgto)))"
        '* FIM
        'vSQL = vSQL & " AND     SN.dt_aviso_sinistro             >= '" & Data_Implantacao_GTR & "'"
            
        vSql = vSql & "    AND  PG_SN.dt_solicitacao_pgto >= '" & Data_Implantacao_GTR & "'                                         " & vbNewLine
        vSql = vSql & "    AND  ISNULL(EV.val_prejuizo_SISBB, 0) <=                                                                 "
        vSql = vSql & "                     (SELECT DISTINCT ISNULL(vl_max_aprovacao,0)                                             "
        vSql = vSql & "                        FROM abss.seguros_db.dbo.usuario_sisbb_tb  with (nolock)                             "
        vSql = vSql & "                       WHERE chave ='" & AChave_SISBB & "')                                                  " & vbNewLine
            
            
        'MATHAYDE - 08/10/2009 - Aprova��o CDC
        '� exibir o sinistro do CDC junto dos demais
        'vSql = vSql & "    AND  pgto_sinistro_lote_tb.sinistro_id  IS NULL                                                          " & vbNewLine
        
        'Alterado Alexandre Debouch - ConfitecSP 03/03/2010
        'Para situacao de sinistro que s� � usado em caso de Ramo = VIDA
        '        If vTpRamo = "1" Then
        '            vSql = vSql & "   AND       SN.situacao <> 7                                                    " & vbNewLine
        '        End If
        'Para situacao de proposta
        vSql = vSql & "   AND       PP.situacao <> 'T'                                                  " & vbNewLine
        
        Select Case cmbCampo.Text
                   
            Case "Sinistro BB" 'Sinistro BB
                If Len(txtConteudo) > 13 Then
                    mensagem_erro 6, "N�mero maior que o permitido!"
                    txtConteudo.Enabled = True
                    txtConteudo.SetFocus
                    Screen.MousePointer = 0
                    Exit Sub
                End If
                        
                vSql = vSql & "    AND  PG_SN.sinistro_bb = " & txtConteudo & vbNewLine
                        
            Case "N� do Sinistro" 'N�mero do Sinistro
                If Len(txtConteudo) > 11 Then
                    mensagem_erro 6, "N�mero maior que o permitido!"
                    txtConteudo.Enabled = True
                    txtConteudo.SetFocus
                    Screen.MousePointer = 0
                    Exit Sub
                End If
                        
                vSql = vSql & "    AND  PG_SN.sinistro_id = " & txtConteudo & vbNewLine
                        
            Case "Data da Remessa" 'Data da Remessa
                If (Len(txtConteudo) > 10 Or (Not IsDate(txtConteudo))) Then
                    mensagem_erro 6, "N�mero maior que o permitido!"
                    txtConteudo.Enabled = True
                    txtConteudo.SetFocus
                    Screen.MousePointer = 0
                    Exit Sub
                End If
                        
                vSql = vSql & "    AND  PS_RM.dt_remessa  = " & txtConteudo & vbNewLine
                        
            Case "N� da Lista de Autoriza��o" 'N�mero da Lista de Autoriza��o
                If Len(txtConteudo) > 9 Then
                    mensagem_erro 6, "N�mero maior que o permitido!"
                    txtConteudo.Enabled = True
                    txtConteudo.SetFocus
                    Screen.MousePointer = 0
                    Exit Sub
                End If
                        
                vSql = vSql & "    AND  PG_SN.num_lista   = " & txtConteudo & vbNewLine
                        
            Case "Voucher" 'C�digo do Voucher
                If Len(txtConteudo) > 9 Then
                    mensagem_erro 6, "N�mero maior que o permitido!"
                    txtConteudo.Enabled = True
                    txtConteudo.SetFocus
                    Screen.MousePointer = 0
                    Exit Sub
                End If
                        
                vSql = vSql & "    AND  PS_AC_PG.voucher_id = " & txtConteudo & vbNewLine
                        
            Case "N� da Autoriza��o" 'N�mero da Autoriza��o
                If Len(txtConteudo) > 9 Then
                    mensagem_erro 6, "N�mero maior que o permitido!"
                    txtConteudo.Enabled = True
                    txtConteudo.SetFocus
                    Screen.MousePointer = 0
                    Exit Sub
                End If
                        
                vSql = vSql & "    AND  PG_SN.chave_autorizacao_sinistro = " & txtConteudo & vbNewLine
                      
        End Select
    
        '''' BNEVES 14/03/2008 - FLOW 278709
        '''' TROCA DA CLAUSULA PG_SN.sinistro_id DO ORDER BY DE LUGAR
        '''' vSql = vSql & " ORDER BY PS_AC_PG.voucher_id, PG_SN.sinistro_id"
        '''' vSql = vSql & " ORDER BY PR.Produto_id, PG_SN.sinistro_id, PS_AC_PG.voucher_id, PG_SN.sinistro_id"
        vSql = vSql & "  ORDER  BY  PR.Produto_id,      " & vbNewLine
        vSql = vSql & "             PG_SN.sinistro_id,  " & vbNewLine
        vSql = vSql & "             PS_AC_PG.voucher_id " & vbNewLine
        '''' FIM
    
        vLog = Conexao_ExecutarSQL("SEGBR", glAmbiente_id, App.Title, App.FileDescription, vSql, vNunCx, False, False)
        'rdocn.Execute (vSQL)
    End If


    'MFrasca - Flow 6223748 - 10/11/2010 - Conforme solicita��o de Marcio Yoshimura e Marcelo Ianneta n�o deve
    'mais excluir os pagamentos relacionados ao produto SQG, para que todos sejam aprovados pelo Financeiro, por isso
    'o trecho a seguir foi comentado.
'
'    'LSA 11/03/2010
'    'ALTERA��O PARA O SINISNTRO DO SQG
'    vSql = ""
'    vSql = vSql & "DELETE #temp_lista" & vbNewLine
'    vSql = vSql & " WHERE produto_id = 719      " & vbNewLine
'    vSql = vSql & "   AND NOT EXISTS (SELECT *  " & vbNewLine
'    vSql = vSql & "                     FROM evento_SEGBR_sinistro_tb evento_2300                   " & vbNewLine
'    vSql = vSql & "                    WHERE evento_2300.sinistro_BB    = #temp_lista.sinistro_BB   " & vbNewLine
'    vSql = vSql & "                      AND evento_2300.evento_BB_id   IN (2300))                  " & vbNewLine
'
'    vLog = Conexao_ExecutarSQL("SEGBR", glAmbiente_id, App.Title, App.FileDescription, vSql, vNunCx, False, False)
    
    '-------------------------------------------------------------------------------------------------------------------
        
    'LSA 11/03/2010
    'ALTERA��O PARA O SINISNTRO DO SQG
    vSql = ""
    vSql = vSql & "DELETE #temp_lista" & vbNewLine
    vSql = vSql & "  FROM #temp_lista" & vbNewLine
    vSql = vSql & "  JOIN pgto_sinistro_tb PG_SN         ON PG_SN.sinistro_id   = #temp_lista.sinistro_id   " & vbNewLine
    vSql = vSql & "  JOIN evento_SEGBR_sinistro_tb EV    ON EV.sinistro_BB      = PG_SN.sinistro_BB         " & vbNewLine
    vSql = vSql & "                                     AND EV.num_recibo       = #temp_lista.num_recibo    " & vbNewLine
    vSql = vSql & "                                     AND EV.evento_BB_id     = 1152                      " & vbNewLine
    vSql = vSql & " WHERE #temp_lista.produto_id <> 719                                                     " & vbNewLine
    vSql = vSql & "   AND PG_SN.item_val_estimativa = 1                                                     " & vbNewLine
    vSql = vSql & "   AND NOT EXISTS (SELECT TOP 1 1                                                        " & vbNewLine
    vSql = vSql & "                     FROM INTERFACE_DB.dbo.entrada_GTR_tb e  with (nolock)               " & vbNewLine
    vSql = vSql & "                    WHERE e.sinistro_BB = PG_SN.sinistro_BB                              " & vbNewLine
    vSql = vSql & "                      AND evento_BB IN (2000)                                            " & vbNewLine
    vSql = vSql & "                      AND NOT EXISTS (SELECT TOP 1 1                                     " & vbNewLine
    vSql = vSql & "                                        FROM evento_SEGBR_sinistro_tb evento_SEGBR_sinistro_tb  with (nolock) " & vbNewLine
    vSql = vSql & "                                       WHERE evento_SEGBR_sinistro_tb.evento_SEGBR_id = 10130                " & vbNewLine
    vSql = vSql & "                                         AND evento_SEGBR_sinistro_tb.sinistro_BB = e.sinistro_BB            " & vbNewLine
    'Confitec Sistemas - DFerreira 09/08/2010 - Flow 4755378: Linha abaixo descomentada pois estava deletando o registro indevidamente
    ''DEMANDA 3.987.879 - Comentado Talitha 17.06.2010
    vSql = vSql & "                                         AND evento_SEGBR_sinistro_tb.dt_evento <= PG_SN.dt_solicitacao_pgto " & vbNewLine
    vSql = vSql & "                                         AND evento_SEGBR_sinistro_tb.EVENTO_ID < EV.evento_id               " & vbNewLine
    vSql = vSql & "                                         AND NOT EXISTS (SELECT TOP 1 1                                      " & vbNewLine
    vSql = vSql & "                                                           FROM evento_SEGBR_sinistro_tb ev1  with (nolock)  " & vbNewLine
    vSql = vSql & "                                                          WHERE ev1.sinistro_BB = e.sinistro_BB              " & vbNewLine
    vSql = vSql & "                                                            AND ev1.evento_bb_id = 1151                      " & vbNewLine
    vSql = vSql & "                                                            AND ev1.evento_id > evento_SEGBR_sinistro_tb.evento_id) " & vbNewLine
    vSql = vSql & "                                         AND NOT EXISTS (SELECT TOP 1 1                       " & vbNewLine
    vSql = vSql & "                                                           FROM evento_SEGBR_sinistro_tb evento_SEGBR_sinistro_tb  with (nolock) " & vbNewLine
    vSql = vSql & "                                                          WHERE evento_SEGBR_sinistro_tb.evento_SEGBR_id = 10120                  " & vbNewLine
    vSql = vSql & "                                                            AND evento_SEGBR_sinistro_tb.sinistro_BB = e.sinistro_BB              " & vbNewLine
    vSql = vSql & "                                                            AND evento_SEGBR_sinistro_tb.EVENTO_ID < EV.evento_id               ) " & vbNewLine
    'DEMANDA 3.987.879 - Comentado Talitha 17.06.2010
    'vSql = vSql & "                                                            AND evento_SEGBR_sinistro_tb.dt_evento <= PG_SN.dt_solicitacao_pgto)  " & vbNewLine
    vSql = vSql & "                                                        )" & vbNewLine
    vSql = vSql & "                                     )                   " & vbNewLine
        
    vLog = Conexao_ExecutarSQL("SEGBR", glAmbiente_id, App.Title, App.FileDescription, vSql, vNunCx, False, False)
        
    Count = 0
    prod_ant = 0
    prod_atual = 0
        
    lblSelecionados = ""
        
    'MATHAYDE - 08/10/2009 - Aprova��o CDC
    'Inserindo dados do CDC junto aos demais sinistros
    'Call CarregarDadosDoCDCnaTemporaria 'Processo removido devido ser desnecess�rio e afetar a performance

    'Rog�rio - 12/07/2007 - Essa parte do c�digo precisa ser revista.
    'Verifica se o aviso est� encerrado - Alexandre Queiroz - STF 28/06/2007 - Demanda: 204361
    'Esta altera��o � para n�o mostrar os avisos que est�o encerrados - situa��o 3
    'vSql = "DELETE FROM #TEMP_LISTA WHERE SINISTRO_ID IN( "
    'vSql = vSql & " SELECT DISTINCT SINISTRO_ID FROM EVENTO_SEGBR_SINISTRO_TB "
    'vSql = vSql & " WHERE SINISTRO_ID IN  (SELECT SINISTRO_ID FROM #TEMP_LISTA) "
    'vSql = vSql & " AND DT_EVENTO IN (  SELECT MAX(DT_EVENTO) FROM EVENTO_SEGBR_SINISTRO_TB EV"
    'vSql = vSql & "                     WHERE EV.SINISTRO_ID = EVENTO_SEGBR_SINISTRO_TB.SINISTRO_ID)"
    'vSql = vSql & " AND SITUACAO_SINISTRO = 3)"

    'vLog = Conexao_ExecutarSQL("SEGBR", _
    '                             glAmbiente_id, _
    '                             App.Title, _
    '                             App.FileDescription, _
    '                             vSql, _
    '                             vNunCx, _
    '                             False, False)
        
    ''' bneves - 14/02/2008 - FLOW 278709
    'vSql = "SELECT * FROM #temp_lista ORDER BY produto_id, voucher_id"
    '' BRUNO NEVES - bneves em 14/03/2008
    '' inclusao na clausula where do campo SINISTRO_ID
    '' vSql = "SELECT * FROM #temp_lista ORDER BY Nome_Produto, voucher_id"
    
    vSql = "SELECT * FROM #temp_lista ORDER BY Nome_Produto, sinistro_id, voucher_id"
    '' FIM

    Dim rs As ADODB.Recordset
    Set rs = New ADODB.Recordset
    Set rs = Conexao_ExecutarSQL("SEGBR", glAmbiente_id, App.Title, App.FileDescription, vSql, vNunCx, True, False)
        
    If Not rs.EOF Then
        rs.MoveFirst
        While Not rs.EOF
            prod_atual = rs!produto_id
            If prod_ant <> prod_atual Then
                'Monta a Linha do Cabe�alho com o Produto
                'Linha = "" & vbTab & "" & vbTab & "" & vbTab & "" & vbTab & rs!produto_id & " - " & rs!nome_produto
                Linha = "" & vbTab & "" & vbTab & "" & vbTab & rs!produto_id & " - " & rs!nome_produto
                   
                With flexSelecao
                    .AddItem Linha
                    '.Col = 4
                    .Col = 3
                    .Row = .Rows - 1
                    .CellFontBold = True
                    .CellAlignment = 1
                    .RowHeight(.Rows - 1) = 500
                End With
                   
                Linha = ""
                Linha = Linha & "0" & vbTab
                Linha = Linha & "" & vbTab
                Linha = Linha & rs!voucher_id & vbTab
                Linha = Linha & rs!nome_beneficiario & vbTab
                Linha = Linha & Format(rs!valor_total_geral, "#,###,##0.00") & vbTab
                Linha = Linha & rs!agencia_id & " - " & rs!nome_agencia & vbTab
                Linha = Linha & rs!sinistro_id & vbTab
                Linha = Linha & rs!sinistro_bb & vbTab
                Linha = Linha & rs!Apolice_id & vbTab
                Linha = Linha & rs!nome_segurado & vbTab
                Linha = Linha & Format(rs!dt_pagamento, "DD/MM/YYYY") & vbTab
                Linha = Linha & Obtem_Descr_Moeda(rs!moeda_id) & vbTab
                Linha = Linha & Format(rs!valor_pagamento, "#,###,##0.00")
                Linha = Linha & vbTab & Format(rs!valor_correcao, "#,###,##0.00") & vbTab
                Linha = Linha & Format(rs!valor_franquia, "#,###,##0.00") & vbTab
                Linha = Linha & Format(rs!valor_depreciacao, "#,###,##0.00") & vbTab
                Linha = Linha & Format(rs!valor_salvados, "#,###,##0.00") & vbTab
                Linha = Linha & Format(rs!valor_prejuizo_total, "#,###,##0.00") & vbTab
                Linha = Linha & rs!num_autorizacao & vbTab
                Linha = Linha & "" & rs!num_lista & vbTab
                Linha = Linha & rs!num_remessa & vbTab
                Linha = Linha & Format(rs!dt_remessa, "DD/MM/YYYY") & vbTab
                Linha = Linha & rs!sucursal_seguradora_id & vbTab
                Linha = Linha & rs!seguradora_cod_susep & vbTab
                Linha = Linha & rs!Ramo_id & vbTab
                Linha = Linha & rs!beneficiario_id & vbTab
                Linha = Linha & rs!num_recibo & vbTab
                
                '* Alterado por Alexandre Debouch
                'Linha = Linha & rs!destinatario_eMail
                Linha = Linha & rs!destinatario_eMail & vbTab
                Linha = Linha & rs!Ambiente & vbTab
                '* FIM
                Linha = Linha & rs!produto_id
                
                prod_ant = prod_atual
                Count = Count + 1
            Else
                Linha = ""
                Linha = Linha & "0" & vbTab
                Linha = Linha & "" & vbTab
                Linha = Linha & rs!voucher_id & vbTab
                Linha = Linha & rs!nome_beneficiario & vbTab
                Linha = Linha & Format(rs!valor_total_geral, "#,###,##0.00") & vbTab
                Linha = Linha & rs!agencia_id & " - " & rs!nome_agencia & vbTab
                Linha = Linha & rs!sinistro_id & vbTab
                Linha = Linha & rs!sinistro_bb & vbTab
                Linha = Linha & rs!Apolice_id & vbTab
                Linha = Linha & rs!nome_segurado & vbTab
                Linha = Linha & Format(rs!dt_pagamento, "DD/MM/YYYY") & vbTab
                Linha = Linha & Obtem_Descr_Moeda(rs!moeda_id) & vbTab
                Linha = Linha & Format(rs!valor_pagamento, "#,###,##0.00") & vbTab
                Linha = Linha & Format(rs!valor_correcao, "#,###,##0.00") & vbTab
                Linha = Linha & Format(rs!valor_franquia, "#,###,##0.00") & vbTab
                Linha = Linha & Format(rs!valor_depreciacao, "#,###,##0.00") & vbTab
                Linha = Linha & Format(rs!valor_salvados, "#,###,##0.00") & vbTab
                Linha = Linha & Format(rs!valor_prejuizo_total, "#,###,##0.00") & vbTab
                Linha = Linha & rs!num_autorizacao & vbTab
                Linha = Linha & "" & rs!num_lista & vbTab
                Linha = Linha & rs!num_remessa & vbTab
                Linha = Linha & Format(rs!dt_remessa, "DD/MM/YYYY") & vbTab
                Linha = Linha & rs!sucursal_seguradora_id & vbTab
                Linha = Linha & rs!seguradora_cod_susep & vbTab
                Linha = Linha & rs!Ramo_id & vbTab
                Linha = Linha & rs!beneficiario_id & vbTab
                Linha = Linha & rs!num_recibo & vbTab
                
                '* Alterado por Alexandre Debouch
                'Linha = Linha & rs!destinatario_eMail
                Linha = Linha & rs!destinatario_eMail & vbTab
                Linha = Linha & rs!Ambiente & vbTab
                '* FIM
                Linha = Linha & rs!produto_id
                
                Count = Count + 1
            End If
                
            With flexSelecao
                .AddItem Linha
                .Col = 4
                .Row = .Rows - 1
                .CellFontBold = False
                .RowHeight(.Rows - 1) = 250
            End With
                
            rs.MoveNext
            DoEvents
        Wend
        StatusBar1.SimpleText = "Clique no item desejado para exibir os dados."
    Else
        StatusBar1.SimpleText = "N�o existe(m) item(s) para esta sele��o."
        cmbCampo.SetFocus
    End If
    rs.Close
    Set rs = Nothing
    lblSelecionados = Str(Count) & " Iten(s) Encontrado(s)"
        
    flexSelecao.Refresh
    Screen.MousePointer = 0
        
    Exit Sub
        
TrataErro:
    TrataErroGeral "btnPesquisar_Click", Me.Caption
    TerminaSEGBR
        
End Sub

Private Sub cmbCampo_Click()
        
    txtConteudo.Enabled = True
    If Left(cmbCampo.Text, 5) = "Todos" Then
        txtConteudo = ""
        txtConteudo.Enabled = False
        btnPesquisar.SetFocus
    Else
        txtConteudo.Enabled = True
        txtConteudo.SetFocus
    End If
    StatusBar1.SimpleText = ""
        
End Sub

Private Sub cmdTodos_Click()
    Dim iTipo As String
    Dim iCont As Integer
    
    iCont = 2
    
    With flexSelecao
        .Row = 2
        While .Rows > iCont
             
             .Col = 1
           
            If .Col = 1 And .Row > 1 Then
                If .TextMatrix(.RowSel, GRID_SELECAO_ID_ACAO) <> "" Then
                    iTipo = .TextMatrix(.RowSel, GRID_SELECAO_ID_ACAO)
                    Call AlteraStatusFlexGrid(Val(iTipo) + 1)
                End If
            End If
            
            iCont = iCont + 1
            
            If .Rows - 1 > .Row Then
            .Row = .Row + 1
            End If
            
        Wend
    End With
End Sub



Private Sub Form_Load()
    vNunCx = -1
    Call CentraFrm(frmAprovarSISBB)
        
    tb_Temp_Criada = False
        
    Call MontaFlexGrid
    Call PreencheComboCampo
        
    '* Obt�m a data de implanta��o do GTR
    Data_Implantacao_GTR = Obtem_Data_Implantacao_GTR
        
    Me.Caption = "SEGP0420 - Aprova��o de Pagamentos de Sinistros para o SISBB " & Ambiente
    StatusBar1.Panels(2).Text = "Autenticado para o CPF.:  " & Format(cUserName, "@@@.@@@.@@@-@@")
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
        
    If Selecionou_Registro = True Then
        If MsgBox("Existem altera��es, deseja sair assim mesmo ?", vbYesNo + vbQuestion + vbDefaultButton2, "!!! Aten��o !!!") = vbNo Then Cancel = -1: Exit Sub
    End If
        
    Call TerminaSEGBR
        
End Sub

Private Sub flexSelecao_Click()
        
    Dim iTipo As String
        
    With flexSelecao
        If .Rows < 1 Then Exit Sub
             
        If .Col = 1 And .Row > 1 Then
            If .TextMatrix(.RowSel, GRID_SELECAO_ID_ACAO) = "" Then Exit Sub
            iTipo = .TextMatrix(.RowSel, GRID_SELECAO_ID_ACAO)
            Call AlteraStatusFlexGrid(Val(iTipo) + 1)
        End If
    End With
        
End Sub

Private Sub flexSelecao_DblClick()
        
    With flexSelecao
        If .Rows < 1 Then Exit Sub
             
        If .Col <> 1 Then
            If .TextMatrix(.RowSel, GRID_SELECAO_ID_ACAO) = "" Then Exit Sub
            'MATHAYDE - 08/10/2009 - Aprova��o CDC
            'Exibindo os dados detalhado do CDC no novo form
            If .TextMatrix(.RowSel, GRID_SELECAO_SINISTRO_ID) = "" Then
                Call ConfiguraInterfaceCDC(.TextMatrix(.RowSel, GRID_SELECAO_VOUCHER), .TextMatrix(.RowSel, GRID_SELECAO_APOLICE_ID), .TextMatrix(.RowSel, GRID_SELECAO_RAMO_ID))

            Else
                frmDetalhe.lblDetalhe(0) = .TextMatrix(.RowSel, GRID_SELECAO_VOUCHER)
                frmDetalhe.lblDetalhe(1) = .TextMatrix(.RowSel, GRID_SELECAO_AGENCIA)
                frmDetalhe.lblDetalhe(2) = .TextMatrix(.RowSel, GRID_SELECAO_SINISTRO_ID)
                frmDetalhe.lblDetalhe(3) = .TextMatrix(.RowSel, GRID_SELECAO_SINISTRO_BB)
                frmDetalhe.lblDetalhe(4) = .TextMatrix(.RowSel, GRID_SELECAO_APOLICE_ID)
                frmDetalhe.lblDetalhe(5) = .TextMatrix(.RowSel, GRID_SELECAO_NOME_SEGURADO)
                frmDetalhe.lblDetalhe(6) = .TextMatrix(.RowSel, GRID_SELECAO_DT_PGTO)
                frmDetalhe.lblDetalhe(7) = .TextMatrix(.RowSel, GRID_SELECAO_MOEDA)
                frmDetalhe.lblDetalhe(8) = .TextMatrix(.RowSel, GRID_SELECAO_VALOR_PGTO)
                frmDetalhe.lblDetalhe(9) = .TextMatrix(.RowSel, GRID_SELECAO_CORR_MONET)
                frmDetalhe.lblDetalhe(10) = .TextMatrix(.RowSel, GRID_SELECAO_NOME_BENEFICIARIO)
                frmDetalhe.lblDetalhe(11) = .TextMatrix(.RowSel, GRID_SELECAO_NUM_AUTORIZACAO)
                frmDetalhe.lblDetalhe(12) = .TextMatrix(.RowSel, GRID_SELECAO_NUM_LISTA)
                frmDetalhe.lblDetalhe(13) = .TextMatrix(.RowSel, GRID_SELECAO_NUM_REMESSA)
                frmDetalhe.lblDetalhe(14) = .TextMatrix(.RowSel, GRID_SELECAO_DT_REMESSA)
                frmDetalhe.lblDetalhe(15) = .TextMatrix(.RowSel, GRID_SELECAO_VALOR_FRANQUIA)
                frmDetalhe.lblDetalhe(16) = .TextMatrix(.RowSel, GRID_SELECAO_VALOR_DEPRECIACAO)
                frmDetalhe.lblDetalhe(17) = .TextMatrix(.RowSel, GRID_SELECAO_VALOR_SALVADOS)
                frmDetalhe.lblDetalhe(18) = .TextMatrix(.RowSel, GRID_SELECAO_VALOR_PREJUIZO_TOTAL)
                frmDetalhe.Show vbModal, frmAprovarSISBB
            End If
                    
        End If
    End With
        
End Sub

Private Sub txtConteudo_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        btnPesquisar_Click
    End If
End Sub

Private Sub PreencheComboCampo()
        
    With cmbCampo
        .Clear
        .ListIndex = -1
    End With
        
    With cmbCampo
        .AddItem "Todos"
        .AddItem "Sinistro BB"
        .AddItem "N� do Sinistro"
        .AddItem "Data da Remessa"
        .AddItem "N� da Lista de Autoriza��o"
        .AddItem "Voucher"
        .AddItem "N� da Autoriza��o"
    End With
        
End Sub

Private Sub Cria_Temp_Lista()
        
    Dim sSQL As String
        
    On Error GoTo TrataErro
        
    'Get para retorno do ID da conex�o
    'ID Usado para Trabalhar com transa��o
    If vNunCx = -1 Then
        vNunCx = AbrirConexao("SEGBR", glAmbiente_id, App.Title, App.FileDescription)
    End If
    'Abrindo a Transa��o
        
    If tb_Temp_Criada = True Then
        sSQL = "DROP TABLE #temp_lista"
        vLog = Conexao_ExecutarSQL("SEGBR", glAmbiente_id, App.Title, App.FileDescription, sSQL, vNunCx, False, False)
        'Set rs = rdocn.OpenResultset(sSQL)
        'rs.Close
    End If
        
    sSQL = "CREATE TABLE #temp_lista"
    sSQL = sSQL & " (voucher_id             int             NULL," & vbNewLine
    sSQL = sSQL & " agencia_id              numeric(5,0)    NULL," & vbNewLine
    sSQL = sSQL & " nome_agencia            varchar(60)     NULL," & vbNewLine
    sSQL = sSQL & " sinistro_id             numeric(11,0)   NULL," & vbNewLine
    sSQL = sSQL & " sucursal_seguradora_id  numeric(5,0)    NULL," & vbNewLine
    sSQL = sSQL & " seguradora_cod_susep    numeric(5,0)    NULL," & vbNewLine
    sSQL = sSQL & " ramo_id                 tinyint         NULL," & vbNewLine
    sSQL = sSQL & " sinistro_bb             numeric(13,0)   NULL," & vbNewLine
    sSQL = sSQL & " apolice_id              numeric(9,0)    NULL," & vbNewLine
    sSQL = sSQL & " segurado_id             numeric(11,0)   NULL," & vbNewLine
    sSQL = sSQL & " nome_segurado           varchar(60)     NULL," & vbNewLine
    sSQL = sSQL & " dt_pagamento            smalldatetime   NULL," & vbNewLine
    sSQL = sSQL & " moeda_id                numeric(5,0)    NULL," & vbNewLine
    sSQL = sSQL & " valor_pagamento         numeric(15,2)   NULL," & vbNewLine
    sSQL = sSQL & " valor_correcao          numeric(15,2)   NULL," & vbNewLine
    '* INCLU�DO POR JUNIOR EM 07/02/2003
    sSQL = sSQL & " valor_franquia          numeric(15,2)   NULL," & vbNewLine
    sSQL = sSQL & " valor_depreciacao       numeric(15,2)   NULL," & vbNewLine
    sSQL = sSQL & " valor_salvados          numeric(15,2)   NULL," & vbNewLine
    sSQL = sSQL & " valor_prejuizo_total    numeric(15,2)   NULL," & vbNewLine
    sSQL = sSQL & " valor_total_geral       numeric(15,2)   NULL," & vbNewLine
    '* FIM
    sSQL = sSQL & " beneficiario_id         tinyint         NULL," & vbNewLine
    sSQL = sSQL & " nome_beneficiario       varchar(60)     NULL," & vbNewLine
    sSQL = sSQL & " num_autorizacao         varchar(12)     NULL," & vbNewLine
    sSQL = sSQL & " num_lista               int             NULL," & vbNewLine
    sSQL = sSQL & " num_remessa             int             NULL," & vbNewLine
    sSQL = sSQL & " dt_remessa              smalldatetime   NULL," & vbNewLine
    sSQL = sSQL & " produto_id              int             NULL," & vbNewLine
    sSQL = sSQL & " nome_produto            varchar(60)     NULL," & vbNewLine
    sSQL = sSQL & " num_recibo              tinyint         NULL," & vbNewLine
        
    'Alterado por Alexandre Debouch - ConfitecSP - 05/03/2010
    'sSql = sSql & " destinatario_eMail      varchar(20)     NULL)" & vbNewLine
    sSQL = sSQL & " destinatario_eMail      varchar(20)     NULL," & vbNewLine
    sSQL = sSQL & " ambiente                int             NULL)" & vbNewLine
    '*FIM
    
    'Set rs = rdocn.OpenResultset(sSQL)
    'rs.Close
    'Executndo a opera��o passando como parametro o ID da Cx.
        
    vLog = Conexao_ExecutarSQL("SEGBR", glAmbiente_id, App.Title, App.FileDescription, sSQL, vNunCx, False, False)
                                   
    If vLog = True Then tb_Temp_Criada = True Else tb_Temp_Criada = False
        
    Exit Sub
        
TrataErro:
    mensagem_erro 6, "Erro ao criar tabela tempor�ria !"
    Call TerminaSEGBR
End Sub

Private Function Obtem_Descr_Moeda(odmMoeda_id As Integer) As String
        
    If odmMoeda_id <> descricao_moeda_id Then
        Dim SQL  As String
        Dim rsTB As ADODB.Recordset
        On Error GoTo TrataErro
            
        'With rdocn_Aux
        '     .Connect = rdocn.Connect
        '     .CursorDriver = rdUseNone
        '     .EstablishConnection rdDriverNoPrompt
        'End With
            
        SQL = "SELECT sigla FROM moeda_tb with (nolock)"
        SQL = SQL & " WHERE moeda_id = " & odmMoeda_id
            
        'Set rsTB = rdocn_Aux.OpenResultset(SQL)
    
        Set rsTB = New ADODB.Recordset
        Set rsTB = ExecutarSQL("SEGBR", glAmbiente_id, App.Title, App.FileDescription, SQL, True, False)
    
        If rsTB.EOF Then
            Obtem_Descr_Moeda = ""
        Else
            Obtem_Descr_Moeda = rsTB!sigla
        End If
        descricao_moeda = Obtem_Descr_Moeda
        descricao_moeda_id = odmMoeda_id
        rsTB.Close
        'rdocn_Aux.Close
    Else
        Obtem_Descr_Moeda = descricao_moeda
    End If
    Exit Function
        
TrataErro:
    mensagem_erro 6, "Conex�o com SEGBR indispon�vel."
    Call TerminaSEGBR
    
End Function

Private Sub MontaFlexGrid()
    
    With flexSelecao
        .Clear
        .Rows = 1
        
        '* Alterado por Alexandre Debouch - 05/03/2010
        '.Cols = 21
        .Cols = 28
        '* FIM        .RowHeightMin = 250
        
        .FixedCols = 0
        
        '* Alterado por Alexandre Debouch - 05/03/2010
        '.FormatString = "Id A��o | A��o | Voucher | Nome do Benefici�rio                               | Vlr Total Geral | Ag�ncia - Nome da Ag�ncia | N� do Sinistro        | N� do Sinistro BB | Ap�lice  | Nome do Segurado                                      | Data Pagto  | Moeda | Valor Pagto    | C. Monet. | Vlr Franquia | Vlr Deprecia��o | Vlr Salvado | Vlr Total Preju�zo | N� Autoriz. | N� Lista | N� Remessa | Dt Remessa | Suc. Seg. ID | Seg. Cod. Susep | Ramo ID | Benef. ID | Num. Recibo | Destinatario "
        .FormatString = "Id A��o | A��o | Voucher | Nome do Benefici�rio                               | Vlr Total Geral | Ag�ncia - Nome da Ag�ncia | N� do Sinistro        | N� do Sinistro BB | Ap�lice  | Nome do Segurado                                      | Data Pagto  | Moeda | Valor Pagto    | C. Monet. | Vlr Franquia | Vlr Deprecia��o | Vlr Salvado | Vlr Total Preju�zo | N� Autoriz. | N� Lista | N� Remessa | Dt Remessa | Suc. Seg. ID | Seg. Cod. Susep | Ramo ID | Benef. ID | Num. Recibo | Destinatario | Id.Ramo | Produto ID"
        '* FIM
        
        .ColWidth(GRID_SELECAO_ID_ACAO) = 0
        .ColWidth(GRID_SELECAO_AGENCIA) = 0
        .ColWidth(GRID_SELECAO_SINISTRO_BB) = 0
        .ColWidth(GRID_SELECAO_APOLICE_ID) = 0
        .ColWidth(GRID_SELECAO_NOME_SEGURADO) = 0
        '.ColWidth(GRID_SELECAO_NUM_LISTA) = 0
        .ColWidth(GRID_SELECAO_NUM_REMESSA) = 0
        .ColWidth(GRID_SELECAO_DT_REMESSA) = 0
        .ColWidth(GRID_SELECAO_SUCURSAL_SEG_ID) = 0
        .ColWidth(GRID_SELECAO_SEGURADORA_COD_SUSEP) = 0
        .ColWidth(GRID_SELECAO_RAMO_ID) = 0
        .ColWidth(GRID_SELECAO_BENEFICIARIO_ID) = 0
        .ColWidth(GRID_SELECAO_NUM_RECIBO) = 0
        .ColWidth(GRID_SELECAO_DESTINATARIO) = 0
    
        '* Alterado por Alexandre Debouch - 05/03/2010
        .ColWidth(GRID_SELECAO_AMBIENTE) = 0
        '* FIM
    
    End With
        
End Sub

Private Sub AlteraStatusFlexGrid(mTipoImagem As Byte)
        
    '--------------------------------------------------------------------'
    ' Altera a Imagem referente ao status da Remessa                     '
    '--------------------------------------------------------------------'
    ' Utiliza o mTipoImagem para Colocar a Figura Correspondente         '
    ' 0-Vazio; 1-Confirmar; 2-Cancelar                                   '
    '--------------------------------------------------------------------'
    If mTipoImagem > 2 Then mTipoImagem = 0
    With flexSelecao
        If .Row = 0 Then Exit Sub
        .TextMatrix(.RowSel, GRID_SELECAO_ID_ACAO) = mTipoImagem
        .Col = 1
        .CellPictureAlignment = 3
        .Row = .RowSel
        If mTipoImagem = 0 Then
            Set .CellPicture = Nothing
        Else
            Set .CellPicture = imglstAcao.ListImages(mTipoImagem).Picture
        End If
    End With
        
End Sub

Private Function Selecionou_Registro() As Boolean
        
    Dim i As Long
        
    Selecionou_Registro = False
        
    If flexSelecao.Rows = 1 Then Exit Function
        
    For i = 1 To flexSelecao.Rows - 1
        If Val(flexSelecao.TextMatrix(i, GRID_SELECAO_ID_ACAO)) <> 0 Then
            Selecionou_Registro = True
            Exit For
        End If
    Next
        
End Function

Private Sub Aprovar_SISBB(aSinistro_id As String, _
                          aApolice_id As String, _
                          aSucursal_Seguradora_id As String, _
                          aSeguradora_cod_Susep As String, _
                          aRamo_id As String, _
                          aBeneficiario_id As String, _
                          aSinistro_BB As String, _
                          aVoucher_id As String)
        
    On Error GoTo TrataErro
        
    vSql = "EXEC pgto_sin_aprova_SISBB_spu "
    vSql = vSql & aSinistro_id & ", "
    vSql = vSql & aApolice_id & ", "
    vSql = vSql & aSucursal_Seguradora_id & ", "
    vSql = vSql & aSeguradora_cod_Susep & ", "
    vSql = vSql & aRamo_id & ", "
    vSql = vSql & aBeneficiario_id & ", "
    vSql = vSql & aSinistro_BB & ", "
    vSql = vSql & aVoucher_id & ", '"
    vSql = vSql & cUserName & "', '"
    vSql = vSql & AChave_SISBB & "'"
    'rdocn.Execute (vSQL)
    vLog = Conexao_ExecutarSQL("SEGBR", glAmbiente_id, App.Title, App.FileDescription, vSql, vNunCx, False, False)
        
    Exit Sub
        
TrataErro:
    'rdocn.RollbackTrans
    vLog = RetornarTransacao(vNunCx)
    mensagem_erro 6, "Erro ao Aprovar SISBB."
    Call TerminaSEGBR
    
End Sub

Private Sub Estornar_Voucher(evVoucher_id As String)
        
    Dim rsTB            As ADODB.Recordset
    Dim vSinistro_id    As String, vApolice_id  As String, vSucursal_id         As String
    Dim vSeguradora_id  As String, vRamo_id     As String, vItem_val_Estimativa As String
    Dim vSeq_Estimativa As String, vSeq_Pgto    As String, vBeneficiario_id     As String
    Dim vNum_Parcela    As String
        
    On Error GoTo TrataErro
        
    vSql = "SELECT  p.sinistro_id, p.apolice_id, p.sucursal_seguradora_id, p.seguradora_cod_susep, p.ramo_id,"
    vSql = vSql & " p.Item_Val_Estimativa , p.Seq_Estimativa, p.Num_parcela, p.Seq_Pgto, p.beneficiario_Id"
    vSql = vSql & " FROM    pgto_sinistro_tb p  with (nolock) "
    vSql = vSql & "     INNER JOIN ps_acerto_pagamento_tb ps  with (nolock) "
    vSql = vSql & "         ON (ps.acerto_id = p.acerto_id)"
    vSql = vSql & " WHERE   ps.voucher_id = " & evVoucher_id
    'Set rsTB = rdocn.OpenResultset(vSQL)
    Set rsTB = ExecutarSQL("SEGBR", glAmbiente_id, App.Title, App.FileDescription, vSql, True, False)
    If rsTB.EOF Then
        rsTB.Close
        vLog = RetornarTransacao(vNunCx)
        'rdocn.RollbackTrans
        MsgBox "Registro selecionado para ser estornado n�o foi encontrado !", vbOKOnly + vbExclamation
        Exit Sub
    End If
        
    vSinistro_id = rsTB!sinistro_id
    vApolice_id = rsTB!Apolice_id
    vSucursal_id = rsTB!sucursal_seguradora_id
    vSeguradora_id = rsTB!seguradora_cod_susep
    vRamo_id = rsTB!Ramo_id
    vItem_val_Estimativa = rsTB!Item_val_estimativa
    vSeq_Estimativa = rsTB!Seq_Estimativa
    vSeq_Pgto = rsTB!seq_pgto
    vNum_Parcela = rsTB!num_parcela
    vBeneficiario_id = rsTB!beneficiario_id
    rsTB.Close
        
    vSql = "EXEC pgto_sinistro_alt_sit_spu  "
    vSql = vSql & vSinistro_id & ", "
    vSql = vSql & vApolice_id & ", "
    vSql = vSql & vSucursal_id & ", "
    vSql = vSql & vSeguradora_id & ", "
    vSql = vSql & vRamo_id & ", "
    vSql = vSql & vItem_val_Estimativa & ", "
    vSql = vSql & vSeq_Estimativa & ", "
    vSql = vSql & vSeq_Pgto & ", "
    vSql = vSql & vNum_Parcela & ", "
    vSql = vSql & vBeneficiario_id & ", "
    vSql = vSql & "'e', '', "
    vSql = vSql & "'" & cUserName & "'"
    'rdocn.Execute (vSQL)
    vLog = Conexao_ExecutarSQL("SEGBR", glAmbiente_id, App.Title, App.FileDescription, vSql, vNunCx, False, False)
    Exit Sub
        
TrataErro:
    'rdocn.RollbackTrans
    vLog = RetornarTransacao(vNunCx)
    mensagem_erro 6, "Erro ao estornar Voucher n�.: " & evVoucher_id & " ."
    Call TerminaSEGBR
    
End Sub

Private Sub Enviar_Email_Estorno(ByVal APosicao As Integer)
    
    Dim strEMail As String
    
    strEMail = "Sinistro BB: " & flexSelecao.TextMatrix(APosicao, GRID_SELECAO_SINISTRO_BB) & vbNewLine
    strEMail = strEMail & "Sinistro: " & flexSelecao.TextMatrix(APosicao, GRID_SELECAO_SINISTRO_ID) & vbNewLine
    strEMail = strEMail & "Ap�lice: " & flexSelecao.TextMatrix(APosicao, GRID_SELECAO_APOLICE_ID) & vbNewLine
    strEMail = strEMail & "Ramo: " & flexSelecao.TextMatrix(APosicao, GRID_SELECAO_RAMO_ID) & vbNewLine
    strEMail = strEMail & "Benefici�rio: " & flexSelecao.TextMatrix(APosicao, GRID_SELECAO_NOME_BENEFICIARIO) & vbNewLine
    strEMail = strEMail & "Data do Estorno: " & Format(Data_Sistema, "dd/mm/yyyy") & vbNewLine
    strEMail = strEMail & "Estornado por: " & cUserName & vbNewLine
    strEMail = strEMail & "N� Voucher: " & flexSelecao.TextMatrix(APosicao, GRID_SELECAO_VOUCHER) & vbNewLine
    strEMail = strEMail & "Data Pagto: " & flexSelecao.TextMatrix(APosicao, GRID_SELECAO_DT_PGTO) & vbNewLine
    strEMail = strEMail & "Valor Pagto: " & flexSelecao.TextMatrix(APosicao, GRID_SELECAO_VALOR_PGTO) & vbNewLine
    strEMail = strEMail & "N� Autoriza��o: " & flexSelecao.TextMatrix(APosicao, GRID_SELECAO_NUM_AUTORIZACAO) & vbNewLine
    strEMail = strEMail & "N� Recibo: " & flexSelecao.TextMatrix(APosicao, GRID_SELECAO_NUM_RECIBO) & vbNewLine & vbNewLine
    Destinatario = flexSelecao.TextMatrix(APosicao, GRID_SELECAO_DESTINATARIO)
    'Destinatario = "jopereira"
    
    ' Manda e-mail
    ctleMail.Enviar_Email cUserName, Destinatario, "Estorno de pagamento do SISBB", strEMail
    
End Sub

Private Function Obtem_Data_Implantacao_GTR() As String
    
    Dim objAmbiente As Object
    
    Set objAmbiente = CreateObject("SABL0010.cls00009")
    
    Obtem_Data_Implantacao_GTR = objAmbiente.RetornaValorAmbiente("GTR", "SISTEMA", "DATA_IMPLANTACAO_GTR", gsCHAVESISTEMA)
    
    Set objAmbiente = Nothing
        
End Function

'MATHAYDE - 08/10/2009 - Aprova��o CDC
'Junta os dados do CDC aos demais dados da tempor�ria que ir� carregar o grid
Private Sub CarregarDadosDoCDCnaTemporaria()

    Dim sSQL     As String
    Dim QueryCdc As String
        
    On Error GoTo TrataErro
       
    sSQL = ""
    sSQL = sSQL & "INSERT INTO #temp_lista (" & vbNewLine
    sSQL = sSQL & "       voucher_id" & vbNewLine
    sSQL = sSQL & "     , agencia_id" & vbNewLine
    sSQL = sSQL & "     , nome_agencia" & vbNewLine
    sSQL = sSQL & "     , sinistro_id" & vbNewLine
    sSQL = sSQL & "     , sucursal_seguradora_id" & vbNewLine
    sSQL = sSQL & "     , seguradora_cod_susep" & vbNewLine
    sSQL = sSQL & "     , ramo_id" & vbNewLine
    sSQL = sSQL & "     , sinistro_bb" & vbNewLine
    sSQL = sSQL & "     , apolice_id" & vbNewLine
    sSQL = sSQL & "     , segurado_id" & vbNewLine
    sSQL = sSQL & "     , nome_segurado" & vbNewLine
    sSQL = sSQL & "     , dt_pagamento" & vbNewLine
    sSQL = sSQL & "     , moeda_id" & vbNewLine
    sSQL = sSQL & "     , valor_pagamento" & vbNewLine
    sSQL = sSQL & "     , valor_correcao" & vbNewLine
    sSQL = sSQL & "     , valor_franquia" & vbNewLine
    sSQL = sSQL & "     , valor_depreciacao" & vbNewLine
    sSQL = sSQL & "     , valor_salvados" & vbNewLine
    sSQL = sSQL & "     , valor_prejuizo_total" & vbNewLine
    sSQL = sSQL & "     , valor_total_geral" & vbNewLine
    sSQL = sSQL & "     ,  beneficiario_id" & vbNewLine
    sSQL = sSQL & "     , nome_beneficiario" & vbNewLine
    sSQL = sSQL & "     , num_autorizacao" & vbNewLine
    sSQL = sSQL & "     , num_lista" & vbNewLine
    sSQL = sSQL & "     , num_remessa" & vbNewLine
    sSQL = sSQL & "     , dt_remessa" & vbNewLine
    sSQL = sSQL & "     , produto_id" & vbNewLine
    sSQL = sSQL & "     , nome_produto" & vbNewLine
    sSQL = sSQL & "     , num_recibo" & vbNewLine
    '*Alterado por Alexandre Debouch - ConfitecSP - 05/03/2010
    'sSQL = sSQL & "     , destinatario_eMail )" & vbNewLine
    sSQL = sSQL & "     , destinatario_eMail " & vbNewLine
    sSQL = sSQL & "     , Ambiente) " & vbNewLine
    
    QueryCdc = MontaQueryCDC(CDC_SINTETICO)
    
    sSQL = sSQL & QueryCdc
    
    Call Conexao_ExecutarSQL("SEGBR", glAmbiente_id, App.Title, App.FileDescription, sSQL, vNunCx, False, False)
                                   
    Exit Sub
        
TrataErro:
    mensagem_erro 6, "Erro ao carregar dados do CDC na tabela tempor�ria !"
    Call TerminaSEGBR

End Sub

'MATHAYDE - 08/10/2009 - Aprova��o CDC
'Detalha os dados do CDC no novo form
Private Sub ConfiguraInterfaceCDC(ByVal lVoucher As Long, _
                                  ByVal iApolice As Integer, _
                                  ByVal iRamo As Integer)
                                  
    Dim sSQL     As String
    Dim sLinha   As String
    Dim QueryCdc As String
    Dim rs       As ADODB.Recordset
     
    QueryCdc = MontaQueryCDC(CDC_ANALITICO, lVoucher, iRamo, iApolice)
        
    sSQL = ""
    sSQL = sSQL & QueryCdc
    
    Set rs = Conexao_ExecutarSQL("SEGBR", glAmbiente_id, App.Title, App.FileDescription, sSQL, vNunCx, True, False)
    While Not rs.EOF
        
        sLinha = ""
        sLinha = sLinha & rs!voucher_id & vbTab
        sLinha = sLinha & rs!nome_beneficiario & vbTab
        sLinha = sLinha & Format(rs!valor_total_geral, "#,###,##0.00") & vbTab
        sLinha = sLinha & rs!agencia_id & " - " & rs!nome_agencia & vbTab
        sLinha = sLinha & rs!sinistro_id & vbTab
        sLinha = sLinha & rs!sinistro_bb & vbTab
        sLinha = sLinha & rs!Apolice_id & vbTab
        sLinha = sLinha & rs!nome_segurado & vbTab
        sLinha = sLinha & Format(rs!dt_pagamento, "DD/MM/YYYY") & vbTab
        sLinha = sLinha & Obtem_Descr_Moeda(rs!moeda_id) & vbTab
        sLinha = sLinha & Format(rs!valor_pagamento, "#,###,##0.00") & vbTab
        sLinha = sLinha & Format(rs!valor_correcao, "#,###,##0.00") & vbTab
        sLinha = sLinha & Format(rs!valor_franquia, "#,###,##0.00") & vbTab
        sLinha = sLinha & Format(rs!valor_depreciacao, "#,###,##0.00") & vbTab
        sLinha = sLinha & Format(rs!valor_salvados, "#,###,##0.00") & vbTab
        sLinha = sLinha & Format(rs!valor_prejuizo_total, "#,###,##0.00") & vbTab
        sLinha = sLinha & rs!num_autorizacao & vbTab & "" & rs!num_lista & vbTab
        sLinha = sLinha & rs!num_remessa & vbTab & Format(rs!dt_remessa, "DD/MM/YYYY") & vbTab
        sLinha = sLinha & rs!sucursal_seguradora_id & vbTab & rs!seguradora_cod_susep & vbTab
        sLinha = sLinha & rs!Ramo_id & vbTab
        sLinha = sLinha & rs!beneficiario_id & vbTab
        sLinha = sLinha & rs!num_recibo & vbTab
        sLinha = sLinha & rs!destinatario_eMail
        
        FrmDetalheCDC.flexSelecaoCDC.AddItem (sLinha)
        
        rs.MoveNext
    Wend
        
    rs.Close
    
    Set rs = Nothing
    
    FrmDetalheCDC.Show vbModal, frmAprovarSISBB
    
    Exit Sub
        
TrataErro:
    mensagem_erro 6, "Erro ao carregar dados do CDC na tabela tempor�ria !"
    Call TerminaSEGBR

End Sub

'MATHAYDE - 08/10/2009 - Aprova��o CDC
'Monta query para consultar CDC
Public Function MontaQueryCDC(ByVal sTipo As String, _
                              Optional ByVal lVoucher As Long, _
                              Optional ByVal iRamo As Integer, _
                              Optional ByVal iApolice As Integer) As String
    ' sTipo = 0 - Sint�tico
    ' sTipo = 1 - Detalhado

    Dim sSQL As String
        
    On Error GoTo TrataErro
    
    'jose.souza - Confitec - Em 16/08/2013
    bUsarConexaoABS = False
    
    sSQL = ""
    If sTipo = 1 Then 'DETALHADO
        sSQL = sSQL & "  SELECT voucher_id                   = PS_AC_PG.voucher_id                      " & vbNewLine
        sSQL = sSQL & "       , SN.agencia_id                                                           " & vbNewLine
        sSQL = sSQL & "       , nome_agencia                 = AG.nome                                  " & vbNewLine
        sSQL = sSQL & "       , sinistro_id                  = PG_SN.sinistro_id                        " & vbNewLine
        sSQL = sSQL & "       , PG_SN.sucursal_seguradora_id                                            " & vbNewLine
        sSQL = sSQL & "       , PG_SN.seguradora_cod_susep                                              " & vbNewLine
        sSQL = sSQL & "       , PG_SN.ramo_id                                                           " & vbNewLine
        sSQL = sSQL & "       , sinistro_bb                  = PG_SN.sinistro_bb                        " & vbNewLine
        sSQL = sSQL & "       , PG_SN.apolice_id                                                        " & vbNewLine
        sSQL = sSQL & "       , SN_VD.cpf                                                               " & vbNewLine
        sSQL = sSQL & "       , nome_segurado                = SN_VD.nome                               " & vbNewLine
        sSQL = sSQL & "       , dt_pagamento                 = PG_SN.dt_acerto_contas_sinistro          " & vbNewLine
        sSQL = sSQL & "       , SN.moeda_id                                                             " & vbNewLine
        sSQL = sSQL & "       , valor_pagamento              = PG_SN.val_acerto                         " & vbNewLine
        sSQL = sSQL & "       , valor_correcao               = PG_SN.val_correcao                       " & vbNewLine
        sSQL = sSQL & "       , valor_franquia               = PG_SN.val_franquia_SISBB                 " & vbNewLine
        sSQL = sSQL & "       , valor_depreciacao            = PG_SN.val_depreciacao_SISBB              " & vbNewLine
        sSQL = sSQL & "       , valor_salvados               = PG_SN.val_salvado_SISBB                  " & vbNewLine
        sSQL = sSQL & "       , valor_prejuizo_total         = ISNULL(EV.val_prejuizo_SISBB,0)          " & vbNewLine
        sSQL = sSQL & "       , valor_total_geral            = ISNULL(EV.val_prejuizo_SISBB,0) + ISNULL(PG_SN.val_correcao,0) + ISNULL(PG_SN.val_franquia_SISBB,0)  " & vbNewLine
        sSQL = sSQL & "       , beneficiario_id              = PG_SN.beneficiario_id                    " & vbNewLine
        sSQL = sSQL & "       , nome_beneficiario            = BF.nome                                  " & vbNewLine
        sSQL = sSQL & "       , num_autorizacao              = CONVERT(varchar(10),PG_SN.chave_autorizacao_sinistro) + '-' + PG_SN.chave_autorizacao_sinistro_dv    " & vbNewLine
        sSQL = sSQL & "       , PG_SN.num_lista                                                         " & vbNewLine
        sSQL = sSQL & "       , num_remessa                  = NULL --PS_RM_VC.remessa_id               " & vbNewLine
        sSQL = sSQL & "       , dt_remessa                   = NULL --PS_RM.dt_remessa                  " & vbNewLine
        sSQL = sSQL & "       , produto_id                   = PR.produto_id                            " & vbNewLine
        sSQL = sSQL & "       , nome_produto                 = PR.nome                                  " & vbNewLine
        sSQL = sSQL & "       , num_recibo                   = PG_SN.num_recibo                         " & vbNewLine
        sSQL = sSQL & "       , destinatario_eMail           = PG_SN.usuario                            " & vbNewLine
        
        '* Alterado por Alexandre Debouch - ConfitecSP - 05/03/2010
        sSQL = sSQL & "       , 'Ambiente'                  = " & glAmbiente_id & vbNewLine
     
    Else 'SINTETICO
        sSQL = sSQL & "  SELECT voucher_id                   = PS_AC_PG.voucher_id                      " & vbNewLine
        sSQL = sSQL & "       , SN.agencia_id                                                           " & vbNewLine
        sSQL = sSQL & "       , nome_agencia                 = AG.nome                                  " & vbNewLine
        sSQL = sSQL & "       , sinistro_id                  = NULL --PG_SN.sinistro_id                 " & vbNewLine
        sSQL = sSQL & "       , PG_SN.sucursal_seguradora_id                                            " & vbNewLine
        sSQL = sSQL & "       , PG_SN.seguradora_cod_susep                                              " & vbNewLine
        sSQL = sSQL & "       , PG_SN.ramo_id                                                           " & vbNewLine
        sSQL = sSQL & "       , sinistro_bb                  = NULL --PG_SN.sinistro_bb                 " & vbNewLine
        sSQL = sSQL & "       , PG_SN.apolice_id                                                        " & vbNewLine
        sSQL = sSQL & "       , SN_VD.cpf                                                               " & vbNewLine
        sSQL = sSQL & "       , nome_segurado                = SN_VD.nome                               " & vbNewLine
        sSQL = sSQL & "       , dt_pagamento                 = NULL --PG_SN.dt_acerto_contas_sinistro   " & vbNewLine
        sSQL = sSQL & "       , SN.moeda_id                                                             " & vbNewLine
        sSQL = sSQL & "       , valor_pagamento              = SUM(PG_SN.val_acerto)                    " & vbNewLine
        sSQL = sSQL & "       , valor_correcao               = SUM(PG_SN.val_correcao)                  " & vbNewLine
        sSQL = sSQL & "       , valor_franquia               = SUM(PG_SN.val_franquia_SISBB)            " & vbNewLine
        sSQL = sSQL & "       , valor_depreciacao            = SUM(PG_SN.val_depreciacao_SISBB)         " & vbNewLine
        sSQL = sSQL & "       , valor_salvados               = SUM(PG_SN.val_salvado_SISBB)             " & vbNewLine
        sSQL = sSQL & "       , valor_prejuizo_total         = SUM(ISNULL(EV.val_prejuizo_SISBB,0))     " & vbNewLine
        sSQL = sSQL & "       , valor_total_geral            = SUM(ISNULL(EV.val_prejuizo_SISBB,0) + ISNULL(PG_SN.val_correcao,0) + ISNULL(PG_SN.val_franquia_SISBB,0)) " & vbNewLine
        sSQL = sSQL & "       , beneficiario_id              = NULL --PG_SN.beneficiario_id             " & vbNewLine
        sSQL = sSQL & "       , nome_beneficiario            = NULL --BF.nome                           " & vbNewLine
        sSQL = sSQL & "       , num_autorizacao              = CONVERT(varchar(10),PG_SN.chave_autorizacao_sinistro) + '-' + PG_SN.chave_autorizacao_sinistro_dv        " & vbNewLine
        sSQL = sSQL & "       , PG_SN.num_lista                                                         " & vbNewLine
        sSQL = sSQL & "       , num_remessa                  = NULL --PS_RM_VC.remessa_id               " & vbNewLine
        sSQL = sSQL & "       , dt_remessa                   = NULL --PS_RM.dt_remessa                  " & vbNewLine
        sSQL = sSQL & "       , produto_id                   = PR.produto_id                            " & vbNewLine
        sSQL = sSQL & "       , nome_produto                 = PR.nome                                  " & vbNewLine
        sSQL = sSQL & "       , num_recibo                   = NULL --PG_SN.num_recibo                  " & vbNewLine
        sSQL = sSQL & "       , destinatario_eMail           = NULL --PG_SN.usuario                     " & vbNewLine
        '* Alterado por Alexandre Debouch - ConfitecSP - 05/03/2010
        sSQL = sSQL & "       , 'Ambiente'                  = " & glAmbiente_id & vbNewLine

    End If
    
    sSQL = sSQL & "    FROM seguros_db..pgto_sinistro_lote_tb pgto_sinistro_lote_tb with (NOLOCK)                                                            " & vbNewLine
    sSQL = sSQL & "    JOIN seguros_db..pgto_sinistro_tb PG_SN with (NOLOCK)                                                                                 " & vbNewLine
    sSQL = sSQL & "      ON PG_SN.sinistro_id = pgto_sinistro_lote_tb.sinistro_id                                                                       " & vbNewLine
    sSQL = sSQL & "   INNER JOIN sinistro_benef_tb BF  with (nolock)                                                                                                   " & vbNewLine
    sSQL = sSQL & "      ON BF.sinistro_id                = PG_SN.sinistro_id                                                                           " & vbNewLine
    sSQL = sSQL & "     AND BF.apolice_id                 = PG_SN.apolice_id                                                                            " & vbNewLine
    sSQL = sSQL & "     AND BF.seguradora_cod_susep       = PG_SN.seguradora_cod_susep                                                                  " & vbNewLine
    sSQL = sSQL & "     AND BF.sucursal_seguradora_id     = PG_SN.sucursal_seguradora_id                                                                " & vbNewLine
    sSQL = sSQL & "     AND BF.ramo_id                    = PG_SN.ramo_id                                                                               " & vbNewLine
    sSQL = sSQL & "     AND BF.beneficiario_id            = PG_SN.beneficiario_id                                                                       " & vbNewLine
    sSQL = sSQL & "   INNER JOIN sinistro_tb SN  with (nolock)                                                                                                         " & vbNewLine
    sSQL = sSQL & "      ON SN.sinistro_id                = PG_SN.sinistro_id                                                                           " & vbNewLine
    sSQL = sSQL & "     AND SN.apolice_id                 = PG_SN.apolice_id                                                                            " & vbNewLine
    sSQL = sSQL & "     AND SN.seguradora_cod_susep       = PG_SN.seguradora_cod_susep                                                                  " & vbNewLine
    sSQL = sSQL & "     AND SN.sucursal_seguradora_id     = PG_SN.sucursal_seguradora_id                                                                " & vbNewLine
    sSQL = sSQL & "     AND SN.ramo_id                    = PG_SN.ramo_id                                                                               " & vbNewLine
    sSQL = sSQL & "   INNER JOIN proposta_tb PP  with (nolock)                                                                                                         " & vbNewLine
    sSQL = sSQL & "      ON PP.proposta_id                = SN.proposta_id                                                                              " & vbNewLine
    sSQL = sSQL & "   INNER JOIN produto_tb PR  with (nolock)                                                                                                          " & vbNewLine
    sSQL = sSQL & "      ON PP.produto_id                 = PR.produto_id                                                                               " & vbNewLine
    sSQL = sSQL & "     AND SN.seguradora_cod_susep       = PR.seguradora_cod_susep                                                                     " & vbNewLine
    sSQL = sSQL & "    LEFT JOIN agencia_tb AG  with (nolock)                                                                                                          " & vbNewLine
    sSQL = sSQL & "      ON AG.agencia_id                  = SN.agencia_id                                                                              " & vbNewLine
    sSQL = sSQL & "     AND AG.banco_id                    = 1                                                                                          " & vbNewLine
    sSQL = sSQL & "   INNER JOIN sinistro_vida_tb SN_VD  with (nolock)                                                                                                 " & vbNewLine
    sSQL = sSQL & "      ON (PG_SN.sinistro_id            = SN_VD.sinistro_id                                                                           " & vbNewLine
    sSQL = sSQL & "     AND PG_SN.apolice_id              = SN_VD.apolice_id                                                                            " & vbNewLine
    sSQL = sSQL & "     AND PG_SN.seguradora_cod_susep    = SN_VD.seguradora_cod_susep                                                                  " & vbNewLine
    sSQL = sSQL & "     AND PG_SN.sucursal_seguradora_id  = SN_VD.sucursal_seguradora_id                                                                " & vbNewLine
    sSQL = sSQL & "     AND PG_SN.ramo_id                 = SN_VD.ramo_id      )                                                                        " & vbNewLine
    sSQL = sSQL & "   INNER JOIN ramo_tb RAMO  with (nolock)                                                                                                           " & vbNewLine
    sSQL = sSQL & "      ON (RAMO.ramo_id                 = PG_SN.ramo_id      )                                                                        " & vbNewLine
    
    sSQL = sSQL & "   INNER JOIN ps_acerto_pagamento_tb PS_AC_PG  with (nolock)                                                                                        " & vbNewLine
    sSQL = sSQL & "      ON (PG_SN.acerto_id              = PS_AC_PG.acerto_id                                                                          " & vbNewLine
    sSQL = sSQL & "     AND PS_AC_PG.cod_origem           = 'SN')                                                                                       " & vbNewLine
    
    '-- MFrasca - Flow 6223748 - 19/10/2010 - Altera��o para que seja poss�vel realizar a aprova��o antes de
    '-- se gerar a remessa ao PAGNET que ficar� condicionada a aprova��o do financeiro --
    'sSQL = sSQL & "   INNER JOIN ps_remessa_voucher_tb PS_RM_VC                                                                                         " & vbNewLine
    sSQL = sSQL & "   LEFT JOIN ps_remessa_voucher_tb PS_RM_VC  with (nolock)                                                                                         " & vbNewLine
    sSQL = sSQL & "      ON (PS_AC_PG.voucher_id          = PS_RM_VC.voucher_id                                                                         " & vbNewLine
    sSQL = sSQL & "     AND PS_RM_VC.cod_origem           = 'SN')                                                                                       " & vbNewLine
    'sSQL = sSQL & "   INNER JOIN ps_remessa_tb PS_RM                                                                                                    " & vbNewLine
    sSQL = sSQL & "   LEFT JOIN ps_remessa_tb PS_RM  with (nolock)                                                                                                    " & vbNewLine
    sSQL = sSQL & "      ON (PS_RM_VC.remessa_id          = PS_RM.remessa_id                                                                            " & vbNewLine
    sSQL = sSQL & "     AND PS_RM.cod_sistema_ps          = 'AP')                                                                                       " & vbNewLine
    '-------------------------------------------------------------------------------------------------------------
    
    sSQL = sSQL & "   INNER JOIN evento_SEGBR_sinistro_tb EV  with (nolock)                                                                                            " & vbNewLine
    sSQL = sSQL & "      ON (EV.sinistro_BB               = PG_SN.sinistro_BB                                                                           " & vbNewLine
    sSQL = sSQL & "     AND EV.num_recibo                 = PG_SN.num_recibo                                                                            " & vbNewLine
    sSQL = sSQL & "     AND EV.evento_BB_id               = 1152                                                                                        " & vbNewLine
    sSQL = sSQL & "     AND ISNULL(cod_erro_remessa, 0)   = 0 )                                                                                         " & vbNewLine
    sSQL = sSQL & "   INNER JOIN interface_db..saida_gtr_tb saida_gtr_tb  with (nolock)                                                                                " & vbNewLine
    sSQL = sSQL & "      ON (saida_gtr_tb.sinistro_bb = ev.sinistro_bb                                                                                  " & vbNewLine
    sSQL = sSQL & "     AND saida_gtr_tb.evento_BB   = ev.evento_BB_id                                                                                  " & vbNewLine
    sSQL = sSQL & "     AND saida_gtr_tb.cod_remessa = ev.num_remessa                                                                                   " & vbNewLine
    sSQL = sSQL & "     AND saida_gtr_tb.situacao_MQ = 'T')                                                                                             " & vbNewLine
    
    '-- MFrasca - 24/11/2010 - Flow 6223748 - Ajuste para utilizar flag de aprova��o autom�tica do financeiro --
    sSQL = sSQL & "  INNER  JOIN   seguros_db..forma_pgto_tb AS forma_pgto_tb WITH (NOLOCK)                                                              " & vbNewLine
    sSQL = sSQL & "  ON     forma_pgto_tb.forma_pgto_id = PG_SN.forma_pgto_id                                                                           " & vbNewLine
    '-----------------------------------------------------------------------------------------------------------
    
    sSQL = sSQL & "   WHERE PG_SN.autorizado_SISBB        = 'n'                                                                                         " & vbNewLine
    sSQL = sSQL & "     AND PG_SN.retorno_recibo_BB       = 'n'                                                                                         " & vbNewLine
    sSQL = sSQL & "     AND PG_SN.item_val_estimativa     = 1                                                                                           " & vbNewLine
    sSQL = sSQL & "     AND RAMO.tp_ramo_id               = 1                                                                                           " & vbNewLine
    sSQL = sSQL & "     AND EXISTS (SELECT TOP 1 1                                                                                                            " & vbNewLine
    sSQL = sSQL & "                 FROM   INTERFACE_DB..entrada_GTR_tb e  with (nolock)                                                                               " & vbNewLine
    sSQL = sSQL & "                 WHERE  e.sinistro_BB      = PG_SN.sinistro_BB                                                                       " & vbNewLine
    sSQL = sSQL & "                 AND    evento_BB          = 2000                                                                                    " & vbNewLine
    sSQL = sSQL & "                 AND    NOT EXISTS (SELECT TOP 1 1 FROM evento_SEGBR_sinistro_tb evento_SEGBR_sinistro_tb  with (nolock)                                                            " & vbNewLine
    sSQL = sSQL & "                                    WHERE  evento_SEGBR_sinistro_tb.evento_SEGBR_id = 10130                                          " & vbNewLine
    sSQL = sSQL & "                                    AND    evento_SEGBR_sinistro_tb.sinistro_BB     = e.sinistro_BB                                  " & vbNewLine
    sSQL = sSQL & "                                    AND    evento_SEGBR_sinistro_tb.dt_evento      <= PG_SN.dt_solicitacao_pgto                      " & vbNewLine
    sSQL = sSQL & "                                    AND    evento_SEGBR_sinistro_tb.EVENTO_ID       < EV.evento_id                                   " & vbNewLine
    sSQL = sSQL & "                                    AND    NOT EXISTS (SELECT TOP 1 1 FROM evento_SEGBR_sinistro_tb ev1  with (nolock)                                     " & vbNewLine
    sSQL = sSQL & "                                                       WHERE  ev1.sinistro_BB  = e.sinistro_BB                                       " & vbNewLine
    sSQL = sSQL & "                                                       AND    ev1.evento_bb_id = 1151                                                " & vbNewLine
    sSQL = sSQL & "                                                       AND    ev1.evento_id    > evento_SEGBR_sinistro_tb.evento_id)                 " & vbNewLine
    sSQL = sSQL & "                                    AND    NOT EXISTS (SELECT TOP 1 1 FROM evento_SEGBR_sinistro_tb  evento_SEGBR_sinistro_tb  with (nolock)                                       " & vbNewLine
    sSQL = sSQL & "                                                       WHERE  evento_SEGBR_sinistro_tb.evento_SEGBR_id = 10120                       " & vbNewLine
    sSQL = sSQL & "                                                       AND    evento_SEGBR_sinistro_tb.sinistro_BB     = e.sinistro_BB               " & vbNewLine
    sSQL = sSQL & "                                                       AND    evento_SEGBR_sinistro_tb.EVENTO_ID       < EV.evento_id                " & vbNewLine
    sSQL = sSQL & "                                                       AND    evento_SEGBR_sinistro_tb.dt_evento       <= PG_SN.dt_solicitacao_pgto) " & vbNewLine
    sSQL = sSQL & "                                  )  " & vbNewLine
    sSQL = sSQL & "                )    " & vbNewLine
    sSQL = sSQL & "     AND PG_SN.dt_solicitacao_pgto         >= '20021118'     " & vbNewLine
    sSQL = sSQL & "     AND ISNULL(EV.val_prejuizo_SISBB, 0)  <= (SELECT DISTINCT ISNULL(vl_max_aprovacao,0)    " & vbNewLine
    sSQL = sSQL & "                                               FROM   seguros_db..usuario_sisbb_tb  with (nolock) " & vbNewLine
    sSQL = sSQL & "                                               WHERE  chave ='E1150881')     " & vbNewLine
    
    '-- MFrasca - 24/11/2010 - Flow 6223748 - Ajuste para utilizar flag de aprova��o autom�tica do financeiro --
    '-- Caso seja 'S' ser� aprovado automaticamente pela tarefa da agenda que roda a procedure SEGS8042_SPI   --
    sSQL = sSQL & "    AND  ISNULL(forma_pgto_tb.aprovacao_automatica_financeiro, 'N') = 'N' " & vbNewLine
    '-----------------------------------------------------------------------------------------------------------

    If sTipo = 1 Then
        sSQL = sSQL & "AND ISNULL(PS_AC_PG.voucher_id,'')   = " & lVoucher & vbNewLine
        sSQL = sSQL & "AND ISNULL(PG_SN.ramo_id,'')         = " & iRamo & vbNewLine
        sSQL = sSQL & "AND ISNULL(PG_SN.apolice_id,'')      = " & iApolice & vbNewLine
        'If bUsarConexaoABS Then
            sSQL = sSQL & "     AND PP.situacao <> 'T'                 " & vbNewLine
            sSQL = sSQL & "     AND SN.situacao <> 7                   " & vbNewLine
        'End If
    Else
    
        Select Case cmbCampo.Text
               
            Case "Sinistro BB" 'Sinistro BB
                sSQL = sSQL & " AND PG_SN.sinistro_bb = " & txtConteudo.Text & vbNewLine
                 
            Case "N� do Sinistro" 'N�mero do Sinistro
                sSQL = sSQL & " AND PG_SN.sinistro_id = " & txtConteudo.Text & vbNewLine
                 
            Case "Data da Remessa" 'Data da Remessa
                sSQL = sSQL & " AND PS_RM.dt_remessa = " & txtConteudo.Text & vbNewLine
                 
            Case "N� da Lista de Autoriza��o" 'N�mero da Lista de Autoriza��o
                sSQL = sSQL & " AND PG_SN.num_lista = " & txtConteudo.Text & vbNewLine
                 
            Case "Voucher" 'C�digo do Voucher
                sSQL = sSQL & " AND PS_AC_PG.voucher_id = " & txtConteudo.Text & vbNewLine
                 
            Case "N� da Autoriza��o" 'N�mero da Autoriza��o
                sSQL = sSQL & " AND PG_SN.chave_autorizacao_sinistro = " & txtConteudo.Text & vbNewLine
               
        End Select
        'If bUsarConexaoABS Then
            sSQL = sSQL & "     AND PP.situacao <> 'T'                      " & vbNewLine
            sSQL = sSQL & "     AND SN.situacao <> 7                        " & vbNewLine
        'End If
        sSQL = sSQL & "   GROUP BY PS_AC_PG.voucher_id                  " & vbNewLine
        sSQL = sSQL & "          , SN.agencia_id                        " & vbNewLine
        sSQL = sSQL & "          , AG.nome                              " & vbNewLine
        sSQL = sSQL & "          , PG_SN.sucursal_seguradora_id         " & vbNewLine
        sSQL = sSQL & "          , PG_SN.seguradora_cod_susep           " & vbNewLine
        sSQL = sSQL & "          , PG_SN.ramo_id                        " & vbNewLine
        sSQL = sSQL & "          , PG_SN.apolice_id                     " & vbNewLine
        sSQL = sSQL & "          , SN_VD.cpf                            " & vbNewLine
        sSQL = sSQL & "          , SN_VD.nome                           " & vbNewLine
        sSQL = sSQL & "          , SN.moeda_id                          " & vbNewLine
        sSQL = sSQL & "          , PG_SN.chave_autorizacao_sinistro     " & vbNewLine
        sSQL = sSQL & "          , PG_SN.chave_autorizacao_sinistro_dv  " & vbNewLine
        sSQL = sSQL & "          , PG_SN.num_lista                      " & vbNewLine
        sSQL = sSQL & "          , PR.produto_id                        " & vbNewLine
        sSQL = sSQL & "          , PR.nome                              " & vbNewLine

    End If
    If bUsarConexaoABS Then
        If sTipo = 1 Then 'DETALHADO
            sSQL = sSQL & "  UNION ALL                                                                      " & vbNewLine
            sSQL = sSQL & "  SELECT voucher_id                   = PS_AC_PG.voucher_id                      " & vbNewLine
            sSQL = sSQL & "       , SN.agencia_id                                                           " & vbNewLine
            sSQL = sSQL & "       , nome_agencia                 = AG.nome                                  " & vbNewLine
            sSQL = sSQL & "       , sinistro_id                  = PG_SN.sinistro_id                        " & vbNewLine
            sSQL = sSQL & "       , PG_SN.sucursal_seguradora_id                                            " & vbNewLine
            sSQL = sSQL & "       , PG_SN.seguradora_cod_susep                                              " & vbNewLine
            sSQL = sSQL & "       , PG_SN.ramo_id                                                           " & vbNewLine
            sSQL = sSQL & "       , sinistro_bb                  = PG_SN.sinistro_bb                        " & vbNewLine
            sSQL = sSQL & "       , PG_SN.apolice_id                                                        " & vbNewLine
            sSQL = sSQL & "       , SN_VD.cpf                                                               " & vbNewLine
            sSQL = sSQL & "       , nome_segurado                = SN_VD.nome                               " & vbNewLine
            sSQL = sSQL & "       , dt_pagamento                 = PG_SN.dt_acerto_contas_sinistro          " & vbNewLine
            sSQL = sSQL & "       , SN.moeda_id                                                             " & vbNewLine
            sSQL = sSQL & "       , valor_pagamento              = PG_SN.val_acerto                         " & vbNewLine
            sSQL = sSQL & "       , valor_correcao               = PG_SN.val_correcao                       " & vbNewLine
            sSQL = sSQL & "       , valor_franquia               = PG_SN.val_franquia_SISBB                 " & vbNewLine
            sSQL = sSQL & "       , valor_depreciacao            = PG_SN.val_depreciacao_SISBB              " & vbNewLine
            sSQL = sSQL & "       , valor_salvados               = PG_SN.val_salvado_SISBB                  " & vbNewLine
            sSQL = sSQL & "       , valor_prejuizo_total         = ISNULL(EV.val_prejuizo_SISBB,0)          " & vbNewLine
            sSQL = sSQL & "       , valor_total_geral            = ISNULL(EV.val_prejuizo_SISBB,0) + ISNULL(PG_SN.val_correcao,0) + ISNULL(PG_SN.val_franquia_SISBB,0)  " & vbNewLine
            sSQL = sSQL & "       , beneficiario_id              = PG_SN.beneficiario_id                    " & vbNewLine
            sSQL = sSQL & "       , nome_beneficiario            = BF.nome                                  " & vbNewLine
            sSQL = sSQL & "       , num_autorizacao              = CONVERT(varchar(10),PG_SN.chave_autorizacao_sinistro) + '-' + PG_SN.chave_autorizacao_sinistro_dv    " & vbNewLine
            sSQL = sSQL & "       , PG_SN.num_lista                                                         " & vbNewLine
            sSQL = sSQL & "       , num_remessa                  = NULL --PS_RM_VC.remessa_id               " & vbNewLine
            sSQL = sSQL & "       , dt_remessa                   = NULL --PS_RM.dt_remessa                  " & vbNewLine
            sSQL = sSQL & "       , produto_id                   = PR.produto_id                            " & vbNewLine
            sSQL = sSQL & "       , nome_produto                 = PR.nome                                  " & vbNewLine
            sSQL = sSQL & "       , num_recibo                   = PG_SN.num_recibo                         " & vbNewLine
            sSQL = sSQL & "       , destinatario_eMail           = PG_SN.usuario                            " & vbNewLine
            
            '* Alterado por Alexandre Debouch - ConfitecSP - 05/03/2010
            sSQL = sSQL & "       , 'Ambiente'                  = " & glAmbiente_id_seg2 & vbNewLine
         
        Else 'SINTETICO
            sSQL = sSQL & "  UNION ALL                                                                      " & vbNewLine
            sSQL = sSQL & "  SELECT voucher_id                   = PS_AC_PG.voucher_id                      " & vbNewLine
            sSQL = sSQL & "       , SN.agencia_id                                                           " & vbNewLine
            sSQL = sSQL & "       , nome_agencia                 = AG.nome                                  " & vbNewLine
            sSQL = sSQL & "       , sinistro_id                  = NULL --PG_SN.sinistro_id                 " & vbNewLine
            sSQL = sSQL & "       , PG_SN.sucursal_seguradora_id                                            " & vbNewLine
            sSQL = sSQL & "       , PG_SN.seguradora_cod_susep                                              " & vbNewLine
            sSQL = sSQL & "       , PG_SN.ramo_id                                                           " & vbNewLine
            sSQL = sSQL & "       , sinistro_bb                  = NULL --PG_SN.sinistro_bb                 " & vbNewLine
            sSQL = sSQL & "       , PG_SN.apolice_id                                                        " & vbNewLine
            sSQL = sSQL & "       , SN_VD.cpf                                                               " & vbNewLine
            sSQL = sSQL & "       , nome_segurado                = SN_VD.nome                               " & vbNewLine
            sSQL = sSQL & "       , dt_pagamento                 = NULL --PG_SN.dt_acerto_contas_sinistro   " & vbNewLine
            sSQL = sSQL & "       , SN.moeda_id                                                             " & vbNewLine
            sSQL = sSQL & "       , valor_pagamento              = SUM(PG_SN.val_acerto)                    " & vbNewLine
            sSQL = sSQL & "       , valor_correcao               = SUM(PG_SN.val_correcao)                  " & vbNewLine
            sSQL = sSQL & "       , valor_franquia               = SUM(PG_SN.val_franquia_SISBB)            " & vbNewLine
            sSQL = sSQL & "       , valor_depreciacao            = SUM(PG_SN.val_depreciacao_SISBB)         " & vbNewLine
            sSQL = sSQL & "       , valor_salvados               = SUM(PG_SN.val_salvado_SISBB)             " & vbNewLine
            sSQL = sSQL & "       , valor_prejuizo_total         = SUM(ISNULL(EV.val_prejuizo_SISBB,0))     " & vbNewLine
            sSQL = sSQL & "       , valor_total_geral            = SUM(ISNULL(EV.val_prejuizo_SISBB,0) + ISNULL(PG_SN.val_correcao,0) + ISNULL(PG_SN.val_franquia_SISBB,0)) " & vbNewLine
            sSQL = sSQL & "       , beneficiario_id              = NULL --PG_SN.beneficiario_id             " & vbNewLine
            sSQL = sSQL & "       , nome_beneficiario            = NULL --BF.nome                           " & vbNewLine
            sSQL = sSQL & "       , num_autorizacao              = CONVERT(varchar(10),PG_SN.chave_autorizacao_sinistro) + '-' + PG_SN.chave_autorizacao_sinistro_dv        " & vbNewLine
            sSQL = sSQL & "       , PG_SN.num_lista                                                         " & vbNewLine
            sSQL = sSQL & "       , num_remessa                  = NULL --PS_RM_VC.remessa_id               " & vbNewLine
            sSQL = sSQL & "       , dt_remessa                   = NULL --PS_RM.dt_remessa                  " & vbNewLine
            sSQL = sSQL & "       , produto_id                   = PR.produto_id                            " & vbNewLine
            sSQL = sSQL & "       , nome_produto                 = PR.nome                                  " & vbNewLine
            sSQL = sSQL & "       , num_recibo                   = NULL --PG_SN.num_recibo                  " & vbNewLine
            sSQL = sSQL & "       , destinatario_eMail           = NULL --PG_SN.usuario                     " & vbNewLine
            '* Alterado por Alexandre Debouch - ConfitecSP - 05/03/2010
            sSQL = sSQL & "       , 'Ambiente'                  = " & glAmbiente_id_seg2 & vbNewLine
        
        End If
        
        sSQL = sSQL & "    FROM abss.seguros_db.dbo.pgto_sinistro_lote_tb pgto_sinistro_lote_tb with (NOLOCK)                                                            " & vbNewLine
        
        sSQL = sSQL & "    JOIN abss.seguros_db.dbo.pgto_sinistro_tb PG_SN with (NOLOCK)                                                                                 " & vbNewLine
        sSQL = sSQL & "      ON PG_SN.sinistro_id = pgto_sinistro_lote_tb.sinistro_id                                                                       " & vbNewLine
        
        sSQL = sSQL & "   INNER JOIN abss.seguros_db.dbo.sinistro_benef_tb BF  with (nolock)                                                                                                   " & vbNewLine
        sSQL = sSQL & "      ON BF.sinistro_id                = PG_SN.sinistro_id                                                                           " & vbNewLine
        sSQL = sSQL & "     AND BF.apolice_id                 = PG_SN.apolice_id                                                                            " & vbNewLine
        sSQL = sSQL & "     AND BF.seguradora_cod_susep       = PG_SN.seguradora_cod_susep                                                                  " & vbNewLine
        sSQL = sSQL & "     AND BF.sucursal_seguradora_id     = PG_SN.sucursal_seguradora_id                                                                " & vbNewLine
        sSQL = sSQL & "     AND BF.ramo_id                    = PG_SN.ramo_id                                                                               " & vbNewLine
        sSQL = sSQL & "     AND BF.beneficiario_id            = PG_SN.beneficiario_id                                                                       " & vbNewLine
        
        sSQL = sSQL & "   INNER JOIN abss.seguros_db.dbo.sinistro_tb SN  with (nolock)                                                                                                         " & vbNewLine
        sSQL = sSQL & "      ON SN.sinistro_id                = PG_SN.sinistro_id                                                                           " & vbNewLine
        sSQL = sSQL & "     AND SN.apolice_id                 = PG_SN.apolice_id                                                                            " & vbNewLine
        sSQL = sSQL & "     AND SN.seguradora_cod_susep       = PG_SN.seguradora_cod_susep                                                                  " & vbNewLine
        sSQL = sSQL & "     AND SN.sucursal_seguradora_id     = PG_SN.sucursal_seguradora_id                                                                " & vbNewLine
        sSQL = sSQL & "     AND SN.ramo_id                    = PG_SN.ramo_id                                                                               " & vbNewLine
        
        sSQL = sSQL & "   INNER JOIN abss.seguros_db.dbo.proposta_tb PP  with (nolock)                                                                                                         " & vbNewLine
        sSQL = sSQL & "      ON PP.proposta_id                = SN.proposta_id                                                                              " & vbNewLine
        
        sSQL = sSQL & "   INNER JOIN abss.seguros_db.dbo.produto_tb PR  with (nolock)                                                                                                          " & vbNewLine
        sSQL = sSQL & "      ON PP.produto_id                 = PR.produto_id                                                                               " & vbNewLine
        sSQL = sSQL & "     AND SN.seguradora_cod_susep       = PR.seguradora_cod_susep                                                                     " & vbNewLine
        
        sSQL = sSQL & "    LEFT JOIN abss.seguros_db.dbo.agencia_tb AG  with (nolock)                                                                                                          " & vbNewLine
        sSQL = sSQL & "      ON AG.agencia_id                  = SN.agencia_id                                                                              " & vbNewLine
        sSQL = sSQL & "     AND AG.banco_id                    = 1                                                                                          " & vbNewLine
        
        sSQL = sSQL & "   INNER JOIN abss.seguros_db.dbo.sinistro_vida_tb SN_VD  with (nolock)                                                                                                 " & vbNewLine
        sSQL = sSQL & "      ON (PG_SN.sinistro_id            = SN_VD.sinistro_id                                                                           " & vbNewLine
        sSQL = sSQL & "     AND PG_SN.apolice_id              = SN_VD.apolice_id                                                                            " & vbNewLine
        sSQL = sSQL & "     AND PG_SN.seguradora_cod_susep    = SN_VD.seguradora_cod_susep                                                                  " & vbNewLine
        sSQL = sSQL & "     AND PG_SN.sucursal_seguradora_id  = SN_VD.sucursal_seguradora_id                                                                " & vbNewLine
        sSQL = sSQL & "     AND PG_SN.ramo_id                 = SN_VD.ramo_id      )                                                                        " & vbNewLine
        
        sSQL = sSQL & "   INNER JOIN abss.seguros_db.dbo.ramo_tb RAMO  with (nolock)                                                                                                           " & vbNewLine
        sSQL = sSQL & "      ON (RAMO.ramo_id                 = PG_SN.ramo_id      )                                                                        " & vbNewLine
        
        sSQL = sSQL & "   INNER JOIN abss.seguros_db.dbo.ps_acerto_pagamento_tb PS_AC_PG  with (nolock)                                                                                        " & vbNewLine
        sSQL = sSQL & "      ON (PG_SN.acerto_id              = PS_AC_PG.acerto_id                                                                          " & vbNewLine
        sSQL = sSQL & "     AND PS_AC_PG.cod_origem           = 'SN')                                                                                       " & vbNewLine
        
        '-- MFrasca - Flow 6223748 - 19/10/2010 - Altera��o para que seja poss�vel realizar a aprova��o antes de
        '-- se gerar a remessa ao PAGNET que ficar� condicionada a aprova��o do financeiro --
        'sSQL = sSQL & "   INNER JOIN abss.seguros_db.dbo.ps_remessa_voucher_tb PS_RM_VC                                                                                         " & vbNewLine
        sSQL = sSQL & "   LEFT JOIN abss.seguros_db.dbo.ps_remessa_voucher_tb PS_RM_VC  with (nolock)                                                                                         " & vbNewLine
        sSQL = sSQL & "      ON (PS_AC_PG.voucher_id          = PS_RM_VC.voucher_id                                                                         " & vbNewLine
        sSQL = sSQL & "     AND PS_RM_VC.cod_origem           = 'SN')                                                                                       " & vbNewLine
        
        'sSQL = sSQL & "   INNER JOIN abss.seguros_db.dbo.ps_remessa_tb PS_RM                                                                                                    " & vbNewLine
        sSQL = sSQL & "   LEFT JOIN abss.seguros_db.dbo.ps_remessa_tb PS_RM  with (nolock)                                                                                                    " & vbNewLine
        sSQL = sSQL & "      ON (PS_RM_VC.remessa_id          = PS_RM.remessa_id                                                                            " & vbNewLine
        sSQL = sSQL & "     AND PS_RM.cod_sistema_ps          = 'AP')                                                                                       " & vbNewLine
        '-------------------------------------------------------------------------------------------------------------
        
        sSQL = sSQL & "   INNER JOIN abss.seguros_db.dbo.evento_SEGBR_sinistro_tb EV  with (nolock)                                                                                            " & vbNewLine
        sSQL = sSQL & "      ON (EV.sinistro_BB               = PG_SN.sinistro_BB                                                                           " & vbNewLine
        sSQL = sSQL & "     AND EV.num_recibo                 = PG_SN.num_recibo                                                                            " & vbNewLine
        sSQL = sSQL & "     AND EV.evento_BB_id               = 1152                                                                                        " & vbNewLine
        sSQL = sSQL & "     AND ISNULL(cod_erro_remessa, 0)   = 0 )                                                                                         " & vbNewLine
        
        sSQL = sSQL & "   INNER JOIN abss.interface_db.dbo.saida_gtr_tb saida_gtr_tb  with (nolock)                                                                                " & vbNewLine
        sSQL = sSQL & "      ON (saida_gtr_tb.sinistro_bb = ev.sinistro_bb                                                                                  " & vbNewLine
        sSQL = sSQL & "     AND saida_gtr_tb.evento_BB   = ev.evento_BB_id                                                                                  " & vbNewLine
        sSQL = sSQL & "     AND saida_gtr_tb.cod_remessa = ev.num_remessa                                                                                   " & vbNewLine
        sSQL = sSQL & "     AND saida_gtr_tb.situacao_MQ = 'T')                                                                                             " & vbNewLine
        
        '-- MFrasca - 24/11/2010 - Flow 6223748 - Ajuste para utilizar flag de aprova��o autom�tica do financeiro --
        sSQL = sSQL & "  INNER  JOIN   abss.seguros_db.dbo.forma_pgto_tb AS forma_pgto_tb WITH (NOLOCK)                                                              " & vbNewLine
        sSQL = sSQL & "  ON     forma_pgto_tb.forma_pgto_id = PG_SN.forma_pgto_id                                                                           " & vbNewLine
        '-----------------------------------------------------------------------------------------------------------
        
        sSQL = sSQL & "   WHERE PG_SN.autorizado_SISBB        = 'n'                                                                                         " & vbNewLine
        sSQL = sSQL & "     AND PG_SN.retorno_recibo_BB       = 'n'                                                                                         " & vbNewLine
        sSQL = sSQL & "     AND PG_SN.item_val_estimativa     = 1                                                                                           " & vbNewLine
        sSQL = sSQL & "     AND RAMO.tp_ramo_id               = 1                                                                                           " & vbNewLine
        sSQL = sSQL & "     AND EXISTS (SELECT TOP 1 1                                                                                                            " & vbNewLine
        sSQL = sSQL & "                 FROM   abss.INTERFACE_DB.dbo.entrada_GTR_tb e  with (nolock)                                                                               " & vbNewLine
        sSQL = sSQL & "                 WHERE  e.sinistro_BB      = PG_SN.sinistro_BB                                                                       " & vbNewLine
        sSQL = sSQL & "                 AND    evento_BB          = 2000                                                                                    " & vbNewLine
        sSQL = sSQL & "                 AND    NOT EXISTS (SELECT TOP 1 1 FROM abss.seguros_db.dbo.evento_SEGBR_sinistro_tb evento_SEGBR_sinistro_tb  with (nolock)                      " & vbNewLine
        sSQL = sSQL & "                                    WHERE  evento_SEGBR_sinistro_tb.evento_SEGBR_id = 10130                                          " & vbNewLine
        sSQL = sSQL & "                                    AND    evento_SEGBR_sinistro_tb.sinistro_BB     = e.sinistro_BB                                  " & vbNewLine
        sSQL = sSQL & "                                    AND    evento_SEGBR_sinistro_tb.dt_evento      <= PG_SN.dt_solicitacao_pgto                      " & vbNewLine
        sSQL = sSQL & "                                    AND    evento_SEGBR_sinistro_tb.EVENTO_ID       < EV.evento_id                                   " & vbNewLine
        sSQL = sSQL & "                                    AND    NOT EXISTS (SELECT TOP 1 1 FROM abss.seguros_db.dbo.evento_SEGBR_sinistro_tb ev1  with (nolock)                                    " & vbNewLine
        sSQL = sSQL & "                                                       WHERE  ev1.sinistro_BB  = e.sinistro_BB                                       " & vbNewLine
        sSQL = sSQL & "                                                       AND    ev1.evento_bb_id = 1151                                                " & vbNewLine
        sSQL = sSQL & "                                                       AND    ev1.evento_id    > evento_SEGBR_sinistro_tb.evento_id)                 " & vbNewLine
        sSQL = sSQL & "                                    AND    NOT EXISTS (SELECT TOP 1 1 FROM abss.seguros_db.dbo.evento_SEGBR_sinistro_tb evento_SEGBR_sinistro_tb  with (nolock)                                       " & vbNewLine
        sSQL = sSQL & "                                                       WHERE  evento_SEGBR_sinistro_tb.evento_SEGBR_id = 10120                       " & vbNewLine
        sSQL = sSQL & "                                                       AND    evento_SEGBR_sinistro_tb.sinistro_BB     = e.sinistro_BB               " & vbNewLine
        sSQL = sSQL & "                                                       AND    evento_SEGBR_sinistro_tb.EVENTO_ID       < EV.evento_id                " & vbNewLine
        sSQL = sSQL & "                                                       AND    evento_SEGBR_sinistro_tb.dt_evento       <= PG_SN.dt_solicitacao_pgto) " & vbNewLine
        sSQL = sSQL & "                                  )  " & vbNewLine
        sSQL = sSQL & "                )    " & vbNewLine
        sSQL = sSQL & "     AND PG_SN.dt_solicitacao_pgto         >= '20021118'     " & vbNewLine
        sSQL = sSQL & "     AND ISNULL(EV.val_prejuizo_SISBB, 0)  <= (SELECT DISTINCT ISNULL(vl_max_aprovacao,0)    " & vbNewLine
        sSQL = sSQL & "                                               FROM   abss.seguros_db.dbo.usuario_sisbb_tb  with (nolock)   " & vbNewLine
        sSQL = sSQL & "                                               WHERE  chave ='E1150881')     " & vbNewLine
        
        '-- MFrasca - 24/11/2010 - Flow 6223748 - Ajuste para utilizar flag de aprova��o autom�tica do financeiro --
        '-- Caso seja 'S' ser� aprovado automaticamente pela tarefa da agenda que roda a procedure SEGS8042_SPI   --
        sSQL = sSQL & "    AND  ISNULL(forma_pgto_tb.aprovacao_automatica_financeiro, 'N') = 'N' " & vbNewLine
        '-----------------------------------------------------------------------------------------------------------
        
        If sTipo = 1 Then
            sSQL = sSQL & "AND ISNULL(PS_AC_PG.voucher_id,'')   = " & lVoucher & vbNewLine
            sSQL = sSQL & "AND ISNULL(PG_SN.ramo_id,'')         = " & iRamo & vbNewLine
            sSQL = sSQL & "AND ISNULL(PG_SN.apolice_id,'')      = " & iApolice & vbNewLine
            sSQL = sSQL & "     AND PP.situacao <> 'T'                 " & vbNewLine
            sSQL = sSQL & "     AND SN.situacao <> 7                   " & vbNewLine
        Else
        
            Select Case cmbCampo.Text
                   
                Case "Sinistro BB" 'Sinistro BB
                    sSQL = sSQL & " AND PG_SN.sinistro_bb = " & txtConteudo.Text & vbNewLine
                     
                Case "N� do Sinistro" 'N�mero do Sinistro
                    sSQL = sSQL & " AND PG_SN.sinistro_id = " & txtConteudo.Text & vbNewLine
                     
                Case "Data da Remessa" 'Data da Remessa
                    sSQL = sSQL & " AND PS_RM.dt_remessa = " & txtConteudo.Text & vbNewLine
                     
                Case "N� da Lista de Autoriza��o" 'N�mero da Lista de Autoriza��o
                    sSQL = sSQL & " AND PG_SN.num_lista = " & txtConteudo.Text & vbNewLine
                     
                Case "Voucher" 'C�digo do Voucher
                    sSQL = sSQL & " AND PS_AC_PG.voucher_id = " & txtConteudo.Text & vbNewLine
                     
                Case "N� da Autoriza��o" 'N�mero da Autoriza��o
                    sSQL = sSQL & " AND PG_SN.chave_autorizacao_sinistro = " & txtConteudo.Text & vbNewLine
                   
            End Select
            sSQL = sSQL & "     AND PP.situacao <> 'T'                      " & vbNewLine
            sSQL = sSQL & "     AND SN.situacao <> 7                        " & vbNewLine
            sSQL = sSQL & "   GROUP BY PS_AC_PG.voucher_id                  " & vbNewLine
            sSQL = sSQL & "          , SN.agencia_id                        " & vbNewLine
            sSQL = sSQL & "          , AG.nome                              " & vbNewLine
            sSQL = sSQL & "          , PG_SN.sucursal_seguradora_id         " & vbNewLine
            sSQL = sSQL & "          , PG_SN.seguradora_cod_susep           " & vbNewLine
            sSQL = sSQL & "          , PG_SN.ramo_id                        " & vbNewLine
            sSQL = sSQL & "          , PG_SN.apolice_id                     " & vbNewLine
            sSQL = sSQL & "          , SN_VD.cpf                            " & vbNewLine
            sSQL = sSQL & "          , SN_VD.nome                           " & vbNewLine
            sSQL = sSQL & "          , SN.moeda_id                          " & vbNewLine
            sSQL = sSQL & "          , PG_SN.chave_autorizacao_sinistro     " & vbNewLine
            sSQL = sSQL & "          , PG_SN.chave_autorizacao_sinistro_dv  " & vbNewLine
            sSQL = sSQL & "          , PG_SN.num_lista                      " & vbNewLine
            sSQL = sSQL & "          , PR.produto_id                        " & vbNewLine
            sSQL = sSQL & "          , PR.nome                              " & vbNewLine
        
        End If
    End If
    MontaQueryCDC = sSQL
    
    Exit Function
        
TrataErro:
    mensagem_erro 6, "Erro ao carregar dados do CDC na tabela tempor�ria !"
    Call TerminaSEGBR

End Function

