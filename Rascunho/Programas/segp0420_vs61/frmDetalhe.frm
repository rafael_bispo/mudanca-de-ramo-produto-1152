VERSION 5.00
Begin VB.Form frmDetalhe 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Detalhe da Aprova��o do Sinistro"
   ClientHeight    =   7080
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   8340
   Icon            =   "frmDetalhe.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7080
   ScaleWidth      =   8340
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton btnVoltar 
      Caption         =   "Volta&r"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   7065
      TabIndex        =   5
      Top             =   6660
      Width           =   1140
   End
   Begin VB.Frame Frame1 
      Caption         =   " Detalhe da Aprova��o do Sinistro "
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   6450
      Left            =   135
      TabIndex        =   0
      Top             =   90
      Width           =   8070
      Begin VB.Label lblDetalhe 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "9.999.999.999,99"
         ForeColor       =   &H80000008&
         Height          =   240
         Index           =   18
         Left            =   1845
         TabIndex        =   39
         Top             =   5760
         Width           =   1860
      End
      Begin VB.Label lblTitulo 
         BackStyle       =   0  'Transparent
         Caption         =   "Valor Tot. Preju�zo:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   18
         Left            =   180
         TabIndex        =   38
         Top             =   5760
         Width           =   1680
      End
      Begin VB.Label lblDetalhe 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "9.999.999.999,99"
         ForeColor       =   &H80000008&
         Height          =   240
         Index           =   17
         Left            =   1845
         TabIndex        =   37
         Top             =   5445
         Width           =   1860
      End
      Begin VB.Label lblTitulo 
         BackStyle       =   0  'Transparent
         Caption         =   "Valor Salvado......:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   17
         Left            =   180
         TabIndex        =   36
         Top             =   5445
         Width           =   1680
      End
      Begin VB.Label lblDetalhe 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "9.999.999.999,99"
         ForeColor       =   &H80000008&
         Height          =   240
         Index           =   16
         Left            =   1845
         TabIndex        =   35
         Top             =   5130
         Width           =   1860
      End
      Begin VB.Label lblTitulo 
         BackStyle       =   0  'Transparent
         Caption         =   "Valor Deprecia��o:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   16
         Left            =   180
         TabIndex        =   34
         Top             =   5130
         Width           =   1680
      End
      Begin VB.Label lblDetalhe 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "9.999.999.999,99"
         ForeColor       =   &H80000008&
         Height          =   240
         Index           =   15
         Left            =   1845
         TabIndex        =   33
         Top             =   4815
         Width           =   1860
      End
      Begin VB.Label lblTitulo 
         BackStyle       =   0  'Transparent
         Caption         =   "Valor Franquia.....:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   15
         Left            =   180
         TabIndex        =   32
         Top             =   4815
         Width           =   1680
      End
      Begin VB.Label lblTitulo 
         BackStyle       =   0  'Transparent
         Caption         =   "Data Remessa......:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   14
         Left            =   180
         TabIndex        =   31
         Top             =   1665
         Width           =   1680
      End
      Begin VB.Label lblDetalhe 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "99/99/9999"
         ForeColor       =   &H80000008&
         Height          =   240
         Index           =   14
         Left            =   1845
         TabIndex        =   30
         Top             =   1665
         Width           =   1860
      End
      Begin VB.Label lblTitulo 
         BackStyle       =   0  'Transparent
         Caption         =   "N� Remessa.........:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   13
         Left            =   180
         TabIndex        =   29
         Top             =   1350
         Width           =   1680
      End
      Begin VB.Label lblDetalhe 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "9999999999999999999"
         ForeColor       =   &H80000008&
         Height          =   240
         Index           =   13
         Left            =   1845
         TabIndex        =   28
         Top             =   1350
         Width           =   1860
      End
      Begin VB.Label lblTitulo 
         BackStyle       =   0  'Transparent
         Caption         =   "N� Lista...............:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   12
         Left            =   180
         TabIndex        =   27
         Top             =   1035
         Width           =   1680
      End
      Begin VB.Label lblDetalhe 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "9999999999999999999"
         ForeColor       =   &H80000008&
         Height          =   240
         Index           =   12
         Left            =   1845
         TabIndex        =   26
         Top             =   1035
         Width           =   1860
      End
      Begin VB.Label lblTitulo 
         BackStyle       =   0  'Transparent
         Caption         =   "N� Autoriza��o.....:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   11
         Left            =   180
         TabIndex        =   25
         Top             =   720
         Width           =   1680
      End
      Begin VB.Label lblDetalhe 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "9999999999999999999"
         ForeColor       =   &H80000008&
         Height          =   240
         Index           =   11
         Left            =   1845
         TabIndex        =   24
         Top             =   720
         Width           =   1860
      End
      Begin VB.Label lblTitulo 
         BackStyle       =   0  'Transparent
         Caption         =   "Nome Benefici�rio:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   10
         Left            =   180
         TabIndex        =   23
         Top             =   6075
         Width           =   1680
      End
      Begin VB.Label lblDetalhe 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
         ForeColor       =   &H80000008&
         Height          =   240
         Index           =   10
         Left            =   1845
         TabIndex        =   22
         Top             =   6075
         Width           =   6045
      End
      Begin VB.Label lblTitulo 
         BackStyle       =   0  'Transparent
         Caption         =   "Valor Corre��o.....:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   9
         Left            =   180
         TabIndex        =   21
         Top             =   4500
         Width           =   1680
      End
      Begin VB.Label lblDetalhe 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "9.999.999.999,99"
         ForeColor       =   &H80000008&
         Height          =   240
         Index           =   9
         Left            =   1845
         TabIndex        =   20
         Top             =   4500
         Width           =   1860
      End
      Begin VB.Label lblTitulo 
         BackStyle       =   0  'Transparent
         Caption         =   "Valor Pagamento..:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   8
         Left            =   180
         TabIndex        =   19
         Top             =   4185
         Width           =   1680
      End
      Begin VB.Label lblDetalhe 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "9.999.999.999,99"
         ForeColor       =   &H80000008&
         Height          =   240
         Index           =   8
         Left            =   1845
         TabIndex        =   18
         Top             =   4185
         Width           =   1860
      End
      Begin VB.Label lblTitulo 
         BackStyle       =   0  'Transparent
         Caption         =   "Moeda.................:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   7
         Left            =   180
         TabIndex        =   17
         Top             =   3870
         Width           =   1680
      End
      Begin VB.Label lblDetalhe 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "XXXX"
         ForeColor       =   &H80000008&
         Height          =   240
         Index           =   7
         Left            =   1845
         TabIndex        =   16
         Top             =   3870
         Width           =   1860
      End
      Begin VB.Label lblTitulo 
         BackStyle       =   0  'Transparent
         Caption         =   "Data Pagamento...:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   6
         Left            =   180
         TabIndex        =   15
         Top             =   3555
         Width           =   1680
      End
      Begin VB.Label lblDetalhe 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "99/99/9999"
         ForeColor       =   &H80000008&
         Height          =   240
         Index           =   6
         Left            =   1845
         TabIndex        =   14
         Top             =   3555
         Width           =   1860
      End
      Begin VB.Label lblTitulo 
         BackStyle       =   0  'Transparent
         Caption         =   "Nome Segurado....:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   5
         Left            =   135
         TabIndex        =   13
         Top             =   3240
         Width           =   1725
      End
      Begin VB.Label lblDetalhe 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
         ForeColor       =   &H80000008&
         Height          =   240
         Index           =   5
         Left            =   1845
         TabIndex        =   12
         Top             =   3240
         Width           =   6045
      End
      Begin VB.Label lblTitulo 
         BackStyle       =   0  'Transparent
         Caption         =   "Ap�lice................:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   4
         Left            =   180
         TabIndex        =   11
         Top             =   2925
         Width           =   1680
      End
      Begin VB.Label lblDetalhe 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "999999999"
         ForeColor       =   &H80000008&
         Height          =   240
         Index           =   4
         Left            =   1845
         TabIndex        =   10
         Top             =   2925
         Width           =   1860
      End
      Begin VB.Label lblTitulo 
         BackStyle       =   0  'Transparent
         Caption         =   "N� Sinistro BB......:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   3
         Left            =   180
         TabIndex        =   9
         Top             =   2610
         Width           =   1680
      End
      Begin VB.Label lblDetalhe 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "9999999999999"
         ForeColor       =   &H80000008&
         Height          =   240
         Index           =   3
         Left            =   1845
         TabIndex        =   8
         Top             =   2610
         Width           =   1860
      End
      Begin VB.Label lblTitulo 
         BackStyle       =   0  'Transparent
         Caption         =   "N� Sinistro...........:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   2
         Left            =   180
         TabIndex        =   7
         Top             =   2295
         Width           =   1680
      End
      Begin VB.Label lblDetalhe 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "99999999999"
         ForeColor       =   &H80000008&
         Height          =   240
         Index           =   2
         Left            =   1845
         TabIndex        =   6
         Top             =   2295
         Width           =   1860
      End
      Begin VB.Label lblDetalhe 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "99999 - XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
         ForeColor       =   &H80000008&
         Height          =   240
         Index           =   1
         Left            =   1845
         TabIndex        =   4
         Top             =   1980
         Width           =   6045
      End
      Begin VB.Label lblTitulo 
         BackStyle       =   0  'Transparent
         Caption         =   "Ag�ncia...............:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   1
         Left            =   180
         TabIndex        =   3
         Top             =   1980
         Width           =   1680
      End
      Begin VB.Label lblDetalhe 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "9999999999999"
         ForeColor       =   &H80000008&
         Height          =   240
         Index           =   0
         Left            =   1845
         TabIndex        =   2
         Top             =   405
         Width           =   1860
      End
      Begin VB.Label lblTitulo 
         BackStyle       =   0  'Transparent
         Caption         =   "N� Voucher..........:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   0
         Left            =   180
         TabIndex        =   1
         Top             =   405
         Width           =   1680
      End
   End
End
Attribute VB_Name = "frmDetalhe"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub btnVoltar_Click()
    Unload Me
End Sub

Private Sub Form_Load()
    Call CentraFrm(Me)
    Me.Caption = "SEGP0420 - Detalhe da Aprova��o do Sinistro " & Ambiente

End Sub
