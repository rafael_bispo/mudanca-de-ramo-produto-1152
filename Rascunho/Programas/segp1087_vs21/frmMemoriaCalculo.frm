VERSION 5.00
Begin VB.Form frmMemoriaCalculo 
   Caption         =   "Mem�ria de C�lculo da Subven��o"
   ClientHeight    =   5505
   ClientLeft      =   120
   ClientTop       =   450
   ClientWidth     =   6285
   LinkTopic       =   "Form1"
   ScaleHeight     =   5505
   ScaleWidth      =   6285
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton btnVoltar 
      Caption         =   "Sair"
      Height          =   495
      Left            =   4965
      TabIndex        =   7
      Top             =   4860
      Width           =   1215
   End
   Begin VB.Frame Frame2 
      Caption         =   "Subven��o Estadual"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1830
      Left            =   240
      TabIndex        =   1
      Top             =   3555
      Width           =   4665
      Begin VB.Label Label11 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "5986356987"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000001&
         Height          =   270
         Left            =   2550
         TabIndex        =   18
         Top             =   1455
         Width           =   1995
      End
      Begin VB.Label Label10 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "5986356987"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000001&
         Height          =   270
         Left            =   2535
         TabIndex        =   17
         Top             =   1095
         Width           =   1995
      End
      Begin VB.Label Label9 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "5986356987"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000001&
         Height          =   270
         Left            =   2550
         TabIndex        =   16
         Top             =   735
         Width           =   1995
      End
      Begin VB.Label Label8 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "5986356987"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000001&
         Height          =   270
         Left            =   2550
         TabIndex        =   15
         Top             =   375
         Width           =   1995
      End
      Begin VB.Label lbl 
         AutoSize        =   -1  'True
         Caption         =   "Valor Subv. Estimado:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   9
         Left            =   90
         TabIndex        =   10
         Top             =   1095
         Width           =   1980
      End
      Begin VB.Label lbl 
         AutoSize        =   -1  'True
         Caption         =   "% Subv. Cultura:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   8
         Left            =   90
         TabIndex        =   9
         Top             =   735
         Width           =   1440
      End
      Begin VB.Label lbl 
         AutoSize        =   -1  'True
         Caption         =   "UF Risco:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   7
         Left            =   90
         TabIndex        =   8
         Top             =   375
         Width           =   885
      End
      Begin VB.Label lbl 
         AutoSize        =   -1  'True
         Caption         =   "Valor Subv. Informado:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   10
         Left            =   90
         TabIndex        =   6
         Top             =   1455
         Width           =   2025
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Subven��o Federal"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1980
      Left            =   225
      TabIndex        =   0
      Top             =   1335
      Width           =   4680
      Begin VB.Label Label7 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "5986356987"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000001&
         Height          =   270
         Left            =   2580
         TabIndex        =   14
         Top             =   1455
         Width           =   1995
      End
      Begin VB.Label Label6 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "5986356987"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000001&
         Height          =   270
         Left            =   2580
         TabIndex        =   13
         Top             =   1095
         Width           =   1995
      End
      Begin VB.Label Label5 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "5986356987"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000001&
         Height          =   270
         Left            =   2565
         TabIndex        =   12
         Top             =   735
         Width           =   1995
      End
      Begin VB.Label Label3 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "5986356987"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000001&
         Height          =   270
         Left            =   2565
         TabIndex        =   11
         Top             =   375
         Width           =   1995
      End
      Begin VB.Label lbl 
         AutoSize        =   -1  'True
         Caption         =   "Valor Subv. Informado:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   6
         Left            =   315
         TabIndex        =   5
         Top             =   1455
         Width           =   2025
      End
      Begin VB.Label lbl 
         AutoSize        =   -1  'True
         Caption         =   "Valor Subv. Estimado:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   5
         Left            =   315
         TabIndex        =   4
         Top             =   1095
         Width           =   1980
      End
      Begin VB.Label lbl 
         AutoSize        =   -1  'True
         Caption         =   "@Variavel_1"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   4
         Left            =   330
         TabIndex        =   3
         Top             =   750
         Width           =   1170
      End
      Begin VB.Label lbl 
         AutoSize        =   -1  'True
         Caption         =   "% Subv. Cultura:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   2
         Left            =   315
         TabIndex        =   2
         Top             =   360
         Width           =   1440
      End
   End
   Begin VB.Label lbl 
      AutoSize        =   -1  'True
      Caption         =   "Proposta:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Index           =   0
      Left            =   480
      TabIndex        =   24
      Top             =   120
      Width           =   870
   End
   Begin VB.Label lbl 
      AutoSize        =   -1  'True
      Caption         =   "Cultura:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Index           =   1
      Left            =   480
      TabIndex        =   23
      Top             =   480
      Width           =   660
   End
   Begin VB.Label lbl 
      AutoSize        =   -1  'True
      Caption         =   "Pr�mio:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Index           =   3
      Left            =   480
      TabIndex        =   22
      Top             =   840
      Width           =   690
   End
   Begin VB.Label Label1 
      Alignment       =   1  'Right Justify
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "5986356987"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000001&
      Height          =   270
      Left            =   2745
      TabIndex        =   21
      Top             =   120
      Width           =   1995
   End
   Begin VB.Label Label2 
      Alignment       =   1  'Right Justify
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "5986356987"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000001&
      Height          =   270
      Left            =   2745
      TabIndex        =   20
      Top             =   480
      Width           =   1995
   End
   Begin VB.Label Label4 
      Alignment       =   1  'Right Justify
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "5986356987"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000001&
      Height          =   270
      Left            =   2745
      TabIndex        =   19
      Top             =   840
      Width           =   1995
   End
End
Attribute VB_Name = "frmMemoriaCalculo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub btnVoltar_Click()
   Unload Me
End Sub

Private Sub CarregaDados_MemoriaDeCalculo(pPropostaId As Long, sUF As String)
'Private Sub CarregaDados_MemoriaDeCalculo(pPropostaId As Long, iTipoSubvencaoId As Integer, sUF As String)

' Laurent Silva - 04/09/2015
' 18657614 - Corre��o Agendamentos SEGP1087
' Carregar a tela de mem�ria de c�lculo

    Dim oConsulta As Object
    Dim rsSub As Recordset
                        
    On Error GoTo Trata_Erro
                    
    Set oConsulta = CreateObject("SEGL0284.cls00459")
    
    Call oConsulta.SetAmbiente(gsSIGLASISTEMA, _
                               glAmbiente_id, _
                               App.Title, _
                               App.FileDescription)
                               
    Set rsSub = oConsulta.ConsultarMemoriaDeCalculo(gsSIGLASISTEMA, _
                                                      App.Title, _
                                                      App.FileDescription, _
                                                      glAmbiente_id, _
                                                      cUserName, _
                                                      Trim(frmSubvencaoAlteracao.lblProposta), _
                                                      sUF)
            
    
    If Not rsSub.EOF Then
        
        '' Se encontrar registros, tento alimentar o grid...
    While rsSub.EOF = False
    
        Me.Label1.Caption = Trim(rsSub("PROPOSTA_ID").Value)
        Me.Label2.Caption = IIf(IsNull(rsSub("CULTURA").Value), 0, Trim(rsSub("CULTURA").Value))
        Me.Label4.Caption = IIf(IsNull(rsSub("VAL_PREMIO_TARIFA").Value), 0, "R$ " & Format(Trim(rsSub("VAL_PREMIO_TARIFA").Value), "###,###.00"))
        
        '' alimento a parte de federal, se tiver.
        If UCase(Trim(rsSub("SUBVENCAO").Value)) = "FEDERAL" Then
        
            'Me.Label1.Caption = Trim(rsSub("PROPOSTA_ID").Value)
            'Me.Label2.Caption = Trim(rsSub("CULTURA").Value)
            Me.Label3.Caption = Trim(rsSub("PC_SBS_SGRO_AGRL").Value) * 100 & " %"
            'Me.Label4.Caption = "R$ " & Format(Trim(rsSub("VAL_PREMIO_TARIFA").Value), "###,###.00")
            lbl(4).Caption = Trim(rsSub("NOME").Value)
            Me.Label5.Caption = Trim(rsSub("PC_SUBVENCAO_FEDERAL").Value) * 100 & " %"
            Me.Label6.Caption = "R$ " & Format(Trim(rsSub("VAL_SUBVENCAO_ESTIMADO").Value), "###,###.00")
            Me.Label7.Caption = "R$ " & Format(Trim(rsSub("SUBV_CALCULADO").Value), "###,###.00")
        
        End If
        
        '' alimento a parte de estadual, se tiver..
        If UCase(Trim(rsSub("SUBVENCAO").Value)) = "ESTADUAL" Then
        
            Me.Label8.Caption = Trim(rsSub("CP_1_ESTADUAL").Value)
            Me.Label9.Caption = Trim(rsSub("CP_2_ESTADUAL").Value) * 100 & " %"
            Me.Label10.Caption = "R$ " & Format(Trim(rsSub("CP_3_ESTADUAL").Value), "###,###.00")
            Me.Label11.Caption = "R$ " & Format(Trim(rsSub("CP_4_ESTADUAL").Value), "###,###.00")
        
        End If
        
        rsSub.MoveNext
     Wend
    End If
           
Exit Sub
           
Trata_Erro:

    Call TratarErro("Pesquisar", Me.name)
    Call FinalizarAplicacao
           
End Sub

Private Sub Limpa_MemoriaDeCalculo()

            Me.Label1.Caption = ""
            Me.Label2.Caption = ""
            Me.Label3.Caption = ""
            Me.Label4.Caption = ""
            lbl(4).Caption = ""
            Me.Label5.Caption = ""
            Me.Label6.Caption = ""
            Me.Label7.Caption = ""
            Me.Label8.Caption = ""
            Me.Label9.Caption = ""
            Me.Label10.Caption = ""
            Me.Label11.Caption = ""
            
End Sub

Private Sub Form_Load()
    '' Testes
          
    Call Limpa_MemoriaDeCalculo
       
    Call CarregaDados_MemoriaDeCalculo(Trim(frmSubvencaoAlteracao.lblProposta), frmSubvencao.cboUF.Text)
End Sub

