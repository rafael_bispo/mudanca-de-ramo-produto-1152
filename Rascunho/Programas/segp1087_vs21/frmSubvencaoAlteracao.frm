VERSION 5.00
Object = "{1CB70356-FEA2-11D4-87FA-00805F396245}#1.0#0"; "mask2s.ocx"
Begin VB.Form frmSubvencaoAlteracao 
   Caption         =   "Subven��o - Altera��o"
   ClientHeight    =   1800
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   7485
   ControlBox      =   0   'False
   Icon            =   "frmSubvencaoAlteracao.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   1800
   ScaleWidth      =   7485
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton BtnMemoriaCalc 
      Caption         =   "Exibir Mem�ria de C�lculo"
      Height          =   405
      Left            =   450
      TabIndex        =   12
      Top             =   1200
      Width           =   2385
   End
   Begin Mask2S.ConMask2S mskValSubvencao 
      Height          =   300
      Left            =   5730
      TabIndex        =   5
      Top             =   660
      Width           =   1575
      _ExtentX        =   2778
      _ExtentY        =   529
      mask            =   ""
      text            =   "0,00"
      locked          =   0   'False
      enabled         =   -1  'True
   End
   Begin VB.CommandButton BtnCancelar 
      Caption         =   "Cancelar"
      Height          =   405
      Left            =   5880
      TabIndex        =   1
      Top             =   1200
      Width           =   1455
   End
   Begin VB.CommandButton BtnOk 
      Caption         =   "Alterar"
      Height          =   405
      Left            =   4320
      TabIndex        =   0
      Top             =   1200
      Width           =   1455
   End
   Begin VB.TextBox txtTipoSubvencao 
      Height          =   225
      Left            =   4620
      TabIndex        =   10
      Top             =   1290
      Visible         =   0   'False
      Width           =   225
   End
   Begin VB.TextBox txtUF 
      Height          =   225
      Left            =   4860
      TabIndex        =   11
      Top             =   1290
      Visible         =   0   'False
      Width           =   225
   End
   Begin VB.Label lblValestimado 
      BorderStyle     =   1  'Fixed Single
      Height          =   285
      Left            =   1620
      TabIndex        =   9
      Top             =   780
      Width           =   1335
   End
   Begin VB.Label Label4 
      AutoSize        =   -1  'True
      Caption         =   "Valor Estimado:"
      Height          =   225
      Left            =   450
      TabIndex        =   8
      Top             =   810
      Width           =   1095
   End
   Begin VB.Label lblSegurado 
      BorderStyle     =   1  'Fixed Single
      Height          =   285
      Left            =   3990
      TabIndex        =   7
      Top             =   195
      Width           =   3315
   End
   Begin VB.Label lblProposta 
      BorderStyle     =   1  'Fixed Single
      Height          =   285
      Left            =   1620
      TabIndex        =   6
      Top             =   210
      Width           =   1335
   End
   Begin VB.Label Label3 
      AutoSize        =   -1  'True
      Caption         =   "Valor da Subven��o:"
      Height          =   195
      Left            =   4170
      TabIndex        =   4
      Top             =   720
      Width           =   1500
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      Caption         =   "Segurado:"
      Height          =   195
      Left            =   3120
      TabIndex        =   3
      Top             =   240
      Width           =   735
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      Caption         =   "Proposta:"
      Height          =   195
      Left            =   900
      TabIndex        =   2
      Top             =   300
      Width           =   675
   End
End
Attribute VB_Name = "frmSubvencaoAlteracao"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub BtnCancelar_Click()

    Unload Me

End Sub

Private Sub BtnMemoriaCalc_Click()
    '18657614 - Corre��o Agendamentos SEGP1087 - Antonio Silva [01/09/2015::Confitec]
    'Chama o formul�rio frmMemoriaCalculo
    frmMemoriaCalculo.Show vbModal
End Sub

Private Sub BtnOk_Click()

    Dim oAtualiza As Object
    Dim rsAtualiza As Recordset
    
    'FLOW 3012625 - Marcelo Ferreira - Confitec Sistemas - 20100130
    'Comparando valores em vari�veis e n�o com objetos.
    '--------------------------------------------------------------
    Dim d_val_Estimado As Double
    Dim d_val_Alteracao As Double
    
    On Error GoTo Trata_Erro

    Set oAtualiza = CreateObject("SEGL0284.cls00459")
    
    'lrocha 08/07/2008
'    If mskValSubvencao.Text > (lblPremioLiquido.Caption * (50 / 100)) Then
'        MsgBox "Valor da subven��o acima de 50% do valor do pr�mio l�quido !", vbInformation, "SEGP1087"
'        Exit Sub
'    End If

    'FLOW 3012625 - Marcelo Ferreira - Confitec Sistemas - 20100130
    '----------------------------------------------------------------------------------------
    '    If Val(mskValSubvencao.Text) > Val(lblValestimado.Caption) Then
    '        MsgBox "Valor da subven��o acima do valor estimado !", vbInformation, "SEGP1087"
    '        Exit Sub
    '    End If
    
    d_val_Estimado = lblValestimado.Caption
    d_val_Alteracao = mskValSubvencao.Text
    
    If d_val_Alteracao > d_val_Estimado Then
        MsgBox "Valor da subven��o acima do valor estimado !", vbInformation, "SEGP1087"
        Exit Sub
    End If
    
    
    Call oAtualiza.SetAmbiente(gsSIGLASISTEMA, _
                               glAmbiente_id, _
                               App.Title, _
                               App.FileDescription)


    'FLOW 3012625 - Marcelo Ferreira - Confitec Sistemas - 20100130
    '----------------------------------------------------------------------------------------
    '    Call oAtualiza.AtualizarValorSubvencao(gsSIGLASISTEMA, _
    '                                           App.Title, _
    '                                           App.FileDescription, _
    '                                           glAmbiente_id, _
    '                                           cUserName, _
    '                                           lblProposta.Caption, _
    '                                           mskValSubvencao.Text, _
    '                                           txtTipoSubvencao.Text, _
    '                                           txtUF.Text)
    Call oAtualiza.AtualizarValorSubvencao(gsSIGLASISTEMA, _
                                           App.Title, _
                                           App.FileDescription, _
                                           glAmbiente_id, _
                                           cUserName, _
                                           lblProposta.Caption, _
                                           d_val_Alteracao, _
                                           txtTipoSubvencao.Text, _
                                           txtUF.Text)


    Set rsAtualiza = Nothing
    
    Call frmSubvencao.Pesquisar
    
    Call Unload(Me)
    
    Set oAtualiza = Nothing
    
    Exit Sub
    
Trata_Erro:

    Call TratarErro("BtnOk_Click", Me.name)
    Call FinalizarAplicacao

End Sub

Private Sub Form_Load()

    CentraFrm Me
    mskValSubvencao.Mask = 15.2

End Sub


