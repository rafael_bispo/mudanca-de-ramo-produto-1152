VERSION 5.00
Object = "{C932BA88-4374-101B-A56C-00AA003668DC}#1.1#0"; "msmask32.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.2#0"; "MSCOMCTL.OCX"
Begin VB.Form frmSubvencao 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "SEGP1087 - Avalia��o das Subven��es - BB Seguro Agr�cola"
   ClientHeight    =   8025
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   11070
   BeginProperty Font 
      Name            =   "MS Sans Serif"
      Size            =   8.25
      Charset         =   0
      Weight          =   700
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmSubvencao.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   ScaleHeight     =   8025
   ScaleWidth      =   11070
   Begin VB.CommandButton BtnRecusarCadin 
      Caption         =   "Recusar CADIN"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   7560
      TabIndex        =   23
      Top             =   7230
      Width           =   1665
   End
   Begin VB.CommandButton cmdDesmarcarTodos 
      Caption         =   "Desmarcar Todos"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   2160
      TabIndex        =   5
      Top             =   7230
      Width           =   1665
   End
   Begin VB.Frame Frame1 
      Caption         =   "Crit�rios de Pesquisa"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2500
      Left            =   90
      TabIndex        =   13
      Top             =   120
      Width           =   10935
      Begin VB.ComboBox cboTipoContrato 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   210
         Style           =   2  'Dropdown List
         TabIndex        =   24
         Top             =   690
         Width           =   1905
      End
      Begin VB.ComboBox cboUF 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         ItemData        =   "frmSubvencao.frx":0442
         Left            =   3090
         List            =   "frmSubvencao.frx":0444
         Style           =   2  'Dropdown List
         TabIndex        =   22
         Top             =   660
         Width           =   1125
      End
      Begin VB.ComboBox cboTipoSubvencao 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         ItemData        =   "frmSubvencao.frx":0446
         Left            =   210
         List            =   "frmSubvencao.frx":0448
         Style           =   2  'Dropdown List
         TabIndex        =   20
         Top             =   1380
         Width           =   1905
      End
      Begin MSMask.MaskEdBox txtProposta 
         Height          =   345
         Left            =   3090
         TabIndex        =   1
         Top             =   1380
         Visible         =   0   'False
         Width           =   2205
         _ExtentX        =   3889
         _ExtentY        =   609
         _Version        =   393216
         MaxLength       =   9
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Mask            =   "#########"
         PromptChar      =   "_"
      End
      Begin VB.ComboBox cboPesquisa 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         ItemData        =   "frmSubvencao.frx":044A
         Left            =   210
         List            =   "frmSubvencao.frx":044C
         Style           =   2  'Dropdown List
         TabIndex        =   0
         Top             =   2070
         Width           =   1935
      End
      Begin VB.CheckBox chkProposta 
         Caption         =   "Verificar Propostas Avaliadas"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   465
         Left            =   3090
         TabIndex        =   9
         Top             =   1870
         Width           =   1755
      End
      Begin VB.ComboBox cboRemessa 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   3120
         Style           =   2  'Dropdown List
         TabIndex        =   2
         Top             =   1410
         Visible         =   0   'False
         Width           =   2175
      End
      Begin VB.CommandButton BtnPesquisa 
         Caption         =   "Pesquisar"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   8640
         TabIndex        =   3
         Top             =   2010
         Width           =   1665
      End
      Begin VB.Label lblTipoContrato 
         Caption         =   "Tipo de Contrato:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   165
         Left            =   210
         TabIndex        =   25
         Top             =   450
         Width           =   1575
      End
      Begin VB.Label lblUF 
         Caption         =   "UF:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   3120
         TabIndex        =   21
         Top             =   450
         Width           =   525
      End
      Begin VB.Label Label1 
         Caption         =   "Tipo de subven��o:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   165
         Left            =   210
         TabIndex        =   19
         Top             =   1110
         Width           =   1575
      End
      Begin VB.Label lblProposta 
         Caption         =   "Proposta :"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   3120
         TabIndex        =   16
         Top             =   1140
         Visible         =   0   'False
         Width           =   825
      End
      Begin VB.Label lblPesquisa 
         Caption         =   "Pesquisar por:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Left            =   210
         TabIndex        =   15
         Top             =   1770
         Width           =   1095
      End
      Begin VB.Label lblRemessa 
         Caption         =   "Remessa :"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   3120
         TabIndex        =   14
         Top             =   1140
         Visible         =   0   'False
         Width           =   885
      End
   End
   Begin VB.CommandButton BtnAprovar 
      Caption         =   "Aceitar"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   3945
      TabIndex        =   6
      Top             =   7230
      Width           =   1665
   End
   Begin VB.CommandButton BtnRecusar 
      Caption         =   "Recusar"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   5745
      TabIndex        =   7
      Top             =   7230
      Width           =   1665
   End
   Begin VB.CommandButton BtnSair 
      Caption         =   "Sair"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   9325
      TabIndex        =   8
      Top             =   7230
      Width           =   1665
   End
   Begin VB.CommandButton BtnSelecionarTodos 
      Caption         =   "Marcar Todos"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   360
      TabIndex        =   4
      Top             =   7230
      Width           =   1665
   End
   Begin MSComctlLib.StatusBar StatusBar1 
      Align           =   2  'Align Bottom
      Height          =   315
      Left            =   0
      TabIndex        =   10
      Top             =   7710
      Width           =   11070
      _ExtentX        =   19526
      _ExtentY        =   556
      Style           =   1
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Frame fraSubvencao 
      Caption         =   "Subven��es:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4500
      Left            =   60
      TabIndex        =   11
      Top             =   2700
      Width           =   10930
      Begin MSComctlLib.ListView lstSubvencao 
         Height          =   3800
         Left            =   120
         TabIndex        =   12
         Top             =   250
         Width           =   10665
         _ExtentX        =   18812
         _ExtentY        =   6694
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         Checkboxes      =   -1  'True
         _Version        =   393217
         SmallIcons      =   "ImageList1"
         ForeColor       =   -2147483646
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         NumItems        =   0
      End
      Begin VB.Label lblEstimado 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         Height          =   285
         Left            =   9090
         TabIndex        =   18
         Top             =   4100
         Width           =   1695
      End
      Begin VB.Label lblValorRemProp 
         AutoSize        =   -1  'True
         Height          =   195
         Left            =   6240
         TabIndex        =   17
         Top             =   6570
         Width           =   75
      End
   End
End
Attribute VB_Name = "frmSubvencao"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False



'FELIPE LANA - 12/09/2013 (17890498) - IN�CIO
Private Function fncLimiteSubvencao(pPropostaId As Long, iTipoSubvencaoId As Integer, sUF As String) As Currency

    Dim rsSub As Recordset
    
    StrSQL = ""
    StrSQL = StrSQL & "SELECT D.tp_cultura_id, "
    StrSQL = StrSQL & "       D.nome,"
    StrSQL = StrSQL & "       B.limite_sbs"
    StrSQL = StrSQL & " FROM  seguros_db..seguro_esp_agricola_tb A WITH(NOLOCK)"
    StrSQL = StrSQL & "   INNER JOIN als_produto_db..grupo_tp_cultura_subvencao_tb B WITH(NOLOCK)"
    StrSQL = StrSQL & "      ON (B.tp_cultura_id = A.tp_cultura_id)"
    StrSQL = StrSQL & "   INNER JOIN seguros_db..proposta_subvencao_tb C WITH(NOLOCK)"
    StrSQL = StrSQL & "      ON (C.proposta_id = A.proposta_id AND C.tipo_subvencao_id = B.cd_tip_sbs)"
    StrSQL = StrSQL & "   INNER JOIN als_produto_db..tp_cultura_tb D WITH(NOLOCK)"
    StrSQL = StrSQL & "      ON (D.tp_cultura_id = A.tp_cultura_id)"
    StrSQL = StrSQL & " WHERE A.proposta_id = " & pPropostaId
    
    If iTipoSubvencaoId = 2 Then
        If sUF <> "" Then
            StrSQL = StrSQL & " AND sg_uf_sbs = '" & sUF & "'"
        End If
    Else
        StrSQL = StrSQL & " AND sg_uf_sbs IS NULL"
    End If
    
    ' In�cio Altera��o - Tales de S� - INC000004491356
    ' Obter percentual ativo
    StrSQL = StrSQL & " AND b.situacao = 'A'"
    ' Fim Altera��o - INC000004491356
    
        'IN�CIO - Altera��o Nova

    StrSQL = StrSQL & " UNION "
    StrSQL = StrSQL & "SELECT D.tp_cultura_id, "
    StrSQL = StrSQL & "       D.nome,"
    StrSQL = StrSQL & "       B.limite_sbs"
    StrSQL = StrSQL & " FROM  seguros_db..seguro_esp_pecuario_tb A WITH(NOLOCK)"
    StrSQL = StrSQL & "   INNER JOIN als_produto_db..grupo_tp_cultura_subvencao_tb B WITH(NOLOCK)"
    StrSQL = StrSQL & "      ON (B.tp_cultura_id = A.tp_cultura_id)"
    StrSQL = StrSQL & "   INNER JOIN seguros_db..proposta_subvencao_tb C WITH(NOLOCK)"
    StrSQL = StrSQL & "      ON (C.proposta_id = A.proposta_id AND C.tipo_subvencao_id = B.cd_tip_sbs)"
    StrSQL = StrSQL & "   INNER JOIN als_produto_db..tp_cultura_tb D WITH(NOLOCK)"
    StrSQL = StrSQL & "      ON (D.tp_cultura_id = A.tp_cultura_id)"
    StrSQL = StrSQL & " WHERE A.proposta_id = " & pPropostaId
    
    If iTipoSubvencaoId = 2 Then
        If sUF <> "" Then
            StrSQL = StrSQL & " AND sg_uf_sbs = '" & sUF & "'"
        End If
    Else
        StrSQL = StrSQL & " AND sg_uf_sbs IS NULL"
    End If
    
    StrSQL = StrSQL & " AND b.situacao = 'A'"
    
        'FIM - Altera��o Nova
    
     'Rafael Inacio - Confitec SP - C00149269
        StrSQL = StrSQL & " UNION " & vbCrLf
        StrSQL = StrSQL & "SELECT TC.TP_CULTURA_ID, " & vbCrLf
        StrSQL = StrSQL & "    TC.NOME, " & vbCrLf
        StrSQL = StrSQL & "    GTCS.LIMITE_SBS " & vbCrLf
        StrSQL = StrSQL & "FROM SEGUROS_DB.DBO.RISCO_BEM_TB                          AS RB WITH(NOLOCK) " & vbCrLf
        StrSQL = StrSQL & "INNER JOIN SEGUROS_DB.DBO.BEM_CULTURA_TB                  AS BC WITH(NOLOCK) " & vbCrLf
        StrSQL = StrSQL & "ON BC.BEM_ID = RB.BEM_ID " & vbCrLf
        StrSQL = StrSQL & "INNER JOIN ALS_PRODUTO_DB.DBO.GRUPO_TP_CULTURA_SUBVENCAO_TB AS GTCS WITH(NOLOCK) " & vbCrLf
        StrSQL = StrSQL & "ON GTCS.TP_CULTURA_ID = BC.TP_CULTURA_ID " & vbCrLf
        StrSQL = StrSQL & "INNER JOIN SEGUROS_DB.DBO.PROPOSTA_SUBVENCAO_TB           AS PS WITH(NOLOCK)  " & vbCrLf
        StrSQL = StrSQL & "ON  PS.PROPOSTA_ID        = RB.PROPOSTA_ID " & vbCrLf
        StrSQL = StrSQL & "AND PS.TIPO_SUBVENCAO_ID = GTCS.CD_TIP_SBS " & vbCrLf
        StrSQL = StrSQL & "INNER JOIN ALS_PRODUTO_DB.DBO.TP_CULTURA_TB                  AS TC WITH(NOLOCK) " & vbCrLf
        StrSQL = StrSQL & "ON TC.TP_CULTURA_ID = BC.TP_CULTURA_ID " & vbCrLf
        StrSQL = StrSQL & "WHERE RB.PROPOSTA_ID = " & pPropostaId & "" & vbCrLf
        
        If iTipoSubvencaoId = 2 Then
            If sUF <> "" Then
                StrSQL = StrSQL & " AND SG_UF_SBS = '" & sUF & "'" & vbCrLf
            End If
        Else
            StrSQL = StrSQL & " AND SG_UF_SBS IS NULL" & vbCrLf
        End If
        
        StrSQL = StrSQL & "AND GTCS.SITUACAO = 'A' "
              

    Set rsSub = rdocn.Execute(StrSQL)
    
    If Not rsSub.EOF Then
        fncLimiteSubvencao = rsSub("limite_sbs")
    Else
        fncLimiteSubvencao = 0
    End If
           
End Function
'FELIPE LANA - 12/09/2013 (17890498) - FIM

Private Sub SelecionarTodos()

Dim i As Integer
Dim Item As ListItem

    For i = 1 To lstSubvencao.ListItems.Count

        Set Item = lstSubvencao.ListItems(i)
        Item.Checked = True
    Next

End Sub

Private Sub BtnAprovar_Click()

Dim oAceite As Object
Dim rsAceite As Recordset
Dim sRecusa As String
Dim iIndex As Integer
Dim Item As ListItem
Dim sLinha As String

'lrocha 04/07/2008
Dim iTipoSubvencaoId As Integer
Dim sUF As String

Dim intcontador As Integer

If cboTipoSubvencao.ListIndex < 0 Then Exit Sub

iTipoSubvencaoId = cboTipoSubvencao.ItemData(cboTipoSubvencao.ListIndex)
If iTipoSubvencaoId = 2 And cboUF.ListCount > 0 Then
    sUF = cboUF.Text
Else
    sUF = ""
End If

Dim sel As Integer  'FELIPE LANA - 26/08/2013 (17890498)

''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    
    On Error GoTo Trata_Erro

        'remessa
        If cboPesquisa.Text = "Remessa" Then
    If MsgBox("Deseja aceitar a(s) proposta(s) selecionada(s) ?", 36, "SEGP1087") = 6 Then
                        Set oAceite = CreateObject("SEGL0284.cls00459")
                            sRecusa = "N"
                            sLinha = ""
    
                            'Criando tabela temporaria
                            For iIndex = 1 To lstSubvencao.ListItems.Count
                                Set Item = lstSubvencao.ListItems(iIndex)
                                If Item.Checked = True Then
                                        
                                    'FELIPE LANA - 26/08/2013
                                    'DEMANDA 17890498 - INICIO
                                        valSubvencaoEstim = Item.SubItems(6)
                                        valSubvencaoInf = Item.SubItems(7)
                                        valLimiteSubvencao = fncLimiteSubvencao(Item.SubItems(1), iTipoSubvencaoId, sUF)
                                        
                                        If CCur(valSubvencaoEstim) > CCur(valLimiteSubvencao) Then
                                            If CCur(valSubvencaoInf) = 0 Or CCur(valSubvencaoInf) > CCur(valLimiteSubvencao) Then
                                                limiteSBSUltrapassou = True
                                            'Carla Corr�a (INC000004230193 - 20/12/2013) - Inclus�o de proposta na vari�vel, proposta n�o estava entrando no processamento
                                            Else
                                                sLinha = sLinha & Item.SubItems(1) & ","
                                            End If
                                        Else
                                            sLinha = sLinha & Item.SubItems(1) & ","
                                        End If
                                        sel = sel + 1
                                    'DEMANDA 17890498 - FIM
                                End If
                            Next
                            If sLinha = "" And sel = 0 Then     'FELIPE LANA - 26/08/2013 (17890498)
                                MsgBox "Nenhuma proposta foi selecionada !", vbInformation, "SEGP1087"
                                Exit Sub
                            Else
                                If sLinha <> "" Then            'FELIPE LANA - 26/08/2013 (17890498)
                                    sLinha = Mid(sLinha, 1, Len(sLinha) - 1)
                                    Call oAceite.ProcessarPropostas(gsSIGLASISTEMA, _
                                                                App.Title, _
                                                                App.FileDescription, _
                                                                glAmbiente_id, _
                                                                cUserName, _
                                                                sRecusa, _
                                                                sLinha, _
                                                                iTipoSubvencaoId, _
                                                                sUF)
                                End If
                            End If
                            
                            Set rsAceite = Nothing
                            Set oAceite = Nothing
                            
                            'FELIPE LANA - 26/08/2013
                            'DEMANDA 17890498 - INICIO
                            If limiteSBSUltrapassou Then
                                MsgBox "Houve(ram) proposta(s) que n�o foi(ram) aceita(s)," & vbNewLine & " pois ultrapassara(m) o limite de subven��o.", vbInformation, "SEGP1087"
                            End If
                            'DEMANDA 17890498 - FIM
                            
                            MsgBox "Proposta(s) processadas com sucesso !", vbInformation, "SEGP1087"
                            
                            Call BtnPesquisa_Click
                    Else
                        Exit Sub
                    End If
                
        
        'culturas - executa proposta por proposta por causa da vers�o
        ElseIf cboPesquisa.Text = "Cultura" Then
                    If MsgBox("Deseja aceitar a(s) proposta(s) selecionada(s) ?", 36, "SEGP1087") = 6 Then
        Set oAceite = CreateObject("SEGL0284.cls00459")
        sRecusa = "N"
        sLinha = ""
        
        'Criando tabela temporaria
        For iIndex = 1 To lstSubvencao.ListItems.Count
            Set Item = lstSubvencao.ListItems(iIndex)
            If Item.Checked = True Then
                sLinha = sLinha & Item.SubItems(1) & ","
            End If
        Next
        If sLinha = "" Then
            MsgBox "Nenhuma proposta foi selecionada !", vbInformation, "SEGP1087"
            Exit Sub
                            Else
                                'iIndex = 0
                                'executa proposta por proposta
                                For iIndex = 1 To lstSubvencao.ListItems.Count
                                    Set Item = lstSubvencao.ListItems(iIndex)
                                    'intcontador = 0
                                    If Item.Checked = True Then
        
                                        sLinha = Item.SubItems(1)
                                        
                                        'FELIPE LANA - 26/08/2013
                                        'DEMANDA 17890498 - INICIO
                                            valSubvencaoEstim = Item.SubItems(6)
                                            valSubvencaoInf = Item.SubItems(7)
                                            valLimiteSubvencao = fncLimiteSubvencao(Item.SubItems(1), iTipoSubvencaoId, sUF)
                                        
                                            If CCur(valSubvencaoEstim) > CCur(valLimiteSubvencao) Then
                                                If CCur(valSubvencaoInf) = 0 Or CCur(valSubvencaoInf) > CCur(valLimiteSubvencao) Then
                                                    limiteSBSUltrapassou = True
                                                    itemPassou = True
                                                End If
        Else
                                                itemPassou = False
                                            End If
                                        'DEMANDA 17890498 - FIM
        
           
                                        If lstSubvencao.SelectedItem.ListSubItems.Item(10).Text = "0" Then
                                            intcontador = intcontador + 1
                                        Else
                                          If Not itemPassou Then        'FELIPE LANA - 26/08/2013 (17890498)
            Call oAceite.ProcessarPropostas(gsSIGLASISTEMA, _
                                            App.Title, _
                                            App.FileDescription, _
                                            glAmbiente_id, _
                                            cUserName, _
                                            sRecusa, _
                                            sLinha, _
                                            iTipoSubvencaoId, _
                                            sUF)
        End If
                                        End If
                                    End If
                                Next
                            End If
        
        Set rsAceite = Nothing
                    Set oAceite = Nothing
                    
                    'exibe a mensagem caso tenha propostas sem direito ao subs�dio
                    'If intcontador = 1 Then
                    '    MsgBox "Proposta sem Direito ao Subs�dio.", vbInformation, "SEGP1087"
                        'Exit Sub
                    'Else
                    If intcontador >= 1 Then
                        MsgBox "Existe(m) proposta(s) " & vbCrLf & _
                               "sem Direito ao Subs�dio", vbInformation, "SEGP1087"
                        'End If
                    End If
        
                       'FELIPE LANA
                       'DEMANDA 17890498 - INICIO
                            If limiteSBSUltrapassou Then
                                MsgBox "Houve(ram) proposta(s) que n�o foi(ram) aceita(s)," & vbNewLine & " pois ultrapassara(m) o limite de subven��o.", vbInformation, "SEGP1087"
                            End If
                       'DEMANDA 17890498 - FIM
        
        MsgBox "Proposta(s) processadas com sucesso !", vbInformation, "SEGP1087"
        
        Call BtnPesquisa_Click
                Else
                    Exit Sub
                End If
        'PROPOSTA
        Else
            If MsgBox("Deseja aceitar a(s) proposta(s) selecionada(s) ?", 36, "SEGP1087") = 6 Then
                Set oAceite = CreateObject("SEGL0284.cls00459")
                    sRecusa = "N"
                    sLinha = ""
                
                'Criando tabela temporaria
                For iIndex = 1 To lstSubvencao.ListItems.Count
                    Set Item = lstSubvencao.ListItems(iIndex)
                    If Item.Checked = True Then

                        'FELIPE LANA
                        'DEMANDA 17890498 - INICIO
                            valSubvencaoEstim = Item.SubItems(6)
                            valSubvencaoInf = Item.SubItems(7)
                            valLimiteSubvencao = fncLimiteSubvencao(Item.SubItems(1), iTipoSubvencaoId, sUF)
                            
                            If CCur(valSubvencaoEstim) > CCur(valLimiteSubvencao) Then
                                If CCur(valSubvencaoInf) = 0 Or CCur(valSubvencaoInf) > CCur(valLimiteSubvencao) Then
                                    limiteSBSUltrapassou = True
                               'Carla Corr�a (INC000004230193 - 20/12/2013) - Inclus�o de proposta na vari�vel, proposta n�o estava entrando no processamento
                                Else
                                    sLinha = sLinha & Item.SubItems(1) & ","
                                End If

                            Else
                                sLinha = sLinha & Item.SubItems(1) & ","
                            End If
                            sel = sel + 1
                        'DEMANDA 17890498 - FIM
    
                    End If
                Next
                If sLinha = "" And sel = 0 Then     'FELIPE LANA - 26/08/2013 (17890498)
                    MsgBox "Nenhuma proposta foi selecionada !", vbInformation, "SEGP1087"
        Exit Sub
                Else
                    If sLinha <> "" Then            'FELIPE LANA - 26/08/2013 (17890498)
                        sLinha = Mid(sLinha, 1, Len(sLinha) - 1)
                        Call oAceite.ProcessarPropostas(gsSIGLASISTEMA, _
                                                App.Title, _
                                                App.FileDescription, _
                                                glAmbiente_id, _
                                                cUserName, _
                                                sRecusa, _
                                                sLinha, _
                                                iTipoSubvencaoId, _
                                                sUF)
                    End If
                End If
        
                'FELIPE LANA
                'DEMANDA 17890498 - INICIO
                If limiteSBSUltrapassou Then
                    MsgBox "Houve(ram) proposta(s) que n�o foi(ram) aceita(s)," & vbNewLine & " pois ultrapassara(m) o limite de subven��o.", vbInformation, "SEGP1087"
    End If
                'DEMANDA 17890498 - FIM

                MsgBox "Proposta(s) processadas com sucesso !", vbInformation, "SEGP1087"
            
            Set rsAceite = Nothing
            Set oAceite = Nothing
          
            Call BtnPesquisa_Click
        Else
            Exit Sub
        End If
        
    End If
    Exit Sub
    
Trata_Erro:

    Call TratarErro("BtnAprovar_Click", Me.name)
    Call FinalizarAplicacao

End Sub


Private Sub BtnPesquisa_Click()
    
    Call Pesquisar
    
End Sub

Public Sub Pesquisar()

Dim oConsulta As Object
Dim rsConsulta As Recordset
Dim cValorTotalEstimado As Currency
Dim i As Integer

'lrocha 04/07/2008
Dim iTipoSubvencaoId As Integer
Dim sUF As String

iTipoSubvencaoId = cboTipoSubvencao.ItemData(cboTipoSubvencao.ListIndex)
If iTipoSubvencaoId = 2 And cboUF.ListCount > 0 Then
    sUF = cboUF.Text
Else
    sUF = ""
End If

    On Error GoTo Trata_Erro
    
    cValorTotalEstimado = 0
    
    If cboPesquisa.ListIndex <> -1 Then
    
        If cboPesquisa.Text = "Proposta" Then
            If txtProposta.ClipText = "" Then
                MsgBox "Preencha o campo Proposta !", vbInformation, "SEGP1087"
                txtProposta.SetFocus
                Exit Sub
            End If
        ElseIf cboPesquisa.Text = "Remessa" Then
            If cboRemessa.ListIndex = -1 Then
                MsgBox "Escolha uma remessa !", vbInformation, "SEGP1087"
                cboRemessa.SetFocus
                Exit Sub
            End If
        ElseIf cboPesquisa.Text = "Cultura" Then
                If cboRemessa.ListIndex = -1 Then
                    MsgBox "Escolha uma cultura !", vbInformation, "SEGP1087"
                    cboRemessa.SetFocus
                    Exit Sub
                End If
        End If
        
        'lrocha 04/07/2008
        If cboTipoSubvencao.ListIndex = -1 Then
            MsgBox "Escolha um tipo de subven��o !", vbInformation, "SEGP1087"
            Exit Sub
        End If
        
        If cboTipoSubvencao.ListIndex > 0 And cboUF.ListIndex = -1 Then
            MsgBox "Escolha uma UF !", vbInformation, "SEGP1087"
            Exit Sub
        End If
        
        
        If cboTipoContrato.ListIndex = -1 Then
            MsgBox "Escolha um tipo de contrato!", vbInformation, "SEGP1087"
            Exit Sub
        End If
        
    Else

        MsgBox "Selecione um item de pesquisa!", vbInformation, "SEGP1087"
        cboPesquisa.SetFocus
        Exit Sub

    End If

    Set oConsulta = CreateObject("SEGL0284.cls00459")
    
    Call oConsulta.SetAmbiente(gsSIGLASISTEMA, _
                              glAmbiente_id, _
                              App.Title, _
                              App.FileDescription)

    If cboPesquisa.Text = "Remessa" Then
    
        Set rsConsulta = oConsulta.ConsultarPropostas(gsSIGLASISTEMA, _
                                                      App.Title, _
                                                      App.FileDescription, _
                                                      glAmbiente_id, _
                                                      cUserName, _
                                                      cboRemessa.ItemData(cboRemessa.ListIndex), _
                                                      0, _
                                                      chkProposta.Value, _
                                                      iTipoSubvencaoId, _
                                                      sUF, _
                                                      cboTipoContrato.Text)
                                                              
        lblValorRemProp.Caption = "Valor estimado total da remessa:"
        lblValorRemProp.Visible = True
        lblEstimado.Visible = True
        Screen.MousePointer = vbDefault
                                                              
    'ricardo santos - demanda 17789489
    'pesquisa por Cultura
    ElseIf cboPesquisa.Text = "Cultura" Then
    Screen.MousePointer = vbHourglass
        Set rsConsulta = oConsulta.ConsultarPropostasCultura(gsSIGLASISTEMA, _
                                                             App.Title, _
                                                             App.FileDescription, _
                                                             glAmbiente_id, _
                                                             cUserName, _
                                                             cboRemessa.ItemData(cboRemessa.ListIndex), _
                                                             0, _
                                                             chkProposta.Value, _
                                                             iTipoSubvencaoId, _
                                                             sUF)
                                                             
        

        lblValorRemProp.Caption = "Valor estimado total da remessa:"
        lblValorRemProp.Visible = True
        lblEstimado.Visible = True
    Screen.MousePointer = vbDefault
    Else
    
        Set rsConsulta = oConsulta.ConsultarPropostas(gsSIGLASISTEMA, _
                                                      App.Title, _
                                                      App.FileDescription, _
                                                      glAmbiente_id, _
                                                      cUserName, _
                                                      0, _
                                                      txtProposta.ClipText, _
                                                      chkProposta.Value, _
                                                      iTipoSubvencaoId, _
                                                      sUF, _
                                                      cboTipoContrato.Text)
    
        lblValorRemProp.Caption = "Valor estimado total da proposta:"
        lblValorRemProp.Visible = True
        lblEstimado.Visible = True
        Screen.MousePointer = vbDefault
    End If

    'Atualizando interface
    lstSubvencao.Visible = False
    lstSubvencao.Checkboxes = True
    
    'Configurando ListView
    lstSubvencao.View = lvwReport
    lstSubvencao.FullRowSelect = True
    lstSubvencao.LabelEdit = lvwManual
    lstSubvencao.HotTracking = True
    
    'Configurando header do ListView
    lstSubvencao.ColumnHeaders.Clear
    lstSubvencao.ListItems.Clear

    lstSubvencao.ColumnHeaders.Add , , "", 300
    lstSubvencao.ColumnHeaders.Add , , "No. Proposta", 1200
    lstSubvencao.ColumnHeaders.Add , , "Segurado", 3000
    lstSubvencao.ColumnHeaders.Add , , "Dt. Contratacao", 1200
    lstSubvencao.ColumnHeaders.Add , , "Custo da Ap�lice", 1200
    lstSubvencao.ColumnHeaders.Add , , "Val. Pr�mio", 1200
    lstSubvencao.ColumnHeaders.Add , , "Val. Subven��o Est.", 1250
    lstSubvencao.ColumnHeaders.Add , , "Val. Subven��o Inf.", 1250
    
    'lrocha 08/07/2008
    lstSubvencao.ColumnHeaders.Add , , "Tipo subvencao", 0
    lstSubvencao.ColumnHeaders.Add , , "UF", 0
    '''''''''''''''''''''''''''''''''''''''''''''''''''''''
    
    If cboPesquisa.Text = "Cultura" Then
        lstSubvencao.ColumnHeaders.Add , , "subsidio", 0
    End If
    
    lstSubvencao.ColumnHeaders.Item(5).Alignment = lvwColumnRight
    lstSubvencao.ColumnHeaders.Item(6).Alignment = lvwColumnRight
    lstSubvencao.ColumnHeaders.Item(7).Alignment = lvwColumnRight
    lstSubvencao.ColumnHeaders.Item(8).Alignment = lvwColumnRight
    

    If rsConsulta.EOF Then
        
        lblEstimado.Visible = False
        lblValorRemProp.Visible = False
        MsgBox "Nenhuma proposta encontrada !", vbInformation, "SEGP1087"
        lstSubvencao.ListItems.Clear
   
    Else

        'Preenchendo ListView
        While Not rsConsulta.EOF
        
            Set Item = lstSubvencao.ListItems.Add(, , "")
            
            Item.SubItems(1) = rsConsulta("proposta_id")
            Item.SubItems(2) = rsConsulta("nome")
            Item.SubItems(3) = rsConsulta("dt_contratacao")
            Item.SubItems(4) = Format(rsConsulta("custo_certificado"), "###,###,###,###,###0.00")
            Item.SubItems(5) = rsConsulta("val_premio_bruto")
            
            'SD01050443 - Alexandre Debouch - Confitec - 21/11/2018
            'Corrigindo a falta de tratamento para campos sem conte�do (Null)
            'Item.SubItems(6) = Format(rsConsulta("val_subvencao_estimado"), "###,###,###,###,###0.00")
            Item.SubItems(6) = Format(IIf(IsNull(rsConsulta("val_subvencao_estimado")), 0, rsConsulta("val_subvencao_estimado")), "###,###,###,###,###0.00")
            'Corrigindo a falta de tratamento para campos sem conte�do (Null)
            'SD01050443 - Alexandre Debouch - Confitec - 21/11/2018
           
            
            Item.SubItems(7) = Format(IIf(IsNull(rsConsulta("val_subvencao_informado")), 0, rsConsulta("val_subvencao_informado")), "###,###,###,###,###0.00")
            
            'lrocha 08/07/2008
            Item.SubItems(8) = cboTipoSubvencao.ItemData(cboTipoSubvencao.ListIndex)
            If cboTipoSubvencao.ItemData(cboTipoSubvencao.ListIndex) = 2 Then
                Item.SubItems(9) = cboUF.Text
            Else
                Item.SubItems(9) = ""
            End If
            '''''''''''''''''''''''''''''''''
            
             If cboPesquisa.Text = "Cultura" Then
                Item.SubItems(10) = rsConsulta("direito_subsidio")
             End If
           
            'SD01050443 - Alexandre Debouch - Confitec - 21/11/2018
            'Corrigindo a falta de tratamento para campos sem conte�do (Null)
            'cValorTotalEstimado = cValorTotalEstimado + rsConsulta("val_subvencao_estimado")
            cValorTotalEstimado = cValorTotalEstimado + IIf(IsNull(rsConsulta("val_subvencao_estimado")), 0, rsConsulta("val_subvencao_estimado"))
            'Corrigindo a falta de tratamento para campos sem conte�do (Null)
            'SD01050443 - Alexandre Debouch - Confitec - 21/11/2018
        
            rsConsulta.MoveNext
        
        Wend
    
        lblEstimado.Caption = Format(cValorTotalEstimado, "###,###,###,###,###0.00")
        
        rsConsulta.Close

    End If

    lstSubvencao.Visible = True

    Set rsConsulta = Nothing
    
    Set oConsulta = Nothing
    
    Exit Sub
    
Trata_Erro:

    Call TratarErro("Pesquisar", Me.name)
    Call FinalizarAplicacao
End Sub


Private Sub BtnRecusar_Click()

RecusarProposta "N"

End Sub

Private Sub BtnRecusarCadin_Click()

RecusarProposta "S"

End Sub

Private Sub RecusarProposta(Optional CADIN As String)

    Dim oRecusa As Object
    Dim rsRecusa As Recordset
    Dim sRecusa As String
    Dim sLinha As String
    Dim sMensagem As String
    
    'lrocha 04/07/2008
    Dim iTipoSubvencaoId As Integer
    Dim sUF As String

    If cboTipoSubvencao.ListIndex < 0 Then Exit Sub

    If CADIN = "" Or CADIN <> "S" Then
        CADIN = "N"
    End If

    iTipoSubvencaoId = cboTipoSubvencao.ItemData(cboTipoSubvencao.ListIndex)
    If iTipoSubvencaoId = 2 And cboUF.ListCount > 0 Then
        sUF = cboUF.Text
    Else
        sUF = ""
    End If
    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    
    On Error GoTo Trata_Erro


    If CADIN = "S" Then
        sMensagem = "Ao recusar esta(s) proposta(s), sistema registrar� como restri��o federal no CADIN, impossibilitando a subven��o estadual. Deseja Continuar? "
    Else
        sMensagem = "Deseja recusar a(s) proposta(s) selecionada(s) ?"
    End If

    If MsgBox(sMensagem, 36, "SEGP1087") = 6 Then
        Set oRecusa = CreateObject("SEGL0284.cls00459")
        
        sRecusa = "S"
        sLinha = ""
    
        'Criando tabela temporaria
        For iIndex = 1 To lstSubvencao.ListItems.Count
    
            Set Item = lstSubvencao.ListItems(iIndex)
    
            If Item.Checked = True Then
                sLinha = sLinha & Item.SubItems(1) & ","
            End If
    
        Next
        
        If sLinha = "" Then
        
            MsgBox "Nenhuma proposta foi selecionada !", vbInformation, "SEGP1087"
            Exit Sub
        
        Else
        
            sLinha = Mid(sLinha, 1, Len(sLinha) - 1)
        
            Call oRecusa.ProcessarPropostas(gsSIGLASISTEMA, _
                                            App.Title, _
                                            App.FileDescription, _
                                            glAmbiente_id, _
                                            cUserName, _
                                            sRecusa, _
                                            sLinha, _
                                            iTipoSubvencaoId, _
                                            sUF, _
                                            CADIN)
        End If
        
        Set rsRecusa = Nothing
        
        Set oRecusa = Nothing
        
        MsgBox "Proposta(s) processadas com sucesso !", vbInformation, "SEGP1087"
    
        Call BtnPesquisa_Click

    Else
    
        Exit Sub
    
    End If

    Exit Sub
    
Trata_Erro:

    Call TratarErro("BtnRecusar_Click", Me.name)
    Call FinalizarAplicacao


End Sub


Private Sub BtnSair_Click()

    End
    
End Sub

Private Sub BtnSelecionarTodos_Click()

    Call SelecionarTodos

End Sub

Private Sub cboPesquisa_Click()
    
    Call ConfigurarInterface

End Sub
'lrocha 04/07/2008 - Valida��o da combo de UF
Private Sub cboTipoSubvencao_Click()

If cboTipoSubvencao.ItemData(cboTipoSubvencao.ListIndex) = 2 Then
    If cboUF.ListCount > 0 Then
        cboUF.Visible = True
        lblUF.Visible = True
        BtnRecusarCadin.Enabled = False
    Else
        MsgBox "N�o h� siglas ativas! ", vbInformation, "SEGP1087"
        cboTipoSubvencao.ListIndex = 0
    End If
Else
    cboUF.Visible = False
    lblUF.Visible = False
    BtnRecusarCadin.Enabled = True
End If

End Sub

Private Sub chkProposta_Click()
    If chkProposta.Value = vbChecked Then
        BtnAprovar.Enabled = False
        BtnSelecionarTodos.Enabled = False
        BtnRecusar.Enabled = False
    Else
        BtnAprovar.Enabled = True
        BtnSelecionarTodos.Enabled = True
        BtnRecusar.Enabled = True
    End If
End Sub

Private Sub cmdDesmarcarTodos_Click()
    Call DesmarcarTodos
End Sub

Private Sub Form_Load()

    On Error GoTo Trata_Erro
   
   cUserName = "antonio.silva"
    glAmbiente_id = 3
    
'   If Not Trata_Parametros(Command) Then
 '      Call Unload(Me)
  '  End If

    ConexaoEvento

    Call ObterDataSistema(gsSIGLASISTEMA)

    Call CentraFrm(Me)
    
    'JOAO.MACHADO
    'ALTERACAO PARA CONTEMPLAR ENDOSSO DE ALTERACAO DE PROCO BASE
    Call CarregarComboTipoContrato
    
    Call CarregarComboPesquisa
    
    'lrocha 04/07/2008 - Demanda 380818
    Call CarregarComboTipoSubvencao
    Call CarregarComboUF
    
    cboUF.Visible = False
    lblUF.Visible = False
    
    Exit Sub

Trata_Erro:

    Call TratarErro("Form_Load", Me.name)
    Call FinalizarAplicacao
          
End Sub

Private Sub CarregarComboUF()

On Error GoTo Trata_Erro
    
    
Dim oRs As Recordset
Dim oUF As Object

Set oUF = CreateObject("SEGL0284.cls00459")

Set oRs = oUF.ObterSiglasAtivas(gsSIGLASISTEMA, _
                                App.Title, _
                                App.FileDescription, _
                                glAmbiente_id)
cboUF.Clear

If Not oRs.EOF Then
    While Not oRs.EOF
        cboUF.AddItem oRs("SG_UF_SGRO_AGRL")
    oRs.MoveNext
    Wend
End If

Set oUF = Nothing

oRs.Close
Set oRs = Nothing

Exit Sub

Trata_Erro:

    Call TratarErro("CarregarComboUF", Me.name)
    Call FinalizarAplicacao

End Sub



Private Sub CarregarComboTipoSubvencao()

    cboTipoSubvencao.Clear
    
    cboTipoSubvencao.AddItem "Federal"
    cboTipoSubvencao.ItemData(cboTipoSubvencao.NewIndex) = 1
    
    cboTipoSubvencao.AddItem "Estadual"
    cboTipoSubvencao.ItemData(cboTipoSubvencao.NewIndex) = 2
    
    
End Sub

Private Sub CarregarComboPesquisa()

    cboPesquisa.Clear
    cboPesquisa.AddItem "Proposta"
    cboPesquisa.AddItem "Remessa"
    '--------------------------------------------------------------------------------
    ' Analista  : ricardo santos (Confitec)
    ' Data      : 06/03/2013
    ' Demanda   : 17789489
    ' Objetivo  : Novo item de pesquisa
    '--------------------------------------------------------------------------------
    cboPesquisa.AddItem "Cultura"
End Sub

'JOAO.MACHADO
'ALTERACAO PARA CONTEMPLAR ENDOSSO DE ALTERACAO DE PROCO BASE
Private Sub CarregarComboTipoContrato()

    cboTipoContrato.Clear
    
    cboTipoContrato.AddItem "Contrata��o"
    cboTipoContrato.ItemData(cboTipoContrato.NewIndex) = 1
    
    cboTipoContrato.AddItem "Endosso"
    cboTipoContrato.ItemData(cboTipoContrato.NewIndex) = 2

End Sub

Private Sub ConfigurarInterface()

    If cboTipoSubvencao.ListIndex = -1 Then
        MsgBox "� necess�rio escolher o tipo de subven��o !", vbInformation, "SEGP1087"
        Exit Sub
    End If

    If cboPesquisa.Text = "Remessa" Or cboPesquisa.Text = "Cultura" Then
        lstSubvencao.ColumnHeaders.Clear
        lstSubvencao.ListItems.Clear
        Call CarregarComboRemessa
        lblRemessa.Visible = True
        If cboPesquisa.Text = "Remessa" Then
            lblRemessa.Caption = "Remessa"
        ElseIf cboPesquisa.Text = "Cultura" Then
            lblRemessa.Caption = "Cultura"
        End If
        cboRemessa.Visible = True
        lblProposta.Visible = False
        txtProposta.Visible = False
        txtProposta.Text = "_________"
        lblEstimado.Visible = False
        lblValorRemProp.Visible = False
    Else
        lstSubvencao.ColumnHeaders.Clear
        lstSubvencao.ListItems.Clear
        lblRemessa.Visible = False
        cboRemessa.Visible = False
        lblProposta.Visible = True
        txtProposta.Visible = True
        txtProposta.Text = "_________"
        txtProposta.SetFocus
        lblEstimado.Visible = False
        lblValorRemProp.Visible = False
    End If

End Sub

Private Sub CarregarComboRemessa()

    Dim oRemessa As Object
    Dim rsRemessa As Recordset
    Dim sUF As String

    On Error GoTo Trata_Erro
    
    'lrocha 04/07/2008
    If cboTipoSubvencao.ListIndex = 1 And cboUF.ListCount > 0 Then
        sUF = cboUF.Text
    Else
        sUF = ""
    End If
    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    

    Set oRemessa = CreateObject("SEGL0284.cls00459")
    
    Call oRemessa.SetAmbiente(gsSIGLASISTEMA, _
                              glAmbiente_id, _
                              App.Title, _
                              App.FileDescription)
 
    If cboPesquisa.ListIndex = 1 Then
        Screen.MousePointer = vbHourglass
    Set rsRemessa = oRemessa.ObterRemessas(gsSIGLASISTEMA, _
                                           App.Title, _
                                           App.FileDescription, _
                                           glAmbiente_id, _
                                           cUserName, _
                                           cboTipoSubvencao.ItemData(cboTipoSubvencao.ListIndex), _
                                           sUF)
    
    cboRemessa.Clear
    
    While Not rsRemessa.EOF
        cboRemessa.AddItem rsRemessa(0) & "." & Format(rsRemessa(1), "0000")
        cboRemessa.ItemData(cboRemessa.NewIndex) = CInt(rsRemessa(1))
        rsRemessa.MoveNext
    Wend
    
        Screen.MousePointer = vbDefault
    rsRemessa.Close
    Set rsRemessa = Nothing
    Set oRemessa = Nothing
    
    '--------------------------------------------------------------------------------
    ' Analista  : ricardo santos (Confitec)
    ' Data      : 06/03/2013
    ' Demanda   : 17789489
    ' Objetivo  : carrega a combo(cboRemessa) com as Culturas Ativas
    '             SEGL0284.CLS00459(OBTERCULTURAS)
    '--------------------------------------------------------------------------------
    ElseIf cboPesquisa.ListIndex = 2 Then
        Screen.MousePointer = vbHourglass
        Set rsRemessa = oRemessa.ObterCulturas(gsSIGLASISTEMA, _
                                               App.Title, _
                                               App.FileDescription, _
                                               glAmbiente_id, _
                                               cUserName, _
                                               cboTipoSubvencao.ItemData(cboTipoSubvencao.ListIndex), _
                                               sUF)

        cboRemessa.Clear

        While Not rsRemessa.EOF
            cboRemessa.AddItem UCase(rsRemessa(1))
            cboRemessa.ItemData(cboRemessa.NewIndex) = CInt(rsRemessa(0))
            rsRemessa.MoveNext
        Wend

        Screen.MousePointer = vbDefault
        rsRemessa.Close
        Set rsRemessa = Nothing
        Set oRemessa = Nothing
    
    End If
    
    Exit Sub
    
Trata_Erro:

    Call TratarErro("CarregarComboRemessa", Me.name)
    Call FinalizarAplicacao
    
End Sub

Private Sub lstSubvencao_DblClick()

    If chkProposta.Value = vbUnchecked Then

        'Pega proposta, nome do segurado e valor de subvencao para passar para o outro formulario
        frmSubvencaoAlteracao.lblProposta.Caption = lstSubvencao.SelectedItem.SubItems(1)
        frmSubvencaoAlteracao.lblSegurado.Caption = lstSubvencao.SelectedItem.SubItems(2)
        
        'lrocha 08/07/2008
        'frmSubvencaoAlteracao.lblPremioLiquido.Caption = lstSubvencao.SelectedItem.SubItems(5)
        frmSubvencaoAlteracao.lblValestimado.Caption = lstSubvencao.SelectedItem.SubItems(6)
        frmSubvencaoAlteracao.mskValSubvencao.Text = lstSubvencao.SelectedItem.SubItems(7)
        frmSubvencaoAlteracao.txtTipoSubvencao.Text = lstSubvencao.SelectedItem.SubItems(8)
        frmSubvencaoAlteracao.txtUF.Text = lstSubvencao.SelectedItem.SubItems(9)
        '''''''''''''''''''
        
        'Chama o formul�rio frmsubvencaoalteracao
        frmSubvencaoAlteracao.Show vbModal
    
    End If

End Sub

Private Sub DesmarcarTodos()

Dim i As Integer
Dim Item As ListItem

    For i = 1 To lstSubvencao.ListItems.Count

        Set Item = lstSubvencao.ListItems(i)
        Item.Checked = False

    Next

End Sub
