Attribute VB_Name = "modGTR"
'Declara��o de Vari�veis para Utiliza��o dos par�metros do programa
Option Explicit
Global rdocn_Interface As New rdoConnection, rdocn_aux As New rdoConnection
Global rdocn_Controle  As New rdoConnection

Enum eTipoAEnviar
    EnviarEvento1160 = 0
    EnviarDemaisEventos = 1
End Enum

Public msgErro     As String

Private Type tpPRM
        BBUsuarioChave                      As String   'Chave do Usu�rio no Banco do Brasil
        BBUsuarioSenha                      As String   'Senha do Usu�rio no Banco do Brasil
        BBTipoServico                       As String   'Tipo de Servi�o utilizado nas transa��es
        BBVersao                            As String   'Vers�o do protocolo da mensagem da transa��o
        BBCodTransEntrada                   As String   'Obt�m o c�digo da transa��o das remessas de entrada
        BBCodTransSaida                     As String   'Obt�m o c�digo da transa��o das remessas de saida
        BBPortaComunicacao                  As String   'Porta de comunica��o com o Banco do Brasil
        Tempo_Reconexao                     As Long     'Tempo para Reconex�o do TimeOut e uma Nova Sess�o
        Qtde_Tentativas_Conexao             As Long     'Obt�m a Qtde de Tentativas de Comunica��o com o Banco do Brasil)
        Tempo_de_Verificacao                As Integer  'Obt�m o Tempo de verifica��o da exist�ncia de remessas � serem enviadas ao Banco do Brasil
        Tempo_Maximo_Verificacao            As Integer  'Obt�m o Tempo m�ximo de verifica��o da exist�ncia de remessas � serem enviadas ao Banco do Brasil
        Limite_Verificacoes_Tempo_Maximo    As Integer  'Obt�m a quantidade de vezes que a verifica��o do tempo m�ximo de exist�ncia de remessas � serem enviadas ao Banco do Brasil ser� executada
End Type
Global PRM As tpPRM

'Declara��o de Vari�veis para Utiliza��o na transa��o de arquivos entre o
'Banco do Brasil (SERVER) e a Alian�a do Brasil (CLIENT)
Private Type tpGTR
        Entrada_GTR_ID      As Long
        Saida_GTR_ID        As Long
        CodigoRemessa       As String
        NroEvento           As Integer
        SessaoBanco         As String
        SequencialCliente   As Integer
        strMensagem         As String
        strRemessa          As String
        strOUTPUT_I         As String
        qtdeRegistros       As Integer
        codigoTransacao     As String
        CodigoErroRemessa   As Long
        MensagemErroRemessa As String
        Layout_Id           As Integer
        Linha               As Integer
        Sinistro_BB         As String
End Type
Global GTR As tpGTR

'Declara��o de Vari�veis para controlar o timer de Atualiza��o da Lista (Sa�da)
Private Type tpTMR_ATU
        ComecaContagem  As Boolean
        Minuto          As Long
        Segundos        As Long
        Tempo           As Long
End Type
Global TMR_ATU As tpTMR_ATU

Global Eventos As New Collection
Global Eventos_ALS As New Collection

'Declara��o de Constantes para utiliza��o do Systray
Public Const TRAY_MSG_MOUSEMOVE = 7680
Public Const TRAY_MSG_LEFTBTN_DOWN = 7695
Public Const TRAY_MSG_LEFTBTN_UP = 7710
Public Const TRAY_MSG_LEFTBTN_DBLCLICK = 7725
Public Const TRAY_MSG_RIGHTBTN_DOWN = 7740
Public Const TRAY_MSG_RIGHTBTN_UP = 7755
Public Const TRAY_MSG_RIGHTBTN_DBLCLICK = 7770

'Declara��o de Vari�veis que ir� conter o ID retornado pelo comando SHELL
Global ID_PGM    As Long

'Declara��o de Constantes que cont�m o t�tulo do programa e o nome do execut�vel do programa GTR controle
Public Const NOME_PGM_GTR_CONTROLE As String = "SEGP0456 - GTR Controle"
Public Const NOME_EXE_GTR_CONTROLE As String = "SEGP0456.EXE"

'Declara��o de Vari�veis para utiliza��o de conex�o do GTR
Global GTR_servidor As String
Global GTR_banco    As String
Global GTR_usuario  As String
Global GTR_senha    As String

'Declara��o de Constantes que ser�o Alteradas depois que o Banco do Brasil enviar as Informa��es
'Public Const BBStringRegistro = "SN0000001A0090"
Public Const BBStringRegistro = "A0080"
Public Sub Abre_Conexoes_GTR(Optional Carrega_rdocn_Aux As Boolean = False)
       
       If Obtem_Dados_Conexao_GTR(GTR_servidor, GTR_banco, GTR_usuario, GTR_senha) = False Then Call GravaLogErro("", "Erro ao carregar par�metros. Quantidade de par�metros necess�rios diferente do esperado.", "SQL", "OBTEM_DADOS_CONEXAO_GTR", Err.Number): Call TerminaSEGBR
       
       GTR_senha = "SISBR_QLD"
       GTR_servidor = "SISAB051\QUALID"
       GTR_usuario = "sisbr"
       GTR_banco = "interface_Db"
             
       With rdocn_Interface
       
            
            .Connect = "UID=" & GTR_usuario & ";PWD=" & GTR_senha & ";server=" & GTR_servidor & ";driver={SQL Server};database=" & GTR_banco & ";"
            .QueryTimeout = 30000
            .CursorDriver = rdUseNone
            .EstablishConnection rdDriverNoPrompt
       End With
       
       If Carrega_rdocn_Aux Then
          With rdocn_aux
               .Connect = rdocn.Connect
               .CursorDriver = rdUseServer
               .QueryTimeout = 3600
               .EstablishConnection rdDriverNoPrompt
          End With
       End If
       
End Sub
    
Public Sub MontaColecaoEventos(mceEntrada_Saida As String)
       
       Dim rc As rdoResultset
       Dim Evento As clEvento_Layout
       Dim sSql As String
       
       On Error GoTo TrataErro
       
       'Adiciona dados � Cole��o de evento_Layout
       sSql = "SELECT a.evento_SEGBR_id, a.evento_BB_id, b.layout_id"
       sSql = sSql & " FROM SEGUROS_DB..evento_SEGBR_tb a WITH (NOLOCK) "
       sSql = sSql & " INNER JOIN (GTR_layout_evento_SEGBR_tb b WITH (NOLOCK) "
       sSql = sSql & "    INNER JOIN GTR_layout_tb c WITH (NOLOCK) "
       sSql = sSql & "      ON b.layout_id = c.layout_id)"
       sSql = sSql & "    ON a.evento_SEGBR_id = b.evento_SEGBR_id"
       sSql = sSql & " WHERE a.evento_BB_id  IS NOT NULL "
       sSql = sSql & "   AND b.layout_id     IS NOT NULL "
       sSql = sSql & "   AND c.entidade_id   = 5"
       sSql = sSql & "   AND c.entrada_saida = '" & mceEntrada_Saida & "'"
       Set rc = rdocn_Interface.OpenResultset(sSql)
       
       Do While Not rc.EOF
          
          Set Evento = New clEvento_Layout
          Evento.evento_BB_ID = rc("evento_BB_id")
          Evento.Layout_Id = rc("layout_id")
          Eventos.Add Evento
          
       rc.MoveNext
       Loop
        
       rc.Close
       
       'Adiciona dados � Cole��o de als_evento_layout
       sSql = "SELECT a.evento_SEGBR_id, a.evento_BB_id, b.layout_id"
       sSql = sSql & " FROM SEGUROS_DB..evento_SEGBR_tb a WITH (NOLOCK) "
       sSql = sSql & " INNER JOIN (INTERFACE_DADOS_DB..evento_ALS_layout_tb b WITH (NOLOCK) "
       sSql = sSql & "    INNER JOIN INTERFACE_DB..layout_tb c WITH (NOLOCK) "
       sSql = sSql & "      ON b.layout_id = c.layout_id)"
       sSql = sSql & "    ON a.evento_bb_id = b.evento_bb"
       sSql = sSql & " WHERE a.evento_bb_id IS NOT NULL "
       sSql = sSql & "   AND b.layout_id     IS NOT NULL "
       sSql = sSql & "   AND (b.cod_transacao = 'ST00' or b.evento_bb = 8199)" 'Eventos de saida
       
       Set rc = rdocn_Interface.OpenResultset(sSql)
       
       Do While Not rc.EOF
          
          Set Evento = New clEvento_Layout
          Evento.evento_BB_ID = rc("evento_BB_id")
          Evento.Layout_Id = rc("layout_id")
          Eventos_ALS.Add Evento
          
           rc.MoveNext
       Loop
       
       Exit Sub
       
TrataErro:
    'Grava o Log do Erro e Sai da aplica��o
    Call GravaLogErro("", "Descri��o: " & Err.Description, "SQL", "MontaColecaoEventos", Err.Number)
    Call TerminaSEGBR

End Sub

Public Function ObtemConteudo(strChave As String) As String
       
       '--------------------------------------------------'
       ' Pega o Conteudo de determinado Par�metro passado '
       ' na fun��o "CarregaParametros".                   '
       '--------------------------------------------------'
       Dim objAmbiente As Variant
       
       On Error GoTo TrataErro
        
       Set objAmbiente = CreateObject("Ambiente.ClasseAmbiente")
       
       ObtemConteudo = objAmbiente.RetornaValorAmbiente("GTR", "SISTEMA", strChave, gsCHAVESISTEMA)
        
       Set objAmbiente = Nothing
       
       Exit Function
       
TrataErro:
'Grava o Log do Erro
Call GravaLogErro("", "Erro ao carregar conte�do do par�metro. Descri��o do Erro: " & Err.Description, "SQL", "OBTEMCONTEUDO", Err.Number)
Call TerminaSEGBR
End Function

Public Sub GravaLogErro(gleRemessa_ID As String, gleRemessa As String, gleTipo As String, gleRotina As String, gleCodigo_Erro As Long)
       
       Dim strRemessa As String
       
       'Monta a String que ser� gravada no Log de Erro
       If gleCodigo_Erro = 0 Then
          strRemessa = gleRemessa
       Else
          strRemessa = "Remessa: " & IIf(Trim(gleRemessa_ID) = "", "<INDEFINIDO>", gleRemessa_ID) & " - Remessa: " & gleRemessa
       End If
       
       'Chama a Rotina que grava Log na Tabela log_erro_tb atrav�s da sp: log_erro_spi
       Call GravaMensagem(strRemessa, gleTipo, gleRotina)
       
End Sub

Public Sub GravaLogErroSaida(gleSaida_GTR_ID As Long, gleRemessa As String, gleTipo As String, gleRotina As String, gleCodigo_Erro As Long)
       
       Dim strRemessa As String
       
       'Monta a String que ser� gravada no Log de Erro
       If gleCodigo_Erro = 0 Then
          strRemessa = gleRemessa
       Else
          strRemessa = "Saida_GTR_ID: " & IIf(Trim(gleSaida_GTR_ID) = 0, "<INDEFINIDO>", gleSaida_GTR_ID) & " - Remessa: " & gleRemessa
       End If
       
       'Chama a Rotina que grava Log na Tabela log_erro_tb atrav�s da sp: log_erro_spi
       Call GravaMensagem(strRemessa, gleTipo, gleRotina)
       
End Sub

Public Function MontaMensagemeMail(mmmRemessa As String, mmmCodErro As String, mmmDescrErro As String, mmmResumoProblema As String, mmmSolucao As String, mmmConteudo As String) As String
       
       MontaMensagemeMail = "Produ��o, " & vbCrLf & vbCrLf & _
                            "Remessa........: " & mmmRemessa & vbCrLf & _
                            "C�digo Erro....: " & mmmCodErro & vbCrLf & _
                            "Descri��o......: " & mmmDescrErro & vbCrLf & _
                            "Resumo\Problema: " & mmmResumoProblema & vbCrLf & _
                            "Solu��o........: " & mmmSolucao & vbCrLf & _
                            "Conte�do.......: " & mmmConteudo & vbCrLf & _
                            "" & vbCrLf & _
                            "------------------------------------------------------------------" & vbCrLf & _
                            "Comunica��o Corporativa da Alian�a do Brasil"
       
End Function
Public Function MontaMensagemeMailSaida(mmmSaida_GTR_ID As Long, mmmQtdeTentativas As Integer, mmmCodErro As String, mmmDescrErro As String, mmmResumoProblema As String, mmmSolucao As String, mmmConteudo As String) As String
       
       MontaMensagemeMailSaida = "Produ��o, " & vbCrLf & vbCrLf & _
                                 "Registro.......: " & mmmSaida_GTR_ID & vbCrLf & _
                                 "Qtde Tentativas: " & mmmQtdeTentativas & vbCrLf & _
                                 "C�digo Erro....: " & mmmCodErro & vbCrLf & _
                                 "Descri��o......: " & mmmDescrErro & vbCrLf & _
                                 "Resumo\Problema: " & mmmResumoProblema & vbCrLf & _
                                 "Solu��o........: " & mmmSolucao & vbCrLf & _
                                 "" & vbCrLf & _
                                 "------------------------------------------------------------------" & vbCrLf & _
                                 "Comunica��o Corporativa da Alian�a do Brasil"
       
End Function

Public Function Obtem_Dados_Conexao_GTR(ByRef odcServidor As String, ByRef odcBanco As String, ByRef odcUsuario As String, ByRef odcSenha As String) As Boolean
        
'        Dim objAmbiente As Object 'Variant
'
'        Obtem_Dados_Conexao_GTR = False
'
'        On Error GoTo TrataErro
'
'        Set objAmbiente = CreateObject("Ambiente.ClasseAmbiente")
'
'        odcServidor = objAmbiente.RetornaValorAmbiente("GTR", "SISTEMA", "SERVIDOR", gsCHAVESISTEMA)
'        odcBanco = objAmbiente.RetornaValorAmbiente("GTR", "SISTEMA", "BANCO", gsCHAVESISTEMA)
'        odcUsuario = objAmbiente.RetornaValorAmbiente("GTR", "SISTEMA", "USUARIO", gsCHAVESISTEMA)
'        odcSenha = objAmbiente.RetornaValorAmbiente("GTR", "SISTEMA", "SENHA", gsCHAVESISTEMA)
'
'        Set objAmbiente = Nothing
        
        Obtem_Dados_Conexao_GTR = True
        
        Exit Function
        
TrataErro:
Exit Function
End Function

Public Function GTR_Controle_em_Execucao() As Boolean
        
        GTR_Controle_em_Execucao = False
        
        Dim rs  As rdoResultset
        
        On Error GoTo TrataErro
        
        SQS = "SELECT OBJECT_ID('TempDB.." & TabTempUsr & "')"
        Set rs = rdocn.OpenResultset(SQS)
        If Not IsNull(rs(0)) Then GTR_Controle_em_Execucao = True
        rs.Close
        
        Exit Function
        
TrataErro:
Exit Function
End Function

Public Sub Grava_Log_eMail_Finaliza_Aplicacao(glef_Descricao_Erro As String, _
                                     glef_Tipo As String, _
                                     glef_Rotina As String, _
                                     glef_Titulo As String, _
                                     GLEF_MAPISession As MAPISession, _
                                     GLEF_MAPIMessages As MAPIMessages, _
                                     Optional glef_Finaliza As Boolean = False)
'---------------------------------------------------------------------------'
' glef_Descricao_Erro       - � a descri��o do Erro ocorrido                '
' glef_Tipo                 - � o tipo de Erro. Ex.: "PROC", "VB", "SQL"    '
' glef_Rotina               - � o lugar onde ocorreu o Erro.                '
' glef_Titulo               - � o t�tulo da mensagem do e-mail              '
' glef_Finaliza(Opcional)   - Finaliza o Programa, default = "false"        '
'---------------------------------------------------------------------------'
            
            Dim strMessageMail  As String
            
            'Grava a descri��o do erro na tabela de Log
            Call GravaMensagem(glef_Descricao_Erro, glef_Tipo, glef_Rotina)
            
            'Mensagem do e-Mail a ser enviado
            strMessageMail = "Descri��o do Erro: " & glef_Descricao_Erro & vbCrLf & _
                             "Tipo do Erro.....: " & glef_Tipo & vbCrLf & _
                             "Rotina do Erro...: " & glef_Rotina & vbCrLf & _
                             "" & vbCrLf & _
                             "Alian�a do Brasil - Companhia de Seguros"
            
            'Envia e-Mail
            Call Envia_eMail(glef_Titulo, strMessageMail, GLEF_MAPISession, GLEF_MAPIMessages)
            
            If glef_Finaliza = True Then
               'Finaliza a Aplica��o
               Call TerminaSEGBR
            End If
            
End Sub

Public Sub Envia_eMail(emTitulo As String, emMensagem As String, emMAPISession1 As MAPISession, emMAPIMessages1 As MAPIMessages)
        
        Dim i As Integer
        
        On Error GoTo TrataErro
        
        emMAPISession1.Action = 1
        With emMAPIMessages1
             .SessionID = emMAPISession1.SessionID
             .Compose
             .RecipAddress = "jopereira.jrp@aliancadobrasil.com.br"
             .AddressResolveUI = True
             .ResolveName
             .MsgSubject = emTitulo
             .MsgNoteText = emMensagem
             .Send False
        End With
        
        Exit Sub
        
TrataErro:
'Grava o Log do Erro
Call GravaLogErro("", "Erro ao tentar enviar e-Mail. Descri��o do Erro: " & Err.Description, "VB", "ENVIA_EMAIL", Err.Number)
End Sub

Public Function Critica_Dependencia(ByVal OEvento_BB As String, ByVal OSinistro_BB As String, ByVal empresa As String) As Boolean
    
    Dim SQL As String, rs As rdoResultset
    Dim strEvento_Dependencia As String
    
    On Error GoTo TrataErro
    
    Critica_Dependencia = False
    
    ' Obt�m a depend�ncia do evento Atual
    If empresa = 2 Then
        SQL = "SELECT evento_dependencia "
        SQL = SQL & " FROM SEGUROS_DB..dependencia_evento_BB_tb WITH (NOLOCK) "
        SQL = SQL & " WHERE evento_atual = " & OEvento_BB
    Else
        SQL = "SELECT evento_dependencia "
        SQL = SQL & " FROM abss.SEGUROS_DB.dbo.dependencia_evento_BB_tb WITH (NOLOCK) "
        SQL = SQL & " WHERE evento_atual = " & OEvento_BB
    End If

    Set rs = rdocn.OpenResultset(SQL)
    While Not rs.EOF
        strEvento_Dependencia = strEvento_Dependencia & rs(0) & ", "
        rs.MoveNext
    Wend
    rs.Close
    
    If strEvento_Dependencia <> ", " Then
        
        strEvento_Dependencia = Left(strEvento_Dependencia, Len(strEvento_Dependencia) - 2)
        
        If empresa = 2 Then
            SQL = "SELECT COUNT(*)"
            SQL = SQL & " FROM SEGUROS_DB..sinistro_historico_tb a WITH (NOLOCK) "
            SQL = SQL & "   INNER JOIN SEGUROS_DB..sinistro_bb_tb b WITH (NOLOCK) "
            SQL = SQL & "       ON (a.sinistro_id            = b.sinistro_id"
            SQL = SQL & "       AND a.apolice_id             = b.apolice_id"
            SQL = SQL & "       AND a.seguradora_cod_susep   = b.seguradora_cod_susep"
            SQL = SQL & "       AND a.sucursal_seguradora_id = b.sucursal_seguradora_id"
            SQL = SQL & "       AND a.ramo_id                = b.ramo_id)"
            SQL = SQL & "   INNER JOIN SEGUROS_DB..evento_SEGBR_tb c WITH (NOLOCK) "
            SQL = SQL & "       ON c.evento_SEGBR_id = a.evento_SEGBR_id"
            SQL = SQL & " WHERE sinistro_bb    = " & OSinistro_BB
            SQL = SQL & "   AND c.evento_BB_id in (" & strEvento_Dependencia & ")"
            SQL = SQL & "   AND seq_evento     = "
            SQL = SQL & "                           (SELECT MAX(c.seq_evento)"
            SQL = SQL & "                           FROM SEGUROS_DB..sinistro_historico_tb c WITH (NOLOCK) "
            SQL = SQL & "                               INNER JOIN SEGUROS_DB..evento_SEGBR_tb d WITH (NOLOCK) "
            SQL = SQL & "                                   ON (d.evento_SEGBR_id = c.evento_SEGBR_id"
            SQL = SQL & "                                   AND d.evento_BB_id IS NOT NULL)"
            SQL = SQL & "                           WHERE a.sinistro_id            = C.sinistro_id"
            SQL = SQL & "                             AND a.apolice_id             = c.apolice_id"
            SQL = SQL & "                             AND a.seguradora_cod_susep   = c.seguradora_cod_susep"
            SQL = SQL & "                             AND a.sucursal_seguradora_id = c.sucursal_seguradora_id"
            SQL = SQL & "                             AND a.ramo_id                = c.ramo_id"
            SQL = SQL & "                             AND c.evento_SEGBR_id        <> 10017)"
        Else
            SQL = "SELECT COUNT(*)"
            SQL = SQL & " FROM abss.SEGUROS_DB.dbo.sinistro_historico_tb a WITH (NOLOCK) "
            SQL = SQL & "   INNER JOIN abss.SEGUROS_DB.dbo.sinistro_bb_tb b WITH (NOLOCK) "
            SQL = SQL & "       ON (a.sinistro_id            = b.sinistro_id"
            SQL = SQL & "       AND a.apolice_id             = b.apolice_id"
            SQL = SQL & "       AND a.seguradora_cod_susep   = b.seguradora_cod_susep"
            SQL = SQL & "       AND a.sucursal_seguradora_id = b.sucursal_seguradora_id"
            SQL = SQL & "       AND a.ramo_id                = b.ramo_id)"
            SQL = SQL & "   INNER JOIN abss.SEGUROS_DB.dbo.evento_SEGBR_tb c WITH (NOLOCK) "
            SQL = SQL & "       ON c.evento_SEGBR_id = a.evento_SEGBR_id"
            SQL = SQL & " WHERE sinistro_bb    = " & OSinistro_BB
            SQL = SQL & "   AND c.evento_BB_id in (" & strEvento_Dependencia & ")"
            SQL = SQL & "   AND seq_evento     = "
            SQL = SQL & "                           (SELECT MAX(c.seq_evento)"
            SQL = SQL & "                           FROM abss.SEGUROS_DB.dbo.sinistro_historico_tb c WITH (NOLOCK) "
            SQL = SQL & "                               INNER JOIN abss.SEGUROS_DB.dbo.evento_SEGBR_tb d WITH (NOLOCK) "
            SQL = SQL & "                                   ON (d.evento_SEGBR_id = c.evento_SEGBR_id"
            SQL = SQL & "                                   AND d.evento_BB_id IS NOT NULL)"
            SQL = SQL & "                           WHERE a.sinistro_id            = C.sinistro_id"
            SQL = SQL & "                             AND a.apolice_id             = c.apolice_id"
            SQL = SQL & "                             AND a.seguradora_cod_susep   = c.seguradora_cod_susep"
            SQL = SQL & "                             AND a.sucursal_seguradora_id = c.sucursal_seguradora_id"
            SQL = SQL & "                             AND a.ramo_id                = c.ramo_id"
            SQL = SQL & "                             AND c.evento_SEGBR_id        <> 10017)"
        End If
        
        Set rs = rdocn.OpenResultset(SQL)
        If rs(0) > 0 Then
           Critica_Dependencia = True
        Else
            rs.Close
            If empresa = 2 Then
                SQL = "SELECT COUNT(*)"
                SQL = SQL & " FROM abss." & GTR_banco & ".dbo.entrada_GTR_tb"
                SQL = SQL & " WHERE sinistro_bb = " & OSinistro_BB
                SQL = SQL & "   AND evento_BB   in (" & strEvento_Dependencia & ")"
            Else
                SQL = "SELECT COUNT(*)"
                SQL = SQL & " FROM abss." & GTR_banco & ".dbo.entrada_GTR_tb"
                SQL = SQL & " WHERE sinistro_bb = " & OSinistro_BB
                SQL = SQL & "   AND evento_BB   in (" & strEvento_Dependencia & ")"
            End If

            Set rs = rdocn.OpenResultset(SQL)
            If rs(0) > 0 Then
               Critica_Dependencia = True
            Else
               rs.Close
               If empresa = 2 Then
                    SQL = "SELECT COUNT(*)"
                    SQL = SQL & " FROM abss." & GTR_banco & ".dbo.saida_GTR_tb"
                    SQL = SQL & " WHERE sinistro_bb = " & OSinistro_BB
                    SQL = SQL & "   AND evento_BB   in (" & strEvento_Dependencia & ")"
               Else
                    SQL = "SELECT COUNT(*)"
                    SQL = SQL & " FROM abss." & GTR_banco & ".dbo.saida_GTR_tb"
                    SQL = SQL & " WHERE sinistro_bb = " & OSinistro_BB
                    SQL = SQL & "   AND evento_BB   in (" & strEvento_Dependencia & ")"
               End If

               Set rs = rdocn.OpenResultset(SQL)
               If rs(0) > 0 Then
                  Critica_Dependencia = True
               End If
            End If
        End If
        rs.Close
    Else
        Critica_Dependencia = True
    End If
    
    Exit Function
    
TrataErro:
    Call GravaLogErro("", "Descri��o do Erro: " & Err.Description, "SQL", "CRITICA_DEPENDENCIA", Err.Number)
End Function

Public Sub Enviar_Notificacao(NomeAplicacao As String, Mensagem As String)
        
    Dim SQL As String, rs As rdoResultset
    Dim destinatario As String
    
    On Error GoTo TrataErro
    
    SQL = "select valor from controle_sistema_db..parametro_tb WITH (NOLOCK) where sigla_sistema = 'SMQ' and secao = 'SMQP0005' and campo = 'EMAIL_ERRO' and ambiente_id = " & glAmbiente_id
    Set rs = rdocn.OpenResultset(SQL)
    If Not rs.EOF Then
        destinatario = rs("valor")
    Else
        GoTo TrataErro
    End If
    rs.Close
    
    SQL = "exec seguros_db..envia_email_sp '" & destinatario & "', '" & "Erro no " & NomeAplicacao & "', '" & MudaAspaSimples(Mensagem) & "'"
    rdocn.Execute (SQL)

    Exit Sub
        
TrataErro:
Call GravaLogErro("", "Erro ao tentar enviar notifica��o. Descri��o do Erro: " & Err.Description, "VB", "Enviar_Notificacao", Err.Number)
End Sub

Public Sub GravarErroArquivo(programa As String, glAmbiente_id As Long)
    'criar tabela temporaria
    Dim ArqOrigem As String
    Dim num As Integer
    Dim Diretorio As String
    
    Diretorio = "C:\log_mq\services\Erro\"
    
    'Erro_SMQP0060_20160316.Text
    ArqOrigem = Diretorio & "Erro_SMQP0060" & "_" & Format(Date, "yyyymmdd") & ".text"
    num = FreeFile
    Open ArqOrigem For Append As #num
    Print #num, Now() & " - " & msgErro
    Close #num
End Sub
    

Public Sub GravarTrace(MsgTrace As String)
    'criar tabela temporaria
'    Dim ArqOrigem As String
'    Dim num As Integer
'    Dim Diretorio As String
'
'    Diretorio = "C:\log_mq\services\Trace\"
'
'    'Trace_SMQP0060_20160316.Text
'    ArqOrigem = Diretorio & "Trace_SMQP0060" & "_" & Format(Date, "yyyymmdd") & ".text"
'    num = FreeFile
'    Open ArqOrigem For Append As #num
'    Print #num, Now() & " - " & MsgTrace
'    Close #num
End Sub



