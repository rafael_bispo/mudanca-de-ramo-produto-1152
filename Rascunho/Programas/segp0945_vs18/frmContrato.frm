VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.2#0"; "MSCOMCTL.OCX"
Begin VB.Form frmContrato 
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   3075
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   5340
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3075
   ScaleWidth      =   5340
   StartUpPosition =   2  'CenterScreen
   Begin VB.TextBox txtQtdProcessar 
      Alignment       =   1  'Right Justify
      Height          =   345
      Left            =   3420
      Locked          =   -1  'True
      TabIndex        =   10
      Tag             =   "2"
      Text            =   "0"
      Top             =   1800
      Width           =   1755
   End
   Begin VB.ComboBox cmbTipoExecucao 
      Height          =   315
      ItemData        =   "frmContrato.frx":0000
      Left            =   1440
      List            =   "frmContrato.frx":000A
      Style           =   2  'Dropdown List
      TabIndex        =   9
      Tag             =   "1"
      Top             =   1800
      Width           =   615
   End
   Begin VB.CommandButton cmdAtivar 
      Caption         =   "Processar"
      Height          =   405
      Left            =   2790
      TabIndex        =   7
      Top             =   2220
      Width           =   1245
   End
   Begin VB.Frame fraResultados 
      Caption         =   "  Resultado do Processamento "
      Height          =   1515
      Left            =   90
      TabIndex        =   2
      Top             =   120
      Width           =   5145
      Begin VB.TextBox txtProcessadas 
         Alignment       =   1  'Right Justify
         Height          =   345
         Left            =   2550
         Locked          =   -1  'True
         TabIndex        =   4
         Text            =   "0"
         Top             =   360
         Width           =   1845
      End
      Begin VB.TextBox txtErros 
         Alignment       =   1  'Right Justify
         Height          =   345
         Left            =   2550
         Locked          =   -1  'True
         TabIndex        =   3
         Text            =   "0"
         Top             =   870
         Width           =   1845
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Contrata��es Recebidas:"
         Height          =   195
         Index           =   0
         Left            =   660
         TabIndex        =   6
         Top             =   480
         Width           =   1800
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Contrata��es com erro:"
         Height          =   195
         Index           =   1
         Left            =   660
         TabIndex        =   5
         Top             =   960
         Width           =   1650
      End
   End
   Begin VB.CommandButton cmdSair 
      Caption         =   "Sair"
      Height          =   405
      Left            =   4080
      TabIndex        =   0
      Top             =   2220
      Width           =   1155
   End
   Begin MSComctlLib.StatusBar StbRodape 
      Align           =   2  'Align Bottom
      Height          =   315
      Left            =   0
      TabIndex        =   1
      Top             =   2760
      Width           =   5340
      _ExtentX        =   9419
      _ExtentY        =   556
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   3
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   8997
            MinWidth        =   8997
            Text            =   "Mensagem"
            TextSave        =   "Mensagem"
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   6
            Alignment       =   1
            AutoSize        =   2
            Object.Width           =   1376
            MinWidth        =   88
            Text            =   "Data do sistema"
            TextSave        =   "21/05/20"
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            AutoSize        =   2
            Object.Width           =   1111
            MinWidth        =   88
            Text            =   "Usu�rio"
            TextSave        =   "Usu�rio"
         EndProperty
      EndProperty
   End
   Begin VB.Label Label3 
      Caption         =   "Qtd a Processar:"
      Height          =   255
      Left            =   2160
      TabIndex        =   11
      Top             =   1800
      Width           =   1215
   End
   Begin VB.Label Label2 
      Caption         =   "Tipo Execu��o:"
      Height          =   255
      Left            =   120
      TabIndex        =   8
      Top             =   1800
      Width           =   1215
   End
End
Attribute VB_Name = "frmContrato"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim sUltimoProcessamento As String
Dim lWorkFlowId As Long
Dim sFlagTrataABS As Boolean

Private Sub cmdSair_Click()

    Call FinalizarAplicacao

End Sub

Private Sub cmdAtivar_Click()

    Call Processar

End Sub

Private Sub Form_Load()

On Local Error GoTo Trata_Erro

'Verifica se foi executado pelo Control-M
'CTM = Verifica_Origem_ControlM(Command)

'Pula se for executado pelo control-m
'If CTM = False Then
'  'Verificando permiss�o de acesso � aplica��o'''''''''''''''''''''''''''''''''
'  If Not Trata_Parametros(Command) Then
'     Call FinalizarAplicacao
'  End If
'End If

  'Retirar antes da libera��o '''''''''''''''''''''''''''''''''''''''''''''''''
  cUserName = "11111111111"
  glAmbiente_id = 3

  'Obtendo a data operacional '''''''''''''''''''''''''''''''''''''''''''''''''
  Call ObterDataSistema("SEGBR")

  If glAmbiente_id = 6 Or glAmbiente_id = 7 Then 'luissantos - 11/09/2010 (Quando for ABS, seta vari�vel como True)
     sFlagTrataABS = True
  Else
     sFlagTrataABS = False
  End If

  'Configurando interface''''''''''''''''''''''''''''''''''''''''''''''''''''''
  Call CentraFrm(Me)
  
  'Atualizando Interface ''''''''''''''''''''''''''''''''''''''''''''''''''''''
  Me.Caption = "SEGP0945 - Importa��o de propostas ALS "
  
  ' Iniciando o processo automaticamente caso seja uma chamada do Scheduler '''''''''''''''
'  If Obtem_agenda_diaria_id(Command) > 0 Or CTM = True Then
    
    Me.Show
  '  Call cmdAtivar_Click
  '  FinalizarAplicacao
    
 ' End If
  
  Exit Sub

Trata_Erro:

    Call TratarErro("Form_Load", Me.name)
  
End Sub


Private Sub Processar()
 
Dim oALS As Object
Dim lProcessadas As Long
Dim lErros As Long


Const EVENTO_EMISSAO As Integer = 4700 '9001

    On Error GoTo Trata_Erro
    
    'Tratamento para o Scheduler ''''''''''''''''''''''''''''''
    'InicializaParametrosExecucaoBatch Me
  
    'Atualizando interface ''''''''''''''''''''''''''''''''''''
    MousePointer = vbHourglass
    cmdAtivar.Enabled = False

'    If cmbTipoExecucao.ListIndex = -1 Then
 '       Call MensagemBatch("N�o foi selecionado o tipo de execu��o")
  '      Exit Sub
   ' End If

    Set oALS = CreateObject("SEGL0281.cls00477")
        
'        If CTM = True Then oALS.CTMArquivoLog = CTMArquivoLog 'INC000004745764 - Confitec Sistemas - 18/11/2015 - Philip Rocha - Realizar retorno da informa��o de LOG da DLL.
    
    Call oALS.ProcessarImportacao(App.ProductName, _
                                  App.Title, _
                                  App.FileDescription, _
                                  glAmbiente_id, _
                                  cUserName, _
                                  Data_Sistema, _
                                  CLng(txtQtdProcessar.Text), _
                                  cmbTipoExecucao.Text, _
                                  lProcessadas, _
                                  lErros, _
                                  sFlagTrataABS)

    Set oALS = Nothing
    
    txtErros = lErros
    txtProcessadas = lProcessadas
    
'    goProducao.AdicionaLog 1, lProcessadas, 1
 '   goProducao.AdicionaLog 2, lErros, 1
  '
    'grava log arquivo CTM
   ' If CTM = True Then
    '    Call GravaLogCTM("OK - ", lProcessadas, "", "")
     '   Call GravaLogCTM("OK - ", lErros, "", "")
    'End If
    
    'Finalizando agenda''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    'Pula de for executado pelo Control-M
    'If CTM = False Then
    '    Call goProducao.finaliza
    'End If
                 
    'Atualizando interface ''''''''''''''''''''''''''''''''''''
    'MousePointer = 0
    'cmdAtivar.Enabled = True
    
'    If lErros > 0 Then
'        Call TratarErro("Erro na Contrata��o", Me.name)
'        FinalizarAplicacao
'    End If
    
    Exit Sub

Trata_Erro:

    Call TratarErro("Processar", Me.name)
    Call FinalizarAplicacao

End Sub
