VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form frmNumeroSorte 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "N�mero da Sorte"
   ClientHeight    =   2445
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   3840
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2445
   ScaleWidth      =   3840
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.Frame Frame1 
      Caption         =   "N�mero da Sorte"
      Height          =   2445
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   3855
      Begin VB.CommandButton cmbBotao 
         Caption         =   "Fechar"
         Height          =   405
         Index           =   0
         Left            =   2550
         TabIndex        =   1
         Top             =   1950
         Width           =   1150
      End
      Begin MSFlexGridLib.MSFlexGrid MSFlexGrid1 
         Height          =   1635
         Left            =   120
         TabIndex        =   2
         Top             =   240
         Width           =   3585
         _ExtentX        =   6324
         _ExtentY        =   2884
         _Version        =   393216
         Rows            =   1
         FixedCols       =   0
         SelectionMode   =   1
         FormatString    =   "N� Sorte                    |Valor                        "
      End
   End
End
Attribute VB_Name = "frmNumeroSorte"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public idProposta As Long

Private Sub cmbBotao_Click(Index As Integer)
    Unload Me
End Sub

Private Sub Form_Load()
  MSFlexGrid1.Rows = 1
  MSFlexGrid1.Row = 0
  MSFlexGrid1.ColAlignment(0) = 4
  MSFlexGrid1.ColAlignment(1) = 4
  Dim rsSC As New Recordset
  SQL = "SELECT * FROM proposta_numero_sorte_tb WHERE proposta_id = " & idProposta
  Set rsSC = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, SQL, True)
  Do While Not rsSC.EOF
    MSFlexGrid1.AddItem Format(rsSC!NUMERO_SORTE, "0000000000") & vbTab & Format(rsSC!VALOR_SORTE, "#,###.00") & vbTab
    MSFlexGrid1.Row = MSFlexGrid1.Rows - 1
    MSFlexGrid1.Col = 0: MSFlexGrid1.CellAlignment = 4
    MSFlexGrid1.Col = 1: MSFlexGrid1.CellAlignment = 8
    rsSC.MoveNext
  Loop
  rsSC.Close
  Set rsSC = Nothing
End Sub


