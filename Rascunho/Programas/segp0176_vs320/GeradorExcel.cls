VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsGeradorExcel"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'vitor.cabral - Nova Consultoria - 10/10/2013 - Demanda 17886578 � Registro das avalia��es na recep��o de parcelas
Option Explicit

Private ExcelApp As Excel.Application
Private Formatacao As clsFormatacao

Private diretorio As String
Private ARQUIVO As String
Private extensao As String
Private fileFormatNum As Integer

Private agendInconsist As clsAgendInconsist

Public Property Get NomeDiretorio() As String
    NomeDiretorio = diretorio
End Property

Public Property Let NomeDiretorio(ByVal dir As String)
    diretorio = dir
End Property

Public Property Get NomeArquivo() As String
    NomeArquivo = ARQUIVO
End Property

Public Property Let NomeArquivo(ByVal arq As String)
    ARQUIVO = arq
End Property

Public Property Get TipoExtensao() As String
    TipoExtensao = extensao
End Property

Public Property Let TipoExtensao(ByVal tipo As String)
    extensao = tipo
End Property


Private Sub Class_Initialize()
    Set ExcelApp = CreateObject("Excel.Application")
    ExcelApp.Workbooks.Add
    
    Set agendInconsist = New clsAgendInconsist
    Set Formatacao = New clsFormatacao
    
End Sub

Private Sub Config()
    Call Formatacao.Config(ExcelApp)

    Call agendInconsist.Config(ExcelApp, Formatacao)
End Sub

Public Sub Preencher(ByVal gridAgendamentos As MSFlexGrid)
                
    Call Config
    Call agendInconsist.Preencher(gridAgendamentos)
    Call AcertarPlanilhas
    Call Formatacao.FormatarFonte
    Call SalvarArquivo
    Call AbrirExcel
    
End Sub

Private Sub Class_Terminate()

    Set agendInconsist = Nothing
    
End Sub

Private Sub AbrirExcel()
    ExcelApp.Visible = True
    Set ExcelApp = Nothing
End Sub

Private Sub SalvarArquivo()
        
    On Error GoTo TratarErro
        
    If Val(ExcelApp.ActiveWorkbook.Application.Version) < 12 Then
        extensao = ".xls": fileFormatNum = -4143
    Else
        extensao = ".xlsx": fileFormatNum = 51
    End If
    
    NomeArquivo = diretorio & "\" & GerarNomeArquivo()
    If (InStr(NomeArquivo, "\\") > 0) Then
        NomeArquivo = Replace(NomeArquivo, "\\", "\")
    End If
    
    Call ExcelApp.ActiveWorkbook.SaveAs( _
                FileName:=NomeArquivo, _
                FileFormat:=fileFormatNum, _
                Password:="", _
                WriteResPassword:="", _
                ReadOnlyRecommended:=False, _
                CreateBackup:=False)
                
    Call ExcelApp.ActiveWorkbook.Close
                
TratarErro:
    If Err.Number <> 0 Then
        MsgBox "Erro ao exportar para Excel!"
    End If
    
    Exit Sub

End Sub

Private Function GerarNomeArquivo() As String
    
    If NomeArquivo = "" Then
        GerarNomeArquivo = "planilha_agend_inconsist_"
        GerarNomeArquivo = GerarNomeArquivo & "_" & Format(Date, "dd_mm_yyyy")
        GerarNomeArquivo = GerarNomeArquivo & "_" & Format(Time, "hh_mm_ss")
        GerarNomeArquivo = GerarNomeArquivo & extensao
    Else
        GerarNomeArquivo = NomeArquivo & extensao
    End If
    

End Function

Private Sub AcertarPlanilhas()

    With ExcelApp

        .ErrorCheckingOptions.InconsistentFormula = False
        .ErrorCheckingOptions.OmittedCells = False

        '.Sheets(NomePlanilhaCalculo).Move Before:=ExcelApp.Sheets(NomePlanilhaAtualizacao)

        .Sheets("Plan1").Delete
        .Sheets("Plan2").Delete
        .Sheets("Plan3").Delete

    End With

End Sub

Function ExportarFlex(objFlex As MSFlexGrid, sLocal As String, Titulo As String, objForm As Form)
   
    Dim Fs As Object
    Dim arq  As Object
    
    Dim x, y, j As Integer
    Dim cConteudo As String
    Dim cCss As String
    Dim cCabecalho As String
    Dim cDetalhe As String
    Dim cDetalheRes As String
    Dim cRodape As String
    Dim iContador As Double
    
    Dim pBoldA, pBoldF As String
    Dim colsPan As Integer
    
    Dim vColunas As String
    Dim vColunasMesclar As String
    
    On Local Error GoTo Trata_Erro
    
    objForm.dlgGrid.CancelError = False
    objForm.dlgGrid.Filter = "Arquivos do Excel|*.xls"
    objForm.dlgGrid.ShowSave
    
    If objForm.dlgGrid.FileName = "" Then
        Exit Function
    End If
    
    Set Fs = CreateObject("Scripting.FileSystemObject")
    Set arq = Fs.CreateTextFile(objForm.dlgGrid.FileName, True)
    
    'CSS do Formato para Exportar para o Excel
    cCss = ""
    cCss = cCss & "<style type=""text/css"">"
    cCss = cCss & "<!--"
    cCss = cCss & ".cabecalho {"
    cCss = cCss & "     color: #000000;"
    cCss = cCss & "     font-weight: bold;"
    cCss = cCss & "     background-color: #CCCCCC;"
    cCss = cCss & "     font-family: Verdana, Arial, Helvetica, sans-serif;"
    cCss = cCss & "     font-size: 12px;"
    cCss = cCss & "}"
    cCss = cCss & ".titulo {"
    cCss = cCss & "    font-family: Verdana, Arial, Helvetica, sans-serif;"
    cCss = cCss & "    font-size: 14px;"
    cCss = cCss & "    font-weight: bold;"
    cCss = cCss & "    text-align: center;"
    cCss = cCss & "    border-top-style: none;"
    cCss = cCss & "    border-right-style: none;"
    cCss = cCss & "    border-bottom-style: none;"
    cCss = cCss & "    border-left-style: none;"
    cCss = cCss & "}"
    cCss = cCss & ".rodape {"
    cCss = cCss & "    font-family: Verdana, Arial, Helvetica, sans-serif;"
    cCss = cCss & "    font-size: 11px;"
    cCss = cCss & "    font-weight: bold;"
    cCss = cCss & "    text-align: right;"
    cCss = cCss & "}"
    cCss = cCss & ".coluna {"
    cCss = cCss & "     mso-number-format:\@;"
    cCss = cCss & "}"
    cCss = cCss & "-->"
    cCss = cCss & "</style>"
    
    
    'Linhas do Cabecalho
    
    With objFlex
    
        cCabecalho = ""
    
        For x = 0 To .FixedRows - 1
            cCabecalho = cCabecalho & "<tr>"
            For y = 0 To .Cols - 1
                colsPan = 0
                    For j = y To .Cols - 1
                        If .Cols - 1 > j Then
                            If .TextMatrix(x, j) = .TextMatrix(x, j + 1) Then
                                colsPan = colsPan + 1
                            Else
                                y = y + colsPan
                                Exit For
                            End If
                        ElseIf colsPan <> 0 Then
                                y = y + colsPan
                                Exit For
                        End If
    
                    Next
                    
                    If .ColWidth(y) > 0 Then
                    cCabecalho = cCabecalho & "<td colspan=" & colsPan + 1 & " class=""cabecalho"" width=" & .ColWidth(y) & ">" & IIf(.ColWidth(y) > 0, .TextMatrix(x, y), "") & "</td>"
                    End If
            Next
    
            cCabecalho = cCabecalho & "</tr>"
        Next
    
        cDetalhe = ""
        iContador = 0
        Screen.MousePointer = vbHourglass
    
        DoEvents
    
        For x = .FixedRows To .Rows - 1
            cDetalhe = cDetalhe & "<tr>"
    
            For y = 0 To .Cols - 1
                    .Col = y
                    .Row = x
    
                    If .CellFontBold = True Then
                        pBoldA = "<b>"
                        pBoldF = "<b>"
                    Else
                        pBoldA = ""
                        pBoldF = ""
                    End If
    
    
                   
                    If .ColWidth(y) > 0 Then
                    cDetalhe = cDetalhe & "<td class=""coluna""><font face=""" & .CellFontName & """>" & IIf(.ColWidth(y) > 0, pBoldA & "" & .TextMatrix(x, y) & "" & pBoldF, "") & "</td>"
                    End If
            Next
    
            cDetalhe = cDetalhe & "</tr>"
    
    
            iContador = iContador + 1
            If iContador > 200 Then
                iContador = 0
                cDetalheRes = cDetalheRes & cDetalhe
                cDetalhe = ""
              
            End If
    
        Next
    
    End With
    
    cRodape = "<tr><td>&nbsp</td></tr><tr><td class=rodape>" & Now() & "</td></tr>"
    
    
    DoEvents
    
    'Substituindo os valores
    cConteudo = "#css##table1##table2##cabecalho##detalhe##fechatable2##rodape##fechatable1#"
    
    cConteudo = Replace(cConteudo, "#css#", cCss)
    cConteudo = Replace(cConteudo, "#table1#", "<table border=""0"" cellpadding=""0"" cellspacing=""0""><tr><td class=""titulo"">&nbsp;</td></tr><tr><td class=""titulo"">" & Titulo & "</td></tr><tr><td>&nbsp;</td></tr>")
    cConteudo = Replace(cConteudo, "#table2#", "<tr><td><table width=""100%"" border=""1"" cellpadding=""0"" cellspacing=""1"" bordercolor=""#000000"">")
    cConteudo = Replace(cConteudo, "#cabecalho#", cCabecalho)
    cConteudo = Replace(cConteudo, "#detalhe#", cDetalheRes & cDetalhe)
    cConteudo = Replace(cConteudo, "#fechatable1#", "</table></td></tr>")
    cConteudo = Replace(cConteudo, "#rodape#", cRodape)
    cConteudo = Replace(cConteudo, "#fechatable2#", "</table>")
    
    arq.writeline cConteudo
    arq.Close
    
    Screen.MousePointer = vbDefault
    'Screen.ActiveForm.StbRodape.Panels(1).Text = "Conclu�do."
    
    
    MsgBox "Arquivo gerado com sucesso!", vbInformation
    
    
    Set arq = Nothing
    Set Fs = Nothing
    
    Exit Function
    
Trata_Erro:
    If Err.Number = 32755 Then
       Err.Clear
    Else
       Call TrataErroGeral("ExportarFlex", "Local.bas")
       Call FinalizarAplicacao
    End If
End Function

