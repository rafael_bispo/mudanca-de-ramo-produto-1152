VERSION 5.00
Object = "{C932BA88-4374-101B-A56C-00AA003668DC}#1.1#0"; "MSMASK32.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form frmConsultaEndosso 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Consulta Endosso"
   ClientHeight    =   11370
   ClientLeft      =   1920
   ClientTop       =   555
   ClientWidth     =   13530
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   11370
   ScaleWidth      =   13530
   StartUpPosition =   2  'CenterScreen
   Visible         =   0   'False
   Begin VB.Frame FraDadosSegurados 
      Caption         =   "Dados Segurados"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2000
      Left            =   150
      TabIndex        =   84
      Top             =   8850
      Visible         =   0   'False
      Width           =   13275
      Begin MSFlexGridLib.MSFlexGrid grdDadosSegurado 
         Height          =   1665
         Left            =   100
         TabIndex        =   85
         Top             =   250
         Width           =   13095
         _ExtentX        =   23098
         _ExtentY        =   2937
         _Version        =   393216
         Cols            =   8
         FixedCols       =   0
         AllowUserResizing=   1
         FormatString    =   $"frmCosultaEndosso.frx":0000
      End
   End
   Begin VB.Frame fraSeguro 
      Caption         =   "Dados do Seguro"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3255
      Left            =   5280
      TabIndex        =   3
      Top             =   3720
      Visible         =   0   'False
      Width           =   8115
      Begin VB.TextBox txtCredDebito 
         Height          =   315
         Left            =   6240
         Locked          =   -1  'True
         TabIndex        =   90
         Top             =   2670
         Width           =   1815
      End
      Begin VB.TextBox txtSegundaParcela 
         Height          =   315
         Left            =   6240
         Locked          =   -1  'True
         TabIndex        =   88
         Top             =   1860
         Width           =   1815
      End
      Begin VB.TextBox txtDemaisParcelas 
         Height          =   315
         Left            =   6240
         Locked          =   -1  'True
         TabIndex        =   87
         Top             =   1455
         Width           =   1815
      End
      Begin VB.TextBox txtPrazoVigencia 
         Height          =   315
         Left            =   6240
         Locked          =   -1  'True
         TabIndex        =   82
         Top             =   240
         Width           =   1815
      End
      Begin VB.TextBox txtFimVigencia 
         Height          =   315
         Left            =   2160
         Locked          =   -1  'True
         TabIndex        =   80
         Top             =   2670
         Width           =   1815
      End
      Begin VB.TextBox txtIniVigencia 
         Height          =   315
         Left            =   2160
         Locked          =   -1  'True
         TabIndex        =   78
         Top             =   2265
         Width           =   1815
      End
      Begin VB.TextBox txtTaxa 
         Height          =   315
         Left            =   2160
         Locked          =   -1  'True
         TabIndex        =   76
         Top             =   1455
         Width           =   1815
      End
      Begin VB.TextBox txtvalorPremio 
         Height          =   315
         Left            =   2160
         Locked          =   -1  'True
         TabIndex        =   74
         Top             =   1050
         Width           =   1815
      End
      Begin VB.TextBox txtCapitalSegurado 
         Height          =   315
         Left            =   2160
         Locked          =   -1  'True
         TabIndex        =   62
         Top             =   240
         Width           =   1815
      End
      Begin VB.TextBox txtValComissao 
         Height          =   315
         Left            =   2160
         Locked          =   -1  'True
         TabIndex        =   60
         Top             =   1860
         Width           =   1815
      End
      Begin VB.TextBox txtValParidade 
         Height          =   315
         Left            =   6240
         Locked          =   -1  'True
         TabIndex        =   58
         Top             =   2265
         Width           =   1815
      End
      Begin VB.TextBox txtNumParcelas 
         Height          =   315
         Left            =   6240
         Locked          =   -1  'True
         TabIndex        =   56
         Top             =   1050
         Width           =   1815
      End
      Begin VB.TextBox txtQtdVidas 
         Height          =   315
         Left            =   6240
         Locked          =   -1  'True
         TabIndex        =   54
         Top             =   645
         Width           =   1815
      End
      Begin VB.TextBox txtValIof 
         Height          =   315
         Left            =   2160
         Locked          =   -1  'True
         TabIndex        =   52
         Top             =   645
         Width           =   1815
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Cr�dito / D�bito"
         Height          =   195
         Index           =   15
         Left            =   4080
         TabIndex        =   91
         Top             =   2670
         Width           =   1125
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "2� Parcela:"
         Height          =   195
         Index           =   8
         Left            =   4080
         TabIndex        =   89
         Top             =   1860
         Width           =   780
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Demais Parcelas:"
         Height          =   195
         Index           =   7
         Left            =   4080
         TabIndex        =   86
         Top             =   2265
         Width           =   1230
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Prazo de Vig�ncia (em dias):"
         Height          =   195
         Index           =   11
         Left            =   4080
         TabIndex        =   83
         Top             =   300
         Width           =   2010
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Fim de Vig�ncia :"
         Height          =   195
         Index           =   10
         Left            =   120
         TabIndex        =   81
         Top             =   2670
         Width           =   1215
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "In�cio de Vig�ncia :"
         Height          =   195
         Index           =   9
         Left            =   120
         TabIndex        =   79
         Top             =   2265
         Width           =   1380
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Taxa Coml. Cobertura:"
         Height          =   195
         Index           =   6
         Left            =   120
         TabIndex        =   77
         Top             =   1455
         Width           =   1575
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Vlr. Pr�mio Coml. (Total):"
         Height          =   195
         Index           =   3
         Left            =   120
         TabIndex        =   75
         Top             =   1050
         Width           =   1725
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Capital Segurado :"
         Height          =   195
         Index           =   0
         Left            =   120
         TabIndex        =   63
         Top             =   300
         Width           =   1305
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Comiss�o :"
         Height          =   195
         Index           =   4
         Left            =   120
         TabIndex        =   61
         Top             =   1860
         Width           =   765
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Pr�xima Parcela:"
         Height          =   195
         Index           =   13
         Left            =   4080
         TabIndex        =   59
         Top             =   1455
         Width           =   1185
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "N�mero de parcelas :"
         Height          =   195
         Index           =   12
         Left            =   4080
         TabIndex        =   57
         Top             =   1050
         Width           =   1515
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Qtd. Vidas :"
         Height          =   195
         Index           =   14
         Left            =   4080
         TabIndex        =   55
         Top             =   705
         Width           =   825
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "IOF :"
         Height          =   195
         Index           =   1
         Left            =   120
         TabIndex        =   53
         Top             =   705
         Width           =   345
      End
   End
   Begin VB.CommandButton cmdFechar 
      Caption         =   "Fechar"
      Height          =   405
      Left            =   11600
      TabIndex        =   0
      Top             =   10900
      Width           =   1785
   End
   Begin VB.Frame fraEndosso 
      Caption         =   "Dados do Endosso "
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3525
      Left            =   150
      TabIndex        =   1
      Top             =   90
      Width           =   13275
      Begin VB.TextBox txttpAlteracaoEndosso 
         Height          =   315
         Left            =   9100
         Locked          =   -1  'True
         TabIndex        =   93
         Top             =   1620
         Width           =   4000
      End
      Begin VB.TextBox txtUsuarioEmi 
         Height          =   315
         Left            =   5490
         Locked          =   -1  'True
         TabIndex        =   51
         Top             =   450
         Width           =   2835
      End
      Begin VB.TextBox mskDtRetornoBB 
         Height          =   315
         Left            =   4470
         TabIndex        =   49
         Top             =   1620
         Width           =   1215
      End
      Begin VB.TextBox mskDtFimVigencia 
         Height          =   315
         Left            =   2370
         TabIndex        =   48
         Top             =   450
         Width           =   1215
      End
      Begin VB.TextBox mskDtEmissao 
         Height          =   315
         Left            =   3960
         TabIndex        =   47
         Top             =   450
         Width           =   1215
      End
      Begin VB.TextBox txtEndossoId 
         Height          =   315
         Left            =   150
         Locked          =   -1  'True
         TabIndex        =   45
         Top             =   1620
         Width           =   1875
      End
      Begin VB.TextBox txtDescEndosso 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1065
         Left            =   150
         Locked          =   -1  'True
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   7
         Top             =   2220
         Width           =   10155
      End
      Begin VB.TextBox txtUsuario 
         Height          =   315
         Left            =   6060
         Locked          =   -1  'True
         TabIndex        =   6
         Top             =   1620
         Width           =   2895
      End
      Begin VB.TextBox txtNumEndossoBB 
         Height          =   315
         Left            =   2280
         Locked          =   -1  'True
         TabIndex        =   5
         Top             =   1620
         Width           =   1875
      End
      Begin VB.TextBox txttpEndosso 
         Height          =   315
         Left            =   150
         Locked          =   -1  'True
         TabIndex        =   2
         Top             =   1020
         Width           =   10155
      End
      Begin MSMask.MaskEdBox mskDtEndosso 
         Height          =   315
         Left            =   150
         TabIndex        =   8
         Top             =   450
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   556
         _Version        =   393216
         AutoTab         =   -1  'True
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         PromptChar      =   "_"
      End
      Begin VB.Label lbltpAlteracaoEndosso 
         AutoSize        =   -1  'True
         Caption         =   "Tipo de Altera��o:"
         Height          =   195
         Left            =   9105
         TabIndex        =   92
         Top             =   1410
         Width           =   1305
      End
      Begin VB.Label lbl 
         Caption         =   "Usu�rio Emiss�o:"
         Height          =   285
         Index           =   5
         Left            =   5505
         TabIndex        =   50
         Top             =   225
         Width           =   1545
      End
      Begin VB.Label lbl 
         AutoSize        =   -1  'True
         Caption         =   "N�mero Endosso AB:"
         Height          =   195
         Index           =   4
         Left            =   150
         TabIndex        =   46
         Top             =   1410
         Width           =   1515
      End
      Begin VB.Label lbl 
         AutoSize        =   -1  'True
         Caption         =   "Texto do Endosso:"
         Height          =   195
         Index           =   80
         Left            =   150
         TabIndex        =   16
         Top             =   2010
         Width           =   1380
      End
      Begin VB.Label lbl 
         AutoSize        =   -1  'True
         Caption         =   "Usu�rio:"
         Height          =   195
         Index           =   3
         Left            =   6060
         TabIndex        =   15
         Top             =   1410
         Width           =   585
      End
      Begin VB.Label lbl 
         AutoSize        =   -1  'True
         Caption         =   "Data Retorno BB:"
         Height          =   195
         Index           =   2
         Left            =   4470
         TabIndex        =   14
         Top             =   1410
         Width           =   1260
      End
      Begin VB.Label lbl 
         AutoSize        =   -1  'True
         Caption         =   "N�mero Endosso BB:"
         Height          =   195
         Index           =   1
         Left            =   2280
         TabIndex        =   13
         Top             =   1410
         Width           =   1515
      End
      Begin VB.Label lbltpEndosso 
         AutoSize        =   -1  'True
         Caption         =   "Tipo de Endosso:"
         Height          =   195
         Left            =   150
         TabIndex        =   12
         Top             =   810
         Width           =   1245
      End
      Begin VB.Label lbl 
         AutoSize        =   -1  'True
         Caption         =   "In�cio de Vig�ncia / Pedido:"
         Height          =   195
         Index           =   75
         Left            =   150
         TabIndex        =   11
         Top             =   210
         Width           =   1995
      End
      Begin VB.Label lbl 
         AutoSize        =   -1  'True
         Caption         =   "Emiss�o:"
         Height          =   195
         Index           =   86
         Left            =   3960
         TabIndex        =   10
         Top             =   210
         Width           =   630
      End
      Begin VB.Label lblFimVig 
         AutoSize        =   -1  'True
         Caption         =   "Fim de Vig�ncia:"
         Height          =   195
         Left            =   2370
         TabIndex        =   9
         Top             =   210
         Width           =   1170
      End
   End
   Begin VB.Frame fraDadosCadastrais 
      Caption         =   "Dados Cadastrais "
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1800
      Left            =   150
      TabIndex        =   4
      Top             =   7050
      Visible         =   0   'False
      Width           =   13275
      Begin VB.TextBox TxtEmail 
         Height          =   285
         Left            =   5790
         Locked          =   -1  'True
         MaxLength       =   60
         TabIndex        =   29
         Top             =   270
         Width           =   3105
      End
      Begin VB.TextBox TxtBairro 
         Height          =   285
         Left            =   9990
         Locked          =   -1  'True
         MaxLength       =   50
         TabIndex        =   28
         Top             =   630
         Width           =   3105
      End
      Begin VB.TextBox txtIdTpPessoa 
         Height          =   285
         Left            =   1380
         Locked          =   -1  'True
         MaxLength       =   3
         TabIndex        =   27
         Top             =   990
         Visible         =   0   'False
         Width           =   315
      End
      Begin VB.TextBox txtNome 
         Height          =   285
         Left            =   1380
         Locked          =   -1  'True
         MaxLength       =   60
         TabIndex        =   26
         Top             =   630
         Width           =   3105
      End
      Begin VB.TextBox TxtEndereco 
         Height          =   285
         Left            =   5790
         Locked          =   -1  'True
         MaxLength       =   50
         TabIndex        =   25
         Top             =   1350
         Width           =   3105
      End
      Begin VB.TextBox TxtCidade 
         Height          =   285
         Left            =   9990
         Locked          =   -1  'True
         MaxLength       =   30
         TabIndex        =   24
         Top             =   270
         Width           =   3105
      End
      Begin VB.TextBox TxtUF 
         Height          =   285
         Left            =   9990
         Locked          =   -1  'True
         MaxLength       =   2
         TabIndex        =   23
         Top             =   960
         Width           =   375
      End
      Begin VB.TextBox TxtCEP 
         Height          =   285
         Left            =   9990
         Locked          =   -1  'True
         MaxLength       =   10
         TabIndex        =   22
         Top             =   1320
         Width           =   1905
      End
      Begin VB.TextBox TxtDDD 
         Height          =   285
         Left            =   5790
         Locked          =   -1  'True
         MaxLength       =   4
         TabIndex        =   21
         Top             =   630
         Width           =   615
      End
      Begin VB.TextBox TxtTelefone 
         Height          =   285
         Left            =   5790
         Locked          =   -1  'True
         MaxLength       =   9
         TabIndex        =   20
         Top             =   990
         Width           =   1215
      End
      Begin VB.TextBox txtClienteId 
         BackColor       =   &H80000003&
         Enabled         =   0   'False
         Height          =   285
         Left            =   1380
         Locked          =   -1  'True
         TabIndex        =   19
         Top             =   270
         Width           =   1335
      End
      Begin VB.TextBox txtCPFCNPJ 
         Height          =   285
         Left            =   1380
         Locked          =   -1  'True
         TabIndex        =   18
         Top             =   1350
         Width           =   3105
      End
      Begin VB.TextBox txtTpPessoa 
         BackColor       =   &H80000004&
         Height          =   285
         Left            =   1380
         Locked          =   -1  'True
         TabIndex        =   17
         Top             =   990
         Width           =   1335
      End
      Begin VB.Label lblCad 
         AutoSize        =   -1  'True
         Caption         =   "E-mail :"
         Height          =   195
         Index           =   26
         Left            =   4950
         TabIndex        =   41
         Top             =   330
         Width           =   510
      End
      Begin VB.Label lblCad 
         AutoSize        =   -1  'True
         Caption         =   "Bairro :"
         Height          =   195
         Index           =   3
         Left            =   9270
         TabIndex        =   40
         Top             =   660
         Width           =   495
      End
      Begin VB.Label lblCad 
         AutoSize        =   -1  'True
         Caption         =   "Nome :"
         Height          =   195
         Index           =   1
         Left            =   270
         TabIndex        =   39
         Top             =   660
         Width           =   510
      End
      Begin VB.Label lblCad 
         AutoSize        =   -1  'True
         Caption         =   "CPF / CNPJ :"
         Height          =   195
         Index           =   27
         Left            =   270
         TabIndex        =   38
         Top             =   1320
         Width           =   960
      End
      Begin VB.Label lblCad 
         AutoSize        =   -1  'True
         Caption         =   "Endere�o :"
         Height          =   195
         Index           =   35
         Left            =   4950
         TabIndex        =   37
         Top             =   1320
         Width           =   780
      End
      Begin VB.Label lblCad 
         AutoSize        =   -1  'True
         Caption         =   "Cidade :"
         Height          =   195
         Index           =   4
         Left            =   9270
         TabIndex        =   36
         Top             =   330
         Width           =   585
      End
      Begin VB.Label lblCad 
         AutoSize        =   -1  'True
         Caption         =   "UF :"
         Height          =   195
         Index           =   7
         Left            =   9270
         TabIndex        =   35
         Top             =   990
         Width           =   300
      End
      Begin VB.Label lblCad 
         AutoSize        =   -1  'True
         Caption         =   "CEP :"
         Height          =   195
         Index           =   8
         Left            =   9270
         TabIndex        =   34
         Top             =   1320
         Width           =   405
      End
      Begin VB.Label lblCad 
         AutoSize        =   -1  'True
         Caption         =   "DDD :"
         Height          =   195
         Index           =   5
         Left            =   4950
         TabIndex        =   33
         Top             =   660
         Width           =   450
      End
      Begin VB.Label lblCad 
         AutoSize        =   -1  'True
         Caption         =   "Telefone :"
         Height          =   195
         Index           =   6
         Left            =   4950
         TabIndex        =   32
         Top             =   990
         Width           =   720
      End
      Begin VB.Label lblCad 
         AutoSize        =   -1  'True
         Caption         =   "Tipo Pessoa :"
         Height          =   195
         Index           =   2
         Left            =   270
         TabIndex        =   31
         Top             =   990
         Width           =   975
      End
      Begin VB.Label lblCad 
         AutoSize        =   -1  'True
         Caption         =   "C�d. Cliente :"
         Height          =   195
         Index           =   0
         Left            =   270
         TabIndex        =   30
         Top             =   330
         Width           =   945
      End
   End
   Begin VB.Frame fraFinanceiro 
      Caption         =   "Dados financeiros "
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3255
      Left            =   150
      TabIndex        =   42
      Tag             =   "2"
      Top             =   3720
      Visible         =   0   'False
      Width           =   4995
      Begin VB.TextBox txtTpPagamento 
         Height          =   315
         Left            =   1740
         Locked          =   -1  'True
         TabIndex        =   72
         Top             =   240
         Width           =   1815
      End
      Begin VB.TextBox TxtCC 
         Height          =   285
         Left            =   1740
         Locked          =   -1  'True
         MaxLength       =   11
         TabIndex        =   70
         Top             =   1860
         Width           =   1575
      End
      Begin VB.TextBox TxtCodAg 
         Height          =   285
         Left            =   1740
         Locked          =   -1  'True
         MaxLength       =   5
         TabIndex        =   68
         Top             =   1455
         Width           =   615
      End
      Begin VB.TextBox TxtNomeAg 
         BackColor       =   &H80000004&
         Enabled         =   0   'False
         Height          =   285
         Left            =   2430
         Locked          =   -1  'True
         TabIndex        =   67
         Top             =   1455
         Width           =   2175
      End
      Begin VB.TextBox TxtBanco 
         Height          =   285
         Left            =   1740
         Locked          =   -1  'True
         MaxLength       =   3
         TabIndex        =   65
         Top             =   1050
         Width           =   615
      End
      Begin VB.TextBox TxtNomeBanco 
         BackColor       =   &H80000004&
         Enabled         =   0   'False
         Height          =   285
         Left            =   2430
         Locked          =   -1  'True
         TabIndex        =   64
         Top             =   1050
         Width           =   2055
      End
      Begin VB.TextBox txtFormaPgto 
         Height          =   315
         Left            =   1740
         Locked          =   -1  'True
         TabIndex        =   43
         Top             =   645
         Width           =   1815
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Tipo de Pagamento :"
         Height          =   195
         Index           =   2
         Left            =   180
         TabIndex        =   73
         Top             =   300
         Width           =   1485
      End
      Begin VB.Label lblCad 
         AutoSize        =   -1  'True
         Caption         =   "Conta :"
         Height          =   195
         Index           =   12
         Left            =   180
         TabIndex        =   71
         Top             =   1860
         Width           =   510
      End
      Begin VB.Label lblCad 
         AutoSize        =   -1  'True
         Caption         =   "Ag. D�bito :"
         Height          =   195
         Index           =   11
         Left            =   180
         TabIndex        =   69
         Top             =   1455
         Width           =   840
      End
      Begin VB.Label lblCad 
         AutoSize        =   -1  'True
         Caption         =   "Banco :"
         Height          =   195
         Index           =   10
         Left            =   180
         TabIndex        =   66
         Top             =   1050
         Width           =   555
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Forma Pagamento :"
         Height          =   195
         Index           =   5
         Left            =   180
         TabIndex        =   44
         Top             =   705
         Width           =   1380
      End
   End
End
Attribute VB_Name = "frmConsultaEndosso"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'--------------------------------------------------------------------------------------------------------------------
'data da Cria��o: 14/07/2014
'Autor.........: Carolina Santos
'Empresa........: JM Confitec - RJ
'Demanda........: FLOW 17919477
'Cria�ao da tela Consulta Endosso baseada na tela frmEndosso para implementa��o dos novos tipos de endosso
'que dever�o apresentar um metodo especifico de visualiza��o
'--------------------------------------------------------------------------------------------------------------------

Option Explicit

Private Const TP_ENDOSSO_CONTRATACAO = 0
Private Const TP_ENDOSSO_ENDOSSO_CADASTRAL_DE_VIDA = 1
Private Const TP_ENDOSSO_ENDOSSO_BENEFICIARIO_VIDA = 2
Private Const TP_ENDOSSO_ADICIONAL = 10
Private Const TP_ENDOSSO_PARTICIPACAO_DO_CONGENERE = 11
Private Const TP_ENDOSSO_SEM_MOVIMENTACAO_FINANCEIRA = 12
Private Const TP_ENDOSSO_ALTERACAO_CADASTRAL = 51
Private Const TP_ENDOSSO_ALTERACAO_DE_PLANO = 52
Private Const TP_ENDOSSO_EXCLUSAO_DE_BENEFICIARIO = 53
Private Const TP_ENDOSSO_INCLUSAO_DE_BENEFICIARIO = 57
Private Const TP_ENDOSSO_NAO_DEFINIDO = 58
Private Const TP_ENDOSSO_ENDOSSO_DE_DADOS_DO_BEM = 59
Private Const TP_ENDOSSO_REINTEGRACAO_DE_IS = 61
Private Const TP_ENDOSSO_CANCELAMENTO_A_PEDIDO_DO_SEGURADO = 63
Private Const TP_ENDOSSO_CANCELAMENTO_A_PEDIDO_DA_SEGURADORA = 64
Private Const TP_ENDOSSO_LIQUIDA��O_DE_CPR = 67
Private Const TP_ENDOSSO_CANCELAMENTO_SINISTRO = 68
Private Const TP_ENDOSSO_ENDOSSO_DE_COBERTURAS = 70
Private Const TP_ENDOSSO_ALTERA��O_DA_DATA_DA_VIAGEM = 74
Private Const TP_ENDOSSO_RESTITUI��O_POR_INICIATIVA_DA_SEGURADORA = 80
Private Const TP_ENDOSSO_CANCELAMENTO_POR_INADIMPLENCIA = 90
Private Const TP_ENDOSSO_CANCELAMENTO_POR_ESGOTAMENTO_DE_IS = 91
Private Const TP_ENDOSSO_CANCELAMENTO_POR_INADIMPLENCIA_PRIMEIRA_PARCELA = 92
Private Const TP_ENDOSSO_ACERTO_CONTA_MENSAL = 93
Private Const TP_ENDOSSO_ENDOSSO_DE_REDU��O_DE_IS_POR_SINISTRO = 94
Private Const TP_ENDOSSO_ENDOSSO_GENERICO_ONLINE = 99
Private Const TP_ENDOSSO_ENDOSSO_DE_FATURA = 100
Private Const TP_ENDOSSO_CANCELAMENTO_DE_ENDOSSO = 101
Private Const TP_ENDOSSO_ENDOSSO_DE_LIQUIDACAO = 104
Private Const TP_ENDOSSO_ENDOSSO_COM_MOVIMENTA��O_DE_PREMIO = 200
Private Const TP_ENDOSSO_ENDOSSO_DE_CANCELAMENTO_DE_PARCELA = 201
Private Const TP_ENDOSSO_SUSPENSAO_DE_APOLICE = 32
Private Const TP_ENDOSSO_CANCELAMENTO_SUSPENSAO_DE_APOLICE = 33
Private Const TP_ENDOSSO_ENDOSSO_DE_REENQUADRAMENTO = 202
Private Const TP_ENDOSSO_ENDOSSO_DE_REAJUSTE = 203
Private Const TP_ENDOSSO_ALTERACAO_DE_DADOS_DE_CARTAO_DE_CREDITO = 274
Private Const TP_ENDOSSO_DE_REPACTUACAO = 336
Private Const TP_ENDOSSO_EXCLCUSAO_DE_SEGURADO = 337
Private Const TP_ENDOSSO_EXCLCUSAO_DE_SEGURADO_POR_NEGATIVA_DE_INDENIZACAO_MORTE = 338
Private Const TP_ENDOSSO_CANCELAMENTO_POR_INELEGIBILIDADE = 340
Private Const TP_ENDOSSO_REPACTUACAO_DA_OPERACAO_DE_CREDITO_PF = 359 'R.FOUREAUX - 14/08/2017

Private Const TP_ENDOSSO_SINISTRO_DE_CONJUGE = 31
Private Const TP_ENDOSSO_ALTERACAO_DE_PLANO_NOVOS = 37
Private Const TP_ENDOSSO_ALTERACAO_DE_FORMA_DE_PAGAMENTO = 252
Private Const TP_ENDOSSO_ALTERACAO_DE_CAPITAL_SEGURADO = 276
Private Const TP_ENDOSSO_AJUSTES_ANUAIS = 308
Private Const TP_ENDOSSO_ALTERACAO_DE_PERIODICIDADE_DE_PAGAMENTO = 348
Private Const TP_ENDOSSO_EXCLUSAO_DE_COBERTURA_POR_SINISTRO = 349
Private Const TP_ENDOSSO_MOV_FINANCEIRA_GERAL = 999


Private Const FRAME_ENDOSSO_CONTRATACAO = 0
Private Const FRAME_ENDOSSO_EXCLUSAO = 1
Private Const FRAME_ENDOSSO_ALTERACAO = 2
Private Const FRAME_ENDOSSO_CANCELAMENTO_ENDOSSO = 3
Private Const FRAME_ENDOSSO_CANCELAMENTO_PROPOSTA = 4
Private Const FRAME_ENDOSSO_REAJUSTE = 5
Private Const FRAME_ENDOSSO_GERAL = 6
Private Const FRAME_ENDOSSO_SUSPENSAO = 7
Private Const FRAME_ENDOSSO_CANCELAMENTO_POR_INELEGIBILIDADE = 8
'MU-2017-060371-adequacoes-no-segp0176
Private Const FRAME_ENDOSSO_ALTERACAO_PLANO = 9
Private Const FRAME_ENDOSSO_ALTERACAO_PAGAMENTO = 10
Private Const FRAME_ENDOSSO_EXCLUSAO_COBERTURA = 11
Private Const FRAME_ENDOSSO_MOV_FINANCEIRA_GERAL = 12

Dim giTpEndosso As Integer
Dim glPropostaId As Long
Dim glClienteId As Long
Dim glEndossoId  As Long

Private Sub cmdFechar_Click()
  Unload Me
End Sub

Private Sub CarregarDadosCadastrais()

On Error GoTo TrataErro

  Dim rsDadosCadastrais As Recordset
  Set rsDadosCadastrais = New Recordset
  
  'Dados Cadastrais ''''''''''''''''''''''''''''''''''
  fraDadosCadastrais.Visible = True
  
  Set rsDadosCadastrais = ObterDadosCadastrais(glPropostaId, glEndossoId)
  
  If rsDadosCadastrais.EOF Then
    Call MsgBox("N�o foi poss�vel obter os dados cadastrais da proposta", vbCritical)
    'oTo TrataErro
  End If
  
  
  txtClienteId.Text = rsDadosCadastrais("cliente_id")
  txtNome.Text = Trim(rsDadosCadastrais("nome"))
  txtTpPessoa.Text = IIf(IsNull(Trim(rsDadosCadastrais("tp_pessoa"))), "", Trim(rsDadosCadastrais("tp_pessoa")))
  txtIdTpPessoa.Text = Trim(rsDadosCadastrais("tp_pessoa_id"))
  
  If (Val(rsDadosCadastrais("tp_pessoa_id")) = 1) Or _
     ((Val(rsDadosCadastrais("tp_pessoa_id")) = 99) And _
      (Val(rsDadosCadastrais("produto_id")) = 1235) Or _
      (Val(rsDadosCadastrais("produto_id")) = 1236) Or _
      (Val(rsDadosCadastrais("produto_id")) = 1237)) Then
    txtCPFCNPJ.Text = MasCPF(rsDadosCadastrais("cpf_cnpj"))
  Else
    txtCPFCNPJ.Text = MasCGC(rsDadosCadastrais("cpf_cnpj"))
   
  End If

  TxtEmail.Text = IIf(IsNull(rsDadosCadastrais("e_mail")), "", rsDadosCadastrais("e_mail"))
  TxtDDD.Text = IIf(IsNull(rsDadosCadastrais("ddd_1")), "", rsDadosCadastrais("ddd_1"))
  TxtTelefone.Text = IIf(Len(rsDadosCadastrais("telefone_1") = 9), Format(rsDadosCadastrais("telefone_1"), "#####-####"), Format(rsDadosCadastrais("telefone_1"), "####-####"))
  TxtEndereco.Text = IIf(IsNull(Trim(rsDadosCadastrais("endereco"))), "", rsDadosCadastrais("endereco"))
  TxtBairro.Text = IIf(IsNull(Trim(rsDadosCadastrais("bairro"))), "", rsDadosCadastrais("bairro"))
  TxtCidade.Text = IIf(IsNull(Trim(rsDadosCadastrais("municipio"))), "", rsDadosCadastrais("municipio"))
  TxtUF.Text = IIf(IsNull(Trim(rsDadosCadastrais("estado"))), "", rsDadosCadastrais("estado"))
  TxtCEP.Text = IIf(IsNull(MasCEP(rsDadosCadastrais("cep"))), "", MasCEP(rsDadosCadastrais("cep")))
  
  rsDadosCadastrais.Close
     
  Set rsDadosCadastrais = Nothing

Exit Sub
TrataErro:
  Call TratarErro("CarregarDadosCadastrais", Me.name)
  Call FinalizarAplicacao

End Sub

Private Sub CarregarDadosSegurados()

On Error GoTo TrataErro

  Dim rsDadosSegurado As Recordset
  Set rsDadosSegurado = New Recordset
  Dim sLinha As String
  
  'Dados Cadastrais ''''''''''''''''''''''''''''''''''
  FraDadosSegurados.Visible = True
  
  'Carregar dados do grid''''''''''''''''''''''''''''''''''''''''''
  Set rsDadosSegurado = ObterDadosSegurado(glPropostaId, 0, glEndossoId)
  
   grdDadosSegurado.Rows = 1
   While Not rsDadosSegurado.EOF
        
           
                sLinha = ""
                sLinha = sLinha & Trim(rsDadosSegurado("cliente_id")) & vbTab
                sLinha = sLinha & Trim(rsDadosSegurado("nome")) & vbTab
                sLinha = sLinha & Format(rsDadosSegurado("cpf_cnpj")) & vbTab
                sLinha = sLinha & Format(rsDadosSegurado("dt_nascimento"), "dd/mm/yyyy") & vbTab
                
                'cristovao.rodrigues 14/07/2015 alteracao para retornar idade correta
                'sLinha = sLinha & DateDiff("yyyy", rsDadosSegurado("dt_nascimento"), DateTime.Now) & vbTab
                sLinha = sLinha & rsDadosSegurado("idade") & vbTab
                
                sLinha = sLinha & Trim(rsDadosSegurado("sexo")) & vbTab
                sLinha = sLinha & Trim(rsDadosSegurado("descricao")) & vbTab
                
                If InStr(1, "1205,1198", ConPropAdesaoALS.txtProduto.Tag) = 0 Then
                   sLinha = sLinha & Format(rsDadosSegurado("dt_fim_vigencia"), "dd/mm/yyyy") & vbTab   'zoro.gomes - Confitec - Projeto 19426678 - Endosso de Repactua��o PF - 29/09/2017
                End If
                grdDadosSegurado.AddItem sLinha
                rsDadosSegurado.MoveNext
    Wend
    
  grdDadosSegurado.ColWidth(0) = 0 'cliente_id
  grdDadosSegurado.ColWidth(1) = 3900 'Nome
    
Exit Sub
TrataErro:
  Call TratarErro("CarregarDadosSegurados", Me.name)
  Call FinalizarAplicacao

End Sub

Private Sub CarregarDadosFinanceiros()

On Error GoTo TrataErro

  Dim rsDadosCadastrais As Recordset
  Set rsDadosCadastrais = New Recordset
    
  ' Dados Bancarios '''''''''''''''''''''''''''''''''''''''''''''''
  fraFinanceiro.Visible = True
  Set rsDadosCadastrais = ObterDadosBancarios(glPropostaId, glEndossoId)
  
  If Not rsDadosCadastrais.EOF Then
  
    txtTpPagamento.Text = Trim(rsDadosCadastrais("nome"))
    txtFormaPgto.Text = Trim(rsDadosCadastrais("forma_pgto"))
    TxtBanco.Text = Trim(rsDadosCadastrais("deb_banco_id"))
    TxtNomeBanco.Text = Trim(rsDadosCadastrais("banco"))
    TxtCodAg.Text = Trim(rsDadosCadastrais("deb_ag_id"))
    TxtNomeAg.Text = Trim(rsDadosCadastrais("agencia_debito"))
    TxtCC.Text = Trim(rsDadosCadastrais("deb_cc_id"))
  
  End If
  
  rsDadosCadastrais.Close
  Set rsDadosCadastrais = Nothing

Exit Sub
TrataErro:
  Call TratarErro("CarregarDadosCadastrais", Me.name)
  Call FinalizarAplicacao

End Sub

Private Sub CarregarDadosEndosso()
On Error GoTo TrataErro

  Dim rsDados As Recordset
  
  Set rsDados = New Recordset
  Set rsDados = ObterDadosEndosso(glPropostaId, Val(txtEndossoId.Text))
  
  If rsDados.EOF Then
    'Call MsgBox("N�o foi poss�vel obter dados do endosso", vbCritical)
    Exit Sub
  End If
  
  mskDtEndosso.Text = Format(rsDados("dt_pedido_endosso"), "dd/mm/yyyy")
  mskDtRetornoBB.Text = IIf(Format(rsDados("dt_retorno_bb"), "dd/mm/yyyy") = "01/01/1900", "", Format(rsDados("dt_retorno_bb"), "dd/mm/yyyy"))
  txtUsuario.Text = Trim(rsDados("usuario"))
  txtUsuarioEmi.Text = Trim(rsDados("usuario_emissao"))
    
  rsDados.Close
  Set rsDados = Nothing

Exit Sub
TrataErro:
  Call TratarErro("CarregarDadosEndosso", Me.name)
  Call FinalizarAplicacao

End Sub

Private Sub CarregarDadosSeguro()
On Error GoTo TrataErro

  fraSeguro.Visible = True
  
      Call PreencheSeguroEndosso

Exit Sub
Resume
TrataErro:
  Call TratarErro("CarregarDadosFinanceiros", Me.name)
  Call FinalizarAplicacao

End Sub

Private Sub PreencheSeguroContratacao()

Dim rsDadosFinanceiros As Recordset
Dim sSql As String


  Set rsDadosFinanceiros = New Recordset
  
    sSql = ""
    sSql = sSql & "SELECT val_is " & vbNewLine
    sSql = sSql & ", val_iof " & vbNewLine
    sSql = sSql & " , val_premio_tarifa " & vbNewLine
    sSql = sSql & ", val_comissao " & vbNewLine
    sSql = sSql & ", dt_inicio_vigencia " & vbNewLine
    sSql = sSql & ", dt_fim_vigencia " & vbNewLine
    sSql = sSql & ", PRAZO_SEGURO = DATEDIFF(DAY,dt_inicio_vigencia,dt_fim_vigencia)" & vbNewLine
    sSql = sSql & ", num_parcelas = qtd_parcela_premio " & vbNewLine
    sSql = sSql & ", val_parcela " & vbNewLine
    sSql = sSql & "FROM SEGUROS_DB..PROPOSTA_ADESAO_TB " & vbNewLine
    sSql = sSql & "Where proposta_id = " & glPropostaId
  
  Set rsDadosFinanceiros = ExecutarSQL(gsSIGLASISTEMA, _
                                            glAmbiente_id, _
                                            App.Title, _
                                            App.FileDescription, _
                                            sSql, _
                                            True)
 
    If Not rsDadosFinanceiros.EOF Then
    
        'DESCOMENTAR QUANDO ATUALIZAR PROCEDURE
        If Not IsNull(rsDadosFinanceiros("val_is")) Then
          txtCapitalSegurado.Text = ConPropAdesao.FormataValorParaPadraoBrasil(rsDadosFinanceiros("val_is"))
        End If
        
        If Not IsNull(rsDadosFinanceiros("val_iof")) Then
          txtValIof.Text = ConPropAdesao.FormataValorParaPadraoBrasil(rsDadosFinanceiros("val_iof"))
        End If
        
        If Not IsNull(rsDadosFinanceiros("val_premio_tarifa")) Then
          txtvalorPremio.Text = ConPropAdesao.FormataValorParaPadraoBrasil(rsDadosFinanceiros("val_premio_tarifa"))
        End If
        
        If Not IsNull(rsDadosFinanceiros("val_comissao")) Then
          txtValComissao.Text = ConPropAdesao.FormataValorParaPadraoBrasil(rsDadosFinanceiros("val_comissao"))
        End If
        
        If Not IsNull(rsDadosFinanceiros("dt_inicio_vigencia")) Then
          txtIniVigencia.Text = (rsDadosFinanceiros("dt_inicio_vigencia"))
        End If
        
        If Not IsNull(rsDadosFinanceiros("dt_fim_vigencia")) Then
          txtFimVigencia.Text = (rsDadosFinanceiros("dt_fim_vigencia"))
        End If
        
        If Not IsNull(rsDadosFinanceiros("PRAZO_SEGURO")) Then
          txtPrazoVigencia.Text = (rsDadosFinanceiros("PRAZO_SEGURO"))
        End If
        
        txtNumParcelas.Text = IIf(IsNull(rsDadosFinanceiros("num_parcelas")), "0", rsDadosFinanceiros("num_parcelas"))
        txtValParidade.Text = IIf(IsNull(rsDadosFinanceiros("val_parcela")), "0,00", ConPropAdesao.FormataValorParaPadraoBrasil(rsDadosFinanceiros("val_parcela")))
    
    End If

  
   
rsDadosFinanceiros.Close
  Set rsDadosFinanceiros = Nothing
  Set rsDadosFinanceiros = New Recordset
  
   sSql = ""
   sSql = sSql & " SELECT TOP 1 qtd_vida = ISNULL(qtd_vida,0)  " & vbNewLine
   sSql = sSql & "FROM SEGUROS_DB..proposta_qtd_vida_tb " & vbNewLine
   sSql = sSql & "Where proposta_id = " & glPropostaId & vbNewLine
   sSql = sSql & "ORDER BY dt_inicio_vigencia " & vbNewLine
  
    Set rsDadosFinanceiros = ExecutarSQL(gsSIGLASISTEMA, _
                                            glAmbiente_id, _
                                            App.Title, _
                                            App.FileDescription, _
                                            sSql, _
                                            True)
  If Not rsDadosFinanceiros.EOF Then
    txtQtdVidas.Text = rsDadosFinanceiros("qtd_vida")
  Else
    txtQtdVidas.Text = "0"
  End If
  
  rsDadosFinanceiros.Close
  Set rsDadosFinanceiros = Nothing
  Set rsDadosFinanceiros = New Recordset
   
   sSql = ""
   sSql = sSql & "SELECT TOP 1 val_taxa = ISNULL(val_taxa,0)  " & vbNewLine
   sSql = sSql & "FROM SEGUROS_DB..ESCOLHA_TP_COB_VIDA_TB " & vbNewLine
   sSql = sSql & "Where proposta_id = " & glPropostaId & vbNewLine
   sSql = sSql & "  AND ISNULL(endosso_id,0) = 0 " & vbNewLine
  
    Set rsDadosFinanceiros = ExecutarSQL(gsSIGLASISTEMA, _
                                            glAmbiente_id, _
                                            App.Title, _
                                            App.FileDescription, _
                                            sSql, _
                                            True)
 If Not rsDadosFinanceiros.EOF Then
    txtTaxa.Text = ConPropAdesao.FormataValorParaPadraoBrasil(rsDadosFinanceiros("val_taxa"))
  End If
  
  rsDadosFinanceiros.Close
  Set rsDadosFinanceiros = Nothing
  
End Sub


Private Sub PreencheSeguroEndosso()

  
Dim rsDadosFinanceiros As Recordset
Dim sSql As String

On Error GoTo TrataErro

    
    Set rsDadosFinanceiros = New Recordset
    
    'cristovao.rodrigues 31/03/2015 - alteracao na proc - 17919477 Prestamista PJ
    sSql = ""
    sSql = sSql & "exec seguros_db.dbo.SEGS12231_SPS " & glPropostaId & " , " & glEndossoId & vbNewLine 'cristovao.rodrigues 31/03/2015
    
    Set rsDadosFinanceiros = ExecutarSQL(gsSIGLASISTEMA, _
                                          glAmbiente_id, _
                                          App.Title, _
                                          App.FileDescription, _
                                          sSql, _
                                          True)
  
    If Not rsDadosFinanceiros.EOF Then
    
        'DESCOMENTAR QUANDO ATUALIZAR PROCEDURE
         If Not IsNull(rsDadosFinanceiros("val_is")) Then
          txtCapitalSegurado.Text = ConPropAdesao.FormataValorParaPadraoBrasil(rsDadosFinanceiros("val_is"))
        End If
                        
        If Not IsNull(rsDadosFinanceiros("val_iof")) Then
            txtValIof.Text = ConPropAdesao.FormataValorParaPadraoBrasil(rsDadosFinanceiros("val_iof"))
        End If
        
        If Not IsNull(rsDadosFinanceiros("val_premio_bruto")) Then
            txtvalorPremio.Text = ConPropAdesao.FormataValorParaPadraoBrasil(rsDadosFinanceiros("val_premio_bruto"))
        End If
        
        If Not IsNull(rsDadosFinanceiros("val_comissao")) Then
          txtValComissao.Text = ConPropAdesao.FormataValorParaPadraoBrasil(rsDadosFinanceiros("val_comissao"))
        End If
            
        txtIniVigencia.Text = IIf(IsNull(rsDadosFinanceiros("dt_inicio_vigencia")), "", rsDadosFinanceiros("dt_inicio_vigencia"))
        
        txtFimVigencia.Text = IIf(IsNull(rsDadosFinanceiros("dt_fim_vigencia")), "", rsDadosFinanceiros("dt_fim_vigencia"))
        
        
        If Not IsNull(rsDadosFinanceiros("PRAZO_SEGURO")) Then
          txtPrazoVigencia.Text = (rsDadosFinanceiros("PRAZO_SEGURO"))
        End If
        
        txtNumParcelas.Text = IIf(IsNull(rsDadosFinanceiros("num_parcelas")), "0", rsDadosFinanceiros("num_parcelas"))
        
        If Not IsNull(rsDadosFinanceiros("val_primeiraparcela")) Then
            txtDemaisParcelas.Text = ConPropAdesao.FormataValorParaPadraoBrasil((rsDadosFinanceiros("val_primeiraparcela")))
        Else
            txtDemaisParcelas.Text = "0,00"
        End If
        'Carolina Santos - Inicio - 10/03/2015
        If Not IsNull(rsDadosFinanceiros("val_segunda_parcela")) Then
            txtSegundaParcela.Text = ConPropAdesao.FormataValorParaPadraoBrasil((rsDadosFinanceiros("val_segunda_parcela")))
        Else
            txtSegundaParcela.Text = "0,00"
        End If
        'Carolina Santos - Fim
        If Not IsNull(rsDadosFinanceiros("val_parcela")) Then
            txtValParidade.Text = ConPropAdesao.FormataValorParaPadraoBrasil((rsDadosFinanceiros("val_parcela")))
        Else
            txtValParidade.Text = "0,00"
        End If
        txtQtdVidas.Text = IIf(IsNull(rsDadosFinanceiros("qtd_vida")), "0", rsDadosFinanceiros("qtd_vida"))
        txtTaxa.Text = IIf(IsNull(rsDadosFinanceiros("val_taxa")), "0,00", rsDadosFinanceiros("val_taxa"))
        'Carolina Santos - Inicio - 10/03/2015
        'txtCredDebito = IIf(IsNull(rsDadosFinanceiros("debitoCredito")), "Outros", rsDadosFinanceiros("debitoCredito"))
        
        If InStr(1, "1205,1198", ConPropAdesaoALS.txtProduto.Tag) = 0 Then 'zoro.gomes - Confitec - Projeto 19426678 - Endosso de Repactua��o PF - 29/09/2017
            'cristovao.rodrigues 31/03/2015 - 17919477 Prestamista PJ
            frmConsultaEndosso.txtCredDebito.Text = ConPropAdesao.FormataValorParaPadraoBrasil(ConPropAdesaoALS.CalCreDeb) 'cristovao.rodrigues 27/07/2015
        Else
            txtCredDebito.Text = rsDadosFinanceiros("val_cred_debito")
        End If
        
        'cristovao.rodrigues 27/04/2015 comentado, matendo valor anterior
        'txtDemaisParcelas.Text = ConPropAdesao.FormataValorParaPadraoBrasil(txtvalorPremio * ((DateDiff("d", ConPropAdesaoALS.txtDtIniVigProposta.Text, IIf(IsNull(rsDadosFinanceiros("dt_prox_parcela")), ConPropAdesaoALS.txtDtIniVigProposta.Text, rsDadosFinanceiros("dt_prox_parcela"))) / txtPrazoVigencia)))
        
        'ConPropAdesaoALS.grdEndosso.TextMatrix(ConPropAdesaoALS.grdEndosso.Row, 0) - endosso
        
'        Select Case rsDadosFinanceiros("debitoCredito")
'        Case 1
'            txtCredDebito.Text = "Debito em Conta"
'        Case 2
'            txtCredDebito.Text = "Cr�dito em Conta"
'        Case Else
'            txtCredDebito.Text = "Outros"
'        End Select
        'Carolina Santos - Fim
    End If



rsDadosFinanceiros.Close
Set rsDadosFinanceiros = Nothing

'cristovao.rodrigues 31/03/2015
Exit Sub
Resume
TrataErro:
  Call TratarErro("PreencheSeguroEndosso", Me.name)
  Call FinalizarAplicacao


End Sub

Private Function ObterDadosBancarios(lPropostaId As Long, lEndossoId As Long) As Recordset

On Error GoTo TrataErro
Dim sSql As String

If lEndossoId = 0 Then
   
    'MU-2017-060371-adequacoes-no-segp0176
    If ConPropAdesaoALS.txtProduto.Tag = "1235" _
       Or ConPropAdesaoALS.txtProduto.Tag = "1236" _
       Or ConPropAdesaoALS.txtProduto.Tag = "1237" Then
       
          sSql = ""
          sSql = sSql & " DECLARE @MENOR_ENDOSSO_252 INT " & vbNewLine
         
          sSql = sSql & " SELECT @MENOR_ENDOSSO_252 = ISNULL(MIN(ENDOSSO_ID),0)" & vbNewLine
          sSql = sSql & "   FROM SEGUROS_DB..ENDOSSO_TB" & vbNewLine
          sSql = sSql & "  Where proposta_id = " & lPropostaId & vbNewLine
          sSql = sSql & "    AND TP_ENDOSSO_ID = 252" & vbNewLine
          
          sSql = sSql & " IF (@MENOR_ENDOSSO_252 = 0)" & vbNewLine
          sSql = sSql & " BEGIN" & vbNewLine
          
          sSql = sSql & "SELECT periodo_pgto_tb.nome,                                                                  " & vbNewLine
          sSql = sSql & "     forma_pgto_tb.Nome Forma_Pgto,                                                           " & vbNewLine
          sSql = sSql & "     deb_ag_id = deb_agencia_id ,                                                                              " & vbNewLine
          sSql = sSql & "     deb_banco_id,                                                                               " & vbNewLine
          sSql = sSql & "     banco_tb.nome banco,                                                                     " & vbNewLine
          sSql = sSql & "     ag_debito.nome as agencia_debito,                                                        " & vbNewLine
          sSql = sSql & "     deb_cc_id = deb_conta_corrente_id                                                                              " & vbNewLine
          sSql = sSql & "  FROM proposta_fechada_tb  WITH (NOLOCK)                                                        " & vbNewLine
          sSql = sSql & " INNER JOIN agencia_tb ag_debito  WITH (NOLOCK)                                                   " & vbNewLine
          sSql = sSql & "  ON ag_debito.agencia_id = proposta_fechada_tb.deb_agencia_id                                 " & vbNewLine
          sSql = sSql & " INNER JOIN banco_tb  WITH (NOLOCK)                                                               " & vbNewLine
          sSql = sSql & "  ON banco_tb.banco_id = proposta_fechada_tb.deb_banco_id                                      " & vbNewLine
          sSql = sSql & " INNER JOIN periodo_pgto_tb  WITH (NOLOCK)                                                        " & vbNewLine
          sSql = sSql & "  ON proposta_fechada_tb.periodo_pgto_id = periodo_pgto_tb.periodo_pgto_id                     " & vbNewLine
          sSql = sSql & " INNER JOIN forma_pgto_tb  WITH (NOLOCK)                                                          " & vbNewLine
          sSql = sSql & "  ON forma_pgto_tb.forma_pgto_id = proposta_fechada_tb.forma_pgto_id                         " & vbNewLine
          sSql = sSql & " WHERE proposta_fechada_tb.proposta_id = " & lPropostaId & vbNewLine
          
          sSql = sSql & " END" & vbNewLine
          sSql = sSql & "ELSE" & vbNewLine
          sSql = sSql & "BEGIN" & vbNewLine
          
          sSql = sSql & "SELECT nome = isnull(periodo_pgto_tb.nome,''),                                                                  " & vbNewLine
          sSql = sSql & "     isnull(forma_pgto_tb.Nome,'') Forma_Pgto,                                                           " & vbNewLine
          sSql = sSql & "     deb_ag_id = isnull(ult_deb_agencia_id,0) ,                                                                              " & vbNewLine
          sSql = sSql & "     deb_banco_id = isnull(deb_banco_id,0),                                                                               " & vbNewLine
          sSql = sSql & "     isnull(banco_tb.nome,'') banco,                                                                     " & vbNewLine
          sSql = sSql & "     isnull(ag_debito.nome,'') as agencia_debito,                                                        " & vbNewLine
          sSql = sSql & "     deb_cc_id = isnull(ult_deb_conta_corrente_id,0)                                                                              " & vbNewLine
          sSql = sSql & "  FROM proposta_fechada_tb  WITH (NOLOCK)                                                        " & vbNewLine
          sSql = sSql & "   JOIN SEGUROS_DB..endosso_proposta_tb  WITH (NOLOCK)                                          " & vbNewLine
          sSql = sSql & "     ON endosso_proposta_tb.proposta_id = proposta_fechada_tb.proposta_id                            " & vbNewLine
          sSql = sSql & " INNER JOIN agencia_tb ag_debito  WITH (NOLOCK)                                                   " & vbNewLine
          sSql = sSql & "  ON ag_debito.agencia_id = endosso_proposta_tb.ult_deb_agencia_id                                 " & vbNewLine
          sSql = sSql & " INNER JOIN banco_tb  WITH (NOLOCK)                                                               " & vbNewLine
          sSql = sSql & "  ON banco_tb.banco_id = proposta_fechada_tb.deb_banco_id                                      " & vbNewLine
          sSql = sSql & " INNER JOIN periodo_pgto_tb  WITH (NOLOCK)                                                        " & vbNewLine
          sSql = sSql & "  ON proposta_fechada_tb.periodo_pgto_id = periodo_pgto_tb.periodo_pgto_id                     " & vbNewLine
          sSql = sSql & " INNER JOIN forma_pgto_tb  WITH (NOLOCK)                                                          " & vbNewLine
          sSql = sSql & "  ON forma_pgto_tb.forma_pgto_id = endosso_proposta_tb.ult_forma_pgto_id                         " & vbNewLine
          sSql = sSql & " WHERE proposta_fechada_tb.proposta_id = " & lPropostaId & vbNewLine
          sSql = sSql & "    AND endosso_proposta_tb.endosso_id = @MENOR_ENDOSSO_252 " & vbNewLine
          
          sSql = sSql & "END" & vbNewLine
       
       
    Else
    'MU-2017-060371-adequacoes-no-segp0176
    sSql = ""
    sSql = sSql & " DECLARE @MENOR_ENDOSSO_252 INT " & vbNewLine
   
    sSql = sSql & " SELECT @MENOR_ENDOSSO_252 = ISNULL(MIN(ENDOSSO_ID),0)" & vbNewLine
    sSql = sSql & "   FROM SEGUROS_DB..ENDOSSO_TB" & vbNewLine
    sSql = sSql & "  Where proposta_id = " & lPropostaId & vbNewLine
    sSql = sSql & "    AND TP_ENDOSSO_ID = 252" & vbNewLine
    
    sSql = sSql & " IF (@MENOR_ENDOSSO_252 = 0)" & vbNewLine
    sSql = sSql & " BEGIN" & vbNewLine
    
    sSql = sSql & "SELECT periodo_pgto_tb.nome,                                                                  " & vbNewLine
    sSql = sSql & "     forma_pgto_tb.Nome Forma_Pgto,                                                           " & vbNewLine
    sSql = sSql & "     deb_ag_id = deb_agencia_id ,                                                                              " & vbNewLine
    sSql = sSql & "     deb_banco_id,                                                                               " & vbNewLine
    sSql = sSql & "     banco_tb.nome banco,                                                                     " & vbNewLine
    sSql = sSql & "     ag_debito.nome as agencia_debito,                                                        " & vbNewLine
    sSql = sSql & "     deb_cc_id = deb_conta_corrente_id                                                                              " & vbNewLine
    sSql = sSql & "  FROM proposta_adesao_tb  WITH (NOLOCK)                                                        " & vbNewLine
    sSql = sSql & " INNER JOIN agencia_tb ag_debito  WITH (NOLOCK)                                                   " & vbNewLine
    sSql = sSql & "  ON ag_debito.agencia_id = proposta_adesao_tb.deb_agencia_id                                 " & vbNewLine
    sSql = sSql & " INNER JOIN banco_tb  WITH (NOLOCK)                                                               " & vbNewLine
    sSql = sSql & "  ON banco_tb.banco_id = proposta_adesao_tb.deb_banco_id                                      " & vbNewLine
    sSql = sSql & " INNER JOIN periodo_pgto_tb  WITH (NOLOCK)                                                        " & vbNewLine
    sSql = sSql & "  ON proposta_adesao_tb.periodo_pgto_id = periodo_pgto_tb.periodo_pgto_id                     " & vbNewLine
    sSql = sSql & " INNER JOIN forma_pgto_tb  WITH (NOLOCK)                                                          " & vbNewLine
    sSql = sSql & "  ON forma_pgto_tb.forma_pgto_id = proposta_adesao_tb.forma_pgto_id                         " & vbNewLine
    sSql = sSql & " WHERE proposta_adesao_tb.proposta_id = " & lPropostaId & vbNewLine
    
    sSql = sSql & " END" & vbNewLine
    sSql = sSql & "ELSE" & vbNewLine
    sSql = sSql & "BEGIN" & vbNewLine
    
    sSql = sSql & "SELECT nome = isnull(periodo_pgto_tb.nome,''),                                                                  " & vbNewLine
    sSql = sSql & "     isnull(forma_pgto_tb.Nome,'') Forma_Pgto,                                                           " & vbNewLine
    sSql = sSql & "     deb_ag_id = isnull(ult_deb_agencia_id,0) ,                                                                              " & vbNewLine
    sSql = sSql & "     deb_banco_id = isnull(deb_banco_id,0),                                                                               " & vbNewLine
    sSql = sSql & "     isnull(banco_tb.nome,'') banco,                                                                     " & vbNewLine
    sSql = sSql & "     isnull(ag_debito.nome,'') as agencia_debito,                                                        " & vbNewLine
    sSql = sSql & "     deb_cc_id = isnull(ult_deb_conta_corrente_id,0)                                                                              " & vbNewLine
    sSql = sSql & "  FROM PROPOSTA_ADESAO_TB  WITH (NOLOCK)                                                        " & vbNewLine
    sSql = sSql & "   JOIN SEGUROS_DB..endosso_proposta_tb  WITH (NOLOCK)                                          " & vbNewLine
    sSql = sSql & "     ON endosso_proposta_tb.proposta_id = PROPOSTA_ADESAO_TB.proposta_id                            " & vbNewLine
    sSql = sSql & " INNER JOIN agencia_tb ag_debito  WITH (NOLOCK)                                                   " & vbNewLine
    sSql = sSql & "  ON ag_debito.agencia_id = endosso_proposta_tb.ult_deb_agencia_id                                 " & vbNewLine
    sSql = sSql & " INNER JOIN banco_tb  WITH (NOLOCK)                                                               " & vbNewLine
    sSql = sSql & "  ON banco_tb.banco_id = proposta_adesao_tb.deb_banco_id                                      " & vbNewLine
    sSql = sSql & " INNER JOIN periodo_pgto_tb  WITH (NOLOCK)                                                        " & vbNewLine
    sSql = sSql & "  ON proposta_adesao_tb.periodo_pgto_id = periodo_pgto_tb.periodo_pgto_id                     " & vbNewLine
    sSql = sSql & " INNER JOIN forma_pgto_tb  WITH (NOLOCK)                                                          " & vbNewLine
    sSql = sSql & "  ON forma_pgto_tb.forma_pgto_id = endosso_proposta_tb.ult_forma_pgto_id                         " & vbNewLine
    sSql = sSql & " WHERE proposta_adesao_tb.proposta_id = " & lPropostaId & vbNewLine
    sSql = sSql & "    AND endosso_proposta_tb.endosso_id = @MENOR_ENDOSSO_252 " & vbNewLine
    
    sSql = sSql & "END" & vbNewLine
    
    
    End If
    
   
    
   Else
   
   'MU-2017-060371-adequacoes-no-segp0176
    If ConPropAdesaoALS.txtProduto.Tag = "1235" _
       Or ConPropAdesaoALS.txtProduto.Tag = "1236" _
       Or ConPropAdesaoALS.txtProduto.Tag = "1237" Then
       
         sSql = ""
         sSql = sSql & "SELECT periodo_pgto_tb.nome,                                                                  " & vbNewLine
         sSql = sSql & "     forma_pgto_tb.Nome Forma_Pgto,                                                           " & vbNewLine
         sSql = sSql & "     deb_ag_id = deb_agencia_id ,                                                                              " & vbNewLine
         sSql = sSql & "     deb_banco_id,                                                                               " & vbNewLine
         sSql = sSql & "     banco_tb.nome banco,                                                                     " & vbNewLine
         sSql = sSql & "     ag_debito.nome as agencia_debito,                                                        " & vbNewLine
         sSql = sSql & "     deb_cc_id = deb_conta_corrente_id                                                                              " & vbNewLine
         sSql = sSql & "  FROM proposta_fechada_tb  WITH (NOLOCK)                                                        " & vbNewLine
         sSql = sSql & " INNER JOIN agencia_tb ag_debito  WITH (NOLOCK)                                                   " & vbNewLine
         sSql = sSql & "  ON ag_debito.agencia_id = proposta_fechada_tb.deb_agencia_id                                 " & vbNewLine
         sSql = sSql & " INNER JOIN banco_tb  WITH (NOLOCK)                                                               " & vbNewLine
         sSql = sSql & "  ON banco_tb.banco_id = proposta_fechada_tb.deb_banco_id                                      " & vbNewLine
         sSql = sSql & " INNER JOIN periodo_pgto_tb  WITH (NOLOCK)                                                        " & vbNewLine
         sSql = sSql & "  ON proposta_fechada_tb.periodo_pgto_id = periodo_pgto_tb.periodo_pgto_id                     " & vbNewLine
         sSql = sSql & " INNER JOIN forma_pgto_tb  WITH (NOLOCK)                                                          " & vbNewLine
         sSql = sSql & "  ON forma_pgto_tb.forma_pgto_id = proposta_fechada_tb.forma_pgto_id                         " & vbNewLine
         sSql = sSql & " WHERE proposta_fechada_tb.proposta_id = " & lPropostaId & vbNewLine
       
    Else
    'MU-2017-060371-adequacoes-no-segp0176
    sSql = ""
    sSql = sSql & "SELECT periodo_pgto_tb.nome,                                                                  " & vbNewLine
    sSql = sSql & "     forma_pgto_tb.Nome Forma_Pgto,                                                           " & vbNewLine
    sSql = sSql & "     deb_ag_id = deb_agencia_id ,                                                                              " & vbNewLine
    sSql = sSql & "     deb_banco_id,                                                                               " & vbNewLine
    sSql = sSql & "     banco_tb.nome banco,                                                                     " & vbNewLine
    sSql = sSql & "     ag_debito.nome as agencia_debito,                                                        " & vbNewLine
    sSql = sSql & "     deb_cc_id = deb_conta_corrente_id                                                                              " & vbNewLine
    sSql = sSql & "  FROM proposta_adesao_tb  WITH (NOLOCK)                                                        " & vbNewLine
    sSql = sSql & " INNER JOIN agencia_tb ag_debito  WITH (NOLOCK)                                                   " & vbNewLine
    sSql = sSql & "  ON ag_debito.agencia_id = proposta_adesao_tb.deb_agencia_id                                 " & vbNewLine
    sSql = sSql & " INNER JOIN banco_tb  WITH (NOLOCK)                                                               " & vbNewLine
    sSql = sSql & "  ON banco_tb.banco_id = proposta_adesao_tb.deb_banco_id                                      " & vbNewLine
    sSql = sSql & " INNER JOIN periodo_pgto_tb  WITH (NOLOCK)                                                        " & vbNewLine
    sSql = sSql & "  ON proposta_adesao_tb.periodo_pgto_id = periodo_pgto_tb.periodo_pgto_id                     " & vbNewLine
    sSql = sSql & " INNER JOIN forma_pgto_tb  WITH (NOLOCK)                                                          " & vbNewLine
    sSql = sSql & "  ON forma_pgto_tb.forma_pgto_id = proposta_adesao_tb.forma_pgto_id                         " & vbNewLine
    sSql = sSql & " WHERE proposta_adesao_tb.proposta_id = " & lPropostaId & vbNewLine

    
    End If
   

    End If
    
    Set ObterDadosBancarios = ExecutarSQL(gsSIGLASISTEMA, _
                                          glAmbiente_id, _
                                          App.Title, _
                                          App.FileDescription, _
                                          sSql, _
                                          True)
Exit Function
TrataErro:
  Call TratarErro("ObterDadosBancarios", Me.name)
  Call FinalizarAplicacao
End Function

Private Function ObterDadosCadastrais(lPropostaId As Long, lEndossoId As Long) As Recordset
On Error GoTo TrataErro

  Dim sSql As String
   
    sSql = ""
    sSql = sSql & " DECLARE @MENOR_ENDOSSO_51 INT " & vbNewLine
   
    sSql = sSql & " SELECT @MENOR_ENDOSSO_51 = ISNULL(MIN(ENDOSSO_ID),0)" & vbNewLine
    sSql = sSql & "   FROM SEGUROS_DB..ENDOSSO_TB" & vbNewLine
    sSql = sSql & "  Where proposta_id = " & lPropostaId & vbNewLine
    sSql = sSql & "    AND ENDOSSO_ID >  " & lEndossoId & vbNewLine
    sSql = sSql & "    AND TP_ENDOSSO_ID = 51" & vbNewLine
    
    sSql = sSql & " IF (@MENOR_ENDOSSO_51 = 0)" & vbNewLine
    sSql = sSql & " BEGIN" & vbNewLine
    
    sSql = sSql & "    SELECT cliente_tb.cliente_id,                                                          " & vbNewLine
    sSql = sSql & "           cliente_tb.nome,                                                                " & vbNewLine
    sSql = sSql & "           tp_pessoa_id ,                                                                  " & vbNewLine
    sSql = sSql & "           tp_pessoa = CASE                                                                " & vbNewLine
    sSql = sSql & "                        WHEN tp_pessoa_id = 1 THEN 'F�SICA'                                " & vbNewLine
    sSql = sSql & "                        WHEN tp_pessoa_id = 2 THEN 'JUR�DICA'                              " & vbNewLine
    sSql = sSql & "                       END ,                                                               " & vbNewLine
    sSql = sSql & "           cpf_cnpj,                                                                       " & vbNewLine
    sSql = sSql & "           cliente_tb.e_mail,                                                              " & vbNewLine
    sSql = sSql & "           cliente_tb.ddd_1,                                                               " & vbNewLine
    sSql = sSql & "           cliente_tb.telefone_1,                                                          " & vbNewLine
    sSql = sSql & "           endereco_corresp_tb.endereco,                                                   " & vbNewLine
    sSql = sSql & "           endereco_corresp_tb.municipio,                                                  " & vbNewLine
    sSql = sSql & "           endereco_corresp_tb.bairro,                                                     " & vbNewLine
    sSql = sSql & "           endereco_corresp_tb.estado,                                                     " & vbNewLine
    sSql = sSql & "           endereco_corresp_tb.cep,                                                        " & vbNewLine
    sSql = sSql & "           proposta_tb.produto_id                                                          " & vbNewLine
    sSql = sSql & "      FROM proposta_tb  WITH (NOLOCK)                                                      " & vbNewLine
    sSql = sSql & "      JOIN cliente_tb   WITH (NOLOCK)                                                      " & vbNewLine
    sSql = sSql & "        ON cliente_tb.cliente_id = proposta_tb.prop_cliente_id                             " & vbNewLine
    sSql = sSql & "      JOIN endereco_corresp_tb  WITH (NOLOCK)                                              " & vbNewLine
    sSql = sSql & "        ON endereco_corresp_tb.proposta_id = proposta_tb.proposta_id                       " & vbNewLine
    sSql = sSql & "     WHERE proposta_tb.proposta_id = " & lPropostaId & vbNewLine
'    'Jessica.Adao - Confitec Sistemas - 20150721 - Projeto 18319298
'    sSql = sSql & "     UNION" & vbNewLine
'    sSql = sSql & "    SELECT cliente_tb.cliente_id," & vbNewLine
'    sSql = sSql & "           cliente_tb.nome," & vbNewLine
'    sSql = sSql & "           tp_pessoa_id ," & vbNewLine
'    sSql = sSql & "           tp_pessoa = CASE" & vbNewLine
'    sSql = sSql & "                        WHEN tp_pessoa_id = 1 THEN 'F�SICA'" & vbNewLine
'    sSql = sSql & "                        WHEN tp_pessoa_id = 2 THEN 'JUR�DICA'" & vbNewLine
'    sSql = sSql & "                       END ," & vbNewLine
'    sSql = sSql & "           cpf_cnpj," & vbNewLine
'    sSql = sSql & "           cliente_tb.e_mail," & vbNewLine
'    sSql = sSql & "           cliente_tb.ddd_1," & vbNewLine
'    sSql = sSql & "           cliente_tb.telefone_1," & vbNewLine
'    sSql = sSql & "           endereco_corresp_tb.endereco," & vbNewLine
'    sSql = sSql & "           endereco_corresp_tb.municipio," & vbNewLine
'    sSql = sSql & "           endereco_corresp_tb.bairro," & vbNewLine
'    sSql = sSql & "           endereco_corresp_tb.estado," & vbNewLine
'    sSql = sSql & "           endereco_corresp_tb.Cep" & vbNewLine
'    sSql = sSql & "      FROM proposta_COMPLEMENTAR_tb  WITH (NOLOCK)" & vbNewLine
'    sSql = sSql & "      JOIN cliente_tb   WITH (NOLOCK)" & vbNewLine
'    sSql = sSql & "        ON cliente_tb.cliente_id = proposta_COMPLEMENTAR_tb.prop_cliente_id" & vbNewLine
'    sSql = sSql & "      JOIN endereco_corresp_tb  WITH (NOLOCK)" & vbNewLine
'    sSql = sSql & "        ON endereco_corresp_tb.proposta_id = proposta_COMPLEMENTAR_tb.proposta_id" & vbNewLine
'    sSql = sSql & "     Where proposta_COMPLEMENTAR_tb.proposta_id = " & lPropostaId & vbNewLine

    
    sSql = sSql & " END" & vbNewLine
    sSql = sSql & "ELSE" & vbNewLine
    sSql = sSql & "BEGIN" & vbNewLine
    
    sSql = sSql & " SELECT cliente_id   = ISNULL(endosso_proposta_tb.pessoa_id, 0),                             " & vbNewLine
    sSql = sSql & "        nome         = ISNULL(ult_nome, ''),                                                 " & vbNewLine
    sSql = sSql & "        tp_pessoa_id = ISNULL(ult_tp_pessoa, 0),                                             " & vbNewLine
    sSql = sSql & "        tp_pessoa    = CASE                                                                  " & vbNewLine
    sSql = sSql & "                           WHEN ult_tp_pessoa = 1 THEN 'F�SICA'                              " & vbNewLine
    sSql = sSql & "                           WHEN ult_tp_pessoa = 2 THEN 'JUR�DICA'                            " & vbNewLine
    sSql = sSql & "                           ELSE ''                                                           " & vbNewLine
    sSql = sSql & "                        END,                                                                 " & vbNewLine
    sSql = sSql & "        cpf_cnpj     = ISNULL(ult_cgc, 0),                                                   " & vbNewLine
    sSql = sSql & "        e_mail       = ISNULL(ult_e_mail, ''),                                               " & vbNewLine
    sSql = sSql & "        ddd_1        = ISNULL(ult_ddd, ''),                                                  " & vbNewLine
    sSql = sSql & "        telefone_1   = ISNULL(ult_telefone, ''),                                             " & vbNewLine
    sSql = sSql & "        endereco     = ISNULL(ult_endereco, ''),                                             " & vbNewLine
    sSql = sSql & "        municipio    = ISNULL(ult_municipio, ''),                                            " & vbNewLine
    sSql = sSql & "        bairro       = ISNULL(ult_bairro, ''),                                               " & vbNewLine
    sSql = sSql & "        estado       = ISNULL(ult_estado, ''),                                               " & vbNewLine
    sSql = sSql & "        cep          = ISNULL(ult_cep, ''),                                                  " & vbNewLine
    sSql = sSql & "        produto_id   = ISNULL(produto_id, 0)                                                 " & vbNewLine
    sSql = sSql & "   FROM SEGUROS_DB..PROPOSTA_TB  WITH (NOLOCK)                                               " & vbNewLine
    sSql = sSql & "   JOIN SEGUROS_DB..endosso_proposta_tb  WITH (NOLOCK)                                       " & vbNewLine
    sSql = sSql & "     ON endosso_proposta_tb.proposta_id = PROPOSTA_TB.proposta_id                            " & vbNewLine
    sSql = sSql & "  WHERE PROPOSTA_TB.proposta_id = " & lPropostaId & vbNewLine
    sSql = sSql & "    AND endosso_proposta_tb.endosso_id = @MENOR_ENDOSSO_51 " & vbNewLine
'
'    sSql = sSql & " UNION                                                                                       " & vbNewLine
    
'    sSql = sSql & " SELECT cliente_id   = ISNULL(endosso_proposta_complementar_tb.cliente_id, 0),                             " & vbNewLine
'    sSql = sSql & "        nome         = ISNULL(ult_nome, ''),                                                             " & vbNewLine
'    sSql = sSql & "        tp_pessoa_id = ISNULL(ult_tp_pessoa, 0),                                                        " & vbNewLine
'    sSql = sSql & "        tp_pessoa    = CASE                                                                  " & vbNewLine
'    sSql = sSql & "                           WHEN ult_tp_pessoa = 1 THEN 'F�SICA'                              " & vbNewLine
'    sSql = sSql & "                           WHEN ult_tp_pessoa = 2 THEN 'JUR�DICA'                            " & vbNewLine
'    sSql = sSql & "                           ELSE ''                                                           " & vbNewLine
'    sSql = sSql & "                        END,                                                                 " & vbNewLine
'    sSql = sSql & "        cpf_cnpj     = ISNULL(ult_cgc, 0),                                                              " & vbNewLine
'    sSql = sSql & "        e_mail       = ISNULL(ult_e_mail, ''),                                                           " & vbNewLine
'    sSql = sSql & "        ddd_1        = ISNULL(ult_ddd, ''),                                                              " & vbNewLine
'    sSql = sSql & "        telefone_1   = ISNULL(ult_telefone, ''),                                                         " & vbNewLine
'    sSql = sSql & "        endereco     = ISNULL(ult_endereco, ''),                                                          " & vbNewLine
'    sSql = sSql & "        municipio    = ISNULL(ult_municipio, ''),                                                        " & vbNewLine
'    sSql = sSql & "        bairro       = ISNULL(ult_bairro, ''),                                                           " & vbNewLine
'    sSql = sSql & "        estado       = ISNULL(ult_estado, ''),                                                           " & vbNewLine
'    sSql = sSql & "        cep          = ISNULL(ult_cep, '')                                                               " & vbNewLine
'    sSql = sSql & "   FROM SEGUROS_DB..PROPOSTA_TB  WITH (NOLOCK)                                                  " & vbNewLine
'    sSql = sSql & "   JOIN SEGUROS_DB..endosso_proposta_complementar_tb  WITH (NOLOCK)                             " & vbNewLine
'    sSql = sSql & "     ON endosso_proposta_complementar_tb.proposta_id = PROPOSTA_TB.proposta_id                  " & vbNewLine
'    sSql = sSql & "  WHERE PROPOSTA_TB.proposta_id = " & lPropostaId & vbNewLine
'    sSql = sSql & "    AND endosso_proposta_complementar_tb.endosso_id = @MENOR_ENDOSSO_51 " & vbNewLine
    
    
    sSql = sSql & "END" & vbNewLine
    
    
    Set ObterDadosCadastrais = ExecutarSQL(gsSIGLASISTEMA, _
                                           glAmbiente_id, _
                                           App.Title, _
                                           App.FileDescription, _
                                           sSql, _
                                           True)

Exit Function
TrataErro:
  Call TratarErro("ObterDadosCadastrais", Me.name)
  Call FinalizarAplicacao

End Function

Private Function ObterDadosSegurado(lPropostaId As Long, lClienteId As Long, lEndossoId As Long) As Recordset
On Error GoTo TrataErro

  Dim sSql As String
  
    
   If InStr(1, "1235,1236,1237", ConPropAdesaoALS.txtProduto.Tag) > 0 Then

      If lEndossoId = 0 Then
             sSql = ""
             sSql = sSql & "    SELECT cliente_tb.cliente_id,                                                          " & vbNewLine
             sSql = sSql & "           cliente_tb.nome,                                                                " & vbNewLine
             sSql = sSql & "           cpf_cnpj = ISNULL(pessoa_fisica_tb.cpf,0),                                      " & vbNewLine
             sSql = sSql & "           dt_nascimento = ISNULL(pessoa_fisica_tb.dt_nascimento,''),                      " & vbNewLine
             sSql = sSql & "           sexo = ISNULL(pessoa_fisica_tb.sexo,''),                                        " & vbNewLine
             sSql = sSql & "           tp_estado_civil_tb.DESCRICAO,                                                   " & vbNewLine
             sSql = sSql & "           dt_fim_vigencia_seg dt_fim_vigencia,                                                                " & vbNewLine
             sSql = sSql & "           idade        = ISNULL(dbo.calcula_idade_fn('" & Format(Data_Sistema, "yyyymmdd") & "',pessoa_fisica_tb.dt_nascimento),0) " & vbNewLine
             sSql = sSql & "       FROM SEGURO_VIDA_TB  WITH (NOLOCK)                                                  " & vbNewLine
             sSql = sSql & " INNER JOIN cliente_tb   WITH (NOLOCK)                                                     " & vbNewLine
             sSql = sSql & "         ON cliente_tb.cliente_id = SEGURO_VIDA_TB.prop_cliente_id                         " & vbNewLine
             sSql = sSql & "       JOIN pessoa_fisica_tb  WITH (NOLOCK)                                                " & vbNewLine
             sSql = sSql & "         ON pessoa_fisica_tb.pf_cliente_id = cliente_tb.cliente_id                         " & vbNewLine
             sSql = sSql & "       LEFT JOIN tp_estado_civil_tb  WITH (NOLOCK)                                         " & vbNewLine
             sSql = sSql & "         ON pessoa_fisica_tb.estado_civil = tp_estado_civil_tb.estado_civil_BB             " & vbNewLine
             sSql = sSql & "      WHERE SEGURO_VIDA_TB.proposta_id = " & lPropostaId & vbNewLine
             sSql = sSql & "        AND SEGURO_VIDA_TB.endosso_id IS NULL " & vbNewLine
      
      Else
             sSql = ""
             sSql = sSql & "    SELECT cliente_tb.cliente_id,                                                          " & vbNewLine
             sSql = sSql & "           cliente_tb.nome,                                                                " & vbNewLine
             sSql = sSql & "           cpf_cnpj = ISNULL(pessoa_fisica_tb.cpf,0),                                      " & vbNewLine
             sSql = sSql & "           dt_nascimento = ISNULL(pessoa_fisica_tb.dt_nascimento,''),                      " & vbNewLine
             sSql = sSql & "           sexo = ISNULL(pessoa_fisica_tb.sexo,''),                                        " & vbNewLine
             sSql = sSql & "           tp_estado_civil_tb.DESCRICAO,                                                   " & vbNewLine
             sSql = sSql & "           dt_fim_vigencia_seg dt_fim_vigencia,                                            " & vbNewLine
             sSql = sSql & "           idade        = ISNULL(dbo.calcula_idade_fn('" & Format(Data_Sistema, "yyyymmdd") & "',pessoa_fisica_tb.dt_nascimento),0) " & vbNewLine
             sSql = sSql & "       FROM SEGURO_VIDA_TB  WITH (NOLOCK)                                                  " & vbNewLine
             sSql = sSql & " INNER JOIN cliente_tb   WITH (NOLOCK)                                                     " & vbNewLine
             sSql = sSql & "         ON cliente_tb.cliente_id = SEGURO_VIDA_TB.prop_cliente_id                         " & vbNewLine
             sSql = sSql & "       JOIN pessoa_fisica_tb  WITH (NOLOCK)                                                " & vbNewLine
             sSql = sSql & "         ON pessoa_fisica_tb.pf_cliente_id = cliente_tb.cliente_id                         " & vbNewLine
             sSql = sSql & "       LEFT JOIN tp_estado_civil_tb  WITH (NOLOCK)                                         " & vbNewLine
             sSql = sSql & "         ON pessoa_fisica_tb.estado_civil = tp_estado_civil_tb.estado_civil_BB             " & vbNewLine
             sSql = sSql & "      WHERE SEGURO_VIDA_TB.proposta_id = " & lPropostaId & vbNewLine
             sSql = sSql & "        AND SEGURO_VIDA_TB.endosso_id = " & lEndossoId & vbNewLine
      
      End If
   
   Else
    
    If lEndossoId = 0 Then
   
        sSql = ""
        sSql = sSql & " DECLARE @MENOR_ENDOSSO_51 INT " & vbNewLine
       
        sSql = sSql & " SELECT @MENOR_ENDOSSO_51 = ISNULL(MIN(ENDOSSO_ID),0)" & vbNewLine
        sSql = sSql & "   FROM SEGUROS_DB..ENDOSSO_TB" & vbNewLine
        sSql = sSql & "  Where proposta_id = " & lPropostaId & vbNewLine
        sSql = sSql & "    AND TP_ENDOSSO_ID = 51" & vbNewLine
        
        sSql = sSql & " IF (@MENOR_ENDOSSO_51 = 0)" & vbNewLine
        sSql = sSql & " BEGIN" & vbNewLine
        
        sSql = sSql & "    SELECT cliente_tb.cliente_id,                                                          " & vbNewLine
        sSql = sSql & "           cliente_tb.nome,                                                                " & vbNewLine
        sSql = sSql & "           cpf_cnpj = ISNULL(pessoa_fisica_tb.cpf,0),                                      " & vbNewLine
        sSql = sSql & "           dt_nascimento = ISNULL(pessoa_fisica_tb.dt_nascimento,''),                      " & vbNewLine
        sSql = sSql & "           sexo = ISNULL(pessoa_fisica_tb.sexo,''),                                        " & vbNewLine
        sSql = sSql & "           tp_estado_civil_tb.DESCRICAO,                        " & vbNewLine
        sSql = sSql & "           dt_fim_vigencia                                                                 " & vbNewLine
        
        'cristovao.rodrigues 14/07/2015 alteracao para retornar idade correta
        sSql = sSql & "        ,idade        = ISNULL(dbo.calcula_idade_fn('" & Format(Data_Sistema, "yyyymmdd") & "',pessoa_fisica_tb.dt_nascimento),0) " & vbNewLine
        
        sSql = sSql & "       FROM PROPOSTA_COMPLEMENTAR_TB  WITH (NOLOCK)                                                                    " & vbNewLine
        sSql = sSql & " INNER JOIN cliente_tb   WITH (NOLOCK)                                                                    " & vbNewLine
        sSql = sSql & "         ON cliente_tb.cliente_id = PROPOSTA_COMPLEMENTAR_TB.prop_cliente_id                            " & vbNewLine
        sSql = sSql & "       JOIN pessoa_fisica_tb  WITH (NOLOCK)                                                               " & vbNewLine
        sSql = sSql & "         ON pessoa_fisica_tb.pf_cliente_id = cliente_tb.cliente_id                         " & vbNewLine
        sSql = sSql & "       LEFT JOIN tp_estado_civil_tb  WITH (NOLOCK)  " & vbNewLine
        sSql = sSql & "         ON pessoa_fisica_tb.estado_civil = tp_estado_civil_tb.estado_civil_BB               " & vbNewLine
        sSql = sSql & "      WHERE PROPOSTA_COMPLEMENTAR_TB.proposta_id = " & lPropostaId & vbNewLine
        
        sSql = sSql & " END" & vbNewLine
        sSql = sSql & "ELSE" & vbNewLine
        sSql = sSql & "BEGIN" & vbNewLine
           
    '    sSql = sSql & " SELECT DISTINCT cliente_id    = ISNULL(endosso_proposta_tb.pessoa_id, 0),                             " & vbNewLine
    '    sSql = sSql & "        nome          = ISNULL(ult_nome, ''),                                                             " & vbNewLine
    '    sSql = sSql & "        cpf_cnpj      = ISNULL(ult_cgc, 0),                                                              " & vbNewLine
    '    sSql = sSql & "        dt_nascimento = ISNULL(ult_dt_nascimento, ''),                                                           " & vbNewLine
    '    sSql = sSql & "        sexo      = ISNULL(ult_sexo, ''),                                                              " & vbNewLine
    '    sSql = sSql & "        tp_estado_civil_tb.DESCRICAO,                        " & vbNewLine
    '    sSql = sSql & "        dt_fim_vigencia                                                          " & vbNewLine
    '
    '    'cristovao.rodrigues 14/07/2015 alteracao para retornar idade correta
    '    sSql = sSql & "        ,idade        = ISNULL(dbo.calcula_idade_fn('" & Format(Data_Sistema, "yyyymmdd") & "',ult_dt_nascimento ),0) " & vbNewLine
    '
    '    sSql = sSql & "   FROM SEGUROS_DB..PROPOSTA_COMPLEMENTAR_TB  WITH (NOLOCK)                                                  " & vbNewLine
    '    sSql = sSql & "   JOIN SEGUROS_DB..endosso_proposta_tb  WITH (NOLOCK)                                          " & vbNewLine
    '    sSql = sSql & "     ON endosso_proposta_tb.proposta_id = PROPOSTA_COMPLEMENTAR_TB.proposta_id                            " & vbNewLine
    '    sSql = sSql & " INNER JOIN cliente_tb   WITH (NOLOCK)                                                                    " & vbNewLine
    '    sSql = sSql & "         ON cliente_tb.cliente_id = PROPOSTA_COMPLEMENTAR_TB.prop_cliente_id                            " & vbNewLine
    '    sSql = sSql & "       JOIN pessoa_fisica_tb  WITH (NOLOCK)                                                               " & vbNewLine
    '    sSql = sSql & "         ON pessoa_fisica_tb.pf_cliente_id = cliente_tb.cliente_id                         " & vbNewLine
    '    sSql = sSql & "       LEFT JOIN tp_estado_civil_tb  WITH (NOLOCK)  " & vbNewLine
    '    sSql = sSql & "         ON pessoa_fisica_tb.estado_civil = tp_estado_civil_tb.estado_civil_BB               " & vbNewLine
    '    sSql = sSql & "  WHERE PROPOSTA_COMPLEMENTAR_TB.proposta_id = " & lPropostaId & vbNewLine
    '    sSql = sSql & "    AND endosso_proposta_tb.endosso_id = @MENOR_ENDOSSO_51 " & vbNewLine
    '    sSql = sSql & " UNION " & vbNewLine
        sSql = sSql & "    SELECT cliente_tb.cliente_id,                                                          " & vbNewLine
        sSql = sSql & "           cliente_tb.nome,                                                                " & vbNewLine
        sSql = sSql & "           cpf_cnpj = ISNULL(pessoa_fisica_tb.cpf,0),                                      " & vbNewLine
        sSql = sSql & "           dt_nascimento = ISNULL(pessoa_fisica_tb.dt_nascimento,''),                      " & vbNewLine
        sSql = sSql & "           sexo = ISNULL(pessoa_fisica_tb.sexo,''),                                        " & vbNewLine
        sSql = sSql & "           tp_estado_civil_tb.DESCRICAO,                        " & vbNewLine
        sSql = sSql & "           dt_fim_vigencia                                                                 " & vbNewLine
        
        'cristovao.rodrigues 14/07/2015 alteracao para retornar idade correta
        sSql = sSql & "        ,idade        = ISNULL(dbo.calcula_idade_fn('" & Format(Data_Sistema, "yyyymmdd") & "',pessoa_fisica_tb.dt_nascimento),0) " & vbNewLine
        
        sSql = sSql & "       FROM PROPOSTA_COMPLEMENTAR_TB  WITH (NOLOCK)                                                                    " & vbNewLine
        sSql = sSql & " INNER JOIN cliente_tb   WITH (NOLOCK)                                                                    " & vbNewLine
        sSql = sSql & "         ON cliente_tb.cliente_id = PROPOSTA_COMPLEMENTAR_TB.prop_cliente_id                            " & vbNewLine
        sSql = sSql & "       JOIN pessoa_fisica_tb  WITH (NOLOCK)                                                               " & vbNewLine
        sSql = sSql & "         ON pessoa_fisica_tb.pf_cliente_id = cliente_tb.cliente_id                         " & vbNewLine
        sSql = sSql & "       LEFT JOIN tp_estado_civil_tb  WITH (NOLOCK)  " & vbNewLine
        sSql = sSql & "         ON pessoa_fisica_tb.estado_civil = tp_estado_civil_tb.estado_civil_BB               " & vbNewLine
        sSql = sSql & "      WHERE PROPOSTA_COMPLEMENTAR_TB.proposta_id = " & lPropostaId & vbNewLine
        
        sSql = sSql & "END" & vbNewLine
   Else
        sSql = ""
        sSql = sSql & "    SELECT cliente_tb.cliente_id,                                                          " & vbNewLine
        sSql = sSql & "           cliente_tb.nome,                                                                " & vbNewLine
        sSql = sSql & "           cpf_cnpj = ISNULL(pessoa_fisica_tb.cpf,0),                                      " & vbNewLine
        sSql = sSql & "           dt_nascimento = ISNULL(pessoa_fisica_tb.dt_nascimento,''),                      " & vbNewLine
        sSql = sSql & "           sexo = ISNULL(pessoa_fisica_tb.sexo,''),                                        " & vbNewLine
        sSql = sSql & "           tp_estado_civil_tb.DESCRICAO,                                                   " & vbNewLine
        sSql = sSql & "           dt_fim_vigencia                                                                 " & vbNewLine
        
        'cristovao.rodrigues 14/07/2015 alteracao para retornar idade correta
        sSql = sSql & "        ,idade        = ISNULL(dbo.calcula_idade_fn('" & Format(Data_Sistema, "yyyymmdd") & "',pessoa_fisica_tb.dt_nascimento),0) " & vbNewLine
        sSql = sSql & "       FROM PROPOSTA_COMPLEMENTAR_TB  WITH (NOLOCK)                                                                               " & vbNewLine
        sSql = sSql & " INNER JOIN cliente_tb   WITH (NOLOCK)                                                                                            " & vbNewLine
        sSql = sSql & "         ON cliente_tb.cliente_id = PROPOSTA_COMPLEMENTAR_TB.prop_cliente_id                                                      " & vbNewLine
        sSql = sSql & "       JOIN pessoa_fisica_tb  WITH (NOLOCK)                                                                                       " & vbNewLine
        sSql = sSql & "         ON pessoa_fisica_tb.pf_cliente_id = cliente_tb.cliente_id                                                                " & vbNewLine
        sSql = sSql & "       LEFT JOIN tp_estado_civil_tb  WITH (NOLOCK)                                                                                " & vbNewLine
        sSql = sSql & "         ON pessoa_fisica_tb.estado_civil = tp_estado_civil_tb.estado_civil_BB                                                    " & vbNewLine
    'DEMANDA 18319298 - PJ FASE IV - CORRE��O NA EXIBI��O DOS CLIENTES NA GRID - MARCIOMS - INICIO
        sSql = sSql & "       join endosso_tb endosso_tb with(nolock)                                                                                    " & vbNewLine
        sSql = sSql & "         on endosso_tb.proposta_id = PROPOSTA_COMPLEMENTAR_TB.proposta_id                                                         " & vbNewLine
        sSql = sSql & "      WHERE PROPOSTA_COMPLEMENTAR_TB.proposta_id = " & lPropostaId & vbNewLine
        sSql = sSql & "        AND ENDOSSO_TB.ENDOSSO_ID = " & lEndossoId & vbNewLine
    '       sSql = sSql & "    AND PROPOSTA_COMPLEMENTAR_TB.dt_fim_vigencia  IS NULL                                    " & vbNewLine
        sSql = sSql & "            AND (PROPOSTA_COMPLEMENTAR_TB.dt_fim_vigencia > endosso_tb.dt_emissao or PROPOSTA_COMPLEMENTAR_TB.dt_FIM_vigencia IS NULL) " & vbNewLine
     'DEMANDA 18319298 - PJ FASE IV - CORRE��O NA EXIBI��O DOS CLIENTES NA GRID - MARCIOMS - FIM
        
        'zoro.gomes - Confitec - Projeto 19426678 - Endosso de Repactua��o PF - 29/09/2017
        If InStr(1, "1205,1198", ConPropAdesaoALS.txtProduto.Tag) > 0 Then
            sSql = sSql & " UNION "
            sSql = sSql & "    SELECT cliente_tb.cliente_id,                                                          " & vbNewLine
            sSql = sSql & "           cliente_tb.nome,                                                                " & vbNewLine
            sSql = sSql & "           cpf_cnpj = ISNULL(pessoa_fisica_tb.cpf,0),                                      " & vbNewLine
            sSql = sSql & "           dt_nascimento = ISNULL(pessoa_fisica_tb.dt_nascimento,''),                      " & vbNewLine
            sSql = sSql & "           sexo = ISNULL(pessoa_fisica_tb.sexo,''),                                        " & vbNewLine
            sSql = sSql & "           tp_estado_civil_tb.DESCRICAO,                                                   " & vbNewLine
            sSql = sSql & "           '',                                                                             " & vbNewLine
            sSql = sSql & "           idade = ISNULL(dbo.calcula_idade_fn('" & Format(Data_Sistema, "yyyymmdd") & "',pessoa_fisica_tb.dt_nascimento),0) " & vbNewLine
            sSql = sSql & "       FROM PROPOSTA_TB  WITH (NOLOCK)                                                                               " & vbNewLine
            sSql = sSql & " INNER JOIN cliente_tb   WITH (NOLOCK)                                                                                            " & vbNewLine
            sSql = sSql & "         ON cliente_tb.cliente_id = PROPOSTA_TB.prop_cliente_id                                                      " & vbNewLine
            sSql = sSql & "       JOIN pessoa_fisica_tb  WITH (NOLOCK)                                                                                       " & vbNewLine
            sSql = sSql & "         ON pessoa_fisica_tb.pf_cliente_id = cliente_tb.cliente_id                                                                " & vbNewLine
            sSql = sSql & "       LEFT JOIN tp_estado_civil_tb  WITH (NOLOCK)                                                                                " & vbNewLine
            sSql = sSql & "         ON pessoa_fisica_tb.estado_civil = tp_estado_civil_tb.estado_civil_BB                                                    " & vbNewLine
            sSql = sSql & "       join endosso_tb endosso_tb with(nolock)                                                                                    " & vbNewLine
            sSql = sSql & "         on endosso_tb.proposta_id = PROPOSTA_TB.proposta_id                                                         " & vbNewLine
            sSql = sSql & "      WHERE PROPOSTA_TB.proposta_id = " & lPropostaId & vbNewLine
            sSql = sSql & "        AND ENDOSSO_TB.ENDOSSO_ID = " & lEndossoId
        End If
    End If
    
   End If
    
       
    Set ObterDadosSegurado = ExecutarSQL(gsSIGLASISTEMA, _
                                           glAmbiente_id, _
                                           App.Title, _
                                           App.FileDescription, _
                                           sSql, _
                                           True)

Exit Function
TrataErro:
  Call TratarErro("ObterDadosSegurado", Me.name)
  Call FinalizarAplicacao

End Function


Private Function ObterDadosEndosso(lPropostaId As Long, lEndossoId As Long) As Recordset
On Error GoTo TrataErro

  Dim sSql As String

  sSql = ""
  sSql = sSql & "SELECT dt_pedido_endosso,  " & vbNewLine
  sSql = sSql & "       ISNULL(dt_retorno_bb,'19000101') dt_retorno_bb, " & vbNewLine
  sSql = sSql & "       usuario,             " & vbNewLine
  sSql = sSql & "       ISNULL(usuario_emissao,'') usuario_emissao " & vbNewLine
  sSql = sSql & "  FROM endosso_tb   WITH (NOLOCK)         " & vbNewLine
  sSql = sSql & " WHERE proposta_id =" & lPropostaId & vbNewLine
  sSql = sSql & "   AND endosso_id = " & lEndossoId & vbNewLine
  
  Set ObterDadosEndosso = ExecutarSQL(gsSIGLASISTEMA, _
                                      glAmbiente_id, _
                                      App.Title, _
                                      App.FileDescription, _
                                      sSql, _
                                      True)
                                      

Exit Function
TrataErro:
  Call TratarErro("ObterDadosEndosso", Me.name)
  Call FinalizarAplicacao

End Function



Private Sub CarregarDados()

On Error GoTo TrataErro

'      'cristovao.rodrigues 19/05/2015
'      Label1(3).Caption = "Vlr. Pr�mio Comercial(Total):"
'      Label1(6).Caption = "Taxa Net Total :"
'      Label1(13).Caption = "Vlr. Pr�mio 1� Parcela:"
'      Label1(7).Caption = "Vlr. Pr�mio Demais Parcelas:"
'      ''' ocultar
'      Label1(8).Visible = False
'      txtSegundaParcela.Visible = False
'      Label1(15).Visible = False
'      txtCredDebito.Visible = False

  
  Select Case giTpEndosso
  
  'ENDOSSO ZERO
    Case FRAME_ENDOSSO_CONTRATACAO
      Call CarregarDadosCadastrais
      Call CarregarDadosFinanceiros
      Call CarregarDadosSegurados
      Call CarregarDadosSeguro
      
      fraEndosso.Visible = False
      
      'cristovao.rodrigues 19/05/2015
      Label1(3).Caption = "Vlr. Pr�mio Comercial(Total):"
      Label1(6).Caption = "Taxa Net Total :"
      Label1(13).Caption = "Vlr. Pr�mio 1� Parcela:"
      Label1(7).Caption = "Vlr. Pr�mio Demais Parcelas:"
      ''' ocultar
      Label1(8).Visible = False
      txtSegundaParcela.Visible = False
      Label1(15).Visible = False
      txtCredDebito.Visible = False
      ''' recolocar
      Label1(7).Top = 1860
      txtValParidade.Top = 1860
            
      
     
    'ENDOSSO 51
    Case FRAME_ENDOSSO_REAJUSTE
      Call CarregarDadosCadastrais
      Call CarregarDadosSegurados

    'ENDOSSO 274
    Case FRAME_ENDOSSO_ALTERACAO
      Call CarregarDadosFinanceiros
  
    'MU-2017-060371-adequacoes-no-segp0176
    'ENDOSSO 336 A 338 e ENDOSSO 31
    Case FRAME_ENDOSSO_EXCLUSAO
      Call CarregarDadosSeguro
      Call CarregarDadosSegurados
      
    'ENDOSSOS 37, 276, 308
    Case FRAME_ENDOSSO_ALTERACAO_PLANO
      Call CarregarDadosSeguro
            
    'ENDOSSOS 252, 348
    Case FRAME_ENDOSSO_ALTERACAO_PAGAMENTO
      Call CarregarDadosSeguro
      Call CarregarDadosFinanceiros
            
    'ENDOSSO 349
    Case FRAME_ENDOSSO_EXCLUSAO_COBERTURA
      Call CarregarDadosCadastrais
      Call CarregarDadosSeguro
      Call CarregarDadosSegurados
      
    'ENDOSSO GERAL COM MOVIMENTA��O FINANCEIRA 999
    Case FRAME_ENDOSSO_MOV_FINANCEIRA_GERAL
      Call CarregarDadosCadastrais
      Call CarregarDadosFinanceiros
      Call CarregarDadosSegurados
      Call CarregarDadosSeguro
    'MU-2017-060371-adequacoes-no-segp0176
            
    'ENDOSSO 340
    Case FRAME_ENDOSSO_CANCELAMENTO_POR_INELEGIBILIDADE
    
    'DEMAIS ENDOSSOS
    Case FRAME_ENDOSSO_GERAL

    Case Else
      Call MsgBox("Tipo de endosso n�o permitido", vbCritical)
      Call FinalizarAplicacao
  
  End Select

Exit Sub
TrataErro:
  Call TratarErro("CarregarDados", Me.name)
  Call FinalizarAplicacao

End Sub

Private Sub Form_Load()

  'mouse
  Me.MousePointer = vbHourglass
  
  'Grid Segurados
   grdDadosSegurado.Rows = 1
    
  'obtendo dados da tela anterior
  Call ObterDadosTelaAnterior
  
 If ConPropAdesaoALS.grdEndosso.TextMatrix(ConPropAdesaoALS.grdEndosso.Row, 2) <> "0" Then
    Call CarregarDadosEndosso
  End If
  
'  interface
  If ConfigurarInterface Then
    'Carregando Dados
    Call CarregarDados
  End If
  
  Me.MousePointer = vbDefault
  
End Sub

Private Sub ObterDadosTelaAnterior()

    txtEndossoId.Text = ConPropAdesaoALS.grdEndosso.TextMatrix(ConPropAdesaoALS.grdEndosso.Row, 0)
    mskDtEmissao.Text = ConPropAdesaoALS.grdEndosso.TextMatrix(ConPropAdesaoALS.grdEndosso.Row, 1)
    txttpEndosso.Text = ConPropAdesaoALS.grdEndosso.TextMatrix(ConPropAdesaoALS.grdEndosso.Row, 2) & " - " & _
                        ConPropAdesaoALS.grdEndosso.TextMatrix(ConPropAdesaoALS.grdEndosso.Row, 3)
    txttpEndosso.Tag = ConPropAdesaoALS.grdEndosso.TextMatrix(ConPropAdesaoALS.grdEndosso.Row, 2)
    txtNumEndossoBB.Text = ConPropAdesaoALS.grdEndosso.TextMatrix(ConPropAdesaoALS.grdEndosso.Row, 5)
    txtDescEndosso.Text = ConPropAdesaoALS.grdEndosso.TextMatrix(ConPropAdesaoALS.grdEndosso.Row, 6)
    txttpAlteracaoEndosso.Text = ConPropAdesaoALS.grdEndosso.TextMatrix(ConPropAdesaoALS.grdEndosso.Row, 8) '19/10/2018 - rfarzat  - Endosso 252 - Sala �gil
  
  glPropostaId = ConPropAdesaoALS.txtProposta.Text
  glEndossoId = txtEndossoId.Text
End Sub

Private Function ConfigurarInterface() As Boolean

  'Titulo '''''''''''
  'lrocha - 05/01/2006
  If Trim(SelPropAdesao.MSFlexGrid1.TextMatrix(SelPropAdesao.MSFlexGrid1.Row, 6)) <> "" Then
    Me.Caption = "SEGP0176 - Consulta de propostas - Endosso" & " - " & SelPropAdesao.MSFlexGrid1.TextMatrix(SelPropAdesao.MSFlexGrid1.Row, 6)
  Else
    Me.Caption = "SEGP0176 - Consulta de propostas - Endosso"
  End If

  ConfigurarInterface = True
  
    'cristovao.rodrigues 19/05/2015
    Label1(3).Caption = "Vlr. Pr�mio Coml. (Total):"
    Label1(6).Caption = "Taxa Coml. Cobertura:"
    Label1(13).Caption = "Pr�xima Parcela:"
    Label1(7).Caption = "Demais Parcelas:"
    ''' ocultar
    Label1(8).Visible = True
    txtSegundaParcela.Visible = True
    Label1(15).Visible = True
    txtCredDebito.Visible = True
    ''' recolocar
    Label1(7).Top = 2265
    txtValParidade.Top = 2265
  
   'MU-2017-060371-adequacoes-no-segp0176
   If ConPropAdesaoALS.txtProduto.Tag = "1235" _
          Or ConPropAdesaoALS.txtProduto.Tag = "1236" _
          Or ConPropAdesaoALS.txtProduto.Tag = "1237" Then
      Select Case Val(txttpEndosso.Tag)
      
      'Endosso Contrata��o''''''''''''''''''''''''''''''''''''''''''''''
        Case TP_ENDOSSO_CONTRATACAO
             giTpEndosso = FRAME_ENDOSSO_CONTRATACAO
             
        'Endosso 336 a 338''''''''''''''''''''''''''''''''''''''''''''''
        'Incluido o endosso 31 sinistro de conjuge
        Case TP_ENDOSSO_DE_REPACTUACAO, _
             TP_ENDOSSO_EXCLCUSAO_DE_SEGURADO, _
             TP_ENDOSSO_EXCLCUSAO_DE_SEGURADO_POR_NEGATIVA_DE_INDENIZACAO_MORTE, _
             TP_ENDOSSO_REPACTUACAO_DA_OPERACAO_DE_CREDITO_PF, _
             TP_ENDOSSO_SINISTRO_DE_CONJUGE
             giTpEndosso = FRAME_ENDOSSO_EXCLUSAO
             
        'Endosso 37 - Altera��o de plano
        'Endosso 276 - Altera��o de capital segurado
        'Endosso 308 - Ajustes anuais
        Case TP_ENDOSSO_ALTERACAO_DE_PLANO_NOVOS, _
             TP_ENDOSSO_ALTERACAO_DE_CAPITAL_SEGURADO, _
             TP_ENDOSSO_AJUSTES_ANUAIS
             giTpEndosso = FRAME_ENDOSSO_ALTERACAO_PLANO
             
        'Endosso 252 - Altera��o de forma de pagamento
        'Endosso 348 - Altera��o de periodicidade de forma de pagamento
        Case TP_ENDOSSO_ALTERACAO_DE_FORMA_DE_PAGAMENTO, _
             TP_ENDOSSO_ALTERACAO_DE_PERIODICIDADE_DE_PAGAMENTO
             giTpEndosso = FRAME_ENDOSSO_ALTERACAO_PAGAMENTO
             
        'Endosso 349 - Exclusao de cobertura por sinistro
        Case TP_ENDOSSO_EXCLUSAO_DE_COBERTURA_POR_SINISTRO
             giTpEndosso = FRAME_ENDOSSO_EXCLUSAO_COBERTURA
        
        'Endosso 51''''''''''''''''''''''''''''''''''''''''''''
        Case TP_ENDOSSO_ALTERACAO_CADASTRAL
             giTpEndosso = FRAME_ENDOSSO_REAJUSTE
                      
        'Cancelamento de proposta ''''''''''''''''''''''''''''''''''''''
        Case TP_ENDOSSO_ALTERACAO_DE_DADOS_DE_CARTAO_DE_CREDITO
             giTpEndosso = FRAME_ENDOSSO_ALTERACAO
                
        Case TP_ENDOSSO_CANCELAMENTO_POR_INELEGIBILIDADE
             giTpEndosso = FRAME_ENDOSSO_CANCELAMENTO_POR_INELEGIBILIDADE
        
        Case Else
             giTpEndosso = FRAME_ENDOSSO_MOV_FINANCEIRA_GERAL
      
      End Select
      
   Else 'MU-2017-060371-adequacoes-no-segp0176
  Select Case Val(txttpEndosso.Tag)
  
  'Endosso Contrata��o''''''''''''''''''''''''''''''''''''''''''''''
    Case TP_ENDOSSO_CONTRATACAO
         giTpEndosso = FRAME_ENDOSSO_CONTRATACAO
         
    'Endosso 336 a 338''''''''''''''''''''''''''''''''''''''''''''''
    Case TP_ENDOSSO_DE_REPACTUACAO, _
         TP_ENDOSSO_EXCLCUSAO_DE_SEGURADO, _
         TP_ENDOSSO_EXCLCUSAO_DE_SEGURADO_POR_NEGATIVA_DE_INDENIZACAO_MORTE, _
         TP_ENDOSSO_REPACTUACAO_DA_OPERACAO_DE_CREDITO_PF 'R.FOUREAUX - 14/08/2017
         giTpEndosso = FRAME_ENDOSSO_EXCLUSAO
         
    
    'Endosso 51''''''''''''''''''''''''''''''''''''''''''''
    Case TP_ENDOSSO_ALTERACAO_CADASTRAL
         giTpEndosso = FRAME_ENDOSSO_REAJUSTE
                  
    'Cancelamento de proposta ''''''''''''''''''''''''''''''''''''''
    Case TP_ENDOSSO_ALTERACAO_DE_DADOS_DE_CARTAO_DE_CREDITO
         giTpEndosso = FRAME_ENDOSSO_ALTERACAO
            
    Case TP_ENDOSSO_CANCELAMENTO_POR_INELEGIBILIDADE
         giTpEndosso = FRAME_ENDOSSO_CANCELAMENTO_POR_INELEGIBILIDADE
    
    Case Else
      giTpEndosso = FRAME_ENDOSSO_GERAL
  
  End Select

   End If
  
End Function

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    If ConPropAdesaoALS.txtProduto.Tag = "1225" _
       Or ConPropAdesaoALS.txtProduto.Tag = "1231" _
       Or ConPropAdesaoALS.txtProduto.Tag = "1235" _
       Or ConPropAdesaoALS.txtProduto.Tag = "1236" _
       Or ConPropAdesaoALS.txtProduto.Tag = "1237" Then
       ConPropAdesaoALS.Show
    Else
       ConPropAdesao.Show
    End If
    Unload Me
    Set frmConsultaEndosso = Nothing
End Sub

Private Function IdentificaPPE(strCPF As String) As Boolean

'Sessao de variaveis
Dim strSql      As String
Dim rc          As Recordset

'Manipulador de erros
On Error GoTo ErrIdentificaPPE

'Valor Padrao
IdentificaPPE = False

'Retira Formacao caso exista
strCPF = Replace(strCPF, ".", "")
strCPF = Replace(strCPF, "-", "")

'Busca Identificar se faz parte do PPE
strSql = "SELECT COUNT(*) AS TOTAL "
strSql = strSql & " FROM seguros_db..PPE_ORGAO_CARGO_TB as TPPE "
strSql = strSql & " WHERE CAST(TPPE.CPF AS BIGINT) = CAST('" & strCPF & "' AS BIGINT)"
strSql = strSql & " AND (TPPE.DT_EXCLUSAO_PPE IS NULL OR  TPPE.DT_EXCLUSAO_PPE >= GETDATE())"

'Executa o Comando
Set rc = ExecutarSQL(gsSIGLASISTEMA, _
                                       glAmbiente_id, _
                                       App.Title, _
                                       App.FileDescription, _
                                       strSql, _
                                       True)
    
'Verifica se voltou registro
If rc.EOF Then GoTo Finaliza

'Verifica Valor Valido
If IsNull(rc!Total) Then GoTo Finaliza

'vERIFICA nao ENCONTROU
If CInt(rc!Total) <= 0 Then GoTo Finaliza
    
'Valor Padrao
IdentificaPPE = True

'Finaliza
Finaliza:
rc.Close
Set rc = Nothing

'Sai
Exit Function

'Manipulador de erros
ErrIdentificaPPE:

    'Fecha Recordsrt
    rc.Close
    Set rc = Nothing

End Function

Private Sub grdDadosSegurado_Click()
'Dim rsDadosSegurado As Recordset
'Set rsDadosSegurado = New Recordset
'
'    glClienteId = grdDadosSegurado.TextMatrix(grdDadosSegurado.RowSel(), 0)
'    MousePointer = vbHourglass
'
'    Set rsDadosSegurado = ObterDadosSegurado(glPropostaId, glClienteId, glEndossoId)
'
'    txtClienteId.Text = Trim(rsDadosSegurado("cliente_id"))
'    txtNome.Text = Trim(rsDadosSegurado("nome"))
'    txtTpPessoa.Text = Trim(rsDadosSegurado("tp_pessoa"))
'    txtIdTpPessoa.Text = Trim(rsDadosSegurado("tp_pessoa_id"))
'
'    If Val(rsDadosSegurado("tp_pessoa_id")) = 1 Then
'        txtCPFCNPJ.Text = MasCPF(rsDadosSegurado("cpf_cnpj"))
'    Else
'        txtCPFCNPJ.Text = MasCGC(rsDadosSegurado("cpf_cnpj"))
'
'    End If
'
'    TxtEmail.Text = IIf(IsNull(rsDadosSegurado("e_mail")), "", rsDadosSegurado("e_mail"))
'    TxtDDD.Text = IIf(IsNull(rsDadosSegurado("ddd_1")), "", rsDadosSegurado("ddd_1"))
'    TxtTelefone.Text = IIf(Len(rsDadosSegurado("telefone_1") = 9), Format(rsDadosSegurado("telefone_1"), "#####-####"), Format(rsDadosSegurado("telefone_1"), "####-####"))
'    TxtEndereco.Text = Trim(rsDadosSegurado("endereco"))
'    TxtBairro.Text = Trim(rsDadosSegurado("bairro"))
'    TxtCidade.Text = Trim(rsDadosSegurado("municipio"))
'    TxtUF.Text = Trim(rsDadosSegurado("estado"))
'    TxtCEP.Text = MasCEP(rsDadosSegurado("cep"))
'
'    MousePointer = vbDefault
    
End Sub

