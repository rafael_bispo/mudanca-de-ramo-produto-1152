VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsAgendInconsist"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'vitor.cabral - Nova Consultoria - 10/10/2013 - Demanda 17886578 � Registro das avalia��es na recep��o de parcelas
Option Explicit

Private ExcelApp As Excel.Application
Private Formatacao As clsFormatacao
Private IntervaloAgendamentosInconsistentes As String


Public Sub Config(oExcelApp As Excel.Application, oFormatacao As clsFormatacao)
    Set ExcelApp = oExcelApp
    Set Formatacao = oFormatacao
End Sub

Public Sub Preencher(ByVal gridAgendamentos As MSFlexGrid)
    
    ExcelApp.Sheets.Add.name = NomePlanilhaAgendInconsist
    ExcelApp.Sheets(NomePlanilhaAgendInconsist).Select
    
    Call PreencherDadosAgendamentosInconsistentes(NomePlanilhaAgendInconsist, gridAgendamentos)
    
    'ExcelApp.Columns(COL_DATA_INDICE & ":" & COL_FATOR_COMPARATIVO_IOF).EntireColumn.AutoFit
End Sub

Private Sub PreencherDadosAgendamentosInconsistentes(ByVal nomePlanilha As String, ByVal gridAgendamentos As MSFlexGrid)

    Dim Linha As Long
    Dim currentGridRow As Integer
    
    IntervaloAgendamentosInconsistentes = ""

    With ExcelApp

        Call PreencherCabecalhoAgendamentosInconsistentes

        Linha = LINHA_CABECALHO
        currentGridRow = 1
        
        Do Until currentGridRow = gridAgendamentos.Rows
            Linha = Linha + 1
            .Cells(Linha, COL_COD_ERRO) = gridAgendamentos.TextMatrix(currentGridRow, 0)
            .Cells(Linha, COL_DESCRICAO_LOG) = gridAgendamentos.TextMatrix(currentGridRow, 1)
            .Cells(Linha, COL_NUMERO_PARCELA) = gridAgendamentos.TextMatrix(currentGridRow, 2)
            .Cells(Linha, COL_REMESSA) = gridAgendamentos.TextMatrix(currentGridRow, 3)
            .Cells(Linha, COL_SEQUENCIAL_REMESSA) = gridAgendamentos.TextMatrix(currentGridRow, 4)
            .Cells(Linha, COL_NOSSO_NUMERO) = gridAgendamentos.TextMatrix(currentGridRow, 5)
            .Cells(Linha, COL_NOSSO_NUMERO_DV) = gridAgendamentos.TextMatrix(currentGridRow, 6)
            .Cells(Linha, COL_FORMA_PGTO_ID) = gridAgendamentos.TextMatrix(currentGridRow, 7)
            .Cells(Linha, COL_DESC_FORMA_PGTO) = gridAgendamentos.TextMatrix(currentGridRow, 8)
            .Cells(Linha, COL_DT_VENC_PARCELA) = gridAgendamentos.TextMatrix(currentGridRow, 9)
            .Cells(Linha, COL_VALOR_PARCELA) = gridAgendamentos.TextMatrix(currentGridRow, 10)
            .Cells(Linha, COL_ANO_MES_REFER) = gridAgendamentos.TextMatrix(currentGridRow, 11)
            .Cells(Linha, COL_DT_INCLUSAO) = gridAgendamentos.TextMatrix(currentGridRow, 12)
            .Cells(Linha, COL_OBSEVACAO) = gridAgendamentos.TextMatrix(currentGridRow, 13)
            .Cells(Linha, COL_STATUS) = gridAgendamentos.TextMatrix(currentGridRow, 14)
            
            currentGridRow = currentGridRow + 1
            
        Loop

        Call FormatarValoresAgendamentosInconsistentes(Linha)
        
        'IntervaloAgendamentosInconsistentes = nomePlanilha & "!" & COL_DATA_IOF & LINHA_INICIO_DADOS & ":" & COL_FATOR_COMPARATIVO_IOF & Linha

    End With
End Sub

Private Sub PreencherCabecalhoAgendamentosInconsistentes()

    With ExcelApp
        
        .Cells(LINHA_CABECALHO, COL_COD_ERRO) = "C�d. do Erro"
        .Cells(LINHA_CABECALHO, COL_DESCRICAO_LOG) = "Descri��o/Log"
        .Cells(LINHA_CABECALHO, COL_NUMERO_PARCELA) = "N�mero da Parcela"
        .Cells(LINHA_CABECALHO, COL_REMESSA) = "Remessa"
        .Cells(LINHA_CABECALHO, COL_SEQUENCIAL_REMESSA) = "Sequencial da Remessa"
        .Cells(LINHA_CABECALHO, COL_NOSSO_NUMERO) = "Nosso N�mero"
        .Cells(LINHA_CABECALHO, COL_NOSSO_NUMERO_DV) = "Nosso N�mero DV."
        .Cells(LINHA_CABECALHO, COL_FORMA_PGTO_ID) = "Forma Pgto Id"
        .Cells(LINHA_CABECALHO, COL_DESC_FORMA_PGTO) = "Descri��o da forma de pagamento"
        .Cells(LINHA_CABECALHO, COL_DT_VENC_PARCELA) = "Dt. Vencimento da parcela"
        .Cells(LINHA_CABECALHO, COL_VALOR_PARCELA) = "Valor da Parcela"
        .Cells(LINHA_CABECALHO, COL_ANO_MES_REFER) = "Ano/M�s de Refer�ncia"
        .Cells(LINHA_CABECALHO, COL_DT_INCLUSAO) = "Data de Inclus�o"
        .Cells(LINHA_CABECALHO, COL_OBSEVACAO) = "Observa��o"
        .Cells(LINHA_CABECALHO, COL_STATUS) = "Status (Tratado = R / N�o tratado = E)"

    End With
    
    Formatacao.FormatarCabecalho (COL_COD_ERRO & LINHA_CABECALHO & ":" & COL_DESCRICAO_LOG & LINHA_CABECALHO & ":" & COL_NUMERO_PARCELA & LINHA_CABECALHO & ":" & COL_REMESSA & LINHA_CABECALHO & ":" & COL_SEQUENCIAL_REMESSA & LINHA_CABECALHO & ":" & COL_NOSSO_NUMERO & LINHA_CABECALHO & ":" & COL_NOSSO_NUMERO_DV & LINHA_CABECALHO & ":" & COL_FORMA_PGTO_ID & LINHA_CABECALHO & ":" & COL_DESC_FORMA_PGTO & LINHA_CABECALHO & ":" & COL_DT_VENC_PARCELA & LINHA_CABECALHO & ":" & COL_VALOR_PARCELA & LINHA_CABECALHO & ":" & COL_ANO_MES_REFER & LINHA_CABECALHO & ":" & COL_DT_INCLUSAO & LINHA_CABECALHO & ":" & COL_OBSEVACAO & LINHA_CABECALHO & ":" & COL_STATUS & LINHA_CABECALHO)
    
    ExcelApp.Columns(COL_DESCRICAO_LOG & ":" & COL_DESCRICAO_LOG).EntireColumn.AutoFit
    ExcelApp.Columns(COL_DESC_FORMA_PGTO & ":" & COL_DESC_FORMA_PGTO).EntireColumn.AutoFit
    ExcelApp.Columns(COL_OBSEVACAO & ":" & COL_OBSEVACAO).EntireColumn.AutoFit
    ExcelApp.Columns(COL_STATUS & ":" & COL_STATUS).EntireColumn.AutoFit
    

End Sub

Private Sub FormatarValoresAgendamentosInconsistentes(ByVal Linha As Long)

    With ExcelApp
        
        .Columns(COL_COD_ERRO).NumberFormat = FormatoNumeroInteiro
        '.Columns(COL_DESCRICAO_LOG) = "Descri��o/Log"
        .Columns(COL_NUMERO_PARCELA).NumberFormat = FormatoNumeroInteiro
        .Columns(COL_REMESSA).NumberFormat = FormatoNumeroInteiro
        .Columns(COL_SEQUENCIAL_REMESSA).NumberFormat = FormatoNumeroInteiro
        .Columns(COL_NOSSO_NUMERO).NumberFormat = FormatoNumeroInteiro
        .Columns(COL_NOSSO_NUMERO_DV).NumberFormat = FormatoNumeroInteiro
        .Columns(COL_FORMA_PGTO_ID).NumberFormat = FormatoNumeroInteiro
        '.Columns(COL_DESC_FORMA_PGTO) = "Descri��o da forma de pagamento"
        .Columns(COL_DT_VENC_PARCELA).NumberFormat = FormatoData
        .Columns(COL_VALOR_PARCELA).NumberFormat = FormatoMoeda
        '.Columns(COL_ANO_MES_REFER).NumberFormat = FormatoMesAno
        .Columns(COL_DT_INCLUSAO).NumberFormat = FormatoData
        '.Columns(COL_OBSEVACAO) = "Observa��o"
        '.Columns(COL_STATUS) = "Status"

    End With
    
    Formatacao.FormatarDados (COL_DT_VENC_PARCELA & Linha & ":" & COL_DT_INCLUSAO & Linha)

End Sub

Public Function ObterIntervaloAgendamentosInconsistentes() As String
    ObterIntervaloAgendamentosInconsistentes = IntervaloAgendamentosInconsistentes
End Function
