VERSION 5.00
Object = "{C932BA88-4374-101B-A56C-00AA003668DC}#1.1#0"; "msmask32.ocx"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "msflxgrd.ocx"
Begin VB.Form frmEndosso 
   BorderStyle     =   0  'None
   Caption         =   "Form1"
   ClientHeight    =   10965
   ClientLeft      =   1875
   ClientTop       =   180
   ClientWidth     =   11160
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   ScaleHeight     =   10965
   ScaleWidth      =   11160
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Visible         =   0   'False
   Begin VB.Frame fraExcConjuge 
      Caption         =   "Dados Segurados"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2175
      Left            =   150
      TabIndex        =   185
      Top             =   8640
      Visible         =   0   'False
      Width           =   10755
      Begin MSFlexGridLib.MSFlexGrid gridDadosSegurados 
         Height          =   1425
         Left            =   240
         TabIndex        =   186
         Top             =   360
         Width           =   8355
         _ExtentX        =   14737
         _ExtentY        =   2514
         _Version        =   393216
         Cols            =   3
         FixedCols       =   0
         AllowUserResizing=   1
         FormatString    =   $"frmEndosso.frx":0000
      End
   End
   Begin VB.CommandButton cmdFechar 
      Caption         =   "Fechar"
      Height          =   405
      Left            =   9120
      TabIndex        =   0
      Top             =   8640
      Width           =   1785
   End
   Begin VB.Frame Frame1 
      Caption         =   "Dados do Endosso "
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3885
      Left            =   150
      TabIndex        =   1
      Top             =   90
      Width           =   10755
      Begin VB.TextBox txtUsuarioEmi 
         Height          =   315
         Left            =   5490
         Locked          =   -1  'True
         TabIndex        =   183
         Top             =   450
         Width           =   2835
      End
      Begin VB.TextBox mskDtRetornoBB 
         Height          =   315
         Left            =   4470
         TabIndex        =   181
         Top             =   1620
         Width           =   1215
      End
      Begin VB.TextBox mskDtFimVigencia 
         Height          =   315
         Left            =   2370
         TabIndex        =   180
         Top             =   450
         Width           =   1215
      End
      Begin VB.TextBox mskDtEmissao 
         Height          =   315
         Left            =   3960
         TabIndex        =   179
         Top             =   450
         Width           =   1215
      End
      Begin VB.TextBox txtEndossoId 
         Height          =   315
         Left            =   150
         Locked          =   -1  'True
         TabIndex        =   173
         Top             =   1620
         Width           =   1875
      End
      Begin VB.TextBox txtDescEndosso 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1425
         Left            =   150
         Locked          =   -1  'True
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   5
         Top             =   2220
         Width           =   10155
      End
      Begin VB.TextBox txtUsuario 
         Height          =   315
         Left            =   6060
         Locked          =   -1  'True
         TabIndex        =   4
         Top             =   1620
         Width           =   2895
      End
      Begin VB.TextBox txtNumEndossoBB 
         Height          =   315
         Left            =   2280
         Locked          =   -1  'True
         TabIndex        =   3
         Top             =   1620
         Width           =   1875
      End
      Begin VB.TextBox txtTpEndosso 
         Height          =   315
         Left            =   150
         Locked          =   -1  'True
         TabIndex        =   2
         Top             =   1020
         Width           =   10155
      End
      Begin MSMask.MaskEdBox mskDtEndosso 
         Height          =   315
         Left            =   150
         TabIndex        =   6
         Top             =   450
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   556
         _Version        =   393216
         AutoTab         =   -1  'True
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         PromptChar      =   "_"
      End
      Begin VB.Label lbl 
         Caption         =   "Usu�rio Emiss�o:"
         Height          =   285
         Index           =   5
         Left            =   5505
         TabIndex        =   182
         Top             =   225
         Width           =   1545
      End
      Begin VB.Label lbl 
         AutoSize        =   -1  'True
         Caption         =   "N�mero Endosso AB:"
         Height          =   195
         Index           =   4
         Left            =   150
         TabIndex        =   174
         Top             =   1410
         Width           =   1515
      End
      Begin VB.Label lbl 
         AutoSize        =   -1  'True
         Caption         =   "Texto do Endosso:"
         Height          =   195
         Index           =   80
         Left            =   150
         TabIndex        =   14
         Top             =   2010
         Width           =   1380
      End
      Begin VB.Label lbl 
         AutoSize        =   -1  'True
         Caption         =   "Usu�rio:"
         Height          =   195
         Index           =   3
         Left            =   6060
         TabIndex        =   13
         Top             =   1410
         Width           =   585
      End
      Begin VB.Label lbl 
         AutoSize        =   -1  'True
         Caption         =   "Data Retorno BB:"
         Height          =   195
         Index           =   2
         Left            =   4470
         TabIndex        =   12
         Top             =   1410
         Width           =   1260
      End
      Begin VB.Label lbl 
         AutoSize        =   -1  'True
         Caption         =   "N�mero Endosso BB:"
         Height          =   195
         Index           =   1
         Left            =   2280
         TabIndex        =   11
         Top             =   1410
         Width           =   1515
      End
      Begin VB.Label lbl 
         AutoSize        =   -1  'True
         Caption         =   "Tipo de Endosso:"
         Height          =   195
         Index           =   0
         Left            =   150
         TabIndex        =   10
         Top             =   810
         Width           =   1245
      End
      Begin VB.Label lbl 
         AutoSize        =   -1  'True
         Caption         =   "In�cio de Vig�ncia / Pedido:"
         Height          =   195
         Index           =   75
         Left            =   150
         TabIndex        =   9
         Top             =   210
         Width           =   1995
      End
      Begin VB.Label lbl 
         AutoSize        =   -1  'True
         Caption         =   "Emiss�o:"
         Height          =   195
         Index           =   86
         Left            =   3960
         TabIndex        =   8
         Top             =   210
         Width           =   630
      End
      Begin VB.Label lblFimVig 
         AutoSize        =   -1  'True
         Caption         =   "Fim de Vig�ncia:"
         Height          =   195
         Left            =   2370
         TabIndex        =   7
         Top             =   210
         Width           =   1170
      End
   End
   Begin VB.Frame fraSuspensao 
      Caption         =   "Suspens�o "
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2175
      Left            =   150
      TabIndex        =   15
      Top             =   7590
      Visible         =   0   'False
      Width           =   10755
      Begin VB.TextBox txtIndAgendamento 
         Height          =   315
         Left            =   180
         Locked          =   -1  'True
         TabIndex        =   17
         Top             =   1230
         Width           =   1455
      End
      Begin VB.TextBox txtCodSuspensao 
         Height          =   315
         Left            =   2130
         Locked          =   -1  'True
         TabIndex        =   16
         Top             =   1230
         Width           =   1455
      End
      Begin MSMask.MaskEdBox mskInicioSuspensao 
         Height          =   315
         Left            =   180
         TabIndex        =   18
         Top             =   450
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   556
         _Version        =   393216
         AutoTab         =   -1  'True
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         PromptChar      =   "_"
      End
      Begin MSMask.MaskEdBox mskFimSuspensao 
         Height          =   315
         Left            =   2130
         TabIndex        =   19
         Top             =   450
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   556
         _Version        =   393216
         AutoTab         =   -1  'True
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         PromptChar      =   "_"
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "In�cio Suspens�o :"
         Height          =   195
         Index           =   6
         Left            =   180
         TabIndex        =   23
         Top             =   240
         Width           =   1335
      End
      Begin VB.Label Label3 
         AutoSize        =   -1  'True
         Caption         =   "Fim Suspens�o :"
         Height          =   195
         Index           =   5
         Left            =   2130
         TabIndex        =   22
         Top             =   240
         Width           =   1170
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "Indicador Agendamento: "
         Height          =   195
         Index           =   7
         Left            =   180
         TabIndex        =   21
         Top             =   990
         Width           =   1785
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "C�digo da Suspens�o:"
         Height          =   195
         Index           =   8
         Left            =   2130
         TabIndex        =   20
         Top             =   990
         Width           =   1605
      End
   End
   Begin VB.Frame fraDadosCadastrais 
      Caption         =   "Dados Cadastrais "
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3585
      Left            =   150
      TabIndex        =   64
      Top             =   3420
      Visible         =   0   'False
      Width           =   10755
      Begin VB.CheckBox chkPPE 
         Caption         =   "PPE"
         Height          =   240
         Left            =   7950
         TabIndex        =   184
         Top             =   1380
         Visible         =   0   'False
         Width           =   1140
      End
      Begin VB.TextBox TxtEmail 
         Height          =   285
         Left            =   270
         Locked          =   -1  'True
         MaxLength       =   60
         TabIndex        =   86
         Top             =   1350
         Width           =   5295
      End
      Begin VB.TextBox txtSexo 
         Height          =   285
         Left            =   4170
         Locked          =   -1  'True
         MaxLength       =   1
         TabIndex        =   85
         Top             =   840
         Visible         =   0   'False
         Width           =   375
      End
      Begin VB.TextBox txtEstCivilAnt 
         Height          =   285
         Left            =   5760
         Locked          =   -1  'True
         MaxLength       =   2
         TabIndex        =   84
         Top             =   840
         Visible         =   0   'False
         Width           =   375
      End
      Begin VB.TextBox txtSexoDesc 
         BackColor       =   &H8000000F&
         Height          =   285
         Left            =   3900
         Locked          =   -1  'True
         MaxLength       =   10
         TabIndex        =   83
         Top             =   840
         Width           =   1635
      End
      Begin VB.TextBox TxtBairro 
         Height          =   285
         Left            =   6570
         Locked          =   -1  'True
         MaxLength       =   50
         TabIndex        =   82
         Top             =   1890
         Width           =   3015
      End
      Begin VB.TextBox txtIdTpPessoa 
         Height          =   285
         Left            =   270
         Locked          =   -1  'True
         MaxLength       =   3
         TabIndex        =   81
         Top             =   840
         Visible         =   0   'False
         Width           =   315
      End
      Begin VB.TextBox TxtNomeBanco 
         BackColor       =   &H80000004&
         Enabled         =   0   'False
         Height          =   285
         Left            =   1500
         Locked          =   -1  'True
         TabIndex        =   80
         Top             =   2790
         Width           =   2055
      End
      Begin VB.TextBox TxtNomeAg 
         BackColor       =   &H80000004&
         Enabled         =   0   'False
         Height          =   285
         Left            =   5190
         Locked          =   -1  'True
         TabIndex        =   79
         Top             =   2790
         Width           =   2175
      End
      Begin VB.TextBox TxtCC 
         Height          =   285
         Left            =   8010
         Locked          =   -1  'True
         MaxLength       =   11
         TabIndex        =   78
         Top             =   2790
         Width           =   1575
      End
      Begin VB.TextBox TxtBanco 
         Height          =   285
         Left            =   840
         Locked          =   -1  'True
         MaxLength       =   3
         TabIndex        =   77
         Top             =   2790
         Width           =   615
      End
      Begin VB.TextBox TxtCodAg 
         Height          =   285
         Left            =   4500
         Locked          =   -1  'True
         MaxLength       =   5
         TabIndex        =   76
         Top             =   2790
         Width           =   615
      End
      Begin VB.TextBox txtNome 
         Height          =   285
         Left            =   3390
         Locked          =   -1  'True
         MaxLength       =   60
         TabIndex        =   75
         Top             =   270
         Width           =   6225
      End
      Begin VB.TextBox TxtEndereco 
         Height          =   285
         Left            =   270
         Locked          =   -1  'True
         MaxLength       =   50
         TabIndex        =   74
         Top             =   1890
         Width           =   6105
      End
      Begin VB.TextBox TxtCidade 
         Height          =   285
         Left            =   270
         Locked          =   -1  'True
         MaxLength       =   30
         TabIndex        =   73
         Top             =   2400
         Width           =   6120
      End
      Begin VB.TextBox TxtUF 
         Height          =   285
         Left            =   6570
         Locked          =   -1  'True
         MaxLength       =   2
         TabIndex        =   72
         Top             =   2400
         Width           =   375
      End
      Begin VB.TextBox TxtCEP 
         Height          =   285
         Left            =   7680
         Locked          =   -1  'True
         MaxLength       =   10
         TabIndex        =   71
         Top             =   2400
         Width           =   1905
      End
      Begin VB.TextBox TxtDDD 
         Height          =   285
         Left            =   5760
         Locked          =   -1  'True
         MaxLength       =   4
         TabIndex        =   70
         Top             =   1350
         Width           =   615
      End
      Begin VB.TextBox TxtTelefone 
         Height          =   285
         Left            =   6570
         Locked          =   -1  'True
         MaxLength       =   9
         TabIndex        =   69
         Top             =   1350
         Width           =   1215
      End
      Begin VB.TextBox txtClienteId 
         BackColor       =   &H80000003&
         Enabled         =   0   'False
         Height          =   285
         Left            =   1380
         Locked          =   -1  'True
         TabIndex        =   68
         Top             =   270
         Width           =   1335
      End
      Begin VB.TextBox txtCPFCNPJ 
         Height          =   285
         Left            =   1830
         Locked          =   -1  'True
         TabIndex        =   67
         Top             =   840
         Width           =   1875
      End
      Begin VB.TextBox txtTpPessoa 
         BackColor       =   &H80000004&
         Height          =   285
         Left            =   270
         Locked          =   -1  'True
         TabIndex        =   66
         Top             =   840
         Width           =   1335
      End
      Begin VB.TextBox txtEstCivilDesc 
         BackColor       =   &H80000004&
         Enabled         =   0   'False
         Height          =   285
         Left            =   6150
         Locked          =   -1  'True
         TabIndex        =   65
         Top             =   840
         Width           =   1635
      End
      Begin MSMask.MaskEdBox txtNasc 
         Height          =   285
         Left            =   7980
         TabIndex        =   87
         Top             =   840
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   503
         _Version        =   393216
         AutoTab         =   -1  'True
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         PromptChar      =   "_"
      End
      Begin VB.Label lblCad 
         AutoSize        =   -1  'True
         Caption         =   "E-mail :"
         Height          =   195
         Index           =   26
         Left            =   270
         TabIndex        =   105
         Top             =   1140
         Width           =   510
      End
      Begin VB.Label lblCad 
         AutoSize        =   -1  'True
         Caption         =   "Estado Civil :"
         Height          =   195
         Index           =   29
         Left            =   5760
         TabIndex        =   104
         Top             =   600
         Width           =   915
      End
      Begin VB.Label lblCad 
         AutoSize        =   -1  'True
         Caption         =   "Sexo :"
         Height          =   195
         Index           =   28
         Left            =   3900
         TabIndex        =   103
         Top             =   600
         Width           =   450
      End
      Begin VB.Label lblCad 
         AutoSize        =   -1  'True
         Caption         =   "Bairro :"
         Height          =   195
         Index           =   3
         Left            =   6570
         TabIndex        =   102
         Top             =   1680
         Width           =   495
      End
      Begin VB.Label lblCad 
         AutoSize        =   -1  'True
         Caption         =   "Ag. D�bito :"
         Height          =   195
         Index           =   11
         Left            =   3600
         TabIndex        =   101
         Top             =   2820
         Width           =   840
      End
      Begin VB.Label lblCad 
         AutoSize        =   -1  'True
         Caption         =   "Conta :"
         Height          =   195
         Index           =   12
         Left            =   7440
         TabIndex        =   100
         Top             =   2820
         Width           =   510
      End
      Begin VB.Label lblCad 
         AutoSize        =   -1  'True
         Caption         =   "Banco :"
         Height          =   195
         Index           =   10
         Left            =   270
         TabIndex        =   99
         Top             =   2820
         Width           =   555
      End
      Begin VB.Label lblCad 
         AutoSize        =   -1  'True
         Caption         =   "Nome :"
         Height          =   195
         Index           =   1
         Left            =   2790
         TabIndex        =   98
         Top             =   300
         Width           =   510
      End
      Begin VB.Label lblCad 
         AutoSize        =   -1  'True
         Caption         =   "CPF / CNPJ :"
         Height          =   195
         Index           =   27
         Left            =   1830
         TabIndex        =   97
         Top             =   600
         Width           =   960
      End
      Begin VB.Label lblCad 
         AutoSize        =   -1  'True
         Caption         =   "Data do Nascimento :"
         Height          =   195
         Index           =   30
         Left            =   7980
         TabIndex        =   96
         Top             =   600
         Width           =   1545
      End
      Begin VB.Label lblCad 
         AutoSize        =   -1  'True
         Caption         =   "Endere�o de Correspond�ncia :"
         Height          =   195
         Index           =   35
         Left            =   270
         TabIndex        =   95
         Top             =   1680
         Width           =   2250
      End
      Begin VB.Label lblCad 
         AutoSize        =   -1  'True
         Caption         =   "Cidade :"
         Height          =   195
         Index           =   4
         Left            =   270
         TabIndex        =   94
         Top             =   2190
         Width           =   585
      End
      Begin VB.Label lblCad 
         AutoSize        =   -1  'True
         Caption         =   "UF :"
         Height          =   195
         Index           =   7
         Left            =   6600
         TabIndex        =   93
         Top             =   2190
         Width           =   300
      End
      Begin VB.Label lblCad 
         AutoSize        =   -1  'True
         Caption         =   "CEP :"
         Height          =   195
         Index           =   8
         Left            =   7680
         TabIndex        =   92
         Top             =   2190
         Width           =   405
      End
      Begin VB.Label lblCad 
         AutoSize        =   -1  'True
         Caption         =   "DDD :"
         Height          =   195
         Index           =   5
         Left            =   5760
         TabIndex        =   91
         Top             =   1140
         Width           =   450
      End
      Begin VB.Label lblCad 
         AutoSize        =   -1  'True
         Caption         =   "Telefone :"
         Height          =   195
         Index           =   6
         Left            =   6570
         TabIndex        =   90
         Top             =   1140
         Width           =   720
      End
      Begin VB.Label lblCad 
         AutoSize        =   -1  'True
         Caption         =   "Tipo Pessoa :"
         Height          =   195
         Index           =   2
         Left            =   270
         TabIndex        =   89
         Top             =   600
         Width           =   975
      End
      Begin VB.Label lblCad 
         AutoSize        =   -1  'True
         Caption         =   "C�d. Cliente :"
         Height          =   195
         Index           =   0
         Left            =   270
         TabIndex        =   88
         Top             =   330
         Width           =   945
      End
   End
   Begin VB.Frame fraCancEndosso 
      Caption         =   "Cancelamento de Endosso "
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1785
      Left            =   150
      TabIndex        =   168
      Top             =   7560
      Visible         =   0   'False
      Width           =   10755
      Begin VB.TextBox txtDtContab 
         Height          =   315
         Left            =   3750
         Locked          =   -1  'True
         TabIndex        =   178
         Top             =   630
         Width           =   1215
      End
      Begin VB.TextBox txtDtCancelamento 
         Height          =   315
         Left            =   1950
         Locked          =   -1  'True
         TabIndex        =   177
         Top             =   630
         Width           =   1215
      End
      Begin VB.TextBox txtEndCartaEmitida 
         Height          =   315
         Left            =   5610
         Locked          =   -1  'True
         TabIndex        =   176
         Top             =   630
         Width           =   1215
      End
      Begin VB.TextBox txtEndossoCancelado 
         Height          =   315
         Left            =   300
         Locked          =   -1  'True
         TabIndex        =   175
         Top             =   630
         Width           =   1215
      End
      Begin VB.Label Label4 
         AutoSize        =   -1  'True
         Caption         =   "Endosso Cancelado: "
         Height          =   195
         Index           =   0
         Left            =   300
         TabIndex        =   172
         Top             =   420
         Width           =   1515
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "Data de Cancelamento:"
         Height          =   195
         Index           =   4
         Left            =   1950
         TabIndex        =   171
         Top             =   420
         Width           =   1680
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "Data de Contabiliza��o:"
         Height          =   195
         Index           =   5
         Left            =   3750
         TabIndex        =   170
         Top             =   420
         Width           =   1695
      End
      Begin VB.Label Label4 
         AutoSize        =   -1  'True
         Caption         =   "Carta Emitida:"
         Height          =   195
         Index           =   1
         Left            =   5610
         TabIndex        =   169
         Top             =   420
         Width           =   975
      End
   End
   Begin VB.Frame fraFinanceiro 
      Caption         =   "Dados financeiros "
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4605
      Left            =   150
      TabIndex        =   106
      Top             =   7560
      Visible         =   0   'False
      Width           =   10755
      Begin VB.TextBox txtValFinanceiro 
         Height          =   315
         Left            =   2700
         Locked          =   -1  'True
         TabIndex        =   127
         Top             =   180
         Width           =   1815
      End
      Begin VB.TextBox txtValIof 
         Height          =   315
         Left            =   2700
         Locked          =   -1  'True
         TabIndex        =   126
         Top             =   536
         Width           =   1815
      End
      Begin VB.TextBox txtValIR 
         Height          =   315
         Left            =   2700
         Locked          =   -1  'True
         TabIndex        =   125
         Top             =   892
         Width           =   1815
      End
      Begin VB.TextBox txtValAdicFracionamento 
         Height          =   315
         Left            =   2700
         Locked          =   -1  'True
         TabIndex        =   124
         Top             =   1248
         Width           =   1815
      End
      Begin VB.TextBox txtValComissao 
         Height          =   315
         Left            =   2700
         Locked          =   -1  'True
         TabIndex        =   123
         Top             =   1604
         Width           =   1815
      End
      Begin VB.TextBox txtValComissaoEstipulante 
         Height          =   315
         Left            =   2700
         Locked          =   -1  'True
         TabIndex        =   122
         Top             =   1960
         Width           =   1815
      End
      Begin VB.TextBox txtValRemuneracao 
         Height          =   315
         Left            =   8250
         Locked          =   -1  'True
         TabIndex        =   121
         Top             =   180
         Width           =   1815
      End
      Begin VB.TextBox txtValCustoAssistencia 
         Height          =   315
         Left            =   2700
         Locked          =   -1  'True
         TabIndex        =   120
         Top             =   2316
         Width           =   1815
      End
      Begin VB.TextBox txtValPremioTarifa 
         Height          =   315
         Left            =   2700
         Locked          =   -1  'True
         TabIndex        =   119
         Top             =   2672
         Width           =   1815
      End
      Begin VB.TextBox txtValDesconto 
         Height          =   315
         Left            =   8250
         Locked          =   -1  'True
         TabIndex        =   118
         Top             =   536
         Width           =   1815
      End
      Begin VB.TextBox txtValCustoApolice 
         Height          =   315
         Left            =   8250
         Locked          =   -1  'True
         TabIndex        =   117
         Top             =   892
         Width           =   1815
      End
      Begin VB.TextBox txtValIS 
         Height          =   315
         Left            =   2700
         Locked          =   -1  'True
         TabIndex        =   116
         Top             =   3030
         Width           =   1815
      End
      Begin VB.TextBox txtNumParcelas 
         Height          =   315
         Left            =   8250
         Locked          =   -1  'True
         TabIndex        =   115
         Top             =   1248
         Width           =   1815
      End
      Begin VB.TextBox txtValParidade 
         Height          =   315
         Left            =   8250
         Locked          =   -1  'True
         TabIndex        =   114
         Top             =   1604
         Width           =   1815
      End
      Begin VB.TextBox txtQtdVidas 
         Height          =   315
         Left            =   8250
         Locked          =   -1  'True
         TabIndex        =   113
         Top             =   1950
         Width           =   1815
      End
      Begin VB.TextBox txtSituacaoResseguro 
         Height          =   315
         Left            =   8250
         Locked          =   -1  'True
         TabIndex        =   112
         Top             =   2310
         Width           =   1815
      End
      Begin VB.TextBox txtDtContabEmissao 
         Height          =   315
         Left            =   8250
         Locked          =   -1  'True
         TabIndex        =   111
         Top             =   2670
         Width           =   1815
      End
      Begin VB.TextBox txtDtPgto 
         Height          =   315
         Left            =   8250
         Locked          =   -1  'True
         TabIndex        =   110
         Top             =   3030
         Width           =   1815
      End
      Begin VB.TextBox txtMoedaSeguro 
         Height          =   315
         Left            =   8250
         Locked          =   -1  'True
         TabIndex        =   109
         Top             =   3390
         Width           =   1815
      End
      Begin VB.TextBox txtMoedaPremio 
         Height          =   315
         Left            =   8250
         Locked          =   -1  'True
         TabIndex        =   108
         Top             =   3750
         Width           =   1815
      End
      Begin VB.TextBox txtFormaPgto 
         Height          =   315
         Left            =   8250
         Locked          =   -1  'True
         TabIndex        =   107
         Top             =   4110
         Width           =   1815
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "IR ................................................"
         Height          =   195
         Index           =   2
         Left            =   180
         TabIndex        =   146
         Top             =   945
         Width           =   2370
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Adic. Fracionamento ..................."
         Height          =   195
         Index           =   3
         Left            =   180
         TabIndex        =   145
         Top             =   1305
         Width           =   2355
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Comiss�o ...................................."
         Height          =   195
         Index           =   4
         Left            =   180
         TabIndex        =   144
         Top             =   1665
         Width           =   2340
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Comiss�o Estipulante .................."
         Height          =   195
         Index           =   5
         Left            =   180
         TabIndex        =   143
         Top             =   2025
         Width           =   2355
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Valor Remunera��o .................................."
         Height          =   195
         Index           =   6
         Left            =   5040
         TabIndex        =   142
         Top             =   240
         Width           =   2985
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Custo Assist�ncia ........................"
         Height          =   195
         Index           =   7
         Left            =   180
         TabIndex        =   141
         Top             =   2370
         Width           =   2370
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Pr�mio Tarifa ..............................."
         Height          =   195
         Index           =   8
         Left            =   180
         TabIndex        =   140
         Top             =   2730
         Width           =   2370
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Desconto Comercial .................................."
         Height          =   195
         Index           =   9
         Left            =   5040
         TabIndex        =   139
         Top             =   600
         Width           =   3000
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Custo Ap�lice ............................................"
         Height          =   195
         Index           =   10
         Left            =   5040
         TabIndex        =   138
         Top             =   945
         Width           =   3000
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Import�ncia Segurada .................."
         Height          =   195
         Index           =   11
         Left            =   180
         TabIndex        =   137
         Top             =   3090
         Width           =   2415
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "N�mero de parcelas .................................."
         Height          =   195
         Index           =   12
         Left            =   5040
         TabIndex        =   136
         Top             =   1305
         Width           =   3000
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Valor de paridade ......................................"
         Height          =   195
         Index           =   13
         Left            =   5040
         TabIndex        =   135
         Top             =   1665
         Width           =   3000
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Qtd. Vidas ................................................."
         Height          =   195
         Index           =   14
         Left            =   5040
         TabIndex        =   134
         Top             =   2010
         Width           =   2985
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Situa��o Resseguro .................................."
         Height          =   195
         Index           =   15
         Left            =   5040
         TabIndex        =   133
         Top             =   2370
         Width           =   3015
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Data Contabiliza��o Emiss�o......................"
         Height          =   195
         Index           =   16
         Left            =   5040
         TabIndex        =   132
         Top             =   2730
         Width           =   3045
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Data Pagamento ........................................"
         Height          =   195
         Index           =   17
         Left            =   5040
         TabIndex        =   131
         Top             =   3090
         Width           =   3045
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Moeda do Seguro ......................................"
         Height          =   195
         Index           =   18
         Left            =   5040
         TabIndex        =   130
         Top             =   3450
         Width           =   3030
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Moeda do Pr�mio ......................................."
         Height          =   195
         Index           =   19
         Left            =   5040
         TabIndex        =   129
         Top             =   3810
         Width           =   3045
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Forma de pagamento .................................."
         Height          =   195
         Index           =   20
         Left            =   5040
         TabIndex        =   128
         Top             =   4170
         Width           =   3075
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Valor Financeiro .........................."
         Height          =   195
         Index           =   0
         Left            =   180
         TabIndex        =   148
         Top             =   300
         Width           =   2355
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "IOF ............................................."
         Height          =   195
         Index           =   1
         Left            =   180
         TabIndex        =   147
         Top             =   630
         Width           =   2325
      End
   End
   Begin VB.Frame fraReenquadramento 
      Caption         =   "Reenquadramento "
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3045
      Left            =   150
      TabIndex        =   43
      Top             =   7560
      Width           =   10755
      Begin VB.TextBox txtPlanoAnt 
         Height          =   315
         Left            =   7050
         Locked          =   -1  'True
         TabIndex        =   53
         Top             =   1125
         Width           =   2625
      End
      Begin VB.TextBox txtPlanoAtual 
         Height          =   315
         Left            =   2430
         Locked          =   -1  'True
         TabIndex        =   52
         Top             =   1125
         Width           =   2625
      End
      Begin VB.TextBox txtReenqPremioAnt 
         Height          =   315
         Left            =   7740
         Locked          =   -1  'True
         TabIndex        =   51
         Top             =   1470
         Width           =   1935
      End
      Begin VB.TextBox txtReenqISAnt 
         Height          =   315
         Left            =   7740
         Locked          =   -1  'True
         TabIndex        =   50
         Top             =   1830
         Width           =   1935
      End
      Begin VB.TextBox txtReenqCustoAssistAnt 
         Height          =   315
         Left            =   7740
         Locked          =   -1  'True
         TabIndex        =   49
         Top             =   2175
         Width           =   1935
      End
      Begin VB.TextBox txtReenqPremioReaj 
         Height          =   315
         Left            =   3120
         Locked          =   -1  'True
         TabIndex        =   48
         Top             =   1470
         Width           =   1935
      End
      Begin VB.TextBox txtReenqISReaj 
         Height          =   315
         Left            =   3120
         Locked          =   -1  'True
         TabIndex        =   47
         Top             =   1830
         Width           =   1935
      End
      Begin VB.TextBox txtReenqCustoAssistReaj 
         Height          =   315
         Left            =   3120
         Locked          =   -1  'True
         TabIndex        =   46
         Top             =   2175
         Width           =   1935
      End
      Begin VB.TextBox txtInicioVigenciaPlano 
         Height          =   315
         Left            =   3780
         Locked          =   -1  'True
         TabIndex        =   45
         Top             =   780
         Width           =   1275
      End
      Begin VB.TextBox txtFatorRenov 
         Height          =   315
         Left            =   3780
         Locked          =   -1  'True
         TabIndex        =   44
         Top             =   420
         Width           =   1275
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Plano Anterior ................."
         Height          =   195
         Index           =   30
         Left            =   5220
         TabIndex        =   63
         Top             =   1245
         Width           =   1800
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Plano Atual ....................."
         Height          =   195
         Index           =   31
         Left            =   600
         TabIndex        =   62
         Top             =   1245
         Width           =   1800
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Pr�mio anterior ............................"
         Height          =   195
         Index           =   32
         Left            =   5220
         TabIndex        =   61
         Top             =   1590
         Width           =   2355
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Import�ncia Segurada anterior ...."
         Height          =   195
         Index           =   33
         Left            =   5220
         TabIndex        =   60
         Top             =   1950
         Width           =   2355
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Custo Assist�ncia anterior ............"
         Height          =   195
         Index           =   34
         Left            =   5220
         TabIndex        =   59
         Top             =   2295
         Width           =   2400
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Pr�mio reajustado ........................"
         Height          =   195
         Index           =   35
         Left            =   600
         TabIndex        =   58
         Top             =   1590
         Width           =   2385
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Import�ncia Segurada reajustada ."
         Height          =   195
         Index           =   36
         Left            =   600
         TabIndex        =   57
         Top             =   1950
         Width           =   2430
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Custo Assist�ncia reajustado ........"
         Height          =   195
         Index           =   37
         Left            =   600
         TabIndex        =   56
         Top             =   2295
         Width           =   2430
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "In�cio Vig�ncia Plano .................................."
         Height          =   195
         Index           =   38
         Left            =   600
         TabIndex        =   55
         Top             =   900
         Width           =   3090
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Fator de renova��o ...................................."
         Height          =   195
         Index           =   39
         Left            =   600
         TabIndex        =   54
         Top             =   540
         Width           =   3060
      End
   End
   Begin VB.Frame fraReajuste 
      Caption         =   "Reajuste "
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3165
      Left            =   150
      TabIndex        =   24
      Top             =   7560
      Visible         =   0   'False
      Width           =   10755
      Begin VB.TextBox txtPercReajuste 
         Height          =   315
         Left            =   2730
         Locked          =   -1  'True
         TabIndex        =   33
         Top             =   690
         Width           =   1815
      End
      Begin VB.TextBox txtPremioAnt 
         Height          =   315
         Left            =   2730
         Locked          =   -1  'True
         TabIndex        =   32
         Top             =   1410
         Width           =   1815
      End
      Begin VB.TextBox txtISAnt 
         Height          =   315
         Left            =   2730
         Locked          =   -1  'True
         TabIndex        =   31
         Top             =   1785
         Width           =   1815
      End
      Begin VB.TextBox txtCustoAssistAnt 
         Height          =   315
         Left            =   2730
         Locked          =   -1  'True
         TabIndex        =   30
         Top             =   2130
         Width           =   1815
      End
      Begin VB.TextBox txtPremioReaj 
         Height          =   315
         Left            =   7560
         Locked          =   -1  'True
         TabIndex        =   29
         Top             =   1410
         Width           =   1815
      End
      Begin VB.TextBox txtISReaj 
         Height          =   315
         Left            =   7560
         Locked          =   -1  'True
         TabIndex        =   28
         Top             =   1770
         Width           =   1815
      End
      Begin VB.TextBox txtCustoAssistReaj 
         Height          =   315
         Left            =   7560
         Locked          =   -1  'True
         TabIndex        =   27
         Top             =   2115
         Width           =   1815
      End
      Begin VB.TextBox txtDtImpressao 
         Height          =   315
         Left            =   2730
         Locked          =   -1  'True
         TabIndex        =   26
         Top             =   1050
         Width           =   1815
      End
      Begin VB.TextBox txtIndiceReajuste 
         Height          =   315
         Left            =   2730
         Locked          =   -1  'True
         TabIndex        =   25
         Top             =   330
         Width           =   2865
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Percentual de Reajuste .............."
         Height          =   195
         Index           =   21
         Left            =   210
         TabIndex        =   42
         Top             =   810
         Width           =   2340
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Pr�mio anterior ............................"
         Height          =   195
         Index           =   23
         Left            =   210
         TabIndex        =   41
         Top             =   1545
         Width           =   2355
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Import�ncia Segurada anterior ...."
         Height          =   195
         Index           =   24
         Left            =   210
         TabIndex        =   40
         Top             =   1905
         Width           =   2355
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Custo Assist�ncia anterior ............"
         Height          =   195
         Index           =   25
         Left            =   210
         TabIndex        =   39
         Top             =   2250
         Width           =   2400
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Pr�mio reajustado ........................"
         Height          =   195
         Index           =   26
         Left            =   5040
         TabIndex        =   38
         Top             =   1530
         Width           =   2385
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Import�ncia Segurada reajustada ."
         Height          =   195
         Index           =   27
         Left            =   5040
         TabIndex        =   37
         Top             =   1890
         Width           =   2430
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Custo Assist�ncia reajustado ........"
         Height          =   195
         Index           =   28
         Left            =   5040
         TabIndex        =   36
         Top             =   2235
         Width           =   2430
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Data de impress�o ....................."
         Height          =   195
         Index           =   29
         Left            =   210
         TabIndex        =   35
         Top             =   1170
         Width           =   2310
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "�ndice de Reajuste ....................."
         Height          =   195
         Index           =   22
         Left            =   210
         TabIndex        =   34
         Top             =   450
         Width           =   2325
      End
   End
   Begin VB.Frame fraCancProposta 
      Caption         =   "Cancelamento de proposta "
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3285
      Left            =   150
      TabIndex        =   149
      Top             =   6450
      Visible         =   0   'False
      Width           =   10755
      Begin VB.TextBox txtCancMotivoCancelamento 
         Height          =   315
         Left            =   210
         Locked          =   -1  'True
         TabIndex        =   153
         Top             =   1260
         Width           =   7845
      End
      Begin VB.TextBox txtCancTpCancelamento 
         Height          =   315
         Left            =   210
         Locked          =   -1  'True
         TabIndex        =   152
         Top             =   1830
         Width           =   4065
      End
      Begin VB.TextBox txtCancCartaEmitida 
         Height          =   315
         Left            =   4350
         Locked          =   -1  'True
         TabIndex        =   151
         Top             =   1830
         Width           =   1305
      End
      Begin VB.TextBox txtCancCanalAtend 
         Height          =   315
         Left            =   5730
         Locked          =   -1  'True
         TabIndex        =   150
         Top             =   1830
         Width           =   2325
      End
      Begin MSMask.MaskEdBox mskCancInicioCancelamento 
         Height          =   315
         Left            =   210
         TabIndex        =   154
         Top             =   630
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   556
         _Version        =   393216
         AutoTab         =   -1  'True
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         PromptChar      =   "_"
      End
      Begin MSMask.MaskEdBox mskCancFimCancelamento 
         Height          =   315
         Left            =   2100
         TabIndex        =   155
         Top             =   630
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   556
         _Version        =   393216
         AutoTab         =   -1  'True
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         PromptChar      =   "_"
      End
      Begin MSMask.MaskEdBox mskCancDtCancBB 
         Height          =   315
         Left            =   210
         TabIndex        =   156
         Top             =   2490
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   556
         _Version        =   393216
         AutoTab         =   -1  'True
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         PromptChar      =   "_"
      End
      Begin MSMask.MaskEdBox mskCancDtContab 
         Height          =   315
         Left            =   2520
         TabIndex        =   157
         Top             =   2490
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   556
         _Version        =   393216
         AutoTab         =   -1  'True
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         PromptChar      =   "_"
      End
      Begin MSMask.MaskEdBox mskCancDtEnvioAssist 
         Height          =   315
         Left            =   4440
         TabIndex        =   158
         Top             =   2490
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   556
         _Version        =   393216
         AutoTab         =   -1  'True
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         PromptChar      =   "_"
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "In�cio Cancelamento :"
         Height          =   195
         Index           =   0
         Left            =   210
         TabIndex        =   167
         Top             =   420
         Width           =   1560
      End
      Begin VB.Label Label3 
         AutoSize        =   -1  'True
         Caption         =   "Motivo Cancelamento  :"
         Height          =   195
         Index           =   1
         Left            =   210
         TabIndex        =   166
         Top             =   1020
         Width           =   1680
      End
      Begin VB.Label Label3 
         AutoSize        =   -1  'True
         Caption         =   "Fim Cancelamento :"
         Height          =   195
         Index           =   0
         Left            =   2100
         TabIndex        =   165
         Top             =   420
         Width           =   1395
      End
      Begin VB.Label Label3 
         AutoSize        =   -1  'True
         Caption         =   "Tipo do Cancelamento "
         Height          =   195
         Index           =   2
         Left            =   210
         TabIndex        =   164
         Top             =   1620
         Width           =   1650
      End
      Begin VB.Label Label3 
         AutoSize        =   -1  'True
         Caption         =   "Carta Emitida :"
         Height          =   195
         Index           =   3
         Left            =   4350
         TabIndex        =   163
         Top             =   1620
         Width           =   1020
      End
      Begin VB.Label Label3 
         AutoSize        =   -1  'True
         Caption         =   "Canal de Atendimento: "
         Height          =   195
         Index           =   4
         Left            =   5730
         TabIndex        =   162
         Top             =   1620
         Width           =   1650
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "Data de Cancelamento BB:"
         Height          =   195
         Index           =   1
         Left            =   210
         TabIndex        =   161
         Top             =   2280
         Width           =   1935
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "Data de Contabiliza��o:"
         Height          =   195
         Index           =   2
         Left            =   2520
         TabIndex        =   160
         Top             =   2280
         Width           =   1695
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "Data de Envio de Assist�ncia:"
         Height          =   195
         Index           =   3
         Left            =   4440
         TabIndex        =   159
         Top             =   2280
         Width           =   2130
      End
   End
   Begin VB.Line Line1 
      Visible         =   0   'False
      X1              =   150
      X2              =   10905
      Y1              =   4020
      Y2              =   4020
   End
End
Attribute VB_Name = "frmEndosso"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Demanda 19310624 - EXCLUIR O CAMPO PPE DO SEGP0176
' O objeto ChkPPE teve sua propriedade VISIBLE alterada para FALSE para atendimento da demanda.
' Optou-se por apenas inibir sua visualiza��o ao inv�s de exclui-lo definitivamente para o caso de futura reativa��o dessa funcionalidade.

Private Const TP_ENDOSSO_ENDOSSO_CADASTRAL_DE_VIDA = 1
Private Const TP_ENDOSSO_ENDOSSO_BENEFICIARIO_VIDA = 2
Private Const TP_ENDOSSO_ADICIONAL = 10
Private Const TP_ENDOSSO_PARTICIPACAO_DO_CONGENERE = 11
Private Const TP_ENDOSSO_SEM_MOVIMENTACAO_FINANCEIRA = 12
Private Const TP_ENDOSSO_ALTERACAO_CADASTRAL = 51
Private Const TP_ENDOSSO_ALTERACAO_DE_PLANO = 52
Private Const TP_ENDOSSO_EXCLUSAO_DE_BENEFICIARIO = 53
Private Const TP_ENDOSSO_INCLUSAO_DE_BENEFICIARIO = 57
Private Const TP_ENDOSSO_NAO_DEFINIDO = 58
Private Const TP_ENDOSSO_ENDOSSO_DE_DADOS_DO_BEM = 59
Private Const TP_ENDOSSO_REINTEGRACAO_DE_IS = 61
Private Const TP_ENDOSSO_CANCELAMENTO_A_PEDIDO_DO_SEGURADO = 63
Private Const TP_ENDOSSO_CANCELAMENTO_A_PEDIDO_DA_SEGURADORA = 64
Private Const TP_ENDOSSO_LIQUIDA��O_DE_CPR = 67
Private Const TP_ENDOSSO_CANCELAMENTO_SINISTRO = 68
Private Const TP_ENDOSSO_ENDOSSO_DE_COBERTURAS = 70
Private Const TP_ENDOSSO_ALTERA��O_DA_DATA_DA_VIAGEM = 74
Private Const TP_ENDOSSO_RESTITUI��O_POR_INICIATIVA_DA_SEGURADORA = 80
Private Const TP_ENDOSSO_CANCELAMENTO_POR_INADIMPLENCIA = 90
Private Const TP_ENDOSSO_CANCELAMENTO_POR_ESGOTAMENTO_DE_IS = 91
Private Const TP_ENDOSSO_CANCELAMENTO_POR_INADIMPLENCIA_PRIMEIRA_PARCELA = 92
Private Const TP_ENDOSSO_ACERTO_CONTA_MENSAL = 93
Private Const TP_ENDOSSO_ENDOSSO_DE_REDU��O_DE_IS_POR_SINISTRO = 94
Private Const TP_ENDOSSO_ENDOSSO_GENERICO_ONLINE = 99
Private Const TP_ENDOSSO_ENDOSSO_DE_FATURA = 100
Private Const TP_ENDOSSO_CANCELAMENTO_DE_ENDOSSO = 101
Private Const TP_ENDOSSO_ENDOSSO_DE_LIQUIDACAO = 104
Private Const TP_ENDOSSO_ENDOSSO_COM_MOVIMENTA��O_DE_PREMIO = 200
Private Const TP_ENDOSSO_ENDOSSO_DE_CANCELAMENTO_DE_PARCELA = 201
Private Const TP_ENDOSSO_SUSPENSAO_DE_APOLICE = 32
Private Const TP_ENDOSSO_CANCELAMENTO_SUSPENSAO_DE_APOLICE = 33
Private Const TP_ENDOSSO_ENDOSSO_DE_REENQUADRAMENTO = 202
Private Const TP_ENDOSSO_ENDOSSO_DE_REAJUSTE = 203

'Paulo Pelegrini - MU-2017-057120 - 09/11/2017 (INI)
Private Const TP_ENDOSSO_CANCELAMENTO_DE_CONJUGE = 30
Private Const TP_ENDOSSO_SINISTRO_DE_CONJUGE = 31
Private Const TP_ENDOSSO_EXCLUSAO_DE_CONJUGE = 279
Private Const TP_ENDOSSO_EXCLUSAO_DE_CONJUGE_POR_SINISTRO = 280
'Paulo Pelegrini - MU-2017-057120 - 09/11/2017 (FIM)

Private Const FRAME_ENDOSSO_CADASTRAL = 1
Private Const FRAME_ENDOSSO_FINANCEIRO = 2
Private Const FRAME_ENDOSSO_CANCELAMENTO_ENDOSSO = 3
Private Const FRAME_ENDOSSO_CANCELAMENTO_PROPOSTA = 4
Private Const FRAME_ENDOSSO_REAJUSTE = 5
Private Const FRAME_ENDOSSO_REENQUADRAMENTO = 6
Private Const FRAME_ENDOSSO_SUSPENSAO = 7
Private Const FRAME_ENDOSSO_EXCLUSAO_CONJUGE = 8 'Paulo Pelegrini - MU-2017-057120 - 09/11/2017

Dim giTpEndosso As Integer
Dim glPropostaId As Long



Private Sub cmdFechar_Click()
  Unload Me
End Sub

Private Function ObterDadosBancarios(lPropostaId As Long) As Recordset

On Error GoTo TrataErro
Dim sSql As String
    sSql = ""
    sSql = sSql & "     SELECT deb_agencia_id,                                                    " & vbNewLine
    sSql = sSql & "            deb_banco_id,                                                      " & vbNewLine
    sSql = sSql & "            banco_tb.nome banco,                                               " & vbNewLine
    sSql = sSql & "            ag_debito.nome as agencia_debito,                                  " & vbNewLine
    sSql = sSql & "            deb_conta_corrente_id                                              " & vbNewLine
    sSql = sSql & "       FROM proposta_adesao_tb  WITH (NOLOCK)                                     " & vbNewLine
    sSql = sSql & " INNER JOIN agencia_tb ag_debito  WITH (NOLOCK)                                   " & vbNewLine
    sSql = sSql & "         ON ag_debito.agencia_id = proposta_adesao_tb.deb_agencia_id           " & vbNewLine
    sSql = sSql & " INNER JOIN banco_tb  WITH (NOLOCK)                                               " & vbNewLine
    sSql = sSql & "         ON banco_tb.banco_id = proposta_adesao_tb.deb_banco_id                " & vbNewLine
    sSql = sSql & "      WHERE proposta_id =  " & lPropostaId & vbNewLine
    sSql = sSql & " UNION                                                                         " & vbNewLine
    sSql = sSql & "     SELECT proposta_fechada_tb.agencia_id deb_agencia_id,                     " & vbNewLine
    sSql = sSql & "            proposta_fechada_tb.banco_id deb_banco_id,                         " & vbNewLine
    sSql = sSql & "            banco_tb.nome banco,                                               " & vbNewLine
    sSql = sSql & "            ag_debito.nome as agencia_debito,                                  " & vbNewLine
    sSql = sSql & "            proposta_fechada_tb.deb_conta_corrente_id                          " & vbNewLine
    sSql = sSql & "       FROM proposta_fechada_tb  WITH (NOLOCK)                                    " & vbNewLine
    sSql = sSql & " INNER JOIN apolice_tb   WITH (NOLOCK)                                            " & vbNewLine
    sSql = sSql & "         ON apolice_tb.proposta_id = proposta_fechada_tb.proposta_id           " & vbNewLine
    sSql = sSql & " INNER JOIN agencia_tb ag_debito  WITH (NOLOCK)                                   " & vbNewLine
    sSql = sSql & "         ON ag_debito.agencia_id = proposta_fechada_tb.agencia_id              " & vbNewLine
    sSql = sSql & " INNER JOIN banco_tb   WITH (NOLOCK)                                              " & vbNewLine
    sSql = sSql & "         ON banco_tb.banco_id = proposta_fechada_tb.banco_id                " & vbNewLine
    sSql = sSql & "      WHERE proposta_fechada_tb.proposta_id = " & lPropostaId


    Set ObterDadosBancarios = ExecutarSQL(gsSIGLASISTEMA, _
                                          glAmbiente_id, _
                                          App.Title, _
                                          App.FileDescription, _
                                          sSql, _
                                          True)
Exit Function
TrataErro:
  Call TratarErro("ObterDadosBancarios", Me.name)
  Call FinalizarAplicacao
End Function

Private Function ObterDadosCadastrais(lPropostaId As Long) As Recordset
On Error GoTo TrataErro

  Dim sSql As String
  
    sSql = ""
    sSql = sSql & "    SELECT cliente_tb.nome,                                                                " & vbNewLine
    sSql = sSql & "           endereco_corresp_tb.endereco,                                                   " & vbNewLine
    sSql = sSql & "           endereco_corresp_tb.bairro,                                                     " & vbNewLine
    sSql = sSql & "           endereco_corresp_tb.municipio,                                                  " & vbNewLine
    sSql = sSql & "           endereco_corresp_tb.estado,                                                     " & vbNewLine
    sSql = sSql & "           endereco_corresp_tb.cep,                                                        " & vbNewLine
    sSql = sSql & "           cliente_tb.ddd_1,                                                               " & vbNewLine
    sSql = sSql & "           cliente_tb.telefone_1,                                                          " & vbNewLine
    sSql = sSql & "           cliente_tb.e_mail,                                                              " & vbNewLine
    sSql = sSql & "           cliente_tb.cliente_id,                                                          " & vbNewLine
    sSql = sSql & "           tp_pessoa = CASE                                                                " & vbNewLine
    sSql = sSql & "                        WHEN pessoa_fisica_tb.pf_cliente_id   IS NOT NULL THEN 'F�SICA'    " & vbNewLine
    sSql = sSql & "                        WHEN pessoa_juridica_tb.pj_cliente_id IS NOT NULL THEN 'JUR�DICA'  " & vbNewLine
    sSql = sSql & "                       END ,                                                               " & vbNewLine
    sSql = sSql & "           tp_pessoa_id = CASE                                                             " & vbNewLine
    sSql = sSql & "                           WHEN pessoa_fisica_tb.pf_cliente_id   IS NOT NULL THEN 1        " & vbNewLine
    sSql = sSql & "                           WHEN pessoa_juridica_tb.pj_cliente_id IS NOT NULL THEN 2        " & vbNewLine
    sSql = sSql & "                          END ,                                                            " & vbNewLine
    sSql = sSql & "                                                                                           " & vbNewLine
    sSql = sSql & "            estado_civil = ISNULL(pessoa_fisica_tb.estado_civil,''),                       " & vbNewLine
    sSql = sSql & "            sexo = ISNULL(pessoa_fisica_tb.sexo,''),                                       " & vbNewLine
    sSql = sSql & "            cpf_cnpj = ISNULL(pessoa_fisica_tb.cpf, pessoa_juridica_tb.cgc),               " & vbNewLine
    sSql = sSql & "            dt_nascimento = ISNULL(pessoa_fisica_tb.dt_nascimento,'')                      " & vbNewLine
    sSql = sSql & "       FROM proposta_tb  WITH (NOLOCK)                                                                    " & vbNewLine
    sSql = sSql & " INNER JOIN cliente_tb   WITH (NOLOCK)                                                                    " & vbNewLine
    sSql = sSql & "         ON cliente_tb.cliente_id = proposta_tb.prop_cliente_id                            " & vbNewLine
    sSql = sSql & " INNER JOIN endereco_corresp_tb  WITH (NOLOCK)                                                            " & vbNewLine
    sSql = sSql & "         ON endereco_corresp_tb.proposta_id = proposta_tb.proposta_id                      " & vbNewLine
    sSql = sSql & "  LEFT JOIN pessoa_fisica_tb  WITH (NOLOCK)                                                               " & vbNewLine
    sSql = sSql & "         ON pessoa_fisica_tb.pf_cliente_id = cliente_tb.cliente_id                         " & vbNewLine
    sSql = sSql & "  LEFT JOIN pessoa_juridica_tb  WITH (NOLOCK)                                                             " & vbNewLine
    sSql = sSql & "         ON pessoa_juridica_tb.pj_cliente_id = cliente_tb.cliente_id                       " & vbNewLine
    sSql = sSql & "      WHERE proposta_tb.proposta_id = " & lPropostaId

    Set ObterDadosCadastrais = ExecutarSQL(gsSIGLASISTEMA, _
                                           glAmbiente_id, _
                                           App.Title, _
                                           App.FileDescription, _
                                           sSql, _
                                           True)

Exit Function
TrataErro:
  Call TratarErro("ObterDadosCadastrais", Me.name)
  Call FinalizarAplicacao

End Function
Private Sub CarregarDadosCadastrais()

On Error GoTo TrataErro

  Dim rsDadosCadastrais As Recordset
  Set rsDadosCadastrais = New Recordset
  
  
  'Dados Cadastrais ''''''''''''''''''''''''''''''''''
  Set rsDadosCadastrais = ObterDadosCadastrais(glPropostaId)
  
  If rsDadosCadastrais.EOF Then
    Call MsgBox("N�o foi poss�vel obter os dados cadastrais da proposta", vbCritical)
    GoTo TrataErro
  End If
  
  txtClienteId.Text = Trim(rsDadosCadastrais("cliente_id"))
  txtNome.Text = Trim(rsDadosCadastrais("nome"))
  txtTpPessoa.Text = Trim(rsDadosCadastrais("tp_pessoa"))
  txtIdTpPessoa.Text = Trim(rsDadosCadastrais("tp_pessoa_id"))
  
  If Val(rsDadosCadastrais("tp_pessoa_id")) = 1 Then
  
    '*** Inclus�o do Sinalizado de PPE - Pessoas politicamente expostas - demanda 269994
    '*** 26/08/2008 - Fabio (Stefanini)
    If IdentificaPPE(rsDadosCadastrais("cpf_cnpj")) Then Me.chkPPE.Value = 1

    txtCPFCNPJ.Text = MasCPF(rsDadosCadastrais("cpf_cnpj"))
    txtSexoDesc.Visible = True
    txtEstCivilDesc.Visible = True
    txtNasc.Visible = True
    lblCad(28).Visible = True
    lblCad(29).Visible = True
    lblCad(30).Visible = True

    
  Else
    txtCPFCNPJ.Text = MasCGC(rsDadosCadastrais("cpf_cnpj"))
    txtSexoDesc.Visible = False
    txtEstCivilDesc.Visible = False
    txtNasc.Visible = False
    lblCad(28).Visible = False
    lblCad(29).Visible = False
    lblCad(30).Visible = False
  End If
  
  If UCase(Trim(rsDadosCadastrais("sexo"))) = "M" Then
    txtSexoDesc.Text = "MASCULINO"
  Else
    txtSexoDesc.Text = "FEMININO"
  End If
  
  txtSexo.Text = UCase(Trim(rsDadosCadastrais("sexo")))
  txtEstCivilDesc.Text = UCase(Trim(rsDadosCadastrais("estado_civil")))
  txtNasc.Text = Format(rsDadosCadastrais("dt_nascimento"), "dd/mm/yyyy")
  txtEmail.Text = IIf(IsNull(rsDadosCadastrais("e_mail")), "", rsDadosCadastrais("e_mail"))
  txtDDD.Text = Trim(rsDadosCadastrais("ddd_1"))
  
'Autor: romulo.barbosa (Nova Consultoria) - Data da Altera��o: 30/05/2012
'Demanda: 15179437 - Altera��o do campo telefone
  txtTelefone.Text = IIf(Len(rsDadosCadastrais("telefone_1") = 9), Format(rsDadosCadastrais("telefone_1"), "#####-####"), Format(rsDadosCadastrais("telefone_1"), "####-####"))
'Autor: romulo.barbosa (Nova Consultoria) - Data da Altera��o: 30/05/2012
  txtEndereco.Text = Trim(rsDadosCadastrais("endereco"))
  txtBairro.Text = Trim(rsDadosCadastrais("bairro"))
  TxtCidade.Text = Trim(rsDadosCadastrais("municipio"))
  txtUF.Text = Trim(rsDadosCadastrais("estado"))
  txtCEP.Text = MasCEP(rsDadosCadastrais("cep"))
  
  
  ' Dados Bancarios '''''''''''''''''''''''''''''''''''''''''''''''
  rsDadosCadastrais.Close
  Set rsDadosCadastrais = ObterDadosBancarios(glPropostaId)
  
  If Not rsDadosCadastrais.EOF Then
  
    TxtBanco.Text = Trim(rsDadosCadastrais("deb_banco_id"))
    TxtNomeBanco.Text = Trim(rsDadosCadastrais("banco"))
    TxtCodAg.Text = Trim(rsDadosCadastrais("deb_agencia_id"))
    TxtNomeAg.Text = Trim(rsDadosCadastrais("agencia_debito"))
    TxtCC.Text = Trim(rsDadosCadastrais("deb_conta_corrente_id"))
  
  End If
  
  rsDadosCadastrais.Close
  Set rsDadosCadastrais = Nothing

Exit Sub
TrataErro:
  Call TratarErro("CarregarDadosCadastrais", Me.name)
  Call FinalizarAplicacao

End Sub
Private Sub CarregarDadosEndosso()
On Error GoTo TrataErro

  Dim rsDados As Recordset
  
  Set rsDados = New Recordset
  Set rsDados = ObterDadosEndosso(glPropostaId, Val(txtEndossoId.Text))
  
  If rsDados.EOF Then
    Call MsgBox("N�o foi poss�vel obter dados do endosso", vbCritical)
    GoTo TrataErro
  End If
  
  mskDtEndosso.Text = Format(rsDados("dt_pedido_endosso"), "dd/mm/yyyy")
  mskDtRetornoBB.Text = IIf(Format(rsDados("dt_retorno_bb"), "dd/mm/yyyy") = "01/01/1900", "", Format(rsDados("dt_retorno_bb"), "dd/mm/yyyy"))
  txtUsuario.Text = Trim(rsDados("usuario"))
  'scasagrande 09/05/2006 usuario_emissao
  txtUsuarioEmi.Text = Trim(rsDados("usuario_emissao"))
    
  rsDados.Close
  Set rsDados = Nothing

Exit Sub
TrataErro:
  Call TratarErro("CarregarDadosEndosso", Me.name)
  Call FinalizarAplicacao

End Sub
Private Function ObterDadosEndosso(lPropostaId As Long, lEndossoId As Long) As Recordset
On Error GoTo TrataErro

  Dim sSql As String

  sSql = ""
  sSql = sSql & "SELECT dt_pedido_endosso,  " & vbNewLine
  sSql = sSql & "       ISNULL(dt_retorno_bb,'19000101') dt_retorno_bb, " & vbNewLine
  sSql = sSql & "       usuario,             " & vbNewLine
  'scasagrande 09/05/2006 usuario_emissao
  sSql = sSql & "       ISNULL(usuario_emissao,'') usuario_emissao " & vbNewLine
  sSql = sSql & "  FROM endosso_tb   WITH (NOLOCK)         " & vbNewLine
  sSql = sSql & " WHERE proposta_id =" & lPropostaId & vbNewLine
  sSql = sSql & "   AND endosso_id = " & lEndossoId & vbNewLine
  
  Set ObterDadosEndosso = ExecutarSQL(gsSIGLASISTEMA, _
                                      glAmbiente_id, _
                                      App.Title, _
                                      App.FileDescription, _
                                      sSql, _
                                      True)
                                      

Exit Function
TrataErro:
  Call TratarErro("ObterDadosEndosso", Me.name)
  Call FinalizarAplicacao

End Function

Private Function ObterDadosCancelamentoEndosso(lPropostaId As Long, lEndossoId As Long) As Recordset
On Error GoTo TrataErro
Dim sSql As String
    sSql = ""
    sSql = sSql & "SELECT canc_endosso_id,         " & vbNewLine
    sSql = sSql & "       ISNULL(dt_cancelamento,'19000101') dt_cancelamento,         " & vbNewLine
    sSql = sSql & "       ISNULL(dt_contabilizacao,'19000101') dt_contabilizacao,    " & vbNewLine
    sSql = sSql & "       carta_emitida = ''            " & vbNewLine
    sSql = sSql & "  FROM cancelamento_endosso_tb  WITH (NOLOCK)   " & vbNewLine
    sSql = sSql & " WHERE proposta_id = " & lPropostaId & vbNewLine
    sSql = sSql & "   AND endosso_id = " & lEndossoId & vbNewLine

   Set ObterDadosCancelamentoEndosso = ExecutarSQL(gsSIGLASISTEMA, _
                                                   glAmbiente_id, _
                                                   App.Title, _
                                                   App.FileDescription, _
                                                   sSql, _
                                                   True)
Exit Function
TrataErro:
  Call TratarErro("ObterDadosCancelamentoEndosso", Me.name)
  Call FinalizarAplicacao

End Function
Private Sub CarregarDadosCancelamentoEndosso()
On Error GoTo TrataErro
Dim rsDadosCancelamento As Recordset

  Set rsDadosCancelamento = New Recordset
  Set rsDadosCancelamento = ObterDadosCancelamentoEndosso(glPropostaId, Val(txtEndossoId.Text))
  
  If rsDadosCancelamento.EOF Then
    Call MsgBox("N�o foi poss�vel obter os dados do endosso de cancelamento", vbCritical)
    GoTo TrataErro
  End If
  
  txtEndossoCancelado.Text = Trim(rsDadosCancelamento("canc_endosso_id"))
  txtDtCancelamento.Text = IIf(Format(rsDadosCancelamento("dt_cancelamento"), "dd/mm/yyyy") = "01/01/1900", "", Format(rsDadosCancelamento("dt_cancelamento"), "dd/mm/yyyy"))
  txtDtContab.Text = IIf(Format(rsDadosCancelamento("dt_contabilizacao"), "dd/mm/yyyy") = "01/01/1900", "", Format(rsDadosCancelamento("dt_contabilizacao"), "dd/mm/yyyy"))
  txtEndCartaEmitida.Text = IIf(IsNull(rsDadosCancelamento("carta_emitida")), "", rsDadosCancelamento("carta_emitida"))
  
  rsDadosCancelamento.Close
  Set rsDadosCancelamento = Nothing

Exit Sub
TrataErro:
  Call TratarErro("CarregarDadosCancelamentoEndosso", Me.name)
  Call FinalizarAplicacao

End Sub
Private Function ObterDadosCancelamentoProposta(lPropostaId As Long) As Recordset

On Error GoTo TrataErro

Dim sSql As String
'JOAO.MACHADO
'17919477 - BB Cr�dito Protegido Empresa Fora do Cronograma
'ALTERACAO NO CANAL DE ATENDIMENTO

    sSql = ""
    sSql = sSql & "     SELECT cancelamento_proposta_tb.dt_inicio_cancelamento,                             " & vbNewLine
    sSql = sSql & "            ISNULL(cancelamento_proposta_tb.dt_fim_cancelamento,'19000101') dt_fim_cancelamento, " & vbNewLine
    sSql = sSql & "            ISNULL(cancelamento_proposta_tb.motivo_cancelamento,'') motivo_cancelamento, " & vbNewLine
    sSql = sSql & "            tp_cancelamento_tb.descricao,                                                " & vbNewLine
    sSql = sSql & "            cancelamento_proposta_tb.tp_cancelamento,                                    " & vbNewLine
    sSql = sSql & "            ISNULL(cancelamento_proposta_tb.carta_emitida,'') carta_emitida,             " & vbNewLine
    sSql = sSql & "            'canal_atendimento' = CASE Isnull(proposta_tb.canal_venda, '0000')           " & vbNewLine
    sSql = sSql & "                                       WHEN '0002' THEN 'Ag�ncia'                        " & vbNewLine
    sSql = sSql & "                                       WHEN '0003' THEN 'Internet'                       " & vbNewLine
    sSql = sSql & "                                       WHEN '0004' THEN 'Call Center'                    " & vbNewLine
    sSql = sSql & "                                       WHEN '0005' THEN 'Renova��o Monitorada'           " & vbNewLine
    sSql = sSql & "                                       WHEN 2 THEN 'canal de contrata��o SISBB'          " & vbNewLine
    sSql = sSql & "                                  Else 'Indefinido'                                      " & vbNewLine
    sSql = sSql & "                                  End,                                                   " & vbNewLine
    sSql = sSql & "            cancelamento_proposta_tb.dt_cancelamento_bb,                                 " & vbNewLine
    sSql = sSql & "            ISNULL(cancelamento_proposta_tb.dt_contabilizacao,'19000101') dt_contabilizacao,      " & vbNewLine
    sSql = sSql & "            ISNULL(cancelamento_proposta_tb.dt_envio_assistencia,'19000101') dt_envio_assistencia " & vbNewLine
    sSql = sSql & "       FROM cancelamento_proposta_tb  WITH (NOLOCK)                                                    " & vbNewLine
    sSql = sSql & " INNER JOIN tp_cancelamento_tb  WITH (NOLOCK)                                                           " & vbNewLine
    sSql = sSql & "         ON tp_cancelamento_tb.tp_cancelamento = cancelamento_proposta_tb.tp_cancelamento" & vbNewLine
    sSql = sSql & " INNER JOIN seguros_db..proposta_tb proposta_tb  WITH (NOLOCK)                              " & vbNewLine
    sSql = sSql & "         ON proposta_tb.proposta_id = cancelamento_proposta_tb.proposta_id               " & vbNewLine
    sSql = sSql & "      WHERE cancelamento_proposta_tb.proposta_id = " & lPropostaId
    'Demanda 19781872 - IN�CIO
    sSql = sSql & "      AND cancelamento_proposta_tb.ENDOSSO_ID = " & Val(txtEndossoId.Text)
    'Demanda 19781872 - FIM

    Set ObterDadosCancelamentoProposta = ExecutarSQL(gsSIGLASISTEMA, _
                                                     glAmbiente_id, _
                                                     App.Title, _
                                                     App.FileDescription, _
                                                     sSql, _
                                                     True)
Exit Function
TrataErro:
  Call TratarErro("ObterDadosCancelamentoProposta", Me.name)
  Call FinalizarAplicacao

End Function
Private Sub CarregarDadosCancelamentoProposta()
On Error GoTo TrataErro
Dim rsDadosCancelamentoProposta As Recordset
  Set rsDadosCancelamentoProposta = New Recordset
  Set rsDadosCancelamentoProposta = ObterDadosCancelamentoProposta(glPropostaId)
  
  If rsDadosCancelamentoProposta.EOF Then
    Call MsgBox("N�o foi poss�vel obter dados do cancelamento de proposta", vbCritical)
    Set rsDadosCancelamentoProposta = Nothing
    Exit Sub
    'GoTo TrataErro
  End If
  
  mskCancInicioCancelamento.Text = Format(rsDadosCancelamentoProposta("dt_inicio_cancelamento"), "dd/mm/yyyy")
  mskCancFimCancelamento.Mask = IIf(Format(rsDadosCancelamentoProposta("dt_fim_cancelamento"), "dd/mm/yyyy") = "01/01/1900", "", Format(rsDadosCancelamentoProposta("dt_fim_cancelamento"), "dd/mm/yyyy"))
  txtCancMotivoCancelamento.Text = Trim(rsDadosCancelamentoProposta("motivo_cancelamento"))
  txtCancTpCancelamento.Text = Trim(rsDadosCancelamentoProposta("tp_cancelamento")) & "-" & _
                               Trim(rsDadosCancelamentoProposta("descricao"))
  txtCancCartaEmitida.Text = rsDadosCancelamentoProposta("carta_emitida")
  txtCancCanalAtend.Text = IIf(IsNull(rsDadosCancelamentoProposta("canal_atendimento")), "", rsDadosCancelamentoProposta("canal_atendimento"))
  mskCancDtCancBB.Text = IIf(Format(rsDadosCancelamentoProposta("dt_cancelamento_bb"), "dd/mm/yyyy") = "01/01/1900", "", Format(rsDadosCancelamentoProposta("dt_cancelamento_bb"), "dd/mm/yyyy"))
  mskCancDtContab.Mask = IIf(Format(rsDadosCancelamentoProposta("dt_contabilizacao"), "dd/mm/yyyy") = "01/01/1900", "", Format(rsDadosCancelamentoProposta("dt_contabilizacao"), "dd/mm/yyyy"))
  mskCancDtEnvioAssist.Mask = IIf(Format(rsDadosCancelamentoProposta("dt_envio_assistencia"), "dd/mm/yyyy") = "01/01/1900", "", Format(rsDadosCancelamentoProposta("dt_envio_assistencia"), "dd/mm/yyyy"))
  
  
  rsDadosCancelamentoProposta.Close
  Set rsDadosCancelamentoProposta = Nothing

Exit Sub
TrataErro:
  Call TratarErro("CarregarDadosCancelamentoProposta", Me.name)
  Call FinalizarAplicacao

End Sub
Private Function ObterDadosFinanceiros(lPropostaId As Long, lEndossoId As Long) As Recordset
On Error GoTo TrataErro
Dim sSql As String

    sSql = ""
    sSql = sSql & "     SELECT endosso_financeiro_tb.val_financeiro,                            " & vbNewLine
    sSql = sSql & "            endosso_financeiro_tb.val_iof,                                   " & vbNewLine
    sSql = sSql & "            endosso_financeiro_tb.val_ir,                                    " & vbNewLine
    sSql = sSql & "            endosso_financeiro_tb.val_adic_fracionamento,                    " & vbNewLine
    sSql = sSql & "            endosso_financeiro_tb.val_comissao,                              " & vbNewLine
    sSql = sSql & "            endosso_financeiro_tb.val_comissao_estipulante,                  " & vbNewLine
    sSql = sSql & "            val_custo_uss = isnull(endosso_financeiro_tb.val_custo_uss,0),                             " & vbNewLine
    sSql = sSql & "            endosso_financeiro_tb.val_premio_tarifa,                         " & vbNewLine
    sSql = sSql & "            val_is = isnull(endosso_financeiro_tb.val_is,0) ,                                   " & vbNewLine
    sSql = sSql & "            val_remuneracao = isnull(endosso_financeiro_tb.val_remuneracao,0),                           " & vbNewLine
    sSql = sSql & "            endosso_financeiro_tb.val_desconto_comercial,                    " & vbNewLine
    sSql = sSql & "            endosso_financeiro_tb.custo_apolice,                             " & vbNewLine
    sSql = sSql & "            endosso_financeiro_tb.num_parcelas,                              " & vbNewLine
    sSql = sSql & "            endosso_financeiro_tb.val_paridade_moeda,                        " & vbNewLine
    sSql = sSql & "            endosso_financeiro_tb.qtd_vidas,                                 " & vbNewLine
    sSql = sSql & "            endosso_financeiro_tb.situacao_re_seguro,                        " & vbNewLine
    sSql = sSql & "            ISNULL(endosso_financeiro_tb.dt_contabilizacao_emissao,'19000101') dt_contabilizacao_emissao, " & vbNewLine
    sSql = sSql & "            ISNULL(endosso_financeiro_tb.dt_pgto,'19000101') dt_pgto,        " & vbNewLine
    sSql = sSql & "            endosso_financeiro_tb.seguro_moeda_id,                           " & vbNewLine
    sSql = sSql & "            seguro_moeda.nome seguro_moeda ,                                 " & vbNewLine
    sSql = sSql & "            endosso_financeiro_tb.premio_moeda_id,                           " & vbNewLine
    sSql = sSql & "            premio_moeda.nome premio_moeda ,                                 " & vbNewLine
    sSql = sSql & "            endosso_financeiro_tb.forma_pgto_id,                             " & vbNewLine
    sSql = sSql & "            forma_pgto_tb.nome forma_pgto                                    " & vbNewLine
    sSql = sSql & "       FROM endosso_financeiro_tb  WITH (NOLOCK)                                " & vbNewLine
    sSql = sSql & "  LEFT JOIN moeda_tb seguro_moeda   WITH (NOLOCK)                               " & vbNewLine
    sSql = sSql & "         ON seguro_moeda.moeda_id = endosso_financeiro_tb.seguro_moeda_id    " & vbNewLine
    sSql = sSql & "  LEFT JOIN moeda_tb premio_moeda    WITH (NOLOCK)                              " & vbNewLine
    sSql = sSql & "         ON premio_moeda.moeda_id = endosso_financeiro_tb.premio_moeda_id    " & vbNewLine
    sSql = sSql & "  LEFT JOIN forma_pgto_tb  WITH (NOLOCK)                                        " & vbNewLine
    sSql = sSql & "         ON forma_pgto_tb.forma_pgto_id = endosso_financeiro_tb.forma_pgto_id" & vbNewLine
    sSql = sSql & "      WHERE proposta_id =" & lPropostaId
    sSql = sSql & "        AND endosso_id  =" & lEndossoId

    Set ObterDadosFinanceiros = ExecutarSQL(gsSIGLASISTEMA, _
                                            glAmbiente_id, _
                                            App.Title, _
                                            App.FileDescription, _
                                            sSql, _
                                            True)
Exit Function
TrataErro:
  Call TratarErro("ObterDadosCadastrais", Me.name)
  Call FinalizarAplicacao

End Function
Private Sub CarregarDadosFinanceiros()
On Error GoTo TrataErro
Dim rsDadosFinanceiros As Recordset

  Set rsDadosFinanceiros = New Recordset
  Set rsDadosFinanceiros = ObterDadosFinanceiros(glPropostaId, Val(txtEndossoId.Text))
  
  If rsDadosFinanceiros.EOF Then
      Call MsgBox("N�o foi poss�vel obter os dados financeiros do endosso", vbCritical)
      rsDadosFinanceiros.Close
      Set rsDadosFinanceiros = Nothing
      Exit Sub
      'GoTo TrataErro
  End If
  
  txtValFinanceiro.Text = ConPropAdesao.FormataValorParaPadraoBrasil(rsDadosFinanceiros("val_financeiro"))
  
  If Not IsNull(rsDadosFinanceiros("val_iof")) Then
    txtValIof.Text = ConPropAdesao.FormataValorParaPadraoBrasil(rsDadosFinanceiros("val_iof"))
  End If
  
  If Not IsNull(rsDadosFinanceiros("val_ir")) Then
    txtValIR.Text = ConPropAdesao.FormataValorParaPadraoBrasil(rsDadosFinanceiros("val_ir"))
  End If
  
  If Not IsNull(rsDadosFinanceiros("val_adic_fracionamento")) Then
    txtValAdicFracionamento.Text = ConPropAdesao.FormataValorParaPadraoBrasil(rsDadosFinanceiros("val_adic_fracionamento"))
  End If
  
  If Not IsNull(rsDadosFinanceiros("val_comissao")) Then
    txtValComissao.Text = ConPropAdesao.FormataValorParaPadraoBrasil(rsDadosFinanceiros("val_comissao"))
  End If
  
  If Not IsNull(rsDadosFinanceiros("val_comissao_estipulante")) Then
    txtValComissaoEstipulante.Text = ConPropAdesao.FormataValorParaPadraoBrasil(rsDadosFinanceiros("val_comissao_estipulante"))
  End If
  
  If Not IsNull(rsDadosFinanceiros("val_custo_uss")) Then
    txtValCustoAssistencia.Text = ConPropAdesao.FormataValorParaPadraoBrasil(rsDadosFinanceiros("val_custo_uss"))
  End If
  
  If Not IsNull(rsDadosFinanceiros("val_premio_tarifa")) Then
    txtValPremioTarifa.Text = ConPropAdesao.FormataValorParaPadraoBrasil(rsDadosFinanceiros("val_premio_tarifa"))
  End If
  
  If Not IsNull(rsDadosFinanceiros("val_is")) Then
    txtValIS.Text = ConPropAdesao.FormataValorParaPadraoBrasil(rsDadosFinanceiros("val_is"))
  End If
  
  If Not IsNull(rsDadosFinanceiros("val_remuneracao")) Then
    txtValRemuneracao.Text = ConPropAdesao.FormataValorParaPadraoBrasil(rsDadosFinanceiros("val_remuneracao"))
  End If
  
  If Not IsNull(rsDadosFinanceiros("val_desconto_comercial")) Then
    txtValDesconto.Text = ConPropAdesao.FormataValorParaPadraoBrasil(rsDadosFinanceiros("val_desconto_comercial"))
  End If
  
  If Not IsNull(rsDadosFinanceiros("custo_apolice")) Then
    txtValCustoApolice.Text = ConPropAdesao.FormataValorParaPadraoBrasil(rsDadosFinanceiros("custo_apolice"))
  End If
    
  txtNumParcelas.Text = IIf(IsNull(rsDadosFinanceiros("num_parcelas")), "", rsDadosFinanceiros("num_parcelas"))
  txtValParidade.Text = IIf(IsNull(rsDadosFinanceiros("val_paridade_moeda")), "", rsDadosFinanceiros("val_paridade_moeda"))
  TxtQtdVidas.Text = IIf(IsNull(rsDadosFinanceiros("qtd_vidas")), "", rsDadosFinanceiros("qtd_vidas"))
  txtSituacaoResseguro.Text = IIf(IsNull(rsDadosFinanceiros("situacao_re_seguro")), "", rsDadosFinanceiros("situacao_re_seguro"))
  txtDtContabEmissao.Text = IIf(Format(rsDadosFinanceiros("dt_contabilizacao_emissao"), "dd/mm/yyyy") = "01/01/1900", "", Format(rsDadosFinanceiros("dt_contabilizacao_emissao"), "dd/mm/yyyy"))
  txtDtPgto.Text = IIf(Format(rsDadosFinanceiros("dt_pgto"), "dd/mm/yyyy") = "01/01/1900", "", Format(rsDadosFinanceiros("dt_pgto"), "dd/mm/yyyy"))
  txtMoedaSeguro.Text = IIf(IsNull(rsDadosFinanceiros("seguro_moeda_id")), "", Trim(rsDadosFinanceiros("seguro_moeda_id")) & "-" & Trim(rsDadosFinanceiros("seguro_moeda")))
  txtMoedaPremio.Text = IIf(IsNull(rsDadosFinanceiros("premio_moeda_id")), "", Trim(rsDadosFinanceiros("premio_moeda_id")) & "-" & Trim(rsDadosFinanceiros("premio_moeda")))
  txtMoedaPremio.Text = IIf(IsNull(rsDadosFinanceiros("forma_pgto_id")), "", Trim(rsDadosFinanceiros("forma_pgto_id")) & "-" & Trim(rsDadosFinanceiros("forma_pgto")))
  
  rsDadosFinanceiros.Close
  Set rsDadosFinanceiros = Nothing

Exit Sub
TrataErro:
  Call TratarErro("CarregarDadosFinanceiros", Me.name)
  Call FinalizarAplicacao

End Sub

Private Function ObterDadosReajuste(lPropostaId As Long, lEndossoId As Long) As Recordset
On Error GoTo TrataErro
Dim sSql As String
    sSql = ""
    sSql = sSql & "    SELECT indice_tb.indice_id,                                           " & vbNewLine
    sSql = sSql & "           indice_tb.nome indice,                               " & vbNewLine
    sSql = sSql & "           perc_reajuste,                                       " & vbNewLine
    sSql = sSql & "           ISNULL(dt_impressao,'19000101') dt_impressao,        " & vbNewLine
    sSql = sSql & "           val_premio_anterior,                                 " & vbNewLine
    sSql = sSql & "           val_premio_atual,                                    " & vbNewLine
    sSql = sSql & "           val_is_anterior,                                     " & vbNewLine
    sSql = sSql & "           val_is_atual,                                        " & vbNewLine
    sSql = sSql & "           val_custo_uss_anterior,                              " & vbNewLine
    sSql = sSql & "           val_custo_uss_atual                                  " & vbNewLine
    sSql = sSql & "      FROM endosso_reajuste_tb   WITH (NOLOCK)                     " & vbNewLine
    sSql = sSql & "INNER JOIN indice_tb   WITH (NOLOCK)                               " & vbNewLine
    sSql = sSql & "        ON indice_tb.indice_id = endosso_reajuste_tb.indice_id  " & vbNewLine
    sSql = sSql & "     WHERE endosso_reajuste_tb.proposta_adesao_id = " & lPropostaId & vbNewLine
    sSql = sSql & "       AND endosso_id = " & lEndossoId & vbNewLine
    'Renato Silva - Confitec - Acrescentei uma union para os produtos BESC - 17/07/2015 - Projeto 18252425
    sSql = sSql & " UNION " & vbNewLine
    sSql = sSql & "    SELECT besc.indice_id,                                                              " & vbNewLine
    sSql = sSql & "           besc.nome_indice,                                                            " & vbNewLine
    sSql = sSql & "           besc.fator_renovacao perc_reajuste,                                          " & vbNewLine
    sSql = sSql & "           ISNULL(besc.dt_impressao,'19000101') dt_impressao,                           " & vbNewLine
    sSql = sSql & "           besc.val_premio_anterior,                                                    " & vbNewLine
    sSql = sSql & "           besc.val_premio_atual,                                                       " & vbNewLine
    sSql = sSql & "           besc.val_is_anterior,                                                        " & vbNewLine
    sSql = sSql & "           besc.val_is_atual,                                                           " & vbNewLine
    sSql = sSql & "           besc.val_custo_uss_anterior,                                                 " & vbNewLine
    sSql = sSql & "           besc.val_custo_uss_atual                                                     " & vbNewLine
    sSql = sSql & "      FROM seguros_db.dbo.Besc_parametro_calculo_capital_segurado_tb besc WITH (NOLOCK) " & vbNewLine
    sSql = sSql & "     WHERE besc.proposta_id = " & lPropostaId & vbNewLine
    sSql = sSql & "       AND besc.endosso_id =  " & lEndossoId & vbNewLine
   
    Set ObterDadosReajuste = ExecutarSQL(gsSIGLASISTEMA, _
                                         glAmbiente_id, _
                                         App.Title, _
                                         App.FileDescription, _
                                         sSql, _
                                         True)
Exit Function
TrataErro:
  Call TratarErro("ObterDadosReajuste", Me.name)
  Call FinalizarAplicacao

End Function
Private Sub CarregarDadosReajuste()

On Error GoTo TrataErro

  Dim rsDadosReajuste As Recordset
  
  Set rsDadosReajuste = New Recordset
  Set rsDadosReajuste = ObterDadosReajuste(glPropostaId, Val(txtEndossoId.Text))
  
  If rsDadosReajuste.EOF Then
    Call MsgBox("N�o foi poss�vel obter os dados do endosso de reajuste", vbCritical)
    GoTo TrataErro
  End If
  
  txtIndiceReajuste.Text = Trim(rsDadosReajuste("indice_id")) & "-" & Trim(rsDadosReajuste("indice"))
  txtPercReajuste.Text = Format(rsDadosReajuste("perc_reajuste"), "##0.0000")
  txtDtImpressao.Text = IIf(Format(rsDadosReajuste("dt_impressao"), "dd/mm/yyyy") = "01/01/1900", "", Format(rsDadosReajuste("dt_impressao"), "dd/mm/yyyy"))
  txtPremioAnt.Text = ConPropAdesao.FormataValorParaPadraoBrasil(rsDadosReajuste("val_premio_anterior"))
  txtPremioReaj.Text = ConPropAdesao.FormataValorParaPadraoBrasil(rsDadosReajuste("val_premio_atual"))
  txtISAnt.Text = ConPropAdesao.FormataValorParaPadraoBrasil(rsDadosReajuste("val_is_anterior"))
  txtISReaj.Text = ConPropAdesao.FormataValorParaPadraoBrasil(rsDadosReajuste("val_is_atual"))
  txtCustoAssistAnt.Text = ConPropAdesao.FormataValorParaPadraoBrasil(rsDadosReajuste("val_custo_uss_anterior"))
  txtCustoAssistReaj.Text = ConPropAdesao.FormataValorParaPadraoBrasil(rsDadosReajuste("val_custo_uss_atual"))
  
  rsDadosReajuste.Close
  Set rsDadosReajuste = Nothing

Exit Sub
TrataErro:
  Call TratarErro("CarregarDadosReajuste", Me.name)
  Call FinalizarAplicacao

End Sub

Private Function ObterDadosReenquadramento(lPropostaId As Long, lEndossoId As Long) As Recordset

On Error GoTo TrataErro
    
    Dim sSql As String
    sSql = ""
    sSql = sSql & "    SELECT endosso_reenquadramento_tb.fator_renovacao,                                         " & vbNewLine
    sSql = sSql & "           ISNULL(endosso_reenquadramento_tb.dt_inicio_vigencia_plano,'19000101') dt_inicio_vigencia_plano , " & vbNewLine
    sSql = sSql & "           endosso_reenquadramento_tb.plano_id,                                                 " & vbNewLine
    sSql = sSql & "           plano_atual.nome plano_atual,                                                        " & vbNewLine
    sSql = sSql & "           endosso_reenquadramento_tb.plano_id_antigo,                                          " & vbNewLine
    sSql = sSql & "           plano_antigo.nome plano_antigo,                                                      " & vbNewLine
    sSql = sSql & "           endosso_reenquadramento_tb.val_premio_anterior,                                      " & vbNewLine
    sSql = sSql & "           endosso_reenquadramento_tb.val_premio_atual,                                         " & vbNewLine
    sSql = sSql & "           endosso_reenquadramento_tb.val_is_anterior,                                          " & vbNewLine
    sSql = sSql & "           endosso_reenquadramento_tb.val_is_atual,                                             " & vbNewLine
    sSql = sSql & "           endosso_reenquadramento_tb.val_custo_uss_anterior,                                   " & vbNewLine
    sSql = sSql & "           endosso_reenquadramento_tb.val_custo_uss_atual                                       " & vbNewLine
    sSql = sSql & "      FROM endosso_reenquadramento_tb  WITH (NOLOCK)                                               " & vbNewLine
    sSql = sSql & "INNER JOIN endosso_tb  WITH (NOLOCK)                                                                   " & vbNewLine
    sSql = sSql & "        ON endosso_tb.endosso_id = endosso_reenquadramento_tb.endosso_id                        " & vbNewLine
    sSql = sSql & "       AND endosso_tb.proposta_id = endosso_reenquadramento_tb.proposta_adesao_id               " & vbNewLine
    sSql = sSql & " LEFT JOIN plano_tb plano_antigo  WITH (NOLOCK)                                                        " & vbNewLine
    sSql = sSql & "        ON plano_antigo.produto_id = endosso_reenquadramento_tb.produto_id                      " & vbNewLine
    sSql = sSql & "       AND plano_antigo.plano_id   = endosso_reenquadramento_tb.plano_id_antigo                 " & vbNewLine
    sSql = sSql & "       AND endosso_tb.dt_emissao BETWEEN plano_antigo.dt_inicio_vigencia AND ISNULL(plano_antigo.dt_fim_vigencia,endosso_tb.dt_emissao)" & vbNewLine
    sSql = sSql & " LEFT JOIN plano_tb plano_atual                                                                 " & vbNewLine
    sSql = sSql & "        ON plano_atual.produto_id = endosso_reenquadramento_tb.produto_id                       " & vbNewLine
    sSql = sSql & "       AND plano_atual.plano_id = endosso_reenquadramento_tb.plano_id                           " & vbNewLine
    sSql = sSql & "       AND plano_atual.dt_inicio_vigencia = endosso_reenquadramento_tb.dt_inicio_vigencia_plano " & vbNewLine
    sSql = sSql & "       and plano_atual.dt_fim_vigencia is null                                                  " & vbNewLine
    sSql = sSql & "     WHERE endosso_reenquadramento_tb.proposta_adesao_id = " & lPropostaId & vbNewLine
    sSql = sSql & "       AND endosso_reenquadramento_tb.endosso_id = " & lEndossoId & vbNewLine
    'Renato Silva - Confitec - Acrescentei uma union para os produtos BESC - 17/07/2015 - Projeto 18252425
    sSql = sSql & " UNION " & vbNewLine
    sSql = sSql & "    SELECT besc.fator_renovacao,                                                        " & vbNewLine
    sSql = sSql & "           ISNULL(besc.dt_inicio_vigencia_plano,'19000101') dt_inicio_vigencia_plano ,  " & vbNewLine
    sSql = sSql & "           besc.plano_id,                                                               " & vbNewLine
    sSql = sSql & "           besc.nome_plano_atual plano_atual,                                           " & vbNewLine
    sSql = sSql & "           besc.plano_id_antigo,                                                        " & vbNewLine
    sSql = sSql & "           besc.nome_plano_antigo plano_antigo,                                         " & vbNewLine
    sSql = sSql & "           besc.val_premio_anterior,                                                    " & vbNewLine
    sSql = sSql & "           besc.val_premio_atual,                                                       " & vbNewLine
    sSql = sSql & "           besc.val_is_anterior,                                                        " & vbNewLine
    sSql = sSql & "           besc.val_is_atual,                                                           " & vbNewLine
    sSql = sSql & "           besc.val_custo_uss_anterior,                                                 " & vbNewLine
    sSql = sSql & "           besc.val_custo_uss_atual                                                     " & vbNewLine
    sSql = sSql & "      FROM seguros_db.dbo.Besc_parametro_calculo_capital_segurado_tb besc WITH (NOLOCK) " & vbNewLine
    sSql = sSql & "     WHERE besc.proposta_id = " & lPropostaId & vbNewLine
    sSql = sSql & "       AND besc.endosso_id =  " & lEndossoId & vbNewLine
    
  
    Set ObterDadosReenquadramento = ExecutarSQL(gsSIGLASISTEMA, _
                                                glAmbiente_id, _
                                                App.Title, _
                                                App.FileDescription, _
                                                sSql, _
                                                True)
Exit Function
TrataErro:
  Call TratarErro("ObterDadosReenquadramento", Me.name)
  Call FinalizarAplicacao

End Function
Private Sub CarregarDadosReenquadramento()

On Error GoTo TrataErro

  Dim rsDadosReenquadramento As Recordset
  
  Set rsDadosReenquadramento = New Recordset
  
  Set rsDadosReenquadramento = ObterDadosReenquadramento(glPropostaId, Val(txtEndossoId.Text))
  
  If rsDadosReenquadramento.EOF Then
    Call MsgBox("N�o foi poss�vel obter os dados do endosso de Reenquadramento", vbCritical)
    GoTo TrataErro
  End If
  
  txtFatorRenov.Text = Format(rsDadosReenquadramento("fator_renovacao"), "##0.00000")
  txtInicioVigenciaPlano.Text = IIf(Format(rsDadosReenquadramento("dt_inicio_vigencia_plano"), "dd/mm/yyyy") = "01/01/1900", "", Format(rsDadosReenquadramento("dt_inicio_vigencia_plano"), "dd/mm/yyyy"))
  txtPlanoAtual.Text = IIf(IsNull(rsDadosReenquadramento("plano_id")), "", Trim(rsDadosReenquadramento("plano_id")) & "-" & Trim(rsDadosReenquadramento("plano_atual")))
  txtPlanoAnt.Text = IIf(IsNull(rsDadosReenquadramento("plano_id_antigo")), "", Trim(rsDadosReenquadramento("plano_id_antigo")) & "-" & Trim(rsDadosReenquadramento("plano_antigo")))
  txtReenqPremioReaj.Text = ConPropAdesao.FormataValorParaPadraoBrasil(rsDadosReenquadramento("val_premio_atual"))
  txtReenqPremioAnt.Text = ConPropAdesao.FormataValorParaPadraoBrasil(rsDadosReenquadramento("val_premio_anterior"))
  txtReenqISReaj.Text = ConPropAdesao.FormataValorParaPadraoBrasil(rsDadosReenquadramento("val_is_atual"))
  txtReenqISAnt.Text = ConPropAdesao.FormataValorParaPadraoBrasil(rsDadosReenquadramento("val_is_anterior"))
  txtReenqCustoAssistReaj.Text = ConPropAdesao.FormataValorParaPadraoBrasil(rsDadosReenquadramento("val_custo_uss_atual"))
  txtReenqCustoAssistAnt.Text = ConPropAdesao.FormataValorParaPadraoBrasil(rsDadosReenquadramento("val_custo_uss_anterior"))
  
  rsDadosReenquadramento.Close
  Set rsDadosReenquadramento = Nothing

Exit Sub
TrataErro:
  Call TratarErro("CarregarDadosReenquadramento", Me.name)
  Call FinalizarAplicacao

End Sub
Private Function ObterDadosSuspensao(lPropostaId As Long) As Recordset

On Error GoTo TrataErro
    Dim sSql As String
    
    sSql = ""
    sSql = sSql & "SELECT dt_inicio_suspensao,                  " & vbNewLine
    sSql = sSql & "       ISNULL(dt_fim_suspensao,'19000101') dt_fim_suspensao, " & vbNewLine
    sSql = sSql & "       ind_agendamento,                      " & vbNewLine
    sSql = sSql & "       ISNULL(cod_suspensao,'') cod_suspensao" & vbNewLine
    sSql = sSql & "  FROM proposta_suspensa_tb                  " & vbNewLine
    sSql = sSql & " WHERE proposta_id = " & lPropostaId

    Set ObterDadosSuspensao = ExecutarSQL(gsSIGLASISTEMA, _
                                          glAmbiente_id, _
                                          App.Title, _
                                          App.FileDescription, _
                                          sSql, _
                                          True)
Exit Function
TrataErro:
  Call TratarErro("ObterDadosSuspensao", Me.name)
  Call FinalizarAplicacao

End Function
Private Sub CarregarDadosSuspensao()
On Error GoTo TrataErro
Dim rsDadosSuspensao As Recordset

  Set rsDadosSuspensao = New Recordset
  Set rsDadosSuspensao = ObterDadosSuspensao(glPropostaId)
  
  If rsDadosSuspensao.EOF Then
    Call MsgBox("N�o foi poss�vel obter os dados do endosso de suspens�o", vbCritical)
    GoTo TrataErro
  End If
  
  mskInicioSuspensao.Text = Format(rsDadosSuspensao("dt_inicio_suspensao"), "dd/mm/yyyy")
  mskFimSuspensao.Text = IIf(Format(rsDadosSuspensao("dt_fim_suspensao"), "dd/mm/yyyy") = "01/01/1900", "", Format(rsDadosSuspensao("dt_fim_suspensao"), "dd/mm/yyyy"))
  txtIndAgendamento.Text = Trim(UCase(rsDadosSuspensao("ind_agendamento")))
  txtCodSuspensao.Text = Trim(UCase(rsDadosSuspensao("cod_suspensao")))
    
  rsDadosSuspensao.Close
  Set rsDadosSuspensao = Nothing

Exit Sub
TrataErro:
  Call TratarErro("CarregarDadosSuspensao", Me.name)
  Call FinalizarAplicacao

End Sub


'Paulo Pelegrini - MU-2017-057120 - 09/11/2017 (INI)
Private Function ObterDadosSegurado(lPropostaId As Long) As Recordset
    On Error GoTo TrataErro

    Dim sSql As String

    sSql = ""
    sSql = sSql & "SELECT b.nome, b.cpf_cnpj, c.dt_nascimento " & vbNewLine
    sSql = sSql & "     FROM SEGUROS_DB.DBO.SEGURO_VIDA_TB A WITH (NOLOCK) " & vbNewLine
    sSql = sSql & "     JOIN SEGUROS_DB.dbo.cliente_tb b " & vbNewLine
    sSql = sSql & "         on a.prop_cliente_id = b.cliente_id  " & vbNewLine
    sSql = sSql & "     JOIN SEGUROS_DB.dbo.pessoa_fisica_tb c " & vbNewLine
    sSql = sSql & "         on c.pf_cliente_id = A.prop_cliente_id  " & vbNewLine
    sSql = sSql & "WHERE A.PROPOSTA_ID =" & lPropostaId & vbNewLine
    sSql = sSql & "AND A.TP_COMPONENTE_ID IN (1,3) " & vbNewLine

    Set ObterDadosSegurado = ExecutarSQL(gsSIGLASISTEMA, _
                                         glAmbiente_id, _
                                         App.Title, _
                                         App.FileDescription, _
                                         sSql, _
                                         True)


    Exit Function
TrataErro:
    Call TratarErro("ObterDadosSegurado", Me.name)
    Call FinalizarAplicacao

End Function


Private Sub CarregarDadosSegurados()
    On Error GoTo TrataErro
    Dim rsDadosSegurados As Recordset
    Dim sLinha As String

    Set rsDadosSegurados = New Recordset
    Set rsDadosSegurados = ObterDadosSegurado(glPropostaId)

    If rsDadosSegurados.EOF Then
        Call MsgBox("N�o foi poss�vel obter os dados do endosso exclus�o de c�njuge.", vbCritical)
        GoTo TrataErro
    End If

    gridDadosSegurados.FormatString = "Nome                                                                           | CPF                                | Dt. Nascimento                          "
    gridDadosSegurados.Rows = 1

    While Not rsDadosSegurados.EOF

        sLinha = ""
        sLinha = sLinha & rsDadosSegurados("Nome") & vbTab
        sLinha = sLinha & Trim(rsDadosSegurados("cpf_cnpj")) & vbTab
        sLinha = sLinha & Trim(rsDadosSegurados("dt_nascimento")) & vbTab

        Call gridDadosSegurados.AddItem(sLinha)

        rsDadosSegurados.MoveNext
    Wend

    rsDadosSegurados.Close
    Set rsDadosSegurados = Nothing

    Exit Sub
TrataErro:
    Call TratarErro("CarregarDadosSegurados", Me.name)
    Call FinalizarAplicacao

End Sub
'Paulo Pelegrini - MU-2017-057120 - 09/11/2017 (FIM)


Private Sub CarregarDados()

On Error GoTo TrataErro

  Select Case giTpEndosso
  
    Case FRAME_ENDOSSO_CADASTRAL
      Call CarregarDadosCadastrais
      
    Case FRAME_ENDOSSO_CANCELAMENTO_ENDOSSO
      Call CarregarDadosCancelamentoEndosso
      
    Case FRAME_ENDOSSO_CANCELAMENTO_PROPOSTA
      Call CarregarDadosCancelamentoProposta
      
    Case FRAME_ENDOSSO_FINANCEIRO
      Call CarregarDadosFinanceiros
      
    Case FRAME_ENDOSSO_REAJUSTE
      Call CarregarDadosReajuste
      
    Case FRAME_ENDOSSO_REENQUADRAMENTO
      Call CarregarDadosReenquadramento
      
    Case FRAME_ENDOSSO_SUSPENSAO
      Call CarregarDadosSuspensao
      
    Case FRAME_ENDOSSO_EXCLUSAO_CONJUGE    'Paulo Pelegrini - MU-2017-057120 - 09/11/2017
      Call CarregarDadosSegurados
    
    Case Else
      Call MsgBox("Tipo de endosso n�o permitido", vbCritical)
      Call FinalizarAplicacao
  
  End Select

Exit Sub
TrataErro:
  Call TratarErro("CarregarDados", Me.name)
  Call FinalizarAplicacao

End Sub

Private Sub Form_Load()

  'mouse
  Me.MousePointer = vbHourglass

  'obtendo dados da tela anterior
  Call ObterDadosTelaAnterior

  'interface
  Call CarregarDadosEndosso
  
  If ConfigurarInterface Then
    'Carregando Dados
    Call CarregarDados
  End If
  
  Me.MousePointer = vbDefault
  Me.chkPPE.Enabled = False
  
  
End Sub
Private Sub ObterDadosTelaAnterior()

    If Not ConPropAdesaoALS.OrigemALS Then
    txtEndossoId.Text = ConPropAdesao.grdEndosso.TextMatrix(ConPropAdesao.grdEndosso.Row, 0)
    mskDtEmissao.Text = ConPropAdesao.grdEndosso.TextMatrix(ConPropAdesao.grdEndosso.Row, 1)
    txtTpEndosso.Text = ConPropAdesao.grdEndosso.TextMatrix(ConPropAdesao.grdEndosso.Row, 2) & " - " & _
                        ConPropAdesao.grdEndosso.TextMatrix(ConPropAdesao.grdEndosso.Row, 3)
    txtTpEndosso.Tag = ConPropAdesao.grdEndosso.TextMatrix(ConPropAdesao.grdEndosso.Row, 2)
    txtNumEndossoBB.Text = ConPropAdesao.grdEndosso.TextMatrix(ConPropAdesao.grdEndosso.Row, 5)
    txtDescEndosso.Text = ConPropAdesao.grdEndosso.TextMatrix(ConPropAdesao.grdEndosso.Row, 6)
    glPropostaId = ConPropAdesao.txtProposta.Text
  Else
    txtEndossoId.Text = ConPropAdesaoALS.grdEndosso.TextMatrix(ConPropAdesaoALS.grdEndosso.Row, 0)
    mskDtEmissao.Text = ConPropAdesaoALS.grdEndosso.TextMatrix(ConPropAdesaoALS.grdEndosso.Row, 1)
    txtTpEndosso.Text = ConPropAdesaoALS.grdEndosso.TextMatrix(ConPropAdesaoALS.grdEndosso.Row, 2) & " - " & _
                        ConPropAdesaoALS.grdEndosso.TextMatrix(ConPropAdesaoALS.grdEndosso.Row, 3)
    txtTpEndosso.Tag = ConPropAdesaoALS.grdEndosso.TextMatrix(ConPropAdesaoALS.grdEndosso.Row, 2)
    txtNumEndossoBB.Text = ConPropAdesaoALS.grdEndosso.TextMatrix(ConPropAdesaoALS.grdEndosso.Row, 5)
    txtDescEndosso.Text = ConPropAdesaoALS.grdEndosso.TextMatrix(ConPropAdesaoALS.grdEndosso.Row, 6)
    glPropostaId = ConPropAdesaoALS.txtProposta.Text
  End If
  
  
End Sub
Private Function ConfigurarInterface() As Boolean

  'Titulo '''''''''''
  'lrocha - 05/01/2006
  If Trim(SelPropAdesao.MSFlexGrid1.TextMatrix(SelPropAdesao.MSFlexGrid1.Row, 6)) <> "" Then
    Me.Caption = "SEGP0176 - Consulta de propostas - Endosso" & " - " & SelPropAdesao.MSFlexGrid1.TextMatrix(SelPropAdesao.MSFlexGrid1.Row, 6)
  Else
    Me.Caption = "SEGP0176 - Consulta de propostas - Endosso"
  End If

  'Frames '''''''''''
  fraCancEndosso.Visible = False
  fraCancProposta.Visible = False
  fraDadosCadastrais.Visible = False
  fraFinanceiro.Visible = False
  fraReajuste.Visible = False
  fraReenquadramento.Visible = False
  fraSuspensao.Visible = False
  Call EsconderFrame(fraCancEndosso)
  Call EsconderFrame(fraCancProposta)
  Call EsconderFrame(fraDadosCadastrais)
  Call EsconderFrame(fraFinanceiro)
  Call EsconderFrame(fraReajuste)
  Call EsconderFrame(fraReenquadramento)
  Call EsconderFrame(fraSuspensao)
  Call EsconderFrame(fraExcConjuge)    'Paulo Pelegrini - MU-2017-057120 - 09/11/2017
  
  ConfigurarInterface = True
  
  Select Case Val(txtTpEndosso.Tag)
  
    'Endosso Cadastral''''''''''''''''''''''''''''''''''''''''''''''
    Case TP_ENDOSSO_ENDOSSO_CADASTRAL_DE_VIDA, _
         TP_ENDOSSO_ENDOSSO_BENEFICIARIO_VIDA, _
         TP_ENDOSSO_ADICIONAL, _
         TP_ENDOSSO_PARTICIPACAO_DO_CONGENERE, _
         TP_ENDOSSO_ALTERACAO_CADASTRAL, _
         TP_ENDOSSO_EXCLUSAO_DE_BENEFICIARIO, _
         TP_ENDOSSO_INCLUSAO_DE_BENEFICIARIO, _
         TP_ENDOSSO_NAO_DEFINIDO
         
         fraDadosCadastrais.Visible = True
         giTpEndosso = FRAME_ENDOSSO_CADASTRAL
         Call ExibirFrame(fraDadosCadastrais)
         
    
    'Endosso Financeiro ''''''''''''''''''''''''''''''''''''''''''''
    Case TP_ENDOSSO_REINTEGRACAO_DE_IS, _
         TP_ENDOSSO_RESTITUI��O_POR_INICIATIVA_DA_SEGURADORA, _
         TP_ENDOSSO_ENDOSSO_DE_REDU��O_DE_IS_POR_SINISTRO, _
         TP_ENDOSSO_ENDOSSO_DE_FATURA, _
         TP_ENDOSSO_ENDOSSO_COM_MOVIMENTA��O_DE_PREMIO, _
         TP_ENDOSSO_ENDOSSO_DE_CANCELAMENTO_DE_PARCELA
         
         fraFinanceiro.Visible = True
         giTpEndosso = FRAME_ENDOSSO_FINANCEIRO
         Call ExibirFrame(fraFinanceiro)
         
    'Cancelamento de proposta ''''''''''''''''''''''''''''''''''''''
    Case TP_ENDOSSO_CANCELAMENTO_A_PEDIDO_DO_SEGURADO, _
         TP_ENDOSSO_CANCELAMENTO_A_PEDIDO_DA_SEGURADORA, _
         TP_ENDOSSO_CANCELAMENTO_POR_INADIMPLENCIA, _
         TP_ENDOSSO_CANCELAMENTO_POR_ESGOTAMENTO_DE_IS, _
         TP_ENDOSSO_CANCELAMENTO_POR_INADIMPLENCIA_PRIMEIRA_PARCELA
         
         fraCancProposta.Visible = True
         giTpEndosso = FRAME_ENDOSSO_CANCELAMENTO_PROPOSTA
         Call ExibirFrame(fraCancProposta)
         
    'Cancelamento de endosso''''''''''''''''''''''''''''''''''''''''
    Case TP_ENDOSSO_CANCELAMENTO_DE_ENDOSSO, _
         TP_ENDOSSO_CANCELAMENTO_SUSPENSAO_DE_APOLICE
         
         fraCancEndosso.Visible = True
         giTpEndosso = FRAME_ENDOSSO_CANCELAMENTO_ENDOSSO
         Call ExibirFrame(fraCancEndosso)
         
    'Reenquadramento '''''''''''''''''''''''''''''''''''''''''''''''
    Case TP_ENDOSSO_ALTERACAO_DE_PLANO, _
         TP_ENDOSSO_ENDOSSO_DE_REENQUADRAMENTO
         
         fraReenquadramento.Visible = True
         giTpEndosso = FRAME_ENDOSSO_REENQUADRAMENTO
         Call ExibirFrame(fraReenquadramento)
         
    'Reajuste ''''''''''''''''''''''''''''''''''''''''''''''''''''''
    Case TP_ENDOSSO_ENDOSSO_DE_REAJUSTE
    
        fraReajuste.Visible = True
        giTpEndosso = FRAME_ENDOSSO_REAJUSTE
        Call ExibirFrame(fraReajuste)
        
    'Suspensao de apolice ''''''''''''''''''''''''''''''''''''''''''
    Case TP_ENDOSSO_SUSPENSAO_DE_APOLICE
         
        fraSuspensao.Visible = True
        giTpEndosso = FRAME_ENDOSSO_SUSPENSAO
        Call ExibirFrame(fraSuspensao)
            
     'Paulo Pelegrini - MU-2017-057120 - 09/11/2017 (INI)
    Case TP_ENDOSSO_CANCELAMENTO_DE_CONJUGE, _
         TP_ENDOSSO_SINISTRO_DE_CONJUGE, _
         TP_ENDOSSO_EXCLUSAO_DE_CONJUGE, _
         TP_ENDOSSO_EXCLUSAO_DE_CONJUGE_POR_SINISTRO

        If ConPropAdesao.txtProduto.Tag = "12" _
           Or ConPropAdesao.txtProduto.Tag = "121" _
           Or ConPropAdesao.txtProduto.Tag = "135" _
           Or ConPropAdesao.txtProduto.Tag = "136" Then


            fraExcConjuge.Visible = True
            giTpEndosso = FRAME_ENDOSSO_EXCLUSAO_CONJUGE
            Call ExibirFrame(fraExcConjuge)

        End If
        'Paulo Pelegrini - MU-2017-057120 - 09/11/2017 (FIM)
            
    Case Else
    '  Call MsgBox("Tipo de Endosso N�o previsto", vbCritical)
      ConfigurarInterface = False
  
  End Select

End Function

Sub ExibirFrame(frame As Object)

  frame.Top = Line1.Y1
  frame.Left = Line1.X1
  frame.Visible = True

End Sub

Sub EsconderFrame(frame As Object)

  frame.Top = 10000
  frame.Visible = False
  
End Sub

'Sub FILLSAUSAGE()
'mskDtEndosso = "01/02/2003"
'mskFimVig = "01/03/2004"
'mskDtEmissao = "02/02/2003"
'txtTpEndosso = "Endosso Cadastral Teste"
'txtNumEndossoBB = "645"
'mskDtRetornoBB = "05/04/2003"
'txtUsuario = "Produ��o2"
'txtDescEndosso = "Endosso teste"
'txtEndossoId = "43"
'
'txtCodCliAnt = "23563"
'txtNomeAnt = "JOAO JOSE NUNES ALVARENGA"
'txtTpPessoaAnt = "F�SICA"
'txtCPFCNPJAnt = "514875421-74"
'txtSexoDescAnt = "MASCULINO"
'txtEstCivilAnt = "1"
'txtEstCivilDescAnt = "SOLTEIRO"
'txtNascAnt = "07/10/1967"
'TxtEmailAnt = "ALVARENGA@GLOBO.COM"
'TxtDDDAnt = "34"
'TxtTelefoneAnt = "81452477"
'TxtEnderecoAnt = "R. SOBRAL FILHO, 34"
'TxtBairroAnt = "GUARAP�S"
'TxtCidadeAnt = "UBERABA"
'TxtUFAnt = "MG"
'TxtCEPAnt = "28712-541"
'
'txtValFinanceiro = "1.500,00"
'txtValIof = "15,12"
'txtValIR = "45,12"
'txtValAdicFracionamento = "34,00"
'txtValComissao = "3,12"
'txtValComissaoEstipulante = "23,40"
'txtValCustoAssistencia = "0,00"
'txtValPremioTarifa = "124,32"
'txtValIS = "15.000,00"
'txtValRemuneracao = "2.343,23"
'txtValDesconto = "35,23"
'txtValCustoApolice = "6,00"
'txtNumParcelas = "12"
'txtValParidade = "1"
'txtQtdVidas = "1"
'txtDtPgto = "05/05/2003"
'txtMoedaSeguro = "790 - REAL"
'txtMoedaPremio = "790 - REAL"
'txtFormaPgto = "DINHEIRO"
'
'mskCancInicioCancelamento = "01/08/2003"
'txtCancMotivoCancelamento = "Cancelamento a pedido do segurado"
'txtCancTpCancelamento = "Cancelamento manual"
'mskCancDtCancBB = "20/07/2003"
'
'txtEndossoCancelado(0) = "421"
'mskEndDtCancelamento = "05/10/2003"
'mskEndDtContab = "10/10/2003"
'txtEndCartaEmitida(1) = "NAO"
'
'mskInicioSuspensao = "08/08/2003"
'mskFimSuspensao = "09/09/2003"
'txtIndAgendamento = "SIM"
'txtCodSuspensao = "4"
'
'txtIndiceReajuste = "19 - IGPM/ACUMULADO"
'txtPercReajuste = "5,00234"
'txtDtImpressao = "08/07/2003"
'txtPremioAnt = "150,00"
'txtPremioReaj = "164,42"
'txtISAnt = "10.000,00"
'txtISReaj = "10.542,12"
'txtCustoAssistAnt = "6,00"
'txtCustoAssistReaj = "6,14"
'
'txtFatorRenov = "1,21"
'txtInicioVigenciaPlano = "01/01/2003"
'txtPlanoAnt = "1"
'txtPlanoAtual = "2"
'txtReenqPremioAnt = "100,00"
'txtReenqPremioReaj = "101,21"
'txtReenqISAnt = "10.000,00"
'txtReenqISReaj = "10.121,00"
'txtReenqCustoAssistAnt = "6,00"
'txtReenqCustoAssistReaj = "6,31"
'
'End Sub

'Private Function ObterDadosCadastrais(lPropostaId As Long, lEndossoId As Long) As Recordset
''on error GoTo TrataErro
'Dim sSql As String
'    sSql = ""
'
'   Set ObterDadosEndosso = ExecutarSQL(gsSIGLASISTEMA, _
'                                       glAmbiente_id, _
'                                       App.Title, _
'                                       App.FileDescription, _
'                                       sSql, _
'                                       True)
'Exit Function
'TrataErro:
'  Call TratarErro("ObterDadosCadastrais", Me.name)
'  Call FinalizarAplicacao
'
'End Function
'Private Sub CarregarDadosCadastrais()
''on error GoTo TrataErro
'Dim rsDadosCadastrais As Recordset
'  Set rsDadosCadastrais = New Recordset
'  rsDadosCadastrais.Close
'  Set rsDadosCadastrais = Nothing
'
'Exit Sub
'TrataErro:
'  Call TratarErro("CarregarDadosCadastrais", Me.name)
'  Call FinalizarAplicacao
'
'End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    If ConPropAdesaoALS.txtProduto.Tag = "1225" Or _
       ConPropAdesaoALS.txtProduto.Tag = "1235" Or _
       ConPropAdesaoALS.txtProduto.Tag = "1236" Or _
       ConPropAdesaoALS.txtProduto.Tag = "1237" Then   '17919477 - BB Cr�dito Protegido Empresa - Confitec - Zoro.Gomes - tratamento para chamar o formulario padr�o do ALS para o produto 1225 - 30/04/2014
       ConPropAdesaoALS.Show
    Else
       ConPropAdesao.Show
    End If
    Unload Me
    Set frmConsultaEndosso = Nothing
End Sub
Private Function IdentificaPPE(strCPF As String) As Boolean

'Sessao de variaveis
Dim strSql      As String
Dim rc          As Recordset

'Manipulador de erros
On Error GoTo ErrIdentificaPPE

'Valor Padrao
IdentificaPPE = False

'Retira Formacao caso exista
strCPF = Replace(strCPF, ".", "")
strCPF = Replace(strCPF, "-", "")

'Busca Identificar se faz parte do PPE
strSql = "SELECT COUNT(*) AS TOTAL "
strSql = strSql & " FROM seguros_db..PPE_ORGAO_CARGO_TB as TPPE "
strSql = strSql & " WHERE CAST(TPPE.CPF AS BIGINT) = CAST('" & strCPF & "' AS BIGINT)"
strSql = strSql & " AND (TPPE.DT_EXCLUSAO_PPE IS NULL OR  TPPE.DT_EXCLUSAO_PPE >= GETDATE())"

'Executa o Comando
Set rc = ExecutarSQL(gsSIGLASISTEMA, _
                                       glAmbiente_id, _
                                       App.Title, _
                                       App.FileDescription, _
                                       strSql, _
                                       True)
    
'Verifica se voltou registro
If rc.EOF Then GoTo Finaliza

'Verifica Valor Valido
If IsNull(rc!Total) Then GoTo Finaliza

'vERIFICA nao ENCONTROU
If CInt(rc!Total) <= 0 Then GoTo Finaliza
    
'Valor Padrao
IdentificaPPE = True

'Finaliza
Finaliza:
rc.Close
Set rc = Nothing

'Sai
Exit Function

'Manipulador de erros
ErrIdentificaPPE:

    'Fecha Recordsrt
    rc.Close
    Set rc = Nothing

End Function



