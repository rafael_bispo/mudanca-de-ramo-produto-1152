VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form SelPropAdesao 
   Caption         =   "SEGP0176 - Sele��o de Propostas - "
   ClientHeight    =   6855
   ClientLeft      =   1365
   ClientTop       =   2490
   ClientWidth     =   11040
   Icon            =   "SelPropAdesao.frx":0000
   LinkTopic       =   "SelecionaProposta"
   ScaleHeight     =   6855
   ScaleWidth      =   11040
   StartUpPosition =   2  'CenterScreen
   Begin MSComctlLib.StatusBar StatusBar1 
      Align           =   2  'Align Bottom
      Height          =   255
      Left            =   0
      TabIndex        =   10
      Top             =   6600
      Width           =   11040
      _ExtentX        =   19473
      _ExtentY        =   450
      Style           =   1
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   13970
            MinWidth        =   13970
         EndProperty
      EndProperty
   End
   Begin VB.CommandButton cmdSelec 
      Caption         =   "Selecionar"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      Left            =   9540
      TabIndex        =   3
      Top             =   840
      Width           =   1125
   End
   Begin VB.Frame Frame2 
      Caption         =   "Proposta(s) encontrada(s)"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3945
      Left            =   180
      TabIndex        =   9
      Top             =   1755
      Width           =   10680
      Begin MSFlexGridLib.MSFlexGrid MSFlexGrid1 
         Height          =   3315
         Left            =   120
         TabIndex        =   12
         Top             =   360
         Width           =   10395
         _ExtentX        =   18336
         _ExtentY        =   5847
         _Version        =   393216
         Cols            =   9
         FixedCols       =   0
         SelectionMode   =   1
         FormatString    =   $"SelPropAdesao.frx":0442
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Crit�rios de Pesquisa"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1035
      Left            =   270
      TabIndex        =   5
      Top             =   360
      Width           =   10560
      Begin VB.TextBox txtCota 
         Height          =   330
         Left            =   6480
         TabIndex        =   18
         Top             =   510
         Width           =   2295
      End
      Begin VB.TextBox txtGrupo 
         Height          =   330
         Left            =   4560
         TabIndex        =   16
         Top             =   510
         Width           =   1515
      End
      Begin VB.TextBox txtArgumento 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   2040
         TabIndex        =   15
         Top             =   510
         Width           =   4155
      End
      Begin VB.ComboBox cboSegmento 
         Height          =   315
         Left            =   2040
         TabIndex        =   13
         Top             =   525
         Width           =   2175
      End
      Begin VB.TextBox TxtNrAgencia 
         Height          =   330
         Left            =   6465
         TabIndex        =   2
         Top             =   510
         Width           =   2265
      End
      Begin VB.ComboBox cboTipoSelecao 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         ItemData        =   "SelPropAdesao.frx":0505
         Left            =   150
         List            =   "SelPropAdesao.frx":0507
         Style           =   2  'Dropdown List
         TabIndex        =   0
         Top             =   510
         Width           =   1755
      End
      Begin VB.Label lblCota 
         AutoSize        =   -1  'True
         Caption         =   "Cota"
         Height          =   195
         Left            =   6480
         TabIndex        =   17
         Top             =   240
         Width           =   330
      End
      Begin VB.Label LblGrupo 
         AutoSize        =   -1  'True
         Caption         =   "Grupo"
         Height          =   195
         Left            =   4560
         TabIndex        =   14
         Top             =   240
         Width           =   435
      End
      Begin VB.Label LblSegmento 
         AutoSize        =   -1  'True
         Caption         =   "Segmento"
         Height          =   195
         Left            =   2040
         TabIndex        =   1
         Top             =   240
         Width           =   720
      End
      Begin VB.Label LblNrAgencia 
         Caption         =   "Ag�ncia"
         Height          =   195
         Left            =   6480
         TabIndex        =   11
         Top             =   270
         Width           =   1335
      End
      Begin VB.Label Label1 
         Caption         =   "Tipo de Sele��o"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   150
         TabIndex        =   8
         Top             =   270
         Width           =   1425
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "Conte�do"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   2040
         TabIndex        =   7
         Top             =   270
         Width           =   690
      End
   End
   Begin VB.PictureBox TabStrip1 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   5775
      Left            =   120
      ScaleHeight     =   5715
      ScaleWidth      =   10740
      TabIndex        =   6
      Top             =   180
      Width           =   10800
   End
   Begin VB.CommandButton cmdCanc 
      Caption         =   "&Sair"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   9720
      TabIndex        =   4
      Top             =   6120
      Width           =   1215
   End
End
Attribute VB_Name = "SelPropAdesao"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
'422007
Public chamado_246 As Boolean

Private Const TpRamoVida = 1
Private Const ProdIdOurovidaEmpresa = 15
Private Const ProdIdOuroAgricola = 145
Private Const ProdIdPenhorRural = 155
Private Const ProdIdSegurBrasilAberta = 115
Private Const ProdIdAvulsoOnLine = 123
Private Const ProdIdSegurBrasilVida = 150
Private Const ProdIdSeguroVidaAliancaIII = 714
Private Const ProdIdBBProtecaoFlex = 1199
'Alterado 30/07/2004 - Eduardo Cintra - Inclusao produto Seguro Vida Prestamista
Private Const ProdIdBBSeguroVidaAgriculturaFamiliar = 722
Const ProdIdSeguroConsorcio As Integer = 718
Private Const ProdBBMicrosseguroProtecaoPessoal = 1218
'Jessica.Adao - Confitec Sistemas - Demanda 16429073 : Inclus�o do produto 1217
Private Const ProdBBSeguroAmparoFamiliar = 1217

Private Sub ConfigurarInterface()

    'Tipo de consulta
    cboTipoSelecao.AddItem "N� da Proposta"
    cboTipoSelecao.AddItem "N� do Cliente"
    cboTipoSelecao.AddItem "N� da Proposta BB"
    cboTipoSelecao.AddItem "N� da Proposta Combinada"
  
    cboTipoSelecao.AddItem "N� da Proposta SC"
    cboTipoSelecao.AddItem "N� da Apolice SC"
    cboTipoSelecao.AddItem "N� da Certificado SC"
    cboTipoSelecao.AddItem "N� da Proposta Internet"
  
    'carolina.marinho 15/02/2011 - Demanda 9106683 - Projeto BB Seguro Prestamista Consorcio
    cboTipoSelecao.AddItem "N� Grupo e Cota"

    cboTipoSelecao.ListIndex = 0
  
    'Titulo
    Me.Caption = "SEG0176 - Sele��o de Propostas "
  
    'Status Bar
    StatusBar1.SimpleText = "Selecione Tipo de Sele��o e digite o Argumento de Pesquisa !"

End Sub

Private Sub Form_Load()

    'breno.nery - Nova Consultoria
    'Demanda: 17889524 - Aplica��o de consulta da pr�-an�lise
    'Inclus�o das vari�veis "programaClip" e "parametroProgramaClip"
    'In�cio --------------------------------------------------------
    Dim programaClip As Boolean
    Dim propostaClip As String
    'GCRUZ
   'glAmbiente_id = 3 '2 '3 - qualidade ' 2 = extrator ab
   'Data_Sistema = Date
    
    
    'marcio.nogueira - comentar ap�s o teste
    'glAmbiente_id = 3
    'UserName = "SISBR"
    'Data_Sistema = Date
    'Fim pmelo - comentar ap�s o teste
    
    
    
    programaClip = (Mid(Command, 1, 4) = "CLIP")
    'Final ---------------------------------------------------------
        
    If Trim(gsMac) = "" Or Trim(gsMac) = "000000000000" Then
        gsMac = Trim(UCase(AjustaBrEntre(GetMACAddress, 0)))
    End If
    
    'FLOW 3993694 - Confitec Sistemas - Cluiz - 13/07/2010 - Este trecho deve ser executado primeiro
    If Not Trata_Parametros(Command) And Not programaClip Then
       'Call FinalizarAplicacao
    ElseIf programaClip Then
        glAmbiente_id = Mid(Command, 5, 1)
        
        Dim valoresPassados As Variant
        
        valoresPassados = Split(Command, ",")
                
        propostaClip = CStr(valoresPassados(1))
        gsCPF = CStr(valoresPassados(2))
        cUserName = gsCPF
    End If
    '
   
    '422007
    chamado_246 = False

    If Len(gsParamAplicacao) > 2 And Not programaClip Then
        Call Seguranca("SEGP0176", "Consulta Proposta")
        chamado_246 = True
    End If
    
    'FLOW 3993694 - Confitec Sistemas - Cluiz - 13/07/2010
    'If Not Trata_Parametros(Command) Then
    '    Call FinalizarAplicacao
    'End If
    
    Call ConfigurarInterface
    'glAmbiente_id = 3 'GCRUZ
    Call ObterDataSistema(gsSIGLASISTEMA)
    
    gsMac = Trim(UCase(AjustaBrEntre(GetMACAddress, 0)))

    '422007
    If programaClip Then
        Call execucaoAutomaticaPrograma(propostaClip)
    ElseIf chamado_246 = True Then
        Call execucaoAutomaticaPrograma(Mid(gsParamAplicacao, 1, 9))
    End If

'    'FLOW 3993694 - Confitec Sistemas - Cluiz - 13/07/2010 - Este trecho deve ser executado primeiro
'    If Not Trata_Parametros(Command) Then
'        Call FinalizarAplicacao
'    End If
'    'cUserName = "GENJUNIOR"
'    'glAmbiente_id = 3
'    '422007
'    chamado_246 = False
'
'    If Len(gsParamAplicacao) > 2 Then
'        ' pablo.dias (Nova Consultoria) - 18/10/2013 - 14620257 - Melhorias no processo de subscri��o
'        ' N�o validar seguran�a quando chamado a partir de outro programa
'        'Call Seguranca("SEGP0176", "Consulta Proposta")
'        chamado_246 = True
'    End If
'
'    'FLOW 3993694 - Confitec Sistemas - Cluiz - 13/07/2010
'    'If Not Trata_Parametros(Command) Then
'    '    Call FinalizarAplicacao
'    'End If
'
'    Call ConfigurarInterface
'
'    Call ObterDataSistema(gsSIGLASISTEMA)
'
'    gsMac = Trim(UCase(AjustaBrEntre(GetMACAddress, 0)))
'
'    '422007
'    If chamado_246 = True Then
'        txtArgumento.Text = Mid(gsParamAplicacao, 1, 9)
'        cboTipoSelecao.ListIndex = 0
'        cmdSelec_Click
'        MSFlexGrid1_DblClick
'    End If
    
End Sub

'breno.nery - Nova Consultoria
'Demanda: 17889524 - Aplica��o de consulta da pr�-an�lise
'Altera��o: Procedimento para fazer a execu��o autom�tica do programa de acordo com o par�metro passado
Private Sub execucaoAutomaticaPrograma(ByVal parametroAplicacao As String)
    Me.MousePointer = vbHourglass

    txtArgumento.Text = parametroAplicacao
    cboTipoSelecao.ListIndex = 0
    cmdSelec_Click
    MSFlexGrid1_DblClick
    
    Me.MousePointer = vbDefault
End Sub

Private Sub cboTipoSelecao_Click()

    If cboTipoSelecao.Text = "N� da Proposta BB" Then
                cboTipoSelecao.Width = 1755
        LblNrAgencia.Visible = True
        TxtNrAgencia.Visible = True
        ' carolina.marinho 15/04/2011 - Demanda 9106683 - Projeto BB Seguro Prestamista Consorcio
       
        '    Else
        '       LblNrAgencia.Visible = False
        '       TxtNrAgencia.Visible = False
        '    End If
        LblSegmento.Visible = False
        cboSegmento.Visible = False
        txtGrupo.Visible = False
        LblGrupo.Visible = False
        lblCota.Visible = False
        txtCota.Visible = False
        Label2.Visible = True
        txtArgumento.Visible = True
        LblNrAgencia.Left = 6480
        TxtNrAgencia.Left = 6480
        txtArgumento.Left = 2040
        LblSegmento.Left = 2040
        
    Else
    
        If cboTipoSelecao.Text = "N� Grupo e Cota" Then
                        cboTipoSelecao.Width = 1755
            LblNrAgencia.Visible = False
            TxtNrAgencia.Visible = False
            Label2.Visible = False
            txtArgumento.Visible = False
            LblSegmento.Visible = True
            cboSegmento.Visible = True
            txtGrupo.Visible = True
            LblGrupo.Visible = True
            lblCota.Visible = True
            txtCota.Visible = True
            LblSegmento.Left = 2040
            txtArgumento.Left = 2040
            LblGrupo.Left = 4560
            txtGrupo.Left = 4560
            lblCota.Left = 6480
            txtCota.Left = 6480
            
            Call MontaComboSegmento
            
        Else
            LblNrAgencia.Visible = False
            TxtNrAgencia.Visible = False
            LblSegmento.Visible = False
            cboSegmento.Visible = False
            txtGrupo.Visible = False
            LblGrupo.Visible = False
            lblCota.Visible = False
            txtCota.Visible = False
            Label2.Visible = True
            txtArgumento.Visible = True
           cboTipoSelecao.Width = 2650
            Label2.Left = 2900
            txtArgumento.Left = 2900
            
        End If
        
    End If

End Sub

Private Sub cmdCanc_Click()
    Unload Me
End Sub

Private Function ValidarParametros() As Boolean

    ValidarParametros = False
  
    'carolina.marinho 18/04/2001 - Demanda 9106683 - Projeto BB Seguro Prestamista Consorcio
    '  If Not IsNumeric(txtArgumento) Then
    '     MsgBox "Argumento n�o Num�rico"
    '     txtArgumento.SetFocus
    '     Exit Function
    '  End If
    '
    '  If Trim(TxtNrAgencia) <> "" Then
    '     If Not IsNumeric(TxtNrAgencia) Then
    '        MsgBox "Agencia n�o num�rica"
    '        TxtNrAgencia.SetFocus
    '        Exit Function
    '     End If
    '  End If
  
    If cboTipoSelecao.Text <> "N� Grupo e Cota" Then
        If cboTipoSelecao.Text = "N� da Proposta Combinada" Then
            If Len(Trim(txtArgumento)) <> 9 Then
                MsgBox "Proposta Combinada deve conter 9 digitos"
                txtArgumento.SetFocus
                Exit Function
            Else
                ValidarParametros = True
                Exit Function
            End If
        End If
        If Not IsNumeric(txtArgumento) Then
            MsgBox "Argumento n�o Num�rico"
            txtArgumento.SetFocus
            Exit Function
        End If
  
        If Trim(TxtNrAgencia) <> "" Then
            If Not IsNumeric(TxtNrAgencia) Then
                MsgBox "Agencia n�o num�rica"
                TxtNrAgencia.SetFocus
                Exit Function
            End If
        End If
  
    Else

        If cboSegmento.Text = "" Then
            MsgBox "Favor selecionar tipo de segmento"
            cboSegmento.SetFocus
            Exit Function
        End If
      
        If Not IsNumeric(txtGrupo.Text) Then
            MsgBox "Grupo n�o num�rico"
            txtGrupo.SetFocus
            Exit Function
        End If
      
        If Not IsNumeric(txtCota) Then
            MsgBox "Cota n�o num�rica"
            txtCota.SetFocus
            Exit Function
        End If
    End If
  
    ValidarParametros = True
  
End Function

Private Function Pesquisar() As Recordset
  
  On Error GoTo TrataErro

 'Inicio alterado 30/08/2013 projeto 17860335 - Melhorias no SEGBR MC_Amaral -confitec
'    On Error GoTo TrataErro
'
'    Dim sSql     As String
'    Dim sSqlAuxA As String
'    Dim sSqlAuxF As String
'    Dim sSqlW    As String
'    Dim lCodSegmento As Long
'
'    sSql = ""
'    sSqlAuxA = ""
'    sSqlAuxF = ""
'    sSqlW = ""
'
'    sSql = sSql & "SELECT a.proposta_id, " & vbNewLine
'    sSql = sSql & "       a.dt_contratacao, " & vbNewLine
'    sSql = sSql & "       b.nome, " & vbNewLine
'    sSql = sSql & "       a.prop_cliente_id, " & vbNewLine
'    sSql = sSql & "       a.produto_id," & vbNewLine
'    'lrocha - 04/01/2006
'    sSql = sSql & "       seg_cliente = UPPER(ISNULL(b.seg_cliente,''))," & vbNewLine
'    sSql = sSql & "       case situacao" & vbNewLine
'    sSql = sSql & "         when 'e' then 'Em Estudo'" & vbNewLine
'    sSql = sSql & "         when 'a' then 'Emitida'" & vbNewLine
'    sSql = sSql & "         when 'v' then 'Em Avalia��o M�dica'" & vbNewLine
'    sSql = sSql & "         when 'l' then 'Pedido de Laudo'" & vbNewLine
'    sSql = sSql & "         when 'c' then 'Cancelada' " & vbNewLine
'    sSql = sSql & "         when 'm' then 'Pedido de Laudo Emitido'" & vbNewLine
'    sSql = sSql & "         when 'r' then 'Recusa'" & vbNewLine
'    '***********************************************************************************
'    'Alterado por Cleber da Stefanini - Data: 11/12/2004
'    'A proposta que estava aguardando renova��o ela ficava no grid sem descri��o nenhuma,
'    '   pois n�o tinha um tratamento para a situa��o do tipo 'o'.
'    'Foi adicionado a situa��o 'o' abaixo.
'    sSql = sSql & "         when 'o' then 'Aguardando Renova��o'" & vbNewLine
'    '***********************************************************************************
'    sSql = sSql & "         when 'i' then " & vbNewLine
'    sSql = sSql & "               case (select count(*)" & vbNewLine
'    sSql = sSql & "                       from proposta_suspensa_tb s  WITH (NOLOCK) " & vbNewLine
'    sSql = sSql & "                      where a.proposta_id = s.proposta_id" & vbNewLine
'    sSql = sSql & "                        and (dt_fim_suspensao is null" & vbNewLine
'    sSql = sSql & "                             or (dt_fim_suspensao is not null" & vbNewLine
'    sSql = sSql & "                        and  cod_suspensao = '1')))" & vbNewLine
'    sSql = sSql & "               when 0 then 'Certificado Emitido'" & vbNewLine
'    sSql = sSql & "               else 'Suspensa'" & vbNewLine
'    sSql = sSql & "               end" & vbNewLine
'    sSql = sSql & "         when 'p' then 'Em Estudo (erro de cadastramento)'" & vbNewLine
'    sSql = sSql & "         when 's' then 'Carta Recusa Emitida' " & vbNewLine
'    sSql = sSql & "          end descr_situacao " & vbNewLine
'
'    Select Case cboTipoSelecao.Text
'
'        Case "N� da Proposta SC"
'
'            sSqlAuxA = sSqlAuxA & "  From proposta_tb a  WITH (NOLOCK)  " & vbNewLine
'            sSqlAuxA = sSqlAuxA & "  Join cliente_tb b   WITH (NOLOCK) " & vbNewLine
'            sSqlAuxA = sSqlAuxA & "    on a.prop_cliente_id = b.cliente_id  " & vbNewLine
'            sSqlAuxA = sSqlAuxA & "  Join proposta_adesao_tb c  WITH (NOLOCK) " & vbNewLine
'            sSqlAuxA = sSqlAuxA & "    on a.proposta_id = c.proposta_id " & vbNewLine
'            sSqlAuxA = sSqlAuxA & "    Join SANTA_CATARINA_LEGADO_TB sc on sc.alianca_cd_certificado = a.proposta_id " & vbNewLine
'            sSqlAuxA = sSqlAuxA & " Where sc.santa_catarina_nu_proposta = " & txtArgumento.Text & vbNewLine
'
'            sSqlAuxF = sSqlAuxF & "  From proposta_tb a  WITH (NOLOCK) " & vbNewLine
'            sSqlAuxF = sSqlAuxF & "  Join cliente_tb b  WITH (NOLOCK) " & vbNewLine
'            sSqlAuxF = sSqlAuxF & "    on a.prop_cliente_id = b.cliente_id " & vbNewLine
'            sSqlAuxF = sSqlAuxF & "  Join proposta_fechada_tb c  WITH (NOLOCK) " & vbNewLine
'            sSqlAuxF = sSqlAuxF & "    on a.proposta_id = c.proposta_id" & vbNewLine
'            sSqlAuxF = sSqlAuxF & "    Join SANTA_CATARINA_LEGADO_TB sc on sc.alianca_cd_certificado = a.proposta_id " & vbNewLine
'            sSqlAuxF = sSqlAuxF & " Where sc.santa_catarina_nu_proposta = " & txtArgumento.Text & vbNewLine
'
'            'pcarvalho - Microsseguro
'            'sSqlAuxF = sSqlAuxF & "   And a.produto_id IN (" & ProdIdOurovidaEmpresa & "," & ProdIdSegurBrasilAberta & "," & ProdIdSegurBrasilVida & "," & ProdIdSeguroVidaAliancaIII & "," & ProdIdBBSeguroVidaAgriculturaFamiliar & "," & ProdIdBBProtecaoFlex & ")" & vbNewLine
'            'sSqlAuxF = sSqlAuxF & "   And a.produto_id IN (" & ProdIdOurovidaEmpresa & "," & ProdIdSegurBrasilAberta & "," & ProdIdSegurBrasilVida & "," & ProdIdSeguroVidaAliancaIII & "," & ProdIdBBSeguroVidaAgriculturaFamiliar & "," & ProdIdBBProtecaoFlex & "," & ProdBBMicrosseguroProtecaoPessoal & ")" & vbNewLine
'            'Jessica.Adao - Confitec Sistemas - Demanda 16429073 - Amparo Familiar - Inclusao do produto 1217
'            sSqlAuxF = sSqlAuxF & "   And a.produto_id IN (" & ProdIdOurovidaEmpresa & "," & ProdIdSegurBrasilAberta & "," & ProdIdSegurBrasilVida & "," & ProdIdSeguroVidaAliancaIII & "," & ProdIdBBSeguroVidaAgriculturaFamiliar & "," & ProdIdBBProtecaoFlex & "," & ProdBBMicrosseguroProtecaoPessoal & "," & ProdBBSeguroAmparoFamiliar & ")" & vbNewLine
'        Case "N� da Certificado SC"
'
'            sSqlAuxA = sSqlAuxA & "  From proposta_tb a  WITH (NOLOCK)  " & vbNewLine
'            sSqlAuxA = sSqlAuxA & "  Join cliente_tb b   WITH (NOLOCK) " & vbNewLine
'            sSqlAuxA = sSqlAuxA & "    on a.prop_cliente_id = b.cliente_id  " & vbNewLine
'            sSqlAuxA = sSqlAuxA & "  Join proposta_adesao_tb c  WITH (NOLOCK) " & vbNewLine
'            sSqlAuxA = sSqlAuxA & "    on a.proposta_id = c.proposta_id " & vbNewLine
'            sSqlAuxA = sSqlAuxA & "    Join SANTA_CATARINA_LEGADO_TB sc on sc.alianca_cd_certificado = a.proposta_id " & vbNewLine
'            sSqlAuxA = sSqlAuxA & " Where sc.santa_catarina_cd_certificado = " & txtArgumento.Text & vbNewLine
'
'            sSqlAuxF = sSqlAuxF & "  From proposta_tb a  WITH (NOLOCK) " & vbNewLine
'            sSqlAuxF = sSqlAuxF & "  Join cliente_tb b  WITH (NOLOCK) " & vbNewLine
'            sSqlAuxF = sSqlAuxF & "    on a.prop_cliente_id = b.cliente_id " & vbNewLine
'            sSqlAuxF = sSqlAuxF & "  Join proposta_fechada_tb c  WITH (NOLOCK) " & vbNewLine
'            sSqlAuxF = sSqlAuxF & "    on a.proposta_id = c.proposta_id" & vbNewLine
'            sSqlAuxF = sSqlAuxF & "    Join SANTA_CATARINA_LEGADO_TB sc on sc.alianca_cd_certificado = a.proposta_id " & vbNewLine
'            sSqlAuxF = sSqlAuxF & " Where sc.santa_catarina_cd_certificado = " & txtArgumento.Text & vbNewLine
'
'            'pcarvalho - Microsseguro
'            'sSqlAuxF = sSqlAuxF & "   And a.produto_id IN (" & ProdIdOurovidaEmpresa & "," & ProdIdSegurBrasilAberta & "," & ProdIdSegurBrasilVida & "," & ProdIdSeguroVidaAliancaIII & "," & ProdIdBBSeguroVidaAgriculturaFamiliar & "," & ProdIdBBProtecaoFlex & ")" & vbNewLine
'            'sSqlAuxF = sSqlAuxF & "   And a.produto_id IN (" & ProdIdOurovidaEmpresa & "," & ProdIdSegurBrasilAberta & "," & ProdIdSegurBrasilVida & "," & ProdIdSeguroVidaAliancaIII & "," & ProdIdBBSeguroVidaAgriculturaFamiliar & "," & ProdIdBBProtecaoFlex & "," & ProdBBMicrosseguroProtecaoPessoal & ")" & vbNewLine
'                        'Jessica.Adao - Confitec Sistemas - Demanda 16429073 - Amparo Familiar - Inclusao do produto 1217
'            sSqlAuxF = sSqlAuxF & "   And a.produto_id IN (" & ProdIdOurovidaEmpresa & "," & ProdIdSegurBrasilAberta & "," & ProdIdSegurBrasilVida & "," & ProdIdSeguroVidaAliancaIII & "," & ProdIdBBSeguroVidaAgriculturaFamiliar & "," & ProdIdBBProtecaoFlex & "," & ProdBBMicrosseguroProtecaoPessoal & "," & ProdBBSeguroAmparoFamiliar & ")" & vbNewLine
'
'        Case "N� da Apolice SC"
'
'            sSqlAuxA = sSqlAuxA & "  From proposta_tb a  WITH (NOLOCK)  " & vbNewLine
'            sSqlAuxA = sSqlAuxA & "  Join cliente_tb b   WITH (NOLOCK) " & vbNewLine
'            sSqlAuxA = sSqlAuxA & "    on a.prop_cliente_id = b.cliente_id  " & vbNewLine
'            sSqlAuxA = sSqlAuxA & "  Join proposta_adesao_tb c  WITH (NOLOCK) " & vbNewLine
'            sSqlAuxA = sSqlAuxA & "    on a.proposta_id = c.proposta_id " & vbNewLine
'            sSqlAuxA = sSqlAuxA & "    Join SANTA_CATARINA_LEGADO_TB sc on sc.alianca_cd_certificado = a.proposta_id " & vbNewLine
'            sSqlAuxA = sSqlAuxA & " Where sc.santa_catarina_cd_apolice = " & txtArgumento.Text & vbNewLine
'
'            sSqlAuxF = sSqlAuxF & "  From proposta_tb a  WITH (NOLOCK) " & vbNewLine
'            sSqlAuxF = sSqlAuxF & "  Join cliente_tb b  WITH (NOLOCK) " & vbNewLine
'            sSqlAuxF = sSqlAuxF & "    on a.prop_cliente_id = b.cliente_id " & vbNewLine
'            sSqlAuxF = sSqlAuxF & "  Join proposta_fechada_tb c  WITH (NOLOCK) " & vbNewLine
'            sSqlAuxF = sSqlAuxF & "    on a.proposta_id = c.proposta_id" & vbNewLine
'            sSqlAuxF = sSqlAuxF & "    Join SANTA_CATARINA_LEGADO_TB sc on sc.alianca_cd_certificado = a.proposta_id " & vbNewLine
'            sSqlAuxF = sSqlAuxF & " Where sc.santa_catarina_cd_apolice = " & txtArgumento.Text & vbNewLine
'
'            'pcarvalho - Microsseguro
'            'sSqlAuxF = sSqlAuxF & "   And a.produto_id IN (" & ProdIdOurovidaEmpresa & "," & ProdIdSegurBrasilAberta & "," & ProdIdSegurBrasilVida & "," & ProdIdSeguroVidaAliancaIII & "," & ProdIdBBSeguroVidaAgriculturaFamiliar & "," & ProdIdBBProtecaoFlex & ")" & vbNewLine
'            'sSqlAuxF = sSqlAuxF & "   And a.produto_id IN (" & ProdIdOurovidaEmpresa & "," & ProdIdSegurBrasilAberta & "," & ProdIdSegurBrasilVida & "," & ProdIdSeguroVidaAliancaIII & "," & ProdIdBBSeguroVidaAgriculturaFamiliar & "," & ProdIdBBProtecaoFlex & "," & ProdBBMicrosseguroProtecaoPessoal & ")" & vbNewLine
'            'Jessica.Adao - Confitec Sistemas - Demanda 16429073 - Amparo Familiar - Inclusao do produto 1217
'            sSqlAuxF = sSqlAuxF & "   And a.produto_id IN (" & ProdIdOurovidaEmpresa & "," & ProdIdSegurBrasilAberta & "," & ProdIdSegurBrasilVida & "," & ProdIdSeguroVidaAliancaIII & "," & ProdIdBBSeguroVidaAgriculturaFamiliar & "," & ProdIdBBProtecaoFlex & "," & ProdBBMicrosseguroProtecaoPessoal & "," & ProdBBSeguroAmparoFamiliar & ")" & vbNewLine
'        Case "N� da Proposta"
'
'            '          sSQL = sSQL & "  From proposta_tb a WITH (NOLOCK)  " & vbNewLine
'            '          sSQL = sSQL & "  Join cliente_tb b   WITH (NOLOCK) " & vbNewLine
'            '          sSQL = sSQL & "    on a.prop_cliente_id = b.cliente_id  " & vbNewLine
'            '          sSQL = sSQL & "  Join proposta_adesao_tb c  WITH (NOLOCK) " & vbNewLine
'            '          sSQL = sSQL & "    on a.proposta_id = c.proposta_id " & vbNewLine
'            '          sSQL = sSQL & " Where a.proposta_id = " & txtArgumento.Text & vbNewLine
'
'            sSqlAuxA = sSqlAuxA & "  From proposta_tb a  WITH (NOLOCK)  " & vbNewLine
'            sSqlAuxA = sSqlAuxA & "  Join cliente_tb b   WITH (NOLOCK) " & vbNewLine
'            sSqlAuxA = sSqlAuxA & "    on a.prop_cliente_id = b.cliente_id  " & vbNewLine
'            sSqlAuxA = sSqlAuxA & "  Join proposta_adesao_tb c  WITH (NOLOCK) " & vbNewLine
'            sSqlAuxA = sSqlAuxA & "    on a.proposta_id = c.proposta_id " & vbNewLine
'            sSqlAuxA = sSqlAuxA & " Where a.proposta_id = " & txtArgumento.Text & vbNewLine
'            sSqlAuxA = sSqlAuxA & " and a.situacao not in ('x','t')" & vbNewLine
'
'            sSqlAuxF = sSqlAuxF & "  From proposta_tb a  WITH (NOLOCK) " & vbNewLine
'            sSqlAuxF = sSqlAuxF & "  Join cliente_tb b  WITH (NOLOCK) " & vbNewLine
'            sSqlAuxF = sSqlAuxF & "    on a.prop_cliente_id = b.cliente_id " & vbNewLine
'            sSqlAuxF = sSqlAuxF & "  Join proposta_fechada_tb c  WITH (NOLOCK) " & vbNewLine
'            sSqlAuxF = sSqlAuxF & "    on a.proposta_id = c.proposta_id" & vbNewLine
'            sSqlAuxF = sSqlAuxF & " Where a.proposta_id  = " & txtArgumento.Text & vbNewLine
'            sSqlAuxF = sSqlAuxF & " and a.situacao not in ('x','t')" & vbNewLine
'
'
'
'            'Luciana - 24/02/2005 - O produto 123 deve ser consultado no SEGP0223, pois este programa n�o est� preparado para buscar todos os dados da proposta
'            '          sSqlAuxF = sSqlAuxF & "   And a.produto_id IN (" & ProdIdOurovidaEmpresa & "," & _
'            '                                                             ProdIdSegurBrasilAberta & "," & _
'            '                                                             ProdIdAvulsoOnLine & "," & _
'            '                                                             ProdIdSegurBrasilVida & "," & _
'            '                                                             ProdIdSeguroVidaAliancaIII & "," & _
'            '                                                             ProdIdBBSeguroVidaAgriculturaFamiliar & ")" & vbNewLine
'
'            'pcarvalho - Microsseguro
'            'sSqlAuxF = sSqlAuxF & "   And a.produto_id IN (" & ProdIdOurovidaEmpresa & "," & ProdIdSegurBrasilAberta & "," & ProdIdSegurBrasilVida & "," & ProdIdSeguroVidaAliancaIII & "," & ProdIdBBSeguroVidaAgriculturaFamiliar & "," & ProdIdBBProtecaoFlex & ")" & vbNewLine
'            'sSqlAuxF = sSqlAuxF & "   And a.produto_id IN (" & ProdIdOurovidaEmpresa & "," & ProdIdSegurBrasilAberta & "," & ProdIdSegurBrasilVida & "," & ProdIdSeguroVidaAliancaIII & "," & ProdIdBBSeguroVidaAgriculturaFamiliar & "," & ProdIdBBProtecaoFlex & "," & ProdBBMicrosseguroProtecaoPessoal & ")" & vbNewLine
'                        'Jessica.Adao - Confitec Sistemas - Demanda 16429073 - Amparo Familiar - Inclusao do produto 1217
'            sSqlAuxF = sSqlAuxF & "   And a.produto_id IN (" & ProdIdOurovidaEmpresa & "," & ProdIdSegurBrasilAberta & "," & ProdIdSegurBrasilVida & "," & ProdIdSeguroVidaAliancaIII & "," & ProdIdBBSeguroVidaAgriculturaFamiliar & "," & ProdIdBBProtecaoFlex & "," & ProdBBMicrosseguroProtecaoPessoal & "," & ProdBBSeguroAmparoFamiliar & ")" & vbNewLine
'        Case "N� do Cliente"
'
'            '          sSQL = sSQL & "  From proposta_tb a WITH (NOLOCK)  " & vbNewLine
'            '          sSQL = sSQL & "  Join cliente_tb b  WITH (NOLOCK) " & vbNewLine
'            '          sSQL = sSQL & "    on a.prop_cliente_id = b.cliente_id  " & vbNewLine
'            '          sSQL = sSQL & "  Join proposta_adesao_tb c WITH (NOLOCK)  " & vbNewLine
'            '          sSQL = sSQL & "    on a.proposta_id = c.proposta_id " & vbNewLine
'            '          sSQL = sSQL & " Where b.cliente_id = " & txtArgumento.Text & vbNewLine
'
'            sSqlAuxA = sSqlAuxA & "  From proposta_tb a  WITH (NOLOCK)  " & vbNewLine
'            sSqlAuxA = sSqlAuxA & "  Join cliente_tb b  WITH (NOLOCK) " & vbNewLine
'            sSqlAuxA = sSqlAuxA & "    on a.prop_cliente_id = b.cliente_id  " & vbNewLine
'            sSqlAuxA = sSqlAuxA & "  Join proposta_adesao_tb c  WITH (NOLOCK)  " & vbNewLine
'            sSqlAuxA = sSqlAuxA & "    on a.proposta_id = c.proposta_id " & vbNewLine
'            sSqlAuxA = sSqlAuxA & " Where b.cliente_id = " & txtArgumento.Text & vbNewLine
'            sSqlAuxA = sSqlAuxA & " and a.situacao not in ('x','t') " & vbNewLine
'
'            sSqlAuxF = sSqlAuxF & "  From proposta_tb a  WITH (NOLOCK) " & vbNewLine
'            sSqlAuxF = sSqlAuxF & "  Join cliente_tb b  WITH (NOLOCK) " & vbNewLine
'            sSqlAuxF = sSqlAuxF & "    on a.prop_cliente_id = b.cliente_id " & vbNewLine
'            sSqlAuxF = sSqlAuxF & "  Join proposta_fechada_tb c  WITH (NOLOCK) " & vbNewLine
'            sSqlAuxF = sSqlAuxF & "    on a.proposta_id = c.proposta_id" & vbNewLine
'            sSqlAuxF = sSqlAuxF & " Where b.cliente_id  = " & txtArgumento.Text & vbNewLine
'            sSqlAuxF = sSqlAuxF & " and a.situacao not in ('x','t') " & vbNewLine
'
'            'pcarvalho - Microsseguro
'            'sSqlAuxF = sSqlAuxF & "   And a.produto_id IN (" & ProdIdOurovidaEmpresa & "," & ProdIdSegurBrasilAberta & "," & ProdIdAvulsoOnLine & "," & ProdIdSegurBrasilVida & "," & ProdIdSeguroVidaAliancaIII & "," & ProdIdBBSeguroVidaAgriculturaFamiliar & "," & ProdIdBBProtecaoFlex & ")" & vbNewLine
'            'sSqlAuxF = sSqlAuxF & "   And a.produto_id IN (" & ProdIdOurovidaEmpresa & "," & ProdIdSegurBrasilAberta & "," & ProdIdAvulsoOnLine & "," & ProdIdSegurBrasilVida & "," & ProdIdSeguroVidaAliancaIII & "," & ProdIdBBSeguroVidaAgriculturaFamiliar & "," & ProdIdBBProtecaoFlex & "," & ProdBBMicrosseguroProtecaoPessoal & ")" & vbNewLine
'            'Jessica.Adao - Confitec Sistemas - Demanda 16429073 - Amparo Familiar - Inclusao do produto 1217
'            sSqlAuxF = sSqlAuxF & "   And a.produto_id IN (" & ProdIdOurovidaEmpresa & "," & ProdIdSegurBrasilAberta & "," & ProdIdAvulsoOnLine & "," & ProdIdSegurBrasilVida & "," & ProdIdSeguroVidaAliancaIII & "," & ProdIdBBSeguroVidaAgriculturaFamiliar & "," & ProdIdBBProtecaoFlex & "," & ProdBBMicrosseguroProtecaoPessoal & "," & ProdBBSeguroAmparoFamiliar & ")" & vbNewLine
'        Case "N� da Proposta BB"
'
'            'Verificando ocorrencias em proposta adesao
'            lCodSegmento = BuscarPropostaSegmento(ProdIdSeguroConsorcio, 0, txtArgumento.Text)
'
'            'Verificando ocorrencias em proposta adesao
'
'            If lCodSegmento = 0 Then
'
'                sSqlAuxA = sSqlAuxA & "  From proposta_tb a  WITH (NOLOCK) " & vbNewLine
'                sSqlAuxA = sSqlAuxA & "  Join cliente_tb b  WITH (NOLOCK) " & vbNewLine
'                sSqlAuxA = sSqlAuxA & "    on a.prop_cliente_id = b.cliente_id " & vbNewLine
'                sSqlAuxA = sSqlAuxA & "  Join proposta_adesao_tb c  WITH (NOLOCK) " & vbNewLine
'                sSqlAuxA = sSqlAuxA & "    On  a.proposta_id = C.proposta_id " & vbNewLine
'                sSqlAuxA = sSqlAuxA & " Where c.proposta_bb = " & txtArgumento.Text & vbNewLine
'                sSqlAuxA = sSqlAuxA & " and a.situacao <> 'x'" & vbNewLine
'
'                If Trim(TxtNrAgencia.Text) <> "" Then
'                    sSqlAuxA = sSqlAuxA & " and c.cont_agencia_id = " & TxtNrAgencia.Text & vbNewLine
'                End If
'
'            Else
'                sSqlAuxA = "                     From proposta_tb a, cliente_tb b, proposta_adesao_tb c"
'                sSqlAuxA = sSqlAuxA & " where a.proposta_id = c.proposta_id"
'                sSqlAuxA = sSqlAuxA & " and   a.situacao <> 'x'" & vbNewLine
'                sSqlAuxA = sSqlAuxA & " and   a.prop_cliente_id = b.cliente_id "
'                sSqlAuxA = sSqlAuxA & " and   a.produto_id = " & ProdIdSeguroConsorcio
'                sSqlAuxA = sSqlAuxA & " and   c.proposta_id in ("
'                sSqlAuxA = sSqlAuxA & "       select distinct proposta_id "
'                sSqlAuxA = sSqlAuxA & "       from   seguro_consorcio_tb d "
'                sSqlAuxA = sSqlAuxA & " where d.codigo_segmento = " & lCodSegmento
'                sSqlAuxA = sSqlAuxA & "                                   )"
'
'            End If
'
'            'Verificando ocorrencias em proposta fechada
'
'            sSqlAuxF = sSqlAuxF & "  From proposta_tb a  WITH (NOLOCK) " & vbNewLine
'            sSqlAuxF = sSqlAuxF & "  Join cliente_tb b  WITH (NOLOCK) " & vbNewLine
'            sSqlAuxF = sSqlAuxF & "    on a.prop_cliente_id = b.cliente_id " & vbNewLine
'            sSqlAuxF = sSqlAuxF & "  Join proposta_fechada_tb c  WITH (NOLOCK) " & vbNewLine
'            sSqlAuxF = sSqlAuxF & "    on a.proposta_id = c.proposta_id" & vbNewLine
'            sSqlAuxF = sSqlAuxF & " Where c.proposta_bb = " & txtArgumento.Text & vbNewLine
'            '          sSqlAuxF = sSqlAuxF & "   And a.produto_id = " & ProdIdOurovidaEmpresa
'
'            'pcarvalho - Microsseguro
'            'sSqlAuxF = sSqlAuxF & "   And a.produto_id IN (" & ProdIdOurovidaEmpresa & "," & ProdIdSegurBrasilAberta & "," & ProdIdAvulsoOnLine & "," & ProdIdSegurBrasilVida & "," & ProdIdSeguroVidaAliancaIII & "," & ProdIdBBSeguroVidaAgriculturaFamiliar & "," & ProdIdBBProtecaoFlex & ")" & vbNewLine
'            'sSqlAuxF = sSqlAuxF & "   And a.produto_id IN (" & ProdIdOurovidaEmpresa & "," & ProdIdSegurBrasilAberta & "," & ProdIdAvulsoOnLine & "," & ProdIdSegurBrasilVida & "," & ProdIdSeguroVidaAliancaIII & "," & ProdIdBBSeguroVidaAgriculturaFamiliar & "," & ProdIdBBProtecaoFlex & "," & ProdBBMicrosseguroProtecaoPessoal & ")" & vbNewLine
'                        'Jessica.Adao - Confitec Sistemas - Demanda 16429073 - Amparo Familiar - Inclusao do produto 1217
'            sSqlAuxF = sSqlAuxF & "   And a.produto_id IN (" & ProdIdOurovidaEmpresa & "," & ProdIdSegurBrasilAberta & "," & ProdIdAvulsoOnLine & "," & ProdIdSegurBrasilVida & "," & ProdIdSeguroVidaAliancaIII & "," & ProdIdBBSeguroVidaAgriculturaFamiliar & "," & ProdIdBBProtecaoFlex & "," & ProdBBMicrosseguroProtecaoPessoal & "," & ProdBBSeguroAmparoFamiliar & ")" & vbNewLine
'
'            If Trim(TxtNrAgencia) <> "" Then
'                sSqlAuxF = sSqlAuxF & " and c.cont_agencia_id = " & TxtNrAgencia.Text & vbNewLine
'            End If
'
'            'End Select
'
'            'carolina.marinho 15/04/2011 - Demanda 9106683 - Projeto BB Seguro Prestamista Consorcio
'        Case "N� Grupo e Cota"
'
'            sSqlAuxA = sSqlAuxA & "  From proposta_tb a  WITH (NOLOCK)  " & vbNewLine
'            sSqlAuxA = sSqlAuxA & "  Join cliente_tb b  WITH (NOLOCK) " & vbNewLine
'            sSqlAuxA = sSqlAuxA & "    on a.prop_cliente_id = b.cliente_id  " & vbNewLine
'            sSqlAuxA = sSqlAuxA & "  Join proposta_adesao_tb c  WITH (NOLOCK) " & vbNewLine
'            sSqlAuxA = sSqlAuxA & "    on a.proposta_id = c.proposta_id " & vbNewLine
'
'            sSqlAuxA = sSqlAuxA & "  join seguro_consorcio_tb  WITH (NOLOCK) " & vbNewLine
'            sSqlAuxA = sSqlAuxA & "    on seguro_consorcio_tb.proposta_id = a.proposta_id " & vbNewLine
'            sSqlAuxA = sSqlAuxA & " Where seguro_consorcio_tb.codigo_segmento = " & cboSegmento.ItemData(cboSegmento.ListIndex) & vbNewLine
'            sSqlAuxA = sSqlAuxA & "   and seguro_consorcio_tb.grupo = " & Trim(txtGrupo.Text) & vbNewLine
'            sSqlAuxA = sSqlAuxA & "   and seguro_consorcio_tb.cota = " & Trim(txtCota.Text) & vbNewLine
'
'            sSqlAuxF = sSqlAuxF & "  From proposta_tb a  WITH (NOLOCK) " & vbNewLine
'            sSqlAuxF = sSqlAuxF & "  Join cliente_tb b  WITH (NOLOCK) " & vbNewLine
'            sSqlAuxF = sSqlAuxF & "    on a.prop_cliente_id = b.cliente_id " & vbNewLine
'            sSqlAuxF = sSqlAuxF & "  Join proposta_fechada_tb c  WITH (NOLOCK) " & vbNewLine
'            sSqlAuxF = sSqlAuxF & "    on a.proposta_id = c.proposta_id" & vbNewLine
'            sSqlAuxF = sSqlAuxF & "  join seguro_consorcio_tb  WITH (NOLOCK) " & vbNewLine
'            sSqlAuxF = sSqlAuxF & "    on seguro_consorcio_tb.proposta_id = a.proposta_id " & vbNewLine
'            sSqlAuxF = sSqlAuxF & " Where seguro_consorcio_tb.codigo_segmento = " & cboSegmento.ItemData(cboSegmento.ListIndex) & vbNewLine
'            sSqlAuxF = sSqlAuxF & "   and seguro_consorcio_tb.grupo = " & Trim(txtGrupo.Text) & vbNewLine
'            sSqlAuxF = sSqlAuxF & "   and seguro_consorcio_tb.cota = " & Trim(txtCota.Text) & vbNewLine
'
'        Case "N� da Proposta Internet"
'
'            sSqlAuxA = sSqlAuxA & "  From proposta_tb a  WITH (NOLOCK)  " & vbNewLine
'            sSqlAuxA = sSqlAuxA & "  Join cliente_tb b  WITH (NOLOCK) " & vbNewLine
'            sSqlAuxA = sSqlAuxA & "    on a.prop_cliente_id = b.cliente_id  " & vbNewLine
'            sSqlAuxA = sSqlAuxA & "  Join proposta_adesao_tb c  WITH (NOLOCK) " & vbNewLine
'            sSqlAuxA = sSqlAuxA & "    on a.proposta_id = c.proposta_id " & vbNewLine
'            sSqlAuxA = sSqlAuxA & "  Join proposta_internet_tb d  WITH (NOLOCK) " & vbNewLine
'            sSqlAuxA = sSqlAuxA & "    on a.proposta_id = d.proposta_id " & vbNewLine
'            sSqlAuxA = sSqlAuxA & " Where d.proposta_internet_id = " & txtArgumento.Text & vbNewLine
'
'            sSqlAuxF = sSqlAuxF & "  From proposta_tb a  WITH (NOLOCK) " & vbNewLine
'            sSqlAuxF = sSqlAuxF & "  Join cliente_tb b  WITH (NOLOCK) " & vbNewLine
'            sSqlAuxF = sSqlAuxF & "    on a.prop_cliente_id = b.cliente_id " & vbNewLine
'            sSqlAuxF = sSqlAuxF & "  Join proposta_fechada_tb c  WITH (NOLOCK) " & vbNewLine
'            sSqlAuxF = sSqlAuxF & "    on a.proposta_id = c.proposta_id" & vbNewLine
'            sSqlAuxF = sSqlAuxF & "  Join proposta_internet_tb d  WITH (NOLOCK) " & vbNewLine
'            sSqlAuxF = sSqlAuxF & "    on a.proposta_id = d.proposta_id" & vbNewLine
'            sSqlAuxF = sSqlAuxF & " Where d.proposta_internet_id  = " & txtArgumento.Text & vbNewLine
'            sSqlAuxF = sSqlAuxF & " and upper(a.situacao)  <> 'X' " & vbNewLine
'
'            'Luciana - 24/02/2005 - O produto 123 deve ser consultado no SEGP0223, pois este programa n�o est� preparado para buscar todos os dados da proposta
'            '          sSqlAuxF = sSqlAuxF & "   And a.produto_id IN (" & ProdIdOurovidaEmpresa & "," & _
'            '                                                             ProdIdSegurBrasilAberta & "," & _
'            '                                                             ProdIdAvulsoOnLine & "," & _
'            '                                                             ProdIdSegurBrasilVida & "," & _
'            '                                                             ProdIdSeguroVidaAliancaIII & "," & _
'            '                                                             ProdIdBBSeguroVidaAgriculturaFamiliar & ")" & vbNewLine
'
'            'pcarvalho - Microsseguro
'            'sSqlAuxF = sSqlAuxF & "   And a.produto_id IN (" & ProdIdOurovidaEmpresa & "," & ProdIdSegurBrasilAberta & "," & ProdIdSegurBrasilVida & "," & ProdIdSeguroVidaAliancaIII & "," & ProdIdBBSeguroVidaAgriculturaFamiliar & "," & ProdIdBBProtecaoFlex & ")" & vbNewLine
'            'sSqlAuxF = sSqlAuxF & "   And a.produto_id IN (" & ProdIdOurovidaEmpresa & "," & ProdIdSegurBrasilAberta & "," & ProdIdSegurBrasilVida & "," & ProdIdSeguroVidaAliancaIII & "," & ProdIdBBSeguroVidaAgriculturaFamiliar & "," & ProdIdBBProtecaoFlex & "," & ProdBBMicrosseguroProtecaoPessoal & ")" & vbNewLine
'                        'Jessica.Adao - Confitec Sistemas - Demanda 16429073 - Amparo Familiar - Inclusao do produto 1217
'            sSqlAuxF = sSqlAuxF & "   And a.produto_id IN (" & ProdIdOurovidaEmpresa & "," & ProdIdSegurBrasilAberta & "," & ProdIdSegurBrasilVida & "," & ProdIdSeguroVidaAliancaIII & "," & ProdIdBBSeguroVidaAgriculturaFamiliar & "," & ProdIdBBProtecaoFlex & "," & ProdBBMicrosseguroProtecaoPessoal & "," & ProdBBSeguroAmparoFamiliar & ")" & vbNewLine
'
'    End Select
'
'    'carolina.marinho 19/04/2011 - Demanda 9106683 - Projeto BB Seguro Prestamista Consorcio
'    'Visto que na seguro_consorcio_tb s� tem propostas do produto 718, n�o se faz necess�rio
'    'o filtro por produto, otimizando o processo.
'    If cboTipoSelecao.Text <> "N� Grupo e Cota" Then
'
'        'para garantir a sele��o de propostas somente para produtos de vida
'
'        sSqlW = sSqlW & " and (a.produto_id in" & vbNewLine
'        sSqlW = sSqlW & "                   (Select Distinct produto_id" & vbNewLine
'        sSqlW = sSqlW & "                    From item_produto_tb i, ramo_tb r, tp_ramo_tb t  WITH (NOLOCK) " & vbNewLine
'        sSqlW = sSqlW & "                    Where i.ramo_id = r.ramo_id" & vbNewLine
'        sSqlW = sSqlW & "                          and r.tp_ramo_id = t.tp_ramo_id" & vbNewLine
'        sSqlW = sSqlW & "                          and i.dt_fim_vigencia is NULL " & vbNewLine
'        'pcarvalho - Microsseguro
'        'sSqlW = sSqlW & "                          and t.tp_ramo_id = " & TpRamoVida & ") " & vbNewLine
'        sSqlW = sSqlW & "                          and (t.tp_ramo_id = " & TpRamoVida & " OR produto_id = 1218)) " & vbNewLine
'
'        'pegar somente propostas antigas do Penhor Rural
'
'        sSqlW = sSqlW & "      or produto_id = " & ProdIdOuroAgricola & vbNewLine
'
'        sSqlW = sSqlW & "      or (a.produto_id = " & ProdIdPenhorRural & vbNewLine
'        sSqlW = sSqlW & "           and a.proposta_id not in (" & vbNewLine
'        sSqlW = sSqlW & "                            SELECT isnull(proposta_id,0)" & vbNewLine
'        sSqlW = sSqlW & "                              FROM emi_re_proposta_tb  WITH (NOLOCK) " & vbNewLine
'        sSqlW = sSqlW & "                             WHERE arquivo like 'SEG488%')))" & vbNewLine
'
'        'Alessandra Grig�rio - 10/08/2004 - N�o deixar carregar no grid propostas recusadas
'        'A pedido de Paulo Almeida - Retirado (verificar coment�rio abaixo)
'
'        'Comentado abaixo por Stefanini Consultoria (Maur�cio) em 06/10/2004
'        'A pedido de Thais Regina, pois informou que dever�o aparecer as propostas Recusadas no grid.
'
'        'sSqlW = sSqlW & " and a.situacao not in ('r') "
'    End If
'
'    ' Uniao das queries
'    If Len(sSqlAuxA) > 0 Then
'
'        sSql = sSql & sSqlAuxA & sSqlW & " UNION " & sSql & sSqlAuxF & sSqlW
'    Else
'
'        sSql = sSql & sSqlW
'
'    End If
'    'Inicio: Data:14/05/2013 Autor:Rodrigo.Moura incidente:
'    'sSql = sSql & " AND A.SITUACAO <> 'T' "
'     sSql = sSql & " AND A.SITUACAO not in ('X','T') "
'    'Fim: Data:14/05/2013 Autor:Rodrigo.Moura incidente:
'    sSql = sSql & " Order By dt_contratacao "
'
'    Set Pesquisar = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, sSql, True)
'
'    Exit Function
'
'TrataErro:
'    Call TratarErro("Pesquisar", Me.name)
'    Call FinalizarAplicacao
'
'End Function.

    On Error GoTo TrataErro

    Dim sSql         As String
    Dim sSql_temp    As String
    Dim sSqlAuxA     As String
    Dim sSqlAuxF     As String
    Dim sSqlW        As String
    Dim lCodSegmento As Long

    sSql = ""
    sSqlAuxA = ""
    sSqlAuxF = ""
    sSqlW = ""

    sSql = sSql & " SELECT DISTINCT " & vbNewLine
    sSql = sSql & "       a.proposta_id, " & vbNewLine
    sSql = sSql & "       a.dt_contratacao, " & vbNewLine
    sSql = sSql & "       b.nome, " & vbNewLine
    sSql = sSql & "       a.prop_cliente_id, " & vbNewLine
    sSql = sSql & "       a.produto_id," & vbNewLine
    sSql = sSql & "       seg_cliente = UPPER(ISNULL(b.seg_cliente,''))," & vbNewLine
    sSql = sSql & "       case situacao" & vbNewLine
    sSql = sSql & "         when 'e' then 'Em Estudo'" & vbNewLine
    sSql = sSql & "         when 'a' then 'Emitida'" & vbNewLine
    sSql = sSql & "         when 'v' then 'Em Avalia��o M�dica'" & vbNewLine
    sSql = sSql & "         when 'l' then 'Pedido de Laudo'" & vbNewLine
    sSql = sSql & "         when 'c' then 'Cancelada' " & vbNewLine
    sSql = sSql & "         when 'm' then 'Pedido de Laudo Emitido'" & vbNewLine
    sSql = sSql & "         when 'r' then 'Recusa'" & vbNewLine
    sSql = sSql & "         when 'o' then 'Aguardando Renova��o'" & vbNewLine
    sSql = sSql & "         when 'i' then " & vbNewLine
    sSql = sSql & "               case (select count(*)" & vbNewLine
    sSql = sSql & "                       from proposta_suspensa_tb s WITH (NOLOCK) " & vbNewLine
    sSql = sSql & "                      where a.proposta_id = s.proposta_id" & vbNewLine
    sSql = sSql & "                        and (dt_fim_suspensao is null" & vbNewLine
    sSql = sSql & "                             or (dt_fim_suspensao is not null" & vbNewLine
    sSql = sSql & "                        and  cod_suspensao = '1')))" & vbNewLine
    sSql = sSql & "               when 0 then 'Certificado Emitido'" & vbNewLine
    sSql = sSql & "               else 'Suspensa'" & vbNewLine
    sSql = sSql & "               end" & vbNewLine
    sSql = sSql & "         when 'p' then 'Em Estudo (erro de cadastramento)'" & vbNewLine
    sSql = sSql & "         when 's' then 'Carta Recusa Emitida' " & vbNewLine
    sSql = sSql & "          end descr_situacao, " & vbNewLine
    sSql = sSql & "       a.origem_proposta_id " & vbNewLine
    sSql = sSql & "  From proposta_tb a  WITH (NOLOCK)  " & vbNewLine
    sSql = sSql & "  INNER JOIN cliente_tb b   WITH (NOLOCK) " & vbNewLine
    sSql = sSql & "    on a.prop_cliente_id = b.cliente_id  " & vbNewLine
    'Visto que na seguro_consorcio_tb s� tem propostas do produto 718, n�o se faz necess�rio
    'o filtro por produto, otimizando o processo.
    If cboTipoSelecao.Text <> "N� Grupo e Cota" Then
        'para garantir a sele��o de propostas somente para produtos de vida
        sSql = sSql & "  INNER JOIN item_produto_tb i  WITH (NOLOCK)  " & vbNewLine
        sSql = sSql & "          ON a.produto_id = i.produto_id  " & vbNewLine
        sSql = sSql & "  INNER JOIN ramo_tb r  WITH (NOLOCK)  " & vbNewLine
        sSql = sSql & "          ON i.ramo_id = r.ramo_id" & vbNewLine
        'Ver continua��o desta condi��o na cl�sula "WHERE"
    End If

    Select Case cboTipoSelecao.Text

        Case "N� da Proposta SC"
            sSqlAuxA = sSqlAuxA & "    INNER JOIN SANTA_CATARINA_LEGADO_TB sc  WITH (NOLOCK)  on sc.alianca_cd_certificado = a.proposta_id " & vbNewLine
            sSqlAuxA = sSqlAuxA & " Where sc.santa_catarina_nu_proposta = " & txtArgumento.Text & vbNewLine
        Case "N� da Certificado SC"
            sSqlAuxA = sSqlAuxA & "    Join SANTA_CATARINA_LEGADO_TB sc  WITH (NOLOCK)  on sc.alianca_cd_certificado = a.proposta_id " & vbNewLine
            sSqlAuxA = sSqlAuxA & " Where sc.santa_catarina_cd_certificado = " & txtArgumento.Text & vbNewLine
        Case "N� da Apolice SC"
            sSqlAuxA = sSqlAuxA & "    Join SANTA_CATARINA_LEGADO_TB sc  WITH (NOLOCK)  on sc.alianca_cd_certificado = a.proposta_id " & vbNewLine
            sSqlAuxA = sSqlAuxA & " Where sc.santa_catarina_cd_apolice = " & txtArgumento.Text & vbNewLine
        Case "N� da Proposta"
            sSqlAuxA = sSqlAuxA & " Where a.proposta_id  = " & txtArgumento.Text & vbNewLine
        Case "N� do Cliente"
            sSqlAuxA = sSqlAuxA & " Where b.cliente_id = " & txtArgumento.Text & vbNewLine
        Case "N� da Proposta Combinada"
            sSqlAuxA = sSqlAuxA & " Where a.proposta_oferta_agrupada  = '" & txtArgumento.Text & "'" & vbNewLine
        Case "N� da Proposta BB"
            'Verificando ocorrencias em proposta adesao
            lCodSegmento = BuscarPropostaSegmento(ProdIdSeguroConsorcio, 0, txtArgumento.Text)
            If lCodSegmento = 0 Then
                sSqlAuxA = sSqlAuxA & "  Join proposta_adesao_tb c  WITH (NOLOCK) " & vbNewLine
                sSqlAuxA = sSqlAuxA & "    On  a.proposta_id = C.proposta_id " & vbNewLine
                sSqlAuxA = sSqlAuxA & " Where c.proposta_bb = " & txtArgumento.Text & vbNewLine
                If Trim(TxtNrAgencia.Text) <> "" Then
                    sSqlAuxA = sSqlAuxA & " and c.cont_agencia_id = " & TxtNrAgencia.Text & vbNewLine
                End If
            Else
                sSqlAuxA = sSqlAuxA & "  Join proposta_adesao_tb c  WITH (NOLOCK) " & vbNewLine
                sSqlAuxA = sSqlAuxA & "    On  a.proposta_id = C.proposta_id " & vbNewLine
                sSqlAuxA = sSqlAuxA & " where a.produto_id = " & ProdIdSeguroConsorcio
                sSqlAuxA = sSqlAuxA & " and   EXISTS "
                sSqlAuxA = sSqlAuxA & "       (select 1 "
                sSqlAuxA = sSqlAuxA & "        from   seguro_consorcio_tb d  WITH (NOLOCK)  "
                sSqlAuxA = sSqlAuxA & "        where d.codigo_segmento = " & lCodSegmento
                sSqlAuxA = sSqlAuxA & "          AND d.proposta_id = c.proposta_id"
                sSqlAuxA = sSqlAuxA & "       ) "
            End If
            'Verificando ocorrencias em proposta fechada
            sSqlAuxF = sSqlAuxF & "  Join proposta_fechada_tb c  WITH (NOLOCK) " & vbNewLine
            sSqlAuxF = sSqlAuxF & "    on a.proposta_id = c.proposta_id" & vbNewLine
            sSqlAuxF = sSqlAuxF & " Where c.proposta_bb = " & txtArgumento.Text & vbNewLine
            If Trim(TxtNrAgencia) <> "" Then
                sSqlAuxF = sSqlAuxF & " and c.cont_agencia_id = " & TxtNrAgencia.Text & vbNewLine
            End If
        Case "N� Grupo e Cota"
            sSqlAuxA = sSqlAuxA & "  join seguro_consorcio_tb  WITH (NOLOCK) " & vbNewLine
            sSqlAuxA = sSqlAuxA & "    on seguro_consorcio_tb.proposta_id = a.proposta_id " & vbNewLine
            sSqlAuxA = sSqlAuxA & " Where seguro_consorcio_tb.codigo_segmento = " & cboSegmento.ItemData(cboSegmento.ListIndex) & vbNewLine
            sSqlAuxA = sSqlAuxA & "   and seguro_consorcio_tb.grupo = " & Trim(txtGrupo.Text) & vbNewLine
            sSqlAuxA = sSqlAuxA & "   and seguro_consorcio_tb.cota = " & Trim(txtCota.Text) & vbNewLine
        Case "N� da Proposta Internet"
            sSqlAuxA = sSqlAuxA & "  Join proposta_internet_tb d  WITH (NOLOCK) " & vbNewLine
            sSqlAuxA = sSqlAuxA & "    on a.proposta_id = d.proposta_id " & vbNewLine
            sSqlAuxA = sSqlAuxA & " Where d.proposta_internet_id = " & txtArgumento.Text & vbNewLine
    End Select

    'Visto que na seguro_consorcio_tb s� tem propostas do produto 718, n�o se faz necess�rio
    'o filtro por produto, otimizando o processo.
    If cboTipoSelecao.Text <> "N� Grupo e Cota" Then
        sSqlW = sSqlW & "  AND i.dt_fim_vigencia is NULL " & vbNewLine
        sSqlW = sSqlW & "  AND ((r.tp_ramo_id = " & TpRamoVida & " OR a.produto_id = " & ProdBBMicrosseguroProtecaoPessoal & ") " & vbNewLine

        'pegar somente propostas antigas do Penhor Rural
        sSqlW = sSqlW & "       or a.produto_id = " & ProdIdOuroAgricola & vbNewLine
        sSqlW = sSqlW & "      or (a.produto_id = " & ProdIdPenhorRural & vbNewLine

        sSqlW = sSqlW & "           AND NOT EXISTS (" & vbNewLine
        sSqlW = sSqlW & "                             SELECT 1 " & vbNewLine
        sSqlW = sSqlW & "                              FROM emi_re_proposta_tb  WITH (NOLOCK)  " & vbNewLine

        'GENJUNIOR - FLOW 17860335
        'UNION COM TABELAS DE EXPURGO
        '10/10/2013
        'sSqlW = sSqlW & "                             WHERE arquivo like 'SEG488%')))" & vbNewLine
        sSqlW = sSqlW & "                             WHERE  a.proposta_id = isnull(proposta_id,0) " & vbNewLine
        sSqlW = sSqlW & "                               and  arquivo like 'SEG488%'" & vbNewLine
        sSqlW = sSqlW & "                             UNION " & vbNewLine
        sSqlW = sSqlW & "                            SELECT 1 " & vbNewLine
        sSqlW = sSqlW & "                              FROM seguros_hist_db.dbo.emi_re_proposta_hist_tb  WITH (NOLOCK) " & vbNewLine
        sSqlW = sSqlW & "                             WHERE  a.proposta_id = isnull(proposta_id,0) " & vbNewLine
        sSqlW = sSqlW & "                               and arquivo LIKE 'SEG488%')))" & vbNewLine
        'FIM GENJUNIOR


        sSqlW = sSqlW & " AND A.SITUACAO not in ('X','T') "
        'Alessandra Grig�rio - 10/08/2004 - N�o deixar carregar no grid propostas recusadas
        'A pedido de Paulo Almeida - Retirado (verificar coment�rio abaixo)

        'Comentado abaixo por Stefanini Consultoria (Maur�cio) em 06/10/2004
        'A pedido de Thais Regina, pois informou que dever�o aparecer as propostas Recusadas no grid.

        'sSqlW = sSqlW & " and a.situacao not in ('r') "
    End If

    ' Uniao das queries
    sSql_temp = sSql
    If Len(sSqlAuxA) > 0 Then
        sSql = sSql & sSqlAuxA & sSqlW
        If Len(sSqlAuxF) > 0 Then
            sSql = sSql & " UNION " & vbNewLine & sSql_temp & sSqlAuxF & sSqlW
        End If
    Else

        sSql = sSql & sSqlW

    End If
    'Inicio: Data:14/05/2013 Autor:Rodrigo.Moura incidente:
    'sSql = sSql & " AND A.SITUACAO <> 'T' "
     sSql = sSql & " AND A.SITUACAO not in ('X','T') "
    'Fim: Data:14/05/2013 Autor:Rodrigo.Moura incidente:
    sSql = sSql & " Order By dt_contratacao "

    Set Pesquisar = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, sSql, True)

    Exit Function
  Resume
TrataErro:
    Call TratarErro("Pesquisar", Me.name)
    Call FinalizarAplicacao
  
End Function

Private Sub PreencherGridPesquisa(rsFonte As Recordset)

    On Error GoTo TrataErro

    Dim sLinha As String
    Dim Pesquisar  As Recordset
    Dim sql2 As String
    Dim idx  As Integer
  
    If rsFonte.EOF Then
        Call MsgBox("Nenhum registro encontrado para a pesquisa", vbInformation)
        Exit Sub
    End If

    MSFlexGrid1.Rows = 1
  
    While Not rsFonte.EOF
  
        sLinha = rsFonte!proposta_id & vbTab
        sLinha = sLinha & rsFonte!Produto_id & vbTab
        sLinha = sLinha & rsFonte!Nome & vbTab
        sLinha = sLinha & Format(rsFonte!dt_contratacao, "dd/mm/yyyy") & vbTab
     
        sql2 = " select dt_fim_vigencia from apolice_tb where proposta_id = " & rsFonte!proposta_id
        sql2 = sql2 & " Union"
        sql2 = sql2 & " select dt_fim_vigencia from proposta_adesao_tb where proposta_id = " & rsFonte!proposta_id
     
        'rsFonte2.ActiveConnection
        Set Pesquisar = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, sql2, True)
    
        'Alterado em 13/03/2008 por Vinicius - Confitec conforme conversado com o Yuri
        'foi incluso na condi��o os produtos 16 e 722
        'flow 280757
     
        'If rsFonte!produto_id = 14 And (rsFonte!descr_situacao = "Certificado Emitido" Or rsFonte!descr_situacao = "Suspensa") Then
     
        If (rsFonte!Produto_id = 14 Or rsFonte!Produto_id = 16 Or rsFonte!Produto_id = 722) And (rsFonte!descr_situacao = "Certificado Emitido" Or rsFonte!descr_situacao = "Suspensa") Then
     
            If IsNull(Pesquisar!dt_fim_vigencia) Then
                sLinha = sLinha & "ATIVA COM CERTIFICADO EMITIDO" & vbTab
            Else

                If CDate(Pesquisar!dt_fim_vigencia) < CDate(Data_Sistema) Then
                    sLinha = sLinha & "VENCIDA" & vbTab
                Else
                    sLinha = sLinha & "ATIVA COM CERTIFICADO EMITIDO" & vbTab
                End If
            End If

        Else
            sLinha = sLinha & rsFonte!descr_situacao & vbTab
        End If
     
        'If rsFonte!produto_id = 14 And CDate(IIf(IsNull(Pesquisar!dt_fim_vigencia), Data_Sistema, Pesquisar!dt_fim_vigencia)) < CDate(Data_Sistema) Then
        '   sLinha = sLinha & "VENCIDA" & vbTab
        'Else
        '   sLinha = sLinha & "ATIVA COM CERTIFICADO EMITIDO" & vbTab
        'End If
     
        sLinha = sLinha & rsFonte!prop_cliente_id & vbTab

        'lrocha - 05/01/2005
        If Trim(rsFonte!seg_cliente) = "ESTILO" Or Trim(rsFonte!seg_cliente) = "PRIVATE" Then
            sLinha = sLinha & rsFonte!seg_cliente & vbTab
        Else
            sLinha = sLinha & "" & vbTab
        End If
     
        sLinha = sLinha & rsFonte!origem_proposta_id & vbTab
    
       
        MSFlexGrid1.AddItem sLinha
  
        rsFonte.MoveNext
  
    Wend
    'MARIA OCULTA COLUNA 9
    MSFlexGrid1.ColWidth(7) = 1
  
    '*** Inclus�o do Sinalizado de PPE - Pessoas politicamente expostas - demanda 269994
    '*** 23/08/2008 - Fabio (Stefanini)
    For idx = 1 To (MSFlexGrid1.Rows - 1)

        If IdentificaPPE(MSFlexGrid1.TextMatrix(idx, 0)) Then
            MSFlexGrid1.TextMatrix(idx, 8) = "Sim"
        Else
            MSFlexGrid1.TextMatrix(idx, 8) = "N�o"
        End If

    Next

    Exit Sub

TrataErro:
    Call TratarErro("Pesquisar", Me.name)
    Call FinalizarAplicacao
  
End Sub

Private Sub cmdSelec_Click()

    On Error GoTo TrataErro
  
    Dim rsPesquisa As New Recordset
    
    ' Valida��o
    If Not ValidarParametros Then Exit Sub
    
    'Interface
    Me.MousePointer = vbHourglass
    cmdSelec.Enabled = False    'MCS 652391 Talitha
        
    'Pesquisa
    Set rsPesquisa = Pesquisar
    
    'Grid
    Call PreencherGridPesquisa(rsPesquisa)
    
    Set rsPesquisa = Nothing
    
    'Interface
    cmdSelec.Enabled = True
    Me.MousePointer = vbDefault
    
    Exit Sub
  Resume
TrataErro:
    Me.MousePointer = vbDefault
    Call TratarErro("cmdSelec_Click", Me.name)
    Call FinalizarAplicacao
    
End Sub

Private Sub Form_Unload(Cancel As Integer)
    End
End Sub

Private Sub MSFlexGrid1_DblClick()
    If MSFlexGrid1.Rows = 1 Then
        Exit Sub
    End If
  
    'carolina.marinho 19/04/2011 - Demanda 9106683 - Projeto BB Seguro Prestamista Consorcio
    If MSFlexGrid1.TextMatrix(MSFlexGrid1.RowSel, 0) = "" Then
        Exit Sub
    End If

    MousePointer = vbHourglass
  
    If MSFlexGrid1.TextMatrix(MSFlexGrid1.RowSel, 7) = 2 Then
    ' MSFlexGrid1.TextMatrix(idx, 7) = 2 Then
        Unload ConPropAdesaoALS
        ConPropAdesaoALS.Show
    Else
        Unload ConPropAdesao
        ConPropAdesao.Show
    End If
    
   
   
'
'    If VerificarPropostaALS(MSFlexGrid1.TextMatrix(MSFlexGrid1.RowSel, 0)) Then
'        ConPropAdesaoALS.Show
'    Else
'        ConPropAdesao.Show
'    End If

    Me.Hide

    MousePointer = vbDefault
  
End Sub

Private Function BuscarPropostaSegmento(ByVal sCodigoProduto As String, _
                                        ByVal lProposta_id As Long, _
                                        ByVal lPropostaBB As Long) As Long
    
    On Error GoTo TrataErro
    
    Dim strPropostaSegmento As String
    Dim rsDadosRetornado As Recordset
    
    strPropostaSegmento = "Select prop.proposta_id,"
    strPropostaSegmento = strPropostaSegmento & " seg.cod_segmento"
    strPropostaSegmento = strPropostaSegmento & " From proposta_tb prop,"
    strPropostaSegmento = strPropostaSegmento & " segmento_consorcio_tb seg,"
    strPropostaSegmento = strPropostaSegmento & " arquivo_produto_tb arq,"
    strPropostaSegmento = strPropostaSegmento & " proposta_fechada_tb fec,"
    strPropostaSegmento = strPropostaSegmento & " apolice_tb apo"
    strPropostaSegmento = strPropostaSegmento & " Where arq.produto_externo_id = prop.produto_externo_id"
    strPropostaSegmento = strPropostaSegmento & " And seg.cod_ramo_bb = arq.produto_externo_id"
    strPropostaSegmento = strPropostaSegmento & " And prop.produto_id = " & sCodigoProduto
    strPropostaSegmento = strPropostaSegmento & " And prop.proposta_id = fec.proposta_id"
    strPropostaSegmento = strPropostaSegmento & " And prop.proposta_id = apo.proposta_id"

    If lProposta_id = 0 Then
        strPropostaSegmento = strPropostaSegmento & " And fec.proposta_bb = " & lPropostaBB
    ElseIf lPropostaBB = 0 Then
        strPropostaSegmento = strPropostaSegmento & " And fec.proposta_id = " & lProposta_id
    End If

    strPropostaSegmento = strPropostaSegmento & " And (seg.dt_fim_vigencia <= '" & Format(Data_Sistema, "yyyymmdd") & "' or seg.dt_fim_vigencia is null) "
    
    
    strPropostaSegmento = strPropostaSegmento & "AND prop.SITUACAO not in ('X','T')"
    
    Set rsDadosRetornado = New Recordset
    Set rsDadosRetornado = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, strPropostaSegmento, True)
    
    If Not rsDadosRetornado.EOF Then
        BuscarPropostaSegmento = rsDadosRetornado("cod_segmento")
    Else
        BuscarPropostaSegmento = 0
    End If
    
    rsDadosRetornado.Close
    Set rsDadosRetornado = Nothing
    
    Exit Function
TrataErro:
    Call TratarErro("BuscarPropostaSegmento", Me.name)
    Call FinalizarAplicacao

End Function

'*** Inclus�o do Sinalizado de PPE - Pessoas politicamente expostas - demanda 269994
'*** 23/08/2008 - Fabio (Stefanini)
Private Function IdentificaPPE(propostaId As String) As Boolean

    'Sessao de variaveis
    Dim strSql      As String
    Dim rc          As Recordset

    'Manipulador de erros
    On Error GoTo ErrIdentificaPPE

    'Valor Padrao
    IdentificaPPE = False

    'Busca Identificar se faz parte do PPE
    strSql = "SELECT COUNT(1) AS TOTAL "
    strSql = strSql & " FROM seguros_db..PPE_ORGAO_CARGO_TB as TPPE "
    strSql = strSql & " INNER JOIN seguros_db..pessoa_fisica_tb AS TPF ON CAST(TPPE.CPF AS BIGINT) = CAST(TPF.CPF AS BIGINT)"
    strSql = strSql & " INNER JOIN seguros_db..CLIENTE_tb AS TCLI ON TCLI.CLIENTE_ID = TPF.PF_CLIENTE_ID"
    strSql = strSql & " INNER JOIN seguros_db..proposta_tb AS TPRO ON TPRO.PROP_CLIENTE_ID = TCLI.CLIENTE_ID"
    strSql = strSql & " WHERE PROPOSTA_ID = " & propostaId
    strSql = strSql & " AND (TPPE.DT_EXCLUSAO_PPE IS NULL OR  TPPE.DT_EXCLUSAO_PPE >= GETDATE())"

    Set rc = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, strSql, True)
   
    'Verifica se voltou registro
    If rc.EOF Then GoTo Finaliza

    'Verifica Valor Valido
    If IsNull(rc!Total) Then GoTo Finaliza

    'vERIFICA nao ENCONTROU
    If CInt(rc!Total) <= 0 Then GoTo Finaliza
    
    'Valor Padrao
    IdentificaPPE = True

    'Finaliza
Finaliza:
    rc.Close
    Set rc = Nothing

    'Sai
    Exit Function

    'Manipulador de erros
ErrIdentificaPPE:

    'Fecha Recordsrt
    rc.Close
    Set rc = Nothing

End Function

'carolina.marinho 15/04/2011 - Demanda 9106683 - Projeto BB Seguro Prestamista Consorcio
Private Function MontaComboSegmento()

    Dim Sql As String
    Dim rsSeg As Recordset
    
    Sql = "SELECT descricao, cod_segmento" & vbNewLine
    Sql = Sql & "  FROM segmento_consorcio_tb" & vbNewLine
    Sql = Sql & " WHERE dt_fim_vigencia IS NULL " & vbNewLine
    
    Set rsSeg = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, Sql, True)
    
    While Not rsSeg.EOF
        
        cboSegmento.AddItem Trim(rsSeg(0))
        cboSegmento.ItemData(cboSegmento.NewIndex) = rsSeg(1)
        rsSeg.MoveNext
        
    Wend
    
    Set rsSeg = Nothing
    
End Function


