VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form fmrExtratoRestituicao 
   Caption         =   "Extrato de Restitui��o"
   ClientHeight    =   7350
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   13725
   LinkTopic       =   "Form1"
   ScaleHeight     =   7350
   ScaleWidth      =   13725
   StartUpPosition =   1  'CenterOwner
   Begin VB.CommandButton cmdFechar 
      Caption         =   "Fechar"
      Height          =   375
      Left            =   12360
      TabIndex        =   24
      Top             =   6840
      Width           =   1215
   End
   Begin VB.Frame frm_extrato_restituicao 
      Caption         =   "Extrato de Restitui��o"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   6495
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   13455
      Begin VB.TextBox txtUsuarioEmissor 
         Height          =   300
         Left            =   8415
         TabIndex        =   26
         Top             =   1440
         Width           =   2760
      End
      Begin VB.TextBox txtUsuarioAvaliacaoTecnica 
         Height          =   300
         Left            =   8400
         Locked          =   -1  'True
         TabIndex        =   21
         Top             =   1800
         Width           =   2760
      End
      Begin VB.TextBox txtUsuarioAvaliacaoGerencial 
         Height          =   285
         Left            =   8400
         Locked          =   -1  'True
         TabIndex        =   20
         Top             =   2160
         Width           =   2760
      End
      Begin VB.TextBox txtPropostaAB 
         Height          =   300
         Left            =   8400
         Locked          =   -1  'True
         TabIndex        =   16
         Top             =   360
         Width           =   1560
      End
      Begin VB.TextBox txtNumeroEndosso 
         Height          =   285
         Left            =   8400
         Locked          =   -1  'True
         TabIndex        =   15
         Top             =   720
         Width           =   1560
      End
      Begin VB.TextBox txtTipoEndosso 
         Height          =   300
         Left            =   8400
         Locked          =   -1  'True
         TabIndex        =   14
         Top             =   1080
         Width           =   4680
      End
      Begin VB.TextBox txtValorRestituicao 
         Height          =   300
         Left            =   1920
         Locked          =   -1  'True
         TabIndex        =   10
         Top             =   1440
         Width           =   1800
      End
      Begin VB.TextBox txtAvaliacaoTecnica 
         Height          =   285
         Left            =   1920
         Locked          =   -1  'True
         TabIndex        =   9
         Top             =   1800
         Width           =   1800
      End
      Begin VB.TextBox txtAvaliacaoGerencial 
         Height          =   300
         Left            =   1920
         Locked          =   -1  'True
         TabIndex        =   8
         Top             =   2160
         Width           =   1800
      End
      Begin VB.TextBox txtPropostaBB 
         Height          =   300
         Left            =   1920
         Locked          =   -1  'True
         TabIndex        =   4
         Top             =   360
         Width           =   1800
      End
      Begin VB.TextBox txtNumeroEndossoBB 
         Height          =   285
         Left            =   1920
         Locked          =   -1  'True
         TabIndex        =   3
         Top             =   720
         Width           =   1800
      End
      Begin VB.TextBox txtEmissaoEndosso 
         Height          =   300
         Left            =   1920
         Locked          =   -1  'True
         TabIndex        =   2
         Top             =   1080
         Width           =   1800
      End
      Begin MSFlexGridLib.MSFlexGrid gridExtratoRestituicao 
         Height          =   3615
         Left            =   240
         TabIndex        =   1
         Top             =   2760
         Width           =   12975
         _ExtentX        =   22886
         _ExtentY        =   6376
         _Version        =   393216
         Cols            =   7
         AllowUserResizing=   1
         FormatString    =   $"frmExtratoRestituicao.frx":0000
      End
      Begin VB.Label lblUsuarioEmissor 
         Caption         =   "Usu�rio Emissor"
         Height          =   195
         Left            =   6120
         TabIndex        =   25
         Top             =   1430
         Width           =   2040
      End
      Begin VB.Label lblUsuarioAvaliacaoTecnica 
         AutoSize        =   -1  'True
         Caption         =   "Usu�rio Avalia��o T�cnica:"
         Height          =   195
         Left            =   6120
         TabIndex        =   23
         Top             =   1800
         Width           =   1965
      End
      Begin VB.Label lblUsuarioAvaliacaoGerencial 
         AutoSize        =   -1  'True
         Caption         =   "Usu�rio Avalia��o Gerencial:"
         Height          =   195
         Left            =   6120
         TabIndex        =   22
         Top             =   2160
         Width           =   2055
      End
      Begin VB.Label lblPropostaAB 
         AutoSize        =   -1  'True
         Caption         =   "Proposta AB:"
         Height          =   195
         Left            =   6120
         TabIndex        =   19
         Top             =   360
         Width           =   930
      End
      Begin VB.Label lblNumeroEndosso 
         AutoSize        =   -1  'True
         Caption         =   "N�mero Endosso:"
         Height          =   195
         Left            =   6120
         TabIndex        =   18
         Top             =   720
         Width           =   1260
      End
      Begin VB.Label lblTipoEndosso 
         AutoSize        =   -1  'True
         Caption         =   "Tipo do Endosso:"
         Height          =   195
         Left            =   6120
         TabIndex        =   17
         Top             =   1080
         Width           =   1245
      End
      Begin VB.Label lblValorRestituicao 
         AutoSize        =   -1  'True
         Caption         =   "Valor da Restitui��o:"
         Height          =   195
         Left            =   240
         TabIndex        =   13
         Top             =   1440
         Width           =   1470
      End
      Begin VB.Label lblAvaliacaoTecnica 
         AutoSize        =   -1  'True
         Caption         =   "Avalia��o T�cnica:"
         Height          =   195
         Left            =   240
         TabIndex        =   12
         Top             =   1800
         Width           =   1380
      End
      Begin VB.Label lblAvaliacaoGerencial 
         AutoSize        =   -1  'True
         Caption         =   "Avalia��o Gerencial:"
         Height          =   195
         Left            =   240
         TabIndex        =   11
         Top             =   2160
         Width           =   1470
      End
      Begin VB.Label lblPropostaBB 
         AutoSize        =   -1  'True
         Caption         =   "Proposta BB:"
         Height          =   195
         Left            =   240
         TabIndex        =   7
         Top             =   360
         Width           =   930
      End
      Begin VB.Label lblNumeroEndossoBB 
         AutoSize        =   -1  'True
         Caption         =   "N�mero Endosso BB:"
         Height          =   195
         Left            =   240
         TabIndex        =   6
         Top             =   720
         Width           =   1515
      End
      Begin VB.Label lblEmissaoEndosso 
         AutoSize        =   -1  'True
         Caption         =   "Emiss�o do Endosso:"
         Height          =   195
         Left            =   240
         TabIndex        =   5
         Top             =   1080
         Width           =   1515
      End
   End
End
Attribute VB_Name = "fmrExtratoRestituicao"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Function PreencheDadosTextbox() As Recordset

On Error GoTo TrataErro
   
'SQL = SQL & "IF (SELECT 1 FROM proposta_adesao_tb WHERE proposta_id =" & ConPropAdesao.txtProposta.Text & ") = 1" & vbNewLine
'
'SQL = SQL & "SELECT pa.proposta_bb, p.proposta_id, e.num_endosso_bb, e.endosso_id, e.dt_emissao, CAST(te.tp_endosso_id AS VARCHAR(60)) + ' - ' + te.descricao AS tipo_endosso," & vbNewLine
'SQL = SQL & "ISNULL(ar.val_restituicao, ABS(ef.val_financeiro)) AS val_restituicao, ar.dt_avaliacao_tecnica, ar.dt_avaliacao_gerencial," & vbNewLine
'SQL = SQL & "ar.usuario_avaliacao_tecnica , ar.usuario_avaliacao_gerencial, ar.usuario_avaliacao_emissor" & vbNewLine
'SQL = SQL & "FROM seguros_db..proposta_tb p  WITH (NOLOCK)  INNER JOIN" & vbNewLine
'SQL = SQL & "seguros_db..proposta_adesao_tb pa  WITH (NOLOCK)  ON p.proposta_id = pa.proposta_id INNER JOIN" & vbNewLine
'SQL = SQL & "seguros_db..endosso_tb e  WITH (NOLOCK)  ON e.proposta_id = p.proposta_id  INNER JOIN" & vbNewLine
'SQL = SQL & "seguros_db..tp_endosso_tb te  WITH (NOLOCK)  ON te.tp_endosso_id = e.tp_endosso_id LEFT JOIN" & vbNewLine
'SQL = SQL & "seguros_db..endosso_financeiro_tb ef  WITH (NOLOCK)  ON ef.proposta_id = e.proposta_id" & vbNewLine
'SQL = SQL & "                                  AND ef.endosso_id = e.endosso_id LEFT JOIN" & vbNewLine
'SQL = SQL & "seguros_db..avaliacao_restituicao_tb ar  WITH (NOLOCK)  ON ar.proposta_id = e.proposta_id" & vbNewLine
'SQL = SQL & "AND ar.endosso_id = e.endosso_id" & vbNewLine
'SQL = SQL & "WHERE p.proposta_id =" & ConPropAdesao.txtProposta.Text & vbNewLine
'If ConPropAdesao.GridProposta(2).TextMatrix(ConPropAdesao.GridProposta(2).Row, 0) <> "" Then
'SQL = SQL & "AND e.endosso_id =" & ConPropAdesao.GridProposta(2).TextMatrix(ConPropAdesao.GridProposta(2).Row, 0) & vbNewLine
'End If
'SQL = SQL & "Else" & vbNewLine
'
'SQL = SQL & "SELECT pf.proposta_bb, p.proposta_id, e.num_endosso_bb, e.endosso_id, e.dt_emissao, CAST(te.tp_endosso_id AS VARCHAR(60)) + ' - ' + te.descricao AS tipo_endosso," & vbNewLine
'SQL = SQL & "ISNULL(ar.val_restituicao, ABS(ef.val_financeiro)) AS val_restituicao, ar.dt_avaliacao_tecnica, ar.dt_avaliacao_gerencial," & vbNewLine
'SQL = SQL & "ar.usuario_avaliacao_tecnica , ar.usuario_avaliacao_gerencial, ar.usuario_avaliacao_emissor" & vbNewLine
'SQL = SQL & "FROM seguros_db..proposta_tb p  WITH (NOLOCK)  INNER JOIN" & vbNewLine
'SQL = SQL & "seguros_db..proposta_fechada_tb pf  WITH (NOLOCK)  ON p.proposta_id = pf.proposta_id INNER JOIN" & vbNewLine
'SQL = SQL & "seguros_db..endosso_tb e  WITH (NOLOCK)  ON e.proposta_id = p.proposta_id  INNER JOIN" & vbNewLine
'SQL = SQL & "seguros_db..tp_endosso_tb te  WITH (NOLOCK)  ON te.tp_endosso_id = e.tp_endosso_id LEFT JOIN" & vbNewLine
'SQL = SQL & "seguros_db..endosso_financeiro_tb ef  WITH (NOLOCK)  ON ef.proposta_id = e.proposta_id" & vbNewLine
'SQL = SQL & "                                  AND ef.endosso_id = e.endosso_id LEFT JOIN" & vbNewLine
'SQL = SQL & "seguros_db..avaliacao_restituicao_tb ar  WITH (NOLOCK)  ON ar.proposta_id = e.proposta_id" & vbNewLine
'SQL = SQL & "                                     AND ar.endosso_id = e.endosso_id" & vbNewLine
'SQL = SQL & "WHERE p.proposta_id =" & ConPropAdesao.txtProposta.Text & vbNewLine
'If ConPropAdesao.GridProposta(2).TextMatrix(ConPropAdesao.GridProposta(2).Row, 0) <> "" Then
'SQL = SQL & "AND e.endosso_id =" & ConPropAdesao.GridProposta(2).TextMatrix(ConPropAdesao.GridProposta(2).Row, 0) & vbNewLine
'End If

'inicio: Fernanda Nunes Dutra - Nova Consultoria 12/05/2016 - 19122976 - Corre��o do c�lculo de restitui��o
    Dim proposta_id As Double
    Dim endosso_id As Integer

    proposta_id = ConPropAdesao.txtProposta.Text

If ConPropAdesao.GridProposta(2).TextMatrix(ConPropAdesao.GridProposta(2).Row, 0) <> "" Then
        endosso_id = ConPropAdesao.GridProposta(2).TextMatrix(ConPropAdesao.GridProposta(2).Row, 0)
End If

    SQL = ""
    SQL = SQL & "SET NOCOUNT ON exec seguros_db.dbo.SEGS12941_SPS " & proposta_id & ", " & endosso_id & vbNewLine
'fim:  Fernanda Nunes Dutra - Nova Consultoria 12/05/2016 - 19122976 - Corre��o do c�lculo de restitui��o


Set PreencheDadosTextbox = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, SQL, True)

    Exit Function

TrataErro:
    Call TratarErro("PreencheDadosTextbox", Me.name)
    Call FinalizarAplicacao

End Function

Private Sub cmdFechar_Click()
Call Unload(Me)
End Sub

Private Sub Form_Load()
Set rsRestituicao = New Recordset
Set rsRestituicao = PreencheDadosTextbox

If Not rsRestituicao.EOF Then
    txtPropostaBB.Text = IIf(IsNull(rsRestituicao!proposta_bb) = False, rsRestituicao!proposta_bb, "")
    txtPropostaAB.Text = rsRestituicao!proposta_id
    txtNumeroEndossoBB.Text = IIf(IsNull(rsRestituicao!num_endosso_bb) = False, rsRestituicao!num_endosso_bb, "")
    txtNumeroEndosso.Text = rsRestituicao!endosso_id
    txtEmissaoEndosso.Text = Format(rsRestituicao!dt_emissao, "dd/mm/yyyy")
    txtTipoEndosso.Text = rsRestituicao!tipo_endosso
    If Not IsNull(rsRestituicao!val_restituicao) Then
    txtValorRestituicao.Text = IIf(IsNull(FormataValorParaPadraoBrasil(rsRestituicao!val_restituicao)) = False, FormataValorParaPadraoBrasil(rsRestituicao!val_restituicao), "")
    End If
    txtAvaliacaoTecnica.Text = IIf(IsNull(rsRestituicao!dt_avaliacao_tecnica) = False, Format(rsRestituicao!dt_avaliacao_tecnica, "dd/mm/yyyy"), "")
    txtAvaliacaoGerencial.Text = IIf(IsNull(rsRestituicao!dt_avaliacao_gerencial) = False, Format(rsRestituicao!dt_avaliacao_gerencial, "dd/mm/yyyy"), "")
    txtUsuarioAvaliacaoTecnica.Text = IIf(IsNull(rsRestituicao!usuario_avaliacao_tecnica) = False, rsRestituicao!usuario_avaliacao_tecnica, "")
    txtUsuarioAvaliacaoGerencial.Text = IIf(IsNull(rsRestituicao!usuario_avaliacao_gerencial) = False, rsRestituicao!usuario_avaliacao_gerencial, "")
    txtUsuarioEmissor.Text = IIf(IsNull(rsRestituicao!usuario_avaliacao_emissor) = False, rsRestituicao!usuario_avaliacao_emissor, "")

End If

rsRestituicao.Close
Call CarregarDadosExtrato

End Sub
Private Function ObterDadosExtrato() As Recordset
On Error GoTo TrataErro

  Dim sSql As String

  sSql = ""
  sSql = sSql & "SELECT dt_inclusao, sigla_recurso, proposta_id, endosso_id, val_restituicao, usuario, evento, situacao, ISNULL(motivo, '') as motivo, arquivo" & vbNewLine
  sSql = sSql & "FROM seguros_db.dbo.extrato_restituicao_tb  WITH (NOLOCK) " & vbNewLine
  sSql = sSql & "WHERE proposta_id = " & txtPropostaAB.Text & vbNewLine
  sSql = sSql & "AND (endosso_id = " & txtNumeroEndosso.Text & vbNewLine
  ' Ic�cio Altera��o - Tales de S� - INC000004240252
  ' Utilizar a fun��o FormataValor
  'sSql = sSql & "OR (endosso_id IS NULL AND val_restituicao = " & TrocaVirgulaPorPonto(txtValorRestituicao) & "))"
  sSql = sSql & "OR (endosso_id IS NULL AND val_restituicao = " & FormataValor(txtValorRestituicao) & "))"
  ' Fim Altera��o - INC000004240252
  'Jessica.Adao (Rafael Prestes / Phillip Tessesine) - Confitec Sistemas - 20150720 - Projeto 18319298 :  Fase III
  sSql = sSql & " ORDER BY DT_INCLUSAO "
  
  Set ObterDadosExtrato = ExecutarSQL(gsSIGLASISTEMA, _
                                      glAmbiente_id, _
                                      App.Title, _
                                      App.FileDescription, _
                                      sSql, _
                                      True)
                                      

Exit Function
TrataErro:
  Call TratarErro("ObterDadosExtrato", Me.name)
  Call FinalizarAplicacao

End Function

Private Function CarregarDadosExtrato() As Boolean

    On Error GoTo TrataErro

    Dim rsExtrato As Recordset
    Dim sLinha As String
    Dim sSql As String
    Dim rsNomeUsuario As Recordset
  
    CarregarDadosExtrato = True
  
    Set rsExtrato = New Recordset
    Set rsExtrato = ObterDadosExtrato()

    gridExtratoRestituicao.Rows = 1
  
    While Not rsExtrato.EOF
  
        sLinha = ""
        sLinha = sLinha & Format(rsExtrato("dt_inclusao"), "dd/mm/yyyy") & vbTab
        sLinha = sLinha & Trim(rsExtrato("sigla_recurso")) & vbTab
        sLinha = sLinha & Trim(rsExtrato("arquivo")) & vbTab
        sLinha = sLinha & Trim(rsExtrato("evento")) & vbTab
        sLinha = sLinha & Trim(rsExtrato("situacao")) & vbTab
        sLinha = sLinha & Trim(rsExtrato("usuario")) & vbTab
        sLinha = sLinha & Trim(rsExtrato("motivo")) & vbTab
        gridExtratoRestituicao.AddItem sLinha

        rsExtrato.MoveNext
    
    Wend
  
    rsExtrato.Close
    Set rsExtrato = Nothing
  
    Exit Function
  
TrataErro:
    Call TratarErro("CarregarDadosExtrato", Me.name)
    Call FinalizarAplicacao

End Function


