VERSION 5.00
Object = "{EAB22AC0-30C1-11CF-A7EB-0000C05BAE0B}#1.1#0"; "shdocvw.dll"
Begin VB.Form frmAbrePDF 
   Caption         =   "Laudo M�dico em PDF"
   ClientHeight    =   3090
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   4680
   LinkTopic       =   "Form1"
   ScaleHeight     =   3090
   ScaleWidth      =   4680
   StartUpPosition =   1  'CenterOwner
   Begin VB.CommandButton cmdFechar 
      Caption         =   "Fechar"
      Height          =   375
      Left            =   2640
      TabIndex        =   1
      Top             =   2520
      Width           =   1695
   End
   Begin SHDocVwCtl.WebBrowser wbrs1 
      Height          =   2055
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Visible         =   0   'False
      Width           =   1815
      ExtentX         =   3201
      ExtentY         =   3625
      ViewMode        =   0
      Offline         =   0
      Silent          =   0
      RegisterAsBrowser=   0
      RegisterAsDropTarget=   1
      AutoArrange     =   0   'False
      NoClientEdge    =   0   'False
      AlignLeft       =   0   'False
      NoWebView       =   0   'False
      HideFileNames   =   0   'False
      SingleClick     =   0   'False
      SingleSelection =   0   'False
      NoFolders       =   0   'False
      Transparent     =   0   'False
      ViewID          =   "{0057D0E0-3573-11CF-AE69-08002B2E1262}"
      Location        =   "http:///"
   End
End
Attribute VB_Name = "frmAbrePDF"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'Flow 19360491 - Confitec: Claudia.Silva - 22/08/2016
Dim caminhoArq As String
Dim sArquivo As String
Dim rsLaudo As Recordset
Dim sAux As String
Dim i As Integer

Public Sub ExibeLaudoMedico(ByVal PropostaBB As String)

Screen.MousePointer = vbHourglass

frmAbrePDF.Width = 10000
frmAbrePDF.Height = 7000

wbrs1.Visible = False

Me.Caption = App.Title & " - Laudo M�dico em PDF da Proposta BB " & Trim(PropostaBB)

sArquivo = "laudomedico_" & Trim(PropostaBB) & ".pdf"

sAux = "SELECT path_laudo_medico FROM seguros_db.dbo.parametro_geral_tb WITH (NOLOCK)"

Set rsLaudo = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, sAux, True)

If Not (rsLaudo.BOF And rsLaudo.EOF) Then
   caminhoArq = rsLaudo("path_laudo_medico").Value & sArquivo
End If

If dir$(caminhoArq) = "" Then
   If glAmbiente_id = 3 Then
      caminhoArq = "\\sisab051\aplicacoes_qld\laudo_medico\" & sArquivo
   Else
      caminhoArq = "\\Sisab101\prodinter\assinatura_eletronica\" & sArquivo
   End If
   If dir$(caminhoArq) = "" Then
      caminhoArq = "\\10.0.35.106\APLICACOES\SISBR\ARQUIVOS_LAUDOMEDICO\" & sArquivo
      If dir$(caminhoArq) = "" Then
         Screen.MousePointer = vbNormal
         
         MsgBox "N�o foram encontrados documentos para o registro solicitado."
        
         Call Unload(Me)

         Exit Sub

      End If
   End If
End If

wbrs1.Navigate caminhoArq
wbrs1.Visible = True

Screen.MousePointer = vbNormal

Me.Show vbModal
    
End Sub

Public Function VerificaLaudoMedico(ByVal PropostaAB As String, ByVal ProdutoID As String) As Boolean

Select Case Val(ProdutoID)
       Case 11, 135, 716, 1174, 1175, 1196, 1217, 1235, 1236, 1237
           
            VerificaLaudoMedico = True
            
       Case Else
    
            sAux = "set nocount on exec seguros_db.dbo.segs13076_sps 'p', " & PropostaAB & ", '', '', '', ''"
            Set rsLaudo = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, sAux, True)
  
            VerificaLaudoMedico = Not (rsLaudo.BOF Or rsLaudo.EOF)
   
End Select

End Function

Private Sub Form_Resize()

wbrs1.Width = Me.Width - 260
wbrs1.Height = Me.Height - 1350

cmdFechar.Top = Me.Height - 1100
cmdFechar.Left = Me.Width - 2150

End Sub

Private Sub cmdFechar_Click()

Call Unload(Me)

End Sub

Private Sub Form_Unload(Cancel As Integer)

wbrs1.Visible = False

End Sub
