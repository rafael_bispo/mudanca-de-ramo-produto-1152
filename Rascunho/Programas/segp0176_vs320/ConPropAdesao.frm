VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFlxGrd.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Object = "{EA5A962F-86AD-4480-BCF2-3A94A4CB62B9}#1.0#0"; "GridAlianca.ocx"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Begin VB.Form ConPropAdesao 
   ClientHeight    =   12300
   ClientLeft      =   750
   ClientTop       =   1290
   ClientWidth     =   12030
   Icon            =   "ConPropAdesao.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   12300
   ScaleWidth      =   12030
   Begin MSComDlg.CommonDialog dlgGrid 
      Left            =   0
      Top             =   450
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.CommandButton cmbBotao 
      Caption         =   "Consulta Imagens Propostas"
      Height          =   405
      Index           =   1
      Left            =   7440
      TabIndex        =   313
      Top             =   11520
      Width           =   2235
   End
   Begin VB.CommandButton cmbBotao 
      Caption         =   "Visualiza Laudo M�dico"
      Height          =   405
      Index           =   2
      Left            =   4200
      TabIndex        =   314
      Top             =   11520
      Width           =   2235
   End
   Begin MSComctlLib.StatusBar StatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   112
      Top             =   12015
      Width           =   12030
      _ExtentX        =   21220
      _ExtentY        =   503
      Style           =   1
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
         EndProperty
      EndProperty
   End
   Begin VB.CommandButton cmbBotao 
      Caption         =   "&Sair"
      Height          =   405
      Index           =   0
      Left            =   10530
      TabIndex        =   106
      Top             =   11535
      Width           =   1150
   End
   Begin TabDlg.SSTab SSTab1 
      Height          =   11415
      Left            =   120
      TabIndex        =   85
      Top             =   60
      Width           =   11895
      _ExtentX        =   20981
      _ExtentY        =   20135
      _Version        =   393216
      Tabs            =   15
      Tab             =   4
      TabsPerRow      =   6
      TabHeight       =   520
      TabCaption(0)   =   "Proposta"
      TabPicture(0)   =   "ConPropAdesao.frx":0442
      Tab(0).ControlEnabled=   0   'False
      Tab(0).Control(0)=   "fraEstipulante"
      Tab(0).Control(1)=   "fraCorretor"
      Tab(0).Control(2)=   "frProposta"
      Tab(0).Control(3)=   "Frame15"
      Tab(0).Control(4)=   "fraSubgrupo(0)"
      Tab(0).ControlCount=   5
      TabCaption(1)   =   "Dados Financeiros"
      TabPicture(1)   =   "ConPropAdesao.frx":045E
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "Frame12(1)"
      Tab(1).Control(1)=   "fraCapitalSegurado"
      Tab(1).Control(2)=   "fraPremio"
      Tab(1).Control(3)=   "Frame11(1)"
      Tab(1).Control(4)=   "Frame11(0)"
      Tab(1).Control(5)=   "fraSubgrupo(1)"
      Tab(1).Control(6)=   "Frame12(0)"
      Tab(1).Control(7)=   "Frame11(2)"
      Tab(1).Control(8)=   "frmPlanosAnteriores"
      Tab(1).ControlCount=   9
      TabCaption(2)   =   "Proponente"
      TabPicture(2)   =   "ConPropAdesao.frx":047A
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "Frame2(0)"
      Tab(2).Control(1)=   "FrameDadosConjuge"
      Tab(2).Control(2)=   "FrameDadosPF"
      Tab(2).ControlCount=   3
      TabCaption(3)   =   "Objeto de Risco"
      TabPicture(3)   =   "ConPropAdesao.frx":0496
      Tab(3).ControlEnabled=   0   'False
      Tab(3).Control(0)=   "fraMutuarios"
      Tab(3).Control(1)=   "Frame6"
      Tab(3).Control(2)=   "fraVidasSeguradas"
      Tab(3).Control(3)=   "fraDeclConjuge"
      Tab(3).ControlCount=   4
      TabCaption(4)   =   "Endosso"
      TabPicture(4)   =   "ConPropAdesao.frx":04B2
      Tab(4).ControlEnabled=   -1  'True
      Tab(4).Control(0)=   "grdEndosso"
      Tab(4).Control(0).Enabled=   0   'False
      Tab(4).ControlCount=   1
      TabCaption(5)   =   "Mutu�rios"
      TabPicture(5)   =   "ConPropAdesao.frx":04CE
      Tab(5).ControlEnabled=   0   'False
      Tab(5).Control(0)=   "Frame7"
      Tab(5).ControlCount=   1
      TabCaption(6)   =   "Benefici�rios"
      TabPicture(6)   =   "ConPropAdesao.frx":04EA
      Tab(6).ControlEnabled=   0   'False
      Tab(6).Control(0)=   "Frame8(1)"
      Tab(6).Control(1)=   "Frame8(0)"
      Tab(6).ControlCount=   2
      TabCaption(7)   =   "Sinistro"
      TabPicture(7)   =   "ConPropAdesao.frx":0506
      Tab(7).ControlEnabled=   0   'False
      Tab(7).Control(0)=   "gridSinistro"
      Tab(7).ControlCount=   1
      TabCaption(8)   =   "Extrato de Seguro"
      TabPicture(8)   =   "ConPropAdesao.frx":0522
      Tab(8).ControlEnabled=   0   'False
      Tab(8).Control(0)=   "GridProposta(0)"
      Tab(8).Control(1)=   "Frame5"
      Tab(8).Control(2)=   "Frame3"
      Tab(8).Control(3)=   "AgendCobInconsist"
      Tab(8).ControlCount=   4
      TabCaption(9)   =   "Coberturas"
      TabPicture(9)   =   "ConPropAdesao.frx":053E
      Tab(9).ControlEnabled=   0   'False
      Tab(9).Control(0)=   "Frame14(0)"
      Tab(9).Control(1)=   "fraSubgrupo(2)"
      Tab(9).ControlCount=   2
      TabCaption(10)  =   "Objeto de Risco RE"
      TabPicture(10)  =   "ConPropAdesao.frx":055A
      Tab(10).ControlEnabled=   0   'False
      Tab(10).Control(0)=   "fraEndRiscoPenhorRural"
      Tab(10).Control(1)=   "fraPenhorRural"
      Tab(10).Control(2)=   "fraSeguroConsorcio"
      Tab(10).Control(3)=   "fraDadosCultivo"
      Tab(10).Control(4)=   "fraDadosComplementares"
      Tab(10).Control(5)=   "fraEndRiscoAgricola"
      Tab(10).ControlCount=   6
      TabCaption(11)  =   "Coberturas RE"
      TabPicture(11)  =   "ConPropAdesao.frx":0576
      Tab(11).ControlEnabled=   0   'False
      Tab(11).Control(0)=   "gridCoberturasRE"
      Tab(11).ControlCount=   1
      TabCaption(12)  =   "Hist�rico"
      TabPicture(12)  =   "ConPropAdesao.frx":0592
      Tab(12).ControlEnabled=   0   'False
      Tab(12).Control(0)=   "cmdExibirTodos"
      Tab(12).Control(1)=   "txtDescricao"
      Tab(12).Control(2)=   "grdHistorico"
      Tab(12).Control(3)=   "Label2"
      Tab(12).ControlCount=   4
      TabCaption(13)  =   "Cosseguro"
      TabPicture(13)  =   "ConPropAdesao.frx":05AE
      Tab(13).ControlEnabled=   0   'False
      Tab(13).Control(0)=   "txtPercDespesa"
      Tab(13).Control(1)=   "txtPercParticipacao"
      Tab(13).Control(2)=   "txtNumOrdem"
      Tab(13).Control(3)=   "cmbCongenere"
      Tab(13).Control(4)=   "gridCobrancaCosseguro"
      Tab(13).Control(5)=   "Label72(2)"
      Tab(13).Control(6)=   "Label72(1)"
      Tab(13).Control(7)=   "Label72(0)"
      Tab(13).ControlCount=   8
      TabCaption(14)  =   "Consulta D�bito"
      TabPicture(14)  =   "ConPropAdesao.frx":05CA
      Tab(14).ControlEnabled=   0   'False
      Tab(14).Control(0)=   "Frame1"
      Tab(14).Control(1)=   "grdAvaliacao"
      Tab(14).ControlCount=   2
      Begin VB.Frame AgendCobInconsist 
         Caption         =   "Agendamentos de cobran�as inconsistentes recebidos pelo BB"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1935
         Left            =   -74760
         TabIndex        =   385
         Top             =   9360
         Width           =   11055
         Begin VB.CommandButton btnExportarExcel 
            Caption         =   "Exportar"
            Height          =   255
            Left            =   9720
            TabIndex        =   387
            Top             =   1560
            Width           =   1095
         End
         Begin MSFlexGridLib.MSFlexGrid GridProposta 
            Height          =   1215
            Index           =   3
            Left            =   120
            TabIndex        =   386
            Top             =   240
            Width           =   10695
            _ExtentX        =   18865
            _ExtentY        =   2143
            _Version        =   393216
            Cols            =   15
            FixedCols       =   0
            FormatString    =   $"ConPropAdesao.frx":05E6
         End
      End
      Begin VB.Frame FrameDadosPF 
         Caption         =   "Segurados"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   3015
         Left            =   -74820
         TabIndex        =   382
         Top             =   4560
         Visible         =   0   'False
         Width           =   11295
         Begin MSFlexGridLib.MSFlexGrid GridPessoaFisica 
            Height          =   2595
            Left            =   120
            TabIndex        =   383
            Top             =   240
            Width           =   10935
            _ExtentX        =   19288
            _ExtentY        =   4577
            _Version        =   393216
            Cols            =   7
            FixedCols       =   0
            AllowUserResizing=   1
            FormatString    =   $"ConPropAdesao.frx":073E
         End
      End
      Begin VB.Frame frmPlanosAnteriores 
         Caption         =   "Planos Anteriores"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1695
         Left            =   -74820
         TabIndex        =   380
         Top             =   5130
         Width           =   11295
         Begin MSFlexGridLib.MSFlexGrid MSPlanosAnteriores 
            Height          =   1335
            Left            =   120
            TabIndex        =   381
            Top             =   240
            Width           =   11055
            _ExtentX        =   19500
            _ExtentY        =   2355
            _Version        =   393216
            Rows            =   1
            Cols            =   4
            FixedCols       =   0
            FormatString    =   "Dt. In�cio Vig�ncia        | Pr�mio Total         |      Capital Segurado            |  Indeniza��o Extra    "
         End
      End
      Begin VB.Frame Frame11 
         Caption         =   "Servi�os Banc�rios"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   765
         Index           =   2
         Left            =   -67440
         TabIndex        =   376
         Top             =   8730
         Width           =   3855
         Begin VB.TextBox txtPercRemuneracaoServico 
            Alignment       =   1  'Right Justify
            Height          =   285
            Left            =   180
            Locked          =   -1  'True
            TabIndex        =   377
            TabStop         =   0   'False
            Top             =   390
            Width           =   1335
         End
         Begin VB.Label lblPercRemuneracaoServico 
            AutoSize        =   -1  'True
            Caption         =   "Percentual de Remunera��o:"
            Height          =   195
            Left            =   180
            TabIndex        =   378
            Top             =   180
            Width           =   2085
         End
      End
      Begin VB.Frame Frame1 
         Height          =   3495
         Left            =   -74760
         TabIndex        =   339
         Top             =   5640
         Width           =   11055
         Begin VB.TextBox txtDados 
            Height          =   285
            Index           =   13
            Left            =   2040
            TabIndex        =   355
            Top             =   2280
            Width           =   4935
         End
         Begin VB.TextBox txtDados 
            Height          =   285
            Index           =   12
            Left            =   1440
            TabIndex        =   354
            Top             =   2280
            Width           =   495
         End
         Begin VB.TextBox txtDados 
            Alignment       =   1  'Right Justify
            Height          =   285
            Index           =   15
            Left            =   3480
            TabIndex        =   353
            Top             =   2760
            Width           =   1575
         End
         Begin VB.TextBox txtDados 
            Height          =   285
            Index           =   14
            Left            =   1440
            TabIndex        =   352
            Top             =   2760
            Width           =   735
         End
         Begin VB.TextBox txtDados 
            Alignment       =   1  'Right Justify
            Height          =   285
            Index           =   1
            Left            =   5040
            TabIndex        =   351
            Top             =   360
            Width           =   1575
         End
         Begin VB.TextBox txtDados 
            Height          =   285
            Index           =   0
            Left            =   1440
            TabIndex        =   350
            Top             =   360
            Width           =   1575
         End
         Begin VB.TextBox txtDados 
            Height          =   285
            Index           =   4
            Left            =   6360
            TabIndex        =   349
            Top             =   840
            Width           =   4335
         End
         Begin VB.TextBox txtDados 
            Height          =   285
            Index           =   6
            Left            =   3480
            TabIndex        =   348
            Top             =   1320
            Width           =   375
         End
         Begin VB.TextBox txtDados 
            Height          =   285
            Index           =   5
            Left            =   1440
            TabIndex        =   347
            Top             =   1320
            Width           =   1575
         End
         Begin VB.TextBox txtDados 
            Height          =   285
            Index           =   3
            Left            =   5040
            TabIndex        =   346
            Top             =   840
            Width           =   1215
         End
         Begin VB.TextBox txtDados 
            Height          =   285
            Index           =   2
            Left            =   1440
            TabIndex        =   345
            Top             =   840
            Width           =   1575
         End
         Begin VB.TextBox txtDados 
            Height          =   285
            Index           =   10
            Left            =   3480
            TabIndex        =   344
            Top             =   1800
            Width           =   1575
         End
         Begin VB.TextBox txtDados 
            Height          =   285
            Index           =   7
            Left            =   5040
            TabIndex        =   343
            Top             =   1320
            Width           =   1575
         End
         Begin VB.TextBox txtDados 
            Height          =   285
            Index           =   8
            Left            =   7920
            TabIndex        =   342
            Top             =   1320
            Width           =   735
         End
         Begin VB.TextBox txtDados 
            Alignment       =   1  'Right Justify
            Height          =   285
            Index           =   16
            Left            =   6960
            TabIndex        =   341
            Top             =   2760
            Width           =   1575
         End
         Begin VB.TextBox txtDados 
            Height          =   285
            Index           =   9
            Left            =   1440
            TabIndex        =   340
            Top             =   1800
            Width           =   735
         End
         Begin VB.Label lblCobranca 
            Caption         =   "Cod.Retorno :"
            Height          =   255
            Index           =   9
            Left            =   240
            TabIndex        =   369
            Top             =   2280
            Width           =   1095
         End
         Begin VB.Label lblCobranca 
            Caption         =   "Valor Parcelas :"
            Height          =   255
            Index           =   8
            Left            =   2280
            TabIndex        =   368
            Top             =   2760
            Width           =   1335
         End
         Begin VB.Label lblCobranca 
            Caption         =   "N� Parcelas :"
            Height          =   255
            Index           =   7
            Left            =   240
            TabIndex        =   367
            Top             =   2760
            Width           =   975
         End
         Begin VB.Label lblCobranca 
            Caption         =   "Data da Importa��o :"
            Height          =   255
            Index           =   6
            Left            =   3360
            TabIndex        =   366
            Top             =   360
            Width           =   1575
         End
         Begin VB.Label lblCobranca 
            Caption         =   "Nome Arquivo :"
            Height          =   255
            Index           =   5
            Left            =   240
            TabIndex        =   365
            Top             =   360
            Width           =   1095
         End
         Begin VB.Label lblCobranca 
            Caption         =   "DV :"
            Height          =   255
            Index           =   3
            Left            =   3120
            TabIndex        =   364
            Top             =   1320
            Width           =   495
         End
         Begin VB.Label lblCobranca 
            Caption         =   "Nosso Numero :"
            Height          =   255
            Index           =   2
            Left            =   240
            TabIndex        =   363
            Top             =   1320
            Width           =   1215
         End
         Begin VB.Label lblCobranca 
            Caption         =   "Produto BB :"
            Height          =   255
            Index           =   1
            Left            =   3960
            TabIndex        =   362
            Top             =   840
            Width           =   1095
         End
         Begin VB.Label lblCobranca 
            Caption         =   "Proposta_BB :"
            Height          =   255
            Index           =   0
            Left            =   240
            TabIndex        =   361
            Top             =   840
            Width           =   1095
         End
         Begin VB.Label lblCobranca 
            Caption         =   "Conta Banc�ria :"
            Height          =   255
            Index           =   4
            Left            =   2280
            TabIndex        =   360
            Top             =   1800
            Width           =   1215
         End
         Begin VB.Label lblCobranca 
            Caption         =   "N� Conv�nio :"
            Height          =   255
            Index           =   11
            Left            =   3960
            TabIndex        =   359
            Top             =   1320
            Width           =   1215
         End
         Begin VB.Label lblCobranca 
            Caption         =   "N� Endossoo :"
            Height          =   255
            Index           =   12
            Left            =   6840
            TabIndex        =   358
            Top             =   1320
            Width           =   1335
         End
         Begin VB.Label lblCobranca 
            Caption         =   "Data Venc. Parcela :"
            Height          =   255
            Index           =   13
            Left            =   5400
            TabIndex        =   357
            Top             =   2760
            Width           =   1575
         End
         Begin VB.Label lblCobranca 
            Caption         =   "Agencia :"
            Height          =   255
            Index           =   14
            Left            =   240
            TabIndex        =   356
            Top             =   1800
            Width           =   1095
         End
      End
      Begin VB.Frame FrameDadosConjuge 
         Caption         =   "C�njuge"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   2955
         Left            =   -74820
         TabIndex        =   315
         Top             =   4560
         Width           =   11295
         Begin VB.TextBox txtConjuge 
            Height          =   300
            Index           =   2
            Left            =   210
            Locked          =   -1  'True
            TabIndex        =   331
            TabStop         =   0   'False
            Top             =   966
            Width           =   9015
         End
         Begin VB.TextBox txtConjuge 
            Height          =   300
            Index           =   11
            Left            =   7230
            Locked          =   -1  'True
            TabIndex        =   330
            TabStop         =   0   'False
            Top             =   2024
            Width           =   1995
         End
         Begin VB.TextBox txtConjuge 
            Height          =   285
            Index           =   13
            Left            =   840
            Locked          =   -1  'True
            TabIndex        =   329
            TabStop         =   0   'False
            Top             =   2550
            Width           =   3195
         End
         Begin VB.TextBox txtConjuge 
            Height          =   285
            Index           =   12
            Left            =   210
            Locked          =   -1  'True
            TabIndex        =   328
            TabStop         =   0   'False
            Top             =   2550
            Width           =   375
         End
         Begin VB.TextBox txtConjuge 
            Height          =   285
            Index           =   16
            Left            =   7560
            Locked          =   -1  'True
            TabIndex        =   327
            TabStop         =   0   'False
            Top             =   2550
            Width           =   615
         End
         Begin VB.TextBox txtConjuge 
            Height          =   285
            Index           =   15
            Left            =   6270
            Locked          =   -1  'True
            TabIndex        =   326
            TabStop         =   0   'False
            Top             =   2550
            Width           =   1095
         End
         Begin VB.TextBox txtConjuge 
            Height          =   285
            Index           =   14
            Left            =   4230
            Locked          =   -1  'True
            TabIndex        =   325
            TabStop         =   0   'False
            Top             =   2550
            Width           =   1935
         End
         Begin VB.TextBox txtConjuge 
            Height          =   300
            Index           =   3
            Left            =   210
            Locked          =   -1  'True
            TabIndex        =   324
            TabStop         =   0   'False
            Top             =   1495
            Width           =   3435
         End
         Begin VB.TextBox txtConjuge 
            Height          =   300
            Index           =   5
            Left            =   7230
            Locked          =   -1  'True
            TabIndex        =   323
            TabStop         =   0   'False
            Top             =   1495
            Width           =   480
         End
         Begin VB.TextBox txtConjuge 
            Height          =   300
            Index           =   6
            Left            =   7770
            Locked          =   -1  'True
            TabIndex        =   322
            TabStop         =   0   'False
            Top             =   1495
            Width           =   1455
         End
         Begin VB.TextBox txtConjuge 
            Height          =   300
            Index           =   7
            Left            =   210
            Locked          =   -1  'True
            TabIndex        =   321
            TabStop         =   0   'False
            Top             =   2024
            Width           =   615
         End
         Begin VB.TextBox txtConjuge 
            Height          =   300
            Index           =   8
            Left            =   870
            Locked          =   -1  'True
            TabIndex        =   320
            TabStop         =   0   'False
            Top             =   2024
            Width           =   1395
         End
         Begin VB.TextBox txtConjuge 
            Height          =   300
            Index           =   4
            Left            =   3690
            Locked          =   -1  'True
            TabIndex        =   319
            TabStop         =   0   'False
            Top             =   1495
            Width           =   3465
         End
         Begin VB.TextBox txtConjuge 
            Height          =   300
            Index           =   9
            Left            =   2310
            Locked          =   -1  'True
            TabIndex        =   318
            TabStop         =   0   'False
            Top             =   2024
            Width           =   2415
         End
         Begin VB.TextBox txtConjuge 
            Height          =   300
            Index           =   10
            Left            =   4770
            Locked          =   -1  'True
            TabIndex        =   317
            TabStop         =   0   'False
            Top             =   2024
            Width           =   2385
         End
         Begin VB.TextBox txtConjuge 
            Height          =   285
            Index           =   1
            Left            =   210
            Locked          =   -1  'True
            TabIndex        =   316
            TabStop         =   0   'False
            Top             =   452
            Width           =   9045
         End
         Begin VB.Label lbltexto 
            AutoSize        =   -1  'True
            Caption         =   $"ConPropAdesao.frx":0803
            Height          =   195
            Index           =   54
            Left            =   210
            TabIndex        =   338
            Top             =   1260
            Width           =   7905
         End
         Begin VB.Label lbltexto 
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            Height          =   285
            Index           =   42
            Left            =   8400
            TabIndex        =   337
            Top             =   2550
            Width           =   615
         End
         Begin VB.Label lbltexto 
            AutoSize        =   -1  'True
            Caption         =   $"ConPropAdesao.frx":08A8
            Height          =   195
            Index           =   39
            Left            =   210
            TabIndex        =   336
            Top             =   2310
            Width           =   8520
         End
         Begin VB.Label lbltexto 
            AutoSize        =   -1  'True
            Caption         =   "Endere�o"
            Height          =   195
            Index           =   58
            Left            =   210
            TabIndex        =   335
            Top             =   720
            Width           =   690
         End
         Begin VB.Label lbltexto 
            AutoSize        =   -1  'True
            Caption         =   $"ConPropAdesao.frx":0949
            Height          =   195
            Index           =   56
            Left            =   210
            TabIndex        =   334
            Top             =   1785
            Width           =   8955
         End
         Begin VB.Label lbltexto 
            AutoSize        =   -1  'True
            Height          =   195
            Index           =   53
            Left            =   7230
            TabIndex        =   333
            Top             =   1260
            Width           =   45
         End
         Begin VB.Label lbltexto 
            AutoSize        =   -1  'True
            Caption         =   "Nome"
            Height          =   195
            Index           =   46
            Left            =   210
            TabIndex        =   332
            Top             =   210
            Width           =   420
         End
      End
      Begin VB.CommandButton cmdExibirTodos 
         Caption         =   "Exibir Todos"
         Height          =   375
         Left            =   -64680
         TabIndex        =   391
         Top             =   8880
         Width           =   1095
      End
      Begin VB.TextBox txtPercDespesa 
         Height          =   315
         Left            =   -66945
         Locked          =   -1  'True
         TabIndex        =   312
         Top             =   1470
         Width           =   1335
      End
      Begin VB.TextBox txtPercParticipacao 
         Height          =   315
         Left            =   -68460
         Locked          =   -1  'True
         TabIndex        =   311
         Top             =   1470
         Width           =   1335
      End
      Begin VB.TextBox txtDescricao 
         Height          =   2415
         Left            =   -74760
         Locked          =   -1  'True
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   305
         Top             =   5835
         Width           =   10785
      End
      Begin VB.TextBox txtNumOrdem 
         Height          =   300
         Left            =   -69975
         Locked          =   -1  'True
         TabIndex        =   303
         Top             =   1485
         Width           =   1335
      End
      Begin VB.ComboBox cmbCongenere 
         Height          =   315
         Left            =   -74835
         Style           =   2  'Dropdown List
         TabIndex        =   302
         Top             =   1485
         Width           =   4665
      End
      Begin VB.Frame Frame7 
         Caption         =   "Mutu�rios"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   5655
         Left            =   -74910
         TabIndex        =   289
         Top             =   1500
         Width           =   11415
         Begin GridAlianca.grdAlianca grdMutuario 
            Height          =   4575
            Left            =   120
            TabIndex        =   290
            Top             =   360
            Width           =   11175
            _ExtentX        =   19711
            _ExtentY        =   8070
            BorderStyle     =   1
            AllowUserResizing=   3
            EditData        =   0   'False
            Highlight       =   1
            ShowTip         =   0   'False
            SortOnHeader    =   0   'False
            BackColor       =   -2147483643
            BackColorBkg    =   -2147483633
            BackColorFixed  =   -2147483633
            BackColorSel    =   -2147483635
            FixedCols       =   1
            FixedRows       =   1
            FocusRect       =   0
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty FontFixed {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   -2147483640
            ForeColorFixed  =   -2147483630
            ForeColorSel    =   -2147483634
            GridColor       =   -2147483630
            GridColorFixed  =   12632256
            GridLine        =   1
            GridLinesFixed  =   2
            MousePointer    =   0
            Redraw          =   -1  'True
            Rows            =   2
            TextStyle       =   0
            TextStyleFixed  =   0
            Cols            =   18
            RowHeightMin    =   0
         End
      End
      Begin VB.Frame Frame3 
         Caption         =   "Restitui��o"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   2175
         Left            =   -74730
         TabIndex        =   287
         Top             =   7020
         Width           =   11025
         Begin MSFlexGridLib.MSFlexGrid GridProposta 
            Height          =   1845
            Index           =   2
            Left            =   210
            TabIndex        =   288
            Top             =   240
            Width           =   10575
            _ExtentX        =   18653
            _ExtentY        =   3254
            _Version        =   393216
            Cols            =   6
            FixedCols       =   0
            FormatString    =   $"ConPropAdesao.frx":09E0
         End
      End
      Begin VB.Frame fraMutuarios 
         Caption         =   "Mutu�rios"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   2355
         Left            =   -74640
         TabIndex        =   281
         Top             =   7140
         Visible         =   0   'False
         Width           =   10935
         Begin MSFlexGridLib.MSFlexGrid grdMutuarios 
            Height          =   1995
            Left            =   150
            TabIndex        =   282
            Top             =   240
            Width           =   10455
            _ExtentX        =   18441
            _ExtentY        =   3519
            _Version        =   393216
            Cols            =   8
            FixedCols       =   0
            AllowUserResizing=   1
            FormatString    =   $"ConPropAdesao.frx":0AFA
         End
      End
      Begin VB.Frame Frame12 
         Caption         =   "Cobran�a "
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1950
         Index           =   0
         Left            =   -74820
         TabIndex        =   279
         Top             =   990
         Width           =   11295
         Begin VB.CommandButton cmdDadosFinanceiros 
            Caption         =   "Extrato de Cobran�a &Detalhado"
            Height          =   330
            Index           =   2
            Left            =   5850
            TabIndex        =   389
            Top             =   1575
            Width           =   2625
         End
         Begin VB.CommandButton cmdDadosFinanceiros 
            Caption         =   "&Exportar Para Excel"
            Height          =   330
            Index           =   3
            Left            =   8550
            TabIndex        =   388
            Top             =   1575
            Width           =   2625
         End
         Begin MSFlexGridLib.MSFlexGrid MSFlexCobranca 
            Height          =   1335
            Left            =   75
            TabIndex        =   280
            Top             =   240
            Width           =   11130
            _ExtentX        =   19632
            _ExtentY        =   2355
            _Version        =   393216
            Cols            =   17
            FixedCols       =   0
            AllowUserResizing=   1
            FormatString    =   $"ConPropAdesao.frx":0C0A
         End
         Begin VB.Label lblImpedirAgendamento 
            Caption         =   "Agendamentos/cobran�as futuras impedidos."
            Height          =   195
            Left            =   1125
            TabIndex        =   390
            Top             =   1620
            Visible         =   0   'False
            Width           =   3885
         End
      End
      Begin VB.Frame fraSubgrupo 
         Caption         =   "Subgrupo "
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   615
         Index           =   1
         Left            =   -74820
         TabIndex        =   255
         Top             =   2925
         Visible         =   0   'False
         Width           =   11295
         Begin VB.ComboBox cboSubgrupo 
            Height          =   315
            Index           =   1
            Left            =   150
            TabIndex        =   20
            Top             =   240
            Width           =   9975
         End
      End
      Begin VB.Frame fraSubgrupo 
         Caption         =   "Subgrupo "
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   675
         Index           =   2
         Left            =   -74790
         TabIndex        =   254
         Top             =   1140
         Visible         =   0   'False
         Width           =   11235
         Begin VB.ComboBox cboSubgrupo 
            Height          =   315
            Index           =   3
            Left            =   240
            TabIndex        =   80
            Top             =   240
            Width           =   10815
         End
      End
      Begin VB.Frame fraSubgrupo 
         Caption         =   "Subgrupo "
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   675
         Index           =   0
         Left            =   -74820
         TabIndex        =   251
         Top             =   6255
         Visible         =   0   'False
         Width           =   11265
         Begin VB.ComboBox cboSubgrupo 
            Height          =   315
            Index           =   0
            Left            =   210
            TabIndex        =   17
            Top             =   240
            Width           =   10875
         End
      End
      Begin VB.Frame Frame15 
         Caption         =   "Produto "
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1515
         Left            =   -74820
         TabIndex        =   246
         Top             =   1095
         Width           =   11265
         Begin VB.TextBox txtRamo 
            Height          =   300
            Left            =   240
            Locked          =   -1  'True
            TabIndex        =   1
            TabStop         =   0   'False
            Top             =   1020
            Width           =   2880
         End
         Begin VB.TextBox txtSubRamo 
            Height          =   300
            Left            =   3300
            Locked          =   -1  'True
            TabIndex        =   2
            TabStop         =   0   'False
            Top             =   1020
            Width           =   5790
         End
         Begin VB.TextBox txtProduto 
            Height          =   300
            Left            =   240
            Locked          =   -1  'True
            TabIndex        =   0
            TabStop         =   0   'False
            Top             =   450
            Width           =   8820
         End
         Begin VB.Label lbltexto 
            AutoSize        =   -1  'True
            Caption         =   "Ramo                                                          Sub-Ramo"
            Height          =   195
            Index           =   28
            Left            =   240
            TabIndex        =   248
            Top             =   795
            Width           =   3780
         End
         Begin VB.Label lbltexto 
            Caption         =   "Produto"
            Height          =   255
            Index           =   22
            Left            =   240
            TabIndex        =   247
            Top             =   240
            Width           =   1005
         End
      End
      Begin VB.Frame Frame14 
         Caption         =   "Coberturas"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   4065
         Index           =   0
         Left            =   -74790
         TabIndex        =   162
         Top             =   1830
         Width           =   11235
         Begin VB.ComboBox cboComponente 
            Height          =   315
            Left            =   2220
            Style           =   2  'Dropdown List
            TabIndex        =   81
            Top             =   390
            Width           =   6495
         End
         Begin VB.TextBox txtPlanoCob 
            Height          =   285
            Index           =   0
            Left            =   1440
            Locked          =   -1  'True
            TabIndex        =   83
            TabStop         =   0   'False
            Top             =   870
            Width           =   9525
         End
         Begin VB.TextBox txtPlanoIdCob 
            Height          =   285
            Index           =   0
            Left            =   840
            Locked          =   -1  'True
            TabIndex        =   82
            TabStop         =   0   'False
            Top             =   870
            Width           =   555
         End
         Begin MSFlexGridLib.MSFlexGrid gridCobertura 
            Height          =   2505
            Left            =   240
            TabIndex        =   84
            Top             =   1380
            Width           =   10755
            _ExtentX        =   18971
            _ExtentY        =   4419
            _Version        =   393216
            Cols            =   5
            FixedCols       =   0
            AllowUserResizing=   1
            FormatString    =   "C�d. | Tipo de Cobertura | Descri��o                                        | Capital Segurado M�nimo | Capital Segurado M�ximo"
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "Selecionar o Componente :"
            Height          =   195
            Index           =   44
            Left            =   240
            TabIndex        =   253
            Top             =   450
            Width           =   1920
         End
         Begin VB.Label Label15 
            Caption         =   "Plano"
            Height          =   255
            Index           =   3
            Left            =   210
            TabIndex        =   163
            Top             =   900
            Width           =   615
         End
      End
      Begin VB.Frame Frame8 
         Caption         =   "C�njuge "
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   3705
         Index           =   1
         Left            =   -74760
         TabIndex        =   159
         Top             =   4770
         Width           =   11145
         Begin VB.TextBox txtBeneficiarioConj 
            Height          =   3195
            Left            =   240
            Locked          =   -1  'True
            MultiLine       =   -1  'True
            TabIndex        =   76
            TabStop         =   0   'False
            Top             =   360
            Width           =   10635
         End
      End
      Begin VB.Frame Frame5 
         Caption         =   "Hist�rico de avalia��es "
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   2175
         Left            =   -74760
         TabIndex        =   158
         Top             =   4755
         Width           =   11025
         Begin MSFlexGridLib.MSFlexGrid GridProposta 
            Height          =   1845
            Index           =   1
            Left            =   240
            TabIndex        =   79
            Top             =   240
            Width           =   10575
            _ExtentX        =   18653
            _ExtentY        =   3254
            _Version        =   393216
            Cols            =   8
            FixedCols       =   0
            AllowUserResizing=   3
            FormatString    =   $"ConPropAdesao.frx":0D2D
         End
         Begin VB.TextBox txtMotRec 
            Height          =   285
            Left            =   240
            Locked          =   -1  'True
            TabIndex        =   297
            Top             =   1800
            Visible         =   0   'False
            Width           =   10565
         End
         Begin VB.Label lblMoteRec 
            AutoSize        =   -1  'True
            Caption         =   "Motivo da recusa:"
            Height          =   195
            Left            =   240
            TabIndex        =   296
            Top             =   1560
            Visible         =   0   'False
            Width           =   1275
         End
      End
      Begin VB.Frame Frame8 
         Caption         =   "Titular "
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   3705
         Index           =   0
         Left            =   -74760
         TabIndex        =   157
         Top             =   1020
         Width           =   11145
         Begin VB.TextBox txtBeneficiario 
            Height          =   3195
            Left            =   240
            Locked          =   -1  'True
            MultiLine       =   -1  'True
            TabIndex        =   75
            TabStop         =   0   'False
            Top             =   360
            Width           =   10665
         End
      End
      Begin VB.Frame Frame11 
         Caption         =   "Inadimpl�ncia "
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1425
         Index           =   0
         Left            =   -74880
         TabIndex        =   156
         Top             =   9525
         Width           =   11295
         Begin MSFlexGridLib.MSFlexGrid MSFlexInadimplencia 
            Height          =   1110
            Left            =   120
            TabIndex        =   46
            Top             =   210
            Width           =   10965
            _ExtentX        =   19341
            _ExtentY        =   1958
            _Version        =   393216
            Cols            =   6
            FixedCols       =   0
            AllowUserResizing=   1
            FormatString    =   $"ConPropAdesao.frx":0E86
         End
      End
      Begin VB.Frame Frame11 
         Caption         =   "Assist�ncia "
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   795
         Index           =   1
         Left            =   -74880
         TabIndex        =   154
         Top             =   8730
         Width           =   7335
         Begin VB.CommandButton cmdAssistencia 
            Caption         =   "Consultar"
            Height          =   540
            Left            =   600
            TabIndex        =   379
            Top             =   840
            Visible         =   0   'False
            Width           =   1395
         End
         Begin VB.TextBox txtAssistFacul 
            Height          =   285
            Left            =   1185
            Locked          =   -1  'True
            TabIndex        =   45
            TabStop         =   0   'False
            Text            =   "N�O"
            Top             =   390
            Width           =   855
         End
         Begin VB.TextBox txtAssistObrigatoria 
            Height          =   285
            Left            =   180
            Locked          =   -1  'True
            TabIndex        =   44
            TabStop         =   0   'False
            Text            =   "N�O"
            Top             =   390
            Width           =   975
         End
         Begin VB.Label lblAssistFacul 
            Caption         =   "Facultativa"
            Height          =   255
            Left            =   1230
            TabIndex        =   161
            Top             =   180
            Width           =   975
         End
         Begin VB.Label lblAssistObrigatoria 
            Caption         =   "Compuls�ria"
            Height          =   255
            Left            =   180
            TabIndex        =   160
            Top             =   180
            Width           =   1695
         End
      End
      Begin VB.Frame fraPremio 
         Caption         =   "Pr�mio Atual"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   795
         Left            =   -74820
         TabIndex        =   147
         Top             =   4335
         Width           =   11295
         Begin VB.CommandButton cmdDadosFinanceiros 
            Caption         =   "&Evolu��o"
            Height          =   375
            Index           =   1
            Left            =   9960
            TabIndex        =   295
            Top             =   300
            Width           =   1095
         End
         Begin VB.TextBox txtDescTec 
            Alignment       =   1  'Right Justify
            Height          =   285
            Index           =   2
            Left            =   4110
            Locked          =   -1  'True
            TabIndex        =   30
            TabStop         =   0   'False
            Top             =   435
            Width           =   1320
         End
         Begin VB.TextBox txtPremioDGN 
            Alignment       =   1  'Right Justify
            Height          =   285
            Left            =   8370
            Locked          =   -1  'True
            TabIndex        =   33
            TabStop         =   0   'False
            Top             =   435
            Visible         =   0   'False
            Width           =   1380
         End
         Begin VB.TextBox txtCustoCertificado 
            Alignment       =   1  'Right Justify
            Height          =   285
            Left            =   5445
            Locked          =   -1  'True
            TabIndex        =   31
            Top             =   435
            Width           =   1320
         End
         Begin VB.TextBox txtPremioVista 
            Alignment       =   1  'Right Justify
            Height          =   285
            Index           =   0
            Left            =   6780
            Locked          =   -1  'True
            TabIndex        =   32
            TabStop         =   0   'False
            Top             =   420
            Width           =   1410
         End
         Begin VB.TextBox txtDescComercial 
            Alignment       =   1  'Right Justify
            Height          =   285
            Index           =   1
            Left            =   2775
            Locked          =   -1  'True
            TabIndex        =   29
            TabStop         =   0   'False
            Top             =   435
            Width           =   1320
         End
         Begin VB.TextBox txtPremio 
            Alignment       =   1  'Right Justify
            Height          =   285
            Left            =   120
            Locked          =   -1  'True
            TabIndex        =   27
            TabStop         =   0   'False
            Top             =   435
            Width           =   1320
         End
         Begin VB.TextBox txtIOF 
            Alignment       =   1  'Right Justify
            Height          =   285
            Left            =   1440
            Locked          =   -1  'True
            TabIndex        =   28
            TabStop         =   0   'False
            Top             =   435
            Width           =   1320
         End
         Begin VB.Label lblPremioDGN 
            AutoSize        =   -1  'True
            Caption         =   "Pr�mio diag. C�ncer"
            Height          =   195
            Left            =   8370
            TabIndex        =   153
            Top             =   240
            Visible         =   0   'False
            Width           =   1425
         End
         Begin VB.Label lblCustoCertificado 
            AutoSize        =   -1  'True
            Caption         =   "Custo Certificado"
            Height          =   195
            Left            =   5445
            TabIndex        =   152
            Top             =   240
            Width           =   1200
         End
         Begin VB.Label lblPremioBrutoVista 
            AutoSize        =   -1  'True
            Caption         =   "Pr�mio Bruto a vista"
            Height          =   195
            Left            =   6780
            TabIndex        =   151
            Top             =   240
            Width           =   1410
         End
         Begin VB.Label Label19 
            AutoSize        =   -1  'True
            Caption         =   "Desc. Comercial     Desc. T�cnico"
            Height          =   195
            Index           =   0
            Left            =   2775
            TabIndex        =   150
            Top             =   240
            Width           =   2430
         End
         Begin VB.Label LblPremio 
            AutoSize        =   -1  'True
            Caption         =   "Pr�mio Bruto"
            Height          =   195
            Left            =   120
            TabIndex        =   149
            Top             =   240
            Width           =   900
         End
         Begin VB.Label lblIOF 
            AutoSize        =   -1  'True
            Caption         =   "IOF"
            Height          =   195
            Left            =   1470
            TabIndex        =   148
            Top             =   240
            Width           =   255
         End
      End
      Begin VB.Frame fraCapitalSegurado 
         Caption         =   "Capital Segurado Atual"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   795
         Left            =   -74820
         TabIndex        =   141
         Top             =   3525
         Width           =   11295
         Begin VB.CommandButton cmdDadosFinanceiros 
            Caption         =   "Facilitador Sist�mico de C�lculo"
            Height          =   585
            Index           =   0
            Left            =   9300
            TabIndex        =   384
            Top             =   150
            Visible         =   0   'False
            Width           =   1770
         End
         Begin VB.TextBox txtISDGN 
            Alignment       =   1  'Right Justify
            Height          =   285
            Left            =   8640
            Locked          =   -1  'True
            TabIndex        =   26
            TabStop         =   0   'False
            Top             =   435
            Visible         =   0   'False
            Width           =   1455
         End
         Begin VB.TextBox txtPlano 
            Height          =   285
            Index           =   0
            Left            =   2280
            Locked          =   -1  'True
            TabIndex        =   23
            TabStop         =   0   'False
            Top             =   435
            Width           =   4035
         End
         Begin VB.TextBox txtCoefCustoPlano 
            Alignment       =   1  'Right Justify
            Height          =   285
            Left            =   6330
            Locked          =   -1  'True
            TabIndex        =   24
            TabStop         =   0   'False
            Top             =   435
            Width           =   1065
         End
         Begin VB.TextBox txtQtdSalPlano 
            Alignment       =   1  'Right Justify
            Height          =   285
            Left            =   7410
            Locked          =   -1  'True
            MaxLength       =   4
            TabIndex        =   25
            TabStop         =   0   'False
            Top             =   435
            Width           =   735
         End
         Begin VB.TextBox txtPlanoId 
            Alignment       =   1  'Right Justify
            Height          =   285
            Index           =   0
            Left            =   1680
            Locked          =   -1  'True
            TabIndex        =   22
            TabStop         =   0   'False
            Top             =   435
            Width           =   555
         End
         Begin VB.TextBox txtIS 
            Alignment       =   1  'Right Justify
            Height          =   285
            Left            =   120
            Locked          =   -1  'True
            TabIndex        =   21
            TabStop         =   0   'False
            Top             =   435
            Width           =   1530
         End
         Begin VB.Label lblISDGN 
            AutoSize        =   -1  'True
            Caption         =   "Capital Seg. Diag. C�ncer"
            Height          =   195
            Left            =   8640
            TabIndex        =   146
            Top             =   240
            Visible         =   0   'False
            Width           =   1830
         End
         Begin VB.Label LblInfPlEmpresa 
            AutoSize        =   -1  'True
            Caption         =   "Qtd. Sal�rios"
            Height          =   195
            Index           =   0
            Left            =   7410
            TabIndex        =   145
            Top             =   240
            Width           =   900
         End
         Begin VB.Label LblInfPlEmpresa 
            AutoSize        =   -1  'True
            Caption         =   "Coef. Custo"
            Height          =   195
            Index           =   1
            Left            =   6360
            TabIndex        =   144
            Top             =   240
            Width           =   825
         End
         Begin VB.Label Label15 
            AutoSize        =   -1  'True
            Caption         =   "Plano     Nome do Plano"
            Height          =   195
            Index           =   1
            Left            =   1680
            TabIndex        =   143
            Top             =   240
            Width           =   1725
         End
         Begin VB.Label Label15 
            AutoSize        =   -1  'True
            Caption         =   "Capital Segurado"
            Height          =   195
            Index           =   2
            Left            =   150
            TabIndex        =   142
            Top             =   240
            Width           =   1215
         End
      End
      Begin VB.Frame Frame6 
         Caption         =   "Declara��oTitular"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   3045
         Left            =   -74640
         TabIndex        =   115
         Top             =   1020
         Width           =   10905
         Begin VB.TextBox txtCboTit 
            Height          =   285
            Left            =   2190
            Locked          =   -1  'True
            TabIndex        =   70
            Top             =   150
            Width           =   615
         End
         Begin VB.TextBox txtDescProftit 
            Height          =   285
            Left            =   2910
            Locked          =   -1  'True
            TabIndex        =   71
            Top             =   150
            Width           =   7755
         End
         Begin MSComctlLib.ListView lvwDeclaracaoTitular 
            Height          =   2415
            Left            =   210
            TabIndex        =   72
            Top             =   450
            Width           =   10485
            _ExtentX        =   18494
            _ExtentY        =   4260
            View            =   3
            LabelWrap       =   -1  'True
            HideSelection   =   -1  'True
            Checkboxes      =   -1  'True
            GridLines       =   -1  'True
            HoverSelection  =   -1  'True
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   1
            NumItems        =   4
            BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Text            =   "Item"
               Object.Width           =   1058
            EndProperty
            BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   1
               Text            =   "Pergunta"
               Object.Width           =   2540
            EndProperty
            BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   2
               Text            =   "Resposta"
               Object.Width           =   2540
            EndProperty
            BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   3
               Text            =   "Descri��o"
               Object.Width           =   5292
            EndProperty
         End
         Begin VB.Label LblProfissao 
            AutoSize        =   -1  'True
            Caption         =   "Profiss�o"
            Height          =   195
            Left            =   300
            TabIndex        =   116
            Top             =   210
            Width           =   645
         End
      End
      Begin VB.Frame Frame2 
         Caption         =   "Proponente"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   3375
         Index           =   0
         Left            =   -74820
         TabIndex        =   113
         Top             =   1020
         Width           =   11295
         Begin VB.CheckBox chkPPE 
            Caption         =   "PPE"
            Height          =   195
            Left            =   9450
            TabIndex        =   301
            Top             =   2955
            Visible         =   0   'False
            Width           =   765
         End
         Begin VB.TextBox txtCodMciTit 
            Height          =   300
            Left            =   7230
            Locked          =   -1  'True
            TabIndex        =   57
            TabStop         =   0   'False
            Top             =   1860
            Width           =   3225
         End
         Begin VB.TextBox txtClienteId 
            Height          =   300
            Left            =   4800
            Locked          =   -1  'True
            TabIndex        =   56
            TabStop         =   0   'False
            Top             =   1860
            Width           =   2385
         End
         Begin VB.TextBox txtIdadeTit 
            Height          =   285
            Left            =   9300
            Locked          =   -1  'True
            TabIndex        =   63
            TabStop         =   0   'False
            Top             =   2400
            Width           =   1155
         End
         Begin VB.TextBox txtSexo 
            Height          =   285
            Left            =   2520
            Locked          =   -1  'True
            TabIndex        =   59
            TabStop         =   0   'False
            Top             =   2400
            Width           =   375
         End
         Begin VB.TextBox txtDtNascTit 
            Height          =   285
            Left            =   8160
            Locked          =   -1  'True
            TabIndex        =   62
            TabStop         =   0   'False
            Top             =   2400
            Width           =   1095
         End
         Begin VB.TextBox txtCPFTit 
            Height          =   285
            Left            =   6300
            Locked          =   -1  'True
            TabIndex        =   61
            TabStop         =   0   'False
            Top             =   2400
            Width           =   1815
         End
         Begin VB.TextBox txtTpPessoa 
            Height          =   285
            Left            =   240
            Locked          =   -1  'True
            TabIndex        =   58
            TabStop         =   0   'False
            Top             =   2400
            Width           =   2175
         End
         Begin VB.TextBox TxtContato 
            Height          =   285
            Left            =   4500
            Locked          =   -1  'True
            TabIndex        =   65
            TabStop         =   0   'False
            Top             =   2400
            Width           =   5955
         End
         Begin VB.TextBox TxtCGC 
            Height          =   285
            Left            =   2520
            Locked          =   -1  'True
            TabIndex        =   64
            TabStop         =   0   'False
            Top             =   2400
            Width           =   1815
         End
         Begin VB.TextBox txtNoDocIdentidade 
            Height          =   300
            Left            =   240
            Locked          =   -1  'True
            TabIndex        =   66
            TabStop         =   0   'False
            Top             =   2910
            Width           =   1815
         End
         Begin VB.TextBox txtTpDocIdentidade 
            Height          =   300
            Left            =   2220
            Locked          =   -1  'True
            TabIndex        =   67
            TabStop         =   0   'False
            Top             =   2910
            Width           =   3015
         End
         Begin VB.TextBox txtDtExpedicaoIdentidade 
            Height          =   300
            Left            =   5430
            Locked          =   -1  'True
            TabIndex        =   68
            TabStop         =   0   'False
            Top             =   2910
            Width           =   1815
         End
         Begin VB.TextBox txtOrgaoExpIdentidade 
            Height          =   300
            Left            =   7440
            Locked          =   -1  'True
            TabIndex        =   69
            TabStop         =   0   'False
            Top             =   2910
            Width           =   1815
         End
         Begin VB.TextBox txtEmail 
            Height          =   300
            Left            =   2310
            Locked          =   -1  'True
            TabIndex        =   55
            TabStop         =   0   'False
            Top             =   1860
            Width           =   2415
         End
         Begin VB.TextBox txtBairro 
            Height          =   300
            Left            =   3720
            Locked          =   -1  'True
            TabIndex        =   50
            TabStop         =   0   'False
            Top             =   1380
            Width           =   4755
         End
         Begin VB.TextBox txtTelefone 
            Height          =   300
            Left            =   900
            Locked          =   -1  'True
            TabIndex        =   54
            TabStop         =   0   'False
            Top             =   1860
            Width           =   1395
         End
         Begin VB.TextBox txtDDD 
            Height          =   300
            Left            =   240
            Locked          =   -1  'True
            TabIndex        =   53
            TabStop         =   0   'False
            Top             =   1860
            Width           =   615
         End
         Begin VB.TextBox txtCEP 
            Height          =   300
            Left            =   9030
            Locked          =   -1  'True
            TabIndex        =   52
            TabStop         =   0   'False
            Top             =   1380
            Width           =   1425
         End
         Begin VB.TextBox txtUF 
            Height          =   300
            Left            =   8520
            Locked          =   -1  'True
            TabIndex        =   51
            TabStop         =   0   'False
            Top             =   1380
            Width           =   480
         End
         Begin VB.TextBox txtMunicipio 
            Height          =   300
            Left            =   240
            Locked          =   -1  'True
            TabIndex        =   49
            TabStop         =   0   'False
            Top             =   1380
            Width           =   3435
         End
         Begin VB.TextBox txtEndereco 
            Height          =   300
            Left            =   240
            Locked          =   -1  'True
            TabIndex        =   48
            TabStop         =   0   'False
            Top             =   900
            Width           =   10215
         End
         Begin VB.TextBox txtNome 
            Height          =   300
            Left            =   240
            Locked          =   -1  'True
            TabIndex        =   47
            TabStop         =   0   'False
            Top             =   405
            Width           =   10215
         End
         Begin VB.TextBox txtEstCivil 
            Height          =   285
            Left            =   3000
            Locked          =   -1  'True
            TabIndex        =   60
            TabStop         =   0   'False
            Top             =   2400
            Width           =   3195
         End
         Begin VB.Label lbltexto 
            Caption         =   "Sexo"
            Height          =   255
            Index           =   8
            Left            =   2520
            TabIndex        =   140
            Top             =   2220
            Width           =   435
         End
         Begin VB.Label lbltexto 
            AutoSize        =   -1  'True
            Caption         =   "Est.Civil"
            Height          =   195
            Index           =   9
            Left            =   3030
            TabIndex        =   139
            Top             =   2220
            Width           =   675
         End
         Begin VB.Label lbltexto 
            Caption         =   "CPF / CNPJ"
            Height          =   255
            Index           =   10
            Left            =   6270
            TabIndex        =   138
            Top             =   2220
            Width           =   1245
         End
         Begin VB.Label lbltexto 
            Caption         =   "Dt.Nasc."
            Height          =   255
            Index           =   11
            Left            =   8190
            TabIndex        =   137
            Top             =   2190
            Width           =   735
         End
         Begin VB.Label lbltexto 
            Caption         =   "Idade"
            Height          =   255
            Index           =   12
            Left            =   9300
            TabIndex        =   136
            Top             =   2190
            Width           =   735
         End
         Begin VB.Label lbltexto 
            AutoSize        =   -1  'True
            Caption         =   "Tipo de Pessoa "
            Height          =   195
            Index           =   34
            Left            =   240
            TabIndex        =   135
            Top             =   2190
            Width           =   1155
         End
         Begin VB.Label lbltexto 
            Caption         =   "Contato"
            Height          =   255
            Index           =   7
            Left            =   4530
            TabIndex        =   134
            Top             =   2190
            Width           =   735
         End
         Begin VB.Label lbltexto 
            Caption         =   "CGC"
            Height          =   255
            Index           =   1
            Left            =   2520
            TabIndex        =   133
            Top             =   2190
            Width           =   735
         End
         Begin VB.Label lbltexto 
            Caption         =   "No. Doc Identidade"
            Height          =   255
            Index           =   35
            Left            =   270
            TabIndex        =   132
            Top             =   2700
            Width           =   1605
         End
         Begin VB.Label lbltexto 
            Caption         =   "Tipo Documento Identidade"
            Height          =   255
            Index           =   36
            Left            =   2220
            TabIndex        =   131
            Top             =   2700
            Width           =   2055
         End
         Begin VB.Label lbltexto 
            Caption         =   "Data Expedi��o"
            Height          =   255
            Index           =   37
            Left            =   5430
            TabIndex        =   130
            Top             =   2700
            Width           =   1605
         End
         Begin VB.Label lbltexto 
            Caption         =   "�rg�o Expedidor"
            Height          =   255
            Index           =   38
            Left            =   7440
            TabIndex        =   129
            Top             =   2700
            Width           =   1605
         End
         Begin VB.Label lbltexto 
            Caption         =   "Nome"
            Height          =   255
            Index           =   27
            Left            =   270
            TabIndex        =   126
            Top             =   225
            Width           =   735
         End
         Begin VB.Label lbltexto 
            Caption         =   $"ConPropAdesao.frx":0F1C
            Height          =   255
            Index           =   5
            Left            =   270
            TabIndex        =   122
            Top             =   1200
            Width           =   10095
         End
         Begin VB.Label lbltexto 
            Caption         =   $"ConPropAdesao.frx":0FDC
            Height          =   255
            Index           =   3
            Left            =   270
            TabIndex        =   121
            Top             =   1680
            Width           =   10215
         End
         Begin VB.Label lbltexto 
            Caption         =   "Endere�o"
            Height          =   255
            Index           =   0
            Left            =   270
            TabIndex        =   114
            Top             =   690
            Width           =   735
         End
      End
      Begin VB.Frame Frame12 
         Caption         =   "Dados Banc�rios "
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1890
         Index           =   1
         Left            =   -74800
         TabIndex        =   107
         Top             =   6825
         Width           =   11295
         Begin VB.TextBox txtNumParcelas 
            Alignment       =   1  'Right Justify
            Height          =   285
            Left            =   5340
            Locked          =   -1  'True
            TabIndex        =   43
            TabStop         =   0   'False
            Top             =   1365
            Width           =   1440
         End
         Begin VB.TextBox txtTpPgto 
            Height          =   285
            Left            =   2400
            Locked          =   -1  'True
            TabIndex        =   42
            TabStop         =   0   'False
            Top             =   1365
            Width           =   2760
         End
         Begin VB.TextBox txtFormaPgto 
            Height          =   285
            Left            =   150
            Locked          =   -1  'True
            TabIndex        =   41
            TabStop         =   0   'False
            Top             =   1350
            Width           =   2145
         End
         Begin VB.TextBox txtIndTitCC 
            Height          =   285
            Left            =   8850
            Locked          =   -1  'True
            TabIndex        =   36
            TabStop         =   0   'False
            Top             =   390
            Visible         =   0   'False
            Width           =   1815
         End
         Begin VB.TextBox txtDiaDebito 
            Height          =   285
            Left            =   4320
            Locked          =   -1  'True
            TabIndex        =   38
            TabStop         =   0   'False
            Top             =   900
            Width           =   1665
         End
         Begin VB.TextBox txtCCDeb 
            Height          =   285
            Left            =   4320
            Locked          =   -1  'True
            TabIndex        =   35
            TabStop         =   0   'False
            Top             =   405
            Width           =   4410
         End
         Begin VB.TextBox txtAgenciaDeb 
            Height          =   285
            Left            =   150
            Locked          =   -1  'True
            TabIndex        =   34
            TabStop         =   0   'False
            Top             =   420
            Width           =   4005
         End
         Begin VB.TextBox txtDtCartao 
            Height          =   285
            Left            =   8850
            Locked          =   -1  'True
            TabIndex        =   40
            TabStop         =   0   'False
            Top             =   900
            Width           =   1815
         End
         Begin VB.TextBox txtNumCartao 
            Height          =   285
            Left            =   6120
            Locked          =   -1  'True
            TabIndex        =   39
            TabStop         =   0   'False
            Top             =   900
            Width           =   2610
         End
         Begin VB.TextBox txtAgencia 
            Height          =   285
            Left            =   150
            Locked          =   -1  'True
            TabIndex        =   37
            TabStop         =   0   'False
            Top             =   900
            Width           =   4005
         End
         Begin VB.Label lbltexto 
            AutoSize        =   -1  'True
            Caption         =   "Forma de Pagamento                Tipo de Pagamento do Pr�mio                  N�mero de Parcelas"
            Height          =   195
            Index           =   43
            Left            =   150
            TabIndex        =   250
            Top             =   1170
            Width           =   6750
         End
         Begin VB.Label lblIndTitCC 
            Caption         =   "Titular de conta corrente"
            Height          =   255
            Left            =   8880
            TabIndex        =   155
            Top             =   120
            Visible         =   0   'False
            Width           =   1815
         End
         Begin VB.Label lbldadosbancarios 
            Caption         =   "Ag�ncia D�bito                                                                     Conta Corrente"
            Height          =   255
            Index           =   0
            Left            =   150
            TabIndex        =   111
            Top             =   210
            Width           =   6495
         End
         Begin VB.Label lblInfCartao 
            Caption         =   "Dt. Vencimento"
            Height          =   255
            Index           =   1
            Left            =   8880
            TabIndex        =   110
            Top             =   660
            Width           =   1455
         End
         Begin VB.Label lblInfCartao 
            Caption         =   "N�mero Cart�o"
            Height          =   255
            Index           =   0
            Left            =   6120
            TabIndex        =   3
            Top             =   690
            Width           =   1455
         End
         Begin VB.Label Label46 
            Caption         =   "Ag�ncia Contratante                                                             Dia D�bito"
            Height          =   255
            Left            =   150
            TabIndex        =   108
            Top             =   690
            Width           =   5055
         End
      End
      Begin VB.Frame frProposta 
         Caption         =   "Proposta "
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   3570
         Left            =   -74820
         TabIndex        =   98
         Top             =   2640
         Width           =   11265
         Begin VB.TextBox txtContratoOpCredito 
            Height          =   315
            Left            =   7830
            TabIndex        =   400
            Top             =   3120
            Width           =   1365
         End
         Begin VB.TextBox txtCertificadoSC 
            Height          =   315
            Index           =   4
            Left            =   1650
            Locked          =   -1  'True
            TabIndex        =   398
            Top             =   2520
            Width           =   1560
         End
         Begin VB.CommandButton cmbSorteio 
            Caption         =   "N�mero da Sorte"
            Height          =   315
            Left            =   4860
            TabIndex        =   375
            Top             =   2520
            Width           =   1425
         End
         Begin VB.TextBox txtCertificadoSC 
            Height          =   300
            Index           =   0
            Left            =   3270
            Locked          =   -1  'True
            TabIndex        =   373
            Top             =   2520
            Width           =   1560
         End
         Begin VB.TextBox txtCertificadoSC 
            Height          =   300
            Index           =   1
            Left            =   9480
            Locked          =   -1  'True
            TabIndex        =   392
            Top             =   2520
            Width           =   1560
         End
         Begin VB.TextBox txtCertificadoSC 
            Height          =   300
            Index           =   2
            Left            =   9480
            Locked          =   -1  'True
            TabIndex        =   394
            Top             =   1920
            Width           =   1560
         End
         Begin VB.TextBox txtCertificadoSC 
            Height          =   300
            Index           =   3
            Left            =   7830
            Locked          =   -1  'True
            TabIndex        =   396
            Top             =   2520
            Width           =   1560
         End
         Begin VB.TextBox txtPropostaSC 
            Height          =   300
            Left            =   240
            Locked          =   -1  'True
            TabIndex        =   371
            Top             =   2520
            Width           =   1365
         End
         Begin VB.TextBox txtPropostaBBAnterior 
            Height          =   300
            Left            =   9630
            Locked          =   -1  'True
            TabIndex        =   300
            TabStop         =   0   'False
            Top             =   540
            Width           =   1455
         End
         Begin VB.TextBox txtTipoProposta 
            Height          =   300
            Left            =   7320
            Locked          =   -1  'True
            TabIndex        =   298
            Top             =   1230
            Visible         =   0   'False
            Width           =   1560
         End
         Begin VB.TextBox txtVencContrato 
            Height          =   300
            Left            =   7920
            TabIndex        =   292
            Top             =   1920
            Width           =   1365
         End
         Begin VB.TextBox txtFormalizacaoContrato 
            Height          =   300
            Left            =   6360
            TabIndex        =   291
            Top             =   1920
            Width           =   1365
         End
         Begin VB.TextBox txtLocalPlantio 
            Height          =   300
            Left            =   9000
            Locked          =   -1  'True
            TabIndex        =   285
            TabStop         =   0   'False
            Top             =   1230
            Width           =   2085
         End
         Begin VB.TextBox txtCanalVenda 
            Height          =   300
            Left            =   6330
            Locked          =   -1  'True
            TabIndex        =   283
            Top             =   2520
            Width           =   1365
         End
         Begin VB.TextBox txtSucursal 
            Height          =   300
            Left            =   8100
            Locked          =   -1  'True
            TabIndex        =   9
            TabStop         =   0   'False
            Top             =   540
            Width           =   1455
         End
         Begin VB.TextBox txtSeguradora 
            Height          =   300
            Left            =   3990
            Locked          =   -1  'True
            TabIndex        =   8
            TabStop         =   0   'False
            Top             =   540
            Width           =   4035
         End
         Begin VB.TextBox txtDtEmissaoProposta 
            Height          =   300
            Left            =   3300
            Locked          =   -1  'True
            TabIndex        =   15
            TabStop         =   0   'False
            Top             =   1920
            Width           =   1365
         End
         Begin VB.TextBox txtDtFimVigProposta 
            Height          =   300
            Left            =   1800
            Locked          =   -1  'True
            TabIndex        =   14
            TabStop         =   0   'False
            Top             =   1920
            Width           =   1365
         End
         Begin VB.TextBox txtDtIniVigProposta 
            Height          =   300
            Left            =   240
            Locked          =   -1  'True
            TabIndex        =   13
            TabStop         =   0   'False
            Top             =   1920
            Width           =   1365
         End
         Begin VB.TextBox txtPropMigracao 
            Height          =   300
            Left            =   7320
            Locked          =   -1  'True
            TabIndex        =   12
            Top             =   1230
            Width           =   1560
         End
         Begin VB.TextBox txtApolice 
            Height          =   300
            Left            =   3270
            Locked          =   -1  'True
            TabIndex        =   7
            TabStop         =   0   'False
            Top             =   540
            Width           =   690
         End
         Begin VB.TextBox TxtCertificado 
            Height          =   300
            Left            =   2400
            Locked          =   -1  'True
            TabIndex        =   6
            TabStop         =   0   'False
            Top             =   540
            Width           =   840
         End
         Begin VB.TextBox TxtQtdVidas 
            Height          =   300
            Left            =   5880
            Locked          =   -1  'True
            TabIndex        =   11
            TabStop         =   0   'False
            Top             =   1230
            Width           =   1365
         End
         Begin VB.TextBox txtPropostaBB 
            Height          =   300
            Left            =   1320
            Locked          =   -1  'True
            TabIndex        =   5
            TabStop         =   0   'False
            Top             =   540
            Width           =   1050
         End
         Begin VB.TextBox txtStatus 
            Height          =   300
            Left            =   240
            Locked          =   -1  'True
            TabIndex        =   10
            TabStop         =   0   'False
            Top             =   1230
            Width           =   5595
         End
         Begin VB.TextBox txtDtContratacao 
            Height          =   300
            Left            =   4830
            Locked          =   -1  'True
            TabIndex        =   16
            TabStop         =   0   'False
            Top             =   1920
            Width           =   1365
         End
         Begin VB.TextBox txtProposta 
            Height          =   300
            Left            =   240
            Locked          =   -1  'True
            TabIndex        =   4
            TabStop         =   0   'False
            Top             =   540
            Width           =   1050
         End
         Begin VB.Label lblCertificadoSC 
            AutoSize        =   -1  'True
            Caption         =   "N� Op Cr�dito"
            Height          =   195
            Index           =   6
            Left            =   7830
            TabIndex        =   401
            Top             =   2880
            Width           =   975
         End
         Begin VB.Label lblCertificadoSC 
            AutoSize        =   -1  'True
            Caption         =   "N� da Apolice SC"
            Height          =   195
            Index           =   4
            Left            =   1650
            TabIndex        =   399
            Top             =   2310
            Width           =   1230
         End
         Begin VB.Label lblCertificadoSC 
            AutoSize        =   -1  'True
            Caption         =   "N� da Certificado SC"
            Height          =   195
            Index           =   0
            Left            =   3270
            TabIndex        =   374
            Top             =   2280
            Width           =   1455
         End
         Begin VB.Label lblCertificadoSC 
            AutoSize        =   -1  'True
            Caption         =   "Cnl. Confirma��o"
            Height          =   195
            Index           =   1
            Left            =   9495
            TabIndex        =   393
            Top             =   2295
            Width           =   1455
         End
         Begin VB.Label lblCertificadoSC 
            AutoSize        =   -1  'True
            Caption         =   "Dt. Slct. Contrata��o"
            Height          =   195
            Index           =   2
            Left            =   9495
            TabIndex        =   395
            Top             =   1665
            Width           =   1485
         End
         Begin VB.Label lblCertificadoSC 
            AutoSize        =   -1  'True
            Caption         =   "Cnl. Condu��o"
            Height          =   195
            Index           =   3
            Left            =   7845
            TabIndex        =   397
            Top             =   2295
            Width           =   1485
         End
         Begin VB.Label lblPropostaSC 
            AutoSize        =   -1  'True
            Caption         =   "N� da Proposta SC"
            Height          =   195
            Left            =   240
            TabIndex        =   372
            Top             =   2280
            Width           =   1335
         End
         Begin VB.Label lblTipoProposta 
            AutoSize        =   -1  'True
            Caption         =   "Tipo Opera��o"
            Height          =   195
            Left            =   7320
            TabIndex        =   299
            Top             =   960
            Visible         =   0   'False
            Width           =   1065
         End
         Begin VB.Label lblNumeroSorte 
            AutoSize        =   -1  'True
            Caption         =   "N�mero da Sorte"
            Height          =   195
            Left            =   6345
            TabIndex        =   293
            Top             =   2295
            Width           =   1230
         End
         Begin VB.Label lblPlantio 
            Caption         =   "Local Plantio �rvore"
            Height          =   255
            Left            =   9000
            TabIndex        =   286
            Top             =   960
            Width           =   2055
         End
         Begin VB.Label lblCanalVenda 
            AutoSize        =   -1  'True
            Caption         =   "Canal de Venda"
            Height          =   195
            Left            =   6330
            TabIndex        =   284
            Top             =   2295
            Width           =   1140
         End
         Begin VB.Label lbltexto 
            AutoSize        =   -1  'True
            Caption         =   $"ConPropAdesao.frx":1072
            Height          =   195
            Index           =   31
            Left            =   225
            TabIndex        =   249
            Top             =   1665
            Width           =   9090
         End
         Begin VB.Label lblPropMigracao 
            AutoSize        =   -1  'True
            Caption         =   "Proposta Anterior"
            Height          =   255
            Left            =   7320
            TabIndex        =   127
            Top             =   960
            Width           =   1215
         End
         Begin VB.Label lbltexto 
            Caption         =   "Situa��o                                                                                                                Qtd.Vidas"
            Height          =   255
            Index           =   24
            Left            =   240
            TabIndex        =   125
            Top             =   975
            Width           =   6885
         End
         Begin VB.Label lbltexto 
            Caption         =   $"ConPropAdesao.frx":1103
            Height          =   255
            Index           =   19
            Left            =   240
            TabIndex        =   124
            Top             =   270
            Width           =   10980
         End
      End
      Begin VB.Frame fraCorretor 
         Caption         =   "Corretor"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1485
         Left            =   -74820
         TabIndex        =   109
         Top             =   6975
         Width           =   11265
         Begin MSFlexGridLib.MSFlexGrid gridCorretor 
            Height          =   1200
            Left            =   180
            TabIndex        =   18
            Top             =   210
            Width           =   10965
            _ExtentX        =   19341
            _ExtentY        =   2117
            _Version        =   393216
            Rows            =   1
            Cols            =   4
            FixedCols       =   0
            ScrollBars      =   2
            AllowUserResizing=   1
            FormatString    =   $"ConPropAdesao.frx":11BE
         End
      End
      Begin VB.Frame fraEstipulante 
         Caption         =   "Estipulante"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1755
         Left            =   -74820
         TabIndex        =   123
         Top             =   8505
         Width           =   11265
         Begin MSFlexGridLib.MSFlexGrid gridEstipulante 
            Height          =   1425
            Left            =   120
            TabIndex        =   19
            Top             =   210
            Width           =   10995
            _ExtentX        =   19394
            _ExtentY        =   2514
            _Version        =   393216
            Rows            =   1
            Cols            =   5
            FixedCols       =   0
            FormatString    =   $"ConPropAdesao.frx":1262
         End
      End
      Begin MSFlexGridLib.MSFlexGrid gridSinistro 
         Height          =   7545
         Left            =   -74580
         TabIndex        =   77
         Top             =   1200
         Width           =   10725
         _ExtentX        =   18918
         _ExtentY        =   13309
         _Version        =   393216
         Cols            =   6
         FixedCols       =   0
         AllowBigSelection=   0   'False
         HighLight       =   0
         FormatString    =   $"ConPropAdesao.frx":1345
      End
      Begin MSFlexGridLib.MSFlexGrid GridProposta 
         Height          =   3465
         Index           =   0
         Left            =   -74730
         TabIndex        =   78
         Top             =   1200
         Width           =   11025
         _ExtentX        =   19447
         _ExtentY        =   6112
         _Version        =   393216
         Cols            =   7
         FixedCols       =   0
         SelectionMode   =   1
         AllowUserResizing=   3
      End
      Begin MSFlexGridLib.MSFlexGrid gridCoberturasRE 
         Height          =   6765
         Left            =   -74700
         TabIndex        =   245
         Top             =   1230
         Width           =   11055
         _ExtentX        =   19500
         _ExtentY        =   11933
         _Version        =   393216
         Cols            =   8
         FixedCols       =   2
         AllowBigSelection=   0   'False
         FormatString    =   $"ConPropAdesao.frx":13DC
      End
      Begin VB.Frame fraEndRiscoPenhorRural 
         Caption         =   "Endere�o de Risco:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1155
         Left            =   -74670
         TabIndex        =   230
         Top             =   6210
         Width           =   10995
         Begin VB.TextBox txtTelRiscoPenhorRural 
            Height          =   285
            Left            =   7680
            Locked          =   -1  'True
            TabIndex        =   237
            TabStop         =   0   'False
            Top             =   810
            Width           =   1215
         End
         Begin VB.TextBox txtDDDRiscoPenhorRural 
            Height          =   285
            Left            =   5625
            Locked          =   -1  'True
            TabIndex        =   236
            TabStop         =   0   'False
            Top             =   810
            Width           =   615
         End
         Begin VB.TextBox txtCEPRiscoPenhorRural 
            Height          =   285
            Left            =   3300
            Locked          =   -1  'True
            TabIndex        =   235
            TabStop         =   0   'False
            Top             =   810
            Width           =   1125
         End
         Begin VB.TextBox txtUFRiscoPenhorRural 
            Height          =   285
            Left            =   1860
            Locked          =   -1  'True
            TabIndex        =   234
            TabStop         =   0   'False
            Top             =   810
            Width           =   375
         End
         Begin VB.TextBox txtMunRiscoPenhorRural 
            Height          =   285
            Left            =   1860
            Locked          =   -1  'True
            TabIndex        =   233
            TabStop         =   0   'False
            Top             =   510
            Width           =   3885
         End
         Begin VB.TextBox txtEndRiscoPenhorRural 
            Height          =   315
            Left            =   1860
            Locked          =   -1  'True
            TabIndex        =   232
            TabStop         =   0   'False
            Top             =   180
            Width           =   7035
         End
         Begin VB.TextBox txtMunIBGEPenhorRural 
            Height          =   285
            Left            =   7680
            Locked          =   -1  'True
            TabIndex        =   231
            TabStop         =   0   'False
            Top             =   510
            Width           =   1215
         End
         Begin VB.Label Label79 
            Alignment       =   1  'Right Justify
            Caption         =   "Municipio IBGE:"
            Height          =   195
            Index           =   2
            Left            =   6495
            TabIndex        =   244
            Top             =   555
            Width           =   1155
         End
         Begin VB.Label Label79 
            Alignment       =   1  'Right Justify
            Caption         =   "Telefone:"
            Height          =   195
            Index           =   6
            Left            =   6900
            TabIndex        =   243
            Top             =   855
            Width           =   735
         End
         Begin VB.Label Label79 
            AutoSize        =   -1  'True
            Caption         =   "DDD:"
            Height          =   195
            Index           =   5
            Left            =   5175
            TabIndex        =   242
            Top             =   855
            Width           =   405
         End
         Begin VB.Label Label79 
            Caption         =   "CEP:"
            Height          =   255
            Index           =   4
            Left            =   2880
            TabIndex        =   241
            Top             =   885
            Width           =   405
         End
         Begin VB.Label Label79 
            Caption         =   "UF:"
            Height          =   255
            Index           =   3
            Left            =   1560
            TabIndex        =   240
            Top             =   825
            Width           =   255
         End
         Begin VB.Label Label79 
            AutoSize        =   -1  'True
            Caption         =   "Municipio:"
            Height          =   195
            Index           =   1
            Left            =   1095
            TabIndex        =   239
            Top             =   555
            Width           =   720
         End
         Begin VB.Label Label79 
            AutoSize        =   -1  'True
            Caption         =   "Endere�o:"
            Height          =   195
            Index           =   0
            Left            =   1080
            TabIndex        =   238
            Top             =   210
            Width           =   735
         End
      End
      Begin VB.Frame fraPenhorRural 
         Caption         =   "Dados Espec�ficos - Penhor Rural"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   4860
         Left            =   -74670
         TabIndex        =   228
         Top             =   1320
         Width           =   10995
         Begin MSFlexGridLib.MSFlexGrid grdPenhorRural 
            Height          =   4485
            Left            =   120
            TabIndex        =   229
            Top             =   240
            Width           =   10770
            _ExtentX        =   18997
            _ExtentY        =   7911
            _Version        =   393216
            Cols            =   16
            FormatString    =   $"ConPropAdesao.frx":146C
         End
      End
      Begin VB.Frame fraSeguroConsorcio 
         Caption         =   "Dados Espec�ficos - Seguro Cons�rcio"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   3615
         Left            =   -74670
         TabIndex        =   257
         Top             =   1320
         Visible         =   0   'False
         Width           =   10125
         Begin VB.TextBox txtConsorcio 
            Enabled         =   0   'False
            Height          =   285
            Index           =   10
            Left            =   4440
            TabIndex        =   268
            Top             =   1080
            Width           =   1335
         End
         Begin VB.TextBox txtConsorcio 
            Enabled         =   0   'False
            Height          =   285
            Index           =   9
            Left            =   3000
            TabIndex        =   267
            Top             =   1080
            Width           =   1335
         End
         Begin VB.TextBox txtConsorcio 
            Enabled         =   0   'False
            Height          =   285
            Index           =   8
            Left            =   1560
            TabIndex        =   266
            Top             =   1080
            Width           =   1335
         End
         Begin VB.TextBox txtConsorcio 
            Enabled         =   0   'False
            Height          =   285
            Index           =   7
            Left            =   120
            TabIndex        =   265
            Top             =   1080
            Width           =   1335
         End
         Begin VB.TextBox txtConsorcio 
            Enabled         =   0   'False
            Height          =   285
            Index           =   6
            Left            =   6240
            TabIndex        =   264
            Top             =   480
            Width           =   2895
         End
         Begin VB.TextBox txtConsorcio 
            Enabled         =   0   'False
            Height          =   285
            Index           =   5
            Left            =   5640
            TabIndex        =   263
            Top             =   480
            Width           =   615
         End
         Begin VB.TextBox txtConsorcio 
            Enabled         =   0   'False
            Height          =   285
            Index           =   4
            Left            =   4800
            TabIndex        =   262
            Top             =   480
            Width           =   735
         End
         Begin VB.TextBox txtConsorcio 
            Enabled         =   0   'False
            Height          =   285
            Index           =   3
            Left            =   3960
            TabIndex        =   261
            Top             =   480
            Width           =   735
         End
         Begin VB.TextBox txtConsorcio 
            Enabled         =   0   'False
            Height          =   285
            Index           =   2
            Left            =   3000
            TabIndex        =   260
            Top             =   480
            Width           =   855
         End
         Begin VB.TextBox txtConsorcio 
            Enabled         =   0   'False
            Height          =   285
            Index           =   1
            Left            =   600
            Locked          =   -1  'True
            TabIndex        =   259
            Top             =   480
            Width           =   2295
         End
         Begin VB.TextBox txtConsorcio 
            Enabled         =   0   'False
            Height          =   285
            Index           =   0
            Left            =   120
            TabIndex        =   258
            Top             =   480
            Width           =   495
         End
         Begin MSFlexGridLib.MSFlexGrid grdConsorcio 
            Height          =   1935
            Left            =   120
            TabIndex        =   269
            Top             =   1560
            Width           =   9015
            _ExtentX        =   15901
            _ExtentY        =   3413
            _Version        =   393216
         End
         Begin VB.Label lblConsorcio 
            Caption         =   "In�cio"
            Height          =   255
            Index           =   14
            Left            =   120
            TabIndex        =   278
            Top             =   840
            Width           =   1455
         End
         Begin VB.Label lblConsorcio 
            Caption         =   "Fim"
            Height          =   255
            Index           =   16
            Left            =   1560
            TabIndex        =   277
            Top             =   840
            Width           =   1335
         End
         Begin VB.Label lblConsorcio 
            Caption         =   "Ades�o"
            Height          =   255
            Index           =   5
            Left            =   3000
            TabIndex        =   276
            Top             =   840
            Width           =   975
         End
         Begin VB.Label lblConsorcio 
            Caption         =   "1� Assembl�ia"
            Height          =   255
            Index           =   6
            Left            =   4440
            TabIndex        =   275
            Top             =   840
            Width           =   1335
         End
         Begin VB.Label lblConsorcio 
            Caption         =   "Plano"
            Height          =   255
            Index           =   3
            Left            =   4800
            TabIndex        =   274
            Top             =   240
            Width           =   735
         End
         Begin VB.Label lblConsorcio 
            Caption         =   "Grupo"
            Height          =   255
            Index           =   1
            Left            =   3000
            TabIndex        =   273
            Top             =   240
            Width           =   855
         End
         Begin VB.Label lblConsorcio 
            Caption         =   "Bem"
            Height          =   255
            Index           =   4
            Left            =   5640
            TabIndex        =   272
            Top             =   240
            Width           =   1695
         End
         Begin VB.Label lblConsorcio 
            Caption         =   "Cota"
            Height          =   255
            Index           =   2
            Left            =   3960
            TabIndex        =   271
            Top             =   240
            Width           =   735
         End
         Begin VB.Label lblConsorcio 
            Caption         =   "Segmento"
            Height          =   255
            Index           =   0
            Left            =   120
            TabIndex        =   270
            Top             =   240
            Width           =   2655
         End
      End
      Begin VB.Frame fraVidasSeguradas 
         Caption         =   "Vidas Seguradas "
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   2205
         Left            =   -74640
         TabIndex        =   252
         Top             =   4860
         Visible         =   0   'False
         Width           =   10905
         Begin VB.ComboBox cboSubgrupo 
            Height          =   315
            Index           =   2
            Left            =   150
            Style           =   2  'Dropdown List
            TabIndex        =   73
            Top             =   270
            Width           =   10515
         End
         Begin MSFlexGridLib.MSFlexGrid gridVidasSeguradas 
            Height          =   2835
            Left            =   120
            TabIndex        =   74
            Top             =   -600
            Width           =   10515
            _ExtentX        =   18547
            _ExtentY        =   5001
            _Version        =   393216
            Rows            =   1
            Cols            =   7
            FixedCols       =   0
            AllowBigSelection=   0   'False
            AllowUserResizing=   1
            FormatString    =   $"ConPropAdesao.frx":15B2
         End
      End
      Begin VB.Frame fraDadosCultivo 
         Caption         =   "Dados Espec�ficos - Ouro Agr�cola"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   2700
         Left            =   -74670
         TabIndex        =   216
         Top             =   1320
         Width           =   10995
         Begin VB.TextBox TxtDescSistemaCultivo 
            Height          =   285
            Left            =   3420
            Locked          =   -1  'True
            TabIndex        =   91
            Top             =   855
            Width           =   6165
         End
         Begin VB.TextBox TxtSubTpCulturaId 
            Height          =   285
            Left            =   1920
            Locked          =   -1  'True
            TabIndex        =   88
            Top             =   555
            Width           =   1485
         End
         Begin VB.TextBox TxtTpCulturaId 
            Height          =   285
            Left            =   1920
            Locked          =   -1  'True
            TabIndex        =   86
            Top             =   255
            Width           =   1485
         End
         Begin VB.TextBox TxtSistemaProducao 
            Height          =   285
            Left            =   1920
            Locked          =   -1  'True
            TabIndex        =   96
            Top             =   1755
            Width           =   1485
         End
         Begin VB.TextBox TxtDtPrevPlantio 
            Height          =   285
            Left            =   1920
            Locked          =   -1  'True
            TabIndex        =   103
            Top             =   2355
            Width           =   1485
         End
         Begin VB.CheckBox ChkIndApta 
            Alignment       =   1  'Right Justify
            Caption         =   "Indicador Apta:"
            Height          =   315
            Left            =   3750
            TabIndex        =   97
            Top             =   1725
            Width           =   1515
         End
         Begin VB.CheckBox ChkIndLavouraPlant 
            Alignment       =   1  'Right Justify
            Caption         =   "Lavoura J� Plantada:"
            Height          =   345
            Left            =   6675
            TabIndex        =   99
            Top             =   1710
            Width           =   1875
         End
         Begin VB.TextBox TxtDtInicioPlantio 
            Height          =   285
            Left            =   5070
            Locked          =   -1  'True
            TabIndex        =   104
            Top             =   2355
            Width           =   1485
         End
         Begin VB.TextBox TxtDtFimPlantio 
            Height          =   285
            Left            =   8100
            Locked          =   -1  'True
            TabIndex        =   105
            Top             =   2355
            Width           =   1485
         End
         Begin VB.TextBox TxtSistemaCultivoId 
            Height          =   285
            Left            =   1920
            Locked          =   -1  'True
            TabIndex        =   90
            Top             =   855
            Width           =   1485
         End
         Begin VB.TextBox TxtDescSubTpCultura 
            Height          =   285
            Left            =   3420
            Locked          =   -1  'True
            TabIndex        =   89
            Top             =   555
            Width           =   6165
         End
         Begin VB.TextBox TxtDescTpCultura 
            Height          =   285
            Left            =   3420
            Locked          =   -1  'True
            TabIndex        =   87
            Top             =   255
            Width           =   6165
         End
         Begin VB.TextBox txtDescTpCiclo 
            Height          =   285
            Left            =   3420
            Locked          =   -1  'True
            TabIndex        =   93
            Top             =   1155
            Width           =   6165
         End
         Begin VB.TextBox txtTpCicloId 
            Height          =   285
            Left            =   1920
            Locked          =   -1  'True
            TabIndex        =   92
            Top             =   1155
            Width           =   1485
         End
         Begin VB.TextBox txtDescTpAgricultura 
            Height          =   285
            Left            =   3420
            Locked          =   -1  'True
            TabIndex        =   95
            Top             =   1455
            Width           =   6165
         End
         Begin VB.TextBox txtTpAgriculturaId 
            Height          =   285
            Left            =   1920
            Locked          =   -1  'True
            TabIndex        =   94
            Top             =   1455
            Width           =   1485
         End
         Begin VB.TextBox txtCNPJAvaliador 
            Height          =   285
            Left            =   1920
            Locked          =   -1  'True
            TabIndex        =   100
            Top             =   2055
            Width           =   1485
         End
         Begin VB.TextBox txtMatAvaliador 
            Height          =   285
            Left            =   5070
            Locked          =   -1  'True
            TabIndex        =   256
            Top             =   2040
            Width           =   1485
         End
         Begin VB.CheckBox chkIndLaudo 
            Alignment       =   1  'Right Justify
            Caption         =   "Indicador Laudo:"
            Height          =   225
            Left            =   6675
            TabIndex        =   102
            Top             =   2090
            Width           =   1875
         End
         Begin VB.Label lbltexto 
            AutoSize        =   -1  'True
            Caption         =   "Tipo de Cultura:"
            Height          =   195
            Index           =   15
            Left            =   615
            TabIndex        =   227
            Top             =   285
            Width           =   1125
         End
         Begin VB.Label lbltexto 
            AutoSize        =   -1  'True
            Caption         =   "Sistema de Produ��o:"
            Height          =   195
            Index           =   61
            Left            =   180
            TabIndex        =   226
            Top             =   1785
            Width           =   1560
         End
         Begin VB.Label lbltexto 
            AutoSize        =   -1  'True
            Caption         =   "Dt. Prevista do Plantio:"
            Height          =   195
            Index           =   63
            Left            =   120
            TabIndex        =   225
            Top             =   2400
            Width           =   1620
         End
         Begin VB.Label lbltexto 
            AutoSize        =   -1  'True
            Caption         =   "Dt. Inicio do Plantio:"
            Height          =   195
            Index           =   66
            Left            =   3510
            TabIndex        =   224
            Top             =   2400
            Width           =   1425
         End
         Begin VB.Label lbltexto 
            AutoSize        =   -1  'True
            Caption         =   "Dt Fim do Plantio:"
            Height          =   195
            Index           =   65
            Left            =   6690
            TabIndex        =   223
            Top             =   2400
            Width           =   1245
         End
         Begin VB.Label lbltexto 
            AutoSize        =   -1  'True
            Caption         =   "Sistema de Cultivo:"
            Height          =   195
            Index           =   17
            Left            =   390
            TabIndex        =   222
            Top             =   885
            Width           =   1350
         End
         Begin VB.Label lbltexto 
            AutoSize        =   -1  'True
            Caption         =   "Sub Tipo de Cultura:"
            Height          =   195
            Index           =   16
            Left            =   300
            TabIndex        =   221
            Top             =   600
            Width           =   1455
         End
         Begin VB.Label Label4 
            AutoSize        =   -1  'True
            Caption         =   "Tipo de Ciclo:"
            Height          =   195
            Index           =   0
            Left            =   765
            TabIndex        =   220
            Top             =   1185
            Width           =   975
         End
         Begin VB.Label lbltexto 
            AutoSize        =   -1  'True
            Caption         =   "Tipo de Agricultura:"
            Height          =   195
            Index           =   18
            Left            =   360
            TabIndex        =   219
            Top             =   1485
            Width           =   1380
         End
         Begin VB.Label lbltexto 
            AutoSize        =   -1  'True
            Caption         =   "CNPJ Avaliador:"
            Height          =   195
            Index           =   62
            Left            =   585
            TabIndex        =   218
            Top             =   2085
            Width           =   1155
         End
         Begin VB.Label lbltexto 
            AutoSize        =   -1  'True
            Caption         =   "Matr�cula Avaliador:"
            Height          =   195
            Index           =   64
            Left            =   3510
            TabIndex        =   217
            Top             =   2085
            Width           =   1425
         End
      End
      Begin VB.Frame fraDadosComplementares 
         Caption         =   "Dados Complementares"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   2145
         Left            =   -74670
         TabIndex        =   164
         Top             =   4050
         Width           =   10995
         Begin VB.TextBox TxtProducaoEsperada 
            Height          =   285
            Left            =   1920
            Locked          =   -1  'True
            TabIndex        =   180
            TabStop         =   0   'False
            Top             =   270
            Width           =   1095
         End
         Begin VB.TextBox TxtProducaoSegurada 
            Height          =   285
            Left            =   1920
            Locked          =   -1  'True
            TabIndex        =   179
            TabStop         =   0   'False
            Top             =   570
            Width           =   1095
         End
         Begin VB.TextBox TxtNrInspPrevia 
            Height          =   285
            Left            =   1920
            Locked          =   -1  'True
            TabIndex        =   178
            TabStop         =   0   'False
            Top             =   870
            Width           =   1095
         End
         Begin VB.TextBox TxtDtInspPrevia 
            Height          =   285
            Left            =   1920
            Locked          =   -1  'True
            TabIndex        =   177
            TabStop         =   0   'False
            Top             =   1170
            Width           =   1095
         End
         Begin VB.TextBox TxtTxNet 
            Height          =   285
            Left            =   1920
            Locked          =   -1  'True
            TabIndex        =   176
            TabStop         =   0   'False
            Top             =   1470
            Width           =   1095
         End
         Begin VB.TextBox TxtTxCarregada 
            Height          =   285
            Left            =   1920
            Locked          =   -1  'True
            TabIndex        =   175
            TabStop         =   0   'False
            Top             =   1770
            Width           =   1095
         End
         Begin VB.TextBox TxtPercBonus 
            Height          =   285
            Left            =   5070
            Locked          =   -1  'True
            TabIndex        =   174
            TabStop         =   0   'False
            Top             =   870
            Width           =   1095
         End
         Begin VB.TextBox TxtNumContrato 
            Height          =   285
            Left            =   5070
            Locked          =   -1  'True
            TabIndex        =   173
            TabStop         =   0   'False
            Top             =   270
            Width           =   3885
         End
         Begin VB.TextBox TxtAgenciaFinanciadora 
            Height          =   285
            Left            =   5070
            Locked          =   -1  'True
            TabIndex        =   172
            TabStop         =   0   'False
            Top             =   570
            Width           =   1905
         End
         Begin VB.TextBox TxtAgenciaFinanciadoraDv 
            Height          =   285
            Left            =   7650
            Locked          =   -1  'True
            TabIndex        =   171
            TabStop         =   0   'False
            Top             =   570
            Width           =   1305
         End
         Begin VB.TextBox TxtValorFinanciado 
            Height          =   285
            Left            =   7650
            Locked          =   -1  'True
            TabIndex        =   170
            TabStop         =   0   'False
            Top             =   870
            Width           =   1305
         End
         Begin VB.TextBox TxtDtFimFinanciamento 
            Height          =   285
            Left            =   7890
            Locked          =   -1  'True
            TabIndex        =   169
            TabStop         =   0   'False
            Top             =   1170
            Width           =   1065
         End
         Begin VB.TextBox TxtMotivoEndosso 
            Height          =   285
            Left            =   5070
            Locked          =   -1  'True
            TabIndex        =   168
            TabStop         =   0   'False
            Top             =   1470
            Width           =   3885
         End
         Begin VB.TextBox TxtRiscoTecnico 
            Height          =   285
            Left            =   5070
            Locked          =   -1  'True
            TabIndex        =   167
            TabStop         =   0   'False
            Top             =   1770
            Width           =   1095
         End
         Begin VB.TextBox TxtRiscoCLiente 
            Height          =   285
            Left            =   7830
            Locked          =   -1  'True
            TabIndex        =   166
            TabStop         =   0   'False
            Top             =   1770
            Width           =   1125
         End
         Begin VB.TextBox TxtDtInicioFinanciamento 
            Height          =   285
            Left            =   5070
            Locked          =   -1  'True
            TabIndex        =   165
            TabStop         =   0   'False
            Top             =   1170
            Width           =   1095
         End
         Begin VB.Label lbltexto 
            AutoSize        =   -1  'True
            Caption         =   "Produ��o Esperada:"
            Height          =   195
            Index           =   67
            Left            =   315
            TabIndex        =   196
            Top             =   300
            Width           =   1455
         End
         Begin VB.Label lbltexto 
            AutoSize        =   -1  'True
            Caption         =   "Produ��o Segurada:"
            Height          =   195
            Index           =   70
            Left            =   300
            TabIndex        =   195
            Top             =   600
            Width           =   1470
         End
         Begin VB.Label Label39 
            Caption         =   "Nr. Insp. Pr�via:"
            Height          =   195
            Left            =   615
            TabIndex        =   194
            Top             =   900
            Width           =   1155
         End
         Begin VB.Label Label3 
            Caption         =   "Dt. Insp. Pr�via:"
            Height          =   195
            Left            =   615
            TabIndex        =   193
            Top             =   1200
            Width           =   1155
         End
         Begin VB.Label Label33 
            AutoSize        =   -1  'True
            Caption         =   "Taxa Net.:"
            Height          =   195
            Left            =   1020
            TabIndex        =   192
            Top             =   1500
            Width           =   750
         End
         Begin VB.Label Label34 
            AutoSize        =   -1  'True
            Caption         =   "Taxa Carregada:"
            Height          =   195
            Left            =   585
            TabIndex        =   191
            Top             =   1800
            Width           =   1185
         End
         Begin VB.Label Label35 
            AutoSize        =   -1  'True
            Caption         =   "Perc Bonus:"
            Height          =   195
            Left            =   4050
            TabIndex        =   190
            Top             =   900
            Width           =   870
         End
         Begin VB.Label lbltexto 
            AutoSize        =   -1  'True
            Caption         =   "Nr.Contrato:"
            Height          =   195
            Index           =   68
            Left            =   4065
            TabIndex        =   189
            Top             =   300
            Width           =   855
         End
         Begin VB.Label lbltexto 
            AutoSize        =   -1  'True
            Caption         =   "Agencia Financiadora:"
            Height          =   195
            Index           =   69
            Left            =   3330
            TabIndex        =   188
            Top             =   600
            Width           =   1590
         End
         Begin VB.Label Label51 
            AutoSize        =   -1  'True
            Caption         =   "Dv:"
            Height          =   195
            Left            =   7320
            TabIndex        =   187
            Top             =   600
            Width           =   255
         End
         Begin VB.Label Label52 
            AutoSize        =   -1  'True
            Caption         =   "Valor Financiado:"
            Height          =   195
            Left            =   6360
            TabIndex        =   186
            Top             =   900
            Width           =   1230
         End
         Begin VB.Label Label53 
            AutoSize        =   -1  'True
            Caption         =   "Inicio do Financiamento:"
            Height          =   195
            Left            =   3195
            TabIndex        =   185
            Top             =   1200
            Width           =   1725
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "Motivo Endosso:"
            Height          =   195
            Index           =   0
            Left            =   3735
            TabIndex        =   184
            Top             =   1500
            Width           =   1185
         End
         Begin VB.Label Label55 
            AutoSize        =   -1  'True
            Caption         =   "Risco T�cnico:"
            Height          =   195
            Left            =   3840
            TabIndex        =   183
            Top             =   1800
            Width           =   1080
         End
         Begin VB.Label Label56 
            AutoSize        =   -1  'True
            Caption         =   "Risco Cliente:"
            Height          =   195
            Left            =   6750
            TabIndex        =   182
            Top             =   1800
            Width           =   975
         End
         Begin VB.Label Label57 
            Caption         =   "Fim do Financiamento:"
            Height          =   255
            Left            =   6240
            TabIndex        =   181
            Top             =   1200
            Width           =   1635
         End
      End
      Begin VB.Frame fraDeclConjuge 
         Caption         =   "Declara��o C�njuge"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   2955
         Left            =   -74640
         TabIndex        =   117
         Top             =   4140
         Width           =   10905
         Begin VB.TextBox txtCboConj 
            Height          =   285
            Left            =   2190
            Locked          =   -1  'True
            TabIndex        =   119
            Top             =   180
            Width           =   615
         End
         Begin VB.TextBox txtDescProfConj 
            Height          =   285
            Left            =   2880
            Locked          =   -1  'True
            TabIndex        =   118
            Top             =   180
            Width           =   7755
         End
         Begin MSComctlLib.ListView lvwDeclaracaoConjuge 
            Height          =   2415
            Left            =   240
            TabIndex        =   128
            Top             =   480
            Width           =   10395
            _ExtentX        =   18336
            _ExtentY        =   4260
            View            =   3
            LabelWrap       =   -1  'True
            HideSelection   =   -1  'True
            Checkboxes      =   -1  'True
            GridLines       =   -1  'True
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   1
            NumItems        =   4
            BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Text            =   "Item"
               Object.Width           =   1058
            EndProperty
            BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   1
               Text            =   "Pergunta"
               Object.Width           =   2540
            EndProperty
            BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   2
               Text            =   "Resposta"
               Object.Width           =   2540
            EndProperty
            BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   3
               Text            =   "Descri��o"
               Object.Width           =   5292
            EndProperty
         End
         Begin VB.Label Label38 
            Caption         =   "Profiss�o"
            Height          =   285
            Left            =   270
            TabIndex        =   120
            Top             =   240
            Width           =   1815
         End
      End
      Begin VB.Frame fraEndRiscoAgricola 
         Caption         =   "Endere�o de Risco:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1515
         Left            =   -74670
         TabIndex        =   197
         Top             =   6210
         Width           =   10995
         Begin VB.TextBox TxtTelefoneRisco 
            Height          =   285
            Left            =   7770
            Locked          =   -1  'True
            TabIndex        =   206
            Top             =   1150
            Width           =   1215
         End
         Begin VB.TextBox TxtDDDRisco 
            Height          =   285
            Left            =   6150
            Locked          =   -1  'True
            TabIndex        =   205
            Top             =   1150
            Width           =   615
         End
         Begin VB.TextBox TxtCEPRisco 
            Height          =   285
            Left            =   3390
            Locked          =   -1  'True
            TabIndex        =   204
            Top             =   1150
            Width           =   1125
         End
         Begin VB.TextBox TxtUFRisco 
            Height          =   285
            Left            =   1950
            Locked          =   -1  'True
            TabIndex        =   203
            Top             =   1150
            Width           =   375
         End
         Begin VB.TextBox TxtMunicipioRisco 
            Height          =   285
            Left            =   1950
            Locked          =   -1  'True
            TabIndex        =   202
            Top             =   850
            Width           =   3885
         End
         Begin VB.TextBox TxtEnderecoRisco 
            Height          =   285
            Left            =   1950
            Locked          =   -1  'True
            TabIndex        =   201
            Top             =   550
            Width           =   7035
         End
         Begin VB.TextBox TxtPropriedade 
            Height          =   285
            Left            =   1950
            Locked          =   -1  'True
            TabIndex        =   200
            Top             =   250
            Width           =   4845
         End
         Begin VB.TextBox TxtArea 
            Height          =   285
            Left            =   7770
            Locked          =   -1  'True
            TabIndex        =   199
            Top             =   250
            Width           =   1215
         End
         Begin VB.TextBox TxtMunicipioIBGE 
            Height          =   285
            Left            =   7770
            Locked          =   -1  'True
            TabIndex        =   198
            Top             =   850
            Width           =   1215
         End
         Begin VB.Label Label58 
            Caption         =   "Telefone:"
            Height          =   255
            Index           =   8
            Left            =   6990
            TabIndex        =   215
            Top             =   1180
            Width           =   735
         End
         Begin VB.Label Label58 
            AutoSize        =   -1  'True
            Caption         =   "DDD:"
            Height          =   195
            Index           =   7
            Left            =   5670
            TabIndex        =   214
            Top             =   1180
            Width           =   405
         End
         Begin VB.Label Label58 
            Caption         =   "CEP:"
            Height          =   255
            Index           =   6
            Left            =   2910
            TabIndex        =   213
            Top             =   1180
            Width           =   405
         End
         Begin VB.Label Label58 
            Caption         =   "UF:"
            Height          =   255
            Index           =   5
            Left            =   1620
            TabIndex        =   212
            Top             =   1180
            Width           =   255
         End
         Begin VB.Label Label58 
            AutoSize        =   -1  'True
            Caption         =   "Municipio:"
            Height          =   195
            Index           =   3
            Left            =   1155
            TabIndex        =   211
            Top             =   880
            Width           =   720
         End
         Begin VB.Label Label58 
            AutoSize        =   -1  'True
            Caption         =   "Endere�o:"
            Height          =   195
            Index           =   2
            Left            =   1140
            TabIndex        =   210
            Top             =   580
            Width           =   735
         End
         Begin VB.Label Label58 
            AutoSize        =   -1  'True
            Caption         =   "Propriedade:"
            Height          =   195
            Index           =   0
            Left            =   975
            TabIndex        =   209
            Top             =   280
            Width           =   900
         End
         Begin VB.Label Label58 
            AutoSize        =   -1  'True
            Caption         =   "�rea(Ha):"
            Height          =   195
            Index           =   1
            Left            =   6930
            TabIndex        =   208
            Top             =   280
            Width           =   675
         End
         Begin VB.Label Label58 
            Caption         =   "Municipio IBGE:"
            Height          =   195
            Index           =   4
            Left            =   6540
            TabIndex        =   207
            Top             =   880
            Width           =   1155
         End
      End
      Begin MSFlexGridLib.MSFlexGrid grdHistorico 
         Height          =   4305
         Left            =   -74760
         TabIndex        =   294
         Top             =   1200
         Width           =   11115
         _ExtentX        =   19606
         _ExtentY        =   7594
         _Version        =   393216
         Cols            =   5
         FixedCols       =   0
         FocusRect       =   0
         HighLight       =   0
         SelectionMode   =   1
         FormatString    =   $"ConPropAdesao.frx":1651
      End
      Begin MSFlexGridLib.MSFlexGrid grdEndosso 
         Height          =   7785
         Left            =   345
         TabIndex        =   307
         Top             =   1440
         Width           =   10935
         _ExtentX        =   19288
         _ExtentY        =   13732
         _Version        =   393216
         Cols            =   10
         FixedCols       =   0
         AllowUserResizing=   1
         FormatString    =   $"ConPropAdesao.frx":1736
      End
      Begin MSFlexGridLib.MSFlexGrid gridCobrancaCosseguro 
         Height          =   6435
         Left            =   -74850
         TabIndex        =   308
         Top             =   1935
         Width           =   11325
         _ExtentX        =   19976
         _ExtentY        =   11351
         _Version        =   393216
         Rows            =   1
         Cols            =   13
         FixedCols       =   0
         AllowBigSelection=   0   'False
         HighLight       =   0
         AllowUserResizing=   1
         FormatString    =   $"ConPropAdesao.frx":1851
      End
      Begin MSFlexGridLib.MSFlexGrid grdAvaliacao 
         Height          =   4335
         Left            =   -74520
         TabIndex        =   370
         Top             =   1200
         Width           =   10695
         _ExtentX        =   18865
         _ExtentY        =   7646
         _Version        =   393216
         Cols            =   16
         FixedCols       =   0
         SelectionMode   =   1
      End
      Begin VB.Label Label72 
         AutoSize        =   -1  'True
         Caption         =   "Perc. Despesa L�der"
         Height          =   195
         Index           =   2
         Left            =   -66945
         TabIndex        =   310
         Top             =   1245
         Width           =   1470
      End
      Begin VB.Label Label72 
         AutoSize        =   -1  'True
         Caption         =   "Perc. Participa��o"
         Height          =   195
         Index           =   1
         Left            =   -68460
         TabIndex        =   309
         Top             =   1245
         Width           =   1305
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "Descri��o:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   -74760
         TabIndex        =   306
         Top             =   5625
         Width           =   930
      End
      Begin VB.Label Label72 
         AutoSize        =   -1  'True
         Caption         =   "Dados referentes a:                                                                             N�mero de Ordem:"
         Height          =   195
         Index           =   0
         Left            =   -74835
         TabIndex        =   304
         Top             =   1260
         Width           =   6195
      End
   End
   Begin VB.CommandButton cmdImprimir 
      Caption         =   "Im&primir"
      Height          =   375
      Left            =   8430
      TabIndex        =   101
      Top             =   8100
      Visible         =   0   'False
      Width           =   1150
   End
End
Attribute VB_Name = "ConPropAdesao"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Demanda 19310624 - EXCLUIR O CAMPO PPE DO SEGP0176
' O objeto ChkPPE teve sua propriedade VISIBLE alterada para FALSE para atendimento da demanda.
' Optou-se por apenas inibir sua visualiza��o ao inv�s de exclui-lo definitivamente para o caso de futura reativa��o dessa funcionalidade.

Dim ConfiguracaoBrasil As Boolean
Dim IndiceIdSalarioMinimo As Integer
Dim TpPlanoIdQtdSalarios As Integer
Dim TpPlanoIdCapitalUnico As Integer

'Constantes
Const ProdIdOurovidaGarantia As Integer = 11
Const ProdIdOurovidaRuralRapido As Integer = 16
Const ProdIdOurovidaEmpresa As Integer = 15
Const ProdIdOuroAgricola As Integer = 145
Const ProdIdPenhorRural As Integer = 155
Const ProdIdOurovidaProdutorRural As Integer = 14
Const ProdIdOuroVidaAntigo As Integer = 12
Const ProdIdOuroVidaMigracao As Integer = 136
Const ProdIdPrestamistaBP As Integer = 712
Const ProdIdOuroVidaMulher As Integer = 716
Const ProdIdSeguroVidaAliancaIII As Integer = 714
Const ProdIdSeguroConsorcio As Integer = 718
'inicio alterado 30/08/22013 - MC_Amaral- confitec projeto 17860335 - Melhorias no SEGBR
'Retirada de hard_code de produtos da rotina CarregaDadosProposta
Const ProdIdOurovida As Integer = 121
Const ProdIdOurovida2000 As Integer = 135
Const ProdIdOurovidaestilo As Integer = 1196

'Alterado 30/07/2004 - Eduardo Cintra - Inclusao produto Seguro Vida Prestamista
Private Const ProdIdBBSeguroVidaAgriculturaFamiliar = 722

'bcarneiro - 25/06/2004 - BB Seguro Vida
Const ProdIdBBSeguroVida As Integer = 721

Const CodRegraCalculoIdade   As Integer = 1
Const CodRegraCalculoSalario As Integer = 2
'vitor.cabral - Nova Consultoria - 23/10/2013 - Demanda 17896885 - Inicio
Const TipoProduto As String = "BBC"
'vitor.cabral - Nova Consultoria - Demanda 17896885 - Fim

'Subgrupo
Dim colSubgrupos As Collection
Dim colCorretores As Collection
Dim colEstipulantes As Collection
'INICIO  alterado 02/09/2013 projeto 17860335 - Melhorias no SEGBR MC_Amaral -confitec
Dim sSubgrupos_flag As String 'flag criada para retorno proc SEGS7366_SPS
Dim iRamo As Integer 'criada retirada do innerjoin

'flowbr3658050
Public bProdutoBESC As Boolean

Public Renova_vida As Boolean

Private p_blNaoExisteErro As Boolean

'GGama - 07/10/2005 - Hist�rico das Propostas
Private sTodasDescricoes() As String

Private Type TabSelecionada
    blDadosProposta As Boolean
    blDadosFinanceiros As Boolean
    blDadosProponente As Boolean
    blDadosObjetoRisco As Boolean
    blDadosEndosso As Boolean
    blDadosMutuarios As Boolean
    blDadosBeneficiarios As Boolean
    blDadosSinistro As Boolean
    blDadosExtratoSeguro As Boolean
    blDadosObjetoRiscoRE As Boolean
    blDadosCoberturas As Boolean
    blDadosHistorico As Boolean
    blDadosCosseguro As Boolean
    'Sergey Souza - 25/08/2010
    blDadosConsultaDebito As Boolean
End Type

Private sTabSelecionada As TabSelecionada

'controles din�micos
Dim txtValAssistFacult As TextBox
Dim lbl As Label
Dim RegistraBoleto As String 'MARCIO.NOGUEIRA - CONFITEC SISTEMAS - 26/07/2017 PROJETO: 19368999 - cobran�a registrada AB e ABS
Dim BlnBoletoRegistrado As Boolean 'MARCIO.NOGUEIRA - CONFITEC SISTEMAS - 26/07/2017 PROJETO: 19368999 - cobran�a registrada AB e ABS
Dim txtPrazoFinanciamento As TextBox     'RMarins - Confitec - 14/08/2018 - MU2018 - 032171 - Criar campo Tempo de Financiamento - Produto 722
Dim lblPrazoFinanciamento As Label       'RMarins - Confitec - 14/08/2018 - MU2018 - 032171 - Criar campo Tempo de Financiamento - Produto 722
'Controles dinamicos para proposta oferta agrupada Projeto 00422823 (BB Seguro Residencial Simplificado)
Dim lblPropostaAgrupada As Label
Dim txtPropostaAgrupada As TextBox

Dim txtTp_Documento As TextBox
Dim lblTp_Documento As Label


Sub Buscar_Restiticao_BESC(ByRef rs_endosso As Recordset)

On Error GoTo TrataErro

    Dim sSql As String

    Set rs_endosso = Nothing

    sSql = ""
    sSql = sSql & "SELECT avaliacao_restituicao_tb.endosso_id," & vbNewLine
    sSql = sSql & "       tp_endosso_tb.descricao," & vbNewLine
    sSql = sSql & "       seg_remessa_arq_retorno_bb = 'PAGNET'," & vbNewLine
    sSql = sSql & "       dt_processamento = CASE WHEN ps_voucher_tb.dt_pagamento IS NOT NULL THEN ps_voucher_tb.dt_pagamento" & vbNewLine
    sSql = sSql & "                               ELSE ps_voucher_tb.dt_inclusao END," & vbNewLine
    sSql = sSql & "       val_financeiro = IsNull(ps_voucher_tb.val_pago,0) ," & vbNewLine ' INC000004655446 - Philip Rocha - 20150727 - Confitec - Inclus�o Tratamento Null - Proposta_id = 24180625
    sSql = sSql & "       situacao_pagamento_tb.descricao_situacao," & vbNewLine
    sSql = sSql & "    fl_BESC = 'S'" & vbNewLine
    sSql = sSql & "FROM avaliacao_restituicao_tb" & vbNewLine
    sSql = sSql & "JOIN endosso_tb" & vbNewLine
    sSql = sSql & "  ON endosso_tb.proposta_id = avaliacao_restituicao_tb.proposta_id" & vbNewLine
    sSql = sSql & " AND endosso_tb.endosso_id = avaliacao_restituicao_tb.endosso_id" & vbNewLine
    sSql = sSql & "JOIN tp_endosso_tb" & vbNewLine
    sSql = sSql & "  ON tp_endosso_tb.tp_endosso_id = endosso_tb.tp_endosso_id" & vbNewLine
    sSql = sSql & "JOIN ps_mov_endosso_financeiro_tb" & vbNewLine
    sSql = sSql & "  ON ps_mov_endosso_financeiro_tb.proposta_id = avaliacao_restituicao_tb.proposta_id" & vbNewLine
    sSql = sSql & " AND ps_mov_endosso_financeiro_tb.endosso_id = avaliacao_restituicao_tb.endosso_id" & vbNewLine
    sSql = sSql & "JOIN ps_mov_cliente_tb" & vbNewLine
    sSql = sSql & "  ON ps_mov_cliente_tb.movimentacao_id = ps_mov_endosso_financeiro_tb.movimentacao_id" & vbNewLine
    sSql = sSql & "JOIN ps_acerto_pagamento_tb" & vbNewLine
    sSql = sSql & "  ON ps_acerto_pagamento_tb.acerto_id = ps_mov_cliente_tb.acerto_id" & vbNewLine
    sSql = sSql & "JOIN ps_voucher_tb" & vbNewLine
    sSql = sSql & "  ON ps_voucher_tb.voucher_id = ps_acerto_pagamento_tb.voucher_id" & vbNewLine
    sSql = sSql & " AND ps_voucher_tb.cod_origem = 'RP'" & vbNewLine
    sSql = sSql & "JOIN ps_voucher_distribuicao_tb" & vbNewLine
    sSql = sSql & "  ON ps_voucher_distribuicao_tb.voucher_id = ps_voucher_tb.voucher_id" & vbNewLine
    sSql = sSql & "LEFT JOIN situacao_pagamento_tb" & vbNewLine
    sSql = sSql & "  ON situacao_pagamento_tb.situacao_pagamento_id = avaliacao_restituicao_tb.situacao_pagamento_id" & vbNewLine
    sSql = sSql & "WHERE produto_id IN(200,201,202,203,204,205,208,209,210,211,212,213,216,217,218,219,220,221,224,225)" & vbNewLine
    sSql = sSql & "  AND avaliacao_restituicao_tb.proposta_id = " & Val(txtProposta.Text)
    'GENJUNIOR - FLOW 17860335
    'UNION NAS TABELAS DE EXPURGO
    sSql = sSql & " UNION " & vbNewLine
    sSql = sSql & "SELECT avaliacao_restituicao_tb.endosso_id," & vbNewLine
    sSql = sSql & "       tp_endosso_tb.descricao," & vbNewLine
    sSql = sSql & "       seg_remessa_arq_retorno_bb = 'PAGNET'," & vbNewLine
    sSql = sSql & "       dt_processamento = CASE WHEN ps_voucher_tb.dt_pagamento IS NOT NULL THEN ps_voucher_tb.dt_pagamento" & vbNewLine
    sSql = sSql & "                               ELSE ps_voucher_tb.dt_inclusao END," & vbNewLine
    sSql = sSql & "       val_financeiro = ps_voucher_tb.val_pago," & vbNewLine
    sSql = sSql & "       situacao_pagamento_tb.descricao_situacao," & vbNewLine
    sSql = sSql & "    fl_BESC = 'S'" & vbNewLine
    sSql = sSql & "FROM avaliacao_restituicao_tb" & vbNewLine
    sSql = sSql & "JOIN endosso_tb" & vbNewLine
    sSql = sSql & "  ON endosso_tb.proposta_id = avaliacao_restituicao_tb.proposta_id" & vbNewLine
    sSql = sSql & " AND endosso_tb.endosso_id = avaliacao_restituicao_tb.endosso_id" & vbNewLine
    sSql = sSql & "JOIN tp_endosso_tb" & vbNewLine
    sSql = sSql & "  ON tp_endosso_tb.tp_endosso_id = endosso_tb.tp_endosso_id" & vbNewLine
    sSql = sSql & "JOIN seguros_hist_db.dbo.ps_mov_endosso_financeiro_hist_tb ps_mov_endosso_financeiro_hist_tb " & vbNewLine
    sSql = sSql & "  ON ps_mov_endosso_financeiro_hist_tb.proposta_id = avaliacao_restituicao_tb.proposta_id" & vbNewLine
    sSql = sSql & " AND ps_mov_endosso_financeiro_hist_tb.endosso_id = avaliacao_restituicao_tb.endosso_id" & vbNewLine
    sSql = sSql & "JOIN seguros_hist_db.dbo.ps_mov_cliente_hist_tb ps_mov_cliente_hist_tb " & vbNewLine
    sSql = sSql & "  ON ps_mov_cliente_hist_tb.movimentacao_id = ps_mov_endosso_financeiro_hist_tb.movimentacao_id" & vbNewLine
    sSql = sSql & "JOIN ps_acerto_pagamento_tb" & vbNewLine
    sSql = sSql & "  ON ps_acerto_pagamento_tb.acerto_id = ps_mov_cliente_hist_tb.acerto_id" & vbNewLine
    sSql = sSql & "JOIN ps_voucher_tb" & vbNewLine
    sSql = sSql & "  ON ps_voucher_tb.voucher_id = ps_acerto_pagamento_tb.voucher_id" & vbNewLine
    sSql = sSql & " AND ps_voucher_tb.cod_origem = 'RP'" & vbNewLine
    sSql = sSql & "JOIN ps_voucher_distribuicao_tb" & vbNewLine
    sSql = sSql & "  ON ps_voucher_distribuicao_tb.voucher_id = ps_voucher_tb.voucher_id" & vbNewLine
    sSql = sSql & "LEFT JOIN situacao_pagamento_tb" & vbNewLine
    sSql = sSql & "  ON situacao_pagamento_tb.situacao_pagamento_id = avaliacao_restituicao_tb.situacao_pagamento_id" & vbNewLine
    sSql = sSql & "WHERE produto_id IN(200,201,202,203,204,205,208,209,210,211,212,213,216,217,218,219,220,221,224,225)" & vbNewLine
    sSql = sSql & "  AND avaliacao_restituicao_tb.proposta_id = " & Val(txtProposta.Text)
    'FIM GENJUNIOR
    
    Set rs_endosso = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, sSql, True)

Exit Sub
TrataErro:
    Call TratarErro("Carrega_Restituicao", Me.name)
    Call FinalizarAplicacao

End Sub

Private Function CarregaAssistenciaFacultativa(ByVal lPropostaId As Long, lProduto As Long) As Boolean
  Dim rs As Recordset
  Dim sSql As String
  
  On Error GoTo jpErro
  
  'sSql = "SELECT ISNULL(val_assistencia_facul,0) AS val_assistencia " & vbCrLf _
  '     & "FROM proposta_adesao_tb " & vbCrLf _
   '    & "WHERE proposta_id=" & lPropostaId


sSql = "set nocount on exec seguros_db..SEGS12290_SPS " & lPropostaId & "," & lProduto & ",'N',1"

  Set rs = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, sSql, True)

  
  txtValAssistFacult.Text = "0,00"
  If Not rs Is Nothing Then
    If Not rs.EOF Then
    txtValAssistFacult.Visible = True
    
      txtValAssistFacult.Text = FormataValorParaPadraoBrasil(IIf(IsNull(rs!val_assistencia_carregado), 0, rs!val_assistencia_carregado))
                                                             
    End If
    rs.Close: Set rs = Nothing
  
  End If

  CarregaAssistenciaFacultativa = True
  
Exit Function

jpErro:
  On Error Resume Next
  rs.Close: Set rs = Nothing
  Err.Clear
  CarregaAssistenciaFacultativa = False
End Function

Public Sub Ler_Cosseguro(FormAtivo As Form, _
                         num_proposta As String, _
                         data As String)
    Dim rc As Recordset
    Dim Sql As String
    Set rc = New Recordset

    On Error GoTo Erro

    FormAtivo.cmbCongenere.Clear
    FormAtivo.cmbCongenere.AddItem "Nossa Parte"
    FormAtivo.cmbCongenere.AddItem "Total do Cosseguro Cedido"

'+----------------------------------------
'| Migra��o SQL - Melhoria de performance
'| petrauskas 08/2016
'|
    Sql = "SET NOCOUNT ON  exec SEGS13060_SPS " & num_proposta & ", '" & Format(data, "yyyymmdd") & "', '" & Format(Data_Sistema, "yyyymmdd") & "'"

'    SQL = ""
'    SQL = SQL & " select   c.nome," & vbNewLine
'    SQL = SQL & "          b.rep_seguradora_cod_susep," & vbNewLine
'    SQL = SQL & "          b.num_ordem," & vbNewLine
'    SQL = SQL & "          b.perc_participacao," & vbNewLine
'    SQL = SQL & "          b.perc_despesa_lider" & vbNewLine
'    SQL = SQL & " from     seguros_db..proposta_adesao_tb a  WITH (NOLOCK) " & vbNewLine
'    SQL = SQL & "          inner   join seguros_db..co_seguro_repassado_tb b  WITH (NOLOCK) " & vbNewLine
'    SQL = SQL & "                  on     b.apolice_id = a.apolice_id" & vbNewLine
'    SQL = SQL & "                  and    b.ramo_id = a.ramo_id" & vbNewLine
'    SQL = SQL & "                  and    b.sucursal_seguradora_id = a.sucursal_seguradora_id" & vbNewLine
'    SQL = SQL & "                  and    b.seguradora_cod_susep = a.seguradora_cod_susep" & vbNewLine
'    SQL = SQL & "          inner   join seguros_db..seguradora_tb c  WITH (NOLOCK) " & vbNewLine
'    SQL = SQL & "                  on     c.seguradora_cod_susep = b.rep_seguradora_cod_susep" & vbNewLine
'    SQL = SQL & " Where    a.proposta_id = " & num_proposta & vbNewLine
'    SQL = SQL & " AND      b.dt_inicio_participacao <= '" & Format(data, "yyyymmdd") & "' " & vbNewLine
'    SQL = SQL & " AND      (b.dt_fim_participacao IS NULL" & vbNewLine
'    SQL = SQL & " OR       b.dt_fim_participacao >= '" & Format(Data_Sistema, "yyyymmdd") & "') " & vbNewLine
'    SQL = SQL & " Union" & vbNewLine
'    SQL = SQL & " select   c.nome," & vbNewLine
'    SQL = SQL & "          b.rep_seguradora_cod_susep," & vbNewLine
'    SQL = SQL & "          b.num_ordem," & vbNewLine
'    SQL = SQL & "          b.perc_participacao," & vbNewLine
'    SQL = SQL & "          b.perc_despesa_lider" & vbNewLine
'    SQL = SQL & " from     seguros_db..apolice_tb a  WITH (NOLOCK) " & vbNewLine
'    SQL = SQL & "          inner   join seguros_db..co_seguro_repassado_tb b  WITH (NOLOCK) " & vbNewLine
'    SQL = SQL & "                  on     b.apolice_id = a.apolice_id" & vbNewLine
'    SQL = SQL & "                  and    b.ramo_id = a.ramo_id" & vbNewLine
'    SQL = SQL & "                  and    b.sucursal_seguradora_id = a.sucursal_seguradora_id" & vbNewLine
'    SQL = SQL & "                  and    b.seguradora_cod_susep = a.seguradora_cod_susep" & vbNewLine
'    SQL = SQL & "          inner   join seguros_db..seguradora_tb c  WITH (NOLOCK) " & vbNewLine
'    SQL = SQL & "                  on     c.seguradora_cod_susep = b.rep_seguradora_cod_susep" & vbNewLine
'    SQL = SQL & " Where    a.proposta_id = " & num_proposta & vbNewLine
'    SQL = SQL & " AND      b.dt_inicio_participacao <= '" & Format(data, "yyyymmdd") & "' " & vbNewLine
'    SQL = SQL & " AND      (b.dt_fim_participacao IS NULL" & vbNewLine
'    SQL = SQL & " OR       b.dt_fim_participacao >= '" & Format(Data_Sistema, "yyyymmdd") & "') "
'| Migra��o SQL - Melhoria de performance
'+----------------------------------------
    
    Set rc = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, Sql, True)
    
    If Not rc.EOF Then
        If FormAtivo.name = "ConPropBasicaTranspInt" Then

            FormAtivo.SSTab1.TabEnabled(10) = True
        Else

            FormAtivo.SSTab1.TabEnabled(8) = True
        End If

        While Not rc.EOF

            FormAtivo.cmbCongenere.AddItem "" & rc("nome")
            FormAtivo.cmbCongenere.ItemData(FormAtivo.cmbCongenere.NewIndex) = rc("rep_seguradora_cod_susep")
            rc.MoveNext
        Wend

    End If

    rc.Close

    Call Carrega_cmbCongenere(CLng(num_proposta), 0, True)

    Exit Sub
    Resume
Erro:
    TrataErroGeral "Ler Cosseguro"
    MsgBox "Rotina: Ler Cosseguro"
    End

End Sub
Sub Carrega_cmbCongenere(proposta_id As Long, _
                         seguradora_cod_susep As Integer, _
                         nossa_parte As Boolean)
    Dim Ramo As String
    Dim rs As Recordset
    Dim Sql As String
    Set rs = New Recordset
    
    Dim Perc_partic As Currency
    Dim Perc_desp_lider As Currency

    Sql = ""
    Sql = Sql & " Exec segs7067_sps "
    Sql = Sql & " @proposta_id = " & proposta_id & ","
    Sql = Sql & " @seguradora_cod_susep = " & seguradora_cod_susep & ","
    Sql = Sql & " @nossa_parte = '" & IIf(nossa_parte = True, "S", "N") & "'"
    Set rs = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, Sql, True)
    
    gridCobrancaCosseguro.Rows = 1
    While Not rs.EOF

        If nossa_parte = True Then
            Perc_partic = 100 - CCur("0" & Replace(rs("perc_participacao"), ".", ","))
            Perc_desp_lider = "0,00"
        Else
            Perc_partic = CCur("0" & Replace(rs("perc_participacao"), ".", ","))
            Perc_desp_lider = CCur("0" & Replace(rs("perc_despesa_lider"), ".", ","))
        End If
    
        gridCobrancaCosseguro.AddItem rs("num_endosso") & vbTab & rs("num_cobranca") & vbTab & rs("nosso_numero") & vbTab & Format(rs("dt_agendamento"), "dd/mm/yyyy") & vbTab & Format(Replace(rs("val_premio_tarifa"), ".", ","), "###,###,###,##0.00") & vbTab & Format(Replace(rs("val_desconto"), ".", ","), "###,###,###,##0.00") & vbTab & Format(Replace(rs("val_adic_fracionamento"), ".", ","), "###,###,###,##0.00") & vbTab & Format(Replace(rs("val_comissao"), ".", ","), "###,###,###,##0.00") & vbTab & Format(Replace(rs("val_premio_total_liquido"), ".", ","), "###,###,###,##0.00") & vbTab & Format(Replace(rs("val_despesa_lideranca"), ".", ","), "###,###,###,##0.00") & vbTab & Format(rs("dt_recebimento"), "dd/mm/yyyy") & vbTab & Format(Perc_partic, "###,###,###,##0.00") & vbTab & Format(Perc_desp_lider, "###,###,###,##0.00")
        rs.MoveNext
    Wend
    rs.Close
    
    Ramo = Trim(Left(txtRamo, InStr(txtRamo, "-") - 1))
    
    Sql = ""

    If CLng(seguradora_cod_susep) <> 0 Then
        Sql = Sql & "select num_ordem, perc_participacao, perc_despesa_lider" & vbNewLine
        Sql = Sql & "from   co_seguro_repassado_tb" & vbNewLine
        Sql = Sql & "where  apolice_id = " & CLng(txtApolice.Text) & vbNewLine
        Sql = Sql & "and    ramo_id = " & Ramo & vbNewLine
        Sql = Sql & "and    rep_seguradora_cod_susep = " & seguradora_cod_susep & vbNewLine
        Sql = Sql & "and    dt_fim_participacao is null" & vbNewLine
        Set rs = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, Sql, True)

        If Not rs.EOF Then
            txtNumOrdem.Text = rs("num_ordem")
            txtPercParticipacao.Text = Format(CCur(Replace(("0" & rs("perc_participacao")), ".", ",")), "###,###,##0.00")
            txtPercDespesa.Text = Format(CCur(Replace(("0" & rs("perc_despesa_lider")), ".", ",")), "###,###,##0.00")
        End If

        rs.Close
    Else
        txtNumOrdem.Text = ""
        
        'verificar se foi selecionado nossa parte
'+----------------------------------------
'| Migra��o SQL - Melhoria de performance
'| petrauskas 08/2016
'|
        Sql = "SET NOCOUNT ON  exec SEGS13062_SPS " & CLng(txtApolice.Text) & ", " & Ramo
'        SQL = SQL & "select perc_participacao = sum(perc_participacao), perc_despesa_lider = sum(perc_despesa_lider)" & vbNewLine
'        SQL = SQL & "from   co_seguro_repassado_tb" & vbNewLine
'        SQL = SQL & "where  apolice_id = " & CLng(txtApolice.Text) & vbNewLine
'        SQL = SQL & "and    ramo_id = " & Ramo & vbNewLine
'        SQL = SQL & "and    dt_fim_participacao is null" & vbNewLine
'| Migra��o SQL - Melhoria de performance
'+----------------------------------------
        
        Set rs = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, Sql, True)
        
        If cmbCongenere.Text = "Nossa Parte" Then
            If Not rs.EOF Then
                txtPercParticipacao.Text = Format(100 - CCur(Replace(("0" & rs("perc_participacao")), ".", ",")), "###,###,##0.00")
                txtPercDespesa.Text = "0,00"
            End If

        Else

            If Not rs.EOF Then
                txtPercParticipacao.Text = Format(CCur(Replace(("0" & rs("perc_participacao")), ".", ",")), "###,###,##0.00")
                txtPercDespesa.Text = Format(CCur(Replace(("0" & rs("perc_despesa_lider")), ".", ",")), "###,###,##0.00")
            End If
        End If
        
        rs.Close
    End If
    
End Sub

Private Function ObterDadosEspecificosAgricola(lPropostaId As Long) As Recordset
    On Error GoTo TrataErro

    Dim sSql As String
  
    sSql = ""
    sSql = sSql & " SELECT b.dt_inicio_plantio, "
    sSql = sSql & "        b.dt_fim_plantio, "
    sSql = sSql & "        b.dt_prev_plantio, "
    sSql = sSql & "        b.sistema_producao, "
    sSql = sSql & "        b.area_seg_hectare, "
    sSql = sSql & "        b.nome_propriedade, "
    sSql = sSql & "        b.taxa_net, "
    sSql = sSql & "        b.taxa_carregada, "
    sSql = sSql & "        b.ind_renovacao, "
    sSql = sSql & "        b.perc_bonus, "
    sSql = sSql & "        b.producao_esperada, "
    sSql = sSql & "        b.producao_segurada, "
    sSql = sSql & "        b.ind_lavoura_plant, "
    sSql = sSql & "        b.ind_APTA, "
    sSql = sSql & "        b.num_insp_previa, "
    sSql = sSql & "        b.dt_insp_previa, "
    sSql = sSql & "        b.num_contrato, "
    sSql = sSql & "        b.num_var_contrato, "
    sSql = sSql & "        b.agencia_financiadora, "
    sSql = sSql & "        b.agencia_financiadora_dv, "
    sSql = sSql & "        b.val_financ, "
    sSql = sSql & "        b.dt_inicio_financ, "
    sSql = sSql & "        b.dt_fim_financ, "
    sSql = sSql & "        b.risco_tecnico, "
    sSql = sSql & "        b.risco_cliente, "
    sSql = sSql & "        b.motivo_endosso, "
    sSql = sSql & "        b.ind_renovacao, "
    sSql = sSql & "        e.sistema_cultivo_id, "
    sSql = sSql & "        nome_sistema_cultivo = e.nome,"
    sSql = sSql & "        c.tp_cultura_id, "
    sSql = sSql & "        nome_tp_cultura = c.nome, "
    sSql = sSql & "        d.sub_tp_cultura_id, "
    sSql = sSql & "        nome_sub_tp_cultura = d.nome, "
    sSql = sSql & "        f.tp_ciclo_id, "
    sSql = sSql & "        nome_tp_ciclo = f.nome, "
    sSql = sSql & "        g.tp_agricultura_id, "
    sSql = sSql & "        nome_tp_agricultura = g.nome, "
    sSql = sSql & "        b.ind_laudo, "
    sSql = sSql & "        b.cnpj_avaliador, "
    sSql = sSql & "        b.matr_avaliador, "
    sSql = sSql & "        b.ind_financiamento "
    sSql = sSql & "   FROM seguro_generico_tb a  WITH (NOLOCK) , "
    sSql = sSql & "        seguro_agricola_tb b  WITH (NOLOCK) , "
    sSql = sSql & "        tp_cultura_tb c  WITH (NOLOCK) , "
    sSql = sSql & "        sub_tp_cultura_tb d  WITH (NOLOCK) , "
    sSql = sSql & "        sistema_cultivo_tb e  WITH (NOLOCK) , "
    sSql = sSql & "        tp_ciclo_tb f  WITH (NOLOCK) , "
    sSql = sSql & "        tp_agricultura_tb g  WITH (NOLOCK) "
    sSql = sSql & "  WHERE a.proposta_id = b.proposta_id "
    sSql = sSql & "    AND a.cod_objeto_segurado = b.cod_objeto_segurado "
    sSql = sSql & "    AND a.dt_inicio_vigencia_seg = b.dt_inicio_vigencia_seg "
    sSql = sSql & "    AND a.seq_canc_endosso_seg = b.seq_canc_endosso_seg "
    sSql = sSql & "    AND b.tp_cultura_id = c.tp_cultura_id "
    sSql = sSql & "    AND c.tp_cultura_id = d.tp_cultura_id "
    sSql = sSql & "    AND b.sub_tp_cultura_id = d.sub_tp_cultura_id "
    sSql = sSql & "    AND b.sistema_cultivo_id = e.sistema_cultivo_id "
    sSql = sSql & "    AND b.tp_ciclo_id = f.tp_ciclo_id "
    sSql = sSql & "    AND b.tp_agricultura_id = g.tp_agricultura_id "
    sSql = sSql & "    AND a.proposta_id = " & lPropostaId
    sSql = sSql & "    AND a.dt_fim_vigencia_seg IS NULL "

    Set ObterDadosEspecificosAgricola = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, sSql, True)

    Exit Function
TrataErro:
    Call TratarErro("ObterDadosEspecificosAgricola", Me.name)
    Call FinalizarAplicacao
End Function

Private Function ObterDadosEnderecoRiscoAgricola(lPropostaId As Long) As Recordset

    On Error GoTo TrataErro

    Dim sSql As String

    sSql = ""
    sSql = sSql & " Select a.endereco, "
    sSql = sSql & "        b.nome, "
    sSql = sSql & "        a.estado, "
    sSql = sSql & "        a.CEP, "
    sSql = sSql & "        b.municipio_ibge_id "
    sSql = sSql & "   From endereco_risco_tb a  WITH (NOLOCK) , "
    sSql = sSql & "        municipio_tb b  WITH (NOLOCK)  "
    sSql = sSql & "  Where a.municipio_id = b.municipio_id "
    sSql = sSql & "    and a.estado = b.estado "
    sSql = sSql & "    and a.proposta_id = " & lPropostaId
    sSql = sSql & "    and a.end_risco_id = "
    sSql = sSql & "                           (Select max(end_risco_id) from endereco_risco_tb a2 "
    sSql = sSql & "                            where a2.proposta_id = a.proposta_id) "

    Set ObterDadosEnderecoRiscoAgricola = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, sSql, True)

    Exit Function
TrataErro:
    Call TratarErro("ObterDadosEnderecoRiscoAgricola", Me.name)
    Call FinalizarAplicacao

End Function

Private Function CarregarDadosAgricola(lPropostaId As Long) As Boolean

    On Error GoTo TrataErro

    Dim rsAgricola As Recordset
    
    CarregarDadosAgricola = True
    
    Set rsAgricola = New Recordset
    
    'Dados Especificos ''''''''''''''''''''''''''''''''''''''''''''''''''''''
    Set rsAgricola = ObterDadosEspecificosAgricola(lPropostaId)
    
    If Not rsAgricola.EOF Then

        TxtTpCulturaId.Text = rsAgricola!tp_cultura_id
        TxtDescTpCultura.Text = rsAgricola!nome_tp_cultura
        TxtSubTpCulturaId.Text = rsAgricola!sub_tp_cultura_id
        TxtDescSubTpCultura.Text = rsAgricola!nome_sub_tp_cultura
        TxtSistemaCultivoId.Text = rsAgricola!sistema_cultivo_id
        TxtDescSistemaCultivo.Text = rsAgricola!nome_sistema_cultivo
        txtTpCicloId.Text = IIf(IsNull(rsAgricola!tp_ciclo_id), "", rsAgricola!tp_ciclo_id)
        txtDescTpCiclo.Text = IIf(IsNull(rsAgricola!nome_tp_ciclo), "", rsAgricola!nome_tp_ciclo)
        txtTpAgriculturaId.Text = IIf(IsNull(rsAgricola!tp_agricultura_id), "", rsAgricola!tp_agricultura_id)
        txtDescTpAgricultura.Text = IIf(IsNull(rsAgricola!nome_tp_agricultura), "", rsAgricola!nome_tp_agricultura)
        txtCNPJAvaliador.Text = IIf(IsNull(rsAgricola!cnpj_avaliador), "", rsAgricola!cnpj_avaliador)
        txtMatAvaliador.Text = IIf(IsNull(rsAgricola!matr_avaliador), "", rsAgricola!matr_avaliador)
        TxtSistemaProducao.Text = rsAgricola!sistema_producao
        TxtDtInicioPlantio.Text = Format(rsAgricola!dt_inicio_plantio, "dd/mm/yyyy")
        TxtDtFimPlantio.Text = Format(rsAgricola!dt_fim_plantio, "dd/mm/yyyy")
        TxtDtPrevPlantio.Text = Format(rsAgricola!dt_prev_plantio, "dd/mm/yyyy")
        TxtPercBonus.Text = Val(rsAgricola!perc_bonus)
        TxtProducaoEsperada.Text = rsAgricola!producao_esperada
        TxtProducaoSegurada.Text = rsAgricola!producao_segurada
        
        If UCase(rsAgricola!ind_lavoura_plant) = "S" Then
            ChkIndLavouraPlant.Value = vbChecked
        Else
            ChkIndLavouraPlant.Value = vbUnchecked
        End If
           
        If UCase(rsAgricola!ind_APTA) = "S" Then
            ChkIndApta.Value = vbChecked
        Else
            ChkIndApta.Value = vbUnchecked
        End If
        
        If UCase(rsAgricola!ind_laudo) = "S" Then
            chkIndLaudo.Value = vbChecked
        Else
            chkIndLaudo.Value = vbUnchecked
        End If
        
        TxtNrInspPrevia.Text = IIf(IsNull(rsAgricola!num_insp_previa) = False, rsAgricola!num_insp_previa, "")
        TxtDtInspPrevia.Text = IIf(IsNull(rsAgricola!dt_insp_previa) = False, rsAgricola!dt_insp_previa, "")
        TxtNumContrato.Text = rsAgricola!num_contrato
        TxtAgenciaFinanciadora.Text = rsAgricola!agencia_financiadora
        
        TxtAgenciaFinanciadoraDv = rsAgricola!agencia_financiadora_dv
        TxtDtInicioFinanciamento.Text = Format(rsAgricola!dt_inicio_financ, "dd/mm/yyyy")
        TxtDtFimFinanciamento.Text = Format(rsAgricola!dt_fim_financ, "dd/mm/yyyy")
        TxtRiscoTecnico.Text = rsAgricola!risco_tecnico
        TxtRiscoCLiente.Text = rsAgricola!risco_cliente
        TxtMotivoEndosso.Text = rsAgricola!motivo_endosso
        
        TxtPropriedade.Text = rsAgricola!nome_propriedade
        TxtArea.Text = rsAgricola!area_seg_hectare
    
        TxtValorFinanciado.Text = FormataValorParaPadraoBrasil(rsAgricola!val_financ)
        
        '       mfilie - 19/07/2004 - Alterado para n�o zerar os valores das casas decimais.
        '       TxtTxNet.Text = Val(rsAgricola!taxa_net)
        '       TxtTxCarregada.Text = Val(rsAgricola!taxa_carregada)

        TxtTxNet.Text = Val(TrocaVirgulaPorPonto(rsAgricola!taxa_net))
        TxtTxCarregada.Text = Val(TrocaVirgulaPorPonto(rsAgricola!taxa_carregada))
     
        '        If Not IsNull(rsAgricola("ind_financiamento")) Then
        '          Select Case UCase(rsAgricola("ind_financiamento"))
        '            Case "S"
        '              txtIndFinanciamento.Text = "COM FINANCIAMENTO"
        '            Case "N"
        '              txtIndFinanciamento.Text = "SEM FINANCIAMENTO"
        '            Case Else
        '              txtIndFinanciamento.Text = ""
        '          End Select
        '        Else
        '          txtIndFinanciamento.Text = ""
        '        End If

    End If
    
    'Endereco de risco ''''''''''''''''''''''''''''''''''''''''''''''
    rsAgricola.Close
    Set rsAgricola = ObterDadosEnderecoRiscoAgricola(lPropostaId)
    
    If Not rsAgricola.EOF Then
   
        TxtEnderecoRisco.Text = rsAgricola!Endereco
        TxtMunicipioRisco.Text = IIf(IsNull(rsAgricola!Nome) = False, rsAgricola!Nome, "")
        TxtUFRisco.Text = rsAgricola!estado
        TxtCEPRisco.Text = IIf(IsNull(rsAgricola!Cep) = False, MasCEP(rsAgricola!Cep), "")
        TxtMunicipioIBGE.Text = IIf(IsNull(rsAgricola!municipio_ibge_id) = False, rsAgricola!municipio_ibge_id, "")
        
    Else
        
        TxtEnderecoRisco.Text = ""
        TxtMunicipioRisco.Text = ""
        TxtUFRisco.Text = ""
        TxtCEPRisco.Text = ""
        TxtMunicipioIBGE.Text = ""
        
    End If
    
    rsAgricola.Close
    Set rsAgricola = Nothing

    Exit Function

TrataErro:
    Call TratarErro("ObterDadosAgricola", Me.name)
    Call FinalizarAplicacao

End Function

Public Function FormataValorParaPadraoBrasil(valor As String) As String
   
    Dim sConfDecimal As String
   
    sConfDecimal = LeArquivoIni("WIN.INI", "intl", "sDecimal")
   
    'Convertendo valor do d�bito para o padr�o brasileiro
   
    If sConfDecimal = "." Then
        valor = TrocaValorAmePorBras(Format(valor, "###,###,##0.00"))
    Else
        valor = Format(valor, "###,###,##0.00")
    End If

    FormataValorParaPadraoBrasil = valor
   
End Function

Private Sub DefinirExibicaoObjetos(lProdutoId As Long, _
                                   iTpPessoa As Integer)

    Select Case lProdutoId
    
        Case ProdIdOuroAgricola
            SSTab1.TabVisible(10) = True
            fraDadosCultivo.Visible = True
            fraDadosComplementares.Visible = True
            fraEndRiscoAgricola.Visible = True
            fraPenhorRural.Visible = False
            fraEndRiscoPenhorRural.Visible = False
            fraSeguroConsorcio.Visible = False
      
        Case ProdIdPenhorRural
            SSTab1.TabVisible(10) = True
            fraDadosCultivo.Visible = False
            fraDadosComplementares.Visible = False
            fraEndRiscoAgricola.Visible = False
            fraPenhorRural.Visible = True
            fraEndRiscoPenhorRural.Visible = True
            fraSeguroConsorcio.Visible = False
      
        Case ProdIdOuroVidaMulher
            'bcarneiro - 01/09/2005
            'lblISDGN.Visible = True
            'lblPremioDGN.Visible = True
            lblISDGN.Visible = False
            lblPremioDGN.Visible = False
            '''''''''''''''''''''''''''
            lblAssistFacul.Visible = True
            lblIndTitCC.Visible = True
            'bcarneiro - 01/09/2005
            'txtISDGN.Visible = True
            'txtPremioDGN.Visible = True
            txtISDGN.Visible = False
            txtPremioDGN.Visible = False
            ''''''''''''''''''''''''
      
            txtAssistFacul.Visible = True
            txtIndTitCC.Visible = True
      
        Case ProdIdSeguroConsorcio
            SSTab1.TabVisible(10) = True
            fraDadosCultivo.Visible = False
            fraDadosComplementares.Visible = False
            fraEndRiscoAgricola.Visible = False
            fraPenhorRural.Visible = False
            fraEndRiscoPenhorRural.Visible = False
            fraSeguroConsorcio.Visible = True
      
        Case Else
            SSTab1.TabVisible(10) = False
    
    End Select

    Select Case iTpPessoa

        Case 1 'pessoa fisica
            lbltexto(1).Visible = False  'cgc
            lbltexto(7).Visible = False  'contato
            lbltexto(8).Visible = True   'sexo
            lbltexto(9).Visible = True   'est.civil
            lbltexto(10).Visible = True  'cpf
            lbltexto(11).Visible = True  'dt nasc
            lbltexto(12).Visible = True  'idade
            TxtCGC.Visible = False
            TxtContato.Visible = False
            txtSexo.Visible = True
            txtEstCivil.Visible = True
            txtCPFTit.Visible = True
            txtDtNascTit.Visible = True
            txtIdadeTit.Visible = True
            lbltexto(35).Visible = True 'no doc id
            lbltexto(36).Visible = True 'tp doc id
            lbltexto(37).Visible = True 'dt exp doc id
            lbltexto(38).Visible = True 'orgao exp
            txtNoDocIdentidade.Visible = True
            txtTpDocIdentidade.Visible = True
            txtDtExpedicaoIdentidade.Visible = True
            txtOrgaoExpIdentidade.Visible = True
      
            SSTab1.TabVisible(6) = True  'beneficiarios
      
            'apresenta os dados de cart�o de cr�dito para pessoa f�sica
            lblInfCartao(0).Visible = True
            lblInfCartao(1).Visible = True
            txtNumCartao.Visible = True
            txtDtCartao.Visible = True
      
            'bcarneiro - 24/06/2004 - BB Seguro Vida
            'If lProdutoId = ProdIdOurovidaRuralRapido Or _
             lProdutoId = ProdIdOurovidaProdutorRural Or _
             lProdutoId = ProdIdOuroAgricola Or _
             lProdutoId = ProdIdPenhorRural Or _
             lProdutoId = ProdIdPrestamistaBP Or _
             lProdutoId = ProdIdSeguroConsorcio Then
               
            'Alterado 30/07/2004 - Eduardo Cintra - Inclusao produto Seguro Vida Prestamista
            If lProdutoId = ProdIdOurovidaRuralRapido Or lProdutoId = ProdIdOurovidaProdutorRural Or lProdutoId = ProdIdOuroAgricola Or lProdutoId = ProdIdPenhorRural Or lProdutoId = ProdIdPrestamistaBP Or lProdutoId = ProdIdSeguroConsorcio Or lProdutoId = ProdIdBBSeguroVida Or lProdutoId = ProdIdBBSeguroVidaAgriculturaFamiliar Then
         
                SSTab1.TabVisible(5) = True
                SSTab1.TabVisible(3) = False 'objeto de risco - declaracoes
                FrameDadosConjuge.Visible = FrameDadosConjuge.Visible And False
                fraMutuarios.Top = FrameDadosConjuge.Top
          
            Else
          
                SSTab1.TabVisible(3) = True  'objeto de risco - declaracoes
                FrameDadosConjuge.Visible = FrameDadosConjuge.Visible And True
          
                'If Trim(txtNomeConj.Text) <> "" Then
                If Trim(txtConjuge(1).Text) <> "" Then
                    FrameDadosConjuge.Visible = True
                End If
          
            End If
 
            If lProdutoId = ProdIdOurovidaRuralRapido Or lProdutoId = ProdIdBBSeguroVida Then
         
                SSTab1.TabVisible(3) = True 'objeto de risco - declaracoes
                Frame6.Visible = False
                fraDeclConjuge = False
                fraVidasSeguradas = False
                fraMutuarios.Top = Frame6.Top
          
            End If
    
        Case 2 'pessoa juridica
            lbltexto(1).Visible = True   'cgc
            lbltexto(7).Visible = True   'contato
            lbltexto(8).Visible = False  'sexo
            lbltexto(9).Visible = False  'est.civil
            lbltexto(10).Visible = False 'cpf
            lbltexto(11).Visible = False 'dt nasc
            lbltexto(12).Visible = False 'idade
            TxtCGC.Visible = True
            TxtContato.Visible = True
            txtSexo.Visible = False
            txtEstCivil.Visible = False
            txtCPFTit.Visible = False
            txtDtNascTit.Visible = False
            txtIdadeTit.Visible = False
            lbltexto(35).Visible = False 'no doc id
            lbltexto(36).Visible = False 'tp doc id
            lbltexto(37).Visible = False 'dt exp doc id
            lbltexto(38).Visible = False 'orgao exp
            txtNoDocIdentidade.Visible = False
            txtTpDocIdentidade.Visible = False
            txtDtExpedicaoIdentidade.Visible = False
            txtOrgaoExpIdentidade.Visible = False
      
            SSTab1.TabVisible(3) = False  'objeto de risco - declaracoes
            SSTab1.TabVisible(6) = False  'beneficiarios
      
            'nao apresenta os dados de cart�o de cr�dito para pessoa juridica
            lblInfCartao(0).Visible = False
            lblInfCartao(1).Visible = False
            txtNumCartao.Visible = False
            txtDtCartao.Visible = False
      
            If lProdutoId = ProdIdOurovidaEmpresa Then
                ' Autor: breno.nery (Nova Consultoria) - Data da Altera��o: 13/06/2012
                ' Demanda: 13574539 - Aplicativo C�lculo para Auxiliar A��es Judiciais
                'LblCapitalSegurado.Visible = False
                Label15(2).Visible = False
                '---------------------------------
                txtIS.Visible = False
                LblPremio.Visible = False
                txtPremio.Visible = False
                'lblAgenciaDeb.Visible = False claudia.araujo
                'lblCCDeb.Visible = False claudia.araujo
                lbldadosbancarios(0) = False
                txtAgenciaDeb.Visible = False
                txtCCDeb.Visible = False
          
                '(in�cio) -- rsouza -- 18/05/2011 --'
                frmPlanosAnteriores.Visible = False
                '(fim)    -- rsouza -- 18/05/2011 --'
          
                fraPremio.Visible = False
                fraCapitalSegurado.Top = fraPremio.Top
                    
                lblIOF.Visible = False
                txtIOF.Visible = False
                Label19(0).Visible = False
                txtDescComercial(1).Visible = False
                'Label19(1).Visible = False claudia.araujo
                txtDescTec(2).Visible = False
                lblCustoCertificado.Visible = False
                txtCustoCertificado.Visible = False
                lblPremioBrutoVista.Visible = False
                txtPremioVista(0).Visible = False
          
            Else
                ' Autor: breno.nery (Nova Consultoria) - Data da Altera��o: 13/06/2012
                ' Demanda: 13574539 - Aplicativo C�lculo para Auxiliar A��es Judiciais
                'LblCapitalSegurado.Visible = True
                Label15(2).Visible = True
                '---------------------------------
                txtIS.Visible = True
                LblPremio.Visible = True
                txtPremio.Visible = True
'                lblAgenciaDeb.Visible = True claudia.araujo
'                lblCCDeb.Visible = True claudia.araujo
                lbldadosbancarios(0) = True
                txtAgenciaDeb.Visible = True
                txtCCDeb.Visible = True
          
                lblIOF.Visible = True
                txtIOF.Visible = True
                Label19(0).Visible = True
                txtDescComercial(1).Visible = True
                'Label19(1).Visible = False claudia.araujo
                txtDescTec(2).Visible = True
                lblCustoCertificado.Visible = True
                txtCustoCertificado.Visible = True
                lblPremioBrutoVista.Visible = True
                txtPremioVista(0).Visible = True
          
            End If
    
        Case Else
            Call MsgBox("Tipo de pessoa inv�lido", vbCritical)
            'Call FinalizarAplicacao
    End Select

End Sub

Function ObterNomeArqExterno(lProdutoId As Long) As String

    On Error GoTo Erro

    Dim Sql As String
    Dim RsArquivo As Recordset

    Sql = "SELECT a.arquivo "
    Sql = Sql & " FROM arquivo_produto_tb a, produto_tb b  WITH (NOLOCK)  "
    Sql = Sql & " WHERE a.produto_id = b.produto_id "
    Sql = Sql & " and (a.produto_id = " & lProdutoId
    Sql = Sql & " or b.produto_anterior_id = " & lProdutoId & ")"

    Set RsArquivo = New Recordset
    Set RsArquivo = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, Sql, True)
                                                                    
    If Not RsArquivo.EOF Then
        ObterNomeArqExterno = RsArquivo(0)
    Else
        'Call Err.Raise(vbObjectError + 1038, , "ObterNomeArqExterno - Arquivo do produto n�o encontrado")
        Call MsgBox("Arquivo do produto n�o encontrado", vbCritical)
        RsArquivo.Close
        Set RsArquivo = Nothing
        Unload Me
        Exit Function
        'FinalizarAplicacao
        'GoTo Erro
    End If
      
    RsArquivo.Close
    Set RsArquivo = Nothing

    Exit Function

Erro:
    Call TratarErro("ObterNomeArqExterno", Me.name)
    Call FinalizarAplicacao
   
End Function

Function BuscaParametro(ByVal pParametro As String) As String

    Dim rs As Recordset
    Dim sSql As String

    On Error GoTo ErroBuscaParametro

    sSql = "Select val_parametro"
    sSql = sSql & " From ps_parametro_tb  WITH (NOLOCK) "
    sSql = sSql & " Where parametro = '" & pParametro & "'"
    
    Set rs = New Recordset
    Set rs = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, sSql, True)

    If Not rs.EOF Then
        BuscaParametro = rs(0)
    Else
        BuscaParametro = ""
    End If

    rs.Close
    Set rs = Nothing

    Exit Function

ErroBuscaParametro:
    Call TratarErro("BuscaParametro", Me.name)
    Call FinalizarAplicacao
    
End Function

Private Function ObterDadosPropostaAnterior(lPropostaId As Long, _
                                            lProdutoId As Long) As Recordset

    On Error GoTo TrataErro

    Dim sSql As String
  
    sSql = ""

    If lProdutoId = ProdIdOuroVidaMigracao Then
    
        sSql = sSql & "SELECT proposta_id_anterior "
        sSql = sSql & "  FROM proposta_tb  WITH (NOLOCK)  "
        sSql = sSql & " WHERE proposta_id = " & lPropostaId
       
    Else
    
        sSql = sSql & " SELECT proposta_id "
        sSql = sSql & "   FROM proposta_tb  WITH (NOLOCK)  "
        sSql = sSql & "  WHERE proposta_id_anterior = " & lPropostaId
        sSql = sSql & "    AND produto_id = 136"
    
    End If
  
    Set ObterDadosPropostaAnterior = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, sSql, True)

    Exit Function
TrataErro:
    Call TratarErro("ObterDadosPropostaAnterior", Me.name)
    Call FinalizarAplicacao
End Function

Private Function ObterDadosSinistro(lPropostaId As Long) As Recordset

    On Error GoTo TrataErro

    Dim sSql As String

'    sSql = ""
'    sSql = sSql & "      SELECT distinct s.sinistro_id, " & vbNewLine
'    sSql = sSql & "             e.nome, " & vbNewLine
'    sSql = sSql & "             s.dt_aviso_sinistro, " & vbNewLine
'    sSql = sSql & "             s.dt_ocorrencia_sinistro, " & vbNewLine
'    sSql = sSql & "             Sitnome = CASE s.situacao " & vbNewLine
'    sSql = sSql & "                          WHEN '0' THEN 'Aberto' " & vbNewLine
'    sSql = sSql & "                          WHEN '1' THEN 'Reaberto' " & vbNewLine
'    sSql = sSql & "                          ELSE CASE p.item_val_estimativa " & vbNewLine
'    sSql = sSql & "                                 WHEN 1 THEN 'Liquidado' " & vbNewLine
'    sSql = sSql & "                                 ELSE 'Encerrado' " & vbNewLine
'    sSql = sSql & "                               END " & vbNewLine
'    sSql = sSql & "                       END, " & vbNewLine
'    sSql = sSql & "           ISNULL( p.val_acerto, 0) + ISNULL( p.val_correcao, 0) val_pgto " & vbNewLine
'    'In�cio - 17860335 MP02 - Felipe Sbragia - 14/10/2013
'    sSql = sSql & "      FROM seguros_db.dbo.sinistro_tb s  WITH (NOLOCK)  " & vbNewLine
'    sSql = sSql & "INNER JOIN seguros_db.dbo.evento_sinistro_tb e  WITH (NOLOCK)  " & vbNewLine
'    sSql = sSql & "        ON e.evento_sinistro_id = s.evento_sinistro_id " & vbNewLine
'    sSql = sSql & " LEFT JOIN seguros_db.dbo.pgto_sinistro_tb p  WITH (NOLOCK)  " & vbNewLine
'    sSql = sSql & "        ON p.sinistro_id = s.sinistro_id " & vbNewLine
'    sSql = sSql & "       AND p.ramo_id = s.ramo_id " & vbNewLine
'    sSql = sSql & "       AND p.apolice_id = s.apolice_id " & vbNewLine
'    sSql = sSql & "       AND p.sucursal_seguradora_id = s.sucursal_seguradora_id " & vbNewLine
'    sSql = sSql & "       AND p.seguradora_cod_susep = s.seguradora_cod_susep " & vbNewLine
'    sSql = sSql & " LEFT JOIN Seguros_Hist_Db.dbo.Pgto_Sinistro_Hist_tb ph  WITH (NOLOCK)  " & vbNewLine
'    sSql = sSql & "        ON ph.sinistro_id = s.sinistro_id " & vbNewLine
'    sSql = sSql & "       AND ph.ramo_id = s.ramo_id " & vbNewLine
'    sSql = sSql & "       AND ph.apolice_id = s.apolice_id " & vbNewLine
'    sSql = sSql & "       AND ph.sucursal_seguradora_id = s.sucursal_seguradora_id " & vbNewLine
'    sSql = sSql & "       AND ph.seguradora_cod_susep = s.seguradora_cod_susep " & vbNewLine
'    'Fim - 17860335 MP02 - Felipe Sbragia - 14/10/2013
'    sSql = sSql & "     WHERE s.proposta_id = " & lPropostaId & vbNewLine

    sSql = ""
    sSql = sSql & " SET NOCOUNT ON EXEC seguros_db.dbo.SEGS13094_SPS " & lPropostaId
  
    Set ObterDadosSinistro = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, sSql, True)

    Exit Function
TrataErro:
    Call TratarErro("ObterDadosSinistro", Me.name)
    Call FinalizarAplicacao
End Function

Private Function ObterDadosMutuario(lPropostaId As Long) As Recordset

    On Error GoTo TrataErro

    Dim sSql As String
  
    'sSQL = sSQL & "    SELECT cliente_tb.cliente_id, " & vbNewLine
    'sSQL = sSQL & "           cliente_tb.nome, " & vbNewLine
    'sSQL = sSQL & "           pessoa_fisica_tb.CPF, " & vbNewLine
    'sSQL = sSQL & "           ISNULL(pessoa_fisica_tb.dt_nascimento,'19000101') dt_nascimento, " & vbNewLine
    'sSQL = sSQL & "           pessoa_fisica_tb.sexo, " & vbNewLine
    'sSQL = sSQL & "           endereco_cliente_tb.endereco, " & vbNewLine
    'sSQL = sSQL & "           endereco_cliente_tb.estado, " & vbNewLine
    'sSQL = sSQL & "           endereco_cliente_tb.cep, " & vbNewLine
    'sSQL = sSQL & "           isnull(seguro_vida_tb.val_capital_segurado,0) val_capital_segurado, " & vbNewLine
    'sSQL = sSQL & "           seguro_vida_tb.tp_cliente " & vbNewLine
    'sSQL = sSQL & "      FROM proposta_tb  WITH (NOLOCK)  " & vbNewLine
    'sSQL = sSQL & "INNER JOIN seguro_vida_tb WITH (NOLOCK)  " & vbNewLine
    'sSQL = sSQL & "        ON proposta_tb.proposta_id  = seguro_vida_tb.proposta_id " & vbNewLine
    'sSQL = sSQL & "INNER JOIN cliente_tb   WITH (NOLOCK) " & vbNewLine
    'sSQL = sSQL & "        ON seguro_vida_tb.prop_cliente_id  = cliente_tb.cliente_id  " & vbNewLine
    'sSQL = sSQL & "INNER JOIN pessoa_fisica_tb WITH (NOLOCK)  " & vbNewLine
    'sSQL = sSQL & "        ON cliente_tb.cliente_id = pessoa_fisica_tb.pf_cliente_id " & vbNewLine
    'sSQL = sSQL & " LEFT JOIN endereco_cliente_tb  WITH (NOLOCK) " & vbNewLine
    'sSQL = sSQL & "        ON seguro_vida_tb.prop_cliente_id  = endereco_cliente_tb.cliente_id " & vbNewLine
    'sSQL = sSQL & "     WHERE proposta_tb.proposta_id  = " & lPropostaId & vbNewLine
    
    
     'Amanda Viana - Confitec - 16/10/2019 - circula 365 - Participa��o mutu�rios
    If Val(txtProduto.Tag) = ProdIdOurovidaProdutorRural Or Val(txtProduto.Tag) = ProdIdBBSeguroVidaAgriculturaFamiliar Then
        sSql = ""
        sSql = "SET nocount on" & vbNewLine
        sSql = sSql & "SELECT s.proposta_id, SUM(s.val_capital_segurado) val_capital_segurado_total   " & vbNewLine
        sSql = sSql & "INTO #total_capital_proposta" & vbNewLine
        sSql = sSql & "FROM seguros_db..SEGURO_VIDA_TB s WITH (NOLOCK) " & vbNewLine
        sSql = sSql & "INNER JOIN cliente_tb  sp WITH (NOLOCK)" & vbNewLine
        sSql = sSql & "ON sp.cliente_id = s.prop_cliente_id  " & vbNewLine
        sSql = sSql & "WHERE s.proposta_id = " & lPropostaId & " "
        sSql = sSql & "group by s.proposta_id " & vbNewLine
        sSql = sSql & vbNewLine
    End If
    
      
  '  sSql = ""
    sSql = sSql & "      SELECT cliente_tb.nome,                                                   " & vbNewLine
    sSql = sSql & "             CPF = ISNULL(pessoa_fisica_tb.CPF, ''),                                              " & vbNewLine
    sSql = sSql & "             sexo = ISNULL(pessoa_fisica_tb.sexo, ''),                                             " & vbNewLine
    sSql = sSql & "             ISNULL(pessoa_fisica_tb.dt_nascimento,'19000101') dt_nascimento,   " & vbNewLine
    sSql = sSql & "             Municipio = ISNULL(municipio_tb.nome, ''),                                                 " & vbNewLine
    sSql = sSql & "             estado = ISNULL(endereco_cliente_tb.estado, ''),                                        " & vbNewLine
    sSql = sSql & "             endereco = ISNULL(endereco_cliente_tb.endereco, ''),                                      " & vbNewLine
    sSql = sSql & "             bairro = ISNULL(endereco_cliente_tb.bairro, ''),                                        " & vbNewLine
    sSql = sSql & "             cep = ISNULL(endereco_cliente_tb.cep, ''),                                           " & vbNewLine
    sSql = sSql & "             ddd_1 = ISNULL(cliente_tb.ddd_1,0),                                                  " & vbNewLine
    sSql = sSql & "             telefone_1 = ISNULL(cliente_tb.telefone_1,0),                                             " & vbNewLine
    sSql = sSql & "             Doc_identidade = ISNULL(cliente_tb.Doc_identidade,0),                                         " & vbNewLine
    sSql = sSql & "             Tp_doc_identidade = ISNULL(cliente_tb.Tp_doc_identidade,0),                                      " & vbNewLine
    sSql = sSql & "             Exp_doc_identidade = ISNULL(cliente_tb.Exp_doc_identidade,0),                                     " & vbNewLine
    sSql = sSql & "             dt_emissao_identidade = ISNULL(cliente_tb.dt_emissao_identidade,0),                                  " & vbNewLine
    sSql = sSql & "             isnull(seguro_vida_tb.val_capital_segurado,0) val_capital_segurado,                     " & vbNewLine
    sSql = sSql & "             isnull(seguro_vida_tb.val_premio_bruto,0) val_premio_bruto,        " & vbNewLine
    sSql = sSql & "             cod_mci = ISNULL(cliente_tb.cod_mci,0)                                                 " & vbNewLine
    'Amanda Viana - Confitec - 16/10/2019 - circula 365 - Participa��o mutu�rios
    If Val(txtProduto.Tag) = ProdIdOurovidaProdutorRural Or Val(txtProduto.Tag) = ProdIdBBSeguroVidaAgriculturaFamiliar Then
        sSql = sSql & " ,   CAST(((seguro_vida_tb.val_capital_segurado / tc.val_capital_segurado_total) * 100.00) AS DECIMAL(15,2)) perc_mutuario" & vbNewLine
    End If
'    sSql = sSql & "        FROM proposta_tb  WITH (NOLOCK)                                           " & vbNewLine
'    sSql = sSql & "  INNER JOIN seguro_vida_tb  WITH (NOLOCK)                                        " & vbNewLine
'    sSql = sSql & "          ON proposta_tb.proposta_id  = seguro_vida_tb.proposta_id             " & vbNewLine
'    sSql = sSql & "  INNER JOIN cliente_tb  WITH (NOLOCK)                                            " & vbNewLine
'    sSql = sSql & "          ON seguro_vida_tb.prop_cliente_id  = cliente_tb.cliente_id           " & vbNewLine
'    sSql = sSql & "  INNER JOIN pessoa_fisica_tb  WITH (NOLOCK)                                      " & vbNewLine
'    sSql = sSql & "          ON cliente_tb.cliente_id = pessoa_fisica_tb.pf_cliente_id            " & vbNewLine
'    sSql = sSql & "   LEFT JOIN endereco_cliente_tb  WITH (NOLOCK)                                   " & vbNewLine
'    sSql = sSql & "          ON seguro_vida_tb.prop_cliente_id  = endereco_cliente_tb.cliente_id  " & vbNewLine
'    sSql = sSql & "   LEFT JOIN municipio_tb                                                      " & vbNewLine
'    sSql = sSql & "          ON municipio_tb.municipio_id = endereco_cliente_tb.municipio_id      " & vbNewLine
'    'Stefanini(Maur�cio), em 16/02/2005 - Deve ser verificado o estado, pois o municipio_id � igual para todos os estados
'    sSql = sSql & "         AND municipio_tb.estado = endereco_cliente_tb.estado                  " & vbNewLine
'    sSql = sSql & "       WHERE proposta_tb.proposta_id = " & lPropostaId & vbNewLine
            sSql = sSql & " FROM seguro_vida_tb  WITH (NOLOCK)  " & vbNewLine
            sSql = sSql & " INNER JOIN cliente_tb  WITH (NOLOCK)  " & vbNewLine
            sSql = sSql & " ON seguro_vida_tb.prop_cliente_id  = cliente_tb.cliente_id " & vbNewLine
            sSql = sSql & " INNER JOIN pessoa_fisica_tb  WITH (NOLOCK)  " & vbNewLine
            sSql = sSql & " ON cliente_tb.cliente_id = pessoa_fisica_tb.pf_cliente_id " & vbNewLine
            sSql = sSql & " LEFT JOIN endereco_cliente_tb  WITH (NOLOCK)  " & vbNewLine
            sSql = sSql & " ON seguro_vida_tb.prop_cliente_id = endereco_cliente_tb.cliente_id " & vbNewLine
            sSql = sSql & " LEFT JOIN municipio_tb " & vbNewLine
            sSql = sSql & " ON municipio_tb.municipio_id = endereco_cliente_tb.municipio_id " & vbNewLine
            sSql = sSql & "         AND municipio_tb.estado = endereco_cliente_tb.estado " & vbNewLine
             'Amanda Viana - Confitec - 16/10/2019 - circula 365 - Participa��o mutu�rios
                If Val(txtProduto.Tag) = ProdIdOurovidaProdutorRural Or Val(txtProduto.Tag) = ProdIdBBSeguroVidaAgriculturaFamiliar Then
                    sSql = sSql & " INNER JOIN seguros_db.dbo.PROPOSTA_TB WITH (NOLOCK) " & vbNewLine
                    sSql = sSql & " ON PROPOSTA_TB.proposta_id = seguro_vida_tb.proposta_id    " & vbNewLine
                    sSql = sSql & " LEFT JOIN  #total_capital_proposta tc " & vbNewLine
                    sSql = sSql & " ON tc.proposta_id = PROPOSTA_TB.proposta_id" & vbNewLine
                End If
            sSql = sSql & "       WHERE seguro_vida_tb.proposta_id = " & lPropostaId & vbNewLine

    
    Set ObterDadosMutuario = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, sSql, True)

    Exit Function
TrataErro:
    Call TratarErro("ObterDadosMutuarios", Me.name)
    Call FinalizarAplicacao
End Function

Private Sub ConfigurarInterface()

    'Titulo
  
    'lrocha - 05/01/2006
    If Trim(SelPropAdesao.MSFlexGrid1.TextMatrix(SelPropAdesao.MSFlexGrid1.Row, 6)) <> "" Then
        Me.Caption = "SEGP0176 - Consulta de Proposta " & " - " & SelPropAdesao.MSFlexGrid1.TextMatrix(SelPropAdesao.MSFlexGrid1.Row, 6)
    Else
        Me.Caption = "SEGP0176 - Consulta de Proposta "
    End If

    ' posicao inicial da tab
    SSTab1.Tab = 0
  
    ' Campo : Proposta Anterior
    lblPropMigracao.Visible = False
    txtPropMigracao.Visible = False
  
    'bcarneiro - 25/06/2004 - BB Seguro Vida
    lblCanalVenda.Visible = False
    txtCanalVenda.Visible = False
  
    'Piva - 01/08/2005 - Numero da sorte - Ouro Vida Grupo Especial
    'O form nao comporta a criacao de mais nenhum campo. Sera utilizado o text: txtCanalVenda
    lblNumeroSorte.Visible = False
  
    ' Configura��o do numero
    If LeArquivoIni("WIN.INI", "intl", "sDecimal") = "." Then
        ConfiguracaoBrasil = False
    Else
        ConfiguracaoBrasil = True
    End If

    SSTab1.TabVisible(5) = False

    'Par�metros
    TpPlanoIdQtdSalarios = BuscaParametro("TP PLANO MULTIPLO SALARIO")
    TpPlanoIdCapitalUnico = BuscaParametro("TP PLANO CAPITAL UNICO")
    IndiceIdSalarioMinimo = BuscaParametro("INDICE SALARIO MINIMO")
  
    'Grid seguro consorcio
    With grdConsorcio
        .Clear
        .Rows = 1
        .FixedCols = 0
        'Alterado por Cleber da Stefanini - data: 24/08/2005
        'A pedido da Mary, adicionar o campo Arquivo no grid
        '.Cols = 9
        .Cols = 10
        '---------------------------------------------------
        .HighLight = flexHighlightNever

        .TextMatrix(0, 0) = "Pagamento"
        .ColWidth(0) = 1100
        .TextMatrix(0, 1) = "Situa��o"
        .ColWidth(1) = 1500
        .TextMatrix(0, 2) = "Inclus�o"
        .ColWidth(2) = 1100
        .TextMatrix(0, 3) = "N� da Parcela"
        .ColWidth(3) = 1100
        .TextMatrix(0, 4) = "Saldo Devedor"
        .ColWidth(4) = 1150
        .TextMatrix(0, 5) = "Agendamento"
        .ColWidth(5) = 1100
        .TextMatrix(0, 6) = "Assembl�ia"
        .ColWidth(6) = 1100
        .TextMatrix(0, 7) = "Opera��o"
        .ColWidth(7) = 1100
        .TextMatrix(0, 8) = "Valor da Cobran�a"
        .ColWidth(8) = 1500
        'Alterado por Cleber da Stefanini - data: 24/08/2005
        'A pedido da Mary, adicionar o campo Arquivo no grid
        .TextMatrix(0, 9) = "Arquivo Origem"
        .ColWidth(9) = 1400
        '---------------------------------------------------
        
        .Refresh
        
        .Col = 0
        .Row = 0

    End With
   
    'Grid de extrato de seguro'''''''''''''''''''''''''''''''''''''
    With GridProposta(0)
        .FixedRows = 0
        'Demanda 546132 - Talitha - 07/10/2008 - Trecho alterado - Inicio
        .ColWidth(0) = 0
        .ColWidth(1) = 1000
        .ColWidth(2) = 1000
        .ColWidth(3) = 1000
        .ColWidth(4) = 4000
        .ColWidth(5) = 9000
        .ColWidth(6) = 3000
        'Demanda 546132 - Talitha - 07/10/2008 - Trecho alterado - Fim
        .Rows = 1
        .Row = 0
        .Col = 1
        .CellAlignment = flexAlignCenterCenter
        .CellBackColor = .BackColorFixed
        .Text = "Tipo"
        .Col = 2
        .CellAlignment = flexAlignCenterCenter
        .CellBackColor = .BackColorFixed
        .Text = "Dt Evento"
        'Demanda 546132 - Talitha - 07/10/2008 - Trecho Incluido - Inicio
        .Col = 3
        .CellAlignment = flexAlignCenterCenter
        .CellBackColor = .BackColorFixed
        .Text = "Dt Inclus�o"
        'Demanda 546132 - Talitha - 07/10/2008 - Trecho Incluido - Fim
        .Col = 4    'Demanda 546132 - Talitha - 07/10/2008 - Linha alterada
        .CellBackColor = .BackColorFixed
        .Text = "Descri��o Evento"
        .Col = 5    'Demanda 546132 - Talitha - 07/10/2008 - Linha alterada
        .CellBackColor = .BackColorFixed
        .Text = "Detalhes do Evento"
        .AllowUserResizing = flexResizeColumns
        'alucas 11/10/2005
        .Col = 6    'Demanda 546132 - Talitha - 07/10/2008 - Linha alterada
        .CellBackColor = .BackColorFixed
        .Text = "Funcion�rio"
        .AllowUserResizing = flexResizeColumns
    End With

    'GGAMA - 19/06/2005
    'Hist�rico das Propostas
    With grdHistorico
        .ColWidth(2) = 0
        .ColWidth(4) = 0
    End With

End Sub

Private Sub ObterDadosTelaAnterior()

    txtProposta.Text = SelPropAdesao.MSFlexGrid1.TextMatrix(SelPropAdesao.MSFlexGrid1.Row, 0)
    txtClienteId.Text = SelPropAdesao.MSFlexGrid1.TextMatrix(SelPropAdesao.MSFlexGrid1.Row, 5)
    txtDtContratacao.Text = SelPropAdesao.MSFlexGrid1.TextMatrix(SelPropAdesao.MSFlexGrid1.Row, 3)

    'flowbr3658050
    If SelPropAdesao.MSFlexGrid1.TextMatrix(SelPropAdesao.MSFlexGrid1.Row, 1) >= 200 And SelPropAdesao.MSFlexGrid1.TextMatrix(SelPropAdesao.MSFlexGrid1.Row, 1) <= 226 Then
        bProdutoBESC = True
    Else
        bProdutoBESC = False
    End If

End Sub

Private Function ConfigurarExibicaoTabs() As Boolean

    On Error GoTo TrataErro
  
    Dim sSql As String
    Dim rsRamo As Recordset
    
    ConfigurarExibicaoTabs = True
    
    If txtRamo.Tag = "" Or Val(txtRamo.Tag) = 0 Then
        'Call Err.Raise(vbObjectError + 1039, , "ConfigurarExibicaoTabs - Ramo Inv�lido")
        Call MsgBox("Ramo Inv�lido", vbCritical)
        ConfigurarExibicaoTabs = False
        Exit Function
        'FinalizarAplicacao
        'GoTo TrataErro
    End If
   'Inicio alterado 02/09/2013 projeto 17860335 - Melhorias no SEGBR MC_Amaral -confitec
'    sSql = ""
'    sSql = sSql & "SELECT tp_ramo_id " & vbNewLine
'    sSql = sSql & "  FROM ramo_tb  WITH (NOLOCK) " & vbNewLine
'    sSql = sSql & " WHERE ramo_id = " & txtRamo.Tag & vbNewLine
   
'    Set rsRamo = New Recordset
'
'    Set rsRamo = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, sSql, True)
'
'    If rsRamo.EOF Then
'        'Call Err.Raise(vbObjectError + 1040, , "ConfigurarExibicaoTabs - Ramo N�o Encontrado")
'        Call MsgBox("Ramo N�o Encontrado", vbCritical)
'        ConfigurarExibicaoTabs = False
'        Exit Function
'        'FinalizarAplicacao
'        'GoTo TrataErro
'    End If
'
    
'    If Val(rsRamo(0)) = 1 Then
    If (txtProduto.Tag <> ProdIdOuroAgricola) And (txtProduto.Tag <> ProdIdPenhorRural) Then
'    If Val(rsRamo(0)) = 1 Or Val(txtProduto.Tag) = 1218 Then
 'FIM alterado 02/09/2013 projeto 17860335 - Melhorias no SEGBR MC_Amaral -confitec
        'vida
        SSTab1.TabVisible(3) = True   'objeto de risco
        SSTab1.TabVisible(9) = True   'cobertura
        SSTab1.TabVisible(10) = False 'objeto de risco RE
        SSTab1.TabVisible(11) = False ' cobertura RE
    Else
        're
        SSTab1.TabVisible(3) = False  'objeto de risco
        SSTab1.TabVisible(9) = False  'cobertura
        SSTab1.TabVisible(10) = True  'objeto de risco RE
        SSTab1.TabVisible(11) = True  ' cobertura RE
    End If
    
'    Call rsRamo.Close
'    Set rsRamo = Nothing

    Exit Function
TrataErro:
    Call TratarErro("ConfigurarExibicaoTabs", Me.name)
    Call FinalizarAplicacao
End Function

Private Function ObterDadosProposta(lPropostaId As Long) As Recordset
    On Error GoTo ErrMetodo
    
    Dim sSql As String

    sSql = "EXEC seguros_db.dbo.SEGS7366_SPS " & lPropostaId & "," & Format(Data_Sistema, "yyyymmdd")
  
    Set ObterDadosProposta = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, sSql, True)
  
ExitMetodo:
    Exit Function

ErrMetodo:
    Call TratarErro("ObterDadosProposta", Me.name)
    Call FinalizarAplicacao
End Function

'Renato Vasconcelos
'MU-2018-017460
'Inicio
Private Function ObterDadosCanalVenda(propostaId As Long) As Recordset
    On Error GoTo ErrMetodo
    
    Dim sSql As String

    sSql = _
        "Select nm_tp_meio_canal " & _
            "from canal_db..tp_meio_canal_tb a " & _
        "inner join proposta_tb b " & _
        "   on a.canal_id = b.canal_id and isnull(a.tp_meio_canal_id_bb, a.tp_meio_canal_id) = convert(int, b.canal_venda) " & _
        "Where " & _
        "   b.proposta_id = " & propostaId
        
    Set ObterDadosCanalVenda = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, sSql, True)
  
ExitMetodo:
    Exit Function

ErrMetodo:
    Call TratarErro("ObterDadosProposta", Me.name)
    Call FinalizarAplicacao
End Function
'Fim

Sub ControlePropostaAgrupada()

    'criar controle em tempo de execu��o
    Set lblPropostaAgrupada = Controls.Add("VB.Label", "lblPropostaAgrupada", frProposta)
    With lblPropostaAgrupada
      .Left = 240
      .Top = 2280
      .Width = 1935
      .Height = 255
      .Caption = "N� da Proposta Combinada"
      .Enabled = True
      .Visible = True
      .Alignment = 0
      .AutoSize = False
    End With
    
    Set txtPropostaAgrupada = Controls.Add("VB.TextBox", "txtPropostaAgrupada", frProposta)
    With txtPropostaAgrupada
      .Left = 240
      .Top = 2520
      .Width = 1365
      .Height = 300
      .Text = ""
      .Locked = True
      .Enabled = True
      .Visible = True
      .Alignment = 0
    End With
End Sub
Private Function CarregarDadosProposta() As Boolean

    On Error GoTo TrataErro

    Dim rsSC As New Recordset
    Dim rsProposta As Recordset
    Dim rsVigencia As Recordset
    Dim rsTipo_Proposta As Recordset
    '------------------------------------------------------------------------------------
    'amarco - 15/12/2006
    '------------------------------------------------------------------------------------
    Dim rsDtEmissaoProposta As Recordset
    '------------------------------------------------------------------------------------
    Dim lOffset As Long
    Dim iSubgrupoId As Integer
    Dim Sql As String
    Dim DESQL As String
    Dim sPrazoFinanciamento As String   'RMarins 14/08/2018
  
    CarregarDadosProposta = True
     'Inicio alterado 30/08/2013 projeto 17860335 - Melhorias no SEGBR MC_Amaral -confitec
    'SQL = "SELECT  * FROM SANTA_CATARINA_LEGADO_TB WHERE alianca_cd_certificado = " & Val(txtProposta.Text)
    Sql = Sql & "SELECT santa_catarina_nu_proposta   " & vbNewLine
    Sql = Sql & "     ,santa_catarina_cd_apolice     " & vbNewLine
    Sql = Sql & "     ,santa_catarina_cd_certificado " & vbNewLine
    '27/12/2015 - Schoralick (nova consultoria) - 18690344 - Melhorias Processo Reagendamento e Reativa��o BESC - inicio
    Sql = Sql & "     , ISNULL(impedir_agendamentos,0) AS impedir_agendamentos          " & vbNewLine ' 21/12/2015 - Schoralick (nova consultoria) - 18690344 - Melhorias Processo Reagendamento e Reativa��o BESC
    Sql = Sql & "  FROM seguros_db.dbo.santa_catarina_legado_tb WITH (NOLOCK) " & vbNewLine
    Sql = Sql & " WHERE alianca_cd_certificado = " & Val(txtProposta.Text)
    '27/12/2015 - Schoralick (nova consultoria) - 18690344 - Melhorias Processo Reagendamento e Reativa��o BESC - fim
    'fim alterado 30/08/2013 projeto 17860335 - Melhorias no SEGBR MC_Amaral -confitec
    Set rsSC = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, Sql, True)
    txtPropostaSC = ""
    'txtApoliceSC = ""
     ' Amanda Viana - Confitec - altera��o para campo numero do contrato da opera��o - foi colocado o campo para indice, por falta de espa�o na tela - 18/10/2019
    txtCertificadoSC(4).Text = ""
    txtCertificadoSC(0) = ""

    If rsSC.EOF Then
        lblPropostaSC.Visible = False
       ' lblApoliceSC.Visible = False
    ' Amanda Viana - Confitec - altera��o para campo numero do contrato da opera��o- foi colocado o campo para indice, por falta de espa�o na tela - 18/10/2019
        lblCertificadoSC(4).Visible = False
        lblCertificadoSC(0).Visible = False
        txtPropostaSC.Visible = False
        'txtApoliceSC.Visible = False
    ' Amanda Viana - Confitec - altera��o para campo numero do contrato da opera��o- foi colocado o campo para indice, por falta de espa�o na tela - 18/10/2019
        txtCertificadoSC(4).Visible = False
        txtCertificadoSC(0).Visible = False
        cmbSorteio.Visible = False
        lblImpedirAgendamento.Visible = False ' 21/12/2015 - Schoralick (nova consultoria) - 18690344 - Melhorias Processo Reagendamento e Reativa��o BESC
    Else
        txtPropostaSC = rsSC("santa_catarina_nu_proposta")
       ' txtApoliceSC = rsSC("santa_catarina_cd_apolice")
     ' Amanda Viana - Confitec - altera��o para campo numero do contrato da opera��o- foi colocado o campo para indice, por falta de espa�o na tela - 18/10/2019
        txtCertificadoSC(4).Text = rsSC("santa_catarina_cd_apolice")
        txtCertificadoSC(0).Text = rsSC("santa_catarina_cd_certificado")
        lblImpedirAgendamento.Visible = rsSC("impedir_agendamentos") ' 21/12/2015 - Schoralick (nova consultoria) - 18690344 - Melhorias Processo Reagendamento e Reativa��o BESC
    End If

    rsSC.Close
    Set rsSC = Nothing
  
    Set rsProposta = New Recordset
    Set rsProposta = ObterDadosProposta(Val(txtProposta.Text))
  
    If rsProposta.EOF Then
        'Call Err.Raise(vbObjectError + 1035, , "CarregarDadosProposta - Falha no carregamento dos dados da proposta")
        'FinalizarAplicacao
        'GoTo TrataErro
        Call MsgBox("Falha no carregamento dos dados da proposta", vbCritical)
        CarregarDadosProposta = False
        Exit Function
   
    End If

    'Produto'''''
    txtProduto.Text = Trim(rsProposta("produto_id")) & " - " & Trim(rsProposta("produto"))
    txtProduto.Tag = Val(rsProposta("produto_id"))
    txtRamo.Text = Trim(rsProposta("ramo_id")) & " - " & Trim(rsProposta("ramo"))
    txtRamo.Tag = Val(rsProposta("ramo_id"))
    txtSubRamo.Text = Trim(rsProposta("subramo_id")) & " - " & Trim(rsProposta("subramo"))
    txtSubRamo.Tag = Val(rsProposta("subramo_id"))
    'Dados Bancarios -'INICIO  alterado 02/09/2013 projeto 17860335 - Melhorias no SEGBR MC_Amaral -confitec
  
    'Inclus�o Campo proposta_oferta_agrupada (Projeto 00422823 (BB Seguro Residencial Simplificado))
    If Val(rsProposta("produto_id")) = 721 Then
        Call ControlePropostaAgrupada
        txtPropostaAgrupada.Text = IIf(IsNull(rsProposta("proposta_oferta_agrupada")), "", rsProposta("proposta_oferta_agrupada"))
    End If
  
    If Not IsNull(rsProposta("deb_agencia_id")) Then
        txtAgenciaDeb.Text = Trim(rsProposta("deb_agencia_id")) & " - " & Trim(rsProposta("agencia_debito"))
        txtAgenciaDeb.Tag = Val(rsProposta("deb_agencia_id"))
    End If

    If Not IsNull(rsProposta("deb_conta_corrente_id")) Then
        txtCCDeb.Text = Trim(rsProposta("deb_conta_corrente_id"))
    End If

    If Not IsNull(rsProposta("cont_agencia_id")) Then
        txtAgencia.Text = Trim(rsProposta("cont_agencia_id")) & " - " & Trim(rsProposta("agencia_contratante"))
        txtAgencia.Tag = Val(rsProposta("cont_agencia_id"))
    End If

    txtDiaDebito.Text = IIf(IsNull(rsProposta("data_cobranca_dia")), "", rsProposta("data_cobranca_dia"))
    txtNumCartao.Text = IIf(Trim(rsProposta("num_cartao")) = 0, "", Trim(rsProposta("num_cartao")))
    txtDtCartao.Text = IIf(Format(rsProposta("dt_validade_cartao"), "dd/mm/yyyy") = "01/01/1900", "", Format(rsProposta("dt_validade_cartao"), "dd/mm/yyyy"))

    txtFormaPgto.Text = IIf(Val(rsProposta("forma_pgto_id")) = 0, "", Trim(rsProposta("forma_pgto_id")) & " - " & Trim(rsProposta("forma_pgto")))
    txtTpPgto.Text = IIf(Val(rsProposta("periodo_pgto_id")) = 0, "", Trim(rsProposta("periodo_pgto_id")) & " - " & Trim(rsProposta("periodo_pgto")))
    txtNumParcelas.Text = IIf(Val(rsProposta("num_parcelas")) = 0, "", Val(rsProposta("num_parcelas")))
    
    'Dados Bancarios -'fim  alterado 02/09/2013 projeto 17860335 - Melhorias no SEGBR MC_Amaral -confitec

  
    'Stefanini 24/10/2005 --- FlowBr 72.216 -----------------------------------------------------------------------------------------------
      'Inicio alterado 30/08/2013 projeto 17860335 - Melhorias no SEGBR MC_Amaral -confitec
     'If (Trim(txtProduto.Tag) = "121" Or Trim(txtProduto.Tag) = "135" Or Trim(txtProduto.Tag) = ProdIdOuroVidaMigracao Or Trim(txtProduto.Tag) = ProdIdOuroVidaMulher) Then
     If (Trim(txtProduto.Tag) = ProdIdOurovida Or Trim(txtProduto.Tag) = ProdIdOurovida2000 Or Trim(txtProduto.Tag) = ProdIdOuroVidaMigracao Or Trim(txtProduto.Tag) = ProdIdOuroVidaMulher) Then
        cmdDadosFinanceiros(1).Visible = True
    'fim alterado 30/08/2013 projeto 17860335 - Melhorias no SEGBR MC_Amaral -confitec
    Else
        cmdDadosFinanceiros(1).Visible = False
    End If

    'Stefanini 24/10/2005 --- FlowBr 72.216 -----------------------------------------------------------------------------------------------
  
    'Proposta '''''
    txtProposta.Text = Val(rsProposta("proposta_id"))
    txtPropostaBB.Text = IIf(IsNull(rsProposta("proposta_bb")), "", rsProposta("proposta_bb"))
    TxtCertificado.Text = IIf(Val(rsProposta("certificado_id")) = 0, "", Val(rsProposta("certificado_id")))
    txtApolice.Text = Val(rsProposta("apolice_id"))
    
    'hi-agro (gilson) - ntendencia (inicio)
    Sql = " select a.canal_venda_conducao, a.canal_venda_confirmacao, a.canal_venda, " & vbNewLine
    Sql = Sql & " a.dt_solicitacao_contratacao, t1.nm_tp_meio_canal as canal_confirmacao, t2.nm_tp_meio_canal as canal_conducao, t3.nm_tp_meio_canal as canal_contratacao " & vbNewLine
    Sql = Sql & " from seguros_db.dbo.proposta_tb a " & vbNewLine
    Sql = Sql & " left join canal_db.dbo.tp_meio_canal_tb t1" & vbNewLine
    Sql = Sql & " on a.canal_venda_confirmacao = t1.tp_meio_canal_id" & vbNewLine
    Sql = Sql & " and t1.canal_id = 1" & vbNewLine
    Sql = Sql & " left join canal_db.dbo.tp_meio_canal_tb t2" & vbNewLine
    Sql = Sql & " on a.canal_venda_conducao = t2.tp_meio_canal_id" & vbNewLine
    Sql = Sql & " and t2.canal_id = 1" & vbNewLine
    Sql = Sql & " left join canal_db.dbo.tp_meio_canal_tb t3" & vbNewLine
    Sql = Sql & " on a.canal_venda = t3.tp_meio_canal_id" & vbNewLine
    Sql = Sql & " and t3.canal_id = 1" & vbNewLine
    Sql = Sql & " where a.proposta_id = " & Val(txtProposta.Text)
    'hi-agro (gilson) - ntendencia (fim)
    
    Set rsSC = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, Sql, True)
    
    If Not rsSC.EOF Then
        'hi-agro (gilson) - ntendencia (inicio)
        txtCertificadoSC(1).Text = IIf(IsNull(rsSC("canal_confirmacao")), "", rsSC("canal_confirmacao"))
        txtCertificadoSC(2).Text = IIf(IsNull(rsSC("dt_solicitacao_contratacao")), "", rsSC("dt_solicitacao_contratacao"))
        txtCertificadoSC(3).Text = IIf(IsNull(rsSC("canal_conducao")), "", rsSC("canal_conducao"))
        txtCanalVenda.Text = IIf(IsNull(rsSC("canal_contratacao")), "", rsSC("canal_contratacao"))
        'hi-agro (gilson) - ntendencia (fim)
    End If

    rsSC.Close
    Set rsSC = Nothing
  
    '428384
    'myoshimura
    txtPropostaBBAnterior.Text = Val(rsProposta("proposta_bb_anterior"))
  
    txtSucursal.Text = Trim(rsProposta("sucursal_seguradora_id")) & " - " & Trim(rsProposta("sucursal"))
    txtSucursal.Tag = Val(rsProposta("sucursal_seguradora_id"))
  
    txtSeguradora.Text = Trim(rsProposta("seguradora_cod_susep")) & " - " & Trim(rsProposta("seguradora"))
    txtSeguradora.Tag = Val(rsProposta("seguradora_cod_susep"))
  
    txtDtContratacao = Format(rsProposta("dt_contratacao"), "dd/mm/yyyy")
    txtStatus.Tag = Trim(rsProposta("situacao"))
  
    '19/10/2005 - Rog�rio - Altera��o para obter a vigencia do endosso de renova��o, caso exista
    If txtProduto.Tag = ProdIdBBSeguroVida Then
        Sql = ""
        Sql = Sql & "SELECT   a.dt_inicio_vigencia_end, " & vbNewLine
        Sql = Sql & "         a.dt_fim_vigencia_end " & vbNewLine
        Sql = Sql & "FROM     endosso_tb a  WITH (NOLOCK) " & vbNewLine
        Sql = Sql & "WHERE    a.proposta_id = " & txtProposta.Text & vbNewLine
        Sql = Sql & "AND      a.tp_endosso_id = 250 " & vbNewLine
        Sql = Sql & "AND      a.endosso_id = (SELECT TOP 1 endosso_id " & vbNewLine
        Sql = Sql & "                         FROM    endosso_tb b  WITH (NOLOCK)  " & vbNewLine
        Sql = Sql & "                         WHERE   b.proposta_id = a.proposta_id  " & vbNewLine
        Sql = Sql & "                         ORDER BY b.endosso_id DESC) " & vbNewLine
    
        'SQL = SQL & "AND      a.endosso_id = (SELECT  MAX(endosso_id)" & vbNewLine
        'SQL = SQL & "                         FROM    endosso_tb b  WITH (NOLOCK) " & vbNewLine
        'SQL = SQL & "                         WHERE   b.endosso_id = a.endosso_id) " & vbNewLine
    
        Set rsVigencia = New Recordset
        Set rsVigencia = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, Sql, True)
    
        If Not rsVigencia.EOF Then
            txtDtIniVigProposta.Text = Format(rsVigencia(0), "dd/mm/yyyy")
            txtDtFimVigProposta.Text = Format(rsVigencia(1), "dd/mm/yyyy")
        Else
            txtDtIniVigProposta.Text = Format(rsProposta("dt_inicio_vigencia"), "dd/mm/yyyy")
            txtDtFimVigProposta.Text = IIf(Format(rsProposta("dt_fim_vigencia"), "dd/mm/yyyy") = "01/01/1900", "", Format(rsProposta("dt_fim_vigencia"), "dd/mm/yyyy"))
        End If

    Else
    
    
'*********************************************************************************************
' fabricio.brandi (Nova Consultoria) 11/09/2012
' Demanda 13833820 -  Renova��o de produtos da Unidade Vida do sistema ALS
' verifica��o para a proposta consultada se existe endosso do tipo 314 (Renova��o de Proposta).
' Se houver, ser� considerado o valor do fim de vig�ncia do endosso,
' ao inv�s do fim de vig�ncia da proposta na contrata��o.
'*********************************************************************************************
        
        Sql = ""
        Sql = Sql & "SELECT   a.dt_fim_vigencia_end " & vbNewLine
        Sql = Sql & "FROM     endosso_tb a  WITH (NOLOCK) " & vbNewLine
        Sql = Sql & "WHERE    a.proposta_id = " & txtProposta.Text & vbNewLine
        Sql = Sql & "AND      a.tp_endosso_id = 314 " & vbNewLine
        Sql = Sql & "AND      a.endosso_id = (SELECT TOP 1 endosso_id " & vbNewLine
        Sql = Sql & "                         FROM    endosso_tb b  WITH (NOLOCK)  " & vbNewLine
        Sql = Sql & "                         WHERE   b.proposta_id = a.proposta_id  " & vbNewLine
        Sql = Sql & "                         AND b.tp_endosso_id = 314   " & vbNewLine
        Sql = Sql & "                         ORDER BY b.endosso_id DESC) " & vbNewLine
        
        Set rsVigencia = New Recordset
        Set rsVigencia = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, Sql, True)
    
        If Not rsVigencia.EOF Then
              txtDtIniVigProposta.Text = Format(rsProposta("dt_inicio_vigencia"), "dd/mm/yyyy")
              txtDtFimVigProposta.Text = Format(rsVigencia(0), "dd/mm/yyyy")
        Else
              txtDtIniVigProposta.Text = Format(rsProposta("dt_inicio_vigencia"), "dd/mm/yyyy")
              txtDtFimVigProposta.Text = IIf(Format(rsProposta("dt_fim_vigencia"), "dd/mm/yyyy") = "01/01/1900", "", Format(rsProposta("dt_fim_vigencia"), "dd/mm/yyyy"))
        End If
  
    End If
  
    'amarco - 19/12/2006
    'seleciona a data de emiss�o no registro de emiss�o de ap�lice ou certificado
  
  ' 'Inicio alterado 30/08/2013 projeto 17860335 - Melhorias no SEGBR MC_Amaral -confitec
  '  If rsProposta("produto_id") <> 15 Then
     If rsProposta("produto_id") <> ProdIdOurovidaEmpresa Then
  '  Fim alterado 30/08/2013 projeto 17860335 - Melhorias no SEGBR MC_Amaral -confitec
        DESQL = ""
        DESQL = DESQL & "SELECT(COALESCE (a.dt_emissao, b.dt_emissao , c.dt_emissao))as data_emissao" & vbNewLine
        DESQL = DESQL & "FROM     proposta_tb p  WITH (NOLOCK) " & vbNewLine
        DESQL = DESQL & "LEFT JOIN apolice_tb b  ON p.proposta_id = b.proposta_id" & vbNewLine
        DESQL = DESQL & "LEFT JOIN certificado_tb a ON p.proposta_id = a.proposta_id" & vbNewLine
        DESQL = DESQL & "LEFT JOIN certificado_re_tb c ON p.proposta_id = c.proposta_id" & vbNewLine
        DESQL = DESQL & "WHERE    p.proposta_id = " & txtProposta.Text & vbNewLine
    
        Set rsDtEmissaoProposta = New Recordset
        Set rsDtEmissaoProposta = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, DESQL, True)
    End If
  
    'Leandro A. Souza - Stefanini IT - flow 180431 - 07/12/2006
    If rsProposta("produto_id") = ProdIdOurovidaEmpresa Then
        txtDtEmissaoProposta.Text = Format(rsProposta("dt_inicio_vigencia"), "dd/mm/yyyy")
    Else
        txtDtEmissaoProposta.Text = Format(rsDtEmissaoProposta("data_emissao"), "dd/mm/yyyy")
    End If

    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  
    Select Case UCase(Trim(rsProposta("situacao")))

        Case "A"
            txtStatus.Text = "ATIVA"

        Case "I"
            'txtStatus.Text = "ATIVA COM CERTIFICADO EMITIDO"
            'se for ouro vida produtor rural e o final de vigencia for menor que a data do sistema
            'ent�o a proposta est� vencida
            '                If txtProduto.Tag = 14 And CDate(IIf(txtDtFimVigProposta.Text = "", Data_Sistema, txtDtFimVigProposta.Text)) < CDate(Data_Sistema) Then
            '                    txtStatus.Text = "VENCIDA"
            '                Else
            '                    txtStatus.Text = "ATIVA COM CERTIFICADO EMITIDO"
            '                End If
                
            'Alterado em 13/03/2008 por Vinicius - Confitec conforme conversado com o Yuri
            'foi incluso na condi��o os produtos 16 e 722
            'flow 280757

            If txtProduto.Tag = ProdIdOurovidaProdutorRural Or txtProduto.Tag = ProdIdOurovidaRuralRapido Or txtProduto.Tag = ProdIdBBSeguroVidaAgriculturaFamiliar Then
                If txtDtFimVigProposta.Text = "" Then
                    txtStatus.Text = "ATIVA COM CERTIFICADO EMITIDO"
                Else

                    If CDate(txtDtFimVigProposta.Text) < CDate(Data_Sistema) Then
                        txtStatus.Text = "VENCIDA"
                    Else
                        txtStatus.Text = "ATIVA COM CERTIFICADO EMITIDO"
                    End If
                End If

            Else
                txtStatus.Text = "ATIVA COM CERTIFICADO EMITIDO"
            End If
                
        Case "C"
            txtStatus.Text = "CANCELADA"

        Case "E"
            txtStatus.Text = "EM ESTUDO"

        Case "P"
            txtStatus.Text = "ERRO NO CADASTRAMENTO"

        Case "V"
            txtStatus.Text = "EM AVALIA��O M�DICA"

        Case "L"
            txtStatus.Text = "PEDIDO DE LAUDO"

        Case "M"
            txtStatus.Text = "PEDIDO DE LAUDO EMITIDO"

        Case "R"
            txtStatus.Text = "RECUSADA"

        Case "S"
            txtStatus.Text = "CARTA RECUSA EMITIDA"

        Case "O"
            txtStatus.Text = "AGUARDANDO RENOVA��O"

        Case "U"
            txtStatus.Text = "SUSPENSA"

        Case "T"
            txtStatus.Text = "TRANSFERIDA"
    End Select
  
    'mcarvalho - 05/08/2004
    'Amanda Viana - Confitec - 16/10/2019 - circula 365 - - adicionando produto 14 para prazo financiamento
    If Trim(rsProposta("produto_id")) = CStr(ProdIdBBSeguroVidaAgriculturaFamiliar) Or SelPropAdesao.MSFlexGrid1.TextMatrix(SelPropAdesao.MSFlexGrid1.RowSel, 1) = ProdIdOurovidaProdutorRural Then
        txtFormalizacaoContrato.Text = Format(rsProposta("dt_formalizacao_contrato"), "dd/mm/yyyy")
        txtVencContrato.Text = Format(rsProposta("dt_vencimento_contrato"), "dd/mm/yyyy")

        If txtFormalizacaoContrato.Text = "01/01/1900" Then
            txtFormalizacaoContrato.Text = ""
        End If

        If txtVencContrato.Text = "01/01/1900" Then
            txtVencContrato.Text = ""
        End If
        
        'RMarins - Confitec - 14/08/2018 - MU2018 - 032171
        If Val(txtFormalizacaoContrato.Text) > 0 And Val(txtVencContrato.Text) > 0 Then
           sPrazoFinanciamento = DateDiff("M", txtFormalizacaoContrato.Text, txtVencContrato.Text)
           txtPrazoFinanciamento.Text = sPrazoFinanciamento + " Meses"
        End If
        'RMarins - Confitec - 14/08/2018


    Else
        txtFormalizacaoContrato.Text = ""
        txtVencContrato.Text = ""
    End If
   
    'bcarneiro - 25/06/2004 - BB Seguro Vida
      'Renato Vasconcelos
    'MU-2018-017460
    'Inicio
   
'    'bcarneiro - 25/06/2004 - BB Seguro Vida
'    If Val(rsProposta("canal_venda")) > 0 Then
'
'        lblCanalVenda.Visible = True
'        txtCanalVenda.Visible = True
'
'        Select Case Val(rsProposta("canal_venda"))
'
'            Case 2
'                txtCanalVenda.Text = "SISBB"
'
'            Case 4
'                txtCanalVenda.Text = "Internet"
'
'            Case 6
'                txtCanalVenda.Text = "TAA"
'        End Select
'
'    End If
  
  Dim rsCanalVenda As Recordset
  
  Set rsCanalVenda = ObterDadosCanalVenda(rsProposta("proposta_id"))
  
    If Not rsCanalVenda.EOF Then
        lblCanalVenda.Visible = True
        txtCanalVenda.Visible = True
        txtCanalVenda.Text = rsCanalVenda("nm_tp_meio_canal")
        
    End If
  
  rsCanalVenda.Close
  Set rsCanalVenda = Nothing
  'Fim
  
    TxtQtdVidas.Text = Val(rsProposta("qtd_vida"))
  
    'Piva - 01/08/2005 - Carrega numero da sorte para o produto 136 - Ouro Vida Grupo Especial
    'Inicio alterado 30/08/2013 projeto 17860335 - Melhorias no SEGBR MC_Amaral -confitec
    'If rsProposta("produto_id") = 136 Then
     If rsProposta("produto_id") = ProdIdOuroVidaMigracao Then
    'fim alterado 30/08/2013 projeto 17860335 - Melhorias no SEGBR MC_Amaral -confitec
        Call CarregarNumeroSorte(Val(txtProposta.Text))
    End If

      'Inicio alterado 30/08/2013 projeto 17860335 - Melhorias no SEGBR MC_Amaral -confitec
    'If rsProposta("produto_id") = 721 And rsProposta("certificado_id") = 0 Then
     If rsProposta("produto_id") = ProdIdBBSeguroVida And rsProposta("certificado_id") = 0 Then
    'Fim alterado 30/08/2013 projeto 17860335 - Melhorias no SEGBR MC_Amaral -confitec
        Call CertificadoPropostaRenovada(Val(txtProposta.Text))
    End If
    'inicio alterado 02/09/2013 projeto 17860335 - Melhorias no SEGBR MC_Amaral -confitec
    sSubgrupos_flag = rsProposta("situacao")
    'Subgrupo''''''''''''''''''''''''''''''''''
     If (rsProposta("produto_id") = ProdIdSeguroVidaAliancaIII And sSubgrupos_flag = "A") Then
      Call CarregarDadosSubgrupo
     End If
     
    If sSubgrupos_flag = "N" Then
     Call CarregarDadosSubgrupo
    End If
    'fim alterado 02/09/2013 projeto 17860335 - Melhorias no SEGBR MC_Amaral -confitec
     
    If colSubgrupos.Count > 0 Then
  
        'Combo de subgrupo'''''
        Call CarregarComboSubgrupo(cboSubgrupo(0)) 'proposta
        Call CarregarComboSubgrupo(cboSubgrupo(1)) 'dados financeiros
        Call CarregarComboSubgrupo(cboSubgrupo(2)) 'vidas seguradas
        Call CarregarComboSubgrupo(cboSubgrupo(3)) 'coberturas
        
        'Definindo exibicao de objetos''''''
        fraSubgrupo(0).Visible = True
        fraSubgrupo(1).Visible = True
        fraSubgrupo(2).Visible = True
    
        iSubgrupoId = cboSubgrupo(0).ItemData(cboSubgrupo(0).ListIndex)

    Else
     
        'Definindo exibicao de objetos''''''
        fraSubgrupo(0).Visible = False
        lOffset = fraCorretor.Top - fraSubgrupo(0).Top
        fraCorretor.Top = fraCorretor.Top - lOffset
        fraEstipulante.Top = fraEstipulante.Top - lOffset
    
        iSubgrupoId = -1
   ' INICIO  alterado 02/09/2013 projeto 17860335 - Melhorias no SEGBR MC_Amaral -confitec
        'Corretores '''''''''''
        Call CarregarDadosCorretores(Val(txtProposta.Text), sSubgrupos_flag, iSubgrupoId, Format(txtDtIniVigProposta.Text, "yyyymmdd"))
        
        'Estipulantes '''''''''
        Call CarregarDadosEstipulantes(Val(txtProposta.Text), sSubgrupos_flag, iSubgrupoId, Format(txtDtIniVigProposta.Text, "yyyymmdd"))
        
    End If

    'dados de profissao do titular ''''''''
    txtDescProftit.Text = IIf(IsNull(rsProposta("profissao_desc")), "", rsProposta("profissao_desc"))
  
    'dados de premio '''''''''''''''''''''''''''
    txtIOF.Text = FormataValorParaPadraoBrasil(rsProposta("val_iof"))
    txtDescComercial(1).Text = FormataValorParaPadraoBrasil(rsProposta("val_tot_desconto_comercial"))
    txtCustoCertificado.Text = FormataValorParaPadraoBrasil(rsProposta("custo_certificado"))
    txtPremioVista(0).Text = FormataValorParaPadraoBrasil(rsProposta("val_premio_bruto"))

    '--12/01/2007 - Adicionar campo "Tipo Opera��o" caso produto = 14
    'If Trim(rsProposta("produto_id")) = 14 Then
     If Trim(rsProposta("produto_id")) = ProdIdOurovidaProdutorRural Then
    'Fim alterado 30/08/2013 projeto 17860335 - Melhorias no SEGBR MC_Amaral -confitec

        'Conecta ao banco e busca o tp_registro
        Dim var_proposta_id As String
        var_proposta_id = Trim(rsProposta("proposta_id"))

        Sql = ""
        'SQL = SQL & "select * from seguros_db..emi_proposta_tb Where proposta_id = " & var_proposta_id & " "
        Sql = Sql & "select tp_registro from seguros_db..emi_proposta_tb Where proposta_id = " & var_proposta_id & " "
        'GENJUNIOR - FLOW 17860335
        'UNION COM TABELAS DE EXPURGO
        '10/10/2013
        Sql = Sql & " UNION "
        Sql = Sql & "select tp_registro from seguros_hist_db.dbo.emi_proposta_hist_tb Where proposta_id = " & var_proposta_id & " "
        'FIM GENJUNIOR
        

        Set rsTipo_Proposta = New Recordset
        Set rsTipo_Proposta = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, Sql, True)

        If Not rsTipo_Proposta.EOF Then
            'exibe valores de acordo com o tipo
            lblTipoProposta.Visible = True
            txtTipoProposta.Visible = True

            If Trim(rsTipo_Proposta("tp_registro")) = "07" Then
                txtTipoProposta.Text = "Renova��o"
            Else
                txtTipoProposta.Text = "Contrata��o"
            End If
        End If

    Else
        lblTipoProposta.Visible = False
        txtTipoProposta.Visible = False
        txtTipoProposta.Text = ""

    End If

    '--Fim de altera��o----------------------------------------------------
  
    Exit Function

TrataErro:
    Call TratarErro("CarregarDadosProposta", Me.name)
    Call FinalizarAplicacao

End Function

Private Sub CarregarNumeroSorte(lPropostaId As Long)

    On Error GoTo TrataErro

    Dim sSql As String
    Dim rsNSorte As Recordset
  
    sSql = ""
    sSql = sSql & "    SELECT numero_sorte "
    sSql = sSql & "      FROM proposta_numero_sorte_tb  WITH (NOLOCK)  "
    sSql = sSql & "     WHERE proposta_id = " & lPropostaId
  
    Set rsNSorte = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, sSql, True)

    If Not rsNSorte.EOF Then
        lblNumeroSorte.Visible = True
        txtCanalVenda.Text = Format(rsNSorte(0), "00,000")
        txtCanalVenda.Visible = True
    End If

    rsNSorte.Close
    Set rsNSorte = Nothing
    Exit Sub

TrataErro:
    Call TratarErro("CarregarNumeroSorte", Me.name)
    Call FinalizarAplicacao

End Sub

Private Sub CarregarDadosCorretores(lPropostaId As Long, _
                                    sSubgrupos_flag As String, _
                                    iSubgrupoId As Integer, _
                                    sDtQuery As String)

    On Error GoTo TrataErro

    Dim rsCorretores As Recordset
    'Dim oCorretor As Corretor
    Dim sLinha As String
  
    Set rsCorretores = New Recordset
    'alterado 02/09/2013 projeto 17860335 - Melhorias no SEGBR MC_Amaral -confitec
      'Call gridCorretor.AddItem("C�d. Corretor      " & vbTab & "Comiss�o     " & vbTab & "Nome                                                                                                 " & vbTab & "Dt. In�cio            ")
    Set rsCorretores = ObterDadosCorretores(iSubgrupoId, lPropostaId, sSubgrupos_flag, Val(txtApolice.Text), Val(txtRamo.Tag), Val(txtSucursal.Tag), Val(txtSeguradora.Tag), sDtQuery)
                                          
    gridCorretor.Clear
    gridCorretor.Rows = 0
    Call gridCorretor.AddItem("C�d. Corretor      " & vbTab & "Comiss�o     " & vbTab & "Nome                                                                                                 " & vbTab & "Dt. In�cio            ")
  
    While Not rsCorretores.EOF
    
        'Set oCorretor = New Corretor
    
        sLinha = ""
        sLinha = sLinha & rsCorretores("corretor_id") & vbTab
        sLinha = sLinha & FormataValorParaPadraoBrasil(rsCorretores("perc_corretagem")) & vbTab
        sLinha = sLinha & rsCorretores("nome") & vbTab
        sLinha = sLinha & Format(rsCorretores("dt_inicio_corretagem"), "dd/mm/yyyy") & vbTab
        'sLinha = sLinha & rsCorretores("corretor_susep") & vbTab
        'sLinha = sLinha & rsCorretores("sucursal_corretor_id") & vbTab
        'sLinha = sLinha & rsCorretores("dt_fim_corretagem") & vbTab
        'sLinha = sLinha & rsCorretores("perc_angariacao") & vbTab
    
        Call gridCorretor.AddItem(sLinha)
    
        rsCorretores.MoveNext
    
    Wend
  
    If gridCorretor.Rows > 1 Then
        gridCorretor.FixedRows = 1
    Else
        gridCorretor.Clear
    End If
  
    rsCorretores.Close
    Set rsCorretores = Nothing
    Exit Sub
  
TrataErro:
    Call TratarErro("CarregarDadosCorretores", Me.name)
    Call FinalizarAplicacao

End Sub

Private Sub CarregarDadosEstipulantes(lPropostaId As Long, _
                                      sSubgrupos_flag As String, _
                                      iSubgrupoId As Integer, _
                                      sDtQuery As String)

    On Error GoTo TrataErro

    Dim rsEstipulantes As Recordset
    Dim sLinha As String
   
    Set rsEstipulantes = New Recordset
    'alterado 02/09/2013 projeto 17860335 - Melhorias no SEGBR MC_Amaral -confitec
    Set rsEstipulantes = ObterDadosEstipulantes(iSubgrupoId, sSubgrupos_flag, lPropostaId, Val(txtApolice.Text), Val(txtRamo.Tag), Val(txtSucursal.Tag), Val(txtSeguradora.Tag), sDtQuery)
                                          
    gridEstipulante.Clear
    gridEstipulante.Rows = 0
    gridEstipulante.AddItem ("% Estipulante   " & vbTab & "Estipulante   " & vbTab & "Nome                                                                                                " & vbTab & "Administrador " & vbTab & "Nome                                             ")
                          
    While Not rsEstipulantes.EOF
    
        '    Set oEstipulante = New Estipulante
        '
        '    oEstipulante.AdministradorId = rsEstipulantes("adm_cliente_id")
        '    oEstipulante.EstipulanteId = rsEstipulantes("est_cliente_id")
        '    oEstipulante.NomeAdministrador = rsEstipulantes("administrador")
        '    oEstipulante.NomeEstipulante = rsEstipulantes("estipulante")
        '    oEstipulante.PercProLabore = rsEstipulantes("perc_pro_labore")

        sLinha = ""
        sLinha = sLinha & FormataValorParaPadraoBrasil(rsEstipulantes("perc_pro_labore")) & vbTab
        sLinha = sLinha & rsEstipulantes("est_cliente_id") & vbTab
        sLinha = sLinha & rsEstipulantes("estipulante") & vbTab
        sLinha = sLinha & rsEstipulantes("adm_cliente_id") & vbTab
        sLinha = sLinha & rsEstipulantes("administrador")
    
        Call gridEstipulante.AddItem(sLinha)
    
        rsEstipulantes.MoveNext
    
    Wend
  
    If gridEstipulante.Rows > 1 Then
        gridEstipulante.FixedRows = 1
    Else
        gridEstipulante.Clear
    End If
  
    rsEstipulantes.Close
    Set rsEstipulantes = Nothing
    Exit Sub
TrataErro:
    Call TratarErro("CarregarDadosEstipulantes", Me.name)
    Call FinalizarAplicacao

End Sub

Private Function ObterDadosEstipulantes(iSubgrupoId As Integer, _
                                        sSubgrupos_flag, _
                                        lPropostaId As Long, _
                                        lApoliceId As Long, _
                                        LramoId As Long, _
                                        lSucursal As Long, _
                                        lSeguradora As Long, _
                                        sDtQuery As String) As Recordset
    On Error GoTo TrataErro

    Dim sSql As String
  
    sSql = ""
  
    If iSubgrupoId = -1 Then
     If sSubgrupos_flag = "Null" Then
                sSql = sSql & " SELECT representacao_proposta_tb.est_cliente_id, " & vbNewLine
                sSql = sSql & "        representacao.nome estipulante, " & vbNewLine
                sSql = sSql & "        administracao_apolice_tb.adm_cliente_id, " & vbNewLine
                sSql = sSql & "        administracao.nome administrador, " & vbNewLine
                sSql = sSql & "        administracao_apolice_tb.perc_pro_labore " & vbNewLine
                sSql = sSql & "   FROM representacao_proposta_tb  WITH (NOLOCK)  " & vbNewLine
                sSql = sSql & "   JOIN cliente_tb representacao  WITH (NOLOCK)  " & vbNewLine
                sSql = sSql & "     ON representacao.cliente_id = representacao_proposta_tb.est_cliente_id " & vbNewLine
                sSql = sSql & "   JOIN administracao_apolice_tb  WITH (NOLOCK)  " & vbNewLine
                sSql = sSql & "     ON administracao_apolice_tb.proposta_id = representacao_proposta_tb.proposta_id " & vbNewLine
                sSql = sSql & "    AND administracao_apolice_tb.dt_inicio_representacao = representacao_proposta_tb.dt_inicio_representacao " & vbNewLine
                sSql = sSql & "    AND administracao_apolice_tb.dt_inicio_administracao <= '" & sDtQuery & "' " & vbNewLine
                sSql = sSql & "    AND ( " & vbNewLine
                sSql = sSql & "           administracao_apolice_tb.dt_fim_administracao is null  " & vbNewLine
                sSql = sSql & "        OR administracao_apolice_tb.dt_fim_administracao >= '" & sDtQuery & "' " & vbNewLine
                sSql = sSql & "        ) " & vbNewLine
                sSql = sSql & "   JOIN cliente_tb administracao  WITH (NOLOCK)  " & vbNewLine
                sSql = sSql & "     ON administracao.cliente_id = administracao_apolice_tb.adm_cliente_id " & vbNewLine
                sSql = sSql & "   JOIN proposta_fechada_tb  WITH (NOLOCK) " & vbNewLine
                sSql = sSql & "     ON proposta_fechada_tb.proposta_id = representacao_proposta_tb.proposta_id" & vbNewLine
                sSql = sSql & "  WHERE proposta_fechada_tb.proposta_id =  " & lPropostaId & vbNewLine
                'Inicio - rfarzat - 10/12/2018 - Sala �gil - Sprint 13 - Altera��o do Estipulante
                'sSql = sSql & "    AND representacao_proposta_tb.dt_inicio_representacao <= '" & sDtQuery & "' " & vbNewLine
                'Fim - rfarzat - 10/12/2018 - Sala �gil - Sprint 13 - Altera��o do Estipulante
                sSql = sSql & "    AND (    " & vbNewLine
                sSql = sSql & "            representacao_proposta_tb.dt_fim_representacao is null  " & vbNewLine
                sSql = sSql & "         OR representacao_proposta_tb.dt_fim_representacao >= '" & sDtQuery & "' " & vbNewLine
                sSql = sSql & "        ) " & vbNewLine
                sSql = sSql & " UNION " & vbNewLine
        
                    sSql = sSql & " SELECT representacao_proposta_tb.est_cliente_id, " & vbNewLine
                    sSql = sSql & "        representacao.nome estipulante, " & vbNewLine
                    sSql = sSql & "        administracao_apolice_tb.adm_cliente_id, " & vbNewLine
                    sSql = sSql & "        administracao.nome administrador, " & vbNewLine
                    sSql = sSql & "        administracao_apolice_tb.perc_pro_labore " & vbNewLine
                    sSql = sSql & "   FROM representacao_proposta_tb   WITH (NOLOCK)  " & vbNewLine
                    sSql = sSql & "   JOIN cliente_tb representacao  WITH (NOLOCK)  " & vbNewLine
                    sSql = sSql & "     ON representacao.cliente_id = representacao_proposta_tb.est_cliente_id " & vbNewLine
                    sSql = sSql & "   JOIN administracao_apolice_tb  WITH (NOLOCK)  " & vbNewLine
                    sSql = sSql & "     ON administracao_apolice_tb.proposta_id = representacao_proposta_tb.proposta_id " & vbNewLine
                    sSql = sSql & "    AND administracao_apolice_tb.dt_inicio_representacao = representacao_proposta_tb.dt_inicio_representacao " & vbNewLine
                    sSql = sSql & "    AND administracao_apolice_tb.dt_inicio_administracao <= '" & sDtQuery & "' " & vbNewLine
                    sSql = sSql & "    AND ( " & vbNewLine
                    sSql = sSql & "           administracao_apolice_tb.dt_fim_administracao is null  " & vbNewLine
                    sSql = sSql & "        OR administracao_apolice_tb.dt_fim_administracao >= '" & sDtQuery & "' " & vbNewLine
                    sSql = sSql & "        ) " & vbNewLine
                    sSql = sSql & "   JOIN cliente_tb administracao  WITH (NOLOCK)  " & vbNewLine
                    sSql = sSql & "     ON administracao.cliente_id = administracao_apolice_tb.adm_cliente_id " & vbNewLine
                    sSql = sSql & "   JOIN apolice_tb  WITH (NOLOCK) " & vbNewLine
                    sSql = sSql & "     ON apolice_tb.proposta_id = representacao_proposta_tb.proposta_id" & vbNewLine
                    sSql = sSql & "   JOIN proposta_adesao_tb  WITH (NOLOCK) " & vbNewLine
                    sSql = sSql & "     ON proposta_adesao_tb.apolice_id = apolice_tb.apolice_id" & vbNewLine
                    sSql = sSql & "    AND proposta_adesao_tb.ramo_id = apolice_tb.ramo_id" & vbNewLine
                    sSql = sSql & "    AND proposta_adesao_tb.sucursal_seguradora_id = apolice_tb.sucursal_seguradora_id" & vbNewLine
                    sSql = sSql & "    AND proposta_adesao_tb.seguradora_cod_susep = apolice_tb.seguradora_cod_susep" & vbNewLine
                    sSql = sSql & "  WHERE proposta_adesao_tb.proposta_id =  " & lPropostaId & vbNewLine
                    'Inicio - rfarzat - 10/12/2018 - Sala �gil - Sprint 13 - Altera��o do Estipulante
                    'sSql = sSql & "    AND representacao_proposta_tb.dt_inicio_representacao <= '" & sDtQuery & "' " & vbNewLine
                    'Fim - rfarzat - 10/12/2018 - Sala �gil - Sprint 13 - Altera��o do Estipulante
                    sSql = sSql & "    AND (    " & vbNewLine
                    sSql = sSql & "            representacao_proposta_tb.dt_fim_representacao is null  " & vbNewLine
                    sSql = sSql & "         OR representacao_proposta_tb.dt_fim_representacao >= '" & sDtQuery & "' " & vbNewLine
                    sSql = sSql & "        ) " & vbNewLine
        Else
            If sSubgrupos_flag = "F" Then
                    sSql = sSql & " SELECT representacao_proposta_tb.est_cliente_id, " & vbNewLine
                    sSql = sSql & "        representacao.nome estipulante, " & vbNewLine
                    sSql = sSql & "        administracao_apolice_tb.adm_cliente_id, " & vbNewLine
                    sSql = sSql & "        administracao.nome administrador, " & vbNewLine
                    sSql = sSql & "        administracao_apolice_tb.perc_pro_labore " & vbNewLine
                    sSql = sSql & "   FROM representacao_proposta_tb  WITH (NOLOCK)  " & vbNewLine
                    sSql = sSql & "   JOIN cliente_tb representacao  WITH (NOLOCK)  " & vbNewLine
                    sSql = sSql & "     ON representacao.cliente_id = representacao_proposta_tb.est_cliente_id " & vbNewLine
                    sSql = sSql & "   JOIN administracao_apolice_tb  WITH (NOLOCK)  " & vbNewLine
                    sSql = sSql & "     ON administracao_apolice_tb.proposta_id = representacao_proposta_tb.proposta_id " & vbNewLine
                    sSql = sSql & "    AND administracao_apolice_tb.dt_inicio_representacao = representacao_proposta_tb.dt_inicio_representacao " & vbNewLine
                    sSql = sSql & "    AND administracao_apolice_tb.dt_inicio_administracao <= '" & sDtQuery & "' " & vbNewLine
                    sSql = sSql & "    AND ( " & vbNewLine
                    sSql = sSql & "           administracao_apolice_tb.dt_fim_administracao is null  " & vbNewLine
                    sSql = sSql & "        OR administracao_apolice_tb.dt_fim_administracao >= '" & sDtQuery & "' " & vbNewLine
                    sSql = sSql & "        ) " & vbNewLine
                    sSql = sSql & "   JOIN cliente_tb administracao  WITH (NOLOCK)  " & vbNewLine
                    sSql = sSql & "     ON administracao.cliente_id = administracao_apolice_tb.adm_cliente_id " & vbNewLine
                    sSql = sSql & "   JOIN proposta_fechada_tb  WITH (NOLOCK) " & vbNewLine
                    sSql = sSql & "     ON proposta_fechada_tb.proposta_id = representacao_proposta_tb.proposta_id" & vbNewLine
                    sSql = sSql & "  WHERE proposta_fechada_tb.proposta_id =  " & lPropostaId & vbNewLine
                    'Inicio - rfarzat - 10/12/2018 - Sala �gil - Sprint 13 - Altera��o do Estipulante
                    'sSql = sSql & "    AND representacao_proposta_tb.dt_inicio_representacao <= '" & sDtQuery & "' " & vbNewLine
                    'Fim - rfarzat - 10/12/2018 - Sala �gil - Sprint 13 - Altera��o do Estipulante
                    sSql = sSql & "    AND (    " & vbNewLine
                    sSql = sSql & "            representacao_proposta_tb.dt_fim_representacao is null  " & vbNewLine
                    sSql = sSql & "         OR representacao_proposta_tb.dt_fim_representacao >= '" & sDtQuery & "' " & vbNewLine
                    sSql = sSql & "        ) " & vbNewLine
        'sSql = sSql & " UNION " & vbNewLine
        Else
            sSql = sSql & " SELECT representacao_proposta_tb.est_cliente_id, " & vbNewLine
            sSql = sSql & "        representacao.nome estipulante, " & vbNewLine
            sSql = sSql & "        administracao_apolice_tb.adm_cliente_id, " & vbNewLine
            sSql = sSql & "        administracao.nome administrador, " & vbNewLine
            sSql = sSql & "        administracao_apolice_tb.perc_pro_labore " & vbNewLine
            sSql = sSql & "   FROM representacao_proposta_tb   WITH (NOLOCK)  " & vbNewLine
            sSql = sSql & "   JOIN cliente_tb representacao  WITH (NOLOCK)  " & vbNewLine
            sSql = sSql & "     ON representacao.cliente_id = representacao_proposta_tb.est_cliente_id " & vbNewLine
            sSql = sSql & "   JOIN administracao_apolice_tb  WITH (NOLOCK)  " & vbNewLine
            sSql = sSql & "     ON administracao_apolice_tb.proposta_id = representacao_proposta_tb.proposta_id " & vbNewLine
            sSql = sSql & "    AND administracao_apolice_tb.dt_inicio_representacao = representacao_proposta_tb.dt_inicio_representacao " & vbNewLine
            'sSql = sSql & "    AND administracao_apolice_tb.dt_inicio_administracao <= '" & sDtQuery & "' " & vbNewLine
            sSql = sSql & "    AND ( " & vbNewLine
            sSql = sSql & "           administracao_apolice_tb.dt_fim_administracao is null  " & vbNewLine
            sSql = sSql & "        OR administracao_apolice_tb.dt_fim_administracao >= '" & sDtQuery & "' " & vbNewLine
            sSql = sSql & "        ) " & vbNewLine
            sSql = sSql & "   JOIN cliente_tb administracao  WITH (NOLOCK)  " & vbNewLine
            sSql = sSql & "     ON administracao.cliente_id = administracao_apolice_tb.adm_cliente_id " & vbNewLine
            sSql = sSql & "   JOIN apolice_tb  WITH (NOLOCK) " & vbNewLine
            sSql = sSql & "     ON apolice_tb.proposta_id = representacao_proposta_tb.proposta_id" & vbNewLine
            sSql = sSql & "   JOIN proposta_adesao_tb  WITH (NOLOCK) " & vbNewLine
            sSql = sSql & "     ON proposta_adesao_tb.apolice_id = apolice_tb.apolice_id" & vbNewLine
            sSql = sSql & "    AND proposta_adesao_tb.ramo_id = apolice_tb.ramo_id" & vbNewLine
            sSql = sSql & "    AND proposta_adesao_tb.sucursal_seguradora_id = apolice_tb.sucursal_seguradora_id" & vbNewLine
            sSql = sSql & "    AND proposta_adesao_tb.seguradora_cod_susep = apolice_tb.seguradora_cod_susep" & vbNewLine
            sSql = sSql & "  WHERE proposta_adesao_tb.proposta_id =  " & lPropostaId & vbNewLine
            'Inicio - rfarzat - 10/12/2018 - Sala �gil - Sprint 13 - Altera��o do Estipulante
            'sSql = sSql & "    AND representacao_proposta_tb.dt_inicio_representacao <= '" & sDtQuery & "' " & vbNewLine
            'Fim - rfarzat - 10/12/2018 - Sala �gil - Sprint 13 - Altera��o do Estipulante
            sSql = sSql & "    AND (    " & vbNewLine
            sSql = sSql & "            representacao_proposta_tb.dt_fim_representacao is null  " & vbNewLine
            sSql = sSql & "         OR representacao_proposta_tb.dt_fim_representacao >= '" & sDtQuery & "' " & vbNewLine
            sSql = sSql & "        ) " & vbNewLine
         End If
        End If

    Else
        sSql = sSql & " SELECT adm_sub_grupo_apolice_tb.sub_grupo_id, " & vbNewLine
        sSql = sSql & "        representacao_sub_grupo_tb.est_cliente_id, " & vbNewLine
        sSql = sSql & "        representacao.nome estipulante, " & vbNewLine
        sSql = sSql & "        adm_sub_grupo_apolice_tb.adm_cliente_id,  " & vbNewLine
        sSql = sSql & "        administracao.nome administrador, " & vbNewLine
        sSql = sSql & "        adm_sub_grupo_apolice_tb.perc_pro_labore " & vbNewLine
        sSql = sSql & "   FROM representacao_sub_grupo_tb   WITH (NOLOCK)   " & vbNewLine
        sSql = sSql & "   JOIN cliente_tb representacao   WITH (NOLOCK)   " & vbNewLine
        sSql = sSql & "     ON representacao.cliente_id = representacao_sub_grupo_tb.est_cliente_id " & vbNewLine
        sSql = sSql & "   JOIN adm_sub_grupo_apolice_tb  WITH (NOLOCK)  " & vbNewLine
        sSql = sSql & "     ON adm_sub_grupo_apolice_tb.est_cliente_id = representacao_sub_grupo_tb.est_cliente_id " & vbNewLine
        sSql = sSql & "    AND adm_sub_grupo_apolice_tb.dt_fim_administracao is null  " & vbNewLine
        sSql = sSql & "    AND adm_sub_grupo_apolice_tb.apolice_id              = representacao_sub_grupo_tb.apolice_id " & vbNewLine
        sSql = sSql & "    AND adm_sub_grupo_apolice_tb.sucursal_seguradora_id  = representacao_sub_grupo_tb.sucursal_seguradora_id " & vbNewLine
        sSql = sSql & "    AND adm_sub_grupo_apolice_tb.seguradora_cod_susep    = representacao_sub_grupo_tb.seguradora_cod_susep " & vbNewLine
        sSql = sSql & "    AND adm_sub_grupo_apolice_tb.ramo_id                 = representacao_sub_grupo_tb.ramo_id " & vbNewLine
        sSql = sSql & "    AND adm_sub_grupo_apolice_tb.dt_inicio_representacao = representacao_sub_grupo_tb.dt_inicio_representacao " & vbNewLine
        sSql = sSql & "   JOIN cliente_tb administracao  WITH (NOLOCK)  " & vbNewLine
        sSql = sSql & "     ON administracao.cliente_id = adm_sub_grupo_apolice_tb.adm_cliente_id " & vbNewLine
        sSql = sSql & "  WHERE representacao_sub_grupo_tb.apolice_id =  " & lApoliceId & vbNewLine
        sSql = sSql & "    AND representacao_sub_grupo_tb.sucursal_seguradora_id =" & lSucursal & vbNewLine
        sSql = sSql & "    AND representacao_sub_grupo_tb.seguradora_cod_susep =" & lSeguradora & vbNewLine
        sSql = sSql & "    AND representacao_sub_grupo_tb.ramo_id =  " & LramoId & vbNewLine
        'Inicio - rfarzat - 10/12/2018 - Sala �gil - Sprint 13 - Altera��o do Estipulante
        'sSql = sSql & "    AND representacao_sub_grupo_tb.dt_inicio_representacao <= '" & sDtQuery & "' " & vbNewLine
        'Fim - rfarzat - 10/12/2018 - Sala �gil - Sprint 13 - Altera��o do Estipulante
        sSql = sSql & "    AND ( " & vbNewLine
        sSql = sSql & "           representacao_sub_grupo_tb.dt_fim_representacao is null  " & vbNewLine
        sSql = sSql & "        OR representacao_sub_grupo_tb.dt_fim_representacao >= '" & sDtQuery & "' " & vbNewLine
        sSql = sSql & "        ) " & vbNewLine
        sSql = sSql & "    AND representacao_sub_grupo_tb.sub_grupo_id = " & iSubgrupoId & vbNewLine
    End If

    Set ObterDadosEstipulantes = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, sSql, True)

    Exit Function
TrataErro:
    Call TratarErro("CarregarDadosEstipulantes", Me.name)
    Call FinalizarAplicacao
                                        
End Function

Private Function ObterDadosCorretores(iSubgrupoId As Integer, _
                                      lPropostaId As Long, _
                                      sSubgrupos_flag As String, _
                                      lApoliceId As Long, _
                                      LramoId As Long, _
                                      lSucursalId As Long, _
                                      lSeguradoraId As Long, _
                                      sDtQuery As String) As Recordset

    On Error GoTo TrataErro

    Dim sSql As String
  
    sSql = ""
  
    If iSubgrupoId = -1 Then
         If sSubgrupos_flag = "Null" Then
         
         'INICIO  alterado 02/09/2013 projeto 17860335 - Melhorias no SEGBR MC_Amaral -confitec
           ' sSql = sSql & "   SELECT  corretor_susep = isnull(ccpj.corretor_susep,ccpf.corretor_susep),          " & vbNewLine
            sSql = sSql & "   SELECT  corretor_susep = corr.corretor_susep,          " & vbNewLine
           ' sSql = sSql & "           corretor_id    = isnull(ccpj.corretor_id,ccpf.corretor_id),                " & vbNewLine
            sSql = sSql & "           corretor_id    =  corr.corretor_id ,                                       " & vbNewLine
            sSql = sSql & "           sucursal_corretor_id = '',                                                 " & vbNewLine
            'sSql = sSql & "          nome = isnull(ccpf.nome,ccpj.nome),                                       " & vbNewLine
            sSql = sSql & "           nome = corr.nome,                                                          " & vbNewLine
            sSql = sSql & "           ca.dt_inicio_corretagem,                                                   " & vbNewLine
            sSql = sSql & "           ca.dt_fim_corretagem,                                                      " & vbNewLine
            sSql = sSql & "           perc_corretagem = isnull(cpf.perc_corretagem,cpj.perc_corretagem) ,        " & vbNewLine
            sSql = sSql & "           perc_angariacao = isnull(cpf.perc_angariacao,cpj.perc_angariacao)          " & vbNewLine
           ' sSql = sSql & "      FROM corretagem_tb ca  WITH (NOLOCK)                                               " & vbNewLine
            sSql = sSql & "      FROM corretagem_tb ca  WITH (NOLOCK)  " & vbNewLine
            sSql = sSql & "      INNER JOIN  corretor_tb corr  WITH (NOLOCK)  " & vbNewLine
            sSql = sSql & "      ON corr.corretor_id = ca.corretor_id  " & vbNewLine
    
            sSql = sSql & " LEFT JOIN corretagem_pf_tb cpf  WITH (NOLOCK)                                           " & vbNewLine
            sSql = sSql & "        ON cpf.proposta_id = ca.proposta_id                                           " & vbNewLine
            sSql = sSql & "       AND cpf.corretor_id = ca.corretor_id                                           " & vbNewLine
            sSql = sSql & "       AND cpf.dt_inicio_corretagem = ca.dt_inicio_corretagem                         " & vbNewLine
    '        sSql = sSql & " LEFT JOIN corretor_tb ccpf  WITH (NOLOCK)                                               " & vbNewLine
    '        sSql = sSql & "        ON ccpf.corretor_id = ca.corretor_id                                          " & vbNewLine
            
            sSql = sSql & " LEFT JOIN corretagem_pj_tb cpj  WITH (NOLOCK)                                           " & vbNewLine
            sSql = sSql & "        ON cpj.proposta_id = ca.proposta_id                                           " & vbNewLine
            sSql = sSql & "       AND cpj.corretor_id = ca.corretor_id                                           " & vbNewLine
            sSql = sSql & "       AND cpj.dt_inicio_corretagem = ca.dt_inicio_corretagem                         " & vbNewLine
    '        sSql = sSql & " LEFT JOIN corretor_tb ccpj  WITH (NOLOCK)                                               " & vbNewLine
    '        sSql = sSql & "        ON ccpj.corretor_id = ca.corretor_id                                          " & vbNewLine
           'FIM  alterado 02/09/2013 projeto 17860335 - Melhorias no SEGBR MC_Amaral -confitec
            sSql = sSql & "      JOIN proposta_fechada_tb pfc  WITH (NOLOCK)                                        " & vbNewLine
            sSql = sSql & "        ON pfc.proposta_id = ca.proposta_id                                           " & vbNewLine
            sSql = sSql & "     WHERE ca.proposta_id = " & lPropostaId & vbNewLine
            sSql = sSql & "       AND ca.dt_inicio_corretagem <= '" & sDtQuery & "'                              " & vbNewLine
            sSql = sSql & "       AND (ca.dt_fim_corretagem is null                                              " & vbNewLine
            sSql = sSql & "            OR ca.dt_fim_corretagem >= '" & sDtQuery & "')                            " & vbNewLine
            sSql = sSql & " UNION   'FIM  alterado 02/09/2013 projeto 17860335 - Melhorias no SEGBR MC_Amaral -confitec                                                                             " & vbNewLine
    
            sSql = sSql & "   SELECT  corretor_susep = isnull(ccpj.corretor_susep,ccpf.corretor_susep),          " & vbNewLine
            sSql = sSql & "           corretor_id    = isnull(ccpj.corretor_id,ccpf.corretor_id),                " & vbNewLine
            sSql = sSql & "           sucursal_corretor_id = '',                                                 " & vbNewLine
            sSql = sSql & "           nome = isnull(ccpf.nome,ccpj.nome),                                        " & vbNewLine
            sSql = sSql & "           ca.dt_inicio_corretagem,                                                   " & vbNewLine
            sSql = sSql & "           ca.dt_fim_corretagem,                                                      " & vbNewLine
            sSql = sSql & "           perc_corretagem = isnull(cpf.perc_corretagem,cpj.perc_corretagem) ,        " & vbNewLine
            sSql = sSql & "           perc_angariacao = isnull(cpf.perc_angariacao,cpj.perc_angariacao)          " & vbNewLine
            sSql = sSql & "      FROM corretagem_tb ca  WITH (NOLOCK)                                               " & vbNewLine
            sSql = sSql & " LEFT JOIN corretagem_pf_tb cpf  WITH (NOLOCK)                                           " & vbNewLine
            sSql = sSql & "        ON cpf.proposta_id = ca.proposta_id                                           " & vbNewLine
            sSql = sSql & "       AND cpf.corretor_id = ca.corretor_id                                           " & vbNewLine
            sSql = sSql & "       AND cpf.dt_inicio_corretagem = ca.dt_inicio_corretagem                         " & vbNewLine
            sSql = sSql & " LEFT JOIN corretor_tb ccpf  WITH (NOLOCK)                                               " & vbNewLine
            sSql = sSql & "        ON ccpf.corretor_id = ca.corretor_id                                          " & vbNewLine
            sSql = sSql & " LEFT JOIN corretagem_pj_tb cpj  WITH (NOLOCK)                                           " & vbNewLine
            sSql = sSql & "        ON cpj.proposta_id = ca.proposta_id                                           " & vbNewLine
            sSql = sSql & "       AND cpj.corretor_id = ca.corretor_id                                           " & vbNewLine
            sSql = sSql & "       AND cpj.dt_inicio_corretagem = ca.dt_inicio_corretagem                         " & vbNewLine
            sSql = sSql & " LEFT JOIN corretor_tb ccpj  WITH (NOLOCK)                                               " & vbNewLine
            sSql = sSql & "        ON ccpj.corretor_id = ca.corretor_id                                          " & vbNewLine
            sSql = sSql & "      JOIN proposta_adesao_tb pad  WITH (NOLOCK)                                         " & vbNewLine
            sSql = sSql & "        ON pad.proposta_id = ca.proposta_id                                           " & vbNewLine
            sSql = sSql & "     WHERE pad.proposta_id = " & lPropostaId & vbNewLine
            sSql = sSql & "       AND ca.dt_inicio_corretagem <= '" & sDtQuery & "'                              " & vbNewLine
            sSql = sSql & "       AND (ca.dt_fim_corretagem is null                                              " & vbNewLine
            sSql = sSql & "            OR ca.dt_fim_corretagem >= '" & sDtQuery & "')                            " & vbNewLine
            sSql = sSql & "  ORDER BY c.corretor_id                                                              " & vbNewLine
    Else
    
     If sSubgrupos_flag = "F" Then
    
        'INICIO  alterado 02/09/2013 projeto 17860335 - Melhorias no SEGBR MC_Amaral -confitec
           ' sSql = sSql & "   SELECT  corretor_susep = isnull(ccpj.corretor_susep,ccpf.corretor_susep),          " & vbNewLine
            sSql = sSql & "   SELECT  corretor_susep = corr.corretor_susep,          " & vbNewLine
           ' sSql = sSql & "           corretor_id    = isnull(ccpj.corretor_id,ccpf.corretor_id),                " & vbNewLine
            sSql = sSql & "           corretor_id    =  corr.corretor_id ,                                       " & vbNewLine
            sSql = sSql & "           sucursal_corretor_id = '',                                                 " & vbNewLine
            'sSql = sSql & "          nome = isnull(ccpf.nome,ccpj.nome),                                       " & vbNewLine
            sSql = sSql & "           nome = corr.nome,                                                          " & vbNewLine
            sSql = sSql & "           ca.dt_inicio_corretagem,                                                   " & vbNewLine
            sSql = sSql & "           ca.dt_fim_corretagem,                                                      " & vbNewLine
            sSql = sSql & "           perc_corretagem = isnull(cpf.perc_corretagem,cpj.perc_corretagem) ,        " & vbNewLine
            sSql = sSql & "           perc_angariacao = isnull(cpf.perc_angariacao,cpj.perc_angariacao)          " & vbNewLine
           ' sSql = sSql & "      FROM corretagem_tb ca  WITH (NOLOCK)                                               " & vbNewLine
            sSql = sSql & "      FROM corretagem_tb ca  WITH (NOLOCK)  " & vbNewLine
            sSql = sSql & "      INNER JOIN  corretor_tb corr  WITH (NOLOCK)  " & vbNewLine
            sSql = sSql & "      ON corr.corretor_id = ca.corretor_id  " & vbNewLine
    
            sSql = sSql & " LEFT JOIN corretagem_pf_tb cpf  WITH (NOLOCK)                                           " & vbNewLine
            sSql = sSql & "        ON cpf.proposta_id = ca.proposta_id                                           " & vbNewLine
            sSql = sSql & "       AND cpf.corretor_id = ca.corretor_id                                           " & vbNewLine
            sSql = sSql & "       AND cpf.dt_inicio_corretagem = ca.dt_inicio_corretagem                         " & vbNewLine
    '        sSql = sSql & " LEFT JOIN corretor_tb ccpf  WITH (NOLOCK)                                               " & vbNewLine
    '        sSql = sSql & "        ON ccpf.corretor_id = ca.corretor_id                                          " & vbNewLine
            
            sSql = sSql & " LEFT JOIN corretagem_pj_tb cpj  WITH (NOLOCK)                                           " & vbNewLine
            sSql = sSql & "        ON cpj.proposta_id = ca.proposta_id                                           " & vbNewLine
            sSql = sSql & "       AND cpj.corretor_id = ca.corretor_id                                           " & vbNewLine
            sSql = sSql & "       AND cpj.dt_inicio_corretagem = ca.dt_inicio_corretagem                         " & vbNewLine
    '        sSql = sSql & " LEFT JOIN corretor_tb ccpj  WITH (NOLOCK)                                               " & vbNewLine
    '        sSql = sSql & "        ON ccpj.corretor_id = ca.corretor_id                                          " & vbNewLine
           'FIM  alterado 02/09/2013 projeto 17860335 - Melhorias no SEGBR MC_Amaral -confitec
            sSql = sSql & "      JOIN proposta_fechada_tb pfc  WITH (NOLOCK)                                        " & vbNewLine
            sSql = sSql & "        ON pfc.proposta_id = ca.proposta_id                                           " & vbNewLine
            sSql = sSql & "     WHERE ca.proposta_id = " & lPropostaId & vbNewLine
            sSql = sSql & "       AND ca.dt_inicio_corretagem <= '" & sDtQuery & "'                              " & vbNewLine
            sSql = sSql & "       AND (ca.dt_fim_corretagem is null                                              " & vbNewLine
            sSql = sSql & "            OR ca.dt_fim_corretagem >= '" & sDtQuery & "')                            " & vbNewLine
    '        sSql = sSql & " UNION   'FIM  alterado 02/09/2013 projeto 17860335 - Melhorias no SEGBR MC_Amaral -confitec                                                                             " & vbNewLine
        Else
            sSql = sSql & "   SELECT  corretor_susep = isnull(ccpj.corretor_susep,ccpf.corretor_susep),          " & vbNewLine
            sSql = sSql & "           corretor_id    = isnull(ccpj.corretor_id,ccpf.corretor_id),                " & vbNewLine
            sSql = sSql & "           sucursal_corretor_id = '',                                                 " & vbNewLine
            sSql = sSql & "           nome = isnull(ccpf.nome,ccpj.nome),                                        " & vbNewLine
            sSql = sSql & "           ca.dt_inicio_corretagem,                                                   " & vbNewLine
            sSql = sSql & "           ca.dt_fim_corretagem,                                                      " & vbNewLine
            sSql = sSql & "           perc_corretagem = isnull(cpf.perc_corretagem,cpj.perc_corretagem) ,        " & vbNewLine
            sSql = sSql & "           perc_angariacao = isnull(cpf.perc_angariacao,cpj.perc_angariacao)          " & vbNewLine
            sSql = sSql & "      FROM corretagem_tb ca  WITH (NOLOCK)                                               " & vbNewLine
            sSql = sSql & " LEFT JOIN corretagem_pf_tb cpf  WITH (NOLOCK)                                           " & vbNewLine
            sSql = sSql & "        ON cpf.proposta_id = ca.proposta_id                                           " & vbNewLine
            sSql = sSql & "       AND cpf.corretor_id = ca.corretor_id                                           " & vbNewLine
            sSql = sSql & "       AND cpf.dt_inicio_corretagem = ca.dt_inicio_corretagem                         " & vbNewLine
            sSql = sSql & " LEFT JOIN corretor_tb ccpf  WITH (NOLOCK)                                               " & vbNewLine
            sSql = sSql & "        ON ccpf.corretor_id = ca.corretor_id                                          " & vbNewLine
            sSql = sSql & " LEFT JOIN corretagem_pj_tb cpj  WITH (NOLOCK)                                           " & vbNewLine
            sSql = sSql & "        ON cpj.proposta_id = ca.proposta_id                                           " & vbNewLine
            sSql = sSql & "       AND cpj.corretor_id = ca.corretor_id                                           " & vbNewLine
            sSql = sSql & "       AND cpj.dt_inicio_corretagem = ca.dt_inicio_corretagem                         " & vbNewLine
            sSql = sSql & " LEFT JOIN corretor_tb ccpj  WITH (NOLOCK)                                               " & vbNewLine
            sSql = sSql & "        ON ccpj.corretor_id = ca.corretor_id                                          " & vbNewLine
            sSql = sSql & "      JOIN proposta_adesao_tb pad  WITH (NOLOCK)                                         " & vbNewLine
            sSql = sSql & "        ON pad.proposta_id = ca.proposta_id                                           " & vbNewLine
            sSql = sSql & "     WHERE pad.proposta_id = " & lPropostaId & vbNewLine
            sSql = sSql & "       AND ca.dt_inicio_corretagem <= '" & sDtQuery & "'                              " & vbNewLine
            sSql = sSql & "       AND (ca.dt_fim_corretagem is null                                              " & vbNewLine
            sSql = sSql & "            OR ca.dt_fim_corretagem >= '" & sDtQuery & "')                            " & vbNewLine
            sSql = sSql & "  ORDER BY corretor_id                                                              " & vbNewLine
            End If
        End If
    Else

        sSql = sSql & "SELECT cs.sub_grupo_id, c.corretor_susep, c.corretor_id, sucursal_corretor_id = ''"
        sSql = sSql & ", c.nome, cs.dt_inicio_corretagem, cs.dt_fim_corretagem"
        sSql = sSql & ", cpf.perc_corretagem , cpf.perc_angariacao "
        sSql = sSql & "FROM corretagem_sub_grupo_tb cs  WITH (NOLOCK) , corretagem_pf_sub_grupo_tb cpf   WITH (NOLOCK) , corretor_tb c   WITH (NOLOCK)  "
        sSql = sSql & "WHERE cs.apolice_id = " & lApoliceId
        sSql = sSql & " AND cs.sucursal_seguradora_id = " & lSucursalId
        sSql = sSql & " AND cs.seguradora_cod_susep = " & lSeguradoraId
        sSql = sSql & " AND cs.ramo_id = " & LramoId
        sSql = sSql & " AND cs.dt_inicio_corretagem <= '" & sDtQuery & "'"
        sSql = sSql & " AND (cs.dt_fim_corretagem is null "
        sSql = sSql & "     OR cs.dt_fim_corretagem >= '" & sDtQuery & "')"
        sSql = sSql & " AND cpf.apolice_id = cs.apolice_id"
        sSql = sSql & " AND cpf.sucursal_seguradora_id = cs.sucursal_seguradora_id"
        sSql = sSql & " AND cpf.seguradora_cod_susep = cs.seguradora_cod_susep"
        sSql = sSql & " AND cpf.ramo_id = cs.ramo_id"
        sSql = sSql & " AND cpf.sub_grupo_id = cs.sub_grupo_id"
        sSql = sSql & " AND cpf.corretor_id = cs.corretor_id"
        sSql = sSql & " AND cpf.dt_inicio_corretagem = cs.dt_inicio_corretagem"
        sSql = sSql & " AND c.corretor_id = cs.corretor_id "
        sSql = sSql & " UNION "
        sSql = sSql & "SELECT cs.sub_grupo_id, c.corretor_susep, c.corretor_id, cpj.sucursal_corretor_id" 'Alterado por Gustavo Machado em 10/11/2003
        sSql = sSql & ", c.nome, cs.dt_inicio_corretagem, cs.dt_fim_corretagem"
        sSql = sSql & ", cpj.perc_corretagem , cpj.perc_angariacao "
        sSql = sSql & "FROM corretagem_sub_grupo_tb cs   WITH (NOLOCK) , corretagem_pj_sub_grupo_tb cpj    WITH (NOLOCK) , corretor_tb c   WITH (NOLOCK)  "
        sSql = sSql & "WHERE cs.apolice_id = " & lApoliceId
        sSql = sSql & " AND cs.sub_grupo_id = " & iSubgrupoId
        sSql = sSql & " AND cs.sucursal_seguradora_id = " & lSucursalId
        sSql = sSql & " AND cs.seguradora_cod_susep = " & lSeguradoraId
        sSql = sSql & " AND cs.ramo_id = " & LramoId
        sSql = sSql & " AND cs.dt_inicio_corretagem <= '" & sDtQuery & "'"
        sSql = sSql & " AND (cs.dt_fim_corretagem is null "
        sSql = sSql & "     OR cs.dt_fim_corretagem > = '" & sDtQuery & "')"
        sSql = sSql & " AND cpj.apolice_id = cs.apolice_id"
        sSql = sSql & " AND cpj.sucursal_seguradora_id = cs.sucursal_seguradora_id"
        sSql = sSql & " AND cpj.seguradora_cod_susep = cs.seguradora_cod_susep"
        sSql = sSql & " AND cpj.ramo_id = cs.ramo_id"
        sSql = sSql & " AND cpj.sub_grupo_id = cs.sub_grupo_id"
        sSql = sSql & " AND cpj.corretor_id = cs.corretor_id"
        sSql = sSql & " AND cpj.dt_inicio_corretagem = cs.dt_inicio_corretagem "
        sSql = sSql & " AND c.corretor_id = cs.corretor_id "
        sSql = sSql & " ORDER BY cs.sub_grupo_id, c.corretor_id"
    End If
  
    Set ObterDadosCorretores = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, sSql, True)

    Exit Function
TrataErro:
    Call TratarErro("CarregarDadosEstipulantes", Me.name)
    Call FinalizarAplicacao
  
End Function

Private Sub CarregarComboSubgrupo(cboCombo As ComboBox)

    Dim oSubgrupo As SubGrupo
  
    For Each oSubgrupo In colSubgrupos
        Call cboCombo.AddItem(oSubgrupo.Id & " - " & oSubgrupo.Nome)
        cboCombo.ItemData(cboCombo.NewIndex) = oSubgrupo.Id
    Next
  
    cboCombo.ListIndex = 0

End Sub

Private Function ObterDadosCapitalPremio(lPropostaId As Long, _
                                         lProdutoId As Long, _
                                         lApoliceId As Long, _
                                         LramoId As Long, _
                                         lSucursalId As Long, _
                                         lSeguradoraId As Long, _
                                         iSubgrupoId As Integer, _
                                         sDtQuery As String) As Recordset

    On Error GoTo TrataErro

    Dim sSql As String
    Dim rsTemp As Recordset
  
    Set rsTemp = New Recordset
    
'+----------------------------------------
'| Migra��o SQL - Melhoria de performance
'| petrauskas 08/2016
'|
    sSql = "SET NOCOUNT ON  exec SEGS13063_SPS " & lPropostaId & ", '" & Format(Data_Sistema, "yyyymmdd") & "'"
    
'    'Lsa - 14/09/2005
'    'Alterado para utilizar a vig�ncia de escolha_plano_tb (dt_escolha)
'    sSql = ""
'    adSQL sSql, "SELECT escolha_plano_tb.imp_segurada,"
'    adSQL sSql, "       escolha_plano_tb.val_premio,"
'    adSQL sSql, "       escolha_plano_tb.plano_id,"
'    adSQL sSql, "       nome_plano = plano_tb.nome,"
'    adSQL sSql, "       qtd_salarios = IsNull(escolha_plano_tb.qtd_salarios,0) "
'    adSQL sSql, "FROM proposta_tb"
'    adSQL sSql, "JOIN escolha_plano_tb"
'    adSQL sSql, "  ON escolha_plano_tb.proposta_id = proposta_tb.proposta_id"
'    'adSQL sSql, " AND escolha_plano_tb.dt_escolha <= " & "'" & Format(Data_Sistema, "yyyymmdd") & "'" -- Gabriel C�mara - 31/01/2012 - INC000003298687
'    adSQL sSql, "JOIN plano_tb  WITH (NOLOCK) "
'    adSQL sSql, "  ON plano_tb.plano_id = escolha_plano_tb.plano_id"
'    adSQL sSql, " AND plano_tb.dt_inicio_vigencia = escolha_plano_tb.dt_inicio_vigencia"
'    adSQL sSql, " AND plano_tb.produto_id = escolha_plano_tb.produto_id"
'
'    'alterado em 26/10/2006, por Leandro A. Souza (Stefanini IT Solutions) - flow 177787
'    'alterado em 28/03/2007, por Alexandre Queiroz(STF) - flow 205599
'    'est� sendo refeita a cl�usula "where" desenvolvida pelo Leandro
'    '
'    'Leandro: adSQL sSQL, " WHERE escolha_plano_tb.lock = (select max(lock) from escolha_plano_tb  WITH (NOLOCK)  where proposta_id = " & lPropostaId & ") "
'    '
'    'abaixo, a nova cl�usula "where"
'    'gabriel.camara - 30/08/2011 - Confitec - Flow 12121133 - Valor Capital Segurado
'    'adSQL sSql, " WHERE escolha_plano_tb.lock = (select top 1 lock from escolha_plano_tb  WITH (NOLOCK)  where proposta_id = " & lPropostaId & " order by dt_escolha desc) "
'
'    'jessica.adao - 27/07/2011 - Confitec - FLOW 11887013 - Diverg�ncia de valores
'    'adSQL sSql, " WHERE escolha_plano_tb.lock = (select max(lock) from escolha_plano_tb  WITH (NOLOCK)  where proposta_id = " & lPropostaId & ") "
'
'    'Gabriel C�mara - 31/01/2012 - INC000003298687
'        adSQL sSql, " WHERE escolha_plano_tb.lock = (select top(1) lock from escolha_plano_tb  WITH (NOLOCK)  where proposta_id = " & lPropostaId & " AND escolha_plano_tb.dt_escolha <= " & "'" & Format(Data_Sistema, "yyyymmdd") & "' order by dt_escolha desc) "
'| Migra��o SQL - Melhoria de performance
'+----------------------------------------
        
    Set rsTemp = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, sSql, True)
                             
    If Not rsTemp.EOF Then
        Set ObterDadosCapitalPremio = rsTemp
        Exit Function
    End If
    
    sSql = ""
    sSql = sSql & "    SELECT DISTINCT " & vbNewLine
    sSql = sSql & "           imp_segurada = val_is, " & vbNewLine
    sSql = sSql & "           val_premio,  " & vbNewLine
    sSql = sSql & "           plano_id = 0,  " & vbNewLine
    sSql = sSql & "           nome_plano = '',  " & vbNewLine
    sSql = sSql & "           qtd_salarios = 0  " & vbNewLine
    sSql = sSql & "      FROM escolha_tp_cob_vida_tb  WITH (NOLOCK)  " & vbNewLine
    sSql = sSql & "      JOIN proposta_tb  WITH (NOLOCK)  " & vbNewLine
    sSql = sSql & "        ON proposta_tb.proposta_id = escolha_tp_cob_vida_tb.proposta_id " & vbNewLine
    sSql = sSql & "      JOIN tp_cob_comp_item_tb  WITH (NOLOCK)  " & vbNewLine
    sSql = sSql & "        ON tp_cob_comp_item_tb.tp_cob_comp_id = escolha_tp_cob_vida_tb.tp_cob_comp_id " & vbNewLine
    sSql = sSql & "       AND tp_cob_comp_item_tb.dt_inicio_vigencia_comp = escolha_tp_cob_vida_tb.dt_inicio_vigencia_comp " & vbNewLine
    sSql = sSql & "       AND tp_cob_comp_item_tb.produto_id = proposta_tb.produto_id " & vbNewLine
    sSql = sSql & "       AND tp_cob_comp_item_tb.ramo_id = proposta_tb.ramo_id " & vbNewLine
    'sSql = sSql & "       AND tp_cob_comp_item_tb.dt_fim_vigencia_comp is null " & vbNewLine 'IM00980208 Thalita 22/10/2019
    sSql = sSql & "      JOIN tp_cob_comp_tb  WITH (NOLOCK)  " & vbNewLine
    sSql = sSql & "        ON tp_cob_comp_tb.tp_cob_comp_id = escolha_tp_cob_vida_tb.tp_cob_comp_id " & vbNewLine
    sSql = sSql & "      JOIN seguro_vida_tb  WITH (NOLOCK)  " & vbNewLine
    sSql = sSql & "        ON seguro_vida_tb.proposta_id            = escolha_tp_cob_vida_tb.proposta_id " & vbNewLine
    sSql = sSql & "       AND seguro_vida_tb.prop_cliente_id        = escolha_tp_cob_vida_tb.prop_cliente_id " & vbNewLine
    sSql = sSql & "       AND seguro_vida_tb.tp_componente_id       = escolha_tp_cob_vida_tb.tp_componente_id " & vbNewLine
    sSql = sSql & "       AND seguro_vida_tb.seq_canc_endosso_seg   = escolha_tp_cob_vida_tb.seq_canc_endosso_seg " & vbNewLine
    sSql = sSql & "       AND seguro_vida_tb.dt_inicio_vigencia_seg = escolha_tp_cob_vida_tb.dt_inicio_vigencia_seg " & vbNewLine
    sSql = sSql & "       AND seguro_vida_tb.dt_fim_vigencia_seg IS NULL " & vbNewLine
    sSql = sSql & "     WHERE escolha_tp_cob_vida_tb.tp_componente_id = 1 -- titular " & vbNewLine
    sSql = sSql & "       AND escolha_tp_cob_vida_tb.dt_fim_vigencia_esc IS NULL " & vbNewLine
    sSql = sSql & "       AND tp_cob_comp_tb.class_tp_cobertura = 'b' " & vbNewLine
    sSql = sSql & "       AND proposta_tb.proposta_id =  " & lPropostaId & vbNewLine
         
    Set rsTemp = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, sSql, True)
                             
    If Not rsTemp.EOF Then
        Set ObterDadosCapitalPremio = rsTemp
        Exit Function
    End If
        
    If lProdutoId = ProdIdSeguroVidaAliancaIII Then
        sSql = ""
        sSql = sSql & "    SELECT imp_segurada = web_seguros_db..proposta_vida_alianca_tb.val_imp_segurada, " & vbNewLine
        sSql = sSql & "           val_premio   = web_seguros_db..proposta_vida_alianca_tb.val_premio_total, " & vbNewLine
        sSql = sSql & "           plano_id     = isnull(web_seguros_db..proposta_vida_alianca_tb.plano_id,0), " & vbNewLine
        sSql = sSql & "           nome_plano   = isnull(plano_tb.nome,''), " & vbNewLine
        sSql = sSql & "           qtd_salarios = 0 " & vbNewLine
        sSql = sSql & "      FROM web_seguros_db..proposta_vida_alianca_tb  WITH (NOLOCK)  " & vbNewLine
        sSql = sSql & "      JOIN seguro_vida_sub_grupo_tb  WITH (NOLOCK)  " & vbNewLine
        sSql = sSql & "        ON seguro_vida_sub_grupo_tb.apolice_id = web_seguros_db..proposta_vida_alianca_tb.apolice_id " & vbNewLine
        sSql = sSql & "       AND seguro_vida_sub_grupo_tb.ramo_id = web_seguros_db..proposta_vida_alianca_tb.ramo_id " & vbNewLine
        sSql = sSql & "       AND seguro_vida_sub_grupo_tb.sucursal_seguradora_id = web_seguros_db..proposta_vida_alianca_tb.sucursal_seguradora_id " & vbNewLine
        sSql = sSql & "       AND seguro_vida_sub_grupo_tb.seguradora_cod_susep = web_seguros_db..proposta_vida_alianca_tb.seguradora_cod_susep " & vbNewLine
        sSql = sSql & "       AND seguro_vida_sub_grupo_tb.sub_grupo_id = web_seguros_db..proposta_vida_alianca_tb.sub_grupo_id " & vbNewLine
        sSql = sSql & "       AND seguro_vida_sub_grupo_tb.proposta_adesao_id = web_seguros_db..proposta_vida_alianca_tb.proposta_id " & vbNewLine
        sSql = sSql & "      JOIN proposta_tb  WITH (NOLOCK)  " & vbNewLine
        sSql = sSql & "        ON proposta_tb.proposta_id = seguro_vida_sub_grupo_tb.proposta_adesao_id " & vbNewLine
        sSql = sSql & " LEFT JOIN plano_tb  WITH (NOLOCK)  " & vbNewLine
        sSql = sSql & "        ON plano_tb.plano_id = web_seguros_db..proposta_vida_alianca_tb.plano_id " & vbNewLine
        sSql = sSql & "       AND plano_tb.dt_fim_vigencia IS NULL " & vbNewLine
        sSql = sSql & "       AND plano_tb.produto_id = proposta_tb.produto_id " & vbNewLine
        sSql = sSql & "     WHERE seguro_vida_sub_grupo_tb.dt_inicio_vigencia_sbg <= '" & sDtQuery & "' " & vbNewLine
        sSql = sSql & "       AND ( seguro_vida_sub_grupo_tb.dt_fim_vigencia_sbg IS NULL " & vbNewLine
        sSql = sSql & "           OR seguro_vida_sub_grupo_tb.dt_fim_vigencia_sbg >= '" & sDtQuery & "')" & vbNewLine
        sSql = sSql & "       AND seguro_vida_sub_grupo_tb.proposta_adesao_id =  " & lPropostaId & vbNewLine
        sSql = sSql & "       AND seguro_vida_sub_grupo_tb.apolice_id = " & lApoliceId & vbNewLine
        sSql = sSql & "       AND seguro_vida_sub_grupo_tb.ramo_id = " & LramoId & vbNewLine
        sSql = sSql & "       AND seguro_vida_sub_grupo_tb.sucursal_seguradora_id = " & lSucursalId & vbNewLine
        sSql = sSql & "       AND seguro_vida_sub_grupo_tb.seguradora_cod_susep =  " & lSeguradoraId & vbNewLine
        sSql = sSql & "       AND seguro_vida_sub_grupo_tb.sub_grupo_id =  " & iSubgrupoId & vbNewLine
    Else
          
        'subgrupos'''''''''''''''''''''''''''''''''''
        sSql = ""
        sSql = sSql & "    SELECT imp_segurada = escolha_sub_grp_tp_cob_comp_tb.val_is,                                                 " & vbNewLine
        sSql = sSql & "           val_premio   = escolha_sub_grp_tp_cob_comp_tb.val_premio,                                             " & vbNewLine
        sSql = sSql & "           plano_id     = 0,                                                                                     " & vbNewLine
        sSql = sSql & "           nome_plano   = '',                                                                                    " & vbNewLine
        sSql = sSql & "           qtd_salarios = 0                                                                                      " & vbNewLine
        sSql = sSql & "      FROM escolha_sub_grp_tp_cob_comp_tb  WITH (NOLOCK)                                                            " & vbNewLine
        sSql = sSql & "      JOIN tp_cob_comp_item_tb  WITH (NOLOCK)                                                                       " & vbNewLine
        sSql = sSql & "        ON tp_cob_comp_item_tb.tp_cob_comp_id = escolha_sub_grp_tp_cob_comp_tb.tp_cob_comp_id                    " & vbNewLine
        sSql = sSql & "       AND tp_cob_comp_item_tb.dt_inicio_vigencia_comp = escolha_sub_grp_tp_cob_comp_tb.dt_inicio_vigencia_comp  " & vbNewLine
        sSql = sSql & "      JOIN tp_cob_comp_tb  WITH (NOLOCK)                                                                            " & vbNewLine
        sSql = sSql & "        ON tp_cob_comp_tb.tp_cob_comp_id = tp_cob_comp_item_tb.tp_cob_comp_id                                    " & vbNewLine
        sSql = sSql & "       AND tp_cob_comp_tb.tp_componente_id = 1                                                                   " & vbNewLine
        sSql = sSql & "       AND tp_cob_comp_tb.class_tp_cobertura = 'b'                                                               " & vbNewLine
        sSql = sSql & "      JOIN apolice_tb  WITH (NOLOCK)                                                                                " & vbNewLine
        sSql = sSql & "        ON apolice_tb.apolice_id = escolha_sub_grp_tp_cob_comp_tb.apolice_id                                     " & vbNewLine
        sSql = sSql & "       AND apolice_tb.ramo_id = escolha_sub_grp_tp_cob_comp_tb.ramo_id                                           " & vbNewLine
        sSql = sSql & "       AND apolice_tb.sucursal_seguradora_id = escolha_sub_grp_tp_cob_comp_tb.sucursal_seguradora_id             " & vbNewLine
        sSql = sSql & "       AND apolice_tb.seguradora_cod_susep = escolha_sub_grp_tp_cob_comp_tb.seguradora_cod_susep                 " & vbNewLine
        sSql = sSql & "     WHERE apolice_tb.proposta_id = " & lPropostaId & vbNewLine
        sSql = sSql & "       AND escolha_sub_grp_tp_cob_comp_tb.sub_grupo_id = " & iSubgrupoId & vbNewLine
        sSql = sSql & "        and escolha_sub_grp_tp_cob_comp_tb.apolice_id  =  " & lApoliceId & vbNewLine
        sSql = sSql & "       AND escolha_sub_grp_tp_cob_comp_tb.ramo_id    = " & LramoId & vbNewLine
        sSql = sSql & "       AND escolha_sub_grp_tp_cob_comp_tb.sucursal_seguradora_id  = " & lSucursalId & vbNewLine
        sSql = sSql & "       AND escolha_sub_grp_tp_cob_comp_tb.seguradora_cod_susep = " & lSeguradoraId

    
   
    End If

    Set rsTemp = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, sSql, True)
    
    Set ObterDadosCapitalPremio = rsTemp

    Exit Function

TrataErro:
    Call TratarErro("ObterDadosCapitalPremio", Me.name)
    Call FinalizarAplicacao

End Function


Private Function ObterDadosCapitalPremioEspecifico(lPropostaId As Long, _
                                                   lProdutoId As Long) As Recordset

    On Error GoTo TrataErro

    Dim sSql As String
    
    sSql = ""

    'Importancia Segurada e Premio (casos especiais) '''''''''''
    Select Case lProdutoId
    
        Case ProdIdPenhorRural
            sSql = sSql & "SELECT val_is                = SUM(val_is) ,    "
            sSql = sSql & "       val_premio            = SUM(val_premio) , "
            sSql = sSql & "       capital_segurado_dgn  = 0     , "
            sSql = sSql & "       val_premio_dgn        = 0     , "
            sSql = sSql & "        val_assistencia_facul = 0     , "
            sSql = sSql & "        ind_tit_cc = ''                 "
            sSql = sSql & "  FROM escolha_tp_cob_generico_tb  WITH (NOLOCK) "
            sSql = sSql & " WHERE proposta_id = " & lPropostaId
            
        Case ProdIdOuroAgricola
            sSql = sSql & " Select val_is, "
            sSql = sSql & "        val_premio ,"
            sSql = sSql & "        capital_segurado_dgn  = 0     , "
            sSql = sSql & "        val_premio_dgn        = 0     , "
            sSql = sSql & "        val_assistencia_facul = 0     , "
            sSql = sSql & "        ind_tit_cc = ''                 "
            sSql = sSql & "   From escolha_tp_cob_generico_tb  WITH (NOLOCK)      "
            sSql = sSql & "  Where proposta_id = " & lPropostaId
            sSql = sSql & "    and dt_fim_vigencia_esc is null     "
  
        Case ProdIdOuroVidaMulher
            sSql = sSql & " SELECT val_is = capital_segurado, "
            sSql = sSql & "        val_premio,"
            sSql = sSql & "        capital_segurado_dgn , "
            sSql = sSql & "        val_premio_dgn , "
            sSql = sSql & "        val_assistencia_facul, "
            sSql = sSql & "        ind_tit_cc = isnull(ind_tit_cc,'')  "
            sSql = sSql & "   FROM emi_proposta_tb  WITH (NOLOCK)  "
            sSql = sSql & "  WHERE tp_registro = '01' "
            sSql = sSql & "    AND proposta_bb = " & txtPropostaBB.Text
            sSql = sSql & "    AND arquivo LIKE '" & ObterNomeArqExterno(lProdutoId) & "%'"
            'GENJUNIOR - FLOW 17860335
            'UNION COM TABELAS DE EXPURGO
            '10/10/2013
            sSql = sSql & "  UNION "
            sSql = sSql & " SELECT val_is = capital_segurado, "
            sSql = sSql & "        val_premio,"
            sSql = sSql & "        capital_segurado_dgn , "
            sSql = sSql & "        val_premio_dgn , "
            sSql = sSql & "        val_assistencia_facul, "
            sSql = sSql & "        ind_tit_cc = isnull(ind_tit_cc,'')  "
            sSql = sSql & "   FROM seguros_hist_db.dbo.emi_proposta_hist_tb  WITH (NOLOCK)  "
            sSql = sSql & "  WHERE tp_registro = '01' "
            sSql = sSql & "    AND proposta_bb = " & txtPropostaBB.Text
            sSql = sSql & "    AND arquivo LIKE '" & ObterNomeArqExterno(lProdutoId) & "%'"
            'FIM GENJUNIOR
      
    End Select
         
    If sSql <> "" Then
    
        Set ObterDadosCapitalPremioEspecifico = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, sSql, True)
    Else
    
        Set ObterDadosCapitalPremioEspecifico = Nothing
      
    End If

    Exit Function

TrataErro:
    Call TratarErro("ObterDadosCapitalPremioEspecifico", Me.name)
    Call FinalizarAplicacao

End Function

Private Function ObterDadosCoeficienteCusto(lProdutoId As Long, _
                                            lPlanoId As Long, _
                                            dt_vigencia As String) As Recordset

    On Error GoTo TrataErro

    Dim sSql As String

'+----------------------------------------
'| Migra��o SQL - Melhoria de performance
'| petrauskas 08/2016
'|
    sSql = "SET NOCOUNT ON  exec SEGS13064_SPS " & lPlanoId _
         & ",  " & lProdutoId _
         & ", '" & dt_vigencia & "'" _
         & ", '" & dt_vigencia & "'" _
         & ", " & CodRegraCalculoSalario
    
'    '275037 - OVEMPRESA - COEFICIENTE DE CUSTO (TROB)
'    'altera��o para incluir a vigencia
'    sSql = ""
'    sSql = sSql & " Select coef_custo"
'    sSql = sSql & "   From plano_empresa_tb  WITH (NOLOCK) "
'    sSql = sSql & "  Where plano_id = " & lPlanoId
'    sSql = sSql & "    and produto_id = " & lProdutoId
'    sSql = sSql & "    and dt_inicio_vigencia in ("
'    sSql = sSql & "                               select p.dt_inicio_vigencia "
'    sSql = sSql & "                                 from tp_plano_tb tp , plano_tb p  WITH (NOLOCK) "
'    sSql = sSql & "                                where tp.tp_plano_id = p.tp_plano_id "
'    sSql = sSql & "                                  and p.plano_id = " & lPlanoId
'    sSql = sSql & "                                  and p.produto_id = " & lProdutoId
'
'    sSql = sSql & "                                  and dt_inicio_vigencia <= '" & dt_vigencia & "'"
'    sSql = sSql & "                                  and (p.dt_fim_vigencia is null"
'    sSql = sSql & "                                  or   p.dt_fim_vigencia >= '" & dt_vigencia & "')"
'
'    sSql = sSql & "                                  and tp.cod_regra_calculo_premio = " & CodRegraCalculoSalario
'    sSql = sSql & "                              )"
'| Migra��o SQL - Melhoria de performance
'+----------------------------------------
    
    Set ObterDadosCoeficienteCusto = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, sSql, True)
    
    Exit Function

TrataErro:
    Call TratarErro("ObterDadosCoeficienteCusto", Me.name)
    Call FinalizarAplicacao

End Function

Private Function ObterDadosBancarios(lPropostaId As Long, _
                                     iSubgrupoId As Integer) As Recordset

    On Error GoTo TrataErro

    Dim sSql As String
  
    sSql = ""
    
    If iSubgrupoId = -1 Then
    
        sSql = sSql & "     SELECT proposta_bb,                                                          " & vbNewLine
        sSql = sSql & "            profissao_desc,                                                       " & vbNewLine
        sSql = sSql & "            cont_agencia_id,                                                      " & vbNewLine
        sSql = sSql & "            ag_contratante.nome as agencia_contratante,                           " & vbNewLine
        sSql = sSql & "            cont_banco_id,                                                        " & vbNewLine
        sSql = sSql & "            deb_agencia_id,                                                       " & vbNewLine
        sSql = sSql & "            ag_debito.nome as agencia_debito,                                     " & vbNewLine
        sSql = sSql & "            deb_conta_corrente_id,                                                " & vbNewLine
        sSql = sSql & "            data_cobranca_dia,                                                    " & vbNewLine
        sSql = sSql & "            ISNULL(num_cartao,0) num_cartao,                                      " & vbNewLine
        sSql = sSql & "            ISNULL(dt_validade_cartao,'') dt_validade_cartao,                     " & vbNewLine
        sSql = sSql & "            apolice_id,                                                           " & vbNewLine
        sSql = sSql & "            ramo_id,                                                              " & vbNewLine
        sSql = sSql & "            sucursal_seguradora_id,                                               " & vbNewLine
        sSql = sSql & "            seguradora_cod_susep,                                                 " & vbNewLine
        sSql = sSql & "            isnull(custo_certificado, 0) as custo_certificado,                    " & vbNewLine
        sSql = sSql & "            isnull(proposta_adesao_tb.qtd_parcela_premio,0) num_parcelas,         " & vbNewLine
        sSql = sSql & "            proposta_adesao_tb.forma_pgto_id,                                     " & vbNewLine
        sSql = sSql & "            forma_pgto_tb.nome forma_pgto,                                        " & vbNewLine
        sSql = sSql & "            proposta_adesao_tb.periodo_pgto_id,                                   " & vbNewLine
        sSql = sSql & "            periodo_pgto_tb.nome periodo_pgto                                     " & vbNewLine
        sSql = sSql & "       FROM proposta_adesao_tb  WITH (NOLOCK)                                        " & vbNewLine
        sSql = sSql & "  LEFT JOIN agencia_tb ag_debito  WITH (NOLOCK)                                      " & vbNewLine
        sSql = sSql & "         ON ag_debito.agencia_id = proposta_adesao_tb.deb_agencia_id              " & vbNewLine
        sSql = sSql & "  LEFT JOIN agencia_tb ag_contratante  WITH (NOLOCK)                                 " & vbNewLine
        sSql = sSql & "         ON ag_contratante.agencia_id = proposta_adesao_tb.cont_agencia_id        " & vbNewLine
        sSql = sSql & "       JOIN forma_pgto_tb  WITH (NOLOCK)                                             " & vbNewLine
        sSql = sSql & "         ON forma_pgto_tb.forma_pgto_id = proposta_adesao_tb.forma_pgto_id        " & vbNewLine
        sSql = sSql & "       JOIN periodo_pgto_tb   WITH (NOLOCK)                                          " & vbNewLine
        sSql = sSql & "         ON periodo_pgto_tb.periodo_pgto_id = proposta_adesao_tb.periodo_pgto_id  " & vbNewLine
        sSql = sSql & "      WHERE proposta_id = " & lPropostaId & vbNewLine
        sSql = sSql & " UNION                                                                            " & vbNewLine
        sSql = sSql & "     SELECT proposta_fechada_tb.proposta_bb,                                      " & vbNewLine
        sSql = sSql & "            profissao_desc = '',                                                  " & vbNewLine
        sSql = sSql & "            proposta_fechada_tb.cont_agencia_id,                                  " & vbNewLine
        sSql = sSql & "            ag_contratante.nome as agencia_contratante,                           " & vbNewLine
        sSql = sSql & "            proposta_fechada_tb.cont_banco_id,                                    " & vbNewLine
        sSql = sSql & "            proposta_fechada_tb.agencia_id deb_agencia_id,                        " & vbNewLine
        sSql = sSql & "            ag_debito.nome as agencia_debito,                                     " & vbNewLine
        sSql = sSql & "            proposta_fechada_tb.deb_conta_corrente_id,                            " & vbNewLine
        sSql = sSql & "            proposta_fechada_tb.data_cobranca_dia,                                " & vbNewLine
        sSql = sSql & "            num_cartao = 0,                                                       " & vbNewLine
        sSql = sSql & "            dt_validade_cartao = '',                                              " & vbNewLine
        sSql = sSql & "            apolice_tb.apolice_id,                                                " & vbNewLine
        sSql = sSql & "            apolice_tb.ramo_id,                                                   " & vbNewLine
        sSql = sSql & "            apolice_tb.sucursal_seguradora_id,                                    " & vbNewLine
        sSql = sSql & "            apolice_tb.seguradora_cod_susep,                                      " & vbNewLine
        sSql = sSql & "            isnull(custo_apolice, 0) as custo_certificado,                        " & vbNewLine
        sSql = sSql & "            isnull(proposta_fechada_tb.num_parcelas,0) num_parcelas,              " & vbNewLine
        sSql = sSql & "            isnull(proposta_fechada_tb.forma_pgto_id,0) forma_pgto_id,            " & vbNewLine
        sSql = sSql & "            isnull(forma_pgto_tb.nome,'') forma_pgto,                             " & vbNewLine
        sSql = sSql & "            isnull(proposta_fechada_tb.periodo_pgto_id,0) periodo_pgto_id,        " & vbNewLine
        sSql = sSql & "            isnull(periodo_pgto_tb.nome,'') periodo_pgto                          " & vbNewLine
        sSql = sSql & "       FROM proposta_fechada_tb  WITH (NOLOCK)                                       " & vbNewLine
        sSql = sSql & " INNER JOIN apolice_tb  WITH (NOLOCK)                                                " & vbNewLine
        sSql = sSql & "         ON apolice_tb.proposta_id = proposta_fechada_tb.proposta_id              " & vbNewLine
        sSql = sSql & "  LEFT JOIN agencia_tb ag_debito  WITH (NOLOCK)                                      " & vbNewLine
        sSql = sSql & "         ON ag_debito.agencia_id = proposta_fechada_tb.agencia_id                 " & vbNewLine
        sSql = sSql & "  LEFT JOIN agencia_tb ag_contratante  WITH (NOLOCK)                                 " & vbNewLine
        sSql = sSql & "         ON ag_contratante.agencia_id = proposta_fechada_tb.cont_agencia_id       " & vbNewLine
        'Rafael Fonseca - 15/08/2006 - Altera��o para amarrar por banco _tambem e n�o s� por agencia - demanda 149646
        sSql = sSql & "        and ag_contratante.banco_id   = proposta_fechada_tb.banco_id              " & vbNewLine
        sSql = sSql & "  LEFT JOIN forma_pgto_tb  WITH (NOLOCK)                                             " & vbNewLine
        sSql = sSql & "         ON forma_pgto_tb.forma_pgto_id = proposta_fechada_tb.forma_pgto_id       " & vbNewLine
        sSql = sSql & "  LEFT JOIN periodo_pgto_tb   WITH (NOLOCK)                                          " & vbNewLine
        sSql = sSql & "         ON periodo_pgto_tb.periodo_pgto_id = proposta_fechada_tb.periodo_pgto_id " & vbNewLine
        sSql = sSql & "      WHERE proposta_fechada_tb.proposta_id = " & lPropostaId & vbNewLine

    Else
    
        sSql = sSql & "     SELECT proposta_fechada_tb.proposta_bb,                                                        " & vbNewLine
        sSql = sSql & "            profissao_desc = '',                                                                    " & vbNewLine
        sSql = sSql & "            proposta_fechada_tb.cont_agencia_id,                                                    " & vbNewLine
        sSql = sSql & "            ag_contratante.nome as agencia_contratante,                                             " & vbNewLine
        sSql = sSql & "            proposta_fechada_tb.cont_banco_id,                                                      " & vbNewLine
        sSql = sSql & "            proposta_fechada_tb.agencia_id deb_agencia_id,                                          " & vbNewLine
        sSql = sSql & "            ag_debito.nome as agencia_debito,                                                       " & vbNewLine
        sSql = sSql & "            proposta_fechada_tb.deb_conta_corrente_id,                                              " & vbNewLine
        sSql = sSql & "            proposta_fechada_tb.data_cobranca_dia,                                                  " & vbNewLine
        sSql = sSql & "            num_cartao = 0,                                                                         " & vbNewLine
        sSql = sSql & "            dt_validade_cartao = '',                                                                " & vbNewLine
        sSql = sSql & "            apolice_tb.apolice_id,                                                                  " & vbNewLine
        sSql = sSql & "            apolice_tb.ramo_id,                                                                     " & vbNewLine
        sSql = sSql & "            apolice_tb.sucursal_seguradora_id,                                                      " & vbNewLine
        sSql = sSql & "            apolice_tb.seguradora_cod_susep,                                                        " & vbNewLine
        sSql = sSql & "            isnull(custo_apolice, 0) as custo_certificado,                                          " & vbNewLine
        sSql = sSql & "            isnull(proposta_fechada_tb.num_parcelas,0) num_parcelas,                                " & vbNewLine
        sSql = sSql & "            isnull(proposta_fechada_tb.forma_pgto_id,0) forma_pgto_id,                              " & vbNewLine
        sSql = sSql & "            isnull(forma_pgto_tb.nome,'') forma_pgto,                                               " & vbNewLine
        sSql = sSql & "            isnull(proposta_fechada_tb.periodo_pgto_id,0) periodo_pgto_id,                          " & vbNewLine
        sSql = sSql & "            isnull(periodo_pgto_tb.nome,'') periodo_pgto                                            " & vbNewLine
        sSql = sSql & "       FROM sub_grupo_apolice_tb  WITH (NOLOCK)                                                        " & vbNewLine
        sSql = sSql & " INNER JOIN apolice_tb  WITH (NOLOCK)                                                                  " & vbNewLine
        sSql = sSql & "         ON apolice_tb.apolice_id = sub_grupo_apolice_tb.apolice_id                                 " & vbNewLine
        sSql = sSql & "        AND apolice_tb.ramo_id = sub_grupo_apolice_tb.ramo_id                                       " & vbNewLine
        sSql = sSql & "        AND apolice_tb.sucursal_seguradora_id = sub_grupo_apolice_tb.sucursal_seguradora_id         " & vbNewLine
        sSql = sSql & "        AND apolice_tb.seguradora_cod_susep = sub_grupo_apolice_tb.seguradora_cod_susep             " & vbNewLine
        sSql = sSql & " INNER JOIN proposta_fechada_tb  WITH (NOLOCK)                                                         " & vbNewLine
        sSql = sSql & "         ON proposta_fechada_tb.proposta_id = apolice_tb.proposta_id                                " & vbNewLine
        sSql = sSql & "  LEFT JOIN agencia_tb ag_debito  WITH (NOLOCK)                                                        " & vbNewLine
        sSql = sSql & "         ON ag_debito.agencia_id = proposta_fechada_tb.agencia_id                                   " & vbNewLine
        sSql = sSql & "  LEFT JOIN agencia_tb ag_contratante   WITH (NOLOCK)                                                  " & vbNewLine
        sSql = sSql & "         ON ag_contratante.agencia_id = proposta_fechada_tb.cont_agencia_id                         " & vbNewLine
        'Rafael Fonseca - 15/08/2006 - Altera��o para amarrar por banco _tambem e n�o s� por agencia - demanda 149646
        sSql = sSql & "        and ag_contratante.banco_id = proposta_fechada_tb.banco_id                                  " & vbNewLine
        sSql = sSql & "  LEFT JOIN forma_pgto_tb   WITH (NOLOCK)                                                              " & vbNewLine
        sSql = sSql & "         ON forma_pgto_tb.forma_pgto_id = proposta_fechada_tb.forma_pgto_id                         " & vbNewLine
        sSql = sSql & "  LEFT JOIN periodo_pgto_tb   WITH (NOLOCK)                                                            " & vbNewLine
        sSql = sSql & "         ON periodo_pgto_tb.periodo_pgto_id = proposta_fechada_tb.periodo_pgto_id                   " & vbNewLine
        sSql = sSql & "      WHERE proposta_fechada_tb.proposta_id =" & lPropostaId & vbNewLine
        sSql = sSql & "        AND sub_grupo_apolice_tb.sub_grupo_id =" & iSubgrupoId & vbNewLine
        sSql = sSql & "UNION                                                                                               " & vbNewLine
        sSql = sSql & "     SELECT proposta_adesao_tb.proposta_bb,                                                         " & vbNewLine
        sSql = sSql & "            profissao_desc = '',                                                                    " & vbNewLine
        sSql = sSql & "            proposta_adesao_tb.cont_agencia_id,                                                     " & vbNewLine
        sSql = sSql & "            ag_contratante.nome as agencia_contratante,                                             " & vbNewLine
        sSql = sSql & "            proposta_adesao_tb.cont_banco_id,                                                       " & vbNewLine
        sSql = sSql & "            proposta_adesao_tb.deb_agencia_id,                                                      " & vbNewLine
        sSql = sSql & "            ag_debito.nome as agencia_debito,                                                       " & vbNewLine
        sSql = sSql & "            proposta_adesao_tb.deb_conta_corrente_id,                                               " & vbNewLine
        sSql = sSql & "            proposta_adesao_tb.data_cobranca_dia,                                                   " & vbNewLine
        sSql = sSql & "            num_cartao = 0,                                                                         " & vbNewLine
        sSql = sSql & "            dt_validade_cartao = '',                                                                " & vbNewLine
        sSql = sSql & "            apolice_tb.apolice_id,                                                                  " & vbNewLine
        sSql = sSql & "            apolice_tb.ramo_id,                                                                     " & vbNewLine
        sSql = sSql & "            apolice_tb.sucursal_seguradora_id,                                                      " & vbNewLine
        sSql = sSql & "            apolice_tb.seguradora_cod_susep,                                                        " & vbNewLine
        sSql = sSql & "            isnull(custo_certificado, 0) as custo_certificado,                                      " & vbNewLine
        sSql = sSql & "            isnull(proposta_adesao_tb.qtd_parcela_premio,0) num_parcelas,                           " & vbNewLine
        sSql = sSql & "            isnull(proposta_adesao_tb.forma_pgto_id,0) forma_pgto_id,                               " & vbNewLine
        sSql = sSql & "            isnull(forma_pgto_tb.nome,'') forma_pgto,                                               " & vbNewLine
        sSql = sSql & "            isnull(proposta_adesao_tb.periodo_pgto_id,0) periodo_pgto_id,                           " & vbNewLine
        sSql = sSql & "            isnull(periodo_pgto_tb.nome,'') periodo_pgto                                            " & vbNewLine
        sSql = sSql & "       FROM sub_grupo_apolice_tb  WITH (NOLOCK)                                                        " & vbNewLine
        sSql = sSql & " INNER JOIN apolice_tb  WITH (NOLOCK)                                                                  " & vbNewLine
        sSql = sSql & "         ON apolice_tb.apolice_id = sub_grupo_apolice_tb.apolice_id                                 " & vbNewLine
        sSql = sSql & "        AND apolice_tb.ramo_id = sub_grupo_apolice_tb.ramo_id                                       " & vbNewLine
        sSql = sSql & "        AND apolice_tb.sucursal_seguradora_id = sub_grupo_apolice_tb.sucursal_seguradora_id         " & vbNewLine
        sSql = sSql & "        AND apolice_tb.seguradora_cod_susep = sub_grupo_apolice_tb.seguradora_cod_susep             " & vbNewLine
        sSql = sSql & " INNER JOIN proposta_adesao_tb  WITH (NOLOCK)                                                          " & vbNewLine
        sSql = sSql & "         ON proposta_adesao_tb.apolice_id = sub_grupo_apolice_tb.apolice_id                         " & vbNewLine
        sSql = sSql & "        AND proposta_adesao_tb.ramo_id = sub_grupo_apolice_tb.ramo_id                               " & vbNewLine
        sSql = sSql & "        AND proposta_adesao_tb.sucursal_seguradora_id = sub_grupo_apolice_tb.sucursal_seguradora_id " & vbNewLine
        sSql = sSql & "        AND proposta_adesao_tb.seguradora_cod_susep = sub_grupo_apolice_tb.seguradora_cod_susep     " & vbNewLine
        sSql = sSql & "  LEFT JOIN agencia_tb ag_debito  WITH (NOLOCK)                                                        " & vbNewLine
        sSql = sSql & "         ON ag_debito.agencia_id = proposta_adesao_tb.deb_agencia_id                                " & vbNewLine
        sSql = sSql & "  LEFT JOIN agencia_tb ag_contratante    WITH (NOLOCK)                                                 " & vbNewLine
        sSql = sSql & "         ON ag_contratante.agencia_id = proposta_adesao_tb.cont_agencia_id                          " & vbNewLine
        sSql = sSql & "  LEFT JOIN forma_pgto_tb   WITH (NOLOCK)                                                              " & vbNewLine
        sSql = sSql & "         ON forma_pgto_tb.forma_pgto_id = proposta_adesao_tb.forma_pgto_id                          " & vbNewLine
        sSql = sSql & "  LEFT JOIN periodo_pgto_tb    WITH (NOLOCK)                                                           " & vbNewLine
        sSql = sSql & "         ON periodo_pgto_tb.periodo_pgto_id = proposta_adesao_tb.periodo_pgto_id                    " & vbNewLine
        sSql = sSql & "      WHERE proposta_adesao_tb.proposta_id =" & lPropostaId & vbNewLine
        sSql = sSql & "        AND sub_grupo_apolice_tb.sub_grupo_id =" & iSubgrupoId & vbNewLine
    
    End If

    Set ObterDadosBancarios = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, sSql, True)
  
    Exit Function

TrataErro:
    Call TratarErro("ObterDadosBancarios", Me.name)
    Call FinalizarAplicacao

End Function

'-- Schoralick - Nova cosultoria - 18699551 - Extrato de Cobran�a
'-- Inicio - sc
Private Function ObterDadosInadimplencia(lPropostaId As Long) As Recordset

    On Error GoTo TrataErro

    Dim sSql As String

'+----------------------------------------
'| Migra��o SQL - Melhoria de performance
'| petrauskas 08/2016
'|
    sSql = "SET NOCOUNT ON  exec SEGS13065_SPS " & lPropostaId

'    sSql = sSql & " SELECT num_cobranca"
'    sSql = sSql & "      , dt_cobranca"
'    sSql = sSql & "      , vl_debito"
'    sSql = sSql & "      , ISNULL(CONVERT(VARCHAR(255), ano_mes_ref), '') AS ano_mes_ref"
''    sSql = sSql & "      , CASE tp_retorno"
''    sSql = sSql & "                  WHEN '01' then 'Insuficiencia de fundos' "
''    sSql = sSql & "                  WHEN '02' then 'Conta corrente n�o cadastrada' "
''    sSql = sSql & "                  WHEN '04' then 'Outras restri��es' "
''    sSql = sSql & "                  WHEN '10' then 'Ag�ncia em regime de encerramento' "
''    sSql = sSql & "                  WHEN '12' then 'Valor inv�lido' "
''    sSql = sSql & "                  WHEN '13' then 'Data de lan�amento inv�lida' "
''    sSql = sSql & "                  WHEN '14' then 'Ag�ncia inv�lida' "
''    sSql = sSql & "                  WHEN '15' then 'DAC da conta_corrente inv�lido' "
''    sSql = sSql & "                  WHEN '18' then 'Data do D�bito anterior a do processamento' "
''    sSql = sSql & "                  WHEN '30' then 'Sem contrato de d�bito autom�tico' "
''    sSql = sSql & "                  ELSE '' "
''    sSql = sSql & "        END AS Retorno"
''    sSql = sSql & "      , ISNULL(CONVERT(VARCHAR(255),dt_baixa,103), '') as dt_baixa "
'    sSql = sSql & "   FROM cobranca_inadimplente_tb (NOLOCK)"
'    sSql = sSql & "  WHERE proposta_id = " & lPropostaId
'    sSql = sSql & "    AND dt_baixa IS NULL"
'    sSql = sSql & "  ORDER BY num_cobranca DESC"
'| Migra��o SQL - Melhoria de performance
'+----------------------------------------

    Set ObterDadosInadimplencia = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, sSql, True)

    Exit Function

TrataErro:
    Call TratarErro("ObterDadosInadimplencia", Me.name)
    Call FinalizarAplicacao

End Function
'-- fim - sc

'(in�cio) -- rsouza -- 18/05/2011 -------------------------------------------------------------------------------------------------'
Private Function ObterDadosPlanosAnteriores(lPropostaId As Long) As Recordset

    On Error GoTo TrataErro
    Dim sSql As String

    sSql = ""
    
    'Demanda - 16590635 - Inicio

    sSql = sSql & " SELECT  DISTINCT " & vbCrLf
    sSql = sSql & "          escolha_plano_tb.imp_segurada, escolha_plano_tp_cob_tb.val_is," & vbCrLf
    sSql = sSql & "          DtEscolha = escolha_plano_tb.dt_escolha" & vbCrLf
    sSql = sSql & "   ,  PremioTotal          = escolha_plano_tb.val_premio" & vbCrLf
    sSql = sSql & "   ,  CapitalSegurado      = CASE WHEN (row_number() over (order by escolha_plano_tb.dt_escolha )) >= 2" & vbCrLf
    sSql = sSql & "                                     and ( CASE ISNULL(tp_cob_comp_tb.class_tp_cobertura, '')" & vbCrLf
    sSql = sSql & "                                                   WHEN 'a' THEN escolha_plano_tp_cob_tb.val_is" & vbCrLf
    sSql = sSql & "                                                   WHEN 'b' THEN escolha_plano_tb.imp_segurada - escolha_plano_tp_cob_tb.val_is" & vbCrLf
    sSql = sSql & "                                                   ELSE 0 END ) > 0" & vbCrLf
    sSql = sSql & "                                  THEN" & vbCrLf
    sSql = sSql & "                                      (iSNULL(escolha_plano_tb.imp_segurada,0) - iSNULL(escolha_plano_tp_cob_tb.val_is,0) )" & vbCrLf
    sSql = sSql & "                                  Else escolha_plano_tb.imp_segurada" & vbCrLf
    sSql = sSql & "                             End" & vbCrLf
    sSql = sSql & "       ,  IndenizacaoExtra     =" & vbCrLf
    sSql = sSql & "   CASE ISNULL(tp_cob_comp_tb.class_tp_cobertura, '')" & vbCrLf
    sSql = sSql & "        WHEN 'a' THEN escolha_plano_tp_cob_tb.val_is" & vbCrLf
    sSql = sSql & "        WHEN 'b' THEN escolha_plano_tb.imp_segurada - escolha_plano_tp_cob_tb.val_is" & vbCrLf
    sSql = sSql & "            ELSE 0" & vbCrLf
    sSql = sSql & "       End" & vbCrLf

    'sSql = sSql & " SELECT  DISTINCT                                                            " & vbCrLf
    'sSql = sSql & "         DtEscolha            = escolha_plano_tb.dt_escolha                  " & vbCrLf
    'sSql = sSql & "      ,  PremioTotal          = escolha_plano_tb.val_premio                  " & vbCrLf
    'sSql = sSql & "      ,  CapitalSegurado      = escolha_plano_tb.imp_segurada                " & vbCrLf
    'sSql = sSql & "      ,  IndenizacaoExtra     =                                              " & vbCrLf
    'sSql = sSql & "      CASE ISNULL(tp_cob_comp_tb.class_tp_cobertura, '')                     " & vbCrLf
    'sSql = sSql & "           WHEN 'a' THEN escolha_plano_tp_cob_tb.val_is                            " & vbCrLf
    'sSql = sSql & "           WHEN 'b' THEN escolha_plano_tb.imp_segurada - escolha_plano_tp_cob_tb.val_is " & vbCrLf
    'sSql = sSql & "           ELSE 0 " & vbCrLf
    'sSql = sSql & "      END  " & vbCrLf

    'Demanda - 16590635 - Fim

    '---------------------------------------------------------------------------------------'
    'sSql = sSql & " SELECT CapitalSegurado      = escolha_plano_tb.imp_segurada " & vbCrLf
    'sSql = sSql & "      , Plano                = plano_tb.plano_id " & vbCrLf
    'sSql = sSql & "      , NomePlano            = nome " & vbCrLf
    'sSql = sSql & "      , Coef_Custo           = coef_ajuste " & vbCrLf
    'sSql = sSql & "      , QtdSalarios          = escolha_plano_tb.qtd_salarios " & vbCrLf
    'sSql = sSql & "      , CapitalSeg           = capital_segurado_dgn " & vbCrLf
    'sSql = sSql & "      , PremioBruto          = escolha_plano_tb.val_premio " & vbCrLf
    'sSql = sSql & "      , IOF                  = val_iof " & vbCrLf
    'sSql = sSql & "      , DescontoComercial    = val_tot_desconto_comercial " & vbCrLf
    'sSql = sSql & "      , DescTecnico          = val_desconto_tecnico " & vbCrLf
    'sSql = sSql & "      , CustoCertificado     = custo_certificado " & vbCrLf
    'sSql = sSql & "      , PremioDGN            = val_premio_dgn " & vbCrLf
    'sSql = sSql & "      , DtEscolha            = escolha_plano_tb.dt_escolha " & vbCrLf
    '------------------------------------------------------------------------------------'

'INICIO  alterado 02/09/2013 projeto 17860335 - Melhorias no SEGBR MC_Amaral -confitec

'    sSql = sSql & "   FROM seguros_db.dbo.proposta_adesao_tb proposta_adesao_tb  WITH (NOLOCK)                              " & vbCrLf
'    sSql = sSql & "   JOIN seguros_db.dbo.escolha_plano_tb        escolha_plano_tb    WITH (NOLOCK)                         " & vbCrLf
'    sSql = sSql & "     ON proposta_adesao_tb.proposta_id                     =  escolha_plano_tb.proposta_id            " & vbCrLf
'    sSql = sSql & "   JOIN seguros_db.dbo.plano_tb                plano_tb        WITH (NOLOCK)                             " & vbCrLf
'    sSql = sSql & "     ON escolha_plano_tb.plano_id                          =  plano_tb.plano_id                       " & vbCrLf
'    sSql = sSql & "    AND escolha_plano_tb.produto_id                        =  plano_tb.produto_id                     " & vbCrLf
    sSql = sSql & " FROM seguros_db.dbo.escolha_plano_tb escolha_plano_tb  WITH (NOLOCK)  " & vbCrLf
    sSql = sSql & " JOIN seguros_db.dbo.plano_tb plano_tb  WITH (NOLOCK)  " & vbCrLf
    sSql = sSql & " ON escolha_plano_tb.plano_id = plano_tb.plano_id " & vbCrLf

    sSql = sSql & "    AND ISNULL(escolha_plano_tb.dt_inicio_vigencia, '')    =  ISNULL(plano_tb.dt_inicio_vigencia, '') " & vbCrLf

    '---------------------------------------------------------------------------------------------------------------------------------'
    sSql = sSql & "   LEFT JOIN seguros_db.dbo.escolha_plano_tp_cob_tb           escolha_plano_tp_cob_tb  WITH (NOLOCK)      " & vbCrLf
    sSql = sSql & "     ON escolha_plano_tb.proposta_id                       =  escolha_plano_tp_cob_tb.proposta_id      " & vbCrLf
    sSql = sSql & "    AND escolha_plano_tb.dt_escolha                        =  escolha_plano_tp_cob_tb.dt_escolha       " & vbCrLf
    sSql = sSql & "    AND escolha_plano_tb.plano_id                          =  escolha_plano_tp_cob_tb.plano_id         " & vbCrLf
    'sSql = sSql & "    AND escolha_plano_tp_cob_tb.dt_fim_vigencia_cob IS NULL                                            " & vbCrLf
    sSql = sSql & "   LEFT JOIN seguros_db.dbo.tp_cob_comp_tb                      tp_cob_comp_tb  WITH (NOLOCK)             " & vbCrLf
    sSql = sSql & "     ON tp_cob_comp_tb.tp_cob_comp_id                         = escolha_plano_tp_cob_tb.tp_cob_comp_id " & vbCrLf
    sSql = sSql & "    AND tp_cob_comp_tb.tp_cobertura_id                        = 455 " & vbCrLf

    '---------------------------------------------------------------------------------------------------------------------------------'

    sSql = sSql & "   LEFT JOIN seguros_db.dbo.plano_empresa_tb   plano_empresa_tb   WITH (NOLOCK)  " & vbCrLf
    sSql = sSql & "     ON escolha_plano_tb.plano_id                          =  plano_empresa_tb.plano_id " & vbCrLf
    sSql = sSql & "    AND escolha_plano_tb.produto_id                        =  plano_empresa_tb.produto_id " & vbCrLf
    sSql = sSql & "    AND ISNULL(escolha_plano_tb.dt_inicio_vigencia, '')    =  ISNULL(plano_empresa_tb.dt_inicio_vigencia, '') " & vbCrLf
'    sSql = sSql & "  WHERE proposta_adesao_tb.proposta_id                     = " & lPropostaId & vbCrLf
'    sSql = sSql & "    AND escolha_plano_tb.dt_fim_vigencia IS NOT NULL " & vbCrLf
    
    sSql = sSql & " WHERE escolha_plano_tb.proposta_id = " & lPropostaId & vbCrLf
    sSql = sSql & " AND escolha_plano_tb.dt_fim_vigencia IS NOT NULL " & vbCrLf
  'FIM alterado 02/09/2013 projeto 17860335 - Melhorias no SEGBR MC_Amaral -confitec
    sSql = sSql & "  ORDER " & vbCrLf
    sSql = sSql & "     BY escolha_plano_tb.dt_escolha "

    Set ObterDadosPlanosAnteriores = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, sSql, True)

    Exit Function
TrataErro:
    Call TratarErro("ObterDadosPlanosAnteriores", Me.name)
    Call FinalizarAplicacao
End Function
'(fim)    ---- 18/05/2011 -----------------------------------------------------------------------------------------------'

Private Function ObterDadosCobranca(lPropostaId As Long) As Recordset

    On Error GoTo TrataErro

    Dim sSql As String
  
    sSql = "obter_dados_cobranca_sps  " & lPropostaId
  
    '''  sSQL = sSQL & "     SELECT DISTINCT" & vbNewLine
    '''  sSQL = sSQL & "            a.proposta_id," & vbNewLine
    '''  sSQL = sSQL & "            a.deb_agencia_id," & vbNewLine
    '''  sSQL = sSQL & "            a.deb_conta_corrente_id," & vbNewLine
    '''  sSQL = sSQL & "            a.dt_agendamento," & vbNewLine
    '''  sSQL = sSQL & "            a.dt_recebimento," & vbNewLine
    '''  sSQL = sSQL & "            a.num_cobranca," & vbNewLine
    '''  sSQL = sSQL & "            a.val_cobranca," & vbNewLine
    '''  sSQL = sSQL & "            CASE" & vbNewLine
    '''  sSQL = sSQL & "               WHEN f.situacao = 'c' THEN 'Cancelada'" & vbNewLine
    '''  sSQL = sSQL & "               WHEN a.situacao = 'a' THEN 'Adimplente'" & vbNewLine
    '''  sSQL = sSQL & "               WHEN a.situacao = 'e' THEN 'Estornada'" & vbNewLine
    '''  sSQL = sSQL & "               WHEN a.situacao = 'i' THEN 'Inadimplente'" & vbNewLine
    '''  sSQL = sSQL & "               WHEN a.situacao = 'p' THEN 'Pendente'" & vbNewLine
    '''  sSQL = sSQL & "            END desc_situacao," & vbNewLine
    '''  sSQL = sSQL & "            a.nosso_numero,   " & vbNewLine
    '''  sSQL = sSQL & "            a.nosso_numero_dv, " & vbNewLine
    '''  sSQL = sSQL & "           CASE ISNULL(a.origem_baixa,  '')  " & vbNewLine
    '''  sSQL = sSQL & "               WHEN 'a' then 'Autom�tico'       " & vbNewLine
    '''  sSQL = sSQL & "               WHEN 'm' then 'Manual    '       " & vbNewLine
    '''  sSQL = sSQL & "               ELSE '          '  " & vbNewLine
    '''  sSQL = sSQL & "           END origem_baixa,  " & vbNewLine
    '''  sSQL = sSQL & "           CASE ISNULL(a.dt_baixa, '19000101')" & vbNewLine
    '''  sSQL = sSQL & "                   WHEN '19000101' THEN  ''" & vbNewLine
    '''  sSQL = sSQL & "                   ELSE CONVERT(varchar, a.dt_baixa, 103) " & vbNewLine
    '''  sSQL = sSQL & "           END  data_baixa    " & vbNewLine
    '''  sSQL = sSQL & "      FROM agendamento_cobranca_tb a  WITH (NOLOCK) " & vbNewLine
    '''  sSQL = sSQL & " LEFT JOIN fatura_tb f WITH (NOLOCK) " & vbNewLine
    '''  sSQL = sSQL & "        ON a.apolice_id = f.apolice_id " & vbNewLine
    '''  sSQL = sSQL & "       AND a.fatura_id = f.fatura_id " & vbNewLine
    '''  sSQL = sSQL & "     WHERE a.proposta_id = " & lPropostaId & vbNewLine
    '''  sSQL = sSQL & "       AND a.situacao in ('a','e','i','p') " & vbNewLine
    '''  sSQL = sSQL & "  ORDER BY a.num_cobranca DESC" & vbNewLine
  
    Set ObterDadosCobranca = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, sSql, True)
    Exit Function
TrataErro:
    Call TratarErro("ObterDadosCobranca", Me.name)
    Call FinalizarAplicacao
End Function

Private Function CarregarDadosCapitalPremio() As Boolean

    On Error GoTo TrataErro

    Dim rsDadosFinanceiros As Recordset
  
    Set rsDadosFinanceiros = New Recordset
  
    'Capital e Premio ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    If colSubgrupos.Count > 0 Then
        fraSubgrupo(1).Visible = True
        Set rsDadosFinanceiros = ObterDadosCapitalPremio(Val(txtProposta.Text), Val(txtProduto.Tag), Val(txtApolice.Text), Val(txtRamo.Tag), Val(txtSucursal.Tag), Val(txtSeguradora.Tag), cboSubgrupo(1).ItemData(cboSubgrupo(1).ListIndex), Format(txtDtIniVigProposta.Text, "yyyymmdd"))
    Else
        fraSubgrupo(1).Visible = False
        Set rsDadosFinanceiros = ObterDadosCapitalPremio(Val(txtProposta.Text), Val(txtProduto.Tag), Val(txtApolice.Text), Val(txtRamo.Tag), Val(txtSucursal.Text), Val(txtSeguradora.Text), -1, Format(txtDtIniVigProposta.Text, "yyyymmdd"))
                                                 
    End If

    If Not rsDadosFinanceiros.EOF Then
        CarregarDadosCapitalPremio = True
  
        txtIS.Text = FormataValorParaPadraoBrasil(rsDadosFinanceiros("imp_segurada"))
        txtPremio.Text = FormataValorParaPadraoBrasil(rsDadosFinanceiros("val_premio"))
        txtPlanoId(0).Text = Val(rsDadosFinanceiros("plano_id"))
        txtPlano(0).Text = Trim(rsDadosFinanceiros("nome_plano"))
        txtQtdSalPlano.Text = Val(rsDadosFinanceiros("qtd_salarios"))
  
        rsDadosFinanceiros.Close
    
    Else
        CarregarDadosCapitalPremio = False
    
    End If
 
    Set rsDadosFinanceiros = Nothing
 
    Exit Function
TrataErro:
    Call TratarErro("CarregarDadosCapitalPremio", Me.name)
    Call FinalizarAplicacao

End Function

Private Function CarregarDadosFinanceiros() As Boolean

    On Error GoTo TrataErro

    Dim rsDadosFinanceiros As Recordset
    Dim rsDadosInadimplencia As Recordset
    Dim bDados As Boolean
    Dim sLinha As String
    Dim iSubgrupoId As Integer
    Dim sRetorno As String
    Dim sDtRetorno As String
    
    CarregarDadosFinanceiros = True
    
    cmdAssistencia.Visible = False
    txtValAssistFacult.Visible = True
    lbl.Visible = True
    
  
    bDados = False
  
    If colSubgrupos.Count > 0 Then
        iSubgrupoId = cboSubgrupo(1).ItemData(cboSubgrupo(1).ListIndex)
    Else
        iSubgrupoId = -1
    End If
  
    If CarregarDadosCapitalPremio Then
        bDados = True
    End If
  
    Set rsDadosInadimplencia = New Recordset
    Set rsDadosFinanceiros = New Recordset

    'Capital e Premio (casos especificos)'''''''''''''''''''''''''''''''''''''''''''''
    Set rsDadosFinanceiros = ObterDadosCapitalPremioEspecifico(Val(txtProposta.Text), Val(txtProduto.Tag))
  
    If Not rsDadosFinanceiros Is Nothing Then

        If Not rsDadosFinanceiros.EOF Then
        
            bDados = True
        
            'bcarneiro - 01/09/2005
            If Val(txtProduto.Tag) <> ProdIdOuroVidaMulher Then
                txtIS.Text = FormataValorParaPadraoBrasil(IIf(IsNull(rsDadosFinanceiros("val_is")), 0, rsDadosFinanceiros("val_is")))
                txtPremio.Text = FormataValorParaPadraoBrasil(IIf(IsNull(rsDadosFinanceiros("val_premio")), 0, rsDadosFinanceiros("val_premio")))
                txtISDGN.Text = FormataValorParaPadraoBrasil(rsDadosFinanceiros("capital_segurado_dgn"))
                txtPremioDGN.Text = FormataValorParaPadraoBrasil(rsDadosFinanceiros("val_premio_dgn"))
            End If

            '''''''''''''''''''
            txtIndTitCC.Text = UCase(Trim(rsDadosFinanceiros("ind_tit_cc")))
            
            'Flow 18334512 - Circular 491 20150109
'            If rsDadosFinanceiros("val_assistencia_facul") > 0 Then
'                txtAssistFacul.Text = "SIM"
'                txtAssistFacul.Tag = "S"
'
'                Else: txtAssistFacul.Text = "N�O"
'                txtAssistFacul.Tag = "N"
'                txtValAssistFacult.Visible = True 'True Flow 18334512 - Circular 491 20150109
'
'            End If
        
        End If
      
        rsDadosFinanceiros.Close
    End If
  
    If Not bDados Then
        'Call Err.Raise(vbObjectError + 1031, , "CarregarDadosFinanceiros - N�o foi poss�vel obter os dados de capital e pr�mio")
        Call MsgBox("N�o foi poss�vel obter os dados de capital e pr�mio", vbCritical)
        CarregarDadosFinanceiros = False
        Exit Function
        'FinalizarAplicacao
        '    GoTo TrataErro
    End If
  
    'Coeficiente de custo '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    '275037 - OVEMPRESA - COEFICIENTE DE CUSTO (TROB)
    'altera��o para incluir a vigencia
    Set rsDadosFinanceiros = ObterDadosCoeficienteCusto(Val(txtProduto.Tag), Val(txtPlanoId(0).Text), Format(txtDtIniVigProposta.Text, "yyyymmdd"))
  
    If Not rsDadosFinanceiros.EOF Then
        txtCoefCustoPlano.Text = Format(rsDadosFinanceiros("coef_custo"), "#0.00000")
    End If
    
    'Dados Banc�rios'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'  rsDadosFinanceiros.Close
'    Set rsDadosFinanceiros = ObterDadosBancarios(Val(txtProposta.Text), iSubgrupoId)
'
'    If rsDadosFinanceiros.EOF Then
'        Call MsgBox("N�o foi poss�vel obter os dados banc�rios", vbCritical)
'        rsDadosFinanceiros.Close
'        Set rsDadosFinanceiros = Nothing
'        CarregarDadosFinanceiros = False
'        Exit Function
'        'Call Err.Raise(vbObjectError + 1036, , "CarregarDadosFinanceiros - N�o foi poss�vel obter os dados banc�rios")
'        'FinalizarAplicacao
'        'GoTo TrataErro
'    End If
'
'    If Not IsNull(rsDadosFinanceiros("deb_agencia_id")) Then
'        txtAgenciaDeb.Text = Trim(rsDadosFinanceiros("deb_agencia_id")) & " - " & Trim(rsDadosFinanceiros("agencia_debito"))
'        txtAgenciaDeb.Tag = Val(rsDadosFinanceiros("deb_agencia_id"))
'    End If
'
'    If Not IsNull(rsDadosFinanceiros("deb_conta_corrente_id")) Then
'        txtCCDeb.Text = Trim(rsDadosFinanceiros("deb_conta_corrente_id"))
'    End If
'
'    If Not IsNull(rsDadosFinanceiros("cont_agencia_id")) Then
'        txtAgencia.Text = Trim(rsDadosFinanceiros("cont_agencia_id")) & " - " & Trim(rsDadosFinanceiros("agencia_contratante"))
'        txtAgencia.Tag = Val(rsDadosFinanceiros("cont_agencia_id"))
'    End If
'
'    txtDiaDebito.Text = IIf(IsNull(rsDadosFinanceiros("data_cobranca_dia")), "", rsDadosFinanceiros("data_cobranca_dia"))
'    txtNumCartao.Text = IIf(Trim(rsDadosFinanceiros("num_cartao")) = 0, "", Trim(rsDadosFinanceiros("num_cartao")))
'    txtDtCartao.Text = IIf(Format(rsDadosFinanceiros("dt_validade_cartao"), "dd/mm/yyyy") = "01/01/1900", "", Format(rsDadosFinanceiros("dt_validade_cartao"), "dd/mm/yyyy"))
'
'    txtFormaPgto.Text = IIf(Val(rsDadosFinanceiros("forma_pgto_id")) = 0, "", Trim(rsDadosFinanceiros("forma_pgto_id")) & " - " & Trim(rsDadosFinanceiros("forma_pgto")))
'    txtTpPgto.Text = IIf(Val(rsDadosFinanceiros("periodo_pgto_id")) = 0, "", Trim(rsDadosFinanceiros("periodo_pgto_id")) & " - " & Trim(rsDadosFinanceiros("periodo_pgto")))
'    txtNumParcelas.Text = IIf(Val(rsDadosFinanceiros("num_parcelas")) = 0, "", Val(rsDadosFinanceiros("num_parcelas")))
'
    'Cobranca ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    rsDadosFinanceiros.Close
    Set rsDadosFinanceiros = ObterDadosCobranca(Val(txtProposta.Text))
  
    MSFlexCobranca.Rows = 1
  
    While Not rsDadosFinanceiros.EOF
        
        sLinha = ""
        sLinha = sLinha & rsDadosFinanceiros("proposta_id") & vbTab
        sLinha = sLinha & rsDadosFinanceiros("deb_agencia_id") & vbTab
        sLinha = sLinha & rsDadosFinanceiros("deb_conta_corrente_id") & vbTab
        sLinha = sLinha & Format(rsDadosFinanceiros("dt_agendamento"), "dd/mm/yyyy") & vbTab
        sLinha = sLinha & Format(rsDadosFinanceiros("dt_recebimento"), "dd/mm/yyyy") & vbTab
        
        'Brauner Rangel - Nova Consultoria -Demanda 17914380 - Inclus�o coluna Ano/M�s Ref.
        Dim Str As String
        If Not rsDadosFinanceiros("ano_mes_ref") Then
        Str = Left$(rsDadosFinanceiros("ano_mes_ref"), Len(rsDadosFinanceiros("ano_mes_ref")) - 2) & "/" & Right$(rsDadosFinanceiros("ano_mes_ref"), 2) & vbTab
        sLinha = sLinha & Str
        Else
        sLinha = sLinha & rsDadosFinanceiros("ano_mes_ref") & vbTab
        End If
         'Brauner Rangel - Nova Consultoria -Demanda 17914380 - Inclus�o coluna Ano/M�s Ref. - fim
        
        sLinha = sLinha & rsDadosFinanceiros("num_cobranca") & vbTab
        sLinha = sLinha & FormataValorParaPadraoBrasil(rsDadosFinanceiros("val_cobranca")) & vbTab
        sLinha = sLinha & FormataValorParaPadraoBrasil(rsDadosFinanceiros("val_pago")) & vbTab
        sLinha = sLinha & rsDadosFinanceiros("desc_situacao") & vbTab
        sLinha = sLinha & rsDadosFinanceiros("nosso_numero") & vbTab
        sLinha = sLinha & rsDadosFinanceiros("nosso_numero_dv") & vbTab
        'vitor.cabral - Nova Consultoria - Demanda 178896885 � Registro de retorno do agente de cobran�a - Inicio
        'Adicao das colunas retorno e dt_retorno
        sLinha = sLinha & rsDadosFinanceiros("retorno") & vbTab
        sLinha = sLinha & Format(rsDadosFinanceiros("dt_retorno"), "dd/mm/yyyy") & vbTab
        'vitor.cabral - Nova Consultoria - Demanda 178896885 - Fim
        sLinha = sLinha & rsDadosFinanceiros("origem_baixa") & vbTab
        sLinha = sLinha & rsDadosFinanceiros("data_baixa") & vbTab
        sLinha = sLinha & rsDadosFinanceiros("intervencao_manual")
    
        MSFlexCobranca.AddItem sLinha
        
        'Formata a largura da coluna "Retorno" do grid
        If MSFlexCobranca.ColWidth(12) < Len(rsDadosFinanceiros("retorno")) * 100 Then
           MSFlexCobranca.ColWidth(12) = Len(rsDadosFinanceiros("retorno")) * 100
        End If
        
        rsDadosFinanceiros.MoveNext
    Wend

    'Ocultando as colunas de Proposta, Ag�ncia e Conta Corrente do grid Cobran�a
    MSFlexCobranca.ColWidth(0) = 0
    MSFlexCobranca.ColWidth(1) = 0
    MSFlexCobranca.ColWidth(2) = 0

'    'Inadimpl�ncia '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    If rsDadosInadimplencia.State = adStateOpen Then
      rsDadosInadimplencia.Close
    End If
    Set rsDadosInadimplencia = ObterDadosInadimplencia(Val(txtProposta.Text))
  
    MSFlexInadimplencia.Rows = 1
  
    While Not rsDadosInadimplencia.EOF
  
        sRetorno = ""
        sDtRetorno = ""
        sLinha = ""
        If Not IsNull(rsDadosInadimplencia("num_cobranca")) Then
            sLinha = sLinha & rsDadosInadimplencia("num_cobranca") & vbTab
            
            'IM01147463 - LEANDRO AMARAL - VALIDA��O SE OS DADOS FINANCEIROS FORAM CARREGADOS
            If Not rsDadosFinanceiros.EOF Then
                rsDadosFinanceiros.MoveFirst
                Do While Not rsDadosFinanceiros.EOF
                   If rsDadosFinanceiros("num_cobranca") = rsDadosInadimplencia("num_cobranca") Then
                      If Not IsNull(rsDadosFinanceiros("retorno")) Then
                         sRetorno = rsDadosFinanceiros("retorno")
                      End If
                      If Not IsNull(rsDadosFinanceiros("dt_retorno")) Then
                         sDtRetorno = Format(rsDadosFinanceiros("dt_retorno"), "dd/mm/yyyy")
                      End If
                      
                      Exit Do
                   End If
                   
                   rsDadosFinanceiros.MoveNext
                Loop
            End If
            'IM01147463 - LEANDRO AMARAL - VALIDA��O SE OS DADOS FINANCEIROS FORAM CARREGADOS
        Else
            sLinha = sLinha & " " & vbTab
        End If
    
        If Not IsNull(rsDadosInadimplencia("dt_cobranca")) Then
            sLinha = sLinha & Format(rsDadosInadimplencia("dt_cobranca"), "dd/mm/yyyy") & vbTab
        Else
            sLinha = sLinha & " " & vbTab
        End If
    
        If Not IsNull(rsDadosInadimplencia("vl_debito")) Then
            sLinha = sLinha & FormataValorParaPadraoBrasil(rsDadosInadimplencia("vl_debito")) & vbTab
        Else
            sLinha = sLinha & "0" & vbTab
        End If
    
        If Not IsNull(rsDadosInadimplencia("ano_mes_ref")) Then
            sLinha = sLinha & rsDadosInadimplencia("ano_mes_ref") & vbTab
        Else
            sLinha = sLinha & " " & vbTab
        End If
    
        sLinha = sLinha & sRetorno & vbTab
        sLinha = sLinha & sDtRetorno

        MSFlexInadimplencia.AddItem sLinha
        rsDadosInadimplencia.MoveNext
    Wend
    
    rsDadosInadimplencia.Close
    
    '136 - Ouro Vida Grupo Especial
    '716 - BB Seguro Vida Mulher
    'Flow 18334512 - Circular 491 09/02/2015
    
    'If Val(txtProduto.Tag) = ProdIdOuroVidaMulher Or Val(txtProduto.Tag) = ProdIdOuroVidaMigracao Then


        'Pcarvalho 05/10/2010 - Consultar Assist�ncia Facultativa
        'rsDadosFinanceiros.Close
        'Flow 18334512 - Circular 491 09/02/2015 1�
        'Set rsDadosFinanceiros = ObterDadosAssistencia(Val(txtProposta.Text), Format(txtDtIniVigProposta.Text, "yyyymmdd"))
        'Set rsDadosFinanceiros = ObterDadosAssistencia(Val(txtProposta.Text), (txtProduto.Text))

       ' txtAssistObrigatoria.Text = "SIM"
        'cmdAssistencia.Visible = False
        'If Not rsDadosFinanceiros.EOF Then
            'txtAssistFacul.Text = "SIM"
         '   If rsDadosFinanceiros!possui_facultativa = "S" Then
          '     txtAssistFacul.Text = "SIM"
           '    txtAssistFacul.Tag = "S"
            '   cmdAssistencia.Visible = False
              ' txtValAssistFacult.Text = FormataValorParaPadraoBrasil(IIf(IsNull(rsDadosFinanceiros!val_assistencia_carregado), 0, rsDadosFinanceiros!val_assistencia_carregado))
'           Else
'               txtAssistFacul.Text = "N�O"
'               txtAssistFacul.Tag = "N"
'               cmdAssistencia.Visible = False
'               txtValAssistFacult.Text = "0,00"
'
'            End If
'
'        Else
'            txtAssistFacul.Text = "N�O"
'            txtAssistFacul.Tag = "N"
'            cmdAssistencia.Visible = False
'            txtValAssistFacult.Text = "0,00"
'
'        End If

   ' End If
    'Flow 18334512 - Circular 491 09/02/2015
    'Pcarvalho 05/10/2010 - Obter o percentual de Remunera��o BB
  'Flow 18334512 - Circular 491 09/02/2015

  rsDadosFinanceiros.Close
    Set rsDadosFinanceiros = ObterDadosAssistencia(Val(txtProposta.Text), Val(txtProduto.Tag))

    If Not rsDadosFinanceiros.EOF Then
    
            If (IIf(IsNull(rsDadosFinanceiros!possui_compulsoria), "", rsDadosFinanceiros!possui_compulsoria) = "S") Then
                txtAssistObrigatoria.Text = "SIM"
                txtAssistObrigatoria.Tag = "S"
            Else
                txtAssistObrigatoria.Text = "N�O"
                txtAssistObrigatoria.Tag = "N"
            End If
            
            If (IIf(IsNull(rsDadosFinanceiros!possui_facultativa), "", rsDadosFinanceiros!possui_facultativa) = "S") Then
               txtAssistFacul.Text = "SIM"
               txtAssistFacul.Tag = "S"
               txtValAssistFacult.Text = FormataValorParaPadraoBrasil(IIf(IsNull(rsDadosFinanceiros!val_assistencia_carregado), 0, rsDadosFinanceiros!val_assistencia_carregado))
           Else
               txtAssistFacul.Text = "N�O"
               txtAssistFacul.Tag = "N"
               txtValAssistFacult.Text = "0,00"

            End If
    Else
          txtAssistObrigatoria.Text = "N�O"
          txtAssistObrigatoria.Tag = "N"
          txtAssistFacul.Text = "N�O"
          txtAssistFacul.Tag = "N"
          txtValAssistFacult.Text = "0,00"
    End If
    
 'Flow 18334512 - Circular 491 09/02/2015

    
 'Flow 18334512 - Circular 491 09/02/2015
    rsDadosFinanceiros.Close
    Set rsDadosFinanceiros = ObterDadosRemuneracaoServico(Val(txtProduto.Tag), Val(txtRamo.Tag), Format(txtDtIniVigProposta.Text, "yyyymmdd"))

    If Not rsDadosFinanceiros.EOF Then
        txtPercRemuneracaoServico.Text = Format(rsDadosFinanceiros("perc_remuneracao_servico"), "#0.00")
    Else
        txtPercRemuneracaoServico.Text = "0,00"
    End If

    ' Finalizando ''''''''''''''''''''''''''''''''''''''''''''''''''''
    rsDadosFinanceiros.Close
    Set rsDadosFinanceiros = Nothing
  
    '(in�cio) -- rsouza -- 18/05/2011 -------------------------------------------------------------------------------------------------------------------------------'
    If Val(txtProduto.Tag) = 721 Then
  
        If MSPlanosAnteriores.Enabled = False Then
            MSPlanosAnteriores.Enabled = True
            frmPlanosAnteriores.Enabled = True
        End If
  
        Set rsDadosFinanceiros = ObterDadosPlanosAnteriores(Val(txtProposta.Text))
    
        MSPlanosAnteriores.Rows = 1
      
        While Not rsDadosFinanceiros.EOF
        
            sLinha = ""
            sLinha = sLinha & Format(IIf(IsNull(rsDadosFinanceiros("DtEscolha")), "", rsDadosFinanceiros("DtEscolha")), "dd/mm/yyyy") & vbTab
            sLinha = sLinha & FormataValorParaPadraoBrasil(IIf(IsNull(rsDadosFinanceiros("PremioTotal")), 0, rsDadosFinanceiros("PremioTotal"))) & vbTab
            sLinha = sLinha & FormataValorParaPadraoBrasil(IIf(IsNull(rsDadosFinanceiros("CapitalSegurado")), 0, rsDadosFinanceiros("CapitalSegurado"))) & vbTab
            sLinha = sLinha & FormataValorParaPadraoBrasil(IIf(IsNull(rsDadosFinanceiros("IndenizacaoExtra")), "", rsDadosFinanceiros("IndenizacaoExtra"))) & vbTab
        
            '----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------'
            'Campos do Grid anteriormente: Dt. In�cio Vig�ncia  |  Capital Segurado  |  Plano   | Nome do Plano                                                         |  Coef. Custo   |    Qtd. Sal�rios   |  Capital Seg. Diag. de C�ncer   |  Valor IOF   |  Desc. Comercial  |  Desc. T�cnico  | Custo Certificado  | Premio Diag. C�ncer
            'sLinha = ""
            'sLinha = sLinha & Format(IIf(IsNull(rsDadosFinanceiros("DtEscolha")), "", rsDadosFinanceiros("DtEscolha")), "dd/mm/yyyy") & vbTab
            'sLinha = sLinha & FormataValorParaPadraoBrasil(IIf(IsNull(rsDadosFinanceiros("CapitalSegurado")), 0, rsDadosFinanceiros("CapitalSegurado"))) & vbTab
            'sLinha = sLinha & IIf(IsNull(rsDadosFinanceiros("Plano")), 0, rsDadosFinanceiros("Plano")) & vbTab
            'sLinha = sLinha & IIf(IsNull(rsDadosFinanceiros("NomePlano")), "", rsDadosFinanceiros("NomePlano")) & vbTab
            'sLinha = sLinha & FormataValorParaPadraoBrasil(IIf(IsNull(rsDadosFinanceiros("Coef_Custo")), 0, rsDadosFinanceiros("Coef_Custo"))) & vbTab
            'sLinha = sLinha & IIf(IsNull(rsDadosFinanceiros("QtdSalarios")), 0, rsDadosFinanceiros("QtdSalarios")) & vbTab
            'sLinha = sLinha & FormataValorParaPadraoBrasil(IIf(IsNull(rsDadosFinanceiros("CapitalSeg")), 0, rsDadosFinanceiros("CapitalSeg"))) & vbTab
            ''sLinha = sLinha & FormataValorParaPadraoBrasil(IIf(IsNull(rsDadosFinanceiros("PremioBruto")), 0, rsDadosFinanceiros("PremioBruto"))) & vbTab
            'sLinha = sLinha & FormataValorParaPadraoBrasil(IIf(IsNull(rsDadosFinanceiros("IOF")), 0, rsDadosFinanceiros("IOF"))) & vbTab
            'sLinha = sLinha & FormataValorParaPadraoBrasil(IIf(IsNull(rsDadosFinanceiros("DescontoComercial")), 0, rsDadosFinanceiros("DescontoComercial"))) & vbTab
            'sLinha = sLinha & FormataValorParaPadraoBrasil(IIf(IsNull(rsDadosFinanceiros("DescTecnico")), 0, rsDadosFinanceiros("DescTecnico"))) & vbTab
            'sLinha = sLinha & FormataValorParaPadraoBrasil(IIf(IsNull(rsDadosFinanceiros("CustoCertificado")), 0, rsDadosFinanceiros("CustoCertificado"))) & vbTab
            ''sLinha = sLinha & FormataValorParaPadraoBrasil(IIf(IsNull(rsDadosFinanceiros("PremioBruto")), 0, rsDadosFinanceiros("PremioBruto"))) & vbTab
            'sLinha = sLinha & FormataValorParaPadraoBrasil(IIf(IsNull(rsDadosFinanceiros("PremioDGN")), 0, rsDadosFinanceiros("PremioDGN")))
            '----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------'
        
            MSPlanosAnteriores.AddItem sLinha
            
            rsDadosFinanceiros.MoveNext
        Wend
     
        rsDadosFinanceiros.Close
        Set rsDadosFinanceiros = Nothing
     
    Else

        If MSPlanosAnteriores.Rows > 1 Then
            MSPlanosAnteriores.Rows = 1
        End If

        frmPlanosAnteriores.Enabled = False
        MSPlanosAnteriores.Enabled = False
    End If

    '(fim)    -- rsouza -- 18/05/2011 -------------------------------------------------------------------------------------------------------------------------------'
   
    Exit Function
    Resume
TrataErro:
    Call TratarErro("CarregarDadosFinanceiros", Me.name)
    Call FinalizarAplicacao

End Function

Private Function ObterDadosAssistencia(ByVal lPropostaId As Long, _
                                       ByVal lProduto As Long) As Recordset

    Dim sSql As String

    On Error GoTo TrataErro

'    sSql = ""
'    sSql = sSql & "select t.nome,"
'    sSql = sSql & "       case s.obrigatoria"
'    sSql = sSql & "         when 'S' then 'OBRIGAT�RIA'"
'    sSql = sSql & "         when 'N' then 'FACULTATIVA'"
'    sSql = sSql & "       end as tipo,"
'    sSql = sSql & "       case s.obrigatoria"
'    sSql = sSql & "          when 'S' then 0"
'    sSql = sSql & "          when 'N' then isnull(a.val_assistencia_facul, 0)"
'    sSql = sSql & "       end as val_assistencia"
'    sSql = sSql & "  from proposta_tb p"
'    sSql = sSql & " inner join proposta_adesao_tb a"
'    sSql = sSql & "    on p.proposta_id = a.proposta_id"
'    sSql = sSql & " inner join assistencia_uss_db..subramo_tp_assistencia_tb s"
'    sSql = sSql & "    on p.produto_id = s.produto_id"
'    sSql = sSql & "   and p.ramo_id = s.ramo_id"
'    sSql = sSql & "   and p.subramo_id = s.subramo_id"
'    sSql = sSql & "   and p.dt_inicio_vigencia_sbr = s.dt_inicio_vigencia_sbr"
'    sSql = sSql & "   and s.dt_inicio_vigencia_ass <= '" & sDtInicioVigencia & "'"
'    sSql = sSql & "   and (s.dt_fim_vigencia_ass is null or s.dt_fim_vigencia_ass >= '" & sDtInicioVigencia & "')"
'    sSql = sSql & "   and s.obrigatoria = 'N'"
'    sSql = sSql & "   and isnull(a.val_assistencia_facul, 0) > 0"
'    sSql = sSql & " inner join assistencia_uss_db..tp_assistencia_tb t"
'    sSql = sSql & "    on s.tp_assistencia_id = t.tp_assistencia_id"
'    sSql = sSql & " where p.proposta_id = " & lPropostaId
'Flow 18334512 - Circular 491 09/02/2015
     sSql = "set nocount on exec seguros_db..SEGS12290_SPS " & lPropostaId & "," & lProduto & ",'N',1"
     
    Set ObterDadosAssistencia = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, sSql, True)

    Exit Function

TrataErro:
    Call TratarErro("ObterDadosAssistencia", Me.name)
    Call FinalizarAplicacao

End Function

Private Function ObterDadosRemuneracaoServico(ByVal iProdutoId As Integer, _
                                              ByVal iRamoId As Integer, _
                                              ByVal sDtInicioVigencia As String) As Recordset

    Dim sSql As String

    On Error GoTo TrataErro

'+----------------------------------------
'| Migra��o SQL - Melhoria de performance
'| petrauskas 08/2016
'|
    sSql = "SET NOCOUNT ON  exec SEGS13066_SPS " _
         & iProdutoId & ", " _
         & iRamoId & ", '" _
         & sDtInicioVigencia & "', '" _
         & sDtInicioVigencia & "'"

'    sSql = ""
'    sSql = sSql & "SELECT ISNULL(perc_remuneracao_servico, 0) perc_remuneracao_servico" & vbNewLine
'    sSql = sSql & "  FROM custo_tb" & vbNewLine
'    sSql = sSql & " WHERE produto_id = " & iProdutoId & vbNewLine
'    sSql = sSql & "   AND ramo_id = " & iRamoId & vbNewLine
'    sSql = sSql & "   AND dt_inicio_vigencia <= '" & sDtInicioVigencia & "'" & vbNewLine
'    sSql = sSql & "   AND (dt_fim_vigencia IS NULL OR dt_fim_vigencia >= '" & sDtInicioVigencia & "')"
'| Migra��o SQL - Melhoria de performance
'+----------------------------------------

    Set ObterDadosRemuneracaoServico = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, sSql, True)

    Exit Function

TrataErro:
    Call TratarErro("ObterDadosRemuneracaoServico", Me.name)
    Call FinalizarAplicacao

End Function

Private Function ObterDadosSegurado(lPropostaId As Long) As Recordset

    On Error GoTo TrataErro

    Dim sSql As String
  
'    sSql = ""
'    sSql = sSql & "    SELECT cliente_tb.nome,                                                                " & vbNewLine
'    sSql = sSql & "           endereco_corresp_tb.endereco,                                                   " & vbNewLine
'    sSql = sSql & "           endereco_corresp_tb.bairro,                                                     " & vbNewLine
'    sSql = sSql & "           endereco_corresp_tb.municipio,                                                  " & vbNewLine
'    sSql = sSql & "           endereco_corresp_tb.estado,                                                     " & vbNewLine
'    sSql = sSql & "           endereco_corresp_tb.cep,                                                        " & vbNewLine
'    sSql = sSql & "           cod_mci = ISNULL(cliente_tb.cod_mci,0),                                         " & vbNewLine
'    sSql = sSql & "           cliente_tb.ddd_1,                                                               " & vbNewLine
'    sSql = sSql & "           cliente_tb.telefone_1,                                                          " & vbNewLine
'    sSql = sSql & "           cliente_tb.e_mail,                                                              " & vbNewLine
'    sSql = sSql & "           cliente_tb.cliente_id,                                                          " & vbNewLine
'    sSql = sSql & "           tp_pessoa = CASE                                                                " & vbNewLine
'    sSql = sSql & "                        WHEN pessoa_fisica_tb.pf_cliente_id   IS NOT NULL THEN 'F�SICA'    " & vbNewLine
'    sSql = sSql & "                        WHEN pessoa_juridica_tb.pj_cliente_id IS NOT NULL THEN 'JUR�DICA'  " & vbNewLine
'    sSql = sSql & "                       END ,                                                               " & vbNewLine
'    sSql = sSql & "           tp_pessoa_id = CASE                                                             " & vbNewLine
'    sSql = sSql & "                           WHEN pessoa_fisica_tb.pf_cliente_id   IS NOT NULL THEN 1        " & vbNewLine
'    sSql = sSql & "                           WHEN pessoa_juridica_tb.pj_cliente_id IS NOT NULL THEN 2        " & vbNewLine
'    sSql = sSql & "                          END ,                                                            " & vbNewLine
'    sSql = sSql & "                                                                                           " & vbNewLine
'    sSql = sSql & "            estado_civil = ISNULL(pessoa_fisica_tb.estado_civil,''),                       " & vbNewLine
'    sSql = sSql & "            sexo = ISNULL(pessoa_fisica_tb.sexo,''),                                       " & vbNewLine
'    'JFILHO - FLOW 707694 - CONFITEC - 22/01/09
'    'Corrigindo problema na exibi��o de CNPJs, quando o campo pessoa_fisica vinha vazio, o programa obtinha o cnpj, por�m, truncado com 11 caracteres (tamanho do CPF)
'    'sSql = sSql & "            cpf_cnpj = ISNULL(pessoa_fisica_tb.cpf, pessoa_juridica_tb.cgc),               " & vbNewLine
'    sSql = sSql & "            cpf_cnpj = ISNULL(pessoa_juridica_tb.cgc, pessoa_fisica_tb.cpf),               " & vbNewLine
'    'FIM
'    sSql = sSql & "            dt_nascimento = ISNULL(pessoa_fisica_tb.dt_nascimento,'19000101'),             " & vbNewLine
'    sSql = sSql & "            contato = ISNULL(pessoa_juridica_tb.contato,''),                               " & vbNewLine
'    sSql = sSql & "            doc_identidade = ISNULL(cliente_tb.doc_identidade,''),                         " & vbNewLine
'    sSql = sSql & "            tp_doc_identidade = ISNULL(cliente_tb.tp_doc_identidade,''),                   " & vbNewLine
'    sSql = sSql & "            exp_doc_identidade = ISNULL(cliente_tb.exp_doc_identidade,''),                 " & vbNewLine
'    sSql = sSql & "            dt_emissao_identidade = ISNULL(cliente_tb.dt_emissao_identidade,'19000101'),   " & vbNewLine
'    sSql = sSql & "            idade = ISNULL(dbo.calcula_idade_fn('" & Format(Data_Sistema, "yyyymmdd") & "',pessoa_fisica_tb.dt_nascimento),0)," & vbNewLine
'    sSql = sSql & "            profissao_cbo = ISNULL(pessoa_fisica_tb.profissao_cbo,0)                       " & vbNewLine
'    sSql = sSql & "       FROM proposta_tb  WITH (NOLOCK)                                                        " & vbNewLine
'    sSql = sSql & " INNER JOIN cliente_tb  WITH (NOLOCK)                                                         " & vbNewLine
'    sSql = sSql & "         ON cliente_tb.cliente_id = proposta_tb.prop_cliente_id                            " & vbNewLine
'    sSql = sSql & " INNER JOIN endereco_corresp_tb  WITH (NOLOCK)                                                " & vbNewLine
'    sSql = sSql & "         ON endereco_corresp_tb.proposta_id = proposta_tb.proposta_id                      " & vbNewLine
'    sSql = sSql & "  LEFT JOIN pessoa_fisica_tb   WITH (NOLOCK)                                                  " & vbNewLine
'    sSql = sSql & "         ON pessoa_fisica_tb.pf_cliente_id = cliente_tb.cliente_id                         " & vbNewLine
'    sSql = sSql & "  LEFT JOIN pessoa_juridica_tb   WITH (NOLOCK)                                                " & vbNewLine
'    sSql = sSql & "         ON pessoa_juridica_tb.pj_cliente_id = cliente_tb.cliente_id                       " & vbNewLine
'    sSql = sSql & "      WHERE proposta_tb.proposta_id = " & lPropostaId
    
    sSql = ""
    sSql = sSql & " SET NOCOUNT ON EXEC seguros_db.dbo.SEGS13100_SPS " & lPropostaId & ", '" & Format(Data_Sistema, "yyyymmdd") & "' "
  
    Set ObterDadosSegurado = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, sSql, True)
                                         
    Exit Function

TrataErro:
    Call TratarErro("CarregarDadosFinanceiros", Me.name)
    Call FinalizarAplicacao

End Function

Private Function ObterDadosConjuge(lPropostaId As Long) As Recordset

    On Error GoTo TrataErro
    
    Dim sSql As String
'    sSql = ""
'    sSql = sSql & " SELECT cliente_tb.nome,                                                          " & vbNewLine
'    sSql = sSql & "        pessoa_fisica_tb.cpf,                                                     " & vbNewLine
'    sSql = sSql & "        pessoa_fisica_tb.dt_nascimento,                                           " & vbNewLine
'    sSql = sSql & "        proposta_complementar_tb.profissao_desc,                                  " & vbNewLine
'    sSql = sSql & "        pessoa_fisica_tb.profissao_cbo,                                           " & vbNewLine
'    sSql = sSql & "        pessoa_fisica_tb.sexo,                                                    " & vbNewLine
'    'Ricardo Toledo - Confitec : 02/08/2010 : inicio
'    'Estava calculando errado a idade do c�njuge
'    'sSql = sSql & "        idade        = ISNULL(dbo.calcula_idade_fn('20040519',pessoa_fisica_tb.dt_nascimento),0), " & vbNewLine
'    sSql = sSql & "        idade        = ISNULL(dbo.calcula_idade_fn('" & Format(Data_Sistema, "yyyymmdd") & "',pessoa_fisica_tb.dt_nascimento),0), " & vbNewLine
'    'Ricardo Toledo - Confitec : 02/08/2010 : fim
'    sSql = sSql & "        idade        = ISNULL(dbo.calcula_idade_fn('20040519',pessoa_fisica_tb.dt_nascimento),0), " & vbNewLine
'    sSql = sSql & "        estado_civil = ISNULL(pessoa_fisica_tb.estado_civil,''),                  " & vbNewLine
'    sSql = sSql & "        endereco   = ISNULL(endereco_corresp_tb.endereco,''),                     " & vbNewLine
'    sSql = sSql & "        bairro     = ISNULL(endereco_corresp_tb.bairro,''),                       " & vbNewLine
'    sSql = sSql & "        municipio  = ISNULL(endereco_corresp_tb.municipio,''),                    " & vbNewLine
'    sSql = sSql & "        estado     = ISNULL(endereco_corresp_tb.estado,''),                       " & vbNewLine
'    sSql = sSql & "        cep        = ISNULL(endereco_corresp_tb.cep,''),                          " & vbNewLine
'    sSql = sSql & "        ddd_1      = ISNULL(cliente_tb.ddd_1,0),                                  " & vbNewLine
'    sSql = sSql & "        telefone_1 = ISNULL(cliente_tb.telefone_1,0),                             " & vbNewLine
'    sSql = sSql & "        e_mail     = ISNULL(cliente_tb.e_mail,''),                                " & vbNewLine
'    sSql = sSql & "        cliente_tb.cliente_id,                                                    " & vbNewLine
'    sSql = sSql & "        cod_mci   = ISNULL(cliente_tb.cod_mci,0)                                  " & vbNewLine
'    sSql = sSql & "      FROM cliente_tb  WITH (NOLOCK)                                                 " & vbNewLine
'    sSql = sSql & "INNER JOIN pessoa_fisica_tb  WITH (NOLOCK)                                           " & vbNewLine
'    sSql = sSql & "        ON pessoa_fisica_tb.pf_cliente_id = cliente_tb.cliente_id                 " & vbNewLine
'    sSql = sSql & "INNER JOIN proposta_complementar_tb  WITH (NOLOCK)                                   " & vbNewLine
'    sSql = sSql & "        ON proposta_complementar_tb.prop_cliente_id = cliente_tb.cliente_id       " & vbNewLine
'    sSql = sSql & "       AND proposta_complementar_tb.dt_fim_vigencia IS NULL                       " & vbNewLine
'    sSql = sSql & " LEFT JOIN endereco_corresp_tb  WITH (NOLOCK)                                        " & vbNewLine
'    sSql = sSql & "        ON endereco_corresp_tb.proposta_id = proposta_complementar_tb.proposta_id " & vbNewLine
'    sSql = sSql & "     WHERE proposta_complementar_tb.proposta_id = " & lPropostaId & vbNewLine
'    sSql = sSql & "       AND proposta_complementar_tb.situacao <> 'c' " & vbNewLine
    
    sSql = ""
    sSql = sSql & " SET NOCOUNT ON EXEC seguros_db.dbo.SEGS13101_SPS " & lPropostaId

    Set ObterDadosConjuge = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, sSql, True)
     
    Exit Function

TrataErro:
    Call TratarErro("ObterDadosConjuge", Me.name)
    Call FinalizarAplicacao
  
End Function

Private Function CarregarDadosProponente() As Boolean

    On Error GoTo TrataErro

    Dim rsSegurado As Recordset
    Dim sAux As String
    Dim sLinha As String
  
    CarregarDadosProponente = True
  
    Set rsSegurado = New Recordset
  
    'Segurado '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    Set rsSegurado = ObterDadosSegurado(Val(txtProposta.Text))
  
    If rsSegurado.EOF Then
        'Call Err.Raise(vbObjectError + 1032, , "CarregarDadosProponente - N�o foi poss�vel obter os dados do segurado")
        Call MsgBox("N�o foi poss�vel obter os dados do segurado", vbCritical)
        rsSegurado.Close
        Set rsSegurado = Nothing
        CarregarDadosProponente = False
        Exit Function
        'FinalizarAplicacao
        'GoTo TrataErro
    End If
  
    txtNome.Text = Trim(rsSegurado("nome") & "")
    txtEndereco.Text = Trim(rsSegurado("endereco") & "")
    txtMunicipio.Text = Trim(rsSegurado("municipio") & "")
    txtBairro.Text = Trim(rsSegurado("bairro") & "")
    txtUF.Text = Trim(rsSegurado("estado") & "")
  
    '  If Not IsNull(rsSegurado("cep")) Then
    '    TxtCEP.Text = Left(Trim(rsSegurado("cep")), 5) & "-" & Right(Trim(rsSegurado("cep")), 3)
    '  End If
  
    txtCEP.Text = MasCEP(Trim(rsSegurado("cep")))
  
    txtDDD.Text = IIf(IsNull(rsSegurado("ddd_1")), "", rsSegurado("ddd_1"))
  
    If Not IsNull(rsSegurado("telefone_1")) Then
  
        sAux = rsSegurado("telefone_1")
        txtTelefone.Text = Right(sAux, 4)
    
        If Len(sAux) = 8 Then
            txtTelefone.Text = Left(sAux, 4) & "-" & txtTelefone.Text
'Autor: romulo.barbosa (Nova Consultoria) - Data da Altera��o: 30/05/2012
'Demanda: 15179437 - Altera��o do campo telefone
        ElseIf Len(sAux) = 9 Then
            txtTelefone.Text = Left(sAux, 5) & "-" & txtTelefone.Text
'Autor: romulo.barbosa (Nova Consultoria) - Data da Altera��o: 30/05/2012
        Else
            txtTelefone.Text = Left(sAux, 3) & "-" & txtTelefone.Text
        End If
  
    End If
  
    txtEmail.Text = IIf(IsNull(rsSegurado("e_mail")), "", rsSegurado("e_mail"))
    txtClienteId.Text = Trim(rsSegurado("cliente_id"))
    txtTpPessoa.Text = Trim(rsSegurado("tp_pessoa") & "")
    txtTpPessoa.Tag = Trim(rsSegurado("tp_pessoa_id") & "")
    txtSexo.Text = Trim(rsSegurado("sexo"))
    txtCodMciTit.Text = IIf(Val(rsSegurado("cod_mci")) = 0, "", Trim(rsSegurado("cod_mci")))
  
    Select Case UCase(Trim(rsSegurado("estado_civil")))

        Case "S"
            txtEstCivil.Text = "SOLTEIRO"

        Case "C"
            txtEstCivil.Text = "CASADO"

        Case "V"
            txtEstCivil.Text = "VIUVO"

        Case "E"
            txtEstCivil.Text = "SEPARADO"

        Case "D"
            txtEstCivil.Text = "DIVORCIADO"

        Case "N"
            txtEstCivil.Text = "N�O INFORMADO"

        Case Else
            txtEstCivil.Text = UCase(Trim(rsSegurado("estado_civil")))
    End Select
    
    If txtTpPessoa.Tag = 1 Then
        txtCPFTit.Text = MasCPF(rsSegurado("cpf_cnpj"))
    
        '*** Inclus�o do Sinalizado de PPE - Pessoas politicamente expostas - demanda 269994
        '*** 26/08/2008 - Fabio (Stefanini)
        If IdentificaPPE(rsSegurado("cpf_cnpj")) Then Me.chkPPE.Value = 1 Else Me.chkPPE.Value = 0
    Else
        txtCPFTit.Text = MasCGC(rsSegurado("cpf_cnpj"))
    End If
  
    If Format(rsSegurado("dt_nascimento"), "dd/mm/yyyy") <> "01/01/1900" Then
        txtDtNascTit.Text = Format(rsSegurado("dt_nascimento"), "dd/mm/yyyy")
        txtIdadeTit.Text = Trim(rsSegurado("idade"))
    End If
  
    TxtCGC.Text = MasCGC(rsSegurado("cpf_cnpj"))
    TxtContato.Text = Trim(rsSegurado("contato"))
    txtNoDocIdentidade.Text = Trim(rsSegurado("doc_identidade"))
    txtTpDocIdentidade.Text = Trim(rsSegurado("tp_doc_identidade"))
    txtDtExpedicaoIdentidade.Text = IIf(Format(rsSegurado("dt_emissao_identidade"), "dd/mm/yyyy") = "01/01/1900", "", Format(rsSegurado("dt_emissao_identidade"), "dd/mm/yyyy"))
    txtOrgaoExpIdentidade.Text = Trim(rsSegurado("exp_doc_identidade"))
    
    'dados de profissao ''''''''''''''
    txtCboTit.Text = Trim(rsSegurado("profissao_cbo"))
        
    'claudia.araujo - demanda 12435268
    If rsSegurado("tp_pessoa_id") = 2 Then
       FrameDadosPF.Visible = True
       rsSegurado.Close
       Set rsSegurado = ObterDadosPessoaFisicaProd718(Val(txtProposta.Text))
       
       
       GridPessoaFisica.Rows = 1
   
       If Not rsSegurado.EOF Then
    
         While Not rsSegurado.EOF
         
     
            sLinha = ""
            sLinha = sLinha & MasCPF(rsSegurado!CPF) & vbTab
            sLinha = sLinha & rsSegurado!Nome & vbTab
            sLinha = sLinha & rsSegurado!dt_nascimento & vbTab
            sLinha = sLinha & rsSegurado!idade & vbTab
            sLinha = sLinha & rsSegurado!sexo & vbTab
            sLinha = sLinha & rsSegurado!grupo
            sLinha = sLinha & vbTab & rsSegurado!cota

           
            GridPessoaFisica.AddItem sLinha
           
            rsSegurado.MoveNext
        
        Wend
   
      End If
  
      'rsSegurado.Close
       
    End If
        
    
    'C�njuge '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    rsSegurado.Close
    Set rsSegurado = ObterDadosConjuge(Val(txtProposta.Text))
  
    If rsSegurado.EOF Then
  
        'escondendo o frame '''''''''''''
        'fraProponenteConjuge.Visible = False
        FrameDadosConjuge.Visible = False
        fraMutuarios.Top = FrameDadosConjuge.Top
    
    Else
    
        ConPropAdesao.FrameDadosConjuge.Visible = True
    
        DoEvents
        'txtNomeConj.Text = Trim(rsSegurado("nome"))
        txtConjuge(1).Text = Trim(rsSegurado("nome"))
        'txtCPFConj.Text = MasCPF(Trim(rsSegurado("cpf")))
        txtConjuge(14).Text = MasCPF(Trim(rsSegurado("cpf")))
    
        '*** Inclus�o do Sinalizado de PPE - Pessoas politicamente expostas - demanda 269994
        '*** 09/12/2008 - ACVCS
        If IdentificaPPE(rsSegurado("cpf")) Then Me.lbltexto(42).Caption = "Sim" Else Me.lbltexto(42).Caption = "N�o"
    
        'txtDtNascConj.Text = Format(rsSegurado("dt_nascimento"), "dd/mm/yyyy")
        txtConjuge(15).Text = Format(rsSegurado("dt_nascimento"), "dd/mm/yyyy")
        'txtIdadeConj.Text = Trim(rsSegurado("idade"))
        txtConjuge(16).Text = Trim(rsSegurado("idade"))
        'txtSexoConj.Text = UCase(Trim(rsSegurado("sexo")))
        txtConjuge(12).Text = UCase(Trim(rsSegurado("sexo")))
    
        'txtEnderecoConjuge.Text = Trim(rsSegurado("endereco"))
        txtConjuge(2).Text = Trim(rsSegurado("endereco"))
        'txtCidadeConjuge.Text = Trim(rsSegurado("municipio"))
        txtConjuge(3).Text = Trim(rsSegurado("municipio"))
        'txtBairroConjuge.Text = Trim(rsSegurado("bairro"))
        txtConjuge(4).Text = Trim(rsSegurado("bairro"))
        'txtUFConjuge.Text = Trim(rsSegurado("estado"))
        txtConjuge(5).Text = Trim(rsSegurado("estado"))
    
        '    If Trim(rsSegurado("cep")) <> "" Then
        '      txtCEPConjuge.Text = Mid(Trim(rsSegurado("cep")), 1, 5) & "-" & Mid(Trim(rsSegurado("cep")), 6, 3)
        '    End If
        'txtCEPConjuge.Text = MasCEP(Trim(rsSegurado("cep")))
        txtConjuge(6).Text = MasCEP(Trim(rsSegurado("cep")))
    
        'txtDDDConjuge.Text = IIf(Val(rsSegurado("ddd_1")) = 0, "", Trim(rsSegurado("ddd_1")))
        txtConjuge(7).Text = IIf(Val(rsSegurado("ddd_1")) = 0, "", Trim(rsSegurado("ddd_1")))
    
        If Val(rsSegurado("telefone_1")) <> 0 Then
    
            sAux = rsSegurado("telefone_1")
            'txtTelefoneConjuge.Text = Right(sAux, 4)
            txtConjuge(8).Text = Right(sAux, 4)
      
            If Len(sAux) = 8 Then
                'txtTelefoneConjuge.Text = Left(sAux, 4) & "-" & txtTelefoneConjuge.Text
                txtConjuge(8).Text = Left(sAux, 4) & "-" & txtConjuge(8).Text
            Else
                'txtTelefoneConjuge.Text = Left(sAux, 3) & "-" & txtTelefoneConjuge.Text
                txtConjuge(8).Text = Left(sAux, 3) & "-" & txtConjuge(8).Text
            End If
    
        End If
    
        'txtEmailConjuge.Text = Trim(rsSegurado("e_mail"))
        txtConjuge(9).Text = Trim(rsSegurado("e_mail"))
        'txtClienteIdConjuge.Text = Trim(rsSegurado("cliente_id"))
        txtConjuge(10).Text = Trim(rsSegurado("cliente_id"))
        'txtCodMciConjuge.Text = IIf(Val(rsSegurado("cod_mci")) = 0, "", Trim(rsSegurado("cod_mci")))
        txtConjuge(11).Text = IIf(Val(rsSegurado("cod_mci")) = 0, "", Trim(rsSegurado("cod_mci")))
    
        Select Case UCase(Trim(rsSegurado("estado_civil")))

            Case "S"
                'txtEstCivilConjuge.Text = "SOLTEIRO"
                txtConjuge(13).Text = "SOLTEIRO"

            Case "C"
                'txtEstCivilConjuge.Text = "CASADO"
                txtConjuge(13).Text = "CASADO"

            Case "V"
                'txtEstCivilConjuge.Text = "VIUVO"
                txtConjuge(13).Text = "VIUVO"

            Case "E"
                'txtEstCivilConjuge.Text = "SEPARADO"
                txtConjuge(13).Text = "SEPARADO"

            Case "D"
                'txtEstCivilConjuge.Text = "DIVORCIADO"
                txtConjuge(13).Text = "DIVORCIADO"

            Case "N"
                'txtEstCivilConjuge.Text = "N�O INFORMADO"
                txtConjuge(13).Text = "N�O INFORMADO"

            Case Else
                'txtEstCivilConjuge.Text = UCase(Trim(rsSegurado("estado_civil")))
                txtConjuge(13).Text = UCase(Trim(rsSegurado("estado_civil")))
        End Select
    
        'ralves
        'FLOW 405744 - ALTERA��O PARA VALIDAR CAMPOS NULL
        'dados de profissao ''''''''''''''''''
        txtCboConj.Text = IIf(IsNull(Trim(rsSegurado("profissao_cbo"))), "", Trim(rsSegurado("profissao_cbo")))
        txtDescProfConj.Text = IIf(IsNull(Trim(rsSegurado("profissao_desc"))), "", Trim(rsSegurado("profissao_desc")))
    
    End If
  
    rsSegurado.Close
    Set rsSegurado = Nothing

    Exit Function
  Resume
TrataErro:
    Call TratarErro("CarregarDadosProponente", Me.name)
    Call FinalizarAplicacao

End Function

Private Function ObterDadosDeclaracaoTitular(lPropostaId As Long, _
                                             lProdutoId As Long) As Recordset
  
    On Error GoTo TrataErro

    Dim sSql As String
  
'    sSql = ""
'    sSql = sSql & "    SELECT item = item_detalhe_objeto_id,                                       " & vbNewLine
'    sSql = sSql & "           pergunta,                                                            " & vbNewLine
'    sSql = sSql & "           resp_titular = isnull(resp_titular,''),                              " & vbNewLine
'    sSql = sSql & "           descricao = isnull(descricao,'')                                     " & vbNewLine
'    sSql = sSql & "      FROM item_detalhe_objeto_tb  WITH (NOLOCK)                                   " & vbNewLine
'    sSql = sSql & " LEFT JOIN decl_titular_tb  WITH (NOLOCK)                                              " & vbNewLine
'    sSql = sSql & "        ON decl_titular_tb.item = item_detalhe_objeto_tb.item_detalhe_objeto_id " & vbNewLine
'    sSql = sSql & "       AND decl_titular_tb.proposta_id = " & lPropostaId & vbNewLine
'    sSql = sSql & "     WHERE item_detalhe_objeto_tb.produto_id = " & lProdutoId & vbNewLine
'    sSql = sSql & "       AND item_detalhe_objeto_tb.dt_fim_vigencia_perg is null" & vbNewLine
'    sSql = sSql & "  ORDER BY item_detalhe_objeto_id " & vbNewLine
    
    sSql = ""
    sSql = sSql & " SET NOCOUNT ON EXEC seguros_db.dbo.SEGS13102_SPS " & lPropostaId & ", " & lProdutoId
  
    Set ObterDadosDeclaracaoTitular = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, sSql, True)

    Exit Function
TrataErro:
    Call TratarErro("ObterDadosDeclaracaoTitular", Me.name)
    Call FinalizarAplicacao

End Function

Private Function ObterDadosDeclaracaoConjuge(lPropostaId As Long, _
                                             lProdutoId As Long) As Recordset

    On Error GoTo TrataErro

    Dim sSql As String

    sSql = ""
    sSql = sSql & "    SELECT item = item_detalhe_objeto_id,                                       " & vbNewLine
    sSql = sSql & "           pergunta,                                                            " & vbNewLine
    sSql = sSql & "           resp_complementar = isnull(resp_complementar,''),                    " & vbNewLine
    sSql = sSql & "           descricao = isnull(descricao,'')                                     " & vbNewLine
    sSql = sSql & "      FROM item_detalhe_objeto_tb  WITH (NOLOCK)                                   " & vbNewLine
    sSql = sSql & " LEFT JOIN decl_complementar_tb  WITH (NOLOCK)                                     " & vbNewLine
    sSql = sSql & "        ON decl_complementar_tb.item = item_detalhe_objeto_tb.item_detalhe_objeto_id " & vbNewLine
    sSql = sSql & "       AND decl_complementar_tb.proposta_id = " & lPropostaId & vbNewLine
    sSql = sSql & " LEFT JOIN proposta_complementar_tb  WITH (NOLOCK)                                              " & vbNewLine
    sSql = sSql & "        ON proposta_complementar_tb.proposta_id = decl_complementar_tb.proposta_id " & vbNewLine
    sSql = sSql & "       AND proposta_complementar_tb.prop_cliente_id = decl_complementar_tb.prop_cliente_id " & vbNewLine
    sSql = sSql & "       AND proposta_complementar_tb.dt_inicio_vigencia = decl_complementar_tb.dt_inicio_vigencia " & vbNewLine
    sSql = sSql & "       AND proposta_complementar_tb.dt_fim_vigencia IS NULL " & vbNewLine
    sSql = sSql & "     WHERE item_detalhe_objeto_tb.produto_id = " & lProdutoId & vbNewLine
    sSql = sSql & "       AND item_detalhe_objeto_tb.dt_fim_vigencia_perg is null" & vbNewLine
    sSql = sSql & " ORDER BY item "

    Set ObterDadosDeclaracaoConjuge = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, sSql, True)

    Exit Function

TrataErro:
    Call TratarErro("ObterDadosDeclaracaoConjuge", Me.name)
    Call FinalizarAplicacao

End Function

Private Function CarregarDadosObjetoRisco() As Boolean

    On Error GoTo TrataErro

    Dim rsobjetoRisco As Recordset
    Dim iItem As Integer
    
    CarregarDadosObjetoRisco = True
    
    Set rsobjetoRisco = New Recordset
    
    'Declara��o Titular ''''''''''''''''''''''''''''''''''''''''''''''
    Set rsobjetoRisco = ObterDadosDeclaracaoTitular(Val(txtProposta.Text), Val(txtProduto.Tag))
    
    iItem = 1
    
    While Not rsobjetoRisco.EOF
    
        'iItem = Val(rsobjetoRisco("item"))
      
        Call lvwDeclaracaoTitular.ListItems.Add(iItem, , Val(rsobjetoRisco("item")))
             
        If UCase(Trim(rsobjetoRisco("resp_titular"))) = "S" Then
            lvwDeclaracaoTitular.ListItems.Item(iItem).Checked = True
        End If
      
        Call lvwDeclaracaoTitular.ListItems.Item(iItem).ListSubItems.Add(1, , Trim(rsobjetoRisco("pergunta")))
        Call lvwDeclaracaoTitular.ListItems.Item(iItem).ListSubItems.Add(2, , Trim(rsobjetoRisco("resp_titular")))
        Call lvwDeclaracaoTitular.ListItems.Item(iItem).ListSubItems.Add(3, , Trim(rsobjetoRisco("descricao")))
      
        iItem = iItem + 1
      
        rsobjetoRisco.MoveNext
    Wend
    
    'Declaracao Conjuge '''''''''''''''''''''''''''''''''''''''''''''''
    rsobjetoRisco.Close
    Set rsobjetoRisco = ObterDadosDeclaracaoConjuge(Val(txtProposta.Text), Val(txtProduto.Tag))
    
    If rsobjetoRisco.EOF Then
        fraDeclConjuge.Visible = False
    End If
     
    iItem = 1
     
    While Not rsobjetoRisco.EOF
    
        'iItem = Val(rsobjetoRisco("item"))
            
        Call lvwDeclaracaoConjuge.ListItems.Add(iItem, , Val(rsobjetoRisco("item")))
      
        'Call lvwDeclaracaoConjuge.ListItems.Add(iItem, , CStr(iItem))
      
        If UCase(Trim(rsobjetoRisco("resp_complementar"))) = "S" Then
            lvwDeclaracaoConjuge.ListItems.Item(iItem).Checked = True
        End If
      
        Call lvwDeclaracaoConjuge.ListItems.Item(iItem).ListSubItems.Add(1, , Trim(rsobjetoRisco("pergunta")))
        Call lvwDeclaracaoConjuge.ListItems.Item(iItem).ListSubItems.Add(2, , Trim(rsobjetoRisco("resp_complementar")))
        Call lvwDeclaracaoConjuge.ListItems.Item(iItem).ListSubItems.Add(3, , Trim(rsobjetoRisco("descricao")))
      
        iItem = iItem + 1
      
        rsobjetoRisco.MoveNext
    Wend
    
    'Vidas Seguradas ''''''''''''''''''''''''''
    Call CarregarDadosVidasSeguradas
    
    'Finalizando ''''''''''''''''''''''''''''''
    rsobjetoRisco.Close
    Set rsobjetoRisco = Nothing
    
    Exit Function
  
TrataErro:
    Call TratarErro("CarregarDadosObjetoRisco", Me.name)
    Call FinalizarAplicacao
End Function

Private Sub CarregarDadosVidasSeguradas()

    On Error GoTo TrataErro

    Dim rsVidaSegurada As Recordset
    Dim sLinha As String

    'Vidas Seguradas ''''''''''''''''''''''''''
    If colSubgrupos.Count > 0 Then
    
        Set rsVidaSegurada = New Recordset
        Set rsVidaSegurada = ObterDadosVidasSeguradas(cboSubgrupo(1).ItemData(cboSubgrupo(1).ListIndex), Val(txtApolice.Text), Val(txtRamo.Tag), Val(txtSucursal.Tag), Val(txtSeguradora.Tag))
      
        If rsVidaSegurada.EOF Then
            'Call Err.Raise(vbObjectError + 1033, , "CarregarDadosVidasSeguradas - N�o foi poss�vel obter as vidas seguradas")
            Call MsgBox("N�o foi poss�vel obter as vidas seguradas", vbCritical)
            rsVidaSegurada.Close
            Set rsVidaSegurada = Nothing
            Exit Sub
            'FinalizarAplicacao
            'GoTo TrataErro
        End If
      
        While Not rsVidaSegurada.EOF
      
            sLinha = ""
            sLinha = sLinha & rsVidaSegurada("nome") & vbTab
            sLinha = sLinha & rsVidaSegurada("cliente_id") & vbTab
            sLinha = sLinha & rsVidaSegurada("val_salario") & vbTab
            sLinha = sLinha & rsVidaSegurada("val_capital_segurado") & vbTab
            sLinha = sLinha & rsVidaSegurada("dt_inicio_vigencia_sbg") & vbTab
            sLinha = sLinha & rsVidaSegurada("dt_fim_vigencia_sbg") & vbTab
            sLinha = sLinha & rsVidaSegurada("situacao")
      
            Call gridVidasSeguradas.AddItem(sLinha)
        
            rsVidaSegurada.MoveNext
      
        Wend
      
        Set rsVidaSegurada = Nothing
      
    End If

    Exit Sub
  
TrataErro:
    Call TratarErro("CarregarDadosVidasSeguradas", Me.name)
    Call FinalizarAplicacao
End Sub

Private Function ObterDadosVidasSeguradas(iSubgrupoId As Integer, _
                                          lApoliceId As Long, _
                                          LramoId As Long, _
                                          lSucursalId As Long, _
                                          lSeguradoraId As Long) As Recordset

    On Error GoTo TrataErro

    Dim sSql As String
  
    sSql = ""
    sSql = sSql & " SELECT sgp.proposta_adesao_id,                                                                              " & vbNewLine
    sSql = sSql & "        cl.nome,                                                                                             " & vbNewLine
    sSql = sSql & "        cl.cliente_id,                                                                                       " & vbNewLine
    sSql = sSql & "        cl.cpf_cnpj,                                                                                         " & vbNewLine
    sSql = sSql & "        isnull(sgp.val_salario,0) val_salario,                                                               " & vbNewLine
    sSql = sSql & "        isnull(sgp.val_capital_segurado,0) val_capital_segurado,                                             " & vbNewLine
    sSql = sSql & "        isnull(sgp.dt_inicio_vigencia_sbg,'') dt_inicio_vigencia_sbg,                                        " & vbNewLine
    sSql = sSql & "        isnull(sgp.dt_fim_vigencia_sbg,'') dt_fim_vigencia_sbg,                                              " & vbNewLine
    sSql = sSql & "        situacao = CASE                                                                                      " & vbNewLine
    sSql = sSql & "                      WHEN dt_fim_vigencia_sbg IS NULL  THEN 'Ativa'                                         " & vbNewLine
    sSql = sSql & "                      ELSE 'Inativa'                                                                         " & vbNewLine
    sSql = sSql & "                   END                                                                                       " & vbNewLine
    sSql = sSql & "   FROM seguro_vida_sub_grupo_tb sgp   WITH (NOLOCK)                                                                " & vbNewLine
    sSql = sSql & "   JOIN cliente_tb cl   WITH (NOLOCK)                                                                               " & vbNewLine
    sSql = sSql & "     ON sgp.prop_cliente_id = cl.cliente_id                                                                  " & vbNewLine
    sSql = sSql & "  WHERE sgp.sub_grupo_id = " & iSubgrupoId & vbNewLine
    sSql = sSql & "    AND dt_fim_vigencia_sbg is null                                                                          " & vbNewLine
    sSql = sSql & "    AND apolice_id = " & lApoliceId & vbNewLine
    sSql = sSql & "    AND sucursal_seguradora_id = " & lSucursalId & vbNewLine
    sSql = sSql & "    AND seguradora_cod_susep =  " & lSeguradoraId & vbNewLine
    sSql = sSql & "    AND ramo_id = " & LramoId & vbNewLine
    sSql = sSql & "    AND sgp.dt_inicio_vigencia_sbg = (                                                                       " & vbNewLine
    sSql = sSql & "                                       SELECT MAX(sgp2.dt_inicio_vigencia_sbg)                               " & vbNewLine
    sSql = sSql & "                                         FROM seguro_vida_sub_grupo_tb sgp2   WITH (NOLOCK)                         " & vbNewLine
    sSql = sSql & "                                        WHERE sgp2.apolice_id = sgp.apolice_id                               " & vbNewLine
    sSql = sSql & "                                          AND sgp2.dt_fim_vigencia_sbg is null                               " & vbNewLine
    sSql = sSql & "                                          AND sgp2.sucursal_seguradora_id = sgp.sucursal_seguradora_id       " & vbNewLine
    sSql = sSql & "                                          AND sgp2.seguradora_cod_susep = sgp.seguradora_cod_susep           " & vbNewLine
    sSql = sSql & "                                          AND sgp2.ramo_id = sgp.ramo_id                                     " & vbNewLine
    sSql = sSql & "                                          AND sgp2.sub_grupo_id = sgp.sub_grupo_id                           " & vbNewLine
    sSql = sSql & "                                          AND sgp2.proposta_id = sgp.proposta_id                             " & vbNewLine
    sSql = sSql & "                                          AND sgp2.prop_cliente_id = sgp.prop_cliente_id                     " & vbNewLine
    sSql = sSql & "                                          AND sgp2.tp_componente_id = sgp.tp_componente_id                   " & vbNewLine
    sSql = sSql & "                                          AND sgp2.seq_canc_endosso_seg = sgp.seq_canc_endosso_seg           " & vbNewLine
    sSql = sSql & "                                          AND sgp2.sub_grupo_id = sgp.sub_grupo_id                           " & vbNewLine
    sSql = sSql & "                                      )                                                                      " & vbNewLine
    sSql = sSql & "                                                                                                             " & vbNewLine
    sSql = sSql & "   ORDER BY cl.Nome                                                                                          " & vbNewLine
  
    Set ObterDadosVidasSeguradas = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, sSql, True)
  
    Exit Function
  
TrataErro:
    Call TratarErro("CarregarDadosObjetoRisco", Me.name)
    Call FinalizarAplicacao
End Function

Private Function CarregarDadosObjetoRiscoRE() As Boolean

    On Error GoTo TrataErro

    CarregarDadosObjetoRiscoRE = True
  
    Select Case Val(txtProduto.Tag)
  
        Case ProdIdOuroAgricola: CarregarDadosObjetoRiscoRE = CarregarDadosAgricola(Val(txtProposta.Text))

        Case ProdIdPenhorRural: CarregarDadosObjetoRiscoRE = CarregarDadosPenhorRural(Val(txtProposta.Text))

        Case ProdIdSeguroConsorcio: CarregarDadosObjetoRiscoRE = CarregarDadosSeguroConsorcio(Val(txtProposta.Text))
    
    End Select

    Exit Function
  
TrataErro:
    Call TratarErro("CarregarDadosObjetoRiscoRE", Me.name)
    Call FinalizarAplicacao
  
End Function

Private Function CarregarDadosSeguroConsorcio(ByVal lPropostaId As Long) As Boolean

    On Error GoTo TrataErro
  
    Dim rsConsorcio As Recordset
    Dim sLinha As String

    CarregarDadosSeguroConsorcio = True

    Set rsConsorcio = New Recordset
  
    'Dados do Segmento''''''''
    Set rsConsorcio = ObterDadosSegmento(lPropostaId)
  
    If rsConsorcio.EOF Then
        'Call Err.Raise(vbObjectError + 1050, , "N�o foi poss�vel obter dados do seguro cons�rcio")
        Call MsgBox("N�o foi poss�vel obter dados do seguro cons�rcio", vbCritical)
        rsConsorcio.Close
        Set rsConsorcio = Nothing
        CarregarDadosSeguroConsorcio = False
        Exit Function
        'FinalizarAplicacao
    End If
  
    txtConsorcio(0).Text = IIf(IsNull(rsConsorcio("CODIGO_SEGMENTO")), "", rsConsorcio("CODIGO_SEGMENTO"))
    txtConsorcio(1).Text = IIf(IsNull(rsConsorcio("DESCRICAO")), "", rsConsorcio("DESCRICAO"))
    txtConsorcio(2).Text = IIf(IsNull(rsConsorcio("GRUPO")), "", rsConsorcio("GRUPO"))
    txtConsorcio(3).Text = IIf(IsNull(rsConsorcio("COTA")), "", rsConsorcio("COTA"))
    txtConsorcio(4).Text = IIf(IsNull(rsConsorcio("TIPO_PLANO")), "", rsConsorcio("TIPO_PLANO"))
    txtConsorcio(5).Text = IIf(IsNull(rsConsorcio("CODIGO_BEM")), "", rsConsorcio("CODIGO_BEM"))
    txtConsorcio(6).Text = IIf(IsNull(rsConsorcio("DESCRICAO_BEM")), "", rsConsorcio("DESCRICAO_BEM"))
    txtConsorcio(7).Text = IIf(IsNull(rsConsorcio("DT_INICIO_VIGENCIA")), "", Format(rsConsorcio("DT_INICIO_VIGENCIA"), "dd/mm/yyyy"))
    txtConsorcio(8).Text = IIf(IsNull(rsConsorcio("DT_FIM_VIGENCIA")), "", Format(rsConsorcio("DT_FIM_VIGENCIA"), "dd/mm/yyyy"))
    txtConsorcio(9).Text = IIf(IsNull(rsConsorcio("DT_ADESAO")), "", Format(rsConsorcio("DT_ADESAO"), "dd/mm/yyyy"))
    txtConsorcio(10).Text = IIf(IsNull(rsConsorcio("DT_PRIMEIRA_ASSEMBLEIA")), "", Format(rsConsorcio("DT_PRIMEIRA_ASSEMBLEIA"), "dd/mm/yyyy"))
    
    'Dados do Grid '''''''''''
    rsConsorcio.Close
    Set rsConsorcio = ObterDadosConsorcio(lPropostaId)
  
    If rsConsorcio.EOF Then
        'Call Err.Raise(vbObjectError + 1051, , "N�o foi poss�vel obter dados do seguro cons�rcio (2)")
        rsConsorcio.Close
        Set rsConsorcio = Nothing
        'CarregarDadosSeguroConsorcio = False
        Exit Function
    End If
  
    While Not rsConsorcio.EOF
  
        sLinha = ""
        sLinha = sLinha & rsConsorcio(0) & vbTab
        sLinha = sLinha & rsConsorcio(1) & vbTab
        sLinha = sLinha & rsConsorcio(2) & vbTab
        sLinha = sLinha & rsConsorcio(3) & vbTab
        sLinha = sLinha & rsConsorcio(4) & vbTab
        sLinha = sLinha & rsConsorcio(5) & vbTab
        sLinha = sLinha & rsConsorcio(6) & vbTab
        sLinha = sLinha & rsConsorcio(7) & vbTab
        sLinha = sLinha & rsConsorcio(8) & vbTab
        'Alterado por Cleber da Stefanini - data: 24/08/2005
        'A pedido da Mary, adicionar o campo Arquivo no grid
        sLinha = sLinha & rsConsorcio(9) & vbTab
        '---------------------------------------------------
        
        grdConsorcio.AddItem sLinha

        rsConsorcio.MoveNext
    Wend
  
    rsConsorcio.Close
    Set rsConsorcio = Nothing

    Exit Function
  
TrataErro:
    Call TratarErro("CarregarDadosSeguroConsorcio", Me.name)
    Call FinalizarAplicacao

End Function

Private Function ObterDadosConsorcio(ByVal lPropostaId As Long) As Recordset

    On Error GoTo TrataErro

    Dim sSql As String
  
    sSql = ""
    sSql = sSql & "SELECT " & vbNewLine
    sSql = sSql & "       A.DT_BAIXA AS DT_PAGAMENTO, " & vbNewLine
    sSql = sSql & "       SITUACAO =  " & vbNewLine
    sSql = sSql & "       case a.situacao " & vbNewLine
    sSql = sSql & "       when 'p' then 'Pendente' " & vbNewLine
    sSql = sSql & "       when 'a' then 'Adimplente' end, " & vbNewLine
    sSql = sSql & "       CONVERT(VARCHAR(10),S.DT_INCLUSAO, 103) AS DT_INCLUSAO, " & vbNewLine
    sSql = sSql & "       S.NUM_PARCELA, " & vbNewLine
    sSql = sSql & "       S.SALDO_DEVEDOR, " & vbNewLine
    sSql = sSql & "       CONVERT(VARCHAR(10),S.DT_AGENDAMENTO, 103) AS  DT_AGENDAMENTO, " & vbNewLine
    sSql = sSql & "       CONVERT(VARCHAR(10),S.DT_ASSEMBLEIA, 103) AS  DT_ASSEMBLEIA, " & vbNewLine
    sSql = sSql & "       CONVERT(VARCHAR(10),S.DT_OPERACAO, 103) AS  DT_OPERACAO, " & vbNewLine
    sSql = sSql & "       A.VAL_COBRANCA " & vbNewLine
    'Alterado por Cleber da Stefanini - data: 24/08/2005
    'A pedido da Mary, adicionar o campo Arquivo no grid
    sSql = sSql & "       , E.ARQUIVO " & vbNewLine
    '---------------------------------------------------
    sSql = sSql & "FROM   SEGURO_CONSORCIO_TB S  WITH (NOLOCK)  " & vbNewLine
    sSql = sSql & "INNER  JOIN    AGENDAMENTO_COBRANCA_TB A   WITH (NOLOCK)  " & vbNewLine
    sSql = sSql & "ON (   S.PROPOSTA_ID  = A.PROPOSTA_ID " & vbNewLine
    'Alterado por Cleber da Stefanini - data: 24/08/2005
    'A pedido da Mary, adicionar o campo Arquivo no grid
    'Estava sendo feito errado o relacionamento entre a AGENDAMENTO_COBRANCA_TB e
    '   a SEGURO_CONSORCIO_TB  que estava sendo feito com o num_cobranca e o codigo_segmento
    '   e o certo � utilizar num_cobranca com o num_parcela.
    'Para pegar o nome do arquivo foi necess�rio adicionar o relacionamento entre
    '   a emi_proposta_tb e emi_conosorcio_tb.
    'sSQL = sSQL & "   AND A.NUM_COBRANCA =  S.CODIGO_SEGMENTO) " & vbNewLine
    sSql = sSql & "   AND A.NUM_COBRANCA =  S.NUM_PARCELA) " & vbNewLine
    sSql = sSql & " INNER JOIN EMI_PROPOSTA_TB E  WITH (NOLOCK)  " & vbNewLine
    sSql = sSql & " ON  E.PROPOSTA_ID = A.PROPOSTA_ID " & vbNewLine
    'FLOWS 12157773 e 12121133 - CONFITEC - gcamara, jadao, mferreira - 29/08/2011
    sSql = sSql & " AND ISNULL(E.ENDOSSO_ID,0) = A.NUM_ENDOSSO " & vbNewLine
'    sSql = sSql & " INNER JOIN EMI_CONSORCIO_TB C  WITH (NOLOCK)  " & vbNewLine
'    sSql = sSql & " ON  C.EMI_PROPOSTA_ID = E.EMI_PROPOSTA_ID " & vbNewLine
'    sSql = sSql & " AND (C.NUM_PARCELA = A.NUM_COBRANCA OR (C.NUM_PARCELA = 0 AND C.num_parcela_grupo_consorcio = A.NUM_COBRANCA)) " & vbNewLine 'FLOW 12157773 - Marcelo Ferreira - Confitec Sistemas 2011-08-25
'    sSql = sSql & " AND     C.CODIGO_SEGMENTO = S.CODIGO_SEGMENTO " & vbNewLine
    '---------------------------------------------------
    sSql = sSql & "WHERE  S.PROPOSTA_ID = " & lPropostaId & vbNewLine
    'Alterado por Cleber da Stefanini - data: 24/08/2005
    'Ordena��o decrescente pelo numero parcela pedido da Mary
    'sSql = sSql & " order by S.NUM_PARCELA desc "
    '---------------------------------------------------
    'GENJUNIOR - FLOW 17860335
    'UNION COM TABELAS DE EXPURGO
    '10/10/2013
    sSql = sSql & " UNION " & vbNewLine
    sSql = sSql & "SELECT A.DT_BAIXA AS DT_PAGAMENTO " & vbNewLine
    sSql = sSql & "     , SITUACAO = CASE a.situacao WHEN 'p' THEN 'Pendente' WHEN 'a' THEN 'Adimplente' END " & vbNewLine
    sSql = sSql & "     , CONVERT(VARCHAR(10), S.DT_INCLUSAO, 103) AS DT_INCLUSAO " & vbNewLine
    sSql = sSql & "     , S.NUM_PARCELA " & vbNewLine
    sSql = sSql & "     , S.SALDO_DEVEDOR " & vbNewLine
    sSql = sSql & "     , CONVERT(VARCHAR(10),S.DT_AGENDAMENTO, 103) AS DT_AGENDAMENTO " & vbNewLine
    sSql = sSql & "     , CONVERT(VARCHAR(10),S.DT_ASSEMBLEIA, 103) AS DT_ASSEMBLEIA " & vbNewLine
    sSql = sSql & "     , CONVERT(VARCHAR(10),S.DT_OPERACAO, 103) AS DT_OPERACAO " & vbNewLine
    sSql = sSql & "     , A.VAL_COBRANCA " & vbNewLine
    sSql = sSql & "     , E.ARQUIVO " & vbNewLine
    sSql = sSql & "  FROM SEGURO_CONSORCIO_TB S  WITH (NOLOCK)  " & vbNewLine
    sSql = sSql & " INNER JOIN AGENDAMENTO_COBRANCA_TB A  WITH (NOLOCK)  " & vbNewLine
    sSql = sSql & "    ON (S.PROPOSTA_ID  = A.PROPOSTA_ID " & vbNewLine
    sSql = sSql & "   AND A.NUM_COBRANCA =  S.NUM_PARCELA) " & vbNewLine
    sSql = sSql & " INNER JOIN SEGUROS_HIST_DB.DBO.EMI_PROPOSTA_HIST_TB E  WITH (NOLOCK)  " & vbNewLine
    sSql = sSql & " ON  E.PROPOSTA_ID = A.PROPOSTA_ID " & vbNewLine
    sSql = sSql & " AND ISNULL(E.ENDOSSO_ID,0) = A.NUM_ENDOSSO " & vbNewLine
    sSql = sSql & "WHERE  S.PROPOSTA_ID = " & lPropostaId & vbNewLine
    sSql = sSql & " order by S.NUM_PARCELA desc "
    
    
  
    Set ObterDadosConsorcio = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, sSql, True)

    Exit Function
  
TrataErro:
    Call TratarErro("ObterDadosConsorcio", Me.name)
    Call FinalizarAplicacao

End Function

Private Function ObterDadosSegmento(ByVal lPropostaId As Long) As Recordset

    On Error GoTo TrataErro

    Dim sSql As String
  
    sSql = ""
    sSql = sSql & "SELECT TOP 1 " & vbNewLine
    sSql = sSql & "S.CODIGO_SEGMENTO, " & vbNewLine
    sSql = sSql & "S.GRUPO, " & vbNewLine
    sSql = sSql & "S.COTA, " & vbNewLine
    sSql = sSql & "S.TIPO_PLANO, " & vbNewLine
    sSql = sSql & "S.CODIGO_BEM, " & vbNewLine
    sSql = sSql & "S.DESCRICAO_BEM, " & vbNewLine
    sSql = sSql & "S.DT_PRIMEIRA_ASSEMBLEIA, " & vbNewLine
    sSql = sSql & "S.DT_ADESAO, " & vbNewLine
    sSql = sSql & "SEGURO = CASE S.TP_SEGURO" & vbNewLine
    sSql = sSql & "         WHEN 1 THEN 'PRESTAMISTA'" & vbNewLine
    sSql = sSql & "         WHEN 2 THEN 'QUEBRA DE GARANTIA'" & vbNewLine
    sSql = sSql & "         END, " & vbNewLine
    sSql = sSql & "S.DT_INICIO_VIGENCIA, " & vbNewLine
    sSql = sSql & "S.DT_FIM_VIGENCIA, " & vbNewLine
    sSql = sSql & "C.DESCRICAO " & vbNewLine
    sSql = sSql & "      FROM SEGURO_CONSORCIO_TB S  WITH (NOLOCK)  " & vbNewLine
    sSql = sSql & "INNER JOIN SEGMENTO_CONSORCIO_TB C   WITH (NOLOCK)  " & vbNewLine
    sSql = sSql & "        ON  (S.CODIGO_SEGMENTO = C.COD_SEGMENTO ) " & vbNewLine
    sSql = sSql & "WHERE PROPOSTA_ID = " & lPropostaId & vbNewLine
  
    Set ObterDadosSegmento = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, sSql, True)

    Exit Function
  
TrataErro:
    Call TratarErro("ObterDadosSegmento", Me.name)
    Call FinalizarAplicacao

End Function

Private Function ObterDadosEndosso(lPropostaId As Long) As Recordset

    On Error GoTo TrataErro

    Dim sSql As String
  
    sSql = ""
    sSql = sSql & "    SELECT endosso_tb.endosso_id, " & vbNewLine
    sSql = sSql & "           tp_endosso_tb.descricao, " & vbNewLine
    sSql = sSql & "           tp_endosso_tb.tp_endosso_id, " & vbNewLine
    'Edilson silva - SD01957076 - inicio
    'sSql = sSql & "           endosso_tb.dt_emissao, " & vbNewLine
    sSql = sSql & "           endosso_tb.dt_emissao as dt_emissao2, " & vbNewLine
    sSql = sSql & "           isnull(endosso_financeiro_tb.dt_contabilizacao_emissao, endosso_tb.dt_emissao) dt_emissao, " & vbNewLine
    'Edilson silva - SD01957076 - fim
    sSql = sSql & "           endosso_tb.dt_pedido_endosso, " & vbNewLine
    sSql = sSql & "           ISNULL(endosso_tb.num_endosso_bb,0) endosso_bb, " & vbNewLine
    sSql = sSql & "           ISNULL(endosso_tb.descricao_endosso,'') descricao_endosso, " & vbNewLine
    sSql = sSql & "           ISNULL(endosso_tb.usuario,'') usuario, " & vbNewLine
    sSql = sSql & "           proposta_tb.produto_id, " & vbNewLine
    sSql = sSql & "           endosso_tb.tp_alteracao_endosso " & vbNewLine   '19/10/2018  - rfarzat - Endosso 252 - Sala Agil
    sSql = sSql & "      FROM endosso_tb  WITH (NOLOCK) " & vbNewLine
    sSql = sSql & "INNER JOIN tp_endosso_tb  WITH (NOLOCK) " & vbNewLine
    sSql = sSql & "        ON tp_endosso_tb.tp_endosso_id = endosso_tb.tp_endosso_id " & vbNewLine
    sSql = sSql & "INNER JOIN proposta_tb  WITH (NOLOCK) " & vbNewLine
    sSql = sSql & "        ON proposta_tb.proposta_id = endosso_tb.proposta_id " & vbNewLine
    'Edilson silva - SD01957076 - inicio
    sSql = sSql & "LEFT JOIN endosso_financeiro_tb  WITH (NOLOCK) " & vbNewLine
    sSql = sSql & "        ON endosso_financeiro_tb.proposta_id = endosso_tb.proposta_id  " & vbNewLine
    sSql = sSql & "        AND endosso_financeiro_tb.endosso_id = endosso_tb.endosso_id  " & vbNewLine
    'Edilson silva - SD01957076 - fim
    sSql = sSql & "     WHERE endosso_tb.proposta_id = " & lPropostaId & vbNewLine
    sSql = sSql & "  ORDER BY endosso_id, dt_emissao DESC " & vbNewLine

    Set ObterDadosEndosso = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, sSql, True)

    Exit Function

TrataErro:
    Call TratarErro("ObterDadosEndosso", Me.name)
    Call FinalizarAplicacao
End Function

Private Function CarregarDadosEndosso() As Boolean

    On Error GoTo TrataErro

    Dim rsEndosso As Recordset
    Dim rsContratacao As Recordset
    Dim sLinha As String
    Dim sSql As String
    Dim rsNomeUsuario As Recordset
    
    'Edilson silva - SD01957076 - inicio
    Dim rsProposta As Recordset
    Dim Sql As String
    'Edilson silva - SD01957076 - fim
  
    CarregarDadosEndosso = True
  
    Set rsEndosso = New Recordset
    Set rsEndosso = ObterDadosEndosso(Val(txtProposta.Text))
    Set rsContratacao = ObterDadosProposta(Val(txtProposta.Text))
  
    '  If rsEndosso.EOF Then
    '    Call MsgBox("N�o foi poss�vel obter os dados dos endossos", vbCritical)
    '    GoTo TrataErro
    '  End If
    
    
    'Edilson silva - SD01957076 - inicio
    If rsContratacao("produto_id") = 1198 Then
        Sql = ""
        Sql = Sql & "SELECT p.dt_contabilizacao_emissao as data_emissao" & vbNewLine
        Sql = Sql & "FROM     proposta_tb p  WITH (NOLOCK) " & vbNewLine
        Sql = Sql & "WHERE    p.proposta_id = " & txtProposta.Text & vbNewLine
          
        Set rsProposta = New Recordset
        Set rsProposta = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, Sql, True)
      
    End If
   


    If InStr(1, "1225,1231,1205,1198,1235,1236,1237", txtProduto.Tag) > 0 Then   'MU-2017-060371-adequacoes-no-segp0176
     
     
        grdEndosso.Rows = 1
  
            sLinha = ""
            sLinha = sLinha & Trim(("0")) & vbTab
            
            If rsContratacao("produto_id") = 1198 Then
                sLinha = sLinha & Format(rsProposta("data_emissao"), "dd/mm/yyyy") & vbTab
            Else
                sLinha = sLinha & Format(rsContratacao("dt_contratacao"), "dd/mm/yyyy") & vbTab
            End If
            'Edilson silva - SD01957076 - fim
            sLinha = sLinha & Trim(("0")) & vbTab
            sLinha = sLinha & Trim(("Contrata��o")) & vbTab
            
        grdEndosso.AddItem sLinha
        
    End If
    
     'Edilson silva - SD01957076 - fim
  
  
   
    'tratamento de erro de rsEndosso confitec mc15/10/2014
   If Not rsEndosso.EOF Then
  
    If rsEndosso("produto_id") = 1225 Then
  
            sLinha = ""
            sLinha = sLinha & Trim(("0")) & vbTab
            sLinha = sLinha & Format(rsContratacao("dt_contratacao"), "dd/mm/yyyy") & vbTab
            sLinha = sLinha & Trim(("0")) & vbTab
            sLinha = sLinha & Trim(("Contrata��o")) & vbTab
            
        grdEndosso.AddItem sLinha
  
    End If
  
    While Not rsEndosso.EOF
  
        sLinha = ""
        sLinha = sLinha & Trim(rsEndosso("endosso_id")) & vbTab
        sLinha = sLinha & Format(rsEndosso("dt_emissao"), "dd/mm/yyyy") & vbTab
        sLinha = sLinha & Trim(rsEndosso("tp_endosso_id")) & vbTab
        sLinha = sLinha & Trim(rsEndosso("descricao")) & vbTab
        sLinha = sLinha & Format(rsEndosso("dt_pedido_endosso"), "dd/mm/yyyy") & vbTab
        sLinha = sLinha & IIf(rsEndosso("endosso_bb") = 0, "", Trim(rsEndosso("endosso_bb"))) & vbTab
        sLinha = sLinha & Trim(rsEndosso("descricao_endosso")) & vbTab
        
        'Edilson silva - SD01957076 - inicio
        If UCase(Trim(rsEndosso("usuario"))) = "PRODUCAO2" Or Trim(rsEndosso("usuario")) = "99988877714" Then
            sLinha = sLinha & "Usu�rio Produ��o" & vbTab
        Else
            sSql = ""
            sSql = sSql & " SELECT DISTINCT nome FROM segab_db..usuario_tb" & vbNewLine
            sSql = sSql & " WHERE cpf = '" & UCase(Trim(rsEndosso("usuario"))) & "'" & vbNewLine
            sSql = sSql & " OR UPPER(login_rede) = upper('" & UCase(Trim(rsEndosso("usuario"))) & "')"

            Set rsNomeUsuario = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, sSql, True)

            If Not rsNomeUsuario.EOF Then
                sLinha = sLinha & rsNomeUsuario("nome") & vbTab
            Else
                sLinha = sLinha & vbTab
            End If

            rsNomeUsuario.Close

        End If
        
        
        sLinha = sLinha & vbTab & Format(rsEndosso("dt_emissao2"), "dd/mm/yyyy") & vbTab
        'Edilson silva - SD01957076 - fim
    
        grdEndosso.AddItem sLinha
  
        rsEndosso.MoveNext
    
    Wend
  
End If
    rsEndosso.Close
    Set rsEndosso = Nothing
  
    grdEndosso.ColWidth(2) = 0 'tp_endosso_id

    Exit Function
  
TrataErro:
    Call TratarErro("CarregarDadosEndosso", Me.name)
    Call FinalizarAplicacao

End Function
'Amanda Viana - Confitec - 16/10/2019 - circula 365 - Carrega dados numero contrato opera��o cr�dito
Private Function CarregarDadosContratoOpCredito(propostaId As Long) As Boolean
    
    On Error GoTo TrataErro
    Dim sSql As String

    Dim rsContrato As Recordset
        
        CarregarDadosContratoOpCredito = True
        
    
    If Val(txtProduto.Tag) = ProdIdOurovidaProdutorRural Or Val(txtProduto.Tag) = ProdIdBBSeguroVidaAgriculturaFamiliar Then
        sSql = sSql & _
        "Select  num_contrato = num_contrato, b.proposta_id,MAX (a.dt_contratacao) " & _
            "from seguros_db..emi_proposta_tb a " & _
        "inner join proposta_tb b " & _
        "   on a.proposta_id = b.proposta_id " & _
        "Where " & _
        "  b.proposta_id = " & propostaId & _
        "  and a.dt_contratacao is not null  " & _
        "  and isnull(num_contrato,0)<> 0 " & _
        "group by b.proposta_id, num_contrato  "

     End If
     
                   
        Set rsContrato = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, sSql, True)
        
        
        If Not rsContrato.EOF Then
            txtContratoOpCredito.Text = rsContrato("num_contrato")
        End If
    
    
 Exit Function
    
TrataErro:
    Call TratarErro("CarregarDadosContratoOpCredito", Me.name)
    Call FinalizarAplicacao

End Function

Private Function CarregarDadosMutuario() As Boolean

    On Error GoTo TrataErro

    Dim rsMututario As Recordset
    Dim sLinha As String
    Dim sSexo As String

    CarregarDadosMutuario = True
  
    Call configurarGridMutuario
  
    Set rsMututario = New Recordset
    Set rsMututario = ObterDadosMutuario(Val(txtProposta.Text))
  
    If Not rsMututario.EOF Then
  
        Do While Not rsMututario.EOF
      
            sSexo = Trim(rsMututario("sexo"))

            If sSexo = "M" Then
                sSexo = "Masculino"
            ElseIf sSexo = "F" Then
                sSexo = "Feminino"
            Else
                sSexo = "N�o definido"
            End If
      
            If grdMutuario.Rows = 2 And grdMutuario.TextMatrix(1, 0) = "" Then
            
                grdMutuario.TextMatrix(1, 0) = Trim(rsMututario("nome"))
                grdMutuario.TextMatrix(1, 1) = Trim(rsMututario("CPF"))
                grdMutuario.TextMatrix(1, 2) = sSexo
                grdMutuario.TextMatrix(1, 3) = Format(Trim(rsMututario("dt_nascimento")), "dd/mm/yyyy")
                grdMutuario.TextMatrix(1, 4) = Trim(rsMututario("endereco"))
                grdMutuario.TextMatrix(1, 5) = Trim(rsMututario("bairro"))
                grdMutuario.TextMatrix(1, 6) = Trim(rsMututario("Municipio"))
                grdMutuario.TextMatrix(1, 7) = Trim(rsMututario("estado"))
                grdMutuario.TextMatrix(1, 8) = Trim(rsMututario("cep"))
                grdMutuario.TextMatrix(1, 9) = Trim(rsMututario("ddd_1"))
                grdMutuario.TextMatrix(1, 10) = Trim(rsMututario("telefone_1"))
                grdMutuario.TextMatrix(1, 11) = Trim(rsMututario("Doc_identidade"))
                grdMutuario.TextMatrix(1, 12) = Trim(rsMututario("Tp_doc_identidade"))
                grdMutuario.TextMatrix(1, 13) = Trim(rsMututario("Exp_doc_identidade"))
                grdMutuario.TextMatrix(1, 14) = Format(rsMututario("dt_emissao_identidade"), "dd/mm/yyyy")
                grdMutuario.TextMatrix(1, 15) = Format(rsMututario("val_capital_segurado"), "###,###,###,##0.00")
                grdMutuario.TextMatrix(1, 16) = Format(rsMututario("val_premio_bruto"), "###,###,###,##0.00")
                grdMutuario.TextMatrix(1, 17) = Trim(rsMututario("cod_mci"))
                 'Amanda Viana - Confitec - 16/10/2019 - circula 365 -- % mutu�rio
                If Val(txtProduto.Tag) = ProdIdOurovidaProdutorRural Or Val(txtProduto.Tag) = ProdIdBBSeguroVidaAgriculturaFamiliar Then
                    grdMutuario.TextMatrix(1, 18) = Format(rsMututario("perc_mutuario"), "###,###,###,##0.00")
                End If
           
            Else
        
                sLinha = ""
                sLinha = sLinha & rsMututario("nome") & vbTab
                sLinha = sLinha & rsMututario("CPF") & vbTab
                sLinha = sLinha & sSexo & vbTab
                sLinha = sLinha & Format(rsMututario("dt_nascimento"), "dd/mm/yyyy") & vbTab
                sLinha = sLinha & rsMututario("endereco") & vbTab
                sLinha = sLinha & rsMututario("bairro") & vbTab
                sLinha = sLinha & rsMututario("Municipio") & vbTab
                sLinha = sLinha & rsMututario("estado") & vbTab
                sLinha = sLinha & rsMututario("cep") & vbTab
                sLinha = sLinha & rsMututario("ddd_1") & vbTab
                sLinha = sLinha & rsMututario("telefone_1") & vbTab
                sLinha = sLinha & rsMututario("Doc_identidade") & vbTab
                sLinha = sLinha & rsMututario("Tp_doc_identidade") & vbTab
                sLinha = sLinha & rsMututario("Exp_doc_identidade") & vbTab
                sLinha = sLinha & Format(rsMututario("dt_emissao_identidade"), "dd/mm/yyyy") & vbTab
                sLinha = sLinha & Format(rsMututario("val_capital_segurado"), "###,###,###,##0.00") & vbTab
                sLinha = sLinha & Format(rsMututario("val_premio_bruto"), "###,###,###,##0.00") & vbTab
                sLinha = sLinha & rsMututario("cod_mci") & vbTab
                If Val(txtProduto.Tag) = ProdIdOurovidaProdutorRural Or Val(txtProduto.Tag) = ProdIdBBSeguroVidaAgriculturaFamiliar Then
                    sLinha = sLinha & Format(rsMututario("perc_mutuario"), "###,###,###,##0.00") & vbTab
                End If
           
                grdMutuario.AdicionarLinha sLinha
           
            End If
           
            rsMututario.MoveNext
        
        Loop
      
    End If
  
    rsMututario.Close
    Set rsMututario = Nothing

    Exit Function
  
TrataErro:
    Call TratarErro("CarregarDadosMutuario", Me.name)
    Call FinalizarAplicacao

End Function

Private Function CarregarDadosBeneficiarios() As Boolean

    On Error GoTo TrataErro

    Dim rsBenef As Recordset
  
    CarregarDadosBeneficiarios = True
  
    Set rsBenef = New Recordset
  
    'Beneficiarios do titular ''''''''''''''''''''''''''''''''''''''''''
    Set rsBenef = ObterDadosBeneficiarios(Val(txtProposta.Text))
  
    If Not rsBenef.EOF Then
        'Rafael Oshiro 26/10/04
        'Mostrar todos os beneficiarios
        While Not rsBenef.EOF
            txtBeneficiario.Text = txtBeneficiario.Text & Trim(rsBenef(0)) & vbNewLine
            rsBenef.MoveNext
        Wend
    End If
  
    'Beneficiarios do conjuge ''''''''''''''''''''''''''''''''''''''''''
  
    rsBenef.Close
    Set rsBenef = Nothing

    Exit Function
  
TrataErro:
    Call TratarErro("CarregarDadosBeneficiarios", Me.name)
    Call FinalizarAplicacao

End Function

Private Function CarregarDadosSinistro() As Boolean

    On Error GoTo TrataErro

    Dim rsSinistro As Recordset
    Dim sLinha As String
  
    CarregarDadosSinistro = True
  
    Set rsSinistro = New Recordset
    Set rsSinistro = ObterDadosSinistro(Val(txtProposta.Text))
  
    gridSinistro.Rows = 1
   
    If Not rsSinistro.EOF Then
    
        While Not rsSinistro.EOF
     
            sLinha = ""
            sLinha = sLinha & rsSinistro!sinistro_id & vbTab
            sLinha = sLinha & Format(rsSinistro!dt_aviso_sinistro, "dd/mm/yyyy") & vbTab
            sLinha = sLinha & rsSinistro!Nome & vbTab
            sLinha = sLinha & rsSinistro!Sitnome & vbTab & MascaraValor(rsSinistro!val_pgto)
            sLinha = sLinha & vbTab & rsSinistro!dt_ocorrencia_sinistro
           
            gridSinistro.AddItem sLinha
           
            rsSinistro.MoveNext
        
        Wend
   
    End If
  
    rsSinistro.Close
    Set rsSinistro = Nothing

    Exit Function
  
TrataErro:
    Call TratarErro("CarregarDadosSinistro", Me.name)
    Call FinalizarAplicacao
End Function

Private Function CarregarDadosExtratoSeguro() As Boolean

    On Error GoTo TrataErro

    Dim rsExtrato As Recordset
    Dim rsExtratoCancelamento As Recordset
    Dim rsSituacaoProposta As Recordset
  
    Dim sLinha As String
    Dim bReativacao As Boolean
    Dim xRow As Integer
    Dim strMotRec As String

    '****************************************************
    'Ana Paula Minesio, em 28/10/2005
    'objetivo: verificar as permiss�es de visualiza��o de mensagens de recusa pelos usu�rios do sistema
    '****************************************************
    Dim ocls00420 As Object
    Dim sMsgRecusa As String
  
    xRow = 0
  
    'Instancia Classe cls00420
    Set ocls00420 = CreateObject("SEGL0191.cls00420")
  
    CarregarDadosExtratoSeguro = True
  
    Set rsExtrato = New Recordset
    
    ' Extrato do Seguro ''''''''''''''''''''''''''''''''''''''''''''''''''''
    Set rsExtrato = ObterDadosExtratoSeguro(Val(txtProposta.Text))
   
    Set rsSituacaoProposta = ObterSituacaoProposta(Val(txtProposta.Text))
    
    GridProposta(0).Rows = 1
  
    While Not rsExtrato.EOF
  
        With GridProposta(0)
            sLinha = ""
            sLinha = sLinha & Format(.Row, "000") & vbTab
            sLinha = sLinha & rsExtrato("tipo") & vbTab
            sLinha = sLinha & rsExtrato("data") & vbTab
            sLinha = sLinha & IIf(IsNull(rsExtrato("dt_inclusao_evento")), "", Format(rsExtrato("dt_inclusao_evento"), "dd/mm/yyyy")) & vbTab 'Demanda 546132 - Talitha - 07/10/2008 - Linha Incluida
            sLinha = sLinha & rsExtrato("evento") & vbTab
            'alucas 11/10/2005
            sLinha = sLinha & rsExtrato("documento") & " | " & IIf(IsNull(rsExtrato("parametro")), "", rsExtrato("parametro")) & " | " & IIf(IsNull(rsExtrato("endosso_id")), "", rsExtrato("endosso_id")) & " | " & IIf(IsNull(rsExtrato("desc_sega")), "", rsExtrato("desc_sega")) & vbTab   'Demanda 546132 - Talitha - 07/10/2008 - Linha Alterada
            sLinha = sLinha & IIf(IsNull(rsExtrato("funcionario")), "", rsExtrato("funcionario"))

            .AddItem sLinha
          
        End With
      
        rsExtrato.MoveNext
 
    Wend

    ' Historico de avaliacoes '''''''''''''''''''''''''''''''''''''''''''''
    rsExtrato.Close
    Set rsExtratoCancelamento = New Recordset
  
    bReativacao = False
  
    Set rsExtrato = ObterDadosAvaliacao(Val(txtProposta.Text))
  
    Set rsSituacaoProposta = ObterSituacaoProposta(Val(txtProposta.Text))
  
    GridProposta(1).Rows = 1
    GridProposta(1).ColWidth(0) = 0
  
    While Not rsExtrato.EOF
  
        If rsExtrato(1) = "REATIVA�AO" Then
    
            'mostra o cancelamento antes da reativacao
        
            bReativacao = True
            Set rsExtratoCancelamento = ObterDadosAvaliacaoCancelamento(Val(txtProposta.Text))
        
            While Not rsExtratoCancelamento.EOF
                sLinha = Format(rsExtratoCancelamento(0), "yyyymmdd") & UCase(rsExtratoCancelamento(1)) & vbTab
                sLinha = sLinha & Format(rsExtratoCancelamento(0), "dd/mm/yyyy") & vbTab 'dt_avaliacao
                sLinha = sLinha & UCase(rsExtratoCancelamento(1)) & vbTab        'nome
                'JOSE MOREIRA - NOVA CONSULTORIA - 14620257 - Melhorias no processo de subscri��o
                sLinha = sLinha & " " & vbTab                  'critica
                sLinha = sLinha & " " & vbTab                  'grupo
                sLinha = sLinha & " " & vbTab                  'item
                sLinha = sLinha & " " & vbTab                  'pedido exame
                sLinha = sLinha & UCase(rsExtratoCancelamento(2))                 'usuario
                GridProposta(1).AddItem (sLinha)
            
                rsExtratoCancelamento.MoveNext
            Wend
        
            rsExtratoCancelamento.Close
        
        End If
  
        'mostra a reativacao e as outras avaliacoes
    
        If Trim(UCase(rsSituacaoProposta("situacao"))) = "R" Or Trim(UCase(rsSituacaoProposta("situacao"))) = "S" Then
          
            lblMoteRec.Visible = True
            txtMotRec.Visible = True
            GridProposta(1).Height = 1245
        
            If ocls00420.VerificarPermissao(gsSIGLASISTEMA, App.Title, App.FileDescription, glAmbiente_id, cUserName, rsExtrato("tp_avaliacao_id")) Then 'Usu�rio tem acesso a mensagem de recusa (mensagem secundaria - campo "nome")

                sMsgRecusa = UCase(Trim(rsExtrato("nome")))
            Else

                If UCase(Trim(rsExtrato("origem"))) = "O" Then
                    sMsgRecusa = UCase(Trim("RECUSA OPERACIONAL"))
                Else
                    sMsgRecusa = UCase(Trim("RECUSA T�CNICA"))
                End If
            End If
    
        Else
            sMsgRecusa = UCase(Trim(rsExtrato("nome")))
            '            lblMoteRec.Visible = False
            '            txtMotRec.Visible = False
            GridProposta(1).Height = 1845
        End If
    
        sLinha = Format(rsExtrato(0), "yyyymmdd") & UCase(Trim(rsExtrato(1))) & vbTab
        sLinha = sLinha & Format(rsExtrato(0), "dd/mm/yyyy") & vbTab 'dt_avaliacao
        'sLinha = sLinha & UCase(Trim(rsExtrato(1))) & vbTab  'nome
        sLinha = sLinha & UCase(sMsgRecusa) & vbTab        'nome
        
        'JOSE MOREIRA - NOVA CONSULTORIA - 14620257 - Melhorias no processo de subscri��o
        sLinha = sLinha & UCase(rsExtrato(8)) & vbTab        'critica
        
        sLinha = sLinha & rsExtrato(4) & vbTab               'grupo
        sLinha = sLinha & rsExtrato(5) & vbTab               'item
        sLinha = sLinha & UCase(rsExtrato(2)) & vbTab        'pedido exame
        sLinha = sLinha & UCase(rsExtrato(3))                'usuario
        GridProposta(1).AddItem (sLinha)
        xRow = GridProposta(1).Rows - 1
        rsExtrato.MoveNext
    
    Wend
  
    rsExtrato.Close
    strMotRec = CStr(GridProposta(1).TextMatrix(xRow, 2))
    txtMotRec = strMotRec
  
    If Not bReativacao Then
    
        Set rsExtrato = ObterDadosAvaliacaoCancelamento(Val(txtProposta.Text))
  
        While Not rsExtrato.EOF
            sLinha = Format(rsExtrato(0), "yyyymmdd") & UCase(rsExtrato(1)) & vbTab
            sLinha = sLinha & Format(rsExtrato(0), "dd/mm/yyyy") & vbTab 'dt_avaliacao
            sLinha = sLinha & UCase(rsExtrato(1)) & vbTab        'nome
            'JOSE MOREIRA - NOVA CONSULTORIA - 14620257 - Melhorias no processo de subscri��o
            sLinha = sLinha & " " & vbTab                  'cr�tica
            sLinha = sLinha & " " & vbTab                  'grupo
            sLinha = sLinha & " " & vbTab                  'item
            sLinha = sLinha & " " & vbTab                  'pedido exame
            sLinha = sLinha & UCase(rsExtrato(2))                 'usuario
            GridProposta(1).AddItem (sLinha)
            rsExtrato.MoveNext
        Wend
  
        rsExtrato.Close
        GridProposta(1).Col = 0
        'Ezequiel - Alterada a ordem para decrescente
        'GridProposta(1).Sort = flexSortStringAscending
    
        GridProposta(1).Sort = flexSortStringDescending
  
    End If
    
    Set rsExtratoCancelamento = Nothing
    Set rsExtrato = Nothing
    Set rsSituacaoProposta = Nothing
  
    Carrega_Restituicao
    GridProposta(1).Sort = 2
    
    'vitor.cabral - Nova Consultoria - 11/10/2013 - 17886578 - Registro das avaliacoes na recepcao das parcelas - Inicio
    'Carrega GRID de agendamentos inconsistentes recebidos pelo BB
    Call CarregaAgendamentosInconsistentes
    
    If GridProposta(3).Rows < 2 Then
        btnExportarExcel.Enabled = False
    Else
        btnExportarExcel.Enabled = True
    End If
    'vitor.cabral - Nova Consultoria - 11/10/2013 - 17886578 - Registro das avaliacoes na recepcao das parcelas - Fim
  
    'Elimina objeto criado
    Set ocls00420 = Nothing
    Exit Function
  
TrataErro:
    Call TratarErro("CarregarDadosSinistro", Me.name)
    Call FinalizarAplicacao
End Function

Sub Carrega_Restituicao()
    
On Error GoTo TrataErro

Dim sSql, Linha As String
Dim rs_endosso As Recordset
Dim rs_avaliacao As Recordset
Dim Situacao As Boolean
Dim sTipoArquivoRestituicao As String
    
    Set rs_endosso = New Recordset
    
    GridProposta(2).Rows = 1
    
    'Vanessa Barbosa - Confitec - 13/06/2012
    'Produtos BESC ter�o a sele��o dos dados diferenciada dos demais produtos.
    
    If Val(txtProduto.Tag) >= 200 And Val(txtProduto.Tag) <= 225 Then
        Call Buscar_Restiticao_BESC(rs_endosso)
    Else
'        sSql = "  select  ef.val_financeiro, e.endosso_id, tp.tp_endosso_id, tp.descricao, fl_BESC = 'N'"
'        sSql = sSql & "  from    endosso_tb e  WITH (NOLOCK) "
'        'sSql = sSql & "      left join endosso_financeiro_tb ef  WITH (NOLOCK) " - Cibele.Pereira - INC000004387741 - 03/09/2014
'        sSql = sSql & "      inner join endosso_financeiro_tb ef  WITH (NOLOCK) " '- Cibele.Pereira - INC000004387741 - 03/09/2014
'        sSql = sSql & "          on e.proposta_id = ef.proposta_id"
'        sSql = sSql & "          and e.endosso_id = ef.endosso_id"
'        sSql = sSql & "          and ef.val_financeiro < 0"
'        sSql = sSql & "      inner join tp_endosso_tb tp  WITH (NOLOCK) "
'        sSql = sSql & "          on e.tp_endosso_id = tp.tp_endosso_id"
'        sSql = sSql & "  Where e.proposta_id = " & Val(txtProposta.Text)
'        sSql = sSql & "  AND (EXISTS (select 1 from extrato_restituicao_tb ex  WITH (NOLOCK)  WHERE ex.proposta_id = e.proposta_id AND (ex.endosso_id = e.endosso_id OR ex.endosso_id IS NULL))"
'        sSql = sSql & "  OR EXISTS (SELECT 1 FROM endosso_financeiro_tb ef2  WITH (NOLOCK)  WHERE ef2.proposta_id = e.proposta_id AND ef2.endosso_id = e.endosso_id AND ef2.val_financeiro < 0) )"
        
        sSql = "SELECT "
        sSql = sSql & "EF.VAL_FINANCEIRO, E.ENDOSSO_ID, TP.TP_ENDOSSO_ID, "
        sSql = sSql & "TP.DESCRICAO, FL_BESC = 'N' "
        sSql = sSql & "FROM ENDOSSO_TB E WITH (NOLOCK) "
        sSql = sSql & "INNER JOIN ENDOSSO_FINANCEIRO_TB EF WITH (NOLOCK) ON E.PROPOSTA_ID = EF.PROPOSTA_ID AND E.ENDOSSO_ID = EF.ENDOSSO_ID AND EF.VAL_FINANCEIRO < 0 "
        sSql = sSql & "INNER JOIN TP_ENDOSSO_TB TP WITH (NOLOCK) ON E.TP_ENDOSSO_ID = TP.TP_ENDOSSO_ID "
        sSql = sSql & "WHERE "
        sSql = sSql & "E.PROPOSTA_ID = " & Val(txtProposta.Text) & " "
        sSql = sSql & "AND ( "
        sSql = sSql & "EXISTS ( "
        sSql = sSql & "SELECT 1 "
        sSql = sSql & "FROM EXTRATO_RESTITUICAO_TB EX WITH (NOLOCK) "
        sSql = sSql & "WHERE "
        sSql = sSql & "EX.PROPOSTA_ID = E.PROPOSTA_ID "
        sSql = sSql & "AND E.PROPOSTA_ID = " & Val(txtProposta.Text) & " "
        sSql = sSql & "AND EX.PROPOSTA_ID = " & Val(txtProposta.Text) & " "
        sSql = sSql & "AND (EX.ENDOSSO_ID = E.ENDOSSO_ID OR EX.ENDOSSO_ID IS NULL) "
        sSql = sSql & ") "
        sSql = sSql & "OR "
        sSql = sSql & "EXISTS ( "
        sSql = sSql & "SELECT 1 "
        sSql = sSql & "FROM ENDOSSO_FINANCEIRO_TB EF2 WITH (NOLOCK) "
        sSql = sSql & "WHERE "
        sSql = sSql & "EF2.PROPOSTA_ID = E.PROPOSTA_ID "
        sSql = sSql & "AND E.PROPOSTA_ID = " & Val(txtProposta.Text) & " "
        sSql = sSql & "AND EF2.PROPOSTA_ID = " & Val(txtProposta.Text) & " "
        sSql = sSql & "AND EF2.ENDOSSO_ID = E.ENDOSSO_ID "
        sSql = sSql & "AND EF2.VAL_FINANCEIRO < 0) "
        sSql = sSql & ") "
            ' INICIO -- GUILHERME CRUZ -- CONFITEC SISTEMAS 25/10/2016 -- FLOWBR18261986
        sSql = sSql & "union" & vbNewLine
        
       
        sSql = sSql & "select   (isnull(cr.valor_credito,0)) * (- 1) as val_financeiro , e.endosso_id, tp.tp_endosso_id, tp.descricao , FL_BESC = 'N' " & vbNewLine
        sSql = sSql & "from conciliacao_restituicao_tb cr" & vbNewLine
        sSql = sSql & "join endosso_tb e  WITH (NOLOCK)" & vbNewLine
        sSql = sSql & "on e.proposta_id = cr.proposta_id" & vbNewLine
        sSql = sSql & "and e.endosso_id = cr.endosso_id" & vbNewLine
        sSql = sSql & "join tp_endosso_tb tp  WITH (NOLOCK)" & vbNewLine
        sSql = sSql & "on e.tp_endosso_id = tp.tp_endosso_id" & vbNewLine
        sSql = sSql & "where E.PROPOSTA_ID = " & Val(txtProposta.Text) & " " & vbNewLine
        
        ' FIM    -- GUILHERME CRUZ -- CONFITEC SISTEMAS 25/10/2016 -- FLOWBR18261986
        
     
        Set rs_endosso = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, sSql, True)
    End If
    
    If Not rs_endosso.EOF Then
        If rs_endosso("fl_BESC") = "N" Then
            While Not rs_endosso.EOF
                sSql = "  select  *"
                sSql = sSql & "  From avaliacao_retorno_bb_tb arb  WITH (NOLOCK) "
                sSql = sSql & "  Where arb.proposta_id = " & Val(txtProposta.Text)
                sSql = sSql & "  and     arb.endosso_id = " & rs_endosso("ENDOSSO_ID")
                sSql = sSql & "  and     arb.seq_avaliacao_retorno_bb_id = ( select max(seq_avaliacao_retorno_bb_id) "
                sSql = sSql & "  from avaliacao_retorno_bb_tb "
                sSql = sSql & "  where proposta_id = " & Val(txtProposta.Text)
                sSql = sSql & "  and     endosso_id =" & rs_endosso("endosso_id") & ")"
                'Cibele.Pereira - INC000004387741 - 03/09/2014
                'sSql = sSql & "  AND NOT EXISTS (SELECT 1 FROM extrato_restituicao_tb ex  WITH (NOLOCK)  WHERE ex.proposta_id = arb.proposta_id AND (ex.endosso_id = arb.endosso_id OR ex.endosso_id IS NULL))"
                
                Set rs_avaliacao = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, sSql, True)
                
                'Condi��o abaixo adicionada por Stefanini Consultoria, em 23/09/2004, pois foi encontrada
                'proposta com valor financeiro negativo (na tabela endosso_financeiro_tb) e o mesmo
                'Endosso_id n�o constava na tabela de avaliacacao_retorno_bb_tb
                If Not rs_avaliacao.EOF Then
                    If UCase(rs_avaliacao("aceite_bb")) = "S" Then
                        sTipoArquivoRestituicao = Mid(rs_avaliacao("seg_remessa_arq_retorno_bb"), 1, 6)
                        
                        If sTipoArquivoRestituicao = "SEG452" Or sTipoArquivoRestituicao = "BBC452" Then
                            Linha = rs_endosso("endosso_id") & vbTab
                            Linha = Linha & rs_endosso("descricao") & vbTab
                            Linha = Linha & rs_avaliacao("seg_remessa_arq_retorno_bb") & vbTab
                            Linha = Linha & Format(rs_avaliacao("dt_processamento"), "dd/mm/yyyy") & vbTab
                            Linha = Linha & (rs_endosso("val_financeiro") * -1) & vbTab
                            Linha = Linha & "Restitui��o aceita pelo Banco"
                        Else
                            Situacao = Situacao_restituicao(Val(txtProposta.Text), rs_endosso("endosso_id"))
                            
                            Linha = rs_endosso("endosso_id") & vbTab
                            Linha = Linha & rs_endosso("descricao") & vbTab
                            Linha = Linha & rs_avaliacao("seg_remessa_arq_retorno_bb") & vbTab
                            Linha = Linha & Format(rs_avaliacao("dt_processamento"), "dd/mm/yyyy") & vbTab
                            Linha = Linha & (rs_endosso("val_financeiro") * -1) & vbTab
                            
                            If Situacao = True Then
                                Linha = Linha & "Aguardando retorno do banco para gerar restitui��o"
                            Else
                                Linha = Linha & "Restitui��o n�o foi processada"
                            End If
                        End If
                    Else
                        If Mid(rs_avaliacao("seg_remessa_arq_retorno_bb"), 1, 6) = "SEG452" Then
                            Linha = rs_endosso("endosso_id") & vbTab
                            Linha = Linha & rs_endosso("descricao") & vbTab
                            Linha = Linha & rs_avaliacao("seg_remessa_arq_retorno_bb") & vbTab
                            Linha = Linha & Format(rs_avaliacao("dt_processamento"), "dd/mm/yyyy") & vbTab
                            Linha = Linha & (rs_endosso("val_financeiro") * -1) & vbTab
                            Linha = Linha & "Restitui��o recusada pelo Banco"
                        Else
                            Linha = rs_endosso("endosso_id") & vbTab
                            Linha = Linha & rs_endosso("descricao") & vbTab
                            Linha = Linha & rs_avaliacao("seg_remessa_arq_retorno_bb") & vbTab
                            Linha = Linha & Format(rs_avaliacao("dt_processamento"), "dd/mm/yyyy") & vbTab
                            Linha = Linha & (rs_endosso("val_financeiro") * -1) & vbTab
                            Linha = Linha & "Endosso recusado pelo Banco"
                        End If
                    End If
                    
                    GridProposta(2).AddItem Linha
                    GridProposta(2).ColAlignment(2) = flexAlignCenterCenter
                    GridProposta(2).ColAlignment(3) = flexAlignCenterCenter
                Else
                    Linha = rs_endosso("endosso_id") & vbTab
                    Linha = Linha & rs_endosso("descricao") & vbTab
                    Linha = Linha & "" & vbTab
                    Linha = Linha & "" & vbTab
                    Linha = Linha & (rs_endosso("val_financeiro") * -1) & vbTab
                    Linha = Linha & ""
                    
                    GridProposta(2).AddItem Linha
                    GridProposta(2).ColAlignment(2) = flexAlignCenterCenter
                    GridProposta(2).ColAlignment(3) = flexAlignCenterCenter
                End If
                
                Set rs_avaliacao = Nothing
                rs_endosso.MoveNext
            Wend
        Else 'Produtos BESC
            While Not rs_endosso.EOF
                Linha = rs_endosso("endosso_id") & vbTab
                Linha = Linha & rs_endosso("descricao") & vbTab
                Linha = Linha & rs_endosso("seg_remessa_arq_retorno_bb") & vbTab
                Linha = Linha & Format(rs_endosso("dt_processamento"), "dd/mm/yyyy") & vbTab
                Linha = Linha & FormataValorParaPadraoBrasil((rs_endosso("val_financeiro"))) & vbTab
                Linha = Linha & rs_endosso("descricao_situacao")
                
                GridProposta(2).AddItem Linha
                GridProposta(2).ColAlignment(2) = flexAlignCenterCenter
                GridProposta(2).ColAlignment(3) = flexAlignCenterCenter
                
                rs_endosso.MoveNext
            Wend
        End If
    End If
    
    Set rs_endosso = Nothing
    
    Exit Sub
TrataErro:
    Call TratarErro("Carrega_Restituicao", Me.name)
    Call FinalizarAplicacao
End Sub

Function Situacao_restituicao(proposta_id As Long, endosso_id As Integer) As Boolean

    Dim rs_situacao As Recordset
    Dim sSql As String

    Situacao_restituicao = False

    sSql = "  select  isnull(flag_aprovacao,'N') as flag_aprovacao"
    sSql = sSql & "  from    ps_mov_endosso_financeiro_tb    ef"
    sSql = sSql & "      inner join ps_movimentacao_tb   mov"
    sSql = sSql & "          on ef.movimentacao_id = mov.movimentacao_id"
    sSql = sSql & "  Where proposta_id = " & proposta_id
    sSql = sSql & "  and     endosso_id =" & endosso_id
    'GENJUNIOR - FLOW 17860335
    'UNION COM AS TABELAS DE EXPURGO
    '10/10/2013
    sSql = sSql & " UNION " & vbNewLine
    sSql = sSql & "  select  isnull(flag_aprovacao,'N') as flag_aprovacao"
    sSql = sSql & "  from seguros_hist_db.dbo.ps_mov_endosso_financeiro_hist_tb ef"
    sSql = sSql & " inner join seguros_hist_db.dbo.ps_movimentacao_hist_tb   mov"
    sSql = sSql & "          on ef.movimentacao_id = mov.movimentacao_id"
    sSql = sSql & "  Where proposta_id = " & proposta_id
    sSql = sSql & "  and     endosso_id =" & endosso_id
    'FIM GENJUNIOR
    Set rs_situacao = New Recordset

    Set rs_situacao = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, sSql, True)

    If UCase(rs_situacao("flag_aprovacao")) = "S" Then
        Situacao_restituicao = True
    Else
        Situacao_restituicao = False
    End If

End Function

Private Function CarregarPropostaAnterior() As Boolean

    On Error GoTo TrataErro

    Dim rsPropostaAnterior As Recordset
  
    CarregarPropostaAnterior = True
  
    Set rsPropostaAnterior = New Recordset
    Set rsPropostaAnterior = ObterDadosPropostaAnterior(Val(txtProposta.Text), Val(txtProduto.Tag))
  
    If Val(txtProduto.Tag) = ProdIdOuroVidaAntigo Then
        lblPropMigracao.Caption = "Proposta Migrada para"
    Else
        lblPropMigracao.Caption = "Proposta Anterior"
    End If
  
    If Not rsPropostaAnterior.EOF Then
        lblPropMigracao.ForeColor = vbRed
        lblPropMigracao.Visible = True
        txtPropMigracao.Visible = True
        txtPropMigracao.Text = rsPropostaAnterior(0)
    End If
  
    rsPropostaAnterior.Close
    Set rsPropostaAnterior = Nothing

    Exit Function
  
TrataErro:
    Call TratarErro("CarregarPropostaAnterior", Me.name)
    Call FinalizarAplicacao
End Function

Private Function ObterDadosCoberturas(lPropostaId As Long, _
                                      lApoliceId As Long, _
                                      LramoId As Long, _
                                      lSeguradoraId As Long, _
                                      lSucursalId As Long, _
                                      iSubgrupoId As Integer, _
                                      iTpComponente As Integer) As Recordset

    On Error GoTo TrataErro

    Dim sSql As String
    
    sSql = ""
    
    If iSubgrupoId = -1 Then

    '22/06/2016 - Victor Fonseca - Demanda 19295296 - Corre��o Indeniza��o Extra
    'Altera��o feita para ajudar na obten��o do valor da IS quando a data de contrata��o for maior que 20/08/2015
            If txtProduto.Tag = ProdIdBBSeguroVida And Renova_vida = False Then
                sSql = sSql & "DECLARE @valor as BIT" & vbNewLine
                sSql = sSql & "DECLARE @dt_contratacao as SMALLDATETIME" & vbNewLine
                sSql = sSql & vbNewLine
                sSql = sSql & "SELECT  @dt_contratacao = dt_contratacao" & vbNewLine
                sSql = sSql & "FROM seguros_db.dbo.proposta_tb" & vbNewLine
                sSql = sSql & "WHERE proposta_id = " & lPropostaId & vbNewLine
                sSql = sSql & vbNewLine
                sSql = sSql & "IF @dt_contratacao >= '2015-08-20'" & vbNewLine
                sSql = sSql & "SELECT @valor = 1" & vbNewLine
                sSql = sSql & "ELSE" & vbNewLine
                sSql = sSql & "SELECT @valor = 0" & vbNewLine
                sSql = sSql & vbNewLine
            End If

        'DF - jconceicao - adquar o pronaf para funcionar como OVM
        '     2006 abr 18
        If txtProduto.Tag = ProdIdOuroVidaMulher Or txtProduto.Tag = ProdIdBBSeguroVidaAgriculturaFamiliar Then
    
            sSql = sSql & "    SELECT tp_cobertura_tb.tp_cobertura_id,                                                              " & vbNewLine
            sSql = sSql & "           tp_cobertura =  CASE tp_cob_comp_tb.class_tp_cobertura                                        " & vbNewLine
            sSql = sSql & "                                WHEN 'a' THEN 'ADICIONAL'                                                " & vbNewLine
            sSql = sSql & "                                WHEN 'b' THEN 'B�SICA'                                                   " & vbNewLine
            sSql = sSql & "                                ELSE 'N�O DEFINIDO'                                                      " & vbNewLine
            sSql = sSql & "                           END ,                                                                         " & vbNewLine
            sSql = sSql & "           plano_tb.nome plano,                                                                          " & vbNewLine
            sSql = sSql & "           plano_tb.plano_id,                                                                            " & vbNewLine
            sSql = sSql & "           tp_cob_comp_tb.class_tp_cobertura,                                                            " & vbNewLine
            sSql = sSql & "           tp_cobertura_tb.nome,                                                                         " & vbNewLine
            sSql = sSql & "           ISNULL(escolha_plano_tp_cob_tb.val_is,0),                                                               " & vbNewLine
            sSql = sSql & "           max_is = ISNULL(ROUND(escolha_plano_tp_cob_tb.val_is * (tp_cob_comp_tb.lim_max_cobertura / 100),2,1),0)," & vbNewLine
            sSql = sSql & "           min_is = ISNULL(ROUND(escolha_plano_tp_cob_tb.val_is * (tp_cob_comp_tb.lim_min_cobertura / 100),2,1),0)," & vbNewLine
            sSql = sSql & "           min_cob = tp_cob_comp_tb.lim_min_cobertura / 100,                                             " & vbNewLine
            sSql = sSql & "           max_cob = tp_cob_comp_tb.lim_max_cobertura / 100,                                             " & vbNewLine
            sSql = sSql & "           taxa_net = 0,                                                                                 " & vbNewLine
            sSql = sSql & "           fat_taxa = 0,                                                                                 " & vbNewLine
            sSql = sSql & "           val_premio = 0,                                                                               " & vbNewLine
            sSql = sSql & "           fat_desconto_tecnico = 0,                                                                     " & vbNewLine
            '                         DF - jconceicao - adquar o pronaf para funcionar como OVM
            '                              2006 abr 18
            sSql = sSql & "           ISNULL(val_fixo_cobertura, 0) AS val_fixo_cobertura,                                           " & vbNewLine
            sSql = sSql & "           ISNULL(tp_val_cobertura, 'p') AS tp_val_cobertura                                             " & vbNewLine
            
'+------------------------------------------------------------
'|INICIO Demanda 18334512 Circular 491 - Petrauskas 11/2014
            sSql = sSql & "         , cobert_obj_risco_tb.val_premio_tarifa, " & vbCrLf _
                        & "           cobert_obj_risco_tb.val_premio_tarifa_sem_custo_assist_facul, " & vbCrLf _
                        & "           val_is_tarifa = isnull(cobert_obj_risco_tb.val_is,0) " & vbCrLf
'|FIM    Demanda 18334512 Circular 491 - Petrauskas 11/2014
'+------------------------------------------------------------
            
            'INICIO  alterado 02/09/2013 projeto 17860335 - Melhorias no SEGBR MC_Amaral -confitec
            'GENJUNIOR - CONFITEC SP - FLOW 17860335
            'RETORNAR A TABELA PROPOSTA_TB - SOMENTE COM ELA PARA COMPARAR O INICIO DE VIGENCIA DO PLANO COM A CONTRATA��O DA PROPOSTA
            '17/10/2013
            sSql = sSql & "      FROM proposta_tb  WITH (NOLOCK)                                                                           " & vbNewLine
            sSql = sSql & "      JOIN escolha_plano_tb  WITH (NOLOCK)                                                                      " & vbNewLine
            sSql = sSql & "        ON escolha_plano_tb.proposta_id = proposta_tb.proposta_id                                        " & vbNewLine
            'sSql = sSql & "      FROM  escolha_plano_tb  WITH (NOLOCK)                                                                   " & vbNewLine
            sSql = sSql & "       JOIN escolha_plano_tp_cob_tb  WITH (NOLOCK)                                                            " & vbNewLine
            sSql = sSql & "        ON escolha_plano_tp_cob_tb.proposta_id = escolha_plano_tb.proposta_id                              " & vbNewLine
           'FIM  alterado 02/09/2013 projeto 17860335 - Melhorias no SEGBR MC_Amaral -confitec
                    'AND escolha_plano_tp_cob_tb.plano_id=plano_tb.plano_id
'            sSql = sSql & "      JOIN escolha_plano_tp_cob_tb  WITH (NOLOCK)                                                               " & vbNewLine
'            sSql = sSql & "        ON escolha_plano_tp_cob_tb.proposta_id = proposta_tb.proposta_id                                 " & vbNewLine
            sSql = sSql & "       AND escolha_plano_tp_cob_tb.dt_fim_vigencia_cob IS NULL                                           " & vbNewLine
            sSql = sSql & "      JOIN tp_cob_comp_tb  WITH (NOLOCK)                                                                        " & vbNewLine
            sSql = sSql & "        ON tp_cob_comp_tb.tp_cob_comp_id = escolha_plano_tp_cob_tb.tp_cob_comp_id                        " & vbNewLine
            sSql = sSql & "      JOIN tp_componente_tb  WITH (NOLOCK)                                                                      " & vbNewLine
            sSql = sSql & "        ON tp_componente_tb.tp_componente_id = tp_cob_comp_tb.tp_componente_id                           " & vbNewLine
            sSql = sSql & "      JOIN tp_cobertura_tb  WITH (NOLOCK)                                                                       " & vbNewLine
            sSql = sSql & "        ON tp_cobertura_tb.tp_cobertura_id = tp_cob_comp_tb.tp_cobertura_id                              " & vbNewLine
            sSql = sSql & "      JOIN plano_tb  WITH (NOLOCK)                                                                              " & vbNewLine
            sSql = sSql & "        ON plano_tb.plano_id = escolha_plano_tb.plano_id                                                 " & vbNewLine
                        ' In�cio Altera��o - Tales de S� - INC000004240252
            ' V�nculo entre o plano e o produto da proposta para evitar duplicidades
            sSql = sSql & "        and plano_tb.produto_id = proposta_tb.produto_id                                                 " & vbNewLine
            ' Fim Altera��o - INC000004240252
'+------------------------------------------------------------
'|INICIO Demanda 18334512 Circular 491 - Petrauskas 11/2014
            sSql = sSql & " LEFT JOIN cobert_obj_risco_tb WITH (NOLOCK) " & vbCrLf _
                        & "        ON escolha_plano_tb.proposta_id = cobert_obj_risco_tb.proposta_id " & vbCrLf _
                        & "       AND cobert_obj_risco_tb.tp_componente_id = " & iTpComponente & vbCrLf _
                        & "       AND tp_cob_comp_tb.tp_cobertura_id = cobert_obj_risco_tb.tp_cobertura_id " & vbCrLf _
                        & "       AND isnull(cobert_obj_risco_tb.endosso_id,0) = (SELECT MAX(isnull(endosso_id,0)) " & vbCrLf _
                        & "                                              FROM cobert_obj_risco_tb WITH (NOLOCK) " & vbCrLf _
                        & "                                              WHERE proposta_id = escolha_plano_tb.proposta_id) " & vbCrLf
'|FIM    Demanda 18334512 Circular 491 - Petrauskas 11/2014
'+------------------------------------------------------------
            sSql = sSql & "      WHERE escolha_plano_tb.proposta_id = " & lPropostaId & vbNewLine
       
            'bcarneiro - 01/08/2005 - Erro na obten��o da vig�ncia do plano
            'sSQL = sSQL & "        AND (plano_tb.dt_fim_vigencia is NULL                                                            " & vbNewLine
            'sSQL = sSQL & "             OR (plano_tb.dt_inicio_vigencia < = proposta_tb.dt_contratacao                              " & vbNewLine
            'sSQL = sSQL & "                 AND plano_tb.dt_fim_vigencia > = proposta_tb.dt_contratacao))                           " & vbNewLine
            sSql = sSql & "        AND plano_tb.dt_inicio_vigencia <= proposta_tb.dt_contratacao                                    " & vbNewLine
            sSql = sSql & "        AND (plano_tb.dt_fim_vigencia is NULL                                                            " & vbNewLine
            sSql = sSql & "             OR plano_tb.dt_fim_vigencia >= proposta_tb.dt_contratacao)                                  " & vbNewLine
            ''''''''''''''''''''''''''''''''''''''
       
            sSql = sSql & "       AND tp_componente_tb.tp_componente_id = " & iTpComponente & vbNewLine
            sSql = sSql & "       AND escolha_plano_tb.dt_fim_vigencia IS NULL    " & vbNewLine
            sSql = sSql & "UNION                                                                                                    " & vbNewLine
            sSql = sSql & "    SELECT escolha_tp_cob_generico_tb.tp_cobertura_id,                                                   " & vbNewLine
            sSql = sSql & "           tp_cobertura = 'B�SICA',                                                                      " & vbNewLine
            sSql = sSql & "           plano = '',                                                                                   " & vbNewLine
            sSql = sSql & "           plano_id = 0,                                                                                 " & vbNewLine
            sSql = sSql & "           class_tp_cobertura = 'b',                                                                     " & vbNewLine
            sSql = sSql & "           tp_cobertura_tb.nome,                                                                         " & vbNewLine
            sSql = sSql & "           imp_segurada = ISNULL(escolha_tp_cob_generico_tb.val_is,0),                                             " & vbNewLine
            sSql = sSql & "           max_is = ISNULL(escolha_tp_cob_generico_tb.val_is,0),                                                   " & vbNewLine
            sSql = sSql & "           min_is = ISNULL(escolha_tp_cob_generico_tb.val_is,0),                                                   " & vbNewLine
            sSql = sSql & "           max_cob = 1,                                                                                  " & vbNewLine
            sSql = sSql & "           min_cob = 1,                                                                                  " & vbNewLine
            sSql = sSql & "           taxa_net = case perc_pro_labore                                                               " & vbNewLine
            sSql = sSql & "                        when null then 0                                                                 " & vbNewLine
            sSql = sSql & "                        when 0 then 0                                                                    " & vbNewLine
            sSql = sSql & "                        else fat_taxa / ( isnull(perc_pro_labore,1) * 100 )                              " & vbNewLine
            sSql = sSql & "                      end,                                                                               " & vbNewLine
            sSql = sSql & "           fat_taxa = ISNULL(fat_taxa,0),                                                                " & vbNewLine
            sSql = sSql & "           val_premio,                                                                                   " & vbNewLine
            sSql = sSql & "           fat_desconto_tecnico  ,                                                                       " & vbNewLine
            '                         DF - jconceicao - adquar o pronaf para funcionar como OVM
            '                              2006 abr 18
            sSql = sSql & "           0 AS val_fixo_cobertura, " & vbNewLine
            sSql = sSql & "           ' ' AS tp_val_cobertura  " & vbNewLine
'+------------------------------------------------------------
'|INICIO Demanda 18334512 Circular 491 - Petrauskas 11/2014
            sSql = sSql & "         , cobert_obj_risco_tb.val_premio_tarifa, " & vbCrLf _
                        & "           cobert_obj_risco_tb.val_premio_tarifa_sem_custo_assist_facul, " & vbCrLf _
                        & "           val_is_tarifa = isnull(cobert_obj_risco_tb.val_is,0) " & vbCrLf
'|FIM    Demanda 18334512 Circular 491 - Petrauskas 11/2014
'+------------------------------------------------------------
       
            sSql = sSql & "      FROM escolha_tp_cob_generico_tb  WITH (NOLOCK)                                                     " & vbNewLine
            'INICIO  alterado 02/09/2013 projeto 17860335 - Melhorias no SEGBR MC_Amaral -confitec
'            sSql = sSql & "      JOIN proposta_tb  WITH (NOLOCK)                                                                   " & vbNewLine
'            sSql = sSql & "        ON escolha_tp_cob_generico_tb.proposta_id = proposta_tb.proposta_id                             " & vbNewLine
           'INICIO  alterado 02/09/2013 projeto 17860335 - Melhorias no SEGBR MC_Amaral -confitec
            sSql = sSql & "      JOIN tp_cobertura_tb  WITH (NOLOCK)                                                                " & vbNewLine
            sSql = sSql & "        ON escolha_tp_cob_generico_tb.tp_cobertura_id = tp_cobertura_tb.tp_cobertura_id                  " & vbNewLine
            sSql = sSql & " LEFT JOIN administracao_apolice_tb  WITH (NOLOCK)                                                       " & vbNewLine
            sSql = sSql & "        ON administracao_apolice_tb.proposta_id = escolha_tp_cob_generico_tb.proposta_id                 " & vbNewLine
            sSql = sSql & "       AND administracao_apolice_tb.dt_fim_administracao IS NULL                                         " & vbNewLine
            
'+------------------------------------------------------------
'|INICIO Demanda 18334512 Circular 491 - Petrauskas 11/2014
            sSql = sSql & " LEFT JOIN cobert_obj_risco_tb WITH (NOLOCK) " & vbCrLf _
                        & "        ON escolha_tp_cob_generico_tb.proposta_id = cobert_obj_risco_tb.proposta_id " & vbCrLf _
                        & "       AND escolha_tp_cob_generico_tb.tp_cobertura_id = cobert_obj_risco_tb.tp_cobertura_id " & vbCrLf _
                        & "       AND cobert_obj_risco_tb.tp_componente_id = " & iTpComponente & vbCrLf _
                        & "       AND isnull(cobert_obj_risco_tb.endosso_id,0) = (SELECT MAX(isnull(endosso_id,0)) " & vbCrLf _
                        & "                                             FROM cobert_obj_risco_tb WITH (NOLOCK) " & vbCrLf _
                        & "                                             WHERE proposta_id = escolha_tp_cob_generico_tb.proposta_id) " & vbCrLf
'|FIM    Demanda 18334512 Circular 491 - Petrauskas 11/2014
'+------------------------------------------------------------
            
            sSql = sSql & "     WHERE escolha_tp_cob_generico_tb.dt_fim_vigencia_esc IS NULL                                        " & vbNewLine
            sSql = sSql & "       AND escolha_tp_cob_generico_tb.proposta_id = " & lPropostaId & vbNewLine
            sSql = sSql & "  ORDER BY class_tp_cobertura DESC                                                                       " & vbNewLine
      
        ElseIf Val(txtProduto.Tag) = 1218 Then
        
                    'RMesquita
            sSql = sSql & "    SELECT tp_cobertura_tb.tp_cobertura_id,                                                              " & vbNewLine
            sSql = sSql & "           tp_cobertura =  CASE tp_cob_comp_tb.class_tp_cobertura                                        " & vbNewLine
            sSql = sSql & "                                WHEN 'a' THEN 'ADICIONAL'                                                " & vbNewLine
            sSql = sSql & "                                WHEN 'b' THEN 'B�SICA'                                                   " & vbNewLine
            sSql = sSql & "                                ELSE 'N�O DEFINIDO'                                                      " & vbNewLine
            sSql = sSql & "                           END ,                                                                         " & vbNewLine
            sSql = sSql & "           plano_tb.nome plano,                                                                          " & vbNewLine
            sSql = sSql & "           plano_tb.plano_id,                                                                            " & vbNewLine
            sSql = sSql & "           tp_cob_comp_tb.class_tp_cobertura,                                                            " & vbNewLine
            sSql = sSql & "           tp_cobertura_tb.nome,            "
             
            sSql = sSql & "           escolha_tp_cob_vida_tb.val_is,                                                                " & vbNewLine
            sSql = sSql & "           max_is = ROUND(escolha_tp_cob_vida_tb.val_is * (tp_cob_comp_tb.lim_max_cobertura / 100),2,1), " & vbNewLine
            sSql = sSql & "           min_is = ROUND(escolha_tp_cob_vida_tb.val_is * (tp_cob_comp_tb.lim_min_cobertura / 100),2,1), " & vbNewLine
                          
            sSql = sSql & "           min_cob = tp_cob_comp_tb.lim_min_cobertura / 100,                                             " & vbNewLine
            sSql = sSql & "           max_cob = tp_cob_comp_tb.lim_max_cobertura / 100,                                             " & vbNewLine
            sSql = sSql & "           taxa_net = 0,                                                                                 " & vbNewLine
            sSql = sSql & "           fat_taxa = 0,                                                                                 " & vbNewLine
            sSql = sSql & "           val_premio = 0,                                                                               " & vbNewLine
            sSql = sSql & "           fat_desconto_tecnico = 0,                                                                     " & vbNewLine
             
            sSql = sSql & "           0 AS val_fixo_cobertura,                                                                      " & vbNewLine
            sSql = sSql & "           ' ' AS tp_val_cobertura                                                                       " & vbNewLine
'+------------------------------------------------------------
'|INICIO Demanda 18334512 Circular 491 - Petrauskas 11/2014
            sSql = sSql & "         , cobert_obj_risco_tb.val_premio_tarifa, " & vbCrLf _
                        & "           cobert_obj_risco_tb.val_premio_tarifa_sem_custo_assist_facul, " & vbCrLf _
                        & "           val_is_tarifa = isnull(cobert_obj_risco_tb.val_is,0) " & vbCrLf
'|FIM    Demanda 18334512 Circular 491 - Petrauskas 11/2014
'+------------------------------------------------------------
             
            sSql = sSql & "      FROM proposta_tb  WITH (NOLOCK)                                                                    " & vbNewLine
            sSql = sSql & "      JOIN escolha_tp_cob_vida_tb  WITH (NOLOCK)                                                         " & vbNewLine
            sSql = sSql & "        ON escolha_tp_cob_vida_tb.proposta_id = proposta_tb.proposta_id                                  " & vbNewLine
            sSql = sSql & "       and escolha_tp_cob_vida_tb.dt_inicio_vigencia_esc <= getdate()                                    " & vbNewLine
            sSql = sSql & "       and IsNull(escolha_tp_cob_vida_tb.dt_fim_vigencia_esc, (getdate() + 1)) >= getdate()              " & vbNewLine
            sSql = sSql & "      JOIN plano_tb  WITH (NOLOCK)                                                                       " & vbNewLine
            sSql = sSql & "        ON plano_tb.produto_id = proposta_tb.produto_id                                                  " & vbNewLine
            sSql = sSql & "      JOIN tp_plano_tb  WITH (NOLOCK)                                                                    " & vbNewLine
            sSql = sSql & "        ON plano_tb.tp_plano_id = tp_plano_tb.tp_plano_id                                                " & vbNewLine
            sSql = sSql & "       AND proposta_tb.produto_id = tp_plano_tb.produto_id                                               " & vbNewLine
            sSql = sSql & "      JOIN tp_cob_comp_plano_tb  WITH (NOLOCK)                                                           " & vbNewLine
            sSql = sSql & "        ON plano_tb.tp_plano_id = tp_cob_comp_plano_tb.tp_plano_id                                       " & vbNewLine
            sSql = sSql & "        AND escolha_tp_cob_vida_tb.tp_cob_comp_id = tp_cob_comp_plano_tb.tp_cob_comp_id                  " & vbNewLine
            sSql = sSql & "      JOIN tp_cob_comp_item_tb  WITH (NOLOCK)                                                            " & vbNewLine
            sSql = sSql & "        ON tp_cob_comp_item_tb.tp_cob_comp_id = tp_cob_comp_plano_tb.tp_cob_comp_id                      " & vbNewLine
            sSql = sSql & "       AND tp_cob_comp_item_tb.produto_id = proposta_tb.produto_id                                       " & vbNewLine
            
            sSql = sSql & "       AND tp_cob_comp_item_tb.ramo_id = proposta_tb.ramo_id                                             " & vbNewLine
            sSql = sSql & "      JOIN tp_cob_comp_tb  WITH (NOLOCK)                                                                 " & vbNewLine
            sSql = sSql & "        ON tp_cob_comp_item_tb.tp_cob_comp_id = tp_cob_comp_tb.tp_cob_comp_id                            " & vbNewLine
             
            sSql = sSql & "      JOIN tp_componente_tb  WITH (NOLOCK)                                                               " & vbNewLine
            sSql = sSql & "        ON tp_cob_comp_tb.tp_componente_id = tp_componente_tb.tp_componente_id                           " & vbNewLine
            sSql = sSql & "      JOIN tp_cobertura_tb  WITH (NOLOCK)                                                                " & vbNewLine
            sSql = sSql & "        ON tp_cob_comp_tb.tp_cobertura_id = tp_cobertura_tb.tp_cobertura_id  " & vbNewLine
             
'+------------------------------------------------------------
'|INICIO Demanda 18334512 Circular 491 - Petrauskas 11/2014
            sSql = sSql & " LEFT JOIN cobert_obj_risco_tb WITH (NOLOCK) " & vbCrLf _
                        & "        ON proposta_tb.proposta_id = cobert_obj_risco_tb.proposta_id " & vbCrLf _
                        & "       AND cobert_obj_risco_tb.tp_componente_id = " & iTpComponente & vbCrLf _
                        & "       AND tp_cobertura_tb.tp_cobertura_id = cobert_obj_risco_tb.tp_cobertura_id " & vbCrLf _
                        & "       AND isnull(cobert_obj_risco_tb.endosso_id,0) = (SELECT MAX(isnull(endosso_id,0)) " & vbCrLf _
                        & "                                             FROM cobert_obj_risco_tb WITH (NOLOCK) " & vbCrLf _
                        & "                                             WHERE proposta_id = proposta_tb.proposta_id) " & vbCrLf
'|FIM    Demanda 18334512 Circular 491 - Petrauskas 11/2014
'+------------------------------------------------------------
             
             
            sSql = sSql & "     WHERE proposta_tb.proposta_id = " & lPropostaId & vbNewLine
            sSql = sSql & "       AND tp_componente_tb.tp_componente_id = " & iTpComponente & vbNewLine
            'sSql = sSql & "       AND tp_cob_comp_item_tb.dt_fim_vigencia_comp IS NULL                                              " & vbNewLine
            sSql = sSql & "AND  plano_tb.dt_fim_vigencia is null" & vbCrLf
 
         ElseIf txtProduto.Tag = ProdIdBBSeguroVida Then
        
            sSql = sSql & "    SELECT tp_cobertura_tb.tp_cobertura_id,                                                              " & vbNewLine
            sSql = sSql & "           tp_cobertura =  CASE tp_cob_comp_tb.class_tp_cobertura                                        " & vbNewLine
            sSql = sSql & "                                WHEN 'a' THEN 'ADICIONAL'                                                " & vbNewLine
            sSql = sSql & "                                WHEN 'b' THEN 'B�SICA'                                                   " & vbNewLine
            sSql = sSql & "                                ELSE 'N�O DEFINIDO'                                                      " & vbNewLine
            sSql = sSql & "                           END ,                                                                         " & vbNewLine
            sSql = sSql & "           plano_tb.nome plano,                                                                          " & vbNewLine
            sSql = sSql & "           plano_tb.plano_id,                                                                            " & vbNewLine
            sSql = sSql & "           tp_cob_comp_tb.class_tp_cobertura,                                                            " & vbNewLine
            sSql = sSql & "           tp_cobertura_tb.nome,                                                                         " & vbNewLine
            
            If Renova_vida = False Then
                '22/06/2016 - Victor Fonseca - Demanda 19295296 - Corre��o Indeniza��o Extra
                'Altera��o feita para alterara a obten��o do valor da IS quando a data de contrata��o for maior que 20/08/2015
                    sSql = sSql & "           imp_segurada =  CASE tp_cob_comp_tb.class_tp_cobertura                                        " & vbNewLine
                    'sSql = sSql & "                              WHEN 'a' THEN ROUND((tp_cob_comp_tb.lim_max_cobertura * 10),2,1)           " & vbNewLine
                    sSql = sSql & "                               WHEN 'a' THEN " & vbNewLine
                    sSql = sSql & "                                 CASE @valor" & vbNewLine '
                    sSql = sSql & "                                     WHEN 0 THEN ROUND((tp_cob_comp_tb.lim_max_cobertura * 10),2,1)" & vbNewLine
                    sSql = sSql & "                                     WHEN 1 THEN tp_cob_comp_tb.val_fixo_cobertura" & vbNewLine
                    sSql = sSql & "                                 END" & vbNewLine
                    sSql = sSql & "                              WHEN 'b' THEN escolha_plano_tb.imp_segurada                                " & vbNewLine
                    sSql = sSql & "                              ELSE 'N�O DEFINIDO'                                                        " & vbNewLine
                    sSql = sSql & "                           END ,                                                                         " & vbNewLine
                    sSql = sSql & "           max_is = CASE tp_cob_comp_tb.class_tp_cobertura                                               " & vbNewLine
                    'sSql = sSql & "                        WHEN 'a' THEN ROUND((tp_cob_comp_tb.lim_max_cobertura * 10),2,1)                 " & vbNewLine
                    sSql = sSql & "                               WHEN 'a' THEN " & vbNewLine
                    sSql = sSql & "                                 CASE @valor" & vbNewLine '
                    sSql = sSql & "                                     WHEN 0 THEN ROUND((tp_cob_comp_tb.lim_max_cobertura * 10),2,1)" & vbNewLine
                    sSql = sSql & "                                     WHEN 1 THEN tp_cob_comp_tb.val_fixo_cobertura" & vbNewLine
                    sSql = sSql & "                                 END" & vbNewLine
                    sSql = sSql & "                        WHEN 'b' THEN ROUND(escolha_plano_tb.imp_segurada * (tp_cob_comp_tb.lim_max_cobertura / 100),2,1)" & vbNewLine
                    sSql = sSql & "                        ELSE 'N�O DEFINIDO'                                                              " & vbNewLine
                    sSql = sSql & "                    END ,                                                                                " & vbNewLine
                    sSql = sSql & "           min_is = CASE tp_cob_comp_tb.class_tp_cobertura                                               " & vbNewLine
                    'sSql = sSql & "                        WHEN 'a' THEN round((tp_cob_comp_tb.lim_min_cobertura * 10),2,1)                 " & vbNewLine
                    sSql = sSql & "                               WHEN 'a' THEN " & vbNewLine
                    sSql = sSql & "                                 CASE @valor" & vbNewLine '
                    sSql = sSql & "                                     WHEN 0 THEN ROUND((tp_cob_comp_tb.lim_min_cobertura * 10),2,1)" & vbNewLine
                    sSql = sSql & "                                     WHEN 1 THEN tp_cob_comp_tb.val_fixo_cobertura" & vbNewLine
                    sSql = sSql & "                                 END" & vbNewLine
                    sSql = sSql & "                        WHEN 'b' THEN ROUND(escolha_plano_tb.imp_segurada * (tp_cob_comp_tb.lim_min_cobertura / 100),2,1)" & vbNewLine
                    sSql = sSql & "                        ELSE 'N�O DEFINIDO'                                                              " & vbNewLine
                    sSql = sSql & "                    END ,                     "
         
            Else
                    sSql = sSql & "imp_segurada =  escolha_plano_tp_cob_tb.val_is,      " & vbNewLine
                    sSql = sSql & "max_is = escolha_plano_tp_cob_tb.val_is,                   " & vbNewLine
                    sSql = sSql & "min_is =escolha_plano_tp_cob_tb.val_is,                                               " & vbNewLine
                    
            End If
            
            sSql = sSql & "           min_cob = tp_cob_comp_tb.lim_min_cobertura / 100,                                             " & vbNewLine
            sSql = sSql & "           max_cob = tp_cob_comp_tb.lim_max_cobertura / 100,                                             " & vbNewLine
            sSql = sSql & "           taxa_net = 0,                                                                                 " & vbNewLine
            sSql = sSql & "           fat_taxa = 0,                                                                                 " & vbNewLine
            sSql = sSql & "           val_premio = 0,                                                                               " & vbNewLine
            sSql = sSql & "           fat_desconto_tecnico = 0,                                                                      " & vbNewLine
            '                         DF - jconceicao - adquar o pronaf para funcionar como OVM
            '                              2006 abr 18
            'P.ROBERTO - 21/03/2011
            'ALTERADO A CONDI��O DO SELECT, POIS, N�O ESTAVA BUSCANDO VALORES FIXOS, EXIBINDO ''
            'sSql = sSql & "           0 AS val_fixo_cobertura,                                           " & vbNewLine
            'sSql = sSql & "           ' ' AS tp_val_cobertura                                             " & vbNewLine
            sSql = sSql & "           ISNULL(tp_cob_comp_tb.val_fixo_cobertura,0) AS val_fixo_cobertura,  " & vbNewLine
            sSql = sSql & "           ISNULL(tp_cob_comp_tb.tp_val_cobertura,' ') AS tp_val_cobertura      " & vbNewLine
'+------------------------------------------------------------
'|INICIO Demanda 18334512 Circular 491 - Petrauskas 11/2014
            sSql = sSql & "         , cobert_obj_risco_tb.val_premio_tarifa, " & vbCrLf _
                        & "           cobert_obj_risco_tb.val_premio_tarifa_sem_custo_assist_facul, " & vbCrLf _
                        & "           val_is_tarifa = isnull(cobert_obj_risco_tb.val_is,0) " & vbCrLf
'|FIM    Demanda 18334512 Circular 491 - Petrauskas 11/2014
'+------------------------------------------------------------
           sSql = sSql & " FROM ESCOLHA_PLANO_TB   " & vbNewLine
           sSql = sSql & " INNER JOIN escolha_plano_tp_cob_tb   " & vbNewLine
           'LUCIANO - CONFITEC - 20/03/2019 - IM00718494 - INICIO
                   sSql = sSql & " ON ESCOLHA_PLANO_TB.PROPOSTA_ID = escolha_plano_tp_cob_tb.PROPOSTA_ID AND ISNULL(ESCOLHA_PLANO_TB.ENDOSSO_ID,0) = ISNULL(escolha_plano_tp_cob_tb.ENDOSSO_ID,0)   " & vbNewLine
                   'LUCIANO - CONFITEC - 20/03/2019 - IM00718494 - FIM
           sSql = sSql & " inner join tp_cob_comp_tb  " & vbNewLine
           sSql = sSql & " on escolha_plano_tp_cob_tb.tp_cob_comp_id = tp_cob_comp_tb.tp_cob_comp_id  " & vbNewLine
           sSql = sSql & " INNER JOIN tp_cobertura_tb   " & vbNewLine
           sSql = sSql & " ON tp_cobertura_tb.tp_cobertura_id = tp_cob_comp_tb.tp_cobertura_id   " & vbNewLine
           sSql = sSql & " INNER JOIN PLANO_TB   " & vbNewLine
           sSql = sSql & " ON ESCOLHA_PLANO_TB.plano_id = PLANO_TB.plano_id    " & vbNewLine
           sSql = sSql & " AND plano_tb.dt_inicio_vigencia = escolha_plano_tb.dt_inicio_vigencia                                " & vbNewLine
           sSql = sSql & " AND plano_tb.produto_id = escolha_plano_tb.produto_id    " & vbNewLine
           sSql = sSql & " INNER JOIN PROPOSTA_TB  " & vbNewLine
           sSql = sSql & " ON   ESCOLHA_PLANO_TB.PROPOSTA_ID  =      PROPOSTA_TB.PROPOSTA_ID  " & vbNewLine
           sSql = sSql & " LEFT JOIN cobert_obj_risco_tb WITH (NOLOCK)  " & vbNewLine
           sSql = sSql & " ON proposta_tb.proposta_id = cobert_obj_risco_tb.proposta_id  " & vbNewLine
           sSql = sSql & " AND cobert_obj_risco_tb.tp_componente_id = " & iTpComponente & vbNewLine
           sSql = sSql & "  AND tp_cobertura_tb.tp_cobertura_id = cobert_obj_risco_tb.tp_cobertura_id   " & vbNewLine
           sSql = sSql & " AND isnull(cobert_obj_risco_tb.endosso_id,0) = (SELECT MAX(isnull(endosso_id,0))   " & vbNewLine
           sSql = sSql & " FROM cobert_obj_risco_tb WITH (NOLOCK)   " & vbNewLine
           sSql = sSql & " WHERE proposta_id = proposta_tb.proposta_id) " & vbNewLine
           sSql = sSql & "     WHERE proposta_tb.proposta_id = " & lPropostaId & vbNewLine
           'sSql = sSql & "       AND tp_componente_tb.tp_componente_id = " & iTpComponente & vbNewLine
           sSql = sSql & "       AND ESCOLHA_PLANO_TB.dt_fim_vigencia IS NULL                                              " & vbNewLine
           sSql = sSql & "        ORDER BY class_tp_cobertura DESC" & vbNewLine
            
           
                 
        Else
    
    
            'carsilva - 21/06/2004 - Exibir o campo tp_cobertura_id da tp_cobertura_tb
            'sSQL = sSQL & "   SELECT tp_cobertura_id = tp_cob_comp_tb.tp_cob_comp_id,                                             " & vbNewLine
            sSql = sSql & "    SELECT tp_cobertura_tb.tp_cobertura_id,                                                              " & vbNewLine
            sSql = sSql & "           tp_cobertura =  CASE tp_cob_comp_tb.class_tp_cobertura                                        " & vbNewLine
            sSql = sSql & "                                WHEN 'a' THEN 'ADICIONAL'                                                " & vbNewLine
            sSql = sSql & "                                WHEN 'b' THEN 'B�SICA'                                                   " & vbNewLine
            sSql = sSql & "                                ELSE 'N�O DEFINIDO'                                                      " & vbNewLine
            sSql = sSql & "                           END ,                                                                         " & vbNewLine
            sSql = sSql & "           plano_tb.nome plano,                                                                          " & vbNewLine
            sSql = sSql & "           plano_tb.plano_id,                                                                            " & vbNewLine
            sSql = sSql & "           tp_cob_comp_tb.class_tp_cobertura,                                                            " & vbNewLine
            sSql = sSql & "           tp_cobertura_tb.nome,                                                                         " & vbNewLine
        
            '13/05/2005 - para pegar o valor correto da cobertura adicional do BB Seguro Vida.
            If txtProduto.Tag = ProdIdBBSeguroVida Then
          
                If Renova_vida = False Then
                '22/06/2016 - Victor Fonseca - Demanda 19295296 - Corre��o Indeniza��o Extra
                'Altera��o feita para alterara a obten��o do valor da IS quando a data de contrata��o for maior que 20/08/2015
                    sSql = sSql & "           imp_segurada =  CASE tp_cob_comp_tb.class_tp_cobertura                                        " & vbNewLine
                    'sSql = sSql & "                              WHEN 'a' THEN ROUND((tp_cob_comp_tb.lim_max_cobertura * 10),2,1)           " & vbNewLine
                    sSql = sSql & "                               WHEN 'a' THEN " & vbNewLine
                    sSql = sSql & "                                 CASE @valor" & vbNewLine '
                    sSql = sSql & "                                     WHEN 0 THEN ROUND((tp_cob_comp_tb.lim_max_cobertura * 10),2,1)" & vbNewLine
                    sSql = sSql & "                                     WHEN 1 THEN tp_cob_comp_tb.val_fixo_cobertura" & vbNewLine
                    sSql = sSql & "                                 END" & vbNewLine
                    sSql = sSql & "                              WHEN 'b' THEN escolha_plano_tb.imp_segurada                                " & vbNewLine
                    sSql = sSql & "                              ELSE 'N�O DEFINIDO'                                                        " & vbNewLine
                    sSql = sSql & "                           END ,                                                                         " & vbNewLine
                    sSql = sSql & "           max_is = CASE tp_cob_comp_tb.class_tp_cobertura                                               " & vbNewLine
                    'sSql = sSql & "                        WHEN 'a' THEN ROUND((tp_cob_comp_tb.lim_max_cobertura * 10),2,1)                 " & vbNewLine
                    sSql = sSql & "                               WHEN 'a' THEN " & vbNewLine
                    sSql = sSql & "                                 CASE @valor" & vbNewLine '
                    sSql = sSql & "                                     WHEN 0 THEN ROUND((tp_cob_comp_tb.lim_max_cobertura * 10),2,1)" & vbNewLine
                    sSql = sSql & "                                     WHEN 1 THEN tp_cob_comp_tb.val_fixo_cobertura" & vbNewLine
                    sSql = sSql & "                                 END" & vbNewLine
                    sSql = sSql & "                        WHEN 'b' THEN ROUND(escolha_plano_tb.imp_segurada * (tp_cob_comp_tb.lim_max_cobertura / 100),2,1)" & vbNewLine
                    sSql = sSql & "                        ELSE 'N�O DEFINIDO'                                                              " & vbNewLine
                    sSql = sSql & "                    END ,                                                                                " & vbNewLine
                    sSql = sSql & "           min_is = CASE tp_cob_comp_tb.class_tp_cobertura                                               " & vbNewLine
                    'sSql = sSql & "                        WHEN 'a' THEN round((tp_cob_comp_tb.lim_min_cobertura * 10),2,1)                 " & vbNewLine
                    sSql = sSql & "                               WHEN 'a' THEN " & vbNewLine
                    sSql = sSql & "                                 CASE @valor" & vbNewLine '
                    sSql = sSql & "                                     WHEN 0 THEN ROUND((tp_cob_comp_tb.lim_min_cobertura * 10),2,1)" & vbNewLine
                    sSql = sSql & "                                     WHEN 1 THEN tp_cob_comp_tb.val_fixo_cobertura" & vbNewLine
                    sSql = sSql & "                                 END" & vbNewLine
                    sSql = sSql & "                        WHEN 'b' THEN ROUND(escolha_plano_tb.imp_segurada * (tp_cob_comp_tb.lim_min_cobertura / 100),2,1)" & vbNewLine
                    sSql = sSql & "                        ELSE 'N�O DEFINIDO'                                                              " & vbNewLine
                    sSql = sSql & "                    END ,                                                                                " & vbNewLine
                Else
                    sSql = sSql & "imp_segurada =  CASE tp_cob_comp_tb.class_tp_cobertura            " & vbNewLine
                    sSql = sSql & "                     WHEN 'a'THEN escolha_plano_tp_cob_tb.val_is " & vbNewLine
                    sSql = sSql & "                     WHEN 'b' THEN escolha_plano_tb.imp_segurada -escolha_plano_tp_cob_tb.val_is  " & vbNewLine
                    sSql = sSql & "                     ELSE 'N�O DEFINIDO'                " & vbNewLine
                    sSql = sSql & "                     END ,                           " & vbNewLine
                    sSql = sSql & "max_is = CASE tp_cob_comp_tb.class_tp_cobertura                   " & vbNewLine
                    sSql = sSql & "              WHEN 'a' THEN escolha_plano_tp_cob_tb.val_is" & vbNewLine
                    sSql = sSql & "              WHEN 'b' THEN escolha_plano_tb.imp_segurada - isnull((select sum(val_is) from escolha_plano_tp_cob_tb ma where ma.proposta_id = proposta_tb.proposta_id and ma.endosso_id = escolha_plano_tb.endosso_id and ma.plano_id = 9309),0) " & vbNewLine
                    sSql = sSql & "              ELSE 'N�O DEFINIDO'                                                              " & vbNewLine
                    sSql = sSql & "              END ,                                                                                " & vbNewLine
                    sSql = sSql & "min_is = CASE tp_cob_comp_tb.class_tp_cobertura                                               " & vbNewLine
                    sSql = sSql & "              WHEN 'a' THEN escolha_plano_tp_cob_tb.val_is" & vbNewLine
                    sSql = sSql & "              WHEN 'b' THEN escolha_plano_tb.imp_segurada - isnull((select sum(val_is) from escolha_plano_tp_cob_tb mb where mb.proposta_id = proposta_tb.proposta_id and mb.endosso_id = escolha_plano_tb.endosso_id and mb.plano_id = 9309),0) " & vbNewLine
                    sSql = sSql & "              ELSE 'N�O DEFINIDO'                                                              " & vbNewLine
                    sSql = sSql & "         END ,                           " & vbNewLine
                End If
       
            Else

                'flowbr3658050
                If bProdutoBESC Then
          
                    sSql = sSql & "           case when tp_cob_comp_tb.tp_val_cobertura='F' then tp_cob_comp_tb.val_fixo_cobertura else escolha_plano_tb.imp_segurada end as imp_segurada,                                                                " & vbNewLine
                    sSql = sSql & "           max_is = ROUND(case when tp_cob_comp_tb.tp_val_cobertura='F' then tp_cob_comp_tb.val_fixo_cobertura else escolha_plano_tb.imp_segurada end * (tp_cob_comp_tb.lim_max_cobertura / 100),2,1), " & vbNewLine
                    sSql = sSql & "           min_is = ROUND(case when tp_cob_comp_tb.tp_val_cobertura='F' then tp_cob_comp_tb.val_fixo_cobertura else escolha_plano_tb.imp_segurada end * (tp_cob_comp_tb.lim_min_cobertura / 100),2,1), " & vbNewLine
          
                Else
          
                    sSql = sSql & "           escolha_plano_tb.imp_segurada,                                                                " & vbNewLine
                    sSql = sSql & "           max_is = ROUND(escolha_plano_tb.imp_segurada * (tp_cob_comp_tb.lim_max_cobertura / 100),2,1), " & vbNewLine
                    sSql = sSql & "           min_is = ROUND(escolha_plano_tb.imp_segurada * (tp_cob_comp_tb.lim_min_cobertura / 100),2,1), " & vbNewLine
            
                End If
            End If
       
            sSql = sSql & "           min_cob = tp_cob_comp_tb.lim_min_cobertura / 100,                                             " & vbNewLine
            sSql = sSql & "           max_cob = tp_cob_comp_tb.lim_max_cobertura / 100,                                             " & vbNewLine
            sSql = sSql & "           taxa_net = 0,                                                                                 " & vbNewLine
            sSql = sSql & "           fat_taxa = 0,                                                                                 " & vbNewLine
            sSql = sSql & "           val_premio = 0,                                                                               " & vbNewLine
            sSql = sSql & "           fat_desconto_tecnico = 0,                                                                      " & vbNewLine
            '                         DF - jconceicao - adquar o pronaf para funcionar como OVM
            '                              2006 abr 18
            'P.ROBERTO - 21/03/2011
            'ALTERADO A CONDI��O DO SELECT, POIS, N�O ESTAVA BUSCANDO VALORES FIXOS, EXIBINDO ''
            'sSql = sSql & "           0 AS val_fixo_cobertura,                                           " & vbNewLine
            'sSql = sSql & "           ' ' AS tp_val_cobertura                                             " & vbNewLine
            sSql = sSql & "           ISNULL(tp_cob_comp_tb.val_fixo_cobertura,0) AS val_fixo_cobertura,  " & vbNewLine
            sSql = sSql & "           ISNULL(tp_cob_comp_tb.tp_val_cobertura,' ') AS tp_val_cobertura      " & vbNewLine
'+------------------------------------------------------------
'|INICIO Demanda 18334512 Circular 491 - Petrauskas 11/2014
            sSql = sSql & "         , cobert_obj_risco_tb.val_premio_tarifa, " & vbCrLf _
                        & "           cobert_obj_risco_tb.val_premio_tarifa_sem_custo_assist_facul, " & vbCrLf _
                        & "           val_is_tarifa = isnull(cobert_obj_risco_tb.val_is,0) " & vbCrLf
'|FIM    Demanda 18334512 Circular 491 - Petrauskas 11/2014
'+------------------------------------------------------------
      
            'Lsa - 14/09/2005
            'Alterado para utilizar a vig�ncia de escolha_plano_tb (dt_escolha)
            sSql = sSql & "      FROM proposta_tb  WITH (NOLOCK)                                                                           " & vbNewLine
            sSql = sSql & "      JOIN escolha_plano_tb  WITH (NOLOCK)                                                                  " & vbNewLine
            sSql = sSql & "        ON escolha_plano_tb.proposta_id = proposta_tb.proposta_id                                        " & vbNewLine

            'FLOW 4242703 - Marcelo Ferreira - Confitec - 28/06/2010
            '--------------------------------------------------------------------------------------
            '       'Gmarques 22/05/2009 -  Carregando as coberturas do endosso atual
            '       'In�cio
            '       'sSql = sSql & "       AND escolha_plano_tb.dt_escolha <= '" & Format(Data_Sistema, "yyyymmdd") & "'                     " & vbNewLine
            '       'FLOW 3780153 - Confitec Sistemas - Cluiz - 20/05/2010
            '       '     Inlcusao da condicao OR (comentado e reescrito abaixo)
            '       'sSql = sSql & "       AND '" & Format(Data_Sistema, "yyyymmdd") & "' BETWEEN escolha_plano_tb.dt_escolha                " & vbNewLine
            '       'sSql = sSql & "                AND ISNULL(escolha_plano_tb.dt_fim_vigencia, '" & Format(Data_Sistema, "yyyymmdd") & "') " & vbNewLine
            '       'fim
            '       sSql = sSql & "       AND (('" & Format(Data_Sistema, "yyyymmdd") & "' BETWEEN escolha_plano_tb.dt_escolha                " & vbNewLine
            '       sSql = sSql & "                AND ISNULL(escolha_plano_tb.dt_fim_vigencia, '" & Format(Data_Sistema, "yyyymmdd") & "')) " & vbNewLine
            '       sSql = sSql & "           OR (proposta_tb.dt_contratacao BETWEEN convert(char(8),escolha_plano_tb.dt_escolha,112) AND ISNULL(escolha_plano_tb.dt_fim_vigencia, proposta_tb.dt_contratacao))) " & vbNewLine
            '       '---
            '---------------------------------------------------------------------------------------
            sSql = sSql & "       AND '" & Format(Data_Sistema, "yyyymmdd") & "' BETWEEN escolha_plano_tb.dt_escolha                " & vbNewLine
            sSql = sSql & "                AND ISNULL(escolha_plano_tb.dt_fim_vigencia, '" & Format(Data_Sistema, "yyyymmdd") & "') " & vbNewLine

            sSql = sSql & "      JOIN plano_tb  WITH (NOLOCK)                                                                               " & vbNewLine
            sSql = sSql & "        ON plano_tb.plano_id = escolha_plano_tb.plano_id                                                 " & vbNewLine
            sSql = sSql & "       AND plano_tb.dt_inicio_vigencia = escolha_plano_tb.dt_inicio_vigencia                             " & vbNewLine
            sSql = sSql & "       AND plano_tb.produto_id = escolha_plano_tb.produto_id                                             " & vbNewLine
            sSql = sSql & "      JOIN tp_plano_tb  WITH (NOLOCK)                                                                       " & vbNewLine
            sSql = sSql & "        ON plano_tb.tp_plano_id = tp_plano_tb.tp_plano_id                                                " & vbNewLine
            sSql = sSql & "       AND proposta_tb.produto_id = tp_plano_tb.produto_id                                               " & vbNewLine
            sSql = sSql & "      JOIN tp_cob_comp_plano_tb  WITH (NOLOCK)                                                              " & vbNewLine
            sSql = sSql & "        ON plano_tb.tp_plano_id = tp_cob_comp_plano_tb.tp_plano_id                                       " & vbNewLine
            sSql = sSql & "      JOIN tp_cob_comp_item_tb  WITH (NOLOCK)                                                               " & vbNewLine
            sSql = sSql & "        ON tp_cob_comp_item_tb.tp_cob_comp_id = tp_cob_comp_plano_tb.tp_cob_comp_id                      " & vbNewLine
            sSql = sSql & "       AND tp_cob_comp_item_tb.produto_id = proposta_tb.produto_id                                       " & vbNewLine
            'carsilva - 21/06/2004 - Inclus�o do ramo_id no join, para evitar duplicidade
            sSql = sSql & "       AND tp_cob_comp_item_tb.ramo_id = proposta_tb.ramo_id                                             " & vbNewLine
            sSql = sSql & "      JOIN tp_cob_comp_tb  WITH (NOLOCK)                                                                    " & vbNewLine
            sSql = sSql & "        ON tp_cob_comp_item_tb.tp_cob_comp_id = tp_cob_comp_tb.tp_cob_comp_id                            " & vbNewLine
            sSql = sSql & "      JOIN tp_componente_tb  WITH (NOLOCK)                                                                  " & vbNewLine
            sSql = sSql & "        ON tp_cob_comp_tb.tp_componente_id = tp_componente_tb.tp_componente_id                           " & vbNewLine
            sSql = sSql & "      JOIN tp_cobertura_tb  WITH (NOLOCK)                                                                   " & vbNewLine
            sSql = sSql & "        ON tp_cob_comp_tb.tp_cobertura_id = tp_cobertura_tb.tp_cobertura_id                              " & vbNewLine
       
            If txtProduto.Tag = ProdIdBBSeguroVida And Renova_vida = True Then
                sSql = sSql & " INNER JOIN escolha_plano_tp_cob_tb  WITH (NOLOCK)    " & vbNewLine
                sSql = sSql & " ON escolha_plano_tp_cob_tb.proposta_id=escolha_plano_tb.proposta_id" & vbNewLine
                sSql = sSql & " AND escolha_plano_tp_cob_tb.plano_id=plano_tb.plano_id" & vbNewLine
                sSql = sSql & " AND isnull(escolha_plano_tp_cob_tb.endosso_id,0) = isnull(escolha_plano_tb.endosso_id,0) " & vbNewLine
                '/* deixar comentario para futura corre��o  INC000005212274 -Corre��o de  duplicidade nas coberturas da tabela escolha_plano_tp_cob_tb / Carlos Gomes **/
                sSql = sSql & " AND isnull(escolha_plano_tp_cob_tb.tp_cob_comp_id,0) = isnull(tp_cob_comp_plano_tb.tp_cob_comp_id,0) " & vbNewLine
               ' /*Fim**/
            End If
'+------------------------------------------------------------
'|INICIO Demanda 18334512 Circular 491 - Petrauskas 11/2014
            sSql = sSql & " LEFT JOIN cobert_obj_risco_tb WITH (NOLOCK) " & vbCrLf _
                        & "        ON proposta_tb.proposta_id = cobert_obj_risco_tb.proposta_id " & vbCrLf _
                        & "       AND cobert_obj_risco_tb.tp_componente_id = " & iTpComponente & vbCrLf _
                        & "       AND tp_cobertura_tb.tp_cobertura_id = cobert_obj_risco_tb.tp_cobertura_id " & vbCrLf _
                        & "       AND isnull(cobert_obj_risco_tb.endosso_id,0) = (SELECT MAX(isnull(endosso_id,0)) " & vbCrLf _
                        & "                                             FROM cobert_obj_risco_tb WITH (NOLOCK) " & vbCrLf _
                        & "                                             WHERE proposta_id = proposta_tb.proposta_id) " & vbCrLf
'|FIM    Demanda 18334512 Circular 491 - Petrauskas 11/2014
'+------------------------------------------------------------

            sSql = sSql & "     WHERE proposta_tb.proposta_id = " & lPropostaId & vbNewLine
            sSql = sSql & "       AND tp_componente_tb.tp_componente_id = " & iTpComponente & vbNewLine
            
            'IM01105412 - CONFITEC SP - LSSOUSA - 16/01/2020 - INICIO
            sSql = sSql & "       AND tp_cob_comp_item_tb.dt_fim_vigencia_comp IS NULL  " & vbNewLine
            'IM01105412 - CONFITEC SP - LSSOUSA - 16/01/2020 - FIM
                        
            sSql = sSql & "AND  plano_tb.dt_fim_vigencia is null" & vbCrLf 'IM00980208 Thalita 22/10/2019
            'alterado em 26/10/2006, por Leandro A. Souza (Stefanini IT Solutions) - flow 177787
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            'Gmarques 22/05/2009 -  Foi comentada a linha abaixo para carrega as coberturas do endosso atual
            'In�cio
            ' sSql = sSql & "       AND escolha_plano_tb.lock = (select max(lock) from escolha_plano_tb  WITH (NOLOCK)   where proposta_id = " & lPropostaId & " ) "
            'fim
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
       
            sSql = sSql & "UNION                                                                                                    " & vbNewLine
       
            sSql = sSql & "    SELECT escolha_tp_cob_generico_tb.tp_cobertura_id,                                                   " & vbNewLine
            sSql = sSql & "           tp_cobertura = 'B�SICA',                                                                      " & vbNewLine
            sSql = sSql & "           plano = '',                                                                                   " & vbNewLine
            sSql = sSql & "           plano_id = 0,                                                                                 " & vbNewLine
            sSql = sSql & "           class_tp_cobertura = 'b',                                                                     " & vbNewLine
            sSql = sSql & "           tp_cobertura_tb.nome,                                                                         " & vbNewLine
            sSql = sSql & "           imp_segurada = escolha_tp_cob_generico_tb.val_is, " & vbNewLine
            sSql = sSql & "           max_is = escolha_tp_cob_generico_tb.val_is,       " & vbNewLine
            sSql = sSql & "           min_is = escolha_tp_cob_generico_tb.val_is,       " & vbNewLine
            sSql = sSql & "           max_cob = 1,                                                                                  " & vbNewLine
            sSql = sSql & "           min_cob = 1,                                                                                  " & vbNewLine
            sSql = sSql & "           taxa_net = case perc_pro_labore                                                               " & vbNewLine
            sSql = sSql & "                        when null then 0                                                                 " & vbNewLine
            sSql = sSql & "                        when 0 then 0                                                                    " & vbNewLine
            sSql = sSql & "                        else fat_taxa / ( isnull(perc_pro_labore,1) * 100 )                              " & vbNewLine
            sSql = sSql & "                      end,                                                                               " & vbNewLine
            sSql = sSql & "           fat_taxa = ISNULL(fat_taxa,0),                                                                " & vbNewLine
            sSql = sSql & "           val_premio,                                                                                   " & vbNewLine
            sSql = sSql & "           fat_desconto_tecnico  ,                                                                        " & vbNewLine
            '                         DF - jconceicao - adquar o pronaf para funcionar como OVM
            '                              2006 abr 18
            sSql = sSql & "           0 AS val_fixo_cobertura,                                           " & vbNewLine
            sSql = sSql & "           ' '  AS tp_val_cobertura                                             " & vbNewLine
'+------------------------------------------------------------
'|INICIO Demanda 18334512 Circular 491 - Petrauskas 11/2014
            sSql = sSql & "         , cobert_obj_risco_tb.val_premio_tarifa, " & vbCrLf _
                        & "           cobert_obj_risco_tb.val_premio_tarifa_sem_custo_assist_facul, " & vbCrLf _
                        & "           val_is_tarifa = isnull(cobert_obj_risco_tb.val_is,0) " & vbCrLf
'|FIM    Demanda 18334512 Circular 491 - Petrauskas 11/2014
'+------------------------------------------------------------
            
            sSql = sSql & "      FROM escolha_tp_cob_generico_tb  WITH (NOLOCK)                                                        " & vbNewLine
'INICIO  alterado 02/09/2013 projeto 17860335 - Melhorias no SEGBR MC_Amaral -confitec
'            sSql = sSql & "      JOIN proposta_tb  WITH (NOLOCK)                                                                       " & vbNewLine
'            sSql = sSql & "        ON escolha_tp_cob_generico_tb.proposta_id = proposta_tb.proposta_id                              " & vbNewLine
'FIM  alterado 02/09/2013 projeto 17860335 - Melhorias no SEGBR MC_Amaral -confitec
            sSql = sSql & "      JOIN tp_cobertura_tb  WITH (NOLOCK)                                                                   " & vbNewLine
            sSql = sSql & "        ON escolha_tp_cob_generico_tb.tp_cobertura_id = tp_cobertura_tb.tp_cobertura_id                  " & vbNewLine
            sSql = sSql & " LEFT JOIN administracao_apolice_tb  WITH (NOLOCK)                                                          " & vbNewLine
            sSql = sSql & "        ON administracao_apolice_tb.proposta_id = escolha_tp_cob_generico_tb.proposta_id                                " & vbNewLine
            sSql = sSql & "       AND administracao_apolice_tb.dt_fim_administracao IS NULL                                         " & vbNewLine
'+------------------------------------------------------------
'|INICIO Demanda 18334512 Circular 491 - Petrauskas 11/2014
            sSql = sSql & " LEFT JOIN cobert_obj_risco_tb WITH (NOLOCK) " & vbCrLf _
                        & "        ON escolha_tp_cob_generico_tb.proposta_id = cobert_obj_risco_tb.proposta_id " & vbCrLf _
                        & "       AND escolha_tp_cob_generico_tb.tp_cobertura_id = cobert_obj_risco_tb.tp_cobertura_id " & vbCrLf _
                        & "       AND isnull(cobert_obj_risco_tb.endosso_id,0) = (SELECT MAX(isnull(endosso_id,0)) " & vbCrLf _
                        & "                                             FROM cobert_obj_risco_tb WITH (NOLOCK) " & vbCrLf _
                        & "                                             WHERE proposta_id = escolha_tp_cob_generico_tb.proposta_id) " & vbCrLf
'|FIM    Demanda 18334512 Circular 491 - Petrauskas 11/2014
'+------------------------------------------------------------
            sSql = sSql & "     WHERE escolha_tp_cob_generico_tb.dt_fim_vigencia_esc IS NULL                                        " & vbNewLine
            sSql = sSql & "       AND escolha_tp_cob_generico_tb.proposta_id = " & lPropostaId & vbNewLine
            sSql = sSql & "  ORDER BY class_tp_cobertura DESC                                                                       " & vbNewLine
    
        End If
    
    Else
        sSql = sSql & "    SELECT  tp_cobertura_id = 0,                                    " & vbNewLine
        sSql = sSql & "            tp_cobertura =  CASE isnull(e.class_tp_cobertura, 'a')  " & vbNewLine
        sSql = sSql & "                                WHEN 'a' THEN 'ADICIONAL'           " & vbNewLine
        sSql = sSql & "                                WHEN 'b' THEN 'B�SICA'              " & vbNewLine
        sSql = sSql & "                                ELSE 'N�O DEFINIDO'                 " & vbNewLine
        sSql = sSql & "                            END ,                                   " & vbNewLine
        sSql = sSql & "            plano = '',                                             " & vbNewLine
        sSql = sSql & "            plano_id = 0,                                           " & vbNewLine
        sSql = sSql & "            class_tp_cobertura = isnull(e.class_tp_cobertura, 'a'), " & vbNewLine
        sSql = sSql & "            c.nome,                                                 " & vbNewLine
        sSql = sSql & "            imp_segurada = e.val_is ,                               " & vbNewLine
        sSql = sSql & "            max_is = e.val_is ,                                     " & vbNewLine
        sSql = sSql & "            min_is = e.val_is ,                                     " & vbNewLine
        sSql = sSql & "            max_cob = 1,                                            " & vbNewLine
        sSql = sSql & "            min_cob = 1,                                            " & vbNewLine
        sSql = sSql & "            taxa_net = val_taxa,                                    " & vbNewLine
        sSql = sSql & "            fat_taxa = ISNULL(val_taxa * 100,0),                    " & vbNewLine
        sSql = sSql & "            e.val_premio,                                           " & vbNewLine
        sSql = sSql & "            fat_desconto_tecnico = 0 ,                              " & vbNewLine
        '                         DF - jconceicao - adquar o pronaf para funcionar como OVM
        '                              2006 abr 18
        sSql = sSql & "           0 AS val_fixo_cobertura,                                 " & vbNewLine
        sSql = sSql & "           ' ' AS tp_val_cobertura                                  " & vbNewLine
'+------------------------------------------------------------
'|INICIO Demanda 18334512 Circular 491 - Petrauskas 11/2014
        sSql = sSql & "         , cobert_obj_risco_tb.val_premio_tarifa, " & vbCrLf _
                    & "           cobert_obj_risco_tb.val_premio_tarifa_sem_custo_assist_facul, " & vbCrLf _
                    & "           val_is_tarifa = isnull(cobert_obj_risco_tb.val_is,0) " & vbCrLf
'|FIM    Demanda 18334512 Circular 491 - Petrauskas 11/2014
'+------------------------------------------------------------

        sSql = sSql & "       FROM escolha_sub_grp_tp_cob_comp_tb e  WITH (NOLOCK)         " & vbNewLine
        sSql = sSql & "       JOIN tp_cob_comp_item_tb ci   WITH (NOLOCK)                  " & vbNewLine
        sSql = sSql & "         ON ci.dt_inicio_vigencia_comp = e.dt_inicio_vigencia_comp  " & vbNewLine
        sSql = sSql & "       JOIN tp_cob_comp_tb cc  WITH (NOLOCK)                        " & vbNewLine
        sSql = sSql & "         ON cc.tp_cob_comp_id  = e.tp_cob_comp_id                   " & vbNewLine
        sSql = sSql & "        AND cc.tp_cob_comp_id = ci.tp_cob_comp_id                   " & vbNewLine
        sSql = sSql & "       JOIN tp_cobertura_tb c   WITH (NOLOCK)                       " & vbNewLine
        sSql = sSql & "         ON c.tp_cobertura_id = cc.tp_cobertura_id                  " & vbNewLine
        
'+------------------------------------------------------------
'|INICIO Demanda 18334512 Circular 491 - Petrauskas 11/2014
        sSql = sSql & "       JOIN apolice_tb ap WITH (NOLOCK)  " & vbCrLf _
                    & "         ON e.apolice_id = ap.apolice_id " & vbCrLf _
                    & "        AND e.ramo_id = ap.ramo_id " & vbCrLf _
                    & "  LEFT JOIN cobert_obj_risco_tb WITH (NOLOCK) " & vbCrLf _
                    & "         ON ap.proposta_id = cobert_obj_risco_tb.proposta_id " & vbCrLf _
                    & "        AND cobert_obj_risco_tb.tp_componente_id = " & iTpComponente & vbCrLf _
                    & "        AND cc.tp_cobertura_id = cobert_obj_risco_tb.tp_cobertura_id " & vbCrLf _
                    & "        AND isnull(cobert_obj_risco_tb.endosso_id,0) = (SELECT MAX(isnull(endosso_id,0)) " & vbCrLf _
                    & "                                              FROM cobert_obj_risco_tb WITH (NOLOCK) " & vbCrLf _
                    & "                                              WHERE proposta_id = ap.proposta_id) " & vbCrLf
'|FIM    Demanda 18334512 Circular 491 - Petrauskas 11/2014
'+------------------------------------------------------------
                                              
        sSql = sSql & "      WHERE e.apolice_id = " & lApoliceId & vbNewLine
        sSql = sSql & "        AND e.sucursal_seguradora_id = " & lSucursalId & vbNewLine
        sSql = sSql & "        AND e.seguradora_cod_susep = " & lSeguradoraId & vbNewLine
        sSql = sSql & "        AND e.ramo_id = " & LramoId & vbNewLine
        sSql = sSql & "        AND e.sub_grupo_id = " & iSubgrupoId & vbNewLine
        sSql = sSql & "        AND cc.tp_componente_id = " & iTpComponente & vbNewLine
        sSql = sSql & "   ORDER BY cc.tp_componente_id  " & vbNewLine

    End If
        
    Set ObterDadosCoberturas = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, sSql, True)
                                           
    Exit Function
TrataErro:
    Call TratarErro("ObterDadosCoberturas", Me.name)
    Call FinalizarAplicacao

End Function

'FLOW 5073467 - Cleiton Silva - Confitec Sistemas - 06/09/2010
'Inclusao do metodo abaixo para retornar as coberturas caso o plano esteja vencido
Private Function ObterDadosCoberturas_Vencidas(lPropostaId As Long, _
                                               lApoliceId As Long, _
                                               LramoId As Long, _
                                               lSeguradoraId As Long, _
                                               lSucursalId As Long, _
                                               iSubgrupoId As Integer, _
                                               iTpComponente As Integer) As Recordset

    On Error GoTo TrataErro

    Dim sSql As String
    
    sSql = ""
    
    If iSubgrupoId = -1 Then

        'DF - jconceicao - adquar o pronaf para funcionar como OVM
        '     2006 abr 18
        If txtProduto.Tag = ProdIdOuroVidaMulher Or txtProduto.Tag = ProdIdBBSeguroVidaAgriculturaFamiliar Then
    
            sSql = sSql & "    SELECT tp_cobertura_tb.tp_cobertura_id,                  " & vbNewLine
            sSql = sSql & "           tp_cobertura =  CASE tp_cob_comp_tb.class_tp_cobertura " & vbNewLine
            sSql = sSql & "                                WHEN 'a' THEN 'ADICIONAL'    " & vbNewLine
            sSql = sSql & "                                WHEN 'b' THEN 'B�SICA'       " & vbNewLine
            sSql = sSql & "                                ELSE 'N�O DEFINIDO'          " & vbNewLine
            sSql = sSql & "                           END ,                             " & vbNewLine
            sSql = sSql & "           plano_tb.nome plano,                              " & vbNewLine
            sSql = sSql & "           plano_tb.plano_id,                                " & vbNewLine
            sSql = sSql & "           tp_cob_comp_tb.class_tp_cobertura,                " & vbNewLine
            sSql = sSql & "           tp_cobertura_tb.nome,                             " & vbNewLine
            sSql = sSql & "           escolha_plano_tp_cob_tb.val_is,                   " & vbNewLine
            sSql = sSql & "           max_is = ROUND(escolha_plano_tp_cob_tb.val_is * (tp_cob_comp_tb.lim_max_cobertura / 100),2,1)," & vbNewLine
            sSql = sSql & "           min_is = ROUND(escolha_plano_tp_cob_tb.val_is * (tp_cob_comp_tb.lim_min_cobertura / 100),2,1)," & vbNewLine
            sSql = sSql & "           min_cob = tp_cob_comp_tb.lim_min_cobertura / 100, " & vbNewLine
            sSql = sSql & "           max_cob = tp_cob_comp_tb.lim_max_cobertura / 100, " & vbNewLine
            sSql = sSql & "           taxa_net = 0,                                     " & vbNewLine
            sSql = sSql & "           fat_taxa = 0,                                     " & vbNewLine
            sSql = sSql & "           val_premio = 0,                                   " & vbNewLine
            sSql = sSql & "           fat_desconto_tecnico = 0,                         " & vbNewLine
            '                         DF - jconceicao - adquar o pronaf para funcionar como OVM
            '                              2006 abr 18
            sSql = sSql & "           ISNULL(val_fixo_cobertura, 0) AS val_fixo_cobertura,                   " & vbNewLine
            sSql = sSql & "           ISNULL(tp_val_cobertura, 'p') AS tp_val_cobertura                      " & vbNewLine
'+------------------------------------------------------------
'|INICIO Demanda 18334512 Circular 491 - Petrauskas 11/2014
            sSql = sSql & "         , cobert_obj_risco_tb.val_premio_tarifa, " & vbCrLf _
                        & "           cobert_obj_risco_tb.val_premio_tarifa_sem_custo_assist_facul, " & vbCrLf _
                        & "           val_is_tarifa = isnull(cobert_obj_risco_tb.val_is,0) " & vbCrLf
'|FIM    Demanda 18334512 Circular 491 - Petrauskas 11/2014
'+------------------------------------------------------------
            sSql = sSql & "      FROM proposta_tb  WITH (NOLOCK)                                             " & vbNewLine
            sSql = sSql & "      JOIN escolha_plano_tb  WITH (NOLOCK)                                        " & vbNewLine
            sSql = sSql & "        ON escolha_plano_tb.proposta_id = proposta_tb.proposta_id                 " & vbNewLine
'            sSql = sSql & "      JOIN escolha_plano_tp_cob_tb  WITH (NOLOCK)                                " & vbNewLine
'            sSql = sSql & "        ON escolha_plano_tp_cob_tb.proposta_id = proposta_tb.proposta_id         " & vbNewLine
'            sSql = sSql & "       AND escolha_plano_tp_cob_tb.dt_fim_vigencia_cob IS NULL                   " & vbNewLine
           
            'sSql = sSql & "       FROM escolha_plano_tb  WITH (NOLOCK)                                      " & vbNewLine
            sSql = sSql & "       JOIN escolha_plano_tp_cob_tb  WITH (NOLOCK)                                " & vbNewLine
            sSql = sSql & "        ON escolha_plano_tp_cob_tb.proposta_id = escolha_plano_tb.proposta_id     " & vbNewLine
            sSql = sSql & "       AND escolha_plano_tp_cob_tb.dt_fim_vigencia_cob IS NULL                    " & vbNewLine
            sSql = sSql & "      JOIN tp_cob_comp_tb  WITH (NOLOCK)                                          " & vbNewLine
            sSql = sSql & "        ON tp_cob_comp_tb.tp_cob_comp_id = escolha_plano_tp_cob_tb.tp_cob_comp_id " & vbNewLine
            sSql = sSql & "      JOIN tp_componente_tb  WITH (NOLOCK)                                        " & vbNewLine
            sSql = sSql & "        ON tp_componente_tb.tp_componente_id = tp_cob_comp_tb.tp_componente_id    " & vbNewLine
            sSql = sSql & "      JOIN tp_cobertura_tb  WITH (NOLOCK)                                         " & vbNewLine
            sSql = sSql & "        ON tp_cobertura_tb.tp_cobertura_id = tp_cob_comp_tb.tp_cobertura_id       " & vbNewLine
            sSql = sSql & "      JOIN plano_tb  WITH (NOLOCK)                                                " & vbNewLine
            sSql = sSql & "        ON plano_tb.plano_id = escolha_plano_tb.plano_id                          " & vbNewLine
            
'+------------------------------------------------------------
'|INICIO Demanda 18334512 Circular 491 - Petrauskas 11/2014
            sSql = sSql & " LEFT JOIN cobert_obj_risco_tb WITH (NOLOCK) " & vbCrLf _
                        & "        ON escolha_plano_tb.proposta_id = cobert_obj_risco_tb.proposta_id " & vbCrLf _
                        & "       AND cobert_obj_risco_tb.tp_componente_id = " & iTpComponente & vbCrLf _
                        & "       AND tp_cobertura_tb.tp_cobertura_id = cobert_obj_risco_tb.tp_cobertura_id " & vbCrLf _
                        & "       AND isnull(cobert_obj_risco_tb.endosso_id,0) = (SELECT MAX(isnull(endosso_id,0)) " & vbCrLf _
                        & "                                             FROM cobert_obj_risco_tb WITH (NOLOCK) " & vbCrLf _
                        & "                                             WHERE proposta_id = escolha_plano_tb.proposta_id) " & vbCrLf
'|FIM    Demanda 18334512 Circular 491 - Petrauskas 11/2014
'+------------------------------------------------------------
            
            sSql = sSql & "      WHERE escolha_plano_tb.proposta_id = " & lPropostaId & vbNewLine
       
            'bcarneiro - 01/08/2005 - Erro na obten��o da vig�ncia do plano
            'sSQL = sSQL & "        AND (plano_tb.dt_fim_vigencia is NULL " & vbNewLine
            'sSQL = sSQL & "             OR (plano_tb.dt_inicio_vigencia < = proposta_tb.dt_contratacao " & vbNewLine
            'sSQL = sSQL & "                 AND plano_tb.dt_fim_vigencia > = proposta_tb.dt_contratacao)) " & vbNewLine
            sSql = sSql & "        AND plano_tb.dt_inicio_vigencia <= proposta_tb.dt_contratacao             " & vbNewLine
            sSql = sSql & "        AND (plano_tb.dt_fim_vigencia is NULL                                     " & vbNewLine
            sSql = sSql & "             OR plano_tb.dt_fim_vigencia >= proposta_tb.dt_contratacao)           " & vbNewLine
            ''''''''''''''''''''''''''''''''''''''
       
            sSql = sSql & "       AND tp_componente_tb.tp_componente_id = " & iTpComponente & vbNewLine
            sSql = sSql & "       AND escolha_plano_tb.dt_fim_vigencia IS NULL    " & vbNewLine
            sSql = sSql & " UNION " & vbNewLine
            sSql = sSql & "    SELECT escolha_tp_cob_generico_tb.tp_cobertura_id,       " & vbNewLine
            sSql = sSql & "           tp_cobertura = 'B�SICA',                          " & vbNewLine
            sSql = sSql & "           plano = '',                                       " & vbNewLine
            sSql = sSql & "           plano_id = 0,                                     " & vbNewLine
            sSql = sSql & "           class_tp_cobertura = 'b',                         " & vbNewLine
            sSql = sSql & "           tp_cobertura_tb.nome,                             " & vbNewLine
            sSql = sSql & "           imp_segurada = escolha_tp_cob_generico_tb.val_is, " & vbNewLine
            sSql = sSql & "           max_is = escolha_tp_cob_generico_tb.val_is,       " & vbNewLine
            sSql = sSql & "           min_is = escolha_tp_cob_generico_tb.val_is,       " & vbNewLine
            sSql = sSql & "           max_cob = 1,                                      " & vbNewLine
            sSql = sSql & "           min_cob = 1,                                      " & vbNewLine
            sSql = sSql & "           taxa_net = case perc_pro_labore                   " & vbNewLine
            sSql = sSql & "                        when null then 0                     " & vbNewLine
            sSql = sSql & "                        when 0 then 0                        " & vbNewLine
            sSql = sSql & "                        else fat_taxa / ( isnull(perc_pro_labore,1) * 100 ) " & vbNewLine
            sSql = sSql & "                      end,                                   " & vbNewLine
            sSql = sSql & "           fat_taxa = ISNULL(fat_taxa,0),                    " & vbNewLine
            sSql = sSql & "           val_premio,                                       " & vbNewLine
            sSql = sSql & "           fat_desconto_tecnico  ,                           " & vbNewLine
            '                         DF - jconceicao - adquar o pronaf para funcionar como OVM
            '                              2006 abr 18
            sSql = sSql & "           0 AS val_fixo_cobertura,                          " & vbNewLine
            sSql = sSql & "           ' ' AS tp_val_cobertura                           " & vbNewLine
'+------------------------------------------------------------
'|INICIO Demanda 18334512 Circular 491 - Petrauskas 11/2014
            sSql = sSql & "         , cobert_obj_risco_tb.val_premio_tarifa, " & vbCrLf _
                        & "           cobert_obj_risco_tb.val_premio_tarifa_sem_custo_assist_facul, " & vbCrLf _
                        & "           val_is_tarifa = isnull(cobert_obj_risco_tb.val_is,0) " & vbCrLf
'|FIM    Demanda 18334512 Circular 491 - Petrauskas 11/2014
'+------------------------------------------------------------

            sSql = sSql & "      FROM escolha_tp_cob_generico_tb  WITH (NOLOCK)                                     " & vbNewLine
'            sSql = sSql & "      JOIN proposta_tb  WITH (NOLOCK)                                                   " & vbNewLine
'            sSql = sSql & "        ON escolha_tp_cob_generico_tb.proposta_id = proposta_tb.proposta_id             " & vbNewLine
            sSql = sSql & "      JOIN tp_cobertura_tb  WITH (NOLOCK)                                                " & vbNewLine
            sSql = sSql & "        ON escolha_tp_cob_generico_tb.tp_cobertura_id = tp_cobertura_tb.tp_cobertura_id  " & vbNewLine
            sSql = sSql & " LEFT JOIN administracao_apolice_tb  WITH (NOLOCK)                                       " & vbNewLine
            sSql = sSql & "        ON administracao_apolice_tb.proposta_id = escolha_tp_cob_generico_tb.proposta_id " & vbNewLine
            sSql = sSql & "       AND administracao_apolice_tb.dt_fim_administracao IS NULL                         " & vbNewLine

'+------------------------------------------------------------
'|INICIO Demanda 18334512 Circular 491 - Petrauskas 11/2014
            sSql = sSql & " LEFT JOIN cobert_obj_risco_tb WITH (NOLOCK) " & vbCrLf _
                        & "        ON escolha_tp_cob_generico_tb.proposta_id = cobert_obj_risco_tb.proposta_id " & vbCrLf _
                        & "       AND escolha_tp_cob_generico_tb.tp_cobertura_id = cobert_obj_risco_tb.tp_cobertura_id " & vbCrLf _
                        & "       AND isnull(cobert_obj_risco_tb.endosso_id,0) = (SELECT MAX(isnull(endosso_id,0)) " & vbCrLf _
                        & "                                             FROM cobert_obj_risco_tb WITH (NOLOCK) " & vbCrLf _
                        & "                                             WHERE proposta_id = escolha_tp_cob_generico_tb.proposta_id) " & vbCrLf
'|FIM    Demanda 18334512 Circular 491 - Petrauskas 11/2014
'+------------------------------------------------------------
            sSql = sSql & "     WHERE escolha_tp_cob_generico_tb.dt_fim_vigencia_esc IS NULL " & vbNewLine
            sSql = sSql & "       AND escolha_tp_cob_generico_tb.proposta_id = " & lPropostaId & vbNewLine
            sSql = sSql & "  ORDER BY class_tp_cobertura DESC                                " & vbNewLine
    
        Else
    
            'carsilva - 21/06/2004 - Exibir o campo tp_cobertura_id da tp_cobertura_tb
            'sSQL = sSQL & "   SELECT tp_cobertura_id = tp_cob_comp_tb.tp_cob_comp_id,       " & vbNewLine
            sSql = sSql & "    SELECT tp_cobertura_tb.tp_cobertura_id,                       " & vbNewLine
            sSql = sSql & "           tp_cobertura =  CASE tp_cob_comp_tb.class_tp_cobertura " & vbNewLine
            sSql = sSql & "                                WHEN 'a' THEN 'ADICIONAL'         " & vbNewLine
            sSql = sSql & "                                WHEN 'b' THEN 'B�SICA'            " & vbNewLine
            sSql = sSql & "                                ELSE 'N�O DEFINIDO'               " & vbNewLine
            sSql = sSql & "                           END ,                                  " & vbNewLine
            sSql = sSql & "           plano_tb.nome plano,                                   " & vbNewLine
            sSql = sSql & "           plano_tb.plano_id,                                     " & vbNewLine
            sSql = sSql & "           tp_cob_comp_tb.class_tp_cobertura,                     " & vbNewLine
            sSql = sSql & "           tp_cobertura_tb.nome,                                  " & vbNewLine
        
            '13/05/2005 - para pegar o valor correto da cobertura adicional do BB Seguro Vida.
            If txtProduto.Tag = ProdIdBBSeguroVida Then
          
                If Renova_vida = False Then
                    sSql = sSql & "           imp_segurada =  CASE tp_cob_comp_tb.class_tp_cobertura                                        " & vbNewLine
                    sSql = sSql & "                              WHEN 'a' THEN ROUND((tp_cob_comp_tb.lim_max_cobertura * 10),2,1)           " & vbNewLine
                    sSql = sSql & "                              WHEN 'b' THEN escolha_plano_tb.imp_segurada                                " & vbNewLine
                    sSql = sSql & "                              ELSE 'N�O DEFINIDO'                                                        " & vbNewLine
                    sSql = sSql & "                           END ,                                                                         " & vbNewLine
                    sSql = sSql & "           max_is = CASE tp_cob_comp_tb.class_tp_cobertura                                               " & vbNewLine
                    sSql = sSql & "                        WHEN 'a' THEN ROUND((tp_cob_comp_tb.lim_max_cobertura * 10),2,1)                 " & vbNewLine
                    sSql = sSql & "                        WHEN 'b' THEN ROUND(escolha_plano_tb.imp_segurada * (tp_cob_comp_tb.lim_max_cobertura / 100),2,1)" & vbNewLine
                    sSql = sSql & "                        ELSE 'N�O DEFINIDO'                                                              " & vbNewLine
                    sSql = sSql & "                    END ,                                                                                " & vbNewLine
                    sSql = sSql & "           min_is = CASE tp_cob_comp_tb.class_tp_cobertura                                               " & vbNewLine
                    sSql = sSql & "                        WHEN 'a' THEN round((tp_cob_comp_tb.lim_min_cobertura * 10),2,1)                 " & vbNewLine
                    sSql = sSql & "                        WHEN 'b' THEN ROUND(escolha_plano_tb.imp_segurada * (tp_cob_comp_tb.lim_min_cobertura / 100),2,1)" & vbNewLine
                    sSql = sSql & "                        ELSE 'N�O DEFINIDO'                                                              " & vbNewLine
                    sSql = sSql & "                    END ,                                                                                " & vbNewLine
                Else
                    sSql = sSql & "imp_segurada =  CASE tp_cob_comp_tb.class_tp_cobertura " & vbNewLine
                    sSql = sSql & "                     WHEN 'a' THEN escolha_plano_tp_cob_tb.val_is " & vbNewLine
                    sSql = sSql & "                     WHEN 'b' THEN escolha_plano_tb.imp_segurada - escolha_plano_tp_cob_tb.val_is  " & vbNewLine
                    sSql = sSql & "                     ELSE 'N�O DEFINIDO' " & vbNewLine
                    sSql = sSql & "                     END , " & vbNewLine
                    sSql = sSql & "max_is = CASE tp_cob_comp_tb.class_tp_cobertura " & vbNewLine
                    sSql = sSql & "              WHEN 'a' THEN escolha_plano_tp_cob_tb.val_is" & vbNewLine
                    sSql = sSql & "              WHEN 'b' THEN escolha_plano_tb.imp_segurada - isnull((select sum(val_is) from escolha_plano_tp_cob_tb ma where ma.proposta_id = proposta_tb.proposta_id and ma.endosso_id = escolha_plano_tb.endosso_id and ma.plano_id = 9309),0) " & vbNewLine
                    sSql = sSql & "              ELSE 'N�O DEFINIDO' " & vbNewLine
                    sSql = sSql & "              END, " & vbNewLine
                    sSql = sSql & "min_is = CASE tp_cob_comp_tb.class_tp_cobertura " & vbNewLine
                    sSql = sSql & "              WHEN 'a' THEN escolha_plano_tp_cob_tb.val_is " & vbNewLine
                    sSql = sSql & "              WHEN 'b' THEN escolha_plano_tb.imp_segurada - isnull((select sum(val_is) from escolha_plano_tp_cob_tb mb where mb.proposta_id = proposta_tb.proposta_id and mb.endosso_id = escolha_plano_tb.endosso_id and mb.plano_id = 9309),0) " & vbNewLine
                    sSql = sSql & "              ELSE 'N�O DEFINIDO' " & vbNewLine
                    sSql = sSql & "         END, " & vbNewLine
                End If
       
            Else
                sSql = sSql & "           escolha_plano_tb.imp_segurada,                                                                " & vbNewLine
                sSql = sSql & "           max_is = ROUND(escolha_plano_tb.imp_segurada * (tp_cob_comp_tb.lim_max_cobertura / 100),2,1), " & vbNewLine
                sSql = sSql & "           min_is = ROUND(escolha_plano_tb.imp_segurada * (tp_cob_comp_tb.lim_min_cobertura / 100),2,1), " & vbNewLine
          
            End If
       
            sSql = sSql & "           min_cob = tp_cob_comp_tb.lim_min_cobertura / 100, " & vbNewLine
            sSql = sSql & "           max_cob = tp_cob_comp_tb.lim_max_cobertura / 100, " & vbNewLine
            sSql = sSql & "           taxa_net = 0,                                     " & vbNewLine
            sSql = sSql & "           fat_taxa = 0,                                     " & vbNewLine
            sSql = sSql & "           val_premio = 0,                                   " & vbNewLine
            sSql = sSql & "           fat_desconto_tecnico = 0,                         " & vbNewLine
            '                         DF - jconceicao - adquar o pronaf para funcionar como OVM
            '                              2006 abr 18
            sSql = sSql & "           0 AS val_fixo_cobertura,                          " & vbNewLine
            sSql = sSql & "           ' ' AS tp_val_cobertura                           " & vbNewLine
'+------------------------------------------------------------
'|INICIO Demanda 18334512 Circular 491 - Petrauskas 11/2014
            sSql = sSql & "         , cobert_obj_risco_tb.val_premio_tarifa, " & vbCrLf _
                        & "           cobert_obj_risco_tb.val_premio_tarifa_sem_custo_assist_facul, " & vbCrLf _
                        & "           val_is_tarifa = isnull(cobert_obj_risco_tb.val_is,0) " & vbCrLf
'|FIM    Demanda 18334512 Circular 491 - Petrauskas 11/2014
'+------------------------------------------------------------
      
            'Lsa - 14/09/2005
            'Alterado para utilizar a vig�ncia de escolha_plano_tb (dt_escolha)
            sSql = sSql & "      FROM proposta_tb  WITH (NOLOCK)                             " & vbNewLine
            sSql = sSql & "      JOIN escolha_plano_tb  WITH (NOLOCK)                        " & vbNewLine
            sSql = sSql & "        ON escolha_plano_tb.proposta_id = proposta_tb.proposta_id " & vbNewLine

            'FLOW 4242703 - Marcelo Ferreira - Confitec - 28/06/2010
            '--------------------------------------------------------------------------------------
            '       'Gmarques 22/05/2009 -  Carregando as coberturas do endosso atual
            '       'In�cio
            '       'sSql = sSql & "       AND escolha_plano_tb.dt_escolha <= '" & Format(Data_Sistema, "yyyymmdd") & "'                     " & vbNewLine
            '       'FLOW 3780153 - Confitec Sistemas - Cluiz - 20/05/2010
            '       '     Inlcusao da condicao OR (comentado e reescrito abaixo)
            '       'sSql = sSql & "       AND '" & Format(Data_Sistema, "yyyymmdd") & "' BETWEEN escolha_plano_tb.dt_escolha                " & vbNewLine
            '       'sSql = sSql & "                AND ISNULL(escolha_plano_tb.dt_fim_vigencia, '" & Format(Data_Sistema, "yyyymmdd") & "') " & vbNewLine
            '       'fim
            '       sSql = sSql & "       AND (('" & Format(Data_Sistema, "yyyymmdd") & "' BETWEEN escolha_plano_tb.dt_escolha                " & vbNewLine
            '       sSql = sSql & "                AND ISNULL(escolha_plano_tb.dt_fim_vigencia, '" & Format(Data_Sistema, "yyyymmdd") & "')) " & vbNewLine
            '       sSql = sSql & "           OR (proposta_tb.dt_contratacao BETWEEN convert(char(8),escolha_plano_tb.dt_escolha,112) AND ISNULL(escolha_plano_tb.dt_fim_vigencia, proposta_tb.dt_contratacao))) " & vbNewLine
            '       '---
            '---------------------------------------------------------------------------------------
            'FLOW 5073467 - Cleiton Silva - Confitec Sistemas - 06/09/2010
            sSql = sSql & "       AND (('" & Format(Data_Sistema, "yyyymmdd") & "' BETWEEN escolha_plano_tb.dt_escolha                " & vbNewLine
            sSql = sSql & "                AND ISNULL(escolha_plano_tb.dt_fim_vigencia, '" & Format(Data_Sistema, "yyyymmdd") & "')) " & vbNewLine
            ' In�cio - IM00051803 - 29/06/2017
            sSql = sSql & "           OR (SELECT max(dt_escolha) FROM seguros_db.dbo.escolha_plano_tb where proposta_id = " & lPropostaId & ") " & vbNewLine
            sSql = sSql & "      BETWEEN CONVERT(CHAR(8), escolha_plano_tb.dt_escolha, 112) AND Isnull(escolha_plano_tb.dt_fim_vigencia, proposta_tb.dt_contratacao)) " & vbNewLine
            'sSql = sSql & "           OR (proposta_tb.dt_contratacao BETWEEN convert(char(8),escolha_plano_tb.dt_escolha,112) AND ISNULL(escolha_plano_tb.dt_fim_vigencia, proposta_tb.dt_contratacao))) " & vbNewLine
            ' Fim IM00051803 - 29/06/2017
            sSql = sSql & "      JOIN plano_tb  WITH (NOLOCK)                                                                          " & vbNewLine
            sSql = sSql & "        ON plano_tb.plano_id = escolha_plano_tb.plano_id                                                 " & vbNewLine
            sSql = sSql & "       AND plano_tb.dt_inicio_vigencia = escolha_plano_tb.dt_inicio_vigencia                             " & vbNewLine
            sSql = sSql & "       AND plano_tb.produto_id = escolha_plano_tb.produto_id                                             " & vbNewLine
            sSql = sSql & "      JOIN tp_plano_tb  WITH (NOLOCK)                                                                       " & vbNewLine
            sSql = sSql & "        ON plano_tb.tp_plano_id = tp_plano_tb.tp_plano_id                                                " & vbNewLine
            sSql = sSql & "       AND proposta_tb.produto_id = tp_plano_tb.produto_id                                               " & vbNewLine
            sSql = sSql & "      JOIN tp_cob_comp_plano_tb  WITH (NOLOCK)                                                              " & vbNewLine
            sSql = sSql & "        ON plano_tb.tp_plano_id = tp_cob_comp_plano_tb.tp_plano_id                                       " & vbNewLine
            sSql = sSql & "      JOIN tp_cob_comp_item_tb  WITH (NOLOCK)                                                               " & vbNewLine
            sSql = sSql & "        ON tp_cob_comp_item_tb.tp_cob_comp_id = tp_cob_comp_plano_tb.tp_cob_comp_id                      " & vbNewLine
            sSql = sSql & "       AND tp_cob_comp_item_tb.produto_id = proposta_tb.produto_id                                       " & vbNewLine
            'carsilva - 21/06/2004 - Inclus�o do ramo_id no join, para evitar duplicidade
            sSql = sSql & "       AND tp_cob_comp_item_tb.ramo_id = proposta_tb.ramo_id                                             " & vbNewLine
            sSql = sSql & "      JOIN tp_cob_comp_tb  WITH (NOLOCK)                                                                    " & vbNewLine
            sSql = sSql & "        ON tp_cob_comp_item_tb.tp_cob_comp_id = tp_cob_comp_tb.tp_cob_comp_id                            " & vbNewLine
            sSql = sSql & "      JOIN tp_componente_tb  WITH (NOLOCK)                                                                  " & vbNewLine
            sSql = sSql & "        ON tp_cob_comp_tb.tp_componente_id = tp_componente_tb.tp_componente_id                           " & vbNewLine
            sSql = sSql & "      JOIN tp_cobertura_tb  WITH (NOLOCK)                                                                   " & vbNewLine
            sSql = sSql & "        ON tp_cob_comp_tb.tp_cobertura_id = tp_cobertura_tb.tp_cobertura_id                              " & vbNewLine
       
            If txtProduto.Tag = ProdIdBBSeguroVida And Renova_vida = True Then
                sSql = sSql & " INNER JOIN escolha_plano_tp_cob_tb  WITH (NOLOCK)    " & vbNewLine
                sSql = sSql & " ON escolha_plano_tp_cob_tb.proposta_id=escolha_plano_tb.proposta_id" & vbNewLine
                sSql = sSql & " AND escolha_plano_tp_cob_tb.plano_id=plano_tb.plano_id" & vbNewLine
                sSql = sSql & " AND isnull(escolha_plano_tp_cob_tb.endosso_id,0) = isnull(escolha_plano_tb.endosso_id,0) " & vbNewLine
              '/*INC000005212274 -Corre��o de  duplicidade nas coberturas da tabela escolha_plano_tp_cob_tb / Carlos Gomes **/
                'sSql = sSql & " AND isnull(escolha_plano_tp_cob_tb.tp_cob_comp_id,0) = isnull(tp_cob_comp_plano_tb.tp_cob_comp_id,0) " & vbNewLine
               ' /*fim**/
            End If
'+------------------------------------------------------------
'|INICIO Demanda 18334512 Circular 491 - Petrauskas 11/2014
            sSql = sSql & " LEFT JOIN cobert_obj_risco_tb WITH (NOLOCK) " & vbCrLf _
                        & "        ON proposta_tb.proposta_id = cobert_obj_risco_tb.proposta_id " & vbCrLf _
                        & "       AND cobert_obj_risco_tb.tp_componente_id = " & iTpComponente & vbCrLf _
                        & "       AND tp_cobertura_tb.tp_cobertura_id = cobert_obj_risco_tb.tp_cobertura_id " & vbCrLf _
                        & "       AND isnull(cobert_obj_risco_tb.endosso_id,0) = (SELECT MAX(isnull(endosso_id,0)) " & vbCrLf _
                        & "                                             FROM cobert_obj_risco_tb WITH (NOLOCK) " & vbCrLf _
                        & "                                             WHERE proposta_id = proposta_tb.proposta_id) " & vbCrLf
'|FIM    Demanda 18334512 Circular 491 - Petrauskas 11/2014
'+------------------------------------------------------------
      
            sSql = sSql & "     WHERE proposta_tb.proposta_id = " & lPropostaId & vbNewLine
            sSql = sSql & "       AND tp_componente_tb.tp_componente_id = " & iTpComponente & vbNewLine
            sSql = sSql & "       AND tp_cob_comp_item_tb.dt_fim_vigencia_comp IS NULL                                              " & vbNewLine
       
            'alterado em 26/10/2006, por Leandro A. Souza (Stefanini IT Solutions) - flow 177787
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            'Gmarques 22/05/2009 -  Foi comentada a linha abaixo para carrega as coberturas do endosso atual
            'In�cio
            ' sSql = sSql & "       AND escolha_plano_tb.lock = (select max(lock) from escolha_plano_tb  WITH (NOLOCK)   where proposta_id = " & lPropostaId & " ) "
            'fim
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
       
            sSql = sSql & "UNION                                                                                                    " & vbNewLine
       
            sSql = sSql & "    SELECT escolha_tp_cob_generico_tb.tp_cobertura_id,                                                   " & vbNewLine
            sSql = sSql & "           tp_cobertura = 'B�SICA',                                                                      " & vbNewLine
            sSql = sSql & "           plano = '',                                                                                   " & vbNewLine
            sSql = sSql & "           plano_id = 0,                                                                                 " & vbNewLine
            sSql = sSql & "           class_tp_cobertura = 'b',                                                                     " & vbNewLine
            sSql = sSql & "           tp_cobertura_tb.nome,                                                                         " & vbNewLine
            sSql = sSql & "           imp_segurada = escolha_tp_cob_generico_tb.val_is,                                                                        " & vbNewLine
            sSql = sSql & "           max_is = escolha_tp_cob_generico_tb.val_is,                                                                              " & vbNewLine
            sSql = sSql & "           min_is = escolha_tp_cob_generico_tb.val_is,                                                                              " & vbNewLine
            sSql = sSql & "           max_cob = 1,                                                                                  " & vbNewLine
            sSql = sSql & "           min_cob = 1,                                                                                  " & vbNewLine
            sSql = sSql & "           taxa_net = case perc_pro_labore                                                               " & vbNewLine
            sSql = sSql & "                        when null then 0                                                                 " & vbNewLine
            sSql = sSql & "                        when 0 then 0                                                                    " & vbNewLine
            sSql = sSql & "                        else fat_taxa / ( isnull(perc_pro_labore,1) * 100 )                              " & vbNewLine
            sSql = sSql & "                      end,                                                                               " & vbNewLine
            sSql = sSql & "           fat_taxa = ISNULL(fat_taxa,0),                                                                " & vbNewLine
            sSql = sSql & "           val_premio,                                                                                   " & vbNewLine
            sSql = sSql & "           fat_desconto_tecnico  ,                                                                        " & vbNewLine
            '                         DF - jconceicao - adquar o pronaf para funcionar como OVM
            '                              2006 abr 18
            sSql = sSql & "           0 AS val_fixo_cobertura,                                           " & vbNewLine
            sSql = sSql & "           ' '  AS tp_val_cobertura                                             " & vbNewLine
'+------------------------------------------------------------
'|INICIO Demanda 18334512 Circular 491 - Petrauskas 11/2014
            sSql = sSql & "         , cobert_obj_risco_tb.val_premio_tarifa, " & vbCrLf _
                        & "           cobert_obj_risco_tb.val_premio_tarifa_sem_custo_assist_facul, " & vbCrLf _
                        & "           val_is_tarifa = isnull(cobert_obj_risco_tb.val_is,0) " & vbCrLf
'|FIM    Demanda 18334512 Circular 491 - Petrauskas 11/2014
'+------------------------------------------------------------
       
            sSql = sSql & "      FROM escolha_tp_cob_generico_tb  WITH (NOLOCK)                                                        " & vbNewLine
'INICIO  alterado 02/09/2013 projeto 17860335 - Melhorias no SEGBR MC_Amaral -confitec
'            sSql = sSql & "      JOIN proposta_tb  WITH (NOLOCK)                                                                       " & vbNewLine
'            sSql = sSql & "        ON escolha_tp_cob_generico_tb.proposta_id = proposta_tb.proposta_id                              " & vbNewLine
  'FIM  alterado 02/09/2013 projeto 17860335 - Melhorias no SEGBR MC_Amaral -confitec
            sSql = sSql & "      JOIN tp_cobertura_tb  WITH (NOLOCK)                                                                   " & vbNewLine
            sSql = sSql & "        ON escolha_tp_cob_generico_tb.tp_cobertura_id = tp_cobertura_tb.tp_cobertura_id                  " & vbNewLine
            sSql = sSql & " LEFT JOIN administracao_apolice_tb  WITH (NOLOCK)                                                          " & vbNewLine
            sSql = sSql & "        ON administracao_apolice_tb.proposta_id = escolha_tp_cob_generico_tb.proposta_id                                " & vbNewLine
            sSql = sSql & "       AND administracao_apolice_tb.dt_fim_administracao IS NULL                                         " & vbNewLine
            
'+------------------------------------------------------------
'|INICIO Demanda 18334512 Circular 491 - Petrauskas 11/2014
            sSql = sSql & " LEFT JOIN cobert_obj_risco_tb WITH (NOLOCK) " & vbCrLf _
                        & "        ON escolha_tp_cob_generico_tb.proposta_id = cobert_obj_risco_tb.proposta_id " & vbCrLf _
                        & "       AND escolha_tp_cob_generico_tb.tp_cobertura_id = cobert_obj_risco_tb.tp_cobertura_id " & vbCrLf _
                        & "       AND isnull(cobert_obj_risco_tb.endosso_id,0) = (SELECT MAX(isnull(endosso_id,0)) " & vbCrLf _
                        & "                                             FROM cobert_obj_risco_tb WITH (NOLOCK) " & vbCrLf _
                        & "                                             WHERE proposta_id = escolha_tp_cob_generico_tb.proposta_id) " & vbCrLf
'|FIM    Demanda 18334512 Circular 491 - Petrauskas 11/2014
'+------------------------------------------------------------
            
            sSql = sSql & "     WHERE escolha_tp_cob_generico_tb.dt_fim_vigencia_esc IS NULL                                        " & vbNewLine
            sSql = sSql & "       AND escolha_tp_cob_generico_tb.proposta_id = " & lPropostaId & vbNewLine
            sSql = sSql & "  ORDER BY class_tp_cobertura DESC                                                                       " & vbNewLine
    
        End If
    
    Else
        sSql = sSql & "    SELECT  tp_cobertura_id = 0,                                                                         " & vbNewLine
        sSql = sSql & "            tp_cobertura =  CASE isnull(e.class_tp_cobertura, 'a')                                       " & vbNewLine
        sSql = sSql & "                                WHEN 'a' THEN 'ADICIONAL'                                                " & vbNewLine
        sSql = sSql & "                                WHEN 'b' THEN 'B�SICA'                                                   " & vbNewLine
        sSql = sSql & "                                ELSE 'N�O DEFINIDO'                                                      " & vbNewLine
        sSql = sSql & "                            END ,                                                                        " & vbNewLine
        sSql = sSql & "            plano = '',                                                                                  " & vbNewLine
        sSql = sSql & "            plano_id = 0,                                                                                " & vbNewLine
        sSql = sSql & "            class_tp_cobertura = isnull(e.class_tp_cobertura, 'a'),                                      " & vbNewLine
        sSql = sSql & "            c.nome,                                                                                      " & vbNewLine
        sSql = sSql & "            imp_segurada = e.val_is ,                                                                    " & vbNewLine
        sSql = sSql & "            max_is = e.val_is ,                                                                          " & vbNewLine
        sSql = sSql & "            min_is = e.val_is ,                                                                          " & vbNewLine
        sSql = sSql & "            max_cob = 1,                                                                                 " & vbNewLine
        sSql = sSql & "            min_cob = 1,                                                                                 " & vbNewLine
        sSql = sSql & "            taxa_net = val_taxa,                                                                         " & vbNewLine
        sSql = sSql & "            fat_taxa = ISNULL(val_taxa * 100,0),                                                         " & vbNewLine
        sSql = sSql & "            e.val_premio,                                                                                " & vbNewLine
        sSql = sSql & "            fat_desconto_tecnico = 0 ,                                                                    " & vbNewLine
        '                         DF - jconceicao - adquar o pronaf para funcionar como OVM
        '                              2006 abr 18
        sSql = sSql & "           0 AS val_fixo_cobertura,                                           " & vbNewLine
        sSql = sSql & "           ' ' AS tp_val_cobertura                                             " & vbNewLine
'+------------------------------------------------------------
'|INICIO Demanda 18334512 Circular 491 - Petrauskas 11/2014
        sSql = sSql & "         , cobert_obj_risco_tb.val_premio_tarifa, " & vbCrLf _
                    & "           cobert_obj_risco_tb.val_premio_tarifa_sem_custo_assist_facul, " & vbCrLf _
                    & "           val_is_tarifa = isnull(cobert_obj_risco_tb.val_is,0) " & vbCrLf
'|FIM    Demanda 18334512 Circular 491 - Petrauskas 11/2014
'+------------------------------------------------------------

        sSql = sSql & "       FROM escolha_sub_grp_tp_cob_comp_tb e  WITH (NOLOCK)                                                 " & vbNewLine
        sSql = sSql & "       JOIN tp_cob_comp_item_tb ci   WITH (NOLOCK)                                                          " & vbNewLine
        sSql = sSql & "         ON ci.dt_inicio_vigencia_comp = e.dt_inicio_vigencia_comp                                       " & vbNewLine
        sSql = sSql & "       JOIN tp_cob_comp_tb cc  WITH (NOLOCK)                                                                " & vbNewLine
        sSql = sSql & "         ON cc.tp_cob_comp_id  = e.tp_cob_comp_id                                                        " & vbNewLine
        sSql = sSql & "        AND cc.tp_cob_comp_id = ci.tp_cob_comp_id                                                        " & vbNewLine
        sSql = sSql & "       JOIN tp_cobertura_tb c   WITH (NOLOCK)                                                               " & vbNewLine
        sSql = sSql & "         ON c.tp_cobertura_id = cc.tp_cobertura_id                                                       " & vbNewLine
        
'+------------------------------------------------------------
'|INICIO Demanda 18334512 Circular 491 - Petrauskas 11/2014
        sSql = sSql & "       JOIN apolice_tb ap WITH (NOLOCK)  " & vbCrLf _
                    & "         ON e.apolice_id = ap.apolice_id " & vbCrLf _
                    & "        AND e.ramo_id = ap.ramo_id " & vbCrLf _
                    & "  LEFT JOIN cobert_obj_risco_tb WITH (NOLOCK) " & vbCrLf _
                    & "         ON ap.proposta_id = cobert_obj_risco_tb.proposta_id " & vbCrLf _
                    & "        AND cc.tp_cobertura_id = cobert_obj_risco_tb.tp_cobertura_id " & vbCrLf _
                    & "        AND isnull(cobert_obj_risco_tb.endosso_id,0) = (SELECT MAX(isnull(endosso_id,0)) " & vbCrLf _
                    & "                                              FROM cobert_obj_risco_tb WITH (NOLOCK) " & vbCrLf _
                    & "                                              WHERE proposta_id = ap.proposta_id) " & vbCrLf
'|FIM    Demanda 18334512 Circular 491 - Petrauskas 11/2014
'+------------------------------------------------------------
        
        sSql = sSql & "      WHERE e.apolice_id = " & lApoliceId & vbNewLine
        sSql = sSql & "        AND e.sucursal_seguradora_id = " & lSucursalId & vbNewLine
        sSql = sSql & "        AND e.seguradora_cod_susep = " & lSeguradoraId & vbNewLine
        sSql = sSql & "        AND e.ramo_id = " & LramoId & vbNewLine
        sSql = sSql & "        AND e.sub_grupo_id = " & iSubgrupoId & vbNewLine
        sSql = sSql & "        AND cc.tp_componente_id = " & iTpComponente & vbNewLine
        sSql = sSql & "   ORDER BY cc.tp_componente_id                                                                          " & vbNewLine

    End If
        
    Set ObterDadosCoberturas_Vencidas = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, sSql, True)
                                           
    Exit Function
TrataErro:
    Call TratarErro("ObterDadosCoberturas", Me.name)
    Call FinalizarAplicacao

End Function

Private Function ObterDadosComponentes(iSubgrupoId As Integer, _
                                       lPropostaId As Long, _
                                       lApoliceId As Long, _
                                       LramoId As Long, _
                                       lSucursalId As Long, _
                                       lSeguradoraId As Long, _
                                       sDtQuery As String) As Recordset
    'lClienteId As Long, _

  On Error GoTo TrataErro
  Dim sSql As String
  
  sSql = ""
  
'+-------------------------------------------------------------------------------
'| Marcelo Melhoria ago/2016
'| Transformar Query em Procedure
'|
'
  If iSubgrupoId = -1 Then
    sSql = " set nocount on exec seguros_db..SEGS13057_SPS 'N', " & lPropostaId & ", '" & sDtQuery & "' "
  Else
    sSql = " set nocount on exec seguros_db..SEGS13057_SPS 'S', " & lPropostaId & ", NULL, " & lSucursalId & ", " & lSeguradoraId & ", " & LramoId & ", " & iSubgrupoId
  End If
  
'    If iSubgrupoId = -1 Then
'        sSql = sSql & " SELECT distinct                                               " & vbNewLine
'        sSql = sSql & "        s.tp_componente_id,                                    " & vbNewLine
'        sSql = sSql & "        c.nome nomecomp                                       " & vbNewLine
'        '    sSQL = sSQL & "        s.ramo_id,                                             " & vbNewLine
'        '    sSQL = sSQL & "        s.subramo_id,                                          " & vbNewLine
'        '    sSQL = sSQL & "        s.dt_inicio_vigencia_seg,                              " & vbNewLine
'        '    sSQL = sSQL & "        sr.nome nomesr,                                        " & vbNewLine
'        '    sSQL = sSQL & "        s.dt_inicio_vigencia_sbr,                              " & vbNewLine
'        '    sSQL = sSQL & "        produto_externo_id                                     " & vbNewLine
'        'INICIO  alterado 02/09/2013 projeto 17860335 - Melhorias no SEGBR MC_Amaral -confitec
''        sSql = sSql & "   FROM seguro_vida_tb  s  WITH (NOLOCK) ,                            " & vbNewLine
''        sSql = sSql & "        tp_componente_tb c  WITH (NOLOCK) ,                           " & vbNewLine
''        sSql = sSql & "        subramo_tb sr  WITH (NOLOCK) ,                                " & vbNewLine
''        sSql = sSql & "        proposta_tb p  WITH (NOLOCK)                                  " & vbNewLine
''        sSql = sSql & "  WHERE p.proposta_id = " & lPropostaId & vbNewLine
''        sSql = sSql & "    AND s.dt_inicio_vigencia_seg <= '" & sDtQuery & "'" & vbNewLine
''        sSql = sSql & "    AND (s.dt_fim_vigencia_seg is null                         " & vbNewLine
''        sSql = sSql & "         OR s.dt_fim_vigencia_seg >= '" & sDtQuery & "')               " & vbNewLine
''        sSql = sSql & "    AND c.tp_componente_id = s.tp_componente_id                " & vbNewLine
''        sSql = sSql & "    AND sr.ramo_id = s.ramo_id                                 " & vbNewLine
''        sSql = sSql & "    AND sr.subramo_id = s.subramo_id                           " & vbNewLine
''        sSql = sSql & "    AND sr.dt_inicio_vigencia_sbr = s.dt_inicio_vigencia_sbr   " & vbNewLine
''        sSql = sSql & "    AND p.proposta_id = s.proposta_id                          " & vbNewLine
'        sSql = sSql & "   FROM seguro_vida_tb  s  WITH (NOLOCK) ,                        " & vbNewLine
'        sSql = sSql & "        tp_componente_tb c  WITH (NOLOCK) ,                        " & vbNewLine
'        sSql = sSql & "        subramo_tb sr  WITH (NOLOCK)                               " & vbNewLine
'        sSql = sSql & "  WHERE s.proposta_id = " & lPropostaId & vbNewLine
'        sSql = sSql & "    AND s.dt_inicio_vigencia_seg <= '" & sDtQuery & "'" & vbNewLine
'        sSql = sSql & "    AND (s.dt_fim_vigencia_seg is null                         " & vbNewLine
'        sSql = sSql & "         OR s.dt_fim_vigencia_seg >= '" & sDtQuery & "')               " & vbNewLine
'        sSql = sSql & "    AND c.tp_componente_id = s.tp_componente_id                " & vbNewLine
'        sSql = sSql & "    AND sr.ramo_id = s.ramo_id                                 " & vbNewLine
'        sSql = sSql & "    AND sr.subramo_id = s.subramo_id                           " & vbNewLine
'        sSql = sSql & "    AND sr.dt_inicio_vigencia_sbr = s.dt_inicio_vigencia_sbr   " & vbNewLine
''FIM  alterado 02/09/2013 projeto 17860335 - Melhorias no SEGBR MC_Amaral -confitec
'    Else
'        sSql = sSql & " SELECT distinct                                                                         " & vbNewLine
'        sSql = sSql & "        tp_componente_tb.tp_componente_id,                                               " & vbNewLine
'        sSql = sSql & "        tp_componente_tb.nome nomecomp                                                   " & vbNewLine
'        sSql = sSql & "   FROM escolha_sub_grp_tp_cob_comp_tb  WITH (NOLOCK)                                       " & vbNewLine
'        sSql = sSql & "   JOIN tp_cob_comp_tb  WITH (NOLOCK)                                                       " & vbNewLine
'        sSql = sSql & "     ON tp_cob_comp_tb.tp_cob_comp_id = escolha_sub_grp_tp_cob_comp_tb.tp_cob_comp_id    " & vbNewLine
'        sSql = sSql & "   JOIN tp_componente_tb  WITH (NOLOCK)                                                         " & vbNewLine
'        sSql = sSql & "     ON tp_componente_tb.tp_componente_id = tp_cob_comp_tb.tp_componente_id              " & vbNewLine
'        sSql = sSql & "  WHERE escolha_sub_grp_tp_cob_comp_tb.apolice_id = " & lApoliceId & vbNewLine
'        sSql = sSql & "    AND escolha_sub_grp_tp_cob_comp_tb.sucursal_seguradora_id = " & lSucursalId & vbNewLine
'        sSql = sSql & "    AND escolha_sub_grp_tp_cob_comp_tb.seguradora_cod_susep = " & lSeguradoraId & vbNewLine
'        sSql = sSql & "    AND escolha_sub_grp_tp_cob_comp_tb.ramo_id = " & LramoId & vbNewLine
'        sSql = sSql & "    AND escolha_sub_grp_tp_cob_comp_tb.sub_grupo_id = " & iSubgrupoId & vbNewLine
'
'    End If
  
    Set ObterDadosComponentes = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, sSql, True)
  
    Exit Function
TrataErro:
    Call TratarErro("ObterDadosComponentes", Me.name)
    Call FinalizarAplicacao

End Function

Private Sub CarregarComboComponentes(iSubgrupoId As Integer, _
                                     lPropostaId As Long, _
                                     lApoliceId As Long, _
                                     LramoId As Long, _
                                     lSucursalId As Long, _
                                     lSeguradoraId As Long, _
                                     sDtQuery As String)
    'lClienteId As Long, _

     On Error GoTo TrataErro

    Dim rsComponentes As Recordset
  
    Set rsComponentes = New Recordset
    Set rsComponentes = ObterDadosComponentes(iSubgrupoId, lPropostaId, lApoliceId, LramoId, lSucursalId, lSeguradoraId, sDtQuery)
  
    cboComponente.Clear
  
    If rsComponentes.EOF Then
        '    Call MsgBox("N�o foi poss�vel obter os componentes para a proposta", vbCritical)
        '    GoTo TrataErro
    
        Call cboComponente.AddItem("1 - Titular")
        cboComponente.ItemData(cboComponente.NewIndex) = 1
     
    End If
  
    While Not rsComponentes.EOF
  
        Call cboComponente.AddItem(rsComponentes("tp_componente_id") & " - " & rsComponentes("nomecomp"))
        cboComponente.ItemData(cboComponente.NewIndex) = rsComponentes("tp_componente_id")
    
        rsComponentes.MoveNext
    Wend
  
    cboComponente.ListIndex = 0
  
    rsComponentes.Close
    Set rsComponentes = Nothing

    Exit Sub
TrataErro:
    Call TratarErro("CarregarComboComponentes", Me.name)
    Call FinalizarAplicacao

End Sub

Private Function CarregarDadosCoberturas(lPropostaId As Long, _
                                         lApoliceId As Long, _
                                         LramoId As Long, _
                                         lSeguradoraId As Long, _
                                         lSucursalId As Long, _
                                         iSubgrupo As Integer, _
                                         iComponente As Integer) As Boolean

    On Error GoTo TrataErro
  
    Dim rsDadosCoberturas As Recordset
    Dim sLinha As String
    Dim bolVerAssistencia As Boolean
    
    CarregarDadosCoberturas = True
    bolVerAssistencia = False
    
    Set rsDadosCoberturas = New Recordset
    sLinha = ""
  
    'Coberturas ''''''''''''''''''''
    Set rsDadosCoberturas = ObterDadosCoberturas(lPropostaId, lApoliceId, LramoId, lSeguradoraId, lSucursalId, iSubgrupo, iComponente)
  
    If rsDadosCoberturas.EOF Then
        'FLOW 5073467 - Cleiton Silva - Confitec Sistemas - 06/09/2010
        'quando a vigencia do plano estiver vencida sera consultado no metodo abaixo
        Set rsDadosCoberturas = Nothing
        Set rsDadosCoberturas = ObterDadosCoberturas_Vencidas(lPropostaId, lApoliceId, LramoId, lSeguradoraId, lSucursalId, iSubgrupo, iComponente)

        If rsDadosCoberturas.EOF Then
            Call MsgBox("N�o foi poss�vel obter as coberturas do componente", vbCritical)
            rsDadosCoberturas.Close
            Set rsDadosCoberturas = Nothing
            CarregarDadosCoberturas = False
            Exit Function
        End If
        
    End If
  
    'Demanda 18334512 Circular 491 - Petrauskas 11/2014
    'gridCobertura.FormatString = "C�d. | Tipo de Cobertura | Descri��o                                        | Capital Segurado M�nimo | Capital Segurado M�ximo"
    gridCobertura.FormatString = "C�d. | Tipo de Cobertura | Descri��o                                        | Capital Segurado M�nimo | Capital Segurado M�ximo | Valor Premio | V_IS_Cob | Valor Premio 395"
    gridCobertura.Rows = 1
    gridCobertura.ColWidth(6) = 0

    While Not rsDadosCoberturas.EOF
    
        txtPlanoIdCob(0).Text = rsDadosCoberturas("plano_id")
        txtPlanoCob(0).Text = rsDadosCoberturas("plano")
    
        sLinha = ""
        sLinha = sLinha & rsDadosCoberturas("tp_cobertura_id") & vbTab
        sLinha = sLinha & Trim(rsDadosCoberturas("tp_cobertura")) & vbTab
        sLinha = sLinha & Trim(rsDadosCoberturas("nome")) & vbTab

        'DF - jconceicao - adquar o pronaf para funcionar como OVM
        '     2006 abr 18
        If UCase(Trim(rsDadosCoberturas("tp_val_cobertura"))) = "F" Then
            sLinha = sLinha & FormataValorParaPadraoBrasil(rsDadosCoberturas("val_fixo_cobertura")) & vbTab
            sLinha = sLinha & FormataValorParaPadraoBrasil(rsDadosCoberturas("val_fixo_cobertura"))
        Else
            sLinha = sLinha & FormataValorParaPadraoBrasil(rsDadosCoberturas("min_is")) & vbTab
            sLinha = sLinha & FormataValorParaPadraoBrasil(rsDadosCoberturas("max_is"))
        End If

        
        'Demanda 18334512 Circular 491 - Petrauskas 11/2014
        If Right(sLinha, 1) <> vbTab Then sLinha = sLinha & vbTab
        
        If IsNull(rsDadosCoberturas("val_premio_tarifa_sem_custo_assist_facul")) Then
          If IsNull(rsDadosCoberturas("val_premio_tarifa")) Then
            'n�o tem registro na cobert_obj_risco_tb
            sLinha = sLinha & "0,00"
          Else
            'verificar se tem custo assistencia facultativo
            sLinha = sLinha & FormataValorParaPadraoBrasil(rsDadosCoberturas("val_premio_tarifa"))
            bolVerAssistencia = True
          End If
        Else
          
          sLinha = sLinha & FormataValorParaPadraoBrasil(rsDadosCoberturas("val_premio_tarifa_sem_custo_assist_facul"))
        End If
        sLinha = sLinha & vbTab & rsDadosCoberturas("val_is_tarifa")
        sLinha = sLinha & vbTab & FormataValorParaPadraoBrasil(IIf(IsNull(rsDadosCoberturas("val_premio_tarifa")), 0, rsDadosCoberturas("val_premio_tarifa")))
                                                            
        Call gridCobertura.AddItem(sLinha)
    
        rsDadosCoberturas.MoveNext
    Wend
  
    rsDadosCoberturas.Close
    Set rsDadosCoberturas = Nothing

'Demanda 18334512 Circular 491 - Petrauskas 11/2014
'Ajustado 03/2015, carga do txtValAssistFacult.Text foi alterado e
'agora s� carrega quando clicado na aba, ap�s a carga das coberturas.

Dim ii As Integer, vAcum As Currency, vAjustado As Currency, fator As Double, valAssistFacult As Variant
    
    'If bolVerAssistencia And CCur(txtValAssistFacult.Text & "0") > 0 Then
    If bolVerAssistencia Then
      Set rsDadosCoberturas = ObterDadosAssistencia(Val(txtProposta.Text), Val(txtProduto.Tag))
      If Not rsDadosCoberturas.EOF Then
        
        valAssistFacult = IIf(IsNull(rsDadosCoberturas!val_assistencia_carregado), 0, rsDadosCoberturas!val_assistencia_carregado)
        
        If valAssistFacult > 0 Then
      'necess�rio ajustar o valor do pr�mio, excluindo o valor assistencia facultativa
      gridCobertura.Col = 6
      For ii = 1 To gridCobertura.Rows - 1
        gridCobertura.Row = ii
        vAcum = vAcum + CCur(gridCobertura.Text)
      Next ii
      
      For ii = 1 To gridCobertura.Rows - 1
        gridCobertura.Row = ii
        gridCobertura.Col = 6
        fator = CCur(gridCobertura.Text) / vAcum
        gridCobertura.Col = 5
              vAjustado = CCur(gridCobertura.Text) - (valAssistFacult * fator)  'CCur(txtValAssistFacult.Text & "0") * fator)
        gridCobertura.Text = FormataValorParaPadraoBrasil(CStr(vAjustado))
      Next ii
        End If
      End If
      rsDadosCoberturas.Close
      Set rsDadosCoberturas = Nothing
      
    End If
    
    Exit Function

TrataErro:
    Call TratarErro("CarregarDadosCoberturas", Me.name)
    Call FinalizarAplicacao

End Function

Private Function CarregarDadosCoberturasRE() As Boolean

    On Error GoTo TrataErro
  
    Dim rsDadosCoberturas As Recordset
    Dim sLinha As String
  
    Set rsDadosCoberturas = New Recordset
    sLinha = ""
  
    CarregarDadosCoberturasRE = True
  
    'titular''''''''''''''''''''''''''''''
    Set rsDadosCoberturas = ObterDadosCoberturas(Val(txtProposta.Text), 0, 0, 0, 0, -1, 0)
  
    If rsDadosCoberturas.EOF Then
        'Call Err.Raise(vbObjectError + 1030, , "CarregarDadosCoberturasRE - N�o foi poss�vel obter as coberturas do titular")
        'FLOW 5073467 - Cleiton Silva - Confitec Sistemas - 06/09/2010
        'quando a vigencia do plano estiver vencida sera consultado no metodo abaixo
        Set rsDadosCoberturas = Nothing
        Set rsDadosCoberturas = ObterDadosCoberturas_Vencidas(Val(txtProposta.Text), 0, 0, 0, 0, -1, 0)

        If rsDadosCoberturas.EOF Then
    
            Call MsgBox("N�o foi poss�vel obter as coberturas do titular", vbCritical)
            rsDadosCoberturas.Close
            Set rsDadosCoberturas = Nothing
            CarregarDadosCoberturasRE = False
            Exit Function
        End If
    End If
  
    gridCoberturasRE.Rows = 1
  
    While Not rsDadosCoberturas.EOF
    
        sLinha = ""
        sLinha = sLinha & rsDadosCoberturas("tp_cobertura_id") & vbTab
        sLinha = sLinha & Trim(rsDadosCoberturas("nome")) & vbTab
        sLinha = sLinha & FormataValorParaPadraoBrasil(rsDadosCoberturas("min_is")) & vbTab
        sLinha = sLinha & Format(rsDadosCoberturas("taxa_net"), "0.0000000") & vbTab
        sLinha = sLinha & Format(rsDadosCoberturas("fat_taxa"), "0.0000000") & vbTab
        sLinha = sLinha & FormataValorParaPadraoBrasil(rsDadosCoberturas("val_premio")) & vbTab
        sLinha = sLinha & FormataValorParaPadraoBrasil(rsDadosCoberturas("fat_desconto_tecnico"))
    
        Call gridCoberturasRE.AddItem(sLinha)
    
        rsDadosCoberturas.MoveNext
    Wend
 
    rsDadosCoberturas.Close
    Set rsDadosCoberturas = Nothing

    Exit Function
TrataErro:
    Call TratarErro("CarregarDadosCoberturasRE", Me.name)
    Call FinalizarAplicacao
End Function

Private Function ObterDadosSubgrupo(lPropostaId As Long, Lseguradora_cod_susep As Long, _
                                    Lsucursal_seguradora_id As Long, Iramo_id As Long, _
                                    LApolice_id As Long, sSubgrupos_flag As String, _
                                    sDtQuery As String) As Recordset



    On Error GoTo TrataErro

    Dim sSql As String
    If sSubgrupos_flag = "A" Then 'alterado 02/09/2013 projeto 17860335 - Melhorias no SEGBR MC_Amaral -confitec
        sSql = ""
        sSql = sSql & " SELECT sa.sub_grupo_id, sa.nome, tp_is, isnull(qtd_salarios, 0) qtd_salarios" & vbNewLine
        sSql = sSql & ", isnull(tp_faturamento,'') tp_faturamento " & vbNewLine
        sSql = sSql & ", isnull(tp_custeio,'') tp_custeio " & vbNewLine
        sSql = sSql & ", isnull(tp_vida,'') tp_vida " & vbNewLine
        sSql = sSql & ", isnull(texto_beneficiario,'') txt_benef " & vbNewLine
        sSql = sSql & ", isnull(qtd_vida, 0) qtd_vida " & vbNewLine
        sSql = sSql & ", isnull(sa.tabua_calculo_id, 0) tabua_calculo_id " & vbNewLine
        sSql = sSql & ", isnull(t.nome, '') nome_tabua " & vbNewLine
        sSql = sSql & ", isnull(fatura_automatica, 'n') fatura_automatica " & vbNewLine
        sSql = sSql & ", isnull(sa.dia_faturamento, 0) dia_faturamento " & vbNewLine
        sSql = sSql & ", isnull(sa.dia_cobranca, 0) dia_cobranca " & vbNewLine
        sSql = sSql & ", isnull(sa.periodo_pgto_id, 99) periodo_pgto_id " & vbNewLine
        sSql = sSql & ", isnull(pp.nome, 'INDEFINIDO') nome_periodo " & vbNewLine
        sSql = sSql & ", isnull(sa.forma_pgto_id, 99) forma_pgto_id " & vbNewLine
        sSql = sSql & ", isnull(fp.nome, 'INDEFINIDO') nome_forma " & vbNewLine
        sSql = sSql & ", isnull(sa.banco_id, 0) banco_id " & vbNewLine
        sSql = sSql & ", isnull(sa.agencia_id, 0) agencia_id " & vbNewLine
        sSql = sSql & ", isnull(ag.nome, '') nome_agencia " & vbNewLine
        sSql = sSql & ", isnull(sa.conta_corrente_id, 0) conta_corrente_id " & vbNewLine
        sSql = sSql & ", isnull(sa.tp_clausula_conjuge, 'n') tp_clausula_conjuge " & vbNewLine
        sSql = sSql & ", isnull(sa.isento_iof, 'n') isento_iof " & vbNewLine
        sSql = sSql & ", isnull(li.lim_min_inclusao, 0) lim_min_inc " & vbNewLine
        sSql = sSql & ", isnull(li.lim_max_inclusao, 0) lim_max_inc " & vbNewLine
        sSql = sSql & ", isnull(li.lim_min_movimentacao, 0) lim_min_mov " & vbNewLine
        sSql = sSql & ", isnull(li.lim_max_movimentacao, 0) lim_max_mov " & vbNewLine
        sSql = sSql & ", isnull(sa.cartao_proposta, '') cartao_proposta " & vbNewLine
        sSql = sSql & ", isnull(ccp.critica_todos, '') critica_todos " & vbNewLine
        sSql = sSql & ", isnull(ccp.critica_idade, '') critica_idade " & vbNewLine
        sSql = sSql & ", isnull(ccp.critica_capital, '') critica_capital " & vbNewLine
        sSql = sSql & ", isnull(ccp.lim_max_idade, 0) lim_max_idade " & vbNewLine
        sSql = sSql & ", isnull(ccp.lim_max_capital, 0) lim_max_capital " & vbNewLine
        sSql = sSql & ", isnull(sa.dt_inicio_sub_grupo, a.dt_inicio_vigencia ) dt_inicio_sub " & vbNewLine
        sSql = sSql & ", isnull(sa.atualizacao_idade, 'N' ) Reenquadramento " & vbNewLine
        sSql = sSql & "  FROM sub_grupo_apolice_tb sa  WITH (NOLOCK) " & vbNewLine
    '    sSql = sSql & " INNER JOIN apolice_tb a   WITH (NOLOCK) " & vbNewLine
    '    sSql = sSql & "   ON  a.seguradora_cod_susep = sa.seguradora_cod_susep " & vbNewLine
    '    sSql = sSql & "   AND a.sucursal_seguradora_id = sa.sucursal_seguradora_id " & vbNewLine
    '    sSql = sSql & "   AND a.ramo_id = sa.ramo_id " & vbNewLine
    '    sSql = sSql & "   AND a.apolice_id = sa.apolice_id " & vbNewLine
    '    sSql = sSql & " INNER JOIN proposta_adesao_tb pa   WITH (NOLOCK)  " & vbNewLine
    '    sSql = sSql & "   ON  pa.seguradora_cod_susep = a.seguradora_cod_susep " & vbNewLine
    '    sSql = sSql & "   AND pa.sucursal_seguradora_id = a.sucursal_seguradora_id " & vbNewLine
    '    sSql = sSql & "   AND pa.ramo_id = a.ramo_id " & vbNewLine
    '    sSql = sSql & "   AND pa.apolice_id = a.apolice_id " & vbNewLine
    '    sSql = sSql & " INNER JOIN proposta_tb p   WITH (NOLOCK)  " & vbNewLine
    '    sSql = sSql & "   ON  p.proposta_id = Sa.proposta_id " & vbNewLine
    '    sSql = sSql & "   AND p.produto_id = " & ProdIdSeguroVidaAliancaIII & vbNewLine
        sSql = sSql & " LEFT JOIN tabua_calculo_tb t   WITH (NOLOCK)  " & vbNewLine
        sSql = sSql & "   ON t.tabua_calculo_id = sa.tabua_calculo_id " & vbNewLine
        sSql = sSql & " LEFT JOIN periodo_pgto_tb pp   WITH (NOLOCK)  " & vbNewLine
        sSql = sSql & "   ON  pp.periodo_pgto_id = sa.periodo_pgto_id " & vbNewLine
        sSql = sSql & " LEFT JOIN forma_pgto_tb fp   WITH (NOLOCK)  " & vbNewLine
        sSql = sSql & "   ON  fp.forma_pgto_id = sa.forma_pgto_id " & vbNewLine
        sSql = sSql & " LEFT JOIN agencia_tb ag   WITH (NOLOCK)  " & vbNewLine
        sSql = sSql & "   ON  ag.banco_id = sa.banco_id " & vbNewLine
        sSql = sSql & "   AND ag.agencia_id = sa.agencia_id " & vbNewLine
        sSql = sSql & " LEFT JOIN limite_idade_sub_grupo_apolice_tb li   WITH (NOLOCK)  " & vbNewLine
        sSql = sSql & "  ON  li.seguradora_cod_susep = sa.seguradora_cod_susep " & vbNewLine
        sSql = sSql & "  AND li.sucursal_seguradora_id = sa.sucursal_seguradora_id " & vbNewLine
        sSql = sSql & "  AND li.ramo_id = sa.ramo_id " & vbNewLine
        sSql = sSql & "  AND li.apolice_id = sa.apolice_id " & vbNewLine
        sSql = sSql & "  AND li.sub_grupo_id = sa.sub_grupo_id " & vbNewLine
        sSql = sSql & "  AND li.dt_inicio_limite <= '" & sDtQuery & "'" & vbNewLine
        sSql = sSql & "  AND (li.dt_fim_limite is null OR li.dt_fim_limite > '" & sDtQuery & "')" & vbNewLine
        sSql = sSql & " LEFT JOIN sub_grupo_criterio_cartao_proposta_tb ccp   WITH (NOLOCK)  " & vbNewLine
        sSql = sSql & "   ON sa.seguradora_cod_susep = ccp.seguradora_cod_susep " & vbNewLine
        sSql = sSql & "  AND sa.sucursal_seguradora_id = ccp.sucursal_seguradora_id " & vbNewLine
        sSql = sSql & "  AND sa.ramo_id = ccp.ramo_id " & vbNewLine
        sSql = sSql & "  AND sa.apolice_id = ccp.apolice_id " & vbNewLine
        sSql = sSql & "  AND sa.sub_grupo_id = ccp.sub_grupo_id " & vbNewLine
        sSql = sSql & "  AND ccp.dt_inicio_criterio = '" & sDtQuery & "'" & vbNewLine
        sSql = sSql & " WHERE pa.proposta_id = " & lPropostaId & vbNewLine
        sSql = sSql & "   AND  Sa.seguradora_cod_susep = " & Lseguradora_cod_susep & vbNewLine
        sSql = sSql & "   AND Sa.sucursal_seguradora_id = " & Lsucursal_seguradora_id & vbNewLine
        sSql = sSql & "   AND Sa.ramo_id = " & Iramo_id & vbNewLine
        sSql = sSql & "   AND Sa.apolice_id = " & LApolice_id & vbNewLine

    'sSql = sSql & " UNION " & vbNewLine 'alterado 02/09/2013 projeto 17860335 - Melhorias no SEGBR MC_Amaral -confitec
  
  Else
  
        sSql = sSql & " SELECT sa.sub_grupo_id, sa.nome, tp_is, isnull(qtd_salarios, 0) qtd_salarios" & vbNewLine
        sSql = sSql & ", isnull(tp_faturamento,'') tp_faturamento " & vbNewLine
        sSql = sSql & ", isnull(tp_custeio,'') tp_custeio " & vbNewLine
        sSql = sSql & ", isnull(tp_vida,'') tp_vida " & vbNewLine
        sSql = sSql & ", isnull(texto_beneficiario,'') txt_benef " & vbNewLine
        sSql = sSql & ", isnull(qtd_vida, 0) qtd_vida " & vbNewLine
        sSql = sSql & ", isnull(sa.tabua_calculo_id, 0) tabua_calculo_id " & vbNewLine
        sSql = sSql & ", isnull(t.nome, '') nome_tabua " & vbNewLine
        sSql = sSql & ", isnull(fatura_automatica, 'n') fatura_automatica " & vbNewLine
        sSql = sSql & ", isnull(sa.dia_faturamento, 0) dia_faturamento " & vbNewLine
        sSql = sSql & ", isnull(sa.dia_cobranca, 0) dia_cobranca " & vbNewLine
        sSql = sSql & ", isnull(sa.periodo_pgto_id, 99) periodo_pgto_id " & vbNewLine
        sSql = sSql & ", isnull(pp.nome, 'INDEFINIDO') nome_periodo " & vbNewLine
        sSql = sSql & ", isnull(sa.forma_pgto_id, 99) forma_pgto_id " & vbNewLine
        sSql = sSql & ", isnull(fp.nome, 'INDEFINIDO') nome_forma " & vbNewLine
        sSql = sSql & ", isnull(sa.banco_id, 0) banco_id " & vbNewLine
        sSql = sSql & ", isnull(sa.agencia_id, 0) agencia_id " & vbNewLine
        sSql = sSql & ", isnull(ag.nome, '') nome_agencia " & vbNewLine
        sSql = sSql & ", isnull(sa.conta_corrente_id, 0) conta_corrente_id " & vbNewLine
        sSql = sSql & ", isnull(sa.tp_clausula_conjuge, 'n') tp_clausula_conjuge " & vbNewLine
        sSql = sSql & ", isnull(sa.isento_iof, 'n') isento_iof " & vbNewLine
        sSql = sSql & ", isnull(li.lim_min_inclusao, 0) lim_min_inc " & vbNewLine
        sSql = sSql & ", isnull(li.lim_max_inclusao, 0) lim_max_inc " & vbNewLine
        sSql = sSql & ", isnull(li.lim_min_movimentacao, 0) lim_min_mov " & vbNewLine
        sSql = sSql & ", isnull(li.lim_max_movimentacao, 0) lim_max_mov " & vbNewLine
        sSql = sSql & ", isnull(sa.cartao_proposta, '') cartao_proposta " & vbNewLine
        sSql = sSql & ", isnull(ccp.critica_todos, '') critica_todos " & vbNewLine
        sSql = sSql & ", isnull(ccp.critica_idade, '') critica_idade " & vbNewLine
        sSql = sSql & ", isnull(ccp.critica_capital, '') critica_capital " & vbNewLine
        sSql = sSql & ", isnull(ccp.lim_max_idade, 0) lim_max_idade " & vbNewLine
        sSql = sSql & ", isnull(ccp.lim_max_capital, 0) lim_max_capital " & vbNewLine
        sSql = sSql & ", isnull(sa.dt_inicio_sub_grupo, a.dt_inicio_vigencia ) dt_inicio_sub " & vbNewLine
        sSql = sSql & ", isnull(sa.atualizacao_idade, 'N' ) Reenquadramento " & vbNewLine
        sSql = sSql & "  FROM sub_grupo_apolice_tb sa   WITH (NOLOCK)  " & vbNewLine
        'INICIO  alterado 02/09/2013 projeto 17860335 - Melhorias no SEGBR MC_Amaral -confitec
    '    sSql = sSql & " INNER JOIN apolice_tb a   WITH (NOLOCK)  " & vbNewLine
    '    sSql = sSql & "   ON  a.seguradora_cod_susep = sa.seguradora_cod_susep " & vbNewLine
    '    sSql = sSql & "   AND a.sucursal_seguradora_id = sa.sucursal_seguradora_id " & vbNewLine
    '    sSql = sSql & "   AND a.ramo_id = sa.ramo_id " & vbNewLine
    '    sSql = sSql & "   AND a.apolice_id = sa.apolice_id " & vbNewLine
    '    sSql = sSql & " INNER JOIN proposta_fechada_tb pf   WITH (NOLOCK)  " & vbNewLine
    '    sSql = sSql & "   ON  pf.proposta_id = a.proposta_id " & vbNewLine
    'FIM  alterado 02/09/2013 projeto 17860335 - Melhorias no SEGBR MC_Amaral -confitec
        sSql = sSql & " LEFT JOIN tabua_calculo_tb t  WITH (NOLOCK)  " & vbNewLine
        sSql = sSql & "   ON t.tabua_calculo_id = sa.tabua_calculo_id " & vbNewLine
        sSql = sSql & " LEFT JOIN periodo_pgto_tb pp   WITH (NOLOCK)  " & vbNewLine
        sSql = sSql & "   ON  pp.periodo_pgto_id = sa.periodo_pgto_id " & vbNewLine
        sSql = sSql & " LEFT JOIN forma_pgto_tb fp   WITH (NOLOCK)  " & vbNewLine
        sSql = sSql & "   ON  fp.forma_pgto_id = sa.forma_pgto_id " & vbNewLine
        sSql = sSql & " LEFT JOIN agencia_tb ag   WITH (NOLOCK)  " & vbNewLine
        sSql = sSql & "   ON  ag.banco_id = sa.banco_id " & vbNewLine
        sSql = sSql & "   AND ag.agencia_id = sa.agencia_id " & vbNewLine
        sSql = sSql & " LEFT JOIN limite_idade_sub_grupo_apolice_tb li   WITH (NOLOCK)  " & vbNewLine
        sSql = sSql & "  ON  li.seguradora_cod_susep = sa.seguradora_cod_susep " & vbNewLine
        sSql = sSql & "  AND li.sucursal_seguradora_id = sa.sucursal_seguradora_id " & vbNewLine
        sSql = sSql & "  AND li.ramo_id = sa.ramo_id " & vbNewLine
        sSql = sSql & "  AND li.apolice_id = sa.apolice_id " & vbNewLine
        sSql = sSql & "  AND li.sub_grupo_id = sa.sub_grupo_id " & vbNewLine
        sSql = sSql & "  AND li.dt_inicio_limite <= '" & sDtQuery & "'" & vbNewLine
        sSql = sSql & "  AND (li.dt_fim_limite is null OR li.dt_fim_limite > '" & sDtQuery & "')" & vbNewLine
        sSql = sSql & " LEFT JOIN sub_grupo_criterio_cartao_proposta_tb ccp   WITH (NOLOCK)  " & vbNewLine
        sSql = sSql & "   ON sa.seguradora_cod_susep = ccp.seguradora_cod_susep " & vbNewLine
        sSql = sSql & "  AND sa.sucursal_seguradora_id = ccp.sucursal_seguradora_id " & vbNewLine
        sSql = sSql & "  AND sa.ramo_id = ccp.ramo_id " & vbNewLine
        sSql = sSql & "  AND sa.apolice_id = ccp.apolice_id " & vbNewLine
        sSql = sSql & "  AND sa.sub_grupo_id = ccp.sub_grupo_id " & vbNewLine
        sSql = sSql & "  AND ccp.dt_inicio_criterio = '" & sDtQuery & "'" & vbNewLine
        sSql = sSql & " WHERE pa.proposta_id = " & lPropostaId & vbNewLine
        'INICIO alterado 02/09/2013 projeto 17860335 - Melhorias no SEGBR MC_Amaral -confitec
        sSql = sSql & "   AND  Sa.seguradora_cod_susep = " & Lseguradora_cod_susep & vbNewLine
        sSql = sSql & "   AND Sa.sucursal_seguradora_id = " & Lsucursal_seguradora_id & vbNewLine
        sSql = sSql & "   AND Sa.ramo_id = " & Iramo_id & vbNewLine
        sSql = sSql & "   AND Sa.apolice_id = " & LApolice_id & vbNewLine
      'FIM alterado 02/09/2013 projeto 17860335 - Melhorias no SEGBR MC_Amaral -confitec
      End If
    Set ObterDadosSubgrupo = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, sSql, True)

    Exit Function
TrataErro:
    Call TratarErro("ObterDadosSubgrupo", Me.name)
    Call FinalizarAplicacao
  
End Function

Private Sub CarregarDadosSubgrupo()
    On Error GoTo TrataErro

    Dim rsSubgrupo As Recordset
    Dim oSubgrupo As SubGrupo
  
    Set rsSubgrupo = New Recordset
' alterado 02/09/2013 projeto 17860335 - Melhorias no SEGBR MC_Amaral -confitec
   ' Set rsSubgrupo = ObterDadosSubgrupo(Val(txtProposta.Text), sSubgrupos_flag, FormatNotByte(txtDtIniVigProposta.Text, "yyyymmdd"))
      Set rsSubgrupo = ObterDadosSubgrupo(Val(txtProposta.Text), Val(txtSeguradora.Text), Val(txtSucursal.Text), txtRamo.Tag, Val(txtApolice.Text), sSubgrupos_flag, Format(txtDtIniVigProposta.Text, "yyyymmdd"))
    While Not rsSubgrupo.EOF
  
        Set oSubgrupo = New SubGrupo
            
        With oSubgrupo
            
            Set .Corretores = New Collection
            Set .Coberturas = New Collection
            Set .Componentes = New Collection
            Set .Estipulantes = New Collection
            Set .Faturas = New Collection
            Set .Assistencias = New Collection
            Set .Vidas_Seguradas = New Collection
                
            .Id = rsSubgrupo!sub_grupo_id
            .Nome = Trim(rsSubgrupo!Nome)
            .TipoIS = UCase(Trim(rsSubgrupo!tp_is))
            .QtdeSal = rsSubgrupo!qtd_salarios
            .TextoBenef = Trim(rsSubgrupo!txt_benef)
            .QtdVidas = rsSubgrupo!qtd_vida
            .IdTabua = rsSubgrupo!tabua_calculo_id
            .NomeTabua = Trim(rsSubgrupo!nome_tabua)
            .FaturaAuto = (UCase(rsSubgrupo!fatura_automatica) = "S")
            .DiaFaturamento = Val(rsSubgrupo!dia_faturamento)
            .DiaCobranca = Val(rsSubgrupo!dia_cobranca)
            .IndPerPgto = rsSubgrupo!periodo_pgto_id
            .PeriodoPgto = Trim(rsSubgrupo!nome_periodo)
            .IndFormaPgto = rsSubgrupo!forma_pgto_id
            .FormaPgto = Trim(rsSubgrupo!nome_forma)
            .IdBanco = rsSubgrupo!banco_id
            .IdAgencia = rsSubgrupo!agencia_id
            .NomeAgencia = Trim(UCase(rsSubgrupo!nome_agencia))
            .CCorrente = rsSubgrupo!conta_corrente_id
            .TpConjuge = UCase(rsSubgrupo!tp_clausula_conjuge)
            .IsentoIOF = (UCase(rsSubgrupo!isento_iof) = "S")
            .LimMinIdadeImpl = Val(rsSubgrupo!lim_min_inc)
            .LimMaxIdadeImpl = Val(rsSubgrupo!lim_max_inc)
            .LimMinIdadeMov = Val(rsSubgrupo!lim_min_mov)
            .LimMaxIdadeMov = Val(rsSubgrupo!lim_max_mov)
            .cartao_proposta = Trim(rsSubgrupo!cartao_proposta)
            .critica_todos = rsSubgrupo!critica_todos
            .critica_idade = rsSubgrupo!critica_idade
            .critica_capital = rsSubgrupo!critica_capital
            .lim_max_idade = rsSubgrupo!lim_max_idade
            .lim_max_capital = Val(rsSubgrupo!lim_max_capital)
            .tp_faturamento = Trim(UCase(rsSubgrupo!tp_faturamento))
            .tp_custeio = Trim(UCase(rsSubgrupo!tp_custeio))
            .tp_vida = Trim(UCase(rsSubgrupo!tp_vida))
            .DtInicioSub = rsSubgrupo!dt_inicio_sub
            .AtualizacaoIdade = rsSubgrupo!Reenquadramento
                
        End With
            
        Call colSubgrupos.Add(oSubgrupo, CStr(rsSubgrupo!sub_grupo_id))
  
        rsSubgrupo.MoveNext
    Wend
  
    Set rsSubgrupo = Nothing

    Exit Sub
TrataErro:
    Call TratarErro("CarregarSubgrupo", Me.name)
    Call FinalizarAplicacao
  
End Sub

Private Sub CarregarDados()
    
'    On Error GoTo ErrMetodo
    
    'p_blNaoExisteErro = CarregarDadosProposta
   
    'Exibicao das Tabs conforme o tipo de ramo
    If p_blNaoExisteErro = True Then
        p_blNaoExisteErro = ConfigurarExibicaoTabs
    End If
    
    'Proponente
    If p_blNaoExisteErro = True Then
        p_blNaoExisteErro = CarregarDadosProponente
    End If
    
    'Dados Financeiros
    If p_blNaoExisteErro = True Then
        'p_blNaoExisteErro = CarregarDadosFinanceiros
    End If
    
    'Objeto de risco
     If p_blNaoExisteErro = True Then
        'p_blNaoExisteErro = CarregarDadosObjetoRisco
    End If
    
    'Objeto de risco RE
    If p_blNaoExisteErro = True Then
        'p_blNaoExisteErro = CarregarDadosObjetoRiscoRE
    End If
    
    'Endosso
    If p_blNaoExisteErro = True Then
        'p_blNaoExisteErro = CarregarDadosEndosso
    End If
    
    'Plantio de Arvore
    If p_blNaoExisteErro = True Then
        p_blNaoExisteErro = CarregarLocalPlantioArvore
    End If
    
                                                                        'Mutu�rios
    'Alterado 30/07/2004 - Eduardo Cintra - Inclusao produto Seguro Vida Prestamista
    If Val(txtProduto.Tag) = ProdIdOurovidaProdutorRural Or Val(txtProduto.Tag) = ProdIdOurovidaRuralRapido Or Val(txtProduto.Tag) = ProdIdBBSeguroVidaAgriculturaFamiliar Then
    
        If p_blNaoExisteErro = True Then
            p_blNaoExisteErro = CarregarDadosMutuario
            fraMutuarios.Visible = True
        End If
    End If
    
    'Beneficiarios
    If p_blNaoExisteErro = True Then
        'p_blNaoExisteErro = CarregarDadosBeneficiarios
    End If
    
    'Sinistro
    If p_blNaoExisteErro = True Then
        'p_blNaoExisteErro = CarregarDadosSinistro
    End If
    
    'Extrato de Seguro
    If p_blNaoExisteErro = True Then
        'p_blNaoExisteErro = CarregarDadosExtratoSeguro
    End If
    
'    If p_blNaoExisteErro Then
'      If Not CarregaAssistenciaFacultativa(Val(txtProposta.Text), Val(txtProduto.Text)) Then Exit Sub
'    End If
    
    If p_blNaoExisteErro Then
        If SSTab1.TabVisible(11) Then
            'Coberturas RE
            p_blNaoExisteErro = CarregarDadosCoberturasRE
        Else

            'Coberturas
            If colSubgrupos.Count > 0 Then
                'j� carregado no momento em que a combo � preenchida
                '          Call CarregarComboComponentes(cboSubgrupo(1).ItemData(cboSubgrupo(1).ListIndex), _
                '                                        Val(txtProposta.Text), _
                '                                        Val(txtApolice.Text), _
                '                                        Val(txtRamo.Tag), _
                '                                        Val(txtSucursal.Tag), _
                '                                        Val(txtSeguradora.Tag), _
                '                                        Format(txtDtIniVigProposta.Text, "yyyymmdd"))
                '
                '          p_blNaoExisteErro = CarregarDadosCoberturas(Val(txtProposta.Text), _
                '                                       Val(txtApolice.Text), _
                '                                       Val(txtRamo.Tag), _
                '                                       Val(txtSeguradora.Text), _
                '                                       Val(txtSucursal.Text), _
                '                                       cboSubgrupo(1).ItemData(cboSubgrupo(1).ListIndex), _
                '                                       cboComponente.ItemData(cboComponente.ListIndex))
            Else
                Call CarregarComboComponentes(-1, Val(txtProposta.Text), Val(txtApolice.Text), Val(txtRamo.Tag), Val(txtSucursal.Text), Val(txtSeguradora.Text), Format(txtDtIniVigProposta.Text, "yyyymmdd"))
                
                p_blNaoExisteErro = CarregarDadosCoberturas(Val(txtProposta.Text), Val(txtApolice.Text), Val(txtRamo.Tag), Val(txtSeguradora.Text), Val(txtSucursal.Text), -1, cboComponente.ItemData(cboComponente.ListIndex))
            End If
        End If
    End If
    
    'Proposta Anterior
    If p_blNaoExisteErro Then
        'p_blNaoExisteErro = CarregarPropostaAnterior
    End If
    
    'GGAMA - 19/06/2005
    'Hist�rico das Propostas
    'If p_blNaoExisteErro = True And v_blHistoricoProposta = True Then
    If p_blNaoExisteErro = True Then
        'p_blNaoExisteErro = CarregarHistoricoProposta
    End If
    
 'Amanda Viana - Confitec - 16/10/2019 - circula 365
    'Carrega dados do numero do contrato op cr�dito
     If Val(txtProduto.Tag) = ProdIdOurovidaProdutorRural Or Val(txtProduto.Tag) = ProdIdBBSeguroVidaAgriculturaFamiliar Then
        p_blNaoExisteErro = CarregarDadosContratoOpCredito(Val(txtProposta.Text))
     End If
    
ExitMetodo:
    Exit Sub
    Resume

ErrMetodo:
    Call TratarErro("CarregarDados", Me.name)
    Call FinalizarAplicacao
End Sub

'vitor.cabral - 11/10/2013 - Nova Consultoria - Demanda 17886578 - Inicio
Private Sub btnExportarExcel_Click()
    
    Dim oGeradorExcel As New clsGeradorExcel
    Dim saveDialog As New SaveFileDialog
    
    saveDialog.Show vbModal
    
    If saveDialog.Confirmacao Then
        oGeradorExcel.NomeDiretorio = saveDialog.SharedFilePath
        oGeradorExcel.NomeArquivo = saveDialog.SharedFileName
        Call oGeradorExcel.Preencher(GridProposta(3))
    End If
    
End Sub
'vitor.cabral - 11/10/2013 - Nova Consultoria - Demanda 17886578 - Fim

Private Sub cboComponente_Click()

    Me.MousePointer = vbHourglass

    If colSubgrupos.Count > 0 Then

        Call CarregarDadosCoberturas(Val(txtProposta.Text), Val(txtApolice.Text), Val(txtRamo.Tag), Val(txtSeguradora.Tag), Val(txtSucursal.Tag), cboSubgrupo(1).ItemData(cboSubgrupo(1).ListIndex), cboComponente.ItemData(cboComponente.ListIndex))
                                   
    Else
  
        Call CarregarDadosCoberturas(Val(txtProposta.Text), Val(txtApolice.Text), Val(txtRamo.Tag), Val(txtSeguradora.Tag), Val(txtSucursal.Tag), -1, cboComponente.ItemData(cboComponente.ListIndex))
    End If
  
    Me.MousePointer = vbDefault
  
End Sub

Private Sub cboSubgrupo_Click(Index As Integer)
  
    Me.MousePointer = vbHourglass
  
    Select Case Index
  
        Case 0 'proposta'''''''''''''''''''''''''''''''''''''''''''''''

            'Corretores '''''''''''
            Call CarregarDadosCorretores(Val(txtProposta.Text), "Null", cboSubgrupo(0).ItemData(cboSubgrupo(0).ListIndex), Format(txtDtIniVigProposta.Text, "yyyymmdd"))
      
            'Estipulantes '''''''''
            Call CarregarDadosEstipulantes(Val(txtProposta.Text), "Null", cboSubgrupo(0).ItemData(cboSubgrupo(0).ListIndex), Format(txtDtIniVigProposta.Text, "yyyymmdd"))
  
        Case 1 'dados financeiros '''''''''''''''''''''''''''''''''''''

            If Not CarregarDadosCapitalPremio Then
                Call MsgBox("N�o foi poss�vel obter dados financeiros", vbCritical)
                Call FinalizarAplicacao
            End If
  
        Case 2 'Vidas Seguradas'''''''''''''''''''''''''''''''''''''''
            Call CarregarDadosVidasSeguradas
  
        Case 3 'Coberturas '''''''''''''''''''''''''''''''''''''''''''
            Call CarregarComboComponentes(cboSubgrupo(1).ItemData(cboSubgrupo(1).ListIndex), Val(txtProposta.Text), Val(txtApolice.Text), Val(txtRamo.Tag), Val(txtSucursal.Tag), Val(txtSeguradora.Tag), Format(txtDtIniVigProposta.Text, "yyyymmdd"))
            'Call CarregarDadosCoberturas(Val(txtProposta.Text), _
             Val(txtApolice.Text), _
             Val(txtRamo.Tag), _
             Val(txtSeguradora.Tag), _
             Val(txtSucursal.Tag), _
             cboSubgrupo(1).ItemData(cboSubgrupo(1).ListIndex), _
             cboComponente.ItemData(cboComponente.ListIndex))
  
    End Select
  
    Me.MousePointer = vbDefault
    
End Sub

Private Sub cmbCongenere_Click()

    If cmbCongenere.Text = "Nossa Parte" And cmbCongenere.ItemData(cmbCongenere.ListIndex) = 0 Then
        Call Carrega_cmbCongenere(CLng(txtProposta.Text), 0, True)
    ElseIf cmbCongenere.Text = "Total do Cosseguro Cedido" And cmbCongenere.ItemData(cmbCongenere.ListIndex) = 0 Then
        Call Carrega_cmbCongenere(CLng(txtProposta.Text), 0, False)
    Else
        Call Carrega_cmbCongenere(CLng(txtProposta.Text), cmbCongenere.ItemData(cmbCongenere.ListIndex), False)
    End If

End Sub

Private Sub cmbSorteio_Click()

    If Val(txtProposta) <> 0 Then
        frmNumeroSorte.idProposta = Val(txtProposta)
        frmNumeroSorte.Show 1
    End If

End Sub

Private Sub cmdAssistencia_Click()

    frmConAssistencia.glPropostaId = Val(txtProposta.Text)
    frmConAssistencia.giProdutoId = Val(txtProduto.Tag)
    frmConAssistencia.gsDtInicioVigencia = Format(txtDtIniVigProposta.Text, "yyyymmdd")
    frmConAssistencia.gsProduto = txtProduto.Text

    frmConAssistencia.Show vbModal

End Sub

'Nova Consultoria - Schoralick - 14/08/2015 - 18699551 - Extrato de Cobran�a
'inicio - sc
Private Sub cmdDadosFinanceiros_Click(Index As Integer)
    On Error GoTo Trata_Erro
    Dim rs As Recordset
    Dim sSql As String
    Dim Id As String
    Dim AryVazio(0) As Variant
    AryVazio(0) = ""
    
    MousePointer = 11
    
    Select Case Index
        Case 0 'cmd Facilitador Sist�mico de C�lculo
            ' Autor: breno.nery (Nova Consultoria) - Data da Altera��o: 13/06/2012
            ' Demanda: 13574539 - Aplicativo C�lculo para Auxiliar A��es Judiciais
            ' Acesso ao aplicativo SEGP1282 - Facilitador Sist�mico de C�lculo
            If VerificaPermissao(gsCPF, "SEGP1282") Then
                ' Autor: pablo.dias (Nova Consultoria) - Data da Altera��o: 04/07/2013
                ' Demanda: 13574539 - Aplicativo C�lculo para Auxiliar A��es Judiciais - Garantia
                ' Corre��o no acesso ao aplicativo Facilitador Sist�mico de C�lculo
                
                'Call ExecutaPrograma("SEGP1282.exe", "SEGP1282.exe", txtProduto.Tag & "," & txtProposta.Text & ",", vbNormalFocus, AryVazio)
                Call Shell("SEGP1282.exe " & txtProduto.Tag & "," & txtProposta.Text & "," & glAmbiente_id, vbNormalFocus)
                ' FIM - pablo.dias - 04/07/2013
            Else
                MsgBox "Esse usu�rio n�o possui acesso ao SEGP1282 - Facilitador Sist�mico de C�lculo", vbExclamation, "Acesso Negado"
            End If
            ' FIM breno.nery (Nova Consultoria) 13/06/2012
        
        Case 1 'cmd Evolu��o
            'Stefanini 24/10/2005 --- FlowBr 72.216
    If VerificaPermissao(gsCPF, "SEGP1134") Then
        If Trim(gsMac) = "" Or Trim(gsMac) = "000000000000" Then
            gsMac = Trim(UCase(AjustaBrEntre(GetMACAddress, 0)))
        End If
        
        Call ExecutaPrograma("SEGP1134.exe", "SEGP1134.exe", "" & txtProduto.Tag & "," & txtProposta.Text & "," & SelPropAdesao.MSFlexGrid1.TextMatrix(SelPropAdesao.MSFlexGrid1.Row, 6), vbNormalFocus, AryVazio)
    Else
        MsgBox "Esse usu�rio n�o possui acesso ao SEGP1134 - Consulta da evolu��o financeira", vbExclamation, "Acesso Negado"
    End If
           
        Case 2 'cmd Extrato de Cobran�a Detalhado
            'PROPOSTA_ID TESTE 5191639
            ExtratoCobrancaDetalhado.CarregaVariaveis (txtProposta.Text)
            ExtratoCobrancaDetalhado.Show vbModal
                    
        Case 3 'cmd Exportar Para Excel
            Dim oGeradorExcel As New clsGeradorExcel
            Call oGeradorExcel.ExportarFlex(MSFlexCobranca, "", "Relat�rio de Cobran�a", ConPropAdesao)

    End Select
    MousePointer = 0
    
    Exit Sub

Trata_Erro:
    MousePointer = 0
    Call TrataErroGeral("cmdDadosFinaceiros_Click", Me.name)
End Sub

Public Function VerificaPermissao(CPF As String, _
                                  Programa As String)
    Dim sSql As String
    Dim rs As ADODB.Recordset
        
    sSql = ""
    sSql = "exec segab_db..valida_permissao_login_rede_sps '" & CPF & "', '" & Programa & "'," & glAmbiente_id
    
    Set rs = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, sSql, True)
                    
    If rs.EOF Then
        VerificaPermissao = False
    Else
        VerificaPermissao = True
    End If

End Function

Private Sub cmdExibirTodos_Click()

    Dim iIndice As Integer

    txtDescricao.Text = ""
    
    If grdHistorico.TextMatrix(grdHistorico.Row, 0) <> "" Then

        For iIndice = 1 To grdHistorico.Rows - 1
            txtDescricao.Text = txtDescricao.Text & sTodasDescricoes(iIndice) & String(110, "-") & vbNewLine & vbNewLine
        Next

    End If
    
End Sub

Private Function CarregarLocalPlantioArvore() As Boolean
    '*****************************************************************************************
    '*****************************************************************************************
    '*********************TRATAMENTO PARA BUSCA DE LOCAL DE PLANTIO***************************
    '*****************************************************************************************
    '*****************************************************************************************
    Dim lProduto As Long
    CarregarLocalPlantioArvore = True
    
    On Error GoTo TrataErro

    Dim SEGL0168 As Object
    Set SEGL0168 = CreateObject("SEGL0168.cls00207")
    lProduto = CLng(Trim(Left(txtProduto.Text, InStr(1, txtProduto.Text, "-") - 1)))
    Dim rs As ADODB.Recordset
    'Localiza o produto de acordo com o brinde devido
    Set rs = SEGL0168.ConsultaProdutoBrinde(gsSIGLASISTEMA, App.Title, App.FileDescription, glAmbiente_id, 7, lProduto)

    If rs.EOF Then
        lblPlantio.Visible = False
        txtLocalPlantio.Visible = False
        rs.Close
        Exit Function
    Else
        lblPlantio.Visible = True
        txtLocalPlantio.Visible = True
        'Procura se tem �rvore plantada para a proposta produto e pega o nome do local de plantio
        Set rs = SEGL0168.ConsultaLocalPlantioProposta(gsSIGLASISTEMA, App.Title, App.FileDescription, glAmbiente_id, CLng(txtProposta.Text))

        If rs.EOF Then
            txtLocalPlantio = ""
            rs.Close
        Else
            txtLocalPlantio = Trim(rs(1)) & " - " & Trim(rs(2))
            rs.Close
        End If
    End If

    Exit Function

TrataErro:
    CarregarLocalPlantioArvore = False
    Call TratarErro("CarregarLocalPlantioArvore", Me.name)
    Call FinalizarAplicacao
Exit Function
Resume
End Function

Private Sub Form_Activate()
    'Definindo exibi��o
    Call DefinirExibicaoObjetos(Val(txtProduto.Tag), Val(txtTpPessoa.Tag))
  
    'FLOW 142242 - Marcelo Ferreira - Confitec - 2006-08-17
    'If Not p_blNaoExisteErro Then Unload Me
  
End Sub

Private Sub Form_Load()
    'criar controle em tempo de execu��o
    Set txtValAssistFacult = Controls.Add("VB.TextBox", "txtValAssistFacult", Frame11(1))
    With txtValAssistFacult
      .Left = 2385
      .Top = 390
      .Width = 1335
      .Height = 285
      .Text = ""
      .Locked = True
      .Enabled = True
      .Visible = True
      .Alignment = 1
      .Text = "0,00"
    End With

    Set lbl = Controls.Add("VB.Label", "lbl", Frame11(1))
    With lbl
      .Left = 2430
      .Top = 180
      .Width = 4000
      .Height = 285
      .Caption = "Valor de Assist�ncia Facultativa"
      .Enabled = True
      .Visible = True
    End With

    'RMarins - Confitec - 13/08/2018 - MU2018-032171 - Criar campo Tempo de Financiamento - Produto 722
    'Amanda Viana - Confitec - 16/10/2019 - circula 365 - Criar campo Tempo de Financiamento - Produto 14
    If SelPropAdesao.MSFlexGrid1.TextMatrix(SelPropAdesao.MSFlexGrid1.RowSel, 1) = ProdIdBBSeguroVidaAgriculturaFamiliar Or SelPropAdesao.MSFlexGrid1.TextMatrix(SelPropAdesao.MSFlexGrid1.RowSel, 1) = ProdIdOurovidaProdutorRural Then
        Set txtPrazoFinanciamento = Controls.Add("VB.TextBox", "txtPrazoFinanciamento", frProposta)
        With txtPrazoFinanciamento
          .Left = 9345
          .Top = 3105
          .Width = 1740
          .Height = 300
          .Text = ""
          .Locked = True
          .Enabled = True
          .Visible = True
          .Alignment = 1
        End With
    
        Set lblPrazoFinanciamento = Controls.Add("VB.Label", "lblPrazoFinanciamento", frProposta)
        With lblPrazoFinanciamento
          .Left = 9360
          .Top = 2880
          .Width = 1710
          .Height = 195
          .Caption = "Prazo de Financiamento"
          .Enabled = True
          .Visible = True
        End With
    End If
    'RMarins - Confitec - 13/08/2018

    Me.chkPPE.Enabled = False
    lbltexto(42).Caption = "N�o"
  
    p_blNaoExisteErro = True
    Renova_vida = False

    'mouse
    'Me.MousePointer = vbHourglass

    'interface
    Call ConfigurarInterface
  
    'obtendo dados da tela anterior
    Call ObterDadosTelaAnterior
    
    ConPropAdesaoALS.OrigemALS = False
  
    'Alterado por Cleber da Stefanini - data: 24/08/2005
    'Altera��o do nome da aba Objeto de Risco RE para Complemento a pedido da Mary
    '---------------------------------------------------
  
    'colecao
    Set colSubgrupos = New Collection
    '  Set colCorretores = New Collection
    '  Set colEstipulantes = New Collection
    Call EsvaziarColecao(colSubgrupos)
  
    Call InicializarVariaveisTab
        
    Call SSTab1_Click(0)
  
    Set txtTp_Documento = Controls.Add("VB.TextBox", "txtTp_Documento", frProposta)
    With txtTp_Documento
      .Left = 240 '1755
      .Top = 3120
      .Width = 1240
      .Height = 300
      .Text = VerificaFluxo()
      .Locked = True
      .Enabled = False
      .Visible = PossuiFluxoDigital
      .Alignment = 0
    End With

    Set lblTp_Documento = Controls.Add("VB.Label", "lblTp_Documento", frProposta)
    With lblTp_Documento
      .Left = 240 '1755
      .Top = 2880
      .Width = 1710
      .Height = 195
      .Caption = "Tipo Documento"
      .Enabled = True
      .Visible = PossuiFluxoDigital
    End With

    'Carregando Dados
    Call CarregarDados

    Call Ler_Cosseguro(Me, txtProposta.Text, txtDtIniVigProposta.Text)
    cmbCongenere.ListIndex = 0

    'Exibir/Ocultar bot�o Facilitador Sist�mico de C�lculo
    Call OcultarBotaoCalculo(txtProduto.Tag)
        
    'Flow 19360491 - Confitec: Claudia.Silva - 22/08/2016
    cmbBotao(2).Visible = frmAbrePDF.VerificaLaudoMedico(Trim(txtProposta), Trim(txtProduto.Tag))
                
    'mouse
    'Me.MousePointer = vbDefault
    DoEvents
    Me.Height = cmbBotao(0).Top + 1300
    
End Sub

'breno.nery - Nova Consultoria - 15/01/2013
'Demanda: 13574539 - Aplicativo de C�lculo para Auxiliar A��es
'M�todo para ocultar o bot�o "Facilitador Sist�mico de C�lculo"

Private Sub OcultarBotaoCalculo(ByVal ProdutoID As Integer)
    Dim i As Integer
    Dim ProdutoCalculo As Boolean
    Dim ProdutosArray(1 To 5) As Integer
    
    ProdutosArray(1) = 12
    ProdutosArray(2) = 121
    ProdutosArray(3) = 135
    ProdutosArray(4) = 136
    ProdutosArray(5) = 716
        
    For i = 1 To 5
        If ProdutoID = ProdutosArray(i) Then: ProdutoCalculo = True: Exit For
    Next i
    
    cmdDadosFinanceiros(0).Visible = ProdutoCalculo
    
End Sub

Private Sub EsvaziarColecao(colColecao As Collection)
 
    While colColecao.Count > 0
        colColecao.Remove (1)
    Wend

End Sub

Private Function ObterDadosEspecificosPenhorRural(lPropostaId As Long) As Recordset

    On Error GoTo TrataErro

    Dim sSql As String
  
    sSql = ""
    sSql = sSql & "   SELECT seguro_penhor_rural_tb.cod_objeto_segurado, " & vbNewLine
    sSql = sSql & "          tp_referencia_tb.tp_referencia_id, " & vbNewLine
    sSql = sSql & "          num_contrato," & vbNewLine
    sSql = sSql & "          prefixo_contrato ," & vbNewLine
    sSql = sSql & "          contrato_dv," & vbNewLine
    sSql = sSql & "          ind_outro_mutuario ," & vbNewLine
    sSql = sSql & "          dt_contrato_seg ," & vbNewLine
    sSql = sSql & "          ISNULL(taxa_referencia,0) taxa_referencia," & vbNewLine
    sSql = sSql & "          ISNULL(taxa_municipal,0) taxa_municipal, " & vbNewLine
    sSql = sSql & "          ISNULL(taxa_intermunicipal,0) taxa_intermunicipal, " & vbNewLine
    sSql = sSql & "          tp_taxa," & vbNewLine
    sSql = sSql & "          grau_operacao," & vbNewLine
    sSql = sSql & "          dt_averbacao, " & vbNewLine
    sSql = sSql & "          dt_baixa, " & vbNewLine
    sSql = sSql & "          tp_referencia_tb.nome, " & vbNewLine
    sSql = sSql & "          val_is, " & vbNewLine
    sSql = sSql & "          val_premio " & vbNewLine
    sSql = sSql & "     FROM seguro_penhor_rural_tb  WITH (NOLOCK)  " & vbNewLine
    sSql = sSql & "     JOIN escolha_tp_cob_generico_tb  WITH (NOLOCK) " & vbNewLine
    sSql = sSql & "       ON escolha_tp_cob_generico_tb.proposta_id=seguro_penhor_rural_tb.proposta_id" & vbNewLine
    sSql = sSql & "      AND escolha_tp_cob_generico_tb.cod_objeto_segurado=seguro_penhor_rural_tb.cod_objeto_segurado" & vbNewLine
    sSql = sSql & "LEFT JOIN tp_referencia_tb  WITH (NOLOCK)  " & vbNewLine
    sSql = sSql & "       ON seguro_penhor_rural_tb.tp_referencia_id=tp_referencia_tb.tp_referencia_id" & vbNewLine
    sSql = sSql & "    WHERE seguro_penhor_rural_tb.proposta_id = " & lPropostaId & vbNewLine
    
    Set ObterDadosEspecificosPenhorRural = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, sSql, True)

    Exit Function

TrataErro:
    Call TratarErro("ObterDadosEspecificosPenhorRural", Me.name)
    Call FinalizarAplicacao
End Function

Private Function ObterDadosEnderecoRiscoPenhorRural(lPropostaId As Long) As Recordset

    On Error GoTo TrataErro

    Dim sSql As String
  
    sSql = ""
    sSql = sSql & " Select a.endereco, "
    sSql = sSql & "        b.nome, "
    sSql = sSql & "        a.estado, "
    sSql = sSql & "        a.CEP, "
    sSql = sSql & "        b.municipio_ibge_id "
    sSql = sSql & "   From endereco_risco_tb a  WITH (NOLOCK)  ,"
    sSql = sSql & "        municipio_tb b  WITH (NOLOCK) "
    sSql = sSql & "  Where a.municipio_id = b.municipio_id "
    sSql = sSql & "    and a.estado = b.estado "
    sSql = sSql & "    and proposta_id = " & lPropostaId
  
    Set ObterDadosEnderecoRiscoPenhorRural = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, sSql, True)

    Exit Function

TrataErro:
    Call TratarErro("ObterDadosEspecificosPenhorRural", Me.name)
    Call FinalizarAplicacao

End Function

Private Function CarregarDadosPenhorRural(lPropostaId As Long) As Boolean

    On Error GoTo TrataErro

    Dim rsPenhorRural As Recordset
    Dim sLinha As String
  
    CarregarDadosPenhorRural = True
  
    Set rsPenhorRural = New Recordset
  
    'Dados da proposta '''''''''''''''''''''''''''''''''''''
    Set rsPenhorRural = ObterDadosEspecificosPenhorRural(lPropostaId)
  
    If rsPenhorRural.EOF Then
        'Call Err.Raise(vbObjectError + 1037, , "CarregarDadosPenhorRural - Dados do Seguro Penhor Rural n�o encontrados")
        Call MsgBox("Dados do Seguro Penhor Rural n�o encontrados", vbCritical)
        rsPenhorRural.Close
        Set rsPenhorRural = Nothing
        CarregarDadosPenhorRural = False
        Exit Function
        'FinalizarAplicacao
        'GoTo TrataErro
    End If
    
    grdPenhorRural.Rows = 1
   
    Do While Not rsPenhorRural.EOF
        sLinha = ""
        sLinha = sLinha & rsPenhorRural!cod_objeto_segurado & vbTab
        sLinha = sLinha & Format(rsPenhorRural!tp_referencia_id, "000") & " - " & Trim("" & rsPenhorRural!Nome) & vbTab
        sLinha = sLinha & rsPenhorRural!num_contrato & vbTab
        sLinha = sLinha & rsPenhorRural!prefixo_contrato & vbTab
        sLinha = sLinha & rsPenhorRural!contrato_dv & vbTab
        sLinha = sLinha & Format(rsPenhorRural!Dt_Contrato_seg, "dd/mm/yyyy") & vbTab
        sLinha = sLinha & Format(rsPenhorRural!dt_averbacao, "dd/mm/yyyy") & vbTab
      
        If IsNull(rsPenhorRural!dt_baixa) Then
            sLinha = sLinha & "" & vbTab
        Else
            sLinha = sLinha & Format(rsPenhorRural!dt_baixa, "dd/mm/yyyy") & vbTab
        End If
      
        sLinha = sLinha & Format(Val(rsPenhorRural!taxa_Referencia), "0.0000000") & vbTab
        sLinha = sLinha & Format(Val(rsPenhorRural!taxa_municipal), "0.0000000") & vbTab
        sLinha = sLinha & Format(Val(rsPenhorRural!taxa_intermunicipal), "0.0000000") & vbTab
      
        Select Case UCase(rsPenhorRural!tp_taxa)

            Case "A"
                sLinha = sLinha & "ANUAL" & vbTab

            Case "D"
                sLinha = sLinha & "DI�RIA" & vbTab

            Case "M"
                sLinha = sLinha & "MENSAL" & vbTab

            Case Else
                sLinha = sLinha & "" & vbTab
        End Select
      
        sLinha = sLinha & rsPenhorRural!grau_operacao & vbTab
      
        If UCase(rsPenhorRural!ind_outro_mutuario) = "S" Then
            sLinha = sLinha & "Sim" & vbTab
        ElseIf UCase(rsPenhorRural!ind_outro_mutuario) = "N" Then
            sLinha = sLinha & "N�o" & vbTab
        Else
            sLinha = sLinha & "" & vbTab
        End If
      
        sLinha = sLinha & FormataValorParaPadraoBrasil(rsPenhorRural!val_is) & vbTab
        sLinha = sLinha & FormataValorParaPadraoBrasil(rsPenhorRural!val_premio)
      
        grdPenhorRural.AddItem sLinha
      
        rsPenhorRural.MoveNext
      
    Loop

    'Endereco de Risco '''''''''''''''''''''''''''''''''''''''''''''''''''''''
    rsPenhorRural.Close
    Set rsPenhorRural = ObterDadosEnderecoRiscoPenhorRural(lPropostaId)
  
    If Not rsPenhorRural.EOF Then
        txtEndRiscoPenhorRural.Text = rsPenhorRural!Endereco
        txtMunRiscoPenhorRural.Text = IIf(IsNull(rsPenhorRural!Nome) = False, rsPenhorRural!Nome, "")
        txtUFRiscoPenhorRural.Text = rsPenhorRural!estado
        txtCEPRiscoPenhorRural.Text = IIf(IsNull(rsPenhorRural!Cep) = False, rsPenhorRural!Cep, "")
        txtMunIBGEPenhorRural.Text = IIf(IsNull(rsPenhorRural!municipio_ibge_id) = False, rsPenhorRural!municipio_ibge_id, "")
    Else
        txtEndRiscoPenhorRural.Text = ""
        txtMunRiscoPenhorRural.Text = ""
        txtUFRiscoPenhorRural.Text = ""
        txtCEPRiscoPenhorRural.Text = ""
        txtMunIBGEPenhorRural.Text = ""
    End If

    rsPenhorRural.Close
    Set rsPenhorRural = Nothing

    Exit Function
TrataErro:
    Call TratarErro("CarregarDadosPenhorRural", Me.name)
    Call FinalizarAplicacao
End Function

Private Function ObterDadosAvaliacao(lPropostaId As Long) As Recordset
    On Error GoTo TrataErro

    Dim sSql As String
  
'    sSql = ""
'    sSql = sSql & "    SELECT a.dt_avaliacao,                        " & vbNewLine
'    sSql = sSql & "           b.nome,                                " & vbNewLine
'    sSql = sSql & "           a.parecer,                             " & vbNewLine
'    sSql = sSql & "           a.usuario,                             " & vbNewLine
'    sSql = sSql & "           a.cid10_grupo_id,                      " & vbNewLine
'    sSql = sSql & "           a.cod_cid10_item,                      " & vbNewLine
'    sSql = sSql & "           b.tp_avaliacao_id,                     " & vbNewLine
'    sSql = sSql & "           b.origem,                              " & vbNewLine
'
'    'JOSE MOREIRA - NOVA CONSULTORIA - 14620257 - Melhorias no processo de subscri��o
'    sSql = sSql & " CASE a.tp_componente_id WHEN 1 THEN 'Titular' WHEN 3 THEN 'C�njuge' ELSE '' END AS 'Critica' " & vbNewLine
'
'    sSql = sSql & "    FROM avaliacao_proposta_tb a  WITH (NOLOCK)                 " & vbNewLine
'    sSql = sSql & "INNER JOIN als_produto_db..tp_avaliacao_tb b   WITH (NOLOCK)                     " & vbNewLine
'    sSql = sSql & "        ON a.tp_avaliacao_id = b.tp_avaliacao_id  " & vbNewLine
'    sSql = sSql & "     WHERE proposta_id = " & lPropostaId & vbNewLine
'    sSql = sSql & "  ORDER BY num_avaliacao                          " & vbNewLine

    sSql = ""
    sSql = sSql & " SET NOCOUNT ON EXEC seguros_db.dbo.SEGS13095_SPS " & lPropostaId
    
    Set ObterDadosAvaliacao = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, sSql, True)
    
    Exit Function

TrataErro:
    Call TratarErro("ObterDadosAvaliacao", Me.name)
    Call FinalizarAplicacao
  
End Function

Private Function ObterDadosAvaliacaoCancelamento(lPropostaId As Long) As Recordset
    On Error GoTo TrataErro

    Dim sSql As String
  
    sSql = ""
    sSql = sSql & "SELECT isnull(dt_cancelamento_bb,dt_inicio_cancelamento), " & vbNewLine
    sSql = sSql & "       motivo_cancelamento, " & vbNewLine
    sSql = sSql & "       usuario " & vbNewLine
    sSql = sSql & "  FROM cancelamento_proposta_tb  WITH (NOLOCK)  " & vbNewLine
    sSql = sSql & " WHERE proposta_id = " & lPropostaId & vbNewLine
    'lrocha 23/01/2007 tratamento para n�o pegar data de in�cio maior que data fim
    sSql = sSql & " AND ((dt_inicio_cancelamento < dt_fim_cancelamento) " & vbNewLine
    sSql = sSql & " OR (dt_fim_cancelamento IS NULL)) " & vbNewLine
    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    sSql = sSql & "ORDER BY isnull(dt_cancelamento_bb,dt_inicio_cancelamento)" & vbNewLine
    
    Set ObterDadosAvaliacaoCancelamento = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, sSql, True)
    
    Exit Function

TrataErro:
    Call TratarErro("ObterDadosAvaliacaoCancelamento", Me.name)
    Call FinalizarAplicacao
  
End Function

Private Function ObterDadosBeneficiarios(lPropostaId) As Recordset
    On Error GoTo TrataErro

    Dim sSql As String
  
    'Rafael Oshiro - 26/10/04
    'Fazer UNION das tabelas texto_beneficiario_tb e beneficiario_tb
  
    '  sSQL = ""
    '  sSQL = sSQL & "SELECT descricao "
    '  sSQL = sSQL & "  FROM texto_beneficiario_tb  WITH (NOLOCK) "
    '  sSQL = sSQL & " WHERE proposta_id = " & lPropostaId

    '    sSQL = sSQL & "SELECT CONVERT(VARCHAR,descricao), 0 as seq_beneficiario "
    '    Stefanini (Maur�cio), em 16/05/2005 - setar tamanho do m�ximo do tipo text no varchar,
    '    pois TEXT � o tipo do campo descri��o nesta tabela (para n�o perder valores do campo)
    sSql = sSql & "SELECT CONVERT(VARCHAR(7000),descricao), 0 as seq_beneficiario "
    sSql = sSql & "  FROM texto_beneficiario_tb  WITH (NOLOCK)  "
    sSql = sSql & " WHERE proposta_id = " & lPropostaId

    sSql = sSql & " UNION "

    sSql = sSql & "SELECT CONVERT(VARCHAR(60),nome) + ' - ' + REPLACE(CONVERT(VARCHAR,convert(numeric(7,2),perc_participacao)),'.',',') + '%', seq_beneficiario "
    sSql = sSql & "  FROM beneficiario_tb  WITH (NOLOCK)  "
    sSql = sSql & " WHERE proposta_id = " & lPropostaId
    sSql = sSql & "   AND dt_fim_vigencia IS NULL " 'Rafael Oshiro 03/11/04
    
    'MYOSHIMURA
    'ALTERA��O PARA ORDENAR POR ORDEM DE INCLUS�O
    '14/12/2004
    sSql = sSql & "   order by seq_beneficiario"

    Set ObterDadosBeneficiarios = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, sSql, True)

    Exit Function

TrataErro:
    Call TratarErro("ObterDadosBeneficiarios", Me.name)
    Call FinalizarAplicacao
  
End Function

Private Function ObterDadosExtratoSeguro(lPropostaId As Long) As Recordset
    On Error GoTo TrataErro

    Dim sSql  As String
  
    sSql = "EXEC evento_seguros_db..evento_sps " & lPropostaId
  
    Set ObterDadosExtratoSeguro = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, sSql, True)

    Exit Function
TrataErro:
    Call TratarErro("ObterDadosExtratoSeguro", Me.name)
    Call FinalizarAplicacao
End Function

Private Function ObterSituacaoProposta(lPropostaId As Long) As Recordset
    On Error GoTo TrataErro

    Dim sSql  As String
  
    sSql = "select situacao from proposta_tb where proposta_id = " & lPropostaId
  
    Set ObterSituacaoProposta = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, sSql, True)

    Exit Function
TrataErro:
    Call TratarErro("ObterSituacaoProposta", Me.name)
    Call FinalizarAplicacao
End Function

Function TrocaValorAmePorBras(ByVal Variavel As Variant) As Variant
    '
    ' Respons�vel : Carlos Rosa
    ' Dt Desenv : 27/04/98
    '
    ' Recebe um valor qualquer
    ' Retorna :
    ' Valor sem v�rgulas, em letras mai�sculas e sem espa�os vazios
    '
    ' troca "," por "V"
    '
    While InStr(Variavel, ",") <> 0
        Variavel = Mid(Variavel, 1, InStr(Variavel, ",") - 1) + "V" + Mid(Variavel, InStr(Variavel, ",") + 1)
    Wend
    '
    ' troca "." por ","
    '
    While InStr(Variavel, ".") <> 0
        Variavel = Mid(Variavel, 1, InStr(Variavel, ".") - 1) + "," + Mid(Variavel, InStr(Variavel, ".") + 1)
    Wend
    '
    ' troca "V" por "."
    '
    While InStr(Variavel, "V") <> 0
        Variavel = Mid(Variavel, 1, InStr(Variavel, "V") - 1) + "." + Mid(Variavel, InStr(Variavel, "V") + 1)
    Wend
    TrocaValorAmePorBras = UCase(Trim(Variavel))
    
    ' troca " " por "."
    '
    While InStr(Variavel, " ") <> 0
        Variavel = Mid(Variavel, 1, InStr(Variavel, " ") - 1) + "." + Mid(Variavel, InStr(Variavel, " ") + 1)
    Wend
End Function

Function Formata(ByVal Variavel As Variant) As Variant
    '
    ' Respons�vel : Marcos esteves
    ' Dt Desenv : 27/04/98
    '
    ' Recebe um valor qualquer
    ' Retorna :
    ' Valor sem v�rgulas, em letras mai�sculas e sem espa�os vazios
   
    Variavel = Trim(Variavel)
    
    ' troca " " por "."
    '
    While InStr(Variavel, " ") <> 0
        Variavel = Mid(Variavel, 1, InStr(Variavel, " ") - 1) + "." + Mid(Variavel, InStr(Variavel, " ") + 1)
    Wend
    
    Variavel = Mid(Variavel, 1, Len(Variavel) - 3) & "," & Right(Variavel, 2)
    
    Formata = Variavel
    
End Function

Function LeArquivoIni(ByVal FileName As String, ByVal SectionName As String, ByVal Item As String) As String
    
    Dim Retorno As String * 100
    Dim RetornoDefault As String
    Dim nc As Long
     
    RetornoDefault = "*"
    nc = GetPrivateProfileString(SectionName, Item, RetornoDefault, Retorno, Len(Retorno), FileName)
    LeArquivoIni = Left$(Retorno, nc)

End Function

Private Sub Form_QueryUnload(Cancel As Integer, _
                             UnloadMode As Integer)
    
    SelPropAdesao.txtArgumento = ""
    SelPropAdesao.Show
    Unload Me
    
End Sub

Private Sub ChkIndApta_Click()

    If ChkIndApta.Value = 1 Then
        ChkIndApta.Value = 0
    End If

End Sub

Private Sub chkIndLaudo_Click()

    If chkIndLaudo.Value = 1 Then
        chkIndLaudo.Value = 0
    End If

End Sub

Private Sub grdAvaliacao_SelChange()

    With grdAvaliacao
        txtDados(0).Text = .TextMatrix(.RowSel, 0)  'Arquivo
        txtDados(1).Text = .TextMatrix(.RowSel, 3)  'Data importa��o
        txtDados(2).Text = .TextMatrix(.RowSel, 1)  'PropostaBB
        txtDados(3).Text = .TextMatrix(.RowSel, 6)  'ProdutoBB
        txtDados(4).Text = .TextMatrix(.RowSel, 9)  'Descri��o ProdutoBB
        txtDados(5).Text = .TextMatrix(.RowSel, 10)  'Nosso Numero
        txtDados(6).Text = .TextMatrix(.RowSel, 11) 'DV Nosso Numero
        txtDados(7).Text = .TextMatrix(.RowSel, 16) ' Convenio
        txtDados(8).Text = .TextMatrix(.RowSel, 2) 'Endosso
        txtDados(9).Text = .TextMatrix(.RowSel, 12) 'Agencia
        txtDados(10).Text = .TextMatrix(.RowSel, 13) 'Conta
        txtDados(12).Text = .TextMatrix(.RowSel, 4) 'Cod Retorno
        txtDados(13).Text = .TextMatrix(.RowSel, 5) 'Descri��o
        txtDados(14).Text = .TextMatrix(.RowSel, 15) 'Parcelas
        txtDados(15).Text = .TextMatrix(.RowSel, 17) 'Valor parcelas
        txtDados(16).Text = .TextMatrix(.RowSel, 18) 'Data parcelas
    End With

End Sub

'Private Sub ChkIndLavouraPlant_Click()
'Comentado por Stefanini Consultoria (Maur�cio), em 20/10/2004, pois quando o form � carregado e o checkbox �
'marcado devido ao recordset do indicador de Planta��o de Lavoura estar com o valor "s", esta fun��o
'desmarca ao carregar, fazendo com que o formul�rio sempre seja carregado com o objeto desmarcado
'If ChkIndLavouraPlant.Value = 1 Then
'    ChkIndLavouraPlant.Value = 0
'End If

'End Sub

Private Sub grdEndosso_DblClick()

    'If grdEndosso.Rows = 1 Then Exit Sub
     
    MousePointer = vbHourglass
    
    Dim rsEndosso As Recordset
    Dim rsContratacao As Recordset
    
    Set rsEndosso = New Recordset
    Set rsEndosso = ObterDadosEndosso(Val(txtProposta.Text))
    Set rsContratacao = ObterDadosProposta(Val(txtProposta.Text))
    
    If rsContratacao("produto_id") = 1225 Then
        frmConsultaEndosso.Show
    Else
        frmEndosso.Show
    End If
          
    Me.Hide
    
    MousePointer = vbDefault
End Sub

Private Sub grdHistorico_Click()

    If grdHistorico.TextMatrix(grdHistorico.Row, 0) <> "" Then
        grdHistorico.HighLight = flexHighlightAlways
        txtDescricao.Text = sTodasDescricoes(grdHistorico.TextMatrix(grdHistorico.Row, 0))
    End If

End Sub

Private Sub grdHistorico_SelChange()

    If grdHistorico.Row <> grdHistorico.RowSel Then
        grdHistorico.RowSel = grdHistorico.Row
    End If

End Sub

Private Sub GridPropsta_Click()
    On Error GoTo ErrMetodo
    
    If Trim$(GridProposta(1).TextMatrix(GridProposta(1).Row, GridProposta(1).Col)) = "" Then
        GridProposta(1).ToolTipText = ""
        GoTo ExitMetodo
    End If
    
    GridProposta(1).ToolTipText = GridProposta(1).TextMatrix(GridProposta(1).Row, GridProposta(1).Col)

ExitMetodo:
    Exit Sub

ErrMetodo:
    MsgBox Err.Number & " " & Err.Description

End Sub

Private Sub GridProposta_Click(Index As Integer)
    If Index = 2 Then
        If GridProposta(2).Rows > 1 Then
            fmrExtratoRestituicao.Show vbModal
        End If
    End If
End Sub

Private Sub lvwDeclaracaoTitular_ItemCheck(ByVal Item As MSComctlLib.ListItem)
    Item.Checked = Not Item.Checked
End Sub

Private Sub lvwDeclaracaoConjuge_ItemCheck(ByVal Item As MSComctlLib.ListItem)
    Item.Checked = Not Item.Checked
End Sub

Private Sub MSFlexCobranca_Click()

    If Val(txtProduto.Tag) = ProdIdOurovidaEmpresa Or bProdutoBESC = True Then     'flowbr3658050
    
        'ExibirBoleto txtProposta.Text, MSFlexCobranca.TextMatrix(MSFlexCobranca.Row, 5)
        ExibirBoleto txtProposta.Text, MSFlexCobranca.TextMatrix(MSFlexCobranca.Row, 6) 'INC000004388853 - 26/09/2014 - Philip Rocha - Altera��o da coluna ao buscar o valor do campo "num_cobranca" na grid.
    Else
        Call MsgBox("A exibi��o de boletas s� esta dispon�vel para o produto Ouro Vida Empresa.")
    End If

End Sub

Private Sub ExibirBoleto(ByVal sProposta As String, _
                         ByVal sCobranca As String)

    On Error GoTo TrataErro

    Dim Resultado
    Dim sCaminho As String
    Dim RsBoleto As Recordset
    Dim sSql As String
    Dim sBancoDados As String
  
    'Verficando a situa��o do agendamento, o boleto n�o pode ser impresso caso o agendamento
    'esteja pago ou cancelado'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  
    sSql = "SELECT situacao, canc_endosso_id cancelado, db_name() banco "
    sSql = sSql & "FROM agendamento_cobranca_tb "
    sSql = sSql & "WHERE proposta_id = " & sProposta
    sSql = sSql & "  AND num_cobranca = " & sCobranca
  
    Set RsBoleto = New Recordset
    Set RsBoleto = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, sSql, True)

    If LCase(RsBoleto("situacao")) <> "p" Then
        MsgBox "Esta cobran�a n�o est� pendente de retorno. A boleta n�o ser� exibida."
        GoTo SAI
    End If
  
    If Not IsNull(RsBoleto("cancelado")) Then
        MsgBox "Esta cobran�a foi cancelada. A Boleta n�o ser� exibida."
        GoTo SAI
    End If
    
  ' Inicio - Demanda - 19368999 - Cobran�a registrada AB e ABS - MArcio.Nogueira
    ' Busca se o flag do convenio do boleto esta como registra boleto = sim
    If Retorna_convenio_registra_boleto(Mid(Trim(MSFlexCobranca.TextMatrix(MSFlexCobranca.Row, 10)), 1, 7)) = "S" Then
     ' Inicio Registra o Boleto no Webservice: Demanda - 19368999 - Cobran�a registrada AB e ABS - MArcio.Nogueira
            sSql = ""
            sSql = "select count (1) as qtd "
            sSql = sSql & " from seguros_db.dbo.agendamento_cobranca_atual_tb with(nolock)"
            sSql = sSql & " Where proposta_id = " & Trim(sProposta) & ""
            sSql = sSql & " and num_cobranca = " & Trim(sCobranca) & ""
            sSql = sSql & " and isnull(fl_boleto_registrado,'N') <> 'S'"
            
            Set RsBoleto = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, sSql, True)
            
            'Se data for alterada Registra o Boleto
            If RsBoleto("qtd") > 0 Then '- Marcio.nogueira - COnfitec/rj - 20/07/2017 - Demanda - 19368999 - Registro online convenios BB - MU-2017-027161
                 Call ProcessaRegistraBoletosBB(sProposta, sCobranca) '-- Marcio.nogueira - COnfitec/rj - 20/07/2017 - Demanda - 19368999 - Registro online convenios BB - MU-2017-027161
                If BlnBoletoRegistrado = False Then
                   Exit Sub
                Else
                   'R.FOUREAUX - 31/03/2017
                    sSql = ""
                    sSql = " EXEC seguros_db.dbo.SEGS13341_SPU " & vbNewLine
                    sSql = sSql & " @proposta_id = " & Trim(sProposta) & "" & vbNewLine
                    sSql = sSql & ", @num_cobranca = " & Trim(sCobranca) & "" & vbNewLine
                    sSql = sSql & ", @fl_boleto_registrado = 'S' "
                    Call ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, sSql, False)

                End If
            End If
     End If
  
    'Carrengando p�gina ASP com a exibi��o da boleta'''''''''''''''''''''''''''''''''''''''''
  
    sBancoDados = LCase(RsBoleto("banco"))
    sCaminho = "C:\Arquivos de programas\Internet explorer\iexplore.exe -nohome "
    'Jessica.Adao (Inicio) - Confitec Sistemas - 20170601 - Projeto 19368999 : Cobranca Registrada
    If glAmbiente_id = 2 Or glAmbiente_id = 6 Then
        sCaminho = sCaminho & "http://www.aliancadobrasil.com.br/internet/serv/boleto/impressao/"
    Else
        sCaminho = sCaminho & "https://qld.aliancadobrasil.com.br/internet/serv/boleto/impressao/"
    End If
    'Jessica.Adao (Fim) - Confitec Sistemas - 20170601 - Projeto 19368999 : Cobranca Registrada
  
    If sBancoDados = "seguros_db" Then
        sBancoDados = ""
    ElseIf sBancoDados = "seguros_qld_db" Then
        sBancoDados = "s"
    Else
        sBancoDados = "d"
    End If
  
    sCaminho = sCaminho & "boleto" & sBancoDados & ".asp"
    sCaminho = sCaminho & "?proposta=" & Trim(sProposta)
    sCaminho = sCaminho & "&cobranca=" & Trim(sCobranca)
    Resultado = Shell(sCaminho, vbMaximizedFocus)

SAI:
    RsBoleto.Close
    Set RsBoleto = Nothing

    Exit Sub
  
TrataErro:
    Call TratarErro("ExibirBoleto", Me.name)
    Call FinalizarAplicacao

End Sub

Private Sub configurarGridMutuario()

    With grdMutuario
    
        .Clear
        .Rows = 2
        .FixedCols = 0
        .Cols = 19
        
        .TextMatrix(0, 0) = "Nome"
        Call .TamanhoColuna(0, 4000)
        
        .TextMatrix(0, 1) = "CPF"
        Call .TamanhoColuna(1, 2000)
            
        .TextMatrix(0, 2) = "Sexo"
        Call .TamanhoColuna(2, 1200)
        
        .TextMatrix(0, 3) = "Dt. Nascimento"
        Call .TamanhoColuna(3, 1500)
        
        .TextMatrix(0, 4) = "Endereco"
        Call .TamanhoColuna(4, 4000)

        .TextMatrix(0, 5) = "Bairro"
        Call .TamanhoColuna(5, 2000)

        .TextMatrix(0, 6) = "Municipio"
        Call .TamanhoColuna(6, 2000)

        .TextMatrix(0, 7) = "Estado"
        Call .TamanhoColuna(7, 700)

        .TextMatrix(0, 8) = "CEP"
        Call .TamanhoColuna(8, 1200)

        .TextMatrix(0, 9) = "DDD"
        Call .TamanhoColuna(9, 700)

        .TextMatrix(0, 10) = "Telefone"
        Call .TamanhoColuna(10, 1000)

        .TextMatrix(0, 11) = "Doc Identifica��o"
        Call .TamanhoColuna(11, 2000)
        
        .TextMatrix(0, 12) = "Tipo"
        Call .TamanhoColuna(12, 1500)
        
        .TextMatrix(0, 13) = "Org�o Exp."
        Call .TamanhoColuna(13, 1200)
        
        .TextMatrix(0, 14) = "Dt. Expedi��o"
        Call .TamanhoColuna(14, 1200)
        
        .TextMatrix(0, 15) = "Capital Segurado"
        Call .TamanhoColuna(15, 1500)
        
        .TextMatrix(0, 16) = "Pr�mio Bruto"
        Call .TamanhoColuna(16, 1500)

        .TextMatrix(0, 17) = "C�digo MCI"
        Call .TamanhoColuna(17, 1500)
        
        'amanda - Mutu�rio
       .TextMatrix(0, 18) = "Participa��o Mutu�rio"
        Call .TamanhoColuna(18, 2000)

    End With

End Sub

Private Sub CertificadoPropostaRenovada(lPropostaId As Long)

    On Error GoTo TrataErro

    Dim sSql As String
    Dim rsRenovada As Recordset
    Dim rsRenovada2 As Recordset
  
    sSql = ""
    sSql = sSql & "SELECT tp_endosso_id" & vbNewLine
    sSql = sSql & " FROM endosso_tb  WITH (NOLOCK) " & vbNewLine
    sSql = sSql & " WHERE proposta_id =" & lPropostaId
    sSql = sSql & " AND tp_endosso_id = 250" & vbNewLine ' 250 - endosso de Renova��o
  
    Set rsRenovada = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, sSql, True)
        
    If Not rsRenovada.EOF Then
        
        Renova_vida = True
            
        sSql = ""
        sSql = sSql & "SELECT certificado_id" & vbNewLine
        sSql = sSql & "FROM proposta_tb  WITH (NOLOCK) " & vbNewLine
        sSql = sSql & "INNER JOIN certificado_tb  WITH (NOLOCK) " & vbNewLine
        sSql = sSql & "ON proposta_tb.proposta_id=certificado_tb.proposta_id" & vbNewLine
        sSql = sSql & "WHERE proposta_tb.proposta_id=" & lPropostaId

        Set rsRenovada2 = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, sSql, True)

        If Not rsRenovada2.EOF Then
            TxtCertificado.Text = rsRenovada2("certificado_id")
            rsRenovada2.Close
            Set rsRenovada2 = Nothing
        Else
            TxtCertificado.Text = 0
        End If

    Else
        TxtCertificado.Text = 0
    End If

    rsRenovada.Close
    Set rsRenovada = Nothing
  
    Exit Sub

TrataErro:
    Call TratarErro("CertificadoPropostaRenovada", Me.name)
    Call FinalizarAplicacao
End Sub

'GGAMA - 19/06/2005
'Hist�rico das Propostas
Private Function CarregarHistoricoProposta() As Boolean

    Dim oHistorico As Object
    Dim rsHistorico As Recordset
    Dim lCont As Long
    
    On Error GoTo TrataErro
    
    CarregarHistoricoProposta = True
    
    grdHistorico.Rows = 2
    
    lCont = 1

    Set oHistorico = CreateObject("SEGL0026.cls00406")
    
    'glAmbiente_id = 2
    
    Set rsHistorico = oHistorico.ConsultarComentarioProposta(gsSIGLASISTEMA, App.Title, App.FileDescription, glAmbiente_id, 3, txtProposta.Text, cUserName, 0, 0, 0, False, , True)
    
    If Not rsHistorico.EOF Then
    
        grdHistorico.Rows = 1
        
        While Not rsHistorico.EOF
        
            grdHistorico.AddItem lCont & vbTab & rsHistorico("assunto") & vbTab & rsHistorico("comentario_id") & vbTab & Format(rsHistorico("dt_inclusao"), "dd/mm/yyyy") & vbTab
                                 
            Call MontarTodasDescricoes(rsHistorico, lCont)
        
            rsHistorico.MoveNext
            
            lCont = lCont + 1
        
        Wend

    Else
    
        grdHistorico.HighLight = flexHighlightNever
    
    End If
    
    Set oHistorico = Nothing
    Set rsHistorico = Nothing
    
    Exit Function
    
TrataErro:
    CarregarHistoricoProposta = False
    Call TratarErro("CarregarHistoricoProposta", Me.name)
    Call FinalizarAplicacao
    
End Function

'GGAMA - 07/10/2005
'Hist�rico das Propostas
Private Sub MontarTodasDescricoes(ByRef rsDescricao As Recordset, _
                                  ByVal lNumComentario As Long)

    Dim sDescricao As String

    On Error GoTo TrataErro
    
    'Cabe�alho da descricao
    sDescricao = ""
    
    sDescricao = sDescricao & "Coment�rio: " & lNumComentario & vbNewLine & vbNewLine
    
    sDescricao = sDescricao & "Assunto: " & rsDescricao("assunto") & vbNewLine & vbNewLine
    
    sDescricao = sDescricao & "Data de Inclus�o: " & Format(rsDescricao("dt_inclusao"), "dd/mm/yyyy") & vbNewLine & vbNewLine
    
    sDescricao = sDescricao & "Usu�rio de Inclus�o: " & rsDescricao("nome") & vbNewLine & vbNewLine
    
    If LCase(rsDescricao("restrito")) = "s" Then
        sDescricao = sDescricao & "Restri��o: Restrito" & vbNewLine & vbNewLine
    Else
        sDescricao = sDescricao & "Restri��o: N�o restrito" & vbNewLine & vbNewLine
    End If
        
    'Descri��o
    sDescricao = sDescricao & "Descri��o:" & vbNewLine & vbNewLine
        
    If UCase(Trim(rsDescricao("restrito"))) = "S" And UCase(Trim(rsDescricao("usuario_unidade_permitida"))) = "N" Then
        sDescricao = sDescricao & "Coment�rio restrito" & vbNewLine & vbNewLine
    Else
        sDescricao = sDescricao & rsDescricao("descricao") & vbNewLine & vbNewLine
    End If
    
    ReDim Preserve sTodasDescricoes(lNumComentario + 1) As String
    
    sTodasDescricoes(lNumComentario) = sDescricao
    
    Exit Sub
    
TrataErro:
    Call TratarErro("MontarTodasDescricoes", Me.name)
    Call FinalizarAplicacao

End Sub

Private Function IdentificaPPE(strCPF As String) As Boolean

    'Sessao de variaveis
    Dim strSql      As String
    Dim rc          As Recordset

    'Manipulador de erros
    On Error GoTo ErrIdentificaPPE

    'Valor Padrao
    IdentificaPPE = False

    'Retira Formacao caso exista
    strCPF = Replace(strCPF, ".", "")
    strCPF = Replace(strCPF, "-", "")

    'Busca Identificar se faz parte do PPE
    strSql = "SELECT COUNT(*) AS TOTAL "
    strSql = strSql & " FROM seguros_db..PPE_ORGAO_CARGO_TB as TPPE "
    strSql = strSql & " WHERE CAST(TPPE.CPF AS BIGINT) = CAST('" & strCPF & "' AS BIGINT)"
    strSql = strSql & " AND (TPPE.DT_EXCLUSAO_PPE IS NULL OR  TPPE.DT_EXCLUSAO_PPE >= GETDATE())"

    'Executa o Comando
    Set rc = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, strSql, True)
    
    'Verifica se voltou registro
    If rc.EOF Then GoTo Finaliza

    'Verifica Valor Valido
    If IsNull(rc!Total) Then GoTo Finaliza

    'vERIFICA nao ENCONTROU
    If CInt(rc!Total) <= 0 Then GoTo Finaliza
    
    'Valor Padrao
    IdentificaPPE = True

    'Finaliza
Finaliza:
    rc.Close
    Set rc = Nothing

    'Sai
    Exit Function

    'Manipulador de erros
ErrIdentificaPPE:

    'Fecha Recordsrt
    rc.Close
    Set rc = Nothing

End Function

Private Sub SSTab1_Click(PreviousTab As Integer)
    On Error GoTo ErrMetodo
    
    With sTabSelecionada
   
        Select Case SSTab1.Tab

            Case 0 'Carregar dados da proposta

                If .blDadosProposta = False Then
                    p_blNaoExisteErro = CarregarDadosProposta
            
                    .blDadosProposta = True
                End If

            Case 1 'Carregar dados financeiros

                If .blDadosFinanceiros = False And p_blNaoExisteErro = True Then
                    p_blNaoExisteErro = CarregarDadosFinanceiros
                    .blDadosFinanceiros = True
                End If
                 If .blDadosFinanceiros = False And p_blNaoExisteErro = False Then
                 'Caso tenha erro n�o carrega dados
                 'Dados Bancarios -'INICIO  alterado 02/09/2013 projeto 17860335 - Melhorias no SEGBR MC_Amaral -confitec
                   txtAgenciaDeb.Text = ""
                   txtAgenciaDeb.Tag = ""
                   txtCCDeb.Text = ""
                   txtAgencia.Text = ""
                   txtAgencia.Tag = ""
                   txtDiaDebito.Text = ""
                   txtNumCartao.Text = ""
                   txtDtCartao.Text = ""
                   txtFormaPgto.Text = ""
                   txtTpPgto.Text = ""
                   txtNumParcelas.Text = ""
    'Dados Bancarios -'fim  alterado 02/09/2013 projeto 17860335 - Melhorias no SEGBR MC_Amaral -confitec
                 End If

            Case 2 'Carregar dados proponente

                If .blDadosProponente = False And p_blNaoExisteErro = True Then
                    p_blNaoExisteErro = CarregarDadosProponente
                    .blDadosProponente = True
                End If
            
            Case 3 'Carregar dados Objeto de Risco

                If .blDadosObjetoRisco = False And p_blNaoExisteErro = True Then
                    p_blNaoExisteErro = CarregarDadosObjetoRisco
                    .blDadosObjetoRisco = True
                End If
        
            Case 4 'Carregar dados endosso

                If .blDadosEndosso = False And p_blNaoExisteErro = True Then
                    p_blNaoExisteErro = CarregarDadosEndosso
                    .blDadosEndosso = True
                End If
        
            Case 5 'Carregar dados mutu�rios

                If .blDadosMutuarios = False And p_blNaoExisteErro = True Then
                    Call ConPropAdesaoALS.Ler_Mutuarios
                    p_blNaoExisteErro = CarregarDadosMutuario
                    
                    .blDadosMutuarios = True
                End If
        
            Case 6 'Carregar dados benefici�rios

                If .blDadosBeneficiarios = False And p_blNaoExisteErro = True Then
                    p_blNaoExisteErro = CarregarDadosBeneficiarios
                    .blDadosBeneficiarios = True
                End If
            
            Case 7 'Carregar dados sinistro

                If .blDadosSinistro = False And p_blNaoExisteErro = True Then
                    p_blNaoExisteErro = CarregarDadosSinistro
                                            
                    .blDadosSinistro = True
                End If
        
            Case 8 'Carregar dados extrato de seguro

                If .blDadosExtratoSeguro = False And p_blNaoExisteErro = True Then
                    p_blNaoExisteErro = CarregarDadosExtratoSeguro
                    
                    .blDadosExtratoSeguro = True
                End If
                
            Case 10 'Carregar dados objeto de risco RE

                If .blDadosObjetoRiscoRE = False And p_blNaoExisteErro = True Then
                    p_blNaoExisteErro = CarregarDadosObjetoRiscoRE
            
                    .blDadosObjetoRiscoRE = True
                End If
        
            Case 12 'Carregar hist�rico

                If .blDadosHistorico = False And p_blNaoExisteErro = True Then
                    p_blNaoExisteErro = CarregarHistoricoProposta
        
                    .blDadosHistorico = True
                End If

            Case 13 'Carregar cosseguro

                If .blDadosCosseguro = False And p_blNaoExisteErro = True Then
                    Call Ler_Cosseguro(Me, txtProposta.Text, txtDtIniVigProposta.Text)
                    cmbCongenere.ListIndex = 0
                    
                    .blDadosCosseguro = True
        
                End If
                
                'Sergey Souza - 25/08/2010
            Case 14 'Consulta Debito

                If .blDadosConsultaDebito = False And p_blNaoExisteErro = True Then
                    MontaGrid_Avaliacao
                    p_blNaoExisteErro = CarregarConsultaDebito
                    .blDadosConsultaDebito = True
                End If

        End Select
   
    End With
    
ExitMetodo:
    Exit Sub
    Resume
ErrMetodo:
    MsgBox Err.Number & " " & Err.Description
    
End Sub

Private Sub InicializarVariaveisTab()
    On Error GoTo ErrMetodo
    
    With sTabSelecionada
        .blDadosProposta = False
        .blDadosFinanceiros = False
        .blDadosProponente = False
        .blDadosObjetoRisco = False
        .blDadosEndosso = False
        .blDadosMutuarios = False
        .blDadosBeneficiarios = False
        .blDadosSinistro = False
        .blDadosExtratoSeguro = False
        .blDadosObjetoRiscoRE = False
        .blDadosCoberturas = False
        .blDadosHistorico = False
        .blDadosCosseguro = False
        .blDadosConsultaDebito = False
    End With
    
ExitMetodo:
    Exit Sub
    
ErrMetodo:
    MsgBox Err.Number & " " & Err.Number

End Sub

'-------------------------------------------------------------------------
'Demanda 875710 -  Outros Links SEGBR X SCI
'Em 30/04/2009 - Joana Agapito - GPTI
Private Sub cmbBotao_Click(Index As Integer)

    Select Case Index

        Case 0
            Call Unload(Me)

        Case 1
            Call visualizaImagens

        Case 2
           'Flow 19360491 - Confitec: Claudia.Silva - 22/08/2016
            Call frmAbrePDF.ExibeLaudoMedico(Trim(txtPropostaBB))

        End Select

End Sub

Private Sub visualizaImagens()
    Dim dRet As Double
    Dim sProgramaExecutado As String
    Dim paramProposta As String
    Dim paramProduto As String
    Dim AryVazio(0) As Variant
    Dim sSql As String
    Dim rsProduto As ADODB.Recordset
    
    AryVazio(0) = ""
    
    Me.WindowState = 1
    'Chamando o link com o SCI
    sProgramaExecutado = "SCIP0018.EXE"
    
    If VerificaPermissao(gsCPF, Mid$(sProgramaExecutado, 1, 8)) = False Then
        MsgBox "Usu�rio n�o tem acesso ao recurso " & Mid$(sProgramaExecutado, 1, 8) & ".", vbInformation, "Aten��o"
        Exit Sub
    End If
    
    paramProposta = SelPropAdesao.MSFlexGrid1.TextMatrix(SelPropAdesao.MSFlexGrid1.Row, 0)
    
    sSql = "SELECT PRODUTO_ID FROM PROPOSTA_TB  WITH (NOLOCK)  WHERE PROPOSTA_ID = " & paramProposta
    
    Set rsProduto = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, sSql, True)

    If Not rsProduto.EOF Then
        paramProduto = rsProduto(0)
    Else
        MsgBox "N�o foi poss�vel recuperar o produto!", vbExclamation
        Exit Sub
    End If
            
    dRet = ExecutaPrograma(sProgramaExecutado, sProgramaExecutado, Format(paramProposta, "000000000") & ", " & Format(paramProduto, "0000") & ", " & Chr(34) & gsCPF & Chr(34), vbNormalFocus, AryVazio)
    
End Sub
'Fim Demanda 875710 -  Outros Links SEGBR X SCI
'-------------------------------------------------------------------------

Private Function CarregarConsultaDebito() As Boolean

    'Sessao de variaveis
    Dim sSql      As String
    Dim rc        As Recordset
    
    On Error GoTo TrataErro
    
    CarregarConsultaDebito = True
    
    'Busca Identificar se faz parte do PPE
    sSql = "EXEC seguros_db.dbo.SEGS9123_SPS " & SelPropAdesao.MSFlexGrid1.TextMatrix(SelPropAdesao.MSFlexGrid1.RowSel, 0)
    
    'Executa o Comando
    Set rc = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, sSql, True)
        
    'Verifica se voltou registro
    If Not rc.EOF Then

        Do While Not rc.EOF

            With grdAvaliacao
                .Rows = .Rows + 1
                .Row = .Rows - 1
                .TextMatrix(.Row, 0) = rc(0) & "." & Right("000" & rc("remessa"), 4)
                .TextMatrix(.Row, 1) = rc("proposta_bb")
                .TextMatrix(.Row, 2) = rc("num_endosso_bb")
                .TextMatrix(.Row, 3) = rc("dt_importacao")
                .TextMatrix(.Row, 4) = rc("cod_retorno_bb")
                .TextMatrix(.Row, 5) = rc("descricao")
                .TextMatrix(.Row, 6) = rc("produto_bb")
                .TextMatrix(.Row, 7) = rc("proposta_id")
                .TextMatrix(.Row, 8) = rc("produto_id")
                .TextMatrix(.Row, 9) = rc("nome")
                .TextMatrix(.Row, 10) = rc("nosso_numero")
                .TextMatrix(.Row, 11) = rc("nosso_numero_dv")
                .TextMatrix(.Row, 12) = rc("agencia_id")
                .TextMatrix(.Row, 13) = rc("deb_conta_corrente_id")
                .TextMatrix(.Row, 14) = rc("dt_retorno_debito")
                .TextMatrix(.Row, 15) = rc("num_parcela")
                .TextMatrix(.Row, 16) = rc("num_convenio")
                .TextMatrix(.Row, 17) = Format(rc("val_parcela"), "####0.00")
                .TextMatrix(.Row, 18) = rc("dt_vencimento_parcela")
                .TextMatrix(.Row, 19) = rc("cod_produto_als")
                .TextMatrix(.Row, 20) = rc("cod_modalidade_als")
                .TextMatrix(.Row, 21) = rc("cod_item_modalidade_als")
      
            End With

            rc.MoveNext
        Loop

    End If

    rc.Close

    If grdAvaliacao.Rows > 1 Then
        grdAvaliacao.Row = 1
        grdAvaliacao.RowSel = 1
    End If

    Exit Function
    
TrataErro:
    CarregarConsultaDebito = False
    Call TratarErro("CarregarHistoricoProposta", Me.name)
    Call FinalizarAplicacao
    
End Function

Private Sub MontaGrid_Avaliacao()

    With grdAvaliacao
        
        .Cols = 22
        .Rows = 1
        .Col = 0
        .Text = "Arquivo"
        .ColWidth(.Col) = 1500
        
        .Col = 1
        .Text = "Proposta BB"
        .ColWidth(.Col) = 1000
        
        .Col = 2
        .Text = "N�Endosso"
        .ColWidth(.Col) = 0
        
        .Col = 3
        .Text = "Dt.Importa��o"
        .ColWidth(.Col) = 1200
                       
        .Col = 4
        .Text = "Cod.RetornoBB"
        .ColWidth(.Col) = 0
        
        .Col = 5
        .Text = "Descri��o"
        .ColWidth(.Col) = 0
        
        .Col = 6
        .Text = "ProdutoBB"
        .ColWidth(.Col) = 1000
        
        .Col = 7
        .Text = "proposta_id"
        .ColWidth(.Col) = 0
        
        .Col = 8
        .Text = "Produto_id"
        .ColWidth(.Col) = 0
        
        .Col = 9
        .Text = "Nome Produto"
        .ColWidth(.Col) = 0
        
        .Col = 10
        .Text = "Nosso N�"
        .ColWidth(.Col) = 2000
        
        .Col = 11
        .Text = "DV"
        .ColWidth(.Col) = 400
        
        .Col = 12
        .Text = "Agencia"
        .ColWidth(.Col) = 0
        
        .Col = 13
        .Text = "Conta Bancaria"
        .ColWidth(.Col) = 0
        
        .Col = 14
        .Text = "Dt.Ret.Deb."
        .ColWidth(.Col) = 0
        
        .Col = 15
        .Text = "N�Parcela"
        .ColWidth(.Col) = 1000
        
        .Col = 16
        .Text = "N�Convenio"
        .ColWidth(.Col) = 0
        
        .Col = 17
        .Text = "Vlr Parcela"
        .ColWidth(.Col) = 1000
        
        .Col = 18
        .Text = "Dt.Vencto"
        .ColWidth(.Col) = 1000
        
        .Col = 19
        .Text = "cod_produto_als"
        .ColWidth(.Col) = 0
        
        .Col = 20
        .Text = "cod_modalidade_als"
        .ColWidth(.Col) = 0
        
        .Col = 21
        .Text = "cod_item_modalidade_als"
        .ColWidth(.Col) = 0
       
    End With

End Sub



Private Function ObterDadosPessoaFisicaProd718(lPropostaId As Long) As Recordset
'claudia.araujo - demanda 12435268

 Dim sSql As String


 On Error GoTo TrataErro

    
    
    sSql = ""
    sSql = sSql & " SELECT distinct cpf as Cpf, proponente  as Nome, dt_nascimento,   " & vbNewLine
    sSql = sSql & " idade  = ISNULL(dbo.calcula_idade_fn('" & Format(Data_Sistema, "yyyymmdd") & "',emi_proposta_tb.dt_nascimento),0), " & vbNewLine
    sSql = sSql & " sexo = CASE WHEN emi_proposta_tb.sexo = 'M' THEN 'Masculino' WHEN emi_proposta_tb.sexo = 'F' THEN 'Feminino' END , " & vbNewLine
    sSql = sSql & " emi_consorcio_tb.grupo grupo , emi_consorcio_tb.cota cota " & vbNewLine
    sSql = sSql & " From emi_proposta_tb  WITH (NOLOCK)  " & vbNewLine
    sSql = sSql & " Join emi_consorcio_tb  WITH (NOLOCK)  " & vbNewLine
    sSql = sSql & " ON emi_proposta_tb.emi_proposta_id = emi_consorcio_tb.emi_proposta_id " & vbNewLine
    sSql = sSql & " WHERE proposta_id = " & lPropostaId & "" & vbNewLine
    'GENJUNIOR - FLOW 17860335
    'UNION COM TABELAS DE EXPURGO
    '10/10/2013
    sSql = sSql & " UNION " & vbNewLine
    sSql = sSql & " SELECT distinct cpf as Cpf, proponente  as Nome, dt_nascimento,   " & vbNewLine
    sSql = sSql & " idade  = ISNULL(dbo.calcula_idade_fn('" & Format(Data_Sistema, "yyyymmdd") & "',emi_proposta_hist_tb.dt_nascimento),0), " & vbNewLine
    sSql = sSql & " sexo = CASE WHEN emi_proposta_hist_tb.sexo = 'M' THEN 'Masculino' WHEN emi_proposta_hist_tb.sexo = 'F' THEN 'Feminino' END , " & vbNewLine
    sSql = sSql & " emi_consorcio_tb.grupo grupo , emi_consorcio_tb.cota cota " & vbNewLine
    sSql = sSql & " From seguros_hist_db.dbo.emi_proposta_hist_tb emi_proposta_hist_tb  WITH (NOLOCK)  " & vbNewLine
    sSql = sSql & " Join emi_consorcio_tb  WITH (NOLOCK)  " & vbNewLine
    sSql = sSql & " ON emi_proposta_hist_tb.emi_proposta_id = emi_consorcio_tb.emi_proposta_id " & vbNewLine
    sSql = sSql & " WHERE proposta_id = " & lPropostaId & "" & vbNewLine
    'FIM GENJUNIOR

    Set ObterDadosPessoaFisicaProd718 = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, sSql, True)
     
    Exit Function

TrataErro:
    Call TratarErro("ObterDadosPessoaFisicaProd718", Me.name)
    Call FinalizarAplicacao
  
End Function


'vitor.cabral - Nova Consultoria - 11/10/2013 - 17886578 - Registro das avaliacoes na recepcao das parcelas - Inicio
Public Sub CarregaAgendamentosInconsistentes()
    Dim sSql, Linha As String
    Dim rs_agendamentos As Recordset
    Dim Situacao As Boolean

    On Error GoTo TrataErro

    Set rs_agendamentos = New Recordset

    GridProposta(3).Rows = 1

    sSql = ""
    sSql = sSql & " Exec seguros_db.dbo.SEGS11597_SPS "
    sSql = sSql & " @pProposta_id = " & txtProposta.Text
    sSql = sSql & ", @pTipo_produto = " & TipoProduto
    
    
    Set rs_agendamentos = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, sSql, True)
    
    While Not rs_agendamentos.EOF
        Linha = rs_agendamentos("cod_erro") & vbTab
        Linha = Linha & rs_agendamentos("descricao_log") & vbTab
        Linha = Linha & rs_agendamentos("num_parcela") & vbTab
        Linha = Linha & rs_agendamentos("remessa") & vbTab
        Linha = Linha & rs_agendamentos("seq_remessa") & vbTab
        Linha = Linha & rs_agendamentos("nosso_numero") & vbTab
        Linha = Linha & rs_agendamentos("nosso_numero_dv") & vbTab
        Linha = Linha & rs_agendamentos("forma_pgto_id") & vbTab
        Linha = Linha & rs_agendamentos("desc_forma_pgto") & vbTab
        Linha = Linha & rs_agendamentos("dt_venc_parcela") & vbTab
        Linha = Linha & rs_agendamentos("val_parcela") & vbTab
        Linha = Linha & rs_agendamentos("ano_mes_ref") & vbTab
        Linha = Linha & rs_agendamentos("dt_inclusao") & vbTab
        Linha = Linha & rs_agendamentos("observacao") & vbTab
        Linha = Linha & rs_agendamentos("status") & vbTab
        
        GridProposta(3).AddItem Linha
        rs_agendamentos.MoveNext
        
    Wend
    
    Set rs_agendamentos = Nothing
    
    Exit Sub

TrataErro:
    Call TratarErro("Carrega_Restituicao", Me.name)
    Call FinalizarAplicacao
        
End Sub
'vitor.cabral - Nova Consultoria - 11/10/2013 - 17886578 - Registro das avaliacoes na recepcao das parcelas - Fim

Private Function Retorna_convenio_registra_boleto(ByVal convenio As String)
     
    Dim rs As Recordset
    Dim Sql As String
    Set rs = New Recordset
    
    Sql = ""
    Sql = "SELECT DISTINCT ISNULL(registra_boleto,'N') AS registra_boleto from seguros_db.dbo.convenio_tb with(nolock) "
    Sql = Sql & " where num_convenio = '" & convenio & "' "
   
   Set rs = ExecutarSQL(gsSIGLASISTEMA, _
                        glAmbiente_id, _
                        App.Title, _
                        App.FileDescription, _
                        Sql, _
                        True)
     
     If Not rs.EOF Then
      Retorna_convenio_registra_boleto = rs("registra_boleto")
     Else
      Retorna_convenio_registra_boleto = "N"
     End If

End Function
Private Function ProcessaRegistraBoletosBB(ByVal AProposta, ByVal ACobranca)

 Dim objXmlWebservice As Object

 Set objXmlWebservice = CreateObject("SEGL0365.WebServiceBB")
        
 RegistraBoleto = objXmlWebservice.ProcessaBoletoBB(Trim(AProposta), Trim(ACobranca), cUserName, _
                             gsSIGLASISTEMA, _
                             "SEGP01189", _
                             App.FileDescription, _
                             glAmbiente_id, _
                             cUserName)          'Marcio.Nogueira - 20/07/2017 - Demanda - 19368999 - Cobran�a registrada AB e ABS

  If RegistraBoleto = "" Then
         
         MsgBox "O Boleto n�o foi Registrado, falha no registro, a solicita��o n�o poder� ser processada!", vbCritical, "Gera Boleto"
         Me.MousePointer = 0
          ProcessaRegistraBoletosBB = ""
         BlnBoletoRegistrado = False
         
  Else
       
         BlnBoletoRegistrado = True
         
  End If
        
End Function

Public Function PossuiFluxoDigital() As Boolean

    Dim sSql As String
    Dim rs As ADODB.Recordset
        
    sSql = ""
    sSql = "      SELECT TOP 1 dt_implantacao_digital "
    sSql = sSql & " FROM interface_db.dbo.layout_produto_tb WITH (NOLOCK) "
    sSql = sSql & "WHERE produto_id = " & txtProduto.Tag
    sSql = sSql & "  AND dt_implantacao_digital IS NOT NULL "
    
       Set rs = ExecutarSQL(gsSIGLASISTEMA, _
                        glAmbiente_id, _
                        App.Title, _
                        App.FileDescription, _
                        sSql, _
                        True)
                    
    If rs.EOF Then
        PossuiFluxoDigital = False
    Else
        PossuiFluxoDigital = True
        rs.Close
        Set rs = Nothing
    End If

End Function

Public Function VerificaFluxo() As String

    Dim sSql As String
    Dim rs As ADODB.Recordset
        
    sSql = ""
    sSql = "      SELECT CASE WHEN p.tp_documento IS NULL THEN CASE WHEN p.dt_contratacao >= l.dt_implantacao_digital "
    sSql = sSql & "                                                 THEN 'Digital' "
    sSql = sSql & "                                                 ELSE 'Fisico' "
    sSql = sSql & "                                             END "
    sSql = sSql & "           WHEN p.tp_documento = 'D' THEN 'Digital' "
    sSql = sSql & "           ELSE 'Fisico' "
    sSql = sSql & "       END "
    sSql = sSql & " FROM seguros_db.dbo.proposta_tb p WITH (NOLOCK) "
    sSql = sSql & "OUTER APPLY (SELECT MIN(dt_implantacao_digital) dt_implantacao_digital "
    sSql = sSql & "               FROM interface_db.dbo.layout_produto_tb i WITH (NOLOCK) "
    sSql = sSql & "              WHERE p.produto_id = i.produto_id "
    sSql = sSql & "                AND i.dt_implantacao_digital IS NOT NULL) l "
    sSql = sSql & "WHERE p.proposta_id = " & txtProposta.Text

       Set rs = ExecutarSQL(gsSIGLASISTEMA, _
                        glAmbiente_id, _
                        App.Title, _
                        App.FileDescription, _
                        sSql, _
                        True)
                    
    If Not rs.EOF Then
        VerificaFluxo = rs(0)
        rs.Close
        Set rs = Nothing
    End If

End Function
