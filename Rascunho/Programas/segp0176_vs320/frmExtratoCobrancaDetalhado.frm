VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "mscomctl.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Begin VB.Form ExtratoCobrancaDetalhado 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "SEGP0176 - Consulta de Proposta"
   ClientHeight    =   4140
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   12030
   LinkTopic       =   "ExtratoCobrancaDetalhado"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4140
   ScaleWidth      =   12030
   StartUpPosition =   3  'Windows Default
   Begin VB.Frame Frame12 
      Caption         =   "Extrato de Cobran�a "
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3255
      Index           =   0
      Left            =   90
      TabIndex        =   3
      Top             =   45
      Width           =   11865
      Begin MSFlexGridLib.MSFlexGrid MSFlexCobranca 
         Height          =   2865
         Left            =   90
         TabIndex        =   4
         Top             =   270
         Width           =   11685
         _ExtentX        =   20611
         _ExtentY        =   5054
         _Version        =   393216
         Cols            =   22
         FixedCols       =   0
         AllowUserResizing=   1
         FormatString    =   $"frmExtratoCobrancaDetalhado.frx":0000
      End
   End
   Begin VB.CommandButton cmbBotao 
      Caption         =   "&Sair"
      Height          =   405
      Index           =   1
      Left            =   10800
      TabIndex        =   2
      Top             =   3375
      Width           =   1150
   End
   Begin VB.CommandButton cmbBotao 
      Caption         =   "&Exportar Para Excel"
      Height          =   405
      Index           =   0
      Left            =   7785
      TabIndex        =   1
      Top             =   3375
      Width           =   2940
   End
   Begin MSComctlLib.StatusBar StatusBar 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   0
      Top             =   3855
      Width           =   12030
      _ExtentX        =   21220
      _ExtentY        =   503
      Style           =   1
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
         EndProperty
      EndProperty
   End
   Begin MSComDlg.CommonDialog dlgGrid 
      Left            =   1980
      Top             =   3375
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
End
Attribute VB_Name = "ExtratoCobrancaDetalhado"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim gProposta As Long

Public Sub CarregaVariaveis(lPropostaId As Long)
  gProposta = lPropostaId
End Sub

Private Function ObterDadosCobranca(lPropostaId As Long) As Recordset

    On Error GoTo TrataErro

    Dim sSql As String
  
    sSql = "EXEC seguros_db.dbo.SEGS12531_SPS " & lPropostaId
    
    Set ObterDadosCobranca = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, sSql, True)
    Exit Function
    
TrataErro:
    Call TratarErro("ObterDadosCobranca", Me.name)
    Call FinalizarAplicacao
    
End Function

Private Sub CarregaDadosCobranca()
    
    On Error GoTo TrataErro

    Dim rsDadosCobranca As Recordset
    Dim sLinha As String

    Set rsDadosCobranca = New Recordset
    Set rsDadosCobranca = ObterDadosCobranca(gProposta)
   
    MSFlexCobranca.Rows = 1
  
    While Not rsDadosCobranca.EOF
        
        sLinha = ""
        sLinha = sLinha & rsDadosCobranca("proposta_bb") & vbTab
        sLinha = sLinha & rsDadosCobranca("proposta_id") & vbTab
        sLinha = sLinha & rsDadosCobranca("tipo_registro") & vbTab
        sLinha = sLinha & rsDadosCobranca("tp_cobranca") & vbTab
        sLinha = sLinha & rsDadosCobranca("num_cobranca") & vbTab
        sLinha = sLinha & rsDadosCobranca("nosso_numero") & vbTab
        sLinha = sLinha & rsDadosCobranca("dv") & vbTab
        sLinha = sLinha & rsDadosCobranca("ano_mes_ref") & vbTab
        sLinha = sLinha & rsDadosCobranca("val_parcela") & vbTab
        sLinha = sLinha & rsDadosCobranca("dt_vcto") & vbTab
        sLinha = sLinha & rsDadosCobranca("dt_evento_envio") & vbTab
        sLinha = sLinha & rsDadosCobranca("nome_arquivo_agendamento") & vbTab
        sLinha = sLinha & rsDadosCobranca("num_cartao_envio") & vbTab
        sLinha = sLinha & rsDadosCobranca("bandeira_envio") & vbTab
        sLinha = sLinha & rsDadosCobranca("dt_evento_retorno") & vbTab
        sLinha = sLinha & rsDadosCobranca("nome_arquivo_ret") & vbTab
        sLinha = sLinha & rsDadosCobranca("sit_ret") & vbTab
        sLinha = sLinha & rsDadosCobranca("num_cartao_retorno") & vbTab
        sLinha = sLinha & rsDadosCobranca("banco") & vbTab
        sLinha = sLinha & rsDadosCobranca("agencia") & vbTab
        sLinha = sLinha & rsDadosCobranca("conta") & vbTab
        '------------------------------------------------------------------------------------------------
        '--- Inicio das altera��es para considerar o tipo de cronograma - DEMANDA - SBRJ009577
        
        sLinha = sLinha & rsDadosCobranca("tp_cronograma")
    
        '--- Fim das altera��es para considerar o tipo de cronograma - DEMANDA - SBRJ009577
        '------------------------------------------------------------------------------------------------
    
        MSFlexCobranca.AddItem sLinha
                
        rsDadosCobranca.MoveNext
    Wend
    
    rsDadosCobranca.Close
    Exit Sub
TrataErro:
    Call TratarErro("CarregaDadosCobranca", Me.name)
    Call FinalizarAplicacao

End Sub

Public Sub cmbBotao_Click(Index As Integer)
  
    On Error GoTo Trata_Erro
    
    MousePointer = 11
    
    Select Case Index
        Case 0
            Dim oGeradorExcel As New clsGeradorExcel
            Call oGeradorExcel.ExportarFlex(MSFlexCobranca, "", "Extrato de Cobran�a Detalhado", ExtratoCobrancaDetalhado)
        Case 1
            Call Unload(Me)
    End Select
            
    MousePointer = 0
    
    Exit Sub
    
Trata_Erro:
    MousePointer = 0
    Call TrataErroGeral("cmbBotao_Click", Me.name)
            
End Sub

Private Sub Form_Activate()
      CarregaDadosCobranca
End Sub

