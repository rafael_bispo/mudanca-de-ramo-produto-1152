VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsFormatacao"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'vitor.cabral - Nova Consultoria - 10/10/2013 - Demanda 17886578 � Registro das avalia��es na recep��o de parcelas
Option Explicit

Private ExcelApp As Excel.Application

Public Sub Config(oExcelApp As Excel.Application)
    Set ExcelApp = oExcelApp
End Sub

Public Sub FormatarCabecalho(ByVal Range As String)

    With ExcelApp
        .Range(Range).Font.Bold = True
        .Range(Range).HorizontalAlignment = xlCenter
        .Range(Range).Interior.Color = RGB(192, 192, 192)
        .Range(Range).VerticalAlignment = xlCenter
        .Range(Range).WrapText = True
    End With
    
    FormatarBorda (Range)
    
End Sub

Public Sub FormatarDados(ByVal Range As String)
    With ExcelApp
        .Range(Range).HorizontalAlignment = xlRight
    End With
    FormatarBorda (Range)
End Sub

Private Sub FormatarBorda(ByVal Range As String)
    With ExcelApp
        .Range(Range).BorderAround xlSolid
        .Range(Range).Borders.LineStyle = xlSolid
    End With
End Sub

Public Sub FormatarFonte()
    Dim Worksheet As Worksheet
    For Each Worksheet In ExcelApp.Worksheets
        With Worksheet.Cells
            .Font.name = "Arial"
            .Font.Size = 8
        End With
    Next
End Sub
