Attribute VB_Name = "Constantes"
'vitor.cabral - Nova Consultoria - 16/10/2013 - Demanda 17886578 � Registro das avalia��es na recep��o de parcelas
' Declara��es de constantes
Option Explicit

' Nome das planilhas
Public Const NomePlanilhaAgendInconsist = "Agendamentos Inconsistentes"

' Formatos de c�lula
Public Const FormatoMoeda = "#,##0.00;-#,##0.00;""Sem pgto"";@"
'Public Const FormatoIndice = "#,##0.0000"
'Public Const FormatoIndiceAcumulado = "#,##0.0000000000"
'Public Const FormatoTaxa = "#,##0.00000"
Public Const FormatoNumeroInteiro = "##0"
Public Const FormatoMesAno = "yyyy/mm"
'Public Const FormatoMesAnoExtenso = "mmm/yyyy"
Public Const FormatoData = "d/m/yyyy"
'Public Const FormatoPercentual = "0.00%"

'Mapeamento das colunas do GRID para o EXCEL - Inicio
Public Const LINHA_CABECALHO As Integer = 2
Public Const LINHA_INICIO_DADOS As Integer = 3
Public Const COL_COD_ERRO As String = "A"
Public Const COL_DESCRICAO_LOG As String = "B"
Public Const COL_NUMERO_PARCELA As String = "C"
Public Const COL_REMESSA As String = "D"
Public Const COL_SEQUENCIAL_REMESSA As String = "E"
Public Const COL_NOSSO_NUMERO As String = "F"
Public Const COL_NOSSO_NUMERO_DV As String = "G"
Public Const COL_FORMA_PGTO_ID As String = "H"
Public Const COL_DESC_FORMA_PGTO As String = "I"
Public Const COL_DT_VENC_PARCELA As String = "J"
Public Const COL_VALOR_PARCELA As String = "K"
Public Const COL_ANO_MES_REFER As String = "L"
Public Const COL_DT_INCLUSAO As String = "M"
Public Const COL_OBSEVACAO As String = "N"
Public Const COL_STATUS As String = "O"
'Mapeamento das colunas do GRID para o EXCEL - Fim
