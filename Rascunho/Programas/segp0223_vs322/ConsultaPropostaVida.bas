Attribute VB_Name = "ConsultaPropostaVida"
Option Explicit

''Constantes para tratamento das colunas do grid de sele��o proposta
Global Const GRD_SEL_APOL = 0
Global Const GRD_SEL_RAMO = 1
Global Const GRD_SEL_NMRAMO = 2
Global Const GRD_SEL_PROD = 3
Global Const GRD_SEL_CODCLI = 4
Global Const GRD_SEL_NOMECLI = 5
Global Const GRD_SEL_DTCONT = 6
Global Const GRD_SEL_SIT = 7
Global Const GRD_SEL_PROPBB = 8
Global Const GRD_SEL_NOENDO = 9
Global Const GRD_SEL_DTENDO = 10
Global Const GRD_SEL_ENDBB = 11
Global Const GRD_SEL_TPENDO = 12
Global Const GRD_SEL_SEGCLI = 13
''Constantes para tratamento das colunas do grid de sele��o cliente
Global Const GRD_CLI_COD = 0
Global Const GRD_CLI_PROP = 1
Global Const GRD_CLI_PROD = 2
Global Const GRD_CLI_NOME = 3
Global Const GRD_CLI_DTCONT = 4
Global Const GRD_CLI_SIT = 5
Global Const GRD_CLI_SEGCLI = 6
'' Constante para o tipo de endosso de cancelamento
Global Const TP_ENDO_CANC_ENDO = 101
''
Global vSeguradoraId As String
Global vSucursalId As String
Global vRamoId As String
Global vApoliceId As String
Global vPropostaId As String
Global vProdutoId As String
Global vNomeProd As String
Global vSituacao As String
Global vorigem_proposta_id As String
Global bPropostaALS As Boolean
Global vClienteId As String
Global vSubGrupoId As String
Global vCongenereId As String
Global gTpEmissao As String
Global vSegCliente As String    'Rmarins - 05/01/2006

' Para exibi��o de informa��es de Cosseguros Aceitos (ap�lices de terceiros).
Global CosseguroAceito As Boolean
Global indEndosso As Boolean
Global indEndossoGrid As Boolean
' Codigos de produtos validos para a consulta.
Global ProdutosValidos As String
' Para consulta - Endosso
Global tp_componente_endosso As String
Global IdTpPessoa_endosso As String

Global gEndossoId As Long
Global gTpEndosso As Integer
Global gDescTpEndosso As String
Global gTpAltEnd As String           'rfarzat - 24/10/2018 - Endosso 252 - Sala �gil
Global gDescEndosso As String
Global gEndossoCancId As Long
Global gTpEndossoCanc As Long
Global gDtEmiEndosso As Variant
Global gDtIniVigEndosso As Variant
Global gDiaFaturamento As Variant
Global gDiaCobranca As Variant
Global gPeriodoPgto As Variant
'fsa - 09052006
Global gUsuEmissao As String
''
Global HaEndFinanceiro As Boolean
Global HaSubGrupos As Boolean
Global HaCertificados As Boolean
Global HaAdministracao As Boolean
Global HaRepresentacao As Boolean
Global HaCorretor As Boolean
' Configura��o padr�o de impressora.
Global DeviceDefault As String
Global OldDefault As String
Global MudouPrtDefault As Boolean
''
Global rdoCn1 As New rdoConnection
Global rdoCn2 As New rdoConnection
Global adoCn As New ADODB.Connection
Global produto_externo_id As Variant
Global ConfiguracaoBrasil As Boolean
Global dt_inicio_vigencia As String
Global dt_inicio_query As String
''
Global vPremioTarifa As Double
Global vPremioTarifaParc As Double
Global vPremioTarifaUltParc As Double
Global vTemp As Double
Global vTotDesconto As Double
Global vTotDescontoParc As Double
Global vTotDescontoUltParcela As Double
Global vTotJuros As Double
Global vTotJurosParcela As Double
Global vTotJurosUltParcela As Double
Global vTotComissaoParcela As Double
Global vTotComissaoUltParcela As Double
Global vTotDespParcela As Double
Global vTotDespUltParcela As Double

Global vValZero As String

Global Congeneres As New Collection
Global SubGrupos As New Collection
Global Corretores As New Collection
Global Componentes As New Collection
Global Coberturas As New Collection
Global Clausulas As New Collection
Global Estipulantes As New Collection
Global Administradores As New Collection
Global Faturas As New Collection
Global Vidas_Seguradas As New Collection

Global Assistencias As New Collection

Global meses(1 To 12) As String
Global FP_Agenciamento(0 To 4) As String

''
Declare Function WritePrivateProfileString Lib "kernel32" Alias "WritePrivateProfileStringA" (ByVal lpApplicationName As String, ByVal lpKeyName As Any, ByVal lpString As Any, ByVal lpFileName As String) As Long

Public Declare Function PostMessage Lib "user32" Alias "PostMessageA" (ByVal hwnd As Long, ByVal wMsg As Long, ByVal wParam As Long, lparam As Any) As Long

Function RetornaPrtDefault(ByVal FileName As String, PrtDefault As String) As Boolean
    Dim nc As Boolean

    nc = WritePrivateProfileString("windows", "device", PrtDefault, FileName)
    RetornaPrtDefault = nc

End Function

Function Renova_Apolice() As String

    Dim rc As rdoResultset
    Dim SQL As String, proposta_id_anterior As String

    SQL = "SELECT isnull(b.proposta_id, 0) proposta_id " & _
          "FROM proposta_fechada_tb  a WITH(NOLOCK) , proposta_fechada_tb b WITH(NOLOCK) " & _
          "WHERE a.proposta_bb_anterior = b.proposta_bb AND " & _
        "      a.proposta_id = " & vPropostaId

    Set rc = rdocn.OpenResultset(SQL)

    If Not rc.EOF Then

        proposta_id_anterior = rc!proposta_id

        rc.Close
        Set rc = Nothing

        SQL = "SELECT a.apolice_id FROM apolice_tb a WITH(NOLOCK), proposta_basica_tb b WITH(NOLOCK) " & _
              "WHERE b.proposta_id = " & proposta_id_anterior & " AND " & _
            "      a.proposta_id = b.proposta_id AND " & _
            "      a.sucursal_seguradora_id = b.sucursal_seguradora_id AND " & _
            "      a.ramo_id = b.ramo_id AND " & _
            "      a.seguradora_cod_susep = b.seguradora_cod_susep "

        Set rc = rdocn.OpenResultset(SQL)

        If Not rc.EOF And Not IsNull(rc(0)) Then
            Renova_Apolice = Format(rc(0), "000000000")
        Else
            Renova_Apolice = String(9, "0")
        End If

    Else
        Renova_Apolice = String(9, "0")
    End If

    rc.Close

End Function


Public Sub Calcula_ValoresCosseguro(vPercParticipacao, vPercDespesa, vAuxComissao, FormAtivo As Form)

    Dim vValComissao As Double, vPremioLiqDesc As Double

    With FormAtivo
        If ConfiguracaoBrasil Then
            'Valor de comiss�o ser� calculado sobre  pr�mio l�quido - descontos comerciais
            vPremioLiqDesc = CDbl(.txtTotalPremioLiq.Text) - CDbl(.txtTotalDescontos.Text)

            vPremioTarifa = (CDbl(.txtTotalPremioLiq.Text) * vPercParticipacao) / 100

            '            vPremioTarifaParc = Trunca(vPremioTarifa / .txtNumParcelas.Text)
            'Premio tarifario �ltima parcela
            vPremioTarifaUltParc = vPremioTarifaParc * (.txtNumParcelas.Text - 1)
            vPremioTarifaUltParc = Trunca(vPremioTarifa - vPremioTarifaUltParc)

            'Total de descontos por parcela
            vTotDesconto = (CDbl(.txtTotalDescontos.Text) * vPercParticipacao) / 100
            '            vTotDescontoParc = Trunca(vTotDesconto / .txtNumParcelas.Text)
            'Total de descontos �lt. parcela
            vTotDescontoUltParcela = vTotDescontoParc * (.txtNumParcelas.Text - 1)
            vTotDescontoUltParcela = Trunca(vTotDesconto - vTotDescontoUltParcela)

            'Total de juros por parcela
            vTotJuros = (CDbl(.txtValJuros.Text) * vPercParticipacao) / 100
            '            vTotJurosParcela = Trunca(vTotJuros / .txtNumParcelas.Text)
            'Total de juros �lt. parcela
            vTotJurosUltParcela = vTotJurosParcela * (.txtNumParcelas.Text - 1)
            vTotJurosUltParcela = Trunca(vTotJuros - vTotJurosUltParcela)

            'Total de comiss�o por parcela
            'vValComissao = ((CDbl(vPremioLiqDesc) * (vAuxComissao + vPercDespesa) / 100) * vPercParticipacao) / 100
            vValComissao = ((CDbl(vPremioLiqDesc) * vAuxComissao / 100) * vPercParticipacao) / 100
            '            vTotComissaoParcela = Trunca(vValComissao / .txtNumParcelas.Text)
            'Total de juros �lt. parcela
            vTotComissaoUltParcela = vTotComissaoParcela * (.txtNumParcelas.Text - 1)
            vTotComissaoUltParcela = Trunca(vValComissao - vTotComissaoUltParcela)
        Else
            'Valor de comiss�o ser� calculado sobre o pr�mio l�quido - descontos comerciais
            vPremioLiqDesc = CDbl(TrocaValorBrasPorAme(.txtTotalPremioLiq.Text)) - CDbl(TrocaValorBrasPorAme(.txtTotalDescontos.Text))

            vPremioTarifa = (CDbl(TrocaValorBrasPorAme(.txtTotalPremioLiq.Text)) * vPercParticipacao) / 100

            vPremioTarifaParc = Trunca(vPremioTarifa / .txtNumParcelas.Text)
            'Premio tarifario �ltima parcela
            vPremioTarifaUltParc = vPremioTarifaParc * (.txtNumParcelas.Text - 1)
            vPremioTarifaUltParc = Trunca(vPremioTarifa - vPremioTarifaUltParc)

            vTotDesconto = (CDbl(TrocaValorBrasPorAme(.txtTotalDescontos.Text)) * vPercParticipacao) / 100
            'Total de descontos por parcela
            vTotDescontoParc = Trunca(vTotDesconto / .txtNumParcelas.Text)
            'Total de descontos �lt. parcela
            vTotDescontoUltParcela = vTotDescontoParc * (.txtNumParcelas.Text - 1)
            vTotDescontoUltParcela = Trunca(vTotDesconto - vTotDescontoUltParcela)

            'Total de juros por parcela
            vTotJuros = (CDbl(TrocaValorBrasPorAme(.txtValJuros.Text)) * vPercParticipacao) / 100
            vTotJurosParcela = Trunca(vTotJuros / .txtNumParcelas.Text)
            'Total de juros �lt. parcela
            vTotJurosUltParcela = vTotJurosParcela * (.txtNumParcelas.Text - 1)
            vTotJurosUltParcela = Trunca(CDbl(TrocaValorBrasPorAme(.txtValJuros.Text)) - vTotJurosUltParcela)

            'vValComissao = ((CDbl(TrocaValorBrasPorAme(vPremioLiqDesc)) * (vAuxComissao + vPercDespesa) / 100) * vPercParticipacao) / 100
            vValComissao = ((vPremioLiqDesc * vAuxComissao / 100) * vPercParticipacao) / 100
            vTotComissaoParcela = Trunca(vValComissao / .txtNumParcelas.Text)
            'Total de juros �lt. parcela
            vTotComissaoUltParcela = vTotComissaoParcela * (.txtNumParcelas.Text - 1)
            vTotComissaoUltParcela = Trunca(vValComissao - vTotComissaoUltParcela)
        End If
    End With

End Sub
Private Function valor_assistencia_facultativa(ByVal pProposta_id As Long, ByVal pProduto_id As Long) As Double
    Dim rs As rdoResultset, sSql As String

    sSql = "set nocount on exec seguros_db..SEGS12290_SPS " & pProposta_id & "," & pProduto_id & ",'N',1"

    Set rs = rdocn.OpenResultset(sSql)

    If Not rs.EOF Then
        If IsNull(rs!val_assistencia_carregado) Then
            valor_assistencia_facultativa = 0
        Else
            valor_assistencia_facultativa = Val(rs!val_assistencia_carregado)
        End If
    Else

    End If
    rs.Close: Set rs = Nothing


End Function



Sub Monta_Colecao_Coberturas_SubGrupo(ByVal rs As rdoResultset)
    Dim bVerAssistencia As Boolean    'Flow 18334512 - Circular 491

    Dim vCobertura As Cobertura
    Dim vValPremio As String, vValPremioLiq As String, vValPremioTarifa As String, vValLimMinFranquia As String, vValIS As String
    Dim vFatTaxa As String, Indice As String, vValFranquia As String
    Dim vDescricao As String, vPercBas As String, vClasseCob As String
    Dim vDesconto As String, vPercFranquia As String, vTxtFranquia As String

    On Error GoTo Erro

    Set Coberturas = Nothing
    bVerAssistencia = False
    '
    While Not rs.EOF
        If ConfiguracaoBrasil Then
            vValIS = Format(Val(rs!val_is), "###,###,###,##0.00")
            vDesconto = Format(0, "##0.00000")
            vValPremio = Format(Val(rs!val_Premio), "###,###,###,##0.00")

            'INICIO Flow 18334512 - Circular 491 - 19/02/2015 inclus�o do vValPremioLiq e vValPremioTarifa
            If IsNull(rs!val_premio_liq) Then
                If IsNull(rs!val_premio_tarifa) Then
                    vValPremioLiq = "0,00"
                    vValPremioTarifa = "0,00"
                Else
                    vValPremioTarifa = Format(Val(rs!val_premio_tarifa), "#,##0.00")
                    vValPremioLiq = vValPremioTarifa
                    bVerAssistencia = True
                End If
            Else
                vValPremioLiq = Format(Val(rs!val_premio_liq), "#,##0.00")
                vValPremioTarifa = Format(Val(rs!val_premio_tarifa), "#,##0.00")
            End If
            'FIM    Flow 18334512 - Circular 491 - 19/02/2015 inclus�o do vValPremioLiq e vValPremioTarifa

            vFatTaxa = Format(Val(rs!val_taxa), "0.0000000")
            vPercBas = Format(Val(rs!perc_basica), "#0.00000")
        Else
            vValIS = TrocaValorAmePorBras(Format(Val(rs!val_is), "###,###,###,##0.00"))
            vDesconto = TrocaValorAmePorBras(Format(0, "##0.00000"))
            vValPremio = TrocaValorAmePorBras(Format(Val(rs!val_Premio), "###,###,###,##0.00"))

            'INICIO Flow 18334512 - Circular 491 - 19/02/2015 inclus�o do vValPremioLiq e vValPremioTarifa
            If IsNull(rs!val_premio_liq) Then
                If IsNull(rs!val_premio_tarifa) Then
                    vValPremioLiq = "0,00"
                    vValPremioTarifa = "0,00"
                Else
                    vValPremioTarifa = TrocaValorAmePorBras(Format(Val(rs!val_premio_tarifa), "#,##0.00"))
                    vValPremioLiq = vValPremioTarifa
                    bVerAssistencia = True
                End If
            Else
                vValPremioLiq = TrocaValorAmePorBras(Format(Val(rs!val_premio_liq), "#,##0.00"))
                vValPremioTarifa = TrocaValorAmePorBras(Format(Val(rs!val_premio_tarifa), "#,##0.00"))
            End If
            'FIM    Flow 18334512 - Circular 491 - 19/02/2015 inclus�o do vValPremioLiq e vValPremioTarifa

            vFatTaxa = TrocaValorAmePorBras(Format(Val(rs!val_taxa), "0.0000000"))
            vPercBas = TrocaValorAmePorBras(Format(Val(rs!perc_basica), "#0.00000"))
        End If
        vClasseCob = UCase(rs!class_tp_cobertura)
        '
        vPercFranquia = Val(rs!fat_franquia) * 100
        vValFranquia = Val(rs!val_min_franquia)
        vTxtFranquia = UCase(Trim(rs!texto_franquia))

        '
        Set vCobertura = New Cobertura
        With vCobertura
            .CodObjSegurado = rs!tp_componente_id
            '.TpCoberturaId = rs!tp_cob_comp_id
            .TpCoberturaId = rs!tp_cobertura_id
            .Descricao = Trim(rs!Nome)
            .ValIS = vValIS
            .ValTaxa = vFatTaxa
            .PercDesconto = vDesconto
            .PercBasica = vPercBas
            .ValPremio = vValPremio
            .ValPremioLiq = vValPremioLiq
            .ValPremioTarifa = vValPremioTarifa
            .Classe = vClasseCob
            .DescFranquia = vDescricao
            .ValFranquia = Format$(vValFranquia, "0.00")
            .PercFranquia = Format$(vPercFranquia, "0.00")
            .DescFranquia = vTxtFranquia
            .LimMinIS = Val(rs!val_lim_min_is)
            .LimMaxIS = Val(rs!val_lim_max_is)
            .Carencia = Val(rs!carencia_dias)
            .LimDiarias = Val(rs!lim_diaria_dias)
        End With
        Coberturas.Add vCobertura, CStr(rs!tp_cob_comp_id)
        '
        rs.MoveNext
    Wend

    '' ResultSet foi aberto fora da fun��o, fechar fora da fun��o
    'Rs.Close

    'INICIO Flow 18334512 - Circular 491 - 19/02/2015 inclus�o do vValPremioLiq e vValPremioTarifa
    If bVerAssistencia Then
        'casos antigos, campo [val_premio_tarifa_sem_custo_assist_facul] n�o est� preenchido.
        'verificar o valor da assistencia facultativa
        Dim ii As Integer
        Dim fator As Double
        Dim vAcum As Currency, vAjustado As Currency, valAssistFacult As Currency

        valAssistFacult = valor_assistencia_facultativa(vPropostaId, vProdutoId)
        If valAssistFacult > 0 Then
            'tem assistencia facultativa, retirar de valpremioliq de modo proporcional
            vAcum = 0
            For Each vCobertura In Coberturas
                vAcum = vAcum + Val(Replace(Replace(vCobertura.ValPremioTarifa, ".", ""), ",", "."))
            Next

            For Each vCobertura In Coberturas
                fator = Val(Replace(Replace(vCobertura.ValPremioTarifa, ".", ""), ",", ".")) / vAcum
                vAjustado = Val(Replace(Replace(vCobertura.ValPremioLiq, ".", ""), ",", ".")) - (valAssistFacult * fator)
                vCobertura.ValPremioLiq = Format(vAjustado, "#,##0.00")
                If Not ConfiguracaoBrasil Then vCobertura.ValPremioLiq = TrocaValorAmePorBras(vCobertura.ValPremioLiq)
            Next
        End If
    End If

    Exit Sub

Erro:
    TrataErroGeral "Ler Coberturas"
    MsgBox "Rotina: Ler Coberturas"
    'End

End Sub


Sub Monta_GridCosseguroTotal(ByRef vAuxComissao, FormAtivo As Form)

    Dim vValPremioParc As String, vValComissaoParc As String
    Dim vValJurosParc As String, vValDescontoParc As String, vValDespParc As Double
    Dim i As Integer, CongenereElemento As Congenere, vValorTemp As Double, vTotalLiquido As Double

    With FormAtivo

        .txtPercDespesa.Text = "0,00%"

        For i = 1 To .gridCobrancaCosseguro.Rows - 1
            .gridCobrancaCosseguro.Row = i
            .gridCobrancaCosseguro.col = 3    ' Valor Premio Tarif
            .gridCobrancaCosseguro.Text = "0,00"
            .gridCobrancaCosseguro.col = 4    ' Valor Desconto
            .gridCobrancaCosseguro.Text = "0,00"
            .gridCobrancaCosseguro.col = 5    ' Valor Juros
            .gridCobrancaCosseguro.Text = "0,00"
            .gridCobrancaCosseguro.col = 6    ' Valor Comissao
            .gridCobrancaCosseguro.Text = "0,00"
            .gridCobrancaCosseguro.col = 7    ' Valor Desp Lider
            .gridCobrancaCosseguro.Text = "0,00"
            .gridCobrancaCosseguro.col = 8    ' Total Liquido
            .gridCobrancaCosseguro.Text = "0,00"
            If Trim(.gridCobrancaCosseguro.Text) <> "" Then
                .gridCobrancaCosseguro.Text = "0,00"
            End If
        Next i
        For Each CongenereElemento In Congeneres
            Call Calcula_ValoresCosseguro(CongenereElemento.Perc_Participacao, CongenereElemento.Perc_Despesa_Lider, vAuxComissao, FormAtivo)
            If ConfiguracaoBrasil Then
                If Right(.txtPercParticipacao.Text, 1) = "%" Then
                    .txtPercParticipacao.Text = Mid(.txtPercParticipacao.Text, 1, Len(.txtPercParticipacao.Text) - 1)
                End If
                .txtPercParticipacao.Text = Format(CDbl(.txtPercParticipacao.Text) + CongenereElemento.Perc_Participacao, "##0.00")
                'If Right(.txtPercDespesa.Text, 1) = "%" Then
                '    .txtPercDespesa.Text = Mid(.txtPercDespesa.Text, 1, Len(.txtPercDespesa.Text) - 1)
                'End If
                '.txtPercDespesa.Text = Format(CDbl(.txtPercDespesa.Text) + CongenereElemento.Perc_Despesa_Lider, "##0.00") & "%"
            Else
                If Right(.txtPercParticipacao.Text, 1) = "%" Then
                    .txtPercParticipacao.Text = Mid(.txtPercParticipacao.Text, 1, Len(.txtPercParticipacao.Text) - 1)
                End If
                .txtPercParticipacao.Text = Format(CDbl(TrocaValorBrasPorAme(.txtPercParticipacao.Text)) + CongenereElemento.Perc_Participacao, "##0.00")
                'If Right(.txtPercDespesa.Text, 1) = "%" Then
                '    .txtPercDespesa.Text = Mid(.txtPercDespesa.Text, 1, Len(.txtPercDespesa.Text) - 1)
                'End If
                '.txtPercDespesa.Text = Format(CDbl(TrocaValorBrasPorAme(.txtPercDespesa.Text)) + CongenereElemento.Perc_Despesa_Lider, "##0.00") & "%"
            End If
            'Calcula valores p/ cada congenere e soma p/ obter total do cosseguro
            For i = 1 To .gridCobrancaCosseguro.Rows - 1
                If .gridCobrancaCosseguro.TextMatrix(i, 1) = .txtNumParcelas.Text Then
                    vValPremioParc = vPremioTarifaUltParc
                    vValDescontoParc = vTotDescontoUltParcela
                    vValJurosParc = vTotJurosUltParcela
                    vValComissaoParc = vTotComissaoUltParcela
                    If CongenereElemento.Perc_Despesa_Lider <> 0 Then
                        vValDespParc = ((vPremioTarifaUltParc * CongenereElemento.Perc_Despesa_Lider) / 100)
                    Else
                        vValDespParc = 0
                    End If
                    vTotalLiquido = vPremioTarifaUltParc - vTotDescontoUltParcela + vTotJurosUltParcela - vTotComissaoUltParcela - vValDespParc
                Else
                    vValPremioParc = vPremioTarifaParc
                    vValDescontoParc = vTotDescontoParc
                    vValJurosParc = vTotJurosParcela
                    vValComissaoParc = vTotComissaoParcela
                    If CongenereElemento.Perc_Despesa_Lider <> 0 Then
                        vValDespParc = ((vPremioTarifaParc * CongenereElemento.Perc_Despesa_Lider) / 100)
                    Else
                        vValDespParc = 0
                    End If
                    vTotalLiquido = vPremioTarifaParc - vTotDescontoParc + vTotJurosParcela - vTotComissaoParcela - vValDespParc
                End If


                If ConfiguracaoBrasil Then
                    .gridCobrancaCosseguro.Row = i
                    .GridCobranca.Row = i
                    ' Valor Pago
                    .gridCobrancaCosseguro.col = 10: .GridCobranca.col = 6
                    vValorTemp = Format(CDbl(.GridCobranca.Text) * CongenereElemento.Perc_Participacao / 100, "############0.00")
                    .gridCobrancaCosseguro.Text = Format(CDbl(.gridCobrancaCosseguro.Text) + CDbl(vValorTemp), "###,###,###,##0.00")
                    vValPremioParc = CDbl(.gridCobrancaCosseguro.TextMatrix(i, 3)) + vValPremioParc
                    vValDescontoParc = CDbl(.gridCobrancaCosseguro.TextMatrix(i, 4)) + vValDescontoParc
                    vValJurosParc = CDbl(.gridCobrancaCosseguro.TextMatrix(i, 5)) + vValJurosParc
                    vValComissaoParc = CDbl(.gridCobrancaCosseguro.TextMatrix(i, 6)) + vValComissaoParc
                    vValDespParc = CDbl(.gridCobrancaCosseguro.TextMatrix(i, 7)) + vValDespParc
                    vTotalLiquido = CDbl(.gridCobrancaCosseguro.TextMatrix(i, 8)) + vTotalLiquido

                    .gridCobrancaCosseguro.TextMatrix(i, 3) = Format(vValPremioParc, "###,###,###,##0.00")
                    .gridCobrancaCosseguro.TextMatrix(i, 4) = Format(vValDescontoParc, "###,###,###,##0.00")
                    .gridCobrancaCosseguro.TextMatrix(i, 5) = Format(vValJurosParc, "###,###,###,##0.00")
                    .gridCobrancaCosseguro.TextMatrix(i, 6) = Format(vValComissaoParc, "###,###,###,##0.00")
                    .gridCobrancaCosseguro.TextMatrix(i, 7) = Format(vValDespParc, "###,###,###,##0.00")
                    .gridCobrancaCosseguro.TextMatrix(i, 8) = Format(vTotalLiquido, "###,###,###,##0.00")
                Else
                    .gridCobrancaCosseguro.Row = i
                    .GridCobranca.Row = i
                    ' Valor Pago
                    .gridCobrancaCosseguro.col = 10: .GridCobranca.col = 6
                    vValorTemp = Format(CDbl(.GridCobranca.Text) * CongenereElemento.Perc_Participacao / 100, "############0.00")
                    .gridCobrancaCosseguro.Text = Format(CDbl(.gridCobrancaCosseguro.Text) + CDbl(vValorTemp), "###,###,###,##0.00")
                    vValPremioParc = CDbl(TrocaValorAmePorBras(.gridCobrancaCosseguro.TextMatrix(i, 3))) + vValPremioParc
                    vValDescontoParc = CDbl(TrocaValorAmePorBras(.gridCobrancaCosseguro.TextMatrix(i, 4))) + vValDescontoParc
                    vValJurosParc = CDbl(TrocaValorAmePorBras(.gridCobrancaCosseguro.TextMatrix(i, 5))) + vValJurosParc
                    vValComissaoParc = CDbl((TrocaValorAmePorBras(.gridCobrancaCosseguro.TextMatrix(i, 6)))) + vValComissaoParc
                    vValDespParc = CDbl((TrocaValorAmePorBras(.gridCobrancaCosseguro.TextMatrix(i, 7)))) + vValDespParc
                    vTotalLiquido = CDbl((TrocaValorAmePorBras(.gridCobrancaCosseguro.TextMatrix(i, 8)))) + vTotalLiquido
                    .gridCobrancaCosseguro.TextMatrix(i, 3) = Format(TrocaValorAmePorBras(vValPremioParc), "###,###,###,##0.00")
                    .gridCobrancaCosseguro.TextMatrix(i, 4) = Format(TrocaValorAmePorBras(vValDescontoParc), "###,###,###,##0.00")
                    .gridCobrancaCosseguro.TextMatrix(i, 5) = Format(TrocaValorAmePorBras(vValJurosParc), "###,###,###,##0.00")
                    .gridCobrancaCosseguro.TextMatrix(i, 6) = Format(TrocaValorAmePorBras(vValComissaoParc), "###,###,###,##0.00")
                    .gridCobrancaCosseguro.TextMatrix(i, 7) = Format(TrocaValorAmePorBras(vValDespParc), "###,###,###,##0.00")
                    .gridCobrancaCosseguro.TextMatrix(i, 8) = Format(TrocaValorAmePorBras(vTotalLiquido), "###,###,###,##0.00")
                End If
            Next i
        Next
    End With

End Sub

Public Sub Monta_GridCosseguroNossaParte(ByRef vAuxComissao, FormAtivo As Form)

    Dim vValPremioParc As String, vValComissaoParc As String
    Dim vValJurosParc As String, vValDescontoParc As String
    Dim i As Integer, CongenereElemento As Congenere
    Dim vValorTemp As Double

    With FormAtivo

        .txtPercDespesa.Text = "0,00%"

        For Each CongenereElemento In Congeneres
            ' Calcula novos valores, subtraindo do total e truncando na segunda casa decimal
            Call Calcula_ValoresCosseguro(CongenereElemento.Perc_Participacao, CongenereElemento.Perc_Despesa_Lider, vAuxComissao, FormAtivo)
            If ConfiguracaoBrasil Then
                ' Percentual de Participa��o
                If Right(.txtPercParticipacao.Text, 1) = "%" Then
                    .txtPercParticipacao.Text = Mid(.txtPercParticipacao.Text, 1, Len(.txtPercParticipacao.Text) - 1)
                End If
                '                .txtPercParticipacao.Text = Format(CDbl(.txtPercParticipacao.Text) - CongenereElemento.Perc_Participacao, "##0.00")
            Else
                ' Percentual de Participa��o
                If Right(.txtPercParticipacao.Text, 1) = "%" Then
                    .txtPercParticipacao.Text = Mid(.txtPercParticipacao.Text, 1, Len(.txtPercParticipacao.Text) - 1)
                End If
                .txtPercParticipacao.Text = Format(CDbl(TrocaValorBrasPorAme(.txtPercParticipacao.Text)) - CongenereElemento.Perc_Participacao, "##0.00")
            End If


            For i = 1 To .gridCobrancaCosseguro.Rows - 1
                If .gridCobrancaCosseguro.TextMatrix(i, 1) = .txtNumParcelas.Text Then
                    vValPremioParc = vPremioTarifaUltParc
                    vValDescontoParc = vTotDescontoUltParcela
                    vValJurosParc = vTotJurosUltParcela
                    vValComissaoParc = vTotComissaoUltParcela
                Else
                    vValPremioParc = vPremioTarifaParc
                    vValDescontoParc = vTotDescontoParc
                    vValJurosParc = vTotJurosParcela
                    vValComissaoParc = vTotComissaoParcela
                End If
                If ConfiguracaoBrasil Then
                    .gridCobrancaCosseguro.Row = i
                    .GridCobranca.Row = i

                    ' Valor Pago
                    .gridCobrancaCosseguro.col = 10: .GridCobranca.col = 6
                    vValorTemp = Format(CDbl(.GridCobranca.Text) * CongenereElemento.Perc_Participacao / 100, "############0.00")
                    .gridCobrancaCosseguro.Text = Format(CDbl(.gridCobrancaCosseguro.Text) - CDbl(vValorTemp), "###,###,###,##0.00")
                    vValPremioParc = CDbl(.gridCobrancaCosseguro.TextMatrix(i, 3)) - vValPremioParc
                    vValDescontoParc = CDbl(.gridCobrancaCosseguro.TextMatrix(i, 4)) - vValDescontoParc
                    vValJurosParc = CDbl(.gridCobrancaCosseguro.TextMatrix(i, 5)) - vValJurosParc
                    vValComissaoParc = CDbl(.gridCobrancaCosseguro.TextMatrix(i, 6)) - vValComissaoParc

                    .gridCobrancaCosseguro.TextMatrix(i, 3) = Format(vValPremioParc, "###,###,###,##0.00")
                    .gridCobrancaCosseguro.TextMatrix(i, 4) = Format(vValDescontoParc, "###,###,###,##0.00")
                    .gridCobrancaCosseguro.TextMatrix(i, 5) = Format(vValJurosParc, "###,###,###,##0.00")
                    .gridCobrancaCosseguro.TextMatrix(i, 6) = Format(vValComissaoParc, "###,###,###,##0.00")
                    .gridCobrancaCosseguro.TextMatrix(i, 7) = Format(0, "###,###,###,##0.00")
                    'Total L�quido (verificar como ficar� p/ nossa parte)
                    .gridCobrancaCosseguro.TextMatrix(i, 8) = Format(0, "###,###,###,##0.00")
                Else
                    .gridCobrancaCosseguro.Row = i
                    .GridCobranca.Row = i
                    ' Valor Pago
                    .gridCobrancaCosseguro.col = 10: .GridCobranca.col = 6
                    vValorTemp = Format(CDbl(.GridCobranca.Text) * CongenereElemento.Perc_Participacao / 100, "############0.00")
                    .gridCobrancaCosseguro.Text = Format(CDbl(.gridCobrancaCosseguro.Text) - CDbl(vValorTemp), "###,###,###,##0.00")
                    vValPremioParc = CDbl(TrocaValorAmePorBras(.gridCobrancaCosseguro.TextMatrix(i, 3))) - vValPremioParc
                    vValDescontoParc = CDbl(TrocaValorAmePorBras(.gridCobrancaCosseguro.TextMatrix(i, 4))) - vValDescontoParc
                    vValJurosParc = CDbl(TrocaValorAmePorBras(.gridCobrancaCosseguro.TextMatrix(i, 5))) - vValJurosParc
                    vValComissaoParc = CDbl((.gridCobrancaCosseguro.TextMatrix(i, 6))) - vValComissaoParc
                    .gridCobrancaCosseguro.TextMatrix(i, 3) = Format(TrocaValorAmePorBras(vValPremioParc), "###,###,###,##0.00")
                    .gridCobrancaCosseguro.TextMatrix(i, 4) = Format(TrocaValorAmePorBras(vValDescontoParc), "###,###,###,##0.00")
                    .gridCobrancaCosseguro.TextMatrix(i, 5) = Format(TrocaValorAmePorBras(vValJurosParc), "###,###,###,##0.00")
                    .gridCobrancaCosseguro.TextMatrix(i, 6) = Format(TrocaValorAmePorBras(vValComissaoParc), "###,###,###,##0.00")
                    .gridCobrancaCosseguro.TextMatrix(i, 7) = Format(TrocaValorAmePorBras(0), "###,###,###,##0.00")
                    .gridCobrancaCosseguro.TextMatrix(i, 8) = Format(TrocaValorAmePorBras(0), "###,###,###,##0.00")
                    'Total L�quido (verificar como ficar� p/ nossa parte)
                    .gridCobrancaCosseguro.TextMatrix(i, 8) = Format(TrocaValorAmePorBras(0), "###,###,###,##0.00")
                End If
            Next i
        Next
    End With

End Sub

Public Sub Monta_GridCosseguroCongenere(ByRef vAuxComissao, FormAtivo As Form)

    Dim vValPremioParc As String, vValComissaoParc As String, vValJurosParc As String
    Dim vValDescontoParc As String, vValDespParc As String, vTotalLiquido As Double
    Dim vCodSusep As String, vDesc As Double
    Dim i As Integer, vValorTemp As Double

    With FormAtivo
        vCodSusep = .cmbCongenere.ItemData(.cmbCongenere.ListIndex)
        'For Each Congeneres(vCodSusep) In Congeneres
        Call Calcula_ValoresCosseguro(Congeneres(vCodSusep).Perc_Participacao, Congeneres(vCodSusep).Perc_Despesa_Lider, vAuxComissao, FormAtivo)

        If ConfiguracaoBrasil Then
            .txtPercParticipacao.Text = Format(Congeneres(vCodSusep).Perc_Participacao, "##0.00")
            .txtPercDespesa.Text = Format(Congeneres(vCodSusep).Perc_Despesa_Lider, "##0.00") & "%"
        Else
            .txtPercParticipacao.Text = TrocaValorAmePorBras(Format(Congeneres(vCodSusep).Perc_Participacao, "##0.00"))
            .txtPercDespesa.Text = TrocaValorAmePorBras(Format(Congeneres(vCodSusep).Perc_Despesa_Lider, "##0.00")) & "%"
        End If

        For i = 1 To .gridCobrancaCosseguro.Rows - 1
            If .gridCobrancaCosseguro.TextMatrix(i, 1) = .txtNumParcelas.Text Then
                vValPremioParc = vPremioTarifaUltParc
                vValDescontoParc = vTotDescontoUltParcela
                vValJurosParc = vTotJurosUltParcela
                vValComissaoParc = vTotComissaoUltParcela
                'valor despesa � calculado sobre premio tarifa - descontos
                If CDbl(Congeneres(vCodSusep).Perc_Despesa_Lider) <> 0 Then
                    If ConfiguracaoBrasil Then
                        vDesc = CDbl(.gridCobrancaCosseguro.TextMatrix(1, 4))
                    Else
                        vDesc = CDbl(TrocaValorBrasPorAme(.gridCobrancaCosseguro.TextMatrix(1, 4)))
                    End If
                    vValDespParc = Trunca(((vPremioTarifaUltParc - vDesc) * Congeneres(vCodSusep).Perc_Despesa_Lider) / 100)
                Else
                    vValDespParc = 0
                End If
                vTotalLiquido = vPremioTarifaUltParc - vTotDescontoUltParcela + vTotJurosUltParcela - vTotComissaoUltParcela - vValDespParc
            Else
                vValPremioParc = vPremioTarifaParc
                vValDescontoParc = vTotDescontoParc
                vValJurosParc = vTotJurosParcela
                vValComissaoParc = vTotComissaoParcela
                'valor despesa � calculado sobre premio tarifa - descontos
                If CDbl(Congeneres(vCodSusep).Perc_Despesa_Lider) <> 0 Then
                    If ConfiguracaoBrasil Then
                        vDesc = CDbl(.gridCobrancaCosseguro.TextMatrix(1, 4))
                    Else
                        vDesc = CDbl(TrocaValorBrasPorAme(.gridCobrancaCosseguro.TextMatrix(1, 4)))
                    End If
                    vValDespParc = Trunca(((vPremioTarifaParc - vDesc) * Congeneres(vCodSusep).Perc_Despesa_Lider) / 100)
                Else
                    vValDespParc = 0
                End If
                vTotalLiquido = vPremioTarifaParc - vTotDescontoParc + vTotJurosParcela - vTotComissaoParcela - vValDespParc
            End If
            If ConfiguracaoBrasil Then
                .gridCobrancaCosseguro.Row = i
                .GridCobranca.Row = i

                ' Valor Pago
                .gridCobrancaCosseguro.col = 10: .GridCobranca.col = 6
                vValorTemp = Format(CDbl(.GridCobranca.Text) * Congeneres(vCodSusep).Perc_Participacao / 100, "############0.00")
                .gridCobrancaCosseguro.Text = Format(CDbl(.gridCobrancaCosseguro.Text) - CDbl(vValorTemp), "###,###,###,##0.00")


                .gridCobrancaCosseguro.TextMatrix(i, 3) = Format(vValPremioParc, "###,###,###,##0.00")
                .gridCobrancaCosseguro.TextMatrix(i, 4) = Format(vValDescontoParc, "###,###,###,##0.00")
                .gridCobrancaCosseguro.TextMatrix(i, 5) = Format(vValJurosParc, "###,###,###,##0.00")
                .gridCobrancaCosseguro.TextMatrix(i, 6) = Format(vValComissaoParc, "###,###,###,##0.00")
                .gridCobrancaCosseguro.TextMatrix(i, 7) = Format(vValDespParc, "###,###,###,##0.00")
                .gridCobrancaCosseguro.TextMatrix(i, 8) = Format(vTotalLiquido, "###,###,###,##0.00")
            Else
                .gridCobrancaCosseguro.Row = i
                .GridCobranca.Row = i
                ' Valor Pago
                .gridCobrancaCosseguro.col = 10: .GridCobranca.col = 6
                vValorTemp = Format(CDbl(TrocaValorBrasPorAme(.GridCobranca.Text)) * Congeneres(vCodSusep).Perc_Participacao / 100, "############0.00")
                .gridCobrancaCosseguro.Text = Format(CDbl(TrocaValorBrasPorAme(.gridCobrancaCosseguro.Text)) - CDbl(TrocaValorBrasPorAme(vValorTemp)), "###,###,###,##0.00")


                .gridCobrancaCosseguro.TextMatrix(i, 3) = Format(TrocaValorAmePorBras(vValPremioParc), "###,###,###,##0.00")
                .gridCobrancaCosseguro.TextMatrix(i, 4) = Format(TrocaValorAmePorBras(vValDescontoParc), "###,###,###,##0.00")
                .gridCobrancaCosseguro.TextMatrix(i, 5) = Format(TrocaValorAmePorBras(vValJurosParc), "###,###,###,##0.00")
                .gridCobrancaCosseguro.TextMatrix(i, 6) = Format(TrocaValorAmePorBras(vValComissaoParc), "###,###,###,##0.00")
                .gridCobrancaCosseguro.TextMatrix(i, 7) = Format(TrocaValorAmePorBras(vValDespParc), "###,###,###,##0.00")
                .gridCobrancaCosseguro.TextMatrix(i, 8) = Format(TrocaValorAmePorBras(vTotalLiquido), "###,###,###,##0.00")
            End If
        Next i
        'Next
    End With

End Sub

Public Sub Copia_Cobrancas(ByVal FormAtivo As Form)

    Dim temp1 As Integer, temp2 As Integer, vPremioTarifCongenere As Double
    Dim vPremioTarifaParc As Double, vPremioTarifaUltParc As Double, vTemp As Double
    Dim vTotDescontoParc As Double, vTotDescontoUltParcela As Double, vAuxComissao As Double
    Dim vTotJurosParcela As Double, vTotJurosUltParcela As Double
    Dim vTotComissaoParcela As Double, vTotComissaoUltParcela As Double
    Dim vValPremioParc As String, vValComissaoParc As String, vValJurosParc As String, vValDescontoParc As String
    Dim vValComissao As Double, linha As String, i As Integer, vPremioLiqDesc As Double

    With FormAtivo
        ' Comiss�o
        vAuxComissao = 0
        If .gridCorretor.Rows > 1 Then
            For i = 1 To .gridCorretor.Rows - 1
                If .gridCorretor.TextMatrix(i, 1) <> "0,00" Then
                    vAuxComissao = vAuxComissao + CDbl(.gridCorretor.TextMatrix(i, 1))
                End If
            Next i
        End If

        'Premio tarifario por parcela
        If ConfiguracaoBrasil Then
            'Valores ser�o calculados sobre o pr�mio l�quido - descontos comerciais
            vPremioLiqDesc = CDbl(.txtTotalPremioLiq.Text) - CDbl(.txtTotalDescontos.Text)

            '       vPremioTarifaParc = Trunca(CDbl((vPremioLiqDesc)) / .txtNumParcelas.Text)
            'Premio tarifario �ltima parcela
            vPremioTarifaUltParc = vPremioTarifaParc * (.txtNumParcelas.Text - 1)
            vPremioTarifaUltParc = CDbl((vPremioLiqDesc)) - vPremioTarifaUltParc

            'Total de descontos por parcela
            '       vTotDescontoParc = Trunca(CDbl((.txtTotalDescontos.Text)) / .txtNumParcelas.Text)
            'Total de descontos �lt. parcela
            vTotDescontoUltParcela = vTotDescontoParc * (.txtNumParcelas.Text - 1)
            vTotDescontoUltParcela = CDbl((.txtTotalDescontos)) - vTotDescontoUltParcela

            'Total de juros por parcela
            '       vTotJurosParcela = Trunca(CDbl((.txtValJuros.Text)) / .txtNumParcelas.Text)
            'Total de juros �lt. parcela
            vTotJurosUltParcela = vTotJurosParcela * (.txtNumParcelas.Text - 1)
            vTotJurosUltParcela = CDbl((.txtValJuros.Text)) - vTotJurosUltParcela

            'Total de comiss�o por parcela
            vValComissao = CDbl((vPremioLiqDesc)) * vAuxComissao / 100
            '       vTotComissaoParcela = Trunca(vValComissao / .txtNumParcelas.Text)
            'Total de juros �lt. parcela
            vTotComissaoUltParcela = vTotComissaoParcela * (.txtNumParcelas.Text - 1)
            vTotComissaoUltParcela = (vPremioLiqDesc * vAuxComissao / 100) - vTotComissaoUltParcela

        Else
            'Valores ser�o calculados sobre o pr�mio l�quido - descontos comerciais
            vPremioLiqDesc = CDbl(TrocaValorBrasPorAme(.txtTotalPremioLiq.Text)) - CDbl(TrocaValorBrasPorAme(.txtTotalDescontos.Text))

            vPremioTarifaParc = Trunca(CDbl(TrocaValorBrasPorAme(vPremioLiqDesc)) / .txtNumParcelas.Text)
            'Premio tarifario �ltima parcela
            vPremioTarifaUltParc = vPremioTarifaParc * (.txtNumParcelas.Text - 1)
            vPremioTarifaUltParc = CDbl(TrocaValorBrasPorAme(vPremioLiqDesc)) - vPremioTarifaUltParc

            'Total de descontos por parcela
            vTotDescontoParc = Trunca(CDbl(TrocaValorBrasPorAme(.txtTotalDescontos.Text)) / .txtNumParcelas.Text)
            'Total de descontos �lt. parcela
            vTotDescontoUltParcela = vTotDescontoParc * (.txtNumParcelas.Text - 1)
            vTotDescontoUltParcela = CDbl(TrocaValorBrasPorAme(.txtTotalDescontos)) - vTotDescontoUltParcela

            'Total de juros por parcela
            vTotJurosParcela = Trunca(CDbl(TrocaValorBrasPorAme(.txtValJuros.Text)) / .txtNumParcelas.Text)
            'Total de juros �lt. parcela
            vTotJurosUltParcela = vTotJurosParcela * (.txtNumParcelas.Text - 1)
            vTotJurosUltParcela = CDbl(TrocaValorBrasPorAme(.txtValJuros.Text)) - vTotJurosUltParcela

            vValComissao = CDbl(TrocaValorBrasPorAme(vPremioLiqDesc)) * vAuxComissao / 100
            'Total de comiss�o por parcela
            vTotComissaoParcela = Trunca(vValComissao / .txtNumParcelas.Text)
            'Total de juros �lt. parcela
            vTotComissaoUltParcela = vTotComissaoParcela * (.txtNumParcelas.Text - 1)
            vTotComissaoUltParcela = (CDbl(TrocaValorBrasPorAme(vPremioLiqDesc)) * vAuxComissao / 100) - vTotComissaoUltParcela
        End If
        .gridCobrancaCosseguro.Rows = 1
        For temp1 = 1 To .GridCobranca.Rows - 1
            .GridCobranca.Row = temp1
            For temp2 = 0 To 6    '.GridCobranca.Cols - 1
                'Pulando a coluna valor cobrado
                If temp2 <> 3 Then
                    .GridCobranca.col = temp2
                    If temp2 = 0 Then
                        linha = .GridCobranca.Text & vbTab
                    ElseIf temp2 = 4 Then    'col 4 = val iof
                        If .GridCobranca.Rows - 1 = temp1 Then
                            vValPremioParc = vPremioTarifaUltParc
                            vValDescontoParc = vTotDescontoUltParcela
                            vValJurosParc = vTotJurosUltParcela
                            vValComissaoParc = vTotComissaoUltParcela
                        Else
                            vValPremioParc = vPremioTarifaParc
                            vValDescontoParc = vTotDescontoParc
                            vValJurosParc = vTotJurosParcela
                            vValComissaoParc = vTotComissaoParcela
                        End If
                        If ConfiguracaoBrasil Then
                            linha = linha & Format(vValPremioParc, "###,###,###,##0.00") & vbTab
                            linha = linha & Format(vValDescontoParc, "###,###,###,##0.00") & vbTab
                            linha = linha & Format(vValJurosParc, "###,###,###,##0.00") & vbTab
                            linha = linha & Format(vValComissaoParc, "###,###,###,##0.00") & vbTab
                            linha = linha & Format(0, "###,###,###,##0.00") & vbTab
                            linha = linha & Format(0, "###,###,###,##0.00") & vbTab
                        Else
                            linha = linha & TrocaValorAmePorBras(Format(vValPremioParc, "###,###,###,##0.00")) & vbTab
                            linha = linha & TrocaValorAmePorBras(Format(vValDescontoParc, "###,###,###,##0.00")) & vbTab
                            linha = linha & TrocaValorAmePorBras(Format(vValJurosParc, "###,###,###,##0.00")) & vbTab
                            linha = linha & TrocaValorAmePorBras(Format(vValComissaoParc, "###,###,###,##0.00")) & vbTab
                            linha = linha & TrocaValorAmePorBras(Format(0, "###,###,###,##0.00")) & vbTab
                            linha = linha & TrocaValorAmePorBras(Format(0, "###,###,###,##0.00")) & vbTab
                        End If
                        'Linha = Linha & vbTab & vbTab & vbTab & vbTab
                    ElseIf temp2 < .GridCobranca.Cols - 1 Then
                        linha = linha & .GridCobranca.Text & vbTab
                    Else
                        linha = linha & .GridCobranca.Text
                    End If
                End If
            Next temp2
            .gridCobrancaCosseguro.AddItem linha
        Next temp1
    End With
End Sub


Sub Conexao_Auxiliar()

    On Error GoTo Erro
    With rdoCn1
        .Connect = rdocn.Connect
        .CursorDriver = rdUseServer
        .QueryTimeout = 3600
        .EstablishConnection rdDriverNoPrompt
    End With

    With rdoCn2
        .Connect = rdocn.Connect
        .CursorDriver = rdUseServer
        .QueryTimeout = 3600
        .EstablishConnection rdDriverNoPrompt
    End With

    With adoCn
        .ConnectionString = rdocn.Connect
        .Open
    End With

    Exit Sub

Erro:
    mensagem_erro 6, "Conex�o (Auxiliar) com BD indispon�vel."
    End

End Sub

Sub desmonta_colecao()
    Dim i As Long    'RRAMOS - 2007/06/26 - Flow 220222 - Esta vari�vel era do tipo Integer e foi trocada para Long.
'
    For i = SubGrupos.Count To 1 Step -1
        SubGrupos.Remove i
    Next
    Set SubGrupos = Nothing
    '
    For i = Coberturas.Count To 1 Step -1
        Coberturas.Remove i
    Next
    Set Coberturas = Nothing
    '
    For i = Congeneres.Count To 1 Step -1
        Congeneres.Remove i
    Next
    Set Congeneres = Nothing
    '
    For i = Clausulas.Count To 1 Step -1
        Clausulas.Remove i
    Next
    Set Clausulas = Nothing
    '
    For i = Corretores.Count To 1 Step -1
        Corretores.Remove i
    Next
    Set Corretores = Nothing
    '
    For i = Componentes.Count To 1 Step -1
        Componentes.Remove i
    Next
    Set Componentes = Nothing
    '
    For i = Estipulantes.Count To 1 Step -1
        Estipulantes.Remove i
    Next
    Set Estipulantes = Nothing
    '
    For i = Administradores.Count To 1 Step -1
        Administradores.Remove i
    Next
    Set Administradores = Nothing
    '
    For i = Faturas.Count To 1 Step -1
        Faturas.Remove i
    Next
    Set Faturas = Nothing
    For i = Faturas.Count To 1 Step -1
        Faturas.Remove i
    Next
    Set Faturas = Nothing

    Set Vidas_Seguradas = Nothing
    For i = Vidas_Seguradas.Count To 1 Step -1
        Vidas_Seguradas.Remove i
    Next
    Set Vidas_Seguradas = Nothing

End Sub

Sub Monta_Colecao_Coberturas(ByVal rs As rdoResultset)

    Dim bVerAssistencia As Boolean    'Flow 18334512 - Circular 491

    Dim vCobertura As Cobertura
    Dim vValPremio As String, vValPremioLiq As String, vValPremioTarifa As String, vValLimMinFranquia As String, vValIS As String
    Dim vPercBas As String, vClasseCob As String
    Dim vFatTaxa As String, Indice As String, vValFranquia As String
    Dim vDescricao As String
    Dim vDesconto As String, vPercFranquia As String, vTxtFranquia As String

    On Error GoTo Erro
    bVerAssistencia = False

    While Not rs.EOF
        If ConfiguracaoBrasil Then
            vValIS = Format(Val(rs!val_is), "###,###,###,##0.00")
            vDesconto = Format(0, "##0.00000")
            vValPremio = Format(Val(rs!val_Premio), "###,###,###,##0.00")


            'INICIO Flow 18334512 - Circular 491 - 19/02/2015 inclus�o do vValPremioLiq e vValPremioTarifa
            If IsNull(rs!val_premio_liq) Then
                If IsNull(rs!val_premio_tarifa) Then
                    vValPremioLiq = "0,00"
                    vValPremioTarifa = "0,00"
                Else
                    vValPremioTarifa = Format(Val(rs!val_premio_tarifa), "#,##0.00")
                    vValPremioLiq = vValPremioTarifa
                    bVerAssistencia = True
                End If
            Else
                vValPremioLiq = Format(Val(rs!val_premio_liq), "#,##0.00")
                vValPremioTarifa = Format(Val(rs!val_premio_tarifa), "#,##0.00")
            End If
            'FIM    Flow 18334512 - Circular 491 - 19/02/2015 inclus�o do vValPremioLiq e vValPremioTarifa


            vFatTaxa = Format(Val(rs!val_taxa), "0.0000000")
            vPercBas = Format(Val(rs!perc_basica), "#0.00000")
        Else
            vDesconto = TrocaValorAmePorBras(Format(0, "##0.00000"))
            vValPremio = TrocaValorAmePorBras(Format(Val(rs!val_Premio), "###,###,###,##0.00"))

            'INICIO Flow 18334512 - Circular 491 - 19/02/2015 inclus�o do vValPremioLiq e vValPremioTarifa
            If IsNull(rs!val_premio_liq) Then
                If IsNull(rs!val_premio_tarifa) Then
                    vValPremioLiq = "0,00"
                    vValPremioTarifa = "0,00"
                Else
                    vValPremioTarifa = TrocaValorAmePorBras(Format(Val(rs!val_premio_tarifa), "#,##0.00"))
                    vValPremioLiq = vValPremioTarifa
                    bVerAssistencia = True
                End If
            Else
                vValPremioLiq = TrocaValorAmePorBras(Format(Val(rs!val_premio_liq), "#,##0.00"))
                vValPremioTarifa = TrocaValorAmePorBras(Format(Val(rs!val_premio_tarifa), "#,##0.00"))
            End If
            'FIM    Flow 18334512 - Circular 491 - 19/02/2015 inclus�o do vValPremioLiq e vValPremioTarifa

            vValIS = TrocaValorAmePorBras(Format(Val(rs!val_is), "###,###,###,##0.00"))
            vFatTaxa = TrocaValorAmePorBras(Format(Val(rs!val_taxa), "0.0000000"))
            vPercBas = TrocaValorAmePorBras(Format(Val(rs!perc_basica), "#0.00000"))
        End If
        vClasseCob = IIf(IsNull(rs!class_tp_cobertura), "A", UCase(rs!class_tp_cobertura))
        '
        vPercFranquia = 0
        vValFranquia = 0
        vTxtFranquia = ""
        '
        Set vCobertura = New Cobertura
        With vCobertura
            .CodObjSegurado = rs!tp_componente_id
            .TpCoberturaId = rs!tp_cob_comp_id
            .Descricao = Trim(rs!Nome)
            .ValIS = vValIS
            .ValTaxa = vFatTaxa
            .PercDesconto = vDesconto
            .PercBasica = vPercBas
            .ValPremio = vValPremio
            .ValPremioLiq = vValPremioLiq
            .ValPremioTarifa = vValPremioTarifa
            .Classe = vClasseCob
            .DescFranquia = vDescricao
            .ValFranquia = Format$(vValFranquia, "0.00")
            .PercFranquia = Format$(vPercFranquia, "0.00")
            .DescFranquia = vTxtFranquia
            .DtInicioVigencia = Format$(rs!dt_inicio_vigencia_esc, "dd/mm/yyyy")
            If Not IsNull(rs!dt_fim_vigencia_esc) Then
                .DtFimVigencia = Format$(rs!dt_fim_vigencia_esc, "dd/mm/yyyy")
            Else
                .DtFimVigencia = ""
            End If
        End With
        '
        Coberturas.Add vCobertura
        rs.MoveNext
    Wend

    '' ResultSet foi aberto fora da fun��o, fechar fora da fun��o
    'Rs.Close

    'INICIO Flow 18334512 - Circular 491 - 19/02/2015 inclus�o do vValPremioLiq e vValPremioTarifa
    If bVerAssistencia Then
        'casos antigos, campo [val_premio_tarifa_sem_custo_assist_facul] n�o est� preenchido.
        'verificar o valor da assistencia facultativa
        Dim ii As Integer
        Dim fator As Double
        Dim vAcum As Currency, vAjustado As Currency, valAssistFacult As Currency

        valAssistFacult = valor_assistencia_facultativa(vPropostaId, vProdutoId)
        If valAssistFacult > 0 Then
            'tem assistencia facultativa, retirar de valpremioliq de modo proporcional
            vAcum = 0
            For Each vCobertura In Coberturas
                vAcum = vAcum + Val(Replace(Replace(vCobertura.ValPremioTarifa, ".", ""), ",", "."))
            Next

            For Each vCobertura In Coberturas
                fator = Val(Replace(Replace(vCobertura.ValPremioTarifa, ".", ""), ",", ".")) / vAcum
                vAjustado = Val(Replace(Replace(vCobertura.ValPremioLiq, ".", ""), ",", ".")) - (valAssistFacult * fator)
                vCobertura.ValPremioLiq = Format(vAjustado, "#,##0.00")
                If Not ConfiguracaoBrasil Then vCobertura.ValPremioLiq = TrocaValorAmePorBras(vCobertura.ValPremioLiq)
            Next
        End If
    End If



    Exit Sub

Erro:
    TrataErroGeral "Ler Coberturas"
    MsgBox "Rotina: Ler Coberturas"
    'End

End Sub




