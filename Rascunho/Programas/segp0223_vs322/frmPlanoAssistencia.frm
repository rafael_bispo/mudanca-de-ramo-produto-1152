VERSION 5.00
Object = "{1CB70356-FEA2-11D4-87FA-00805F396245}#1.0#0"; "mask2s.ocx"
Begin VB.Form frmPlanoAssistencia 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Form1"
   ClientHeight    =   5220
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   7920
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   ScaleHeight     =   5220
   ScaleWidth      =   7920
   StartUpPosition =   2  'CenterScreen
   Begin Mask2S.ConMask2S txtCusto 
      Height          =   285
      Left            =   180
      TabIndex        =   7
      Top             =   1200
      Width           =   1665
      _ExtentX        =   2937
      _ExtentY        =   503
      mask            =   ""
      text            =   "0,00"
      locked          =   -1  'True
      enabled         =   -1  'True
   End
   Begin VB.CommandButton cmdSair 
      Caption         =   "Sair"
      Height          =   405
      Left            =   6570
      TabIndex        =   6
      Top             =   4710
      Width           =   1095
   End
   Begin VB.TextBox txtLista 
      Height          =   2955
      Left            =   180
      Locked          =   -1  'True
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   5
      Top             =   1620
      Width           =   7515
   End
   Begin VB.TextBox txtTpAssistencia 
      Height          =   285
      Left            =   1830
      Locked          =   -1  'True
      TabIndex        =   4
      Text            =   "Text2"
      Top             =   510
      Width           =   3735
   End
   Begin VB.TextBox txtCodPlano 
      Height          =   285
      Left            =   180
      Locked          =   -1  'True
      TabIndex        =   3
      Text            =   "Text1"
      Top             =   510
      Width           =   1095
   End
   Begin VB.Label Label3 
      Caption         =   "Custo do Plano p/ Vida"
      Height          =   195
      Left            =   180
      TabIndex        =   2
      Top             =   960
      Width           =   2055
   End
   Begin VB.Label Label2 
      Caption         =   "Tipo de Assist�ncia"
      Height          =   225
      Left            =   1830
      TabIndex        =   1
      Top             =   270
      Width           =   1665
   End
   Begin VB.Label Label1 
      Caption         =   "C�digo do Plano"
      Height          =   225
      Left            =   180
      TabIndex        =   0
      Top             =   270
      Width           =   1455
   End
End
Attribute VB_Name = "frmPlanoAssistencia"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub cmdSair_Click()

    Unload Me

End Sub

Private Sub Form_Load()
    On Error GoTo Erro

    Call CentraFrm(Me)
    Me.Caption = "Plano de Assist�ncia"

    Exit Sub

Erro:

    TrataErroGeral "Erro ao carregar a tela de Consulta"
    MsgBox "Erro ao carregar a tela de Consulta"

End Sub
