VERSION 5.00
Begin VB.Form SelImpressora 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Selecionar o Tipo de impressão"
   ClientHeight    =   2460
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   3915
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2460
   ScaleWidth      =   3915
   StartUpPosition =   2  'CenterScreen
   Begin VB.OptionButton Opt3 
      Caption         =   "Direto na impressora c/ Beneficiários"
      Height          =   255
      Left            =   360
      TabIndex        =   4
      Top             =   240
      Width           =   2895
   End
   Begin VB.OptionButton Opt4 
      Caption         =   "Mostra na tela c/ Beneficiários"
      Height          =   255
      Left            =   360
      TabIndex        =   3
      Top             =   960
      Width           =   2775
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Imprimir"
      Default         =   -1  'True
      Height          =   375
      Left            =   1200
      TabIndex        =   2
      Top             =   1920
      Width           =   1216
   End
   Begin VB.OptionButton Opt2 
      Caption         =   "Mostra na tela s/ Beneficiários"
      Height          =   255
      Left            =   360
      TabIndex        =   1
      Top             =   1320
      Width           =   2895
   End
   Begin VB.OptionButton Opt1 
      Caption         =   "Direto na impressora s/ Beneficiários"
      Height          =   255
      Left            =   360
      TabIndex        =   0
      Top             =   600
      Value           =   -1  'True
      Width           =   3015
   End
End
Attribute VB_Name = "SelImpressora"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Command1_Click()
'************************************************
'*      Nova função para impressão de relatório *
'*      Demanda Evolutiva: 5704416              *
'*      Jessé Dias - G&P - Data: 24/01/2010     *
'************************************************
    Dim bln_Beneficiario As Boolean

    If Opt1.Value = True Or Opt3.Value = True Then
        ConPropBasicaVida.ImprimirTela = False
    ElseIf Opt2.Value = True Or Opt4.Value = True Then
        ConPropBasicaVida.ImprimirTela = True
    Else
        ConPropBasicaVida.ImprimirTela = False
        Exit Sub
    End If

    If Opt4.Value = True Or Opt3.Value = True Then
        bln_Beneficiario = True
    End If

    Unload Me
    ConPropBasicaVida.Enabled = True

    ConPropBasicaVida.Impressao_OnLine (bln_Beneficiario)
End Sub

Private Sub Form_Load()

    Call CentraFrm(Me)

End Sub

Private Sub Form_Unload(Cancel As Integer)
    ConPropBasicaVida.Enabled = True
End Sub
