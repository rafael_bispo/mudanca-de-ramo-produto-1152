VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "msflxgrd.ocx"
Begin VB.Form frmConAssistencia 
   Caption         =   "Consulta de Assistência"
   ClientHeight    =   5490
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   9525
   LinkTopic       =   "Form1"
   ScaleHeight     =   5490
   ScaleWidth      =   9525
   StartUpPosition =   1  'CenterOwner
   Begin VB.CommandButton cmdFechar 
      Caption         =   "Fechar"
      Height          =   375
      Left            =   8040
      TabIndex        =   5
      Top             =   4920
      Width           =   1215
   End
   Begin VB.Frame fraAssistencia 
      Caption         =   "Assistências Contratadas"
      Height          =   3015
      Left            =   240
      TabIndex        =   3
      Top             =   1680
      Width           =   9015
      Begin MSFlexGridLib.MSFlexGrid gridAssistencia 
         Height          =   2400
         Left            =   240
         TabIndex        =   4
         Top             =   360
         Width           =   8565
         _ExtentX        =   15108
         _ExtentY        =   4233
         _Version        =   393216
         Rows            =   1
         Cols            =   3
         FixedCols       =   0
         ScrollBars      =   2
         AllowUserResizing=   1
         FormatString    =   $"frmConAssistencia.frx":0000
      End
   End
   Begin VB.Frame fraProposta 
      Height          =   1455
      Left            =   240
      TabIndex        =   0
      Top             =   120
      Width           =   9015
      Begin VB.TextBox txtProduto 
         BackColor       =   &H80000004&
         Height          =   300
         Left            =   1230
         Locked          =   -1  'True
         TabIndex        =   6
         TabStop         =   0   'False
         Top             =   840
         Width           =   6975
      End
      Begin VB.TextBox txtProposta 
         BackColor       =   &H80000004&
         Height          =   300
         Left            =   1230
         Locked          =   -1  'True
         TabIndex        =   1
         TabStop         =   0   'False
         Top             =   400
         Width           =   2055
      End
      Begin VB.Label lblProduto 
         AutoSize        =   -1  'True
         Caption         =   "Produto:"
         Height          =   195
         Left            =   360
         TabIndex        =   7
         Top             =   900
         Width           =   600
      End
      Begin VB.Label lblProposta 
         AutoSize        =   -1  'True
         Caption         =   "Proposta:"
         Height          =   195
         Left            =   360
         TabIndex        =   2
         Top             =   453
         Width           =   675
      End
   End
End
Attribute VB_Name = "frmConAssistencia"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public glPropostaId As Long
Public giProdutoId As Integer
Public gsDtInicioVigencia As String
Public gsProduto As String

Private Sub cmdFechar_Click()

    Call Unload(Me)

End Sub

Private Sub Form_Load()

  If glPropostaId > 0 Then
  
    txtProposta.Text = glPropostaId
    txtProduto.Text = gsProduto

    Call ConsultarAssistencia

  End If

End Sub

Private Sub ConsultarAssistencia()

  Dim sSql As String
  Dim sLinha As String
  Dim rsAssistencia As Recordset

  On Error GoTo TrataErro

  If giProdutoId >= 1000 Then

    sSql = ""
    sSql = sSql & "select t.nome,"
    sSql = sSql & "       case s.obrigatoria"
    sSql = sSql & "         when 'S' then 'OBRIGATÓRIA'"
    sSql = sSql & "         when 'N' then 'FACULTATIVA'"
    sSql = sSql & "       end as tipo,"
    sSql = sSql & "       pa.VL_SRVC_AST_CTR val_assistencia"
    sSql = sSql & "  from seguros_db..proposta_tb p"
    sSql = sSql & " inner join seguros_db..proposta_processo_susep_tb pp"
    sSql = sSql & "    on pp.proposta_id = p.proposta_id"
    sSql = sSql & " inner join als_operacao_db..pln_ast_ctr pa"
    sSql = sSql & "    on pa.CD_PRD = pp.cod_produto"
    sSql = sSql & "   and pa.CD_MDLD = pp.cod_modalidade"
    sSql = sSql & "   and pa.CD_ITEM_MDLD = pp.cod_item_modalidade"
    sSql = sSql & "   and pa.NR_CTR_SGRO = pp.num_contrato_seguro"
    sSql = sSql & "   and pa.NR_VRS_EDS = pp.num_versao_endosso"
    sSql = sSql & " inner join assistencia_uss_db..subramo_tp_assistencia_tb s"
    sSql = sSql & "    on s.produto_id = p.produto_id"
    sSql = sSql & "   and s.plano_assistencia_id = pa.NR_SEQL_PLN_CTR"
    sSql = sSql & "   and s.dt_inicio_vigencia_ass <= '" & gsDtInicioVigencia & "'"
    sSql = sSql & "   and (s.dt_fim_vigencia_ass is null or s.dt_fim_vigencia_ass >= '" & gsDtInicioVigencia & "')"
    sSql = sSql & "   and s.obrigatoria = 'N'"
    sSql = sSql & " inner join assistencia_uss_db..tp_assistencia_tb t"
    sSql = sSql & "    on s.tp_assistencia_id = t.tp_assistencia_id"
    sSql = sSql & " where p.proposta_id = " & glPropostaId
    sSql = sSql & " order by s.obrigatoria desc,"
    sSql = sSql & "          pa.NR_SEQL_PLN_CTR"

  Else

    sSql = ""
    sSql = sSql & "select t.nome,"
    sSql = sSql & "       case s.obrigatoria"
    sSql = sSql & "         when 'S' then 'OBRIGATÓRIA'"
    sSql = sSql & "         when 'N' then 'FACULTATIVA'"
    sSql = sSql & "       end as tipo,"
    sSql = sSql & "       case s.obrigatoria"
    sSql = sSql & "          when 'S' then 0"
    sSql = sSql & "          when 'N' then isnull(a.val_assistencia_facul, 0)"
    sSql = sSql & "       end as val_assistencia"
    sSql = sSql & "  from proposta_tb p"
    sSql = sSql & " inner join proposta_adesao_tb a"
    sSql = sSql & "    on p.proposta_id = a.proposta_id"
    sSql = sSql & " inner join assistencia_uss_db..subramo_tp_assistencia_tb s"
    sSql = sSql & "    on p.produto_id = s.produto_id"
    sSql = sSql & "   and p.ramo_id = s.ramo_id"
    sSql = sSql & "   and p.subramo_id = s.subramo_id"
    sSql = sSql & "   and p.dt_inicio_vigencia_sbr = s.dt_inicio_vigencia_sbr"
    sSql = sSql & "   and s.dt_inicio_vigencia_ass <= '" & gsDtInicioVigencia & "'"
    sSql = sSql & "   and (s.dt_fim_vigencia_ass is null or s.dt_fim_vigencia_ass >= '" & gsDtInicioVigencia & "')"
    sSql = sSql & "   and s.obrigatoria = 'N'"
    sSql = sSql & "   and isnull(a.val_assistencia_facul, 0) > 0"
    sSql = sSql & " inner join assistencia_uss_db..tp_assistencia_tb t"
    sSql = sSql & "    on s.tp_assistencia_id = t.tp_assistencia_id"
    sSql = sSql & " where p.proposta_id = " & glPropostaId
    sSql = sSql & " order by s.obrigatoria desc"
 
  End If

  Set rsAssistencia = ExecutarSQL(gsSIGLASISTEMA, _
                                  glAmbiente_id, _
                                  App.Title, _
                                  App.FileDescription, _
                                  sSql, _
                                  True)

  If Not rsAssistencia.EOF Then

    While Not rsAssistencia.EOF

        sLinha = ""
        sLinha = sLinha & rsAssistencia("nome") & vbTab
        sLinha = sLinha & rsAssistencia("tipo") & vbTab
        sLinha = sLinha & Format(Val(TrocaVirgulaPorPonto(rsAssistencia("val_assistencia"))), "#0.00")

        Call gridAssistencia.AddItem(sLinha)

        rsAssistencia.MoveNext

    Wend

    rsAssistencia.Close
  
  End If

  Set rsAssistencia = Nothing

  Exit Sub

TrataErro:
  Call TratarErro("ObterDadosRemuneracaoServico", Me.name)
  Call FinalizarAplicacao

End Sub
