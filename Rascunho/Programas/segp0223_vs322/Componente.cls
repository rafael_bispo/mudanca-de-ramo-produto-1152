VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Componente"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Option Explicit

'local variable(s) to hold property value(s)
Private mvarCodigo As Long    'local copy
Private mvarNome As String    'local copy
Private mvarCodSubRamo As Long    'local copy
Private mvarDtIniVigSbr As Date    'local copy
Private mvarOperacao As String    'local copy
'local variable(s) to hold property value(s)
Private mvarPersistente As Boolean    'local copy
'local variable(s) to hold property value(s)
Private mvarDtIniSeguro As Date    'local copy
'local variable(s) to hold property value(s)
Private mvarNomeSubRamo As String    'local copy
'local variable(s) to hold property value(s)
Private mvarDtFimVigSbr As Date    'local copy
Public Property Let DtFimVigSbr(ByVal vData As Date)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.DtFimVigSbr = 5
    mvarDtFimVigSbr = vData
End Property


Public Property Get DtFimVigSbr() As Date
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.DtFimVigSbr
    DtFimVigSbr = mvarDtFimVigSbr
End Property



Public Property Let NomeSubRamo(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.NomeSubRamo = 5
    mvarNomeSubRamo = vData
End Property


Public Property Get NomeSubRamo() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.NomeSubRamo
    NomeSubRamo = mvarNomeSubRamo
End Property



Public Property Let DtIniSeguro(ByVal vData As Date)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.DtIniSeguro = 5
    mvarDtIniSeguro = vData
End Property


Public Property Get DtIniSeguro() As Date
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.DtIniSeguro
    DtIniSeguro = mvarDtIniSeguro
End Property



Public Property Let Persistente(ByVal vData As Boolean)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Persistente = 5
    mvarPersistente = vData
End Property


Public Property Get Persistente() As Boolean
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Persistente
    Persistente = mvarPersistente
End Property



Public Property Let Operacao(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Operacao = 5
    mvarOperacao = vData
End Property


Public Property Get Operacao() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Operacao
    Operacao = mvarOperacao
End Property



Public Property Let DtIniVigSbr(ByVal vData As Date)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.DtIniVigSbr = 5
    mvarDtIniVigSbr = vData
End Property


Public Property Get DtIniVigSbr() As Date
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.DtIniVigSbr
    DtIniVigSbr = mvarDtIniVigSbr
End Property



Public Property Let CodSubRamo(ByVal vData As Long)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.CodSubRamo = 5
    mvarCodSubRamo = vData
End Property


Public Property Get CodSubRamo() As Long
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.CodSubRamo
    CodSubRamo = mvarCodSubRamo
End Property



Public Property Let Nome(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Nome = 5
    mvarNome = vData
End Property


Public Property Get Nome() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Nome
    Nome = mvarNome
End Property



Public Property Let Codigo(ByVal vData As Long)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Codigo = 5
    mvarCodigo = vData
End Property


Public Property Get Codigo() As Long
Attribute Codigo.VB_UserMemId = 0
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Codigo
    Codigo = mvarCodigo
End Property



