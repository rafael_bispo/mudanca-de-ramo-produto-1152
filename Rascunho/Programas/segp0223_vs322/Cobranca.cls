VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Cobranca"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Option Explicit

'local variable(s) to hold property value(s)
Private mvarEndossoId As String    'local copy
Private mvarId As String    'local copy
Private mvarValPago As Double    'local copy
Private mvarSituacao As String    'local copy
'local variable(s) to hold property value(s)
Private mvarDtAgendamento As String    'local copy
Private mvarDtPagamento As String    'local copy
Public Property Let DtPagamento(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.DtPgto = 5
    mvarDtPagamento = vData
End Property


Public Property Get DtPagamento() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.DtPgto
    DtPagamento = mvarDtPagamento
End Property



Public Property Let DtAgendamento(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.DtAgend = 5
    mvarDtAgendamento = vData
End Property


Public Property Get DtAgendamento() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.DtAgend
    DtAgendamento = mvarDtAgendamento
End Property



Public Property Let Situacao(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Situacao = 5
    mvarSituacao = vData
End Property


Public Property Get Situacao() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Situacao
    Situacao = mvarSituacao
End Property



Public Property Let ValPago(ByVal vData As Double)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.ValPago = 5
    mvarValPago = vData
End Property


Public Property Get ValPago() As Double
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.ValPago
    ValPago = mvarValPago
End Property
















Public Property Let Id(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Id = 5
    mvarId = vData
End Property


Public Property Get Id() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Id
    Id = mvarId
End Property



Public Property Let EndossoId(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.EndossoId = 5
    mvarEndossoId = vData
End Property


Public Property Get EndossoId() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.EndossoId
    EndossoId = mvarEndossoId
End Property



