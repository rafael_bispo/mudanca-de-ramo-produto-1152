VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form FaturaAgregada 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Form1"
   ClientHeight    =   4440
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6720
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4440
   ScaleWidth      =   6720
   StartUpPosition =   1  'CenterOwner
   Begin VB.CommandButton cmdBoleto 
      Caption         =   "&Boleto"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   3360
      TabIndex        =   7
      Top             =   3960
      Width           =   1335
   End
   Begin VB.CommandButton CmdSair 
      Caption         =   "&Sair"
      Default         =   -1  'True
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   5040
      TabIndex        =   1
      Top             =   3960
      Width           =   1335
   End
   Begin VB.Frame Frame1 
      Caption         =   "Dados da Fatura "
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3255
      Left            =   120
      TabIndex        =   0
      Top             =   480
      Width           =   6495
      Begin MSFlexGridLib.MSFlexGrid flxgFaturas 
         Height          =   2295
         Left            =   120
         TabIndex        =   4
         Top             =   360
         Width           =   6255
         _ExtentX        =   11033
         _ExtentY        =   4048
         _Version        =   393216
         Rows            =   3
         Cols            =   6
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Label Label2 
         Caption         =   "Total de itens:"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   4560
         TabIndex        =   5
         Top             =   2760
         Width           =   1215
      End
      Begin VB.Label lblQtd 
         Alignment       =   1  'Right Justify
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   5640
         TabIndex        =   6
         Top             =   2760
         Width           =   495
      End
   End
   Begin VB.Label Label1 
      Caption         =   "Cobran�a Personalizada n�"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   3720
      TabIndex        =   2
      Top             =   120
      Width           =   2295
   End
   Begin VB.Label lblPersonal_id 
      Alignment       =   1  'Right Justify
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   5880
      TabIndex        =   3
      Top             =   120
      Width           =   495
   End
End
Attribute VB_Name = "FaturaAgregada"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim personalizacao_fatura_id As Long
Dim personalizacao_id As Long

Private Sub flxgFaturasFaturasSetup()

    flxgFaturas.ColWidth(0) = 890
    flxgFaturas.ColWidth(1) = 1300
    flxgFaturas.ColWidth(2) = 590
    flxgFaturas.ColWidth(3) = 1000
    flxgFaturas.ColWidth(4) = 590
    flxgFaturas.ColWidth(5) = 1400

    flxgFaturas.TextMatrix(0, 0) = "Cod.Fatura"
    flxgFaturas.TextMatrix(0, 1) = "Dt.Agendamento"
    flxgFaturas.TextMatrix(0, 2) = "Itens"
    flxgFaturas.TextMatrix(0, 3) = "SubGrupo"
    flxgFaturas.TextMatrix(0, 4) = "Fatura"
    flxgFaturas.TextMatrix(0, 5) = "Valor Cobran�a  "

    flxgFaturas.ColAlignment(0) = flexAlignCenterCenter
    flxgFaturas.ColAlignment(1) = flexAlignCenterCenter
    flxgFaturas.ColAlignment(2) = flexAlignCenterCenter
    flxgFaturas.ColAlignment(3) = flexAlignCenterCenter
    flxgFaturas.ColAlignment(4) = flexAlignCenterCenter
    flxgFaturas.ColAlignment(5) = flexAlignRightCenter

    flxgFaturas.Rows = 2
End Sub

Private Sub cmdSair_Click()
    ConPropBasicaVida.MostraBoleto = False
    Unload Me
End Sub

Private Sub cmdBoleto_Click()
    ConPropBasicaVida.MostraBoleto = True
    Unload Me
End Sub

Private Sub Form_Load()

    flxgFaturasFaturasSetup

    If Trim(vSegCliente) <> "" Then
        Me.Caption = "Fatura Agregada - " & Ambiente & " - " & vSegCliente   'Rmarins - 05/01/2006
    Else
        Me.Caption = "Fatura Agregada - " & Ambiente
    End If

    With ConPropBasicaVida
        personalizacao_fatura_id = .personalizacao_fatura_id
        personalizacao_id = .personalizacao_id
        lblPersonal_id.Caption = personalizacao_id
        .MostraBoleto = False
    End With

    CarregaDadosGrid

End Sub

Private Sub CarregaDadosGrid()
    Dim rs As rdoResultset
    Dim SQL As String
    Dim QTD As Integer
    Dim valor_total As String

    Set rs = Nothing

    'Seleciona as faturas-parcelas da cobranca agregada
    SQL = "select pc.personalizacao_id , pf.personalizacao_fatura_id, pfi.fatura_item, " & _
          "f.sub_grupo_id,a.val_cobranca , a.fatura_id, pf.dt_agendamento, pf.valor_total " & _
          "from fatura_tb f  WITH (NOLOCK) , agendamento_cobranca_tb a  WITH (NOLOCK) , personalizacao_fatura_tb pf  WITH (NOLOCK) , " & _
        " personalizacao_fatura_item_tb pfi  WITH (NOLOCK) , personalizacao_cobranca_tb pc   WITH (NOLOCK) " & _
          "Where pc.apolice_id = a.apolice_id  " & _
        " and pc.ramo_id = a.ramo_id " & _
        " and pc.personalizacao_id = pf.personalizacao_id " & _
        " and pf.personalizacao_id = pfi.personalizacao_id " & _
        " and pf.personalizacao_fatura_id = pfi.personalizacao_fatura_id " & _
        " and a.fatura_id = pfi.fatura_id " & _
        " and f.apolice_id = a.apolice_id " & _
        " and f.ramo_id = a.ramo_id " & _
        " and f.fatura_id = a.fatura_id " & _
        " and f.proposta_id = a.proposta_id" & _
        " and pc.ramo_id = " & vRamoId & _
        " and pc.apolice_id = " & vApoliceId & _
        " and pc.personalizacao_id = " & personalizacao_id & _
        " and pf.personalizacao_fatura_id = " & personalizacao_fatura_id & _
        " order by pfi.fatura_item"

    Set rs = rdocn.OpenResultset(SQL, rdOpenForwardOnly)

    'cabe�alho
    flxgFaturas.TextMatrix(1, 0) = rs!personalizacao_fatura_id
    flxgFaturas.TextMatrix(1, 1) = rs!dt_agendamento
    valor_total = TrocaValorAmePorBras(rs!valor_total)

    QTD = 0

    Do While Not rs.EOF
        flxgFaturas.TextMatrix(flxgFaturas.Rows - 1, 2) = rs!fatura_item
        flxgFaturas.TextMatrix(flxgFaturas.Rows - 1, 3) = rs!sub_grupo_id
        flxgFaturas.TextMatrix(flxgFaturas.Rows - 1, 4) = rs!fatura_id
        flxgFaturas.TextMatrix(flxgFaturas.Rows - 1, 5) = "R$ " & TrocaValorAmePorBras(rs!val_cobranca)
        flxgFaturas.Rows = flxgFaturas.Rows + 1
        rs.MoveNext
        QTD = QTD + 1
    Loop

    'total
    lblQtd.Caption = QTD
    flxgFaturas.TextMatrix(flxgFaturas.Rows - 1, 0) = "Total"
    flxgFaturas.TextMatrix(flxgFaturas.Rows - 1, 5) = "R$ " & valor_total
    rs.Close
End Sub

