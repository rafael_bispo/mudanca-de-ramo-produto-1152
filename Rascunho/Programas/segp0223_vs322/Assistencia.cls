VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Assistencia"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
'local variable(s) to hold property value(s)
Private mvarCodAssistencia As Integer    'local copy
Private mvarDescricaoAssistencia As String    'local copy
Private mvarDtIniAssistSbg As Variant    'local copy
Private mvarDTFimAssistSbg As Variant    'local copy
Private mvarValAssistencia As String    'local copy

Public Property Let ValAssistencia(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.ValAssistencia = 5
    mvarValAssistencia = vData
End Property

Public Property Get ValAssistencia() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.ValAssistencia
    ValAssistencia = mvarValAssistencia
End Property

Public Property Let DtIniAssistSbg(ByVal vData As Variant)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.DtIniAssistSbg = 5
    mvarDtIniAssistSbg = vData
End Property

Public Property Get DtIniAssistSbg() As Variant
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.DtIniAssistSbg
    DtIniAssistSbg = mvarDtIniAssistSbg
End Property

Public Property Let DTFimAssistSbg(ByVal vData As Variant)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.DTFimAssistSbg = 5
    mvarDTFimAssistSbg = vData
End Property


Public Property Get DTFimAssistSbg() As Variant
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.DTFimAssistSbg
    DtIniAssistSbg = mvarDTFimAssistSbg
End Property

Public Property Let DescricaoAssistencia(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.DescricaoAssistencia = 5
    mvarDescricaoAssistencia = vData
End Property

Public Property Get DescricaoAssistencia() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.DescricaoAssistencia
    DescricaoAssistencia = mvarDescricaoAssistencia
End Property

Public Property Let CodAssistencia(ByVal vData As Integer)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.CodAssistencia = 5
    mvarCodAssistencia = vData
End Property

Public Property Get CodAssistencia() As Integer
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.CodAssistencia
    CodAssistencia = mvarCodAssistencia
End Property
