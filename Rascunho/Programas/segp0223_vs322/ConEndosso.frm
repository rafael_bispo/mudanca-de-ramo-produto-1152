VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "tabctl32.ocx"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Object = "{C932BA88-4374-101B-A56C-00AA003668DC}#1.1#0"; "MSMASK32.OCX"
Object = "{00025600-0000-0000-C000-000000000046}#5.2#0"; "crystl32.ocx"
Object = "{3B7C8863-D78F-101B-B9B5-04021C009402}#1.2#0"; "richtx32.ocx"
Begin VB.Form ConEndosso 
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   8655
   ClientLeft      =   1995
   ClientTop       =   1965
   ClientWidth     =   11295
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   ScaleHeight     =   8655
   ScaleWidth      =   11295
   StartUpPosition =   2  'CenterScreen
   Begin Crystal.CrystalReport Report 
      Left            =   3720
      Top             =   7920
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   348160
      PrintFileLinesPerPage=   60
   End
   Begin MSComDlg.CommonDialog dialog 
      Left            =   1800
      Top             =   7800
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.CommandButton cmdRetorna 
      Caption         =   "&Retornar"
      Height          =   375
      Index           =   4
      Left            =   9600
      TabIndex        =   39
      Top             =   7860
      Width           =   1215
   End
   Begin VB.CommandButton cmdImprime 
      Caption         =   "Imprimir"
      Height          =   375
      Index           =   0
      Left            =   8160
      TabIndex        =   38
      Top             =   7860
      Width           =   1215
   End
   Begin TabDlg.SSTab tabEndossos 
      Height          =   7665
      Left            =   0
      TabIndex        =   1
      Top             =   120
      Width           =   11295
      _ExtentX        =   19923
      _ExtentY        =   13520
      _Version        =   393216
      Tabs            =   11
      TabsPerRow      =   6
      TabHeight       =   520
      TabCaption(0)   =   "&Proposta"
      TabPicture(0)   =   "ConEndosso.frx":0000
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "fraEndosso"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).ControlCount=   1
      TabCaption(1)   =   "&Dados Cadastrais"
      TabPicture(1)   =   "ConEndosso.frx":001C
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "fra(8)"
      Tab(1).ControlCount=   1
      TabCaption(2)   =   "&SubGrupos"
      TabPicture(2)   =   "ConEndosso.frx":0038
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "Frame3"
      Tab(2).ControlCount=   1
      TabCaption(3)   =   "Co&rretagem"
      TabPicture(3)   =   "ConEndosso.frx":0054
      Tab(3).ControlEnabled=   0   'False
      Tab(3).Control(0)=   "fraCorretor"
      Tab(3).ControlCount=   1
      TabCaption(4)   =   "&Administra��o"
      TabPicture(4)   =   "ConEndosso.frx":0070
      Tab(4).ControlEnabled=   0   'False
      Tab(4).Control(0)=   "fraAdministracao"
      Tab(4).ControlCount=   1
      TabCaption(5)   =   "Com&ponentes"
      TabPicture(5)   =   "ConEndosso.frx":008C
      Tab(5).ControlEnabled=   0   'False
      Tab(5).Control(0)=   "fraComponentes"
      Tab(5).ControlCount=   1
      TabCaption(6)   =   "&Coberturas"
      TabPicture(6)   =   "ConEndosso.frx":00A8
      Tab(6).ControlEnabled=   0   'False
      Tab(6).Control(0)=   "fraCoberturas"
      Tab(6).ControlCount=   1
      TabCaption(7)   =   "&Benefici�rio"
      TabPicture(7)   =   "ConEndosso.frx":00C4
      Tab(7).ControlEnabled=   0   'False
      Tab(7).Control(0)=   "fraBenef"
      Tab(7).ControlCount=   1
      TabCaption(8)   =   "C&l�usulas"
      TabPicture(8)   =   "ConEndosso.frx":00E0
      Tab(8).ControlEnabled=   0   'False
      Tab(8).Control(0)=   "fra(2)"
      Tab(8).ControlCount=   1
      TabCaption(9)   =   "C&osseguro"
      TabPicture(9)   =   "ConEndosso.frx":00FC
      Tab(9).ControlEnabled=   0   'False
      Tab(9).Control(0)=   "fra(14)"
      Tab(9).ControlCount=   1
      TabCaption(10)  =   "&Vidas Seguradas"
      TabPicture(10)  =   "ConEndosso.frx":0118
      Tab(10).ControlEnabled=   0   'False
      Tab(10).Control(0)=   "Frame7"
      Tab(10).ControlCount=   1
      Begin VB.Frame Frame7 
         Height          =   5925
         Left            =   -74850
         TabIndex        =   148
         Top             =   720
         Width           =   10815
         Begin VB.CommandButton cmdRelatorioVidas 
            Caption         =   "Relat�rio Vidas"
            Height          =   375
            Left            =   9360
            TabIndex        =   150
            Top             =   5280
            Width           =   1215
         End
         Begin MSFlexGridLib.MSFlexGrid msfVidas 
            Height          =   4845
            Left            =   210
            TabIndex        =   149
            Top             =   270
            Width           =   10410
            _ExtentX        =   18362
            _ExtentY        =   8546
            _Version        =   393216
            Rows            =   1
            Cols            =   32
            FixedCols       =   0
            GridColor       =   8421504
            AllowBigSelection=   0   'False
            HighLight       =   0
            AllowUserResizing=   1
            FormatString    =   $"ConEndosso.frx":0134
         End
      End
      Begin VB.Frame fra 
         Height          =   5895
         Index           =   2
         Left            =   -74880
         TabIndex        =   109
         Top             =   780
         Width           =   10815
         Begin VB.Frame fra 
            Caption         =   "Cl�usulas da Ap�lice"
            Height          =   1700
            Index           =   16
            Left            =   120
            TabIndex        =   111
            Top             =   195
            Width           =   10575
            Begin MSFlexGridLib.MSFlexGrid gridClausulas 
               Height          =   1275
               Left            =   240
               TabIndex        =   112
               Top             =   255
               Width           =   10005
               _ExtentX        =   17648
               _ExtentY        =   2249
               _Version        =   393216
               Cols            =   5
               FixedCols       =   0
               SelectionMode   =   1
               FormatString    =   $"ConEndosso.frx":03A6
            End
         End
         Begin VB.Frame fra 
            Caption         =   "Texto da Cl�usula Selecionada"
            Height          =   3735
            Index           =   17
            Left            =   120
            TabIndex        =   110
            Top             =   2000
            Width           =   10575
            Begin RichTextLib.RichTextBox txtClausula 
               Height          =   3375
               Left            =   120
               TabIndex        =   147
               Top             =   240
               Width           =   10335
               _ExtentX        =   18230
               _ExtentY        =   5953
               _Version        =   393217
               ScrollBars      =   2
               TextRTF         =   $"ConEndosso.frx":045A
            End
         End
      End
      Begin VB.Frame fraCoberturas 
         Caption         =   "Coberturas"
         Height          =   5895
         Left            =   -74880
         TabIndex        =   105
         Top             =   780
         Width           =   10815
         Begin VB.ComboBox CboTpComponente 
            Height          =   315
            Left            =   5160
            Style           =   2  'Dropdown List
            TabIndex        =   126
            Top             =   840
            Width           =   3315
         End
         Begin VB.ComboBox CboSubGrupo 
            Height          =   315
            Index           =   3
            Left            =   240
            Style           =   2  'Dropdown List
            TabIndex        =   106
            Top             =   840
            Width           =   3315
         End
         Begin MSFlexGridLib.MSFlexGrid gridCoberturas 
            Height          =   3735
            Left            =   240
            TabIndex        =   107
            Top             =   1470
            Width           =   10365
            _ExtentX        =   18283
            _ExtentY        =   6588
            _Version        =   393216
            Cols            =   10
            AllowBigSelection=   0   'False
            HighLight       =   0
            AllowUserResizing=   1
            FormatString    =   $"ConEndosso.frx":04DC
         End
         Begin VB.Label Label32 
            AutoSize        =   -1  'True
            Caption         =   "Objeto Segurado :"
            Height          =   195
            Left            =   5160
            TabIndex        =   127
            Top             =   540
            Width           =   1290
         End
         Begin VB.Label lblConsEndosso24 
            AutoSize        =   -1  'True
            Caption         =   "SubGrupo :"
            Height          =   195
            Left            =   300
            TabIndex        =   108
            Top             =   540
            Width           =   810
         End
      End
      Begin VB.Frame fraComponentes 
         Caption         =   "Componentes"
         Height          =   5895
         Left            =   -74880
         TabIndex        =   101
         Top             =   780
         Width           =   10815
         Begin VB.ComboBox CboSubGrupo 
            Height          =   315
            Index           =   2
            Left            =   240
            Style           =   2  'Dropdown List
            TabIndex        =   102
            Top             =   840
            Width           =   3315
         End
         Begin MSFlexGridLib.MSFlexGrid GridComponente 
            Height          =   1695
            Left            =   240
            TabIndex        =   103
            Top             =   1470
            Width           =   10215
            _ExtentX        =   18018
            _ExtentY        =   2990
            _Version        =   393216
            Cols            =   5
            FixedCols       =   0
            HighLight       =   2
            SelectionMode   =   1
            AllowUserResizing=   1
            FormatString    =   $"ConEndosso.frx":05CA
         End
         Begin VB.Label lblConsEndosso39 
            AutoSize        =   -1  'True
            Caption         =   "SubGrupo :"
            Height          =   195
            Left            =   300
            TabIndex        =   104
            Top             =   540
            Width           =   810
         End
      End
      Begin VB.Frame fraBenef 
         Caption         =   " Benefici�rio(s) "
         Height          =   5895
         Left            =   -74880
         TabIndex        =   98
         Top             =   780
         Width           =   10815
         Begin VB.TextBox txtBeneficiario 
            Height          =   5055
            Left            =   240
            TabIndex        =   99
            Top             =   480
            Width           =   10215
         End
         Begin VB.Label lbl 
            AutoSize        =   -1  'True
            Caption         =   "Texto Benefici�rio :"
            Height          =   315
            Index           =   1
            Left            =   480
            TabIndex        =   100
            Top             =   2640
            Width           =   1365
         End
      End
      Begin VB.Frame fraAdministracao 
         Caption         =   "Administra��o"
         Height          =   5895
         Left            =   -74880
         TabIndex        =   91
         Top             =   780
         Width           =   10815
         Begin VB.TextBox TxtRepAnt 
            Height          =   300
            Left            =   240
            Locked          =   -1  'True
            MaxLength       =   9
            TabIndex        =   94
            Top             =   1890
            Width           =   1035
         End
         Begin VB.TextBox TxtNomeRepAnt 
            BackColor       =   &H80000004&
            Height          =   300
            Left            =   1440
            Locked          =   -1  'True
            TabIndex        =   93
            Top             =   1890
            Width           =   7905
         End
         Begin VB.ComboBox CboSubGrupo 
            Height          =   315
            Index           =   1
            Left            =   240
            Style           =   2  'Dropdown List
            TabIndex        =   92
            Top             =   840
            Width           =   3315
         End
         Begin MSFlexGridLib.MSFlexGrid GridAdmAnt 
            Height          =   1695
            Left            =   240
            TabIndex        =   95
            Top             =   2880
            Width           =   10215
            _ExtentX        =   18018
            _ExtentY        =   2990
            _Version        =   393216
            Cols            =   5
            FixedCols       =   0
            SelectionMode   =   1
            AllowUserResizing=   1
            FormatString    =   $"ConEndosso.frx":0686
         End
         Begin VB.Label lblConsEndosso42 
            AutoSize        =   -1  'True
            Caption         =   "Representante :"
            Height          =   195
            Left            =   270
            TabIndex        =   97
            Top             =   1560
            Width           =   1140
         End
         Begin VB.Label lblConsEndosso41 
            AutoSize        =   -1  'True
            Caption         =   "SubGrupo :"
            Height          =   195
            Left            =   300
            TabIndex        =   96
            Top             =   540
            Width           =   810
         End
      End
      Begin VB.Frame fra 
         Caption         =   "Participa��o das Congeneres"
         Height          =   5895
         Index           =   14
         Left            =   -74880
         TabIndex        =   90
         Top             =   780
         Width           =   10815
         Begin VB.Frame Frame1 
            Caption         =   "Cedido:"
            Height          =   3765
            Left            =   240
            TabIndex        =   134
            Top             =   300
            Width           =   10455
            Begin VB.TextBox txtPercParticipacao 
               Height          =   300
               Left            =   5565
               Locked          =   -1  'True
               TabIndex        =   144
               Top             =   570
               Width           =   1335
            End
            Begin VB.TextBox txtPercDespesa 
               Height          =   300
               Left            =   7020
               TabIndex        =   143
               Top             =   570
               Width           =   1215
            End
            Begin VB.ComboBox cmbCongenere 
               Height          =   315
               Left            =   195
               Style           =   2  'Dropdown List
               TabIndex        =   139
               Top             =   555
               Width           =   3780
            End
            Begin VB.TextBox txtNumOrdem 
               Height          =   300
               Left            =   4080
               Locked          =   -1  'True
               TabIndex        =   138
               Top             =   570
               Width           =   1335
            End
            Begin MSFlexGridLib.MSFlexGrid gridCobrancaCosseguro 
               Height          =   2640
               Left            =   180
               TabIndex        =   140
               Top             =   975
               Width           =   10095
               _ExtentX        =   17806
               _ExtentY        =   4657
               _Version        =   393216
               Rows            =   1
               Cols            =   13
               FixedCols       =   0
               AllowBigSelection=   0   'False
               HighLight       =   0
               FormatString    =   $"ConEndosso.frx":073A
            End
            Begin VB.Label Label1 
               Caption         =   "Perc.Participa��o:"
               Height          =   255
               Index           =   75
               Left            =   5565
               TabIndex        =   146
               Top             =   330
               Width           =   1410
            End
            Begin VB.Label Label1 
               AutoSize        =   -1  'True
               Caption         =   "Perc.Desp. L�der:"
               Height          =   195
               Index           =   76
               Left            =   7005
               TabIndex        =   145
               Top             =   330
               Width           =   1260
            End
            Begin VB.Label Label11 
               AutoSize        =   -1  'True
               Caption         =   "Cong�neres:"
               Height          =   195
               Left            =   225
               TabIndex        =   142
               Top             =   315
               Width           =   900
            End
            Begin VB.Label Label1 
               Caption         =   "N�mero de Ordem:"
               Height          =   255
               Index           =   74
               Left            =   4080
               TabIndex        =   141
               Top             =   330
               Width           =   1455
            End
         End
         Begin VB.Frame Frame4 
            Caption         =   "Cedido:"
            Height          =   3015
            Left            =   240
            TabIndex        =   113
            Top             =   480
            Width           =   10455
            Begin MSFlexGridLib.MSFlexGrid gridCongeneres 
               Height          =   2295
               Left            =   120
               TabIndex        =   114
               Top             =   360
               Width           =   10215
               _ExtentX        =   18018
               _ExtentY        =   4048
               _Version        =   393216
               Cols            =   6
               HighLight       =   2
               SelectionMode   =   1
               FormatString    =   $"ConEndosso.frx":0824
            End
         End
         Begin VB.Frame Frame6 
            Caption         =   "Aceito:"
            Height          =   1635
            Left            =   240
            TabIndex        =   115
            Top             =   4140
            Width           =   10455
            Begin VB.TextBox txtDtEmissaoLider 
               Height          =   300
               Left            =   4455
               Locked          =   -1  'True
               TabIndex        =   125
               Top             =   1155
               Width           =   1695
            End
            Begin VB.TextBox txtApoliceLider 
               Height          =   300
               Left            =   210
               Locked          =   -1  'True
               TabIndex        =   119
               Top             =   570
               Width           =   1455
            End
            Begin VB.TextBox txtSeguradoraLider 
               Height          =   300
               Left            =   210
               Locked          =   -1  'True
               TabIndex        =   118
               Top             =   1155
               Width           =   3585
            End
            Begin VB.TextBox txtNumEndossoLider 
               Height          =   300
               Left            =   2430
               Locked          =   -1  'True
               TabIndex        =   117
               Top             =   570
               Width           =   1425
            End
            Begin VB.TextBox txtNumOrdemEndossoLider 
               Height          =   285
               Left            =   4440
               Locked          =   -1  'True
               TabIndex        =   116
               Top             =   570
               Width           =   1695
            End
            Begin VB.Label Label1 
               AutoSize        =   -1  'True
               Caption         =   "Data Emiss�o L�der:"
               Height          =   195
               Index           =   0
               Left            =   4455
               TabIndex        =   124
               Top             =   915
               Width           =   1440
            End
            Begin VB.Label Label31 
               AutoSize        =   -1  'True
               Caption         =   "Ap�lice L�der :"
               Height          =   195
               Left            =   180
               TabIndex        =   123
               Top             =   330
               Width           =   1035
            End
            Begin VB.Label Label34 
               AutoSize        =   -1  'True
               Caption         =   "Seguradora L�der :"
               Height          =   195
               Left            =   210
               TabIndex        =   122
               Top             =   915
               Width           =   1335
            End
            Begin VB.Label Label48 
               AutoSize        =   -1  'True
               Caption         =   "Endosso L�der N� :"
               Height          =   195
               Left            =   2430
               TabIndex        =   121
               Top             =   330
               Width           =   1350
            End
            Begin VB.Label Label49 
               AutoSize        =   -1  'True
               Caption         =   "No. Ordem End. L�der:"
               Height          =   195
               Left            =   4440
               TabIndex        =   120
               Top             =   330
               Width           =   1725
            End
         End
      End
      Begin VB.Frame fraCorretor 
         Caption         =   " Corretagem"
         Height          =   5895
         Left            =   -74880
         TabIndex        =   86
         Top             =   780
         Width           =   10815
         Begin VB.ComboBox CboSubGrupo 
            Height          =   315
            Index           =   0
            Left            =   240
            Style           =   2  'Dropdown List
            TabIndex        =   87
            Top             =   840
            Width           =   3315
         End
         Begin MSFlexGridLib.MSFlexGrid GridCorretores 
            Height          =   1695
            Left            =   240
            TabIndex        =   88
            Top             =   1470
            Width           =   10215
            _ExtentX        =   18018
            _ExtentY        =   2990
            _Version        =   393216
            Cols            =   7
            FixedCols       =   0
            SelectionMode   =   1
            AllowUserResizing=   1
            FormatString    =   $"ConEndosso.frx":08EB
         End
         Begin VB.Label lblConsEndosso40 
            AutoSize        =   -1  'True
            Caption         =   "SubGrupo :"
            Height          =   195
            Left            =   300
            TabIndex        =   89
            Top             =   540
            Width           =   810
         End
      End
      Begin VB.Frame Frame3 
         Caption         =   "SubGrupos"
         Height          =   5895
         Left            =   -74880
         TabIndex        =   84
         Top             =   780
         Width           =   10815
         Begin MSFlexGridLib.MSFlexGrid GridSubGrupos 
            Height          =   2460
            Left            =   360
            TabIndex        =   85
            Top             =   720
            Width           =   10125
            _ExtentX        =   17859
            _ExtentY        =   4339
            _Version        =   393216
            Rows            =   1
            Cols            =   29
            FixedCols       =   0
            AllowBigSelection=   0   'False
            AllowUserResizing=   1
            FormatString    =   $"ConEndosso.frx":09BA
         End
      End
      Begin VB.Frame fra 
         Height          =   5895
         Index           =   8
         Left            =   -74880
         TabIndex        =   52
         Top             =   780
         Width           =   10815
         Begin VB.CheckBox chkPPE 
            Caption         =   "PPE"
            Height          =   240
            Left            =   9975
            TabIndex        =   137
            Top             =   750
            Visible         =   0   'False
            Width           =   615
         End
         Begin VB.TextBox TxtTpPessoaAnt 
            BackColor       =   &H80000004&
            Enabled         =   0   'False
            Height          =   285
            Left            =   1170
            Locked          =   -1  'True
            TabIndex        =   68
            Top             =   2550
            Width           =   2535
         End
         Begin VB.TextBox TxtIdTpPessoaAnt 
            Height          =   285
            Left            =   480
            Locked          =   -1  'True
            MaxLength       =   3
            TabIndex        =   67
            Top             =   2550
            Width           =   615
         End
         Begin VB.TextBox TxtNomeBancoAnt 
            BackColor       =   &H80000004&
            Enabled         =   0   'False
            Height          =   285
            Left            =   1170
            Locked          =   -1  'True
            TabIndex        =   66
            Top             =   3300
            Width           =   2535
         End
         Begin VB.TextBox TxtNomeAgAnt 
            BackColor       =   &H80000004&
            Enabled         =   0   'False
            Height          =   285
            Left            =   4650
            Locked          =   -1  'True
            TabIndex        =   65
            Top             =   3300
            Width           =   2415
         End
         Begin VB.TextBox TxtCCAnt 
            Height          =   285
            Left            =   7500
            Locked          =   -1  'True
            MaxLength       =   11
            TabIndex        =   64
            Top             =   3270
            Width           =   1575
         End
         Begin VB.TextBox TxtBancoAnt 
            Height          =   285
            Left            =   480
            Locked          =   -1  'True
            MaxLength       =   3
            TabIndex        =   63
            Top             =   3300
            Width           =   615
         End
         Begin VB.TextBox TxtCodAgAnt 
            Height          =   285
            Left            =   3960
            Locked          =   -1  'True
            MaxLength       =   5
            TabIndex        =   62
            Top             =   3300
            Width           =   615
         End
         Begin VB.TextBox txtNomeAnt 
            Height          =   285
            Left            =   2040
            Locked          =   -1  'True
            MaxLength       =   60
            TabIndex        =   61
            Top             =   720
            Width           =   7785
         End
         Begin VB.TextBox TxtEnderecoAnt 
            Height          =   285
            Left            =   480
            Locked          =   -1  'True
            MaxLength       =   50
            TabIndex        =   60
            Top             =   1275
            Width           =   10095
         End
         Begin VB.TextBox TxtCidadeAnt 
            Height          =   285
            Left            =   450
            Locked          =   -1  'True
            MaxLength       =   30
            TabIndex        =   59
            Top             =   1830
            Width           =   4320
         End
         Begin VB.TextBox TxtUFAnt 
            Height          =   285
            Left            =   8100
            Locked          =   -1  'True
            MaxLength       =   2
            TabIndex        =   58
            Top             =   1830
            Width           =   375
         End
         Begin VB.TextBox TxtCEPAnt 
            Height          =   285
            Left            =   9000
            Locked          =   -1  'True
            MaxLength       =   10
            TabIndex        =   57
            Top             =   1830
            Width           =   1575
         End
         Begin VB.TextBox TxtDDDAnt 
            Height          =   285
            Left            =   5175
            Locked          =   -1  'True
            MaxLength       =   4
            TabIndex        =   56
            Top             =   1830
            Width           =   495
         End
         Begin VB.TextBox TxtTelefoneAnt 
            Height          =   285
            Left            =   6255
            Locked          =   -1  'True
            MaxLength       =   9
            TabIndex        =   55
            Top             =   1830
            Width           =   1215
         End
         Begin VB.TextBox txtCodCliAnt 
            BackColor       =   &H80000003&
            Enabled         =   0   'False
            Height          =   285
            Left            =   480
            Locked          =   -1  'True
            TabIndex        =   54
            Top             =   720
            Width           =   1335
         End
         Begin VB.TextBox TxtCPF_CGCAnt 
            Height          =   285
            Left            =   5190
            Locked          =   -1  'True
            TabIndex        =   53
            Top             =   2550
            Width           =   1815
         End
         Begin MSMask.MaskEdBox txtNascAnt 
            Height          =   285
            Left            =   8100
            TabIndex        =   69
            Top             =   2550
            Width           =   1215
            _ExtentX        =   2143
            _ExtentY        =   503
            _Version        =   393216
            AutoTab         =   -1  'True
            Enabled         =   0   'False
            MaxLength       =   10
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Mask            =   "##/##/####"
            PromptChar      =   "_"
         End
         Begin VB.Label lblConsEndosso37 
            AutoSize        =   -1  'True
            Caption         =   "Ag�ncia D�bito :"
            Height          =   195
            Left            =   3990
            TabIndex        =   83
            Top             =   3060
            Width           =   1185
         End
         Begin VB.Label lblConsEndosso38 
            AutoSize        =   -1  'True
            Caption         =   "Conta Corrente :"
            Height          =   195
            Left            =   7500
            TabIndex        =   82
            Top             =   3030
            Width           =   1155
         End
         Begin VB.Label lblConsEndosso36 
            AutoSize        =   -1  'True
            Caption         =   "Banco :"
            Height          =   195
            Left            =   480
            TabIndex        =   81
            Top             =   3000
            Width           =   555
         End
         Begin VB.Label lblConsEndosso26 
            AutoSize        =   -1  'True
            Caption         =   "Nome :"
            Height          =   195
            Left            =   2040
            TabIndex        =   80
            Top             =   480
            Width           =   510
         End
         Begin VB.Label lblConsEndosso34 
            AutoSize        =   -1  'True
            Caption         =   "CPF / CGC :"
            Height          =   195
            Left            =   5190
            TabIndex        =   79
            Top             =   2310
            Width           =   885
         End
         Begin VB.Label lblConsEndosso35 
            AutoSize        =   -1  'True
            Caption         =   "Data do Nascimento :"
            Height          =   195
            Left            =   8100
            TabIndex        =   78
            Top             =   2310
            Width           =   1545
         End
         Begin VB.Label lblConsEndosso27 
            AutoSize        =   -1  'True
            Caption         =   "Endere�o :"
            Height          =   195
            Left            =   480
            TabIndex        =   77
            Top             =   1035
            Width           =   780
         End
         Begin VB.Label lblConsEndosso28 
            AutoSize        =   -1  'True
            Caption         =   "Cidade :"
            Height          =   195
            Left            =   480
            TabIndex        =   76
            Top             =   1605
            Width           =   585
         End
         Begin VB.Label lblConsEndosso31 
            AutoSize        =   -1  'True
            Caption         =   "UF :"
            Height          =   195
            Left            =   8115
            TabIndex        =   75
            Top             =   1605
            Width           =   300
         End
         Begin VB.Label lblConsEndosso32 
            AutoSize        =   -1  'True
            Caption         =   "CEP :"
            Height          =   195
            Left            =   9000
            TabIndex        =   74
            Top             =   1605
            Width           =   405
         End
         Begin VB.Label lblConsEndosso29 
            AutoSize        =   -1  'True
            Caption         =   "DDD :"
            Height          =   195
            Left            =   5175
            TabIndex        =   73
            Top             =   1605
            Width           =   450
         End
         Begin VB.Label lblConsEndosso30 
            AutoSize        =   -1  'True
            Caption         =   "Telefone :"
            Height          =   195
            Left            =   6255
            TabIndex        =   72
            Top             =   1605
            Width           =   720
         End
         Begin VB.Label lblConsEndosso33 
            AutoSize        =   -1  'True
            Caption         =   "Tipo Pessoa :"
            Height          =   195
            Left            =   480
            TabIndex        =   71
            Top             =   2280
            Width           =   975
         End
         Begin VB.Label lblConsEndosso25 
            AutoSize        =   -1  'True
            Caption         =   "C�digo Cliente :"
            Height          =   195
            Left            =   480
            TabIndex        =   70
            Top             =   480
            Width           =   1110
         End
      End
      Begin VB.Frame fraEndosso 
         Height          =   6870
         Left            =   165
         TabIndex        =   2
         Top             =   720
         Width           =   10815
         Begin VB.TextBox txtDesctpAlteracaoEndosso 
            Height          =   285
            Left            =   120
            Locked          =   -1  'True
            TabIndex        =   152
            Top             =   2640
            Width           =   4335
         End
         Begin VB.TextBox txtUsuEmissor 
            Height          =   285
            Left            =   5535
            Locked          =   -1  'True
            TabIndex        =   136
            Top             =   2025
            Width           =   2385
         End
         Begin VB.Frame Frame2 
            Caption         =   "Texto Endosso"
            Height          =   1245
            Left            =   120
            TabIndex        =   22
            Top             =   5535
            Width           =   10545
            Begin VB.TextBox txtDescEndosso 
               BeginProperty Font 
                  Name            =   "Courier"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   885
               Left            =   240
               Locked          =   -1  'True
               MultiLine       =   -1  'True
               ScrollBars      =   2  'Vertical
               TabIndex        =   33
               Top             =   240
               Width           =   10050
            End
         End
         Begin VB.Frame Frame5 
            Caption         =   "Valores"
            Height          =   2235
            Left            =   120
            TabIndex        =   3
            Top             =   3135
            Width           =   10545
            Begin VB.TextBox TxtDtPgto 
               Height          =   285
               Left            =   8760
               TabIndex        =   133
               Top             =   1520
               Width           =   1500
            End
            Begin VB.TextBox TxtDtVenc 
               Height          =   285
               Left            =   8760
               TabIndex        =   131
               Top             =   1000
               Width           =   1500
            End
            Begin VB.TextBox TxtSituacao 
               Height          =   285
               Left            =   8760
               TabIndex        =   129
               Top             =   480
               Width           =   1500
            End
            Begin VB.TextBox TxtImpSegurada 
               Alignment       =   1  'Right Justify
               Height          =   285
               Left            =   5505
               Locked          =   -1  'True
               MultiLine       =   -1  'True
               TabIndex        =   34
               Top             =   1800
               Width           =   1605
            End
            Begin VB.TextBox Txtval_premio_tarifa 
               Alignment       =   1  'Right Justify
               Height          =   285
               Left            =   1800
               Locked          =   -1  'True
               MultiLine       =   -1  'True
               TabIndex        =   12
               Top             =   240
               Width           =   1605
            End
            Begin VB.TextBox Txtval_premio_total 
               Alignment       =   1  'Right Justify
               Height          =   285
               Left            =   1800
               Locked          =   -1  'True
               MultiLine       =   -1  'True
               TabIndex        =   11
               Top             =   630
               Width           =   1605
            End
            Begin VB.TextBox Txtval_iof 
               Alignment       =   1  'Right Justify
               Height          =   285
               Left            =   1800
               Locked          =   -1  'True
               MultiLine       =   -1  'True
               TabIndex        =   10
               Top             =   1020
               Width           =   1605
            End
            Begin VB.TextBox Txtval_comissao 
               Alignment       =   1  'Right Justify
               Height          =   285
               Left            =   1800
               Locked          =   -1  'True
               MultiLine       =   -1  'True
               TabIndex        =   9
               Top             =   1410
               Width           =   1605
            End
            Begin VB.TextBox Txtval_adic_fracionamento 
               Alignment       =   1  'Right Justify
               Height          =   285
               Left            =   1800
               Locked          =   -1  'True
               MultiLine       =   -1  'True
               TabIndex        =   8
               Top             =   1800
               Width           =   1605
            End
            Begin VB.TextBox Txtval_desconto_comercial 
               Alignment       =   1  'Right Justify
               Height          =   285
               Left            =   5505
               Locked          =   -1  'True
               MultiLine       =   -1  'True
               TabIndex        =   7
               Top             =   240
               Width           =   1605
            End
            Begin VB.TextBox Txtval_ir 
               Alignment       =   1  'Right Justify
               Height          =   285
               Left            =   5505
               Locked          =   -1  'True
               MultiLine       =   -1  'True
               TabIndex        =   6
               Top             =   630
               Width           =   1605
            End
            Begin VB.TextBox Txtval_comissao_estipulante 
               Alignment       =   1  'Right Justify
               Height          =   285
               Left            =   5505
               Locked          =   -1  'True
               MultiLine       =   -1  'True
               TabIndex        =   5
               Top             =   1020
               Width           =   1605
            End
            Begin VB.TextBox Txtcusto_endosso 
               Alignment       =   1  'Right Justify
               Height          =   285
               Left            =   5505
               Locked          =   -1  'True
               MultiLine       =   -1  'True
               TabIndex        =   4
               Top             =   1410
               Width           =   1605
            End
            Begin VB.Label LblConsEndosso45 
               AutoSize        =   -1  'True
               Caption         =   "Dt.Pagamento:"
               Height          =   195
               Left            =   7590
               TabIndex        =   132
               Top             =   1575
               Width           =   1065
            End
            Begin VB.Label LblConsEndosso44 
               AutoSize        =   -1  'True
               Caption         =   "Dt.Vencimento:"
               Height          =   195
               Left            =   7560
               TabIndex        =   130
               Top             =   1065
               Width           =   1095
            End
            Begin VB.Label LblConsEndosso43 
               AutoSize        =   -1  'True
               Caption         =   "Situa��o:"
               Height          =   195
               Left            =   7980
               TabIndex        =   128
               Top             =   540
               Width           =   675
            End
            Begin VB.Label lblConsEndosso23 
               Alignment       =   1  'Right Justify
               AutoSize        =   -1  'True
               Caption         =   "Import�ncia Segurada:"
               Height          =   195
               Left            =   3750
               TabIndex        =   35
               Top             =   1830
               Width           =   1605
            End
            Begin VB.Label lblConsEndosso21 
               AutoSize        =   -1  'True
               Caption         =   "Custo Endosso:"
               Height          =   195
               Left            =   4260
               TabIndex        =   21
               Top             =   1440
               Width           =   1110
            End
            Begin VB.Label lblConsEndosso19 
               AutoSize        =   -1  'True
               Caption         =   "Comiss�o Estipulante:"
               Height          =   195
               Left            =   3810
               TabIndex        =   20
               Top             =   1035
               Width           =   1545
            End
            Begin VB.Label lblConsEndosso17 
               AutoSize        =   -1  'True
               Caption         =   "IR:"
               Height          =   195
               Left            =   5160
               TabIndex        =   19
               Top             =   675
               Width           =   210
            End
            Begin VB.Label lblConsEndosso18 
               Alignment       =   1  'Right Justify
               AutoSize        =   -1  'True
               Caption         =   "IOF:"
               Height          =   195
               Left            =   390
               TabIndex        =   18
               Top             =   1050
               Width           =   1260
            End
            Begin VB.Label lblConsEndosso14 
               AutoSize        =   -1  'True
               Caption         =   "Pr�mio Tarifa:"
               Height          =   195
               Left            =   660
               TabIndex        =   17
               Top             =   270
               Width           =   975
            End
            Begin VB.Label lblConsEndosso16 
               AutoSize        =   -1  'True
               Caption         =   "Pr�mio Total:"
               Height          =   195
               Left            =   720
               TabIndex        =   16
               Top             =   660
               Width           =   930
            End
            Begin VB.Label lblConsEndosso20 
               AutoSize        =   -1  'True
               Caption         =   "Comiss�o:"
               Height          =   195
               Left            =   930
               TabIndex        =   15
               Top             =   1440
               Width           =   720
            End
            Begin VB.Label lblConsEndosso22 
               Alignment       =   1  'Right Justify
               AutoSize        =   -1  'True
               Caption         =   "Juros:"
               Height          =   195
               Left            =   210
               TabIndex        =   14
               Top             =   1830
               Width           =   1440
            End
            Begin VB.Label lblConsEndosso15 
               AutoSize        =   -1  'True
               Caption         =   "Desconto Comercial:"
               Height          =   195
               Left            =   3900
               TabIndex        =   13
               Top             =   300
               Width           =   1470
            End
         End
         Begin VB.Label LbltpAlteracaoEndosso 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Tipo Altera��o"
            Height          =   195
            Index           =   0
            Left            =   120
            TabIndex        =   151
            Top             =   2400
            Width           =   1035
         End
         Begin VB.Label lblUsuEmissao 
            Caption         =   "Usu�rio Emissor:"
            Height          =   195
            Index           =   0
            Left            =   5535
            TabIndex        =   135
            Top             =   1815
            Width           =   1335
         End
         Begin VB.Label LblTpCancEndosso 
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   1  'Fixed Single
            Height          =   285
            Left            =   180
            TabIndex        =   51
            Top             =   2025
            Width           =   4275
         End
         Begin VB.Label Label10 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Tipo de cancelamento:"
            Height          =   195
            Left            =   180
            TabIndex        =   50
            Top             =   1815
            Width           =   1635
         End
         Begin VB.Label LbLRamo 
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   1  'Fixed Single
            Height          =   285
            Left            =   1680
            TabIndex        =   49
            Top             =   915
            Width           =   6225
         End
         Begin VB.Label LbLProduto 
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   1  'Fixed Single
            Height          =   285
            Left            =   1680
            TabIndex        =   48
            Top             =   360
            Width           =   6225
         End
         Begin VB.Label lblConsEndosso6 
            AutoSize        =   -1  'True
            Caption         =   "Ramo :"
            Height          =   195
            Left            =   1665
            TabIndex        =   47
            Top             =   705
            Width           =   510
         End
         Begin VB.Label lblConsEndosso2 
            AutoSize        =   -1  'True
            Caption         =   "Produto :"
            Height          =   195
            Left            =   1665
            TabIndex        =   46
            Top             =   150
            Width           =   645
         End
         Begin VB.Label LblPropostaBB 
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   1  'Fixed Single
            Height          =   285
            Left            =   8940
            TabIndex        =   43
            Top             =   360
            Width           =   1455
         End
         Begin VB.Label lblConsEndosso3 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Proposta BB:"
            Height          =   195
            Left            =   8940
            TabIndex        =   42
            Top             =   150
            Width           =   930
         End
         Begin VB.Label lblApolice_id 
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   1  'Fixed Single
            Height          =   285
            Left            =   180
            TabIndex        =   41
            Top             =   915
            Width           =   1335
         End
         Begin VB.Label lblConsEndosso5 
            AutoSize        =   -1  'True
            Caption         =   "Ap�lice:"
            Height          =   195
            Left            =   165
            TabIndex        =   40
            Top             =   705
            Width           =   570
         End
         Begin VB.Label lblFatCambio 
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   1  'Fixed Single
            Height          =   285
            Left            =   8940
            TabIndex        =   37
            Top             =   2025
            Width           =   1455
         End
         Begin VB.Label lblConsEndosso12 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "C�mbio:"
            Height          =   195
            Left            =   8940
            TabIndex        =   36
            Top             =   1815
            Width           =   570
         End
         Begin VB.Label lblConsEndosso8 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Endosso:"
            Height          =   195
            Left            =   180
            TabIndex        =   32
            Top             =   1245
            Width           =   660
         End
         Begin VB.Label lblConsEndosso1 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Proposta:"
            Height          =   195
            Left            =   165
            TabIndex        =   31
            Top             =   150
            Width           =   675
         End
         Begin VB.Label lblConsEndosso9 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Tipo de endosso:"
            Height          =   195
            Left            =   1665
            TabIndex        =   30
            Top             =   1245
            Width           =   1230
         End
         Begin VB.Label Lbltp_endosso_id 
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   1  'Fixed Single
            Height          =   285
            Left            =   1680
            TabIndex        =   29
            Top             =   1470
            Width           =   6195
         End
         Begin VB.Label Lbldt_emissao 
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   1  'Fixed Single
            Height          =   285
            Left            =   8940
            TabIndex        =   28
            Top             =   915
            Width           =   1455
         End
         Begin VB.Label lblConsEndosso4 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Data de Emiss�o:"
            Height          =   195
            Left            =   8940
            TabIndex        =   27
            Top             =   705
            Width           =   1245
         End
         Begin VB.Label Lbldt_pedido_endosso 
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   1  'Fixed Single
            Height          =   285
            Left            =   8940
            TabIndex        =   26
            Top             =   1470
            Width           =   1455
         End
         Begin VB.Label lblConsEndosso7 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Data do Pedido:"
            Height          =   195
            Left            =   8955
            TabIndex        =   25
            Top             =   1245
            Width           =   1155
         End
         Begin VB.Label Lblproposta_id 
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   1  'Fixed Single
            Height          =   285
            Left            =   180
            TabIndex        =   24
            Top             =   360
            Width           =   1335
         End
         Begin VB.Label Lblendosso_id 
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   1  'Fixed Single
            Height          =   285
            Left            =   180
            TabIndex        =   23
            Top             =   1470
            Width           =   1335
         End
      End
   End
   Begin VB.PictureBox statusBar1 
      Align           =   2  'Align Bottom
      Height          =   255
      Left            =   0
      ScaleHeight     =   195
      ScaleWidth      =   11235
      TabIndex        =   0
      Top             =   8400
      Width           =   11295
   End
   Begin VB.Label Label47 
      AutoSize        =   -1  'True
      Caption         =   "Ramo :"
      Height          =   195
      Left            =   6600
      TabIndex        =   45
      Top             =   1650
      Width           =   510
   End
   Begin VB.Label Label7 
      AutoSize        =   -1  'True
      Caption         =   "Produto :"
      Height          =   195
      Left            =   6600
      TabIndex        =   44
      Top             =   1080
      Width           =   645
   End
End
Attribute VB_Name = "ConEndosso"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

' Demanda 19310624 - EXCLUIR O CAMPO PPE DO SEGP0223
' O objeto ChkPPE teve sua propriedade VISIBLE alterada para FALSE para atendimento da demanda.
' Optou-se por apenas inibir sua visualiza��o ao inv�s de exclui-lo definitivamente para o caso de futura reativa��o dessa funcionalidade.

'#
'# Sistema de Seguros : M�dulo de consulta a um endosso.
'#

Dim Clausulas_endosso As New Collection
Dim Coberturas_endosso As New Collection
Dim NovaCobertura As New Collection
Dim Corretores_endosso As New Collection
Dim SubGrupos_endosso As New Collection
Dim Estipulantes_endosso As New Collection
Dim Administradores_endosso As New Collection
Dim Componentes_endosso As New Collection
Dim Congeneres_endosso As New Collection
Dim Vida_Fatura_endosso As New Collection

'Dim beneficiarios                As New Beneficiario
Dim novoCorretor As New Corretor
Dim cob As Cobertura
Dim novaCob As Cobertura
Dim HaConteudo As Boolean
Dim SubGrupoId As Integer
Dim TabSeguro As String
Dim TabEscolha As String
Dim ValPremioTarifaNum As Variant
Dim ValComissaoNum As Variant
Dim ValComissaoEstipNum As Variant
Dim ValAdicFracNum As Variant
Dim ValDescNum As Variant
Dim dtInicioVigEndosso As String

'' Constantes para tratamento dos tabs
Const TABendosso_PROPOSTA = 0
Const TABendosso_DADOSCADASTRAIS = 1
Const TABendosso_SUBGRUPO = 2
Const TABendosso_CORRETAGEM = 3
Const TABendosso_ADMINISTRACAO = 4
Const TABendosso_COMPONENTES = 5
Const TABendosso_COBERTURAS = 6
Const TABendosso_BENEFICIARIOS = 7
Const TABendosso_CLAUSULA = 8
Const TABendosso_COSSEGURO = 9
Const TABendosso_VIDA = 10

Dim vEstip As estipulante
Dim vSubGrupo As SubGrupo

Sub Imprime_Endosso()
    Dim rc As rdoResultset
    Dim SQL As String
    Dim vSubGrupo As SubGrupo
    Dim vCorretor As Corretor
    Dim Texto1 As String, Texto2 As String
    Dim data_rodape As String
    Dim dt_inicio As String
    Dim dt_fim As String
    Dim Forma_Pgto As String
    Dim Periodo_Pgto As String
    Dim Observacao As String
    Dim NomeRamo As String
    Dim Atividade As String
    Dim Cod_Susep_Corretor() As String, Nome_Corretor() As String
    Dim Premio_Bruto As Double
    Dim texto_anexos(18) As String
    Dim linha_anexos(73) As String
    'Dim linha_desc(73)      As String  :: Maur�cio (Stefanini), em 20/12/2005 - FLOWBR 116210 - Cl�usula tem mais de 73 linhas
    Dim linha_desc() As String
    Dim nlinclau As Integer
    Dim i As Variant
    Dim ind_anexos As Integer
    Dim inicio_linha_anexo As Variant
    Dim fim_linha_anexo As Variant
    Dim tamanho_linha_anexo As Variant
    Dim ind_desc As Integer
    Dim inicio_linha_desc As Variant
    Dim fim_linha_desc As Variant
    Dim tamanho_linha_desc As Variant
    Dim ind_formula As Integer
    Dim ind_aux As Integer
    Dim resp As Integer
    Dim QtdDatas As Integer
    Dim DataAgendamento(1 To 12) As String
    Dim ValParcela1 As Double
    Dim ValParcelaDemais As Double
    Dim ValUltParcela As Double
    Dim ValPrimParc As Double
    Dim ValDemaisParcelas As Double
    Dim cont_corr As Integer
    Dim TemClausula As Boolean, DescAnexo As Boolean
    Dim aux_desc As String
    Dim tam_desc As Long
    Dim Processo_SUSEP As String    'Alessandra Grig�rio - 20/05/2004 - Trazer o processo susep correto
    ''
    On Error GoTo Erro

    MousePointer = vbHourglass
    '
    meses(1) = "Janeiro"
    meses(2) = "Fevereiro"
    meses(3) = "Mar�o"
    meses(4) = "Abril"
    meses(5) = "Maio"
    meses(6) = "Junho"
    meses(7) = "Julho"
    meses(8) = "Agosto"
    meses(9) = "Setembro"
    meses(10) = "Outubro"
    meses(11) = "Novembro"
    meses(12) = "Dezembro"

    '  Recupera dados da impressora padr�o
    DeviceDefault = LeArquivoIni("WIN.INI", "windows", "device")
    OldDefault = DeviceDefault

    '  Abre a conex�o com o Crystal Report.
    Report.Reset
    Report.Connect = rdocn.Connect
    Report.ReportFileName = App.PATH & "\endosso.rpt"
    Report.Destination = crptToPrinter    ' direciona saida para impressora.

    ''Inicializa o vetor de formulas
    For i = 0 To 78
        Report.Formulas(i) = ""
    Next
    '/*******************************************************************/
    ''  Impress�o da Ap�lice
    '/*******************************************************************/
    '
    Atividade = ""

    '' Obtem Nome do Ramo
    SQL = "SELECT r.nome " & _
          "FROM   ramo_tb r   WITH (NOLOCK)  " & _
        " RIGHT JOIN proposta_basica_tb pb   WITH (NOLOCK)  " & _
        "       ON r.ramo_id = pb.ramo_id " & _
        " WHERE  pb.proposta_id = " & vPropostaId

    Set rc = rdocn.OpenResultset(SQL)

    If Not rc.EOF Then
        NomeRamo = UCase("" & rc!Nome)
    Else
        NomeRamo = ""
    End If
    rc.Close

    '' Obtem Corretores
    If SubGrupos.Count > 0 Then
        '' Para SubGrupos, descobrir TODOS os Corretores
        cont_corr = 0
        For Each vSubGrupo In SubGrupos
            For Each vCorretor In vSubGrupo.Corretores
                For i = 1 To cont_corr
                    If Cod_Susep_Corretor(i) = Format(vCorretor.Id, "#####-###") Then
                        Exit For
                    End If
                Next
                '' Se n�o achou o corretor, adiciona ao vetor
                If i > cont_corr Then
                    cont_corr = cont_corr + 1
                    ReDim Preserve Cod_Susep_Corretor(1 To cont_corr) As String    '** alterado por fpimenta / Imar�s - 26/03/2003 - (PRESERVE)
                    ReDim Preserve Nome_Corretor(1 To cont_corr) As String      '**
                    '
                    'Luciana - 11/11/2003 - De: vCorretor.Id - Para: vCorretor.Codigo_susep
                    Cod_Susep_Corretor(cont_corr) = Replace(Format(vCorretor.Codigo_susep, "00_00_00_0_000000-0"), "_", ".")
                    Nome_Corretor(cont_corr) = vCorretor.Nome
                End If
            Next
        Next
        ''
        If cont_corr = 0 Then
            ReDim Cod_Susep_Corretor(1 To 1) As String
            ReDim Nome_Corretor(1 To 1) As String
            '
            Cod_Susep_Corretor(1) = Space(10)
            Nome_Corretor(1) = ""
        End If
    Else

        If GridCorretores.Rows > 1 Then
            ReDim Cod_Susep_Corretor(1 To GridCorretores.Rows - 1) As String
            ReDim Nome_Corretor(1 To GridCorretores.Rows - 1) As String
            '
            For i = 1 To GridCorretores.Rows - 1
                'Alterado por fpimenta / Imar�s - 10/09/2003 (Solicitado por Marcos Scombati Martins)
                Cod_Susep_Corretor(i) = Replace(Format(GridCorretores.TextMatrix(i, 0), "00_00_00_0_000000-0"), "_", ".")
                Nome_Corretor(i) = GridCorretores.TextMatrix(i, 2)
            Next
        Else
            ReDim Cod_Susep_Corretor(1 To 1) As String
            ReDim Nome_Corretor(1 To 1) As String
            '
            Cod_Susep_Corretor(1) = Space(10)
            Nome_Corretor(1) = ""
        End If
    End If

    '' Mensagem Inicial
    Report.Formulas(0) = "tp_documento= 'ENDOSSO'"
    '
    '' Cabecalho da Ap�lice e seus anexos
    Report.Formulas(1) = "num_apolice= '" & Format(lblApolice_id.Caption, "000000000") & "'"
    Report.Formulas(2) = "num_endosso= '" & Format(Lblendosso_id.Caption, "000000000") & "'"
    Report.Formulas(3) = "apolice_anterior= '" & Renova_Apolice() & "'"
    If Trim(LblPropostaBB.Caption) <> "" Then
        Report.Formulas(4) = "num_proposta= '" & Format(LblPropostaBB.Caption, "000000000") & "'"
    Else
        Report.Formulas(4) = "num_proposta= '" & Format(vPropostaId, "000000000") & "'"
    End If
    Report.Formulas(5) = "desc_produto= '" & Left(LbLProduto.Caption, 40) & "'"
    Report.Formulas(6) = "desc_ramo= '" & Left(Format(vRamoId, "00") & " - " & NomeRamo, 50) & "'"
    '
    '' Dados do Segurado
    With ConPropBasicaVida
        Report.Formulas(7) = "nome_segurado= '" & Left(.txtNome.Text & Space(50), 50) & "'"
        Report.Formulas(8) = "cgc_segurado= '" & .CPF_CNPJ_saida & "  " & .txtCPF_CGC.Text & "'"
        Report.Formulas(9) = "endereco_segurado= '" & Left(.txtEndereco.Text & Space(50), 50) & "'"
        Report.Formulas(10) = "bairro_segurado= '" & IIf(Trim(.TxtBairro.Text) = "", "", Left("BAIRRO       :  " & .TxtBairro.Text & Space(35), 35)) & "'"
        Report.Formulas(11) = "municipio_segurado= '" & Left(.txtCidade.Text & " - " & .txtUF.Text & Space(50), 50) & "'"
        Report.Formulas(12) = "cep_segurado= '" & Left(.txtCEP.Text & Space(35), 35) & "'"
    End With
    '' Local do Risco
    Report.Formulas(13) = "endereco_local_risco= '" & String(60, "*") & "'"
    '
    '' Atividade Principal
    If Atividade = "" Then
        Texto1 = String(20, "*")
    Else
        Texto1 = Left(Atividade & Space(20), 20)
    End If
    Report.Formulas(14) = "atividade_principal= '" & Texto1 & "'"
    '
    '' Vig�ncia
    dt_inicio = Mid(Lbldt_pedido_endosso.Caption, 1, 2) & " de "
    dt_inicio = UCase(dt_inicio & meses(Val(Mid(Lbldt_pedido_endosso.Caption, 4, 2))) & " de " & Mid(Lbldt_pedido_endosso.Caption, 7, 4))
    Report.Formulas(15) = "inicio_vigencia_seguro= '" & dt_inicio & "'"
    '
    If ConPropBasicaVida.txtFimVigencia.Text <> "" Then
        dt_fim = Mid(ConPropBasicaVida.txtFimVigencia.Text, 1, 2) & " de "
        dt_fim = UCase(dt_fim & meses(Val(Mid(ConPropBasicaVida.txtFimVigencia.Text, 4, 2))) & " de " & Mid(ConPropBasicaVida.txtFimVigencia.Text, 7, 4))
        Texto1 = dt_fim
        '
    Else
        Texto1 = Space(33)
    End If
    Report.Formulas(16) = "fim_vigencia_seguro= '" & Texto1 & "'"
    '
    nlinclau = 0
    '
    If Trim(txtDescEndosso.Text) <> "" Then
        aux_desc = MudaAspaSimples(Trim(txtDescEndosso.Text))
        tam_desc = Len(aux_desc)
        i = 1
        ''
        '' Quebra em linhas de 90 chars, sem dividir palavras pelo meio
        Texto2 = Formata_Clausula(aux_desc)
        '' Seta posicao da primeira linha de impressao do texto da clausula
        ind_desc = 0
        inicio_linha_desc = 1
        fim_linha_desc = 1
        tamanho_linha_desc = 0
        '' Verifica se descri��o do endosso pode ser impressa na capa
        '' ou como anexo
        For i = 1 To Len(Texto2)
            '' Procura 'returns' no texto da clausula para
            '' contar o no. de linhas; achou CR_LF, passou para nova linha.
            If Mid(Texto2, i, 2) = Chr(13) & Chr(10) Then
                ' Movimenta o conteudo da linha para o vetor de linhas de descricao.
                fim_linha_desc = i - 1
                tamanho_linha_desc = (fim_linha_desc - inicio_linha_desc) + 1
                ind_desc = ind_desc + 1
                'Maur�cio (Stefanini), em 20/12/2005 - FLOWBR 116210 - Cl�usula tem mais de 73 linhas
                ReDim Preserve linha_desc(ind_desc)
                'LUCIANO - CONFITEC 28/06/2018 - IM00394869 - INICIO
                linha_desc(ind_desc) = Replace(Replace(Mid(Texto2, inicio_linha_desc, tamanho_linha_desc), Chr(10), ""), Chr(13), "")
                'linha_desc(ind_desc) = Mid(Texto2, inicio_linha_desc, tamanho_linha_desc)
                'LUCIANO - CONFITEC 28/06/2018 - IM00394869 - FIM
                inicio_linha_desc = i + 2
            End If
        Next i
        '
        If inicio_linha_desc < Len(Texto2) Then
            ind_desc = ind_desc + 1
            'Maur�cio (Stefanini), em 20/12/2005 - FLOWBR 116210 - Cl�usula tem mais de 73 linhas
            ReDim Preserve linha_desc(ind_desc)
            linha_desc(ind_desc) = Mid(Texto2, inicio_linha_desc, Len(Texto2) - inicio_linha_desc)
        End If
        '' Se descricao tem mais de 13 linhas, imprimir em anexo
        If ind_desc > 13 Then
            'Descri��o maior que o tamanho permitido na capa, imprimir como anexo
            DescAnexo = True
        End If
        '
        If Not DescAnexo Then
            texto_anexos(1) = String(100, "=")
            texto_anexos(2) = Space(40) & "DESCRI��O DO ENDOSSO"
            texto_anexos(3) = String(100, "=")
            For i = 1 To ind_desc
                texto_anexos(i + 3) = linha_desc(i)
            Next
            nlinclau = i + 3
        End If
        ''
    End If  'txtdescendosso <> ""

    '' Seleciona as cl�usulas do endosso
    'SQL = "SELECT descr_clausula "
    SQL = "SELECT c.cod_clausula, c.descr_clausula "    'Stefanini (Maur�cio), em 20/01/2005
    SQL = SQL & "FROM clausula_tb c  WITH (NOLOCK) , clausula_personalizada_tb cp   WITH (NOLOCK) "
    SQL = SQL & "WHERE cp.proposta_id = " & vPropostaId
    SQL = SQL & " AND isnull(cp.endosso_id, 0) = " & Lblendosso_id.Caption
    SQL = SQL & " AND c.dt_fim_vigencia is null"
    SQL = SQL & " AND c.cod_clausula = cp.cod_clausula_original "
    SQL = SQL & " AND c.dt_inicio_vigencia = cp.dt_inicio_vigencia_cl_original "
    SQL = SQL & " ORDER BY cp.seq_clausula"
    '
    Set rc = rdoCn1.OpenResultset(SQL, rdOpenStatic)
    '
    If Not rc.EOF Then
        '' Se a descricao do endosso mais a lista de clausulas > 17 linhas,
        '' imprime a descricao em anexo
        If nlinclau + rc.RowCount + 3 > 17 Then
            DescAnexo = True
            nlinclau = 0
        End If
        '
        texto_anexos(nlinclau + 1) = String(100, "=")
        texto_anexos(nlinclau + 2) = Space(47) & "ANEXOS"
        texto_anexos(nlinclau + 3) = String(100, "=")
        nlinclau = nlinclau + 3

        '' Se descri��o tiver que ser impressa no anexo, colocar o titulo descri��o na lista de anexos
        If DescAnexo Then
            nlinclau = nlinclau + 1
            texto_anexos(nlinclau) = Space(10) & "- DESCRI��O DO ENDOSSO"
        End If
        ''
        '--------------------------------------------------
        'Alterado Por Victor Neto - Stefanini - 27/10/2006
        'Referente ao flow 163185
        '--------------------------------------------------
        'TemClausula = False             'TemClausula = True

        '--------------------------------------------------
        'Adicionado por Victor Neto - Stefanini - 27/10/2006
        'Referente ao flow 163185
        '--------------------------------------------------
        'rc.MoveLast: rc.MoveNext                'Adicionado
        '



        'flow 204257 - Leandro A. Souza - Stefanini IT - 05/03/2007
        If tabEndossos.Tab = 9 Then

            While Not rc.EOF
                'If Val(Trim$(rc!cod_clausula)) <> 93011 Then 'Apenas imprime clausula quando n�o for especifica��o da ap�lice.
                Texto1 = Left(Space(10) & "- " & Trim$(rc!descr_clausula & Space(90)), 90)
                nlinclau = nlinclau + 1
                texto_anexos(nlinclau) = Texto1
                'End If
                rc.MoveNext
            Wend

            TemClausula = True
        Else
            TemClausula = False
        End If
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

    Else
        '
        TemClausula = False
    End If
    '
    '' Preenche as linhas de cl�usula restantes com branco
    For i = nlinclau + 1 To 17
        texto_anexos(i) = Space(90)
    Next i
    '
    For ind_formula = 17 To 33
        ind_aux = ind_formula - 16
        Report.Formulas(ind_formula) = "texto_anexos_" & CStr(ind_aux) & "= '" & texto_anexos(ind_aux) & "'"
    Next ind_formula

    'Pr�mio L�quido
    If Val(Txtval_premio_tarifa.Text) > 0 Then
        Report.Formulas(34) = "val_premio_liquido_total= '" & "R$ " & Txtval_premio_tarifa.Text & "'"
    Else
        Report.Formulas(34) = "val_premio_liquido_total= '" & "R$ 0,00'"
    End If
    'Juros / Custo / IOF
    If Val(Txtval_adic_fracionamento.Text) > 0 Then
        Report.Formulas(35) = "val_juros= '" & "R$ " & Txtval_adic_fracionamento.Text & "'"
    Else
        Report.Formulas(35) = "val_juros= '" & "R$ 0,00'"
    End If
    '
    If Val(Txtcusto_endosso.Text) > 0 Then
        Report.Formulas(36) = "val_custo_apolice= '" & "R$ " & Txtcusto_endosso.Text & "'"
    Else
        Report.Formulas(36) = "val_custo_apolice= '" & "R$ 0,00'"
    End If
    '
    If Val(Txtval_iof.Text) > 0 Then
        Report.Formulas(37) = "val_iof= '" & "R$ " & Txtval_iof.Text & "'"
    Else
        Report.Formulas(37) = "val_iof= '" & "R$ 0,00'"
    End If
    'Pr�mio Bruto
    '
    If Val(Txtval_premio_total.Text) <> 0 Then
        Report.Formulas(38) = "val_premio_bruto_total= '" & "R$ " & Txtval_premio_total.Text & "'"
    Else
        Report.Formulas(38) = "val_premio_bruto_total= '" & "R$ 0,00'"
    End If

    '' Parcelamento
    '
    SQL = "SELECT  num_cobranca, dt_agendamento, val_cobranca "
    SQL = SQL & "FROM agendamento_cobranca_tb   WITH (NOLOCK) "
    SQL = SQL & " WHERE proposta_id = " & vPropostaId
    SQL = SQL & " AND isnull(num_endosso, 0) = " & Lblendosso_id.Caption
    SQL = SQL & " ORDER BY num_cobranca"
    '
    Set rc1 = rdocn.OpenResultset(SQL)
    QtdDatas = 0
    If Not rc1.EOF Then
        ValParcela1 = Val(rc1!val_cobranca)
        QtdDatas = QtdDatas + 1
        DataAgendamento(QtdDatas) = Format$(rc1!dt_agendamento, "dd/mm/yyyy")
        '
        rc1.MoveNext
        If rc1.EOF Then    's� tem uma parcela
            ValParcelaDemais = 0
            ValUltParcela = 0
        Else    'tem mais de uma parcela
            ValParcelaDemais = Val(rc1!val_cobranca)
            ValUltParcela = 0
            QtdDatas = QtdDatas + 1
            DataAgendamento(QtdDatas) = Format$(rc1!dt_agendamento, "dd/mm/yyyy")
            '
            rc1.MoveNext
            Do While Not rc1.EOF
                QtdDatas = QtdDatas + 1
                DataAgendamento(QtdDatas) = Format$(rc1!dt_agendamento, "dd/mm/yyyy")
                ValUltParcela = Val(0 & rc1!val_cobranca)
                rc1.MoveNext
            Loop
            ''
            If ValParcelaDemais = ValUltParcela Then
                ValUltParcela = 0
            End If
        End If
    Else
        ValParcela1 = 0
        ValParcelaDemais = 0
        ValUltParcela = 0
    End If

    If QtdDatas > 0 Then
        Report.Formulas(39) = "txt_forma_pgto_1= '" & "QT. PARCELAS    :" & "'"
        Report.Formulas(40) = "forma_pgto_1= '" & Format(QtdDatas, "00") & "'"
        '
        Report.Formulas(41) = "txt_forma_pgto_2= '" & "PARCELA 01      :" & "'"
        Texto1 = Format(ValParcela1, "###,###,##0.00")
        Report.Formulas(42) = "forma_pgto_2= '" & "R$ " & Space(14 - Len(Texto1)) & Texto1 & "'"
        '
        Report.Formulas(43) = "txt_forma_pgto_3= '" & "DEMAIS PARCELAS :" & "'"
        Texto1 = Format(ValParcelaDemais, "###,###,##0.00")
        Report.Formulas(44) = "forma_pgto_3= '" & "R$ " & Space(14 - Len(Texto1)) & Texto1 & "'"
        '
        Report.Formulas(45) = "txt_forma_pgto_4= '" & "�LTIMA PARCELA  :" & "'"
        Texto1 = Format(ValUltParcela, "###,###,##0.00")
        Report.Formulas(46) = "forma_pgto_4= '" & "R$ " & Space(14 - Len(Texto1)) & Texto1 & "'"
        '
        '        Para Endosso , sempre colocar a data, n�o colocar mensagem � vista
        '        '' Vencimentos
        '        If Val(txtPagoAto.Text) > 0 Then
        '            DataAgendamento(1) = " � VISTA"
        '        End If
        '
        For i = 1 To 12
            If DataAgendamento(i) <> "" Then
                Report.Formulas(46 + i) = "vencimento_" & i & "='" & Format(i, "00") & "-" & DataAgendamento(i) & "'"
            Else
                Report.Formulas(46 + i) = "vencimento_" & i & "=''"
            End If
        Next
    Else
        Report.Formulas(39) = "txt_forma_pgto_1= '" & Space(17) & "'"
        Report.Formulas(40) = "forma_pgto_1= '" & Space(17) & "'"
        '
        '        If Periodo_Pgto <> "" And Val(Txtval_premio_total.Text) > 0 Then
        '            Report.Formulas(41) = "txt_forma_pgto_2= '" & "FATURA " & Periodo_Pgto & "'"
        '        Else
        Report.Formulas(41) = "txt_forma_pgto_2= '" & Space(17) & "'"
        '        End If
        Report.Formulas(42) = "forma_pgto_2= '" & Space(17) & "'"
        '
        Report.Formulas(43) = "txt_forma_pgto_3= '" & Space(17) & "'"
        Report.Formulas(44) = "forma_pgto_3= '" & Space(17) & "'"
        '
        Report.Formulas(45) = "txt_forma_pgto_4= '" & Space(17) & "'"
        Report.Formulas(46) = "forma_pgto_4= '" & Space(17) & "'"
        '
        For i = 1 To 12
            Report.Formulas(46 + i) = "vencimento_" & i & "= '" & Space(13) & "'"
        Next
    End If

    With ConPropBasicaVida
        '' Forma de Cobran�a
        If Val(Txtval_premio_total.Text) > 0 Then
            Report.Formulas(59) = "forma_cobranca= '" & .txtFormaPg.Text & "'"
        Else
            Report.Formulas(59) = "forma_cobranca= ''"
        End If
        '' Ag�ncia de Contrata��o
        If .txtCodAgencia.Text <> "" Then
            Texto1 = Left(.txtCodAgencia.Text & Space(4), 4)
            Texto1 = Texto1 & "-" & Left(.txtNomeAgencia.Text & Space(18), 18)
        Else
            Texto1 = Space(25)
        End If
        '
        Report.Formulas(60) = "agencia_cobranca= '" & Texto1 & "'"
    End With


    'Implementado por fpimenta / Imar�s - 26/03/2003
    If cont_corr > 1 Then
        VNomeCorretor = ""
        VCodSusepCorretor = ""
        For x = 1 To cont_corr
            If x = 1 Then
                Report.Formulas(61) = "desc_corretor= '" & Nome_Corretor(x) & "'"
                Report.Formulas(62) = "cod_susep= '" & Cod_Susep_Corretor(x) & "'"
            Else
                If x = 2 Then
                    VNomeCorretor = "'" & Nome_Corretor(x) & "'"
                    VCodSusepCorretor = "'" & Cod_Susep_Corretor(x) & "'"
                Else
                    If InStr(1, VNomeCorretor, Nome_Corretor(x), 1) < 0 Then     'Agrigorio - 17/07/2003
                        VNomeCorretor = VNomeCorretor & " + chr(13) + " & "'" & Nome_Corretor(x) & "'"
                        VCodSusepCorretor = VCodSusepCorretor & " + chr(13) + " & "'" & Cod_Susep_Corretor(x) & "'"
                    End If
                End If
            End If
        Next x
        Report.Formulas(66) = "obs_corretor= " & VNomeCorretor
        Report.Formulas(67) = "obs_susep= " & VCodSusepCorretor
    Else
        Report.Formulas(61) = "desc_corretor= '" & Nome_Corretor(1) & "'"
        Report.Formulas(62) = "cod_susep= '" & Cod_Susep_Corretor(1) & "'"
    End If
    'fim

    Report.Formulas(63) = "inspetoria= '" & String(10, "*") & "'"
    '
    If Not VerificaData2(Lbldt_emissao.Caption) Then
        MsgBox "Data de Emiss�o do Endosso inv�lida!!"
        GoTo Erro
    Else
        data_rodape = Lbldt_emissao.Caption
    End If
    '
    Observacao = Mid(data_rodape, 1, 2) & " de "
    Observacao = UCase(Observacao & meses(Val(Mid(data_rodape, 4, 2))) & " de " & Mid(data_rodape, 7, 4))

    Texto2 = " "
    '
    Texto2 = "S�O PAULO, " & Observacao    '
    Report.Formulas(64) = "local_data_emissao= '" & Texto2 & "'"

    ' Traz Numero Processo SUSEP do produto.
    'SQL = "SELECT num_proc_susep "
    'SQL = SQL & "FROM produto_tb  WITH (NOLOCK)  "
    'SQL = SQL & "WHERE produto_id = " & Val(Left(Trim(ConPropBasicaVida.txtProduto.Text), 3)) & " "
    'SQL = SQL & "AND (num_proc_susep is not null) "
    'SQL = SQL & "AND (num_proc_susep <> ' ')"
    'Alessandra Grig�rio - 20/05/2004 - Trazer o processo susep correto
    SQL = "SELECT   isnull(a.num_proc_susep, p.num_proc_susep) num_proc_susep"
    SQL = SQL & " FROM  apolice_tb a  WITH (NOLOCK) "
    SQL = SQL & " join  proposta_tb b  WITH (NOLOCK) "
    SQL = SQL & "    on b.proposta_id = a.proposta_id"
    SQL = SQL & " join  produto_tb p  WITH (NOLOCK) "
    SQL = SQL & "    on p.produto_id = b.produto_id"
    SQL = SQL & " Where a.proposta_id = " & vPropostaId
    '
    Set rc = rdocn.OpenResultset(SQL)
    '
    If Not rc.EOF Then
        Processo_SUSEP = Replace(Replace(Replace(rc!num_proc_susep, "-", ""), ".", ""), "/", "")
        Processo_SUSEP = Format(Processo_SUSEP, "@@.@@@@@@/@@-@@")
        Report.Formulas(65) = "num_proc_susep= 'Processo SUSEP : " & Processo_SUSEP & "'"
    End If
    '
    'Pede a impressora a ser utilizada
    dialog.CancelError = True
    dialog.ShowPrinter
    '
    If Err = 32755 Then
        MousePointer = vbDefault
        Exit Sub
    End If

    '' Seta a impressora
    '  Recupera dados da impressora padr�o
    DeviceDefault = LeArquivoIni("WIN.INI", "windows", "device")

    If DeviceDefault <> OldDefault Then
        MudouPrtDefault = True
    Else
        MudouPrtDefault = False
    End If

    '  Imprime documento.
    Report.Action = 1
    ''
    ''
    On Error GoTo cancelar

    If TemClausula Or DescAnexo Then
        resp = MsgBox("Favor colocar o formul�rio Anexo", vbOKCancel, Me.Caption)
        If resp = vbCancel Then
            GoTo cancelar
        End If

        ''Inicializa o vetor de formulas
        For i = 0 To 78
            If i <> 7 And i <> 8 And i <> 9 And i <> 10 And i <> 11 And i <> 12 Then    ' N�o limpar os dados do estipulante incidente INC000003987090.
                Report.Formulas(i) = ""
            End If
        Next
        '/*******************************************************************/
        ''  Impress�o dos Anexos
        '/*******************************************************************/
        Report.ReportFileName = App.PATH & "\anexos.rpt"
        '
        Report.Formulas(0) = "num_apolice_anexos= '" & Format(lblApolice_id.Caption, "000000000") & "'"
        Report.Formulas(1) = "num_endosso_anexos= '" & Format(Lblendosso_id.Caption, "000000000") & "'"
        Report.Formulas(2) = "apolice_anterior_anexos= '" & Renova_Apolice() & "'"
        If Trim(LblPropostaBB.Caption) <> "" Then
            Report.Formulas(3) = "num_proposta_anexos= '" & Format(LblPropostaBB.Caption, "000000000") & "'"
        Else
            Report.Formulas(3) = "num_proposta_anexos= '" & Format(vPropostaId, "000000000") & "'"
        End If
        Report.Formulas(4) = "desc_produto_anexos= '" & Left(LbLProduto.Caption, 40) & "'"
        Report.Formulas(5) = "desc_ramo_anexos= '" & Left(Format(vRamoId, "00") & " - " & NomeRamo, 50) & "'"
        '

        Report.StoredProcParam(0) = ConPropBasicaVida.Usuario
        On Error GoTo cancelar
        '
        '' Imprime a descri��o do endosso como primeiro anexo
        'Para cada cl�usula, formatar e imprimir
        'APAGAR REGISTROS ANTIGOS DO USUARIO - Diego - GPTI - 29/07/2008
        'INICIO - INC000004357092 - 15/08/2014 - Colocar o usuario entre aspas simples
        SQL = "EXEC SEGS7129_SPD '" & ConPropBasicaVida.Usuario & "'"
        'SQL = "EXEC SEGS7129_SPD " & ConPropBasicaVida.Usuario
        'FIM - INC000004357092 - 15/08/2014 - Colocar o usuario entre aspas simples
        Set rc_del = rdoCn1.OpenResultset(SQL)
        If DescAnexo Then
            aux_desc = String(90, "=") & vbCrLf & Space(35) & "DESCRI��O DO ENDOSSO" & vbCrLf & String(90, "=") & vbCrLf

            Texto2 = Formata_Clausula(Trim(aux_desc & txtDescEndosso.Text))
            'INSERIR NA TABELA DO RELAT�RIO - Diego - GPTI - 29/07/2008
            SQL = "EXEC SEGS7128_SPI '" & Texto2 & "', 1  , 0, '" & ConPropBasicaVida.Usuario & "'"
            Set rc_proc = rdoCn1.OpenResultset(SQL)
            '' Seta posicao da primeira linha de impressao do texto da clausula
            '            ultquebra = 1               'pos. da ult. quebra de pagina
            '            ind_anexos = 0
            '            inicio_linha_anexos = 1
            '            fim_linha_anexos = 1
            '            tamanho_linha_anexos = 0
            '            '' Imprime cl�usula, colocando cabe�alho a cada quebra de p�gina
            '            For i = 1 To Len(Texto2)
            '                '' Procura 'returns' no texto da clausula para
            '                '' contar o no. de linhas; achou CR_LF, passou para nova linha.
            '                If Mid(Texto2, i, 2) = Chr(13) & Chr(10) Then
            '                ' Movimenta o conteudo da linha para o vetor de linhas_anexo.
            '                    fim_linha_anexos = i - 1
            '                    tamanho_linha_anexos = (fim_linha_anexos - inicio_linha_anexos) + 1
            '                    ind_anexos = ind_anexos + 1
            '                    linha_anexos(ind_anexos) = Mid(Texto2, inicio_linha_anexos, tamanho_linha_anexos)
            '                    Report.Formulas(ind_anexos + 5) = "linha_" & CStr(ind_anexos + 2) & "= '" & linha_anexos(ind_anexos) & "'"
            '                    inicio_linha_anexos = i + 2
            '                End If
            '                '' Quando chega na linha 71, pula para nova p�gina
            '                If ind_anexos = 71 Then
            '                '   imprime relatorio - as 71 linhas sao descarregadas na saida e action=1
            '                    Report.Action = 1
            '                    ind_anexos = 0
            '                    ultquebra = i + 2
            '                    i = i + 1
            '                End If 'Quebra de pagina (quando cheia) !
            '            Next i
            '            ''' Se ultima frase nao tem CR/LF, n�o estava imprimindo
            '            If inicio_linha_anexos < Len(Texto2) Then
            '                ind_anexos = ind_anexos + 1
            '                linha_anexos(ind_anexos) = Mid(Texto2, inicio_linha_anexos, Len(Texto2) - inicio_linha_anexos)
            '                Report.Formulas(ind_anexos + 5) = "linha_" & CStr(ind_anexos + 2) & "= '" & linha_anexos(ind_anexos) & "'"
            '            End If
            '            ''
            '            If ultquebra < Len(Texto2) Then
            ''                For ind_formula = 6 To ind_anexos
            ''                    Report.Formulas(ind_formula) = "linha_" & CStr(ind_formula - 3) & "= '" & linha_anexos(ind_formula - 5) & "'"
            ''                Next ind_formula
            ''                '
            '                If ind_anexos < 71 Then
            '                    For ind_formula = ind_anexos + 1 To 71
            '                        linha_anexos(ind_formula) = Space(90)
            '                        Report.Formulas(ind_formula + 5) = "linha_" & CStr(ind_formula + 2) & "= '" & linha_anexos(ind_formula) & "'"
            '                    Next ind_formula
            '                End If
            '                '
            '                Report.Action = 1
            '                ind_anexos = 0
            '            End If

        End If    ' Se imprime descri��o endosso como anexo

        'Imprime cl�usulas, colocando cabe�alho a cada quebra de p�gina
        '-------------------------------------------------------------------
        'If adicionado por Victor NEto - Stefanini 27/10/2006 FLOW - 163185
        '-------------------------------------------------------------------
        If TemClausula Then

            SQL = "SELECT cod_clausula_original, seq_clausula, isnull(texto_clausula,'') as txt_clausula "
            SQL = SQL & "FROM clausula_personalizada_tb cp   WITH (NOLOCK) "
            SQL = SQL & "WHERE cp.proposta_id = " & vPropostaId
            SQL = SQL & " AND isnull(cp.endosso_id, 0) = " & Lblendosso_id.Caption
            SQL = SQL & " ORDER BY cp.seq_clausula"
            '
            Set rc = rdocn.OpenResultset(SQL)
            '
            If Not rc.EOF Then
                'Para cada cl�usula, formatar e imprimir
                Do While Not rc.EOF
                    'Diego - GPTI - 30/07/2008
                    Dim codigoClausula As Double
                    Dim sequencial As Integer
                    Texto = rc("txt_clausula")
                    codigoClausula = rc!cod_clausula_original
                    sequencial = rc!seq_clausula
                    If Val(Trim$(rc!cod_clausula_original)) <> 93011 Then    'Apenas imprime clausula quando n�o for especifica��o da ap�lice.

                        Texto2 = Formata_Clausula(Texto)
                        'INSERIR NA TABELA DO RELAT�RIO - Diego - GPTI - 29/07/2008
                        SQL = "EXEC SEGS7128_SPI '" & Texto2 & "', " & codigoClausula & ", " & sequencial & ", '" & Usuario & "'"
                        Set rc_proc = rdoCn1.OpenResultset(SQL)
                        '                        '' Seta posicao da primeira linha de impressao do texto da clausula
                        '                        ultquebra = 1
                        '                        ind_anexos = 0
                        '                        inicio_linha_anexos = 1
                        '                        fim_linha_anexos = 1
                        '                        tamanho_linha_anexos = 0
                        '                        '' Imprime cl�usula, colocando cabe�alho a cada quebra de p�gina
                        '                        For i = 1 To Len(Texto2)
                        '                            '' Procura 'returns' no texto da clausula para
                        '                            '' contar o no. de linhas; achou CR_LF, passou para nova linha.
                        '                            If Mid(Texto2, i, 2) = Chr(13) & Chr(10) Then
                        '                            ' Movimenta o conteudo da linha para o vetor de linhas_anexo.
                        '                                fim_linha_anexos = i - 1
                        '                                tamanho_linha_anexos = (fim_linha_anexos - inicio_linha_anexos) + 1
                        '                                ind_anexos = ind_anexos + 1
                        '                                linha_anexos(ind_anexos) = Mid(Texto2, inicio_linha_anexos, tamanho_linha_anexos)
                        '                                Report.Formulas(ind_anexos + 5) = "linha_" & CStr(ind_anexos + 2) & "= '" & linha_anexos(ind_anexos) & "'"
                        '                                inicio_linha_anexos = i + 2
                        '                            End If
                        '                            '' Quando chega na linha 71, pula para nova p�gina
                        '                            If ind_anexos = 71 Then
                        '                            '   imprime relatorio - as 71 linhas sao descarregadas na saida e action=1
                        '                                Report.Action = 1
                        '                                ind_anexos = 0
                        '                                aux = Mid(Texto2, ultquebra, i + 1 - ultquebra)
                        '                                ultquebra = i + 2
                        '                                i = i + 1
                        '                            End If 'Quebra de pagina (quando cheia) !
                        '                        Next i
                        '                        '
                        '                        ''' Se ultima frase nao tem CR/LF, n�o estava imprimindo
                        '                        If inicio_linha_anexos < Len(Texto2) Then
                        '                            ind_anexos = ind_anexos + 1
                        '                            linha_anexos(ind_anexos) = Mid(Texto2, inicio_linha_anexos, Len(Texto2) - inicio_linha_anexos)
                        '                            Report.Formulas(ind_anexos + 5) = "linha_" & CStr(ind_anexos + 2) & "= '" & linha_anexos(ind_anexos) & "'"
                        '                        End If
                        '                        '
                        '                        If ultquebra < Len(Texto2) Then
                        '        '                    For ind_formula = 6 To ind_anexos
                        '        '                        Report.Formulas(ind_formula) = "linha_" & CStr(ind_formula - 3) & "= '" & linha_anexos(ind_formula - 5) & "'"
                        '        '                    Next ind_formula
                        '        '                    '
                        '                            If ind_anexos < 71 Then
                        '                                For ind_formula = ind_anexos + 1 To 71
                        '                                    linha_anexos(ind_formula) = Space(90)
                        '                                    Report.Formulas(ind_formula + 5) = "linha_" & CStr(ind_formula + 2) & "= '" & linha_anexos(ind_formula) & "'"
                        '                                Next ind_formula
                        '                            End If
                        '                            '
                        '                            Report.Action = 1
                        '                            ind_anexos = 0
                        '                        End If
                    End If
                    rc.MoveNext
                Loop
                '            Report.Action = 1
            End If
        End If

        'Gmarques - FLOW 696658 - Incluindo comando para imprimir anexos com descri��o de endosso e clausula ou s� descri��o de endosso.
        'In�cio
        Report.Action = 1
        'Fim

    End If

    If MudouPrtDefault Then
        Call RetornaPrtDefault("WIN.INI", OldDefault)
    End If

cancelar:
    If MudouPrtDefault Then
        Call RetornaPrtDefault("WIN.INI", OldDefault)
    End If
    MousePointer = vbDefault
    Exit Sub

Erro:
    If MudouPrtDefault Then
        Call RetornaPrtDefault("WIN.INI", OldDefault)
    End If
    MousePointer = vbDefault
    TrataErroGeral "Impress�o de Ap�lice"
    MsgBox "Houve um erro na impress�o da ap�lice. O processo ser� abortado."
End Sub

Private Sub Monta_GridAdm_endosso()
    Dim linha As String
    Dim vAdm As Administrador

    GridAdmAnt.Rows = 1
    '
    If Not Estipulantes_endosso(TxtRepAnt.Text).Administradores Is Nothing Then
        For Each vAdm In Estipulantes_endosso(TxtRepAnt.Text).Administradores
            linha = vAdm.Id
            linha = linha & vbTab & vAdm.Nome
            linha = linha & vbTab & Format(vAdm.PercProLabore, "0.00")
            linha = linha & vbTab & vAdm.DtIniAdministracao
            linha = linha & vbTab & vAdm.DtFimAdministracao
            '
            GridAdmAnt.AddItem linha
            GridAdmAnt.RowData(GridAdmAnt.Rows - 1) = vAdm.Id
        Next
        '
    End If
    '
    GridAdmAnt.Refresh
End Sub
Private Sub Monta_GridAdm_endosso_fatura(cod_cli As String)
    Dim linha As String
    Dim seq_adm As Integer
    Dim vAdm As Administrador
    Dim vEstip As estipulante

    GridAdmAnt.Rows = 1
    '
    For Each vAdm In Estipulantes_endosso(cod_cli).Administradores
        linha = vAdm.Id
        linha = linha & vbTab & vAdm.Nome
        linha = linha & vbTab & Format(vAdm.PercProLabore, "0.00")
        linha = linha & vbTab & vAdm.DtIniAdministracao
        linha = linha & vbTab & vAdm.DtFimAdministracao
        '
        GridAdmAnt.AddItem linha
        GridAdmAnt.RowData(GridAdmAnt.Rows - 1) = vAdm.Id
    Next
    '

End Sub

Private Sub Prepara_Administracao_Fatura()
'
    Dim rc As rdoResultset
    Dim rc1 As rdoResultset
    Dim SQL As String
    Dim sql1 As String
    Dim linha As String
    Dim vAdm As Administrador
    Dim PercPL As String
    Dim ClienteId As String
    Dim NomeClienteId As String
    Dim PercProLabore As Variant
    Dim DtIniVigencia As String
    Dim DtFimVigencia As String
    Dim SubGrupoFatura As String
    Dim pesq_sub_grupo_fatura As String
    Dim ind_sg As Integer

    On Error GoTo Erro_Administracao_endosso

    GridAdmAnt.Rows = 1
    HaAdministracao = False
    HaRepresentacao = False
    If HaSubGrupos Then
        pesq_sub_grupo_fatura = ", sub_grupo_id "
        cboSubGrupo(1).Locked = False
    End If

    ' Recupera os dados da tabela de faturas - sub_grupo (se for o caso) e
    ' datas de inicio e fim de vigencia.

    sql1 = "SELECT dt_inicio_vigencia, dt_fim_vigencia "
    sql1 = sql1 & pesq_sub_grupo_fatura
    sql1 = sql1 & " FROM fatura_tb   WITH (NOLOCK) "
    sql1 = sql1 & "WHERE proposta_id = " & vPropostaId
    sql1 = sql1 & " AND endosso_id = " & gEndossoId

    Set rc = rdoCn1.OpenResultset(sql1, rdOpenStatic)

    If Not rc.EOF Then
        DtIniVigencia = IIf(IsNull(rc!dt_inicio_vigencia), " ", rc!dt_inicio_vigencia)
        DtFimVigencia = IIf(IsNull(rc!dt_fim_vigencia), " ", rc!dt_fim_vigencia)
        If HaSubGrupos Then
            SubGrupoFatura = IIf(IsNull(rc!sub_grupo_id), "", rc!sub_grupo_id)
        End If
    End If

    '' Recupera os administradores - endosso "financeiro"
    SQL = "SELECT plf.cliente_id, cli.nome, plf.perc_pro_labore "
    SQL = SQL & "FROM cliente_tb cli  WITH (NOLOCK) , "
    SQL = SQL & " pro_labore_endosso_fin_tb plf   WITH (NOLOCK) "
    SQL = SQL & "WHERE plf.proposta_id = " & vPropostaId
    SQL = SQL & " AND plf.endosso_id = " & gEndossoId
    SQL = SQL & " AND cli.cliente_id = plf.cliente_id"
    '
    Set rc1 = rdoCn1.OpenResultset(SQL, rdOpenStatic)

    While Not rc1.EOF
        HaAdministracao = True
        ClienteId = IIf(IsNull(rc1!Cliente_id), " ", rc1!Cliente_id)
        NomeClienteId = IIf(IsNull(rc1!Nome), " ", rc1!Nome)
        PercProLabore = rc1!perc_pro_labore

        '' Monta grid de Administradores.
        linha = ClienteId
        linha = linha & vbTab & NomeClienteId

        If ConfiguracaoBrasil Then
            PercPL = Format(Val(PercProLabore), "0.00")
        Else
            PercPL = TrocaValorAmePorBras(Format(Val(PercProLabore), "0.00"))
        End If

        linha = linha & vbTab & Format(PercPL, "0.00")
        linha = linha & vbTab & Format(DtIniVigencia, "dd/mm/yyyy")
        linha = linha & vbTab & Format(DtFimVigencia, "dd/mm/yyyy")
        '
        GridAdmAnt.AddItem linha
        GridAdmAnt.RowData(GridAdmAnt.Rows - 1) = ClienteId
        rc1.MoveNext
    Wend

    sql1 = ""

    If Not HaSubGrupos Then    ' Representacao sem sub-grupo !
        sql1 = "SELECT est_cliente_id, nome "
        sql1 = sql1 & "FROM representacao_proposta_tb  WITH (NOLOCK) , cliente_tb   WITH (NOLOCK) "
        sql1 = sql1 & "WHERE proposta_id = " & vPropostaId
        sql1 = sql1 & " AND endosso_id = " & gEndossoId
        sql1 = sql1 & " AND cliente_id = est_cliente_id"
    Else                    ' Representacao com sub-grupo !
        If SubGrupoFatura <> "" Then
            sql1 = "SELECT est_cliente_id, CLIENTE_TB.nome                                      "
            sql1 = sql1 & "FROM representacao_sub_grupo_tb  WITH (NOLOCK)                              "
            sql1 = sql1 & " INNER JOIN  cliente_tb   WITH (NOLOCK)         ON                          "
            sql1 = sql1 & "             cliente_id              = est_cliente_id                "
            sql1 = sql1 & "WHERE representacao_sub_grupo_tb.apolice_id             = " & vApoliceId
            sql1 = sql1 & " AND representacao_sub_grupo_tb.sucursal_seguradora_id  = " & vSucursalId
            sql1 = sql1 & " AND representacao_sub_grupo_tb.seguradora_cod_susep    = " & vSeguradoraId
            sql1 = sql1 & " AND representacao_sub_grupo_tb.ramo_id                 = " & vRamoId
            sql1 = sql1 & " AND representacao_sub_grupo_tb.sub_grupo_id            = " & SubGrupoFatura

        End If
    End If

    ' Recupera��o dos dados do representante / estipulante.

    If sql1 <> "" Then
        Set rc = rdoCn1.OpenResultset(sql1, rdOpenStatic)
        '
        If Not rc.EOF Then
            '
            HaRepresentacao = True
            TxtRepAnt.Text = rc!est_cliente_id
            TxtNomeRepAnt.Text = Trim(rc!Nome)
            '
        End If
        Set rc = Nothing

    End If
    '' Bloqueia o uso da combo de Sub-Grupo(1) !
    If SubGrupoFatura = "" Then
        cboSubGrupo(1).ListIndex = -1
        TxtRepAnt.Enabled = False
        TxtNomeRepAnt.Enabled = False
    Else
        For ind_sg = 0 To cboSubGrupo(1).ListCount - 1
            If cboSubGrupo(1).ItemData(ind_sg) = Val(SubGrupoFatura) Then
                cboSubGrupo(1).ListIndex = ind_sg
                Exit For
            End If
        Next ind_sg
    End If
    cboSubGrupo(1).Locked = True
    '
    'If HaAdministracao Or HaRepresentacao Then
    tabEndossos.TabEnabled(TABendosso_ADMINISTRACAO) = True
    'Else
    '    tabEndossos.TabEnabled(TABendosso_ADMINISTRACAO) = False
    'End If

    Exit Sub

Erro_Administracao_endosso:
    TrataErroGeral "Prepara_Administra��o_Fatura"
    MsgBox "Erro na rotina Prepara_Administra��o_Fatura."
    '

End Sub

Private Sub Prepara_Administracao_Geral()
'
    Dim rc As rdoResultset
    Dim rc1 As rdoResultset
    Dim SQL As String
    Dim linha As String
    Dim vAdm As Administrador
    Dim PercPL As String
    Dim DtIniAdm As String
    Dim DataEmissaoEndosso As String

    On Error GoTo Erro_Administracao_endosso

    HaAdministracao = False
    DataEmissaoEndosso = Format(dtInicioVigEndosso, "yyyymmdd")
    '
    If Not HaSubGrupos Then
        SQL = "SELECT est_cliente_id, nome "
        SQL = SQL & "FROM representacao_proposta_tb  WITH (NOLOCK) , cliente_tb   WITH (NOLOCK) "
        SQL = SQL & "WHERE proposta_id = " & vPropostaId
        SQL = SQL & " AND endosso_id = " & gEndossoId
        SQL = SQL & " AND cliente_id = est_cliente_id"
        '
        Set rc = rdoCn1.OpenResultset(SQL, rdOpenStatic)
        '
        While Not rc.EOF
            '
            Set vEstip = New estipulante
            '
            vEstip.Id = rc!est_cliente_id
            vEstip.Nome = Trim(rc!Nome)
            TxtRepAnt.Text = rc!est_cliente_id
            TxtNomeRepAnt.Text = Trim(rc!Nome)
            '
            '' Seleciona os administradores ligados ao estipulante
            SQL = "SELECT adm_cliente_id, nome, perc_pro_labore "
            SQL = SQL & ", dt_inicio_administracao,dt_fim_administracao "
            SQL = SQL & "FROM administracao_apolice_tb  WITH (NOLOCK) , cliente_tb   WITH (NOLOCK)  "
            'Rafael Oshiro 11/02/2005 - Vari�vel vem vazia
            'SQL = SQL & "WHERE proposta_id = " & num_proposta_tb
            SQL = SQL & "WHERE proposta_id = " & vPropostaId
            SQL = SQL & " AND endosso_id = " & gEndossoId
            SQL = SQL & " AND est_cliente_id = " & rc!est_cliente_id
            SQL = SQL & " AND cliente_id = adm_cliente_id"
            '
            Set rc1 = rdoCn1.OpenResultset(SQL, rdOpenStatic)
            If Not rc1.EOF Then
                Set vEstip.Administradores = New Collection
                '
                While Not rc1.EOF
                    Set vAdm = New Administrador
                    vAdm.Id = rc1!adm_cliente_id
                    vAdm.Nome = Trim(rc1!Nome)
                    If ConfiguracaoBrasil Then
                        PercPL = Format(Val(rc1!perc_pro_labore), "0.00")
                    Else
                        PercPL = TrocaValorAmePorBras(Format(Val(rc1!perc_pro_labore), "0.00"))
                    End If
                    vAdm.PercProLabore = PercPL
                    vAdm.DtIniAdministracao = Format(rc1!dt_inicio_administracao, "dd/mm/yyyy")
                    vAdm.DtFimAdministracao = Format(rc1!dt_fim_administracao, "dd/mm/yyyy")
                    '
                    vEstip.Administradores.Add vAdm, CStr(rc1!adm_cliente_id)
                    HaAdministracao = True
                    rc1.MoveNext
                Wend
                '
            End If
            Estipulantes_endosso.Add vEstip, CStr(rc!est_cliente_id)
            '
            rc.MoveNext
        Wend
        '
        If HaAdministracao Then
            Call Monta_GridAdm_endosso
        End If
        '
        rc.Close
        Set rc = Nothing
        '
    Else
        SQL = "SELECT representacao_sub_grupo_tb.sub_grupo_id, est_cliente_id, cliente_tb.nome, dt_inicio_representacao "
        SQL = SQL & "FROM representacao_sub_grupo_tb  WITH (NOLOCK)  "

        SQL = SQL & " INNER JOIN  cliente_tb   WITH (NOLOCK)         ON                          "
        SQL = SQL & "             cliente_tb.cliente_id               = est_cliente_id                "
        SQL = SQL & "WHERE representacao_sub_grupo_tb.apolice_id                       = " & vApoliceId
        SQL = SQL & " AND representacao_sub_grupo_tb.sucursal_seguradora_id            = " & vSucursalId
        SQL = SQL & " AND representacao_sub_grupo_tb.seguradora_cod_susep              = " & vSeguradoraId
        SQL = SQL & " AND representacao_sub_grupo_tb.ramo_id                           = " & vRamoId
        SQL = SQL & " AND representacao_sub_grupo_tb.dt_inicio_representacao           <= '" & DataEmissaoEndosso & "'"
        SQL = SQL & " AND (representacao_sub_grupo_tb.dt_fim_representacao             is null "
        SQL = SQL & "      OR representacao_sub_grupo_tb.dt_fim_representacao >= '" & DataEmissaoEndosso & "')"

        '
        Set rc = rdoCn1.OpenResultset(SQL, rdOpenStatic)
        '
        While Not rc.EOF
            Set vEstip = New estipulante
            '
            vEstip.Id = rc!est_cliente_id
            vEstip.Nome = Trim(rc!Nome)

            '' Seleciona os administradores ligados ao estipulante
            ' romulo.barbosa (Nova Consultoria) 17/08/2011
            ' Demanda 864186 - Melhorias no Processo de Emiss�o de Ap�lice
            ' Cria��o e preenchimento de tabela auxiliar para dados da tabela adm_sub_grupo_apolice_tb

            SQL = " SELECT adm_sub_grupo_apolice_tb.sub_grupo_id, adm_cliente_id, cliente_tb.nome  "
            SQL = SQL & " , perc_pro_labore, dt_inicio_administracao   "
            SQL = SQL & " , dt_fim_administracao   "

            'inserindo a consulta de adm_sub_grupo_apolice_tb para a tabela temporaria #adm_sub_grupo_apolice
            SQL = SQL & " INTO #adm_sub_grupo_apolice  "
            SQL = SQL & " FROM adm_sub_grupo_apolice_tb  WITH (NOLOCK)     "
            SQL = SQL & " INNER JOIN  cliente_tb   WITH (NOLOCK)         ON              "
            SQL = SQL & " cliente_tb.cliente_id = adm_cliente_id                    "
            SQL = SQL & " INNER JOIN  sub_grupo_apolice_tb     WITH(NOLOCK)                               "
            SQL = SQL & " ON sub_grupo_apolice_tb.apolice_id              =  adm_sub_grupo_apolice_tb.apolice_id                       "
            SQL = SQL & " AND sub_grupo_apolice_tb.ramo_id                =  adm_sub_grupo_apolice_tb.ramo_id                         "
            SQL = SQL & " AND sub_grupo_apolice_tb.sucursal_seguradora_id =  adm_sub_grupo_apolice_tb.sucursal_seguradora_id          "
            SQL = SQL & " AND sub_grupo_apolice_tb.seguradora_cod_susep   =  adm_sub_grupo_apolice_tb.seguradora_cod_susep            "
            SQL = SQL & " AND sub_grupo_apolice_tb.sub_grupo_id           =  adm_sub_grupo_apolice_tb.sub_grupo_id                     "
            SQL = SQL & " AND sub_grupo_apolice_tb.dt_inicio_vigencia_sbg =  adm_sub_grupo_apolice_tb.dt_inicio_vigencia_sbg    "
            SQL = SQL & " AND sub_grupo_apolice_tb.dt_inicio_vigencia_sbg IS NULL   "
            SQL = SQL & " WHERE adm_sub_grupo_apolice_tb.apolice_id              =  " & vApoliceId
            SQL = SQL & " AND adm_sub_grupo_apolice_tb.sucursal_seguradora_id   =  " & vSucursalId
            SQL = SQL & " AND adm_sub_grupo_apolice_tb.seguradora_cod_susep     =  " & vSeguradoraId
            SQL = SQL & " AND adm_sub_grupo_apolice_tb.ramo_id                  =  " & vRamoId
            SQL = SQL & " AND adm_sub_grupo_apolice_tb.sub_grupo_id             =  " & rc!sub_grupo_id
            SQL = SQL & " AND est_cliente_id                                    =  " & rc!est_cliente_id
            SQL = SQL & " AND dt_inicio_administracao                           <= ' " & DataEmissaoEndosso & " '  "
            SQL = SQL & " AND (dt_fim_administracao                             is null   "
            SQL = SQL & " OR dt_fim_administracao >= ' " & DataEmissaoEndosso & "  ')  "
            SQL = SQL & " AND dt_inicio_representacao = ' " & Format(rc!dt_inicio_representacao, yyyymmdd) & "  '  "

            If DateDiff("d", gDtIniVigEndosso, Date) < 0 Then
                ' Criando uma tabela temporaria com os dados da endosso_vigencia_futura_tb referente a mudan�a, nome da tabela #adm_sub_grupo_apolice_futuro.
                SQL = SQL & " SELECT CAST(MAX(CASE WHEN nome_campo = 'sub_grupo_id' THEN valor_campo END) AS INT ) sub_grupo_id   "
                SQL = SQL & " , CAST(MAX(CASE WHEN nome_campo = 'adm_cliente_id' THEN valor_campo END) AS INT) adm_cliente_id  "
                SQL = SQL & " , CAST(MAX(CASE WHEN nome_campo = 'perc_pro_labore' THEN valor_campo END) AS NUMERIC(9,0)) perc_pro_labore  "
                SQL = SQL & " , CAST(MAX(CASE WHEN nome_campo = 'dt_inicio_administracao' THEN valor_campo END) AS SMALLDATETIME) dt_inicio_administracao  "
                SQL = SQL & " , CAST(MAX(CASE WHEN nome_campo = 'dt_fim_administracao' THEN valor_campo END) AS SMALLDATETIME) dt_fim_administracao  "
                SQL = SQL & " , CAST(MAX(CASE WHEN nome_campo = 'apolice_id' THEN valor_campo END) AS numeric(9,0)) apolice_id  "
                SQL = SQL & " , CAST(MAX(CASE WHEN nome_campo = 'ramo_id' THEN valor_campo END) AS INT) ramo_id "
                SQL = SQL & " , CAST(MAX(CASE WHEN nome_campo = 'sucursal_seguradora_id' THEN valor_campo END) AS numeric(5,0)) sucursal_seguradora_id "
                SQL = SQL & " , CAST(MAX(CASE WHEN nome_campo = 'seguradora_cod_susep' THEN valor_campo END) AS numeric(5,0)) seguradora_cod_susep   "
                SQL = SQL & " into #adm_sub_grupo_apolice_futuro  "
                SQL = SQL & " FROM endosso_vigencia_futura_tb WITH(NOLOCK)  "
                SQL = SQL & " WHERE endosso_id =  " & CInt(Lblendosso_id.Caption)
                SQL = SQL & " AND nome_procedure =  'adm_sub_grupo_apolice_spi'  "
                SQL = SQL & " GROUP BY execucao_id, nome_procedure  "

                ' Inserindo os dados da tabela temporatia #adm_sub_grupo_apolice_futuro com os dados do join para a tabela #adm_sub_grupo_apolice
                SQL = SQL & " INSERT #adm_sub_grupo_apolice  "
                SQL = SQL & " select asgaf.sub_grupo_id,  "
                SQL = SQL & " asgaf.adm_cliente_id,  "
                SQL = SQL & " c.nome,  "
                SQL = SQL & " asgaf.perc_pro_labore,  "
                SQL = SQL & " asgaf.dt_inicio_administracao ,  "
                SQL = SQL & " asgaf.dt_fim_administracao  "
                SQL = SQL & " FROM #adm_sub_grupo_apolice_futuro asgaf   "
                SQL = SQL & " JOIN cliente_tb c  WITH(NOLOCK) "
                SQL = SQL & " ON c.cliente_id = asgaf.adm_cliente_id  "

                SQL = SQL & " WHERE asgaf.apolice_id              =   " & vApoliceId
                SQL = SQL & " AND asgaf.sucursal_seguradora_id   =   " & vSucursalId
                SQL = SQL & " AND asgaf.seguradora_cod_susep     =   " & vSeguradoraId
                SQL = SQL & " AND asgaf.ramo_id                  =   " & vRamoId
                SQL = SQL & " AND asgaf.sub_grupo_id             =   " & rc!sub_grupo_id


                ' Excluindo da #adm_sub_grupo_apolice se estiver na tabela endosso_vigencia_futura_tb o endosso
                SQL = SQL & " DELETE asga  "
                SQL = SQL & " FROM #adm_sub_grupo_apolice asga  "
                SQL = SQL & " JOIN (SELECT CAST(MAX(CASE WHEN nome_campo = 'sub_grupo_id' THEN valor_campo END) AS INT ) sub_grupo_id   "
                SQL = SQL & " , CAST(MAX(CASE WHEN nome_campo = 'adm_cliente_id' THEN valor_campo END) AS INT) adm_cliente_id  "
                SQL = SQL & " , CAST(MAX(CASE WHEN nome_campo = 'dt_endosso' THEN valor_campo END) AS SMALLDATETIME) dt_endosso  "
                SQL = SQL & " FROM endosso_vigencia_futura_tb  WITH(NOLOCK) "
                SQL = SQL & " WHERE endosso_id = " & CInt(Lblendosso_id.Caption)
                SQL = SQL & " AND nome_procedure = 'endo_adm_sub_grupo_apolice_spd'  "
                SQL = SQL & " GROUP BY execucao_id, nome_procedure) asga_del  "
                SQL = SQL & " ON asga.sub_grupo_id = asga_del.sub_grupo_id  "
                SQL = SQL & " AND asga.adm_cliente_id = asga_del.adm_cliente_id  "

                SQL = SQL & " DROP TABLE #adm_sub_grupo_apolice_futuro  "
            End If

            ' Listando os dados com as altera��es da tabela #adm_sub_grupo_apolice
            SQL = SQL & " SELECT sub_grupo_id,  "
            SQL = SQL & " adm_cliente_id,  "
            SQL = SQL & " nome,  "
            SQL = SQL & " perc_pro_labore,  "
            SQL = SQL & " dt_inicio_administracao ,  "
            SQL = SQL & " dt_fim_administracao  "
            SQL = SQL & " FROM #adm_sub_grupo_apolice  WITH(NOLOCK) "

            SQL = SQL & " DROP TABLE #adm_sub_grupo_apolice  "


            Set rc1 = rdocn.OpenResultset(SQL)

            If Not rc1.EOF Then
                Set vEstip.Administradores = New Collection
                '
                While Not rc1.EOF
                    Set vAdm = New Administrador
                    vAdm.Id = rc1!adm_cliente_id
                    vAdm.Nome = Trim(rc1!Nome)
                    If ConfiguracaoBrasil Then
                        PercPL = Format(Val(rc1!perc_pro_labore), "0.00")
                    Else
                        PercPL = TrocaValorAmePorBras(Format(Val(rc1!perc_pro_labore), "0.00"))
                    End If
                    vAdm.PercProLabore = PercPL
                    vAdm.DtIniAdministracao = Format(rc1!dt_inicio_administracao, "dd/mm/yyyy")
                    vAdm.DtFimAdministracao = Format(rc1!dt_fim_administracao, "dd/mm/yyyy")
                    '
                    vEstip.Administradores.Add vAdm, CStr(rc1!adm_cliente_id)
                    HaAdministracao = True
                    rc1.MoveNext
                Wend

            End If
            rc1.Close
            Set rc1 = Nothing
            '
            SubGrupos_endosso(CStr(rc!sub_grupo_id)).Estipulantes.Add vEstip, CStr(rc!est_cliente_id)
            '
            rc.MoveNext
        Wend
        '
        rc.Close
        Set rc = Nothing
    End If
    '
    If HaAdministracao Then
        tabEndossos.TabEnabled(TABendosso_ADMINISTRACAO) = True
    Else
        tabEndossos.TabEnabled(TABendosso_ADMINISTRACAO) = False
    End If

    Exit Sub

Erro_Administracao_endosso:
    TrataErroGeral "Prepara_Administra��o_Geral"
    MsgBox "Erro na rotina Prepara_Administra��o_Geral."
    '
End Sub
Private Sub Prepara_Beneficiario()
    Dim rc As rdoResultset
    Dim SQL_benef As String
    Dim Beneficiario As Variant

    On Error GoTo Erro

    TxtBeneficiario.Text = ""
    '
    SQL_benef = "SELECT descricao FROM texto_beneficiario_tb   WITH (NOLOCK) "
    SQL_benef = SQL_benef & "WHERE proposta_id = " & vPropostaId
    '
    Set rc = rdoCn1.OpenResultset(SQL_benef, rdOpenStatic)
    '
    If Not rc.EOF Then
        Beneficiario = rc!Descricao
        If IsNull(Beneficiario) Or Beneficiario = " " Then
            TxtBeneficiario.Text = " "
            tabEndossos.TabEnabled(TABendosso_BENEFICIARIOS) = False
        Else
            TxtBeneficiario.Text = Beneficiario
            tabEndossos.TabEnabled(TABendosso_BENEFICIARIOS) = True
        End If
    End If
    '
    Exit Sub

Erro:
    TrataErroGeral "Prepara_Beneficiario"
    MsgBox "Erro na rotina Prepara_Beneficiario"

End Sub
Private Sub Prepara_Clausulas()
    Dim vClausula As Clausula
    Dim vClauAnt As Clausula
    Dim SQL As String
    Dim rs As rdoResultset
    Dim aux_clau As String
    Dim cont_clau As Integer
    Dim txtPrimeiraClausula As String
    Dim texto_saida As Variant

    On Error GoTo Erro_Le_Clausula

    If num_endosso_consulta = "" Then
        num_endosso_consulta = gEndossoId
    End If

    Set Clausulas_endosso = Nothing

    SQL = "SELECT cp.seq_clausula, c.descr_clausula, cp.cod_clausula_original "
    SQL = SQL & ", cp.dt_inicio_vigencia, cp.texto_clausula "
    SQL = SQL & ", cp.dt_fim_vigencia "
    SQL = SQL & " FROM clausula_personalizada_tb cp  WITH (NOLOCK) , clausula_tb c   WITH (NOLOCK) "
    SQL = SQL & " WHERE proposta_id = " & vPropostaId
    'sql = sql & " AND   endosso_id = " & num_endosso_consulta
    SQL = SQL & " AND   endosso_id = " & gEndossoId
    SQL = SQL & " AND   c.cod_clausula = cp.cod_clausula_original  "
    SQL = SQL & " AND   c.dt_inicio_vigencia = cp.dt_inicio_vigencia_cl_original  "
    SQL = SQL & " ORDER BY seq_clausula "

    '
    Set rs = rdoCn1.OpenResultset(SQL, rdOpenStatic)
    '
    Do While Not rs.EOF
        ''
        cont_clau = cont_clau + 1
        texto_saida = rs!texto_clausula
        '
        If cont_clau = 1 Then
            txtPrimeiraClausula = texto_saida
        End If
        '
        Set vClausula = New Clausula
        '
        With vClausula
            .Id = rs!seq_clausula
            .Codigo = rs!cod_clausula_original
            .Nome = Trim(rs!descr_clausula)
            If Not IsNull(rs!dt_inicio_vigencia) Then
                .DtIniVigencia = rs!dt_inicio_vigencia
            End If
            If Not IsNull(rs!dt_fim_vigencia) Then
                .DtFimVigencia = rs!dt_fim_vigencia
            End If
            .Descricao = IIf(IsNull(texto_saida), " ", texto_saida)
        End With
        '
        Clausulas_endosso.Add vClausula, CStr(cont_clau)
        '
        rs.MoveNext
    Loop
    ''
    Call Monta_GridClausulas_endosso
    '
    If cont_clau >= 1 Then
        txtClausula.Text = txtPrimeiraClausula
    End If
    '
    rs.Close
    '
    Exit Sub

Erro_Le_Clausula:
    TrataErroGeral "Erro lendo cl�usulas da ap�lice!"
    MsgBox "Rotina : Prepara_Clausulas "
    '    Exit Sub

End Sub


Private Sub Prepara_Coberturas()
    Dim vSubGrupo As SubGrupo
    Dim rc As rdoResultset
    Dim SQL_cob As String
    Dim linha As String
    Dim prim_comp As Long

    On Error GoTo Erro_Coberturas_endosso

    If Not HaSubGrupos Then
        SQL_cob = "SELECT e.tp_componente_id, e.tp_cob_comp_id, cc.tp_cobertura_id, c.nome, "
        SQL_cob = SQL_cob & " e.val_is, e.val_premio, e.dt_inicio_vigencia_seg, "
        SQL_cob = SQL_cob & " e.dt_inicio_vigencia_esc, e.dt_fim_vigencia_esc "
        SQL_cob = SQL_cob & ", e.val_taxa, e.perc_basica "
        If UCase(gTpEmissao) <> "A" Then
            SQL_cob = SQL_cob & ", e.class_tp_cobertura "
        Else
            SQL_cob = SQL_cob & ", null class_tp_cobertura "
        End If

        '*** Abosco @ 2003 mai 06
        '*** lista de componentes quando h� endosso de exclusao de coberturas
        SQL_cob = SQL_cob & " , e.tp_componente_id , cp.nome nome_componente "

        SQL_cob = SQL_cob & "FROM " & TabEscolha & " e WITH(NOLOCK) , tp_cob_comp_tb cc  WITH (NOLOCK) , tp_cobertura_tb c   WITH (NOLOCK) "

        '*** Abosco @ 2003 abr 24
        '*** inclusao de inicio de vigencia em tp_cob_comp_item_tb
        SQL_cob = SQL_cob & " , tp_cob_comp_item_tb ci   WITH (NOLOCK) "

        '*** Abosco @ 2003 mai 06
        '*** lista de componentes quando h� endosso de exclusao de coberturas
        SQL_cob = SQL_cob & " , tp_componente_tb cp   WITH (NOLOCK) "

        SQL_cob = SQL_cob & " WHERE e.proposta_id = " & vPropostaId
        '    SQL_cob = SQL_cob & " AND escolha_sub_grp_tp_cob_comp_tb.dt_fim_vigencia_esc is null "
        SQL_cob = SQL_cob & " AND e.endosso_id = " & gEndossoId
        SQL_cob = SQL_cob & " AND cc.tp_cob_comp_id = e.tp_cob_comp_id "
        SQL_cob = SQL_cob & " AND c.tp_cobertura_id = cc.tp_cobertura_id "

        '*** Abosco @ 2003 abr 24
        '*** inclusao de inicio de vigencia em tp_cob_comp_item_tb
        SQL_cob = SQL_cob & " AND cc.tp_cob_comp_id = ci.tp_cob_comp_id "
        SQL_cob = SQL_cob & " AND e.dt_inicio_vigencia_comp = ci.dt_inicio_vigencia_comp "
        'SQL_cob = SQL_cob & " AND tp_cob_comp_item_tb.dt_fim_vigencia_comp IS NULL "

        '*** Abosco @ 2003 mai 06
        '*** lista de componentes quando h� endosso de exclusao de coberturas
        SQL_cob = SQL_cob & " AND e.tp_componente_id = cp.tp_componente_id "

        SQL_cob = SQL_cob & " ORDER BY e.tp_componente_id "
        '
        Set rc = rdoCn1.OpenResultset(SQL_cob, rdOpenStatic)
        '
        If Not rc.EOF Then
            prim_comp = rc!tp_componente_id
            tabEndossos.TabEnabled(TABendosso_COBERTURAS) = True
            Call Monta_Colecao_Coberturas(rc)
            Call MontarComboComponentes
            Call Monta_GridCoberturas_endosso(prim_comp)
        End If
    Else

        For Each vSubGrupo In SubGrupos_endosso

            ' romulo.barbosa (Nova Consultoria) 17/08/2011
            ' Demanda 864186 - Melhorias no Processo de Emiss�o de Ap�lice
            ' Cria��o e preenchimento de tabela auxiliar para dados da tabela escolha_sub_grp_tp_cob_comp_tb
            SQL = " SELECT tp_cob_comp_TB.tp_componente_id, escolha_sub_grp_tp_cob_comp_TB.tp_cob_comp_id , tp_cob_comp_TB.tp_cobertura_id, "
            SQL = SQL & " tp_cobertura_TB.nome, escolha_sub_grp_tp_cob_comp_TB.val_is , escolha_sub_grp_tp_cob_comp_TB.val_premio, "
            SQL = SQL & " CAST(escolha_sub_grp_tp_cob_comp_TB.val_taxa AS NUMERIC(15,7)) AS val_taxa, escolha_sub_grp_tp_cob_comp_TB.perc_basica, "
            SQL = SQL & " isnull(escolha_sub_grp_tp_cob_comp_TB.class_tp_cobertura, 'a') class_tp_cobertura "
            'inserindo a consulta de escolha_sub_grp_tp_cob_comp_tb para a tabela temporaria #escolha_sub_grp_tp_cob_comp
            SQL = SQL & " INTO #escolha_sub_grp_tp_cob_comp "
            SQL = SQL & " FROM escolha_sub_grp_tp_cob_comp_TB  WITH (NOLOCK)  "
            SQL = SQL & " INNER JOIN tp_cob_comp_TB  WITH (NOLOCK)  "
            SQL = SQL & " ON tp_cob_comp_TB.tp_cob_comp_id = escolha_sub_grp_tp_cob_comp_TB.tp_cob_comp_id "
            SQL = SQL & " INNER JOIN tp_cobertura_TB  WITH (NOLOCK)  "
            SQL = SQL & " ON tp_cobertura_TB.tp_cobertura_id = tp_cob_comp_TB.tp_cobertura_id "
            SQL = SQL & " INNER JOIN tp_cob_comp_item_TB  WITH (NOLOCK)  "
            SQL = SQL & " ON tp_cob_comp_TB.tp_cob_comp_id = tp_cob_comp_item_TB.tp_cob_comp_id "
            SQL = SQL & "  WHERE     escolha_sub_grp_tp_cob_comp_tb.apolice_id             = " & vApoliceId
            SQL = SQL & "  AND    escolha_sub_grp_tp_cob_comp_tb.sucursal_seguradora_id = " & vSucursalId
            SQL = SQL & "  AND    escolha_sub_grp_tp_cob_comp_tb.seguradora_cod_susep   = " & vSeguradoraId
            SQL = SQL & "  AND    escolha_sub_grp_tp_cob_comp_tb.ramo_id                = " & vRamoId
            SQL = SQL & "  AND    escolha_sub_grp_tp_cob_comp_tb.sub_grupo_id           = " & vSubGrupo.Id
            SQL = SQL & " AND escolha_sub_grp_tp_cob_comp_TB.dt_inicio_vigencia_comp = tp_cob_comp_item_TB.dt_inicio_vigencia_comp "
            SQL = SQL & " ORDER BY tp_cob_comp_TB.tp_componente_id "

            If DateDiff("d", gDtIniVigEndosso, Date) < 0 Then
                ' Criando uma tabela temporaria com os dados da endosso_vigencia_futura_tb referente a mudan�a, nome da tabela #escolha_sub_grp_tp_cob_comp_futuro.
                SQL = SQL & " SELECT CAST(MAX(CASE WHEN nome_campo = 'tp_cob_comp_id' THEN valor_campo END) AS smallint ) tp_cob_comp_id , "
                SQL = SQL & " CAST(MAX(CASE WHEN nome_campo = 'val_is' THEN valor_campo END) AS numeric(15,2)) val_is , "
                SQL = SQL & " CAST(MAX(CASE WHEN nome_campo = 'val_premio' THEN valor_campo END) AS numeric(15,2)) val_premio , "
                SQL = SQL & " CAST(MAX(CASE WHEN nome_campo = 'val_taxa' THEN valor_campo END) AS numeric(15,7)) val_taxa , "
                SQL = SQL & " CAST(MAX(CASE WHEN nome_campo = 'perc_basica' THEN valor_campo END) AS numeric(9,6)) perc_basica , "
                SQL = SQL & " isnull(CAST(MAX(CASE WHEN nome_campo = 'class_tp_cobertura' THEN valor_campo END) AS char(1)),'a') class_tp_cobertura , "
                SQL = SQL & " CAST(MAX(CASE WHEN nome_campo = 'dt_inicio_vigencia_comp' THEN valor_campo END) AS smalldatetime)  dt_inicio_vigencia_comp , "
                SQL = SQL & " CAST(MAX(CASE WHEN nome_campo = 'sub_grupo_id' THEN valor_campo END) AS numeric(9,6)) sub_grupo_id , "
                SQL = SQL & " CAST(MAX(CASE WHEN nome_campo = 'apolice_id' THEN valor_campo END) AS numeric(9,0)) apolice_id , "
                SQL = SQL & " CAST(MAX(CASE WHEN nome_campo = 'sucursal_id' THEN valor_campo END) AS numeric(5)) sucursal_seguradora_id , "
                SQL = SQL & " CAST(MAX(CASE WHEN nome_campo = 'ramo_id' THEN valor_campo END) AS tinyint) ramo_id , "
                SQL = SQL & " CAST(MAX(CASE WHEN nome_campo = 'seguradora_id' THEN valor_campo END) AS numeric(5)) seguradora_cod_susep  "
                SQL = SQL & " into #escolha_sub_grp_tp_cob_comp_futuro "
                SQL = SQL & " FROM endosso_vigencia_futura_TB WITH(NOLOCK) "
                SQL = SQL & " WHERE endosso_id = " & CInt(Lblendosso_id.Caption)
                SQL = SQL & " AND nome_procedure = 'escolha_tp_cob_sub_grupo_spi' "
                SQL = SQL & " AND dt_aplicacao IS NULL "
                SQL = SQL & " GROUP BY execucao_id, nome_procedure  "

                ' Inserindo os dados da tabela temporatia #escolha_sub_grp_tp_cob_comp_futuro com os dados do join para a tabela #escolha_sub_grp_tp_cob_comp "
                SQL = SQL & " INSERT #escolha_sub_grp_tp_cob_comp "
                SQL = SQL & " SELECT tcc.tp_componente_id, esgf.tp_cob_comp_id, tcc.tp_cobertura_id, tc.nome, esgf.val_is, esgf.val_premio,  "
                SQL = SQL & " esgf.val_taxa , esgf.perc_basica, esgf.class_tp_cobertura "
                SQL = SQL & " FROM #escolha_sub_grp_tp_cob_comp_futuro esgf  "
                SQL = SQL & " JOIN tp_cob_comp_TB tcc  WITH (NOLOCK)  "
                SQL = SQL & " ON esgf.tp_cob_comp_id = tcc.tp_cob_comp_id "
                SQL = SQL & " JOIN tp_cobertura_TB tc  WITH (NOLOCK)  "
                SQL = SQL & " ON tcc.tp_cobertura_id = tc.tp_cobertura_id "
                SQL = SQL & " INNER JOIN tp_cob_comp_item_TB tci  WITH (NOLOCK)  "
                SQL = SQL & " ON TCC.tp_cob_comp_id = tci.tp_cob_comp_id "
                SQL = SQL & " WHERE esgf.dt_inicio_vigencia_comp = tci.dt_inicio_vigencia_comp "
                SQL = SQL & " AND esgf.sub_grupo_id = " & vSubGrupo.Id
                SQL = SQL & " AND esgf.apolice_id = " & vApoliceId
                SQL = SQL & " AND esgf.sucursal_seguradora_id = " & vSucursalId
                SQL = SQL & " AND esgf.seguradora_cod_susep = " & vSeguradoraId
                SQL = SQL & " AND esgf.ramo_id = " & vRamoId
                ' Excluindo da #escolha_sub_grp_tp_cob_comp se estiver na tabela endosso_vigencia_futura_tb o endosso
                SQL = SQL & " DELETE esgt "
                SQL = SQL & " FROM #escolha_sub_grp_tp_cob_comp esgt JOIN ( "
                SQL = SQL & " SELECT CAST(MAX(CASE WHEN nome_campo = 'tp_cob_comp_id' THEN valor_campo END) AS INT ) tp_cob_comp_id "
                SQL = SQL & " FROM endosso_vigencia_futura_TB WITH(NOLOCK) "
                SQL = SQL & " WHERE endosso_id = " & CInt(Lblendosso_id.Caption)
                SQL = SQL & " AND nome_procedure = 'escolha_tp_cob_sub_grupo_spd' "
                SQL = SQL & " AND dt_aplicacao IS NULL "
                SQL = SQL & " GROUP BY execucao_id, nome_procedure) esgt_del "
                SQL = SQL & " ON esgt.tp_cob_comp_id = esgt_del.tp_cob_comp_id "
                ' Alterando #escolha_sub_grp_tp_cob_comp se estiver com dados na endosso_vigencia_futura_tb a procedure de update(escolha_tp_cob_sub_grupo_spu)
                SQL = SQL & " UPDATE esgt "
                SQL = SQL & " SET esgt.val_is = esgt_upd.val_is , esgt.val_premio = esgt_upd.val_premio , esgt.val_taxa = esgt_upd.val_taxa , esgt.perc_basica = esgt_upd.perc_basica , esgt.class_tp_cobertura = esgt_upd.class_tp_cobertura "
                SQL = SQL & " FROM #escolha_sub_grp_tp_cob_comp esgt JOIN ( "
                SQL = SQL & " SELECT CAST(MAX(CASE WHEN nome_campo = 'tp_cob_comp_id' THEN valor_campo END) AS smallint ) tp_cob_comp_id  "
                SQL = SQL & " , CAST(MAX(CASE WHEN nome_campo = 'val_is' THEN valor_campo END) AS numeric(15,2)) val_is "
                SQL = SQL & " , CAST(MAX(CASE WHEN nome_campo = 'val_premio' THEN valor_campo END) AS numeric(15,2)) val_premio  "
                SQL = SQL & " , CAST(MAX(CASE WHEN nome_campo = 'val_taxa' THEN valor_campo END) AS numeric(15,7)) val_taxa  "
                SQL = SQL & " , CAST(MAX(CASE WHEN nome_campo = 'perc_basica' THEN valor_campo END) AS numeric(9,6)) perc_basica "
                SQL = SQL & " , isnull(CAST(MAX(CASE WHEN nome_campo = 'class_tp_cobertura' THEN valor_campo END) AS char(1)),'a') class_tp_cobertura "
                SQL = SQL & " FROM endosso_vigencia_futura_TB WITH(NOLOCK) "
                SQL = SQL & " WHERE endosso_id = " & CInt(Lblendosso_id.Caption)
                SQL = SQL & " AND nome_procedure = 'escolha_tp_cob_sub_grupo_spu' "
                SQL = SQL & "  AND dt_aplicacao IS NULL "
                SQL = SQL & "  GROUP BY execucao_id, nome_procedure) esgt_upd "
                SQL = SQL & "  ON esgt_upd.tp_cob_comp_id = esgt.tp_cob_comp_id "
                SQL = SQL & "  drop table #escolha_sub_grp_tp_cob_comp_futuro "
            End If

            ' Listando os dados com as altera��es da tabela #escolha_sub_grp_tp_cob_comp
            SQL = SQL & "  SELECT tp_componente_id, tp_cob_comp_id, tp_cobertura_id, nome, val_is, val_premio, val_taxa , perc_basica, class_tp_cobertura "
            SQL = SQL & "  FROM #escolha_sub_grp_tp_cob_comp WITH(NOLOCK)"
            SQL = SQL & "  ORDER BY tp_componente_id "

            SQL = SQL & "  drop table #escolha_sub_grp_tp_cob_comp "
            '''' romulo.barbosa (Nova Consultoria) 17/08/2011

            Set rc = rdocn.OpenResultset(SQL)
            If Not rc.EOF Then
                prim_comp = rc!tp_componente_id
                tabEndossos.TabEnabled(TABendosso_COBERTURAS) = True
                Call Monta_Colecao_Coberturas_SubGrupo(vSubGrupo, rc)
            End If

            'rc.Close
            Set rc = Nothing
        Next    'SubGrupo
    End If
    ''
    Exit Sub

Erro:
    TrataErroGeral "Ler Coberturas"
    MsgBox "Rotina: Ler Coberturas, erro na estrutura dos dados o programa ser� abortado " _
         , vbCritical, Me.Caption
    '


    Exit Sub

Erro_Coberturas_endosso:
    TrataErroGeral "Prepara_Coberturas"
    MsgBox "Erro na rotina Prepara_Coberturas."

End Sub

Sub Monta_Colecao_Coberturas(ByVal rs As rdoResultset)

    Dim vCobertura As Cobertura
    Dim vValPremio As String, vValIS As String
    Dim vPercDesconto As String, vPercBas As String
    Dim vFatTaxa As String, vFatFranquia As String, vValFranquia As String
    Dim DtFimVigencia As String

    On Error GoTo Erro
    '
    vValPremioAnterior = 0
    '
    While Not rs.EOF
        '' IS
        If Not IsNull(rs!val_is) Then
            If ConfiguracaoBrasil Then
                vValIS = Format(Val(rs!val_is), "###,###,###,##0.00")
            Else
                vValIS = TrocaValorAmePorBras(Format(Val(rs!val_is), "###,###,###,##0.00"))
            End If
        Else
            vValIS = Format(0, "#0.00")
        End If
        '' Premio
        If Not IsNull(rs!val_Premio) Then
            If ConfiguracaoBrasil Then
                vValPremio = Format(Val(rs!val_Premio), "###,###,###,##0.00")
            Else
                vValPremio = TrocaValorAmePorBras(Format(Val(rs!val_Premio), "###,###,###,##0.00"))
            End If
        Else
            vValPremio = Format(0, "#0.00")
        End If
        '' Acumula o pr�mio original
        vValPremioAnterior = vValPremioAnterior + vValPremio

        '' Le a taxa
        If Not IsNull(rs!val_taxa) Then
            vFatTaxa = Format(Val(rs!val_taxa), "#0.0000000")
        Else
            '' Calcula a taxa a partir da IS e pr�mio
            If vValIS <> 0 Then
                If ConfiguracaoBrasil Then
                    vFatTaxa = Format(vValPremio / vValIS, "#0.0000000")
                Else
                    vFatTaxa = TrocaValorAmePorBras(Format(vValPremio / vValIS, "#0.0000000"))
                End If
            Else
                vFatTaxa = Format(0, "#0.0000000")
            End If
        End If
        '' Percentual sobre a b�sica
        If Not IsNull(rs!perc_basica) Then
            vPercBas = Format(Val(rs!perc_basica), "#0.000000")
        Else
            vPercBas = Format(0, "#0.000000")
        End If
        '' Na emiss�o original n�o permite informar desconto,
        '' ser� sempre zero.
        vPercDesconto = Format(0, "#0.000000")
        vFatFranquia = Format(0, "#0.00")
        vValFranquia = Format(0, "#0.00")
        '
        Set vCobertura = New Cobertura

        With vCobertura
            .CodObjSegurado = rs!tp_componente_id
            .TpCoberturaId = rs!tp_cob_comp_id
            .Classe = IIf(IsNull(rs!class_tp_cobertura), "a", rs!class_tp_cobertura)
            If UCase(rs!class_tp_cobertura) = "B" Then
                .Descricao = "* " & Trim(rs!Nome)
            Else
                .Descricao = Trim(rs!Nome)
            End If
            .ValIS = vValIS
            .ValTaxa = vFatTaxa
            .PercBasica = vPercBas
            .PercDesconto = vPercDesconto
            '        .FatFranquia = vFatFranquia
            .ValFranquia = vValFranquia
            .ValPremio = vValPremio
            '        .DtIniVigSeg = Format$(rs!dt_inicio_vigencia_seg, "dd/mm/yyyy")
            .DtInicioVigencia = Format$(rs!dt_inicio_vigencia_esc, "dd/mm/yyyy")
            If Not IsNull(rs!dt_fim_vigencia_esc) Then
                .DtFimVigencia = Format(rs!dt_fim_vigencia_esc, "dd/mm/yyyy")
            Else
                .DtFimVigencia = ""
            End If
            '
            '*** Abosco @ 2003 mai 06
            '*** lista de componentes quando h� endosso de exclusao de coberturas
            .TpComponenteId = rs!tp_componente_id
            .NomeComponente = Trim(rs!nome_componente)
            '
        End With
        '
        Coberturas_endosso.Add vCobertura, CStr(rs!tp_cob_comp_id)
        rs.MoveNext
    Wend

    rs.Close
    Exit Sub

Erro:
    TrataErroGeral "Monta_Colecao_Coberturas"
    MsgBox "Erro na rotina Monta_Colecao_Coberturas"

End Sub
Sub Monta_Colecao_Coberturas_SubGrupo(vSubGrupoAnt As SubGrupo, ByVal rs As rdoResultset)

    Dim vCobertura As Cobertura, vCobAnt As Cobertura
    Dim vValPremio As String, vValIS As String
    Dim vPercDesconto As String, vPercBas As String
    Dim vFatTaxa As String, vFatFranquia As String, vValFranquia As String
    Dim DtFimVigencia As String

    On Error GoTo Erro
    '
    '
    Do While Not rs.EOF
        '' IS
        If Not IsNull(rs!val_is) Then
            If ConfiguracaoBrasil Then
                vValIS = Format(Val(rs!val_is), "###,###,###,##0.00")
            Else
                vValIS = TrocaValorAmePorBras(Format(Val(rs!val_is), "###,###,###,##0.00"))
            End If
        Else
            vValIS = Format(0, "#0.00")
        End If
        '' Premio
        If Not IsNull(rs!val_Premio) Then
            If ConfiguracaoBrasil Then
                vValPremio = Format(Val(rs!val_Premio), "###,###,###,##0.00")
            Else
                vValPremio = TrocaValorAmePorBras(Format(Val(rs!val_Premio), "###,###,###,##0.00"))
            End If
        Else
            vValPremio = Format(0, "#0.00")
        End If
        '' Acumula o pr�mio original
        vValPremioAnterior = vValPremioAnterior + vValPremio

        '' Le a taxa
        If Not IsNull(rs!val_taxa) Then
            vFatTaxa = Format(Val(rs!val_taxa), "#0.0000000")
        Else
            '' Calcula a taxa a partir da IS e pr�mio
            If vValIS <> 0 Then
                If ConfiguracaoBrasil Then
                    vFatTaxa = Format(vValPremio / vValIS, "#0.0000000")
                Else
                    vFatTaxa = TrocaValorAmePorBras(Format(vValPremio / vValIS, "#0.0000000"))
                End If
            Else
                vFatTaxa = Format(0, "#0.0000000")
            End If
        End If
        '' Percentual sobre a b�sica
        If Not IsNull(rs!perc_basica) Then
            vPercBas = Format(Val(rs!perc_basica), "#0.000000")
        Else
            vPercBas = Format(0, "#0.000000")
        End If
        '' Na emiss�o original n�o permite informar desconto,
        '' ser� sempre zero.
        vPercDesconto = Format(0, "#0.000000")
        vFatFranquia = Format(0, "#0.00")
        vValFranquia = Format(0, "#0.00")
        '

        Set vCobertura = New Cobertura

        With vCobertura
            .CodObjSegurado = rs!tp_componente_id
            .TpCoberturaId = rs!tp_cob_comp_id
            .Classe = IIf(IsNull(rs!class_tp_cobertura), "a", rs!class_tp_cobertura)
            If UCase(rs!class_tp_cobertura) = "B" Then
                .Descricao = "* " & Trim(rs!Nome)
            Else
                .Descricao = Trim(rs!Nome)
            End If
            .ValIS = vValIS
            .ValTaxa = vFatTaxa
            .PercBasica = vPercBas
            .PercDesconto = vPercDesconto
            '        .FatFranquia = vFatFranquia
            .ValFranquia = vValFranquia
            .ValPremio = vValPremio
            '.DtIniVigSeg = Format$(rs!dt_inicio_vigencia_seg, "dd/mm/yyyy")
            '        .DtInicioVigencia = Format$(rs!dt_inicio_vigencia_esc, "dd/mm/yyyy")
            '        .DtInicioVigencia = txtIniVigencia.Text
            'If Not IsNull(rs!dt_fim_vigencia_esc) Then
            '   .DtFimVigencia = Format(rs!dt_fim_vigencia_esc, "dd/mm/yyyy")
            'Else
            .DtFimVigencia = ""
            'End If
            '
        End With
        '
        SubGrupos_endosso(CStr(vSubGrupoAnt.Id)).Coberturas.Add vCobertura, CStr(vCobertura.TpCoberturaId)
        '
        rs.MoveNext
    Loop

    rs.Close
    Exit Sub

Erro:
    TrataErroGeral "Monta_Colecao_Coberturas_SubGrupo"
    MsgBox "Erro na rotina Monta_Colecao_Coberturas_SubGrupo"

End Sub
Private Sub Prepara_Componentes()
    Dim rc As rdoResultset
    Dim SQL As String
    Dim linhaCboComponentes As String
    Dim vSubGrupo As SubGrupo

    On Error GoTo Erro_Componentes_endosso

    If Not HaSubGrupos Then
        SQL = "SELECT distinct s.tp_componente_id, c.nome "
        SQL = SQL & ", s.subramo_id, s.dt_inicio_vigencia_sbr, s.dt_inicio_vigencia_seg "
        '    If UCase(gTpEmissao) = "A" Then
        '        sql = sql & ", seguro_vida_tb.dt_fim_vigencia_sbr "
        '    End If
        SQL = SQL & "FROM " & TabSeguro & " s WITH(NOLOCK), tp_componente_tb c  WITH (NOLOCK)  "
        SQL = SQL & "WHERE s.proposta_id = " & vPropostaId
        SQL = SQL & " AND s.endosso_id = " & gEndossoId
        If UCase(gTpEmissao) <> "A" Then
            ' Atributo n�o existe na seguro_vida_aceito_tb
            SQL = SQL & " AND isnull(s.tp_cliente,'p') = 'p'"
        End If
        '    sql = sql & " AND seguro_vida_tb.dt_fim_vigencia_seg is null"
        SQL = SQL & " AND c.tp_componente_id = s.tp_componente_id "
        SQL = SQL & " ORDER BY s.tp_componente_id "
        '
        Set rc = rdoCn1.OpenResultset(SQL, rdOpenStatic)    ' Acha o cod_objeto_segurado

        If Not rc.EOF Then
            tabEndossos.TabEnabled(TABendosso_COMPONENTES) = True
            tabEndossos.TabEnabled(TABendosso_COBERTURAS) = True
            '
            Call Monta_Colecao_Componentes(rc)
            Call Monta_GridComponentes_endosso
        End If
        '
    Else    'Ap�lice c/ Subgrupos

        For Each vSubGrupo In SubGrupos_endosso

            ' pablo.dias (Nova Consultoria) 13/08/2011
            ' Demanda 864186 - Melhorias no Processo de Emiss�o de Ap�lice
            ' Cria��o e preenchimento de tabela auxiliar para dados da tabela seguro_vida_tb
            SQL = "SELECT subramo_id, tp_componente_id, ramo_id, proposta_id "
            SQL = SQL & " INTO #seguro_vida "
            SQL = SQL & "  FROM seguro_vida_tb  WITH (NOLOCK)  "
            SQL = SQL & " WHERE proposta_id = " & vPropostaId

            ' Cria��o e preenchimento de tabela auxiliar para dados da tabela escolha_sub_grp_tp_cob_comp_TB
            SQL = SQL & " SELECT"
            SQL = SQL & "        tp_cob_comp_id,"
            SQL = SQL & "        dt_inicio_vigencia_comp,"
            SQL = SQL & "        apolice_id,"
            SQL = SQL & "        sucursal_seguradora_id,"
            SQL = SQL & "        seguradora_cod_susep,"
            SQL = SQL & "        ramo_id,"
            SQL = SQL & "        sub_grupo_id"
            SQL = SQL & "   INTO #escolha_sub_grp_tp_cob_comp"
            SQL = SQL & "   FROM escolha_sub_grp_tp_cob_comp_TB WITH(NOLOCK)"
            SQL = SQL & "  WHERE escolha_sub_grp_tp_cob_comp_tb.apolice_id             = " & vApoliceId
            SQL = SQL & "    AND escolha_sub_grp_tp_cob_comp_tb.sucursal_seguradora_id = " & vSucursalId
            SQL = SQL & "    AND escolha_sub_grp_tp_cob_comp_tb.seguradora_cod_susep   = " & vSeguradoraId
            SQL = SQL & "    AND escolha_sub_grp_tp_cob_comp_tb.ramo_id                = " & vRamoId
            SQL = SQL & "    AND escolha_sub_grp_tp_cob_comp_tb.sub_grupo_id           = " & vSubGrupo.Id

            ' Trazer dados de endosso_vigencia_futura_tb quando a vig�ncia do endosso for futura
            If DateDiff("d", gDtIniVigEndosso, Date) < 0 Then
                ' Obtendo dados que ser�o inclu�dos futuramente em seguro_vida_tb
                SQL = SQL & " INSERT INTO #seguro_vida "
                SQL = SQL & " SELECT "
                SQL = SQL & "       CAST(MAX(CASE WHEN nome_campo = 'subramo_id' THEN valor_campo END) AS NUMERIC(5, 0)) subramo_id "
                SQL = SQL & "     , CAST(MAX(CASE WHEN nome_campo = 'tp_componente_id' THEN valor_campo END) AS TINYINT) tp_componente_id "
                SQL = SQL & "     , CAST(MAX(CASE WHEN nome_campo = 'ramo_id' THEN valor_campo END) AS INT) ramo_id "
                SQL = SQL & "     , CAST(MAX(CASE WHEN nome_campo = 'proposta_id' THEN valor_campo END) AS NUMERIC(9, 0)) proposta_id "
                SQL = SQL & "  FROM endosso_vigencia_futura_tb  WITH (NOLOCK) "
                SQL = SQL & " WHERE nome_procedure = 'endosso_seguro_vida_param_spi' "
                SQL = SQL & "   AND proposta_id = " & vPropostaId
                SQL = SQL & "   AND endosso_id = " & gEndossoId
                SQL = SQL & "   AND dt_aplicacao IS NULL "
                SQL = SQL & " GROUP BY execucao_id, nome_procedure "

                ' Obtendo dados que ser�o inclu�dos futuramente em escolha_sub_grp_tp_cob_comp_tb
                SQL = SQL & " INSERT INTO #escolha_sub_grp_tp_cob_comp"
                SQL = SQL & " SELECT"
                SQL = SQL & "     CAST(MAX(CASE WHEN nome_campo = 'tp_cob_comp_id' THEN valor_campo END) AS smallint ) tp_cob_comp_id,"
                SQL = SQL & "     CAST(MAX(CASE WHEN nome_campo = 'dt_inicio_vigencia_comp' THEN valor_campo END) AS smalldatetime) dt_inicio_vigencia_comp,"
                SQL = SQL & "     CAST(MAX(CASE WHEN nome_campo = 'apolice_id' THEN valor_campo END) AS numeric(9,0)) apolice_id,"
                SQL = SQL & "     CAST(MAX(CASE WHEN nome_campo = 'sucursal_id' THEN valor_campo END) AS numeric(5)) sucursal_seguradora_id,"
                SQL = SQL & "     CAST(MAX(CASE WHEN nome_campo = 'seguradora_id' THEN valor_campo END) AS numeric(5)) seguradora_cod_susep,"
                SQL = SQL & "     CAST(MAX(CASE WHEN nome_campo = 'ramo_id' THEN valor_campo END) AS tinyint) ramo_id,"
                SQL = SQL & "     CAST(MAX(CASE WHEN nome_campo = 'sub_grupo_id' THEN valor_campo END) AS numeric(9,6)) sub_grupo_id"
                SQL = SQL & "   FROM endosso_vigencia_futura_TB WITH(NOLOCK)"
                SQL = SQL & "  WHERE nome_procedure = 'escolha_tp_cob_sub_grupo_spi'"
                SQL = SQL & "    AND proposta_id = " & vPropostaId
                SQL = SQL & "    AND endosso_id = " & gEndossoId
                SQL = SQL & "    AND dt_aplicacao IS NULL"
                SQL = SQL & "  GROUP BY execucao_id, nome_procedure "

            End If
            ''''' pablo.dias (Nova Consultoria) 13/08/2011

            SQL = SQL & " SELECT DISTINCT tp_componente_tb.tp_componente_id, tp_componente_tb.nome nomecomp "
            '        sql = sql & ", seguro_vida_tb.dt_inicio_vigencia_sbr, seguro_vida_tb.subramo_id "
            SQL = SQL & ", seguro_vida_tb.subramo_id "
            SQL = SQL & ", subramo_tb.nome nomesr, subramo_tb.dt_inicio_vigencia_sbr dt_inicio_vigencia_sbr "
            SQL = SQL & ", subramo_tb.dt_fim_vigencia_sbr "

            ' pablo.dias (Nova Consultoria) 13/08/2011
            ' Demanda 864186 - Melhorias no Processo de Emiss�o de Ap�lice
            ' Uso da tabela auxiliar para obter dados da tabela escolha_sub_grp_tp_cob_comp_TB
            SQL = SQL & " FROM #escolha_sub_grp_tp_cob_comp escolha_sub_grp_tp_cob_comp_TB  WITH (NOLOCK)     "
            ''''' pablo.dias (Nova Consultoria) 13/08/2011

            SQL = SQL & "      INNER JOIN tp_cob_comp_tb        WITH (NOLOCK)     "
            SQL = SQL & "              ON   tp_cob_comp_tb.tp_cob_comp_id = escolha_sub_grp_tp_cob_comp_tb.tp_cob_comp_id "
            SQL = SQL & "      INNER JOIN tp_componente_tb      WITH (NOLOCK)     "
            SQL = SQL & "              ON tp_componente_tb.tp_componente_id = tp_cob_comp_tb.tp_componente_id "

            ' pablo.dias (Nova Consultoria) 13/08/2011
            ' Demanda 864186 - Melhorias no Processo de Emiss�o de Ap�lice
            ' Uso da tabela auxiliar para obter dados da tabela seguro_vida_tb
            SQL = SQL & "      INNER JOIN #seguro_vida seguro_vida_tb        WITH (NOLOCK)     "
            ''''' pablo.dias (Nova Consultoria) 13/08/2011

            SQL = SQL & "              ON seguro_vida_tb.tp_componente_id = tp_componente_tb.tp_componente_id"
            SQL = SQL & "      INNER JOIN subramo_tb            WITH (NOLOCK)     "
            SQL = SQL & "              ON subramo_tb.ramo_id = seguro_vida_tb.ramo_id "
            SQL = SQL & "             AND subramo_tb.subramo_id = seguro_vida_tb.subramo_id "
            '*** Abosco @ 2003 abr 24
            '*** inclusao de inicio de vigencia em tp_cob_comp_item_tb
            SQL = SQL & "      INNER JOIN tp_cob_comp_item_tb    WITH (NOLOCK) "
            SQL = SQL & "              ON tp_cob_comp_tb.tp_cob_comp_id = tp_cob_comp_item_tb.tp_cob_comp_id "
            SQL = SQL & "             AND escolha_sub_grp_tp_cob_comp_tb.dt_inicio_vigencia_comp = tp_cob_comp_item_tb.dt_inicio_vigencia_comp "
            SQL = SQL & "WHERE escolha_sub_grp_tp_cob_comp_tb.apolice_id = " & vApoliceId
            SQL = SQL & " AND escolha_sub_grp_tp_cob_comp_tb.sucursal_seguradora_id = " & vSucursalId
            SQL = SQL & " AND escolha_sub_grp_tp_cob_comp_tb.seguradora_cod_susep = " & vSeguradoraId
            SQL = SQL & " AND escolha_sub_grp_tp_cob_comp_tb.ramo_id = " & vRamoId
            SQL = SQL & " AND escolha_sub_grp_tp_cob_comp_tb.sub_grupo_id = " & vSubGrupo.Id
            SQL = SQL & " AND seguro_vida_tb.proposta_id = " & vPropostaId
            '        sql = sql & " AND seguro_vida_tb.prop_cliente_id = " & vClienteId
            '        sql = sql & " AND seguro_vida_tb.endosso_id = " & gEndossoId
            SQL = SQL & " AND subramo_tb.dt_fim_vigencia_sbr IS NULL "
            '*** Abosco @ 2003 abr 24
            '*** inclusao de inicio de vigencia em tp_cob_comp_item_tb
            SQL = SQL & " AND tp_cob_comp_item_tb.dt_fim_vigencia_comp IS NULL "

            ' pablo.dias (Nova Consultoria) 13/08/2011
            ' Demanda 864186 - Melhorias no Processo de Emiss�o de Ap�lice
            ' Exclus�o das tabelas tempor�rias
            SQL = SQL & " DROP TABLE #seguro_vida "
            SQL = SQL & " DROP TABLE #escolha_sub_grp_tp_cob_comp "

            ' Altera��o do par�metro de forma a permitir bloco de c�digo SQL
            Set rc = rdocn.OpenResultset(SQL)    ' Acha o cod_objeto_segurado
            ''''' pablo.dias (Nova Consultoria) 13/08/2011

            If Not rc.EOF Then
                SubGrupoId = vSubGrupo.Id
                tabEndossos.TabEnabled(TABendosso_COMPONENTES) = True
                Call Monta_Colecao_Componentes_SubGrupo(SubGrupoId, rc)
            End If
            '

            ' pablo.dias (Nova Consultoria) 13/08/2011
            ' Demanda 864186 - Melhorias no Processo de Emiss�o de Ap�lice
            ' Fechar recordset a cada itera��o de subgrupo
            rc.Close
            ''''' pablo.dias (Nova Consultoria) 13/08/2011
        Next
    End If
    ''
    'rc.Close
    Set rc = Nothing
    '
    Exit Sub

Erro_Componentes_endosso:
    TrataErroGeral "Prepara_Componentes"
    MsgBox "Erro na rotina Prepara_Componentes."

End Sub

Sub Monta_Colecao_Componentes(ByVal rs As rdoResultset)
    Dim NovoComponente As Componente
    Dim NovaCobertura As Cobertura
    Dim SQL As String
    Dim auxlista As String
    Dim DtIniVigencia As String
    Dim DtFimVigencia As String

    On Error GoTo Erro
    '
    While Not rs.EOF
        '
        Set NovoComponente = New Componente
        '
        DtIniVigencia = Format(IIf(IsNull(rs!dt_inicio_vigencia_sbr), "", rs!dt_inicio_vigencia_sbr), "dd/mm/yyyy")
        'DtFimVigencia = Format(IIf(IsNull(rs!dt_fim_vigencia), "", rs!dt_fim_vigencia), "dd/mm/yyyy")
        '
        With NovoComponente
            .Codigo = rs!tp_componente_id
            .Nome = Trim(rs!Nome)
            .CodSubRamo = rs!Subramo_id
            .DtIniVigSbr = CDate(DtIniVigencia)
            '.DtFimVigSbr = CDate(DtFimVigencia)
            .DtIniSeguro = CDate(rs!dt_inicio_vigencia_seg)
        End With
        '
        Componentes_endosso.Add NovoComponente, CStr(rs!tp_componente_id)
        '
        rs.MoveNext
    Wend
    '
    Exit Sub

Erro:
    TrataErroGeral "Monta_Colecao_Componentes"
    MsgBox "Rotina: Monta_Colecao_Componentes, erro na estrutura dos dados. O programa ser� abortado " _
         , vbCritical

End Sub



Sub Monta_Colecao_Componentes_SubGrupo(SubGrupoIdEndosso As Integer, ByVal rs As rdoResultset)
    Dim NovoComponente As Componente
    Dim SQL As String
    Dim auxlista As String
    Dim DtIniVigencia As String
    Dim DtFimVigencia As String

    On Error GoTo Erro
    ''
    Do While Not rs.EOF
        '
        Set NovoComponente = New Componente
        '
        DtIniVigencia = Format(IIf(IsNull(rs!dt_inicio_vigencia_sbr), " ", rs!dt_inicio_vigencia_sbr), "dd/mm/yyyy")
        DtFimVigencia = Format(IIf(IsNull(rs!dt_fim_vigencia_sbr), " ", rs!dt_fim_vigencia_sbr), "dd/mm/yyyy")
        '
        With NovoComponente
            .Codigo = rs!tp_componente_id
            .Nome = Trim(rs!Nomecomp)
            .CodSubRamo = rs!Subramo_id
            If DtIniVigencia <> " " Then
                .DtIniVigSbr = CDate(DtIniVigencia)
            End If
            If DtFimVigencia <> " " Then
                .DtFimVigSbr = CDate(DtFimVigencia)
            End If
            '        .DtIniSeguro = CDate(rs!dt_inicio_vigencia_seg)
        End With
        '
        SubGrupos_endosso(CStr(SubGrupoIdEndosso)).Componentes.Add NovoComponente, CStr(rs!tp_componente_id)
        '
        rs.MoveNext
    Loop
    '
    Exit Sub

Erro:
    TrataErroGeral "Monta_Colecao_Componentes_SubGrupo"
    MsgBox "Rotina: Monta_Colecao_Componentes_SubGrupo, erro na estrutura dos dados. O programa ser� abortado " _
         , vbCritical

End Sub
Private Sub Prepara_Corretagem_Geral()
    Dim rc As rdoResultset
    Dim vSql As String
    Dim CodCorretor As String
    Dim NomeCorretor As String
    Dim PercCorretagem As String
    Dim vCorretor As Corretor
    Dim DataEmissaoEndosso As String
    Dim sub_grupo_fatura As String
    Dim SubGrupoIdFatura As String
    Dim ind_sg As Integer

    On Error GoTo Erro_Corretagem_endosso

    CodCorretor = ""
    PercCorretagem = ""
    TotCorretagem = 0#
    HaCorretor = False
    '
    GridCorretores.Rows = 1
    '
    DataEmissaoEndosso = Format(dtInicioVigEndosso, "yyyymmdd")
    If HaSubGrupos Then
        sub_grupo_fatura = ", fa.sub_grupo_id "
        cboSubGrupo(0).Locked = False
    End If
    '
    If Not HaSubGrupos Then
        vSql = "SELECT ca.corretor_id, sucursal_corretor_id = ''"
        vSql = vSql & ", 'tipo' = 'P. Fisica' "
        vSql = vSql & ", c.nome, ca.dt_inicio_corretagem, ca.dt_fim_corretagem"
        vSql = vSql & ",  cpf.perc_corretagem "
        vSql = vSql & "FROM corretagem_tb ca  WITH (NOLOCK) , corretagem_pf_tb cpf  WITH (NOLOCK) , corretor_tb c   WITH (NOLOCK) "
        vSql = vSql & "WHERE ca.proposta_id = " & vPropostaId
        vSql = vSql & " AND ca.endosso_id = " & gEndossoId
        vSql = vSql & " AND  cpf.proposta_id = ca.proposta_id"
        vSql = vSql & " AND  cpf.corretor_id = ca.corretor_id"
        vSql = vSql & " AND  cpf.dt_inicio_corretagem = ca.dt_inicio_corretagem"
        vSql = vSql & " AND c.corretor_id = ca.corretor_id "
        vSql = vSql & " UNION "
        vSql = vSql & "SELECT ca.corretor_id, cpj.sucursal_corretor_id"
        vSql = vSql & ", 'tipo' = 'P. Juridica' "
        vSql = vSql & ", c.nome, ca.dt_inicio_corretagem, ca.dt_fim_corretagem"
        vSql = vSql & ", cpj.perc_corretagem "
        vSql = vSql & "FROM corretagem_tb ca  WITH (NOLOCK) , corretagem_pj_tb cpj  WITH (NOLOCK) , corretor_tb c   WITH (NOLOCK) "
        vSql = vSql & "WHERE ca.proposta_id = " & vPropostaId
        vSql = vSql & " AND ca.endosso_id = " & gEndossoId
        vSql = vSql & " AND cpj.proposta_id = ca.proposta_id"
        vSql = vSql & " AND cpj.corretor_id = ca.corretor_id"
        vSql = vSql & " AND cpj.dt_inicio_corretagem = ca.dt_inicio_corretagem "
        vSql = vSql & " AND c.corretor_id = ca.corretor_id"
        vSql = vSql & " ORDER BY ca.corretor_id"
        '
        Set rc = rdoCn1.OpenResultset(vSql, rdOpenStatic)
        If Not rc.EOF Then
            tabEndossos.TabEnabled(TABendosso_CORRETAGEM) = True
        End If
        While Not rc.EOF
            PercCorretagem = IIf(IsNull(rc!perc_corretagem), vValZero, Format(Val(rc!perc_corretagem), "0.00"))
            NomeCorretor = Trim(rc!Nome)
            '
            If (Trim(PercCorretagem) <> "") And (Not ConfiguracaoBrasil) Then
                PercCorretagem = TrocaValorAmePorBras(PercCorretagem)
            End If
            '
            Set vCorretor = New Corretor
            vCorretor.Id = rc!Corretor_id
            vCorretor.Nome = NomeCorretor
            vCorretor.PercCorretagem = PercCorretagem
            vCorretor.DtIniCorretagem = rc!dt_inicio_corretagem
            vCorretor.DtFimCorretagem = rc!dt_fim_corretagem
            vCorretor.Tipo = rc!Tipo
            vCorretor.Sucursal = rc!sucursal_corretor_id
            '
            Corretores_endosso.Add vCorretor, CStr(rc!Corretor_id)
            '
            rc.MoveNext
        Wend
        '
        Monta_GridCorretores_endosso
        '
    Else    'Tem Subgrupos

        ' Autor: rodrigo.almeida (Nova Consultoria) - Data Altera��o: 17/08/2011 - Demanda: 864186
        ' Modifica os valores de corretagem de subgrupo de acordo com a tabela de endosso de vig�ncia futura.
        ' Somente para o produto 115
        vSql = "SELECT * "
        vSql = vSql & "INTO #corretagem_temp_tb "
        vSql = vSql & "FROM ( "
        '
        vSql = vSql & "SELECT corretagem_sub_grupo_tb.sub_grupo_id, corretagem_sub_grupo_tb.corretor_id, sucursal_corretor_id = ''"
        vSql = vSql & ", 'tipo' = 'P. Fisica'               "
        vSql = vSql & ",  corretor_tb.nome                             "
        vSql = vSql & ",  corretagem_sub_grupo_tb.dt_inicio_corretagem           "
        vSql = vSql & ",  corretagem_sub_grupo_tb.dt_fim_corretagem               "
        vSql = vSql & ",   corretagem_pf_sub_grupo_tb.perc_corretagem                "
        vSql = vSql & "FROM corretagem_sub_grupo_tb             WITH (NOLOCK)     "
        vSql = vSql & " INNER JOIN    corretagem_pf_sub_grupo_tb          WITH (NOLOCK)          "
        vSql = vSql & "     ON   corretagem_pf_sub_grupo_tb.apolice_id = corretagem_sub_grupo_tb.apolice_id                              "
        vSql = vSql & "     AND  corretagem_pf_sub_grupo_tb.ramo_id = corretagem_sub_grupo_tb.ramo_id                                    "
        vSql = vSql & "     AND  corretagem_pf_sub_grupo_tb.sucursal_seguradora_id = corretagem_sub_grupo_tb.sucursal_seguradora_id      "
        vSql = vSql & "     AND  corretagem_pf_sub_grupo_tb.seguradora_cod_susep = corretagem_sub_grupo_tb.seguradora_cod_susep          "
        vSql = vSql & "     AND  corretagem_pf_sub_grupo_tb.sub_grupo_id = corretagem_sub_grupo_tb.sub_grupo_id                          "
        vSql = vSql & "     AND  corretagem_pf_sub_grupo_tb.dt_inicio_vigencia_sbg =  corretagem_sub_grupo_tb.dt_inicio_vigencia_sbg"
        vSql = vSql & "     AND  corretagem_pf_sub_grupo_tb.corretor_id = corretagem_sub_grupo_tb.corretor_id                            "
        vSql = vSql & "     AND  corretagem_pf_sub_grupo_tb.dt_inicio_corretagem = corretagem_sub_grupo_tb.dt_inicio_corretagem"
        vSql = vSql & " INNER JOIN  corretor_tb                          WITH (NOLOCK)     "
        vSql = vSql & "    ON       corretor_tb.corretor_id = corretagem_sub_grupo_tb.corretor_id "
        vSql = vSql & "WHERE    corretagem_sub_grupo_tb.apolice_id               = " & vApoliceId
        vSql = vSql & "     AND corretagem_sub_grupo_tb.sucursal_seguradora_id   = " & vSucursalId
        vSql = vSql & "     AND corretagem_sub_grupo_tb.seguradora_cod_susep     = " & vSeguradoraId
        vSql = vSql & "     AND corretagem_sub_grupo_tb.ramo_id                  = " & vRamoId
        vSql = vSql & " AND ( corretagem_sub_grupo_tb.dt_inicio_corretagem = '" & DataEmissaoEndosso & "' OR datediff(dd,corretagem_sub_grupo_tb.dt_fim_corretagem,'" & DataEmissaoEndosso & "') = 1) "
        vSql = vSql & " UNION                                                               "
        vSql = vSql & "SELECT corretagem_sub_grupo_tb.sub_grupo_id,                                              "
        vSql = vSql & "   corretagem_sub_grupo_tb.corretor_id,                                                   "
        vSql = vSql & "   corretagem_pj_sub_grupo_tb.sucursal_corretor_id                                          "
        vSql = vSql & ",  'tipo' = 'P. Juridica'                                            "
        vSql = vSql & ",  corretor_tb.nome,                                                           "
        vSql = vSql & "   corretagem_sub_grupo_tb.dt_inicio_corretagem,                                          "
        vSql = vSql & "   corretagem_sub_grupo_tb.dt_fim_corretagem                                              "
        vSql = vSql & ",  corretagem_pj_sub_grupo_tb.perc_corretagem                                               "
        vSql = vSql & "FROM     corretagem_sub_grupo_tb    WITH (NOLOCK)                          "
        vSql = vSql & " INNER JOIN  corretagem_pj_sub_grupo_tb    WITH (NOLOCK)                  "
        vSql = vSql & "     ON  corretagem_pj_sub_grupo_tb.apolice_id              = corretagem_sub_grupo_tb.apolice_id"
        vSql = vSql & "     AND corretagem_pj_sub_grupo_tb.sucursal_seguradora_id  = corretagem_sub_grupo_tb.sucursal_seguradora_id"
        vSql = vSql & "     AND corretagem_pj_sub_grupo_tb.seguradora_cod_susep    = corretagem_sub_grupo_tb.seguradora_cod_susep"
        vSql = vSql & "     AND corretagem_pj_sub_grupo_tb.ramo_id                 = corretagem_sub_grupo_tb.ramo_id"
        vSql = vSql & "     AND corretagem_pj_sub_grupo_tb.sub_grupo_id            = corretagem_sub_grupo_tb.sub_grupo_id"
        vSql = vSql & "     AND corretagem_pj_sub_grupo_tb.dt_inicio_vigencia_sbg  = corretagem_sub_grupo_tb.dt_inicio_vigencia_sbg"
        vSql = vSql & "     AND corretagem_pj_sub_grupo_tb.corretor_id             = corretagem_sub_grupo_tb.corretor_id"
        vSql = vSql & "     AND corretagem_pj_sub_grupo_tb.dt_inicio_corretagem    = corretagem_sub_grupo_tb.dt_inicio_corretagem "
        vSql = vSql & " INNER JOIN  corretor_tb     WITH (NOLOCK)                                  "
        vSql = vSql & "     ON      corretor_tb.corretor_id = corretagem_sub_grupo_tb.corretor_id "
        vSql = vSql & "WHERE corretagem_sub_grupo_tb.apolice_id = " & vApoliceId
        vSql = vSql & "     AND corretagem_sub_grupo_tb.sucursal_seguradora_id = " & vSucursalId
        vSql = vSql & "     AND corretagem_sub_grupo_tb.seguradora_cod_susep = " & vSeguradoraId
        vSql = vSql & "     AND corretagem_sub_grupo_tb.ramo_id = " & vRamoId
        vSql = vSql & "     AND ( corretagem_sub_grupo_tb.dt_inicio_corretagem = '" & Format(dt_inicio_vigencia, "yyyymmdd") & "' OR datediff(dd,corretagem_sub_grupo_tb.dt_fim_corretagem,'" & DataEmissaoEndosso & "') = 1) "    'Alterado por fpimenta / Imar�s - 26/03/2003
        '
        ' Autor: rodrigo.almeida (Nova Consultoria) - Data Altera��o: 17/08/2011 - Demanda: 864186
        vSql = vSql & " ) temp "
        '

        If DateDiff("d", gDtIniVigEndosso, Date) < 0 Then

            vSql = vSql & "DELETE temp "
            vSql = vSql & "  FROM #corretagem_temp_tb temp "
            vSql = vSql & " INNER JOIN ( "
            vSql = vSql & "     SELECT proposta_id "
            vSql = vSql & "          , CAST(MAX(CASE WHEN nome_campo = 'apolice_id' THEN valor_campo END) AS NUMERIC(9)) apolice_id "
            vSql = vSql & "          , CAST(MAX(CASE WHEN nome_campo = 'seguradora_cod_susep' THEN valor_campo END) AS NUMERIC(5)) seguradora_cod_susep "
            vSql = vSql & "          , CAST(MAX(CASE WHEN nome_campo = 'sucursal_seguradora_id' THEN valor_campo END) AS NUMERIC(5)) sucursal_seguradora_id "
            vSql = vSql & "          , CAST(MAX(CASE WHEN nome_campo = 'ramo_id' THEN valor_campo END) AS INT) ramo_id "
            vSql = vSql & "          , CAST(MAX(CASE WHEN nome_campo = 'sub_grupo_id' THEN valor_campo END) AS NUMERIC(5)) sub_grupo_id "
            vSql = vSql & "          , CAST(MAX(CASE WHEN nome_campo = 'status' THEN valor_campo END) AS CHAR(1)) status "
            vSql = vSql & "       FROM endosso_vigencia_futura_tb WITH(NOLOCK) "
            vSql = vSql & "      WHERE dt_aplicacao IS NULL "
            vSql = vSql & "        AND proposta_id = " & vPropostaId
            vSql = vSql & "       AND endosso_id = " & gEndossoId
            vSql = vSql & "        AND (nome_procedure LIKE '%corretagem_pf_sub_grupo_spi' OR nome_procedure LIKE '%corretagem_pj_sub_grupo_spi') "
            vSql = vSql & "      GROUP BY proposta_id ) evf "
            vSql = vSql & "    ON temp.sub_grupo_id = evf.sub_grupo_id "
            vSql = vSql & "   AND evf.seguradora_cod_susep = " & vSeguradoraId
            vSql = vSql & "   AND evf.sucursal_seguradora_id = " & vSucursalId
            vSql = vSql & "   AND evf.ramo_id = " & vRamoId
            vSql = vSql & "   AND evf.apolice_id = " & vApoliceId
            vSql = vSql & "   AND evf.status IN ('a', 'e') "
            '
            vSql = vSql & "INSERT INTO #corretagem_temp_tb ( "
            vSql = vSql & "       sub_grupo_id "
            vSql = vSql & "     , corretor_id "
            vSql = vSql & "     , sucursal_corretor_id "
            vSql = vSql & "     , tipo "
            vSql = vSql & "     , nome "
            vSql = vSql & "     , dt_inicio_corretagem "
            vSql = vSql & "     , dt_fim_corretagem "
            vSql = vSql & "     , perc_corretagem ) "
            vSql = vSql & "SELECT evf.sub_grupo_id "
            vSql = vSql & "     , evf.corretor_id "
            vSql = vSql & "     , ISNULL(evf.sucursal_corretor_id, '') "
            vSql = vSql & "     , evf.tipo "
            vSql = vSql & "     , corretor_tb.nome "
            vSql = vSql & "     , evf.dt_inicio_corretagem "
            vSql = vSql & "     , evf.dt_fim_corretagem "
            vSql = vSql & "     , evf.perc_corretagem "
            vSql = vSql & "  FROM (SELECT proposta_id "
            vSql = vSql & "          , CAST(MAX(CASE WHEN nome_campo = 'apolice_id' THEN valor_campo END) AS NUMERIC(9)) apolice_id "
            vSql = vSql & "          , CAST(MAX(CASE WHEN nome_campo = 'seguradora_cod_susep' THEN valor_campo END) AS NUMERIC(5)) seguradora_cod_susep "
            vSql = vSql & "          , CAST(MAX(CASE WHEN nome_campo = 'sucursal_seguradora_id' THEN valor_campo END) AS NUMERIC(5)) sucursal_seguradora_id "
            vSql = vSql & "          , CAST(MAX(CASE WHEN nome_campo = 'ramo_id' THEN valor_campo END) AS INT) ramo_id "
            vSql = vSql & "          , CAST(MAX(CASE WHEN nome_campo = 'sub_grupo_id' THEN valor_campo END) AS NUMERIC(5)) sub_grupo_id "
            vSql = vSql & "          , CAST(MAX(CASE WHEN nome_campo = 'corretor_id' THEN valor_campo END) AS NUMERIC(9)) corretor_id "
            vSql = vSql & "          , CAST(MAX(CASE WHEN nome_campo = 'sucursal_corretor_id' THEN valor_campo END) AS VARCHAR(4)) sucursal_corretor_id "
            vSql = vSql & "          , CAST(MAX(CASE WHEN nome_campo = 'dtinicorretagem' THEN valor_campo END) AS SMALLDATETIME) dt_inicio_corretagem "
            vSql = vSql & "          , CAST(MAX(CASE WHEN nome_campo = 'dtfimcorretagem' THEN valor_campo END) AS SMALLDATETIME) dt_fim_corretagem "
            vSql = vSql & "          , CAST(MAX(CASE WHEN nome_campo = 'perc_corretagem' THEN valor_campo END) AS NUMERIC(5)) perc_corretagem "
            vSql = vSql & "          , CAST(MAX(CASE WHEN nome_campo = 'status' THEN valor_campo END) AS CHAR(1)) status "
            vSql = vSql & "          , CAST(MAX(CASE WHEN nome_procedure LIKE '%corretagem_pf_sub_grupo_spi' THEN 'P. Fisica' ELSE  'P. Juridica' END) AS VARCHAR(11)) tipo "
            vSql = vSql & "       FROM endosso_vigencia_futura_tb WITH(NOLOCK) "
            vSql = vSql & "      WHERE dt_aplicacao IS NULL "
            vSql = vSql & "        AND proposta_id = " & vPropostaId
            vSql = vSql & "       AND endosso_id = " & gEndossoId
            vSql = vSql & "        AND (nome_procedure LIKE '%corretagem_pf_sub_grupo_spi' OR nome_procedure LIKE '%corretagem_pj_sub_grupo_spi') "
            vSql = vSql & "      GROUP BY proposta_id ) evf "
            vSql = vSql & " INNER JOIN corretor_tb  WITH (NOLOCK)  "
            vSql = vSql & "    ON corretor_tb.corretor_id = evf.corretor_id "
            vSql = vSql & " WHERE evf.seguradora_cod_susep = " & vSeguradoraId
            vSql = vSql & "   AND evf.sucursal_seguradora_id = " & vSucursalId
            vSql = vSql & "   AND evf.ramo_id = " & vRamoId
            vSql = vSql & "   AND evf.apolice_id = " & vApoliceId
            vSql = vSql & "   AND evf.status IN ('a', 'i') "

        End If

        vSql = vSql & " SELECT * FROM #corretagem_temp_tb ORDER BY sub_grupo_id, corretor_id "
        vSql = vSql & " DROP TABLE #corretagem_temp_tb "
        ' Autor: rodrigo.almeida (Nova Consultoria) - Data Altera��o: 17/08/2011 - Demanda: 864186 - FIM

        Set rc = rdocn.OpenResultset(vSql)
        If Not rc.EOF Then
            tabEndossos.TabEnabled(TABendosso_CORRETAGEM) = True
            While Not rc.EOF
                PercCorretagem = IIf(IsNull(rc!perc_corretagem), vValZero, Format(Val(rc!perc_corretagem), "0.00"))
                NomeCorretor = Trim(rc!Nome)
                '
                If (Trim(PercCorretagem) <> "") And (Not ConfiguracaoBrasil) Then
                    PercCorretagem = TrocaValorAmePorBras(PercCorretagem)
                End If
                '
                Set vCorretor = New Corretor
                vCorretor.Id = rc!Corretor_id
                vCorretor.Nome = NomeCorretor
                vCorretor.PercCorretagem = PercCorretagem
                vCorretor.DtIniCorretagem = IIf(IsNull(rc!dt_inicio_corretagem), "", rc!dt_inicio_corretagem)
                vCorretor.DtFimCorretagem = IIf(IsNull(rc!dt_fim_corretagem), "", rc!dt_fim_corretagem)
                vCorretor.Tipo = rc!Tipo
                vCorretor.Sucursal = rc!sucursal_corretor_id
                '
                SubGrupos_endosso(CStr(rc!sub_grupo_id)).Corretores.Add vCorretor
                '
                rc.MoveNext
            Wend
            '
        End If
    End If
    '
    rc.Close
    Set rc = Nothing
    '
    Exit Sub

Erro_Corretagem_endosso:
    TrataErroGeral "Prepara_Corretagem_Geral"
    MsgBox "Erro na rotina Prepara_Corretagem_Geral."

End Sub
Private Sub Monta_GridCorretores_endosso()

    Dim linha As String
    Dim vCorretor As Corretor
    Dim TotCorretagem As Double

    GridCorretores.Rows = 1
    TotCorretagem = 0
    '
    For Each vCorretor In Corretores_endosso
        linha = Format(vCorretor.Id, "######-###")
        linha = linha & vbTab & vCorretor.Tipo
        linha = linha & vbTab & Format(vCorretor.PercCorretagem, "0.00000")
        linha = linha & vbTab & vCorretor.Nome
        linha = linha & vbTab & vCorretor.Sucursal
        linha = linha & vbTab & Format(vCorretor.DtIniCorretagem, "dd/mm/yyyy")
        linha = linha & vbTab & Format(vCorretor.DtFimCorretagem, "dd/mm/yyyy")
        '
        GridCorretores.AddItem linha
        GridCorretores.RowData(GridCorretores.Rows - 1) = vCorretor.Id
        '
        TotCorretagem = TotCorretagem + vCorretor.PercCorretagem
    Next
    '
    '
    GridCorretores.Refresh
    '

End Sub

Sub Monta_GridComponentes_endosso()
    Dim vComponente As Componente
    Dim linha As String

    GridComponente.Rows = 1
    '
    For Each vComponente In Componentes_endosso
        linha = Format(vComponente.Codigo, "000") & vbTab
        linha = linha & vComponente.Nome & vbTab
        linha = linha & Format(vComponente.CodSubRamo, "0000") & vbTab
        If vComponente.DtIniVigSbr <> 0 Then
            linha = linha & Format(vComponente.DtIniVigSbr, "dd/mm/yyyy") & vbTab
        End If
        If vComponente.DtFimVigSbr <> 0 Then
            linha = linha & Format(vComponente.DtFimVigSbr, "dd/mm/yyyy")
        End If
        '
        GridComponente.AddItem linha
        GridComponente.RowData(GridComponente.Rows - 1) = vComponente.Codigo
    Next
    '
    GridComponente.Refresh

End Sub
Private Sub Prepara_Corretagem_Fatura()
    Dim rc As rdoResultset
    Dim vSql As String
    Dim CodCorretor As String
    Dim NomeCorretor As String
    Dim PercCorretagem As String
    Dim vCorretor As Corretor
    Dim DataEmissaoEndosso As String
    Dim sub_grupo_fatura As String
    Dim SubGrupoIdFatura As String
    Dim ind_sg As Integer

    On Error GoTo Erro_Corr_end_fatura

    CodCorretor = ""
    PercCorretagem = ""
    TotCorretagem = 0#
    HaCorretor = False
    '
    GridCorretores.Rows = 1
    '
    DataEmissaoEndosso = Format(dtInicioVigEndosso, "yyyymmdd")
    If HaSubGrupos Then
        sub_grupo_fatura = ", fa.sub_grupo_id "
        cboSubGrupo(0).Locked = False
    End If
    '

    'maria implata��o Sql-2012 14102014
    'vSql = "SELECT c.corretor_id "
    'vSql = vSql & ", sucursal_corretor_id = ''"
    'vSql = vSql & ", 'tipo' = 'P. Fisica' "
    'vSql = vSql & ", c.nome, fa.dt_inicio_vigencia, fa.dt_fim_vigencia "
    'vSql = vSql & ", cpf.perc_corretagem " & sub_grupo_fatura
    'vSql = vSql & " FROM fatura_tb fa  WITH (NOLOCK) , corretagem_pf_endosso_fin_tb cpf  WITH (NOLOCK) , corretor_tb c   WITH (NOLOCK) "
    'vSql = vSql & " WHERE cpf.proposta_id = " & vPropostaId
    'vSql = vSql & " AND cpf.endosso_id = " & gEndossoId
    'vSql = vSql & " AND fa.proposta_id =* cpf.proposta_id "
    'vSql = vSql & " AND fa.endosso_id =* cpf.endosso_id "
    'vSql = vSql & " AND c.corretor_id = cpf.corretor_id"
    'vSql = vSql & " UNION "
    'vSql = vSql & " SELECT c.corretor_id, cpj.sucursal_corretor_id "
    'vSql = vSql & ", 'tipo' = 'P. Juridica' "
    'vSql = vSql & ", c.nome, fa.dt_inicio_vigencia, fa.dt_fim_vigencia "
    'vSql = vSql & ", cpj.perc_corretagem " & sub_grupo_fatura
    'vSql = vSql & " FROM fatura_tb fa  WITH (NOLOCK) , corretagem_pj_endosso_fin_tb cpj  WITH (NOLOCK) , corretor_tb c   WITH (NOLOCK) "
    'vSql = vSql & " WHERE cpj.proposta_id = " & vPropostaId
    'vSql = vSql & " AND cpj.endosso_id = " & gEndossoId
    'vSql = vSql & " AND fa.proposta_id =* cpj.proposta_id "
    'vSql = vSql & " AND fa.endosso_id =* cpj.endosso_id "
    'vSql = vSql & " AND c.corretor_id = cpj.corretor_id "
    'vSql = vSql & " ORDER BY c.corretor_id "
    '

    vSql = "SELECT c.corretor_id "
    vSql = vSql & ", sucursal_corretor_id = '' "
    vSql = vSql & ", 'tipo' = 'P. Fisica'  "
    vSql = vSql & ", c.nome, fa.dt_inicio_vigencia, fa.dt_fim_vigencia "
    vSql = vSql & ", cpf.perc_corretagem " & sub_grupo_fatura
    vSql = vSql & " FROM fatura_tb fa  WITH (NOLOCK) "
    vSql = vSql & " RIGHT join corretagem_pf_endosso_fin_tb cpf  WITH (NOLOCK) "
    vSql = vSql & "    ON fa.proposta_id = cpf.proposta_id "
    vSql = vSql & "   AND fa.endosso_id = cpf.endosso_id "
    vSql = vSql & "  join corretor_tb c   WITH (NOLOCK) "
    vSql = vSql & "    on  c.corretor_id = cpf.corretor_id "
    vSql = vSql & " WHERE cpf.proposta_id = " & vPropostaId
    vSql = vSql & "   AND cpf.endosso_id = " & gEndossoId

    vSql = vSql & " UNION "

    vSql = vSql & " SELECT c.corretor_id, cpj.sucursal_corretor_id "
    vSql = vSql & ", 'tipo' = 'P. Juridica' "
    vSql = vSql & ", c.nome, fa.dt_inicio_vigencia, fa.dt_fim_vigencia "
    vSql = vSql & ", cpj.perc_corretagem " & sub_grupo_fatura
    vSql = vSql & "  FROM fatura_tb fa  WITH (NOLOCK) "
    vSql = vSql & " RIGHT join  corretagem_pj_endosso_fin_tb cpj  WITH (NOLOCK) "
    vSql = vSql & "    ON fa.proposta_id = cpj.proposta_id "
    vSql = vSql & "   AND fa.endosso_id = cpj.endosso_id "
    vSql = vSql & "  join corretor_tb c   WITH (NOLOCK) "
    vSql = vSql & "    ON  c.corretor_id = cpj.corretor_id "
    vSql = vSql & " WHERE cpj.proposta_id = " & vPropostaId
    vSql = vSql & "   AND cpj.endosso_id = " & gEndossoId
    vSql = vSql & " ORDER BY c.corretor_id "



    Set rc = rdoCn1.OpenResultset(vSql, rdOpenStatic)
    'If Not rc.EOF Then
    tabEndossos.TabEnabled(TABendosso_CORRETAGEM) = True
    'End If
    While Not rc.EOF
        PercCorretagem = IIf(IsNull(rc!perc_corretagem), vValZero, Format(Val(rc!perc_corretagem), "0.0"))
        NomeCorretor = Trim(rc!Nome)
        If HaSubGrupos Then
            SubGrupoIdFatura = IIf(IsNull(rc!sub_grupo_id), "", rc!sub_grupo_id)
            If SubGrupoIdFatura = "" Then
                cboSubGrupo(0).ListIndex = -1
            Else
                For ind_sg = 0 To cboSubGrupo(0).ListCount - 1
                    If cboSubGrupo(0).ItemData(ind_sg) = Val(SubGrupoIdFatura) Then
                        cboSubGrupo(0).ListIndex = ind_sg
                        Exit For
                    End If
                Next ind_sg
            End If
            '        CboSubGrupo(0).Locked = True
        End If
        '
        If (Trim(PercCorretagem) <> "") And (Not ConfiguracaoBrasil) Then
            PercCorretagem = TrocaValorAmePorBras(PercCorretagem)
        End If
        '
        Set Corretores_endosso = Nothing
        Set vCorretor = New Corretor
        vCorretor.Id = rc!Corretor_id
        vCorretor.Nome = NomeCorretor
        vCorretor.PercCorretagem = PercCorretagem
        vCorretor.DtIniCorretagem = IIf(IsNull(rc!dt_inicio_vigencia), " ", rc!dt_inicio_vigencia)
        vCorretor.DtFimCorretagem = IIf(IsNull(rc!dt_fim_vigencia), " ", rc!dt_fim_vigencia)
        vCorretor.Tipo = rc!Tipo
        vCorretor.Sucursal = rc!sucursal_corretor_id
        '
        Corretores_endosso.Add vCorretor, CStr(rc!Corretor_id)
        '
        rc.MoveNext
    Wend
    '
    cboSubGrupo(0).Locked = True
    Monta_GridCorretores_endosso
    '
    Set rc = Nothing

    Exit Sub

Erro_Corr_end_fatura:
    TrataErroGeral "Prepara_Corretagem_Fatura"
    MsgBox "Erro na rotina Prepara_Corretagem_Fatura."

End Sub

Private Sub Prepara_Cosseguro_Geral()
    Dim HaCosseguroCedido As Boolean
    Dim HaCosseguroAceito As Boolean
    Dim SQL As String
    Dim linha As String
    Dim rec As rdoResultset
    Dim rc As rdoResultset

    On Error GoTo Erro_PreparaCosseguro

    GridCongeneres.Rows = 1

    '  Se cosseguro cedido ...
    '
    If UCase(gTpEmissao) = "C" Then
        SQL = "SELECT cosr.rep_seguradora_cod_susep "
        SQL = SQL & ", seg.nome "
        SQL = SQL & ", cosr.perc_participacao "
        SQL = SQL & ", cosr.perc_despesa_lider "
        SQL = SQL & ", cosr.dt_inicio_participacao "
        SQL = SQL & ", cosr.num_ordem "
        SQL = SQL & "FROM co_seguro_repassado_tb cosr   WITH (NOLOCK) "
        SQL = SQL & ", seguradora_tb seg   WITH (NOLOCK) "
        SQL = SQL & "WHERE cosr.proposta_id = " & vPropostaId
        SQL = SQL & " AND cosr.endosso_id = " & gEndossoId
        SQL = SQL & " AND seg.seguradora_cod_susep = cosr.rep_seguradora_cod_susep"
        '

        Set rec = rdoCn1.OpenResultset(SQL, rdOpenStatic)

        If Not rec.EOF Then
            HaCosseguroCedido = True
            HaCosseguroAceito = False
        Else
            HaCosseguroCedido = False
            HaCosseguroAceito = True
        End If
        While Not rec.EOF
            '   Monta grid de cosseguro cedido.
            linha = rec!rep_seguradora_cod_susep & vbTab
            linha = linha & rec!Nome & vbTab
            linha = linha & rec!Perc_Participacao & vbTab
            linha = linha & rec!Perc_Despesa_Lider & vbTab
            linha = linha & rec!dt_inicio_participacao & vbTab
            linha = linha & rec!NUM_ORDEM & vbTab

            GridCongeneres.AddItem linha

            rec.MoveNext

        Wend
        If HaCosseguroCedido Then
            tabEndossos.TabEnabled(TABendosso_COSSEGURO) = True
            Frame1.Visible = False
            Frame4.Visible = True
            Frame6.Visible = False
        End If

    ElseIf UCase(gTpEmissao) = "A" Then
        ' Se cosseguro aceito ...
        '
        SQL = "SELECT num_ordem_co_seguro_aceito, "
        SQL = SQL & "num_endosso_cosseguro "
        SQL = SQL & "FROM endosso_co_seguro_aceito_tb   WITH (NOLOCK) "
        SQL = SQL & "WHERE proposta_id = " & vPropostaId
        SQL = SQL & "AND   endosso_id = " & gEndossoId

        Set rc = rdoCn1.OpenResultset(SQL, rdOpenStatic)

        If Not rc.EOF Then
            txtNumEndossoLider.Text = IIf(IsNull(rc!num_endosso_cosseguro) = False, rc!num_endosso_cosseguro, "    ")
            txtNumOrdemEndossoLider.Text = IIf(IsNull(rc!num_ordem_co_seguro_aceito) = False, rc!num_ordem_co_seguro_aceito, "    ")
            tabEndossos.TabEnabled(TABendosso_COSSEGURO) = True

        End If

        Set rc = Nothing

        SQL = "SELECT a.seg_cod_susep_lider, a.sucursal_seg_lider, s.nome"
        SQL = SQL & ", c.num_apolice_lider, c.dt_emissa_apolice_lider, c.num_ordem_co_seguro_aceito "
        SQL = SQL & "FROM apolice_terceiros_tb a  WITH (NOLOCK) , co_seguro_aceito_tb c  WITH (NOLOCK) , seguradora_tb s   WITH (NOLOCK) "
        SQL = SQL & "WHERE a.apolice_id = " & vApoliceId
        SQL = SQL & " AND a.ramo_id = " & vRamoId
        SQL = SQL & " AND a.seguradora_cod_susep = " & vSeguradoraId
        SQL = SQL & " AND a.sucursal_seguradora_id = " & vSucursalId
        SQL = SQL & " AND c.num_ordem_co_seguro_aceito = a.num_ordem_co_seguro_aceito "
        SQL = SQL & " AND c.sucursal_seg_lider = a.sucursal_seg_lider "
        SQL = SQL & " AND c.seg_cod_susep_lider = a.seg_cod_susep_lider "
        SQL = SQL & " AND s.seguradora_cod_susep = a.seg_cod_susep_lider "

        Set rc = rdoCn1.OpenResultset(SQL, rdOpenStatic)

        If Not rc.EOF Then
            If Not IsNull(rc!dt_emissa_apolice_lider) Then
                txtDtEmissaoLider.Text = Format(rc!dt_emissa_apolice_lider, "dd/mm/yyyy")
            End If
            '
            If Not IsNull(rc!Nome) Then
                txtSeguradoraLider.Text = Trim(rc!Nome)
            End If
            '
            If Not IsNull(rc!num_apolice_lider) Then
                txtApoliceLider.Text = rc!num_apolice_lider
            End If
            HaCosseguroAceito = True
            HaCosseguroCedido = False
            tabEndossos.TabEnabled(TABendosso_COSSEGURO) = True
            Frame1.Visible = False
            Frame4.Visible = False
            Frame6.Visible = True
            Frame6.Height = 3015
            Frame6.Top = 360
        End If

        rc.Close
        Set rc = Nothing
        '
    End If
    '
    Exit Sub
    '
Erro_PreparaCosseguro:
    TrataErroGeral "Erro preparando cosseguros!"
    MsgBox "Erro na rotina Prepara_Cosseguro "

End Sub

Private Sub Prepara_Cosseguro_Fatura()
'
    Dim rec As rdoResultset
    Dim NovaCongenere As Congenere
    Dim SQL As String
    Dim NomeCongenere As String
    Dim CodCongenere As String
    Dim DataEndosso As String
    Dim HaCongenere As Boolean

    DataEndosso = dtInicioVigEndosso
    HaCongenere = False
    Set Congeneres_endosso = Nothing

    '
    SQL = "SELECT cosr.rep_seguradora_cod_susep "
    SQL = SQL & ", seg.nome "
    SQL = SQL & ", cosr.dt_inicio_participacao "
    SQL = SQL & ", cosr.dt_fim_participacao "
    SQL = SQL & ", isnull(cosr.perc_participacao,0) perc_participacao "
    SQL = SQL & ", isnull(cosr.perc_despesa_lider,0) perc_despesa_lider "
    SQL = SQL & ", cosr.num_ordem "
    SQL = SQL & "FROM co_seguro_repassado_tb cosr   WITH (NOLOCK) "
    SQL = SQL & ", seguradora_tb seg   WITH (NOLOCK) "
    SQL = SQL & "WHERE cosr.proposta_id = " & vPropostaId
    SQL = SQL & " AND cosr.dt_inicio_participacao <= '" & Format(DataEndosso, "yyyymmdd") & "'"
    SQL = SQL & " AND (cosr.dt_fim_participacao is null or cosr.dt_fim_participacao > '" & Format(DataEndosso, "yyyymmdd") & "')"
    SQL = SQL & " AND seg.seguradora_cod_susep = cosr.rep_seguradora_cod_susep "
    '
    Set rec = rdoCn1.OpenResultset(SQL, rdOpenStatic)

    While Not rec.EOF
        HaCongenere = True

        Set NovaCongenere = New Congenere

        With NovaCongenere
            ' Guarda informacoes das congeneres do periodo coberto pelo endosso.
            NomeCongenere = rec!Nome
            CodCongenere = rec!rep_seguradora_cod_susep
            .Nome = NomeCongenere
            .Seguradora = CodCongenere
            .DtIniParticipacao = Format(rec!dt_inicio_participacao, "yyyymmdd")
            .DtFimParticipacao = Format(rec!dt_fim_participacao, "yyyymmdd")
            .NumOrdem = rec!NUM_ORDEM
            .Perc_Despesa_Lider = FormatValor(rec("perc_despesa_lider"))
            .Perc_Participacao = FormatValor(rec("perc_participacao"))
            ' Acrescenta a congenere na rela��o.
            CboCongeneresCed.AddItem Trim(NomeCongenere)
            CboCongeneresCed.ItemData(CboCongeneresCed.NewIndex) = CodCongenere
            ' Inclui congenere na cole��o.
            Congeneres_endosso.Add NovaCongenere, CStr(rec!rep_seguradora_cod_susep)

            rec.MoveNext
        End With
    Wend
    '

    If HaCongenere Then
        Frame1.Top = 360
        Frame1.Left = 200
        Frame1.Height = 3550
        Frame1.Width = 10455
        CboCongeneresCed.Refresh
        '
        Frame1.Visible = True
        Frame4.Visible = False
        Frame6.Visible = False
        Frame1.Refresh
        '
        tabEndossos.TabEnabled(TABendosso_COSSEGURO) = True
    Else
        tabEndossos.TabEnabled(TABendosso_COSSEGURO) = False
    End If

    '
    Exit Sub
    '
Erro_PreparaCosseguro_Fatura:
    TrataErroGeral "Erro preparando cosseguros fatura!"
    MsgBox "Erro na rotina Prepara_Cosseguro_Fatura "

End Sub


Private Sub Prepara_Dados_Cadastrais()
    Dim SQL As String
    Dim Sql_Aux As String
    Dim Ss As rdoResultset
    Dim RecNome As rdoResultset
    Dim i As Integer

    On Error GoTo Erro_DadosCadastrais_endosso

    HaConteudo = False

    SQL = " SELECT "
    SQL = SQL & " 'Nome'               = ult_nome, "
    SQL = SQL & " 'Nome_Conjuge'       = ult_nome_conjuge, "
    SQL = SQL & " 'Sexo'               = ult_sexo, "
    SQL = SQL & " 'Estado_Civil'       = ult_estado_civil, "
    SQL = SQL & " 'Endereco'           = ult_endereco, "
    SQL = SQL & " 'Telefone'           = ult_telefone, "
    SQL = SQL & " 'ddd'                = ult_ddd, "
    SQL = SQL & " 'Municipio'          = ult_municipio, "
    SQL = SQL & " 'Estado'             = ult_estado, "
    SQL = SQL & " 'CEP'                = ult_cep, "
    SQL = SQL & " 'e mail'             = ult_e_mail, "
    SQL = SQL & " 'Numero_Cart�o'      = ult_num_cartao, "
    SQL = SQL & " 'Validade_do_Cart�o' = CONVERT(CHAR(10),ult_dt_validade_cartao,103),  "
    SQL = SQL & " 'Agencia'            = ult_deb_agencia_id, "
    SQL = SQL & " 'Conta_Corrente'     = ult_deb_conta_corrente_id, "
    SQL = SQL & " 'Banco'              = ult_deb_banco_id, "
    SQL = SQL & " 'Data_de_Nascimento' = CONVERT(CHAR(10),ult_dt_nascimento,103),  "
    SQL = SQL & " 'CGC'                = ult_cgc, "
    SQL = SQL & " 'CPF'                = ult_cpf, "
    SQL = SQL & " 'Dia_Cobran�a'       = ult_data_cobranca_dia, "
    SQL = SQL & " 'Inicio_Viagem'      = CONVERT(CHAR(10),ult_dt_inicio_viagem,103), "
    SQL = SQL & " 'Fim_Viagem'         = CONVERT(CHAR(10),ult_dt_fim_viagem,103), "
    SQL = SQL & " 'Fim_Vigencia'       = CONVERT(CHAR(10),ult_dt_fim_vigencia,103) "
    SQL = SQL & " FROM endosso_proposta_tb   WITH (NOLOCK) "
    SQL = SQL & " WHERE proposta_id = " & vPropostaId
    SQL = SQL & " AND endosso_id = " & gEndossoId
    '
    Set Ss = rdoCn1.OpenResultset(SQL, rdOpenStatic)

    If Not Ss.EOF Then
        txtCodCliAnt.Text = vClienteId
        If Not IsNull(Ss!Nome) Then
            txtNomeAnt.Text = Trim(Ss!Nome)
            HaConteudo = True
        End If
        If Not IsNull(Ss!Endereco) Then
            TxtEnderecoAnt.Text = Trim(Ss!Endereco)
            HaConteudo = True
        End If
        If Not IsNull(Ss!Municipio) Then
            TxtCidadeAnt.Text = Trim(Ss!Municipio)
            HaConteudo = True
        End If
        If Not IsNull(Ss!DDD) Then
            TxtDDDAnt.Text = Trim(Format(Ss!DDD, "000"))
            HaConteudo = True
        End If
        If Not IsNull(Ss!telefone) Then
            TxtTelefoneAnt.Text = Trim(Ss!telefone)
            HaConteudo = True
        End If
        If Not IsNull(Ss!Estado) Then
            TxtUFAnt.Text = Trim(Ss!Estado)
            HaConteudo = True
        End If
        If Not IsNull(Ss!CEP) Then
            TxtCEPAnt.Text = Trim(Format(Ss!CEP, "00000-000"))
            HaConteudo = True
        End If
        If (Not IsNull(Ss!CPF)) Or (Not IsNull(Ss!CGC)) Then
            HaConteudo = True
            If IdTpPessoa_endosso = 1 Then  ' PF
                lblConsEndosso34.Caption = "CPF :"
                TxtCPF_CGCAnt.Text = Trim(Format$(Ss!CPF, "&&&.&&&.&&&-&&"))
            Else
                lblConsEndosso34.Caption = "CNPJ :"
                TxtCPF_CGCAnt.Text = Trim(Format(Ss!CGC, "&&.&&&.&&&/&&&&-&&"))
            End If
            TxtIdTpPessoaAnt.Text = IdTpPessoa_endosso
            TxtTpPessoaAnt.Text = IIf(IdTpPessoa_endosso = 1, "PESSOA F�SICA", "PESSOA JUR�DICA")
            If IdTpPessoa_endosso = 1 Then    ' PF
                If Not IsNull(Ss!data_de_nascimento) Then
                    txtNascAnt.Text = Trim(Format(Ss!data_de_nascimento, "dd/mm/yyyy"))
                End If
            Else    'PJ
                lblConsEndosso35.Visible = False
                txtNascAnt.Visible = False
            End If
        End If
        If Not IsNull(Ss!banco) Then
            TxtBancoAnt.Text = Trim(Format(Ss!banco, "0000"))
            Sql_Aux = "SELECT nome FROM banco_tb   WITH (NOLOCK) "
            Sql_Aux = Sql_Aux & "WHERE banco_id = " & Val(TxtBancoAnt.Text)
            '
            Set RecNome = rdoCn1.OpenResultset(Sql_Aux, rdOpenStatic)
            If Not RecNome.EOF Then
                If Not IsNull(RecNome!Nome) Then
                    TxtNomeBancoAnt.Text = Trim(RecNome!Nome)
                End If
            End If
            RecNome.Close
            Set RecNome = Nothing
            HaConteudo = True
        End If
        If Not IsNull(Ss!agencia) Then
            TxtCodAgAnt.Text = Trim(Format(Ss!agencia, "0000"))
            Sql_Aux = "SELECT nome FROM agencia_tb   WITH (NOLOCK) "
            Sql_Aux = Sql_Aux & "WHERE agencia_id = " & Ss!agencia
            Sql_Aux = Sql_Aux & " AND banco_id = " & Val(TxtBancoAnt.Text)
            '
            Set RecNome = rdoCn1.OpenResultset(Sql_Aux, rdOpenStatic)
            If Not RecNome.EOF Then
                If Not IsNull(RecNome!Nome) Then
                    TxtNomeAgAnt.Text = Trim(RecNome!Nome)
                End If
            End If
            RecNome.Close
            Set RecNome = Nothing
            HaConteudo = True
        End If
        If Not IsNull(Ss!conta_corrente) Then
            TxtCCAnt.Text = Trim(Ss!conta_corrente)
            HaConteudo = True
        End If
    End If

    If HaConteudo Then
        tabEndossos.TabEnabled(TABendosso_DADOSCADASTRAIS) = True
    Else
        tabEndossos.TabEnabled(TABendosso_DADOSCADASTRAIS) = False
    End If

    Exit Sub

Erro_DadosCadastrais_endosso:
    TrataErroGeral "Prepara_Dados_Cadastrais"
    MsgBox "Erro na rotina Prepara_Dados_Cadastrais."

End Sub

Private Sub Prepara_Proposta()
    Dim SQL As String
    Dim sql2 As String
    Dim rs As rdoResultset
    Dim DescEndosso As Variant

    On Error GoTo Erro_Proposta_endosso

    Lblproposta_id.Caption = vPropostaId
    LbLProduto.Caption = ConPropBasicaVida.txtProduto.Text
    LblPropostaBB.Caption = ConPropBasicaVida.txtPropostaBB.Text
    lblApolice_id.Caption = vApoliceId
    lblRamo.Caption = ConPropBasicaVida.txtRamo.Text
    Lbldt_emissao.Caption = gDtEmiEndosso
    Lblendosso_id.Caption = gEndossoId
    Lbltp_endosso_id.Caption = gDescTpEndosso
    Lbldt_pedido_endosso.Caption = gDtIniVigEndosso
    txtDescEndosso.Text = gDescEndosso
    txtUsuEmissor.Text = gUsuEmissao
    txtDesctpAlteracaoEndosso.Text = gTpAltEnd  'rfarzat - 24/10/2018 - Endosso 252 - Sala �gil

    '  Obtem tipo de cancelamento da proposta.
    'sql2 = "SELECT cp.tp_cancelamento, tp.descricao "           'eSantos Flow 153278  05/08/2006
    sql2 = "SELECT cp.tp_cancelamento, cp.motivo_cancelamento as Descricao  "  'eSantos Flow 153278  05/08/2006
    sql2 = sql2 & "FROM cancelamento_proposta_tb cp   WITH (NOLOCK) "
    sql2 = sql2 & ", tp_cancelamento_tb tp   WITH (NOLOCK) "
    sql2 = sql2 & "WHERE cp.proposta_id = " & vPropostaId
    sql2 = sql2 & " AND cp.endosso_id = " & gEndossoId
    sql2 = sql2 & " AND tp.tp_cancelamento = cp.tp_cancelamento"

    Set rs = rdoCn1.OpenResultset(sql2, rdOpenStatic)

    If Not rs.EOF Then
        LblTpCancEndosso.Caption = Trim(" " & rs!Descricao)
        Label10.Visible = True
        LblTpCancEndosso.Visible = True
    Else
        Label10.Visible = False
        LblTpCancEndosso.Visible = False
    End If

    tabEndossos.TabEnabled(TABendosso_PROPOSTA) = True

    Set rs = Nothing

    '' Obtem numero do endosso de cobranca / fatura cancelado - cancelamento de endossos
    If Val(gTpEndosso) = 101 Then
        SQL = "SELECT isnull(canc_endosso_id,0) canc_endosso_id "
        SQL = SQL & "FROM cancelamento_endosso_tb   WITH (NOLOCK) "
        SQL = SQL & "WHERE proposta_id = " & vPropostaId
        SQL = SQL & " AND endosso_id = " & gEndossoId

        Set rs = rdoCn1.OpenResultset(SQL, rdOpenStatic)

        If Not rs.EOF Then
            If (rs!canc_endosso_id <> 0) Then
                LblTpCancEndosso.Caption = UCase("cancelamento do endosso - ") & rs!canc_endosso_id
                Label10.Visible = True
                LblTpCancEndosso.Visible = True
                num_endosso_cancelado = rs!canc_endosso_id
                num_endosso_consulta = gEndossoId
                gEndossoId = Val(num_endosso_cancelado)
                TxtSituacao.Text = "Cancelado"
            Else
                Label10.Visible = False
                LblTpCancEndosso.Visible = False
            End If
        Else
            Label10.Visible = False
            LblTpCancEndosso.Visible = False
        End If

        tabEndossos.TabEnabled(TABendosso_PROPOSTA) = True

    End If

    Call Ler_Endosso_Financeiro

    '' Obtem data de vencimento da fatura.
    If InStr(1, tp_endo_fatura, CStr(gTpEndosso), 1) > 0 Then
        sql2 = "SELECT isnull(dt_recebimento, ' ') dt_recebimento "
        sql2 = sql2 & "FROM fatura_tb   WITH (NOLOCK) "
        sql2 = sql2 & "WHERE proposta_id = " & vPropostaId
        sql2 = sql2 & " AND endosso_id = " & gEndossoId

        Set rs = rdoCn1.OpenResultset(sql2, rdOpenStatic)

        If Not rs.EOF Then
            If Trim(rs("dt_recebimento")) <> " " Then
                TxtDtVenc.Text = Format(Trim(rs("dt_recebimento")), "dd/mm/yyyy")
            End If
        End If

    End If

    Exit Sub

Erro_Proposta_endosso:
    TrataErroGeral "Prepara_Proposta"
    MensagemBatch "Erro na rotina Prepara_Proposta."

End Sub

Public Sub carrega_congenere()

    SQL = "SELECT seguradora_cod_susep, nome from seguradora_tb  WITH (NOLOCK)  order by nome"
    Set rc = rdocn.OpenResultset(SQL)

    With cmbCongenere
        .Clear
        If Not rc.EOF Then
            While Not rc.EOF
                .AddItem rc(1)
                .ItemData(.NewIndex) = rc(0)
                rc.MoveNext
            Wend
        End If
        rc.Close
    End With

End Sub

Sub carrega_cboClasseLocal()
    Dim linha As String

    SQL = "SELECT classe_local_id, nome FROM classe_local_tb   WITH (NOLOCK) "
    Set rs = rdocn.OpenResultset(SQL)

    Do While Not rs.EOF
        linha = Format$(rs!classe_local_id, "000")
        linha = linha & " - " & rs!Nome
        cboClasseLocal.AddItem linha
        cboClasseLocal.ItemData(cboClasseLocal.NewIndex) = rs(0)
        rs.MoveNext
    Loop
    rs.Close

End Sub
Function Ler_Clausulas(ByVal codClausula As String) As String


    On Error GoTo Erro

    SQL = "SELECT texto"
    SQL = SQL & " FROM  clausula_tb   WITH (NOLOCK) "
    SQL = SQL & " WHERE cod_clausula = " & codClausula
    SQL = SQL & "   AND dt_inicio_vigencia <= '" & Format(txtDtContratacao.Text, "YYYYMMDD") & "'"
    SQL = SQL & "   AND dt_fim_vigencia is null"

    Set rc = rdoCn1.OpenResultset(SQL)

    If Not rc.EOF Then
        Ler_Clausulas = rc("texto")
    End If

    rc.Close

    Exit Function

Erro:
    TrataErroGeral "Ler_Cl�usula"
    MsgBox "Rotina: Ler_Cla�sula"
End Function


Function Formata_Clausula(TEXTOCLAUSULA) As String

    Dim Texto As String, ULTIMA_QUEBRA As Variant
    Dim Encontrou As Boolean, FRASE As String
    Dim CONT_CLAUSULA As Variant, CONT_FRASE As Variant
    Dim ACHA_ESPACO As Integer

    ULTIMA_QUEBRA = 1
    Encontrou = False
    Texto = ""
    CONT_FRASE = 0
    For CONT_CLAUSULA = 1 To Len(TEXTOCLAUSULA)
        CONT_FRASE = CONT_FRASE + 1
        If Mid(TEXTOCLAUSULA, CONT_CLAUSULA, 1) = Chr(13) Then
            FRASE = Mid(TEXTOCLAUSULA, ULTIMA_QUEBRA, CONT_CLAUSULA - ULTIMA_QUEBRA) & " " & vbNewLine
            ULTIMA_QUEBRA = CONT_CLAUSULA + 2
            CONT_CLAUSULA = CONT_CLAUSULA + 1
            CONT_FRASE = 0
        ElseIf CONT_FRASE = 90 Then
            Encontrou = False
            If Mid(TEXTOCLAUSULA, CONT_CLAUSULA + 1, 1) <> " " And Mid(TEXTOCLAUSULA, CONT_CLAUSULA + 1, 1) <> Chr(13) Then
                For ACHA_ESPACO = CONT_CLAUSULA To ULTIMA_QUEBRA Step -1
                    If Mid(TEXTOCLAUSULA, ACHA_ESPACO, 1) = " " Then
                        FRASE = Mid(TEXTOCLAUSULA, ULTIMA_QUEBRA, ACHA_ESPACO - ULTIMA_QUEBRA) & vbNewLine
                        CONT_FRASE = CONT_CLAUSULA - ACHA_ESPACO
                        ULTIMA_QUEBRA = ACHA_ESPACO + 1
                        Encontrou = True
                        Exit For
                    End If
                Next ACHA_ESPACO
            End If
            If Not Encontrou Then
                FRASE = RTrim(Mid(TEXTOCLAUSULA, ULTIMA_QUEBRA, 90)) & vbNewLine
                CONT_FRASE = 0
                ULTIMA_QUEBRA = ULTIMA_QUEBRA + 90
                If Mid(TEXTOCLAUSULA, ULTIMA_QUEBRA, 1) = Chr(13) Then
                    ULTIMA_QUEBRA = ULTIMA_QUEBRA + 2
                    CONT_CLAUSULA = CONT_CLAUSULA + 2
                ElseIf Mid(TEXTOCLAUSULA, ULTIMA_QUEBRA, 1) = " " Then
                    ULTIMA_QUEBRA = ULTIMA_QUEBRA + 1
                    CONT_CLAUSULA = CONT_CLAUSULA + 1
                    If Mid(TEXTOCLAUSULA, ULTIMA_QUEBRA, 1) = Chr(13) Then
                        ULTIMA_QUEBRA = ULTIMA_QUEBRA + 2
                        CONT_CLAUSULA = CONT_CLAUSULA + 2
                    End If
                End If
            End If
        End If
        If FRASE <> "" Then
            Texto = Texto & FRASE
            FRASE = ""
        End If
    Next CONT_CLAUSULA
    If ULTIMA_QUEBRA < Len(TEXTOCLAUSULA) Then
        Texto = Texto & Mid(TEXTOCLAUSULA, ULTIMA_QUEBRA, CONT_CLAUSULA - ULTIMA_QUEBRA + 1)
    End If

    Formata_Clausula = Texto

End Function



Public Sub Monta_GridClausulas_endosso()

    Dim clausulaElemento As Clausula
    Dim linha As String
    Dim HaClausula As Boolean

    With ConEndosso
        .gridClausulas.Rows = 1
        If Clausulas_endosso.Count > 0 Then
            HaClausula = True
            For Each clausulaElemento In Clausulas_endosso
                .gridClausulas.Row = .gridClausulas.Rows - 1
                linha = clausulaElemento.Id & vbTab
                linha = linha & clausulaElemento.Codigo & vbTab
                linha = linha & clausulaElemento.Nome & vbTab
                If clausulaElemento.DtIniVigencia <> 0 Then
                    linha = linha & clausulaElemento.DtIniVigencia & vbTab
                End If
                If clausulaElemento.DtFimVigencia <> 0 Then
                    linha = linha & clausulaElemento.DtFimVigencia
                End If
                .gridClausulas.AddItem linha
                .gridClausulas.RowData(.gridClausulas.Rows - 1) = clausulaElemento.Id
            Next
            .gridClausulas.col = 0
            .gridClausulas.ColSel = 0
            .gridClausulas.Sort = 0
        End If
    End With

    If HaClausula Then
        tabEndossos.TabEnabled(TABendosso_CLAUSULA) = True
    Else
        tabEndossos.TabEnabled(TABendosso_CLAUSULA) = False
    End If

End Sub

Private Sub Prepara_Vidas()
    Dim linha As String
    Dim rvidas As rdoResultset
    Dim SQL As String
    Dim auxsub As String
    Dim i As Integer

    On Error GoTo Erro_Vida_endosso

    tabEndossos.TabEnabled(TABendosso_VIDA) = True

    SQL = "EXEC SEGS7859_SPS " & vApoliceId & ", " & vRamoId & ", " & gEndossoId

    Set rvidas = rdocn.OpenResultset(SQL)

    msfVidas.Rows = 1

    While Not rvidas.EOF
        linha = rvidas("DtInicioCobertura")
        linha = linha & vbTab & rvidas("DtEntradaSubGrupo")
        linha = linha & vbTab & rvidas("Nome")
        linha = linha & vbTab & rvidas("CPF")
        linha = linha & vbTab & rvidas("DataNasc")
        linha = linha & vbTab & rvidas("Sexo")
        linha = linha & vbTab & rvidas("EstadoCivil")
        linha = linha & vbTab & rvidas("CapitalSegurado")
        linha = linha & vbTab & rvidas("Salario")
        linha = linha & vbTab & rvidas("Endereco")
        linha = linha & vbTab & rvidas("Bairro")
        linha = linha & vbTab & rvidas("Municipio")
        linha = linha & vbTab & rvidas("CEP")
        linha = linha & vbTab & rvidas("Estado")
        linha = linha & vbTab & rvidas("Email")
        linha = linha & vbTab & rvidas("DDD")
        linha = linha & vbTab & rvidas("Tel")
        linha = linha & vbTab & rvidas("NomeConjuge")
        linha = linha & vbTab & rvidas("CPFConjuge")
        linha = linha & vbTab & rvidas("DataNascConjuge")
        linha = linha & vbTab & rvidas("SexoConjuge")
        linha = linha & vbTab & rvidas("DtIniCoberturaConjuge")
        linha = linha & vbTab & rvidas("Beneficiario1")
        linha = linha & vbTab & rvidas("Beneficiario2")
        linha = linha & vbTab & rvidas("Beneficiario3")
        linha = linha & vbTab & rvidas("Beneficiario4")
        linha = linha & vbTab & rvidas("Beneficiario5")
        linha = linha & vbTab & rvidas("Beneficiario6")
        linha = linha & vbTab & rvidas("Beneficiario7")
        linha = linha & vbTab & rvidas("Pendencia")
        linha = linha & vbTab & rvidas("MotivoPendencia")
        'linha = linha & vbTab & rvidas("Premio")
        linha = linha & vbTab & rvidas("PremioBruto")
        'fabreu - Confitec (06/07/2010)
        ' O programa n�o retornava o valor do Premio, pois na procedure
        ' o mesmo passou a ser PremioBruto
        '
        msfVidas.AddItem linha
        '
        rvidas.MoveNext
    Wend

    rvidas.Close

    Exit Sub

Erro_Vida_endosso:
    TrataErroGeral "Prepara_Vidas", Me.name
    MensagemBatch "Erro na rotina Prepara_Vidas.", , , False
End Sub

Private Sub Prepara_SubGrupos()

    Dim linha As String
    Dim TipoIS As String
    Dim Conjuge As String
    Dim rc As rdoResultset
    Dim SQL As String
    Dim auxsub As String
    Dim NovoSubGrupoEnd As SubGrupo
    Dim i As Integer

    On Error GoTo Erro_SubGrupo_endosso

    If HaSubGrupos Then

        tabEndossos.TabEnabled(TABendosso_SUBGRUPO) = True

        SQL = "SELECT sa.sub_grupo_id, sa.nome, tp_is, isnull(qtd_salarios, 0) qtd_salarios"
        SQL = SQL & ", isnull(texto_beneficiario,'') txt_benef "
        SQL = SQL & ", isnull(qtd_vida, 0) qtd_vida "
        SQL = SQL & ", isnull(sa.tabua_calculo_id, 0) tabua_calculo_id "
        SQL = SQL & ", isnull(t.nome, '') nome_tabua "
        SQL = SQL & ", isnull(fatura_automatica, 'n') fatura_automatica "
        SQL = SQL & ", isnull(sa.dia_faturamento, 0) dia_faturamento "
        SQL = SQL & ", isnull(sa.dia_cobranca, 0) dia_cobranca "
        SQL = SQL & ", isnull(sa.periodo_pgto_id, 99) periodo_pgto_id "
        SQL = SQL & ", isnull(pp.nome, 'INDEFINIDO') nome_periodo "
        SQL = SQL & ", isnull(sa.forma_pgto_id, 99) forma_pgto_id "
        SQL = SQL & ", isnull(fp.nome, 'INDEFINIDO') nome_forma "
        SQL = SQL & ", isnull(sa.banco_id, 0) banco_id "
        SQL = SQL & ", isnull(sa.agencia_id, 0) agencia_id "
        SQL = SQL & ", isnull(ag.nome, '') nome_agencia "
        SQL = SQL & ", isnull(sa.conta_corrente_id, 0) conta_corrente_id "
        SQL = SQL & ", isnull(sa.tp_clausula_conjuge, 'n') tp_clausula_conjuge "
        SQL = SQL & ", isnull(sa.isento_iof, 'n') isento_iof "
        SQL = SQL & ", isnull(li.lim_min_inclusao, 0) lim_min_inc "
        SQL = SQL & ", isnull(li.lim_max_inclusao, 0) lim_max_inc "
        SQL = SQL & ", isnull(li.lim_min_movimentacao, 0) lim_min_mov "
        SQL = SQL & ", isnull(li.lim_max_movimentacao, 0) lim_max_mov "
        SQL = SQL & ", isnull(sa.cartao_proposta, '') cartao_proposta "
        SQL = SQL & ", isnull(ccp.critica_todos, '') critica_todos "
        SQL = SQL & ", isnull(ccp.critica_idade, '') critica_idade "
        SQL = SQL & ", isnull(ccp.critica_capital, '') critica_capital "
        SQL = SQL & ", isnull(ccp.lim_max_idade, 0) lim_max_idade "
        SQL = SQL & ", isnull(ccp.lim_max_capital, 0) lim_max_capital "
        SQL = SQL & ", convert(varchar(10), sa.dt_inicio_vigencia_sbg,103) dt_inicio_vigencia_sbg"
        SQL = SQL & ", isnull(convert(varchar(10), sa.dt_fim_vigencia_sbg,103),'') dt_fim_vigencia_sbg"

        ' Autor: rodrigo.almeida - Data Altera��o: 17/08/2011 - Demanda: 864186
        SQL = SQL & " INTO #sub_grupo_apolice_temp_tb "
        '

        SQL = SQL & " FROM sub_grupo_apolice_tb sa  WITH (NOLOCK) "
        SQL = SQL & " LEFT JOIN tabua_calculo_tb t   WITH (NOLOCK) "
        SQL = SQL & "   ON t.tabua_calculo_id = sa.tabua_calculo_id "
        SQL = SQL & " LEFT JOIN periodo_pgto_tb pp   WITH (NOLOCK) "
        SQL = SQL & "   ON  pp.periodo_pgto_id = sa.periodo_pgto_id "
        SQL = SQL & " LEFT JOIN forma_pgto_tb fp   WITH (NOLOCK) "
        SQL = SQL & "   ON  fp.forma_pgto_id = sa.forma_pgto_id "
        SQL = SQL & " LEFT JOIN agencia_tb ag   WITH (NOLOCK) "
        SQL = SQL & "   ON  ag.banco_id = sa.banco_id "
        SQL = SQL & "   AND ag.agencia_id = sa.agencia_id "
        SQL = SQL & " LEFT JOIN limite_idade_sub_grupo_apolice_tb li   WITH (NOLOCK) "
        SQL = SQL & "   ON  li.seguradora_cod_susep = sa.seguradora_cod_susep "
        SQL = SQL & "   AND li.sucursal_seguradora_id = sa.sucursal_seguradora_id "
        SQL = SQL & "   AND li.ramo_id = sa.ramo_id "
        SQL = SQL & "   AND li.apolice_id = sa.apolice_id "
        SQL = SQL & "   AND li.sub_grupo_id = sa.sub_grupo_id "
        SQL = SQL & "   AND li.dt_inicio_vigencia_sbg = sa.dt_inicio_vigencia_sbg "
        SQL = SQL & "   AND li.dt_inicio_limite <= '" & dt_inicio_query & "'"
        SQL = SQL & "   AND (li.dt_fim_limite is null OR li.dt_fim_limite > '" & dt_inicio_query & "')"
        SQL = SQL & " LEFT JOIN sub_grupo_criterio_cartao_proposta_tb ccp   WITH (NOLOCK) "
        SQL = SQL & "   ON sa.seguradora_cod_susep = ccp.seguradora_cod_susep "
        SQL = SQL & "   AND sa.sucursal_seguradora_id = ccp.sucursal_seguradora_id "
        SQL = SQL & "   AND sa.ramo_id = ccp.ramo_id "
        SQL = SQL & "   AND sa.apolice_id = ccp.apolice_id "
        SQL = SQL & "   AND sa.sub_grupo_id = ccp.sub_grupo_id "
        SQL = SQL & "   AND sa.dt_inicio_vigencia_sbg = ccp.dt_inicio_vigencia_sbg "
        '---------------------------------------------------------------------------------------------------
        'Luciana - 04/11/2003
        '---------------------------------------------------------------------------------------------------
        SQL = SQL & "   AND ccp.dt_inicio_criterio <='" & dt_inicio_query & "'"
        SQL = SQL & "   AND (ccp.dt_fim_criterio is null OR ccp.dt_fim_criterio >'" & dt_inicio_query & "')"
        '---------------------------------------------------------------------------------------------------
        SQL = SQL & " WHERE sa.apolice_id              = " & vApoliceId
        SQL = SQL & " AND sa.sucursal_seguradora_id    = " & vSucursalId
        SQL = SQL & " AND sa.seguradora_cod_susep      = " & vSeguradoraId
        SQL = SQL & " AND sa.ramo_id                   = " & vRamoId
        '

        ' Autor: rodrigo.almeida (Nova Consultoria) - Data Altera��o: 17/08/2011 - Demanda: 864186
        ' Modifica os Sub Grupos para receber os dados de acordo com o endosso de vig�ncia futura, quando houver.
        ' V�lido apenas para o produto 115
        If DateDiff("d", gDtIniVigEndosso, Date) < 0 Then

            SQL = SQL & vbNewLine & " "
            SQL = SQL & "DELETE temp "
            SQL = SQL & "  FROM #sub_grupo_apolice_temp_tb temp "
            SQL = SQL & " INNER JOIN ( "
            SQL = SQL & "       SELECT proposta_id "
            SQL = SQL & "            , CAST(MAX(CASE WHEN nome_campo = 'subgrupo' THEN valor_campo END) AS NUMERIC(5)) sub_grupo_id "
            SQL = SQL & "            , CAST(MAX(CASE WHEN nome_campo = 'seguradora' THEN valor_campo END) AS NUMERIC(5)) seguradora_cod_susep "
            SQL = SQL & "            , CAST(MAX(CASE WHEN nome_campo = 'sucursal' THEN valor_campo END) AS NUMERIC(5)) sucursal_seguradora_id "
            SQL = SQL & "            , CAST(MAX(CASE WHEN nome_campo = 'ramo' THEN valor_campo END) AS INT) ramo_id "
            SQL = SQL & "            , CAST(MAX(CASE WHEN nome_campo = 'apolice' THEN valor_campo END) AS NUMERIC(9)) apolice_id "
            SQL = SQL & "         FROM endosso_vigencia_futura_tb WITH(NOLOCK)"
            SQL = SQL & "        WHERE dt_aplicacao IS NULL "
            SQL = SQL & "          AND proposta_id = " & vPropostaId
            SQL = SQL & "          AND endosso_id = " & gEndossoId
            SQL = SQL & "          AND nome_procedure = 'sub_grupo_apolice_spd' "
            SQL = SQL & "        GROUP BY proposta_id ) evf "
            SQL = SQL & "    ON temp.sub_grupo_id = evf.sub_grupo_id "
            SQL = SQL & "   AND evf.apolice_id = " & vApoliceId
            SQL = SQL & "   AND evf.seguradora_cod_susep = " & vSeguradoraId
            SQL = SQL & "   AND evf.sucursal_seguradora_id = " & vSucursalId
            SQL = SQL & "   AND evf.ramo_id = " & vRamoId
            SQL = SQL & vbNewLine & " "

            SQL = SQL & "UPDATE temp SET "
            SQL = SQL & "      nome = ISNULL(evf.nome, temp.nome) "
            SQL = SQL & "    , tp_is = ISNULL(evf.tp_is, temp.tp_is) "
            SQL = SQL & "    , qtd_salarios = ISNULL(evf.qtd_salarios, temp.qtd_salarios) "
            SQL = SQL & "    , txt_benef = ISNULL(evf.txt_benef, temp.txt_benef) "
            SQL = SQL & "    , qtd_vida = ISNULL(evf.qtd_vida, temp.qtd_vida) "
            SQL = SQL & "    , tabua_calculo_id = ISNULL(evf.tabua_calculo_id, temp.tabua_calculo_id) "
            SQL = SQL & "    , nome_tabua = ISNULL(tc.nome, temp.nome_tabua) "
            SQL = SQL & "    , fatura_automatica = ISNULL(evf.fatura_automatica, temp.fatura_automatica) "
            SQL = SQL & "    , dia_faturamento = ISNULL(evf.dia_faturamento, temp.dia_faturamento) "
            SQL = SQL & "    , dia_cobranca = ISNULL(evf.dia_cobranca, temp.dia_cobranca) "
            SQL = SQL & "    , periodo_pgto_id = ISNULL(evf.periodo_pgto_id, temp.periodo_pgto_id) "
            SQL = SQL & "    , nome_periodo = ISNULL(pp.nome, temp.nome_periodo) "
            SQL = SQL & "    , forma_pgto_id = ISNULL(evf.forma_pgto_id, temp.forma_pgto_id) "
            SQL = SQL & "    , nome_forma = ISNULL(fp.nome, temp.nome_forma) "
            SQL = SQL & "    , banco_id = ISNULL(evf.banco_id, temp.banco_id) "
            SQL = SQL & "    , agencia_id = ISNULL(evf.agencia_id, temp.agencia_id) "
            SQL = SQL & "    , nome_agencia = ISNULL(ag.nome, temp.nome_agencia) "
            SQL = SQL & "    , conta_corrente_id = ISNULL(evf.conta_corrente_id, temp.conta_corrente_id) "
            SQL = SQL & "    , tp_clausula_conjuge = ISNULL(evf.tp_clausula_conjuge, temp.tp_clausula_conjuge) "
            SQL = SQL & "    , isento_iof = ISNULL(evf.isento_iof, temp.isento_iof) "
            SQL = SQL & "    , cartao_proposta = ISNULL(evf.cartao_proposta, temp.cartao_proposta) "
            SQL = SQL & "  FROM #sub_grupo_apolice_temp_tb temp "
            SQL = SQL & " INNER JOIN ( "
            SQL = SQL & "       SELECT proposta_id "
            SQL = SQL & "            , CAST(MAX(CASE WHEN nome_campo = 'apolice' THEN valor_campo END) AS NUMERIC(9)) apolice_id "
            SQL = SQL & "            , CAST(MAX(CASE WHEN nome_campo = 'seguradora' THEN valor_campo END) AS NUMERIC(5)) seguradora_cod_susep "
            SQL = SQL & "            , CAST(MAX(CASE WHEN nome_campo = 'sucursal' THEN valor_campo END) AS NUMERIC(5)) sucursal_seguradora_id "
            SQL = SQL & "            , CAST(MAX(CASE WHEN nome_campo = 'ramo' THEN valor_campo END) AS INT) ramo_id "
            SQL = SQL & "            , CAST(MAX(CASE WHEN nome_campo = 'subgrupo' THEN valor_campo END) AS INT) sub_grupo_id "
            SQL = SQL & "            , CAST(MAX(CASE WHEN nome_campo = 'nome' THEN valor_campo END) AS VARCHAR(60)) nome "
            SQL = SQL & "            , CAST(MAX(CASE WHEN nome_campo = 'tp_is' THEN valor_campo END) AS CHAR(1)) tp_is "
            SQL = SQL & "            , CAST(MAX(CASE WHEN nome_campo = 'qtd_salarios' THEN valor_campo END) AS INT) qtd_salarios "
            SQL = SQL & "            , CAST(MAX(CASE WHEN nome_campo = 'textobenef' THEN valor_campo END) AS VARCHAR(255)) txt_benef "
            SQL = SQL & "            , CAST(MAX(CASE WHEN nome_campo = 'qtd_vida' THEN valor_campo END) AS INT) qtd_vida "
            SQL = SQL & "            , CAST(MAX(CASE WHEN nome_campo = 'tabua_calculo_id' THEN valor_campo END) AS TINYINT) tabua_calculo_id "
            SQL = SQL & "            , CAST(MAX(CASE WHEN nome_campo = 'fat_auto' THEN valor_campo END) AS CHAR(1)) fatura_automatica "
            SQL = SQL & "            , CAST(MAX(CASE WHEN nome_campo = 'periodo_pgto' THEN valor_campo END) AS TINYINT) periodo_pgto_id "
            SQL = SQL & "            , CAST(MAX(CASE WHEN nome_campo = 'dia_cobranca' THEN valor_campo END) AS TINYINT) dia_cobranca "
            SQL = SQL & "            , CAST(MAX(CASE WHEN nome_campo = 'dia_faturamento' THEN valor_campo END) AS TINYINT) dia_faturamento "
            SQL = SQL & "            , CAST(MAX(CASE WHEN nome_campo = 'forma_pgto' THEN valor_campo END) AS TINYINT) forma_pgto_id "
            SQL = SQL & "            , CAST(MAX(CASE WHEN nome_campo = 'banco_id' THEN valor_campo END) AS NUMERIC(5)) banco_id "
            SQL = SQL & "            , CAST(MAX(CASE WHEN nome_campo = 'agencia_id' THEN valor_campo END) AS NUMERIC(5)) agencia_id "
            SQL = SQL & "            , CAST(MAX(CASE WHEN nome_campo = 'conta_corrente_id' THEN valor_campo END) AS NUMERIC(9)) conta_corrente_id "
            SQL = SQL & "            , CAST(MAX(CASE WHEN nome_campo = 'clausula_conjuge' THEN valor_campo END) AS CHAR(1)) tp_clausula_conjuge "
            SQL = SQL & "            , CAST(MAX(CASE WHEN nome_campo = 'isento_iof' THEN valor_campo END) AS CHAR(1)) isento_iof "
            SQL = SQL & "            , CAST(MAX(CASE WHEN nome_campo = 'cartao_prop' THEN valor_campo END) AS CHAR(1)) cartao_proposta "
            SQL = SQL & "         FROM endosso_vigencia_futura_tb WITH(NOLOCK) "
            SQL = SQL & "        WHERE dt_aplicacao IS NULL "
            SQL = SQL & "          AND proposta_id = " & vPropostaId
            SQL = SQL & "          AND endosso_id = " & gEndossoId
            SQL = SQL & "          AND nome_procedure = 'sub_grupo_apolice_spu' "
            SQL = SQL & "        GROUP BY proposta_id, execucao_id ) evf "
            SQL = SQL & "   ON temp.sub_grupo_id = evf.sub_grupo_id "
            SQL = SQL & "  LEFT JOIN tabua_calculo_tb tc  WITH (NOLOCK)  "
            SQL = SQL & "    ON tc.tabua_calculo_id = evf.tabua_calculo_id "
            SQL = SQL & "  LEFT JOIN agencia_tb ag  WITH (NOLOCK)  "
            SQL = SQL & "    ON ag.banco_id = evf.banco_id "
            SQL = SQL & "   AND ag.agencia_id = evf.agencia_id "
            SQL = SQL & "  LEFT JOIN periodo_pgto_tb pp  WITH (NOLOCK)  "
            SQL = SQL & "    ON pp.periodo_pgto_id = evf.periodo_pgto_id "
            SQL = SQL & "  LEFT JOIN forma_pgto_tb fp  WITH (NOLOCK)  "
            SQL = SQL & "    ON fp.forma_pgto_id = evf.forma_pgto_id "
            SQL = SQL & " WHERE evf.apolice_id = " & vApoliceId
            SQL = SQL & "   AND evf.seguradora_cod_susep = " & vSeguradoraId
            SQL = SQL & "   AND evf.sucursal_seguradora_id = " & vSucursalId
            SQL = SQL & "   AND evf.ramo_id = " & vRamoId
            SQL = SQL & vbNewLine & " "

            SQL = SQL & "DECLARE @subgrupo INT "
            SQL = SQL & "SELECT @subgrupo = sub_grupo_id "
            SQL = SQL & "  FROM chave_sub_grupo_apolice_tb WITH(NOLOCK)"
            SQL = SQL & " WHERE apolice_id = " & vApoliceId
            SQL = SQL & "   AND seguradora_cod_susep = " & vSeguradoraId
            SQL = SQL & "   AND sucursal_seguradora_id = " & vSucursalId
            SQL = SQL & "   AND ramo_id = " & vRamoId
            SQL = SQL & vbNewLine & " "

            SQL = SQL & "INSERT INTO #sub_grupo_apolice_temp_tb ( "
            SQL = SQL & "      sub_grupo_id "
            SQL = SQL & "    , nome "
            SQL = SQL & "    , tp_is "
            SQL = SQL & "    , qtd_salarios "
            SQL = SQL & "    , txt_benef "
            SQL = SQL & "    , qtd_vida "
            SQL = SQL & "    , tabua_calculo_id "
            SQL = SQL & "    , nome_tabua "
            SQL = SQL & "    , fatura_automatica "
            SQL = SQL & "    , dia_faturamento "
            SQL = SQL & "    , dia_cobranca "
            SQL = SQL & "    , periodo_pgto_id "
            SQL = SQL & "    , nome_periodo "
            SQL = SQL & "    , forma_pgto_id "
            SQL = SQL & "    , nome_forma "
            SQL = SQL & "    , banco_id "
            SQL = SQL & "    , agencia_id "
            SQL = SQL & "    , nome_agencia "
            SQL = SQL & "    , conta_corrente_id "
            SQL = SQL & "    , tp_clausula_conjuge "
            SQL = SQL & "    , isento_iof "
            SQL = SQL & "    , lim_min_inc "
            SQL = SQL & "    , lim_max_inc "
            SQL = SQL & "    , lim_min_mov "
            SQL = SQL & "    , lim_max_mov "
            SQL = SQL & "    , cartao_proposta "
            SQL = SQL & "    , critica_todos "
            SQL = SQL & "    , critica_idade "
            SQL = SQL & "    , critica_capital "
            SQL = SQL & "    , lim_max_idade "
            SQL = SQL & "    , lim_max_capital "
            SQL = SQL & "    , dt_inicio_vigencia_sbg "
            SQL = SQL & "    , dt_fim_vigencia_sbg ) "
            SQL = SQL & "SELECT (ROW_NUMBER() OVER (ORDER BY execucao_id) + @subgrupo) sub_grupo_id "
            SQL = SQL & "    , evf.nome "
            SQL = SQL & "    , evf.tp_is "
            SQL = SQL & "    , ISNULL(evf.qtd_salarios, 0) qtd_salarios "
            SQL = SQL & "    , ISNULL(evf.txt_benef,'') txt_benef "
            SQL = SQL & "    , ISNULL(evf.qtd_vida, 0) qtd_vida "
            SQL = SQL & "    , ISNULL(evf.tabua_calculo_id, 0) tabua_calculo_id "
            SQL = SQL & "    , ISNULL(tc.nome, '') nome_tabua "
            SQL = SQL & "    , ISNULL(evf.fatura_automatica, 'n') fatura_automatica "
            SQL = SQL & "    , ISNULL(evf.dia_faturamento, 0) dia_faturamento "
            SQL = SQL & "    , ISNULL(evf.dia_cobranca, 0) dia_cobranca "
            SQL = SQL & "    , ISNULL(evf.periodo_pgto_id, 99) periodo_pgto_id "
            SQL = SQL & "    , ISNULL(pp.nome, 'INDEFINIDO') nome_periodo "
            SQL = SQL & "    , ISNULL(evf.forma_pgto_id, 99) forma_pgto_id "
            SQL = SQL & "    , ISNULL(fp.nome, 'INDEFINIDO') nome_forma "
            SQL = SQL & "    , ISNULL(evf.banco_id, 0) banco_id "
            SQL = SQL & "    , ISNULL(evf.agencia_id, 0) agencia_id "
            SQL = SQL & "    , ISNULL(ag.nome, '') nome_agencia "
            SQL = SQL & "    , ISNULL(evf.conta_corrente_id, 0) conta_corrente_id "
            SQL = SQL & "    , ISNULL(evf.tp_clausula_conjuge, 'n') tp_clausula_conjuge "
            SQL = SQL & "    , ISNULL(evf.isento_iof, 'n') isento_iof "
            SQL = SQL & "    , 0, 0, 0, 0 "
            SQL = SQL & "    , ISNULL(evf.cartao_proposta, '') cartao_proposta "
            SQL = SQL & "    , '', '', '', 0, 0, '', '' "
            SQL = SQL & "  FROM (SELECT proposta_id "
            SQL = SQL & "            , execucao_id "
            SQL = SQL & "            , CAST(MAX(CASE WHEN nome_campo = 'apolice_id' THEN valor_campo END) AS NUMERIC(9)) apolice_id "
            SQL = SQL & "            , CAST(MAX(CASE WHEN nome_campo = 'seguradora_cod_susep' THEN valor_campo END) AS NUMERIC(5)) seguradora_cod_susep "
            SQL = SQL & "            , CAST(MAX(CASE WHEN nome_campo = 'sucursal_seguradora_id' THEN valor_campo END) AS NUMERIC(5)) sucursal_seguradora_id "
            SQL = SQL & "            , CAST(MAX(CASE WHEN nome_campo = 'ramo_id' THEN valor_campo END) AS INT) ramo_id "
            SQL = SQL & "            , CAST(MAX(CASE WHEN nome_campo = 'nome' THEN valor_campo END) AS VARCHAR(60)) nome "
            SQL = SQL & "            , CAST(MAX(CASE WHEN nome_campo = 'tp_is' THEN valor_campo END) AS CHAR(1)) tp_is "
            SQL = SQL & "            , CAST(MAX(CASE WHEN nome_campo = 'qtd_salarios' THEN valor_campo END) AS INT) qtd_salarios "
            SQL = SQL & "            , CAST(MAX(CASE WHEN nome_campo = 'texto_beneficiario' THEN valor_campo END) AS VARCHAR(255)) txt_benef "
            SQL = SQL & "            , CAST(MAX(CASE WHEN nome_campo = 'qtd_vida' THEN valor_campo END) AS INT) qtd_vida "
            SQL = SQL & "            , CAST(MAX(CASE WHEN nome_campo = 'tabua_calculo_id' THEN valor_campo END) AS TINYINT) tabua_calculo_id "
            SQL = SQL & "            , CAST(MAX(CASE WHEN nome_campo = 'fat_auto' THEN valor_campo END) AS CHAR(1)) fatura_automatica "
            SQL = SQL & "            , CAST(MAX(CASE WHEN nome_campo = 'periodoPgto' THEN valor_campo END) AS TINYINT) periodo_pgto_id "
            SQL = SQL & "            , CAST(MAX(CASE WHEN nome_campo = 'diaCobranca' THEN valor_campo END) AS TINYINT) dia_cobranca "
            SQL = SQL & "            , CAST(MAX(CASE WHEN nome_campo = 'diaFaturamento' THEN valor_campo END) AS TINYINT) dia_faturamento "
            SQL = SQL & "            , CAST(MAX(CASE WHEN nome_campo = 'formapgto' THEN valor_campo END) AS TINYINT) forma_pgto_id "
            SQL = SQL & "            , CAST(MAX(CASE WHEN nome_campo = 'bancodeb' THEN valor_campo END) AS NUMERIC(5)) banco_id "
            SQL = SQL & "            , CAST(MAX(CASE WHEN nome_campo = 'agdeb' THEN valor_campo END) AS NUMERIC(5)) agencia_id "
            SQL = SQL & "            , CAST(MAX(CASE WHEN nome_campo = 'contadeb' THEN valor_campo END) AS NUMERIC(9)) conta_corrente_id "
            SQL = SQL & "            , CAST(MAX(CASE WHEN nome_campo = 'tp_conjuge' THEN valor_campo END) AS CHAR(1)) tp_clausula_conjuge "
            SQL = SQL & "            , CAST(MAX(CASE WHEN nome_campo = 'isencao_iof' THEN valor_campo END) AS CHAR(1)) isento_iof "
            SQL = SQL & "            , CAST(MAX(CASE WHEN nome_campo = 'cartao_prop' THEN valor_campo END) AS CHAR(1)) cartao_proposta "
            SQL = SQL & "         FROM endosso_vigencia_futura_tb WITH(NOLOCK)"
            SQL = SQL & "        WHERE dt_aplicacao IS NULL "
            SQL = SQL & "          AND proposta_id = " & vPropostaId
            SQL = SQL & "          AND endosso_id = " & gEndossoId
            SQL = SQL & "          AND nome_procedure = 'sub_grupo_apolice_spi' "
            SQL = SQL & "        GROUP BY proposta_id, execucao_id ) evf "
            SQL = SQL & "  LEFT JOIN tabua_calculo_tb tc  WITH (NOLOCK)  "
            SQL = SQL & "    ON tc.tabua_calculo_id = evf.tabua_calculo_id "
            SQL = SQL & "  LEFT JOIN agencia_tb ag  WITH (NOLOCK)  "
            SQL = SQL & "    ON ag.banco_id = evf.banco_id "
            SQL = SQL & "   AND ag.agencia_id = evf.agencia_id "
            SQL = SQL & "  LEFT JOIN periodo_pgto_tb pp  WITH (NOLOCK)  "
            SQL = SQL & "    ON pp.periodo_pgto_id = evf.periodo_pgto_id "
            SQL = SQL & "  LEFT JOIN forma_pgto_tb fp  WITH (NOLOCK)  "
            SQL = SQL & "    ON fp.forma_pgto_id = evf.forma_pgto_id "
            SQL = SQL & vbNewLine & " "

            SQL = SQL & "UPDATE temp SET "
            SQL = SQL & "      lim_min_inc = ISNULL(evf.lim_min_inc, 0) "
            SQL = SQL & "    , lim_max_inc = ISNULL(evf.lim_max_inc, 0) "
            SQL = SQL & "    , lim_min_mov = ISNULL(evf.lim_min_mov, 0) "
            SQL = SQL & "    , lim_max_mov = ISNULL(evf.lim_max_mov, 0) "
            SQL = SQL & "  FROM #sub_grupo_apolice_temp_tb temp "
            SQL = SQL & " INNER JOIN ( "
            SQL = SQL & "       SELECT proposta_id "
            SQL = SQL & "            , CAST(MAX(CASE WHEN nome_campo = 'apolice' THEN valor_campo END) AS NUMERIC(9)) apolice_id "
            SQL = SQL & "            , CAST(MAX(CASE WHEN nome_campo = 'seguradora' THEN valor_campo END) AS NUMERIC(5)) seguradora_cod_susep "
            SQL = SQL & "            , CAST(MAX(CASE WHEN nome_campo = 'sucursal' THEN valor_campo END) AS NUMERIC(5)) sucursal_seguradora_id "
            SQL = SQL & "            , CAST(MAX(CASE WHEN nome_campo = 'ramo' THEN valor_campo END) AS INT) ramo_id "
            SQL = SQL & "            , CAST(MAX(CASE WHEN nome_campo = 'subgrupo' THEN valor_campo END) AS INT) sub_grupo_id "
            SQL = SQL & "            , CAST(MAX(CASE WHEN nome_campo = 'lim_min_inc' THEN valor_campo END) AS TINYINT) lim_min_inc "
            SQL = SQL & "            , CAST(MAX(CASE WHEN nome_campo = 'lim_max_inc' THEN valor_campo END) AS TINYINT) lim_max_inc "
            SQL = SQL & "            , CAST(MAX(CASE WHEN nome_campo = 'lim_min_mov' THEN valor_campo END) AS TINYINT) lim_min_mov "
            SQL = SQL & "            , CAST(MAX(CASE WHEN nome_campo = 'lim_max_mov' THEN valor_campo END) AS TINYINT) lim_max_mov "
            SQL = SQL & "         FROM endosso_vigencia_futura_tb WITH(NOLOCK)"
            SQL = SQL & "        WHERE dt_aplicacao IS NULL "
            SQL = SQL & "          AND proposta_id = " & vPropostaId
            SQL = SQL & "          AND endosso_id = " & gEndossoId
            SQL = SQL & "          AND (nome_procedure = 'limite_idade_sub_grupo_spu' OR nome_procedure LIKE 'limite_idade_sub_grupo_spi') "
            SQL = SQL & "        GROUP BY proposta_id, execucao_id ) evf "
            SQL = SQL & "    ON temp.sub_grupo_id = evf.sub_grupo_id "
            SQL = SQL & "   AND evf.apolice_id = " & vApoliceId
            SQL = SQL & "   AND evf.seguradora_cod_susep = " & vSeguradoraId
            SQL = SQL & "   AND evf.sucursal_seguradora_id = " & vSucursalId
            SQL = SQL & "   AND evf.ramo_id = " & vRamoId
            SQL = SQL & vbNewLine & " "

            SQL = SQL & "UPDATE temp SET "
            SQL = SQL & "      critica_todos = ISNULL(evf.critica_todos, '') "
            SQL = SQL & "    , critica_idade = ISNULL(evf.critica_idade, '') "
            SQL = SQL & "    , critica_capital = ISNULL(evf.critica_capital, '') "
            SQL = SQL & "    , lim_max_idade = ISNULL(evf.lim_max_idade, 0) "
            SQL = SQL & "    , lim_max_capital = ISNULL(evf.lim_max_capital, 0) "
            SQL = SQL & "  FROM #sub_grupo_apolice_temp_tb temp "
            SQL = SQL & " INNER JOIN ( "
            SQL = SQL & "       SELECT proposta_id "
            SQL = SQL & "            , CAST(MAX(CASE WHEN nome_campo = 'apolice' THEN valor_campo END) AS NUMERIC(9)) apolice_id "
            SQL = SQL & "            , CAST(MAX(CASE WHEN nome_campo = 'seguradora' THEN valor_campo END) AS NUMERIC(5)) seguradora_cod_susep "
            SQL = SQL & "            , CAST(MAX(CASE WHEN nome_campo = 'sucursal' THEN valor_campo END) AS NUMERIC(5)) sucursal_seguradora_id "
            SQL = SQL & "            , CAST(MAX(CASE WHEN nome_campo = 'ramo' THEN valor_campo END) AS INT) ramo_id "
            SQL = SQL & "            , CAST(MAX(CASE WHEN nome_campo = 'subgrupo' THEN valor_campo END) AS INT) sub_grupo_id "
            SQL = SQL & "            , CAST(MAX(CASE WHEN nome_campo = 'todos' THEN valor_campo END) AS CHAR(1)) critica_todos "
            SQL = SQL & "            , CAST(MAX(CASE WHEN nome_campo = 'idade' THEN valor_campo END) AS CHAR(1)) critica_idade "
            SQL = SQL & "            , CAST(MAX(CASE WHEN nome_campo = 'capital' THEN valor_campo END) AS CHAR(1)) critica_capital "
            SQL = SQL & "            , CAST(MAX(CASE WHEN nome_campo = 'lim_max_idade' THEN valor_campo END) AS TINYINT) lim_max_idade "
            SQL = SQL & "            , CAST(MAX(CASE WHEN nome_campo = 'lim_min_capital' THEN valor_campo END) AS NUMERIC(9,2)) lim_max_capital "
            SQL = SQL & "         FROM endosso_vigencia_futura_tb WITH(NOLOCK)"
            SQL = SQL & "        WHERE dt_aplicacao IS NULL "
            SQL = SQL & "          AND proposta_id = " & vPropostaId
            SQL = SQL & "          AND endosso_id = " & gEndossoId
            SQL = SQL & "          AND (nome_procedure = 'sub_grupo_criterio_cartao_proposta_spu' OR nome_procedure LIKE 'sub_grupo_criterio_cartao_proposta_spi') "
            SQL = SQL & "        GROUP BY proposta_id, execucao_id ) evf "
            SQL = SQL & "    ON evf.sub_grupo_id = temp.sub_grupo_id "
            SQL = SQL & "   AND evf.apolice_id = " & vApoliceId
            SQL = SQL & "   AND evf.seguradora_cod_susep = " & vSeguradoraId
            SQL = SQL & "   AND evf.sucursal_seguradora_id = " & vSucursalId
            SQL = SQL & "   AND evf.ramo_id = " & vRamoId
            SQL = SQL & vbNewLine & " "

        End If

        SQL = SQL & " SELECT * FROM #sub_grupo_apolice_temp_tb ORDER BY sub_grupo_id "
        SQL = SQL & " DROP TABLE #sub_grupo_apolice_temp_tb "
        ' Autor: rodrigo.almeida (Nova Consultoria) - Data Altera��o: 17/08/2011 - Demanda: 864186 - FIM


        Set rc = rdocn.OpenResultset(SQL)
        If Not rc.EOF Then
            ' Deixa de exibir a coluna tabua_id.
            GridSubGrupos.ColWidth(5) = 0
            ' Tab Corretagem
            cboSubGrupo(0).Clear
            cboSubGrupo(0).Enabled = True
            fraCorretor.Refresh
            ' Tab Administra��o
            cboSubGrupo(1).Clear
            cboSubGrupo(1).Enabled = True
            fraAdministracao.Refresh
            ' Tab Componentes
            cboSubGrupo(2).Clear
            cboSubGrupo(2).Enabled = True
            fraComponentes.Refresh
            ' Tab Coberturas
            cboSubGrupo(3).Clear
            cboSubGrupo(3).Enabled = True
            FraCoberturas.Refresh
            '
            Set SubGrupos_endosso = Nothing

            While Not rc.EOF

                Set NovoSubGrupoEnd = New SubGrupo
                '
                With NovoSubGrupoEnd
                    Set .Corretores = New Collection
                    Set .Coberturas = New Collection
                    Set .Componentes = New Collection
                    Set .Estipulantes = New Collection
                    Set .Faturas = New Collection
                    Set .Assistencias = New Collection
                    .Id = Format(rc!sub_grupo_id, "000")
                    .Nome = Trim(rc!Nome)
                    .TipoIS = UCase(Trim(rc!tp_is))
                    .QtdeSal = rc!qtd_salarios
                    .TextoBenef = Trim(rc!txt_benef)
                    .QtdVidas = rc!qtd_vida
                    .IdTabua = rc!tabua_calculo_id
                    .NomeTabua = Trim(rc!nome_tabua)
                    .FaturaAuto = (UCase(rc!fatura_automatica) = "S")
                    .DiaFaturamento = Val(rc!dia_faturamento)
                    .DiaCobranca = Val(rc!dia_cobranca)
                    .IndPerPgto = rc!periodo_pgto_id
                    .PeriodoPgto = Trim(rc!nome_periodo)
                    .IndFormaPgto = rc!forma_pgto_id
                    .FormaPgto = Trim(rc!nome_forma)
                    .IdBanco = rc!banco_id
                    .IdAgencia = rc!agencia_id
                    .NomeAgencia = Trim(UCase(rc!nome_agencia))
                    .CCorrente = rc!conta_corrente_id
                    .TpConjuge = UCase(rc!tp_clausula_conjuge)
                    .IsentoIOF = (UCase(rc!isento_iof) = "S")
                    .LimMinIdadeImpl = Val(rc!lim_min_inc)
                    .LimMaxIdadeImpl = Val(rc!lim_max_inc)
                    .LimMinIdadeMov = Val(rc!lim_min_mov)
                    .LimMaxIdadeMov = Val(rc!lim_max_mov)
                    .cartao_proposta = Trim(rc!cartao_proposta)
                    .critica_todos = rc!critica_todos
                    .critica_idade = rc!critica_idade
                    .critica_capital = rc!critica_capital
                    .lim_max_idade = rc!lim_max_idade
                    .lim_max_capital = Val(rc!lim_max_capital)
                    .DtInicioVigenciaSBG = rc!dt_inicio_vigencia_sbg
                    .DtFimVigenciaSBG = rc!dt_fim_vigencia_sbg

                End With
                'Adiciona o SubGrupo � cole��o
                SubGrupos_endosso.Add NovoSubGrupoEnd, CStr(rc!sub_grupo_id)
                'Adiciona o SubGrupo aos Combos
                auxsub = Format(rc!sub_grupo_id, "000") & " - " & Trim(rc!Nome)
                '
                For i = 0 To 3
                    cboSubGrupo(i).AddItem auxsub
                    cboSubGrupo(i).ItemData(cboSubGrupo(i).NewIndex) = rc!sub_grupo_id
                Next
                '
                rc.MoveNext
            Wend

        End If

        GridSubGrupos.Rows = 1

        For Each vSubGrupo In SubGrupos_endosso
            Select Case vSubGrupo.TipoIS
            Case "C"
                TipoIS = "Capital Informado"
            Case "F"
                TipoIS = "Capital Fixo"
            Case "S"
                TipoIS = "M�ltiplo Salarial"
            Case "G"
                TipoIS = "Capital Global"
            Case "P"
                TipoIS = "Capital por Planos"
            Case "E"
                TipoIS = "Capital por Faixa Et�ria"
            Case Else
                TipoIS = "N�o Informado"
            End Select

            Select Case vSubGrupo.TpConjuge
            Case "N"
                Conjuge = "Inexistente"
            Case "A"
                Conjuge = "Automatico"
            Case "F"
                Conjuge = "Facultativo"
            Case Else
                Conjuge = "N�o Informado"
            End Select

            linha = vSubGrupo.Id
            linha = linha & vbTab & vSubGrupo.Nome
            linha = linha & vbTab & TipoIS
            linha = linha & vbTab & vSubGrupo.QtdeSal
            linha = linha & vbTab & vSubGrupo.QtdVidas
            linha = linha & vbTab & vSubGrupo.IdTabua
            linha = linha & vbTab & vSubGrupo.NomeTabua
            linha = linha & vbTab & IIf(vSubGrupo.FaturaAuto, "S", "N")
            linha = linha & vbTab & vSubGrupo.TextoBenef
            linha = linha & vbTab & vSubGrupo.DiaFaturamento
            linha = linha & vbTab & vSubGrupo.DiaCobranca
            linha = linha & vbTab & Format(vSubGrupo.IndPerPgto, "00") & " - " & vSubGrupo.PeriodoPgto
            linha = linha & vbTab & Format(vSubGrupo.IndFormaPgto, "00") & " - " & vSubGrupo.FormaPgto
            If vSubGrupo.IndFormaPgto = 1 Then
                linha = linha & vbTab & Format(vSubGrupo.IdAgencia, "0000") & " - " & vSubGrupo.NomeAgencia
                linha = linha & vbTab & Format(vSubGrupo.CCorrente, "##########0")
            Else
                linha = linha & vbTab & ""
                linha = linha & vbTab & ""
            End If
            linha = linha & vbTab & Conjuge
            linha = linha & vbTab & IIf(vSubGrupo.IsentoIOF, "S", "N")
            linha = linha & vbTab & vSubGrupo.LimMinIdadeImpl
            linha = linha & vbTab & vSubGrupo.LimMaxIdadeImpl
            linha = linha & vbTab & vSubGrupo.LimMinIdadeMov
            linha = linha & vbTab & vSubGrupo.LimMaxIdadeMov
            linha = linha & vbTab & vSubGrupo.cartao_proposta
            linha = linha & vbTab & vSubGrupo.critica_todos
            linha = linha & vbTab & vSubGrupo.critica_idade
            linha = linha & vbTab & vSubGrupo.critica_capital
            linha = linha & vbTab & vSubGrupo.lim_max_idade
            linha = linha & vbTab & vSubGrupo.lim_max_capital
            linha = linha & vbTab & vSubGrupo.DtInicioVigenciaSBG
            linha = linha & vbTab & vSubGrupo.DtFimVigenciaSBG
            '
            GridSubGrupos.AddItem linha
            GridSubGrupos.RowData(GridSubGrupos.Rows - 1) = vSubGrupo.Id
            '
        Next

    Else
        tabEndossos.TabEnabled(TABendosso_SUBGRUPO) = False

        ' Tab Corretagem
        cboSubGrupo(0).Clear
        cboSubGrupo(0).Enabled = False
        fraCorretor.Refresh
        ' Tab Administra��o
        cboSubGrupo(1).Clear
        cboSubGrupo(1).Enabled = False
        fraAdministracao.Refresh
        ' Tab Componentes
        cboSubGrupo(2).Clear
        cboSubGrupo(2).Enabled = False
        fraComponentes.Refresh
        ' Tab Coberturas
        cboSubGrupo(2).Clear
        cboSubGrupo(2).Enabled = False
        FraCoberturas.Refresh
        ' Tab Administra��o
        cboSubGrupo(3).Clear
        cboSubGrupo(3).Enabled = False
        fraAdministracao.Refresh

    End If
    Exit Sub

Erro_SubGrupo_endosso:
    TrataErroGeral "Prepara_SubGrupos", Me.name
    MensagemBatch "Erro na rotina Prepara_SubGrupos.", , , False
End Sub

Private Sub CboCongeneresCed_Click()

    Dim SQL As String
    Dim NomeSeguradora As String
    Dim CodSeguradora As String
    Dim CodSeguradoraClick As String
    Dim DtIniPart As String
    Dim DtFimPart As String
    Dim NumOrdem As String
    Dim PercDespLider As Variant
    Dim PercParticip As Variant
    Dim vCongenereCed As Congenere
    Dim rec As rdoResultset
    Dim ValPremioTarifaCed As Variant
    Dim ValDescontoCed As Variant
    Dim ValAdicFracCed As Variant
    Dim ValPremioRepCed As Variant
    Dim ValComCed As Variant
    Dim ValComEstipCed As Variant
    Dim ValDespLiderCed As Variant


    CodSeguradoraClick = CboCongeneresCed.ItemData(CboCongeneresCed.ListIndex)

    '  Cosseguro cedido ...
    '
    '  Recupera percentuais da congenere assinalada.
    '
    For Each vCongenereCed In Congeneres_endosso
        If vCongenereCed.Seguradora = CodSeguradoraClick Then
            PercDespLider = vCongenereCed.Perc_Despesa_Lider / 100
            PercParticip = vCongenereCed.Perc_Participacao / 100
            Exit For
        End If
    Next

    '   Prepara campos do cosseguro cedido - endosso financeiro.
    ValPremioTarifaCed = Format(FormatValor(ValPremioTarifaNum) * PercParticip, "###,###,##0.00")
    ValDescontoCed = Format(FormatValor(ValDescNum) * PercParticip, "###,###,##0.00")
    ValAdicFracCed = Format(FormatValor(ValAdicFracNum) * PercParticip, "###,###,##0.00")
    ValPremioRepCed = Format(ValPremioTarifaCed - ValDescontoCed + ValAdicFracCed, "###,###,##0.00")
    ValComCed = Format(FormatValor(ValComissaoNum) * PercParticip, "###,###,##0.00")
    ValComEstipCed = Format(FormatValor(ValComissaoEstipNum) * PercParticip, "###,###,##0.00")
    ValDespLiderCed = Format(FormatValor(ValPremioRepCed) * PercDespLider, "###,###,##0.00")
    '
    txtValPremioTarifaCed.Text = ValPremioTarifaCed
    txtValDescontoCed = ValDescontoCed
    txtValAdicFracCed = ValAdicFracCed
    txtValPremioRepCed = ValPremioRepCed
    txtValComCed = ValComCed
    txtValComEstipCed = ValComEstipCed
    txtValDespLiderCed = ValDespLiderCed

    Frame1.Refresh

End Sub

Private Sub CboSubGrupo_Click(Index As Integer)
    Dim vEstip As estipulante
    Dim ind_sub As String
    '
    If cboSubGrupo(Index).ListIndex <> -1 Then
        ind_sub = CStr(cboSubGrupo(Index).ItemData(cboSubGrupo(Index).ListIndex))
        '
        Select Case Index
        Case 0  'Corretagem
            Set Corretores_endosso = Nothing
            Set Corretores_endosso = SubGrupos_endosso(ind_sub).Corretores
            Call Monta_GridCorretores_endosso

        Case 1  'Administra��o
            Set Estipulantes_endosso = Nothing
            Set Estipulantes_endosso = SubGrupos_endosso(ind_sub).Estipulantes
            For Each vEstip In Estipulantes_endosso
                TxtRepAnt.Text = vEstip.Id
                TxtNomeRepAnt.Text = vEstip.Nome
            Next
            If Not HaEndFinanceiro Then
                Call Monta_GridAdm_endosso
            End If
            '
        Case 2  'Componentes
            Set Componentes_endosso = Nothing
            Set Componentes_endosso = SubGrupos_endosso(ind_sub).Componentes
            Call Monta_GridComponentes_endosso
            '
        Case 3  'Coberturas
            Set Coberturas_endosso = Nothing
            Set Componentes_endosso = Nothing
            CboTpComponente.Clear
            GridCoberturas.Rows = 1
            Set Componentes_endosso = SubGrupos_endosso(ind_sub).Componentes
            Set Coberturas_endosso = SubGrupos_endosso(ind_sub).Coberturas
            Monta_cboTpComponente_endosso

        Case Else
            MsgBox "SubGrupo n�o existente!", vbCritical
        End Select
    End If
    '

End Sub

Private Sub CboTpComponente_Click()

    If CboTpComponente.ListIndex <> -1 Then
        Call Monta_GridCoberturas_endosso(CboTpComponente.ItemData(CboTpComponente.ListIndex))
    Else
        GridCoberturas.Rows = 1
    End If
    '
End Sub

Sub Carrega_cmbCongenere(proposta_id As Long, seguradora_cod_susep As Integer, endosso_id As Long, nossa_parte As Boolean)
    Dim rs As rdoResultset
    Dim SQL As String
    Dim Perc_partic As Currency
    Dim Perc_desp_lider As Currency

    SQL = ""
    SQL = SQL & " Exec segs7067_sps "
    SQL = SQL & " @proposta_id = " & proposta_id & ","
    SQL = SQL & " @endosso_id = " & endosso_id & ","
    SQL = SQL & " @seguradora_cod_susep = " & seguradora_cod_susep & ","
    SQL = SQL & " @nossa_parte = '" & IIf(nossa_parte = True, "S", "N") & "'"
    Set rs = rdocn.OpenResultset(SQL)

    gridCobrancaCosseguro.Rows = 1
    While Not rs.EOF

        If nossa_parte = True Then
            Perc_partic = 100 - CCur("0" & Replace(rs("perc_participacao"), ".", ","))
            Perc_desp_lider = "0,00"
        Else
            Perc_partic = CCur("0" & Replace(rs("perc_participacao"), ".", ","))
            Perc_desp_lider = CCur("0" & Replace(rs("perc_despesa_lider"), ".", ","))
        End If

        gridCobrancaCosseguro.AddItem rs("num_endosso") & vbTab & _
                                      rs("num_cobranca") & vbTab & _
                                      rs("nosso_numero") & vbTab & _
                                      Format(rs("dt_agendamento"), "dd/mm/yyyy") & vbTab & _
                                      Format(Replace(rs("val_premio_tarifa"), ".", ","), "###,###,###,##0.00") & vbTab & _
                                      Format(Replace(rs("val_desconto"), ".", ","), "###,###,###,##0.00") & vbTab & _
                                      Format(Replace(rs("val_adic_fracionamento"), ".", ","), "###,###,###,##0.00") & vbTab & _
                                      Format(Replace(rs("val_comissao"), ".", ","), "###,###,###,##0.00") & vbTab & _
                                      Format(Replace(rs("val_premio_total_liquido"), ".", ","), "###,###,###,##0.00") & vbTab & _
                                      Format(Replace(rs("val_despesa_lideranca"), ".", ","), "###,###,###,##0.00") & vbTab & _
                                      Format(rs("dt_recebimento"), "dd/mm/yyyy") & vbTab & _
                                      Format(Perc_partic, "###,###,###,##0.00") & vbTab & _
                                      Format(Perc_desp_lider, "###,###,###,##0.00")
        rs.MoveNext
    Wend
    rs.Close

    SQL = ""
    If CLng(seguradora_cod_susep) <> 0 Then
        SQL = SQL & "select num_ordem, perc_participacao, perc_despesa_lider" & vbNewLine
        SQL = SQL & "from   co_seguro_repassado_tb WITH(NOLOCK)" & vbNewLine
        SQL = SQL & "where  apolice_id = " & CLng(txtApolice.Text) & vbNewLine
        SQL = SQL & "and    ramo_id = " & RamoId & vbNewLine
        SQL = SQL & "and    rep_seguradora_cod_susep = " & seguradora_cod_susep & vbNewLine
        SQL = SQL & "and    dt_fim_participacao is null" & vbNewLine
        Set rs = rdocn.OpenResultset(SQL)
        If Not rs.EOF Then
            txtNumOrdem.Text = rs("num_ordem")
            txtPercParticipacao.Text = Format(CCur("0" & Replace((rs("perc_participacao")), ".", ",")), "###,###,##0.00")
            txtPercDespesa.Text = Format(CCur("0" & Replace((rs("perc_despesa_lider")), ".", ",")), "###,###,##0.00")
        End If
        rs.Close
    Else
        txtNumOrdem.Text = ""

        'verificar se foi selecionado nossa parte
        SQL = SQL & "select perc_participacao = sum(perc_participacao), perc_despesa_lider = sum(perc_despesa_lider)" & vbNewLine
        SQL = SQL & "from   co_seguro_repassado_tb WITH(NOLOCK)" & vbNewLine
        SQL = SQL & "where  apolice_id = " & CLng(txtApolice.Text) & vbNewLine
        SQL = SQL & "and    ramo_id = " & RamoId & vbNewLine
        SQL = SQL & "and    dt_fim_participacao is null" & vbNewLine
        Set rs = rdocn.OpenResultset(SQL)

        If cmbCongenere.Text = "Nossa Parte" Then
            If Not rs.EOF Then
                txtPercParticipacao.Text = Format(100 - CCur("0" & Replace((rs("perc_participacao")), ".", ",")), "###,###,##0.00")
                txtPercDespesa.Text = "0,00"
            End If
        Else
            If Not rs.EOF Then
                txtPercParticipacao.Text = Format(CCur("0" & Replace((rs("perc_participacao")), ".", ",")), "###,###,##0.00")
                txtPercDespesa.Text = Format(CCur("0" & Replace((rs("perc_despesa_lider")), ".", ",")), "###,###,##0.00")
            End If
        End If

        rs.Close
    End If

End Sub
Private Sub cmbCongenere_Change()
    Dim sCodSusep As String

    If cmbCongenere.ListIndex <> -1 Then
        vCongenereId = cmbCongenere.ListIndex
        'Obt�m n� de ordem se uma cosseguradora for selecionada
        If cmbCongenere.ListIndex > 1 And Congeneres.Count <> 0 Then
            sCodSusep = cmbCongenere.ItemData(cmbCongenere.ListIndex)
            txtNumOrdem = Format(Congeneres(sCodSusep).NumOrdem, "000000000")
        Else
            txtNumOrdem = ""
        End If

    End If

    If cmbCongenere.Text = "Nossa Parte" And cmbCongenere.ItemData(cmbCongenere.ListIndex) = 0 Then
        Call Carrega_cmbCongenere(CLng(vPropostaId), 0, gEndossoId, True)
    ElseIf cmbCongenere.Text = "Total do Cosseguro Cedido" And cmbCongenere.ItemData(cmbCongenere.ListIndex) = 0 Then
        Call Carrega_cmbCongenere(CLng(vPropostaId), 0, gEndossoId, False)
    Else
        Call Carrega_cmbCongenere(CLng(vPropostaId), cmbCongenere.ItemData(cmbCongenere.ListIndex), gEndossoId, False)
    End If

End Sub

Private Sub cmdImprime_Click(Index As Integer)

    Call Imprime_Endosso

End Sub

Private Sub cmdRelatorioVidas_Click()

    If msfVidas.Rows > 1 Then
        '  Recupera dados da impressora padr�o
        DeviceDefault = LeArquivoIni("WIN.INI", "windows", "device")
        OldDefault = DeviceDefault

        '  Abre a conex�o com o Crystal Report.
        Report.Reset
        Report.Connect = rdocn.Connect
        Report.ReportFileName = App.PATH & "\SEGR0223-08.rpt"

        Report.StoredProcParam(0) = CStr(vApoliceId)
        Report.StoredProcParam(1) = CStr(vRamoId)
        Report.StoredProcParam(2) = CStr(gEndossoId)

        Report.Formulas(1) = "pParametros='Ap�lice: " & CStr(vApoliceId) & "          Ramo: " & CStr(vRamoId) & "          Endosso: " & CStr(gEndossoId) & "'"

        Report.Destination = crptToWindow    ' direciona saida para Preview de tela.
        Report.WindowState = crptMaximized

        Report.Action = 1

    Else
        MsgBox "N�o h� itens na grade a serem selecionados."
        Exit Sub
    End If

End Sub

Private Sub cmdRetorna_Click(Index As Integer)
    Unload Me
End Sub

Private Sub Form_Activate()

    Screen.MousePointer = vbDefault

End Sub

Private Sub gridClausulas_Click()
    Dim i As Long, clausulaElemento As Clausula

    'Se grid possui apenas uma linha ou duas linhas com uma em branco n�o executa
    If gridClausulas.Rows = 2 And gridClausulas.RowSel = 1 Then
        If gridClausulas.TextMatrix(gridClausulas.RowSel, 0) = "" Then Exit Sub
    ElseIf gridClausulas.Rows = 1 Then
        Exit Sub
    End If

    For Each clausulaElemento In Clausulas_endosso
        If clausulaElemento.Id = gridClausulas.TextMatrix(gridClausulas.RowSel, 0) Then
            txtClausula.Text = clausulaElemento.Descricao
        End If
    Next

    vSelClausula = True

End Sub

Private Sub Form_Load()
    On Error GoTo Erro
    '
    Screen.MousePointer = vbHourglass
    '
    Me.Left = (Screen.Width - Me.Width) / 2     ' Center form horizontally.
    Me.Top = (Screen.Height - Me.Height) / 2    ' Center form vertically.

    '*** Inclus�o do Sinalizado de PPE - Pessoas politicamente expostas - demanda 269994
    '*** 23/09/2008 - Fabio (Stefanini)
    Me.chkPPE.Enabled = False

    If Trim(vSegCliente) <> "" Then
        Me.Caption = "SEGP0223 - Consulta Endosso Vida - " & Ambiente & " - " & vSegCliente  'Rmarins - 05/01/2006
    Else
        Me.Caption = "SEGP0223 - Consulta Endosso Vida - " & Ambiente
    End If

    ' Inibe todas as tabs de tabEndosso.
    tabEndossos.TabEnabled(TABendosso_PROPOSTA) = False
    tabEndossos.TabEnabled(TABendosso_DADOSCADASTRAIS) = False
    tabEndossos.TabEnabled(TABendosso_SUBGRUPO) = False
    tabEndossos.TabEnabled(TABendosso_CORRETAGEM) = False
    tabEndossos.TabEnabled(TABendosso_ADMINISTRACAO) = False
    tabEndossos.TabEnabled(TABendosso_COMPONENTES) = False
    tabEndossos.TabEnabled(TABendosso_COBERTURAS) = False
    tabEndossos.TabEnabled(TABendosso_BENEFICIARIOS) = False
    tabEndossos.TabEnabled(TABendosso_CLAUSULA) = False
    tabEndossos.TabEnabled(TABendosso_COSSEGURO) = False
    tabEndossos.TabEnabled(TABendosso_VIDA) = False
    '
    If gTpEmissao = "A" Then
        TabEscolha = "escolha_tp_cob_vida_aceito_tb"
        TabSeguro = "seguro_vida_aceito_tb"
    Else
        TabEscolha = "escolha_tp_cob_vida_tb"
        TabSeguro = "seguro_vida_tb"
    End If
    '
    dtInicioVigEndosso = CDate(gDtEmiEndosso)
    tp_endo_fatura = "10 100 101 200"
    HaEndFinanceiro = False
    '
    Call LimparColecao(Coberturas_endosso)
    Call LimparColecao(Componentes_endosso)
    '-----  novas rotinas para composicao das abas -----
    Call Prepara_Proposta
    Call Prepara_Dados_Cadastrais
    Call Prepara_SubGrupos
    Call Prepara_Vidas

    If InStr(1, tp_endo_fatura, CStr(gTpEndosso), 1) > 0 Then
        HaEndFinanceiro = True
        Call Prepara_Corretagem_Fatura
        Call Prepara_Administracao_Fatura
        If gTpEmissao = "C" Then
            Call Prepara_Cosseguro_Fatura
        Else
            Call Prepara_Cosseguro_Geral
        End If
    Else
        Call Prepara_Corretagem_Geral
        Call Prepara_Administracao_Geral
        Call Prepara_Cosseguro_Geral
    End If
    Call Prepara_Componentes
    Call Prepara_Coberturas
    Call Prepara_Beneficiario
    Call Prepara_Clausulas
    '---------------------------------------------------
    '

    '*** Inclus�o do Sinalizado de PPE - Pessoas politicamente expostas - demanda 269994
    '*** 23/09/2008 - Fabio (Stefanini)
    If IdentificaPPE(Me.TxtCPF_CGCAnt.Text) Then Me.chkPPE.Value = 1 Else Me.chkPPE.Value = 0

    Screen.MousePointer = vbDefault
    '
    Exit Sub

Erro:
    TrataErroGeral "Form_Load", Me.name
    MensagemBatch ("Erro ao carregar a tela de Endosso")
    TerminaSEGBR

End Sub


Sub monta_colecao(ByVal rs As rdoResultset, ByVal NumProposta, ByVal PRODUTOID, _
                  Optional ByVal dt_endosso As String, _
                  Optional ByVal DtIniVigencia As String, Optional ByVal dtIniVigenciaSeg As String)

    Dim Premio As Double, DESCONTO As Double, vCobertura As New Cobertura
    Dim vValPremio As String, vValLimMinFranquia As String, vValIS As String
    Dim vFatTaxa As String, Indice As String, vValFranquia As String, sql1 As String
    Dim DtFimVigencia As String, vDescricao As String, TipoOperacao As String

    On Error GoTo Erro

    Select Case rs(0)
    Case "1"
        TabEscolha = "escolha_tp_cob_emp_tb"
    Case "2"
        TabEscolha = "escolha_tp_cob_res_tb"
    Case "3"
        TabEscolha = "escolha_tp_cob_cond_tb"
    Case "4"
        TabEscolha = "escolha_tp_cob_maq_tb"
    Case "5"
        TabEscolha = "escolha_tp_cob_aceito_tb"
    Case "6"
        TabEscolha = "escolha_tp_cob_avulso_tb"
    Case "7"
        TabEscolha = "escolha_tp_cob_generico_tb"
    End Select

    While Not rs.EOF

        'Verifica se foi altera��o ou nova cobertura
        SQL = "SELECT e.num_endosso, c.tp_cobertura_id, e.val_is, e.val_premio, " & _
            "         e.dt_fim_vigencia_esc, e.cod_objeto_segurado, " & _
              "e.acumula_is, e.acumula_is_re_seguro " & _
            " FROM  " & TabEscolha & " e WITH(NOLOCK), tp_cob_item_prod_tb b   WITH (NOLOCK)  , tp_cobertura_tb c  WITH (NOLOCK)  " & _
            " WHERE (c.tp_cobertura_id = e.tp_cobertura_id)                   AND " & _
            "       (e.tp_cobertura_id = b.tp_cobertura_id)                   AND " & _
            "       (e.produto_id = b.produto_id)                             AND " & _
            "       (e.ramo_id = b.ramo_id)                                   AND " & _
            "       (e.proposta_id         =  " & GPProposta_id & ")          AND " & _
            "       (e.produto_id          =  " & PRODUTOID & ")              AND " & _
            "       (c.tp_cobertura_id     =  " & rs!tp_cobertura_id & ")     AND " & _
            "       (e.cod_objeto_segurado =  " & rs!cod_objeto_segurado & ") AND " & _
            "       (e.dt_fim_vigencia_esc = '" & Format$(DateAdd("d", -1, dt_pedido_endosso), "yyyymmdd") & "') AND " & _
            "       (e.num_endosso       < " & GPEndosso_id & " or e.num_endosso  is null )" & _
            "        order by e.num_endosso desc "

        Set Rs1 = rdoCn1.OpenResultset(SQL)

        If Not Rs1.EOF Then    'Altera��o

            If Val(rs!val_is) - Val(Rs1!val_is) < 0 Then
                TipoOperacao = "Redu��o"
            Else
                TipoOperacao = "Aumento"
            End If

            If Val(rs!val_is) = 0 Then
                vValIS = "0,00"
            Else
                If ConfiguracaoBrasil Then
                    vValIS = Format(Abs(Val(rs!val_is) - Val(Rs1!val_is)), "###,###,###,##0.00")
                Else
                    vValIS = TrocaValorAmePorBras(Format(Abs(Val(rs!val_is) - Val(Rs1!val_is)), "###,###,###,##0.00"))
                End If
            End If

            If ConfiguracaoBrasil Then
                vValPremio = Format(Abs(Val(rs!val_Premio) - Val(Rs1!val_Premio)), "###,###,###,##0.00")
            Else
                vValPremio = TrocaValorAmePorBras(Format(Abs(Val(rs!val_Premio) - Val(Rs1!val_Premio)), "###,###,###,##0.00"))
            End If

        Else

            'Nova Cobertura
            TipoOperacao = "Inclus�o"

            If ConfiguracaoBrasil Then
                vValIS = Format(Val(rs!val_is), "###,###,###,##0.00")
            Else
                vValIS = TrocaValorAmePorBras(Format(Val(rs!val_is), "###,###,###,##0.00"))
            End If

            If ConfiguracaoBrasil Then
                vValPremio = Format(Val(rs!val_Premio), "###,###,###,##0.00")
            Else
                vValPremio = TrocaValorAmePorBras(Format(Val(rs!val_Premio), "###,###,###,##0.00"))
            End If
        End If


        If ConfiguracaoBrasil Then
            vFatTaxa = Format(Val(rs!fat_taxa), "0.0000000")
        Else
            vFatTaxa = TrocaValorAmePorBras(Format(Val(rs!fat_taxa), "0.0000000"))
        End If

        If Not IsNull(rs!fat_desconto_tecnico) Then
            DESCONTO = Val(rs!fat_desconto_tecnico)
            If Not DESCONTO = 0 Then
                DESCONTO = (1 - DESCONTO) * 100
            End If
        Else
            DESCONTO = 0
        End If

        If ConfiguracaoBrasil Then
            DESCONTO = Format(DESCONTO, "##0.00")
        Else
            DESCONTO = TrocaValorAmePorBras(Format(DESCONTO, "##0.00"))
        End If

        If ConfiguracaoBrasil Then
            vFatFranquia = Format(Val(rs!fat_franquia), "0.00000")
        Else
            vFatFranquia = TrocaValorAmePorBras(Format(Val(rs!fat_franquia), "0.00000"))
        End If

        If ConfiguracaoBrasil Then
            vValFranquia = Format(Val(rs!val_min_franquia), "###,###,###,##0.00")
        Else
            vValFranquia = TrocaValorAmePorBras(Format(Val(rs!val_min_franquia), "###,###,###,##0.00"))
        End If

        Set vCobertura = New Cobertura

        With vCobertura
            .CodObjSegurado = rs!cod_objeto_segurado
            .TpCoberturaId = rs!tp_cobertura_id
            .Descricao = rs!Nome
            .ValIS = vValIS
            .ValTaxa = vFatTaxa
            .PercDesconto = DESCONTO
            .ValPremio = vValPremio

            If Not IsNull(rs!texto_franquia) Then
                .DescFranquia = rs!texto_franquia
            Else
                .DescFranquia = " "
            End If

            .ValFranquia = vValFranquia
            .DtInicioVigencia = Format$(rs!dt_inicio_vigencia_esc, "dd/mm/yyyy")

            'Provisorio
            .DtFimVigencia = TipoOperacao

            ''****************************************************
            '      .AcumulaIS = rs!acumula_is
            '      .AcumulaISReseguro = rs!acumula_is_re_seguro
            ''****************************************************

        End With

        Coberturas.Add vCobertura
        rs.MoveNext
        Rs1.Close

    Wend

    rs.Close
    Exit Sub

Erro:
    TrataErroGeral "Ler Coberturas"
    MsgBox "Rotina: Ler Coberturas, erro na estrutura dos dados o programa ser� abortado " _
         , vbCritical
    End
End Sub
Sub Monta_CboComponente_endosso()
    Dim rs As rdoResultset
    Dim vSql As String
    Dim linha As String

    On Error GoTo Erro_Le_Comp

    CboComponente.Clear
    '
    vSql = "SELECT distinct c.tp_componente_id , c.nome "
    vSql = vSql & " FROM tp_plano_tb p  WITH (NOLOCK) , tp_cob_comp_plano_tb ccp   WITH (NOLOCK) , "
    vSql = vSql & " tp_cob_comp_tb cc  WITH (NOLOCK) , tp_componente_tb c   WITH (NOLOCK) "

    '*** Abosco @ 2003 abr 30
    '*** inclusao de vigencias em tp_cob_comp_item_tb
    vSql = vSql & " , tp_cob_comp_item_tb ci   WITH (NOLOCK) "

    vSql = vSql & " WHERE  p.produto_id = " & vProdutoId
    vSql = vSql & " AND ccp.tp_plano_id = p.tp_plano_id"
    vSql = vSql & " AND cc.tp_cob_comp_id = ccp.tp_cob_comp_id"
    vSql = vSql & " AND c.tp_componente_id = cc.tp_componente_id"

    '*** Abosco @ 2003 abr 30
    '*** inclusao de vigencias em tp_cob_comp_item_tb
    vSql = vSql & " AND cc.tp_cob_comp_id = ci.tp_cob_comp_id "
    vSql = vSql & " AND ci.dt_fim_vigencia_comp IS NULL "

    Set rs = rdoCn1.OpenResultset(vSql, rdOpenStatic)

    Do While Not rs.EOF
        linha = Format$(rs!tp_componente_id, "000")
        linha = linha & " - " & Trim$(rs!Nome)
        '
        CboComponente.AddItem linha
        CboComponente.ItemData(CboComponente.NewIndex) = rs!tp_componente_id
        rs.MoveNext
    Loop
    '
    Exit Sub

Erro_Le_Comp:
    TrataErroGeral "Erro Buscando Componentes do Produto."
    MsgBox "Erro na rotina Monta_CboComponente_endosso"
End Sub


Sub Monta_cboTpComponente_endosso()
    Dim vComponente As Componente

    CboTpComponente.Clear
    ''
    For Each vComponente In Componentes_endosso
        CboTpComponente.AddItem Format(vComponente.Codigo, "000") & " - " & vComponente.Nome
        CboTpComponente.ItemData(CboTpComponente.NewIndex) = vComponente.Codigo
    Next

End Sub
Sub MontarComboComponentes()
    Dim vCobertura As Cobertura
    Dim vComponente As Componente

    Dim cComponente As Collection

    Dim bExiste As Boolean
    Dim linhaCboComponentes As String

    Set cComponente = New Collection



    For Each vCobertura In Coberturas_endosso

        bExiste = False

        For Each vComponente In cComponente
            If vComponente.Codigo = vCobertura.TpComponenteId Then
                bExiste = True
                Exit For
            End If
        Next

        If Not bExiste Then
            Set vComponente = New Componente
            vComponente.Codigo = vCobertura.TpComponenteId
            vComponente.Nome = vCobertura.NomeComponente
            '
            Call cComponente.Add(vComponente, CStr(vComponente.Codigo))
            '
        End If

    Next

    For Each vComponente In cComponente
        '  acrescenta componente � CboTpComponente da aba de Coberturas
        linhaCboComponentes = Format(vComponente.Codigo, "000") & "-" & vComponente.Nome
        CboTpComponente.AddItem linhaCboComponentes
        CboTpComponente.ItemData(CboTpComponente.NewIndex) = vComponente.Codigo
    Next



End Sub

Sub Monta_GridCoberturas_endosso(vObjeto As Variant)
    Dim vCobertura As Cobertura, linha As String, i As Integer

    GridCoberturas.Rows = 1
    i = 0
    For Each vCobertura In Coberturas_endosso
        If vCobertura.CodObjSegurado = vObjeto Then
            i = i + 1
            linha = vCobertura.TpCoberturaId & vbTab  ' C�digo
            linha = linha & vCobertura.Descricao & vbTab    'Descri��o
            linha = linha & vCobertura.ValIS & vbTab    ' IS
            linha = linha & vCobertura.ValTaxa & vbTab    ' Taxa Tarifa
            linha = linha & vCobertura.PercBasica & vbTab    ' Perc.B�sica
            linha = linha & vCobertura.PercDesconto & vbTab    ' % Desconto T�cnico
            linha = linha & vCobertura.ValPremio & vbTab    ' Pr�mio
            linha = linha & vCobertura.ValFranquia & vbTab    ' Franquia
            If DtInicioVigencia <> " " Then
                linha = linha & vCobertura.DtInicioVigencia    ' Dt. In�cio Vig�ncia
            Else
                GridCoberturas.ColWidth(8) = 0
            End If
            If DtFimVigencia <> " " Then
                linha = linha & vCobertura.DtFimVigencia & vbTab    ' Dt. Fim Vig�ncia
            Else
                GridCoberturas.ColWidth(9) = 0
            End If

            GridCoberturas.AddItem linha

        End If
    Next

End Sub
Private Sub GrdObjSegurado_Click()
'    If grdObjSegurado.Rows > 1 Then
'
'        For Each riscoEndossado In endossoRisco
'            If (riscoEndossado.CodObjSegurado = grdObjSegurado.TextMatrix(grdObjSegurado.Row, 0)) And _
             '               (riscoEndossado.RiscoId = grdObjSegurado.TextMatrix(grdObjSegurado.Row, 3)) Then
'
'                End_Risco_id = riscoEndossado.RiscoId
'
'                txtEndRisco.Text = riscoEndossado.endereco
'                txtBaiRisco.Text = riscoEndossado.Bairro
'                txtCidRisco.Text = riscoEndossado.municipio
'                txtUfRisco.Text = riscoEndossado.estado
'                mskCepRisco.Text = riscoEndossado.CEP
'
'
'                'Residencial
'                If frmRes2.Visible Then
'
'                    'Aumento de performance
'                    auxFor = cboTpMoradia.ListCount - 1
'
'                    For i = 0 To auxFor
'                        If cboTpMoradia.ItemData(i) = Val(riscoEndossado.TpObjSegurado) Then
'                            cboTpMoradia.ListIndex = i
'                            Exit For
'                        End If
'                    Next
'
'                    'Aumento de performance
'                    auxFor = cboTpPredio.ListCount - 1
'
'                    For i = 0 To auxFor
'                        If cboTpPredio.ItemData(i) = Val(riscoEndossado.TpPredio) Then
'                            cboTpPredio.ListIndex = i
'                            Exit For
'                        End If
'                    Next
'
'                'Condom�nio
'                ElseIf frmCond1.Visible Then
'
'                    'Aumento de performance
'                    auxFor = cboTpCondominio.ListCount - 1
'
'                    For i = 0 To auxFor
'                        If cboTpCondominio.ItemData(i) = Val(riscoEndossado.TpObjSegurado) Then
'                            cboTpCondominio.ListIndex = i
'                            Exit For
'                        End If
'                    Next
'
'                'Maquinas
'                ElseIf fraMaquinas.Visible Then
'
'                    'Aumento de performance
'                    auxFor = cboTpEquipamento.ListCount - 1
'
'                    For i = 0 To auxFor
'                        If cboTpEquipamento.ItemData(i) = Val(riscoEndossado.TpObjSegurado) Then
'                            cboTpEquipamento.ListIndex = i
'                            Exit For
'                        End If
'                    Next
'
'                    'Aumento de performance
'                    auxFor = cboClEquipamento.ListCount - 1
'
'                    For i = 0 To auxFor
'                        If cboClEquipamento.ItemData(i) = Val(riscoEndossado.CmpTpObj) Then
'                            cboClEquipamento.ListIndex = i
'                            Exit For
'                        End If
'                    Next
'
'                'Empresarial / Avulso / Aceito
'                ElseIf fraLOC.Visible Then
'
'                    txtDescObjeto.Text = riscoEndossado.DescObjeto
'                    txtCodRubrica.Text = riscoEndossado.CodRubrica
'
'                    auxFor = cboSubRamo.ListCount - 1
'                    For i = 0 To auxFor
'                        If cboSubRamo.ItemData(i) = Val(riscoEndossado.SubRamo) Then
'                            cboSubRamo.ListIndex = i
'                            Exit For
'                        End If
'                    Next
'
'                    'Aceito
'                    If TabSeguro = "seguro_aceito_tb" Then
'
'                            'Aumento de performance
'                            auxFor = cboTpEquipamentoAce.ListCount - 1
'
'                            For i = 0 To auxFor
'                                If cboTpEquipamentoAce.ItemData(i) = Val(riscoEndossado.TpObjSegurado) Then
'                                    cboTpEquipamentoAce.ListIndex = i
'                                    Exit For
'                                End If
'                            Next
'                    Else
'                        'Transporte
'                        If RAMOID = 22 Then
'                            If riscoEndossado.ImpExp = "I" Then
''                                OptImpExp(0).Value = True
'                            Else
''                                OptImpExp(1).Value = True
'                            End If
'                        Else
'                            'Aumento de performance
'                            auxFor = cboClasseLocal.ListCount - 1
'
'                            For i = 0 To auxFor
'                                If cboClasseLocal.ItemData(i) = Val(riscoEndossado.ClsLoc) Then
'                                    cboClasseLocal.ListIndex = i
'                                    Exit For
'                                End If
'                            Next
'
'
'                            'Aumento de performance
'                            auxFor = cboTipoConstrucao.ListCount - 1
'
'                            For i = 0 To auxFor
'                                If cboTipoConstrucao.ItemData(i) = Val(riscoEndossado.TpObjSegurado) Then
'                                    cboTipoConstrucao.ListIndex = i
'                                    Exit For
'                                End If
'                            Next
'                        End If
'                    End If
'                End If
'                Exit For
'            End If
'        Next
'    End If

End Sub








Public Sub desmonta_colecao()

    Dim i As Integer

    'Benefici�rio
    i = endBenef.Count
    While endBenef.Count > 0
        endBenef.Remove i
        i = i - 1
    Wend


    'Coberturas Originais
    i = Coberturas.Count
    While Coberturas.Count > 0
        Coberturas.Remove i
        i = i - 1
    Wend


    'para nova cobertura no endosso
    i = NovaCobertura.Count
    While NovaCobertura.Count > 0
        NovaCobertura.Remove i
        i = i - 1
    Wend

    'Corretores
    i = Corretores.Count
    While Corretores.Count > 0
        Corretores.Remove i
        i = i - 1
    Wend

    'EndRisco
    i = endossoRisco.Count
    While endossoRisco.Count > 0
        endossoRisco.Remove i
        i = i - 1
    Wend

    'Cl�usula
    i = Clausulas_endosso.Count
    While Clausulas_endosso.Count > 0
        Clausulas_endosso.Remove i
        i = i - 1
    Wend

End Sub


Public Sub monta_seguro_generico_avulso(ByVal rc As rdoResultset)


    On Error GoTo Erro

    'Comp�e frame e inicializa vari�veis
    frmRes2.Visible = False
    frmRes3.Visible = False
    frmCond1.Visible = False
    fraMaquinas.Visible = False
    fraLOC.Visible = False
    FraAceito.Visible = False
    fraImpExp.Visible = False


    'Processa
    While Not rc.EOF

        If Not IsNull(rc!dt_inicio_vigencia_seg) Then
            DT_Ini_Vigencia_Seg = Format$(rc!dt_inicio_vigencia_seg, "yyyy/mm/dd")
        Else
            DT_Ini_Vigencia_Seg = "19010101"
        End If

        If Not IsNull(rc!End_Risco_id) Then
            End_Risco_id = Val(rc!End_Risco_id)
        End If

        '    Set riscoEndossado = New EndRisco

        '** Falta definir os par�metros do gen�rico - se tiver
        With riscoEndossado
            .CodObjSegurado = rc!cod_objeto_segurado
            .RiscoId = End_Risco_id
            '.SubRamo = IIf(IsNull(rc!subramo_id), "", rc!subramo_id)
            '.CodRubrica = IIf(IsNull(rc!cod_rubrica), "", rc!cod_rubrica)
            '.ClsLoc = IIf(IsNull(rc!classe_local_id), "", rc!classe_local_id)
            '.TpObjSegurado = IIf(IsNull(rc!tp_construcao_id), "", rc!tp_construcao_id)
            .DtInicioVigencia = Format$(DT_Ini_Vigencia_Seg, "dd/mm/yyyy")
            .Operacao = "O"
        End With

        endossoRisco.Add riscoEndossado

        rc.MoveNext
    Wend

    Exit Sub

Erro:
    TrataErroGeral "Monta Seguro Generico Avulso"
    MsgBox "Rotina: Monta Seguro Generico Avulso"
    End
End Sub

Public Sub monta_seguro_aceito(ByVal rc As rdoResultset)
    Dim linha As String

    On Error GoTo Erro


    'Comp�e frame e inicializa vari�veis
    frmCond1.Visible = False
    frmRes2.Visible = False
    frmRes3.Visible = False
    fraMaquinas.Visible = False
    fraLOC.Visible = True
    FraAceito.Visible = False
    'FraAceito.Visible = True
    fraImpExp.Visible = False

    End_Risco_id = 0

    'Processa
    Do While Not rc.EOF

        If Not IsNull(rc!dt_inicio_vigencia_seg) Then
            DT_Ini_Vigencia_Seg = Format(rc!dt_inicio_vigencia_seg, "yyyy/mm/dd")
        Else
            DT_Ini_Vigencia_Seg = "19010101"
        End If

        If Not IsNull(rc!End_Risco_id) Then
            End_Risco_id = Val(rc!End_Risco_id)
        End If

        '    Set riscoEndossado = New EndRisco

        With riscoEndossado
            .CodObjSegurado = rc!cod_objeto_segurado
            .RiscoId = End_Risco_id
            .SubRamo = IIf(IsNull(rc!Subramo_id), "", rc!Subramo_id)
            .DescObjeto = IIf(IsNull(rc!nome_objeto), "", rc!nome_objeto)
            .CodRubrica = IIf(IsNull(rc!cod_rubrica), "", rc!cod_rubrica)
            '.TpObjSegurado = IIf(IsNull(rc!tp_equipamento_id), "", rc!tp_equipamento_id)
            '.CmpTpObj = IIf(IsNull(rc!cod_modalidade_ocupacao), "", rc!cod_modalidade_ocupacao)
            .DtInicioVigencia = Format$(DT_Ini_Vigencia_Seg, "dd/mm/yyyy")
            .Operacao = "O"
        End With

        endossoRisco.Add riscoEndossado

        fraLOC.Height = 1020

        rc.MoveNext
    Loop

    carrega_cboSubRamo

    Exit Sub

Erro:
    TrataErroGeral "Monta Seguro Avulso"
    MsgBox "Rotina: Monta Seguro Avulso"
    End
End Sub

Public Sub monta_seguro_avulso(ByVal rc As rdoResultset)
    Dim linha As String

    On Error GoTo Erro


    'Comp�e frame e inicializa vari�veis
    frmCond1.Visible = False
    frmRes2.Visible = False
    frmRes3.Visible = False
    fraMaquinas.Visible = False
    fraLOC.Visible = True
    FraAceito.Visible = False
    fraImpExp.Visible = False

    End_Risco_id = 0

    'Processa
    Do While Not rc.EOF

        If Not IsNull(rc!dt_inicio_vigencia_seg) Then
            DT_Ini_Vigencia_Seg = Format(rc!dt_inicio_vigencia_seg, "yyyy/mm/dd")
        Else
            DT_Ini_Vigencia_Seg = "19010101"
        End If

        If Not IsNull(rc!End_Risco_id) Then
            End_Risco_id = Val(rc!End_Risco_id)
        End If

        '    Set riscoEndossado = New EndRisco

        With riscoEndossado
            .CodObjSegurado = rc!cod_objeto_segurado
            .RiscoId = End_Risco_id
            .SubRamo = IIf(IsNull(rc!Subramo_id), "", rc!Subramo_id)
            .DescObjeto = IIf(IsNull(rc!nome_objeto), "", rc!nome_objeto)
            .DtInicioVigencia = Format$(DT_Ini_Vigencia_Seg, "dd/mm/yyyy")
            .Operacao = "O"

            If RamoId = 11 Then
                .CodRubrica = IIf(IsNull(rc!cod_rubrica), "", rc!cod_rubrica)
                .ClsLoc = IIf(IsNull(rc!classe_local_id), "", rc!classe_local_id)
                .TpObjSegurado = IIf(IsNull(rc!tp_construcao_id), "", rc!tp_construcao_id)
                fraLOC.Height = 1980
            ElseIf RamoId = 22 Then
                .ImpExp = IIf(IsNull(rc!importacao_exportacao), "", rc!importacao_exportacao)
                fraLOC.Height = 1020
                fraImpExp.Visible = True
                FraAceito.Top = 1440
            ElseIf RamoId = 51 Then
                fraLOC.Height = 1020
            End If
        End With

        endossoRisco.Add riscoEndossado

        rc.MoveNext
    Loop

    carrega_cboSubRamo

    Exit Sub

Erro:
    TrataErroGeral "Monta Seguro Avulso"
    MsgBox "Rotina: Monta Seguro Avulso"
    End
End Sub

Public Sub monta_seguro_empresarial(ByVal rc As rdoResultset)
    Dim linha As String

    On Error GoTo Erro

    'Comp�e frame e inicializa vari�veis
    frmCond1.Visible = False
    frmRes2.Visible = False
    frmRes3.Visible = False
    fraMaquinas.Visible = False
    fraLOC.Visible = True
    FraAceito.Visible = False
    fraImpExp.Visible = False

    End_Risco_id = 0


    'Processa
    While Not rc.EOF

        If Not IsNull(rc!dt_inicio_vigencia_seg) Then
            DT_Ini_Vigencia_Seg = Format(rc!dt_inicio_vigencia_seg, "yyyy/mm/dd")
        Else
            DT_Ini_Vigencia_Seg = "19010101"
        End If

        If Not IsNull(rc!End_Risco_id) Then
            End_Risco_id = Val(rc!End_Risco_id)
        End If

        '    Set riscoEndossado = New EndRisco

        With riscoEndossado
            .CodObjSegurado = rc!cod_objeto_segurado
            .RiscoId = End_Risco_id
            .SubRamo = IIf(IsNull(rc!Subramo_id), "", rc!Subramo_id)
            .CodRubrica = IIf(IsNull(rc!cod_rubrica), "", rc!cod_rubrica)
            .ClsLoc = IIf(IsNull(rc!classe_local_id), "", rc!classe_local_id)
            .TpObjSegurado = IIf(IsNull(rc!tp_construcao_id), "", rc!tp_construcao_id)
            .DtInicioVigencia = Format$(DT_Ini_Vigencia_Seg, "dd/mm/yyyy")
            .Operacao = "O"
        End With

        endossoRisco.Add riscoEndossado

        rc.MoveNext
    Wend

    carrega_cboSubRamo

    Exit Sub

Erro:
    TrataErroGeral "Ler Seguro Empresarial"
    MsgBox "Rotina: Ler Seguro Empresarial"
    End




End Sub



Public Sub carrega_cboSubRamo()

    SQL = "SELECT subramo_id FROM subramo_tb   WITH (NOLOCK)  "
    SQL = SQL & "WHERE ramo_id=" & RamoId
    SQL = SQL & "  and dt_fim_vigencia_sbr is null"

    Set rs = rdoCn1.OpenResultset(SQL, rdOpenStatic)

    If Not rs.EOF Then

        While Not rs.EOF
            cboSubRamo.AddItem rs(0)
            cboSubRamo.ItemData(cboSubRamo.NewIndex) = rs(0)
            rs.MoveNext
        Wend

    End If
    rs.Close

End Sub

Public Sub Ler_Endosso_Financeiro()
    Dim SQL As String
    Dim sqlB As String
    Dim Ss As rdoResultset
    Dim DtPgto As rdoResultset
    Dim rcSituacao As rdoResultset

    On Error GoTo Erro

    SQL = "     SELECT isnull(val_premio_tarifa,0) val_premio_tarifa, "
    SQL = SQL & "      isnull(val_desconto_comercial,0) val_desconto_comercial, "
    SQL = SQL & "      isnull(val_is,0) val_is, "
    SQL = SQL & "      isnull(val_ir,0) val_ir, "
    SQL = SQL & "      isnull(val_adic_fracionamento,0) val_adic_fracionamento, "
    SQL = SQL & "      isnull(val_comissao,0) val_comissao, "
    SQL = SQL & "      isnull(val_financeiro,0) val_financeiro, "
    SQL = SQL & "      isnull(val_comissao_estipulante,0) val_comissao_estipulante, "
    SQL = SQL & "      isnull(num_parcelas,0) num_parcelas, "
    SQL = SQL & "      isnull(val_iof,0) val_iof, "
    SQL = SQL & "      isnull(custo_apolice,0) custo_apolice, "
    SQL = SQL & "      isnull(val_paridade_moeda,0) val_paridade_moeda, "
    SQL = SQL & "      isnull(dt_pgto,' ') dt_pgto "
    SQL = SQL & " FROM endosso_financeiro_tb   WITH (NOLOCK) "
    SQL = SQL & "WHERE endosso_id = " & Val(gEndossoId)
    SQL = SQL & "  AND proposta_id = " & Val(vPropostaId)

    Set Ss = rdoCn1.OpenResultset(SQL, rdOpenStatic)

    ''FlowBR 156809 - Apresenta��o da somat�ria das faturas quando n�o houver endosso financeiro para
    'Produto 718 - Seguro Cons�rcio Prestamista.
    'Ana Paula - Stefanini | 25/08/2006.
    If Ss.EOF And vProdutoId = 718 And gTpEndosso = 200 Then

        SQL = "     SELECT  ISNULL(SUM(f.val_bruto + ISNULL(f.val_desconto,0) - (f.val_iof + ISNULL(f.custo_apolice,0) + ISNULL(f.val_adic_fracionamento,0))),0) val_premio_tarifa,"
        SQL = SQL & " ISNULL(SUM(f.val_desconto),0) val_desconto_comercial,"
        SQL = SQL & " ISNULL(SUM(f.tot_imp_segurada),0) val_is,"
        SQL = SQL & " ISNULL(SUM((f.val_comissao * (0.015))),0) val_ir,"
        SQL = SQL & " ISNULL(SUM(f.val_adic_fracionamento),0) val_adic_fracionamento,"
        SQL = SQL & " ISNULL(SUM(f.val_comissao),0) val_comissao,"
        SQL = SQL & " ISNULL(SUM(f.val_bruto),0) val_financeiro,"
        SQL = SQL & " ISNULL(SUM(f.val_comissao_estipulante),0) val_comissao_estipulante,"
        SQL = SQL & " 1 as  num_parcelas,"
        SQL = SQL & " ISNULL(SUM(f.val_iof),0) val_iof,"
        SQL = SQL & " ISNULL(SUM(f.custo_apolice),0) custo_apolice,"
        SQL = SQL & " 0 as val_paridade_moeda,"
        SQL = SQL & " isnull(max(f.dt_recebimento),' ') dt_pgto"
        SQL = SQL & " FROM    fatura_tb f  WITH (NOLOCK) "
        SQL = SQL & " Where f.apolice_id = " & Val(vApoliceId)
        SQL = SQL & " AND   f.ramo_id = " & Val(vRamoId)
        SQL = SQL & " AND   f.endosso_id =  " & Val(gEndossoId)

        Set Ss = rdoCn1.OpenResultset(SQL, rdOpenStatic)
    End If

    If Not Ss.EOF Then
        txtIOF = FormatValor(Ss("val_iof"))
        Txtval_adic_fracionamento = FormatValor(Ss("val_adic_fracionamento"))
        ValAdicFracNum = Ss("val_adic_fracionamento")
        Txtval_premio_total = FormatValor(Ss("val_financeiro"))
        Txtval_premio_tarifa = FormatValor(Ss("val_premio_tarifa"))
        ValPremioTarifaNum = Ss("val_premio_tarifa")
        Txtval_desconto_comercial = FormatValor(Ss("val_desconto_comercial"))
        ValDescNum = Ss("val_desconto_comercial")
        Txtval_iof = FormatValor(Ss("val_iof"))
        Txtval_ir = FormatValor(Ss("val_ir"))
        TxtImpSegurada = FormatValor(Ss("val_is"))
        Txtval_comissao = FormatValor(Ss("val_comissao"))
        ValComissaoNum = Ss("val_comissao")
        Txtcusto_endosso = FormatValor(Ss("custo_apolice"))
        Txtval_comissao_estipulante = FormatValor(Ss("val_comissao_estipulante"))
        ValComissaoEstipNum = Ss("val_comissao_estipulante")
        If Ss("dt_pgto") <> " " Then
            If Ss("dt_pgto") = "01/01/1900" Then
                TxtDtPgto.Text = "(Migra��o)"
            Else
                TxtDtPgto.Text = Format(Trim(Ss("dt_pgto")), "dd/mm/yyyy")
            End If
            If gTpEndosso <> "101" Then
                ''FlowBR 156809 - Apresenta��o da situa��o da fatura.
                'Ana Paula - Stefanini | 29/08/2006.
                If vProdutoId = 718 And gTpEndosso = 200 Then
                    SQL = "SELECT  situacaoDesc = CASE " & vbNewLine
                    SQL = SQL + " WHEN situacao = 'b' THEN 'Baixado' " & vbNewLine
                    SQL = SQL + " WHEN situacao = 'c' THEN 'Cancelado'" & vbNewLine
                    SQL = SQL + " WHEN situacao = 'e' THEN 'Pend. Envio'" & vbNewLine
                    SQL = SQL + " WHEN situacao = 't' THEN 'Pend. Aprova��o'" & vbNewLine
                    SQL = SQL + " Else ' '" & vbNewLine
                    SQL = SQL + " End" & vbNewLine
                    SQL = SQL + " From fatura_tb WITH (NOLOCK) " & vbNewLine
                    SQL = SQL + " Where proposta_id = " & Val(vPropostaId) & vbNewLine
                    SQL = SQL + " AND endosso_id = " & Val(gEndossoId)

                    Set rcSituacao = rdoCn1.OpenResultset(SQL, rdOpenStatic)

                    If Not rcSituacao.EOF Then
                        TxtSituacao.Text = rcSituacao("situacaoDesc")
                    End If
                Else
                    TxtSituacao.Text = "Pendente"
                End If
            End If
        Else
            ' Procura pela data de pagamento em agendamento_cobranca_tb.
            sqlB = "SELECT isnull(dt_baixa,' ') dt_baixa "
            sqlB = sqlB & " FROM agendamento_cobranca_tb   WITH (NOLOCK)  "
            sqlB = sqlB & " WHERE proposta_id = " & Val(vPropostaId)
            sqlB = sqlB & " AND num_endosso = " & Val(gEndossoId)

            Set DtPgto = rdoCn1.OpenResultset(sqlB, rdOpenStatic)
            If Not DtPgto.EOF Then
                If DtPgto!dt_baixa <> " " Then
                    If DtPgto!dt_baixa = "01/01/1900" Then
                        TxtDtPgto.Text = "Migra��o"
                    Else
                        TxtDtPgto.Text = Format(Trim(DtPgto!dt_baixa), "dd/mm/yyyy")
                    End If
                End If
            Else
                TxtSituacao.Text = "Pendente"
            End If
        End If

        If Val(Ss!val_paridade_moeda) > 0 Then
            lblConsEndosso12.Visible = True: lblFatCambio.Visible = True
            lblFatCambio.Caption = Format(Val(Ss!val_paridade_moeda), "##0.0000")
        Else
            lblConsEndosso12.Visible = False: lblFatCambio.Visible = False
        End If
    End If

    Exit Sub

Erro:
    TrataErroGeral "ConEndosso_Ler_Endosso_Financeiro"
    MsgBox "Rotina: Lendo Endosso"
    End

End Sub

Private Function FormatValor(valor As Variant) As String
    Dim F As Boolean

    If IsNull(valor) Then
        FormatValor = "0,00"
    Else
        FormatValor = Format(Val(valor), "###,###,##0.00")
    End If

End Function

'*** Inclus�o do Sinalizado de PPE - Pessoas politicamente expostas - demanda 269994
'*** 23/09/2008 - Fabio (Stefanini)

Private Function IdentificaPPE(strCPF As String) As Boolean

'Sessao de variaveis
    Dim strSql As String
    Dim rc As rdoResultset

    'Manipulador de erros
    On Error GoTo ErrIdentificaPPE

    'Valor Padrao
    IdentificaPPE = False

    'Retira Formacao caso exista
    strCPF = Replace(strCPF, ".", "")
    strCPF = Replace(strCPF, "-", "")
    strCPF = Replace(strCPF, "/", "")

    'Busca Identificar se faz parte do PPE
    'strSql = "SELECT COUNT(*) AS TOTAL "'alterado 12/09/2013 projeto 17860335 - Melhorias no SEGBR MC_Amaral -confitec
    strSql = "SELECT COUNT(1) AS TOTAL "
    strSql = strSql & " FROM seguros_db.DBO.PPE_ORGAO_CARGO_TB as TPPE WITH(NOLOCK) "
    strSql = strSql & " WHERE CAST(TPPE.CPF AS BIGINT) = CAST('" & strCPF & "' AS BIGINT)"
    strSql = strSql & " AND (TPPE.DT_EXCLUSAO_PPE IS NULL OR  TPPE.DT_EXCLUSAO_PPE >= GETDATE())"

    'Executa o Comando
    Set rc = rdocn.OpenResultset(strSql)

    'Verifica se voltou registro
    If rc.EOF Then GoTo Finaliza

    'Verifica Valor Valido
    If IsNull(rc!TOTAL) Then GoTo Finaliza

    'vERIFICA nao ENCONTROU
    If CInt(rc!TOTAL) <= 0 Then GoTo Finaliza

    'Valor Padrao
    IdentificaPPE = True

    'Finaliza
Finaliza:
    rc.Close
    Set rc = Nothing

    'Sai
    Exit Function

    'Manipulador de erros
ErrIdentificaPPE:

    'Fecha Recordsrt
    rc.Close
    Set rc = Nothing

End Function




