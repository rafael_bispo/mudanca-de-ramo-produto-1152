Attribute VB_Name = "PorExtenso"
Option Explicit
Public Function Extenso_Valor(pdbl_Valor As Double) As String
'Rotina Criada para ler um n�mero e transform�-lo em extenso
'Limite m�ximo de 9 Bilh�es (9.999.999.999,99)
'N�o aceita n�meros negativos

    Dim strValorExtenso As String    'Vari�vel que ir� armazenar o valor por extenso do n�mero informado
    Dim strNumero As String    'Ir� armazenar o n�mero para exibir por extenso
    Dim strCentena, strDezena, strUnidade As String
    Dim dblCentavos, dblValorInteiro As Double
    Dim intContador As Integer
    Dim bln_Bilhao, bln_Milhao, bln_Mil, bln_Real, bln_Unidade As Boolean

    'Verificar se foi informado um dado indevido
    If Not IsNumeric(pdbl_Valor) Or IsEmpty(pdbl_Valor) Then
        strValorExtenso = "Fun��o s� suporta n�meros"
    ElseIf pdbl_Valor <= 0 Then    'Verificar se h� valor negativo ou nada foi informado
        strValorExtenso = ""
        'Verificar se foi informado um valor n�o suportado pela fun��o
    ElseIf pdbl_Valor > 9999999999.99 Then
        strValorExtenso = "Valor n�o Suportado pela Fun��o"
    Else
        'Gerar Extenso Centavos
        dblCentavos = pdbl_Valor - Int(pdbl_Valor)
        'Gerar Extenso parte Inteira
        dblValorInteiro = Int(pdbl_Valor)

        If dblValorInteiro > 0 Then
            For intContador = Len(Trim(Str(dblValorInteiro))) To 1 Step -1
                strNumero = Mid(Trim(Str(dblValorInteiro)), (Len(Trim(Str(dblValorInteiro))) - intContador) + 1, 1)
                Select Case intContador
                Case Is = 10    'Bilh�o
                    strValorExtenso = fcn_Numero_Unidade(strNumero) + IIf(strNumero > "1", " Bilh�es ", " Bilh�o ")
                    bln_Bilhao = True
                Case Is = 9, 6, 3   'Centena
                    If strNumero > "0" Then
                        strCentena = Mid(Trim(Str(dblValorInteiro)), (Len(Trim(Str(dblValorInteiro))) - intContador) + 1, 3)
                        If strCentena > "100" And strCentena < "200" Then
                            strValorExtenso = strValorExtenso + " Cento e "
                        Else
                            strValorExtenso = strValorExtenso + " " + fcn_Numero_Centena(strNumero)
                        End If
                        If intContador = 9 Then
                            bln_Milhao = True
                        ElseIf intContador = 6 Then
                            bln_Mil = True
                        End If
                    End If
                Case Is = 8, 5, 2   'Dezena de Milh�o
                    If strNumero > "0" Then
                        strDezena = Mid(Trim(Str(dblValorInteiro)), (Len(Trim(Str(dblValorInteiro))) - intContador) + 1, 2)
                        If strDezena > 10 And strDezena < 20 Then
                            strValorExtenso = strValorExtenso + IIf(Trim(Right(strValorExtenso, 5)) = "entos", " e ", " ") + fcn_Numero_Dezena0(Right(strDezena, 1))
                            bln_Unidade = True
                        Else
                            strValorExtenso = strValorExtenso + IIf(Trim(Right(strValorExtenso, 5)) = "entos", " e ", " ") + fcn_Numero_Dezena1(strNumero)
                            bln_Unidade = False
                        End If
                        If intContador = 8 Then
                            bln_Milhao = True
                        ElseIf intContador = 5 Then
                            bln_Mil = True
                        End If
                    End If
                Case Is = 7, 4, 1   'Unidade de Milh�o
                    If strNumero > "0" And Not bln_Unidade Then
                        If Trim(Right(strValorExtenso, 5)) = "entos" Or Trim(Right(strValorExtenso, 3)) = "nte" Or Trim(Right(strValorExtenso, 3)) = "nta" Then
                            strValorExtenso = strValorExtenso + " e "
                        Else
                            strValorExtenso = strValorExtenso + " "
                        End If
                        strValorExtenso = strValorExtenso + fcn_Numero_Unidade(strNumero)
                    End If
                    If intContador = 7 Then
                        If bln_Milhao Or strNumero > "0" Then
                            strValorExtenso = strValorExtenso + IIf(strNumero = "1" And Not bln_Unidade, " Milh�o ", " Milh�es ")
                            bln_Milhao = True
                        End If
                    End If
                    If intContador = 4 Then
                        If bln_Mil Or strNumero > "0" Then
                            strValorExtenso = strValorExtenso + " Mil "
                            bln_Mil = True
                        End If
                    End If
                    If intContador = 1 Then
                        If (bln_Bilhao And Not bln_Milhao And Not bln_Mil And Right(Trim(Str(dblValorInteiro)), 3) = 0) Or _
                           (Not bln_Bilhao And bln_Milhao And Not bln_Mil And Right(Trim(Str(dblValorInteiro)), 3) = 0) Then
                            strValorExtenso = strValorExtenso + " de "
                        End If
                        strValorExtenso = strValorExtenso + IIf(dblValorInteiro > 1, " Reais", " Real ")
                    End If
                    bln_Unidade = False
                End Select
            Next intContador
        End If

        If dblCentavos > 0# And dblCentavos < 0.1 Then
            strNumero = Right(Trim(Str(Round(dblCentavos, 2))), 1)
            strValorExtenso = strValorExtenso + IIf(dblValorInteiro > 0, " e ", " ") + fcn_Numero_Unidade(strNumero) + IIf(strNumero > "1", " Centavos ", " Centavo ")
        ElseIf dblCentavos > 0.1 And dblCentavos < 0.2 Then
            strNumero = Right(Trim(Str(Round(dblCentavos, 2) - 0.1)), 1)
            strValorExtenso = strValorExtenso + IIf(dblValorInteiro > 0, " e ", " ") + fcn_Numero_Dezena0(strNumero) + " Centavos "
        Else
            If dblCentavos > 0# Then
                strNumero = Mid(Trim(Str(dblCentavos)), 2, 1)
                strValorExtenso = strValorExtenso + IIf(dblValorInteiro > 0, " e ", " ") + fcn_Numero_Dezena1(strNumero)
                If Len(Trim(Str(dblCentavos))) > 2 Then
                    strNumero = Right(Trim(Str(Round(dblCentavos, 2))), 1)
                    strValorExtenso = strValorExtenso + " e " + fcn_Numero_Unidade(strNumero)
                End If
                strValorExtenso = strValorExtenso + " Centavos "
            End If
        End If

    End If

    strValorExtenso = Replace(Trim(strValorExtenso), "  ", ", ")

    Extenso_Valor = Trim(strValorExtenso)
End Function

Function fcn_Numero_Unidade(pstrUnidade As String) As String
'Vetor que ir� conter o n�mero por extenso
    Dim array_Unidade(9) As String

    array_Unidade(0) = "Um"
    array_Unidade(1) = "Dois"
    array_Unidade(2) = "Tr�s"
    array_Unidade(3) = "Quatro"
    array_Unidade(4) = "Cinco"
    array_Unidade(5) = "Seis"
    array_Unidade(6) = "Sete"
    array_Unidade(7) = "Oito"
    array_Unidade(8) = "Nove"

    fcn_Numero_Unidade = array_Unidade(Val(pstrUnidade) - 1)
End Function

Function fcn_Numero_Dezena0(pstrDezena0 As String) As String
'Vetor que ir� conter o n�mero por extenso
    Dim array_Dezena0(9) As String

    array_Dezena0(0) = "Onze"
    array_Dezena0(1) = "Doze"
    array_Dezena0(2) = "Treze"
    array_Dezena0(3) = "Quatorze"
    array_Dezena0(4) = "Quinze"
    array_Dezena0(5) = "Dezesseis"
    array_Dezena0(6) = "Dezessete"
    array_Dezena0(7) = "Dezoito"
    array_Dezena0(8) = "Dezenove"

    fcn_Numero_Dezena0 = array_Dezena0(Val(pstrDezena0) - 1)
End Function

Function fcn_Numero_Dezena1(pstrDezena1 As String) As String
'Vetor que ir� conter o n�mero por extenso
    Dim array_Dezena1(9) As String

    array_Dezena1(0) = "Dez"
    array_Dezena1(1) = "Vinte"
    array_Dezena1(2) = "Trinta"
    array_Dezena1(3) = "Quarenta"
    array_Dezena1(4) = "Cinquenta"
    array_Dezena1(5) = "Sessenta"
    array_Dezena1(6) = "Setenta"
    array_Dezena1(7) = "Oitenta"
    array_Dezena1(8) = "Noventa"

    fcn_Numero_Dezena1 = array_Dezena1(Val(pstrDezena1) - 1)
End Function

Function fcn_Numero_Centena(pstrCentena As String) As String
'Vetor que ir� conter o n�mero por extenso
    Dim array_Centena(9) As String

    array_Centena(0) = "Cem"
    array_Centena(1) = "Duzentos"
    array_Centena(2) = "Trezentos"
    array_Centena(3) = "Quatrocentos"
    array_Centena(4) = "Quinhentos"
    array_Centena(5) = "Seiscentos"
    array_Centena(6) = "Setecentos"
    array_Centena(7) = "Oitocentos"
    array_Centena(8) = "Novecentos"

    fcn_Numero_Centena = array_Centena(Val(pstrCentena) - 1)
End Function

