VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Fatura"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Option Explicit

'local variable(s) to hold property value(s)
Private mvarId As Long    'local copy
Private mvarValIS As Double    'local copy
Private mvarValPremio As Double    'local copy
Private mvarValIOF As Double    'local copy
Private mvarDtInicioVigencia As Date    'local copy
Private mvarDtFimVigencia As Date    'local copy
Private mvarDtRecebimento As Date    'local copy
Private mvarSituacao As String    'local copy
Private mvarPercCorretagem As Single    'local copy
Private mvarPercProLabore As Single    'local copy
Private mvarObservacao As String    'local copy
'local variable(s) to hold property value(s)
Private mvarEndossoId As String    'local copy
'local variable(s) to hold property value(s)
Private mvarEndossoIdLider As String    'local copy
Private mvarDtBaixa As Date
Private mvarOriBaixa As String
Private mvarNossoNumero As String
Private mvarQtdeVidas As Long

Public Property Get DtBaixa() As Date
    DtBaixa = mvarDtBaixa
End Property

Public Property Let DtBaixa(ByVal vData As Date)
    mvarDtBaixa = vData
End Property

Public Property Get OriBaixa() As String
    OriBaixa = mvarOriBaixa
End Property

Public Property Let OriBaixa(ByVal vData As String)
    mvarOriBaixa = vData
End Property

Public Property Let EndossoIdLider(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.EndossoIdLider = 5
    mvarEndossoIdLider = vData
End Property


Public Property Get EndossoIdLider() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.EndossoIdLider
    EndossoIdLider = mvarEndossoIdLider
End Property

Public Property Let EndossoId(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.EndossoId = 5
    mvarEndossoId = vData
End Property


Public Property Get EndossoId() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.EndossoId
    EndossoId = mvarEndossoId
End Property

Public Property Let NossoNumero(ByVal vData As String)
    mvarNossoNumero = vData
End Property

Public Property Get NossoNumero() As String
    NossoNumero = mvarNossoNumero
End Property

Public Property Let Observacao(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Observacao = 5
    mvarObservacao = vData
End Property


Public Property Get Observacao() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Observacao
    Observacao = mvarObservacao
End Property



Public Property Let PercProLabore(ByVal vData As Single)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.PercProLabore = 5
    mvarPercProLabore = vData
End Property


Public Property Get PercProLabore() As Single
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.PercProLabore
    PercProLabore = mvarPercProLabore
End Property



Public Property Let PercCorretagem(ByVal vData As Single)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.PercCorretagem = 5
    mvarPercCorretagem = vData
End Property


Public Property Get PercCorretagem() As Single
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.PercCorretagem
    PercCorretagem = mvarPercCorretagem
End Property



Public Property Let Situacao(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Situacao = 5
    mvarSituacao = vData
End Property


Public Property Get Situacao() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Situacao
    Situacao = mvarSituacao
End Property



Public Property Let DtRecebimento(ByVal vData As Date)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.DtRecebimento = 5
    mvarDtRecebimento = vData
End Property


Public Property Get DtRecebimento() As Date
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.DtRecebimento
    DtRecebimento = mvarDtRecebimento
End Property



Public Property Let DtFimVigencia(ByVal vData As Date)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.DtFimVigencia = 5
    mvarDtFimVigencia = vData
End Property


Public Property Get DtFimVigencia() As Date
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.DtFimVigencia
    DtFimVigencia = mvarDtFimVigencia
End Property



Public Property Let DtInicioVigencia(ByVal vData As Date)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.DtInicioVigencia = 5
    mvarDtInicioVigencia = vData
End Property


Public Property Get DtInicioVigencia() As Date
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.DtInicioVigencia
    DtInicioVigencia = mvarDtInicioVigencia
End Property



Public Property Let ValIOF(ByVal vData As Double)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.ValIOF = 5
    mvarValIOF = vData
End Property


Public Property Get ValIOF() As Double
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.ValIOF
    ValIOF = mvarValIOF
End Property



Public Property Let ValPremio(ByVal vData As Double)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.ValPremio = 5
    mvarValPremio = vData
End Property


Public Property Get ValPremio() As Double
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.ValPremio
    ValPremio = mvarValPremio
End Property



Public Property Let ValIS(ByVal vData As Double)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.ValIS = 5
    mvarValIS = vData
End Property


Public Property Get ValIS() As Double
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.ValIS
    ValIS = mvarValIS
End Property



Public Property Let Id(ByVal vData As Long)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Id = 5
    mvarId = vData
End Property


Public Property Get Id() As Long
Attribute Id.VB_UserMemId = 0
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Id
    Id = mvarId
End Property

Public Property Get QtdeVidas() As Long
    QtdeVidas = mvarQtdeVidas
End Property

Public Property Let QtdeVidas(ByVal vData As Long)
    mvarQtdeVidas = vData
End Property

