VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Congenere"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Private mvarSeguradora As String    'local copy
Private mvarPerc_Despesa_Lider As Double    'local copy
Private mvarPerc_Participacao As Double    'local copy
Private mvarNome As String    'local copy
'local variable(s) to hold property value(s)
Private mvarNumOrdem As String    'local copy
'local variable(s) to hold property value(s)
Private mvarDtIniParticipacao As String    'local copy
Private mvarDtFimParticipacao As String    'local copy

Public Property Let DtFimParticipacao(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.DtFimParticipacao = 5
    mvarDtFimParticipacao = vData
End Property

Public Property Get DtFimParticipacao() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.DtFimParticipacao
    DtFimParticipacao = mvarDtFimParticipacao
End Property

Public Property Let DtIniParticipacao(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.DtIniParticipacao = 5
    mvarDtIniParticipacao = vData
End Property

Public Property Get DtIniParticipacao() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.DtIniParticipacao
    DtIniParticipacao = mvarDtIniParticipacao
End Property

Public Property Let NumOrdem(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.NumOrdem = 5
    mvarNumOrdem = vData
End Property

Public Property Get NumOrdem() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.NumOrdem
    NumOrdem = mvarNumOrdem
End Property

Public Property Let Nome(ByVal vData As String)
Attribute Nome.VB_Description = "N=Novo,E=Existente"
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Nome = 5
    mvarNome = vData
End Property

Public Property Get Nome() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Nome
    Nome = mvarNome
End Property

Public Property Let Seguradora(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Seguradora = 5
    mvarSeguradora = vData
End Property

Public Property Get Seguradora() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Seguradora
    Seguradora = mvarSeguradora
End Property

Public Property Let Perc_Despesa_Lider(ByVal vData As Double)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Perc_Despesa_Lider = 5
    mvarPerc_Despesa_Lider = vData
End Property

Public Property Get Perc_Despesa_Lider() As Double
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Perc_Despesa_Lider
    Perc_Despesa_Lider = mvarPerc_Despesa_Lider
End Property

Public Property Let Perc_Participacao(ByVal vData As Double)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Perc_Participacao = 5
    mvarPerc_Participacao = vData
End Property

Public Property Get Perc_Participacao() As Double
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Perc_Participacao
    Perc_Participacao = mvarPerc_Participacao
End Property
