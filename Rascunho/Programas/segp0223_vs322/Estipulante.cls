VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Estipulante"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Attribute VB_Ext_KEY = "Member0" ,"colAdministrador"
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
'local variable(s) to hold property value(s)
Private mvarId As String    'local copy
Private mvarNome As String    'local copy
'local variable(s) to hold property value(s)
Private mvarAdministradores As Collection    'local copy
Public Property Set Administradores(ByVal vData As Object)
'used when assigning an Object to the property, on the left side of a Set statement.
'Syntax: Set x.Admistradores = Form1
    Set mvarAdministradores = vData
End Property


Public Property Get Administradores() As Collection
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Admistradores
    Set Administradores = mvarAdministradores
End Property






















Public Property Let Nome(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Nome = 5
    mvarNome = vData
End Property


Public Property Get Nome() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Nome
    Nome = mvarNome
End Property



Public Property Let Id(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Id = 5
    mvarId = vData
End Property


Public Property Get Id() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Id
    Id = mvarId
End Property



