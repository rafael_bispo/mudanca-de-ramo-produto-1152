VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Corretor"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
'local variable(s) to hold property value(s)
Private mvarId As String    'local copy
Private mvarNome As String    'local copy
Private mvarPercCorretagem As Double    'local copy
'local variable(s) to hold property value(s)
Private mvarTipo As String    'local copy
'local variable(s) to hold property value(s)
Private mvarSucursal As String    'local copy
'local variable(s) to hold property value(s)
Private mvarPercParticipacao As Double    'local copy
Private mvarPercComissao As Double    'local copy
'local variable(s) to hold property value(s)
Private mvarDtIniCorretagem As String    'local copy
'local variable(s) to hold property value(s)
Private mvarDtFimCorretagem As String    'local copy
'Luciana - 11/11/2003
Private mvarCodigo_susep As String

Public Property Let DtFimCorretagem(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.DtFimCorretagem = 5
    mvarDtFimCorretagem = vData
End Property


Public Property Get DtFimCorretagem() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.DtFimCorretagem
    DtFimCorretagem = mvarDtFimCorretagem
End Property



Public Property Let DtIniCorretagem(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.DtIniCorretagem = 5
    mvarDtIniCorretagem = vData
End Property


Public Property Get DtIniCorretagem() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.DtIniCorretagem
    DtIniCorretagem = mvarDtIniCorretagem
End Property



Public Property Let PercComissao(ByVal vData As Double)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.PercComissao = 5
    mvarPercComissao = vData
End Property


Public Property Get PercComissao() As Double
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.PercComissao
    PercComissao = mvarPercComissao
End Property



Public Property Let PercParticipacao(ByVal vData As Double)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.PercParticipacao = 5
    mvarPercParticipacao = vData
End Property


Public Property Get PercParticipacao() As Double
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.PercParticipacao
    PercParticipacao = mvarPercParticipacao
End Property



Public Property Let Sucursal(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Sucursal = 5
    mvarSucursal = vData
End Property


Public Property Get Sucursal() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Sucursal
    Sucursal = mvarSucursal
End Property



Public Property Let Tipo(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Tipo = 5
    mvarTipo = vData
End Property


Public Property Get Tipo() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Tipo
    Tipo = mvarTipo
End Property



Public Property Let PercCorretagem(ByVal vData As Double)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.PercCorretagem = 5
    mvarPercCorretagem = vData
End Property


Public Property Get PercCorretagem() As Double
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.PercCorretagem
    PercCorretagem = mvarPercCorretagem
End Property



Public Property Let Nome(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Nome = 5
    mvarNome = vData
End Property


Public Property Get Nome() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Nome
    Nome = mvarNome
End Property



Public Property Let Id(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Id = 5
    mvarId = vData
End Property


Public Property Get Id() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Id
    Id = mvarId
End Property


'Luciana - 11/11/2003
Public Property Let Codigo_susep(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Id = 5
    mvarCodigo_susep = vData
End Property


'Luciana - 11/11/2003
Public Property Get Codigo_susep() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Id
    Codigo_susep = mvarCodigo_susep
End Property

