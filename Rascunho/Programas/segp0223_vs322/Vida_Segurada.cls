VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Vida_Segurada"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
'local variable(s) to hold property value(s)
Private mvarNome As String    'local copy
Private mvarCapital As Currency  'local copy
Private mvarSalario As Currency    'local copy
Private mvardtIniVigencia As String    'local copy
Private mvarDtFimVigencia As String    'local copy
Private mvarPropostaAdesao As Variant
Private mvarCPF_CNPJ As Variant
Private mvarCliente_id As Variant
Private mvarDtNascimento As String    'local copy
Private mvarDtEntradaSubGrupo As Date
Private mvarSexo As String * 1
Private mvarEstadoCivil As String
Private mvarEndereco As String
Private mvarBairro As String
Private mvarMunicipio As String
Private mvarCEP As String
Private mvarUF As String * 2
Private mvarEmail As String
Private mvarDDD As String
Private mvarTel As String
Private mvarConjuge As String
Private mvarConjugeCPF As Variant
Private mvarConjugeDtNascimento As Date    'local copy
Private mvarConjugeSexo As String * 1
Private mvarConjugeDtIniCobertura As Date
Private mvarBeneficiario1 As String
Private mvarBeneficiario2 As String
Private mvarBeneficiario3 As String
Private mvarBeneficiario4 As String
Private mvarBeneficiario5 As String
Private mvarBeneficiario6 As String
Private mvarBeneficiario7 As String
Private mvarTipoPendencia As Integer
Private mvarMotivoPendencia As String
Private mvarTextoBeneficiario As String
Private mvarPremio As Currency

Public Property Let DtNascimento(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.DtNascimento = 5
    mvarDtNascimento = vData
End Property

Public Property Get DtNascimento() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.DtNascimento
    DtNascimento = mvarDtNascimento
End Property

Public Property Let DtFimVigencia(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.DtFimAdministracao = 5
    mvarDtFimVigencia = vData
End Property

Public Property Get DtFimVigencia() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.DtFimAdministracao
    DtFimVigencia = mvarDtFimVigencia
End Property

Public Property Let DtIniVigencia(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.DtIniAdministracao = 5
    mvardtIniVigencia = vData
End Property

Public Property Get DtIniVigencia() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.DtIniAdministracao
    DtIniVigencia = mvardtIniVigencia
End Property

Public Property Let Salario(ByVal vData As Currency)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.IdEstipulante = 5
    mvarSalario = vData
End Property

Public Property Get Salario() As Currency
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.IdEstipulante
    Salario = mvarSalario
End Property

Public Property Let Capital(ByVal vData As Currency)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.PercProLabore = 5
    mvarCapital = vData
End Property

Public Property Get Capital() As Currency
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.PercProLabore
    Capital = mvarCapital
End Property

Public Property Let Nome(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Nome = 5
    mvarNome = vData
End Property

Public Property Get Nome() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Nome
    Nome = mvarNome
End Property

Public Property Let PropostaAdesao(ByVal vData As Variant)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.PercProLabore = 5
    mvarPropostaAdesao = vData
End Property

Public Property Get PropostaAdesao() As Variant
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.PercProLabore
    PropostaAdesao = mvarPropostaAdesao
End Property

Public Property Let CPF_CNPJ(ByVal vData As Variant)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.PercProLabore = 5
    mvarCPF_CNPJ = vData
End Property

Public Property Get CPF_CNPJ() As Variant
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.PercProLabore
    CPF_CNPJ = mvarCPF_CNPJ
End Property

Public Property Let Cliente_id(ByVal vData As Variant)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.PercProLabore = 5
    mvarCliente_id = vData
End Property

Public Property Get Cliente_id() As Variant
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.PercProLabore
    Cliente_id = mvarCliente_id
End Property

Public Property Let DtEntradaSubGrupo(ByVal vData As Date)
    mvarDtEntradaSubGrupo = vData
End Property

Public Property Get DtEntradaSubGrupo() As Date
    DtEntradaSubGrupo = mvarDtEntradaSubGrupo
End Property

Public Property Let Sexo(ByVal vData As String)
    mvarSexo = vData
End Property

Public Property Get Sexo() As String
    Sexo = mvarSexo
End Property

Public Property Let EstadoCivil(ByVal vData As String)
    mvarEstadoCivil = vData
End Property

Public Property Get EstadoCivil() As String
    EstadoCivil = mvarEstadoCivil
End Property

Public Property Let Endereco(ByVal vData As String)
    mvarEndereco = vData
End Property

Public Property Get Endereco() As String
    Endereco = mvarEndereco
End Property

Public Property Let Bairro(ByVal vData As String)
    mvarBairro = vData
End Property

Public Property Get Bairro() As String
    Bairro = mvarBairro
End Property

Public Property Let Municipio(ByVal vData As String)
    mvarMunicipio = vData
End Property

Public Property Get Municipio() As String
    Municipio = mvarMunicipio
End Property

Public Property Let CEP(ByVal vData As String)
    mvarCEP = vData
End Property

Public Property Get CEP() As String
    CEP = mvarCEP
End Property

Public Property Let UF(ByVal vData As String)
    mvarUF = vData
End Property

Public Property Get UF() As String
    UF = mvarUF
End Property

Public Property Let Email(ByVal vData As String)
    mvarEmail = vData
End Property

Public Property Get Email() As String
    Email = mvarEmail
End Property

Public Property Let DDD(ByVal vData As String)
    mvarDDD = vData
End Property

Public Property Get DDD() As String
    DDD = mvarDDD
End Property

Public Property Let Tel(ByVal vData As String)
    mvarTel = vData
End Property

Public Property Get Tel() As String
    Tel = mvarTel
End Property

Public Property Let Conjuge(ByVal vData As String)
    mvarConjuge = vData
End Property

Public Property Get Conjuge() As String
    Conjuge = mvarConjuge
End Property

Public Property Let ConjugeCPF(ByVal vData As Variant)
    mvarConjugeCPF = vData
End Property

Public Property Get ConjugeCPF() As Variant
    ConjugeCPF = mvarConjugeCPF
End Property

Public Property Let ConjugeDtNascimento(ByVal vData As Date)
    mvarConjugeDtNascimento = vData
End Property

Public Property Get ConjugeDtNascimento() As Date
    ConjugeDtNascimento = mvarConjugeDtNascimento
End Property

Public Property Let ConjugeSexo(ByVal vData As String)
    mvarConjugeSexo = vData
End Property

Public Property Get ConjugeSexo() As String
    ConjugeSexo = mvarConjugeSexo
End Property

Public Property Let ConjugeDtIniCobertura(ByVal vData As Date)
    mvarConjugeDtIniCobertura = vData
End Property

Public Property Get ConjugeDtIniCobertura() As Date
    ConjugeDtIniCobertura = mvarConjugeDtIniCobertura
End Property

Public Property Let Beneficiario1(ByVal vData As String)
    mvarBeneficiario1 = vData
End Property

Public Property Get Beneficiario1() As String
    Beneficiario1 = mvarBeneficiario1
End Property

Public Property Let Beneficiario2(ByVal vData As String)
    mvarBeneficiario2 = vData
End Property

Public Property Get Beneficiario2() As String
    Beneficiario2 = mvarBeneficiario2
End Property

Public Property Let Beneficiario3(ByVal vData As String)
    mvarBeneficiario3 = vData
End Property

Public Property Get Beneficiario3() As String
    Beneficiario3 = mvarBeneficiario3
End Property

Public Property Let Beneficiario4(ByVal vData As String)
    mvarBeneficiario4 = vData
End Property

Public Property Get Beneficiario4() As String
    Beneficiario4 = mvarBeneficiario4
End Property

Public Property Let Beneficiario5(ByVal vData As String)
    mvarBeneficiario5 = vData
End Property

Public Property Get Beneficiario5() As String
    Beneficiario5 = mvarBeneficiario5
End Property

Public Property Let Beneficiario6(ByVal vData As String)
    mvarBeneficiario6 = vData
End Property

Public Property Get Beneficiario6() As String
    Beneficiario6 = mvarBeneficiario6
End Property

Public Property Let Beneficiario7(ByVal vData As String)
    mvarBeneficiario7 = vData
End Property

Public Property Get Beneficiario7() As String
    Beneficiario7 = mvarBeneficiario7
End Property

Public Property Let TipoPendencia(ByVal vData As Integer)
    mvarTipoPendencia = vData
End Property

Public Property Get TipoPendencia() As Integer
    TipoPendencia = mvarTipoPendencia
End Property

Public Property Let MotivoPendencia(ByVal vData As String)
    mvarMotivoPendencia = vData
End Property

Public Property Get MotivoPendencia() As String
    MotivoPendencia = mvarMotivoPendencia
End Property

Public Property Let TextoBeneficiario(ByVal vData As String)
    mvarTextoBeneficiario = vData
End Property

Public Property Get TextoBeneficiario() As String
    TextoBeneficiario = mvarTextoBeneficiario
End Property

Public Property Let Premio(ByVal vData As Currency)
    mvarPremio = vData
End Property

Public Property Get Premio() As Currency
    Premio = mvarPremio
End Property

