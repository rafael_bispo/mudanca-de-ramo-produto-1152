VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Cobertura"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
'local variable(s) to hold property value(s)
Private mvarCodObjSegurado As String    'local copy
Private mvarTpCoberturaId As String    'local copy
Private mvarIS As String    'local copy
Private mvarTaxa As String    'local copy
Private mvarPercDesconto As String    'local copy
Private mvarPremio As String    'local copy
Private mvarPremioLiq As String    'local copy
Private mvarPremioTarifa As String    'local copy
Private mvarFranquia As String    'local copy
Private mvarValIS As String    'local copy
Private mvarDescricao As String    'local copy
Private mvarCodObjSeguradoAnt As String    'local copy
Private mvarDescFranquia As String    'local copy
Private mvarPercFranquia As String    'local copy
Private mvarDtInicioVigencia As String    'local copy
Private mvarDtFimVigencia As String    'local copy
'local variable(s) to hold property value(s)
Private mvarPercBasica As String    'local copy
Private mvarClasse As String    'local copy
'local variable(s) to hold property value(s)
Private mvarLimMinIS As Double    'local copy
Private mvarLimMaxIS As Double    'local copy
Private mvarCarencia As Integer    'local copy
Private mvarLimDiarias As Integer    'local copy
'local variable(s) to hold property value(s)
Private mvarNomeComponente As String    'local copy
'local variable(s) to hold property value(s)
Private mvarTpComponenteId As Integer    'local copy
Public Property Let TpComponenteId(ByVal vData As Integer)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.TpComponenteId = 5
    mvarTpComponenteId = vData
End Property


Public Property Get TpComponenteId() As Integer
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.TpComponenteId
    TpComponenteId = mvarTpComponenteId
End Property



Public Property Let NomeComponente(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.NomeComponente = 5
    mvarNomeComponente = vData
End Property


Public Property Get NomeComponente() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.NomeComponente
    NomeComponente = mvarNomeComponente
End Property



Public Property Let LimDiarias(ByVal vData As Integer)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.LimDiarias = 5
    mvarLimDiarias = vData
End Property


Public Property Get LimDiarias() As Integer
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.LimDiarias
    LimDiarias = mvarLimDiarias
End Property



Public Property Let Carencia(ByVal vData As Integer)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Carencia = 5
    mvarCarencia = vData
End Property


Public Property Get Carencia() As Integer
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Carencia
    Carencia = mvarCarencia
End Property



Public Property Let LimMaxIS(ByVal vData As Double)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.LimMaxIS = 5
    mvarLimMaxIS = vData
End Property


Public Property Get LimMaxIS() As Double
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.LimMaxIS
    LimMaxIS = mvarLimMaxIS
End Property



Public Property Let LimMinIS(ByVal vData As Double)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.LimMinIS = 5
    mvarLimMinIS = vData
End Property


Public Property Get LimMinIS() As Double
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.LimMinIS
    LimMinIS = mvarLimMinIS
End Property



Public Property Let Classe(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Classe = 5
    mvarClasse = vData
End Property


Public Property Get Classe() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Classe
    Classe = mvarClasse
End Property



Public Property Let PercBasica(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.PercBasica = 5
    mvarPercBasica = vData
End Property


Public Property Get PercBasica() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.PercBasica
    PercBasica = mvarPercBasica
End Property




Public Property Let DtFimVigencia(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.DtFimVigencia = 5
    mvarDtFimVigencia = vData
End Property

Public Property Get DtFimVigencia() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.DtFimVigencia
    DtFimVigencia = mvarDtFimVigencia
End Property

Public Property Let DtInicioVigencia(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.DtInicioVigencia = 5
    mvarDtInicioVigencia = vData
End Property

Public Property Get DtInicioVigencia() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.DtInicioVigencia
    DtInicioVigencia = mvarDtInicioVigencia
End Property

Public Property Let DescFranquia(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.DescFranquia = 5
    mvarDescFranquia = vData
End Property

Public Property Get DescFranquia() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.DescFranquia
    DescFranquia = mvarDescFranquia
End Property

Public Property Let PercFranquia(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.PercFranquia = 5
    mvarPercFranquia = vData
End Property

Public Property Get PercFranquia() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.PercFranquia
    PercFranquia = mvarPercFranquia
End Property

Public Property Let CodObjSeguradoAnt(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.CodObjSeguradoAnt = 5
    mvarCodObjSeguradoAnt = vData
End Property

Public Property Get CodObjSeguradoAnt() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.CodObjSeguradoAnt
    CodObjSeguradoAnt = mvarCodObjSeguradoAnt
End Property

Public Property Let Descricao(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Descricao = 5
    mvarDescricao = vData
End Property

Public Property Get Descricao() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Descricao
    Descricao = mvarDescricao
End Property

Public Property Let ValIS(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.ValIS = 5
    mvarValIS = vData
End Property

Public Property Get ValIS() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.ValIS
    ValIS = mvarValIS
End Property

Public Property Let ValFranquia(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Franquia = 5
    mvarFranquia = vData
End Property

Public Property Get ValFranquia() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Franquia
    ValFranquia = mvarFranquia
End Property

Public Property Let ValPremio(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Premio = 5
    mvarPremio = vData
End Property

Public Property Get ValPremio() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Premio
    ValPremio = mvarPremio
End Property


Public Property Let ValPremioLiq(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.ValPremioLiq = 5
    mvarPremioLiq = vData
End Property
Public Property Get ValPremioLiq() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Premio
    ValPremioLiq = mvarPremioLiq
End Property

Public Property Let ValPremioTarifa(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.ValPremioTarifa = 5
    mvarPremioTarifa = vData
End Property
Public Property Get ValPremioTarifa() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Premio
    ValPremioTarifa = mvarPremioTarifa
End Property


Public Property Let PercDesconto(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.PercDesconto = 5
    mvarPercDesconto = vData
End Property

Public Property Get PercDesconto() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.PercDesconto
    PercDesconto = mvarPercDesconto
End Property

Public Property Let ValTaxa(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Taxa = 5
    mvarTaxa = vData
End Property

Public Property Get ValTaxa() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Taxa
    ValTaxa = mvarTaxa
End Property

Public Property Let TpCoberturaId(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.TpCoberturaId = 5
    mvarTpCoberturaId = vData
End Property

Public Property Get TpCoberturaId() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.TpCoberturaId
    TpCoberturaId = mvarTpCoberturaId
End Property

Public Property Let CodObjSegurado(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.CodObjSegurado = 5
    mvarCodObjSegurado = vData
End Property

Public Property Get CodObjSegurado() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.CodObjSegurado
    CodObjSegurado = mvarCodObjSegurado
End Property

