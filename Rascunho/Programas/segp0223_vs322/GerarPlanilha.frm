VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Begin VB.Form GerarPlanilha 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Gerar Planilha de Subgrupos"
   ClientHeight    =   3105
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   4680
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3105
   ScaleWidth      =   4680
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin MSComDlg.CommonDialog ComDialog 
      Left            =   120
      Top             =   2400
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "C&ancelar"
      Height          =   495
      Left            =   2760
      TabIndex        =   4
      Top             =   2280
      Width           =   1215
   End
   Begin VB.CommandButton cmdConfirmar 
      Caption         =   "C&onfirmar"
      Height          =   495
      Left            =   720
      TabIndex        =   3
      Top             =   2280
      Width           =   1215
   End
   Begin VB.OptionButton optTodosSubgrupos 
      Caption         =   "Gerar planilha com dados de todos os subgrupos pertencentes � Ap�lice"
      Height          =   615
      Left            =   240
      TabIndex        =   2
      Top             =   1320
      Width           =   4335
   End
   Begin VB.OptionButton optSubgrupoSelecionado 
      Caption         =   "Gerar planilha com dados do subgrupo selecionado"
      Height          =   495
      Left            =   240
      TabIndex        =   1
      Top             =   720
      Width           =   4335
   End
   Begin VB.Label lblTitulo 
      Alignment       =   2  'Center
      Caption         =   "Selecione a op��o desejada:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   4455
   End
End
Attribute VB_Name = "GerarPlanilha"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim sPath As String
Dim newrdocn As New Connection
Dim rc As New Recordset
Dim dt_sistema As Date


Private Sub Command1_Click()
    ComDialog.ShowSave
End Sub

Private Sub cmdCancelar_Click()
    Unload Me
End Sub

Private Sub cmdConfirmar_Click()

    If optSubgrupoSelecionado.Value = True And ConPropBasicaVida.cboSubGrupo(8).Text = "" Then
        MsgBox ("Por favor Selecione um subgrupo para gerar o arquivo")
    Else
        Call Gera_Arquivo_Excel
        Unload Me
    End If

End Sub

Private Sub Gera_Arquivo_Excel_OLD()
    Dim rc As rdoResultset
    Dim SQL As String
    Dim cNomeArquivo As String
    Dim nomearq As String
    Dim cpath As String
    Dim dataMovimento As String
    Dim n As Integer
    Dim exclApp As Object
    Dim exclBook As Object
    Dim exclSheet As Object
    Dim exclSheet2 As Object
    Dim exclSheet3 As Object
    Dim SubGrupos As String

    'Carregar o Excel:
    Set exclApp = CreateObject("Excel.Application")
    'Crie um WorkBook:
    Set exclBook = exclApp.Workbooks.Add

    'Defina a planilha ativa p/ facilitar o trabalho:
    Set exclSheet = exclApp.ActiveWorkBook.ActiveSheet
    exclSheet.name = "Header"

    MousePointer = vbHourglass    ' Muda o ponteiro do mouse

    On Error GoTo Erro

    dt_sistema = Data_Sistema

    dataMovimento = Format$(dt_sistema, "yyyymmdd")

    nomearq = "SEGP0244_" & vApoliceId & "_" & vSubGrupoId & "_" & dataMovimento

    cpath = "\\SISAB007\USERS\DITEC\geper\rotinas\manuten��o de apolices"

    cNomeArquivo = cpath & nomearq


    If optTodosSubgrupos.Value = True Then

        SQL = "  SELECT DISTINCT sub_grupo_apolice_tb.sub_grupo_id " & vbNewLine
        SQL = SQL & " FROM sub_grupo_apolice_tb    WITH (NOLOCK) " & vbNewLine
        SQL = SQL & " WHERE sub_grupo_apolice_tb.apolice_id              = " & vApoliceId & vbNewLine
        SQL = SQL & " AND sub_grupo_apolice_tb.sucursal_seguradora_id    = " & vSucursalId & vbNewLine
        SQL = SQL & " AND sub_grupo_apolice_tb.seguradora_cod_susep      = " & vSeguradoraId & vbNewLine
        SQL = SQL & " AND sub_grupo_apolice_tb.ramo_id                   = " & vRamoId & vbNewLine
        SQL = SQL & " ORDER BY sub_grupo_apolice_tb.sub_grupo_id"

        Set rc = rdocn.OpenResultset(SQL)

        While Not rc.EOF
            SubGrupos = SubGrupos & "," & rc!sub_grupo_id
            rc.MoveNext
        Wend

        'RETIRA A PRIMEIRA VIRGULA
        SubGrupos = Right(SubGrupos, Len(SubGrupos) - 1)

        rc.Close
        Set rc = Nothing

    End If


    'MONTANDO O HEADER DO ARQUIVO
    Dim Header As String
    Dim sequencial As Integer
    Dim num_remessa As Integer
    Dim estipulante As String

    '*************busca o grupo
    SQL = ""
    SQL = "SELECT representacao_sub_grupo_tb.sub_grupo_id" & vbNewLine
    SQL = SQL & " , est_cliente_id" & vbNewLine
    SQL = SQL & " , CLIENTE_TB.nome" & vbNewLine
    SQL = SQL & ", dt_inicio_representacao " & vbNewLine
    SQL = SQL & " into #GrupoSEGP0223" & vbNewLine
    SQL = SQL & "FROM representacao_sub_grupo_tb  with (nolock)                  " & vbNewLine
    SQL = SQL & " INNER JOIN      cliente_tb with  (nolock)                     " & vbNewLine
    SQL = SQL & " ON cliente_id = est_cliente_id                            " & vbNewLine
    SQL = SQL & " WHERE representacao_sub_grupo_tb.apolice_id           = " & vApoliceId & vbNewLine
    SQL = SQL & " AND representacao_sub_grupo_tb.sucursal_seguradora_id = " & vSucursalId & vbNewLine
    SQL = SQL & " AND representacao_sub_grupo_tb.seguradora_cod_susep   = " & vSeguradoraId & vbNewLine
    SQL = SQL & " AND representacao_sub_grupo_tb.ramo_id                = " & vRamoId & vbNewLine
    SQL = SQL & " AND ( dt_fim_representacao                            is null " & vbNewLine
    SQL = SQL & "     OR dt_fim_representacao                           >= '" & dt_inicio_query & "')" & vbNewLine & vbNewLine

    '************busca o estipulante
    SQL = SQL & " SELECT adm_sub_grupo_apolice_tb.sub_grupo_id" & vbNewLine
    SQL = SQL & " , adm_cliente_id" & vbNewLine
    SQL = SQL & " , #GrupoSEGP0223.est_cliente_id" & vbNewLine
    SQL = SQL & " , cliente_tb.nome as nome_estipulante" & vbNewLine
    SQL = SQL & " , #GrupoSEGP0223.nome as nome_grupo" & vbNewLine
    SQL = SQL & " , perc_pro_labore" & vbNewLine
    SQL = SQL & " , dt_inicio_administracao " & vbNewLine
    SQL = SQL & "FROM adm_sub_grupo_apolice_tb with (nolock)  " & vbNewLine
    SQL = SQL & " INNER JOIN apolice_tb with(nolock)" & vbNewLine
    SQL = SQL & "         ON apolice_tb.apolice_id = adm_sub_grupo_apolice_tb.adm_cliente_id " & vbNewLine
    SQL = SQL & " INNER JOIN proposta_tb with(nolock)" & vbNewLine
    SQL = SQL & "         ON proposta_tb.proposta_id = apolice_tb.proposta_id " & vbNewLine
    SQL = SQL & " INNER JOIN cliente_tb with(nolock)" & vbNewLine
    SQL = SQL & "         ON cliente_tb.cliente_id = proposta_tb.prop_cliente_id " & vbNewLine
    SQL = SQL & " INNER JOIN  #GrupoSEGP0223 with(nolock)" & vbNewLine
    SQL = SQL & " on  adm_sub_grupo_apolice_tb.sub_grupo_id = #GrupoSEGP0223.sub_grupo_id" & vbNewLine
    SQL = SQL & " AND adm_sub_grupo_apolice_tb.est_cliente_id = #GrupoSEGP0223.est_cliente_id" & vbNewLine
    SQL = SQL & "WHERE adm_sub_grupo_apolice_tb.apolice_id = " & vApoliceId & vbNewLine
    SQL = SQL & " AND adm_sub_grupo_apolice_tb.sucursal_seguradora_id = " & vSucursalId & vbNewLine
    SQL = SQL & " AND adm_sub_grupo_apolice_tb.seguradora_cod_susep = " & vSeguradoraId & vbNewLine
    SQL = SQL & " AND adm_sub_grupo_apolice_tb.ramo_id = " & vRamoId & vbNewLine
    SQL = SQL & " AND dt_fim_administracao IS NULL " & vbNewLine
    SQL = SQL & " DROP TABLE #GrupoSEGP0223"


    Set rc = rdocn.OpenResultset(SQL)

    If (rc!nome_estipulante = "") Then
        estipulante = ""
    Else
        estipulante = rc!nome_estipulante
    End If

    rc.Close
    Set rc = Nothing


    SQL = ""
    SQL = SQL & " set ANSI_NULLS off " & vbNewLine & vbNewLine
    SQL = SQL & "CREATE TABLE #Planilha " & vbNewLine
    SQL = SQL & "(ID INT IDENTITY(1,1), " & vbNewLine
    SQL = SQL & "cliente_id INT,  " & vbNewLine
    SQL = SQL & "sub_grupo_id INT,  " & vbNewLine
    SQL = SQL & "Inicio_de_cobertura NVARCHAR(14),  " & vbNewLine
    SQL = SQL & "dt_entrada_sub_grupo NVARCHAR(14),  " & vbNewLine
    SQL = SQL & "nome NVARCHAR(100),  " & vbNewLine
    SQL = SQL & "cpf CHAR(20),  " & vbNewLine
    SQL = SQL & "sexo CHAR(20),  " & vbNewLine
    SQL = SQL & "dt_nascimento NVARCHAR(20),  " & vbNewLine
    SQL = SQL & "estado_civil NVARCHAR(15),  " & vbNewLine
    SQL = SQL & "proposta_id INT,  " & vbNewLine
    SQL = SQL & "prop_cliente_id INT,  " & vbNewLine
    SQL = SQL & "endereco_principal NVARCHAR(100),  " & vbNewLine
    SQL = SQL & "bairro_principal NVARCHAR(50),  " & vbNewLine
    SQL = SQL & "municipio NVARCHAR(50),  " & vbNewLine
    SQL = SQL & "cep NVARCHAR(20),  " & vbNewLine
    SQL = SQL & "estado NVARCHAR(5),  " & vbNewLine
    SQL = SQL & "e_mail NVARCHAR(50),  " & vbNewLine
    SQL = SQL & "ddd_principal NVARCHAR(5),  " & vbNewLine
    SQL = SQL & "telefone_principal NVARCHAR(20),  " & vbNewLine
    SQL = SQL & "Nome_Conjuge NVARCHAR(100),  " & vbNewLine
    SQL = SQL & "cfp_conjuge NVARCHAR(20),  " & vbNewLine
    SQL = SQL & "dt_nasc_conjuge NVARCHAR(14),  " & vbNewLine
    SQL = SQL & "Sexo_conjuge NVARCHAR(10),  " & vbNewLine
    SQL = SQL & "Inicio_de_cobertura_do_conjuge NVARCHAR(16),  " & vbNewLine
    SQL = SQL & "val_salario NVARCHAR(20),  " & vbNewLine
    SQL = SQL & "val_capital_segurado NVARCHAR(20),  " & vbNewLine
    SQL = SQL & "dt_inicio_vigencia_sbg NVARCHAR(11),  " & vbNewLine
    SQL = SQL & "dt_fim_vigencia_sbg NVARCHAR(11),  " & vbNewLine
    SQL = SQL & "situacao NVARCHAR(10), " & vbNewLine
    SQL = SQL & "Beneficiario1 NVARCHAR(100),  " & vbNewLine
    SQL = SQL & "Beneficiario2 NVARCHAR(100),  " & vbNewLine
    SQL = SQL & "Beneficiario3 NVARCHAR(100),  " & vbNewLine
    SQL = SQL & "Beneficiario4 NVARCHAR(100),  " & vbNewLine
    SQL = SQL & "Beneficiario5 NVARCHAR(100),  " & vbNewLine
    SQL = SQL & "Beneficiario6 NVARCHAR(100),  " & vbNewLine
    SQL = SQL & "Beneficiario7 NVARCHAR(100),  " & vbNewLine
    SQL = SQL & "Descricao1 NVARCHAR(100),  " & vbNewLine
    SQL = SQL & "Descricao2 NVARCHAR(100),  " & vbNewLine
    SQL = SQL & "Descricao3 NVARCHAR(100),  " & vbNewLine
    SQL = SQL & "Descricao4 NVARCHAR(100),  " & vbNewLine
    SQL = SQL & "Descricao5 NVARCHAR(100) ) " & vbNewLine & vbNewLine

    SQL = SQL & " INSERT INTO #Planilha ( " & vbNewLine
    SQL = SQL & " cliente_id, sub_grupo_id, In�cio_de_cobertura, dt_entrada_sub_grupo, nome, cpf, sexo, dt_nascimento, estado_civil, proposta_id, " & vbNewLine
    SQL = SQL & " prop_cliente_id, endereco_principal, bairro_principal, municipio, cep, estado, e_mail, ddd_principal, " & vbNewLine
    SQL = SQL & " telefone_principal , Nome_Conjuge, cfp_conjuge, dt_nasc_conjuge, Sexo_conjuge, In�cio_de_cobertura_do_c�njuge, " & vbNewLine
    SQL = SQL & " val_salario, val_capital_segurado, dt_inicio_vigencia_sbg, dt_fim_vigencia_sbg, situacao,  " & vbNewLine
    SQL = SQL & " Beneficiario1, Beneficiario2, Beneficiario3, Beneficiario4, Beneficiario5, Beneficiario6, Beneficiario7, " & vbNewLine
    SQL = SQL & " Descricao1, Descricao2, Descricao3, Descricao4, Descricao5) " & vbNewLine & vbNewLine


    SQL = SQL & " SELECT distinct(cl.cliente_id), " & vbNewLine
    SQL = SQL & " sgp.sub_grupo_id, CONVERT(VARCHAR,sgp.dt_inicio_vigencia_seg,103) as Inicio_de_cobertura, " & vbNewLine
    SQL = SQL & " CONVERT(VARCHAR,sgp.dt_entrada_sub_grupo,103) as dt_entrada_sub_grupo , cl.nome,   " & vbNewLine
    SQL = SQL & " pf.cpf, pf.sexo as sexo,  " & vbNewLine
    SQL = SQL & " CONVERT(VARCHAR,pf.dt_nascimento,103) as dt_nascimento,  " & vbNewLine
    SQL = SQL & " pf.estado_civil as estado_civil,  " & vbNewLine
    SQL = SQL & " sgp.proposta_id, sgp.prop_cliente_id, LTRIM(RTRIM(emi.endereco))as endereco_principal, LTRIM(RTRIM(emi.bairro))as  bairro_principal,  " & vbNewLine
    SQL = SQL & " emi.municipio as municipio, emi.cep, emi.estado, cl.e_mail, LTRIM(RTRIM(cl.ddd_1)) as ddd_principal,   " & vbNewLine
    SQL = SQL & " LTRIM(RTRIM(cl.telefone_1))as telefone_principal, conjuge.nome as nome_conjuge, conjuge.cpf_cnpj as cfp_conjuge,  " & vbNewLine
    SQL = SQL & " CONVERT(VARCHAR,pf_conjuge.dt_nascimento,103) as dt_nasc_conjuge,  " & vbNewLine
    SQL = SQL & " pf_conjuge.sexo as sexo_conjuge,  " & vbNewLine
    SQL = SQL & " CONVERT(VARCHAR,vsg.dt_inicio_vigencia_seg,103) as In�cio_de_cobertura_do_c�njuge, ISNULL(sgp.val_salario, 0) val_salario,  " & vbNewLine
    SQL = SQL & " ISNULL(sgp.val_capital_segurado, 0) val_capital_segurado, ISNULL(CONVERT(VARCHAR,sgp.dt_inicio_vigencia_sbg,103), '') dt_inicio_vigencia_sbg,  " & vbNewLine
    SQL = SQL & " ISNULL(CONVERT(VARCHAR,sgp.dt_fim_vigencia_sbg,103), '') dt_fim_vigencia_sbg,  " & vbNewLine
    SQL = SQL & " situacao = CASE WHEN sgp.dt_fim_vigencia_sbg Is Null THEN 'Ativa' Else 'Inativa' END,  " & vbNewLine
    SQL = SQL & " beneficiario1.nome as beneficiario1, beneficiario2.nome as beneficiario2, beneficiario3.nome as beneficiario3,  " & vbNewLine
    SQL = SQL & " beneficiario4.nome as beneficiario4, beneficiario5.nome as beneficiario5, beneficiario6.nome as beneficiario6, beneficiario7.nome as beneficiario7,  " & vbNewLine
    SQL = SQL & " null as Descricao1, null as Descricao2, null as Descricao3, null as Descricao4, null as Descricao5 " & vbNewLine
    SQL = SQL & " FROM seguro_vida_sub_grupo_tb sgp WITH (NOLOCK)  " & vbNewLine
    SQL = SQL & " INNER JOIN cliente_tb cl   WITH (NOLOCK) ON sgp.prop_cliente_id = cl.cliente_id   " & vbNewLine
    SQL = SQL & " INNER JOIN pessoa_fisica_tb pf   WITH (NOLOCK) ON sgp.prop_cliente_id = pf.pf_cliente_id  " & vbNewLine
    SQL = SQL & " LEFT JOIN emi_proposta_tb emi WITH (NOLOCK) ON cl.cpf_cnpj = emi.cpf  " & vbNewLine
    SQL = SQL & " LEFT JOIN endereco_cliente_tb ec   WITH (NOLOCK) ON sgp.prop_cliente_id = ec.cliente_id  " & vbNewLine
    SQL = SQL & " LEFT JOIN seguro_vida_sub_grupo_tb vsg WITH (NOLOCK) ON sgp.sub_grupo_id = vsg.sub_grupo_id AND sgp.proposta_id = vsg.proposta_id AND sgp.prop_cliente_id = vsg.prop_cliente_id AND vsg.tp_componente_id = 3 -- conjuge   " & vbNewLine
    SQL = SQL & " LEFT JOIN cliente_tb conjuge   WITH (NOLOCK) ON vsg.prop_cliente_id = conjuge.cliente_id  " & vbNewLine
    SQL = SQL & " LEFT JOIN pessoa_fisica_tb pf_conjuge   WITH (NOLOCK) ON vsg.prop_cliente_id = pf_conjuge.pf_cliente_id  " & vbNewLine
    SQL = SQL & " LEFT JOIN beneficiario_tb beneficiario1   WITH (NOLOCK) ON sgp.proposta_id = beneficiario1.proposta_id AND beneficiario1.seq_beneficiario = 1  " & vbNewLine
    SQL = SQL & " LEFT JOIN beneficiario_tb beneficiario2   WITH (NOLOCK) ON sgp.proposta_id = beneficiario2.proposta_id AND beneficiario2.seq_beneficiario = 2  " & vbNewLine
    SQL = SQL & " LEFT JOIN beneficiario_tb beneficiario3   WITH (NOLOCK) ON sgp.proposta_id = beneficiario3.proposta_id AND beneficiario3.seq_beneficiario = 3  " & vbNewLine
    SQL = SQL & " LEFT JOIN beneficiario_tb beneficiario4   WITH (NOLOCK) ON sgp.proposta_id = beneficiario4.proposta_id AND beneficiario4.seq_beneficiario = 4  " & vbNewLine
    SQL = SQL & " LEFT JOIN beneficiario_tb beneficiario5   WITH (NOLOCK) ON sgp.proposta_id = beneficiario5.proposta_id AND beneficiario5.seq_beneficiario = 5  " & vbNewLine
    SQL = SQL & " LEFT JOIN beneficiario_tb beneficiario6   WITH (NOLOCK) ON sgp.proposta_id = beneficiario6.proposta_id AND beneficiario6.seq_beneficiario = 6  " & vbNewLine
    SQL = SQL & " LEFT JOIN beneficiario_tb beneficiario7   WITH (NOLOCK) ON sgp.proposta_id = beneficiario7.proposta_id AND beneficiario7.seq_beneficiario = 7  " & vbNewLine
    If optTodosSubgrupos.Value = True Then
        SQL = SQL & " WHERE sgp.sub_grupo_id IN (" & SubGrupos & ")" & vbNewLine
    Else
        SQL = SQL & " WHERE sgp.sub_grupo_id = " & vSubGrupoId & vbNewLine
    End If
    SQL = SQL & " AND sgp.apolice_id = " & vApoliceId & " " & vbNewLine
    SQL = SQL & " AND sgp.sucursal_seguradora_id = " & vSucursalId & "" & vbNewLine
    SQL = SQL & " AND sgp.seguradora_cod_susep = " & vSeguradoraId & "" & vbNewLine
    SQL = SQL & " AND sgp.ramo_id = " & vRamoId & "" & vbNewLine
    SQL = SQL & " AND sgp.dt_fim_vigencia_sbg Is Null" & vbNewLine
    SQL = SQL & " AND sgp.dt_inicio_vigencia_sbg = (Select TOP 1 sgp2.dt_inicio_vigencia_sbg FROM seguro_vida_sub_grupo_tb sgp2   WITH (NOLOCK) " & vbNewLine
    SQL = SQL & "                                                                            WHERE sgp2.apolice_id = sgp.apolice_id " & vbNewLine
    SQL = SQL & "                                                                              AND sgp2.sucursal_seguradora_id = sgp.sucursal_seguradora_id " & vbNewLine
    SQL = SQL & "                                                                              AND sgp2.seguradora_cod_susep = sgp.seguradora_cod_susep" & vbNewLine
    SQL = SQL & "                                                                              AND sgp2.ramo_id = sgp.ramo_id" & vbNewLine
    SQL = SQL & "                                                                              AND sgp2.sub_grupo_id = sgp.sub_grupo_id" & vbNewLine
    SQL = SQL & "                                                                              AND sgp2.dt_inicio_vigencia_apol_sbg = sgp.dt_inicio_vigencia_apol_sbg" & vbNewLine
    SQL = SQL & "                                                                              AND sgp2.proposta_id = sgp.proposta_id" & vbNewLine
    SQL = SQL & "                                                                              AND sgp2.prop_cliente_id = sgp.prop_cliente_id" & vbNewLine
    SQL = SQL & "                                                                              AND sgp2.tp_componente_id = sgp.tp_componente_id" & vbNewLine
    SQL = SQL & "                                                                              AND sgp2.seq_canc_endosso_seg = sgp.seq_canc_endosso_seg" & vbNewLine
    SQL = SQL & "                                                                            ORDER BY sgp2.dt_inicio_vigencia_sbg DESC)" & vbNewLine

    SQL = SQL & " GROUP BY cl.cliente_id,  " & vbNewLine
    SQL = SQL & " sgp.sub_grupo_id , sgp.dt_inicio_vigencia_seg, sgp.dt_entrada_sub_grupo, cl.Nome, pf.CPF, pf.Sexo, pf.dt_nascimento, pf.estado_civil, sgp.proposta_id,  " & vbNewLine
    SQL = SQL & " sgp.prop_cliente_id, emi.Endereco, emi.Bairro, emi.Municipio, emi.CEP, emi.estado, cl.e_mail, cl.ddd_1,  " & vbNewLine
    SQL = SQL & " cl.telefone_1 , Conjuge.Nome, Conjuge.CPF_CNPJ, pf_conjuge.dt_nascimento, pf_conjuge.Sexo, vsg.dt_inicio_vigencia_seg,  " & vbNewLine
    SQL = SQL & " sgp.val_salario, sgp.val_capital_segurado, sgp.dt_inicio_vigencia_sbg,  " & vbNewLine
    SQL = SQL & " sgp.dt_fim_vigencia_sbg , sgp.dt_fim_vigencia_sbg, sgp.tp_pendencia_id, sgp.motivo_pendencia,  " & vbNewLine
    SQL = SQL & " Beneficiario1.Nome, Beneficiario2.Nome, Beneficiario3.Nome, Beneficiario4.Nome, Beneficiario5.Nome, Beneficiario6.Nome, Beneficiario7.Nome  " & vbNewLine
    SQL = SQL & " ORDER BY cl.Nome  " & vbNewLine & vbNewLine

    SQL = SQL & " DELETE A  " & vbNewLine
    SQL = SQL & " FROM #Planilha A " & vbNewLine
    SQL = SQL & " JOIN #Planilha B " & vbNewLine
    SQL = SQL & " ON A.cliente_id = B.cliente_id " & vbNewLine
    SQL = SQL & "where A.cliente_id = B.cliente_id  " & vbNewLine
    SQL = SQL & "and a.ID < b.ID " & vbNewLine & vbNewLine

    SQL = SQL & "SELECT cliente_id, sub_grupo_id, Inicio_de_cobertura,dt_entrada_sub_grupo, nome, cpf, sexo, dt_nascimento, estado_civil, " & vbNewLine
    SQL = SQL & "       proposta_id, prop_cliente_id, endereco_principal, bairro_principal, municipio, cep, estado, e_mail, ddd_principal, " & vbNewLine
    SQL = SQL & "       telefone_principal , Nome_Conjuge, cfp_conjuge, dt_nasc_conjuge, Sexo_conjuge, Inicio_de_cobertura_do_conjuge," & vbNewLine
    SQL = SQL & "       val_salario, val_capital_segurado, dt_inicio_vigencia_sbg, dt_fim_vigencia_sbg, " & vbNewLine
    SQL = SQL & "       Beneficiario1, Beneficiario2, Beneficiario3, Beneficiario4, Beneficiario5, Beneficiario6, Beneficiario7, Descricao1, Descricao2, Descricao3, Descricao4, Descricao5 " & vbNewLine
    SQL = SQL & "  FROM #Planilha " & vbNewLine
    SQL = SQL & "  ORDER BY sub_grupo_id " & vbNewLine
    SQL = SQL & " DROP TABLE #Planilha"


    Set rc = rdocn.OpenResultset(SQL)


    'sequencial inicial
    sequencial = 1


    'escrevendo no arquivo
    n = 2
    'ABA HEADER
    With exclSheet
        .cells(n, 1).numberformat = "@"
        .cells(n, 1).Value = "000000"
        .cells(n, 2).numberformat = "@"
        .cells(n, 2).Value = "SEGA4000"
        .cells(n, 3).numberformat = "@"
        .cells(n, 3).Value = Right("00" & vRamoId, 2)
        .cells(n, 4).numberformat = "@"
        .cells(n, 4).Value = Right("000000000" & vApoliceId, 9)
        If optTodosSubgrupos.Value = True Then
            .cells(n, 5).numberformat = "@"
            .cells(n, 5).Value = "0"
        Else
            .cells(n, 5).numberformat = "@"
            .cells(n, 5).Value = Right("000" & vSubGrupoId, 3)
        End If
        .cells(n, 6).numberformat = "@"
        .cells(n, 6).Value = Right("00" & Month(CDate(Date)), 2) & "/" & Year(CDate(Date))
        .cells(n, 7).numberformat = "@"
        .cells(n, 7).Value = estipulante
    End With


    Set exclSheet2 = exclApp.ActiveWorkBook.Worksheets(2)
    exclSheet2.name = "Detalhe"

    Do While Not rc.EOF

        'Definir o conte�do das c�lulas:
        With exclSheet2
            .cells(n, 1).Value = sequencial
            .cells(n, 2).Value = 10
            .cells(n, 3).Value = Format$(rc!Inicio_de_cobertura, "dd/MM/YYYY")
            .cells(n, 4).Value = Format$(rc!dt_entrada_sub_grupo, "dd/MM/YYYY")
            .cells(n, 5).Value = rc!Nome
            .cells(n, 6).Value = rc!CPF
            .cells(n, 7).Value = Format$(rc!dt_nascimento, "dd/MM/YYYY")
            .cells(n, 8).Value = rc!Sexo
            .cells(n, 9).Value = rc!estado_civil
            .cells(n, 10).Value = rc!val_capital_segurado
            .cells(n, 11).Value = rc!val_salario
            .cells(n, 12).Value = rc!endereco_principal
            .cells(n, 13).Value = rc!bairro_principal
            .cells(n, 14).Value = rc!Municipio
            .cells(n, 15).Value = rc!CEP
            .cells(n, 16).Value = rc!Estado
            .cells(n, 17).Value = rc!e_mail
            .cells(n, 18).Value = rc!ddd_principal
            .cells(n, 19).Value = rc!telefone_principal
            .cells(n, 20).Value = rc!Nome_Conjuge
            .cells(n, 21).Value = rc!cfp_conjuge
            .cells(n, 22).Value = Format$(rc!dt_nasc_conjuge, "dd/MM/YYYY")
            .cells(n, 23).Value = rc!Sexo_conjuge
            .cells(n, 24).Value = Format$(rc!Inicio_de_cobertura_do_conjuge, "dd/MM/YYYY")
            .cells(n, 25).Value = rc!Beneficiario1
            .cells(n, 26).Value = rc!Beneficiario2
            .cells(n, 27).Value = rc!Beneficiario3
            .cells(n, 28).Value = rc!Beneficiario4
            .cells(n, 29).Value = rc!Beneficiario5
            .cells(n, 30).Value = rc!Beneficiario6
            .cells(n, 31).Value = rc!Beneficiario7
            .cells(n, 32).Value = rc!Descricao1
            .cells(n, 33).Value = rc!Descricao2
            .cells(n, 34).Value = rc!Descricao3
            .cells(n, 35).Value = rc!Descricao4
            .cells(n, 36).Value = rc!Descricao5
            .cells(n, 37).Value = rc!sub_grupo_id

            sequencial = sequencial + 1

            rc.MoveNext
            n = n + 1
        End With
    Loop

    'MONTANDO O TRAILER DO ARQUIVO
    Set exclSheet3 = exclApp.ActiveWorkBook.Worksheets(3)
    exclSheet3.name = "Trailer"

    n = 2

    With exclSheet3
        .cells(n, 1).Value = "999999"
        .cells(n, 4).Value = sequencial - 1
    End With

    'Salvar o Arquivo
    exclSheet.SaveAs cNomeArquivo

    MousePointer = vbDefault  ' Muda o ponteiro do mouse
    MsgBox ("Arquivo gerado com sucesso!")


    'Limpe as vari�veis de Objeto:
    exclApp.Workbooks.Close
    exclApp.Quit
    Set exclSheet = Nothing
    Set exclBook = Nothing
    Set exclApp = Nothing


    rc.Close

    Exit Sub

Erro:
    TrataErroGeral "Gera_Arquivo_Excel", Me.name
    Unload Me
    TerminaSEGBR
End Sub

Private Sub Gera_Arquivo_Excel()
    Dim rc As rdoResultset
    Dim SQL As String
    Dim cNomeArquivo As String
    Dim nomearq As String
    Dim cpath As String
    Dim dataMovimento As String
    Dim n As Long
    Dim exclApp As Object
    Dim exclBook As Object
    Dim exclSheet As Object
    Dim SubGrupos As String


    'Carregar o Excel:
    Set exclApp = CreateObject("Excel.Application")
    'Crie um WorkBook:
    Set exclBook = exclApp.Workbooks.Add

    'Defina a planilha ativa p/ facilitar o trabalho:
    Set exclSheet = exclApp.ActiveWorkBook.ActiveSheet

    MousePointer = vbHourglass    ' Muda o ponteiro do mouse
    Screen.MousePointer = 11

    On Error GoTo Erro

    dt_sistema = Data_Sistema

    dataMovimento = Format$(dt_sistema, "yyyymmdd")

    nomearq = "SEGP0244_" & vApoliceId & "_" & vSubGrupoId & "_" & dataMovimento

    'cpath = "C:\" 'teste
    cpath = "\\SISAB007\USERS\DITEC\geper\rotinas\manuten��o de apolices"
	cpath = cpath & "\"    'DIEGO SILVA - CONFITEC-IM01194162

    cNomeArquivo = cpath & nomearq


    If optTodosSubgrupos.Value = True Then

        SQL = "  SELECT DISTINCT sub_grupo_apolice_tb.sub_grupo_id " & vbNewLine
        SQL = SQL & " FROM sub_grupo_apolice_tb    WITH (NOLOCK) " & vbNewLine
        SQL = SQL & " WHERE sub_grupo_apolice_tb.apolice_id              = " & vApoliceId & vbNewLine
        SQL = SQL & " AND sub_grupo_apolice_tb.sucursal_seguradora_id    = " & vSucursalId & vbNewLine
        SQL = SQL & " AND sub_grupo_apolice_tb.seguradora_cod_susep      = " & vSeguradoraId & vbNewLine
        SQL = SQL & " AND sub_grupo_apolice_tb.ramo_id                   = " & vRamoId & vbNewLine
        SQL = SQL & " ORDER BY sub_grupo_apolice_tb.sub_grupo_id"

        Set rc = rdocn.OpenResultset(SQL)

        While Not rc.EOF
            SubGrupos = SubGrupos & "," & rc!sub_grupo_id
            rc.MoveNext
        Wend

        'RETIRA A PRIMEIRA VIRGULA
        SubGrupos = Right(SubGrupos, Len(SubGrupos) - 1)

        rc.Close
        Set rc = Nothing

    End If


    'MONTANDO O HEADER DO ARQUIVO

    Dim sequencial As Long
    Dim estipulante As String

    '*************busca o grupo
    SQL = ""
    SQL = "SELECT representacao_sub_grupo_tb.sub_grupo_id" & vbNewLine
    SQL = SQL & " , est_cliente_id" & vbNewLine
    SQL = SQL & " , CLIENTE_TB.nome" & vbNewLine
    SQL = SQL & ", dt_inicio_representacao " & vbNewLine
    SQL = SQL & " into #GrupoSEGP0223" & vbNewLine
    SQL = SQL & "FROM representacao_sub_grupo_tb  with (nolock)                  " & vbNewLine
    SQL = SQL & " INNER JOIN      cliente_tb with  (nolock)                     " & vbNewLine
    SQL = SQL & " ON cliente_id = est_cliente_id                            " & vbNewLine
    SQL = SQL & " WHERE representacao_sub_grupo_tb.apolice_id           = " & vApoliceId & vbNewLine
    SQL = SQL & " AND representacao_sub_grupo_tb.sucursal_seguradora_id = " & vSucursalId & vbNewLine
    SQL = SQL & " AND representacao_sub_grupo_tb.seguradora_cod_susep   = " & vSeguradoraId & vbNewLine
    SQL = SQL & " AND representacao_sub_grupo_tb.ramo_id                = " & vRamoId & vbNewLine
    SQL = SQL & " AND ( dt_fim_representacao                            is null " & vbNewLine
    SQL = SQL & "     OR dt_fim_representacao                           >= '" & dt_inicio_query & "')" & vbNewLine & vbNewLine

    '************busca o estipulante
    SQL = SQL & " SELECT adm_sub_grupo_apolice_tb.sub_grupo_id" & vbNewLine
    SQL = SQL & " , adm_cliente_id" & vbNewLine
    SQL = SQL & " , #GrupoSEGP0223.est_cliente_id" & vbNewLine
    SQL = SQL & " , cliente_tb.nome as nome_estipulante" & vbNewLine
    SQL = SQL & " , #GrupoSEGP0223.nome as nome_grupo" & vbNewLine
    SQL = SQL & " , perc_pro_labore" & vbNewLine
    SQL = SQL & " , dt_inicio_administracao " & vbNewLine
    SQL = SQL & "FROM adm_sub_grupo_apolice_tb with (nolock)  " & vbNewLine
    SQL = SQL & " INNER JOIN cliente_tb with(nolock)" & vbNewLine
    SQL = SQL & "         ON cliente_tb.cliente_id = adm_cliente_id " & vbNewLine
    SQL = SQL & " INNER JOIN  #GrupoSEGP0223 with(nolock)" & vbNewLine
    SQL = SQL & " on  adm_sub_grupo_apolice_tb.sub_grupo_id = #GrupoSEGP0223.sub_grupo_id" & vbNewLine
    SQL = SQL & " AND adm_sub_grupo_apolice_tb.est_cliente_id = #GrupoSEGP0223.est_cliente_id" & vbNewLine
    SQL = SQL & "WHERE adm_sub_grupo_apolice_tb.apolice_id = " & vApoliceId & vbNewLine
    SQL = SQL & " AND adm_sub_grupo_apolice_tb.sucursal_seguradora_id = " & vSucursalId & vbNewLine
    SQL = SQL & " AND adm_sub_grupo_apolice_tb.seguradora_cod_susep = " & vSeguradoraId & vbNewLine
    SQL = SQL & " AND adm_sub_grupo_apolice_tb.ramo_id = " & vRamoId & vbNewLine
    SQL = SQL & " AND dt_fim_administracao IS NULL " & vbNewLine
    SQL = SQL & " DROP TABLE #GrupoSEGP0223"


    Set rc = rdocn.OpenResultset(SQL)

    If Not rc.EOF Then

        If (rc!nome_estipulante = "") Then
            estipulante = ""
        Else
            estipulante = rc!nome_estipulante
        End If
    Else
        estipulante = ""
    End If

    rc.Close
    Set rc = Nothing


    SQL = ""
    SQL = SQL & " set ANSI_NULLS off " & vbNewLine & vbNewLine
    SQL = SQL & "CREATE TABLE #Planilha " & vbNewLine
    SQL = SQL & "(ID INT IDENTITY(1,1), " & vbNewLine
    SQL = SQL & "cliente_id INT,  " & vbNewLine
    SQL = SQL & "sub_grupo_id INT,  " & vbNewLine
    SQL = SQL & "Inicio_de_cobertura NVARCHAR(14),  " & vbNewLine
    SQL = SQL & "dt_entrada_sub_grupo NVARCHAR(14),  " & vbNewLine
    SQL = SQL & "nome NVARCHAR(100),  " & vbNewLine
    SQL = SQL & "cpf CHAR(20),  " & vbNewLine
    SQL = SQL & "sexo CHAR(20),  " & vbNewLine
    SQL = SQL & "dt_nascimento NVARCHAR(20),  " & vbNewLine
    SQL = SQL & "estado_civil NVARCHAR(15),  " & vbNewLine
    SQL = SQL & "proposta_id INT,  " & vbNewLine
    SQL = SQL & "prop_cliente_id INT,  " & vbNewLine
    SQL = SQL & "endereco_principal NVARCHAR(100),  " & vbNewLine
    SQL = SQL & "bairro_principal NVARCHAR(50),  " & vbNewLine
    SQL = SQL & "municipio NVARCHAR(50),  " & vbNewLine
    SQL = SQL & "cep NVARCHAR(20),  " & vbNewLine
    SQL = SQL & "estado NVARCHAR(5),  " & vbNewLine
    SQL = SQL & "e_mail NVARCHAR(50),  " & vbNewLine
    SQL = SQL & "ddd_principal NVARCHAR(5),  " & vbNewLine
    SQL = SQL & "telefone_principal NVARCHAR(20),  " & vbNewLine
    SQL = SQL & "Nome_Conjuge NVARCHAR(100),  " & vbNewLine
    SQL = SQL & "cfp_conjuge NVARCHAR(20),  " & vbNewLine
    SQL = SQL & "dt_nasc_conjuge NVARCHAR(14),  " & vbNewLine
    SQL = SQL & "Sexo_conjuge NVARCHAR(10),  " & vbNewLine
    SQL = SQL & "Inicio_de_cobertura_do_conjuge NVARCHAR(16),  " & vbNewLine
    SQL = SQL & "val_salario NVARCHAR(20),  " & vbNewLine
    SQL = SQL & "val_capital_segurado NVARCHAR(20),  " & vbNewLine
    SQL = SQL & "dt_inicio_vigencia_sbg NVARCHAR(11),  " & vbNewLine
    SQL = SQL & "dt_fim_vigencia_sbg NVARCHAR(11),  " & vbNewLine
    SQL = SQL & "situacao NVARCHAR(10), " & vbNewLine
    SQL = SQL & "Beneficiario1 NVARCHAR(100),  " & vbNewLine
    SQL = SQL & "Beneficiario2 NVARCHAR(100),  " & vbNewLine
    SQL = SQL & "Beneficiario3 NVARCHAR(100),  " & vbNewLine
    SQL = SQL & "Beneficiario4 NVARCHAR(100),  " & vbNewLine
    SQL = SQL & "Beneficiario5 NVARCHAR(100),  " & vbNewLine
    SQL = SQL & "Beneficiario6 NVARCHAR(100),  " & vbNewLine
    SQL = SQL & "Beneficiario7 NVARCHAR(100),  " & vbNewLine
    SQL = SQL & "Descricao1 NVARCHAR(100),  " & vbNewLine
    SQL = SQL & "Descricao2 NVARCHAR(100),  " & vbNewLine
    SQL = SQL & "Descricao3 NVARCHAR(100),  " & vbNewLine
    SQL = SQL & "Descricao4 NVARCHAR(100),  " & vbNewLine
    SQL = SQL & "Descricao5 NVARCHAR(100) ) " & vbNewLine & vbNewLine

    SQL = SQL & " INSERT INTO #Planilha ( " & vbNewLine
    SQL = SQL & " cliente_id, sub_grupo_id, In�cio_de_cobertura, dt_entrada_sub_grupo, nome, cpf, sexo, dt_nascimento, estado_civil, proposta_id, " & vbNewLine
    SQL = SQL & " prop_cliente_id, endereco_principal, bairro_principal, municipio, cep, estado, e_mail, ddd_principal, " & vbNewLine
    SQL = SQL & " telefone_principal , Nome_Conjuge, cfp_conjuge, dt_nasc_conjuge, Sexo_conjuge, In�cio_de_cobertura_do_c�njuge, " & vbNewLine
    SQL = SQL & " val_salario, val_capital_segurado, dt_inicio_vigencia_sbg, dt_fim_vigencia_sbg, situacao,  " & vbNewLine
    SQL = SQL & " Beneficiario1, Beneficiario2, Beneficiario3, Beneficiario4, Beneficiario5, Beneficiario6, Beneficiario7, " & vbNewLine
    SQL = SQL & " Descricao1, Descricao2, Descricao3, Descricao4, Descricao5) " & vbNewLine & vbNewLine


    SQL = SQL & " SELECT distinct(cl.cliente_id), " & vbNewLine
    SQL = SQL & " sgp.sub_grupo_id, CONVERT(VARCHAR,sgp.dt_inicio_vigencia_seg,103) as Inicio_de_cobertura, " & vbNewLine
    SQL = SQL & " CONVERT(VARCHAR,sgp.dt_entrada_sub_grupo,103) as dt_entrada_sub_grupo , cl.nome,   " & vbNewLine
    SQL = SQL & " pf.cpf, pf.sexo as sexo,  " & vbNewLine
    SQL = SQL & " CONVERT(VARCHAR,pf.dt_nascimento,103) as dt_nascimento,  " & vbNewLine
    SQL = SQL & " pf.estado_civil as estado_civil,  " & vbNewLine
    SQL = SQL & " sgp.proposta_id, sgp.prop_cliente_id, LTRIM(RTRIM(emi.endereco))as endereco_principal, LTRIM(RTRIM(emi.bairro))as  bairro_principal,  " & vbNewLine
    SQL = SQL & " emi.municipio as municipio, emi.cep, emi.estado, cl.e_mail, LTRIM(RTRIM(cl.ddd_1)) as ddd_principal,   " & vbNewLine
    SQL = SQL & " LTRIM(RTRIM(cl.telefone_1))as telefone_principal, conjuge.nome as nome_conjuge, conjuge.cpf_cnpj as cfp_conjuge,  " & vbNewLine
    SQL = SQL & " CONVERT(VARCHAR,pf_conjuge.dt_nascimento,103) as dt_nasc_conjuge,  " & vbNewLine
    SQL = SQL & " pf_conjuge.sexo as sexo_conjuge,  " & vbNewLine
    SQL = SQL & " CONVERT(VARCHAR,vsg.dt_inicio_vigencia_seg,103) as In�cio_de_cobertura_do_c�njuge, ISNULL(sgp.val_salario, 0) val_salario,  " & vbNewLine
    SQL = SQL & " ISNULL(esg.val_is, sgp.val_capital_segurado) val_is, ISNULL(CONVERT(VARCHAR,sgp.dt_inicio_vigencia_sbg,103), '') dt_inicio_vigencia_sbg,  " & vbNewLine
    SQL = SQL & " ISNULL(CONVERT(VARCHAR,sgp.dt_fim_vigencia_sbg,103), '') dt_fim_vigencia_sbg,  " & vbNewLine
    SQL = SQL & " situacao = CASE WHEN sgp.dt_fim_vigencia_sbg Is Null THEN 'Ativa' Else 'Inativa' END,  " & vbNewLine
    SQL = SQL & " beneficiario1.nome as beneficiario1, beneficiario2.nome as beneficiario2, beneficiario3.nome as beneficiario3,  " & vbNewLine
    SQL = SQL & " beneficiario4.nome as beneficiario4, beneficiario5.nome as beneficiario5, beneficiario6.nome as beneficiario6, beneficiario7.nome as beneficiario7,  " & vbNewLine
    SQL = SQL & " null as Descricao1, null as Descricao2, null as Descricao3, null as Descricao4, null as Descricao5 " & vbNewLine
    SQL = SQL & " FROM seguro_vida_sub_grupo_tb sgp WITH (NOLOCK)  " & vbNewLine
    SQL = SQL & " LEFT JOIN escolha_sub_grp_tp_cob_comp_tb esg   WITH (NOLOCK)  ON sgp.apolice_id = esg.apolice_id AND sgp.sucursal_seguradora_id = esg.sucursal_seguradora_id AND sgp.seguradora_cod_susep = esg.seguradora_cod_susep AND sgp.ramo_id = esg.ramo_id AND sgp.sub_grupo_id = esg.sub_grupo_id AND sgp.dt_inicio_vigencia_sbg = esg.dt_inicio_vigencia_sbg AND sgp.dt_fim_vigencia_sbg    IS NULL   " & vbNewLine
    SQL = SQL & " INNER JOIN cliente_tb cl   WITH (NOLOCK) ON sgp.prop_cliente_id = cl.cliente_id   " & vbNewLine
    SQL = SQL & " INNER JOIN pessoa_fisica_tb pf   WITH (NOLOCK) ON sgp.prop_cliente_id = pf.pf_cliente_id  " & vbNewLine
    SQL = SQL & " LEFT JOIN emi_proposta_tb emi WITH (NOLOCK) ON cl.cpf_cnpj = emi.cpf  " & vbNewLine
    SQL = SQL & " LEFT JOIN endereco_cliente_tb ec   WITH (NOLOCK) ON sgp.prop_cliente_id = ec.cliente_id  " & vbNewLine
    SQL = SQL & " LEFT JOIN seguro_vida_sub_grupo_tb vsg WITH (NOLOCK) ON sgp.sub_grupo_id = vsg.sub_grupo_id AND sgp.proposta_id = vsg.proposta_id AND sgp.prop_cliente_id = vsg.prop_cliente_titular_id AND vsg.tp_componente_id = 3 -- conjuge   " & vbNewLine
    SQL = SQL & " LEFT JOIN cliente_tb conjuge   WITH (NOLOCK) ON vsg.prop_cliente_id = conjuge.cliente_id  " & vbNewLine
    SQL = SQL & " LEFT JOIN pessoa_fisica_tb pf_conjuge   WITH (NOLOCK) ON vsg.prop_cliente_id = pf_conjuge.pf_cliente_id  " & vbNewLine
    SQL = SQL & " LEFT JOIN beneficiario_tb beneficiario1   WITH (NOLOCK) ON sgp.proposta_id = beneficiario1.proposta_id AND beneficiario1.seq_beneficiario = 1  " & vbNewLine
    SQL = SQL & " LEFT JOIN beneficiario_tb beneficiario2   WITH (NOLOCK) ON sgp.proposta_id = beneficiario2.proposta_id AND beneficiario2.seq_beneficiario = 2  " & vbNewLine
    SQL = SQL & " LEFT JOIN beneficiario_tb beneficiario3   WITH (NOLOCK) ON sgp.proposta_id = beneficiario3.proposta_id AND beneficiario3.seq_beneficiario = 3  " & vbNewLine
    SQL = SQL & " LEFT JOIN beneficiario_tb beneficiario4   WITH (NOLOCK) ON sgp.proposta_id = beneficiario4.proposta_id AND beneficiario4.seq_beneficiario = 4  " & vbNewLine
    SQL = SQL & " LEFT JOIN beneficiario_tb beneficiario5   WITH (NOLOCK) ON sgp.proposta_id = beneficiario5.proposta_id AND beneficiario5.seq_beneficiario = 5  " & vbNewLine
    SQL = SQL & " LEFT JOIN beneficiario_tb beneficiario6   WITH (NOLOCK) ON sgp.proposta_id = beneficiario6.proposta_id AND beneficiario6.seq_beneficiario = 6  " & vbNewLine
    SQL = SQL & " LEFT JOIN beneficiario_tb beneficiario7   WITH (NOLOCK) ON sgp.proposta_id = beneficiario7.proposta_id AND beneficiario7.seq_beneficiario = 7  " & vbNewLine
    If optTodosSubgrupos.Value = True Then
        SQL = SQL & " WHERE sgp.sub_grupo_id IN (" & SubGrupos & ")" & vbNewLine
    Else
        SQL = SQL & " WHERE sgp.sub_grupo_id = " & vSubGrupoId & vbNewLine
    End If
    SQL = SQL & " AND sgp.apolice_id = " & vApoliceId & " " & vbNewLine
    SQL = SQL & " AND sgp.sucursal_seguradora_id = " & vSucursalId & "" & vbNewLine
    SQL = SQL & " AND sgp.seguradora_cod_susep = " & vSeguradoraId & "" & vbNewLine
    SQL = SQL & " AND sgp.ramo_id = " & vRamoId & "" & vbNewLine
    SQL = SQL & " AND sgp.dt_fim_vigencia_sbg Is Null" & vbNewLine
    SQL = SQL & " AND sgp.dt_inicio_vigencia_sbg = (Select TOP 1 sgp2.dt_inicio_vigencia_sbg FROM seguro_vida_sub_grupo_tb sgp2   WITH (NOLOCK) " & vbNewLine
    SQL = SQL & "                                                                            WHERE sgp2.apolice_id = sgp.apolice_id " & vbNewLine
    SQL = SQL & "                                                                              AND sgp2.sucursal_seguradora_id = sgp.sucursal_seguradora_id " & vbNewLine
    SQL = SQL & "                                                                              AND sgp2.seguradora_cod_susep = sgp.seguradora_cod_susep" & vbNewLine
    SQL = SQL & "                                                                              AND sgp2.ramo_id = sgp.ramo_id" & vbNewLine
    SQL = SQL & "                                                                              AND sgp2.sub_grupo_id = sgp.sub_grupo_id" & vbNewLine
    SQL = SQL & "                                                                              AND sgp2.dt_inicio_vigencia_apol_sbg = sgp.dt_inicio_vigencia_apol_sbg" & vbNewLine
    SQL = SQL & "                                                                              AND sgp2.proposta_id = sgp.proposta_id" & vbNewLine
    SQL = SQL & "                                                                              AND sgp2.prop_cliente_id = sgp.prop_cliente_id" & vbNewLine
    SQL = SQL & "                                                                              AND sgp2.tp_componente_id = sgp.tp_componente_id" & vbNewLine
    SQL = SQL & "                                                                              AND sgp2.seq_canc_endosso_seg = sgp.seq_canc_endosso_seg" & vbNewLine
    SQL = SQL & "                                                                            ORDER BY sgp2.dt_inicio_vigencia_sbg DESC)" & vbNewLine

    SQL = SQL & " GROUP BY cl.cliente_id,  " & vbNewLine
    SQL = SQL & " sgp.sub_grupo_id , sgp.dt_inicio_vigencia_seg, sgp.dt_entrada_sub_grupo, cl.Nome, pf.CPF, pf.Sexo, pf.dt_nascimento, pf.estado_civil, sgp.proposta_id,  " & vbNewLine
    SQL = SQL & " sgp.prop_cliente_id, emi.Endereco, emi.Bairro, emi.Municipio, emi.CEP, emi.estado, cl.e_mail, cl.ddd_1,  " & vbNewLine
    SQL = SQL & " cl.telefone_1 , Conjuge.Nome, Conjuge.CPF_CNPJ, pf_conjuge.dt_nascimento, pf_conjuge.Sexo, vsg.dt_inicio_vigencia_seg,  " & vbNewLine
    SQL = SQL & " sgp.val_salario, esg.val_is, sgp.val_capital_segurado, sgp.dt_inicio_vigencia_sbg,  " & vbNewLine
    SQL = SQL & " sgp.dt_fim_vigencia_sbg , sgp.dt_fim_vigencia_sbg, sgp.tp_pendencia_id, sgp.motivo_pendencia,  " & vbNewLine
    SQL = SQL & " Beneficiario1.Nome, Beneficiario2.Nome, Beneficiario3.Nome, Beneficiario4.Nome, Beneficiario5.Nome, Beneficiario6.Nome, Beneficiario7.Nome  " & vbNewLine
    SQL = SQL & " ORDER BY cl.Nome  " & vbNewLine & vbNewLine

    SQL = SQL & " DELETE A  " & vbNewLine
    SQL = SQL & " FROM #Planilha A " & vbNewLine
    SQL = SQL & " JOIN #Planilha B " & vbNewLine
    SQL = SQL & " ON A.cliente_id = B.cliente_id " & vbNewLine
    SQL = SQL & "where A.cliente_id = B.cliente_id  " & vbNewLine
    SQL = SQL & "and a.ID < b.ID " & vbNewLine & vbNewLine

    SQL = SQL & "SELECT cliente_id, sub_grupo_id, Inicio_de_cobertura,dt_entrada_sub_grupo, Upper(nome) as nome, cpf, Upper(sexo) as sexo, dt_nascimento, Upper(estado_civil) as estado_civil, " & vbNewLine
    SQL = SQL & "       proposta_id, prop_cliente_id, Upper(endereco_principal) as endereco_principal, Upper(bairro_principal) as bairro_principal, Upper(municipio) as municipio, cep, Upper(estado) as estado, Upper(e_mail) as e_mail, ddd_principal, " & vbNewLine
    SQL = SQL & "       telefone_principal , Upper(Nome_Conjuge) as Nome_Conjuge, cfp_conjuge, dt_nasc_conjuge, Upper(Sexo_conjuge) as Sexo_conjuge, Inicio_de_cobertura_do_conjuge," & vbNewLine
    SQL = SQL & "       val_salario, ROUND(val_capital_segurado,0) val_capital_segurado, dt_inicio_vigencia_sbg, dt_fim_vigencia_sbg, " & vbNewLine
    SQL = SQL & "       Beneficiario1, Beneficiario2, Beneficiario3, Beneficiario4, Beneficiario5, Beneficiario6, Beneficiario7, Descricao1, Descricao2, Descricao3, Descricao4, Descricao5 " & vbNewLine
    SQL = SQL & "  FROM #Planilha " & vbNewLine
    SQL = SQL & "  ORDER BY sub_grupo_id " & vbNewLine
    SQL = SQL & " DROP TABLE #Planilha"


    Set rc = rdocn.OpenResultset(SQL)


    'sequencial inicial
    sequencial = 1


    'escrevendo no arquivo
    n = 1

    With exclSheet
        .cells(n, 1).numberformat = "@"
        .cells(n, 1).Value = "000000"
        .cells(n, 2).numberformat = "@"
        .cells(n, 2).Value = "SEGA4000"
        .cells(n, 3).numberformat = "@"
        .cells(n, 3).Value = Right("00" & vRamoId, 2)
        .cells(n, 4).numberformat = "@"
        .cells(n, 4).Value = Right("000000000" & vApoliceId, 9)
        If optTodosSubgrupos.Value = True Then
            .cells(n, 5).numberformat = "@"
            .cells(n, 5).Value = "0"
        Else
            .cells(n, 5).numberformat = "@"
            .cells(n, 5).Value = Right("000" & vSubGrupoId, 3)
        End If
        .cells(n, 6).numberformat = "@"
        .cells(n, 6).Value = Right("00" & Month(CDate(Date)), 2) & "/" & Year(CDate(Date))
        .cells(n, 7).numberformat = "@"
        .cells(n, 7).Value = estipulante
    End With

    n = n + 1

    Do While Not rc.EOF

        'Definir o conte�do das c�lulas:
        With exclSheet
            .cells(n, 1).Value = sequencial
            .cells(n, 2).Value = 10
            .cells(n, 3).Value = rc!Inicio_de_cobertura
            .cells(n, 4).Value = rc!dt_entrada_sub_grupo
            .cells(n, 5).Value = rc!Nome
            .cells(n, 6).Value = rc!CPF
            .cells(n, 7).Value = rc!dt_nascimento
            .cells(n, 8).Value = rc!Sexo
            .cells(n, 9).Value = rc!estado_civil
            .cells(n, 10).Value = rc!val_capital_segurado
            .cells(n, 11).Value = rc!val_salario
            .cells(n, 12).Value = rc!endereco_principal
            .cells(n, 13).Value = rc!bairro_principal
            .cells(n, 14).Value = rc!Municipio
            .cells(n, 15).Value = rc!CEP
            .cells(n, 16).Value = rc!Estado
            .cells(n, 17).Value = rc!e_mail
            .cells(n, 18).Value = rc!ddd_principal
            .cells(n, 19).Value = rc!telefone_principal
            .cells(n, 20).Value = rc!Nome_Conjuge
            .cells(n, 21).Value = rc!cfp_conjuge
            .cells(n, 22).Value = rc!dt_nasc_conjuge
            .cells(n, 23).Value = rc!Sexo_conjuge
            .cells(n, 24).Value = rc!Inicio_de_cobertura_do_conjuge
            .cells(n, 25).Value = rc!Beneficiario1
            .cells(n, 26).Value = rc!Beneficiario2
            .cells(n, 27).Value = rc!Beneficiario3
            .cells(n, 28).Value = rc!Beneficiario4
            .cells(n, 29).Value = rc!Beneficiario5
            .cells(n, 30).Value = rc!Beneficiario6
            .cells(n, 31).Value = rc!Beneficiario7
            .cells(n, 32).Value = rc!Descricao1
            .cells(n, 33).Value = rc!Descricao2
            .cells(n, 34).Value = rc!Descricao3
            .cells(n, 35).Value = rc!Descricao4
            .cells(n, 36).Value = rc!Descricao5
            .cells(n, 37).Value = rc!sub_grupo_id

            sequencial = sequencial + 1

            rc.MoveNext
            n = n + 1

        End With
    Loop


    'MONTANDO O TRAILER DO ARQUIVO

    With exclSheet
        .cells(n, 1).Value = "999999"
        .cells(n, 4).Value = sequencial - 1
    End With

    'INICIO 'DIEGO SILVA - CONFITEC-IM01194162
    'Salvar o Arquivo
    'exclSheet.SaveAs cNomeArquivo
        
    'Tenta salvar uma c�pia do arquivo no local
    Call exclBook.SaveCopyAs(cNomeArquivo & ".xlsx")
    'FIM 'DIEGO SILVA - CONFITEC-IM01194162

    ' MousePointer = Normal  ' Muda o ponteiro do mouse
    Screen.MousePointer = 0
    'MsgBox ("Arquivo gerado com sucesso!") 'DIEGO SILVA - CONFITEC-IM01194162

    'Limpe as vari�veis de Objeto:
	exclApp.Visible = True 'DIEGO SILVA - CONFITEC-IM01194162
    exclApp.Workbooks.Close
    exclApp.Application.Quit
    exclApp.Quit
    Set exclSheet = Nothing
    Set exclBook = Nothing
    Set exclApp = Nothing


    rc.Close

	MsgBox ("Arquivo gerado com sucesso!") 'DIEGO SILVA - CONFITEC-IM01194162

    Exit Sub

Erro:
	
    'INICIO 'DIEGO SILVA - CONFITEC-IM01194162
    'Exibir a janela de salvamento do excel caso der erro ao salvar a c�pia.
    If Err.Number = 1004 Then
                
        exclApp.Visible = True
        exclApp.Workbooks.Close
        exclApp.Application.Quit
        exclApp.Quit
        Set exclSheet = Nothing
        Set exclBook = Nothing
        Set exclApp = Nothing
        
        Screen.MousePointer = 0
        MsgBox ("Arquivo gerado com sucesso!")
                
        Exit Sub

    End If
    'FIM 'DIEGO SILVA - CONFITEC-IM01194162
	
    TrataErroGeral "Gera_Arquivo_Excel", Me.name
    Unload Me
    TerminaSEGBR
End Sub


