VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "tabctl32.ocx"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "mscomctl.OCX"
Object = "{00025600-0000-0000-C000-000000000046}#5.2#0"; "crystl32.ocx"
Begin VB.Form ConFatura 
   ClientHeight    =   8970
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   8895
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   ScaleHeight     =   8970
   ScaleWidth      =   8895
   StartUpPosition =   2  'CenterScreen
   Begin Crystal.CrystalReport Report 
      Left            =   1080
      Top             =   8160
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   348160
      PrintFileLinesPerPage=   60
   End
   Begin VB.CommandButton cmdImprime 
      Caption         =   "Imprimir"
      Height          =   375
      Index           =   0
      Left            =   6060
      TabIndex        =   97
      Top             =   8280
      Width           =   1215
   End
   Begin VB.CommandButton cmdSaida 
      Caption         =   "&Sair"
      Height          =   380
      Left            =   7440
      TabIndex        =   96
      Top             =   8280
      Width           =   1185
   End
   Begin MSComctlLib.StatusBar StatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   83
      Top             =   8685
      Width           =   8895
      _ExtentX        =   15690
      _ExtentY        =   503
      Style           =   1
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin TabDlg.SSTab SSTabFat 
      Height          =   8055
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   8625
      _ExtentX        =   15214
      _ExtentY        =   14208
      _Version        =   393216
      Tabs            =   4
      TabHeight       =   520
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "Fatura"
      TabPicture(0)   =   "ConFatura.frx":0000
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "frCorretores"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "FraHistorico"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "FraProLabore"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).Control(3)=   "Frame1"
      Tab(0).Control(3).Enabled=   0   'False
      Tab(0).ControlCount=   4
      TabCaption(1)   =   "Segurados"
      TabPicture(1)   =   "ConFatura.frx":001C
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "fraSegurados"
      Tab(1).ControlCount=   1
      TabCaption(2)   =   "Valores"
      TabPicture(2)   =   "ConFatura.frx":0038
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "frPercentuais"
      Tab(2).Control(1)=   "frObservacao"
      Tab(2).Control(2)=   "frDatasFatura"
      Tab(2).Control(3)=   "frValoresFatura"
      Tab(2).Control(4)=   "frNumeroFatura"
      Tab(2).ControlCount=   5
      TabCaption(3)   =   "Cosseguro"
      TabPicture(3)   =   "ConFatura.frx":0054
      Tab(3).ControlEnabled=   0   'False
      Tab(3).Control(0)=   "FraAceito"
      Tab(3).Control(1)=   "FraCongeneres"
      Tab(3).ControlCount=   2
      Begin VB.Frame frNumeroFatura 
         Caption         =   "Fatura"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   735
         Left            =   -74790
         TabIndex        =   103
         Top             =   720
         Width           =   8085
         Begin VB.TextBox TxtNoVidas 
            Alignment       =   1  'Right Justify
            Height          =   285
            Left            =   4320
            Locked          =   -1  'True
            MultiLine       =   -1  'True
            TabIndex        =   107
            Top             =   330
            Width           =   945
         End
         Begin VB.TextBox txtNumFatura 
            Alignment       =   1  'Right Justify
            Height          =   285
            Left            =   480
            Locked          =   -1  'True
            MultiLine       =   -1  'True
            TabIndex        =   106
            Top             =   330
            Width           =   735
         End
         Begin VB.TextBox txtEmiFatura 
            Alignment       =   1  'Right Justify
            Height          =   285
            Left            =   2040
            Locked          =   -1  'True
            TabIndex        =   105
            Top             =   330
            Width           =   1095
         End
         Begin VB.TextBox txtUsuarioEmissor 
            Height          =   285
            Left            =   6720
            Locked          =   -1  'True
            TabIndex        =   104
            Top             =   330
            Width           =   975
         End
         Begin VB.Label lblDesc 
            Caption         =   "Nro Vidas :"
            Height          =   225
            Index           =   20
            Left            =   3360
            TabIndex        =   111
            Top             =   330
            Width           =   1035
         End
         Begin VB.Label lblDesc 
            Caption         =   "N� :"
            Height          =   180
            Index           =   14
            Left            =   120
            TabIndex        =   110
            Top             =   330
            Width           =   375
         End
         Begin VB.Label lblDesc 
            Caption         =   "Emiss�o :"
            Height          =   210
            Index           =   13
            Left            =   1320
            TabIndex        =   109
            Top             =   330
            Width           =   825
         End
         Begin VB.Label lblUsuarioEmissor 
            AutoSize        =   -1  'True
            Caption         =   "Usuario Emissor:"
            Height          =   195
            Left            =   5400
            TabIndex        =   108
            Top             =   330
            Width           =   1170
         End
      End
      Begin VB.Frame Frame1 
         Caption         =   "Agenciamento"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1095
         Left            =   240
         TabIndex        =   5
         Top             =   3450
         Width           =   8085
         Begin VB.TextBox TxtCodAgenciador 
            Alignment       =   1  'Right Justify
            Height          =   285
            Left            =   1200
            Locked          =   -1  'True
            MultiLine       =   -1  'True
            TabIndex        =   7
            Top             =   270
            Width           =   885
         End
         Begin VB.TextBox TxtAgenciador 
            Alignment       =   1  'Right Justify
            Height          =   285
            Left            =   3210
            Locked          =   -1  'True
            TabIndex        =   9
            Top             =   270
            Width           =   4425
         End
         Begin VB.TextBox TxtNoParcAgenciamento 
            Alignment       =   1  'Right Justify
            Height          =   285
            Left            =   7020
            Locked          =   -1  'True
            MultiLine       =   -1  'True
            TabIndex        =   15
            Top             =   660
            Width           =   615
         End
         Begin VB.TextBox TxtPercAg 
            Alignment       =   1  'Right Justify
            Height          =   285
            Left            =   1200
            Locked          =   -1  'True
            MultiLine       =   -1  'True
            TabIndex        =   11
            Top             =   660
            Width           =   885
         End
         Begin VB.TextBox TxtFPAgenciamento 
            Alignment       =   1  'Right Justify
            Height          =   285
            Left            =   3210
            Locked          =   -1  'True
            TabIndex        =   13
            Top             =   660
            Width           =   2655
         End
         Begin VB.Label lblDesc 
            Caption         =   "C�digo :"
            Height          =   210
            Index           =   16
            Left            =   270
            TabIndex        =   6
            Top             =   300
            Width           =   855
         End
         Begin VB.Label lblDesc 
            Caption         =   "Agenciador :"
            Height          =   210
            Index           =   15
            Left            =   2220
            TabIndex        =   8
            Top             =   300
            Width           =   1065
         End
         Begin VB.Label lblDesc 
            Caption         =   "No Parcelas :"
            Height          =   225
            Index           =   2
            Left            =   5970
            TabIndex        =   14
            Top             =   690
            Width           =   1005
         End
         Begin VB.Label lblDesc 
            Caption         =   "Percentual :"
            Height          =   180
            Index           =   1
            Left            =   270
            TabIndex        =   10
            Top             =   690
            Width           =   855
         End
         Begin VB.Label lblDesc 
            Caption         =   "Forma Pgto. :"
            Height          =   210
            Index           =   0
            Left            =   2190
            TabIndex        =   12
            Top             =   690
            Width           =   1065
         End
      End
      Begin VB.Frame FraProLabore 
         Caption         =   "Pr�-Labore"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1275
         Left            =   240
         TabIndex        =   3
         Top             =   2190
         Width           =   8085
         Begin MSFlexGridLib.MSFlexGrid GridProLabore 
            Height          =   900
            Left            =   300
            TabIndex        =   4
            Top             =   240
            Width           =   7440
            _ExtentX        =   13123
            _ExtentY        =   1588
            _Version        =   393216
            Rows            =   1
            Cols            =   3
            FixedCols       =   0
            AllowUserResizing=   1
            FormatString    =   $"ConFatura.frx":0070
         End
      End
      Begin VB.Frame FraHistorico 
         Caption         =   "Hist�rico"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   3345
         Left            =   240
         TabIndex        =   16
         Top             =   4560
         Width           =   8085
         Begin VB.TextBox txtValPremioAltNaoProcessadas 
            Alignment       =   1  'Right Justify
            Height          =   285
            Left            =   5940
            Locked          =   -1  'True
            MultiLine       =   -1  'True
            TabIndex        =   123
            Top             =   2940
            Width           =   1725
         End
         Begin VB.TextBox txtValISAltNaoProcessadas 
            Alignment       =   1  'Right Justify
            Height          =   285
            Left            =   3690
            Locked          =   -1  'True
            MultiLine       =   -1  'True
            TabIndex        =   122
            Top             =   2940
            Width           =   1725
         End
         Begin VB.TextBox txtVidasAltNaoProcessadas 
            Alignment       =   1  'Right Justify
            Height          =   285
            Left            =   2310
            Locked          =   -1  'True
            MaxLength       =   8
            MultiLine       =   -1  'True
            TabIndex        =   121
            Top             =   2940
            Width           =   915
         End
         Begin VB.TextBox txtValPremioInclNaoProcessadas 
            Alignment       =   1  'Right Justify
            Height          =   285
            Left            =   5940
            Locked          =   -1  'True
            MultiLine       =   -1  'True
            TabIndex        =   119
            Top             =   2580
            Width           =   1725
         End
         Begin VB.TextBox txtValISInclNaoProcessadas 
            Alignment       =   1  'Right Justify
            Height          =   285
            Left            =   3690
            Locked          =   -1  'True
            MultiLine       =   -1  'True
            TabIndex        =   118
            Top             =   2580
            Width           =   1725
         End
         Begin VB.TextBox txtVidasInclNaoProcessadas 
            Alignment       =   1  'Right Justify
            Height          =   285
            Left            =   2310
            Locked          =   -1  'True
            MaxLength       =   8
            MultiLine       =   -1  'True
            TabIndex        =   117
            Top             =   2580
            Width           =   915
         End
         Begin VB.TextBox TxtVidasCob 
            Alignment       =   1  'Right Justify
            Height          =   285
            Left            =   2310
            Locked          =   -1  'True
            MaxLength       =   8
            MultiLine       =   -1  'True
            TabIndex        =   114
            Top             =   2190
            Width           =   915
         End
         Begin VB.TextBox TxtValISCob 
            Alignment       =   1  'Right Justify
            Height          =   285
            Left            =   3690
            Locked          =   -1  'True
            MultiLine       =   -1  'True
            TabIndex        =   113
            Top             =   2190
            Width           =   1725
         End
         Begin VB.TextBox TxtValPremioCob 
            Alignment       =   1  'Right Justify
            Height          =   285
            Left            =   5940
            Locked          =   -1  'True
            MultiLine       =   -1  'True
            TabIndex        =   112
            Top             =   2190
            Width           =   1725
         End
         Begin VB.TextBox TxtValPremio 
            Alignment       =   1  'Right Justify
            Height          =   285
            Left            =   5940
            MultiLine       =   -1  'True
            TabIndex        =   23
            Top             =   390
            Width           =   1725
         End
         Begin VB.TextBox TxtValPremioAcerto 
            Alignment       =   1  'Right Justify
            Height          =   285
            Left            =   5940
            Locked          =   -1  'True
            MultiLine       =   -1  'True
            TabIndex        =   39
            Top             =   1830
            Width           =   1725
         End
         Begin VB.TextBox TxtValPremioExc 
            Alignment       =   1  'Right Justify
            Height          =   285
            Left            =   5940
            Locked          =   -1  'True
            MultiLine       =   -1  'True
            TabIndex        =   35
            Top             =   1470
            Width           =   1725
         End
         Begin VB.TextBox TxtValPremioCap 
            Alignment       =   1  'Right Justify
            Height          =   285
            Left            =   5940
            Locked          =   -1  'True
            MultiLine       =   -1  'True
            TabIndex        =   31
            Top             =   1110
            Width           =   1725
         End
         Begin VB.TextBox TxtValPremioInc 
            Alignment       =   1  'Right Justify
            Height          =   285
            Left            =   5940
            Locked          =   -1  'True
            MultiLine       =   -1  'True
            TabIndex        =   27
            Top             =   750
            Width           =   1725
         End
         Begin VB.TextBox TxtValISAcerto 
            Alignment       =   1  'Right Justify
            Height          =   285
            Left            =   3690
            Locked          =   -1  'True
            MultiLine       =   -1  'True
            TabIndex        =   38
            Top             =   1830
            Width           =   1725
         End
         Begin VB.TextBox TxtValISExc 
            Alignment       =   1  'Right Justify
            Height          =   285
            Left            =   3690
            Locked          =   -1  'True
            MultiLine       =   -1  'True
            TabIndex        =   34
            Top             =   1470
            Width           =   1725
         End
         Begin VB.TextBox TxtValISCap 
            Alignment       =   1  'Right Justify
            Height          =   285
            Left            =   3690
            Locked          =   -1  'True
            MultiLine       =   -1  'True
            TabIndex        =   30
            Top             =   1110
            Width           =   1725
         End
         Begin VB.TextBox TxtValISInc 
            Alignment       =   1  'Right Justify
            Height          =   285
            Left            =   3690
            Locked          =   -1  'True
            MultiLine       =   -1  'True
            TabIndex        =   26
            Top             =   750
            Width           =   1725
         End
         Begin VB.TextBox TxtValIS 
            Alignment       =   1  'Right Justify
            Height          =   285
            Left            =   3690
            Locked          =   -1  'True
            MultiLine       =   -1  'True
            TabIndex        =   22
            Top             =   390
            Width           =   1725
         End
         Begin VB.TextBox TxtVidas 
            Alignment       =   1  'Right Justify
            Height          =   285
            Left            =   2310
            Locked          =   -1  'True
            MaxLength       =   8
            MultiLine       =   -1  'True
            TabIndex        =   21
            Top             =   390
            Width           =   915
         End
         Begin VB.TextBox TxtVidasInc 
            Alignment       =   1  'Right Justify
            Height          =   285
            Left            =   2310
            Locked          =   -1  'True
            MaxLength       =   8
            MultiLine       =   -1  'True
            TabIndex        =   25
            Top             =   750
            Width           =   915
         End
         Begin VB.TextBox TxtVidasCap 
            Alignment       =   1  'Right Justify
            Height          =   285
            Left            =   2310
            Locked          =   -1  'True
            MaxLength       =   8
            MultiLine       =   -1  'True
            TabIndex        =   29
            Top             =   1110
            Width           =   915
         End
         Begin VB.TextBox TxtVidasExc 
            Alignment       =   1  'Right Justify
            Height          =   285
            Left            =   2310
            Locked          =   -1  'True
            MaxLength       =   8
            MultiLine       =   -1  'True
            TabIndex        =   33
            Top             =   1470
            Width           =   915
         End
         Begin VB.TextBox TxtVidasAcerto 
            Alignment       =   1  'Right Justify
            Height          =   285
            Left            =   2310
            Locked          =   -1  'True
            MaxLength       =   8
            MultiLine       =   -1  'True
            TabIndex        =   37
            Top             =   1830
            Width           =   915
         End
         Begin VB.Label lblDesc 
            Caption         =   "Altera��es n�o processadas:"
            Height          =   225
            Index           =   23
            Left            =   240
            TabIndex        =   124
            Top             =   2970
            Width           =   2505
         End
         Begin VB.Label lblDesc 
            Caption         =   "Inclus�es n�o Processadas:"
            Height          =   225
            Index           =   22
            Left            =   240
            TabIndex        =   120
            Top             =   2610
            Width           =   2025
         End
         Begin VB.Label lblDesc 
            Caption         =   "Cobertura Provis�ria :"
            Height          =   225
            Index           =   17
            Left            =   240
            TabIndex        =   115
            Top             =   2220
            Width           =   1665
         End
         Begin VB.Label lblDesc 
            Caption         =   "Vidas Normais :"
            Height          =   225
            Index           =   30
            Left            =   270
            TabIndex        =   20
            Top             =   420
            Width           =   1125
         End
         Begin VB.Label lblDesc 
            Alignment       =   1  'Right Justify
            Caption         =   "Vidas"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   225
            Index           =   31
            Left            =   2430
            TabIndex        =   17
            Top             =   180
            Width           =   645
         End
         Begin VB.Label lblDesc 
            Alignment       =   1  'Right Justify
            Caption         =   "Capital Segurado"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   225
            Index           =   32
            Left            =   3750
            TabIndex        =   18
            Top             =   180
            Width           =   1515
         End
         Begin VB.Label lblDesc 
            Alignment       =   1  'Right Justify
            Caption         =   "Pr�mio"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   225
            Index           =   33
            Left            =   6180
            TabIndex        =   19
            Top             =   180
            Width           =   1395
         End
         Begin VB.Label lblDesc 
            Caption         =   "Inclus�es :"
            Height          =   225
            Index           =   34
            Left            =   270
            TabIndex        =   24
            Top             =   780
            Width           =   1125
         End
         Begin VB.Label lblDesc 
            Caption         =   "Aumento de Capitais :"
            Height          =   225
            Index           =   35
            Left            =   270
            TabIndex        =   28
            Top             =   1140
            Width           =   1635
         End
         Begin VB.Label lblDesc 
            Caption         =   "Exclus�es :"
            Height          =   225
            Index           =   36
            Left            =   270
            TabIndex        =   32
            Top             =   1500
            Width           =   1125
         End
         Begin VB.Label lblDesc 
            Caption         =   "Acertos :"
            Height          =   225
            Index           =   37
            Left            =   270
            TabIndex        =   36
            Top             =   1860
            Width           =   1125
         End
      End
      Begin VB.Frame frCorretores 
         Caption         =   "Corretagem"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1485
         Left            =   240
         TabIndex        =   1
         Top             =   720
         Width           =   8085
         Begin MSFlexGridLib.MSFlexGrid GridCorretagem 
            Height          =   1125
            Left            =   300
            TabIndex        =   2
            Top             =   240
            Width           =   7440
            _ExtentX        =   13123
            _ExtentY        =   1984
            _Version        =   393216
            Rows            =   1
            Cols            =   6
            FixedCols       =   0
            AllowUserResizing=   1
            FormatString    =   $"ConFatura.frx":00FE
         End
      End
      Begin VB.Frame fraSegurados 
         Caption         =   " Segurados: "
         Height          =   6915
         Left            =   -74760
         TabIndex        =   40
         Top             =   840
         Width           =   7935
         Begin MSFlexGridLib.MSFlexGrid GridSegurados 
            Height          =   6195
            Left            =   240
            TabIndex        =   41
            Top             =   480
            Width           =   7455
            _ExtentX        =   13150
            _ExtentY        =   10927
            _Version        =   393216
            Cols            =   6
            AllowUserResizing=   1
            FormatString    =   $"ConFatura.frx":019D
         End
      End
      Begin VB.Frame frValoresFatura 
         Caption         =   "Valores"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   2385
         Left            =   -74790
         TabIndex        =   52
         Top             =   2280
         Width           =   8085
         Begin VB.TextBox txtPercIOF 
            Height          =   285
            Left            =   1680
            TabIndex        =   116
            Top             =   2280
            Visible         =   0   'False
            Width           =   1695
         End
         Begin VB.TextBox TxtValAgenciamento 
            Alignment       =   1  'Right Justify
            Height          =   285
            Left            =   5940
            Locked          =   -1  'True
            MultiLine       =   -1  'True
            TabIndex        =   98
            Top             =   1530
            Width           =   1725
         End
         Begin VB.TextBox txtValPremioBruto 
            Alignment       =   1  'Right Justify
            Height          =   285
            Left            =   5940
            Locked          =   -1  'True
            MultiLine       =   -1  'True
            TabIndex        =   91
            Top             =   1950
            Width           =   1725
         End
         Begin VB.TextBox txtValEstipulante 
            Alignment       =   1  'Right Justify
            Height          =   285
            Left            =   5940
            Locked          =   -1  'True
            MultiLine       =   -1  'True
            TabIndex        =   90
            Top             =   1110
            Width           =   1725
         End
         Begin VB.TextBox txtValIR 
            Alignment       =   1  'Right Justify
            Height          =   285
            Left            =   5940
            Locked          =   -1  'True
            MultiLine       =   -1  'True
            TabIndex        =   89
            Top             =   690
            Width           =   1725
         End
         Begin VB.TextBox txtValCorretagem 
            Alignment       =   1  'Right Justify
            Height          =   285
            Left            =   5940
            Locked          =   -1  'True
            MultiLine       =   -1  'True
            TabIndex        =   88
            Top             =   270
            Width           =   1725
         End
         Begin VB.TextBox txtValIOF 
            Alignment       =   1  'Right Justify
            Height          =   285
            Left            =   1650
            Locked          =   -1  'True
            MultiLine       =   -1  'True
            TabIndex        =   87
            Top             =   1950
            Width           =   1725
         End
         Begin VB.TextBox txtValJuros 
            Alignment       =   1  'Right Justify
            Height          =   285
            Left            =   1650
            Locked          =   -1  'True
            MultiLine       =   -1  'True
            TabIndex        =   86
            Top             =   1530
            Width           =   1725
         End
         Begin VB.TextBox txtValDesconto 
            Alignment       =   1  'Right Justify
            Height          =   285
            Left            =   1650
            Locked          =   -1  'True
            MultiLine       =   -1  'True
            TabIndex        =   85
            Top             =   1110
            Width           =   1725
         End
         Begin VB.TextBox txtValPremioLiquido 
            Alignment       =   1  'Right Justify
            Height          =   285
            Left            =   1650
            Locked          =   -1  'True
            MultiLine       =   -1  'True
            TabIndex        =   84
            Top             =   690
            Width           =   1725
         End
         Begin VB.TextBox txtValImpSegurada 
            Alignment       =   1  'Right Justify
            Height          =   285
            Left            =   1650
            Locked          =   -1  'True
            MultiLine       =   -1  'True
            TabIndex        =   54
            Top             =   270
            Width           =   1725
         End
         Begin VB.Label lblDesc 
            Caption         =   "Agenciamento :"
            Height          =   300
            Index           =   3
            Left            =   4500
            TabIndex        =   99
            Top             =   1560
            Width           =   1155
         End
         Begin VB.Label lblDesc 
            Caption         =   "Pr�mio Total :"
            Height          =   300
            Index           =   9
            Left            =   4530
            TabIndex        =   62
            Top             =   1980
            Width           =   1245
         End
         Begin VB.Label lblDesc 
            Caption         =   "IOF :"
            Height          =   300
            Index           =   8
            Left            =   180
            TabIndex        =   58
            Top             =   1980
            Width           =   1395
         End
         Begin VB.Label lblDesc 
            Caption         =   "Capital Segurado :"
            Height          =   300
            Index           =   7
            Left            =   180
            TabIndex        =   53
            Top             =   300
            Width           =   1575
         End
         Begin VB.Label lblDesc 
            Caption         =   "Corretagem Total :"
            Height          =   300
            Index           =   18
            Left            =   4500
            TabIndex        =   59
            Top             =   300
            Width           =   1365
         End
         Begin VB.Label lblDesc 
            Caption         =   "Pr�-Labore :"
            Height          =   300
            Index           =   19
            Left            =   4500
            TabIndex        =   61
            Top             =   1140
            Width           =   1005
         End
         Begin VB.Label lblDesc 
            Caption         =   "IR :"
            Height          =   300
            Index           =   21
            Left            =   4500
            TabIndex        =   60
            Top             =   720
            Width           =   1005
         End
         Begin VB.Label lblDesc 
            Caption         =   "Pr�mio L�quido :"
            Height          =   300
            Index           =   38
            Left            =   180
            TabIndex        =   55
            Top             =   720
            Width           =   1395
         End
         Begin VB.Label lblDesc 
            Caption         =   "Desconto :"
            Height          =   300
            Index           =   43
            Left            =   180
            TabIndex        =   56
            Top             =   1140
            Width           =   1395
         End
         Begin VB.Label lblDesc 
            Caption         =   "Juros :"
            Height          =   300
            Index           =   44
            Left            =   180
            TabIndex        =   57
            Top             =   1560
            Width           =   1395
         End
      End
      Begin VB.Frame frDatasFatura 
         Caption         =   "Datas"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   735
         Left            =   -74820
         TabIndex        =   63
         Top             =   4710
         Width           =   8115
         Begin VB.TextBox txtDatVencFatura 
            Alignment       =   1  'Right Justify
            Height          =   315
            Left            =   6600
            Locked          =   -1  'True
            TabIndex        =   94
            Top             =   300
            Width           =   1170
         End
         Begin VB.TextBox txtDatFimVigFatura 
            Alignment       =   1  'Right Justify
            Height          =   315
            Left            =   4320
            Locked          =   -1  'True
            TabIndex        =   93
            Top             =   300
            Width           =   1170
         End
         Begin VB.TextBox txtDatIniVigFatura 
            Alignment       =   1  'Right Justify
            Height          =   315
            Left            =   1530
            Locked          =   -1  'True
            TabIndex        =   92
            Top             =   300
            Width           =   1170
         End
         Begin VB.Label lblDesc 
            Caption         =   "Vencimento"
            Height          =   300
            Index           =   6
            Left            =   5670
            TabIndex        =   66
            Top             =   330
            Width           =   945
         End
         Begin VB.Label lblDesc 
            Caption         =   "Fim de Vig�ncia"
            Height          =   300
            Index           =   5
            Left            =   3060
            TabIndex        =   65
            Top             =   330
            Width           =   1125
         End
         Begin VB.Label lblDesc 
            Caption         =   "In�cio de Vig�ncia"
            Height          =   210
            Index           =   4
            Left            =   180
            TabIndex        =   64
            Top             =   330
            Width           =   1305
         End
      End
      Begin VB.Frame frObservacao 
         Caption         =   "Observa��es"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   2415
         Left            =   -74820
         TabIndex        =   67
         Top             =   5490
         Width           =   8115
         Begin VB.TextBox txtObservacao 
            Height          =   1995
            Left            =   180
            Locked          =   -1  'True
            MaxLength       =   60
            MultiLine       =   -1  'True
            TabIndex        =   68
            Top             =   270
            Width           =   7755
         End
      End
      Begin VB.Frame FraCongeneres 
         Caption         =   "Cedido"
         Height          =   2535
         Left            =   -74820
         TabIndex        =   69
         Top             =   810
         Width           =   8115
         Begin MSFlexGridLib.MSFlexGrid GridCongeneres 
            Height          =   1890
            Left            =   330
            TabIndex        =   70
            Top             =   330
            Width           =   7590
            _ExtentX        =   13388
            _ExtentY        =   3334
            _Version        =   393216
            Cols            =   3
            FixedCols       =   0
            FormatString    =   $"ConFatura.frx":0244
         End
      End
      Begin VB.Frame frPercentuais 
         Caption         =   "Percentuais"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   675
         Left            =   -74790
         TabIndex        =   42
         Top             =   1530
         Width           =   8085
         Begin VB.TextBox TxtPercAgenciamento 
            Alignment       =   1  'Right Justify
            Height          =   285
            Left            =   1290
            Locked          =   -1  'True
            MultiLine       =   -1  'True
            TabIndex        =   100
            Top             =   240
            Width           =   885
         End
         Begin VB.TextBox txtPercIR 
            Alignment       =   1  'Right Justify
            Height          =   285
            Left            =   7080
            Locked          =   -1  'True
            MultiLine       =   -1  'True
            TabIndex        =   50
            Top             =   240
            Width           =   585
         End
         Begin VB.TextBox txtPercCorretagem 
            Alignment       =   1  'Right Justify
            Height          =   285
            Left            =   5565
            Locked          =   -1  'True
            MultiLine       =   -1  'True
            TabIndex        =   47
            Top             =   240
            Width           =   885
         End
         Begin VB.TextBox txtPerProLabore 
            Alignment       =   1  'Right Justify
            Height          =   285
            Left            =   3390
            Locked          =   -1  'True
            MultiLine       =   -1  'True
            TabIndex        =   44
            Top             =   240
            Width           =   885
         End
         Begin VB.Label lblDesc 
            Caption         =   "Agenciamento :"
            Height          =   300
            Index           =   11
            Left            =   120
            TabIndex        =   102
            Top             =   270
            Width           =   1155
         End
         Begin VB.Label lblDesc 
            Caption         =   "%"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   10
            Left            =   2220
            TabIndex        =   101
            Top             =   270
            Width           =   195
         End
         Begin VB.Label lblDesc 
            Caption         =   "IR :"
            Height          =   300
            Index           =   29
            Left            =   6780
            TabIndex        =   49
            Top             =   270
            Width           =   255
         End
         Begin VB.Label lblDesc 
            Caption         =   "%"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   12
            Left            =   7710
            TabIndex        =   51
            Top             =   270
            Width           =   195
         End
         Begin VB.Label lblDesc 
            Caption         =   "%"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   28
            Left            =   6510
            TabIndex        =   48
            Top             =   270
            Width           =   195
         End
         Begin VB.Label lblDesc 
            Caption         =   "Corretagem :"
            Height          =   300
            Index           =   27
            Left            =   4590
            TabIndex        =   46
            Top             =   270
            Width           =   915
         End
         Begin VB.Label lblDesc 
            Caption         =   "%"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   26
            Left            =   4320
            TabIndex        =   45
            Top             =   270
            Width           =   195
         End
         Begin VB.Label lblDesc 
            Caption         =   "Pro Labore :"
            Height          =   300
            Index           =   24
            Left            =   2460
            TabIndex        =   43
            Top             =   270
            Width           =   885
         End
      End
      Begin VB.Frame FraAceito 
         Caption         =   "Aceito"
         Height          =   2055
         Left            =   -74820
         TabIndex        =   71
         Top             =   3510
         Width           =   8115
         Begin VB.TextBox txtDtIniVigLider 
            Height          =   315
            Left            =   6630
            Locked          =   -1  'True
            TabIndex        =   95
            Top             =   750
            Width           =   1140
         End
         Begin VB.TextBox TxtNoOrdem 
            Alignment       =   1  'Right Justify
            Height          =   285
            Left            =   1590
            Locked          =   -1  'True
            MaxLength       =   11
            MultiLine       =   -1  'True
            TabIndex        =   80
            Top             =   1470
            Width           =   1365
         End
         Begin VB.TextBox TxtEndossoLider 
            Alignment       =   1  'Right Justify
            Height          =   285
            Left            =   6420
            Locked          =   -1  'True
            MaxLength       =   11
            MultiLine       =   -1  'True
            TabIndex        =   82
            Top             =   1470
            Width           =   1365
         End
         Begin VB.TextBox TxtSegLider 
            Height          =   285
            Left            =   1590
            Locked          =   -1  'True
            MaxLength       =   60
            MultiLine       =   -1  'True
            TabIndex        =   73
            Top             =   270
            Width           =   6195
         End
         Begin VB.TextBox TxtApLider 
            Alignment       =   1  'Right Justify
            Height          =   285
            Left            =   3390
            Locked          =   -1  'True
            MaxLength       =   11
            MultiLine       =   -1  'True
            TabIndex        =   77
            Top             =   780
            Width           =   1365
         End
         Begin VB.TextBox TxtRamoLider 
            Alignment       =   1  'Right Justify
            Height          =   285
            Left            =   1590
            Locked          =   -1  'True
            MaxLength       =   2
            MultiLine       =   -1  'True
            TabIndex        =   75
            Top             =   780
            Width           =   435
         End
         Begin VB.Label lblDesc 
            Caption         =   "No. Ordem :"
            Height          =   225
            Index           =   41
            Left            =   480
            TabIndex        =   79
            Top             =   1500
            Width           =   1125
         End
         Begin VB.Label lblDesc 
            Caption         =   "Endosso L�der :"
            Height          =   225
            Index           =   42
            Left            =   5160
            TabIndex        =   81
            Top             =   1500
            Width           =   1125
         End
         Begin VB.Label lblDesc 
            Caption         =   "Seg. L�der :"
            Height          =   225
            Index           =   45
            Left            =   450
            TabIndex        =   72
            Top             =   300
            Width           =   1125
         End
         Begin VB.Label lblDesc 
            Caption         =   "Ap�lice L�der :"
            Height          =   225
            Index           =   46
            Left            =   2250
            TabIndex        =   76
            Top             =   810
            Width           =   1125
         End
         Begin VB.Label lblDesc 
            Caption         =   "In�cio de Vig�ncia :"
            Height          =   210
            Index           =   47
            Left            =   5130
            TabIndex        =   78
            Top             =   780
            Width           =   1455
         End
         Begin VB.Label lblDesc 
            Caption         =   "Ramo L�der :"
            Height          =   225
            Index           =   48
            Left            =   450
            TabIndex        =   74
            Top             =   810
            Width           =   1125
         End
      End
   End
   Begin MSComDlg.CommonDialog dialog 
      Left            =   600
      Top             =   8100
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
End
Attribute VB_Name = "ConFatura"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
'#
'# Sistema de Seguros : M�dulo de consulta a uma fatura.
'#

'' Constantes para tratamento dos tabs
Const TAB_FAT = 0
Const TAB_SEG = 1
Const TAB_VAL = 2
Const TAB_COSSEG = 3

'' Constantes para as colunas do grid de pro-labore
Const GRD_PL_COD = 0
Const GRD_PL_NOME = 1
Const GRD_PL_PERC = 2

'' Constante para verifica��o de %IR durante a vigencia da fatura.
Const CodItemFinanceiroIR As String = "IR"

Dim NumVidasFatura As Long
Dim DtIniVigFatura As String
Dim DtFimVigFatura As String
Dim DtVencFatura As String
Dim ObservacaoFatura As String
Dim PercCorretFatura As Double
Dim PercProLaboreFatura As Double
Dim DataIniVigenciaFatura As Date
Dim DataFimVigenciaFatura As Date

Dim FaturaAuto As Boolean
Dim SubGrupo As Integer

Dim lFaturaId As Long
Dim flagvidavida As Boolean

'Lsa - 06/11/2003
Dim sUsuarioEmissor As String

Private Function FormatValor(valor As Double) As String
    Dim F As Boolean

    If IsNull(valor) Then
        FormatValor = "0,00"
    Else
        FormatValor = Format(Val(valor), "###,##0.00")
    End If

End Function


Sub Imprime_Fatura(ByVal NoFatura As Long, ByVal NoApolice As Double, ByVal Sucursal As Integer, ByVal Seguradora As Integer, ByVal Ramo As Integer, ByVal TemSubGrupo As Boolean)
    Dim rs As rdoResultset
    Dim SQL As String
    Dim NomeSuc As String
    Dim selform As String
    Dim i As Integer

    On Error Resume Next

    dialog.CancelError = True
    dialog.ShowPrinter
    '
    If Err = 32755 Then
        MsgBox "Impress�o da Fatura Cancelada.", vbInformation
        Exit Sub
    Else
        On Error GoTo Cancela_Impressao
        '
        MousePointer = vbHourglass
        '


        Report.Reset
        Report.Connect = rdocn.Connect
        '        If TemSubGrupo Then
        '            Report.ReportFileName = App.PATH & "\faturasub.rpt"
        '        Else
        '            Report.ReportFileName = App.PATH & "\faturaapol.rpt"
        '        End If
        If TemSubGrupo Then
            Report.ReportFileName = App.PATH & "\rpt_relacao_fatura_sub.rpt"

            'Maur�cio(Stefanini), em 14/10/2005
            'Informar ao relat�rio que n�o � Vida-Vida e ent�o o texto Pr�mio L�quido ser�
            'alterado para Pr�mio Total.
            If Not flagvidavida Then
                Report.Formulas(0) = "Premio = 'Pr�mio Total'"
            End If

        Else
            Report.ReportFileName = App.PATH & "\rpt_relacao_fatura_apolices.rpt"
        End If
        '
        Report.StoredProcParam(0) = CStr(NoApolice)
        Report.StoredProcParam(1) = CStr(Sucursal)
        Report.StoredProcParam(2) = CStr(Seguradora)
        Report.StoredProcParam(3) = CStr(Ramo)
        Report.StoredProcParam(4) = CStr(NoFatura)
        '
        Report.Destination = crptToPrinter
        '' Imprime a fatura
        Report.Action = 1

        ''
        ' Imprimindo o anexo
        If FaturaAuto Then
            Report.Reset
            Report.Connect = rdocn.Connect
            Report.ReportFileName = App.PATH & "\SEGR0544-01.rpt"
            '
            Report.StoredProcParam(0) = CStr(NoApolice)
            Report.StoredProcParam(1) = CStr(Sucursal)
            Report.StoredProcParam(2) = CStr(Seguradora)
            Report.StoredProcParam(3) = CStr(Ramo)
            Report.StoredProcParam(4) = CStr(NoFatura)
            Report.Formulas(0) = "PercIOF = " & TrocaVirgulaPorPonto(txtPercIOF.Text)
            '
            Report.Destination = crptToPrinter

            Report.Action = 1

        End If

        'Alessandra Grig�rio - 12/08/2004 - Imprimir a capa da fatura
        Report.Reset
        Report.Connect = rdocn.Connect
        Report.ReportFileName = App.PATH & "\rpt_relacao_capa_fatura.rpt"
        Report.StoredProcParam(0) = CStr(NoApolice)
        Report.StoredProcParam(1) = CStr(Sucursal)
        Report.StoredProcParam(2) = CStr(Seguradora)
        Report.StoredProcParam(3) = CStr(Ramo)
        Report.StoredProcParam(4) = CStr(NoFatura)
        Report.Destination = crptToPrinter
        Report.Action = 1
        'FIM

    End If

    '---- Inicio das altera��es ( Eventos de impress�o on-line )
    '---- Stefanini 15/07/2005
    On Error GoTo Erro

    Set rs = rdocn.OpenResultset("exec evento_seguros_db..evento_impressao_spi " & _
                                 vPropostaId & _
                                 "," & Val(gEndossoId) & _
                                 ", null " & _
                                 ", '" & Usuario & "'" & _
                                 ", null " & _
                                 ", 23 " & _
                                 ", 'O'" & _
                                 ", 'IMPRESSAO DE FATURA DE PREMIO E VIDAS'" & _
                                 ", " & Val(vSubGrupoId) & _
                                 ", 'E'" & _
                                 ", null " & _
                                 ", null " & _
                                 ", '" & Usuario & "'" & _
                                 ", null" & _
                                 ", null")
    rs.Close
    '---- Fim das altera��es ( Eventos de impress�o on-line )
    '---- Stefanini 15/07/2005

    MousePointer = vbDefault
    Exit Sub

Cancela_Impressao:
    MousePointer = vbDefault
    MsgBox "Erro imprimindo fatura." & vbCrLf & "Rotina : Imprime_Fatura" & vbCrLf & "ERRO: " & Str(Err.Number) & " - " & Err.Description, vbCritical
    Exit Sub

Erro:
    MousePointer = vbDefault
    TrataErroGeral "Imprime_Fatura" & "ERRO: " & Str(Err.Number) & " - " & Err.Description
    MsgBox "Rotina: Imprime_Fatura. Programa ser� Cancelado" & "   ERRO: " & Str(Err.Number) & " - " & Err.Description, vbCritical
    End

End Sub

Sub Prepara_Cosseguro()
    Dim vSql1 As String
    Dim SQL As String
    Dim linha As String
    Dim rc As rdoResultset
    Dim rec As rdoResultset


    If gTpEmissao = "C" Then
        '  Recupera informa��es do cosseguro cedido.
        SQL = "SELECT cosr.rep_seguradora_cod_susep "
        SQL = SQL & ", seg.nome "
        SQL = SQL & ", cosr.dt_inicio_participacao "
        SQL = SQL & ", cosr.dt_fim_participacao "
        SQL = SQL & ", isnull(cosr.perc_participacao,0) perc_participacao "
        SQL = SQL & ", isnull(cosr.perc_despesa_lider,0) perc_despesa_lider "
        SQL = SQL & ", cosr.num_ordem "
        SQL = SQL & "FROM co_seguro_repassado_tb cosr   WITH (NOLOCK) "
        SQL = SQL & ", seguradora_tb seg   WITH (NOLOCK) "
        SQL = SQL & "WHERE cosr.proposta_id = " & vPropostaId
        SQL = SQL & " AND cosr.dt_inicio_participacao <= '" & Format(DtIniVigFatura, "yyyymmdd") & "'"
        SQL = SQL & " AND (cosr.dt_fim_participacao is null or cosr.dt_fim_participacao > '" & Format(DtIniVigFatura, "yyyymmdd") & "')"
        SQL = SQL & " AND seg.seguradora_cod_susep = cosr.rep_seguradora_cod_susep "
        '
        Set rec = rdoCn1.OpenResultset(SQL, rdOpenStatic)

        While Not rec.EOF
            linha = Trim(rec("nome")) & vbTab
            linha = linha & FormatValor(rec("perc_participacao")) & vbTab
            'linha = linha & FormatValor(rec(""perc_despesa_lider"")) - Monica.Randolfi - Confitec - 11/01/2016 - INC000004806015
            linha = linha & Format(CCur(Replace(("0" & rec("perc_despesa_lider")), ".", ",")), "###,###,##0.00")
            'Monica.Randolfi - Confitec - 11/01/2016 - INC000004806015



            GridCongeneres.AddItem linha
            GridCongeneres.RowData(GridCongeneres.Rows - 1) = rec!rep_seguradora_cod_susep
            rec.MoveNext
        Wend
    ElseIf gTpEmissao = "A" Then   ' Recupera informa��es do cosseguro aceito.
        SQL = "SELECT num_ordem_co_seguro_aceito, "
        SQL = SQL & "num_endosso_cosseguro "
        SQL = SQL & "FROM endosso_co_seguro_aceito_tb  WITH (NOLOCK)  "
        SQL = SQL & "WHERE proposta_id = " & vPropostaId
        SQL = SQL & "AND   endosso_id = " & gEndossoId

        Set rc = rdoCn1.OpenResultset(SQL, rdOpenStatic)


        ''' altera��o feita por Leandro A.Souza (Stefanini), em 13/03/2006 - Flow 123272
        ''' corre��o na exibi��o dos campos de tela "Endosso L�der" e "No. ordem"
        If Not rc.EOF Then
            If Not IsNull(rc!num_endosso_cosseguro) Then
                Me.TxtEndossoLider.Text = "" & rc!num_endosso_cosseguro
            Else
                TxtEndossoLider.Text = ""
            End If
            If Not IsNull(rc!num_ordem_co_seguro_aceito) Then
                Me.TxtNoOrdem.Text = "" & rc!num_ordem_co_seguro_aceito
            Else
                Me.TxtNoOrdem.Text = ""
            End If
        End If


        ''''''

        Set rc = Nothing

        SQL = "SELECT a.seg_cod_susep_lider, a.sucursal_seg_lider, s.nome, c.ramo_lider "
        SQL = SQL & ", c.num_apolice_lider, c.dt_emissa_apolice_lider, c.num_ordem_co_seguro_aceito "
        SQL = SQL & "FROM apolice_terceiros_tb a  WITH (NOLOCK) , co_seguro_aceito_tb c  WITH (NOLOCK) , seguradora_tb s   WITH (NOLOCK) "
        SQL = SQL & "WHERE a.apolice_id = " & vApoliceId
        SQL = SQL & " AND a.ramo_id = " & vRamoId
        SQL = SQL & " AND a.seguradora_cod_susep = " & vSeguradoraId
        SQL = SQL & " AND a.sucursal_seguradora_id = " & vSucursalId
        SQL = SQL & " AND c.num_ordem_co_seguro_aceito = a.num_ordem_co_seguro_aceito "
        SQL = SQL & " AND c.sucursal_seg_lider = a.sucursal_seg_lider "
        SQL = SQL & " AND c.seg_cod_susep_lider = a.seg_cod_susep_lider "
        SQL = SQL & " AND s.seguradora_cod_susep = a.seg_cod_susep_lider "

        Set rc = rdoCn1.OpenResultset(SQL, rdOpenStatic)

        If Not rc.EOF Then
            If Not IsNull(rc!dt_emissa_apolice_lider) Then
                txtDtIniVigLider.Text = Format(rc!dt_emissa_apolice_lider, "dd/mm/yyyy")
            End If
            '
            If Not IsNull(rc!Nome) Then
                TxtSegLider.Text = Trim(rc!Nome)
            End If
            '
            If Not IsNull(rc!num_apolice_lider) Then
                TxtApLider.Text = rc!num_apolice_lider
            End If
            '
            If Not IsNull(rc!ramo_lider) Then
                TxtRamoLider.Text = rc!ramo_lider
            End If
        End If
    End If

End Sub

Sub Prepara_Fatura()

    Dim vSubGrupo As SubGrupo
    Dim rc As rdoResultset
    Dim SQL As String
    Dim linha As String
    Dim PercCorretagem As Double
    Dim ValComissao As Double
    Dim ValIR As Double
    Dim PercPL As String
    Dim NumVidas As Long    '* LONG -> 4 bytes A long integer between ���2,147,483,648 and 2,147,483,647.
    Dim NumVidasInc As Long
    Dim NumVidasAumento As Long
    Dim NumVidasExc As Long
    Dim NumVidasAcerto As Long
    Dim NumVidasCob As Long    'Alessandra - 21/06/2004
    Dim ValIS As Double             '* DOUBLE -> 8 bytes A double-precision floating-point value with a range of ���1.79769313486232E308 to ���4.94065645841247E-324 for negative values, 4.94065645841247E-324 to 1.79769313486232E308 for positive values, and 0.
    Dim ValIsInc As Double
    Dim ValIsAumento As Double
    Dim ValIsExc As Double
    Dim ValIsAcerto As Double
    Dim ValIsCob As Double    'Alessandra - 21/06/2004
    Dim ValPremio As Double
    Dim ValPremioInc As Double
    Dim ValPremioAumento As Double
    Dim ValPremioExc As Double
    Dim ValPremioAcerto As Double
    Dim ValPremioCob As Double    'Alessandra - 21/06/2004
    '----------------------------------------------------------------------------------------------
    '-- C00157284 - Eliminar pend�ncias a cada movimenta��o e criar hist�rico no Vida Web  - INICIO
    Dim rcPendentes As rdoResultset
    Dim num_VidasInclNaoProcessadas As Long
    Dim val_ISInclNaoProcessadas As Double
    Dim val_PremioInclNaoProcessadas As Double
    Dim num_VidasAltNaoProcessadas As Long
    Dim val_ISAltNaoProcessadas As Double
    Dim val_PremioAltNaoProcessadas As Double
    '-- C00157284 - Eliminar pend�ncias a cada movimenta��o e criar hist�rico no Vida Web  - FIM
    '----------------------------------------------------------------------------------------------

    '* Recupera os dados da fatura.
    SQL = "SELECT fatura_id "
    SQL = SQL & ", isnull(dt_inicio_vigencia, ' ') dt_inicio_vigencia "
    SQL = SQL & ", isnull(dt_fim_vigencia, ' ') dt_fim_vigencia "
    SQL = SQL & ", isnull(dt_recebimento, ' ') dt_recebimento "
    SQL = SQL & ", isnull(perc_corretagem,0) perc_corretagem "
    SQL = SQL & ", isnull(perc_pro_labore,0) perc_pro_labore "
    SQL = SQL & ", isnull(observacao, ' ') observacao "
    SQL = SQL & ", sub_grupo_id "
    'Lsa - 06/11/2003
    SQL = SQL & ", usuario_emissor "
    SQL = SQL & " FROM fatura_tb   WITH (NOLOCK) "
    SQL = SQL & " WHERE proposta_id = " & vPropostaId
    SQL = SQL & " AND endosso_id = " & gEndossoId
    Set rc = rdoCn1.OpenResultset(SQL, rdOpenStatic)
    '
    If Not rc.EOF Then
        lFaturaId = rc!fatura_id
        DtIniVigFatura = Format(rc!dt_inicio_vigencia, "dd/mm/yyyy")
        DataIniVigenciaFatura = rc!dt_inicio_vigencia
        DtFimVigFatura = Format(rc!dt_fim_vigencia, "dd/mm/yyyy")
        DataFimVigenciaFatura = rc!dt_fim_vigencia
        DtVencFatura = Format(rc!dt_recebimento, "dd/mm/yyyy")
        ObservacaoFatura = Trim(rc!Observacao)
        PercCorretFatura = Val(rc!perc_corretagem)
        PercProLaboreFatura = Val(rc!perc_pro_labore)

        'Lsa - 06/11/2003
        sUsuarioEmissor = IIf(IsNull(rc!usuario_emissor), "", Trim(rc!usuario_emissor))
        '
        If Not IsNull(rc!sub_grupo_id) Then
            For Each vSubGrupo In SubGrupos
                If vSubGrupo.Id = rc!sub_grupo_id Then
                    FaturaAuto = vSubGrupo.FaturaAuto
                    SubGrupo = vSubGrupo.Id
                    Exit For
                End If
            Next
        Else
            FaturaAuto = False
        End If
        '
        rc.Close
    Else

        FaturaAuto = False
        lFaturaId = 0
        DtIniVigFatura = gDtIniVigEndosso
        DtFimVigFatura = gDtIniVigEndosso

        'Lsa - 06/11/2003
        sUsuarioEmissor = ""

    End If
    Set rc = Nothing

    'Recupera dados dos corretores.
    SQL = " SELECT  cpf.corretor_id corretor_id, sucursal_corretor_id = ''"
    SQL = SQL & "   , c.nome, isnull(cpf.val_comissao, 0) val_comissao, 0 val_ir"
    SQL = SQL & "   , isnull(cpf.perc_corretagem, 0) perc_corretagem "
    SQL = SQL & " FROM  fatura_tb fa   WITH (NOLOCK) "
    SQL = SQL & " RIGHT OUTER JOIN corretagem_pf_endosso_fin_tb cpf  WITH (NOLOCK) "
    SQL = SQL & "   ON  fa.proposta_id = cpf.proposta_id"
    SQL = SQL & "   AND fa.endosso_id = cpf.endosso_id"
    SQL = SQL & " INNER JOIN corretor_tb c  WITH (NOLOCK) "
    SQL = SQL & "   ON  c.corretor_id = cpf.corretor_id"
    SQL = SQL & " WHERE cpf.proposta_id = " & vPropostaId
    SQL = SQL & " AND   cpf.endosso_id = " & gEndossoId
    SQL = SQL & " UNION "
    SQL = SQL & " SELECT  cpj.corretor_id corretor_id, sucursal_corretor_id"
    SQL = SQL & "   , c.nome, isnull(cpj.val_comissao, 0) val_comissao, isnull(cpj.val_ir, 0) val_ir"
    SQL = SQL & "   , isnull(cpj.perc_corretagem, 0) perc_corretagem "
    SQL = SQL & " FROM  fatura_tb fa   WITH (NOLOCK) "
    SQL = SQL & " RIGHT OUTER JOIN corretagem_pj_endosso_fin_tb cpj  WITH (NOLOCK) "
    SQL = SQL & "   ON  fa.proposta_id = cpj.proposta_id"
    SQL = SQL & "   AND fa.endosso_id = cpj.endosso_id"
    SQL = SQL & " INNER JOIN corretor_tb c  WITH (NOLOCK) "
    SQL = SQL & "   ON  c.corretor_id = cpj.corretor_id"
    SQL = SQL & " WHERE cpj.proposta_id = " & vPropostaId
    SQL = SQL & " AND   cpj.endosso_id = " & gEndossoId
    ''
    SQL = SQL & " ORDER BY corretor_id "

    Set rc = rdoCn1.OpenResultset(SQL, rdOpenStatic)

    While Not rc.EOF

        If ConfiguracaoBrasil Then
            ValComissao = Format(Val(rc!val_comissao), "###,###,##0.00")
            ValIR = Format(Val(rc!val_ir), "###,###,##0.00")
            PercCorretagem = Format(Val(rc!perc_corretagem), "###,###,##0.00")
        Else
            ValComissao = TrocaValorAmePorBras(Format(rc!val_comissao, "###,###,##0.00"))
            ValIR = TrocaValorAmePorBras(Format(rc!val_ir, "###,###,##0.00"))
            PercCorretagem = TrocaValorAmePorBras(Format(rc!perc_corretagem, "###,###,##0.00"))
        End If

        '  Monta linha - dados do corretor.
        linha = Format(rc!Corretor_id, "######-###") & vbTab
        linha = linha & rc!sucursal_corretor_id & vbTab
        linha = linha & Trim(rc!Nome) & vbTab
        linha = linha & PercCorretagem & vbTab
        linha = linha & ValComissao & vbTab
        linha = linha & ValIR & vbTab

        '  Inclui registros na GridCorretagem ...
        GridCorretagem.AddItem linha
        GridCorretagem.RowData(GridCorretagem.Rows - 1) = rc!Corretor_id

        rc.MoveNext
    Wend
    '
    Set rc = Nothing

    '' Recupera os dados de pr�-labore
    SQL = "SELECT plf.cliente_id, cli.nome, isnull(plf.perc_pro_labore, 0) perc_pro_labore "
    SQL = SQL & "FROM pro_labore_endosso_fin_tb plf   WITH (NOLOCK) "
    SQL = SQL & "INNER JOIN cliente_tb cli   WITH (NOLOCK) "
    SQL = SQL & "   ON  cli.cliente_id = plf.cliente_id "
    SQL = SQL & "WHERE plf.proposta_id = " & vPropostaId
    SQL = SQL & " AND plf.endosso_id = " & gEndossoId
    '
    Set rc = rdoCn1.OpenResultset(SQL, rdOpenStatic)

    While Not rc.EOF
        If ConfiguracaoBrasil Then
            PercPL = Format(Val(rc!perc_pro_labore), "0.00")
        Else
            PercPL = TrocaValorAmePorBras(Format(rc!perc_pro_labore, "0.00"))
        End If

        '' Inclui registros na grid de Pr�-labore.
        linha = rc!Cliente_id
        linha = linha & vbTab & Trim(rc!Nome)
        linha = linha & vbTab & PercPL
        '
        GridProLabore.AddItem linha
        GridProLabore.RowData(GridProLabore.Rows - 1) = rc!Cliente_id
        '
        rc.MoveNext
    Wend
    Set rc = Nothing

    '' Comentado at� ser liberado para testes
    '''' Recupera os valores de agenciamento
    ''sql = "SELECT aef.agenciador_id, a.nome, aef.perc_agenciamento, aef.val_agenciamento"
    ''sql = sql & ", aef.no_parcelas, aef.forma_pgto "
    ''sql = sql & " FROM agenciamento_endosso_fin_tb aef INNER JOIN agenciador_tb a "
    ''sql = sql & " ON aef.agenciador_id = a.agenciador_id "
    ''sql = sql & " WHERE aef.proposta_id = " & vPropostaId
    ''sql = sql & " AND aef.endosso_id = " & gEndossoId
    ''
    ''Set rc = rdoCn1.OpenResultset(sql, rdOpenStatic)
    ''If Not rc.EOF Then
    ''    TxtCodAgenciador.Text = Format(rc!agenciador_id, "###000000")
    ''    TxtAgenciador.Text = Trim(rc!Nome)
    ''    TxtFPAgenciamento.Text = FP_Agenciamento(rc!Forma_Pgto)
    ''    TxtNoParcAgenciamento.Text = rc!no_parcelas
    ''    If ConfiguracaoBrasil Then
    ''        TxtPercAg.Text = Format(Val(rc!perc_Agenciamento), "#0.00")
    ''        TxtValAgenciamento.Text = Format(Val(rc!val_agenciamento), "#0.00")
    ''    Else
    ''        TxtPercAg.Text = Format(rc!perc_Agenciamento, "#0.00")
    ''        TxtValAgenciamento.Text = Format(rc!val_agenciamento, "#0.00")
    ''    End If
    ''    TxtPercAgenciamento.Text = TxtPercAg.Text
    ''    '
    ''    rc.Close
    ''End If
    ''Set rc = Nothing


    ' Recupera campos do Historico.
    If lFaturaId > 0 Then
        SQL = "SELECT isnull(num_vidas,0) num_vidas "
        SQL = SQL & ", isnull(val_is,0) val_is"
        SQL = SQL & ", isnull(val_premio,0) val_premio "
        SQL = SQL & ", isnull(num_vidas_inc,0) num_vidas_inc "
        SQL = SQL & ", isnull(val_is_inc,0) val_is_inc "
        SQL = SQL & ", isnull(val_premio_inc,0) val_premio_inc "
        SQL = SQL & ", isnull(num_vidas_aumento,0) num_vidas_aumento "
        SQL = SQL & ", isnull(val_is_aumento,0) val_is_aumento "
        SQL = SQL & ", isnull(val_premio_aumento,0) val_premio_aumento "
        SQL = SQL & ", isnull(num_vidas_exc,0) num_vidas_exc "
        SQL = SQL & ", isnull(val_is_exc,0) val_is_exc "
        SQL = SQL & ", isnull(val_premio_exc,0) val_premio_exc "
        SQL = SQL & ", isnull(num_vidas_acerto,0) num_vidas_acerto "
        SQL = SQL & ", isnull(val_is_acerto,0) val_is_acerto "
        SQL = SQL & ", isnull(val_premio_acerto,0) val_premio_acerto "
        'Alessandra Grig�rio - 21/06/2004
        SQL = SQL & ", isnull(num_vidas_cob_provisoria,0) num_vidas_cob_provisoria "
        SQL = SQL & ", isnull(val_is_cob_provisoria,0) val_is_cob_provisoria "
        SQL = SQL & ", isnull(val_premio_cob_provisoria,0) val_premio_cob_provisoria "

        SQL = SQL & " FROM fatura_movimentacao_vidas_tb   WITH (NOLOCK) "
        SQL = SQL & " WHERE apolice_id = " & vApoliceId
        SQL = SQL & " AND sucursal_seguradora_id = " & vSucursalId
        SQL = SQL & " AND seguradora_cod_susep = " & vSeguradoraId
        SQL = SQL & " AND ramo_id = " & vRamoId
        SQL = SQL & " AND fatura_id = " & lFaturaId

        Set rc = rdoCn1.OpenResultset(SQL, rdOpenStatic)

        If Not rc.EOF Then
            '  Prepara campos de historico para exibicao.
            NumVidas = rc!num_vidas
            NumVidasInc = rc!num_vidas_inc
            NumVidasAumento = rc!num_vidas_aumento
            NumVidasExc = rc!num_vidas_exc
            NumVidasAcerto = rc!num_vidas_acerto
            NumVidasCob = rc!num_vidas_cob_provisoria

            'NumVidasFatura = NumVidas + NumVidasInc + NumVidasAumento + NumVidasAcerto - NumVidasExc
            NumVidasFatura = NumVidas + NumVidasInc - NumVidasExc + NumVidasCob
            '
            ValIS = Val(rc!val_is)
            ValIsInc = Val(rc!val_is_inc)
            ValIsAumento = Val(rc!val_is_aumento)
            ValIsExc = Val(rc!val_is_exc)
            ValIsAcerto = Val(rc!val_is_acerto)
            ValIsCob = Val(rc!val_is_cob_provisoria)
            '
            ValPremio = Val(rc!val_Premio)
            ValPremioInc = Val(rc!val_premio_inc)
            ValPremioAumento = Val(rc!val_premio_aumento)
            ValPremioExc = Val(rc!val_premio_exc)
            ValPremioAcerto = Val(rc!val_premio_acerto)
            ValPremioCob = Val(rc!val_premio_cob_provisoria)
            '----------------------------------------------------------------------------------------------
            '-- C00157284 - Eliminar pend�ncias a cada movimenta��o e criar hist�rico no Vida Web  - INICIO
            'Inclus�es pendentes
            SQL = "select count(isnull(prop_cliente_id,1)) as vidas, sum(val_capital_segurado) as val_capital_segurado, 0 as premio"
            SQL = SQL & " From "
            SQL = SQL & " [SISAB013\WEB].vida_web_db.dbo.historico_movimentacao_pendente_tb "
            SQL = SQL & " Where "
            SQL = SQL & " ind_operacao_origem = 'I' "
            SQL = SQL & " AND apolice_id = " & vApoliceId
            SQL = SQL & " AND seguradora_cod_susep = " & vSeguradoraId
            SQL = SQL & " AND sucursal_seguradora_id = " & vSucursalId
            SQL = SQL & " AND ramo_id = " & vRamoId
            SQL = SQL & " AND fatura_id = " & lFaturaId
            Set rcPendentes = rdoCn1.OpenResultset(SQL, rdOpenStatic)
            If Not rcPendentes.EOF Then
                num_VidasInclNaoProcessadas = Val(IIf(IsNull(rcPendentes!vidas), 0, rcPendentes!vidas))
                val_ISInclNaoProcessadas = Val(IIf(IsNull(rcPendentes!val_capital_segurado), 0, rcPendentes!val_capital_segurado))
                val_PremioInclNaoProcessadas = Val(IIf(IsNull(rcPendentes!Premio), 0, rcPendentes!Premio))
            End If
            
            'Altera��es pendentes
            rcPendentes.Close
            Set rcPendentes = Nothing
            SQL = "select count(isnull(prop_cliente_id,1)) as vidas, sum(val_capital_segurado) as val_capital_segurado, 0 as premio"
            SQL = SQL & " From "
            SQL = SQL & " [SISAB013\WEB].vida_web_db.dbo.historico_movimentacao_pendente_tb "
            SQL = SQL & " Where "
            SQL = SQL & " ind_operacao_origem = 'A' "
            SQL = SQL & " AND apolice_id = " & vApoliceId
            SQL = SQL & " AND seguradora_cod_susep = " & vSeguradoraId
            SQL = SQL & " AND sucursal_seguradora_id = " & vSucursalId
            SQL = SQL & " AND ramo_id = " & vRamoId
            SQL = SQL & " AND fatura_id = " & lFaturaId
            Set rcPendentes = rdoCn1.OpenResultset(SQL, rdOpenStatic)
            If Not rcPendentes.EOF Then
                num_VidasAltNaoProcessadas = Val(IIf(IsNull(rcPendentes!vidas), 0, rcPendentes!vidas))
                val_ISAltNaoProcessadas = Val(IIf(IsNull(rcPendentes!val_capital_segurado), 0, rcPendentes!val_capital_segurado))
                val_PremioAltNaoProcessadas = Val(IIf(IsNull(rcPendentes!Premio), 0, rcPendentes!Premio))
            End If
            '----
            '-- C00157284 - Eliminar pend�ncias a cada movimenta��o e criar hist�rico no Vida Web  - FIM
            '----------------------------------------------------------------------------------------------
            ''   Formatando os campos de saida.
            If ConfiguracaoBrasil Then
                TxtVidas.Text = Format(NumVidas, "#,###,###,##0")
                TxtVidasInc.Text = Format(NumVidasInc, "#,###,###,##0")
                TxtVidasCap.Text = Format(NumVidasAumento, "#,###,###,##0")
                TxtVidasExc.Text = Format(NumVidasExc, "#,###,###,##0")
                TxtVidasAcerto.Text = Format(NumVidasAcerto, "#,###,###,##0")
                TxtVidasCob.Text = Format(NumVidasCob, "#,###,###,##0")
                '
                TxtValIS.Text = Format(ValIS, "#,###,###,###,##0.00")
                TxtValISInc.Text = Format(ValIsInc, "#,###,###,###,##0.00")
                TxtValISCap.Text = Format(ValIsAumento, "#,###,###,###,##0.00")
                TxtValISExc.Text = Format(ValIsExc, "#,###,###,###,##0.00")
                TxtValISAcerto.Text = Format(ValIsAcerto, "#,###,###,###,##0.00")
                TxtValISCob.Text = Format(ValIsCob, "#,###,###,###,##0.00")
                '
                TxtValPremio.Text = Format(ValPremio, "#,###,###,###,##0.00")
                TxtValPremioInc.Text = Format(ValPremioInc, "#,###,###,###,##0.00")
                TxtValPremioCap.Text = Format(ValPremioAumento, "#,###,###,###,##0.00")
                TxtValPremioExc.Text = Format(ValPremioExc, "#,###,###,###,##0.00")
                TxtValPremioAcerto.Text = Format(ValPremioAcerto, "#,###,###,###,##0.00")
                TxtValPremioCob.Text = Format(ValPremioCob, "#,###,###,###,##0.00")
                
                '----------------------------------------------------------------------------------------------
                '-- C00157284 - Eliminar pend�ncias a cada movimenta��o e criar hist�rico no Vida Web  - INICIO
                txtVidasInclNaoProcessadas.Text = Format(num_VidasInclNaoProcessadas, "#,###,###,##0")
                txtValISInclNaoProcessadas.Text = Format(val_ISInclNaoProcessadas, "#,###,###,###,##0.00")
                txtValPremioInclNaoProcessadas.Text = Format(val_PremioInclNaoProcessadas, "#,###,###,###,##0.00")
                txtVidasAltNaoProcessadas.Text = Format(num_VidasAltNaoProcessadas, "#,###,###,##0")
                txtValISAltNaoProcessadas.Text = Format(val_ISAltNaoProcessadas, "#,###,###,###,##0.00")
                txtValPremioAltNaoProcessadas.Text = Format(val_PremioAltNaoProcessadas, "#,###,###,###,##0.00")
                '-- C00157284 - Eliminar pend�ncias a cada movimenta��o e criar hist�rico no Vida Web  - FIM
                '----------------------------------------------------------------------------------------------
            Else
                TxtVidas.Text = TrocaValorAmePorBras(Format(NumVidas, "#,###,###,##0"))
                TxtVidasInc.Text = TrocaValorAmePorBras(Format(NumVidasInc, "#,###,###,##0"))
                TxtVidasCap.Text = TrocaValorAmePorBras(Format(NumVidasAumento, "#,###,###,##0"))
                TxtVidasExc.Text = TrocaValorAmePorBras(Format(NumVidasExc, "#,###,###,##0"))
                TxtVidasAcerto.Text = TrocaValorAmePorBras(Format(NumVidasAcerto, "#,###,###,##0"))
                TxtVidasCob.Text = TrocaValorAmePorBras(Format(NumVidasCob, "#,###,###,##0"))
                '
                TxtValIS.Text = TrocaValorAmePorBras(Format(ValIS, "#,###,###,###,##0.00"))
                TxtValISInc.Text = TrocaValorAmePorBras(Format(ValIsInc, "#,###,###,###,##0.00"))
                TxtValISCap.Text = TrocaValorAmePorBras(Format(ValIsAumento, "#,###,###,###,##0.00"))
                TxtValISExc.Text = TrocaValorAmePorBras(Format(ValIsExc, "#,###,###,###,##0.00"))
                TxtValISAcerto.Text = TrocaValorAmePorBras(Format(ValIsAcerto, "#,###,###,###,##0.00"))
                TxtValISCob.Text = TrocaValorAmePorBras(Format(ValIsCob, "#,###,###,###,##0.00"))
                '
                TxtValPremio.Text = TrocaValorAmePorBras(Format(ValPremio, "#,###,###,###,##0.00"))
                TxtValPremioInc.Text = TrocaValorAmePorBras(Format(ValPremioInc, "#,###,###,###,##0.00"))
                TxtValPremioCap.Text = TrocaValorAmePorBras(Format(ValPremioAumento, "#,###,###,###,##0.00"))
                TxtValPremioExc.Text = TrocaValorAmePorBras(Format(ValPremioExc, "#,###,###,###,##0.00"))
                TxtValPremioAcerto.Text = TrocaValorAmePorBras(Format(ValPremioAcerto, "#,###,###,###,##0.00"))
                TxtValPremioCob.Text = TrocaValorAmePorBras(Format(ValPremioCob, "#,###,###,###,##0.00"))
                
                '----------------------------------------------------------------------------------------------
                '-- C00157284 - Eliminar pend�ncias a cada movimenta��o e criar hist�rico no Vida Web  - INICIO
                txtVidasInclNaoProcessadas.Text = TrocaValorAmePorBras(Format(num_VidasInclNaoProcessadas, "#,###,###,##0"))
                txtValISInclNaoProcessadas.Text = TrocaValorAmePorBras(Format(val_ISInclNaoProcessadas, "#,###,###,###,##0.00"))
                txtValPremioInclNaoProcessadas.Text = TrocaValorAmePorBras(Format(val_PremioInclNaoProcessadas, "#,###,###,###,##0.00"))
                txtVidasAltNaoProcessadas.Text = TrocaValorAmePorBras(Format(num_VidasAltNaoProcessadas, "#,###,###,##0"))
                txtValISAltNaoProcessadas.Text = TrocaValorAmePorBras(Format(val_ISAltNaoProcessadas, "#,###,###,###,##0.00"))
                txtValPremioAltNaoProcessadas.Text = TrocaValorAmePorBras(Format(val_PremioAltNaoProcessadas, "#,###,###,###,##0.00"))
                '-- C00157284 - Eliminar pend�ncias a cada movimenta��o e criar hist�rico no Vida Web  - FIM
                '----------------------------------------------------------------------------------------------
                
            End If
            '
        End If    'se not rc.eof
    End If    'se fatura > 0

End Sub

Sub Prepara_Segurados()
    Dim linha As String
    Dim SQL As String
    Dim CapitalSaida As String
    Dim SalarioSaida As String
    Dim Premio As String
    Dim rc As rdoResultset

    flagvidavida = False

    If lFaturaId > 0 Then

        GridSegurados.Rows = 1

        
        SQL = "SELECT cl.nome " & vbNewLine
        SQL = SQL & "   ,isnull(fs.prop_cliente_id,0)  as prop_cliente_id " & vbNewLine
        SQL = SQL & "   ,isnull(dt_entrada_sub_grupo,'') dt_entrada_sub " & vbNewLine
        SQL = SQL & "   ,isnull(dt_ini_vig_seg,'') dt_ini_vig_seg " & vbNewLine
        SQL = SQL & "   ,isnull(capital_seg,0) capital_seg " & vbNewLine
        SQL = SQL & "   ,isnull(fs.premio_seg,0) as premio " & vbNewLine
        SQL = SQL & "   ,isnull(salario_seg,0) salario_seg " & vbNewLine
        SQL = SQL & " FROM fatura_segurado_tb fs   WITH (NOLOCK) " & vbNewLine
        SQL = SQL & " INNER JOIN cliente_tb cl   WITH (NOLOCK) " & vbNewLine
        SQL = SQL & "   ON  cl.cliente_id = fs.prop_cliente_id " & vbNewLine
        SQL = SQL & " WHERE fs.apolice_id = " & vApoliceId & vbNewLine
        SQL = SQL & "   AND fs.seguradora_cod_susep = " & vSeguradoraId & vbNewLine
        SQL = SQL & "   AND fs.sucursal_seguradora_id = " & vSucursalId & vbNewLine
        SQL = SQL & "   AND fs.ramo_id = " & vRamoId & vbNewLine
        SQL = SQL & "   AND fs.fatura_id = " & lFaturaId & vbNewLine
        SQL = SQL & "   AND fs.prop_cliente_id is not null " & vbNewLine & vbNewLine
        '----------------------------------------------------------------------------------------------
        '-- C00157284 - Eliminar pend�ncias a cada movimenta��o e criar hist�rico no Vida Web  - INICIO
        SQL = SQL & "UNION " & vbNewLine & vbNewLine
        SQL = SQL & " SELECT isnull(cl.nome,h.nome)," & vbNewLine
        SQL = SQL & "   isnull(prop_cliente_id,0)  as prop_cliente_id," & vbNewLine
        SQL = SQL & "   dt_entrada_sbg," & vbNewLine
        SQL = SQL & "   CASE ind_operacao" & vbNewLine
        SQL = SQL & "       WHEN 'I'" & vbNewLine
        SQL = SQL & "           THEN NULL" & vbNewLine
        SQL = SQL & "       ELSE dt_inicio_vigencia" & vbNewLine
        SQL = SQL & "       END AS dt_inicio_vigencia," & vbNewLine
        SQL = SQL & "   val_capital_segurado," & vbNewLine
        SQL = SQL & "   0 premio," & vbNewLine
        SQL = SQL & "   val_salario" & vbNewLine
        SQL = SQL & " FROM [SISAB013\WEB].vida_web_db.dbo.historico_movimentacao_pendente_tb h" & vbNewLine
        SQL = SQL & " LEFT JOIN seguros_db.dbo.cliente_tb cl WITH (NOLOCK)" & vbNewLine
        SQL = SQL & "   ON cl.cliente_id = h.prop_cliente_id" & vbNewLine
        SQL = SQL & " WHERE apolice_id =" & vApoliceId & vbNewLine
        SQL = SQL & "   AND seguradora_cod_susep =" & vSeguradoraId & vbNewLine
        SQL = SQL & "   AND sucursal_seguradora_id =" & vSucursalId & vbNewLine
        SQL = SQL & "   AND ramo_id =" & vRamoId & vbNewLine
        SQL = SQL & "   AND fatura_id =" & lFaturaId & vbNewLine
        '-- C00157284 - Eliminar pend�ncias a cada movimenta��o e criar hist�rico no Vida Web  - FIM
        '----------------------------------------------------------------------------------------------
        SQL = SQL & " ORDER BY cl.nome " & vbNewLine

        Set rc = rdoCn1.OpenResultset(SQL, rdOpenStatic)

        While Not rc.EOF

            If ConfiguracaoBrasil Then
                'Stefanini (Maur�cio), em 27/07/2005 para entrar na mesma condi��o da verifica��o
                'da ap�lice da Alian�a do Brasil.
                'CapitalSaida = Format(Val(rc!capital_seg), "#,###,###,###,##0.00")

                'myoshimura
                '28/01/2005
                'se for apolice 13231 (Alian�a do Brasil) os sal�rios n�o dever�o ser exibidos.
                If vApoliceId = 13231 And vRamoId = 93 Then
                    SalarioSaida = Format(0, "#,###,###,###,##0.00")
                    CapitalSaida = Format(0, "#,###,###,###,##0.00")
                Else
                    SalarioSaida = Format(Val(rc!salario_seg), "#,###,###,###,##0.00")
                    CapitalSaida = Format(Val(rc!capital_seg), "#,###,###,###,##0.00")
                End If
                '---- Vagner Vilela NTendencia - 23/07/2019
                Premio = Format(Val(IIf(IsNull(rc!Premio), 0, rc!Premio)), "#,###,###,###,##0.00")
                '----
            Else
                CapitalSaida = TrocaValorAmePorBras(Format(Val(rc!capital_seg), "#,###,###,###,##0.00"))
                'Vagner Vilela NTendencia - 23/07/2019
                Premio = TrocaValorAmePorBras(Format(Val(rc!Premio), "#,###,###,###,##0.00"))

                If vApoliceId = 13231 And vRamoId = 93 Then
                    SalarioSaida = TrocaValorAmePorBras(Format(Val(rc!salario_seg), "#,###,###,###,##0.00"))
                Else
                    SalarioSaida = TrocaValorAmePorBras(Format(0, "#,###,###,###,##0.00"))
                End If
            End If

            linha = Trim(rc!Nome) & vbTab
            linha = linha & Format(rc!dt_entrada_sub, "dd/mm/yyyy") & vbTab
            linha = linha & Format(rc!dt_ini_vig_seg, "dd/mm/yyyy") & vbTab
            linha = linha & CapitalSaida & vbTab
            linha = linha & SalarioSaida & vbTab
            'Vagner Vilela NTendencia - 23/07/2019
            linha = linha & Premio & vbTab

            '  Inclui registros na GridSegurados ...
            GridSegurados.AddItem linha
            GridSegurados.RowData(GridSegurados.Rows - 1) = rc!prop_cliente_id

            flagvidavida = True

            rc.MoveNext
        Wend
    End If

End Sub

Sub Prepara_Valores()

    Dim rc As rdoResultset
    Dim SQL As String
    Dim PercCorretSaida As String
    Dim PercProLaboreSaida As String
    Dim varPercIR As Currency
    Dim ValImpSeguradaSaida As String
    Dim ValCorretagemSaida As String
    Dim ValPremioLiquidoSaida As String
    Dim ValIRSaida As String
    Dim ValDescontoSaida As String
    Dim ValJurosSaida As String
    Dim ValIOFSaida As String
    Dim ValEstipulanteSaida As String
    Dim ValPremioBrutoSaida As String
    Dim TamCampo As Double

    '
    '  Dados da fatura.
    txtNumFatura.Text = lFaturaId
    txtEmiFatura.Text = Format(gDtEmiEndosso, "dd/mm/yyyy")
    TxtNoVidas.Text = Format(NumVidasFatura, "###,###,##0")

    'Lsa - 06/10/2003
    txtUsuarioEmissor.Text = sUsuarioEmissor

    If ConfiguracaoBrasil Then
        txtPercCorretagem.Text = Format(PercCorretFatura, "###,###,##0.00")
        txtPerProLabore.Text = Format(PercProLaboreFatura, "0.00")
    Else
        txtPercCorretagem.Text = TrocaValorAmePorBras(Format(PercCorretFatura, "###,###,##00.00"))
        txtPerProLabore.Text = TrocaValorAmePorBras(Format(PercProLaboreFatura, "0.00"))
    End If

    ' Recupera percentual de IR.
    SQL = "SELECT isnull(valor,0) valor "
    SQL = SQL & " From val_item_financeiro_ramo_tb   WITH (NOLOCK) "
    SQL = SQL & " Where ramo_id = " & vRamoId
    SQL = SQL & " and cod_item_financeiro = '" & CodItemFinanceiroIR & "'"
    SQL = SQL & " and dt_inicio_vigencia <= '" & Format(DtIniVigFatura, "yyyy/mm/dd") & "'"
    SQL = SQL & " and (dt_fim_vigencia >= '" & Format(DtFimVigFatura, "yyyy/mm/dd") & "' "
    SQL = SQL & "      or dt_fim_vigencia is null)"

    Set rc = rdoCn1.OpenResultset(SQL, rdOpenStatic)

    If Not rc.EOF Then
        If Not ConfiguracaoBrasil Then
            varPercIR = rc("valor")
        Else
            varPercIR = Val(rc("valor"))
        End If
    Else
        varPercIR = 0
    End If

    txtPercIR.Text = Format(varPercIR * 100, "#0.00")
    '
    ' Recupera valores.
    SQL = "SELECT isnull(val_financeiro, 0) val_financeiro "
    SQL = SQL & ", isnull(val_comissao, 0) val_comissao "
    SQL = SQL & ", isnull(val_iof, 0) val_iof "
    SQL = SQL & ", isnull(val_adic_fracionamento, 0) val_adic_fracionamento "
    SQL = SQL & ", isnull(val_ir, 0) val_ir "
    SQL = SQL & ", isnull(val_desconto_comercial, 0) val_desconto_comercial "
    SQL = SQL & ", isnull(val_premio_tarifa, 0) val_premio_tarifa "
    SQL = SQL & ", isnull(val_is, 0) val_is "
    SQL = SQL & ", isnull(val_comissao_estipulante, 0) val_comissao_estipulante "
    SQL = SQL & " FROM endosso_financeiro_tb   WITH (NOLOCK) "
    SQL = SQL & " WHERE proposta_id = " & vPropostaId
    SQL = SQL & " AND endosso_id = " & gEndossoId

    Set rc = rdoCn1.OpenResultset(SQL, rdOpenStatic)

    If Not rc.EOF Then
        txtValImpSegurada.Text = Format(Val(rc!val_is), "#,###,###,###,##0.00")
        '
        txtValCorretagem.Text = Format(Val(rc!val_comissao), "#,###,###,##0.00")
        '
        txtValPremioLiquido.Text = Format(Val(rc!val_premio_tarifa), "#,###,###,##0.00")
        '
        txtValIR.Text = Format(Val(rc!val_ir), "#,###,###,##0.00")
        '
        txtValDesconto.Text = Format(Val(rc!val_desconto_comercial), "#,###,###,##0.00")
        '
        txtValJuros.Text = Format(Val(rc!val_adic_fracionamento), "#,###,###,##0.00")
        '
        txtValIOF.Text = Format(Val(rc!val_iof), "#,###,###,##0.00")
        '
        txtValEstipulante.Text = Format(Val(rc!val_comissao_estipulante), "#,###,###,##0.00")
        '
        txtValPremioBruto.Text = Format(Val(rc!val_financeiro), "#,###,###,##0.00")
    End If

    'Obt�m perc IOF para utilizar no relat�rio
    txtPercIOF.Text = Obter_IOF(vRamoId, vPropostaId, DtVencFatura)

    ' Formata datas da fatura.
    txtDatIniVigFatura = DtIniVigFatura
    txtDatFimVigFatura = DtFimVigFatura
    txtDatVencFatura = DtVencFatura
    '
    ' Recupera texto de observa��o.
    txtObservacao = ObservacaoFatura

End Sub

Private Sub cmdImprime_Click(Index As Integer)

    Call Imprime_Fatura(txtNumFatura.Text, vApoliceId, vSucursalId, vSeguradoraId, vRamoId, SubGrupos.Count > 0)

End Sub

Private Sub cmdSaida_Click()

    Unload Me

End Sub

Private Sub Form_Activate()

    Screen.MousePointer = vbDefault
    '

End Sub

Private Sub Form_Load()

    If Trim(vSegCliente) <> "" Then
        Me.Caption = "SEGP0223 - Consulta Fatura Vida - " & Ambiente & " - " & vSegCliente    'Rmarins - 05/01/2006
    Else
        Me.Caption = "SEGP0223 - Consulta Fatura Vida - " & Ambiente
    End If

    '# Apresenta a TAB inicial (Ap�lice)
    SSTabFat.Tab = TAB_FAT

    Call Prepara_Fatura
    Call Prepara_Valores
    Call Prepara_Segurados

    If gTpEmissao = "C" Or gTpEmissao = "A" Then
        Call Prepara_Cosseguro
        SSTabFat.TabEnabled(TAB_COSSEG) = True
    Else
        SSTabFat.TabEnabled(TAB_COSSEG) = False
    End If

End Sub

Private Function Obter_IOF(ByVal RamoId As Integer, ByVal propostaId As String, ByVal DtVencimento As String) As Double

    Dim SQL As String
    Dim rc As rdoResultset
    Dim rs As Object
    Dim oEmissao As Object
    Dim SubRamoId As Double
    Dim sDtReferencia As String
    Dim sDtIOF As String

    'Obter Subramo
    ''SQL = "SELECT subramo_id  " & vbNewLine
    ''SQL = SQL & "  FROM proposta_tb  WITH (NOLOCK) " & vbNewLine
    ''SQL = SQL & " WHERE proposta_id = " & PropostaID & vbNewLine

    'alessilva - 26/01/2008
    'Obter Subramo e data do pedido do endosso
    SQL = "SELECT p.subramo_id , e.dt_pedido_endosso" & vbNewLine
    SQL = SQL & "FROM proposta_tb p  WITH (NOLOCK) " & vbNewLine
    SQL = SQL & "INNER JOIN endosso_tb e  WITH (NOLOCK) " & vbNewLine
    SQL = SQL & "ON  e.proposta_id = p.proposta_id" & vbNewLine
    SQL = SQL & " WHERE p.proposta_id = " & propostaId & vbNewLine
    SQL = SQL & "AND endosso_id = " & gEndossoId

    Set rc = rdocn.OpenResultset(SQL)

    If Not rc.EOF Then
        SubRamoId = rc("subramo_id")
        sDtReferencia = rc("dt_pedido_endosso")
        sDtIOF = sDtReferencia
    End If

    If IsDate(sDtReferencia) Then
        sDtReferencia = Format(sDtReferencia, "yyyymmdd")
        sDtIOF = Format(sDtIOF, "yyyymmdd")
    End If

    rc.Close
    Set rc = Nothing


    'FLOW 279390 - MFerreira - Confitec Sistemas - 2008-03-07
    'Substituindo meio de pesquisa do IOF
    '--------------------------------------------------------
    '        Set oEmissao = CreateObject("SEGL0026.cls00125")
    '
    '    'Obtendo percentual de IOF '''''''''''''''''''''''''''''''''''''''''''
    '            Set rs = oEmissao.ObterTributacaoSubRamo("SEGBR", _
                 '                                               App.Title, _
                 '                                               App.FileDescription, _
                 '                                               glAmbiente_id, _
                 '                                               RamoId, _
                 '                                               SubRamoId, _
                 '                                               CStr(sDtReferencia), _
                 '                                               CStr(sDtIOF))
    '    '    Set rs = oEmissao.ObterTributacaoSubRamo("SEGBR", _
         '    '                                           App.Title, _
         '    '                                           App.FileDescription, _
         '    '                                           glAmbiente_id, _
         '    '                                           RamoId, _
         '    '                                           SubRamoId, _
         '    '                                           CStr(DtVencimento))
    SQL = ""
    SQL = SQL & "SELECT dbo.AliquotaIOF_fatura_fn("
    SQL = SQL & CInt(SubRamoId)
    SQL = SQL & ",'"
    SQL = SQL & sDtIOF
    SQL = SQL & "')/100 as valor"

    Set rs = rdocn.OpenResultset(SQL)

    If Not rs.EOF Then
        Obter_IOF = Val("0" & rs("valor"))
    Else
        MsgBox ("N�o foi poss�vel verificar o IOF utilizado para este subramo")
        Call TerminaSEGBR
        'Obter_IOF = 0
    End If

    'Destruindo objetos ''''''''''''''''''''''''''''''''''''''''''''''''''
    Set oEmissao = Nothing


End Function
