VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFlxGrd.ocx"
Object = "{C932BA88-4374-101B-A56C-00AA003668DC}#1.1#0"; "msmask32.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Begin VB.Form SelPropBasica 
   ClientHeight    =   6750
   ClientLeft      =   675
   ClientTop       =   2580
   ClientWidth     =   9540
   Icon            =   "SelPropBasica.frx":0000
   LinkTopic       =   "SelecionaProposta"
   ScaleHeight     =   6750
   ScaleWidth      =   9540
   Begin MSComctlLib.StatusBar StatusBar1 
      Align           =   2  'Align Bottom
      Height          =   255
      Left            =   0
      TabIndex        =   16
      Top             =   6495
      Width           =   9540
      _ExtentX        =   16828
      _ExtentY        =   450
      Style           =   1
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
         EndProperty
      EndProperty
   End
   Begin VB.ComboBox CmbRamo 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      ItemData        =   "SelPropBasica.frx":0442
      Left            =   6480
      List            =   "SelPropBasica.frx":0444
      Sorted          =   -1  'True
      Style           =   2  'Dropdown List
      TabIndex        =   8
      Top             =   720
      Width           =   1245
   End
   Begin VB.CommandButton cmdSelec 
      Caption         =   "Pesquisar"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      Left            =   7980
      TabIndex        =   11
      Top             =   1200
      Width           =   1125
   End
   Begin VB.Frame Frame2 
      Caption         =   " Proposta(s) Encontrada(s) "
      Height          =   3975
      Left            =   240
      TabIndex        =   12
      Top             =   1800
      Width           =   9015
      Begin MSFlexGridLib.MSFlexGrid GridProposta 
         Height          =   2775
         Left            =   120
         TabIndex        =   13
         Top             =   360
         Width           =   8775
         _ExtentX        =   15478
         _ExtentY        =   4895
         _Version        =   393216
         Cols            =   15
         FixedCols       =   0
         SelectionMode   =   1
         AllowUserResizing=   1
         FormatString    =   $"SelPropBasica.frx":0446
      End
      Begin MSFlexGridLib.MSFlexGrid GridCliente 
         Height          =   2775
         Left            =   120
         TabIndex        =   14
         Top             =   360
         Width           =   8775
         _ExtentX        =   15478
         _ExtentY        =   4895
         _Version        =   393216
         Cols            =   9
         FixedCols       =   0
         AllowBigSelection=   0   'False
         AllowUserResizing=   1
         FormatString    =   $"SelPropBasica.frx":0546
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   " Crit�rios de Pesquisa "
      Height          =   1455
      Left            =   240
      TabIndex        =   1
      Top             =   240
      Width           =   9015
      Begin VB.TextBox txtRazaoSocial 
         Height          =   315
         Left            =   4440
         TabIndex        =   21
         Top             =   480
         Width           =   4455
      End
      Begin MSMask.MaskEdBox mskCNPJ 
         Height          =   315
         Left            =   7200
         TabIndex        =   19
         Top             =   480
         Width           =   1635
         _ExtentX        =   2884
         _ExtentY        =   556
         _Version        =   393216
         MaxLength       =   18
         Format          =   "##.###.###/####-##"
         Mask            =   "##.###.###/####-##"
         PromptChar      =   "_"
      End
      Begin VB.ComboBox CmbSeguradora 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         ItemData        =   "SelPropBasica.frx":061E
         Left            =   2100
         List            =   "SelPropBasica.frx":0620
         Sorted          =   -1  'True
         Style           =   2  'Dropdown List
         TabIndex        =   17
         Top             =   480
         Width           =   2325
      End
      Begin VB.ComboBox CmbSucursal 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         ItemData        =   "SelPropBasica.frx":0622
         Left            =   4440
         List            =   "SelPropBasica.frx":0624
         Sorted          =   -1  'True
         Style           =   2  'Dropdown List
         TabIndex        =   6
         Top             =   480
         Width           =   1785
      End
      Begin VB.TextBox txtArgumento 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   7500
         TabIndex        =   10
         Top             =   480
         Width           =   1395
      End
      Begin VB.ComboBox cboTipoSelecao 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         ItemData        =   "SelPropBasica.frx":0626
         Left            =   105
         List            =   "SelPropBasica.frx":0628
         Sorted          =   -1  'True
         Style           =   2  'Dropdown List
         TabIndex        =   3
         Top             =   480
         Width           =   1980
      End
      Begin VB.Label lblRazaoSocial 
         AutoSize        =   -1  'True
         Caption         =   "Raz�o Social:"
         Height          =   195
         Left            =   4440
         TabIndex        =   20
         Top             =   240
         Width           =   990
      End
      Begin VB.Label lblCNPJ 
         AutoSize        =   -1  'True
         Caption         =   "CNPJ"
         Height          =   195
         Left            =   7320
         TabIndex        =   18
         Top             =   240
         Width           =   405
      End
      Begin VB.Label lblSeguradora 
         Caption         =   "Seguradora Lider :"
         Height          =   255
         Left            =   2100
         TabIndex        =   4
         Top             =   240
         Visible         =   0   'False
         Width           =   1365
      End
      Begin VB.Label lblRamo 
         Caption         =   "Ramo :"
         Height          =   255
         Left            =   6255
         TabIndex        =   7
         Top             =   240
         Width           =   855
      End
      Begin VB.Label lblSucursal 
         Caption         =   "Sucursal Seguradora :"
         Height          =   255
         Left            =   4440
         TabIndex        =   5
         Top             =   240
         Width           =   1575
      End
      Begin VB.Label Label1 
         Caption         =   "Tipo de Sele��o :"
         Height          =   255
         Left            =   150
         TabIndex        =   2
         Top             =   240
         Width           =   1425
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "Conte�do :"
         Height          =   195
         Left            =   7530
         TabIndex        =   9
         Top             =   240
         Width           =   780
      End
   End
   Begin VB.PictureBox TabStrip1 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   5775
      Left            =   120
      ScaleHeight     =   5715
      ScaleWidth      =   9195
      TabIndex        =   0
      Top             =   120
      Width           =   9255
   End
   Begin VB.CommandButton cmdCanc 
      Caption         =   "&Sair"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      Left            =   8220
      TabIndex        =   15
      Top             =   6000
      Width           =   1125
   End
End
Attribute VB_Name = "SelPropBasica"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim PesqNomeCliente As Boolean
Dim PesqCodCliente As Boolean
Dim bPropostaRecusada As Boolean
Dim bPropostaEstudo As Boolean    'rmaiellaro (19/01/2007)
Dim bPropostaNaoEmitida As Boolean
Dim iprod_id    '-- alterado 06/09/2013 projeto 17860335 - Melhorias no SEGBR MC_Amaral -confitec
' Jos� Moreira (Nova Consultoria) - 10/11/2012 - 14620257 - Melhorias no processo de subscri��o
' Permitir abertura de proposta via chamada em outro programa
Dim AbrirProposta As Boolean

'Zoro.Gomes - Confitec - Circular 365 - Novos ajustes de formata��o e Codeguard - 24/10/2019
Public apolice As Integer
Public Seguradora As Integer 'GCRUZ
Public Ramo As Integer 'GCRUZ
Public Sucursal As Integer 'GCRUZ


Sub Monta_CmbSeguradora()
    Dim SQL As String
    Dim rc As rdoResultset

    On Error GoTo Erro

    CmbSeguradora.Clear
    '
    MousePointer = vbHourglass

    SQL = "SELECT seguradora_cod_susep, nome "
    SQL = SQL & " FROM seguradora_tb   WITH (NOLOCK) "
    '
    Set rc = rdocn.OpenResultset(SQL)
    If Not rc.EOF Then
        While Not rc.EOF
            CmbSeguradora.AddItem Format(rc!seguradora_cod_susep, "0000") & " - " & Trim(rc!Nome)
            CmbSeguradora.ItemData(CmbSeguradora.NewIndex) = rc!seguradora_cod_susep
            '
            rc.MoveNext
        Wend
        '
        rc.Close
    End If
    Set rc = Nothing
    '
    MousePointer = vbDefault
    '
    Exit Sub
Erro:
    mensagem_erro 6, "Erro carregando lista de cias. seguradoras."
    MousePointer = vbDefault
End Sub

Function Le_Parametro_Geral(param As String) As String
    Dim SQL As String
    Dim rc As rdoResultset

    SQL = "SELECT val_parametro FROM ps_parametro_tb   WITH (NOLOCK) "
    SQL = SQL & "WHERE parametro = '" & param & "'"
    '
    Set rc = rdocn.OpenResultset(SQL)
    '
    If Not rc.EOF Then
        If IsNull(rc!val_parametro) Then
            MsgBox "C�digo da Seguradora est� nulo na tabela de par�metros." _
                 & Chr(13) & Chr(10) & "O Programa ser� cancelado.", vbCritical
            End
        Else
            Le_Parametro_Geral = Trim(rc!val_parametro)
        End If
    Else
        MsgBox "C�digo da Seguradora n�o est� definido na tabela de par�metros." _
             & Chr(13) & Chr(10) & "O Programa ser� cancelado.", vbCritical
        End
    End If

End Function

Sub Monta_CmbRamo()
    Dim stLSql As String
    Dim rs As rdoResultset

    CmbRamo.Clear
    '
    stLSql = "SELECT ramo_id, nome FROM ramo_tb   WITH (NOLOCK) "

    stLSql = stLSql & " left JOIN controle_sistema_db..empresa_ambiente_tb s WITH (NOLOCK) on s.ambiente_destino_id = " & glAmbiente_id & vbNewLine
    stLSql = stLSql & "WHERE tp_ramo_id = 1"
    stLSql = stLSql & " AND ramo_tb.cod_empresa = s.cod_empresa "

    '
    Set rs = rdocn.OpenResultset(stLSql)
    '
    If Not rs.EOF Then
        While Not rs.EOF
            CmbRamo.AddItem rs!ramo_id & " - " & rs!Nome
            CmbRamo.ItemData(CmbRamo.NewIndex) = rs!ramo_id
            '
            rs.MoveNext
        Wend
    End If

End Sub

Sub Monta_CmbSucursal()
    Dim stLSql As String
    Dim rs As rdoResultset
    CmbSucursal.Clear
    '
    stLSql = "SELECT sucursal_seguradora_id, nome FROM sucursal_seguradora_tb   WITH (NOLOCK) "
    'stLSql = stLSql & "WHERE seguradora_cod_susep = " & Trim(txtSeguradoraLider.Text)
    stLSql = stLSql & "WHERE seguradora_cod_susep = " & CmbSeguradora.ItemData(CmbSeguradora.ListIndex)
    '
    Set rs = rdocn.OpenResultset(stLSql)

    If Not rs.EOF Then
        While Not rs.EOF
            CmbSucursal.AddItem Format((rs!sucursal_seguradora_id), "000") & " - " & Trim(rs!Nome)
            CmbSucursal.ItemData(CmbSucursal.NewIndex) = rs!sucursal_seguradora_id
            '
            rs.MoveNext
        Wend
    End If
    '
    CmbSucursal.ListIndex = Le_Parametro_Geral("SUCURSAL_ALIANCA_DEFAULT")
    rs.Close
    Set rs = Nothing
    '
End Sub

Sub Monta_GridProposta_Cliente(ByVal vPropostaId As String, _
                               Optional sPropostaBB As String)
    Dim SQL As String
    Dim rc As rdoResultset

    On Error GoTo Erro_LePropCliente

    MousePointer = vbHourglass
    '
    SQL = ""
    SQL = "SELECT a.apolice_id, a.ramo_id, r.nome nm_ramo, a.seguradora_cod_susep, a.sucursal_seguradora_id " & vbNewLine
    SQL = SQL & ", pf.proposta_bb, p.proposta_id, p.produto_id, e.endosso_id " & vbNewLine
    SQL = SQL & ", e.dt_pedido_endosso, e.num_endosso_bb, e.tp_endosso_id " & vbNewLine
    SQL = SQL & ", c.nome, p.dt_contratacao, p.prop_cliente_id, A.tp_emissao " & vbNewLine
    SQL = SQL & ", descr_situacao = case p.situacao " & vbNewLine
    SQL = SQL & "      when 'e' then 'Em Estudo' " & vbNewLine
    SQL = SQL & "      when 'a' then 'Aceite' " & vbNewLine
    SQL = SQL & "      when 'c' then 'Cancelada' " & vbNewLine
    SQL = SQL & "      when 'r' then 'Recusada' " & vbNewLine
    'SQL = SQL & "      when 'i' then 'Ap�lice Emitida' "
    'bcarneiro - 05/11/2003 - Vida Vida - Novas Funcionalidades ''''''''''''''''
    SQL = SQL & "      when 'i' then " & vbNewLine
    'SQL = SQL & "       case (select count(*) " & vbNewLine 'alterado 12/09/2013 projeto 17860335 - Melhorias no SEGBR MC_Amaral -confitec
    SQL = SQL & "       case (select count(1) " & vbNewLine
    SQL = SQL & "               from proposta_suspensa_tb s WITH (NOLOCK) " & vbNewLine
    SQL = SQL & "              where p.proposta_id = s.proposta_id " & vbNewLine
    'SQL = SQL & "                and s.dt_inicio_suspensao >= '" & Format(Data_Sistema, "yyyymmdd") & "'"
    SQL = SQL & "                and s.dt_inicio_suspensao <= '" & Format(Data_Sistema, "yyyymmdd") & "'"
    SQL = SQL & "                and (s.dt_fim_suspensao is null " & vbNewLine
    SQL = SQL & "                 or (s.dt_fim_suspensao is not null " & vbNewLine
    SQL = SQL & "                and  s.cod_suspensao = '1'))) " & vbNewLine
    SQL = SQL & "           when 0 then 'Ap�lice Emitida' " & vbNewLine
    'Alessandra Grig�rio - 25/06/2004 - Atendendo solic. Marcio Yoshimura
    'SQL = SQL & "           else 'Suspensa'        "
    SQL = SQL & "           else 'Cancelada'        " & vbNewLine
    'FIM
    SQL = SQL & "       end " & vbNewLine
    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    SQL = SQL & "      when 'p' then 'Em Estudo (erro de cadastramento)' " & vbNewLine
    SQL = SQL & "      when 's' then 'Carta Recusa Emitida' " & vbNewLine
    SQL = SQL & "  end " & vbNewLine
    'Rmarins - 05/01/2006 - Inclus�o do campo seg_cliente '''''''''''''''''''''
    SQL = SQL & ", seg_cliente = CASE c.seg_cliente " & vbNewLine
    SQL = SQL & "                   WHEN 'private' THEN 'PRIVATE' " & vbNewLine
    SQL = SQL & "                   WHEN 'estilo' THEN 'ESTILO' " & vbNewLine
    SQL = SQL & "                   ELSE ' ' " & vbNewLine
    SQL = SQL & "                END " & vbNewLine
    SQL = SQL & "     , P.Situacao " & vbNewLine    'MCAMARAL MELHORIAS
    SQL = SQL & "     , pr.nome as nome_prod   " & vbNewLine    'MCAMARAL MELHORIAS
    SQL = SQL & "     , isnull(pr.processa_cobranca,'n') as Cobranca"    'MCAMARAL MELHORIAS
    SQL = SQL & "     , P.origem_proposta_id" & vbNewLine        'MCAMARAL MELHORIAS


    SQL = SQL & "FROM   proposta_tb p  WITH (NOLOCK) " & vbNewLine
    SQL = SQL & "       JOIN  cliente_tb c  WITH (NOLOCK)  "
    SQL = SQL & "         ON  c.cliente_id = p.prop_cliente_id " & vbNewLine
    SQL = SQL & "       JOIN   proposta_fechada_tb pf  WITH (NOLOCK) "
    SQL = SQL & "         ON   pf.proposta_id = p.proposta_id " & vbNewLine
    SQL = SQL & "       JOIN   apolice_tb a  WITH (NOLOCK) "
    SQL = SQL & "         ON   a.proposta_id = p.proposta_id " & vbNewLine
    SQL = SQL & "       JOIN  ramo_tb r   WITH (NOLOCK) "
    SQL = SQL & "         ON   r.ramo_id = a.ramo_id " & vbNewLine
    SQL = SQL & " left  JOIN endosso_tb e  WITH (NOLOCK)    " & vbNewLine
    SQL = SQL & "         ON e.proposta_id = p.proposta_id " & vbNewLine
    SQL = SQL & "        AND   e.tp_endosso_id not in (10,100,101,200) " & vbNewLine
    SQL = SQL & "       JOIN Produto_tb pr WITH(NOLOCK)"
    SQL = SQL & "         ON pr.produto_id = p.produto_id "
    SQL = SQL & "      WHERE  p.proposta_id = " & vPropostaId
    'SQL = SQL & " AND   e.proposta_id =* p.proposta_id " & vbNewLine 'alterado 12/09/2013 projeto 17860335 - Melhorias no SEGBR MC_Amaral -confitec
    '' N�o inclui os endossos financeiros
    'SQL = SQL & " AND   e.tp_endosso_id not in (10,100,101,200) " & vbNewLine
    SQL = SQL & " AND   p.produto_id in (" & ProdutosValidos & ") " & vbNewLine
    '    SQL = SQL & " ORDER BY p.dt_contratacao "
    ''
    ' lpinto 19/09/2005
    SQL = SQL & "" & vbNewLine
    SQL = SQL & "" & vbNewLine
    SQL = SQL & " UNION ALL " & vbNewLine
    SQL = SQL & "" & vbNewLine
    SQL = SQL & "" & vbNewLine

    '*******************************************************************************
    'Stefanini(Maur�cio), em 03/03/2006 - FLOWBR 123005 - inclus�o do distinct

    'Atual
    SQL = SQL & "    SELECT DISTINCT NULL apolice_id,                                                                   " & vbNewLine

    'Anterior
    'SQL = SQL & "    SELECT NULL apolice_id,                                                                           " & vbNewLine
    '*******************************************************************************

    SQL = SQL & "           p.ramo_id,                                                                                 " & vbNewLine
    SQL = SQL & "           ramo_tb.nome nm_ramo,                                                                      " & vbNewLine
    SQL = SQL & "           produto_tb.seguradora_cod_susep,                                                           " & vbNewLine
    SQL = SQL & "           arquivo_produto_tb.sucursal_seguradora_id,                                                 " & vbNewLine
    SQL = SQL & "           proposta_fechada_tb.proposta_bb,                                                           " & vbNewLine
    SQL = SQL & "           p.proposta_id,                                                                             " & vbNewLine
    SQL = SQL & "           p.produto_id,                                                                              " & vbNewLine
    SQL = SQL & "           NULL endosso_id,                                                                           " & vbNewLine
    SQL = SQL & "           NULL dt_pedido_endosso,                                                                    " & vbNewLine
    SQL = SQL & "           NULL num_endosso_bb,                                                                       " & vbNewLine
    SQL = SQL & "           NULL tp_endosso_id,                                                                        " & vbNewLine
    SQL = SQL & "           cliente_tb.nome,                                                                           " & vbNewLine
    SQL = SQL & "           p.dt_contratacao,                                                                          " & vbNewLine
    SQL = SQL & "           p.prop_cliente_id,                                                                         " & vbNewLine
    SQL = SQL & "           NULL tp_emissao,                                                                           " & vbNewLine
    SQL = SQL & "           descr_situacao = CASE p.situacao                                                           " & vbNewLine
    SQL = SQL & "                              WHEN 'e' THEN 'Em Estudo'                                               " & vbNewLine
    SQL = SQL & "                              WHEN 'a' THEN 'Aceite'                                                  " & vbNewLine
    SQL = SQL & "                              WHEN 'c' THEN 'Cancelada'                                               " & vbNewLine
    SQL = SQL & "                              WHEN 'r' THEN 'Recusada'                                                " & vbNewLine
    SQL = SQL & "                              WHEN 'i' THEN CASE (SELECT COUNT(1)                                     " & vbNewLine    'alterado COUNT(*) para COUNT(1)12/09/2013 projeto 17860335 - Melhorias no SEGBR MC_Amaral -confitec
    SQL = SQL & "                                                    FROM proposta_suspensa_tb s  WITH (NOLOCK)        " & vbNewLine
    SQL = SQL & "                                                   WHERE p.proposta_id = s.proposta_id                " & vbNewLine
    SQL = SQL & "                                                     AND s.dt_inicio_suspensao <= '20050722'          " & vbNewLine
    SQL = SQL & "                                                     AND (s.dt_fim_suspensao is NULL                  " & vbNewLine
    SQL = SQL & "                                                      OR (s.dt_fim_suspensao is NOT NULL              " & vbNewLine
    SQL = SQL & "                                                          AND  s.cod_suspensao = '1')))               " & vbNewLine
    SQL = SQL & "                                              WHEN 0 THEN 'Ap�lice Emitida'                           " & vbNewLine
    SQL = SQL & "                                              ELSE 'Cancelada'                                        " & vbNewLine
    SQL = SQL & "                                            END                                                       " & vbNewLine
    SQL = SQL & "                              WHEN 'p' THEN 'Em Estudo (erro de cadastramento)'                       " & vbNewLine
    SQL = SQL & "                              WHEN 's' THEN 'Carta Recusa Emitida'                                    " & vbNewLine
    SQL = SQL & "                           END                                                                        " & vbNewLine
    'Rmarins - 05/01/2006 - Inclus�o do campo seg_cliente '''''''''''''''''''''
    SQL = SQL & ", seg_cliente = CASE cliente_tb.seg_cliente                                                                              " & vbNewLine
    SQL = SQL & "                   WHEN 'private' THEN 'PRIVATE'                                                                " & vbNewLine
    SQL = SQL & "                   WHEN 'estilo' THEN 'ESTILO'                                                                  " & vbNewLine
    SQL = SQL & "                   ELSE ' '                                                                                     " & vbNewLine
    SQL = SQL & "                END                                                                                             " & vbNewLine
    SQL = SQL & "     , P.Situacao " & vbNewLine    'MCAMARAL MELHORIAS
    SQL = SQL & "     , pr.nome as nome_prod   " & vbNewLine    'MCAMARAL MELHORIAS
    SQL = SQL & "     , isnull(pr.processa_cobranca,'n') as Cobranca"    'MCAMARAL MELHORIAS
    SQL = SQL & "     , P.origem_proposta_id" & vbNewLine        'MCAMARAL MELHORIAS


    SQL = SQL & "       FROM proposta_tb p WITH (NOLOCK)                                                                                 " & vbNewLine
    SQL = SQL & " INNER JOIN proposta_fechada_tb WITH (NOLOCK)                                                                           " & vbNewLine
    SQL = SQL & "         ON proposta_fechada_tb.proposta_id = p.proposta_id                                            " & vbNewLine
    SQL = SQL & " INNER JOIN produto_tb  WITH (NOLOCK)                                                                                            " & vbNewLine
    SQL = SQL & "         ON produto_tb.produto_id = p.produto_id                                                       " & vbNewLine
    SQL = SQL & " INNER JOIN arquivo_produto_tb  WITH (NOLOCK)                                                                                   " & vbNewLine
    SQL = SQL & "         ON arquivo_produto_tb.produto_id = produto_tb.produto_id                                                " & vbNewLine
    SQL = SQL & " INNER JOIN cliente_tb  WITH (NOLOCK)                                                                                   " & vbNewLine
    SQL = SQL & "         ON cliente_tb.cliente_id = p.prop_cliente_id                                                  " & vbNewLine
    SQL = SQL & " INNER JOIN ramo_tb  WITH (NOLOCK)                                                                                      " & vbNewLine
    SQL = SQL & "         ON ramo_tb.ramo_id = p.ramo_id                                                                " & vbNewLine
    SQL = SQL & "       JOIN Produto_tb pr WITH(NOLOCK)"
    SQL = SQL & "         ON pr.produto_id = p.produto_id "
    SQL = SQL & "      WHERE p.proposta_id = " & vPropostaId & vbNewLine
    SQL = SQL & "        AND proposta_fechada_tb.proposta_id = p.proposta_id                                            " & vbNewLine
    SQL = SQL & "        AND ramo_tb.ramo_id = p.ramo_id                                                                " & vbNewLine
    SQL = SQL & "        AND cliente_tb.cliente_id = p.prop_cliente_id                                                  " & vbNewLine
    SQL = SQL & "        AND p.produto_id in (" & ProdutosValidos & ")" & vbNewLine

    'Lpinto - 27/09/2005
    If sPropostaBB = "" Then
        SQL = SQL & " ORDER BY dt_contratacao " & vbNewLine
    Else

        SQL = SQL & "" & vbNewLine
        SQL = SQL & "" & vbNewLine
        SQL = SQL & "   UNION ALL" & vbNewLine
        SQL = SQL & "" & vbNewLine
        SQL = SQL & "   SELECT NULL apolice_id,                " & vbNewLine
        SQL = SQL & "          NULL ramo_id,                   " & vbNewLine
        SQL = SQL & "          NULL nm_ramo,                   " & vbNewLine
        SQL = SQL & "          cod_seguradora,                 " & vbNewLine
        SQL = SQL & "          NULL sucursal_seguradora_id,    " & vbNewLine
        SQL = SQL & "          proposta_bb,                    " & vbNewLine
        SQL = SQL & "          proposta_id,                    " & vbNewLine
        SQL = SQL & "          NULL produto_id,                " & vbNewLine
        SQL = SQL & "          NULL endosso_id,                " & vbNewLine
        SQL = SQL & "          NULL dt_pedido_endosso,         " & vbNewLine
        SQL = SQL & "          NULL num_endosso_bb,            " & vbNewLine
        SQL = SQL & "          NULL tp_endosso_id,             " & vbNewLine
        SQL = SQL & "          proponente,                     " & vbNewLine
        SQL = SQL & "          dt_contratacao,                 " & vbNewLine
        SQL = SQL & "          NULL prop_cliente_id,           " & vbNewLine
        SQL = SQL & "          descr_situacao = 'N�o Emitido', " & vbNewLine
        SQL = SQL & "          NULL tp_emissao                 " & vbNewLine
        'Rmarins - 05/01/2006 - Inclus�o do campo seg_cliente '''''''''''''''''''''
        SQL = SQL & ",         NULL seg_cliente                " & vbNewLine
        SQL = SQL & "       ,  NULL Situacao " & vbNewLine    'MCAMARAL MELHORIAS
        SQL = SQL & "     , NULL nome_prod   " & vbNewLine    'MCAMARAL MELHORIAS
        SQL = SQL & "       , NULL as Cobranca"    'MCAMARAL MELHORIAS
        SQL = SQL & "       , NULL origem_proposta_id" & vbNewLine        'MCAMARAL MELHORIAS

        SQL = SQL & "     FROM emi_proposta_tb  WITH (NOLOCK)         " & vbNewLine
        SQL = SQL & "    WHERE situacao = 'n'                  " & vbNewLine
        SQL = SQL & "      AND proposta_bb = " & sPropostaBB & vbNewLine
        ' SQL = SQL & " ORDER BY dt_contratacao                  " & vbNewLine
        SQL = SQL & "" & vbNewLine
        SQL = SQL & " UNION ALL" & vbNewLine
        SQL = SQL & "" & vbNewLine
        SQL = SQL & "   SELECT NULL apolice_id,                 " & vbNewLine
        SQL = SQL & "          NULL ramo_id,                    " & vbNewLine
        SQL = SQL & "          NULL nm_ramo,                    " & vbNewLine
        SQL = SQL & "          cod_seguradora,                  " & vbNewLine
        SQL = SQL & "          NULL sucursal_seguradora_id,     " & vbNewLine
        SQL = SQL & "          proposta_bb,                     " & vbNewLine
        SQL = SQL & "          proposta_id,                     " & vbNewLine
        SQL = SQL & "          NULL produto_id,                 " & vbNewLine
        SQL = SQL & "          endosso_id,                      " & vbNewLine
        SQL = SQL & "          NULL dt_pedido_endosso,          " & vbNewLine
        SQL = SQL & "          num_endosso_bb,                  " & vbNewLine
        SQL = SQL & "          NULL tp_endosso_id,              " & vbNewLine
        SQL = SQL & "          proponente,                      " & vbNewLine
        SQL = SQL & "          dt_contratacao,                  " & vbNewLine
        SQL = SQL & "          NULL prop_cliente_id,            " & vbNewLine
        SQL = SQL & "          descr_situacao = 'N�o Emitido',  " & vbNewLine
        SQL = SQL & "          NULL tp_emissao                  " & vbNewLine
        'Rmarins - 05/01/2006 - Inclus�o do campo seg_cliente '''''''''''''''''''''
        SQL = SQL & ",         NULL seg_cliente                " & vbNewLine
        SQL = SQL & "       ,  NULL Situacao " & vbNewLine    'MCAMARAL MELHORIAS
        SQL = SQL & "       , NULL nome_prod   " & vbNewLine    'MCAMARAL MELHORIAS
        SQL = SQL & "       , NULL as Cobranca"    'MCAMARAL MELHORIAS
        SQL = SQL & "       , NULL origem_proposta_id" & vbNewLine        'MCAMARAL MELHORIAS

        SQL = SQL & "     FROM emi_re_proposta_tb  WITH (NOLOCK)       " & vbNewLine
        SQL = SQL & "    WHERE situacao = 'n'                   " & vbNewLine
        SQL = SQL & "      AND proposta_bb = " & sPropostaBB & vbNewLine
        SQL = SQL & "      AND(arquivo LIKE 'SEG491D%'          " & vbNewLine
        SQL = SQL & "          OR arquivo LIKE 'SEG493%'        " & vbNewLine
        SQL = SQL & "          OR arquivo LIKE 'SEGF496%'       " & vbNewLine
        SQL = SQL & "          OR arquivo LIKE 'INTERNET%'      " & vbNewLine
        SQL = SQL & "          OR arquivo LIKE 'SEG486%'        " & vbNewLine
        SQL = SQL & "          OR arquivo LIKE 'SEG422%'        " & vbNewLine
        SQL = SQL & "          OR arquivo LIKE 'SEGF495%')      " & vbNewLine
        'GENJUNIOR - FLOW 17860335 - CONSULTA TABELAS DE HIST�RICO
        SQL = SQL & "" & vbNewLine
        SQL = SQL & "  UNION ALL" & vbNewLine
        SQL = SQL & "" & vbNewLine
        SQL = SQL & " SELECT NULL apolice_id " & vbNewLine
        SQL = SQL & "      , NULL ramo_id " & vbNewLine
        SQL = SQL & "      , NULL nm_ramo " & vbNewLine
        SQL = SQL & "      , cod_seguradora " & vbNewLine
        SQL = SQL & "      , NULL sucursal_seguradora_id " & vbNewLine
        SQL = SQL & "      , proposta_bb " & vbNewLine
        SQL = SQL & "      , proposta_id " & vbNewLine
        SQL = SQL & "      , NULL produto_id " & vbNewLine
        SQL = SQL & "      , NULL endosso_id " & vbNewLine
        SQL = SQL & "      , NULL dt_pedido_endosso " & vbNewLine
        SQL = SQL & "      , NULL num_endosso_bb " & vbNewLine
        SQL = SQL & "      , NULL tp_endosso_id " & vbNewLine
        SQL = SQL & "      , proponente " & vbNewLine
        SQL = SQL & "      , dt_contratacao " & vbNewLine
        SQL = SQL & "      , NULL prop_cliente_id " & vbNewLine
        SQL = SQL & "      , descr_situacao = 'N�o Emitido' " & vbNewLine
        SQL = SQL & "      , NULL tp_emissao " & vbNewLine
        SQL = SQL & "      , NULL seg_cliente " & vbNewLine
        SQL = SQL & "      , NULL Situacao " & vbNewLine
        SQL = SQL & "      , NULL nome_prod " & vbNewLine
        SQL = SQL & "      , NULL as Cobranca "
        SQL = SQL & "      , NULL origem_proposta_id " & vbNewLine
        SQL = SQL & "   FROM seguros_hist_db.dbo.emi_proposta_hist_tb  WITH (NOLOCK) " & vbNewLine
        SQL = SQL & "  WHERE situacao = 'n' " & vbNewLine
        SQL = SQL & "    AND proposta_bb = " & sPropostaBB & vbNewLine
        SQL = SQL & "" & vbNewLine
        SQL = SQL & " UNION ALL" & vbNewLine
        SQL = SQL & "" & vbNewLine
        SQL = SQL & " SELECT NULL apolice_id " & vbNewLine
        SQL = SQL & "      , NULL ramo_id " & vbNewLine
        SQL = SQL & "      , NULL nm_ramo " & vbNewLine
        SQL = SQL & "      , cod_seguradora " & vbNewLine
        SQL = SQL & "      , NULL sucursal_seguradora_id " & vbNewLine
        SQL = SQL & "      , proposta_bb " & vbNewLine
        SQL = SQL & "      , proposta_id " & vbNewLine
        SQL = SQL & "      , NULL produto_id " & vbNewLine
        SQL = SQL & "      , endosso_id " & vbNewLine
        SQL = SQL & "      , NULL dt_pedido_endosso " & vbNewLine
        SQL = SQL & "      , num_endosso_bb " & vbNewLine
        SQL = SQL & "      , NULL tp_endosso_id " & vbNewLine
        SQL = SQL & "      , proponente " & vbNewLine
        SQL = SQL & "      , dt_contratacao " & vbNewLine
        SQL = SQL & "      , NULL prop_cliente_id " & vbNewLine
        SQL = SQL & "      , descr_situacao = 'N�o Emitido' " & vbNewLine
        SQL = SQL & "      , NULL tp_emissao " & vbNewLine
        SQL = SQL & "      , NULL seg_cliente " & vbNewLine
        SQL = SQL & "      , NULL Situacao " & vbNewLine
        SQL = SQL & "      , NULL nome_prod " & vbNewLine
        SQL = SQL & "      , NULL as Cobranca "
        SQL = SQL & "      , NULL origem_proposta_id " & vbNewLine
        SQL = SQL & "   FROM seguros_hist_db.dbo.emi_re_proposta_hist_tb WITH (NOLOCK) " & vbNewLine
        SQL = SQL & "  WHERE situacao = 'n' " & vbNewLine
        SQL = SQL & "    AND proposta_bb = " & sPropostaBB & vbNewLine
        SQL = SQL & "    AND (arquivo LIKE 'SEG491D%' " & vbNewLine
        SQL = SQL & "     OR arquivo LIKE 'SEG493%' " & vbNewLine
        SQL = SQL & "     OR arquivo LIKE 'SEGF496%' " & vbNewLine
        SQL = SQL & "     OR arquivo LIKE 'INTERNET%' " & vbNewLine
        SQL = SQL & "     OR arquivo LIKE 'SEG486%' " & vbNewLine
        SQL = SQL & "     OR arquivo LIKE 'SEG422%' " & vbNewLine
        SQL = SQL & "     OR arquivo LIKE 'SEGF495%') " & vbNewLine
        'FIM GENJUNIOR
        SQL = SQL & " ORDER BY dt_contratacao  " & vbNewLine


    End If

    Set rc = rdocn.OpenResultset(SQL)
    '
    If Not rc.EOF Then
        GridCliente.Visible = False
        GridProposta.Visible = True
        Frame2.Caption = " Ap�lice(s) Encontrada(s)"
        '
        gTpEmissao = IIf(IsNull(rc!tp_emissao), " ", rc!tp_emissao)
        If gTpEmissao = "A" Then
            CosseguroAceito = True
        Else
            CosseguroAceito = False
        End If
        '
        Call Carrega_GridProposta_Geral(rc)
    Else
        MsgBox "Proposta n�o encontrada!", vbInformation
    End If
    '
    MousePointer = vbDefault
    Exit Sub

Erro_LePropCliente:
    mensagem_erro 6, "Erro lendo proposta!"
    MousePointer = vbDefault

End Sub

Private Function TrocaPrimeiraColuna(formato As String, NovaColuna As String) As String

    Dim temp As String

    temp = NovaColuna & Space(42 - Len(NovaColuna)) & Mid(formato, InStr(formato, "|"))
    GridProposta.FormatString = temp

End Function

Private Sub cboTipoSelecao_Click()
    Dim i As Integer
    'carolina.marinho
    Dim col As Integer

    CmbSeguradora.ListIndex = -1
    CmbSucursal.ListIndex = -1
    CmbRamo.ListIndex = -1
    txtArgumento.Text = ""
    Label2.Left = 7500
    txtArgumento.Left = 7470
    txtArgumento.Width = 1395

    If cboTipoSelecao.Text = "N� da Ap�lice" Then

        Frame2.Caption = " Ap�lice(s) Encontrada(s) "

        'TrocaPrimeiraColuna GridProposta.FormatString, "Ap�lice"

        GridProposta.Visible = True
        GridCliente.Visible = False
        Label2.Caption = "N� Apolice :"
        lblSucursal.Visible = True
        CmbSucursal.Visible = True
        lblRamo.Visible = True
        CmbRamo.Visible = True
        lblSeguradora.Visible = True
        CmbSeguradora.Visible = True

        'carolina.marinho 24/10/2011 - Demanda 11938655 - Criar ferramenta para identificar exist�ncia de subgrupo (rp) - Confitec RJ
        lblCNPJ.Visible = False
        mskCNPJ.Visible = False
        lblRazaoSocial.Visible = False
        txtRazaoSocial.Visible = False
        txtArgumento.Visible = True
        Label2.Visible = True
        GridProposta.FormatString = "Ap�lice                          |Ramo |Nome Ramo |Produto  |Cod. Cliente |Proponente                                     |Dt. Contrata��o |Situa��o                       |Proposta BB |Endosso Id |Dt. Endosso |N� Endosso BB |Tipo End. |Segmento   | PPE  |CNPJ   |Nome                                                            |Ramo/Ap�lice  |Subgrupo      "
        GridProposta.ColWidth(15) = 0
        GridProposta.ColWidth(16) = 0
        GridProposta.ColWidth(17) = 0
        GridProposta.ColWidth(18) = 0
        'carolina.marinho fim

        '
        vseguradora = Le_Parametro_Geral("COD_ALIANCA")
        For i = 0 To CmbSeguradora.ListCount - 1
            If CmbSeguradora.ItemData(i) = vseguradora Then
                CmbSeguradora.ListIndex = i
                Exit For
            End If
        Next


        '
        '        CmbSeguradora.Locked = True
        CmbSeguradora.Locked = False    ' Permite escolha de outra seguradora lider. (Marisa)

        '
        Monta_CmbSucursal
        CmbSucursal.SetFocus
        Monta_CmbRamo

    ElseIf cboTipoSelecao.Text = "N� da Proposta" Or cboTipoSelecao.Text = "N� da Proposta SC" Or cboTipoSelecao.Text = "N� da Apolice SC" Or cboTipoSelecao.Text = "N� da Certificado SC" Then

        Frame2.Caption = " Proposta(s) Encontrada(s) "

        'TrocaPrimeiraColuna GridProposta.FormatString, "Proposta"

        GridProposta.Visible = True
        GridCliente.Visible = False
        Label2.Caption = "N� Proposta :"

        If cboTipoSelecao.Text = "N� da Proposta SC" Then
            Label2.Caption = "N� da Proposta SC :"
        ElseIf cboTipoSelecao.Text = "N� da Apolice SC" Then
            Label2.Caption = "N� da Apolice SC :"
        ElseIf cboTipoSelecao.Text = "N� da Certificado SC" Then
            Label2.Caption = "N� da Certificado SC :"
        End If

        lblSucursal.Visible = False
        CmbSucursal.Visible = False
        lblRamo.Visible = False
        CmbRamo.Visible = False
        lblSeguradora.Visible = False
        CmbSeguradora.Visible = False
        txtArgumento.SelStart = 0
        txtArgumento.SelLength = Len(txtArgumento.Text)
        If txtArgumento.Visible Then
            txtArgumento.SetFocus
        End If

        'carolina.marinho 24/10/2011 - Demanda 11938655 - Criar ferramenta para identificar exist�ncia de subgrupo (rp) - Confitec RJ
        lblCNPJ.Visible = False
        mskCNPJ.Visible = False
        lblRazaoSocial.Visible = False
        txtRazaoSocial.Visible = False
        txtArgumento.Visible = True
        Label2.Visible = True
        GridProposta.FormatString = "Ap�lice                          |Ramo |Nome Ramo |Produto  |Cod. Cliente |Proponente                                     |Dt. Contrata��o |Situa��o                       |Proposta BB |Endosso Id |Dt. Endosso |N� Endosso BB |Tipo End. |Segmento   | PPE  |CNPJ   |Nome                                                            |Ramo/Ap�lice  |Subgrupo      "
        GridProposta.ColWidth(15) = 0
        GridProposta.ColWidth(16) = 0
        GridProposta.ColWidth(17) = 0
        GridProposta.ColWidth(18) = 0
        'carolina.marinho fim


    ElseIf cboTipoSelecao.Text = "N� da Proposta BB" Then

        Frame2.Caption = " Proposta(s) Encontrada(s) "

        'TrocaPrimeiraColuna GridProposta.FormatString, "Proposta BB"

        GridProposta.Visible = True
        GridCliente.Visible = False
        Label2.Caption = "N� Proposta BB :"
        lblSucursal.Visible = False
        CmbSucursal.Visible = False
        lblRamo.Visible = False
        CmbRamo.Visible = False
        lblSeguradora.Visible = False
        'txtSeguradoraLider.Visible = False
        CmbSeguradora.Visible = False
        txtArgumento.SelStart = 0
        txtArgumento.SelLength = Len(txtArgumento.Text)
        If txtArgumento.Visible Then
            txtArgumento.SetFocus
        End If


        'carolina.marinho 24/10/2011 - Demanda 11938655 - Criar ferramenta para identificar exist�ncia de subgrupo (rp) - Confitec RJ
        lblCNPJ.Visible = False
        mskCNPJ.Visible = False
        lblRazaoSocial.Visible = False
        txtRazaoSocial.Visible = False
        txtArgumento.Visible = True
        Label2.Visible = True
        GridProposta.FormatString = "Ap�lice                          |Ramo |Nome Ramo |Produto  |Cod. Cliente |Proponente                                     |Dt. Contrata��o |Situa��o                       |Proposta BB |Endosso Id |Dt. Endosso |N� Endosso BB |Tipo End. |Segmento   | PPE  |CNPJ   |Nome                                                            |Ramo/Ap�lice  |Subgrupo      "
        GridProposta.ColWidth(15) = 0
        GridProposta.ColWidth(16) = 0
        GridProposta.ColWidth(17) = 0
        GridProposta.ColWidth(18) = 0

        'carolina.marinho fim


    ElseIf cboTipoSelecao.Text = "N� do Cliente" Then

        Frame2.Caption = " Cliente(s) Encontrado(s) "
        GridProposta.Visible = False
        GridCliente.Visible = True
        GridCliente.FormatString = "C�d. Cliente |Proposta           |Produto|Segurado                                          |Dt. Contrata��o|Situa��o                                |Segmento "
        GridCliente.Cols = 7
        Label2.Caption = "N� do Cliente :"
        lblSucursal.Visible = False
        CmbSucursal.Visible = False
        lblRamo.Visible = False
        CmbRamo.Visible = False
        lblSeguradora.Visible = False
        'txtSeguradoraLider.Visible = False
        CmbSeguradora.Visible = False
        txtArgumento.SelStart = 0
        txtArgumento.SelLength = Len(txtArgumento.Text)
        If txtArgumento.Visible Then
            txtArgumento.SetFocus
        Else
            txtArgumento.Visible = True
        End If

        'carolina.marinho 24/10/2011 - Demanda 11938655 - Criar ferramenta para identificar exist�ncia de subgrupo (rp) - Confitec RJ
        lblCNPJ.Visible = False
        mskCNPJ.Visible = False
        lblRazaoSocial.Visible = False
        txtRazaoSocial.Visible = False
        txtArgumento.Visible = True
        Label2.Visible = True
        'carolina.marinho fim


    ElseIf cboTipoSelecao.Text = "N� de Ordem" Then

        'TrocaPrimeiraColuna GridProposta.FormatString, "N� Ordem"

        GridProposta.Visible = True
        GridCliente.Visible = False
        Label2.Caption = "N� de Ordem :"
        lblSucursal.Visible = True
        CmbSucursal.Visible = True
        lblRamo.Visible = False
        CmbRamo.Visible = False
        lblSeguradora.Visible = True
        CmbSeguradora.Visible = True
        CmbSeguradora.Locked = False
        CmbSeguradora.ListIndex = -1
        CmbSeguradora.SetFocus

        'carolina.marinho 24/10/2011 - Demanda 11938655 - Criar ferramenta para identificar exist�ncia de subgrupo (rp) - Confitec RJ
        lblCNPJ.Visible = False
        mskCNPJ.Visible = False
        lblRazaoSocial.Visible = False
        txtRazaoSocial.Visible = False
        txtArgumento.Visible = True
        Label2.Visible = True
        GridProposta.FormatString = "Ap�lice                          |Ramo |Nome Ramo |Produto  |Cod. Cliente |Proponente                                     |Dt. Contrata��o |Situa��o                       |Proposta BB |Endosso Id |Dt. Endosso |N� Endosso BB |Tipo End. |Segmento   | PPE  |CNPJ   |Nome                                                            |Ramo/Ap�lice  |Subgrupo      "
        GridProposta.ColWidth(15) = 0
        GridProposta.ColWidth(16) = 0
        GridProposta.ColWidth(17) = 0
        GridProposta.ColWidth(18) = 0

        'carolina.marinho fim


    ElseIf cboTipoSelecao.Text = "Nome do Cliente" Then

        Frame2.Caption = " Cliente(s) Encontrado(s) "
        GridProposta.Visible = False
        GridCliente.Visible = True
        GridCliente.FormatString = "Segurado                                          |C�d. Cliente|Proposta           |Produto|Dt. Contrata��o|Situa��o                                "
        GridCliente.Cols = 6
        Label2.Caption = "Nome do Cliente :"
        lblSucursal.Visible = False
        CmbSucursal.Visible = False
        lblRamo.Visible = False
        CmbRamo.Visible = False
        lblSeguradora.Visible = False
        'txtSeguradoraLider.Visible = False
        CmbSeguradora.Visible = False
        Label2.Left = 4400
        txtArgumento.Left = 4350
        txtArgumento.Width = 4515
        txtArgumento.SelStart = 0
        txtArgumento.SelLength = Len(txtArgumento.Text)
        If txtArgumento.Visible Then
            txtArgumento.SetFocus
        End If

        'carolina.marinho 24/10/2011 - Demanda 11938655 - Criar ferramenta para identificar exist�ncia de subgrupo (rp) - Confitec RJ
        lblCNPJ.Visible = False
        mskCNPJ.Visible = False
        lblRazaoSocial.Visible = False
        txtRazaoSocial.Visible = False
        txtArgumento.Visible = True
        Label2.Visible = True
        'End If


    ElseIf cboTipoSelecao.Text = "CNPJ" Then

        Frame2.Caption = " Cliente(s) Encontrado(s) "

        GridProposta.Visible = True
        GridCliente.Visible = False
        Label2.Visible = False
        lblSucursal.Visible = False
        CmbSucursal.Visible = False
        lblRamo.Visible = False
        CmbRamo.Visible = False
        lblSeguradora.Visible = False
        'txtSeguradoraLider.Visible = False
        CmbSeguradora.Visible = False
        txtArgumento.Visible = False

        lblCNPJ.Visible = True
        mskCNPJ.Visible = True
        lblRazaoSocial.Visible = False
        txtRazaoSocial.Visible = False
        mskCNPJ.SetFocus
        GridProposta.FormatString = "Ap�lice                          |Ramo |Nome Ramo |Produto  |Cod. Cliente |Proponente                                     |Dt. Contrata��o |Situa��o                       |Proposta BB |Endosso Id |Dt. Endosso |N� Endosso BB |Tipo End. |Segmento   | PPE  |CNPJ   |Nome                                                            |Ramo/Ap�lice  |Subgrupo      "
        col = 0
        Do While col < 15
            'Debug.Print col, GridProposta.ColWidth(col)
            GridProposta.ColWidth(col) = 0
            col = col + 1
        Loop

    ElseIf cboTipoSelecao.Text = "Raz�o Social" Then

        Frame2.Caption = " Cliente(s) Encontrado(s) "

        GridProposta.Visible = True
        GridCliente.Visible = False
        Label2.Visible = False
        lblSucursal.Visible = False
        CmbSucursal.Visible = False
        lblRamo.Visible = False
        CmbRamo.Visible = False
        lblSeguradora.Visible = False
        'txtSeguradoraLider.Visible = False
        CmbSeguradora.Visible = False
        txtArgumento.Visible = False

        lblCNPJ.Visible = False
        mskCNPJ.Visible = False
        lblRazaoSocial.Visible = True
        txtRazaoSocial.Visible = True
        txtRazaoSocial.SetFocus
        GridProposta.FormatString = "Ap�lice                          |Ramo |Nome Ramo |Produto  |Cod. Cliente |Proponente                                     |Dt. Contrata��o |Situa��o                       |Proposta BB |Endosso Id |Dt. Endosso |N� Endosso BB |Tipo End. |Segmento   | PPE  |CNPJ   |Nome                                                            |Ramo/Ap�lice  |Subgrupo      "
        col = 0
        Do While col < 15
            'Debug.Print col, GridProposta.ColWidth(col)
            GridProposta.ColWidth(col) = 0
            col = col + 1
        Loop

    End If

    GridProposta.HighLight = flexHighlightNever

    GridProposta.Rows = 1
    GridCliente.Rows = 1

    StatusBar1.SimpleText = "Selecione Tipo de Sele��o e digite os Argumentos de Pesquisa !"
    '
    Frame1.Refresh
    '
End Sub

Private Sub CmbSeguradora_Click()

    If CmbSeguradora.ListIndex <> -1 Then
        Monta_CmbSucursal
    End If

End Sub


Private Sub Form_Activate()
    Dim Comando As String
    Dim NroCliente As Long

    cboTipoSelecao.ListIndex = 2
    StatusBar1.SimpleText = "Selecione Tipo de Sele��o e digite os Argumentos de Pesquisa !"
    '--------------------------------------------------------------------
    ' JOCONCEICAO - 27/APR/2001 - PARA CONSULTAR AUTOMATICAMENTE O CLIENTE
    '                             A PARTIR DO PROGRAMA CLIENTE_RESTRICAO
    '
    Comando = UCase(gsParamAplicacao)
    If UCase(Mid(Comando, 1, 7)) = "CLIENTE" Then
        NroCliente = Val(Mid(Comando, 8, Len(Comando)))
        cboTipoSelecao.ListIndex = 3
        txtArgumento.Text = NroCliente

        Call cmdSelec_Click
        ' EYOSHINO - 06/09/2002 - Chamada direta � consulta
        If GridProposta.Visible = True Then
            Call GridProposta_DblClick
        Else
            Call GridCliente_DblClick
        End If

    ElseIf UCase(Mid(Comando, 1, 8)) = "PROPOSTA" Then
        NroCliente = Val(Mid(Comando, 9, Len(Comando)))
        cboTipoSelecao.ListIndex = 1
        txtArgumento.Text = NroCliente

        Call cmdSelec_Click

        ' EYOSHINO - 06/09/2002 - Chamada direta � consulta
        If GridProposta.Visible = True Then
            Call GridProposta_DblClick
        Else
            Call GridCliente_DblClick
        End If
    End If

    ' Jos� Moreira (Nova Consultoria) - 10/11/2012 - 14620257 - Melhorias no processo de subscri��o
    ' Permitir abertura de proposta via chamada em outro programa
    If AbrirProposta = True Then
        cboTipoSelecao.ListIndex = 4
        txtArgumento.Text = Mid(gsParamAplicacao, 1, 9)
        cmdSelec_Click
    End If
    ' Fim - Jos� Moreira

    '------------------------------------------------------------------------
End Sub

Private Sub Form_Load()

'pmelo - comentar ap�s o teste
'    glAmbiente_id = 2
'    cUserName = "SISBR"
'Data_Sistema = Date
'Fim pmelo - comentar ap�s o teste

    Call Conexao

    Call Conexao_Auxiliar

    ' Jos� Moreira (Nova Consultoria) - 10/11/2012 - 14620257 - Melhorias no processo de subscri��o
    ' Permitir abertura de proposta via chamada em outro programa
    If Not Trata_Parametros(Command) Then

        Call FinalizarAplicacao
    End If

    AbrirProposta = False

    If Len(gsParamAplicacao) > 2 Then
        ' pablo.dias (Nova Consultoria) - 18/10/2013 - 14620257 - Melhorias no processo de subscri��o
        ' N�o validar seguran�a quando chamado a partir de outro programa
        'Call Seguranca("CLIP0001", "Consulta Proposta")
        AbrirProposta = True
    End If
    ' Fim - Jos� Moreira


    'Call Seguranca("SEGP0223", "Consulta de Proposta Vida")
    '' Recupera os codigos de produtos validos para a consulta.



    Produtos = ""

    SQL = "SELECT distinct p.produto_id  "
    SQL = SQL & " FROM produto_tb p  WITH (NOLOCK) , item_produto_tb i  WITH (NOLOCK) , ramo_tb r   WITH (NOLOCK) "
    SQL = SQL & " WHERE   p.produto_id = i.produto_id  and "
    SQL = SQL & "         r.tp_ramo_id = 1 and "
    SQL = SQL & "         r.ramo_id = i.ramo_id "

    'Set rc = rdocn.OpenResultset(SQL)

    Set rc = rdoCn1.OpenResultset(SQL, rdOpenStatic)
    While Not rc.EOF
        Produtos = Produtos & CStr(rc!Produto_id) & ","
        rc.MoveNext
    Wend
    ProdutosValidos = Mid(Produtos, 1, Len(Produtos) - 1)

    FP_Agenciamento(0) = "N�O DEFINIDO"
    FP_Agenciamento(1) = "INCLUS�ES"
    FP_Agenciamento(2) = "INCLUS�ES + ACERTOS IS"
    FP_Agenciamento(3) = "SALDO (INC - EXC)"
    FP_Agenciamento(4) = "SALDO (INC - EXC) + ACERTOS IS"
    '
    GridProposta.ColWidth(GRD_SEL_RAMO) = 0
    GridProposta.ColWidth(GRD_SEL_NMRAMO) = 0
    GridProposta.ColWidth(GRD_SEL_CODCLI) = 0
    '
    cboTipoSelecao.AddItem "N� da Ap�lice"
    cboTipoSelecao.AddItem "N� da Proposta"
    cboTipoSelecao.AddItem "N� da Proposta BB"
    cboTipoSelecao.AddItem "N� do Cliente"
    cboTipoSelecao.AddItem "N� de Ordem"
    cboTipoSelecao.AddItem "Nome do Cliente"

    cboTipoSelecao.AddItem "N� da Proposta SC"
    cboTipoSelecao.AddItem "N� da Apolice SC"
    cboTipoSelecao.AddItem "N� da Certificado SC"

    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    'carolina.marinho 24/10/2011 - Demanda 11938655
    'Criar ferramenta para identificar exist�ncia de subgrupo (rp) - Confitec RJ
    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    cboTipoSelecao.AddItem "CNPJ"
    cboTipoSelecao.AddItem "Raz�o Social"
    'carolina.marinho fim

    '
    Monta_CmbSeguradora
    '
    cboTipoSelecao.ListIndex = 2
    '

    StatusBar1.SimpleText = "Selecione Tipo de Sele��o e digite os Argumentos de Pesquisa !"
    '
    CentraFrm Me
    '
    Me.Caption = "SEG00223 - Sele��o de Proposta / Ap�lice Vida Avulso - " & Ambiente


End Sub

Private Sub cmdCanc_Click()
'AFONSO - G&P - 19/05/2008
    Call TerminaSEGBR
    'Unload Me
    End

End Sub

Private Sub cmdSelec_Click()
    Dim rc As rdoResultset
    Dim rc2 As rdoResultset
    Dim Sql_Aux As String
    Dim table_name As String
    Dim Produtos As String
    Dim apolice_pesq As Variant
    Dim ramo_pesq As Variant
    Dim sucursal_pesq As Variant
    Dim seguradora_pesq As Variant
    Dim proposta_id_bb As String    'usado na melhoria da consulta BB mcamaral
    Dim proposta_id_bb1 As String    'usado na melhoria da consulta BB mcamaral
    Dim SIT As String

    Dim rsAltRowCount As rdoResultset
    Dim rsRetRowCount As rdoResultset
    Dim sql1 As String
    PesqNomeCliente = False
    PesqCodCliente = False
    Dim sSituacao As String    'usado na melhoria da consulta BB mcamaral

    On Error GoTo Erro

    vNomeProd = ""
    MousePointer = vbHourglass

    StatusBar1.SimpleText = "Pesquisando..."
    GridCliente.Rows = 1
    GridProposta.Rows = 1
    '*******************************************************************************
    'Stefanini(Maur�cio), em 03/03/2006 - FLOWBR 123005 - inclus�o das linhas

    If cboTipoSelecao.Text = "Nome do Cliente" Then

        GridProposta.Rows = 1
        GridProposta.Visible = False
        GridCliente.Visible = True

    End If

    '*******************************************************************************

    '' Valida os dados da consulta
    If (CmbSeguradora.Visible) And (CmbSeguradora.ListIndex = -1) Then
        mensagem_erro 3, "Seguradora Lider"
        CmbSeguradora.SetFocus
        MousePointer = vbDefault
        Exit Sub
    End If
    '
    If (CmbSucursal.Visible) And (CmbSucursal.ListIndex = -1) Then
        mensagem_erro 3, "Sucursal Seguradora"
        CmbSucursal.SetFocus
        MousePointer = vbDefault
        Exit Sub
    End If
    '
    If (CmbRamo.Visible) And (CmbRamo.ListIndex = -1) Then
        mensagem_erro 3, "Ramo"
        CmbRamo.SetFocus
        MousePointer = vbDefault
        Exit Sub
    End If
    '
    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    'carolina.marinho 24/10/2011 - Demanda 11938655
    'Criar ferramenta para identificar exist�ncia de subgrupo (rp) - Confitec RJ
    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    '    If Trim(txtArgumento.Text) = "" Then
    '        mensagem_erro 3, "Argumento de Pesquisa"
    '        txtArgumento.SetFocus
    '        MousePointer = vbDefault
    '        Exit Sub
    '    End If

    If cboTipoSelecao.Text <> "CNPJ" And cboTipoSelecao.Text <> "Raz�o Social" Then

        If Trim(txtArgumento.Text) = "" Then
            mensagem_erro 3, "Argumento de Pesquisa"
            txtArgumento.SetFocus
            MousePointer = vbDefault
            Exit Sub
        End If
    Else
        If mskCNPJ.Visible = True Then
            If Trim(mskCNPJ.Text) = "__.___.___/____-__" Then
                mensagem_erro 3, "Argumento de Pesquisa"
                txtArgumento.SetFocus
                MousePointer = vbDefault
                Exit Sub
            End If
        Else
            If Trim(txtRazaoSocial.Text) = "" Then
                mensagem_erro 3, "Argumento de Pesquisa"
                txtArgumento.SetFocus
                MousePointer = vbDefault
                Exit Sub
            End If
        End If
    End If

    '' Recupera os codigos de produtos validos para a consulta.
    'INICIO  'alterado 06/09/2013 projeto 17860335 - Melhorias no SEGBR MC_Amaral -confitec
    'REMOVIDO PARA O LOAD DO FORM
    '    Produtos = ""
    '
    '    SQL = "SELECT distinct p.produto_id "
    '    SQL = SQL & " FROM produto_tb p  WITH (NOLOCK) , item_produto_tb i  WITH (NOLOCK) , ramo_tb r   WITH (NOLOCK) "
    '    SQL = SQL & " WHERE   p.produto_id = i.produto_id  and "
    '    SQL = SQL & "         r.tp_ramo_id = 1 and "
    '    SQL = SQL & "         r.ramo_id = i.ramo_id "
    '
    '    Set rc = rdoCn1.OpenResultset(SQL, rdOpenStatic)
    '    While Not rc.EOF
    '        Produtos = Produtos & CStr(rc!Produto_id) & ","
    '        rc.MoveNext
    '    Wend
    '    ProdutosValidos = Mid(Produtos, 1, Len(Produtos) - 1)
    '
    '' Monta a query de acordo com o tipo de sele��o

    If cboTipoSelecao.Text = "N� da Proposta" Then

        If Not IsNumeric(txtArgumento.Text) Then
            mensagem_erro 3, "N� da Proposta"
            txtArgumento.SelStart = 0
            txtArgumento.SelLength = Len(txtArgumento)
            txtArgumento.SetFocus
            MousePointer = vbDefault
            Exit Sub
        End If

        '**inicio Proposta
        'Ler dados do produto na proposta
        SQL = ""
        SQL = SQL & "SELECT a.situacao      " & vbNewLine
        SQL = SQL & "     , a.produto_id    " & vbNewLine
        SQL = SQL & "     , p.nome  AS nome_prod  " & vbNewLine
        SQL = SQL & "     , isnull(p.processa_cobranca,'n') as cobranca "
        SQL = SQL & "     , a.origem_proposta_id" & vbNewLine
        SQL = SQL & "  FROM proposta_tb a  WITH(NOLOCK)" & vbNewLine
        SQL = SQL & "  JOIN Produto_tb p WITH(NOLOCK)"
        SQL = SQL & "    ON a.produto_id = p.produto_id "
        SQL = SQL & " WHERE a.proposta_id = " & txtArgumento.Text
        SQL = SQL & " AND a.situacao <> 't' "
        ' SQL = SQL & "  AND  p.produto_id in (" & ProdutosValidos & ") " & vbNewLine

        Set rc2 = rdoCn2.OpenResultset(SQL)

        '**Se existir verifica se o produto e valido

        If Not rc2.EOF Then

            SQL = ""
            SQL = SQL & "SELECT count(1)" & vbNewLine
            SQL = SQL & "  FROM Produto_tb   WITH(NOLOCK)" & vbNewLine
            SQL = SQL & " WHERE  produto_id in (" & ProdutosValidos & ") " & vbNewLine
            SQL = SQL & "   AND  produto_id = " & rc2!Produto_id & " " & vbNewLine
            Set rc = rdoCn2.OpenResultset(SQL)


            '** Se o produto n�o for valido mostra mensagem e finaliza consulta

            If rc.EOF Then
                MsgBox "Produto inv�lido!", vbInformation, Screen.ActiveForm.Caption
                txtArgumento.SelStart = 0
                txtArgumento.SelLength = Len(txtArgumento)
                txtArgumento.SetFocus
                MousePointer = vbDefault
                rc.Close
                Exit Sub
            End If
            rc.Close
        End If


        'lpinto/acintra 27/09/2005
        bPropostaRecusada = False
        'eduardo.amaral(Nova Consultoria) 23-02-2012 Demanda: BB SEGURO VIDA EMPRESA FLEX
        If Not rc2.EOF Then
            If UCase(rc2(0)) = "R" Or UCase(rc2(0)) = "S" Then
                bPropostaRecusada = True
            End If
        End If

        'fim eduardo.amaral(Nova Consultoria)
        'rmaiellaro (19/01/2007)

        bPropostaEstudo = False
        If Not rc2.EOF Then
            If UCase(rc2(0)) = "E" Or UCase(rc2(0)) = "P" Then
                bPropostaEstudo = True
            End If
        End If


        If Not rc2.EOF Then
            vProdutoId = IIf(IsNull(rc2!Produto_id), "", rc2!Produto_id)
            vNomeProd = rc2!Nome_prod
            ProcessaCobranca = rc2!Cobranca
            vSituacao = rc2!Situacao
            vorigem_proposta_id = rc2!origem_proposta_id

            If vorigem_proposta_id = 2 Then
                bPropostaALS = True
            Else
                bPropostaALS = False
            End If
        End If
        rc2.Close
        Set rc2 = Nothing

        '**primeiro select da proposta
        'lpinto - 21/09/2005 - Consulta proposta recusada - proposta_fechada_tb
        If Not bPropostaRecusada And Not bPropostaEstudo Then    'rmaiellaro (19/01/2007)

            SQL = ""
            SQL = "     SELECT a.apolice_id" & vbNewLine
            SQL = SQL & "    , a.ramo_id" & vbNewLine
            SQL = SQL & "    , r.nome nm_ramo" & vbNewLine
            SQL = SQL & "    , a.seguradora_cod_susep" & vbNewLine
            SQL = SQL & "    , a.sucursal_seguradora_id " & vbNewLine
            SQL = SQL & "    , pf.proposta_bb" & vbNewLine
            SQL = SQL & "    , p.proposta_id" & vbNewLine
            SQL = SQL & "    , p.produto_id" & vbNewLine
            SQL = SQL & "    , e.endosso_id " & vbNewLine
            SQL = SQL & "    , e.dt_pedido_endosso" & vbNewLine
            SQL = SQL & "    , e.num_endosso_bb" & vbNewLine
            SQL = SQL & "    , e.tp_endosso_id " & vbNewLine
            SQL = SQL & "    , c.nome" & vbNewLine
            SQL = SQL & "    , p.dt_contratacao" & vbNewLine
            SQL = SQL & "    , p.prop_cliente_id" & vbNewLine
            SQL = SQL & "    , p.origem_proposta_id " & vbNewLine
            SQL = SQL & "    , descr_situacao = case p.situacao " & vbNewLine
            SQL = SQL & "                       when 'e' then 'Em Estudo' " & vbNewLine
            SQL = SQL & "                       when 'a' then 'Aceite' " & vbNewLine
            SQL = SQL & "                       when 'c' then 'Cancelada' " & vbNewLine
            SQL = SQL & "                       when 'r' then 'Recusada' " & vbNewLine
            'SQL = SQL & "     when 'i' then 'Ap�lice Emitida' "
            'bcarneiro - 05/11/2003 - Vida Vida - Novas Funcionalidades ''''''''''''''''
            SQL = SQL & "                       when 'i' then " & vbNewLine
            SQL = SQL & "                                case (select count(1) " & vbNewLine
            SQL = SQL & "                                        from proposta_suspensa_tb s WITH(NOLOCK) " & vbNewLine
            SQL = SQL & "                                       where p.proposta_id = s.proposta_id " & vbNewLine
            'SQL = SQL & "                 and s.dt_inicio_suspensao >= '" & Format(Data_Sistema, "yyyymmdd") & "'"
            SQL = SQL & "                                         and s.dt_inicio_suspensao <= '" & Format(Data_Sistema, "yyyymmdd") & "'" & vbNewLine
            SQL = SQL & "                                         and (s.dt_fim_suspensao is null " & vbNewLine
            SQL = SQL & "                                          or (s.dt_fim_suspensao is not null " & vbNewLine
            SQL = SQL & "                                         and  s.cod_suspensao = '1'))) " & vbNewLine
            SQL = SQL & "                                when 0 then 'Ap�lice Emitida'" & vbNewLine
            'Alessandra Grig�rio - 25/06/2004 - Atendendo solic. Marcio Yoshimura
            'SQL = SQL & "            else 'Suspensa'        "
            SQL = SQL & "                                else 'Cancelada'       " & vbNewLine
            'FIM
            SQL = SQL & "                                 end " & vbNewLine
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            SQL = SQL & "                    when 'p' then 'Em Estudo (erro de cadastramento)' " & vbNewLine
            SQL = SQL & "                    when 's' then 'Carta Recusa Emitida' " & vbNewLine
            SQL = SQL & "                     end " & vbNewLine
            SQL = SQL & "    , a.tp_emissao " & vbNewLine
            SQL = SQL & "    , NULL nome_arquivo " & vbNewLine
            'Rmarins - 05/01/2006 - Inclus�o do campo seg_cliente '''''''''''''''''''''
            SQL = SQL & "    , seg_cliente = CASE c.seg_cliente " & vbNewLine
            SQL = SQL & "                    WHEN 'private' THEN 'PRIVATE' " & vbNewLine
            SQL = SQL & "                    WHEN 'estilo' THEN 'ESTILO' " & vbNewLine
            SQL = SQL & "                    ELSE ' ' " & vbNewLine
            SQL = SQL & "                    END " & vbNewLine
            'ANTES
            'SQL = SQL & "FROM   proposta_tb p  WITH (NOLOCK) , apolice_tb a  WITH (NOLOCK) " & vbNewLine
            'SQL = SQL & ",      proposta_fechada_tb pf  WITH (NOLOCK) , cliente_tb c  WITH (NOLOCK) , ramo_tb r   WITH (NOLOCK)  " & vbNewLine
            'DEPOIS
            SQL = SQL & "      FROM proposta_tb p  WITH (NOLOCK) " & vbNewLine
            SQL = SQL & "      JOIN apolice_tb a  WITH (NOLOCK) " & vbNewLine
            SQL = SQL & "        ON a.proposta_id = p.proposta_id " & vbNewLine
            SQL = SQL & "      JOIN proposta_fechada_tb pf  WITH (NOLOCK) "
            SQL = SQL & "        ON pf.proposta_id = p.proposta_id " & vbNewLine
            SQL = SQL & "      JOIN cliente_tb c  WITH (NOLOCK) " & vbNewLine
            SQL = SQL & "        ON c.cliente_id = p.prop_cliente_id " & vbNewLine
            SQL = SQL & "      JOIN ramo_tb r   WITH (NOLOCK)  " & vbNewLine
            SQL = SQL & "        ON r.ramo_id = a.ramo_id " & vbNewLine
            SQL = SQL & "     LEFT  JOIN endosso_tb e  WITH (NOLOCK)  "
            SQL = SQL & "        ON e.proposta_id = p.proposta_id " & vbNewLine
            SQL = SQL & "       AND  e.tp_endosso_id not in (10,100,101,200) " & vbNewLine
            SQL = SQL & "      JOIN Produto_tb pr WITH(NOLOCK)" & vbNewLine
            SQL = SQL & "        ON p.produto_id = pr.produto_id " & vbNewLine
            SQL = SQL & "     WHERE  p.proposta_id = " & txtArgumento & vbNewLine
            ' SQL = SQL & "  AND  e.proposta_id =* p.proposta_id " & vbNewLine
            '' N�o inclui os endossos financeiros
            SQL = SQL & "       AND  p.produto_id in (" & ProdutosValidos & ") " & vbNewLine
            SQL = SQL & "       AND  p.situacao <>'t' " & vbNewLine
            'SQL = SQL & " ORDER BY p.dt_contratacao "
            ' SQL = SQL & " ORDER BY e.dt_pedido_endosso " 'Alterado por fpimenta / Imar�s - 05/06/2003 - SAS 027-03
            'AFONSO DUTRA - DATA: 09/03/2010 - FLOW: 3206098
            'Raimundo Belas da Silva Junior - GPTI - 13/04/2010
            'Acertando ORDER BY
            'SQL = SQL & " ORDER BY e.dt_pedido_endosso, /*e.dt_emissao,*/ p.dt_contratacao, e.endosso_id "
            'Denis Silva  - CWI - 26/05/2010
            'Acertando ORDER BY - FLOW:3727253
            SQL = SQL & "ORDER BY  e.dt_emissao, /*e.dt_pedido_endosso,*/ p.dt_contratacao, e.endosso_id "
            'FIM

        Else

            SQL = ""
            SQL = SQL & "    SELECT DISTINCT NULL apolice_id,  --rmaiellaro (19/01/2007)                                                                                      " & vbNewLine
            SQL = SQL & "           ramo_tb.ramo_id,                                                                                                            " & vbNewLine
            SQL = SQL & "           ramo_tb.nome nm_ramo,                                                                                                       " & vbNewLine
            SQL = SQL & "           produto_tb.seguradora_cod_susep,                                                                                            " & vbNewLine
            SQL = SQL & "           arquivo_produto_tb.sucursal_seguradora_id,                                                                                  " & vbNewLine
            SQL = SQL & "           proposta_fechada_tb.proposta_bb,                                                                                            " & vbNewLine
            SQL = SQL & "           proposta_tb.proposta_id,                                                                                                    " & vbNewLine
            SQL = SQL & "           proposta_tb.produto_id,                                                                                                     " & vbNewLine
            SQL = SQL & "           NULL endosso_id,                                                                                                            " & vbNewLine
            SQL = SQL & "           NULL dt_pedido_endosso,                                                                                                     " & vbNewLine
            SQL = SQL & "           NULL num_endosso_bb,                                                                                                        " & vbNewLine
            SQL = SQL & "           NULL tp_endosso_id,                                                                                                         " & vbNewLine
            SQL = SQL & "           cliente_tb.nome,                                                                                                            " & vbNewLine
            SQL = SQL & "           proposta_tb.dt_contratacao,                                                                                                 " & vbNewLine
            SQL = SQL & "           proposta_tb.prop_cliente_id,                                                                                                " & vbNewLine
            SQL = SQL & "           descr_situacao = CASE proposta_tb.situacao                                                                                  " & vbNewLine
            SQL = SQL & "                              WHEN 'e' THEN 'Em Estudo'                                                                                " & vbNewLine
            SQL = SQL & "                              WHEN 'a' THEN 'Aceite'                                                                                   " & vbNewLine
            SQL = SQL & "                              WHEN 'c' THEN 'Cancelada'                                                                                " & vbNewLine
            SQL = SQL & "                              WHEN 'r' THEN 'Recusada'                                                                                 " & vbNewLine
            SQL = SQL & "                              WHEN 'i' THEN CASE (    SELECT COUNT(1)                                                                  " & vbNewLine
            SQL = SQL & "                                                        FROM proposta_suspensa_tb WITH(NOLOCK)                                         " & vbNewLine
            SQL = SQL & "                                                  INNER JOIN proposta_tb     WITH(NOLOCK)                                              " & vbNewLine
            SQL = SQL & "                                                          ON proposta_tb.proposta_id = proposta_suspensa_tb.proposta_id                " & vbNewLine
            SQL = SQL & "                                                       WHERE proposta_suspensa_tb.dt_inicio_suspensao <= '20050722'                    " & vbNewLine
            SQL = SQL & "                                                         AND (proposta_suspensa_tb.dt_fim_suspensao IS NULL                            " & vbNewLine
            SQL = SQL & "                                                          OR (proposta_suspensa_tb.dt_fim_suspensao IS NOT NULL                        " & vbNewLine
            SQL = SQL & "                                                              AND  proposta_suspensa_tb.cod_suspensao = '1')))                         " & vbNewLine
            SQL = SQL & "                                              WHEN 0 THEN 'Ap�lice Emitida'                                                            " & vbNewLine
            SQL = SQL & "                                              ELSE 'Cancelada'                                                                         " & vbNewLine
            SQL = SQL & "                                            END                                                                                        " & vbNewLine
            SQL = SQL & "                              WHEN 'p' THEN 'Em Estudo (erro de cadastramento)'                                                        " & vbNewLine
            SQL = SQL & "                              WHEN 's' THEN 'Carta Recusa Emitida'                                                                     " & vbNewLine
            SQL = SQL & "                            END,                                                                                                       " & vbNewLine
            SQL = SQL & "           NULL tp_emissao,                                                                                                            " & vbNewLine
            SQL = SQL & "           NULL nome_arquivo                                                                                                           " & vbNewLine
            'Rmarins - 05/01/2006 - Inclus�o do campo seg_cliente ''''''''''''''''''''''''''''''''''
            SQL = SQL & ",          seg_cliente = CASE cliente_tb.seg_cliente                                                                                   " & vbNewLine
            SQL = SQL & "                           WHEN 'private' THEN 'PRIVATE'                                                                               " & vbNewLine
            SQL = SQL & "                           WHEN 'estilo' THEN 'ESTILO'                                                                                 " & vbNewLine
            SQL = SQL & "                           ELSE ' '                                                                                                    " & vbNewLine
            SQL = SQL & "                         END                                                                                                           " & vbNewLine

            SQL = SQL & "      FROM proposta_tb WITH (NOLOCK)                                                                                                          " & vbNewLine
            SQL = SQL & "INNER JOIN proposta_fechada_tb  WITH (NOLOCK)                                                                                                 " & vbNewLine
            SQL = SQL & "        ON proposta_fechada_tb.proposta_id = proposta_tb.proposta_id                                                                   " & vbNewLine
            SQL = SQL & "INNER JOIN produto_tb  WITH (NOLOCK)                                                                                                          " & vbNewLine
            SQL = SQL & "        ON produto_tb.produto_id = proposta_tb.produto_id                                                                              " & vbNewLine
            SQL = SQL & "INNER JOIN arquivo_produto_tb  WITH (NOLOCK)                                                                                                  " & vbNewLine
            SQL = SQL & "        ON arquivo_produto_tb.produto_id = proposta_tb.produto_id                                                                      " & vbNewLine
            SQL = SQL & "       AND arquivo_produto_tb.ramo_id = proposta_tb.ramo_id                                                                            " & vbNewLine
            SQL = SQL & "INNER JOIN cliente_tb  WITH (NOLOCK)                                                                                                          " & vbNewLine
            SQL = SQL & "        ON cliente_tb.cliente_id = proposta_tb.prop_cliente_id                                                                         " & vbNewLine
            SQL = SQL & "INNER JOIN ramo_tb  WITH (NOLOCK)                                                                                                             " & vbNewLine
            SQL = SQL & "        ON ramo_tb.ramo_id = proposta_tb.ramo_id                                                                                       " & vbNewLine
            SQL = SQL & "     WHERE proposta_tb.proposta_id = " & txtArgumento.Text & vbNewLine
            SQL = SQL & "       AND proposta_fechada_tb.proposta_id = proposta_tb.proposta_id                                                                   " & vbNewLine
            SQL = SQL & "       AND proposta_tb.produto_id in (" & ProdutosValidos & ")                                                                         " & vbNewLine
            SQL = SQL & "       AND proposta_tb.situacao<>'t'                                                                                                   " & vbNewLine

            SQL = SQL & "  ORDER BY proposta_tb.dt_contratacao                                                                                                  " & vbNewLine

        End If

        '*****************************************************************************
        '** Descri��o: Implementa��o de consulta por proposta ades�o                **
        '** Motivo: Atender consultas para o produto 714                            **
        '** Autor: Febner (Fabio Alves de Araujo Ebner - JR & P(SP)                 **
        '** Data: 10/06/03                                                          **
        '*****************************************************************************

        Sql_Aux = SQL
        Set rc2 = rdoCn2.OpenResultset(Sql_Aux)
        If rc2.EOF Then

            ' lpinto 21/09/2005 - Consulta proposta recusada - proposta_adesao_tb
            'If Not bPropostaRecusada And Not bPropostaEstudo Then 'rmaiellaro (19/01/2007)
            'alterado And vProdutoId = 714 -11/09/2013 projeto 17860335 - Melhorias no SEGBR MC_Amaral -confitec
            If Not bPropostaRecusada And Not bPropostaEstudo And (IIf(IsNull(vProdutoId), "", vProdutoId)) = 714 Then    'rmaiellaro (19/01/2007)
                SQL = ""
                SQL = SQL & "       SELECT a.apolice_id" & vbNewLine
                SQL = SQL & "            , a.ramo_id" & vbNewLine
                SQL = SQL & "            , r.nome nm_ramo" & vbNewLine
                SQL = SQL & "            , a.seguradora_cod_susep" & vbNewLine
                SQL = SQL & "            , a.sucursal_seguradora_id  " & vbNewLine
                SQL = SQL & "            , pf.proposta_bb, p.proposta_id" & vbNewLine
                SQL = SQL & "            , p.produto_id" & vbNewLine
                SQL = SQL & "            , e.endosso_id " & vbNewLine
                SQL = SQL & "            , e.dt_pedido_endosso" & vbNewLine
                SQL = SQL & "            , e.num_endosso_bb" & vbNewLine
                SQL = SQL & "            , e.tp_endosso_id" & vbNewLine
                SQL = SQL & "            , c.nome " & vbNewLine
                SQL = SQL & "            , p.dt_contratacao" & vbNewLine
                SQL = SQL & "            , p.prop_cliente_id " & vbNewLine
                SQL = SQL & "            , descr_situacao = case p.situacao " & vbNewLine
                SQL = SQL & "                               when 'e' then 'Em Estudo' " & vbNewLine
                SQL = SQL & "                               when 'a' then 'Aceite' " & vbNewLine
                SQL = SQL & "                               when 'c' then 'Cancelada' " & vbNewLine
                SQL = SQL & "                               when 'r' then 'Recusada' " & vbNewLine
                'SQL = SQL & "           when 'i' then 'Ap�lice Emitida' "
                'bcarneiro - 05/11/2003 - Vida Vida - Novas Funcionalidades ''''''''''''''''
                SQL = SQL & "                               when 'i' then " & vbNewLine
                SQL = SQL & "                                       case (select count(1)" & vbNewLine
                SQL = SQL & "                                               from proposta_suspensa_tb s WITH(NOLOCK)" & vbNewLine
                SQL = SQL & "                                              where p.proposta_id = s.proposta_id " & vbNewLine
                'SQL = SQL & "                and s.dt_inicio_suspensao >= '" & Format(Data_Sistema, "yyyymmdd") & "'"
                SQL = SQL & "                                                and s.dt_inicio_suspensao <= '" & Format(Data_Sistema, "yyyymmdd") & "'" & vbNewLine
                SQL = SQL & "                                                and (s.dt_fim_suspensao is null " & vbNewLine
                SQL = SQL & "                                                 or (s.dt_fim_suspensao is not null " & vbNewLine
                SQL = SQL & "                                                and  s.cod_suspensao = '1')))" & vbNewLine
                SQL = SQL & "                                               when 0 then 'Ap�lice Emitida' " & vbNewLine
                'Alessandra Grig�rio - 25/06/2004 - Atendendo solic. Marcio Yoshimura
                'SQL = SQL & "                    else 'Suspensa'        "
                SQL = SQL & "                                               else 'Cancelada'       " & vbNewLine
                'FIM
                SQL = SQL & "                                                end " & vbNewLine
                ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                SQL = SQL & "                               when 'p' then 'Em Estudo (erro de cadastramento)' " & vbNewLine
                SQL = SQL & "                               when 's' then 'Carta Recusa Emitida' " & vbNewLine
                SQL = SQL & "                                end, ap.tp_emissao, NULL nome_arquivo " & vbNewLine

                'Rmarins - 05/01/2006 - Inclus�o do campo seg_cliente '''''''''''''''''''''''''''''''
                SQL = SQL & "              ,  seg_cliente = CASE c.seg_cliente    " & vbNewLine
                SQL = SQL & "                               WHEN 'private' THEN 'PRIVATE'  " & vbNewLine
                SQL = SQL & "                               WHEN 'estilo' THEN 'ESTILO'    " & vbNewLine
                SQL = SQL & "                               ELSE ' '   " & vbNewLine
                SQL = SQL & "                                END  " & vbNewLine

                SQL = SQL & "           FROM  proposta_tb p  WITH (NOLOCK) " & vbNewLine
                SQL = SQL & "               , certificado_tb a  WITH (NOLOCK) " & vbNewLine
                SQL = SQL & "               , endosso_tb e  WITH (NOLOCK) " & vbNewLine
                SQL = SQL & "               , proposta_adesao_tb pf  WITH (NOLOCK) " & vbNewLine
                SQL = SQL & "               , cliente_tb c  WITH (NOLOCK) " & vbNewLine
                SQL = SQL & "               , ramo_tb r  WITH (NOLOCK) " & vbNewLine
                SQL = SQL & "               , apolice_tb ap  WITH (NOLOCK)  " & vbNewLine
                SQL = SQL & "           WHERE p.proposta_id = " & txtArgumento & vbNewLine
                SQL = SQL & "             AND a.proposta_id = p.proposta_id " & vbNewLine
                SQL = SQL & "             AND pf.proposta_id = p.proposta_id " & vbNewLine
                SQL = SQL & "             AND e.proposta_id = p.proposta_id " & vbNewLine
                SQL = SQL & "             AND e.tp_endosso_id not in (10,100,101,200) " & vbNewLine
                SQL = SQL & "             AND c.cliente_id = p.prop_cliente_id " & vbNewLine
                SQL = SQL & "             AND r.ramo_id = a.ramo_id " & vbNewLine
                SQL = SQL & "             AND a.apolice_id = ap.apolice_id " & vbNewLine
                SQL = SQL & "             AND a.ramo_id = ap.ramo_id " & vbNewLine
                SQL = SQL & "             AND p.produto_id = 714 " & vbNewLine
                SQL = SQL & "             AND p.situacao<>'t'          " & vbNewLine
                'SQL = SQL & "   ORDER BY  p.dt_contratacao " & vbNewLine
                'AFONSO DUTRA NOGUEIRA FILHO - GPTI - DATA: 25/03/2010
                'Raimundo Belas da Silva Junior - GPTI - 13/04/2010
                'Acertando ORDER BY
                'Denis Silva  - CWI - 26/05/2010
                'Acertando ORDER BY - FLOW:3727253
                'SQL = SQL & " ORDER BY e.dt_pedido_endosso, /*e.dt_emissao,*/ p.dt_contratacao, e.endosso_id "
                SQL = SQL & " ORDER BY e.dt_emissao, /*e.dt_pedido_endosso,*/ p.dt_contratacao, e.endosso_id "
            Else
                'acrescentado And vProdutoId = 714 -11/09/2013 projeto 17860335 - Melhorias no SEGBR MC_Amaral -confitec
                If (IIf(IsNull(vProdutoId), "", vProdutoId)) = 714 Then
                    SQL = ""
                    SQL = SQL & "    SELECT DISTINCT NULL apolice_id, --rmaiellaro (19/01/2007)                                                                                      " & vbNewLine
                    SQL = SQL & "           ramo_tb.ramo_id,                                                                                                           " & vbNewLine
                    SQL = SQL & "           ramo_tb.nome nm_ramo,                                                                                                      " & vbNewLine
                    SQL = SQL & "           produto_tb.seguradora_cod_susep,                                                                                           " & vbNewLine
                    SQL = SQL & "           arquivo_produto_tb.sucursal_seguradora_id ,                                                                                " & vbNewLine
                    SQL = SQL & "           proposta_adesao_tb.proposta_bb,                                                                                            " & vbNewLine
                    SQL = SQL & "           proposta_tb.proposta_id,                                                                                                   " & vbNewLine
                    SQL = SQL & "           proposta_tb.produto_id,                                                                                                    " & vbNewLine
                    SQL = SQL & "           NULL endosso_id,                                                                                                           " & vbNewLine
                    SQL = SQL & "           NULL dt_pedido_endosso,                                                                                                    " & vbNewLine
                    SQL = SQL & "           NULL num_endosso_bb,                                                                                                       " & vbNewLine
                    SQL = SQL & "           NULL endosso_id,                                                                                                           " & vbNewLine
                    SQL = SQL & "           cliente_tb.nome,                                                                                                           " & vbNewLine
                    SQL = SQL & "           proposta_tb.dt_contratacao,                                                                                                " & vbNewLine
                    SQL = SQL & "           proposta_tb.prop_cliente_id,                                                                                               " & vbNewLine
                    SQL = SQL & "           descr_situacao = CASE proposta_tb.situacao                                                                                 " & vbNewLine
                    SQL = SQL & "                              WHEN 'e' THEN 'Em Estudo'                                                                               " & vbNewLine
                    SQL = SQL & "                              WHEN 'a' THEN 'Aceite'                                                                                  " & vbNewLine
                    SQL = SQL & "                              WHEN 'c' THEN 'Cancelada'                                                                               " & vbNewLine
                    SQL = SQL & "                              WHEN 'r' THEN 'Recusada'                                                                                " & vbNewLine
                    SQL = SQL & "                              WHEN 'i' THEN CASE (    SELECT COUNT(1)                                                                 " & vbNewLine    'alterado count(*)para count(1)12/09/2013 projeto 17860335 - Melhorias no SEGBR MC_Amaral -confitec
                    SQL = SQL & "                                                        FROM proposta_suspensa_tb WITH(NOLOCK)                                        " & vbNewLine
                    SQL = SQL & "                                                  INNER JOIN proposta_tb  WITH (NOLOCK)                                               " & vbNewLine
                    SQL = SQL & "                                                          ON proposta_tb.proposta_id = proposta_suspensa_tb.proposta_id               " & vbNewLine
                    SQL = SQL & "                                                       WHERE proposta_suspensa_tb.dt_inicio_suspensao <= '20050722'                   " & vbNewLine
                    SQL = SQL & "                                                         AND (proposta_suspensa_tb.dt_fim_suspensao IS NULL                           " & vbNewLine
                    SQL = SQL & "                                                          OR (proposta_suspensa_tb.dt_fim_suspensao is NOT NULL                       " & vbNewLine
                    SQL = SQL & "                                                              AND  proposta_suspensa_tb.cod_suspensao = '1')))                        " & vbNewLine
                    SQL = SQL & "                                              WHEN 0 THEN 'Ap�lice Emitida'                                                           " & vbNewLine
                    SQL = SQL & "                                              ELSE 'Cancelada'                                                                        " & vbNewLine
                    SQL = SQL & "                                            END                                                                                       " & vbNewLine
                    SQL = SQL & "                              WHEN 'p' THEN 'Em Estudo (erro de cadastramento)'                                                       " & vbNewLine
                    SQL = SQL & "                              WHEN 's' THEN 'Carta Recusa Emitida'                                                                    " & vbNewLine
                    SQL = SQL & "                            END,                                                                                                      " & vbNewLine
                    SQL = SQL & "           NULL tp_emissao,                                                                                                           " & vbNewLine
                    SQL = SQL & "           NULL nome_arquivo                                                                                                          " & vbNewLine
                    'Rmarins - 05/01/2006 - Inclus�o do campo seg_cliente ''''''''''''''''''''''''''''''''''''''''''''''''
                    SQL = SQL & ",          seg_cliente = CASE cliente_tb.seg_cliente                                                                                  " & vbNewLine
                    SQL = SQL & "                           WHEN 'private' THEN 'PRIVATE'                                                                              " & vbNewLine
                    SQL = SQL & "                           WHEN 'estilo' THEN 'ESTILO'                                                                                " & vbNewLine
                    SQL = SQL & "                           ELSE ' '                                                                                                   " & vbNewLine
                    SQL = SQL & "                         END                                                                                                          " & vbNewLine

                    SQL = SQL & "      FROM proposta_tb  WITH (NOLOCK)                                                                                                        " & vbNewLine
                    SQL = SQL & "INNER JOIN proposta_adesao_tb  WITH (NOLOCK)                                                                                                 " & vbNewLine
                    SQL = SQL & "        ON proposta_adesao_tb.proposta_id = proposta_tb.proposta_id                                                                   " & vbNewLine
                    SQL = SQL & "INNER JOIN produto_tb  WITH (NOLOCK)                                                                                                         " & vbNewLine
                    SQL = SQL & "        ON produto_tb.produto_id = proposta_tb.produto_id                                                                             " & vbNewLine
                    SQL = SQL & "INNER JOIN arquivo_produto_tb  WITH (NOLOCK)                                                                                                 " & vbNewLine
                    SQL = SQL & "        ON arquivo_produto_tb.produto_id = produto_tb.produto_id                                                                      " & vbNewLine
                    SQL = SQL & "INNER JOIN cliente_tb  WITH (NOLOCK)                                                                                                         " & vbNewLine
                    SQL = SQL & "        ON cliente_tb.cliente_id = proposta_tb.prop_cliente_id                                                                        " & vbNewLine
                    SQL = SQL & "INNER JOIN ramo_tb  WITH (NOLOCK)                                                                                                            " & vbNewLine
                    SQL = SQL & "        ON ramo_tb.ramo_id = proposta_tb.ramo_id                                                                                      " & vbNewLine
                    SQL = SQL & "     WHERE proposta_tb.proposta_id = " & txtArgumento.Text & vbNewLine
                    SQL = SQL & "       AND proposta_tb.produto_id = 714                                                                                               " & vbNewLine
                    SQL = SQL & "       AND proposta_tb.situacao<>'t'                                                                                                   " & vbNewLine

                    SQL = SQL & "  ORDER BY proposta_tb.dt_contratacao                                                                                                " & vbNewLine
                End If

            End If

        End If
        rc2.Close
        Set rc2 = Nothing
        '********************************************************************
        '***FIM CONSULTA PROPOSTA
        '********************************************************************************************************************************
        'SANTA CATARINA
        '********************************************************************************************************************************
    ElseIf cboTipoSelecao.Text = "N� da Proposta SC" Or cboTipoSelecao.Text = "N� da Apolice SC" Or cboTipoSelecao.Text = "N� da Certificado SC" Then

        Dim NumProposta As Long

        If Not IsNumeric(txtArgumento.Text) Then
            mensagem_erro 3, "Informe o Argumento"
            txtArgumento.SelStart = 0
            txtArgumento.SelLength = Len(txtArgumento)
            txtArgumento.SetFocus
            MousePointer = vbDefault
            Exit Sub
        End If
        '**INICIO  "N� da Proposta SC" , "N� da Apolice SC" , "N� da Certificado SC"


        SQL = ""
        SQL = SQL & "SELECT a.Situacao " & vbNewLine
        SQL = SQL & "     , a.produto_id    " & vbNewLine
        SQL = SQL & "     , p.nome   as nome_prod " & vbNewLine
        SQL = SQL & "     , cobranca = isnull(p.processa_cobranca,'n') "
        SQL = SQL & "     , a.origem_proposta_id" & vbNewLine
        SQL = SQL & "     , proposta_id " & vbNewLine
        SQL = SQL & "     , A.produto_id " & vbNewLine
        SQL = SQL & "  FROM proposta_tb   a  WITH (NOLOCK)  " & vbNewLine
        SQL = SQL & "  JOIN SANTA_CATARINA_LEGADO_TB b  WITH (NOLOCK)   " & vbNewLine
        SQL = SQL & "    ON b.alianca_cd_certificado = a.proposta_id " & vbNewLine
        SQL = SQL & "  JOIN Produto_tb p WITH(NOLOCK)"
        SQL = SQL & "    ON a.produto_id = p.produto_id "

        If cboTipoSelecao.Text = "N� da Proposta SC" Then
            SQL = SQL & " WHERE b.santa_catarina_nu_proposta = " & txtArgumento.Text
        ElseIf cboTipoSelecao.Text = "N� da Apolice SC" Then
            SQL = SQL & " WHERE b.santa_catarina_cd_apolice = " & txtArgumento.Text
        ElseIf cboTipoSelecao.Text = "N� da Certificado SC" Then
            SQL = SQL & " WHERE b.santa_catarina_cd_certificado = " & txtArgumento.Text
        End If
        SQL = SQL & " AND a.situacao <> 't' "



        Set rc2 = rdoCn2.OpenResultset(SQL)


        'lpinto/acintra 27/09/2005
        bPropostaRecusada = False
        'eduardo.amaral(Nova Consultoria) 23-02-2012 Demanda: BB SEGURO VIDA EMPRESA FLEX
        If Not rc2.EOF Then
            NumProposta = rc2(1)
            If UCase(rc2(0)) = "R" Or UCase(rc2(0)) = "S" Then
                bPropostaRecusada = True
            End If
        End If
        'fim eduardo.amaral(Nova Consultoria)
        'rmaiellaro (19/01/2007)
        bPropostaEstudo = False
        If Not rc2.EOF Then
            If UCase(rc2(0)) = "E" Or UCase(rc2(0)) = "P" Then
                bPropostaEstudo = True
            End If
        End If

        If Not rc2.EOF Then
            vProdutoId = IIf(IsNull(rc2!Produto_id), "", rc2!Produto_id)
            vNomeProd = rc2!Nome_prod
            ProcessaCobranca = rc2!Cobranca
            vSituacao = rc2!Situacao
            vorigem_proposta_id = rc2!origem_proposta_id

            If vorigem_proposta_id = 2 Then
                bPropostaALS = True
            Else
                bPropostaALS = False
            End If
        End If
        rc2.Close
        Set rc2 = Nothing


        'lpinto - 21/09/2005 - Consulta proposta recusada - proposta_fechada_tb
        If Not bPropostaRecusada And Not bPropostaEstudo Then    'rmaiellaro (19/01/2007)

            SQL = ""
            SQL = "             SELECT a.apolice_id" & vbNewLine
            SQL = SQL & "            , a.ramo_id" & vbNewLine
            SQL = SQL & "            , r.nome nm_ramo" & vbNewLine
            SQL = SQL & "            , a.seguradora_cod_susep" & vbNewLine
            SQL = SQL & "            , a.sucursal_seguradora_id " & vbNewLine
            SQL = SQL & "            , pf.proposta_bb" & vbNewLine
            SQL = SQL & "            , p.proposta_id" & vbNewLine
            SQL = SQL & "            , p.produto_id" & vbNewLine
            SQL = SQL & "            , e.endosso_id " & vbNewLine
            SQL = SQL & "            , e.dt_pedido_endosso" & vbNewLine
            SQL = SQL & "            , e.num_endosso_bb" & vbNewLine
            SQL = SQL & "            , e.tp_endosso_id " & vbNewLine
            SQL = SQL & "            , c.nome" & vbNewLine
            SQL = SQL & "            , p.dt_contratacao" & vbNewLine
            SQL = SQL & "            , p.prop_cliente_id " & vbNewLine
            SQL = SQL & "            , descr_situacao = case p.situacao " & vbNewLine
            SQL = SQL & "                               when 'e' then 'Em Estudo' " & vbNewLine
            SQL = SQL & "                               when 'a' then 'Aceite' " & vbNewLine
            SQL = SQL & "                               when 'c' then 'Cancelada' " & vbNewLine
            SQL = SQL & "                               when 'r' then 'Recusada' " & vbNewLine
            'SQL = SQL & "     when 'i' then 'Ap�lice Emitida' "
            'bcarneiro - 05/11/2003 - Vida Vida - Novas Funcionalidades ''''''''''''''''
            SQL = SQL & "                               when 'i' then " & vbNewLine
            SQL = SQL & "                                        case (select count(1) " & vbNewLine    'alterado count(*)para count(1)12/09/2013 projeto 17860335 - Melhorias no SEGBR MC_Amaral -confitec
            SQL = SQL & "                                                from proposta_suspensa_tb s WITH(NOLOCK)" & vbNewLine
            SQL = SQL & "                                               where p.proposta_id = s.proposta_id " & vbNewLine
            'SQL = SQL & "                 and s.dt_inicio_suspensao >= '" & Format(Data_Sistema, "yyyymmdd") & "'"
            SQL = SQL & "                                                 and s.dt_inicio_suspensao <= '" & Format(Data_Sistema, "yyyymmdd") & "'" & vbNewLine
            SQL = SQL & "                                                 and (s.dt_fim_suspensao is null " & vbNewLine
            SQL = SQL & "                                                  or (s.dt_fim_suspensao is not null " & vbNewLine
            SQL = SQL & "                                                 and  s.cod_suspensao = '1'))) " & vbNewLine
            SQL = SQL & "                                        when 0 then 'Ap�lice Emitida'" & vbNewLine
            'Alessandra Grig�rio - 25/06/2004 - Atendendo solic. Marcio Yoshimura
            'SQL = SQL & "            else 'Suspensa'        "
            SQL = SQL & "                                        else 'Cancelada'       " & vbNewLine
            'FIM
            SQL = SQL & "                                         end " & vbNewLine
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            SQL = SQL & "                               when 'p' then 'Em Estudo (erro de cadastramento)' " & vbNewLine
            SQL = SQL & "                               when 's' then 'Carta Recusa Emitida' " & vbNewLine
            SQL = SQL & "                                end " & vbNewLine
            SQL = SQL & "            , a.tp_emissao " & vbNewLine
            SQL = SQL & "            , NULL nome_arquivo " & vbNewLine
            'Rmarins - 05/01/2006 - Inclus�o do campo seg_cliente '''''''''''''''''''''
            SQL = SQL & "            , seg_cliente = CASE c.seg_cliente " & vbNewLine
            SQL = SQL & "                            WHEN 'private' THEN 'PRIVATE' " & vbNewLine
            SQL = SQL & "                            WHEN 'estilo' THEN 'ESTILO' " & vbNewLine
            SQL = SQL & "                            ELSE ' ' " & vbNewLine
            SQL = SQL & "                             END " & vbNewLine

            '            SQL = SQL & "FROM   proposta_tb p  WITH (NOLOCK) , apolice_tb a  WITH (NOLOCK) , endosso_tb e  WITH (NOLOCK)  " & vbNewLine
            '            SQL = SQL & ",      proposta_fechada_tb pf  WITH (NOLOCK) , cliente_tb c  WITH (NOLOCK) , ramo_tb r   WITH (NOLOCK)  " & vbNewLine
            SQL = SQL & "        FROM   proposta_tb p  WITH (NOLOCK) " & vbNewLine
            SQL = SQL & "        join  apolice_tb a  WITH (NOLOCK)" & vbNewLine
            SQL = SQL & "          on  a.proposta_id = p.proposta_id " & vbNewLine
            SQL = SQL & "   left join endosso_tb e  WITH (NOLOCK)  " & vbNewLine
            SQL = SQL & "          on  e.proposta_id = p.proposta_id " & vbNewLine
            SQL = SQL & "         AND  e.tp_endosso_id not in (10,100,101,200) " & vbNewLine
            SQL = SQL & "        join  proposta_fechada_tb pf  WITH (NOLOCK) "
            SQL = SQL & "          on  pf.proposta_id = p.proposta_id " & vbNewLine
            SQL = SQL & "        join  cliente_tb c  WITH (NOLOCK)"
            SQL = SQL & "          on  c.cliente_id = p.prop_cliente_id " & vbNewLine
            SQL = SQL & "        join  ramo_tb r   WITH (NOLOCK)  " & vbNewLine
            SQL = SQL & "          on  r.ramo_id = a.ramo_id " & vbNewLine
            SQL = SQL & "       WHERE  p.proposta_id = " & NumProposta & vbNewLine
            '            SQL = SQL & "  AND  a.proposta_id = p.proposta_id " & vbNewLine
            '            SQL = SQL & "  AND  pf.proposta_id = p.proposta_id " & vbNewLine
            'SQL = SQL & "  AND  e.proposta_id =* p.proposta_id " & vbNewLine
            '' N�o inclui os endossos financeiros
            '            SQL = SQL & "  AND  e.tp_endosso_id not in (10,100,101,200) " & vbNewLine
            '            SQL = SQL & "  AND  c.cliente_id = p.prop_cliente_id " & vbNewLine
            '            SQL = SQL & "  AND  r.ramo_id = a.ramo_id " & vbNewLine
            SQL = SQL & "        AND  p.produto_id in (" & ProdutosValidos & ") " & vbNewLine

            SQL = SQL & "        AND  p.situacao<>'t' " & vbNewLine

            SQL = SQL & " ORDER BY  e.dt_emissao, /*e.dt_pedido_endosso,*/ p.dt_contratacao, e.endosso_id "
            'FIM

        Else

            SQL = ""
            SQL = SQL & "    SELECT DISTINCT NULL apolice_id,  --rmaiellaro (19/01/2007)                                                                                      " & vbNewLine
            SQL = SQL & "           ramo_tb.ramo_id,                                                                                                            " & vbNewLine
            SQL = SQL & "           ramo_tb.nome nm_ramo,                                                                                                       " & vbNewLine
            SQL = SQL & "           produto_tb.seguradora_cod_susep,                                                                                            " & vbNewLine
            SQL = SQL & "           arquivo_produto_tb.sucursal_seguradora_id,                                                                                  " & vbNewLine
            SQL = SQL & "           proposta_fechada_tb.proposta_bb,                                                                                            " & vbNewLine
            SQL = SQL & "           proposta_tb.proposta_id,                                                                                                    " & vbNewLine
            SQL = SQL & "           proposta_tb.produto_id,                                                                                                     " & vbNewLine
            SQL = SQL & "           NULL endosso_id,                                                                                                            " & vbNewLine
            SQL = SQL & "           NULL dt_pedido_endosso,                                                                                                     " & vbNewLine
            SQL = SQL & "           NULL num_endosso_bb,                                                                                                        " & vbNewLine
            SQL = SQL & "           NULL tp_endosso_id,                                                                                                         " & vbNewLine
            SQL = SQL & "           cliente_tb.nome,                                                                                                            " & vbNewLine
            SQL = SQL & "           proposta_tb.dt_contratacao,                                                                                                 " & vbNewLine
            SQL = SQL & "           proposta_tb.prop_cliente_id,                                                                                                " & vbNewLine
            SQL = SQL & "           descr_situacao = CASE proposta_tb.situacao                                                                                  " & vbNewLine
            SQL = SQL & "                              WHEN 'e' THEN 'Em Estudo'                                                                                " & vbNewLine
            SQL = SQL & "                              WHEN 'a' THEN 'Aceite'                                                                                   " & vbNewLine
            SQL = SQL & "                              WHEN 'c' THEN 'Cancelada'                                                                                " & vbNewLine
            SQL = SQL & "                              WHEN 'r' THEN 'Recusada'                                                                                 " & vbNewLine
            SQL = SQL & "                              WHEN 'i' THEN CASE (    SELECT COUNT(1)                                                                  " & vbNewLine    'alterado count(*)para count(1)12/09/2013 projeto 17860335 - Melhorias no SEGBR MC_Amaral -confitec
            SQL = SQL & "                                                        FROM proposta_suspensa_tb   WITH(NOLOCK)                                       " & vbNewLine
            SQL = SQL & "                                                  INNER JOIN proposta_tb   WITH(NOLOCK)                                                " & vbNewLine
            SQL = SQL & "                                                          ON proposta_tb.proposta_id = proposta_suspensa_tb.proposta_id                " & vbNewLine
            SQL = SQL & "                                                       WHERE proposta_suspensa_tb.dt_inicio_suspensao <= '20050722'                    " & vbNewLine
            SQL = SQL & "                                                         AND (proposta_suspensa_tb.dt_fim_suspensao IS NULL                            " & vbNewLine
            SQL = SQL & "                                                          OR (proposta_suspensa_tb.dt_fim_suspensao IS NOT NULL                        " & vbNewLine
            SQL = SQL & "                                                              AND  proposta_suspensa_tb.cod_suspensao = '1')))                         " & vbNewLine
            SQL = SQL & "                                              WHEN 0 THEN 'Ap�lice Emitida'                                                            " & vbNewLine
            SQL = SQL & "                                              ELSE 'Cancelada'                                                                         " & vbNewLine
            SQL = SQL & "                                            END                                                                                        " & vbNewLine
            SQL = SQL & "                              WHEN 'p' THEN 'Em Estudo (erro de cadastramento)'                                                        " & vbNewLine
            SQL = SQL & "                              WHEN 's' THEN 'Carta Recusa Emitida'                                                                     " & vbNewLine
            SQL = SQL & "                            END,                                                                                                       " & vbNewLine
            SQL = SQL & "           NULL tp_emissao,                                                                                                            " & vbNewLine
            SQL = SQL & "           NULL nome_arquivo                                                                                                           " & vbNewLine
            'Rmarins - 05/01/2006 - Inclus�o do campo seg_cliente ''''''''''''''''''''''''''''''''''
            SQL = SQL & ",          seg_cliente = CASE cliente_tb.seg_cliente                                                                                   " & vbNewLine
            SQL = SQL & "                           WHEN 'private' THEN 'PRIVATE'                                                                               " & vbNewLine
            SQL = SQL & "                           WHEN 'estilo' THEN 'ESTILO'                                                                                 " & vbNewLine
            SQL = SQL & "                           ELSE ' '                                                                                                    " & vbNewLine
            SQL = SQL & "                         END                                                                                                           " & vbNewLine

            SQL = SQL & "      FROM proposta_tb WITH (NOLOCK)                                                                                                          " & vbNewLine
            SQL = SQL & "INNER JOIN proposta_fechada_tb  WITH (NOLOCK)                                                                                                 " & vbNewLine
            SQL = SQL & "        ON proposta_fechada_tb.proposta_id = proposta_tb.proposta_id                                                                   " & vbNewLine
            SQL = SQL & "INNER JOIN produto_tb  WITH (NOLOCK)                                                                                                          " & vbNewLine
            SQL = SQL & "        ON produto_tb.produto_id = proposta_tb.produto_id                                                                              " & vbNewLine
            SQL = SQL & "INNER JOIN arquivo_produto_tb  WITH (NOLOCK)                                                                                                  " & vbNewLine
            SQL = SQL & "        ON arquivo_produto_tb.produto_id = proposta_tb.produto_id                                                                      " & vbNewLine
            SQL = SQL & "       AND arquivo_produto_tb.ramo_id = proposta_tb.ramo_id                                                                            " & vbNewLine
            SQL = SQL & "INNER JOIN cliente_tb  WITH (NOLOCK)                                                                                                          " & vbNewLine
            SQL = SQL & "        ON cliente_tb.cliente_id = proposta_tb.prop_cliente_id                                                                         " & vbNewLine
            SQL = SQL & "INNER JOIN ramo_tb  WITH (NOLOCK)                                                                                                             " & vbNewLine
            SQL = SQL & "        ON ramo_tb.ramo_id = proposta_tb.ramo_id                                                                                       " & vbNewLine
            SQL = SQL & "     WHERE proposta_tb.proposta_id = " & NumProposta & vbNewLine
            SQL = SQL & "       AND proposta_fechada_tb.proposta_id = proposta_tb.proposta_id                                                                   " & vbNewLine
            SQL = SQL & "       AND proposta_tb.produto_id in (" & ProdutosValidos & ")                                                                         " & vbNewLine
            SQL = SQL & "       AND proposta_tb.situacao<>'t'                                                                                                   " & vbNewLine

            SQL = SQL & "  ORDER BY proposta_tb.dt_contratacao                                                                                                  " & vbNewLine

        End If

        '*****************************************************************************
        '** Descri��o: Implementa��o de consulta por proposta ades�o                **
        '** Motivo: Atender consultas para o produto 714                            **
        '** Autor: Febner (Fabio Alves de Araujo Ebner - JR & P(SP)                 **
        '** Data: 10/06/03                                                          **
        '*****************************************************************************

        Sql_Aux = SQL
        Set rc2 = rdoCn2.OpenResultset(Sql_Aux)
        If rc2.EOF Then
            ' lpinto 21/09/2005 - Consulta proposta recusada - proposta_adesao_tb
            If Not bPropostaRecusada And Not bPropostaEstudo And (IIf(IsNull(vProdutoId), "", vProdutoId)) = 714 Then  'rmaiellaro (19/01/2007)

                SQL = ""
                SQL = SQL & "       SELECT a.apolice_id" & vbNewLine
                SQL = SQL & "            , a.ramo_id" & vbNewLine
                SQL = SQL & "            , r.nome nm_ramo" & vbNewLine
                SQL = SQL & "            , a.seguradora_cod_susep" & vbNewLine
                SQL = SQL & "            , a.sucursal_seguradora_id  " & vbNewLine
                SQL = SQL & "            , pf.proposta_bb" & vbNewLine
                SQL = SQL & "            , p.proposta_id" & vbNewLine
                SQL = SQL & "            , p.produto_id" & vbNewLine
                SQL = SQL & "            , e.endosso_id " & vbNewLine
                SQL = SQL & "            , e.dt_pedido_endosso" & vbNewLine
                SQL = SQL & "            , e.num_endosso_bb" & vbNewLine
                SQL = SQL & "            , e.tp_endosso_id"
                SQL = SQL & "            , c.nome " & vbNewLine
                SQL = SQL & "            , p.dt_contratacao" & vbNewLine
                SQL = SQL & "            , p.prop_cliente_id " & vbNewLine
                SQL = SQL & "            , descr_situacao = case p.situacao " & vbNewLine
                SQL = SQL & "                               when 'e' then 'Em Estudo' " & vbNewLine
                SQL = SQL & "                               when 'a' then 'Aceite' " & vbNewLine
                SQL = SQL & "                               when 'c' then 'Cancelada' " & vbNewLine
                SQL = SQL & "                               when 'r' then 'Recusada' " & vbNewLine
                'SQL = SQL & "           when 'i' then 'Ap�lice Emitida' "
                'bcarneiro - 05/11/2003 - Vida Vida - Novas Funcionalidades ''''''''''''''''
                SQL = SQL & "                               when 'i' then " & vbNewLine
                SQL = SQL & "                                        case (select count(1)" & vbNewLine
                SQL = SQL & "                                                from proposta_suspensa_tb s WITH(NOLOCK)" & vbNewLine
                SQL = SQL & "                                               where p.proposta_id = s.proposta_id " & vbNewLine
                'SQL = SQL & "                and s.dt_inicio_suspensao >= '" & Format(Data_Sistema, "yyyymmdd") & "'"
                SQL = SQL & "                                                 and s.dt_inicio_suspensao <= '" & Format(Data_Sistema, "yyyymmdd") & "'" & vbNewLine
                SQL = SQL & "                                                 and (s.dt_fim_suspensao is null " & vbNewLine
                SQL = SQL & "                                                  or (s.dt_fim_suspensao is not null " & vbNewLine
                SQL = SQL & "                                                 and  s.cod_suspensao = '1')))" & vbNewLine
                SQL = SQL & "                                       when 0 then 'Ap�lice Emitida' " & vbNewLine
                SQL = SQL & "                                       else 'Cancelada'       " & vbNewLine
                SQL = SQL & "                                       end " & vbNewLine
                ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                SQL = SQL & "                               when 'p' then 'Em Estudo (erro de cadastramento)' " & vbNewLine
                SQL = SQL & "                               when 's' then 'Carta Recusa Emitida' " & vbNewLine
                SQL = SQL & "                                end"
                SQL = SQL & "           , ap.tp_emissao"
                SQL = SQL & "           , NULL nome_arquivo " & vbNewLine
                SQL = SQL & "           ,  seg_cliente = CASE c.seg_cliente    " & vbNewLine
                SQL = SQL & "                            WHEN 'private' THEN 'PRIVATE'  " & vbNewLine
                SQL = SQL & "                            WHEN 'estilo'  THEN 'ESTILO'    " & vbNewLine
                SQL = SQL & "                            ELSE ' '   " & vbNewLine
                SQL = SQL & "                            END  " & vbNewLine

                SQL = SQL & "        FROM proposta_tb p  WITH (NOLOCK)               " & vbNewLine
                SQL = SQL & "           , certificado_tb a  WITH (NOLOCK)            " & vbNewLine
                SQL = SQL & "           , endosso_tb e  WITH (NOLOCK)                " & vbNewLine
                SQL = SQL & "           , proposta_adesao_tb pf  WITH (NOLOCK)       " & vbNewLine
                SQL = SQL & "           , cliente_tb c  WITH (NOLOCK)                " & vbNewLine
                SQL = SQL & "           , ramo_tb r  WITH (NOLOCK)          " & vbNewLine
                SQL = SQL & "           , apolice_tb ap  WITH (NOLOCK)      " & vbNewLine
                SQL = SQL & "       WHERE p.proposta_id = " & NumProposta & vbNewLine
                SQL = SQL & "         AND a.proposta_id = p.proposta_id     " & vbNewLine
                SQL = SQL & "         AND pf.proposta_id = p.proposta_id    " & vbNewLine
                SQL = SQL & "         AND e.proposta_id = p.proposta_id     " & vbNewLine
                SQL = SQL & "         AND e.tp_endosso_id not in (10,100,101,200) " & vbNewLine
                SQL = SQL & "         AND c.cliente_id = p.prop_cliente_id  " & vbNewLine
                SQL = SQL & "         AND r.ramo_id = a.ramo_id             " & vbNewLine
                SQL = SQL & "         AND a.apolice_id = ap.apolice_id      " & vbNewLine
                SQL = SQL & "         AND a.ramo_id = ap.ramo_id            " & vbNewLine
                SQL = SQL & "         AND p.produto_id = 714                " & vbNewLine
                SQL = SQL & "         AND p.situacao<>'t'                   " & vbNewLine
                SQL = SQL & " ORDER BY e.dt_emissao, /*e.dt_pedido_endosso,*/ p.dt_contratacao, e.endosso_id "
            Else

                If (IIf(IsNull(vProdutoId), "", vProdutoId)) = 714 Then
                    SQL = ""
                    SQL = SQL & "    SELECT DISTINCT NULL apolice_id, --rmaiellaro (19/01/2007)                                                                                      " & vbNewLine
                    SQL = SQL & "           ramo_tb.ramo_id,                                                                                                           " & vbNewLine
                    SQL = SQL & "           ramo_tb.nome nm_ramo,                                                                                                      " & vbNewLine
                    SQL = SQL & "           produto_tb.seguradora_cod_susep,                                                                                           " & vbNewLine
                    SQL = SQL & "           arquivo_produto_tb.sucursal_seguradora_id ,                                                                                " & vbNewLine
                    SQL = SQL & "           proposta_adesao_tb.proposta_bb,                                                                                            " & vbNewLine
                    SQL = SQL & "           proposta_tb.proposta_id,                                                                                                   " & vbNewLine
                    SQL = SQL & "           proposta_tb.produto_id,                                                                                                    " & vbNewLine
                    SQL = SQL & "           NULL endosso_id,                                                                                                           " & vbNewLine
                    SQL = SQL & "           NULL dt_pedido_endosso,                                                                                                    " & vbNewLine
                    SQL = SQL & "           NULL num_endosso_bb,                                                                                                       " & vbNewLine
                    SQL = SQL & "           NULL endosso_id,                                                                                                           " & vbNewLine
                    SQL = SQL & "           cliente_tb.nome,                                                                                                           " & vbNewLine
                    SQL = SQL & "           proposta_tb.dt_contratacao,                                                                                                " & vbNewLine
                    SQL = SQL & "           proposta_tb.prop_cliente_id,                                                                                               " & vbNewLine
                    SQL = SQL & "           descr_situacao = CASE proposta_tb.situacao                                                                                 " & vbNewLine
                    SQL = SQL & "                              WHEN 'e' THEN 'Em Estudo'                                                                               " & vbNewLine
                    SQL = SQL & "                              WHEN 'a' THEN 'Aceite'                                                                                  " & vbNewLine
                    SQL = SQL & "                              WHEN 'c' THEN 'Cancelada'                                                                               " & vbNewLine
                    SQL = SQL & "                              WHEN 'r' THEN 'Recusada'                                                                                " & vbNewLine
                    SQL = SQL & "                              WHEN 'i' THEN CASE (    SELECT COUNT(1)                                                                 " & vbNewLine    'alterado count(*)para count(1)12/09/2013 projeto 17860335 - Melhorias no SEGBR MC_Amaral -confitec
                    SQL = SQL & "                                                        FROM proposta_suspensa_tb  WITH(NOLOCK)                                       " & vbNewLine
                    SQL = SQL & "                                                  INNER JOIN proposta_tb  WITH (NOLOCK)                                               " & vbNewLine
                    SQL = SQL & "                                                          ON proposta_tb.proposta_id = proposta_suspensa_tb.proposta_id               " & vbNewLine
                    SQL = SQL & "                                                       WHERE proposta_suspensa_tb.dt_inicio_suspensao <= '20050722'                   " & vbNewLine
                    SQL = SQL & "                                                         AND (proposta_suspensa_tb.dt_fim_suspensao IS NULL                           " & vbNewLine
                    SQL = SQL & "                                                          OR (proposta_suspensa_tb.dt_fim_suspensao is NOT NULL                       " & vbNewLine
                    SQL = SQL & "                                                              AND  proposta_suspensa_tb.cod_suspensao = '1')))                        " & vbNewLine
                    SQL = SQL & "                                              WHEN 0 THEN 'Ap�lice Emitida'                                                           " & vbNewLine
                    SQL = SQL & "                                              ELSE 'Cancelada'                                                                        " & vbNewLine
                    SQL = SQL & "                                            END                                                                                       " & vbNewLine
                    SQL = SQL & "                              WHEN 'p' THEN 'Em Estudo (erro de cadastramento)'                                                       " & vbNewLine
                    SQL = SQL & "                              WHEN 's' THEN 'Carta Recusa Emitida'                                                                    " & vbNewLine
                    SQL = SQL & "                            END,                                                                                                      " & vbNewLine
                    SQL = SQL & "           NULL tp_emissao,                                                                                                           " & vbNewLine
                    SQL = SQL & "           NULL nome_arquivo                                                                                                          " & vbNewLine
                    SQL = SQL & ",          seg_cliente = CASE cliente_tb.seg_cliente                                                                                  " & vbNewLine
                    SQL = SQL & "                           WHEN 'private' THEN 'PRIVATE'                                                                              " & vbNewLine
                    SQL = SQL & "                           WHEN 'estilo' THEN 'ESTILO'                                                                                " & vbNewLine
                    SQL = SQL & "                           ELSE ' '                                                                                                   " & vbNewLine
                    SQL = SQL & "                         END                                                                                                          " & vbNewLine
                    SQL = SQL & "      FROM proposta_tb  WITH (NOLOCK)                                                                                                        " & vbNewLine
                    SQL = SQL & "INNER JOIN proposta_adesao_tb  WITH (NOLOCK)                                                                                                 " & vbNewLine
                    SQL = SQL & "        ON proposta_adesao_tb.proposta_id = proposta_tb.proposta_id                                                                   " & vbNewLine
                    SQL = SQL & "INNER JOIN produto_tb  WITH (NOLOCK)                                                                                                         " & vbNewLine
                    SQL = SQL & "        ON produto_tb.produto_id = proposta_tb.produto_id                                                                             " & vbNewLine
                    SQL = SQL & "INNER JOIN arquivo_produto_tb  WITH (NOLOCK)                                                                                                 " & vbNewLine
                    SQL = SQL & "        ON arquivo_produto_tb.produto_id = produto_tb.produto_id                                                                      " & vbNewLine
                    SQL = SQL & "INNER JOIN cliente_tb  WITH (NOLOCK)                                                                                                         " & vbNewLine
                    SQL = SQL & "        ON cliente_tb.cliente_id = proposta_tb.prop_cliente_id                                                                        " & vbNewLine
                    SQL = SQL & "INNER JOIN ramo_tb  WITH (NOLOCK)                                                                                                            " & vbNewLine
                    SQL = SQL & "        ON ramo_tb.ramo_id = proposta_tb.ramo_id                                                                                      " & vbNewLine
                    SQL = SQL & "     WHERE proposta_tb.proposta_id = " & NumProposta & vbNewLine
                    SQL = SQL & "       AND proposta_tb.produto_id = 714                                                                                               " & vbNewLine
                    SQL = SQL & "       AND proposta_tb.situacao<>'t'                                                                                                   " & vbNewLine
                    SQL = SQL & "  ORDER BY proposta_tb.dt_contratacao                                                                                                " & vbNewLine

                End If

            End If

        End If
        rc2.Close
        Set rc2 = Nothing

        '***FIM "N� da Proposta SC" , "N� da Apolice SC" , "N� da Certificado SC"

    ElseIf cboTipoSelecao.Text = "N� do Cliente" Then
        If Not IsNumeric(txtArgumento.Text) Then
            mensagem_erro 3, "N� do Cliente"
            txtArgumento.SelStart = 0
            txtArgumento.SelLength = Len(txtArgumento)
            txtArgumento.SetFocus
            MousePointer = vbDefault
            Exit Sub
        End If
        '
        PesqCodCliente = True
        '
        '*** CLIENTE NUMERO
        SQL = ""
        SQL = SQL & "  SELECT p.proposta_id"
        SQL = SQL & "       , p.dt_contratacao"
        SQL = SQL & "       , c.nome " & vbNewLine
        SQL = SQL & "       , p.prop_cliente_id, p.produto_id " & vbNewLine
        SQL = SQL & "       , p.Situacao " & vbNewLine
        SQL = SQL & "       , p.produto_id    " & vbNewLine
        SQL = SQL & "       , pr.nome as nome_prod   " & vbNewLine
        SQL = SQL & "       , isnull(pr.processa_cobranca,'n') as Cobranca"
        SQL = SQL & "       , p.origem_proposta_id" & vbNewLine
        SQL = SQL & "       , p.proposta_id " & vbNewLine
        SQL = SQL & "       , descr_situacao = case p.situacao " & vbNewLine
        SQL = SQL & "                          when 'e' then 'Em Estudo' " & vbNewLine
        SQL = SQL & "                          when 'a' then 'Aceite' " & vbNewLine
        SQL = SQL & "                          when 'c' then 'Cancelada' " & vbNewLine
        SQL = SQL & "                          when 'r' then 'Recusada' " & vbNewLine
        'SQL = SQL & "    when 'i' then 'Ap�lice Emitida' "
        'bcarneiro - 05/11/2003 - Vida Vida - Novas Funcionalidades ''''''''''''''''''''''''
        SQL = SQL & "                          when 'i' then " & vbNewLine
        SQL = SQL & "                                   case (select count(1)" & vbNewLine
        SQL = SQL & "                                           from proposta_suspensa_tb s WITH(NOLOCK)" & vbNewLine
        SQL = SQL & "                                          where p.proposta_id = s.proposta_id" & vbNewLine
        'SQL = SQL & "                and s.dt_inicio_suspensao >= '" & Format(Data_Sistema, "yyyymmdd") & "'"
        SQL = SQL & "                                            and s.dt_inicio_suspensao <= '" & Format(Data_Sistema, "yyyymmdd") & "'" & vbNewLine
        SQL = SQL & "                                            and (s.dt_fim_suspensao is null" & vbNewLine
        SQL = SQL & "                                             or (s.dt_fim_suspensao is not null" & vbNewLine
        SQL = SQL & "                                            and  s.cod_suspensao = '1')))" & vbNewLine
        SQL = SQL & "                                   when 0 then 'Ap�lice Emitida'" & vbNewLine
        'Alessandra Grig�rio - 25/06/2004 - Atendendo solic. Marcio Yoshimura
        'SQL = SQL & "             else 'Suspensa'        "
        SQL = SQL & "                                   else 'Cancelada'        " & vbNewLine
        'FIM
        SQL = SQL & "                                    end " & vbNewLine
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        SQL = SQL & "                       when 'p' then 'Em Estudo (erro de cadastramento)' " & vbNewLine
        SQL = SQL & "                       when 's' then 'Carta Recusa Emitida' " & vbNewLine
        SQL = SQL & "                       end " & vbNewLine
        SQL = SQL & "       ,NULL proposta_bb " & vbNewLine
        SQL = SQL & "       ,NULL nome_arquivo " & vbNewLine

        'Rmarins - 05/01/2006 - Inclus�o do campo seg_cliente ''''''''''''''''''''''''''''''
        SQL = SQL & "       , seg_cliente = CASE c.seg_cliente " & vbNewLine
        SQL = SQL & "                       WHEN 'private' THEN 'PRIVATE'  " & vbNewLine
        SQL = SQL & "                       WHEN 'estilo' THEN 'ESTILO' " & vbNewLine
        SQL = SQL & "                       ELSE ' ' " & vbNewLine
        SQL = SQL & "                        END  " & vbNewLine

        SQL = SQL & "    FROM proposta_tb p  WITH (NOLOCK) " & vbNewLine
        SQL = SQL & "    JOIN  cliente_tb c   WITH (NOLOCK) " & vbNewLine
        SQL = SQL & "      ON   p.prop_cliente_id = c.cliente_id " & vbNewLine
        SQL = SQL & "    JOIN Produto_tb pr WITH(NOLOCK)"
        SQL = SQL & "      ON pr.produto_id = p.produto_id "
        SQL = SQL & "   WHERE c.cliente_id = " & txtArgumento & vbNewLine

        SQL = SQL & "     AND   p.produto_id in (" & ProdutosValidos & ") " & vbNewLine
        SQL = SQL & "     AND   p.situacao<>'t' " & vbNewLine

        '**FIM CLIENTE NUMERO

        ' Nova pesquisa - por nome do cliente.
    ElseIf cboTipoSelecao.Text = "Nome do Cliente" Then
        If Len(txtArgumento.Text) = 0 Then
            mensagem_erro 3, "Nome do Cliente"
            txtArgumento.SelStart = 0
            txtArgumento.SelLength = Len(txtArgumento)
            txtArgumento.SetFocus
            MousePointer = vbDefault
            Exit Sub
        End If
        '
        PesqNomeCliente = True
        '
        '**INICIO CLIENTE NOME
        SQL = ""
        SQL = SQL & "   SELECT c.nome as nome"
        SQL = SQL & "        , p.proposta_id"
        SQL = SQL & "        , p.dt_contratacao " & vbNewLine
        SQL = SQL & "        , p.prop_cliente_id"
        SQL = SQL & "        , p.produto_id " & vbNewLine
        SQL = SQL & "        , descr_situacao = case p.situacao " & vbNewLine
        SQL = SQL & "                           when 'e' then 'Em Estudo' " & vbNewLine
        SQL = SQL & "                           when 'a' then 'Aceite' " & vbNewLine
        SQL = SQL & "                           when 'c' then 'Cancelada' " & vbNewLine
        SQL = SQL & "                           when 'r' then 'Recusada' " & vbNewLine
        'SQL = SQL & "    when 'i' then 'Ap�lice Emitida' "
        'bcarneiro - 05/11/2003 - Vida Vida - Novas Funcionalidades ''''''''''''''''''''''''
        SQL = SQL & "                           when 'i' then "
        SQL = SQL & "                                    case (select count(1)" & vbNewLine
        SQL = SQL & "                                            from proposta_suspensa_tb s WITH(NOLOCK)" & vbNewLine
        SQL = SQL & "                                           where p.proposta_id = s.proposta_id" & vbNewLine
        'SQL = SQL & "                     and s.dt_inicio_suspensao >= '" & Format(Data_Sistema, "yyyymmdd") & "'"
        SQL = SQL & "                                             and s.dt_inicio_suspensao <= '" & Format(Data_Sistema, "yyyymmdd") & "'" & vbNewLine
        SQL = SQL & "                                             and (s.dt_fim_suspensao is null" & vbNewLine
        SQL = SQL & "                                              or (s.dt_fim_suspensao is not null" & vbNewLine
        SQL = SQL & "                                             and  s.cod_suspensao = '1')))" & vbNewLine
        SQL = SQL & "                                    when 0 then 'Ap�lice Emitida'" & vbNewLine
        'Alessandra Grig�rio - 25/06/2004 - Atendendo solic. Marcio Yoshimura
        'SQL = SQL & "              else 'Suspensa'        "
        SQL = SQL & "                                    else 'Cancelada'       " & vbNewLine
        'FIM
        SQL = SQL & "                                     end " & vbNewLine
        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        SQL = SQL & "                           when 'p' then 'Em Estudo (erro de cadastramento)' " & vbNewLine
        SQL = SQL & "                           when 's' then 'Carta Recusa Emitida' " & vbNewLine
        SQL = SQL & "                            end  " & vbNewLine
        SQL = SQL & "       , pf.proposta_bb " & vbNewLine
        SQL = SQL & "       , NULL nome_arquivo " & vbNewLine

        'Rmarins - 05/01/2006 - Inclus�o do campo seg_cliente '''''''''''''''''''''''''''''''
        SQL = SQL & "       , seg_cliente = CASE c.seg_cliente " & vbNewLine
        SQL = SQL & "                       WHEN 'private' THEN 'PRIVATE' " & vbNewLine
        SQL = SQL & "                       WHEN 'estilo' THEN 'ESTILO' " & vbNewLine
        SQL = SQL & "                       ELSE ' ' " & vbNewLine
        SQL = SQL & "                        END  " & vbNewLine
        SQL = SQL & "      , p.Situacao " & vbNewLine    'MCAMARAL MELHORIAS
        SQL = SQL & "      , p.produto_id    " & vbNewLine    'MCAMARAL MELHORIAS
        SQL = SQL & "      , pr.nome as nome_prod   " & vbNewLine    'MCAMARAL MELHORIAS
        SQL = SQL & "      , isnull(pr.processa_cobranca,'n') as Cobranca"    'MCAMARAL MELHORIAS
        SQL = SQL & "      , p.origem_proposta_id" & vbNewLine        'MCAMARAL MELHORIAS
        '        SQL = SQL & " FROM proposta_tb p  WITH (NOLOCK) , cliente_tb c  WITH (NOLOCK) , proposta_fechada_tb pf   WITH (NOLOCK) " & vbNewLine
        SQL = SQL & "   FROM proposta_tb p  WITH (NOLOCK) "
        SQL = SQL & "   JOIN  cliente_tb c  WITH (NOLOCK) "
        SQL = SQL & "     ON   p.prop_cliente_id = c.cliente_id " & vbNewLine
        SQL = SQL & "   JOIN  proposta_fechada_tb pf   WITH (NOLOCK) "
        SQL = SQL & "     ON   pf.proposta_id = p.proposta_id " & vbNewLine
        SQL = SQL & "   JOIN Produto_tb pr WITH(NOLOCK)"
        SQL = SQL & "     ON pr.produto_id = p.produto_id "
        SQL = SQL & " WHERE c.nome like '" & (txtArgumento) & "%'"
        '        SQL = SQL & "   AND   p.prop_cliente_id = c.cliente_id " & vbNewLine
        '        SQL = SQL & "   AND   pf.proposta_id = p.proposta_id " & vbNewLine
        SQL = SQL & "   AND   p.produto_id in (" & ProdutosValidos & ") " & vbNewLine
        SQL = SQL & "   AND   p.situacao<>'t' " & vbNewLine

        'SQL = SQL & " ORDER BY c.nome "
        ' Lpinto - 27/09/2005
        SQL = SQL & "" & vbNewLine
        SQL = SQL & "" & vbNewLine
        SQL = SQL & " UNION ALL" & vbNewLine
        SQL = SQL & "" & vbNewLine
        SQL = SQL & "" & vbNewLine
        SQL = SQL & " SELECT proponente nome                          " & vbNewLine
        SQL = SQL & "      , emi_proposta_tb.proposta_id                            " & vbNewLine
        SQL = SQL & "      , emi_proposta_tb.dt_contratacao                         " & vbNewLine
        SQL = SQL & "      , NULL prop_cliente_id                   " & vbNewLine
        SQL = SQL & "      , NULL produto_id                     " & vbNewLine
        SQL = SQL & "      , descr_situacao = 'N�o emitida'         " & vbNewLine
        SQL = SQL & "      , emi_proposta_tb.proposta_bb                            " & vbNewLine
        SQL = SQL & "      , NULL nome_arquivo                       " & vbNewLine
        'Rmarins - 05/01/2006 - Inclus�o do campo seg_cliente ''''''''''''''''''''''''''
        SQL = SQL & "      , NULL seg_cliente                        " & vbNewLine
        SQL = SQL & "      , proposta_tb.Situacao " & vbNewLine    'MCAMARAL MELHORIAS
        SQL = SQL & "      , proposta_tb.produto_id    " & vbNewLine    'MCAMARAL MELHORIAS
        SQL = SQL & "      , pr.nome as nome_prod   " & vbNewLine    'MCAMARAL MELHORIAS
        SQL = SQL & "      , isnull(pr.processa_cobranca,'n') as Cobranca"    'MCAMARAL MELHORIAS
        SQL = SQL & "      , proposta_tb.origem_proposta_id" & vbNewLine        'MCAMARAL MELHORIAS


        SQL = SQL & "   FROM emi_proposta_tb  WITH (NOLOCK)                 " & vbNewLine
        SQL = SQL & "   join proposta_tb  WITH (NOLOCK)  on proposta_tb.proposta_id = emi_proposta_tb.proposta_id   " & vbNewLine
        SQL = SQL & "   JOIN Produto_tb pr WITH(NOLOCK)"
        SQL = SQL & "     ON pr.produto_id = proposta_tb.produto_id "
        SQL = SQL & "  WHERE emi_proposta_tb.situacao = 'n'                          " & vbNewLine
        SQL = SQL & "    AND proponente LIKE '" & (txtArgumento) & "%'"
        SQL = SQL & "    AND proposta_tb.situacao<>'t' " & vbNewLine
        SQL = SQL & "                                                  " & vbNewLine
        SQL = SQL & " UNION ALL                                        " & vbNewLine
        SQL = SQL & "                                                  " & vbNewLine
        SQL = SQL & " SELECT proponente nome                        " & vbNewLine
        SQL = SQL & "      , emi_re_proposta_tb.proposta_id                            " & vbNewLine
        SQL = SQL & "      , emi_re_proposta_tb.dt_contratacao                         " & vbNewLine
        SQL = SQL & "      , NULL prop_cliente_id                   " & vbNewLine
        SQL = SQL & "      , NULL produto_id                        " & vbNewLine
        SQL = SQL & "      , descr_situacao = 'N�o emitida'         " & vbNewLine
        SQL = SQL & "      , emi_re_proposta_tb.proposta_bb                            " & vbNewLine
        SQL = SQL & "      , NULL nome_arquivo                       " & vbNewLine
        'Rmarins - 05/01/2006 - Inclus�o do campo seg_cliente ''''''''''''''''''''''''''
        SQL = SQL & "      , ' ' seg_cliente                         " & vbNewLine
        SQL = SQL & "      , proposta_tb.Situacao " & vbNewLine    'MCAMARAL MELHORIAS
        SQL = SQL & "      , proposta_tb.produto_id    " & vbNewLine    'MCAMARAL MELHORIAS
        SQL = SQL & "      , pr.nome as nome_prod   " & vbNewLine    'MCAMARAL MELHORIAS
        SQL = SQL & "      , isnull(pr.processa_cobranca,'n') as Cobranca"    'MCAMARAL MELHORIAS
        SQL = SQL & "      , proposta_tb.origem_proposta_id" & vbNewLine        'MCAMARAL MELHORIAS
        SQL = SQL & "   FROM emi_re_proposta_tb WITH (NOLOCK)               " & vbNewLine
        SQL = SQL & "   join proposta_tb  WITH (NOLOCK)  "
        SQL = SQL & "     on proposta_tb.proposta_id = emi_re_proposta_tb.proposta_id   " & vbNewLine
        SQL = SQL & "   JOIN Produto_tb pr WITH(NOLOCK)"
        SQL = SQL & "     ON pr.produto_id = proposta_tb.produto_id "
        SQL = SQL & "  WHERE emi_re_proposta_tb.situacao = 'n'                          " & vbNewLine
        SQL = SQL & "    AND proponente LIKE '" & (txtArgumento) & "%'"
        SQL = SQL & "    AND (arquivo LIKE 'SEG491D%'                 " & vbNewLine
        SQL = SQL & "          OR arquivo LIKE 'SEG493%'               " & vbNewLine
        SQL = SQL & "          OR arquivo LIKE 'SEGF496%'              " & vbNewLine
        SQL = SQL & "          OR arquivo LIKE 'INTERNET%'             " & vbNewLine
        SQL = SQL & "          OR arquivo LIKE 'SEG486%'               " & vbNewLine
        SQL = SQL & "          OR arquivo LIKE 'SEG422%'               " & vbNewLine
        SQL = SQL & "          OR arquivo LIKE 'SEGF495%')             " & vbNewLine
        SQL = SQL & "    AND proposta_tb.situacao<>'t' " & vbNewLine
        'GENJUNIOR - FLOW 17860335 - CONSULTA TABELAS DE HIST�RICO
        SQL = SQL & "" & vbNewLine
        SQL = SQL & " UNION ALL" & vbNewLine

        SQL = SQL & " SELECT proponente nome " & vbNewLine
        SQL = SQL & "      , emi_proposta_hist_tb.proposta_id " & vbNewLine
        SQL = SQL & "      , emi_proposta_hist_tb.dt_contratacao " & vbNewLine
        SQL = SQL & "      , NULL prop_cliente_id " & vbNewLine
        SQL = SQL & "      , NULL produto_id " & vbNewLine
        SQL = SQL & "      , descr_situacao = 'N�o emitida' " & vbNewLine
        SQL = SQL & "      , emi_proposta_hist_tb.proposta_bb " & vbNewLine
        SQL = SQL & "      , NULL nome_arquivo " & vbNewLine
        SQL = SQL & "      , NULL seg_cliente " & vbNewLine
        SQL = SQL & "      , proposta_tb.Situacao " & vbNewLine
        SQL = SQL & "      , proposta_tb.produto_id " & vbNewLine
        SQL = SQL & "      , pr.nome as nome_prod " & vbNewLine
        SQL = SQL & "      , isnull(pr.processa_cobranca, 'n') as Cobranca"
        SQL = SQL & "      , proposta_tb.origem_proposta_id" & vbNewLine
        SQL = SQL & "   FROM seguros_hist_db.dbo.emi_proposta_hist_tb emi_proposta_hist_tb WITH (NOLOCK) " & vbNewLine
        SQL = SQL & "   join proposta_tb  WITH (NOLOCK) on proposta_tb.proposta_id = emi_proposta_hist_tb.proposta_id " & vbNewLine
        SQL = SQL & "   JOIN Produto_tb pr WITH(NOLOCK)"
        SQL = SQL & "     ON pr.produto_id = proposta_tb.produto_id "
        SQL = SQL & "  WHERE emi_proposta_hist_tb.situacao = 'n' " & vbNewLine
        SQL = SQL & "    AND proponente LIKE '" & (txtArgumento) & "%'"
        SQL = SQL & "    AND proposta_tb.situacao<>'t' " & vbNewLine
        SQL = SQL & " UNION ALL " & vbNewLine
        SQL = SQL & " SELECT proponente nome " & vbNewLine
        SQL = SQL & "      , emi_re_proposta_hist_tb.proposta_id " & vbNewLine
        SQL = SQL & "      , emi_re_proposta_hist_tb.dt_contratacao " & vbNewLine
        SQL = SQL & "      , NULL prop_cliente_id " & vbNewLine
        SQL = SQL & "      , NULL produto_id " & vbNewLine
        SQL = SQL & "      , descr_situacao = 'N�o emitida' " & vbNewLine
        SQL = SQL & "      , emi_re_proposta_hist_tb.proposta_bb " & vbNewLine
        SQL = SQL & "      , NULL nome_arquivo " & vbNewLine
        SQL = SQL & "      , ' ' seg_cliente " & vbNewLine
        SQL = SQL & "      , proposta_tb.Situacao " & vbNewLine
        SQL = SQL & "      , proposta_tb.produto_id " & vbNewLine
        SQL = SQL & "      , pr.nome as nome_prod " & vbNewLine
        SQL = SQL & "      , isnull(pr.processa_cobranca,'n') as Cobranca"
        SQL = SQL & "      , proposta_tb.origem_proposta_id" & vbNewLine
        SQL = SQL & "   FROM seguros_hist_db.dbo.emi_re_proposta_hist_tb emi_re_proposta_hist_tb WITH (NOLOCK) " & vbNewLine
        SQL = SQL & "   join proposta_tb  WITH (NOLOCK) "
        SQL = SQL & "     on proposta_tb.proposta_id = emi_re_proposta_hist_tb.proposta_id " & vbNewLine
        SQL = SQL & "   JOIN Produto_tb pr WITH(NOLOCK)"
        SQL = SQL & "     ON pr.produto_id = proposta_tb.produto_id "
        SQL = SQL & "  WHERE emi_re_proposta_hist_tb.situacao = 'n' " & vbNewLine
        SQL = SQL & "    AND proponente LIKE '" & (txtArgumento) & "%'"
        SQL = SQL & "    AND (arquivo LIKE 'SEG491D%' " & vbNewLine
        SQL = SQL & "          OR arquivo LIKE 'SEG493%' " & vbNewLine
        SQL = SQL & "          OR arquivo LIKE 'SEGF496%' " & vbNewLine
        SQL = SQL & "          OR arquivo LIKE 'INTERNET%' " & vbNewLine
        SQL = SQL & "          OR arquivo LIKE 'SEG486%' " & vbNewLine
        SQL = SQL & "          OR arquivo LIKE 'SEG422%' " & vbNewLine
        SQL = SQL & "          OR arquivo LIKE 'SEGF495%') " & vbNewLine
        SQL = SQL & "    AND proposta_tb.situacao<>'t' " & vbNewLine
        'FIM GENJUNIOR
        SQL = SQL & " ORDER BY nome  " & vbNewLine

        '
        DoEvents
        '
        '*** FIM NOME CLIENRE
    ElseIf cboTipoSelecao.Text = "N� da Ap�lice" Then
        If Not IsNumeric(txtArgumento.Text) Then
            mensagem_erro 3, "N� da Ap�lice"
            txtArgumento.SelStart = 0
            txtArgumento.SelLength = Len(txtArgumento)
            txtArgumento.SetFocus
            MousePointer = vbDefault
            Exit Sub
        End If
        '
        '**INICIO APOLICE
        If CmbSeguradora.ItemData(CmbSeguradora.ListIndex) <> 6785 Then
            SQL = ""
            SQL = SQL & " SELECT at.apolice_id" & vbNewLine
            SQL = SQL & "      , at.seguradora_cod_susep" & vbNewLine
            SQL = SQL & "      , at.sucursal_seguradora_id" & vbNewLine
            SQL = SQL & "      , at.ramo_id " & vbNewLine
            SQL = SQL & "   FROM co_seguro_aceito_tb cs  WITH (NOLOCK) " & vbNewLine
            SQL = SQL & "      , apolice_terceiros_tb at   WITH (NOLOCK) " & vbNewLine
            SQL = SQL & "  WHERE cs.seg_cod_susep_lider = " & CmbSeguradora.ItemData(CmbSeguradora.ListIndex) & vbNewLine
            SQL = SQL & "    AND  at.sucursal_seg_lider = cs.sucursal_seg_lider" & vbNewLine
            SQL = SQL & "    AND  cs.sucursal_seg_lider = " & CmbSucursal.ItemData(CmbSucursal.ListIndex) & vbNewLine
            SQL = SQL & "    AND  cs.ramo_lider = " & CmbRamo.ItemData(CmbRamo.ListIndex) & vbNewLine
            SQL = SQL & "    AND  cs.num_apolice_lider = " & txtArgumento & vbNewLine
            SQL = SQL & "    AND  at.seg_cod_susep_lider = cs.seg_cod_susep_lider " & vbNewLine
            SQL = SQL & "    AND  at.num_ordem_co_seguro_aceito = cs.num_ordem_co_seguro_aceito "
            Set rc = rdocn.OpenResultset(SQL)

            If Not rc.EOF Then
                apolice_pesq = rc!apolice_id
                seguradora_pesq = rc!seguradora_cod_susep
                sucursal_pesq = rc!sucursal_seguradora_id
                ramo_pesq = rc!ramo_id
            Else
                apolice_pesq = 0
                seguradora_pesq = 0
                sucursal_pesq = 0
                ramo_pesq = 0
            End If
            CosseguroAceito = True
        Else
            'AFONSO DUTRA - DATA: 09/03/2010 - FLOW: 3206098
            apolice_pesq = Trim(txtArgumento)
            'Fim
            seguradora_pesq = CmbSeguradora.ItemData(CmbSeguradora.ListIndex)
            sucursal_pesq = CmbSucursal.ItemData(CmbSucursal.ListIndex)
            ramo_pesq = CmbRamo.ItemData(CmbRamo.ListIndex)
            CosseguroAceito = False
        End If

        Set rc = Nothing

        SQL = "         SELECT a.apolice_id"
        SQL = SQL & "        , a.ramo_id"
        SQL = SQL & "        , r.nome nm_ramo"
        SQL = SQL & "        , a.seguradora_cod_susep"
        SQL = SQL & "        , a.sucursal_seguradora_id " & vbNewLine
        SQL = SQL & "        , pf.proposta_bb"
        SQL = SQL & "        , p.proposta_id"
        SQL = SQL & "        , p.produto_id"
        SQL = SQL & "        , e.endosso_id " & vbNewLine
        'inicio mcamara]
        SQL = SQL & "        , p.situacao      " & vbNewLine
        SQL = SQL & "        , pr.nome  AS nome_prod  " & vbNewLine
        SQL = SQL & "        , cobranca = isnull(pr.processa_cobranca,'n') "
        SQL = SQL & "        , p.origem_proposta_id" & vbNewLine
        'fim
        SQL = SQL & "        , e.dt_pedido_endosso"
        SQL = SQL & "        , e.num_endosso_bb"
        SQL = SQL & "        , e.tp_endosso_id " & vbNewLine
        SQL = SQL & "        , c.nome"
        SQL = SQL & "        , p.dt_contratacao"
        SQL = SQL & "        , p.prop_cliente_id " & vbNewLine
        SQL = SQL & "        , descr_situacao = case p.situacao " & vbNewLine
        SQL = SQL & "                           when 'e' then 'Em Estudo' " & vbNewLine
        SQL = SQL & "                           when 'a' then 'Aceite' " & vbNewLine
        SQL = SQL & "                           when 'c' then 'Cancelada' " & vbNewLine
        SQL = SQL & "                           when 'r' then 'Recusada' " & vbNewLine
        'SQL = SQL & "     when 'i' then 'Ap�lice Emitida' "
        'bcarneiro - 05/11/2003 - Vida Vida - Novas Funcionalidades ''''''''''''''''''''''''
        SQL = SQL & "                           when 'i' then " & vbNewLine
        SQL = SQL & "                                    case (select count(1) " & vbNewLine    'alterado count(*)para count(1)12/09/2013 projeto 17860335 - Melhorias no SEGBR MC_Amaral -confitec
        SQL = SQL & "                                            from proposta_suspensa_tb s WITH(NOLOCK) " & vbNewLine
        SQL = SQL & "                                           where p.proposta_id = s.proposta_id " & vbNewLine
        'SQL = SQL & "                and s.dt_inicio_suspensao >= '" & Format(Data_Sistema, "yyyymmdd") & "'"
        SQL = SQL & "                                               and s.dt_inicio_suspensao <= '" & Format(Data_Sistema, "yyyymmdd") & "'" & vbNewLine
        SQL = SQL & "                                               and (s.dt_fim_suspensao is null " & vbNewLine
        SQL = SQL & "                                                or (s.dt_fim_suspensao is not null " & vbNewLine
        SQL = SQL & "                                               and  s.cod_suspensao = '1'))) " & vbNewLine
        SQL = SQL & "                                   when 0 then 'Ap�lice Emitida' " & vbNewLine
        'Alessandra Grig�rio - 25/06/2004 - Atendendo solic. Marcio Yoshimura
        'SQL = SQL & "             else 'Suspensa'        "
        SQL = SQL & "                                   else 'Cancelada'       " & vbNewLine
        'FIM
        SQL = SQL & "                                   end " & vbNewLine
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        SQL = SQL & "                          when 'p' then 'Em Estudo (erro de cadastramento)' " & vbNewLine
        SQL = SQL & "                          when 's' then 'Carta Recusa Emitida' " & vbNewLine
        SQL = SQL & "                           end " & vbNewLine
        SQL = SQL & "        , a.tp_emissao"
        SQL = SQL & "        , NULL nome_arquivo " & vbNewLine

        'Rmarins - 05/01/2006 - Inclus�o do campo seg_cliente '''''''''''''''''''''''''''''''
        SQL = SQL & "        , seg_cliente = CASE c.seg_cliente    " & vbNewLine
        SQL = SQL & "                        WHEN 'private' THEN 'PRIVATE'  " & vbNewLine
        SQL = SQL & "                        WHEN 'estilo' THEN 'ESTILO'  " & vbNewLine
        SQL = SQL & "                        ELSE ' '   " & vbNewLine
        SQL = SQL & "                         END  " & vbNewLine
        'ANTES
        '            SQL = SQL & " FROM proposta_tb p  WITH (NOLOCK) , apolice_tb a  WITH (NOLOCK) , endosso_tb e  WITH (NOLOCK)  " & vbNewLine
        '            SQL = SQL & ",    proposta_fechada_tb pf  WITH (NOLOCK) , cliente_tb c  WITH (NOLOCK) , ramo_tb r   WITH (NOLOCK)  " & vbNewLine
        'DEPOIS

        SQL = SQL & "    FROM proposta_tb p  WITH (NOLOCK) " & vbNewLine
        SQL = SQL & "    JOIN apolice_tb a  WITH (NOLOCK)" & vbNewLine
        SQL = SQL & "      ON p.proposta_id = a.proposta_id " & vbNewLine
        SQL = SQL & "    left JOIN endosso_tb e  WITH (NOLOCK)  " & vbNewLine
        SQL = SQL & "      ON e.proposta_id = p.proposta_id " & vbNewLine
        SQL = SQL & "     AND e.tp_endosso_id not in (10,100,101,200) " & vbNewLine
        SQL = SQL & "    JOIN proposta_fechada_tb pf  WITH (NOLOCK) "
        SQL = SQL & "      ON pf.proposta_id = p.proposta_id " & vbNewLine
        SQL = SQL & "    JOIN cliente_tb c  WITH (NOLOCK) "
        SQL = SQL & "      ON c.cliente_id = p.prop_cliente_id " & vbNewLine
        SQL = SQL & "    JOIN ramo_tb r   WITH (NOLOCK)  " & vbNewLine
        SQL = SQL & "      ON r.ramo_id = a.ramo_id " & vbNewLine
        'inicio mcamaral
        SQL = SQL & "    JOIN Produto_tb pr WITH(NOLOCK)" & vbNewLine
        SQL = SQL & "      ON p.produto_id = pr.produto_id " & vbNewLine
        'fim  MCAMARAL -INCUS�O PARA PEGAR DADOS DO PRODUTO
        SQL = SQL & "   WHERE a.apolice_id = " & apolice_pesq & vbNewLine
        SQL = SQL & "     AND a.seguradora_cod_susep = " & seguradora_pesq & vbNewLine
        SQL = SQL & "     AND a.sucursal_seguradora_id = " & sucursal_pesq & vbNewLine
        SQL = SQL & "     AND a.ramo_id = " & ramo_pesq & vbNewLine
        'SQL = SQL & " AND    e.proposta_id =* p.proposta_id " & vbNewLine
        '' N�o inclui os endossos financeiros

        SQL = SQL & "     AND p.produto_id in (" & ProdutosValidos & ")" & vbNewLine
        SQL = SQL & "      AND p.situacao <>'t' " & vbNewLine
        'SQL = SQL & " ORDER BY p.dt_contratacao "
        'SQL = SQL & " ORDER BY e.dt_pedido_endosso " 'Alterado por fpimenta / Imar�s - 05/06/2003 - SAS 027-03
        'AFONSO DUTRA - FLOW: 3206098
        'Raimundo Belas da Silva Junior - GPTI - 13/04/2010
        'Acertando ORDER BY
        '-------------------------------------------------------------------------
        'SQL = SQL & " ORDER BY e.dt_pedido_endosso, /*e.dt_emissao,*/ p.dt_contratacao, e.endosso_id "
        'Denis Silva  - CWI - 26/05/2010
        'Acertando ORDER BY - FLOW:3727253
        SQL = SQL & " ORDER BY  e.dt_emissao, /*e.dt_pedido_endosso,*/ p.dt_contratacao, e.endosso_id "


        '**FIM APOLICE

    ElseIf cboTipoSelecao.Text = "N� de Ordem" Then
        If Not IsNumeric(txtArgumento.Text) Then
            mensagem_erro 3, "N� de Ordem"
            txtArgumento.SelStart = 0
            txtArgumento.SelLength = Len(txtArgumento)
            txtArgumento.SetFocus
            MousePointer = vbDefault
            Exit Sub
        End If
        '**INICIO ORDEM
        SQL = "             SELECT at.apolice_id"
        SQL = SQL & "            , at.ramo_id"
        SQL = SQL & "            , r.nome nm_ramo"
        SQL = SQL & "            , at.seguradora_cod_susep"
        SQL = SQL & "            , at.sucursal_seguradora_id " & vbNewLine
        SQL = SQL & "            , p.Situacao " & vbNewLine
        SQL = SQL & "            , pr.produto_id    " & vbNewLine
        SQL = SQL & "            , pr.nome  AS nome_prod  " & vbNewLine
        SQL = SQL & "            , cobranca = isnull(pr.processa_cobranca,'n') "
        SQL = SQL & "            , p.origem_proposta_id" & vbNewLine
        SQL = SQL & "            , pf.proposta_bb"
        SQL = SQL & "            , p.proposta_id"
        SQL = SQL & "            , p.produto_id"
        SQL = SQL & "            , e.endosso_id " & vbNewLine
        SQL = SQL & "            , e.dt_pedido_endosso"
        SQL = SQL & "            , e.num_endosso_bb"
        SQL = SQL & "            , e.tp_endosso_id " & vbNewLine
        SQL = SQL & "            , c.nome"
        SQL = SQL & "            , p.dt_contratacao"
        SQL = SQL & "            , p.prop_cliente_id " & vbNewLine
        'sql = sql & ", ca.num_apolice_lider, ca.num_ordem_co_seguro_aceito "
        'sql = sql & ", ca.dt_emissa_apolice_lider "
        SQL = SQL & "            , descr_situacao = case p.situacao " & vbNewLine
        SQL = SQL & "                               when 'e' then 'Em Estudo' " & vbNewLine
        SQL = SQL & "                               when 'a' then 'Aceite' " & vbNewLine
        SQL = SQL & "                               when 'c' then 'Cancelada' " & vbNewLine
        SQL = SQL & "                               when 'r' then 'Recusada' " & vbNewLine
        'SQL = SQL & "     when 'i' then 'Ap�lice Emitida' "
        'bcarneiro - 05/11/2003 - Vida Vida - Novas Funcionalidades '''''''''''''''''''''''''''
        SQL = SQL & "                               when 'i' then " & vbNewLine
        SQL = SQL & "                                        case (select count(1)" & vbNewLine
        SQL = SQL & "                                                from proposta_suspensa_tb s WITH(NOLOCK)" & vbNewLine
        SQL = SQL & "                                               where p.proposta_id = s.proposta_id " & vbNewLine
        'SQL = SQL & "                and s.dt_inicio_suspensao >= '" & Format(Data_Sistema, "yyyymmdd") & "'"
        SQL = SQL & "                                                 and s.dt_inicio_suspensao <= '" & Format(Data_Sistema, "yyyymmdd") & "'" & vbNewLine
        SQL = SQL & "                                                 and (s.dt_fim_suspensao is null " & vbNewLine
        SQL = SQL & "                                                  or (s.dt_fim_suspensao is not null " & vbNewLine
        SQL = SQL & "                                                 and  s.cod_suspensao = '1')))" & vbNewLine
        SQL = SQL & "                                       when 0 then 'Ap�lice Emitida' " & vbNewLine
        'Alessandra Grig�rio - 25/06/2004 - Atendendo solic. Marcio Yoshimura
        'SQL = SQL & "             else 'Suspensa'        "
        SQL = SQL & "                                       else 'Cancelada'       " & vbNewLine
        'FIM
        SQL = SQL & "                                        end " & vbNewLine
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        SQL = SQL & "                               when 'p' then 'Em Estudo (erro de cadastramento)' " & vbNewLine
        SQL = SQL & "                               when 's' then 'Carta Recusa Emitida' " & vbNewLine
        SQL = SQL & "                                end " & vbNewLine
        SQL = SQL & "           , a.tp_emissao, NULL nome_arquivo " & vbNewLine
        'Rmarins - 05/01/2006 - Inclus�o do campo seg_cliente '''''''''''''''''''''''''''''''
        SQL = SQL & "           , seg_cliente = CASE c.seg_cliente      " & vbNewLine
        SQL = SQL & "                           WHEN 'private' THEN 'PRIVATE'   " & vbNewLine
        SQL = SQL & "                           WHEN 'estilo' THEN 'ESTILO'     " & vbNewLine
        SQL = SQL & "                           ELSE ' '  " & vbNewLine
        SQL = SQL & "                            END  " & vbNewLine
        ' retirada  =* maria
        '        SQL = SQL & "FROM co_seguro_aceito_tb ca  WITH (NOLOCK) , apolice_terceiros_tb at  WITH (NOLOCK) , apolice_tb a  WITH (NOLOCK)  " & vbNewLine
        '        SQL = SQL & ",    proposta_tb p  WITH (NOLOCK) , proposta_fechada_tb pf  WITH (NOLOCK) , endosso_tb e  WITH (NOLOCK) , cliente_tb c  WITH (NOLOCK) , ramo_tb r   WITH (NOLOCK)  " & vbNewLine
        SQL = SQL & "        FROM co_seguro_aceito_tb ca  WITH (NOLOCK)" & vbNewLine
        SQL = SQL & "        JOIN apolice_terceiros_tb at  WITH (NOLOCK)" & vbNewLine
        SQL = SQL & "          ON  at.num_ordem_co_seguro_aceito = ca.num_ordem_co_seguro_aceito " & vbNewLine
        SQL = SQL & "         AND  at.seg_cod_susep_lider =       ca.seg_cod_susep_lider " & vbNewLine
        SQL = SQL & "         AND  at.sucursal_seg_lider =        ca.sucursal_seg_lider " & vbNewLine
        SQL = SQL & "        JOIN apolice_tb a  WITH (NOLOCK)  " & vbNewLine
        SQL = SQL & "          ON a.sucursal_seguradora_id = at.sucursal_seguradora_id " & vbNewLine
        SQL = SQL & "         AND a.apolice_id = at.apolice_id " & vbNewLine
        SQL = SQL & "         AND a.seguradora_cod_susep = at.seguradora_cod_susep " & vbNewLine
        SQL = SQL & "         AND a.ramo_id = at.ramo_id " & vbNewLine
        SQL = SQL & "        JOIN proposta_tb p  WITH (NOLOCK)" & vbNewLine
        SQL = SQL & "          ON p.proposta_id = a.proposta_id " & vbNewLine
        SQL = SQL & "        JOIN proposta_fechada_tb pf  WITH (NOLOCK)" & vbNewLine
        SQL = SQL & "          ON pf.proposta_id = p.proposta_id " & vbNewLine
        SQL = SQL & "   LEFT JOIN endosso_tb e  WITH (NOLOCK)" & vbNewLine
        SQL = SQL & "          ON e.proposta_id = p.proposta_id " & vbNewLine
        SQL = SQL & "         AND e.tp_endosso_id not in (10,100,101,200) " & vbNewLine
        SQL = SQL & "        JOIN cliente_tb c  WITH (NOLOCK)" & vbNewLine
        SQL = SQL & "          ON c.cliente_id = p.prop_cliente_id " & vbNewLine
        SQL = SQL & "        JOIN ramo_tb r   WITH (NOLOCK)  " & vbNewLine
        SQL = SQL & "          ON  r.ramo_id = a.ramo_id " & vbNewLine
        SQL = SQL & "        JOIN Produto_tb pr   WITH (NOLOCK)  " & vbNewLine
        SQL = SQL & "          ON p.produto_id  = pr.produto_id " & vbNewLine
        SQL = SQL & "       WHERE ca.num_ordem_co_seguro_aceito = " & txtArgumento & vbNewLine
        SQL = SQL & "         AND ca.seg_cod_susep_lider = " & CmbSeguradora.ItemData(CmbSeguradora.ListIndex)
        SQL = SQL & "         AND ca.sucursal_seg_lider = " & CmbSucursal.ItemData(CmbSucursal.ListIndex)
        ' retirada  =* maria
        '        SQL = SQL & " AND at.num_ordem_co_seguro_aceito = ca.num_ordem_co_seguro_aceito " & vbNewLine
        '        SQL = SQL & " AND at.seg_cod_susep_lider = ca.seg_cod_susep_lider " & vbNewLine
        '        SQL = SQL & " AND at.sucursal_seg_lider = ca.sucursal_seg_lider " & vbNewLine
        '        SQL = SQL & " AND a.sucursal_seguradora_id = at.sucursal_seguradora_id " & vbNewLine
        '        SQL = SQL & " AND a.apolice_id = at.apolice_id " & vbNewLine
        '        SQL = SQL & " AND a.seguradora_cod_susep = at.seguradora_cod_susep " & vbNewLine
        '        SQL = SQL & " AND a.ramo_id = at.ramo_id " & vbNewLine
        '        SQL = SQL & " AND p.proposta_id = a.proposta_id " & vbNewLine
        '        SQL = SQL & " AND pf.proposta_id = p.proposta_id " & vbNewLine
        '        SQL = SQL & " AND e.proposta_id =* p.proposta_id " & vbNewLine
        '' N�o inclui os endossos financeiros
        '        SQL = SQL & " AND e.tp_endosso_id not in (10,100,101,200) " & vbNewLine
        '        SQL = SQL & " AND c.cliente_id = p.prop_cliente_id " & vbNewLine
        '        SQL = SQL & " AND r.ramo_id = a.ramo_id " & vbNewLine
        SQL = SQL & "       AND p.produto_id in (" & ProdutosValidos & ")" & vbNewLine
        SQL = SQL & "       AND p.situacao <>'t' " & vbNewLine
        'SQL = SQL & " ORDER BY p.dt_contratacao "
        'SQL = SQL & " ORDER BY e.dt_pedido_endosso " 'Alterado por fpimenta / Imar�s - 05/06/2003 - SAS 027-03
        'AFONSO
        'Raimundo Belas da Silva Junior - GPTI - 13/04/2010
        'Acertando ORDER BY
        SQL = SQL & " ORDER BY e.dt_pedido_endosso/*, e.dt_emissao*/"




        '****FIM ORDEM

        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        'carolina.marinho 24/10/2011 - Demanda 11938655
        'Criar ferramenta para identificar exist�ncia de subgrupo (rp) - Confitec RJ
        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

    ElseIf cboTipoSelecao.Text = "CNPJ" Then
        If Not IsNumeric(DatCGC(mskCNPJ.Text)) Then
            mensagem_erro 3, "CNPJ"
            mskCNPJ.SetFocus
            MousePointer = vbDefault
            Exit Sub
        End If

        '***INICIO CNPJ
        SQL = ""
        SQL = SQL & " SELECT cliente_tb.cpf_cnpj" & vbNewLine
        SQL = SQL & "      , sub_grupo_apolice_tb.nome" & vbNewLine
        SQL = SQL & "      , proposta_tb.ramo_id" & vbNewLine
        SQL = SQL & "      , sub_grupo_apolice_tb.apolice_id" & vbNewLine
        SQL = SQL & "      , sub_grupo_apolice_tb.sub_grupo_id " & vbNewLine
        SQL = SQL & "      , NULL proposta_bb" & vbNewLine
        SQL = SQL & "      , NULL nome_arquivo" & vbNewLine
        SQL = SQL & "      , proposta_tb.proposta_id" & vbNewLine
        SQL = SQL & "      , proposta_tb.dt_contratacao" & vbNewLine
        SQL = SQL & "      , proposta_tb.prop_cliente_id" & vbNewLine
        SQL = SQL & "      , descr_situacao = case proposta_tb.situacao " & vbNewLine
        SQL = SQL & "                         when 'e' then 'Em Estudo'  " & vbNewLine
        SQL = SQL & "                         when 'a' then 'Aceite' " & vbNewLine
        SQL = SQL & "                         when 'c' then 'Cancelada' " & vbNewLine
        SQL = SQL & "                         when 'r' then 'Recusada' " & vbNewLine
        SQL = SQL & "                         when 'i' then " & vbNewLine
        SQL = SQL & "                                  case (select count(1)" & vbNewLine
        SQL = SQL & "                                          from proposta_suspensa_tb s WITH(NOLOCK)" & vbNewLine
        SQL = SQL & "                                         where proposta_tb.proposta_id = s.proposta_id" & vbNewLine
        SQL = SQL & "                                           and s.dt_inicio_suspensao <= '20111024'" & vbNewLine
        SQL = SQL & "                                           and (s.dt_fim_suspensao is null" & vbNewLine
        SQL = SQL & "                                            or (s.dt_fim_suspensao is not null" & vbNewLine
        SQL = SQL & "                                           and  s.cod_suspensao = '1')))" & vbNewLine
        SQL = SQL & "                                  when 0 then 'Ap�lice Emitida'" & vbNewLine
        SQL = SQL & "                                  else 'Cancelada'        " & vbNewLine
        SQL = SQL & "                                  end " & vbNewLine
        SQL = SQL & "                         when 'p' then 'Em Estudo (erro de cadastramento)' " & vbNewLine
        SQL = SQL & "                         when 's' then 'Carta Recusa Emitida' " & vbNewLine
        SQL = SQL & "                       end " & vbNewLine
        SQL = SQL & "      , seg_cliente = CASE cliente_tb.seg_cliente " & vbNewLine
        SQL = SQL & "                      WHEN 'private' THEN 'PRIVATE'  " & vbNewLine
        SQL = SQL & "                      WHEN 'estilo' THEN 'ESTILO' " & vbNewLine
        SQL = SQL & "                      ELSE ' ' " & vbNewLine
        SQL = SQL & "                      END " & vbNewLine
        SQL = SQL & "      , endosso_id" & vbNewLine
        SQL = SQL & "      , nm_ramo = ramo_tb.nome" & vbNewLine
        SQL = SQL & "      , proposta_tb.produto_id" & vbNewLine
        SQL = SQL & "      , dt_pedido_endosso" & vbNewLine
        SQL = SQL & "      , num_endosso_bb" & vbNewLine
        SQL = SQL & "      , tp_endosso_id" & vbNewLine
        SQL = SQL & "      , proposta_tb.situacao      " & vbNewLine
        SQL = SQL & "      , pr.produto_id    " & vbNewLine
        SQL = SQL & "      , pr.nome as nome_prod   " & vbNewLine
        SQL = SQL & "      , isnull(pr.processa_cobranca,'n')as cobranca " & vbNewLine
        SQL = SQL & "      , proposta_tb.origem_proposta_id" & vbNewLine
        SQL = SQL & "  FROM proposta_tb  WITH (NOLOCK) " & vbNewLine
        SQL = SQL & "  join proposta_fechada_tb  WITH (NOLOCK) " & vbNewLine
        SQL = SQL & "    on proposta_fechada_tb.proposta_id = proposta_tb.proposta_id" & vbNewLine
        SQL = SQL & "  JOIN cliente_tb  WITH (NOLOCK) " & vbNewLine
        SQL = SQL & "    ON proposta_tb.prop_cliente_id = cliente_tb.cliente_id " & vbNewLine
        SQL = SQL & "  JOIN Produto_tb pr WITH(NOLOCK)"
        SQL = SQL & "    ON proposta_tb.produto_id = pr.produto_id "
        SQL = SQL & "  JOIN apolice_tb  WITH (NOLOCK) " & vbNewLine
        SQL = SQL & "    ON proposta_tb.proposta_id = apolice_tb.proposta_id" & vbNewLine
        SQL = SQL & "  JOIN sub_grupo_apolice_tb  WITH (NOLOCK) " & vbNewLine
        SQL = SQL & "    ON apolice_tb.apolice_id = sub_grupo_apolice_tb.apolice_id" & vbNewLine
        SQL = SQL & "   AND apolice_tb.sucursal_seguradora_id = sub_grupo_apolice_tb.sucursal_seguradora_id" & vbNewLine
        SQL = SQL & "   AND apolice_tb.seguradora_cod_susep = sub_grupo_apolice_tb.seguradora_cod_susep" & vbNewLine
        SQL = SQL & "   AND apolice_tb.ramo_id = sub_grupo_apolice_tb.ramo_id" & vbNewLine
        SQL = SQL & "  LEFT JOIN endosso_tb  WITH (NOLOCK) " & vbNewLine
        SQL = SQL & "    ON proposta_tb.proposta_id = endosso_tb.proposta_id" & vbNewLine
        SQL = SQL & "  JOIN ramo_tb  WITH (NOLOCK) " & vbNewLine
        SQL = SQL & "    ON proposta_tb.ramo_id = ramo_tb.ramo_id" & vbNewLine
        SQL = SQL & " WHERE cliente_tb.cpf_cnpj = '" & DatCGC(mskCNPJ.Text) & "'" & vbNewLine
        SQL = SQL & "   AND proposta_tb.produto_id in (" & ProdutosValidos & ")" & vbNewLine
        SQL = SQL & "   AND proposta_tb.situacao<>'t' " & vbNewLine
        SQL = SQL & "   AND tp_endosso_id not in (10,100,101,200)" & vbNewLine

        SQL = SQL & "UNION" & vbNewLine

        SQL = SQL & " SELECT cliente_tb.cpf_cnpj" & vbNewLine
        SQL = SQL & "      , sub_grupo_apolice_tb.nome" & vbNewLine
        SQL = SQL & "      , proposta_tb.ramo_id" & vbNewLine
        SQL = SQL & "      , sub_grupo_apolice_tb.apolice_id" & vbNewLine
        SQL = SQL & "      , sub_grupo_apolice_tb.sub_grupo_id " & vbNewLine
        SQL = SQL & "      , NULL proposta_bb" & vbNewLine
        SQL = SQL & "      , NULL nome_arquivo" & vbNewLine
        SQL = SQL & "      , proposta_tb.proposta_id" & vbNewLine
        SQL = SQL & "      , proposta_tb.dt_contratacao" & vbNewLine
        SQL = SQL & "      , proposta_tb.prop_cliente_id" & vbNewLine
        SQL = SQL & "      , descr_situacao = case proposta_tb.situacao " & vbNewLine
        SQL = SQL & "                         when 'e' then 'Em Estudo'  " & vbNewLine
        SQL = SQL & "                         when 'a' then 'Aceite' " & vbNewLine
        SQL = SQL & "                         when 'c' then 'Cancelada' " & vbNewLine
        SQL = SQL & "                         when 'r' then 'Recusada' " & vbNewLine
        SQL = SQL & "                         when 'i' then " & vbNewLine
        SQL = SQL & "                                  case (select count(1)" & vbNewLine
        SQL = SQL & "                                          from proposta_suspensa_tb s WITH(NOLOCK)" & vbNewLine
        SQL = SQL & "                                         where proposta_tb.proposta_id = s.proposta_id" & vbNewLine
        SQL = SQL & "                                           and s.dt_inicio_suspensao <= '20111024'" & vbNewLine
        SQL = SQL & "                                           and (s.dt_fim_suspensao is null" & vbNewLine
        SQL = SQL & "                                            or (s.dt_fim_suspensao is not null" & vbNewLine
        SQL = SQL & "                                           and  s.cod_suspensao = '1')))" & vbNewLine
        SQL = SQL & "                                  when 0 then 'Ap�lice Emitida'" & vbNewLine
        SQL = SQL & "                                  else 'Cancelada'        " & vbNewLine
        SQL = SQL & "                                  end " & vbNewLine
        SQL = SQL & "                         when 'p' then 'Em Estudo (erro de cadastramento)' " & vbNewLine
        SQL = SQL & "                         when 's' then 'Carta Recusa Emitida' " & vbNewLine
        SQL = SQL & "                       end " & vbNewLine
        SQL = SQL & "      , seg_cliente = CASE cliente_tb.seg_cliente " & vbNewLine
        SQL = SQL & "                      WHEN 'private' THEN 'PRIVATE'  " & vbNewLine
        SQL = SQL & "                      WHEN 'estilo' THEN 'ESTILO' " & vbNewLine
        SQL = SQL & "                      ELSE ' ' " & vbNewLine
        SQL = SQL & "                      END " & vbNewLine
        SQL = SQL & "      , endosso_id" & vbNewLine
        SQL = SQL & "      , nm_ramo = ramo_tb.nome" & vbNewLine
        SQL = SQL & "      , proposta_tb.produto_id" & vbNewLine
        SQL = SQL & "      , dt_pedido_endosso" & vbNewLine
        SQL = SQL & "      , num_endosso_bb" & vbNewLine
        SQL = SQL & "      , tp_endosso_id" & vbNewLine
        SQL = SQL & "      , proposta_tb.situacao      " & vbNewLine
        SQL = SQL & "      , pr.produto_id    " & vbNewLine
        SQL = SQL & "      , pr.nome as nome_prod   " & vbNewLine
        SQL = SQL & "      , isnull(pr.processa_cobranca,'n')as cobranca " & vbNewLine
        SQL = SQL & "      , proposta_tb.origem_proposta_id" & vbNewLine
        SQL = SQL & "  FROM proposta_tb  WITH (NOLOCK) " & vbNewLine
        SQL = SQL & "  JOIN Produto_tb pr WITH(NOLOCK)" & vbNewLine
        SQL = SQL & "    ON proposta_tb.produto_id = pr.produto_id " & vbNewLine
        SQL = SQL & "  JOIN proposta_adesao_tb  WITH (NOLOCK) " & vbNewLine
        SQL = SQL & "    ON proposta_adesao_tb.proposta_id = proposta_tb.proposta_id" & vbNewLine
        SQL = SQL & "  JOIN cliente_tb  WITH (NOLOCK) " & vbNewLine
        SQL = SQL & "    ON proposta_tb.prop_cliente_id = cliente_tb.cliente_id " & vbNewLine
        SQL = SQL & "  JOIN sub_grupo_apolice_tb  WITH (NOLOCK) " & vbNewLine
        SQL = SQL & "    ON sub_grupo_apolice_tb.apolice_id             = proposta_adesao_tb.apolice_id" & vbNewLine
        SQL = SQL & "   AND sub_grupo_apolice_tb.sucursal_seguradora_id = proposta_adesao_tb.sucursal_seguradora_id" & vbNewLine
        SQL = SQL & "   AND sub_grupo_apolice_tb.seguradora_cod_susep   = proposta_adesao_tb.seguradora_cod_susep" & vbNewLine
        SQL = SQL & "   AND sub_grupo_apolice_tb.ramo_id                = proposta_adesao_tb.ramo_id" & vbNewLine
        SQL = SQL & "  LEFT JOIN endosso_tb  WITH (NOLOCK) " & vbNewLine
        SQL = SQL & "    ON proposta_tb.proposta_id = endosso_tb.proposta_id" & vbNewLine
        SQL = SQL & "  JOIN ramo_tb  WITH (NOLOCK) " & vbNewLine
        SQL = SQL & "    ON proposta_tb.ramo_id = ramo_tb.ramo_id" & vbNewLine
        SQL = SQL & " WHERE cliente_tb.cpf_cnpj = '" & DatCGC(mskCNPJ.Text) & "'" & vbNewLine
        SQL = SQL & "   AND proposta_tb.produto_id in (" & ProdutosValidos & ")" & vbNewLine
        SQL = SQL & "   AND proposta_tb.situacao<>'t' " & vbNewLine
        SQL = SQL & "   AND tp_endosso_id not in (10,100,101,200)" & vbNewLine
        SQL = SQL & " ORDER BY cliente_tb.cpf_cnpj" & vbNewLine
        SQL = SQL & "        , sub_grupo_apolice_tb.nome" & vbNewLine
        SQL = SQL & "        , sub_grupo_apolice_tb.sub_grupo_id" & vbNewLine

        'FIM CNPJ

    ElseIf cboTipoSelecao.Text = "Raz�o Social" Then
        If Len(txtRazaoSocial.Text) = 0 Then
            mensagem_erro 3, "Raz�o Social"
            txtRazaoSocial.SetFocus
            MousePointer = vbDefault
            Exit Sub
        End If

        '***INICIO RAZ�O SOCIAL
        SQL = ""
        SQL = SQL & " SELECT cliente_tb.cpf_cnpj" & vbNewLine
        SQL = SQL & "      , sub_grupo_apolice_tb.nome" & vbNewLine
        SQL = SQL & "      , proposta_tb.ramo_id" & vbNewLine
        SQL = SQL & "      , sub_grupo_apolice_tb.apolice_id" & vbNewLine
        SQL = SQL & "      , sub_grupo_apolice_tb.sub_grupo_id " & vbNewLine
        SQL = SQL & "      , NULL proposta_bb" & vbNewLine
        SQL = SQL & "      , NULL nome_arquivo" & vbNewLine
        SQL = SQL & "      , proposta_tb.proposta_id" & vbNewLine
        SQL = SQL & "      , proposta_tb.dt_contratacao" & vbNewLine
        SQL = SQL & "      , proposta_tb.prop_cliente_id" & vbNewLine
        SQL = SQL & "      , descr_situacao = case proposta_tb.situacao " & vbNewLine
        SQL = SQL & "                         when 'e' then 'Em Estudo'  " & vbNewLine
        SQL = SQL & "                         when 'a' then 'Aceite' " & vbNewLine
        SQL = SQL & "                         when 'c' then 'Cancelada' " & vbNewLine
        SQL = SQL & "                         when 'r' then 'Recusada' " & vbNewLine
        SQL = SQL & "                         when 'i' then " & vbNewLine
        SQL = SQL & "                                  case (select count(1)" & vbNewLine
        SQL = SQL & "                                          from proposta_suspensa_tb s WITH(NOLOCK)" & vbNewLine
        SQL = SQL & "                                         where proposta_tb.proposta_id = s.proposta_id" & vbNewLine
        SQL = SQL & "                                           and s.dt_inicio_suspensao <= '20111024'" & vbNewLine
        SQL = SQL & "                                           and (s.dt_fim_suspensao is null" & vbNewLine
        SQL = SQL & "                                            or (s.dt_fim_suspensao is not null" & vbNewLine
        SQL = SQL & "                                           and  s.cod_suspensao = '1')))" & vbNewLine
        SQL = SQL & "                                  when 0 then 'Ap�lice Emitida'" & vbNewLine
        SQL = SQL & "                                  else 'Cancelada'        " & vbNewLine
        SQL = SQL & "                                  end " & vbNewLine
        SQL = SQL & "                         when 'p' then 'Em Estudo (erro de cadastramento)' " & vbNewLine
        SQL = SQL & "                         when 's' then 'Carta Recusa Emitida' " & vbNewLine
        SQL = SQL & "                       end " & vbNewLine
        SQL = SQL & "      , seg_cliente = CASE cliente_tb.seg_cliente " & vbNewLine
        SQL = SQL & "                      WHEN 'private' THEN 'PRIVATE'  " & vbNewLine
        SQL = SQL & "                      WHEN 'estilo' THEN 'ESTILO' " & vbNewLine
        SQL = SQL & "                      ELSE ' ' " & vbNewLine
        SQL = SQL & "                      END " & vbNewLine
        SQL = SQL & "      , endosso_id" & vbNewLine
        SQL = SQL & "      , nm_ramo = ramo_tb.nome" & vbNewLine
        SQL = SQL & "      , proposta_tb.produto_id" & vbNewLine
        SQL = SQL & "      , dt_pedido_endosso" & vbNewLine
        SQL = SQL & "      , num_endosso_bb" & vbNewLine
        SQL = SQL & "      , tp_endosso_id" & vbNewLine

        SQL = SQL & "      , proposta_tb.situacao      " & vbNewLine
        SQL = SQL & "      , pr.produto_id    " & vbNewLine
        SQL = SQL & "      , pr.nome as nome_prod   " & vbNewLine
        SQL = SQL & "      , isnull(pr.processa_cobranca,'n')as cobranca " & vbNewLine
        SQL = SQL & "      , proposta_tb.origem_proposta_id" & vbNewLine

        SQL = SQL & "  FROM proposta_tb  WITH (NOLOCK) " & vbNewLine
        SQL = SQL & "  join proposta_fechada_tb  WITH (NOLOCK) " & vbNewLine
        SQL = SQL & "    on proposta_fechada_tb.proposta_id = proposta_tb.proposta_id" & vbNewLine
        SQL = SQL & "  JOIN cliente_tb  WITH (NOLOCK) " & vbNewLine
        SQL = SQL & "    ON proposta_tb.prop_cliente_id = cliente_tb.cliente_id " & vbNewLine
        SQL = SQL & "  JOIN apolice_tb  WITH (NOLOCK) " & vbNewLine
        SQL = SQL & "    ON proposta_tb.proposta_id = apolice_tb.proposta_id" & vbNewLine
        SQL = SQL & "  JOIN sub_grupo_apolice_tb  WITH (NOLOCK) " & vbNewLine
        SQL = SQL & "    ON apolice_tb.apolice_id = sub_grupo_apolice_tb.apolice_id" & vbNewLine
        SQL = SQL & "   AND apolice_tb.sucursal_seguradora_id = sub_grupo_apolice_tb.sucursal_seguradora_id" & vbNewLine
        SQL = SQL & "   AND apolice_tb.seguradora_cod_susep = sub_grupo_apolice_tb.seguradora_cod_susep" & vbNewLine
        SQL = SQL & "   AND apolice_tb.ramo_id = sub_grupo_apolice_tb.ramo_id" & vbNewLine
        SQL = SQL & "  LEFT JOIN endosso_tb  WITH (NOLOCK) " & vbNewLine
        SQL = SQL & "    ON proposta_tb.proposta_id = endosso_tb.proposta_id" & vbNewLine
        SQL = SQL & "  JOIN ramo_tb  WITH (NOLOCK) " & vbNewLine
        SQL = SQL & "    ON proposta_tb.ramo_id = ramo_tb.ramo_id" & vbNewLine

        SQL = SQL & "  JOIN Produto_tb pr WITH(NOLOCK)"
        SQL = SQL & "    ON pr.produto_id = proposta_tb.produto_id "

        SQL = SQL & "  WHERE sub_grupo_apolice_tb.nome LIKE '%" & Trim(txtRazaoSocial) & "%'" & vbNewLine
        SQL = SQL & "   AND proposta_tb.produto_id in (" & ProdutosValidos & ")" & vbNewLine
        SQL = SQL & "   AND proposta_tb.situacao<>'t' " & vbNewLine
        SQL = SQL & "   AND tp_endosso_id not in (10,100,101,200)" & vbNewLine

        SQL = SQL & "UNION" & vbNewLine

        SQL = SQL & " SELECT cliente_tb.cpf_cnpj" & vbNewLine
        SQL = SQL & "      , sub_grupo_apolice_tb.nome" & vbNewLine
        SQL = SQL & "      , proposta_tb.ramo_id" & vbNewLine
        SQL = SQL & "      , sub_grupo_apolice_tb.apolice_id" & vbNewLine
        SQL = SQL & "      , sub_grupo_apolice_tb.sub_grupo_id " & vbNewLine
        SQL = SQL & "      , NULL proposta_bb" & vbNewLine
        SQL = SQL & "      , NULL nome_arquivo" & vbNewLine
        SQL = SQL & "      , proposta_tb.proposta_id" & vbNewLine
        SQL = SQL & "      , proposta_tb.dt_contratacao" & vbNewLine
        SQL = SQL & "      , proposta_tb.prop_cliente_id" & vbNewLine
        SQL = SQL & "      , descr_situacao = case proposta_tb.situacao " & vbNewLine
        SQL = SQL & "                         when 'e' then 'Em Estudo'  " & vbNewLine
        SQL = SQL & "                         when 'a' then 'Aceite' " & vbNewLine
        SQL = SQL & "                         when 'c' then 'Cancelada' " & vbNewLine
        SQL = SQL & "                         when 'r' then 'Recusada' " & vbNewLine
        SQL = SQL & "                         when 'i' then " & vbNewLine
        SQL = SQL & "                                  case (select count(1)" & vbNewLine
        SQL = SQL & "                                          from proposta_suspensa_tb s WITH(NOLOCK)" & vbNewLine
        SQL = SQL & "                                         where proposta_tb.proposta_id = s.proposta_id" & vbNewLine
        SQL = SQL & "                                           and s.dt_inicio_suspensao <= '20111024'" & vbNewLine
        SQL = SQL & "                                           and (s.dt_fim_suspensao is null" & vbNewLine
        SQL = SQL & "                                            or (s.dt_fim_suspensao is not null" & vbNewLine
        SQL = SQL & "                                           and  s.cod_suspensao = '1')))" & vbNewLine
        SQL = SQL & "                                  when 0 then 'Ap�lice Emitida'" & vbNewLine
        SQL = SQL & "                                  else 'Cancelada'        " & vbNewLine
        SQL = SQL & "                                  end " & vbNewLine
        SQL = SQL & "                         when 'p' then 'Em Estudo (erro de cadastramento)' " & vbNewLine
        SQL = SQL & "                         when 's' then 'Carta Recusa Emitida' " & vbNewLine
        SQL = SQL & "                       end " & vbNewLine
        SQL = SQL & "      , seg_cliente = CASE cliente_tb.seg_cliente " & vbNewLine
        SQL = SQL & "                      WHEN 'private' THEN 'PRIVATE'  " & vbNewLine
        SQL = SQL & "                      WHEN 'estilo' THEN 'ESTILO' " & vbNewLine
        SQL = SQL & "                      ELSE ' ' " & vbNewLine
        SQL = SQL & "                      END " & vbNewLine
        SQL = SQL & "      , endosso_id" & vbNewLine
        SQL = SQL & "      , nm_ramo = ramo_tb.nome" & vbNewLine
        SQL = SQL & "      , proposta_tb.produto_id" & vbNewLine
        SQL = SQL & "      , dt_pedido_endosso" & vbNewLine
        SQL = SQL & "      , num_endosso_bb" & vbNewLine
        SQL = SQL & "      , tp_endosso_id" & vbNewLine
        SQL = SQL & "      , proposta_tb.situacao      " & vbNewLine
        SQL = SQL & "      , pr.produto_id    " & vbNewLine
        SQL = SQL & "      , pr.nome as nome_prod   " & vbNewLine
        SQL = SQL & "      , isnull(pr.processa_cobranca,'n')as cobranca " & vbNewLine
        SQL = SQL & "      , proposta_tb.origem_proposta_id" & vbNewLine

        SQL = SQL & "  FROM proposta_tb  WITH (NOLOCK) " & vbNewLine
        SQL = SQL & "  JOIN proposta_adesao_tb  WITH (NOLOCK) " & vbNewLine
        SQL = SQL & "    ON proposta_adesao_tb.proposta_id = proposta_tb.proposta_id" & vbNewLine
        SQL = SQL & "  JOIN cliente_tb  WITH (NOLOCK) " & vbNewLine
        SQL = SQL & "    ON proposta_tb.prop_cliente_id = cliente_tb.cliente_id " & vbNewLine
        SQL = SQL & "  JOIN sub_grupo_apolice_tb  WITH (NOLOCK) " & vbNewLine
        SQL = SQL & "    ON sub_grupo_apolice_tb.apolice_id             = proposta_adesao_tb.apolice_id" & vbNewLine
        SQL = SQL & "   AND sub_grupo_apolice_tb.sucursal_seguradora_id = proposta_adesao_tb.sucursal_seguradora_id" & vbNewLine
        SQL = SQL & "   AND sub_grupo_apolice_tb.seguradora_cod_susep   = proposta_adesao_tb.seguradora_cod_susep" & vbNewLine
        SQL = SQL & "   AND sub_grupo_apolice_tb.ramo_id                = proposta_adesao_tb.ramo_id" & vbNewLine
        SQL = SQL & "  LEFT JOIN endosso_tb  WITH (NOLOCK) " & vbNewLine
        SQL = SQL & "    ON proposta_tb.proposta_id = endosso_tb.proposta_id" & vbNewLine
        SQL = SQL & "  JOIN ramo_tb  WITH (NOLOCK) " & vbNewLine
        SQL = SQL & "    ON proposta_tb.ramo_id = ramo_tb.ramo_id" & vbNewLine

        SQL = SQL & "  JOIN Produto_tb pr WITH(NOLOCK)"
        SQL = SQL & "    ON pr.produto_id = proposta_tb.produto_id "

        SQL = SQL & "  WHERE sub_grupo_apolice_tb.nome LIKE '%" & Trim(txtRazaoSocial) & "%'" & vbNewLine
        SQL = SQL & "   AND proposta_tb.produto_id in (" & ProdutosValidos & ")" & vbNewLine
        SQL = SQL & "   AND proposta_tb.situacao<>'t' " & vbNewLine
        SQL = SQL & "   AND tp_endosso_id not in (10,100,101,200)" & vbNewLine
        SQL = SQL & " ORDER BY cliente_tb.cpf_cnpj" & vbNewLine
        SQL = SQL & "        , sub_grupo_apolice_tb.nome" & vbNewLine
        SQL = SQL & "        , sub_grupo_apolice_tb.sub_grupo_id" & vbNewLine
        'carolina.marinho fim

        '***FIM RAZ�O SOCIAL
    Else    ' Proposta BB
        If Not IsNumeric(txtArgumento.Text) Then
            mensagem_erro 3, "N� da Proposta BB"
            txtArgumento.SelStart = 0
            txtArgumento.SelLength = Len(txtArgumento)
            txtArgumento.SetFocus
            MousePointer = vbDefault
            Exit Sub
        End If

        '****INICIO PROPOSTA BB
        ' mc_amaral 22/10/201317860335 - Melhorias no SEGBR(MP13?) - Acelerar aplicativos de consulta acessados pelo Sinistro
        '   SQL = ""
        '        SQL = " SELECT a.apolice_id, a.ramo_id, r.nome nm_ramo, a.seguradora_cod_susep, a.sucursal_seguradora_id " & vbNewLine
        '        SQL = SQL & ", p.proposta_id, pf.proposta_bb, p.produto_id " & vbNewLine
        '        SQL = SQL & ", e.endosso_id, e.dt_pedido_endosso, e.dt_emissao, e.num_endosso_bb " & vbNewLine
        '        SQL = SQL & ", e.tp_endosso_id, p.dt_contratacao, c.nome " & vbNewLine
        '        SQL = SQL & ", p.prop_cliente_id " & vbNewLine
        '        SQL = SQL & ", descr_situacao = case p.situacao " & vbNewLine
        '        SQL = SQL & "      when 'e' then 'Em Estudo' " & vbNewLine
        '        SQL = SQL & "      when 'a' then 'Aceite' " & vbNewLine
        '        SQL = SQL & "      when 'c' then 'Cancelada' " & vbNewLine
        '        SQL = SQL & "      when 'r' then 'Recusada' " & vbNewLine
        '        'SQL = SQL & "     when 'i' then 'Ap�lice Emitida' "
        '        'bcarneiro - 05/11/2003 - Vida Vida - Novas Funcionalidades ''''''''''''''''''''''''''
        '        SQL = SQL & "      when 'i' then " & vbNewLine
        '        SQL = SQL & "             case (select count(*)" & vbNewLine
        '        SQL = SQL & "                     from proposta_suspensa_tb s" & vbNewLine
        '        SQL = SQL & "                    where p.proposta_id = s.proposta_id" & vbNewLine
        '        'SQL = SQL & "                     and s.dt_inicio_suspensao >= '" & Format(Data_Sistema, "yyyymmdd") & "'"
        '        SQL = SQL & "                      and s.dt_inicio_suspensao <= '" & Format(Data_Sistema, "yyyymmdd") & "'" & vbNewLine
        '        SQL = SQL & "                      and (s.dt_fim_suspensao is null" & vbNewLine
        '        SQL = SQL & "                       or (s.dt_fim_suspensao is not null" & vbNewLine
        '        SQL = SQL & "                      and  s.cod_suspensao = '1')))" & vbNewLine
        '        SQL = SQL & "                 when 0 then 'Ap�lice Emitida'" & vbNewLine
        '        'Alessandra Grig�rio - 25/06/2004 - Atendendo solic. Marcio Yoshimura
        '        'SQL = SQL & "                else 'Suspensa'        "
        '        SQL = SQL & "                 else 'Cancelada'       " & vbNewLine
        '        'FIM
        '        SQL = SQL & "             end " & vbNewLine
        '        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        '        SQL = SQL & "      when 'p' then 'Em Estudo (erro de cadastramento)' " & vbNewLine
        '        SQL = SQL & "      when 's' then 'Carta Recusa Emitida' " & vbNewLine
        '        SQL = SQL & "    end " & vbNewLine
        '        SQL = SQL & ", a.tp_emissao " & vbNewLine
        '        SQL = SQL & ", NULL nome_arquivo " & vbNewLine
        '        'Rmarins - 05/01/2006 - Inclus�o do campo seg_cliente '''''''''''''''''''''''''''''''
        '        SQL = SQL & ",  seg_cliente = CASE c.seg_cliente                                                                                                     " & vbNewLine
        '        SQL = SQL & "                    WHEN 'private' THEN 'PRIVATE'                                                                                       " & vbNewLine
        '        SQL = SQL & "                    WHEN 'estilo' THEN 'ESTILO'                                                                                         " & vbNewLine
        '        SQL = SQL & "                    ELSE ' '                                                                                                            " & vbNewLine
        '        SQL = SQL & "                 END  " & vbNewLine
        '
        '        SQL = SQL & " FROM proposta_fechada_tb pf (NOLOCK), proposta_tb p (NOLOCK), apolice_tb a (NOLOCK)" & vbNewLine
        '        SQL = SQL & ",    endosso_tb e (NOLOCK), cliente_tb c (NOLOCK), ramo_tb r  (NOLOCK)" & vbNewLine
        '        SQL = SQL & " WHERE  pf.proposta_bb = " & txtArgumento & vbNewLine
        '        SQL = SQL & " AND   p.proposta_id = pf.proposta_id " & vbNewLine
        '        SQL = SQL & " AND   a.proposta_id = p.proposta_id " & vbNewLine
        '        SQL = SQL & " AND   e.proposta_id =* p.proposta_id " & vbNewLine
        '        '' N�o inclui os endossos financeiros
        '        SQL = SQL & " AND   e.tp_endosso_id not in (10,100,101,200) " & vbNewLine
        '        SQL = SQL & " AND   c.cliente_id = p.prop_cliente_id " & vbNewLine
        '        SQL = SQL & " AND   r.ramo_id = a.ramo_id " & vbNewLine
        '        SQL = SQL & " AND   p.produto_id in (" & ProdutosValidos & ") " & vbNewLine
        '        SQL = SQL & " AND   p.situacao <>'t' " & vbNewLine
        '        'SQL = SQL & " ORDER BY p.dt_contratacao "
        '        'acintra 06/10/2005
        '        'SQL = SQL & " ORDER BY e.dt_pedido_endosso " 'Alterado por fpimenta / Imar�s - 05/06/2003 - SAS 027-03
        '
        '        SQL = SQL & "" & vbNewLine
        '        SQL = SQL & " UNION ALL" & vbNewLine
        '        SQL = SQL & "" & vbNewLine
        '        SQL = SQL & "    SELECT NULL apolice_id,                                                                                                             " & vbNewLine
        '        SQL = SQL & "           ramo_tb.ramo_id,                                                                                                             " & vbNewLine
        '        SQL = SQL & "           ramo_tb.nome nm_ramo,                                                                                                        " & vbNewLine
        '        SQL = SQL & "           produto_tb.seguradora_cod_susep,                                                                                             " & vbNewLine
        '        SQL = SQL & "           arquivo_produto_tb.sucursal_seguradora_id ,                                                                                  " & vbNewLine
        '        SQL = SQL & "           proposta_tb.proposta_id,                                                                                                     " & vbNewLine
        '        SQL = SQL & "           proposta_fechada_tb.proposta_bb,                                                                                             " & vbNewLine
        '        SQL = SQL & "           proposta_tb.produto_id ,                                                                                                     " & vbNewLine
        '        SQL = SQL & "           NULL endosso_id,                                                                                                             " & vbNewLine
        '        SQL = SQL & "           NULL dt_pedido_endosso,                                                                                                      " & vbNewLine
        '        SQL = SQL & "           NULL dt_emissao,                                                                                                             " & vbNewLine
        '        SQL = SQL & "           NULL num_endosso_bb ,                                                                                                        " & vbNewLine
        '        SQL = SQL & "           NULL tp_endosso_id,                                                                                                          " & vbNewLine
        '        SQL = SQL & "           proposta_tb.dt_contratacao,                                                                                                  " & vbNewLine
        '        SQL = SQL & "           cliente_tb.nome ,                                                                                                            " & vbNewLine
        '        SQL = SQL & "           proposta_tb.prop_cliente_id,                                                                                                 " & vbNewLine
        '        SQL = SQL & "           descr_situacao = CASE proposta_tb.situacao                                                                                   " & vbNewLine
        '        SQL = SQL & "                              WHEN 'e' THEN 'Em Estudo'                                                                                 " & vbNewLine
        '        SQL = SQL & "                              WHEN 'r' THEN 'Recusada'                                                                                  " & vbNewLine
        '        SQL = SQL & "                              WHEN 'p' THEN 'Em Estudo (erro de cadastramento)'                                                         " & vbNewLine
        '        SQL = SQL & "                              WHEN 's' THEN 'Carta Recusa Emitida'                                                                      " & vbNewLine
        '        SQL = SQL & "                            END ,                                                                                                       " & vbNewLine
        '        SQL = SQL & "           NULL tp_emissao,                                                                                                             " & vbNewLine
        '        SQL = SQL & "           NULL nome_arquivo                                                                                                            " & vbNewLine
        '        'Rmarins - 05/01/2006 - Inclus�o do campo seg_cliente '''''''''''''''''''''''''''''''
        '        SQL = SQL & ",  seg_cliente = CASE cliente_tb.seg_cliente                                                                                                     " & vbNewLine
        '        SQL = SQL & "                    WHEN 'private' THEN 'PRIVATE'                                                                                       " & vbNewLine
        '        SQL = SQL & "                    WHEN 'estilo' THEN 'ESTILO'                                                                                         " & vbNewLine
        '        SQL = SQL & "                    ELSE ' '                                                                                                            " & vbNewLine
        '        SQL = SQL & "                 END  "
        '
        '        SQL = SQL & "      FROM proposta_tb(NOLOCK)                                                                                                          " & vbNewLine
        '        SQL = SQL & "INNER JOIN proposta_fechada_tb (NOLOCK)                                                                                                 " & vbNewLine
        '        SQL = SQL & "        ON proposta_fechada_tb.proposta_id = proposta_tb.proposta_id                                                                    " & vbNewLine
        '        SQL = SQL & " LEFT JOIN emi_re_proposta_tb (NOLOCK)                                                                                                     " & vbNewLine
        '        SQL = SQL & "        ON emi_re_proposta_tb.proposta_bb = proposta_fechada_tb.proposta_bb                                                             " & vbNewLine
        '        SQL = SQL & "       AND emi_re_proposta_tb.tp_registro = '01'                                                                                        " & vbNewLine
        '        SQL = SQL & " LEFT JOIN emi_proposta_tb (NOLOCK)                                                                                                       " & vbNewLine
        '        SQL = SQL & "        ON emi_proposta_tb.proposta_bb = proposta_fechada_tb.proposta_bb                                                                " & vbNewLine
        '        SQL = SQL & "       AND emi_proposta_tb.tp_registro = '01'                                                                                           " & vbNewLine
        '        SQL = SQL & "INNER JOIN produto_tb (NOLOCK)                                                                                                            " & vbNewLine
        '        SQL = SQL & "        ON produto_tb.produto_id = proposta_tb.produto_id                                                                               " & vbNewLine
        '        SQL = SQL & "INNER JOIN arquivo_produto_tb (NOLOCK)                                                                                                      " & vbNewLine
        '        SQL = SQL & "        ON arquivo_produto_tb.produto_id = proposta_tb.produto_id                                                                       " & vbNewLine
        '        SQL = SQL & "       AND arquivo_produto_tb.ramo_id = proposta_tb.ramo_id                                                                             " & vbNewLine
        '        SQL = SQL & "       AND arquivo_produto_tb.produto_externo_id = ISNULL(emi_re_proposta_tb.produto_externo, emi_proposta_tb.produto_externo)          " & vbNewLine
        '        SQL = SQL & "INNER JOIN cliente_tb (NOLOCK)                                                                                                          " & vbNewLine
        '        SQL = SQL & "        ON cliente_tb.cliente_id = proposta_tb.prop_cliente_id                                                                          " & vbNewLine
        '        SQL = SQL & "INNER JOIN ramo_tb (NOLOCK)                                                                                                             " & vbNewLine
        '        SQL = SQL & "        ON ramo_tb.ramo_id = proposta_tb.ramo_id                                                                                        " & vbNewLine
        '        SQL = SQL & "     WHERE proposta_fechada_tb.proposta_bb = " & txtArgumento.Text & vbNewLine
        '        SQL = SQL & "       AND proposta_tb.produto_id in (" & ProdutosValidos & ")                                                                          " & vbNewLine
        '        SQL = SQL & "       AND proposta_tb.situacao in ('r','e','p','s')         --rmaiellaro (19/01/2007)                                                                               " & vbNewLine
        '        'SQL = SQL & "  ORDER BY proposta_tb.dt_contratacao                                                                                                   " & vbNewLine
        '
        '        SQL = SQL & "" & vbNewLine
        '        SQL = SQL & " UNION ALL" & vbNewLine
        '        SQL = SQL & "" & vbNewLine
        '        SQL = SQL & "     SELECT NULL apolice_id,                          " & vbNewLine
        '        SQL = SQL & "            NULL ramo_id,                             " & vbNewLine
        '        SQL = SQL & "            ramo_atividade nm_ramo,                   " & vbNewLine
        '        SQL = SQL & "            cod_seguradora seguradora_cod_susep,      " & vbNewLine
        '        SQL = SQL & "            NULL sucursal_seguradora_id ,             " & vbNewLine
        '        SQL = SQL & "            emi_proposta_tb.proposta_id,                              " & vbNewLine
        '        SQL = SQL & "            emi_proposta_tb.proposta_bb,                              " & vbNewLine
        '        SQL = SQL & "            NULL produto_id ,                         " & vbNewLine
        '        SQL = SQL & "            emi_proposta_tb.endosso_id,                               " & vbNewLine
        '        SQL = SQL & "            NULL dt_pedido_endosso,                   " & vbNewLine
        '        SQL = SQL & "            NULL dt_emissao,                          " & vbNewLine
        '        SQL = SQL & "            NULL num_endosso_bb ,                     " & vbNewLine
        '        SQL = SQL & "            NULL tp_endosso_id,                       " & vbNewLine
        '        SQL = SQL & "            emi_proposta_tb.dt_contratacao,                           " & vbNewLine
        '        SQL = SQL & "            proponente nome,                          " & vbNewLine
        '        SQL = SQL & "            NULL prop_cliente_id,                     " & vbNewLine
        '        SQL = SQL & "            descr_situacao = 'N�o Emitido',           " & vbNewLine
        '        SQL = SQL & "            NULL tp_emissao,                          " & vbNewLine
        '        SQL = SQL & "            SUBSTRING(emi_proposta_tb.arquivo,1, CHARINDEX('.' ,emi_proposta_tb.arquivo) - 1) nome_arquivo " & vbNewLine
        '        'Rmarins - 05/01/2006 - Inclus�o do campo seg_cliente ''''''''''''''''''''''''''
        '        SQL = SQL & ",           NULL seg_cliente                          " & vbNewLine
        '
        '        SQL = SQL & "       FROM emi_proposta_tb (NOLOCK)                  " & vbNewLine
        '        SQL = SQL & "       inner join proposta_tb (NOLOCK) on proposta_tb.proposta_id = emi_proposta_tb.proposta_id                  " & vbNewLine
        '        SQL = SQL & "      WHERE emi_proposta_tb.situacao = 'n'                            " & vbNewLine
        '        SQL = SQL & "        AND emi_proposta_tb.proposta_bb = " & txtArgumento.Text & vbNewLine
        '        SQL = SQL & "        AND proposta_tb.situacao<>'t' " & vbNewLine
        '        SQL = SQL & "                                                      " & vbNewLine
        '        SQL = SQL & "  UNION ALL                                           " & vbNewLine
        '        SQL = SQL & "                                                      " & vbNewLine
        '        SQL = SQL & "     SELECT NULL apolice_id,                          " & vbNewLine
        '        SQL = SQL & "            NULL ramo_id,                             " & vbNewLine
        '        SQL = SQL & "            NULL nm_ramo,                             " & vbNewLine
        '        SQL = SQL & "            cod_seguradora seguradora_cod_susep,      " & vbNewLine
        '        SQL = SQL & "            NULL sucursal_seguradora_id ,             " & vbNewLine
        '        SQL = SQL & "            emi_re_proposta_tb.proposta_id,                              " & vbNewLine
        '        SQL = SQL & "            emi_re_proposta_tb.proposta_bb,                              " & vbNewLine
        '        SQL = SQL & "            NULL produto_id,                          " & vbNewLine
        '        SQL = SQL & "            emi_re_proposta_tb.endosso_id,                               " & vbNewLine
        '        SQL = SQL & "            NULL dt_pedido_endosso,                   " & vbNewLine
        '        SQL = SQL & "            NULL dt_emissao,                          " & vbNewLine
        '        SQL = SQL & "            emi_re_proposta_tb.num_endosso_bb,                           " & vbNewLine
        '        SQL = SQL & "            NULL tp_endosso_id,                       " & vbNewLine
        '        SQL = SQL & "            emi_re_proposta_tb.dt_contratacao,                           " & vbNewLine
        '        SQL = SQL & "            proponente nome,                          " & vbNewLine
        '        SQL = SQL & "            NULL prop_cliente_id,                     " & vbNewLine
        '        SQL = SQL & "            descr_situacao = 'N�o Emitido',           " & vbNewLine
        '        SQL = SQL & "            NULL tp_emissao,                          " & vbNewLine
        '        SQL = SQL & "            SUBSTRING(emi_re_proposta_tb.arquivo,1, CHARINDEX('.' ,emi_re_proposta_tb.arquivo) - 1) nome_arquivo " & vbNewLine
        '        'Rmarins - 05/01/2006 - Inclus�o do campo seg_cliente ''''''''''''''''''''''''''
        '        SQL = SQL & ",           ' ' seg_cliente                           " & vbNewLine
        '
        '        SQL = SQL & "       FROM emi_re_proposta_tb (NOLOCK)               " & vbNewLine
        '        SQL = SQL & "       inner join proposta_tb (NOLOCK) on proposta_tb.proposta_id = emi_re_proposta_tb.proposta_id                  " & vbNewLine
        '        SQL = SQL & "      WHERE emi_re_proposta_tb.situacao = 'n'                            " & vbNewLine
        '        SQL = SQL & "        AND emi_re_proposta_tb.proposta_bb = " & txtArgumento.Text & vbNewLine
        '        SQL = SQL & "        AND (arquivo LIKE 'SEG491D%'                  " & vbNewLine
        '        SQL = SQL & "             OR arquivo LIKE 'SEG493%'                " & vbNewLine
        '        SQL = SQL & "             OR arquivo LIKE 'SEGF496%'               " & vbNewLine
        '        SQL = SQL & "             OR arquivo LIKE 'INTERNET%'              " & vbNewLine
        '        SQL = SQL & "             OR arquivo LIKE 'SEG486%'                " & vbNewLine
        '        SQL = SQL & "             OR arquivo LIKE 'SEG422%'                " & vbNewLine
        '        SQL = SQL & "             OR arquivo LIKE 'SEGF495%')              " & vbNewLine
        '        SQL = SQL & "        AND proposta_tb.situacao<>'t' " & vbNewLine
        '        'SQL = SQL & "   ORDER BY dt_contratacao                            " & vbNewLine
        '        'AFONSO DUTRA - DATA: 09/03/2010 - FLOW: 3206098
        '        'Raimundo Belas da Silva Junior - GPTI - 13/04/2010
        '        'Acertando ORDER BY
        '        'SQL = SQL & " ORDER BY e.dt_pedido_endosso, /*e.dt_emissao,*/ p.dt_contratacao, e.endosso_id "
        '        'Denis Silva  - CWI - 26/05/2010
        '        'Acertando ORDER BY - FLOW:3727253
        '        SQL = SQL & " ORDER BY  e.dt_emissao, /*e.dt_pedido_endosso,*/ p.dt_contratacao, e.endosso_id "
        '        'FIM
        '
        '        End If

        'Inicio

        proposta_id_bb = ""
        SQL = ""
        SQL = SQL & "SELECT SIT = 'F'"
        SQL = SQL & "     , a.proposta_id"
        SQL = SQL & "     , a.situacao      " & vbNewLine
        SQL = SQL & "     , a.produto_id    " & vbNewLine
        SQL = SQL & "  FROM proposta_tb a  WITH(NOLOCK)" & vbNewLine
        SQL = SQL & "  JOIN proposta_fechada_tb pf  WITH (NOLOCK) " & vbNewLine
        SQL = SQL & "    ON a.proposta_id = pf.proposta_id" & vbNewLine
        SQL = SQL & " WHERE PF.proposta_bb = " & txtArgumento.Text
        SQL = SQL & " AND a.situacao <> 't' " & vbNewLine




        Set rc = rdocn.OpenResultset(SQL)




        If rc.EOF Then

            Set rc = Nothing

            SQL = ""
            SQL = SQL & "SELECT a.situacao "
            SQL = SQL & "     , a.proposta_id"
            SQL = SQL & "     , SIT = 'A'      " & vbNewLine
            SQL = SQL & "     , a.produto_id    " & vbNewLine
            SQL = SQL & "  FROM proposta_tb a  WITH(NOLOCK)" & vbNewLine
            SQL = SQL & "  JOIN proposta_ADESAO_tb pf  WITH (NOLOCK) " & vbNewLine
            SQL = SQL & "    ON a.proposta_id = pf.proposta_id" & vbNewLine
            SQL = SQL & " WHERE PF.proposta_bb = " & txtArgumento.Text
            SQL = SQL & " AND a.situacao <> 't' " & vbNewLine


            Set rc = rdocn.OpenResultset(SQL)
        End If
        'Se n�o existi ja sai do programa
        '*******************************************************************
        If rc.EOF Then
            StatusBar1.SimpleText = "Nenhuma proposta foi encontrada !"
            GridProposta.Rows = 1
            rc.Close
            MousePointer = vbDefault
            Exit Sub
        End If
        '******************************************************************
        SIT = (rc!SIT)
        sSituacao = (rc!Situacao)
        vProdutoId = IIf(IsNull(rc!Produto_id), "", rc!Produto_id)
        While Not rc.EOF
            proposta_id_bb1 = proposta_id_bb1 & CStr(rc!proposta_id) & ","
            rc.MoveNext
        Wend
        proposta_id_bb = Mid(proposta_id_bb1, 1, Len(proposta_id_bb1) - 1)





        'Verifica se o produto e valido
        '*********************************************************************************
        SQL = ""
        SQL = SQL & "SELECT count(1)" & vbNewLine
        SQL = SQL & "  FROM Produto_tb   WITH(NOLOCK)" & vbNewLine
        SQL = SQL & " WHERE  produto_id in (" & ProdutosValidos & ") " & vbNewLine
        SQL = SQL & "   AND  produto_id = " & vProdutoId & " " & vbNewLine
        Set rc2 = rdoCn2.OpenResultset(SQL)


        '** Se o produto n�o for valido mostra mensagem e finaliza consulta

        If rc2.EOF Then
            MsgBox "Produto inv�lido!", vbInformation, Screen.ActiveForm.Caption
            txtArgumento.SelStart = 0
            txtArgumento.SelLength = Len(txtArgumento)
            txtArgumento.SetFocus
            MousePointer = vbDefault
            rc2.Close
            Exit Sub
        End If
        rc2.Close
        SQL = ""
        '******************************************************************************************************

        'lpinto/acintra 27/09/2005
        bPropostaRecusada = False
        'eduardo.amaral(Nova Consultoria) 23-02-2012 Demanda: BB SEGURO VIDA EMPRESA FLEX
        If Not rc.EOF Then
            If UCase(rc(0)) = "R" Or UCase(rc(0)) = "S" Then
                bPropostaRecusada = True
            End If
        End If

        'fim eduardo.amaral(Nova Consultoria)
        'rmaiellaro (19/01/2007)

        bPropostaEstudo = False
        If Not rc.EOF Then
            If UCase(rc(0)) = "E" Or UCase(rc(0)) = "P" Then
                bPropostaEstudo = True
            End If
        End If


        '        If Not rc2.EOF Then
        '           vProdutoId = IIf(IsNull(rc2!Produto_id), "", rc2!Produto_id)
        '           vNomeProd = rc2!nome_prod
        '           ProcessaCobranca = rc2!Cobranca
        '           vSituacao = rc2!Situacao
        '           vorigem_proposta_id = rc2!origem_proposta_id
        '
        '           If vorigem_proposta_id = 2 Then
        '                bPropostaALS = True
        '            Else
        '                bPropostaALS = False
        '            End If
        '        End If
        '        rc2.Close
        '        Set rc2 = Nothing



        'lpinto - 21/09/2005 - Consulta proposta recusada - proposta_fechada_tb
        If Not bPropostaRecusada And Not bPropostaEstudo And UCase(SIT) = "F" Then    'rmaiellaro (19/01/2007)

            SQL = ""
            SQL = "     SELECT a.apolice_id" & vbNewLine
            SQL = SQL & "    , a.ramo_id" & vbNewLine
            SQL = SQL & "    , r.nome nm_ramo" & vbNewLine
            SQL = SQL & "    , a.seguradora_cod_susep" & vbNewLine
            SQL = SQL & "    , a.sucursal_seguradora_id " & vbNewLine
            SQL = SQL & "    , pf.proposta_bb" & vbNewLine
            SQL = SQL & "    , p.proposta_id" & vbNewLine
            SQL = SQL & "    , pr.produto_id" & vbNewLine
            SQL = SQL & "    , P.situacao      " & vbNewLine
            SQL = SQL & "    , pr.nome  AS nome_prod  " & vbNewLine
            SQL = SQL & "    , isnull(pr.processa_cobranca,'n') as cobranca "
            SQL = SQL & "    , e.endosso_id " & vbNewLine
            SQL = SQL & "    , e.dt_pedido_endosso" & vbNewLine
            SQL = SQL & "    , e.num_endosso_bb" & vbNewLine
            SQL = SQL & "    , e.tp_endosso_id " & vbNewLine
            SQL = SQL & "    , c.nome" & vbNewLine
            SQL = SQL & "    , p.dt_contratacao" & vbNewLine
            SQL = SQL & "    , p.prop_cliente_id" & vbNewLine
            SQL = SQL & "    , p.origem_proposta_id " & vbNewLine
            SQL = SQL & "    , descr_situacao = case p.situacao " & vbNewLine
            SQL = SQL & "                       when 'e' then 'Em Estudo' " & vbNewLine
            SQL = SQL & "                       when 'a' then 'Aceite' " & vbNewLine
            SQL = SQL & "                       when 'c' then 'Cancelada' " & vbNewLine
            SQL = SQL & "                       when 'r' then 'Recusada' " & vbNewLine
            'SQL = SQL & "     when 'i' then 'Ap�lice Emitida' "
            'bcarneiro - 05/11/2003 - Vida Vida - Novas Funcionalidades ''''''''''''''''
            SQL = SQL & "                       when 'i' then " & vbNewLine
            SQL = SQL & "                                case (select count(1) " & vbNewLine
            SQL = SQL & "                                        from proposta_suspensa_tb s WITH(NOLOCK) " & vbNewLine
            SQL = SQL & "                                       where p.proposta_id = s.proposta_id " & vbNewLine
            'SQL = SQL & "                 and s.dt_inicio_suspensao >= '" & Format(Data_Sistema, "yyyymmdd") & "'"
            SQL = SQL & "                                         and s.dt_inicio_suspensao <= '" & Format(Data_Sistema, "yyyymmdd") & "'" & vbNewLine
            SQL = SQL & "                                         and (s.dt_fim_suspensao is null " & vbNewLine
            SQL = SQL & "                                          or (s.dt_fim_suspensao is not null " & vbNewLine
            SQL = SQL & "                                         and  s.cod_suspensao = '1'))) " & vbNewLine
            SQL = SQL & "                                when 0 then 'Ap�lice Emitida'" & vbNewLine
            'Alessandra Grig�rio - 25/06/2004 - Atendendo solic. Marcio Yoshimura
            'SQL = SQL & "            else 'Suspensa'        "
            SQL = SQL & "                                else 'Cancelada'       " & vbNewLine
            'FIM
            SQL = SQL & "                                 end " & vbNewLine
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            SQL = SQL & "                    when 'p' then 'Em Estudo (erro de cadastramento)' " & vbNewLine
            SQL = SQL & "                    when 's' then 'Carta Recusa Emitida' " & vbNewLine
            SQL = SQL & "                     end " & vbNewLine
            SQL = SQL & "    , a.tp_emissao " & vbNewLine
            SQL = SQL & "    , NULL nome_arquivo " & vbNewLine
            'Rmarins - 05/01/2006 - Inclus�o do campo seg_cliente '''''''''''''''''''''
            SQL = SQL & "    , seg_cliente = CASE c.seg_cliente " & vbNewLine
            SQL = SQL & "                    WHEN 'private' THEN 'PRIVATE' " & vbNewLine
            SQL = SQL & "                    WHEN 'estilo' THEN 'ESTILO' " & vbNewLine
            SQL = SQL & "                    ELSE ' ' " & vbNewLine
            SQL = SQL & "                    END " & vbNewLine
            'ANTES
            'SQL = SQL & "FROM   proposta_tb p  WITH (NOLOCK) , apolice_tb a  WITH (NOLOCK) " & vbNewLine
            'SQL = SQL & ",      proposta_fechada_tb pf  WITH (NOLOCK) , cliente_tb c  WITH (NOLOCK) , ramo_tb r   WITH (NOLOCK)  " & vbNewLine
            'DEPOIS
            SQL = SQL & "      FROM proposta_tb p  WITH (NOLOCK) " & vbNewLine
            SQL = SQL & "      JOIN apolice_tb a  WITH (NOLOCK) " & vbNewLine
            SQL = SQL & "        ON a.proposta_id = p.proposta_id " & vbNewLine
            SQL = SQL & "      JOIN proposta_fechada_tb pf  WITH (NOLOCK) "
            SQL = SQL & "        ON pf.proposta_id = p.proposta_id " & vbNewLine
            SQL = SQL & "      JOIN cliente_tb c  WITH (NOLOCK) " & vbNewLine
            SQL = SQL & "        ON c.cliente_id = p.prop_cliente_id " & vbNewLine
            SQL = SQL & "      JOIN ramo_tb r   WITH (NOLOCK)  " & vbNewLine
            SQL = SQL & "        ON r.ramo_id = a.ramo_id " & vbNewLine
            SQL = SQL & "     LEFT JOIN endosso_tb e  WITH (NOLOCK)  "
            SQL = SQL & "        ON e.proposta_id = p.proposta_id " & vbNewLine
            SQL = SQL & "       AND  e.tp_endosso_id not in (10,100,101,200) " & vbNewLine
            SQL = SQL & "      JOIN Produto_tb pr WITH(NOLOCK)" & vbNewLine
            SQL = SQL & "        ON p.produto_id = pr.produto_id " & vbNewLine
            'SQL = SQL & "     WHERE  p.proposta_id = " & txtArgumento & vbNewLine ' mc_amaral 22/10/201317860335 - Melhorias no SEGBR(MP13?) - Acelerar aplicativos de consulta acessados pelo Sinistro
            SQL = SQL & "     WHERE  p.proposta_id in (" & proposta_id_bb & ")" & vbNewLine
            ' SQL = SQL & "  AND  e.proposta_id =* p.proposta_id " & vbNewLine
            '' N�o inclui os endossos financeiros
            SQL = SQL & "       AND  p.produto_id in (" & ProdutosValidos & ") " & vbNewLine
            SQL = SQL & "       AND  p.situacao <>'t' " & vbNewLine
            'SQL = SQL & " ORDER BY p.dt_contratacao "
            ' SQL = SQL & " ORDER BY e.dt_pedido_endosso " 'Alterado por fpimenta / Imar�s - 05/06/2003 - SAS 027-03
            'AFONSO DUTRA - DATA: 09/03/2010 - FLOW: 3206098
            'Raimundo Belas da Silva Junior - GPTI - 13/04/2010
            'Acertando ORDER BY
            'SQL = SQL & " ORDER BY e.dt_pedido_endosso, /*e.dt_emissao,*/ p.dt_contratacao, e.endosso_id "
            'Denis Silva  - CWI - 26/05/2010
            'Acertando ORDER BY - FLOW:3727253
            SQL = SQL & "ORDER BY  e.dt_emissao, /*e.dt_pedido_endosso,*/ p.dt_contratacao, e.endosso_id "
            'FIM
        Else
            If UCase(SIT) = "F" Then
                SQL = ""
                SQL = SQL & "    SELECT DISTINCT NULL apolice_id,  --rmaiellaro (19/01/2007)                                                                                      " & vbNewLine
                SQL = SQL & "           ramo_tb.ramo_id,                                                                                                            " & vbNewLine
                SQL = SQL & "           ramo_tb.nome nm_ramo,                                                                                                       " & vbNewLine
                SQL = SQL & "           produto_tb.seguradora_cod_susep,                                                                                            " & vbNewLine
                SQL = SQL & "           arquivo_produto_tb.sucursal_seguradora_id,                                                                                  " & vbNewLine
                SQL = SQL & "           proposta_fechada_tb.proposta_bb,                                                                                            " & vbNewLine
                SQL = SQL & "           proposta_tb.proposta_id,                                                                                                    " & vbNewLine
                SQL = SQL & "           proposta_tb.produto_id,                                                                                                     " & vbNewLine
                SQL = SQL & "           produto_tb.nome  AS nome_prod,  " & vbNewLine
                SQL = SQL & "           isnull(produto_tb.processa_cobranca,'n') as cobranca, "
                SQL = SQL & "           NULL endosso_id,                                                                                                            " & vbNewLine
                SQL = SQL & "           NULL dt_pedido_endosso,                                                                                                     " & vbNewLine
                SQL = SQL & "           NULL num_endosso_bb,                                                                                                        " & vbNewLine
                SQL = SQL & "           NULL tp_endosso_id,                                                                                                         " & vbNewLine
                SQL = SQL & "           cliente_tb.nome,                                                                                                            " & vbNewLine
                SQL = SQL & "           proposta_tb.dt_contratacao,                                                                                                 " & vbNewLine
                SQL = SQL & "           proposta_tb.prop_cliente_id,                                                                                                " & vbNewLine
                SQL = SQL & "           proposta_tb.situacao,      " & vbNewLine
                SQL = SQL & "           descr_situacao = CASE proposta_tb.situacao                                                                                  " & vbNewLine
                SQL = SQL & "                              WHEN 'e' THEN 'Em Estudo'                                                                                " & vbNewLine
                SQL = SQL & "                              WHEN 'a' THEN 'Aceite'                                                                                   " & vbNewLine
                SQL = SQL & "                              WHEN 'c' THEN 'Cancelada'                                                                                " & vbNewLine
                SQL = SQL & "                              WHEN 'r' THEN 'Recusada'                                                                                 " & vbNewLine
                SQL = SQL & "                              WHEN 'i' THEN CASE (    SELECT COUNT(1)                                                                  " & vbNewLine
                SQL = SQL & "                                                        FROM proposta_suspensa_tb WITH(NOLOCK)                                         " & vbNewLine
                SQL = SQL & "                                                  INNER JOIN proposta_tb     WITH(NOLOCK)                                              " & vbNewLine
                SQL = SQL & "                                                          ON proposta_tb.proposta_id = proposta_suspensa_tb.proposta_id                " & vbNewLine
                SQL = SQL & "                                                       WHERE proposta_suspensa_tb.dt_inicio_suspensao <= '20050722'                    " & vbNewLine
                SQL = SQL & "                                                         AND (proposta_suspensa_tb.dt_fim_suspensao IS NULL                            " & vbNewLine
                SQL = SQL & "                                                          OR (proposta_suspensa_tb.dt_fim_suspensao IS NOT NULL                        " & vbNewLine
                SQL = SQL & "                                                              AND  proposta_suspensa_tb.cod_suspensao = '1')))                         " & vbNewLine
                SQL = SQL & "                                              WHEN 0 THEN 'Ap�lice Emitida'                                                            " & vbNewLine
                SQL = SQL & "                                              ELSE 'Cancelada'                                                                         " & vbNewLine
                SQL = SQL & "                                            END                                                                                        " & vbNewLine
                SQL = SQL & "                              WHEN 'p' THEN 'Em Estudo (erro de cadastramento)'                                                        " & vbNewLine
                SQL = SQL & "                              WHEN 's' THEN 'Carta Recusa Emitida'                                                                     " & vbNewLine
                SQL = SQL & "                            END,                                                                                                       " & vbNewLine
                SQL = SQL & "           NULL tp_emissao,                                                                                                            " & vbNewLine
                SQL = SQL & "           NULL nome_arquivo                                                                                                           " & vbNewLine
                'Rmarins - 05/01/2006 - Inclus�o do campo seg_cliente ''''''''''''''''''''''''''''''''''
                SQL = SQL & ",          seg_cliente = CASE cliente_tb.seg_cliente                                                                                   " & vbNewLine
                SQL = SQL & "                           WHEN 'private' THEN 'PRIVATE'                                                                               " & vbNewLine
                SQL = SQL & "                           WHEN 'estilo' THEN 'ESTILO'                                                                                 " & vbNewLine
                SQL = SQL & "                           ELSE ' '                                                                                                    " & vbNewLine
                SQL = SQL & "                         END                                                                                                           " & vbNewLine

                SQL = SQL & "      FROM proposta_tb WITH (NOLOCK)                                                                                                          " & vbNewLine
                SQL = SQL & "INNER JOIN proposta_fechada_tb  WITH (NOLOCK)                                                                                                 " & vbNewLine
                SQL = SQL & "        ON proposta_fechada_tb.proposta_id = proposta_tb.proposta_id                                                                   " & vbNewLine
                SQL = SQL & "INNER JOIN produto_tb  WITH (NOLOCK)                                                                                                          " & vbNewLine
                SQL = SQL & "        ON produto_tb.produto_id = proposta_tb.produto_id                                                                              " & vbNewLine
                SQL = SQL & "INNER JOIN arquivo_produto_tb  WITH (NOLOCK)                                                                                                  " & vbNewLine
                SQL = SQL & "        ON arquivo_produto_tb.produto_id = proposta_tb.produto_id                                                                      " & vbNewLine
                SQL = SQL & "       AND arquivo_produto_tb.ramo_id = proposta_tb.ramo_id                                                                            " & vbNewLine
                SQL = SQL & "INNER JOIN cliente_tb  WITH (NOLOCK)                                                                                                          " & vbNewLine
                SQL = SQL & "        ON cliente_tb.cliente_id = proposta_tb.prop_cliente_id                                                                         " & vbNewLine
                SQL = SQL & "INNER JOIN ramo_tb  WITH (NOLOCK)                                                                                                             " & vbNewLine
                SQL = SQL & "        ON ramo_tb.ramo_id = proposta_tb.ramo_id                                                                                       " & vbNewLine
                'SQL = SQL & "     WHERE proposta_tb.proposta_id = " & txtArgumento.Text & vbNewLine
                SQL = SQL & "     WHERE  proposta_tb.proposta_id in (" & proposta_id_bb & ")" & vbNewLine
                SQL = SQL & "       AND proposta_fechada_tb.proposta_id = proposta_tb.proposta_id                                                                   " & vbNewLine
                SQL = SQL & "       AND proposta_tb.produto_id in (" & ProdutosValidos & ")                                                                         " & vbNewLine
                SQL = SQL & "       AND proposta_tb.situacao<>'t'                                                                                                   " & vbNewLine

                SQL = SQL & "  ORDER BY proposta_tb.dt_contratacao                                                                                                  " & vbNewLine
            End If

        End If

        '*****************************************************************************
        '** Descri��o: Implementa��o de consulta por proposta ades�o                **
        '** Motivo: Atender consultas para o produto 714                            **
        '** Autor: Febner (Fabio Alves de Araujo Ebner - JR & P(SP)                 **
        '** Data: 10/06/03                                                          **
        '*****************************************************************************
        If SQL <> "" Then
            Sql_Aux = SQL
            Set rc2 = rdoCn2.OpenResultset(Sql_Aux)
            If rc2.EOF Then

                ' lpinto 21/09/2005 - Consulta proposta recusada - proposta_adesao_tb
                'If Not bPropostaRecusada And Not bPropostaEstudo Then 'rmaiellaro (19/01/2007)
                'alterado And vProdutoId = 714 -11/09/2013 projeto 17860335 - Melhorias no SEGBR MC_Amaral -confitec
                If Not bPropostaRecusada And Not bPropostaEstudo And (IIf(IsNull(vProdutoId), "", vProdutoId)) = 714 And UCase(SIT) = "A" Then    'rmaiellaro (19/01/2007)
                    SQL = ""
                    SQL = SQL & "       SELECT a.apolice_id" & vbNewLine
                    SQL = SQL & "            , a.ramo_id" & vbNewLine
                    SQL = SQL & "            , r.nome nm_ramo" & vbNewLine
                    SQL = SQL & "            , a.seguradora_cod_susep" & vbNewLine
                    SQL = SQL & "            , a.sucursal_seguradora_id  " & vbNewLine
                    SQL = SQL & "            , pf.proposta_bb, p.proposta_id" & vbNewLine
                    SQL = SQL & "            , p.produto_id" & vbNewLine
                    SQL = SQL & "            , pR.nome  AS nome_prod  " & vbNewLine
                    SQL = SQL & "            , isnull(pr.processa_cobranca,'n') as cobranca "
                    SQL = SQL & "            , e.endosso_id " & vbNewLine
                    SQL = SQL & "            , e.dt_pedido_endosso" & vbNewLine
                    SQL = SQL & "            , e.num_endosso_bb" & vbNewLine
                    SQL = SQL & "            , e.tp_endosso_id" & vbNewLine
                    SQL = SQL & "            , c.nome " & vbNewLine
                    SQL = SQL & "            , p.dt_contratacao" & vbNewLine
                    SQL = SQL & "            , p.prop_cliente_id " & vbNewLine
                    SQL = SQL & "            , P.situacao      " & vbNewLine
                    SQL = SQL & "            , descr_situacao = case p.situacao " & vbNewLine
                    SQL = SQL & "                               when 'e' then 'Em Estudo' " & vbNewLine
                    SQL = SQL & "                               when 'a' then 'Aceite' " & vbNewLine
                    SQL = SQL & "                               when 'c' then 'Cancelada' " & vbNewLine
                    SQL = SQL & "                               when 'r' then 'Recusada' " & vbNewLine
                    'SQL = SQL & "           when 'i' then 'Ap�lice Emitida' "
                    'bcarneiro - 05/11/2003 - Vida Vida - Novas Funcionalidades ''''''''''''''''
                    SQL = SQL & "                               when 'i' then " & vbNewLine
                    SQL = SQL & "                                       case (select count(1)" & vbNewLine
                    SQL = SQL & "                                               from proposta_suspensa_tb s WITH(NOLOCK)" & vbNewLine
                    SQL = SQL & "                                              where p.proposta_id = s.proposta_id " & vbNewLine
                    'SQL = SQL & "                and s.dt_inicio_suspensao >= '" & Format(Data_Sistema, "yyyymmdd") & "'"
                    SQL = SQL & "                                                and s.dt_inicio_suspensao <= '" & Format(Data_Sistema, "yyyymmdd") & "'" & vbNewLine
                    SQL = SQL & "                                                and (s.dt_fim_suspensao is null " & vbNewLine
                    SQL = SQL & "                                                 or (s.dt_fim_suspensao is not null " & vbNewLine
                    SQL = SQL & "                                                and  s.cod_suspensao = '1')))" & vbNewLine
                    SQL = SQL & "                                               when 0 then 'Ap�lice Emitida' " & vbNewLine
                    'Alessandra Grig�rio - 25/06/2004 - Atendendo solic. Marcio Yoshimura
                    'SQL = SQL & "                    else 'Suspensa'        "
                    SQL = SQL & "                                               else 'Cancelada'       " & vbNewLine
                    'FIM
                    SQL = SQL & "                                                end " & vbNewLine
                    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                    SQL = SQL & "                               when 'p' then 'Em Estudo (erro de cadastramento)' " & vbNewLine
                    SQL = SQL & "                               when 's' then 'Carta Recusa Emitida' " & vbNewLine
                    SQL = SQL & "                                end, ap.tp_emissao, NULL nome_arquivo " & vbNewLine

                    'Rmarins - 05/01/2006 - Inclus�o do campo seg_cliente '''''''''''''''''''''''''''''''
                    SQL = SQL & "              ,  seg_cliente = CASE c.seg_cliente    " & vbNewLine
                    SQL = SQL & "                               WHEN 'private' THEN 'PRIVATE'  " & vbNewLine
                    SQL = SQL & "                               WHEN 'estilo' THEN 'ESTILO'    " & vbNewLine
                    SQL = SQL & "                               ELSE ' '   " & vbNewLine
                    SQL = SQL & "                                END  " & vbNewLine

                    SQL = SQL & "           FROM  proposta_tb p  WITH (NOLOCK) " & vbNewLine
                    SQL = SQL & "               , certificado_tb a  WITH (NOLOCK) " & vbNewLine
                    SQL = SQL & "               , endosso_tb e  WITH (NOLOCK) " & vbNewLine
                    SQL = SQL & "               , proposta_adesao_tb pf  WITH (NOLOCK) " & vbNewLine
                    SQL = SQL & "               , cliente_tb c  WITH (NOLOCK) " & vbNewLine
                    SQL = SQL & "               , ramo_tb r  WITH (NOLOCK) " & vbNewLine
                    SQL = SQL & "               , apolice_tb ap  WITH (NOLOCK)  " & vbNewLine
                    SQL = SQL & "               , produto_tb pr  WITH (NOLOCK)  " & vbNewLine
                    'SQL = SQL & "           WHERE p.proposta_id = " & txtArgumento & vbNewLine
                    SQL = SQL & "           WHERE  p.proposta_id in (" & proposta_id_bb & ")" & vbNewLine
                    SQL = SQL & "             AND a.proposta_id = p.proposta_id " & vbNewLine
                    SQL = SQL & "             AND pf.proposta_id = p.proposta_id " & vbNewLine
                    SQL = SQL & "             AND e.proposta_id = p.proposta_id " & vbNewLine
                    SQL = SQL & "             AND e.tp_endosso_id not in (10,100,101,200) " & vbNewLine
                    SQL = SQL & "             AND c.cliente_id = p.prop_cliente_id " & vbNewLine
                    SQL = SQL & "             AND r.ramo_id = a.ramo_id " & vbNewLine
                    SQL = SQL & "             AND a.apolice_id = ap.apolice_id " & vbNewLine
                    SQL = SQL & "             AND a.ramo_id = ap.ramo_id " & vbNewLine
                    SQL = SQL & "             AND p.produto_id = 714 " & vbNewLine
                    SQL = SQL & "             AND p.produto_id = pr.produto_id " & vbNewLine
                    SQL = SQL & "             AND p.situacao<>'t'          " & vbNewLine
                    'SQL = SQL & "   ORDER BY  p.dt_contratacao " & vbNewLine
                    'AFONSO DUTRA NOGUEIRA FILHO - GPTI - DATA: 25/03/2010
                    'Raimundo Belas da Silva Junior - GPTI - 13/04/2010
                    'Acertando ORDER BY
                    'Denis Silva  - CWI - 26/05/2010
                    'Acertando ORDER BY - FLOW:3727253
                    'SQL = SQL & " ORDER BY e.dt_pedido_endosso, /*e.dt_emissao,*/ p.dt_contratacao, e.endosso_id "
                    SQL = SQL & " ORDER BY e.dt_emissao, /*e.dt_pedido_endosso,*/ p.dt_contratacao, e.endosso_id "

                Else
                    'acrescentado And vProdutoId = 714 -11/09/2013 projeto 17860335 - Melhorias no SEGBR MC_Amaral -confitec
                    If (IIf(IsNull(vProdutoId), "", vProdutoId)) = 714 And UCase(SIT) = "A" Then
                        SQL = ""
                        SQL = SQL & "    SELECT DISTINCT NULL apolice_id, --rmaiellaro (19/01/2007)                                                                                      " & vbNewLine
                        SQL = SQL & "           ramo_tb.ramo_id,                                                                                                           " & vbNewLine
                        SQL = SQL & "           ramo_tb.nome nm_ramo,                                                                                                      " & vbNewLine
                        SQL = SQL & "           produto_tb.seguradora_cod_susep,                                                                                           " & vbNewLine
                        SQL = SQL & "           arquivo_produto_tb.sucursal_seguradora_id ,                                                                                " & vbNewLine
                        SQL = SQL & "           proposta_adesao_tb.proposta_bb,                                                                                            " & vbNewLine
                        SQL = SQL & "           proposta_tb.proposta_id,                                                                                                   " & vbNewLine
                        SQL = SQL & "           proposta_tb.produto_id,                                                                                                    " & vbNewLine
                        SQL = SQL & "           produto_tb.nome  AS nome_prod,  " & vbNewLine
                        SQL = SQL & "           isnull(produto_tb.processa_cobranca,'n') as cobranca, "
                        SQL = SQL & "           NULL endosso_id,                                                                                                           " & vbNewLine
                        SQL = SQL & "           NULL dt_pedido_endosso,                                                                                                    " & vbNewLine
                        SQL = SQL & "           NULL num_endosso_bb,                                                                                                       " & vbNewLine
                        SQL = SQL & "           NULL endosso_id,                                                                                                           " & vbNewLine
                        SQL = SQL & "           cliente_tb.nome,                                                                                                           " & vbNewLine
                        SQL = SQL & "           proposta_tb.dt_contratacao,                                                                                                " & vbNewLine
                        SQL = SQL & "           proposta_tb.prop_cliente_id,                                                                                               " & vbNewLine
                        SQL = SQL & "           proposta_tb.situacao,      " & vbNewLine
                        SQL = SQL & "           descr_situacao = CASE proposta_tb.situacao                                                                                 " & vbNewLine
                        SQL = SQL & "                              WHEN 'e' THEN 'Em Estudo'                                                                               " & vbNewLine
                        SQL = SQL & "                              WHEN 'a' THEN 'Aceite'                                                                                  " & vbNewLine
                        SQL = SQL & "                              WHEN 'c' THEN 'Cancelada'                                                                               " & vbNewLine
                        SQL = SQL & "                              WHEN 'r' THEN 'Recusada'                                                                                " & vbNewLine
                        SQL = SQL & "                              WHEN 'i' THEN CASE (    SELECT COUNT(1)                                                                 " & vbNewLine    'alterado count(*)para count(1)12/09/2013 projeto 17860335 - Melhorias no SEGBR MC_Amaral -confitec
                        SQL = SQL & "                                                        FROM proposta_suspensa_tb WITH(NOLOCK)                                        " & vbNewLine
                        SQL = SQL & "                                                  INNER JOIN proposta_tb  WITH (NOLOCK)                                               " & vbNewLine
                        SQL = SQL & "                                                          ON proposta_tb.proposta_id = proposta_suspensa_tb.proposta_id               " & vbNewLine
                        SQL = SQL & "                                                       WHERE proposta_suspensa_tb.dt_inicio_suspensao <= '20050722'                   " & vbNewLine
                        SQL = SQL & "                                                         AND (proposta_suspensa_tb.dt_fim_suspensao IS NULL                           " & vbNewLine
                        SQL = SQL & "                                                          OR (proposta_suspensa_tb.dt_fim_suspensao is NOT NULL                       " & vbNewLine
                        SQL = SQL & "                                                              AND  proposta_suspensa_tb.cod_suspensao = '1')))                        " & vbNewLine
                        SQL = SQL & "                                              WHEN 0 THEN 'Ap�lice Emitida'                                                           " & vbNewLine
                        SQL = SQL & "                                              ELSE 'Cancelada'                                                                        " & vbNewLine
                        SQL = SQL & "                                            END                                                                                       " & vbNewLine
                        SQL = SQL & "                              WHEN 'p' THEN 'Em Estudo (erro de cadastramento)'                                                       " & vbNewLine
                        SQL = SQL & "                              WHEN 's' THEN 'Carta Recusa Emitida'                                                                    " & vbNewLine
                        SQL = SQL & "                            END,                                                                                                      " & vbNewLine
                        SQL = SQL & "           NULL tp_emissao,                                                                                                           " & vbNewLine
                        SQL = SQL & "           NULL nome_arquivo                                                                                                          " & vbNewLine
                        'Rmarins - 05/01/2006 - Inclus�o do campo seg_cliente ''''''''''''''''''''''''''''''''''''''''''''''''
                        SQL = SQL & ",          seg_cliente = CASE cliente_tb.seg_cliente                                                                                  " & vbNewLine
                        SQL = SQL & "                           WHEN 'private' THEN 'PRIVATE'                                                                              " & vbNewLine
                        SQL = SQL & "                           WHEN 'estilo' THEN 'ESTILO'                                                                                " & vbNewLine
                        SQL = SQL & "                           ELSE ' '                                                                                                   " & vbNewLine
                        SQL = SQL & "                         END                                                                                                          " & vbNewLine

                        SQL = SQL & "      FROM proposta_tb  WITH (NOLOCK)                                                                                                        " & vbNewLine
                        SQL = SQL & "INNER JOIN proposta_adesao_tb  WITH (NOLOCK)                                                                                                 " & vbNewLine
                        SQL = SQL & "        ON proposta_adesao_tb.proposta_id = proposta_tb.proposta_id                                                                   " & vbNewLine
                        SQL = SQL & "INNER JOIN produto_tb  WITH (NOLOCK)                                                                                                         " & vbNewLine
                        SQL = SQL & "        ON produto_tb.produto_id = proposta_tb.produto_id                                                                             " & vbNewLine
                        SQL = SQL & "INNER JOIN arquivo_produto_tb  WITH (NOLOCK)                                                                                                 " & vbNewLine
                        SQL = SQL & "        ON arquivo_produto_tb.produto_id = produto_tb.produto_id                                                                      " & vbNewLine
                        SQL = SQL & "INNER JOIN cliente_tb  WITH (NOLOCK)                                                                                                         " & vbNewLine
                        SQL = SQL & "        ON cliente_tb.cliente_id = proposta_tb.prop_cliente_id                                                                        " & vbNewLine
                        SQL = SQL & "INNER JOIN ramo_tb  WITH (NOLOCK)                                                                                                            " & vbNewLine
                        SQL = SQL & "        ON ramo_tb.ramo_id = proposta_tb.ramo_id                                                                                      " & vbNewLine
                        'SQL = SQL & "     WHERE proposta_tb.proposta_id = " & txtArgumento.Text & vbNewLine
                        SQL = SQL & "     WHERE  proposta_tb.proposta_id in (" & proposta_id_bb & ")" & vbNewLine
                        SQL = SQL & "       AND proposta_tb.produto_id = 714                                                                                               " & vbNewLine
                        SQL = SQL & "       AND proposta_tb.situacao<>'t'                                                                                                   " & vbNewLine

                        SQL = SQL & "  ORDER BY proposta_tb.dt_contratacao                                                                                                " & vbNewLine
                    End If
                End If
            End If

            rc2.Close
            Set rc2 = Nothing
        Else


            ' lpinto 21/09/2005 - Consulta proposta recusada - proposta_adesao_tb
            'If Not bPropostaRecusada And Not bPropostaEstudo Then 'rmaiellaro (19/01/2007)
            'alterado And vProdutoId = 714 -11/09/2013 projeto 17860335 - Melhorias no SEGBR MC_Amaral -confitec
            If Not bPropostaRecusada And Not bPropostaEstudo And (IIf(IsNull(vProdutoId), "", vProdutoId)) = 714 And UCase(SIT) = "A" Then    'rmaiellaro (19/01/2007)
                SQL = ""
                SQL = SQL & "       SELECT a.apolice_id" & vbNewLine
                SQL = SQL & "            , a.ramo_id" & vbNewLine
                SQL = SQL & "            , r.nome nm_ramo" & vbNewLine
                SQL = SQL & "            , a.seguradora_cod_susep" & vbNewLine
                SQL = SQL & "            , a.sucursal_seguradora_id  " & vbNewLine
                SQL = SQL & "            , pf.proposta_bb, p.proposta_id" & vbNewLine
                SQL = SQL & "            , p.produto_id" & vbNewLine
                SQL = SQL & "            , pR.nome  AS nome_prod  " & vbNewLine
                SQL = SQL & "            , isnull(pr.processa_cobranca,'n') as cobranca "
                SQL = SQL & "            , e.endosso_id " & vbNewLine
                SQL = SQL & "            , e.dt_pedido_endosso" & vbNewLine
                SQL = SQL & "            , e.num_endosso_bb" & vbNewLine
                SQL = SQL & "            , e.tp_endosso_id" & vbNewLine
                SQL = SQL & "            , c.nome " & vbNewLine
                SQL = SQL & "            , p.dt_contratacao" & vbNewLine
                SQL = SQL & "            , p.prop_cliente_id " & vbNewLine
                SQL = SQL & "            , P.situacao      " & vbNewLine
                SQL = SQL & "            , descr_situacao = case p.situacao " & vbNewLine
                SQL = SQL & "                               when 'e' then 'Em Estudo' " & vbNewLine
                SQL = SQL & "                               when 'a' then 'Aceite' " & vbNewLine
                SQL = SQL & "                               when 'c' then 'Cancelada' " & vbNewLine
                SQL = SQL & "                               when 'r' then 'Recusada' " & vbNewLine
                'SQL = SQL & "           when 'i' then 'Ap�lice Emitida' "
                'bcarneiro - 05/11/2003 - Vida Vida - Novas Funcionalidades ''''''''''''''''
                SQL = SQL & "                               when 'i' then " & vbNewLine
                SQL = SQL & "                                       case (select count(1)" & vbNewLine
                SQL = SQL & "                                               from proposta_suspensa_tb s WITH(NOLOCK)" & vbNewLine
                SQL = SQL & "                                              where p.proposta_id = s.proposta_id " & vbNewLine
                'SQL = SQL & "                and s.dt_inicio_suspensao >= '" & Format(Data_Sistema, "yyyymmdd") & "'"
                SQL = SQL & "                                                and s.dt_inicio_suspensao <= '" & Format(Data_Sistema, "yyyymmdd") & "'" & vbNewLine
                SQL = SQL & "                                                and (s.dt_fim_suspensao is null " & vbNewLine
                SQL = SQL & "                                                 or (s.dt_fim_suspensao is not null " & vbNewLine
                SQL = SQL & "                                                and  s.cod_suspensao = '1')))" & vbNewLine
                SQL = SQL & "                                               when 0 then 'Ap�lice Emitida' " & vbNewLine
                'Alessandra Grig�rio - 25/06/2004 - Atendendo solic. Marcio Yoshimura
                'SQL = SQL & "                    else 'Suspensa'        "
                SQL = SQL & "                                               else 'Cancelada'       " & vbNewLine
                'FIM
                SQL = SQL & "                                                end " & vbNewLine
                ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                SQL = SQL & "                               when 'p' then 'Em Estudo (erro de cadastramento)' " & vbNewLine
                SQL = SQL & "                               when 's' then 'Carta Recusa Emitida' " & vbNewLine
                SQL = SQL & "                                end, ap.tp_emissao, NULL nome_arquivo " & vbNewLine

                'Rmarins - 05/01/2006 - Inclus�o do campo seg_cliente '''''''''''''''''''''''''''''''
                SQL = SQL & "              ,  seg_cliente = CASE c.seg_cliente    " & vbNewLine
                SQL = SQL & "                               WHEN 'private' THEN 'PRIVATE'  " & vbNewLine
                SQL = SQL & "                               WHEN 'estilo' THEN 'ESTILO'    " & vbNewLine
                SQL = SQL & "                               ELSE ' '   " & vbNewLine
                SQL = SQL & "                                END  " & vbNewLine

                SQL = SQL & "           FROM  proposta_tb p  WITH (NOLOCK) " & vbNewLine
                SQL = SQL & "               , certificado_tb a  WITH (NOLOCK) " & vbNewLine
                SQL = SQL & "               , endosso_tb e  WITH (NOLOCK) " & vbNewLine
                SQL = SQL & "               , proposta_adesao_tb pf  WITH (NOLOCK) " & vbNewLine
                SQL = SQL & "               , cliente_tb c  WITH (NOLOCK) " & vbNewLine
                SQL = SQL & "               , ramo_tb r  WITH (NOLOCK) " & vbNewLine
                SQL = SQL & "               , apolice_tb ap  WITH (NOLOCK)  " & vbNewLine
                SQL = SQL & "               , produto_tb pr  WITH (NOLOCK)  " & vbNewLine
                'SQL = SQL & "           WHERE p.proposta_id = " & txtArgumento & vbNewLine
                SQL = SQL & "           WHERE  p.proposta_id in (" & proposta_id_bb & ")" & vbNewLine
                SQL = SQL & "             AND a.proposta_id = p.proposta_id " & vbNewLine
                SQL = SQL & "             AND pf.proposta_id = p.proposta_id " & vbNewLine
                SQL = SQL & "             AND e.proposta_id = p.proposta_id " & vbNewLine
                SQL = SQL & "             AND e.tp_endosso_id not in (10,100,101,200) " & vbNewLine
                SQL = SQL & "             AND c.cliente_id = p.prop_cliente_id " & vbNewLine
                SQL = SQL & "             AND r.ramo_id = a.ramo_id " & vbNewLine
                SQL = SQL & "             AND a.apolice_id = ap.apolice_id " & vbNewLine
                SQL = SQL & "             AND a.ramo_id = ap.ramo_id " & vbNewLine
                SQL = SQL & "             AND p.produto_id = 714 " & vbNewLine
                SQL = SQL & "             AND p.produto_id = pr.produto_id " & vbNewLine
                SQL = SQL & "             AND p.situacao<>'t'          " & vbNewLine
                'SQL = SQL & "   ORDER BY  p.dt_contratacao " & vbNewLine
                'AFONSO DUTRA NOGUEIRA FILHO - GPTI - DATA: 25/03/2010
                'Raimundo Belas da Silva Junior - GPTI - 13/04/2010
                'Acertando ORDER BY
                'Denis Silva  - CWI - 26/05/2010
                'Acertando ORDER BY - FLOW:3727253
                'SQL = SQL & " ORDER BY e.dt_pedido_endosso, /*e.dt_emissao,*/ p.dt_contratacao, e.endosso_id "
                SQL = SQL & " ORDER BY e.dt_emissao, /*e.dt_pedido_endosso,*/ p.dt_contratacao, e.endosso_id "

            Else
                'acrescentado And vProdutoId = 714 -11/09/2013 projeto 17860335 - Melhorias no SEGBR MC_Amaral -confitec
                If (IIf(IsNull(vProdutoId), "", vProdutoId)) = 714 And UCase(SIT) = "A" Then
                    SQL = ""
                    SQL = SQL & "    SELECT DISTINCT NULL apolice_id, --rmaiellaro (19/01/2007)                                                                                      " & vbNewLine
                    SQL = SQL & "           ramo_tb.ramo_id,                                                                                                           " & vbNewLine
                    SQL = SQL & "           ramo_tb.nome nm_ramo,                                                                                                      " & vbNewLine
                    SQL = SQL & "           produto_tb.seguradora_cod_susep,                                                                                           " & vbNewLine
                    SQL = SQL & "           arquivo_produto_tb.sucursal_seguradora_id ,                                                                                " & vbNewLine
                    SQL = SQL & "           proposta_adesao_tb.proposta_bb,                                                                                            " & vbNewLine
                    SQL = SQL & "           proposta_tb.proposta_id,                                                                                                   " & vbNewLine
                    SQL = SQL & "           proposta_tb.produto_id,                                                                                                    " & vbNewLine
                    SQL = SQL & "           produto_tb.nome  AS nome_prod,  " & vbNewLine
                    SQL = SQL & "           isnull(produto_tb.processa_cobranca,'n') as cobranca, "
                    SQL = SQL & "           NULL endosso_id,                                                                                                           " & vbNewLine
                    SQL = SQL & "           NULL dt_pedido_endosso,                                                                                                    " & vbNewLine
                    SQL = SQL & "           NULL num_endosso_bb,                                                                                                       " & vbNewLine
                    SQL = SQL & "           NULL endosso_id,                                                                                                           " & vbNewLine
                    SQL = SQL & "           cliente_tb.nome,                                                                                                           " & vbNewLine
                    SQL = SQL & "           proposta_tb.dt_contratacao,                                                                                                " & vbNewLine
                    SQL = SQL & "           proposta_tb.prop_cliente_id,                                                                                               " & vbNewLine
                    SQL = SQL & "           proposta_tb.situacao,      " & vbNewLine
                    SQL = SQL & "           descr_situacao = CASE proposta_tb.situacao                                                                                 " & vbNewLine
                    SQL = SQL & "                              WHEN 'e' THEN 'Em Estudo'                                                                               " & vbNewLine
                    SQL = SQL & "                              WHEN 'a' THEN 'Aceite'                                                                                  " & vbNewLine
                    SQL = SQL & "                              WHEN 'c' THEN 'Cancelada'                                                                               " & vbNewLine
                    SQL = SQL & "                              WHEN 'r' THEN 'Recusada'                                                                                " & vbNewLine
                    SQL = SQL & "                              WHEN 'i' THEN CASE (    SELECT COUNT(1)                                                                 " & vbNewLine    'alterado count(*)para count(1)12/09/2013 projeto 17860335 - Melhorias no SEGBR MC_Amaral -confitec
                    SQL = SQL & "                                                        FROM proposta_suspensa_tb WITH(NOLOCK)                                        " & vbNewLine
                    SQL = SQL & "                                                  INNER JOIN proposta_tb  WITH (NOLOCK)                                               " & vbNewLine
                    SQL = SQL & "                                                          ON proposta_tb.proposta_id = proposta_suspensa_tb.proposta_id               " & vbNewLine
                    SQL = SQL & "                                                       WHERE proposta_suspensa_tb.dt_inicio_suspensao <= '20050722'                   " & vbNewLine
                    SQL = SQL & "                                                         AND (proposta_suspensa_tb.dt_fim_suspensao IS NULL                           " & vbNewLine
                    SQL = SQL & "                                                          OR (proposta_suspensa_tb.dt_fim_suspensao is NOT NULL                       " & vbNewLine
                    SQL = SQL & "                                                              AND  proposta_suspensa_tb.cod_suspensao = '1')))                        " & vbNewLine
                    SQL = SQL & "                                              WHEN 0 THEN 'Ap�lice Emitida'                                                           " & vbNewLine
                    SQL = SQL & "                                              ELSE 'Cancelada'                                                                        " & vbNewLine
                    SQL = SQL & "                                            END                                                                                       " & vbNewLine
                    SQL = SQL & "                              WHEN 'p' THEN 'Em Estudo (erro de cadastramento)'                                                       " & vbNewLine
                    SQL = SQL & "                              WHEN 's' THEN 'Carta Recusa Emitida'                                                                    " & vbNewLine
                    SQL = SQL & "                            END,                                                                                                      " & vbNewLine
                    SQL = SQL & "           NULL tp_emissao,                                                                                                           " & vbNewLine
                    SQL = SQL & "           NULL nome_arquivo                                                                                                          " & vbNewLine
                    'Rmarins - 05/01/2006 - Inclus�o do campo seg_cliente ''''''''''''''''''''''''''''''''''''''''''''''''
                    SQL = SQL & ",          seg_cliente = CASE cliente_tb.seg_cliente                                                                                  " & vbNewLine
                    SQL = SQL & "                           WHEN 'private' THEN 'PRIVATE'                                                                              " & vbNewLine
                    SQL = SQL & "                           WHEN 'estilo' THEN 'ESTILO'                                                                                " & vbNewLine
                    SQL = SQL & "                           ELSE ' '                                                                                                   " & vbNewLine
                    SQL = SQL & "                         END                                                                                                          " & vbNewLine

                    SQL = SQL & "      FROM proposta_tb  WITH (NOLOCK)                                                                                                        " & vbNewLine
                    SQL = SQL & "INNER JOIN proposta_adesao_tb  WITH (NOLOCK)                                                                                                 " & vbNewLine
                    SQL = SQL & "        ON proposta_adesao_tb.proposta_id = proposta_tb.proposta_id                                                                   " & vbNewLine
                    SQL = SQL & "INNER JOIN produto_tb  WITH (NOLOCK)                                                                                                         " & vbNewLine
                    SQL = SQL & "        ON produto_tb.produto_id = proposta_tb.produto_id                                                                             " & vbNewLine
                    SQL = SQL & "INNER JOIN arquivo_produto_tb  WITH (NOLOCK)                                                                                                 " & vbNewLine
                    SQL = SQL & "        ON arquivo_produto_tb.produto_id = produto_tb.produto_id                                                                      " & vbNewLine
                    SQL = SQL & "INNER JOIN cliente_tb  WITH (NOLOCK)                                                                                                         " & vbNewLine
                    SQL = SQL & "        ON cliente_tb.cliente_id = proposta_tb.prop_cliente_id                                                                        " & vbNewLine
                    SQL = SQL & "INNER JOIN ramo_tb  WITH (NOLOCK)                                                                                                            " & vbNewLine
                    SQL = SQL & "        ON ramo_tb.ramo_id = proposta_tb.ramo_id                                                                                      " & vbNewLine
                    'SQL = SQL & "     WHERE proposta_tb.proposta_id = " & txtArgumento.Text & vbNewLine
                    SQL = SQL & "     WHERE  proposta_tb.proposta_id in (" & proposta_id_bb & ")" & vbNewLine
                    SQL = SQL & "       AND proposta_tb.produto_id = 714                                                                                               " & vbNewLine
                    SQL = SQL & "       AND proposta_tb.situacao<>'t'                                                                                                   " & vbNewLine

                    SQL = SQL & "  ORDER BY proposta_tb.dt_contratacao                                                                                                " & vbNewLine
                End If
            End If
        End If
    End If




    '********************************************************************
    '***FIM CONSULTA PROPOSTA_BB

    ' Montagem da query de sele��o
    '

    If SQL = "" Then
        StatusBar1.SimpleText = "Nenhuma proposta foi encontrada !"
        GridProposta.Rows = 1
        MousePointer = vbDefault
        Exit Sub
    End If



    If PesqNomeCliente Then
        sql1 = SQL
        Set rsAltRowCount = rdoCn1.OpenResultset("SET ROWCOUNT 101")
        Set rc = rdoCn1.OpenResultset(sql1, rdOpenStatic)
        Set rsRetRowCount = rdoCn1.OpenResultset("SET ROWCOUNT 0")
    Else
        Set rc = rdoCn2.OpenResultset(SQL)
    End If
    'modifica��o para retirar o ler produto
    'INICIO  'alterado 30/09/2013 projeto 17860335 - Melhorias no SEGBR MC_Amaral �confitec
    If Not rc.EOF Then
        If (Trim(vProdutoId) = "") Then
            vProdutoId = IIf(IsNull(rc!Produto_id), "", rc!Produto_id)
        End If

        If (Trim(vNomeProd) = "") Then
            vNomeProd = rc!Nome_prod
        End If

        If (Trim(ProcessaCobranca) = "") Then
            ProcessaCobranca = rc!Cobranca
        End If

        If (Trim(vSituacao) = "") Then
            vSituacao = rc!Situacao
        End If

        If (Trim(vorigem_proposta_id) = "") Then
            vorigem_proposta_id = rc!origem_proposta_id
            If vorigem_proposta_id = 2 Then
                bPropostaALS = True
            Else
                bPropostaALS = False
            End If
        End If
    End If
    'INICIO  'alterado 30/09/2013 projeto 17860335 - Melhorias no SEGBR MC_Amaral �confitec
    If rc.EOF Then
        StatusBar1.SimpleText = "Nenhuma proposta foi encontrada !"
        GridProposta.Rows = 1
    Else

        'Lpinto - 30/09/2005
        ConPropBasicaVida.sPropostaBB = IIf(IsNull(rc("proposta_bb")), "", rc("proposta_bb"))
        ConPropBasicaVida.sArquivo = IIf(IsNull(rc("nome_arquivo")), "", rc("nome_arquivo"))
        StatusBar1.SimpleText = ""

        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        'carolina.marinho 24/10/2011 - Demanda 11938655
        'Criar ferramenta para identificar exist�ncia de subgrupo (rp) - Confitec RJ
        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        'If Not (PesqNomeCliente Or PesqCodCliente) Then
        If Not (PesqNomeCliente Or PesqCodCliente) And cboTipoSelecao.Text <> "CNPJ" And cboTipoSelecao.Text <> "Raz�o Social" Then
            'carolina.marinho fim
            If (IsNull(rc!tp_emissao)) Then
                gTpEmissao = " "
            Else
                gTpEmissao = UCase(rc!tp_emissao)
            End If
            If gTpEmissao = "A" Then
                CosseguroAceito = True
            Else
                CosseguroAceito = False
            End If
        End If
        If GridProposta.Visible Then
            Carrega_GridProposta_Geral rc
        Else
            Carrega_GridCliente_Cliente rc
        End If

        StatusBar1.SimpleText = StatusBar1.SimpleText & " - Clique 2 vezes na proposta desejada para mais informa��es"
    End If
    '
    MousePointer = vbDefault
    '
        ' Guilherme Cruz -- Confitec Sistemas -- Produto 1183 -- Sala �gil
        If vProdutoId <> "" Then 'Thalita - IM01067132 - 16/12/2019
            If vProdutoId = 1183 Then
                apolice = apolice_pesq
                Seguradora = seguradora_pesq
                Sucursal = sucursal_pesq
                Ramo = ramo_pesq
            End If
        End If

    Exit Sub
    Resume
Erro:
    TrataErroGeral "Consulta Proposta Vida"
    Resume
    MousePointer = vbDefault

End Sub

Private Sub Carrega_GridProposta_Geral(rc As rdoResultset)

    Dim linha As String
    Dim proposta_id As String, apolice_id As String
    Dim rc_apoio As rdoResultset
    Dim CriaRegOriginal As Boolean

    GridProposta.Rows = 1
    'Lpinto - 28/09/2005
    If Not IsNull(rc!proposta_id) Then
        proposta_id = rc!proposta_id
        apolice_id = IIf(IsNull(rc!apolice_id), "", rc!apolice_id)
        If IsNull(vNomeProd) Then
            vNomeProd = IIf(IsNull(rc!Nome_prod), "", rc!Nome_prod)
        End If

        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        'carolina.marinho 24/10/2011 - Demanda 11938655
        'Criar ferramenta para identificar exist�ncia de subgrupo (rp) - Confitec RJ
        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        If cboTipoSelecao.Text <> "CNPJ" And cboTipoSelecao.Text <> "Raz�o Social" Then
            CriaRegOriginal = True
        End If
        'carolina.marinho fim

    End If    '

    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    'carolina.marinho 24/10/2011 - Demanda 11938655
    'Criar ferramenta para identificar exist�ncia de subgrupo (rp) - Confitec RJ
    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

    Dim sCNPJ As String
    Dim sNomeSubGrupo As String
    Dim iSubGrupo As Integer


    While Not rc.EOF

        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        'carolina.marinho 24/10/2011 - Demanda 11938655
        'Criar ferramenta para identificar exist�ncia de subgrupo (rp) - Confitec RJ
        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        If cboTipoSelecao.Text = "CNPJ" Or cboTipoSelecao.Text = "Raz�o Social" Then

            If sCNPJ = Trim(rc!CPF_CNPJ) And sNomeSubGrupo = Trim(rc!Nome) And iSubGrupo = Trim(rc!sub_grupo_id) Then

                rc.MoveNext

            Else

                linha = " " & rc!apolice_id & " (Ap�s endos. " & rc!endosso_id & ")"
                linha = linha & vbTab & rc!ramo_id
                linha = linha & vbTab & Trim(rc!nm_ramo)
                linha = linha & vbTab & rc!Produto_id
                linha = linha & vbTab & rc!prop_cliente_id
                linha = linha & vbTab & Trim(rc!Nome_prod)
                linha = linha & vbTab & Format(rc!dt_contratacao, "dd/mm/yyyy")
                linha = linha & vbTab & Trim(rc!descr_situacao)
                linha = linha & vbTab & IIf(IsNull(rc!proposta_bb), "", rc!proposta_bb)
                linha = linha & vbTab & IIf(IsNull(rc!endosso_id), "", rc!endosso_id)
                linha = linha & vbTab & IIf(IsNull(rc!dt_pedido_endosso), "", Format(rc!dt_pedido_endosso, "dd/mm/yyyy"))
                linha = linha & vbTab & IIf(IsNull(rc!num_endosso_bb), "", rc!num_endosso_bb)
                linha = linha & vbTab & IIf(IsNull(rc!tp_endosso_id), "", rc!tp_endosso_id)
                'Rmarins - 05/01/2006 - Inclus�o do campo seg_cliente
                linha = linha & vbTab & rc!seg_cliente

                linha = linha & vbTab & vbTab & rc!CPF_CNPJ
                linha = linha & vbTab & rc!Nome
                linha = linha & vbTab & rc!ramo_id & "/" & rc!apolice_id
                linha = linha & vbTab & rc!sub_grupo_id
                GridProposta.ColWidth(15) = 1500
                GridProposta.ColWidth(16) = 4875

                GridProposta.AddItem linha
                GridProposta.RowData(GridProposta.Rows - 1) = rc!proposta_id

                sCNPJ = Trim(rc!CPF_CNPJ)
                sNomeSubGrupo = Trim(rc!Nome)
                iSubGrupo = Trim(rc!sub_grupo_id)

                rc.MoveNext

            End If

        Else


            '' Ap�lice n�o tem endosso, registro �nico
            If IsNull(rc!endosso_id) Then

                'GridProposta.Col = GRD_SEL_APOL
                'GridProposta.Row = GridProposta.Rows - 1
                '
                'If InStr(GridProposta.Text, "Ap�s") > 0 Then
                '    GridProposta.Text = Left(GridProposta.Text, Len(GridProposta.Text) - 1) & ", Atual)"
                'End If
                '
                'lpinto 27/09/2005
                If apolice_id = "" Then
                    linha = ""
                Else
                    linha = " " & rc!apolice_id & " (Atual)"
                End If

                linha = linha & vbTab & rc!ramo_id
                linha = linha & vbTab & Trim(rc!nm_ramo)
                linha = linha & vbTab & rc!Produto_id
                linha = linha & vbTab & rc!prop_cliente_id
                linha = linha & vbTab & Trim(rc!Nome)
                linha = linha & vbTab & Format(rc!dt_contratacao, "dd/mm/yyyy")
                linha = linha & vbTab & Trim(rc!descr_situacao)
                linha = linha & vbTab & IIf(IsNull(rc!proposta_bb), "", rc!proposta_bb)
                linha = linha & vbTab & ""                                'endosso
                linha = linha & vbTab & ""                                'dt_endosso
                linha = linha & vbTab & ""                                'num_endosso_bb
                linha = linha & vbTab & ""                                'tipo_end
                'Rmarins - 05/01/2006 - Inclus�o do campo seg_cliente
                linha = linha & vbTab & rc!seg_cliente

                GridProposta.AddItem linha
                'Identifica��o da linha do grid
                If Not IsNull(rc!proposta_id) Then
                    GridProposta.RowData(GridProposta.Rows - 1) = rc!proposta_id
                End If
                'GridProposta.Col = GRD_SEL_APOL
                'GridProposta.Row = GridProposta.Rows - 1
                'GridProposta.CellAlignment = flexAlignLeftCenter

                'If rc!endosso_id = 1 Then
                '    GridProposta.Text = rc!apolice_id & " (Original)"
                'End If

                'End If

                'If Not IsNull(rc!endosso_id) Then
            Else
                '' Ap�lice tem endossos
                '' Como o endosso '0' n�o vem na query, criar 1a. linha
                If CriaRegOriginal Then
                    linha = " " & rc!apolice_id & " (Original)"
                    linha = linha & vbTab & rc!ramo_id
                    linha = linha & vbTab & Trim(rc!nm_ramo)
                    linha = linha & vbTab & rc!Produto_id
                    linha = linha & vbTab & rc!prop_cliente_id
                    linha = linha & vbTab & Trim(rc!Nome)
                    linha = linha & vbTab & Format(rc!dt_contratacao, "dd/mm/yyyy")
                    linha = linha & vbTab & Trim(rc!descr_situacao)
                    linha = linha & vbTab & IIf(IsNull(rc!proposta_bb), "", rc!proposta_bb)
                    linha = linha & vbTab & ""                              'endosso
                    linha = linha & vbTab & ""                              'dt_endosso
                    linha = linha & vbTab & ""                              'num_endosso_bb
                    linha = linha & vbTab & ""                              'tipo_end
                    'Rmarins - 05/01/2006 - Inclus�o do campo seg_cliente
                    linha = linha & vbTab & rc!seg_cliente
                    '

                    GridProposta.AddItem linha
                    GridProposta.RowData(GridProposta.Rows - 1) = rc!proposta_id
                    '
                    CriaRegOriginal = False
                End If
                '
                linha = " " & rc!apolice_id & " (Ap�s endos. " & rc!endosso_id & ")"
                linha = linha & vbTab & rc!ramo_id
                linha = linha & vbTab & Trim(rc!nm_ramo)
                linha = linha & vbTab & rc!Produto_id
                linha = linha & vbTab & rc!prop_cliente_id
                linha = linha & vbTab & Trim(rc!Nome)
                linha = linha & vbTab & Format(rc!dt_contratacao, "dd/mm/yyyy")
                linha = linha & vbTab & Trim(rc!descr_situacao)
                linha = linha & vbTab & IIf(IsNull(rc!proposta_bb), "", rc!proposta_bb)
                linha = linha & vbTab & IIf(IsNull(rc!endosso_id), "", rc!endosso_id)
                linha = linha & vbTab & IIf(IsNull(rc!dt_pedido_endosso), "", Format(rc!dt_pedido_endosso, "dd/mm/yyyy"))
                linha = linha & vbTab & IIf(IsNull(rc!num_endosso_bb), "", rc!num_endosso_bb)
                linha = linha & vbTab & IIf(IsNull(rc!tp_endosso_id), "", rc!tp_endosso_id)
                'Rmarins - 05/01/2006 - Inclus�o do campo seg_cliente
                linha = linha & vbTab & rc!seg_cliente


                '
                GridProposta.AddItem linha
                GridProposta.RowData(GridProposta.Rows - 1) = rc!proposta_id

                'GridProposta.Col = GRD_SEL_APOL
                'GridProposta.Row = GridProposta.Rows - 1
                'GridProposta.CellAlignment = flexAlignLeftCenter
            End If

            rc.MoveNext


        End If    'carolina.marinho

    Wend


    '*** Inclus�o do Sinalizado de PPE - Pessoas politicamente expostas - demanda 269994
    '*** 23/09/2008 - Fabio (Stefanini)
    For Idx = 1 To (GridProposta.Rows - 1)
        If IdentificaPPE(GridProposta.TextMatrix(Idx, 1)) Then
            GridProposta.TextMatrix(Idx, 14) = "Sim"
        Else
            GridProposta.TextMatrix(Idx, 14) = "N�o"
        End If
    Next

    rc.Close
    '
    Set rc = Nothing

    GridProposta.col = 0
    GridProposta.Row = GridProposta.Rows - 1
    GridProposta.CellAlignment = flexAlignLeftCenter
    '
    'If InStr(GridProposta.Text, "Atual") = 0 Then
    '    GridProposta.Text = Left(GridProposta.Text, Len(GridProposta.Text) - 1) & ", Atual)"
    'End If

    'lpinto 27/09/2005
    If apolice_id = "" Then
        GridProposta.Text = ""
    Else
        GridProposta.Text = " " & apolice_id & " (Atual)"
    End If
    '
    GridProposta.col = 0
    GridProposta.Row = 1

    If GridProposta.Rows = 2 Then

        'produto_externo_id = aux_prod_ext
        '
        ' Jos� Moreira (Nova Consultoria) - 10/11/2012 - 14620257 - Melhorias no processo de subscri��o
        ' Permite encerrar a aplica��o quando a proposta � aberta via passagem de par�metros
        ConPropBasicaVida.PodeFecharPrograma = AbrirProposta
        ConPropBasicaVida.Show    ' PRODUTOS PERSONALIZADOS
        '
        Me.Hide

        ' Jos� Moreira (Nova Consultoria) - 10/11/2012 - 14620257 - Melhorias no processo de subscri��o
        ' Garantir que a proposta ser� aberta somente na 1� execu��o
        AbrirProposta = False

    Else
        GridProposta.HighLight = flexHighlightAlways
    End If

End Sub

Private Sub Carrega_GridCliente_Cliente(rc As rdoResultset)

    Dim rc_apoio As rdoResultset
    Dim linha As String, primeira_col As String, proposta_id As String
    Dim Aux_Produto_Externo As String
    Dim qtd_clientes As Integer
    Dim SaidoLoop As Boolean
    Dim sPropostaBB As String


    GridCliente.Rows = 1
    qtd_clientes = 1
    SaidoLoop = False

    DoEvents

    While Not rc.EOF

        If (Not SaidoLoop) And (PesqNomeCliente) Then

            StatusBar1.SimpleText = "Recuperando os clientes selecionados ..."

            linha = rc!Nome & vbTab
            linha = linha & rc!prop_cliente_id & vbTab
            linha = linha & rc!proposta_id & " (Atual)" & vbTab
            linha = linha & rc!Produto_id & vbTab
            linha = linha & Format(rc!dt_contratacao, "dd/mm/yyyy") & vbTab
            linha = linha & rc!descr_situacao & vbTab
            '           linha = linha & rc!prop_cliente_id & vbTab
            '           linha = linha & " "

            'Rmarins - 05/01/2006 - Inclus�o do campo seg_cliente
            linha = linha & rc!seg_cliente

            GridCliente.AddItem linha

            'Lpinto - 28/09/2005
            If GridCliente.Rows = 2 Then
                If GridCliente.TextMatrix(1, 5) = "N�o emitida" Then
                    sPropostaBB = rc("proposta_bb")
                    ConPropBasicaVida.sPropostaBB = rc("proposta_bb")
                End If
            Else
                If GridCliente.TextMatrix(GridCliente.Row, 5) = "N�o emitida" Then
                    sPropostaBB = rc("proposta_bb")
                    ConPropBasicaVida.sPropostaBB = rc("proposta_bb")
                End If
            End If

            GridCliente.RowData(GridCliente.Rows - 1) = IIf(IsNull(rc!proposta_id), 0, rc!proposta_id)
            rc.MoveNext

            GridCliente.col = 0
            GridCliente.Row = GridProposta.Rows - 1
            GridCliente.CellAlignment = 1

            qtd_clientes = qtd_clientes + 1
            If qtd_clientes > 100 Then
                MsgBox "Estao sendo exibidos apenas os 100 primeiros clientes encontrados."
                SaidoLoop = True
                GoTo Continua
            End If

            DoEvents

        ElseIf Not PesqNomeCliente Then

            '           linha = linha & rc!prop_cliente_id & vbTab
            linha = rc!prop_cliente_id & vbTab
            linha = linha & rc!proposta_id & " (Atual)" & vbTab
            linha = linha & rc!Produto_id & vbTab
            linha = linha & rc!Nome & vbTab
            linha = linha & Format(rc!dt_contratacao, "dd/mm/yyyy") & vbTab
            linha = linha & rc!descr_situacao & vbTab
            '            linha = linha & rc!prop_cliente_id & vbTab
            '            linha = linha & " "

            'Rmarins - 05/01/2006 - Inclus�o do campo seg_cliente
            linha = linha & rc!seg_cliente

            GridCliente.AddItem linha

            GridCliente.RowData(GridCliente.Rows - 1) = rc!proposta_id

            rc.MoveNext

            GridCliente.col = 0
            GridCliente.Row = GridProposta.Rows - 1
            GridCliente.CellAlignment = 1

        End If

    Wend

Continua:

    rc.Close

    qtd_clientes = GridCliente.Rows - 1

    GridCliente.col = 0
    GridCliente.Row = 1

    If GridCliente.Rows = 2 Then


        ''produto_externo_id = Aux_Produto_Externo
        '
        'ConPropBasicaPersonalizado.Show ' PRODUTOS PERSONALIZADOS

        'Me.Hide
        ' Lpinto - 27/09/2005
        If GridCliente.TextMatrix(GridCliente.Row, 5) = "N�o emitida" Then
            Call Monta_GridProposta_Cliente(GridCliente.RowData(1), sPropostaBB)
        Else
            Call Monta_GridProposta_Cliente(GridCliente.RowData(1), "")
        End If

        GridCliente.HighLight = flexHighlightAlways
    End If

    StatusBar1.SimpleText = "Foram recuperados " & qtd_clientes & " clientes"

End Sub


Private Sub Form_Unload(Cancel As Integer)
    cmdCanc_Click
End Sub

Private Sub GridProposta_DblClick()

    If GridProposta.Rows > 1 Then
        If IsNumeric(GridProposta.TextMatrix(GridProposta.Row, 9)) Then
            gEndossoId = GridProposta.TextMatrix(GridProposta.Row, 9)
        End If
        '
        StatusBar1.SimpleText = "Carregando Ap�lice......."
        MousePointer = vbHourglass
        '
        ' Jos� Moreira (Nova Consultoria) - 10/11/2012 - 14620257 - Melhorias no processo de subscri��o
        ' Ao fechar proposta, volta para a tela de busca normalmente
        ConPropBasicaVida.PodeFecharPrograma = False
        ConPropBasicaVida.Show
        '
        MousePointer = vbDefault
        StatusBar1.SimpleText = ""
        '
        Me.Hide
    End If


End Sub

Private Sub GridCliente_DblClick()

    If GridCliente.Rows > 1 Then

        ''produto_externo_id = GridCliente.TextMatrix(GridCliente.Row, GridCliente.Cols - 1)

        ''If Val(produto_externo_id) = 109 Or Val(produto_externo_id) = 110 Then
        ''    ConPropBasica.Show
        ''ElseIf Val(produto_externo_id) = 709 Or Val(produto_externo_id) = 710 Or _
         ''       Val(produto_externo_id) = 8 Then
        ''    ConPropBasicaParametrizado.Show
        ''Else
        ''ConPropBasicaPersonalizado.Show
        ''End If
        ''Me.Hide
        ' Comentado em 20/06 - N�o chamar a tela de propostas
        ' Apresentar os endossos da ap�lice antes
        'ConPropBasicaPersonalizado.Show
        'Me.Hide
        Monta_GridProposta_Cliente (GridCliente.RowData(GridCliente.Row))

    End If

End Sub


Private Sub txtArgumento_KeyPress(KeyAscii As Integer)

    If KeyAscii = 13 Then
        cmdSelec_Click
    End If

End Sub

'*** Inclus�o do Sinalizado de PPE - Pessoas politicamente expostas - demanda 269994
'*** 23/09/2008 - Fabio (Stefanini)

Private Function IdentificaPPE(propostaId As String) As Boolean

'Sessao de variaveis
    Dim strSql As String
    Dim rc As rdoResultset

    'Manipulador de erros
    On Error GoTo ErrIdentificaPPE

    'Valor Padrao
    IdentificaPPE = False

    'Busca Identificar se faz parte do PPE
    strSql = "SELECT COUNT(1) AS TOTAL "    'alterado count(*)para count(1)12/09/2013 projeto 17860335 - Melhorias no SEGBR MC_Amaral -confitec
    strSql = strSql & " FROM seguros_db..PPE_ORGAO_CARGO_TB as TPPE WITH(NOLOCK) "
    strSql = strSql & " INNER JOIN seguros_db.DBO.pessoa_fisica_tb AS TPF WITH(NOLOCK) ON CAST(TPPE.CPF AS BIGINT) = CAST(TPF.CPF AS BIGINT)"
    strSql = strSql & " INNER JOIN seguros_db.DBO.CLIENTE_tb AS TCLI  WITH(NOLOCK) ON TCLI.CLIENTE_ID = TPF.PF_CLIENTE_ID"
    strSql = strSql & " INNER JOIN seguros_db.DBO.proposta_tb AS TPRO WITH(NOLOCK) ON TPRO.PROP_CLIENTE_ID = TCLI.CLIENTE_ID"
    strSql = strSql & " WHERE PROPOSTA_ID = " & propostaId
    strSql = strSql & " AND (TPPE.DT_EXCLUSAO_PPE IS NULL OR  TPPE.DT_EXCLUSAO_PPE >= GETDATE())"

    'Executa o Comando
    Set rc = rdocn.OpenResultset(strSql)

    'Verifica se voltou registro
    If rc.EOF Then GoTo Finaliza

    'Verifica Valor Valido
    If IsNull(rc!TOTAL) Then GoTo Finaliza

    'vERIFICA nao ENCONTROU
    If CInt(rc!TOTAL) <= 0 Then GoTo Finaliza

    'Valor Padrao
    IdentificaPPE = True

    'Finaliza
Finaliza:
    rc.Close
    Set rc = Nothing

    'Sai
    Exit Function

    'Manipulador de erros
ErrIdentificaPPE:

    'Fecha Recordsrt
    rc.Close
    Set rc = Nothing

End Function



