VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Administrador"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
'local variable(s) to hold property value(s)
Private mvarId As String    'local copy
Private mvarNome As String    'local copy
Private mvarPercProLabore As Double    'local copy
'local variable(s) to hold property value(s)
Private mvarIdEstipulante As String    'local copy
'local variable(s) to hold property value(s)
Private mvarDtIniAdministracao As String    'local copy
'local variable(s) to hold property value(s)
Private mvarDtFimAdministracao As String    'local copy

Public Property Let DtFimAdministracao(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.DtFimAdministracao = 5
    mvarDtFimAdministracao = vData
End Property

Public Property Get DtFimAdministracao() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.DtFimAdministracao
    DtFimAdministracao = mvarDtFimAdministracao
End Property

Public Property Let DtIniAdministracao(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.DtIniAdministracao = 5
    mvarDtIniAdministracao = vData
End Property

Public Property Get DtIniAdministracao() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.DtIniAdministracao
    DtIniAdministracao = mvarDtIniAdministracao
End Property

Public Property Let IdEstipulante(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.IdEstipulante = 5
    mvarIdEstipulante = vData
End Property

Public Property Get IdEstipulante() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.IdEstipulante
    IdEstipulante = mvarIdEstipulante
End Property

Public Property Let PercProLabore(ByVal vData As Double)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.PercProLabore = 5
    mvarPercProLabore = vData
End Property

Public Property Get PercProLabore() As Double
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.PercProLabore
    PercProLabore = mvarPercProLabore
End Property

Public Property Let Nome(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Nome = 5
    mvarNome = vData
End Property

Public Property Get Nome() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Nome
    Nome = mvarNome
End Property

Public Property Let Id(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Id = 5
    mvarId = vData
End Property

Public Property Get Id() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Id
    Id = mvarId
End Property
