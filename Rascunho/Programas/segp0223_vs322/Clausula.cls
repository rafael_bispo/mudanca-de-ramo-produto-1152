VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Clausula"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
'local variable(s) to hold property value(s)
Private mvarId As Integer    'local copy
Private mvarDescricao As String    'local copy
'local variable(s) to hold property value(s)
Private mvarNome As String    'local copy
'local variable(s) to hold property value(s)
Private mvarCodigo As Long    'local copy
Private mvardtIniVigencia As Date    'local copy
'local variable(s) to hold property value(s)
Private mvarDtFimVigencia As Date    'local copy
Public Property Let DtFimVigencia(ByVal vData As Date)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.DtFimVigencia = 5
    mvarDtFimVigencia = vData
End Property


Public Property Get DtFimVigencia() As Date
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.DtFimVigencia
    DtFimVigencia = mvarDtFimVigencia
End Property



Public Property Let DtIniVigencia(ByVal vData As Date)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.dtIniVigencia = 5
    mvardtIniVigencia = vData
End Property


Public Property Get DtIniVigencia() As Date
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.dtIniVigencia
    DtIniVigencia = mvardtIniVigencia
End Property



Public Property Let Codigo(ByVal vData As Long)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Codigo = 5
    mvarCodigo = vData
End Property


Public Property Get Codigo() As Long
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Codigo
    Codigo = mvarCodigo
End Property



Public Property Let Nome(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Nome = 5
    mvarNome = vData
End Property


Public Property Get Nome() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Nome
    Nome = mvarNome
End Property



Public Property Let Descricao(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Descricao = 5
    mvarDescricao = vData
End Property


Public Property Get Descricao() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Descricao
    Descricao = mvarDescricao
End Property



Public Property Let Id(ByVal vData As Integer)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Id = 5
    mvarId = vData
End Property


Public Property Get Id() As Integer
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Id
    Id = mvarId
End Property



