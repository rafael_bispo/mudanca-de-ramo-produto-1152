VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "SubGrupo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Option Explicit

'local variable(s) to hold property value(s)
Private mvarId As Integer    'local copy
Private mvarNome As String    'local copy
'Genesco Silva 10/03/2011 - Flow 4532893 - Confitec SP
Private mvarCnpj As String    'local copy
'fim Genesco Silva
Private mvarTipoIS As String    'local copy
Private mvarQtdeSal As String    'local copy
Private mvarTextoBenef As String    'local copy
Private mvarCorretores As Collection    'local copy
Private mvarIdEstipulante As String    'local copy
Private mvarNomeEstipulante As String    'local copy
Private mvarIdAdministrador As String    'local copy
Private mvarNomeAdministrador As String    'local copy
Private mvarProLabore As String    'local copy
Private mvarComponentes As Collection    'local copy
Private mvarQtdVidas As String    'local copy
Private mvarIdTabua As String    'local copy
Private mvarNomeTabua As String    'local copy
Private mvarCoberturas As Collection    'local copy
Private mvarPremioLiq As String    'local copy
Private mvarValIOF As String    'local copy
Private mvarCusto As String    'local copy
Private mvarPremioBruto As String    'local copy
Private mvarDescComercial As String    'local copy
Private mvarIndFormaPgto As Integer    'local copy
Private mvarDiaDebito As String    'local copy
Private mvarIndMoedaPremio As Integer    'local copy
Private mvarIndMoedaSeguro As Integer    'local copy
Private mvarIndPerPgto As Integer    'local copy
Private mvarIdBanco As String    'local copy
Private mvarIdAgencia As String    'local copy
Private mvarNomeAgencia As String    'local copy
Private mvarCCorrente As String    'local copy
Private mvarPgtoPostecipado As Integer    'local copy
Private mvarNoParcelas As String    'local copy
Private mvarTaxaJuros As String    'local copy
Private mvarValParcelado As String    'local copy
Private mvarValPagoAto As String    'local copy
Private mvarParcelas As Collection    'local copy
Private mvarPremioTarifaTotal As String    'local copy
Private mvarDescontoTotal As String    'local copy
Private mvarADFTotal As String    'local copy
Private mvarIOFTotal As String    'local copy
Private mvarComissaoTotal As String    'local copy
Private mvarAplicado As Boolean    'local copy
Private mvarPercComissao As String    'local copy
Private mvarBasicas As Collection    'local copy
Private mvarEstipulantes As Collection    'local copy
'local variable(s) to hold property value(s)
Private mvarFaturas As Collection    'local copy
'local variable(s) to hold property value(s)
Private mvarFaturaAuto As Boolean    'local copy
'local variable(s) to hold property value(s)
Private mvarIdAgenciador As Variant    'local copy
Private mvarNomeAgenciador As String    'local copy
Private mvarPercAgenciamento As Double    'local copy
Private mvarNoParcAgenciamento As Integer    'local copy
Private mvarPercAgDemais As Double    'local copy
Private mvarNoParcAgDemais As Integer    'local copy
Private mvarFormaPgtoAgDemais As Integer    'local copy

Private mvarDiaFaturamento As String   'local copy
Private mvarDiaCobranca As String      'local copy
Private mvarPeriodoPgto As String       'local copy
Private mvarCodPeriodoPgto As String    'local copy
Private mvarAssistencias As Collection
Private mvarVidas_Seguradas As Collection
'local variable(s) to hold property value(s)
Private mvarFormaPgto As String    'local copy
'local variable(s) to hold property value(s)
Private mvarTpConjuge As String    'local copy
Private mvarIsentoIOF As Boolean    'local copy
'local variable(s) to hold property value(s)
Private mvarLimMinIdadeImpl As Integer    'local copy
Private mvarLimMaxIdadeImpl As Integer    'local copy
Private mvarLimMinIdadeMov As Integer    'local copy
Private mvarLimMaxIdadeMov As Integer    'local copy
Private mvarCartao_proposta As String
Private mvarCritica_todos As String
Private mvarCritica_idade As String
Private mvarCritica_capital As String
Private mvarlim_max_idade As Integer
Private mvarlim_max_capital As Currency
Private mvartp_faturamento As String
Private mvartp_vida As String
Private mvartp_custeio As String
'local variable(s) to hold property value(s)
Private mvarDtInicioSub As Variant    'local copy
'local variable(s) to hold property value(s)
Private mvarAtualizacaoIdade As String    'local copy
Private mvarDtInicioVigenciaSBG As String    'local copy
Private mvarDtFimVigenciaSBG As String    'local copy
'pmelo - inclusao da situa��o da tabela apolice_subgrupo_vida_web_tb
Private mvarSituacao As String    'local copy

Public Property Let AtualizacaoIdade(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.AtualizacaoIdade = 5
    mvarAtualizacaoIdade = vData
End Property

Public Property Get AtualizacaoIdade() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.AtualizacaoIdade
    AtualizacaoIdade = mvarAtualizacaoIdade
End Property

Public Property Let DtInicioSub(ByVal vData As Variant)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.DtInicioSub = 5
    mvarDtInicioSub = vData
End Property

Public Property Set DtInicioSub(ByVal vData As Variant)
'used when assigning an Object to the property, on the left side of a Set statement.
'Syntax: Set x.DtInicioSub = Form1
    Set mvarDtInicioSub = vData
End Property

Public Property Get DtInicioSub() As Variant
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.DtInicioSub
    If IsObject(mvarDtInicioSub) Then
        Set DtInicioSub = mvarDtInicioSub
    Else
        DtInicioSub = mvarDtInicioSub
    End If
End Property

Public Property Let LimMaxIdadeMov(ByVal vData As Integer)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.LimMaxIdadeMov = 5
    mvarLimMaxIdadeMov = vData
End Property

Public Property Get LimMaxIdadeMov() As Integer
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.LimMaxIdadeMov
    LimMaxIdadeMov = mvarLimMaxIdadeMov
End Property

Public Property Let LimMinIdadeMov(ByVal vData As Integer)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.LimMinIdadeMov = 5
    mvarLimMinIdadeMov = vData
End Property

Public Property Get LimMinIdadeMov() As Integer
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.LimMinIdadeMov
    LimMinIdadeMov = mvarLimMinIdadeMov
End Property

Public Property Let LimMaxIdadeImpl(ByVal vData As Integer)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.LimMaxIdadeImpl = 5
    mvarLimMaxIdadeImpl = vData
End Property

Public Property Get LimMaxIdadeImpl() As Integer
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.LimMaxIdadeImpl
    LimMaxIdadeImpl = mvarLimMaxIdadeImpl
End Property

Public Property Let LimMinIdadeImpl(ByVal vData As Integer)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.LimMinIdadeImpl = 5
    mvarLimMinIdadeImpl = vData
End Property

Public Property Get LimMinIdadeImpl() As Integer
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.LimMinIdadeImpl
    LimMinIdadeImpl = mvarLimMinIdadeImpl
End Property

Public Property Let IsentoIOF(ByVal vData As Boolean)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.IsentoIOF = 5
    mvarIsentoIOF = vData
End Property

Public Property Get IsentoIOF() As Boolean
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.IsentoIOF
    IsentoIOF = mvarIsentoIOF
End Property

Public Property Let TpConjuge(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.TpConjuge = 5
    mvarTpConjuge = vData
End Property

Public Property Get TpConjuge() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.TpConjuge
    TpConjuge = mvarTpConjuge
End Property

Public Property Let FormaPgto(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.FormaPgto = 5
    mvarFormaPgto = vData
End Property

Public Property Get FormaPgto() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.FormaPgto
    FormaPgto = mvarFormaPgto
End Property

Public Property Get Assistencias() As Collection
    Set Assistencias = mvarAssistencias
End Property

Public Property Set Assistencias(ByVal vData As Object)
    Set mvarAssistencias = vData
End Property

Public Property Get Vidas_Seguradas() As Collection
    Set Vidas_Seguradas = mvarVidas_Seguradas
End Property

Public Property Set Vidas_Seguradas(ByVal vData As Object)
    Set mvarVidas_Seguradas = vData
End Property

Public Property Get DiaCobranca() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.DiaCobranca
    DiaCobranca = mvarDiaCobranca
End Property

Public Property Let DiaCobranca(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.DiaCobranca = 5
    mvarDiaCobranca = vData
End Property

Public Property Get DiaFaturamento() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.DiaFaturamento
    DiaFaturamento = mvarDiaFaturamento
End Property

Public Property Let DiaFaturamento(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.DiaFaturamento = 5
    mvarDiaFaturamento = vData
End Property

Public Property Get PeriodoPgto() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.PeriodoPgto
    PeriodoPgto = mvarPeriodoPgto
End Property

Public Property Let PeriodoPgto(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.mvarPeriodoPgto  = 5
    mvarPeriodoPgto = vData
End Property

Public Property Let FormaPgtoAgDemais(ByVal vData As Integer)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.FormaPgtoAgDemais = 5
    mvarFormaPgtoAgDemais = vData
End Property

Public Property Get FormaPgtoAgDemais() As Integer
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.FormaPgtoAgDemais
    FormaPgtoAgDemais = mvarFormaPgtoAgDemais
End Property

Public Property Let NoParcAgDemais(ByVal vData As Integer)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.NoParcAgDemais = 5
    mvarNoParcAgDemais = vData
End Property

Public Property Get NoParcAgDemais() As Integer
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.NoParcAgDemais
    NoParcAgDemais = mvarNoParcAgDemais
End Property

Public Property Let PercAgDemais(ByVal vData As Double)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.PercAgDemais = 5
    mvarPercAgDemais = vData
End Property

Public Property Get PercAgDemais() As Double
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.PercAgDemais
    PercAgDemais = mvarPercAgDemais
End Property

Public Property Let NoParcAgenciamento(ByVal vData As Integer)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.NoParcAgenciamento = 5
    mvarNoParcAgenciamento = vData
End Property

Public Property Get NoParcAgenciamento() As Integer
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.NoParcAgenciamento
    NoParcAgenciamento = mvarNoParcAgenciamento
End Property

Public Property Let PercAgenciamento(ByVal vData As Double)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.PercAgenciamento = 5
    mvarPercAgenciamento = vData
End Property

Public Property Get PercAgenciamento() As Double
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.PercAgenciamento
    PercAgenciamento = mvarPercAgenciamento
End Property

Public Property Let NomeAgenciador(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.NomeAgenciador = 5
    mvarNomeAgenciador = vData
End Property

Public Property Get NomeAgenciador() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.NomeAgenciador
    NomeAgenciador = mvarNomeAgenciador
End Property

Public Property Let IdAgenciador(ByVal vData As Variant)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.IdAgenciador = 5
    mvarIdAgenciador = vData
End Property

Public Property Set IdAgenciador(ByVal vData As Object)
'used when assigning an Object to the property, on the left side of a Set statement.
'Syntax: Set x.IdAgenciador = Form1
    Set mvarIdAgenciador = vData
End Property

Public Property Get IdAgenciador() As Variant
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.IdAgenciador
    If IsObject(mvarIdAgenciador) Then
        Set IdAgenciador = mvarIdAgenciador
    Else
        IdAgenciador = mvarIdAgenciador
    End If
End Property

Public Property Let FaturaAuto(ByVal vData As Boolean)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.FaturaAuto = 5
    mvarFaturaAuto = vData
End Property

Public Property Get FaturaAuto() As Boolean
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.FaturaAuto
    FaturaAuto = mvarFaturaAuto
End Property

Public Property Set Faturas(ByVal vData As Object)
'used when assigning an Object to the property, on the left side of a Set statement.
'Syntax: Set x.Faturas = Form1
    Set mvarFaturas = vData
End Property

Public Property Get Faturas() As Collection
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Faturas
    Set Faturas = mvarFaturas
End Property

Public Property Set Estipulantes(ByVal vData As Object)
'used when assigning an Object to the property, on the left side of a Set statement.
'Syntax: Set x.Estipulantes = Form1
    Set mvarEstipulantes = vData
End Property

Public Property Get Estipulantes() As Collection
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Estipulantes
    Set Estipulantes = mvarEstipulantes
End Property

Public Property Set Basicas(ByVal vData As Object)
'used when assigning an Object to the property, on the left side of a Set statement.
'Syntax: Set x.Basicas = Form1
    Set mvarBasicas = vData
End Property

Public Property Get Basicas() As Collection
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Basicas
    Set Basicas = mvarBasicas
End Property

Public Property Let QtdeSal(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.QtdeSal = 5
    mvarQtdeSal = vData
End Property

Public Property Get QtdeSal() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.QtdeSal
    QtdeSal = mvarQtdeSal
End Property

Public Property Let PercComissao(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.PercComissao = 5
    mvarPercComissao = vData
End Property

Public Property Get PercComissao() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.PercComissao
    PercComissao = mvarPercComissao
End Property

Public Property Let Aplicado(ByVal vData As Boolean)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Aplicado = 5
    mvarAplicado = vData
End Property

Public Property Get Aplicado() As Boolean
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Aplicado
    Aplicado = mvarAplicado
End Property

Public Property Let ComissaoTotal(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.ComissaoTotal = 5
    mvarComissaoTotal = vData
End Property

Public Property Get ComissaoTotal() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.ComissaoTotal
    ComissaoTotal = mvarComissaoTotal
End Property

Public Property Let IOFTotal(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.IOFTotal = 5
    mvarIOFTotal = vData
End Property

Public Property Get IOFTotal() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.IOFTotal
    IOFTotal = mvarIOFTotal
End Property

Public Property Let ADFTotal(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.ADFTotal = 5
    mvarADFTotal = vData
End Property

Public Property Get ADFTotal() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.ADFTotal
    ADFTotal = mvarADFTotal
End Property

Public Property Let DescontoTotal(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.DescontoTotal = 5
    mvarDescontoTotal = vData
End Property

Public Property Get DescontoTotal() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.DescontoTotal
    DescontoTotal = mvarDescontoTotal
End Property

Public Property Let PremioTarifaTotal(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.PremioTarifaTotal = 5
    mvarPremioTarifaTotal = vData
End Property

Public Property Get PremioTarifaTotal() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.PremioTarifaTotal
    PremioTarifaTotal = mvarPremioTarifaTotal
End Property

Public Property Set Parcelas(ByVal vData As Object)
'used when assigning an Object to the property, on the left side of a Set statement.
'Syntax: Set x.Parcelas = Form1
    Set mvarParcelas = vData
End Property

Public Property Get Parcelas() As Collection
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Parcelas
    Set Parcelas = mvarParcelas
End Property

Public Property Let ValPagoAto(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.ValPagoAto = 5
    mvarValPagoAto = vData
End Property

Public Property Get ValPagoAto() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.ValPagoAto
    ValPagoAto = mvarValPagoAto
End Property

Public Property Let ValParcelado(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.ValParcelado = 5
    mvarValParcelado = vData
End Property

Public Property Get ValParcelado() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.ValParcelado
    ValParcelado = mvarValParcelado
End Property

Public Property Let TaxaJuros(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.TaxaJuros = 5
    mvarTaxaJuros = vData
End Property

Public Property Get TaxaJuros() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.TaxaJuros
    TaxaJuros = mvarTaxaJuros
End Property

Public Property Let NoParcelas(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.NoParcelas = 5
    mvarNoParcelas = vData
End Property

Public Property Get NoParcelas() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.NoParcelas
    NoParcelas = mvarNoParcelas
End Property

Public Property Let PgtoPostecipado(ByVal vData As Integer)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.PgtoPostecipado = 5
    mvarPgtoPostecipado = vData
End Property

Public Property Get PgtoPostecipado() As Integer
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.PgtoPostecipado
    PgtoPostecipado = mvarPgtoPostecipado
End Property

Public Property Let CCorrente(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.CCorrente = 5
    mvarCCorrente = vData
End Property

Public Property Get CCorrente() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.CCorrente
    CCorrente = mvarCCorrente
End Property

Public Property Let NomeAgencia(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.NomeAgencia = 5
    mvarNomeAgencia = vData
End Property

Public Property Get NomeAgencia() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.NomeAgencia
    NomeAgencia = mvarNomeAgencia
End Property

Public Property Let IdAgencia(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.IdAgencia = 5
    mvarIdAgencia = vData
End Property

Public Property Get IdAgencia() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.IdAgencia
    IdAgencia = mvarIdAgencia
End Property

Public Property Let IdBanco(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.IdBanco = 5
    mvarIdBanco = vData
End Property

Public Property Get IdBanco() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.IdBanco
    IdBanco = mvarIdBanco
End Property

Public Property Let IndPerPgto(ByVal vData As Integer)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.IndPerPgto = 5
    mvarIndPerPgto = vData
End Property

Public Property Get IndPerPgto() As Integer
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.IndPerPgto
    IndPerPgto = mvarIndPerPgto
End Property

Public Property Let IndMoedaSeguro(ByVal vData As Integer)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.IndMoedaSeguro = 5
    mvarIndMoedaSeguro = vData
End Property

Public Property Get IndMoedaSeguro() As Integer
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.IndMoedaSeguro
    IndMoedaSeguro = mvarIndMoedaSeguro
End Property

Public Property Let IndMoedaPremio(ByVal vData As Integer)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.IndMoedaPremio = 5
    mvarIndMoedaPremio = vData
End Property

Public Property Get IndMoedaPremio() As Integer
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.IndMoedaPremio
    IndMoedaPremio = mvarIndMoedaPremio
End Property

Public Property Let DiaDebito(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.DiaDebito = 5
    mvarDiaDebito = vData
End Property

Public Property Get DiaDebito() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.DiaDebito
    DiaDebito = mvarDiaDebito
End Property

Public Property Let IndFormaPgto(ByVal vData As Integer)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.IndFormaPgto = 5
    mvarIndFormaPgto = vData
End Property

Public Property Get IndFormaPgto() As Integer
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.IndFormaPgto
    IndFormaPgto = mvarIndFormaPgto
End Property

Public Property Let DescComercial(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.DescComercial = 5
    mvarDescComercial = vData
End Property

Public Property Get DescComercial() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.DescComercial
    DescComercial = mvarDescComercial
End Property

Public Property Let PremioBruto(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.PremioBruto = 5
    mvarPremioBruto = vData
End Property

Public Property Get PremioBruto() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.PremioBruto
    PremioBruto = mvarPremioBruto
End Property

Public Property Let Custo(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Custo = 5
    mvarCusto = vData
End Property

Public Property Get Custo() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Custo
    Custo = mvarCusto
End Property

Public Property Let ValIOF(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.ValIOF = 5
    mvarValIOF = vData
End Property

Public Property Get ValIOF() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.ValIOF
    ValIOF = mvarValIOF
End Property

Public Property Let PremioLiq(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.PremioLiq = 5
    mvarPremioLiq = vData
End Property

Public Property Get PremioLiq() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.PremioLiq
    PremioLiq = mvarPremioLiq
End Property

Public Property Set Coberturas(ByVal vData As Object)
'used when assigning an Object to the property, on the left side of a Set statement.
'Syntax: Set x.Coberturas = Form1
    Set mvarCoberturas = vData
End Property

Public Property Get Coberturas() As Collection
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Coberturas
    Set Coberturas = mvarCoberturas
End Property

Public Property Let NomeTabua(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.NomeTabua = 5
    mvarNomeTabua = vData
End Property

Public Property Get NomeTabua() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.NomeTabua
    NomeTabua = mvarNomeTabua
End Property

Public Property Let IdTabua(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.IdTabua = 5
    mvarIdTabua = vData
End Property

Public Property Get IdTabua() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.IdTabua
    IdTabua = mvarIdTabua
End Property

Public Property Let QtdVidas(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.QtdVidas = 5
    mvarQtdVidas = vData
End Property

Public Property Get QtdVidas() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.QtdVidas
    QtdVidas = mvarQtdVidas
End Property

Public Property Set Componentes(ByVal vData As Object)
'used when assigning an Object to the property, on the left side of a Set statement.
'Syntax: Set x.Componentes = Form1
    Set mvarComponentes = vData
End Property

Public Property Get Componentes() As Collection
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Componentes
    Set Componentes = mvarComponentes
End Property

Public Property Let ProLabore(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.ProLabore = 5
    mvarProLabore = vData
End Property

Public Property Get ProLabore() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.ProLabore
    ProLabore = mvarProLabore
End Property

Public Property Let NomeAdministrador(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.NomeAdministrador = 5
    mvarNomeAdministrador = vData
End Property

Public Property Get NomeAdministrador() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.NomeAdministrador
    NomeAdministrador = mvarNomeAdministrador
End Property

Public Property Let IdAdministrador(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.IdAdministrador = 5
    mvarIdAdministrador = vData
End Property

Public Property Get IdAdministrador() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.IdAdministrador
    IdAdministrador = mvarIdAdministrador
End Property

Public Property Let NomeEstipulante(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.NomeEstipulante = 5
    mvarNomeEstipulante = vData
End Property

Public Property Get NomeEstipulante() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.NomeEstipulante
    NomeEstipulante = mvarNomeEstipulante
End Property

Public Property Let IdEstipulante(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.IdEstipulante = 5
    mvarIdEstipulante = vData
End Property

Public Property Get IdEstipulante() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.IdEstipulante
    IdEstipulante = mvarIdEstipulante
End Property

Public Property Set Corretores(ByVal vData As Object)
'used when assigning an Object to the property, on the left side of a Set statement.
'Syntax: Set x.Corretores = Form1
    Set mvarCorretores = vData
End Property

Public Property Get Corretores() As Collection
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Corretores
    Set Corretores = mvarCorretores
End Property

Public Property Let TextoBenef(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.TextoBenef = 5
    mvarTextoBenef = vData
End Property

Public Property Get TextoBenef() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.TextoBenef
    TextoBenef = mvarTextoBenef
End Property

Public Property Let cartao_proposta(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Id = 5
    mvarCartao_proposta = vData
End Property

Public Property Get cartao_proposta() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Id
    cartao_proposta = mvarCartao_proposta
End Property

Public Property Let critica_todos(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Id = 5
    mvarCritica_todos = vData
End Property

Public Property Get critica_todos() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Id
    critica_todos = mvarCritica_todos
End Property

Public Property Let critica_idade(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Id = 5
    mvarCritica_idade = vData
End Property

Public Property Get critica_idade() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Id
    critica_idade = mvarCritica_idade
End Property

Public Property Let critica_capital(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Id = 5
    mvarCritica_capital = vData
End Property

Public Property Get critica_capital() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Id
    critica_capital = mvarCritica_capital
End Property

Public Property Let TipoIS(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.TipoIS = 5
    mvarTipoIS = vData
End Property

Public Property Get TipoIS() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.TipoIS
    TipoIS = mvarTipoIS
End Property

Public Property Let Nome(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Nome = 5
    mvarNome = vData
End Property

Public Property Get Nome() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Nome
    Nome = mvarNome
End Property

Public Property Let Id(ByVal vData As Integer)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Id = 5
    mvarId = vData
End Property

Public Property Get Id() As Integer
Attribute Id.VB_UserMemId = 0
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Id
    Id = mvarId
End Property


Public Property Let lim_max_idade(ByVal vData As Integer)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Id = 5
    mvarlim_max_idade = vData
End Property


Public Property Get lim_max_idade() As Integer
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Id
    lim_max_idade = mvarlim_max_idade
End Property


Public Property Let lim_max_capital(ByVal vData As Currency)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Id = 5
    mvarlim_max_capital = vData
End Property

Public Property Get lim_max_capital() As Currency
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Id
    lim_max_capital = mvarlim_max_capital
End Property

Public Property Let tp_faturamento(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Id = 5
    mvartp_faturamento = vData
End Property

Public Property Get tp_faturamento() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Id
    tp_faturamento = mvartp_faturamento
End Property

Public Property Let tp_custeio(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Id = 5
    mvartp_custeio = vData
End Property

Public Property Get tp_custeio() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Id
    tp_custeio = mvartp_custeio
End Property

Public Property Let tp_vida(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Id = 5
    mvartp_vida = vData
End Property

Public Property Get tp_vida() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Id
    tp_vida = mvartp_vida
End Property

Public Property Let DtInicioVigenciaSBG(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Id = 5
    mvarDtInicioVigenciaSBG = vData
End Property

Public Property Get DtInicioVigenciaSBG() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Id
    DtInicioVigenciaSBG = mvarDtInicioVigenciaSBG
End Property

Public Property Let DtFimVigenciaSBG(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Id = 5
    mvarDtFimVigenciaSBG = vData
End Property

Public Property Get DtFimVigenciaSBG() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Id
    DtFimVigenciaSBG = mvarDtFimVigenciaSBG
End Property

Public Property Let Situacao(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Id = 5
    mvarSituacao = vData
End Property

Public Property Get Situacao() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Id
    Situacao = mvarSituacao
End Property


'Genesco Silva - ConfitecSP
'Cria��o de nova Propriedade CNPJ, para inclus�o de dado no GridSubgrupo

Public Property Let Cnpj(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Nome = 5
    mvarCnpj = vData
End Property

Public Property Get Cnpj() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Cnpj
    Cnpj = mvarCnpj
End Property
