VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "msflxgrd.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "tabctl32.ocx"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Object = "{C932BA88-4374-101B-A56C-00AA003668DC}#1.1#0"; "msmask32.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Object = "{1CB70356-FEA2-11D4-87FA-00805F396245}#1.0#0"; "mask2s.ocx"
Object = "{00025600-0000-0000-C000-000000000046}#5.2#0"; "Crystl32.OCX"
Object = "{3B7C8863-D78F-101B-B9B5-04021C009402}#1.2#0"; "richtx32.ocx"
Begin VB.Form ConPropBasicaVida 
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   10095
   ClientLeft      =   4545
   ClientTop       =   1110
   ClientWidth     =   10650
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MouseIcon       =   "ConPropBasicaVida.frx":0000
   ScaleHeight     =   10095
   ScaleWidth      =   10650
   Begin VB.CommandButton vis_endosso 
      Caption         =   "Visualizar Endosso"
      Height          =   375
      Left            =   4440
      TabIndex        =   352
      Top             =   9405
      Visible         =   0   'False
      Width           =   1455
   End
   Begin Crystal.CrystalReport Report 
      Left            =   2280
      Top             =   9405
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   348160
      WindowControlBox=   -1  'True
      WindowMaxButton =   -1  'True
      WindowMinButton =   -1  'True
      PrintFileLinesPerPage=   60
   End
   Begin MSComctlLib.StatusBar StatusBar1 
      Align           =   2  'Align Bottom
      Height          =   315
      Left            =   0
      TabIndex        =   311
      Top             =   9780
      Width           =   10650
      _ExtentX        =   18785
      _ExtentY        =   556
      Style           =   1
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
         EndProperty
      EndProperty
   End
   Begin VB.CommandButton visualizar 
      Caption         =   "Visualizar"
      Enabled         =   0   'False
      Height          =   375
      Left            =   6060
      TabIndex        =   10
      Top             =   9405
      Visible         =   0   'False
      Width           =   1155
   End
   Begin VB.CommandButton cmdImprimir 
      Caption         =   "&Imprimir"
      Height          =   375
      Left            =   7545
      TabIndex        =   7
      Top             =   9405
      Width           =   1095
   End
   Begin VB.CommandButton cmdSair 
      Caption         =   "&Sair"
      Height          =   375
      Left            =   8970
      TabIndex        =   8
      Top             =   9405
      Width           =   1095
   End
   Begin TabDlg.SSTab SSTabProp 
      Height          =   8970
      Left            =   120
      TabIndex        =   11
      Top             =   120
      Width           =   10395
      _ExtentX        =   18336
      _ExtentY        =   15822
      _Version        =   393216
      Tabs            =   20
      TabsPerRow      =   5
      TabHeight       =   520
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "Proposta"
      TabPicture(0)   =   "ConPropBasicaVida.frx":030A
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "Frame2"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "Frame21"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).ControlCount=   2
      TabCaption(1)   =   "SubGrupos"
      TabPicture(1)   =   "ConPropBasicaVida.frx":0326
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "Frame8"
      Tab(1).ControlCount=   1
      TabCaption(2)   =   "Corretagem"
      TabPicture(2)   =   "ConPropBasicaVida.frx":0342
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "FraSubGrupo(0)"
      Tab(2).Control(1)=   "FraCorretagem"
      Tab(2).ControlCount=   2
      TabCaption(3)   =   "Administra��o"
      TabPicture(3)   =   "ConPropBasicaVida.frx":035E
      Tab(3).ControlEnabled=   0   'False
      Tab(3).Control(0)=   "FraSubGrupo(3)"
      Tab(3).Control(1)=   "FraEstipulante"
      Tab(3).Control(2)=   "FraAdministradores"
      Tab(3).ControlCount=   3
      TabCaption(4)   =   "Agenciamento"
      TabPicture(4)   =   "ConPropBasicaVida.frx":037A
      Tab(4).ControlEnabled=   0   'False
      Tab(4).Control(0)=   "FraSubGrupo(6)"
      Tab(4).Control(1)=   "FraAgenciamento"
      Tab(4).ControlCount=   2
      TabCaption(5)   =   "Complemento"
      TabPicture(5)   =   "ConPropBasicaVida.frx":0396
      Tab(5).ControlEnabled=   0   'False
      Tab(5).Control(0)=   "fraHis"
      Tab(5).Control(1)=   "fraLOC"
      Tab(5).Control(2)=   "Frame11"
      Tab(5).Control(3)=   "Frame24"
      Tab(5).Control(4)=   "fraRemuneracaoServico"
      Tab(5).Control(5)=   "Frame7"
      Tab(5).ControlCount=   6
      TabCaption(6)   =   "Par�metros"
      TabPicture(6)   =   "ConPropBasicaVida.frx":03B2
      Tab(6).ControlEnabled=   0   'False
      Tab(6).Control(0)=   "Frame25"
      Tab(6).Control(1)=   "FraParametros"
      Tab(6).Control(2)=   "FraSubGrupo(1)"
      Tab(6).ControlCount=   3
      TabCaption(7)   =   "Coberturas/Pr�mios"
      TabPicture(7)   =   "ConPropBasicaVida.frx":03CE
      Tab(7).ControlEnabled=   0   'False
      Tab(7).Control(0)=   "FraCoberturas"
      Tab(7).Control(0).Enabled=   0   'False
      Tab(7).Control(1)=   "FraSubGrupo(2)"
      Tab(7).Control(1).Enabled=   0   'False
      Tab(7).Control(2)=   "Frame20"
      Tab(7).Control(2).Enabled=   0   'False
      Tab(7).Control(3)=   "Frame23"
      Tab(7).Control(3).Enabled=   0   'False
      Tab(7).ControlCount=   4
      TabCaption(8)   =   "Cl�usulas"
      TabPicture(8)   =   "ConPropBasicaVida.frx":03EA
      Tab(8).ControlEnabled=   0   'False
      Tab(8).Control(0)=   "Frame1"
      Tab(8).ControlCount=   1
      TabCaption(9)   =   "Sinistros"
      TabPicture(9)   =   "ConPropBasicaVida.frx":0406
      Tab(9).ControlEnabled=   0   'False
      Tab(9).Control(0)=   "Frame4"
      Tab(9).ControlCount=   1
      TabCaption(10)  =   "Endossos"
      TabPicture(10)  =   "ConPropBasicaVida.frx":0422
      Tab(10).ControlEnabled=   0   'False
      Tab(10).Control(0)=   "PETOCX1"
      Tab(10).Control(0).Enabled=   0   'False
      Tab(10).Control(1)=   "Frame28"
      Tab(10).Control(1).Enabled=   0   'False
      Tab(10).ControlCount=   2
      TabCaption(11)  =   "Cobran�as"
      TabPicture(11)  =   "ConPropBasicaVida.frx":043E
      Tab(11).ControlEnabled=   0   'False
      Tab(11).Control(0)=   "fraCartaCobranca"
      Tab(11).Control(0).Enabled=   0   'False
      Tab(11).Control(1)=   "cmdPagina(0)"
      Tab(11).Control(1).Enabled=   0   'False
      Tab(11).Control(2)=   "cmdPagina(1)"
      Tab(11).Control(2).Enabled=   0   'False
      Tab(11).Control(3)=   "cmdPagina(2)"
      Tab(11).Control(3).Enabled=   0   'False
      Tab(11).Control(4)=   "cmdPagina(3)"
      Tab(11).Control(4).Enabled=   0   'False
      Tab(11).Control(5)=   "FraCobranca"
      Tab(11).Control(5).Enabled=   0   'False
      Tab(11).Control(6)=   "FraSubGrupo(4)"
      Tab(11).Control(6).Enabled=   0   'False
      Tab(11).Control(7)=   "Label69"
      Tab(11).Control(7).Enabled=   0   'False
      Tab(11).ControlCount=   8
      TabCaption(12)  =   "Cosseguro Cedido"
      TabPicture(12)  =   "ConPropBasicaVida.frx":045A
      Tab(12).ControlEnabled=   0   'False
      Tab(12).Control(0)=   "FraCosseguroCedido"
      Tab(12).Control(1)=   "FraSubGrupo(5)"
      Tab(12).ControlCount=   2
      TabCaption(13)  =   "Resseguro"
      TabPicture(13)  =   "ConPropBasicaVida.frx":0476
      Tab(13).ControlEnabled=   0   'False
      Tab(13).Control(0)=   "GridResseguro"
      Tab(13).ControlCount=   1
      TabCaption(14)  =   "Assist�ncia"
      TabPicture(14)  =   "ConPropBasicaVida.frx":0492
      Tab(14).ControlEnabled=   0   'False
      Tab(14).Control(0)=   "FraSubGrupoAssist"
      Tab(14).Control(1)=   "FraGridAssistencia"
      Tab(14).ControlCount=   2
      TabCaption(15)  =   "Vidas Seguradas"
      TabPicture(15)  =   "ConPropBasicaVida.frx":04AE
      Tab(15).ControlEnabled=   0   'False
      Tab(15).Control(0)=   "Label68(0)"
      Tab(15).Control(1)=   "Label68(1)"
      Tab(15).Control(2)=   "Label68(2)"
      Tab(15).Control(3)=   "FraSubGrupo(7)"
      Tab(15).Control(4)=   "txtVidasAtivas"
      Tab(15).Control(5)=   "txtVidasInativas"
      Tab(15).Control(6)=   "txtVidasTotal"
      Tab(15).Control(7)=   "Frame3"
      Tab(15).Control(8)=   "cmdGerarPlanilha"
      Tab(15).ControlCount=   9
      TabCaption(16)  =   "Extrato Seguro"
      TabPicture(16)  =   "ConPropBasicaVida.frx":04CA
      Tab(16).ControlEnabled=   0   'False
      Tab(16).Control(0)=   "GridProposta"
      Tab(16).Control(1)=   "frmRestituicao"
      Tab(16).ControlCount=   2
      TabCaption(17)  =   "Certificados"
      TabPicture(17)  =   "ConPropBasicaVida.frx":04E6
      Tab(17).ControlEnabled=   0   'False
      Tab(17).Control(0)=   "Frame5(0)"
      Tab(17).ControlCount=   1
      TabCaption(18)  =   "Hist�rico"
      TabPicture(18)  =   "ConPropBasicaVida.frx":0502
      Tab(18).ControlEnabled=   0   'False
      Tab(18).Control(0)=   "cmdExibirTodos"
      Tab(18).Control(1)=   "Frame6"
      Tab(18).Control(2)=   "grdHistorico"
      Tab(18).ControlCount=   3
      TabCaption(19)  =   "Question�rio"
      TabPicture(19)  =   "ConPropBasicaVida.frx":051E
      Tab(19).ControlEnabled=   0   'False
      Tab(19).Control(0)=   "Frame10"
      Tab(19).Control(1)=   "Questionario_By_Componente"
      Tab(19).ControlCount=   2
      Begin VB.CommandButton cmdGerarPlanilha 
         Caption         =   "Gerar Planilha"
         Height          =   465
         Left            =   -67395
         TabIndex        =   355
         Top             =   7335
         Width           =   1770
      End
      Begin VB.Frame fraCartaCobranca 
         Caption         =   "Cartas de Cobran�a"
         Height          =   1095
         Left            =   -74700
         TabIndex        =   353
         Top             =   7200
         Visible         =   0   'False
         Width           =   2745
         Begin MSFlexGridLib.MSFlexGrid msFlexCartaCobranca 
            Height          =   855
            Left            =   90
            TabIndex        =   354
            ToolTipText     =   "clique na linha do gride correspondente ao n� da carta para a exibi��o"
            Top             =   180
            Width           =   2580
            _ExtentX        =   4551
            _ExtentY        =   1508
            _Version        =   393216
            Rows            =   1
            Cols            =   6
            FixedCols       =   0
            AllowBigSelection=   0   'False
            HighLight       =   0
            AllowUserResizing=   1
            FormatString    =   "<N� Carta        |<Dt.Impress�o  |||"
         End
      End
      Begin VB.Frame Frame7 
         Caption         =   "Tipo de Segurado"
         Height          =   915
         Left            =   -69720
         TabIndex        =   349
         Top             =   7280
         Width           =   4215
         Begin VB.CommandButton btnGravarTpSegurado 
            Caption         =   "&Gravar"
            Height          =   375
            Left            =   3000
            TabIndex        =   351
            Top             =   360
            Width           =   975
         End
         Begin VB.ComboBox cboTpSegurado 
            Height          =   315
            Left            =   240
            Style           =   2  'Dropdown List
            TabIndex        =   350
            Top             =   360
            Width           =   2655
         End
      End
      Begin VB.Frame Questionario_By_Componente 
         Caption         =   "Question�rio do Objeto Segurado"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   3540
         Left            =   -74880
         TabIndex        =   345
         Top             =   4575
         Width           =   9600
         Begin MSFlexGridLib.MSFlexGrid grdQuestionarioByComponente 
            Height          =   2535
            Left            =   120
            TabIndex        =   348
            Top             =   870
            Width           =   9345
            _ExtentX        =   16484
            _ExtentY        =   4471
            _Version        =   393216
            FixedCols       =   0
            WordWrap        =   -1  'True
            FormatString    =   "<Pergunta |<Resposta           "
         End
         Begin VB.ComboBox cboComponenteQuestionario 
            Height          =   315
            Left            =   2535
            Style           =   2  'Dropdown List
            TabIndex        =   346
            Top             =   285
            Width           =   2145
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "Selecione o Objeto Segurado:"
            Height          =   195
            Index           =   82
            Left            =   300
            TabIndex        =   347
            Top             =   345
            Width           =   2130
         End
      End
      Begin VB.Frame fraRemuneracaoServico 
         Caption         =   "Servi�os Banc�rios"
         Height          =   915
         Left            =   -74400
         TabIndex        =   341
         Top             =   7280
         Width           =   4515
         Begin VB.TextBox txtPercRemuneracaoServico 
            Alignment       =   1  'Right Justify
            Height          =   285
            Left            =   180
            Locked          =   -1  'True
            TabIndex        =   342
            TabStop         =   0   'False
            Top             =   460
            Width           =   1335
         End
         Begin VB.Label lblPercRemuneracaoServico 
            AutoSize        =   -1  'True
            Caption         =   "Percentual de Remunera��o:"
            Height          =   195
            Left            =   180
            TabIndex        =   343
            Top             =   220
            Width           =   2085
         End
      End
      Begin VB.Frame Frame10 
         Caption         =   "Question�rio"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   3105
         Left            =   -74880
         TabIndex        =   339
         Top             =   1440
         Width           =   9600
         Begin MSFlexGridLib.MSFlexGrid grdQuestionario 
            Height          =   2760
            Left            =   120
            TabIndex        =   340
            Top             =   240
            Width           =   9345
            _ExtentX        =   16484
            _ExtentY        =   4868
            _Version        =   393216
            FixedCols       =   0
            WordWrap        =   -1  'True
            FormatString    =   $"ConPropBasicaVida.frx":053A
         End
      End
      Begin VB.Frame frmRestituicao 
         Caption         =   "Restitui��o"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1800
         Left            =   -74760
         TabIndex        =   331
         Top             =   6360
         Width           =   9495
         Begin MSFlexGridLib.MSFlexGrid GridRest 
            Height          =   1395
            Left            =   135
            TabIndex        =   332
            Top             =   240
            Width           =   9195
            _ExtentX        =   16219
            _ExtentY        =   2461
            _Version        =   393216
            Cols            =   6
            FixedCols       =   0
            FormatString    =   $"ConPropBasicaVida.frx":05F4
         End
      End
      Begin VB.CommandButton cmdPagina 
         Caption         =   "<<"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   0
         Left            =   -71250
         TabIndex        =   318
         Top             =   7350
         Width           =   525
      End
      Begin VB.CommandButton cmdPagina 
         Caption         =   "<"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   1
         Left            =   -70665
         TabIndex        =   317
         Top             =   7350
         Width           =   525
      End
      Begin VB.CommandButton cmdPagina 
         Caption         =   ">"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   2
         Left            =   -70065
         TabIndex        =   316
         Top             =   7350
         Width           =   525
      End
      Begin VB.CommandButton cmdPagina 
         Caption         =   ">>"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   3
         Left            =   -69480
         TabIndex        =   315
         Top             =   7350
         Width           =   525
      End
      Begin VB.CommandButton cmdExibirTodos 
         Caption         =   "Exibir Todos"
         Height          =   405
         Left            =   -66240
         TabIndex        =   308
         Top             =   7200
         Width           =   1150
      End
      Begin VB.Frame Frame6 
         Caption         =   "Descri��o"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   2985
         Left            =   -74880
         TabIndex        =   306
         Top             =   4080
         Width           =   9795
         Begin VB.TextBox txtDescricao 
            Height          =   2655
            Left            =   120
            Locked          =   -1  'True
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   307
            Top             =   240
            Width           =   9585
         End
      End
      Begin VB.Frame Frame5 
         Height          =   6375
         Index           =   0
         Left            =   -74640
         TabIndex        =   289
         Top             =   1320
         Width           =   9495
         Begin VB.Frame Frame5 
            Enabled         =   0   'False
            Height          =   675
            Index           =   3
            Left            =   5520
            TabIndex        =   319
            Top             =   120
            Visible         =   0   'False
            Width           =   3855
            Begin VB.CheckBox Check2 
               Caption         =   "Inclus�o de Certificados"
               Height          =   255
               Index           =   2
               Left            =   120
               TabIndex        =   1
               Top             =   120
               Width           =   2055
            End
            Begin VB.CheckBox Check2 
               Caption         =   "Altera��o de IS"
               Height          =   255
               Index           =   0
               Left            =   120
               TabIndex        =   2
               Top             =   360
               Width           =   1455
            End
            Begin MSMask.MaskEdBox MEBVigencia 
               Height          =   300
               Left            =   2520
               TabIndex        =   3
               Top             =   330
               Width           =   1065
               _ExtentX        =   1879
               _ExtentY        =   529
               _Version        =   393216
               MaxLength       =   7
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Format          =   "mm/yyyy"
               Mask            =   "##/####"
               PromptChar      =   "_"
            End
            Begin VB.Label Label3 
               AutoSize        =   -1  'True
               Caption         =   "Vig�ncia da Fatura"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   210
               Left            =   2400
               TabIndex        =   320
               Top             =   120
               Width           =   1365
            End
         End
         Begin VB.CommandButton cmdPesquisar 
            Caption         =   "Pesquisar"
            Default         =   -1  'True
            Height          =   375
            Left            =   8520
            TabIndex        =   5
            Top             =   960
            Visible         =   0   'False
            Width           =   855
         End
         Begin VB.ComboBox cboDestino 
            Height          =   315
            ItemData        =   "ConPropBasicaVida.frx":06D0
            Left            =   6720
            List            =   "ConPropBasicaVida.frx":06DA
            Style           =   2  'Dropdown List
            TabIndex        =   4
            Top             =   1000
            Visible         =   0   'False
            Width           =   1695
         End
         Begin VB.TextBox Text1 
            BackColor       =   &H80000009&
            Height          =   300
            Index           =   3
            Left            =   4200
            Locked          =   -1  'True
            TabIndex        =   299
            Text            =   "numero dePropostas\Subgrupos"
            Top             =   360
            Width           =   1575
         End
         Begin VB.TextBox Text1 
            BackColor       =   &H80000009&
            Height          =   300
            Index           =   2
            Left            =   1995
            Locked          =   -1  'True
            TabIndex        =   294
            Text            =   "numero dePropostas\Subgrupos"
            Top             =   360
            Width           =   1575
         End
         Begin VB.TextBox Text1 
            BackColor       =   &H80000009&
            Height          =   300
            Index           =   1
            Left            =   240
            Locked          =   -1  'True
            TabIndex        =   293
            Text            =   "Nome e numero do Subgrupo"
            Top             =   1000
            Visible         =   0   'False
            Width           =   6000
         End
         Begin VB.CheckBox Check1 
            Caption         =   "Selecionar Todos"
            Height          =   255
            Left            =   240
            TabIndex        =   6
            Top             =   6000
            Visible         =   0   'False
            Width           =   1815
         End
         Begin VB.TextBox Text1 
            BackColor       =   &H80000009&
            Height          =   300
            Index           =   0
            Left            =   240
            Locked          =   -1  'True
            TabIndex        =   291
            Text            =   "Apolice: 19110"
            Top             =   360
            Width           =   1575
         End
         Begin VB.CommandButton Command1 
            Height          =   360
            Index           =   0
            Left            =   6315
            Picture         =   "ConPropBasicaVida.frx":06FA
            Style           =   1  'Graphical
            TabIndex        =   9
            ToolTipText     =   "Retornar"
            Top             =   980
            Visible         =   0   'False
            Width           =   360
         End
         Begin VB.Frame Frame5 
            Enabled         =   0   'False
            Height          =   435
            Index           =   2
            Left            =   3600
            TabIndex        =   300
            Top             =   250
            Visible         =   0   'False
            Width           =   1695
            Begin VB.CheckBox Check2 
               Caption         =   "Somente Ativas"
               Height          =   255
               Index           =   1
               Left            =   120
               TabIndex        =   0
               Top             =   120
               Width           =   1455
            End
         End
         Begin MSFlexGridLib.MSFlexGrid MSFlexGrid1 
            Height          =   4455
            Index           =   1
            Left            =   240
            TabIndex        =   292
            Top             =   1440
            Width           =   8655
            _ExtentX        =   15266
            _ExtentY        =   7858
            _Version        =   393216
            Cols            =   4
            FixedCols       =   0
            FormatString    =   $"ConPropBasicaVida.frx":0A69
         End
         Begin MSFlexGridLib.MSFlexGrid MSFlexGrid2 
            Height          =   4455
            Index           =   0
            Left            =   240
            TabIndex        =   290
            Top             =   1440
            Visible         =   0   'False
            Width           =   9075
            _ExtentX        =   16007
            _ExtentY        =   7858
            _Version        =   393216
            Cols            =   6
            FixedCols       =   0
            MergeCells      =   1
            FormatString    =   $"ConPropBasicaVida.frx":0AF8
         End
         Begin VB.Label Label2 
            Caption         =   "Destino:"
            Height          =   255
            Left            =   6720
            TabIndex        =   302
            Top             =   800
            Width           =   1455
         End
         Begin VB.Label Label1 
            Caption         =   "Label2"
            Height          =   255
            Index           =   81
            Left            =   2115
            TabIndex        =   298
            Top             =   120
            Width           =   1215
         End
         Begin VB.Label Label1 
            Caption         =   "N� de Vidas Total"
            Height          =   255
            Index           =   80
            Left            =   4240
            TabIndex        =   297
            Top             =   120
            Width           =   2655
         End
         Begin VB.Label Label1 
            Caption         =   "SubGrupos:"
            Height          =   375
            Index           =   79
            Left            =   240
            TabIndex        =   296
            Top             =   760
            Visible         =   0   'False
            Width           =   1695
         End
         Begin VB.Label Label1 
            Caption         =   "Apolice:"
            Height          =   255
            Index           =   78
            Left            =   240
            TabIndex        =   295
            Top             =   120
            Width           =   1215
         End
      End
      Begin VB.Frame Frame3 
         Caption         =   "Agendamentos"
         Height          =   2055
         Left            =   -74880
         TabIndex        =   282
         Top             =   5160
         Width           =   9765
         Begin MSFlexGridLib.MSFlexGrid GridAgendamentoVidasSeguradas 
            Height          =   1695
            Left            =   120
            TabIndex        =   284
            Top             =   240
            Width           =   9600
            _ExtentX        =   16933
            _ExtentY        =   2990
            _Version        =   393216
            Cols            =   7
            FixedCols       =   0
            AllowBigSelection=   0   'False
            HighLight       =   0
            AllowUserResizing=   1
            FormatString    =   $"ConPropBasicaVida.frx":0BBC
         End
      End
      Begin VB.TextBox txtVidasTotal 
         Height          =   300
         Left            =   -69555
         TabIndex        =   270
         Top             =   7335
         Width           =   690
      End
      Begin VB.TextBox txtVidasInativas 
         Height          =   300
         Left            =   -71525
         TabIndex        =   269
         Top             =   7335
         Width           =   690
      End
      Begin VB.TextBox txtVidasAtivas 
         Height          =   300
         Left            =   -73515
         TabIndex        =   268
         Top             =   7335
         Width           =   690
      End
      Begin VB.Frame FraSubGrupo 
         Caption         =   "SubGrupos"
         Height          =   3765
         Index           =   7
         Left            =   -74880
         TabIndex        =   262
         Top             =   1320
         Width           =   9765
         Begin VB.ComboBox cboSubGrupo 
            Height          =   315
            Index           =   8
            Left            =   150
            Style           =   2  'Dropdown List
            TabIndex        =   263
            Top             =   300
            Width           =   8565
         End
         Begin MSFlexGridLib.MSFlexGrid GridVidasSeguradas 
            Height          =   2955
            Left            =   120
            TabIndex        =   283
            Top             =   720
            Width           =   9585
            _ExtentX        =   16907
            _ExtentY        =   5212
            _Version        =   393216
            Rows            =   1
            Cols            =   12
            FixedCols       =   0
            AllowBigSelection=   0   'False
            AllowUserResizing=   1
            FormatString    =   $"ConPropBasicaVida.frx":0C4B
         End
      End
      Begin VB.Frame FraGridAssistencia 
         Height          =   4845
         Left            =   -74880
         TabIndex        =   219
         Top             =   2520
         Width           =   9885
         Begin MSFlexGridLib.MSFlexGrid gridSubGrupoAssist 
            Height          =   4620
            Left            =   60
            TabIndex        =   220
            Top             =   165
            Width           =   9765
            _ExtentX        =   17224
            _ExtentY        =   8149
            _Version        =   393216
            Rows            =   1
            Cols            =   9
            FixedCols       =   0
            WordWrap        =   -1  'True
            AllowBigSelection=   0   'False
            HighLight       =   2
            SelectionMode   =   1
            AllowUserResizing=   3
            FormatString    =   $"ConPropBasicaVida.frx":0D82
         End
      End
      Begin VB.Frame FraSubGrupoAssist 
         Caption         =   "Assist�ncia"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1065
         Left            =   -74880
         TabIndex        =   216
         Top             =   1380
         Width           =   9885
         Begin VB.ComboBox cboSubGrupo 
            Enabled         =   0   'False
            Height          =   315
            Index           =   7
            Left            =   1755
            Style           =   2  'Dropdown List
            TabIndex        =   217
            Top             =   465
            Width           =   8010
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "Sub-Grupo"
            Height          =   195
            Index           =   77
            Left            =   480
            TabIndex        =   218
            Top             =   465
            Width           =   765
         End
      End
      Begin VB.Frame Frame23 
         Caption         =   " C�lculo do Pr�mio "
         Height          =   1335
         Left            =   -74490
         TabIndex        =   154
         Top             =   4710
         Width           =   9105
         Begin VB.TextBox txtPremioBruto 
            Height          =   285
            Left            =   7515
            Locked          =   -1  'True
            TabIndex        =   174
            Top             =   960
            Width           =   1410
         End
         Begin VB.TextBox txtTotalDescontos 
            Height          =   285
            Left            =   2058
            Locked          =   -1  'True
            TabIndex        =   168
            Top             =   960
            Width           =   1410
         End
         Begin VB.TextBox txtBonus 
            Height          =   285
            Left            =   3876
            Locked          =   -1  'True
            TabIndex        =   160
            Top             =   420
            Width           =   1410
         End
         Begin VB.TextBox txtDescProg 
            Height          =   285
            Left            =   2058
            Locked          =   -1  'True
            TabIndex        =   158
            Top             =   420
            Width           =   1410
         End
         Begin VB.TextBox txtLocal 
            Height          =   285
            Left            =   5694
            Locked          =   -1  'True
            TabIndex        =   162
            Top             =   420
            Width           =   1410
         End
         Begin VB.TextBox txtCoefAjuste 
            Height          =   285
            Left            =   240
            Locked          =   -1  'True
            TabIndex        =   166
            Top             =   960
            Width           =   1410
         End
         Begin VB.TextBox txtTotalPremioLiq 
            Height          =   285
            Left            =   240
            Locked          =   -1  'True
            TabIndex        =   156
            Top             =   420
            Width           =   1410
         End
         Begin VB.TextBox txtCustoApolice 
            Height          =   285
            Left            =   5694
            Locked          =   -1  'True
            TabIndex        =   172
            Top             =   960
            Width           =   1410
         End
         Begin VB.TextBox txtIOF 
            Height          =   285
            Left            =   3876
            Locked          =   -1  'True
            TabIndex        =   170
            Top             =   960
            Width           =   1410
         End
         Begin VB.TextBox txtOutro 
            Height          =   285
            Left            =   7515
            Locked          =   -1  'True
            TabIndex        =   164
            Top             =   420
            Width           =   1410
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "Pr�mio Bruto � Vista:"
            Height          =   195
            Index           =   50
            Left            =   7530
            TabIndex        =   173
            Top             =   750
            Width           =   1470
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "Total Desc. Comercial:"
            Height          =   195
            Index           =   53
            Left            =   2070
            TabIndex        =   167
            Top             =   750
            Width           =   1605
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "Desc. Progressivo:"
            Height          =   195
            Index           =   46
            Left            =   2070
            TabIndex        =   157
            Top             =   210
            Width           =   1335
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "Desc.Renova��o:"
            Height          =   195
            Index           =   47
            Left            =   3900
            TabIndex        =   159
            Top             =   210
            Width           =   1305
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "Desc.Sinistralidade:"
            Height          =   195
            Index           =   48
            Left            =   5730
            TabIndex        =   161
            Top             =   210
            Width           =   1395
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "Coef. Fracionamento:"
            Height          =   195
            Index           =   54
            Left            =   270
            TabIndex        =   165
            Top             =   750
            Width           =   1515
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "Val. Total do IOF:"
            Height          =   195
            Index           =   52
            Left            =   3900
            TabIndex        =   169
            Top             =   750
            Width           =   1245
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "Pr�mio Tarifa:"
            Height          =   195
            Index           =   45
            Left            =   270
            TabIndex        =   155
            Top             =   210
            Width           =   975
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "Custo Ap�lice:"
            Height          =   195
            Index           =   51
            Left            =   5730
            TabIndex        =   171
            Top             =   750
            Width           =   1020
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "Desc.Outros:"
            Height          =   195
            Index           =   49
            Left            =   7530
            TabIndex        =   163
            Top             =   210
            Width           =   930
         End
      End
      Begin VB.Frame Frame20 
         Caption         =   " Pagamento "
         Height          =   1335
         Left            =   -74490
         TabIndex        =   175
         Top             =   6060
         Width           =   9105
         Begin VB.TextBox TxtTaxaJuros 
            Height          =   285
            Left            =   2055
            Locked          =   -1  'True
            TabIndex        =   179
            Top             =   465
            Width           =   1410
         End
         Begin VB.TextBox txtValTotalParc 
            Height          =   285
            Left            =   7515
            Locked          =   -1  'True
            TabIndex        =   192
            Top             =   990
            Width           =   1410
         End
         Begin VB.TextBox txtCodFormaPg 
            Height          =   285
            Left            =   240
            Locked          =   -1  'True
            TabIndex        =   187
            Top             =   990
            Width           =   495
         End
         Begin VB.TextBox txtFormaPg 
            Height          =   285
            Left            =   720
            Locked          =   -1  'True
            TabIndex        =   188
            Top             =   990
            Width           =   4515
         End
         Begin VB.TextBox txtDiaDebito 
            Height          =   285
            Left            =   5700
            Locked          =   -1  'True
            TabIndex        =   190
            Top             =   990
            Width           =   855
         End
         Begin VB.TextBox txtValJuros 
            Height          =   285
            Left            =   3870
            Locked          =   -1  'True
            TabIndex        =   181
            Top             =   465
            Width           =   1410
         End
         Begin VB.TextBox txtDemaisParcelas 
            Height          =   285
            Left            =   7515
            Locked          =   -1  'True
            TabIndex        =   185
            Top             =   465
            Width           =   1410
         End
         Begin VB.TextBox txtPagoAto 
            Height          =   285
            Left            =   5700
            Locked          =   -1  'True
            TabIndex        =   183
            Top             =   465
            Width           =   1410
         End
         Begin VB.TextBox txtNumParcelas 
            Height          =   285
            Left            =   240
            Locked          =   -1  'True
            TabIndex        =   177
            Top             =   465
            Width           =   975
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "Pago no Ato:"
            Height          =   195
            Index           =   59
            Left            =   5730
            TabIndex        =   182
            Top             =   240
            Width           =   930
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "Demais Parcelas:"
            Height          =   195
            Index           =   60
            Left            =   7530
            TabIndex        =   184
            Top             =   240
            Width           =   1230
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "Ad. Fracionamento:"
            Height          =   195
            Index           =   58
            Left            =   3900
            TabIndex        =   180
            Top             =   240
            Width           =   1380
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "Qtd. Parcelas:"
            Height          =   195
            Index           =   55
            Left            =   270
            TabIndex        =   176
            Top             =   240
            Width           =   1005
         End
         Begin VB.Label Label1 
            Caption         =   "Taxa Juros:"
            Height          =   255
            Index           =   57
            Left            =   2070
            TabIndex        =   178
            Top             =   240
            Width           =   1215
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "Val. Total Parcelado:"
            Height          =   195
            Index           =   61
            Left            =   7515
            TabIndex        =   191
            Top             =   780
            Width           =   1485
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "Forma de Pagamento:"
            Height          =   195
            Index           =   56
            Left            =   270
            TabIndex        =   186
            Top             =   780
            Width           =   1560
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "Dia D�bito:"
            Height          =   195
            Index           =   62
            Left            =   5700
            TabIndex        =   189
            Top             =   780
            Width           =   915
            WordWrap        =   -1  'True
         End
      End
      Begin VB.Frame FraSubGrupo 
         Caption         =   "SubGrupos"
         Height          =   885
         Index           =   2
         Left            =   -74505
         TabIndex        =   147
         Top             =   1260
         Width           =   9105
         Begin VB.ComboBox cboSubGrupo 
            Height          =   315
            Index           =   2
            Left            =   240
            Style           =   2  'Dropdown List
            TabIndex        =   149
            Top             =   450
            Width           =   8715
         End
         Begin VB.Label Label1 
            Caption         =   "SubGrupo :"
            Height          =   195
            Index           =   43
            Left            =   240
            TabIndex        =   148
            Top             =   240
            Width           =   1755
         End
      End
      Begin VB.Frame FraCoberturas 
         Caption         =   "Componente"
         Height          =   2505
         Left            =   -74490
         TabIndex        =   150
         Top             =   2190
         Width           =   9105
         Begin VB.ComboBox cboObjSegurado 
            Height          =   315
            Left            =   2250
            Style           =   2  'Dropdown List
            TabIndex        =   152
            Top             =   240
            Width           =   2145
         End
         Begin MSFlexGridLib.MSFlexGrid GridCoberturas 
            Height          =   1665
            Left            =   210
            TabIndex        =   153
            Top             =   720
            Width           =   8775
            _ExtentX        =   15478
            _ExtentY        =   2937
            _Version        =   393216
            Rows            =   1
            Cols            =   19
            AllowBigSelection=   0   'False
            HighLight       =   0
            AllowUserResizing=   1
            FormatString    =   $"ConPropBasicaVida.frx":0E6F
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "Selecione o Componente :"
            Height          =   195
            Index           =   44
            Left            =   270
            TabIndex        =   151
            Top             =   300
            Width           =   1875
         End
      End
      Begin VB.Frame Frame25 
         Caption         =   " Seguradora onde mant�m seguro sobre mesmo bem "
         Height          =   735
         Left            =   -74430
         TabIndex        =   132
         Top             =   1410
         Width           =   8955
         Begin VB.TextBox txtSeg2 
            Height          =   315
            Left            =   240
            Locked          =   -1  'True
            TabIndex        =   133
            Top             =   270
            Width           =   8535
         End
      End
      Begin VB.Frame FraParametros 
         Caption         =   " Selecione o objeto segurado "
         Height          =   3765
         Left            =   -74430
         TabIndex        =   137
         Top             =   3360
         Width           =   8955
         Begin VB.TextBox TxtTbCalcId 
            Height          =   315
            Left            =   240
            Locked          =   -1  'True
            TabIndex        =   143
            Top             =   1140
            Width           =   855
         End
         Begin VB.TextBox TxtTbCalculo 
            Height          =   315
            Left            =   1140
            Locked          =   -1  'True
            TabIndex        =   144
            Top             =   1140
            Width           =   7635
         End
         Begin VB.TextBox txtQtdVida 
            Height          =   315
            Left            =   240
            Locked          =   -1  'True
            TabIndex        =   146
            Top             =   1800
            Width           =   855
         End
         Begin VB.ComboBox CboComponente 
            Height          =   315
            Left            =   240
            Style           =   2  'Dropdown List
            TabIndex        =   138
            Top             =   420
            Width           =   2175
         End
         Begin VB.TextBox txtSubRamo 
            Height          =   315
            Left            =   3360
            Locked          =   -1  'True
            TabIndex        =   141
            Top             =   420
            Width           =   5415
         End
         Begin VB.TextBox txtSubRamoId 
            Height          =   315
            Left            =   2520
            Locked          =   -1  'True
            TabIndex        =   140
            Top             =   420
            Width           =   795
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "Quantidade de Vidas :"
            Height          =   195
            Index           =   65
            Left            =   240
            TabIndex        =   145
            Top             =   1560
            Width           =   1575
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "T�bua de C�lculo :"
            Height          =   195
            Index           =   66
            Left            =   240
            TabIndex        =   142
            Top             =   900
            Width           =   1350
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "Sub Ramo :"
            Height          =   195
            Index           =   64
            Left            =   2550
            TabIndex        =   139
            Top             =   180
            Width           =   900
         End
      End
      Begin VB.Frame FraSubGrupo 
         Caption         =   "SubGrupos"
         Height          =   1125
         Index           =   1
         Left            =   -74430
         TabIndex        =   134
         Top             =   2190
         Width           =   8955
         Begin VB.ComboBox cboSubGrupo 
            Height          =   315
            Index           =   1
            Left            =   210
            Style           =   2  'Dropdown List
            TabIndex        =   136
            Top             =   540
            Width           =   8565
         End
         Begin VB.Label Label1 
            Caption         =   "SubGrupo :"
            Height          =   255
            Index           =   63
            Left            =   240
            TabIndex        =   135
            Top             =   300
            Width           =   1755
         End
      End
      Begin VB.Frame Frame24 
         Caption         =   " Dados Banc�rios "
         Height          =   870
         Left            =   -74430
         TabIndex        =   115
         Top             =   1380
         Width           =   8955
         Begin VB.TextBox txtCodAgencia 
            Height          =   285
            Left            =   150
            Locked          =   -1  'True
            TabIndex        =   117
            Top             =   480
            Width           =   615
         End
         Begin VB.TextBox txtCodAgDebito 
            Height          =   285
            Left            =   4050
            Locked          =   -1  'True
            TabIndex        =   122
            Top             =   480
            Width           =   615
         End
         Begin VB.TextBox txtBanco 
            Height          =   285
            Left            =   3210
            Locked          =   -1  'True
            TabIndex        =   120
            Top             =   480
            Width           =   615
         End
         Begin VB.TextBox txtContaCorrente 
            Height          =   285
            Left            =   7290
            Locked          =   -1  'True
            TabIndex        =   125
            Top             =   480
            Width           =   1455
         End
         Begin VB.TextBox txtNomeAgDebito 
            Height          =   285
            Left            =   4650
            Locked          =   -1  'True
            TabIndex        =   123
            Top             =   480
            Width           =   2415
         End
         Begin VB.TextBox txtNomeAgencia 
            Height          =   285
            Left            =   750
            Locked          =   -1  'True
            TabIndex        =   118
            Top             =   480
            Width           =   2265
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "Banco :"
            Height          =   195
            Index           =   68
            Left            =   3210
            TabIndex        =   119
            Top             =   255
            Width           =   555
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "Conta Corrente :"
            Height          =   195
            Index           =   70
            Left            =   7290
            TabIndex        =   124
            Top             =   255
            Width           =   1155
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "Ag�ncia D�bito :"
            Height          =   195
            Index           =   69
            Left            =   4050
            TabIndex        =   121
            Top             =   255
            Width           =   1185
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "Ag�ncia Contratante :"
            Height          =   195
            Index           =   67
            Left            =   180
            TabIndex        =   116
            Top             =   255
            Width           =   1545
         End
      End
      Begin VB.Frame Frame11 
         Caption         =   " Benefici�rio(s) "
         Height          =   1530
         Left            =   -74430
         TabIndex        =   126
         Top             =   2310
         Width           =   8955
         Begin MSFlexGridLib.MSFlexGrid GridBeneficiario 
            Height          =   1155
            Left            =   150
            TabIndex        =   312
            Top             =   270
            Width           =   8625
            _ExtentX        =   15214
            _ExtentY        =   2037
            _Version        =   393216
            Rows            =   1
            Cols            =   3
            FixedCols       =   0
            AllowUserResizing=   1
            FormatString    =   $"ConPropBasicaVida.frx":1042
         End
         Begin VB.TextBox TxtBeneficiario 
            Height          =   1155
            Left            =   150
            Locked          =   -1  'True
            TabIndex        =   127
            Top             =   270
            Width           =   8595
         End
      End
      Begin VB.Frame fraLOC 
         Caption         =   "Componentes"
         Height          =   1620
         Left            =   -74430
         TabIndex        =   128
         Top             =   3900
         Width           =   8955
         Begin MSFlexGridLib.MSFlexGrid GridComponente 
            Height          =   1275
            Left            =   150
            TabIndex        =   129
            Top             =   240
            Width           =   8625
            _ExtentX        =   15214
            _ExtentY        =   2249
            _Version        =   393216
            Rows            =   1
            Cols            =   3
            FixedCols       =   0
            AllowUserResizing=   1
            FormatString    =   "Tp. Componente|<  Descri��o                                                                                 | SubRamo    "
         End
      End
      Begin VB.Frame fraHis 
         Caption         =   " Hist�rico "
         Height          =   1695
         Left            =   -74430
         TabIndex        =   130
         Top             =   5550
         Width           =   8955
         Begin MSFlexGridLib.MSFlexGrid gridAval 
            Height          =   1275
            Left            =   120
            TabIndex        =   131
            Top             =   240
            Width           =   8655
            _ExtentX        =   15266
            _ExtentY        =   2249
            _Version        =   393216
            Rows            =   1
            Cols            =   3
            FixedCols       =   0
            AllowUserResizing=   1
            FormatString    =   $"ConPropBasicaVida.frx":10CF
         End
         Begin VB.TextBox txtMotRec 
            Height          =   285
            Left            =   1560
            Locked          =   -1  'True
            TabIndex        =   314
            Top             =   1320
            Visible         =   0   'False
            Width           =   7215
         End
         Begin VB.Label lblMotRec 
            AutoSize        =   -1  'True
            Caption         =   "Motivo da recusa:"
            Height          =   195
            Left            =   120
            TabIndex        =   313
            Top             =   1320
            Visible         =   0   'False
            Width           =   1275
         End
      End
      Begin VB.Frame FraCobranca 
         Caption         =   "Endossos de Cobran�a e Faturas "
         Height          =   4575
         Left            =   -74700
         TabIndex        =   203
         Top             =   2580
         Width           =   9555
         Begin MSFlexGridLib.MSFlexGrid GridFaturas 
            Height          =   2235
            Left            =   30
            TabIndex        =   205
            Top             =   2310
            Width           =   9510
            _ExtentX        =   16775
            _ExtentY        =   3942
            _Version        =   393216
            Rows            =   1
            Cols            =   16
            FixedCols       =   0
            AllowBigSelection=   0   'False
            HighLight       =   0
            AllowUserResizing=   1
            FormatString    =   $"ConPropBasicaVida.frx":116F
         End
         Begin MSFlexGridLib.MSFlexGrid GridCobranca 
            Height          =   2025
            Left            =   30
            TabIndex        =   204
            Top             =   240
            Width           =   9510
            _ExtentX        =   16775
            _ExtentY        =   3572
            _Version        =   393216
            Rows            =   1
            Cols            =   17
            FixedCols       =   0
            AllowBigSelection=   0   'False
            HighLight       =   0
            AllowUserResizing=   1
            FormatString    =   $"ConPropBasicaVida.frx":1305
         End
      End
      Begin VB.Frame FraSubGrupo 
         Caption         =   "SubGrupos"
         Height          =   1125
         Index           =   4
         Left            =   -74700
         TabIndex        =   200
         Top             =   1410
         Width           =   9555
         Begin VB.ComboBox cboSubGrupo 
            Height          =   315
            Index           =   4
            Left            =   270
            Style           =   2  'Dropdown List
            TabIndex        =   202
            Top             =   540
            Width           =   8505
         End
         Begin VB.Label Label1 
            Caption         =   "SubGrupo :"
            Height          =   255
            Index           =   71
            Left            =   270
            TabIndex        =   201
            Top             =   300
            Width           =   1755
         End
      End
      Begin VB.Frame Frame28 
         Caption         =   " Endossos "
         Height          =   5775
         Left            =   -74700
         TabIndex        =   198
         Top             =   1410
         Width           =   9555
         Begin MSFlexGridLib.MSFlexGrid gridEndosso 
            Height          =   5295
            Left            =   195
            TabIndex        =   199
            Top             =   330
            Width           =   9195
            _ExtentX        =   16219
            _ExtentY        =   9340
            _Version        =   393216
            Rows            =   1
            Cols            =   11
            FixedCols       =   0
            AllowBigSelection=   0   'False
            HighLight       =   0
            AllowUserResizing=   1
            FormatString    =   $"ConPropBasicaVida.frx":1494
         End
      End
      Begin VB.Frame Frame1 
         Caption         =   "Cl�usulas Personalizadas "
         Height          =   5895
         Left            =   -74910
         TabIndex        =   193
         Top             =   1410
         Width           =   9975
         Begin RichTextLib.RichTextBox txtClausula 
            Height          =   4815
            Left            =   120
            TabIndex        =   328
            Top             =   960
            Width           =   9735
            _ExtentX        =   17171
            _ExtentY        =   8493
            _Version        =   393217
            Enabled         =   -1  'True
            ReadOnly        =   -1  'True
            ScrollBars      =   2
            TextRTF         =   $"ConPropBasicaVida.frx":1646
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin VB.ComboBox cboClausula 
            Height          =   315
            Left            =   120
            Style           =   2  'Dropdown List
            TabIndex        =   195
            Top             =   510
            Width           =   8205
         End
         Begin VB.Label Label1 
            Caption         =   "Cl�usula :"
            Height          =   255
            Index           =   42
            Left            =   120
            TabIndex        =   194
            Top             =   270
            Width           =   1305
         End
      End
      Begin VB.Frame FraAgenciamento 
         Caption         =   "Agenciamento "
         Height          =   2925
         Left            =   -74460
         TabIndex        =   98
         Top             =   2700
         Width           =   8985
         Begin VB.TextBox TxtAgenciador 
            Alignment       =   1  'Right Justify
            Height          =   300
            Left            =   2520
            Locked          =   -1  'True
            TabIndex        =   102
            Top             =   540
            Width           =   5610
         End
         Begin VB.TextBox TxtCodAgenciador 
            Alignment       =   1  'Right Justify
            Height          =   300
            Left            =   210
            Locked          =   -1  'True
            TabIndex        =   100
            Top             =   540
            Width           =   1410
         End
         Begin VB.TextBox TxtNoParcAgDemais 
            Alignment       =   1  'Right Justify
            Height          =   300
            Left            =   6720
            Locked          =   -1  'True
            TabIndex        =   114
            Top             =   2310
            Width           =   1410
         End
         Begin VB.TextBox TxtFPAgDemais 
            Alignment       =   1  'Right Justify
            Height          =   300
            Left            =   2520
            Locked          =   -1  'True
            TabIndex        =   112
            Top             =   2310
            Width           =   3690
         End
         Begin VB.TextBox TxtPercAgDemais 
            Alignment       =   1  'Right Justify
            Height          =   300
            Left            =   210
            Locked          =   -1  'True
            TabIndex        =   110
            Top             =   2310
            Width           =   1410
         End
         Begin VB.TextBox TxtNoParcAgPrim 
            Alignment       =   1  'Right Justify
            Height          =   300
            Left            =   6720
            Locked          =   -1  'True
            TabIndex        =   108
            Top             =   1410
            Width           =   1410
         End
         Begin VB.TextBox TxtFPAgPrim 
            Alignment       =   1  'Right Justify
            Height          =   300
            Left            =   2520
            Locked          =   -1  'True
            TabIndex        =   106
            Top             =   1410
            Width           =   3690
         End
         Begin VB.TextBox TxtPercAgPrim 
            Alignment       =   1  'Right Justify
            Height          =   300
            Left            =   210
            Locked          =   -1  'True
            TabIndex        =   104
            Top             =   1410
            Width           =   1410
         End
         Begin VB.Label Label1 
            Caption         =   "Nome Agenciador :"
            Height          =   255
            Index           =   35
            Left            =   2550
            TabIndex        =   101
            Top             =   300
            Width           =   1965
         End
         Begin VB.Label Label1 
            Caption         =   "C�digo :"
            Height          =   255
            Index           =   34
            Left            =   240
            TabIndex        =   99
            Top             =   300
            Width           =   1965
         End
         Begin VB.Label Label1 
            Caption         =   "No. Parcelas Demais Faturas :"
            Height          =   255
            Index           =   39
            Left            =   6720
            TabIndex        =   113
            Top             =   2070
            Width           =   2145
         End
         Begin VB.Label Label1 
            Caption         =   "Forma Pgto. Demais Faturas :"
            Height          =   255
            Index           =   40
            Left            =   2550
            TabIndex        =   111
            Top             =   2070
            Width           =   2145
         End
         Begin VB.Label Label1 
            Caption         =   "Percentual Demais Faturas :"
            Height          =   255
            Index           =   41
            Left            =   240
            TabIndex        =   109
            Top             =   2070
            Width           =   1995
         End
         Begin VB.Label Label1 
            Caption         =   "No. Parcelas 1a. Fatura :"
            Height          =   255
            Index           =   38
            Left            =   6750
            TabIndex        =   107
            Top             =   1170
            Width           =   1965
         End
         Begin VB.Label Label1 
            Caption         =   "Forma Pgto. 1a. Fatura :"
            Height          =   255
            Index           =   37
            Left            =   2550
            TabIndex        =   105
            Top             =   1170
            Width           =   1965
         End
         Begin VB.Label Label1 
            Caption         =   "Percentual 1a. Fatura :"
            Height          =   255
            Index           =   36
            Left            =   240
            TabIndex        =   103
            Top             =   1170
            Width           =   1965
         End
      End
      Begin VB.Frame FraSubGrupo 
         Caption         =   "SubGrupos"
         Height          =   1125
         Index           =   6
         Left            =   -74460
         TabIndex        =   95
         Top             =   1530
         Width           =   8985
         Begin VB.ComboBox cboSubGrupo 
            Height          =   315
            Index           =   6
            Left            =   210
            Style           =   2  'Dropdown List
            TabIndex        =   97
            Top             =   570
            Width           =   8565
         End
         Begin VB.Label Label56 
            Caption         =   "SubGrupo :"
            Height          =   255
            Index           =   0
            Left            =   240
            TabIndex        =   96
            Top             =   330
            Width           =   1755
         End
      End
      Begin VB.Frame FraAdministradores 
         Caption         =   "Administra��o"
         Height          =   4245
         Left            =   -74400
         TabIndex        =   93
         Top             =   3690
         Width           =   8865
         Begin MSFlexGridLib.MSFlexGrid GridAdm 
            Height          =   3195
            Left            =   210
            TabIndex        =   94
            Top             =   300
            Width           =   8550
            _ExtentX        =   15081
            _ExtentY        =   5636
            _Version        =   393216
            Rows            =   1
            Cols            =   4
            FixedCols       =   0
            WordWrap        =   -1  'True
            AllowUserResizing=   1
            FormatString    =   $"ConPropBasicaVida.frx":16BD
         End
         Begin VB.Label lblObsEstipulante 
            AutoSize        =   -1  'True
            Caption         =   "Obs.:"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   12
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H000000FF&
            Height          =   300
            Left            =   1200
            TabIndex        =   344
            Top             =   3600
            Width           =   645
         End
      End
      Begin VB.Frame FraEstipulante 
         Caption         =   "Estipulantes"
         Height          =   825
         Left            =   -74400
         TabIndex        =   91
         Top             =   2760
         Width           =   8865
         Begin VB.ComboBox cboEstipulante 
            Height          =   315
            Left            =   210
            Style           =   2  'Dropdown List
            TabIndex        =   92
            Top             =   300
            Width           =   8505
         End
      End
      Begin VB.Frame FraSubGrupo 
         Caption         =   "SubGrupos"
         Height          =   1125
         Index           =   3
         Left            =   -74400
         TabIndex        =   88
         Top             =   1560
         Width           =   8865
         Begin VB.ComboBox cboSubGrupo 
            Height          =   315
            Index           =   3
            Left            =   210
            Style           =   2  'Dropdown List
            TabIndex        =   90
            Top             =   540
            Width           =   8505
         End
         Begin VB.Label Label1 
            Caption         =   "SubGrupo :"
            Height          =   255
            Index           =   33
            Left            =   240
            TabIndex        =   89
            Top             =   300
            Width           =   1755
         End
      End
      Begin VB.Frame Frame8 
         Caption         =   "SubGrupos"
         Height          =   6945
         Left            =   -74910
         TabIndex        =   79
         Top             =   1305
         Width           =   9870
         Begin VB.TextBox TxtTipoFaturamento 
            Height          =   285
            Left            =   5985
            TabIndex        =   330
            Top             =   5310
            Width           =   1500
         End
         Begin VB.TextBox txtDtFimVigenciaSBG 
            Height          =   300
            Left            =   8040
            Locked          =   -1  'True
            TabIndex        =   303
            Top             =   4155
            Width           =   1650
         End
         Begin VB.CheckBox chkAtualizacaoIdade 
            Caption         =   "Reenquadramento"
            Enabled         =   0   'False
            Height          =   255
            Left            =   4110
            TabIndex        =   287
            Top             =   4785
            Width           =   1800
         End
         Begin VB.TextBox TxtDtIniSub 
            Height          =   285
            Left            =   150
            TabIndex        =   285
            Top             =   2400
            Width           =   1320
         End
         Begin VB.CheckBox chkCartao_porposta 
            Caption         =   "Cart�o proposta"
            Height          =   285
            Left            =   2520
            TabIndex        =   280
            Top             =   4770
            Width           =   1500
         End
         Begin VB.CheckBox chkIsento_IOF 
            Caption         =   "Isento IOF"
            Height          =   285
            Left            =   1350
            TabIndex        =   279
            Top             =   4770
            Width           =   1065
         End
         Begin VB.CheckBox chkFatura_Auto 
            Caption         =   "Fatura Auto"
            Height          =   285
            Left            =   135
            TabIndex        =   278
            Top             =   4770
            Width           =   1260
         End
         Begin VB.TextBox txtTpVida 
            Height          =   285
            Left            =   8600
            TabIndex        =   277
            Top             =   4725
            Width           =   1095
         End
         Begin VB.TextBox txtTpCusteio 
            Height          =   285
            Left            =   7300
            TabIndex        =   276
            Top             =   4725
            Width           =   1095
         End
         Begin VB.TextBox txtTpFaturamento 
            Height          =   285
            Left            =   6000
            TabIndex        =   275
            Top             =   4725
            Width           =   1095
         End
         Begin VB.Frame frmTp_Cartao_proposta 
            Height          =   1230
            Left            =   6000
            TabIndex        =   239
            Top             =   5595
            Width           =   3705
            Begin VB.CheckBox chkCapital 
               Caption         =   "Capital"
               Height          =   240
               Left            =   360
               TabIndex        =   260
               Top             =   870
               Width           =   975
            End
            Begin VB.TextBox txtIdade 
               Height          =   285
               Left            =   1440
               TabIndex        =   242
               Top             =   450
               Width           =   510
            End
            Begin VB.CheckBox chkIdade 
               Caption         =   "Idade"
               Height          =   195
               Left            =   360
               TabIndex        =   241
               Top             =   547
               Width           =   855
            End
            Begin VB.CheckBox chkTodos 
               Caption         =   "Todos"
               Height          =   240
               Left            =   345
               TabIndex        =   240
               Top             =   180
               Width           =   855
            End
            Begin Mask2S.ConMask2S MskCapital 
               Height          =   285
               Left            =   1425
               TabIndex        =   310
               Top             =   825
               Width           =   1815
               _ExtentX        =   3201
               _ExtentY        =   503
               mask            =   ""
               text            =   "0,00"
               locked          =   0   'False
               enabled         =   -1  'True
            End
         End
         Begin VB.TextBox txtIdade_Max_Mov 
            Height          =   300
            Left            =   4365
            TabIndex        =   235
            Top             =   3555
            Width           =   1230
         End
         Begin VB.TextBox txtIdade_Min_Mov 
            Height          =   300
            Left            =   2925
            TabIndex        =   234
            Top             =   3555
            Width           =   1230
         End
         Begin VB.TextBox txtConjuge 
            Height          =   300
            Left            =   5805
            TabIndex        =   236
            Top             =   3555
            Width           =   2040
         End
         Begin VB.TextBox txtIdade_Max_Impl 
            Height          =   300
            Left            =   1530
            TabIndex        =   238
            Top             =   3555
            Width           =   1230
         End
         Begin VB.TextBox txtIdade_Min_Impl 
            Height          =   300
            Left            =   135
            TabIndex        =   233
            Top             =   3555
            Width           =   1230
         End
         Begin VB.TextBox txtCta_Corrente 
            Height          =   300
            Left            =   7440
            TabIndex        =   232
            Top             =   2970
            Width           =   2130
         End
         Begin VB.TextBox txtAg_Debito 
            Height          =   300
            Left            =   4200
            TabIndex        =   231
            Top             =   2970
            Width           =   3165
         End
         Begin VB.TextBox txtForma_Pgto 
            Height          =   300
            Left            =   135
            TabIndex        =   230
            Top             =   2970
            Width           =   3930
         End
         Begin VB.TextBox txtPeriodo_Pgto 
            Height          =   330
            Left            =   6465
            TabIndex        =   229
            Top             =   2385
            Width           =   3225
         End
         Begin VB.TextBox txtDt_Cobranca 
            Height          =   300
            Left            =   4890
            TabIndex        =   228
            Top             =   2400
            Width           =   1440
         End
         Begin VB.TextBox txtDt_faturamento 
            Height          =   300
            Left            =   3285
            TabIndex        =   227
            Top             =   2385
            Width           =   1455
         End
         Begin VB.TextBox txtTexto_Padrao 
            Height          =   300
            Left            =   135
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   237
            Top             =   4140
            Width           =   7680
         End
         Begin VB.TextBox txtTabua_calculo 
            Height          =   300
            Left            =   1800
            TabIndex        =   226
            Top             =   2385
            Width           =   1320
         End
         Begin VB.TextBox txtQuant_vidas 
            Height          =   300
            Left            =   8025
            Locked          =   -1  'True
            TabIndex        =   225
            Top             =   3555
            Width           =   1650
         End
         Begin VB.TextBox txtMult_Sal 
            Height          =   300
            Left            =   8730
            TabIndex        =   224
            Top             =   1800
            Width           =   930
         End
         Begin VB.TextBox txtTp_IS 
            Height          =   300
            Left            =   6345
            TabIndex        =   223
            Top             =   1800
            Width           =   2205
         End
         Begin VB.TextBox txtNome_subgrupo 
            Height          =   300
            Left            =   765
            MaxLength       =   60
            TabIndex        =   222
            Top             =   1800
            Width           =   5355
         End
         Begin VB.TextBox txtID 
            Height          =   300
            Left            =   135
            TabIndex        =   221
            Top             =   1800
            Width           =   465
         End
         Begin MSFlexGridLib.MSFlexGrid GridSubGrupos 
            Height          =   1350
            Left            =   135
            TabIndex        =   80
            Top             =   210
            Width           =   9570
            _ExtentX        =   16880
            _ExtentY        =   2381
            _Version        =   393216
            Rows            =   1
            Cols            =   31
            FixedCols       =   0
            AllowBigSelection=   0   'False
            AllowUserResizing=   1
         End
         Begin MSFlexGridLib.MSFlexGrid grdFaixas 
            Height          =   1320
            Left            =   120
            TabIndex        =   281
            Top             =   5325
            Width           =   5685
            _ExtentX        =   10028
            _ExtentY        =   2328
            _Version        =   393216
            Rows            =   1
            Cols            =   5
            FixedCols       =   0
            AllowBigSelection=   0   'False
            AllowUserResizing=   1
         End
         Begin VB.Label LblTpFatur 
            Caption         =   "Tipo de Faturamento:"
            Height          =   240
            Left            =   5985
            TabIndex        =   329
            Top             =   5085
            Width           =   1635
         End
         Begin VB.Label Label66 
            Caption         =   "Dt de Fim de Vig�ncia:"
            Height          =   240
            Index           =   22
            Left            =   8040
            TabIndex        =   304
            Top             =   3900
            Width           =   1665
         End
         Begin VB.Label Label66 
            AutoSize        =   -1  'True
            Caption         =   "Dt In�cio SubGrupo:"
            Height          =   195
            Index           =   21
            Left            =   150
            TabIndex        =   286
            Top             =   2160
            Width           =   1425
         End
         Begin VB.Label Label66 
            AutoSize        =   -1  'True
            Caption         =   "Tipo de Vida:"
            Height          =   195
            Index           =   20
            Left            =   8595
            TabIndex        =   274
            Top             =   4485
            Width           =   945
         End
         Begin VB.Label Label66 
            Caption         =   "Custeio:"
            Height          =   255
            Index           =   19
            Left            =   7305
            TabIndex        =   273
            Top             =   4485
            Width           =   735
         End
         Begin VB.Label Label66 
            AutoSize        =   -1  'True
            Caption         =   "Faturamento:"
            Height          =   195
            Index           =   5
            Left            =   6000
            TabIndex        =   272
            Top             =   4485
            Width           =   930
         End
         Begin VB.Label Label66 
            Caption         =   "Texto Padr�o:"
            Height          =   240
            Index           =   18
            Left            =   135
            TabIndex        =   261
            Top             =   3915
            Width           =   1230
         End
         Begin VB.Label Label66 
            Caption         =   "C�njuge:"
            Height          =   240
            Index           =   17
            Left            =   5805
            TabIndex        =   259
            Top             =   3330
            Width           =   915
         End
         Begin VB.Label Label66 
            Caption         =   "Idade M�n. Mov.:"
            Height          =   240
            Index           =   16
            Left            =   2925
            TabIndex        =   258
            Top             =   3330
            Width           =   1230
         End
         Begin VB.Label Label66 
            Caption         =   "Idade M�x. Mov.:"
            Height          =   240
            Index           =   15
            Left            =   4365
            TabIndex        =   257
            Top             =   3330
            Width           =   1275
         End
         Begin VB.Label Label66 
            Caption         =   "Agencia D�bito:"
            Height          =   240
            Index           =   14
            Left            =   4230
            TabIndex        =   256
            Top             =   2760
            Width           =   1245
         End
         Begin VB.Label Label66 
            Caption         =   "Conta Corrente:"
            Height          =   240
            Index           =   13
            Left            =   7560
            TabIndex        =   255
            Top             =   2745
            Width           =   1320
         End
         Begin VB.Label Label66 
            Caption         =   "Idade M�n. Impl.:"
            Height          =   240
            Index           =   12
            Left            =   135
            TabIndex        =   254
            Top             =   3330
            Width           =   1230
         End
         Begin VB.Label Label66 
            Caption         =   "Idade M�x. Impl.:"
            Height          =   240
            Index           =   11
            Left            =   1530
            TabIndex        =   253
            Top             =   3330
            Width           =   1230
         End
         Begin VB.Label Label66 
            Caption         =   "Forma de Pgto:"
            Height          =   240
            Index           =   10
            Left            =   135
            TabIndex        =   252
            Top             =   2745
            Width           =   1230
         End
         Begin VB.Label Label66 
            Caption         =   "Periodo de Pgto:"
            Height          =   240
            Index           =   9
            Left            =   6480
            TabIndex        =   251
            Top             =   2160
            Width           =   1230
         End
         Begin VB.Label Label66 
            AutoSize        =   -1  'True
            Caption         =   "Dia de Vencimento:"
            Height          =   195
            Index           =   8
            Left            =   4905
            TabIndex        =   250
            Top             =   2160
            Width           =   1395
         End
         Begin VB.Label Label66 
            AutoSize        =   -1  'True
            Caption         =   "Dia de Movimento: "
            Height          =   195
            Index           =   7
            Left            =   3285
            TabIndex        =   249
            Top             =   2160
            Width           =   1380
         End
         Begin VB.Label Label66 
            Caption         =   "T�bua de C�lculo:"
            Height          =   240
            Index           =   6
            Left            =   1800
            TabIndex        =   248
            Top             =   2160
            Width           =   1320
         End
         Begin VB.Label Label66 
            Caption         =   "Quantidade de Vidas:"
            Height          =   240
            Index           =   4
            Left            =   7995
            TabIndex        =   247
            Top             =   3330
            Width           =   1545
         End
         Begin VB.Label Label66 
            AutoSize        =   -1  'True
            Caption         =   "M�lt. Salarial:"
            Height          =   195
            Index           =   3
            Left            =   8730
            TabIndex        =   246
            Top             =   1560
            Width           =   945
         End
         Begin VB.Label Label66 
            AutoSize        =   -1  'True
            Caption         =   "Tipo de IS:"
            Height          =   195
            Index           =   2
            Left            =   6345
            TabIndex        =   245
            Top             =   1575
            Width           =   780
         End
         Begin VB.Label Label66 
            AutoSize        =   -1  'True
            Caption         =   "Nome:"
            Height          =   195
            Index           =   1
            Left            =   765
            TabIndex        =   244
            Top             =   1575
            Width           =   465
         End
         Begin VB.Label Label66 
            AutoSize        =   -1  'True
            Caption         =   "C�digo:"
            Height          =   195
            Index           =   0
            Left            =   135
            TabIndex        =   243
            Top             =   1560
            Width           =   540
         End
      End
      Begin VB.Frame FraSubGrupo 
         Caption         =   "SubGrupos"
         Height          =   1125
         Index           =   0
         Left            =   -74850
         TabIndex        =   81
         Top             =   1530
         Width           =   9585
         Begin VB.ComboBox cboSubGrupo 
            Height          =   315
            Index           =   0
            Left            =   210
            Style           =   2  'Dropdown List
            TabIndex        =   83
            Top             =   570
            Width           =   8565
         End
         Begin VB.Label Label1 
            Caption         =   "SubGrupo :"
            Height          =   255
            Index           =   31
            Left            =   240
            TabIndex        =   82
            Top             =   330
            Width           =   1755
         End
      End
      Begin VB.Frame FraCorretagem 
         Caption         =   "Corretagem "
         Height          =   2925
         Left            =   -74850
         TabIndex        =   84
         Top             =   2700
         Width           =   9585
         Begin VB.TextBox TxtComissao 
            Alignment       =   1  'Right Justify
            Height          =   300
            Left            =   210
            Locked          =   -1  'True
            TabIndex        =   86
            Top             =   600
            Width           =   1410
         End
         Begin MSFlexGridLib.MSFlexGrid gridCorretor 
            Height          =   1770
            Left            =   30
            TabIndex        =   87
            Top             =   1020
            Width           =   9495
            _ExtentX        =   16748
            _ExtentY        =   3122
            _Version        =   393216
            Rows            =   1
            Cols            =   5
            FixedCols       =   0
            ScrollBars      =   2
            AllowUserResizing=   1
            FormatString    =   $"ConPropBasicaVida.frx":1773
         End
         Begin VB.Label Label1 
            Caption         =   "Percentual de Comiss�o :"
            Height          =   255
            Index           =   32
            Left            =   240
            TabIndex        =   85
            Top             =   360
            Width           =   1965
         End
      End
      Begin VB.Frame Frame4 
         Caption         =   "Sinistros Avisados "
         Height          =   5895
         Left            =   -74490
         TabIndex        =   196
         Top             =   1410
         Width           =   8985
         Begin MSFlexGridLib.MSFlexGrid GridSinistro 
            Height          =   5205
            Left            =   210
            TabIndex        =   197
            Top             =   420
            Width           =   8595
            _ExtentX        =   15161
            _ExtentY        =   9181
            _Version        =   393216
            Rows            =   1
            Cols            =   4
            FixedCols       =   0
            AllowBigSelection=   0   'False
            HighLight       =   0
            AllowUserResizing=   1
            FormatString    =   $"ConPropBasicaVida.frx":181A
         End
      End
      Begin VB.Frame Frame21 
         Caption         =   "Geral"
         Height          =   4110
         Left            =   600
         TabIndex        =   12
         Top             =   1320
         Width           =   8895
         Begin VB.TextBox txtTp_Documento 
            Enabled         =   0   'False
            Height          =   300
            Left            =   5350
            TabIndex        =   360
            Top             =   3720
            Width           =   1230
         End
         Begin VB.TextBox txtPropostaSC 
            Height          =   300
            Left            =   150
            Locked          =   -1  'True
            TabIndex        =   335
            Top             =   3720
            Width           =   1560
         End
         Begin VB.TextBox txtApoliceSC 
            Height          =   300
            Left            =   1920
            Locked          =   -1  'True
            TabIndex        =   334
            Top             =   3720
            Width           =   1560
         End
         Begin VB.TextBox txtCertificadoSC 
            Height          =   300
            Left            =   3660
            Locked          =   -1  'True
            TabIndex        =   333
            Top             =   3720
            Width           =   1560
         End
         Begin VB.CheckBox chkVidaEmpresa 
            Caption         =   "Proposta Ouro Vida Empresa ?"
            Enabled         =   0   'False
            Height          =   195
            Left            =   180
            TabIndex        =   301
            Top             =   3180
            Width           =   2955
         End
         Begin VB.TextBox txtNumOrdemEndossoLider 
            Height          =   300
            Left            =   7230
            Locked          =   -1  'True
            TabIndex        =   29
            Top             =   2175
            Width           =   1455
         End
         Begin VB.TextBox txtNumEndossoLider 
            Height          =   300
            Left            =   5595
            Locked          =   -1  'True
            TabIndex        =   28
            Top             =   2175
            Width           =   1545
         End
         Begin VB.TextBox txtDtEmissao 
            Height          =   300
            Left            =   150
            Locked          =   -1  'True
            TabIndex        =   26
            Top             =   2190
            Width           =   1575
         End
         Begin VB.TextBox txtLimMaxInd 
            Height          =   300
            Left            =   1830
            Locked          =   -1  'True
            TabIndex        =   27
            Top             =   2175
            Width           =   1575
         End
         Begin VB.TextBox txtDtEmissaoLider 
            Height          =   300
            Left            =   1830
            Locked          =   -1  'True
            TabIndex        =   31
            Top             =   2790
            Width           =   1575
         End
         Begin VB.TextBox txtPropAnterior 
            Height          =   300
            Left            =   7230
            Locked          =   -1  'True
            TabIndex        =   20
            Top             =   1020
            Width           =   1455
         End
         Begin VB.TextBox txtProduto 
            Height          =   300
            Left            =   1845
            Locked          =   -1  'True
            TabIndex        =   15
            Top             =   420
            Width           =   3825
         End
         Begin VB.TextBox txtStatus 
            Height          =   300
            Left            =   3525
            Locked          =   -1  'True
            TabIndex        =   19
            Top             =   1020
            Width           =   3585
         End
         Begin VB.TextBox txtDtContratacao 
            Height          =   300
            Left            =   1860
            Locked          =   -1  'True
            TabIndex        =   18
            Top             =   1020
            Width           =   1575
         End
         Begin VB.TextBox txtNumero 
            Height          =   300
            Left            =   150
            Locked          =   -1  'True
            TabIndex        =   14
            Top             =   420
            Width           =   1575
         End
         Begin VB.TextBox txtIniVigencia 
            Height          =   300
            Left            =   5595
            Locked          =   -1  'True
            TabIndex        =   24
            Top             =   1590
            Width           =   1515
         End
         Begin VB.TextBox txtFimVigencia 
            Height          =   300
            Left            =   7230
            Locked          =   -1  'True
            TabIndex        =   25
            Top             =   1590
            Width           =   1455
         End
         Begin VB.TextBox txtApolice 
            Height          =   300
            Left            =   150
            Locked          =   -1  'True
            TabIndex        =   21
            Top             =   1590
            Width           =   1575
         End
         Begin VB.TextBox txtNumEndosso 
            Height          =   300
            Left            =   1845
            Locked          =   -1  'True
            TabIndex        =   22
            Top             =   1590
            Width           =   1575
         End
         Begin VB.TextBox txtDtEndosso 
            Height          =   300
            Left            =   3525
            Locked          =   -1  'True
            TabIndex        =   23
            Top             =   1590
            Width           =   1455
         End
         Begin VB.TextBox txtRamo 
            Height          =   300
            Left            =   5790
            Locked          =   -1  'True
            TabIndex        =   16
            Top             =   420
            Width           =   2910
         End
         Begin VB.TextBox TxtNumeroOrdem 
            Height          =   300
            Left            =   150
            Locked          =   -1  'True
            TabIndex        =   30
            Top             =   2790
            Width           =   1575
         End
         Begin VB.TextBox txtSeguradoraLider 
            Height          =   300
            Left            =   3540
            Locked          =   -1  'True
            TabIndex        =   32
            Top             =   2790
            Width           =   3585
         End
         Begin VB.TextBox txtApoliceLider 
            Height          =   300
            Left            =   7230
            Locked          =   -1  'True
            TabIndex        =   33
            Top             =   2790
            Width           =   1455
         End
         Begin VB.TextBox txtPropostaBB 
            Height          =   300
            Left            =   150
            Locked          =   -1  'True
            TabIndex        =   17
            Top             =   1020
            Width           =   1575
         End
         Begin Mask2S.ConMask2S mskLimRetResseguro 
            Height          =   300
            Left            =   3510
            TabIndex        =   309
            Top             =   2160
            Width           =   1935
            _ExtentX        =   3413
            _ExtentY        =   529
            mask            =   ""
            text            =   "0,00"
            locked          =   0   'False
            enabled         =   -1  'True
         End
         Begin VB.Label lblTp_Documento 
            Caption         =   "Tipo Documento"
            Height          =   285
            Left            =   5350
            TabIndex        =   359
            Top             =   3465
            Width           =   1320
         End
         Begin VB.Label lblPropostaSC 
            AutoSize        =   -1  'True
            Caption         =   "N� da Proposta SC"
            Height          =   195
            Left            =   150
            TabIndex        =   338
            Top             =   3480
            Width           =   1335
         End
         Begin VB.Label lblApoliceSC 
            AutoSize        =   -1  'True
            Caption         =   "N� da Apolice SC"
            Height          =   195
            Left            =   1920
            TabIndex        =   337
            Top             =   3480
            Width           =   1230
         End
         Begin VB.Label lblCertificadoSC 
            AutoSize        =   -1  'True
            Caption         =   "N� da Certificado SC"
            Height          =   195
            Left            =   3660
            TabIndex        =   336
            Top             =   3480
            Width           =   1455
         End
         Begin VB.Label Label5246 
            AutoSize        =   -1  'True
            Caption         =   "Lim. Reten��o Resseguro :"
            Height          =   195
            Index           =   1
            Left            =   3600
            TabIndex        =   264
            Top             =   1935
            Width           =   1935
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "No. Ordem End. L�der:"
            Height          =   195
            Index           =   15
            Left            =   7230
            TabIndex        =   60
            Top             =   1950
            Width           =   1605
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "Endosso L�der N� :"
            Height          =   195
            Index           =   14
            Left            =   5595
            TabIndex        =   59
            Top             =   1950
            Width           =   1350
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "Seguradora L�der :"
            Height          =   195
            Index           =   18
            Left            =   3540
            TabIndex        =   63
            Top             =   2550
            Width           =   1335
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "No. Ordem :"
            Height          =   195
            Index           =   16
            Left            =   180
            TabIndex        =   61
            Top             =   2550
            Width           =   855
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "Data da Emiss�o :"
            Height          =   195
            Index           =   17
            Left            =   1845
            TabIndex        =   62
            Top             =   2550
            Width           =   1290
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "Ap�lice L�der :"
            Height          =   195
            Index           =   19
            Left            =   7230
            TabIndex        =   64
            Top             =   2550
            Width           =   1035
         End
         Begin VB.Label Label9858 
            AutoSize        =   -1  'True
            Caption         =   "Lim. M�x. Indeniza��o :"
            Height          =   195
            Index           =   0
            Left            =   1845
            TabIndex        =   58
            Top             =   1950
            Width           =   1680
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "Data Emiss�o Ap�lice :"
            Height          =   195
            Index           =   13
            Left            =   150
            TabIndex        =   57
            Top             =   1950
            Width           =   1635
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "Proposta Anterior :"
            Height          =   195
            Index           =   4
            Left            =   7230
            TabIndex        =   51
            Top             =   795
            Width           =   1305
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "Data da Contrata��o:"
            Height          =   195
            Index           =   7
            Left            =   1845
            TabIndex        =   49
            Top             =   795
            Width           =   1530
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "Proposta BB :"
            Height          =   195
            Index           =   6
            Left            =   150
            TabIndex        =   48
            Top             =   795
            Width           =   975
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "Produto :"
            Height          =   195
            Index           =   2
            Left            =   1845
            TabIndex        =   46
            Top             =   195
            Width           =   645
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "Situa��o :"
            Height          =   195
            Index           =   5
            Left            =   3525
            TabIndex        =   50
            Top             =   795
            Width           =   720
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "Proposta :"
            Height          =   195
            Index           =   1
            Left            =   150
            TabIndex        =   13
            Top             =   195
            Width           =   720
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "In�cio de Vig�ncia :"
            Height          =   195
            Index           =   9
            Left            =   5595
            TabIndex        =   55
            Top             =   1365
            Width           =   1380
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "Fim de Vig�ncia :"
            Height          =   195
            Index           =   8
            Left            =   7230
            TabIndex        =   56
            Top             =   1365
            Width           =   1215
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "Ap�lice :"
            Height          =   195
            Index           =   12
            Left            =   165
            TabIndex        =   52
            Top             =   1365
            Width           =   615
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "Endosso N� :"
            Height          =   195
            Index           =   11
            Left            =   1845
            TabIndex        =   53
            Top             =   1365
            Width           =   930
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "Data do Endosso :"
            Height          =   195
            Index           =   10
            Left            =   3525
            TabIndex        =   54
            Top             =   1365
            Width           =   1320
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "Ramo :"
            Height          =   195
            Index           =   3
            Left            =   5790
            TabIndex        =   47
            Top             =   195
            Width           =   510
         End
      End
      Begin VB.Frame Frame2 
         Caption         =   "Proponente "
         Height          =   3150
         Left            =   600
         TabIndex        =   65
         Top             =   5490
         Width           =   8895
         Begin VB.TextBox txtProfissao 
            Height          =   285
            Left            =   645
            Locked          =   -1  'True
            TabIndex        =   357
            Top             =   2760
            Width           =   6495
         End
         Begin VB.TextBox txtProfissaoCbo 
            Height          =   285
            Left            =   165
            Locked          =   -1  'True
            TabIndex        =   356
            Top             =   2760
            Width           =   495
         End
         Begin VB.CheckBox chkPPE 
            Caption         =   "PPE"
            Height          =   240
            Left            =   8100
            TabIndex        =   321
            Top             =   525
            Visible         =   0   'False
            Width           =   615
         End
         Begin VB.TextBox TxtBairro 
            Height          =   285
            Left            =   6750
            Locked          =   -1  'True
            TabIndex        =   37
            Top             =   1020
            Width           =   1965
         End
         Begin VB.TextBox txtNome 
            Height          =   285
            Left            =   1725
            Locked          =   -1  'True
            TabIndex        =   35
            Top             =   475
            Width           =   6225
         End
         Begin VB.TextBox txtCPF_CGC 
            Height          =   285
            Left            =   4620
            Locked          =   -1  'True
            TabIndex        =   43
            Top             =   2130
            Width           =   2520
         End
         Begin VB.TextBox txtNascimento 
            Height          =   285
            Left            =   7365
            Locked          =   -1  'True
            TabIndex        =   44
            Top             =   2130
            Width           =   1335
         End
         Begin VB.TextBox txtEndereco 
            Height          =   285
            Left            =   160
            Locked          =   -1  'True
            TabIndex        =   36
            Top             =   1020
            Width           =   6375
         End
         Begin VB.TextBox txtCidade 
            Height          =   285
            Left            =   160
            Locked          =   -1  'True
            TabIndex        =   38
            Top             =   1560
            Width           =   4320
         End
         Begin VB.TextBox txtUF 
            Height          =   285
            Left            =   6765
            Locked          =   -1  'True
            TabIndex        =   40
            Top             =   1560
            Width           =   375
         End
         Begin VB.TextBox txtCEP 
            Height          =   285
            Left            =   7365
            Locked          =   -1  'True
            TabIndex        =   75
            Top             =   1560
            Width           =   1335
         End
         Begin VB.TextBox txtDDD 
            Height          =   285
            Left            =   4620
            Locked          =   -1  'True
            TabIndex        =   45
            Top             =   1575
            Width           =   495
         End
         Begin VB.TextBox txtTelefone 
            Height          =   285
            Left            =   5340
            Locked          =   -1  'True
            TabIndex        =   39
            Top             =   1560
            Width           =   1215
         End
         Begin VB.TextBox txtTpPessoa 
            Height          =   285
            Left            =   645
            Locked          =   -1  'True
            TabIndex        =   42
            Top             =   2130
            Width           =   3840
         End
         Begin VB.TextBox txtTpPessoaId 
            Height          =   285
            Left            =   160
            Locked          =   -1  'True
            TabIndex        =   41
            Top             =   2130
            Width           =   495
         End
         Begin VB.TextBox txtCodCliente 
            Height          =   285
            Left            =   160
            Locked          =   -1  'True
            TabIndex        =   34
            Top             =   480
            Width           =   1335
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "Atividade :"
            Height          =   195
            Index           =   83
            Left            =   165
            TabIndex        =   358
            Top             =   2520
            Width           =   750
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "Bairro :"
            Height          =   195
            Index           =   27
            Left            =   6750
            TabIndex        =   69
            Top             =   810
            Width           =   495
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "Nome :"
            Height          =   195
            Index           =   0
            Left            =   1725
            TabIndex        =   67
            Top             =   270
            Width           =   510
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "CPF / CGC :"
            Height          =   195
            Index           =   29
            Left            =   4605
            TabIndex        =   77
            Top             =   1920
            Width           =   885
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "Data Nascimento :"
            Height          =   195
            Index           =   28
            Left            =   7380
            TabIndex        =   78
            Top             =   1920
            Width           =   1320
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "Endere�o :"
            Height          =   195
            Index           =   21
            Left            =   165
            TabIndex        =   68
            Top             =   810
            Width           =   780
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "Cidade :"
            Height          =   195
            Index           =   22
            Left            =   180
            TabIndex        =   70
            Top             =   1350
            Width           =   585
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "UF :"
            Height          =   195
            Index           =   25
            Left            =   6765
            TabIndex        =   73
            Top             =   1350
            Width           =   300
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "CEP :"
            Height          =   195
            Index           =   26
            Left            =   7365
            TabIndex        =   74
            Top             =   1350
            Width           =   405
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "DDD :"
            Height          =   195
            Index           =   23
            Left            =   4620
            TabIndex        =   71
            Top             =   1350
            Width           =   450
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "Telefone :"
            Height          =   195
            Index           =   24
            Left            =   5340
            TabIndex        =   72
            Top             =   1350
            Width           =   720
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "Tipo Pessoa :"
            Height          =   195
            Index           =   30
            Left            =   165
            TabIndex        =   76
            Top             =   1890
            Width           =   975
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "C�digo Cliente :"
            Height          =   195
            Index           =   20
            Left            =   165
            TabIndex        =   66
            Top             =   270
            Width           =   1110
         End
      End
      Begin MSFlexGridLib.MSFlexGrid GridResseguro 
         Height          =   5055
         Left            =   -74730
         TabIndex        =   215
         Top             =   1740
         Width           =   9585
         _ExtentX        =   16907
         _ExtentY        =   8916
         _Version        =   393216
         Rows            =   1
         Cols            =   7
         FixedCols       =   0
         AllowUserResizing=   1
         FormatString    =   $"ConPropBasicaVida.frx":18B9
      End
      Begin MSFlexGridLib.MSFlexGrid GridProposta 
         Height          =   4770
         Left            =   -74850
         TabIndex        =   271
         Top             =   1485
         Width           =   9765
         _ExtentX        =   17224
         _ExtentY        =   8414
         _Version        =   393216
         Cols            =   7
         FixedRows       =   0
         FixedCols       =   0
         GridLines       =   2
         SelectionMode   =   1
         AllowUserResizing=   1
      End
      Begin MSFlexGridLib.MSFlexGrid grdHistorico 
         Height          =   2745
         Left            =   -74880
         TabIndex        =   305
         Top             =   1320
         Width           =   9795
         _ExtentX        =   17277
         _ExtentY        =   4842
         _Version        =   393216
         Cols            =   5
         FixedCols       =   0
         FocusRect       =   0
         HighLight       =   0
         SelectionMode   =   1
         FormatString    =   $"ConPropBasicaVida.frx":196B
      End
      Begin VB.Frame FraCosseguroCedido 
         Caption         =   " Cosseguro Cedido "
         Height          =   5970
         Left            =   -74835
         TabIndex        =   206
         Top             =   1425
         Width           =   9570
         Begin VB.Frame Frame9 
            Caption         =   "Agendamento cosseguro"
            Height          =   4860
            Left            =   210
            TabIndex        =   322
            Top             =   885
            Width           =   9300
            Begin MSFlexGridLib.MSFlexGrid gridCobrancaCosseguro 
               Height          =   4440
               Left            =   180
               TabIndex        =   323
               Top             =   285
               Width           =   8970
               _ExtentX        =   15822
               _ExtentY        =   7832
               _Version        =   393216
               Rows            =   1
               Cols            =   13
               FixedCols       =   0
               AllowBigSelection=   0   'False
               HighLight       =   0
               FormatString    =   $"ConPropBasicaVida.frx":1A50
            End
         End
         Begin VB.ComboBox cmbCongenere 
            Height          =   315
            Left            =   210
            Style           =   2  'Dropdown List
            TabIndex        =   208
            Top             =   480
            Width           =   4245
         End
         Begin VB.TextBox txtPercDespesa 
            Height          =   300
            Left            =   7440
            TabIndex        =   214
            Top             =   480
            Width           =   1215
         End
         Begin VB.TextBox txtPercParticipacao 
            Height          =   300
            Left            =   5970
            Locked          =   -1  'True
            TabIndex        =   212
            Top             =   480
            Width           =   1335
         End
         Begin VB.TextBox txtNumOrdem 
            Height          =   300
            Left            =   4530
            Locked          =   -1  'True
            TabIndex        =   210
            Top             =   480
            Width           =   1335
         End
         Begin VB.Label Label1 
            Caption         =   "Cong�nere"
            Height          =   255
            Index           =   73
            Left            =   240
            TabIndex        =   207
            Top             =   240
            Width           =   2535
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "Perc.Desp. L�der:"
            Height          =   195
            Index           =   76
            Left            =   7425
            TabIndex        =   213
            Top             =   240
            Width           =   1260
         End
         Begin VB.Label Label1 
            Caption         =   "Perc.Participa��o:"
            Height          =   255
            Index           =   75
            Left            =   5970
            TabIndex        =   211
            Top             =   240
            Width           =   1410
         End
         Begin VB.Label Label1 
            Caption         =   "N�mero de Ordem:"
            Height          =   255
            Index           =   74
            Left            =   4530
            TabIndex        =   209
            Top             =   240
            Width           =   1455
         End
      End
      Begin VB.Frame FraSubGrupo 
         Caption         =   "SubGrupos"
         Height          =   1005
         Index           =   5
         Left            =   -74730
         TabIndex        =   324
         Top             =   1650
         Width           =   9150
         Begin VB.ComboBox cboSubGrupo 
            Height          =   315
            Index           =   5
            Left            =   210
            Style           =   2  'Dropdown List
            TabIndex        =   325
            Top             =   510
            Width           =   8445
         End
         Begin VB.Label Label1 
            Caption         =   "SubGrupo :"
            Height          =   255
            Index           =   72
            Left            =   240
            TabIndex        =   326
            Top             =   270
            Width           =   1515
         End
      End
      Begin VB.PictureBox PETOCX1 
         Height          =   480
         Left            =   -74700
         ScaleHeight     =   420
         ScaleWidth      =   1140
         TabIndex        =   327
         Top             =   7260
         Visible         =   0   'False
         Width           =   1200
      End
      Begin VB.Label Label69 
         Caption         =   "* Fatura agregada"
         Height          =   255
         Left            =   -66720
         TabIndex        =   288
         Top             =   7440
         Width           =   1455
      End
      Begin VB.Label Label68 
         Caption         =   "Total de Vidas:"
         Height          =   240
         Index           =   2
         Left            =   -70635
         TabIndex        =   267
         Top             =   7380
         Width           =   1140
      End
      Begin VB.Label Label68 
         Caption         =   "Vidas Inativas:"
         Height          =   240
         Index           =   1
         Left            =   -72570
         TabIndex        =   266
         Top             =   7380
         Width           =   1095
      End
      Begin VB.Label Label68 
         Caption         =   "Vidas Ativas:"
         Height          =   240
         Index           =   0
         Left            =   -74460
         TabIndex        =   265
         Top             =   7380
         Width           =   1005
      End
   End
   Begin MSComDlg.CommonDialog dialog 
      Left            =   1440
      Top             =   9405
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
End
Attribute VB_Name = "ConPropBasicaVida"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

' Demanda 19310624 - EXCLUIR O CAMPO PPE DO SEGP0223
' O objeto ChkPPE teve sua propriedade VISIBLE alterada para FALSE para atendimento da demanda.
' Optou-se por apenas inibir sua visualiza��o ao inv�s de exclui-lo definitivamente para o caso de futura reativa��o dessa funcionalidade.

'Febner - > Variavel para verificacao do tipo de proposta
Dim Prop_Adesao As Boolean

Dim dt_contratacao As String
Dim dt_emissao As String
Dim PropostaBasica As String
Dim ProcessaCobranca As String
Dim TabSeguro As String
Dim TabEscolha As String
Dim vCodSusep As Integer

'Variaveis para pegar o tipo de documento
Dim iTpDocumentoId As Integer  'AFONSO - G&P - 15/05/2008
Dim iDocumentoId As Integer    'AFONSO - G&P - 15/05/2008
''
Public CPF_CNPJ_saida As String
Public personalizacao_fatura_id As Long
Public personalizacao_id As Long
Public MostraBoleto As Boolean
'--
Public vFaturaId As String    'AFONSO - G&P - 15/05/2008
Public DtIniVigFatura As String    'AFONSO - G&P - 15/05/2008
Public NumRows As Long                 'AFONSO - G&P - 16/05/2008
Public NumRows2 As Long                 'AFONSO - G&P - 16/05/2008
Public Sim As Integer                 'AFONSO - G&P - 16/05/2008
Public Retorno As Variant                 'AFONSO - G&P - 16/05/2008
Public Cat_Cliente As Integer    'AFONSO - G&P - 16/05/2008
Public txtTpContrato As String    'AFONSO - G&P - 16/05/2008
Public txtSeguroAnterior As String    'AFONSO - G&P - 16/05/2008
Public TxtHouveSinistro As String    'AFONSO - G&P - 16/05/2008
Public RamoId As Integer                 'AFONSO - G&P - 16/05/2008
Public Texto As String                 'AFONSO - G&P - 16/05/2008
Public Texto2 As String                 'AFONSO - G&P - 16/05/2008
Public subgrupo_id As Long    'AFONSO - G&P - 16/05/2008
Public DB_DadosBeneficiario_aux As String       'AFONSO - G&P - 19/05/2008
Public num_solicitacao As Long    'AFONSO - G&P - 20/05/2008
'
Public endosso_cancel As Boolean    ' mc_amaral 22/10/201317860335 - Melhorias no SEGBR(MP13?) - Acelerar aplicativos de consulta acessados pelo Sinistro



'Genesco Silva Confitec SP - Flow 4532893 10/03/2011 - Vari�veis para armazenar o endere�o do estipulante
Public vLembraProd As Integer
Public vLembraRamo As Integer



Dim Sortable As Integer    'AFONSO - G&P - 19/05/2008
Dim vTitulo_R93 As String    'AFONSO - G&P - 19/05/2008
Dim vTitulo_R82 As String    'AFONSO - G&P - 19/05/2008
Dim vTipoDoc As String    'AFONSO - G&P - 19/05/2008
Dim vCorretora_bb As String    'AFONSO - G&P - 19/05/2008
Dim ImprimeOk As Boolean    'AFONSO - G&P - 04/06/2008
Dim vAceito As Boolean
Dim vSubGrupoOk As Boolean
Dim vCongenereOk As Boolean
Public ImprimirTela As Boolean    'AFONSO - G&P - 19/05/2008
'
Dim Cobrancas As New Collection
'
'' Constantes para tratamento dos tabs
Const TAB_PROPOSTA = 0
Const TAB_SUBGRUPO = 1
Const TAB_CORRETAGEM = 2
Const TAB_ADMINISTRACAO = 3
Const TAB_AGENCIAMENTO = 4
Const TAB_COMPLEMENTO = 5
Const TAB_PARAMETROS = 6
Const TAB_COBERTURAS = 7
Const TAB_CLAUSULA = 8
Const TAB_SINISTRO = 9
Const TAB_ENDOSSO = 10
Const TAB_COBRANCA = 11
Const TAB_COSSEGURO = 12
Const TAB_RESSEGURO = 13
Const TAB_ASSISTENCIA = 14
Const TAB_VIDAS_SEGURADAS = 15
'lpinto
Const TAB_EXTRATO_SEGURO = 16
Const TAB_CERTIFICADOS = 17
''pcarvalho - 18/10/2005
Const TAB_HISTORICO = 18
''Sergey Souza - 10/09/2010
Const TAB_QUESTIONARIO = 19
'' Constantes para as colunas dos grids
Const GRD_ENDO_NUM = 0

'Barney - 12/05/2004
'Const GRD_ENDO_DTINI = 1
'Const GRD_ENDO_DTEMI = 2
Const GRD_ENDO_DTEMI = 1
Const GRD_ENDO_DTINI = 2

Const GRD_ENDO_TPENDO = 4   'Por Daniel Romualdo - 06/11/2003
Const GRD_ENDO_DTIMP = 5    'Por Daniel Romualdo - 06/11/2003
Const GRD_ENDO_DESC = 6     'Por Daniel Romualdo - 06/11/2003
Const GRD_TP_ALT_END = 10     'rfarzat1
'' Constantes para as colunas do grid de faixa et�ria sub grupos
Const GRD_FAIXA_IDADEMINIMA = 0
Const GRD_FAIXA_IDADEMAXIMA = 1
Const GRD_FAIXA_IS = 2
Const GRD_FAIXA_PREMIOMENSAL = 3
Const GRD_FAIXA_PREMIOMENSALCONJ = 4
Const GRD_FAIXA_PREMIOTARIFA = 5
Const GRD_FAIXA_PREMIOTARIFACONJ = 6
Const GRD_FAIXA_TAXA = 7
Const GRD_FAIXA_TAXACONJ = 8
Const GRD_FAIXA_PREMIO = 4

'Adenilson (20/05/2003) Declara��o de constantes
'Constantes para as colunas do grid de faixa planos sub grupo
Const GRD_FXPLANO_IDADEMINIMA = 0
Const GRD_FXPLANO_IDADEMAXIMA = 1
Const GRD_FXPLANO_IS = 2
Const GRD_FXPLANO_TAXA = 3
Const GRD_FXPLANO_PREMIO = 4

'' Constantes para as colunas do grid de subgrupos
Const GRD_SUB_ID = 0
Const GRD_SUB_NOME = 1
Const GRD_SUB_CNPJ = 2    'Genesco Silva - Confitec - Flow 4532893
Const GRD_SUB_DTINICIOSUB = 3
Const GRD_SUB_TPIS = 4
Const GRD_SUB_MULTSAL = 5
Const GRD_SUB_QTDVIDAS = 6
Const GRD_SUB_TABUAID = 7
Const GRD_SUB_TABUA = 8
Const GRD_SUB_FATAUTO = 9
Const GRD_SUB_TEXTOPADRAO = 10
Const GRD_SUB_DIAFAT = 11
Const GRD_SUB_DIACOB = 12
Const GRD_SUB_PERPGTO = 13
Const GRD_SUB_FORMAPGTO = 14
Const GRD_SUB_AGDEBITO = 15
Const GRD_SUB_CONTA = 16
Const GRD_SUB_CONJUGE = 17
Const GRD_SUB_ISENTOIOF = 18
Const GRD_SUB_IDADEMINIMPL = 19
Const GRD_SUB_IDADEMAXIMPL = 20
Const GRD_SUB_IDADEMINMOV = 21
Const GRD_SUB_IDADEMAXMOV = 22
Const GRD_SUB_CARTAOPROPOSTA = 23
Const GRD_SUB_CRITICATODOS = 24
Const GRD_SUB_CRITICACAPITAL = 25
Const GRD_SUB_CRITICAIDADE = 26
Const GRD_SUB_MAXIDADE = 27
Const GRD_SUB_MAXCAPITAL = 28
Const GRD_SUB_TPFATURA = 29
Const GRD_SUB_TPVIDA = 30
Const GRD_SUB_TPCUSTEIO = 31
Const GRD_SUB_REENQUAD = 32
Const GRD_SUB_EXCLUSAO = 33
Const GRD_SUB_SITUACAO = 34

'Genesco Silva - Confitec SP - Constante de Visualiza��o - 10/03/2011
'CheckBox no GridAdministra��o, indicando se o endere�o v�lido � do subgrupo
Const Sub_Marcado = "�"
Const Sub_Desmarcado = "q"
'Genesco Silva End

Dim entrada    ' VARIAVEL DE CONTROLE PARA A IMPRESS�O DE CERTIFICADO
Dim ENTRADA2    ' VARIAVEL DE CONTROLE PARA A IMPRESS�O DE CERTIFICADO
Dim pagina As Integer
Public Usuario As String
Dim bd_cob As Variant
Dim c_nomeConjuge As String
'Dim gerou As Boolean 'variavel para controle da impressao de  certificado
Dim ED_NomeEstipulante As String
Dim ED_CNPJEstipulante As String
Dim ED_NomeSubEstipulante As String
Dim ED_CNPJSubEstipulante As String
'lpinto
Dim bPropostaRecusada As Boolean
Dim bPropostaNaoEmitida As Boolean
Dim bPropostaEstudo As Boolean    'rmaiellaro (19/01/2007)
Public sPropostaBB As String
Public sArquivo As String
''pcarvalho - 20/10/2005
Private sTodasDescricoes() As String

'RRAMOS - 2007/06/26 - Flow 220222 - Vari�veis utilizadas para a funcionalidade dos bot�es de navega��o.
Dim lngLinhasGridCobranca As Long
Dim lngLinMIN As Long
Dim lngLinMAX As Long

'lrocha 24/08/2008 - Demanda 273032       ''''''******''''''
Dim oServico As Object                    'SEGL0308.Cls00490
Dim oPlanoAssistencia As Object           'SEGL0308.Cls00491
Dim oSubGrupo As Object                   'SEGL0308.Cls00492
Dim oApoliceAssistenciaSubGrupo As Object    'SEGL0308.Cls00493
Dim oAssistencia As Object                'SEGL0308.Cls00495
'''''''''''''''''''''''''''''''''''''''''''

' Jos� Moreira (Nova Consultoria) - 10/11/2012 - 14620257 - Melhorias no processo de subscri��o
' Fechar o programa caso o detalhe tenha sido aberto via par�metros
Public PodeFecharPrograma As Boolean
Dim RegistraBoleto As String    'MARCIO.NOGUEIRA - CONFITEC SISTEMAS - 25/07/2017 PROJETO: 19368999 - cobran�a registrada AB e ABS

'Isabeli Silva - Confitec SP - 2017-10-09
'MU-2017-036534 Cadastramento dos Corretores Exclusivos no SEGBR
Dim isCorretor As Boolean
Dim CorretorProposta As Boolean


Sub Monta_GridFaturas()
'RRAMOS - 2007/06/26 - Flow 220222

    Dim linha As String

    Dim Item As Long
    Dim i As Long
    Dim cFaturaFiltro As Collection

    Screen.MousePointer = vbHourglass

    GridFaturas.Rows = 1

    cmdPagina(0).Enabled = False  'Bot�es desabilitados neste ponto para que o usu�rio n�o fique "clicando" enquanto o grid � carregado.
    cmdPagina(1).Enabled = False
    cmdPagina(2).Enabled = False
    cmdPagina(3).Enabled = False

    lngLinMAX = IIf(Faturas.Count > lngLinMAX, lngLinMAX, Faturas.Count)    'Ajustar a quantidade m�xima para que a mesma nunca seja maior que quantidade total de registros existentes.

    Set cFaturaFiltro = New Collection

    For i = lngLinMIN To lngLinMAX
        cFaturaFiltro.Add Faturas.Item(i)    'Esta collection foi criada para conter apenas os registros a serem mostrados no grid, pois ao utilizar a collection principal (Faturas) para propostas com muitas faturas estava demorando muito para carregar o grid. (ex: ap�lice 4 - ramo 77)
    Next i

    For Item = 1 To cFaturaFiltro.Count

        With cFaturaFiltro(Item)
            linha = .EndossoId
            DoEvents
            'para o caso de faturas agregadas
            If eAgregada(CInt(vRamoId), CLng(vApoliceId), CLng(vSubGrupoId), CLng(.Id)) Then
                linha = linha & vbTab & "*" & .Id
            Else
                linha = linha & vbTab & .Id
            End If

            linha = linha & vbTab & .NossoNumero
            linha = linha & vbTab & Format(.ValIS, "###,###,##0.00")
            linha = linha & vbTab & Format(.ValPremio, "###,###,##0.00")
            linha = linha & vbTab & Format(.ValIOF, "###,###,##0.00")
            linha = linha & vbTab & Format(.DtInicioVigencia, "dd/mm/yyyy")
            linha = linha & vbTab & Format(.DtFimVigencia, "dd/mm/yyyy")
            linha = linha & vbTab & Format(.DtRecebimento, "dd/mm/yyyy")
            linha = linha & vbTab & .Situacao
            linha = linha & vbTab & Format(.PercCorretagem, "#0.00000")
            linha = linha & vbTab & Format(.PercProLabore, "#0.00000")
            linha = linha & vbTab & .Observacao
            'Luciana - 27/08/2003
            If .DtBaixa <> "00:00:00" Then
                linha = linha & vbTab & Format(.DtBaixa, "dd/mm/yyyy")
            Else
                linha = linha & vbTab & ""
            End If
            linha = linha & vbTab & .OriBaixa
            linha = linha & vbTab & .QtdeVidas
            linha = linha & vbTab

        End With

        GridFaturas.AddItem linha

    Next

    GridFaturas.ColAlignment(1) = flexAlignRightCenter

    GridFaturas.Refresh

    cmdPagina(0).Enabled = IIf(lngLinMIN > 1, True, False)
    cmdPagina(1).Enabled = IIf(lngLinMIN > 1, True, False)
    cmdPagina(2).Enabled = IIf(Faturas.Count > lngLinMAX, True, False)
    cmdPagina(3).Enabled = IIf(Faturas.Count > lngLinMAX, True, False)

    Set cFaturaFiltro = Nothing

    Screen.MousePointer = vbDefault


    '''C�digo utilizado at� a vers�o 179
    '''
    '''Dim linha As String
    '''Dim vFatura As Fatura
    '''
    '''GridFaturas.Rows = 1
    '''
    '''For Each vFatura In Faturas
    '''    With vFatura
    '''        linha = .EndossoId
    '''        DoEvents
    '''        'para o caso de faturas agregadas
    '''        If eAgregada(CInt(vRamoId), CLng(vApoliceId), CInt(vSubGrupoId), CInt(.Id)) Then
    '''            linha = linha & vbTab & "*" & .Id
    '''        Else
    '''            linha = linha & vbTab & .Id
    '''        End If
    '''
    '''        linha = linha & vbTab & .NossoNumero
    '''        linha = linha & vbTab & Format(.ValIS, "###,###,##0.00")
    '''        linha = linha & vbTab & Format(.ValPremio, "###,###,##0.00")
    '''        linha = linha & vbTab & Format(.ValIOF, "###,###,##0.00")
    '''        linha = linha & vbTab & Format(.DtInicioVigencia, "dd/mm/yyyy")
    '''        linha = linha & vbTab & Format(.DtFimVigencia, "dd/mm/yyyy")
    '''        linha = linha & vbTab & Format(.DtRecebimento, "dd/mm/yyyy")
    '''        linha = linha & vbTab & .Situacao
    '''        linha = linha & vbTab & Format(.PercCorretagem, "#0.00000")
    '''        linha = linha & vbTab & Format(.PercProLabore, "#0.00000")
    '''        linha = linha & vbTab & .Observacao
    '''        'Luciana - 27/08/2003
    '''        If .DtBaixa <> "00:00:00" Then
    '''            linha = linha & vbTab & Format(.DtBaixa, "dd/mm/yyyy")
    '''        Else
    '''            linha = linha & vbTab & ""
    '''        End If
    '''        linha = linha & vbTab & .OriBaixa
    '''        linha = linha & vbTab
    '''    End With
    '''    '
    '''    GridFaturas.AddItem linha
    '''
    '''Next
    '''GridFaturas.ColAlignment(1) = flexAlignRightCenter
    '''
    '''GridFaturas.Refresh

End Sub

'Renato Vasconcelos
'Flow 14735947
'Inicio
Private Sub btnGravarTpSegurado_Click()

    On Error GoTo Erro

    If cboTpSegurado.ListIndex < 0 Then
        MsgBox "Selecione um tipo de Segurado!", vbInformation, "SEGP0223"
        Exit Sub
    End If
    Dim SQL As String
    Dim i As Integer
    Dim tpSegurado As String
    i = cboTpSegurado.ItemData(cboTpSegurado.ListIndex)
    Select Case i
    Case 1
        tpSegurado = "V"
    Case 2
        tpSegurado = "P"
    Case 3
        tpSegurado = "C"
    End Select


    SQL = "EXECUTE SEGS10438_SPI " & vbNewLine & _
          "@proposta_id =" & txtNumero.Text & vbNewLine & _
          ",@tp_segurado_proposta = '" & tpSegurado & "'" & vbNewLine & _
          ",@usuario = '" & Usuario & "'"
    rdoCn1.Execute (SQL)
    MsgBox "Tipo de Segurado Alterado com sucesso!", vbInformation, "SEGP0223"
    Exit Sub
Erro:
    TrataErroGeral "Gravar Tipo Segurado"
    MsgBox "Rotina: Gravar Tipo Segurado"
End Sub
'Fim

Private Sub CboSubGrupo_Click(Index As Integer)
    Dim ind_sub As String
    StatusBar1.SimpleText = "Aguarde..."
    '
    vSubGrupoOk = False
    vSubGrupoId = "-1"
    If cboSubGrupo(Index).ListIndex <> -1 Then
        ind_sub = CStr(cboSubGrupo(Index).ItemData(cboSubGrupo(Index).ListIndex))
        vSubGrupoId = ind_sub
        vSubGrupoOk = True
        '
        Select Case Index
        Case 0  'Corretagem
            Set Corretores = Nothing
            '
            Set Corretores = SubGrupos(ind_sub).Corretores
            Monta_GridCorretores

        Case 1  'Parametros
            Set Componentes = Nothing
            '
            Set Componentes = SubGrupos(ind_sub).Componentes
            '
            Monta_CboComponente
            Monta_CboComponenteQuestionario

            ''
            TxtTbCalcId.Text = SubGrupos(ind_sub).IdTabua
            TxtTbCalculo.Text = SubGrupos(ind_sub).NomeTabua
            txtQtdVida.Text = SubGrupos(ind_sub).QtdVidas

        Case 2  'Coberturas
            Set Coberturas = Nothing
            Set Componentes = Nothing
            '
            cboObjSegurado.Clear
            GridCoberturas.Rows = 1
            '
            Set Componentes = SubGrupos(ind_sub).Componentes
            Set Coberturas = SubGrupos(ind_sub).Coberturas
            '
            Monta_CboObjSegurado
        Case 3  'Administra��o
            Set Estipulantes = Nothing
            '
            Set Estipulantes = SubGrupos(ind_sub).Estipulantes

            '
            Monta_CboEstipulante

        Case 4  'Cobran�a
            Set Faturas = Nothing
            '
            Set Faturas = SubGrupos(ind_sub).Faturas

            'RRAMOS - 2007/06/26 - Flow 220222
            lngLinMIN = 1
            lngLinMAX = lngLinhasGridCobranca

            cboSubGrupo(4).Enabled = False
            Monta_GridFaturas
            cboSubGrupo(4).Enabled = True
            cboSubGrupo(4).SetFocus


        Case 5  ' Cosseguro Aceito
            If vCongenereOk Then
                Set Faturas = Nothing
                Set Faturas = SubGrupos(vSubGrupoId).Faturas
                'Call Monta_GridFaturas_Cosseguro(Val(txtPercParticipacao.Text), Val(txtPercDespesa.Text))
            End If

        Case 6  ' Agenciamento
            If SubGrupos(vSubGrupoId).IdAgenciador <> "" Then
                TxtCodAgenciador.Text = Format(SubGrupos(vSubGrupoId).IdAgenciador, "###000000")
                TxtAgenciador.Text = SubGrupos(vSubGrupoId).NomeAgenciador
                TxtPercAgPrim.Text = Format(SubGrupos(vSubGrupoId).PercAgenciamento, "#0.00")
                TxtFPAgPrim.Text = FP_Agenciamento(1)
                TxtNoParcAgPrim.Text = SubGrupos(vSubGrupoId).NoParcAgenciamento
                If SubGrupos(vSubGrupoId).PercAgDemais > 0 Then
                    TxtPercAgDemais.Text = Format(SubGrupos(vSubGrupoId).PercAgDemais, "#0.00")
                    TxtFPAgDemais.Text = FP_Agenciamento(SubGrupos(vSubGrupoId).FormaPgtoAgDemais)
                    TxtNoParcAgDemais.Text = SubGrupos(vSubGrupoId).NoParcAgDemais
                End If
            Else
                TxtCodAgenciador.Text = ""
                TxtAgenciador.Text = ""
                TxtPercAgPrim.Text = ""
                TxtFPAgPrim.Text = ""
                TxtNoParcAgPrim.Text = ""
                TxtPercAgDemais.Text = ""
                TxtFPAgDemais.Text = ""
                TxtNoParcAgDemais.Text = ""
            End If
        Case 7
            Set Assistencias = Nothing

            Set Assistencias = SubGrupos(ind_sub).Assistencias

            'lrocha 29/10/2008 - Demanda 273032
            Monta_Grid_Assistencia

        Case 8

            Set Vidas_Seguradas = Nothing

            Set Vidas_Seguradas = SubGrupos(ind_sub).Vidas_Seguradas

            Check2(0).Value = 0
            Check2(1).Value = 0
            Check2(2).Value = 0
            MEBVigencia.Text = "__/____"

            Ler_Vidas_Seguradas

            'Monta_GridVidas_Seguradas

        Case Else
            MsgBox "SubGrupo n�o existente!", vbCritical
        End Select
        GridAgendamentoVidasSeguradas.Rows = 1

    End If
    StatusBar1.SimpleText = ""
End Sub

Private Sub cmdGerarPlanilha_Click()
    GerarPlanilha.Show vbModal
End Sub

Private Sub cmdPagina_Click(Index As Integer)
'RRAMOS - 2007/06/26 - Flow 220222 - Nova funcionalidade para limitar a quantidade de registros a serem carregados no grid.

    Select Case Index

    Case Is = 0
        lngLinMIN = 1
        lngLinMAX = lngLinhasGridCobranca

    Case Is = 1
        lngLinMIN = IIf((lngLinMIN - lngLinhasGridCobranca) < 1, 1, lngLinMIN - lngLinhasGridCobranca)
        lngLinMAX = lngLinMIN + (lngLinhasGridCobranca - 1)

    Case Is = 2
        lngLinMIN = lngLinMIN + lngLinhasGridCobranca
        lngLinMAX = lngLinMIN + (lngLinhasGridCobranca - 1)

    Case Is = 3
        lngLinMIN = Faturas.Count - (lngLinhasGridCobranca - 1)
        lngLinMAX = Faturas.Count

    End Select

    Call Monta_GridFaturas

End Sub

Private Sub Ler_Resseguro_Nova()

    Dim rs As rdoResultset
    Dim SQL As String
    Dim cod_resseg As Integer
    Dim cod_objeto As Integer
    Dim Item_Negociacao As String
    Dim Item_Financeiro As String
    Dim Premio_Excedente As String
    Dim Comissao_Excedente As String
    Dim Premio_Quota As String
    Dim Comissao_Quota As String
    Dim Premio_ED As String
    Dim Premio_Catastrofe As String
    Dim linha As String

    On Error GoTo Erro

    SQL = "  SELECT resseguro_objeto_plano_tb.resseguro_objeto_id, " & vbNewLine _
        & "         resseguro_objeto_tb.cod_objeto_segurado, " & vbNewLine _
        & "         contrato_plano_tb.cod_plano, " & vbNewLine _
        & "         resseguro_objeto_plano_tb.val_premio, " & vbNewLine _
        & "         resseguro_objeto_plano_tb.val_comissao " & vbNewLine _
        & "    FROM resseg_db..resseguro_objeto_tb resseguro_objeto_tb WITH (NOLOCK) , " & vbNewLine _
        & "         resseg_db..resseguro_objeto_plano_tb resseguro_objeto_plano_tb WITH (NOLOCK) , " & vbNewLine _
        & "         resseg_db..contrato_plano_tb contrato_plano_tb WITH (NOLOCK)  " & vbNewLine _
        & "   WHERE resseguro_objeto_tb.proposta_id = " & vPropostaId & vbNewLine _
        & "     AND resseguro_objeto_tb.resseguro_objeto_id = resseguro_objeto_plano_tb.resseguro_objeto_id " & vbNewLine _
        & "     AND contrato_plano_tb.contrato_plano_id = resseguro_objeto_plano_tb.contrato_plano_id " & vbNewLine _
        & "     AND situacao = 'S' " & vbNewLine _
        & "ORDER BY resseguro_objeto_tb.cod_objeto_segurado, " & vbNewLine _
        & "         contrato_plano_tb.cod_plano"


    Set rs = rdocn.OpenResultset(SQL)

    If Not rs.EOF Then
        SSTabProp.TabEnabled(TAB_RESSEGURO) = True
        GridResseguro.Rows = 1
        '
        While Not rs.EOF
            cod_resseg = rs("resseguro_objeto_id")
            cod_objeto = rs("cod_objeto_segurado")

            Do While cod_resseg = rs("resseguro_objeto_id")
                Item_Negociacao = rs("cod_plano")

                'resseguro excedente de responsabilidade
                If Item_Negociacao = 1 Then
                    Premio_Excedente = Format(Val(0 & rs("val_premio")), "#,###,###,##0.00")
                    Comissao_Excedente = Format(Val(0 & rs("val_comissao")), "#,###,###,##0.00")
                Else
                    Premio_Excedente = "0,00"
                    Comissao_Excedente = "0,00"
                End If
                'resseguro quota
                If Item_Negociacao = 2 Then
                    Premio_Quota = Format(Val(0 & rs("val_premio")), "#,###,###,##0.00")
                    Comissao_Quota = Format(Val(0 & rs("val_comissao")), "#,###,###,##0.00")
                Else
                    Premio_Quota = "0,00"
                    Comissao_Quota = "0,00"
                End If

                'resseguro excesso de danos
                If Item_Negociacao = 3 Then
                    Premio_ED = Format(Val(0 & rs("val_premio")), "#,###,###,##0.00")
                Else
                    Premio_ED = "0,00"
                End If

                'resseguro cat�strofe
                If Item_Negociacao = 4 Then
                    Premio_Catastrofe = Format(Val(0 & rs("val_premio")), "#,###,###,##0.00")
                Else
                    Premio_Catastrofe = "0,00"
                End If

                cod_resseg = rs("resseguro_objeto_id")

                rs.MoveNext
                If rs.EOF Then
                    Exit Do
                End If

            Loop

            linha = Format(cod_objeto, "0000")
            linha = linha & vbTab & Premio_Excedente
            linha = linha & vbTab & Comissao_Excedente
            linha = linha & vbTab & Premio_Quota
            linha = linha & vbTab & Comissao_Quota
            linha = linha & vbTab & Premio_ED
            linha = linha & vbTab & Premio_Catastrofe

            GridResseguro.AddItem linha
        Wend
    Else
        SSTabProp.TabEnabled(TAB_RESSEGURO) = False
    End If

    rs.Close
    Set rs = Nothing

    Exit Sub

Erro:
    TrataErroGeral "Ler Resseguro"
    MsgBox "Rotina: Ler Resseguro. Programa ser� Cancelado", vbCritical
    End
End Sub


Public Sub Monta_GridFaixaEtariaSubGrupo()

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'Defini��o: Esta rotina carrega grid com as faixas do subgrupo por faixa etaria.  '
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

    Dim rsFaixa As rdoResultset
    Dim sSQL As String

    On Error GoTo Trata_Erro

    'Redimensionando o grid ''''''''''''''''
    grdFaixas.Cols = 9
    grdFaixas.TextMatrix(0, GRD_FAIXA_IDADEMAXIMA) = "Idade M�xima"
    grdFaixas.TextMatrix(0, GRD_FAIXA_IDADEMINIMA) = "Idade M�nima"
    grdFaixas.TextMatrix(0, GRD_FAIXA_IS) = "IS"
    grdFaixas.TextMatrix(0, GRD_FAIXA_TAXA) = "Taxa Tarifa"
    grdFaixas.TextMatrix(0, GRD_FAIXA_PREMIOMENSAL) = "Pr�mio Mensal"
    grdFaixas.TextMatrix(0, GRD_FAIXA_PREMIOTARIFA) = "Pr�mio Tarifa"
    grdFaixas.TextMatrix(0, GRD_FAIXA_TAXACONJ) = "Taxa Tarifa C�njuge"
    grdFaixas.TextMatrix(0, GRD_FAIXA_PREMIOMENSALCONJ) = "Pr�mio Mensal C�njuge"
    grdFaixas.TextMatrix(0, GRD_FAIXA_PREMIOTARIFACONJ) = "Pr�mio Tarifa C�njuge"

    'Pesquisando dados''''''''''''''''''''''''''''''''''''

    'Agrig�rio - 23/10/2003 - A sele��o foi alterada para que mostre as faixas vigentes no periodo correspondente
    sSQL = ""
    adSQL sSQL, "SELECT  ISNULL(F.idade_min,0) idade_min,"
    adSQL sSQL, "    ISNULL(F.idade_max,0) idade_max,"
    adSQL sSQL, "    ISNULL(F.val_is,0) val_is,"
    adSQL sSQL, "    ISNULL(F.taxa_tarifa,0) taxa_tarifa,"
    adSQL sSQL, "    ISNULL(F.val_premio_mensal,0) val_premio_mensal,"
    adSQL sSQL, "    ISNULL(F.val_premio_tarifa,0) val_premio_tarifa,"
    adSQL sSQL, "    ISNULL(F.taxa_tarifa_conjuge,0) taxa_tarifa_conjuge,"
    adSQL sSQL, "    ISNULL(F.val_premio_mensal_conjuge,0) val_premio_mensal_conjuge,"
    adSQL sSQL, "    ISNULL(F.val_premio_tarifa_conjuge,0) val_premio_tarifa_conjuge"
    adSQL sSQL, "FROM    sub_grupo_faixa_etaria_tb F  WITH (NOLOCK)  "
    adSQL sSQL, "INNER   JOIN    sub_grupo_apolice_tb S  WITH (NOLOCK)  "
    adSQL sSQL, "    ON  S.seguradora_cod_susep = F.seguradora_cod_susep"
    adSQL sSQL, "    AND     S.sucursal_seguradora_id = F.sucursal_seguradora_id"
    adSQL sSQL, "    AND     S.ramo_id = F.ramo_id"
    adSQL sSQL, "    AND     S.apolice_id = F.apolice_id"
    adSQL sSQL, "    AND     S.sub_grupo_id = F.sub_grupo_id"
    adSQL sSQL, "    AND     S.dt_inicio_vigencia_sbg = F.dt_inicio_vigencia_sbg"
    adSQL sSQL, "INNER   JOIN    apolice_tb  A    WITH (NOLOCK)  "
    adSQL sSQL, "    ON  A.seguradora_cod_susep = F.seguradora_cod_susep"
    adSQL sSQL, "    AND     A.sucursal_seguradora_id = F.sucursal_seguradora_id"
    adSQL sSQL, "    AND     A.ramo_id = F.ramo_id"
    adSQL sSQL, "    AND     A.apolice_id = F.apolice_id"
    adSQL sSQL, "Where  F.seguradora_cod_susep = " & vSeguradoraId
    adSQL sSQL, "AND     F.sucursal_seguradora_id = " & vSucursalId
    adSQL sSQL, "AND     F.ramo_id = " & vRamoId
    adSQL sSQL, "AND     F.apolice_id = " & vApoliceId
    adSQL sSQL, "AND     F.sub_grupo_id = " & txtID.Text
    adSQL sSQL, "AND     (F.dt_inicio_faixa = isnull(S.dt_inicio_sub_grupo, A.dt_inicio_vigencia)"
    adSQL sSQL, "    OR  F.dt_inicio_faixa <= '" & dt_inicio_query & "')"
    adSQL sSQL, "AND     (F.dt_fim_faixa is null"
    adSQL sSQL, "    OR F.dt_fim_faixa  > '" & dt_inicio_query & "')"
    adSQL sSQL, "ORDER   BY F.idade_min,"
    adSQL sSQL, "    F.idade_max,"
    adSQL sSQL, "    F.val_is,"
    adSQL sSQL, "    F.taxa_tarifa,"
    adSQL sSQL, "    F.val_premio_mensal,"
    adSQL sSQL, "    F.val_premio_tarifa"

    'Exibindo dados''''''''''''''''''''''''''''''''''''''''''''''''

    Set rsFaixa = rdocn.OpenResultset(sSQL)

    If Not rsFaixa.EOF Then
        While Not rsFaixa.EOF
            grdFaixas.AddItem ""

            With grdFaixas
                .TextMatrix(.Rows - 1, GRD_FAIXA_IDADEMAXIMA) = rsFaixa("idade_max")
                .TextMatrix(.Rows - 1, GRD_FAIXA_IDADEMINIMA) = rsFaixa("idade_min")
                .TextMatrix(.Rows - 1, GRD_FAIXA_IS) = Format(TrocaPontoPorVirgula(rsFaixa("val_is")), "0.00")
                .TextMatrix(.Rows - 1, GRD_FAIXA_PREMIOMENSAL) = Format(TrocaPontoPorVirgula(rsFaixa("val_premio_mensal")), "0.00")
                .TextMatrix(.Rows - 1, GRD_FAIXA_PREMIOTARIFA) = Format(TrocaPontoPorVirgula(rsFaixa("val_premio_tarifa")), "0.00")
                .TextMatrix(.Rows - 1, GRD_FAIXA_TAXA) = Format(TrocaPontoPorVirgula(rsFaixa("taxa_tarifa")), "0.0000000")
                .TextMatrix(.Rows - 1, GRD_FAIXA_PREMIOMENSALCONJ) = Format(TrocaPontoPorVirgula(rsFaixa("val_premio_mensal_conjuge")), "0.00")
                .TextMatrix(.Rows - 1, GRD_FAIXA_PREMIOTARIFACONJ) = Format(TrocaPontoPorVirgula(rsFaixa("val_premio_tarifa_conjuge")), "0.00")
                .TextMatrix(.Rows - 1, GRD_FAIXA_TAXACONJ) = Format(TrocaPontoPorVirgula(rsFaixa("taxa_tarifa_conjuge")), "0.0000000")

            End With

            rsFaixa.MoveNext
        Wend
        rsFaixa.Close
    End If

    Set rsFaixa = Nothing

    Exit Sub

Trata_Erro:
    Call TrataErroGeral("MontaGridFaixaEtariaSubGrupo", Me.name)
End Sub

Public Sub Monta_GridFaixaPlanoSubGrupo()

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'Defini��o: Esta rotina carrega grid com as faixas do subgrupo por plano. '
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    Dim rsFaixa As rdoResultset
    Dim sSQL As String

    On Error GoTo Trata_Erro

    'Redimensionando o grid ''''''''''''''''
    grdFaixas.Cols = 5
    grdFaixas.TextMatrix(0, GRD_FXPLANO_IDADEMINIMA) = "Idade M�nima"
    grdFaixas.TextMatrix(0, GRD_FXPLANO_IDADEMAXIMA) = "Idade M�xima"
    grdFaixas.TextMatrix(0, GRD_FXPLANO_IS) = "IS"
    grdFaixas.TextMatrix(0, GRD_FXPLANO_TAXA) = "Taxa"
    grdFaixas.TextMatrix(0, GRD_FXPLANO_PREMIO) = "Pr�mio"

    'Pesquisando dados''''''''''''''''''''''''''''''''''''

    sSQL = ""
    adSQL sSQL, "SELECT "
    adSQL sSQL, "    ISNULL(sub_grupo_plano_faixa_tb.idade_min,0) idade_min,"
    adSQL sSQL, "    ISNULL(sub_grupo_plano_faixa_tb.idade_max,0) idade_max,"
    adSQL sSQL, "    ISNULL(sub_grupo_plano_faixa_tb.val_is,0) val_is,"
    adSQL sSQL, "    ISNULL(sub_grupo_plano_faixa_tb.taxa,0) taxa,"
    adSQL sSQL, "    ISNULL(sub_grupo_plano_faixa_tb.val_premio,0) val_premio"
    adSQL sSQL, "FROM "
    adSQL sSQL, "    sub_grupo_plano_tb    WITH (NOLOCK)  "
    adSQL sSQL, "    JOIN sub_grupo_plano_faixa_tb    WITH (NOLOCK)  "
    adSQL sSQL, "      ON sub_grupo_plano_faixa_tb.seguradora_cod_susep = sub_grupo_plano_tb.seguradora_cod_susep"
    adSQL sSQL, "     AND sub_grupo_plano_faixa_tb.sucursal_seguradora_id = sub_grupo_plano_tb.sucursal_seguradora_id"
    adSQL sSQL, "     AND sub_grupo_plano_faixa_tb.ramo_id = sub_grupo_plano_tb.ramo_id"
    adSQL sSQL, "     AND sub_grupo_plano_faixa_tb.sub_grupo_id = sub_grupo_plano_tb.sub_grupo_id"
    adSQL sSQL, "     AND sub_grupo_plano_faixa_tb.apolice_id = sub_grupo_plano_tb.apolice_id"
    adSQL sSQL, "     AND sub_grupo_plano_faixa_tb.tp_plano_id = sub_grupo_plano_tb.tp_plano_id"
    adSQL sSQL, "     AND sub_grupo_plano_faixa_tb.dt_inicio_vig_plano = sub_grupo_plano_tb.dt_inicio_vig_plano"
    adSQL sSQL, " WHERE"
    adSQL sSQL, "    sub_grupo_plano_faixa_tb.seguradora_cod_susep = " & vSeguradoraId
    adSQL sSQL, "    AND sub_grupo_plano_faixa_tb.sucursal_seguradora_id = " & vSucursalId
    adSQL sSQL, "    AND sub_grupo_plano_faixa_tb.ramo_id = " & vRamoId
    adSQL sSQL, "    AND sub_grupo_plano_faixa_tb.apolice_id = " & vApoliceId
    adSQL sSQL, "    AND sub_grupo_plano_faixa_tb.sub_grupo_id = " & txtID.Text
    adSQL sSQL, "    AND sub_grupo_plano_tb.dt_inicio_vig_plano <= '" & dt_inicio_query & "'"
    adSQL sSQL, "    AND (sub_grupo_plano_tb.dt_fim_vig_plano is null OR sub_grupo_plano_tb.dt_fim_vig_plano  >'" & dt_inicio_query & "')"
    adSQL sSQL, " ORDER BY"
    adSQL sSQL, "    sub_grupo_plano_faixa_tb.idade_min,"
    adSQL sSQL, "    sub_grupo_plano_faixa_tb.idade_max,"
    adSQL sSQL, "    sub_grupo_plano_faixa_tb.val_is,"
    adSQL sSQL, "    sub_grupo_plano_faixa_tb.taxa,"
    adSQL sSQL, "    sub_grupo_plano_faixa_tb.val_premio"


    'Exibindo dados''''''''''''''''''''''''''''''''''''''''''''''''

    Set rsFaixa = rdocn.OpenResultset(sSQL)

    If Not rsFaixa.EOF Then

        While Not rsFaixa.EOF
            grdFaixas.AddItem rsFaixa("idade_min") & vbTab & _
                              rsFaixa("idade_max") & vbTab & rsFaixa("val_is") & vbTab & _
                              TrocaPontoPorVirgula(Val(rsFaixa("taxa"))) & vbTab & rsFaixa("val_premio")
            rsFaixa.MoveNext
        Wend
        rsFaixa.Close
    End If

    Set rsFaixa = Nothing

    Exit Sub

Trata_Erro:
    Call TrataErroGeral("MontaGridFaixaPlanoSubGrupo", Me.name)
End Sub

Sub Ler_Agenciamento()

    Dim rc As rdoResultset
    Dim SQL As String
    Dim vSubGrupo As SubGrupo

    On Error GoTo Erro_Age

    If SubGrupos.Count = 0 Then
        SQL = "SELECT ap.agenciador_id, a.nome" & vbNewLine
        SQL = SQL & ", perc_prim_fat, no_parc_prim_fat" & vbNewLine
        SQL = SQL & ", isnull(pgto_demais, 0) pgto_demais " & vbNewLine
        SQL = SQL & ", isnull(perc_demais_fat, 0) perc_demais_fat " & vbNewLine
        SQL = SQL & ", isnull(no_parc_demais_fat, 0) no_parc_demais_fat " & vbNewLine
        SQL = SQL & " FROM agenciamento_proposta_tb ap WITH(NOLOCK), agenciador_tb a WITH(NOLOCK)" & vbNewLine
        SQL = SQL & "WHERE proposta_id = " & vPropostaId & vbNewLine
        SQL = SQL & " AND dt_ini_agenciamento <= '" & dt_inicio_query & "'" & vbNewLine
        SQL = SQL & " AND (dt_fim_agenciamento >= '" & dt_inicio_query & "'" & vbNewLine
        SQL = SQL & "     OR dt_fim_agenciamento is null) " & vbNewLine
        SQL = SQL & " AND a.agenciador_id = ap.agenciador_id " & vbNewLine
        '
        Set rc = rdocn.OpenResultset(SQL)
        If Not rc.EOF Then
            TxtCodAgenciador.Text = Format(rc!agenciador_id, "###000000")
            TxtAgenciador.Text = Trim(rc!Nome)
            TxtFPAgPrim.Text = FP_Agenciamento(1)
            TxtNoParcAgPrim.Text = rc!no_parc_prim_fat
            TxtNoParcAgDemais.Text = rc!no_parc_demais_fat
            TxtFPAgDemais.Text = FP_Agenciamento(rc!pgto_demais)
            If ConfiguracaoBrasil Then
                TxtPercAgPrim.Text = Format(Val(rc!perc_prim_fat), "##0.00")
                TxtPercAgDemais.Text = Format(Val(rc!perc_demais_fat), "##0.00")
            Else
                TxtPercAgPrim.Text = Format(rc!perc_prim_fat, "##0.00")
                TxtPercAgDemais.Text = Format(rc!perc_demais_fat, "##0.00")
            End If
        End If

    Else
        For Each vSubGrupo In SubGrupos
            SQL = "SELECT ap.agenciador_id, a.nome" & vbNewLine
            SQL = SQL & ", perc_prim_fat, no_parc_prim_fat" & vbNewLine
            SQL = SQL & ", isnull(pgto_demais, 0) pgto_demais " & vbNewLine
            SQL = SQL & ", isnull(perc_demais_fat, 0) perc_demais_fat " & vbNewLine
            SQL = SQL & ", isnull(no_parc_demais_fat, 0) no_parc_demais_fat " & vbNewLine
            SQL = SQL & " FROM          agenciamento_sub_grupo_tb ap  WITH(NOLOCK) " & vbNewLine
            SQL = SQL & " INNER JOIN    agenciador_tb a WITH(NOLOCK) " & vbNewLine
            SQL = SQL & "       ON a.agenciador_id = ap.agenciador_id " & vbNewLine

            SQL = SQL & " INNER JOIN  sub_grupo_apolice_tb WITH(NOLOCK) " & vbNewLine
            SQL = SQL & "     ON sub_grupo_apolice_tb.apolice_id             =  AP.apolice_id" & vbNewLine
            SQL = SQL & "     AND sub_grupo_apolice_tb.ramo_id                =  AP.ramo_id" & vbNewLine
            SQL = SQL & "     AND sub_grupo_apolice_tb.sucursal_seguradora_id =  AP.sucursal_seguradora_id" & vbNewLine
            SQL = SQL & "     AND sub_grupo_apolice_tb.seguradora_cod_susep   =  AP.seguradora_cod_susep" & vbNewLine
            SQL = SQL & "     AND sub_grupo_apolice_tb.dt_inicio_vigencia_sbg =  AP.dt_inicio_vigencia_sbg" & vbNewLine

            SQL = SQL & " WHERE AP.apolice_id              = " & vApoliceId & vbNewLine
            SQL = SQL & " AND AP.ramo_id                   = " & vRamoId & vbNewLine
            SQL = SQL & " AND AP.seguradora_cod_susep      = " & vSeguradoraId & vbNewLine
            SQL = SQL & " AND AP.sucursal_seguradora_id    = " & vSucursalId & vbNewLine
            SQL = SQL & " AND AP.sub_grupo_id              = " & vSubGrupo.Id & vbNewLine
            SQL = SQL & " AND  dt_ini_agenciamento       <= '" & dt_inicio_query & "'" & vbNewLine
            SQL = SQL & " AND (dt_fim_agenciamento      >= '" & dt_inicio_query & "'" & vbNewLine
            SQL = SQL & "     OR dt_fim_agenciamento is null) " & vbNewLine

            '
            Set rc = rdocn.OpenResultset(SQL)
            If Not rc.EOF Then
                vSubGrupo.IdAgenciador = rc!agenciador_id
                vSubGrupo.NomeAgenciador = Trim(rc!Nome)
                vSubGrupo.NoParcAgenciamento = rc!no_parc_prim_fat
                vSubGrupo.NoParcAgDemais = rc!no_parc_demais_fat
                vSubGrupo.FormaPgtoAgDemais = rc!pgto_demais
                If ConfiguracaoBrasil Then
                    vSubGrupo.PercAgenciamento = Val(rc!perc_prim_fat)
                    vSubGrupo.PercAgDemais = Val(rc!perc_demais_fat)
                Else
                    vSubGrupo.PercAgenciamento = rc!perc_prim_fat
                    vSubGrupo.PercAgDemais = rc!perc_demais_fat
                End If
            End If
        Next
    End If
    '
    Exit Sub

Erro_Age:
    TrataErroGeral "Ler Agenciamento"
    MsgBox "Rotina Ler_Agenciamento : Erro obtendo dados de agenciamento.", vbInformation
End Sub

Sub Ler_Dados_Assistencia()

    Dim linha As String
    Dim i As Integer
    Dim vAssistencias As Assistencia
    Dim SQL As String
    Dim rs As rdoResultset
    'lpinto
    If Not bPropostaRecusada And Not bPropostaNaoEmitida And Not bPropostaEstudo Then    'rmaiellaro (19/01/2007)

        SQL = ""
        SQL = "SELECT " & vbNewLine
        SQL = SQL & "    SGA.sub_grupo_id, " & vbNewLine
        SQL = SQL & "    SGA.tp_assistencia_id, " & vbNewLine
        SQL = SQL & "    TA.nome, " & vbNewLine
        SQL = SQL & "    ISNULL(SGA.dt_inicio_assist_sbg,'') dt_inicio_assist_sbg, " & vbNewLine
        SQL = SQL & "    ISNULL(SGA.dt_fim_assist_sbg,'') dt_fim_assist_sbg, " & vbNewLine
        SQL = SQL & "    ISNULL(SGA.val_assistencia,0) val_assistencia" & vbNewLine
        SQL = SQL & " FROM " & vbNewLine
        SQL = SQL & "    sub_grupo_assistencia_tb SGA   WITH (NOLOCK)  " & vbNewLine
        SQL = SQL & "        INNER JOIN tp_assistencia_tb TA  WITH (NOLOCK)     ON " & vbNewLine
        SQL = SQL & "            TA.tp_assistencia_id = SGA.tp_assistencia_id " & vbNewLine
        SQL = SQL & " WHERE " & vbNewLine
        SQL = SQL & "            SGA.apolice_id               = " & vApoliceId & vbNewLine
        SQL = SQL & "    AND     SGA.sucursal_seguradora_id   = " & vSucursalId & vbNewLine
        SQL = SQL & "    AND     SGA.seguradora_cod_susep     = " & vSeguradoraId & vbNewLine
        SQL = SQL & "    AND     SGA.ramo_id                  = " & vRamoId & vbNewLine
        SQL = SQL & "    AND     SGA.dt_inicio_assist_sbg    <= '" & Format(dt_inicio_vigencia, "YYYYMMDD") & "'" & vbNewLine
        SQL = SQL & "    AND    (SGA.dt_fim_assist_sbg       IS NULL " & vbNewLine
        SQL = SQL & "    OR     SGA.dt_fim_assist_sbg        >= '" & Format(dt_inicio_vigencia, "YYYYMMDD") & "') " & vbNewLine
        SQL = SQL & "ORDER BY SGA.sub_grupo_id, SGA.tp_assistencia_id " & vbNewLine

        Set rs = rdocn.OpenResultset(SQL)
        If Not rs.EOF Then
            While Not rs.EOF
                Set vAssistencias = New Assistencia
                vAssistencias.CodAssistencia = Format(rs!tp_assistencia_id, "000")
                vAssistencias.DescricaoAssistencia = Trim(rs!Nome)
                vAssistencias.DtIniAssistSbg = rs!dt_inicio_assist_sbg
                '            If Not IsNull(rs!dt_fim_assist_sbg) And rs!dt_fim_assist_sbg <> "" Then
                '                vAssistencias.DTFimAssistSbg = rs!dt_fim_assist_sbg
                '            Else
                '                vAssistencias.DTFimAssistSbg = ""
                '            End If

                vAssistencias.DTFimAssistSbg = IIf(Year(rs!dt_fim_assist_sbg) = "1900", " ", rs!dt_fim_assist_sbg)
                vAssistencias.ValAssistencia = TrocaPontoPorVirgula(rs!val_assistencia)
                SubGrupos(CStr(rs!sub_grupo_id)).Assistencias.Add vAssistencias, CStr(rs!tp_assistencia_id)
                rs.MoveNext

            Wend
        End If

        rs.Close
        Screen.MousePointer = vbArrow
    End If
    Exit Sub

End Sub

'lrocha 29/10/2008 - Demanda 273032
Sub Monta_Grid_Assistencia()

    Dim linha As String
    Dim oRs As Object
    Dim oAssistencias As Object
    Dim sDataInicioVigenciaSbg As String

    gridSubGrupoAssist.Rows = 1

    sDataInicioVigenciaSbg = SubGrupos(vSubGrupoId).DtInicioVigenciaSBG

    Set oSubGrupo = Nothing
    Set oSubGrupo = CreateObject("SEGL0308.Cls00492")

    'jessica.adao - Confitec Sistemas - 20/07/2012 - INC000003615672 - Overflow - Altera��o de Int para Long para a vApoliceId
    If oSubGrupo.ConsultarPlanos(gsSIGLASISTEMA, _
                                 App.Title, _
                                 App.FileDescription, _
                                 glAmbiente_id, _
                                 Data_Sistema, _
                                 CLng(vApoliceId), _
                                 CInt(vRamoId), _
                                 CInt(vSucursalId), _
                                 CInt(vSeguradoraId), _
                                 sDataInicioVigenciaSbg, _
                                 CInt(vSubGrupoId)) = True Then

        Set oAssistencias = CreateObject("SEGL0308.Cls00491")

        For Each oAssistencias In oSubGrupo.PlanosAssistencia
            linha = oAssistencias.iPlanoId & vbTab
            linha = linha & oAssistencias.iSequencial & vbTab
            linha = linha & oAssistencias.sTipoAssistencia & vbTab
            linha = linha & Format(oAssistencias.dCustoAssistencia, "###,##0.00") & vbTab
            linha = linha & Format(oAssistencias.dCustoAssistenciaNumVidas, "###,##0.00") & vbTab
            linha = linha & Format(oAssistencias.sInicioAssistenciaSubGrupo, "dd/mm/yyyy") & vbTab
            linha = linha & IIf(Format(oAssistencias.sFimAssistenciaSubGrupo, "dd/mm/yyyy") = "01/01/1900", " ", Format(oAssistencias.sFimAssistenciaSubGrupo, "dd/mm/yyyy")) & vbTab
            linha = linha & Format(oAssistencias.sInicioVigencia, "dd/mm/yyyy") & vbTab
            linha = linha & oAssistencias.sStatus & vbTab
            gridSubGrupoAssist.AddItem linha
        Next

        gridSubGrupoAssist.Refresh

    End If

End Sub

Sub Monta_GridCobranca_Cosseguro(ByVal PercPart, ByVal PercDespLider)

    Dim linha As String
    Dim vCobranca As Cobranca
    Dim endosso_id As String
    Dim rc As rdoResultset
    Dim SQL As String
    Dim ValPremioTarifa As String
    Dim ValPremioTarifaPart As String
    Dim ValDescComercial As String
    Dim ValDescComercialPart As String
    Dim ValJuros As String
    Dim ValJurosPart As String
    Dim ValComissao As String
    Dim ValComissaoPart As String
    Dim ValDespLider As String
    Dim ValDespLiderPart As String
    Dim ValPago As String
    Dim ValPagoPart As String
    Dim TotalLiquido As String
    Dim TotalLiquidoPart As String
    Dim DtPagamento As String
    Dim PercPartOk As Variant
    Dim PercDespLiderOk As Variant

    gridCobrancaCosseguro.Rows = 1
    PercPartOk = PercPart / 100
    PercDespLiderOk = PercDespLider / 100
    '

    For Each vCobranca In Cobrancas
        With vCobranca

            If IsNull(.ValPago) Then
                ValPago = vValZero
            Else
                If ConfiguracaoBrasil Then
                    ValPago = Format(.ValPago, "###,###,##0.00")
                Else
                    ValPago = TrocaValorAmePorBras(Format(.ValPago, "###,###,##0.00"))
                End If
            End If

            SQL = "SELECT val_premio_tarifa " & vbNewLine
            SQL = SQL & ", val_desconto_comercial " & vbNewLine
            SQL = SQL & ", val_comissao " & vbNewLine
            SQL = SQL & ", dt_pgto " & vbNewLine
            SQL = SQL & ", val_adic_fracionamento " & vbNewLine
            SQL = SQL & "FROM endosso_financeiro_tb  WITH (NOLOCK)  " & vbNewLine
            SQL = SQL & "WHERE proposta_id = " & vPropostaId & vbNewLine
            SQL = SQL & " AND  endosso_id = " & .EndossoId & vbNewLine

            Set rc = rdoCn1.OpenResultset(SQL, rdOpenStatic)

            If Not rc.EOF Then

                If IsNull(rc!val_premio_tarifa) Then
                    ValPremioTarifa = vValZero
                Else
                    ValPremioTarifaPart = Val(rc!val_premio_tarifa) * PercPartOk
                    ValDespLiderPart = Val(rc!val_premio_tarifa) * PercDespLiderOk
                    If ConfiguracaoBrasil Then
                        ValPremioTarifa = Format(ValPremioTarifaPart, "###,###,##0.00")
                        ValDespLider = Format(ValDespLiderPart, "###,###,##0.00")
                    Else
                        ValPremioTarifa = TrocaValorAmePorBras(Format(ValPremioTarifaPart, "###,###,##0.00"))
                        ValDespLider = TrocaValorAmePorBras(Format(ValDespLiderPart, "###,###,##0.00"))
                    End If
                End If

                If IsNull(rc!val_desconto_comercial) Then
                    ValDescComercial = vValZero
                Else
                    ValDescComercialPart = Val(rc!val_desconto_comercial) * PercPartOk
                    If ConfiguracaoBrasil Then
                        ValDescComercial = Format(ValDescComercialPart, "###,###,##0.00")
                    Else
                        ValDescComercial = TrocaValorAmePorBras(Format(ValDescComercialPart, "###,###,##0.00"))
                    End If
                End If

                If IsNull(rc!val_adic_fracionamento) Then
                    ValJuros = vValZero
                Else
                    ValJurosPart = Val(rc!val_adic_fracionamento) * PercPartOk
                    If ConfiguracaoBrasil Then
                        ValJuros = Format(ValJurosPart, "###,###,##0.00")
                    Else
                        ValJuros = TrocaValorAmePorBras(Format(ValJurosPart, "###,###,##0.00"))
                    End If
                End If

                TotalLiquido = Format(ValPremioTarifa - ValDescComercial + ValJuros, "###,###,##0.00")

                If IsNull(rc!val_comissao) Then
                    ValComissao = vValZero
                Else
                    ValComissaoPart = Val(rc!val_comissao) * PercPartOk
                    If ConfiguracaoBrasil Then
                        ValComissao = Format(ValComissaoPart, "###,###,##0.00")
                    Else
                        ValComissao = TrocaValorAmePorBras(Format(ValComissaoPart, "###,###,##0.00"))
                    End If
                End If

                If Not IsNull(rc!dt_pgto) Then
                    DtPagamento = Format(rc!dt_pgto, "dd/mm/yyyy")
                Else
                    DtPagamento = " "
                End If

            End If

            linha = .EndossoId
            linha = linha & vbTab & .Id
            linha = linha & vbTab & Format(.DtAgendamento, "dd/mm/yyyy")
            linha = linha & vbTab & ValPremioTarifa
            linha = linha & vbTab & ValDescComercial
            linha = linha & vbTab & ValJuros
            linha = linha & vbTab & ValComissao
            linha = linha & vbTab & ValDespLider
            linha = linha & vbTab & TotalLiquido
            linha = linha & vbTab & DtPagamento
            linha = linha & vbTab & ValPago
        End With
        '
        gridCobrancaCosseguro.AddItem linha

    Next

    '
    gridCobrancaCosseguro.Refresh


End Sub

Sub Monta_GridFaturas_Cosseguro(ByVal PercPart As Variant, ByVal PercDespLider As Variant)

    Dim linha As String
    Dim vFatura As Fatura
    Dim endosso_id As String
    Dim rc As rdoResultset
    Dim SQL As String
    Dim ValPremioTarifa As String
    Dim ValPremioTarifaPart As String
    Dim ValDescComercial As String
    Dim ValDescComercialPart As String
    Dim ValJuros As String
    Dim ValJurosPart As String
    Dim ValComissao As String
    Dim ValComissaoPart As String
    Dim ValDespLider As String
    Dim ValDespLiderPart As String
    Dim TotalLiquido As String
    Dim TotalLiquidoPart As String
    Dim DtPagamento As String
    Dim PosVirgula As Integer
    Dim PercPartOk As Variant
    Dim PercDespLiderOk As Variant

    GridFaturasCosseguro.Rows = 1
    PercPartOk = PercPart / 100
    PercDespLiderOk = PercDespLider / 100
    If HaSubGrupos Then
        Set Faturas = Nothing
        Set Faturas = SubGrupos(vSubGrupoId).Faturas
    End If
    '

    For Each vFatura In Faturas
        With vFatura

            SQL = "SELECT val_premio_tarifa " & vbNewLine
            SQL = SQL & ", val_desconto_comercial " & vbNewLine
            SQL = SQL & ", val_comissao " & vbNewLine
            SQL = SQL & ", dt_pgto " & vbNewLine
            SQL = SQL & ", val_adic_fracionamento " & vbNewLine
            SQL = SQL & "FROM endosso_financeiro_tb  WITH (NOLOCK)  " & vbNewLine
            SQL = SQL & "WHERE proposta_id = " & vPropostaId & vbNewLine
            SQL = SQL & " AND  endosso_id = " & .EndossoId & vbNewLine

            Set rc = rdoCn1.OpenResultset(SQL, rdOpenStatic)

            If Not rc.EOF Then

                If IsNull(rc!val_premio_tarifa) Then
                    ValPremioTarifa = vValZero
                Else
                    ValPremioTarifaPart = Val(rc!val_premio_tarifa) * PercPartOk
                    ValDespLiderPart = Val(rc!val_premio_tarifa) * PercDespLiderOk
                    If ConfiguracaoBrasil Then
                        ValPremioTarifa = Format(ValPremioTarifaPart, "###,###,##0.00")
                        ValDespLider = Format(ValDespLiderPart, "###,###,##0.00")
                    Else
                        ValPremioTarifa = TrocaValorAmePorBras(Format(ValPremioTarifaPart, "###,###,##0.00"))
                        ValDespLider = TrocaValorAmePorBras(Format(ValDespLiderPart, "###,###,##0.00"))
                    End If
                End If

                If IsNull(rc!val_desconto_comercial) Then
                    ValDescComercial = vValZero
                Else
                    ValDescComercialPart = Val(rc!val_desconto_comercial) * PercPartOk
                    If ConfiguracaoBrasil Then
                        ValDescComercial = Format(ValDescComercialPart, "###,###,##0.00")
                    Else
                        ValDescComercial = TrocaValorAmePorBras(Format(ValDescComercialPart, "###,###,##0.00"))
                    End If
                End If

                If IsNull(rc!val_adic_fracionamento) Then
                    ValJuros = vValZero
                Else
                    ValJurosPart = Val(rc!val_adic_fracionamento) * PercPartOk
                    If ConfiguracaoBrasil Then
                        ValJuros = Format(ValJurosPart, "###,###,##0.00")
                    Else
                        ValJuros = TrocaValorAmePorBras(Format(ValJurosPart, "###,###,##0.00"))
                    End If
                End If

                TotalLiquido = Format(ValPremioTarifa - ValDescComercial + ValJuros, "###,###,##0.00")

                If IsNull(rc!val_comissao) Then
                    ValComissao = vValZero
                Else
                    ValComissaoPart = Val(rc!val_comissao) * PercPartOk
                    If ConfiguracaoBrasil Then
                        ValComissao = Format(ValComissaoPart, "###,###,##0.00")
                    Else
                        ValComissao = TrocaValorAmePorBras(Format(ValComissaoPart, "###,###,##0.00"))
                    End If
                End If

                If Not IsNull(rc!dt_pgto) Then
                    DtPagamento = Format(rc!dt_pgto, "dd/mm/yyyy")
                Else
                    DtPagamento = " "
                End If

            End If

            linha = .EndossoId
            linha = linha & vbTab & .Id
            linha = linha & vbTab & Format(.DtRecebimento, "dd/mm/yyyy")
            linha = linha & vbTab & ValPremioTarifa
            linha = linha & vbTab & ValComissao
            linha = linha & vbTab & ValDespLider
            linha = linha & vbTab & TotalLiquido
            linha = linha & vbTab & DtPagamento
        End With
        '
        GridFaturasCosseguro.AddItem linha

    Next

    '
    GridFaturasCosseguro.Refresh

End Sub

Sub Monta_GridFaturas_Lider()
    Dim linha As String
    Dim vFatura As Fatura

    GridFaturas.Rows = 1
    '

    For Each vFatura In Faturas
        With vFatura
            linha = .EndossoId
            linha = linha & vbTab & .EndossoIdLider
            linha = linha & vbTab & .Id
            linha = linha & vbTab & .NossoNumero
            linha = linha & vbTab & Format(.ValIS, "###,###,##0.00")
            linha = linha & vbTab & Format(.ValPremio, "###,###,##0.00")
            linha = linha & vbTab & Format(.ValIOF, "###,###,##0.00")
            linha = linha & vbTab & Format(.DtInicioVigencia, "dd/mm/yyyy")
            linha = linha & vbTab & Format(.DtFimVigencia, "dd/mm/yyyy")
            linha = linha & vbTab & Format(.DtRecebimento, "dd/mm/yyyy")
            linha = linha & vbTab & .Situacao
            linha = linha & vbTab & Format(.PercCorretagem, "#0.00000")
            linha = linha & vbTab & Format(.PercProLabore, "#0.00000")
            linha = linha & vbTab & .Observacao
            linha = linha & vbTab & .QtdeVidas
        End With
        '
        GridFaturas.AddItem linha

    Next

    GridFaturas.FormatString = "Endosso|Endosso L�der|N� Fatura    |Nosso N�     | Valor IS                  | Valor Cobrado  | Valor IOF    | Dt. In�cio Vig. |Dt. Fim Vig.   |Dt. Pagamento| Situa��o                              | % Comiss�o | % Pr�-Labore | Observa��o                                                                           "

    '
    GridFaturas.Refresh
End Sub

Function Formata_Clausula(stRClau As String) As String
'* Formata texto para que n�o existam quebras no meio de palavras,
'* considerando-se 90 chars por linha

    Dim Texto As String, ult_quebra As Variant
    Dim Encontrou As Boolean
    Dim FRASE As String
    Dim cont_clau As Variant, CONT_FRASE As Integer
    Dim ACHA_ESPACO As Variant
    Dim SQL As String
    Dim rc As rdoResultset

    ult_quebra = 1    'Contem o �ndice do 1� char. depois da ult. quebra
    Encontrou = False
    Texto = ""
    CONT_FRASE = 0
    '
    For cont_clau = 1 To Len(stRClau)
        CONT_FRASE = CONT_FRASE + 1
        '' Se caracter 'cont' � return, 'frase' recebe peda�o
        '' de ultima quebra at� cont_clau
        If Mid(stRClau, cont_clau, 1) = Chr(13) Then
            FRASE = Mid(stRClau, ult_quebra, cont_clau - ult_quebra) & " " & vbNewLine
            ult_quebra = cont_clau + 2
            'Pula o char de line feed - Chr(10)
            cont_clau = cont_clau + 1
            CONT_FRASE = 0
            '' Se char n�o � return, se frase j� tem 90 chars
            '' isto e, ja chegou ao final da linha de impressao
        ElseIf CONT_FRASE = 90 Then
            Encontrou = False
            '' Se o prox. char � <> de espa�o e return
            If Mid(stRClau, cont_clau + 1, 1) <> " " And Mid(stRClau, cont_clau + 1, 1) <> Chr(13) Then
                '' Procura o 1� espaco anterior ao char atual dentro da frase
                For ACHA_ESPACO = cont_clau To ult_quebra Step -1
                    '' Se achou o espaco
                    If Mid(stRClau, ACHA_ESPACO, 1) = " " Then
                        FRASE = Mid(stRClau, ult_quebra, ACHA_ESPACO - ult_quebra) & vbNewLine
                        CONT_FRASE = cont_clau - ACHA_ESPACO
                        ult_quebra = ACHA_ESPACO + 1
                        Encontrou = True
                        Exit For
                    End If
                Next ACHA_ESPACO
            End If
            '' Se n�o precisou procurar por espaco
            If Not Encontrou Then
                FRASE = RTrim(Mid(stRClau, ult_quebra, 90)) & vbNewLine
                CONT_FRASE = 0
                ult_quebra = ult_quebra + 90
                '' Se ultimo char restante � espa�o ou return, pula
                If Mid(stRClau, ult_quebra, 1) = Chr(13) Then
                    ult_quebra = ult_quebra + 2
                    cont_clau = cont_clau + 2
                ElseIf Mid(stRClau, ult_quebra, 1) = " " Then
                    ult_quebra = ult_quebra + 1
                    cont_clau = cont_clau + 1
                    If Mid(stRClau, ult_quebra, 1) = Chr(13) Then
                        ult_quebra = ult_quebra + 2
                        cont_clau = cont_clau + 2
                    End If
                End If
            End If    'not encontrou
        End If
        '' Acumula as frases na var. 'texto'
        If FRASE <> "" Then
            Texto = Texto & FRASE
            FRASE = ""
        End If
    Next cont_clau
    '
    If ult_quebra < Len(stRClau) Then
        Texto = Texto & Mid(stRClau, ult_quebra, cont_clau - ult_quebra + 1)
    End If

    Formata_Clausula = Texto

End Function



Sub Imprimir_Apolice()

    Dim rc As rdoResultset
    Dim rc1 As rdoResultset
    Dim vSubGrupo As SubGrupo
    Dim vCorretor As Corretor
    Dim data_rodape As String
    Dim Cabecalho As String
    Dim Texto1 As String
    Dim Texto2 As String
    Dim dt_inicio As String
    Dim dt_fim As String
    Dim CR_LF As String
    Dim SQL As String
    Dim linha As Integer
    Dim Forma_Pgto As String, forma_pgto_id As String
    Dim Periodo_Pgto As String, periodo_pgto_id As String
    Dim Observacao As String
    Dim NomeRamo As String
    Dim Nome_Corretor() As String
    Dim Cod_Susep_Corretor() As String
    Dim Premio_Bruto As Double
    Dim Limite_Apolice As String
    Dim Data_Aprovacao As String
    Dim Liquido As Double
    Dim Atividade As String
    Dim texto_anexos(18) As String
    Dim linha_anexos(73) As String
    Dim linha_anexosTemp As String
    Dim num_apolice As String
    Dim num_endosso As String
    Dim apolice_anterior As String
    Dim num_proposta As String
    Dim desc_produto As String
    Dim desc_ramo As String
    Dim temp As Long
    Dim nlinclau As Integer
    Dim i As Variant
    Dim ind_anexos As Integer
    Dim ind_formula As Integer
    Dim ind_aux As Integer
    Dim inicio_linha_anexo As Variant
    Dim fim_linha_anexo As Variant
    Dim tamanho_linha_anexo As Variant
    Dim resp As Integer
    Dim QtdDatas As Integer
    'Dim DataAgendamento(1 To 12)   As String
    Dim DataAgendamento(1 To 50) As String
    Dim ValParcela1 As Double
    Dim ValParcelaDemais As Double
    Dim ValUltParcela As Double
    Dim ValPrimParc As Double
    Dim ValDemaisParcelas As Double
    Dim flagCentralizar As Boolean
    Dim flagTitulo As Boolean
    Dim cont_corr As Integer
    Dim ret_imp As Boolean
    Dim ultquebra As Long
    Dim inicio_linha_anexos As Long
    Dim fim_linha_anexos As Long
    Dim tamanho_linha_anexos As Long
    Dim aux As Variant

    ''
    '
    On Error GoTo Erro
    MousePointer = vbHourglass
    '
    meses(1) = "Janeiro"
    meses(2) = "Fevereiro"
    meses(3) = "Mar�o"
    meses(4) = "Abril"
    meses(5) = "Maio"
    meses(6) = "Junho"
    meses(7) = "Julho"
    meses(8) = "Agosto"
    meses(9) = "Setembro"
    meses(10) = "Outubro"
    meses(11) = "Novembro"
    meses(12) = "Dezembro"

    '  Recupera dados da impressora padr�o
    DeviceDefault = LeArquivoIni("WIN.INI", "windows", "device")
    OldDefault = DeviceDefault
    '    MsgBox "Device default: " & DeviceDefault
    '    MsgBox "Old default: " & OldDefault
    '  Abre a conex�o com o Crystal Report.
    Report.Reset
    Report.Connect = rdocn.Connect
    Report.ReportFileName = App.PATH & "\apolice.rpt"
    'DIEGO
    Report.Destination = crptToPrinter    ' direciona saida para impressora.

    '
    ''Inicializa o vetor de formulas
    For i = 0 To 78
        Report.Formulas(i) = ""
    Next
    '/*******************************************************************/
    ''  Impress�o da Ap�lice
    '/*******************************************************************/
    '
    Atividade = ""

    '' Obtem Limite de indeniza��o da ap�lice
    SQL = "SELECT sum(val_is) " & vbNewLine
    SQL = SQL & "FROM " & TabEscolha & "      WITH (NOLOCK)   " & vbNewLine
    SQL = SQL & " WHERE proposta_id = " & vPropostaId & vbNewLine
    SQL = SQL & " AND dt_fim_vigencia_esc is null " & vbNewLine

    Set rc = rdocn.OpenResultset(SQL)

    If Not rc.EOF And Not IsNull(rc(0)) Then
        If Not ConfiguracaoBrasil Then
            Limite_Apolice = TrocaValorAmePorBras(Format(rc(0), "###,###,##0.00"))
        Else
            Limite_Apolice = Format(Val(rc(0)), "###,###,##0.00")
        End If
    Else
        Limite_Apolice = ""
    End If
    rc.Close

    '' Obtem Forma e Periodicidade Pgto.
    SQL = "SELECT pf.forma_pgto_id, fp.nome forma_pgto" & vbNewLine
    SQL = SQL & ", pf.periodo_pgto_id, pp.nome periodo_pgto " & vbNewLine
    SQL = SQL & "       FROM proposta_fechada_tb pf    WITH (NOLOCK)  " & vbNewLine
    SQL = SQL & " RIGHT JOIN forma_pgto_tb fp    WITH (NOLOCK)  " & vbNewLine
    SQL = SQL & "         ON fp.forma_pgto_id = pf.forma_pgto_id " & vbNewLine
    SQL = SQL & " RIGHT JOIN periodo_pgto_tb pp   WITH (NOLOCK)  " & vbNewLine
    SQL = SQL & "         ON pp.periodo_pgto_id = pf.periodo_pgto_id " & vbNewLine

    SQL = SQL & "WHERE pf.proposta_id = " & vPropostaId & vbNewLine
    'SQL = SQL & " AND fp.forma_pgto_id =* pf.forma_pgto_id " & vbNewLine 'alterado 12/09/2013 projeto 17860335 - Melhorias no SEGBR MC_Amaral -confitec
    'SQL = SQL & " AND pp.periodo_pgto_id =* pf.periodo_pgto_id " & vbNewLine 'alterado 12/09/2013 projeto 17860335 - Melhorias no SEGBR MC_Amaral -confitec

    Set rc = rdocn.OpenResultset(SQL)

    If Not rc.EOF Then
        If Not IsNull(rc!Forma_Pgto) Then
            If rc!forma_pgto_id <> "99" Then
                forma_pgto_id = rc!forma_pgto_id
                Forma_Pgto = Trim(rc!Forma_Pgto)
            Else
                forma_pgto_id = ""
                Forma_Pgto = ""
            End If
        Else
            forma_pgto_id = ""
            Forma_Pgto = ""
        End If
        '
        If Not IsNull(rc!Periodo_Pgto) Then
            If rc!forma_pgto_id <> "99" Then
                periodo_pgto_id = rc!periodo_pgto_id
                Periodo_Pgto = Trim(rc!Periodo_Pgto)
            Else
                periodo_pgto_id = ""
                Periodo_Pgto = ""
            End If
        Else
            periodo_pgto_id = ""
            Periodo_Pgto = ""
        End If

    Else
        forma_pgto_id = ""
        Forma_Pgto = ""
        periodo_pgto_id = ""
        Periodo_Pgto = ""
    End If
    rc.Close
    '' Trunca a mensagem FICHA DE COMPENSA��O BANC�RIA
    '' Para FICHA DE COMPENSA��O
    If forma_pgto_id = "3" Then
        Forma_Pgto = UCase(Left(Forma_Pgto, 20))
    End If

    '' Obtem Nome do Ramo
    SQL = "SELECT r.nome " & _
          "FROM   ramo_tb r  WITH (NOLOCK)   RIGHT JOIN proposta_basica_tb pb  WITH (NOLOCK)    " & _
        "       ON r.ramo_id = pb.ramo_id " & _
          "WHERE  pb.proposta_id = " & vPropostaId

    Set rc = rdocn.OpenResultset(SQL)

    If Not rc.EOF Then
        NomeRamo = UCase("" & rc!Nome)
    Else
        NomeRamo = ""
    End If
    rc.Close


    '' Obtem Corretores
    If SubGrupos.Count > 0 Then
        '' Para SubGrupos, descobrir TODOS os Corretores
        cont_corr = 0
        For Each vSubGrupo In SubGrupos
            For Each vCorretor In vSubGrupo.Corretores
                For i = 1 To cont_corr
                    If Cod_Susep_Corretor(i) = Format(vCorretor.Id, "#####-###") Then
                        Exit For
                    End If
                Next
                '' Se n�o achou o corretor, adiciona ao vetor
                If i > cont_corr Then
                    cont_corr = cont_corr + 1
                    ReDim Preserve Cod_Susep_Corretor(1 To cont_corr) As String
                    ReDim Preserve Nome_Corretor(1 To cont_corr) As String
                    'Alterado por fpimenta / Imar�s - 10/09/2003 (Solicitado por Marcos Scombati Martins)
                    'Cod_Susep_Corretor(cont_corr) = Format(vCorretor.Id, "#####-###")

                    'Luciana - 11/11/2003 - De: vCorretor.Id - Para: vCorretor.Codigo_susep
                    Cod_Susep_Corretor(cont_corr) = Replace(Format(Replace(vCorretor.Codigo_susep, "-", ""), "00_00_00_0_000000-0"), "_", ".")
                    Nome_Corretor(cont_corr) = vCorretor.Nome
                End If
            Next
        Next
        ''
        If cont_corr = 0 Then
            ReDim Cod_Susep_Corretor(1 To 1) As String
            ReDim Nome_Corretor(1 To 1) As String
            '
            Cod_Susep_Corretor(1) = Space(10)
            Nome_Corretor(1) = ""
        End If
    Else
        If gridCorretor.Rows > 1 Then
            ReDim Cod_Susep_Corretor(1 To gridCorretor.Rows - 1) As String
            ReDim Nome_Corretor(1 To gridCorretor.Rows - 1) As String
            '
            For i = 1 To gridCorretor.Rows - 1
                'Alterado por fpimenta / Imar�s - 10/09/2003 (Solicitado por Marcos Scombati Martins)
                'Cod_Susep_Corretor(i) = Format(gridCorretor.TextMatrix(i, 0), "###-######")

                Cod_Susep_Corretor(i) = Replace(Format(gridCorretor.TextMatrix(i, 0), "00_00_00_0_000000-0"), "_", ".")
                Nome_Corretor(i) = gridCorretor.TextMatrix(i, 2)
            Next
        Else
            ReDim Cod_Susep_Corretor(1 To 1) As String
            ReDim Nome_Corretor(1 To 1) As String
            '
            Cod_Susep_Corretor(1) = Space(10)
            Nome_Corretor(1) = ""
        End If
    End If

    '' Mensagem Inicial
    Report.Formulas(0) = "tp_documento= 'AP�LICE'"
    '' Cabecalho da Ap�lice e seus anexos
    num_apolice = Format(Val(txtApolice.Text), "000000000")
    Report.Formulas(1) = "num_apolice= '" & num_apolice & "'"
    num_endosso = Format(0, "000000000")
    Report.Formulas(2) = "num_endosso= '" & num_endosso & "'"
    apolice_anterior = Renova_Apolice()
    Report.Formulas(3) = "apolice_anterior= '" & apolice_anterior & "'"

    If Trim(txtPropostaBB.Text) <> "" Then
        num_proposta = Format(txtPropostaBB.Text, "000000000")
    Else
        num_proposta = Format(vPropostaId, "000000000")
    End If

    Report.Formulas(4) = "num_proposta= '" & num_proposta & "'"
    ''
    desc_produto = Left(txtProduto.Text, 40)
    Report.Formulas(5) = "desc_produto= '" & desc_produto & "'"
    desc_ramo = Left(Format(vRamoId, "00") & " - " & NomeRamo, 50)
    Report.Formulas(6) = "desc_ramo= '" & desc_ramo & "'"
    '
    '' Dados do Segurado

    With ConPropBasicaVida
        Report.Formulas(7) = "nome_segurado= '" & Left(txtNome.Text & Space(50), 50) & "'"
        Report.Formulas(8) = "cgc_segurado= '" & CPF_CNPJ_saida & "  " & txtCPF_CGC.Text & "'"
        Report.Formulas(9) = "endereco_segurado= '" & Left(txtEndereco.Text & Space(50), 50) & "'"
        Report.Formulas(10) = "bairro_segurado= '" & IIf(Trim(TxtBairro.Text) = "", "", Left("BAIRRO       :  " & TxtBairro.Text & Space(35), 35)) & "'"
        Report.Formulas(11) = "municipio_segurado= '" & Left(txtCidade.Text & " - " & txtUF.Text & Space(50), 50) & "'"
        Report.Formulas(12) = "cep_segurado= '" & Left(txtCEP.Text & Space(35), 35) & "'"
    End With

    '' Local do Risco
    Report.Formulas(13) = "endereco_local_risco= '" & String(60, "*") & "'"
    '
    '' Atividade Principal
    If Atividade = "" Then
        Texto1 = String(20, "*")
    Else
        Texto1 = Left(Atividade & Space(20), 20)
    End If
    Report.Formulas(14) = "atividade_principal= '" & Texto1 & "'"
    '
    '' Vig�ncia
    dt_inicio = Mid(txtIniVigencia.Text, 1, 2) & " de "
    dt_inicio = UCase(dt_inicio & meses(Val(Mid(txtIniVigencia.Text, 4, 2))) & " de " & Mid(txtIniVigencia.Text, 7, 4))
    Report.Formulas(15) = "inicio_vigencia_seguro= '" & dt_inicio & "'"
    '
    If txtFimVigencia.Text <> "" Then
        dt_fim = Mid(txtFimVigencia.Text, 1, 2) & " de "
        dt_fim = UCase(dt_fim & meses(Val(Mid(txtFimVigencia.Text, 4, 2))) & " de " & Mid(txtFimVigencia.Text, 7, 4))
        Texto1 = dt_fim
        '
    Else
        Texto1 = Space(33)
    End If
    Report.Formulas(16) = "fim_vigencia_seguro= '" & Texto1 & "'"
    '
    nlinclau = 0

    'Alterado por Vagner Saraiva-Confitec em 04/01/2011
    'Mostrar somente clausulas recentes para comparar com SEGP0236 e impress�o correta
    SQL = "SELECT c.descr_clausula "
    SQL = SQL & "FROM clausula_personalizada_tb cp WITH(NOLOCK) "
    SQL = SQL & "JOIN clausula_tb c  WITH (NOLOCK)  "
    SQL = SQL & "ON c.cod_clausula = cp.cod_clausula_original "
    SQL = SQL & "AND c.dt_inicio_vigencia = cp.dt_inicio_vigencia_cl_original "
    SQL = SQL & "Where cp.proposta_id = " & vPropostaId
    SQL = SQL & "AND cp.dt_fim_vigencia IS NULL "
    SQL = SQL & "ORDER BY cp.seq_clausula"

    Set rc = rdocn.OpenResultset(SQL)

    If Not rc.EOF Then

        'flow 204257 - Leandro A. Souza - Stefanini IT - 05/03/2007
        If SSTabProp.Tab = 8 Then
            Report.Formulas(17) = "texto_anexos_1= '" & String(100, "=") & "'"
            Report.Formulas(18) = "texto_anexos_2= '" & Space(47) & "ANEXOS" & "'"
            Report.Formulas(19) = "texto_anexos_3= '" & String(100, "=") & "'"
            nlinclau = 3
            '
            While Not rc.EOF
                Texto1 = Left(Space(10) & "- " & Trim$(rc!descr_clausula & Space(90)), 90)
                nlinclau = nlinclau + 1
                texto_anexos(nlinclau) = Texto1
                rc.MoveNext
            Wend
        End If
        rc.Close
        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

    Else
        '
        Report.Formulas(17) = "texto_anexos_1= ''"
        Report.Formulas(18) = "texto_anexos_2= ''"
        Report.Formulas(19) = "texto_anexos_3= ''"
    End If
    '
    '' Preenche as linhas de cl�usula restantes com branco
    For i = nlinclau + 1 To 17
        texto_anexos(i) = Space(90)
    Next i
    '
    For ind_formula = 20 To 33
        ind_aux = ind_formula - 16
        Report.Formulas(ind_formula) = "texto_anexos_" & CStr(ind_aux) & "= '" & texto_anexos(ind_aux) & "'"
    Next ind_formula

    '' Linha Valores de Pr�mio
    'Pr�mio L�quido
    If Not ConfiguracaoBrasil Then
        Liquido = CDbl(TrocaValorAmePorBras(txtTotalPremioLiq.Text)) - CDbl(TrocaValorAmePorBras(txtTotalDescontos.Text))
        Texto1 = "R$ " & TrocaValorAmePorBras(Format(Liquido, "###,###,##0.00"))
    Else
        Liquido = CDbl(txtTotalPremioLiq.Text) - CDbl(txtTotalDescontos.Text)
        Texto1 = "R$ " & Format(Liquido, "###,###,##0.00")
    End If
    Report.Formulas(34) = "val_premio_liquido_total= '" & Texto1 & "'"
    'Juros / Custo / IOF
    Report.Formulas(35) = "val_juros= '" & "R$ " & txtValJuros.Text & "'"
    Report.Formulas(36) = "val_custo_apolice= '" & "R$ " & txtCustoApolice.Text & "'"
    Report.Formulas(37) = "val_iof= '" & "R$ " & txtIOF.Text & "'"
    'Pr�mio Bruto
    Premio_Bruto = CDbl(txtValJuros.Text) + CDbl(txtCustoApolice.Text)
    Premio_Bruto = Premio_Bruto + CDbl(txtIOF.Text) + CDbl(Liquido)
    If Not ConfiguracaoBrasil Then
        Texto1 = "R$ " & TrocaValorAmePorBras(Format(Premio_Bruto, "###,###,##0.00"))
    Else
        Texto1 = "R$ " & Format(Premio_Bruto, "###,###,##0.00")
    End If
    Report.Formulas(38) = "val_premio_bruto_total= '" & Texto1 & "'"

    '' Parcelamento
    '
    SQL = "SELECT  num_cobranca, dt_agendamento, val_cobranca "
    SQL = SQL & "FROM agendamento_cobranca_tb  WITH (NOLOCK)  "
    SQL = SQL & " WHERE proposta_id = " & vPropostaId
    SQL = SQL & " AND isnull(num_endosso, 0) = 0"
    SQL = SQL & " ORDER BY num_cobranca"
    '
    Set rc1 = rdocn.OpenResultset(SQL)
    QtdDatas = 0
    If Not rc1.EOF Then
        ValParcela1 = Val(rc1!val_cobranca)
        QtdDatas = QtdDatas + 1
        DataAgendamento(QtdDatas) = Format$(rc1!dt_agendamento, "dd/mm/yyyy")
        '
        rc1.MoveNext
        If rc1.EOF Then    's� tem uma parcela
            ValParcelaDemais = 0
            ValUltParcela = 0
        Else    'tem mais de uma parcela
            ValParcelaDemais = Val(rc1!val_cobranca)
            ValUltParcela = 0
            QtdDatas = QtdDatas + 1
            DataAgendamento(QtdDatas) = Format$(rc1!dt_agendamento, "dd/mm/yyyy")
            '
            rc1.MoveNext
            Do While Not rc1.EOF
                QtdDatas = QtdDatas + 1
                DataAgendamento(QtdDatas) = Format$(rc1!dt_agendamento, "dd/mm/yyyy")
                ValUltParcela = Val(0 & rc1!val_cobranca)
                rc1.MoveNext
            Loop
            If ValParcelaDemais = ValUltParcela Then
                ValUltParcela = 0
            End If
        End If
    Else
        ValParcela1 = 0
        ValParcelaDemais = 0
        ValUltParcela = 0
    End If

    If QtdDatas > 0 Then
        Report.Formulas(39) = "txt_forma_pgto_1= '" & "QT. PARCELAS    :" & "'"
        Report.Formulas(40) = "forma_pgto_1= '" & Format(QtdDatas, "00") & "'"
        '
        Report.Formulas(41) = "txt_forma_pgto_2= '" & "PARCELA 01      :" & "'"
        Texto1 = Format(ValParcela1, "###,###,##0.00")
        Report.Formulas(42) = "forma_pgto_2= '" & "R$ " & Space(14 - Len(Texto1)) & Texto1 & "'"
        '
        Report.Formulas(43) = "txt_forma_pgto_3= '" & "DEMAIS PARCELAS :" & "'"
        Texto1 = Format(ValParcelaDemais, "###,###,##0.00")
        Report.Formulas(44) = "forma_pgto_3= '" & "R$ " & Space(14 - Len(Texto1)) & Texto1 & "'"
        '
        Report.Formulas(45) = "txt_forma_pgto_4= '" & "�LTIMA PARCELA  :" & "'"
        Texto1 = Format(ValUltParcela, "###,###,##0.00")
        Report.Formulas(46) = "forma_pgto_4= '" & "R$ " & Space(14 - Len(Texto1)) & Texto1 & "'"
        '
        '' Vencimentos
        If Val(txtPagoAto.Text) > 0 Then
            DataAgendamento(1) = " � VISTA"
        End If
        '
        For i = 1 To 12
            If DataAgendamento(i) <> "" Then
                Report.Formulas(46 + i) = "vencimento_" & i & "='" & Format(i, "00") & "-" & DataAgendamento(i) & "'"
            Else
                Report.Formulas(46 + i) = "vencimento_" & i & "=''"
            End If
        Next
    Else
        Report.Formulas(39) = "txt_forma_pgto_1= '" & Space(17) & "'"
        Report.Formulas(40) = "forma_pgto_1= '" & Space(17) & "'"
        '
        If Periodo_Pgto <> "" And Premio_Bruto = 0 Then
            'Report.Formulas(41) = "txt_forma_pgto_2= '" & "FATURA " & Periodo_Pgto & "'"
            'Alterado por Francisco em 07/11/2005 (Stefanini).
            Report.Formulas(41) = "txt_forma_pgto_2= '" & "FATURA           " & "'"
        Else
            Report.Formulas(41) = "txt_forma_pgto_2= '" & Space(17) & "'"
        End If
        Report.Formulas(42) = "forma_pgto_2= '" & Space(17) & "'"
        '
        Report.Formulas(43) = "txt_forma_pgto_3= '" & Space(17) & "'"
        Report.Formulas(44) = "forma_pgto_3= '" & Space(17) & "'"
        '
        Report.Formulas(45) = "txt_forma_pgto_4= '" & Space(17) & "'"
        Report.Formulas(46) = "forma_pgto_4= '" & Space(17) & "'"
        '
        For i = 1 To 12
            Report.Formulas(46 + i) = "vencimento_" & i & "= '" & Space(13) & "'"
        Next
    End If
    '' Forma de Cobran�a
    Report.Formulas(59) = "forma_cobranca= '" & Forma_Pgto & "'"

    '' Ag�ncia de Contrata��o
    If txtCodAgencia.Text <> "" Then
        Texto1 = Left(txtCodAgencia.Text & Space(4), 4)
        Texto1 = Texto1 & "-" & Left(txtNomeAgencia.Text & Space(18), 18)
    Else
        Texto1 = Space(25)
    End If
    '
    Report.Formulas(60) = "agencia_cobranca= '" & Texto1 & "'"

    Texto2 = ""
    '
    Dim x As Integer
    Dim VNomeCorretor As String
    Dim VCodSusepCorretor As String


    'ALTERADO POR ODAIR SANTOS - CONFITEC - INC000003903858 - INICIO
    If cboSubGrupo(Index).ListIndex <> -1 Then
        ind_sub = CStr(cboSubGrupo(Index).ItemData(cboSubGrupo(Index).ListIndex))
        vSubGrupoId = ind_sub
        vSubGrupoOk = True

        Select Case Index
        Case 0  'Corretagem
            Set Corretores = Nothing
            Set Corretores = SubGrupos(ind_sub).Corretores
            VNomeCorretor = ""
            VCodSusepCorretor = ""
            Dim contador As Integer
            contador = 1
            If Corretores.Count > 1 Then
                For Each vCorretor In Corretores
                    If contador = 1 Then
                        Report.Formulas(61) = "desc_corretor= '" & Nome_Corretor(contador) & "'"
                        Report.Formulas(62) = "cod_susep= '" & Cod_Susep_Corretor(contador) & "'"
                    Else
                        If contador = 2 Then
                            VNomeCorretor = "'" & Nome_Corretor(contador) & "'"
                            VCodSusepCorretor = "'" & Cod_Susep_Corretor(contador) & "'"
                        Else
                            If InStr(1, VNomeCorretor, Nome_Corretor(contador), 1) = 0 Then     'Agrigorio - 17/07/2003
                                VNomeCorretor = VNomeCorretor & " + chr(13) + " & "'" & Nome_Corretor(contador) & "'"
                                VCodSusepCorretor = VCodSusepCorretor & " + chr(13) + " & "'" & Cod_Susep_Corretor(contador) & "'"
                            End If
                        End If
                    End If
                    contador = contador + 1
                Next
                Report.Formulas(64) = "obs_corretor= " & VNomeCorretor
                Report.Formulas(65) = "obs_susep= " & VCodSusepCorretor
            Else
                Report.Formulas(61) = "desc_corretor= '" & Nome_Corretor(1) & "'"
                Report.Formulas(62) = "cod_susep= '" & Cod_Susep_Corretor(1) & "'"
            End If
        End Select
    End If

    'If cont_corr > 1 Then
    '    VNomeCorretor = ""
    '    VCodSusepCorretor = ""
    '  For x = 1 To cont_corr
    '       If x = 1 Then
    '            Report.Formulas(61) = "desc_corretor= '" & Nome_Corretor(x) & "'"
    '            Report.Formulas(62) = "cod_susep= '" & Cod_Susep_Corretor(x) & "'"
    '        Else
    '            If x = 2 Then
    '                VNomeCorretor = "'" & Nome_Corretor(x) & "'"
    '                VCodSusepCorretor = "'" & Cod_Susep_Corretor(x) & "'"
    '            Else
    '               If InStr(1, VNomeCorretor, Nome_Corretor(x), 1) < 0 Then     'Agrigorio - 17/07/2003
    '                  VNomeCorretor = VNomeCorretor & " + chr(13) + " & "'" & Nome_Corretor(x) & "'"
    '                  VCodSusepCorretor = VCodSusepCorretor & " + chr(13) + " & "'" & Cod_Susep_Corretor(x) & "'"
    '               End If
    '            End If
    '        End If
    '    Next x

    '    Report.Formulas(64) = "obs_corretor= " & VNomeCorretor
    '    Report.Formulas(65) = "obs_susep= " & VCodSusepCorretor
    'Else
    '    Report.Formulas(61) = "desc_corretor= '" & Nome_Corretor(1) & "'"
    '    Report.Formulas(62) = "cod_susep= '" & Cod_Susep_Corretor(1) & "'"
    'End If

    'ALTERADO POR ODAIR SANTOS - CONFITEC - INC000003903858 - FIM

    Report.Formulas(63) = "inspetoria= '" & String(10, "*") & "'"

    If Trim(txtDtEndosso.Text) = "" Then
        If Not IsDate(txtDtEmissao.Text) Then
            MsgBox "Data de Emiss�o da Ap�lice inv�lida!!"
            GoTo Erro
        End If

        data_rodape = txtDtEmissao.Text
    Else
        data_rodape = txtDtEndosso.Text
    End If

    Observacao = Mid(data_rodape, 1, 2) & " de "
    Observacao = UCase(Observacao & meses(Val(Mid(data_rodape, 4, 2))) & " de " & Mid(data_rodape, 7, 4))

    Texto2 = "S�O PAULO, " & Observacao    '
    Report.Formulas(66) = "local_data_emissao= '" & Texto2 & "'"

    ' Traz Numero Processo SUSEP do produto.
    SQL = "SELECT num_proc_susep "
    SQL = SQL & "FROM produto_tb  WITH (NOLOCK)  "
    SQL = SQL & "WHERE produto_id = " & Val(Left(txtProduto.Text, InStr(txtProduto.Text, "-") - 1))
    SQL = SQL & " AND (num_proc_susep is not null) "
    SQL = SQL & "AND (num_proc_susep <> ' ')"
    '
    Set rc = rdocn.OpenResultset(SQL)
    '
    If Not rc.EOF Then
        Report.Formulas(67) = "num_proc_susep= 'Processo SUSEP : " & rc!num_proc_susep & "'"
    End If
    '
    'Pede a impressora a ser utilizada
    dialog.CancelError = True
    dialog.ShowPrinter
    '
    If Err = 32755 Then
        MousePointer = vbDefault
        Exit Sub
    End If

    '' Seta a impressora
    '  Recupera dados da impressora padr�o
    DeviceDefault = LeArquivoIni("WIN.INI", "windows", "device")

    If DeviceDefault <> OldDefault Then
        MudouPrtDefault = True
    Else
        MudouPrtDefault = False
    End If

    '  Imprime documento.
    Report.Action = 1
    ''
    ''
    On Error GoTo cancelar

    If nlinclau > 0 Then
        resp = MsgBox("Favor colocar o formul�rio Anexo", vbOKCancel, Me.Caption)
        If resp = vbCancel Then
            GoTo cancelar
        End If

        ''Inicializa o vetor de formulas
        For i = 0 To 78
            Report.Formulas(i) = ""
        Next
        '/*******************************************************************/
        ''  Impress�o dos Anexos
        '/*******************************************************************/
        Report.ReportFileName = App.PATH & "\anexos.rpt"
        Report.Formulas(0) = "num_apolice_anexos= '" & num_apolice & "'"
        Report.Formulas(1) = "num_endosso_anexos= '" & num_endosso & "'"
        Report.Formulas(2) = "apolice_anterior_anexos= '" & apolice_anterior & "'"
        Report.Formulas(3) = "num_proposta_anexos= '" & num_proposta & "'"
        Report.Formulas(4) = "desc_produto_anexos= '" & desc_produto & "'"
        Report.Formulas(5) = "desc_ramo_anexos= '" & desc_ramo & "'"
        'Passando o usu�rio como par�metro para o relat�rio - Diego - GPTI - 29/07/2008
        Report.StoredProcParam(0) = Usuario


        ' Autor: pablo.cardoso (Nova Consultoria) - Data da Altera��o: 13/02/2011
        ' Demanda: 12784293 Item: 9 - Inserir logotipo e endere�o da Cia em todas as p�ginas do clausulado.
        ' Descri��o: Enviar dados do segurado para o cabe�alho do relat�rio
        Report.Formulas(6) = "nome_segurado= '" & Left(txtNome.Text & Space(50), 50) & "'"
        Report.Formulas(7) = "cgc_segurado= '" & CPF_CNPJ_saida & "  " & txtCPF_CGC.Text & "'"
        Report.Formulas(8) = "endereco_segurado= '" & Left(txtEndereco.Text & Space(50), 50) & "'"
        Report.Formulas(9) = "bairro_segurado= '" & IIf(Trim(TxtBairro.Text) = "", "", Left("BAIRRO       :  " & TxtBairro.Text & Space(35), 35)) & "'"
        Report.Formulas(10) = "municipio_segurado= '" & Left(txtCidade.Text & " - " & txtUF.Text & Space(50), 50) & "'"
        Report.Formulas(11) = "cep_segurado= '" & Left(txtCEP.Text & Space(35), 35) & "'"
        Report.Formulas(12) = "tp_documento= 'AP�LICE'"

        On Error GoTo cancelar

        'FLOW 381024 - MFerreira - Confitec Sistemas - 2008-06-25
        'Substituindo query para buscar as coberturas de forma
        'incremental em rela��o ao endosso selecionado
        '---------------------------------------------------------
        'SQL = "SELECT texto_clausula "
        'SQL = SQL & "FROM clausula_personalizada_tb cp  WITH (NOLOCK) "
        'SQL = SQL & "WHERE cp.proposta_id = " & vPropostaId
        'SQL = SQL & " AND isnull(cp.endosso_id, 0) = 0 "
        'SQL = SQL & " ORDER BY cp.seq_clausula"
        '---------------------------------------------------------
        If indEndosso Then
            SQL = "SELECT cod_clausula_original, seq_clausula, isnull(texto_clausula,'') as txt_clausula "
            SQL = SQL & "  FROM clausula_personalizada_tb cp  WITH (NOLOCK) "
            SQL = SQL & " WHERE 1 = 1 " & vbNewLine
            SQL = SQL & "   AND cp.proposta_id      = " & vPropostaId & vbNewLine
            SQL = SQL & "   AND (cp.dt_fim_vigencia IS NULL OR " & vbNewLine
            SQL = SQL & "        cp.dt_fim_vigencia >= '" & Format(txtDtEndosso.Text, "yyyymmdd") & "')" & vbNewLine
            SQL = SQL & "   AND ISNULL(cp.endosso_id, 0) <= " & IIf(txtNumEndosso.Text = "", 0, txtNumEndosso.Text) & vbNewLine
            SQL = SQL & " ORDER BY cp.seq_clausula"
        Else
            SQL = "SELECT cod_clausula_original, seq_clausula, isnull(texto_clausula,'') as txt_clausula "
            'SQL = "SELECT texto_clausula "
            SQL = SQL & "FROM clausula_personalizada_tb cp  WITH (NOLOCK) "
            SQL = SQL & "WHERE cp.proposta_id = " & vPropostaId
            SQL = SQL & " AND isnull(cp.endosso_id, 0) = 0 "
            SQL = SQL & " ORDER BY cp.seq_clausula"
        End If

        '
        Set rc = rdocn.OpenResultset(SQL)
        '
        If Not rc.EOF Then
            'Para cada cl�usula, formatar e imprimir
            'APAGAR REGISTROS ANTIGOS DO USUARIO - Diego - GPTI - 29/07/2008
            'INICIO - INC000004357092 - 15/08/2014 - Colocar o usuario entre aspas simples
            SQL = "EXEC SEGS7129_SPD '" & Usuario & "'"
            'SQL = "EXEC SEGS7129_SPD " & Usuario
            'FIM - INC000004357092 - 15/08/2014 - Colocar o usuario entre aspas simples
            Set rc_del = rdoCn1.OpenResultset(SQL)
            Do While Not rc.EOF
                Dim codigoClausula As Double
                Dim sequencial As Integer
                Texto = rc("txt_clausula")
                codigoClausula = rc!cod_clausula_original
                sequencial = rc!seq_clausula

                'Texto = rc("cod_clausula_original")
                'flow 204257 - Leandro A. Souza - Stefanini IT - 05/03/2007
                If SSTabProp.Tab <> 8 Then Exit Do
                '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                'Texto2 = Formata_Clausula(rc!texto_clausula)
                Texto2 = Formata_Clausula(Texto)


                'INSERIR NA TABELA DO RELAT�RIO - Diego - GPTI - 29/07/2008
                SQL = "EXEC SEGS7128_SPI '" & Texto2 & "', " & codigoClausula & ", " & sequencial & ", '" & Usuario & "'"
                Set rc_proc = rdoCn1.OpenResultset(SQL)
                '' Seta posicao da primeira linha de impressao do texto da clausula
                '                ultquebra = 1
                '                ind_anexos = 0
                '                inicio_linha_anexos = 1
                '                fim_linha_anexos = 1
                '                tamanho_linha_anexos = 0
                '                '' Imprime cl�usula, colocando cabe�alho a cada quebra de p�gina
                '                For i = 1 To Len(Texto2)
                '                    '' Procura 'returns' no texto da clausula para
                '                    '' contar o no. de linhas; achou CR_LF, passou para nova linha.
                '                    'MFILIE - 11/08/2004 - ALTERADO PARA IMPRIMIR A �LTIMA LINHA
                '                    'If Mid(Texto2, i, 2) = Chr(13) & Chr(10)  Then
                '                    'Alessandra Grig�rio - 17/08/2004 - Validar o produto para impressao
                '                    If Val(Left(Trim(txtProduto.Text), 3)) = 720 Then
                '                        If Mid(Texto2, i, 2) = Chr(13) & Chr(10) Or i = Len(Texto2) Then
                '                        ' Movimenta o conteudo da linha para o vetor de linhas_anexo.
                '                            fim_linha_anexos = i - 1
                '                            tamanho_linha_anexos = (fim_linha_anexos - inicio_linha_anexos) + 1
                '                            ind_anexos = ind_anexos + 1
                '                            linha_anexos(ind_anexos) = Mid(Texto2, inicio_linha_anexos, tamanho_linha_anexos)
                '                            'Alterado por Cleber - data: 18/08/2006 - Flow: 155461
                '                            linha_anexos(ind_anexos) = Replace(linha_anexos(ind_anexos), Chr(10), "")
                '                            '-----------------------------------------------------
                '                            Report.Formulas(ind_anexos + 5) = "linha_" & CStr(ind_anexos + 2) & "= '" & linha_anexos(ind_anexos) & "'"
                '                            inicio_linha_anexos = i + 2
                '                        End If
                '                     Else
                '                        If Mid(Texto2, i, 2) = Chr(13) & Chr(10) Then
                '                        ' Movimenta o conteudo da linha para o vetor de linhas_anexo.
                '                            fim_linha_anexos = i - 1
                '                            tamanho_linha_anexos = (fim_linha_anexos - inicio_linha_anexos) + 1
                '                            ind_anexos = ind_anexos + 1
                '                            linha_anexos(ind_anexos) = Mid(Texto2, inicio_linha_anexos, tamanho_linha_anexos)
                '                            'Alterado por Cleber - data: 18/08/2006 - Flow: 155461
                '                            linha_anexos(ind_anexos) = Replace(linha_anexos(ind_anexos), Chr(10), "")
                '                            '-----------------------------------------------------
                '                            Report.Formulas(ind_anexos + 5) = "linha_" & CStr(ind_anexos + 2) & "= '" & linha_anexos(ind_anexos) & "'"
                '                            inicio_linha_anexos = i + 2
                '                        End If
                '                    End If
                '                    '' Quando chega na linha 71, pula para nova p�gina
                '                    If ind_anexos = 71 Then
                '                    '   imprime relatorio - as 71 linhas sao descarregadas na saida e action=1
                '                        Report.Action = 1
                '                        ind_anexos = 0
                '                        aux = Mid(Texto2, ultquebra, i + 1 - ultquebra)
                '                        ultquebra = i + 2
                '                        i = i + 1
                '                    End If 'Quebra de pagina (quando cheia) !
                '                Next i
                '                '
                '                If ultquebra < Len(Texto2) Then
                '                    For ind_formula = 6 To ind_anexos
                '                        Report.Formulas(ind_formula) = "linha_" & CStr(ind_formula - 3) & "= '" & linha_anexos(ind_formula - 5) & "'"
                '                    Next ind_formula
                '                    '
                '                    If ind_anexos < 71 Then
                '                        For ind_formula = ind_anexos + 1 To 71
                '                            linha_anexos(ind_formula) = Space(90)
                '                            Report.Formulas(ind_formula + 5) = "linha_" & CStr(ind_formula + 2) & "= '" & linha_anexos(ind_formula) & "'"
                '                        Next ind_formula
                '                    End If
                '                    '
                '                    Report.Action = 1
                '                    ind_anexos = 0

                rc.MoveNext
            Loop
            Report.Action = 1
        End If
    End If

    If MudouPrtDefault Then
        Call RetornaPrtDefault("WIN.INI", OldDefault)
    End If

    MsgBox "Impress�o Concluida", vbInformation + vbOKOnly, "Aviso"

cancelar:
    If MudouPrtDefault Then
        Call RetornaPrtDefault("WIN.INI", OldDefault)
    End If
    MousePointer = vbDefault
    Exit Sub

Erro:
    If MudouPrtDefault Then
        Call RetornaPrtDefault("WIN.INI", OldDefault)
    End If
    MousePointer = vbDefault
    TrataErroGeral "Impress�o de Ap�lice"
    MsgBox "Houve um erro na impress�o da ap�lice. O processo ser� abortado."

End Sub
Sub Monta_CboComponente()
    Dim vComponente As Componente
    Dim straux As String

    CboComponente.Clear
    '
    For Each vComponente In Componentes
        straux = Format(vComponente.Codigo, "000") & " - " & vComponente.Nome
        '
        CboComponente.AddItem straux
        CboComponente.ItemData(CboComponente.NewIndex) = vComponente.Codigo
    Next

End Sub


Sub Monta_CboEstipulante()
    Dim vEstip As estipulante

    cboEstipulante.Clear
    '
    If Estipulantes.Count > 0 Then
        For Each vEstip In Estipulantes
            cboEstipulante.AddItem vEstip.Nome
            cboEstipulante.ItemData(cboEstipulante.NewIndex) = vEstip.Id
            '
        Next
        '
        cboEstipulante.ListIndex = 0
    End If

End Sub


Sub Monta_CboObjSegurado()
    Dim vComponente As Componente
    Dim straux As String

    cboObjSegurado.Clear
    '
    For Each vComponente In Componentes
        straux = Format(vComponente.Codigo, "000") & " - " & vComponente.Nome
        '
        cboObjSegurado.AddItem straux
        cboObjSegurado.ItemData(cboObjSegurado.NewIndex) = vComponente.Codigo
    Next

End Sub

Sub Monta_CboComponenteQuestionario()
    Dim vComponente As Componente
    Dim straux As String

    cboComponenteQuestionario.Clear
    '
    For Each vComponente In Componentes
        straux = Format(vComponente.Codigo, "000") & " - " & vComponente.Nome
        '
        cboComponenteQuestionario.AddItem straux
        cboComponenteQuestionario.ItemData(cboComponenteQuestionario.NewIndex) = vComponente.Codigo
    Next

End Sub

Sub Monta_GridAdministradores()
    Dim linha As String
    Dim vAdm As Administrador

    'Genesco Silva - Confitec SP - Flow 4532893
    'Visualiza��o da Forma de Envio na Grid Administradores

    Dim usql As String
    Dim rcMarca As rdoResultset
    Const Marcado = "�"
    Const Desmarcado = "q"


    GridAdm.Rows = 1
    '
    'Query que verifica se o subgrupo � quem receber� as postagens
    usql = "select sbe.envia_sega as 'Resposta' from sub_grupo_apolice_endereco_tb sbe WITH(NOLOCK)"
    usql = usql & " join sub_grupo_apolice_tb sg WITH(NOLOCK)"
    usql = usql & " on sg.apolice_id = sbe.apolice_id and sg.sub_grupo_id = sbe.sub_grupo_id "
    usql = usql & " where sg.apolice_id = " & txtApolice.Text
    If cboSubGrupo(3).ListIndex <> -1 Then
        usql = usql & " and sg.sub_grupo_id = " & cboSubGrupo(3).ItemData(cboSubGrupo(3).ListIndex)
    End If
    'Incidente:INC000003898012
    'Autor da Altera��o:Guilherme Henrique da Silva - CONFITEC - SP
    'Data de Altera��o:08/01/2013
    'Motivo da Altera��o: Permitir que o sistema abra a apolice sem subGrupo


    Set rcMarca = rdocn.OpenResultset(usql)
    '
    For Each vAdm In Administradores

        'Verifica a Forma de Envio do Subgrupo Corrente
        If Not rcMarca.EOF Then
            If UCase(rcMarca("Resposta")) = "S" Then
                linha = Marcado
            Else
                linha = Desmarcado
            End If
        Else
            linha = Desmarcado
        End If

        linha = linha & vbTab & vAdm.Id
        linha = linha & vbTab & vAdm.Nome
        linha = linha & vbTab & Format(vAdm.PercProLabore, "0.00")
        '
        GridAdm.AddItem linha
        GridAdm.RowData(GridAdm.Rows - 1) = vAdm.Id

        'Configura a c�lula da CheckBox da Forma de Envio
        GridAdm.Row = 1
        GridAdm.col = 0
        GridAdm.CellFontName = "Wingdings"
        GridAdm.CellFontSize = 11
        GridAdm.CellAlignment = flexAlignCenterCenter
        'Fim Genesco Silva
    Next
    '
    GridAdm.Refresh

End Sub


Sub Monta_GridComponentes()
    Dim linha As String
    Dim vComponente As Componente

    GridComponente.Rows = 1
    '
    For Each vComponente In Componentes
        linha = Format(vComponente.Codigo, "000")
        linha = linha & vbTab & vComponente.Nome
        linha = linha & vbTab & Format(vComponente.CodSubRamo, "0000")
        '
        GridComponente.AddItem linha
        GridComponente.RowData(GridComponente.Rows - 1) = vComponente.Codigo
    Next
    '
    GridComponente.Refresh

End Sub


Private Sub Ler_Administracao()
    Dim rc As rdoResultset, rc1 As rdoResultset
    Dim SQL As String
    Dim linha As String
    Dim vEstip As estipulante
    Dim vAdm As Administrador
    Dim PercPL As String

    On Error GoTo Erro

    If SubGrupos.Count = 0 Then
        'INICIO  'alterado 12/09/2013 projeto 17860335 - Melhorias no SEGBR MC_Amaral -confitec
        '        SQL = "SELECT est_cliente_id, nome, dt_inicio_representacao "
        '        SQL = SQL & "FROM representacao_proposta_tb   WITH (NOLOCK) , cliente_tb    WITH (NOLOCK) "
        '        SQL = SQL & "WHERE proposta_id = " & vPropostaId
        '        SQL = SQL & " AND dt_inicio_representacao <= '" & dt_inicio_query & "'"
        '        SQL = SQL & " AND ( dt_fim_representacao is null "
        '        SQL = SQL & "     OR dt_fim_representacao >= '" & dt_inicio_query & "')"
        '        SQL = SQL & " AND cliente_id = est_cliente_id"
        '        '
        '        Set rc = rdocn.OpenResultset(SQL)
        '
        '        While Not rc.EOF
        '            Set vEstip = New Estipulante
        '            '
        '            vEstip.Id = rc!est_cliente_id
        '            vEstip.Nome = Trim(rc!Nome)
        '            '' Seleciona os administradores ligados ao estipulante
        '            SQL = "SELECT adm_cliente_id, nome, perc_pro_labore "
        '            SQL = SQL & "FROM administracao_apolice_tb   WITH (NOLOCK) , cliente_tb    WITH (NOLOCK) "
        '            SQL = SQL & "WHERE proposta_id = " & vPropostaId
        '            SQL = SQL & " AND est_cliente_id = " & rc!est_cliente_id
        '            SQL = SQL & " AND dt_inicio_representacao = '" & Format(rc!dt_inicio_representacao, "yyyymmdd") & "'"
        '            SQL = SQL & " AND dt_inicio_administracao <= '" & dt_inicio_query & "'"
        '            SQL = SQL & " AND (dt_fim_administracao is null "
        '            SQL = SQL & "     OR dt_fim_administracao >= '" & dt_inicio_query & "')"
        '            SQL = SQL & " AND cliente_id = adm_cliente_id"
        '            '

        SQL = "     SELECT distinct ap.adm_cliente_id, c.nome, ap.perc_pro_labore,rp.est_cliente_id "
        SQL = SQL & "  FROM administracao_apolice_tb ap  WITH (NOLOCK) "
        SQL = SQL & "  JOIN   cliente_tb c   WITH (NOLOCK) "
        SQL = SQL & "    ON   c.cliente_id = ap.adm_cliente_id "
        SQL = SQL & "  JOIN  representacao_proposta_tb  rp  WITH (NOLOCK) "
        SQL = SQL & "    ON c.cliente_id = RP.est_cliente_id "
        SQL = SQL & " WHERE ap.proposta_id = " & vPropostaId
        SQL = SQL & "   AND ap.dt_inicio_administracao <= '" & dt_inicio_query & "'"
        SQL = SQL & "   AND (ap.dt_fim_administracao is null "
        SQL = SQL & "     OR ap.dt_fim_administracao >= '" & dt_inicio_query & "')"





        'Fim   'alterado 12/09/2013 projeto 17860335 - Melhorias no SEGBR MC_Amaral -confitec

        Set rc = rdoCn1.OpenResultset(SQL)

        If Not rc.EOF Then
            Set vEstip = New estipulante
            Set vEstip.Administradores = New Collection
            '
            While Not rc.EOF

                vEstip.Id = rc!est_cliente_id
                vEstip.Nome = Trim(rc!Nome)

                Set vAdm = New Administrador
                vAdm.Id = rc!adm_cliente_id
                vAdm.Nome = Trim(rc!Nome)



                If ConfiguracaoBrasil Then
                    PercPL = Format(Val(rc!perc_pro_labore), "0.00")
                Else
                    PercPL = TrocaValorAmePorBras(Format(Val(rc!perc_pro_labore), "0.00"))
                End If
                vAdm.PercProLabore = PercPL
                '
                vEstip.Administradores.Add vAdm, CStr(rc!adm_cliente_id)
                Estipulantes.Add vEstip, CStr(rc!est_cliente_id)
                '
                rc.MoveNext
            Wend
            '
        End If
        '            Estipulantes.Add vEstip, CStr(rc!est_cliente_id)
        '            '
        '            rc.MoveNext
        '        Wend
        '        '
        '        Monta_CboEstipulante
        '        '
        '        rc.Close
        '        Set rc = Nothing
        '
        'If cboEstipulante.ListCount > 0 Then
        '    cboEstipulante.ListIndex = 0
        'End If
        '
    Else
        'inicio    'alterado 12/09/2013 projeto 17860335 - Melhorias no SEGBR MC_Amaral -confitec
        'Alterado por Vagner Saraiva-Confitec em 06/12/2010
        'Mostrar tamb�m os que a data de in�cio de apresenta��o � mais recente que 31-03-2009 (regra que j� existia)
        '        SQL = "SELECT representacao_sub_grupo_tb.sub_grupo_id, est_cliente_id, CLIENTE_TB.nome, dt_inicio_representacao "
        '        SQL = SQL & "FROM representacao_sub_grupo_tb  (nolock)                  "
        '        SQL = SQL & " INNER JOIN      cliente_tb   (nolock)                     "
        '        SQL = SQL & " ON cliente_id = est_cliente_id                            "
        '        SQL = SQL & " WHERE representacao_sub_grupo_tb.apolice_id           = " & vApoliceId
        '        SQL = SQL & " AND representacao_sub_grupo_tb.sucursal_seguradora_id = " & vSucursalId
        '        SQL = SQL & " AND representacao_sub_grupo_tb.seguradora_cod_susep   = " & vSeguradoraId
        '        SQL = SQL & " AND representacao_sub_grupo_tb.ramo_id                = " & vRamoId
        '        'SQL = SQL & " AND dt_inicio_representacao                           <= '" & dt_inicio_query & "'"
        '        SQL = SQL & " AND ( dt_fim_representacao                            is null "
        '        SQL = SQL & "     OR dt_fim_representacao                           >= '" & dt_inicio_query & "')"
        '
        '        Set rc = rdocn.OpenResultset(SQL)
        '        '
        '        While Not rc.EOF
        '            Set vEstip = New Estipulante
        '            '
        '            vEstip.Id = rc!est_cliente_id
        '            vEstip.Nome = Trim(rc!Nome)
        '            '' Seleciona os administradores ligados ao estipulante
        '
        '            SQL = "SELECT adm_sub_grupo_apolice_tb.sub_grupo_id, adm_cliente_id, cliente_tb.nome"
        '            SQL = SQL & ", perc_pro_labore, dt_inicio_administracao "
        '            SQL = SQL & "FROM adm_sub_grupo_apolice_tb (nolock)  "
        '            SQL = SQL & " INNER JOIN cliente_tb (nolock)"
        '            SQL = SQL & "         ON cliente_tb.cliente_id = adm_cliente_id "
        '            SQL = SQL & "WHERE adm_sub_grupo_apolice_tb.apolice_id = " & vApoliceId
        '            SQL = SQL & " AND adm_sub_grupo_apolice_tb.sucursal_seguradora_id = " & vSucursalId
        '            SQL = SQL & " AND adm_sub_grupo_apolice_tb.seguradora_cod_susep = " & vSeguradoraId
        '            SQL = SQL & " AND adm_sub_grupo_apolice_tb.ramo_id = " & vRamoId
        '            'Rafael Oshiro 21/03/2006 - Data de fim de vig�ncia deve ser nula
        '            'SQL = SQL & " AND dt_inicio_administracao <= '" & dt_inicio_query & "'"
        '            'SQL = SQL & " AND (dt_fim_administracao is null "
        '            'SQL = SQL & "     OR dt_fim_administracao >= '" & dt_inicio_query & "')"
        '            'SQL = SQL & " AND dt_inicio_representacao = '" & Format(rc!dt_inicio_representacao, "yyyymmdd") & "'"
        '            SQL = SQL & " AND dt_fim_administracao IS NULL "
        '
        '            SQL = SQL & " AND adm_sub_grupo_apolice_tb.sub_grupo_id = " & rc!sub_grupo_id
        '            SQL = SQL & " AND est_cliente_id = " & rc!est_cliente_id
        '              Set rc1 = rdoCn1.OpenResultset(SQL)
        '            If Not rc1.EOF Then
        '                Set vEstip.Administradores = New Collection
        '                '
        '                While Not rc1.EOF
        '                    Set vAdm = New Administrador
        '                    vAdm.Id = rc1!adm_cliente_id
        '                    vAdm.Nome = Trim(rc1!Nome)
        '                    If ConfiguracaoBrasil Then
        '                        PercPL = Format(Val(rc1!perc_pro_labore), "0.00")
        '                    Else
        '                        PercPL = TrocaValorAmePorBras(Format(Val(rc1!perc_pro_labore), "0.00"))
        '                    End If
        '                    vAdm.PercProLabore = PercPL
        '                    '
        '                    vEstip.Administradores.Add vAdm, CStr(rc1!adm_cliente_id)
        '                    '
        '                    rc1.MoveNext
        '                Wend
        '                '
        '                rc1.Close
        '                Set rc1 = Nothing
        '            End If
        '            '
        '            SubGrupos(CStr(rc!sub_grupo_id)).Estipulantes.Add vEstip, CStr(rc!est_cliente_id)
        '            '
        '            rc.MoveNext
        '        Wend
        '        '
        '       rc.Close
        '     Set rc = Nothing
        '    End If


        '*************busca o grupo
        SQL = "SELECT representacao_sub_grupo_tb.sub_grupo_id" & vbNewLine
        SQL = SQL & " , est_cliente_id" & vbNewLine
        SQL = SQL & " , CLIENTE_TB.nome" & vbNewLine
        SQL = SQL & ", dt_inicio_representacao " & vbNewLine
        SQL = SQL & " into #GrupoSEGP0223" & vbNewLine
        SQL = SQL & "FROM representacao_sub_grupo_tb  with (nolock)                  " & vbNewLine
        SQL = SQL & " INNER JOIN      cliente_tb with  (nolock)                     " & vbNewLine
        SQL = SQL & " ON cliente_id = est_cliente_id                            " & vbNewLine
        SQL = SQL & " WHERE representacao_sub_grupo_tb.apolice_id           = " & vApoliceId & vbNewLine
        SQL = SQL & " AND representacao_sub_grupo_tb.sucursal_seguradora_id = " & vSucursalId & vbNewLine
        SQL = SQL & " AND representacao_sub_grupo_tb.seguradora_cod_susep   = " & vSeguradoraId & vbNewLine
        SQL = SQL & " AND representacao_sub_grupo_tb.ramo_id                = " & vRamoId & vbNewLine
        SQL = SQL & " AND ( dt_fim_representacao                            is null " & vbNewLine
        SQL = SQL & "     OR dt_fim_representacao                           >= '" & dt_inicio_query & "')" & vbNewLine

        '************busca o administrador
        SQL = SQL & " SELECT adm_sub_grupo_apolice_tb.sub_grupo_id" & vbNewLine
        SQL = SQL & " , adm_cliente_id" & vbNewLine
        SQL = SQL & " , #GrupoSEGP0223.est_cliente_id" & vbNewLine
        SQL = SQL & " , cliente_tb.nome as nome_admin" & vbNewLine
        SQL = SQL & " , #GrupoSEGP0223.nome as nome_grupo" & vbNewLine
        SQL = SQL & " , perc_pro_labore" & vbNewLine
        SQL = SQL & " , dt_inicio_administracao " & vbNewLine
        SQL = SQL & "FROM adm_sub_grupo_apolice_tb with (nolock)  " & vbNewLine
        SQL = SQL & " INNER JOIN cliente_tb with(nolock)" & vbNewLine
        SQL = SQL & "         ON cliente_tb.cliente_id = adm_cliente_id " & vbNewLine
        SQL = SQL & " INNER JOIN  #GrupoSEGP0223 with(nolock)" & vbNewLine
        SQL = SQL & " on  adm_sub_grupo_apolice_tb.sub_grupo_id = #GrupoSEGP0223.sub_grupo_id" & vbNewLine
        SQL = SQL & " AND adm_sub_grupo_apolice_tb.est_cliente_id = #GrupoSEGP0223.est_cliente_id" & vbNewLine
        SQL = SQL & "WHERE adm_sub_grupo_apolice_tb.apolice_id = " & vApoliceId & vbNewLine
        SQL = SQL & " AND adm_sub_grupo_apolice_tb.sucursal_seguradora_id = " & vSucursalId & vbNewLine
        SQL = SQL & " AND adm_sub_grupo_apolice_tb.seguradora_cod_susep = " & vSeguradoraId & vbNewLine
        SQL = SQL & " AND adm_sub_grupo_apolice_tb.ramo_id = " & vRamoId & vbNewLine
        SQL = SQL & " AND dt_fim_administracao IS NULL " & vbNewLine

        SQL = SQL & " DROP TABLE  #GrupoSEGP0223" & vbNewLine

        'Set rc = rdoCn1.OpenResultset(SQL)

        Set rc = rdocn.OpenResultset(SQL)



        If Not rc.EOF Then



            While Not rc.EOF
                'seleciona o estipulante
                Set vEstip = New estipulante
                vEstip.Id = rc!est_cliente_id
                vEstip.Nome = Trim(rc!nome_grupo)



                ' seleciona o adiministrado
                Set vEstip.Administradores = New Collection
                Set vAdm = New Administrador
                vAdm.Id = rc!adm_cliente_id
                vAdm.Nome = Trim(rc!nome_admin)
                If ConfiguracaoBrasil Then
                    PercPL = Format(Val(rc!perc_pro_labore), "0.00")
                Else
                    PercPL = TrocaValorAmePorBras(Format(Val(rc!perc_pro_labore), "0.00"))
                End If

                vAdm.PercProLabore = PercPL
                '

                vEstip.Administradores.Add vAdm, CStr(rc!adm_cliente_id)

                'subgrupo do stipulante
                SubGrupos(CStr(rc!sub_grupo_id)).Estipulantes.Add vEstip, CStr(rc!est_cliente_id)
                rc.MoveNext



            Wend
            '
            rc.Close
            Set rc = Nothing
        End If
        '
        '            SubGrupos(CStr(rc!sub_grupo_id)).Estipulantes.Add vEstip, CStr(rc!est_cliente_id)
        '            '
        '            rc.MoveNext
        '        Wend
        '        '
        '        rc.Close
        '        Set rc = Nothing
    End If
    ''Fim   'alterado 12/09/2013 projeto 17860335 - Melhorias no SEGBR MC_Amaral -confitec
    'Mfreitas - Nova Consultoria - Demanda 696613 - Seguro BB Prestamista CDC Exercito
    'Quando o produto for 1183 - Seguro BB Prestamista CDC Exercito, deve exibir a observa��o abaixo
    If vProdutoId = 1183 Then
        GridAdm.ForeColor = vbRed
        lblObsEstipulante.Visible = True
        lblObsEstipulante.Caption = "Obs.: O Pr�-labore ser� pago ao Subestipulante da" & vbCrLf & _
                                    "Ap�lice que � o Centro de Pagamento do Ex�rcito -" & vbCrLf & _
                                    "CNPJ: 00.394.452/0533-04."
    Else
        GridAdm.ForeColor = vbBlack
        lblObsEstipulante.Visible = False
    End If

    Exit Sub
Erro:
    TrataErroGeral "Ler Estipulantes"
    MsgBox "Rotina: Ler Administra��o"
    'End

End Sub

Sub Ler_SubGrupos()
    Dim rc As rdoResultset
    Dim SQL As String, auxsub As String
    Dim Sql_Aux As String
    Dim NovoSubGrupo As SubGrupo
    Dim i As Integer


    On Error GoTo Erro_SubGrupo

    Set rc = Nothing

    'lpinto
    If Not bPropostaRecusada And Not bPropostaNaoEmitida And Not bPropostaEstudo Then    'rmaiellaro (19/01/2007)
        'Alterado por Genesco Silva - Confitec SP em 29/06/2011 - Flow 4532893 - inclus�o do campo cpf_cnpj na query
        'jessica.adao - 20120614 - Confitec - INC000003565105

        SQL = "SELECT DISTINCT sub_grupo_apolice_tb.sub_grupo_id, sub_grupo_apolice_tb.nome" & vbNewLine
        SQL = SQL & ", cpf_cnpj = coalesce(coalesce(jur.cgc,fis.cpf),cli.cpf_cnpj)" & vbNewLine    'Flow 19711975 - 08/02/2017 - Confitec: Claudia.Silva
        SQL = SQL & ", tp_is, isnull(qtd_salarios, 0) qtd_salarios" & vbNewLine

        SQL = SQL & ", isnull(tp_faturamento,'') tp_faturamento " & vbNewLine
        SQL = SQL & ", isnull(tp_custeio,'') tp_custeio " & vbNewLine
        SQL = SQL & ", isnull(tp_vida,'') tp_vida " & vbNewLine
        SQL = SQL & ", isnull(texto_beneficiario,'') txt_benef " & vbNewLine
        SQL = SQL & ", isnull(qtd_vida, 0) qtd_vida " & vbNewLine
        SQL = SQL & ", isnull( sub_grupo_apolice_tb.tabua_calculo_id, 0) tabua_calculo_id " & vbNewLine
        SQL = SQL & ", isnull(t.nome, '') nome_tabua " & vbNewLine
        SQL = SQL & ", isnull(fatura_automatica, 'n') fatura_automatica " & vbNewLine
        SQL = SQL & ", isnull( sub_grupo_apolice_tb.dia_faturamento, 0) dia_faturamento " & vbNewLine
        SQL = SQL & ", isnull( sub_grupo_apolice_tb.dia_cobranca, 0) dia_cobranca " & vbNewLine
        SQL = SQL & ", isnull( sub_grupo_apolice_tb.periodo_pgto_id, 99) periodo_pgto_id " & vbNewLine
        SQL = SQL & ", isnull(pp.nome, 'INDEFINIDO') nome_periodo " & vbNewLine
        SQL = SQL & ", isnull( sub_grupo_apolice_tb.forma_pgto_id, 99) forma_pgto_id " & vbNewLine
        SQL = SQL & ", isnull(fp.nome, 'INDEFINIDO') nome_forma " & vbNewLine
        SQL = SQL & ", isnull( sub_grupo_apolice_tb.banco_id, 0) banco_id " & vbNewLine
        SQL = SQL & ", isnull( sub_grupo_apolice_tb.agencia_id, 0) agencia_id " & vbNewLine
        SQL = SQL & ", isnull(ag.nome, '') nome_agencia " & vbNewLine
        SQL = SQL & ", isnull( sub_grupo_apolice_tb.conta_corrente_id, 0) conta_corrente_id " & vbNewLine
        SQL = SQL & ", isnull( sub_grupo_apolice_tb.tp_clausula_conjuge, 'n') tp_clausula_conjuge " & vbNewLine
        SQL = SQL & ", isnull( sub_grupo_apolice_tb.isento_iof, 'n') isento_iof " & vbNewLine
        SQL = SQL & ", isnull(li.lim_min_inclusao, 0) lim_min_inc " & vbNewLine
        SQL = SQL & ", isnull(li.lim_max_inclusao, 0) lim_max_inc " & vbNewLine
        SQL = SQL & ", isnull(li.lim_min_movimentacao, 0) lim_min_mov " & vbNewLine
        SQL = SQL & ", isnull(li.lim_max_movimentacao, 0) lim_max_mov " & vbNewLine
        SQL = SQL & ", isnull( sub_grupo_apolice_tb.cartao_proposta, '') cartao_proposta " & vbNewLine
        SQL = SQL & ", isnull(ccp.critica_todos, '') critica_todos " & vbNewLine
        SQL = SQL & ", isnull(ccp.critica_idade, '') critica_idade " & vbNewLine
        SQL = SQL & ", isnull(ccp.critica_capital, '') critica_capital " & vbNewLine
        SQL = SQL & ", isnull(ccp.lim_max_idade, 0) lim_max_idade " & vbNewLine
        SQL = SQL & ", isnull(ccp.lim_max_capital, 0) lim_max_capital " & vbNewLine
        SQL = SQL & ", isnull( sub_grupo_apolice_tb.dt_inicio_sub_grupo, a.dt_inicio_vigencia ) dt_inicio_sub " & vbNewLine
        SQL = SQL & ", isnull( sub_grupo_apolice_tb.atualizacao_idade, 'N' ) Reenquadramento " & vbNewLine
        SQL = SQL & ", sub_grupo_apolice_tb.dt_inicio_vigencia_sbg " & vbNewLine
        SQL = SQL & ", sub_grupo_apolice_tb.dt_fim_vigencia_sbg, web.situacao " & vbNewLine    'pmelo - inclus�o da coluna situa��o
        SQL = SQL & " FROM sub_grupo_apolice_tb    WITH (NOLOCK) " & vbNewLine
        SQL = SQL & " INNER JOIN apolice_tb a  WITH (NOLOCK) " & vbNewLine
        SQL = SQL & "   ON  a.seguradora_cod_susep = sub_grupo_apolice_tb.seguradora_cod_susep " & vbNewLine
        SQL = SQL & "   AND a.sucursal_seguradora_id = sub_grupo_apolice_tb.sucursal_seguradora_id " & vbNewLine
        SQL = SQL & "   AND a.ramo_id = sub_grupo_apolice_tb.ramo_id " & vbNewLine
        SQL = SQL & "   AND a.apolice_id = sub_grupo_apolice_tb.apolice_id " & vbNewLine
        SQL = SQL & " LEFT JOIN representacao_sub_grupo_tb rsgtb  WITH (NOLOCK) " & vbNewLine    'Genesco Silva Confitec SP Flow 4532893
        SQL = SQL & "   ON sub_grupo_apolice_tb.apolice_id = rsgtb.apolice_id " & vbNewLine
        SQL = SQL & "   AND sub_grupo_apolice_tb.ramo_id = rsgtb.ramo_id " & vbNewLine
        SQL = SQL & "   AND sub_grupo_apolice_tb.sub_grupo_id = rsgtb.sub_grupo_id " & vbNewLine
        'Gabriel C�mara - 05/01/2012 - Confitec RJ - INC000003153960
        'jessica.adao - 05/10/2011 - Confitec RJ - FLOW 12598012 - ERRO AO ABRIR AP�LICE
        SQL = SQL & " AND '" & dt_inicio_query & "' BETWEEN rsgtb.dt_inicio_representacao AND ISNULL(rsgtb.dt_fim_representacao,'20780101') " & vbNewLine    'Fim Genesco Silva

        'IM00127438 - LEANDRO AMARAL
        SQL = SQL & " LEFT JOIN SEGUROS_DB.DBO.ADM_SUB_GRUPO_APOLICE_TB AS ASGA WITH (NOLOCK) ON (sub_grupo_apolice_tb.apolice_id = ASGA.apolice_id "
        SQL = SQL & " AND sub_grupo_apolice_tb.ramo_id = ASGA.ramo_id "
        SQL = SQL & " AND sub_grupo_apolice_tb.sub_grupo_id = ASGA.sub_grupo_id "
        SQL = SQL & " AND ASGA.DT_FIM_ADMINISTRACAO IS NULL) "
        'IM00127438 - LEANDRO AMARAL

        'Flow 19711975 - 08/02/2017 - Confitec: Claudia.Silva
        SQL = SQL & " LEFT JOIN cliente_tb cli  WITH (NOLOCK) " & vbNewLine
        'IM00127438 - LEANDRO AMARAL
        'SQL = SQL & "   ON cli.cliente_id = coalesce(rsgtb.est_cliente_id, " & vClienteId & ") " & vbNewLine
        SQL = SQL & "   ON cli.cliente_id = coalesce(ASGA.est_cliente_id, rsgtb.est_cliente_id, " & vClienteId & ") " & vbNewLine
        'IM00127438 - LEANDRO AMARAL

        SQL = SQL & " LEFT JOIN pessoa_juridica_tb jur  WITH (NOLOCK) " & vbNewLine
        SQL = SQL & "   ON cli.cliente_id = jur.pj_cliente_id " & vbNewLine
        SQL = SQL & " LEFT JOIN pessoa_fisica_tb fis  WITH (NOLOCK) " & vbNewLine
        SQL = SQL & "   ON cli.cliente_id = fis.pf_cliente_id " & vbNewLine
        'Flow 19711975 - 08/02/2017 - Confitec: Claudia.Silva

        SQL = SQL & " LEFT JOIN tabua_calculo_tb t  WITH (NOLOCK) " & vbNewLine
        SQL = SQL & "   ON t.tabua_calculo_id = sub_grupo_apolice_tb.tabua_calculo_id " & vbNewLine
        SQL = SQL & " LEFT JOIN periodo_pgto_tb pp  WITH (NOLOCK) " & vbNewLine
        SQL = SQL & "   ON  pp.periodo_pgto_id = sub_grupo_apolice_tb.periodo_pgto_id " & vbNewLine
        SQL = SQL & " LEFT JOIN forma_pgto_tb fp  WITH (NOLOCK) " & vbNewLine
        SQL = SQL & "   ON  fp.forma_pgto_id = sub_grupo_apolice_tb.forma_pgto_id " & vbNewLine
        SQL = SQL & " LEFT JOIN agencia_tb ag  WITH (NOLOCK) " & vbNewLine
        SQL = SQL & "   ON  ag.banco_id = sub_grupo_apolice_tb.banco_id " & vbNewLine
        SQL = SQL & "   AND ag.agencia_id = sub_grupo_apolice_tb.agencia_id " & vbNewLine
        SQL = SQL & " LEFT JOIN limite_idade_sub_grupo_apolice_tb li  WITH (NOLOCK) " & vbNewLine
        SQL = SQL & "   ON  li.seguradora_cod_susep = sub_grupo_apolice_tb.seguradora_cod_susep " & vbNewLine
        SQL = SQL & "   AND li.sucursal_seguradora_id = sub_grupo_apolice_tb.sucursal_seguradora_id " & vbNewLine
        SQL = SQL & "   AND li.ramo_id = sub_grupo_apolice_tb.ramo_id " & vbNewLine
        SQL = SQL & "   AND li.apolice_id = sub_grupo_apolice_tb.apolice_id " & vbNewLine
        SQL = SQL & "   AND li.sub_grupo_id = sub_grupo_apolice_tb.sub_grupo_id " & vbNewLine
        SQL = SQL & "   AND li.dt_inicio_vigencia_sbg = sub_grupo_apolice_tb.dt_inicio_vigencia_sbg " & vbNewLine
        SQL = SQL & "   AND li.dt_inicio_limite <= '" & dt_inicio_query & "'" & vbNewLine
        'SQL = SQL & "   AND (li.dt_fim_limite is null OR li.dt_fim_limite > '" & dt_inicio_query & "')" & vbNewLine
        SQL = SQL & "   AND li.dt_fim_limite is null " & vbNewLine
        SQL = SQL & " LEFT JOIN sub_grupo_criterio_cartao_proposta_tb ccp " & vbNewLine
        SQL = SQL & "   ON sub_grupo_apolice_tb.seguradora_cod_susep = ccp.seguradora_cod_susep " & vbNewLine
        SQL = SQL & "   AND sub_grupo_apolice_tb.sucursal_seguradora_id = ccp.sucursal_seguradora_id " & vbNewLine
        SQL = SQL & "   AND sub_grupo_apolice_tb.ramo_id = ccp.ramo_id " & vbNewLine
        SQL = SQL & "   AND sub_grupo_apolice_tb.apolice_id = ccp.apolice_id " & vbNewLine
        SQL = SQL & "   AND sub_grupo_apolice_tb.sub_grupo_id = ccp.sub_grupo_id " & vbNewLine
        SQL = SQL & "   AND sub_grupo_apolice_tb.dt_inicio_vigencia_sbg = ccp.dt_inicio_vigencia_sbg " & vbNewLine
        SQL = SQL & "   AND ccp.dt_inicio_criterio = '" & dt_inicio_query & "'" & vbNewLine
        SQL = SQL & " LEFT JOIN web_intranet_db..apolice_subgrupo_vida_web_tb web " & vbNewLine
        SQL = SQL & "   ON sub_grupo_apolice_tb.apolice_id = web.apolice_id " & vbNewLine
        SQL = SQL & "   AND sub_grupo_apolice_tb.sub_grupo_id = web.subgrupo_id " & vbNewLine
        SQL = SQL & "   AND sub_grupo_apolice_tb.ramo_id  = web.ramo_id " & vbNewLine
        SQL = SQL & " WHERE sub_grupo_apolice_tb.apolice_id              = " & vApoliceId & vbNewLine
        SQL = SQL & " AND sub_grupo_apolice_tb.sucursal_seguradora_id    = " & vSucursalId & vbNewLine
        SQL = SQL & " AND sub_grupo_apolice_tb.seguradora_cod_susep      = " & vSeguradoraId & vbNewLine
        SQL = SQL & " AND sub_grupo_apolice_tb.ramo_id                   = " & vRamoId & vbNewLine
        SQL = SQL & " ORDER BY sub_grupo_apolice_tb.sub_grupo_id"

        Set rc = rdocn.OpenResultset(SQL)

        If Not rc.EOF Then
            '' Habilita o Tab de SubGrupos
            ' Deixa a coluna tabua_id invisivel
            HaSubGrupos = True
            HaCertificados = True
            GridSubGrupos.ColWidth(5) = 0
            '' e deixa os controles de subgrupo visiveis
            SSTabProp.TabEnabled(TAB_SUBGRUPO) = True
            'If glAmbiente_id = 3 Then
            SSTabProp.TabEnabled(TAB_CERTIFICADOS) = True
            'Else
            'SSTabProp.TabEnabled(TAB_CERTIFICADOS) = False
            'End If

            '' Tab Corretagem
            FraSubGrupo(0).Visible = True
            '        FraCorretagem.Top = 2280

            cboSubGrupo(0).Clear
            FraSubGrupo(0).Refresh
            FraCorretagem.Refresh

            '' Tab Parametros
            FraSubGrupo(1).Visible = True
            FraParametros.Top = 3060
            FraParametros.Height = 3765
            '
            cboSubGrupo(1).Clear
            FraSubGrupo(1).Refresh
            FraParametros.Refresh

            '' Tab Coberturas
            FraSubGrupo(2).Visible = True
            FraSubGrupo(2).Top = 1260
            FraCoberturas.Top = 2190
            FraCoberturas.Height = 2505
            GridCoberturas.Height = 1665
            '
            cboSubGrupo(2).Clear
            FraSubGrupo(2).Refresh
            FraCoberturas.Refresh

            '' Tab Administra��o
            FraSubGrupo(3).Visible = True
            '        FraEstipulante.Top = 2310
            '        FraAdministradores.Top = 3180
            FraAdministradores.Height = 3645
            '
            cboSubGrupo(3).Clear
            FraSubGrupo(3).Refresh
            FraEstipulante.Refresh
            FraAdministradores.Refresh

            'If vProdutoId <> 1183 Then ' Guilherme Cruz -- Confitec SISTEMAS -- Demanda Sala �gil -- inicio

            '' Tab Cobran�a
            FraSubGrupo(4).Visible = True
            '        FraCobranca.Top = 2330
            FraCobranca.Height = 4695
            '
            cboSubGrupo(4).Clear
            FraSubGrupo(4).Refresh
            FraCobranca.Refresh
            'End If

'            If vProdutoId = 1183 Then ' Guilherme Cruz -- Confitec SISTEMAS -- Demanda Sala �gil -- inicio
'
'                '' Tab Cobran�a
'                FraSubGrupo(4).Visible = True
'                '        FraCobranca.Top = 2330
'                FraCobranca.Height = 4695
'                '
'                cboSubGrupo(4).Clear
'                FraSubGrupo(4).Refresh
'                FraCobranca.Refresh
'            End If
            
            '
            '' Tab Cosseguro Cedido
            'FraSubGrupo(5).Visible = True

            'FraCosseguroCedido.Top = 1320
            'FraCosseguroCedido.Height = 4455
            '
            'cboSubGrupo(5).Clear
            'FraSubGrupo(5).Refresh
            FraCosseguroCedido.Refresh
            '
            '' Tab Agenciamento
            FraSubGrupo(6).Visible = True

            FraAgenciamento.Top = 2280
            '
            cboSubGrupo(6).Clear
            FraSubGrupo(6).Refresh
            FraAgenciamento.Refresh
            '
            cboSubGrupo(7).Clear
            cboSubGrupo(7).Enabled = True
            SSTabProp.TabEnabled(TAB_ASSISTENCIA) = True

            cboSubGrupo(8).Clear
            cboSubGrupo(8).Enabled = True
            SSTabProp.TabEnabled(TAB_VIDAS_SEGURADAS) = True

            While Not rc.EOF
                Set NovoSubGrupo = New SubGrupo
                '
                With NovoSubGrupo
                    Set .Corretores = New Collection
                    Set .Coberturas = New Collection
                    Set .Componentes = New Collection
                    Set .Estipulantes = New Collection
                    Set .Faturas = New Collection
                    Set .Assistencias = New Collection
                    Set .Vidas_Seguradas = New Collection

                    .Id = Format(rc!sub_grupo_id, "000")
                    .Nome = Trim(rc!Nome)
                    'GENESCO SILVA - CONFITEC SP
                    'Inclus�o de CPF/CNPJ no GridSubGrupo
                    If Not IsNull(rc!CPF_CNPJ) Then
                        If Len(rc!CPF_CNPJ) > 11 Then    'Flow 19711975 - 08/02/2017 - Confitec: Claudia.Silva
                            .Cnpj = Format$(rc!CPF_CNPJ, "&&.&&&.&&&/&&&&-&&")
                        Else
                            .Cnpj = Format$(rc!CPF_CNPJ, "&&&.&&&.&&&-&&")
                        End If
                    Else
                        .Cnpj = "N�o Informado"
                    End If
                    'MYOSHIMURA 11/12/2004
                    'TRATAMENTO DA N�O INFORMA��O DO TIPO DE IS
                    '.TipoIS = UCase(Trim(rc!tp_is))
                    .TipoIS = IIf(IsNull(UCase(Trim(rc!tp_is))), "", UCase(Trim(rc!tp_is)))
                    .QtdeSal = rc!qtd_salarios
                    .TextoBenef = Trim(rc!txt_benef)
                    .QtdVidas = rc!qtd_vida
                    .IdTabua = rc!tabua_calculo_id
                    .NomeTabua = Trim(rc!nome_tabua)
                    .FaturaAuto = (UCase(rc!fatura_automatica) = "S")
                    .DiaFaturamento = Val(rc!dia_faturamento)
                    .DiaCobranca = Val(rc!dia_cobranca)
                    .IndPerPgto = rc!periodo_pgto_id
                    .PeriodoPgto = Trim(rc!nome_periodo)
                    .IndFormaPgto = rc!forma_pgto_id
                    .FormaPgto = Trim(rc!nome_forma)
                    .IdBanco = rc!banco_id
                    .IdAgencia = rc!agencia_id
                    .NomeAgencia = Trim(UCase(rc!nome_agencia))
                    .CCorrente = rc!conta_corrente_id
                    .TpConjuge = UCase(rc!tp_clausula_conjuge)
                    .IsentoIOF = (UCase(rc!isento_iof) = "S")
                    .LimMinIdadeImpl = Val(rc!lim_min_inc)
                    .LimMaxIdadeImpl = Val(rc!lim_max_inc)
                    .LimMinIdadeMov = Val(rc!lim_min_mov)
                    .LimMaxIdadeMov = Val(rc!lim_max_mov)
                    .cartao_proposta = Trim(rc!cartao_proposta)
                    .critica_todos = rc!critica_todos
                    .critica_idade = rc!critica_idade
                    .critica_capital = rc!critica_capital
                    .lim_max_idade = rc!lim_max_idade
                    .lim_max_capital = Val(rc!lim_max_capital)
                    .tp_faturamento = Trim(UCase(rc!tp_faturamento))
                    .tp_custeio = Trim(UCase(rc!tp_custeio))
                    .tp_vida = Trim(UCase(rc!tp_vida))
                    .DtInicioSub = rc!dt_inicio_sub
                    .AtualizacaoIdade = rc!Reenquadramento
                    .DtInicioVigenciaSBG = rc!dt_inicio_vigencia_sbg
                    .DtFimVigenciaSBG = IIf(IsNull(rc!dt_fim_vigencia_sbg), "", Format(rc!dt_fim_vigencia_sbg, "dd/mm/yyyy"))
                    'pmelo - inclusao da situa��o da apolice_subgrupo_vida_web_tb
                    .Situacao = IIf(IsNull(rc!Situacao), "", rc!Situacao)

                End With

                ' Adiciona o SubGrupo � cole��o
                SubGrupos.Add NovoSubGrupo, CStr(rc!sub_grupo_id)
                ' Adiciona o SubGrupo aos Combos
                auxsub = Format(rc!sub_grupo_id, "000") & " - " & Trim(rc!Nome)
                '
                For i = 0 To 8
                    cboSubGrupo(i).AddItem auxsub
                    cboSubGrupo(i).ItemData(cboSubGrupo(i).NewIndex) = rc!sub_grupo_id
                Next
                '
                rc.MoveNext
            Wend
            Monta_GridSubGrupos
            Limpa_Campos_Subgrupo
            '
        Else
            HaSubGrupos = False
            HaCertificados = False
            '' Desabilita o Tab de SubGrupos
            '' e deixa os controles de subgrupo invisiveis
            SSTabProp.TabEnabled(TAB_SUBGRUPO) = False
            'SSTabProp.TabEnabled(TAB_CERTIFICADOS) = False
            '
            '' Tab Corretagem

            FraCorretagem.Top = FraSubGrupo(0).Top
            'FraCorretagem.Top = 1110
            FraSubGrupo(0).Visible = False

            'FraCorretagem.Top = 1160
            '
            cboSubGrupo(0).Clear
            FraCorretagem.Refresh

            '' Tab Parametros
            FraParametros.Top = FraSubGrupo(1).Top
            FraSubGrupo(1).Visible = False
            FraParametros.Height = 4995
            '
            cboSubGrupo(1).Clear
            FraParametros.Refresh

            '' Tab Coberturas

            FraCoberturas.Top = FraSubGrupo(2).Top
            FraSubGrupo(2).Visible = False
            FraCoberturas.Height = 3285
            GridCoberturas.Height = 2475
            '
            cboSubGrupo(2).Clear
            FraCoberturas.Refresh

            '' Tab Administra��o

            FraAdministradores.Top = FraEstipulante.Top
            FraEstipulante.Top = FraSubGrupo(3).Top
            FraSubGrupo(3).Visible = False
            FraAdministradores.Height = 4785
            '
            cboSubGrupo(3).Clear
            FraEstipulante.Refresh
            FraAdministradores.Refresh

            '' Tab Cobran�a
            'If vProdutoId <> 1183 Then ' Guilherme Cruz -- Confitec SISTEMAS -- Demanda Sala �gil -- inicio
            FraCobranca.Top = FraSubGrupo(4).Top
            FraSubGrupo(4).Visible = False
            FraCobranca.Height = 5895
            '
            cboSubGrupo(4).Clear
            FraCobranca.Refresh
            'End If
            
'            If vProdutoId = 1183 Then ' Guilherme Cruz -- Confitec SISTEMAS -- Demanda Sala �gil -- inicio
''                FraCobranca.Top = FraSubGrupo(4).Top
''                FraSubGrupo(4).Visible = True
''                FraCobranca.Height = 5895
'                '
''                CboSubGrupo(4).Clear
''                FraCobranca.Refresh
'            End If

            '' Tab Cosseguro Cedido
            'FraCosseguroCedido.Top = 240
            'FraCosseguroCedido.Height = 5460
            '
            cboSubGrupo(5).Clear
            FraCosseguroCedido.Refresh
            '
            '' Tab Agenciamento
            FraAgenciamento.Top = FraSubGrupo(6).Top
            FraSubGrupo(6).Visible = False
            '
            cboSubGrupo(6).Clear
            FraAgenciamento.Refresh

            cboSubGrupo(7).Clear
            cboSubGrupo(7).Enabled = False
            SSTabProp.TabEnabled(TAB_ASSISTENCIA) = False

            cboSubGrupo(8).Clear
            cboSubGrupo(8).Enabled = False
            SSTabProp.TabEnabled(TAB_VIDAS_SEGURADAS) = False

        End If

        rc.Close
        Set rc = Nothing

    Else
        HaSubGrupos = False
        HaCertificados = False
        '' Desabilita o Tab de SubGrupos
        '' e deixa os controles de subgrupo invisiveis
        SSTabProp.TabEnabled(TAB_SUBGRUPO) = False
        'lpinto 28/09/2005
        SSTabProp.TabEnabled(TAB_CERTIFICADOS) = False
        '
        '' Tab Corretagem

        FraCorretagem.Top = FraSubGrupo(0).Top
        'FraCorretagem.Top = 1110
        FraSubGrupo(0).Visible = False

        'FraCorretagem.Top = 1160
        '
        cboSubGrupo(0).Clear
        FraCorretagem.Refresh

        '' Tab Parametros
        FraParametros.Top = FraSubGrupo(1).Top
        FraSubGrupo(1).Visible = False
        FraParametros.Height = 4995
        '
        cboSubGrupo(1).Clear
        FraParametros.Refresh

        '' Tab Coberturas

        FraCoberturas.Top = FraSubGrupo(2).Top
        FraSubGrupo(2).Visible = False
        FraCoberturas.Height = 3285
        GridCoberturas.Height = 2475
        '
        cboSubGrupo(2).Clear
        FraCoberturas.Refresh

        '' Tab Administra��o

        FraAdministradores.Top = FraEstipulante.Top
        FraEstipulante.Top = FraSubGrupo(3).Top
        FraSubGrupo(3).Visible = False
        FraAdministradores.Height = 4785
        '
        cboSubGrupo(3).Clear
        FraEstipulante.Refresh
        FraAdministradores.Refresh

        '' Tab Cobran�a

        FraCobranca.Top = FraSubGrupo(4).Top
        FraSubGrupo(4).Visible = False
        FraCobranca.Height = 5895
        '
        cboSubGrupo(4).Clear
        FraCobranca.Refresh

        '' Tab Cosseguro Cedido
        'FraCosseguroCedido.Top = 240
        'FraCosseguroCedido.Height = 5460
        '
        cboSubGrupo(5).Clear
        FraCosseguroCedido.Refresh
        '
        '' Tab Agenciamento
        FraAgenciamento.Top = FraSubGrupo(6).Top
        FraSubGrupo(6).Visible = False
        '
        cboSubGrupo(6).Clear
        FraAgenciamento.Refresh

        cboSubGrupo(7).Clear
        cboSubGrupo(7).Enabled = False
        SSTabProp.TabEnabled(TAB_ASSISTENCIA) = False

        cboSubGrupo(8).Clear
        cboSubGrupo(8).Enabled = False
        SSTabProp.TabEnabled(TAB_VIDAS_SEGURADAS) = False

    End If


    Exit Sub
    Resume
Erro_SubGrupo:
    mensagem_erro 6, "Erro lendo subgrupos da ap�lice!"
    MsgBox "Rotina : Ler_SubGrupos"

End Sub

Sub Monta_GridCoberturas(vObjeto As Variant)

    Dim rs As rdoResultset
    Dim vCobertura As Cobertura
    Dim linha As String
    Dim SQL As String
    Dim i As Integer, j As Integer
    Dim PercCorretagem As Double
    Dim TaxaNet As Double
    Dim vSubGrupo As SubGrupo
    Dim Corretagem As New Collection
    Dim Adm As New Collection
    Dim vCorretor As Corretor
    Dim vEstip As estipulante
    Dim vAdm As Administrador

    GridCoberturas.Rows = 1
    PercCorretagem = 0
    '
    If SubGrupos.Count > 0 Then
        For Each vSubGrupo In SubGrupos
            If vSubGrupo.Id = cboSubGrupo(2).ItemData(cboSubGrupo(2).ListIndex) Then
                Set Corretagem = vSubGrupo.Corretores
                Set Adm = vSubGrupo.Estipulantes
            End If
        Next
    Else
        Set Corretagem = Corretores
        Set Adm = Estipulantes
    End If
    '
    For Each vCorretor In Corretagem
        PercCorretagem = PercCorretagem + Val(vCorretor.PercCorretagem)
    Next
    '
    If Not (Adm Is Nothing) Then
        For Each vEstip In Adm
            If Not (vEstip.Administradores Is Nothing) Then
                For Each vAdm In vEstip.Administradores
                    PercCorretagem = PercCorretagem + Val(vAdm.PercProLabore)
                Next
            End If
        Next
    End If
    '
    For Each vCobertura In Coberturas
        If vCobertura.CodObjSegurado = vObjeto Then
            linha = vCobertura.TpCoberturaId
            linha = linha & vbTab & vCobertura.Descricao
            linha = linha & vbTab & vCobertura.ValIS
            linha = linha & vbTab & vCobertura.PercBasica
            If ConfiguracaoBrasil Then
                If vCobertura.ValTaxa <> "" Then
                    TaxaNet = Val(vCobertura.ValTaxa) * (100 - PercCorretagem)
                Else
                    TaxaNet = 0
                End If
            Else
                If vCobertura.ValTaxa <> "" Then
                    TaxaNet = Val(MudaVirgulaParaPonto(vCobertura.ValTaxa)) * (100 - PercCorretagem)
                Else
                    TaxaNet = 0
                End If
            End If
            '
            linha = linha & vbTab & Format(TaxaNet, "##0.0000000")
            linha = linha & vbTab & Format(vCobertura.ValTaxa, "0.0000000")
            linha = linha & vbTab & vCobertura.ValPremio
            linha = linha & vbTab & vCobertura.ValPremioLiq
            linha = linha & vbTab & vCobertura.ValPremioTarifa
            linha = linha & vbTab & vCobertura.PercDesconto
            linha = linha & vbTab & vCobertura.DtInicioVigencia
            linha = linha & vbTab & vCobertura.DtFimVigencia
            linha = linha & vbTab & vCobertura.ValFranquia
            linha = linha & vbTab & vCobertura.PercFranquia
            linha = linha & vbTab & vCobertura.DescFranquia
            linha = linha & vbTab & Format(vCobertura.LimMinIS, "#,###,###,###,##0.00")
            linha = linha & vbTab & Format(vCobertura.LimMaxIS, "#,###,###,###,##0.00")
            linha = linha & vbTab & vCobertura.Carencia
            linha = linha & vbTab & vCobertura.LimDiarias
            '
            GridCoberturas.AddItem linha
            GridCoberturas.Row = GridCoberturas.Rows - 1
        End If
    Next
    '' Fixa a linha da cobertura b�sica
    '
    GridCoberturas.Refresh

End Sub

Private Sub Ler_Resseguro()
    Dim rs As rdoResultset
    Dim SQL As String
    Dim cod_resseg As Integer, cod_objeto As Integer
    Dim Item_Negociacao As String, Item_Financeiro As String, Premio_Excedente As String
    Dim Comissao_Excedente As String, Premio_Quota As String, Comissao_Quota As String
    Dim Premio_ED As String, Premio_Catastrofe As String
    Dim linha As String

    On Error GoTo Erro

    'lpinto
    If Not bPropostaRecusada And Not bPropostaNaoEmitida And Not bPropostaEstudo Then    'rmaiellaro (19/01/2007)

        SQL = "SELECT c.re_seguro_item_id, r.cod_objeto_segurado, " _
            & "  c.cod_plano_re_seguro, c.cod_item_financeiro, " _
            & "  c.val_original, c.val_repassado " _
            & "FROM re_seguro_item_tb r   WITH (NOLOCK) , composicao_res_item_tb c  WITH (NOLOCK)  " _
            & "WHERE r.proposta_id = " & vPropostaId _
            & "  and r.re_seguro_item_id = c.re_seguro_item_id " _
            & "  and situacao <> 'P' " _
            & "  AND r.dt_inicio_vigencia_seg <= '" & dt_inicio_query & "' " _
            & "  AND c.dt_inicio_vigencia_comp <= '" & dt_inicio_query & "' " _
            & "ORDER by r.cod_objeto_segurado, " _
            & "      c.cod_plano_re_seguro, c.cod_item_financeiro "
        '
        Set rs = rdocn.OpenResultset(SQL)

        If Not rs.EOF Then
            SSTabProp.TabEnabled(TAB_RESSEGURO) = True
            GridResseguro.Rows = 1
            '
            While Not rs.EOF
                cod_resseg = rs!re_seguro_item_id
                cod_objeto = rs!cod_objeto_segurado

                Do While cod_resseg = rs!re_seguro_item_id
                    Item_Negociacao = rs!cod_plano_re_seguro
                    Item_Financeiro = rs!cod_item_financeiro

                    If Item_Financeiro = "PREMIO" Then Item_Financeiro = 1 Else Item_Financeiro = 2
                    'resseguro excedente de responsabilidade - pr�mio
                    If Item_Negociacao = 1 And Item_Financeiro = 1 Then
                        Premio_Excedente = Format(Val(0 & rs!val_repassado), "#,###,###,##0.00")
                    Else
                        Premio_Excedente = "0,00"
                    End If
                    'resseguro excedente de responsabilidade - comiss�o
                    If Item_Negociacao = 1 And Item_Financeiro = 2 Then
                        Comissao_Excedente = Format(Val(0 & rs!val_repassado), "#,###,###,##0.00")
                    Else
                        Comissao_Excedente = "0,00"
                    End If
                    'resseguro quota - pr�mio
                    If Item_Negociacao = 2 And Item_Financeiro = 1 Then
                        Premio_Quota = Format(Val(0 & rs!val_repassado), "#,###,###,##0.00")
                    Else
                        Premio_Quota = "0,00"
                    End If
                    'resseguro quota - comiss�o
                    If Item_Negociacao = 2 And Item_Financeiro = 2 Then
                        Comissao_Quota = Format(Val(0 & rs!val_repassado), "#,###,###,##0.00")
                    Else
                        Comissao_Quota = "0,00"
                    End If
                    'resseguro excesso de danos - pr�mio
                    If Item_Negociacao = 3 And Item_Financeiro = 1 Then
                        Premio_ED = Format(Val(0 & rs!val_repassado), "#,###,###,##0.00")
                    Else
                        Premio_ED = "0,00"
                    End If

                    'resseguro cat�strofe - pr�mio
                    If Item_Negociacao = 4 And Item_Financeiro = 1 Then
                        Premio_Catastrofe = Format(Val(0 & rs!val_repassado), "#,###,###,##0.00")
                    Else
                        Premio_Catastrofe = "0,00"
                    End If

                    cod_resseg = rs!re_seguro_item_id

                    rs.MoveNext
                    If rs.EOF Then
                        Exit Do
                    End If

                Loop

                linha = Format(cod_objeto, "0000")
                linha = linha & vbTab & Premio_Excedente
                linha = linha & vbTab & Comissao_Excedente
                linha = linha & vbTab & Premio_Quota
                linha = linha & vbTab & Comissao_Quota
                linha = linha & vbTab & Premio_ED
                linha = linha & vbTab & Premio_Catastrofe

                GridResseguro.AddItem linha
            Wend
        Else
            SSTabProp.TabEnabled(TAB_RESSEGURO) = False
        End If
        rs.Close
        Set rs = Nothing
    Else
        SSTabProp.TabEnabled(TAB_RESSEGURO) = False
    End If

    Exit Sub

Erro:
    TrataErroGeral "Ler Resseguro"
    MsgBox "Rotina: Ler Resseguro. Programa ser� Cancelado", vbCritical
    End
End Sub

''Ggama - 03/11/2006 - Flow: 131777
''Fun��o Ler_Beneficiario recriada abaixo
'Private Sub Ler_Beneficiario()
'
'Dim rc As rdoResultset
'Dim SQL As String
'Dim linha As String
'
'On Error GoTo Erro
'   SQL = "SELECT descricao " & _
    '          "FROM texto_beneficiario_tb  WITH (NOLOCK) " & _
    '          "WHERE proposta_id = " & vPropostaId
'
'    Set rc = rdocn.OpenResultset(SQL)
'
'    If Not rc.EOF Then
'        TxtBeneficiario.Text = Trim("" & rc!Descricao)
'    End If
'
'    rc.Close
'    Set rc = Nothing
'Exit Sub
'
'Erro:
'   TrataErroGeral "Ler Beneficiario"
'   MsgBox "Rotina: Ler Beneficiario"
'   'End
'
'End Sub

Private Sub Ler_Beneficiario()
    Dim rc As rdoResultset
    Dim SQL As String
    Dim linha As String

    On Error GoTo Erro

    TxtBeneficiario.Text = ""
    SQL = ""
    SQL = "SELECT cod_objeto_segurado, nome, cpf_cnpj                 "
    SQL = SQL & " From seguro_item_benef_tb    WITH(NOLOCK)          "
    SQL = SQL & "  WHERE dt_fim_vigencia_benef IS NULL          " & vbCrLf
    SQL = SQL & "    AND proposta_id            = " & vPropostaId
    Set rc = rdoCn1.OpenResultset(SQL, rdOpenStatic)
    If Not rc.EOF Then
        With GridBeneficiario
            .Rows = 1
            Do While Not rc.EOF
                linha = Format(rc("cod_objeto_segurado"), "###")
                linha = linha & vbTab & rc("nome")
                linha = linha & vbTab & rc("cpf_cnpj")
                .AddItem linha
                rc.MoveNext
            Loop
        End With
        Set rc = Nothing
    Else
        SQL = "SELECT descricao " & _
              "FROM texto_beneficiario_tb  WITH (NOLOCK) " & _
              "WHERE proposta_id = " & vPropostaId

        Set rc = rdocn.OpenResultset(SQL)

        If Not rc.EOF Then
            TxtBeneficiario.Text = Trim("" & rc!Descricao)
        End If

        rc.Close
        Set rc = Nothing
    End If
    With GridBeneficiario
        .Enabled = IIf(TxtBeneficiario.Text = "", True, False)
        .Visible = GridBeneficiario.Enabled
    End With
    With TxtBeneficiario
        .Enabled = Not GridBeneficiario.Enabled
        .Visible = Not GridBeneficiario.Enabled
    End With

    Exit Sub
Erro:
    TrataErroGeral "Ler Beneficiario"
    MsgBox "Rotina: Ler Beneficiario"
    'End
End Sub
''

Private Sub Ler_Cosseguro()
    Dim rc As rdoResultset
    Dim NovaCongenere As Congenere
    Dim vAuxParticipacao As Variant
    Dim vAuxPercDespLider As Variant
    Dim SQL As String

    On Error GoTo Erro

    If Not bPropostaRecusada And Not bPropostaNaoEmitida And Not bPropostaEstudo Then    'rmaiellaro (19/01/2007)
        cmbCongenere.Clear
        '

        SQL = "SELECT c.rep_seguradora_cod_susep, s.nome, "
        SQL = SQL & " c.num_ordem, c.perc_participacao, c.perc_despesa_lider "
        SQL = SQL & "FROM co_seguro_repassado_tb c  WITH (NOLOCK) , apolice_tb a  WITH (NOLOCK) , seguradora_tb s  WITH (NOLOCK)  "
        SQL = SQL & "WHERE a.proposta_id = " & vPropostaId
        SQL = SQL & " AND c.apolice_id = a.apolice_id "
        SQL = SQL & " AND c.sucursal_seguradora_id = a.sucursal_seguradora_id "
        SQL = SQL & " AND c.seguradora_cod_susep = a.seguradora_cod_susep "
        SQL = SQL & " AND c.ramo_id = a.ramo_id "
        SQL = SQL & " AND s.seguradora_cod_susep = c.rep_seguradora_cod_susep "
        SQL = SQL & " AND c.dt_inicio_participacao <= '" & dt_inicio_query & "' "
        SQL = SQL & " AND c.dt_fim_participacao IS NULL "

        Set rc = rdocn.OpenResultset(SQL)

        If rc.EOF Then
            Set rc = Nothing

            SQL = "SELECT c.rep_seguradora_cod_susep, s.nome, "
            SQL = SQL & " c.num_ordem, c.perc_participacao, c.perc_despesa_lider "
            SQL = SQL & "FROM co_seguro_repassado_tb c  WITH (NOLOCK) , apolice_tb a  WITH (NOLOCK) , seguradora_tb s  WITH (NOLOCK)  "
            SQL = SQL & "WHERE a.proposta_id = " & vPropostaId
            SQL = SQL & " AND c.apolice_id = a.apolice_id "
            SQL = SQL & " AND c.sucursal_seguradora_id = a.sucursal_seguradora_id "
            SQL = SQL & " AND c.seguradora_cod_susep = a.seguradora_cod_susep "
            SQL = SQL & " AND c.ramo_id = a.ramo_id "
            SQL = SQL & " AND s.seguradora_cod_susep = c.rep_seguradora_cod_susep "
            SQL = SQL & " AND c.dt_inicio_participacao <= '" & dt_inicio_query & "' "
            SQL = SQL & " AND (c.dt_fim_participacao IS NULL OR "
            SQL = SQL & "      c.dt_fim_participacao >= '" & dt_inicio_query & "') "

            Set rc = rdocn.OpenResultset(SQL)
        End If

        If Not rc.EOF Then
            'INICIO - Cibele Pereira - INC000004453034 - 02/12/2014
            vAceito = True
            TabSeguro = "seguro_vida_aceito_tb"
            TabEscolha = "escolha_tp_cob_vida_aceito_tb"
            SSTabProp.TabEnabled(TAB_COSSEGURO) = True
            '
            cmbCongenere.AddItem "Nossa Parte"
            cmbCongenere.AddItem "Total do Cosseguro Cedido"
            '
            While Not rc.EOF
                Set NovaCongenere = New Congenere

                NovaCongenere.Nome = Trim(rc!Nome)
                If IsNull(rc!Perc_Participacao) Then
                    NovaCongenere.Perc_Participacao = 0
                Else
                    vAuxParticipacao = Val(rc!Perc_Participacao)
                    NovaCongenere.Perc_Participacao = vAuxParticipacao
                End If
                '
                If IsNull(rc!Perc_Despesa_Lider) Then
                    NovaCongenere.Perc_Despesa_Lider = 0
                Else
                    vAuxPercDespLider = Val(rc!Perc_Despesa_Lider)
                    NovaCongenere.Perc_Despesa_Lider = vAuxPercDespLider
                End If
                '
                NovaCongenere.Seguradora = rc!rep_seguradora_cod_susep
                NovaCongenere.NumOrdem = Val("0" & rc!NUM_ORDEM)
                '
                cmbCongenere.AddItem Trim(rc!Nome)
                cmbCongenere.ItemData(cmbCongenere.NewIndex) = rc!rep_seguradora_cod_susep
                '
                Congeneres.Add NovaCongenere, rc!rep_seguradora_cod_susep
                rc.MoveNext
            Wend
        Else
            'FIM - Cibele Pereira - INC000004453034 - 02/12/2014
            vAceito = False
            TabSeguro = "seguro_vida_tb"
            TabEscolha = "escolha_tp_cob_vida_tb"
            SSTabProp.TabEnabled(TAB_COSSEGURO) = False
        End If
        rc.Close
        Set rc = Nothing

    Else
        SSTabProp.TabEnabled(TAB_COSSEGURO) = False
    End If

    '    txtPercDespesa.Text = vValZero & " %"
    Exit Sub
Erro:
    TrataErroGeral "Ler Cosseguro"
    MsgBox "Rotina: Ler Cosseguro"
End Sub

Sub Ler_Proposta_Basica_Vida()
    Dim rc As rdoResultset
    Dim SQL As String
    '
    On Error GoTo Erro_LPBV

    If SubGrupos.Count = 0 Then
        '' Obtem a tabua de calculo utilizada
        SQL = "SELECT pbv.tabua_calculo_id, tc.nome "
        'sql = sql & ", pbv.banco_cobrador, pbv.agencia_cobradora, pbv.cod_agencia_cobradora "
        SQL = SQL & "FROM proposta_basica_vida_tb pbv  WITH (NOLOCK)  , tabua_calculo_tb tc  WITH (NOLOCK) "
        SQL = SQL & "WHERE pbv.proposta_id = " & vPropostaId
        SQL = SQL & " AND tc.tabua_calculo_id = pbv.tabua_calculo_id"
        '
        Set rc = rdocn.OpenResultset(SQL)
        '
        If Not rc.EOF Then
            If Not IsNull(rc!tabua_calculo_id) Then
                TxtTbCalcId.Text = rc!tabua_calculo_id
            End If
            '
            If Not IsNull(rc!Nome) Then
                TxtTbCalculo.Text = Trim(rc!Nome)
            End If
            '
            rc.Close
        End If
        '
        Set rc = Nothing
        '' Obtem o no. de vidas
        SQL = "SELECT qtd_vida FROM proposta_qtd_vida_tb  WITH (NOLOCK) "
        SQL = SQL & "WHERE proposta_id = " & vPropostaId
        SQL = SQL & " AND dt_inicio_vigencia <= '" & dt_inicio_query & "'"
        SQL = SQL & " AND ( dt_fim_vigencia is null "
        SQL = SQL & "     OR dt_fim_vigencia >= '" & dt_inicio_query & "')"
        '
        Set rc = rdocn.OpenResultset(SQL)
        '
        If Not rc.EOF Then
            If Not IsNull(rc!qtd_vida) Then
                txtQtdVida.Text = rc!qtd_vida
            End If
            '
            rc.Close
        End If
        '
    End If
    '
    Set rc = Nothing
    '
    Exit Sub

Erro_LPBV:
    TrataErroGeral "Ler Proposta B�sica Vida"
    MsgBox "Rotina: Ler_Proposta_Basica_Vida"
    'End
End Sub

Private Sub Ler_Seguro_Vida()
    Dim rc As rdoResultset
    Dim SQL As String, linha As String
    Dim vComponente As Componente

    On Error GoTo Erro

    'lpinto
    If Not bPropostaRecusada And Not bPropostaNaoEmitida And Not bPropostaEstudo Then    'rmaiellaro (19/01/2007)
        Set rc = Nothing
        CboComponente.Clear
        cboObjSegurado.Clear
        GridComponente.Rows = 1
        '' Seleciona todos os componentes da ap�lice, independente de subgrupo
        '
        '' Monta o grid e combos de tipos de componente
        '        SQL = "SELECT distinct s.tp_componente_id, c.nome nomecomp, "
        '        SQL = SQL & "s.ramo_id, s.subramo_id, s.dt_inicio_vigencia_seg "
        '        SQL = SQL & ", sr.nome nomesr, s.dt_inicio_vigencia_sbr, produto_externo_id "
        '        SQL = SQL & " FROM " & TabSeguro & " s  WITH (NOLOCK) , tp_componente_tb c  WITH (NOLOCK) , subramo_tb sr  WITH (NOLOCK) , proposta_tb p  WITH (NOLOCK) "
        '        SQL = SQL & " WHERE p.proposta_id = " & vPropostaId
        '        'Inicio - Alexandre Fim - INC000004344237 - Desfazendo altera��o do Tales
        '        ' In�cio Altera��o - Tales de S� - INC000004323069
        '        ' Corre��o para permitir a visualiza��o de coberturas vigentes
        '        SQL = SQL & " AND s.dt_inicio_vigencia_seg <= '" & dt_inicio_query & "'"
        '        SQL = SQL & " AND (s.dt_fim_vigencia_seg is null "
        '        SQL = SQL & " OR s.dt_fim_vigencia_seg >= '" & dt_inicio_query & "')"
        '                'fim - Alexandre Fim - INC000004344237 - Desfazendo altera��o do Tales
        '        SQL = SQL & " AND c.tp_componente_id = s.tp_componente_id "
        '        SQL = SQL & " AND sr.ramo_id = s.ramo_id "
        '        SQL = SQL & " AND sr.subramo_id = s.subramo_id "
        '        SQL = SQL & " AND sr.dt_inicio_vigencia_sbr = s.dt_inicio_vigencia_sbr "
        '        SQL = SQL & " AND p.proposta_id = s.proposta_id"
        '
        '        If Not vAceito Then
        '            SQL = SQL & " AND isnull(s.tp_cliente, 'p') = 'p' "
        '        End If

        ' --- INCIDENTE: INC000004512282 --- '
        SQL = "SELECT DISTINCT "
        SQL = SQL & "ISNULL(SVA.TP_COMPONENTE_ID, 1) AS TP_COMPONENTE_ID, ISNULL(C.NOME, 'TITULAR/EMPREGADO/DIRIGENTE') AS NOMECOMP, P.RAMO_ID, "
        SQL = SQL & "SR.SUBRAMO_ID, ISNULL(SVA.DT_INICIO_VIGENCIA_SEG, SR.DT_INICIO_VIGENCIA_SBR) AS DT_INICIO_VIGENCIA_SEG, SR.NOME NOMESR, "
        SQL = SQL & "SR.DT_INICIO_VIGENCIA_SBR , P.PRODUTO_EXTERNO_ID "
        '
        SQL = SQL & "FROM SEGUROS_DB.DBO.PROPOSTA_TB AS P WITH (NOLOCK) "
        SQL = SQL & "INNER JOIN SEGUROS_DB.DBO.SUBRAMO_TB AS SR WITH (NOLOCK) ON (P.RAMO_ID = SR.RAMO_ID AND P.SUBRAMO_ID = SR.SUBRAMO_ID AND SR.DT_FIM_VIGENCIA_SBR IS NULL) "
        SQL = SQL & "LEFT JOIN " & TabSeguro & " AS SVA WITH (NOLOCK) ON (P.PROPOSTA_ID = SVA.PROPOSTA_ID) "
        SQL = SQL & "LEFT JOIN SEGUROS_DB.DBO.TP_COMPONENTE_TB AS C WITH (NOLOCK) ON (SVA.TP_COMPONENTE_ID = C.TP_COMPONENTE_ID) "
        '
        SQL = SQL & "WHERE 1=1 "
        SQL = SQL & "AND P.PROPOSTA_ID = " & vPropostaId & " "
        SQL = SQL & "AND ISNULL(SVA.DT_INICIO_VIGENCIA_SEG, SR.DT_INICIO_VIGENCIA_SBR) <= '" & dt_inicio_query & "' "
        SQL = SQL & "AND (SVA.DT_FIM_VIGENCIA_SEG IS NULL OR SVA.DT_FIM_VIGENCIA_SEG >= '" & dt_inicio_query & "') "

        If Not vAceito Then
            SQL = SQL & "AND ISNULL(SVA.TP_CLIENTE, 'p') = 'p' "
        End If
        ' --- INCIDENTE: INC000004512282 --- '

        Set rc = rdoCn1.OpenResultset(SQL)

        If Not rc.EOF Then
            'Por Alessandra Grig�rio - 27/10/2003
            If rc("produto_externo_id") = "9308" Or rc("produto_externo_id") = "8208" Then
                chkVidaEmpresa.Value = 1
            Else
                chkVidaEmpresa.Value = 0
            End If

            If SubGrupos.Count = 0 Then
                'Apolice n�o tem subgrupos
                Do While Not rc.EOF
                    Set vComponente = New Componente
                    '
                    vComponente.Codigo = rc!tp_componente_id
                    vComponente.Nome = Trim(rc!Nomecomp)
                    vComponente.DtIniSeguro = rc!dt_inicio_vigencia_seg
                    vComponente.CodSubRamo = rc!Subramo_id
                    vComponente.DtIniVigSbr = rc!dt_inicio_vigencia_sbr
                    vComponente.NomeSubRamo = rc!nomesr
                    '
                    Componentes.Add vComponente, CStr(rc!tp_componente_id)
                    '
                    'CboComponente.AddItem Trim(rc!Nomecomp)
                    'CboComponente.ItemData(CboComponente.NewIndex) = rc!tp_componente_id
                    '
                    'cboObjSegurado.AddItem Trim(rc!Nomecomp)
                    'cboObjSegurado.ItemData(cboObjSegurado.NewIndex) = rc!tp_componente_id
                    '
                    rc.MoveNext
                Loop
                '
                Monta_GridComponentes
                '
                Monta_CboComponente
                Monta_CboObjSegurado
                '
                Monta_CboComponenteQuestionario
                '

                CboComponente.ListIndex = 0
                cboObjSegurado.ListIndex = 0
                cboComponenteQuestionario.ListIndex = 0
                '
            Else    ' Tem SubGrupos
                Do While Not rc.EOF
                    Set vComponente = New Componente
                    '
                    vComponente.Codigo = rc!tp_componente_id
                    vComponente.Nome = Trim(rc!Nomecomp)
                    vComponente.DtIniSeguro = rc!dt_inicio_vigencia_seg
                    vComponente.CodSubRamo = rc!Subramo_id
                    vComponente.DtIniVigSbr = rc!dt_inicio_vigencia_sbr
                    vComponente.NomeSubRamo = Trim(rc!nomesr)
                    '
                    Componentes.Add vComponente, CStr(rc!tp_componente_id)
                    '
                    rc.MoveNext
                Loop
                '
                Monta_GridComponentes
                '
                rc.Close
                Set rc = Nothing
                '' Le os componentes do subgrupo a partir das coberturas,
                '' pq. n�o existe tabela de componentes por subgrupo
                SQL = "SELECT DISTINCT e.sub_grupo_id, c.tp_componente_id, c.nome nomecomp "
                SQL = SQL & ", s.dt_inicio_vigencia_seg , s.subramo_id, s.dt_inicio_vigencia_sbr "
                SQL = SQL & ", sr.nome nomesr, sr.dt_inicio_vigencia_sbr "
                SQL = SQL & "  FROM escolha_sub_grp_tp_cob_comp_tb e   WITH (NOLOCK)   "
                SQL = SQL & "  INNER JOIN      tp_cob_comp_tb cc  WITH(NOLOCK)                          "
                SQL = SQL & "   ON  cc.tp_cob_comp_id = e.tp_cob_comp_id "
                SQL = SQL & "  INNER JOIN      tp_componente_tb c   WITH(NOLOCK)                        "
                SQL = SQL & "   ON  c.tp_componente_id = cc.tp_componente_id "
                SQL = SQL & "  INNER JOIN      seguro_vida_tb s  WITH (NOLOCK)                    "
                SQL = SQL & "   ON s.tp_componente_id = c.tp_componente_id"
                SQL = SQL & "  INNER JOIN      subramo_tb sr  WITH (NOLOCK)                       "
                SQL = SQL & "   ON  sr.ramo_id = s.ramo_id "
                SQL = SQL & "   AND sr.subramo_id = s.subramo_id "
                SQL = SQL & "   AND sr.dt_inicio_vigencia_sbr = s.dt_inicio_vigencia_sbr "
                '*** Abosco @ 2003 abr 24
                '*** inclusao de inicio de vigencia em tp_cob_comp_item_tb
                SQL = SQL & "  INNER JOIN      tp_cob_comp_item_tb ci   WITH(NOLOCK)                "
                SQL = SQL & "   ON  cc.tp_cob_comp_id = ci.tp_cob_comp_id "
                SQL = SQL & " WHERE e.apolice_id = " & vApoliceId
                SQL = SQL & " AND e.sucursal_seguradora_id = " & vSucursalId
                SQL = SQL & " AND e.seguradora_cod_susep = " & vSeguradoraId
                SQL = SQL & " AND e.ramo_id = " & vRamoId
                SQL = SQL & " AND s.proposta_id = " & vPropostaId
                SQL = SQL & " AND s.prop_cliente_id = " & vClienteId
                'inicio - Alexandre Fim - INC000004344237 - Desfazendo altera��o do Tales
                ' In�cio Altera��o - Tales de S� - INC000004323069
                ' Corre��o para permitir a visualiza��o de coberturas vigentes
                SQL = SQL & " AND s.dt_inicio_vigencia_seg <= '" & dt_inicio_query & "'"
                SQL = SQL & " AND (s.dt_fim_vigencia_seg is null "
                SQL = SQL & "     OR s.dt_fim_vigencia_seg > = '" & dt_inicio_query & "')"
                'fim- Alexandre Fim - INC000004344237 - Desfazendo altera��o do Tales
                '*** Abosco @ 2003 abr 24
                '*** inclusao de inicio de vigencia em tp_cob_comp_item_tb
                SQL = SQL & " AND e.dt_inicio_vigencia_comp = ci.dt_inicio_vigencia_comp "
                'SQL = SQL & " AND ci.dt_fim_vigencia_comp IS NULL "

                Set rc = rdoCn1.OpenResultset(SQL)
                '
                If Not rc.EOF Then
                    While Not rc.EOF
                        Set vComponente = New Componente
                        '
                        vComponente.Codigo = rc!tp_componente_id
                        vComponente.Nome = Trim(rc!Nomecomp)
                        vComponente.DtIniSeguro = rc!dt_inicio_vigencia_seg
                        vComponente.CodSubRamo = rc!Subramo_id
                        vComponente.NomeSubRamo = Trim(rc!nomesr)
                        vComponente.DtIniVigSbr = rc!dt_inicio_vigencia_sbr
                        '
                        SubGrupos(CStr(rc!sub_grupo_id)).Componentes.Add vComponente, CStr(rc!tp_componente_id)
                        '
                        rc.MoveNext
                    Wend
                End If

            End If    ' subgrupos
            '
            rc.Close
        End If
        '
        Set rc = Nothing

    End If

    Exit Sub
Erro:
    TrataErroGeral "Ler Seguro Vida"
    MsgBox "Rotina: Ler Seguro Vida"
    'End
End Sub

Private Sub Ler_Clausulas()
    Dim rc As ADODB.Recordset
    'rdoResultset
    Dim vClausula As String, SQL As String
    Dim NovaClausula As New Clausula
    Dim NovaClasula As Clausula
    Dim TEXTOCLAUSULA As String

    On Error GoTo Erro

    cboClausula.Clear
    '
    'SQL = "SELECT cp.seq_clausula, c.descr_clausula, cp.texto_clausula "
    'SQL = SQL & "FROM clausula_personalizada_tb cp    WITH (NOLOCK) , clausula_tb c  WITH (NOLOCK)  "
    'SQL = SQL & "WHERE cp.proposta_id = " & vPropostaId
    'SQL = SQL & " AND cp.dt_inicio_vigencia <= '" & dt_inicio_query & "'"
    'SQL = SQL & " AND (cp.dt_fim_vigencia is null"
    'SQL = SQL & "      OR cp.dt_fim_vigencia >= '" & dt_inicio_query & "')"
    'SQL = SQL & " AND c.cod_clausula = cp.cod_clausula_original "
    '
    ''RCR - 2007/04/16  - Roberto Carlos Ramos (Stefanini) - FLOW 210636
    ''Alguns registros n�o estavam aparecendo no grid de cl�usulas devido ao relacionamento entre as colunas c.dt_inicio_vigencia e cp.dt_inicio_vigencia_cl_original, pois a coluna c.dt_inicio_vigencia apresentava data/hora enquanto a outra somente data.
    '
    ''edfaria - 12/08/2008 - retornado novamente no select pois necessita da hora e data para buscar o texto correto para a clausula
    'SQL = SQL & " AND c.dt_inicio_vigencia = cp.dt_inicio_vigencia_cl_original"
    ''SQL = SQL & " AND convert(char(8),c.dt_inicio_vigencia, 112) = convert(char(8),cp.dt_inicio_vigencia_cl_original,112) "
    'SQL = SQL & " ORDER BY seq_clausula"

    'AFONSO FILHO - GPTI
    'ARRUMANDO A QUERY DAS CLAUSULAS

    '*****************************************************************************
    '** Autor: Afonso Dutra Nogueira Filho - GPTI                               **
    '** Data: 29/05/09                                                          **
    '** Flow: 998293 - erro segp0223 apolice 17229                              **
    '** Descri��o: Arrumando a query para usar JOIN e organizando a query,      **
    '**            alterado a conex�o de RDO para ADO, devido a erro no retorno **
    '**            da consulta, pois o texto da clausula vem mais de 65mil      **
    '**            caracteres, causando erro no rdo                             **
    '*****************************************************************************

    'Alterado por Vagner Saraiva-Confitec em 06/12/2010
    'Mostrar somente clausulas recentes para comparar com SEGP0236
    SQL = "SELECT cp.seq_clausula [seq_clausula], IsNull(c.descr_clausula,'SEM DESCRI��O') [descr_clausula], IsNull(cp.texto_clausula,'SEM TEXTO') [texto_clausula]" & vbNewLine
    SQL = SQL & "FROM seguros_db.dbo.clausula_personalizada_tb cp    WITH (NOLOCK) " & vbNewLine
    SQL = SQL & "JOIN seguros_db.dbo.clausula_tb c  WITH (NOLOCK) " & vbNewLine
    SQL = SQL & "   ON c.cod_clausula = cp.cod_clausula_original" & vbNewLine
    SQL = SQL & "   AND c.dt_inicio_vigencia = cp.dt_inicio_vigencia_cl_original" & vbNewLine
    SQL = SQL & "Where cp.proposta_id = " & vPropostaId & vbNewLine
    'SQL = SQL & "AND cp.dt_inicio_vigencia <= '" & dt_inicio_query & "'" & vbNewLine
    SQL = SQL & "AND cp.dt_fim_vigencia IS NULL" & vbNewLine
    'SQL = SQL & "     OR cp.dt_fim_vigencia >= '" & dt_inicio_query & "')" & vbNewLine
    SQL = SQL & "ORDER BY cp.seq_clausula"

    '
    'Set rc = rdocn.OpenResultset(SQL)
    Set rc = ExecutarSQL(gsSIGLASISTEMA, _
                         glAmbiente_id, _
                         App.Title, _
                         App.FileDescription, _
                         SQL, _
                         True)

    If Not rc.EOF Then
        SSTabProp.TabEnabled(TAB_CLAUSULA) = True
        '
        While Not rc.EOF
            TEXTOCLAUSULA = rc("texto_clausula")

            cboClausula.AddItem rc!seq_clausula & " - " & Trim(rc("descr_clausula"))
            cboClausula.ItemData(cboClausula.NewIndex) = rc("seq_clausula")
            '
            Set NovaClausula = Nothing
            Set NovaClasula = New Clausula
            '
            With NovaClausula
                .Id = rc("seq_clausula")
                .Nome = Trim(rc("descr_clausula"))
                '.Descricao = rc!texto_clausula
                .Descricao = TEXTOCLAUSULA
            End With
            Clausulas.Add NovaClausula, CStr(rc("seq_clausula"))

            rc.MoveNext
        Wend
    Else
        SSTabProp.TabEnabled(TAB_CLAUSULA) = False
    End If
    '
    If cboClausula.ListCount > 0 Then cboClausula.ListIndex = 0

    rc.Close
    Set rc = Nothing

    Exit Sub
Erro:
    TrataErroGeral "Ler Clausulas Personalizadas"
    MsgBox "Rotina: Ler_Clausulas"
    'End
End Sub

Sub Monta_GridCorretores()
    Dim linha As String
    Dim vCorretor As Corretor
    Dim TotCorretagem As Double

    gridCorretor.Rows = 1
    TotCorretagem = 0
    '
    For Each vCorretor In Corretores
        linha = Format(vCorretor.Id, "######-###")
        'FLOWBR 12083065 - 07/12/2011 - AKIO OKUNO - INICIO
        linha = linha & vbTab & vCorretor.Codigo_susep
        'FLOWBR 12083065 - 07/12/2011 - AKIO OKUNO - FIM

        linha = linha & vbTab & Format(vCorretor.PercCorretagem, "0.00000")
        linha = linha & vbTab & vCorretor.Nome
        linha = linha & vbTab & Format(vCorretor.DtIniCorretagem, "dd/mm/yyyy")
        '
        gridCorretor.AddItem linha
        gridCorretor.RowData(gridCorretor.Rows - 1) = vCorretor.Id
        '
        TotCorretagem = TotCorretagem + vCorretor.PercCorretagem
    Next
    '
    TxtComissao.Text = Format(TotCorretagem, "0.00000")
    gridCorretor.Refresh
    '
End Sub

Private Sub Ler_Remuneracao_Servico()

    Dim rc As rdoResultset
    Dim SQL As String

    On Error GoTo Erro

    txtPercRemuneracaoServico.Text = ""

    SQL = ""
    SQL = SQL & "SELECT ISNULL(perc_remuneracao_servico, 0) perc_remuneracao_servico"
    SQL = SQL & "  FROM custo_tb WITH(NOLOCK)"
    SQL = SQL & " WHERE produto_id = " & vProdutoId
    SQL = SQL & "   AND ramo_id = " & vRamoId
    SQL = SQL & "   AND dt_inicio_vigencia <= '" & dt_inicio_query & "'"
    SQL = SQL & "   AND (dt_fim_vigencia IS NULL OR dt_fim_vigencia >= '" & dt_inicio_query & "')"

    Set rc = rdoCn1.OpenResultset(SQL, rdOpenStatic)

    If Not rc.EOF Then
        txtPercRemuneracaoServico.Text = Format(Val(TrocaVirgulaPorPonto(rc("perc_remuneracao_servico"))), "0.00")
        rc.Close
    End If

    Set rc = Nothing

    Exit Sub

Erro:
    TrataErroGeral "Ler Remuneracao Servico"
    MsgBox "Rotina: Ler Remuneracao Servico"

End Sub

Private Sub Ler_Assistencia()

    Dim rc As rdoResultset
    Dim SQL As String

    On Error GoTo Erro

    '136 - Ouro Vida Grupo Especial, 716 - BB Seguro Vida Mulher
    '1174 - BB Seguro Vida Mulher PA, 1196 - Ouro Vida Estilo
    If Val(vProdutoId) = 136 Or Val(vProdutoId) = 716 Or _
       Val(vProdutoId) = 1174 Or Val(vProdutoId) = 1196 Then

        fraAssistencia.Visible = True

        If Val(vProdutoId) = 136 Or Val(vProdutoId) = 716 Then

            SQL = ""
            SQL = SQL & "select t.nome,"
            SQL = SQL & "       case s.obrigatoria"
            SQL = SQL & "         when 'S' then 'OBRIGAT�RIA'"
            SQL = SQL & "         when 'N' then 'FACULTATIVA'"
            SQL = SQL & "       end as tipo,"
            SQL = SQL & "       case s.obrigatoria"
            SQL = SQL & "          when 'S' then 0"
            SQL = SQL & "          when 'N' then isnull(a.val_assistencia_facul, 0)"
            SQL = SQL & "       end as val_assistencia"
            SQL = SQL & "  from proposta_tb p WITH(NOLOCK)"
            SQL = SQL & " inner join proposta_adesao_tb a WITH(NOLOCK)"
            SQL = SQL & "    on p.proposta_id = a.proposta_id"
            SQL = SQL & " inner join assistencia_uss_db.DBO.subramo_tp_assistencia_tb s WITH(NOLOCK)"
            SQL = SQL & "    on p.produto_id = s.produto_id"
            SQL = SQL & "   and p.ramo_id = s.ramo_id"
            SQL = SQL & "   and p.subramo_id = s.subramo_id"
            SQL = SQL & "   and p.dt_inicio_vigencia_sbr = s.dt_inicio_vigencia_sbr"
            SQL = SQL & "   and s.dt_inicio_vigencia_ass <= '" & dt_inicio_query & "'"
            SQL = SQL & "   and (s.dt_fim_vigencia_ass is null or s.dt_fim_vigencia_ass >= '" & dt_inicio_query & "')"
            SQL = SQL & "   and s.obrigatoria = 'N'"
            SQL = SQL & "   and isnull(a.val_assistencia_facul, 0) > 0"
            SQL = SQL & " inner join assistencia_uss_db..tp_assistencia_tb t WITH(NOLOCK) "
            SQL = SQL & "    on s.tp_assistencia_id = t.tp_assistencia_id"
            SQL = SQL & " where p.proposta_id = " & vPropostaId

        ElseIf Val(vProdutoId) = 1174 Or Val(vProdutoId) = 1196 Then

            SQL = ""
            SQL = SQL & "select t.nome,"
            SQL = SQL & "       case s.obrigatoria"
            SQL = SQL & "         when 'S' then 'OBRIGAT�RIA'"
            SQL = SQL & "         when 'N' then 'FACULTATIVA'"
            SQL = SQL & "       end as tipo,"
            SQL = SQL & "       pa.VL_SRVC_AST_CTR val_assistencia"
            SQL = SQL & "  from seguros_db..proposta_tb p WITH(NOLOCK)"
            SQL = SQL & " inner join seguros_db..proposta_processo_susep_tb pp WITH(NOLOCK)"
            SQL = SQL & "    on pp.proposta_id = p.proposta_id"
            SQL = SQL & " inner join als_operacao_db..pln_ast_ctr pa WITH(NOLOCK)"
            SQL = SQL & "    on pa.CD_PRD = pp.cod_produto"
            SQL = SQL & "   and pa.CD_MDLD = pp.cod_modalidade"
            SQL = SQL & "   and pa.CD_ITEM_MDLD = pp.cod_item_modalidade"
            SQL = SQL & "   and pa.NR_CTR_SGRO = pp.num_contrato_seguro"
            SQL = SQL & "   and pa.NR_VRS_EDS = pp.num_versao_endosso"
            SQL = SQL & " inner join assistencia_uss_db..subramo_tp_assistencia_tb s WITH(NOLOCK)"
            SQL = SQL & "    on s.produto_id = p.produto_id"
            SQL = SQL & "   and s.plano_assistencia_id = pa.NR_SEQL_PLN_CTR"
            SQL = SQL & "   and s.dt_inicio_vigencia_ass <= '" & dt_inicio_query & "'"
            SQL = SQL & "   and (s.dt_fim_vigencia_ass is null or s.dt_fim_vigencia_ass >= '" & dt_inicio_query & "')"
            SQL = SQL & "   and s.obrigatoria = 'N'"
            SQL = SQL & " inner join assistencia_uss_db..tp_assistencia_tb t WITH(NOLOCK)"
            SQL = SQL & "    on s.tp_assistencia_id = t.tp_assistencia_id"
            SQL = SQL & " where p.proposta_id = " & vPropostaId
            'GENJUNIOR - FLOW 17860335 - CONSULTA TABELAS DE HIST�RICO

            SQL = SQL & "  UNION " & vbNewLine
            SQL = SQL & " SELECT t.nome " & vbNewLine
            SQL = SQL & "      , CASE s.obrigatoria WHEN 'S' THEN 'OBRIGAT�RIA' " & vbNewLine
            SQL = SQL & "                           WHEN 'N' THEN 'FACULTATIVA' " & vbNewLine
            SQL = SQL & "        END AS tipo " & vbNewLine
            SQL = SQL & "      , pa.VL_SRVC_AST_CTR val_assistencia " & vbNewLine
            SQL = SQL & "   FROM seguros_db..proposta_tb p WITH (NOLOCK) " & vbNewLine
            SQL = SQL & "  INNER JOIN seguros_db..proposta_processo_susep_tb pp WITH (NOLOCK) " & vbNewLine
            SQL = SQL & "     ON pp.proposta_id = p.proposta_id " & vbNewLine
            SQL = SQL & "  INNER JOIN als_operacao_hist_db.dbo.pln_ast_ctr_hist_tb pa WITH (NOLOCK) " & vbNewLine
            SQL = SQL & "     ON pa.CD_PRD = pp.cod_produto " & vbNewLine
            SQL = SQL & "    AND pa.CD_MDLD = pp.cod_modalidade " & vbNewLine
            SQL = SQL & "    AND pa.CD_ITEM_MDLD = pp.cod_item_modalidade " & vbNewLine
            SQL = SQL & "    AND pa.NR_CTR_SGRO = pp.num_contrato_seguro " & vbNewLine
            SQL = SQL & "    AND pa.NR_VRS_EDS = pp.num_versao_endosso " & vbNewLine
            SQL = SQL & "  INNER JOIN assistencia_uss_db..subramo_tp_assistencia_tb s WITH (NOLOCK) " & vbNewLine
            SQL = SQL & "     ON s.produto_id = p.produto_id " & vbNewLine
            SQL = SQL & "    AND s.plano_assistencia_id = pa.NR_SEQL_PLN_CTR " & vbNewLine
            SQL = SQL & "    AND s.dt_inicio_vigencia_ass <= '" & dt_inicio_query & "' " & vbNewLine
            SQL = SQL & "    AND (s.dt_fim_vigencia_ass IS NULL OR s.dt_fim_vigencia_ass >= '" & dt_inicio_query & "') " & vbNewLine
            SQL = SQL & "    AND s.obrigatoria = 'N' " & vbNewLine
            SQL = SQL & "  INNER JOIN assistencia_uss_db..tp_assistencia_tb t WITH (NOLOCK) " & vbNewLine
            SQL = SQL & "     ON s.tp_assistencia_id = t.tp_assistencia_id " & vbNewLine
            SQL = SQL & "  WHERE p.proposta_id = " & vPropostaId & vbNewLine

            'FIM GENJUNIOR

        End If

        Set rc = rdoCn1.OpenResultset(SQL, rdOpenStatic)

        If Not rc.EOF Then

            txtAssistFacul.Text = "SIM"
            txtAssistFacul.Tag = "S"
            cmdAssistencia.Visible = True

            rc.Close

        Else

            txtAssistFacul.Text = "N�O"
            txtAssistFacul.Tag = "N"
            cmdAssistencia.Visible = False

        End If

    End If

    Set rc = Nothing

    Exit Sub

Erro:
    TrataErroGeral "Ler Assistencia"
    MsgBox "Rotina: Ler Assistencia"

End Sub

Private Function eAgregada(Ramo As Integer, apolice As Long, SubGrupo As Long, Fatura As Long) As Boolean
'RRAMOS - 2007/06/26 - Flow 220222 - Alterado os par�metros SubGrupo e Fatura de Integer para Long.

    Dim rs As rdoResultset
    Dim SQL As String

    eAgregada = False

    SQL = " select * " & _
        " from personalizacao_cobranca_tb pc   WITH (NOLOCK) , personalizacao_cobranca_subgrupo_tb ps  WITH (NOLOCK) , " & _
        " personalizacao_fatura_item_tb pfi  WITH (NOLOCK) " & _
        " where pc.personalizacao_id = ps.personalizacao_id " & _
        " and pc.personalizacao_id = pfi.personalizacao_id " & _
        " and pc.apolice_id = " & apolice & _
        " and pc.ramo_id = " & Ramo & _
        " and ps.sub_grupo_id = " & SubGrupo & _
        " and pfi.fatura_id = " & Fatura

    Set rs = rdoCn1.OpenResultset(SQL)
    If Not rs.EOF Then
        eAgregada = True
    End If

    Set rs = Nothing
End Function

Sub Monta_GridSubGrupos()
    Dim vSubGrupo As SubGrupo
    Dim linha As String
    Dim TipoIS As String
    Dim Conjuge As String

    GridSubGrupos.Rows = 1

    'pmelo - Inclusao da situa��o na apolice_subgrupo_vida_web_tb
    GridSubGrupos.ColWidth(GRD_SUB_SITUACAO) = 0

    For Each vSubGrupo In SubGrupos
        Select Case vSubGrupo.TipoIS
        Case "C"
            TipoIS = "Capital Informado"
        Case "F"
            TipoIS = "Capital Fixo"
        Case "S"
            TipoIS = "M�ltiplo Salarial"
        Case "G"
            TipoIS = "Capital Global"
        Case "P"
            TipoIS = "Capital por Planos"
        Case "E"
            TipoIS = "Capital por Faixa Et�ria"
        Case Else
            TipoIS = "N�o Informado"
        End Select
        '
        Select Case vSubGrupo.TpConjuge
        Case "N"
            Conjuge = "Inexistente"
        Case "A"
            Conjuge = "Automatico"
        Case "F"
            Conjuge = "Facultativo"
        Case Else
            Conjuge = "N�o Informado"
        End Select
        '
        'Id     | Nome                                        | Dt Inicio Sub  | Tipo de IS
        '| Mult. Sal.  | Qtde. Vidas | IDTabua | T�bua de C�lculo | Fatura Auto. | Texto Padr�o
        '| Dia de Faturamento | Dia de Cobranca | Periodo de Pgto | Forma de Pgto. |
        'Ag. D�bito | Cta. Corrente | C�njuge     | Isento IOF
        Dim a As Long
        Dim linha1 As String

        linha = vSubGrupo.Id
        linha = linha & vbTab & vSubGrupo.Nome
        a = Conta_Vidas_Subgrupos(vSubGrupo.Id)
        linha1 = vbTab & linha & vbTab & a
        MSFlexGrid1(1).AddItem linha1
        Text1(2).Visible = True
        Text1(2).Text = MSFlexGrid1(1).Rows

        linha = linha & vbTab & vSubGrupo.Cnpj    'Genesco Silva Confitec SP Flow 4532893

        linha = linha & vbTab & Format(vSubGrupo.DtInicioSub, "dd/mm/yyyy")
        linha = linha & vbTab & TipoIS
        linha = linha & vbTab & vSubGrupo.QtdeSal
        linha = linha & vbTab & vSubGrupo.QtdVidas
        linha = linha & vbTab & vSubGrupo.IdTabua
        linha = linha & vbTab & vSubGrupo.NomeTabua
        linha = linha & vbTab & IIf(vSubGrupo.FaturaAuto, "S", "N")
        linha = linha & vbTab & vSubGrupo.TextoBenef
        linha = linha & vbTab & vSubGrupo.DiaFaturamento
        linha = linha & vbTab & vSubGrupo.DiaCobranca
        linha = linha & vbTab & Format(vSubGrupo.IndPerPgto, "00") & " - " & vSubGrupo.PeriodoPgto
        linha = linha & vbTab & Format(vSubGrupo.IndFormaPgto, "00") & " - " & vSubGrupo.FormaPgto
        If vSubGrupo.IndFormaPgto = 1 Then
            linha = linha & vbTab & Format(vSubGrupo.IdAgencia, "0000") & " - " & vSubGrupo.NomeAgencia
            linha = linha & vbTab & Format(vSubGrupo.CCorrente, "##########0")
        Else
            linha = linha & vbTab & ""
            linha = linha & vbTab & ""
        End If

        'Inicio da altera��o - Alterado por Thiago Mazzero - Confitec SP em 12/01/2012 - INC000003218986

        linha = linha & vbTab & Conjuge
        linha = linha & vbTab & IIf(vSubGrupo.IsentoIOF, "S", "N")
        linha = linha & vbTab & vSubGrupo.LimMinIdadeImpl
        linha = linha & vbTab & vSubGrupo.LimMaxIdadeImpl
        linha = linha & vbTab & vSubGrupo.LimMinIdadeMov
        linha = linha & vbTab & vSubGrupo.LimMaxIdadeMov
        linha = linha & vbTab & vSubGrupo.cartao_proposta
        linha = linha & vbTab & vSubGrupo.critica_todos
        linha = linha & vbTab & vSubGrupo.critica_capital
        linha = linha & vbTab & vSubGrupo.critica_idade
        linha = linha & vbTab & vSubGrupo.lim_max_idade
        linha = linha & vbTab & vSubGrupo.lim_max_capital
        linha = linha & vbTab & vSubGrupo.tp_faturamento
        linha = linha & vbTab & vSubGrupo.tp_vida
        linha = linha & vbTab & vSubGrupo.tp_custeio
        linha = linha & vbTab & vSubGrupo.AtualizacaoIdade
        linha = linha & vbTab & vSubGrupo.DtFimVigenciaSBG
        linha = linha & vbTab & vSubGrupo.Situacao

        'Fim da altera��o - Alterado por Thiago Mazzero - Confitec SP em 12/01/2012 - INC000003218986

        GridSubGrupos.AddItem linha
        GridSubGrupos.RowData(GridSubGrupos.Rows - 1) = vSubGrupo.Id

    Next

    GridSubGrupos.Refresh
End Sub

Private Sub cboClausula_Click()
    Dim vClausula As Clausula

    If cboClausula.ListIndex <> -1 Then
        For Each vClausula In Clausulas
            If vClausula.Id = cboClausula.ItemData(cboClausula.ListIndex) Then
                txtClausula.Text = vClausula.Descricao
                Exit For
            End If
        Next
    End If
    '
End Sub

Private Sub cboEstipulante_Click()
    Dim rc As rdoResultset
    Dim SQL As String
    Dim linha As String
    Dim PercPL As String

    On Error GoTo Erro
    If cboEstipulante.ListIndex <> -1 Then
        Set Administradores = Estipulantes(CStr(cboEstipulante.ItemData(cboEstipulante.ListIndex))).Administradores

        Monta_GridAdministradores
    End If

    GridAdm.Refresh
    '
    Exit Sub
Erro:
    TrataErroGeral "Ler Administra��o"
    MsgBox "Rotina: cboEstipulante_Click()"
End Sub

Private Sub cboObjSegurado_Click()
    If cboObjSegurado.ListIndex <> -1 Then
        Call Monta_GridCoberturas(cboObjSegurado.ItemData(cboObjSegurado.ListIndex))
    End If
    '
    FraCoberturas.Refresh
End Sub

Private Sub cboComponenteQuestionario_Click()

    Dim rc As rdoResultset
    Dim SQL As String

    If cboComponenteQuestionario.ListIndex <> -1 Then
        'INICIO questionario_objeto_tb

        Dim vObjetoSegurado As Integer
        'ListIndex 0 = Funcion�rios
        'ListIndex 1 = Diretores
        If cboComponenteQuestionario.ListIndex = 0 Then vObjetoSegurado = 1 Else vObjetoSegurado = 2

        SQL = ""
        SQL = SQL & "   SELECT q.texto_pergunta, "
        SQL = SQL & "          q.texto_resposta "
        SQL = SQL & "     FROM questionario_objeto_tb q  WITH (NOLOCK) "
        SQL = SQL & "    WHERE q.proposta_id = " & vPropostaId
        SQL = SQL & "    AND q.cod_objeto_segurado = " & vObjetoSegurado
        SQL = SQL & "    ORDER BY num_resposta "

        Set rc = rdocn.OpenResultset(SQL)

        grdQuestionarioByComponente.Rows = 1
        grdQuestionarioByComponente.Row = 0

        If Not rc.EOF Then
            While Not rc.EOF
                With grdQuestionarioByComponente
                    .Rows = .Rows + 1
                    .Row = .Rows - 1
                    If Len(rc(0)) > 186 Then
                        .RowHeight(.Row) = .RowHeight(.Row) + (.RowHeight(.Row) * 2)
                    ElseIf Len(rc(0)) > 93 Then
                        .RowHeight(.Row) = .RowHeight(.Row) * 2
                    End If
                    .TextMatrix(.Row, 0) = rc(0)

                    .TextMatrix(.Row, 1) = rc(1)

                    'Definindo tamanho das colunas de acordo com o texto
                    If .ColWidth(0) < (Len(rc(0)) * 80) Then
                        .ColWidth(0) = Len(rc(0)) * 80
                    End If

                    If .ColWidth(1) < (Len(rc(1)) * 80) Then
                        .ColWidth(1) = Len(rc(1)) * 80
                    End If

                End With
                rc.MoveNext
            Wend
            rc.Close
        End If
        'FIM questionario_objeto_tb
    End If

    FraCoberturas.Refresh
End Sub

'Private Sub CboSubGrupo_Click(Index As Integer)
'Dim ind_sub As String
'StatusBar1.SimpleText = "Aguarde..."
''
'vSubGrupoOk = False
'vSubGrupoId = "-1"
'If cboSubGrupo(Index).ListIndex <> -1 Then
'    ind_sub = CStr(cboSubGrupo(Index).ItemData(cboSubGrupo(Index).ListIndex))
'    vSubGrupoId = ind_sub
'    vSubGrupoOk = True
'    '
'    Select Case Index
'    Case 0  'Corretagem
'        Set Corretores = Nothing
'        '
'        Set Corretores = SubGrupos(ind_sub).Corretores
'        Monta_GridCorretores
'
'    Case 1  'Parametros
'        Set Componentes = Nothing
'        '
'        Set Componentes = SubGrupos(ind_sub).Componentes
'        '
'        Monta_CboComponente
'        ''
'        TxtTbCalcId.Text = SubGrupos(ind_sub).IdTabua
'        TxtTbCalculo.Text = SubGrupos(ind_sub).NomeTabua
'        txtQtdVida.Text = SubGrupos(ind_sub).QtdVidas
'
'    Case 2  'Coberturas
'        Set Coberturas = Nothing
'        Set Componentes = Nothing
'        '
'        cboObjSegurado.Clear
'        GridCoberturas.Rows = 1
'        '
'        Set Componentes = SubGrupos(ind_sub).Componentes
'        Set Coberturas = SubGrupos(ind_sub).Coberturas
'        '
'        Monta_CboObjSegurado
'    Case 3  'Administra��o
'        Set Estipulantes = Nothing
'        '
'        Set Estipulantes = SubGrupos(ind_sub).Estipulantes
'        '
'        Monta_CboEstipulante
'
'    Case 4  'Cobran�a
'        Set Faturas = Nothing
'        '
'        Set Faturas = SubGrupos(ind_sub).Faturas
'
'        'RRAMOS - 2007/06/26 - Flow 220222
'        lngLinMIN = 1
'        lngLinMAX = lngLinhasGridCobranca
'
'        cboSubGrupo(4).Enabled = False
'        Monta_GridFaturas
'        cboSubGrupo(4).Enabled = True
'        cboSubGrupo(4).SetFocus
'
'
'    Case 5  ' Cosseguro Aceito
'        If vCongenereOk Then
'            Set Faturas = Nothing
'            Set Faturas = SubGrupos(vSubGrupoId).Faturas
'            Call Monta_GridFaturas_Cosseguro(Val(txtPercParticipacao.Text), Val(txtPercDespesa.Text))
'        End If
'
'    Case 6  ' Agenciamento
'        If SubGrupos(vSubGrupoId).IdAgenciador <> "" Then
'            TxtCodAgenciador.Text = Format(SubGrupos(vSubGrupoId).IdAgenciador, "###000000")
'            TxtAgenciador.Text = SubGrupos(vSubGrupoId).NomeAgenciador
'            TxtPercAgPrim.Text = Format(SubGrupos(vSubGrupoId).PercAgenciamento, "#0.00")
'            TxtFPAgPrim.Text = FP_Agenciamento(1)
'            TxtNoParcAgPrim.Text = SubGrupos(vSubGrupoId).NoParcAgenciamento
'            If SubGrupos(vSubGrupoId).PercAgDemais > 0 Then
'                TxtPercAgDemais.Text = Format(SubGrupos(vSubGrupoId).PercAgDemais, "#0.00")
'                TxtFPAgDemais.Text = FP_Agenciamento(SubGrupos(vSubGrupoId).FormaPgtoAgDemais)
'                TxtNoParcAgDemais.Text = SubGrupos(vSubGrupoId).NoParcAgDemais
'            End If
'        Else
'            TxtCodAgenciador.Text = ""
'            TxtAgenciador.Text = ""
'            TxtPercAgPrim.Text = ""
'            TxtFPAgPrim.Text = ""
'            TxtNoParcAgPrim.Text = ""
'            TxtPercAgDemais.Text = ""
'            TxtFPAgDemais.Text = ""
'            TxtNoParcAgDemais.Text = ""
'        End If
'    Case 7
'        Set Assistencias = Nothing
'
'        Set Assistencias = SubGrupos(ind_sub).Assistencias
'
'        Monta_Grid_Assistencia
'
'    Case 8
'
'        Set Vidas_Seguradas = Nothing
'
'        Set Vidas_Seguradas = SubGrupos(ind_sub).Vidas_Seguradas
'
'        Check2(0).Value = 0
'        Check2(1).Value = 0
'        Check2(2).Value = 0
'        MEBVigencia.Text = "__/____"
'
'        Ler_Vidas_Seguradas
'
'        'Monta_GridVidas_Seguradas
'
'    Case Else
'        MsgBox "SubGrupo n�o existente!", vbCritical
'    End Select
'    GridAgendamentoVidasSeguradas.Rows = 1
'
'End If
'StatusBar1.SimpleText = ""
'End Sub

Private Sub Check1_Click()
'AFONSO - GEP -15/05/2008
'If entrada <> 0 Then
    Dim a As Long
    If Check1.Value = 1 Then
        If MSFlexGrid1(1).Visible = True Then
            a = MSFlexGrid1(1).Rows - 1
            Do While a >= 1
                MSFlexGrid1(1).TextMatrix(a, 0) = "X"
                a = a - 1
            Loop
        Else
            a = MSFlexGrid2(0).Rows - 1
            Do While a >= 1
                MSFlexGrid2(0).TextMatrix(a, 0) = "X"
                a = a - 1
            Loop
        End If
    ElseIf Check1.Value = 0 Then
        If MSFlexGrid1(1).Visible = True Then
            a = MSFlexGrid1(1).Rows - 1
            Do While a >= 1
                MSFlexGrid1(1).TextMatrix(a, 0) = ""
                a = a - 1
            Loop
        Else
            a = MSFlexGrid2(0).Rows - 1
            Do While a >= 1
                MSFlexGrid2(0).TextMatrix(a, 0) = ""
                a = a - 1
            Loop
        End If
    End If
    'End If
    'entrada = 1
End Sub

Private Sub Check2_Click(Index As Integer)
'If Index = 0 Then '- Altera��o de IS
'
'    If Check2(0).Value = 1 Then
'        MEBVigencia.Enabled = True
'    Else
'        MEBVigencia.Enabled = False
'    End If
'
'ElseIf Index = 2 Then '-Inclu��o de certificado
'
'    If Check2(2).Value = 1 Then
'        MEBVigencia.Enabled = True
'    Else
'        MEBVigencia.Enabled = False
'    End If
'
'End If
End Sub

Sub Carrega_cmbCongenere(proposta_id As Long, seguradora_cod_susep As Integer, endosso_id As Long, nossa_parte As Boolean)
    Dim rs As rdoResultset
    Dim SQL As String
    Dim Perc_partic As Currency
    Dim Perc_desp_lider As Currency
    Dim Ramo As String

    SQL = ""
    SQL = SQL & " Exec segs7067_sps "
    SQL = SQL & " @proposta_id = " & proposta_id & ","
    SQL = SQL & " @endosso_id = " & endosso_id & ","
    SQL = SQL & " @seguradora_cod_susep = " & seguradora_cod_susep & ","
    SQL = SQL & " @nossa_parte = '" & IIf(nossa_parte = True, "S", "N") & "'"
    Set rs = rdocn.OpenResultset(SQL)

    gridCobrancaCosseguro.Rows = 1
    While Not rs.EOF
        If nossa_parte = True Then
            Perc_partic = 100 - CCur("0" & Replace(rs("perc_participacao"), ".", ","))
            Perc_desp_lider = "0,00"
        Else
            Perc_partic = CCur(Replace("0" & rs("perc_participacao"), ".", ","))
            Perc_desp_lider = CCur(Replace("0" & rs("perc_despesa_lider"), ".", ","))
        End If

        gridCobrancaCosseguro.AddItem rs("num_endosso") & vbTab & _
                                      rs("num_cobranca") & vbTab & _
                                      rs("nosso_numero") & vbTab & _
                                      Format(rs("dt_agendamento"), "dd/mm/yyyy") & vbTab & _
                                      Format(Replace(rs("val_premio_tarifa"), ".", ","), "###,###,###,##0.00") & vbTab & _
                                      Format(Replace(rs("val_desconto"), ".", ","), "###,###,###,##0.00") & vbTab & _
                                      Format(Replace(rs("val_adic_fracionamento"), ".", ","), "###,###,###,##0.00") & vbTab & _
                                      Format(Replace(rs("val_comissao"), ".", ","), "###,###,###,##0.00") & vbTab & _
                                      Format(Replace(rs("val_premio_total_liquido"), ".", ","), "###,###,###,##0.00") & vbTab & _
                                      Format(Replace(rs("val_despesa_lideranca"), ".", ","), "###,###,###,##0.00") & vbTab & _
                                      Format(rs("dt_recebimento"), "dd/mm/yyyy") & vbTab & _
                                      Format(Perc_partic, "###,###,###,##0.00") & vbTab & _
                                      Format(Perc_desp_lider, "###,###,###,##0.00")
        rs.MoveNext
    Wend
    rs.Close

    Ramo = Trim(Left(txtRamo, InStr(txtRamo, "-") - 1))
    SQL = ""
    If CLng(seguradora_cod_susep) <> 0 Then
        SQL = SQL & "select num_ordem, perc_participacao, perc_despesa_lider" & vbNewLine
        SQL = SQL & "from   co_seguro_repassado_tb WITH(NOLOCK)" & vbNewLine
        SQL = SQL & "where  apolice_id = " & CLng(txtApolice.Text) & vbNewLine
        SQL = SQL & "and    ramo_id = " & Ramo & vbNewLine
        SQL = SQL & "and    rep_seguradora_cod_susep = " & seguradora_cod_susep & vbNewLine
        SQL = SQL & "and    dt_fim_participacao is null" & vbNewLine
        Set rs = rdocn.OpenResultset(SQL)
        If Not rs.EOF Then
            txtNumOrdem.Text = rs("num_ordem")
            txtPercParticipacao.Text = Format(CCur(Replace(("0" & rs("perc_participacao")), ".", ",")), "###,###,##0.00")
            txtPercDespesa.Text = Format(CCur(Replace(("0" & rs("perc_despesa_lider")), ".", ",")), "###,###,##0.00")
        End If
        rs.Close
    Else
        txtNumOrdem.Text = ""

        'verificar se foi selecionado nossa parte
        SQL = SQL & "select perc_participacao = sum(perc_participacao), perc_despesa_lider = sum(perc_despesa_lider)" & vbNewLine
        SQL = SQL & "from   co_seguro_repassado_tb WITH(NOLOCK)" & vbNewLine
        SQL = SQL & "where  apolice_id = " & CLng(txtApolice.Text) & vbNewLine
        SQL = SQL & "and    ramo_id = " & Ramo & vbNewLine
        SQL = SQL & "and    dt_fim_participacao is null" & vbNewLine
        Set rs = rdocn.OpenResultset(SQL)

        If cmbCongenere.Text = "Nossa Parte" Then
            If Not rs.EOF Then
                txtPercParticipacao.Text = Format(100 - CCur(Replace(("0" & rs("perc_participacao")), ".", ",")), "###,###,##0.00")
                txtPercDespesa.Text = "0,00"
            End If
        Else
            If Not rs.EOF Then
                txtPercParticipacao.Text = Format(CCur(Replace(("0" & rs("perc_participacao")), ".", ",")), "###,###,##0.00")
                txtPercDespesa.Text = Format(CCur(Replace(("0" & rs("perc_despesa_lider")), ".", ",")), "###,###,##0.00")
            End If
        End If

        rs.Close
    End If
End Sub

Private Sub cmbCongenere_Click()
    Dim CongenereElemento As Congenere
    Dim i As Integer
    Dim linha As String
    Dim vValor As String
    Dim vValorTemp As String
    Dim vCodSusep As String
    Dim PosPercent As String
    Dim Premio_tarifario_Total As Double
    Dim vAuxComissao As Double
    Dim Perc_desp_lider As Double
    Dim Comissao_Total As Double
    Dim Juros_Total As Double
    Dim vAuxParticipacao As Variant
    Dim vAuxPercDespLider As Variant

    vCongenereOk = False
    vCongenereId = "-1"

    If cmbCongenere.ListIndex <> -1 Then
        vCongenereId = cmbCongenere.ListIndex
        vCongenereOk = True
        'Obt�m n� de ordem se uma cosseguradora for selecionada
        If cmbCongenere.ListIndex > 1 And Congeneres.Count <> 0 Then
            vCodSusep = cmbCongenere.ItemData(cmbCongenere.ListIndex)
            txtNumOrdem = Format(Congeneres(vCodSusep).NumOrdem, "000000000")
        Else
            txtNumOrdem = ""
        End If

    End If

    If cmbCongenere.Text = "Nossa Parte" And cmbCongenere.ItemData(cmbCongenere.ListIndex) = 0 Then
        Call Carrega_cmbCongenere(CLng(vPropostaId), 0, 0, True)
    ElseIf cmbCongenere.Text = "Total do Cosseguro Cedido" And cmbCongenere.ItemData(cmbCongenere.ListIndex) = 0 Then
        Call Carrega_cmbCongenere(CLng(vPropostaId), 0, 0, False)
    Else
        Call Carrega_cmbCongenere(CLng(vPropostaId), cmbCongenere.ItemData(cmbCongenere.ListIndex), 0, False)
    End If
End Sub

Function Obtem_Nome_Arq_Externo() As String
    On Error GoTo Erro

    Dim SQL As String, linha As String
    Dim RstArquivo As rdoResultset

    'AFONSO
    'ERRO
    'no lugar do vProdutoId esta produto
    ' soh que essa variavel n�o eh usada e mais lugar nenhum!

    SQL = "SELECT a.arquivo "
    SQL = SQL & " FROM arquivo_produto_tb a    WITH (NOLOCK) , produto_tb b  WITH (NOLOCK) "
    SQL = SQL & " WHERE a.produto_id = b.produto_id "
    SQL = SQL & " and (a.produto_id = " & vProdutoId
    SQL = SQL & " or b.produto_anterior_id = " & vProdutoId & ")"

    Set RstArquivo = rdocn.OpenResultset(SQL)

    If Not RstArquivo.EOF Then
        Obtem_Nome_Arq_Externo = RstArquivo(0)
    Else
        MsgBox "Arquivo do produto n�o encontrado"
        End
    End If

    Exit Function
Erro:
    TrataErroGeral "Obter Nome do Produto Externo"
    linha = "Rotina: Obtem_Nome_Arq_Externo. Programa ser� cancelado."
    MsgBox linha
    End
End Function

Private Sub CboComponente_Click()
    Dim SQL As String
    Dim rc As rdoResultset

    If CboComponente.ListIndex <> -1 Then
        txtSubRamoId.Text = Format(Componentes(CStr(CboComponente.ItemData(CboComponente.ListIndex))).CodSubRamo, "0000")
        txtSubRamo.Text = Componentes(CStr(CboComponente.ItemData(CboComponente.ListIndex))).NomeSubRamo
    End If
End Sub

Private Sub cmdExibirTodos_Click()
    Dim iIndice As Integer

    txtDescricao.Text = ""

    If grdHistorico.TextMatrix(grdHistorico.Row, 0) <> "" Then
        For iIndice = 1 To grdHistorico.Rows - 1
            txtDescricao.Text = txtDescricao.Text & sTodasDescricoes(iIndice) & String(110, "-") & vbNewLine & vbNewLine
        Next
    End If
End Sub

Private Sub cmdImprimir_Click()
    Dim sSQL As String
    Dim rs As rdoResultset
    Dim iContVidas As Double
    Dim Selec As Boolean

    On Error GoTo TraraErro

    Screen.MousePointer = 11
    StatusBar1.SimpleText = "Aguarde..."
    If SSTabProp.Tab = 17 Then
        Selec = False
        If cboDestino.ListIndex = -1 Then
            MsgBox "Escolha o destino da impress�o do certificado", vbCritical + vbOKOnly, "Destino"
            Screen.MousePointer = vbDefault
            Exit Sub
        End If

        'AFONSO - GEP - 14/5/08
        'Verificar se foi selecionado algum segurado
        For iContVidas = 1 To MSFlexGrid2(0).Rows - 1
            If MSFlexGrid2(0).TextMatrix(iContVidas, 0) = "X" Then
                Selec = True
                Exit For
            End If
        Next iContVidas

        'caso n�o tenha selecionado nenhum segurado exibir a msg e sair do sub
        If Selec = False Then
            MsgBox "Selecione um segurado", vbCritical + vbOKOnly, "Segurado"
            Screen.MousePointer = vbDefault
            Exit Sub
        End If
        '-----------

        If cboDestino.ListIndex = 0 Then    'impres�o online - Alian�a do Brasil
            Screen.MousePointer = 11
            StatusBar1.SimpleText = "Aguarde a impress�o do documento..."
            ImprimirTela = False
            Me.Enabled = False
            SelImpressora.Show
            Screen.MousePointer = vbDefault
            StatusBar1.SimpleText = ""
        ElseIf cboDestino.ListIndex = 1 Then    'Grafica
            Screen.MousePointer = 11
            StatusBar1.SimpleText = "Aguarde a solicita��o � grafica..."
            Impressao_Grafica
            Screen.MousePointer = vbDefault
            StatusBar1.SimpleText = ""
        End If
    ElseIf InStr(",115,123,150,222,223,", "," & vProdutoId & ",") > 0 Then
        MsgBox "Imprimindo o documento de ap�lice", vbInformation + vbOKOnly, "Aviso"
        Call Imprimir_apolice_2
    Else
        MsgBox "Imprimindo o documento de ap�lice", vbInformation + vbOKOnly, "Aviso"
        Call Imprimir_Apolice
    End If

    Screen.MousePointer = 0
    MousePointer = 0
    Screen.MousePointer = vbDefault

    StatusBar1.SimpleText = ""
    DoEvents

    Exit Sub
TraraErro:
    TrataErroGeral "cmdImprimir_Click", Me.name
    TerminaSEGBR
End Sub

Private Sub Imprimir_apolice_2()

    Dim pnt As Integer
    Dim sAux As String, sAux2 As String
    Dim sSQL As String
    Dim vAux As Variant
    Dim dt_documento As Date

    Dim S_grupo As SubGrupo
    Dim vCobertura As Cobertura
    Dim vCorretor As Corretor
    Dim vAdm As Administrador
    Dim vComponente As Componente
    Dim vAssistencias As Assistencia
    Dim vEstip As estipulante

    Dim PercCorretagem As Double
    Dim vTaxa As Double
    Dim c_premio_liquido As Currency, c_premio As Currency, c_premio_tarifa As Currency

    Dim rc As rdoResultset
    Dim last_bnc_id As Integer, last_bnc_ds As String

    '--------
    Dim s_proposta_id As String, s_usuario As String
    '-----------
    Dim s_subgrupo As String, s_dados_nome As String, s_dados_cnpj_cpf As String
    Dim s_dados_ramo_ativ As String, s_dados_endereco As String, s_dados_numero As String
    Dim s_dados_bairro As String, s_dados_cidade As String, s_dados_cep As String
    Dim s_dados_uf As String, s_dados_email As String, s_dados_fone As String
    Dim s_dados_admin As String, s_dados_cpf As String, s_seguravel_perfil As String
    Dim s_seguravel_adesao As String, s_seguravel_min_1a As String, s_seguravel_max_1a As String
    Dim s_seguravel_transf As String, s_seguravel_min_demais As String, s_seguravel_max_demais As String
    Dim s_taxa_taxa As String, s_taxa_Cust_seguro As String, s_taxa_estipulante As String
    Dim s_taxa_segurado As String, s_taxa_pgto_premio As String, s_cobranca_corte As String
    Dim s_cobranca_forma_pgto As String, s_cobranca_venc_demais As String, s_cobranca_periodo As String
    Dim s_cobranca_premio_liquido_1a As String, s_cobranca_IOF_1a As String, s_cobranca_premio_total_1a As String
    Dim s_cobranca_correntista As String, s_cobranca_bnc_codigo As String, s_cobranca_bnc_nome As String
    Dim s_cobranca_bnc_agencia_dv As String, s_cobranca_bnc_conta_dv As String, s_excedente_tecnico As String
    Dim s_comissao_percentual As String
    '-----------
    Dim s_garantia_tipoIS As String, s_garantia_label_fator As String, s_garantia_valor_fator As String
    Dim s_garantia_max_IS As String, s_garantia_min_IS As String, s_garantia_desc As String
    Dim s_garantia_eleg As String, s_garantia_capital As String, s_garantia_carencia As String
    Dim s_garantia_franquia As String, s_garantia_taxa As String, s_garantia_assistencias As String
    Dim s_garantia_limites As String
    '-----------
    Dim s_cosseguro_razao As String, s_cosseguro_cnpj As String, s_cosseguro_cod_susep As String
    Dim s_cosseguro_participacao As String
    '-----------
    Dim s_corretor_nome As String, s_corretor_cod_susep As String, s_corretor_distribuicao As String
    '-----------
    Dim Processo_SUSEP As String           '   'mcamaral inicio sega9201-18672750 06102016
    Dim texto1_tp_is As String, texto2_tp_is As String, texto3_tp_is As String


    On Error GoTo jpErro
    Screen.MousePointer = vbHourglass
    DoEvents

    'Validar dados necess�rios
    If IsDate(txtDtEndosso.Text) Then
        dt_documento = CDate(txtDtEndosso.Text)

    ElseIf IsDate(txtDtEmissao.Text) Then
        dt_documento = CDate(txtDtEmissao.Text)

    Else
        Screen.MousePointer = vbNormal
        DoEvents
        MsgBox "Data de emiss�o da ap�lice inv�lida!"
        GoTo jpFim

    End If

    s_proposta_id = vPropostaId
    s_usuario = "'" & Usuario & "'"
    last_bnc_id = -1

    'limpar dados tabelas tempor�rias
    sSQL = "EXEC SEGS12948_SPI " & s_proposta_id & "," & s_usuario & ",'s'"
    Set rs = rdocn.OpenResultset(sSQL)
    rs.Close

    For Each S_grupo In SubGrupos

        c_premio_liquido = 0
        c_premio = 0
        c_premio_tarifa = 0

        With S_grupo

            s_subgrupo = "'" & .Id & "'"
            s_dados_nome = "'" & .Nome & "'"
            s_dados_cnpj_cpf = "'" & .Cnpj & "'"

            s_dados_ramo_ativ = "''"

            Dim s_grupo_ramo As String, s_modalidade As String

            sSQL = "SELECT R.sub_grupo_id, E.endereco, E.bairro, E.municipio_sise, E.estado, E.cep, C.ddd_1, c.telefone_1, c.ddd_2, c.telefone_2, c.e_mail, X.grupo_ramo, X.nome as Nome_ramo " & vbCrLf _
                 & "FROM endereco_cliente_tb        E WITH (NOLOCK) " & vbCrLf _
                 & "JOIN representacao_sub_grupo_tb R WITH (NOLOCK) ON E.cliente_id = R.est_cliente_id " & vbCrLf _
                 & "JOIN cliente_tb                 C WITH (NOLOCK) ON E.cliente_id = C.cliente_id " & vbCrLf _
                 & "JOIN ramo_tb                    X WITH (NOLOCK) ON R.ramo_id = X.ramo_id " & vbCrLf _
                 & "WHERE R.apolice_id = " & Val(txtApolice.Text) & vbCrLf _
                 & "  AND R.ramo_id = " & vRamoId & vbCrLf _
                 & "  AND R.sucursal_seguradora_id = " & vSucursalId & vbCrLf _
                 & "  AND R.seguradora_cod_susep = " & vSeguradoraId & vbCrLf _
                 & "  AND R.sub_grupo_id = " & Val(.Id)

            Set rc = rdocn.OpenResultset(sSQL)

            s_grupo_ramo = vRamoId
            s_modalidade = ""

            If Not rc.EOF Then

                s_grupo_ramo = Format(rc!grupo_ramo, "00") & Format(vRamoId, "00")
                s_modalidade = Trim(rc!Nome_ramo & "")

                s_dados_endereco = "'" & Trim(rc!Endereco & "") & "'"
                s_dados_numero = "''"
                s_dados_bairro = "'" & Trim(rc!Bairro & "") & "'"
                s_dados_cidade = "'" & Trim(rc!municipio_sise & "") & "'"
                s_dados_cep = "'" & Trim(rc!CEP & "") & "'"
                s_dados_uf = "'" & Trim(rc!Estado & "") & "'"
                s_dados_email = "'" & Trim(rc!e_mail & "") & "'"

                sAux = Trim(rc!ddd_1 & "")
                sAux2 = Trim(rc!telefone_1 & "")
                If sAux2 <> "" Then
                    sAux = IIf(sAux = "", "", sAux & " ") & sAux2
                Else
                    sAux = Trim(rc!ddd_2 & "")
                    sAux2 = Trim(rc!telefone_2 & "")
                    sAux = IIf(sAux = "", "", sAux & " ") & sAux2
                End If
                s_dados_fone = "'" & Trim(sAux) & "'"

            Else
                s_dados_endereco = "''"
                s_dados_numero = "''"
                s_dados_bairro = "''"
                s_dados_cidade = "''"
                s_dados_cep = "''"
                s_dados_uf = "''"
                s_dados_email = "''"
                s_dados_fone = "''"
            End If
            rc.Close
            Set rc = Nothing

            s_dados_admin = "'" & "" & "'"                  '<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
            s_dados_cpf = "'" & "" & "'"                    '<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

            s_seguravel_perfil = "'" & "" & "'"             '<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
            s_seguravel_adesao = "'" & "" & "'"             '<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

            s_seguravel_min_1a = "'" & .LimMinIdadeImpl & "'"
            s_seguravel_max_1a = "'" & .LimMaxIdadeImpl & "'"

            s_seguravel_transf = "'" & "" & "'"             '<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

            s_seguravel_min_demais = "'" & .LimMinIdadeMov & "'"
            s_seguravel_max_demais = "'" & .LimMaxIdadeMov & "'"


            'Campos de Taxa M�dia n�o ser�o utilizados no momento
            s_taxa_taxa = "''"
            s_taxa_Cust_seguro = "''"
            s_taxa_estipulante = "''"
            s_taxa_segurado = "''"
            s_taxa_pgto_premio = "''"

            s_cobranca_corte = "'" & .DiaFaturamento & "'"
            s_cobranca_forma_pgto = "'" & .FormaPgto & "'"
            s_cobranca_venc_demais = "'" & .DiaCobranca & "'"
            s_cobranca_periodo = "'" & .PeriodoPgto & "'"

            s_cobranca_correntista = "''"         '<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

            s_cobranca_bnc_codigo = "'" & .IdBanco & "'"
            s_cobranca_bnc_agencia_dv = "'" & .IdAgencia & "'"
            s_cobranca_bnc_conta_dv = "'" & .CCorrente & "'"

            If last_bnc_id <> .IdAgencia Then
                last_bnc_ds = ""
                last_bnc_id = .IdAgencia

                sSQL = "SELECT nome FROM seguros_db.dbo.banco_tb WHERE banco_id = " & .IdBanco
                Set rc = rdocn.OpenResultset(sSQL)
                If Not rc.EOF Then
                    last_bnc_ds = Trim(rc!Nome & "")
                End If
                rc.Close
                Set rc = Nothing
            End If
            s_cobranca_bnc_nome = "'" & last_bnc_ds & "'"

            s_excedente_tecnico = "'" & "" & "'"            '<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

            s_comissao_percentual = ""                      '<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

            'Dim vEstip As Estipulante
            For Each vEstip In S_grupo.Estipulantes
                For Each vAdm In vEstip.Administradores
                    s_comissao_percentual = "'" & vAdm.PercProLabore & "'"
                    Exit For
                Next
                If s_comissao_percentual <> "" Then
                    Exit For
                End If
            Next
        End With

        If s_comissao_percentual = "" Then
            s_comissao_percentual = "NULL"
        End If

        'BLOCO DE GARANTIAS
        Select Case UCase(S_grupo.TipoIS)
        Case "C": sAux = "Capital Informado"
        Case "F": sAux = "Capital Fixo"
        Case "S": sAux = "M�ltiplo Salarial"
        Case "G": sAux = "Capital Global"
        Case "P": sAux = "Capital por Planos"
        Case "E": sAux = "Capital por Faixa Et�ria"
        Case Else: sAux = "N�o Informado"
        End Select
        s_garantia_tipoIS = "'" & sAux & "'"
        s_garantia_valor_fator = "'" & S_grupo.QtdeSal & "'"


        s_garantia_label_fator = "'M�lt. Salarial'"   '
        s_garantia_max_IS = "'" & "" & "'"            '<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
        s_garantia_min_IS = "'" & "" & "'"            '<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

        sAux = ""
        For Each vAssistencias In S_grupo.Assistencias
            sAux = sAux & Trim(vAssistencias.DescricaoAssistencia) & vbCrLf
        Next
        If Right(sAux, 2) = vbCrLf Then sAux = Left(sAux, Len(sAux) - 2)

        s_garantia_assistencias = "'" & sAux & "'"
        s_garantia_limites = "'De acordo com regulamento(s) integrante (s) das condi��es contratuais da ap�lice.'"

        PercCorretagem = 0

        For Each vCorretor In S_grupo.Corretores
            PercCorretagem = PercCorretagem + Val(vCorretor.PercCorretagem)
        Next

        If Not (S_grupo.Estipulantes Is Nothing) Then
            For Each vEstip In S_grupo.Estipulantes
                For Each vAdm In vEstip.Administradores
                    PercCorretagem = PercCorretagem + Val(vAdm.PercProLabore)
                Next
            Next
        End If

        Set Coberturas = S_grupo.Coberturas

        'Renato Fran�a - Confitec SP - 12/05/2017 - IM00037402
        s_garantia_eleg = "''"
        'Renato Fran�a - Confitec SP - 12/05/2017 - IM00037402

        For Each vCobertura In Coberturas
            For Each vComponente In S_grupo.Componentes
                s_garantia_eleg = "''"
                If vCobertura.CodObjSegurado = vComponente.Codigo Then
                    s_garantia_eleg = "'" & vComponente.Nome & "'"
                    Exit For
                End If
            Next
            s_garantia_desc = "'" & Trim(vCobertura.Descricao) & "'"
            s_garantia_capital = "'" & Trim(vCobertura.ValIS) & "'"
            s_garantia_carencia = "'" & IIf(vCobertura.Carencia > 0, "H�", "N�O H�") & "'"
            s_garantia_franquia = "'" & IIf(vCobertura.ValFranquia > 0, "H�", "N�O H�") & "'"

            If vCobertura.ValTaxa = "" Then
                s_garantia_taxa = "'0,00'"
            Else
                If ConfiguracaoBrasil Then
                    vTaxa = vCobertura.ValTaxa
                Else
                    vTaxa = MudaVirgulaParaPonto(vCobertura.ValTaxa)
                End If
                vTaxa = vTaxa * (100 - PercCorretagem)

                s_garantia_taxa = "'" & Format(vTaxa, "0.0000") & "'"
            End If

            If ConfiguracaoBrasil Then
                c_premio_liquido = c_premio_liquido + Val(vCobertura.ValPremioLiq)
                c_premio = c_premio + Val(vCobertura.ValPremio)
                c_premio_tarifa = c_premio_tarifa + Val(vCobertura.ValPremioTarifa)

            Else
                c_premio_liquido = c_premio_liquido + Val(MudaVirgulaParaPonto(vCobertura.ValPremioLiq))
                c_premio = c_premio + Val(MudaVirgulaParaPonto(vCobertura.ValPremio))
                c_premio_tarifa = c_premio_tarifa + Val(MudaVirgulaParaPonto(vCobertura.ValPremioTarifa))
            End If


            sSQL = "EXEC SEGS12948_SPI " & s_proposta_id & "," & s_usuario & ",'n'," & s_subgrupo
            'bloco rpt_apolice_circular_491_tb
            sSQL = sSQL & ",NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL" _
                 & ",NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL" _
                 & ",NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL"
            'bloco rpt_apolice_garantia_circular_491_tb
            sSQL = sSQL & "," & s_garantia_tipoIS & "," & s_garantia_label_fator & "," & s_garantia_valor_fator _
                 & "," & s_garantia_max_IS & "," & s_garantia_min_IS & "," & s_garantia_desc _
                 & "," & s_garantia_eleg & "," & s_garantia_capital & "," & s_garantia_carencia _
                 & "," & s_garantia_franquia & "," & s_garantia_taxa & "," & s_garantia_assistencias _
                 & "," & s_garantia_limites
            'bloco rpt_apolice_cosseguro_circular_491_tb
            'sSql = sSql & ",NULL,NULL,NULL,NULL"
            'bloco rpt_apolice_corretor_circular_491_tb
            'sSql = sSql & ",NULL,NULL,NULL"

            Set rs = rdocn.OpenResultset(sSQL)
            rs.Close


        Next

        'Distribiu��o de cosseguro
        s_cosseguro_razao = "'" & "" & "'"          '<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
        s_cosseguro_cnpj = "'" & "" & "'"           '<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
        s_cosseguro_cod_susep = "'" & "" & "'"      '<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
        s_cosseguro_participacao = "'" & "" & "'"   '<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<


        'Dados do Corretor
        'Dim vCorretor As Corretor
        For Each vCorretor In S_grupo.Corretores
            s_corretor_nome = "'" & Trim(vCorretor.Nome) & "'"
            s_corretor_cod_susep = "'" & Trim(vCorretor.Codigo_susep) & "'"
            s_corretor_distribuicao = "'" & Format(vCorretor.PercParticipacao, "0.00") & "%'"

            sSQL = "EXEC SEGS12948_SPI " & s_proposta_id & "," & s_usuario & ",'n'," & s_subgrupo
            'bloco rpt_apolice_circular_491_tb
            sSQL = sSQL & ",NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL" _
                 & ",NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL" _
                 & ",NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL"
            'bloco rpt_apolice_garantia_circular_491_tb
            sSQL = sSQL & ",NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL"
            'bloco rpt_apolice_cosseguro_circular_491_tb
            sSQL = sSQL & ",NULL,NULL,NULL,NULL"
            'bloco rpt_apolice_corretor_circular_491_tb
            sSQL = sSQL & "," & s_corretor_nome & "," & s_corretor_cod_susep & "," & s_corretor_distribuicao

            Set rs = rdocn.OpenResultset(sSQL)
            rs.Close
        Next

        '    s_cobranca_premio_liquido_1a = "'" & .PremioLiq & "'"
        '    s_cobranca_IOF_1a = "'" & .IOFTotal & "'"
        '    s_cobranca_premio_total_1a = "'" & .PremioTarifaTotal & "'"


        sSQL = "EXEC SEGS12948_SPI " & s_proposta_id & "," & s_usuario & ",'n'," & s_subgrupo
        'bloco rpt_apolice_circular_491_tb
        sSQL = sSQL & "," & s_dados_nome & "," & s_dados_cnpj_cpf & "," & s_dados_ramo_ativ & "," & s_dados_endereco & "," & s_dados_numero & "," _
             & s_dados_bairro & "," & s_dados_cidade & "," & s_dados_cep & "," & s_dados_uf & "," & s_dados_email & "," & s_dados_fone & "," _
             & s_dados_admin & "," & s_dados_cpf & "," & s_seguravel_perfil & "," & s_seguravel_adesao & "," & s_seguravel_min_1a & "," _
             & s_seguravel_max_1a & "," & s_seguravel_transf & "," & s_seguravel_min_demais & "," & s_seguravel_max_demais & "," & s_taxa_taxa & "," _
             & s_taxa_Cust_seguro & "," & s_taxa_estipulante & "," & s_taxa_segurado & "," & s_taxa_pgto_premio & "," & s_cobranca_corte & "," _
             & s_cobranca_forma_pgto & "," & s_cobranca_venc_demais & "," & s_cobranca_periodo & ",'" & s_cobranca_premio_liquido_1a & "','" _
             & s_cobranca_IOF_1a & "','" & s_cobranca_premio_total_1a & "'," & s_cobranca_correntista & "," & s_cobranca_bnc_codigo & "," _
             & s_cobranca_bnc_nome & "," & s_cobranca_bnc_agencia_dv & "," & s_cobranca_bnc_conta_dv & "," & s_excedente_tecnico & "," & s_comissao_percentual

        Set rs = rdocn.OpenResultset(sSQL)
        rs.Close

    Next
    SQL = " select tp_is" & vbNewLine
    SQL = SQL & "  FROM sub_grupo_apolice_tb    WITH (NOLOCK)" & vbNewLine
    SQL = SQL & " WHERE sub_grupo_apolice_tb.apolice_id              = " & vApoliceId & vbNewLine
    SQL = SQL & "   AND sub_grupo_apolice_tb.sucursal_seguradora_id    = " & vSucursalId & vbNewLine
    SQL = SQL & "   AND sub_grupo_apolice_tb.seguradora_cod_susep      = " & vSeguradoraId & vbNewLine
    SQL = SQL & "   AND sub_grupo_apolice_tb.ramo_id                   = " & vRamoId & vbNewLine

    Set rc = rdocn.OpenResultset(SQL)
    '
    texto1_tp_is = "0"
    texto2_tp_is = "0"

    While Not rc.EOF
        If (rc!tp_is = "F") Then
            texto1_tp_is = 1
        ElseIf (rc!tp_is <> "F") Then
            texto2_tp_is = 1
        End If
        rc.MoveNext
    Wend



    '  Recupera dados da impressora padr�o
    DeviceDefault = LeArquivoIni("WIN.INI", "windows", "device")
    OldDefault = DeviceDefault

    'Inicializar Cristal
    Report.Reset

    If ImprimirTela = True Then
        Report.Destination = crptToWindow    ' direciona saida para Preview de tela.
        Report.WindowState = crptMaximized
    Else
        Report.Destination = crptToPrinter
    End If
    Report.Connect = rdocn.Connect
    Report.ReportFileName = App.PATH & "\SEGR0223-11.rpt"

    For pnt = 1 To 50: Report.Formulas(pnt) = "": Next pnt

    'Carregar dados do formul�rio
    Report.Formulas(1) = "proposta_id=" & vPropostaId
    Report.Formulas(2) = "usuario='" & Usuario & "'"

    Report.Formulas(3) = "001_grupo_ramo = '" & s_grupo_ramo & "'"
    Report.Formulas(4) = "002_modalidade = '" & s_modalidade & "'"
    Report.Formulas(5) = "003_num_apolice = '" & txtApolice.Text & "'"
    Report.Formulas(6) = "004_num_proposta = '" & txtNumero.Text & "'"
    Report.Formulas(7) = "005_dt_emissao = '" & txtDtEmissao.Text & "'"

    Report.Formulas(8) = "006_cod_agencia = '" & Trim(txtCodAgencia.Text) & "'"
    Report.Formulas(9) = "007_nm_agencia = '" & Trim(txtNomeAgencia.Text) & "'"
    Report.Formulas(10) = "008_dt_inicio_vigencia = '" & txtIniVigencia.Text & "'"
    Report.Formulas(11) = "009_dt_fim_vigencia = '" & txtFimVigencia.Text & "'"

    Report.Formulas(12) = "010_estipula_nome = '" & txtNome.Text & "'"
    Report.Formulas(13) = "011_estipula_cnpj_cpf = '" & txtCPF_CGC.Text & "'"
    Report.Formulas(14) = "012_estipula_ramo_ativ = ''"                    'N�o h� "Ramo de Atividade" e "Administrador - CPF" cadastrados no SEGBR.

    Report.Formulas(15) = "013_estipula_endereco = '" & txtEndereco.Text & "'"
    Report.Formulas(16) = "014_estipula_numero = ''"                       '<<<<<<<< N�O EXISTE, ENDERECO E N�MERO � JUNTO. Formula n�o � usada
    Report.Formulas(17) = "015_estipula_bairro = '" & TxtBairro.Text & "'"
    Report.Formulas(18) = "016_estipula_cidade = '" & txtCidade.Text & "'"
    Report.Formulas(19) = "017_estipula_cep = '" & txtCEP.Text & "'"
    Report.Formulas(20) = "018_estipula_uf = '" & txtUF.Text & "'"
    Report.Formulas(21) = "019_estipula_email = ''"                        '<<<<<<<< CARREGAR DE cliente_tb
    Report.Formulas(22) = "020_estipula_fone = '" & Trim(txtDDD.Text & " " & txtTelefone.Text) & "'"

    Report.Formulas(23) = "021_estipula_admin = ''"                        'N�o h� "Ramo de Atividade" e "Administrador - CPF" cadastrados no SEGBR.
    Report.Formulas(24) = "022_estipula_cpf = ''"                          'N�o h� "Ramo de Atividade" e "Administrador - CPF" cadastrados no SEGBR.

    vAux = Split("janeiro,fevereiro,mar�o,abril,maio,junho,julho,agosto,setembro,outubro,novembro,dezembro", ",")
    sAux = "S�o Paulo, " & Day(dt_documento) & " de " & vAux(Month(dt_documento) - 1) & " de " & Year(dt_documento) & "."
    Report.Formulas(25) = "099_local_data = '" & sAux & "'"
    vAux = Null


    'mcamaral inicio sega9201-18672750 06102016
    Report.Formulas(26) = "027_nome_produto = '" & txtProduto.Text & "'"

    SQL = "SELECT   isnull(a.num_proc_susep, p.num_proc_susep) num_proc_susep"
    SQL = SQL & " FROM  apolice_tb a  WITH (NOLOCK) "
    SQL = SQL & " join  proposta_tb b  WITH (NOLOCK) "
    SQL = SQL & "    on b.proposta_id = a.proposta_id"
    SQL = SQL & " join  produto_tb p  WITH (NOLOCK) "
    SQL = SQL & "    on p.produto_id = b.produto_id"
    SQL = SQL & " Where a.proposta_id = " & vPropostaId
    '
    Set rc = rdocn.OpenResultset(SQL)
    '
    If Not rc.EOF Then
        Processo_SUSEP = Replace(Replace(Replace(rc!num_proc_susep, "-", ""), ".", ""), "/", "")
        Processo_SUSEP = Format(Processo_SUSEP, "@@.@@@@@@/@@-@@")
        Report.Formulas(27) = "028_processo_susep= 'Processo SUSEP N�: " & Processo_SUSEP & "'"
    End If


    Report.Formulas(28) = "029_titulo = 'AP�LICE DE SEGURO'"

    If texto1_tp_is <> 1 And texto2_tp_is = 1 Then
        texto3_tp_is = 3
    End If
    If texto1_tp_is = 1 And texto2_tp_is = 1 Then
        texto3_tp_is = 2
    End If
    If texto1_tp_is = 1 And texto2_tp_is <> 1 Then
        texto3_tp_is = 1
    End If

    Report.Formulas(29) = "031_texto1 = '" & texto3_tp_is & "'"



    'mcamaral FIM sega9201-18672750 06102016


    If ImprimirTela = False Then
        ImprimeOk = False
        dialog.CancelError = True
        dialog.ShowPrinter
        '
        If Err = 32755 Then
            MousePointer = vbDefault
            GoTo jpFim
        End If
    Else
        ImprimeOk = True
    End If

    ' Seta a impressora
    DeviceDefault = LeArquivoIni("WIN.INI", "windows", "device")

    If DeviceDefault <> OldDefault Then
        MudouPrtDefault = True
    Else
        MudouPrtDefault = False
    End If

    ' Imprime documento.
    Report.Action = 1



jpFim:

    On Error Resume Next

    'limpar tabela
    sSQL = "EXEC SEGS12948_SPI " & s_proposta_id & "," & s_usuario & ",'s'"
    Set rs = rdocn.OpenResultset(sSQL)
    rs.Close

    For pnt = 1 To 50: Report.Formulas(pnt) = "": Next pnt

    If MudouPrtDefault Then
        Call RetornaPrtDefault("WIN.INI", OldDefault)
    End If
    Err.Clear

    Screen.MousePointer = vbNormal
    Exit Sub


jpErro:

    Screen.MousePointer = vbNormal
    TrataErroGeral "Impress�o de Ap�lice"


    GoTo jpFim

    Resume
End Sub


Private Sub cmdPesquisar_Click()
    Dim a As Variant
    StatusBar1.SimpleText = "Aguarde..."
    Screen.MousePointer = 11

    If MSFlexGrid1(1).Enabled = True Then
        NumRows = MSFlexGrid1(1).Rows

        Do While NumRows > 2
            MSFlexGrid1(1).RemoveItem NumRows
            NumRows = NumRows - 1
        Loop

        MSFlexGrid1(1).AddItem ""
        MSFlexGrid1(1).RemoveItem 1
        Monta_GridSubGrupos
        MSFlexGrid1(1).RemoveItem 1

        '1� fun��o de pesquisa
        Ler_Total_Vidas
        '**********************
    Else

        a = MSFlexGrid2(0).Rows - 2
        'INICIO DA ALTERA��O - DEMANDA: 10947642 - Rodrigo.Moura- Confitec Data:21/05/2011
        If a > 1 Then
            MSFlexGrid2(0).Clear
            MSFlexGrid2(0).Rows = 1
            MSFlexGrid2(0).FormatString = "   | N� Proposta | Nome do Segurado                                                                                 |   CPF                                    | Situa��o   | C�digo do Cliente"
        End If

        'Do While a >= 1
        '    MSFlexGrid2(0).Clear
        '    MSFlexGrid2(0).RemoveItem (a)
        '    a = a - 1
        'Loop
        'FIM DA ALTERA��O - DEMANDA: 10947642 - Rodrigo.Moura- Confitec Data:21/05/2011
        If MSFlexGrid2(0).Rows > 1 Then
            MSFlexGrid2(0).AddItem ""
            MSFlexGrid2(0).RemoveItem (1)
        End If

        entrada = 0

        '2� fun��o para montar o grid
        Ler_Vidas_Seguradas
        '*************************
        entrada = 1

        'Inicio:Demanda:11993840 Data:09/08/2011 Autor:Rodrigo.Moura
        'Objetivo: Tratar a grid quando o recordset da sub Ler_Vidas_Seguradas nao retornar dados
        If MSFlexGrid2(0).Rows > 1 Then
            'Fim:Demanda:11993840
            If (MSFlexGrid2(0).Rows > 2) And (MSFlexGrid2(0).TextMatrix(1, 1) = "") Then
                MSFlexGrid2(0).RemoveItem (1)
            End If
            If MSFlexGrid2(0).Rows = 2 And MSFlexGrid2(0).TextMatrix(1, 2) = "" Then
                Text1(2).Text = "0"
            Else
                Text1(2).Text = MSFlexGrid2(0).Rows - 1
            End If
            'Inicio:Demanda:11993840 Data:09/08/2011 Autor:Rodrigo.Moura
        End If
        'Fim:Demanda:11993840
    End If

    Screen.MousePointer = 0
    StatusBar1.SimpleText = ""
End Sub

Private Sub cmdSair_Click()
    SelPropBasica.txtArgumento = ""
    SelPropBasica.CmbSucursal.ListIndex = -1
    SelPropBasica.CmbRamo.ListIndex = -1
    SelPropBasica.Show
    Unload Me
    Set Clausulas = Nothing

    ' Jos� Moreira (Nova Consultoria) - 10/11/2012 - 14620257 - Melhorias no processo de subscri��o
    ' Fechar o programa caso o detalhe tenha sido aberto via par�metros
    If PodeFecharPrograma Then
        Call FinalizarAplicacao
    End If
End Sub

Private Sub Command1_Click(Index As Integer)
    Dim a As Long
    StatusBar1.SimpleText = "Aguarde..."
    entrada = 0
    MSFlexGrid2(0).Visible = False
    MSFlexGrid2(0).Enabled = False
    MSFlexGrid1(1).Visible = True
    MSFlexGrid1(1).Enabled = True
    Frame5(2).Enabled = False
    Frame5(2).Visible = False
    Check2(0).Value = 0
    Check2(1).Value = 0
    Check2(2).Value = 0
    Label1(79).Visible = False
    Command1(0).Visible = False
    Command1(0).Visible = False
    Text1(3).Visible = True
    Label1(80).Visible = True
    Text1(1).Visible = False
    Label1(81) = "N� de Subgrupos"
    Text1(2).Text = (MSFlexGrid1(1).Rows - 1)
    entrada = 1

    a = MSFlexGrid1(1).Rows - 1
    Sim = 1
    Do While a >= 1 And Sim = 1
        If MSFlexGrid1(1).TextMatrix(a, 0) = "" Then
            Sim = 0
        End If
        a = a - 1
    Loop

    If Sim = 1 Then
        Check1.Value = 1
    Else
        Check1.Value = 0
    End If

    Check1.Visible = False
    cmdImprimir.Visible = False
    cboDestino.Enabled = False
    cboDestino.Visible = False
    cboDestino.ListIndex = -1
    Frame5(3).Visible = False
    Frame5(3).Enabled = False
    visualizar.Visible = False
    visualizar.Enabled = False
    cmdPesquisar.Visible = False
    cmdPesquisar.Enabled = False

    Check2(0).Value = 0
    Check2(1).Value = 0
    Check2(2).Value = 0

    StatusBar1.SimpleText = ""
End Sub

Private Sub msFlexCartaCobranca_Click()  'Zoro.Gomes - confitec - 23/05/2014
'Imprime a cobranca
    On Error GoTo TrataErro

    If msFlexCartaCobranca.Rows > 1 Then
        '  0               1             2               3              4                5
        'N� Carta    |Dt.Impress�o   |num_deposito |dt_cancelamento |valor_corrigido |extenso
        With msFlexCartaCobranca
            If Not IsNumeric(Left(.TextMatrix(.Row, 0), 5)) Then Exit Sub
            Report.ReportFileName = App.PATH & "\SEGR0223-10.rpt"    'rpt igual nos aplicativos SEGP1381 e SEGP1383
            Report.Formulas(0) = "NumCartaCobranca= 'Carta Cobran�a n� " & .TextMatrix(.Row, 0) & "'"
            Report.Formulas(1) = "DataCorrente = 'S�o Paulo, " & DataExtenso(.TextMatrix(.Row, 1)) & ".'"

            Report.Formulas(2) = "Estipulante = '" & txtNome.Text & "'"
            Report.Formulas(3) = "Endereco = '" & txtEndereco.Text & IIf(Trim(TxtBairro.Text) = "", "", " - " & TxtBairro.Text) & "'"
            Report.Formulas(4) = "Municipio = '" & IIf(txtCidade.Text = "INDEFINIDO", "", txtCidade.Text) & IIf(Trim(txtUF.Text) = "", "", " - " & txtUF.Text) & "'"
            Report.Formulas(5) = "Cep = 'CEP: " & txtCEP.Text & "'"

            Report.Formulas(6) = "Apolice = 'REF. Ap�lice: " & txtApolice.Text & "'"
            Report.Formulas(7) = "Ramo = 'Ramo: " & txtRamo.Text & "'"

            Report.Formulas(8) = "DtCancelamento = '" & .TextMatrix(.Row, 3) & "'"
            Report.Formulas(9) = "NumDeposito = '" & .TextMatrix(.Row, 2) & "'"
            Report.Formulas(10) = "Valor = 'R$ " & Format(.TextMatrix(.Row, 4), "###,###,##0.00") & "'"
            Report.Formulas(11) = "Extenso = ' (" & .TextMatrix(.Row, 5) & ").'"
            Report.WindowTop = 0
            Report.WindowLeft = 300
            Report.WindowWidth = 870
            Report.WindowHeight = 850
            Report.Action = 1
        End With

    End If
    Exit Sub

TrataErro:
    MsgBox Err.Description, "msFlexCartaCobranca_Click", vbCritical, "ERRO"
End Sub

Private Sub vis_endosso_Click()
'ConPropBasicaVida.MousePointer = 11
    Dim rc As rdoResultset, rc1 As rdoResultset
    Dim linha As String
    Dim dt_emissao_saida As String
    Dim dt_impressao_saida As String
    Dim descricao_saida As String
    Dim descricao_aux As Variant
    Dim SQL As String

    '05/12/2005 - Ana Paula - Stefanini
    Dim sSQLNU As String
    Dim rsNomeUsuario As rdoResultset
    vis_endosso.Visible = False

    On Error GoTo Erro
    'lpinto
    If Not bPropostaRecusada And Not bPropostaNaoEmitida And Not bPropostaEstudo Then    'rmaiellaro (19/01/2007)
        gridEndosso.Rows = 1

        'MYOSHIMURA 11/12/2004
        'SE HOUVER MAIS DE 43750 ENDOSSOS
        'EXIBIR SOMENTE OS DO ANO VIGENTE

        SQL = "SELECT COUNT(1) "
        SQL = SQL & "FROM  endosso_tb e WITH (NOLOCK) "
        SQL = SQL & "LEFT JOIN tp_endosso_tb te WITH (nolock) "
        SQL = SQL & "   ON  te.tp_endosso_id = e.tp_endosso_id "
        SQL = SQL & "LEFT JOIN fatura_tb f on  f.proposta_id = e.proposta_id   and f.endosso_id = e.endosso_id "
        SQL = SQL & "WHERE e.proposta_id = " & vPropostaId
        Set rc = rdoCn1.OpenResultset(SQL, rdOpenStatic)

        SQL = _
        "SELECT DISTINCT  e.endosso_id " & _
              ", e.dt_emissao " & _
              ", e.dt_pedido_endosso " & _
              ", e.dt_impressao " & _
              ", CONVERT(VARCHAR(7900), e.descricao_endosso) descricao_endosso " & _
              ", ISNULL(e.tp_endosso_id, 0) tp_endosso_id " & _
              ", ISNULL(te.descricao, '') descricao " & _
              ", e.origem_endosso " & _
              ", f.dt_inicio_vigencia " & _
              ", e.usuario " & _
              ", s.sub_grupo_id " & _
              ", ISNULL(CASE WHEN te.tp_endosso_id IN (100,101) " & _
              "THEN ' ' + REPLICATE('0', 3 - LEN(s.sub_grupo_id)) + CAST(s.sub_grupo_id AS VARCHAR(3)) + ' - ' + s.nome " & _
              "END, ' ') sub_grupo_endoso " & _
                 " ,e.tp_alteracao_endosso as tp_alteracao_endosso " 'rfarzat - 30/11/2018 - Endosso 252 Sala �gil
        SQL = SQL & _
            " FROM endosso_TB e WITH (NOLOCK) " & _
              "LEFT JOIN tp_endosso_TB te WITH (NOLOCK) " & _
              "ON te.tp_endosso_id = e.tp_endosso_id " & _
              "LEFT JOIN fatura_TB f WITH (NOLOCK) " & _
              "ON f.proposta_id = e.proposta_id " & _
              "AND f.endosso_id = e.endosso_id " & _
              "LEFT JOIN sub_grupo_apolice_tb AS s WITH (NOLOCK) " & _
              "ON s.apolice_id = f.apolice_id " & _
              "AND s.sucursal_seguradora_id = f.sucursal_seguradora_id " & _
              "AND s.seguradora_cod_susep = f.seguradora_cod_susep " & _
              "AND s.ramo_id = f.ramo_id " & _
              "AND s.sub_grupo_id = f.sub_grupo_id "

        SQL = SQL & _
              "WHERE e.proposta_id = " & vPropostaId & " "

        If rc(0) > 43750 Then
            SQL = SQL & "AND e.dt_emissao >= '" & Year(Date) & "0101" & "' "
        End If

        SQL = SQL & _
              "ORDER BY e.endosso_id "

        rc.Close

        Set rc = rdoCn1.OpenResultset(SQL, rdOpenStatic)

        Do While Not rc.EOF
            DoEvents
            descricao_aux = rc!descricao_endosso
            descricao_saida = IIf(IsNull(descricao_aux), " ", descricao_aux)
            descricao_saida = Trim(descricao_saida)
            linha = rc!endosso_id
            '' Data de Ini. Vig. Fatura
            '''Pedro Henrique Gomes dos Santos em 06 de Maio de 2004'''''''''''''''''''''''''''''''''''''''
            ' Tratamento do SCS - GEROT/DIOPE 233-04
            linha = linha & vbTab & Format(rc!dt_emissao, "dd/mm/yyyy")
            linha = linha & vbTab & Format(rc!dt_pedido_endosso, "dd/mm/yyyy")
            '''Fim - Pedro Henrique Gomes dos Santos'''''''''''''''''''''''''''''''''''''''''''''''''''''''

            'Por Daniel Romualdo - 06/11/2003
            If rc!tp_endosso_id = 100 Then    'Fatura
                linha = linha & vbTab & Format(rc!dt_inicio_vigencia, "dd/mm/yyyy")
                gridEndosso.ColWidth(3) = 1500
            Else
                linha = linha & vbTab & " "
            End If

            If rc!tp_endosso_id = 0 Then
                linha = linha & vbTab & " "
            Else
                linha = linha & vbTab & rc!tp_endosso_id & " - " & Trim(rc!Descricao)
            End If
            '
            If IsNull(rc!dt_emissao) Then
                dt_emissao_saida = " "
            Else
                dt_emissao_saida = Format(rc!dt_emissao, "dd/mm/yyyy")
            End If
            '
            If IsNull(rc!dt_impressao) Then
                dt_impressao_saida = " "
            ElseIf Year(rc!dt_impressao) = "1900" Then
                dt_impressao_saida = "Migra��o"
            Else
                dt_impressao_saida = Format(rc!dt_impressao, "dd/mm/yyyy")
            End If
            '
            linha = linha & vbTab & dt_impressao_saida
            linha = linha & vbTab & descricao_saida
            ''
            Select Case UCase(rc!origem_endosso)
            Case "A"
                linha = linha & vbTab & "AUTOMATICO"
            Case "M"
                linha = linha & vbTab & "MANUAL"
            Case Else
                linha = linha & vbTab & ""
            End Select

            '05/12/2005 - Ana Paula - Stefanini
            'Inclus�o da Informa��o Usu�rio.
            If UCase(Trim(rc("usuario"))) = "PRODUCAO2" Or Trim(rc("usuario")) = "99988877714" Then
                linha = linha & vbTab & "Usu�rio Produ��o"
            Else
                sSQLNU = ""
                sSQLNU = sSQLNU & " SELECT DISTINCT nome FROM segab_db..usuario_tb" & vbNewLine
                sSQLNU = sSQLNU & " WHERE cpf = '" & UCase(Trim(rc("usuario"))) & "'" & vbNewLine
                sSQLNU = sSQLNU & " OR UPPER(login_rede) = upper('" & UCase(Trim(rc("usuario"))) & "')"

                Set rsNomeUsuario = rdoCn1.OpenResultset(sSQLNU, rdOpenStatic)

                If Not rsNomeUsuario.EOF Then
                    linha = linha & vbTab & rsNomeUsuario("nome")
                End If

                rsNomeUsuario.Close
            End If

            linha = linha & vbTab & rc!sub_grupo_endoso
            linha = linha & vbTab & rc!tp_alteracao_endosso    'rfarzat - 24/10/2018 - Endosso 252 Sala �gil

            gridEndosso.AddItem linha
            '
            rc.MoveNext
        Loop

        rc.Close
        Set rc = Nothing
    End If
    Exit Sub
    Resume
Erro:
    TrataErroGeral "vis_endosso_Click"
    MsgBox "Rotina: vis_endosso_Click"
    'End

End Sub



Private Sub Form_Activate()
'SSTabProp.Tab = TAB_PROPOSTA
    ConPropBasicaVida.Refresh
    visualizar.Visible = False
End Sub

Sub ExibeBoleto(ByVal AProposta, ByVal ACobranca, ByVal AEndosso, ByVal ANossoNumero)
    Dim Resultado
    Dim RX As rdoResultset
    Dim rs As rdoResultset
    Dim Caminho As String, SQL As String, banco_de_dados As String
    Dim convenio As String    'MARCIO.NOGUEIRA - CONFITEC SISTEMAS - 25/07/2017 PROJETO: 19368999 - cobran�a registrada AB e ABS

    'Verficando a situa��o do agendamento, o boleto n�o pode ser impresso caso o agendamento
    'esteja pago ou cancelado'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    'Agrigorio - 31/03/2004 - Alterado para impress�o de boleto de produtos WEB

    '************************************************************************************
    ' 02/09/2004 - Alandy - Inserido a verifica��o se o boleto tem data de at�
    '                       31/08/2004, se esta condi��o for verdadeira o sistema
    '                       n�o permitir� a emiss�o do Boleto. Para isso foi
    '                       inclu�do na select o campo dt_agendamento as data_vencimento
    '                       e um bloco if para verifica��o do crit�rio.
    ' 07/10/2004 - Alandy - Retirado a regra citada acima conforme solicitado pela Ana
    '************************************************************************************

    If Not (vProdutoId >= 712 And vProdutoId <= 715) Then
        SQL = "SELECT situacao, canc_endosso_id cancelado, dt_agendamento as data_vencimento "    ', db_name() banco "
        SQL = SQL & "FROM agendamento_cobranca_tb  WITH (NOLOCK) "
        SQL = SQL & "WHERE proposta_id = " & AProposta
        SQL = SQL & "  AND num_cobranca = " & ACobranca

        Set RX = rdoCn1.OpenResultset(SQL)

        If LCase(RX("situacao")) <> "p" Then
            MsgBox "Esta cobran�a n�o est� pendente de retorno. A boleta n�o ser� exibida."
            GoTo SAI
        End If

        If Not IsNull(RX("cancelado")) Then
            MsgBox "Esta cobran�a foi cancelada. A Boleta n�o ser� exibida."
            GoTo SAI
        End If

        '      If Sgn(DateDiff("d", RX("data_vencimento"), "1/9/2004")) > -1 Then
        '         MsgBox "Esta cobran�a  tem o vencimento inferior a 31/08/2004 e est� inadimplente. A Boleta n�o ser� exibida.", vbInformation
        '         GoTo SAI
        '      End If
    Else
        'Valida��o para impress�o de boleto de produtos WEB - N�o possuem agendamento
        SQL = "SELECT situacao "
        SQL = SQL & "FROM fatura_tb  WITH (NOLOCK) "
        SQL = SQL & "WHERE proposta_id = " & AProposta
        SQL = SQL & "  AND fatura_id = " & ACobranca
        Set RX = rdocn.OpenResultset(SQL)

        If LCase(RX("situacao")) = "c" Then
            MsgBox "Esta cobran�a foi cancelada. A Boleta n�o ser� exibida."
            GoTo SAI
        End If

        If LCase(RX("situacao")) <> "p" And LCase(RX("situacao")) <> "t" Then
            MsgBox "Esta cobran�a n�o est� pendente de retorno. A boleta n�o ser� exibida."
            GoTo SAI
        End If
    End If


    ' Inicio - Demanda - 19368999 - Cobran�a registrada AB e ABS - MArcio.Nogueira
    ' Busca se o flag do convenio do boleto esta como registra boleto = sim
    If Retorna_convenio_registra_boleto(Mid(Trim(ANossoNumero), 1, 7)) = "S" Then
        ' Inicio Registra o Boleto no Webservice: Demanda - 19368999 - Cobran�a registrada AB e ABS - MArcio.Nogueira
        SQL = ""
        SQL = "select count (1) as qtd "
        SQL = SQL & " from seguros_db.dbo.agendamento_cobranca_atual_tb with(nolock)"
        SQL = SQL & " Where proposta_id = " & Trim(AProposta) & ""
        SQL = SQL & " and num_cobranca = " & Trim(ACobranca) & ""
        SQL = SQL & " and isnull(fl_boleto_registrado,'N') <> 'S'"

        Set rs = rdocn.OpenResultset(SQL)

        'Se data for alterada Registra o Boleto
        If rs("qtd") > 0 Then    '- Marcio.nogueira - COnfitec/rj - 20/07/2017 - Demanda - 19368999 - Registro online convenios BB - MU-2017-027161
            Call ProcessaRegistraBoletosBB(AProposta, ACobranca)    '-- Marcio.nogueira - COnfitec/rj - 20/07/2017 - Demanda - 19368999 - Registro online convenios BB - MU-2017-027161
            If BlnBoletoRegistrado = False Then
                Exit Sub
            Else
                'R.FOUREAUX - 31/03/2017
                SQL = ""
                SQL = " EXEC seguros_db.dbo.SEGS13341_SPU " & vbNewLine
                SQL = SQL & " @proposta_id = " & Trim(AProposta) & "" & vbNewLine
                SQL = SQL & ", @num_cobranca = " & Trim(ACobranca) & "" & vbNewLine
                SQL = SQL & ", @fl_boleto_registrado = 'S' "
                Call rdocn.OpenResultset(SQL)
            End If
        End If
    End If

    'Carrengando p�gina ASP com a exibi��o da boleta'''''''''''''''''''''''''''''''''''''''''
    ''
    Caminho = Environ("programfiles") & "\Internet explorer\iexplore.exe -nohome "
    If glAmbiente_id = 3 Or glAmbiente_id = 7 Then    'Demanda - 19368999 - Cobran�a registrada AB e ABS - MArcio.Nogueira
        Caminho = Caminho & "https://qld.aliancadobrasil.com.br/internet/serv/boleto/impressao/"
    Else
        Caminho = Caminho & "http://www.aliancadobrasil.com.br/internet/serv/boleto/impressao/"
    End If

    Caminho = Caminho & "boleto.asp"
    Caminho = Caminho & "?proposta=" & Trim(AProposta)
    Caminho = Caminho & "&cobranca=" & Trim(ACobranca)
    Resultado = Shell(Caminho, vbMaximizedFocus)

    'Tiago Nascimento - Stefanini - 27/07/2007 - FLOW: 224109 ****************************
    'Campo imprime_grafica � setado no SEGP0544 para verificar se a impressao ser� na gr�fica
    SQL = "SELECT imprime_grafica "
    SQL = SQL & "FROM endosso_financeiro_tb  WITH (NOLOCK) "
    SQL = SQL & "WHERE proposta_id = " & AProposta
    SQL = SQL & "  AND endosso_id = " & AEndosso
    Set RX = rdocn.OpenResultset(SQL)

    If Not RX.EOF Then
        If RX("imprime_grafica") <> "s" Then
            Set rs = rdocn.OpenResultset("exec evento_seguros_db..evento_impressao_spi " & _
                                         vPropostaId & _
                                         "," & Val(AEndosso) & _
                                         ", null " & _
                                         ", '" & Usuario & "'" & _
                                         ", null " & _
                                         ", 21 " & _
                                         ",'O'" & _
                                         ", 'IMPRESSAO DE BOLETO BANCARIO'" & _
                                         ", " & Val(vSubGrupoId) & _
                                         ", 'E'" & _
                                         ", null " & _
                                         ", null " & _
                                         ", '" & Usuario & "'" & _
                                         ", null" & _
                                         ", null")
            rs.Close
        End If
    End If

    '*************************************************************************************
    '---- Inicio das altera��es ( Eventos de impress�o on-line )
    '---- Stefanini 15/07/2005
    '    Set Rs = rdocn.OpenResultset("exec evento_seguros_db..evento_impressao_spi " & _
         '               vPropostaId & _
         '               "," & Val(AEndosso) & _
         '               ", null " & _
         '               ", '" & Usuario & "'" & _
         '               ", null " & _
         '               ", 21 " & _
         '               ",'O'" & _
         '               ", 'IMPRESSAO DE BOLETO BANCARIO'" & _
         '               ", " & Val(vSubGrupoId) & _
         '               ", 'E'" & _
         '               ", null " & _
         '               ", null " & _
         '               ", '" & Usuario & "'" & _
         '               ", null" & _
         '               ", null")
    '    Rs.Close
    '---- Fim das altera��es ( Eventos de impress�o on-line )
    '---- Stefanini 15/07/2005
SAI:
    RX.Close
End Sub

'************************
'FUN��O DESABILIDADA!   *
'AFONSO - G&P           *
'DATA: 16/05/2008       *
'************************
'Function Monta_TextoAnexo(ByRef Texto1 As String) As String
'Dim CR_LF As String, i As Integer, j As Integer, vCobertura As Cobertura
'Dim SQL As String, rs As rdoResultset
'
'CR_LF = Chr$(13) & Chr$(10) & Space(7)
'
'Texto1 = ""
''Para cada Objeto segurado
'For i = 1 To GridComponente.Rows - 1
'   Texto1 = Texto1 & "ITEM " & Format(i, "00") & ":"
'   Texto1 = Texto1 & CR_LF
'   Texto1 = Texto1 & "LOCAL DO RISCO: "
'   'Procurar endere�o de risco
'   For j = 1 To GridEndRisco.Rows - 1
'      If GridEndRisco.TextMatrix(j, 0) = GridEndRisco.TextMatrix(i, 0) Then
'         ' Endere�o
'         Texto1 = Texto1 & GridEndRisco.TextMatrix(j, 1)
'         ' Bairro
'         Texto1 = Texto1 & IIf(GridEndRisco.TextMatrix(j, 2) = "", "", " - " & GridEndRisco.TextMatrix(j, 2))
'         ' Cidade
'         Texto1 = Texto1 & " - " & GridEndRisco.TextMatrix(j, 3)
'         ' Estado
'         Texto1 = Texto1 & "- " & GridEndRisco.TextMatrix(j, 4)
'      End If
'   Next
'   Texto1 = Texto1 & CR_LF
'   Texto1 = Texto1 & "CLASSIFICA��O:  RUBRICA: "
'   Texto1 = Texto1 & Format(GridComponente.TextMatrix(i, 2), "0000\.00") & CR_LF
'   Texto1 = Texto1 & "OCUPAC�O:" & Space(7)
'
'   SQL = "Select modalidade_ocupacao_tb.nome "
'   SQL = SQL & "From rubrica_tb   WITH (NOLOCK) , modalidade_ocupacao_tb   WITH (NOLOCK)   Where "
'   SQL = SQL & "rubrica_tb.cod_modalidade_ocupacao=modalidade_ocupacao_tb.cod_modalidade_ocupacao"
'   SQL = SQL & " and rubrica_tb.tp_rubrica = 2 And "
'   SQL = SQL & "rubrica_tb.cod_rubrica ='" & GridComponente.TextMatrix(i, 2) & "'"
'
'   Set rs = rdocn.OpenResultset(SQL)
'
'   If Not rs.EOF Then
'       If IsNull(rs(0)) = False Then
'         Texto1 = Texto1 & rs(0) & CR_LF
'       End If
'   End If
'
'   'T�tulo p/ coberturas
'
'   Texto1 = Texto1 & "COBERTURAS"
'   Texto1 = Texto1 & Space(50) & "I. S. (R$)"
'   Texto1 = Texto1 & Space(9) & "PR�MIO(R$)" & CR_LF
'   Texto1 = Texto1 & CR_LF
'
'   'Procurar todas as coberturas
'   For Each vCobertura In Coberturas
'      If vCobertura.CodObjSegurado = i Then
'      ' C�digo da Cobertura
'         Texto1 = Texto1 & Format(vCobertura.TpCoberturaId, "000") & " - "
'         ' Descri��o
'         Texto1 = Texto1 & Left(Left(vCobertura.Descricao, 45) & Space(46), 46)
'         ' Import�ncia Segurada
'         If Not ConfiguracaoBrasil Then
'             Texto1 = Texto1 & Right(Space(18) & TrocaValorAmePorBras(Format(CDbl(vCobertura.ValIS), "###,###,##0.00")), 18) & Space(1)
'         Else
'             Texto1 = Texto1 & Right(Space(18) & Format(CDbl(vCobertura.ValIS), "###,###,##0.00"), 18) & Space(1)
'         End If
'         ' Pr�mio
'         If Not ConfiguracaoBrasil Then
'             Texto1 = Texto1 & Right(Space(18) & TrocaValorAmePorBras(Format(CDbl(vCobertura.ValPremio), "###,###,##0.00")), 18) & CR_LF
'         Else
'             Texto1 = Texto1 & Right(Space(18) & Format(CDbl(vCobertura.ValPremio), "###,###,##0.00"), 18) & CR_LF
'         End If
'      End If
'   Next
'   Texto1 = Texto1 & CR_LF
'   Texto1 = Texto1 & CR_LF
'Next
'
'Texto1 = Texto1 & CR_LF ' LINHA EM BRANCO
'Texto1 = Texto1 & CR_LF ' LINHA EM BRANCO
'Texto1 = Texto1 & CR_LF ' LINHA EM BRANCO
'
'End Function

Private Sub Form_Load()
    Dim SQL As String, rc As rdoResultset
    Dim dt_endosso As String, num_endosso As String
    Dim Atual As Boolean
    Dim lpBuff As String * 25
    Dim straux As String
    
    Dim bPossui_Fluxo_Digital As Boolean

    On Error GoTo Erro

    SSTabProp.TabVisible(TAB_AGENCIAMENTO) = False

    MousePointer = vbHourglass
    '
    Call CentraFrm(Me)
    '
    Call desmonta_colecao
    '
    Zera_Campos
    'busca usuario da maquina
    Retorno = GetUserName(lpBuff, 25)
    Usuario = Left(lpBuff, InStr(lpBuff, Chr(0)) - 1)


    MSFlexGrid1(1).Top = 800
    MSFlexGrid1(1).Height = 5155

    '*** Inclus�o do Sinalizado de PPE - Pessoas politicamente expostas - demanda 269994
    '*** 23/09/2008 - Fabio (Stefanini)
    Me.chkPPE.Enabled = False

    With SelPropBasica
        vPropostaId = .GridProposta.RowData(.GridProposta.Row)
        txtNumero.Text = vPropostaId
        ' inicio mc_amaral 22/10/201317860335 - Melhorias no SEGBR(MP13?) - Acelerar aplicativos de consulta acessados pelo Sinistro
        If (vPropostaId = 209) Or (vPropostaId = 221) Or (vPropostaId = 201) Or (vPropostaId = 212) Or (vPropostaId = 218) Or _
           vPropostaId = 213 Or vPropostaId = 216 Or vPropostaId = 210 Or vPropostaId = 224 Or vPropostaId = 204 Or _
           vPropostaId = 211 Or vPropostaId = 219 Or vPropostaId = 205 Or vPropostaId = 202 Or vPropostaId = 225 Or _
           vPropostaId = 208 Or vPropostaId = 217 Or vPropostaId = 203 Or vPropostaId = 214 Or vPropostaId = 220 Or _
           vPropostaId = 206 Or vPropostaId = 200 Then

            Dim rsSC As New Recordset
            SQL = "SELECT santa_catarina_nu_proposta" & vbNewLine
            SQL = SQL & "  ,santa_catarina_cd_apolice" & vbNewLine
            SQL = SQL & "  ,santa_catarina_cd_certificado " & vbNewLine
            SQL = SQL & "  ,alianca_cd_produto"
            SQL = SQL & " FROM SANTA_CATARINA_LEGADO_TB WITH(NOLOCK) WHERE alianca_cd_certificado = " & vPropostaId

            'Set rsSC = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, SQL, True)
            Set rsSC = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, "SEGP0223", App.FileDescription, SQL, True)
            txtPropostaSC = ""
            txtApoliceSC = ""
            txtCertificadoSC = ""
            If rsSC.EOF Then
                lblPropostaSC.Visible = False
                lblApoliceSC.Visible = False
                lblCertificadoSC.Visible = False
                txtPropostaSC.Visible = False
                txtApoliceSC.Visible = False
                txtCertificadoSC.Visible = False
            Else
                txtPropostaSC = rsSC("santa_catarina_nu_proposta")
                txtApoliceSC = rsSC("santa_catarina_cd_apolice")
                txtCertificadoSC = rsSC("santa_catarina_cd_certificado")
            End If
            rsSC.Close
            Set rsSC = Nothing
        Else
            txtPropostaSC = ""
            txtApoliceSC = ""
            txtCertificadoSC = ""
            lblPropostaSC.Visible = False
            lblApoliceSC.Visible = False
            lblCertificadoSC.Visible = False
            txtPropostaSC.Visible = False
            txtApoliceSC.Visible = False
            txtCertificadoSC.Visible = False
        End If
        ' Fim  mc_amaral 22/10/201317860335 - Melhorias no SEGBR(MP13?) - Acelerar aplicativos de consulta acessados pelo Sinistro
        'Rmarins - 05/01/2006 - Inclu��o do campo seg_cliente
        vSegCliente = Trim(.GridProposta.TextMatrix(.GridProposta.Row, GRD_SEL_SEGCLI))

        '
        '' Converte e reconverte para retirar o coment�rio
        '' que aparece no grid (Atual, Original, etc.)
        vApoliceId = CStr(Val(.GridProposta.TextMatrix(.GridProposta.Row, GRD_SEL_APOL)))
        '

        vProdutoId = .GridProposta.TextMatrix(.GridProposta.Row, GRD_SEL_PROD)

        'In�cio - Isabeli Silva - Confitec SP - 2017-10-17
        'MU-2017-036534 Cadastramento dos Corretores Exclusivos no SEGBR
        isCorretor = ValidaCorretor(gsCPF)
        CorretorProposta = ValidaCorretorProposta(vPropostaId)
        'Fim - Isabeli Silva - Confitec SP - 2017-10-17

        If .cboTipoSelecao.Text = "CNPJ" Or .cboTipoSelecao.Text = "Raz�o Social" Then
            vNomeProd = .GridProposta.TextMatrix(.GridProposta.Row, 5)
        End If

        dt_endosso = .GridProposta.TextMatrix(.GridProposta.Row, GRD_SEL_DTENDO)
        Atual = False
        If .GridProposta.Row = .GridProposta.Rows - 1 Then
            Atual = True
        End If
        '
        num_endosso = .GridProposta.TextMatrix(.GridProposta.Row, GRD_SEL_NOENDO)
        If Val(num_endosso) > 0 Then
            txtNumEndosso.Text = num_endosso
            indEndosso = True
        Else
            indEndosso = False
        End If
        '
        vClienteId = .GridProposta.TextMatrix(.GridProposta.Row, GRD_SEL_CODCLI)
        txtCodCliente.Text = vClienteId
        '
        dt_contratacao = .GridProposta.TextMatrix(.GridProposta.Row, GRD_SEL_DTCONT)
        txtDtContratacao.Text = dt_contratacao
        '
        straux = .GridProposta.TextMatrix(.GridProposta.Row, GRD_SEL_RAMO)
        straux = straux & " - " & Trim(.GridProposta.TextMatrix(.GridProposta.Row, GRD_SEL_NMRAMO))
        txtRamo.Text = straux
        '
        txtStatus.Text = .GridProposta.TextMatrix(.GridProposta.Row, GRD_SEL_SIT)
    End With    'SelPropBasica
    '
    sDecimal = LeArquivoIni("WIN.INI", "intl", "sDecimal")
    If sDecimal = "." Then
        ConfiguracaoBrasil = False
        vValZero = "0.00"
    Else
        ConfiguracaoBrasil = True
        vValZero = "0,00"
    End If
    '
    ' Lpinto 28/08/2005
    If SelPropBasica.cboTipoSelecao.Text = "Nome do Cliente" Then
        If SelPropBasica.GridCliente.Rows = 2 Then
            If SelPropBasica.GridCliente.TextMatrix(1, 5) = "N�o emitida" Then
                cmdImprimir.Enabled = False
                bPropostaNaoEmitida = True
            End If
        Else

            If SelPropBasica.GridCliente.TextMatrix(SelPropBasica.GridCliente.Row, 5) = "N�o Emitida" Then
                cmdImprimir.Enabled = False
                bPropostaNaoEmitida = True
            Else
                bPropostaNaoEmitida = False
            End If
        End If
    Else    ' proposta bb
        If SelPropBasica.cboTipoSelecao.Text = "N� da Proposta BB" Then
            If SelPropBasica.GridProposta.Rows = 2 Then
                If SelPropBasica.GridProposta.TextMatrix(1, 7) = "N�o Emitido" Then
                    cmdImprimir.Enabled = False
                    bPropostaNaoEmitida = True
                End If
            Else

                If SelPropBasica.GridProposta.TextMatrix(SelPropBasica.GridProposta.Row, 7) = "N�o Emitido" Then
                    cmdImprimir.Enabled = False
                    bPropostaNaoEmitida = True
                Else
                    bPropostaNaoEmitida = False
                End If
            End If
        End If
    End If
    If Not bPropostaNaoEmitida Then
        '' L� a ap�lice para obter o in�cio de vig�ncia
        Ler_Apolice
    End If    '

    If Trim(dt_endosso) = "" Then
        'dt_inicio_vigencia = dt_contratacao
        dt_inicio_vigencia = txtIniVigencia.Text
    Else
        dt_inicio_vigencia = dt_endosso
    End If

    ' Recupera Numeros de Endosso e de Ordem da Lider, no caso de Cosseguros Aceitos (endossos).
    If CosseguroAceito And indEndosso Then

        SQL = "SELECT num_ordem_co_seguro_aceito, "
        SQL = SQL & "num_endosso_cosseguro "
        SQL = SQL & "FROM endosso_co_seguro_aceito_tb  WITH (NOLOCK)  "
        SQL = SQL & "WHERE proposta_id = " & Val(txtNumero.Text)
        SQL = SQL & "AND   endosso_id = " & Val(txtNumEndosso.Text)

        Set rc = rdocn.OpenResultset(SQL)

        If Not rc.EOF Then
            txtNumEndossoLider.Text = IIf(IsNull(rc!num_endosso_cosseguro) = False, rc!num_endosso_cosseguro, "    ")
            txtNumOrdemEndossoLider.Text = IIf(IsNull(rc!num_ordem_co_seguro_aceito) = False, rc!num_ordem_co_seguro_aceito, "    ")
        End If

        Set rc = Nothing

    End If

    Call InicializarGridSubGrupos

    'lpinto/acintra 20/09/2005
    bPropostaRecusada = False
    'eduardo.amaral(Nova Consultoria) 23-02-2012 Demanda: BB SEGURO VIDA EMPRESA FLEX
    Dim Situacao As String
    If SelPropBasica.GridProposta.Rows = 2 Then
        Situacao = SelPropBasica.GridProposta.TextMatrix(1, 7)
        If Situacao = "Recusada" Or Situacao = "Carta Recusa Emitida" Then
            ' cmdImprimir.Enabled = False
            bPropostaRecusada = True
        End If
    Else
        Situacao = SelPropBasica.GridProposta.TextMatrix(SelPropBasica.GridProposta.Row, 7)
        If Situacao = "Recusada" Or Situacao = "Carta Recusa Emitida" Then
            'cmdImprimir.Enabled = False
            bPropostaRecusada = True
        Else
            bPropostaRecusada = False
        End If

    End If
    ' fim eduardo.amaral(Nova Consultoria)
    'rmaiellaro (19/01/2007)
    bPropostaEstudo = False
    If SelPropBasica.GridProposta.Rows = 2 Then
        If Left(SelPropBasica.GridProposta.TextMatrix(1, 7), 9) = "Em Estudo" Then
            cmdImprimir.Enabled = False
            bPropostaEstudo = True
        End If
    Else

        If Left(SelPropBasica.GridProposta.TextMatrix(SelPropBasica.GridProposta.Row, 7), 9) = "Em Estudo" Then
            cmdImprimir.Enabled = False
            bPropostaEstudo = True
        Else
            bPropostaEstudo = False
        End If

    End If

    ' Lpinto - 28/09/2005

    If bPropostaRecusada Then
        dt_inicio_vigencia = dt_contratacao
    End If

    dt_inicio_query = Format(dt_inicio_vigencia, "yyyymmdd")

    If bPropostaNaoEmitida Then
        Call LerPropostaNaoEmitida
        With SSTabProp
            .TabEnabled(TAB_SUBGRUPO) = False
            .TabEnabled(TAB_CORRETAGEM) = False
            .TabEnabled(TAB_AGENCIAMENTO) = False
            .TabEnabled(TAB_ADMINISTRACAO) = False
            .TabEnabled(TAB_PARAMETROS) = False
            .TabEnabled(TAB_CLAUSULA) = False
            .TabEnabled(TAB_SINISTRO) = False
            .TabEnabled(TAB_ENDOSSO) = False
            .TabEnabled(TAB_COBRANCA) = False
            .TabEnabled(TAB_COSSEGURO) = False
            .TabEnabled(TAB_RESSEGURO) = False
            .TabEnabled(TAB_ASSISTENCIA) = False
            .TabEnabled(TAB_VIDAS_SEGURADAS) = False
            .TabEnabled(TAB_EXTRATO_SEGURO) = False
            .TabEnabled(TAB_CERTIFICADOS) = False
            .TabEnabled(TAB_HISTORICO) = False
            .TabEnabled(TAB_QUESTIONARIO) = False    'Sergey Souza - 10/09/2010
        End With
    Else
        Ler_SubGrupos
        Ler_Proposta_Fechada
        Ler_Proposta_Basica_Vida
        Ler_Cliente
        'INICIO  'alterado 12/09/2013 projeto 17860335 - Melhorias no SEGBR MC_Amaral -confitec
        Ler_Cosseguro
        ' Se a rotina Ler_Cosseguro achou algum registro de cosseguro repassado,
        'ent�o n�o precisa ir na Ler_Cosseguro_Aceito, pois elas s�o exclusivas (ou � um ou � o outro).
        If SSTabProp.TabEnabled(TAB_COSSEGURO) = False Then
            Ler_Cosseguro_Aceito
        End If
        'Fim   'alterado 12/09/2013 projeto 17860335 - Melhorias no SEGBR MC_Amaral -confitec
        Ler_Resseguro
        Ler_Dados_Corretor

        Ler_Administracao

        'lrocha 29/10/2008 - Demanda 273032
        'Ler_Dados_Assistencia
        '----------------------------------

        '' Comentado at� ser liberado para testes
        'Ler_Agenciamento
        ''
        '
        Ler_Seguro_Vida
        Ler_Beneficiario
        Ler_ExtratoSeguro

        'INICIO  'alterado 12/09/2013 projeto 17860335 - Melhorias no SEGBR MC_Amaral -confitec
        ' Excluir a rotina Ler_Produto, e j� trazer as
        'informa��es do produto atrav�s da primeira query, que j� acessa o produto pela proposta_tb.
        'Ler_Produto
        txtProduto.Text = Trim(vProdutoId & " - " & vNomeProd)
        'FIM
        Ler_Coberturas
        Ler_Clausulas
        'INICIO  'alterado 12/09/2013 projeto 17860335 - Melhorias no SEGBR MC_Amaral -confitec
        Ler_Endosso_Proposta
        Ler_Avaliacao
        'fim   'alterado 12/09/2013 projeto 17860335 - Melhorias no SEGBR MC_Amaral -confitec
        'Pcarvalho 05/10/2010 - Obter o percentual de Remunera��o BB
        Ler_Remuneracao_Servico

        'Pcarvalho 05/10/2010 - Obter assist�ncia facultativa
        'Pcarvalho 23/11/2010 - Retirado a pedido do usu�rio, pois j� tem
        'aba espec�fica tratando as assist�ncias
        'Ler_Assistencia

        'RRAMOS - 2007/06/26 - Flow 220222 ----------------------------------------------------------------------------
        'Estas linhas obrigat�riamente devem estar antes da sub Ler_Cobraca.
        'lngLinhasGridCobranca -> Esta vari�vel armazena a quantidade m�xima de linhas que ser�o mostradas no grid.
        '                         Se precisar aumentar ou diminuir o limite � s� alterar o seu valor, lembrando-se
        '                         que quanto menor for este valor, mais r�pido o grid ser� carregado.
        '                         Defini como 100 o limite pois aproximadamente 97% das propostas possuem menos de
        '                         100 faturas.

        lngLinhasGridCobranca = 100
        lngLinMIN = 1
        lngLinMAX = lngLinhasGridCobranca
        cmdPagina(0).ToolTipText = "Visualizar as " & lngLinhasGridCobranca & " primeiras faturas."
        cmdPagina(1).ToolTipText = "Visualizar as " & lngLinhasGridCobranca & " faturas anteriores."
        cmdPagina(2).ToolTipText = "Visualizar as " & lngLinhasGridCobranca & " pr�ximas faturas."
        cmdPagina(3).ToolTipText = "Visualizar as " & lngLinhasGridCobranca & " �ltimas faturas."
        cmdPagina(0).Enabled = False
        cmdPagina(1).Enabled = False
        cmdPagina(2).Enabled = False
        cmdPagina(3).Enabled = False
        '--------------------------------------------------------------------------------------------------------------


        Ler_Cobranca
        ''INICIO  'alterado 12/09/2013 projeto 17860335 - Melhorias no SEGBR MC_Amaral -confitec
        'Ler_Endosso_Proposta - Executar a rotina Ler_Endosso_Proposta antes da rotina Ler_Avaliacao

        Ler_Sinistro
        '
        Ler_Total_Vidas

        ''pcarvalho - 20/10/2005
        Call CarregarHistoricoProposta

        ''Sergey Souza - 10/09/2010
        ' If vProdutoId = 1198 Then

        ' Autor: pablo.cardoso (Nova Consultoria)
        ' Data da Altera��o: 09/11/2011
        ' Projeto: 1236945 - BB Seguro Vida Flex
        ' Altera��o da condicional para exibir question�rio de proposta ALS
        If (bPropostaALS = True) Then
            Call Ler_Questionario
        Else
            SSTabProp.TabEnabled(TAB_QUESTIONARIO) = False
        End If

        'Renato Vasconcelos
        'Flow 14735947
        Ler_Tipo_Segurado

    End If
    
    txtTp_Documento.Text = VerificaFluxo(Val(vPropostaId))
    bPossui_Fluxo_Digital = PossuiFluxoDigital(Val(vProdutoId))
    
    lblTp_Documento.Visible = bPossui_Fluxo_Digital
    txtTp_Documento.Visible = bPossui_Fluxo_Digital

    '*** Inclus�o do Sinalizado de PPE - Pessoas politicamente expostas - demanda 269994
    '*** 23/09/2008 - Fabio (Stefanini)
    If IdentificaPPE(txtCPF_CGC.Text) Then Me.chkPPE.Value = 1 Else Me.chkPPE.Value = 0

    gridSubGrupoAssist.ColWidth(0) = 0
    txtDtEndosso.Text = dt_endosso

    If Trim(vSegCliente) <> "" Then
        Me.Caption = "SEGP0223 - Consulta Proposta / Ap�lice Vida Avulso - " & Ambiente & " - " & vSegCliente     'Rmarins - 05/01/2006
    Else
        Me.Caption = "SEGP0223 - Consulta Proposta / Ap�lice Vida Avulso - " & Ambiente
    End If

    ConPropBasicaVida.Refresh
    '
    '

    'Luciana - 07/11/2003
    SSTabProp.Tab = 0
    'afonso
    Check1.Visible = False
    cmdImprimir.Visible = True
    cmdImprimir.Enabled = True
    cboDestino.Enabled = False
    cboDestino.Visible = False
    visualizar.Visible = False
    visualizar.Enabled = False
    ImprimirTela = False
    '
    'Zoro.Gomes - Confitec - 23/05/2014
    'Montando gride de carta de cobran�a
    If SelPropBasica.cboTipoSelecao.Text <> "CNPJ" And SelPropBasica.cboTipoSelecao.Text <> "Raz�o Social" Then
        Monta_Gride_Carta_Cobranca
    End If
    MousePointer = vbDefault

    Exit Sub
Erro:
    MousePointer = vbDefault
    '
    TrataErroGeral "Erro ao carregar a tela de Consulta"
    MsgBox "Erro ao carregar a tela de Consulta"
    'Resume Next
    End
End Sub
Public Function VerificarPropostaALS(ByVal propostaId As Long) As Boolean

    Dim SQL As String
    Dim rc As rdoResultset

    On Error GoTo Erro

    SQL = SQL & "SELECT proposta_tb.proposta_id " & vbNewLine
    SQL = SQL & "  FROM proposta_tb WITH (NOLOCK)  " & vbNewLine
    SQL = SQL & " WHERE proposta_id = " & propostaId
    SQL = SQL & "  AND origem_proposta_id = 2"

    Set rc = rdocn.OpenResultset(SQL)

    If rc.EOF Then
        VerificarPropostaALS = False
    Else
        VerificarPropostaALS = True
    End If

    Exit Function

Erro:
    MousePointer = vbDefault
    '
    TrataErroGeral "Erro ao carregar a tela de Consulta"
    MsgBox "Erro ao carregar a tela de Consulta"
    'Resume Next
End Function

Private Sub Ler_Cosseguro_Aceito()
    Dim rc As rdoResultset
    Dim SQL As String

    On Error GoTo Erro

    'lpinto
    If Not bPropostaRecusada And Not bPropostaNaoEmitida And Not bPropostaEstudo Then    'rmaiellaro (19/01/2007)
        SQL = "SELECT a.seg_cod_susep_lider, a.sucursal_seg_lider, s.nome"
        SQL = SQL & ", c.num_apolice_lider, c.dt_emissa_apolice_lider, c.num_ordem_co_seguro_aceito "
        SQL = SQL & "FROM apolice_terceiros_tb a  WITH (NOLOCK) , co_seguro_aceito_tb c  WITH (NOLOCK) , seguradora_tb s  WITH (NOLOCK) "
        SQL = SQL & "WHERE a.apolice_id = " & vApoliceId
        SQL = SQL & " AND a.ramo_id = " & vRamoId
        SQL = SQL & " AND a.seguradora_cod_susep = " & vSeguradoraId
        SQL = SQL & " AND a.sucursal_seguradora_id = " & vSucursalId
        SQL = SQL & " AND c.num_ordem_co_seguro_aceito = a.num_ordem_co_seguro_aceito "
        SQL = SQL & " AND c.sucursal_seg_lider = a.sucursal_seg_lider "
        SQL = SQL & " AND c.seg_cod_susep_lider = a.seg_cod_susep_lider "
        SQL = SQL & " AND s.seguradora_cod_susep = a.seg_cod_susep_lider "

        Set rc = rdocn.OpenResultset(SQL)

        If Not rc.EOF Then
            vAceito = True
            TabSeguro = "seguro_vida_aceito_tb"
            TabEscolha = "escolha_tp_cob_vida_aceito_tb"
            '
            If Not IsNull(rc!num_ordem_co_seguro_aceito) Then
                TxtNumeroOrdem.Text = rc!num_ordem_co_seguro_aceito
            End If
            '
            If Not IsNull(rc!dt_emissa_apolice_lider) Then
                txtDtEmissaoLider.Text = Format(rc!dt_emissa_apolice_lider, "dd/mm/yyyy")
            End If
            '
            If Not IsNull(rc!Nome) Then
                txtSeguradoraLider.Text = Trim(rc!Nome)
            End If
            '
            If Not IsNull(rc!num_apolice_lider) Then
                txtApoliceLider.Text = rc!num_apolice_lider
            End If
        Else
            vAceito = False
            TabSeguro = "seguro_vida_tb"
            TabEscolha = "escolha_tp_cob_vida_tb"
        End If
        rc.Close
        Set rc = Nothing
    End If

    Exit Sub
Erro:
    TrataErroGeral "Ler Cosseguro Aceito"
    MsgBox "Rotina: Ler Cosseguro Aceito"
End Sub

Private Sub Ler_Coberturas()
    Dim rc As rdoResultset
    Dim vComponente As Variant
    Dim SQL As String, linha As String
    Dim vSubGrupo As SubGrupo
    Dim bolAchou As Boolean
    Dim lIntEndossoCobert As Integer

    On Error GoTo Erro
    'lpinto

    'Sergey Souza - 10/09/2010 =======================
    'Default
    bolAchou = False

    If Not bPropostaRecusada And Not bPropostaNaoEmitida And Not bPropostaEstudo Or vProdutoId = 1198 Then    'rmaiellaro (19/01/2007)
        bolAchou = True
        If vProdutoId = 1198 Then
            TabEscolha = "escolha_tp_cob_vida_tb"
        End If
    End If
    ' =======================

    Dim lBlnFlg As Boolean

    SQL = "SELECT MAX(ISNULL(COR.ENDOSSO_ID, 0)) AS ENDOSSO_ID "
    SQL = SQL & "FROM COBERT_OBJ_RISCO_TB AS COR WITH (NOLOCK) "
    SQL = SQL & "WHERE 1=1 "
    SQL = SQL & "AND COR.PROPOSTA_ID = " & vPropostaId & " "

    Set rc = rdocn.OpenResultset(SQL)

    lBlnFlg = False

    If IsNull(rc(0)) Then
        lBlnFlg = False
    Else
        If rc!endosso_id > 0 Then
            lBlnFlg = True
        Else
            lBlnFlg = False
        End If
    End If

    rc.Close

    Set rc = Nothing

    If bolAchou Then
        If SubGrupos.Count = 0 Then
                'IM00716805 - LEANDRO AMARAL - CONFITEC - 20/03/2019
            SQL = "SELECT MAX(COALESCE(ENDOSSO_ID, 0)) AS ENDOSSO_ID FROM " & TabEscolha & " WHERE PROPOSTA_ID = " & vPropostaId & " "
            
                        'CONFITEC SP - IM00865761 - LUCIANO.SOUSA - 22/07/2019 - INICIO
                        SQL = SQL & "AND (ENDOSSO_ID <= " & Val(Me.txtNumEndosso.Text) & " OR ENDOSSO_ID IS NULL)"
                        'CONFITEC SP - IM00865761 - LUCIANO.SOUSA - 22/07/2019 - FIM
                        
            Set rc = rdocn.OpenResultset(SQL)
            
            lIntEndossoCobert = 0
            If Not rc.EOF Then
                'INICIO CONFITEC - 27/05/2020 - IM01362516
                'lIntEndossoCobert = rc(0)
                lIntEndossoCobert = IIf(IsNull(rc(0)), 0, rc(0))
                'FIM CONFITEC - 27/05/2020 - IM01362516
            End If
            
            rc.Close
            Set rc = Nothing
                'IM00716805 - LEANDRO AMARAL - CONFITEC - 20/03/2019
                
            'Busca o total de IS para todas as coberturas
            '' S� � v�lido para ap�lices s/ subgrupo
            SQL = "SELECT SUM(val_is) FROM " & TabEscolha
            SQL = SQL & "  WITH (NOLOCK)    WHERE proposta_id = " & vPropostaId
            SQL = SQL & " AND prop_cliente_id = " & vClienteId
            '
            'Demanda MU-2018-002920 - Vig�ncia e endossos 1206 - BB Seguro Vida Empresa FLEX
            'Verifica se � do produto 1206 para ajuste na exibi��o da IS para endosso ou contrata��o
            If vProdutoId <> 1206 Then
                SQL = SQL & " AND dt_inicio_vigencia_esc <= '" & dt_inicio_query & "'"
                SQL = SQL & " AND (dt_fim_vigencia_esc is null "
                SQL = SQL & "      OR dt_fim_vigencia_esc >= '" & dt_inicio_query & "')"
            Else
                If indEndosso Then
                        'IM00716805 - LEANDRO AMARAL - CONFITEC - 20/03/2019
                    'SQL = SQL & " AND endosso_id = " & txtNumEndosso.Text & ""
                    SQL = SQL & " AND endosso_id IN (" & txtNumEndosso.Text & ", " & lIntEndossoCobert & " )"
                    'IM00716805 - LEANDRO AMARAL - CONFITEC - 20/03/2019
                Else
                    SQL = SQL & " AND endosso_id IS NULL"
                End If
            End If
            '
            'Jessica.Adao - Confitec Sistemas - 20181019 - Sala �gil - Sprint 10: Ajustes Amparo
            If vProdutoId = 1217 Then
                SQL = ObterISAmparo(CLng(vPropostaId))
            End If

            Set rc = rdocn.OpenResultset(SQL)
            '
            If IsNull(rc(0)) Then
                txtLimMaxInd.Text = vValZero
            Else
                If ConfiguracaoBrasil Then
                    txtLimMaxInd.Text = Format(Val(rc(0)), "###,###,###,##0.00")
                Else
                    txtLimMaxInd.Text = TrocaValorAmePorBras(Format(Val(rc(0)), "###,###,###,##0.00"))
                End If
            End If
            '
            rc.Close
            Set rc = Nothing
            '
            'SQL = "SELECT DISTINCT e.tp_componente_id , t.tp_cob_comp_id, c.nome, e.val_is, e.val_premio "
            '            SQL = "SELECT DISTINCT e.tp_componente_id , c.tp_cobertura_id tp_cob_comp_id, c.nome, e.val_is, e.val_premio "
            '            SQL = SQL & ", e.dt_inicio_vigencia_esc , e.dt_fim_vigencia_esc "
            '            SQL = SQL & ", isnull(e.val_taxa, 0) val_taxa, isnull(e.perc_basica, 0) perc_basica "
            '            If Not vAceito Then
            '                SQL = SQL & ", e.class_tp_cobertura class_tp_cobertura "
            '            Else
            '                SQL = SQL & ", 'a' class_tp_cobertura "
            '            End If
            '            '
            '            SQL = SQL & "FROM " & TabEscolha & " e   WITH (NOLOCK) , tp_cobertura_tb c    WITH (NOLOCK) , tp_cob_comp_tb t  WITH (NOLOCK) "
            '
            '            '*** Abosco @ 2003 abr 24
            '            '*** inclusao de inicio de vigencia em tp_cob_comp_item_tb
            '            SQL = SQL & ", tp_cob_comp_item_tb i "
            '
            '            SQL = SQL & "WHERE  e.proposta_id = " & vPropostaId
            '            SQL = SQL & " AND   t.tp_cob_comp_id = e.tp_cob_comp_id "
            '            SQL = SQL & " AND   c.tp_cobertura_id = t.tp_cobertura_id "
            '            '
            '            SQL = SQL & " AND e.dt_inicio_vigencia_esc <= '" & dt_inicio_query & "'"
            '            SQL = SQL & " AND (e.dt_fim_vigencia_esc is null "
            '            SQL = SQL & "      OR e.dt_fim_vigencia_esc >= '" & dt_inicio_query & "')"
            '            '
            '            '*** Abosco @ 2003 abr 24
            '            '*** inclusao de inicio de vigencia em tp_cob_comp_item_tb
            '            SQL = SQL & " AND i.tp_cob_comp_id = e.tp_cob_comp_id "
            '            SQL = SQL & " AND i.dt_inicio_vigencia_comp = e.dt_inicio_vigencia_comp "
            '            'SQL = SQL & " AND i.dt_fim_vigencia_comp IS NULL "
            '            '
            '            SQL = SQL & " ORDER BY e.tp_componente_id, class_tp_cobertura "
            '

            '################################################################################
            SQL = "SELECT DISTINCT e.tp_componente_id , c.tp_cobertura_id tp_cob_comp_id, c.nome, e.val_is, e.val_premio " & vbNewLine
            SQL = SQL & ", e.dt_inicio_vigencia_esc , e.dt_fim_vigencia_esc " & vbNewLine
            SQL = SQL & ", isnull(e.val_taxa, 0) val_taxa, isnull(e.perc_basica, 0) perc_basica " & vbNewLine
            If Not vAceito Then
                SQL = SQL & ", e.class_tp_cobertura class_tp_cobertura " & vbNewLine
            Else
                SQL = SQL & ", 'a' class_tp_cobertura " & vbNewLine
            End If
            '
            '+------------------------------------------------------------
            '|INICIO Demanda 18334512 Circular 491 - Petrauskas 02/2015
            SQL = SQL & "  ,cobert_obj_risco_tb.val_premio_tarifa " & vbNewLine
            SQL = SQL & "  ,cobert_obj_risco_tb.val_premio_tarifa_sem_custo_assist_facul as val_premio_liq " & vbNewLine
            SQL = SQL & "  ,val_is_tarifa = isnull(cobert_obj_risco_tb.val_is,0) " & vbNewLine
            '|FIM    Demanda 18334512 Circular 491 - maria 02/2015
            '+------------------------------------------------------------

            SQL = SQL & "FROM " & TabEscolha & " e   WITH (NOLOCK)  " & vbNewLine
            SQL = SQL & "join tp_cob_comp_item_tb i   WITH (NOLOCK) " & vbNewLine
            SQL = SQL & "  on i.tp_cob_comp_id = e.tp_cob_comp_id " & vbNewLine
            SQL = SQL & " AND i.dt_inicio_vigencia_comp = e.dt_inicio_vigencia_comp" & vbNewLine
            SQL = SQL & " join tp_cob_comp_tb t  WITH (NOLOCK) " & vbNewLine
            SQL = SQL & " on t.tp_cob_comp_id = e.tp_cob_comp_id " & vbNewLine
            SQL = SQL & " join tp_cobertura_tb c    WITH (NOLOCK) " & vbNewLine
            SQL = SQL & " on c.tp_cobertura_id = t.tp_cobertura_id  " & vbNewLine

            '+------------------------------------------------------------
            '|INICIO Demanda 18334512 Circular 491 - Maria 19/2015
            SQL = SQL & " LEFT JOIN cobert_obj_risco_tb WITH (NOLOCK)  " & vbNewLine
            SQL = SQL & "   ON e.proposta_id = cobert_obj_risco_tb.proposta_id  " & vbNewLine
            SQL = SQL & "   AND t.tp_cobertura_id = cobert_obj_risco_tb.tp_cobertura_id  " & vbNewLine
            SQL = SQL & "   AND cobert_obj_risco_tb.tp_componente_id = e.tp_componente_id " & vbNewLine

            'CONFITEC SP - IM00865761 - LUCIANO.SOUSA - 22/07/2019 - INICIO
            'CONFITEC SP - IM00865761 - LUCIANO.SOUSA - 22/07/2019 - INICIO
            If lBlnFlg Then
                SQL = SQL & "   AND isnull(cobert_obj_risco_tb.endosso_id, 0) = (SELECT MAX(isnull(endosso_id, 0))  " & vbNewLine
                SQL = SQL & "                                           FROM cobert_obj_risco_tb WITH (NOLOCK)  " & vbNewLine
                SQL = SQL & "                                           WHERE proposta_id = e.proposta_id  " & vbNewLine
                SQL = SQL & "                                           AND (endosso_id <= " & Val(txtNumEndosso.Text) & " OR endosso_id is null ))" & vbNewLine
            Else
                SQL = SQL & "   AND isnull(cobert_obj_risco_tb.OBJ_RISCO_ID, 0) = (SELECT MAX(isnull(OBJ_RISCO_ID, 0))  " & vbNewLine
                SQL = SQL & "                                           FROM cobert_obj_risco_tb WITH (NOLOCK)  " & vbNewLine
                SQL = SQL & "                                           WHERE proposta_id = e.proposta_id)  " & vbNewLine
            End If
            'CONFITEC SP - IM00865761 - LUCIANO.SOUSA - 22/07/2019 - FIM
                        
                        
            ''|FIM    Demanda 18334512 Circular 491 - Maria 19/2015
            ''+------------------------------------------------------------


            SQL = SQL & "WHERE  e.proposta_id = " & vPropostaId & vbNewLine
            'Demanda MU-2018-002920 - Vig�ncia e endossos 1206 - BB Seguro Vida Empresa FLEX
            'Verifica se � do produto 1206 para ajuste na exibi��o dos dados da aba 'Coberturas/Pr�mio' para cada endosso ou para a contrata��o
            If vProdutoId <> 1206 Then
                SQL = SQL & " AND e.dt_inicio_vigencia_esc <= '" & dt_inicio_query & "'" & vbNewLine
                SQL = SQL & " AND (e.dt_fim_vigencia_esc is null " & vbNewLine
                SQL = SQL & "      OR e.dt_fim_vigencia_esc >= '" & dt_inicio_query & "')" & vbNewLine
            Else
                If indEndosso Then
                        'IM00716805 - LEANDRO AMARAL - CONFITEC - 20/03/2019
                    'SQL = SQL & " AND e.endosso_id = " & txtNumEndosso.Text & ""
                    SQL = SQL & " AND E.endosso_id IN (" & txtNumEndosso.Text & ", " & lIntEndossoCobert & " )"
                    'IM00716805 - LEANDRO AMARAL - CONFITEC - 20/03/2019
                Else
                    SQL = SQL & " AND e.endosso_id IS NULL"
                End If
            End If

            SQL = SQL & " ORDER BY e.tp_componente_id, class_tp_cobertura " & vbNewLine
            '################################################################################

            'Jessica.Adao - Confitec Sistemas - 20181019 - Sala �gil - Sprint 10: Ajustes Amparo
            If vProdutoId = 1217 Then
                SQL = ObterDadosCoberturasAmparo(CLng(vPropostaId))
            End If

            Set rc = rdocn.OpenResultset(SQL)

            If Not rc.EOF Then
                vComponente = rc!tp_componente_id
                '
                Call Monta_Colecao_Coberturas(rc)
                '
                Call Monta_GridCoberturas(vComponente)

            End If
            '
        Else
            txtLimMaxInd.Text = vValZero
            '
            For Each vSubGrupo In SubGrupos

                SQL = "SELECT DISTINCT t.tp_componente_id"
                'abazo 25/03/2004
                'SQL = SQL & ", t.tp_cob_comp_id, c.nome, e.val_is, e.val_premio "
                SQL = SQL & ", c.tp_cobertura_id, t.tp_cob_comp_id, c.nome, e.val_is, e.val_premio " & vbNewLine
                SQL = SQL & ", isnull(e.val_taxa, 0) val_taxa , isnull(e.perc_basica, 0) perc_basica" & vbNewLine
                SQL = SQL & ", isnull(e.class_tp_cobertura, 'a') class_tp_cobertura " & vbNewLine
                SQL = SQL & ", isnull(e.val_min_franquia, 0) val_min_franquia, isnull(e.fat_franquia, 0) fat_franquia, isnull(e.texto_franquia, '') texto_franquia" & vbNewLine
                SQL = SQL & ", isnull(e.val_lim_min_is, 0) val_lim_min_is, isnull(e.val_lim_max_is, 0) val_lim_max_is " & vbNewLine
                SQL = SQL & ", isnull(e.carencia_dias, 0) carencia_dias, isnull(e.lim_diaria_dias, 0) lim_diaria_dias " & vbNewLine

                '+------------------------------------------------------------
                '|INICIO Demanda 18334512 Circular 491 - maria 19/2014
                SQL = SQL & ", cobert_obj_risco_tb.val_premio_tarifa " & vbNewLine
                SQL = SQL & ", cobert_obj_risco_tb.val_premio_tarifa_sem_custo_assist_facul as val_premio_liq" & vbNewLine
                SQL = SQL & ", val_is_tarifa = isnull(cobert_obj_risco_tb.val_is,0)" & vbNewLine
                '|FIM    Demanda 18334512 Circular 491 - maria 19/2014
                '+------------------------------------------------------------

                SQL = SQL & " FROM escolha_sub_grp_tp_cob_comp_tb e  WITH (NOLOCK)  " & vbNewLine
                SQL = SQL & " INNER JOIN tp_cob_comp_tb t  WITH (NOLOCK)  " & vbNewLine
                SQL = SQL & "   ON  t.tp_cob_comp_id = e.tp_cob_comp_id " & vbNewLine
                '*** Abosco @ 2003 abr 24
                '*** inclusao de inicio de vigencia em tp_cob_comp_item_tb
                SQL = SQL & " INNER JOIN tp_cob_comp_item_tb i  WITH (NOLOCK)  " & vbNewLine
                SQL = SQL & "    ON i.tp_cob_comp_id = e.tp_cob_comp_id " & vbNewLine
                SQL = SQL & "   AND i.dt_inicio_vigencia_comp = e.dt_inicio_vigencia_comp " & vbNewLine
                'SQL = SQL & "   AND i.dt_fim_vigencia_comp IS NULL "
                SQL = SQL & " INNER JOIN tp_cobertura_tb c     WITH (NOLOCK) " & vbNewLine
                SQL = SQL & "   ON  c.tp_cobertura_id = t.tp_cobertura_id " & vbNewLine
                '+------------------------------------------------------------
                '|INICIO Demanda 18334512 Circular 491 - maria 19/2015
                SQL = SQL & "       JOIN apolice_tb ap WITH (NOLOCK) " & vbNewLine
                SQL = SQL & "         ON e.apolice_id = ap.apolice_id " & vbNewLine
                SQL = SQL & "        AND e.ramo_id = ap.ramo_id " & vbNewLine
                SQL = SQL & "  LEFT JOIN cobert_obj_risco_tb WITH (NOLOCK) " & vbNewLine
                SQL = SQL & "         ON ap.proposta_id = cobert_obj_risco_tb.proposta_id " & vbNewLine
                SQL = SQL & "        AND t.tp_cobertura_id = cobert_obj_risco_tb.tp_cobertura_id " & vbNewLine
                SQL = SQL & "       AND cobert_obj_risco_tb.tp_componente_id = t.tp_componente_id " & vbNewLine

                If lBlnFlg Then
                    SQL = SQL & "        AND isnull(cobert_obj_risco_tb.endosso_id, 0) = (SELECT MAX(isnull(endosso_id, 0)) " & vbNewLine
                Else
                    SQL = SQL & "        AND isnull(cobert_obj_risco_tb.OBJ_RISCO_ID, 0) = (SELECT MAX(isnull(OBJ_RISCO_ID, 0)) " & vbNewLine
                End If

                SQL = SQL & "                                              FROM cobert_obj_risco_tb WITH (NOLOCK) " & vbNewLine
                SQL = SQL & "                                              WHERE proposta_id = ap.proposta_id) " & vbNewLine
                '|FIM    Demanda 18334512 Circular 491 - maria 19/2015
                '+------------------------------------------------------------

                SQL = SQL & "WHERE e.apolice_id = " & vApoliceId
                SQL = SQL & " AND e.sucursal_seguradora_id = " & vSucursalId
                SQL = SQL & " AND e.seguradora_cod_susep = " & vSeguradoraId
                SQL = SQL & " AND e.ramo_id = " & vRamoId
                SQL = SQL & " AND e.sub_grupo_id = " & vSubGrupo.Id
                SQL = SQL & " AND AP.apolice_id = " & vApoliceId
                SQL = SQL & " AND AP.sucursal_seguradora_id = " & vSucursalId
                SQL = SQL & " AND AP.seguradora_cod_susep = " & vSeguradoraId
                SQL = SQL & " AND AP.ramo_id = " & vRamoId
                SQL = SQL & " ORDER BY t.tp_componente_id, isnull(e.class_tp_cobertura, 'a') DESC "
                'SQL = SQL & " ORDER BY t.tp_componente_id, e.class_tp_cobertura DESC "
                '
                Set rc = rdocn.OpenResultset(SQL)

                If Not rc.EOF Then
                    '
                    Call Monta_Colecao_Coberturas_SubGrupo(rc)
                    '
                    Set vSubGrupo.Coberturas = Coberturas
                    '
                    rc.Close
                End If
                '
                Set rc = Nothing
            Next
        End If    ' SubGrupos
    End If
    Exit Sub
Erro:
    TrataErroGeral "Ler Coberturas"
    MsgBox "Rotina: Ler Coberturas, erro na estrutura dos dados o programa ser� abortado " _
         , vbCritical, Me.Caption
    End
End Sub

Private Sub Zera_Campos()
    entrada = 0
    txtNumero.Text = ""
    txtProduto.Text = ""
    txtRamo.Text = ""
    '
    txtPropostaBB.Text = ""
    txtDtContratacao.Text = ""
    txtStatus.Text = ""
    txtPropAnterior.Text = ""
    '
    txtApolice.Text = ""
    txtNumEndosso.Text = ""
    txtDtEndosso.Text = ""
    txtIniVigencia.Text = ""
    txtFimVigencia.Text = ""
    '
    txtDtEmissao.Text = ""
    txtLimMaxInd.Text = ""
    '    mskLimRetResseguro.Text = ""
    '    mskLimRetResseguro.Mask = "15,2"
    '    txtNumEndossoLider.Text = ""
    '    txtNumOrdemEndossoLider.Text = ""
    '    '
    '    TxtNumeroOrdem.Text = ""
    '    txtDtEmissaoLider.Text = ""
    '    txtSeguradoraLider.Text = ""
    '    txtApoliceLider.Text = ""
    '    '
    '    txtCodCliente.Text = ""
    '    txtNome.Text = ""
    '    txtEndereco.Text = ""
    '    TxtBairro.Text = ""
    '
    '    txtCidade.Text = ""
    '    txtDDD.Text = ""
    '    txtTelefone.Text = ""
    '    txtUF.Text = ""
    '    txtCEP.Text = ""
    '
    '    txtTpPessoaId.Text = ""
    '    txtTpPessoa.Text = ""
    '    txtCPF_CGC.Text = ""
    '    txtNascimento.Text = ""
    '    ''
    '    TxtComissao.Text = ""
    '    ''
    '    TxtCodAgenciador.Text = ""
    '    TxtAgenciador.Text = ""
    '    TxtPercAgPrim.Text = ""
    '    TxtFPAgPrim.Text = ""
    '    TxtNoParcAgPrim.Text = ""
    '    TxtPercAgDemais.Text = ""
    '    TxtFPAgDemais.Text = ""
    '    TxtNoParcAgDemais.Text = ""
    '    ''
    '    txtCodAgencia.Text = ""
    '    txtNomeAgencia.Text = ""
    '    txtBanco.Text = ""
    '    txtCodAgDebito.Text = ""
    '    txtNomeAgDebito.Text = ""
    '    txtContaCorrente.Text = ""
    '    TxtBeneficiario.Text = ""
    '    ''
    '    txtSeg2.Text = ""
    '
    '    txtSubRamoId.Text = ""
    '    txtSubRamo.Text = ""
    '    TxtTbCalcId.Text = ""
    '    TxtTbCalculo.Text = ""
    '    txtQtdVida.Text = ""
    '    ''
    '    txtTotalPremioLiq.Text = ""
    '    txtDescProg.Text = ""
    '    txtBonus.Text = ""
    '    txtLocal.Text = ""
    '    txtOutro.Text = ""
    '    txtCoefAjuste.Text = ""
    '    txtTotalDescontos.Text = ""
    '    txtIOF.Text = ""
    '    txtCustoApolice.Text = ""
    '    txtPremioBruto.Text = ""
    '    '
    '    txtNumParcelas.Text = ""
    '    TxtTaxaJuros.Text = ""
    '    txtValJuros.Text = ""
    '    txtPagoAto.Text = ""
    '    txtDemaisParcelas.Text = ""
    '    txtCodFormaPg.Text = ""
    '    txtFormaPg.Text = ""
    '    txtDiaDebito.Text = ""
    '    txtValTotalParc.Text = ""
    '
    '    txtVidasAtivas.Text = 0
    '    txtVidasInativas.Text = 0
    '    txtVidasTotal = 0
    '    txtVidasAtivas.Locked = True
    '    txtVidasInativas.Locked = True
    '    txtVidasTotal.Locked = True
    '
    '    '
    '    TabSeguro = ""
    '    TabEscolha = ""
    '    '
    '    vSeguradoraId = Empty
    '    vSucursalId = Empty
    '    vRamoId = Empty
    '    vApoliceId = Empty
    '    vPropostaId = Empty
    '    vProdutoId = Empty
    '    vClienteId = Empty
    '    '
    '    vAceito = False

End Sub

Public Sub Ler_Cobranca()
    Dim rc As rdoResultset
    Dim rc1 As rdoResultset
    Dim ValorTotParc As String
    Dim SQL As String
    Dim linha As String
    Dim ValCobranca As String
    Dim ValIOF As String
    Dim ValPago As String
    Dim vNossoNumero As String
    Dim NumEndosso As String
    Dim NumEndossoLider As String
    Dim sql1 As String
    Dim dt_recebimento As String
    Dim auxsel As String
    Dim auxorder As String
    Dim vFatura As Fatura
    Dim vCobranca As Cobranca
    Dim sServidor As String
    ''
    On Error GoTo Erro

    ' Autor: pablo.cardoso (Nova Consultoria)
    ' Data da Altera��o: 09/09/2011
    ' Demanda: 11427194 - Altera��o da Gera��o de Fatura CDC Carteira
    ' inser��o dos campos 'capital segurado' e 'qtde registros' quando o produto for 1140

    'Renato Vasconcelos
    'Demanda MU-2018-015339
    'Inicio

    'Criamos um novo campo por isso essa parte tamb�m precisa ser alterada
    '    If vProdutoId <> "1140" Then
    '        GridCobranca.ColWidth(14) = 0
    '        GridCobranca.ColWidth(15) = 0
    '    End If

    If vProdutoId <> "1140" Then
        GridCobranca.ColWidth(15) = 0
        GridCobranca.ColWidth(16) = 0
    End If


    If vProdutoId <> 1206 Then
        GridCobranca.ColWidth(11) = 0
        ' GridFaturas.FormatString = "Endosso|N� Cobran�a| Nosso N�mero               |Dt. Agendam.|Valor Cobrado| Valor IOF |Dt. Pagamento| Valor Pago | Situa��o                                                     | Ag�ncia | Conta Corrente | Retorno                                                      | Carta Emitida | Dt. Baixa           | Origem Baixa                             | Capital Segurado       | Qtde Registros      "
    End If
    'Fim
    'lpinto
    If Not bPropostaRecusada And Not bPropostaNaoEmitida And Not bPropostaEstudo Then    'rmaiellaro (19/01/2007)

        ValorTotParc = 0

        If SubGrupos.Count = 0 Then    ''And Trim(txtFimVigencia.Text) <> "" Then
            'GridCobranca.Visible = True
            'GridFaturas.Visible = False
            'FraCobranca.Caption = "Agendamento "
            '' Em vida as �nicas ap�lices que ter�o agendamento s�o as
            '' de vig�ncia fechada e sem subgrupo

            vSubGrupoId = -1    'Alterado por fpimenta / Imar�s 16/07/2003

            'Guilherme Cruz -- Confitec SISTEMAS -- Demanda Sala �gil -- inicio
            'Zoro.Gomes - Confitec - Circular 365 - Novos ajustes de formata��o e Codeguard - 24/10/2019
            If vProdutoId = 1183 Then ' CDC - Ex�rcito
                      SQL = "SELECT agem.DT_AGENDAMENTO AS DT_AGENDAMENTO" & vbNewLine
                SQL = SQL & "     , SUM(agem.VAL_COBRANCA) AS VAL_COBRADO" & vbNewLine
                SQL = SQL & "     , SUM(agem.VAL_IOF) AS VAL_IOF" & vbNewLine
                SQL = SQL & "     , agem.DT_RECEBIMENTO AS DT_PAGAMENTO" & vbNewLine
                SQL = SQL & "     , SUM(ISNULL(agem.VAL_PAGO,0)) AS VAL_PAGO" & vbNewLine
                SQL = SQL & "     , agem.SITUACAO " & vbNewLine
                SQL = SQL & "     , agem.DT_BAIXA AS DT_BAIXA" & vbNewLine
                SQL = SQL & "     , CASE agem.ORIGEM_BAIXA WHEN 'A' THEN 'AUTOM�TICO'  WHEN 'M' THEN 'MANUAL' ELSE '          ' END  AS ORIGEM_BAIXA" & vbNewLine
                SQL = SQL & "     , SUM(ades.VAL_IS) AS CAPITAL_SEGURADO" & vbNewLine
                SQL = SQL & "     , COUNT(1) AS QTD" & vbNewLine
                SQL = SQL & "  FROM SEGUROS_DB.DBO.AGENDAMENTO_COBRANCA_ATUAL_TB as agem WITH (NOLOCK)" & vbNewLine
                SQL = SQL & " INNER JOIN SEGUROS_DB.DBO.PROPOSTA_ADESAO_TB as ades WITH (NOLOCK) " & vbNewLine
                SQL = SQL & "    ON ades.PROPOSTA_ID = agem.PROPOSTA_ID" & vbNewLine
                SQL = SQL & " WHERE agem.APOLICE_ID = " & SelPropBasica.apolice & vbNewLine
                SQL = SQL & "   AND agem.SUCURSAL_SEGURADORA_ID = " & SelPropBasica.Sucursal & vbNewLine
                SQL = SQL & "   AND agem.SEGURADORA_COD_SUSEP = " & SelPropBasica.Seguradora & vbNewLine
                SQL = SQL & "   AND agem.RAMO_ID = " & SelPropBasica.Ramo & vbNewLine
                SQL = SQL & "   AND agem.FATURA_ID IS NULL" & vbNewLine
                SQL = SQL & " GROUP BY agem.DT_AGENDAMENTO, agem.DT_RECEBIMENTO, agem.SITUACAO, agem.DT_BAIXA, agem.ORIGEM_BAIXA" & vbNewLine
                SQL = SQL & " ORDER BY agem.DT_AGENDAMENTO"

                Set rc = rdoCn2.OpenResultset(SQL)
                    
                GridCobranca.Clear
                GridCobranca.Cols = 10
                GridCobranca.FormatString = "Dt Agendam.|Val Cobrado  |Val IOF|Dt Pagamento |Val Pago |Sit.|Dt Baixa       |Origem Baixa |Capital Segurado|Qtd "
                
                'Zoro.Gomes - Confitec - Circular 365 - Novos ajustes de formata��o e Codeguard - 24/10/2019
                GridCobranca.ColWidth(0) = 1000
                GridCobranca.ColWidth(1) = 1000
                GridCobranca.ColWidth(2) = 700
                GridCobranca.ColWidth(4) = 950
                GridCobranca.ColWidth(5) = 300
                GridCobranca.ColWidth(6) = 950
                GridCobranca.ColWidth(7) = 1100
                GridCobranca.ColWidth(8) = 1350
                GridCobranca.ColWidth(9) = 550
                'GridCobranca.Rows = 1
                        
                ' Guilherme Cruz
                Do While Not rc.EOF
                    linha = rc!dt_agendamento & vbTab
                    linha = linha & Format(Val(rc!VAL_COBRADO), "###,###,##0.00") & vbTab
                    linha = linha & Format(Val(rc!val_iof), "###,###,##0.00") & vbTab ' Format(Val(rc!VAL_IOF), "###,###,##0.00")
                    linha = linha & rc!DT_PAGAMENTO & vbTab  '
                    linha = linha & Format(Val(rc!val_pago), "###,###,##0.00") & vbTab '
                    linha = linha & UCase(rc!Situacao) & vbTab  '
                    linha = linha & rc!dt_baixa & vbTab '
                    linha = linha & rc!origem_baixa & vbTab '
                    linha = linha & Format(Val(rc!capital_segurado), "###,###,##0.00") & vbTab '
                    linha = linha & rc!QTD & vbTab '
    
                    GridCobranca.AddItem linha
                    rc.MoveNext
                Loop
                rc.Close
                Exit Sub
        End If ' Guilherme Cruz -- Confitec SISTEMAS -- Demanda Sala �gil -- Fim

            SQL = "SELECT a.num_endosso, a.num_parcela_endosso, a.num_cobranca"
            SQL = SQL & ", isnull(a.nosso_numero, 0) nosso_numero "
            SQL = SQL & ", isnull(a.nosso_numero_dv, 11) nosso_numero_dv "
            SQL = SQL & ", a.dt_agendamento, a.val_cobranca, a.dt_recebimento"
            SQL = SQL & ", a.val_pago, a.val_iof, desc_situacao = case "
            SQL = SQL & "       when a.situacao = 'a' then 'Adimplente'"

            '--egama - projeto SAP(confitec)
            SQL = SQL & "       WHEN agendamento_cobranca_cartao_atual_tb.proposta_id is not null and agendamento_cobranca_cartao_atual_tb.situacao_operadora in ('a', 'p') then 'Pagamento Realizado'"
            SQL = SQL & "       WHEN agendamento_cobranca_cartao_tb.proposta_id is not null and agendamento_cobranca_cartao_tb.situacao_operadora in ('a', 'p') then 'Pagamento Realizado'"
            SQL = SQL & "       WHEN agendamento_pendente_cartao_tb.proposta_id is not null and agendamento_pendente_cartao_tb.situacao_operadora in ('a', 'p') then 'Pagamento Realizado'"
            SQL = SQL & "       WHEN repasse_agendamento_cobranca_atual_tb.proposta_id is not null then 'Pagamento Realizado'"
            SQL = SQL & "       WHEN repasse_cartao_credito_tb.proposta_id is not null then 'Pagamento Realizado'"
            ' --egama - projeto SAP(confitec)

            SQL = SQL & "       when a.situacao = 'e' then 'Pendente de Envio'"
            SQL = SQL & "       when a.situacao = 'i' then 'Inadimplente'"
            SQL = SQL & "       when a.situacao = 'r' then 'Reenviado'"
            SQL = SQL & "       WHEN a.situacao = 'p' and ARQUIVO_CUTOVER.proposta_id is not null THEN 'Pgto em Transi��o SAP' "    ' --egama - projeto sap (Confitec)
            SQL = SQL & "       when a.situacao = 'p' then 'Pendente de Retorno'"
            SQL = SQL & "       when a.situacao = 'x' then 'Estorno'"
            SQL = SQL & "   end                                   "
            SQL = SQL & ",  a.dt_baixa               " & vbCrLf
            SQL = SQL & ",  origem_baixa = case a.origem_baixa when 'a' then 'Autom�tico'       " & vbCrLf
            SQL = SQL & "    when 'm' then 'Manual    '       " & vbCrLf
            SQL = SQL & "             else '          '  end  " & vbCrLf
            SQL = SQL & ", isnull(convert(char, a.deb_agencia_id), '') deb_agencia_id"
            SQL = SQL & ", isnull(convert(char, a.deb_conta_corrente_id),'') deb_conta_corrente_id"
            SQL = SQL & ", carta_emitida = case "
            SQL = SQL & "       when a.carta_emitida = 's' then 'Sim'"
            SQL = SQL & "       when a.carta_emitida <> 's' then NULL"
            SQL = SQL & "   end"
            SQL = SQL & ", isnull(a.canc_endosso_id, 0) canc_endosso_id "
            'Renato Vasconcelos
            'Demanda MU-2018-015339
            'Inicio
            If vProdutoId = 1206 Then
                SQL = SQL & ", isnull(tp_avaliacao_cobranca_tb.descricao, '') retorno_bb "
            Else
                SQL = SQL & ", '' retorno_bb "
            End If
            'Fim
            SQL = SQL & " FROM agendamento_cobranca_tb  a  WITH (NOLOCK) "
            'Egama - projeto SAP(confitec)
            SQL = SQL & " LEFT JOIN SEGUROS_DB.DBO.ARQUIVO_CUTOVER_TB  ARQUIVO_CUTOVER with(nolock)"
            SQL = SQL & "   on a.proposta_id = ARQUIVO_CUTOVER.proposta_id"
            SQL = SQL & "  and a.num_cobranca = ARQUIVO_CUTOVER.num_cobranca"
            SQL = SQL & " left join seguros_db.dbo.repasse_cartao_credito_tb  repasse_cartao_credito_tb with(nolock)"
            SQL = SQL & "   on repasse_cartao_credito_tb.proposta_id = a.proposta_id"
            SQL = SQL & "  and repasse_cartao_credito_tb.num_cobranca = a.num_cobranca"
            SQL = SQL & " Left Join seguro_cartao_db.dbo.agendamento_cobranca_cartao_atual_tb agendamento_cobranca_cartao_atual_tb with(nolock)"
            SQL = SQL & "   on a.proposta_id = agendamento_cobranca_cartao_atual_tb.proposta_id"
            SQL = SQL & "  and a.num_cobranca = agendamento_cobranca_cartao_atual_tb.num_cobranca"
            SQL = SQL & "  and agendamento_cobranca_cartao_atual_tb.situacao_operadora in ('a', 'p')"
            SQL = SQL & " Left Join seguro_cartao_db.dbo.agendamento_cobranca_cartao_tb agendamento_cobranca_cartao_tb with(nolock)"
            SQL = SQL & "   on a.proposta_id = agendamento_cobranca_cartao_tb.proposta_id"
            SQL = SQL & "  and a.num_cobranca = agendamento_cobranca_cartao_tb.num_cobranca"
            SQL = SQL & "  and agendamento_cobranca_cartao_tb.situacao_operadora in ('a', 'p')"
            SQL = SQL & " Left Join seguro_cartao_db.dbo.agendamento_pendente_cartao_tb agendamento_pendente_cartao_tb with(nolock)"
            SQL = SQL & "   on a.proposta_id = agendamento_pendente_cartao_tb.proposta_id"
            SQL = SQL & "  and a.num_cobranca = agendamento_pendente_cartao_tb.num_cobranca"
            SQL = SQL & "  and agendamento_pendente_cartao_tb.situacao_operadora in ('a', 'p')"
            SQL = SQL & " Left Join seguros_db.dbo.repasse_agendamento_cobranca_atual_tb repasse_agendamento_cobranca_atual_tb with(nolock)"
            SQL = SQL & "   on a.proposta_id = repasse_agendamento_cobranca_atual_tb.proposta_id"
            SQL = SQL & "  and a.num_cobranca = repasse_agendamento_cobranca_atual_tb.num_cobranca"

            'Renato Vasconcelos
            'Demanda MU-2018-015339
            'Inicio

            If vProdutoId = 1206 Then
                SQL = SQL & "  left join (select proposta_id, nosso_numero, max(remessa) remessa from seguros_db.dbo.avaliacao_cobranca_tb with(nolock) group by proposta_id, nosso_numero) Ult_Avaliacao"
                SQL = SQL & "  on a.proposta_id = Ult_Avaliacao.proposta_id"
                SQL = SQL & "  and a.nosso_numero = Ult_Avaliacao.nosso_numero"
                SQL = SQL & "  Left join seguros_db.dbo.avaliacao_cobranca_tb avaliacao_cobranca_tb with(nolock)"
                SQL = SQL & "  on a.proposta_id = avaliacao_cobranca_tb.proposta_id"
                SQL = SQL & "  and a.nosso_numero = avaliacao_cobranca_tb.nosso_numero"
                SQL = SQL & "  and avaliacao_cobranca_tb.remessa = Ult_Avaliacao.remessa"
                SQL = SQL & "  Left join seguros_db.dbo.tp_avaliacao_cobranca_tb tp_avaliacao_cobranca_tb with(nolock)"
                SQL = SQL & "  on avaliacao_cobranca_tb.tp_avaliacao_cobranca_id = tp_avaliacao_cobranca_tb.tp_avaliacao_cobranca_id "
            End If
            'Final

            'Egama - Projeto SAP

            SQL = SQL & " WHERE a.proposta_id = " & vPropostaId
            SQL = SQL & " AND  a.situacao in ('a','e','i','r','p','x')"
            SQL = SQL & " ORDER BY a.num_cobranca "

            'End If

            Set rc = rdocn.OpenResultset(SQL)

            GridCobranca.Rows = 1

            Do While Not rc.EOF

                If IsNull(rc("num_endosso")) Then
                    NumEndosso = "0"
                Else
                    NumEndosso = rc("num_endosso")
                End If
                '
                If IsNull(rc("val_cobranca")) Then
                    ValCobranca = vValZero
                Else
                    If ConfiguracaoBrasil Then
                        ValCobranca = Format(Val(rc!val_cobranca), "###,###,##0.00")
                    Else
                        ValCobranca = TrocaValorAmePorBras(Format(Val(rc!val_cobranca), "###,###,##0.00"))
                    End If
                End If
                '
                If IsNull(rc("val_pago")) Then
                    ValPago = vValZero
                Else
                    If ConfiguracaoBrasil Then
                        ValPago = Format(Val(rc!val_pago), "###,###,##0.00")
                    Else
                        ValPago = TrocaValorAmePorBras(Format(Val(rc!val_pago), "###,###,##0.00"))
                    End If
                End If
                '
                If IsNull(rc("val_iof")) Then
                    ValIOF = vValZero
                Else
                    If ConfiguracaoBrasil Then
                        ValIOF = Format(Val(rc!val_iof), "###,###,##0.00")
                    Else
                        ValIOF = TrocaValorAmePorBras(Format(Val(rc!val_iof), "###,###,##0.00"))
                    End If
                End If
                '
                If IsNull(rc!dt_recebimento) Then
                    dt_recebimento = ""
                Else
                    dt_recebimento = Format(rc!dt_recebimento, "dd/mm/yyyy")
                End If

                If IsNull(rc!nosso_numero) Then
                    vNossoNumero = ""
                Else
                    vNossoNumero = rc("nosso_numero") & IIf(rc("nosso_numero_dv") <> "", "-" & rc("nosso_numero_dv"), "")
                End If

                '
                linha = NumEndosso & vbTab & rc!num_cobranca
                linha = linha & vbTab & vNossoNumero
                linha = linha & vbTab & Format(rc!dt_agendamento, "dd/mm/yyyy")
                linha = linha & vbTab & ValCobranca
                linha = linha & vbTab & ValIOF
                linha = linha & vbTab & dt_recebimento
                linha = linha & vbTab & ValPago
                linha = linha & vbTab & IIf(rc!canc_endosso_id > 0, "Cancelada", Trim(rc!desc_situacao))
                linha = linha & vbTab & Trim(rc!deb_agencia_id)
                linha = linha & vbTab & Trim(rc!deb_conta_corrente_id)
                'Renato Vasconcelos
                'Demanda MU-2018-015339
                'Inicio
                linha = linha & vbTab & rc!retorno_bb
                'Fim
                linha = linha & vbTab & Trim(rc!carta_emitida)
                ' joconceicao - 2002 aug 14 - data da baixa no grid

                'INC000004915723 - 20/04/2016

                linha = linha & vbTab & rc("dt_baixa")
                linha = linha & vbTab & rc("Origem_baixa")

                'INC000004915723 - 20/04/2016

                'mcarvalho - 22/09/2004
                'linha = linha & vbTab & rc("nosso_numero") & IIf(rc("nosso_numero_dv") <> "", "-" & rc("nosso_numero_dv"), "")

                '  Monta GridCobranca.
                GridCobranca.AddItem linha

                If NumEndosso = "0" Then
                    ValorTotParc = ValorTotParc + Val(rc("Val_Cobranca"))
                End If
                '
                Set vCobranca = Nothing
                Set vCobranca = New Cobranca
                '
                With vCobranca
                    .EndossoId = NumEndosso
                    .Id = rc!num_cobranca
                    If Not IsNull(rc!dt_agendamento) Then
                        .DtAgendamento = rc!dt_agendamento
                    End If
                    If Not IsNull(rc!dt_recebimento) Then
                        .DtPagamento = rc!dt_recebimento
                    End If
                    .ValPago = ValPago
                    .Situacao = rc!desc_situacao
                End With
                '
                Cobrancas.Add vCobranca
                '
                rc.MoveNext

            Loop


            'FLOW 221061 - MFerreira - Confitec Sistemas - 2007-06-27
            'Incluindo UNION para exibir faturas resumo

            '        SQL = "select   f.fatura_id, " & vbNewLine
            '        SQL = SQL & "     f.tot_imp_segurada, " & vbNewLine
            '        SQL = SQL & "     f.val_bruto, " & vbNewLine
            '        SQL = SQL & "     f.val_iof, " & vbNewLine
            '        SQL = SQL & "     f.dt_inicio_vigencia, " & vbNewLine
            '        SQL = SQL & "     f.dt_fim_vigencia, " & vbNewLine
            '        SQL = SQL & "     f.dt_recebimento, " & vbNewLine
            '        SQL = SQL & "     ac.nosso_numero, " & vbNewLine
            '        SQL = SQL & "     ac.nosso_numero_dv, " & vbNewLine
            '        SQL = SQL & "     desc_situacao = case    when f.situacao = 'a' then 'Aberta'    " & vbNewLine
            '        SQL = SQL & "              when f.situacao = 'b' then 'Baixada'    " & vbNewLine
            '        SQL = SQL & "              when f.situacao = 'c' then 'Cancelada'    " & vbNewLine
            '        SQL = SQL & "              when f.situacao = 'e' then 'Pendente de Envio'    " & vbNewLine
            '        SQL = SQL & "              when f.situacao = 'f' then 'Fechada'    " & vbNewLine
            '        SQL = SQL & "              when f.situacao = 't' or f.situacao = 'p' then 'Pendente'   end, " & vbNewLine
            '        SQL = SQL & "     isnull(f.perc_corretagem, 0) perc_corretagem, " & vbNewLine
            '        SQL = SQL & "     isnull(f.perc_pro_labore, 0) perc_pro_labore, " & vbNewLine
            '        SQL = SQL & "     isnull(f.observacao, '') observacao, f.endosso_id, " & vbNewLine
            '        SQL = SQL & "     isnull(ac.dt_baixa, f.dt_baixa) dt_baixa , " & vbNewLine
            '        SQL = SQL & "     origem_baixa = case     when ac.origem_baixa = 'a' then 'Autom�tico'    " & vbNewLine
            '        SQL = SQL & "              when ac.origem_baixa = 'm' then 'Manual'    " & vbNewLine
            '        SQL = SQL & "              when ac.origem_baixa is null then case when f.dt_baixa is not null and f.arquivo_baixa is not null    then 'Autom�tico'    " & vbNewLine
            '        SQL = SQL & "                             when f.dt_baixa is not null and f.arquivo_baixa is null    then 'Manual'    " & vbNewLine
            '        SQL = SQL & "                             else '          ' end     " & vbNewLine
            '        SQL = SQL & "              else '          '   end  " & vbNewLine
            '        SQL = SQL & "  from  apolice_tb ap            WITH (NOLOCK) " & vbNewLine
            '        SQL = SQL & "  left  join  fatura_tb f           WITH (NOLOCK) " & vbNewLine
            '        SQL = SQL & "     on f.apolice_id         = ap.apolice_id" & vbNewLine
            '        SQL = SQL & "     and   f.sucursal_seguradora_id   = ap.sucursal_seguradora_id" & vbNewLine
            '        SQL = SQL & "     and   f.seguradora_cod_susep     = ap.seguradora_cod_susep" & vbNewLine
            '        SQL = SQL & "     and   f.ramo_id         = ap.ramo_id" & vbNewLine
            '        SQL = SQL & "  left  join  agendamento_cobranca_tb ac     WITH (NOLOCK) " & vbNewLine
            '        SQL = SQL & "     on ac.proposta_id    = f.proposta_id" & vbNewLine
            '        SQL = SQL & "     and   ac.fatura_id   = f.fatura_id" & vbNewLine
            '        SQL = SQL & "     and   ac.apolice_id  = f.apolice_id" & vbNewLine
            '        SQL = SQL & "     and   ac.ramo_id  = f.ramo_id" & vbNewLine
            '        SQL = SQL & "  where    ap.apolice_id = " & vApoliceId & vbNewLine
            '        SQL = SQL & "  AND   ap.sucursal_seguradora_id = " & vSucursalId & vbNewLine
            '        SQL = SQL & "  AND   ap.seguradora_cod_susep = " & vSeguradoraId & vbNewLine
            '        SQL = SQL & "  and   ap.ramo_id = " & vRamoId & vbNewLine
            '        SQL = SQL & "  ORDER BY f.fatura_id    "



            'RRAMOS - 2007/07/13 - Flow 224711 - Melhoria de performance. Nova query elaborada pelo Anderson Lorusso. ----------------------

            '''        SQL = "select fatura_id = f.fatura_id, " & vbNewLine
            '''        SQL = SQL & "     f.tot_imp_segurada, " & vbNewLine
            '''        SQL = SQL & "     f.val_bruto, " & vbNewLine
            '''        SQL = SQL & "     f.val_iof, " & vbNewLine
            '''        SQL = SQL & "     f.dt_inicio_vigencia, " & vbNewLine
            '''        SQL = SQL & "     f.dt_fim_vigencia, " & vbNewLine
            '''        SQL = SQL & "     f.dt_recebimento, " & vbNewLine
            '''        SQL = SQL & "     ac.nosso_numero, " & vbNewLine
            '''        SQL = SQL & "     ac.nosso_numero_dv, " & vbNewLine
            '''        SQL = SQL & "     desc_situacao = case    when f.situacao = 'a' then 'Aberta'    " & vbNewLine
            '''        SQL = SQL & "              when f.situacao = 'b' then 'Baixada'    " & vbNewLine
            '''        SQL = SQL & "              when f.situacao = 'c' then 'Cancelada'    " & vbNewLine
            '''        SQL = SQL & "              when f.situacao = 'e' then 'Pendente de Envio'    " & vbNewLine
            '''        SQL = SQL & "              when f.situacao = 'f' then 'Fechada'    " & vbNewLine
            '''        SQL = SQL & "              when f.situacao = 't' or f.situacao = 'p' then 'Pendente'   end, " & vbNewLine
            '''        SQL = SQL & "     isnull(f.perc_corretagem, 0) perc_corretagem, " & vbNewLine
            '''        SQL = SQL & "     isnull(f.perc_pro_labore, 0) perc_pro_labore, " & vbNewLine
            '''        SQL = SQL & "     isnull(f.observacao, '') observacao, f.endosso_id, " & vbNewLine
            '''        SQL = SQL & "     isnull(ac.dt_baixa, f.dt_baixa) dt_baixa , " & vbNewLine
            '''        SQL = SQL & "     origem_baixa = case     when ac.origem_baixa = 'a' then 'Autom�tico'    " & vbNewLine
            '''        SQL = SQL & "              when ac.origem_baixa = 'm' then 'Manual'    " & vbNewLine
            '''        SQL = SQL & "              when ac.origem_baixa is null then case when f.dt_baixa is not null and f.arquivo_baixa is not null    then 'Autom�tico'    " & vbNewLine
            '''        SQL = SQL & "                             when f.dt_baixa is not null and f.arquivo_baixa is null    then 'Manual'    " & vbNewLine
            '''        SQL = SQL & "                             else '          ' end     " & vbNewLine
            '''        SQL = SQL & "              else '          '   end  " & vbNewLine
            '''        SQL = SQL & "  from  apolice_tb ap            WITH (NOLOCK) " & vbNewLine
            '''        SQL = SQL & "  left  join  fatura_tb f           WITH (NOLOCK) " & vbNewLine
            '''        SQL = SQL & "     on f.apolice_id         = ap.apolice_id" & vbNewLine
            '''        SQL = SQL & "     and   f.sucursal_seguradora_id   = ap.sucursal_seguradora_id" & vbNewLine
            '''        SQL = SQL & "     and   f.seguradora_cod_susep     = ap.seguradora_cod_susep" & vbNewLine
            '''        SQL = SQL & "     and   f.ramo_id         = ap.ramo_id" & vbNewLine
            '''        SQL = SQL & "  left  join  agendamento_cobranca_tb ac     WITH (NOLOCK) " & vbNewLine
            '''        SQL = SQL & "     on ac.proposta_id    = f.proposta_id" & vbNewLine
            '''        SQL = SQL & "     and   ac.fatura_id   = f.fatura_id" & vbNewLine
            '''        SQL = SQL & "     and   ac.apolice_id  = f.apolice_id" & vbNewLine
            '''        SQL = SQL & "     and   ac.ramo_id  = f.ramo_id" & vbNewLine
            '''        SQL = SQL & "  where    ap.apolice_id = " & vApoliceId & vbNewLine
            '''        SQL = SQL & "  AND   ap.sucursal_seguradora_id = " & vSucursalId & vbNewLine
            '''        SQL = SQL & "  AND   ap.seguradora_cod_susep = " & vSeguradoraId & vbNewLine
            '''        SQL = SQL & "  and   ap.ramo_id = " & vRamoId & vbNewLine
            '''
            '''        'FLOW 221061 - MFerreira - Confitec Sistemas - 2007-06-29
            '''        'Exibir somente as faturas que n�o estejam na fatura resumo
            '''        SQL = SQL & "  AND   f.fatura_resumo_id IS NULL " & vbNewLine
            '''
            '''        SQL = SQL & "  " & vbNewLine
            '''        SQL = SQL & "  UNION " & vbNewLine
            '''        SQL = SQL & "  " & vbNewLine
            '''        SQL = SQL & "  SELECT fatura_id = fatura_resumo_tb.fatura_resumo_id " & vbNewLine
            '''        SQL = SQL & "       , fatura_resumo_tb.tot_imp_segurada " & vbNewLine
            '''        SQL = SQL & "       , fatura_resumo_tb.val_bruto " & vbNewLine
            '''        SQL = SQL & "       , fatura_resumo_tb.val_iof " & vbNewLine
            '''        SQL = SQL & "       , dt_inicio_vigencia = ' ' " & vbNewLine
            '''        SQL = SQL & "       , dt_fim_vigencia    = ' ' " & vbNewLine
            '''        SQL = SQL & "       , fatura_resumo_tb.dt_recebimento " & vbNewLine
            '''        SQL = SQL & "       , fatura_resumo_tb.nosso_numero " & vbNewLine
            '''        SQL = SQL & "       , fatura_resumo_tb.nosso_numero_dv " & vbNewLine
            '''        SQL = SQL & "       , desc_situacao = CASE WHEN fatura_resumo_tb.situacao = 'b' THEN 'Baixada' " & vbNewLine
            '''        SQL = SQL & "                              WHEN fatura_resumo_tb.situacao = 't' THEN 'Pendente' " & vbNewLine
            '''        SQL = SQL & "                          END " & vbNewLine
            '''        SQL = SQL & "       , perc_corretagem = 0 " & vbNewLine
            '''        SQL = SQL & "       , perc_pro_labore = 0 " & vbNewLine
            '''        SQL = SQL & "       , observacao      = 'Fatura Resumo' " & vbNewLine
            '''        SQL = SQL & "       , endosso_id      = fatura_resumo_tb.endosso_id " & vbNewLine
            '''        SQL = SQL & "       , dt_baixa        = ISNULL(fatura_resumo_tb.dt_baixa, ' ') " & vbNewLine
            '''        SQL = SQL & "       , origem_baixa = CASE WHEN fatura_resumo_tb.dt_baixa      IS NOT NULL " & vbNewLine
            '''        SQL = SQL & "                              AND fatura_resumo_tb.arquivo_baixa IS NOT NULL THEN 'Autom�tico' " & vbNewLine
            '''        SQL = SQL & "                             WHEN fatura_resumo_tb.dt_baixa      IS NOT NULL " & vbNewLine
            '''        SQL = SQL & "                              AND fatura_resumo_tb.arquivo_baixa IS NULL     THEN 'Manual' " & vbNewLine
            '''        SQL = SQL & "                             ELSE '          ' " & vbNewLine
            '''        SQL = SQL & "                         END " & vbNewLine
            '''        SQL = SQL & "    FROM apolice_tb  WITH (NOLOCK)  " & vbNewLine
            '''        SQL = SQL & "    JOIN fatura_resumo_tb  WITH (NOLOCK)  " & vbNewLine
            '''        SQL = SQL & "      ON fatura_resumo_tb.apolice_id             = apolice_tb.apolice_id " & vbNewLine
            '''        SQL = SQL & "     AND fatura_resumo_tb.sucursal_seguradora_id = apolice_tb.sucursal_seguradora_id " & vbNewLine
            '''        SQL = SQL & "     AND fatura_resumo_tb.seguradora_cod_susep   = apolice_tb.seguradora_cod_susep " & vbNewLine
            '''        SQL = SQL & "     AND fatura_resumo_tb.ramo_id                = apolice_tb.ramo_id " & vbNewLine
            '''        SQL = SQL & "   WHERE apolice_tb.apolice_id             = " & vApoliceId & vbNewLine
            '''        SQL = SQL & "     AND apolice_tb.sucursal_seguradora_id = " & vSucursalId & vbNewLine
            '''        SQL = SQL & "     AND apolice_tb.seguradora_cod_susep   = " & vSeguradoraId & vbNewLine
            '''        SQL = SQL & "     AND apolice_tb.ramo_id                = " & vRamoId & vbNewLine
            '''        SQL = SQL & "   ORDER BY fatura_id " & vbNewLine



            SQL = ""
            SQL = SQL & "SET NOCOUNT ON " & vbNewLine & vbNewLine
            SQL = SQL & "Select apolice_id, sucursal_seguradora_id, seguradora_cod_susep, ramo_id, proposta_id " & vbNewLine
            SQL = SQL & "Into #apolice " & vbNewLine
            SQL = SQL & "From apolice_tb  WITH (NOLOCK)  " & vbNewLine
            SQL = SQL & "Where apolice_id             = " & vApoliceId & vbNewLine
            SQL = SQL & "  AND sucursal_seguradora_id = " & vSucursalId & vbNewLine
            SQL = SQL & "  AND seguradora_cod_susep   = " & vSeguradoraId & vbNewLine
            SQL = SQL & "  AND ramo_id                = " & vRamoId & vbNewLine & vbNewLine

            SQL = SQL & "CREATE UNIQUE CLUSTERED INDEX UK001_#apolice ON #apolice(apolice_id, ramo_id, sucursal_seguradora_id, seguradora_cod_susep) " & vbNewLine & vbNewLine

            SQL = SQL & "Select f.fatura_id, " & vbNewLine
            SQL = SQL & "       f.tot_imp_segurada, " & vbNewLine
            SQL = SQL & "       f.val_bruto, " & vbNewLine
            SQL = SQL & "       f.val_iof, " & vbNewLine
            SQL = SQL & "       f.dt_inicio_vigencia, " & vbNewLine
            SQL = SQL & "       f.dt_fim_vigencia, " & vbNewLine
            SQL = SQL & "       f.dt_recebimento, " & vbNewLine
            SQL = SQL & "       f.situacao, " & vbNewLine
            SQL = SQL & "       f.perc_corretagem, " & vbNewLine
            SQL = SQL & "       f.perc_pro_labore, " & vbNewLine
            SQL = SQL & "       f.observacao, " & vbNewLine
            SQL = SQL & "       f.endosso_id, " & vbNewLine
            SQL = SQL & "       f.dt_baixa, " & vbNewLine
            SQL = SQL & "       f.arquivo_baixa, " & vbNewLine
            SQL = SQL & "       f.proposta_id, " & vbNewLine
            SQL = SQL & "       f.apolice_id, " & vbNewLine
            SQL = SQL & "       f.ramo_id, " & vbNewLine
            SQL = SQL & "       f.sucursal_seguradora_id, " & vbNewLine
            SQL = SQL & "       f.seguradora_cod_susep " & vbNewLine
            SQL = SQL & "Into #fatura " & vbNewLine
            SQL = SQL & "From #apolice ap " & vbNewLine
            SQL = SQL & "left join fatura_tb f  WITH (NOLOCK)  " & vbNewLine
            SQL = SQL & "  on f.apolice_id             = ap.apolice_id " & vbNewLine
            SQL = SQL & " and f.sucursal_seguradora_id = ap.sucursal_seguradora_id " & vbNewLine
            SQL = SQL & " and f.seguradora_cod_susep   = ap.seguradora_cod_susep " & vbNewLine
            SQL = SQL & " and f.ramo_id                = ap.ramo_id " & vbNewLine
            SQL = SQL & "Where f.fatura_resumo_id Is Null " & vbNewLine & vbNewLine

            SQL = SQL & "CREATE UNIQUE CLUSTERED INDEX UK001_#fatura ON #fatura(fatura_id, proposta_id, apolice_id, ramo_id) " & vbNewLine & vbNewLine

            ' G&P Rlambert - 17/12/2007 - Flow 271172 - Inclus�o de campo identity na tabela #agendamento_cobranca
            ' Ocorria erro na cria��o de indice na tabela devido a fatura_id = null para mais de um registro
            SQL = SQL & "Select agendamento_cobranca_id = IDENTITY(int, 1, 1), ac.fatura_id, f.proposta_id, f.apolice_id, f.ramo_id, ac.nosso_numero, ac.nosso_numero_dv, ac.origem_baixa, ac.dt_baixa " & vbNewLine
            SQL = SQL & "Into #agendamento_cobranca " & vbNewLine
            SQL = SQL & "From #apolice f " & vbNewLine
            SQL = SQL & "left join agendamento_cobranca_tb ac  WITH (NOLOCK)  " & vbNewLine
            SQL = SQL & "  on ac.apolice_id  = f.apolice_id " & vbNewLine
            SQL = SQL & " and ac.ramo_id     = f.ramo_id " & vbNewLine
            SQL = SQL & " and ac.proposta_id = f.proposta_id " & vbNewLine & vbNewLine

            SQL = SQL & "CREATE UNIQUE CLUSTERED INDEX UK001_#agendamento_cobranca ON #agendamento_cobranca(agendamento_cobranca_id, fatura_id, proposta_id, apolice_id, ramo_id) " & vbNewLine & vbNewLine

            SQL = SQL & "select fatura_id = f.fatura_id, " & vbNewLine
            SQL = SQL & "     f.tot_imp_segurada, " & vbNewLine
            SQL = SQL & "     f.val_bruto, " & vbNewLine
            SQL = SQL & "     f.val_iof, " & vbNewLine
            SQL = SQL & "     f.dt_inicio_vigencia, " & vbNewLine
            SQL = SQL & "     f.dt_fim_vigencia, " & vbNewLine
            SQL = SQL & "     f.dt_recebimento, " & vbNewLine
            SQL = SQL & "     ac.nosso_numero, " & vbNewLine
            SQL = SQL & "     ac.nosso_numero_dv, " & vbNewLine
            SQL = SQL & "     desc_situacao = case    when f.situacao = 'a' then 'Aberta' " & vbNewLine
            SQL = SQL & "              when f.situacao = 'b' then 'Baixada' " & vbNewLine
            SQL = SQL & "              when f.situacao = 'c' then 'Cancelada' " & vbNewLine
            SQL = SQL & "              when f.situacao = 'e' then 'Pendente de Envio' " & vbNewLine
            SQL = SQL & "              when f.situacao = 'f' then 'Fechada' " & vbNewLine
            SQL = SQL & "              when f.situacao = 't' or f.situacao = 'p' then 'Pendente'   end, " & vbNewLine
            SQL = SQL & "     isnull(f.perc_corretagem, 0) perc_corretagem, " & vbNewLine
            SQL = SQL & "     isnull(f.perc_pro_labore, 0) perc_pro_labore, " & vbNewLine
            SQL = SQL & "     isnull(f.observacao, '') observacao, f.endosso_id, " & vbNewLine
            SQL = SQL & "     isnull(ac.dt_baixa, f.dt_baixa) dt_baixa , " & vbNewLine
            SQL = SQL & "     origem_baixa = case     when ac.origem_baixa = 'a' then 'Autom�tico' " & vbNewLine
            SQL = SQL & "              when ac.origem_baixa = 'm' then 'Manual' " & vbNewLine
            SQL = SQL & "              when ac.origem_baixa is null then case when f.dt_baixa is not null and f.arquivo_baixa is not null    then 'Autom�tico' " & vbNewLine
            SQL = SQL & "                             when f.dt_baixa is not null and f.arquivo_baixa is null    then 'Manual' " & vbNewLine
            SQL = SQL & "                             Else '          ' end " & vbNewLine
            SQL = SQL & "              Else '          '   end " & vbNewLine
            SQL = SQL & "  from  #apolice ap  WITH (NOLOCK)  " & vbNewLine
            SQL = SQL & "  left  join  #fatura f  WITH (NOLOCK)  " & vbNewLine
            SQL = SQL & "     on f.apolice_id             = ap.apolice_id " & vbNewLine
            SQL = SQL & "    and f.sucursal_seguradora_id = ap.sucursal_seguradora_id " & vbNewLine
            SQL = SQL & "    and f.seguradora_cod_susep   = ap.seguradora_cod_susep " & vbNewLine
            SQL = SQL & "    and f.ramo_id                = ap.ramo_id " & vbNewLine
            SQL = SQL & "  left  join #agendamento_cobranca ac  WITH (NOLOCK)  " & vbNewLine
            SQL = SQL & "    on  ac.proposta_id = f.proposta_id " & vbNewLine
            SQL = SQL & "    and ac.fatura_id   = f.fatura_id  " & vbNewLine
            SQL = SQL & "    and ac.apolice_id  = f.apolice_id " & vbNewLine
            SQL = SQL & "    and ac.ramo_id     = f.ramo_id " & vbNewLine

            SQL = SQL & "Union " & vbNewLine

            SQL = SQL & "  SELECT fatura_id = fatura_resumo_tb.fatura_resumo_id " & vbNewLine
            SQL = SQL & ", fatura_resumo_tb.tot_imp_segurada " & vbNewLine
            SQL = SQL & "       , fatura_resumo_tb.val_bruto " & vbNewLine
            SQL = SQL & "       , fatura_resumo_tb.val_iof " & vbNewLine
            SQL = SQL & "       , dt_inicio_vigencia = ' ' " & vbNewLine
            SQL = SQL & "       , dt_fim_vigencia    = ' ' " & vbNewLine
            SQL = SQL & "       , fatura_resumo_tb.dt_recebimento " & vbNewLine
            SQL = SQL & "       , fatura_resumo_tb.nosso_numero " & vbNewLine
            SQL = SQL & "       , fatura_resumo_tb.nosso_numero_dv " & vbNewLine
            SQL = SQL & "       , desc_situacao = CASE WHEN fatura_resumo_tb.situacao = 'b' THEN 'Baixada' " & vbNewLine
            SQL = SQL & "                              WHEN fatura_resumo_tb.situacao = 't' THEN 'Pendente' " & vbNewLine
            SQL = SQL & "                          End " & vbNewLine
            SQL = SQL & "       , perc_corretagem = 0 " & vbNewLine
            SQL = SQL & "       , perc_pro_labore = 0 " & vbNewLine
            SQL = SQL & "       , observacao      = 'Fatura Resumo' " & vbNewLine
            SQL = SQL & "       , endosso_id      = fatura_resumo_tb.endosso_id " & vbNewLine
            SQL = SQL & "       , dt_baixa        = ISNULL(fatura_resumo_tb.dt_baixa, ' ') " & vbNewLine
            SQL = SQL & "       , origem_baixa = CASE WHEN fatura_resumo_tb.dt_baixa      IS NOT NULL " & vbNewLine
            SQL = SQL & "                              AND fatura_resumo_tb.arquivo_baixa IS NOT NULL THEN 'Autom�tico' " & vbNewLine
            SQL = SQL & "                             WHEN fatura_resumo_tb.dt_baixa Is Not Null " & vbNewLine
            SQL = SQL & "                              AND fatura_resumo_tb.arquivo_baixa IS NULL     THEN 'Manual' " & vbNewLine
            SQL = SQL & "                             Else '          ' " & vbNewLine
            SQL = SQL & "                         End " & vbNewLine
            SQL = SQL & "    FROM #apolice  WITH (NOLOCK)  " & vbNewLine
            SQL = SQL & "    Join fatura_resumo_tb WITH (NOLOCK)  " & vbNewLine
            SQL = SQL & "      ON fatura_resumo_tb.apolice_id             = #apolice.apolice_id " & vbNewLine
            SQL = SQL & "     AND fatura_resumo_tb.sucursal_seguradora_id = #apolice.sucursal_seguradora_id " & vbNewLine
            SQL = SQL & "     AND fatura_resumo_tb.seguradora_cod_susep   = #apolice.seguradora_cod_susep " & vbNewLine
            SQL = SQL & "     AND fatura_resumo_tb.ramo_id                = #apolice.ramo_id " & vbNewLine
            SQL = SQL & "   ORDER BY fatura_id " & vbNewLine & vbNewLine

            SQL = SQL & "drop table #apolice " & vbNewLine
            SQL = SQL & "drop table #fatura " & vbNewLine
            SQL = SQL & "drop table #agendamento_cobranca "

            '------------------------------------------------------------------------------------------------------------
            'AFONSO : 15/05/2008
            'CORRE��O DE BUG
            rc.Close

            Set rc = rdocn.OpenResultset(SQL)

            Do While Not rc.EOF

                If Not IsNull(rc!fatura_id) Then

                    If Not IsNull(rc!endosso_id) Then
                        indEndossoGrid = True
                    Else
                        indEndossoGrid = False
                    End If
                    '
                    If CosseguroAceito And indEndossoGrid Then
                        sql1 = "SELECT num_endosso_cosseguro "
                        sql1 = sql1 & "FROM endosso_co_seguro_aceito_tb   WITH (NOLOCK)  "
                        sql1 = sql1 & "WHERE proposta_id = " & Val(txtNumero.Text)
                        sql1 = sql1 & "AND   endosso_id = " & rc!endosso_id

                        Set rc1 = rdoCn1.OpenResultset(sql1)

                        If Not rc1.EOF Then
                            NumEndossoLider = IIf(IsNull(rc1!num_endosso_cosseguro) = False, rc1!num_endosso_cosseguro, "    ")
                        End If

                        Set rc1 = Nothing

                    End If
                    '
                    Set vFatura = New Fatura
                    '
                    With vFatura
                        .Id = rc!fatura_id
                        .ValIS = Val(rc!tot_imp_segurada)
                        .ValPremio = Val(rc!val_bruto)
                        .ValIOF = Val(rc!val_iof)
                        .DtInicioVigencia = rc!dt_inicio_vigencia
                        If Not IsNull(rc!dt_fim_vigencia) Then
                            .DtFimVigencia = rc!dt_fim_vigencia
                        End If
                        If Not IsNull(rc!nosso_numero) Then
                            .NossoNumero = rc!nosso_numero & IIf(rc("nosso_numero_dv") <> "", "-" & rc("nosso_numero_dv"), "")
                        End If
                        .DtRecebimento = rc!dt_recebimento
                        .Situacao = Trim(rc!desc_situacao)
                        .PercCorretagem = Val(rc!perc_corretagem)
                        .PercProLabore = Val(rc!perc_pro_labore)
                        .Observacao = Trim(rc!Observacao)
                        .EndossoId = IIf(IsNull(rc!endosso_id) = False, rc!endosso_id, "")
                        .EndossoIdLider = NumEndossoLider
                        'Luciana - 27/08/2003
                        If Not IsNull(rc!dt_baixa) Then
                            .DtBaixa = rc!dt_baixa
                        End If
                        .OriBaixa = rc!origem_baixa
                    End With
                    '
                    Faturas.Add vFatura, CStr(rc!fatura_id)
                    '
                End If

                rc.MoveNext
            Loop
            '

            If CosseguroAceito And indEndossoGrid Then
                Monta_GridFaturas_Lider
            Else
                Monta_GridFaturas
            End If

        Else    ' Ap�lice tem cobran�a por fatura

            'Alessandra Grig�rio - 23/06/2003
            SQL = "SELECT   num_endosso," & vbCrLf
            SQL = SQL & "   num_parcela_endosso," & vbCrLf
            SQL = SQL & "   num_cobranca," & vbCrLf
            SQL = SQL & "   a.nosso_numero," & vbCrLf
            SQL = SQL & "   a.nosso_numero_dv," & vbCrLf
            SQL = SQL & "   dt_agendamento," & vbCrLf
            SQL = SQL & "   val_cobranca," & vbCrLf
            SQL = SQL & "   dt_recebimento," & vbCrLf
            SQL = SQL & "   val_pago," & vbCrLf
            SQL = SQL & "   val_iof," & vbCrLf
            SQL = SQL & "   desc_situacao = case    when situacao = 'a' then 'Adimplente'" & vbCrLf
            SQL = SQL & "            when situacao = 'e' then 'Pendente de Envio'" & vbCrLf
            SQL = SQL & "            when situacao = 'i' then 'Inadimplente'" & vbCrLf
            SQL = SQL & "            when situacao = 'r' then 'Reenviado'" & vbCrLf
            SQL = SQL & "            when situacao = 'p' then 'Pendente de Retorno'" & vbCrLf
            SQL = SQL & "            when situacao = 'x' then 'Estorno'" & vbCrLf
            SQL = SQL & "         end," & vbCrLf
            SQL = SQL & "   dt_baixa," & vbCrLf
            SQL = SQL & "   origem_baixa = case origem_baixa    when 'a' then 'Autom�tico'" & vbCrLf
            SQL = SQL & "                  when 'm' then 'Manual    '" & vbCrLf
            SQL = SQL & "                  Else '          '" & vbCrLf
            SQL = SQL & "         end," & vbCrLf
            SQL = SQL & "   isnull(convert(char, a.deb_agencia_id), '') deb_agencia_id," & vbCrLf
            SQL = SQL & "   isnull(convert(char, a.deb_conta_corrente_id),'') deb_conta_corrente_id," & vbCrLf
            SQL = SQL & "   carta_emitida = case    when carta_emitida = 's' then 'Sim'" & vbCrLf
            SQL = SQL & "            when carta_emitida <> 's' then NULL" & vbCrLf
            SQL = SQL & "         end," & vbCrLf
            SQL = SQL & "   isnull(canc_endosso_id, 0) canc_endosso_id" & vbCrLf
            'Carolina Ramos
            'Incidente: IM00623890
            If vProdutoId = 1206 Then
                SQL = SQL & ", isnull(tp_avaliacao_cobranca_tb.descricao, '') retorno_bb "
            Else
                SQL = SQL & ", '' retorno_bb "
            End If
            ' Autor: pablo.cardoso (Nova Consultoria)
            ' Data da Altera��o: 09/09/2011
            ' Demanda: 11427194 - Altera��o da Gera��o de Fatura CDC Carteira
            ' inser��o dos campos 'capital segurado' e 'qtde registros' quando o produto for 1140
            If vProdutoId = "1140" Then
                sServidor = ObterServidor
                SQL = SQL & "       , (SELECT ISNULL(SUM(cf.capital_segurado), 0) " & vbNewLine
                SQL = SQL & "           FROM " & sServidor & "cdc_db.dbo.controle_financeiro_tb cf WITH(NOLOCK) " & vbNewLine
                SQL = SQL & "           WHERE cf.proposta_id = a.proposta_id " & vbNewLine
                SQL = SQL & "           AND cf.endosso_id = a.num_endosso) capital_segurado " & vbNewLine
                SQL = SQL & "       , ISNULL((SELECT qtd_aceita " & vbNewLine
                SQL = SQL & "                   FROM " & sServidor & "cdc_db.dbo.controle_financeiro_tb cf WITH(NOLOCK)" & vbNewLine
                SQL = SQL & "                  INNER JOIN " & sServidor & "cdc_db.dbo.resumo_operacao_tb ro WITH(NOLOCK)" & vbNewLine
                SQL = SQL & "                     ON cf.resumo_operacao_id = ro.resumo_operacao_id" & vbNewLine
                SQL = SQL & "                  WHERE cf.proposta_id = a.proposta_id " & vbNewLine
                SQL = SQL & "                    AND cf.endosso_id = a.num_endosso), 0) qtde_registros " & vbNewLine
            End If
            '
            SQL = SQL & " FROM  APOLICE_TB AP    WITH (NOLOCK) " & vbCrLf
            SQL = SQL & " join  agendamento_cobranca_tb  a  WITH (NOLOCK) " & vbCrLf
            SQL = SQL & "   ON      A.APOLICE_ID   = AP.APOLICE_ID" & vbCrLf
            SQL = SQL & "   and      A.SUCURSAL_SEGURADORA_ID   = AP.SUCURSAL_SEGURADORA_ID" & vbCrLf
            SQL = SQL & "   AND     A.SEGURADORA_COD_SUSEP     = AP.SEGURADORA_COD_SUSEP" & vbCrLf
            SQL = SQL & "   AND     A.RAMO_ID             = AP.RAMO_ID" & vbCrLf
            SQL = SQL & "   and      A.PROPOSTA_ID        = AP.PROPOSTA_ID" & vbCrLf
            If vProdutoId = 1206 Then
                SQL = SQL & "  left join (select proposta_id, nosso_numero, max(remessa) remessa from seguros_db.dbo.avaliacao_cobranca_tb with(nolock) group by proposta_id, nosso_numero) Ult_Avaliacao"
                SQL = SQL & "  on a.proposta_id = Ult_Avaliacao.proposta_id"
                SQL = SQL & "  and a.nosso_numero = Ult_Avaliacao.nosso_numero"
                SQL = SQL & "  Left join seguros_db.dbo.avaliacao_cobranca_tb avaliacao_cobranca_tb with(nolock)"
                SQL = SQL & "  on a.proposta_id = avaliacao_cobranca_tb.proposta_id"
                SQL = SQL & "  and a.nosso_numero = avaliacao_cobranca_tb.nosso_numero"
                SQL = SQL & "  and avaliacao_cobranca_tb.remessa = Ult_Avaliacao.remessa"
                SQL = SQL & "  Left join seguros_db.dbo.tp_avaliacao_cobranca_tb tp_avaliacao_cobranca_tb with(nolock)"
                SQL = SQL & "  on avaliacao_cobranca_tb.tp_avaliacao_cobranca_id = tp_avaliacao_cobranca_tb.tp_avaliacao_cobranca_id "
            End If
            SQL = SQL & " Where AP.apolice_id = " & vApoliceId & vbCrLf
            SQL = SQL & " AND     AP.sucursal_seguradora_id = " & vSucursalId & vbCrLf
            SQL = SQL & " AND     AP.seguradora_cod_susep = " & vSeguradoraId & vbCrLf
            SQL = SQL & " AND     AP.ramo_id = " & vRamoId & vbCrLf
            SQL = SQL & " AND   a.situacao in ('a','e','i','r','p','x')" & vbCrLf
            SQL = SQL & " AND   a.val_cobranca <> 0" & vbCrLf
            SQL = SQL & " AND   not exists  (select fatura_id" & vbCrLf
            SQL = SQL & "         from  FATURA_TB F  WITH (NOLOCK) " & vbCrLf
            SQL = SQL & "         Where F.apolice_id = AP.apolice_id" & vbCrLf
            SQL = SQL & "         AND     F.RAMO_ID                  = AP.RAMO_ID" & vbCrLf
            SQL = SQL & "         AND     F.SUCURSAL_SEGURADORA_ID   = AP.SUCURSAL_SEGURADORA_ID" & vbCrLf
            SQL = SQL & "         AND     F.SEGURADORA_COD_SUSEP     = AP.SEGURADORA_COD_SUSEP" & vbCrLf
            SQL = SQL & "         AND     F.FATURA_ID              = A.FATURA_ID)" & vbCrLf
            SQL = SQL & " ORDER BY num_cobranca"

            Set rc = rdocn.OpenResultset(SQL)

            GridCobranca.Rows = 1

            Do While Not rc.EOF

                If IsNull(rc("num_endosso")) Then
                    NumEndosso = "0"
                Else
                    NumEndosso = rc("num_endosso")
                End If
                '
                If IsNull(rc("val_cobranca")) Then
                    ValCobranca = vValZero
                Else
                    If ConfiguracaoBrasil Then
                        ValCobranca = Format(Val(rc!val_cobranca), "###,###,##0.00")
                    Else
                        ValCobranca = TrocaValorAmePorBras(Format(Val(rc!val_cobranca), "###,###,##0.00"))
                    End If
                End If
                '
                If IsNull(rc("val_pago")) Then
                    ValPago = vValZero
                Else
                    If ConfiguracaoBrasil Then
                        ValPago = Format(Val(rc!val_pago), "###,###,##0.00")
                    Else
                        ValPago = TrocaValorAmePorBras(Format(Val(rc!val_pago), "###,###,##0.00"))
                    End If
                End If
                '
                If IsNull(rc("val_iof")) Then
                    ValIOF = vValZero
                Else
                    If ConfiguracaoBrasil Then
                        ValIOF = Format(Val(rc!val_iof), "###,###,##0.00")
                    Else
                        ValIOF = TrocaValorAmePorBras(Format(Val(rc!val_iof), "###,###,##0.00"))
                    End If
                End If
                '
                If IsNull(rc!dt_recebimento) Then
                    dt_recebimento = ""
                Else
                    dt_recebimento = Format(rc!dt_recebimento, "dd/mm/yyyy")
                End If
                '

                If IsNull(rc!nosso_numero) Then
                    vNossoNumero = ""
                Else
                    vNossoNumero = rc!nosso_numero & IIf(rc("nosso_numero_dv") <> "", "-" & rc("nosso_numero_dv"), "")
                End If

                linha = NumEndosso & vbTab & rc!num_cobranca
                linha = linha & vbTab & vNossoNumero
                linha = linha & vbTab & Format(rc!dt_agendamento, "dd/mm/yyyy")
                linha = linha & vbTab & ValCobranca
                linha = linha & vbTab & ValIOF
                linha = linha & vbTab & dt_recebimento
                linha = linha & vbTab & ValPago
                linha = linha & vbTab & IIf(rc!canc_endosso_id > 0, "Cancelada", Trim(rc!desc_situacao))
                linha = linha & vbTab & Trim(rc!deb_agencia_id)
                linha = linha & vbTab & Trim(rc!deb_conta_corrente_id)
                'Carolina Ramos
                'Incidente: IM00623890
                linha = linha & vbTab & rc!retorno_bb
                
                linha = linha & vbTab & Trim(rc!carta_emitida)
                ' joconceicao - 2002 aug 14 - data da baixa no grid

                'INC000004915723 - 20/04/2016

                linha = linha & vbTab & rc("dt_baixa")
                linha = linha & vbTab & rc("Origem_baixa")

                'INC000004915723 - 20/04/2016


                ' Autor: pablo.cardoso (Nova Consultoria)
                ' Data da Altera��o: 09/09/2011
                ' Demanda: 11427194 - Altera��o da Gera��o de Fatura CDC Carteira
                ' inser��o dos campos 'capital segurado' e 'qtde registros' quando o produto for 1140
                If vProdutoId = "1140" Then
                    If ConfiguracaoBrasil Then
                        linha = linha & vbTab & Format(Val(rc!capital_segurado), "###,###,##0.00")
                    Else
                        linha = linha & vbTab & TrocaValorAmePorBras(Format(Val(rc!capital_segurado), "###,###,##0.00"))
                    End If
                    linha = linha & vbTab & rc!qtde_registros
                End If
                '  Monta GridCobranca.
                GridCobranca.AddItem linha

                If NumEndosso = "0" Then
                    ValorTotParc = ValorTotParc + Val(rc("Val_Cobranca"))
                End If
                '
                Set vCobranca = Nothing
                Set vCobranca = New Cobranca
                '
                With vCobranca
                    .EndossoId = NumEndosso
                    .Id = rc!num_cobranca

                    If Not IsNull(rc!dt_agendamento) Then
                        .DtAgendamento = rc!dt_agendamento
                    End If
                    If Not IsNull(rc!dt_recebimento) Then
                        .DtPagamento = rc!dt_recebimento
                    End If
                    .ValPago = ValPago
                    .Situacao = rc!desc_situacao
                End With
                '
                Cobrancas.Add vCobranca
                '
                rc.MoveNext

            Loop
            'FIM
            If SubGrupos.Count > 0 Then
                auxsel = ", isnull(sub_grupo_id, 1) sub_grupo_id "
                auxorder = "sub_grupo_id, "
            Else
                auxsel = ""
                auxorder = ""
            End If

            ''FlowBR 156809 - Apresenta��o das faturas com nosso n�mero para o produto 718 - Consorcio Prestamista.
            'Ana Paula - Stefanini | 29/08/2006.
            If vProdutoId = 718 Then
                SQL = "SELECT  f.fatura_id" & auxsel
                SQL = SQL & "  ,f.tot_imp_segurada,"
                SQL = SQL & "  f.val_bruto,"
                SQL = SQL & " isnull(f.val_iof,0) AS val_iof,"
                SQL = SQL & " f.dt_inicio_vigencia,"
                SQL = SQL & " f.dt_fim_vigencia,"
                SQL = SQL & " f.dt_recebimento,"
                SQL = SQL & " desc_situacao = case"
                SQL = SQL & " when f.situacao = 'a' then 'Aberta'"
                SQL = SQL & " when f.situacao = 'b' then 'Baixada'"
                SQL = SQL & " when f.situacao = 'c' then 'Cancelada'"
                SQL = SQL & " when f.situacao = 'e' then 'Pendente de Envio'"
                SQL = SQL & " when f.situacao = 'f' then 'Fechada'"
                'SQL = SQL & " when f.situacao = 't' then 'Pendente de Aprova��o Tesouraria'"
                SQL = SQL & " when f.situacao = 't' then 'Pendente'"          'Flow 215805 - Rmarins 22/05/07
                SQL = SQL & " end,"
                SQL = SQL & " isnull(f.perc_corretagem, 0) perc_corretagem,"
                SQL = SQL & " isnull(f.perc_pro_labore, 0) perc_pro_labore,"
                SQL = SQL & " isnull(f.observacao, '') observacao,"
                SQL = SQL & " f.endosso_id,"
                SQL = SQL & " f.dt_baixa,"
                SQL = SQL & " f.arquivo_baixa as origem_baixa,"
                SQL = SQL & " F.nosso_numero,"
                SQL = SQL & " F.nosso_numero_dv,"
                SQL = SQL & " TBvidas_endoso.qtde_vidas_endoso "
                SQL = SQL & " FROM     APOLICE_TB AP    WITH (NOLOCK) "
                SQL = SQL & " INNER   JOIN   FATURA_TB F  WITH (NOLOCK) "
                SQL = SQL & "    ON      F.APOLICE_ID               = AP.APOLICE_ID"
                SQL = SQL & "    AND     F.RAMO_ID                  = AP.RAMO_ID"
                SQL = SQL & "    AND     F.SUCURSAL_SEGURADORA_ID   = AP.SUCURSAL_SEGURADORA_ID"
                SQL = SQL & "    AND     F.SEGURADORA_COD_SUSEP     = AP.SEGURADORA_COD_SUSEP"

                SQL = SQL & " LEFT JOIN ( "
                SQL = SQL & "     SELECT apolice_id, ramo_id, sucursal_seguradora_id, seguradora_cod_susep, fatura_id, count(prop_cliente_id) AS qtde_vidas_endoso "
                SQL = SQL & "         FROM fatura_segurado_tb WITH(NOLOCK)"
                SQL = SQL & "        GROUP BY apolice_id, ramo_id, sucursal_seguradora_id, seguradora_cod_susep, fatura_id "
                SQL = SQL & "   ) AS TBvidas_endoso "
                SQL = SQL & "  ON TBvidas_endoso.apolice_id = F.apolice_id "
                SQL = SQL & " AND TBvidas_endoso.ramo_id = F.ramo_id "
                SQL = SQL & " AND TBvidas_endoso.sucursal_seguradora_id = F.sucursal_seguradora_id "
                SQL = SQL & " AND TBvidas_endoso.seguradora_cod_susep = F.seguradora_cod_susep "
                SQL = SQL & " AND TBvidas_endoso.fatura_id = F.fatura_id "

                SQL = SQL & " LEFT   JOIN    agendamento_cobranca_tb a    WITH (NOLOCK) "
                SQL = SQL & "    ON      A.APOLICE_ID   = AP.APOLICE_ID"
                SQL = SQL & "    and   A.SUCURSAL_SEGURADORA_ID   = AP.SUCURSAL_SEGURADORA_ID"
                SQL = SQL & "    AND     A.SEGURADORA_COD_SUSEP     = AP.SEGURADORA_COD_SUSEP"
                SQL = SQL & "    AND      A.RAMO_ID            = AP.RAMO_ID"
                SQL = SQL & "    AND     A.FATURA_ID           = F.FATURA_ID"
                SQL = SQL & "    and   A.PROPOSTA_ID        = AP.PROPOSTA_ID"
                SQL = SQL & " Where AP.apolice_id = " & vApoliceId
                SQL = SQL & " AND     AP.sucursal_seguradora_id = " & vSucursalId
                SQL = SQL & " AND     AP.seguradora_cod_susep = " & vSeguradoraId
                SQL = SQL & " AND     AP.ramo_id = " & vRamoId
                SQL = SQL & " ORDER BY sub_grupo_id, f.fatura_id"
            Else
                'Rodrigo Maiellaro (Stefanini) 18/10/2006
                'Inserido distinct para evitar duplicidades
                SQL = "SELECT  distinct f.fatura_id" & auxsel & vbCrLf
                SQL = SQL & "  ,f.tot_imp_segurada," & vbCrLf
                SQL = SQL & "  f.val_bruto," & vbCrLf
                SQL = SQL & " isnull(f.val_iof,0) AS val_iof," & vbCrLf
                SQL = SQL & " f.dt_inicio_vigencia," & vbCrLf
                SQL = SQL & " f.dt_fim_vigencia," & vbCrLf
                SQL = SQL & " f.dt_recebimento," & vbCrLf
                SQL = SQL & " desc_situacao = case" & vbCrLf
                SQL = SQL & " when f.situacao = 'a' then 'Aberta'" & vbCrLf
                SQL = SQL & " when f.situacao = 'b' then 'Baixada'" & vbCrLf
                SQL = SQL & " when f.situacao = 'c' then 'Cancelada'" & vbCrLf
                SQL = SQL & " when f.situacao = 'e' then 'Pendente de Envio'" & vbCrLf
                SQL = SQL & " when f.situacao = 'f' then 'Fechada'" & vbCrLf
                'SQL = SQL & " when f.situacao = 't' then 'Pendente de Aprova��o Tesouraria'"& vbCrLf
                'SQL = SQL & " when f.situacao = 't' then 'Pendente'" & vbCrLf         'Flow 215805 - Rmarins 22/05/07
                SQL = SQL & " when f.situacao = 't' then (case when ativ.tp_ativ_id = 7  then 'Em An�lise - GEROT' else 'Pendente' end )" & vbCrLf
                SQL = SQL & " end," & vbCrLf
                SQL = SQL & " isnull(f.perc_corretagem, 0) perc_corretagem," & vbCrLf
                SQL = SQL & " isnull(f.perc_pro_labore, 0) perc_pro_labore," & vbCrLf
                SQL = SQL & " isnull(f.observacao, '') observacao," & vbCrLf
                SQL = SQL & " f.endosso_id," & vbCrLf
                SQL = SQL & " a.dt_baixa," & vbCrLf
                SQL = SQL & " a.origem_baixa," & vbCrLf
                SQL = SQL & " a.nosso_numero," & vbCrLf
                SQL = SQL & " a.nosso_numero_dv," & vbCrLf
                SQL = SQL & " TBvidas_endoso.qtde_vidas_endoso "
                SQL = SQL & " FROM     APOLICE_TB AP    WITH (NOLOCK) " & vbCrLf
                SQL = SQL & " INNER   JOIN   FATURA_TB F  WITH (NOLOCK) " & vbCrLf
                SQL = SQL & "    ON      F.APOLICE_ID               = AP.APOLICE_ID" & vbCrLf
                SQL = SQL & "    AND     F.RAMO_ID                  = AP.RAMO_ID" & vbCrLf
                SQL = SQL & "    AND     F.SUCURSAL_SEGURADORA_ID   = AP.SUCURSAL_SEGURADORA_ID" & vbCrLf
                SQL = SQL & "    AND     F.SEGURADORA_COD_SUSEP     = AP.SEGURADORA_COD_SUSEP" & vbCrLf
                SQL = SQL & " INNER   JOIN    agendamento_cobranca_tb a    WITH (NOLOCK) " & vbCrLf
                SQL = SQL & "    ON      A.APOLICE_ID   = AP.APOLICE_ID" & vbCrLf
                SQL = SQL & "    and   A.SUCURSAL_SEGURADORA_ID   = AP.SUCURSAL_SEGURADORA_ID" & vbCrLf
                SQL = SQL & "    AND     A.SEGURADORA_COD_SUSEP     = AP.SEGURADORA_COD_SUSEP" & vbCrLf
                SQL = SQL & "    AND      A.RAMO_ID            = AP.RAMO_ID" & vbCrLf
                SQL = SQL & "    AND     A.FATURA_ID           = F.FATURA_ID" & vbCrLf
                SQL = SQL & "    and   A.PROPOSTA_ID        = AP.PROPOSTA_ID" & vbCrLf
                SQL = SQL & "             LEFT JOIN   APOLICE_WORKFLOW_TB aw  WITH (NOLOCK) " & vbCrLf
                SQL = SQL & "     ON  aw.apolice_id = f.apolice_id" & vbCrLf
                SQL = SQL & "     AND     aw.ramo_id = f.ramo_id" & vbCrLf
                SQL = SQL & "     AND     aw.subgrupo_id = f.sub_grupo_id" & vbCrLf
                SQL = SQL & "     AND     aw.dt_inicio_vigencia_apl = f.dt_inicio_vigencia" & vbCrLf
                SQL = SQL & "     AND     aw.wf_id = (SELECT TOP 1 WF_ID" & vbCrLf
                SQL = SQL & "         FROM APOLICE_WORKFLOW_TB AA" & vbCrLf
                SQL = SQL & "         Where AA.apolice_id = aw.apolice_id" & vbCrLf
                SQL = SQL & "         AND AA.SUBGRUPO_ID = aw.SUBGRUPO_ID" & vbCrLf
                SQL = SQL & "         AND AA.RAMO_ID = aw.RAMO_ID" & vbCrLf
                SQL = SQL & "         ORDER BY WF_ID DESC)" & vbCrLf

                SQL = SQL & " LEFT JOIN ( "
                SQL = SQL & "     SELECT apolice_id, ramo_id, sucursal_seguradora_id, seguradora_cod_susep, fatura_id, count(prop_cliente_id) AS qtde_vidas_endoso "
                SQL = SQL & "         FROM fatura_segurado_tb WITH(NOLOCK)"
                SQL = SQL & "        GROUP BY apolice_id, ramo_id, sucursal_seguradora_id, seguradora_cod_susep, fatura_id "
                SQL = SQL & "   ) AS TBvidas_endoso "
                SQL = SQL & "  ON TBvidas_endoso.apolice_id = F.apolice_id "
                SQL = SQL & " AND TBvidas_endoso.ramo_id = F.ramo_id "
                SQL = SQL & " AND TBvidas_endoso.sucursal_seguradora_id = F.sucursal_seguradora_id "
                SQL = SQL & " AND TBvidas_endoso.seguradora_cod_susep = F.seguradora_cod_susep "
                SQL = SQL & " AND TBvidas_endoso.fatura_id = F.fatura_id "

                SQL = SQL & " LEFT    JOIN    WORKFLOW_DB..ATIVIDADE_TB ativ  WITH (NOLOCK) " & vbCrLf
                SQL = SQL & "     ON  ativ.WF_ID = aw.wf_iD" & vbCrLf
                SQL = SQL & "     AND     ativ.tp_wf_id = aw.tp_wf_id" & vbCrLf
                SQL = SQL & "     AND     ativ.ativ_estado = 1    --atividade pronta para execu��o" & vbCrLf
                SQL = SQL & " Where AP.apolice_id = " & vApoliceId & vbCrLf
                SQL = SQL & " AND     AP.sucursal_seguradora_id = " & vSucursalId & vbCrLf
                SQL = SQL & " AND     AP.seguradora_cod_susep = " & vSeguradoraId & vbCrLf
                SQL = SQL & " AND     AP.ramo_id = " & vRamoId & vbCrLf
                'FLOW 180464 - MFerreira - Confitec 2006-11-08
                'Tratamento para faturas que foram Canceladas e Reativadas
                SQL = SQL & " AND NOT EXISTS (SELECT 1 " & vbCrLf
                SQL = SQL & "                   FROM agendamento_cobranca_tb  WITH (NOLOCK)  " & vbCrLf
                SQL = SQL & "                  WHERE agendamento_cobranca_tb.APOLICE_ID             = a.APOLICE_ID " & vbCrLf
                SQL = SQL & "                    AND agendamento_cobranca_tb.SUCURSAL_SEGURADORA_ID = a.SUCURSAL_SEGURADORA_ID " & vbCrLf
                SQL = SQL & "                    AND agendamento_cobranca_tb.SEGURADORA_COD_SUSEP   = a.SEGURADORA_COD_SUSEP " & vbCrLf
                SQL = SQL & "                    AND agendamento_cobranca_tb.RAMO_ID                = a.RAMO_ID " & vbCrLf
                SQL = SQL & "                    AND agendamento_cobranca_tb.FATURA_ID              = a.FATURA_ID " & vbCrLf
                SQL = SQL & "                    AND agendamento_cobranca_tb.PROPOSTA_ID            = a.PROPOSTA_ID " & vbCrLf
                'LSA - 07/05/2007
                'FLOW 212564 - Retornar a cobran�a reativada da fatura
                SQL = SQL & "                    AND agendamento_cobranca_tb.num_cobranca           > a.num_cobranca) " & vbCrLf
                'SQL = SQL & "                    AND agendamento_cobranca_tb.num_cobranca           < a.num_cobranca) "

                SQL = SQL & " ORDER BY f.fatura_id DESC, sub_grupo_id "
            End If
            Set rc = rdocn.OpenResultset(SQL)

            Do While Not rc.EOF

                If Not IsNull(rc!endosso_id) Then
                    indEndossoGrid = True
                Else
                    indEndossoGrid = False
                End If
                '
                '
                If CosseguroAceito And indEndossoGrid Then
                    sql1 = "SELECT num_endosso_cosseguro "
                    sql1 = sql1 & "FROM endosso_co_seguro_aceito_tb  WITH (NOLOCK)  "
                    sql1 = sql1 & "WHERE proposta_id = " & Val(txtNumero.Text)
                    sql1 = sql1 & "AND   endosso_id = " & rc!endosso_id

                    Set rc1 = rdoCn1.OpenResultset(sql1)

                    If Not rc1.EOF Then
                        NumEndossoLider = IIf(IsNull(rc1!num_endosso_cosseguro) = False, rc1!num_endosso_cosseguro, "    ")
                    End If

                    Set rc1 = Nothing
                End If
                '
                Set vFatura = New Fatura
                '
                With vFatura
                    .Id = rc!fatura_id
                    .NossoNumero = rc!nosso_numero & IIf(rc("nosso_numero_dv") <> "", "-" & rc("nosso_numero_dv"), "")
                    .ValIS = Val(rc!tot_imp_segurada)
                    .ValPremio = Val(rc!val_bruto)
                    .ValIOF = Val(rc!val_iof)
                    If Not IsNull(rc!dt_inicio_vigencia) Then
                        .DtInicioVigencia = rc!dt_inicio_vigencia
                    End If
                    If Not IsNull(rc!dt_fim_vigencia) Then
                        .DtFimVigencia = rc!dt_fim_vigencia
                    End If
                    .DtRecebimento = rc!dt_recebimento
                    .Situacao = Trim(rc!desc_situacao)
                    .PercCorretagem = Val(rc!perc_corretagem)
                    .PercProLabore = Val(rc!perc_pro_labore)
                    .Observacao = Trim(rc!Observacao)
                    .EndossoId = IIf(IsNull(rc!endosso_id) = False, rc!endosso_id, "")
                    .EndossoIdLider = NumEndossoLider
                    If Not IsNull(rc!dt_baixa) Then
                        .DtBaixa = rc!dt_baixa
                    End If
                    If Not IsNull(rc!origem_baixa) Then
                        If Trim(rc!origem_baixa) = "A" Or Trim(rc!origem_baixa) = "a" Then
                            .OriBaixa = "Autom�tico"
                        ElseIf Trim(rc!origem_baixa) = "M" Or Trim(rc!origem_baixa) = "m" Then
                            .OriBaixa = "Manual"
                        End If
                    End If

                    .QtdeVidas = IIf(IsNull(rc!qtde_vidas_endoso), 0, rc!qtde_vidas_endoso)

                End With
                '
                If SubGrupos.Count > 0 Then
                    SubGrupos(CStr(rc!sub_grupo_id)).Faturas.Add vFatura, CStr(rc!fatura_id)
                Else
                    Faturas.Add vFatura, CStr(rc!fatura_id)
                End If
                '
                rc.MoveNext
            Loop
            '
            If SubGrupos.Count = 0 Then    ' Monta_GridFaturas
                If CosseguroAceito And indEndossoGrid Then
                    Monta_GridFaturas_Lider
                Else
                    Monta_GridFaturas
                End If
            End If
        End If
        ''
        rc.Close
        Set rc = Nothing
        ''C�lculo Valor Total Parcelado p/ ap�lices emitidas
        'If txtStatus = "Ap�lice Emitida" Then
        '    If ConfiguracaoBrasil Then
        '        txtValTotalParc.Text = Format(ValorTotParc, "###,###,###,##0.00")
        '    Else
        '        txtValTotalParc.Text = TrocaValorAmePorBras(Format(ValorTotParc, "###,###,###,##0.00"))
        '    End If
        'End If

    Else

        SSTabProp.TabEnabled(TAB_COBRANCA) = False

    End If

    Exit Sub
    Resume
Erro:
    TrataErroGeral "Ler Cobranca"
    MsgBox "Rotina: Ler Cobranca"
End Sub

Private Sub Ler_Avaliacao()

    Dim rc As rdoResultset
    Dim SQL As String, linha As String

    Dim xRow As Integer
    Dim strMotRec As String

    '****************************************************
    'Ana Paula Minesio, em 31/10/2005
    'objetivo: verificar as permiss�es de visualiza��o de mensagens de recusa pelos usu�rios do sistema
    '****************************************************
    Dim ocls00420 As Object

    On Error GoTo Erro
    xRow = 0
    'Instancia Classe cls00420
    Set ocls00420 = CreateObject("SEGL0191.cls00420")

    '   Luciana - 12/07/2006 - JOIN incorreto -- prop.proposta_id = prop.proposta_id
    '    SQL = ""
    '    SQL = SQL & " SELECT a.dt_avaliacao dt_avaliacao, isnull(b.nome, 'TIPO DE AVALIACAO N�O CADASTRADO') descricao, "
    '    SQL = SQL & " a.Usuario Usuario, b.tp_avaliacao_id, b.Origem, prop.Situacao "
    '    SQL = SQL & " FROM avaliacao_proposta_tb a   WITH (NOLOCK) , als_produto_db..tp_avaliacao_tb b   WITH (NOLOCK) , proposta_tb prop "
    '    SQL = SQL & " Where a.proposta_id = " & vPropostaId
    '    SQL = SQL & " AND b.tp_avaliacao_id = a.tp_avaliacao_id "
    '    SQL = SQL & " AND a.proposta_id = prop.proposta_id "
    '
    '    SQL = SQL & " Union "
    '
    '    SQL = SQL & " SELECT isnull(c.dt_cancelamento_bb, c.dt_inicio_cancelamento) dt_avaliacao, "
    '    SQL = SQL & " 'CANCELAMENTO: ' + isnull(motivo_cancelamento, d.descricao) descricao, "
    '    SQL = SQL & " C.Usuario Usuario, b.tp_avaliacao_id, b.Origem, prop.Situacao "
    '    SQL = SQL & " FROM cancelamento_proposta_tb c    WITH (NOLOCK) , tp_cancelamento_tb d  WITH (NOLOCK) , als_produto_db..tp_avaliacao_tb b   WITH (NOLOCK) , proposta_tb prop "
    '    SQL = SQL & " Where C.proposta_id = " & vPropostaId
    '    SQL = SQL & " AND d.tp_cancelamento = c.tp_cancelamento "
    '    SQL = SQL & " AND prop.proposta_id = prop.proposta_id "
    '    SQL = SQL & " ORDER BY dt_avaliacao "
    '
    'SQL = SQL & " ORDER BY dt_avaliacao " - 05/12/2005 - Ana Paula

    SQL = ""
    'INICIO  'alterado 12/09/2013 projeto 17860335 - Melhorias no SEGBR MC_Amaral -confitec
    ' SQL = SQL & "SELECT     a.dt_avaliacao dt_avaliacao, isnull(b.nome, 'TIPO DE AVALIACAO N�O CADASTRADO') descricao,  a.Usuario Usuario, b.tp_avaliacao_id, b.Origem, prop.Situacao" & vbNewLine
    SQL = SQL & "SELECT     a.dt_avaliacao dt_avaliacao, isnull(b.nome, 'TIPO DE AVALIACAO N�O CADASTRADO') descricao,  a.Usuario Usuario, b.tp_avaliacao_id, b.Origem" & vbNewLine
    SQL = SQL & "FROM       avaliacao_proposta_tb a   WITH (NOLOCK) " & vbNewLine
    SQL = SQL & "           JOIN    als_produto_db.dbo.tp_avaliacao_tb b   WITH (NOLOCK) " & vbNewLine
    SQL = SQL & "                   ON  b.tp_avaliacao_id = a.tp_avaliacao_id" & vbNewLine
    '    SQL = SQL & "           JOIN    proposta_tb prop" & vbNewLine
    '    SQL = SQL & "                   ON  prop.proposta_id = a.proposta_id" & vbNewLine
    SQL = SQL & "WHERE      a.proposta_id = " & vPropostaId & vbNewLine
    If endosso_cancel Then
        SQL = SQL & "UNION" & vbNewLine
        'SQL = SQL & "SELECT     isnull(c.dt_cancelamento_bb, c.dt_inicio_cancelamento) dt_avaliacao,  'CANCELAMENTO: ' + isnull(motivo_cancelamento, d.descricao) descricao,  C.Usuario Usuario, b.tp_avaliacao_id, b.Origem, prop.Situacao" & vbNewLine
        SQL = SQL & "SELECT     isnull(c.dt_cancelamento_bb, c.dt_inicio_cancelamento) dt_avaliacao,  'CANCELAMENTO: ' + isnull(motivo_cancelamento, d.descricao) descricao,  C.Usuario Usuario, b.tp_avaliacao_id, b.Origem" & vbNewLine
        SQL = SQL & "FROM       cancelamento_proposta_tb c    WITH (NOLOCK) " & vbNewLine
        SQL = SQL & "           JOIN    tp_cancelamento_tb d  WITH (NOLOCK) " & vbNewLine
        SQL = SQL & "                   ON  d.tp_cancelamento = c.tp_cancelamento" & vbNewLine
        SQL = SQL & "           JOIN    avaliacao_proposta_tb e  WITH (NOLOCK) " & vbNewLine
        SQL = SQL & "                   ON  e.proposta_id = c.proposta_id" & vbNewLine
        SQL = SQL & "           JOIN    als_produto_db..tp_avaliacao_tb b   WITH (NOLOCK) " & vbNewLine
        SQL = SQL & "                   ON  b.tp_avaliacao_id = e.tp_avaliacao_id" & vbNewLine
        '    SQL = SQL & "           JOIN    proposta_tb prop   WITH (NOLOCK) " & vbNewLine
        '    SQL = SQL & "                   ON  prop.proposta_id = c.proposta_id" & vbNewLine
        SQL = SQL & "WHERE      c.proposta_id = " & vPropostaId & vbNewLine
    End If
    SQL = SQL & "ORDER      BY dt_avaliacao"
    'fim  'alterado 12/09/2013 projeto 17860335 - Melhorias no SEGBR MC_Amaral -confitec
    Set rc = rdocn.OpenResultset(SQL)

    gridAval.Rows = 1

    If Not rc.EOF Then
        While Not rc.EOF
            linha = Format(rc!dt_avaliacao, "dd/mm/yyyy")   'dt_avaliacao


            If Trim(UCase(vSituacao)) = "R" Or Trim(UCase(vSituacao)) = "S" Then

                lblMotRec.Visible = True
                txtMotRec.Visible = True
                gridAval.Height = 975

                'If ocls00420.VerificarPermissao(gsSIGLASISTEMA, _
                 App.Title, _
                 App.FileDescription, _
                 glAmbiente_id, _
                 Usuario, _
                 rc!tp_avaliacao_id) Then 'Usu�rio tem acesso a mensagem de recusa (mensagem secundaria - campo "nome")

                If ocls00420.VerificarPermissao(gsSIGLASISTEMA, _
                                                "SEGP0223", _
                                                App.FileDescription, _
                                                glAmbiente_id, _
                                                Usuario, _
                                                rc!tp_avaliacao_id) Then    'Usu�rio tem acesso a mensagem de recusa (mensagem secundaria - campo "nome")



                    linha = linha & vbTab & UCase(Trim(rc!Descricao))
                Else
                    If UCase(Trim(rc!Origem)) = "O" Then
                        linha = linha & vbTab & UCase(Trim("RECUSA OPERACIONAL"))
                    Else
                        linha = linha & vbTab & UCase(Trim("RECUSA T�CNICA"))
                    End If
                End If
            Else
                linha = linha & vbTab & UCase(Trim(rc!Descricao))

                lblMotRec.Visible = True
                txtMotRec.Visible = True
                gridAval.Height = 975

            End If

            'linha = linha & vbTab & UCase(Trim(rc!Descricao)) 'descricao

            linha = linha & vbTab & UCase(Trim(rc!Usuario))    'usuario
            '
            gridAval.AddItem linha
            '
            xRow = gridAval.Rows - 1
            rc.MoveNext
        Wend
        rc.Close
    End If
    '
    strMotRec = CStr(gridAval.TextMatrix(xRow, 1))
    txtMotRec.Text = strMotRec
    Set rc = Nothing

    'Elimina objeto criado
    Set ocls00420 = Nothing


    Exit Sub

Erro:
    TrataErroGeral "Ler Avaliacao"
    MsgBox "Rotina: Ler Avalia��o"
    'End
    Exit Sub
    Resume
End Sub


Sub Ler_Dados_Corretor()

    Dim rc As rdoResultset
    Dim vSql As String
    Dim CodCorretor As String
    Dim NomeCorretor As String
    Dim PercCorretagem As String
    Dim vCorretor As Corretor
    Dim TotCorretagem As Variant

    Dim sDtPedidoEndosso As String

    On Error GoTo Erro

    CodCorretor = ""
    PercCorretagem = ""
    TotCorretagem = 0#
    '
    gridCorretor.Rows = 1
    '
    If SubGrupos.Count = 0 Then

        'In�cio - Isabeli Silva - Confitec SP - 2017-10-17
        'MU-2017-036534 Cadastramento dos Corretores Exclusivos no SEGBR
        If isCorretor Then
            If CorretorProposta Then
                'Format(rsProposta("dt_validade_cartao"), "dd/mm/yyyy")
                sSQL = ""
                sSQL = sSQL & "SELECT dt_inicio_corretagem" & vbNewLine
                sSQL = sSQL & "FROM seguros_db.dbo.corretagem_tb WITH(NOLOCK)" & vbNewLine
                sSQL = sSQL & "WHERE proposta_id = " & vPropostaId & vbNewLine

                Set rc = rdocn.OpenResultset(sSQL)

                Dim sDataInicioCorretagem As String

                sDataInicioCorretagem = Format(rc("dt_inicio_corretagem"), "yyyymmdd")

                rc.Close

                Set rc = Nothing

                vSql = ""
                vSql = vSql & "   SELECT  corretor_susep = corr.corretor_susep,          " & vbNewLine
                vSql = vSql & "           corretor_id    =  corr.corretor_id ,                                       " & vbNewLine
                vSql = vSql & "           sucursal_corretor_id = '',                                                 " & vbNewLine
                vSql = vSql & "           nome = corr.nome,                                                          " & vbNewLine
                vSql = vSql & "           ca.dt_inicio_corretagem,                                                   " & vbNewLine
                vSql = vSql & "           ca.dt_fim_corretagem,                                                      " & vbNewLine
                vSql = vSql & "           perc_corretagem = isnull(cpf.perc_corretagem,cpj.perc_corretagem) ,        " & vbNewLine
                vSql = vSql & "           perc_angariacao = isnull(cpf.perc_angariacao,cpj.perc_angariacao)          " & vbNewLine
                vSql = vSql & "      FROM corretagem_tb ca  WITH (NOLOCK)  " & vbNewLine
                vSql = vSql & "      INNER JOIN  corretor_tb corr  WITH (NOLOCK)  " & vbNewLine
                vSql = vSql & "      ON corr.corretor_id = ca.corretor_id  " & vbNewLine
                vSql = vSql & " LEFT JOIN corretagem_pf_tb cpf  WITH (NOLOCK)                                        " & vbNewLine
                vSql = vSql & "        ON cpf.proposta_id = ca.proposta_id                                           " & vbNewLine
                vSql = vSql & "       AND cpf.corretor_id = ca.corretor_id                                           " & vbNewLine
                vSql = vSql & "       AND cpf.dt_inicio_corretagem = ca.dt_inicio_corretagem                         " & vbNewLine
                vSql = vSql & " LEFT JOIN corretagem_pj_tb cpj  WITH (NOLOCK)                                        " & vbNewLine
                vSql = vSql & "        ON cpj.proposta_id = ca.proposta_id                                           " & vbNewLine
                vSql = vSql & "       AND cpj.corretor_id = ca.corretor_id                                           " & vbNewLine
                vSql = vSql & "       AND cpj.dt_inicio_corretagem = ca.dt_inicio_corretagem                         " & vbNewLine
                vSql = vSql & "      JOIN proposta_fechada_tb pfc  WITH (NOLOCK)                                     " & vbNewLine
                vSql = vSql & "        ON pfc.proposta_id = ca.proposta_id                                           " & vbNewLine
                vSql = vSql & "     WHERE ca.proposta_id = " & vPropostaId & vbNewLine
                vSql = vSql & "       AND ca.dt_inicio_corretagem <= '" & sDataInicioCorretagem & "'                 " & vbNewLine
                vSql = vSql & "       AND (ca.dt_fim_corretagem is null                                              " & vbNewLine
                vSql = vSql & "            OR ca.dt_fim_corretagem >= '" & sDataInicioCorretagem & "')               " & vbNewLine
            End If
        Else
            vSql = " SELECT c.corretor_susep, c.corretor_id, cpj.sucursal_corretor_id"    'Alterado por Gustavo Machado em 10/11/2003
            vSql = vSql & ", c.nome, ca.dt_inicio_corretagem, ca.dt_fim_corretagem"
            vSql = vSql & ", cpj.perc_corretagem , cpj.perc_angariacao "
            vSql = vSql & "FROM corretagem_tb ca  WITH (NOLOCK) , corretagem_pj_tb cpj   WITH (NOLOCK) , corretor_tb c  WITH (NOLOCK)  "
            vSql = vSql & "WHERE ca.proposta_id = " & vPropostaId
            vSql = vSql & " AND ca.dt_inicio_corretagem <= '" & dt_inicio_query & "'"
            vSql = vSql & " AND (ca.dt_fim_corretagem is null "
            vSql = vSql & "     OR ca.dt_fim_corretagem > = '" & dt_inicio_query & "')"
            vSql = vSql & " AND cpj.proposta_id = ca.proposta_id"
            vSql = vSql & " AND cpj.corretor_id = ca.corretor_id"
            vSql = vSql & " AND cpj.dt_inicio_corretagem = ca.dt_inicio_corretagem "
            vSql = vSql & " AND c.corretor_id = ca.corretor_id"
            'Alessandra Grig�rio - 12/11/2003
            'vSql = vSql & " ORDER BY ca.corretor_id"
            vSql = vSql & " ORDER BY c.corretor_id"
        End If
        'Fim - Isabeli Silva - Confitec SP - 2017-10-17
        Set rc = rdocn.OpenResultset(vSql)
        If rc.EOF Then

            vSql = "SELECT c.corretor_susep, c.corretor_id, sucursal_corretor_id = ''"    'Alterado por Gustavo Machado em 10/11/2003
            vSql = vSql & ", c.nome, ca.dt_inicio_corretagem, ca.dt_fim_corretagem"
            vSql = vSql & ", cpf.perc_corretagem , cpf.perc_angariacao "
            vSql = vSql & "FROM corretagem_tb ca  WITH (NOLOCK) , corretagem_pf_tb cpf  WITH (NOLOCK) , corretor_tb c  WITH (NOLOCK) "
            vSql = vSql & "WHERE ca.proposta_id = " & vPropostaId
            vSql = vSql & " AND ca.dt_inicio_corretagem <= '" & dt_inicio_query & "'"
            vSql = vSql & " AND (ca.dt_fim_corretagem is null "
            vSql = vSql & "     OR ca.dt_fim_corretagem >= '" & dt_inicio_query & "')"
            vSql = vSql & " AND cpf.proposta_id = ca.proposta_id"
            vSql = vSql & " AND cpf.corretor_id = ca.corretor_id"
            vSql = vSql & " AND cpf.dt_inicio_corretagem = ca.dt_inicio_corretagem"
            vSql = vSql & " AND c.corretor_id = ca.corretor_id "
            vSql = vSql & " ORDER BY  c.corretor_id"
            '       vSql = vSql & " UNION "
            '        vSql = vSql & "SELECT c.corretor_susep, c.corretor_id, cpj.sucursal_corretor_id" 'Alterado por Gustavo Machado em 10/11/2003
            '        vSql = vSql & ", c.nome, ca.dt_inicio_corretagem, ca.dt_fim_corretagem"
            '        vSql = vSql & ", cpj.perc_corretagem , cpj.perc_angariacao "
            '        vSql = vSql & "FROM corretagem_tb ca  WITH (NOLOCK) , corretagem_pj_tb cpj   WITH (NOLOCK) , corretor_tb c  WITH (NOLOCK)  "
            '        vSql = vSql & "WHERE ca.proposta_id = " & vPropostaId
            '        vSql = vSql & " AND ca.dt_inicio_corretagem <= '" & dt_inicio_query & "'"
            '        vSql = vSql & " AND (ca.dt_fim_corretagem is null "
            '        vSql = vSql & "     OR ca.dt_fim_corretagem > = '" & dt_inicio_query & "')"
            '        vSql = vSql & " AND cpj.proposta_id = ca.proposta_id"
            '        vSql = vSql & " AND cpj.corretor_id = ca.corretor_id"
            '        vSql = vSql & " AND cpj.dt_inicio_corretagem = ca.dt_inicio_corretagem "
            '        vSql = vSql & " AND c.corretor_id = ca.corretor_id"
            '        'Alessandra Grig�rio - 12/11/2003
            '        'vSql = vSql & " ORDER BY ca.corretor_id"
            '        vSql = vSql & " ORDER BY c.corretor_id"
            '
            Set rc = rdocn.OpenResultset(vSql)
        End If


        While Not rc.EOF
            PercCorretagem = IIf(IsNull(rc!perc_corretagem), vValZero, Format(Val(rc!perc_corretagem), "0.00"))
            NomeCorretor = Trim(rc!Nome)
            '
            If (Trim(PercCorretagem) <> "") And (Not ConfiguracaoBrasil) Then
                PercCorretagem = TrocaValorAmePorBras(PercCorretagem)
            End If
            '
            Set vCorretor = New Corretor
            vCorretor.Id = rc!Corretor_id
            vCorretor.Nome = NomeCorretor
            vCorretor.PercCorretagem = PercCorretagem
            vCorretor.DtIniCorretagem = rc!dt_inicio_corretagem
            'Luciana - 11/11/2003
            'Alessandra Grig�rio - 12/08/2004
            'vCorretor.Codigo_susep = rc!corretor_susep
            vCorretor.Codigo_susep = IIf(IsNull(rc!corretor_susep), "", rc!corretor_susep)
            'FIM
            Corretores.Add vCorretor, CStr(rc!Corretor_id)
            '
            rc.MoveNext
        Wend
        '
        Monta_GridCorretores

    Else    'Tem Subgrupos

        vSql = " SELECT cs.sub_grupo_id, c.corretor_susep, c.corretor_id, cpj.sucursal_corretor_id"    'Alterado por Gustavo Machado em 10/11/2003
        vSql = vSql & ", c.nome, cs.dt_inicio_corretagem, cs.dt_fim_corretagem"
        vSql = vSql & ", cpj.perc_corretagem , cpj.perc_angariacao "
        ' vSql = vSql & "FROM corretagem_sub_grupo_tb cs   WITH (NOLOCK) , corretagem_pj_sub_grupo_tb cpj    WITH (NOLOCK) , corretor_tb c   WITH (NOLOCK)  "
        vSql = vSql & " FROM corretagem_sub_grupo_tb cs  WITH (NOLOCK)                     "
        vSql = vSql & " INNER JOIN     corretagem_pJ_sub_grupo_tb cpJ   WITH (NOLOCK)      "
        vSql = vSql & "         ON cpj.sub_grupo_id             = cs.sub_grupo_id               "
        vSql = vSql & "         AND cpj.corretor_id             = cs.corretor_id                "
        vSql = vSql & "         AND cpj.sucursal_seguradora_id  = cs.sucursal_seguradora_id"
        vSql = vSql & "         AND cpj.ramo_id                 = cs.ramo_id                    "
        vSql = vSql & "         AND cpj.seguradora_cod_susep    = cs.seguradora_cod_susep"
        vSql = vSql & "         AND cpj.dt_inicio_corretagem    = cs.dt_inicio_corretagem"
        vSql = vSql & "         AND cpj.apolice_id              = cs.apolice_id"
        vSql = vSql & " INNER JOIN      corretor_tb c   WITH (NOLOCK)                         "
        vSql = vSql & "         ON  c.corretor_id               = cs.corretor_id "
        vSql = vSql & "WHERE cs.apolice_id = " & vApoliceId
        vSql = vSql & " AND cs.sucursal_seguradora_id = " & vSucursalId
        vSql = vSql & " AND cs.seguradora_cod_susep = " & vSeguradoraId
        vSql = vSql & " AND cs.ramo_id = " & vRamoId
        vSql = vSql & " AND cs.dt_inicio_corretagem <= '" & dt_inicio_query & "'"
        vSql = vSql & " AND (cs.dt_fim_corretagem is null "
        vSql = vSql & "     OR cs.dt_fim_corretagem > = '" & dt_inicio_query & "')"
        vSql = vSql & " ORDER BY cs.sub_grupo_id, c.corretor_id"

        Set rc = rdocn.OpenResultset(vSql)


        If rc.EOF Then

            vSql = "SELECT cs.sub_grupo_id, c.corretor_susep, c.corretor_id, sucursal_corretor_id = ''"    'Alterado por Gustavo Machado em 10/11/2003
            vSql = vSql & ", c.nome, cs.dt_inicio_corretagem, cs.dt_fim_corretagem      "
            vSql = vSql & ", cpf.perc_corretagem , cpf.perc_angariacao                  "
            vSql = vSql & " FROM corretagem_sub_grupo_tb cs  WITH (NOLOCK)                     "
            vSql = vSql & " INNER JOIN     corretagem_pf_sub_grupo_tb cpf   WITH (NOLOCK)      "
            vSql = vSql & "         ON cpf.sub_grupo_id             = cs.sub_grupo_id               "
            vSql = vSql & "         AND cpf.corretor_id             = cs.corretor_id                "
            vSql = vSql & "         AND cpf.sucursal_seguradora_id  = cs.sucursal_seguradora_id"
            vSql = vSql & "         AND cpf.ramo_id                 = cs.ramo_id                    "
            vSql = vSql & "         AND cpf.seguradora_cod_susep    = cs.seguradora_cod_susep"
            vSql = vSql & "         AND cpf.dt_inicio_corretagem    = cs.dt_inicio_corretagem"
            vSql = vSql & "         AND cpf.apolice_id              = cs.apolice_id"
            vSql = vSql & " INNER JOIN      corretor_tb c   WITH (NOLOCK)                         "
            vSql = vSql & "         ON  c.corretor_id               = cs.corretor_id "
            vSql = vSql & "WHERE    cs.apolice_id               = " & vApoliceId
            vSql = vSql & "     AND cs.sucursal_seguradora_id   = " & vSucursalId
            vSql = vSql & "     AND cs.seguradora_cod_susep     = " & vSeguradoraId
            vSql = vSql & "     AND cs.ramo_id                  = " & vRamoId
            vSql = vSql & "     AND cs.dt_inicio_corretagem     <= '" & dt_inicio_query & "'"
            vSql = vSql & "     AND (cs.dt_fim_corretagem       is null "
            vSql = vSql & "       OR cs.dt_fim_corretagem       >= '" & dt_inicio_query & "')"
            vSql = vSql & " ORDER BY cs.sub_grupo_id"

            Set rc = rdocn.OpenResultset(vSql)
        End If
        '        vSql = vSql & " UNION "
        '        vSql = vSql & "SELECT cs.sub_grupo_id, c.corretor_susep, c.corretor_id, cpj.sucursal_corretor_id" 'Alterado por Gustavo Machado em 10/11/2003
        '        vSql = vSql & ", c.nome, cs.dt_inicio_corretagem, cs.dt_fim_corretagem"
        '        vSql = vSql & ", cpj.perc_corretagem , cpj.perc_angariacao "
        '       ' vSql = vSql & "FROM corretagem_sub_grupo_tb cs   WITH (NOLOCK) , corretagem_pj_sub_grupo_tb cpj    WITH (NOLOCK) , corretor_tb c   WITH (NOLOCK)  "
        '        vSql = vSql & " FROM corretagem_sub_grupo_tb cs  WITH (NOLOCK)                     "
        '        vSql = vSql & " INNER JOIN     corretagem_pJ_sub_grupo_tb cpJ   WITH (NOLOCK)      "
        '        vSql = vSql & "         ON cpj.sub_grupo_id             = cs.sub_grupo_id               "
        '        vSql = vSql & "         AND cpj.corretor_id             = cs.corretor_id                "
        '        vSql = vSql & "         AND cpj.sucursal_seguradora_id  = cs.sucursal_seguradora_id"
        '        vSql = vSql & "         AND cpj.ramo_id                 = cs.ramo_id                    "
        '        vSql = vSql & "         AND cpj.seguradora_cod_susep    = cs.seguradora_cod_susep"
        '        vSql = vSql & "         AND cpj.dt_inicio_corretagem    = cs.dt_inicio_corretagem"
        '        vSql = vSql & "         AND cpj.apolice_id              = cs.apolice_id"
        '         vSql = vSql & " INNER JOIN      corretor_tb c   WITH (NOLOCK)                         "
        '        vSql = vSql & "         ON  c.corretor_id               = cs.corretor_id "
        '         vSql = vSql & "WHERE cs.apolice_id = " & vApoliceId
        '        vSql = vSql & " AND cs.sucursal_seguradora_id = " & vSucursalId
        '        vSql = vSql & " AND cs.seguradora_cod_susep = " & vSeguradoraId
        '        vSql = vSql & " AND cs.ramo_id = " & vRamoId
        '        vSql = vSql & " AND cs.dt_inicio_corretagem <= '" & dt_inicio_query & "'"
        '        vSql = vSql & " AND (cs.dt_fim_corretagem is null "
        '        vSql = vSql & "     OR cs.dt_fim_corretagem > = '" & dt_inicio_query & "')"
        '        vSql = vSql & " ORDER BY cs.sub_grupo_id, c.corretor_id"
        '
        '

        If Not rc.EOF Then
            While Not rc.EOF
                PercCorretagem = IIf(IsNull(rc!perc_corretagem), vValZero, Format(Val(rc!perc_corretagem), "0.00"))
                NomeCorretor = Trim(rc!Nome)
                '
                If (Trim(PercCorretagem) <> "") And (Not ConfiguracaoBrasil) Then
                    PercCorretagem = TrocaValorAmePorBras(PercCorretagem)
                End If
                '
                Set vCorretor = New Corretor
                vCorretor.Id = rc!Corretor_id
                vCorretor.Nome = NomeCorretor
                vCorretor.PercCorretagem = PercCorretagem
                vCorretor.DtIniCorretagem = rc!dt_inicio_corretagem
                'Luciana - 11/11/2003
                'Alessandra Grig�rio - 12/08/2004
                'vCorretor.Codigo_susep = rc!corretor_susep
                vCorretor.Codigo_susep = IIf(IsNull(rc!corretor_susep), "", rc!corretor_susep)
                '
                SubGrupos(CStr(rc!sub_grupo_id)).Corretores.Add vCorretor, CStr(rc!Corretor_id)
                '
                rc.MoveNext
            Wend
        End If
    End If
    rc.Close
    Set rc = Nothing
    '
    Exit Sub

Erro:
    TrataErroGeral "Ler Dados Corretor"
    MsgBox "Rotina: Ler Dados Corretor"
End Sub

Private Sub Ler_Produto()

' Deve ser executada ap�s a Ler_Proposta (produto_id)

    Dim rc As rdoResultset, SQL As String
    'Lpinto - 29/08/2005
    If Not bPropostaNaoEmitida Then
        On Error GoTo Erro

        SQL = "SELECT nome, cobranca = isnull(processa_cobranca,'n') " & _
              "FROM produto_tb    WITH (NOLOCK)    " & _
              "WHERE produto_id = " & vProdutoId

        Set rc = rdocn.OpenResultset(SQL)

        If Not rc.EOF Then
            If Not IsNull(rc!Nome) Then
                txtProduto.Text = Trim(vProdutoId & " - " & rc!Nome)
            End If
            ProcessaCobranca = rc!Cobranca
        End If

        rc.Close
        Set rc = Nothing
    End If
    Exit Sub

Erro:
    TrataErroGeral "Ler Produto"
    MsgBox "Rotina: Ler Produto"
    End

End Sub

Private Sub Ler_Cliente()

    Dim rc As rdoResultset
    Dim SQL As String

    On Error GoTo Erro
    Dim Cnpj As String
    'Renato Vasconcelos
    'MU-2018-004805
    Cnpj = ""
    'INICIO  'alterado 12/09/2013 projeto 17860335 - Melhorias no SEGBR MC_Amaral -confitec

    '    SQL = "SELECT 2 tp_pessoa, pj.cgc cpf_cgc, c.nome, null dt_nasc, null func,"
    '    SQL = SQL & " e.endereco , e.bairro, e.municipio, e.estado, e.cep, c.ddd_1, c.telefone_1 "
    '    SQL = SQL & "FROM pessoa_juridica_tb pj   WITH (NOLOCK)  , proposta_tb p   WITH (NOLOCK) , cliente_tb c   WITH (NOLOCK) , endereco_corresp_tb e   WITH (NOLOCK) "
    '    SQL = SQL & "WHERE p.proposta_id = " & vPropostaId
    '    SQL = SQL & " AND pj.pj_cliente_id = p.prop_cliente_id"
    '    SQL = SQL & " AND c.cliente_id = pj.pj_cliente_id"
    '    SQL = SQL & " AND e.proposta_id =* p.proposta_id"
    '    SQL = SQL & " UNION "
    '    SQL = SQL & " SELECT 1 tp_pessoa, pf.cpf cpf_cgc, c.nome, convert(char(12), pf.dt_nascimento, 103) dt_nasc, funcionario func, "
    '    SQL = SQL & " e.endereco , e.bairro, e.municipio, e.estado, e.cep, c.ddd_1, c.telefone_1 "
    '    SQL = SQL & " FROM    pessoa_fisica_tb pf    WITH (NOLOCK) , proposta_tb p    WITH (NOLOCK) , cliente_tb c    WITH (NOLOCK) , endereco_corresp_tb e    WITH (NOLOCK) "
    '    SQL = SQL & " WHERE p.proposta_id = " & vPropostaId
    '    SQL = SQL & " AND pf.pf_cliente_id = p.prop_cliente_id "
    '    SQL = SQL & " AND c.cliente_id = pf.pf_cliente_id "
    '    SQL = SQL & " AND e.proposta_id =* p.proposta_id"



    SQL = " SELECT 2 tp_pessoa, coalesce(pj.cgc,c.cpf_cnpj) cpf_cgc, c.nome, null dt_nasc, null func,"    'Flow 19711975 - 08/02/2017 - Confitec: Claudia.Silva
    SQL = SQL & " e.endereco , e.bairro, e.municipio, e.estado, e.cep, c.ddd_1, c.telefone_1 "
    SQL = SQL & "       FROM pessoa_juridica_tb pj   WITH (NOLOCK)    "
    SQL = SQL & "       JOIN  proposta_tb p   WITH (NOLOCK) "
    SQL = SQL & "         ON  pj.pj_cliente_id = p.prop_cliente_id"
    SQL = SQL & "       JOIN  cliente_tb c   WITH (NOLOCK) "
    SQL = SQL & "         ON c.cliente_id = pj.pj_cliente_id"
    SQL = SQL & " RIGHT JOIN  endereco_corresp_tb e   WITH (NOLOCK) "
    SQL = SQL & "         ON e.proposta_id = p.proposta_id"
    SQL = SQL & "      WHERE p.proposta_id = " & vPropostaId


    '

    Set rc = rdocn.OpenResultset(SQL)
    If rc.EOF Then
        SQL = " SELECT 1 tp_pessoa, coalesce(pf.cpf,c.cpf_cnpj) cpf_cgc, c.nome, convert(char(12), pf.dt_nascimento, 103) dt_nasc, funcionario func, "    'Flow 19711975 - 08/02/2017 - Confitec: Claudia.Silva
        SQL = SQL & " e.endereco , e.bairro, e.municipio, e.estado, e.cep, c.ddd_1, c.telefone_1 "
        SQL = SQL & " FROM    pessoa_fisica_tb pf    WITH (NOLOCK) "
        SQL = SQL & "       JOIN  proposta_tb p   WITH (NOLOCK) "
        SQL = SQL & "         ON  pf.pf_cliente_id = p.prop_cliente_id"
        SQL = SQL & "       JOIN  cliente_tb c   WITH (NOLOCK) "
        SQL = SQL & "         ON c.cliente_id = pf.pf_cliente_id"
        SQL = SQL & " RIGHT JOIN  endereco_corresp_tb e   WITH (NOLOCK) "
        SQL = SQL & "         ON e.proposta_id = p.proposta_id"
        SQL = SQL & "      WHERE p.proposta_id = " & vPropostaId

        Set rc = rdocn.OpenResultset(SQL)

    End If

    'FIM  'alterado 12/09/2013 projeto 17860335 - Melhorias no SEGBR MC_Amaral -confitec

    '

    If rc.EOF Then
        MsgBox "Cliente da proposta n�o encontrado!", vbCritical
    Else
        If Not IsNull(rc!Nome) Then
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            'carolina.marinho 24/10/2011 - Demanda 11938655
            'Criar ferramenta para identificar exist�ncia de subgrupo (rp) - Confitec RJ
            'conforme especifica��o funcional da demanda, o campo NOME DO PROPONENTE dever�
            'receber o nome do subgrupo
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            If SelPropBasica.cboTipoSelecao.Text = "CNPJ" Or SelPropBasica.cboTipoSelecao.Text = "Raz�o Social" Then
                'txtNome.Text = Trim(rc!Nome)
                txtNome.Text = SelPropBasica.GridProposta.TextMatrix(SelPropBasica.GridProposta.Row, 16)
            Else
                txtNome.Text = Trim(rc!Nome)
            End If
            'carolina.marinho fim

        End If
        '
        If Not IsNull(rc!CPF_CGC) Then
            IdTpPessoa_endosso = rc!tp_pessoa
            If rc!tp_pessoa = 1 Then    ' PF
                Label1(29).Caption = "CPF :"
                CPF_CNPJ_saida = "CPF          :"
                txtCPF_CGC.Text = Format$(rc!CPF_CGC, "&&&.&&&.&&&-&&")
            Else
                Label1(29).Caption = "CNPJ :"
                CPF_CNPJ_saida = "CNPJ         :"
                txtCPF_CGC.Text = Format(rc!CPF_CGC, "&&.&&&.&&&/&&&&-&&")
                'Renato Vasconcelos
                'MU-2018-004805
                Cnpj = rc!CPF_CGC
            End If
        End If
        '
        txtTpPessoaId.Text = rc!tp_pessoa
        txtTpPessoa.Text = IIf(rc!tp_pessoa = 1, "PESSOA F�SICA", "PESSOA JUR�DICA")
        '
        If rc!tp_pessoa = 1 Then    ' PF
            If Not IsNull(rc!dt_nasc) Then
                txtNascimento.Text = Format(rc!dt_nasc, "dd/mm/yyyy")
            End If
        Else    'PJ
            Label1(28).Visible = False
            txtNascimento.Visible = False
        End If
        '
        If Not IsNull(rc!Endereco) Then
            txtEndereco.Text = Trim(rc!Endereco)
        End If
        '
        If Not IsNull(rc!Bairro) Then
            TxtBairro.Text = Trim(rc!Bairro)
        End If
        '
        If Not IsNull(rc!Municipio) Then
            txtCidade.Text = Trim(rc!Municipio)
        End If
        '
        If Not IsNull(rc!Estado) Then
            txtUF.Text = Trim(rc!Estado)
        End If
        '
        If Not IsNull(rc!CEP) Then
            If Trim(rc!CEP) <> "" Then
                txtCEP.Text = Format(rc!CEP, "&&.&&&-&&&")
            End If
        End If
        '
        If Not IsNull(rc!ddd_1) Then
            txtDDD.Text = rc!ddd_1
        End If
        '
        If Not IsNull(rc!telefone_1) Then
            txtTelefone.Text = rc!telefone_1
        End If
        '
        If Not IsNull(rc!func) Then
            If rc!func = "S" Then
                Cat_Cliente = 1
            Else
                Cat_Cliente = 9
            End If
        Else
            Cat_Cliente = 9
        End If
    End If
    '
    rc.Close
    Set rc = Nothing

    'Renato Vasconcelos
    'MU-2018-004805
    'Inicio
    If Cnpj <> "" Then
        SQL = _
        "SELECT " & _
            "   a.CD_ATV_CLI, a.TX_DSC_ATV_CLI " & _
              "From " & _
            "   ALS_OPERACAO_DB..CLIENTE_CTR A " & _
            "   INNER JOIN PROPOSTA_PROCESSO_SUSEP_TB B " & _
              "ON A.CD_PRD = B.COD_PRODUTO " & _
              "AND A.CD_MDLD = B.COD_MODALIDADE " & _
              "AND A.CD_ITEM_MDLD = B.COD_ITEM_MODALIDADE " & _
              "AND A.NR_CTR_SGRO = B.NUM_CONTRATO_SEGURO " & _
              "AND A.NR_VRS_EDS = B.NUM_VERSAO_ENDOSSO " & _
              "Where " & _
              "B.proposta_id = " & txtNumero.Text & " " & _
              "and NR_CPF_CNPJ = '" & Cnpj & "'"



        Set rc = rdocn.OpenResultset(SQL)

        If Not rc.EOF Then
            txtProfissaoCbo.Text = rc!CD_ATV_CLI
            txtProfissao.Text = rc!TX_DSC_ATV_CLI
        End If

        rc.Close
        Set rc = Nothing
    End If
    'Fim
    Exit Sub

Erro:
    TrataErroGeral "Ler Cliente"
    MsgBox "Rotina: Ler Cliente"
    End
End Sub

'Lpinto - 29/09/2005
Private Sub LerPropostaNaoEmitida()

    Dim rc As rdoResultset, rc1 As rdoResultset
    Dim SQL As String
    Dim Sql_Aux As String
    Dim TotParcelado As Double
    Dim ValParcela As Double
    Dim vAuxComissao As String

    On Error GoTo Erro

    If UCase(sArquivo) = "SEG248R" _
       Or UCase(sArquivo) = "BBC441" _
       Or UCase(sArquivo) = "SEG486D" _
       Or UCase(sArquivo) = "BBC493" _
       Or UCase(sArquivo) = "SEGF496" _
       Or UCase(sArquivo) = "BBC492" _
       Or UCase(sArquivo) = "SEG409A" _
       Or UCase(sArquivo) = "SEG248E" _
       Or UCase(sArquivo) = "SEG491D" _
       Or UCase(sArquivo) = "CDR751" _
       Or UCase(sArquivo) = "INTERNET" _
       Or UCase(sArquivo) = "SEG155" _
       Or UCase(sArquivo) = "SEG248M" _
       Or UCase(sArquivo) = "SEG248S" _
       Or UCase(sArquivo) = "SEG409D" _
       Or UCase(sArquivo) = "SEG410" _
       Or UCase(sArquivo) = "SEG411" _
       Or UCase(sArquivo) = "SEG493" _
       Or UCase(sArquivo) = "SEGF495" _
       Or UCase(sArquivo) = "SEGF496" Then

        'INICIO  'alterado 12/09/2013 projeto 17860335 - Melhorias no SEGBR MC_Amaral -confitec

        If UCase(sArquivo) = "SEG491D" _
           Or UCase(sArquivo) = "SEG493" _
           Or UCase(sArquivo) = "SEGF496" _
           Or UCase(sArquivo) = "INTERNET" _
           Or UCase(sArquivo) = "SEG486" _
           Or UCase(sArquivo) = "SEG422" _
           Or UCase(sArquivo) = "SEGF495" Then


            SQL = SQL & "    SELECT proposta_bb,                                                                                                                    " & vbNewLine
            SQL = SQL & "           proposta_bb_anterior,                                                                                                           " & vbNewLine
            SQL = SQL & "           ind_houve_sinistro,                                                                                                             " & vbNewLine
            SQL = SQL & "           seguro_anterior,                                                                                                                " & vbNewLine
            SQL = SQL & "           NULL tp_contrato,                                                                                                               " & vbNewLine
            SQL = SQL & "           agencia_contratante cont_agencia_id,                                                                                            " & vbNewLine
            SQL = SQL & "           NULL  banco_id,                                                                                                                 " & vbNewLine
            SQL = SQL & "           agencia_cliente agencia_id,                                                                                                     " & vbNewLine
            SQL = SQL & "           conta_corrente deb_conta_corrente_id,                                                                                           " & vbNewLine
            SQL = SQL & "           NULL val_comissao,                                                                                                              " & vbNewLine
            SQL = SQL & "           clausula_benef,                                                                                                                 " & vbNewLine
            SQL = SQL & "           outra_seguradora,                                                                                                               " & vbNewLine
            SQL = SQL & "           NULL val_iof_ato,                                                                                                               " & vbNewLine
            SQL = SQL & "           NULL custo_apolice,                                                                                                             " & vbNewLine
            SQL = SQL & "           NULL val_premio_tarifa,                                                                                                         " & vbNewLine
            SQL = SQL & "           NULL desconto_progres,                                                                                                          " & vbNewLine
            SQL = SQL & "           NULL desconto_renov,                                                                                                            " & vbNewLine
            SQL = SQL & "           NULL desconto_outro,                                                                                                            " & vbNewLine
            SQL = SQL & "           NULL desconto_sinistralidade,                                                                                                   " & vbNewLine
            SQL = SQL & "           NULL coef_ajuste,                                                                                                               " & vbNewLine
            SQL = SQL & "           NULL val_premio_bruto,                                                                                                          " & vbNewLine
            SQL = SQL & "           num_parcelas,                                                                                                                   " & vbNewLine
            SQL = SQL & "           NULL val_juros,                                                                                                                 " & vbNewLine
            SQL = SQL & "           NULL val_pgto_ato,                                                                                                              " & vbNewLine
            SQL = SQL & "           NULL val_parcela,                                                                                                               " & vbNewLine
            SQL = SQL & "           tp_pgto forma_pgto_id,                                                                                                          " & vbNewLine
            SQL = SQL & "           dia_debito data_cobranca_dia,                                                                                                   " & vbNewLine
            SQL = SQL & "           NULL val_tot_desconto_comercial,                                                                                                " & vbNewLine
            SQL = SQL & "           NULL taxa_juros,                                                                                                                " & vbNewLine
            SQL = SQL & "           tp_pessoa = CASE WHEN cpf IS NULL AND cgc IS NOT NULL THEN 2                                                                    " & vbNewLine
            SQL = SQL & "                            WHEN cgc IS NULL AND cpf IS NOT NULL THEN 1                                                                    " & vbNewLine
            SQL = SQL & "                            ELSE ''                                                                                                        " & vbNewLine
            SQL = SQL & "                       END,                                                                                                                " & vbNewLine
            SQL = SQL & "           isnull(cgc, cpf) CPF_CGC,                                                                                                       " & vbNewLine
            SQL = SQL & "           proponente nome,                                                                                                                " & vbNewLine
            SQL = SQL & "           dt_nascimento dt_nasc,                                                                                                          " & vbNewLine
            SQL = SQL & "           NULL func,                                                                                                                      " & vbNewLine
            SQL = SQL & "           endereco ,                                                                                                                      " & vbNewLine
            SQL = SQL & "           bairro,                                                                                                                         " & vbNewLine
            SQL = SQL & "           municipio,                                                                                                                      " & vbNewLine
            SQL = SQL & "           estado,                                                                                                                         " & vbNewLine
            SQL = SQL & "           cep,                                                                                                                            " & vbNewLine
            SQL = SQL & "           ddd ddd_1,                                                                                                                      " & vbNewLine
            SQL = SQL & "           telefone telefone_1,                                                                                                             " & vbNewLine
            SQL = SQL & "           NULL produto_id,                                                                                                                " & vbNewLine
            SQL = SQL & "           NULL nome_produto,                                                                                                                " & vbNewLine
            SQL = SQL & "           NULL ramo_id,                                                                                                                " & vbNewLine
            SQL = SQL & "           NULL nome_ramo                                                                                                                " & vbNewLine
            SQL = SQL & "      FROM emi_re_proposta_tb WITH (NOLOCK)                                                                                                       " & vbNewLine
            SQL = SQL & "     WHERE emi_re_proposta_tb.proposta_bb = " & sPropostaBB & vbNewLine
            SQL = SQL & "       AND emi_re_proposta_tb.situacao = 'N'                                                                                               " & vbNewLine
            SQL = SQL & "       AND(emi_re_proposta_tb.arquivo LIKE 'SEG491D%'                                                                                      " & vbNewLine
            SQL = SQL & "           OR emi_re_proposta_tb.arquivo LIKE 'SEG493%'                                                                                    " & vbNewLine
            SQL = SQL & "           OR emi_re_proposta_tb.arquivo LIKE 'SEGF496%'                                                                                   " & vbNewLine
            SQL = SQL & "           OR emi_re_proposta_tb.arquivo LIKE 'INTERNET%'                                                                                  " & vbNewLine
            SQL = SQL & "           OR emi_re_proposta_tb.arquivo LIKE 'SEG486%'                                                                                    " & vbNewLine
            SQL = SQL & "           OR emi_re_proposta_tb.arquivo LIKE 'SEG422%'                                                                                    " & vbNewLine
            SQL = SQL & "        OR emi_re_proposta_tb.arquivo LIKE 'SEGF495%')                                                                                     " & vbNewLine
            'GENJUNIOR - FLOW 17860335 - CONSULTA TABELAS DE HIST�RICO
            SQL = SQL & "  UNION " & vbNewLine
            SQL = SQL & "    SELECT proposta_bb,                                                                                                                    " & vbNewLine
            SQL = SQL & "           proposta_bb_anterior,                                                                                                           " & vbNewLine
            SQL = SQL & "           ind_houve_sinistro,                                                                                                             " & vbNewLine
            SQL = SQL & "           seguro_anterior,                                                                                                                " & vbNewLine
            SQL = SQL & "           NULL tp_contrato,                                                                                                               " & vbNewLine
            SQL = SQL & "           agencia_contratante cont_agencia_id,                                                                                            " & vbNewLine
            SQL = SQL & "           NULL  banco_id,                                                                                                                 " & vbNewLine
            SQL = SQL & "           agencia_cliente agencia_id,                                                                                                     " & vbNewLine
            SQL = SQL & "           conta_corrente deb_conta_corrente_id,                                                                                           " & vbNewLine
            SQL = SQL & "           NULL val_comissao,                                                                                                              " & vbNewLine
            SQL = SQL & "           clausula_benef,                                                                                                                 " & vbNewLine
            SQL = SQL & "           outra_seguradora,                                                                                                               " & vbNewLine
            SQL = SQL & "           NULL val_iof_ato,                                                                                                               " & vbNewLine
            SQL = SQL & "           NULL custo_apolice,                                                                                                             " & vbNewLine
            SQL = SQL & "           NULL val_premio_tarifa,                                                                                                         " & vbNewLine
            SQL = SQL & "           NULL desconto_progres,                                                                                                          " & vbNewLine
            SQL = SQL & "           NULL desconto_renov,                                                                                                            " & vbNewLine
            SQL = SQL & "           NULL desconto_outro,                                                                                                            " & vbNewLine
            SQL = SQL & "           NULL desconto_sinistralidade,                                                                                                   " & vbNewLine
            SQL = SQL & "           NULL coef_ajuste,                                                                                                               " & vbNewLine
            SQL = SQL & "           NULL val_premio_bruto,                                                                                                          " & vbNewLine
            SQL = SQL & "           num_parcelas,                                                                                                                   " & vbNewLine
            SQL = SQL & "           NULL val_juros,                                                                                                                 " & vbNewLine
            SQL = SQL & "           NULL val_pgto_ato,                                                                                                              " & vbNewLine
            SQL = SQL & "           NULL val_parcela,                                                                                                               " & vbNewLine
            SQL = SQL & "           tp_pgto forma_pgto_id,                                                                                                          " & vbNewLine
            SQL = SQL & "           dia_debito data_cobranca_dia,                                                                                                   " & vbNewLine
            SQL = SQL & "           NULL val_tot_desconto_comercial,                                                                                                " & vbNewLine
            SQL = SQL & "           NULL taxa_juros,                                                                                                                " & vbNewLine
            SQL = SQL & "           tp_pessoa = CASE WHEN cpf IS NULL AND cgc IS NOT NULL THEN 2                                                                    " & vbNewLine
            SQL = SQL & "                            WHEN cgc IS NULL AND cpf IS NOT NULL THEN 1                                                                    " & vbNewLine
            SQL = SQL & "                            ELSE ''                                                                                                        " & vbNewLine
            SQL = SQL & "                       END,                                                                                                                " & vbNewLine
            SQL = SQL & "           isnull(cgc, cpf) CPF_CGC,                                                                                                       " & vbNewLine
            SQL = SQL & "           proponente nome,                                                                                                                " & vbNewLine
            SQL = SQL & "           dt_nascimento dt_nasc,                                                                                                          " & vbNewLine
            SQL = SQL & "           NULL func,                                                                                                                      " & vbNewLine
            SQL = SQL & "           endereco ,                                                                                                                      " & vbNewLine
            SQL = SQL & "           bairro,                                                                                                                         " & vbNewLine
            SQL = SQL & "           municipio,                                                                                                                      " & vbNewLine
            SQL = SQL & "           estado,                                                                                                                         " & vbNewLine
            SQL = SQL & "           cep,                                                                                                                            " & vbNewLine
            SQL = SQL & "           ddd ddd_1,                                                                                                                      " & vbNewLine
            SQL = SQL & "           telefone telefone_1,                                                                                                             " & vbNewLine
            SQL = SQL & "           NULL produto_id,                                                                                                                " & vbNewLine
            SQL = SQL & "           NULL nome_produto,                                                                                                                " & vbNewLine
            SQL = SQL & "           NULL ramo_id,                                                                                                                " & vbNewLine
            SQL = SQL & "           NULL nome_ramo                                                                                                                " & vbNewLine
            SQL = SQL & "      FROM seguros_hist_db.dbo.emi_re_proposta_hist_tb emi_re_proposta_hist_tb WITH (NOLOCK)                                                                                                       " & vbNewLine
            SQL = SQL & "     WHERE emi_re_proposta_hist_tb.proposta_bb = " & sPropostaBB & vbNewLine
            SQL = SQL & "       AND emi_re_proposta_hist_tb.situacao = 'N'                                                                                               " & vbNewLine
            SQL = SQL & "       AND(emi_re_proposta_hist_tb.arquivo LIKE 'SEG491D%'                                                                                      " & vbNewLine
            SQL = SQL & "           OR emi_re_proposta_hist_tb.arquivo LIKE 'SEG493%'                                                                                    " & vbNewLine
            SQL = SQL & "           OR emi_re_proposta_hist_tb.arquivo LIKE 'SEGF496%'                                                                                   " & vbNewLine
            SQL = SQL & "           OR emi_re_proposta_hist_tb.arquivo LIKE 'INTERNET%'                                                                                  " & vbNewLine
            SQL = SQL & "           OR emi_re_proposta_hist_tb.arquivo LIKE 'SEG486%'                                                                                    " & vbNewLine
            SQL = SQL & "           OR emi_re_proposta_hist_tb.arquivo LIKE 'SEG422%'                                                                                    " & vbNewLine
            SQL = SQL & "        OR emi_re_proposta_hist_tb.arquivo LIKE 'SEGF495%')                                                                                     " & vbNewLine


            'FIM - GENJUNIOR
            SQL = SQL & "  ORDER BY proposta_bb      "

            Sql_Aux = SQL
            Set rc = rdoCn2.OpenResultset(Sql_Aux)

            If rc.EOF Then

                SQL = ""
                SQL = SQL & "    SELECT proposta_bb,                                                                                                                    " & vbNewLine
                SQL = SQL & "           proposta_bb_anterior,                                                                                                           " & vbNewLine
                SQL = SQL & "           NULL houve_sinistro,                                                                                                            " & vbNewLine
                SQL = SQL & "           seguro_anterior,                                                                                                                " & vbNewLine
                SQL = SQL & "           tp_contrato,                                                                                                                    " & vbNewLine
                SQL = SQL & "           agencia_contratante cont_agencia_id,                                                                                            " & vbNewLine
                SQL = SQL & "           NULL  banco_id,                                                                                                                 " & vbNewLine
                SQL = SQL & "           agencia_cliente agencia_id,                                                                                                     " & vbNewLine
                SQL = SQL & "           conta_corrente deb_conta_corrente_id,                                                                                           " & vbNewLine
                SQL = SQL & "           NULL val_comissao,                                                                                                              " & vbNewLine
                SQL = SQL & "           clausula_benef,                                                                                                                 " & vbNewLine
                SQL = SQL & "           NULL outra_seguradora,                                                                                                          " & vbNewLine
                SQL = SQL & "           NULL val_iof,                                                                                                                   " & vbNewLine
                SQL = SQL & "           NULL custo_apolice,                                                                                                             " & vbNewLine
                SQL = SQL & "           NULL val_premio_tarifa,                                                                                                         " & vbNewLine
                SQL = SQL & "           NULL desconto_progres,                                                                                                          " & vbNewLine
                SQL = SQL & "           NULL desconto_renov,                                                                                                            " & vbNewLine
                SQL = SQL & "           NULL desconto_outro,                                                                                                            " & vbNewLine
                SQL = SQL & "           NULL desconto_sinistralidade,                                                                                                   " & vbNewLine
                SQL = SQL & "           NULL coef_ajuste,                                                                                                               " & vbNewLine
                SQL = SQL & "           NULL val_premio_bruto,                                                                                                          " & vbNewLine
                SQL = SQL & "           num_parcelas,                                                                                                                   " & vbNewLine
                SQL = SQL & "           NULL val_juros,                                                                                                                 " & vbNewLine
                SQL = SQL & "           NULL val_pgto_ato,                                                                                                              " & vbNewLine
                SQL = SQL & "           NULL val_parcela,                                                                                                               " & vbNewLine
                SQL = SQL & "           tp_pgto forma_pgto_id,                                                                                                          " & vbNewLine
                SQL = SQL & "           NULL data_cobranca_dia,                                                                                                         " & vbNewLine
                SQL = SQL & "           NULL val_tot_desconto_comercial,                                                                                                " & vbNewLine
                SQL = SQL & "           NULL taxa_juros,                                                                                                                " & vbNewLine
                SQL = SQL & "           tp_pessoa = CASE WHEN cpf IS NULL AND cgc IS NOT NULL THEN 2                                                                    " & vbNewLine
                SQL = SQL & "                            WHEN cgc IS NULL AND cpf IS NOT NULL THEN 1                                                                    " & vbNewLine
                SQL = SQL & "                            ELSE ''                                                                                                        " & vbNewLine
                SQL = SQL & "                       END,                                                                                                                " & vbNewLine
                SQL = SQL & "           ISNULL(cgc, cpf) CPF_CGC,                                                                                                       " & vbNewLine
                SQL = SQL & "           proponente nome,                                                                                                                " & vbNewLine
                SQL = SQL & "           dt_nascimento dt_nasc,                                                                                                          " & vbNewLine
                SQL = SQL & "           NULL func,                                                                                                                      " & vbNewLine
                SQL = SQL & "           endereco ,                                                                                                                      " & vbNewLine
                SQL = SQL & "           bairro,                                                                                                                         " & vbNewLine
                SQL = SQL & "           municipio,                                                                                                                      " & vbNewLine
                SQL = SQL & "           estado,                                                                                                                         " & vbNewLine
                SQL = SQL & "           cep,                                                                                                                            " & vbNewLine
                SQL = SQL & "           ddd ddd_1,                                                                                                                      " & vbNewLine
                SQL = SQL & "           telefone telefone_1,                                                                                                            " & vbNewLine
                SQL = SQL & "           NULL produto_id,                                                                                                                " & vbNewLine
                SQL = SQL & "           NULL nome_produto,                                                                                                                " & vbNewLine
                SQL = SQL & "           NULL ramo_id,                                                                                                                " & vbNewLine
                SQL = SQL & "           NULL nome_ramo                                                                                                                " & vbNewLine
                SQL = SQL & "      FROM emi_proposta_tb WITH (NOLOCK)                                                                                                          " & vbNewLine
                SQL = SQL & "     WHERE emi_proposta_tb.proposta_bb = " & sPropostaBB & vbNewLine
                SQL = SQL & "       AND emi_proposta_tb.situacao = 'N'    "
                'GENJUNIOR - FLOW 17860335 - CONSULTA TABELAS DE HIST�RICO
                SQL = SQL & "    UNION " & vbNewLine
                SQL = SQL & "    SELECT proposta_bb,                                                                                                                    " & vbNewLine
                SQL = SQL & "           proposta_bb_anterior,                                                                                                           " & vbNewLine
                SQL = SQL & "           NULL houve_sinistro,                                                                                                            " & vbNewLine
                SQL = SQL & "           seguro_anterior,                                                                                                                " & vbNewLine
                SQL = SQL & "           tp_contrato,                                                                                                                    " & vbNewLine
                SQL = SQL & "           agencia_contratante cont_agencia_id,                                                                                            " & vbNewLine
                SQL = SQL & "           NULL  banco_id,                                                                                                                 " & vbNewLine
                SQL = SQL & "           agencia_cliente agencia_id,                                                                                                     " & vbNewLine
                SQL = SQL & "           conta_corrente deb_conta_corrente_id,                                                                                           " & vbNewLine
                SQL = SQL & "           NULL val_comissao,                                                                                                              " & vbNewLine
                SQL = SQL & "           clausula_benef,                                                                                                                 " & vbNewLine
                SQL = SQL & "           NULL outra_seguradora,                                                                                                          " & vbNewLine
                SQL = SQL & "           NULL val_iof,                                                                                                                   " & vbNewLine
                SQL = SQL & "           NULL custo_apolice,                                                                                                             " & vbNewLine
                SQL = SQL & "           NULL val_premio_tarifa,                                                                                                         " & vbNewLine
                SQL = SQL & "           NULL desconto_progres,                                                                                                          " & vbNewLine
                SQL = SQL & "           NULL desconto_renov,                                                                                                            " & vbNewLine
                SQL = SQL & "           NULL desconto_outro,                                                                                                            " & vbNewLine
                SQL = SQL & "           NULL desconto_sinistralidade,                                                                                                   " & vbNewLine
                SQL = SQL & "           NULL coef_ajuste,                                                                                                               " & vbNewLine
                SQL = SQL & "           NULL val_premio_bruto,                                                                                                          " & vbNewLine
                SQL = SQL & "           num_parcelas,                                                                                                                   " & vbNewLine
                SQL = SQL & "           NULL val_juros,                                                                                                                 " & vbNewLine
                SQL = SQL & "           NULL val_pgto_ato,                                                                                                              " & vbNewLine
                SQL = SQL & "           NULL val_parcela,                                                                                                               " & vbNewLine
                SQL = SQL & "           tp_pgto forma_pgto_id,                                                                                                          " & vbNewLine
                SQL = SQL & "           NULL data_cobranca_dia,                                                                                                         " & vbNewLine
                SQL = SQL & "           NULL val_tot_desconto_comercial,                                                                                                " & vbNewLine
                SQL = SQL & "           NULL taxa_juros,                                                                                                                " & vbNewLine
                SQL = SQL & "           tp_pessoa = CASE WHEN cpf IS NULL AND cgc IS NOT NULL THEN 2                                                                    " & vbNewLine
                SQL = SQL & "                            WHEN cgc IS NULL AND cpf IS NOT NULL THEN 1                                                                    " & vbNewLine
                SQL = SQL & "                            ELSE ''                                                                                                        " & vbNewLine
                SQL = SQL & "                       END,                                                                                                                " & vbNewLine
                SQL = SQL & "           ISNULL(cgc, cpf) CPF_CGC,                                                                                                       " & vbNewLine
                SQL = SQL & "           proponente nome,                                                                                                                " & vbNewLine
                SQL = SQL & "           dt_nascimento dt_nasc,                                                                                                          " & vbNewLine
                SQL = SQL & "           NULL func,                                                                                                                      " & vbNewLine
                SQL = SQL & "           endereco ,                                                                                                                      " & vbNewLine
                SQL = SQL & "           bairro,                                                                                                                         " & vbNewLine
                SQL = SQL & "           municipio,                                                                                                                      " & vbNewLine
                SQL = SQL & "           estado,                                                                                                                         " & vbNewLine
                SQL = SQL & "           cep,                                                                                                                            " & vbNewLine
                SQL = SQL & "           ddd ddd_1,                                                                                                                      " & vbNewLine
                SQL = SQL & "           telefone telefone_1,                                                                                                            " & vbNewLine
                SQL = SQL & "           NULL produto_id,                                                                                                                " & vbNewLine
                SQL = SQL & "           NULL nome_produto,                                                                                                                " & vbNewLine
                SQL = SQL & "           NULL ramo_id,                                                                                                                " & vbNewLine
                SQL = SQL & "           NULL nome_ramo                                                                                                                " & vbNewLine
                SQL = SQL & "      FROM seguros_hist_db.dbo.emi_proposta_hist_tb emi_proposta_hist_tb WITH (NOLOCK)                                                                                                          " & vbNewLine
                SQL = SQL & "     WHERE emi_proposta_hist_tb.proposta_bb = " & sPropostaBB & vbNewLine
                SQL = SQL & "       AND emi_proposta_hist_tb.situacao = 'N'    "

                'FIM GENJUNIOR

            End If
        Else
            SQL = ""
            SQL = SQL & "    SELECT proposta_bb,                                                                                                                    " & vbNewLine
            SQL = SQL & "           proposta_bb_anterior,                                                                                                           " & vbNewLine
            SQL = SQL & "           NULL houve_sinistro,                                                                                                            " & vbNewLine
            SQL = SQL & "           seguro_anterior,                                                                                                                " & vbNewLine
            SQL = SQL & "           tp_contrato,                                                                                                                    " & vbNewLine
            SQL = SQL & "           agencia_contratante cont_agencia_id,                                                                                            " & vbNewLine
            SQL = SQL & "           NULL  banco_id,                                                                                                                 " & vbNewLine
            SQL = SQL & "           agencia_cliente agencia_id,                                                                                                     " & vbNewLine
            SQL = SQL & "           conta_corrente deb_conta_corrente_id,                                                                                           " & vbNewLine
            SQL = SQL & "           NULL val_comissao,                                                                                                              " & vbNewLine
            SQL = SQL & "           clausula_benef,                                                                                                                 " & vbNewLine
            SQL = SQL & "           NULL outra_seguradora,                                                                                                          " & vbNewLine
            SQL = SQL & "           NULL val_iof,                                                                                                                   " & vbNewLine
            SQL = SQL & "           NULL custo_apolice,                                                                                                             " & vbNewLine
            SQL = SQL & "           NULL val_premio_tarifa,                                                                                                         " & vbNewLine
            SQL = SQL & "           NULL desconto_progres,                                                                                                          " & vbNewLine
            SQL = SQL & "           NULL desconto_renov,                                                                                                            " & vbNewLine
            SQL = SQL & "           NULL desconto_outro,                                                                                                            " & vbNewLine
            SQL = SQL & "           NULL desconto_sinistralidade,                                                                                                   " & vbNewLine
            SQL = SQL & "           NULL coef_ajuste,                                                                                                               " & vbNewLine
            SQL = SQL & "           NULL val_premio_bruto,                                                                                                          " & vbNewLine
            SQL = SQL & "           num_parcelas,                                                                                                                   " & vbNewLine
            SQL = SQL & "           NULL val_juros,                                                                                                                 " & vbNewLine
            SQL = SQL & "           NULL val_pgto_ato,                                                                                                              " & vbNewLine
            SQL = SQL & "           NULL val_parcela,                                                                                                               " & vbNewLine
            SQL = SQL & "           tp_pgto forma_pgto_id,                                                                                                          " & vbNewLine
            SQL = SQL & "           NULL data_cobranca_dia,                                                                                                         " & vbNewLine
            SQL = SQL & "           NULL val_tot_desconto_comercial,                                                                                                " & vbNewLine
            SQL = SQL & "           NULL taxa_juros,                                                                                                                " & vbNewLine
            SQL = SQL & "           tp_pessoa = CASE WHEN cpf IS NULL AND cgc IS NOT NULL THEN 2                                                                    " & vbNewLine
            SQL = SQL & "                            WHEN cgc IS NULL AND cpf IS NOT NULL THEN 1                                                                    " & vbNewLine
            SQL = SQL & "                            ELSE ''                                                                                                        " & vbNewLine
            SQL = SQL & "                       END,                                                                                                                " & vbNewLine
            SQL = SQL & "           ISNULL(cgc, cpf) CPF_CGC,                                                                                                       " & vbNewLine
            SQL = SQL & "           proponente nome,                                                                                                                " & vbNewLine
            SQL = SQL & "           dt_nascimento dt_nasc,                                                                                                          " & vbNewLine
            SQL = SQL & "           NULL func,                                                                                                                      " & vbNewLine
            SQL = SQL & "           endereco ,                                                                                                                      " & vbNewLine
            SQL = SQL & "           bairro,                                                                                                                         " & vbNewLine
            SQL = SQL & "           municipio,                                                                                                                      " & vbNewLine
            SQL = SQL & "           estado,                                                                                                                         " & vbNewLine
            SQL = SQL & "           cep,                                                                                                                            " & vbNewLine
            SQL = SQL & "           ddd ddd_1,                                                                                                                      " & vbNewLine
            SQL = SQL & "           telefone telefone_1,                                                                                                            " & vbNewLine
            SQL = SQL & "           NULL produto_id,                                                                                                                " & vbNewLine
            SQL = SQL & "           NULL nome_produto,                                                                                                                " & vbNewLine
            SQL = SQL & "           NULL ramo_id,                                                                                                                " & vbNewLine
            SQL = SQL & "           NULL nome_ramo                                                                                                                " & vbNewLine
            SQL = SQL & "      FROM emi_proposta_tb WITH (NOLOCK)                                                                                                          " & vbNewLine
            SQL = SQL & "     WHERE emi_proposta_tb.proposta_bb = " & sPropostaBB & vbNewLine
            SQL = SQL & "       AND emi_proposta_tb.situacao = 'N'                                                                                                  " & vbNewLine
            'GENJUNIOR - FLOW 17860335 - CONSULTA TABELAS DE HIST�RICO
            SQL = SQL & "  UNION " & vbNewLine
            SQL = SQL & "    SELECT proposta_bb,                                                                                                                    " & vbNewLine
            SQL = SQL & "           proposta_bb_anterior,                                                                                                           " & vbNewLine
            SQL = SQL & "           NULL houve_sinistro,                                                                                                            " & vbNewLine
            SQL = SQL & "           seguro_anterior,                                                                                                                " & vbNewLine
            SQL = SQL & "           tp_contrato,                                                                                                                    " & vbNewLine
            SQL = SQL & "           agencia_contratante cont_agencia_id,                                                                                            " & vbNewLine
            SQL = SQL & "           NULL  banco_id,                                                                                                                 " & vbNewLine
            SQL = SQL & "           agencia_cliente agencia_id,                                                                                                     " & vbNewLine
            SQL = SQL & "           conta_corrente deb_conta_corrente_id,                                                                                           " & vbNewLine
            SQL = SQL & "           NULL val_comissao,                                                                                                              " & vbNewLine
            SQL = SQL & "           clausula_benef,                                                                                                                 " & vbNewLine
            SQL = SQL & "           NULL outra_seguradora,                                                                                                          " & vbNewLine
            SQL = SQL & "           NULL val_iof,                                                                                                                   " & vbNewLine
            SQL = SQL & "           NULL custo_apolice,                                                                                                             " & vbNewLine
            SQL = SQL & "           NULL val_premio_tarifa,                                                                                                         " & vbNewLine
            SQL = SQL & "           NULL desconto_progres,                                                                                                          " & vbNewLine
            SQL = SQL & "           NULL desconto_renov,                                                                                                            " & vbNewLine
            SQL = SQL & "           NULL desconto_outro,                                                                                                            " & vbNewLine
            SQL = SQL & "           NULL desconto_sinistralidade,                                                                                                   " & vbNewLine
            SQL = SQL & "           NULL coef_ajuste,                                                                                                               " & vbNewLine
            SQL = SQL & "           NULL val_premio_bruto,                                                                                                          " & vbNewLine
            SQL = SQL & "           num_parcelas,                                                                                                                   " & vbNewLine
            SQL = SQL & "           NULL val_juros,                                                                                                                 " & vbNewLine
            SQL = SQL & "           NULL val_pgto_ato,                                                                                                              " & vbNewLine
            SQL = SQL & "           NULL val_parcela,                                                                                                               " & vbNewLine
            SQL = SQL & "           tp_pgto forma_pgto_id,                                                                                                          " & vbNewLine
            SQL = SQL & "           NULL data_cobranca_dia,                                                                                                         " & vbNewLine
            SQL = SQL & "           NULL val_tot_desconto_comercial,                                                                                                " & vbNewLine
            SQL = SQL & "           NULL taxa_juros,                                                                                                                " & vbNewLine
            SQL = SQL & "           tp_pessoa = CASE WHEN cpf IS NULL AND cgc IS NOT NULL THEN 2                                                                    " & vbNewLine
            SQL = SQL & "                            WHEN cgc IS NULL AND cpf IS NOT NULL THEN 1                                                                    " & vbNewLine
            SQL = SQL & "                            ELSE ''                                                                                                        " & vbNewLine
            SQL = SQL & "                       END,                                                                                                                " & vbNewLine
            SQL = SQL & "           ISNULL(cgc, cpf) CPF_CGC,                                                                                                       " & vbNewLine
            SQL = SQL & "           proponente nome,                                                                                                                " & vbNewLine
            SQL = SQL & "           dt_nascimento dt_nasc,                                                                                                          " & vbNewLine
            SQL = SQL & "           NULL func,                                                                                                                      " & vbNewLine
            SQL = SQL & "           endereco ,                                                                                                                      " & vbNewLine
            SQL = SQL & "           bairro,                                                                                                                         " & vbNewLine
            SQL = SQL & "           municipio,                                                                                                                      " & vbNewLine
            SQL = SQL & "           estado,                                                                                                                         " & vbNewLine
            SQL = SQL & "           cep,                                                                                                                            " & vbNewLine
            SQL = SQL & "           ddd ddd_1,                                                                                                                      " & vbNewLine
            SQL = SQL & "           telefone telefone_1,                                                                                                            " & vbNewLine
            SQL = SQL & "           NULL produto_id,                                                                                                                " & vbNewLine
            SQL = SQL & "           NULL nome_produto,                                                                                                                " & vbNewLine
            SQL = SQL & "           NULL ramo_id,                                                                                                                " & vbNewLine
            SQL = SQL & "           NULL nome_ramo                                                                                                                " & vbNewLine
            SQL = SQL & "      FROM seguros_hist_db.dbo.emi_proposta_hist_tb emi_proposta_hist_tb WITH (NOLOCK)                                                                                                          " & vbNewLine
            SQL = SQL & "     WHERE emi_proposta_hist_tb.proposta_bb = " & sPropostaBB & vbNewLine
            SQL = SQL & "       AND emi_proposta_hist_tb.situacao = 'N' "

            'FIM GENJUNIOR


            ' SQL = SQL & "                                                                                                                                           " & vbNewLine
            '        SQL = SQL & "UNION ALL                                                                                                                                  " & vbNewLine
            '        SQL = SQL & "                                                                                                                                           " & vbNewLine
            '        SQL = SQL & "    SELECT proposta_bb,                                                                                                                    " & vbNewLine
            '        SQL = SQL & "           proposta_bb_anterior,                                                                                                           " & vbNewLine
            '        SQL = SQL & "           ind_houve_sinistro,                                                                                                             " & vbNewLine
            '        SQL = SQL & "           seguro_anterior,                                                                                                                " & vbNewLine
            '        SQL = SQL & "           NULL tp_contrato,                                                                                                               " & vbNewLine
            '        SQL = SQL & "           agencia_contratante cont_agencia_id,                                                                                            " & vbNewLine
            '        SQL = SQL & "           NULL  banco_id,                                                                                                                 " & vbNewLine
            '        SQL = SQL & "           agencia_cliente agencia_id,                                                                                                     " & vbNewLine
            '        SQL = SQL & "           conta_corrente deb_conta_corrente_id,                                                                                           " & vbNewLine
            '        SQL = SQL & "           NULL val_comissao,                                                                                                              " & vbNewLine
            '        SQL = SQL & "           clausula_benef,                                                                                                                 " & vbNewLine
            '        SQL = SQL & "           outra_seguradora,                                                                                                               " & vbNewLine
            '        SQL = SQL & "           NULL val_iof_ato,                                                                                                               " & vbNewLine
            '        SQL = SQL & "           NULL custo_apolice,                                                                                                             " & vbNewLine
            '        SQL = SQL & "           NULL val_premio_tarifa,                                                                                                         " & vbNewLine
            '        SQL = SQL & "           NULL desconto_progres,                                                                                                          " & vbNewLine
            '        SQL = SQL & "           NULL desconto_renov,                                                                                                            " & vbNewLine
            '        SQL = SQL & "           NULL desconto_outro,                                                                                                            " & vbNewLine
            '        SQL = SQL & "           NULL desconto_sinistralidade,                                                                                                   " & vbNewLine
            '        SQL = SQL & "           NULL coef_ajuste,                                                                                                               " & vbNewLine
            '        SQL = SQL & "           NULL val_premio_bruto,                                                                                                          " & vbNewLine
            '        SQL = SQL & "           num_parcelas,                                                                                                                   " & vbNewLine
            '        SQL = SQL & "           NULL val_juros,                                                                                                                 " & vbNewLine
            '        SQL = SQL & "           NULL val_pgto_ato,                                                                                                              " & vbNewLine
            '        SQL = SQL & "           NULL val_parcela,                                                                                                               " & vbNewLine
            '        SQL = SQL & "           tp_pgto forma_pgto_id,                                                                                                          " & vbNewLine
            '        SQL = SQL & "           dia_debito data_cobranca_dia,                                                                                                   " & vbNewLine
            '        SQL = SQL & "           NULL val_tot_desconto_comercial,                                                                                                " & vbNewLine
            '        SQL = SQL & "           NULL taxa_juros,                                                                                                                " & vbNewLine
            '        SQL = SQL & "           tp_pessoa = CASE WHEN cpf IS NULL AND cgc IS NOT NULL THEN 2                                                                    " & vbNewLine
            '        SQL = SQL & "                            WHEN cgc IS NULL AND cpf IS NOT NULL THEN 1                                                                    " & vbNewLine
            '        SQL = SQL & "                            ELSE ''                                                                                                        " & vbNewLine
            '        SQL = SQL & "                       END,                                                                                                                " & vbNewLine
            '        SQL = SQL & "           isnull(cgc, cpf) CPF_CGC,                                                                                                       " & vbNewLine
            '        SQL = SQL & "           proponente nome,                                                                                                                " & vbNewLine
            '        SQL = SQL & "           dt_nascimento dt_nasc,                                                                                                          " & vbNewLine
            '        SQL = SQL & "           NULL func,                                                                                                                      " & vbNewLine
            '        SQL = SQL & "           endereco ,                                                                                                                      " & vbNewLine
            '        SQL = SQL & "           bairro,                                                                                                                         " & vbNewLine
            '        SQL = SQL & "           municipio,                                                                                                                      " & vbNewLine
            '        SQL = SQL & "           estado,                                                                                                                         " & vbNewLine
            '        SQL = SQL & "           cep,                                                                                                                            " & vbNewLine
            '        SQL = SQL & "           ddd ddd_1,                                                                                                                      " & vbNewLine
            '        SQL = SQL & "           telefone telefone_1,                                                                                                             " & vbNewLine
            '        SQL = SQL & "           NULL produto_id,                                                                                                                " & vbNewLine
            '        SQL = SQL & "           NULL nome_produto,                                                                                                                " & vbNewLine
            '        SQL = SQL & "           NULL ramo_id,                                                                                                                " & vbNewLine
            '        SQL = SQL & "           NULL nome_ramo                                                                                                                " & vbNewLine
            '        SQL = SQL & "      FROM emi_re_proposta_tb WITH (NOLOCK)                                                                                                       " & vbNewLine
            '        SQL = SQL & "     WHERE emi_re_proposta_tb.proposta_bb = " & sPropostaBB & vbNewLine
            '        SQL = SQL & "       AND emi_re_proposta_tb.situacao = 'N'                                                                                               " & vbNewLine
            '        SQL = SQL & "       AND(emi_re_proposta_tb.arquivo LIKE 'SEG491D%'                                                                                      " & vbNewLine
            '        SQL = SQL & "           OR emi_re_proposta_tb.arquivo LIKE 'SEG493%'                                                                                    " & vbNewLine
            '        SQL = SQL & "           OR emi_re_proposta_tb.arquivo LIKE 'SEGF496%'                                                                                   " & vbNewLine
            '        SQL = SQL & "           OR emi_re_proposta_tb.arquivo LIKE 'INTERNET%'                                                                                  " & vbNewLine
            '        SQL = SQL & "           OR emi_re_proposta_tb.arquivo LIKE 'SEG486%'                                                                                    " & vbNewLine
            '        SQL = SQL & "           OR emi_re_proposta_tb.arquivo LIKE 'SEG422%'                                                                                    " & vbNewLine
            '        SQL = SQL & "        OR emi_re_proposta_tb.arquivo LIKE 'SEGF495%')                                                                                     " & vbNewLine
            '        SQL = SQL & "  ORDER BY proposta_bb                                                                                                                     " & vbNewLine
        End If
    Else

        SQL = ""
        SQL = SQL & "    SELECT proposta_bb,                                                                                                                  " & vbNewLine
        SQL = SQL & "           proposta_bb_anterior,                                                                                                         " & vbNewLine
        SQL = SQL & "           NULL houve_sinistro,                                                                                                          " & vbNewLine
        SQL = SQL & "           seguro_anterior,                                                                                                              " & vbNewLine
        SQL = SQL & "           tp_contrato,                                                                                                                  " & vbNewLine
        SQL = SQL & "           agencia_contratante cont_agencia_id,                                                                                          " & vbNewLine
        SQL = SQL & "           NULL  banco_id,                                                                                                               " & vbNewLine
        SQL = SQL & "           agencia_cliente agencia_id,                                                                                                   " & vbNewLine
        SQL = SQL & "           conta_corrente deb_conta_corrente_id,                                                                                         " & vbNewLine
        SQL = SQL & "           NULL val_comissao,                                                                                                            " & vbNewLine
        SQL = SQL & "           clausula_benef,                                                                                                               " & vbNewLine
        SQL = SQL & "           NULL outra_seguradora,                                                                                                        " & vbNewLine
        SQL = SQL & "           NULL val_iof,                                                                                                                 " & vbNewLine
        SQL = SQL & "           NULL custo_apolice,                                                                                                           " & vbNewLine
        SQL = SQL & "           NULL val_premio_tarifa,                                                                                                       " & vbNewLine
        SQL = SQL & "           NULL desconto_progres,                                                                                                        " & vbNewLine
        SQL = SQL & "           NULL desconto_renov,                                                                                                          " & vbNewLine
        SQL = SQL & "           NULL desconto_outro,                                                                                                          " & vbNewLine
        SQL = SQL & "           NULL desconto_sinistralidade,                                                                                                 " & vbNewLine
        SQL = SQL & "           NULL coef_ajuste,                                                                                                             " & vbNewLine
        SQL = SQL & "           NULL val_premio_bruto,                                                                                                        " & vbNewLine
        SQL = SQL & "           num_parcelas,                                                                                                                 " & vbNewLine
        SQL = SQL & "           NULL val_juros,                                                                                                               " & vbNewLine
        SQL = SQL & "           NULL val_pgto_ato,                                                                                                            " & vbNewLine
        SQL = SQL & "           NULL val_parcela,                                                                                                             " & vbNewLine
        SQL = SQL & "           tp_pgto forma_pgto_id,                                                                                                        " & vbNewLine
        SQL = SQL & "           NULL data_cobranca_dia,                                                                                                       " & vbNewLine
        SQL = SQL & "           NULL val_tot_desconto_comercial,                                                                                              " & vbNewLine
        SQL = SQL & "           NULL taxa_juros,                                                                                                              " & vbNewLine
        SQL = SQL & "           tp_pessoa = CASE WHEN cpf IS NULL AND cgc IS NOT NULL THEN 2                                                                  " & vbNewLine
        SQL = SQL & "                            WHEN cgc IS NULL AND cpf IS NOT NULL THEN 1                                                                  " & vbNewLine
        SQL = SQL & "                            ELSE ''                                                                                                      " & vbNewLine
        SQL = SQL & "                       END,                                                                                                              " & vbNewLine
        SQL = SQL & "           ISNULL(cgc, cpf) CPF_CGC,                                                                                                     " & vbNewLine
        SQL = SQL & "           proponente nome,                                                                                                              " & vbNewLine
        SQL = SQL & "           dt_nascimento dt_nasc,                                                                                                        " & vbNewLine
        SQL = SQL & "           NULL func,                                                                                                                    " & vbNewLine
        SQL = SQL & "           endereco ,                                                                                                                    " & vbNewLine
        SQL = SQL & "           bairro,                                                                                                                       " & vbNewLine
        SQL = SQL & "           municipio,                                                                                                                    " & vbNewLine
        SQL = SQL & "           estado,                                                                                                                       " & vbNewLine
        SQL = SQL & "           cep,                                                                                                                          " & vbNewLine
        SQL = SQL & "           ddd ddd_1,                                                                                                                    " & vbNewLine
        SQL = SQL & "           telefone telefone_1,                                                                                                          " & vbNewLine
        SQL = SQL & "           produto_tb.produto_id,                                                                                                        " & vbNewLine
        SQL = SQL & "           produto_tb.nome nome_produto,                                                                                                 " & vbNewLine
        SQL = SQL & "           ramo_tb.ramo_id,                                                                                                              " & vbNewLine
        SQL = SQL & "           ramo_tb.nome nome_ramo                                                                                                        " & vbNewLine
        SQL = SQL & "      FROM emi_proposta_tb WITH (NOLOCK)                                                                                                        " & vbNewLine
        SQL = SQL & "INNER JOIN arquivo_produto_tb WITH (NOLOCK)                                                                                                            " & vbNewLine
        SQL = SQL & "        ON arquivo_produto_tb.produto_externo_id = emi_proposta_tb.produto_externo                                                       " & vbNewLine
        SQL = SQL & "       AND arquivo_produto_tb.arquivo = SUBSTRING(emi_proposta_tb.arquivo,1, CHARINDEX('.' ,emi_proposta_tb.arquivo) - 1)                " & vbNewLine
        SQL = SQL & "INNER JOIN produto_tb WITH (NOLOCK)                                                                                                                     " & vbNewLine
        SQL = SQL & "        ON produto_tb.produto_id = arquivo_produto_tb.produto_id                                                                         " & vbNewLine
        SQL = SQL & "INNER JOIN ramo_tb WITH (NOLOCK)                                                                                                                        " & vbNewLine
        SQL = SQL & "        ON ramo_tb.ramo_id = arquivo_produto_tb.ramo_id                                                                                  " & vbNewLine
        SQL = SQL & "     WHERE emi_proposta_tb.proposta_bb = " & sPropostaBB & vbNewLine
        SQL = SQL & "       AND emi_proposta_tb.situacao = 'N'                                                                                                " & vbNewLine
        'GENJUNIOR - FLOW 17860335 - CONSULTA TABELAS DE HIST�RICO
        SQL = SQL & "  UNION " & vbNewLine
        SQL = SQL & "    SELECT proposta_bb,                                                                                                                  " & vbNewLine
        SQL = SQL & "           proposta_bb_anterior,                                                                                                         " & vbNewLine
        SQL = SQL & "           NULL houve_sinistro,                                                                                                          " & vbNewLine
        SQL = SQL & "           seguro_anterior,                                                                                                              " & vbNewLine
        SQL = SQL & "           tp_contrato,                                                                                                                  " & vbNewLine
        SQL = SQL & "           agencia_contratante cont_agencia_id,                                                                                          " & vbNewLine
        SQL = SQL & "           NULL  banco_id,                                                                                                               " & vbNewLine
        SQL = SQL & "           agencia_cliente agencia_id,                                                                                                   " & vbNewLine
        SQL = SQL & "           conta_corrente deb_conta_corrente_id,                                                                                         " & vbNewLine
        SQL = SQL & "           NULL val_comissao,                                                                                                            " & vbNewLine
        SQL = SQL & "           clausula_benef,                                                                                                               " & vbNewLine
        SQL = SQL & "           NULL outra_seguradora,                                                                                                        " & vbNewLine
        SQL = SQL & "           NULL val_iof,                                                                                                                 " & vbNewLine
        SQL = SQL & "           NULL custo_apolice,                                                                                                           " & vbNewLine
        SQL = SQL & "           NULL val_premio_tarifa,                                                                                                       " & vbNewLine
        SQL = SQL & "           NULL desconto_progres,                                                                                                        " & vbNewLine
        SQL = SQL & "           NULL desconto_renov,                                                                                                          " & vbNewLine
        SQL = SQL & "           NULL desconto_outro,                                                                                                          " & vbNewLine
        SQL = SQL & "           NULL desconto_sinistralidade,                                                                                                 " & vbNewLine
        SQL = SQL & "           NULL coef_ajuste,                                                                                                             " & vbNewLine
        SQL = SQL & "           NULL val_premio_bruto,                                                                                                        " & vbNewLine
        SQL = SQL & "           num_parcelas,                                                                                                                 " & vbNewLine
        SQL = SQL & "           NULL val_juros,                                                                                                               " & vbNewLine
        SQL = SQL & "           NULL val_pgto_ato,                                                                                                            " & vbNewLine
        SQL = SQL & "           NULL val_parcela,                                                                                                             " & vbNewLine
        SQL = SQL & "           tp_pgto forma_pgto_id,                                                                                                        " & vbNewLine
        SQL = SQL & "           NULL data_cobranca_dia,                                                                                                       " & vbNewLine
        SQL = SQL & "           NULL val_tot_desconto_comercial,                                                                                              " & vbNewLine
        SQL = SQL & "           NULL taxa_juros,                                                                                                              " & vbNewLine
        SQL = SQL & "           tp_pessoa = CASE WHEN cpf IS NULL AND cgc IS NOT NULL THEN 2                                                                  " & vbNewLine
        SQL = SQL & "                            WHEN cgc IS NULL AND cpf IS NOT NULL THEN 1                                                                  " & vbNewLine
        SQL = SQL & "                            ELSE ''                                                                                                      " & vbNewLine
        SQL = SQL & "                       END,                                                                                                              " & vbNewLine
        SQL = SQL & "           ISNULL(cgc, cpf) CPF_CGC,                                                                                                     " & vbNewLine
        SQL = SQL & "           proponente nome,                                                                                                              " & vbNewLine
        SQL = SQL & "           dt_nascimento dt_nasc,                                                                                                        " & vbNewLine
        SQL = SQL & "           NULL func,                                                                                                                    " & vbNewLine
        SQL = SQL & "           endereco ,                                                                                                                    " & vbNewLine
        SQL = SQL & "           bairro,                                                                                                                       " & vbNewLine
        SQL = SQL & "           municipio,                                                                                                                    " & vbNewLine
        SQL = SQL & "           estado,                                                                                                                       " & vbNewLine
        SQL = SQL & "           cep,                                                                                                                          " & vbNewLine
        SQL = SQL & "           ddd ddd_1,                                                                                                                    " & vbNewLine
        SQL = SQL & "           telefone telefone_1,                                                                                                          " & vbNewLine
        SQL = SQL & "           produto_tb.produto_id,                                                                                                        " & vbNewLine
        SQL = SQL & "           produto_tb.nome nome_produto,                                                                                                 " & vbNewLine
        SQL = SQL & "           ramo_tb.ramo_id,                                                                                                              " & vbNewLine
        SQL = SQL & "           ramo_tb.nome nome_ramo                                                                                                        " & vbNewLine
        SQL = SQL & "      FROM seguros_hist_db.dbo.emi_proposta_hist_tb emi_proposta_hist_tb WITH (NOLOCK)                                                                                                        " & vbNewLine
        SQL = SQL & "INNER JOIN arquivo_produto_tb WITH (NOLOCK)                                                                                                            " & vbNewLine
        SQL = SQL & "        ON arquivo_produto_tb.produto_externo_id = emi_proposta_hist_tb.produto_externo                                                       " & vbNewLine
        SQL = SQL & "       AND arquivo_produto_tb.arquivo = SUBSTRING(emi_proposta_hist_tb.arquivo,1, CHARINDEX('.' ,emi_proposta_hist_tb.arquivo) - 1)                " & vbNewLine
        SQL = SQL & "INNER JOIN produto_tb WITH (NOLOCK)                                                                                                                     " & vbNewLine
        SQL = SQL & "        ON produto_tb.produto_id = arquivo_produto_tb.produto_id                                                                         " & vbNewLine
        SQL = SQL & "INNER JOIN ramo_tb WITH (NOLOCK)                                                                                                                        " & vbNewLine
        SQL = SQL & "        ON ramo_tb.ramo_id = arquivo_produto_tb.ramo_id                                                                                  " & vbNewLine
        SQL = SQL & "     WHERE emi_proposta_hist_tb.proposta_bb = " & sPropostaBB & vbNewLine
        SQL = SQL & "       AND emi_proposta_hist_tb.situacao = 'N'                                                                                                " & vbNewLine


        'FIM GENJUNIOR

        '        SQL = SQL & "                                                                                                                                         " & vbNewLine
        '        SQL = SQL & "UNION ALL                                                                                                                                " & vbNewLine
        '        SQL = SQL & "                                                                                                                                         " & vbNewLine
        '        SQL = SQL & "    SELECT proposta_bb,                                                                                                                  " & vbNewLine
        '        SQL = SQL & "           proposta_bb_anterior,                                                                                                         " & vbNewLine
        '        SQL = SQL & "           ind_houve_sinistro,                                                                                                           " & vbNewLine
        '        SQL = SQL & "           seguro_anterior,                                                                                                              " & vbNewLine
        '        SQL = SQL & "           NULL tp_contrato,                                                                                                             " & vbNewLine
        '        SQL = SQL & "           agencia_contratante cont_agencia_id,                                                                                          " & vbNewLine
        '        SQL = SQL & "           NULL  banco_id,                                                                                                               " & vbNewLine
        '        SQL = SQL & "           agencia_cliente agencia_id,                                                                                                   " & vbNewLine
        '        SQL = SQL & "           conta_corrente deb_conta_corrente_id,                                                                                         " & vbNewLine
        '        SQL = SQL & "           NULL val_comissao,                                                                                                            " & vbNewLine
        '        SQL = SQL & "           clausula_benef,                                                                                                               " & vbNewLine
        '        SQL = SQL & "           outra_seguradora,                                                                                                             " & vbNewLine
        '        SQL = SQL & "           NULL val_iof_ato,                                                                                                             " & vbNewLine
        '        SQL = SQL & "           NULL custo_apolice,                                                                                                           " & vbNewLine
        '        SQL = SQL & "           NULL val_premio_tarifa,                                                                                                       " & vbNewLine
        '        SQL = SQL & "           NULL desconto_progres,                                                                                                        " & vbNewLine
        '        SQL = SQL & "           NULL desconto_renov,                                                                                                          " & vbNewLine
        '        SQL = SQL & "           NULL desconto_outro,                                                                                                          " & vbNewLine
        '        SQL = SQL & "           NULL desconto_sinistralidade,                                                                                                 " & vbNewLine
        '        SQL = SQL & "           NULL coef_ajuste,                                                                                                             " & vbNewLine
        '        SQL = SQL & "           NULL val_premio_bruto,                                                                                                        " & vbNewLine
        '        SQL = SQL & "           num_parcelas,                                                                                                                 " & vbNewLine
        '        SQL = SQL & "           NULL val_juros,                                                                                                               " & vbNewLine
        '        SQL = SQL & "           NULL val_pgto_ato,                                                                                                            " & vbNewLine
        '        SQL = SQL & "           NULL val_parcela,                                                                                                             " & vbNewLine
        '        SQL = SQL & "           tp_pgto forma_pgto_id,                                                                                                        " & vbNewLine
        '        SQL = SQL & "           dia_debito data_cobranca_dia,                                                                                                 " & vbNewLine
        '        SQL = SQL & "           NULL val_tot_desconto_comercial,                                                                                              " & vbNewLine
        '        SQL = SQL & "           NULL taxa_juros,                                                                                                              " & vbNewLine
        '        SQL = SQL & "           tp_pessoa = CASE WHEN cpf IS NULL AND cgc IS NOT NULL THEN 2                                                                  " & vbNewLine
        '        SQL = SQL & "                            WHEN cgc IS NULL AND cpf IS NOT NULL THEN 1                                                                  " & vbNewLine
        '        SQL = SQL & "                            ELSE ''                                                                                                      " & vbNewLine
        '        SQL = SQL & "                       END,                                                                                                              " & vbNewLine
        '        SQL = SQL & "           isnull(cgc, cpf) CPF_CGC,                                                                                                     " & vbNewLine
        '        SQL = SQL & "           proponente nome,                                                                                                              " & vbNewLine
        '        SQL = SQL & "           dt_nascimento dt_nasc,                                                                                                        " & vbNewLine
        '        SQL = SQL & "           NULL func,                                                                                                                    " & vbNewLine
        '        SQL = SQL & "           endereco ,                                                                                                                    " & vbNewLine
        '        SQL = SQL & "           bairro,                                                                                                                       " & vbNewLine
        '        SQL = SQL & "           municipio,                                                                                                                    " & vbNewLine
        '        SQL = SQL & "           estado,                                                                                                                       " & vbNewLine
        '        SQL = SQL & "           cep,                                                                                                                          " & vbNewLine
        '        SQL = SQL & "           ddd ddd_1,                                                                                                                    " & vbNewLine
        '        SQL = SQL & "           telefone telefone_1,                                                                                                          " & vbNewLine
        '        SQL = SQL & "           produto_tb.produto_id,                                                                                                        " & vbNewLine
        '        SQL = SQL & "           produto_tb.nome nome_produto,                                                                                                 " & vbNewLine
        '        SQL = SQL & "           ramo_tb.ramo_id,                                                                                                              " & vbNewLine
        '        SQL = SQL & "           ramo_tb.nome nome_ramo                                                                                                        " & vbNewLine
        '        SQL = SQL & "      FROM emi_re_proposta_tb WITH (NOLOCK)                                                                                                     " & vbNewLine
        '        SQL = SQL & "INNER JOIN arquivo_produto_tb WITH (NOLOCK)                                                                                                             " & vbNewLine
        '        SQL = SQL & "        ON arquivo_produto_tb.produto_externo_id = emi_re_proposta_tb.produto_externo                                                    " & vbNewLine
        '        SQL = SQL & "       AND arquivo_produto_tb.arquivo = SUBSTRING(emi_re_proposta_tb.arquivo,1, CHARINDEX('.' ,emi_re_proposta_tb.arquivo) - 1)          " & vbNewLine
        '        SQL = SQL & "INNER JOIN produto_tb WITH (NOLOCK)                                                                                                                     " & vbNewLine
        '        SQL = SQL & "        ON produto_tb.produto_id = arquivo_produto_tb.produto_id                                                                         " & vbNewLine
        '        SQL = SQL & "INNER JOIN ramo_tb WITH (NOLOCK)                                                                                                                        " & vbNewLine
        '        SQL = SQL & "        ON ramo_tb.ramo_id = arquivo_produto_tb.ramo_id                                                                                  " & vbNewLine
        '        SQL = SQL & "     WHERE emi_re_proposta_tb.proposta_bb = " & sPropostaBB & vbNewLine
        '        SQL = SQL & "       AND emi_re_proposta_tb.situacao = 'N'                                                                                             " & vbNewLine
        '        SQL = SQL & "       AND(emi_re_proposta_tb.arquivo LIKE 'SEG491D%'                                                                                    " & vbNewLine
        '        SQL = SQL & "           OR emi_re_proposta_tb.arquivo LIKE 'SEG493%'                                                                                  " & vbNewLine
        '        SQL = SQL & "           OR emi_re_proposta_tb.arquivo LIKE 'SEGF496%'                                                                                 " & vbNewLine
        '        SQL = SQL & "           OR emi_re_proposta_tb.arquivo LIKE 'INTERNET%'                                                                                " & vbNewLine
        '        SQL = SQL & "           OR emi_re_proposta_tb.arquivo LIKE 'SEG486%'                                                                                  " & vbNewLine
        '        SQL = SQL & "           OR emi_re_proposta_tb.arquivo LIKE 'SEG422%'                                                                                  " & vbNewLine
        '        SQL = SQL & "        OR emi_re_proposta_tb.arquivo LIKE 'SEGF495%')                                                                                   " & vbNewLine
        '        SQL = SQL & "ORDER BY proposta_bb                                                                                                                     " & vbNewLine


    End If

    Set rc = rdocn.OpenResultset(SQL)
    ' Fim   'alterado 12/09/2013 projeto 17860335 - Melhorias no SEGBR MC_Amaral -confitec
    If Not IsNull(rc("tp_contrato")) Then
        If rc("tp_contrato") = 1 Then
            txtTpContrato = "1. Contrata��o"
        ElseIf rc("tp_contrato") = 2 Then
            txtTpContrato = "2. Renova��o"
        End If
    Else
        txtTpContrato = "1. Contrata��o"
    End If

    If Not IsNull(rc("seguro_anterior")) Then
        If (rc("seguro_anterior")) = "1" Then
            txtSeguroAnterior = "1. N�o"
        ElseIf (rc("seguro_anterior")) = "2" Then
            txtSeguroAnterior = "2. Sim"
        ElseIf (rc("seguro_anterior")) = "3" Then
            txtSeguroAnterior = "3. Sim/BB Corretora"
        ElseIf (rc("seguro_anterior")) = "9" Then
            txtSeguroAnterior = "9. N�o Informado"
        End If
    Else
        txtSeguroAnterior = "9. N�o Informado"
    End If

    TxtHouveSinistro = ""

    If Not IsNull(rc("proposta_bb")) Then
        txtPropostaBB.Text = Format(rc("proposta_bb"), "000000000")
    End If

    If Not IsNull(rc("proposta_bb_anterior")) Then
        If Val(rc("proposta_bb_anterior")) > 0 Then
            txtPropAnterior.Text = Format(rc("proposta_bb_anterior"), "000000000")
        End If
    End If

    '' Ag�ncia Contratante
    If Not IsNull(rc("cont_agencia_id")) Then
        txtCodAgencia.Text = Format(rc("cont_agencia_id"), "0000")
        SQL = ""
        SQL = "SELECT nome FROM agencia_tb  WITH (NOLOCK)  "
        SQL = SQL & "WHERE agencia_id = " & rc("cont_agencia_id")
        SQL = SQL & " AND banco_id = 1"    'Ag. Contratante ser� sempre do BB

        Set rc1 = rdoCn1.OpenResultset(SQL)
        If Not rc1.EOF Then
            If IsNull(rc1(0)) = False Then
                txtNomeAgencia.Text = rc1(0)
            End If
        End If
        rc1.Close
        Set rc1 = Nothing
    End If

    If Not IsNull(rc("agencia_id")) Then
        txtCodAgDebito.Text = Format(rc("agencia_id"), "0000")
        SQL = ""
        SQL = "SELECT nome FROM agencia_tb    WITH (NOLOCK)  "
        SQL = SQL & "WHERE agencia_id = " & rc("agencia_id")
        SQL = SQL & " AND banco_id = " & Val(txtBanco.Text)

        Set rc1 = rdoCn1.OpenResultset(SQL)
        If Not rc1.EOF Then
            If Not IsNull(rc1(0)) Then
                txtNomeAgDebito.Text = Trim(rc1(0))
            End If
        End If
        rc1.Close
        Set rc1 = Nothing
    End If

    If Not IsNull(rc("deb_conta_corrente_id")) Then
        If Val(rc("deb_conta_corrente_id")) > 0 Then
            txtContaCorrente.Text = Format(rc("deb_conta_corrente_id"), "00000000000")
        End If
    End If

    If Not IsNull(rc("outra_seguradora")) Then
        If Trim(rc("outra_seguradora")) <> "" Then
            txtSeg2.Text = rc("outra_seguradora")
        End If
    End If

    txtIOF.Text = ""
    txtCustoApolice.Text = ""
    txtTotalPremioLiq.Text = ""
    vAuxComissao = ""
    txtDescProg.Text = ""
    txtBonus.Text = ""
    txtLocal.Text = ""
    txtOutro.Text = ""
    txtCoefAjuste.Text = ""
    txtPremioBruto.Text = ""
    txtTotalDescontos.Text = ""

    If Not IsNull(rc("num_parcelas")) Then
        txtNumParcelas.Text = rc("num_parcelas")
    Else
        txtNumParcelas.Text = 0
    End If

    txtValJuros.Text = ""
    TxtTaxaJuros.Text = ""
    txtPagoAto.Text = ""
    txtDemaisParcelas.Text = ""
    txtValTotalParc.Text = ""

    If Not IsNull(rc("forma_pgto_id")) Then
        txtCodFormaPg.Text = rc("forma_pgto_id")
        SQL = ""
        SQL = SQL & "SELECT nome FROM forma_pgto_tb  WITH (NOLOCK)    " & vbNewLine
        SQL = SQL & "WHERE forma_pgto_id = " & rc("forma_pgto_id")
        Set rc1 = rdoCn1.OpenResultset(SQL)
        If Not rc1.EOF Then
            If Not IsNull(rc1(0)) Then
                txtFormaPg.Text = Trim(rc1(0))
            End If
        End If
        rc1.Close
        Set rc1 = Nothing
    End If

    If Not IsNull(rc("data_cobranca_dia")) Then
        txtDiaDebito.Text = rc("data_cobranca_dia")
    End If

    'dados cliente
    If rc.EOF Then
        MsgBox "Cliente da proposta n�o encontrado!", vbCritical
    Else
        If Not IsNull(rc("Nome")) Then
            txtNome.Text = Trim(rc("Nome"))
        End If
        '
        If Not IsNull(rc("CPF_CGC")) Then
            IdTpPessoa_endosso = rc("tp_pessoa")
            If rc("tp_pessoa") = 1 Then    ' PF
                Label1(29).Caption = "CPF :"
                CPF_CNPJ_saida = "CPF          :"
                txtCPF_CGC.Text = Format$(rc("CPF_CGC"), "&&&.&&&.&&&-&&")
            Else
                Label1(29).Caption = "CNPJ :"
                CPF_CNPJ_saida = "CNPJ         :"
                txtCPF_CGC.Text = Format(rc("CPF_CGC"), "&&.&&&.&&&/&&&&-&&")
            End If
        End If
        '
        txtTpPessoaId.Text = rc("tp_pessoa")
        txtTpPessoa.Text = IIf(rc!tp_pessoa = 1, "PESSOA F�SICA", "PESSOA JUR�DICA")
        '
        If rc!tp_pessoa = 1 Then    ' PF
            If Not IsNull(rc("dt_nasc")) Then
                txtNascimento.Text = Format(rc("dt_nasc"), "dd/mm/yyyy")
            End If
        Else    'PJ
            Label1(28).Visible = False
            txtNascimento.Visible = False
        End If
        '
        If Not IsNull(rc("Endereco")) Then
            txtEndereco.Text = Trim(rc("Endereco"))
        End If
        '
        If Not IsNull(rc("bairro")) Then
            TxtBairro.Text = Trim(rc("bairro"))
        End If
        '
        If Not IsNull(rc("municipio")) Then
            txtCidade.Text = Trim(rc("municipio"))
        End If
        '
        If Not IsNull(rc("Estado")) Then
            txtUF.Text = Trim(rc("Estado"))
        End If
        '
        If Not IsNull(rc("Cep")) Then
            If Trim(rc!CEP) <> "" Then
                txtCEP.Text = Format(rc("Cep"), "&&.&&&-&&&")
            End If
        End If
        '
        If Not IsNull(rc("ddd_1")) Then
            txtDDD.Text = rc!ddd_1
        End If
        '
        If Not IsNull(rc("telefone_1")) Then
            txtTelefone.Text = rc("telefone_1")
        End If
        '
        'produto
        If Not IsNull(rc("produto_id")) Then
            txtProduto.Text = Trim(rc("produto_id") & " - " & rc("nome_produto"))
        End If
        'ramo
        If Not IsNull(rc("ramo_id")) Then
            txtRamo.Text = Format(rc("ramo_id"), "000") & " - " & rc("nome_ramo")
        End If

    End If

    rc.Close
    Set rc = Nothing

    Exit Sub

Erro:
    TrataErroGeral "Ler Proposta N�o Emitida"
    MsgBox "Rotina: Ler Proposta N�o Emitida"

End Sub



Private Sub Ler_Proposta_Fechada()
    Dim rc As rdoResultset, rc1 As rdoResultset
    Dim rc_aux As rdoResultset
    Dim SQL As String
    Dim TotParcelado As Double
    Dim ValParcela As Double
    Dim Sql_Aux As String
    Dim vAuxComissao As Variant

    On Error GoTo Erro
    'Demanda MU-2018-002920 - Vig�ncia e endossos 1206 - BB Seguro Vida Empresa FLEX
    'Verifica se � do produto 1206 para ajuste na exibi��o dos dados financeiros para endosso ou contrata��o
    If vProdutoId <> 1206 Then
        SQL = "SELECT proposta_bb, proposta_bb_anterior, " & _
              "houve_sinistro, seguro_anterior, tp_contrato, " & _
              "cont_agencia_id, banco_id, agencia_id, " & _
              "deb_conta_corrente_id, val_comissao, " & _
              "clausula_benef, outra_seguradora, " & _
              "val_iof, custo_apolice, val_premio_tarifa, " & _
              "desconto_progres, desconto_renov, desconto_outro, " & _
              "desconto_sinistralidade, coef_ajuste, val_premio_bruto, " & _
              "num_parcelas, val_juros, val_pgto_ato, val_parcela, " & _
              "forma_pgto_id, data_cobranca_dia, val_tot_desconto_comercial, taxa_juros "
        SQL = SQL & "FROM seguros_db.dbo.proposta_fechada_tb    WITH (NOLOCK)  "
        SQL = SQL & "WHERE proposta_id = " & vPropostaId
    Else
        If indEndosso Then
            SQL = "SELECT  p.proposta_bb" & vbNewLine
            SQL = SQL & ", p.proposta_bb_anterior" & vbNewLine
            SQL = SQL & ", p.houve_sinistro" & vbNewLine
            SQL = SQL & ", p.seguro_anterior" & vbNewLine
            SQL = SQL & ", p.tp_contrato" & vbNewLine
            SQL = SQL & ", p.cont_agencia_id" & vbNewLine
            SQL = SQL & ", p.banco_id" & vbNewLine
            SQL = SQL & ", d.NR_AG_DEB as agencia_id" & vbNewLine
            SQL = SQL & ", d.NR_CC_DEB as deb_conta_corrente_id" & vbNewLine
            SQL = SQL & ", p.val_comissao" & vbNewLine
            SQL = SQL & ", p.clausula_benef" & vbNewLine
            SQL = SQL & ", p.outra_seguradora" & vbNewLine
            SQL = SQL & ", CAST(d.VL_IOF_CTR as numeric(9,2)) as val_iof" & vbNewLine
            SQL = SQL & ", CAST(d.VL_CST_APLC_CTR as numeric(5,2)) as custo_apolice" & vbNewLine
            SQL = SQL & ", CAST(d.VL_LQDO_INC_MOEN as numeric(15,2)) as val_premio_tarifa" & vbNewLine
            SQL = SQL & ", p.desconto_progres" & vbNewLine
            SQL = SQL & ", p.desconto_renov" & vbNewLine
            SQL = SQL & ", p.desconto_outro" & vbNewLine
            SQL = SQL & ", p.desconto_sinistralidade" & vbNewLine
            SQL = SQL & ", p.coef_ajuste" & vbNewLine
            SQL = SQL & ", CAST(d.VL_PREM_MOEN_CTR as numeric(15,2)) as val_premio_bruto" & vbNewLine
            SQL = SQL & ", d.QT_PCL_PGTO_CTR as num_parcelas" & vbNewLine
            SQL = SQL & ", p.val_juros" & vbNewLine
            SQL = SQL & ", CAST(d.VL_PRMO_MOEN_CTR as numeric(15,2)) as val_pgto_ato" & vbNewLine
            SQL = SQL & ", CAST(d.VL_PCL_MOEN_CTR as numeric(15,2)) as val_parcela" & vbNewLine
            SQL = SQL & ", d.CD_FMA_PGTO as forma_pgto_id" & vbNewLine
            SQL = SQL & ", d.DD_DEB_CBR_PREM as data_cobranca_dia" & vbNewLine
            SQL = SQL & ", CAST(d.VL_DSC_CML_CTR as numeric(15,2)) as val_tot_desconto_comercial" & vbNewLine
            SQL = SQL & ", p.taxa_juros " & vbNewLine
            SQL = SQL & "FROM seguros_db.dbo.proposta_fechada_tb p WITH (NOLOCK)  " & vbNewLine
            SQL = SQL & "INNER JOIN seguros_db.dbo.endosso_tb a with(NOLOCK)" & vbNewLine
            SQL = SQL & "ON a.proposta_id = p.proposta_id" & vbNewLine
            SQL = SQL & "INNER JOIN seguros_db.dbo.proposta_processo_susep_tb b with(NOLOCK)" & vbNewLine
            SQL = SQL & "ON b.proposta_id = a.proposta_id" & vbNewLine
            'IM00716805 - LEANDRO AMARAL - CONFITEC - 20/03/2019
            'SQL = SQL & "INNER JOIN als_operacao_db.dbo.EDS_CTR_SGRO c WITH (NOLOCK)" & vbNewLine
            SQL = SQL & "LEFT JOIN als_operacao_db.dbo.EDS_CTR_SGRO c WITH (NOLOCK)" & vbNewLine
            'IM00716805 - LEANDRO AMARAL - CONFITEC - 20/03/2019
            SQL = SQL & "ON b.cod_produto = c.CD_PRD" & vbNewLine
            SQL = SQL & "AND b.cod_modalidade = c.CD_MDLD" & vbNewLine
            SQL = SQL & "AND b.cod_item_modalidade = c.CD_ITEM_MDLD" & vbNewLine
            SQL = SQL & "AND b.num_contrato_seguro = c.NR_CTR_SGRO" & vbNewLine
            SQL = SQL & "AND a.num_endosso_bb = c.NR_SLCT_EDS" & vbNewLine
            'IM00716805 - LEANDRO AMARAL - CONFITEC - 20/03/2019
            'SQL = SQL & "INNER JOIN als_operacao_db.dbo.DADO_FNCO_CTR d WITH (NOLOCK)" & vbNewLine
            SQL = SQL & "LEFT JOIN als_operacao_db.dbo.DADO_FNCO_CTR d WITH (NOLOCK)" & vbNewLine
            'IM00716805 - LEANDRO AMARAL - CONFITEC - 20/03/2019
            SQL = SQL & "ON c.CD_PRD = d.CD_PRD" & vbNewLine
            SQL = SQL & "AND c.CD_MDLD = d.CD_MDLD" & vbNewLine
            SQL = SQL & "AND c.CD_ITEM_MDLD = d.CD_ITEM_MDLD" & vbNewLine
            SQL = SQL & "AND c.NR_CTR_SGRO = d.NR_CTR_SGRO" & vbNewLine
            SQL = SQL & "AND c.NR_VRS_EDS = d.NR_VRS_EDS" & vbNewLine
            SQL = SQL & "WHERE a.proposta_id = " & vPropostaId & vbNewLine
            SQL = SQL & "AND a.endosso_id = " & txtNumEndosso.Text & vbNewLine
        Else
            SQL = "SELECT proposta_bb, proposta_bb_anterior, " & _
                  "houve_sinistro, seguro_anterior, tp_contrato, " & _
                  "cont_agencia_id, banco_id, agencia_id, " & _
                  "deb_conta_corrente_id, val_comissao, " & _
                  "clausula_benef, outra_seguradora, " & _
                  "val_iof, custo_apolice, val_premio_tarifa, " & _
                  "desconto_progres, desconto_renov, desconto_outro, " & _
                  "desconto_sinistralidade, coef_ajuste, val_premio_bruto, " & _
                  "num_parcelas, val_juros, val_pgto_ato, val_parcela, " & _
                  "forma_pgto_id, data_cobranca_dia, val_tot_desconto_comercial, taxa_juros "
            SQL = SQL & "FROM seguros_db.dbo.proposta_fechada_tb    WITH (NOLOCK)  "
            SQL = SQL & "WHERE proposta_id = " & vPropostaId
        End If

    End If


    '*****************************************************************************
    '** Descri��o: Implementa��o de consulta por proposta ades�o                **
    '** Motivo: Atender consultas para o produto 714                            **
    '** Autor: Febner (Fabio Alves de Araujo Ebner - JR & P(SP)                 **
    '** Data: 10/06/03                                                          **
    '*****************************************************************************

    Sql_Aux = SQL
    Set rc_aux = rdocn.OpenResultset(Sql_Aux)
    If rc_aux.EOF Then
        Prop_Adesao = True
        SQL = ""
        SQL = SQL & " SELECT  proposta_bb,cont_agencia_id,"
        SQL = SQL & "   seguro_anterior,"
        SQL = SQL & "   deb_conta_corrente_id,  val_comissao"
        SQL = SQL & "   clausula_benef,"
        SQL = SQL & "   val_iof,"
        SQL = SQL & "   val_premio_tarifa,  desconto_progres,"
        SQL = SQL & "   desconto_renov,"
        SQL = SQL & "   coef_ajuste,"
        SQL = SQL & "   val_premio_bruto,   "
        SQL = SQL & "   val_juros,      val_pgto_ato,"
        SQL = SQL & "   val_parcela,        forma_pgto_id ,"
        SQL = SQL & "   data_cobranca_dia,  val_tot_desconto_comercial,"
        SQL = SQL & "   taxa_juros "
        SQL = SQL & " FROM  proposta_adesao_tb  WITH (NOLOCK)    "
        SQL = SQL & " WHERE     proposta_id = " & vPropostaId
    End If
    rc_aux.Close
    '**********************************************************************************

    Set rc = rdocn.OpenResultset(SQL)

    If Not rc.EOF Then
        If Prop_Adesao Then    'FEBNER  - > Verifica se a proposta e do tipo ades�o
            txtTpContrato = "1. Contrata��o"
        Else
            If Not IsNull(rc!tp_contrato) Then
                If rc!tp_contrato = 1 Then
                    txtTpContrato = "1. Contrata��o"
                ElseIf rc!tp_contrato = 2 Then
                    txtTpContrato = "2. Renova��o"
                End If
            Else
                txtTpContrato = "1. Contrata��o"
            End If
        End If
        '
        If Not IsNull(rc!seguro_anterior) Then
            If rc!seguro_anterior = "1" Then
                txtSeguroAnterior = "1. N�o"
            ElseIf rc!seguro_anterior = "2" Then
                txtSeguroAnterior = "2. Sim"
            ElseIf rc!seguro_anterior = "3" Then
                txtSeguroAnterior = "3. Sim/BB Corretora"
            ElseIf rc!seguro_anterior = "9" Then
                txtSeguroAnterior = "9. N�o Informado"
            End If
        Else
            txtSeguroAnterior = "9. N�o Informado"
        End If
        '
        'FEBNER  - > Verifica se a proposta e do tipo ades�o
        If Prop_Adesao Then
            TxtHouveSinistro = "N�o Informado"
        Else
            If Not IsNull(rc!houve_sinistro) Then
                If rc!houve_sinistro = "N" Then
                    TxtHouveSinistro = "N�o"
                ElseIf rc!houve_sinistro = "S" Then
                    TxtHouveSinistro = "Sim"
                Else
                    TxtHouveSinistro = "N�o"
                End If
            Else
                TxtHouveSinistro = "N�o Informado"
            End If
        End If
        '
        If Not IsNull(rc!proposta_bb) Then
            txtPropostaBB.Text = Format(rc!proposta_bb, "000000000")
        End If
        'FEBNER  - > Verifica se a proposta e do tipo ades�o
        '
        If Not Prop_Adesao Then
            If Not IsNull(rc!proposta_bb_anterior) Then
                If Val(rc!proposta_bb_anterior) > 0 Then
                    txtPropAnterior.Text = Format(rc!proposta_bb_anterior, "000000000")
                End If
            End If
        End If
        '' Ag�ncia Contratante
        If Not IsNull(rc!cont_agencia_id) Then
            txtCodAgencia.Text = Format(rc!cont_agencia_id, "0000")
            '
            SQL = "SELECT nome FROM agencia_tb     WITH (NOLOCK)  "
            SQL = SQL & "WHERE agencia_id = " & rc!cont_agencia_id
            SQL = SQL & " AND banco_id = 1"    'Ag. Contratante ser� sempre do BB
            '
            Set rc1 = rdoCn1.OpenResultset(SQL)
            If Not rc1.EOF Then
                If IsNull(rc1(0)) = False Then
                    txtNomeAgencia.Text = rc1(0)
                End If
            End If
            rc1.Close
            Set rc1 = Nothing
        End If
        '' Conta para d�bito
        'FEBNER  - > Verifica se a proposta e do tipo ades�o
        If Not Prop_Adesao Then
            If Not IsNull(rc!banco_id) Then
                txtBanco.Text = Format(rc!banco_id, "000")
            End If
        End If
        '
        'FEBNER  - > Verifica se a proposta e do tipo ades�o
        If Not Prop_Adesao Then
            If Not IsNull(rc!agencia_id) Then
                txtCodAgDebito.Text = Format(rc!agencia_id, "0000")
                SQL = "SELECT nome FROM agencia_tb    WITH (NOLOCK)  "
                SQL = SQL & "WHERE agencia_id = " & rc!agencia_id
                SQL = SQL & " AND banco_id = " & Val(txtBanco.Text)
                '
                Set rc1 = rdoCn1.OpenResultset(SQL)
                If Not rc1.EOF Then
                    If Not IsNull(rc1(0)) Then
                        txtNomeAgDebito.Text = Trim(rc1(0))
                    End If
                End If
                rc1.Close
                Set rc1 = Nothing
            End If
        End If
        '
        If Not IsNull(rc!deb_conta_corrente_id) Then
            If Val(rc!deb_conta_corrente_id) > 0 Then
                txtContaCorrente.Text = Format(rc!deb_conta_corrente_id, "00000000000")
            End If
        End If

        '' Seguro em outra cia.

        'FEBNER  - > Verifica se a proposta e do tipo ades�o
        If Not Prop_Adesao Then
            If Not IsNull(rc!outra_seguradora) Then
                If Trim(rc!outra_seguradora) <> "" Then
                    txtSeg2.Text = rc!outra_seguradora
                End If
            End If
        End If
        '' Valores de Premio
        ' IOF
        If Not IsNull(rc!val_iof) Then
            If ConfiguracaoBrasil Then
                txtIOF.Text = Format(Val(rc!val_iof), "###,###,###,##0.00")
            Else
                txtIOF.Text = TrocaValorAmePorBras(Format(Val(rc!val_iof), "###,###,###,##0.00"))
            End If
        Else
            txtIOF.Text = vValZero
        End If
        ' Custo
        'FEBNER  - > Verifica se a proposta e do tipo ades�o
        If Prop_Adesao Then
            txtCustoApolice.Text = vValZero
        Else
            If Not IsNull(rc!Custo_Apolice) Then
                If ConfiguracaoBrasil Then
                    txtCustoApolice.Text = Format(Val(rc!Custo_Apolice), "###,###,###,##0.00")
                Else
                    txtCustoApolice.Text = TrocaValorAmePorBras(Format(Val(rc!Custo_Apolice), "###,###,###,##0.00"))
                End If
            Else
                txtCustoApolice.Text = vValZero
            End If
        End If
        ' Pr�mio Tarifa
        If Not IsNull(rc!val_premio_tarifa) Then
            If ConfiguracaoBrasil Then
                txtTotalPremioLiq.Text = Format(Val(rc!val_premio_tarifa), "###,###,###,##0.00")
            Else
                txtTotalPremioLiq.Text = TrocaValorAmePorBras(Format(Val(rc!val_premio_tarifa), "###,###,###,##0.00"))
            End If
        Else
            txtTotalPremioLiq.Text = vValZero
        End If
        'Percentual de Comiss�o
        '' N�o faz nada com esse valor. Os percentuais de comiss�o
        '' est�o nas tabelas de corretagem
        'FEBNER  - > Verifica se a proposta e do tipo ades�o
        If Prop_Adesao Then
            vAuxComissao = vValZero
        Else
            If Not IsNull(rc!val_comissao) Then
                If ConfiguracaoBrasil Then
                    vAuxComissao = Format(Val(rc!val_comissao), "###,###,###,##0.00")
                Else
                    vAuxComissao = TrocaValorAmePorBras(Format(Val(rc!val_comissao), "###,###,###,##0.00"))
                End If
            Else
                vAuxComissao = vValZero
            End If
        End If
        'txtPercComissao.Text = Format(vAuxComissao * 100 / Val(txtTotalPremioLiq.Text), "##0.00") & "%"

        'Desconto Progressivo
        If Not IsNull(rc!desconto_progres) Then
            If ConfiguracaoBrasil Then
                txtDescProg.Text = Format(Val(rc!desconto_progres), "#0.00")
            Else
                txtDescProg.Text = TrocaValorAmePorBras(Format(Val(rc!desconto_progres), "#0.00"))
            End If
        Else
            txtDescProg.Text = vValZero
        End If
        'Desconto Renova��o
        If Not IsNull(rc!desconto_renov) Then
            If ConfiguracaoBrasil Then
                txtBonus.Text = Format(Val(rc!desconto_renov), "#0.00")
            Else
                txtBonus.Text = TrocaValorAmePorBras(Format(Val(rc!desconto_renov), "#0.00"))
            End If
        Else
            txtBonus.Text = vValZero
        End If
        'Desconto Sinistralidade
        If Prop_Adesao Then
            txtLocal.Text = vValZero
        Else
            If Not IsNull(rc!desconto_sinistralidade) Then
                If ConfiguracaoBrasil Then
                    txtLocal.Text = Format(Val(rc!desconto_sinistralidade), "#0.00")
                Else
                    txtLocal.Text = TrocaValorAmePorBras(Format(Val(rc!desconto_sinistralidade), "#0.00"))
                End If
            Else
                txtLocal.Text = vValZero
            End If
        End If
        'Desconto Outros
        'FEBNER  - > Verifica se a proposta e do tipo ades�o
        If Prop_Adesao Then
            txtOutro.Text = vValZero
        Else
            If Not IsNull(rc!desconto_outro) Then
                If ConfiguracaoBrasil Then
                    txtOutro.Text = Format(Val(rc!desconto_outro), "#0.00")
                Else
                    txtOutro.Text = TrocaValorAmePorBras(Format(Val(rc!desconto_outro), "#0.00"))
                End If
            Else
                txtOutro.Text = vValZero
            End If
        End If
        'Coef. Fracionamento
        If Not IsNull(rc!coef_ajuste) Then
            If ConfiguracaoBrasil Then
                txtCoefAjuste.Text = Format(Val(rc!coef_ajuste), "#0.00000")
            Else
                txtCoefAjuste.Text = TrocaValorAmePorBras(Format(Val(rc!coef_ajuste), "#0.00000"))
            End If
        Else
            txtCoefAjuste.Text = Format(vValZero, "#0.00000")
        End If
        'Premio Bruto
        If Not IsNull(rc!val_premio_bruto) Then
            If ConfiguracaoBrasil Then
                txtPremioBruto.Text = Format(Val(rc!val_premio_bruto), "###,###,###,##0.00")
            Else
                txtPremioBruto.Text = TrocaValorAmePorBras(Format(Val(rc!val_premio_bruto), "###,###,###,##0.00"))
            End If
        Else
            txtPremioBruto.Text = vValZero
        End If
        'Desconto Comercial
        If Not IsNull(rc!val_tot_desconto_comercial) Then
            If ConfiguracaoBrasil Then
                txtTotalDescontos.Text = Format(Val(rc!val_tot_desconto_comercial), "###,###,###,##0.00")
            Else
                txtTotalDescontos.Text = TrocaValorAmePorBras(Format(Val(rc!val_tot_desconto_comercial), "###,###,###,##0.00"))
            End If
        Else
            txtTotalDescontos.Text = vValZero
        End If
        'No. de Parcelas
        If Prop_Adesao Then
            txtNumParcelas.Text = 0
        Else
            If Not IsNull(rc!num_parcelas) Then
                txtNumParcelas.Text = rc!num_parcelas
            Else
                '' Estava atribuindo zero, mas estava gerando erro de divis�o por zero
                'txtNumParcelas.Text = 1
                txtNumParcelas.Text = 0
            End If
        End If
        'Valor de Juros
        If Not IsNull(rc!val_juros) Then
            If ConfiguracaoBrasil Then
                txtValJuros.Text = Format(Val(rc!val_juros), "###,###,###,##0.00")
            Else
                txtValJuros.Text = TrocaValorAmePorBras(Format(Val(rc!val_juros), "###,###,###,##0.00"))
            End If
        Else
            txtValJuros.Text = vValZero
        End If
        'Taxa de Juros
        If Not IsNull(rc!taxa_juros) Then
            If ConfiguracaoBrasil Then
                TxtTaxaJuros.Text = Format(Val(rc!taxa_juros), "##0.00") & " %"
            Else
                TxtTaxaJuros.Text = TrocaValorAmePorBras(Format(Val(rc!taxa_juros), "##0.00")) & " %"
            End If
        Else
            TxtTaxaJuros.Text = vValZero & " %"
        End If
        'Valor Entrada
        If Not IsNull(rc!val_pgto_ato) Then
            If ConfiguracaoBrasil Then
                txtPagoAto.Text = Format(Val(rc!val_pgto_ato), "###,###,###,##0.00")
            Else
                txtPagoAto.Text = TrocaValorAmePorBras(Format(Val(rc!val_pgto_ato), "###,###,###,##0.00"))
            End If
        Else
            txtPagoAto.Text = vValZero
        End If
        'Valor da Parcela
        If Not IsNull(rc!val_parcela) Then
            If ConfiguracaoBrasil Then
                txtDemaisParcelas.Text = Format(Val(rc!val_parcela), "###,###,###,##0.00")
            Else
                txtDemaisParcelas.Text = TrocaValorAmePorBras(Format(Val(rc!val_parcela), "###,###,###,##0.00"))
            End If
        Else
            txtDemaisParcelas.Text = vValZero
        End If
        'Total das Parcelas
        '' Verificar esta conta, na emiss�o n�o leva em conta o valor
        '' pago no ato, mas aqui est� utilizando para compor o pr�mio
        '' Comparar com a emiss�o !!
        'If (Not IsNull(rc!num_parcelas)) And _
         (Not IsNull(rc!val_pgto_ato)) And _
         (Not IsNull(rc!val_parcela)) Then
        '    If Val(rc!val_pgto_ato) = 0 Then
        '         TotParcelado = (Val(rc!num_parcelas) * Val(rc!val_parcela)) + Val(txtCustoApolice.Text)
        '    Else
        '         TotParcelado = ((Val(rc!num_parcelas) - 1) * Val(rc!val_parcela)) + Val(rc!val_pgto_ato) + Val(txtCustoApolice.Text)
        '    End If
        '    '
        '    If ConfiguracaoBrasil Then
        '        txtValTotalParc.Text = Format(TotParcelado, "###,###,###,##0.00")
        '    Else
        '        txtValTotalParc.Text = TrocaValorAmePorBras(Format(TotParcelado, "###,###,###,##0.00"))
        '    End If
        'Else
        '    txtValTotalParc.Text = vValZero
        'End If
        '' Le o valor da parcela para uma temporaria
        If Not IsNull(rc!val_parcela) Then
            ValParcela = Val(rc!val_parcela)
        Else
            ValParcela = 0#
        End If
        '
        If Val(txtPagoAto.Text) = 0 Then
            TotParcelado = (txtNumParcelas.Text * ValParcela) + txtCustoApolice.Text
        Else
            TotParcelado = ((txtNumParcelas.Text - 1) * ValParcela) + txtPagoAto.Text + txtCustoApolice.Text
        End If
        '
        If ConfiguracaoBrasil Then
            txtValTotalParc.Text = Format(TotParcelado, "###,###,###,##0.00")
        Else
            txtValTotalParc.Text = TrocaValorAmePorBras(Format(TotParcelado, "###,###,###,##0.00"))
        End If

        'Forma de Pgto.
        If Not IsNull(rc!forma_pgto_id) Then
            txtCodFormaPg.Text = rc!forma_pgto_id
            '
            SQL = "SELECT nome FROM forma_pgto_tb  WITH (NOLOCK)    " & _
                  "WHERE forma_pgto_id = " & rc!forma_pgto_id
            Set rc1 = rdoCn1.OpenResultset(SQL)
            If Not rc1.EOF Then
                If Not IsNull(rc1(0)) Then
                    txtFormaPg.Text = Trim(rc1(0))
                End If
            End If
            rc1.Close
            Set rc1 = Nothing
        End If
        'Dia do D�bito
        If Not IsNull(rc!data_cobranca_dia) Then
            txtDiaDebito.Text = rc!data_cobranca_dia
        End If
    Else
        MsgBox "Dados da Proposta Fechada n�o encontrados.", vbInformation
    End If    ' not rc.eof

    rc.Close
    Set rc = Nothing

    'Jessica.Adao - Confitec Sistemas - 20181019 - Sala �gil - Sprint 10: Ajustes Amparo
    If vProdutoId = 1217 Then Call carregaFinanceiroAmparo
    Exit Sub

Erro:
    TrataErroGeral "Ler Proposta Fechada"
    MsgBox "Rotina: Ler Proposta Fechada"
    'End

End Sub

Private Sub Ler_Emi_Coberturas()

' Deve ser executada ap�s a Ler_Proposta (produto_id)
    Dim rc As rdoResultset, SQL As String, linha As String
    Dim rc1 As rdoResultset, Premio As Double, DESCONTO As Double, TAXA As Double
    Dim EMI_ID As Variant
    Dim vigengia As String

    On Error GoTo Erro

    SQL = "SELECT emi_re_proposta_id, cod_ramo_bb FROM emi_re_proposta_tb    WITH (NOLOCK)  "
    SQL = SQL & "WHERE (proposta_bb = " & txtPropostaBB.Text & ") "
    'GENJUNIOR - FLOW 17860335 - CONSULTA TABELAS DE HIST�RICO
    SQL = SQL & "  UNION " & vbNewLine
    SQL = SQL & " SELECT emi_re_proposta_id " & vbNewLine
    SQL = SQL & "      , cod_ramo_bb " & vbNewLine
    SQL = SQL & "   FROM seguros_hist_db.dbo.emi_re_proposta_hist_tb WITH (NOLOCK)  "
    SQL = SQL & "WHERE (proposta_bb = " & txtPropostaBB.Text & ") "
    'FIM GENJUNIOR

    Set rc1 = rdoCn1.OpenResultset(SQL)

    If Not rc1.EOF Then

        EMI_ID = rc1(0)
        RamoId = Left(Val(rc1(1)), 2)

        rc1.Close

        SQL = "SELECT desc_cobertura, val_risco_cobertura, "
        SQL = SQL & "taxa_cobertura, desconto, val_premio, franquia, "
        SQL = SQL & " val_franquia "
        SQL = SQL & "FROM emi_re_cobertura_tb   WITH (NOLOCK)    WHERE emi_re_proposta_id = " & EMI_ID
        'GENJUNIOR
        SQL = SQL & "  UNION " & vbNewLine
        SQL = SQL & " SELECT desc_cobertura " & vbNewLine
        SQL = SQL & "      , val_risco_cobertura " & vbNewLine
        SQL = SQL & "      , taxa_cobertura " & vbNewLine
        SQL = SQL & "      , desconto " & vbNewLine
        SQL = SQL & "      , val_premio " & vbNewLine
        SQL = SQL & "      , franquia " & vbNewLine
        SQL = SQL & "      , val_franquia " & vbNewLine
        SQL = SQL & "   FROM seguros_hist_db.dbo.emi_re_cobertura_hist_tb WITH (NOLOCK) " & vbNewLine
        SQL = SQL & "  WHERE emi_re_proposta_id = " & EMI_ID & vbNewLine
        'FIM GENJUNIOR
        Set rc = rdocn.OpenResultset(SQL)

        GridCoberturas.Rows = 1

        While Not rc.EOF
            linha = " " & vbTab
            linha = linha & rc!desc_cobertura & vbTab
            If ConfiguracaoBrasil Then
                linha = linha & Format(Val(rc!val_risco_cobertura), "###,###,###,##0.00") & vbTab
            Else
                linha = linha & TrocaValorAmePorBras(Format(Val(rc!val_risco_cobertura), "###,###,###,##0.00")) & vbTab
            End If
            TAXA = Val(rc!taxa_cobertura) / 100
            If Not IsNull(rc!DESCONTO) Then
                DESCONTO = Val(rc!DESCONTO)
                If Not DESCONTO = 0 Then
                    DESCONTO = (1 - DESCONTO) * 100
                End If
            Else
                DESCONTO = 0
            End If
            Premio = Val(rc!val_risco_cobertura) * TAXA
            If Not DESCONTO = 0 Then
                Premio = Premio * ((100 - DESCONTO) / 100)
            End If
            If ConfiguracaoBrasil Then
                linha = linha & Format(Premio, "###,###,###,##0.00") & vbTab
                linha = linha & TAXA & vbTab
                linha = linha & Format(DESCONTO, "##0.00") & vbTab
            Else
                linha = linha & TrocaValorAmePorBras(Format(Premio, "###,###,###,##0.00")) & vbTab
                linha = linha & TrocaValorAmePorBras(TAXA) & vbTab
                linha = linha & TrocaValorAmePorBras(Format(DESCONTO, "##0.00")) & vbTab
            End If
            linha = linha & vbTab & vbTab & " " & rc!franquia & vbTab
            GridCoberturas.AddItem linha
            rc.MoveNext
        Wend
        rc.Close
    Else
        rc1.Close
    End If

    vigengia = Mid(dt_contratacao, 7, 4) & Mid(dt_contratacao, 4, 2) & Mid(dt_contratacao, 1, 2)

    SQL = "SELECT valor FROM val_item_financeiro_ramo_tb    WITH (NOLOCK)  "
    SQL = SQL & "WHERE cod_item_financeiro = 'IOF'"
    SQL = SQL & "  and ramo_id = " & RamoId
    SQL = SQL & "  and dt_inicio_vigencia <= '" & dt_inicio_query & "'"
    SQL = SQL & "  and (dt_fim_vigencia >= '" & dt_inicio_query & "'"
    SQL = SQL & "  or dt_fim_vigencia is Null) "

    Set rc = rdocn.OpenResultset(SQL)

    If rc.EOF Then
        val_iof = 0
    Else
        val_iof = Val(rc("valor"))
    End If

    rc.Close

    val_iof_apolice = Format$((txtCustoApolice.Text * val_iof), "###,###,###,##0.00")

    Exit Sub

Erro:
    TrataErroGeral "Ler EMI_Coberturas"
    MsgBox "Rotina: Ler EMI_Coberturas"

End Sub

Private Sub Ler_Endosso_Proposta()

    Dim rc As rdoResultset, rc1 As rdoResultset
    Dim linha As String
    Dim dt_emissao_saida As String
    Dim dt_impressao_saida As String
    Dim descricao_saida As String
    Dim descricao_aux As Variant
    Dim SQL As String

    '05/12/2005 - Ana Paula - Stefanini
    Dim sSQLNU As String
    Dim rsNomeUsuario As rdoResultset

    On Error GoTo Erro
    'lpinto
    endosso_cancel = False
    If Not bPropostaRecusada And Not bPropostaNaoEmitida And Not bPropostaEstudo Then    'rmaiellaro (19/01/2007)
        gridEndosso.Rows = 1


        'MYOSHIMURA 11/12/2004
        'SE HOUVER MAIS DE 43750 ENDOSSOS
        'EXIBIR SOMENTE OS DO ANO VIGENTE

        '        SQL = "SELECT COUNT(1) "
        '        SQL = SQL & "FROM  endosso_tb e WITH (NOLOCK) "
        '        SQL = SQL & "LEFT JOIN tp_endosso_tb te WITH (nolock) "
        '        SQL = SQL & "   ON  te.tp_endosso_id = e.tp_endosso_id "
        '        SQL = SQL & "LEFT JOIN fatura_tb f on  f.proposta_id = e.proposta_id   and f.endosso_id = e.endosso_id "
        '        SQL = SQL & "WHERE e.proposta_id = " & vPropostaId
        '        Set rc = rdoCn1.OpenResultset(SQL, rdOpenStatic)


        SQL = "SELECT COUNT(1) "
        SQL = SQL & "FROM  endosso_tb e WITH (NOLOCK) "
        SQL = SQL & "WHERE e.proposta_id = " & vPropostaId

        Set rc = rdoCn1.OpenResultset(SQL, rdOpenStatic)

        '
        If Not rc.EOF Then

            If rc(0) > 999 Then
                MsgBox " Sua pesquisa retornou " & rc(0) & "  endossos. " & _
                     " Ser�o visualizados 1.000 endossos. " & _
                     " Para visualizar os endossos limitados � este ano, " & _
                     " clique no bot�o 'Visualizar Endosso' na aba Endosso. "


                ' coloqquei top 1000 mc_amaral 22/10/201317860335 - Melhorias no SEGBR(MP13?) - Acelerar aplicativos de consulta acessados pelo Sinistro
                SQL = ""
                SQL = "SELECT DISTINCT  top 1000 e.endosso_id                                        " & vbNewLine
                SQL = SQL & ", e.dt_emissao                                                     " & vbNewLine
                SQL = SQL & ", e.dt_pedido_endosso                                              " & vbNewLine
                SQL = SQL & ", e.dt_impressao                                                   " & vbNewLine
                SQL = SQL & ", CONVERT(VARCHAR(7900), e.descricao_endosso) descricao_endosso    " & vbNewLine
                SQL = SQL & ", ISNULL(e.tp_endosso_id, 0) tp_endosso_id                         " & vbNewLine
                SQL = SQL & ", ISNULL(te.descricao, '') descricao                               " & vbNewLine
                SQL = SQL & ", e.origem_endosso                                                 " & vbNewLine
                SQL = SQL & ", f.dt_inicio_vigencia                                             " & vbNewLine
                SQL = SQL & ",  case when   upper(e.usuario)  = 'PRODUCAO2' or  upper(e.usuario) = '99988877714'  "
                SQL = SQL & "        then  'Usu�rio Produ��o' else  u.nome end  as usuario " & vbNewLine
                SQL = SQL & ", s.sub_grupo_id                                                   " & vbNewLine
                SQL = SQL & ", ISNULL(CASE WHEN te.tp_endosso_id IN (100,101)                   " & vbNewLine
                SQL = SQL & "               THEN ' ' + REPLICATE('0', 3 - LEN(s.sub_grupo_id)) + CAST(s.sub_grupo_id AS VARCHAR(3)) + ' - ' + s.nome " & vbNewLine
                SQL = SQL & "               END, ' ') sub_grupo_endoso                          " & vbNewLine
                SQL = SQL & " ,e.tp_alteracao_endosso as tp_alteracao_endosso                    " & vbNewLine  'rfarzat1
                SQL = SQL & "      FROM endosso_TB e  WITH (NOLOCK)                               " & vbNewLine
                SQL = SQL & "INNER JOIN tp_endosso_TB te  WITH (NOLOCK)                           " & vbNewLine
                SQL = SQL & "        ON te.tp_endosso_id = e.tp_endosso_id                      " & vbNewLine
                SQL = SQL & "LEFT JOIN segab_db.dbo.usuario_tb u                               " & vbNewLine
                SQL = SQL & "        ON  UPPER(u.cpf) = UPPER(e.usuario)                        " & vbNewLine
                SQL = SQL & "        OR UPPER(u.login_rede) = UPPER(e.usuario)                  " & vbNewLine
                SQL = SQL & " LEFT JOIN fatura_TB f  WITH (NOLOCK)                                " & vbNewLine
                SQL = SQL & "ON f.proposta_id = e.proposta_id                                   " & vbNewLine
                SQL = SQL & "AND f.endosso_id = e.endosso_id                                    " & vbNewLine
                SQL = SQL & "LEFT JOIN sub_grupo_apolice_tb AS s  WITH (NOLOCK)                   " & vbNewLine
                SQL = SQL & "ON s.apolice_id = f.apolice_id                                     " & vbNewLine
                SQL = SQL & "AND s.sucursal_seguradora_id = f.sucursal_seguradora_id            " & vbNewLine
                SQL = SQL & "AND s.seguradora_cod_susep = f.seguradora_cod_susep                " & vbNewLine
                SQL = SQL & "AND s.ramo_id = f.ramo_id                                          " & vbNewLine
                SQL = SQL & "AND s.sub_grupo_id = f.sub_grupo_id "

                SQL = SQL & "WHERE e.proposta_id = " & vPropostaId & " " & vbNewLine
                ' mc_amaral 22/10/201317860335 - Melhorias no SEGBR(MP13?) - Acelerar aplicativos de consulta acessados pelo Sinistro
                SQL = SQL & " AND upper(isnull(u.nome,'')) <> 'USUARIO DA PRODU��O - AUTPROD2'"

                ' mc_amaral 22/10/201317860335 - Melhorias no SEGBR(MP13?) - Acelerar aplicativos de consulta acessados pelo Sinistro
                '        If rc(0) > 43750 Then
                '            SQL = SQL & "AND e.dt_emissao >= '" & Year(Date) & "0101" & "' "
                '        End If

                SQL = SQL & _
                      "ORDER BY e.endosso_id "

                rc.Close

                Set rc = rdoCn1.OpenResultset(SQL, rdOpenStatic)


                vis_endosso.Visible = True

            Else

                SQL = ""
                SQL = " SELECT DISTINCT  e.endosso_id                                            " & vbNewLine
                SQL = SQL & ", e.dt_emissao                                                     " & vbNewLine
                SQL = SQL & ", e.dt_pedido_endosso                                              " & vbNewLine
                SQL = SQL & ", e.dt_impressao                                                   " & vbNewLine
                SQL = SQL & ", CONVERT(VARCHAR(7900), e.descricao_endosso) descricao_endosso    " & vbNewLine
                SQL = SQL & ", ISNULL(e.tp_endosso_id, 0) tp_endosso_id                         " & vbNewLine
                SQL = SQL & ", ISNULL(te.descricao, '') descricao                               " & vbNewLine
                SQL = SQL & ", e.origem_endosso                                                 " & vbNewLine
                SQL = SQL & ", f.dt_inicio_vigencia                                             " & vbNewLine
                SQL = SQL & ",  case when   upper(e.usuario)  = 'PRODUCAO2' or  upper(e.usuario) = '99988877714'  "
                SQL = SQL & "        then  'Usu�rio Produ��o' else  u.nome end  as usuario      " & vbNewLine
                SQL = SQL & ", s.sub_grupo_id                                                   " & vbNewLine
                SQL = SQL & ", ISNULL(CASE WHEN te.tp_endosso_id IN (100,101)                   " & vbNewLine
                SQL = SQL & "               THEN ' ' + REPLICATE('0', 3 - LEN(s.sub_grupo_id)) + CAST(s.sub_grupo_id AS VARCHAR(3)) + ' - ' + s.nome " & vbNewLine
                SQL = SQL & "               END, ' ') sub_grupo_endoso                          " & vbNewLine
                SQL = SQL & " ,e.tp_alteracao_endosso as tp_alteracao_endosso                    " & vbNewLine  'rfarzat1
                SQL = SQL & "      FROM endosso_TB e  WITH (NOLOCK)                               " & vbNewLine
                SQL = SQL & "INNER JOIN tp_endosso_TB te  WITH (NOLOCK)                           " & vbNewLine
                SQL = SQL & "        ON te.tp_endosso_id = e.tp_endosso_id                      " & vbNewLine
                SQL = SQL & "left JOIN segab_db.dbo.usuario_tb u   WITH (NOLOCK)               " & vbNewLine
                SQL = SQL & "        ON  UPPER(u.cpf) = UPPER(e.usuario)                        " & vbNewLine
                SQL = SQL & "        OR UPPER(u.login_rede) = UPPER(e.usuario)                  " & vbNewLine
                SQL = SQL & " LEFT JOIN fatura_TB f  WITH (NOLOCK)                                " & vbNewLine
                SQL = SQL & "ON f.proposta_id = e.proposta_id                                   " & vbNewLine
                SQL = SQL & "AND f.endosso_id = e.endosso_id                                    " & vbNewLine
                SQL = SQL & "LEFT JOIN sub_grupo_apolice_tb AS s  WITH (NOLOCK)                   " & vbNewLine
                SQL = SQL & "ON s.apolice_id = f.apolice_id                                     " & vbNewLine
                SQL = SQL & "AND s.sucursal_seguradora_id = f.sucursal_seguradora_id            " & vbNewLine
                SQL = SQL & "AND s.seguradora_cod_susep = f.seguradora_cod_susep                " & vbNewLine
                SQL = SQL & "AND s.ramo_id = f.ramo_id                                          " & vbNewLine
                SQL = SQL & "AND s.sub_grupo_id = f.sub_grupo_id "

                SQL = SQL & "WHERE e.proposta_id = " & vPropostaId & " " & vbNewLine
                SQL = SQL & " AND upper(isnull(u.nome,'')) <> 'USUARIO DA PRODU��O - AUTPROD2'"

                ' mc_amaral 22/10/201317860335 - Melhorias no SEGBR(MP13?) - Acelerar aplicativos de consulta acessados pelo Sinistro
                '        If rc(0) > 43750 Then
                '            SQL = SQL & "AND e.dt_emissao >= '" & Year(Date) & "0101" & "' "
                '        End If

                SQL = SQL & _
                      "ORDER BY e.endosso_id "
                rc.Close
                vis_endosso.Visible = False

                Set rc = rdoCn1.OpenResultset(SQL, rdOpenStatic)

            End If

            Do While Not rc.EOF
                DoEvents
                descricao_aux = rc!descricao_endosso
                descricao_saida = IIf(IsNull(descricao_aux), " ", descricao_aux)
                descricao_saida = Trim(descricao_saida)
                linha = rc!endosso_id
                '' Data de Ini. Vig. Fatura
                '''Pedro Henrique Gomes dos Santos em 06 de Maio de 2004'''''''''''''''''''''''''''''''''''''''
                ' Tratamento do SCS - GEROT/DIOPE 233-04
                linha = linha & vbTab & Format(rc!dt_emissao, "dd/mm/yyyy")
                linha = linha & vbTab & Format(rc!dt_pedido_endosso, "dd/mm/yyyy")
                '''Fim - Pedro Henrique Gomes dos Santos'''''''''''''''''''''''''''''''''''''''''''''''''''''''

                'Por Daniel Romualdo - 06/11/2003
                If rc!tp_endosso_id = 100 Then    'Fatura
                    linha = linha & vbTab & Format(rc!dt_inicio_vigencia, "dd/mm/yyyy")
                    gridEndosso.ColWidth(3) = 1500
                Else
                    linha = linha & vbTab & " "
                End If

                If rc!tp_endosso_id = 0 Then
                    linha = linha & vbTab & " "
                Else
                    linha = linha & vbTab & rc!tp_endosso_id & " - " & Trim(rc!Descricao)
                End If
                '
                If IsNull(rc!dt_emissao) Then
                    dt_emissao_saida = " "
                Else
                    dt_emissao_saida = Format(rc!dt_emissao, "dd/mm/yyyy")
                End If
                '
                If IsNull(rc!dt_impressao) Then
                    dt_impressao_saida = " "
                ElseIf Year(rc!dt_impressao) = "1900" Then
                    dt_impressao_saida = "Migra��o"
                Else
                    dt_impressao_saida = Format(rc!dt_impressao, "dd/mm/yyyy")
                End If
                '
                linha = linha & vbTab & dt_impressao_saida
                linha = linha & vbTab & descricao_saida
                ''
                Select Case UCase(rc!origem_endosso)
                Case "A"
                    linha = linha & vbTab & "AUTOMATICO"
                Case "M"
                    linha = linha & vbTab & "MANUAL"
                Case Else
                    linha = linha & vbTab & ""
                End Select

                ' alterado 12/09/2013 projeto 17860335 - Melhorias no SEGBR MC_Amaral -confitec
                '05/12/2005 - Ana Paula - Stefanini
                'Inclus�o da Informa��o Usu�rio.
                'If UCase(Trim(rc("usuario"))) = "PRODUCAO2" Or Trim(rc("usuario")) = "99988877714" Then
                '    linha = linha & vbTab & "Usu�rio Produ��o"
                ' Else
                'inicio alterado 17/09/2013 projeto 17860335 - Melhorias no SEGBR MC_Amaral -confitec
                '                sSQLNU = ""
                '                sSQLNU = sSQLNU & " SELECT DISTINCT nome FROM segab_db..usuario_tb" & vbNewLine
                '                sSQLNU = sSQLNU & " WHERE cpf = '" & UCase(Trim(rc("usuario"))) & "'" & vbNewLine
                '                sSQLNU = sSQLNU & " OR UPPER(login_rede) = upper('" & UCase(Trim(rc("usuario"))) & "')"
                '
                '                Set rsNomeUsuario = rdoCn1.OpenResultset(sSQLNU, rdOpenStatic)
                '
                '                If Not rsNomeUsuario.EOF Then
                '   linha = linha & vbTab & Trim(rc("NOME"))
                '                End If
                '
                '                rsNomeUsuario.Close

                ' End If
                'fim alterado 12/09/2013 projeto 17860335 - Melhorias no SEGBR MC_Amaral -confitec
                linha = linha & vbTab & Trim(rc("usuario"))

                linha = linha & vbTab & rc!sub_grupo_endoso

                If rc!tp_endosso_id = 310 Or _
                   rc!tp_endosso_id = 63 Or _
                   rc!tp_endosso_id = 64 Or _
                   rc!tp_endosso_id = 102 Or _
                   rc!tp_endosso_id = 103 Then
                    endosso_cancel = True
                End If

                linha = linha & vbTab & rc!tp_alteracao_endosso

                gridEndosso.AddItem linha
                '
                rc.MoveNext
            Loop

            rc.Close
            Set rc = Nothing
        End If

    End If
    Exit Sub
    Resume
Erro:
    TrataErroGeral "Ler Endosso Proposta"
    MsgBox "Rotina: Ler Endosso Proposta"
    'End

End Sub

Private Sub Ler_Endosso_Financeiro()

    Dim rc As rdoResultset, rc1 As rdoResultset
    Dim linha As String
    Dim SQL As String
    On Error GoTo Erro

    SQL = "SELECT a.endosso_id, a.val_financeiro,  "
    SQL = SQL & "b.dt_pedido_endosso, isnull(b.tp_endosso_id, 0) tp_endosso_id "
    SQL = SQL & ", isnull(c.descricao, '') descricao "
    SQL = SQL & ", b.usuario "    '05/12/2005 - Ana Paula - Stefanini
    SQL = SQL & "        FROM endosso_financeiro_tb a  WITH (NOLOCK)  "
    SQL = SQL & "        JOIN endosso_tb b  WITH (NOLOCK)  "
    SQL = SQL & "          ON  a.proposta_id = b.proposta_id"
    SQL = SQL & "         AND a.endosso_id =  b.endosso_id "
    SQL = SQL & " RIGHT  JOIN  tp_endosso_tb c   WITH (NOLOCK)"
    SQL = SQL & "          ON c.tp_endosso_id = b.tp_endosso_id "
    SQL = SQL & "WHERE a.proposta_id = " & vPropostaId

    'SQL = SQL & " AND c.tp_endosso_id =* b.tp_endosso_id "'alterado 12/09/2013 projeto 17860335 - Melhorias no SEGBR MC_Amaral -confitec
    SQL = SQL & "ORDER BY a.endosso_id "

    Set rc = rdocn.OpenResultset(SQL)

    gridEndosso.Rows = 1

    If Not rc.EOF Then
        While Not rc.EOF
            linha = rc!endosso_id
            linha = linha & vbTab & Format(rc!dt_pedido_endosso, "dd/mm/yyyy")
            If rc!tp_endosso_id = 0 Then
                linha = linha & vbTab & " "
            Else
                linha = linha & vbTab & rc!tp_endosso_id & " - " & Trim(rc!Descricao)
            End If
            If ConfiguracaoBrasil Then
                linha = linha & vbTab & Format(Val(rc!val_financeiro), "###,###,###,##0.00")
            Else
                linha = linha & vbTab & TrocaValorAmePorBras(Format(rc!val_financeiro, "###,###,###,##0.00"))
            End If
            If rc!tp_endosso_id = 63 Then
                SQL = "SELECT dt_acerto, isnull(voucher_id,0) voucher_id "
                SQL = SQL & " FROM ps_mov_endosso_financeiro_tb a   WITH (NOLOCK)  , "
                SQL = SQL & "      ps_movimentacao_tb b     WITH (NOLOCK) ,"
                SQL = SQL & "      ps_mov_cliente_tb c    WITH (NOLOCK)  ,"
                SQL = SQL & "      ps_acerto_pagamento_tb d    WITH (NOLOCK)  , "
                SQL = SQL & "      ps_acerto_tb e   WITH (NOLOCK)  "
                SQL = SQL & " WHERE  a.proposta_id = " & vPropostaId & " and "
                SQL = SQL & "        a.endosso_id = " & rc!endosso_id & " and "
                SQL = SQL & "        a.movimentacao_id = b.movimentacao_id  and "
                SQL = SQL & "        b.movimentacao_id = c.movimentacao_id and "
                SQL = SQL & "        c.acerto_id = d.acerto_id and "
                SQL = SQL & "        c.acerto_id = e.acerto_id and "
                SQL = SQL & "        d.situacao = 'a'"
                'GENJUNIOR - FLOW 17860335 - CONSULTA TABELAS DE HIST�RICO
                SQL = SQL & "  UNION " & vbNewLine
                SQL = SQL & " SELECT dt_acerto " & vbNewLine
                SQL = SQL & "      , ISNULL(voucher_id, 0) voucher_id " & vbNewLine
                SQL = SQL & "   FROM seguros_hist_db.dbo.ps_mov_endosso_financeiro_hist_tb a WITH (NOLOCK) " & vbNewLine
                SQL = SQL & "      , seguros_hist_db.dbo.ps_movimentacao_hist_tb b WITH (NOLOCK) " & vbNewLine
                SQL = SQL & "      , seguros_hist_db.dbo.ps_mov_cliente_hist_tb c WITH (NOLOCK) " & vbNewLine
                SQL = SQL & "      , ps_acerto_pagamento_tb d WITH (NOLOCK) " & vbNewLine
                SQL = SQL & "      , ps_acerto_tb e WITH (NOLOCK) " & vbNewLine
                SQL = SQL & "  WHERE a.proposta_id = " & vPropostaId & vbNewLine
                SQL = SQL & "    AND a.endosso_id = " & rc!endosso_id & vbNewLine
                SQL = SQL & "    AND a.movimentacao_id = b.movimentacao_id " & vbNewLine
                SQL = SQL & "    AND b.movimentacao_id = c.movimentacao_id " & vbNewLine
                SQL = SQL & "    AND c.acerto_id = d.acerto_id " & vbNewLine
                SQL = SQL & "    AND c.acerto_id = e.acerto_id " & vbNewLine
                SQL = SQL & "    AND d.situacao = 'a' " & vbNewLine
                'FIM GENJUNIOR
                Set rc1 = rdoCn1.OpenResultset(SQL)

                If Not rc1.EOF Then
                    linha = linha & Format(rc1!dt_acerto, "dd/mm/yyyy") & vbTab
                    linha = linha & rc1!voucher_id
                End If

                rc1.Close
            End If

            gridEndosso.AddItem linha

            rc.MoveNext

        Wend
    End If

    rc.Close

    Exit Sub

Erro:
    TrataErroGeral "Ler Endosso Financeiro"
    MsgBox "Rotina: Ler Endosso Financeiro"
    End
End Sub
'Renato Vasconcelos
'Flow 14735947
'Inicio
Private Sub Ler_Tipo_Segurado()

    Dim rc As rdoResultset, linha As String
    Dim SQL As String
    Dim tpSegurado As String
    Dim propostaId As Long

    On Error GoTo Erro

    cboTpSegurado.AddItem "Varejo"
    cboTpSegurado.ItemData(cboTpSegurado.NewIndex) = 1
    cboTpSegurado.AddItem "Pilar Atacado"
    cboTpSegurado.ItemData(cboTpSegurado.NewIndex) = 2
    cboTpSegurado.AddItem "Corporate"
    cboTpSegurado.ItemData(cboTpSegurado.NewIndex) = 3

    propostaId = CLng(txtNumero.Text)
    SQL = "EXEC SEGS10437_SPS " & CStr(propostaId)
    Set rc = rdoCn1.OpenResultset(SQL)
    tpSegurado = ""
    If Not rc.EOF Then

        tpSegurado = rc!tp_segurado_proposta

    End If
    rc.Close
    Select Case tpSegurado
    Case "V"
        cboTpSegurado.ListIndex = 0
    Case "P"
        cboTpSegurado.ListIndex = 1
    Case "C"
        cboTpSegurado.ListIndex = 2
    Case Else
        cboTpSegurado.ListIndex = -1
    End Select

    Exit Sub

Erro:
    TrataErroGeral "Ler Tipo Segurado"
    MsgBox "Rotina: Ler Tipo Segurado"
End Sub
'Fim
Private Sub Ler_Sinistro()

    Dim rc As rdoResultset, linha As String
    Dim Sucursal As String, Ramo As String, Cod_Susep As String
    Dim SQL As String

    On Error GoTo Erro
    'lpinto
    If Not bPropostaRecusada And Not bPropostaNaoEmitida And Not bPropostaEstudo Then    'rmaiellaro (19/01/2007)
        GridSinistro.Rows = 1
        '
        SQL = "SELECT a.sinistro_id, b.nome, a.dt_aviso_sinistro, "
        SQL = SQL & "Sitnome = CASE a.situacao "
        SQL = SQL & "       WHEN '0' THEN 'Aberto' "
        SQL = SQL & "       WHEN '1' THEN 'Reaberto' "
        SQL = SQL & "       WHEN '2' THEN 'Liquidado' "
        SQL = SQL & "       WHEN '3' THEN 'Encerrado por ind�cio de fraude' "
        SQL = SQL & "       WHEN '4' THEN 'Encerrado sem cobertura t�cnica' "
        SQL = SQL & "       WHEN '5' THEN 'Encerrado por duplicidade de registro' "
        SQL = SQL & "       WHEN '6' THEN 'Encerrado por falta de documenta��o' "
        SQL = SQL & "       WHEN '7' THEN 'Encerrado a pedido da lider' "
        SQL = SQL & "       WHEN '8' THEN 'Encerrado cosseguro inferior' "
        SQL = SQL & "       WHEN '9' THEN 'Aguardando financeiro(encerrado ou liquidado)' "
        SQL = SQL & "       ELSE ' ' END "
        SQL = SQL & "FROM sinistro_tb a    WITH (NOLOCK) , evento_sinistro_tb b    WITH (NOLOCK)   "
        SQL = SQL & "WHERE a.evento_sinistro_id = b.evento_sinistro_id"
        SQL = SQL & " AND   a.ramo_id = " & vRamoId
        SQL = SQL & " AND   a.apolice_id = " & vApoliceId
        SQL = SQL & " AND   a.sucursal_seguradora_id = " & vSucursalId
        SQL = SQL & " AND   a.seguradora_cod_susep = " & vSeguradoraId
        SQL = SQL & " ORDER BY sinistro_id"

        Set rc = rdocn.OpenResultset(SQL)

        If Not rc.EOF Then
            SSTabProp.TabEnabled(TAB_SINISTRO) = True
            '
            While Not rc.EOF

                linha = rc!sinistro_id
                linha = linha & vbTab & Format(rc!dt_aviso_sinistro, "dd/mm/yyyy")
                linha = linha & vbTab & Trim(rc!Nome)
                linha = linha & vbTab & rc!sitnome

                GridSinistro.AddItem linha

                rc.MoveNext
            Wend
            '
        Else
            SSTabProp.TabEnabled(TAB_SINISTRO) = False
        End If

        rc.Close

    Else

        SSTabProp.TabEnabled(TAB_SINISTRO) = False

    End If

    Exit Sub

Erro:
    TrataErroGeral "Ler Sinistro"
    MsgBox "Rotina: Ler Sinistro"

End Sub

Private Sub Ler_Apolice()

    Dim rc As rdoResultset
    Dim rc_aux As rdoResultset
    Dim SQL As String
    Dim Sql_Aux As String
    Dim rdoCn3 As New rdoConnection

    With rdoCn3
        .Connect = rdocn.Connect
        .CursorDriver = rdUseServer
        .QueryTimeout = 3600
        .EstablishConnection rdDriverNoPrompt
    End With

    On Error GoTo Erro

    'Demanda MU-2017-042351 - IN�CIO

    '    SQL = "SELECT apolice_id, ramo_id, sucursal_seguradora_id, seguradora_cod_susep"
    '    SQL = SQL & ", dt_emissao, dt_inicio_vigencia, dt_fim_vigencia, tp_emissao, isnull(val_lim_retencao_resseg ,0) val_lim_retencao_resseg "
    '    SQL = SQL & "FROM apolice_tb   WITH (NOLOCK) "
    '    SQL = SQL & "WHERE proposta_id = " & vPropostaId

    If vProdutoId <> 1206 Then
        SQL = "SELECT apolice_id, ramo_id, sucursal_seguradora_id, seguradora_cod_susep"
        SQL = SQL & ", dt_emissao, dt_inicio_vigencia, dt_fim_vigencia, tp_emissao, isnull(val_lim_retencao_resseg ,0) val_lim_retencao_resseg "
        SQL = SQL & "FROM apolice_tb   WITH (NOLOCK) "
        SQL = SQL & "WHERE proposta_id = " & vPropostaId
    Else
        SQL = "Select  A.apolice_id, A.ramo_id, A.sucursal_seguradora_id, A.seguradora_cod_susep, A.dt_emissao, " & vbLf
        SQL = SQL & "  A.dt_inicio_vigencia, " & vbLf
        SQL = SQL & "  Case When B.proposta_id Is Not Null " & vbLf
        SQL = SQL & "       Then B.dt_fim_vigencia_end " & vbLf
        SQL = SQL & "       Else A.dt_fim_vigencia " & vbLf
        SQL = SQL & "  End as dt_fim_vigencia, " & vbLf
        SQL = SQL & "  tp_emissao, isnull(val_lim_retencao_resseg ,0) val_lim_retencao_resseg " & vbLf
        SQL = SQL & "From apolice_tb A With (Nolock) " & vbLf
        SQL = SQL & "Left Join (Select en.proposta_id, MAX(en.dt_inicio_vigencia_end) as dt_inicio_vigencia_end, MAX(en.dt_fim_vigencia_end) as dt_fim_vigencia_end " & vbLf
        SQL = SQL & "           From endosso_tb en With (Nolock) " & vbLf
        SQL = SQL & "           Left Join cancelamento_endosso_tb ce With (Nolock) " & vbLf
        SQL = SQL & "               On en.proposta_id = ce.proposta_id " & vbLf
        SQL = SQL & "               And en.endosso_id = ce.canc_endosso_id " & vbLf
        SQL = SQL & "           Where en.proposta_id = " & vPropostaId & vbLf
        SQL = SQL & "           And en.tp_endosso_id = 314 " & vbLf
        SQL = SQL & "           And ce.proposta_id Is Null " & vbLf
        SQL = SQL & "           Group by en.proposta_id) B " & vbLf
        SQL = SQL & "   On A.proposta_id = B.proposta_id " & vbLf
        SQL = SQL & "Where A.proposta_id = " & vPropostaId & vbLf
    End If
    'Demanda MU-2017-042351 - FIM

    'Jessica.Adao - Confitec Sistemas - 20181031 - Sprint 10: Ajustes Amparo
    If vProdutoId = 1217 Then SQL = ObterApoliceAmparo(CLng(vPropostaId))

    '*****************************************************************************
    '** Descri��o: Implementa��o de consulta por proposta ades�o                **
    '** Motivo: Atender consultas para o produto 714                            **
    '** Autor: Febner (Fabio Alves de Araujo Ebner - JR & P(SP)                 **
    '** Data: 10/06/03                                                          **
    '*****************************************************************************
    Sql_Aux = SQL
    Set rc_aux = rdoCn3.OpenResultset(Sql_Aux)
    If rc_aux.EOF Then
        SQL = ""
        SQL = SQL & " SELECT c.apolice_id, "
        SQL = SQL & "   c.ramo_id, "
        SQL = SQL & "   c.sucursal_seguradora_id,   "
        SQL = SQL & "   c.seguradora_cod_susep, "
        SQL = SQL & "   c.dt_emissao, "
        SQL = SQL & "   c.dt_inicio_vigencia, "
        SQL = SQL & "   c.dt_fim_vigencia, "
        SQL = SQL & "   a.tp_emissao, "
        SQL = SQL & "   isnull(a.val_lim_retencao_resseg ,0)"
        SQL = SQL & " val_lim_retencao_resseg "
        SQL = SQL & " FROM     certificado_tb c  WITH (NOLOCK) , apolice_tb a  WITH (NOLOCK)  "
        SQL = SQL & " WHERE c.proposta_id = " & vPropostaId
        SQL = SQL & "   and c.apolice_id = a.apolice_Id"
        SQL = SQL & "   and c.ramo_id = a.ramo_id"
    End If
    '*****************************************************************
    Set rc = rdocn.OpenResultset(SQL)
    If Not rc.EOF Then
        If Not IsNull(rc!dt_inicio_vigencia) Then
            txtIniVigencia.Text = Format$(rc!dt_inicio_vigencia, "dd/mm/yyyy")
        End If
        '
        If Not IsNull(rc!dt_fim_vigencia) Then
            txtFimVigencia.Text = Format$(rc!dt_fim_vigencia, "dd/mm/yyyy")
        End If
        '
        If Not IsNull(rc!dt_emissao) Then
            txtDtEmissao.Text = Format(rc!dt_emissao, "dd/mm/yyyy")
        End If
        '
        mskLimRetResseguro.Mask = "15,2"
        mskLimRetResseguro.Text = Format$(CCur(TrocaPontoPorVirgula(rc!val_lim_retencao_resseg)), "###############0.00")

        If Not IsNull(rc!tp_emissao) Then
            gTpEmissao = rc!tp_emissao
            If gTpEmissao = "A" Then
                CosseguroAceito = True
            Else
                CosseguroAceito = False
            End If
        End If
        '
        txtApolice.Text = Format(rc!apolice_id, "000000000")
        Text1(0).Text = Format(rc!apolice_id, "000000000")
        vApoliceId = rc!apolice_id
        vSeguradoraId = rc!seguradora_cod_susep
        vSucursalId = rc!sucursal_seguradora_id
        vRamoId = rc!ramo_id
    End If

    rc.Close
    Set rc = Nothing

    ' Autor: pablo.cardoso (Nova Consultoria)
    ' Data da Altera��o: 11/11/2011
    ' Existe ainda a possibilidade de vRamoId vir com valor vazio
    ' impactando v�rias partes do programa, motivo desta altera��o
    If vRamoId = "" Then
        vRamoId = RetornaRamoByPropostaCorrente
    End If
    '

    Exit Sub

Erro:
    TrataErroGeral "Ler Apolice"
    MsgBox "Rotina: Ler Apolice"
    'End

End Sub
Function RetornaRamoByPropostaCorrente() As String

    Dim SQL As String
    Dim rc As rdoResultset

    On Error GoTo Erro

    SQL = ""
    SQL = SQL & "SELECT ramo_id " & vbNewLine
    SQL = SQL & "  FROM proposta_tb WITH (NOLOCK)  " & vbNewLine
    SQL = SQL & " WHERE proposta_id = " & vPropostaId


    Set rc = rdocn.OpenResultset(SQL)

    If Not rc.EOF Then
        RetornaRamoByPropostaCorrente = rc!ramo_id
    Else
        RetornaRamoByPropostaCorrente = ""
    End If

    Exit Function

Erro:
    MousePointer = vbDefault
    '
    TrataErroGeral "Erro ao carregar a tela de Consulta"
    MsgBox "Erro ao carregar a tela de Consulta"
    'Resume Next
End Function


Function TrocaValorAmePorBras(ByVal Variavel As Variant) As Variant
'
' Respons�vel : Carlos Rosa
' Dt Desenv : 27/04/98
'
' Recebe um valor qualquer
' Retorna :
' Valor sem v�rgulas, em letras mai�sculas e sem espa�os vazios
'
' troca "," por "V"
'
    While InStr(Variavel, ",") <> 0
        Variavel = Mid(Variavel, 1, InStr(Variavel, ",") - 1) + "V" + Mid(Variavel, InStr(Variavel, ",") + 1)
    Wend
    '
    ' troca "." por ","
    '
    While InStr(Variavel, ".") <> 0
        Variavel = Mid(Variavel, 1, InStr(Variavel, ".") - 1) + "," + Mid(Variavel, InStr(Variavel, ".") + 1)
    Wend
    '
    ' troca "V" por "."
    '
    While InStr(Variavel, "V") <> 0
        Variavel = Mid(Variavel, 1, InStr(Variavel, "V") - 1) + "." + Mid(Variavel, InStr(Variavel, "V") + 1)
    Wend
    TrocaValorAmePorBras = UCase(Trim(Variavel))

    ' troca " " por "."
    '
    While InStr(Variavel, " ") <> 0
        Variavel = Mid(Variavel, 1, InStr(Variavel, " ") - 1) + "." + Mid(Variavel, InStr(Variavel, " ") + 1)
    Wend
End Function

Function LeArquivoIni(ByVal FileName As String, ByVal SectionName As String, ByVal Item As String) As String

    Dim Retorno As String * 100
    Dim RetornoDefault As String
    Dim nc As String

    RetornoDefault = "*"
    nc = GetPrivateProfileString(SectionName, Item, RetornoDefault, Retorno, Len(Retorno), FileName)
    LeArquivoIni = Left$(Retorno, nc)

End Function

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)

    cmdSair_Click

End Sub


Private Sub grdHistorico_Click()
    If grdHistorico.TextMatrix(grdHistorico.Row, 0) <> "" Then
        grdHistorico.HighLight = flexHighlightAlways
        txtDescricao.Text = sTodasDescricoes(grdHistorico.TextMatrix(grdHistorico.Row, 0))
    End If
End Sub

Private Sub grdHistorico_SelChange()
    If grdHistorico.Row <> grdHistorico.RowSel Then
        grdHistorico.RowSel = grdHistorico.Row
    End If
End Sub

Private Sub GridCobranca_DblClick()
'FEBNER
'Esse IF foi acrescentado para qdo o tipo de proposta for ades�o ao invez de fechado nao exiba
'Boleto pois o mesmo n�o existe para ades�o
    If vProdutoId = "1183" Then Exit Sub ' Guilherme Cruz -- Confitec Sistemas
    If GridCobranca.Rows = 1 Then Exit Sub    'Implementado por fpimenta / Imar�s - 16/07/2003
    If Not Prop_Adesao Then
        ExibeBoleto txtNumero.Text, GridCobranca.TextMatrix(GridCobranca.Row, 1), GridCobranca.TextMatrix(GridCobranca.Row, 0), GridCobranca.TextMatrix(GridCobranca.Row, 2)
    End If
End Sub

Private Sub gridEndosso_Click()
'Alessandra Grig�rio - 13/11/2003 - N�o ordenava quando selecionasse o titulo da coluna
    Dim i As Long
    If gridEndosso.ColSel = 0 Then
        Exit Sub
    End If
    If gridEndosso.ColSel <> 1 And gridEndosso.ColSel <> 2 And gridEndosso.MouseRow = 0 Then
        gridEndosso.col = gridEndosso.ColSel
        gridEndosso.Sort = 1
    ElseIf (gridEndosso.ColSel = 1 Or gridEndosso.ColSel = 2 Or gridEndosso.ColSel = 3) And gridEndosso.MouseRow = 0 Then
        For i = 1 To gridEndosso.Rows - 1
            gridEndosso.TextMatrix(i, gridEndosso.ColSel) = Format(gridEndosso.TextMatrix(i, gridEndosso.ColSel), "yyyymmdd")
        Next
        gridEndosso.col = gridEndosso.ColSel
        gridEndosso.Sort = 1
        For i = 1 To gridEndosso.Rows - 1
            gridEndosso.TextMatrix(i, gridEndosso.ColSel) = Mid(gridEndosso.TextMatrix(i, gridEndosso.ColSel), 7, 2) & "/" & _
                                                            Mid(gridEndosso.TextMatrix(i, gridEndosso.ColSel), 5, 2) & "/" & _
                                                            Mid(gridEndosso.TextMatrix(i, gridEndosso.ColSel), 1, 4)
        Next
    End If
End Sub

Private Sub gridEndosso_DblClick()

    Dim SQL As String
    Dim rs As rdoResultset
    Dim num_endosso_consulta As Long

    If gridEndosso.Rows > 1 Then
        gEndossoId = gridEndosso.TextMatrix(gridEndosso.Row, GRD_ENDO_NUM)
        gDtIniVigEndosso = gridEndosso.TextMatrix(gridEndosso.Row, GRD_ENDO_DTINI)
        gDtEmiEndosso = gridEndosso.TextMatrix(gridEndosso.Row, GRD_ENDO_DTEMI)
        gTpEndosso = Val(Left(gridEndosso.TextMatrix(gridEndosso.Row, GRD_ENDO_TPENDO), 3))
        gDescTpEndosso = gridEndosso.TextMatrix(gridEndosso.Row, GRD_ENDO_TPENDO)
        gTpAltEnd = gridEndosso.TextMatrix(gridEndosso.Row, GRD_TP_ALT_END)  'rfarzat - 24/10/2018 - Endosso 252 - Sala �gil

        '        gDescEndosso = gridEndosso.TextMatrix(gridEndosso.Row, GRD_ENDO_DESC)

        'Agustin 25/09/2003
        SQL = "select descricao_endosso, ISNULL(usuario_emissao, '') usuario_emissao from endosso_tb WITH(NOLOCK) where proposta_id = " & txtNumero.Text
        SQL = SQL & " and endosso_id = " & gridEndosso.TextMatrix(gridEndosso.Row, GRD_ENDO_NUM)
        Set rs = rdoCn1.OpenResultset(SQL, rdOpenStatic)
        gDescEndosso = " " & rs!descricao_endosso
        gUsuEmissao = rs!usuario_emissao
        rs.Close

        '* Se for um cancelamento de endosso, pega o tipo de endosso cancelado. (Marisa).
        If gTpEndosso = TP_ENDO_CANC_ENDO Then
            SQL = "SELECT isnull(canc_endosso_id,0) canc_endosso_id "
            SQL = SQL & " FROM cancelamento_endosso_tb   WITH (NOLOCK) "
            SQL = SQL & " WHERE proposta_id = " & vPropostaId
            SQL = SQL & " AND endosso_id = " & gEndossoId
            Set rs = rdoCn1.OpenResultset(SQL, rdOpenStatic)
            If Not rs.EOF Then
                If (rs!canc_endosso_id <> 0) Then
                    gEndossoCancId = rs!canc_endosso_id
                    num_endosso_consulta = gEndossoId
                End If
                Set rs = Nothing
            End If
            SQL = "SELECT tp_endosso_id FROM endosso_tb   WITH (NOLOCK) "
            SQL = SQL & " WHERE proposta_id = " & vPropostaId
            SQL = SQL & " AND endosso_id = " & gEndossoCancId
            Set rs = rdoCn1.OpenResultset(SQL, rdOpenStatic)
            If Not rs.EOF Then
                gTpEndossoCanc = rs!tp_endosso_id
            Else
                gTpEndossoCanc = 0
            End If
        Else
            gTpEndossoCanc = 0
        End If
        '* Mostra os dados da fatura se � emiss�o/cancelamento de fatura
        If gTpEndosso = 100 Then    'Por Daniel Romualdo - 06/11/2003
            ConFatura.Show vbModal
        Else
            ConEndosso.Show vbModal
        End If
        SSTabProp.Tab = TAB_ENDOSSO
    End If

End Sub
Private Sub GridFaturas_DblClick()
    Dim SQL As String
    Dim Rst As rdoResultset

    On Error GoTo Erro
    MostraBoleto = True

    'Marcio.Nogueira - CONFITEC SISTEMAS - 25/07/2017 PROJETO: 19368999 - cobran�a registrada AB e ABS
    BlnBoletoRegistrado = False



    ' Comentado por Kylme 06/05/2003
    'Se fatura agregada
    'FEBNER
    'Esse IF foi acrescentado para qdo o tipo de proposta for ades�o ao invez de fechado nao exiba
    'Boleto pois o mesmo n�o existe para ades�o
    If Not Prop_Adesao Then
        If Left(GridFaturas.TextMatrix(GridFaturas.Row, 1), 1) = "*" Then
            'Verificar nr. da fatura_personalizada
            SQL = " select pf.personalizacao_id, pf.personalizacao_fatura_id " & _
                " from personalizacao_fatura_tb pf  WITH (NOLOCK) , personalizacao_cobranca_tb pc  WITH (NOLOCK) , personalizacao_fatura_item_tb pfi   WITH (NOLOCK) " & _
                " where pf.personalizacao_id = pc.personalizacao_id " & _
                " and pf.personalizacao_id = pfi.personalizacao_id " & _
                " and pf.personalizacao_fatura_id = pfi.personalizacao_fatura_id " & _
                " and pc.ramo_id = " & vRamoId & _
                " and pc.apolice_id = " & vApoliceId & _
                " and pfi.fatura_id = " & Mid(GridFaturas.TextMatrix(GridFaturas.Row, 1), 2, Len(GridFaturas.TextMatrix(GridFaturas.Row, 1)))

            Set Rst = rdocn.OpenResultset(SQL)
            personalizacao_id = 0
            personalizacao_fatura_id = 0
            If Not Rst.EOF Then
                personalizacao_id = Rst!personalizacao_id
                personalizacao_fatura_id = Rst!personalizacao_fatura_id
                FaturaAgregada.Show vbModal, Me
            End If
            Rst.Close
        End If

        If MostraBoleto Then

            If InStr(1, GridFaturas.TextMatrix(GridFaturas.Row, 9), "GEROT") <= 0 Then
                'Agrigorio - 31/03/2004 - Alterado para impress�o de boleto de produtos WEB
                If Not (vProdutoId >= 712 And vProdutoId <= 715) Then
                    ' Colhendo nr. da cobran�a
                    SQL = "Select "
                    SQL = SQL & "      num_cobranca "
                    SQL = SQL & "From "
                    SQL = SQL & "      agendamento_cobranca_tb   WITH (NOLOCK) "
                    SQL = SQL & "Where "
                    SQL = SQL & "      proposta_id = " & txtNumero.Text
                    SQL = SQL & "  and num_endosso = " & GridFaturas.TextMatrix(GridFaturas.Row, 0)
                    Set Rst = rdocn.OpenResultset(SQL)

                    If Not Rst.EOF Then
                        'Exibindo cobran�a
                        ExibeBoleto txtNumero.Text, Rst!num_cobranca, GridFaturas.TextMatrix(GridFaturas.Row, 0), GridFaturas.TextMatrix(GridFaturas.Row, 2)
                    Else
                        MsgBox "N�mero de cobran�a n�o encontrado. N�o � poss�vel a visualiza��o da boleta."
                    End If

                    Rst.Close

                Else

                    'Passar como parametro o n�mero da fatura no lugar do agendamento
                    ExibeBoleto txtNumero.Text, GridFaturas.TextMatrix(GridFaturas.Row, 1), GridFaturas.TextMatrix(GridFaturas.Row, 0), GridFaturas.TextMatrix(GridFaturas.Row, 2)

                End If
            Else
                MsgBox "Fatura em an�lise pela Companhia. " & vbCrLf & "N�o disponibilizar ao Estipulante - Consulte a GEROT."
            End If

        End If
    End If
    Exit Sub
Erro:
    MsgBox "Erro obtendo dados da boleta : " & Error, vbCritical
End Sub

Private Sub GridProposta_Click()
    With GridProposta
        If .Rows > 1 Then
            If .Row = 0 Then
                .col = 0
                .FixedRows = 1
                If Sortable = 1 Then
                    Sortable = 2
                    .Sort = flexSortGenericAscending
                Else
                    Sortable = 1
                    .Sort = flexSortGenericDescending
                End If
                .FixedRows = 0
            End If
        End If
    End With

End Sub

'lrocha 29/10/2008 - Demanda 273032
Private Sub gridSubGrupoAssist_DblClick()

    Dim sChave As String
    Dim oAux As Object

    If gridSubGrupoAssist.Rows > 1 Then

        Set oAux = CreateObject("SEGL0308.Cls00490")

        sChave = gridSubGrupoAssist.TextMatrix(gridSubGrupoAssist.Row, 0) & gridSubGrupoAssist.TextMatrix(gridSubGrupoAssist.Row, 1) & Format(gridSubGrupoAssist.TextMatrix(gridSubGrupoAssist.Row, 7), "yyyymmdd")
        linha = ""
        For Each oAux In oSubGrupo.PlanosAssistencia(sChave).ServicoAssistencia
            linha = linha & "Nome do Servi�o: " & oAux.sNomeServico & vbNewLine
            linha = linha & "Limite: " & oAux.sLimite & vbNewLine
            linha = linha & "Abrang�ncia: " & oAux.sAbrangencia & vbNewLine
            linha = linha & vbNewLine
        Next

        If linha <> "" Then
            frmPlanoAssistencia.txtLista.Text = linha
        Else
            frmPlanoAssistencia.txtLista.Text = ""
        End If

        frmPlanoAssistencia.txtCodPlano.Text = gridSubGrupoAssist.TextMatrix(gridSubGrupoAssist.Row, 0)
        frmPlanoAssistencia.txtTpAssistencia.Text = gridSubGrupoAssist.TextMatrix(gridSubGrupoAssist.Row, 2)
        frmPlanoAssistencia.txtCusto.Text = Format(gridSubGrupoAssist.TextMatrix(gridSubGrupoAssist.Row, 3), "###,#00.00")

        frmPlanoAssistencia.Show vbModal

    End If

End Sub

Private Sub GridSubGrupos_Click()
    With GridSubGrupos
        'Alterado por Genesco Silva - Confitec SP em 29/06/2011 - Flow 4532893
        'Inicio da altera��o - Alterado por Thiago Mazzero - Confitec SP em 12/01/2012 - INC000003218986
        'Efetuado reordena��o de acordo com os dados na GridSubGrupos
        If .Rows > 1 Then
            Call Limpa_Campos_Subgrupo
            txtID.Text = .TextMatrix(.Row, 0)
            txtNome_subgrupo.Text = .TextMatrix(.Row, 1)
            TxtDtIniSub.Text = .TextMatrix(.Row, 3)
            txtTp_IS.Text = .TextMatrix(.Row, 4)
            txtMult_Sal.Text = .TextMatrix(.Row, 5)
            txtQuant_vidas.Text = .TextMatrix(.Row, 6)
            If Trim(.TextMatrix(.Row, 8)) = "" Then
                txtTabua_calculo.Text = ""
            Else
                txtTabua_calculo.Text = Format(.TextMatrix(.Row, 7), "00") & " - " & .TextMatrix(.Row, 8)
            End If
            chkFatura_Auto.Value = IIf(Trim(UCase(.TextMatrix(.Row, 9))) = "S", 1, 0)
            txtTexto_Padrao.Text = .TextMatrix(.Row, 10)
            txtDt_faturamento.Text = .TextMatrix(.Row, 11)
            txtDt_Cobranca.Text = .TextMatrix(.Row, 12)
            txtPeriodo_Pgto.Text = .TextMatrix(.Row, 13)
            txtForma_Pgto.Text = .TextMatrix(.Row, 14)
            txtAg_Debito.Text = .TextMatrix(.Row, 15)
            txtCta_Corrente.Text = .TextMatrix(.Row, 16)
            txtConjuge.Text = .TextMatrix(.Row, 17)
            chkIsento_IOF.Value = IIf(Trim(UCase(.TextMatrix(.Row, 18))) = "S", 1, 0)
            txtIdade_Min_Impl.Text = .TextMatrix(.Row, 19)
            txtIdade_Max_Impl.Text = .TextMatrix(.Row, 20)
            txtIdade_Min_Mov.Text = .TextMatrix(.Row, 21)
            txtIdade_Max_Mov.Text = .TextMatrix(.Row, 22)
            chkCartao_porposta.Value = IIf(Trim(UCase(.TextMatrix(.Row, 23))) = "S", 1, 0)

            If chkCartao_porposta.Value = 1 Then
                frmTp_Cartao_proposta.Visible = True
                chkTodos.Value = IIf(Trim(UCase(.TextMatrix(.Row, 24))) = "S", 1, 0)
                chkIdade.Value = IIf(Trim(UCase(.TextMatrix(.Row, 25))) = "S", 1, 0)
                If chkIdade.Value = 1 Then
                    txtIdade.Text = Val(.TextMatrix(.Row, 27))
                Else
                    txtIdade.Text = 0
                End If
                chkCapital.Value = IIf(Trim(UCase(.TextMatrix(.Row, 26))) = "S", 1, 0)
                'MskCapital.Mask = "15,2"
                If chkCapital.Value = 1 Then
                    MskCapital.Text = Format(.TextMatrix(.Row, 28), "#####0.00")
                Else
                    MskCapital.Text = "0,00"
                End If

            Else
                frmTp_Cartao_proposta.Visible = False
                chkTodos.Value = 0
                chkIdade.Value = 0
                chkCapital.Value = 0

            End If

            Select Case Trim(UCase(.TextMatrix(.Row, 29)))
            Case "F": txtTpFaturamento.Text = "Fatura"
            Case "I": txtTpFaturamento.Text = "Individual"
            End Select

            Select Case Trim(UCase(.TextMatrix(.Row, 30)))
            Case "C": txtTpVida.Text = "Rel.Segurado"
            Case "A": txtTpVida.Text = "Ades�o"
            End Select


            Select Case Trim(UCase(.TextMatrix(.Row, 31)))
            Case "C": txtTpCusteio.Text = "Contrib."
            Case "N": txtTpCusteio.Text = "N�o Contrib."
            End Select


            grdFaixas.Rows = 1
            If (txtTp_IS.Text = "Capital por Planos") Then
                Call Monta_GridFaixaPlanoSubGrupo
            End If

            If (txtTp_IS.Text = "Capital por Faixa Et�ria") Then
                Call Monta_GridFaixaEtariaSubGrupo
            End If
            chkAtualizacaoIdade.Value = IIf(Trim(UCase(.TextMatrix(.Row, GRD_SUB_REENQUAD))) = "S", 1, 0)

            txtDtFimVigenciaSBG.Text = Trim(.TextMatrix(.Row, GRD_SUB_EXCLUSAO))

            'pmelo
            If Trim(UCase(.TextMatrix(.Row, 34))) = "A" And Trim(UCase(.TextMatrix(.Row, 9))) = "S" Then
                TxtTipoFaturamento.Text = "Web"
            Else
                TxtTipoFaturamento.Text = "Tradicional"
            End If

            'FIM da altera��o - Alterado por Thiago Mazzero - Confitec SP em 12/01/2012 - INC000003218986

        End If

    End With

End Sub

Private Sub GridVidasSeguradas_Click()

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'Defini��o: Esta rotina carrega grid com os agendamentos do subgrupo. '
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

    Dim rsAgendamento As rdoResultset
    Dim SQL As String
    Dim sLinha As String

    On Error GoTo Trata_Erro

    GridAgendamentoVidasSeguradas.Rows = 1
    '
    If GridVidasSeguradas.Rows > 1 Then
        If Val(GridVidasSeguradas.TextMatrix(GridVidasSeguradas.Row, 0)) > 0 Then
            'Pesquisando dados''''''''''''''''''''''''''''''''''''
            SQL = ""
            SQL = "SELECT num_endosso, num_cobranca, dt_agendamento, val_cobranca"
            SQL = SQL & ", val_iof, dt_recebimento, val_pago "
            SQL = SQL & ", desc_situacao = case "
            SQL = SQL & "       when situacao = 'a' and canc_endosso_id is null then 'Adimplente'"
            SQL = SQL & "       when situacao = 'e' and canc_endosso_id is null then 'Pendente de Envio'"
            SQL = SQL & "       when situacao = 'i' and canc_endosso_id is null then 'Inadimplente'"
            SQL = SQL & "       when situacao = 'r' and canc_endosso_id is null then 'Reenviado'"
            SQL = SQL & "       when situacao = 'p' and canc_endosso_id is null then 'Pendente de Retorno'"
            SQL = SQL & "       when situacao = 'x' and canc_endosso_id is null then 'Estorno'"
            SQL = SQL & "       else 'Cancelado'"
            SQL = SQL & "   end"
            SQL = SQL & " FROM agendamento_cobranca_tb    WITH (NOLOCK)  "
            SQL = SQL & " WHERE proposta_id = " & GridVidasSeguradas.TextMatrix(GridVidasSeguradas.Row, 0)
            SQL = SQL & " AND  situacao in ('a','e','i','r','p','x')"
            SQL = SQL & " ORDER BY num_cobranca "

            Set rsAgendamento = rdocn.OpenResultset(SQL)

            'Exibindo dados''''''''''''''''''''''''''''''''''''''''''''''''

            If Not rsAgendamento.EOF Then
                While Not rsAgendamento.EOF

                    sLinha = ""

                    sLinha = rsAgendamento("num_cobranca") & vbTab

                    sLinha = sLinha & rsAgendamento("dt_agendamento") & vbTab

                    If IsNull(rsAgendamento("val_cobranca")) Then
                        sLinha = sLinha & vbTab
                    Else
                        sLinha = sLinha & TrocaVirgulaPorPonto(Val(rsAgendamento("val_cobranca"))) & vbTab
                    End If

                    If IsNull(rsAgendamento("val_iof")) Then
                        sLinha = sLinha & vbTab
                    Else
                        sLinha = sLinha & TrocaVirgulaPorPonto(Val(rsAgendamento("val_iof"))) & vbTab
                    End If

                    sLinha = sLinha & rsAgendamento("dt_recebimento") & vbTab

                    If IsNull(rsAgendamento("val_pago")) Then
                        sLinha = sLinha & vbTab
                    Else
                        sLinha = sLinha & TrocaVirgulaPorPonto(Val(rsAgendamento("val_pago"))) & vbTab
                    End If

                    sLinha = sLinha & rsAgendamento("desc_situacao")

                    GridAgendamentoVidasSeguradas.AddItem (sLinha)
                    '
                    rsAgendamento.MoveNext
                Wend
                '
                rsAgendamento.Close
            End If

            Set rsAgendamento = Nothing
        End If
    End If

    Exit Sub

Trata_Erro:
    Call TrataErroGeral("GridVidasSeguradas_Click", Me.name)

End Sub

Private Sub lblDtContratacao_Click()

End Sub

Private Sub MSFlexGrid1_Click(Index As Integer)
    Dim a As Long
    If entrada <> 0 Then
        entrada = 0
        If MSFlexGrid1(1).col = 0 And MSFlexGrid1(1).Row <> 0 Then
            If MSFlexGrid1(1).TextMatrix(MSFlexGrid1(1).Row, 0) = "X" Then
                MSFlexGrid1(1).TextMatrix(MSFlexGrid1(1).Row, 0) = ""
                Check1.Value = 0
            Else
                MSFlexGrid1(1).TextMatrix(MSFlexGrid1(1).Row, 0) = "X"
                a = MSFlexGrid1(1).Rows - 1
                Sim = 1
                Do While a >= 1 And Sim = 1
                    If MSFlexGrid1(1).TextMatrix(a, 0) = "" Then
                        Sim = 0
                    End If
                    a = a - 1
                Loop
                If Sim = 1 Then
                    Check1.Value = 1
                End If

            End If

        End If

    End If
    entrada = 1

End Sub

Private Sub MSFlexGrid1_DblClick(Index As Integer)
    Dim a As Long
    StatusBar1.SimpleText = "Aguarde..."
    If MSFlexGrid1(1).Row <> 0 And MSFlexGrid1(1).col <> 0 Then
        entrada = 0
        a = MSFlexGrid2(0).Rows - 2
        Do While a >= 1
            MSFlexGrid2(0).RemoveItem (a)
            a = a - 1
        Loop
        If MSFlexGrid2(0).Rows > 1 Then
            MSFlexGrid2(0).AddItem ""
            MSFlexGrid2(0).RemoveItem (1)
        End If
        'somente os ativos
        Check2(1).Value = 1

        Ler_Vidas_Seguradas

        Text1(1).Visible = True
        Text1(1).Text = MSFlexGrid1(1).TextMatrix(MSFlexGrid1(1).Row, 1) & " - " & MSFlexGrid1(1).TextMatrix(MSFlexGrid1(1).Row, 2)
        MSFlexGrid1(1).Visible = False
        MSFlexGrid1(1).Enabled = False
        MSFlexGrid2(0).Visible = True
        MSFlexGrid2(0).Enabled = True
        Label1(79).Visible = True
        Command1(0).Visible = True
        Command1(0).Enabled = True
        Text1(3).Visible = False
        Label1(80).Visible = False
        Frame5(2).Enabled = True
        Frame5(2).Visible = True
        Label1(81).Caption = "N� de Vidas"

        If (MSFlexGrid2(0).Rows > 2) And (MSFlexGrid2(0).TextMatrix(1, 1) = "") Then
            MSFlexGrid2(0).RemoveItem (1)
        End If
        If MSFlexGrid2(0).Rows = 2 And MSFlexGrid2(0).TextMatrix(1, 2) = "" Then
            Text1(2).Text = "0"
        Else
            Text1(2).Text = MSFlexGrid2(0).Rows - 1
        End If

        If Check2(1).Value = 0 Then
            Check2(1).Value = 1
        End If

        Check1.Visible = True
        cmdImprimir.Visible = True
        cmdImprimir.Enabled = True
        cboDestino.Enabled = True
        cboDestino.Visible = True
        cboDestino.ListIndex = 0
        Frame5(3).Visible = True
        Frame5(3).Enabled = True
        cmdPesquisar.Visible = True
        cmdPesquisar.Enabled = True
        Check2(1).SetFocus

    End If

    a = MSFlexGrid2(0).Rows - 1
    Sim = 1
    Do While a >= 1 And Sim = 1
        If MSFlexGrid2(0).TextMatrix(a, 0) = "" Then
            Sim = 0
        End If
        a = a - 1
    Loop

    If Sim = 1 Then
        Check1.Value = 1
    Else
        Check1.Value = 0
    End If
    StatusBar1.SimpleText = ""
End Sub

Private Sub MSFlexGrid2_Click(Index As Integer)
    Dim a As Long
    If entrada <> 0 Then
        entrada = 0
        If MSFlexGrid2(0).col = 0 And MSFlexGrid2(0).Row <> 0 Then
            If MSFlexGrid2(0).TextMatrix(MSFlexGrid2(0).Row, 0) = "X" Then
                MSFlexGrid2(0).TextMatrix(MSFlexGrid2(0).Row, 0) = ""
                Check1.Value = 0
            Else
                MSFlexGrid2(0).TextMatrix(MSFlexGrid2(0).Row, 0) = "X"
                a = MSFlexGrid2(0).Rows - 1
                Sim = 1
                Do While a >= 1 And Sim = 1
                    If MSFlexGrid2(0).TextMatrix(a, 0) = "" Then
                        Sim = 0
                    End If
                    a = a - 1
                Loop
                If Sim = 1 Then
                    Check1.Value = 1
                End If

            End If

        End If

    End If
    entrada = 1

End Sub

Private Sub SSTabProp_Click(PreviousTab As Integer)

    If SSTabProp.Tab <> 17 Then

        cmdImprimir.Visible = True
        cmdImprimir.Enabled = True

    Else
        If MSFlexGrid2(0).Visible = True Then
            cmdImprimir.Visible = True
            cmdImprimir.Enabled = True
        Else
            cmdImprimir.Visible = False
            cmdImprimir.Enabled = False
        End If

    End If
    entrada = 1

End Sub

Private Sub Limpa_Campos_Subgrupo()

    txtID.Text = ""
    txtNome_subgrupo.Text = ""
    txtTp_IS.Text = ""
    txtMult_Sal.Text = ""
    txtQuant_vidas.Text = ""
    '    txtTabua_id.Text = ""
    txtTabua_calculo.Text = ""
    txtDt_faturamento.Text = ""
    txtDt_Cobranca.Text = ""
    txtPeriodo_Pgto.Text = ""
    txtForma_Pgto.Text = ""
    txtAg_Debito.Text = ""
    txtCta_Corrente.Text = ""
    txtIdade_Min_Impl.Text = ""
    txtIdade_Max_Impl.Text = ""
    txtConjuge.Text = ""
    txtIdade_Min_Mov.Text = ""
    txtIdade_Max_Mov.Text = ""
    txtTexto_Padrao.Text = ""
    chkFatura_Auto.Value = 0
    chkIsento_IOF.Value = 0
    chkCartao_porposta.Value = 0
    chkTodos.Value = 0
    chkIdade.Value = 0
    chkCapital.Value = 0
    txtIdade.Text = ""
    MskCapital.Text = ""
    MskCapital.Mask = "15,2"
    frmTp_Cartao_proposta.Visible = False
    txtTpFaturamento.Text = ""
    txtTpCusteio.Text = ""
    txtTpVida.Text = ""
    txtDtFimVigenciaSBG.Text = ""
    txtTpFaturamento.Locked = True
    txtTpCusteio.Locked = True
    txtTpVida.Locked = True
    txtID.Locked = True
    txtNome_subgrupo.Locked = True
    txtTp_IS.Locked = True
    txtMult_Sal.Locked = True
    txtQuant_vidas.Locked = True
    '    txtTabua_id.Locked = True
    txtTabua_calculo.Locked = True
    txtDt_faturamento.Locked = True
    txtDt_Cobranca.Locked = True
    txtPeriodo_Pgto.Locked = True
    txtForma_Pgto.Locked = True
    txtAg_Debito.Locked = True
    txtCta_Corrente.Locked = True
    txtIdade_Min_Impl.Locked = True
    txtIdade_Max_Impl.Locked = True
    txtConjuge.Locked = True
    txtIdade_Min_Mov.Locked = True
    txtIdade_Max_Mov.Locked = True
    txtTexto_Padrao.Locked = True
    chkFatura_Auto.Enabled = False
    chkIsento_IOF.Enabled = False
    chkCartao_porposta.Enabled = False
    chkTodos.Enabled = False
    chkIdade.Enabled = False
    chkCapital.Enabled = False
    txtIdade.Locked = True
    MskCapital.Locked = True
    chkAtualizacaoIdade.Value = vbUnchecked
    txtDtFimVigenciaSBG.Locked = True
    'pmelo - inclusao da situacao na apolice_subgrupo_vida_web_tb
    TxtTipoFaturamento.Text = ""
    TxtTipoFaturamento.Locked = True

End Sub


Private Sub Ler_Vidas_Seguradas()

'******************************************************************************************
'*  Altera��es Feita por AFONSO NOGUEIRA - G&P - DEMANDA EVOLUTIVA: 176384 - 14/05/2008   *
'******************************************************************************************

'13/05/2008
'Altera��es para efetuar a buscar por solicita��es pendente
'para mostrar no grid "Vidas Seguradas"

'15/05/2008
'E altera��es para filtros da busca.

    Dim rc As rdoResultset
    Dim SQL As String
    Dim vVidas_Seguradas As Vida_Segurada
    'Dim cont As Integer
    Dim CONT As Long
    Dim linha As String
    Dim linha1 As String
    Dim Total_Ativas As Double
    Dim Total_Inativas As Double
    Dim mes As String
    Dim ano As String
    Dim Data As String
    Dim DataOk As Boolean
    Dim FaturaAutomatica As Boolean
    Dim bValido As Boolean

    Dim lBlnFlg As Boolean

    On Error GoTo Erro

    'limpar a variavel
    Texto2 = ""
    Texto = ""
    DataOk = False
    FaturaAutomatica = False
    bValido = False

    ano = Right(MEBVigencia.Text, 4)
    mes = Left(MEBVigencia.Text, 2)


    'Verifica se foi selecionado algum filtro
    'Inclus�o de Certificado e Altera��o IS
    If Check2(2).Value = 1 Or Check2(0).Value = 1 Then
        'Verifica se a data da Vigencia foi digitada
        If MEBVigencia.Text = "__/____" Or MEBVigencia.Text = "" Then
            MsgBox "Favor digitar a Vig�ncia da Fatura.", vbInformation + vbOKOnly, "Aviso"
            MEBVigencia.SetFocus
            Exit Sub
        Else
            If Not CInt(mes) >= 1 And CInt(mes) <= 12 Then
                MsgBox "Favor digitar uma data valida.", vbInformation + vbOKOnly, "Aviso"
                MEBVigencia.SetFocus
                Exit Sub
            Else
                'Verifica se o mes e ano s�o numeros
                If Not IsNumeric(ano) Or Not IsNumeric(mes) Then
                    MsgBox "Favor digitar a Vig�ncia da Fatura correta.", vbInformation + vbOKOnly, "Aviso"
                    MEBVigencia.SetFocus
                    Exit Sub
                End If
            End If
        End If
    End If

    If MEBVigencia.Text = "__/____" Then
        DataOk = False
    Else
        If IsNumeric(ano) And IsNumeric(mes) Then
            If (CInt(mes) >= 1 And CInt(mes) <= 12) Then
                DataOk = True
            Else
                MsgBox "Favor digitar a Vig�ncia da Fatura correta.", vbInformation + vbOKOnly, "Aviso"
                MEBVigencia.SetFocus
                Exit Sub
            End If
        Else
            MsgBox "Favor digitar a Vig�ncia da Fatura correta.", vbInformation + vbOKOnly, "Aviso"
            MEBVigencia.SetFocus
            Exit Sub
        End If
    End If

    Screen.MousePointer = vbHourglass

    txtVidasAtivas.Text = 0
    txtVidasInativas.Text = 0
    txtVidasTotal = 0

    SQL = ""
    'Criar a Tabela Temporaria
    SQL = SQL & "CREATE TABLE #SolicPendente (" & vbNewLine
    SQL = SQL & "   id_num  int IDENTITY(1,1)," & vbNewLine
    SQL = SQL & "   proposta_adesao_id numeric(9,0)," & vbNewLine
    SQL = SQL & "   nome varchar(60)," & vbNewLine
    SQL = SQL & "   cliente_id int," & vbNewLine
    SQL = SQL & "   cpf char(11)," & vbNewLine
    SQL = SQL & "   proposta_id numeric(9,0)," & vbNewLine
    SQL = SQL & "   prop_cliente_id int," & vbNewLine
    SQL = SQL & "   val_salario numeric(15,2)," & vbNewLine
    SQL = SQL & "   val_capital_segurado numeric(15,2)," & vbNewLine
    SQL = SQL & "   dt_inicio_vigencia_sbg smalldatetime," & vbNewLine
    SQL = SQL & "   dt_fim_vigencia_sbg smalldatetime," & vbNewLine
    SQL = SQL & "   situacao varchar(10)," & vbNewLine
    SQL = SQL & "   situacao_impressao VarChar(50)," & vbNewLine
    SQL = SQL & "   endosso_id int," & vbNewLine
    SQL = SQL & "   tp_pendencia_id INT," & vbNewLine
    SQL = SQL & "   motivo_pendencia VARCHAR(100)," & vbNewLine
    SQL = SQL & "   num_solicitacao int" & vbNewLine
    SQL = SQL & ")" & vbNewLine & vbNewLine

    SQL = SQL & "INSERT INTO #SolicPendente (proposta_adesao_id, nome, cliente_id, cpf, proposta_id, prop_cliente_id, val_salario, val_capital_segurado, dt_inicio_vigencia_sbg, dt_fim_vigencia_sbg, situacao, situacao_impressao, endosso_id, tp_pendencia_id, motivo_pendencia, num_solicitacao)" & vbNewLine
    'insere na tabela temporaria
    SQL = SQL & "SELECT" & vbNewLine
    SQL = SQL & "   sgp.proposta_adesao_id," & vbNewLine
    SQL = SQL & "   cl.nome," & vbNewLine
    SQL = SQL & "   cl.cliente_id," & vbNewLine
    SQL = SQL & "   pf.cpf," & vbNewLine
    SQL = SQL & "   sgp.proposta_id," & vbNewLine
    SQL = SQL & "   sgp.prop_cliente_id," & vbNewLine
    SQL = SQL & "   ISNULL(sgp.val_salario, 0) val_salario," & vbNewLine
    SQL = SQL & "   ISNULL(sgp.val_capital_segurado, 0) val_capital_segurado," & vbNewLine
    SQL = SQL & "   ISNULL(sgp.dt_inicio_vigencia_sbg, '') dt_inicio_vigencia_sbg," & vbNewLine
    SQL = SQL & "   ISNULL(sgp.dt_fim_vigencia_sbg, '') dt_fim_vigencia_sbg," & vbNewLine
    SQL = SQL & "   situacao = CASE" & vbNewLine
    SQL = SQL & "       WHEN sgp.dt_fim_vigencia_sbg Is Null" & vbNewLine
    SQL = SQL & "       THEN 'Ativa'" & vbNewLine
    SQL = SQL & "       ELSE 'Inativa'" & vbNewLine
    SQL = SQL & "       END," & vbNewLine
    SQL = SQL & "   NULL As situacao_impressao," & vbNewLine
    SQL = SQL & "   NULL AS endosso_id," & vbNewLine
    SQL = SQL & "   tp_pendencia_id," & vbNewLine
    'jpinheiro - 2009-10-15 15:07
    SQL = SQL & "   ISNULL(motivo_pendencia, '') motivo_pendencia," & vbNewLine
    SQL = SQL & "   NULL As num_solicitacao" & vbNewLine & vbNewLine
    'joins
    SQL = SQL & "FROM" & vbNewLine
    SQL = SQL & "seguro_vida_sub_grupo_tb sgp WITH (NOLOCK) " & vbNewLine
    SQL = SQL & "   INNER JOIN cliente_tb cl   WITH (NOLOCK) " & vbNewLine
    SQL = SQL & "       ON  sgp.prop_cliente_id = cl.cliente_id" & vbNewLine
    SQL = SQL & "   INNER JOIN pessoa_fisica_tb pf   WITH (NOLOCK) " & vbNewLine
    SQL = SQL & "       ON  sgp.prop_cliente_id = pf.pf_cliente_id" & vbNewLine & vbNewLine

    If (entrada = 1) Then
        Texto = cboSubGrupo(8).ItemData(cboSubGrupo(8).ListIndex)
        FaturaAutomatica = SubGrupos(cboSubGrupo(8).ItemData(cboSubGrupo(8).ListIndex)).FaturaAuto
    Else
        Texto = MSFlexGrid1(1).TextMatrix(MSFlexGrid1(1).Row, 1)
        FaturaAutomatica = SubGrupos(MSFlexGrid1(1).TextMatrix(MSFlexGrid1(1).Row, 1)).FaturaAuto
        If Check2(1).Value = 1 Then    ' somente os ativos
            Texto2 = "    AND sgp.dt_fim_vigencia_sbg is null "
            'INICIO DA ALTERA��O - DEMANDA: 10947642 - Rodrigo.Moura- Confitec Data:21/05/2011

        Else
            Texto2 = "    AND sgp.dt_fim_vigencia_sbg is not null "
            'FIM DA ALTERA��O - DEMANDA: 10947642 - Rodrigo.Moura- Confitec Data:21/05/2011
        End If
    End If

    SQL = SQL & " WHERE " & vbNewLine
    SQL = SQL & "   sgp.sub_grupo_id = " & Texto & vbNewLine
    SQL = SQL & Texto2 & vbNewLine  'somente os ativos
    SQL = SQL & "   AND sgp.apolice_id = " & vApoliceId & vbNewLine
    SQL = SQL & "   AND sgp.sucursal_seguradora_id = " & vSucursalId & vbNewLine
    SQL = SQL & "   AND sgp.seguradora_cod_susep = " & vSeguradoraId & vbNewLine
    SQL = SQL & "   AND sgp.ramo_id = " & vRamoId & vbNewLine
    SQL = SQL & "   AND sgp.dt_inicio_vigencia_sbg = "
    SQL = SQL & "       (Select TOP 1 sgp2.dt_inicio_vigencia_sbg " & vbNewLine
    SQL = SQL & "       from seguro_vida_sub_grupo_tb sgp2   WITH (NOLOCK)  " & vbNewLine
    SQL = SQL & "       WHERE   sgp2.apolice_id = sgp.apolice_id  " & vbNewLine
    SQL = SQL & "           " & Texto2 & vbNewLine
    SQL = SQL & "           AND sgp2.sucursal_seguradora_id = sgp.sucursal_seguradora_id " & vbNewLine
    SQL = SQL & "           AND sgp2.seguradora_cod_susep = sgp.seguradora_cod_susep " & vbNewLine
    SQL = SQL & "           AND sgp2.ramo_id = sgp.ramo_id " & vbNewLine
    SQL = SQL & "           AND sgp2.sub_grupo_id = sgp.sub_grupo_id " & vbNewLine
    SQL = SQL & "           AND sgp2.dt_inicio_vigencia_apol_sbg = sgp.dt_inicio_vigencia_apol_sbg  " & vbNewLine
    SQL = SQL & "           AND sgp2.proposta_id = sgp.proposta_id " & vbNewLine
    SQL = SQL & "           AND sgp2.prop_cliente_id = sgp.prop_cliente_id " & vbNewLine
    SQL = SQL & "           AND sgp2.tp_componente_id = sgp.tp_componente_id " & vbNewLine
    SQL = SQL & "           AND sgp2.seq_canc_endosso_seg = sgp.seq_canc_endosso_seg " & vbNewLine

    If IsDate(txtFimVigencia) Then         ''Apolice Fechada
        If FaturaAutomatica Then
            bValido = True
        End If
    End If

    If Not bValido Then
        If IsDate(txtDtEndosso.Text) Then
            'Inicio: FLOW 10764386 Autor:Rodrigo.Moura- ERRO ENDOSSO AP�LICE FECHADA PROD 150
            'SQL = SQL & "           AND sgp2.dt_inicio_vigencia_seg                  <= '" & Format(txtDtEndosso.Text, "yyyy-mm-dd") & "'" & vbNewLine
            'Fim: FLOW 10764386 Autor:Rodrigo.Moura- ERRO ENDOSSO AP�LICE FECHADA PROD 150
            SQL = SQL & "           AND sgp2.dt_inicio_vigencia_sbg                  <= '" & Format(txtDtEndosso.Text, "yyyy-mm-dd") & "'" & vbNewLine
            'jadao, mferreira, gcamara em 06/06/2011 este
            'FLOW 8884537 - ERRO ENDOSSO AP�LICE FECHADA PROD 150
            'SQL = SQL & "           AND sgp2.dt_entrada_sub_grupo                    <= '" & Format(txtDtEndosso.Text, "yyyy-mm-dd") & "'" & vbNewLine
            'SQL = SQL & "           AND isnull(sgp2.dt_saida_sub_grupo, '" & Format(txtDtEndosso.Text, "yyyy-mm-dd") & "') >= '" & Format(txtDtEndosso.Text, "yyyy-mm-dd") & "'" & vbNewLine
            SQL = SQL & "           AND sgp2.dt_inicio_vigencia_apol_sbg             <= '" & Format(txtDtEndosso.Text, "yyyy-mm-dd") & "'" & vbNewLine
        End If
    End If

    If (entrada <> 0) Then
        Texto = cboSubGrupo(8).ItemData(cboSubGrupo(8).ListIndex)    '& ") "
    Else
        Texto = (MSFlexGrid1(1).TextMatrix(MSFlexGrid1(1).Row, 1))    '& ") "
    End If

    Texto = Texto & " ORDER BY sgp2.dt_inicio_vigencia_sbg DESC) "    'Esta linha foi adicionada.
    SQL = SQL & "       AND sgp2.sub_grupo_id = " & Texto & vbNewLine & vbNewLine

    'filtro por data
    If DataOk = True Then
        SQL = SQL & "  --Filtro por data de vig�ncia" & vbNewLine
        SQL = SQL & "AND sgp.prop_cliente_id = (SELECT TOP 1 fs.prop_cliente_id" & vbNewLine
        SQL = SQL & "            FROM fatura_segurado_tb fs  WITH (NOLOCK) " & vbNewLine
        SQL = SQL & "        INNER JOIN fatura_tb fa  WITH (NOLOCK) " & vbNewLine
        SQL = SQL & "             ON  fa.fatura_id = fs.fatura_id" & vbNewLine
        SQL = SQL & "             AND fs.apolice_id = fa.apolice_id" & vbNewLine
        SQL = SQL & "         AND fs.ramo_id = fa.ramo_id" & vbNewLine
        SQL = SQL & "           Where" & vbNewLine
        SQL = SQL & "            fs.prop_cliente_id = sgp.prop_cliente_id" & vbNewLine
        SQL = SQL & "            AND fs.apolice_id = sgp.apolice_id" & vbNewLine
        SQL = SQL & "            AND fs.sucursal_seguradora_id = sgp.sucursal_seguradora_id" & vbNewLine
        SQL = SQL & "            AND fs.seguradora_cod_susep = sgp.seguradora_cod_susep" & vbNewLine
        SQL = SQL & "            AND fs.ramo_id = sgp.ramo_id" & vbNewLine
        SQL = SQL & "            AND YEAR(fa.dt_inicio_vigencia) <= " & CInt(ano) & vbNewLine
        SQL = SQL & "            AND MONTH(fa.dt_inicio_vigencia) <= " & CInt(mes) & vbNewLine
        SQL = SQL & "            AND YEAR(fa.dt_fim_vigencia) >= " & CInt(ano) & vbNewLine
        SQL = SQL & "            AND MONTH(fa.dt_fim_vigencia) >= " & CInt(mes) & vbNewLine

    End If

    'filtro por Inclus�o de Certificado , altera��o de IS
    If Check2(0).Value = 1 And Check2(2).Value = 0 Then    'somente o IS esta selecionado.
        SQL = SQL & "           --Filtro por opera��o A ou I" & vbNewLine
        SQL = SQL & "           AND fs.operacao = 'A' " & vbNewLine
    ElseIf Check2(2).Value = 1 And Check2(0).Value = 0 Then    'somente Inclus�o de Certificados selecionado
        SQL = SQL & "           --Filtro por opera��o A ou I" & vbNewLine
        SQL = SQL & "           AND fs.operacao = 'I' " & vbNewLine
    ElseIf Check2(2).Value = 1 And Check2(0).Value = 1 Then    'os dois est�o selecionado
        SQL = SQL & "           --Filtro por opera��o A ou I" & vbNewLine
        SQL = SQL & "           AND (fs.operacao = 'I' OR fs.operacao = 'A')" & vbNewLine
    End If

    If DataOk Then
        SQL = SQL & ")"
    End If

    'fim ************
    SQL = SQL & "" & vbNewLine
    SQL = SQL & " ORDER BY cl.Nome " & vbNewLine & vbNewLine


    'Inicio DA ALTERA��O - DEMANDA: 10947642 - Rodrigo.Moura- Confitec Data:21/05/2011
    ' fim da primeira etapa
    'verificando a solicita��o Pendentes
    '''''    SQL = SQL & "DECLARE @count int, @qtde int, @prop_cliente_id int, @situacao_impressao varchar(50), @proposta_id numeric(9,0), @endosso_id int, @num_solicitacao int" & vbNewLine & vbNewLine
    '''''    SQL = SQL & "SET @count = 1" & vbNewLine
    '''''    SQL = SQL & "SELECT @qtde = COUNT(id_num) FROM #SolicPendente" & vbNewLine & vbNewLine
    '''''    SQL = SQL & "   WHILE (@count <= @qtde)" & vbNewLine
    '''''    SQL = SQL & "       BEGIN" & vbNewLine
    '''''    SQL = SQL & "           SET @situacao_impressao = NULL" & vbNewLine
    '''''    SQL = SQL & "           SET @endosso_id = NULL" & vbNewLine
    '''''    SQL = SQL & "           SET @num_solicitacao = NULL" & vbNewLine & vbNewLine
    '''''    SQL = SQL & "       SELECT" & vbNewLine
    '''''    SQL = SQL & "           @prop_cliente_id = prop_cliente_id," & vbNewLine
    '''''    SQL = SQL & "           @proposta_id = proposta_id" & vbNewLine
    '''''    SQL = SQL & "           FROM #SolicPendente WHERE id_num = @count" & vbNewLine & vbNewLine
    '''''    SQL = SQL & "           SELECT" & vbNewLine
    '''''    SQL = SQL & "               @endosso_id = ei.endosso_id," & vbNewLine
    '''''    SQL = SQL & "               @num_solicitacao = ei.num_solicitacao," & vbNewLine
    '''''    SQL = SQL & "               @situacao_impressao = 'Pendente desde ' + cast(day(ei.dt_solicitacao) as varchar(2)) + '/' + cast(month(ei.dt_solicitacao) as varchar(2)) + '/' + cast(year(ei.dt_solicitacao) as varchar(4))" & vbNewLine
    '''''    SQL = SQL & "               FROM" & vbNewLine
    '''''    SQL = SQL & "               evento_seguros_db..impressao_vidas_tb iv  WITH (NOLOCK) " & vbNewLine
    '''''    SQL = SQL & "               INNER JOIN evento_seguros_db..evento_impressao_tb ei  WITH (NOLOCK) " & vbNewLine
    '''''    SQL = SQL & "                   ON iv.num_solicitacao = ei.num_solicitacao" & vbNewLine
    '''''    SQL = SQL & "           WHERE" & vbNewLine
    '''''    SQL = SQL & "           upper(ei.status) = 'L'" & vbNewLine
    '''''    SQL = SQL & "           AND ei.dt_impressao_grf IS NULL" & vbNewLine
    '''''    SQL = SQL & "           AND iv.prop_cliente_id = @prop_cliente_id" & vbNewLine
    '''''    SQL = SQL & "           AND ei.proposta_id = @proposta_id" & vbNewLine & vbNewLine
    '''''    SQL = SQL & "       IF Len(@situacao_impressao) <= 1" & vbNewLine
    '''''    SQL = SQL & "           BEGIN" & vbNewLine
    '''''    SQL = SQL & "               SET @situacao_impressao = NULL" & vbNewLine
    '''''    SQL = SQL & "               SET @endosso_id = NULL" & vbNewLine
    '''''    SQL = SQL & "               SET @num_solicitacao = NULL" & vbNewLine
    '''''    SQL = SQL & "           END" & vbNewLine & vbNewLine
    '''''    SQL = SQL & "       UPDATE #SolicPendente SET" & vbNewLine
    '''''    SQL = SQL & "           endosso_id = @endosso_id," & vbNewLine
    '''''    SQL = SQL & "           situacao_impressao = @situacao_impressao," & vbNewLine
    '''''    SQL = SQL & "           num_solicitacao = @num_solicitacao" & vbNewLine
    '''''    SQL = SQL & "       WHERE id_num = @count" & vbNewLine & vbNewLine
    '''''    SQL = SQL & "   SET @count = @count + 1" & vbNewLine & vbNewLine
    '''''    SQL = SQL & "END" & vbNewLine & vbNewLine

    'o novo select da tabela temporaria
    'j� trazendo todos os novos dados necessarios
    '''    SQL = SQL & "SELECT" & vbNewLine
    '''    SQL = SQL & "   proposta_adesao_id," & vbNewLine
    '''    SQL = SQL & "   nome," & vbNewLine
    '''    SQL = SQL & "   cliente_id," & vbNewLine
    '''    SQL = SQL & "   #SolicPendente.cpf," & vbNewLine
    '''    SQL = SQL & "   proposta_id," & vbNewLine
    '''    SQL = SQL & "   prop_cliente_id," & vbNewLine
    '''    SQL = SQL & "   val_salario," & vbNewLine
    '''    SQL = SQL & "   val_capital_segurado," & vbNewLine
    '''    SQL = SQL & "   dt_inicio_vigencia_sbg," & vbNewLine
    '''    SQL = SQL & "   dt_fim_vigencia_sbg," & vbNewLine
    '''    SQL = SQL & "   situacao," & vbNewLine
    '''    SQL = SQL & "   situacao_impressao," & vbNewLine
    '''    SQL = SQL & "   endosso_id," & vbNewLine
    '''    SQL = SQL & "   tp_pendencia_id," & vbNewLine
    '''    SQL = SQL & "   motivo_pendencia," & vbNewLine
    '''    SQL = SQL & "   num_solicitacao," & vbNewLine
    '''    SQL = SQL & "   dt_nascimento" & vbNewLine
    '''
    '''    SQL = SQL & "FROM #SolicPendente" & vbNewLine & vbNewLine
    '''
    '''    'lrocha 03/12/2008 - Altera��es no faturamento vida
    '''    SQL = SQL & " JOIN pessoa_fisica_tb " & vbNewLine
    '''    SQL = SQL & " ON pessoa_fisica_tb.pf_cliente_id = #SolicPendente.cliente_id " & vbNewLine
    '''
    '''   'Ralves - FLOW 428545 - Adicionada linha para ordenar por ordem alfabetica.
    '''    SQL = SQL & "ORDER BY nome" & vbNewLine & vbNewLine
    '''    SQL = SQL & "DROP TABLE #SolicPendente" & vbNewLine

    'FIM DA ALTERA��O - DEMANDA: 176384 - AFONSO - G&P

    'SQL = ""
    'SQL = SQL & "DROP TABLE #SolicPendente" & vbNewLine
    SQL = SQL & " SELECT distinct " & vbNewLine
    SQL = SQL & " #SolicPendente.proposta_adesao_id," & vbNewLine
    SQL = SQL & " #SolicPendente.nome," & vbNewLine
    SQL = SQL & " #SolicPendente.cliente_id," & vbNewLine
    SQL = SQL & "   #SolicPendente.cpf," & vbNewLine
    SQL = SQL & " #SolicPendente.proposta_id," & vbNewLine
    SQL = SQL & " #SolicPendente.prop_cliente_id," & vbNewLine
    SQL = SQL & "   val_salario," & vbNewLine
    SQL = SQL & "   val_capital_segurado," & vbNewLine
    SQL = SQL & "   dt_inicio_vigencia_sbg," & vbNewLine
    SQL = SQL & "   dt_fim_vigencia_sbg," & vbNewLine
    SQL = SQL & "   situacao," & vbNewLine
    SQL = SQL & " Case WHEN" & vbNewLine
    SQL = SQL & "      LEN('Pendente desde ' + cast(day(ei.dt_solicitacao) as varchar(2)) + '/' + cast(month(ei.dt_solicitacao) as varchar(2)) + '/' + cast(year(ei.dt_solicitacao) as varchar(4))) <= 1 THEN" & vbNewLine
    SQL = SQL & "      NULL" & vbNewLine
    SQL = SQL & " Else" & vbNewLine
    SQL = SQL & "      'Pendente desde ' + cast(day(ei.dt_solicitacao) as varchar(2)) + '/' + cast(month(ei.dt_solicitacao) as varchar(2)) + '/' + cast(year(ei.dt_solicitacao) as varchar(4))" & vbNewLine
    SQL = SQL & " END  situacao_impressao," & vbNewLine
    SQL = SQL & " ei.endosso_id," & vbNewLine
    SQL = SQL & "   tp_pendencia_id," & vbNewLine
    SQL = SQL & "   motivo_pendencia," & vbNewLine
    SQL = SQL & " ei.num_solicitacao," & vbNewLine
    SQL = SQL & "   dt_nascimento" & vbNewLine
    SQL = SQL & " FROM #SolicPendente" & vbNewLine
    SQL = SQL & "  LEFT JOIN evento_seguros_db.DBO.impressao_vidas_tb iv  WITH (NOLOCK)  ON iv.prop_cliente_id = #SolicPendente.prop_cliente_id" & vbNewLine
    SQL = SQL & "  LEFT JOIN evento_seguros_db.DBO.evento_impressao_tb ei  WITH (NOLOCK)  ON ei.proposta_id = #SolicPendente.proposta_id" & vbNewLine
    SQL = SQL & "                                                                AND iv.num_solicitacao = ei.num_solicitacao" & vbNewLine
    'INICIO DA ALTERA��O - DEMANDA: 12214222 - EDUARDO.OLIVEIRA - Confitec Data:02/09/2011
    SQL = SQL & "                                                                AND upper(ei.status) = 'L'" & vbNewLine
    SQL = SQL & "                                                                AND ei.dt_impressao_grf IS NULL" & vbNewLine
    'FIM DA ALTERA��O - DEMANDA: 12214222 - EDUARDO.OLIVEIRA - Confitec Data:02/09/2011
    SQL = SQL & "  INNER JOIN pessoa_fisica_tb  WITH (NOLOCK) " & vbNewLine
    SQL = SQL & " ON pessoa_fisica_tb.pf_cliente_id = #SolicPendente.cliente_id " & vbNewLine
    SQL = SQL & " ORDER BY nome " & vbNewLine

    'FIM DA ALTERA��O - DEMANDA: 10947642 - Rodrigo.Moura- Confitec Data:21/05/2011

    'Ralves - FLOW 428545 - Adicionada linha para ordenar por ordem alfabetica.
    SQL = SQL & "DROP TABLE #SolicPendente" & vbNewLine

    'FIM DA ALTERA��O - DEMANDA: 176384 - AFONSO - G&P

    GridVidasSeguradas.Rows = 1
    Total_Ativas = 0
    Total_Inativas = 0
    CONT = 0
    Set rc = rdocn.OpenResultset(SQL)

    Dim DataPendente As String

    lBlnFlg = False

    If Not rc.EOF Then
        While Not rc.EOF
            MSFlexGrid2(0).Redraw = False
            Set vVidas_Seguradas = New Vida_Segurada

            vVidas_Seguradas.Nome = rc!Nome
            vVidas_Seguradas.Salario = Format(Val(rc!val_salario), "0.00")
            vVidas_Seguradas.Capital = Format(Val(rc!val_capital_segurado), "0.00")
            vVidas_Seguradas.DtIniVigencia = IIf(Year(rc!dt_inicio_vigencia_sbg) = "1900", "", rc!dt_inicio_vigencia_sbg)
            vVidas_Seguradas.DtFimVigencia = IIf(Year(rc!dt_fim_vigencia_sbg) = "1900", "", rc!dt_fim_vigencia_sbg)
            vVidas_Seguradas.PropostaAdesao = " " & rc!proposta_adesao_id
            vVidas_Seguradas.CPF_CNPJ = IIf(IsNull(rc!CPF), "", rc!CPF)
            vVidas_Seguradas.Cliente_id = rc!Cliente_id

            'lrocha 03/12/2008 - Altera��es no faturamento vida
            vVidas_Seguradas.DtNascimento = rc!dt_nascimento
            '''''''''''''''''''''''''''''''''''''''''''''''''''

            vVidas_Seguradas.TipoPendencia = rc!tp_pendencia_id
            vVidas_Seguradas.MotivoPendencia = rc!Motivo_Pendencia

            If Not IsDate(vVidas_Seguradas.DtFimVigencia) Then
                Total_Ativas = Total_Ativas + 1
            Else
                Total_Inativas = Total_Inativas + 1
            End If

            CONT = CONT + 1
            linha = vVidas_Seguradas.PropostaAdesao
            linha = linha & vbTab & vVidas_Seguradas.Nome

            'lrocha 03/12/2008 - Altera��es no faturamento vida
            linha = linha & vbTab & vVidas_Seguradas.CPF_CNPJ
            linha = linha & vbTab & Format(vVidas_Seguradas.DtNascimento, "dd/mm/yyyy")
            '''''''''''''''''''''''''''''''''''''''''''''''''''

            'linha1 = vbTab & linha & vbTab & vVidas_Seguradas.CPF_CNPJ


            'verificar se a proposta � da Alian�a
            If vPropostaId = 6379517 Then
                'verifica se o usuario tem permiss�o de acesso
                Dim rPermissao As rdoResultset
                ' SQL = "select count(*) "'alterado 12/09/2013 projeto 17860335 - Melhorias no SEGBR MC_Amaral -confitec
                SQL = "select count(1) " _
                    & "from segab_db..usuario_tb " _
                    & "where unidade_id in(3, 4, 5, 6, 7, 8, 9, 21, 22, 186, 207, 206) " _
                    & "and cpf = '" & Usuario & "'"
                Set rPermissao = rdoCn1.OpenResultset(SQL)
                If rPermissao(0) = 0 Then
                    linha = linha & vbTab & "0.00"
                    linha = linha & vbTab & "0.00"
                Else
                    linha = linha & vbTab & Format(vVidas_Seguradas.Salario, "#####0.00")
                    linha = linha & vbTab & Format(vVidas_Seguradas.Capital, "#####0.00")
                End If
            Else
                linha = linha & vbTab & Format(vVidas_Seguradas.Salario, "#####0.00")
                linha = linha & vbTab & Format(vVidas_Seguradas.Capital, "#####0.00")

            End If

            linha = linha & vbTab & IIf(vVidas_Seguradas.DtIniVigencia = "", "", Format(vVidas_Seguradas.DtIniVigencia, "dd/mm/yyyy"))
            linha = linha & vbTab & IIf(vVidas_Seguradas.DtFimVigencia = "", "", Format(vVidas_Seguradas.DtFimVigencia, "dd/mm/yyyy"))
            linha = linha & vbTab & rc("situacao")

            'AFONSO - GEP - EVOLUTIVA - 176384 - 12/05/08
            If Not IsNull(rc!situacao_impressao) Then
                DataPendente = rc!situacao_impressao
                linha = linha & vbTab & DataPendente
            Else
                DataPendente = ""
                linha = linha & vbTab & DataPendente
            End If

            linha = linha & vbTab & vVidas_Seguradas.TipoPendencia
            linha = linha & vbTab & vVidas_Seguradas.MotivoPendencia

            '************************
            'linha1 = linha1 & vbTab & rc("situacao") & vbTab & vVidas_Seguradas.Cliente_id
            Vidas_Seguradas.Add vVidas_Seguradas

            'Objeto da grade n�o suporta mais de 350000 celulas
            If Me.GridVidasSeguradas.Rows < 29166 Then
                If entrada <> 0 Then
                    GridVidasSeguradas.AddItem linha
                Else
                    '========================================================================
                    'CAIO - GPTI - 712142 - Impress�o de Certificados - 30/01/2009
                    'Corre��o da ordem de valores passados para o GRID
                    '========================================================================
                    linha1 = vbTab
                    linha1 = linha1 & rc!proposta_id
                    linha1 = linha1 & vbTab & rc!Nome
                    linha1 = linha1 & vbTab & IIf(IsNull(rc!CPF), "", rc!CPF)
                    linha1 = linha1 & vbTab & rc!Situacao
                    linha1 = linha1 & vbTab & rc!Cliente_id
                    '========================================================================

                    MSFlexGrid2(0).AddItem linha1
                End If
            Else
                lBlnFlg = True
            End If

            GridVidasSeguradas.RowData(GridVidasSeguradas.Rows - 1) = CONT
            rc.MoveNext

        Wend
        MSFlexGrid2(0).Redraw = True
        txtVidasAtivas.Text = Total_Ativas
        txtVidasInativas.Text = Total_Inativas
        txtVidasTotal = Total_Ativas + Total_Inativas

    End If

    rc.Close
    Set rc = Nothing

    GridVidasSeguradas.Refresh
    Screen.MousePointer = vbArrow

    If lBlnFlg Then
        MsgBox "Atingiu a capacidade maxima do grid de 350.000 celulas!", vbInformation, Me.Caption

        MSFlexGrid2(0).Redraw = True

        Screen.MousePointer = vbDefault
    End If

    Exit Sub

Erro:
    'INICIO DA ALTERA��O - DEMANDA: 10947642 - Rodrigo.Moura- Confitec Data:21/05/2011
    If Err.Number = 7 Then
        MsgBox "Atingiu a capacidade maxima do grid de 350.000 celulas!", vbInformation, Me.Caption
        MSFlexGrid2(0).Redraw = True
        Screen.MousePointer = vbDefault
        Exit Sub
    End If
    'FIM DA ALTERA��O - DEMANDA: 10947642 - Rodrigo.Moura- Confitec Data:21/05/2011
    TrataErroGeral "Ler Vidas Seguradas"
    MsgBox "Rotina: Ler Vidas Seguradas"
    cmdSair_Click

End Sub

Private Sub Ler_ExtratoSeguro()

    On Error GoTo ErrorHandle
    Dim ExtratoSeguro As rdoResultset
    Dim rsProduto As rdoResultset
    Dim Prop_Endo As String
    Dim SQL As String
    With GridProposta
        'Rafael Oshiro 09/03/2006 - Alterando FixedRows para 1, para possibilitar o redimensionamento da coluna
        '.FixedRows = 0
        .FixedRows = 1
        'Demanda 546132 - Talitha - 07/10/2008 - Trecho Incluido - Inicio
        .ColWidth(0) = 0
        .ColWidth(1) = 1000
        .ColWidth(2) = 1000
        .ColWidth(3) = 1000
        .ColWidth(4) = 4000
        .ColWidth(5) = 9000
        .ColWidth(6) = 5000
        'Demanda 546132 - Talitha - 07/10/2008 - Trecho Incluido - FIM
        .Rows = 1
        .Row = 0
        .col = 1
        .CellAlignment = flexAlignCenterCenter
        .CellBackColor = .BackColorFixed
        .Text = "Tipo"

        .col = 2
        .CellAlignment = flexAlignCenterCenter
        .CellBackColor = .BackColorFixed
        .Text = "Dt Evento"

        'Demanda 546132 - Talitha - 07/10/2008 - Trecho Incluido - Inicio
        .col = 3
        .CellBackColor = .BackColorFixed
        .Text = "Inclus�o Evento"
        'Demanda 546132 - Talitha - 07/10/2008 - Trecho Incluido - Fim

        .col = 4    'Demanda 546132 - Talitha - 07/10/2008 - linha Alterada
        .CellBackColor = .BackColorFixed
        .Text = "Descri��o Evento"

        .col = 5    'Demanda 546132 - Talitha - 07/10/2008 - linha Alterada
        .CellBackColor = .BackColorFixed
        .Text = "Detalhes do Evento"
        'alucas 10/10/2005

        .col = 6    'Demanda 546132 - Talitha - 07/10/2008 - linha Alterada
        'rafael oshiro 24/02/2006 - aumento na largura da coluna
        .CellBackColor = .BackColorFixed
        .Text = "Funcionario"
    End With
    '
    SQL = "SELECT  'Evento' = c.nome, "
    SQL = SQL & "'Data' = convert(char(10),a.dt_evento,103), "
    SQL = SQL & "'Documento' = (SELECT nome FROM evento_seguros_db..documento_tb WITH(NOLOCK) WHERE documento_id = b.documento_id), "
    SQL = SQL & "'Parametro' = case when b.tp_evento_id = 1 then (select nome from evento_seguros_db..item_exibicao_tb WITH(NOLOCK) where grupo_exibicao_id = a.valor1 and item_exibicao_id = a.valor2) when b.tp_evento_id = 2 then (select 'Caixa ' + convert(varchar(20),caixa_custodia) + ' - ' + 'Lote ' + convert(varchar(20),lote_custodia) + ' - ' + 'Blip ' + convert(varchar(20),blip_custodia)  from evento_seguros_db..custodia_tb where proposta_id = a.proposta_id And documento_id = b.documento_id) End "
    SQL = SQL & "FROM    evento_seguros_db..evento_tb a   WITH (NOLOCK)  "
    SQL = SQL & "INNER   JOIN evento_seguros_db..evento_padrao_tb b   WITH (NOLOCK)   ON a.evento_padrao_id = b.evento_padrao_id "
    SQL = SQL & "INNER   JOIN evento_seguros_db..tp_evento_tb c   WITH (NOLOCK)   ON b.tp_evento_id = c.tp_evento_id "
    SQL = SQL & "WHERE   a.proposta_id = " & vPropostaId & " "
    SQL = SQL & "ORDER   BY data DESC"

    SQL = "EXEC evento_seguros_db..evento_sps " & vPropostaId
    Set ExtratoSeguro = rdocn.OpenResultset(SQL)

    Prop_Endo = ""
    While Not ExtratoSeguro.EOF
        With GridProposta
            .Rows = .Rows + 1
            .Row = .Rows - 1
            .col = 0
            .Text = Format(.Row, "000")
            If Prop_Endo <> ExtratoSeguro("Tipo") Then
                .col = 1
                .CellAlignment = flexAlignCenterCenter
                Prop_Endo = ExtratoSeguro("Tipo")
                .Text = Prop_Endo
            End If

            .col = 2
            .CellAlignment = flexAlignCenterCenter
			
            'INICIO CONFITEC - 27/05/2020 - IM01362516
            '.Text = ExtratoSeguro("data")
            .Text = IIf(IsNull(ExtratoSeguro("data")), "", ExtratoSeguro("data"))
            'FIM CONFITEC - 27/05/2020 - IM01362516
			
            'Demanda 546132 - Talitha - 07/10/2008 - Trecho Incluido - Inicio
            .col = 3
            .Text = IIf(IsNull(ExtratoSeguro("dt_inclusao_evento")), "", Format(ExtratoSeguro("dt_inclusao_evento"), "DD/MM/YYYY"))
            'Demanda 546132 - Talitha - 07/10/2008 - Trecho Incluido - Fim
            .col = 4    'Demanda 546132 - Talitha - 07/10/2008 - Linha alterada
            .CellAlignment = flexAlignLeftCenter
            .Text = ExtratoSeguro("evento")
            'alucas 10/10/2005
            .col = 5    'Demanda 546132 - Talitha - 07/10/2008 - Linha alterada
            .Text = Trim(ExtratoSeguro("documento")) & " | " & Trim(ExtratoSeguro("Parametro")) & " | " & IIf(IsNull(ExtratoSeguro("endosso_id")), "", ExtratoSeguro("endosso_id")) & " | " & Trim(ExtratoSeguro("desc_sega"))    'Demanda 546132 - Talitha - 07/10/2008 - Linha alt.
            .col = 6    'Demanda 546132 - Talitha - 07/10/2008 - Linha alterada
            .Text = IIf(IsNull(ExtratoSeguro("funcionario")), "", ExtratoSeguro("funcionario"))
        End With
        ExtratoSeguro.MoveNext
    Wend

    ExtratoSeguro.Close
    '
    Set ExtratoSeguro = Nothing

    'Odil Rincon Junior - 22/02/2010 - Demanda 3063240
    'Se o produto_id = 115, 150 e 850 mostra o grid de restitui��o, caso contr�rio n�o mostrar
    If IsNull(vProdutoId) Then

        SQL = "SELECT PRODUTO_ID FROM PROPOSTA_TB WITH(NOLOCK) WHERE PROPOSTA_ID = " & vPropostaId

        Set rsProduto = rdocn.OpenResultset(SQL)

        Dim iProduto_id As Integer

        iProduto_id = rsProduto(0)
    Else
        iProduto_id = vProdutoId
    End If

    Select Case iProduto_id

    Case 115, 150, 850, 1206
        frmRestituicao.Visible = True
        Call Carrega_Restituicao(vPropostaId)
    Case Else
        GridProposta.Height = 6700
        frmRestituicao.Visible = False
    End Select

    'fim demanda 3063240

    Exit Sub

ErrorHandle:
    MsgBox Err.Description
End Sub

Sub InicializarGridSubGrupos()

    Dim iCont As Integer

    'pmelo - inclusao da situacao na apolice_subgrupo_vida_web_tb
    GridSubGrupos.Cols = 35
    'GridSubGrupos.Cols = 33
    GridSubGrupos.Rows = 1
    'Alterado por Genesco Silva - Confitec SP - Flow 4532893 - em 29/06/2011
    'Inicio da altera��o - Alterado por Thiago Mazzero - Confitec SP em 12/01/2012 - INC000003218986
    'titulos
    GridSubGrupos.TextMatrix(0, GRD_SUB_ID) = "Id"
    GridSubGrupos.TextMatrix(0, GRD_SUB_NOME) = "Nome"
    GridSubGrupos.TextMatrix(0, GRD_SUB_CNPJ) = "CPF/CNPJ"
    GridSubGrupos.TextMatrix(0, GRD_SUB_DTINICIOSUB) = "Dt. Ini SubGrupo"
    GridSubGrupos.TextMatrix(0, GRD_SUB_TPIS) = "Tp. IS"
    GridSubGrupos.TextMatrix(0, GRD_SUB_MULTSAL) = "Mult. Sal."
    GridSubGrupos.TextMatrix(0, GRD_SUB_QTDVIDAS) = "Qtd. Vidas"
    GridSubGrupos.TextMatrix(0, GRD_SUB_TABUAID) = "Id T�bua"
    GridSubGrupos.TextMatrix(0, GRD_SUB_TABUA) = "T�bua"
    GridSubGrupos.TextMatrix(0, GRD_SUB_FATAUTO) = "Fatura Auto"
    GridSubGrupos.TextMatrix(0, GRD_SUB_TEXTOPADRAO) = "Texto Padr�o"
    GridSubGrupos.TextMatrix(0, GRD_SUB_DIAFAT) = "Dia Fatura"
    GridSubGrupos.TextMatrix(0, GRD_SUB_DIACOB) = "Dia Cobran�a"
    GridSubGrupos.TextMatrix(0, GRD_SUB_PERPGTO) = "Per�odo Pgto"
    GridSubGrupos.TextMatrix(0, GRD_SUB_FORMAPGTO) = "Forma pgto"
    GridSubGrupos.TextMatrix(0, GRD_SUB_AGDEBITO) = "Ag. D�bito"
    GridSubGrupos.TextMatrix(0, GRD_SUB_CONTA) = "Conta Corrente"
    GridSubGrupos.TextMatrix(0, GRD_SUB_CONJUGE) = "C�njuge"
    GridSubGrupos.TextMatrix(0, GRD_SUB_ISENTOIOF) = "Isento IOF"
    GridSubGrupos.TextMatrix(0, GRD_SUB_IDADEMINIMPL) = "Idade M�n Impl"
    GridSubGrupos.TextMatrix(0, GRD_SUB_IDADEMAXIMPL) = "Idade M�x Impl"
    GridSubGrupos.TextMatrix(0, GRD_SUB_IDADEMINMOV) = "Idade M�n Mov"
    GridSubGrupos.TextMatrix(0, GRD_SUB_IDADEMAXMOV) = "Idade M�x Mov"
    GridSubGrupos.TextMatrix(0, GRD_SUB_CARTAOPROPOSTA) = "Cart�o Proposta"
    GridSubGrupos.TextMatrix(0, GRD_SUB_CRITICATODOS) = "Critica Todos"
    GridSubGrupos.TextMatrix(0, GRD_SUB_CRITICACAPITAL) = "Critica Capital"
    GridSubGrupos.TextMatrix(0, GRD_SUB_CRITICAIDADE) = "Critica Idade"
    GridSubGrupos.TextMatrix(0, GRD_SUB_MAXIDADE) = "M�x Idade"
    GridSubGrupos.TextMatrix(0, GRD_SUB_MAXCAPITAL) = "M�x Capital"
    GridSubGrupos.TextMatrix(0, GRD_SUB_TPFATURA) = "Tp. Fatura"
    GridSubGrupos.TextMatrix(0, GRD_SUB_TPVIDA) = "Tp. Vida"
    GridSubGrupos.TextMatrix(0, GRD_SUB_TPCUSTEIO) = "Tp. Custeio"
    GridSubGrupos.TextMatrix(0, GRD_SUB_REENQUAD) = "Reenquadramento"
    GridSubGrupos.TextMatrix(0, GRD_SUB_EXCLUSAO) = "Dt. Fim de Vig�ncia"
    'pmelo - inclusao da situacao na apolice_subgrupo_vida_web_tb
    GridSubGrupos.TextMatrix(0, GRD_SUB_SITUACAO) = "Situa��o"

    'Fim da altera��o - Alterado por Thiago Mazzero - Confitec SP em 12/01/2012 - INC000003218986

    For iCont = 0 To GridSubGrupos.Cols - 1
        GridSubGrupos.ColWidth(iCont) = 1000
    Next

    GridSubGrupos.Rows = 2
    GridSubGrupos.FixedRows = 1
    GridSubGrupos.Rows = 1

End Sub

Private Sub SSTabProp_GotFocus()
    If SSTabProp.Tab = 17 Then
        If MSFlexGrid1(1).TextMatrix(1, 1) = "" And MSFlexGrid1(1).Rows > 2 Then
            MSFlexGrid1(1).RemoveItem (1)
        End If
        Text1(2).Text = (MSFlexGrid1(1).Rows - 1)
        Label1(81).Caption = "N� de Subgrupos"
        visualizar.Visible = False
        visualizar.Enabled = False
    Else
        If MSFlexGrid1(1).TextMatrix(1, 1) = "" And MSFlexGrid1(1).Rows > 2 Then
            MSFlexGrid1(1).RemoveItem (1)
        End If
        Text1(2).Text = (MSFlexGrid1(1).Rows - 1)
        Label1(81).Caption = "N� de Subgrupos"
        visualizar.Visible = False
        visualizar.Enabled = False
    End If
End Sub

Private Sub Imprimir_apolice_3(ComBeneficiarios As Boolean)

    Dim pnt As Integer
    Dim sAux As String, sAux2 As String
    Dim sSQL As String
    Dim vAux As Variant
    Dim dt_documento As Date

    Dim S_grupo As SubGrupo
    Dim vCobertura As Cobertura
    Dim vCorretor As Corretor
    Dim vAdm As Administrador
    Dim vComponente As Componente
    Dim vAssistencias As Assistencia
    Dim vEstip As estipulante

    Dim PercCorretagem As Double
    Dim vTaxa As Double
    Dim c_premio_liquido As Currency, c_premio As Currency, c_premio_tarifa As Currency

    Dim rc As rdoResultset
    Dim last_bnc_id As Integer, last_bnc_ds As String
    Dim rsimpressao As rdoResultset
    '--------
    Dim s_proposta_id As String, s_usuario As String
    '-----------
    Dim s_subgrupo As String, s_dados_nome As String, s_dados_cnpj_cpf As String
    Dim s_dados_ramo_ativ As String, s_dados_endereco As String, s_dados_numero As String
    Dim s_dados_bairro As String, s_dados_cidade As String, s_dados_cep As String
    Dim s_dados_uf As String, s_dados_email As String, s_dados_fone As String
    Dim s_dados_admin As String, s_dados_cpf As String, s_seguravel_perfil As String
    Dim s_seguravel_adesao As String, s_seguravel_min_1a As String, s_seguravel_max_1a As String
    Dim s_seguravel_transf As String, s_seguravel_min_demais As String, s_seguravel_max_demais As String
    Dim s_taxa_taxa As String, s_taxa_Cust_seguro As String, s_taxa_estipulante As String
    Dim s_taxa_segurado As String, s_taxa_pgto_premio As String, s_cobranca_corte As String
    Dim s_cobranca_forma_pgto As String, s_cobranca_venc_demais As String, s_cobranca_periodo As String
    Dim s_cobranca_premio_liquido_1a As String, s_cobranca_IOF_1a As String, s_cobranca_premio_total_1a As String
    Dim s_cobranca_correntista As String, s_cobranca_bnc_codigo As String, s_cobranca_bnc_nome As String
    Dim s_cobranca_bnc_agencia_dv As String, s_cobranca_bnc_conta_dv As String, s_excedente_tecnico As String
    Dim s_comissao_percentual As String
    '-----------
    Dim s_garantia_tipoIS As String, s_garantia_label_fator As String, s_garantia_valor_fator As String
    Dim s_garantia_max_IS As String, s_garantia_min_IS As String, s_garantia_desc As String
    Dim s_garantia_eleg As String, s_garantia_capital As String, s_garantia_carencia As String
    Dim s_garantia_franquia As String, s_garantia_taxa As String, s_garantia_assistencias As String
    Dim s_garantia_limites As String
    '-----------
    Dim s_cosseguro_razao As String, s_cosseguro_cnpj As String, s_cosseguro_cod_susep As String
    Dim s_cosseguro_participacao As String
    '-----------
    Dim s_corretor_nome As String, s_corretor_cod_susep As String, s_corretor_distribuicao As String
    '-
    Dim Processo_SUSEP As String, dt_nascimento_ti As String, Texto1 As String
    Dim Texto2 As String, texto3 As String, texto4 As String
    Dim texto_benef_1 As String, texto_benef_2 As String, texto_benef_3 As String
    Dim texto_benef_4 As String, texto_benef_5 As String, Titular As String
    Dim pagamento_dia As String, periodo_pagamento As String, custeio As String
    Dim PremioBruto As String, PremioTarifaTotal As String, Cpf_Titular As String

    Dim Count As Integer, TotalImpresoTela As Integer, cod_clit As Integer
    Dim id_sub As Integer

    Dim prop_cliente_id As Long
    Dim certificado_id As Variant

    NumRows2 = MSFlexGrid2(0).Rows
    NumRows = 1
    lugar = InStr(1, Text1(1).Text, " ", vbTextCompare)
    subgrupo_id = Mid(Text1(1).Text, 1, lugar - 1)
    num_solicitacao = 0
    Count = 0
    TotalImpresoTela = 0

    StatusBar1.SimpleText = "Imprimindo..."
    MousePointer = vbHourglass

    On Error GoTo jpErro
    Screen.MousePointer = vbHourglass
    DoEvents

    'Validar dados necess�rios
    If IsDate(txtDtEndosso.Text) Then
        dt_documento = CDate(txtDtEndosso.Text)

    ElseIf IsDate(txtDtEmissao.Text) Then
        dt_documento = CDate(txtDtEmissao.Text)
    Else
        Screen.MousePointer = vbNormal
        DoEvents
        MsgBox "Data de emiss�o da ap�lice inv�lida!"
        GoTo jpFim

    End If

    Do While NumRows < NumRows2
        If MSFlexGrid2(0).TextMatrix(NumRows, 0) = "X" And MSFlexGrid2(0).TextMatrix(NumRows, 3) <> "" And MSFlexGrid2(0).TextMatrix(NumRows, 5) <> "" And MSFlexGrid2(0).TextMatrix(NumRows, 5) <> "" Then
            Count = Count + 1
            s_proposta_id = vPropostaId
            s_usuario = "'" & Usuario & "'"
            last_bnc_id = -1

            'limpar dados tabelas tempor�rias
            sSQL = "EXEC SEGS12948_SPI " & s_proposta_id & "," & s_usuario & ",'s'"
            Set rs = rdocn.OpenResultset(sSQL)
            rs.Close

            SQL = "select nome, dt_nascimento, cpf  "
            SQL = SQL & " from   seguros_db.dbo.cliente_tb WITH (NOLOCK) "
            SQL = SQL & " join seguros_db.dbo.pessoa_fisica_tb WITH (NOLOCK) "
            SQL = SQL & "   on seguros_db.dbo.cliente_tb.cliente_id = seguros_db..pessoa_fisica_tb.pf_cliente_id"
            SQL = SQL & " where seguros_db..cliente_tb.cliente_Id = " & MSFlexGrid2(0).TextMatrix(NumRows, 5)
            Set rc = rdocn.OpenResultset(SQL)

            If Not rc.EOF Then
                dt_nascimento_ti = (rc!dt_nascimento)
            End If
            rc.Close

            SQL = "exec rel_certif_sps " & vApoliceId & ", " & vSucursalId & ", " & vSeguradoraId & ", " & vRamoId & "," & subgrupo_id & ", " & prop_cliente_id & ", '" & Usuario & "', null"
            Set rs = rdocn.OpenResultset(SQL)

            certificado_id = Format(rs(0).Value, "000000000")
            rs.Close
            Set rs = Nothing

            Titular = MSFlexGrid2(0).TextMatrix(NumRows, 2)
            Cpf_Titular = MSFlexGrid2(0).TextMatrix(NumRows, 3)
            id_sub = MSFlexGrid1(1).TextMatrix(MSFlexGrid1(1).Row, 1)
            For Each S_grupo In SubGrupos
                If S_grupo.Id = MSFlexGrid1(1).TextMatrix(MSFlexGrid1(1).Row, 1) Then
                    c_premio_liquido = 0
                    c_premio = 0
                    c_premio_tarifa = 0
                    With S_grupo    '--inclui estipulante
                        s_subgrupo = MSFlexGrid1(1).TextMatrix(MSFlexGrid1(1).Row, 1)
                        s_dados_nome = "'" & .Nome & "'"
                        s_dados_cnpj_cpf = "'" & .Cnpj & "'"
                        s_dados_ramo_ativ = "''"

                        Dim s_grupo_ramo As String, s_modalidade As String
                        sSQL = "SELECT R.sub_grupo_id, E.endereco, E.bairro, E.municipio_sise, E.estado, E.cep, C.ddd_1, c.telefone_1, c.ddd_2, c.telefone_2, c.e_mail, X.grupo_ramo, X.nome as Nome_ramo " & vbCrLf _
                             & "FROM endereco_cliente_tb        E WITH (NOLOCK) " & vbCrLf _
                             & "JOIN representacao_sub_grupo_tb R WITH (NOLOCK) ON E.cliente_id = R.est_cliente_id " & vbCrLf _
                             & "JOIN cliente_tb                 C WITH (NOLOCK) ON E.cliente_id = C.cliente_id " & vbCrLf _
                             & "JOIN ramo_tb                    X WITH (NOLOCK) ON R.ramo_id = X.ramo_id " & vbCrLf _
                             & "WHERE R.apolice_id = " & Val(txtApolice.Text) & vbCrLf _
                             & "  AND R.ramo_id = " & vRamoId & vbCrLf _
                             & "  AND R.sucursal_seguradora_id = " & vSucursalId & vbCrLf _
                             & "  AND R.seguradora_cod_susep = " & vSeguradoraId & vbCrLf _
                             & "  AND R.sub_grupo_id = " & Val(.Id)

                        Set rc = rdocn.OpenResultset(sSQL)
                        s_grupo_ramo = vRamoId
                        s_modalidade = ""

                        If Not rc.EOF Then
                            s_grupo_ramo = Format(rc!grupo_ramo, "00") & Format(vRamoId, "00")
                            s_modalidade = Trim(rc!Nome_ramo & "")
                            s_dados_endereco = "'" & Trim(rc!Endereco & "") & "'"
                            s_dados_numero = "''"
                            s_dados_bairro = "'" & Trim(rc!Bairro & "") & "'"
                            s_dados_cidade = "'" & Trim(rc!municipio_sise & "") & "'"
                            s_dados_cep = "'" & Trim(rc!CEP & "") & "'"
                            s_dados_uf = "'" & Trim(rc!Estado & "") & "'"
                            s_dados_email = "'" & Trim(rc!e_mail & "") & "'"

                            sAux = Trim(rc!ddd_1 & "")
                            sAux2 = Trim(rc!telefone_1 & "")
                            If sAux2 <> "" Then
                                sAux = IIf(sAux = "", "", sAux & " ") & sAux2
                            Else
                                sAux = Trim(rc!ddd_2 & "")
                                sAux2 = Trim(rc!telefone_2 & "")
                                sAux = IIf(sAux = "", "", sAux & " ") & sAux2
                            End If
                            s_dados_fone = "'" & Trim(sAux) & "'"
                        Else
                            s_dados_endereco = "''"
                            s_dados_numero = "''"
                            s_dados_bairro = "''"
                            s_dados_cidade = "''"
                            s_dados_cep = "''"
                            s_dados_uf = "''"
                            s_dados_email = "''"
                            s_dados_fone = "''"
                        End If
                        rc.Close
                        Set rc = Nothing

                        s_dados_admin = "'" & "" & "'"                  '<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
                        s_dados_cpf = "'" & "" & "'"                    '<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
                        s_seguravel_perfil = "'" & "" & "'"             '<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
                        s_seguravel_adesao = "'" & "" & "'"             '<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
                        s_seguravel_min_1a = "'" & .LimMinIdadeImpl & "'"
                        s_seguravel_max_1a = "'" & .LimMaxIdadeImpl & "'"
                        s_seguravel_transf = "'" & "" & "'"             '<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
                        s_seguravel_min_demais = "'" & .LimMinIdadeMov & "'"
                        s_seguravel_max_demais = "'" & .LimMaxIdadeMov & "'"
                        'Campos de Taxa M�dia n�o ser�o utilizados no momento
                        s_taxa_taxa = "''"
                        s_taxa_Cust_seguro = "''"
                        s_taxa_estipulante = "''"
                        s_taxa_segurado = "''"
                        s_taxa_pgto_premio = "''"
                        s_cobranca_corte = "'" & .DiaFaturamento & "'"
                        s_cobranca_forma_pgto = "'" & .FormaPgto & "'"
                        s_cobranca_venc_demais = "'" & .DiaCobranca & "'"
                        s_cobranca_periodo = "'" & .PeriodoPgto & "'"
                        s_cobranca_correntista = "''"         '<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
                        s_cobranca_bnc_codigo = "'" & .IdBanco & "'"
                        s_cobranca_bnc_agencia_dv = "'" & .IdAgencia & "'"
                        s_cobranca_bnc_conta_dv = "'" & .CCorrente & "'"

                        If last_bnc_id <> .IdAgencia Then
                            last_bnc_ds = ""
                            last_bnc_id = .IdAgencia

                            sSQL = "SELECT nome FROM seguros_db.dbo.banco_tb WHERE banco_id = " & .IdBanco
                            Set rc = rdocn.OpenResultset(sSQL)
                            If Not rc.EOF Then
                                last_bnc_ds = Trim(rc!Nome & "")
                            End If
                            rc.Close
                            Set rc = Nothing
                        End If
                        s_cobranca_bnc_nome = "'" & last_bnc_ds & "'"
                        s_excedente_tecnico = "'" & "" & "'"            '<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
                        s_comissao_percentual = ""                      '<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<


                        'Dim vEstip As Estipulante
                        For Each vEstip In S_grupo.Estipulantes
                            For Each vAdm In vEstip.Administradores
                                s_comissao_percentual = "'" & vAdm.PercProLabore & "'"
                                Exit For
                            Next
                            If s_comissao_percentual <> "" Then
                                Exit For
                            End If
                        Next
                    End With
                    If s_comissao_percentual = "" Then
                        s_comissao_percentual = "NULL"
                    End If


                    pagamento_dia = S_grupo.DiaFaturamento
                    periodo_pagamento = S_grupo.PeriodoPgto
                    Select Case S_grupo.tp_custeio
                    Case "C": custeio = "Contribut�rio."
                    Case "N": custeio = "N�o Contribut�rio."
                    End Select

                    PremioTarifaTotal = S_grupo.PremioTarifaTotal
                    PremioBruto = S_grupo.PremioBruto

                    'BLOCO DE GARANTIAS
                    Select Case UCase(S_grupo.TipoIS)
                    Case "C": sAux = "Capital Informado"
                    Case "F": sAux = "Capital Fixo"
                    Case "S": sAux = "M�ltiplo Salarial"
                    Case "G": sAux = "Capital Global"
                    Case "P": sAux = "Capital por Planos"
                    Case "E": sAux = "Capital por Faixa Et�ria"
                    Case Else: sAux = "N�o Informado"
                    End Select
                    s_garantia_tipoIS = "'" & sAux & "'"
                    s_garantia_valor_fator = "'" & S_grupo.QtdeSal & "'"

                    If (S_grupo.TipoIS) = "S" Then
                        Texto1 = "Os capitais segurados e os pr�mios correspondentes ser�o atualizados monetariamente segundo a varia��o do"
                        Texto2 = "reajuste salarial que dever� ser informado previamente � Seguradora pelo Estipulante.  "
                    Else
                        Texto1 = "Os Capitais Segurados e os pr�mios ser�o atualizados monetariamente, de acordo com o �ndice Os Capitais "
                        Texto2 = "Segurados e os pr�mios ser�o atualizados monetariamente anualmente com base na varia��o do IGP-M/FGV - "
                        texto3 = "�ndice geral de pre�os de mercado da Funda��o Get�lio Vargas, acumulado dos �ltimos 12 (doze) meses que "
                        texto4 = "antecedem o m�s anterior ao anivers�rio do seguro."
                    End If

                    s_garantia_label_fator = "'M�lt. Salarial'"   '
                    s_garantia_max_IS = "'" & "" & "'"            '<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
                    s_garantia_min_IS = "'" & "" & "'"            '<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

                    sAux = ""

                    For Each Assistencias In S_grupo.Assistencias
                        sAux = sAux & Trim(vAssistencias.DescricaoAssistencia) & vbCrLf
                    Next
                    If Right(sAux, 2) = vbCrLf Then sAux = Left(sAux, Len(sAux) - 2)
                    s_garantia_assistencias = "'" & sAux & "'"

                    If s_garantia_limites <> "" Then
                        s_garantia_limites = "'De acordo com regulamento(s) integrante (s) das condi��es contratuais da ap�lice.'"
                    Else
                        s_garantia_limites = "''"
                    End If
                    PercCorretagem = 0

                    For Each vCorretor In S_grupo.Corretores
                        PercCorretagem = PercCorretagem + Val(vCorretor.PercCorretagem)
                    Next

                    If Not (S_grupo.Estipulantes Is Nothing) Then
                        For Each vEstip In S_grupo.Estipulantes
                            For Each vAdm In vEstip.Administradores
                                PercCorretagem = PercCorretagem + Val(vAdm.PercProLabore)
                            Next
                        Next
                    End If

                    Set Coberturas = S_grupo.Coberturas

                    For Each vCobertura In Coberturas
                        For Each vComponente In S_grupo.Componentes
                            s_garantia_eleg = "''"
                            If vCobertura.CodObjSegurado = vComponente.Codigo Then
                                s_garantia_eleg = "'" & vComponente.Nome & "'"
                                Exit For
                            End If
                        Next
                        s_garantia_desc = "'" & Trim(vCobertura.Descricao) & "'"
                        s_garantia_capital = "'" & Trim(vCobertura.ValIS) & "'"
                        s_garantia_carencia = "'" & IIf(vCobertura.Carencia > 0, "H�", "N�O H�") & "'"
                        s_garantia_franquia = "'" & IIf(vCobertura.ValFranquia > 0, "H�", "N�O H�") & "'"

                        If vCobertura.ValTaxa = "" Then
                            s_garantia_taxa = "'0,00'"
                        Else
                            If ConfiguracaoBrasil Then
                                vTaxa = vCobertura.ValTaxa
                            Else
                                vTaxa = MudaVirgulaParaPonto(vCobertura.ValTaxa)
                            End If
                            vTaxa = vTaxa * (100 - PercCorretagem)

                            s_garantia_taxa = "'" & Format(vTaxa, "0.0000") & "'"
                        End If

                        If ConfiguracaoBrasil Then
                            c_premio_liquido = c_premio_liquido + Val(vCobertura.ValPremioLiq)
                            c_premio = c_premio + Val(vCobertura.ValPremio)
                            c_premio_tarifa = c_premio_tarifa + Val(vCobertura.ValPremioTarifa)

                        Else
                            c_premio_liquido = c_premio_liquido + Val(MudaVirgulaParaPonto(vCobertura.ValPremioLiq))
                            c_premio = c_premio + Val(MudaVirgulaParaPonto(vCobertura.ValPremio))
                            c_premio_tarifa = c_premio_tarifa + Val(MudaVirgulaParaPonto(vCobertura.ValPremioTarifa))
                        End If

                        sSQL = "EXEC SEGS12948_SPI " & s_proposta_id & "," & s_usuario & ",'n'," & s_subgrupo
                        'bloco rpt_apolice_circular_491_tb
                        sSQL = sSQL & ",NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL" _
                             & ",NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL" _
                             & ",NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL"
                        'bloco rpt_apolice_garantia_circular_491_tb
                        sSQL = sSQL & "," & s_garantia_tipoIS & "," & s_garantia_label_fator & "," & s_garantia_valor_fator _
                             & "," & s_garantia_max_IS & "," & s_garantia_min_IS & "," & s_garantia_desc _
                             & "," & s_garantia_eleg & "," & s_garantia_capital & "," & s_garantia_carencia _
                             & "," & s_garantia_franquia & "," & s_garantia_taxa & "," & s_garantia_assistencias _
                             & "," & s_garantia_limites
                        'bloco rpt_apolice_cosseguro_circular_491_tb
                        'sSql = sSql & ",NULL,NULL,NULL,NULL"
                        'bloco rpt_apolice_corretor_circular_491_tb
                        'sSql = sSql & ",NULL,NULL,NULL"

                        Set rs = rdocn.OpenResultset(sSQL)
                        rs.Close

                    Next

                    'Distribiu��o de cosseguro
                    s_cosseguro_razao = "'" & "" & "'"          '<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
                    s_cosseguro_cnpj = "'" & "" & "'"           '<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
                    s_cosseguro_cod_susep = "'" & "" & "'"      '<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
                    s_cosseguro_participacao = "'" & "" & "'"   '<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

                    If ComBeneficiarios = True Then
                        If vProdutoId = 222 Then
                            texto_benef_1 = "Os Capitais Segurados e os pr�mios ser�o atualizados monetariamente, de acordo com o �ndice Os Capitais "
                            texto_benef_2 = "Segurados e os pr�mios  O primeiro Benefici�rio do seguro ser� o Estipulante e/ou Subestipulante, que receber� "
                            texto_benef_3 = "o valor correspondente ao saldo devedor ou valor do financiamento na data do evento coberto, de acordo "
                            texto_benef_4 = " com o Tipo de Capital contratado (Valor do Financiamento ou Saldo Devedor).  Eventual diferen�a entre o "
                            texto_benef_5 = " saldo devedor apurado e o Capital Segurado Contratado ser� paga ao(s) benefici�rio (s) indicado (s) pelo segurado. "
                        Else
                            texto_benef_1 = "Os benefici�rios ser�o aqueles indicados pelo Segurado. Na falta de indica��o de benefici�rio, aplicar-se-� o "
                            texto_benef_2 = "disposto no art. 792 do C�digo Civil Brasileiro. Em caso de morte de c�njuge e/ou filhos, quando contratada "
                            texto_benef_3 = "cobertura espec�fica, o benefici�rio ser� sempre o Segurado Titular."
                            texto_benef_4 = "O somat�rio dos percentuais de participa��o ser� de no m�ximo 100% (cem por cento)"
                            texto_benef_5 = ""
                        End If

                    End If


                    'Dados do Corretor
                    'Dim vCorretor As Corretor
                    For Each vCorretor In S_grupo.Corretores
                        s_corretor_nome = "'" & Trim(vCorretor.Nome) & "'"
                        s_corretor_cod_susep = "'" & Trim(vCorretor.Codigo_susep) & "'"
                        s_corretor_distribuicao = "'" & Format(vCorretor.PercParticipacao, "0.00") & "%'"

                        sSQL = "EXEC SEGS12948_SPI " & s_proposta_id & "," & s_usuario & ",'n'," & s_subgrupo
                        'bloco rpt_apolice_corretor_circular_491_tb
                        sSQL = sSQL & ",NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL" _
                             & ",NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL" _
                             & ",NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL"
                        'bloco rpt_apolice_garantia_circular_491_tb
                        sSQL = sSQL & ",NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL"
                        'bloco rpt_apolice_cosseguro_circular_491_tb
                        sSQL = sSQL & ",NULL,NULL,NULL,NULL"
                        'bloco rpt_apolice_corretor_circular_491_tb
                        sSQL = sSQL & "," & s_corretor_nome & "," & s_corretor_cod_susep & "," & s_corretor_distribuicao

                        Set rs = rdocn.OpenResultset(sSQL)
                        rs.Close
                    Next

                    '    s_cobranca_premio_liquido_1a = "'" & .PremioLiq & "'"
                    '    s_cobranca_IOF_1a = "'" & .IOFTotal & "'"
                    '    s_cobranca_premio_total_1a = "'" & .PremioTarifaTotal & "'"


                    sSQL = "EXEC SEGS12948_SPI " & s_proposta_id & "," & s_usuario & ",'n'," & s_subgrupo
                    'bloco rpt_apolice_circular_491_tb
                    sSQL = sSQL & "," & s_dados_nome & "," & s_dados_cnpj_cpf & "," & s_dados_ramo_ativ & "," & s_dados_endereco & "," & s_dados_numero & "," _
                         & s_dados_bairro & "," & s_dados_cidade & "," & s_dados_cep & "," & s_dados_uf & "," & s_dados_email & "," & s_dados_fone & "," _
                         & s_dados_admin & "," & s_dados_cpf & "," & s_seguravel_perfil & "," & s_seguravel_adesao & "," & s_seguravel_min_1a & "," _
                         & s_seguravel_max_1a & "," & s_seguravel_transf & "," & s_seguravel_min_demais & "," & s_seguravel_max_demais & "," & s_taxa_taxa & "," _
                         & s_taxa_Cust_seguro & "," & s_taxa_estipulante & "," & s_taxa_segurado & "," & s_taxa_pgto_premio & "," & s_cobranca_corte & "," _
                         & s_cobranca_forma_pgto & "," & s_cobranca_venc_demais & "," & s_cobranca_periodo & ",'" & s_cobranca_premio_liquido_1a & "','" _
                         & s_cobranca_IOF_1a & "','" & s_cobranca_premio_total_1a & "'," & s_cobranca_correntista & "," & s_cobranca_bnc_codigo & "," _
                         & s_cobranca_bnc_nome & "," & s_cobranca_bnc_agencia_dv & "," & s_cobranca_bnc_conta_dv & "," & s_excedente_tecnico & "," & s_comissao_percentual

                    Set rs = rdocn.OpenResultset(sSQL)
                    rs.Close
                End If
            Next
            '  Recupera dados da impressora padr�o
            DeviceDefault = LeArquivoIni("WIN.INI", "windows", "device")
            OldDefault = DeviceDefault

            'Inicializar Cristal
            Report.Reset

            If ImprimirTela = True Then
                Report.Destination = crptToWindow    ' direciona saida para Preview de tela.
                Report.WindowState = crptMaximized
                
                TotalImpresoTela = TotalImpresoTela + 1
            Else
                Report.Destination = crptToPrinter
            End If

            Report.Connect = rdocn.Connect
            Report.ReportFileName = App.PATH & "\SEGR0223-12.rpt"

            For pnt = 1 To 70: Report.Formulas(pnt) = "": Next pnt

            'Carregar dados do formul�rio
            Report.Formulas(1) = "proposta_id=" & vPropostaId
            Report.Formulas(2) = "usuario='" & Usuario & "'"
            Report.Formulas(3) = "001_grupo_ramo = '" & s_grupo_ramo & "'"
            Report.Formulas(4) = "002_modalidade = '" & s_modalidade & "'"
            Report.Formulas(5) = "003_num_apolice = '" & txtApolice.Text & "'"
            Report.Formulas(6) = "004_num_proposta = '" & txtNumero.Text & "'"
            Report.Formulas(7) = "005_dt_emissao = '" & txtDtEmissao.Text & "'"
            Report.Formulas(8) = "006_cod_agencia = '" & Trim(txtCodAgencia.Text) & "'"
            Report.Formulas(9) = "007_nm_agencia = '" & Trim(txtNomeAgencia.Text) & "'"
            Report.Formulas(10) = "008_dt_inicio_vigencia = '" & txtIniVigencia.Text & "'"
            Report.Formulas(11) = "009_dt_fim_vigencia = '" & txtFimVigencia.Text & "'"
            Report.Formulas(12) = "010_estipula_nome = '" & txtNome.Text & "'"
            Report.Formulas(13) = "011_estipula_cnpj_cpf = '" & txtCPF_CGC.Text & "'"
            Report.Formulas(14) = "012_estipula_ramo_ativ = ''"                    'N�o h� "Ramo de Atividade" e "Administrador - CPF" cadastrados no SEGBR.
            Report.Formulas(15) = "013_estipula_endereco = '" & txtEndereco.Text & "'"
            Report.Formulas(16) = "014_estipula_numero = ''"                       '<<<<<<<< N�O EXISTE, ENDERECO E N�MERO � JUNTO. Formula n�o � usada
            Report.Formulas(17) = "015_estipula_bairro = '" & TxtBairro.Text & "'"
            Report.Formulas(18) = "016_estipula_cidade = '" & txtCidade.Text & "'"
            Report.Formulas(19) = "017_estipula_cep = '" & txtCEP.Text & "'"
            Report.Formulas(20) = "018_estipula_uf = '" & txtUF.Text & "'"
            Report.Formulas(21) = "019_estipula_email = ''"                        '<<<<<<<< CARREGAR DE cliente_tb
            Report.Formulas(22) = "020_estipula_fone = '" & Trim(txtDDD.Text & " " & txtTelefone.Text) & "'"
            Report.Formulas(23) = "021_estipula_admin = ''"                        'N�o h� "Ramo de Atividade" e "Administrador - CPF" cadastrados no SEGBR.
            Report.Formulas(24) = "022_estipula_cpf = ''"                          'N�o h� "Ramo de Atividade" e "Administrador - CPF" cadastrados no SEGBR.
            vAux = Split("janeiro,fevereiro,mar�o,abril,maio,junho,julho,agosto,setembro,outubro,novembro,dezembro", ",")
            sAux = "S�o Paulo, " & Day(dt_documento) & " de " & vAux(Month(dt_documento) - 1) & " de " & Year(dt_documento) & "."
            Report.Formulas(25) = "099_local_data = '" & sAux & "'"
            vAux = Null
            Report.Formulas(26) = "027_nome_produto = '" & txtProduto.Text & "'"
            SQL = "SELECT   isnull(a.num_proc_susep, p.num_proc_susep) num_proc_susep"
            SQL = SQL & " FROM  apolice_tb a  WITH (NOLOCK) "
            SQL = SQL & " join  proposta_tb b  WITH (NOLOCK) "
            SQL = SQL & "    on b.proposta_id = a.proposta_id"
            SQL = SQL & " join  produto_tb p  WITH (NOLOCK) "
            SQL = SQL & "    on p.produto_id = b.produto_id"
            SQL = SQL & " Where a.proposta_id = " & vPropostaId
            '
            Set rc = rdocn.OpenResultset(SQL)
            '
            If Not rc.EOF Then
                Processo_SUSEP = Replace(Replace(Replace(rc!num_proc_susep, "-", ""), ".", ""), "/", "")
                Processo_SUSEP = Format(Processo_SUSEP, "@@.@@@@@@/@@-@@")
                Report.Formulas(27) = "028_processo_susep= 'Processo SUSEP N�: " & Processo_SUSEP & "'"
            End If
            rc.Close

            Report.Formulas(28) = "029_titulo = 'CERTIFICADO DO SEGURO'"
            Report.Formulas(29) = "033_titular = '" & Titular & "'"
            Report.Formulas(30) = "039_cpf_ti = '" & Cpf_Titular & "'"
            Report.Formulas(31) = "031_texto1 = '" & Texto1 & "'"
            Report.Formulas(32) = "032_texto2 = '" & Texto2 & "'"
            Report.Formulas(33) = "033_texto3 = '" & texto3 & "'"
            Report.Formulas(34) = "034_texto4 = '" & texto4 & "'"
            Report.Formulas(35) = "049_texto_benef_1 = '" & texto_benef_1 & "'"
            Report.Formulas(36) = "050_texto_benef_2 = '" & texto_benef_2 & "'"
            Report.Formulas(37) = "051_texto_benef_3 = '" & texto_benef_3 & "'"
            Report.Formulas(38) = "052_texto_benef_4 = '" & texto_benef_4 & "'"
            Report.Formulas(39) = "053_texto_benef_5 = '" & texto_benef_5 & "'"
            Report.Formulas(40) = "034_dt_nascimento_ti = '" & dt_nascimento_ti & "'"
            Report.Formulas(41) = "063_vencimento = '" & pagamento_dia & "'"
            Report.Formulas(42) = "062_forma_pg = '" & periodo_pagamento & "'"
            Report.Formulas(43) = "058_custeio = '" & custeio & "'"
            Report.Formulas(44) = "066_premio_bruto = '" & PremioBruto & "'"
            Report.Formulas(45) = "065_premio_total = '" & PremioTarifaTotal & "'"
            Report.Formulas(46) = "057_num_certificado = '" & certificado_id & "'"

            If ImprimirTela = False Then
                ImprimeOk = False
                dialog.CancelError = True
                dialog.ShowPrinter
                '
                If Err = 32755 Then
                    MousePointer = vbDefault
                    GoTo jpFim
                End If
            Else
                ImprimeOk = True
            End If

            ' Seta a impressora
            DeviceDefault = LeArquivoIni("WIN.INI", "windows", "device")

            If DeviceDefault <> OldDefault Then
                MudouPrtDefault = True
            Else
                MudouPrtDefault = False
            End If

            If TotalImpresoTela >= 15 Then
                NumRows = NumRows2
            End If
            ' Imprime documento.
            Report.Action = 1
        End If

        NumRows = NumRows + 1
    Loop
    
    If NumRows > 1 Then
        SQL = ""
        SQL = "exec evento_seguros_db.dbo.evento_impressao_spi "
        SQL = SQL & vPropostaId & ", " & gEndossoId & ", NULL, '" & Usuario & "', NULL, 5, 'O','IMPRESSAO DE CERTIFICADO'," & subgrupo_id & ",'A', NULL, NULL, '" & Usuario & "' ,NULL, NULL, NULL, NULL, NULL, NULL"
        
        Set rsimpressao = rdocn.OpenResultset(SQL)
        
        num_solicitacao = rsimpressao(0).Value
        
        Set rsimpressao = Nothing
    
        'Inclus�o do evento 81034 pois as propostas inseridas na evento_impressao_tb dessa maneira n�o ser�o impressas.
        SQL = "exec evento_seguros_db.dbo.evento_spi " & vPropostaId & ", " & IIf(sPropostaBB = "", "null", sPropostaBB) & ",null, null, null, null, null, null, null, null, null, null, '" & Usuario & "', '" & Format(Now, "yyyymmdd") & "'" & ",81034"
        
        rdocn.Execute (SQL)
    End If
    
    If TotalImpresoTela >= 15 Then
        MsgBox "O limite m�ximo para impress�o em tela � de 15 certificados por vez", vbOKOnly + vbInformation, "Limite"
    End If

jpFim:
    On Error Resume Next

    'limpar tabela
    sSQL = "EXEC SEGS12948_SPI " & s_proposta_id & "," & s_usuario & ",'s'"
    Set rs = rdocn.OpenResultset(sSQL)
    rs.Close

    For pnt = 1 To 70: Report.Formulas(pnt) = "": Next pnt
    If MudouPrtDefault Then
        Call RetornaPrtDefault("WIN.INI", OldDefault)
    End If
    Err.Clear

    MousePointer = vbDefault
    StatusBar1.SimpleText = ""
    Screen.MousePointer = vbNormal
    Exit Sub
jpErro:

    Screen.MousePointer = vbNormal
    TrataErroGeral "Impress�o de Ap�lice"

    GoTo jpFim
    Resume
End Sub


'****************************************
'*  Bot�o Visualizar desabilidado       *
'*  Demanda Evolutiva: 176384           *
'*  AFONSO - G&P - DATA: 19/05/2008     *
'****************************************
Private Sub visualizar_Click()

'Dim i As Long
'Dim marcado As Boolean
'
''***************************************************************
''Stefanini (Maur�cio), em 05/08/2005 para Verificar se o usu�rio
''selecionou algum subgrupo.
''***************************************************************
'marcado = False
'
'If MSFlexGrid1(1).Visible = True Then
'    For i = 1 To MSFlexGrid1(1).Rows - 1
'        If MSFlexGrid1(1).TextMatrix(i, 0) = "X" Then
'            marcado = True
'        End If
'    Next
'
'    If Not marcado Then
'        MsgBox ("Favor selecionar um SubGrupo")
'        Exit Sub
'    End If
'End If
'
'If MSFlexGrid2(0).Visible = True Then
'    For i = 1 To MSFlexGrid2(0).Rows - 1
'        If MSFlexGrid2(0).TextMatrix(i, 0) = "X" Then
'            marcado = True
'        End If
'    Next
'
'    If Not marcado Then
'        MsgBox ("Favor selecionar uma Proposta")
'        Exit Sub
'    End If
'
'End If
'
'MsgBox "A Vizualisa��o do Documento n�o afetar� o processo", vbInformation + vbOKOnly, "Aviso"
'
'
'x = 0
'R = 1
'
'Screen.MousePointer = 11
'PETOCX1.OutputPageHeight = 840
'PETOCX1.OutputPageWidth = 596
'
'
'Do While (R <> 0) And (x < 10)
'arqu = "c:\temp\pdftemp" & x & ".pdf"
'R = PETOCX1.OpenOutputFile(arqu)
'x = x + 1
'Loop
'
'If R = 0 Then
'
'    Visualizacao_Doc = True
'
'    PETOCX1.AddLogo App.PATH & "\certificado.pdf", 1
'
'        'Gerar_visu_vidas
'
'
'    PETOCX1.CloseOutputFile
'    ShellExecute Me.hwnd, "open", arqu, "", "c:\temp", SW_SHOWNORMAL
'Else
'
'    MsgBox "Erro ao Gerar o Arquivo de Visualiza��o" & vbNewLine & "Erro: " & R, vbCritical + vbOKOnly, "Erro"
'
'End If
'
'Screen.MousePointer = 0
'Visualizacao_Doc = False
'
End Sub

'Private Sub Gerar_visu_subgrupos()
''****************************************************************
''*  Fun��o n�o ser� mais ultilizada                             *
''*  Demanda Evolutiva: 176384 - AFONSO - G&P                    *
''*  Novo processo, ser� eliminado o sistema de cria��o de PDF   *
''****************************************************************
'
'Dim corretor_c As String
'Dim rsAux       As New ADODB.Recordset
'
'On Error GoTo Erro
'
'numrows = MSFlexGrid1(1).Rows - 1
''PETOCX1.AddInternalLinkBookmark RetirarAcento("Apolice - " & vApoliceId), 1, 0, 840
''PETOCX1.GotoNextBookmarkLevel
'a = 1
'
''busca os dados do certificado
''nome
'parametro = RetirarAcento("SEGA8017 CERTIFICADO RAMO " & vRamoId)
'SQL = "exec vl_parametro_sps '" & parametro & "'"
'Set rs = rdocn.OpenResultset(SQL)
'NomeCertificado = RetirarAcento(rs(0).Value)
'Set rs = Nothing
'
''num susep
'SQL = "exec num_proc_susep_apolice_sps " & vApoliceId & "," & vRamoId
'Set rs = rdocn.OpenResultset(SQL)
'proc_susep = rs(0).Value
'Set rs = Nothing
'If IsNull(proc_susep) Then
'    proc_susep = "00.000000/00-0"
'End If
'
''nome do ramo
'SQL = "select nome from ramo_tb   WITH (NOLOCK)  "
'SQL = SQL & " where ramo_id = " & vRamoId
'Set rs = rdocn.OpenResultset(SQL)
'NomeRamo = vRamoId & " - " & rs(0).Value
'Set rs = Nothing
'
''nome do estipulante
'SQL = "select pr.proposta_id, cl.nome, cl.cpf_cnpj from cliente_tb cl  WITH (NOLOCK) , proposta_tb pr  WITH (NOLOCK) , apolice_tb ap  WITH (NOLOCK)  where "
'SQL = SQL & " ap.apolice_id = " & vApoliceId & " and ap.ramo_id = " & vRamoId
'SQL = SQL & " and pr.proposta_id = ap.proposta_id and cl.cliente_id = prop_cliente_id"
'Set rs = rdocn.OpenResultset(SQL)
'Estipulante = rs(1).Value
'CPF_cnpj_estipulante = MasCGC(rs(2).Value)
'PROPOSTA = rs("proposta_id").Value
'Set rs = Nothing
'
'Do While numrows <> 0
'    'coment
'    If MSFlexGrid1(1).TextMatrix(numrows, 0) = "X" Then
'       gerou = True
'       subgrupo_id = MSFlexGrid1(1).TextMatrix(numrows, 1)
'       num_subgrupo = Conta_Vidas_Subgrupos(CInt(subgrupo_id))
'
'       'n�o executar a SPI se for paravisualizar o documento
'       If Visualizacao_Doc = False Then
'            SQL = "exec evento_seguros_db..evento_impressao_spi " & PROPOSTA & ", 0 , null, " & cUserName & ",c, 5, i,'Sub Grupo - Inteiro'," & num_subgrupo & ",'l', null, null," & cUserName & ",'" & Format(Date, "mm/dd/yyyy") & " " & Time & "', 'segp0223->certificado->*.pdf'"
'            adoCn.Execute (SQL)
'        End If
'
'       'ALTERA��O - DIEGO GALIZONI CAVERSAN - G&P - 27/12/2007
'       'Inclus�o do evento 81034 pois as propostas inseridas na evento_impressao_tb dessa maneira n�o ser�o impressas.
'       'AFONSO N�o executar a SPI se for para visualizar o documento
'       If Visualizacao_Doc = False Then
'            SQL = "exec evento_seguros_db..evento_spi " & PROPOSTA & ", " & sPropostaBB & ",null, null, null, null, null, null, null, null, null, null, " & cUserName & ", '" & Format(Now, "yyyymmdd") & "'" & ",81034"
'            adoCn.Execute (SQL)
'       End If
'
'       SQL = "exec dados_segurado_sps null, " & vRamoId & ", " & vApoliceId & ", " & subgrupo_id
'       Set rssegurado = rdocn.OpenResultset(SQL)
'       PETOCX1.AddInternalLinkBookmark "Subgrupo - " & MSFlexGrid1(1).TextMatrix(numrows, 2), a, 20, 840
'       PETOCX1.GotoNextBookmarkLevel
'       SQL = "  Select cl.nome, "
'       SQL = SQL & " cl.cpf_cnpj "
'       SQL = SQL & " from cliente_tb cl                  WITH (NOLOCK)        "
'       SQL = SQL & " INNER JOIN representacao_sub_grupo_tb rsbg     WITH (NOLOCK) "
'       SQL = SQL & " ON  cl.cliente_id = rsbg.est_cliente_id"
'       SQL = SQL & " where  rsbg.apolice_id     = " & vApoliceId & " "
'       SQL = SQL & "    and rsbg.ramo_id        = " & vRamoId
'       SQL = SQL & "    and rsbg.sub_grupo_id   = " & subgrupo_id & " "
'
'       Set rs = rdoCn1.OpenResultset(SQL)
'       subEstipulante = rs(0).Value
'       CPF_cnpj_subestipulante = MasCGC(rs(1).Value)
'       Set rs = Nothing
'
'       SQL = "select distinct cor.nome, cor.corretor_susep "
'       SQL = SQL & "  from corretagem_sub_grupo_tb csgb"
'       SQL = SQL & "  INNER JOIN  corretor_tb cor "
'       SQL = SQL & "    ON cor.corretor_id = csgb.corretor_id "
'       SQL = SQL & "  where csgb.apolice_id     = " & vApoliceId & "        and "
'       SQL = SQL & "  csgb.ramo_id              = " & vRamoId & "            and "
'       SQL = SQL & "  csgb.sub_grupo_id         = " & subgrupo_id & "      "
'
'       Set rs = rdoCn1.OpenResultset(SQL)
'       corretor_c = ""
'       If Not rs.EOF Then
'          While Not rs.EOF
'            corretor_c = corretor_c & "|" & rs("nome")
'            susep_corretor = susep_corretor & "|" & MasCorrSusep(IIf(IsNull(rs("corretor_susep")), 0, rs("corretor_susep")))
'            rs.MoveNext
'          Wend
'       End If
'       rs.Close
'       Set rs = Nothing
'
'       'Layout do Subgrupo
'        SQL = "SELECT"
'        SQL = SQL & " campo_layout_vida_sub_grupo_tb.sucursal_seguradora_id,"
'        SQL = SQL & " campo_layout_vida_sub_grupo_tb.seguradora_cod_susep,"
'        SQL = SQL & " campo_layout_vida_sub_grupo_tb.ramo_id,"
'        SQL = SQL & " campo_layout_vida_sub_grupo_tb.apolice_id,"
'        SQL = SQL & " campo_layout_vida_sub_grupo_tb.sub_grupo_id,"
'        SQL = SQL & " campo_layout_vida_sub_grupo_tb.titulo_campo_1,"
'        SQL = SQL & " campo_layout_vida_sub_grupo_tb.titulo_campo_2,"
'        SQL = SQL & " campo_layout_vida_sub_grupo_tb.titulo_campo_3,"
'        SQL = SQL & " campo_layout_vida_sub_grupo_tb.titulo_campo_4,"
'        SQL = SQL & " campo_layout_vida_sub_grupo_tb.   titulo_campo_5"
'        SQL = SQL & " FROM"
'        SQL = SQL & "       campo_layout_vida_sub_grupo_tb      WITH (NOLOCK)  "
'        SQL = SQL & " INNER JOIN  sub_grupo_apolice_tb"
'        SQL = SQL & "     ON sub_grupo_apolice_tb.apolice_id             =  campo_layout_vida_sub_grupo_tb.apolice_id"
'        SQL = SQL & "     AND sub_grupo_apolice_tb.ramo_id                =  campo_layout_vida_sub_grupo_tb.ramo_id"
'        SQL = SQL & "     AND sub_grupo_apolice_tb.sucursal_seguradora_id =  campo_layout_vida_sub_grupo_tb.sucursal_seguradora_id"
'        SQL = SQL & "     AND sub_grupo_apolice_tb.seguradora_cod_susep   =  campo_layout_vida_sub_grupo_tb.seguradora_cod_susep"
'        SQL = SQL & "     AND sub_grupo_apolice_tb.dt_inicio_vigencia_sbg =  campo_layout_vida_sub_grupo_tb.dt_inicio_vigencia_sbg"
'        SQL = SQL & " WHERE"
'        SQL = SQL & "       campo_layout_vida_sub_grupo_tb.apolice_id   = " & vApoliceId         'Ap�lice digitada pelo usu�rio
'        SQL = SQL & "   and campo_layout_vida_sub_grupo_tb.sub_grupo_id = " & subgrupo_id  'Sub-grupo digitado pelo usu�rio
'        SQL = SQL & "   and campo_layout_vida_sub_grupo_tb.ramo_id      = " & vRamoId           'ramo digitado pelo usu�rio
'        SQL = SQL & " order by campo_layout_vida_sub_grupo_tb.sub_grupo_id"
'
'        Set rs = rdoCn2.OpenResultset(SQL)
'
'        If Not rs.EOF Then
'            titulo1 = IIf(IsNull(rs("titulo_campo_1")), "", Trim(rs("titulo_campo_1")))
'            valortitulo1 = IIf(IsNull(rs("valor_1")), "", Trim(rs("valor_1")))
'            titulo2 = IIf(IsNull(rs("titulo_campo_2")), "", Trim(rs("titulo_campo_2")))
'            valortitulo2 = IIf(IsNull(rs("valor_2")), "", Trim(rs("valor_2")))
'            titulo3 = IIf(IsNull(rs("titulo_campo_3")), "", Trim(rs("titulo_campo_3")))
'            valortitulo3 = IIf(IsNull(rs("valor_3")), "", Trim(rs("valor_3")))
'            titulo4 = IIf(IsNull(rs("titulo_campo_4")), "", Trim(rs("titulo_campo_4")))
'            valortitulo4 = IIf(IsNull(rs("valor_4")), "", Trim(rs("valor_4")))
'            titulo5 = IIf(IsNull(rs("titulo_campo_5")), "", Trim(rs("titulo_campo_5")))
'            valortitulo5 = IIf(IsNull(rs("valor_5")), "", Trim(rs("valor_5")))
'        End If
'        Set rs = Nothing
'
'        Do While Not rssegurado.EOF
'          vSucursalId = rssegurado("sucursal")
'          vCodSusep = rssegurado("cod_susep")
'          'verifica para ver se imprime segurado ou n�o dependendo do check2 (somente ativas)
'          If Check2(0).Value = 1 And rssegurado("situacao") = "Ativa" Then
'             gerar = True
'          ElseIf Check2(0).Value = 0 Then
'              gerar = True
'          Else
'              gerar = False
'          End If
'
'
'          If gerar Then
'
'            If a <> 1 Then PETOCX1.NewPage
'            PETOCX1.PrintLogo
'
'            PETOCX1.SetFont "Arial", 9
'            'imprime dados do certificado
'            PETOCX1.PrintText 230, 818, RetirarAcento(NomeCertificado)
'            PETOCX1.PrintText 230, 807, RetirarAcento("Processo SUSEP : VG - " & proc_susep)
'
'            PETOCX1.SetFont "Arial", 7
'
'            'imprime apolice
'            PETOCX1.PrintText 52, 765, RetirarAcento(Text1(0).Text)
'
'            'imprime certificado
'
'            SQL = "exec rel_certif_sps " & vApoliceId & ", " & rssegurado("sucursal") & ", " & rssegurado("cod_susep") & ", " & vRamoId & "," & subgrupo_id & ", " & rssegurado("prop_cliente_id") & ", '" & Usuario & "', null"
'            'Set rs = rdoCn1.OpenResultset(SQL)
'
'            'Alterado: Rafael OShiro 08/10/04
'            'usando ADO para executar procedure
'            Set rsAux = adoCn.Execute(SQL)
'            certificado_id = Format(rsAux(0).Value, "000000000")
'
'                If rssegurado("situacao") = "Ativa" Then
'                'Data Emiss�o Certificado
'                SQL = ""
'                SQL = SQL & "Select "
'                SQL = SQL & "Dt_Emissao "
'                SQL = SQL & "From "
'                SQL = SQL & "certificado_tb "
'                SQL = SQL & "Where "
'                SQL = SQL & "apolice_id = " & vApoliceId & " "
'                SQL = SQL & "and sucursal_seguradora_id = " & vSucursalId & " "
'                SQL = SQL & "and seguradora_cod_susep = " & vCodSusep & " "
'                SQL = SQL & "and ramo_id = " & vRamoId & " "
'                SQL = SQL & "and certificado_id = " & certificado_id & " "
'                SQL = SQL & "and dt_fim_vigencia is NULL "
'
'                Set bd = rdoCn1.OpenResultset(SQL)
'                If Not bd.EOF Then
'                If Not IsNull(bd!dt_emissao) Then
'                s_dataemissao = Format(bd!dt_emissao, "dd/mm/yyyy")
'                Else
'                SQL = "exec certificado_emissao_spu " & vCodSusep & "," & vSucursalId & "," & vRamoId & "," & vApoliceId & "," & PROPOSTA & "," & certificado_id & ",'" & Format(Date, "mm/dd/yyyy") & " " & Time & "'," & cUserName
'                rdoCn1.OpenResultset SQL
'                SQL = ""
'                SQL = SQL & "Select "
'                SQL = SQL & "Dt_Emissao "
'                SQL = SQL & "From "
'                SQL = SQL & "certificado_tb "
'                SQL = SQL & "Where "
'                SQL = SQL & "apolice_id = " & vApoliceId & " "
'                SQL = SQL & "and sucursal_seguradora_id = " & vSucursalId & " "
'                SQL = SQL & "and seguradora_cod_susep = " & vCodSusep & " "
'                SQL = SQL & "and ramo_id = " & vRamoId & " "
'                SQL = SQL & "and certificado_id = " & certificado_id & " "
'                SQL = SQL & "and dt_fim_vigencia is NULL "
'                Set bd = rdoCn1.OpenResultset(SQL)
'                s_dataemissao = IIf(Not IsNull(bd!dt_emissao), Format(bd!dt_emissao, "dd/mm/yyyy"), Date)
'                End If
'                End If
'                bd.Close
'
'
'            'Set rs = Nothing
'            'rdoCn1.Close
'            rsAux.Close
'
'            PETOCX1.PrintText 152, 765, RetirarAcento(certificado_id)
'
'            'imprime ramo
'            PETOCX1.PrintText 232, 765, RetirarAcento(NomeRamo)
'
'            'imprime estipulante
'            PETOCX1.PrintText 52, 736, RetirarAcento(Estipulante)
'            PETOCX1.PrintText 440, 736, RetirarAcento(CPF_cnpj_estipulante)
'
'            'imprime subestipulante
'            BuscaDadosSUBEstipulante (subgrupo_id)
'            If ED_NomeSubEstipulante <> Estipulante Then
'                PETOCX1.PrintText 52, 710, RetirarAcento(ED_NomeSubEstipulante)
'                PETOCX1.PrintText 440, 710, RetirarAcento(ED_CNPJSubEstipulante)
'            End If
'
'            'imprime dados do segurado
'            PETOCX1.PrintText 40, 675, RetirarAcento("Titular: ")
'            PETOCX1.PrintText 120, 675, RetirarAcento(rssegurado("nome"))
'            PETOCX1.PrintText 40, 665, RetirarAcento("Data de Nascimento: ")
'            PETOCX1.PrintText 120, 665, RetirarAcento(rssegurado("dt_nascimento"))
'            PETOCX1.PrintText 40, 655, RetirarAcento("CPF: ")
'            PETOCX1.PrintText 120, 655, RetirarAcento(MasCPF(rssegurado("cpf")))
'            PETOCX1.SetFont "Arial", 6
'            'imprime titulo e dados do segurado
'            Texto = ""
'            If Not IsNull(titulo1) And titulo1 <> "" Then Texto = titulo1 & " - " & valortitulo1 & "    "
'            If Not IsNull(titulo1) And titulo1 <> "" Then Texto = Texto & titulo2 & " - " & valortitulo2 & "    "
'            If Not IsNull(titulo1) And titulo1 <> "" Then Texto = Texto & titulo2 & " - " & valortitulo3 & "    "
'            If Not IsNull(titulo1) And titulo1 <> "" Then Texto = Texto & titulo4 & " - " & valortitulo4 & "    "
'            If Not IsNull(titulo1) And titulo1 <> "" Then Texto = Texto & titulo5 & " - " & valortitulo5
'            PETOCX1.PrintText 40, 687, RetirarAcento(Texto)
'
'            PETOCX1.SetFont "Arial", 7
'
'            '------------------------'
'            '  COBERTURA DO TITULAR  '
'            '------------------------'
'
'            'Para buscar da cobertura do titular
'            'vparametro = "TP COMP TITULAR"
'            'BuscaDadosCob
'            Call BuscarCoberturas("TP COMP TITULAR", CInt(subgrupo_id))
'
'            CT_SequencialCoberturaTitular = 0
'
'            If Not bd_cob.EOF Then
'
'                PETOCX1.PrintText 35, 585, RetirarAcento("Titular")
'                PETOCX1.PrintText 120, 585, RetirarAcento("Inicio de Vig�ncia")
'                datavig = rssegurado("dt_inicio_vigencia_sbg")
'                If Mid(datavig, 2, 1) = "/" Then datavig = "0" & datavig
'                If Mid(datavig, 5, 1) = "/" Then datavig = Mid(datavig, 1, 3) & "0" & Mid(datavig, 4, 6)
'                PETOCX1.PrintText 190, 585, RetirarAcento(datavig)
'                pos = 575
'                PETOCX1.SetFont "Arial", 6
'                While Not bd_cob.EOF
'
'                    'Efetua atribui��es dos campos
'                    CT_NomeGarantia = IIf(Not IsNull(bd_cob!Nome), bd_cob!Nome, Space(60))
'                    If UCase(CT_NomeGarantia) = UCase("INVALIDEZ PERMANENTE TOTAL OU PARCIAL POR ACIDENTE - IPA") Then CT_NomeGarantia = CT_NomeGarantia & " AT�"
'                    'Valor da Garantia
'                    If UCase(bd_cob!class_tp_cobertura) = "B" Then 'Se for cobertura b�sica do titular
'                        If Not IsNull(rssegurado("val_capital_segurado")) Then
'                            CT_Valorgarantia = "R$ " + Format(Replace(rssegurado("val_capital_segurado"), ".", ","), "##,###,##0.00")
'                        Else
'                            CT_Valorgarantia = Space(15)
'                        End If
'                     Else 'Se for outra cobertura, aplicar o percentual sobre a cobertura b�sica
'                        If Not IsNull(rssegurado("val_capital_segurado")) Then
'                            temp = Val(Replace(rssegurado("val_capital_segurado"), ",", "."))
'                            temp2 = Replace(bd_cob!perc_basica, ",", ".")
'                            temp2 = Val(temp2)
'                            temp2 = (temp * temp2) / 100
'                            temp = Format(temp2, "###,###,###,##0.00")
'
'                            CT_Valorgarantia = "R$ " & temp
'                        Else
'                            CT_Valorgarantia = Space(15)
'                        End If
'                    End If
'                    If Not IsNull(bd_cob!val_min_franquia) Then
'                        Ct_FRANQUIA = "R$ " + Format(Val(Replace(bd_cob!val_min_franquia, ".", ",")), "##,###,##0.00")
'                    Else
'                        Ct_FRANQUIA = "R$0,00"
'                    End If
'                    ct_carencia = IIf(Not IsNull(bd_cob!carencia_dias), bd_cob!carencia_dias, 0)
'
'                    PETOCX1.SetFont "Arial", 6
'
'                    PETOCX1.PrintText 35, pos, RetirarAcento(CT_NomeGarantia)
'                    PETOCX1.PrintText 270, pos, RetirarAcento(CT_Valorgarantia)
'                    If (Ct_FRANQUIA <> "R$0,00") Then
'                        PETOCX1.PrintText 350, pos, RetirarAcento("Franquia - ")
'                        PETOCX1.PrintText 380, pos, RetirarAcento(Ct_FRANQUIA)
'                    End If
'
'                    If ct_carencia <> "0" Then
'                        PETOCX1.PrintText 460, pos, RetirarAcento("Car�ncia -")
'                        PETOCX1.PrintText 490, pos, RetirarAcento(ct_carencia & " dias")
'                    End If
'
'                    bd_cob.MoveNext
'                    pos = pos - 10
'                Wend
'            End If
'            bd_cob.Close
'
'            texto_beneficiario_conjuge = ""
'
'            'imprime dados do conjuge
'            SQL = "exec dados_conjuge_sps " & vApoliceId & "," & vRamoId & "," & subgrupo_id & "," & rssegurado("prop_cliente_id")
'           ' SQL = "EXEC DADOS_CONJUGE_SPS 13220, 93,1,3949530"
'            Set rsconjuge = rdoCn1.OpenResultset(SQL)
'            If Not rsconjuge.EOF Then
'                PETOCX1.SetFont "Arial", 7
'                PETOCX1.PrintText 35, 640, RetirarAcento("Dados do C�njuge")
'                PETOCX1.PrintText 40, 630, RetirarAcento("Nome: ")
'                c_nomeConjuge = rsconjuge("nome")
'                PETOCX1.PrintText 120, 630, RetirarAcento(rsconjuge("nome"))
'                PETOCX1.PrintText 40, 620, RetirarAcento("Data de Nascimento: ")
'                PETOCX1.PrintText 120, 620, RetirarAcento(rsconjuge("dt_nascimento"))
'                PETOCX1.PrintText 40, 610, RetirarAcento("CPF: ")
'                PETOCX1.PrintText 120, 610, RetirarAcento(MasCPF(rsconjuge("cpf")))
'                texto_beneficiario_conjuge = IIf(IsNull(rsconjuge("texto_beneficiario")), "", RetirarAcento(rsconjuge("texto_beneficiario")))
'            End If
'
'            '----------------------'
'            ' COBERTURA DO C�NJUGE '
'            '----------------------'
'
'            'Para buscar coberturas do conjuge
'            Call BuscarCoberturas("TP COMP CONJUGE", 1)     'Alterado em 03/04/03 - Abosco
'
'            SQL = "select tp_clausula_conjuge from sub_grupo_apolice_tb "
'            SQL = SQL & " where "
'            SQL = SQL & " apolice_id = " & vApoliceId & " and "
'            SQL = SQL & "     ramo_id = " & vRamoId & " and "
'            SQL = SQL & " sub_grupo_id = " & subgrupo_id
'
'            rs = rdoCn2.OpenResultset(SQL)
'            If rs("tp_clausula_conjuge") = "A" Then
'                Automatico = True
'            End If
'            Set rs = Nothing
'
'            CC_SequencialCoberturaConjuge = 0
'
'            If (Not bd_cob.EOF) And (Trim(c_nomeConjuge) <> "") Or Automatico Then   'Alterado em 03/04/03
'
'                If Automatico Then
'                    PETOCX1.SetFont "Arial", 7
'                    PETOCX1.PrintText 35, 515, RetirarAcento("C�njuge                 Inclus�o Autom�tica")
'                Else
'                    If Not rsconjuge.EOF Then
'                    PETOCX1.PrintText 35, 515, RetirarAcento("C�njuge")
'                    PETOCX1.PrintText 120, 515, RetirarAcento("Inicio de Vig�ncia")
'                    PETOCX1.PrintText 190, 515, RetirarAcento(Format(rsconjuge("dt_inicio_vigencia_sbg"), "dd/mm/yyyy"))
'                    End If
'
'                End If
'
'                pos = 505
'                PETOCX1.SetFont "Arial", 6
'                If Not rsconjuge.EOF Or Automatico Then
'                While Not bd_cob.EOF
'
'                   'Efetua atribui��es dos campos
'                    CC_NomeGarantia = IIf(Not IsNull(bd_cob!Nome), bd_cob!Nome, Space(60))
'                    If UCase(CC_NomeGarantia) = UCase("INVALIDEZ PERMANENTE TOTAL OU PARCIAL POR ACIDENTE - IPA") Then CC_NomeGarantia = CC_NomeGarantia & " AT�"
'
'                    temp = Val(Replace(rssegurado("val_capital_segurado"), ",", "."))
'                    temp2 = Replace(bd_cob!perc_basica, ",", ".")
'                    temp2 = Val(temp2)
'                    temp2 = (temp * temp2) / 100
'                    temp = Format(temp2, "###,###,###,##0.00")
'
'                    cc_valorgarantia = "R$ " & temp
'
'                    If Not IsNull(bd_cob!val_min_franquia) And (bd_cob!val_lim_max_is) <> ".00" Then
'                        CC_FRANQUIA = "R$ " + Format(Val(bd_cob!val_min_franquia), "##,###,##0.00")
'                    Else
'                        CC_FRANQUIA = "R$0,00"
'                    End If
'                    cc_carencia = IIf(Not IsNull(bd_cob!carencia_dias), bd_cob!carencia_dias, 0)
'
'                    bd_cob.MoveNext
'
'                    PETOCX1.SetFont "Arial", 6
'
'                    PETOCX1.PrintText 35, pos, RetirarAcento(CC_NomeGarantia)
'                    PETOCX1.PrintText 270, pos, RetirarAcento(cc_valorgarantia)
'                    If (CC_FRANQUIA <> "R$0,00") Then
'                        PETOCX1.PrintText 350, pos, RetirarAcento("Franquia - ")
'                        PETOCX1.PrintText 380, pos, RetirarAcento(CC_FRANQUIA)
'                    End If
'                    If cc_carencia <> "0" Then
'                        PETOCX1.PrintText 460, pos, RetirarAcento("Carencia -")
'                        PETOCX1.PrintText 490, pos, RetirarAcento(cc_carencia & " dias")
'                    End If
'
'
'                    pos = pos - 10
'                Wend
'                End If
'            End If
'            bd_cob.Close
'
'            PETOCX1.SetFont "Arial", 7
'
'            '----------------------'
'            ' COBERTURA DOS FILHOS '
'            '----------------------'
'
'            'Para buscar da cobertura dos filhos
'            Call BuscarCoberturas("TP COMP FILHOS", CInt(subgrupo_id))   'Alterado em 03/04/03 - Abosco
'
'            CF_SequencialCoberturaFilhos = 0
'
'            If Not bd_cob.EOF Then
'                PETOCX1.PrintText 35, 450, RetirarAcento("Filhos                     Inclus�o Autom�tica")
'                pos = 441
'                While Not bd_cob.EOF
'
'                    'Efetua atribui��es dos campos
'                    Cf_NomeGarantia = IIf(Not IsNull(bd_cob!Nome), bd_cob!Nome, Space(60))
'                    Cf_NomeGarantia = Cf_NomeGarantia & " AT�"
'
'                    'Valor da Garantia
'                    temp = Val(Replace(rssegurado("val_capital_segurado"), ",", "."))
'                    temp2 = Replace(bd_cob!perc_basica, ",", ".")
'                    temp2 = Val(temp2)
'                    temp2 = (temp * temp2) / 100
'                    temp = Format(temp2, "###,###,###,##0.00")
'
'                    'Stefanini (Maur�cio), em 18/03/2005 para verificar se ultrapassa o limite de is e apresentar corretamente no PDF.
'                    If temp2 > Val(Replace(bd_cob!val_lim_max_is, ",", ".")) And bd_cob!val_lim_max_is > 0 Then
'                        temp = Format(Val(Replace(bd_cob!val_lim_max_is, ",", ".")), "###,###,###,##0.00")
'                    End If
'
'                    Cf_Valorgarantia = "R$ " & temp
'
'                    If Not IsNull(bd_cob!val_min_franquia) And bd_cob!val_lim_max_is <> ".00" Then
'                        CF_FRANQUIA = "R$ " + Format(Val(Replace(bd_cob!val_min_franquia, ".", ",")), "##,###,##0.00")
'                    Else
'                        CF_FRANQUIA = "R$0,00"
'                    End If
'                    cf_carencia = IIf(Not IsNull(bd_cob!carencia_dias), bd_cob!carencia_dias, 0)
'
'                    If Not IsNull(bd_cob!val_lim_max_is) And bd_cob!val_lim_max_is <> ".00" Then
'                        cf_limite = "R$ " + Format(Val(bd_cob!val_lim_max_is), "##,###,##0.00")
'                    Else
'                        cf_limite = Space(15)
'                    End If
'
'                    PETOCX1.SetFont "Arial", 6
'
'                    PETOCX1.PrintText 35, pos, RetirarAcento(Cf_NomeGarantia)
'                    PETOCX1.PrintText 270, pos, RetirarAcento(Cf_Valorgarantia)
'
'                    If (CF_FRANQUIA <> "R$0,00") Then
'                        PETOCX1.PrintText 350, pos, RetirarAcento("Franquia - ")
'                        PETOCX1.PrintText 380, pos, RetirarAcento(CF_FRANQUIA)
'                    End If
'                    If cf_carencia <> "0" Then
'                        PETOCX1.PrintText 460, pos, RetirarAcento("Carencia -")
'                        PETOCX1.PrintText 490, pos, RetirarAcento(cf_carencia & " dias")
'                    End If
'
'                    bd_cob.MoveNext
'                Wend
'            End If
'            bd_cob.Close
'
'
'            PETOCX1.SetFont "Arial", 7
'
'            '-----------------------'
'            ' DADOS DO BENEFICI�RIO '
'            '-----------------------'
'
'            'titular
'            texto_beneficiario = Trim(rssegurado("texto_beneficiario"))
'
'            'texto_beneficiario = "ANDRE LUIZ OLIVEIRA DA SILVA - ESPOSO - 33.33%  MARIA DE LOURDES MORAES DE SOUZA - MAE - 33.33%  HERALDO PESSANHA DE SOUZA - 33.34%"
'
'            If texto_beneficiario <> "" Then
'                PETOCX1.PrintText 40, 363, RetirarAcento("Titular")
'                comeco = 1
'                pos = 353
'                Do While Trim(texto_beneficiario) <> ""
'                    localper = InStr(1, texto_beneficiario, "%", vbTextCompare) + 2
'                    If localper = 2 Then localper = 60
'                    db_dadosbeneficiarioaux = Mid(texto_beneficiario, localper + 1)
'                    Texto = Mid(texto_beneficiario, 1, localper - 2)
'
'                    PETOCX1.PrintText 40, pos, RetirarAcento(Texto)
'
'                    texto_beneficiario = db_dadosbeneficiarioaux
'                    pos = pos - 10
'                Loop
'            End If
'
'            'Conjuge
'            texto_beneficiario = texto_beneficiario_conjuge
'
'            If texto_beneficiario <> "" Then
'                PETOCX1.PrintText 300, 363, RetirarAcento("Conjuge")
'                comeco = 1
'                pos = 353
'                Do While Trim(texto_beneficiario) <> ""
'                    localper = InStr(1, texto_beneficiario, "%", vbTextCompare) + 2
'                    db_dadosbeneficiarioaux = Mid(texto_beneficiario, localper + 1)
'                    Texto = Mid(texto_beneficiario, 1, localper - 2)
'
'                    PETOCX1.PrintText 300, pos, RetirarAcento(Texto)
'
'                    texto_beneficiario = db_dadosbeneficiarioaux
'                    pos = pos - 10
'                Loop
'            End If
'
'            Set rsconjuge = Nothing
'
'            'imprime corretor
'            Dim splitCorretor
'            Dim splitCodSusep
'            splitCorretor = Split(corretor_c, "|")
'            splitCodSusep = Split(susep_corretor, "|")
'
'            For x = 1 To UBound(splitCorretor)
'                'imprime primeiro corretor
'                If x = 1 Then
'                    PETOCX1.PrintText 40, 263, RetirarAcento(splitCorretor(x))
'                    PETOCX1.PrintText 450, 263, RetirarAcento(splitCodSusep(x))
'                'imprime primeiro corretor
'                ElseIf x = 2 Then
'                    PETOCX1.PrintText 40, 255, RetirarAcento(splitCorretor(x))
'                    PETOCX1.PrintText 450, 255, RetirarAcento(splitCodSusep(x))
'                End If
'            Next
'            'PETOCX1.PrintText 40, 256, RetirarAcento(corretor_c)
'            'PETOCX1.PrintText 450, 256, RetirarAcento(susep_corretor)
'
'            'imprime assistencia
'            SQL = "SELECT "
'            SQL = SQL & "tp.nome assistencia, "
'            SQL = SQL & "tp.ddd, "
'            SQL = SQL & "tp.telefone "
'            SQL = SQL & "From "
'            SQL = SQL & "       sub_grupo_assistencia_tb sg   WITH (NOLOCK)  "
'
'            SQL = SQL & " INNER JOIN  sub_grupo_apolice_tb"
'            SQL = SQL & "     ON sub_grupo_apolice_tb.apolice_id             =  SG.apolice_id"
'            SQL = SQL & "     AND sub_grupo_apolice_tb.ramo_id                =  SG.ramo_id"
'            SQL = SQL & "     AND sub_grupo_apolice_tb.sucursal_seguradora_id =  SG.sucursal_seguradora_id"
'            SQL = SQL & "     AND sub_grupo_apolice_tb.seguradora_cod_susep   =  SG.seguradora_cod_susep"
'            SQL = SQL & "     AND sub_grupo_apolice_tb.dt_inicio_vigencia_sbg =  SG.dt_inicio_vigencia_sbg"
'
'
'            SQL = SQL & "   inner join produto_tp_assistencia_tb pt   WITH (NOLOCK) "
'            SQL = SQL & "       on sg.tp_assistencia_id = pt.tp_assistencia_id "
'            SQL = SQL & "       and sg.produto_id = pt.produto_id "
'
'            SQL = SQL & "   inner join tp_assistencia_tb tp   WITH (NOLOCK) "
'            SQL = SQL & "       on pt.tp_assistencia_id = tp.tp_assistencia_id "
'
'            SQL = SQL & "Where "
'            SQL = SQL & "       sg.apolice_id               = " & vApoliceId & " "
'            SQL = SQL & " and   sg.sucursal_seguradora_id   = " & vSucursalId & " "
'            SQL = SQL & " and   sg.seguradora_cod_susep     = " & vCodSusep & " "
'            SQL = SQL & " and   sg.ramo_id                      = " & vRamoId & " "
'            SQL = SQL & " and   sg.sub_grupo_id         = " & subgrupo_id & " "
'            SQL = SQL & " and   sg.dt_fim_assist_sbg is NULL "
'
'            Set bd = rdoCn1.OpenResultset(SQL)
'
'            If Not bd.EOF Then
'
'                s_assistencia = "Condi��es da Assist�ncia encontram-se em poder do Estipulante"
'                S_TelefoneAssistencia = "0800-161660" & Space(4)
'
'                Do While Not bd.EOF
'
'                    i = i + 1
'                    If i > 3 Then Exit Do
'
'                    Select Case i
'                        Case 1
'                            S_assistencia1 = IIf(Not IsNull(bd!Assistencia), bd!Assistencia, "")
'                        Case 2
'                            S_assistencia2 = IIf(Not IsNull(bd!Assistencia), bd!Assistencia, "")
'                        Case 3
'                            S_assistencia3 = IIf(Not IsNull(bd!Assistencia), bd!Assistencia, "")
'                    End Select
'
'                Loop
'            Else
'                s_assistencia = ""
'                S_TelefoneAssistencia = ""
'                S_assistencia1 = ""
'                S_assistencia2 = ""
'                S_assistencia3 = ""
'            End If
'            bd.Close
'
'            If s_assistencia <> "" Then
'                s_assistencia = "Condi��es da Assist�ncia encontram-se em poder do Estipulante" & "      Telefone Assistencia - " & S_TelefoneAssistencia
'
'                    PETOCX1.PrintText 70, 245, RetirarAcento(s_assistencia)
'                    PETOCX1.PrintText 40, 230, RetirarAcento(S_assistencia1)
'                    PETOCX1.PrintText 40, 220, RetirarAcento(S_assistencia2)
'                    PETOCX1.PrintText 40, 210, RetirarAcento(S_assistencia3)
'            End If
'
'
'
'                If s_dataemissao = "" Then
'                    s_dataemissao = Now
'                    If Mid(s_dataemissao, 2, 1) = "/" Then s_dataemissao = "0" & s_dataemissao
'                    If Mid(s_dataemissao, 5, 1) = "/" Then s_dataemissao = Mid(s_dataemissao, 1, 3) & "0" & Mid(s_dataemissao, 4, 6)
'                End If
'
'                PETOCX1.PrintText 460, 205, RetirarAcento(s_dataemissao)
'            End If
'
'                'Imprime Observa�oes
'                'Para a observa��o1, temporariamente, temos a indica��o do subgrupo e sua descri��o -- Kylme 05/05/2003
'                S_observacao1 = Space(60)
'                SQL = " select sub_grupo_id, nome" & _
                 '                " from sub_grupo_apolice_tb " & _
                 '                " Where     apolice_id      = " & vApoliceId & _
                 '                "       and ramo_id         = " & vRamoId & _
                 '                "       and sub_grupo_id    = " & subgrupo_id
'                Set bd = rdoCn1.OpenResultset(SQL)
'                If Not bd.EOF And bd!Nome <> ED_NomeSubEstipulante Then S_observacao1 = "Subgrupo = " & bd!sub_grupo_id & " - " & bd!Nome
'
'                'Este select n�o est� contemplando:
'                'observa�ao1,observa�ao2,observa�ao3 -> n�o foi inclu�do do modelo, o programa est� enviando brancos
'                S_observacao2 = Space(60)
'                S_observacao3 = Space(60)
'
'                PETOCX1.PrintText 40, 175, RetirarAcento(S_observacao1)
'                PETOCX1.PrintText 40, 165, RetirarAcento(S_observacao2)
'                PETOCX1.PrintText 40, 155, RetirarAcento(S_observacao3)
'
'                PETOCX1.AddInternalLinkBookmark rssegurado("nome"), a, 0, 840
'               a = a + 1
'            End If
'            rssegurado.MoveNext
'
'       Loop
'       PETOCX1.GotoPreviousBookmarkLevel
'    End If
'    numrows = numrows - 1
'Loop
'PETOCX1.GotoPreviousBookmarkLevel
'Set rsAux = Nothing
'Visualizacao_Doc = False
'
''Fun��o n�o ser� mais ultilizada
'
'Exit Sub
'Resume
'Erro:
'    MsgBox ("Ocorreu o seguinte erro: " & Err.Description & " - SQL " & SQL)
'    TrataErroGeral "Gerar_Visu_Subgrupos", Me.name
'    Unload Me
'End Sub

'Private Sub Gerar_visu_vidas()
'
''****************************************************************
''*  Fun��o n�o ser� mais ultilizada                             *
''*  Demanda Evolutiva: 176384 - AFONSO - G&P                    *
''*  Novo processo, ser� eliminado o sistema de cria��o de PDF   *
''****************************************************************
'Dim corretor_c As String
'
'On Error GoTo Erro
'
'numrows2 = MSFlexGrid2(0).Rows
'numrows = 1
'PETOCX1.AddInternalLinkBookmark "Apolice - " & vApoliceId, 1, 0, 840
'PETOCX1.GotoNextBookmarkLevel
'PETOCX1.AddInternalLinkBookmark "Subgrupo - " & Text1(1).Text, 1, 0, 840
'PETOCX1.GotoNextBookmarkLevel
'a = 1
'
''busca os dados do certificado
'
''nome
'parametro = "SEGA8017 CERTIFICADO RAMO " & vRamoId
'SQL = "exec vl_parametro_sps '" & parametro & "'"
'Set rs = rdocn.OpenResultset(SQL)
'NomeCertificado = rs(0).Value
'Set rs = Nothing
'
''num susep
'SQL = "exec num_proc_susep_apolice_sps " & vApoliceId & ", " & vRamoId
'Set rs = rdocn.OpenResultset(SQL)
'proc_susep = rs(0).Value
'Set rs = Nothing
'If IsNull(proc_susep) Then
'proc_susep = "00.000000/00-0"
'End If
''nome do ramo
'SQL = "select nome from ramo_tb  WITH (NOLOCK)  where ramo_id = " & vRamoId
'Set rs = rdocn.OpenResultset(SQL)
'NomeRamo = vRamoId & " - " & rs(0).Value
'Set rs = Nothing
'
''nome do estipulante
'SQL = "select pr.proposta_id, cl.nome, cl.cpf_cnpj, prop_cliente_id from cliente_tb cl  WITH (NOLOCK) , proposta_tb pr  WITH (NOLOCK) , apolice_tb ap   WITH (NOLOCK)  where "
'SQL = SQL & " ap.apolice_id = " & vApoliceId & " and ap.ramo_id = " & vRamoId & " and pr.proposta_id = ap.proposta_id "
'SQL = SQL & " and cl.cliente_id = prop_cliente_id"
'Set rs = rdocn.OpenResultset(SQL)
'Estipulante = rs(1).Value
'CPF_cnpj_estipulante = MasCGC(rs(2).Value)
'proposta = rs("proposta_id")
'Set rs = Nothing
'
'        lugar = InStr(1, Text1(1).Text, " ", vbTextCompare)
'        subgrupo_id = Mid(Text1(1).Text, 1, lugar - 1)
'
'        Texto2 = proposta & ", 0 , null, " & cUserName & ",c, 5, i,'Sub Grupo - Por Vidas'," & subgrupo_id & ",'l', null, null," & cUserName & ",'" & Format(Date, "mm/dd/yyyy") & " " & Time & "', 'segp0223->certificado->*.pdf'"
'        SQL = "exec evento_seguros_db..evento_impressao_spi " & Texto2
'        Set rsimpressao = rdocn.OpenResultset(SQL)
'        num_solicitacao = rsimpressao(0).Value
'        Set rsimpressao = Nothing
'
'
'        'ALTERA��O - DIEGO GALIZONI CAVERSAN - G&P - 27/12/2007
'        'Inclus�o do evento 81034 pois as propostas inseridas na evento_impressao_tb dessa maneira n�o ser�o impressas.
'
'        SQL = "exec evento_seguros_db..evento_spi " & proposta & ", " & sPropostaBB & ",null, null, null, null, null, null, null, null, null, null, " & cUserName & ", '" & Format(Now, "yyyymmdd") & "'" & ",81034"
'        rdocn.Execute (SQL)
'
'
'Do While numrows < numrows2
'    If MSFlexGrid2(0).TextMatrix(numrows, 0) = "X" Then
'       gerou = True
'
'       SQL = "exec dados_segurado_sps " & MSFlexGrid2(0).TextMatrix(numrows, 5) & " , " & vRamoId & ", " & vApoliceId & ", " & subgrupo_id
'       Set rssegurado = rdocn.OpenResultset(SQL)
'       SQL = "select cl.nome, cl.cpf_cnpj "
'       SQL = SQL & "from    cliente_tb cl                "
'       SQL = SQL & "     INNER JOIN    representacao_sub_grupo_tb rsbg "
'       SQL = SQL & "        ON cl.cliente_id = rsbg.est_cliente_id"
'       SQL = SQL & "  where rsbg.apolice_id     = " & vApoliceId & " and "
'       SQL = SQL & "        rsbg.ramo_id        = " & vRamoId & " and  "
'       SQL = SQL & "        rsbg.sub_grupo_id   = " & subgrupo_id & "   "
'
'       Set rs = rdoCn1.OpenResultset(SQL)
'       subEstipulante = rs(0).Value
'       CPF_cnpj_subestipulante = MasCGC(rs(1).Value)
'       prop_cliente_id = rssegurado("prop_cliente_id")
'       Set rs = Nothing
'
'       SQL = "select cor.nome, cor.corretor_susep "
'       SQL = SQL & "  from corretagem_sub_grupo_tb csgb"
'       SQL = SQL & "  INNER JOIN  corretor_tb cor "
'       SQL = SQL & "    ON cor.corretor_id = csgb.corretor_id "
'       SQL = SQL & "  where csgb.apolice_id     = " & vApoliceId & "        and "
'       SQL = SQL & "  csgb.ramo_id              = " & vRamoId & "            and "
'       SQL = SQL & "  csgb.sub_grupo_id         = " & subgrupo_id & "      "
'
'       'Alterado Rafael Oshiro 15/10/04
'       'Captura todos os corretores e separa por "|"
'       corretor_c = ""
'       Set rs = rdoCn1.OpenResultset(SQL)
'       If Not rs.EOF Then
'          While Not rs.EOF
'            corretor_c = corretor_c & "|" & rs("nome")
'            susep_corretor = susep_corretor & "|" & MasCorrSusep(IIf(IsNull(rs("corretor_susep")), 0, rs("corretor_susep")))
'            rs.MoveNext
'          Wend
'       End If
'
'       'corretor_c = rs(0).Value
'       'susep_corretor = MasCorrSusep(IIf(IsNull(rs("corretor_susep")), 0, rs("corretor_susep")))
'       Set rs = Nothing
'
'        'Layout do Subgrupo
'        SQL = "SELECT"
'        SQL = SQL & " campo_layout_vida_sub_grupo_tb.sucursal_seguradora_id,"
'        SQL = SQL & " campo_layout_vida_sub_grupo_tb.seguradora_cod_susep,"
'        SQL = SQL & " campo_layout_vida_sub_grupo_tb.ramo_id,"
'        SQL = SQL & " campo_layout_vida_sub_grupo_tb.apolice_id,"
'        SQL = SQL & " campo_layout_vida_sub_grupo_tb.sub_grupo_id,"
'        SQL = SQL & " campo_layout_vida_sub_grupo_tb.titulo_campo_1,"
'        SQL = SQL & " campo_layout_vida_sub_grupo_tb.titulo_campo_2,"
'        SQL = SQL & " campo_layout_vida_sub_grupo_tb.titulo_campo_3,"
'        SQL = SQL & " campo_layout_vida_sub_grupo_tb.titulo_campo_4,"
'        SQL = SQL & " campo_layout_vida_sub_grupo_tb.titulo_campo_5"
'        SQL = SQL & " FROM"
'        SQL = SQL & "       campo_layout_vida_sub_grupo_tb   WITH (NOLOCK) "
'
'        SQL = SQL & " INNER JOIN  sub_grupo_apolice_tb"
'        SQL = SQL & "     ON sub_grupo_apolice_tb.apolice_id              =  campo_layout_vida_sub_grupo_tb.apolice_id"
'        SQL = SQL & "     AND sub_grupo_apolice_tb.ramo_id                =  campo_layout_vida_sub_grupo_tb.ramo_id"
'        SQL = SQL & "     AND sub_grupo_apolice_tb.sucursal_seguradora_id =  campo_layout_vida_sub_grupo_tb.sucursal_seguradora_id"
'        SQL = SQL & "     AND sub_grupo_apolice_tb.seguradora_cod_susep   =  campo_layout_vida_sub_grupo_tb.seguradora_cod_susep"
'        SQL = SQL & "     AND sub_grupo_apolice_tb.dt_inicio_vigencia_sbg =  campo_layout_vida_sub_grupo_tb.dt_inicio_vigencia_sbg"
'
'
'        SQL = SQL & " WHERE"
'        SQL = SQL & "   campo_layout_vida_sub_grupo_tb.apolice_id       = " & vApoliceId         'Ap�lice digitada pelo usu�rio
'        SQL = SQL & "   AND campo_layout_vida_sub_grupo_tb.sub_grupo_id = " & subgrupo_id  'Sub-grupo digitado pelo usu�rio
'        SQL = SQL & "   AND campo_layout_vida_sub_grupo_tb.ramo_id      = " & vRamoId           'ramo digitado pelo usu�rio
'        SQL = SQL & " order by campo_layout_vida_sub_grupo_tb.sub_grupo_id"
'        Set rs = rdoCn2.OpenResultset(SQL)
'        If Not rs.EOF Then
'            titulo1 = IIf(IsNull(rs("titulo_campo_1")), "", Trim(rs("titulo_campo_1")))
'            valortitulo1 = IIf(IsNull(rs("valor_1")), "", Trim(rs("valor_1")))
'            titulo2 = IIf(IsNull(rs("titulo_campo_2")), "", Trim(rs("titulo_campo_2")))
'            valortitulo2 = IIf(IsNull(rs("valor_2")), "", Trim(rs("valor_2")))
'            titulo3 = IIf(IsNull(rs("titulo_campo_3")), "", Trim(rs("titulo_campo_3")))
'            valortitulo3 = IIf(IsNull(rs("valor_3")), "", Trim(rs("valor_3")))
'            titulo4 = IIf(IsNull(rs("titulo_campo_4")), "", Trim(rs("titulo_campo_4")))
'            valortitulo4 = IIf(IsNull(rs("valor_4")), "", Trim(rs("valor_4")))
'            titulo5 = IIf(IsNull(rs("titulo_campo_5")), "", Trim(rs("titulo_campo_5")))
'            valortitulo5 = IIf(IsNull(rs("valor_5")), "", Trim(rs("valor_5")))
'        End If
'        Set rs = Nothing
'
'        Do While Not rssegurado.EOF
'            vSucursalId = rssegurado("sucursal")
'            vCodSusep = rssegurado("cod_susep")
'            'verifica para ver se imprime segurado ou n�o dependendo do check2 (somente ativas)
'            If Check2(1).Value = 1 And rssegurado("situacao") = "Ativa" Then
'                gerar = True
'            ElseIf Check2(1).Value = 0 Then
'                gerar = True
'            Else
'                gerar = False
'            End If
'
'            If gerar Then
'
'
'                SQL = "exec  evento_seguros_db..impressao_vidas_spi " & num_solicitacao & "," & prop_cliente_id
'                rdocn.Execute (SQL)
'
'
'                If a <> 1 Then PETOCX1.NewPage
'                PETOCX1.PrintLogo
'
'                PETOCX1.SetFont "Arial", 9
'                'imprime dados do certificado
'                PETOCX1.PrintText 230, 818, RetirarAcento(NomeCertificado)
'                PETOCX1.PrintText 230, 807, RetirarAcento("Processo SUSEP : VG - " & proc_susep)
'
'                PETOCX1.SetFont "Arial", 7
'
'                'imprime apolice
'                PETOCX1.PrintText 52, 765, RetirarAcento(Text1(0).Text)
'
'                'imprime certificado
'                SQL = "exec rel_certif_sps " & vApoliceId & ", " & rssegurado("sucursal") & ", " & rssegurado("cod_susep") & ", " & vRamoId & "," & subgrupo_id & ", " & rssegurado("prop_cliente_id") & ", '" & Usuario & "', null"
'                Set rs = rdoCn1.OpenResultset(SQL)
'                certificado_id = Format(rs(0).Value, "000000000")
'                Set rs = Nothing
'
'
'            If rssegurado("situacao") = "Ativa" Then
'                'Data Emiss�o Certificado
'                SQL = ""
'                SQL = SQL & "Select "
'                SQL = SQL & "Dt_Emissao "
'                SQL = SQL & "From "
'                SQL = SQL & "certificado_tb "
'                SQL = SQL & "Where "
'                SQL = SQL & "apolice_id = " & vApoliceId & " "
'                SQL = SQL & "and sucursal_seguradora_id = " & vSucursalId & " "
'                SQL = SQL & "and seguradora_cod_susep = " & vCodSusep & " "
'                SQL = SQL & "and ramo_id = " & vRamoId & " "
'                SQL = SQL & "and certificado_id = " & certificado_id & " "
'                SQL = SQL & "and dt_fim_vigencia is NULL "
'
'                Set bd = rdoCn1.OpenResultset(SQL)
'                If Not bd.EOF Then
'                If Not IsNull(bd!dt_emissao) Then
'                s_dataemissao = Format(bd!dt_emissao, "dd/mm/yyyy")
'                primeiravia = "n"
'                Else
'                primeiravia = "s"
'                SQL = "exec certificado_emissao_spu " & vCodSusep & "," & vSucursalId & "," & vRamoId & "," & vApoliceId & "," & proposta & "," & certificado_id & ",'" & Format(Date, "mm/dd/yyyy") & " " & Time & "'," & cUserName
'                rdoCn1.OpenResultset SQL
'                SQL = ""
'                SQL = SQL & "Select "
'                SQL = SQL & "Dt_Emissao "
'                SQL = SQL & "From "
'                SQL = SQL & "certificado_tb "
'                SQL = SQL & "Where "
'                SQL = SQL & "apolice_id = " & vApoliceId & " "
'                SQL = SQL & "and sucursal_seguradora_id = " & vSucursalId & " "
'                SQL = SQL & "and seguradora_cod_susep = " & vCodSusep & " "
'                SQL = SQL & "and ramo_id = " & vRamoId & " "
'                SQL = SQL & "and certificado_id = " & certificado_id & " "
'                SQL = SQL & "and dt_fim_vigencia is NULL "
'                Set bd = rdoCn1.OpenResultset(SQL)
'                s_dataemissao = IIf(Not IsNull(bd!dt_emissao), Format(bd!dt_emissao, "dd/mm/yyyy"), Date)
'                End If
'                End If
'                bd.Close
'
'
'
'
'                'rdoCn1.Close
'                PETOCX1.PrintText 152, 765, RetirarAcento(certificado_id)
'
'                'imprime ramo
'                PETOCX1.PrintText 232, 765, RetirarAcento(NomeRamo)
'
'                'imprime estipulante
'                PETOCX1.PrintText 52, 736, RetirarAcento(Estipulante)
'                PETOCX1.PrintText 440, 736, RetirarAcento(CPF_cnpj_estipulante)
'
'                'imprime subestipulante
'                BuscaDadosSUBEstipulante (subgrupo_id)
'                If ED_NomeSubEstipulante <> Estipulante Then
'                    PETOCX1.PrintText 52, 710, RetirarAcento(ED_NomeSubEstipulante)
'                    PETOCX1.PrintText 440, 710, RetirarAcento(ED_CNPJSubEstipulante)
'                End If
'
'                'imprime dados do segurado
'                PETOCX1.PrintText 40, 675, RetirarAcento("Titular: ")
'                PETOCX1.PrintText 120, 675, RetirarAcento(rssegurado("nome"))
'                PETOCX1.PrintText 40, 665, RetirarAcento("Data de Nascimento: ")
'                PETOCX1.PrintText 120, 665, RetirarAcento(rssegurado("dt_nascimento"))
'                PETOCX1.PrintText 40, 655, RetirarAcento("CPF: ")
'                PETOCX1.PrintText 120, 655, RetirarAcento(MasCPF(rssegurado("cpf")))
'                PETOCX1.SetFont "Arial", 6
'                'imprime titulo e dados do segurado
'                Texto = ""
'                If Not IsNull(titulo1) And titulo1 <> "" Then Texto = titulo1 & " - " & valortitulo1 & "    "
'                If Not IsNull(titulo1) And titulo1 <> "" Then Texto = Texto & titulo2 & " - " & valortitulo2 & "    "
'                If Not IsNull(titulo1) And titulo1 <> "" Then Texto = Texto & titulo2 & " - " & valortitulo3 & "    "
'                If Not IsNull(titulo1) And titulo1 <> "" Then Texto = Texto & titulo4 & " - " & valortitulo4 & "    "
'                If Not IsNull(titulo1) And titulo1 <> "" Then Texto = Texto & titulo5 & " - " & valortitulo5
'
'                PETOCX1.PrintText 40, 687, RetirarAcento(Texto)
'
'                PETOCX1.SetFont "Arial", 7
'                '------------------------'
'                '  COBERTURA DO TITULAR  '
'                '------------------------'
'
'                'Para buscar da cobertura do titular
'                'vparametro = "TP COMP TITULAR"
'                'BuscaDadosCob
'                Call BuscarCoberturas("TP COMP TITULAR", CInt(subgrupo_id))
'
'                CT_SequencialCoberturaTitular = 0
'
'                If Not bd_cob.EOF Then
'                    PETOCX1.SetFont "Arial", 7
'                    PETOCX1.PrintText 35, 585, RetirarAcento("Titular")
'                    PETOCX1.PrintText 120, 585, RetirarAcento("Inicio de Vig�ncia")
'                    datavig = rssegurado("dt_inicio_vigencia_sbg")
'                    If Mid(datavig, 2, 1) = "/" Then datavig = "0" & datavig
'                    If Mid(datavig, 5, 1) = "/" Then datavig = Mid(datavig, 1, 3) & "0" & Mid(datavig, 4, 6)
'                    PETOCX1.PrintText 190, 585, RetirarAcento(datavig)
'
'                    PETOCX1.SetFont "Arial", 6
'                    pos = 575
'                    While Not bd_cob.EOF
'
'                    'Efetua atribui��es dos campos
'                    CT_NomeGarantia = IIf(Not IsNull(bd_cob!Nome), bd_cob!Nome, Space(60))
'                    If UCase(CT_NomeGarantia) = UCase("INVALIDEZ PERMANENTE TOTAL OU PARCIAL POR ACIDENTE - IPA") Then CT_NomeGarantia = CT_NomeGarantia & " AT�"
'
'                    'Valor da Garantia
'                    If UCase(bd_cob!class_tp_cobertura) = "B" Then 'Se for cobertura b�sica do titular
'                        If Not IsNull(rssegurado("val_capital_segurado")) Then
'                            CT_Valorgarantia = "R$ " + Format(Replace(rssegurado("val_capital_segurado"), ".", ","), "##,###,##0.00")
'                        Else
'                            CT_Valorgarantia = Space(15)
'                        End If
'                    Else 'Se for outra cobertura, aplicar o percentual sobre a cobertura b�sica
'                        If Not IsNull(rssegurado("val_capital_segurado")) Then
'                            CT_Valorgarantia = (Replace(rssegurado("val_capital_segurado"), ".", ",") * Replace(bd_cob!perc_basica, ".", ",")) / 100
'                            CT_Valorgarantia = "R$ " + Format(Val(CT_Valorgarantia), "##,###,##0.00")
'                        Else
'                            CT_Valorgarantia = Space(15)
'                        End If
'                    End If
'                    If Not IsNull(bd_cob!val_min_franquia) Then
'                        Ct_FRANQUIA = "R$ " + Format(Val(Replace(bd_cob!val_min_franquia, ".", ",")), "##,###,##0.00")
'                    Else
'                        Ct_FRANQUIA = "R$0,00"
'                    End If
'                    ct_carencia = IIf(Not IsNull(bd_cob!carencia_dias), bd_cob!carencia_dias, 0)
'
'                    PETOCX1.SetFont "Arial", 6
'
'                    PETOCX1.PrintText 35, pos, RetirarAcento(CT_NomeGarantia)
'                    PETOCX1.PrintText 270, pos, RetirarAcento(CT_Valorgarantia)
'                    If (Ct_FRANQUIA <> "R$0,00") Then
'                        PETOCX1.PrintText 350, pos, RetirarAcento("Franquia - ")
'                        PETOCX1.PrintText 380, pos, RetirarAcento(Ct_FRANQUIA)
'                    End If
'
'                    If ct_carencia <> "0" Then
'                        PETOCX1.PrintText 460, pos, RetirarAcento("Carencia -")
'                        PETOCX1.PrintText 490, pos, RetirarAcento(ct_carencia & " dias")
'                    End If
'
'                    bd_cob.MoveNext
'                    pos = pos - 10
'                Wend
'            End If
'            bd_cob.Close
'
'            texto_beneficiario_conjuge = ""
'
'            'imprime dados do conjuge
'            SQL = "exec dados_conjuge_sps " & vApoliceId & "," & vRamoId & "," & subgrupo_id & "," & rssegurado("prop_cliente_id")
'
'            'SQL = "EXEC DADOS_CONJUGE_SPS 13220, 93,1,3949530"
'            Set rsconjuge = rdoCn1.OpenResultset(SQL)
'            If Not rsconjuge.EOF Or Conjuge Then
'                PETOCX1.SetFont "Arial", 7
'                PETOCX1.PrintText 35, 640, RetirarAcento("Dados do C�njuge")
'                PETOCX1.PrintText 40, 630, RetirarAcento("Nome: ")
'                c_nomeConjuge = rsconjuge("nome")
'                PETOCX1.PrintText 120, 630, RetirarAcento(rsconjuge("nome"))
'                PETOCX1.PrintText 40, 620, RetirarAcento("Data de Nascimento: ")
'                PETOCX1.PrintText 120, 620, RetirarAcento(rsconjuge("dt_nascimento"))
'                PETOCX1.PrintText 40, 610, RetirarAcento("CPF: ")
'                PETOCX1.PrintText 120, 610, RetirarAcento(MasCPF(rsconjuge("cpf")))
'                texto_beneficiario_conjuge = RetirarAcento(IIf(IsNull(rsconjuge("texto_beneficiario")), "", rsconjuge("texto_beneficiario")))
'
'                Automatico = False
'
'            End If
'
'            '----------------------'
'            ' COBERTURA DO C�NJUGE '
'            '----------------------'
'
'            'Para buscar coberturas do conjuge
'            'vparametro = "TP COMP CONJUGE"
'            'BuscaDadosCob
'            'Call BuscarCoberturas("TP COMP CONJUGE", CInt(SUBGRUPO_ID))     'Alterado em 03/04/03 - Abosco
'            Call BuscarCoberturas("TP COMP CONJUGE", 1)     'Alterado em 03/04/03 - Abosco
'
'            SQL = "select tp_clausula_conjuge "
'            SQL = SQL & " from sub_grupo_apolice_tb "
'            SQL = SQL & " where apolice_id = " & vApoliceId & " and "
'            SQL = SQL & " ramo_id = " & vRamoId & " and "
'            SQL = SQL & " sub_grupo_id = " & subgrupo_id
'
'            rs = rdoCn2.OpenResultset(SQL)
'            If rs("tp_clausula_conjuge") = "A" Then
'                Automatico = True
'            End If
'            Set rs = Nothing
'
'            CC_SequencialCoberturaConjuge = 0
'
'
'            'If Not bd_cob.EOF Then
'            If (Not bd_cob.EOF) And (Trim(c_nomeConjuge) <> "") Or Automatico Then   'Alterado em 03/04/03
'                PETOCX1.SetFont "Arial", 7
'
'                If Automatico Then
'                    PETOCX1.PrintText 35, 515, RetirarAcento("C�njuge                 Inclus�o Autom�tica")
'                Else
'                    If Not rsconjuge.EOF Then
'                    PETOCX1.PrintText 35, 515, RetirarAcento("C�njuge")
'                    PETOCX1.PrintText 120, 515, RetirarAcento("Inicio de Vig�ncia")
'                    PETOCX1.PrintText 190, 515, RetirarAcento(Format(rsconjuge("dt_inicio_vigencia_sbg"), "dd/mm/yyyy"))
'                    End If
'                End If
'
'                pos = 505
'                PETOCX1.SetFont "Arial", 6
'                If Not rsconjuge.EOF Or Automatico Then
'                While Not bd_cob.EOF
'
'                   'Efetua atribui��es dos campos
'                    CC_NomeGarantia = IIf(Not IsNull(bd_cob!Nome), bd_cob!Nome, Space(60))
'                    If UCase(CC_NomeGarantia) = UCase("INVALIDEZ PERMANENTE TOTAL OU PARCIAL POR ACIDENTE - IPA") Then CC_NomeGarantia = CC_NomeGarantia & " AT�"
'                    'Valor da Garantia
'                    temp = Val(Replace(rssegurado("val_capital_segurado"), ",", "."))
'                    temp2 = Replace(bd_cob!perc_basica, ",", ".")
'                    temp2 = Val(temp2)
'                    temp2 = (temp * temp2) / 100
'                    temp = Format(temp2, "###,###,###,##0.00")
'
'                    cc_valorgarantia = "R$ " & temp
'
'                    If Not IsNull(bd_cob!val_min_franquia) And (bd_cob!val_lim_max_is) <> ".00" Then
'                        CC_FRANQUIA = "R$ " + Format(Val(bd_cob!val_min_franquia), "##,###,##0.00")
'                    Else
'                        CC_FRANQUIA = "R$0,00"
'                    End If
'                    cc_carencia = IIf(Not IsNull(bd_cob!carencia_dias), bd_cob!carencia_dias, 0)
'
'                    bd_cob.MoveNext
'
'                    PETOCX1.SetFont "Arial", 6
'
'                    PETOCX1.PrintText 35, pos, RetirarAcento(CC_NomeGarantia)
'                    PETOCX1.PrintText 270, pos, RetirarAcento(cc_valorgarantia)
'                    If (CC_FRANQUIA <> "R$0,00") Then
'                        PETOCX1.PrintText 350, pos, RetirarAcento("Franquia - ")
'                        PETOCX1.PrintText 380, pos, RetirarAcento(CC_FRANQUIA)
'                    End If
'                    If cc_carencia <> "0" Then
'                        PETOCX1.PrintText 460, pos, RetirarAcento("Carencia -")
'                        PETOCX1.PrintText 490, pos, RetirarAcento(cc_carencia & " dias")
'                    End If
'
'                    pos = pos - 10
'                Wend
'                End If
'            End If
'            bd_cob.Close
'
'            PETOCX1.SetFont "Arial", 7
'
'            '----------------------'
'            ' COBERTURA DOS FILHOS '
'            '----------------------'
'
'            'Para buscar da cobertura dos filhos
'            'vparametro = "TP COMP FILHOS"
'            'BuscaDadosCob
'
'            Call BuscarCoberturas("TP COMP FILHOS", CInt(subgrupo_id))   'Alterado em 03/04/03 - Abosco
'
'            CF_SequencialCoberturaFilhos = 0
'
'            If Not bd_cob.EOF Then
'                PETOCX1.PrintText 35, 450, RetirarAcento("Filhos                     Inclus�o Autom�tica")
'                pos = 441
'                PETOCX1.SetFont "Arial", 6
'                While Not bd_cob.EOF
'
'                    'Efetua atribui��es dos campos
'                    Cf_NomeGarantia = IIf(Not IsNull(bd_cob!Nome), bd_cob!Nome, Space(60))
'                    Cf_NomeGarantia = Cf_NomeGarantia & " AT�"
'                    'Valor da Garantia
'                    temp = Val(Replace(rssegurado("val_capital_segurado"), ",", "."))
'                    temp2 = Replace(bd_cob!perc_basica, ",", ".")
'                    temp2 = Val(temp2)
'                    temp2 = (temp * temp2) / 100
'                    temp = Format(temp2, "###,###,###,##0.00")
'
'                    'Stefanini (Maur�cio), em 18/03/2005 para verificar se ultrapassa o limite de is e apresentar corretamente no PDF.
'                    If temp2 > Val(Replace(bd_cob!val_lim_max_is, ",", ".")) And Val(Replace(bd_cob!val_lim_max_is, ",", ".")) > 0 Then
'                        temp = Format(Val(Replace(bd_cob!val_lim_max_is, ",", ".")), "###,###,###,##0.00")
'                    End If
'
'                    Cf_Valorgarantia = "R$ " & temp
'
'                    If Not IsNull(bd_cob!val_min_franquia) And bd_cob!val_lim_max_is <> ".00" Then
'                        CF_FRANQUIA = "R$ " + Format(Val(Replace(bd_cob!val_min_franquia, ".", ",")), "##,###,##0.00")
'                    Else
'                        CF_FRANQUIA = "R$0,00"
'                    End If
'                    cf_carencia = IIf(Not IsNull(bd_cob!carencia_dias), bd_cob!carencia_dias, 0)
'
'                    If Not IsNull(bd_cob!val_lim_max_is) And bd_cob!val_lim_max_is <> ".00" Then
'                        cf_limite = "R$ " + Format(Val(bd_cob!val_lim_max_is), "##,###,##0.00")
'                    Else
'                        cf_limite = Space(15)
'                    End If
'
'                    PETOCX1.SetFont "Arial", 6
'
'                    PETOCX1.PrintText 35, pos, RetirarAcento(Cf_NomeGarantia)
'                    PETOCX1.PrintText 270, pos, RetirarAcento(Cf_Valorgarantia)
'
'                    If (CF_FRANQUIA <> "R$0,00") Then
'                        PETOCX1.PrintText 350, pos, RetirarAcento("Franquia - ")
'                        PETOCX1.PrintText 380, pos, RetirarAcento(CF_FRANQUIA)
'                    End If
'                    If cf_carencia <> "0" Then
'                        PETOCX1.PrintText 460, pos, RetirarAcento("Carencia -")
'                        PETOCX1.PrintText 490, pos, RetirarAcento(cf_carencia & " dias")
'                    End If
'
'                    bd_cob.MoveNext
'                Wend
'            End If
'            bd_cob.Close
'
'            PETOCX1.SetFont "Arial", 7
'
'            '-----------------------'
'            ' DADOS DO BENEFICI�RIO '
'            '-----------------------'
'
'            'titular
'            texto_beneficiario = Trim(rssegurado("texto_beneficiario"))
'
'            'texto_beneficiario = "ANDRE LUIZ OLIVEIRA DA SILVA - ESPOSO - 33.33%  MARIA DE LOURDES MORAES DE SOUZA - MAE - 33.33%  HERALDO PESSANHA DE SOUZA - 33.34%"
'
'            If texto_beneficiario <> "" Then
'                PETOCX1.PrintText 40, 363, RetirarAcento("Titular")
'                comeco = 1
'                pos = 353
'                Do While Trim(texto_beneficiario) <> ""
'                    localper = InStr(1, texto_beneficiario, "%", vbTextCompare) + 2
'                    If localper = 2 Then localper = 60
'                    db_dadosbeneficiarioaux = Mid(texto_beneficiario, localper + 1)
'                    Texto = Mid(texto_beneficiario, 1, localper - 2)
'
'                    PETOCX1.PrintText 40, pos, RetirarAcento(Texto)
'
'                    texto_beneficiario = db_dadosbeneficiarioaux
'                    pos = pos - 10
'                Loop
'            End If
'
'            'Conjuge
'            texto_beneficiario = texto_beneficiario_conjuge
'
'            If texto_beneficiario <> "" Then
'                PETOCX1.PrintText 300, 363, RetirarAcento("Conjuge")
'                comeco = 1
'                pos = 353
'                Do While Trim(texto_beneficiario) <> ""
'                    localper = InStr(1, texto_beneficiario, "%", vbTextCompare) + 2
'                    db_dadosbeneficiarioaux = Mid(texto_beneficiario, localper + 1)
'                    Texto = Mid(texto_beneficiario, 1, localper - 2)
'
'                    PETOCX1.PrintText 300, pos, RetirarAcento(Texto)
'
'                    texto_beneficiario = db_dadosbeneficiarioaux
'                    pos = pos - 10
'                Loop
'            End If
'
'            Set rsconjuge = Nothing
'
'            'imprime corretor
'
'            'Rafael Oshiro 15/10/04
'            'Imprime dois corretores
'            Dim splitCorretor
'            Dim splitCodSusep
'            splitCorretor = Split(corretor_c, "|")
'            splitCodSusep = Split(susep_corretor, "|")
'
'            For x = 1 To UBound(splitCorretor)
'                'imprime primeiro corretor
'                If x = 1 Then
'                    PETOCX1.PrintText 40, 263, RetirarAcento(splitCorretor(x))
'                    PETOCX1.PrintText 450, 263, RetirarAcento(splitCodSusep(x))
'                'imprime primeiro corretor
'                ElseIf x = 2 Then
'                    PETOCX1.PrintText 40, 255, RetirarAcento(splitCorretor(x))
'                    PETOCX1.PrintText 450, 255, RetirarAcento(splitCodSusep(x))
'                End If
'            Next
'
'            'PETOCX1.PrintText 40, 256, RetirarAcento(corretor_c)
'            'PETOCX1.PrintText 450, 256, RetirarAcento(susep_corretor)
'
'
'            'imprime assistencia
'            SQL = "SELECT "
'            SQL = SQL & "tp.nome assistencia, "
'            SQL = SQL & "tp.ddd, "
'            SQL = SQL & "tp.telefone "
'            SQL = SQL & "From "
'            SQL = SQL & "   sub_grupo_assistencia_tb sg   WITH (NOLOCK) "
'            SQL = SQL & "inner join produto_tp_assistencia_tb pt   WITH (NOLOCK) "
'            SQL = SQL & "   on sg.tp_assistencia_id = pt.tp_assistencia_id "
'            SQL = SQL & "   and sg.produto_id = pt.produto_id "
'            SQL = SQL & "inner join tp_assistencia_tb tp   WITH (NOLOCK) "
'            SQL = SQL & "   on pt.tp_assistencia_id = tp.tp_assistencia_id "
'
'            SQL = SQL & " INNER JOIN  sub_grupo_apolice_tb"
'            SQL = SQL & " ON sub_grupo_apolice_tb.apolice_id             =  SG.apolice_id"
'            SQL = SQL & " AND sub_grupo_apolice_tb.ramo_id                =  SG.ramo_id"
'            SQL = SQL & " AND sub_grupo_apolice_tb.sucursal_seguradora_id =  SG.sucursal_seguradora_id "
'            SQL = SQL & " AND sub_grupo_apolice_tb.seguradora_cod_susep   =  SG.seguradora_cod_susep "
'            SQL = SQL & " AND sub_grupo_apolice_tb.dt_inicio_vigencia_sbg =  SG.dt_inicio_vigencia_sbg "
'
'
'            SQL = SQL & " Where "
'            SQL = SQL & "       sg.apolice_id = " & vApoliceId & " "
'            SQL = SQL & "   and sg.sucursal_seguradora_id = " & vSucursalId & " "
'            SQL = SQL & "   and sg.seguradora_cod_susep = " & vCodSusep & " "
'            SQL = SQL & "   and sg.ramo_id = " & vRamoId & " "
'            SQL = SQL & "   and sg.sub_grupo_id = " & subgrupo_id & " "
'            SQL = SQL & "   and sg.dt_fim_assist_sbg is NULL "
'
'            Set bd = rdoCn1.OpenResultset(SQL)
'
'            If Not bd.EOF Then
'
'                s_assistencia = "Condi��es da Assist�ncia encontram-se em poder do Estipulante"
'                S_TelefoneAssistencia = "0800-161660" & Space(4)
'
'                Do While Not bd.EOF
'
'                    i = i + 1
'                    If i > 3 Then Exit Do
'
'                    Select Case i
'                    Case 1
'                        S_assistencia1 = IIf(Not IsNull(bd!Assistencia), bd!Assistencia, "")
'                    Case 2
'                        S_assistencia2 = IIf(Not IsNull(bd!Assistencia), bd!Assistencia, "")
'                    Case 3
'                        S_assistencia3 = IIf(Not IsNull(bd!Assistencia), bd!Assistencia, "")
'                    End Select
'
'                Loop
'            Else
'                s_assistencia = ""
'                S_TelefoneAssistencia = ""
'                S_assistencia1 = ""
'                S_assistencia2 = ""
'                S_assistencia3 = ""
'            End If
'            bd.Close
'
'            If s_assistencia <> "" Then
'                s_assistencia = "Condi��es da Assist�ncia encontram-se em poder do Estipulante" & "      Telefone Assistencia - " & S_TelefoneAssistencia
'
'                PETOCX1.PrintText 70, 245, RetirarAcento(s_assistencia)
'                PETOCX1.PrintText 40, 230, RetirarAcento(S_assistencia1)
'                PETOCX1.PrintText 40, 220, RetirarAcento(S_assistencia2)
'                PETOCX1.PrintText 40, 210, RetirarAcento(S_assistencia3)
'            End If
'
'                PETOCX1.PrintText 460, 205, RetirarAcento(s_dataemissao)
'
'            End If
'
'            'Imprime Observa�oes
'            'Para a observa��o1, temporariamente, temos a indica��o do subgrupo e sua descri��o -- Kylme 05/05/2003
'            S_observacao1 = Space(60)
'            SQL = " select sub_grupo_id, nome"
'            SQL = SQL & " from  sub_grupo_apolice_tb   WITH (NOLOCK) "
'            SQL = SQL & " Where     apolice_id      = " & vApoliceId
'            SQL = SQL & "   and     ramo_id         = " & vRamoId
'            SQL = SQL & "   and     sub_grupo_id    = " & subgrupo_id
'            'SQL = SQL & " and dt_fim_vigencia IS NULL"
'            Set bd = rdoCn1.OpenResultset(SQL)
'            If Not bd.EOF And bd!Nome <> ED_NomeSubEstipulante Then S_observacao1 = "Subgrupo = " & bd!sub_grupo_id & " - " & bd!Nome
'
'            'Este select n�o est� contemplando:
'            'observa�ao1,observa�ao2,observa�ao3 -> n�o foi inclu�do do modelo, o programa est� enviando brancos
'            S_observacao2 = Space(60)
'            S_observacao3 = Space(60)
'
'            PETOCX1.PrintText 40, 175, RetirarAcento(S_observacao1)
'            PETOCX1.PrintText 40, 165, RetirarAcento(S_observacao2)
'            PETOCX1.PrintText 40, 155, RetirarAcento(S_observacao3)
'
'            PETOCX1.AddInternalLinkBookmark rssegurado("nome"), a, 0, 840
'            a = a + 1
'        End If
'        rssegurado.MoveNext
'   Loop
'
'End If
'numrows = numrows + 1
'Loop
'
'PETOCX1.GotoPreviousBookmarkLevel
'PETOCX1.GotoPreviousBookmarkLevel
'
'Exit Sub
'
'Erro:
'    MsgBox ("Ocorreu o seguinte erro: " & Err.Description & " - SQL " & SQL)
'    TrataErroGeral "Gerar_Visu_Vidas", Me.name
'    Unload Me
'End Sub

'************************************************
'*      Nova fun��o para a impress�o online     *
'*      Demanda Evolutiva: 176384               *
'*      AFONSO - G&P -  Data: 15/05/2008        *
'************************************************
'jesse dias confitec - demanda 5704416 - inicio
'Public Sub Impressao_OnLine()
Public Sub Impressao_OnLine(ByVal ComBeneficiarios As Boolean)
'jesse dias confitec - demanda 5704416 - fim
    On Error Resume Next

    ImprimeOk = True

    'genjunior
    If InStr(",115,123,150,222,223,", "," & vProdutoId & ",") > 0 Then
        MsgBox "Imprimindo o documento de ap�lice", vbInformation + vbOKOnly, "Aviso"
        Call Imprimir_apolice_3(ComBeneficiarios)
        Exit Sub
    End If
    'fim genjunior

    'n�o exibir a tela de impressora caso selecionado imprimir na tela
    If ImprimirTela = False Then

        ImprimeOk = False
        dialog.CancelError = True
        dialog.ShowPrinter
        '
        If Err = 32755 Then
            MsgBox "Impress�o Cancelada.", vbInformation + vbOKOnly, "Cancelado"
            Exit Sub
        Else
            ImprimeOk = True
        End If
    End If


    If ImprimeOk = True Then

        On Error GoTo Erro

        Dim lugar As Long
        Dim SQL As String
        Dim rssegurado As rdoResultset
        Dim rs As rdoResultset
        Dim prop_cliente_id As Long
        Dim certificado_id As Variant
        Dim rsimpressao As rdoResultset
        Dim Count As Integer
        Dim TotalImpresoTela As Integer

        NumRows2 = MSFlexGrid2(0).Rows
        NumRows = 1
        lugar = InStr(1, Text1(1).Text, " ", vbTextCompare)
        subgrupo_id = Mid(Text1(1).Text, 1, lugar - 1)
        num_solicitacao = 0
        Count = 0
        TotalImpresoTela = 0

        StatusBar1.SimpleText = "Imprimindo..."

        MousePointer = vbHourglass

        Do While NumRows < NumRows2
            If MSFlexGrid2(0).TextMatrix(NumRows, 0) = "X" And MSFlexGrid2(0).TextMatrix(NumRows, 3) <> "" And MSFlexGrid2(0).TextMatrix(NumRows, 5) <> "" And MSFlexGrid2(0).TextMatrix(NumRows, 5) <> "" Then

                'Pegar os dados do segurado
                SQL = "exec dados_segurado_sps " & MSFlexGrid2(0).TextMatrix(NumRows, 5) & " , " & vRamoId & ", " & vApoliceId & ", " & subgrupo_id
                Set rssegurado = rdocn.OpenResultset(SQL)

                prop_cliente_id = rssegurado("prop_cliente_id")

                If Not rssegurado.EOF Then
                    Count = Count + 1
                    'imprime certificado
                    'Cuidado pois essa SPS chama uma SPU e em seguida da um INSERT
                    SQL = "exec rel_certif_sps " & vApoliceId & ", " & rssegurado("sucursal") & ", " & rssegurado("cod_susep") & ", " & vRamoId & "," & subgrupo_id & ", " & prop_cliente_id & ", '" & Usuario & "', null"
                    'akio.okuno 22/11/2011 - Demanda 8337000 - Impress�o de certificado no SEGP0223
                    'Set rs = rdoCn1.OpenResultset(SQL)
                    Set rs = rdocn.OpenResultset(SQL)

                    certificado_id = Format(rs(0).Value, "000000000")
                    rs.Close
                    Set rs = Nothing

                    FaturaSegurado prop_cliente_id, rssegurado("sucursal"), rssegurado("cod_susep")

                    '********************************
                    '*   Impress�o do relatorio     *
                    '*   Demanda: 176384            *
                    '*   AFONSO - G&P - 15/05/2008  *
                    '********************************
                    DtIniVigFatura = Format(DtIniVigFatura, "yyyy-mm-dd")

                    With Report

                        .Reset

                        If ImprimirTela = True Then
                            .Destination = crptToWindow
                            TotalImpresoTela = TotalImpresoTela + 1
                        Else
                            .Destination = crptToPrinter
                        End If

                        .Connect = rdocn.Connect

                        'jesse dias confitec - demanda 5704416 - inicio
                        'Inclus�o de sub-relat�rio benefici�rios
                        If ComBeneficiarios = True Then
                            .ReportFileName = App.PATH & "\SEGR0223-09.rpt"
                            'Procedure: SEGS7015_SPS
                            .StoredProcParam(0) = CStr(vApoliceId)                     'Apolice
                            .StoredProcParam(1) = CStr(vRamoId)                        'Ramo
                            .StoredProcParam(2) = CStr(subgrupo_id)                         'Sub Grupo
                            .StoredProcParam(3) = CStr(prop_cliente_id)                   'Codigo do Segurado
                            .StoredProcParam(4) = CStr(vFaturaId)                         'Fatura_id do Segurado
                            .StoredProcParam(5) = CStr(DtIniVigFatura)              'Data da Vig�ncia da Fatura do Segurado
                            'Troca de Report - Passa os par�metros para procedure de cada sub relat�rio
                            'Procedure: SEGS7016_SPS
                            .SubreportToChange = "sub_rel_Certificado_Vida_Pers_Titular.rpt"
                            .StoredProcParam(0) = CStr(vApoliceId)                     'Apolice
                            .StoredProcParam(1) = CStr(vRamoId)                        'Ramo
                            .StoredProcParam(2) = CStr(subgrupo_id)                         'Sub Grupo
                            .StoredProcParam(3) = CStr(prop_cliente_id)                   'Codigo do Segurado
                            .SubreportToChange = "sub_rel_Certificado_Vida_Pers_Conjuge.rpt"
                            .StoredProcParam(0) = CStr(vApoliceId)                     'Apolice
                            .StoredProcParam(1) = CStr(vRamoId)                        'Ramo
                            .StoredProcParam(2) = CStr(subgrupo_id)                         'Sub Grupo
                            .StoredProcParam(3) = CStr(prop_cliente_id)                   'Codigo do Segurado
                            .SubreportToChange = "sub_rel_Certificado_Vida_Pers_Filhos.rpt"
                            .StoredProcParam(0) = CStr(vApoliceId)                     'Apolice
                            .StoredProcParam(1) = CStr(vRamoId)                        'Ramo
                            .StoredProcParam(2) = CStr(subgrupo_id)                         'Sub Grupo
                            .StoredProcParam(3) = CStr(prop_cliente_id)             'Codigo do Segurado
                            .SubreportToChange = "sub_rel_beneficiario"
                            .StoredProcParam(0) = CStr(vApoliceId)                     'Apolice
                            .StoredProcParam(1) = CStr(vRamoId)                        'Ramo
                            .StoredProcParam(2) = CStr(subgrupo_id)                    'Sub Grupo
                            .StoredProcParam(3) = CStr(prop_cliente_id)                'Codigo do Segurado
                        Else

                            .ReportFileName = App.PATH & "\rel_Certificado_Vida_Pers.rpt"
                            'Procedure: SEGS7015_SPS
                            .StoredProcParam(0) = CStr(vApoliceId)                     'Apolice
                            .StoredProcParam(1) = CStr(vRamoId)                        'Ramo
                            .StoredProcParam(2) = CStr(subgrupo_id)                         'Sub Grupo
                            .StoredProcParam(3) = CStr(prop_cliente_id)                   'Codigo do Segurado
                            .StoredProcParam(4) = CStr(vFaturaId)                         'Fatura_id do Segurado
                            .StoredProcParam(5) = CStr(DtIniVigFatura)              'Data da Vig�ncia da Fatura do Segurado
                            'Troca de Report - Passa os par�metros para procedure de cada sub relat�rio
                            'Procedure: SEGS7016_SPS
                            .SubreportToChange = "sub_rel_Certificado_Vida_Pers_Titular.rpt"
                            .StoredProcParam(0) = CStr(vApoliceId)                     'Apolice
                            .StoredProcParam(1) = CStr(vRamoId)                        'Ramo
                            .StoredProcParam(2) = CStr(subgrupo_id)                         'Sub Grupo
                            .StoredProcParam(3) = CStr(prop_cliente_id)                   'Codigo do Segurado
                            .SubreportToChange = "sub_rel_Certificado_Vida_Pers_Conjuge.rpt"
                            .StoredProcParam(0) = CStr(vApoliceId)                     'Apolice
                            .StoredProcParam(1) = CStr(vRamoId)                        'Ramo
                            .StoredProcParam(2) = CStr(subgrupo_id)                         'Sub Grupo
                            .StoredProcParam(3) = CStr(prop_cliente_id)                   'Codigo do Segurado
                            .SubreportToChange = "sub_rel_Certificado_Vida_Pers_Filhos.rpt"
                            .StoredProcParam(0) = CStr(vApoliceId)                     'Apolice
                            .StoredProcParam(1) = CStr(vRamoId)                        'Ramo
                            .StoredProcParam(2) = CStr(subgrupo_id)                         'Sub Grupo
                            .StoredProcParam(3) = CStr(prop_cliente_id)             'Codigo do Segurado
                        End If

                        .Action = 1
                        'jesse dias confitec - demanda 5704416 - fim

                    End With

                    If TotalImpresoTela >= 15 Then
                        NumRows = NumRows2
                    End If

                End If

            End If

            NumRows = NumRows + 1
        Loop

        If TotalImpresoTela >= 15 Then
            MsgBox "O limite m�ximo para impress�o em tela � de 15 certificados por vez", vbOKOnly + vbInformation, "Limite"
        End If

        '**********************************************************************************
        '*   AFONSO - G&P - EVOLUTIVA: 176384  - Data: 05/05/2008                         *
        '*   ESTA INCLUS�O E EFETUADA APENA PARA O LOG DE IMPRESSAO DE CERTIFICADO ONLINE *
        '*   O PARAMETRO @STATUS='O' IDENTIFICA QUE A IMPRESS�O REALIZADA FOI ONLINE      *
        '**********************************************************************************
        If Count > 0 Then
            SQL = ""
            SQL = "exec evento_seguros_db..evento_impressao_spi "
            SQL = SQL & vPropostaId & ", " & gEndossoId & ", NULL, '" & Usuario & "', NULL, 5, 'O','IMPRESSAO DE CERTIFICADO'," & subgrupo_id & ",'A', NULL, NULL, '" & Usuario & "' ,NULL, NULL, NULL, NULL, NULL, NULL"
            Set rsimpressao = rdocn.OpenResultset(SQL)
            num_solicitacao = rsimpressao(0).Value
            Set rsimpressao = Nothing

            'ALTERA��O - DIEGO GALIZONI CAVERSAN - G&P - 27/12/2007
            'Inclus�o do evento 81034 pois as propostas inseridas na evento_impressao_tb dessa maneira n�o ser�o impressas.
            SQL = "exec evento_seguros_db..evento_spi " & vPropostaId & ", " & sPropostaBB & ",null, null, null, null, null, null, null, null, null, null, '" & Usuario & "', '" & Format(Now, "yyyymmdd") & "'" & ",81034"
            rdocn.Execute (SQL)

        Else
            MsgBox "Favor refazer a pesquisa.", vbInformation + vbOKOnly, "Aviso"
        End If
    End If

    MousePointer = vbDefault
    StatusBar1.SimpleText = ""

    Exit Sub

Erro:
    If Err.Number = 40002 Or Err.Number = 91 Then
        Resume Next
    Else
        MsgBox ("Ocorreu o seguinte erro: " & Err.Description & " - SQL " & SQL)
        TrataErroGeral "Impressao_OnLine", Me.name
        MousePointer = vbDefault
    End If
End Sub

Private Sub Impressao_Grafica()
'************************************************
'*      Nova fun��o para a impress�o na grafica *
'*      Demanda Evolutiva: 176384               *
'*      AFONSO - G&P -  Data: 15/05/2008        *
'************************************************

    On Error GoTo Erro

    Dim lugar As Long
    Dim SQL As String
    Dim rssegurado As rdoResultset
    Dim rs As rdoResultset
    Dim prop_cliente_id As Long
    Dim certificado_id As Variant
    Dim rsimpressao As rdoResultset

    NumRows2 = MSFlexGrid2(0).Rows
    NumRows = 1
    lugar = InStr(1, Text1(1).Text, " ", vbTextCompare)
    subgrupo_id = Mid(Text1(1).Text, 1, lugar - 1)


    Dim CountVida As Long
    Dim VidaSeg As Long


    MousePointer = vbHourglass
    CountVida = 0
    VidaSeg = 0
    num_solicitacao = 0

    '****************************************************
    '* DESABILATA��O DA OP��O DE IMPRESS�O NA GRAFICA   *
    '* POR DIEGO DINIZ POIS N�O TINHA SIDO HOMOLOGADO   *
    '* PELO O USUARIO.  DATA: 19/06/2008                *
    '****************************************************
    MsgBox "A Impress�o na Gr�fica est� em constru��o!" & vbNewLine & vbNewLine & "O programa n�o gerou solicita��o.", vbCritical + vbOKOnly, "Aviso"
    Screen.MousePointer = vbDefault
    MousePointer = 0
    DoEvents
    Exit Sub
    '****************************************************
    '* PARA VOLTAR A FUNCIONAR A FUNCTION DE IMPRESS�O  *
    '* DEVE-SE COMENTAR O MSGBOX , O EXIT SUB E         *
    '* Screen.MousePointer ACIMA.                        *
    '****************************************************

    '****************************************************
    '*  Verifica se existe alguma solicita��o pendente  *
    '*  AFONSO - G&P - DATA: 20/05/2008                 *
    '****************************************************
    SQL = ""
    SQL = SQL & "SELECT TOP 1" & vbNewLine
    SQL = SQL & "ei.num_solicitacao" & vbNewLine
    SQL = SQL & "From evento_seguros_db..evento_impressao_tb ei  WITH (NOLOCK) " & vbNewLine
    SQL = SQL & "Where UPPER(ei.status) = 'L'" & vbNewLine
    SQL = SQL & "AND ei.dt_impressao_grf IS NULL" & vbNewLine
    SQL = SQL & "AND ei.proposta_id = " & vPropostaId & vbNewLine
    SQL = SQL & "AND ei.sub_grupo_id = " & subgrupo_id & vbNewLine
    SQL = SQL & "ORDER BY ei.dt_solicitacao DESC" & vbNewLine
    Set rsimpressao = rdocn.OpenResultset(SQL)

    If Not rsimpressao.EOF Then

        num_solicitacao = rsimpressao(0).Value
        rsimpressao.Close
        Set rsimpressao = Nothing

    Else    ' SE N�O EXISTIR SOLICITA��O CRIA UMA NOVA

        '**********************************************************************************
        '*   AFONSO - G&P - EVOLUTIVA: 176384  - Data: 05/05/2008                         *
        '*   ESTA INCLUS�O E EFETUADA APENA PARA O LOG DE IMPRESSAO DE CERTIFICADO ONLINE *
        '*   O PARAMETRO @STATUS='I' IDENTIFICA QUE A IMPRESS�O  FOI PARA GRAFICA         *
        '**********************************************************************************
        rsimpressao.Close
        Set rsimpressao = Nothing
        SQL = ""
        SQL = "exec evento_seguros_db..evento_impressao_spi "
        SQL = SQL & vPropostaId & ", " & gEndossoId & ", NULL, '" & Usuario & "' ,'C', 5, 'L',NULL, " & subgrupo_id & ",'A', NULL, NULL, '" & Usuario & "' ,NULL, NULL, NULL, NULL, NULL, NULL"
        Set rsimpressao = rdocn.OpenResultset(SQL)
        num_solicitacao = rsimpressao(0).Value
        rsimpressao.Close
        Set rsimpressao = Nothing

    End If


    Do While NumRows < NumRows2
        'manda para a impress�o somente os que est�o selecionado
        If MSFlexGrid2(0).TextMatrix(NumRows, 0) = "X" And MSFlexGrid2(0).TextMatrix(NumRows, 3) <> "" And MSFlexGrid2(0).TextMatrix(NumRows, 5) <> "" And MSFlexGrid2(0).TextMatrix(NumRows, 5) <> "" Then

            CountVida = CountVida + 1

            'Pegar os dados do segurado
            SQL = "exec dados_segurado_sps " & MSFlexGrid2(0).TextMatrix(NumRows, 5) & " , " & vRamoId & ", " & vApoliceId & ", " & subgrupo_id
            Set rssegurado = rdocn.OpenResultset(SQL)

            prop_cliente_id = rssegurado("prop_cliente_id")
            'rssegurado("sucursal")
            'rssegurado("cod_susep")

            SQL = ""
            SQL = SQL & "SELECT TOP 1 *" & vbNewLine
            SQL = SQL & "FROM evento_seguros_db..impressao_vidas_tb iv  WITH (NOLOCK) " & vbNewLine
            SQL = SQL & "WHERE num_solicitacao = " & num_solicitacao & vbNewLine
            SQL = SQL & "AND prop_cliente_id = " & prop_cliente_id & vbNewLine
            Set rsimpressao = rdocn.OpenResultset(SQL)

            If Not rsimpressao.EOF Then    'O segurado j� possui solicita��o.

                VidaSeg = VidaSeg + 1

            Else    'Se n�o exite solicita��o para o segurado

                'imprime certificado
                'Cuidado pois essa SPS chama uma SPU e em seguida da um INSERT
                SQL = "exec rel_certif_sps " & vApoliceId & ", " & rssegurado("sucursal") & ", " & rssegurado("cod_susep") & ", " & vRamoId & "," & subgrupo_id & ", " & prop_cliente_id & ", '" & Usuario & "', null"
                Set rs = rdoCn1.OpenResultset(SQL)

                certificado_id = Format(rs(0).Value, "000000000")
                rs.Close
                Set rs = Nothing

                'pega o vFaturaId do segurado
                FaturaSegurado prop_cliente_id, rssegurado("sucursal"), rssegurado("cod_susep")

                SQL = ""
                SQL = "exec  evento_seguros_db..impressao_vidas_spi " & num_solicitacao & ", " & prop_cliente_id & ", " & vFaturaId
                rdocn.Execute (SQL)

            End If

            rssegurado.Close
            Set rssegurado = Nothing
            rsimpressao.Close
            Set rsimpressao = Nothing

        End If

        NumRows = NumRows + 1

    Loop

    StatusBar1.SimpleText = "Aguarde..."
    MousePointer = 0
    num_solicitacao = 0
    'verifica quantos segurado pendetes tem
    If CountVida = VidaSeg Then
        MsgBox "O(s) Segurado(s) Selecionado possui solicita��o pendente", vbInformation + vbOKOnly, "Aviso"
    Else
        MsgBox "Solicita��o gerada com sucesso", vbInformation + vbOKOnly, "Sucesso"
    End If

    Exit Sub

Erro:
    MsgBox ("Ocorreu o seguinte erro: " & Err.Description & " - SQL " & SQL)
    TrataErroGeral "Impressao_Grafica", Me.name
End Sub

Private Sub FaturaSegurado(ByVal Cliente_id As Long, ByVal Sucursal As Long, ByVal Cod_Susep As Long)

'*************************************************************************
'*  fun��o para retornar a fatura_id e dt_inicio_vigencia de um segurado *
'*  AFONSO - G&P - DEMANDA EVOLUTIVA: 176384 - DATA: 16/05/2008          *
'*************************************************************************

    Dim FaturaSeg As rdoResultset
    Dim SQL As String
    Dim DataOk As Boolean
    Dim mes As String
    Dim ano As String

    ano = Right(MEBVigencia.Text, 4)
    mes = Left(MEBVigencia.Text, 2)

    DataOk = False

    If MEBVigencia.Text = "__/____" Then
        DataOk = False
    Else
        If IsNumeric(ano) And IsNumeric(mes) Then
            If (CInt(mes) >= 1 And CInt(mes) <= 12) Then
                DataOk = True
            Else
                DataOk = False
            End If
        Else
            DataOk = False
        End If
    End If

    SQL = "select fatura_tb.fatura_id,dt_inicio_vigencia,dt_fim_vigencia from fatura_tb WITH(NOLOCK)" & vbNewLine
    SQL = SQL & "Join fatura_segurado_tb WITH(NOLOCK)" & vbNewLine
    SQL = SQL & "   on fatura_segurado_tb.fatura_id = fatura_tb.fatura_id" & vbNewLine
    SQL = SQL & "   and fatura_segurado_tb.ramo_id = fatura_tb.ramo_id" & vbNewLine
    SQL = SQL & "   and fatura_segurado_tb.apolice_id = fatura_tb.apolice_id" & vbNewLine
    SQL = SQL & "   and fatura_segurado_tb.seguradora_cod_susep = fatura_tb.seguradora_cod_susep" & vbNewLine
    SQL = SQL & "   and fatura_segurado_tb.sucursal_seguradora_id = fatura_tb.sucursal_seguradora_id" & vbNewLine
    SQL = SQL & "Where fatura_segurado_tb.ramo_id = " & vRamoId & vbNewLine
    SQL = SQL & "   and fatura_segurado_tb.apolice_id = " & vApoliceId & vbNewLine
    SQL = SQL & "   and fatura_segurado_tb.prop_cliente_id = " & Cliente_id & vbNewLine
    SQL = SQL & "   and fatura_segurado_tb.seguradora_cod_susep = " & Cod_Susep & vbNewLine
    SQL = SQL & "   and fatura_segurado_tb.sucursal_seguradora_id = " & Sucursal & vbNewLine & vbNewLine

    'Verificar a fatura_id por data
    If DataOk = True Then
        SQL = SQL & "   AND YEAR(fatura_tb.dt_inicio_vigencia) <= " & CInt(ano) & vbNewLine
        SQL = SQL & "   AND MONTH(fatura_tb.dt_inicio_vigencia) <= " & CInt(mes) & vbNewLine
        SQL = SQL & "   AND YEAR(fatura_tb.dt_fim_vigencia) >=  " & CInt(ano) & vbNewLine
        SQL = SQL & "   AND MONTH(fatura_tb.dt_fim_vigencia) >= " & CInt(mes) & vbNewLine

    Else
        'pegar o maximo da fatura_id
        SQL = SQL & "   and fatura_tb.fatura_id = (select max(fatura_id) from fatura_segurado_tb fat WITH(NOLOCK)" & vbNewLine
        SQL = SQL & "           Where fat.apolice_id = fatura_segurado_tb.apolice_id" & vbNewLine
        SQL = SQL & "           and fatura_segurado_tb.ramo_id = fat.ramo_id" & vbNewLine
        SQL = SQL & "           and fatura_segurado_tb.apolice_id = fat.apolice_id" & vbNewLine
        SQL = SQL & "           and fat.prop_cliente_id = fatura_segurado_tb.prop_cliente_id" & vbNewLine
        SQL = SQL & "           and fat.seguradora_cod_susep = fatura_segurado_tb.seguradora_cod_susep" & vbNewLine
        SQL = SQL & "           and fat.sucursal_seguradora_id = fatura_segurado_tb.sucursal_seguradora_id)" & vbNewLine

    End If

    Set FaturaSeg = rdoCn2.OpenResultset(SQL)

    If Not FaturaSeg.EOF Then
        vFaturaId = FaturaSeg(0)
        DtIniVigFatura = Format(FaturaSeg(1), "dd/mm/yyyy")
    Else
        vFaturaId = 0
        DtIniVigFatura = "01/01/1900"
    End If

    FaturaSeg.Close
    Set FaturaSeg = Nothing
    SQL = ""

End Sub


Private Sub Ler_Total_Vidas()
    Dim SQL As String
    Dim rc As rdoResultset
    Texto = ""
    'lpinto
    If Not bPropostaRecusada And Not bPropostaNaoEmitida And Not bPropostaEstudo Then    'rmaiellaro (19/01/2007)

        Texto = " and sgp.dt_fim_vigencia_sbg is null"

        SQL = " select count(sgp.proposta_id) numero"
        SQL = SQL & " from seguro_vida_sub_grupo_tb sgp   WITH (NOLOCK) "
        SQL = SQL & " join cliente_tb cl   WITH (NOLOCK)  on "
        SQL = SQL & " sgp.prop_cliente_id = cl.cliente_id "
        SQL = SQL & " where "
        SQL = SQL & "           sgp.apolice_id = " & vApoliceId
        SQL = SQL & "       AND sgp.sucursal_seguradora_id = " & vSucursalId
        SQL = SQL & "       AND sgp.seguradora_cod_susep = " & vSeguradoraId
        SQL = SQL & "       AND sgp.ramo_id = " & vRamoId
        SQL = SQL & Texto
        SQL = SQL & "       AND sgp.dt_inicio_vigencia_sbg = (Select MAX(sgp2.dt_inicio_vigencia_sbg) from seguro_vida_sub_grupo_tb sgp2  WITH (NOLOCK)  "
        SQL = SQL & " WHERE   sgp2.apolice_id = sgp.apolice_id "
        SQL = SQL & Texto & " and "
        SQL = SQL & " sgp2.sucursal_seguradora_id = sgp.sucursal_seguradora_id AND "
        SQL = SQL & " sgp2.seguradora_cod_susep = sgp.seguradora_cod_susep AND "
        SQL = SQL & " sgp2.ramo_id = sgp.ramo_id AND "
        SQL = SQL & " sgp2.sub_grupo_id = sgp.sub_grupo_id AND "
        SQL = SQL & " sgp2.dt_inicio_vigencia_apol_sbg = sgp.dt_inicio_vigencia_apol_sbg AND "
        SQL = SQL & " sgp2.proposta_id = sgp.proposta_id AND "
        SQL = SQL & " sgp2.prop_cliente_id = sgp.prop_cliente_id AND "
        SQL = SQL & " sgp2.tp_componente_id = sgp.tp_componente_id AND "
        SQL = SQL & " sgp2.seq_canc_endosso_seg = sgp.seq_canc_endosso_seg)"

        Set rc = rdocn.OpenResultset(SQL)

        If Not rc.EOF Then
            Text1(3).Text = rc("numero")
        End If
        rc.Close
        Set rc = Nothing
    End If

End Sub

Private Function Conta_Vidas_Subgrupos(subgrupo_id As Integer)
    Dim SQL As String
    Dim rc As rdoResultset

    Texto = " and SGP.dt_fim_vigencia_sbg is null"

    'RRAMOS - 2007/07/13 - Flow 224711 - Melhoria de performance. Nova query elaborada pelo Anderson Lorusso.-------------------------
    '''
    '''SQL = " select count(sgp.proposta_id) numero"
    '''SQL = SQL & " from seguro_vida_sub_grupo_tb sgp  WITH (NOLOCK)  "
    '''SQL = SQL & " join cliente_tb cl on "
    '''SQL = SQL & " sgp.prop_cliente_id = cl.cliente_id "
    '''SQL = SQL & "  where sgp.sub_grupo_id = " & subgrupo_id & " AND "
    '''SQL = SQL & " sgp.apolice_id = " & vApoliceId
    '''SQL = SQL & " AND sgp.sucursal_seguradora_id = " & vSucursalId
    '''SQL = SQL & " AND sgp.seguradora_cod_susep = " & vSeguradoraId
    '''SQL = SQL & " AND sgp.ramo_id = " & vRamoId
    '''SQL = SQL & Texto
    '''SQL = SQL & " AND sgp.dt_inicio_vigencia_sbg = (Select MAX(sgp2.dt_inicio_vigencia_sbg) from seguro_vida_sub_grupo_tb sgp2  WITH (NOLOCK)  "
    '''SQL = SQL & " WHERE   sgp2.apolice_id = sgp.apolice_id "
    '''SQL = SQL & Texto & " AND "
    '''SQL = SQL & " sgp2.sucursal_seguradora_id = sgp.sucursal_seguradora_id AND "
    '''SQL = SQL & " sgp2.seguradora_cod_susep = sgp.seguradora_cod_susep AND "
    '''SQL = SQL & " sgp2.ramo_id = sgp.ramo_id AND "
    '''SQL = SQL & " sgp2.sub_grupo_id = sgp.sub_grupo_id AND "
    '''SQL = SQL & " sgp2.dt_inicio_vigencia_apol_sbg = sgp.dt_inicio_vigencia_apol_sbg AND "
    '''SQL = SQL & " sgp2.proposta_id = sgp.proposta_id AND "
    '''SQL = SQL & " sgp2.prop_cliente_id = sgp.prop_cliente_id AND "
    '''SQL = SQL & " sgp2.tp_componente_id = sgp.tp_componente_id AND "
    '''SQL = SQL & " sgp2.seq_canc_endosso_seg = sgp.seq_canc_endosso_seg AND"
    '''SQL = SQL & " sgp2.sub_grupo_id = " & subgrupo_id & ")"
    '''
    '''Set rc = rdocn.OpenResultset(SQL)

    SQL = ""
    SQL = SQL & "SET NOCOUNT ON " & vbNewLine
    SQL = SQL & "Select * into #seguro_vida_sub_grupo "
    SQL = SQL & " From seguro_vida_sub_grupo_tb sgp  WITH (NOLOCK)  "
    SQL = SQL & " Where sub_grupo_id = " & subgrupo_id
    SQL = SQL & "   AND apolice_id = " & vApoliceId
    SQL = SQL & "   AND sucursal_seguradora_id = " & vSucursalId
    SQL = SQL & "   AND seguradora_cod_susep = " & vSeguradoraId
    SQL = SQL & "   AND ramo_id = " & vRamoId
    SQL = SQL & "   AND dt_fim_vigencia_sbg is null " & vbNewLine

    SQL = SQL & "CREATE UNIQUE CLUSTERED INDEX UK001_#seguro_vida_sub_grupo ON #seguro_vida_sub_grupo(apolice_id, sub_grupo_id, ramo_id, prop_cliente_id, dt_inicio_vigencia_sbg) " & vbNewLine

    SQL = SQL & "select count(sgp.proposta_id) numero"
    SQL = SQL & " from #seguro_vida_sub_grupo sgp  WITH (NOLOCK)  "
    SQL = SQL & " join cliente_tb cl on "
    SQL = SQL & "      sgp.prop_cliente_id = cl.cliente_id "
    SQL = SQL & " where sgp.sub_grupo_id = " & subgrupo_id
    SQL = SQL & " AND sgp.apolice_id = " & vApoliceId
    SQL = SQL & " AND sgp.sucursal_seguradora_id = " & vSucursalId
    SQL = SQL & " AND sgp.seguradora_cod_susep = " & vSeguradoraId
    SQL = SQL & " AND sgp.ramo_id = " & vRamoId
    SQL = SQL & Texto
    SQL = SQL & " AND sgp.dt_inicio_vigencia_sbg = (Select TOP 1 sgp2.dt_inicio_vigencia_sbg from #seguro_vida_sub_grupo sgp2  WITH (NOLOCK)  "
    SQL = SQL & " WHERE   sgp2.apolice_id = sgp.apolice_id "
    SQL = SQL & Texto & " AND "
    SQL = SQL & " sgp2.sucursal_seguradora_id = sgp.sucursal_seguradora_id AND "
    SQL = SQL & " sgp2.seguradora_cod_susep = sgp.seguradora_cod_susep AND "
    SQL = SQL & " sgp2.ramo_id = sgp.ramo_id AND "
    SQL = SQL & " sgp2.sub_grupo_id = sgp.sub_grupo_id AND "
    SQL = SQL & " sgp2.dt_inicio_vigencia_apol_sbg = sgp.dt_inicio_vigencia_apol_sbg AND "
    SQL = SQL & " sgp2.proposta_id = sgp.proposta_id AND "
    SQL = SQL & " sgp2.prop_cliente_id = sgp.prop_cliente_id AND "
    SQL = SQL & " sgp2.tp_componente_id = sgp.tp_componente_id AND "
    SQL = SQL & " sgp2.seq_canc_endosso_seg = sgp.seq_canc_endosso_seg AND"
    SQL = SQL & " sgp2.sub_grupo_id = " & subgrupo_id
    SQL = SQL & " ORDER BY sgp2.dt_inicio_vigencia_sbg DESC) " & vbNewLine

    SQL = SQL & "DROP TABLE #seguro_vida_sub_grupo"
    '----------------------------------------------------------------------------------------------------------

    Set rc = rdocn.OpenResultset(SQL)

    If Not rc.EOF Then
        Conta_Vidas_Subgrupos = rc("numero")
    End If
    rc.Close
    Set rc = Nothing

End Function


Public Sub BuscarCoberturas(vParam As String, subgrupo_id As Integer)

    On Error GoTo ErroBusca

    Dim sParam As String
    Dim sSQL As String

    sParam = BuscaParametro(vParam, rdoCn1)

    sSQL = ""
    sSQL = sSQL & "SELECT distinct es.class_tp_cobertura, "
    sSQL = sSQL & "       perc_basica = CASE  "
    sSQL = sSQL & "                     WHEN es.perc_basica = 0 THEN 100 "
    sSQL = sSQL & "                     ELSE es.perc_basica "
    sSQL = sSQL & "                     END , "
    sSQL = sSQL & "                es.carencia_dias, "
    sSQL = sSQL & "                isnull(es.val_lim_max_is, 0) val_lim_max_is, "
    sSQL = sSQL & "                es.val_min_franquia, "
    sSQL = sSQL & "                cb.nome, cb.tp_cobertura_id "
    sSQL = sSQL & "FROM    escolha_sub_grp_tp_cob_comp_tb es   WITH (NOLOCK)  "
    sSQL = sSQL & "JOIN tp_cob_comp_tb cc   WITH (NOLOCK)  ON  es.tp_cob_comp_id = cc.tp_cob_comp_id "
    sSQL = sSQL & "JOIN tp_cobertura_tb cb   WITH (NOLOCK)  ON cc.tp_cobertura_id = cb.tp_cobertura_id "
    sSQL = sSQL & " WHERE   es.apolice_id = " & vApoliceId
    sSQL = sSQL & " AND     es.sucursal_seguradora_id = " & vSucursalId
    sSQL = sSQL & " AND     es.seguradora_cod_susep = " & vCodSusep
    sSQL = sSQL & " AND     es.ramo_id = " & vRamoId
    sSQL = sSQL & " AND     es.sub_grupo_id = " & subgrupo_id
    sSQL = sSQL & " AND     cc.tp_componente_id = " & sParam
    sSQL = sSQL & " ORDER BY es.class_tp_cobertura DESC, cb.tp_cobertura_id DESC "

    Set bd_cob = rdoCn2.OpenResultset(sSQL)

    Exit Sub

ErroBusca:
    TrataErroGeral "BuscarCoberturas", Me.name
    cmdSair_Click
End Sub

Public Sub BuscaParametros()

    On Error GoTo ErroBuscaParametros
    Dim SQL As String
    Dim ps_parametro As rdoResultset

    SQL = "SELECT  'SCR_93' = ISNULL(val_parametro,''), "    'SEGA8017 CERTIFICADO RAMO 93
    SQL = SQL & " 'SCR_82' = (SELECT ISNULL(val_parametro,'')"
    SQL = SQL & "  From ps_parametro_tb   WITH (NOLOCK) "
    SQL = SQL & " WHERE parametro ='SEGA8017 CERTIFICADO RAMO 82'),"    'SEGA8017 CERTIFICADO RAMO 82
    SQL = SQL & " 'STP_DOC' = (SELECT ISNULL(val_parametro,'')"   'SEGA8017 TP DOC
    SQL = SQL & " From ps_parametro_tb   WITH (NOLOCK) "
    SQL = SQL & " WHERE parametro ='SEGA8017 TP DOC'),"
    SQL = SQL & " 'corretora_bb' = (SELECT ISNULL(val_parametro,'')"
    SQL = SQL & " From ps_parametro_tb   WITH (NOLOCK) "
    SQL = SQL & " WHERE parametro ='CORRETORA_BB')"
    SQL = SQL & " From ps_parametro_tb   WITH (NOLOCK) "
    SQL = SQL & " WHERE parametro = 'SEGA8017 CERTIFICADO RAMO 93'"
    Set ps_parametro = rdocn.OpenResultset(SQL)
    If ps_parametro.EOF Then
        MensagemBatch "Os par�metros devem ser cadastrados!", vbExclamation
        Set ps_parametro = Nothing
        Exit Sub
    Else
        If ps_parametro!SCR_93 <> "" Then
            vTitulo_R93 = ps_parametro!SCR_93
        Else
            MsgBox ("Titulo do Certificado n�o cadastrado - Ramo 93!")
            Exit Sub
        End If

        If ps_parametro!SCR_82 <> "" Then
            vTitulo_R82 = ps_parametro!SCR_82
        Else
            MsgBox ("Titulo do Certificado n�o cadastrado - Ramo 82!")
            Exit Sub
        End If

        If ps_parametro!STP_DOC <> "" Then
            vTipoDoc = ps_parametro!STP_DOC
        Else
            MsgBox ("Tipo do Documento n�o Cadastrado!")
            Exit Sub
        End If

        If ps_parametro!corretora_bb <> "" Then
            vCorretora_bb = ps_parametro!corretora_bb
        Else
            MsgBox ("Corretora BB n�o cadastrada!")
            Exit Sub
        End If
    End If

    Exit Sub
ErroBuscaParametros:
    TrataErroGeral "ErroBuscaParametros", Me.name
    TerminaSEGBR
End Sub

Public Sub BuscaDadosBeneficiario(subgrupo_id)

    On Error GoTo ErroBuscaDadosBeneficiario
    Dim SQL As String
    Dim bd As rdoResultset

    SQL = "SELECT"
    SQL = SQL & " texto_beneficiario "
    SQL = SQL & " FROM"
    SQL = SQL & " sub_grupo_apolice_tb sg    WITH (NOLOCK)    "
    SQL = SQL & " WHERE"
    SQL = SQL & " sg.apolice_id = " & vApoliceId & ""
    SQL = SQL & " and sg.sucursal_seguradora_id = " & vSucursalId & ""
    SQL = SQL & " and sg.seguradora_cod_susep   = " & vCodSusep & ""
    SQL = SQL & " and sg.sub_grupo_id = " & subgrupo_id & " "
    SQL = SQL & " and sg.ramo_id = " & vRamoId & ""

    Set bd = rdoCn2.OpenResultset(SQL)
    If Not bd.EOF Then
        DB_DadosBeneficiario_aux = IIf(Not IsNull(bd!texto_beneficiario) And Trim(bd!texto_beneficiario) <> "", bd!texto_beneficiario, "")
    Else
        DB_DadosBeneficiario_aux = ""
    End If

    bd.Close
    Exit Sub

ErroBuscaDadosBeneficiario:
    TrataErroGeral "ErroBuscaDadosBeneficiario", Me.name
    TerminaSEGBR
End Sub


Public Sub BuscaDadosSUBEstipulante(subgrupo_id As String)
'Busca Nome e CNPJ do Sub-Estipulante
    Dim SQL As String
    Dim bd As rdoResultset

    SQL = ""
    SQL = "SELECT"
    SQL = SQL & " ISNULL(cl.nome,'') nome,                                          "
    SQL = SQL & " ISNULL(pj.cgc,'')cpf_cnpj                                         "
    SQL = SQL & " From                                                              "
    SQL = SQL & " cliente_tb cl   WITH (NOLOCK)                                            "
    SQL = SQL & " INNER JOIN pessoa_juridica_tb pj   WITH (NOLOCK)                         "
    SQL = SQL & "   ON  cl.cliente_id = pj.pj_cliente_id                            "
    SQL = SQL & " INNER JOIN representacao_sub_grupo_tb rp   WITH (NOLOCK)                 "
    SQL = SQL & "   ON  cl.cliente_id = rp.est_cliente_id                           "
    SQL = SQL & " Where"
    SQL = SQL & "       rp.apolice_id               = " & vApoliceId & ""
    SQL = SQL & "   and rp.sucursal_seguradora_id   = " & vSucursalId & ""
    SQL = SQL & "   and rp.seguradora_cod_susep     = " & vCodSusep & ""
    SQL = SQL & "   and rp.ramo_id                  = " & vRamoId & ""
    SQL = SQL & "   and rp.sub_grupo_id             = " & subgrupo_id & ""

    SQL = SQL & " and rp.dt_fim_representacao is null"

    Set bd = rdoCn1.OpenResultset(SQL)
    If Not bd.EOF Then
        ED_NomeSubEstipulante = bd!Nome
        ED_CNPJSubEstipulante = Format(bd!CPF_CNPJ, "@@@.@@@.@@@/@@@@-@@")     'Inclu�do por Rmarins 17/01/03
    Else
        Exit Sub
    End If
    bd.Close

    'Para caso de Estimpulante e Subestipulante for igual
    If (ED_NomeEstipulante = ED_NomeSubEstipulante) And (ED_CNPJEstipulante = ED_CNPJSubEstipulante) Then
        ED_NomeSubEstipulante = Space(60)
        ED_CNPJSubEstipulante = Space(19)
    End If

    Exit Sub

ErroBuscaDadosEstipulante:
    TrataErroGeral "BuscaDadosEstipulante", Me.name
    TerminaSEGBR
End Sub

Public Function MasCorrSusep(corr_susep) As String
    Dim mascara2 As Boolean
    If Len(corr_susep) = 15 Then
        mascara2 = True
    End If
    If mascara2 Then
        MasCorrSusep = Format(IIf(Trim(Mid(corr_susep, 1, 3)) = "", 0, Mid(corr_susep, 1, 3)), "000") & "." & _
                       Format(IIf(Trim(Mid(corr_susep, 4, 2)) = "", 0, Mid(corr_susep, 4, 2)), "00") & "." & _
                       Format(IIf(Trim(Mid(corr_susep, 6, 2)) = "", 0, Mid(corr_susep, 6, 2)), "00") & "." & _
                       Format(IIf(Trim(Mid(corr_susep, 8, 1)) = "", 0, Mid(corr_susep, 8, 1)), "0") & "." & _
                       Format(IIf(Trim(Mid(corr_susep, 9, 6)) = "", 0, Mid(corr_susep, 9, 6)), "000000") & "." & _
                       Format(IIf(Trim(Mid(corr_susep, 15, 1)) = "", 0, Mid(corr_susep, 15, 1)), "0")
    Else
        MasCorrSusep = Format(IIf(Trim(Mid(corr_susep, 1, 2)) = "", 0, Mid(corr_susep, 1, 2)), "00") & "." & _
                       Format(IIf(Trim(Mid(corr_susep, 3, 2)) = "", 0, Mid(corr_susep, 3, 2)), "00") & "." & _
                       Format(IIf(Trim(Mid(corr_susep, 5, 2)) = "", 0, Mid(corr_susep, 5, 2)), "00") & "." & _
                       Format(IIf(Trim(Mid(corr_susep, 7, 1)) = "", 0, Mid(corr_susep, 7, 1)), "0") & "." & _
                       Format(IIf(Trim(Mid(corr_susep, 8, 6)) = "", 0, Mid(corr_susep, 8, 6)), "000000") & "." & _
                       Format(IIf(Trim(Mid(corr_susep, 14, 1)) = "", 0, Mid(corr_susep, 14, 1)), "0")
    End If

End Function

Public Function RetirarAcento(ByVal Variavel As String) As String

    Variavel = Replace(Variavel, "�", "a", , , vbBinaryCompare)
    Variavel = Replace(Variavel, "�", "e", , , vbBinaryCompare)
    Variavel = Replace(Variavel, "�", "i", , , vbBinaryCompare)
    Variavel = Replace(Variavel, "�", "o", , , vbBinaryCompare)
    Variavel = Replace(Variavel, "�", "u", , , vbBinaryCompare)
    Variavel = Replace(Variavel, "�", "a", , , vbBinaryCompare)
    Variavel = Replace(Variavel, "�", "o", , , vbBinaryCompare)
    Variavel = Replace(Variavel, "�", "a", , , vbBinaryCompare)
    Variavel = Replace(Variavel, "�", "e", , , vbBinaryCompare)
    Variavel = Replace(Variavel, "�", "o", , , vbBinaryCompare)
    Variavel = Replace(Variavel, "�", "n", , , vbBinaryCompare)
    Variavel = Replace(Variavel, "�", "A", , , vbBinaryCompare)
    Variavel = Replace(Variavel, "�", "E", , , vbBinaryCompare)
    Variavel = Replace(Variavel, "�", "I", , , vbBinaryCompare)
    Variavel = Replace(Variavel, "�", "O", , , vbBinaryCompare)
    Variavel = Replace(Variavel, "�", "U", , , vbBinaryCompare)
    Variavel = Replace(Variavel, "�", "A", , , vbBinaryCompare)
    Variavel = Replace(Variavel, "�", "O", , , vbBinaryCompare)
    Variavel = Replace(Variavel, "�", "A", , , vbBinaryCompare)
    Variavel = Replace(Variavel, "�", "E", , , vbBinaryCompare)
    Variavel = Replace(Variavel, "�", "O", , , vbBinaryCompare)
    Variavel = Replace(Variavel, "�", "N", , , vbBinaryCompare)

    RetirarAcento = Variavel

End Function


Private Sub ObterTpDocumento(ByVal sArquivoSaida As String)

    On Error GoTo Erro

    Dim rsDoc As RDO.rdoResultset, sSQL As String

    sSQL = ""
    sSQL = sSQL & " SELECT TOP 1 isnull(tp_documento_id, 0) tp_documento_id, " & vbNewLine
    sSQL = sSQL & " documento_id " & vbNewLine
    sSQL = sSQL & " FROM evento_seguros_db..documento_tb  WITH (NOLOCK)  " & vbNewLine
    sSQL = sSQL & " WHERE arquivo_saida = '" & sArquivoSaida & "'" & vbNewLine

    Set rsDoc = rdocn.OpenResultset(sSQL)

    If rsDoc.EOF Then
        GoTo Erro
    Else
        iTpDocumentoId = CInt(rsDoc("tp_documento_id"))
        iDocumentoId = CInt(rsDoc("documento_id"))
    End If

    Set rsDoc = Nothing

    Exit Sub

Erro:
    MensagemBatch "Tipo de documento n�o encontrado para o produto! Programa ser� cancelado", vbCritical
    TrataErroGeral "ObterTpDocumento", Me.name
    TerminaSEGBR

End Sub

''pcarvalho - 20/10/2005
Private Function CarregarHistoricoProposta() As Boolean

    Dim oHistorico As Object
    Dim rsHistorico As Recordset
    Dim lCont As Long

    On Error GoTo TrataErro

    CarregarHistoricoProposta = True

    grdHistorico.Rows = 2

    lCont = 1

    Set oHistorico = CreateObject("SEGL0026.cls00406")

    'Set rsHistorico = oHistorico.ConsultarComentarioProposta(gsSIGLASISTEMA, _
     App.Title, _
     App.FileDescription, _
     glAmbiente_id, _
     3, _
     txtNumero.Text, _
     Usuario, _
     0, _
     0, _
     0, _
     False, , _
     True)


    Set rsHistorico = oHistorico.ConsultarComentarioProposta(gsSIGLASISTEMA, _
                                                             "SEGP0223", _
                                                             App.FileDescription, _
                                                             glAmbiente_id, _
                                                             3, _
                                                             txtNumero.Text, _
                                                             Usuario, _
                                                             0, _
                                                             0, _
                                                             0, _
                                                             False, , _
                                                             True)


    If Not rsHistorico.EOF Then

        grdHistorico.Rows = 1

        While Not rsHistorico.EOF

            grdHistorico.AddItem lCont & vbTab & _
                                 rsHistorico("assunto") & vbTab & _
                                 rsHistorico("comentario_id") & vbTab & _
                                 Format(rsHistorico("dt_inclusao"), "dd/mm/yyyy") & vbTab

            Call MontarTodasDescricoes(rsHistorico, lCont)

            rsHistorico.MoveNext

            lCont = lCont + 1

        Wend

    Else

        grdHistorico.HighLight = flexHighlightNever

    End If

    Set oHistorico = Nothing
    Set rsHistorico = Nothing

    Exit Function

TrataErro:
    CarregarHistoricoProposta = False
    Call TratarErro("CarregarHistoricoProposta", Me.name)
    Call FinalizarAplicacao

End Function

''pcarvalho - 20/10/2005
Private Sub MontarTodasDescricoes(ByRef rsDescricao As Recordset, _
                                  ByVal lNumComentario As Long)

    Dim sDescricao As String

    On Error GoTo TrataErro

    'Cabe�alho da descricao
    sDescricao = ""

    sDescricao = sDescricao & "Coment�rio: " & lNumComentario & vbNewLine & vbNewLine

    sDescricao = sDescricao & "Assunto: " & rsDescricao("assunto") & vbNewLine & vbNewLine

    sDescricao = sDescricao & "Data de Inclus�o: " & Format(rsDescricao("dt_inclusao"), "dd/mm/yyyy") & vbNewLine & vbNewLine

    sDescricao = sDescricao & "Usu�rio de Inclus�o: " & rsDescricao("nome") & vbNewLine & vbNewLine

    If LCase(rsDescricao("restrito")) = "s" Then
        sDescricao = sDescricao & "Restri��o: Restrito" & vbNewLine & vbNewLine
    Else
        sDescricao = sDescricao & "Restri��o: N�o restrito" & vbNewLine & vbNewLine
    End If

    'Descri��o
    sDescricao = sDescricao & "Descri��o:" & vbNewLine & vbNewLine

    If UCase(Trim(rsDescricao("restrito"))) = "S" _
       And UCase(Trim(rsDescricao("usuario_unidade_permitida"))) = "N" Then
        sDescricao = sDescricao & "Coment�rio restrito" & vbNewLine & vbNewLine
    Else
        sDescricao = sDescricao & rsDescricao("descricao") & vbNewLine & vbNewLine
    End If

    ReDim Preserve sTodasDescricoes(lNumComentario + 1) As String

    sTodasDescricoes(lNumComentario) = sDescricao

    Exit Sub

TrataErro:
    Call TratarErro("MontarTodasDescricoes", Me.name)
    Call FinalizarAplicacao
End Sub

'*** Inclus�o do Sinalizado de PPE - Pessoas politicamente expostas - demanda 269994
'*** 17/09/2008 - Fabio (Stefanini)

Private Function IdentificaPPE(strCPF As String) As Boolean

'Sessao de variaveis
    Dim StrSQL As String
    Dim rc As rdoResultset

    'Manipulador de erros
    On Error GoTo ErrIdentificaPPE

    'Valor Padrao
    IdentificaPPE = False

    'Retira Formacao caso exista
    strCPF = Replace(strCPF, ".", "")
    strCPF = Replace(strCPF, "-", "")
    strCPF = Replace(strCPF, "/", "")

    'Busca Identificar se faz parte do PPE
    'strSql = "SELECT COUNT(*) AS TOTAL "'alterado 12/09/2013 projeto 17860335 - Melhorias no SEGBR MC_Amaral -confitec
    StrSQL = "SELECT COUNT(1) AS TOTAL "
    StrSQL = StrSQL & " FROM seguros_db..PPE_ORGAO_CARGO_TB as TPPE WITH(NOLOCK)"
    StrSQL = StrSQL & " WHERE CAST(TPPE.CPF AS BIGINT) = CAST('" & strCPF & "' AS BIGINT)"
    StrSQL = StrSQL & " AND (TPPE.DT_EXCLUSAO_PPE IS NULL OR  TPPE.DT_EXCLUSAO_PPE >= GETDATE())"

    'Executa o Comando
    Set rc = rdocn.OpenResultset(StrSQL)

    'Verifica se voltou registro
    If rc.EOF Then GoTo Finaliza

    'Verifica Valor Valido
    If IsNull(rc!TOTAL) Then GoTo Finaliza

    'vERIFICA nao ENCONTROU
    If CInt(rc!TOTAL) <= 0 Then GoTo Finaliza

    'Valor Padrao
    IdentificaPPE = True

    'Finaliza
Finaliza:
    rc.Close
    Set rc = Nothing

    'Sai
    Exit Function

    'Manipulador de erros
ErrIdentificaPPE:

    'Fecha Recordsrt
    rc.Close
    Set rc = Nothing

End Function

'Odil Rincon Junior - 22/02/2010 - Demanda 3063240
Sub Carrega_Restituicao(proposta As String)

    Dim sSQL, linha As String
    Dim rs_endosso As rdoResultset
    Dim rs_avaliacao As rdoResultset
    Dim Situacao As Boolean

    On Error GoTo TrataErro

    sSQL = "  select  ef.val_financeiro, e.endosso_id, tp.tp_endosso_id, tp.descricao"
    sSQL = sSQL & "  from endosso_tb e  WITH (NOLOCK) "
    sSQL = sSQL & "      inner join endosso_financeiro_tb ef  WITH (NOLOCK) "
    sSQL = sSQL & "          on e.proposta_id = ef.proposta_id"
    sSQL = sSQL & "          and e.endosso_id = ef.endosso_id"
    sSQL = sSQL & "          and ef.val_financeiro < 0"
    sSQL = sSQL & "      inner join tp_endosso_tb tp  WITH (NOLOCK) "
    sSQL = sSQL & "          on e.tp_endosso_id = tp.tp_endosso_id"
    sSQL = sSQL & "  Where e.proposta_id = " & proposta    '& Val(txtProposta.Text)

    ' GUILHERME CRUZ -- CONFITEC SISTEMAS -- 26/07/2016
    sSQL = sSQL & " union " & vbNewLine

    sSQL = sSQL & "select (isnull(cr.valor_credito,0)*-1) as val_financeiro , e.endosso_id, tp.tp_endosso_id, tp.descricao " & vbNewLine
    sSQL = sSQL & "from conciliacao_restituicao_tb cr" & vbNewLine
    sSQL = sSQL & "join endosso_tb e  WITH (NOLOCK)" & vbNewLine
    sSQL = sSQL & "on e.proposta_id = cr.proposta_id" & vbNewLine
    sSQL = sSQL & "and e.endosso_id = cr.endosso_id" & vbNewLine
    sSQL = sSQL & "join tp_endosso_tb tp  WITH (NOLOCK)" & vbNewLine
    sSQL = sSQL & "on e.tp_endosso_id = tp.tp_endosso_id" & vbNewLine
    sSQL = sSQL & "Where cr.proposta_id = " & proposta & vbNewLine
    ' GUILHERME CRUZ -- CONFITEC SISTEMAS -- 26/07/2016

    Set rs_endosso = rdocn.OpenResultset(sSQL)


    While Not rs_endosso.EOF

        sSQL = "  select  *"
        sSQL = sSQL & "  From avaliacao_retorno_bb_tb WITH(NOLOCK)"
        sSQL = sSQL & "  Where proposta_id = " & proposta
        sSQL = sSQL & "  and     endosso_id = " & rs_endosso("ENDOSSO_ID")
        sSQL = sSQL & "  and     seq_avaliacao_retorno_bb_id = ( select max(seq_avaliacao_retorno_bb_id) "
        sSQL = sSQL & "  from avaliacao_retorno_bb_tb WITH(NOLOCK)"
        sSQL = sSQL & "  where proposta_id = " & proposta
        sSQL = sSQL & "  and     endosso_id =" & rs_endosso("endosso_id") & ")"

        Debug.Print sSQL & Chr(13)

        Set rs_avaliacao = rdoCn1.OpenResultset(sSQL)

        If Not rs_avaliacao.EOF Then
            If UCase(rs_avaliacao("aceite_bb")) = "S" Then
                If Mid(rs_avaliacao("seg_remessa_arq_retorno_bb"), 1, 6) = "SEG452" Then
                    linha = rs_endosso("endosso_id") & vbTab
                    linha = linha & rs_endosso("descricao") & vbTab
                    linha = linha & rs_avaliacao("seg_remessa_arq_retorno_bb") & vbTab
                    linha = linha & Format(rs_avaliacao("dt_processamento"), "dd/mm/yyyy") & vbTab
                    linha = linha & (rs_endosso("val_financeiro") * -1) & vbTab
                    linha = linha & "Restitui��o aceita pelo Banco"
                Else

                    'Comentado por Stefanini Consultoria, por Maur�cio A, em 10/09/2004
                    'pois mostrava msg de erro "Object doesn't support this property or method"
                    'Situacao = Situacao_restituicao(Val(txtProposta.Text), rs_endosso("endosso_id"))
                    'Situacao = Situacao_restituicao(Val(proposta), rs_endosso("endosso_id"))

                    linha = rs_endosso("endosso_id") & vbTab
                    linha = linha & rs_endosso("descricao") & vbTab
                    linha = linha & rs_avaliacao("seg_remessa_arq_retorno_bb") & vbTab
                    linha = linha & Format(rs_avaliacao("dt_processamento"), "dd/mm/yyyy") & vbTab
                    linha = linha & (rs_endosso("val_financeiro") * -1) & vbTab
                    If Situacao = True Then
                        linha = linha & "Aguardando retorno do banco para gerar restitui��o"
                    Else
                        linha = linha & "Restitui��o n�o foi processada"
                    End If
                End If

            Else
                If Mid(rs_avaliacao("seg_remessa_arq_retorno_bb"), 1, 6) = "SEG452" Then
                    linha = rs_endosso("endosso_id") & vbTab
                    linha = linha & rs_endosso("descricao") & vbTab
                    linha = linha & rs_avaliacao("seg_remessa_arq_retorno_bb") & vbTab
                    linha = linha & Format(rs_avaliacao("dt_processamento"), "dd/mm/yyyy") & vbTab
                    linha = linha & (rs_endosso("val_financeiro") * -1) & vbTab
                    linha = linha & "Restitui��o recusada pelo Banco"
                Else
                    linha = rs_endosso("endosso_id") & vbTab
                    linha = linha & rs_endosso("descricao") & vbTab
                    linha = linha & rs_avaliacao("seg_remessa_arq_retorno_bb") & vbTab
                    linha = linha & Format(rs_avaliacao("dt_processamento"), "dd/mm/yyyy") & vbTab
                    linha = linha & (rs_endosso("val_financeiro") * -1) & vbTab
                    linha = linha & "Endosso recusado pelo Banco"
                End If
            End If
            'Ralves - 05/01/2009 - FLOW 680584 - Altera��o para evitar a duplica��o de endosso no grid, quando algum endosso ainda n�o teve retorno do BB.
            GridRest.AddItem linha
            GridRest.ColAlignment(2) = flexAlignCenterCenter
            GridRest.ColAlignment(3) = flexAlignCenterCenter
            Set rs_avaliacao = Nothing

        End If
        '            NomeForm.GridRest.AddItem linha
        '            NomeForm.GridRest.ColAlignment(2) = flexAlignCenterCenter
        '            NomeForm.GridRest.ColAlignment(3) = flexAlignCenterCenter
        '            Set rs_avaliacao = Nothing
        'GUILHERME CRUZ -- CONFITEC SISTEMAS

        linha = rs_endosso("endosso_id") & vbTab
        linha = linha & rs_endosso("tp_endosso_id") & " - " & rs_endosso("descricao") & vbTab
        linha = linha & " " & vbTab
        linha = linha & " " & vbTab
        'linha = linha & Format(rs_avaliacao("dt_processamento"), "dd/mm/yyyy") & vbTab
        '                linha = linha & (rs_endosso("val_financeiro") * -1) & vbTab
        '                linha = linha & Format(Replace(Trim((rs_endosso("val_financeiro") * -1)), ".", ","), "###,###,##0.00") & vbTab
        linha = linha & Format(Replace(Trim((rs_endosso("val_financeiro"))), ".", ","), "###,###,##0.00") * -1 & vbTab    ' GUILHERMECRUZ -- CONFITEC RJ -- 10/10/2016
        linha = linha & rs_endosso("descricao") & vbTab
        'linha = linha & "Devolu��o de Restitui��o Gerado pelo Sistema"

        GridRest.AddItem linha
        GridRest.ColAlignment(2) = flexAlignCenterCenter
        GridRest.ColAlignment(3) = flexAlignCenterCenter
        Set rs_avaliacao = Nothing
        'GUILHERME CRUZ -- CONFITEC SISTEMAS
        rs_endosso.MoveNext



    Wend

    If GridRest.Rows > 2 Then
        For i = 1 To GridRest.Rows - 1
            If GridRest.TextMatrix(i, 0) = "" And GridRest.TextMatrix(i, 1) = "" And GridRest.TextMatrix(i, 3) = "" Then
                GridRest.RemoveItem (i)
                Exit For
            End If
        Next
    End If


    Set rs_endosso = Nothing

    'End If
    Exit Sub
TrataErro:
    Call TratarErro("Carrega_Restituicao")
    Call FinalizarAplicacao

End Sub
' Fim Odil Rincon Junior - 22/02/2010 - Demanda 3063240

'Sergey Souza - 10/09/2010
Private Sub Ler_Questionario()
    Dim rc As rdoResultset
    Dim i As Integer
    'Dim NovaCongenere As Congenere
    'Dim vAuxParticipacao    As Variant
    'Dim vAuxPercDespLider   As Variant
    Dim SQL As String

    On Error GoTo Erro

    SQL = "SELECT texto_pergunta, texto_resposta "
    SQL = SQL & "FROM questionario_proposta_tb  WITH (NOLOCK)  "
    SQL = SQL & "WHERE proposta_id = " & vPropostaId

    Set rc = rdocn.OpenResultset(SQL)

    grdQuestionario.Rows = 1
    grdQuestionario.Row = 0

    i = 0
    If Not rc.EOF Then
        SSTabProp.TabEnabled(TAB_QUESTIONARIO) = True
        '
        While Not rc.EOF
            With grdQuestionario
                .Rows = .Rows + 1
                .Row = .Rows - 1
                If Len(rc(0)) > 186 Then
                    .RowHeight(.Row) = .RowHeight(.Row) + (.RowHeight(.Row) * 2)
                ElseIf Len(rc(0)) > 93 Then
                    .RowHeight(.Row) = .RowHeight(.Row) * 2
                End If
                .TextMatrix(.Row, 0) = rc(0)

                .TextMatrix(.Row, 1) = Val(rc(1))

            End With

            i = i + 1
            rc.MoveNext
        Wend
        rc.Close
    End If

    SQL = "SELECT texto_pergunta, texto_resposta "
    SQL = SQL & "FROM questionario_proponente_tb  WITH (NOLOCK)  "
    SQL = SQL & "WHERE proposta_id = " & vPropostaId

    Set rc = rdocn.OpenResultset(SQL)


    If Not rc.EOF Then
        '
        While Not rc.EOF

            With grdQuestionario
                .Rows = .Rows + 1
                .Row = .Rows - 1
                If Len(rc(0)) > 186 Then
                    .RowHeight(.Row) = .RowHeight(.Row) + (.RowHeight(.Row) * 2)
                ElseIf Len(rc(0)) > 93 Then
                    .RowHeight(.Row) = .RowHeight(.Row) * 2
                End If
                .TextMatrix(.Row, 0) = rc(0)
                .TextMatrix(.Row, 1) = rc(1)

            End With

            rc.MoveNext
        Wend
        rc.Close
    End If
    Set rc = Nothing

    '    txtPercDespesa.Text = vValZero & " %"
    Exit Sub
Erro:
    TrataErroGeral "Ler Cosseguro"
    MsgBox "Rotina: Ler Cosseguro"
End Sub

Private Function ObterServidor() As String

    Dim rc As rdoResultset
    Dim sSQL As String

    On Error GoTo Erro

    ' Autor: pablo.cardoso (Nova Consultoria)
    ' Data da Altera��o: 09/09/2011
    ' Demanda: 11427194 - Altera��o da Gera��o de Fatura CDC Carteira
    ' obtem linked server do banco cdc_db

    sSQL = "EXEC controle_sistema_db.dbo.segs7641_sps 'cdc', " & CStr(glAmbiente_id)

    Set rc = rdocn.OpenResultset(sSQL)

    If Not rc.EOF Then
        If IsNull(rc(0)) Then
            ObterServidor = ""
        Else
            ObterServidor = "[" & Trim(rc(0)) & "]."
        End If
    Else
        ObterServidor = ""
    End If

    rc.Close
    Set rs = Nothing

    Exit Function
Erro:
    TrataErroGeral "ObterServidor"
    MsgBox "Rotina: ObterServidor"
End Function

Private Sub Monta_Gride_Carta_Cobranca()   'Zoro.Gomes - Confitec - 23/05/2014
    Dim sSQL As String, rc As rdoResultset, linha As String
    On Error GoTo TrataErro

    'monica - 02/02/2016
    sSQL = "SELECT * "
    sSQL = sSQL & "  FROM seguros_db.dbo.Carta_cobranca_tb (NOLOCK) "
    sSQL = sSQL & " WHERE proposta_id = " & txtNumero.Text

    If Not bPropostaRecusada And Not bPropostaNaoEmitida And Not bPropostaEstudo Then
        sSQL = sSQL & "   AND apolice_id = " & txtApolice.Text
    End If

    sSQL = sSQL & "   AND ramo_id = " & Left(txtRamo.Text, 2)
    sSQL = sSQL & " ORDER BY sequencial "

    'monica - 02/02/2016
    Set rc = rdocn.OpenResultset(sSQL)
    If Not rc.EOF Then
        fraCartaCobranca.Visible = True

        'N� Carta   |Dt.Impress�o   |num_deposito |dt_cancelamento |valor_corrigido |extenso
        msFlexCartaCobranca.Rows = 1
        Do While Not rc.EOF
            linha = Format(rc!sequencial, "00000") & "/" & rc!ano
            linha = linha & vbTab & Format(rc!dt_inclusao, "dd/mm/yyyy")

            linha = linha & vbTab & rc!num_deposito
            linha = linha & vbTab & rc!dt_cancelamento
            linha = linha & vbTab & Format(Replace(Trim(rc!valor_corrigido), ".", ","), "###,###,##0.00")
            linha = linha & vbTab & Extenso_Valor(Replace(Trim(rc!valor_corrigido), ".", ","))

            msFlexCartaCobranca.ColWidth(2) = 0
            msFlexCartaCobranca.ColWidth(3) = 0
            msFlexCartaCobranca.ColWidth(4) = 0
            msFlexCartaCobranca.ColWidth(5) = 0

            msFlexCartaCobranca.AddItem linha
            rc.MoveNext
        Loop
    Else
        rc.Close
    End If

    Exit Sub

TrataErro:
    MsgBox Err.Description, "Monta_Gride_Carta_Cobranca", vbCritical, "ERRO"
End Sub
Private Function Retorna_convenio_registra_boleto(ByVal convenio As String)

'SQL = " select distinct registra_boleto from seguros_db.dbo.convenio_tb with(nolock) " TRATANDO NULO
    SQL = ""
    SQL = "SELECT DISTINCT ISNULL(registra_boleto,'N') AS registra_boleto from seguros_db.dbo.convenio_tb with(nolock) "
    SQL = SQL & " where num_convenio = '" & convenio & "' "

    Set rs = ExecutarSQL(gsSIGLASISTEMA, _
                         glAmbiente_id, _
                         App.Title, _
                         App.FileDescription, _
                         SQL, _
                         True)

    If Not rs.EOF Then
        Retorna_convenio_registra_boleto = rs("registra_boleto")
    Else
        Retorna_convenio_registra_boleto = "N"
    End If

End Function
Private Function ProcessaRegistraBoletosBB(ByVal AProposta, ByVal ACobranca)

    Dim objXmlWebservice As Object

    Set objXmlWebservice = CreateObject("SEGL0365.WebServiceBB")

    RegistraBoleto = objXmlWebservice.ProcessaBoletoBB(Trim(AProposta), Trim(ACobranca), cUserName, _
                                                       gsSIGLASISTEMA, _
                                                       "SEGP01189", _
                                                       App.FileDescription, _
                                                       glAmbiente_id, _
                                                       cUserName)          'Marcio.Nogueira - 20/07/2017 - Demanda - 19368999 - Cobran�a registrada AB e ABS

    If RegistraBoleto = "" Then

        MsgBox "O Boleto n�o foi Registrado, falha no registro, a solicita��o n�o poder� ser processada!", vbCritical, "Gera Boleto"
        Me.MousePointer = 0
        ProcessaRegistraBoletosBB = ""
        BlnBoletoRegistrado = False

    Else

        BlnBoletoRegistrado = True

    End If
End Function
Function ValidaCorretor(gsCPF) As Boolean
'Isabeli Silva - Confitec SP - 2017-10-17
'MU-2017-036534 Cadastramento dos Corretores Exclusivos no SEGBR

    Dim sSQL As String
    Dim rsCorretor As rdoResultset
    Dim propostaCorretor As String

    If Trim(gsCPF) = "" Then

        If glAmbiente_id = 2 Or glAmbiente_id = 3 Or glAmbiente_id = 4 Then
            sSQL = ""
            sSQL = sSQL & " select isnull(cpf, '0') as cpf " & vbNewLine
            sSQL = sSQL & "   from segab_db.dbo.usuario_tb with (nolock) " & vbNewLine
            sSQL = sSQL & "  where login_rede = '" & cUserName & "' " & vbNewLine
            sSQL = sSQL & "    and situacao = 'a' " & vbNewLine
        Else
            sSQL = ""
            sSQL = sSQL & " select isnull(cpf, '0') as cpf " & vbNewLine
            sSQL = sSQL & "   from abss.segab_db.dbo.usuario_tb with (nolock) " & vbNewLine
            sSQL = sSQL & "  where login_rede = '" & cUserName & "' " & vbNewLine
            sSQL = sSQL & "    and situacao = 'a' " & vbNewLine
        End If

        Set rsCorretor = rdocn.OpenResultset(sSQL)

        If Not rsCorretor.EOF Then
            gsCPF = rsCorretor(0)
        End If

        rsCorretor.Close
        Set rsCorretor = Nothing
    End If

    sSQL = ""
    sSQL = sSQL & "EXEC seguros_db.dbo.SEGS13508_SPS " & vbNewLine
    sSQL = sSQL & "'" & gsCPF & "'"

    'Set rsCorretor = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, sSQL, True)
    Set rsCorretor = rdocn.OpenResultset(sSQL)

    If Not rsCorretor.EOF Then

        propostaCorretor = rsCorretor(0)

        If propostaCorretor = "s" Then
            ValidaCorretor = True
            rsCorretor.Close
        Else
            ValidaCorretor = False
            rsCorretor.Close
        End If
    End If

    Set rsCorretor = Nothing

    Exit Function

End Function
Function ValidaCorretorProposta(ByVal iPropostaId As Long) As Boolean
'Isabeli Silva - Confitec SP - 2017-10-17
'MU-2017-036534 Cadastramento dos Corretores Exclusivos no SEGBR


    Dim sSQL As String
    Dim rsCorretorProposta As rdoResultset
    Dim propostaCorretora As String

    If Trim(gsCPF) = "" Then

        If glAmbiente_id = 2 Or glAmbiente_id = 3 Or glAmbiente_id = 4 Then
            sSQL = ""
            sSQL = sSQL & " select isnull(cpf, '0') as cpf " & vbNewLine
            sSQL = sSQL & "   from segab_db.dbo.usuario_tb with (nolock) " & vbNewLine
            sSQL = sSQL & "  where login_rede = '" & cUserName & "' " & vbNewLine
            sSQL = sSQL & "    and situacao = 'a' " & vbNewLine
        Else
            sSQL = ""
            sSQL = sSQL & " select isnull(cpf, '0') as cpf " & vbNewLine
            sSQL = sSQL & "   from abss.segab_db.dbo.usuario_tb with (nolock) " & vbNewLine
            sSQL = sSQL & "  where login_rede = '" & cUserName & "' " & vbNewLine
            sSQL = sSQL & "    and situacao = 'a' " & vbNewLine
        End If

        Set rsCorretor = rdocn.OpenResultset(sSQL)

        If Not rsCorretor.EOF Then
            gsCPF = rsCorretor(0)
        End If

        rsCorretor.Close
        Set rsCorretor = Nothing
    End If

    sSQL = ""
    sSQL = sSQL & "EXEC seguros_db.dbo.SEGS13508_SPS " & vbNewLine
    sSQL = sSQL & "'" & gsCPF & "'"
    sSQL = sSQL & ", " & iPropostaId

    Set rsCorretorProposta = rdocn.OpenResultset(sSQL)

    If Not rsCorretorProposta.EOF Then
        propostaCorretora = rsCorretorProposta("CorretorCorretora")

        If propostaCorretora = "s" Then
            ValidaCorretorProposta = True
            rsCorretorProposta.Close
        Else
            ValidaCorretorProposta = False
            rsCorretorProposta.Close
        End If
    End If

    Set rsCorretorProposta = Nothing

End Function

'[Inicio] - Jessica.Adao - Confitec Sistemas - 20181019 - Sala �gil - Sprint 10: Ajustes Amparo
Private Function ObterISAmparo(lPropostaId As Long) As String

    On Error GoTo TrataErro

    Dim sSQL As String

    sSQL = ""

    'sSQL = sSQL & "select sum(cbt_ctr.vl_iptc_cbt_ctr) as val_is" & vbNewLine
    sSQL = sSQL & "select CAST(sum(cbt_ctr.vl_iptc_cbt_ctr) as numeric(9,2)) as val_is" & vbNewLine

    sSQL = sSQL & "  from seguros_db.dbo.proposta_processo_susep_tb proposta_processo_susep_tb with (nolock)" & vbNewLine
    sSQL = sSQL & " inner" & vbNewLine
    sSQL = sSQL & "  join als_operacao_db.dbo.cbt_ctr cbt_ctr with (nolock)" & vbNewLine
    sSQL = sSQL & "    on cbt_ctr.cd_prd = proposta_processo_susep_tb.cod_produto" & vbNewLine
    sSQL = sSQL & "   and cbt_ctr.cd_mdld = proposta_processo_susep_tb.cod_modalidade" & vbNewLine
    sSQL = sSQL & "   and cbt_ctr.cd_item_mdld = proposta_processo_susep_tb.cod_item_modalidade" & vbNewLine
    sSQL = sSQL & "   and cbt_ctr.nr_ctr_sgro = proposta_processo_susep_tb.num_contrato_seguro" & vbNewLine
    sSQL = sSQL & "   and cbt_ctr.nr_vrs_eds = ( select max(aux.nr_vrs_eds)" & vbNewLine
    sSQL = sSQL & "                                from als_operacao_db.dbo.cbt_ctr aux with (nolock)" & vbNewLine
    sSQL = sSQL & "                               where aux.cd_prd = cbt_ctr.cd_prd" & vbNewLine
    sSQL = sSQL & "                                 and aux.cd_mdld = cbt_ctr.cd_mdld" & vbNewLine
    sSQL = sSQL & "                                 and aux.cd_item_mdld = cbt_ctr.cd_item_mdld" & vbNewLine
    sSQL = sSQL & "                                 and aux.nr_ctr_sgro = cbt_ctr.nr_ctr_sgro )" & vbNewLine
    sSQL = sSQL & " where proposta_processo_susep_tb.proposta_id = " & lPropostaId & vbNewLine

    ObterISAmparo = sSQL

    Exit Function
    Resume
TrataErro:
    Call TratarErro("ObterISAmparo", Me.name)
    Call FinalizarAplicacao
End Function

Private Function ObterDadosCoberturasAmparo(lPropostaId As Long) As String

    On Error GoTo TrataErro

    Dim sSQL As String

    sSQL = ""




    sSQL = sSQL & "select escolha_tp_cob_vida_tb.tp_componente_id" & vbNewLine
    sSQL = sSQL & "     , cbt_ctr.cd_cbt as tp_cob_comp_id, tp_cobertura_tb.nome" & vbNewLine
    sSQL = sSQL & "     , cast(cbt_ctr.vl_iptc_cbt_ctr as numeric(9,2)) as val_is "
    sSQL = sSQL & "     , cast(cbt_ctr.vl_prem_cbt_ctr as numeric(9,2)) as val_premio " & vbNewLine
    sSQL = sSQL & "     , escolha_tp_cob_vida_tb.dt_inicio_vigencia_esc, escolha_tp_cob_vida_tb.dt_fim_vigencia_esc" & vbNewLine
    sSQL = sSQL & "     , isnull(cast(cbt_ctr.pc_prem_cbt_ctr as numeric(10,7)), 0) as val_taxa, isnull(cbt_ctr.pc_iptc_cbt_ctr, 0) as perc_basica" & vbNewLine
    sSQL = sSQL & "     , escolha_tp_cob_vida_tb.class_tp_cobertura" & vbNewLine
    sSQL = sSQL & "     , cast(cbt_ctr.vl_prem_cbt_ctr as numeric(9,2)) as val_premio_tarifa" & vbNewLine
    sSQL = sSQL & "     , cast(cbt_ctr.vl_prem_cbt_ctr as numeric(9,2)) as val_premio_liq" & vbNewLine
    sSQL = sSQL & "     , cast(cbt_ctr.vl_iptc_cbt_ctr as numeric(9,2)) as val_is_tarifa" & vbNewLine
    sSQL = sSQL & "  from seguros_db.dbo.proposta_tb proposta_tb with (nolock)" & vbNewLine
    sSQL = sSQL & " inner" & vbNewLine
    sSQL = sSQL & "  join seguros_db.dbo.proposta_processo_susep_tb proposta_processo_susep_tb with (nolock)" & vbNewLine
    sSQL = sSQL & "    on proposta_processo_susep_tb.proposta_id = proposta_tb.proposta_id" & vbNewLine
    sSQL = sSQL & " inner" & vbNewLine
    sSQL = sSQL & "  join als_operacao_db.dbo.cbt_ctr cbt_ctr with (nolock)" & vbNewLine
    sSQL = sSQL & "    on cbt_ctr.cd_prd = proposta_processo_susep_tb.cod_produto" & vbNewLine
    sSQL = sSQL & "   and cbt_ctr.cd_mdld = proposta_processo_susep_tb.cod_modalidade" & vbNewLine
    sSQL = sSQL & "   and cbt_ctr.cd_item_mdld = proposta_processo_susep_tb.cod_item_modalidade" & vbNewLine
    sSQL = sSQL & "   and cbt_ctr.nr_ctr_sgro = proposta_processo_susep_tb.num_contrato_seguro" & vbNewLine
    sSQL = sSQL & "   and cbt_ctr.nr_vrs_eds = ( select max(aux.nr_vrs_eds)" & vbNewLine
    sSQL = sSQL & "                                from als_operacao_db.dbo.cbt_ctr aux with (nolock)" & vbNewLine
    sSQL = sSQL & "                               Where aux.cd_prd = cbt_ctr.cd_prd" & vbNewLine
    sSQL = sSQL & "                                 and aux.cd_mdld = cbt_ctr.cd_mdld" & vbNewLine
    sSQL = sSQL & "                                 and aux.cd_item_mdld = cbt_ctr.cd_item_mdld" & vbNewLine
    sSQL = sSQL & "                                 and aux.nr_ctr_sgro = cbt_ctr.nr_ctr_sgro )" & vbNewLine
    sSQL = sSQL & " inner" & vbNewLine
    sSQL = sSQL & "  join seguros_db.dbo.escolha_tp_cob_vida_tb escolha_tp_cob_vida_tb with (nolock)" & vbNewLine
    sSQL = sSQL & "    on escolha_tp_cob_vida_tb.proposta_id = proposta_tb.proposta_id" & vbNewLine
    sSQL = sSQL & " inner" & vbNewLine
    sSQL = sSQL & "  join seguros_db.dbo.tp_cob_comp_plano_tb tp_cob_comp_plano_tb with (nolock)" & vbNewLine
    sSQL = sSQL & "    on tp_cob_comp_plano_tb.tp_cob_comp_id = escolha_tp_cob_vida_tb.tp_cob_comp_id" & vbNewLine
    sSQL = sSQL & " inner" & vbNewLine
    sSQL = sSQL & "  join seguros_db.dbo.plano_tb plano_tb with (nolock)" & vbNewLine
    sSQL = sSQL & "    on plano_tb.produto_id = proposta_tb.produto_id" & vbNewLine
    sSQL = sSQL & "   and plano_tb.tp_plano_id = tp_cob_comp_plano_tb.tp_plano_id" & vbNewLine

    'Altera��o para solu��o de duplicidade de coberturas, devido ao versionamento incorreto do produto pela area de produtos em 01/10/2018
    'IM00571423 - Confitec - 04/12/2018 - Alexandre Debouch
    'Adicionamos apenas a linhas abaixo. Nenhuma outra linha foi alterada ou adicionada alem desta.

    sSQL = sSQL & "   and plano_tb.dt_fim_vigencia is null " & vbNewLine

    'Adicionamos apenas a linhas abaixo. Nenhuma outra linha foi alterada ou adicionada alem desta.
    'IM00571423 - Confitec - 04/12/2018 - Alexandre Debouch
    'Altera��o para solu��o de duplicidade de coberturas, devido ao versionamento incorreto do produto pela area de produtos em 01/10/2018

    sSQL = sSQL & " inner" & vbNewLine
    sSQL = sSQL & "  join seguros_db.dbo.tp_cob_comp_tb tp_cob_comp_tb with (nolock)" & vbNewLine
    sSQL = sSQL & "    on tp_cob_comp_tb.tp_cob_comp_id = tp_cob_comp_plano_tb.tp_cob_comp_id" & vbNewLine
    sSQL = sSQL & "   and tp_cob_comp_tb.tp_cobertura_id = cbt_ctr.cd_cbt" & vbNewLine
    sSQL = sSQL & " inner" & vbNewLine
    sSQL = sSQL & "  join seguros_db.dbo.tp_cobertura_tb tp_cobertura_tb with (nolock)" & vbNewLine
    sSQL = sSQL & "    on tp_cobertura_tb.tp_cobertura_id = cbt_ctr.cd_cbt" & vbNewLine
    sSQL = sSQL & " where proposta_tb.proposta_id = " & lPropostaId & vbNewLine
    sSQL = sSQL & "   and escolha_tp_cob_vida_tb.dt_fim_vigencia_esc is null" & vbNewLine

    ObterDadosCoberturasAmparo = sSQL

    Exit Function
    Resume
TrataErro:
    Call TratarErro("ObterDadosCoberturasAmparo", Me.name)
    Call FinalizarAplicacao
End Function

Private Sub carregaFinanceiroAmparo()
    Dim sSQL As String, rRs As rdoResultset

    On Error GoTo TrataErro

    sSQL = ""
    '    sSQL = sSQL & "select isnull(dado_fnco_ctr.vl_iof_ctr,0) as val_iof, isnull(dado_fnco_ctr.vl_prem_moen_ctr,0) as val_premio, isnull(dado_fnco_ctr.vl_lqdo_moen_ctr,0) as val_premio_liquido" & vbNewLine
    sSQL = sSQL & "select isnull(convert(numeric(9,2), dado_fnco_ctr.vl_iof_ctr),0) as val_iof, isnull(convert(numeric(15,2), dado_fnco_ctr.vl_prem_moen_ctr),0) as val_premio, isnull(convert(numeric(15,2), dado_fnco_ctr.vl_lqdo_moen_ctr),0) as val_premio_liquido" & vbNewLine
    sSQL = sSQL & "  from seguros_db.dbo.proposta_processo_susep_tb proposta_processo_susep_tb with (nolock)" & vbNewLine
    sSQL = sSQL & " inner" & vbNewLine
    sSQL = sSQL & "  join als_operacao_db.dbo.dado_fnco_ctr dado_fnco_ctr with (nolock)" & vbNewLine
    sSQL = sSQL & "    on dado_fnco_ctr.cd_prd = proposta_processo_susep_tb.cod_produto" & vbNewLine
    sSQL = sSQL & "   and dado_fnco_ctr.cd_mdld = proposta_processo_susep_tb.cod_modalidade" & vbNewLine
    sSQL = sSQL & "   and dado_fnco_ctr.cd_item_mdld = proposta_processo_susep_tb.cod_item_modalidade" & vbNewLine
    sSQL = sSQL & "   and dado_fnco_ctr.nr_ctr_sgro = proposta_processo_susep_tb.num_contrato_seguro" & vbNewLine
    sSQL = sSQL & "   and dado_fnco_ctr.nr_vrs_eds = ( select max(aux.nr_vrs_eds)" & vbNewLine
    sSQL = sSQL & "                                      from als_operacao_db.dbo.dado_fnco_ctr aux with (nolock)" & vbNewLine
    sSQL = sSQL & "                                     Where aux.cd_prd = dado_fnco_ctr.cd_prd" & vbNewLine
    sSQL = sSQL & "                                       and aux.cd_mdld = dado_fnco_ctr.cd_mdld" & vbNewLine
    sSQL = sSQL & "                                       and aux.cd_item_mdld = dado_fnco_ctr.cd_item_mdld" & vbNewLine
    sSQL = sSQL & "                                       and aux.nr_ctr_sgro = dado_fnco_ctr.nr_ctr_sgro )" & vbNewLine
    sSQL = sSQL & " where proposta_processo_susep_tb.proposta_id = " & txtNumero.Text

    Set rRs = rdocn.OpenResultset(sSQL)

    If Not rRs.EOF Then

        If ConfiguracaoBrasil Then
            txtIOF.Text = Format(Val(rRs!val_iof), "###,###,###,##0.00")
            txtTotalPremioLiq.Text = Format(Val(rRs!val_premio_liquido), "###,###,###,##0.00")
            txtPremioBruto.Text = Format(Val(rRs!val_Premio), "###,###,###,##0.00")
        Else
            txtIOF.Text = TrocaValorAmePorBras(Format(Val(rRs!val_iof), "###,###,###,##0.00"))
            txtTotalPremioLiq.Text = TrocaValorAmePorBras(Format(Val(rRs!val_premio_liquido), "###,###,###,##0.00"))
            txtPremioBruto.Text = TrocaValorAmePorBras(Format(Val(rRs!val_Premio), "###,###,###,##0.00"))
        End If
    Else
        txtIOF.Text = vValZero
        txtTotalPremioLiq.Text = vValZero
        txtPremioBruto.Text = vValZero
    End If

    rRs.Close

    Exit Sub

TrataErro:
    Call TratarErro("carregaFinanceiroAmparo", Me.name)
    Call FinalizarAplicacao
End Sub

Private Function ObterApoliceAmparo(lPropostaId As Long) As String

    On Error GoTo TrataErro

    Dim sSQL As String

    sSQL = ""

    sSQL = sSQL & "select apolice_tb.apolice_id, apolice_tb.ramo_id, apolice_tb.sucursal_seguradora_id, apolice_tb.seguradora_cod_susep, apolice_tb.dt_emissao" & vbNewLine
    sSQL = sSQL & "     , apolice_tb.dt_inicio_vigencia, isnull(endosso_tb.dt_fim_vigencia_end, apolice_tb.dt_fim_vigencia) as dt_fim_vigencia, apolice_tb.tp_emissao, isnull(apolice_tb.val_lim_retencao_resseg, 0) as val_lim_retencao_resseg" & vbNewLine
    sSQL = sSQL & "  from seguros_db.dbo.apolice_tb apolice_tb with (nolock)" & vbNewLine
    sSQL = sSQL & "  left" & vbNewLine
    sSQL = sSQL & "  join seguros_db.dbo.endosso_tb endosso_tb with (nolock)" & vbNewLine
    sSQL = sSQL & "    on endosso_tb.proposta_id = apolice_tb.proposta_id" & vbNewLine
    sSQL = sSQL & "   and (endosso_tb.tp_endosso_id = 314" & vbNewLine
    sSQL = sSQL & "         and endosso_tb.endosso_id = ( select max(aux.endosso_id)" & vbNewLine
    sSQL = sSQL & "                                         from seguros_db.dbo.endosso_tb aux with (nolock)" & vbNewLine
    sSQL = sSQL & "                                        Where aux.proposta_id = apolice_tb.proposta_id" & vbNewLine
    sSQL = sSQL & "                                          and aux.tp_endosso_id = 314 ))" & vbNewLine
    sSQL = sSQL & " Where apolice_tb.proposta_id = " & lPropostaId & vbNewLine

    ObterApoliceAmparo = sSQL

    Exit Function
    Resume
TrataErro:
    Call TratarErro("ObterApoliceAmparo", Me.name)
    Call FinalizarAplicacao
End Function


'[Fim] - Jessica.Adao - Confitec Sistemas - 20181019 - Sala �gil - Sprint 10: Ajustes Amparo

Public Function PossuiFluxoDigital(iProduto As Integer) As Boolean

    Dim sSQL As String
    Dim rs As rdoResultset
        
    sSQL = ""
    sSQL = "      SELECT TOP 1 dt_implantacao_digital "
    sSQL = sSQL & " FROM interface_db.dbo.layout_produto_tb WITH (NOLOCK) "
    sSQL = sSQL & "WHERE produto_id = " & iProduto
    sSQL = sSQL & "  AND dt_implantacao_digital IS NOT NULL "
    
    Set rs = rdocn.OpenResultset(sSQL)
                    
    If rs.EOF Then
        PossuiFluxoDigital = False
    Else
        PossuiFluxoDigital = True
        rs.Close
        Set rs = Nothing
    End If

End Function

Public Function VerificaFluxo(lPropostaId As Long) As String

    Dim sSQL As String
    Dim rs As rdoResultset
        
    sSQL = ""
    sSQL = "      SELECT CASE WHEN p.tp_documento IS NULL THEN CASE WHEN p.dt_contratacao >= l.dt_implantacao_digital "
    sSQL = sSQL & "                                                 THEN 'Digital' "
    sSQL = sSQL & "                                                 ELSE 'Fisico' "
    sSQL = sSQL & "                                             END "
    sSQL = sSQL & "           WHEN p.tp_documento = 'D' THEN 'Digital' "
    sSQL = sSQL & "           ELSE 'Fisico' "
    sSQL = sSQL & "       END tipo_documento "
    sSQL = sSQL & " FROM seguros_db.dbo.proposta_tb p WITH (NOLOCK) "
    sSQL = sSQL & "OUTER APPLY (SELECT MIN(dt_implantacao_digital) dt_implantacao_digital "
    sSQL = sSQL & "               FROM interface_db.dbo.layout_produto_tb i WITH (NOLOCK) "
    sSQL = sSQL & "              WHERE p.produto_id = i.produto_id "
    sSQL = sSQL & "                AND i.dt_implantacao_digital IS NOT NULL) l "
    sSQL = sSQL & "WHERE p.proposta_id = " & lPropostaId
    
    Set rs = rdocn.OpenResultset(sSQL)
    
    If Not rs.EOF Then
        VerificaFluxo = rs!tipo_documento
        rs.Close
        Set rs = Nothing
    End If

End Function
