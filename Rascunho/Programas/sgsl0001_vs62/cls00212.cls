VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cls00212"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Public Sub ProcessarSpTratamentoEntrada(ByVal sSiglaSistema As String, _
                                        ByVal sSiglaRecurso As String, _
                                        ByVal sDescricaoRecurso As String, _
                                        ByVal iAmbienteId As Integer, _
                                        ByVal iEventoBB As Integer, _
                                        ByVal iEntradaMQId As Long, _
                                        ByVal sUsuario As String, _
                                        ByVal sDtSistema As String, _
                                        Optional ByVal sFlagTrataAbs As String)

    Dim sSql   As String

    On Error GoTo Trata_Erro

    sSql = sSql & "EXEC interface_dados_db..executa_sp_tratamento_evento_sps"
    sSql = sSql & "  " & iEventoBB
    sSql = sSql & ", " & iEntradaMQId
    sSql = sSql & ",'" & Format(sDtSistema, "YYYYMMDD") & "'"
    sSql = sSql & ",'" & sUsuario & "'"

    'luissantos - 07/09/2010
    If Not IsMissing(sFlagTrataAbs) And sFlagTrataAbs <> "" Then
        If sFlagTrataAbs Then
            sSql = sSql & ", 'S'"
        Else
            sSql = sSql & ", 'N'"
        End If
    End If

    Call ExecutarSQL(sSiglaSistema, _
                     iAmbienteId, _
                     sSiglaRecurso, _
                     sDescricaoRecurso, _
                     sSql, _
                     False)

    Exit Sub

Trata_Erro:

    Call Err.Raise(Err.Number, , "SGSL0001.cls00212.ProcessarSpTratamentoEntrada - " & Err.Description)

End Sub

Public Sub ProcessarSpMontagemRegistroSaida(ByVal sSiglaSistema As String, _
                                            ByVal sSiglaRecurso As String, _
                                            ByVal sDescricaoRecurso As String, _
                                            ByVal iAmbienteId As Integer, _
                                            ByVal DadoALSId As Long, _
                                            ByVal sUsuario As String, _
                                            ByVal sDtSistema As String, _
                                            Optional ByVal sFlagTrataAbs As String)

    Dim sSql   As String
    Dim rsTB   As ADODB.Recordset
    Dim SP_TRATAMENTO As String

    On Error GoTo Trata_Erro

    ''ALTERADO POR JUNIOR EM 28/04/2004 PARA OBTER A STORED PROCEDURE AUTOMATICAMENTE - INICIO
    sSql = ""
    '' PRODUTO
    sSql = sSql & " SELECT sp_tratamento_evento"
    sSql = sSql & " FROM interface_dados_db..evento_als_layout_tb evento_als_layout_tb"
    sSql = sSql & "     JOIN interface_dados_db..dado_als_layout_coluna_produto_tb dado_als_layout_coluna_produto_tb"
    sSql = sSql & "       ON dado_als_layout_coluna_produto_tb.evento_bb = evento_als_layout_tb.evento_bb"
    sSql = sSql & "     JOIN interface_dados_db..dado_als_layout_produto_tb dado_als_layout_produto_tb"
    sSql = sSql & "       ON dado_als_layout_produto_tb.dados_als_id = dado_als_layout_coluna_produto_tb.dados_als_id"
    sSql = sSql & " WHERE dado_als_layout_produto_tb.dados_als_id = " & DadoALSId
    sSql = sSql & "   AND dado_als_layout_produto_tb.dt_envio IS NULL"
    sSql = sSql & " UNION "
    '' COTA��O
    sSql = sSql & " SELECT sp_tratamento_evento"
    sSql = sSql & " FROM interface_dados_db..evento_als_layout_tb evento_als_layout_tb"
    sSql = sSql & "     JOIN interface_dados_db..dado_als_layout_coluna_cotacao_tb dado_als_layout_coluna_cotacao_tb"
    sSql = sSql & "       ON dado_als_layout_coluna_cotacao_tb.evento_bb = evento_als_layout_tb.evento_bb"
    sSql = sSql & "     JOIN interface_dados_db..dado_als_layout_cotacao_tb dado_als_layout_cotacao_tb"
    sSql = sSql & "       ON dado_als_layout_cotacao_tb.dados_als_id = dado_als_layout_coluna_cotacao_tb.dados_als_id"
    sSql = sSql & " WHERE dado_als_layout_cotacao_tb.dados_als_id = " & DadoALSId
    sSql = sSql & "   AND dado_als_layout_cotacao_tb.dt_envio IS NULL"
    sSql = sSql & " UNION "
    '' CONTRATA��O
    sSql = sSql & " SELECT sp_tratamento_evento"
    sSql = sSql & " FROM interface_dados_db..evento_als_layout_tb evento_als_layout_tb"
    sSql = sSql & "     JOIN interface_dados_db..dado_als_layout_coluna_contrato_tb dado_als_layout_coluna_contrato_tb"
    sSql = sSql & "       ON dado_als_layout_coluna_contrato_tb.evento_bb = evento_als_layout_tb.evento_bb"
    sSql = sSql & "     JOIN interface_dados_db..dado_als_layout_contrato_tb dado_als_layout_contrato_tb"
    sSql = sSql & "       ON dado_als_layout_contrato_tb.dados_als_id = dado_als_layout_coluna_contrato_tb.dados_als_id"
    sSql = sSql & " WHERE dado_als_layout_contrato_tb.dados_als_id = " & DadoALSId
    sSql = sSql & "   AND dado_als_layout_contrato_tb.dt_envio IS NULL"
    sSql = sSql & " UNION "
    '' MCI
    sSql = sSql & " SELECT sp_tratamento_evento"
    sSql = sSql & " FROM interface_dados_db..evento_als_layout_tb evento_als_layout_tb"
    sSql = sSql & "     JOIN interface_dados_db..dado_als_layout_coluna_mci_tb dado_als_layout_coluna_mci_tb"
    sSql = sSql & "       ON dado_als_layout_coluna_mci_tb.evento_bb = evento_als_layout_tb.evento_bb"
    sSql = sSql & "     JOIN interface_dados_db..dado_als_layout_mci_tb dado_als_layout_mci_tb"
    sSql = sSql & "       ON dado_als_layout_mci_tb.dados_als_id = dado_als_layout_coluna_mci_tb.dados_als_id"
    sSql = sSql & " WHERE dado_als_layout_mci_tb.dados_als_id = " & DadoALSId
    sSql = sSql & "   AND dado_als_layout_mci_tb.dt_envio IS NULL"
    Set rsTB = ExecutarSQL(sSiglaSistema, _
                           iAmbienteId, _
                           sSiglaRecurso, _
                           sDescricaoRecurso, _
                           sSql, _
                           True)


    If rsTB.EOF Then GoTo Trata_Erro

    SP_TRATAMENTO = rsTB(0)

    rsTB.Close

    ''ALTERADO POR JUNIOR EM 28/04/2004 PARA OBTER A STORED PROCEDURE AUTOMATICAMENTE - INICIO

    sSql = ""
    sSql = sSql & "EXEC interface_dados_db.." & SP_TRATAMENTO
    sSql = sSql & "  " & DadoALSId
    sSql = sSql & ",'" & Format(sDtSistema, "yyyymmdd") & "'"
    sSql = sSql & ",'" & sUsuario & "'"

    'luissantos - 07/09/2010
    If Not IsMissing(sFlagTrataAbs) And sFlagTrataAbs <> "" Then
        If sFlagTrataAbs Then
            sSql = sSql & ", 'S'"
        Else
            sSql = sSql & ", 'N'"
        End If
    End If

    Call ExecutarSQL(sSiglaSistema, _
                     iAmbienteId, _
                     sSiglaRecurso, _
                     sDescricaoRecurso, _
                     sSql, _
                     False)

    Exit Sub

Trata_Erro:

    sSql = ""
    sSql = sSql & "EXEC interface_dados_db..SMQS00151_SPU " & "'" & SP_TRATAMENTO & "',"
    sSql = sSql & DadoALSId & ";"


    Call ExecutarSQL(sSiglaSistema, _
                     iAmbienteId, _
                     sSiglaRecurso, _
                     sDescricaoRecurso, _
                     sSql, _
                     False)

    Err.Clear

    'Call Err.Raise(Err.Number, , "SGSL0001.cls00212.ProcessarSpMontagemRegistroSaida - " & Err.Description)

End Sub

Public Function ConsultarDadoAlsEmissao(ByVal sSiglaSistema As String, _
                                        ByVal sSiglaRecurso As String, _
                                        ByVal sDescricaoRecurso As String, _
                                        ByVal iAmbienteId As Integer, _
                                        ByVal iEventoBB As Integer, _
                                        Optional ByVal lWorkFlowId As Long = 0) As Object

    Dim sSql   As String

    On Error GoTo Trata_Erro

    sSql = ""
    sSql = sSql & "SET NOCOUNT ON" & vbNewLine
    sSql = sSql & "EXEC interface_dados_db..consulta_dado_als_emissao_sps" & vbNewLine
    sSql = sSql & iEventoBB & vbNewLine
    'INCLU�DO POR JUNIOR EM 05/09/2005 - INICIO
    '''    sSql = sSql & ", " & IIf(lWorkFlowId = 0, "NULL", lWorkFlowId) & vbNewLine
    'INCLU�DO POR JUNIOR EM 05/09/2005 - FIM

    Set ConsultarDadoAlsEmissao = ExecutarSQL(sSiglaSistema, _
                                              iAmbienteId, _
                                              sSiglaRecurso, _
                                              sDescricaoRecurso, _
                                              sSql, _
                                              True)

    Exit Function

Trata_Erro:

    Call Err.Raise(Err.Number, , "SGSL0001.cls00212.VerificarDadoAlsEmissao - " & Err.Description)

End Function

Public Sub ProcessarSpIncluirEventoALS(ByVal sSiglaSistema As String, _
                                       ByVal sSiglaRecurso As String, _
                                       ByVal sDescricaoRecurso As String, _
                                       ByVal iAmbienteId As Integer, _
                                       ByVal iEventoBB As Integer, _
                                       ByVal sRegistro As String, _
                                       ByVal sUsuario As String)

    Dim sSql   As String

    On Error GoTo Trata_Erro

    sSql = ""
    sSql = sSql & "EXEC interface_dados_db..incluir_evento_als_spi"
    sSql = sSql & "  " & iEventoBB
    sSql = sSql & ",'" & sRegistro & "'"
    sSql = sSql & ",'" & sUsuario & "'"

    Call ExecutarSQL(sSiglaSistema, _
                     iAmbienteId, _
                     sSiglaRecurso, _
                     sDescricaoRecurso, _
                     sSql, _
                     False)

    Exit Sub

Trata_Erro:

    Call Err.Raise(Err.Number, , "SGSL0001.cls00212.ProcessarSpIncluirEventoALS - " & Err.Description)

End Sub


Public Sub ProcessarSpMontagemRegistroSaidaPROC(ByVal sSiglaSistema As String, _
                                                ByVal sSiglaRecurso As String, _
                                                ByVal sDescricaoRecurso As String, _
                                                ByVal iAmbienteId As Integer, _
                                                ByVal DadoALSId As Long, _
                                                ByVal sSp_Tratamento As String, _
                                                ByVal sUsuario As String, _
                                                ByVal sDtSistema As String, _
                                                Optional ByVal sFlagTrataAbs As String)

    Dim sSql   As String

    On Error GoTo Trata_Erro

    sSql = ""
    sSql = sSql & "EXEC interface_dados_db.." & sSp_Tratamento
    sSql = sSql & "  " & DadoALSId
    sSql = sSql & ",'" & Format(sDtSistema, "yyyymmdd") & "'"
    sSql = sSql & ",'" & sUsuario & "'"

    'luissantos - 07/09/2010
    If Not IsMissing(sFlagTrataAbs) And sFlagTrataAbs <> "" Then
        If sFlagTrataAbs Then
            sSql = sSql & ", 'S'"
        Else
            sSql = sSql & ", 'N'"
        End If
    End If

    Call ExecutarSQL(sSiglaSistema, _
                     iAmbienteId, _
                     sSiglaRecurso, _
                     sDescricaoRecurso, _
                     sSql, _
                     False)

    Exit Sub

Trata_Erro:

    sSql = ""
    sSql = sSql & "EXEC interface_dados_db..SMQS00151_SPU " & "'" & sSp_Tratamento & "',"
    sSql = sSql & DadoALSId & ";"


    Call ExecutarSQL(sSiglaSistema, _
                     iAmbienteId, _
                     sSiglaRecurso, _
                     sDescricaoRecurso, _
                     sSql, _
                     False)

    Err.Clear

    'Call Err.Raise(Err.Number, , "SGSL0001.cls00212.ProcessarSpMontagemRegistroSaida - " & Err.Description)

End Sub

