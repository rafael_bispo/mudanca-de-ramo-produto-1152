VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cls00210"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit

Public Sub ExcluirGrupoEvento(ByVal sSiglaSistema As String, _
                              ByVal sSiglaRecurso As String, _
                              ByVal sDescricaoRecurso As String, _
                              ByVal iAmbienteId As Integer, _
                              ByVal iGrupo_evento_als_id As Integer)


    Dim sSql   As String

    On Error GoTo Trata_Erro

    sSql = "SET NOCOUNT ON "
    sSql = "EXEC interface_dados_db..grupo_evento_als_spd " & iGrupo_evento_als_id

    Call ExecutarSQL(sSiglaSistema, _
                     iAmbienteId, _
                     sSiglaRecurso, _
                     sDescricaoRecurso, _
                     sSql, _
                     False)

    Exit Sub

Trata_Erro:

    Call Err.Raise(Err.Number, , "SGSL0001.cls00210.ExcluirGrupoEvento - " & Err.Description)

End Sub

Public Function AlterarGrupoEvento(ByVal sSiglaSistema As String, _
                                   ByVal sSiglaRecurso As String, _
                                   ByVal sDescricaoRecurso As String, _
                                   ByVal iAmbienteId As Integer, _
                                   ByVal iGrupoEventoAlsId As Integer, _
                                   ByVal sDescricao As String, _
                                   ByVal iSeqEvento As Integer, _
                                   ByVal sUsuario As String)

    Dim sSql   As String

    On Error GoTo Trata_Erro

    ' Incluindo Evento ''''''''''''''''''''''''''''''''''''''''''''

    sSql = "SET NOCOUNT ON "
    sSql = sSql & "EXEC interface_dados_db..grupo_evento_als_spu "
    sSql = sSql & "  " & iGrupoEventoAlsId
    sSql = sSql & ",'" & sDescricao & "'"
    sSql = sSql & ",  " & iSeqEvento
    sSql = sSql & ",'" & sUsuario & "'"

    Call ExecutarSQL(sSiglaSistema, _
                     iAmbienteId, _
                     sSiglaRecurso, _
                     sDescricaoRecurso, _
                     sSql, _
                     False)

    Exit Function

Trata_Erro:

    Call Err.Raise(Err.Number, , "SGSL0001.cls00210.AlterarGrupoEvento - " & Err.Description)

End Function

Public Function ConsultarEventoALSLayout(ByVal sSiglaSistema As String, _
                                         ByVal sSiglaRecurso As String, _
                                         ByVal sDescricaoRecurso As String, _
                                         ByVal iAmbienteId As Integer, _
                                         Optional ByVal iEventoBB As Integer = 0, _
                                         Optional ByVal sDescricao As String = "") As Object

    Dim sSql   As String

    On Error GoTo Trata_Erro

    sSql = ""
    sSql = sSql & "SELECT * " & vbNewLine
    sSql = sSql & "  FROM interface_dados_db..evento_als_layout_tb " & vbNewLine
    If iEventoBB > 0 Then
        sSql = sSql & " WHERE evento_bb = " & iEventoBB & vbNewLine

    ElseIf Trim(sDescricao) <> "" Then
        sSql = sSql & " WHERE descricao_evento LIKE '" & sDescricao & "%'" & vbNewLine
    End If

    sSql = sSql & " ORDER BY evento_bb"

    Set ConsultarEventoALSLayout = ExecutarSQL(sSiglaSistema, _
                                               iAmbienteId, _
                                               sSiglaRecurso, _
                                               sDescricaoRecurso, _
                                               sSql, _
                                               True)

    Exit Function

Trata_Erro:

    Call Err.Raise(Err.Number, , "SGSL0001.cls00210.ConsultarEventoALSLayout - " & Err.Description)

End Function

Public Function ConsultarEventoALSLayoutColuna(ByVal sSiglaSistema As String, _
                                               ByVal sSiglaRecurso As String, _
                                               ByVal sDescricaoRecurso As String, _
                                               ByVal iAmbienteId As Integer, _
                                               ByVal iEventoBB As Integer) As Object

    Dim sSql   As String

    On Error GoTo Trata_Erro

    sSql = ""
    sSql = sSql & "SELECT * " & vbNewLine
    sSql = sSql & "  FROM interface_dados_db..evento_als_layout_coluna_tb " & vbNewLine
    sSql = sSql & " WHERE evento_bb = " & iEventoBB & vbNewLine
    sSql = sSql & " ORDER BY seq_coluna"

    Set ConsultarEventoALSLayoutColuna = ExecutarSQL(sSiglaSistema, _
                                                     iAmbienteId, _
                                                     sSiglaRecurso, _
                                                     sDescricaoRecurso, _
                                                     sSql, _
                                                     True)

    Exit Function

Trata_Erro:

    Call Err.Raise(Err.Number, , "SGSL0001.cls00210.ConsultarEventoALSLayoutColuna - " & Err.Description)

End Function


Public Sub ExcluirEvento(ByVal sSiglaSistema As String, _
                         ByVal sSiglaRecurso As String, _
                         ByVal sDescricaoRecurso As String, _
                         ByVal iAmbienteId As Integer, _
                         ByVal iEventoBB As Integer)

    Dim sSql   As String

    On Error GoTo Trata_Erro

    sSql = "SET NOCOUNT ON "
    sSql = sSql & "EXEC interface_dados_db..excluir_evento_spd " & iEventoBB

    Call ExecutarSQL(sSiglaSistema, _
                     iAmbienteId, _
                     sSiglaRecurso, _
                     sDescricaoRecurso, _
                     sSql, _
                     False)

    Exit Sub

Trata_Erro:

    Call Err.Raise(Err.Number, , "SGSL0001.cls00210.ExcluirEvento - " & Err.Description)

End Sub

Public Function IncluirEventoALSLayout(ByVal sSiglaSistema As String, _
                                       ByVal sSiglaRecurso As String, _
                                       ByVal sDescricaoRecurso As String, _
                                       ByVal iAmbienteId As Integer, _
                                       ByVal sCodTransacao As String, _
                                       ByVal iEventoBB As Integer, _
                                       ByVal sNomeBanco As String, _
                                       ByVal sDescricao As String, _
                                       ByVal snometabela As String, _
                                       ByVal sSpTratamentoEvento As String, _
                                       ByVal sUsuario As String, _
                                       ByVal iLayoutId As Integer, _
                                       ByVal iGrupoEventoId As Integer, _
                                       ByVal sCondicao As String, _
                                       ByRef colColunas As Collection) As Object

    Dim sSql   As String
    Dim lConexao As Long

    On Error GoTo Trata_Erro

    ' Abrindo Conexao ''''''''''''''''''''''''''''''''''''''''''''

    lConexao = AbrirConexao(sSiglaSistema, _
                            iAmbienteId, _
                            sSiglaRecurso, _
                            sDescricaoRecurso)

    ' Abrindo Transacao '''''''''''''''''''''''''''''''''''''''''''

    If Not AbrirTransacao(lConexao) Then
        Call Err.Raise(vbObjectError + 1000, , "SGSL0001.cls00210.IncluirEventoALSLayout - N�o foi poss�vel abrir transa��o")
    End If

    ' Incluindo Evento ''''''''''''''''''''''''''''''''''''''''''''

    sSql = "SET NOCOUNT ON "
    sSql = sSql & "EXEC interface_dados_db..evento_als_layout_spi "
    sSql = sSql & "'" & sCodTransacao & "'"
    sSql = sSql & ", " & iEventoBB
    sSql = sSql & ",'" & sNomeBanco & "'"
    sSql = sSql & ",'" & sDescricao & "'"
    sSql = sSql & ",'" & snometabela & "'"
    sSql = sSql & ",'" & sSpTratamentoEvento & "'"
    sSql = sSql & ",'" & sUsuario & "'"
    sSql = sSql & ", " & iLayoutId
    sSql = sSql & ", " & iGrupoEventoId
    sSql = sSql & ",'" & sCondicao & "'"

    Call Conexao_ExecutarSQL(sSiglaSistema, _
                             iAmbienteId, _
                             sSiglaRecurso, _
                             sDescricaoRecurso, _
                             sSql, _
                             lConexao, _
                             False)

    ' Incluindo Colunas do Evento ''''''''''''''''''''''''''''''''''

    Call IncluirEventoALSLayoutColuna(sSiglaSistema, _
                                      sSiglaRecurso, _
                                      sDescricaoRecurso, _
                                      iAmbienteId, _
                                      iEventoBB, _
                                      colColunas, _
                                      sUsuario, _
                                      lConexao)

    ' Fechando Transacao '''''''''''''''''''''''''''''''''''''''''''''

    If Not ConfirmarTransacao(lConexao) Then
        Call Err.Raise(vbObjectError + 1000, , "SGSL0001.cls00210.IncluirEventoALSLayout - N�o foi poss�vel confirmar transa��o")
    End If

    ' Fechando Conexao '''''''''''''''''''''''''''''''''''''''''''''''

    If Not FecharConexao(lConexao) Then
        Call Err.Raise(vbObjectError + 1000, , "SGSL0001.cls00210.IncluirEventoALSLayout - N�o foi poss�vel fechar conex�o")
    End If

    Exit Function

Trata_Erro:

    Call Err.Raise(Err.Number, , "SGSL0001.cls00210.IncluirEventoALSLayout - " & Err.Description)

End Function

Public Function IncluirGrupoEvento(ByVal sSiglaSistema As String, _
                                   ByVal sSiglaRecurso As String, _
                                   ByVal sDescricaoRecurso As String, _
                                   ByVal iAmbienteId As Integer, _
                                   ByVal iGrupo_evento_als_id As Integer, _
                                   ByVal sDescricao As String, _
                                   ByVal iSeqEvento As Integer, _
                                   ByVal sUsuario As String)

    Dim sSql   As String
    Dim lConexao As Long

    On Error GoTo Trata_Erro

    ' Incluindo Evento ''''''''''''''''''''''''''''''''''''''''''''

    sSql = "SET NOCOUNT ON "
    sSql = sSql & "EXEC interface_dados_db..grupo_evento_als_spi "
    sSql = sSql & "   " & iGrupo_evento_als_id
    sSql = sSql & ", '" & sDescricao & "'"
    sSql = sSql & ",  " & iSeqEvento
    sSql = sSql & ", '" & sUsuario & "'"

    Call ExecutarSQL(sSiglaSistema, _
                     iAmbienteId, _
                     sSiglaRecurso, _
                     sDescricaoRecurso, _
                     sSql, _
                     False)


    Exit Function

Trata_Erro:

    Call Err.Raise(Err.Number, , "SGSL0001.cls00210.IncluirGrupoEvento - " & Err.Description)

End Function


Public Function IncluirEventoALSLayoutColuna(ByVal sSiglaSistema As String, _
                                             ByVal sSiglaRecurso As String, _
                                             ByVal sDescricaoRecurso As String, _
                                             ByVal iAmbienteId As Integer, _
                                             ByVal iEventoBB As Integer, _
                                             ByRef colColunas As Collection, _
                                             ByVal sUsuario As String, _
                                             ByVal lConexao As Long) As Object

    Dim oColuna As Object
    Dim iSequencial As Integer
    Dim sColuna As String
    Dim iTamanho As Integer
    Dim sIdentificador As String
    Dim iGrupoIdentificador As Integer

    Dim sSql   As String

    On Error GoTo Trata_Erro

    For Each oColuna In colColunas

        iSequencial = oColuna.Sequencial
        sColuna = oColuna.Nome
        iTamanho = oColuna.Tamanho
        sIdentificador = oColuna.Identificador
        iGrupoIdentificador = oColuna.GrupoIdentificador

        sSql = "SET NOCOUNT ON "
        sSql = sSql & "EXEC interface_dados_db..evento_als_layout_coluna_spi "
        sSql = sSql & "  " & iEventoBB
        sSql = sSql & ", " & iSequencial
        sSql = sSql & ",'" & sColuna & "'"
        sSql = sSql & ", " & iTamanho
        sSql = sSql & ",'" & sIdentificador & "'"
        sSql = sSql & ", " & iGrupoIdentificador
        sSql = sSql & ",'" & sUsuario & "'"

        Call Conexao_ExecutarSQL(sSiglaSistema, _
                                 iAmbienteId, _
                                 sSiglaRecurso, _
                                 sDescricaoRecurso, _
                                 sSql, _
                                 lConexao, _
                                 False)

    Next

    Set oColuna = Nothing

    Exit Function

Trata_Erro:
    Resume
    Call Err.Raise(Err.Number, , "SGSL0001.cls00210.IncluirEventoALSLayoutColuna - " & Err.Description)

End Function

Public Function AlterarEventoALSLayout(ByVal sSiglaSistema As String, _
                                       ByVal sSiglaRecurso As String, _
                                       ByVal sDescricaoRecurso As String, _
                                       ByVal iAmbienteId As Integer, _
                                       ByVal sCodTransacao As String, _
                                       ByVal iEventoBB As Integer, _
                                       ByVal sNomeBanco As String, _
                                       ByVal sDescricao As String, _
                                       ByVal snometabela As String, _
                                       ByVal sSpTratamentoEvento As String, _
                                       ByVal sUsuario As String, _
                                       ByVal iLayoutId As Integer, _
                                       ByVal iGrupoEventoId As Integer, _
                                       ByVal sCondicao As String, _
                                       ByRef colColunas As Collection) As Object

    Dim sSql   As String
    Dim lConexao As Long

    On Error GoTo Trata_Erro

    ' Abrindo Conexao ''''''''''''''''''''''''''''''''''''''''''''

    lConexao = AbrirConexao(sSiglaSistema, _
                            iAmbienteId, _
                            sSiglaRecurso, _
                            sDescricaoRecurso)

    ' Abrindo Transacao '''''''''''''''''''''''''''''''''''''''''''

    If Not AbrirTransacao(lConexao) Then
        Call Err.Raise(vbObjectError + 1000, , "SGSL0001.cls00210.AlterarEventoALSLayout - N�o foi poss�vel abrir transa��o")
    End If

    ' Incluindo Evento ''''''''''''''''''''''''''''''''''''''''''''

    sSql = "SET NOCOUNT ON "
    sSql = sSql & "EXEC interface_dados_db..evento_als_layout_spu "
    sSql = sSql & "'" & sCodTransacao & "'"
    sSql = sSql & "," & iEventoBB
    sSql = sSql & ",'" & sNomeBanco & "'"
    sSql = sSql & ",'" & sDescricao & "'"
    sSql = sSql & ",'" & snometabela & "'"
    sSql = sSql & ",'" & sSpTratamentoEvento & "'"
    sSql = sSql & ",'" & sUsuario & "'"
    sSql = sSql & ", " & iLayoutId
    sSql = sSql & ", " & iGrupoEventoId
    sSql = sSql & ",'" & sCondicao & "'"

    Call Conexao_ExecutarSQL(sSiglaSistema, _
                             iAmbienteId, _
                             sSiglaRecurso, _
                             sDescricaoRecurso, _
                             sSql, _
                             lConexao, _
                             False)

    ' Incluindo Colunas do Evento ''''''''''''''''''''''''''''''''''

    Call AlterarEventoALSLayoutColuna(sSiglaSistema, _
                                      sSiglaRecurso, _
                                      sDescricaoRecurso, _
                                      iAmbienteId, _
                                      iEventoBB, _
                                      colColunas, _
                                      sUsuario, _
                                      lConexao)

    ' Fechando Transacao '''''''''''''''''''''''''''''''''''''''''''''

    If Not ConfirmarTransacao(lConexao) Then
        Call Err.Raise(vbObjectError + 1000, , "SGSL0001.cls00210.AlterarEventoALSLayout - N�o foi poss�vel confirmar transa��o")
    End If

    ' Fechando Conexao '''''''''''''''''''''''''''''''''''''''''''''''

    If Not FecharConexao(lConexao) Then
        Call Err.Raise(vbObjectError + 1000, , "SGSL0001.cls00210.AlterarEventoALSLayout - N�o foi poss�vel fechar conex�o")
    End If

    Exit Function

Trata_Erro:

    Call Err.Raise(Err.Number, , "SGSL0001.cls00210.AlterarEventoALSLayout - " & Err.Description)

End Function

Public Sub AlterarEventoALSLayoutColuna(ByVal sSiglaSistema As String, _
                                        ByVal sSiglaRecurso As String, _
                                        ByVal sDescricaoRecurso As String, _
                                        ByVal iAmbienteId As Integer, _
                                        ByVal iEventoBB As Integer, _
                                        ByRef colColunas As Collection, _
                                        ByVal sUsuario As String, _
                                        ByVal lConexao As Long)

    Dim oColuna As Object
    Dim iSequencial As Integer
    Dim sColuna As String
    Dim iTamanho As Integer
    Dim sOperacao As String
    Dim sIdentificador As String
    Dim iGrupoIdentificador As Integer

    Dim sSql   As String

    On Error GoTo Trata_Erro

    For Each oColuna In colColunas

        sOperacao = oColuna.Operacao
        iSequencial = oColuna.Sequencial
        sColuna = oColuna.Nome
        iTamanho = oColuna.Tamanho
        sIdentificador = oColuna.Identificador
        iGrupoIdentificador = oColuna.GrupoIdentificador

        Select Case Trim(UCase(sOperacao))

            Case "I"
                ' Inclus�o '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                sSql = "SET NOCOUNT ON "
                sSql = sSql & "EXEC interface_dados_db..evento_als_layout_coluna_spi "
                sSql = sSql & "  " & iEventoBB
                sSql = sSql & ", " & iSequencial
                sSql = sSql & ",'" & sColuna & "'"
                sSql = sSql & ", " & iTamanho
                sSql = sSql & ",'" & sIdentificador & "'"
                sSql = sSql & ", " & iGrupoIdentificador
                sSql = sSql & ",'" & sUsuario & "'"

                Call Conexao_ExecutarSQL(sSiglaSistema, _
                                         iAmbienteId, _
                                         sSiglaRecurso, _
                                         sDescricaoRecurso, _
                                         sSql, _
                                         lConexao, _
                                         False)

            Case "A"
                ' Altera��o ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                sSql = "SET NOCOUNT ON "
                sSql = sSql & "EXEC interface_dados_db..evento_als_layout_coluna_spu "
                sSql = sSql & "  " & iEventoBB
                sSql = sSql & ", " & iSequencial
                sSql = sSql & ",'" & sColuna & "'"
                sSql = sSql & ", " & iTamanho
                sSql = sSql & ",'" & sIdentificador & "'"
                sSql = sSql & ", " & iGrupoIdentificador
                sSql = sSql & ",'" & sUsuario & "'"

                Call Conexao_ExecutarSQL(sSiglaSistema, _
                                         iAmbienteId, _
                                         sSiglaRecurso, _
                                         sDescricaoRecurso, _
                                         sSql, _
                                         lConexao, _
                                         False)

            Case "E"
                ' Exclus�o '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                sSql = "SET NOCOUNT ON "
                sSql = sSql & "EXEC interface_dados_db..evento_als_layout_coluna_spd "
                sSql = sSql & "  " & iEventoBB
                sSql = sSql & ", " & iSequencial

                Call Conexao_ExecutarSQL(sSiglaSistema, _
                                         iAmbienteId, _
                                         sSiglaRecurso, _
                                         sDescricaoRecurso, _
                                         sSql, _
                                         lConexao, _
                                         False)

            Case "N"
                ' Sem  altera��o ''''''''''''''''''''''''''''''''''''''''''''''''''''

        End Select

    Next

    Set oColuna = Nothing

    Exit Sub

Trata_Erro:

    Call Err.Raise(Err.Number, , "SGSL0001.cls00210.AlterarEventoALSLayoutColuna - " & Err.Description)

End Sub

Public Function ConsultarDadosALSLayoutColuna(ByVal sSiglaSistema As String, _
                                              ByVal sSiglaRecurso As String, _
                                              ByVal sDescricaoRecurso As String, _
                                              ByVal iAmbienteId As Integer, _
                                              ByVal iEventoBB As Integer, _
                                              Optional ByVal lSeqColuna As Long = 0) As Object

    Dim sSql   As String

    On Error GoTo Trata_Erro

    sSql = ""
    sSql = sSql & "SELECT * " & vbNewLine
    sSql = sSql & "  FROM interface_dados_db..dado_als_layout_coluna_produto_tb " & vbNewLine
    sSql = sSql & " WHERE evento_bb = " & iEventoBB & vbNewLine

    If lSeqColuna > 0 Then
        sSql = sSql & "   AND seq_coluna = " & lSeqColuna & vbNewLine
    End If

    Set ConsultarDadosALSLayoutColuna = ExecutarSQL(sSiglaSistema, _
                                                    iAmbienteId, _
                                                    sSiglaRecurso, _
                                                    sDescricaoRecurso, _
                                                    sSql, _
                                                    True)

    Exit Function

Trata_Erro:

    Call Err.Raise(Err.Number, , "SGSL0001.cls00210.ConsultarDadosALSLayoutColuna - " & Err.Description)

End Function

Public Function ConsultarDadosALSLayout(ByVal sSiglaSistema As String, _
                                        ByVal sSiglaRecurso As String, _
                                        ByVal sDescricaoRecurso As String, _
                                        ByVal iAmbienteId As Integer) As Object

    Dim sSql   As String

    On Error GoTo Trata_Erro
    ''Alterado por Michel P�rez (Confitec) -- 13.12.2010
    sSql = "SET NOCOUNT ON "
    sSql = "EXEC interface_dados_db..SEGS9311_SPS"

    Set ConsultarDadosALSLayout = ExecutarSQL(sSiglaSistema, _
                                              iAmbienteId, _
                                              sSiglaRecurso, _
                                              sDescricaoRecurso, _
                                              sSql, _
                                              True)
    Exit Function

Trata_Erro:

    Call Err.Raise(Err.Number, , "SGSL0001.cls00210.ConsultarDadoALSLayoutColunaProduto - " & Err.Description)

End Function

'vmuniz em 04/03/04
Public Function VerificarContratoSeguro(ByVal sSiglaSistema As String, _
                                        ByVal sSiglaRecurso As String, _
                                        ByVal sDescricaoRecurso As String, _
                                        ByVal iAmbienteId As Integer, _
                                        Optional ByVal sDtEnvio As String = "") As Object

    Dim sSql   As String

    On Error GoTo Trata_Erro

    sSql = ""
    sSql = sSql & "SELECT COUNT(*)" & vbNewLine
    sSql = sSql & "  FROM als_operacao_db..CTR_SGRO" & vbNewLine
    sSql = sSql & " WHERE DT_ENVO_CTR_SGRA IS NULL" & vbNewLine

    Set VerificarContratoSeguro = ExecutarSQL(sSiglaSistema, _
                                              iAmbienteId, _
                                              sSiglaRecurso, _
                                              sDescricaoRecurso, _
                                              sSql, _
                                              True)

    Exit Function

Trata_Erro:

    Call Err.Raise(Err.Number, , "SGSL0001.cls00210.VerificarContratoSeguro - " & Err.Description)

End Function

Public Function ConsultarLayout(ByVal sSiglaSistema As String, _
                                ByVal sSiglaRecurso As String, _
                                ByVal sDescricaoRecurso As String, _
                                ByVal iAmbienteId As Integer) As Object

    Dim sSql   As String

    On Error GoTo Trata_Erro

    sSql = ""
    sSql = sSql & "SELECT layout_id, " & vbNewLine
    sSql = sSql & "       descricao " & vbNewLine
    sSql = sSql & "  FROM interface_db..layout_tb " & vbNewLine
    sSql = sSql & " ORDER BY layout_id "

    Set ConsultarLayout = ExecutarSQL(sSiglaSistema, _
                                      iAmbienteId, _
                                      sSiglaRecurso, _
                                      sDescricaoRecurso, _
                                      sSql, _
                                      True)

    Exit Function

Trata_Erro:

    Call Err.Raise(Err.Number, , "SGSL0001.cls00210.ConsultarLayout - " & Err.Description)

End Function

Public Function ConsultarExistenciaTabelaProcedures(ByVal sSiglaSistema As String, _
                                                    ByVal sSiglaRecurso As String, _
                                                    ByVal sDescricaoRecurso As String, _
                                                    ByVal iAmbienteId As Integer, _
                                                    ByVal sNome As String, _
                                                    ByVal sTipo As String) As Boolean

    Dim sSql   As String
    Dim rs     As Recordset

    On Error GoTo Trata_Erro

    sSql = "" & vbNewLine
    sSql = sSql & "SELECT count(1) " & vbNewLine

    If sTipo = "u" Then    'Tabela
        sSql = sSql & "  FROM als_produto_db..sysobjects " & vbNewLine
    Else
        sSql = sSql & "  FROM interface_dados_db..sysobjects " & vbNewLine
    End If

    sSql = sSql & " WHERE name = '" & sNome & "'"
    sSql = sSql & "   AND type = '" & sTipo & "'"

    Set rs = ExecutarSQL(sSiglaSistema, _
                         iAmbienteId, _
                         sSiglaRecurso, _
                         sDescricaoRecurso, _
                         sSql, _
                         True)

    If rs(0) > 0 Then
        ConsultarExistenciaTabelaProcedures = True
    Else
        ConsultarExistenciaTabelaProcedures = False
    End If

    Exit Function

Trata_Erro:

    Call Err.Raise(Err.Number, , "SGSL0001.cls00210.ConsultarExistenciaTabelaProcedures - " & Err.Description)

End Function

Public Function ConsultarGrupoEvento(ByVal sSiglaSistema As String, _
                                     ByVal sSiglaRecurso As String, _
                                     ByVal sDescricaoRecurso As String, _
                                     ByVal iAmbienteId As Integer, _
                                     Optional ByVal iGrupoEventoAlsId As Integer, _
                                     Optional ByVal sDescricao As String) As Object

    Dim sSql   As String

    On Error GoTo Trata_Erro

    sSql = ""
    sSql = sSql & "SELECT grupo_evento_als_id, " & vbNewLine
    sSql = sSql & "       descricao " & vbNewLine
    sSql = sSql & "  FROM interface_dados_db..grupo_evento_als_tb " & vbNewLine
    If Not iGrupoEventoAlsId = 0 Then
        sSql = sSql & " WHERE grupo_evento_als_id = " & iGrupoEventoAlsId
    ElseIf Not sDescricao = "" Then
        sSql = sSql & " WHERE descricao LIKE '" & sDescricao & "%'"
    End If
    sSql = sSql & " ORDER BY grupo_evento_als_id "

    Set ConsultarGrupoEvento = ExecutarSQL(sSiglaSistema, _
                                           iAmbienteId, _
                                           sSiglaRecurso, _
                                           sDescricaoRecurso, _
                                           sSql, _
                                           True)

    Exit Function

Trata_Erro:

    Call Err.Raise(Err.Number, , "SGSL0001.cls00210.ConsultarGrupoEvento - " & Err.Description)

End Function

Public Function ConsultarProduto(ByVal sSiglaSistema As String, _
                                 ByVal sSiglaRecurso As String, _
                                 ByVal sDescricaoRecurso As String, _
                                 ByVal iAmbienteId As Integer) As Object

    Dim sSql   As String

    On Error GoTo Trata_Erro

    sSql = ""
    sSql = sSql & "SELECT produto_id,"
    sSql = sSql & "       nome"
    sSql = sSql & "  FROM als_produto_db..produto_tb"

    Set ConsultarProduto = ExecutarSQL(sSiglaSistema, _
                                       iAmbienteId, _
                                       sSiglaRecurso, _
                                       sDescricaoRecurso, _
                                       sSql, _
                                       True)

    Exit Function

Trata_Erro:

    Call Err.Raise(Err.Number, , "SGSL0001.cls00210.ConsultarProduto - " & Err.Description)

End Function

Public Function ConsultarEntradaMQ(ByVal sSiglaSistema As String, _
                                   ByVal sSiglaRecurso As String, _
                                   ByVal sDescricaoRecurso As String, _
                                   ByVal iAmbienteId As Integer) As Object

    On Error GoTo Trata_Erro

    Dim sSql   As String

    'ALTERADO POR JUNIOR EM 22/04/2004 PARA TRATAR OS EVENTOS DE COTA��O - IN�CIO
    sSql = ""
    '' PRODUTO
    sSql = sSql & "SELECT evento_als_layout_tb.evento_bb, " & vbNewLine
    sSql = sSql & "       evento_als_layout_tb.descricao_evento, " & vbNewLine
    sSql = sSql & "       entrada_mq_produto_tb.dt_recebimento, " & vbNewLine
    sSql = sSql & "       entrada_MQ_id = entrada_mq_produto_tb.entrada_mq_produto_id, " & vbNewLine
    sSql = sSql & "       grupo = 'PRODUTO' " & vbNewLine
    sSql = sSql & "  FROM interface_dados_db..evento_als_layout_tb evento_als_layout_tb " & vbNewLine
    sSql = sSql & "  JOIN interface_dados_db..entrada_mq_produto_tb entrada_mq_produto_tb " & vbNewLine
    sSql = sSql & "    ON entrada_mq_produto_tb.evento_bb = evento_als_layout_tb.evento_bb " & vbNewLine
    sSql = sSql & " WHERE entrada_mq_produto_tb.dt_processamento IS NULL " & vbNewLine  ''NOVA CONSULTORIA RAFAEL BISPO
    ''sSql = sSql & " WHERE entrada_mq_produto_tb.dt_processamento IS NULL AND entrada_mq_produto_tb.evento_bb<>8199  " & vbNewLine
    sSql = sSql & " UNION "
    '' COTA��O
    sSql = sSql & "SELECT evento_als_layout_tb.evento_BB, " & vbNewLine
    sSql = sSql & "       evento_als_layout_tb.descricao_evento, " & vbNewLine
    sSql = sSql & "       entrada_MQ_cotacao_tb.dt_recebimento, " & vbNewLine
    sSql = sSql & "       entrada_MQ_id = entrada_MQ_cotacao_tb.entrada_MQ_cotacao_id, " & vbNewLine
    sSql = sSql & "       grupo = 'COTA��O' " & vbNewLine
    sSql = sSql & "  FROM interface_dados_db..evento_als_layout_tb evento_als_layout_tb " & vbNewLine
    sSql = sSql & "  JOIN interface_dados_db..entrada_MQ_cotacao_tb entrada_MQ_cotacao_tb " & vbNewLine
    sSql = sSql & "    ON entrada_MQ_cotacao_tb.evento_bb = evento_als_layout_tb.evento_bb " & vbNewLine
    sSql = sSql & " WHERE entrada_MQ_cotacao_tb.dt_processamento IS NULL " & vbNewLine
    sSql = sSql & "   AND entrada_mq_cotacao_tb.evento_BB        <> 8199 " & vbNewLine
    sSql = sSql & "   AND entrada_mq_cotacao_tb.evento_BB NOT IN (4400, 4401, 4402, 4403, 4404, 4405, 4406, 4408, 4409, 4410, 4412, 4413, 4414, " & vbNewLine
    sSql = sSql & "   4415, 4416, 4450, 4500, 4501, 4502, 4503, 4504, 4505, 4506, 4520, 4599)" & vbNewLine
    sSql = sSql & " UNION "
    '' CONTRATO
    sSql = sSql & "SELECT evento_als_layout_tb.evento_BB, " & vbNewLine
    sSql = sSql & "       evento_als_layout_tb.descricao_evento, " & vbNewLine
    sSql = sSql & "       entrada_MQ_contrato_tb.dt_recebimento, " & vbNewLine
    sSql = sSql & "       entrada_MQ_id = entrada_MQ_contrato_tb.entrada_MQ_contrato_id, " & vbNewLine
    sSql = sSql & "       grupo = 'CONTRATO' " & vbNewLine
    sSql = sSql & "  FROM interface_dados_db..evento_als_layout_tb evento_als_layout_tb " & vbNewLine
    sSql = sSql & "  JOIN interface_dados_db..entrada_MQ_contrato_tb entrada_MQ_contrato_tb " & vbNewLine
    sSql = sSql & "    ON entrada_MQ_contrato_tb.evento_bb = evento_als_layout_tb.evento_bb " & vbNewLine
    sSql = sSql & " WHERE entrada_MQ_contrato_tb.dt_processamento IS NULL " & vbNewLine

    'IM00041490 - Inicio - Corrigindo os dados dos eventos processados pelo SMAP0021 e n�o pelo SMQP007 - 21/06/2017
    'sSql = sSql & "   AND entrada_mq_contrato_tb.evento_BB        <> 8199 " & vbNewLine
    'sSql = sSql & "   AND entrada_mq_contrato_tb.evento_BB        <> 4700 " & vbNewLine    ''Altera��o feita por Michel - Confitec
    'sSql = sSql & "   AND entrada_mq_contrato_tb.evento_BB        <> 4710 " & vbNewLine    ''Altera��o feita por Michel - Confitec
    '''MARCIOMS - DEMANDA 17919477 - PRESTAMISTA PJ - INCLUSAO DE EVENTOS DE ENDOSSO 337 E 338 - INICIO
    '''sSql = sSql & "   AND entrada_mq_contrato_tb.evento_BB NOT IN (4800, 4801, 4800, 4801, 4802, 4803, 4804, 4805, 4806, 4807, 4810, 4811, 4812, 4813, 4814, 4815, 4817, 4822, 4702) " & vbNewLine ''Altera��o feita por Michel - Confitec
    'sSql = sSql & "   AND entrada_mq_contrato_tb.evento_BB NOT IN (4800, 4801, 4800, 4801, 4802, 4803, 4804, 4805, 4806, 4807, 4810, " & vbNewLine
    'sSql = sSql & "                                                4811, 4812, 4813, 4814, 4815, 4817, 4822, 4702, 4823, 4825) " & vbNewLine
    '''MARCIOMS - DEMANDA 17919477 - PRESTAMISTA PJ - INCLUSAO DE EVENTOS DE ENDOSSO 337 E 338 - FIM

    sSql = sSql & "    AND entrada_mq_contrato_tb.evento_BB NOT IN (4700, 4701, 4702, 4710, 4720, 4800, 4801, 4802, 4803, 4804, 4805, 4806, 4807, 4810, " & vbNewLine
    sSql = sSql & "                                                 4811, 4812, 4813, 4814, 4815, 4816, 4817, 4819, 4820, 4821, 4822, 4823, 4825, 8199) " & vbNewLine

    'IM00041490 - Fim - Corrigindo os dados dos eventos processados pelo SMAP0021 e n�o pelo SMQP007  - 21/06/2017



    sSql = sSql & " UNION "
    '' MCI
    sSql = sSql & "SELECT evento_als_layout_tb.evento_BB, " & vbNewLine
    sSql = sSql & "       evento_als_layout_tb.descricao_evento, " & vbNewLine
    sSql = sSql & "       entrada_MQ_mci_tb.dt_recebimento, " & vbNewLine
    sSql = sSql & "       entrada_MQ_id = entrada_MQ_mci_tb.entrada_MQ_mci_id, " & vbNewLine
    sSql = sSql & "       grupo = 'MCI' " & vbNewLine
    sSql = sSql & "  FROM interface_dados_db..evento_als_layout_tb evento_als_layout_tb " & vbNewLine
    sSql = sSql & "  JOIN interface_dados_db..entrada_MQ_mci_tb entrada_MQ_mci_tb " & vbNewLine
    sSql = sSql & "    ON entrada_MQ_mci_tb.evento_bb = evento_als_layout_tb.evento_bb " & vbNewLine
    sSql = sSql & " WHERE entrada_MQ_mci_tb.dt_processamento IS NULL " & vbNewLine
    sSql = sSql & "   AND entrada_mq_mci_tb.evento_BB        <> 8199 " & vbNewLine
    sSql = sSql & "   AND entrada_mq_mci_tb.evento_BB        <> 8110" & vbNewLine    ''Altera��o feita por Michel - Confitec
    sSql = sSql & "   AND entrada_mq_mci_tb.evento_BB        <> 8120 " & vbNewLine    ''Altera��o feita por Michel - Confitec
    sSql = sSql & "   AND entrada_mq_mci_tb.evento_BB        <> 8130 " & vbNewLine    ''Altera��o feita por Michel - Confitec
    sSql = sSql & "   AND entrada_mq_mci_tb.evento_BB        <> 8140 " & vbNewLine    ''Altera��o feita por Michel - Confitec
    sSql = sSql & " ORDER BY entrada_MQ_id" & vbNewLine
    'ALTERADO POR JUNIOR EM 22/04/2004 PARA TRATAR OS EVENTOS DE COTA��O - FIM

    Set ConsultarEntradaMQ = ExecutarSQL(sSiglaSistema, _
                                         iAmbienteId, _
                                         sSiglaRecurso, _
                                         sDescricaoRecurso, _
                                         sSql, _
                                         True)

    Exit Function

Trata_Erro:

    Call Err.Raise(Err.Number, , "SGSL0001.cls00210.ConsultarEntradaMQ - " & Err.Description)

End Function
