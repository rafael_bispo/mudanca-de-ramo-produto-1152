Attribute VB_Name = "Local"

' constantes
Public Const gsOBJETOAMBIENTE = "SABL0010.cls00009"
Public Const gsSIGLAEMPRESA = "SoftRoad"
Public Const gsSIGLASISTEMA = "SEGBR"
Public Const gsCODPRINCIPAL = "SEGP0007"
Public Const glINTERVALOATUALIZACAO = 60000    ' milissegundos
Public Const gsCHAVESISTEMA = "@Li�c@"
Public glAmbiente_id As Long
Public SQS     As String
Public Const HKEY_CLASSES_ROOT = &H80000000
Public Const HKEY_CURRENT_USER = &H80000001
Public Const HKEY_LOCAL_MACHINE = &H80000002
Public Const HKEY_USERS = &H80000003
Public gsParamAplicacao As String
Public goProducao As Object


Sub EnviarEmail(ByVal sSiglaSistema As String, _
                ByVal sSiglaRecurso As String, _
                ByVal sDescricaoRecurso As String, _
                ByVal iAmbienteId As Integer, _
                ByVal sDestinatario As String, _
                ByVal sAssunto As String, _
                ByVal sMensagem As String)

    'Rotina de envio de e-mail

    sSql = ""
    sSql = sSql & "EXEC envia_email_sp '" & sDestinatario & "'" & vbNewLine
    sSql = sSql & ",'" & sAssunto & "'" & vbNewLine
    sSql = sSql & ", '" & sMensagem & "'"

    Call ExecutarSQL(sSiglaSistema, _
                     iAmbienteId, _
                     sSiglaRecurso, _
                     sDescricaoRecurso, _
                     sSql)

End Sub


Public Sub adSQL(ByRef sSql As String, ByVal sCadeia As String)
    sSql = sSql & sCadeia & vbNewLine
End Sub

Function MudaAspaSimples(ByVal valor As String) As String
    Do Until InStr(valor, "'") = 0
        valor = Mid$(valor, 1, InStr(valor, "'") - 1) + "�" + Mid$(valor, InStr(valor, "'") + 1)
    Loop
    MudaAspaSimples = UCase(valor)
End Function



Sub MensagemBatch(ByVal PMensagem As String, Optional PBotao As VbMsgBoxStyle = vbOKOnly, _
                  Optional PTitulo As String = "", Optional ByVal PGravaMensagem As Boolean = True, _
                  Optional PFormulario As Form)
    If Len(Trim(PTitulo)) = 0 Then PTitulo = gsSIGLASISTEMA

    If PGravaMensagem Then
        Call GravaMensagem(PMensagem, "PROC")
    End If

    If goProducao Is Nothing Then
        If Obtem_agenda_diaria_id(Command) = 0 And PMensagem <> "" Then
            MsgBox PMensagem, PBotao, PTitulo
        End If
    ElseIf Not goProducao.Automatico Then
        If PMensagem <> "" Then
            MsgBox PMensagem, PBotao, PTitulo
        End If
    End If
End Sub



Function GravaMensagem(ByVal PMensagem As String, ByVal PTipo As String, _
                       Optional ByVal PRotina As String = "", _
                       Optional ByVal PArquivo As String = "")
    On Local Error GoTo TrataErro
    Dim Erro   As Object
    Set Erro = CreateObject("Erro.Log")
    Erro.Usuario = IIf(Trim(cUserName) = "", gsIp & "~" & gsHost, cUserName)
    Erro.NmProjeto = gsSIGLASISTEMA
    Erro.NmAplicacao = App.EXEName
    Erro.NmForm = PArquivo
    'Erro.NmArquivo = PArquivo
    Erro.NmRotina = PRotina

    If goProducao Is Nothing Then
        Call Erro.Grava_Log_Erro(0, PMensagem, PTipo, Obtem_agenda_diaria_id(gsParamAplicacao))
    Else
        Call Erro.Grava_Log_Erro(0, PMensagem, PTipo, goProducao.Agenda_Diaria_id)
    End If

TrataErro:
    ' se houver erro, sai da rotina
End Function


Function Obtem_agenda_diaria_id(OParametro As String) As Long
    'Raphael Luiz Gagliardi 22/05/2001
    'Obt�m agenda_diaria_id a partir do par�metro passado
    'Se retornar 0 --> Manual
    Dim lposicao1 As Long
    Dim lposicao2 As Long

    lposicao1 = InStr(1, OParametro, "||")
    lposicao2 = InStr(lposicao1 + 1, OParametro, "||")
    If lposicao1 > 0 And lposicao2 > 0 Then
        Obtem_agenda_diaria_id = Mid(OParametro, lposicao1 + 2, lposicao2 - lposicao1 - 2)
    Else
        Obtem_agenda_diaria_id = 0
    End If

End Function


