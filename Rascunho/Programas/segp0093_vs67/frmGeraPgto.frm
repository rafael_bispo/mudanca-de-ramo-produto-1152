VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.2#0"; "MSCOMCTL.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form frmGeraPgto 
   Caption         =   "Gera��o de Pagamentos"
   ClientHeight    =   4395
   ClientLeft      =   2385
   ClientTop       =   2445
   ClientWidth     =   6765
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   4395
   ScaleWidth      =   6765
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame Frame2 
      Caption         =   " Origem "
      Height          =   2175
      Left            =   120
      TabIndex        =   11
      Top             =   120
      Width           =   3015
      Begin VB.ListBox lstOrigem 
         Height          =   1815
         ItemData        =   "frmGeraPgto.frx":0000
         Left            =   120
         List            =   "frmGeraPgto.frx":0019
         TabIndex        =   0
         Tag             =   "1"
         Top             =   240
         Width           =   2775
      End
   End
   Begin MSComctlLib.ProgressBar ProgressBar1 
      Height          =   255
      Left            =   120
      TabIndex        =   10
      Top             =   3720
      Width           =   4095
      _ExtentX        =   7223
      _ExtentY        =   450
      _Version        =   393216
      Appearance      =   1
   End
   Begin VB.Frame Frame1 
      Height          =   1215
      Left            =   120
      TabIndex        =   7
      Top             =   2280
      Width           =   6615
      Begin VB.TextBox txtIni 
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   3960
         Locked          =   -1  'True
         TabIndex        =   4
         Top             =   240
         Width           =   2415
      End
      Begin VB.TextBox txtfim 
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   3960
         Locked          =   -1  'True
         TabIndex        =   5
         Top             =   720
         Width           =   2415
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "In�cio.........................................................................."
         Height          =   195
         Left            =   120
         TabIndex        =   9
         Top             =   240
         Width           =   3735
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "Fim.............................................................................."
         Height          =   195
         Left            =   120
         TabIndex        =   8
         Top             =   720
         Width           =   3750
      End
   End
   Begin VB.CommandButton cmdOk 
      Caption         =   "&Ok"
      Enabled         =   0   'False
      Height          =   375
      Left            =   4320
      TabIndex        =   2
      Top             =   3675
      Width           =   1095
   End
   Begin VB.CommandButton cmdSair 
      Caption         =   "&Sair"
      Height          =   375
      Left            =   5640
      TabIndex        =   3
      Top             =   3675
      Width           =   1095
   End
   Begin MSComctlLib.StatusBar StatusBar1 
      Align           =   2  'Align Bottom
      Height          =   255
      Left            =   0
      TabIndex        =   6
      Top             =   4140
      Width           =   6765
      _ExtentX        =   11933
      _ExtentY        =   450
      Style           =   1
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   15240
            MinWidth        =   15240
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSFlexGridLib.MSFlexGrid gridResumo 
      Height          =   2055
      Left            =   3240
      TabIndex        =   1
      Top             =   240
      Width           =   3495
      _ExtentX        =   6165
      _ExtentY        =   3625
      _Version        =   393216
      FixedCols       =   0
      FormatString    =   "Origem                     |Total de Pagamentos   "
   End
End
Attribute VB_Name = "frmGeraPgto"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'Option Explicit
Const ORIG_CORR = 0

Const ORIG_AGE = 1

Const ORIG_CLI = 2

Const ORIG_SIN = 3

Const ORIG_IOF = 4

'Demanda 654224 - emaior - 27/05/2009 - Projeto Visanet
'Gera��o de pagamento para cart�o de cr�dito.
Const ORIG_CARTAO = 5

'JOAO.MACHADO - IPREM - 26-09-2013
Const ORIG_ASSISTENCIA = 6


Public Endosso_Contestacao As Boolean


'-------------------------------------
Dim rdocn1 As New rdoConnection
Dim rdocn2 As New rdoConnection    'R.FOUREAUX - 16/08/2019
Dim rdocn3 As New rdoConnection  'R.FOUREAUX - 28/10/2019

Dim rc_geral As rdoResultset

Dim Data_Pagamento As Date

Dim Dias_Antecedencia As Integer

Dim Retido_Banco_ant As String

Dim Corretor_ant As String

Dim Sucursal_ant As String

Dim tp_movimentacao As String

Dim Tp_movimentacao_ant As String

Dim Estipulante_ant As String

Dim Regulador_ant As String

Dim Prestador_ant As String

Dim Cliente_ant As String

Dim Item_financeiro_ant As String

Dim Seguradora_ant As String

Dim Resseguradora_ant As String

Dim Total_mov As Currency

Dim Total_Multa As Currency

Dim Total_Juros As Currency

Dim acerto_id As String

'pmelo - Acerto Sinistro CDC - 11/11/09
Dim sinistro_id As Integer

Dim Dt_Sistema As Date

Dim Dt_Pagamento_Comissao As Date

Dim Produto_id As String

Dim ramo_id As String

Dim dia_semana

Dim Flag_Pgto_ok As Boolean

Dim ConfiguracaoBrasil As Boolean

Dim dt_proxima_quarta As Date

Dim dt_segunda_anterior As Date

Dim dt_sexta_anterior As Date

Dim dt_recebimento As String

Dim multa As Currency

Dim juros As Currency

Dim Conta_lidas As Long

Dim Conta_Processadas As Long

'Vari�vel para verificar se o GTR est� implantado
Dim GTR_Implantado As Boolean

Private Sub Atualiza_Movimento(movimentacao_id As Variant)
    Dim rc As rdoResultset

    Dim SQL As String

    On Error GoTo Erro

    If Trim(agenciador_ant) = "" Then
        agenciador_ant = "0"
    End If

    If Trim(Dt_Agendamento_ant) = "" Then
        Dt_Agendamento_ant = "19000101"
    End If

    SQL = "Exec ps_mov_acerto_spu "
    SQL = SQL & movimentacao_id & ","
    SQL = SQL & acerto_id & ","
    SQL = SQL & "'" & tp_movimentacao & "',"
    SQL = SQL & "'" & cUserName & "','"
    SQL = SQL & "a',"
    SQL = SQL & "0,"
    SQL = SQL & "'    ',"
    SQL = SQL & MudaVirgulaParaPonto(multa) & ","
    SQL = SQL & MudaVirgulaParaPonto(juros)
    Set rc = rdocn.OpenResultset(SQL)
    rc.Close
    Exit Sub
Erro:
    TrataErroGeral "Atualiza Movimento"
    TerminaSEGBR
End Sub

Private Sub Atualiza_Pagamento(ByVal Origem, _
                               tp_movimentacao, _
                               Corretor, _
                               Sucursal, _
                               Cliente, _
                               cod_item_financeiro, _
                               Cod_Seguradora, _
                               Prestador_id, _
                               Regulador_id As String, _
                               ByVal Data_Pagamento As Date, _
                               ByVal Total_mov As Currency, _
                               ByVal Total_aux As Currency, _
                               ByVal proposta As String, _
                               ByVal situacao As String, _
                               ByVal agenciador_id As String, _
                               Optional ByVal operadora_cartao_id As Integer = 0)
    Dim rc As rdoResultset

    Dim SQL As String

    On Error GoTo Erro

    If Total_aux = 0 Then
        Exit Sub
    End If

    SQL = "Exec ps_acerto_spu "
    SQL = SQL & acerto_id & ","
    SQL = SQL & "'" & Origem & "',"
    SQL = SQL & "'" & Format(Data_Pagamento, "yyyymmdd") & "',"
    SQL = SQL & "'" & Retido_Banco_ant & "',"
    SQL = SQL & "'" & situacao & "',"
    SQL = SQL & MudaVirgulaParaPonto(Total_mov) & ","
    SQL = SQL & "'" & tp_movimentacao & "',"
    SQL = SQL & Corretor & ","
    SQL = SQL & "'" & Sucursal & "',"
    SQL = SQL & Cliente & ","
    SQL = SQL & "'" & cod_item_financeiro & "',"
    SQL = SQL & Cod_Seguradora & ","
    SQL = SQL & "'" & cUserName & "',"
    SQL = SQL & Prestador_id & ","
    SQL = SQL & Regulador_id & ","
    SQL = SQL & MudaVirgulaParaPonto(Total_aux) & ","
    SQL = SQL & proposta & ","
    SQL = SQL & "0,"
    SQL = SQL & MudaVirgulaParaPonto(Total_Multa) & ","
    SQL = SQL & MudaVirgulaParaPonto(Total_Juros) & ","
    'Demanda 654224 - emaior - 27/05/2009 - Projeto Visanet
    'Inclus�o de mais um parametro para tratamento de cart�o de cr�dito.
    SQL = SQL & operadora_cartao_id
    Set rc = rdocn.OpenResultset(SQL)
    rc.Close
    Exit Sub
Erro:
    TrataErroGeral "Atualiza Pagamento"
    TerminaSEGBR
End Sub

Sub Processa_Pgto_Agenciador()
    Dim rc As rdoResultset
    Dim SQL As String
    Dim val_mov As Double
    Dim cont_pgto_agenciador As Long
    Dim acerto_id As Long

    On Error GoTo Erro
    txtIni.Text = Now
    ' Obtem dados do Pagamento
    SQL = Monta_Query_Agenciador
    Set rc_geral = rdocn1.OpenResultset(SQL, rdOpenStatic)

    If Not rc_geral.EOF Then
        Conta_lidas = 0
    Else
        rdocn.RollbackTrans
        txtfim = Now
        MensagemBatch "Nenhum pagamento de agenciador foi selecionado"
        Call goProducao.AdicionaLog(1, "Agenciador", 6)
        Call goProducao.AdicionaLog(2, Conta_Processadas, 6)

        'grava log arquivo CTM
        If CTM = True Then
            Call GravaLogCTM("OK - ", "Agenciador", "", "")
            Call GravaLogCTM("OK - ", Conta_Processadas, "", "")
        End If

        rc_geral.Close
        Flag_Pgto_ok = True
        Exit Sub
    End If

    flagprimeiravez = True
    Retido_Banco_ant = ""
    agenciador_ant = ""
    tp_movimentacao = "7"
    ProgressBar1.Value = 0
    Total_mov = 0
    Cliente_ant = 0
    Proposta_ant = 0

    While Not rc_geral.EOF
        Conta_lidas = Conta_lidas + 1
        ProgressBar1.Value = Conta_lidas

        If rc_geral!retido_banco <> Retido_Banco_ant Or rc_geral!agenciador_id <> agenciador_ant Then

            If Not flagprimeiravez Then
                If Total_mov >= 0 Then
                    If Retido_Banco_ant = "s" Then
                        situacao_aux = "a"
                    Else
                        situacao_aux = "p"
                    End If

                    Atualiza_Pagamento "AG", tp_movimentacao, 0, " ", 0, " ", 0, 0, 0, Data_Pagamento, Total_mov, Total_mov, 0, situacao_aux, agenciador_ant
                    Conta_Processadas = Conta_Processadas + 1
                    rdocn.CommitTrans
                Else
                    rdocn.RollbackTrans
                End If

            Else
                flagprimeiravez = False
            End If

            Retido_Banco_ant = rc_geral!retido_banco
            agenciador_ant = rc_geral!agenciador_id
            Dt_Agendamento_ant = Format(rc_geral!dt_agendamento, "yyyymmdd")
            rdocn.BeginTrans
            Insere_Pagamento "AG"
            Total_mov = 0
        End If

        If UCase(rc_geral!tp_operacao) = "C" Then
            Total_mov = Total_mov + Val(rc_geral!val_movimentacao)
        Else
            Total_mov = Total_mov - Val(rc_geral!val_movimentacao)
        End If

        Call Atualiza_Movimento(rc_geral!movimentacao_id)
        rc_geral.MoveNext
    Wend

    rc_geral.Close

    If Total_mov >= 0 Then

        'Verificar - Jailson
        If Retido_Banco_ant = "s" Then
            situacao_aux = "a"
        Else
            situacao_aux = "p"
        End If

        Atualiza_Pagamento "AG", "7", 0, " ", 0, " ", 0, 0, 0, Data_Pagamento, Total_mov, Total_mov, 0, situacao_aux, agenciador_ant
        Conta_Processadas = Conta_Processadas + 1
        rdocn.CommitTrans
    Else
        rdocn.RollbackTrans
    End If


    gridResumo.AddItem "Agenciador" & vbTab & Str(Conta_Processadas)
    Call goProducao.AdicionaLog(1, "Agenciador", 6)
    Call goProducao.AdicionaLog(2, Conta_Processadas, 6)

    'grava log arquivo CTM
    If CTM = True Then
        Call GravaLogCTM("OK - ", "Agenciador", "", "")
        Call GravaLogCTM("OK - ", Conta_Processadas, "", "")
    End If

    Flag_Pgto_ok = True
    txtfim = Now
    Exit Sub
Erro:
    TrataErroGeral "Processa Pagamento Agenciador"
    TerminaSEGBR
End Sub

Private Function Monta_Query_Agenciador() As String
    Dim SQL As String

    'Barney - 21/01/2004
    On Error GoTo Error
    SQL = ""
    SQL = SQL & "SELECT   m.movimentacao_id             " & vbNewLine
    SQL = SQL & "       , m.dt_movimentacao             " & vbNewLine
    SQL = SQL & "       , a.tp_operacao                 " & vbNewLine
    SQL = SQL & "       , a.val_movimentacao            " & vbNewLine
    SQL = SQL & "       , a.agenciador_id               " & vbNewLine
    SQL = SQL & "       , a.dt_agendamento              " & vbNewLine
    SQL = SQL & "       , a.retido_banco                " & vbNewLine
    SQL = SQL & "  FROM   ps_mov_agenciador_tb a        " & vbNewLine
    SQL = SQL & "       , ps_movimentacao_tb m          " & vbNewLine
    SQL = SQL & " WHERE   a.acerto_id IS NULL           " & vbNewLine
    SQL = SQL & "   AND   a.flag_cancelamento IS NULL   " & vbNewLine
    SQL = SQL & "   AND   a.dt_agendamento <= '" & Format(Data_Sistema, "yyyymmdd") & "'" & vbNewLine
    SQL = SQL & "   AND   m.movimentacao_id = a.movimentacao_id                         " & vbNewLine
    SQL = SQL & "   AND  (m.flag_aprovacao  IS NULL OR m.flag_aprovacao = 's')          " & vbNewLine
    SQL = SQL & "   AND   ISNULL(m.situacao,'') <> 'T'                                             " & vbNewLine

    SQL = SQL & " ORDER   BY a.agenciador_id, m.dt_movimentacao                         " & vbNewLine

    Monta_Query_Agenciador = SQL
    'Barney - 21/01/2004
    Exit Function
Error:
    Call TrataErroGeral("Monta_query_Agenciador", Me.name)
    Call TerminaSEGBR
End Function

Private Sub cmdOk_Click()

'Barney - 21/01/2004
    On Error GoTo Error
    'Call InicializaParametrosExecucaoBatch(Me)
    MousePointer = vbHourglass

    Select Case lstOrigem.ListIndex
    Case ORIG_CORR
        gridResumo.Rows = 1
        StatusBar1.SimpleText = "Processando Pagamentos de Corretor + Estipulante."
        txtIni = Now
        Flag_Pgto_ok = False
        Call Processa_Pgto_Corretor

        txtfim = Now

        If Flag_Pgto_ok = True Then
            Call MensagemBatch("Processamento finalizado com sucesso !", , , False)
        Else
            Call MensagemBatch("Erro no processamento")
        End If

        StatusBar1.SimpleText = "Fim do Processamento."
    Case ORIG_AGE
        gridResumo.Rows = 1
        StatusBar1.SimpleText = "Processando Pagamentos de Agenciador."
        Flag_Pgto_ok = False
        Call Processa_Pgto_Agenciador

        If Flag_Pgto_ok Then
            Call MensagemBatch("Processamento finalizado com sucesso !", , , False)
        Else
            Call MensagemBatch("Erro no processamento")
        End If

        StatusBar1.SimpleText = "Fim do Processamento."
    Case ORIG_CLI
        StatusBar1.SimpleText = "Processando Pagamentos de Cliente."
        txtIni = Now
        Call Obtem_valores_progress_bar
        Flag_Pgto_ok = False
        Call Processa_Pgto_Cliente
        txtfim = Now

        If Flag_Pgto_ok = True Then
            Call MensagemBatch("Processamento finalizado com sucesso !", , , False)
        Else
            Call MensagemBatch("Erro no processamento")
        End If

        StatusBar1.SimpleText = "Fim do Processamento."
    Case ORIG_SIN
        StatusBar1.SimpleText = "Processando Pagamentos de Sinistro."
        txtIni = Now
        Call Obtem_valores_progress_bar
        Flag_Pgto_ok = False

        'pmelo - Acerto Sinistro CDC - 05/11/09
        If Processa_Pgto_Sinistro_CDC Then
            Call Processa_Pgto_Sinistro(True)
        Else
            Call Processa_Pgto_Sinistro(False)
        End If

        If Flag_Pgto_ok = True Then
            Call MensagemBatch("Processamento finalizado com sucesso !", , , False)
        Else
            Call MensagemBatch("Erro no processamento")
        End If

        StatusBar1.SimpleText = "Fim do Processamento de Sinistro."
    Case ORIG_IOF
        Data_inicial = ""
        Data_Final = ""
        StatusBar1.SimpleText = "Processando Pagamentos de IOF."
        txtIni = Now
        Call Obtem_valores_progress_bar
        Flag_Pgto_ok = False
        Call Processa_Pgto_IOF_nao_Recolhido
        txtfim = Now

        If Flag_Pgto_ok = True Then
            Call MensagemBatch("Processamento finalizado com sucesso !", , , False)
        Else
            Call MensagemBatch("Erro no processamento")
        End If

        StatusBar1.SimpleText = "Fim do Processamento."
        'Demanda 654224 - emaior - 27/05/2009 - Projeto Visanet
        'Gera��o de pagamento para cart�o de cr�dito.
    Case ORIG_CARTAO
        gridResumo.Rows = 1
        StatusBar1.SimpleText = "Processando Pagamentos de Cart�o de Cr�dito."
        txtIni = Now
        Flag_Pgto_ok = False
        Call Obtem_valores_progress_bar
        Call Processa_Pgto_Cartao
        txtfim = Now

        If Flag_Pgto_ok = True Then
            Call MensagemBatch("Processamento finalizado com sucesso !", , , False)
        Else
            Call MensagemBatch("Erro no processamento")
        End If

        StatusBar1.SimpleText = "Fim do Processamento."

    Case ORIG_ASSISTENCIA    'JOAO.MACHADO - IPREM - 26-09-2013
        StatusBar1.SimpleText = "Processando Pagamentos de Assist�ncias."
        txtIni = Now
        Call Obtem_valores_progress_bar
        Flag_Pgto_ok = False
        Call Processa_Pgto_Assistencia
        txtfim = Now

        If Flag_Pgto_ok = True Then
            Call MensagemBatch("Processamento finalizado com sucesso !", , , False)
        Else
            Call MensagemBatch("Erro no processamento")
        End If

        StatusBar1.SimpleText = "Fim do Processamento."

    Case Else
        Call MensagemBatch("Fun��o n�o implementada!")
    End Select

    MousePointer = vbDefault
    lstOrigem.ListIndex = -1
    cmdOk.Enabled = False
    'Barney - 21/01/2004
    Exit Sub
Error:
    Call TrataErroGeral("On_Click", Me.name)
    Call TerminaSEGBR
End Sub

Private Sub cmdSair_Click()
    Call Unload(Me)
End Sub

Private Sub Form_Load()
    Dim SQL As String, rc As rdoResultset

    Dim nameu As String

    On Error GoTo Erro

    '* Comentar - teste
    'UserName = "Teste"
    glAmbiente_id = 3

    'Verifica se foi executado pelo Control-M
    CTM = Verifica_Origem_ControlM(Command)

    'Confitec - Alexandre Debouch - EdsContestacao
    Endosso_Contestacao = Verifica_Endosso_Contestacao(Command)
    'Confitec - Alexandre Debouch - EdsContestacao

    Conexao

    Call Seguranca("SEGP0093", "Gera pagamentos")
    Me.Caption = "SEG00093 - Gera��o de Pagamentos - " & Ambiente

    'Comentar
'    GTR_Implantado = Obtem_GTR_Implantado     'teste - descomentar para liberar
    GTR_Implantado = True                    'teste - comentar para liberar

    sDecimal = LeArquivoIni("WIN.INI", "intl", "sDecimal")

    If sDecimal = "." Then
        ConfiguracaoBrasil = False
    Else
        ConfiguracaoBrasil = True
    End If

    'Data_Sistema = Now  'Teste

    Conexao_Auxiliar
    Le_Dias_Antecedencia
    Dt_Sistema = Data_Sistema
    Data_Pagamento = ProximoDiaUtil(Data_Sistema, Dias_Antecedencia)
    gridResumo.Rows = 1

    ' joconceicao - 03/ago/01 - agenda diaria
'    If Obtem_agenda_diaria_id(Command) > 0 Or CTM = True Then
'        Me.Show
'        Call cmdOk_Click
'        If CTM = False Then
'            Call goProducao.finaliza
'        End If
'        Call TerminaSEGBR
'    End If

    Exit Sub
Erro:
    TrataErroGeral "Carga do Formul�rio"
    TerminaSEGBR
End Sub

Sub Conexao_Auxiliar()

    On Error GoTo Erro

    With rdocn1
        .Connect = rdocn.Connect
        .CursorDriver = rdUseServer
        .QueryTimeout = 240
        .EstablishConnection rdDriverNoPrompt
    End With

    'R.FOUREAUX - 16/08/2019
    With rdocn2
        .Connect = rdocn.Connect
        .CursorDriver = rdUseServer
        .QueryTimeout = 30000
        .EstablishConnection rdDriverNoPrompt
    End With

 'R.FOUREAUX - 28/10/2019
    With rdocn3
        .Connect = rdocn.Connect
        .CursorDriver = rdUseServer
        .QueryTimeout = 30000
        .EstablishConnection rdDriverNoPrompt
    End With

    Exit Sub
Erro:
    mensagem_erro 6, "Conex�o com BRCAPDB indispon�vel."
    TerminaSEGBR
End Sub

Private Sub Obtem_valores_progress_bar()

'Barney - 21/01/2003
    On Error GoTo Error
    Dim rc As rdoResultset, SQL As String

    Dim Contador As Long

    'Conta o n�mero de registros da query ( estudar melhoria )
    Select Case lstOrigem.ListIndex
    Case ORIG_SIN
        SQL = Monta_query_Sinistro
        Set rc = rdocn1.OpenResultset(SQL)
        Contador = 0

        While Not rc.EOF
            rc.MoveNext
            Contador = Contador + 1
        Wend

        rc.Close

        If Contador > 0 Then
            ProgressBar1.Max = Contador
        End If

    Case ORIG_CORR
        SQL = Monta_query_Corretor
        Set rc = rdocn1.OpenResultset(SQL)
        Contador = 0

        While Not rc.EOF
            rc.MoveNext
            Contador = Contador + 1
        Wend

        rc.Close

        If Contador > 0 Then
            ProgressBar1.Max = Contador
        End If

    Case ORIG_CLI
        SQL = ""
        SQL = SQL & "set nocount on " & vbNewLine
        SQL = SQL & "declare @nRetornoAcerto int " & vbNewLine
        SQL = SQL & "exec seguros_db.dbo.SEGS14353_SPS 's', @nRetornoAcerto OutPut " & IIf(Endosso_Contestacao, ", 'S'", ", 'N'") & vbNewLine
        SQL = SQL & "select @nRetornoAcerto as QtdeRegistros " & vbNewLine

        Set rc = rdocn1.OpenResultset(SQL)
        If Not rc.EOF Then
            If rc!QtdeRegistros > 0 Then
                ProgressBar1.Max = rc!QtdeRegistros
            End If
        End If

    Case ORIG_IOF
        SQL = Monta_query_IOF
        Set rc = rdocn1.OpenResultset(SQL)
        Contador = 0

        While Not rc.EOF
            rc.MoveNext
            Contador = Contador + 1
        Wend

        rc.Close

        If Contador > 0 Then
            ProgressBar1.Max = Contador
        End If

    Case ORIG_CARTAO
        SQL = Monta_Query_Cartao
        Set rc = rdocn1.OpenResultset(SQL)
        Contador = 0

        While Not rc.EOF
            rc.MoveNext
            Contador = Contador + 1
        Wend

        rc.Close

        If Contador > 0 Then
            ProgressBar1.Max = Contador
        End If

    Case ORIG_ASSISTENCIA    'JOAO.MACHADO - IPREM - 26-09-2013
        SQL = Monta_query_Assistencia
        Set rc = rdocn1.OpenResultset(SQL)
        Contador = 0

        While Not rc.EOF
            rc.MoveNext
            Contador = Contador + 1
        Wend

        rc.Close

        If Contador > 0 Then
            ProgressBar1.Max = Contador
        End If

    End Select

    'Barney - 21/01/2004
    Exit Sub
Error:
    Call TrataErroGeral("Obtem_valores_progress_bar", Me.name)
    Call TerminaSEGBR
End Sub

'pmelo - Acerto Sinistro CDC - 05/11/09
'Inclusao do parametro na funcao para diferenciar o sinistro CDC
Private Function Monta_query_Sinistro(Optional ByVal TpSinistro As String) As String

'Barney - 21/01/2004
    On Error GoTo Error
    Dim SQL As String

    SQL = ""
    SQL = SQL & "SELECT a.beneficiario_id,                                                       " & vbNewLine
    SQL = SQL & "       a.sinistro_id,                                                           " & vbNewLine
    SQL = SQL & "       pgto.dt_acerto_contas_sinistro,                                          " & vbNewLine
    SQL = SQL & "       pgto.forma_pgto_id,                                                      " & vbNewLine
    SQL = SQL & "       a.ramo_id,                                                               " & vbNewLine
    SQL = SQL & "       a.seguradora_cod_susep,                                                  " & vbNewLine
    SQL = SQL & "       a.apolice_id,                                                            " & vbNewLine
    SQL = SQL & "       a.sucursal_seguradora_id,                                                " & vbNewLine
    SQL = SQL & "       a.item_val_estimativa,                                                   " & vbNewLine
    SQL = SQL & "       a.val_movimentacao,                                                      " & vbNewLine
    'FLOW 1110779 - Confitec - JFILHO/MFERREIRA - Corrigindo falha no processo de pagamento de sinistros, que n�o considerava o campo seq_pgto na hora de efetuar o acerto
    SQL = SQL & "       a.seq_pgto,                                                              " & vbNewLine
    SQL = SQL & "       b.situacao,                                                              " & vbNewLine
    SQL = SQL & "       b.proposta_id,                                                           " & vbNewLine
    SQL = SQL & "       b.cliente_id,                                                            " & vbNewLine
    SQL = SQL & "       pgto.usuario_avaliador,                                                  " & vbNewLine
    SQL = SQL & "       pgto.dt_avaliacao,                                                       " & vbNewLine
    SQL = SQL & "       a.movimentacao_id,                                                       " & vbNewLine
    SQL = SQL & "       c.tp_emissao,                                                            " & vbNewLine
    SQL = SQL & "       pgto.num_recibo,                                                         " & vbNewLine
    SQL = SQL & "       chave_autorizacao_sinistro,                                              " & vbNewLine
    SQL = SQL & "       chave_autorizacao_sinistro_dv                                            " & vbNewLine
    SQL = SQL & "  FROM ps_mov_sinistro_tb a                                                     " & vbNewLine
    SQL = SQL & " INNER JOIN pgto_sinistro_tb pgto                                               " & vbNewLine
    SQL = SQL & "    ON pgto.sinistro_id = a.sinistro_id                                         " & vbNewLine
    SQL = SQL & "   AND pgto.ramo_id = a.ramo_id                                                 " & vbNewLine
    SQL = SQL & "   AND pgto.apolice_id = a.apolice_id                                           " & vbNewLine
    SQL = SQL & "   AND pgto.sucursal_seguradora_id = a.sucursal_seguradora_id                   " & vbNewLine
    SQL = SQL & "   AND pgto.seguradora_cod_susep = a.seguradora_cod_susep                       " & vbNewLine
    SQL = SQL & "   AND pgto.beneficiario_id = a.beneficiario_id                                 " & vbNewLine
    SQL = SQL & "   AND pgto.seq_pgto = a.seq_pgto                                               " & vbNewLine
    SQL = SQL & "   AND pgto.seq_estimativa = a.seq_estimativa                                   " & vbNewLine
    SQL = SQL & "   AND pgto.num_parcela = a.num_parcela                                         " & vbNewLine
    SQL = SQL & "   AND pgto.item_val_estimativa = a.item_val_estimativa                         " & vbNewLine
    SQL = SQL & "   AND pgto.situacao_op = 'a',                                                  " & vbNewLine
    SQL = SQL & "       sinistro_tb b,                                                           " & vbNewLine
    SQL = SQL & "       apolice_tb c,                                                            " & vbNewLine
    'pmelo - Acerto Sinistro CDC - 05/11/09
    SQL = SQL & "       proposta_tb prop                                                         " & vbNewLine
    'fim pmelo - Acerto Sinistro CDC - 05/11/09
    SQL = SQL & " WHERE a.acerto_id IS NULL                                                      " & vbNewLine
    SQL = SQL & "   AND a.tp_operacao = 'd'                                                      " & vbNewLine
    SQL = SQL & "   AND a.ramo_id = b.ramo_id                                                    " & vbNewLine
    SQL = SQL & "   AND a.seguradora_cod_susep = b.seguradora_cod_susep                          " & vbNewLine
    SQL = SQL & "   AND a.sucursal_seguradora_id = b.sucursal_seguradora_id                      " & vbNewLine
    SQL = SQL & "   AND a.sinistro_id = b.sinistro_id                                            " & vbNewLine
    SQL = SQL & "   AND a.apolice_id = b.apolice_id                                              " & vbNewLine
    '   SQL = SQL & "   AND a.item_val_estimativa = 1                                               " & vbNewLine
    SQL = SQL & "   AND c.ramo_id = a.ramo_id                                                    " & vbNewLine
    SQL = SQL & "   AND c.seguradora_cod_susep = a.seguradora_cod_susep                          " & vbNewLine
    SQL = SQL & "   AND c.sucursal_seguradora_id = a.sucursal_seguradora_id                      " & vbNewLine
    SQL = SQL & "   AND c.apolice_id = a.apolice_id                                              " & vbNewLine
    SQL = SQL & "   AND pgto.resseguro_analisado = 's'                                           " & vbNewLine
    'pmelo - Acerto Sinistro CDC - 05/11/09
    SQL = SQL & "   AND prop.proposta_id = b.proposta_id                                         " & vbNewLine

    If TpSinistro = "CDC" Then
        SQL = SQL & "   AND prop.proposta_id  = 1140                                             " & vbNewLine
    Else
        SQL = SQL & "   AND prop.proposta_id <> 1140                                             " & vbNewLine
    End If

    'fim pmelo - Acerto Sinistro CDC - 05/11/09
    SQL = SQL & "   AND NOT EXISTS (SELECT sinistro_id                                           " & vbNewLine
    SQL = SQL & "                     FROM sinistro_benef_congenere_tb e                         " & vbNewLine
    SQL = SQL & "                    WHERE e.ramo_id = pgto.ramo_id                              " & vbNewLine
    SQL = SQL & "                      AND e.seguradora_cod_susep = pgto.seguradora_cod_susep    " & vbNewLine
    SQL = SQL & "                      AND e.sucursal_seguradora_id = pgto.sucursal_seguradora_id" & vbNewLine
    SQL = SQL & "                      AND e.sinistro_id = pgto.sinistro_id                      " & vbNewLine
    SQL = SQL & "                      AND e.apolice_id = pgto.apolice_id                        " & vbNewLine
    SQL = SQL & "                      AND e.beneficiario_id = pgto.beneficiario_id )            " & vbNewLine
    SQL = SQL & "                                                                                " & vbNewLine
    '* Alterado por Alexandre Debouch
    SQL = SQL & "   AND b.situacao <> 7                                                          " & vbNewLine

    'Confitec Sistemas - Diogo Ferreira 31/03/2011 - Flow 9457455: Inclus�o de critica. Se for ramo 75 n�o verifica se a proposta foi transferida
    'SQL = SQL & "   AND prop.situacao <> 'T'                                                     " & vbNewLine
    SQL = SQL & "   AND (a.sinistro_id LIKE '75%'                                                " & vbNewLine
    SQL = SQL & "        OR (a.sinistro_id NOT LIKE '75%' AND prop.situacao <> 'T')              " & vbNewLine
    SQL = SQL & "       )                                                                        " & vbNewLine


    SQL = SQL & "                                                                                " & vbNewLine
    SQL = SQL & " ORDER BY a.sinistro_id, a.beneficiario_id,                                     " & vbNewLine
    SQL = SQL & "          pgto.dt_acerto_contas_sinistro, pgto.forma_pgto_id "
    Monta_query_Sinistro = SQL
    'Barney - 21/01/2004
    Exit Function
Error:
    Call TrataErroGeral("Monta_query_Sinistro", Me.name)
    Call TerminaSEGBR
End Function

Private Function Monta_query_Cliente() As String
    On Error GoTo Error
    Dim SQL As String

    SQL = ""
    SQL = SQL & " SET NOCOUNT ON " & vbNewLine
    SQL = SQL & "     SELECT DISTINCT           " & vbNewLine
    SQL = SQL & "            a.movimentacao_id, " & vbNewLine
    SQL = SQL & "            a.dt_movimentacao, " & vbNewLine
    SQL = SQL & "            b.tp_operacao,     " & vbNewLine
    SQL = SQL & "            b.val_movimentacao," & vbNewLine
    SQL = SQL & "            b.retido_banco,    " & vbNewLine
    SQL = SQL & "            b.cliente_id,      " & vbNewLine
    SQL = SQL & "            c.proposta_id,     " & vbNewLine
    SQL = SQL & "            c.endosso_id,      " & vbNewLine
    SQL = SQL & "            d.produto_id,      " & vbNewLine
    SQL = SQL & "            d.ramo_id,         " & vbNewLine
    SQL = SQL & "            isnull(a.flag_aprovacao,' ') flag_aprovacao  , " & vbNewLine
    SQL = SQL & "            'N' as restituicao_unica, " & vbNewLine
    SQL = SQL & "            'N' as restituicao_documento_gru_mapa, " & vbNewLine
    SQL = SQL & "            ISNULL(item_produto_tb.aglutina_restituicao, 'N') aglutina_restituicao, " & vbNewLine    'Instancia 19408029 - Aglutina��o de pgtos restitui��o - RMarins 02/03/2017
    SQL = SQL & "           endosso_tb.tp_endosso_id " & vbNewLine
    SQL = SQL & "   INTO #Q_CLIENTE " & vbNewLine
    SQL = SQL & "      FROM ps_movimentacao_tb a" & vbNewLine
    SQL = SQL & " INNER JOIN ps_mov_cliente_tb b (nolock) " & vbNewLine
    SQL = SQL & "         ON a.movimentacao_id = b.movimentacao_id " & vbNewLine
    SQL = SQL & " INNER JOIN ps_mov_endosso_financeiro_tb c (nolock) " & vbNewLine
    SQL = SQL & "         ON a.movimentacao_id = c.movimentacao_id " & vbNewLine
    SQL = SQL & " INNER JOIN proposta_tb d (nolock) " & vbNewLine
    SQL = SQL & "         ON c.proposta_id = d.proposta_id " & vbNewLine
    SQL = SQL & "  LEFT JOIN seguro_generico_tb e (nolock) " & vbNewLine
    SQL = SQL & "         ON e.proposta_id = c.proposta_id        " & vbNewLine
    SQL = SQL & "  LEFT JOIN subramo_tb f (nolock) " & vbNewLine
    SQL = SQL & "         ON f.ramo_id    = e.ramo_id" & vbNewLine
    SQL = SQL & "        AND f.subramo_id = e.subramo_id" & vbNewLine
    SQL = SQL & " INNER JOIN endosso_financeiro_tb g (nolock) " & vbNewLine
    SQL = SQL & "         ON g.proposta_id = c.proposta_id" & vbNewLine
    SQL = SQL & "        AND g.endosso_id = c.endosso_id" & vbNewLine
    SQL = SQL & "  LEFT JOIN endosso_resumo_tb er (nolock) " & vbNewLine
    SQL = SQL & "         ON er.endosso_resumo_id = g.endosso_resumo_id " & vbNewLine
    SQL = SQL & "       JOIN endosso_tb endosso_tb (nolock) " & vbNewLine
    SQL = SQL & "         ON endosso_tb.proposta_id = g.proposta_id " & vbNewLine
    SQL = SQL & "        AND endosso_tb.endosso_id = g.endosso_id " & vbNewLine
    SQL = SQL & "  LEFT JOIN ps_mov_cliente_preremessa_tb ps_mov_cliente_preremessa_tb (nolock) " & vbNewLine
    SQL = SQL & "         ON ps_mov_cliente_preremessa_tb.movimentacao_id = b.movimentacao_id " & vbNewLine
    SQL = SQL & "  LEFT JOIN item_produto_tb item_produto_tb (nolock) " & vbNewLine
    SQL = SQL & "         ON item_produto_tb.produto_id = d.produto_id " & vbNewLine
    SQL = SQL & "        AND item_produto_tb.ramo_id = d.ramo_id " & vbNewLine
    SQL = SQL & "      WHERE b.acerto_id IS NULL " & vbNewLine
    SQL = SQL & "        AND er.endosso_resumo_id is null " & vbNewLine
    SQL = SQL & "        AND b.flag_cancelamento is null " & vbNewLine
    SQL = SQL & "        AND (a.flag_aprovacao is null or a.flag_aprovacao = 's') " & vbNewLine
    SQL = SQL & "        AND er.endosso_resumo_id IS NULL "
    SQL = SQL & "        AND ( " & vbNewLine
    SQL = SQL & "             ISNULL(f.restituicao_unica,'N') = 'N'" & vbNewLine
    SQL = SQL & "             OR" & vbNewLine
    SQL = SQL & "                 (" & vbNewLine
    SQL = SQL & "                 ISNULL(f.restituicao_unica,'N') = 'S'" & vbNewLine
    SQL = SQL & "                 AND g.val_financeiro > 0" & vbNewLine
    SQL = SQL & "                )" & vbNewLine
    SQL = SQL & "           )" & vbNewLine
    SQL = SQL & "        AND ISNULL(a.situacao,'') <> 'T' " & vbNewLine
    SQL = SQL & "        AND ((ps_mov_cliente_preremessa_tb.Lote_liberado            = 'S' OR d.produto_id in (12,121,135,136,716))" & vbNewLine
    SQL = SQL & "         OR endosso_tb.tp_endosso_id = 200) " & vbNewLine
    SQL = SQL & "        AND b.aceite_bb IS NULL " & vbNewLine
    SQL = SQL & " UNION " & vbNewLine
    SQL = SQL & "        SELECT null as movimentacao_id,   " & vbNewLine
    SQL = SQL & "               null as dt_movimentacao,   " & vbNewLine
    SQL = SQL & "               b.tp_operacao,  " & vbNewLine
    SQL = SQL & "               sum(b.val_movimentacao) as val_movimentacao,  " & vbNewLine
    SQL = SQL & "               b.retido_banco,  " & vbNewLine
    SQL = SQL & "               b.cliente_id,  " & vbNewLine
    SQL = SQL & "               c.proposta_id,  " & vbNewLine
    SQL = SQL & "               er.endosso_id,  " & vbNewLine
    SQL = SQL & "               d.produto_id,  " & vbNewLine
    SQL = SQL & "               d.ramo_id,         " & vbNewLine
    SQL = SQL & "               isnull(a.flag_aprovacao,' ') flag_aprovacao ," & vbNewLine
    SQL = SQL & "               f.restituicao_unica," & vbNewLine
    SQL = SQL & "               'N' as restituicao_documento_gru_mapa, " & vbNewLine
    SQL = SQL & "               ISNULL(item_produto_tb.aglutina_restituicao, 'N') aglutina_restituicao, " & vbNewLine
    SQL = SQL & "           endosso_tb.tp_endosso_id " & vbNewLine
    SQL = SQL & "      FROM ps_movimentacao_tb a" & vbNewLine
    SQL = SQL & "    INNER JOIN ps_mov_cliente_tb b (nolock) " & vbNewLine
    SQL = SQL & "            ON a.movimentacao_id = b.movimentacao_id  " & vbNewLine
    SQL = SQL & "    INNER JOIN ps_mov_endosso_financeiro_tb c (nolock) " & vbNewLine
    SQL = SQL & "            ON a.movimentacao_id = c.movimentacao_id  " & vbNewLine
    SQL = SQL & "    INNER JOIN endosso_financeiro_tb e (nolock) " & vbNewLine
    SQL = SQL & "            ON e.proposta_id = c.proposta_id " & vbNewLine
    SQL = SQL & "           AND e.endosso_id  = c.endosso_id " & vbNewLine
    SQL = SQL & "    INNER JOIN endosso_resumo_tb er (nolock) " & vbNewLine
    SQL = SQL & "            ON er.endosso_resumo_id = e.endosso_resumo_id " & vbNewLine
    SQL = SQL & "    INNER JOIN proposta_tb d (nolock) " & vbNewLine
    SQL = SQL & "            ON c.proposta_id = d.proposta_id  " & vbNewLine
    SQL = SQL & "    INNER JOIN seguro_generico_tb g (nolock) " & vbNewLine
    SQL = SQL & "            ON g.proposta_id = c.proposta_id        " & vbNewLine
    SQL = SQL & "    INNER JOIN subramo_tb f (nolock) " & vbNewLine
    SQL = SQL & "            ON f.ramo_id    = g.ramo_id" & vbNewLine
    SQL = SQL & "           AND f.subramo_id = g.subramo_id" & vbNewLine
    SQL = SQL & "           AND dt_fim_vigencia_sbr IS NULL"
    SQL = SQL & "          JOIN endosso_tb endosso_tb (nolock) " & vbNewLine
    SQL = SQL & "            ON endosso_tb.proposta_id = e.proposta_id " & vbNewLine
    SQL = SQL & "           AND endosso_tb.endosso_id = e.endosso_id " & vbNewLine
    SQL = SQL & "     LEFT JOIN ps_mov_cliente_preremessa_tb ps_mov_cliente_preremessa_tb (nolock) " & vbNewLine
    SQL = SQL & "            ON ps_mov_cliente_preremessa_tb.movimentacao_id = b.movimentacao_id " & vbNewLine
    SQL = SQL & "     LEFT JOIN item_produto_tb item_produto_tb (nolock) " & vbNewLine
    SQL = SQL & "            ON item_produto_tb.produto_id = d.produto_id " & vbNewLine
    SQL = SQL & "           AND item_produto_tb.ramo_id = d.ramo_id " & vbNewLine
    SQL = SQL & "         WHERE b.acerto_id IS NULL  " & vbNewLine
    SQL = SQL & "           AND b.flag_cancelamento is null  " & vbNewLine
    SQL = SQL & "           AND (a.flag_aprovacao is null or a.flag_aprovacao = 's') " & vbNewLine
    SQL = SQL & "           AND e.endosso_resumo_id IS NOT NULL " & vbNewLine
    SQL = SQL & "           AND f.restituicao_unica = 'S'" & vbNewLine
    SQL = SQL & "           AND ISNULL(a.situacao,'') <> 'T' " & vbNewLine
    SQL = SQL & "           AND b.aceite_bb IS NULL " & vbNewLine
    SQL = SQL & "           AND ((ps_mov_cliente_preremessa_tb.Lote_liberado            = 'S' OR d.produto_id in (12,121,135,136,716))" & vbNewLine
    SQL = SQL & "            OR endosso_tb.tp_endosso_id = 200) " & vbNewLine
    SQL = SQL & "      GROUP BY b.tp_operacao, " & vbNewLine
    SQL = SQL & "               b.retido_banco,   " & vbNewLine
    SQL = SQL & "               b.cliente_id,  " & vbNewLine
    SQL = SQL & "               c.proposta_id,  " & vbNewLine
    SQL = SQL & "               er.endosso_id,  " & vbNewLine
    SQL = SQL & "               d.produto_id,  " & vbNewLine
    SQL = SQL & "               d.ramo_id,     " & vbNewLine
    SQL = SQL & "               isnull(a.flag_aprovacao,' ') ," & vbNewLine
    SQL = SQL & "               f.restituicao_unica, " & vbNewLine
    SQL = SQL & "               ISNULL(item_produto_tb.aglutina_restituicao, 'N') ," & vbNewLine
    SQL = SQL & "           endosso_tb.tp_endosso_id " & vbNewLine
    SQL = SQL & " UNION " & vbNewLine
    SQL = SQL & "        SELECT a.movimentacao_id, " & vbNewLine
    SQL = SQL & "               a.dt_movimentacao, " & vbNewLine
    SQL = SQL & "               b.tp_operacao," & vbNewLine
    SQL = SQL & "               b.val_movimentacao," & vbNewLine
    SQL = SQL & "               b.retido_banco," & vbNewLine
    SQL = SQL & "               b.cliente_id," & vbNewLine
    SQL = SQL & "               proposta_id = 0," & vbNewLine
    SQL = SQL & "               endosso_id = 0," & vbNewLine
    SQL = SQL & "               f.produto_id," & vbNewLine
    SQL = SQL & "               f.ramo_id, " & vbNewLine
    SQL = SQL & "               isnull(a.flag_aprovacao,' ') flag_aprovacao, " & vbNewLine
    SQL = SQL & "               'N' as restituicao_unica, " & vbNewLine
    SQL = SQL & "               'S' as restituicao_documento_gru_mapa, " & vbNewLine
    SQL = SQL & "               ISNULL(item_produto_tb.aglutina_restituicao, 'N') aglutina_restituicao , " & vbNewLine
    SQL = SQL & "           cast(null as int) as tp_endosso_id " & vbNewLine
    SQL = SQL & "         FROM ps_movimentacao_tb a" & vbNewLine
    SQL = SQL & "    INNER JOIN ps_mov_cliente_tb b  (nolock) " & vbNewLine
    SQL = SQL & "            ON a.movimentacao_id = b.movimentacao_id  " & vbNewLine
    SQL = SQL & "    INNER JOIN ps_mov_documento_gru_tb c (nolock) " & vbNewLine
    SQL = SQL & "            ON a.movimentacao_id = c.movimentacao_id  " & vbNewLine
    SQL = SQL & "    INNER JOIN documento_gru_tb d (nolock) " & vbNewLine
    SQL = SQL & "            ON d.num_documento_gru = c.num_documento_gru " & vbNewLine
    SQL = SQL & "    INNER JOIN proposta_subvencao_tb e (nolock) " & vbNewLine
    SQL = SQL & "            ON e.num_documento_gru = d.num_documento_gru " & vbNewLine
    SQL = SQL & "    INNER JOIN proposta_tb f (nolock) " & vbNewLine
    SQL = SQL & "            ON f.proposta_id = e.proposta_id  " & vbNewLine
    SQL = SQL & "     LEFT JOIN ps_mov_cliente_preremessa_tb ps_mov_cliente_preremessa_tb (nolock) " & vbNewLine
    SQL = SQL & "            ON ps_mov_cliente_preremessa_tb.movimentacao_id = b.movimentacao_id " & vbNewLine
    SQL = SQL & "     LEFT JOIN item_produto_tb item_produto_tb (nolock) " & vbNewLine
    SQL = SQL & "            ON item_produto_tb.produto_id = f.produto_id " & vbNewLine
    SQL = SQL & "           AND item_produto_tb.ramo_id = f.ramo_id " & vbNewLine
    SQL = SQL & "         WHERE b.acerto_id IS NULL  " & vbNewLine
    SQL = SQL & "           AND b.flag_cancelamento is null  " & vbNewLine
    SQL = SQL & "           AND (a.flag_aprovacao is null or a.flag_aprovacao = 's') " & vbNewLine
    SQL = SQL & "           AND ISNULL(a.situacao,'') <> 'T' " & vbNewLine
    SQL = SQL & "           AND b.aceite_bb IS NULL " & vbNewLine
    SQL = SQL & "         AND (ps_mov_cliente_preremessa_tb.Lote_liberado            = 'S' OR f.produto_id in (12,121,135,136,716))" & vbNewLine    'FLAVIO.BEZERRA - 13/06/2013
    SQL = SQL & "      GROUP BY a.movimentacao_id, " & vbNewLine
    SQL = SQL & "               a.dt_movimentacao, " & vbNewLine
    SQL = SQL & "               b.tp_operacao," & vbNewLine
    SQL = SQL & "               b.val_movimentacao," & vbNewLine
    SQL = SQL & "               b.retido_banco," & vbNewLine
    SQL = SQL & "               b.cliente_id," & vbNewLine
    SQL = SQL & "               f.produto_id," & vbNewLine
    SQL = SQL & "               f.ramo_id, " & vbNewLine
    SQL = SQL & "               isnull(a.flag_aprovacao,' '), " & vbNewLine
    SQL = SQL & "               ISNULL(item_produto_tb.aglutina_restituicao, 'N') " & vbNewLine
    SQL = SQL & " ORDER BY produto_id, ramo_id, cliente_id, " & vbNewLine
    SQL = SQL & "          retido_banco, " & vbNewLine
    SQL = SQL & "          flag_aprovacao " & vbNewLine
    SQL = SQL & "        UPDATE #Q_CLIENTE                                 " & vbNewLine
    SQL = SQL & "           SET #Q_CLIENTE.aglutina_restituicao = 'N'      " & vbNewLine
    SQL = SQL & "          FROM #Q_CLIENTE #Q_CLIENTE                      " & vbNewLine
    SQL = SQL & "         WHERE #Q_CLIENTE.tp_endosso_id = 363             " & vbNewLine
    SQL = SQL & "         SELECT *                                         " & vbNewLine
    SQL = SQL & "           FROM #Q_CLIENTE                                " & vbNewLine
    SQL = SQL & "           DROP TABLE #Q_CLIENTE                                " & vbNewLine
    Monta_query_Cliente = SQL
    Exit Function
Error:
    Call TrataErroGeral("Monta_query_Cliente", Me.name)
    Call TerminaSEGBR
End Function

Private Function Monta_query_IOF() As String

'Barney - 21/01/2004
    On Error GoTo Error
    Dim SQL As String

    SQL = " SELECT a.movimentacao_id, "
    SQL = SQL & "  a.dt_movimentacao, "
    SQL = SQL & "  b.tp_operacao,"
    SQL = SQL & "  isnull(b.val_movimentacao,0) val_movimentacao, "
    ' Jarbas - confitec - RJ ---- Tratamento de valor nulo RNC - 21/02/2017 '''
    ''' SQL = SQL & "  b.retido_banco,"
    SQL = SQL & "  isnull(b.retido_banco,'N') retido_banco,"
    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    SQL = SQL & "  b.cod_item_financeiro "
    SQL = SQL & " FROM ps_movimentacao_tb a,"
    SQL = SQL & "      ps_mov_item_financeiro_tb b "
    SQL = SQL & " WHERE b.acerto_id IS NULL "
    SQL = SQL & "   AND a.movimentacao_id = b.movimentacao_id "
    SQL = SQL & "   AND b.cod_item_financeiro =  'IOF'  "
    SQL = SQL & "   AND b.tp_operacao =  'c' "
    SQL = SQL & "   AND b.flag_cancelamento is null "
    SQL = SQL & "   AND (a.flag_aprovacao is null or a.flag_aprovacao = 's')"
    SQL = SQL & "   AND ISNULL(a.situacao,'') <> 'T' "
    SQL = SQL & " ORDER BY cod_item_financeiro, "
    SQL = SQL & "          retido_banco "
    Monta_query_IOF = SQL
    'Barney - 21/01/2004
    Exit Function
Error:
    Call TrataErroGeral("Monta_query_IOF", Me.name)
    Call TerminaSEGBR
End Function

Private Sub Processa_Pgto_Sinistro(Optional ByVal AcertoCDC As Boolean)
    Dim rc As rdoResultset, rc1 As rdoResultset

    Dim SQL As String, Seq_Evento As String

    Dim situacao As String, Descricao As String, MOTIVO As String

    Dim vSeq_Estimativa As String

    On Error GoTo Erro
    ' Obtem dados do Pagamento
    SQL = Monta_query_Sinistro
    rdocn.BeginTrans
    Set rc = rdocn1.OpenResultset(SQL, rdOpenStatic)

    If Not rc.EOF Then

        'pmelo - Acerto Sinistro CDC - 12/11/09
        'Inicializa contador com 1 caso tenha gerado acerto para sinistro CDC anteriormente
        If AcertoCDC Then
            Conta_lidas = 1
        Else
            Conta_lidas = 0
        End If

    Else
        rdocn.RollbackTrans

        'pmelo - Acerto Sinistro CDC - 12/11/09
        'Contador com 1 caso tenha gerado acerto para sinistro CDC anteriormente
        If AcertoCDC Then
            Conta_lidas = 1
            gridResumo.AddItem "Sinistro" & vbTab & Str(Conta_lidas)
        Else
            Conta_lidas = 0
            MensagemBatch "Nenhum pagamento de sinistro foi selecionado"
        End If

        Call goProducao.AdicionaLog(1, "Sinistro", 1)
        Call goProducao.AdicionaLog(2, Conta_lidas, 1)

        'grava log arquivo CTM
        If CTM = True Then
            Call GravaLogCTM("OK - ", "Sinistro", "", "")
            Call GravaLogCTM("OK - ", Conta_lidas, "", "")
        End If

        rc.Close
        Flag_Pgto_ok = True
        txtfim = Now
        Exit Sub
    End If

    ProgressBar1.Value = 0

    While Not rc.EOF
        Descricao = "N� DA AUTORIZA��O: " & rc("chave_autorizacao_sinistro") & "-" & rc("chave_autorizacao_sinistro_dv")
        MOTIVO = ""
        situacao = ""
        SQL = "SELECT seq_evento, situacao, motivo_encerramento "
        SQL = SQL & " FROM sinistro_historico_tb "
        SQL = SQL & " WHERE sinistro_id = " & rc!sinistro_id
        SQL = SQL & "   AND ramo_id = " & rc!ramo_id
        SQL = SQL & "   AND seguradora_cod_susep = " & rc!seguradora_cod_susep
        SQL = SQL & "   AND sucursal_seguradora_id = " & rc!sucursal_seguradora_id
        SQL = SQL & "   AND apolice_id = " & rc!Apolice_id
        SQL = SQL & " ORDER BY seq_evento DESC "
        Set rc1 = rdocn.OpenResultset(SQL)

        If Not rc1.EOF Then
            If IsNull(rc1!MOTIVO_encerramento) = False Then
                MOTIVO = Trim(rc1!MOTIVO_encerramento)
            End If

            If IsNull(rc1!situacao) = False Then
                situacao = Trim(rc1!situacao)
            End If
        End If

        rc1.Close
        SQL = "SELECT MAX(seq_estimativa) FROM sinistro_estimativa_tb "
        SQL = SQL & " WHERE apolice_id = " & rc!Apolice_id
        SQL = SQL & "   AND sinistro_id = " & rc!sinistro_id
        SQL = SQL & "   AND sucursal_seguradora_id = " & rc!sucursal_seguradora_id
        SQL = SQL & "   AND seguradora_cod_susep = " & rc!seguradora_cod_susep
        SQL = SQL & "   AND ramo_id = " & rc!ramo_id
        Set rc1 = rdocn.OpenResultset(SQL)
        vSeq_Estimativa = rc1(0)
        rc1.Close
        SQL = " exec sinistro_historico_spi "
        SQL = SQL & rc!sinistro_id    ' sinistro_id
        SQL = SQL & ", " & rc!Apolice_id    ' apolice_id
        SQL = SQL & ", " & rc!sucursal_seguradora_id    ' sucursal_seguradora_id
        SQL = SQL & ", " & rc!seguradora_cod_susep    ' seguradora_cod_susep
        SQL = SQL & ", " & rc!ramo_id    ' ramo_id
        SQL = SQL & ", 10026"   ' TRANSFER�NCIA PARA O FINANCEIRO
        SQL = SQL & ", '" & situacao    ' situacao
        SQL = SQL & "', " & IIf(MOTIVO = "", "NULL", "'" & MOTIVO & "'")  ' motivo
        SQL = SQL & ", '" & Format(Dt_Sistema, "yyyymmdd")    ' dt_evento
        SQL = SQL & "', '" & cUserName & "'"    ' usuario
        SQL = SQL & ", " & vSeq_Estimativa    ' �ltimo seq_estimativa
        Set rc1 = rdocn.OpenResultset(SQL)
        Seq_Evento = rc1(0)
        rc1.Close
        vsql = "EXEC sinistro_item_historico_spi " & rc!sinistro_id & "," & rc!Apolice_id & "," & rc!sucursal_seguradora_id & "," & rc!seguradora_cod_susep & "," & rc!ramo_id & "," & Seq_Evento & ",'" & Descricao & "','" & cUserName & "'"
        rdocn.Execute (vsql)

        'Verifica se o GTR est� implantado e grava o registro
        If GTR_Implantado Then
            ' Grava registro GTR na tabela evento_SEGBR_sinistro_tb
            SQL = "EXEC evento_SEGBR_sinistro_spi "
            SQL = SQL & rc!sinistro_id                      ' @sinistro_id
            SQL = SQL & ", " & rc!Apolice_id                ' @apolice_id
            SQL = SQL & ", " & rc!seguradora_cod_susep      ' @seguradora_cod_susep
            SQL = SQL & ", " & rc!sucursal_seguradora_id    ' @sucursal_seguradora_id
            SQL = SQL & ", " & rc!ramo_id                   ' @ramo_id
            SQL = SQL & ", 10026"                           ' @evento_SEGBR_id
            SQL = SQL & ", '" & Format(Dt_Sistema, "yyyymmdd") & "'"  ' @dt_evento
            SQL = SQL & ", 1"                               ' @entidade_id (1 = Alian�a do Brasil)
            SQL = SQL & ", 'SEGBR'"                         ' @localizacao
            SQL = SQL & ", '" & cUserName & "'"             ' @usuario
            SQL = SQL & ", NULL "                           ' @produto_id
            SQL = SQL & ", NULL"                            ' @sinistro_bb
            SQL = SQL & ", " & rc!proposta_id               ' @proposta_id
            'Rafael Oshiro 27/01/2005 - Tratamento quando for nulo, pois campo num�rico nunca ser� ""
            'SQL = SQL & ", " & IIf(rc!Num_recibo = "", "NULL", rc!Num_recibo) ' @num_recibo
            SQL = SQL & ", " & IIf(IsNull(rc!Num_recibo), "NULL", rc!Num_recibo)    ' @num_recibo
            rdocn.Execute (SQL)
        End If

        SQL = " exec gera_pgto_sinistro_spi "
        SQL = SQL & rc!sinistro_id    ' sinistro_id
        SQL = SQL & ", " & rc!Apolice_id    ' apolice_id
        SQL = SQL & ", " & rc!sucursal_seguradora_id    ' sucursal_seguradora_id
        SQL = SQL & ", " & rc!seguradora_cod_susep    ' seguradora_cod_susep
        SQL = SQL & ", " & rc!ramo_id    ' ramo_id
        SQL = SQL & ", '" & Format(rc!dt_acerto_contas_sinistro, "yyyymmdd")    ' dt_acerto_contas_sinistro
        SQL = SQL & "', " & rc!forma_pgto_id    ' forma_pgto_id
        SQL = SQL & ", " & rc!beneficiario_id    ' Beneficiario_id
        SQL = SQL & ", " & rc!item_val_estimativa    ' item_val_estimativa
        SQL = SQL & ", 'SN'"    ' cod_origem
        SQL = SQL & ", '" & Format(Dt_Sistema, "yyyymmdd")    ' dt_geracao
        SQL = SQL & "', 's'"    ' retido_banco
        SQL = SQL & ", 'a'"    ' situacao: p = pendente a = aprovado
        SQL = SQL & ", " & rc!val_movimentacao    ' valor pago
        SQL = SQL & ", " & rc!movimentacao_id  ' movimentacao_id
        SQL = SQL & ",'" & rc!tp_emissao      ' tp emissao apolice (cedido, aceito, direto)
        SQL = SQL & "', '" & Format(rc!dt_avaliacao, "yyyymmdd")    ' dt_avaliacao
        SQL = SQL & "', '" & UCase(Trim(rc!usuario_avaliador))    ' usuario_avaliador
        SQL = SQL & "', '" & cUserName & "'"    ' usuario
        'Rafael Oshiro 27/01/2005 - Tratamento quando for nulo, pois campo num�rico nunca ser� ""
        'SQL = SQL & ", " & IIf(rc!Num_recibo = "", "NULL", rc!Num_recibo) ' @num_recibo
        SQL = SQL & ", " & IIf(IsNull(rc!Num_recibo), "NULL", rc!Num_recibo)    ' @num_recibo
        SQL = SQL & ", " & rc!seq_pgto    'FLOW 1110779 - Confitec - JFILHO/MFERREIRA - Corrigindo falha no processo de pagamento de sinistros, que n�o considerava o campo seq_pgto na hora de efetuar o acerto
        Set rc1 = rdocn.OpenResultset(SQL)
        rc1.Close
        Conta_lidas = Conta_lidas + 1
        ProgressBar1.Value = Conta_lidas
        rc.MoveNext
    Wend

    rc.Close
    rdocn.CommitTrans

    gridResumo.AddItem "Sinistro" & vbTab & Str(Conta_lidas)
'    Call goProducao.AdicionaLog(1, "Sinistro", 1)
  '  Call goProducao.AdicionaLog(2, Conta_lidas, 1)

    'grava log arquivo CTM
   ' If CTM = True Then'
  '      Call GravaLogCTM("OK - ", "Sinistro", "", "")
  '      Call GravaLogCTM("OK - ", Conta_lidas, "", "")
   ' End If

    Flag_Pgto_ok = True
    txtfim = Now
    Exit Sub
Erro:
    TrataErroGeral "Processa Pagamento Sinistro"
    TerminaSEGBR
End Sub
'R.FOUREAUX - 28/10/2019
Private Sub Processa_Pgto_ClienteDevolucao()
 
 On Error GoTo Erro
 Dim rc As rdoResultset
 Dim iPagamentosGerados As Long
 

 SQL = ""
 SQL = SQL & " EXEC seguros_db.dbo.SEGS14493_SPI @DataPagamento= '" & Format(Data_Pagamento, "yyyymmdd") & "'" & vbNewLine
 SQL = SQL & ", @DataSistema = '" & Format(Data_Sistema, "yyyymmdd") & "'" & vbNewLine
 SQL = SQL & ", @Usuario = '" & cUserName & "'" & vbNewLine
 
  Set rc = rdocn3.OpenResultset(SQL)
  
  If rc.EOF Then
    iPagamentosGerados = 0
        Else
    iPagamentosGerados = rc!total
    gridResumo.AddItem "ClienteDevolucao" & vbTab & Str(iPagamentosGerados)
  End If
  
  rc.Close
  
    Call goProducao.AdicionaLog(1, "ClienteDevolucao", 4)
    Call goProducao.AdicionaLog(2, iPagamentosGerados, 4)

    If CTM = True Then
        Call GravaLogCTM("OK - ", "ClienteDevolucao", "", "")
        Call GravaLogCTM("OK - ", iPagamentosGerados, "", "")
    End If

 
 
  Exit Sub
 
Erro:
    TrataErroGeral "Processa Pagamento Devolucao"
    TerminaSEGBR
End Sub

Private Sub Processa_Pgto_Corretor()
    Dim situacao As String, Descricao As String

    Dim flagprimeiravez As Boolean

    Dim a As Long

    Dim b As Long

    Dim C As Long

    On Error GoTo Erro
    dia_semana = Weekday(Data_Pagamento)
    rdocn.BeginTrans

    ' Obtem dados do Pagamento n�o retido
    SQL = "Exec gera_pgto_corretor_spi "
    SQL = SQL & "'" & Format(Data_Pagamento, "yyyymmdd") & "', "
    SQL = SQL & "'" & Format(Data_Sistema, "yyyymmdd") & "', "
    SQL = SQL & "'" & dia_semana & "', "
    SQL = SQL & "'" & cUserName & "'"

    Set rc_geral = rdocn.OpenResultset(SQL)

    If rc_geral(0) = 0 Then
        pagamentos_gerados_corretor = 0
        pagamentos_gerados_estipulante = 0
    Else
        pagamentos_gerados_corretor = rc_geral(0)
        pagamentos_gerados_estipulante = rc_geral(1)
    End If

    rc_geral.Close

    'Obtem dados de comiss�o retida
    SQL = "EXEC SEGS5398_SPI '" & Format(Data_Pagamento, "yyyymmdd") & "', '" & cUserName & "'"
    Set rc_geral = rdocn.OpenResultset(SQL)

    If Not rc_geral.EOF Then
        If rc_geral(0) <> 0 Then
            pagamentos_gerados_corretor = pagamentos_gerados_corretor + rc_geral(0)
        End If
    End If

    rc_geral.Close

    'Obtem dados de pr�-labore retida
    SQL = "EXEC SEGS5400_SPI '" & Format(Data_Pagamento, "yyyymmdd") & "', '" & cUserName & "'"
    Set rc_geral = rdocn.OpenResultset(SQL)

    If Not rc_geral.EOF Then
        If rc_geral(0) <> 0 Then
            pagamentos_gerados_estipulante = pagamentos_gerados_estipulante + rc_geral(0)
        End If
    End If

    rc_geral.Close

    'Genesco Silva Flow 5220798 - 01/12/2010
    SQL = "EXEC SEGS9294_SPI '" & Format(Data_Pagamento, "yyyymmdd") & "', '" & cUserName & "'"
    Set rc_geral = rdocn.OpenResultset(SQL)

    If Not rc_geral.EOF Then
        If rc_geral(0) <> 0 Then
            pagamentos_gerados_remuneracao_servico = pagamentos_gerados_remuneracao_servico + rc_geral(0)
        End If
    End If
    ' End Genesco Silva
    rc_geral.Close
    rdocn.CommitTrans

    If pagamentos_gerados_corretor = 0 And pagamentos_gerados_estipulante = 0 Then
        txtfim = Now
        MensagemBatch "Nenhum pagamento de corretor foi selecionado"
        Call goProducao.AdicionaLog(1, "Corretor", 2)
        Call goProducao.AdicionaLog(2, pagamentos_gerados_corretor, 2)
        Call goProducao.AdicionaLog(1, "Estipulante", 3)
        Call goProducao.AdicionaLog(2, pagamentos_gerados_estipulante, 3)
        Call goProducao.AdicionaLog(1, "Rem. Servi�o", 4)
        Call goProducao.AdicionaLog(2, pagamentos_gerados_remuneracao_servico, 4)

        'grava log arquivo CTM
        If CTM = True Then
            Call GravaLogCTM("OK - ", "Corretor", "", "")
            Call GravaLogCTM("OK - ", pagamentos_gerados_corretor, "", "")
            Call GravaLogCTM("OK - ", "Estipulante", "", "")
            Call GravaLogCTM("OK - ", pagamentos_gerados_estipulante, "", "")
            Call GravaLogCTM("OK - ", "Rem. Servi�o", "", "")
            Call GravaLogCTM("OK - ", pagamentos_gerados_remuneracao_servico, "", "")
        End If

        Flag_Pgto_ok = True
        Exit Sub
    End If

    gridResumo.AddItem "Corretor" & vbTab & Str(pagamentos_gerados_corretor)
    Call goProducao.AdicionaLog(1, "Corretor", 2)
    Call goProducao.AdicionaLog(2, pagamentos_gerados_corretor, 2)
    gridResumo.AddItem "Estipulante" & vbTab & Str(pagamentos_gerados_estipulante)
    Call goProducao.AdicionaLog(1, "Estipulante", 3)
    Call goProducao.AdicionaLog(2, pagamentos_gerados_estipulante, 3)
    gridResumo.AddItem "Remun. Servi�o" & vbTab & Str(pagamentos_gerados_remuneracao_servico)
    Call goProducao.AdicionaLog(1, "Remun. Servi�o", 4)
    Call goProducao.AdicionaLog(2, pagamentos_gerados_remuneracao_servico, 3)

    'grava log arquivo CTM
    If CTM = True Then
        Call GravaLogCTM("OK - ", "Corretor", "", "")
        Call GravaLogCTM("OK - ", pagamentos_gerados_corretor, "", "")
        Call GravaLogCTM("OK - ", "Estipulante", "", "")
        Call GravaLogCTM("OK - ", pagamentos_gerados_estipulante, "", "")
        Call GravaLogCTM("OK - ", "Rem. Servi�o", "", "")
        Call GravaLogCTM("OK - ", pagamentos_gerados_remuneracao_servico, "", "")
    End If

    Flag_Pgto_ok = True
    txtfim = Now
    Exit Sub

    Resume
Erro:
    TrataErroGeral "Processa Pagamento Corretor"
    TerminaSEGBR
End Sub

Private Sub Processa_Pgto_Cliente()

    Dim situacao As String
    Dim Descricao As String
    Dim flagprimeiravez As Boolean
    Dim Proposta_ant As Variant
    Dim proposta_id As Variant
    Dim proposta_basica As Variant
    Dim Produto_ant As Variant
    Dim flag_aprovacao_ant As Variant
    Dim endosso_id As Variant
    Dim Tp_Endosso_Ant As Variant
    Dim tp_endosso_id As Variant
    Dim sRestituicaoDocumentoGruMAPA As String
    Dim bAglutinaPagamentos As Boolean
    Dim proposta_cliente As Variant
    Dim Ramo_ant As Variant
    Dim CONTADOR_ARQUIVO As Integer

    On Error GoTo Erro

    'R.FOUREAUX - 25/10/2019
    Call Processa_Pgto_ClienteDevolucao


    SQL = ""
    SQL = SQL & "set nocount on " & vbNewLine
    SQL = SQL & "exec seguros_db.dbo.SEGS14353_SPS " & IIf(Endosso_Contestacao, "@EdsContestacao = 'S'", "@EdsContestacao = 'N'") & vbNewLine

    Set rc_geral = rdocn2.OpenResultset(SQL, rdOpenStatic)

    If Not rc_geral.EOF Then
        Conta_lidas = 0
    Else
        rdocn.RollbackTrans
        txtfim = Now
        MensagemBatch "Nenhum pagamento de cliente foi selecionado"
        Call goProducao.AdicionaLog(1, "Cliente", 4)
        Call goProducao.AdicionaLog(2, Conta_Processadas, 4)

        If CTM = True Then
            Call GravaLogCTM("OK - ", "Cliente", "", "")
            Call GravaLogCTM("OK - ", Conta_Processadas, "", "")
        End If

        rc_geral.Close
        Flag_Pgto_ok = True
        Exit Sub
    End If

    flagprimeiravez = True
    Retido_Banco_ant = ""
    Cliente_ant = ""
    tp_movimentacao = "1"
    ProgressBar1.Value = 0
    Total_mov = 0
    Proposta_ant = Null
    Tp_Endosso_Ant = Null
    Produto_ant = ""
    flag_aprovacao_ant = ""
    Ramo_ant = ""

    While Not rc_geral.EOF
        Conta_lidas = Conta_lidas + 1
        ProgressBar1.Value = Conta_lidas
        proposta_id = rc_geral!proposta_id
        proposta_cliente = rc_geral!proposta_id
        endosso_id = rc_geral!endosso_id
        proposta_basica = ""
        sRestituicaoDocumentoGruMAPA = rc_geral!restituicao_documento_gru_mapa
        tp_endosso_id = ObterTipoEndosso(CLng(proposta_id), CLng(endosso_id))
        If UCase(rc_geral!flag_aprovacao) = "S" Then
            Obtem_Proposta_Basica proposta_id, proposta_basica
            If Val(proposta_basica & "") <> 0 Then
                proposta_id = proposta_basica
            End If
        End If
        If (flagprimeiravez = False And UCase(rc_geral!aglutina_restituicao) = "S") And _
           (rc_geral!Produto_id = Produto_ant) And (rc_geral!ramo_id = Ramo_ant) Then
            bAglutinaPagamentos = False
        Else
            bAglutinaPagamentos = True
        End If
        If ((rc_geral!retido_banco <> Retido_Banco_ant _
             Or rc_geral!cliente_id <> Cliente_ant _
             Or proposta_id <> Proposta_ant _
             Or rc_geral!Produto_id <> Produto_ant _
             Or rc_geral!ramo_id <> Ramo_ant _
             Or rc_geral!flag_aprovacao <> flag_aprovacao_ant) _
             And (bAglutinaPagamentos = True)) _
             Or (CONTADOR_ARQUIVO = 32000) Then

            If Not flagprimeiravez Then
                If Total_mov >= 0 Then

                    If UCase(Retido_Banco_ant) = "S" Or _
                       (UCase(flag_aprovacao_ant) = "S" And _
                        (Tp_Endosso_Ant = 63 Or _
                         Tp_Endosso_Ant = 70 Or _
                         Tp_Endosso_Ant = 80 Or _
                         Tp_Endosso_Ant = 81 Or _
                         Tp_Endosso_Ant = 101 Or _
                         Tp_Endosso_Ant = 104 Or _
                         Tp_Endosso_Ant = 270 Or _
                         ((Produto_ant = 114 Or Produto_ant = 1141 Or Produto_ant = 1125) And Tp_Endosso_Ant = 200) Or _
                         (Produto_ant = 1152 And (Tp_Endosso_Ant = 64 Or Tp_Endosso_Ant = 269)))) Then
                        situacao_aux = "a"
                    Else
                        situacao_aux = "p"
                    End If

                    If UCase(sRestituicaoDocumentoGruMAPA) = "S" Then
                        situacao_aux = "a"
                    End If

                    Atualiza_Pagamento "RP", tp_movimentacao, 0, " ", Cliente_ant, " ", 0, 0, 0, Data_Pagamento, Total_mov, Total_mov, Proposta_ant, situacao_aux, 0

                    Conta_Processadas = Conta_Processadas + 1
                    rdocn.CommitTrans
                Else
                    rdocn.RollbackTrans
                End If

            Else
                flagprimeiravez = False
            End If

            Retido_Banco_ant = rc_geral!retido_banco
            Cliente_ant = rc_geral!cliente_id
            Proposta_ant = proposta_id
            Tp_Endosso_Ant = tp_endosso_id
            Produto_ant = rc_geral!Produto_id
            Ramo_ant = rc_geral!ramo_id
            flag_aprovacao_ant = Trim(rc_geral!flag_aprovacao)

            rdocn.BeginTrans

            Insere_Pagamento "RP"

            If CONTADOR_ARQUIVO = 32000 Then
                CONTADOR_ARQUIVO = 0
            End If
            Total_mov = 0
        End If

        If UCase(rc_geral!tp_operacao) = "C" Then
            Total_mov = Total_mov + Val(rc_geral!val_movimentacao)
        Else
            Total_mov = Total_mov - Val(rc_geral!val_movimentacao)
        End If

        If UCase(Trim(rc_geral!restituicao_unica)) = "S" Then
            Call AtualizarMovimentoEndossoResumo(Val(proposta_id), Val(endosso_id))
        Else
            Call Atualiza_Movimento(rc_geral!movimentacao_id)
        End If

        Set oFinanceiro = CreateObject("SEGL0026.cls00125")
        Call oFinanceiro.InserirExtratoRestituicao(gsSIGLASISTEMA, _
                                                   "SEGP0093", _
                                                   App.FileDescription, _
                                                   glAmbiente_id, _
                                                   "Sistema Autom�tico", _
                                                   CLng(proposta_cliente), _
                                                   TrocaPontoPorVirgula(rc_geral!val_movimentacao), _
                                                   CLng(endosso_id), _
                                                   0, _
                                                   "Gera��o de Acerto", _
                                                   "Aguardando aprova��o financeira")

        Set oFinanceiro = Nothing

        CONTADOR_ARQUIVO = CONTADOR_ARQUIVO + 1
        rc_geral.MoveNext
    Wend
    rc_geral.Close

    If Total_mov >= 0 Then
        If UCase(Retido_Banco_ant) = "S" Or _
            (UCase(flag_aprovacao_ant) = "S" And _
                (Tp_Endosso_Ant = 63 Or _
                 Tp_Endosso_Ant = 70 Or _
                 Tp_Endosso_Ant = 80 Or _
                 Tp_Endosso_Ant = 81 Or _
                 Tp_Endosso_Ant = 101 Or _
                 Tp_Endosso_Ant = 104 Or _
                 Tp_Endosso_Ant = 270 Or _
                 ((Produto_ant = 114 Or Produto_ant = 1141 Or Produto_ant = 1125) And Tp_Endosso_Ant = 200) Or _
                  (Produto_ant = 1152 And (Tp_Endosso_Ant = 64 Or Tp_Endosso_Ant = 269)))) Then
            situacao_aux = "a"
        Else
            situacao_aux = "p"
        End If

        If UCase(sRestituicaoDocumentoGruMAPA) = "S" Then
            situacao_aux = "a"
        End If

        Atualiza_Pagamento "RP", "1", 0, " ", Cliente_ant, " ", 0, 0, 0, Data_Pagamento, Total_mov, Total_mov, Proposta_ant, situacao_aux, 0

        Conta_Processadas = Conta_Processadas + 1
        rdocn.CommitTrans
    Else
        rdocn.RollbackTrans
    End If

    gridResumo.AddItem "Cliente" & vbTab & Str(Conta_Processadas)

    Call goProducao.AdicionaLog(1, "Cliente", 4)
    Call goProducao.AdicionaLog(2, Conta_Processadas, 4)

    If CTM = True Then
        Call GravaLogCTM("OK - ", "Cliente", "", "")
        Call GravaLogCTM("OK - ", Conta_Processadas, "", "")
    End If

    Flag_Pgto_ok = True
    txtfim = Now
    Exit Sub
Erro:
    TrataErroGeral "Processa Pagamento Cliente"
    TerminaSEGBR
End Sub

Private Sub Processa_Pgto_IOF_nao_Recolhido()
    Dim situacao As String, Descricao As String

    Dim flagprimeiravez As Boolean

    Dim Proposta_ant As Variant

    On Error GoTo Erro
    dia_semana = Weekday(Data_Pagamento)

    If dia_semana <= 4 Then
        dt_proxima_quarta = DateAdd("d", 4 - dia_semana, Data_Pagamento)
    Else
        dt_proxima_quarta = DateAdd("d", 11 - dia_semana, Data_Pagamento)
    End If

    ' Obtem dados do Pagamento
    SQL = Monta_query_IOF
    Set rc_geral = rdocn1.OpenResultset(SQL, rdOpenStatic)

    If Not rc_geral.EOF Then
        Conta_lidas = 0
        Conta_Processadas = 0
    Else
        rdocn.RollbackTrans
        txtfim = Now
        MensagemBatch "Nenhum pagamento de item financeiro foi selecionado"
        Call goProducao.AdicionaLog(1, "Item Financeiro", 5)
        Call goProducao.AdicionaLog(2, Conta_Processadas, 5)

        'grava log arquivo CTM
        If CTM = True Then
            Call GravaLogCTM("OK - ", "Item Financeiro", "", "")
            Call GravaLogCTM("OK - ", Conta_Processadas, "", "")
        End If

        rc_geral.Close
        Flag_Pgto_ok = True
        Exit Sub
    End If

    flagprimeiravez = True
    Retido_Banco_ant = ""
    Item_financeiro_ant = ""
    tp_movimentacao = "8"
    ProgressBar1.Value = 0
    Total_mov = 0
    Total_Multa = 0
    Total_Juros = 0

    While Not rc_geral.EOF
        Conta_lidas = Conta_lidas + 1
        ProgressBar1.Value = Conta_lidas
        multa = 0
        juros = 0

        If rc_geral!retido_banco <> Retido_Banco_ant Or rc_geral!cod_item_financeiro <> Item_financeiro_ant Then

            If Not flagprimeiravez Then
                If Total_mov >= 0 Then
                    Atualiza_Pagamento "IF", tp_movimentacao, 0, " ", 0, Item_financeiro_ant, 0, 0, 0, dt_proxima_quarta, Total_mov, Total_mov - Total_Juros - Total_Multa, 0, "p", 0
                    Conta_Processadas = Conta_Processadas + 1
                    rdocn.CommitTrans
                Else
                    rdocn.RollbackTrans
                End If

            Else
                flagprimeiravez = False
            End If

            Retido_Banco_ant = rc_geral!retido_banco
            Item_financeiro_ant = rc_geral!cod_item_financeiro
            rdocn.BeginTrans
            Insere_Pagamento "IF"
            Total_mov = 0
            Total_Multa = 0
            Total_Juros = 0
        End If

        If UCase(rc_geral!retido_banco) = "N" Then
            dt_recebimento = ""
            Obtem_Dt_Recebimento

            If Trim(dt_recebimento) <> "" Then
                dia_semana_recebimento = Weekday(dt_recebimento)
            Else
                dia_semana_recebimento = Weekday(Data_Sistema)
                dt_recebimento = Data_Sistema
            End If

        Else
            dia_semana_recebimento = Weekday(Data_Sistema)
            dt_recebimento = Data_Sistema
        End If

        If dia_semana_recebimento <= 4 Then
            dt_para_recolhimento = DateAdd("d", 4 - dia_semana_recebimento, dt_recebimento)
        Else
            dt_para_recolhimento = DateAdd("d", 11 - dia_semana_recebimento, dt_recebimento)
        End If

        If UCase(rc_geral!retido_banco) = "N" Then
            If Format(dt_para_recolhimento, "yyyymmdd") < Format(dt_proxima_quarta, "yyyymmdd") Then
                If UCase(rc_geral!tp_operacao) = "C" Then
                    Calcula_Juros "IOF", Val(rc_geral!val_movimentacao), dt_para_recolhimento, dt_proxima_quarta
                    Total_mov = Total_mov + multa + juros
                    Total_Multa = Total_Multa + multa
                    Total_Juros = Total_Juros + juros
                End If
            End If
        End If

        If UCase(rc_geral!tp_operacao) = "C" Then
            Total_mov = Total_mov + Val(rc_geral!val_movimentacao)
        Else
            Total_mov = Total_mov - Val(rc_geral!val_movimentacao)
        End If

        Call Atualiza_Movimento(rc_geral!movimentacao_id)
        rc_geral.MoveNext
    Wend

    rc_geral.Close

    If Total_mov >= 0 Then
        Atualiza_Pagamento "IF", tp_movimentacao, 0, " ", 0, Item_financeiro_ant, 0, 0, 0, dt_proxima_quarta, Total_mov, Total_mov - Total_Juros - Total_Multa, 0, "p", 0
        '  Conta_Processadas = Conta_Processadas + Conta_lidas
        Conta_Processadas = Conta_Processadas + 1
        rdocn.CommitTrans
    Else
        rdocn.RollbackTrans
    End If

    gridResumo.AddItem "Item Financeiro" & vbTab & Str(Conta_Processadas)
    Call goProducao.AdicionaLog(1, "Item Financeiro", 5)
    Call goProducao.AdicionaLog(2, Conta_Processadas, 5)

    'grava log arquivo CTM
    If CTM = True Then
        Call GravaLogCTM("OK - ", "Item Financeiro", "", "")
        Call GravaLogCTM("OK - ", Conta_Processadas, "", "")
    End If

    Flag_Pgto_ok = True
    txtfim = Now
    multa = 0
    juros = 0
    Total_Multa = 0
    Total_Juros = 0
    Exit Sub
Erro:
    TrataErroGeral "Processa Pagamento IOF nao recolhido"
    TerminaSEGBR
End Sub

Private Sub lstOrigem_Click()
    cmdOk.Enabled = True
End Sub

Private Sub Le_Dias_Antecedencia()
    Dim rc As rdoResultset

    Dim SQL As String

    On Error GoTo Erro
    SQL = "SELECT val_parametro FROM ps_parametro_tb "
    SQL = SQL & " WHERE parametro = 'DIAS ANTECEDENCIA PGTO'"
    'FLOW 617031 RNC - Marcelo Ferreira - Confitec Sistemas 2008-10-31
    'Alterando conex�o auxiliar
    '    Set rc = rdocn1.OpenResultset(SQL)
    '-------------------------------------------------------------------------------------------------------
    Set rc = rdocn.OpenResultset(SQL)

    If Not rc.EOF Then
        Dias_Antecedencia = Val(Trim(rc!val_parametro))
    Else
        MensagemBatch "Parametro Dias de Antecedencia n�o encontrado. Programa ser� cancelado"
        End
    End If

    rc.Close
    Exit Sub
Erro:
    TrataErroGeral "Le Dias Antecedencia"
    TerminaSEGBR
End Sub

Private Sub Insere_Pagamento(ByVal Origem As String)
    Dim rc As rdoResultset

    Dim SQL As String

    On Error GoTo Erro
    SQL = "Exec ps_acerto_spi "
    SQL = SQL & "'" & Origem & "',"
    SQL = SQL & "'" & Format(Data_Pagamento, "yyyymmdd") & "',"
    SQL = SQL & "'" & Format(Data_Sistema, "yyyymmdd") & "',"
    SQL = SQL & "'s',"
    SQL = SQL & "'p',"
    SQL = SQL & "0.00,"
    SQL = SQL & "'1',"  'chamada pelo VB
    SQL = SQL & "'" & cUserName & "',"
    SQL = SQL & "0"
    Set rc = rdocn.OpenResultset(SQL)

    If Not rc.EOF Then
        acerto_id = rc(0)
    End If

    rc.Close
    Exit Sub
Erro:
    TrataErroGeral "Insere Pagamento"
    TerminaSEGBR
End Sub

Private Sub Obtem_Dt_Recebimento()
    Dim rc As rdoResultset

    Dim SQL As String

    On Error GoTo Erro
    SQL = "SELECT dt_recebimento "
    SQL = SQL & " FROM ps_mov_agenda_cobranca_tb a,"
    SQL = SQL & "      agendamento_cobranca_tb  b "
    SQL = SQL & " WHERE movimentacao_id = " & rc_geral!movimentacao_id & " and "
    SQL = SQL & "       a.proposta_id = b.proposta_id and "
    SQL = SQL & "       a.num_cobranca = b.num_cobranca "
    Set rc = rdocn.OpenResultset(SQL)

    If Not rc.EOF Then
        dt_recebimento = Format(rc!dt_recebimento, "dd/mm/yyyy")
        rc.Close
        Exit Sub
    Else
        rc.Close
        SQL = "SELECT dt_recebimento "
        SQL = SQL & " FROM ps_mov_endosso_financeiro_tb a,"
        SQL = SQL & "      agendamento_cobranca_tb b"
        SQL = SQL & " WHERE movimentacao_id = " & rc_geral!movimentacao_id & " and "
        SQL = SQL & "       a.proposta_id = b.proposta_id and "
        SQL = SQL & "       a.endosso_id = b.num_endosso and "
        SQL = SQL & "       situacao = 'a' "
        Set rc = rdocn.OpenResultset(SQL)

        If Not rc.EOF Then
            '03/08/06 ecosta flow 152327
            'dt_recebimento = Format(rc!dt_emissao, "dd/mm/yyyy")
            dt_recebimento = Format(rc!dt_recebimento, "dd/mm/yyyy")
            rc.Close
            Exit Sub
        End If
    End If

    Exit Sub
Erro:
    TrataErroGeral "Obtem Dt Recebimento"
    TerminaSEGBR
End Sub

Private Sub Obtem_Proposta_Basica(ByVal proposta_id, ByRef proposta_basica)
    Dim rc As rdoResultset

    Dim SQL As String

    On Error GoTo Erro
    SQL = "SELECT b.proposta_id "
    SQL = SQL & " FROM proposta_adesao_tb a with (nolock) "
    SQL = SQL & " JOIN apolice_tb  b with (nolock) on a.apolice_id = b.apolice_id "
    SQL = SQL & "  AND a.ramo_id = b.ramo_id "
    SQL = SQL & " WHERE a.proposta_id = " & proposta_id
    Set rc = rdocn.OpenResultset(SQL)

    If Not rc.EOF Then
        proposta_basica = rc!proposta_id
    Else
        proposta_basica = "0"
    End If

    rc.Close
    Exit Sub
Erro:
    TrataErroGeral "Obtem Proposta Basica"
    TerminaSEGBR
End Sub

Private Function Comissao_Paga() As Boolean
    Dim rc As rdoResultset

    Dim SQL As String

    On Error GoTo Erro
    Comissao_Paga = False
    SQL = "SELECT b.dt_acerto FROM ps_mov_corretor_pj_tb a,"
    SQL = SQL & "                     ps_acerto_tb b, "
    SQL = SQL & "                     ps_acerto_pagamento_tb c "
    SQL = SQL & " WHERE movimentacao_id = " & rc_geral!movimentacao_id & " and "
    SQL = SQL & "       a.acerto_id is not null and "
    SQL = SQL & "       a.acerto_id = b.acerto_id and "
    SQL = SQL & "       a.acerto_id = c.acerto_id and "
    SQL = SQL & "       b.dt_acerto <= '" & Format(dt_sexta_anterior, "yyyymmdd") & "' and "
    SQL = SQL & "       c.situacao = 'a'"
    Set rc = rdocn.OpenResultset(SQL)

    If Not rc.EOF Then
        Comissao_Paga = True
        Dt_Pagamento_Comissao = rc!dt_acerto
        rc.Close
        Exit Function
    Else
        rc.Close
        SQL = "SELECT b.dt_acerto FROM  ps_mov_corretor_pf_tb a,"
        SQL = SQL & "                   ps_acerto_tb b, "
        SQL = SQL & "                   ps_acerto_pagamento_tb c "
        SQL = SQL & " WHERE movimentacao_id = " & rc_geral!movimentacao_id & " and "
        SQL = SQL & "       a.acerto_id is not null and "
        SQL = SQL & "       a.acerto_id = b.acerto_id and "
        SQL = SQL & "       a.acerto_id = c.acerto_id and "
        SQL = SQL & "       b.dt_acerto <= '" & Format(dt_sexta_anterior, "yyyymmdd") & "' and "
        SQL = SQL & "       c.situacao = 'a'"
        Set rc = rdocn.OpenResultset(SQL)

        If Not rc.EOF Then
            Comissao_Paga = True
            Dt_Pagamento_Comissao = rc!dt_acerto
            rc.Close
            Exit Function
        End If
    End If

    Exit Function
Erro:
    TrataErroGeral "Comissao paga"
    TerminaSEGBR
End Function

Private Sub Calcula_Juros(ByVal cod_item_financeiro, _
                          valor As String, _
                          ByVal Data_Base, _
                          Data_Pagamento As Date)
    Dim rc As rdoResultset

    Dim SQL As String

    On Error GoTo Erro
    data_indice = "01/" & Format(Data_Sistema, "mm/yyyy")
    data_indice = DateAdd("m", -1, data_indice)
    SQL = "select val_indice "
    SQL = SQL & " FROM  indice_tb a, val_indice_tb b "
    SQL = SQL & " WHERE a.indice_id = b.indice_id and "
    SQL = SQL & "       a.nome = 'SELIC'   and "
    SQL = SQL & "       dt_inicio_vigencia = '" & Format(data_indice, "yyyymmdd") & "'"
    Set rc = rdocn.OpenResultset(SQL)

    If rc.EOF Then
        MensagemBatch "Indice SELIC n�o cadastrado para dia " & Format(data_indice, "dd/mm/yyyy") & ". Programa ser� cancelado"
        rdocn.RollbackTrans
        End
    End If

    rc.Close
    dias = DateDiff("d", Data_Base, Data_Pagamento)

    '
    ' se a multa exceder 20% assume 20%
    '
    If dias * 0.33 > 20 Then
        multa = Trunca(valor * 0.2)
    Else
        multa = Trunca(valor * ((dias * 0.33) / 100))
    End If

    If DateDiff("m", Data_Base, Data_Pagamento) = 1 Then
        juros = Trunca(valor * (1 / 100))
        Exit Sub
    End If

    data_indice = "01/" & Format(Data_Base, "mm/yyyy")
    data_indice = DateAdd("m", 1, data_indice)
    SQL = "select isnull(sum(val_indice),0) + 1 val_indice "
    SQL = SQL & " FROM  indice_tb a, val_indice_tb b "
    SQL = SQL & " WHERE a.indice_id = b.indice_id and "
    SQL = SQL & "       a.nome = 'SELIC'   and "
    SQL = SQL & "       dt_inicio_vigencia >= '" & Format(data_indice, "yyyymmdd") & "'"
    Set rc = rdocn.OpenResultset(SQL)

    If Not rc.EOF Then
        juros = Trunca(valor * (Val(rc!val_indice) / 100))
    End If

    rc.Close
    Exit Sub
Erro:
    TrataErroGeral "Calcula Juros"
    TerminaSEGBR
End Sub

Sub Inclui_Mov_Item_Financeiro(ByVal cod_item_financeiro, ByVal dtmovimento, ByVal valor)
    Dim rc As rdoResultset

    On Error GoTo TrataErro
    SQL = "exec ps_mov_item_financeiro_spi "
    SQL = SQL & rc_geral!movimentacao_id & ","
    SQL = SQL & "'" & cod_item_financeiro & "',"
    SQL = SQL & "'c',"
    SQL = SQL & "'" & Format(dtmovimento, "yyyymmdd") & "',"
    SQL = SQL & MudaVirgulaParaPonto(valor) & ","
    SQL = SQL & "'" & rc_geral!retido_banco & "',"
    SQL = SQL & "null,"
    SQL = SQL & "null,"
    SQL = SQL & "null,"
    SQL = SQL & "'3'" & ","  ' tipo especializacao 1 - baixa  2 - endosso 3 - sem especializacao
    SQL = SQL & "'" & cUserName & "',"
    SQL = SQL & acerto_id
    Set rc = rdocn.OpenResultset(SQL)
    rc.Close
    Exit Sub
TrataErro:
    TrataErroGeral "Inclui Mov item financeiro"
    TerminaSEGBR
End Sub

Function Trunca(valor) As String

    If ConfiguracaoBrasil Then
        pos = InStr(valor, ",")
    Else
        pos = InStr(valor, ".")
    End If

    If pos <> 0 Then
        valor = Mid(valor, 1, pos + 2)
    End If

    Trunca = valor
End Function

Private Function Obtem_GTR_Implantado() As Boolean
    Dim objAmbiente As Object, strGTR_Implantado As String

    Obtem_GTR_Implantado = False
    Set objAmbiente = CreateObject("Ambiente.ClasseAmbiente")
    strGTR_Implantado = objAmbiente.RetornaValorAmbiente("GTR", "SISTEMA", "GTR_IMPLANTADO", gsCHAVESISTEMA)

    If (strGTR_Implantado = "s") Then
        Obtem_GTR_Implantado = True
    End If

    Set objAmbiente = Nothing
End Function

Private Function ObterTipoEndosso(ByVal lPropostaId As Long, _
                                  ByVal iEndossoId As Long) As Integer
    Dim sSQL As String

    Dim rsTpEndosso As rdoResultset

    On Error GoTo Erro
    'Selecionando o tipo de endosso
    sSQL = ""
    adSQL sSQL, "SELECT tp_endosso_id"
    adSQL sSQL, "  FROM endosso_tb with (nolock) "
    adSQL sSQL, " WHERE proposta_id = " & lPropostaId
    adSQL sSQL, "   AND endosso_id = " & iEndossoId
    Set rsTpEndosso = rdocn.OpenResultset(sSQL)

    If Not rsTpEndosso.EOF Then
        ObterTipoEndosso = rsTpEndosso("tp_endosso_id")
    Else
        ObterTipoEndosso = 0
    End If

    rsTpEndosso.Close
    Set rsTpEndosso = Nothing
    Exit Function
Erro:
    Call TrataErroGeral("ObterTipoEndosso", Me.name)
    Call TerminaSEGBR
End Function

Private Sub AtualizarMovimentoEndossoResumo(lPropostaId As Long, lEndossoId As Long)

'## Atualiza a movimenta��o de cada endosso financeiro do endosso resumo
'## Abosco @ 2003 dec 05
    On Error GoTo TrataErro
    Dim rsEndosso As rdoResultset

    Dim sSQL As String

    sSQL = ""
    sSQL = sSQL & "    SELECT movimentacao_id" & vbNewLine
    sSQL = sSQL & "      FROM endosso_resumo_tb" & vbNewLine
    sSQL = sSQL & "INNER JOIN endosso_financeiro_tb" & vbNewLine
    sSQL = sSQL & "        ON endosso_financeiro_tb.endosso_resumo_id = endosso_resumo_tb.endosso_resumo_id" & vbNewLine
    sSQL = sSQL & "INNER JOIN ps_mov_endosso_financeiro_tb" & vbNewLine
    sSQL = sSQL & "        ON ps_mov_endosso_financeiro_tb.proposta_id = endosso_financeiro_tb.proposta_id" & vbNewLine
    sSQL = sSQL & "       AND ps_mov_endosso_financeiro_tb.endosso_id  = endosso_financeiro_tb.endosso_id" & vbNewLine
    sSQL = sSQL & "     WHERE endosso_resumo_tb.proposta_id = " & lPropostaId & vbNewLine
    sSQL = sSQL & "       AND endosso_resumo_tb.endosso_id = " & lEndossoId & vbNewLine
    Set rsEndosso = rdocn1.OpenResultset(sSQL)

    While Not rsEndosso.EOF
        Call Atualiza_Movimento(rsEndosso("movimentacao_id"))
        Call rsEndosso.MoveNext
    Wend

    Set rsEndosso = Nothing
    Exit Sub
TrataErro:
    Call TrataErroGeral("AtualizarMovimentoEndossoResumo", Me.name)
    Call TerminaSEGBR
End Sub

'Demanda 654224 - emaior - 27/05/2009 - Projeto Visanet
'Gera��o de pagamento para cart�o de cr�dito.
Sub Processa_Pgto_Cartao()
    Dim rc As rdoResultset

    Dim SQL As String

    Dim acerto_id As Long

    Dim ArquivoAnterior As Variant

    On Error GoTo Erro
    txtIni.Text = Now
    'Obtem dados do Pagamento
    SQL = Monta_Query_Cartao
    Set rc_geral = rdocn1.OpenResultset(SQL, rdOpenStatic)

    If Not rc_geral.EOF Then
        Conta_lidas = 0
    Else
        rdocn.RollbackTrans
        txtfim = Now
        MensagemBatch "Nenhum pagamento de cartao de credito foi selecionado"
        Call goProducao.AdicionaLog(1, "Cartao de Credito", 7)
        Call goProducao.AdicionaLog(2, Conta_Processadas, 7)

        'grava log arquivo CTM
        If CTM = True Then
            Call GravaLogCTM("OK - ", "Cartao de Credito", "", "")
            Call GravaLogCTM("OK - ", Conta_Processadas, "", "")
        End If

        rc_geral.Close
        Flag_Pgto_ok = True
        Exit Sub
    End If

    flagprimeiravez = True
    Retido_Banco_ant = ""
    dt_repasse_ant = ""
    Produto_ant = ""
    Ramo_ant = ""
    tp_movimentacao = "12"
    ProgressBar1.Value = 0
    Total_mov = 0

    While Not rc_geral.EOF
        Conta_lidas = Conta_lidas + 1
        ProgressBar1.Value = Conta_lidas

        If rc_geral!arquivo_envio_bb <> ArquivoAnterior Then
            If Not flagprimeiravez Then
                If Total_mov >= 0 Then
                    situacao_aux = "p"
                    Retido_Banco_ant = "S"
                    Atualiza_Pagamento "CR", tp_movimentacao, 0, " ", 0, " ", 0, 0, 0, Data_Pagamento, Total_mov, Total_mov, 0, situacao_aux, 0, 1
                    Conta_Processadas = Conta_Processadas + 1
                    rdocn.CommitTrans
                Else
                    rdocn.RollbackTrans
                End If

            Else
                flagprimeiravez = False
            End If

            ArquivoAnterior = rc_geral!arquivo_envio_bb
            rdocn.BeginTrans
            Insere_Pagamento "CR"
            Total_mov = 0
        End If

        If UCase(rc_geral!tp_operacao) = "C" Then
            Total_mov = Total_mov + Val(rc_geral!val_movimentacao)
        Else
            Total_mov = Total_mov - Val(rc_geral!val_movimentacao)
        End If

        Call Atualiza_Movimento(rc_geral!movimentacao_id)
        rc_geral.MoveNext
    Wend

    rc_geral.Close

    If Total_mov >= 0 Then
        situacao_aux = "p"
        Retido_Banco_ant = "S"
        Atualiza_Pagamento "CR", tp_movimentacao, 0, " ", 0, " ", 0, 0, 0, Data_Pagamento, Total_mov, Total_mov, 0, situacao_aux, 0, 1
        Conta_Processadas = Conta_Processadas + 1
        rdocn.CommitTrans
    Else
        rdocn.RollbackTrans
    End If

    gridResumo.AddItem "Cart�o de Cr�dito" & vbTab & Str(Conta_Processadas)
    Call goProducao.AdicionaLog(1, "Cart�o de Cr�dito", 7)
    Call goProducao.AdicionaLog(2, Conta_Processadas, 7)

    'grava log arquivo CTM
    If CTM = True Then
        Call GravaLogCTM("OK - ", "Cart�o de Cr�dito", "", "")
        Call GravaLogCTM("OK - ", Conta_Processadas, "", "")
    End If

    Flag_Pgto_ok = True
    txtfim = Now
    Exit Sub
Erro:
    TrataErroGeral "Processa Pagamento de Cart�o de Cr�dito"
    TerminaSEGBR
End Sub

'Demanda 654224 - emaior - 27/05/2009 - Projeto Visanet
'Gera��o de pagamento para cart�o de cr�dito.
Private Function Monta_Query_Cartao() As String
    Dim SQL As String

    On Error GoTo Error
    'Criando tabela com as movimentacoes a gerar acerto
    SQL = "SELECT  ps_movimentacao_tb.movimentacao_id" & vbNewLine
    SQL = SQL & ", proposta_tb.produto_id" & vbNewLine
    SQL = SQL & ", proposta_tb.ramo_id" & vbNewLine
    SQL = SQL & ", repasse_cartao_credito_tb.dt_repasse" & vbNewLine
    SQL = SQL & ", ps_mov_repasse_bb_tb.tp_operacao" & vbNewLine
    SQL = SQL & ", ps_mov_repasse_bb_tb.retido_banco" & vbNewLine
    SQL = SQL & ", ps_mov_repasse_bb_tb.val_movimentacao" & vbNewLine
    SQL = SQL & ", arquivo_envio_bb" & vbNewLine
    SQL = SQL & " FROM ps_mov_repasse_cartao_credito_tb (NOLOCK) " & vbNewLine
    SQL = SQL & " JOIN ps_mov_repasse_bb_tb (NOLOCK) " & vbNewLine
    SQL = SQL & "   ON ps_mov_repasse_bb_tb.movimentacao_id = ps_mov_repasse_cartao_credito_tb.movimentacao_id " & vbNewLine
    SQL = SQL & " JOIN ps_movimentacao_tb (NOLOCK) " & vbNewLine
    SQL = SQL & "   ON ps_movimentacao_tb.movimentacao_id = ps_mov_repasse_cartao_credito_tb.movimentacao_id " & vbNewLine
    SQL = SQL & " JOIN repasse_cartao_credito_tb (NOLOCK) " & vbNewLine
    SQL = SQL & "   ON repasse_cartao_credito_tb.repasse_cartao_credito_id = ps_mov_repasse_cartao_credito_tb.repasse_cartao_credito_id " & vbNewLine
    SQL = SQL & " JOIN proposta_tb (NOLOCK) " & vbNewLine
    SQL = SQL & "   ON proposta_tb.proposta_id = repasse_cartao_credito_tb.proposta_id " & vbNewLine
    SQL = SQL & " WHERE ps_mov_repasse_bb_tb.acerto_id IS NULL" & vbNewLine
    SQL = SQL & "   AND repasse_cartao_credito_tb.dt_envio_bb IS NOT NULL" & vbNewLine
    SQL = SQL & "   AND repasse_cartao_credito_tb.dt_repasse IS NOT NULL" & vbNewLine
    SQL = SQL & "   AND proposta_tb.situacao<>'T' " & vbNewLine
    SQL = SQL & "   AND ISNULL(ps_movimentacao_tb.situacao,'') <> 'T'" & vbNewLine
    'mathayde - 10/08/2009
    'Demanda: 654224 - Ordernando por arquivo para definir quebra para acertos
    SQL = SQL & " ORDER BY arquivo_envio_bb" & vbNewLine
    '    SQL = SQL & " ORDER BY proposta_tb.produto_id " & vbNewLine
    '    SQL = SQL & "        , proposta_tb.ramo_id" & vbNewLine
    '    SQL = SQL & "        , repasse_cartao_credito_tb.dt_repasse" & vbNewLine
    '    SQL = SQL & "        , ps_movimentacao_tb.dt_movimentacao" & vbNewLine
    '    SQL = SQL & "        , ps_movimentacao_tb.movimentacao_id"
    Monta_Query_Cartao = SQL
    Exit Function
Error:
    Call TrataErroGeral("Monta_Query_Cartao", Me.name)
    Call TerminaSEGBR
End Function

'pmelo - Acerto Sinistro CDC - 05/11/09
Private Function Processa_Pgto_Sinistro_CDC() As Boolean
    Dim rc As rdoResultset, rc1 As rdoResultset

    Dim SQL As String, Seq_Evento As String

    Dim situacao As String, Descricao As String, MOTIVO As String

    Dim vSeq_Estimativa As String

    On Error GoTo Erro
    ' Obtem dados do Pagamento
    SQL = Monta_query_Sinistro("CDC")
    rdocn.BeginTrans
    Set rc = rdocn1.OpenResultset(SQL, rdOpenStatic)

    If Not rc.EOF Then
        Conta_lidas = 0
    Else
        rdocn.RollbackTrans
        txtfim = Now
        rc.Close
        Flag_Pgto_ok = True
        Processa_Pgto_Sinistro_CDC = False
        Exit Function
    End If

    flagprimeiravez = True
    Retido_Banco_ant = ""
    dt_repasse_ant = ""
    Produto_ant = ""
    Ramo_ant = ""
    tp_movimentacao = "10"
    ProgressBar1.Value = 0
    Total_mov = 0

    While Not rc.EOF
        Descricao = "N� DA AUTORIZA��O: " & rc("chave_autorizacao_sinistro") & "-" & rc("chave_autorizacao_sinistro_dv")
        MOTIVO = ""
        situacao = ""
        SQL = "                                                                       " & vbNewLine
        SQL = SQL & "SELECT seq_evento, situacao, motivo_encerramento                 " & vbNewLine
        SQL = SQL & "  FROM sinistro_historico_tb                                     " & vbNewLine
        SQL = SQL & " WHERE sinistro_id            = " & rc!sinistro_id & "           " & vbNewLine
        SQL = SQL & "   AND ramo_id                = " & rc!ramo_id & "               " & vbNewLine
        SQL = SQL & "   AND seguradora_cod_susep   = " & rc!seguradora_cod_susep & "  " & vbNewLine
        SQL = SQL & "   AND sucursal_seguradora_id = " & rc!sucursal_seguradora_id & "" & vbNewLine
        SQL = SQL & "   AND apolice_id             = " & rc!Apolice_id & "            " & vbNewLine
        SQL = SQL & " ORDER BY seq_evento DESC                                        "
        Set rc1 = rdocn.OpenResultset(SQL)

        If Not rc1.EOF Then
            If IsNull(rc1!MOTIVO_encerramento) = False Then
                MOTIVO = Trim(rc1!MOTIVO_encerramento)
            End If

            If IsNull(rc1!situacao) = False Then
                situacao = Trim(rc1!situacao)
            End If
        End If

        rc1.Close
        SQL = "                                                                       " & vbNewLine
        SQL = SQL & "SELECT MAX(seq_estimativa)                                       " & vbNewLine
        SQL = SQL & "  FROM sinistro_estimativa_tb                                    " & vbNewLine
        SQL = SQL & " WHERE apolice_id             = " & rc!Apolice_id & "            " & vbNewLine
        SQL = SQL & "   AND sinistro_id            = " & rc!sinistro_id & "           " & vbNewLine
        SQL = SQL & "   AND sucursal_seguradora_id = " & rc!sucursal_seguradora_id & "" & vbNewLine
        SQL = SQL & "   AND seguradora_cod_susep   = " & rc!seguradora_cod_susep & "  " & vbNewLine
        SQL = SQL & "   AND ramo_id                = " & rc!ramo_id
        Set rc1 = rdocn.OpenResultset(SQL)
        vSeq_Estimativa = rc1(0)
        rc1.Close
        SQL = "                                                        " & vbNewLine
        SQL = SQL & "EXEC sinistro_historico_spi " & "                 " & vbNewLine
        SQL = SQL & "   " & rc!sinistro_id & "                         " & vbNewLine    ' sinistro_id
        SQL = SQL & ",  " & rc!Apolice_id & "                          " & vbNewLine    ' apolice_id
        SQL = SQL & ",  " & rc!sucursal_seguradora_id & "              " & vbNewLine    ' sucursal_seguradora_id
        SQL = SQL & ",  " & rc!seguradora_cod_susep & "                " & vbNewLine    ' seguradora_cod_susep
        SQL = SQL & ",  " & rc!ramo_id & "                             " & vbNewLine    ' ramo_id
        SQL = SQL & ",  10026" & "                                     " & vbNewLine    ' Transfer�ncia para o financeiro
        SQL = SQL & ", '" & situacao & "'                              " & vbNewLine    ' situacao
        SQL = SQL & ",  " & IIf(MOTIVO = "", "NULL", "'" & MOTIVO & "'") & vbNewLine    ' motivo
        SQL = SQL & ", '" & Format(Dt_Sistema, "yyyymmdd") & "'        " & vbNewLine    ' dt_evento
        SQL = SQL & ", '" & cUserName & "'                             " & vbNewLine    ' usuario
        SQL = SQL & ",  " & vSeq_Estimativa                                          ' �ltimo seq_estimativa
        Set rc1 = rdocn.OpenResultset(SQL)
        Seq_Evento = rc1(0)
        rc1.Close
        vsql = "                                           " & vbNewLine
        vsql = vsql & "EXEC sinistro_item_historico_spi    " & vbNewLine
        vsql = vsql & "   " & rc!sinistro_id & "           " & vbNewLine
        vsql = vsql & ",  " & rc!Apolice_id & "            " & vbNewLine
        vsql = vsql & ",  " & rc!sucursal_seguradora_id & "" & vbNewLine
        vsql = vsql & ",  " & rc!seguradora_cod_susep & "  " & vbNewLine
        vsql = vsql & ",  " & rc!ramo_id & "               " & vbNewLine
        vsql = vsql & ",  " & Seq_Evento & "               " & vbNewLine
        vsql = vsql & ", '" & Descricao & "'               " & vbNewLine
        vsql = vsql & ", '" & cUserName & "'               "
        rdocn.Execute (vsql)

        'Verifica se o GTR est� implantado e grava o registro
        If GTR_Implantado Then
            ' Grava registro GTR na tabela evento_SEGBR_sinistro_tb
            SQL = "                                                 " & vbNewLine
            SQL = SQL & "EXEC evento_SEGBR_sinistro_spi             " & vbNewLine
            SQL = SQL & "   " & rc!sinistro_id & "                  " & vbNewLine    ' @sinistro_id
            SQL = SQL & ",  " & rc!Apolice_id & "                   " & vbNewLine    ' @apolice_id
            SQL = SQL & ",  " & rc!seguradora_cod_susep & "         " & vbNewLine    ' @seguradora_cod_susep
            SQL = SQL & ",  " & rc!sucursal_seguradora_id & "       " & vbNewLine    ' @sucursal_seguradora_id
            SQL = SQL & ",  " & rc!ramo_id & "                      " & vbNewLine    ' @ramo_id
            SQL = SQL & ",  10026                                   " & vbNewLine    ' @evento_SEGBR_id
            SQL = SQL & ", '" & Format(Dt_Sistema, "yyyymmdd") & "' " & vbNewLine    ' @dt_evento
            SQL = SQL & ",  1                                       " & vbNewLine    ' @entidade_id (1 = Alian�a do Brasil)
            SQL = SQL & ", 'SEGBR'                                  " & vbNewLine    ' @localizacao
            SQL = SQL & ", '" & cUserName & "'                      " & vbNewLine    ' @usuario
            SQL = SQL & ",  NULL                                    " & vbNewLine    ' @produto_id
            SQL = SQL & ",  NULL                                    " & vbNewLine    ' @sinistro_bb
            SQL = SQL & ",  " & rc!proposta_id & "                  " & vbNewLine    ' @proposta_id
            SQL = SQL & ",  " & IIf(IsNull(rc!Num_recibo), "NULL", rc!Num_recibo)    ' @num_recibo
            rdocn.Execute (SQL)
        End If

        If flagprimeiravez Then
            flagprimeiravez = False
            Insere_Pagamento "SN"
        End If

        Call Atualiza_Movimento(rc!movimentacao_id)

        Total_mov = Total_mov + Val(rc!val_movimentacao)

        SQL = "                                                                   " & vbNewLine
        SQL = SQL & "EXEC SEGS8048_SPU                                            " & vbNewLine
        SQL = SQL & "   " & rc!sinistro_id & "                                    " & vbNewLine
        SQL = SQL & ",  " & acerto_id & "                                         " & vbNewLine
        SQL = SQL & ",  " & rc!Apolice_id & "                                     " & vbNewLine
        SQL = SQL & ",  " & rc!sucursal_seguradora_id & "                         " & vbNewLine
        SQL = SQL & ",  " & rc!seguradora_cod_susep & "                           " & vbNewLine
        SQL = SQL & ",  " & rc!ramo_id & "                                        " & vbNewLine
        SQL = SQL & ", '" & Format(rc!dt_acerto_contas_sinistro, "yyyymmdd") & "' " & vbNewLine
        SQL = SQL & ",  " & rc!forma_pgto_id & "                                  " & vbNewLine
        SQL = SQL & ",  " & rc!beneficiario_id & "                                " & vbNewLine
        SQL = SQL & ",  " & rc!item_val_estimativa & "                            " & vbNewLine
        SQL = SQL & ",  " & rc!seq_pgto & "                                       " & vbNewLine
        SQL = SQL & ", '" & rc!tp_emissao & "'                                    " & vbNewLine
        SQL = SQL & ", '" & cUserName & "'"
        Set rc1 = rdocn.OpenResultset(SQL)
        rc1.Close
        Conta_lidas = Conta_lidas + 1
        ProgressBar1.Value = Conta_lidas
        rc.MoveNext
    Wend

    rc.Close
    situacao_aux = "a"
    Retido_Banco_ant = "s"
    Atualiza_Pagamento "SN", 0, 0, " ", 0, " ", 0, 0, 0, Data_Pagamento, Total_mov, Total_mov, 0, situacao_aux, 0, 1
    rdocn.CommitTrans
    Processa_Pgto_Sinistro_CDC = True
    Flag_Pgto_ok = True

    txtfim = Now
    Exit Function
Erro:
    TrataErroGeral "Processa Pagamento Sinistro"
    TerminaSEGBR
End Function

'JOAO.MACHADO - IPREM - 26-09-2013
Private Function Monta_query_Assistencia() As String

    On Error GoTo Error
    Dim SQL As String

    SQL = ""
    'Sql = Sql & " EXEC SEGS11547_SPS "

    '            Sql = Sql & " SELECT DISTINCT " & vbNewLine
    '            Sql = Sql & "    a.movimentacao_id," & vbNewLine
    '            Sql = Sql & "    a.dt_movimentacao," & vbNewLine
    '            Sql = Sql & "    b.tp_operacao," & vbNewLine
    '            Sql = Sql & "    b.val_movimentacao," & vbNewLine
    '            Sql = Sql & "    b.retido_banco," & vbNewLine
    '            Sql = Sql & "    b.cliente_id," & vbNewLine
    '            Sql = Sql & "    c.proposta_id," & vbNewLine
    '            Sql = Sql & "    c.endosso_id," & vbNewLine
    '            Sql = Sql & "    d.produto_id," & vbNewLine
    '            Sql = Sql & "    isnull(a.flag_aprovacao,' ') flag_aprovacao  ," & vbNewLine
    '            Sql = Sql & "    'N' as restituicao_unica," & vbNewLine
    '            Sql = Sql & "    'N' as restituicao_documento_gru_mapa" & vbNewLine
    '            Sql = Sql & "  FROM ps_movimentacao_tb a" & vbNewLine
    '            Sql = Sql & "  INNER JOIN ps_mov_cliente_tb b" & vbNewLine
    '            Sql = Sql & "          ON a.movimentacao_id = b.movimentacao_id" & vbNewLine
    '            Sql = Sql & "  INNER JOIN ps_mov_endosso_financeiro_tb c" & vbNewLine
    '            Sql = Sql & "          ON a.movimentacao_id = c.movimentacao_id" & vbNewLine
    '            Sql = Sql & "  INNER JOIN proposta_tb d" & vbNewLine
    '            Sql = Sql & "          ON c.proposta_id = d.proposta_id" & vbNewLine
    '            Sql = Sql & "   LEFT JOIN subramo_tb f" & vbNewLine
    '            Sql = Sql & "          ON f.ramo_id    = d.ramo_id" & vbNewLine
    '            Sql = Sql & "         AND f.subramo_id = d.subramo_id" & vbNewLine
    '            Sql = Sql & "  INNER JOIN endosso_financeiro_tb g" & vbNewLine
    '            Sql = Sql & "          ON g.proposta_id = c.proposta_id" & vbNewLine
    '            Sql = Sql & "         AND g.endosso_id = c.endosso_id" & vbNewLine
    '            Sql = Sql & "   LEFT JOIN endosso_resumo_tb er" & vbNewLine
    '            Sql = Sql & "          ON er.endosso_resumo_id = g.endosso_resumo_id" & vbNewLine
    '            Sql = Sql & "       Where b.acerto_id Is Null" & vbNewLine
    '            Sql = Sql & "         AND er.endosso_resumo_id is null" & vbNewLine
    '            Sql = Sql & "         AND b.flag_cancelamento is null" & vbNewLine
    '            Sql = Sql & "         AND (a.flag_aprovacao is null or a.flag_aprovacao = 's')" & vbNewLine
    '            Sql = Sql & "         AND er.endosso_resumo_id IS NULL" & vbNewLine
    '            Sql = Sql & "         AND (" & vbNewLine
    '            Sql = Sql & "              ISNULL(f.restituicao_unica,'N') = 'N'" & vbNewLine
    '            Sql = Sql & "               OR" & vbNewLine
    '            Sql = Sql & "                  (" & vbNewLine
    '            Sql = Sql & "                  ISNULL(f.restituicao_unica,'N') = 'S'" & vbNewLine
    '            Sql = Sql & "                  AND g.val_financeiro > 0" & vbNewLine
    '            Sql = Sql & "                 )" & vbNewLine
    '            Sql = Sql & "            )" & vbNewLine
    '            Sql = Sql & "         AND ISNULL(a.situacao,'') <> 'T'" & vbNewLine
    '            Sql = Sql & "         AND b.aceite_bb IS NULL" & vbNewLine
    '
    '            Sql = Sql & "   Union" & vbNewLine
    SQL = SQL & "          SELECT a.movimentacao_id," & vbNewLine
    SQL = SQL & "                 a.dt_movimentacao," & vbNewLine
    SQL = SQL & "                 b.tp_operacao," & vbNewLine
    SQL = SQL & "                 sum(b.val_movimentacao) as val_movimentacao," & vbNewLine
    SQL = SQL & "                 b.retido_banco," & vbNewLine
    SQL = SQL & "                 d.prop_cliente_id," & vbNewLine
    SQL = SQL & "                 c.proposta_id," & vbNewLine
    SQL = SQL & "                 er.endosso_id," & vbNewLine
    SQL = SQL & "                 d.produto_id," & vbNewLine
    SQL = SQL & "                 isnull(a.flag_aprovacao,' ') flag_aprovacao ," & vbNewLine
    SQL = SQL & "                 f.restituicao_unica," & vbNewLine
    SQL = SQL & "                'N' as restituicao_documento_gru_mapa" & vbNewLine
    SQL = SQL & "            FROM ps_movimentacao_tb a" & vbNewLine
    SQL = SQL & "      INNER JOIN ps_mov_assistencia_tb b" & vbNewLine
    SQL = SQL & "              ON a.movimentacao_id = b.movimentacao_id" & vbNewLine
    SQL = SQL & "      INNER JOIN ps_mov_endosso_financeiro_tb c" & vbNewLine
    SQL = SQL & "              ON a.movimentacao_id = c.movimentacao_id" & vbNewLine
    SQL = SQL & "      INNER JOIN endosso_financeiro_tb e" & vbNewLine
    SQL = SQL & "              ON e.proposta_id = c.proposta_id" & vbNewLine
    SQL = SQL & "             AND e.endosso_id  = c.endosso_id" & vbNewLine
    SQL = SQL & "      LEFT JOIN endosso_resumo_tb er" & vbNewLine
    SQL = SQL & "              ON er.endosso_resumo_id = e.endosso_resumo_id" & vbNewLine
    SQL = SQL & "      INNER JOIN proposta_tb d" & vbNewLine
    SQL = SQL & "              ON c.proposta_id = d.proposta_id" & vbNewLine
    SQL = SQL & "      INNER JOIN subramo_tb f" & vbNewLine
    SQL = SQL & "              ON f.ramo_id    = d.ramo_id" & vbNewLine
    SQL = SQL & "             AND f.subramo_id = d.subramo_id" & vbNewLine
    SQL = SQL & "             AND dt_fim_vigencia_sbr IS NULL" & vbNewLine
    SQL = SQL & "           Where b.acerto_id Is Null" & vbNewLine
    SQL = SQL & "            AND (a.flag_aprovacao is null or a.flag_aprovacao = 's')" & vbNewLine
    SQL = SQL & "             AND ISNULL(a.situacao,'') <> 'T'" & vbNewLine
    SQL = SQL & "        GROUP BY a.movimentacao_id," & vbNewLine
    SQL = SQL & "                 a.dt_movimentacao, b.tp_operacao," & vbNewLine
    SQL = SQL & "                 b.retido_banco," & vbNewLine
    SQL = SQL & "                 d.prop_cliente_id," & vbNewLine
    SQL = SQL & "                 c.proposta_id," & vbNewLine
    SQL = SQL & "                 er.endosso_id," & vbNewLine
    SQL = SQL & "                 d.produto_id," & vbNewLine
    SQL = SQL & "                isnull(a.flag_aprovacao,' ') ," & vbNewLine
    SQL = SQL & "                 F.restituicao_unica" & vbNewLine

    Monta_query_Assistencia = SQL
    Exit Function
Error:
    Call TrataErroGeral("Monta_query_Assistencia", Me.name)
    Call TerminaSEGBR
End Function

'JOAO.MACHADO - IPREM - 26-09-2013
Private Sub Processa_Pgto_Assistencia()
    Dim situacao As String
    Dim Descricao As String
    Dim flagprimeiravez As Boolean
    Dim Proposta_ant As Variant
    Dim proposta_id As Variant
    Dim proposta_basica As Variant
    Dim Produto_ant As Variant
    Dim flag_aprovacao_ant As Variant
    Dim endosso_id As Variant
    Dim Tp_Endosso_Ant As Variant
    Dim tp_endosso_id As Variant
    Dim bAglutinaPagamentos As Boolean

    On Error GoTo Erro

    SQL = Monta_query_Assistencia
    Set rc_geral = rdocn1.OpenResultset(SQL, rdOpenStatic)

    If Not rc_geral.EOF Then
        Conta_lidas = 0
    Else
        rdocn.RollbackTrans
        txtfim = Now
        MensagemBatch "Nenhum pagamento de assist�ncia selecionado!"
        Call goProducao.AdicionaLog(1, "Assistencias", 4)
        Call goProducao.AdicionaLog(2, Conta_Processadas, 4)

        'grava log arquivo CTM
        If CTM = True Then
            Call GravaLogCTM("OK - ", "Pagamento de Assist�ncias", "", "")
            Call GravaLogCTM("OK - ", Conta_Processadas, "", "")
        End If

        rc_geral.Close
        Flag_Pgto_ok = True
        Exit Sub
    End If

    flagprimeiravez = True
    Retido_Banco_ant = ""
    Cliente_ant = ""
    tp_movimentacao = "13"
    ProgressBar1.Value = 0
    Total_mov = 0
    Proposta_ant = Null
    Tp_Endosso_Ant = Null
    Produto_ant = ""
    flag_aprovacao_ant = ""

    While Not rc_geral.EOF
        Conta_lidas = Conta_lidas + 1
        ProgressBar1.Value = Conta_lidas

        proposta_id = rc_geral!proposta_id
        endosso_id = rc_geral!endosso_id
        proposta_basica = ""

        '        tp_endosso_id = ObterTipoEndosso(CLng(proposta_id), CLng(endosso_id))

        If UCase(rc_geral!flag_aprovacao) = "S" Then
            Obtem_Proposta_Basica proposta_id, proposta_basica


            If Val(proposta_basica & "") <> 0 Then
                proposta_id = proposta_basica
            End If
        End If

        If (flagprimeiravez = False And rc_geral!Produto_id = 1198) And (rc_geral!Produto_id = Produto_ant) Then
            bAglutinaPagamentos = False
        Else
            bAglutinaPagamentos = True
        End If

        If (rc_geral!retido_banco <> Retido_Banco_ant _
            Or rc_geral!prop_cliente_id <> Cliente_ant _
            Or proposta_id <> Proposta_ant _
            Or rc_geral!Produto_id <> Produto_ant _
            Or rc_geral!flag_aprovacao <> flag_aprovacao_ant) _
            And (bAglutinaPagamentos = True) Then    'mathayde

            If Not flagprimeiravez Then
                If Total_mov >= 0 Then

                    If UCase(Retido_Banco_ant) = "S" Or (UCase(flag_aprovacao_ant) = "S" And (Tp_Endosso_Ant = 63 Or Tp_Endosso_Ant = 70 Or Tp_Endosso_Ant = 80 Or Tp_Endosso_Ant = 81 Or Tp_Endosso_Ant = 101 Or Tp_Endosso_Ant = 104 Or Tp_Endosso_Ant = 270 Or ((Produto_ant = 114 Or Produto_ant = 1141 Or Produto_ant = 1125) And Tp_Endosso_Ant = 200) Or (Produto_ant = 1152 And (Tp_Endosso_Ant = 64 Or Tp_Endosso_Ant = 269)))) Then
                        situacao_aux = "a"
                    Else
                        situacao_aux = "p"
                    End If

                    Atualiza_Pagamento "AS", tp_movimentacao, 0, " ", Cliente_ant, " ", 0, 0, 0, Data_Pagamento, Total_mov, Total_mov, Proposta_ant, situacao_aux, 0
                    Conta_Processadas = Conta_Processadas + 1
                    rdocn.CommitTrans
                Else
                    rdocn.RollbackTrans
                End If

            Else
                flagprimeiravez = False
            End If

            Retido_Banco_ant = rc_geral!retido_banco
            Cliente_ant = rc_geral!prop_cliente_id
            Proposta_ant = proposta_id
            Tp_Endosso_Ant = tp_endosso_id
            Produto_ant = rc_geral!Produto_id
            flag_aprovacao_ant = Trim(rc_geral!flag_aprovacao)
            rdocn.BeginTrans
            Insere_Pagamento "AS"
            Total_mov = 0
        End If

        If UCase(rc_geral!tp_operacao) = "C" Then
            Total_mov = Total_mov + Val(rc_geral!val_movimentacao)
        Else
            Total_mov = Total_mov - Val(rc_geral!val_movimentacao)
        End If

        Call Atualiza_Movimento(rc_geral!movimentacao_id)

        rc_geral.MoveNext
    Wend

    rc_geral.Close

    If Total_mov >= 0 Then

        If UCase(Retido_Banco_ant) = "S" Or (UCase(flag_aprovacao_ant) = "S" And (Tp_Endosso_Ant = 63 Or Tp_Endosso_Ant = 70 Or Tp_Endosso_Ant = 80 Or Tp_Endosso_Ant = 81 Or Tp_Endosso_Ant = 101 Or Tp_Endosso_Ant = 104 Or Tp_Endosso_Ant = 270 Or ((Produto_ant = 114 Or Produto_ant = 1141 Or Produto_ant = 1125) And Tp_Endosso_Ant = 200) Or (Produto_ant = 1152 And (Tp_Endosso_Ant = 64 Or Tp_Endosso_Ant = 269)))) Then
            situacao_aux = "a"
        Else
            situacao_aux = "p"
        End If


        Atualiza_Pagamento "AS", tp_movimentacao, 0, " ", Cliente_ant, " ", 0, 0, 0, Data_Pagamento, Total_mov, Total_mov, Proposta_ant, situacao_aux, 0
        Conta_Processadas = Conta_Processadas + 1
        rdocn.CommitTrans
    Else
        rdocn.RollbackTrans
    End If



    gridResumo.AddItem "Assist�ncia" & vbTab & Str(Conta_Processadas)
    Call goProducao.AdicionaLog(1, "Cliente", 4)
    Call goProducao.AdicionaLog(2, Conta_Processadas, 4)

    'grava log arquivo CTM
    If CTM = True Then
        Call GravaLogCTM("OK - ", "Cliente", "", "")
        Call GravaLogCTM("OK - ", Conta_Processadas, "", "")
    End If

    Flag_Pgto_ok = True
    txtfim = Now
    Exit Sub
Erro:
    TrataErroGeral "Processa Pagamento Assit�ncia"
    TerminaSEGBR
End Sub


