Attribute VB_Name = "Local"
Option Explicit

Global gsSiglaSistema As String
Global glAmbienteId As Long
Global gsSiglaRecurso As String
Global gsDescricaoRecurso As String

Global Const NOMECOMP = "SEGL0284"

Function TrocaVirgulaPorPonto(ByVal ValorAux As Variant) As String
    
    If InStr(ValorAux, ",") = 0 Then
        TrocaVirgulaPorPonto = ValorAux
    Else
        ValorAux = Mid(ValorAux, 1, InStr(ValorAux, ",") - 1) + "." + Mid(ValorAux, InStr(ValorAux, ",") + 1)
        TrocaVirgulaPorPonto = ValorAux
    End If

End Function

Public Sub adSQL(ByRef sSQL As String, ByVal sCadeia As String)
    'Jorfilho 04/11/2002 - Concatena a query com vbNewLine
    sSQL = sSQL & sCadeia & vbNewLine
End Sub

Function MudaVirgulaParaPonto(ByVal valor As String) As String

    If InStr(valor, ",") = 0 Then
        MudaVirgulaParaPonto = valor
    Else
        valor = Mid$(valor, 1, InStr(valor, ",") - 1) + "." + Mid$(valor, InStr(valor, ",") + 1)
        MudaVirgulaParaPonto = valor
    End If

End Function

