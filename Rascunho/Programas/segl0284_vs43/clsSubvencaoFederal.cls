VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsSubvencaoFederal"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Function ValidarImportacaoArquivoROA(ByVal sSiglaSistema As String, _
                                     ByVal sSiglaRecurso As String, _
                                     ByVal sDescricaoRecurso As String, _
                                     ByVal iAmbienteId As Integer, _
                                     ByVal sArquivo As String, _
                                     ByVal sNum_relatorio As String, _
                                     ByVal sNum_lote As String) As Integer

On Error GoTo Trata_Erro

'RSilva - 01/06/2010
Dim sSQL As String
Dim rs As Recordset

sSQL = ""
sSQL = "EXEC seguros_db..SEGS8674_SPS "

If Trim(sArquivo) = "" Then
    sSQL = sSQL & "NULL"
Else
    sSQL = sSQL & "NULL"
    'sSQL = sSQL & "'" & sArquivo & "'"
End If

If Trim(sNum_relatorio) = "" Then
    sSQL = sSQL & ",NULL"
Else
    sSQL = sSQL & ",'" & sNum_relatorio & "'"
End If

If Trim(sNum_lote) = "" Then
    sSQL = sSQL & ",NULL"
Else
    sSQL = sSQL & ",'" & sNum_lote & "'"
End If


Set rs = ExecutarSQL(sSiglaSistema, _
                     iAmbienteId, _
                     sSiglaRecurso, _
                     sDescricaoRecurso, _
                     sSQL, _
                     True)

If Not rs.EOF Then
    ValidarImportacaoArquivoROA = 1 'rs(0)
Else
    ValidarImportacaoArquivoROA = 0
End If
rs.Close

Exit Function
Trata_Erro:
    Call Err.Raise(Err.Number, , "SEGL0143.clsSubvencaoFederal.ValidarImportacaoArquivoROA - " & Err.Description)
End Function

Private Sub CriarTemporaria(ByVal sSiglaSistema As String, _
                           ByVal sSiglaRecurso As String, _
                           ByVal sDescricaoRecurso As String, _
                           ByVal iAmbienteId As Integer, _
                           ByRef lconexao As Long)

On Error GoTo Trata_Erro
Dim sSQL As String

'    sSql = ""
'    adSQL sSql, "    CREATE TABLE #arquivo_tmpo ( seq_linha INT IDENTITY(1,1)"
'    adSQL sSql, "                              , linha     VARCHAR(8000) NULL)"
'
'    Call Conexao_ExecutarSQL(sSiglaSistema, _
'                             iAmbienteId, _
'                             sSiglaRecurso, _
'                             sDescricaoRecurso, _
'                             sSql, _
'                             lConexao, _
'                             False, _
'                             False, _
'                             30000, _
'                             adLockOptimistic, _
'                             adUseClient)

    sSQL = ""
    adSQL sSQL, "CREATE TABLE #repasse_subvencao_tb ("
    adSQL sSQL, "              repasse_subvencao_id      INT IDENTITY(1,1),"
    adSQL sSQL, "              arquivo                   VARCHAR(60),"
    adSQL sSQL, "              val_subvencao             NUMERIC(15,2),"
    adSQL sSQL, "              num_relatorio             NUMERIC(15),"
    adSQL sSQL, "              dt_pagamento              SMALLDATETIME,"
    adSQL sSQL, "              num_lote_aprovacao        VARCHAR(100), " 'NUMERIC(15),"
    adSQL sSQL, "              val_premio                NUMERIC(15,2),"
    adSQL sSQL, "              val_custo_emissao         NUMERIC(15,2),"
    adSQL sSQL, "              situacao                  CHAR(1)"
    adSQL sSQL, "              )"
    
    Call Conexao_ExecutarSQL(sSiglaSistema, _
                             iAmbienteId, _
                             sSiglaRecurso, _
                             sDescricaoRecurso, _
                             sSQL, _
                             lconexao, _
                             False, _
                             False, _
                             30000, _
                             adLockOptimistic, _
                             adUseClient)


    sSQL = ""
    adSQL sSQL, "CREATE TABLE #repasse_subvencao_documento_tb ("
    adSQL sSQL, "  repasse_subvencao_documento_id  INT IDENTITY(1,1),"
    adSQL sSQL, "  proposta_id                     NUMERIC(9,0),"
    adSQL sSQL, "  repasse_subvencao_id            INT,"
    adSQL sSQL, "  seguradora_id                 INT,"
    adSQL sSQL, "  dt_proposta                   SMALLDATETIME,"
    adSQL sSQL, "  endosso_id                    INT,"
    adSQL sSQL, "  cpf_cnpj_segurado             VARCHAR(14),"
    adSQL sSQL, "  nome_segurado                 VARCHAR(60),"
    adSQL sSQL, "  val_subvencao                 NUMERIC(15,2),"
    adSQL sSQL, "  val_premio_total              NUMERIC(15,2),"
    adSQL sSQL, "  val_custo_emissao             NUMERIC(15,2),"
    adSQL sSQL, "  cod_bacen_cultura             INT ,"
    adSQL sSQL, "  cod_municipio_bacen               NUMERIC(10),"
    adSQL sSQL, "  zaneamento_agricola               CHAR(1),"
    adSQL sSQL, "  razao_social_seguradora           VARCHAR(60),"
    adSQL sSQL, "  cnpj_seguradora                   VARCHAR(14),"
    adSQL sSQL, "  dt_apolice                    SMALLDATETIME,"
    adSQL sSQL, "  apolice_id                    NUMERIC(6),"
    adSQL sSQL, "  nome_propriedade_rural            VARCHAR(60),"
    adSQL sSQL, "  logradouro_propriedade_rural      VARCHAR(60),"
    adSQL sSQL, "  num_propriedade_rural             NUMERIC(6) ,"
    adSQL sSQL, "  complemento_endereco_propriedade_rural    VARCHAR(20),"
    adSQL sSQL, "  municipio_propriedade_rural           VARCHAR(60),"
    adSQL sSQL, "  uf_propriedade_rural              CHAR(2),"
    adSQL sSQL, "  cep_propriedade_rural             VARCHAR(8),"
    adSQL sSQL, "  logradouro_segurado               VARCHAR(60),"
    adSQL sSQL, "  num_endereco_segurado             NUMERIC(6),"
    adSQL sSQL, "  complemento_endereco_segurado     VARCHAR(20),"
    adSQL sSQL, "  municipio_segurado                VARCHAR(60),"
    adSQL sSQL, "  uf_segurado                   CHAR(2),"
    adSQL sSQL, "  cep_segurado                  VARCHAR(8),"
    adSQL sSQL, "  qtd_produtividade_estimada            NUMERIC(15,2),"
    adSQL sSQL, "  qtd_area_segurada             NUMERIC(15,2),"
    adSQL sSQL, "  qtd_produtividade_segurada            NUMERIC(15,2),"
    adSQL sSQL, "  val_is                        NUMERIC(15,2),"
    adSQL sSQL, "  num_proc_susep                    VARCHAR(25),"
    adSQL sSQL, "  cod_justificativa             INT,"
    adSQL sSQL, "  dt_baixa                      DATETIME ,"
    adSQL sSQL, "  situacao                      CHAR(1)"
    adSQL sSQL, "  )"


    Call Conexao_ExecutarSQL(sSiglaSistema, _
                             iAmbienteId, _
                             sSiglaRecurso, _
                             sDescricaoRecurso, _
                             sSQL, _
                             lconexao, _
                             False, _
                             False, _
                             30000, _
                             adLockOptimistic, _
                             adUseClient)



Exit Sub
Trata_Erro:
    Call Err.Raise(Err.Number, , "SEGL0143.clsSubvencaoFederal.CriarTemporaria - " & Err.Description)
End Sub


Public Function ProcessaImportacao(ByVal sSiglaSistema As String, _
                                   ByVal sSiglaRecurso As String, _
                                   ByVal sDescricaoRecurso As String, _
                                   ByVal iAmbienteId As Integer, _
                                   ByVal sUsuario As String, _
                                   ByVal sDtSistema As String, _
                                   ByVal sCaminho As String, _
                                   ByVal lNumRelatorio As Long, _
                                   ByVal sDtPgto As String, _
                                   ByVal lNumLote As String) As Object

Dim lconexao As Long
Dim sMsgErro As String
Dim ErrNumero As Long
Dim ErrDescricao As String

Dim lTotalRegistro As Long
Dim lTotalSubvencao As Currency

Dim sSQL As String


On Error GoTo Trata_Erro

    sMsgErro = ""

    'Obtendo Conex�o ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    lconexao = AbrirConexao(sSiglaSistema, iAmbienteId, sDescricaoRecurso, sSiglaRecurso)

    Call AbrirTransacao(lconexao)

    'Criando as temporarias
    Call CriarTemporaria(sSiglaSistema, _
                         sSiglaRecurso, _
                         sDescricaoRecurso, _
                         iAmbienteId, _
                         lconexao)

    'Importar Planilha para tratamento
    If (ImportaPlanilhaCSV(sCaminho, lNumRelatorio, sDtPgto, lNumLote, _
                            sSiglaSistema, _
                            sSiglaRecurso, _
                            sDescricaoRecurso, _
                            iAmbienteId, _
                            lconexao, lTotalRegistro, lTotalSubvencao)) = True Then
    
        'Tratamento
        
        
        
        'Inclus�o
        If MsgBox("O Arquivo selectionado para a importacao cont�m um total de " & lTotalRegistro & " ap�lices e o valor total de R$ " & lTotalSubvencao & _
                   vbNewLine & "Confirma a importa��o deste arquivo?", vbYesNo, "Confirma��o de Importa��o") = vbYes Then
                Call GravarDadosImportacaoArquivoROA(sSiglaSistema, _
                                                    sSiglaRecurso, _
                                                    sDescricaoRecurso, _
                                                    iAmbienteId, _
                                                    lconexao, sUsuario)
                                                    
                Call ConfirmarTransacao(lconexao)
                
                
                MsgBox "Arquivo Importado com sucesso", vbInformation, sSiglaSistema
                
        Else
            Call RetornarTransacao(lconexao)
        End If
    Else
        Call RetornarTransacao(lconexao)
    End If



Exit Function
Resume
  
Trata_Erro:
  ErrNumero = Err.Number
  ErrDescricao = Err.Description
  
  Call RetornarTransacao(lconexao)
  Call Err.Raise(ErrNumero, , "SEGL0284.clsSubvencaoFederal.ProcessaImportacao - " & ErrDescricao)
  
End Function

Sub GravarDadosImportacaoArquivoROA(ByVal sSiglaSistema As String, _
                                    ByVal sSiglaRecurso As String, _
                                    ByVal sDescricaoRecurso As String, _
                                    ByVal iAmbienteId As Integer, _
                                    ByRef lconexao As Long, _
                                    ByVal sUsuario As String)

On Error GoTo Trata_Erro

Dim sSQL As String
Dim rs As Recordset
Dim repasse_subvencao_id As Integer

sSQL = ""
adSQL sSQL, "SET NOCOUNT ON"
adSQL sSQL, "EXEC seguros_db.dbo.SEGS8675_SPI '" & sUsuario & "'"

Set rs = Conexao_ExecutarSQL(sSiglaSistema, _
                             iAmbienteId, _
                             sSiglaRecurso, _
                             sDescricaoRecurso, _
                             sSQL, _
                             lconexao, _
                             True, _
                             False)
                             


If Not rs.EOF Then
    repasse_subvencao_id = rs(0)
    sSQL = "SET NOCOUNT ON"
    sSQL = "EXEC seguros_db.dbo.SEGS8676_SPI " & repasse_subvencao_id & ",'" & sUsuario & "'"
    
    Call Conexao_ExecutarSQL(sSiglaSistema, _
                             iAmbienteId, _
                             sSiglaRecurso, _
                             sDescricaoRecurso, _
                             sSQL, _
                             lconexao, _
                             False, _
                             False, _
                             30000, _
                             adLockOptimistic, _
                             adUseClient)

End If
rs.Close

Exit Sub
Resume
  
Trata_Erro:
  ErrNumero = Err.Number
  ErrDescricao = Err.Description
  
  Call RetornarTransacao(lconexao)
  Call Err.Raise(ErrNumero, , "SEGL0284.clsSubvencaoFederal.GravarDadosImportacaoArquivoROA - " & ErrDescricao)


End Sub

Function ImportaPlanilhaCSV(ByVal sCaminho As String, _
                            ByVal lNumRelatorio As String, _
                            ByVal sDtPgto As String, _
                            ByVal lNumLote As String, _
                            ByVal sSiglaSistema As String, _
                            ByVal sSiglaRecurso As String, _
                            ByVal sDescricaoRecurso As String, _
                            ByVal iAmbienteId As Integer, _
                            ByRef lconexao As Long, _
                            ByRef lTotRegistro As Long, _
                            ByRef lTotSubvencao As Currency) As Boolean
On Error GoTo Trata_Erro

Dim sSQL As String
Dim sLinha As String
Dim linha() As String

Dim iFlag As Integer

'Variaveis Total
Dim lTotalRegistro As Long
Dim lTotalSubvencao As Currency
Dim lTotalValPremio As Currency
Dim lTotalCustoEmissao As Currency


'Variaveis para tratamento dos arquivos
Dim oSistemaArquivo As New FileSystemObject
Dim arquivo As TextStream
Dim DadosArquivo As File

'Obter dados do arquivo
Set DadosArquivo = oSistemaArquivo.GetFile(sCaminho)

ImportaPlanilhaCSV = False

If ValidarImportacaoArquivoROA(sSiglaSistema, sSiglaRecurso, sDescricaoRecurso, iAmbienteId, _
                               DadosArquivo.Name, lNumRelatorio, lNumLote) > 0 Then
    'MsgBox "O arquivo " & DadosArquivo.Name & " j� foi importado", vbInformation
    MsgBox "Relat�rio: " & lNumRelatorio & vbNewLine & "N�mero do lote: " & lNumLote & vbNewLine & " J� foi importado.", vbInformation
    Exit Function
End If
    
    iFlag = 0
    
    'Obter texto do arquivo
    Set arquivo = oSistemaArquivo.OpenTextFile(sCaminho, 1, False, -2)
    
    Do While arquivo.AtEndOfStream <> True
        DoEvents
        sLinha = arquivo.ReadLine
        linha() = Split(sLinha, ";")
        
        If iFlag > 0 Then
        
            lTotalRegistro = lTotalRegistro + 1
            lTotalSubvencao = lTotalSubvencao + linha(5)
            lTotalValPremio = lTotalValPremio + linha(6)
            lTotalCustoEmissao = lTotalCustoEmissao + linha(7)
   
   
            sSQL = ""
            adSQL sSQL, "INSERT INTO #repasse_subvencao_documento_tb ("
            adSQL sSQL, "  proposta_id"
            adSQL sSQL, ", seguradora_id"
            adSQL sSQL, ", dt_proposta"
            adSQL sSQL, ", endosso_id "
            adSQL sSQL, ", cpf_cnpj_segurado"
            adSQL sSQL, ", nome_segurado"
            adSQL sSQL, ", val_subvencao"
            adSQL sSQL, ", val_premio_total  "
            adSQL sSQL, ", val_custo_emissao  "
            adSQL sSQL, ", cod_bacen_cultura  "
            adSQL sSQL, ", cod_municipio_bacen  "
            adSQL sSQL, ", zaneamento_agricola  "
            adSQL sSQL, ", razao_social_seguradora "
            adSQL sSQL, ", cnpj_seguradora  "
            adSQL sSQL, ", dt_apolice   "
            adSQL sSQL, ", apolice_id   "
            adSQL sSQL, ", nome_propriedade_rural "
            adSQL sSQL, ", logradouro_propriedade_rural "
            adSQL sSQL, ", num_propriedade_rural  "
            adSQL sSQL, ", complemento_endereco_propriedade_rural"
            adSQL sSQL, ", municipio_propriedade_rural  "
            adSQL sSQL, ", uf_propriedade_rural   "
            adSQL sSQL, ", cep_propriedade_rural   "
            adSQL sSQL, ", logradouro_segurado   "
            adSQL sSQL, ", num_endereco_segurado   "
            adSQL sSQL, ", complemento_endereco_segurado  "
            adSQL sSQL, ", municipio_segurado   "
            adSQL sSQL, ", uf_segurado    "
            adSQL sSQL, ", cep_segurado    "
            adSQL sSQL, ", qtd_produtividade_estimada  "
            adSQL sSQL, ", qtd_area_segurada   "
            adSQL sSQL, ", qtd_produtividade_segurada  "
            adSQL sSQL, ", val_is    "
            adSQL sSQL, ", num_proc_susep   "
            adSQL sSQL, ", cod_justificativa   "
            adSQL sSQL, ", situacao    )"
        
            adSQL sSQL, "VALUES ("
            adSQL sSQL, " '" & Trim(linha(2)) & "'"   'proposta_id"
            adSQL sSQL, ",'" & Trim(linha(0)) & "'"   'seguradora_id"
            adSQL sSQL, ",'" & Format(Trim(linha(1)), "yyyymmdd") & "'"  'dt_proposta"
            adSQL sSQL, ",'" & Trim(linha(32)) & "'"  'endosso_id "
            adSQL sSQL, ",'" & Trim(linha(3)) & "'"   'cpf_cnpj_segurado"
            adSQL sSQL, ",'" & Trim(linha(4)) & "'"   'nome_segurado"
            adSQL sSQL, "," & MudaVirgulaParaPonto(Replace(Trim(linha(5)), ".", "")) 'val_subvencao"
            adSQL sSQL, "," & MudaVirgulaParaPonto(Replace(Trim(linha(6)), ".", "")) 'val_premio_total  "
            adSQL sSQL, "," & MudaVirgulaParaPonto(Replace(Trim(linha(7)), ".", "")) 'val_custo_emissao  "
            adSQL sSQL, ",'" & Trim(linha(8)) & "'"   'cod_bacen_cultura  "
            adSQL sSQL, ",'" & Trim(linha(9)) & "'"   'cod_municipio_bacen  "
            adSQL sSQL, ",'" & UCase(Mid(Trim(linha(10)), 1, 1)) & "'" 'zoneamento_agricola  "
            adSQL sSQL, ",'" & Trim(linha(11)) & "'"  'razao_social_seguradora "
            adSQL sSQL, ",'" & Trim(linha(12)) & "'"  'cnpj_seguradora  "
            adSQL sSQL, ",'" & Format(Trim(linha(13)), "yyyymmdd") & "'" 'dt_apolice   "
            adSQL sSQL, ", " & Trim(linha(14))        'apolice_id   "
            adSQL sSQL, ",'" & Trim(linha(15)) & "'"  'nome_propriedade_rural "
            adSQL sSQL, ",'" & Trim(linha(16)) & "'"  'logradouro_propriedade_rural "
            adSQL sSQL, ", " & Val(Trim(linha(17)))   'num_propriedade_rural  "
            adSQL sSQL, ",'" & Trim(linha(18)) & "'"  'complemento_endereco_propriedade_rural"
            adSQL sSQL, ",'" & Trim(linha(19)) & "'"  'municipio_propriedade_rural  "
            adSQL sSQL, ",'" & Trim(linha(20)) & "'"  'uf_propriedade_rural   "
            adSQL sSQL, ",'" & Trim(linha(21)) & "'"  'cep_propriedade_rural   "
            adSQL sSQL, ",'" & Trim(linha(22)) & "'"  'logradouro_segurado   "
            adSQL sSQL, ", " & Val(Trim(linha(23)))   'num_endereco_segurado   "
            adSQL sSQL, ",'" & Trim(linha(24)) & "'"  'complemento_endereco_segurado  "
            adSQL sSQL, ",'" & Trim(linha(25)) & "'"  'municipio_segurado   "
            adSQL sSQL, ",'" & Trim(linha(26)) & "'"  'uf_segurado    "
            adSQL sSQL, ",'" & Trim(linha(27)) & "'"  'cep_segurado    "
            adSQL sSQL, "," & MudaVirgulaParaPonto(Replace(Trim(linha(28)), ".", ""))  'qtd_produtividade_estimada
            adSQL sSQL, "," & MudaVirgulaParaPonto(Replace(Trim(linha(29)), ".", ""))  'qtd_area_segurada
            adSQL sSQL, "," & MudaVirgulaParaPonto(Replace(Trim(linha(30)), ".", ""))  'qtd_produtividade_segurada
            adSQL sSQL, "," & MudaVirgulaParaPonto(Replace(Trim(linha(31)), ".", ""))  'val_is    "
            adSQL sSQL, ",'" & Trim(linha(33)) & "'"  'num_proc_susep   "
            adSQL sSQL, ",'" & Trim(linha(34)) & "'"  'cod_justificativa   "
            adSQL sSQL, ",'P')"                 'situacao"
            
          Call Conexao_ExecutarSQL(sSiglaSistema, _
                                   iAmbienteId, _
                                   sSiglaRecurso, _
                                   sDescricaoRecurso, _
                                   sSQL, _
                                   lconexao, _
                                   False, _
                                   False, _
                                   30000, _
                                   adLockOptimistic, _
                                   adUseClient)
        
        End If
        iFlag = iFlag + 1
    Loop
    
    
   sSQL = ""
   adSQL sSQL, "INSERT INTO #repasse_subvencao_tb ("
   adSQL sSQL, "            arquivo"
   adSQL sSQL, "          , val_subvencao"
   adSQL sSQL, "          , num_relatorio"
   adSQL sSQL, "          , dt_pagamento"
   adSQL sSQL, "          , num_lote_aprovacao"
   adSQL sSQL, "          , val_premio"
   adSQL sSQL, "          , val_custo_emissao"
   adSQL sSQL, "          , situacao)"
   adSQL sSQL, " VALUES ("
   adSQL sSQL, "           '" & DadosArquivo.Name & "'"
   adSQL sSQL, "          ,'" & MudaVirgulaParaPonto(lTotalSubvencao) & "'"
   adSQL sSQL, "          ,'" & lNumRelatorio & "'"
   adSQL sSQL, "          ,'" & Format(sDtPgto, "yyyymmdd") & "'"
   adSQL sSQL, "          ,'" & lNumLote & "'"
   adSQL sSQL, "          ,'" & MudaVirgulaParaPonto(lTotalValPremio) & "'"
   adSQL sSQL, "          ,'" & MudaVirgulaParaPonto(lTotalCustoEmissao) & "'"
   adSQL sSQL, "          ,'P'"
   adSQL sSQL, "          )"
   

   Call Conexao_ExecutarSQL(sSiglaSistema, _
                            iAmbienteId, _
                            sSiglaRecurso, _
                            sDescricaoRecurso, _
                            sSQL, _
                            lconexao, _
                            False, _
                            False, _
                            30000, _
                            adLockOptimistic, _
                            adUseClient)
    
    
    
    lTotRegistro = lTotalRegistro
    lTotSubvencao = lTotalSubvencao

    ImportaPlanilhaCSV = True

Exit Function
Resume
  
Trata_Erro:
  ErrNumero = Err.Number
  ErrDescricao = Err.Description
  
  Call RetornarTransacao(lconexao)
  Call Err.Raise(ErrNumero, , "SEGL0284.clsSubvencaoFederal.ImportaPlanilhaCSV - Problemas na importa��o do arquivo " & vbNewLine & ErrDescricao)

End Function

Function ConsultarLotes(ByVal sSiglaSistema As String, _
                        ByVal sSiglaRecurso As String, _
                        ByVal sDescricaoRecurso As String, _
                        ByVal iAmbienteId As Integer, _
                        ByVal sUsuario As String, _
                        ByVal sSituacao As String) As Object


On Error GoTo Trata_Erro

Dim sSQL As String
Dim rs As Recordset



sSQL = ""
sSQL = "SET NOCOUNT ON " & vbNewLine
sSQL = sSQL & "EXEC SEGS8674_SPS NULL, NULL, NULL, '" & sSituacao & "'"

Set ConsultarLotes = ExecutarSQL(sSiglaSistema, iAmbienteId, sSiglaRecurso, sDescricaoRecurso, sSQL, True)


Exit Function
Resume
Trata_Erro:
  
  ErrNumero = Err.Number
  ErrDescricao = Err.Description
  Call Err.Raise(ErrNumero, , "SEGL0284.clsSubvencaoFederal.ConsultarLotes - Problemas na importa��o do arquivo " & vbNewLine & ErrDescricao)



End Function


Function ConsultarDetalhesLote(ByVal sSiglaSistema As String, _
                               ByVal sSiglaRecurso As String, _
                               ByVal sDescricaoRecurso As String, _
                               ByVal iAmbienteId As Integer, _
                               ByVal iRepasseId As Integer) As Object
                               
On Error GoTo Trata_Erro

Dim sSQL As String

sSQL = ""
adSQL sSQL, "SET NOCOUNT ON"
adSQL sSQL, "EXEC seguros_db..SEGS8677_SPS " & iRepasseId

Set ConsultarDetalhesLote = ExecutarSQL(sSiglaSistema, iAmbienteId, sSiglaRecurso, sDescricaoRecurso, sSQL, True)



Exit Function
Resume
Trata_Erro:
  
  ErrNumero = Err.Number
  ErrDescricao = Err.Description
  Call Err.Raise(ErrNumero, , "SEGL0284.clsSubvencaoFederal.ConsultarDetalhesLote - Problemas na importa��o do arquivo " & vbNewLine & ErrDescricao)


End Function
