VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cls00460"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Public Function ObterRemessaCancelada(ByVal sSiglaSistema As String, _
                                      ByVal sSiglaRecurso As String, _
                                      ByVal sDescricaoRecurso As String, _
                                      ByVal lAmbienteID As Long)

Dim sSQL As String

On Error GoTo Trata_Erro

    sSQL = ""
    adSQL sSQL, " SELECT DISTINCT remessa_subvencao_cancelamento_tb.versao "
    adSQL sSQL, " FROM remessa_subvencao_cancelamento_tb "
    adSQL sSQL, " JOIN proposta_subvencao_tb "
    adSQL sSQL, "   ON remessa_subvencao_cancelamento_tb.remessa_subvencao_cancelamento_id = proposta_subvencao_tb.remessa_subvencao_cancelamento_id "
    adSQL sSQL, " WHERE proposta_subvencao_tb.num_documento_gru is null "
    adSQL sSQL, " ORDER BY versao "

    Set ObterRemessaCancelada = ExecutarSQL(sSiglaSistema, _
                                              lAmbienteID, _
                                              sSiglaRecurso, _
                                              sDescricaoRecurso, _
                                              sSQL, _
                                              True)

Exit Function

Trata_Erro:
    Call Err.Raise(Err.Number, , "SEGL0143.cls00128.ObterRemessaParaTroca - " & Err.Description)
End Function

Public Function ObterRemessaDocumentoNulo(ByVal sSiglaSistema As String, _
                                          ByVal sSiglaRecurso As String, _
                                          ByVal sDescricaoRecurso As String, _
                                          ByVal lAmbienteID As Long, _
                                          ByVal iRemessa As Integer)

Dim sSQL As String

On Error GoTo Trata_Erro

    sSQL = " "
    adSQL sSQL, " SELECT ps.proposta_id, ps.val_subvencao_informado "
    adSQL sSQL, "   FROM proposta_subvencao_tb ps "
    adSQL sSQL, " JOIN remessa_subvencao_cancelamento_tb rs"
    adSQL sSQL, "   ON rs.remessa_subvencao_cancelamento_id = ps.remessa_subvencao_cancelamento_id "
    adSQL sSQL, "  WHERE ps.num_documento_gru is null "
    adSQL sSQL, "    AND rs.versao = " & iRemessa

    Set ObterRemessaDocumentoNulo = ExecutarSQL(sSiglaSistema, _
                                                lAmbienteID, _
                                                sSiglaRecurso, _
                                                sDescricaoRecurso, _
                                                sSQL, _
                                                True)

Exit Function

Trata_Erro:
    Call Err.Raise(Err.Number, , "SEGL0143.cls00128.ObterRemessaDocumentoNulo - " & Err.Description)
End Function

Public Function ValidarExistenciaDocGRU(ByVal sSiglaSistema As String, _
                                          ByVal sSiglaRecurso As String, _
                                          ByVal sDescricaoRecurso As String, _
                                          ByVal lAmbienteID As Long, _
                                          ByVal NumDocumentoGRU As Currency)

Dim sSQL As String

On Error GoTo Trata_Erro

    sSQL = " "
    adSQL sSQL, " SELECT num_documento_gru "
    adSQL sSQL, "   FROM documento_gru_tb "
    adSQL sSQL, "  WHERE num_documento_gru = " & NumDocumentoGRU

    Set ValidarExistenciaDocGRU = ExecutarSQL(sSiglaSistema, _
                                                lAmbienteID, _
                                                sSiglaRecurso, _
                                                sDescricaoRecurso, _
                                                sSQL, _
                                                True)

Exit Function

Trata_Erro:
    Call Err.Raise(Err.Number, , "SEGL0143.cls00128.ValidarExistenciaDocGRU - " & Err.Description)
End Function

Public Function IncluirDocumentoGRU(ByVal sSiglaSistema As String, _
                                    ByVal sSiglaRecurso As String, _
                                    ByVal sDescricaoRecurso As String, _
                                    ByVal lAmbienteID As Long, _
                                    ByVal NumDocumentoGRU As Integer, _
                                    ByVal cValMulta As Currency, _
                                    ByVal cValJuros As Currency, _
                                    ByVal cValMora As Currency, _
                                    ByVal cValOutros As Currency, _
                                    ByVal cValTotal As Currency, _
                                    ByVal sUsuario As String, _
                                    ByRef lconexao As Long)
                                    

Dim sSQL As String

On Error GoTo Trata_Erro

  'Iniciando a transa��o '''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  lconexao = AbrirConexao(sSiglaSistema, lAmbienteID, sSiglaRecurso, sDescricaoRecurso)
  
  If Not AbrirTransacao(lconexao) Then
    Call Err.Raise(vbObjectError + 1000, , "SEGL0284.cls00460.IncluirDocumentoGRU - N�o foi poss�vel abrir transa��o")
  End If
  
    sSQL = " "
    adSQL sSQL, " EXEC SEGS6637_SPI "
    adSQL sSQL, NumDocumentoGRU & ", "
    adSQL sSQL, MudaVirgulaParaPonto(cValMulta) & ", "
    adSQL sSQL, MudaVirgulaParaPonto(cValJuros) & ", "
    adSQL sSQL, MudaVirgulaParaPonto(cValMora) & ", "
    adSQL sSQL, MudaVirgulaParaPonto(cValOutros) & ", "
    adSQL sSQL, MudaVirgulaParaPonto(cValTotal) & ", "
    adSQL sSQL, "'" & sUsuario & "'"

  Call Conexao_ExecutarSQL(sSiglaSistema, _
                           lAmbienteID, _
                           sSiglaRecurso, _
                           sDescricaoRecurso, _
                           sSQL, _
                           lconexao, _
                           False, _
                           False, _
                           30000, _
                           adLockOptimistic, _
                           adUseClient)

Exit Function

Trata_Erro:
    Call Err.Raise(Err.Number, , "SEGL0143.cls00128.IncluirDocumentoGRU - " & Err.Description)
End Function

Public Function AlterarPropostaDocumentoGRU(ByVal sSiglaSistema As String, _
                                            ByVal sSiglaRecurso As String, _
                                            ByVal sDescricaoRecurso As String, _
                                            ByVal lAmbienteID As Long, _
                                            ByVal NumDocumentoGRU As Integer, _
                                            ByRef lPropostas() As Long, _
                                            ByVal sUsuario As String, _
                                            ByVal lconexao As Long)
    
On Error GoTo Trata_Erro

Dim sSQL As String
Dim iCont As Integer
Dim i As Integer

iCont = UBound(lPropostas)

For i = 1 To iCont

    sSQL = " "
    adSQL sSQL, " EXEC SEGS6636_SPU "
    adSQL sSQL, NumDocumentoGRU & ", "
    adSQL sSQL, lPropostas(i) & ", "
    adSQL sSQL, "'" & sUsuario & "'"

Call Conexao_ExecutarSQL(sSiglaSistema, _
                           lAmbienteID, _
                           sSiglaRecurso, _
                           sDescricaoRecurso, _
                           sSQL, _
                           lconexao, _
                           False, _
                           False, _
                           30000, _
                           adLockOptimistic, _
                           adUseClient)
        
Next

Exit Function

Trata_Erro:
    Call Err.Raise(Err.Number, , "SEGL0143.cls00128.AlterarPropostaDocumentoGRU - " & Err.Description)
End Function

Public Function GerarMovimentacao(ByVal sSiglaSistema As String, _
                                  ByVal sSiglaRecurso As String, _
                                  ByVal sDescricaoRecurso As String, _
                                  ByVal lAmbienteID As Long, _
                                  ByVal NumDocumentoGRU As Integer, _
                                  ByVal sFlagAprovacao As String, _
                                  ByVal sRetidoBanco As String, _
                                  ByVal sTpOperacao As String, _
                                  ByVal sDtMovimentacao As String, _
                                  ByVal sUsuario As String, _
                                  ByRef lconexao As Long)

Dim sSQL As String

sDtMovimentacao = Mid(sDtMovimentacao, 7, 4) & Mid(sDtMovimentacao, 4, 2) & Mid(sDtMovimentacao, 1, 2)

On Error GoTo Trata_Erro

    sSQL = " "
    adSQL sSQL, " EXEC SEGS6641_SPI "
    adSQL sSQL, NumDocumentoGRU & ", "
    adSQL sSQL, "'" & sFlagAprovacao & "', "
    adSQL sSQL, "'" & sRetidoBanco & "', "
    adSQL sSQL, "'" & sTpOperacao & "', "
    adSQL sSQL, "'" & sDtMovimentacao & "', "
    adSQL sSQL, "'" & sUsuario & "'"

  Call Conexao_ExecutarSQL(sSiglaSistema, _
                           lAmbienteID, _
                           sSiglaRecurso, _
                           sDescricaoRecurso, _
                           sSQL, _
                           lconexao, _
                           False, _
                           False, _
                           30000, _
                           adLockOptimistic, _
                           adUseClient)
                           

 'Fechando a transa��o ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  Call ConfirmarTransacao(lconexao)

Exit Function

Trata_Erro:
    Call Err.Raise(Err.Number, , "SEGL0143.cls00128.GerarMovimentacao - " & Err.Description)
End Function

