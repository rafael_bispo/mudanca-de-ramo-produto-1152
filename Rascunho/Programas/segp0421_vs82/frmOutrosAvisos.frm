VERSION 5.00
Object = "{EA5A962F-86AD-4480-BCF2-3A94A4CB62B9}#1.0#0"; "GridAlianca.OCX"
Begin VB.Form frmOutrosAvisos 
   Caption         =   "Form1"
   ClientHeight    =   6165
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   10110
   LinkTopic       =   "Form1"
   ScaleHeight     =   6165
   ScaleWidth      =   10110
   StartUpPosition =   3  'Windows Default
   Begin VB.Frame Frame1 
      Enabled         =   0   'False
      Height          =   975
      Left            =   240
      TabIndex        =   2
      Top             =   240
      Width           =   9615
      Begin VB.TextBox txtDtNascimento 
         Height          =   285
         Left            =   7560
         TabIndex        =   8
         Top             =   480
         Width           =   1695
      End
      Begin VB.TextBox txtSexo 
         Height          =   285
         Left            =   4080
         TabIndex        =   6
         Top             =   480
         Width           =   2295
      End
      Begin VB.TextBox txtCpf 
         Height          =   285
         Left            =   240
         TabIndex        =   4
         Top             =   480
         Width           =   2295
      End
      Begin VB.Label Label3 
         Caption         =   "Data Nascimento:"
         Height          =   255
         Left            =   7560
         TabIndex        =   7
         Top             =   240
         Width           =   1695
      End
      Begin VB.Label Label2 
         Caption         =   "Sexo:"
         Height          =   255
         Left            =   4080
         TabIndex        =   5
         Top             =   240
         Width           =   1455
      End
      Begin VB.Label Label1 
         Caption         =   "CPF:"
         Height          =   255
         Left            =   240
         TabIndex        =   3
         Top             =   240
         Width           =   1215
      End
   End
   Begin GridAlianca.grdAlianca grdPesquisar 
      Height          =   3975
      Left            =   240
      TabIndex        =   1
      Top             =   1440
      Width           =   9615
      _ExtentX        =   16960
      _ExtentY        =   7011
      BorderStyle     =   1
      AllowUserResizing=   3
      EditData        =   0   'False
      Highlight       =   1
      ShowTip         =   0   'False
      SortOnHeader    =   0   'False
      BackColor       =   -2147483643
      BackColorBkg    =   -2147483633
      BackColorFixed  =   -2147483633
      BackColorSel    =   -2147483635
      FixedCols       =   1
      FixedRows       =   1
      FocusRect       =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty FontFixed {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   -2147483640
      ForeColorFixed  =   -2147483630
      ForeColorSel    =   -2147483634
      GridColor       =   -2147483630
      GridColorFixed  =   12632256
      GridLine        =   1
      GridLinesFixed  =   2
      MousePointer    =   0
      Redraw          =   -1  'True
      Rows            =   2
      TextStyle       =   0
      TextStyleFixed  =   0
      Cols            =   2
      RowHeightMin    =   0
   End
   Begin VB.CommandButton cmdVoltar 
      Caption         =   "Voltar"
      Height          =   375
      Left            =   8640
      TabIndex        =   0
      Top             =   5640
      Width           =   1215
   End
End
Attribute VB_Name = "frmOutrosAvisos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmdVoltar_Click()

    Unload Me

End Sub

Private Sub Form_Activate()

    If Pesquisar = False Then
        Unload Me
    End If

End Sub

Private Sub Form_Load()

    Call CentraFrm(Me)
    
    Me.Caption = "SEGPP0421 - Outros Avisos"
    
    Screen.MousePointer = vbNormal

End Sub

Private Sub InicializarGrid()

    With grdPesquisar
    
        .Clear
        .Rows = 2
        .FixedCols = 0
        .Cols = 7
        
        .TextMatrix(0, 0) = "Aviso AB"
        Call .TamanhoColuna(0, 1200)
        
        .TextMatrix(0, 1) = "Aviso BB"
        Call .TamanhoColuna(1, 1200)
            
        .TextMatrix(0, 2) = "Proposta"
        Call .TamanhoColuna(2, 1200)
        
        .TextMatrix(0, 3) = "Ap�lice"
        Call .TamanhoColuna(3, 1200)
        
        .TextMatrix(0, 4) = "Ramo"
        Call .TamanhoColuna(4, 800)

        .TextMatrix(0, 5) = "Data da Ocorr�ncia"
        Call .TamanhoColuna(5, 1500)
        
        .TextMatrix(0, 6) = "Data do Aviso"
        Call .TamanhoColuna(6, 1500)

    End With

End Sub

Function Pesquisar() As Boolean

Dim rsPesquisa As rdoResultset
Dim sSinistroBB As String
Dim sSexo As String
Dim sDtNascimento As String
Dim sCpf As String

On Error GoTo Trata_Erro

    sSexo = ""
    sDtNascimento = ""
    sCpf = ""
    
    Pesquisar = True
    
    MousePointer = vbHourglass
    
    sSinistroBB = frmConfDocumentos.flexSelecao.TextMatrix(frmConfDocumentos.flexSelecao.Row, 4)
    
    If sSinistroBB <> "" Then
        If ObterDados(rsPesquisa, sSinistroBB, sSexo, sDtNascimento, sCpf) = False Then
            MsgBox "N�o existem avisos para serem consultados", vbInformation
            Pesquisar = False
            Exit Function
        End If
    Else
        MsgBox "N�o existe informa��o sobre o segurado", vbInformation
        Pesquisar = False
        Exit Function
    End If
    
    If sCpf <> "" Then
        txtCpf.Text = Format(sCpf, "000000000-00")
    Else
        txtCpf.Text = ""
    End If
    If sSexo = "M" Then
        txtSexo.Text = "Masculino"
    ElseIf sSexo = "F" Then
        txtSexo.Text = "Feminino"
    Else
        txtSexo.Text = "N�o informado"
    End If
    txtDtNascimento.Text = Format(sDtNascimento, "dd/mm/yyyy")
    
    '# Exibindo dados
    
    Call InicializarGrid
  
    If Not rsPesquisa.EOF Then
    
        While Not rsPesquisa.EOF

            If grdPesquisar.Rows = 2 And grdPesquisar.TextMatrix(1, 0) = "" Then
            
                grdPesquisar.TextMatrix(1, 0) = Trim(rsPesquisa("sinistro_id"))
                grdPesquisar.TextMatrix(1, 1) = Trim(rsPesquisa("sinistro_bb"))
                grdPesquisar.TextMatrix(1, 2) = Trim(rsPesquisa("proposta_id"))
                grdPesquisar.TextMatrix(1, 3) = Trim(rsPesquisa("apolice_id"))
                grdPesquisar.TextMatrix(1, 4) = Trim(rsPesquisa("ramo_id"))
                grdPesquisar.TextMatrix(1, 5) = Format(rsPesquisa("dt_ocorrencia_sinistro"), "dd/mm/yyyy")
                grdPesquisar.TextMatrix(1, 6) = Format(rsPesquisa("dt_aviso_sinistro"), "dd/mm/yyyy")
            
            Else
            
                grdPesquisar.AdicionarLinha Trim(rsPesquisa("sinistro_id")) & vbTab & _
                                            Trim(rsPesquisa("sinistro_bb")) & vbTab & _
                                            Trim(rsPesquisa("proposta_id")) & vbTab & _
                                            Trim(rsPesquisa("apolice_id")) & vbTab & _
                                            Trim(rsPesquisa("ramo_id")) & vbTab & _
                                            Format(rsPesquisa("dt_ocorrencia_sinistro"), "dd/mm/yyyy") & vbTab & _
                                            Format(rsPesquisa("dt_aviso_sinistro"), "dd/mm/yyyy")

            End If
    
            rsPesquisa.MoveNext
            
        Wend
                
        rsPesquisa.Close
      
    End If
  
    Set rsPesquisa = Nothing

    MousePointer = vbDefault

    Exit Function
  
Trata_Erro:
    Call TratarErro("Pesquisar", Me.name)
    Call FinalizarAplicacao

End Function

Function ObterDados(ByRef rsPesquisa As rdoResultset, _
                    ByVal sSinistroBB As String, _
                    ByRef sSexo As String, _
                    ByRef sDtNascimento As String, _
                    ByRef sCpf As String) As Boolean

Dim sSQL As String

On Error GoTo Trata_Erro
    
    ObterDados = False
    
    sSQL = ""
    sSQL = sSQL & "SELECT sexo = ISNULL(sexo_segurado, ''), " & vbNewLine
    sSQL = sSQL & "       dt_nascimento = ISNULL(dt_nascimento_segurado, '19000101'), " & vbNewLine
    sSQL = sSQL & "       cpf = ISNULL(cpf_cgc_segurado, '') " & vbNewLine
    sSQL = sSQL & "  FROM evento_segbr_sinistro_tb WITH(NOLOCK) " & vbNewLine
    sSQL = sSQL & " WHERE sinistro_bb = " & sSinistroBB & vbNewLine
    sSQL = sSQL & "   AND evento_bb_id IN (2000, 1100) "
    Set rsPesquisa = rdocn.OpenResultset(sSQL)
    If rsPesquisa.EOF = False Then
        sSexo = rsPesquisa("sexo")
        sDtNascimento = rsPesquisa("dt_nascimento")
        sCpf = rsPesquisa("cpf")
    Else
        Set rsPesquisa = Nothing
        Exit Function
    End If
    Set rsPesquisa = Nothing
    
    sSQL = ""
    sSQL = sSQL & "SELECT sinistro_vida_tb.sinistro_id, " & vbNewLine
    sSQL = sSQL & "       sinistro_vida_tb.apolice_id, " & vbNewLine
    sSQL = sSQL & "       sinistro_vida_tb.ramo_id, " & vbNewLine
    sSQL = sSQL & "       sinistro_bb = '" & sSinistroBB & "', " & vbNewLine
    sSQL = sSQL & "       sinistro_tb.proposta_id, " & vbNewLine
    sSQL = sSQL & "       sinistro_tb.dt_aviso_sinistro, " & vbNewLine
    sSQL = sSQL & "       sinistro_tb.dt_ocorrencia_sinistro " & vbNewLine
    sSQL = sSQL & "  FROM sinistro_vida_tb WITH(NOLOCK) " & vbNewLine
    sSQL = sSQL & "  JOIN sinistro_tb WITH(NOLOCK) " & vbNewLine
    sSQL = sSQL & "    ON sinistro_vida_tb.sinistro_id = sinistro_tb.sinistro_id " & vbNewLine
    sSQL = sSQL & "   AND sinistro_vida_tb.apolice_id = sinistro_tb.apolice_id " & vbNewLine
    sSQL = sSQL & "   AND sinistro_vida_tb.sucursal_seguradora_id = sinistro_tb.sucursal_seguradora_id " & vbNewLine
    sSQL = sSQL & "   AND sinistro_vida_tb.seguradora_cod_susep = sinistro_tb.seguradora_cod_susep " & vbNewLine
    sSQL = sSQL & "   AND sinistro_vida_tb.ramo_id = sinistro_tb.ramo_id " & vbNewLine
    sSQL = sSQL & " WHERE sinistro_vida_tb.sexo = '" & sSexo & "'"
    sSQL = sSQL & "   AND sinistro_vida_tb.dt_nascimento = '" & Format(sDtNascimento, "yyyymmdd") & "'"
    sSQL = sSQL & "   AND sinistro_vida_tb.cpf = '" & sCpf & "'"
    Set rsPesquisa = rdocn.OpenResultset(sSQL)
    If rsPesquisa.EOF = False Then
        ObterDados = True
    End If

    Exit Function
  
Trata_Erro:
    Call TratarErro("ObterDados", Me.name)
    Call FinalizarAplicacao

End Function

