Attribute VB_Name = "modConfDocumentos"
Option Explicit

'***************************************************
'Alterado por Cleber da Stefanini - data: 24/03/2005
Global Str_captura_list_checks As String
'***************************************************

Global rdocn_aux                As New rdoConnection
Global rdocn_Interface          As New rdoConnection
Global rs                       As rdoResultset
Global rsTB                     As rdoResultset
Global vsql                     As String
Global vTpRamo                  As String

''* **************************************************************************** *'
''* Constantes utilizadas para evento_SEGBR_id                                   *'
''* **************************************************************************** *'
Global Const EVENTO_SEGBR_CONF_RECEB_DOCUMENTO = 10100         'evento BB: 1110
Global Const EVENTO_SEGBR_CANCELAMENTO_RECEB_DOCUMENTO = 10101 'evento BB: 1111
Global Const EVENTO_SEGBR_RESSALVA_FALTA_DOCS_BASICOS = 10125  'evento BB: 1112
Global Const EVENTO_SEGBR_RECUSADO_MOTIVOS_DIVERSOS = 10126    'evento BB: 1113
'Global Const EVENTO_SEGBR_AVISO_HOMOLOGADO = 10110             'evento BB: 2003
'Global Const EVENTO_SEGBR_EXIGENCIA_PROVIDENCIADA = 10117      'evento BB: 2011
'Global Const EVENTO_SEGBR_PGTO_DE_RECIBO = 10123               'evento BB: 2014
''Global Const EVENTO_SEGBR_AVISO_MANUAL = 10124                'evento BB: NULL
''* **************************************************************************** *'

'* **************************************************************************** *'
'* Constantes utilizadas para evento_SEGBR_id                                   *'
'* **************************************************************************** *'
Global Const EVENTO_SEGBR_AVISO_HOMOLOGADO = 2003              'evento ID: 10110
Global Const EVENTO_SEGBR_EXIGENCIA_PROVIDENCIADA = 2011       'evento ID: 10117
Global Const EVENTO_SEGBR_PGTO_DE_RECIBO = 2014                'evento ID: 10123
'* **************************************************************************** *'

Global SinistrosSelecionados    As New Collection


'*****************************************************
'Matriz para substituir a classe com os dados do aviso
'*****************************************************
Public Type objAviso
    TipoAlteracao           As Variant
    Sinistro_id             As Variant
    Apolice_id              As Variant
    Sucursal_id             As Variant
    Seguradora_id           As Variant
    Ramo_id                 As Variant
    Situacao                As Variant
    MotivoEncerramento      As Variant
    Seq_Estimativa          As Variant
    Sinistro_BB             As Variant
    Proposta_id             As Variant
    Num_Recibo              As Variant
    Lock_Aviso              As Variant
    Segurado_id             As Variant
    Nome_Segurado           As Variant
    Proposta_BB             As Variant
    evento_SEGBR_id         As Variant
    Cod_Remessa_GTR         As Variant
    Acao_ID                 As Variant
End Type
'****************************
'TipoAlteracao (1)
'Sinistro_id (2)
'Apolice_id (3)
'Sucursal_id (4)
'Seguradora_id (5)
'Ramo_id (6)
'Situacao (7)
'MotivoEncerramento (8)
'Seq_Estimativa (9)
'Sinistro_BB (10)
'Proposta_id (11)
'Num_Recibo (12)
'Lock_Aviso (13)
'Segurado_id (14)
'Nome_Segurado (15)
'Proposta_BB (16)
'evento_SEGBR_id (17)
'Cod_Remessa_GTR (18)
'*****************************
Public arrAviso                 As objAviso
'Guarda a posi��o da linha selecionada
Public posAviso                 As Integer


Public Function AlterarConfirmacaoDocumentos(OTipoAlteracao As String) As Boolean
                                                                    ', OSinistro As cSinistro) As Boolean
        
        Dim OEvento_SEGBR_ID    As String, ADescr_Evento    As String
        Dim ALocalizacao_Aviso   As String

        'sergio.sn - 22/10/2010 - valida��o para n�o enviar campos preenchidos
        '                         incorretamente
        Dim strDescricaoErro As String
        
        strDescricaoErro = ""
        
        'sergio.en - 22/10/2010
        
        On Error GoTo TrataErro
        
        '* Verifica qual o evento ser� gravado
        If OTipoAlteracao = "1" Then      'evento BB: 1110
           OEvento_SEGBR_ID = EVENTO_SEGBR_CONF_RECEB_DOCUMENTO 'CONFIRMAR RECEBIMENTO DE DOCUMENTO
           ADescr_Evento = "PROCESSO ACATADO"
        ElseIf OTipoAlteracao = "2" Then  'evento BB: 1112
           OEvento_SEGBR_ID = EVENTO_SEGBR_RESSALVA_FALTA_DOCS_BASICOS 'PROTOCOLO COM RESSALVA POR FALTA DE DOCUMENTOS B�SICOS
           ADescr_Evento = "PROCESSO DEVOLVIDO POR FALTA DE DOCUMENTOS B�SICOS"
        ElseIf OTipoAlteracao = "3" Then  'evento BB: 1113
           OEvento_SEGBR_ID = EVENTO_SEGBR_RECUSADO_MOTIVOS_DIVERSOS 'PROTOCOLO RECUSADO POR MOTIVOS DIVERSOS
           ADescr_Evento = "PROCESSO DEVOLVIDO POR MOTIVOS DIVERSOS"
        End If
        
        'If Verifica_Localizacao(OSinistro.Sinistro_BB, ALocalizacao_Aviso) Then
        If Verifica_Localizacao(arrAviso.Sinistro_BB, ALocalizacao_Aviso) Then
           '* Inicia transa��o
           rdocn.BeginTrans
           
           If ALocalizacao_Aviso = "SEGBR" Then
              'If Not Grava_Evento_SEGBR(OTipoAlteracao, OSinistro, OEvento_SEGBR_ID, ADescr_Evento) Then
              If Not Grava_Evento_SEGBR(OTipoAlteracao, OEvento_SEGBR_ID, ADescr_Evento) Then
                 rdocn.RollbackTrans
                 MsgBox "Erro ao Incluir evento SEGBR.", vbOKOnly + vbCritical
                 Exit Function
              End If
           ElseIf ALocalizacao_Aviso = "LOG" Then
              'If Not Grava_Evento_LOG(OTipoAlteracao, OSinistro, OEvento_SEGBR_ID, ADescr_Evento) Then
              If Not Grava_Evento_LOG(OTipoAlteracao, OEvento_SEGBR_ID, ADescr_Evento) Then
                 rdocn.RollbackTrans
                 MsgBox "Erro ao Incluir evento SEGBR.", vbOKOnly + vbCritical
                 Exit Function
              End If
           End If
        Else
           MsgBox "Ocorreu um erro na verifica��o da localiza��o do aviso ! Opera��o n�o pode ser efetuada.", vbOKOnly + vbCritical
           Exit Function
        End If

        '*******************************************************************
        'Para o evento 1200 alterar a data de envio para NULL do evento 2003
        If OEvento_SEGBR_ID = 10138 Then
            Dim SQL             As String
            Dim rsEvento        As rdoResultset
            Dim strCodRemessa   As String

            strCodRemessa = ""
            SQL = ""
            SQL = SQL & "Select cod_remessa "
            SQL = SQL & " from INTERFACE_DB..entrada_gtr_tb with(nolock) "
            SQL = SQL & " where evento_bb = 2003 and sinistro_bb = " & arrAviso.Sinistro_BB
            Set rsEvento = rdocn.OpenResultset(SQL)
            strCodRemessa = rsEvento(0)
            rsEvento.Close
                        
            'sergio.sn - 22/10/2010 - valida��o para n�o enviar campos preenchidos
            '                         incorretamente
            
            If IsNumeric(Trim(strCodRemessa)) = False Or Len(Trim(cUserName)) = 0 Then
            
               strDescricaoErro = "Evento " & OEvento_SEGBR_ID & vbCrLf
               
               strDescricaoErro = strDescricaoErro & IIf(IsNumeric(Trim(strCodRemessa)) = False, " Codigo de Remessa Inv�lido! " & strCodRemessa & vbCrLf, "")
               
               strDescricaoErro = strDescricaoErro & IIf(Len(Trim(cUserName)) = 0, "Identifica��o do usu�rio n�o informado", "")
            
               GoTo TrataErro
                            
            End If
            
            'sergio.en - 22/10/2010

            vsql = "EXEC INTERFACE_DB..documentos_recebidos_GTR_spu "
            vsql = vsql & "'" & strCodRemessa & "'"
            vsql = vsql & ", NULL"
            vsql = vsql & ", '" & cUserName & "'"
            rdocn.Execute (vsql)
        End If
        
        'sergio.sn - 22/10/2010 - valida��o para n�o enviar campos preenchidos
        '                         incorretamente
        
        If IsNumeric(Trim(arrAviso.Cod_Remessa_GTR)) = False Or _
            IsDate(Data_Sistema) = False Or _
            Len(Trim(cUserName)) = 0 Then
            
           strDescricaoErro = "Evento " & OEvento_SEGBR_ID & vbCrLf
        
           strDescricaoErro = strDescricaoErro & IIf(IsNumeric(Trim(arrAviso.Cod_Remessa_GTR)) = False, " C�digo de Remessa Inv�lido: " & strCodRemessa & vbCrLf, "")
           
           strDescricaoErro = strDescricaoErro & IIf(IsDate(Data_Sistema) = False, " Data de Envio Inv�lida! " & Data_Sistema & vbCrLf, "")
           
           strDescricaoErro = strDescricaoErro & IIf(Len(Trim(cUserName)) = 0, "Identifica��o do usu�rio n�o informado", "")
        
           GoTo TrataErro
                        
        End If
        
        'sergio.en - 22/10/2010
                
        '* Marca o Documento como confirmado na tabela Documentos_Recebidos_GTR_tb.
        vsql = "SET XACT_ABORT ON EXEC INTERFACE_DB..documentos_recebidos_GTR_spu "
        'vSql = vSql & "'" & OSinistro.Cod_Remessa_GTR & "'"
        vsql = vsql & "'" & arrAviso.Cod_Remessa_GTR & "'"
        vsql = vsql & ", '" & Format(Data_Sistema, "YYYYMMDD") & "'"
        vsql = vsql & ", '" & cUserName & "'"
        rdocn.Execute (vsql)

        rdocn.CommitTrans

'*****************************************************************************************
'        Mensagem para mostrar os dados do aviso a ser protocolado
'*****************************************************************************************
'        If MsgBox("CONFIRMA OS DADOS A SEREM PROCESSADOS?" & vbCrLf & vbCrLf _
'                    & "     N�  DO AVISO: " & arrAviso.Sinistro_id & vbCrLf _
'                    & "     N� DO AVISO BB: " & arrAviso.Sinistro_BB & vbCrLf _
'                    & "     N� DA PROPOSTA: " & arrAviso.Proposta_id & vbCrLf _
'                    & "     N� DA PROPOSTA BB: " & arrAviso.Proposta_BB & vbCrLf _
'                    & "     N� DO RECIBO: " & arrAviso.Num_Recibo & vbCrLf _
'                    & "     NOME DO SEGURADO: " & arrAviso.Nome_Segurado, vbCrLf _
'                    & vbQuestion + vbDefaultButton2 + vbYesNo, "Aviso...") = vbYes Then
'            rdocn.CommitTrans
'        Else
'            rdocn.RollbackTrans
'            AlterarConfirmacaoDocumentos = False
'            Exit Function
'        End If

        AlterarConfirmacaoDocumentos = True
        
        Exit Function
        
TrataErro:
    rdocn.RollbackTrans
    AlterarConfirmacaoDocumentos = False
    'sergio.so - 22/10/2010
    'mensagem_erro 6, "Erro ao executar altera��o na confirma��o de recebimento de documentos."
    'sergio.eo
    
    'sergio.sn - 22/10/2010
    
    mensagem_erro 6, "Erro ao executar altera��o na confirma��o de recebimento de documentos." & _
                    IIf(Len(strDescricaoErro) = 0, "", vbCrLf & strDescricaoErro)
                    
    'sergio.en
    '* Call TerminaSEGBR

End Function


Public Sub Conexao_GTR()
       
       Dim GTR_servidor As String, GTR_banco    As String
       Dim GTR_usuario  As String, GTR_senha    As String
       
       If Obtem_Dados_Conexao_GTR(GTR_servidor, GTR_banco, GTR_usuario, GTR_senha) = False Then MsgBox "N�o foi poss�vel criar conex�o com o Banco INTERFACE_DB.", vbOKOnly + vbExclamation: Call TerminaSEGBR

       GTR_servidor = "SISAB051\QUALID"
       GTR_usuario = "SISBR"
       GTR_senha = "SISBR_QLD"
       
       With rdocn_Interface
            If .Connect = "" Then
                .Connect = "UID=" & GTR_usuario & ";PWD=" & GTR_senha & ";server=" & GTR_servidor & ";driver={SQL Server};database=" & GTR_banco & ";"
                .QueryTimeout = 30000
                .CursorDriver = rdUseNone
                .EstablishConnection rdDriverNoPrompt
            End If
       End With

End Sub

Public Function Obtem_Dados_Conexao_GTR(ByRef odcServidor As String, ByRef odcBanco As String, ByRef odcUsuario As String, ByRef odcSenha As String) As Boolean
        
        Dim objAmbiente As Object 'Variant
        
        Obtem_Dados_Conexao_GTR = False
        
        On Error GoTo TrataErro
        
        Set objAmbiente = CreateObject("SABL0010.cls00009")
        
        odcServidor = objAmbiente.RetornaValorAmbiente("GTR", "SISTEMA", "SERVIDOR", gsCHAVESISTEMA)
        odcBanco = objAmbiente.RetornaValorAmbiente("GTR", "SISTEMA", "BANCO", gsCHAVESISTEMA)
        odcUsuario = objAmbiente.RetornaValorAmbiente("GTR", "SISTEMA", "USUARIO", gsCHAVESISTEMA)
        odcSenha = objAmbiente.RetornaValorAmbiente("GTR", "SISTEMA", "SENHA", gsCHAVESISTEMA)
        
        Set objAmbiente = Nothing
        
        Obtem_Dados_Conexao_GTR = True
        
        Exit Function
        
TrataErro:
    Exit Function
    
End Function

Public Function Verifica_Localizacao(ByVal OSinistro_BB As String, ByRef ALocalizacao_Aviso As String) As Boolean
        
        '***********************************************************************'
        ' Esta rotina verifica qual a localiza��o do AVISO                      '
        ' Localizacao_Aviso: Este campo ser� utilizado para informar a locali-  '
        '                    za��o do aviso.                                    '
        '***********************************************************************'
        
        Dim SQL As String
        Dim rs  As rdoResultset
        
        On Error GoTo TrataErro
        
        Verifica_Localizacao = True
        
        'Procura no SEGBR
        SQL = "SELECT COUNT(*) "
        SQL = SQL & " FROM sinistro_bb_tb with(nolock) "
        SQL = SQL & " WHERE sinistro_bb = " & OSinistro_BB
        Set rs = rdocn.OpenResultset(SQL)
        If rs(0) = 0 Then ' n�o encontrou no SEGBR
           rs.Close
           ' Procura no LOG
           SQL = "SELECT COUNT(*) "
           SQL = SQL & " FROM web_seguros_db.dbo.robo_aviso_sinistro_log with(nolock) "
           SQL = SQL & " WHERE num_aviso = '" & OSinistro_BB & "'"
           Set rs = rdocn.OpenResultset(SQL)
           If rs(0) > 0 Then ' Encontrou no LOG
              ALocalizacao_Aviso = "LOG"
           End If
        Else
           ALocalizacao_Aviso = "SEGBR"
        End If
        rs.Close
        
        Exit Function
        
TrataErro:
    'Grava o Log do Erro
    Verifica_Localizacao = False
    'Call GravaLogErro("", "Descri��o: " & Err.Description, "SQL", "VERIFICA_LOCALIZACAO", Err.Number)
    Call TerminaSEGBR
    
End Function

Private Function Grava_Evento_SEGBR(ByVal OTipoAlteracao As String, ByVal OEvento_SEGBR_ID As String, ByVal ADescr_Evento As String) As Boolean
'Private Function Grava_Evento_SEGBR(ByVal OTipoAlteracao As String, ByVal OSinistro As cSinistro, ByVal OEvento_SEGBR_ID As String, ByVal ADescr_Evento As String) As Boolean
    
    Dim OSeq_Evento     As String, OProduto_id  As String
    Dim Detalhamento_id As String, OEvento      As String
    Dim ASeq_Estimativa As String, Conta_Doctos As Integer
    Dim i               As Integer
    Dim nova_situacao   As Integer 'Stefanini (Maur�cio), em 23/02/2005
    Dim Situacao        As Integer 'Stefanini (Maur�cio), em 23/02/2005
    
    Dim bObsDetalhamento As Boolean
    
    bObsDetalhamento = False
    
    On Error GoTo TrataErro
    
    Grava_Evento_SEGBR = True

    '* Vari�vel para contar a qtde de documentos
    Conta_Doctos = 0

    
    'Stefanini (Maur�cio), em 23/02/2005 para inserir a �ltima situa��o registrada
    '*******************************************************************************************
    If OEvento_SEGBR_ID = "10100" Or OEvento_SEGBR_ID = "10101" Then
    nova_situacao = BuscaUltimaSituacaoHistorico(arrAviso.Sinistro_id)
        If nova_situacao <> -1 Then 'Caso for "", vai inserir a situa��o j� registrada no array
            Situacao = nova_situacao
        Else
            Situacao = arrAviso.Situacao
        End If
    End If
    '*******************************************************************************************
    
    If arrAviso.Num_Recibo = "0" Then
        
        Detalhamento_id = ""
        '* Obt�m o �ltimo detalhamento_id
        vsql = "SELECT detalhamento_id = isnull(MAX(detalhamento_id),0) + 1"
        vsql = vsql & " FROM  sinistro_detalhamento_tb with(nolock) "
        vsql = vsql & " WHERE sinistro_id            = " & arrAviso.Sinistro_id
        'myoshimura
        '07/03/2004
        'tratamento sem apolice
        'vSQL = vSQL & "   AND apolice_id             = " & .Apolice_id
        'vSQL = vSQL & "   AND seguradora_cod_susep   = " & .Seguradora_id
        'vSQL = vSQL & "   AND sucursal_seguradora_id = " & .Sucursal_id
        'vSQL = vSQL & "   AND ramo_id                = " & .Ramo_id
        Set rsTB = rdocn.OpenResultset(vsql)
            Detalhamento_id = rsTB!Detalhamento_id
        rsTB.Close
        
        vsql = "SELECT seq_estimativa = isnull(MAX(seq_estimativa),1)"
        vsql = vsql & " FROM  sinistro_estimativa_tb with(nolock) "
        vsql = vsql & " WHERE sinistro_id            = " & arrAviso.Sinistro_id
        'myoshimura
        '07/03/2004
        'tratamento sem apolice
        'vSQL = vSQL & "   AND apolice_id             = " & .Apolice_id
        'vSQL = vSQL & "   AND seguradora_cod_susep   = " & .Seguradora_id
        'vSQL = vSQL & "   AND sucursal_seguradora_id = " & .Sucursal_id
        'vSQL = vSQL & "   AND ramo_id                = " & .Ramo_id
        Set rsTB = rdocn.OpenResultset(vsql)
            ASeq_Estimativa = rsTB!Seq_Estimativa
        rsTB.Close
        
        
        vsql = "EXEC sinistro_historico_spi "
        vsql = vsql & arrAviso.Sinistro_id & ", "
        'tratamento sem apolice
        vsql = vsql & IIf(IsNull(arrAviso.Apolice_id), "NULL", arrAviso.Apolice_id) & ", "
        vsql = vsql & IIf(IsNull(arrAviso.Sucursal_id), "NULL", arrAviso.Sucursal_id) & ", "
        vsql = vsql & IIf(IsNull(arrAviso.Seguradora_id), "NULL", arrAviso.Seguradora_id) & ", "
        vsql = vsql & arrAviso.Ramo_id & ", "
        vsql = vsql & OEvento_SEGBR_ID & ", '"
        'vSql = vSql & arrAviso.situacao & "', " 'Stefanini (Maur�cio) , em 23/02/2005
        vsql = vsql & Situacao & "', "
        vsql = vsql & IIf(arrAviso.MotivoEncerramento = "", "NULL", "'" & arrAviso.MotivoEncerramento & "'") & ", '"
        vsql = vsql & Format(Data_Sistema, "YYYYMMDD") & "', '"
        vsql = vsql & gsCPF & "', "
        vsql = vsql & ASeq_Estimativa & ", "        '@seq_estimativa
        If OEvento_SEGBR_ID = 10100 Then
            vsql = vsql & " NULL, "        '@detalhamento_id
        Else
            vsql = vsql & Detalhamento_id & ", "        '@detalhamento_id
        End If
        vsql = vsql & "'s' "                        '@evento_visualizado
        Set rsTB = rdocn.OpenResultset(vsql)
            OSeq_Evento = rsTB(0)
        rsTB.Close
        
        vsql = "EXEC sinistro_item_historico_spi "
        vsql = vsql & arrAviso.Sinistro_id & ","
        'tratamento sem apolice
        vsql = vsql & IIf(IsNull(arrAviso.Apolice_id), "NULL", arrAviso.Apolice_id) & ", "
        vsql = vsql & IIf(IsNull(arrAviso.Sucursal_id), "NULL", arrAviso.Sucursal_id) & ", "
        vsql = vsql & IIf(IsNull(arrAviso.Seguradora_id), "NULL", arrAviso.Seguradora_id) & ", "
        vsql = vsql & arrAviso.Ramo_id & ","
        vsql = vsql & OSeq_Evento & ",'"
        vsql = vsql & ADescr_Evento & "','"
        vsql = vsql & gsCPF & "'"
        rdocn.Execute (vsql)
        
            
        If frmDetalhamento.txtObservacao1.Text <> "" Then
            vsql = "EXEC sinistro_item_historico_spi "
            vsql = vsql & arrAviso.Sinistro_id & ","
            vsql = vsql & IIf(IsNull(arrAviso.Apolice_id), "NULL", arrAviso.Apolice_id) & ", "
            vsql = vsql & IIf(IsNull(arrAviso.Sucursal_id), "NULL", arrAviso.Sucursal_id) & ", "
            vsql = vsql & IIf(IsNull(arrAviso.Seguradora_id), "NULL", arrAviso.Seguradora_id) & ", "
            vsql = vsql & arrAviso.Ramo_id & ","
            vsql = vsql & CInt(OSeq_Evento) + 1 & ",'"
            vsql = vsql & frmDetalhamento.txtObservacao1.Text & "','"
            vsql = vsql & gsCPF & "'"
            rdocn.Execute (vsql)
            If frmDetalhamento.txtObservacao2.Text <> "" Then
                vsql = "EXEC sinistro_item_historico_spi "
                vsql = vsql & arrAviso.Sinistro_id & ","
                vsql = vsql & IIf(IsNull(arrAviso.Apolice_id), "NULL", arrAviso.Apolice_id) & ", "
                vsql = vsql & IIf(IsNull(arrAviso.Sucursal_id), "NULL", arrAviso.Sucursal_id) & ", "
                vsql = vsql & IIf(IsNull(arrAviso.Seguradora_id), "NULL", arrAviso.Seguradora_id) & ", "
                vsql = vsql & arrAviso.Ramo_id & ","
                vsql = vsql & CInt(OSeq_Evento) + 2 & ",'"
                vsql = vsql & frmDetalhamento.txtObservacao2.Text & "','"
                vsql = vsql & gsCPF & "'"
                rdocn.Execute (vsql)
                If frmDetalhamento.txtObservacao3.Text <> "" Then
                    vsql = "EXEC sinistro_item_historico_spi "
                    vsql = vsql & arrAviso.Sinistro_id & ","
                    vsql = vsql & IIf(IsNull(arrAviso.Apolice_id), "NULL", arrAviso.Apolice_id) & ", "
                    vsql = vsql & IIf(IsNull(arrAviso.Sucursal_id), "NULL", arrAviso.Sucursal_id) & ", "
                    vsql = vsql & IIf(IsNull(arrAviso.Seguradora_id), "NULL", arrAviso.Seguradora_id) & ", "
                    vsql = vsql & arrAviso.Ramo_id & ","
                    vsql = vsql & CInt(OSeq_Evento) + 3 & ",'"
                    vsql = vsql & frmDetalhamento.txtObservacao3.Text & "','"
                    vsql = vsql & gsCPF & "'"
                    rdocn.Execute (vsql)
                End If
            End If
        End If
        
        vsql = "EXEC sinistro_detalhamento_spi "
        vsql = vsql & arrAviso.Sinistro_id & ","
        'tratamento sem apolice
        vsql = vsql & IIf(IsNull(arrAviso.Apolice_id), "NULL", arrAviso.Apolice_id) & ", "
        vsql = vsql & IIf(IsNull(arrAviso.Sucursal_id), "NULL", arrAviso.Sucursal_id) & ", "
        vsql = vsql & IIf(IsNull(arrAviso.Seguradora_id), "NULL", arrAviso.Seguradora_id) & ", "
        vsql = vsql & arrAviso.Ramo_id & ","
        vsql = vsql & "'" & Format(Data_Sistema, "YYYYMMDD") & "', "
        vsql = vsql & "0, "                     '@tp_detalhamento
        vsql = vsql & "'n', "                   '@restrito
        vsql = vsql & "'" & cUserName & "'"      '@usuario
        rdocn.Execute (vsql)
        
        Conta_Doctos = Conta_Doctos + 1
        '* Grava o titulo do detalhamento
        vsql = "EXEC sinistro_linha_detalhe_spi "
        vsql = vsql & arrAviso.Sinistro_id & ","
        'tratamento sem apolice
        vsql = vsql & IIf(IsNull(arrAviso.Apolice_id), "NULL", arrAviso.Apolice_id) & ", "
        vsql = vsql & IIf(IsNull(arrAviso.Sucursal_id), "NULL", arrAviso.Sucursal_id) & ", "
        vsql = vsql & IIf(IsNull(arrAviso.Seguradora_id), "NULL", arrAviso.Seguradora_id) & ", "
        vsql = vsql & arrAviso.Ramo_id & ","
        vsql = vsql & Detalhamento_id & ",'"         '@detalhamento_id
        vsql = vsql & Conta_Doctos & "', "           '@linha_id
        vsql = vsql & "'" & ADescr_Evento & "', "    '@linha
        vsql = vsql & "'" & gsCPF & "'"            '@usuario
        rdocn.Execute (vsql)
        
        If frmDetalhamento.txtObservacao1.Text <> "" Then
            vsql = "EXEC sinistro_linha_detalhe_spi "
            vsql = vsql & arrAviso.Sinistro_id & ","
            vsql = vsql & IIf(IsNull(arrAviso.Apolice_id), "NULL", arrAviso.Apolice_id) & ", "
            vsql = vsql & IIf(IsNull(arrAviso.Sucursal_id), "NULL", arrAviso.Sucursal_id) & ", "
            vsql = vsql & IIf(IsNull(arrAviso.Seguradora_id), "NULL", arrAviso.Seguradora_id) & ", "
            vsql = vsql & arrAviso.Ramo_id & ","
            vsql = vsql & Detalhamento_id & ",'"         '@detalhamento_id
            vsql = vsql & Conta_Doctos + 1 & "', "           '@linha_id
            vsql = vsql & "'" & frmDetalhamento.txtObservacao1.Text & "', "    '@linha
            vsql = vsql & "'" & gsCPF & "'"            '@usuario
            rdocn.Execute (vsql)
            If frmDetalhamento.txtObservacao2.Text <> "" Then
                vsql = "EXEC sinistro_linha_detalhe_spi "
                vsql = vsql & arrAviso.Sinistro_id & ","
                vsql = vsql & IIf(IsNull(arrAviso.Apolice_id), "NULL", arrAviso.Apolice_id) & ", "
                vsql = vsql & IIf(IsNull(arrAviso.Sucursal_id), "NULL", arrAviso.Sucursal_id) & ", "
                vsql = vsql & IIf(IsNull(arrAviso.Seguradora_id), "NULL", arrAviso.Seguradora_id) & ", "
                vsql = vsql & arrAviso.Ramo_id & ","
                vsql = vsql & Detalhamento_id & ",'"         '@detalhamento_id
                vsql = vsql & Conta_Doctos + 2 & "', "           '@linha_id
                vsql = vsql & "'" & frmDetalhamento.txtObservacao2.Text & "', "    '@linha
                vsql = vsql & "'" & gsCPF & "'"            '@usuario
                rdocn.Execute (vsql)
                If frmDetalhamento.txtObservacao3.Text <> "" Then
                    vsql = "EXEC sinistro_linha_detalhe_spi "
                    vsql = vsql & arrAviso.Sinistro_id & ","
                    vsql = vsql & IIf(IsNull(arrAviso.Apolice_id), "NULL", arrAviso.Apolice_id) & ", "
                    vsql = vsql & IIf(IsNull(arrAviso.Sucursal_id), "NULL", arrAviso.Sucursal_id) & ", "
                    vsql = vsql & IIf(IsNull(arrAviso.Seguradora_id), "NULL", arrAviso.Seguradora_id) & ", "
                    vsql = vsql & arrAviso.Ramo_id & ","
                    vsql = vsql & Detalhamento_id & ",'"         '@detalhamento_id
                    vsql = vsql & Conta_Doctos + 3 & "', "           '@linha_id
                    vsql = vsql & "'" & frmDetalhamento.txtObservacao3.Text & "', "    '@linha
                    vsql = vsql & "'" & gsCPF & "'"            '@usuario
                    rdocn.Execute (vsql)
                End If
            End If
        End If
        
        
        '* Verifica qual o tipo de altera��o para gravar a linha do detalhamento
        If OTipoAlteracao = "2" Then
           With frmDetalhamento.lstDocumentos
                For i = 0 To .ListCount - 1
                    If .Selected(i) Then
                        Conta_Doctos = Conta_Doctos + 1
                       '* GRAVA NO ITEM DO HIST�RICO
                        vsql = "EXEC sinistro_item_historico_spi "
                        vsql = vsql & arrAviso.Sinistro_id & ","
                        'tratamento sem apolice
                        vsql = vsql & IIf(IsNull(arrAviso.Apolice_id), "NULL", arrAviso.Apolice_id) & ", "
                        vsql = vsql & IIf(IsNull(arrAviso.Sucursal_id), "NULL", arrAviso.Sucursal_id) & ", "
                        vsql = vsql & IIf(IsNull(arrAviso.Seguradora_id), "NULL", arrAviso.Seguradora_id) & ", "
                        vsql = vsql & arrAviso.Ramo_id & ","
                        vsql = vsql & OSeq_Evento & ",'"
                        vsql = vsql & .List(i) & "','"
                        vsql = vsql & gsCPF & "'"
                        rdocn.Execute (vsql)
                       
                       If frmDetalhamento.txtObservacao1.Text <> "" Then
                            vsql = "EXEC sinistro_item_historico_spi "
                            vsql = vsql & arrAviso.Sinistro_id & ","
                            vsql = vsql & IIf(IsNull(arrAviso.Apolice_id), "NULL", arrAviso.Apolice_id) & ", "
                            vsql = vsql & IIf(IsNull(arrAviso.Sucursal_id), "NULL", arrAviso.Sucursal_id) & ", "
                            vsql = vsql & IIf(IsNull(arrAviso.Seguradora_id), "NULL", arrAviso.Seguradora_id) & ", "
                            vsql = vsql & arrAviso.Ramo_id & ","
                            vsql = vsql & CInt(OSeq_Evento) + 1 & ",'"
                            vsql = vsql & frmDetalhamento.txtObservacao1.Text & "','"
                            vsql = vsql & gsCPF & "'"
                            rdocn.Execute (vsql)
                            If frmDetalhamento.txtObservacao2.Text <> "" Then
                                 vsql = "EXEC sinistro_item_historico_spi "
                                 vsql = vsql & arrAviso.Sinistro_id & ","
                                 vsql = vsql & IIf(IsNull(arrAviso.Apolice_id), "NULL", arrAviso.Apolice_id) & ", "
                                 vsql = vsql & IIf(IsNull(arrAviso.Sucursal_id), "NULL", arrAviso.Sucursal_id) & ", "
                                 vsql = vsql & IIf(IsNull(arrAviso.Seguradora_id), "NULL", arrAviso.Seguradora_id) & ", "
                                 vsql = vsql & arrAviso.Ramo_id & ","
                                 vsql = vsql & CInt(OSeq_Evento) + 2 & ",'"
                                 vsql = vsql & frmDetalhamento.txtObservacao2.Text & "','"
                                 vsql = vsql & gsCPF & "'"
                                 rdocn.Execute (vsql)
                                If frmDetalhamento.txtObservacao3.Text <> "" Then
                                     vsql = "EXEC sinistro_item_historico_spi "
                                     vsql = vsql & arrAviso.Sinistro_id & ","
                                     vsql = vsql & IIf(IsNull(arrAviso.Apolice_id), "NULL", arrAviso.Apolice_id) & ", "
                                     vsql = vsql & IIf(IsNull(arrAviso.Sucursal_id), "NULL", arrAviso.Sucursal_id) & ", "
                                     vsql = vsql & IIf(IsNull(arrAviso.Seguradora_id), "NULL", arrAviso.Seguradora_id) & ", "
                                     vsql = vsql & arrAviso.Ramo_id & ","
                                     vsql = vsql & CInt(OSeq_Evento) + 3 & ",'"
                                     vsql = vsql & frmDetalhamento.txtObservacao3.Text & "','"
                                     vsql = vsql & gsCPF & "'"
                                     rdocn.Execute (vsql)
                                End If
                            End If
                       End If
                       
                       
                       '* GRAVA NO DETALHAMENTO
                        vsql = "EXEC sinistro_linha_detalhe_spi "
                        vsql = vsql & arrAviso.Sinistro_id & ","
                        vsql = vsql & IIf(IsNull(arrAviso.Apolice_id), "NULL", arrAviso.Apolice_id) & ", "
                        vsql = vsql & IIf(IsNull(arrAviso.Sucursal_id), "NULL", arrAviso.Sucursal_id) & ", "
                        vsql = vsql & IIf(IsNull(arrAviso.Seguradora_id), "NULL", arrAviso.Seguradora_id) & ", "
                        vsql = vsql & arrAviso.Ramo_id & ","
                        vsql = vsql & Detalhamento_id & ",'"         '@detalhamento_id
                        vsql = vsql & Conta_Doctos & "', "           '@linha_id
                        vsql = vsql & "'" & .List(i) & "', "         '@linha
                        vsql = vsql & "'" & gsCPF & "'"           '@usuario
                        rdocn.Execute (vsql)
                       
                       If frmDetalhamento.txtObservacao1.Text <> "" Then
                            vsql = "EXEC sinistro_linha_detalhe_spi "
                            vsql = vsql & arrAviso.Sinistro_id & ","
                            vsql = vsql & IIf(IsNull(arrAviso.Apolice_id), "NULL", arrAviso.Apolice_id) & ", "
                            vsql = vsql & IIf(IsNull(arrAviso.Sucursal_id), "NULL", arrAviso.Sucursal_id) & ", "
                            vsql = vsql & IIf(IsNull(arrAviso.Seguradora_id), "NULL", arrAviso.Seguradora_id) & ", "
                            vsql = vsql & arrAviso.Ramo_id & ","
                            vsql = vsql & Detalhamento_id & ",'"         '@detalhamento_id
                            vsql = vsql & Conta_Doctos + 1 & "', "           '@linha_id
                            vsql = vsql & "'" & frmDetalhamento.txtObservacao1.Text & "', "         '@linha
                            vsql = vsql & "'" & gsCPF & "'"           '@usuario
                            rdocn.Execute (vsql)
                            If frmDetalhamento.txtObservacao2.Text <> "" Then
                                vsql = "EXEC sinistro_linha_detalhe_spi "
                                vsql = vsql & arrAviso.Sinistro_id & ","
                                vsql = vsql & IIf(IsNull(arrAviso.Apolice_id), "NULL", arrAviso.Apolice_id) & ", "
                                vsql = vsql & IIf(IsNull(arrAviso.Sucursal_id), "NULL", arrAviso.Sucursal_id) & ", "
                                vsql = vsql & IIf(IsNull(arrAviso.Seguradora_id), "NULL", arrAviso.Seguradora_id) & ", "
                                vsql = vsql & arrAviso.Ramo_id & ","
                                vsql = vsql & Detalhamento_id & ",'"         '@detalhamento_id
                                vsql = vsql & Conta_Doctos + 2 & "', "           '@linha_id
                                vsql = vsql & "'" & frmDetalhamento.txtObservacao2.Text & "', "         '@linha
                                vsql = vsql & "'" & gsCPF & "'"           '@usuario
                                rdocn.Execute (vsql)
                                If frmDetalhamento.txtObservacao3.Text <> "" Then
                                     vsql = "EXEC sinistro_linha_detalhe_spi "
                                     vsql = vsql & arrAviso.Sinistro_id & ","
                                     vsql = vsql & IIf(IsNull(arrAviso.Apolice_id), "NULL", arrAviso.Apolice_id) & ", "
                                     vsql = vsql & IIf(IsNull(arrAviso.Sucursal_id), "NULL", arrAviso.Sucursal_id) & ", "
                                     vsql = vsql & IIf(IsNull(arrAviso.Seguradora_id), "NULL", arrAviso.Seguradora_id) & ", "
                                     vsql = vsql & arrAviso.Ramo_id & ","
                                     vsql = vsql & Detalhamento_id & ",'"         '@detalhamento_id
                                     vsql = vsql & Conta_Doctos + 3 & "', "           '@linha_id
                                     vsql = vsql & "'" & frmDetalhamento.txtObservacao3.Text & "', "         '@linha
                                     vsql = vsql & "'" & gsCPF & "'"           '@usuario
                                     rdocn.Execute (vsql)
                                End If
                            End If
                       End If
                    End If
                Next
           End With
        ElseIf OTipoAlteracao = "3" Then
           With frmDetalhamento
                For i = 0 To 9
                    If .txtExigencia(i) <> "" Then
                       Conta_Doctos = Conta_Doctos + 1
                       
                       '* GRAVA NO ITEM DO HIST�RICO
                        vsql = "EXEC sinistro_item_historico_spi "
                        vsql = vsql & arrAviso.Sinistro_id & ","
                        'tratamento sem apolice
                        vsql = vsql & IIf(IsNull(arrAviso.Apolice_id), "NULL", arrAviso.Apolice_id) & ", "
                        vsql = vsql & IIf(IsNull(arrAviso.Sucursal_id), "NULL", arrAviso.Sucursal_id) & ", "
                        vsql = vsql & IIf(IsNull(arrAviso.Seguradora_id), "NULL", arrAviso.Seguradora_id) & ", "
                        vsql = vsql & arrAviso.Ramo_id & ","
                        vsql = vsql & OSeq_Evento & ",'"
                        vsql = vsql & MudaAspaSimples(.txtExigencia(i)) & "','"
                        vsql = vsql & gsCPF & "'"
                        rdocn.Execute (vsql)
                       
                        If frmDetalhamento.txtObservacao1.Text <> "" Then
                            vsql = "EXEC sinistro_item_historico_spi "
                            vsql = vsql & arrAviso.Sinistro_id & ","
                            vsql = vsql & IIf(IsNull(arrAviso.Apolice_id), "NULL", arrAviso.Apolice_id) & ", "
                            vsql = vsql & IIf(IsNull(arrAviso.Sucursal_id), "NULL", arrAviso.Sucursal_id) & ", "
                            vsql = vsql & IIf(IsNull(arrAviso.Seguradora_id), "NULL", arrAviso.Seguradora_id) & ", "
                            vsql = vsql & arrAviso.Ramo_id & ","
                            vsql = vsql & CInt(OSeq_Evento) + 1 & ",'"
                            vsql = vsql & MudaAspaSimples(frmDetalhamento.txtObservacao1.Text) & "','"
                            vsql = vsql & gsCPF & "'"
                            rdocn.Execute (vsql)
                            If frmDetalhamento.txtObservacao2.Text <> "" Then
                                vsql = "EXEC sinistro_item_historico_spi "
                                vsql = vsql & arrAviso.Sinistro_id & ","
                                vsql = vsql & IIf(IsNull(arrAviso.Apolice_id), "NULL", arrAviso.Apolice_id) & ", "
                                vsql = vsql & IIf(IsNull(arrAviso.Sucursal_id), "NULL", arrAviso.Sucursal_id) & ", "
                                vsql = vsql & IIf(IsNull(arrAviso.Seguradora_id), "NULL", arrAviso.Seguradora_id) & ", "
                                vsql = vsql & arrAviso.Ramo_id & ","
                                vsql = vsql & CInt(OSeq_Evento) + 2 & ",'"
                                vsql = vsql & MudaAspaSimples(frmDetalhamento.txtObservacao2.Text) & "','"
                                vsql = vsql & gsCPF & "'"
                                rdocn.Execute (vsql)
                                If frmDetalhamento.txtObservacao3.Text <> "" Then
                                     vsql = "EXEC sinistro_item_historico_spi "
                                     vsql = vsql & arrAviso.Sinistro_id & ","
                                     vsql = vsql & IIf(IsNull(arrAviso.Apolice_id), "NULL", arrAviso.Apolice_id) & ", "
                                     vsql = vsql & IIf(IsNull(arrAviso.Sucursal_id), "NULL", arrAviso.Sucursal_id) & ", "
                                     vsql = vsql & IIf(IsNull(arrAviso.Seguradora_id), "NULL", arrAviso.Seguradora_id) & ", "
                                     vsql = vsql & arrAviso.Ramo_id & ","
                                     vsql = vsql & CInt(OSeq_Evento) + 3 & ",'"
                                     vsql = vsql & MudaAspaSimples(frmDetalhamento.txtObservacao3.Text) & "','"
                                     vsql = vsql & gsCPF & "'"
                                     rdocn.Execute (vsql)
                                End If
                            End If
                       End If

                       '* GRAVA NO DETALHAMENTO
                        vsql = "EXEC sinistro_linha_detalhe_spi "
                        vsql = vsql & arrAviso.Sinistro_id & ","
                        'tratamento sem apolice
                        vsql = vsql & IIf(IsNull(arrAviso.Apolice_id), "NULL", arrAviso.Apolice_id) & ", "
                        vsql = vsql & IIf(IsNull(arrAviso.Sucursal_id), "NULL", arrAviso.Sucursal_id) & ", "
                        vsql = vsql & IIf(IsNull(arrAviso.Seguradora_id), "NULL", arrAviso.Seguradora_id) & ", "
                        vsql = vsql & arrAviso.Ramo_id & ","
                        vsql = vsql & Detalhamento_id & ", "         '@detalhamento_id
                        vsql = vsql & "'" & Conta_Doctos & "', "           '@linha_id
                        vsql = vsql & "'" & MudaAspaSimples(.txtExigencia(i)) & "', "    '@linha
                        vsql = vsql & "'" & cUserName & "'"                '@usuario
                        rdocn.Execute (vsql)
                       
                       If frmDetalhamento.txtObservacao1.Text <> "" Then
                            vsql = "EXEC sinistro_linha_detalhe_spi "
                            vsql = vsql & arrAviso.Sinistro_id & ","
                            vsql = vsql & IIf(IsNull(arrAviso.Apolice_id), "NULL", arrAviso.Apolice_id) & ", "
                            vsql = vsql & IIf(IsNull(arrAviso.Sucursal_id), "NULL", arrAviso.Sucursal_id) & ", "
                            vsql = vsql & IIf(IsNull(arrAviso.Seguradora_id), "NULL", arrAviso.Seguradora_id) & ", "
                            vsql = vsql & arrAviso.Ramo_id & ","
                            vsql = vsql & Detalhamento_id & ", "         '@detalhamento_id
                            vsql = vsql & "'" & Conta_Doctos + 1 & "', "           '@linha_id
                            vsql = vsql & "'" & MudaAspaSimples(frmDetalhamento.txtObservacao1.Text) & "', "    '@linha
                            vsql = vsql & "'" & cUserName & "'"                '@usuario
                            rdocn.Execute (vsql)
                            If frmDetalhamento.txtObservacao2.Text <> "" Then
                                vsql = "EXEC sinistro_linha_detalhe_spi "
                                vsql = vsql & arrAviso.Sinistro_id & ","
                                vsql = vsql & IIf(IsNull(arrAviso.Apolice_id), "NULL", arrAviso.Apolice_id) & ", "
                                vsql = vsql & IIf(IsNull(arrAviso.Sucursal_id), "NULL", arrAviso.Sucursal_id) & ", "
                                vsql = vsql & IIf(IsNull(arrAviso.Seguradora_id), "NULL", arrAviso.Seguradora_id) & ", "
                                vsql = vsql & arrAviso.Ramo_id & ","
                                vsql = vsql & Detalhamento_id & ", "         '@detalhamento_id
                                vsql = vsql & "'" & Conta_Doctos + 2 & "', "           '@linha_id
                                vsql = vsql & "'" & MudaAspaSimples(frmDetalhamento.txtObservacao2.Text) & "', "    '@linha
                                vsql = vsql & "'" & cUserName & "'"                '@usuario
                                rdocn.Execute (vsql)
                                If frmDetalhamento.txtObservacao3.Text <> "" Then
                                     vsql = "EXEC sinistro_linha_detalhe_spi "
                                     vsql = vsql & arrAviso.Sinistro_id & ","
                                     vsql = vsql & IIf(IsNull(arrAviso.Apolice_id), "NULL", arrAviso.Apolice_id) & ", "
                                     vsql = vsql & IIf(IsNull(arrAviso.Sucursal_id), "NULL", arrAviso.Sucursal_id) & ", "
                                     vsql = vsql & IIf(IsNull(arrAviso.Seguradora_id), "NULL", arrAviso.Seguradora_id) & ", "
                                     vsql = vsql & arrAviso.Ramo_id & ","
                                     vsql = vsql & Detalhamento_id & ", "         '@detalhamento_id
                                     vsql = vsql & "'" & Conta_Doctos + 3 & "', "           '@linha_id
                                     vsql = vsql & "'" & MudaAspaSimples(frmDetalhamento.txtObservacao3.Text) & "', "    '@linha
                                     vsql = vsql & "'" & cUserName & "'"                '@usuario
                                     rdocn.Execute (vsql)
                                End If
                            End If
                       End If
                    End If
                Next
           End With
        End If
        
    Else
   
        vsql = "EXEC sinistro_historico_spi "
        vsql = vsql & arrAviso.Sinistro_id & ","
        'tratamento sem apolice
        vsql = vsql & IIf(IsNull(arrAviso.Apolice_id), "NULL", arrAviso.Apolice_id) & ", "
        vsql = vsql & IIf(IsNull(arrAviso.Sucursal_id), "NULL", arrAviso.Sucursal_id) & ", "
        vsql = vsql & IIf(IsNull(arrAviso.Seguradora_id), "NULL", arrAviso.Seguradora_id) & ", "
        vsql = vsql & arrAviso.Ramo_id & ","
        vsql = vsql & OEvento_SEGBR_ID & ",'"
        'vSql = vSql & arrAviso.situacao & "',"
        vsql = vsql & Situacao & "', "
        vsql = vsql & IIf(arrAviso.MotivoEncerramento = "", "NULL", "'" & arrAviso.MotivoEncerramento & "'") & ",'"
        vsql = vsql & Format(Data_Sistema, "YYYYMMDD") & "','"
        vsql = vsql & gsCPF & "'"
        Set rsTB = rdocn.OpenResultset(vsql)
        OSeq_Evento = rsTB(0)
        rsTB.Close
        
        vsql = "EXEC sinistro_item_historico_spi "
        vsql = vsql & arrAviso.Sinistro_id & ","
        'tratamento sem apolice
        vsql = vsql & IIf(IsNull(arrAviso.Apolice_id), "NULL", arrAviso.Apolice_id) & ", "
        vsql = vsql & IIf(IsNull(arrAviso.Sucursal_id), "NULL", arrAviso.Sucursal_id) & ", "
        vsql = vsql & IIf(IsNull(arrAviso.Seguradora_id), "NULL", arrAviso.Seguradora_id) & ", "
        vsql = vsql & arrAviso.Ramo_id & ","
        vsql = vsql & OSeq_Evento & ",'"
        If OTipoAlteracao = "1" Then
           vsql = vsql & "CONFIRMA��O DE PAGAMENTO DE RECIBO PELO BB','"
        Else
           vsql = vsql & ADescr_Evento & "','"
        End If
        vsql = vsql & gsCPF & "'"
        rdocn.Execute (vsql)
        
        
        vsql = "EXEC sinistro_item_historico_spi "
        vsql = vsql & arrAviso.Sinistro_id & ","
        'tratamento sem apolice
        vsql = vsql & IIf(IsNull(arrAviso.Apolice_id), "NULL", arrAviso.Apolice_id) & ", "
        vsql = vsql & IIf(IsNull(arrAviso.Sucursal_id), "NULL", arrAviso.Sucursal_id) & ", "
        vsql = vsql & IIf(IsNull(arrAviso.Seguradora_id), "NULL", arrAviso.Seguradora_id) & ", "
        vsql = vsql & arrAviso.Ramo_id & ","
        vsql = vsql & OSeq_Evento & ",'"
        vsql = vsql & "RECIBO N�.: " & arrAviso.Num_Recibo & "','"   'Num_recibo
        vsql = vsql & gsCPF & "'"
        rdocn.Execute (vsql)

        If frmDetalhamento.txtObservacao1.Text <> "" Then
            vsql = "EXEC sinistro_item_historico_spi "
            vsql = vsql & arrAviso.Sinistro_id & ","
            vsql = vsql & IIf(IsNull(arrAviso.Apolice_id), "NULL", arrAviso.Apolice_id) & ", "
            vsql = vsql & IIf(IsNull(arrAviso.Sucursal_id), "NULL", arrAviso.Sucursal_id) & ", "
            vsql = vsql & IIf(IsNull(arrAviso.Seguradora_id), "NULL", arrAviso.Seguradora_id) & ", "
            vsql = vsql & arrAviso.Ramo_id & ","
            vsql = vsql & CInt(OSeq_Evento) + 1 & ",'"
            vsql = vsql & frmDetalhamento.txtObservacao1.Text & "','"
            vsql = vsql & gsCPF & "'"
            rdocn.Execute (vsql)
            If frmDetalhamento.txtObservacao2.Text <> "" Then
                vsql = "EXEC sinistro_item_historico_spi "
                vsql = vsql & arrAviso.Sinistro_id & ","
                vsql = vsql & IIf(IsNull(arrAviso.Apolice_id), "NULL", arrAviso.Apolice_id) & ", "
                vsql = vsql & IIf(IsNull(arrAviso.Sucursal_id), "NULL", arrAviso.Sucursal_id) & ", "
                vsql = vsql & IIf(IsNull(arrAviso.Seguradora_id), "NULL", arrAviso.Seguradora_id) & ", "
                vsql = vsql & arrAviso.Ramo_id & ","
                vsql = vsql & CInt(OSeq_Evento) + 2 & ",'"
                vsql = vsql & frmDetalhamento.txtObservacao2.Text & "','"
                vsql = vsql & gsCPF & "'"
                rdocn.Execute (vsql)
                If frmDetalhamento.txtObservacao3.Text <> "" Then
                    vsql = "EXEC sinistro_item_historico_spi "
                    vsql = vsql & arrAviso.Sinistro_id & ","
                    vsql = vsql & IIf(IsNull(arrAviso.Apolice_id), "NULL", arrAviso.Apolice_id) & ", "
                    vsql = vsql & IIf(IsNull(arrAviso.Sucursal_id), "NULL", arrAviso.Sucursal_id) & ", "
                    vsql = vsql & IIf(IsNull(arrAviso.Seguradora_id), "NULL", arrAviso.Seguradora_id) & ", "
                    vsql = vsql & arrAviso.Ramo_id & ","
                    vsql = vsql & CInt(OSeq_Evento) + 3 & ",'"
                    vsql = vsql & frmDetalhamento.txtObservacao3.Text & "','"
                    vsql = vsql & gsCPF & "'"
                    rdocn.Execute (vsql)
                End If
            End If
        End If

        'Altera a situa��o do recibo na tabela pgto_sinistro_tb
        vsql = "EXEC altera_situacao_recibo_spu "
        vsql = vsql & arrAviso.Sinistro_id & ","
        'tratamento sem apolice
        vsql = vsql & IIf(IsNull(arrAviso.Apolice_id), "NULL", arrAviso.Apolice_id) & ", "
        vsql = vsql & IIf(IsNull(arrAviso.Sucursal_id), "NULL", arrAviso.Seguradora_id) & ", "
        vsql = vsql & IIf(IsNull(arrAviso.Seguradora_id), "NULL", arrAviso.Sucursal_id) & ", "
        vsql = vsql & arrAviso.Ramo_id & ","
        vsql = vsql & arrAviso.Num_Recibo & ",'"
        vsql = vsql & cUserName & "'"
        rdocn.Execute (vsql)
    End If
    
    'Obt�m produto_id
    'myoshimura
    '07/03/2004
    'tratamento sem apolice
    If arrAviso.Proposta_id = 0 Or IsNull(arrAviso.Proposta_id) Then
        OProduto_id = 0
    Else
        vsql = "SELECT produto_id"
        vsql = vsql & " FROM proposta_tb with(nolock) "
        vsql = vsql & " WHERE proposta_id = " & arrAviso.Proposta_id
        
        Set rsTB = rdocn.OpenResultset(vsql)
            OProduto_id = rsTB(0)
        rsTB.Close
    End If
    'SP que grava na tabela evento_SEGBR_sinistro_tb
    vsql = "EXEC evento_SEGBR_sinistro_spi "
    vsql = vsql & arrAviso.Sinistro_id & ","
    'tratamento sem apolice
    vsql = vsql & IIf(IsNull(arrAviso.Apolice_id), "NULL", arrAviso.Apolice_id) & ", "
    vsql = vsql & IIf(IsNull(arrAviso.Sucursal_id), "NULL", arrAviso.Seguradora_id) & ", "
    vsql = vsql & IIf(IsNull(arrAviso.Seguradora_id), "NULL", arrAviso.Sucursal_id) & ", "
    vsql = vsql & IIf(IsNull(arrAviso.Ramo_id), "NULL", arrAviso.Ramo_id) & ", "
    vsql = vsql & OEvento_SEGBR_ID & ",'"
    vsql = vsql & Format(Data_Sistema, "YYYYMMDD") & "',"
    vsql = vsql & "1,'" ' entidade_id (1 = Alian�a do Brasil)
    vsql = vsql & "SEGBR','"
    vsql = vsql & cUserName & "',"
    vsql = vsql & IIf(OProduto_id = 0, "NULL", OProduto_id) & ", " 'Produto
    vsql = vsql & arrAviso.Sinistro_BB & ","
    vsql = vsql & IIf(IsNull(arrAviso.Proposta_id) Or arrAviso.Proposta_id = 0, "NULL", arrAviso.Proposta_id) & ","
    If OEvento_SEGBR_ID = 10100 Then
        vsql = vsql & "NULL "
    Else
        vsql = vsql & IIf(arrAviso.Num_Recibo = "0", "NULL", arrAviso.Num_Recibo)
    End If
    Set rsTB = rdocn.OpenResultset(vsql)
        OEvento = rsTB(0)
    rsTB.Close
    
    Conta_Doctos = 0
    '* Verifica qual o tipo de altera��o para gravar o detalhamento
    If OTipoAlteracao = "2" Then
       With frmDetalhamento.lstDocumentos
            For i = 0 To .ListCount - 1
                If .Selected(i) Then
                     '*****************************************************************
                     'Adicionado por Cleber da Stefanini - data: 24/03/2005
                     If Validador_evento_SEGBR_sinistro_detalhe(.ItemData(i)) Then
            
                         Conta_Doctos = Conta_Doctos + 1
                         vsql = "EXEC evento_SEGBR_sinistro_detalhe_spi  "
                         vsql = vsql & OEvento & ", "             '@evento_id
                         vsql = vsql & "'EXIG�NCIA', "            '@tp_detalhe
                         vsql = vsql & Conta_Doctos & ","         '@seq_detalhe
                         vsql = vsql & "'" & .List(i) & "', "     '@descricao
                         vsql = vsql & "'" & cUserName & "', "    '@usuario
                         vsql = vsql & .ItemData(i)               '@tp_documento_BB_id
                         vsql = vsql & ", NULL"                       '@documento_enviado
                         rdocn.Execute (vsql)
                         
                         bObsDetalhamento = True
             
                     End If
                     '*****************************************************************
              
                End If
            Next
            
            If bObsDetalhamento Then
                If frmDetalhamento.txtObservacao1.Text <> "" Then
                     vsql = "EXEC evento_SEGBR_sinistro_descricao_spi  "
                     vsql = vsql & OEvento & ", "             '@evento_id
                     vsql = vsql & Conta_Doctos + 1 & ","         '@seq_detalhe
                     vsql = vsql & "'" & frmDetalhamento.txtObservacao1.Text & "', "     '@descricao
                     vsql = vsql & "'" & cUserName & "'"    '@usuario
                     rdocn.Execute (vsql)
                     If frmDetalhamento.txtObservacao2.Text <> "" Then
                         vsql = "EXEC evento_SEGBR_sinistro_descricao_spi  "
                         vsql = vsql & OEvento & ", "             '@evento_id
                         vsql = vsql & Conta_Doctos + 2 & ","         '@seq_detalhe
                         vsql = vsql & "'" & frmDetalhamento.txtObservacao2.Text & "', "     '@descricao
                         vsql = vsql & "'" & cUserName & "'"    '@usuario
                         rdocn.Execute (vsql)
                         If frmDetalhamento.txtObservacao3.Text <> "" Then
                              vsql = "EXEC evento_SEGBR_sinistro_descricao_spi  "
                              vsql = vsql & OEvento & ", "             '@evento_id
                              vsql = vsql & Conta_Doctos + 3 & ","         '@seq_detalhe
                              vsql = vsql & "'" & frmDetalhamento.txtObservacao3.Text & "', "     '@descricao
                              vsql = vsql & "'" & cUserName & "'"    '@usuario
                              rdocn.Execute (vsql)
                         End If
                     End If
                End If
            End If
       
       End With
    ElseIf OTipoAlteracao = "3" Then
       With frmDetalhamento
            For i = 0 To 9
                If .txtExigencia(i) <> "" Then
                   Conta_Doctos = Conta_Doctos + 1
                   vsql = "EXEC evento_SEGBR_sinistro_detalhe_spi  "
                   vsql = vsql & OEvento & ", "                 '@evento_id
                   vsql = vsql & "'EXIG�NCIA', "                '@tp_detalhe
                   vsql = vsql & Conta_Doctos & ","             '@seq_detalhe
                   vsql = vsql & "'" & MudaAspaSimples(.txtExigencia(i)) & "', " '@descricao
                   vsql = vsql & "'" & cUserName & "'"          '@usuario
                   vsql = vsql & ", NULL"                       '@tp_documento_BB_id
                   vsql = vsql & ", NULL"                       '@documento_enviado
                   rdocn.Execute (vsql)
                   
                   bObsDetalhamento = True
                   
                End If
            Next
            
            If bObsDetalhamento Then
                If frmDetalhamento.txtObservacao1.Text <> "" Then
                     vsql = "EXEC evento_SEGBR_sinistro_descricao_spi  "
                     vsql = vsql & OEvento & ", "             '@evento_id
                     vsql = vsql & Conta_Doctos + 1 & ","         '@seq_detalhe
                     vsql = vsql & "'" & frmDetalhamento.txtObservacao1.Text & "', "     '@descricao
                     vsql = vsql & "'" & cUserName & "'"    '@usuario
                     rdocn.Execute (vsql)
                     If frmDetalhamento.txtObservacao2.Text <> "" Then
                         vsql = "EXEC evento_SEGBR_sinistro_descricao_spi  "
                         vsql = vsql & OEvento & ", "             '@evento_id
                         vsql = vsql & Conta_Doctos + 2 & ","         '@seq_detalhe
                         vsql = vsql & "'" & frmDetalhamento.txtObservacao2.Text & "', "     '@descricao
                         vsql = vsql & "'" & cUserName & "'"    '@usuario
                         rdocn.Execute (vsql)
                         If frmDetalhamento.txtObservacao3.Text <> "" Then
                              vsql = "EXEC evento_SEGBR_sinistro_descricao_spi  "
                              vsql = vsql & OEvento & ", "             '@evento_id
                              vsql = vsql & Conta_Doctos + 3 & ","     '@seq_detalhe
                              vsql = vsql & "'" & frmDetalhamento.txtObservacao3.Text & "', "     '@descricao
                              vsql = vsql & "'" & cUserName & "'"    '@usuario
                              rdocn.Execute (vsql)
                         End If
                     End If
                End If
            End If
            
       End With
    End If

    Exit Function
    
TrataErro:
    'Grava o Log do Erro
    Grava_Evento_SEGBR = False
    
End Function


Private Function Grava_Evento_LOG(ByVal OTipoAlteracao As String, ByVal OEvento_SEGBR_ID As String, ByVal ADescr_Evento As String) As Boolean
'Private Function Grava_Evento_LOG(ByVal OTipoAlteracao As String, ByVal OSinistro As cSinistro, ByVal OEvento_SEGBR_ID As String, ByVal ADescr_Evento As String) As Boolean
    
    Dim OSeq_Evento     As String, OProduto_id  As String
    Dim Detalhamento_id As String, OEvento      As String
    Dim ASeq_Estimativa As String, Conta_Doctos As Integer
    Dim i               As Integer
    
    Dim bObsDetalhamento As Boolean
    
    bObsDetalhamento = False
    
    On Error GoTo TrataErro
    
    Grava_Evento_LOG = True

    '* Vari�vel para contar a qtde de documentos
    Conta_Doctos = 0

    'With OSinistro
        Detalhamento_id = ""
        '* Obt�m o �ltimo detalhamento_id
        vsql = "SELECT detalhamento_id = isnull(MAX(detalhamento_id),0) + 1"
        vsql = vsql & " FROM  sinistro_detalhamento_LOG_tb with(nolock) "
        vsql = vsql & " WHERE sinistro_BB            = " & arrAviso.Sinistro_BB
        Set rsTB = rdocn.OpenResultset(vsql)
            Detalhamento_id = rsTB!Detalhamento_id
        rsTB.Close
        
        vsql = "SELECT seq_estimativa = isnull(MAX(seq_estimativa),1)"
        vsql = vsql & " FROM  sinistro_historico_LOG_tb with(nolock) "
        vsql = vsql & " WHERE sinistro_BB            = " & arrAviso.Sinistro_BB
        Set rsTB = rdocn.OpenResultset(vsql)
            ASeq_Estimativa = rsTB!Seq_Estimativa
        rsTB.Close
        
        vsql = "EXEC sinistro_historico_LOG_spi "
        vsql = vsql & arrAviso.Sinistro_BB & ", "
        vsql = vsql & OEvento_SEGBR_ID & ", '"
        vsql = vsql & arrAviso.Situacao & "', " 'Situacao
        vsql = vsql & IIf(arrAviso.MotivoEncerramento = "", "NULL", "'" & arrAviso.MotivoEncerramento & "'") & ", '"
        vsql = vsql & Format(Data_Sistema, "YYYYMMDD") & "', '"
        vsql = vsql & cUserName & "', "
        vsql = vsql & ASeq_Estimativa & ", "        '@seq_estimativa
        vsql = vsql & Detalhamento_id & ", "        '@detalhamento_id
        vsql = vsql & "'s' "                        '@evento_visualizado
        Set rsTB = rdocn.OpenResultset(vsql)
            OSeq_Evento = rsTB(0)
        rsTB.Close
        
        vsql = "EXEC sinistro_item_historico_LOG_spi "
        vsql = vsql & arrAviso.Sinistro_BB & ","
        vsql = vsql & OSeq_Evento & ",'"
        vsql = vsql & ADescr_Evento & "','"
        vsql = vsql & cUserName & "'"
        rdocn.Execute (vsql)
        
        vsql = "EXEC sinistro_detalhamento_LOG_spi "
        vsql = vsql & arrAviso.Sinistro_BB & ", "
        vsql = vsql & "'" & Format(Data_Sistema, "YYYYMMDD") & "', "
        vsql = vsql & "0, "                     '@tp_detalhamento
        vsql = vsql & "'n', "                   '@restrito
        vsql = vsql & "'" & cUserName & "'"     '@usuario
        rdocn.Execute (vsql)
        
        Conta_Doctos = Conta_Doctos + 1
        '* Grava o titulo do detalhamento
        vsql = "EXEC sinistro_linha_detalhe_LOG_spi "
        vsql = vsql & arrAviso.Sinistro_BB & ","
        vsql = vsql & Detalhamento_id & ",'"         '@detalhamento_id
        vsql = vsql & Conta_Doctos & "', "           '@linha_id
        vsql = vsql & "'" & ADescr_Evento & "', "    '@linha
        vsql = vsql & "'" & cUserName & "'"          '@usuario
        rdocn.Execute (vsql)
        
        '* Verifica qual o tipo de altera��o para gravar a linha do detalhamento
        If OTipoAlteracao = "2" Then
           With frmDetalhamento.lstDocumentos
                For i = 0 To .ListCount - 1
                    If .Selected(i) Then
                       Conta_Doctos = Conta_Doctos + 1
                       
                       '* GRAVA NO ITEM DO HIST�RICO
                       vsql = "EXEC sinistro_item_historico_LOG_spi "
                       vsql = vsql & arrAviso.Sinistro_BB & ","
                       vsql = vsql & OSeq_Evento & ",'"
                       vsql = vsql & .List(i) & "','"
                       vsql = vsql & cUserName & "'"
                       rdocn.Execute (vsql)
                       
                       '* GRAVA NO DETALHAMENTO
                       vsql = "EXEC sinistro_linha_detalhe_LOG_spi "
                       vsql = vsql & arrAviso.Sinistro_BB & ","
                       vsql = vsql & Detalhamento_id & ",'"         '@detalhamento_id
                       vsql = vsql & Conta_Doctos & "', "           '@linha_id
                       vsql = vsql & "'" & .List(i) & "', "         '@linha
                       vsql = vsql & "'" & cUserName & "'"          '@usuario
                       rdocn.Execute (vsql)
                    End If
                Next
           End With
        ElseIf OTipoAlteracao = "3" Then
           With frmDetalhamento
                For i = 0 To 9
                    If .txtExigencia(i) <> "" Then
                       Conta_Doctos = Conta_Doctos + 1
                       
                       '* GRAVA NO ITEM DO HIST�RICO
                       vsql = "EXEC sinistro_item_historico_LOG_spi "
                       vsql = vsql & arrAviso.Sinistro_BB & ","
                       vsql = vsql & OSeq_Evento & ",'"
                       vsql = vsql & MudaAspaSimples(.txtExigencia(i)) & "','"
                       vsql = vsql & cUserName & "'"
                       rdocn.Execute (vsql)
                       
                       '* GRAVA NO DETALHAMENTO
                       vsql = "EXEC sinistro_linha_detalhe_LOG_spi "
                       vsql = vsql & arrAviso.Sinistro_BB & ","
                       vsql = vsql & Detalhamento_id & ", "         '@detalhamento_id
                       vsql = vsql & "'" & Conta_Doctos & "', "           '@linha_id
                       vsql = vsql & "'" & MudaAspaSimples(.txtExigencia(i)) & "', "    '@linha
                       vsql = vsql & "'" & cUserName & "'"                '@usuario
                       rdocn.Execute (vsql)
                    End If
                Next
           End With
        End If
        
        'SP que grava na tabela evento_SEGBR_sinistro_tb
        vsql = "EXEC evento_SEGBR_sinistro_spi "
        vsql = vsql & "NULL"
        vsql = vsql & ", NULL"
        vsql = vsql & ", NULL"
        vsql = vsql & ", NULL"
        vsql = vsql & ", NULL"
        vsql = vsql & ", " & OEvento_SEGBR_ID
        vsql = vsql & ", '" & Format(Data_Sistema, "YYYYMMDD") & "'"
        vsql = vsql & ", 1"                     'entidade_id (1 = Alian�a do Brasil)
        vsql = vsql & ", 'LOG'"                 'Localiza��o
        vsql = vsql & ",'" & cUserName & "'"    'Usu�rio
        vsql = vsql & ", NULL"                  'Produto
        vsql = vsql & ", " & arrAviso.Sinistro_BB
        vsql = vsql & ", NULL"                  'Proposta ID
        vsql = vsql & ", NULL"                  'Num_Recibo
        Set rsTB = rdocn.OpenResultset(vsql)
            OEvento = rsTB(0)
        rsTB.Close
        
        Conta_Doctos = 0
        '* Verifica qual o tipo de altera��o para gravar o detalhamento
        If OTipoAlteracao = "2" Then
           With frmDetalhamento.lstDocumentos
                For i = 0 To .ListCount - 1
                    If .Selected(i) Then
                            Conta_Doctos = Conta_Doctos + 1
                            vsql = "EXEC evento_SEGBR_sinistro_detalhe_spi "
                            vsql = vsql & OEvento & ", "             '@evento_id
                            vsql = vsql & "'EXIG�NCIA', "            '@tp_detalhe
                            vsql = vsql & Conta_Doctos & ","         '@seq_detalhe
                            vsql = vsql & "'" & .List(i) & "', "     '@descricao
                            vsql = vsql & "'" & cUserName & "', "    '@usuario
                            vsql = vsql & .ItemData(i)               '@tp_documento_BB_id
                            vsql = vsql & ", NULL"                       '@documento_enviado
                            rdocn.Execute (vsql)
                            
                            bObsDetalhamento = True
                    End If
                                        
                Next
                
                If bObsDetalhamento Then
                   If frmDetalhamento.txtObservacao1.Text <> "" Then
                        Conta_Doctos = Conta_Doctos + 1
                        vsql = "EXEC evento_SEGBR_sinistro_descricao_spi "
                        vsql = vsql & OEvento & ", "             '@evento_id
                        vsql = vsql & Conta_Doctos & ","          '@seq_detalhe
                        vsql = vsql & "'" & frmDetalhamento.txtObservacao1.Text & "', "     '@descricao
                        vsql = vsql & "'" & cUserName & "'"    '@usuario
                        rdocn.Execute (vsql)
                        If frmDetalhamento.txtObservacao2.Text <> "" Then
                            Conta_Doctos = Conta_Doctos + 1
                            vsql = "EXEC evento_SEGBR_sinistro_descricao_spi "
                            vsql = vsql & OEvento & ", "             '@evento_id
                            vsql = vsql & Conta_Doctos & ","         '@seq_detalhe
                            vsql = vsql & "'" & frmDetalhamento.txtObservacao2.Text & "', "     '@descricao
                            vsql = vsql & "'" & cUserName & "'"    '@usuario
                            rdocn.Execute (vsql)
                            If frmDetalhamento.txtObservacao3.Text <> "" Then
                                Conta_Doctos = Conta_Doctos + 1
                                vsql = "EXEC evento_SEGBR_sinistro_descricao_spi "
                                vsql = vsql & OEvento & ", "             '@evento_id
                                vsql = vsql & Conta_Doctos & ","         '@seq_detalhe
                                vsql = vsql & "'" & frmDetalhamento.txtObservacao3.Text & "', "     '@descricao
                                vsql = vsql & "'" & cUserName & "'"    '@usuario
                                rdocn.Execute (vsql)
                            End If
                        End If
                   End If
                
                End If
                
                
           End With
        ElseIf OTipoAlteracao = "3" Then
           With frmDetalhamento
                For i = 0 To 9
                    If .txtExigencia(i) <> "" Then
                       Conta_Doctos = Conta_Doctos + 1
                       vsql = "EXEC evento_SEGBR_sinistro_detalhe_spi  "
                       vsql = vsql & OEvento & ", "                 '@evento_id
                       vsql = vsql & "'EXIG�NCIA', "                '@tp_detalhe
                       vsql = vsql & Conta_Doctos & ","             '@seq_detalhe
                       vsql = vsql & "'" & MudaAspaSimples(.txtExigencia(i)) & "', " '@descricao
                       vsql = vsql & "'" & cUserName & "'"          '@usuario
                       vsql = vsql & ", NULL"                       '@tp_documento_BB_id
                       vsql = vsql & ", NULL"                       '@documento_enviado
                       rdocn.Execute (vsql)
                       
                       bObsDetalhamento = True
                       
                    End If
                Next
                
                If bObsDetalhamento Then
                
                    If frmDetalhamento.txtObservacao1.Text <> "" Then
                         Conta_Doctos = Conta_Doctos + 1
                         vsql = "EXEC evento_SEGBR_sinistro_descricao_spi  "
                         vsql = vsql & OEvento & ", "                 '@evento_id
                         vsql = vsql & Conta_Doctos & ","             '@seq_detalhe
                         vsql = vsql & "'" & MudaAspaSimples(frmDetalhamento.txtObservacao1.Text) & "', " '@descricao
                         vsql = vsql & "'" & cUserName & ""          '@usuario
                         rdocn.Execute (vsql)
                         If frmDetalhamento.txtObservacao2.Text <> "" Then
                             Conta_Doctos = Conta_Doctos + 1
                             vsql = "EXEC evento_SEGBR_sinistro_descricao_spi  "
                             vsql = vsql & OEvento & ", "                 '@evento_id
                             vsql = vsql & Conta_Doctos & ","             '@seq_detalhe
                             vsql = vsql & "'" & MudaAspaSimples(frmDetalhamento.txtObservacao2.Text) & "', " '@descricao
                             vsql = vsql & "'" & cUserName & "'"          '@usuario
                             rdocn.Execute (vsql)
                             If frmDetalhamento.txtObservacao3.Text <> "" Then
                                 Conta_Doctos = Conta_Doctos + 1
                                 vsql = "EXEC evento_SEGBR_sinistro_descricao_spi  "
                                 vsql = vsql & OEvento & ", "                 '@evento_id
                                 vsql = vsql & Conta_Doctos & ","             '@seq_detalhe
                                 vsql = vsql & "'" & MudaAspaSimples(frmDetalhamento.txtObservacao3.Text) & "', " '@descricao
                                 vsql = vsql & "'" & cUserName & "'"          '@usuario
                                 rdocn.Execute (vsql)
                             End If
                         End If
                    End If
                
                End If
                
           End With
        End If
    'End With
    
    Exit Function
    
TrataErro:
    'Grava o Log do Erro
    Grava_Evento_LOG = False
    
End Function

Private Function BuscaUltimaSituacaoHistorico(ByVal Sinistro_id As String) As Integer

Dim rs As rdoResultset
Dim SQL As String

On Error GoTo TrataErro

SQL = " SELECT situacao FROM sinistro_historico_tb with(nolock)  WHERE sinistro_id = " & Sinistro_id
SQL = SQL & " AND seq_evento = (SELECT MAX(seq_evento) FROM sinistro_historico_tb with(nolock) WHERE sinistro_id = " & Sinistro_id & ")"

Set rs = rdocn.OpenResultset(SQL)

If Not rs.EOF Then
BuscaUltimaSituacaoHistorico = rs(0)
Else
BuscaUltimaSituacaoHistorico = -1
End If

Exit Function

TrataErro:
BuscaUltimaSituacaoHistorico = -1
End Function

Public Function Validador_evento_SEGBR_sinistro_detalhe(Item_data_list As Integer) As Boolean
'*********************************************************************
'Criada por Cleber da Stefanini - data: 24/03/2005
'Valida a string que pegou do list, pois o funcionamento dele nao estava
'   normal (os clicks est�o se perdendo), por esse motivo foi criado essa
'   fun��o para ser mais uma consist�ncia nos checks que o list possui.
'*********************************************************************
On Error GoTo TrataErro

Validador_evento_SEGBR_sinistro_detalhe = False

If InStr(Str_captura_list_checks, CStr(Item_data_list)) <> 0 Then
    Validador_evento_SEGBR_sinistro_detalhe = True
End If

Exit Function

TrataErro:
    Exit Function

End Function

Public Function Captura_List_Checks() As String
'*********************************************************************
'Criada por Cleber da Stefanini - data: 24/03/2005
'Captura os c�digos dos documentos para no momento de inserir na tabela
'   evento_SEGBR_sinistro_detalhe_tb verificar se vai inserir somente os
'   documentos que forma selecionados, pois esta sendo adicionado
'   documentos que n�o deveriam ser inseridos.
'*********************************************************************

Dim Count_captura As Integer
Dim STR_captura As String

On Error GoTo TrataErro

STR_captura = ""

For Count_captura = 0 To frmDetalhamento.lstDocumentos.ListCount - 1
    If frmDetalhamento.lstDocumentos.Selected(Count_captura) Then
        STR_captura = STR_captura & frmDetalhamento.lstDocumentos.ItemData(Count_captura) & "; "
    End If
Next

Str_captura_list_checks = STR_captura

Exit Function

TrataErro:
    Exit Function

End Function
Public Function ProtocolarExigencia(strSinistroId As String)
Dim objResultset As rdoResultset
Dim objConnection As rdoConnection

On Error GoTo Erro_ERR

    Set objConnection = New rdoConnection
    With objConnection
       .Connect = rdocn.Connect
       .QueryTimeout = rdocn.QueryTimeout
       .CursorDriver = rdocn.CursorDriver
       .EstablishConnection rdDriverNoPrompt
    End With

    Set objResultset = objConnection.OpenResultset("SELECT * FROM exigencia_tb a WITH (NOLOCK) WHERE a.sinistro_id = " & strSinistroId & " AND UPPER(a.situacao) IN ('p','a') ")
    If Not objResultset.EOF Then
        objConnection.Execute "EXEC SEGS9915_SPU " & objResultset!Sinistro_id & ", NULL, " & objResultset!seq_exigencia & ", NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'x'"
    End If
    objResultset.Close
    objConnection.Close

Erro_EXIT:
    Exit Function
Erro_ERR:
    ProtocolarExigencia = False
    GoTo Erro_EXIT
End Function
