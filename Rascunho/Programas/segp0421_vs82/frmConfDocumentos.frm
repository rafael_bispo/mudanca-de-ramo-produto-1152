VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.2#0"; "MSCOMCTL.OCX"
Begin VB.Form frmConfDocumentos 
   Caption         =   "Confirma��o de Recebimento de Documenta��o de Sinistro"
   ClientHeight    =   8760
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   11865
   Icon            =   "frmConfDocumentos.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   8760
   ScaleWidth      =   11865
   Begin VB.CommandButton cmdRecusadoMotivosDiversos 
      Caption         =   "&Recusado por Motivos Diversos"
      Enabled         =   0   'False
      Height          =   555
      Left            =   7965
      TabIndex        =   16
      Top             =   7830
      Width           =   1815
   End
   Begin VB.CommandButton cmdRessalvaDoctosBasicos 
      Caption         =   "Ressalva por falta de Documentos &B�sicos"
      Enabled         =   0   'False
      Height          =   555
      Left            =   5985
      TabIndex        =   15
      Top             =   7830
      Width           =   1815
   End
   Begin VB.CommandButton cmdConfirmarRecebimento 
      Caption         =   "&Confirmar Recebimento de Documentos"
      Enabled         =   0   'False
      Height          =   555
      Left            =   4005
      TabIndex        =   14
      Top             =   7830
      Width           =   1815
   End
   Begin MSFlexGridLib.MSFlexGrid flexSelecao 
      Height          =   5595
      Left            =   135
      TabIndex        =   6
      TabStop         =   0   'False
      Top             =   2115
      Width           =   11595
      _ExtentX        =   20452
      _ExtentY        =   9869
      _Version        =   393216
      Rows            =   1
      Cols            =   19
      WordWrap        =   -1  'True
      HighLight       =   2
      FillStyle       =   1
      MergeCells      =   1
      FormatString    =   $"frmConfDocumentos.frx":0442
   End
   Begin VB.CommandButton btnSair 
      Caption         =   "&Sair"
      Height          =   555
      Left            =   9915
      TabIndex        =   7
      Top             =   7815
      Width           =   1815
   End
   Begin VB.Frame frameCriterios 
      Caption         =   " Crit�rios para Sele��o "
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1890
      Left            =   120
      TabIndex        =   8
      Top             =   120
      Width           =   11595
      Begin VB.ComboBox cboConteudo 
         Height          =   315
         ItemData        =   "frmConfDocumentos.frx":059B
         Left            =   3375
         List            =   "frmConfDocumentos.frx":059D
         Style           =   2  'Dropdown List
         TabIndex        =   3
         Top             =   1350
         Visible         =   0   'False
         Width           =   3765
      End
      Begin VB.TextBox txtConteudo 
         Height          =   315
         Left            =   3360
         TabIndex        =   4
         Top             =   1350
         Width           =   6495
      End
      Begin VB.Frame fraRamo 
         Caption         =   " Ramo "
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1335
         Left            =   240
         TabIndex        =   12
         Top             =   360
         Width           =   2655
         Begin VB.OptionButton optRE 
            Caption         =   "Ramos Elementares"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   495
            Left            =   225
            TabIndex        =   1
            Top             =   720
            Width           =   2250
         End
         Begin VB.OptionButton optVida 
            Caption         =   "Vida"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Left            =   240
            TabIndex        =   0
            Top             =   360
            Value           =   -1  'True
            Width           =   1770
         End
      End
      Begin VB.CommandButton btnPesquisar 
         Caption         =   "&Pesquisar"
         Height          =   375
         Left            =   10080
         TabIndex        =   5
         Top             =   1305
         Width           =   1332
      End
      Begin VB.ComboBox cmbCampo 
         Height          =   315
         ItemData        =   "frmConfDocumentos.frx":059F
         Left            =   3360
         List            =   "frmConfDocumentos.frx":05A1
         Style           =   2  'Dropdown List
         TabIndex        =   2
         Top             =   600
         Width           =   3765
      End
      Begin VB.Label Label1 
         Caption         =   "Pesquisar por:"
         Height          =   255
         Left            =   3360
         TabIndex        =   10
         Top             =   360
         Width           =   1215
      End
      Begin VB.Label Label2 
         Caption         =   "Conte�do:"
         Height          =   255
         Left            =   3360
         TabIndex        =   9
         Top             =   1080
         Width           =   975
      End
   End
   Begin MSComctlLib.StatusBar StatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   11
      Top             =   8475
      Width           =   11865
      _ExtentX        =   20929
      _ExtentY        =   503
      Style           =   1
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSComctlLib.ImageList imglstAcao 
      Left            =   45
      Top             =   7695
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   20
      ImageHeight     =   18
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   2
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmConfDocumentos.frx":05A3
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmConfDocumentos.frx":0B5D
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin VB.Label lblSelecionados 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   120
      TabIndex        =   13
      Top             =   7905
      Width           =   3765
   End
End
Attribute VB_Name = "frmConfDocumentos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'* ********************************************************************************* *'
'* Constantes para utiliza��o do GRID                                                *'
'* ********************************************************************************* *'
Const GRID_SELECAO_SINISTRO_EM_USO = 0
Const GRID_SELECAO_ACAO_ID = 1
Const GRID_SELECAO_ACAO = 2
Const GRID_SELECAO_SINISTRO_ID = 3
Const GRID_SELECAO_SINISTRO_BB = 4
Const GRID_SELECAO_SEGURADO_ID = 5
Const GRID_SELECAO_NOME_SEGURADO = 6
Const GRID_SELECAO_PROPOSTA_BB = 7
Const GRID_SELECAO_PROPOSTA_ID = 8
Const GRID_SELECAO_APOLICE_ID = 9
Const GRID_SELECAO_SUCURSAL_SEGURADORA_ID = 10
Const GRID_SELECAO_SEGURADORA_COD_SUSEP = 11
Const GRID_SELECAO_RAMO_ID = 12
Const GRID_SELECAO_SITUACAO = 13
Const GRID_SELECAO_MOTIVO_ENCERRAMENTO = 14
Const GRID_SELECAO_SEQ_ESTIMATIVA = 15
Const GRID_SELECAO_NUM_RECIBO = 16
Const GRID_SELECAO_EVENTO_SEGBR_ID = 17
Const GRID_SELECAO_COD_REMESSA = 18
Const GRID_SELECAO_LOCK_AVISO = 19
Const GRID_SELECAO_LINHA_SEL = 20

'* ********************************************************************************* *'

Private Function Obtem_Linha_Selecionada() As objAviso
Dim iGrid                       As Integer

For iGrid = 1 To flexSelecao.Rows - 1
    'If flexSelecao.TextMatrix(iGrid, GRID_SELECAO_ACAO_ID) = 1 Then
    If flexSelecao.TextMatrix(iGrid, GRID_SELECAO_LINHA_SEL) = "1" Then
        Obtem_Linha_Selecionada.Apolice_id = flexSelecao.TextMatrix(iGrid, GRID_SELECAO_APOLICE_ID)
        Obtem_Linha_Selecionada.Cod_Remessa_GTR = flexSelecao.TextMatrix(iGrid, GRID_SELECAO_COD_REMESSA)
        Obtem_Linha_Selecionada.evento_SEGBR_id = flexSelecao.TextMatrix(iGrid, GRID_SELECAO_EVENTO_SEGBR_ID)
        Obtem_Linha_Selecionada.Lock_Aviso = flexSelecao.TextMatrix(iGrid, GRID_SELECAO_LOCK_AVISO)
        Obtem_Linha_Selecionada.MotivoEncerramento = flexSelecao.TextMatrix(iGrid, GRID_SELECAO_MOTIVO_ENCERRAMENTO)
        Obtem_Linha_Selecionada.Nome_Segurado = flexSelecao.TextMatrix(iGrid, GRID_SELECAO_NOME_SEGURADO)
        Obtem_Linha_Selecionada.Num_Recibo = flexSelecao.TextMatrix(iGrid, GRID_SELECAO_NUM_RECIBO)
        Obtem_Linha_Selecionada.Proposta_BB = flexSelecao.TextMatrix(iGrid, GRID_SELECAO_PROPOSTA_BB)
        Obtem_Linha_Selecionada.Proposta_id = flexSelecao.TextMatrix(iGrid, GRID_SELECAO_PROPOSTA_ID)
        Obtem_Linha_Selecionada.Ramo_id = flexSelecao.TextMatrix(iGrid, GRID_SELECAO_RAMO_ID)
        Obtem_Linha_Selecionada.Segurado_id = flexSelecao.TextMatrix(iGrid, GRID_SELECAO_SEGURADO_ID)
        Obtem_Linha_Selecionada.Seguradora_id = flexSelecao.TextMatrix(iGrid, GRID_SELECAO_SEGURADORA_COD_SUSEP)
        Obtem_Linha_Selecionada.Seq_Estimativa = flexSelecao.TextMatrix(iGrid, GRID_SELECAO_SEQ_ESTIMATIVA)
        Obtem_Linha_Selecionada.Sinistro_BB = flexSelecao.TextMatrix(iGrid, GRID_SELECAO_SINISTRO_BB)
        Obtem_Linha_Selecionada.Sinistro_id = flexSelecao.TextMatrix(iGrid, GRID_SELECAO_SINISTRO_ID)
        Obtem_Linha_Selecionada.Situacao = flexSelecao.TextMatrix(iGrid, GRID_SELECAO_SITUACAO)
        Obtem_Linha_Selecionada.Sucursal_id = flexSelecao.TextMatrix(iGrid, GRID_SELECAO_SUCURSAL_SEGURADORA_ID)
        'Obtem_Linha_Selecionada.TipoAlteracao = flexSelecao.TextMatrix(iGrid, GRID_SELECAO_)
        Obtem_Linha_Selecionada.Acao_ID = flexSelecao.TextMatrix(iGrid, GRID_SELECAO_ACAO_ID)

        Exit For
    End If
Next

End Function

Private Sub Verifica_Grid()


'With flexSelecao
'     If .Col = 2 Then
'        If .TextMatrix(.RowSel, GRID_SELECAO_ACAO_ID) = "" Then Exit Sub
'        iTipo = .TextMatrix(.RowSel, GRID_SELECAO_ACAO_ID)
'        Call AlteraStatusFlexGrid(Val(iTipo) + 1, True)
'
'        For iSel = 1 To flexSelecao.Rows - 1
'            If flexSelecao.TextMatrix(iSel, GRID_SELECAO_ACAO_ID) = 1 Then
'                If flgSel = False Then
'                    flgSel = True
'                Else
'                    MsgBox "Protocolar somente um registro por vez.", vbInformation, "Aviso..."
'                    'Call AlteraStatusFlexGrid(flexSelecao.TextMatrix(iSel, GRID_SELECAO_ACAO_ID) + 1, False)
'                    Call AlteraStatusFlexGrid(flexSelecao.TextMatrix(.Row, GRID_SELECAO_ACAO_ID) + 1, False)
'                    Exit For
'                End If
'            End If
'            If flexSelecao.CellPicture Is Nothing Then
'                MsgBox "OK"
'            End If
'        Next
'
'        If (Val(iTipo) + 1) = 1 Then Call Habilita_Botoes(.TextMatrix(.RowSel, GRID_SELECAO_SINISTRO_BB), .TextMatrix(.RowSel, GRID_SELECAO_EVENTO_SEGBR_ID))
'     End If
'End With

End Sub

Private Sub BtnSair_Click()
        Unload Me
End Sub

Private Sub btnPesquisar_Click()
            
        Dim Linha As String, Count As Long, sSQL As String, pProposta_BB As String
        Dim Query_Clausula_AND As String, SQL_Query As String
        Dim Sin As New cSinistro, Query_WHERE   As String
        Dim where_sinistroId, where_sinistroBB As String
        
        If Selecionou_Registro = True Then
           If MsgBox("Existem altera��es, deseja executar uma nova pesquisa ?", vbYesNo + vbQuestion + vbDefaultButton2, "!!! Aten��o !!!") = vbNo Then Exit Sub
        End If
        
        txtConteudo = Trim(txtConteudo)
        flexSelecao.Rows = 1
        
        If Not optVida.Value And Not optRE.Value Then
           mensagem_erro 6, "Selecione o ramo a ser pesquisado!"
           txtConteudo.Enabled = True
           cmbCampo.SetFocus
           Exit Sub
        End If
        
        If cmbCampo.Text = "" Then
           mensagem_erro 6, "Informe o crit�rio de pesquisa!"
           txtConteudo.Enabled = True
           cmbCampo.SetFocus
           Exit Sub
        End If
        
        If txtConteudo.Text = "" And txtConteudo.Visible Then
           mensagem_erro 6, "Informe o conte�do a ser pesquisado!"
           txtConteudo.Enabled = True
           txtConteudo.SetFocus
           Exit Sub
        End If
            
        If cboConteudo.Text = "" And cboConteudo.Visible Then
           mensagem_erro 6, "Informe o conte�do a ser pesquisado!"
           txtConteudo.Enabled = True
           txtConteudo.SetFocus
           Exit Sub
        End If
        
        If ((cmbCampo.Text = "Sinistro BB") Or (cmbCampo.Text = "N� do Sinistro")) Then
           If IsNumeric(txtConteudo.Text) = False Then
              mensagem_erro 6, "O conte�do informado n�o � um n�mero!"
              txtConteudo.Enabled = True
              txtConteudo.SetFocus
              Exit Sub
           End If
        End If
        
        'Limpa a vari�vel que conta a quantidade de registros
        Count = 0
        lblSelecionados = ""
        
        'Limpa a cole��o de sinistros
        'Set SinistrosSelecionados = Nothing
        
        Screen.MousePointer = 11
        
        StatusBar1.SimpleText = "Aguarde !!! Procurando registros . . ."
        
        If optVida.Value Then
            vTpRamo = "1"
        Else
            vTpRamo = "2"
        End If
        
        'Obt�m clausula AND e WHERE
        Query_Clausula_AND = ""
        where_sinistroBB = ""
        where_sinistroId = ""
        
        Query_WHERE = EVENTO_SEGBR_AVISO_HOMOLOGADO
        Query_WHERE = Query_WHERE & ", " & EVENTO_SEGBR_EXIGENCIA_PROVIDENCIADA
        
        Select Case cmbCampo.Text
               
               Case "Todos os Encaminhados"
                    'Verifica qual a forma de pesquisa dos Eventos
                    If cboConteudo.ListIndex <> 0 Then 'TODOS
                       Query_WHERE = cboConteudo.ItemData(cboConteudo.ListIndex)
                    End If
                    
               Case "Sinistro BB" 'Sinistro BB
                    If Len(txtConteudo) > 13 Then
                       mensagem_erro 6, "N�mero maior que o permitido!"
                       txtConteudo.Enabled = True
                       txtConteudo.SetFocus
                       Screen.MousePointer = 0
                       Exit Sub
                    End If
                    
                    'R�mulo Ribeiro
                    'Data: 21/09/2010
                    'Demanda 3800584
                    Query_Clausula_AND = Query_Clausula_AND & " AND SN_BB.sinistro_id = (select sinistro_id from sinistro_bb_tb with(nolock) where sinistro_bb = " & txtConteudo & ")"
                    'Query_Clausula_AND = Query_Clausula_AND & " AND d.sinistro_bb = " & txtConteudo
                    
                    where_sinistroBB = txtConteudo
                    
               Case "N� do Sinistro" 'N�mero do Sinistro
                    If Len(txtConteudo) > 11 Then
                       mensagem_erro 6, "N�mero maior que o permitido!"
                       txtConteudo.Enabled = True
                       txtConteudo.SetFocus
                       Screen.MousePointer = 0
                       Exit Sub
                    End If
                    
                    Query_Clausula_AND = Query_Clausula_AND & " AND SN_BB.sinistro_id = " & txtConteudo
                    
                    where_sinistroId = txtConteudo
                    
        End Select
        
        'Tabelas temporarias
        SQL_Query = SQL_Query & Monta_Insert_Temporaria(Query_WHERE, where_sinistroBB, where_sinistroId)
        
        'Monta a Query Geral
        'Query de Documentos
        If (cboConteudo.ListIndex = 0) Or (cboConteudo.ListIndex <> 3) Then
            'R�mulo Ribeiro
            'Data: 21/09/2010
            'Demanda 3800584
            'If cmbCampo.ListIndex <> 2 Then
            '   SQL_Query = Monta_Query_Documento_LOG(Query_WHERE)
            '   SQL_Query = SQL_Query & Query_Clausula_AND
            '   SQL_Query = SQL_Query & " UNION "
            'End If
            SQL_Query = SQL_Query & Monta_Query_Documento(vTpRamo, Query_WHERE)
            SQL_Query = SQL_Query & Query_Clausula_AND & vbCrLf
        End If
        
        If cmbCampo.ListIndex = 0 Then
           
            If (cboConteudo.ListIndex = 0) Or (cboConteudo.ListIndex = 3) Then
               If (cboConteudo.ListIndex = 0) Then
                  SQL_Query = SQL_Query & " UNION "
               End If
               'Query de Recibos
               SQL_Query = SQL_Query & Monta_Query_Recibo(vTpRamo)
               SQL_Query = SQL_Query & Query_Clausula_AND
            End If
           
        Else
        
            SQL_Query = SQL_Query & " UNION " & vbCrLf
            SQL_Query = SQL_Query & Monta_Query_Recibo(vTpRamo)
            SQL_Query = SQL_Query & Query_Clausula_AND & vbCrLf
        
        End If
        
        
        SQL_Query = "set nocount on " & _
                    SQL_Query & _
                    " ORDER BY sinistro_BB, sinistro_id, num_recibo"
        
        Set rs = rdocn.OpenResultset(SQL_Query)
        
        If Not rs.EOF Then
        
            '*********************************************
            'Alterado por Stefanini Consultoria 18/11/2004
            'Substituindo a Classe por Matriz
            Dim iAviso          As Integer
            Dim arrGrid()       As objAviso

            Do Until rs.EOF
                iAviso = iAviso + 1
                ReDim Preserve arrGrid(iAviso)

                If rs!Proposta_id <> "0" Then
                   pProposta_BB = Obtem_Proposta_BB(rs!Proposta_id)
                Else
                   pProposta_BB = "0"
                End If

                arrGrid(iAviso).Lock_Aviso = IIf(rs!Lock_Aviso = "s", "*", "")

                If rs!Sinistro_id = 0 Then
                    arrGrid(iAviso).Sinistro_id = ""
                    arrGrid(iAviso).Sucursal_id = ""
                    arrGrid(iAviso).Sucursal_id = ""
                    arrGrid(iAviso).Seguradora_id = ""
                    arrGrid(iAviso).Ramo_id = ""
                    arrGrid(iAviso).Proposta_id = ""
                Else
                    arrGrid(iAviso).Sinistro_id = rs!Sinistro_id
                    arrGrid(iAviso).Apolice_id = IIf(IsNull(rs!Apolice_id), 0, rs!Apolice_id)
                    arrGrid(iAviso).Sucursal_id = IIf(IsNull(rs!sucursal_seguradora_id), 0, rs!sucursal_seguradora_id)
                    arrGrid(iAviso).Seguradora_id = IIf(IsNull(rs!seguradora_cod_susep), 0, rs!seguradora_cod_susep)
                    arrGrid(iAviso).Ramo_id = rs!Ramo_id
                    arrGrid(iAviso).Proposta_id = IIf(IsNull(rs!Proposta_id), 0, rs!Proposta_id)
                End If
                arrGrid(iAviso).Sinistro_BB = rs!Sinistro_BB
                arrGrid(iAviso).Proposta_BB = pProposta_BB
                arrGrid(iAviso).Segurado_id = rs!Segurado_id
                arrGrid(iAviso).Nome_Segurado = rs!Nome_Segurado
                arrGrid(iAviso).Situacao = rs!Situacao
                arrGrid(iAviso).MotivoEncerramento = ""
                arrGrid(iAviso).Seq_Estimativa = 1
                arrGrid(iAviso).Num_Recibo = rs!Num_Recibo
                arrGrid(iAviso).evento_SEGBR_id = rs!evento_SEGBR_id
                arrGrid(iAviso).Cod_Remessa_GTR = rs!cod_remessa

                flexSelecao.AddItem arrGrid(iAviso).Lock_Aviso & vbTab & "0" & vbTab & "" & vbTab & arrGrid(iAviso).Sinistro_id & vbTab & arrGrid(iAviso).Sinistro_BB & vbTab & arrGrid(iAviso).Segurado_id & vbTab & arrGrid(iAviso).Nome_Segurado & vbTab & arrGrid(iAviso).Proposta_BB & vbTab & arrGrid(iAviso).Proposta_id & vbTab & arrGrid(iAviso).Apolice_id & vbTab & arrGrid(iAviso).Sucursal_id & vbTab & arrGrid(iAviso).Seguradora_id & vbTab & arrGrid(iAviso).Ramo_id & vbTab & arrGrid(iAviso).Situacao & vbTab & arrGrid(iAviso).MotivoEncerramento & vbTab & arrGrid(iAviso).Seq_Estimativa & vbTab & arrGrid(iAviso).Num_Recibo & vbTab & arrGrid(iAviso).evento_SEGBR_id & vbTab & arrGrid(iAviso).Cod_Remessa_GTR & vbTab & arrGrid(iAviso).Lock_Aviso

                rs.MoveNext
                DoEvents
            Loop
            '*********************************************
        End If
        rs.Close
        
        If flexSelecao.Rows = 1 Then
           StatusBar1.SimpleText = "N�o existe(m) item(s) para esta sele��o."
           Call Habilita_Botoes("", "")
           cmbCampo.SetFocus
        Else
           StatusBar1.SimpleText = "Clique na coluna a��o para selecionar o registro e depois clique no bot�o desejado."
        End If
                
        'Raimundo Belas da Silva Junior - GPTI - 12/05/2009
        'Flow 860880
        'Retorna ao lblSelecionados o total de registros encontrados
        lblSelecionados = Str(iAviso) & " Iten(s) Encontrado(s)"
        'Raimundo - Fim
        
        flexSelecao.Refresh
        Screen.MousePointer = 0
        
End Sub

Private Sub cmbCampo_Click()
        
        txtConteudo.Enabled = True
        If Left(cmbCampo.Text, 5) = "Todos" Then
            txtConteudo = ""
            txtConteudo.Visible = False
            cboConteudo.Visible = True
            cboConteudo.ListIndex = 0
            btnPesquisar.SetFocus
        Else
            cboConteudo.Visible = False
            txtConteudo.Visible = True
            txtConteudo.SetFocus
        End If
        StatusBar1.SimpleText = ""
        
End Sub

Private Sub cmdConfirmarRecebimento_Click()

        Dim i As Long, j As Long, iPos As Integer, Count_Remove As Long
        Dim Existe_Registro_Lock As Boolean
        Dim Sin As cSinistro
        Dim intExigenciaId As Integer
        Dim objResultset As rdoResultset
        
        
        'Carrega os dados para o protocolo
        arrAviso = Obtem_Linha_Selecionada()
        
        If Selecionou_Registro = False Then
           mensagem_erro 6, "Nenhuma altera��o foi efetuada !"
           Exit Sub
        End If
        
        If MsgBox("Confirma o Recebimento do(s) Documento(s) ?", vbYesNo + vbQuestion + vbDefaultButton2, "Confirma��o de Documentos") = vbNo Then Exit Sub
        
        Count_Remove = 0
        Existe_Registro_Lock = False
        
        Screen.MousePointer = vbHourglass
        
        For i = 1 To flexSelecao.Rows - 1
            If Val(flexSelecao.TextMatrix(i, GRID_SELECAO_ACAO_ID)) <> 0 Then
               
               'Set Sin = SinistrosSelecionados(i)
               
               'Verifica se o registro que ser� alterado est� sendo usuado por outro usu�rio.
               'If Registro_Lock(Sin) = False Then
               If Registro_Lock() = False Then
               
                  If Existe_Cancelamento(flexSelecao.TextMatrix(i, GRID_SELECAO_SINISTRO_BB), flexSelecao.TextMatrix(i, GRID_SELECAO_COD_REMESSA)) Then
                     MsgBox "N�o foi poss�vel gerar uma confirma��o pois existe um evento de cancelamento para este sinistro !", vbExclamation
                     If (flexSelecao.Rows - 1) = 1 Then
                        flexSelecao.Rows = 1
                        'SinistrosSelecionados.Remove 1
                     Else
                        flexSelecao.RemoveItem i
                        'SinistrosSelecionados.Remove i
                     End If
                     Exit Sub
                  End If
                  
                 'GENJUNIOR - VALIDA��O PARA N�O PERMITIR O ENVIO DE EVENTO 1110 ANTES DO RECEBIMENTO DE DOCUMENTOS
                  If ValidaRecebimentoDocumentoExigencia(flexSelecao.TextMatrix(i, GRID_SELECAO_SINISTRO_ID)) = "N" Then
                        Screen.MousePointer = vbNormal
                        MsgBox "O protocolo n�o pode ser realizado por esta aplica��o, pois n�o foi " & vbNewLine & "identificado o recebimento de evento de documenta��o pelo Banco.", vbInformation
                        Exit Sub
                  End If
                  
                  'If Not AlterarConfirmacaoDocumentos("1", Sin) Then
                  If Not AlterarConfirmacaoDocumentos("1") Then
                     MsgBox "A Confirma��o n�o foi efetuada !!!", vbOKOnly + vbCritical
                     Screen.MousePointer = vbNormal
                     Exit Sub
                  End If
                  
                  ProtocolarExigencia flexSelecao.TextMatrix(i, GRID_SELECAO_SINISTRO_ID)
               Else
                  Existe_Registro_Lock = True
               End If
            End If
        Next
        
        i = i - 1
        For j = i To 1 Step -1
            If Val(flexSelecao.TextMatrix(j, GRID_SELECAO_ACAO_ID)) <> 0 Then
               If (flexSelecao.Rows - 1) = 1 Then
                  flexSelecao.Rows = 1
                  'SinistrosSelecionados.Remove 1
               Else
                  flexSelecao.RemoveItem j
                  'SinistrosSelecionados.Remove j
               End If
               Count_Remove = Count_Remove + 1
            End If
        Next
        
        iPos = InStr(1, lblSelecionados, "I")
        lblSelecionados = Val(Mid(lblSelecionados, 1, iPos - 1)) - (Count_Remove) & " Iten(s) Encontrado(s)"

        'Para recarregar o grid
        Call btnPesquisar_Click

        Screen.MousePointer = 0
        MsgBox "Altera��es efetuadas com sucesso !", vbOKOnly + vbInformation
        If Existe_Registro_Lock = True Then
           MsgBox "Um ou mais sinistros n�o foram alterados " & vbCrLf & "porque est�o sendo usados por outros usu�rios !", vbOKOnly + vbExclamation, "Aten��o"
        End If
        
        Call Habilita_Botoes("", "")
        
End Sub

Private Sub cmdRecusadoMotivosDiversos_Click()
    
    Dim i As Integer, OIndice As Integer
    
    If Selecionou_Registro = False Then
       mensagem_erro 6, "Nenhuma altera��o foi efetuada !"
       Exit Sub
    End If
    
    Screen.MousePointer = vbHourglass

    'Carrega os dados para a recusa
    arrAviso = Obtem_Linha_Selecionada()

    For i = 1 To flexSelecao.Rows - 1
        If Val(flexSelecao.TextMatrix(i, GRID_SELECAO_ACAO_ID)) <> 0 Then
           'Verifica se o registro que ser� alterado est� sendo usuado por outro usu�rio.
           If flexSelecao.TextMatrix(i, GRID_SELECAO_SINISTRO_EM_USO) = "" Then
              OIndice = i
              Exit For
           Else
              MsgBox "Sinistro n�o foi alterado porque est� sendo utilizado por outro usu�rio!", vbOKOnly + vbExclamation, "Aten��o"
              flexSelecao.TextMatrix(i, GRID_SELECAO_ACAO_ID) = 0
              ' Nakata - Flow 198349 - 06/02/2007
              Screen.MousePointer = vbNormal
              Exit Sub
           End If
        End If
    Next
    
    If Existe_Cancelamento(flexSelecao.TextMatrix(OIndice, GRID_SELECAO_SINISTRO_BB), flexSelecao.TextMatrix(OIndice, GRID_SELECAO_COD_REMESSA)) = True Then
       MsgBox "N�o foi poss�vel gerar uma recusa de documentos b�sicos pois existe um evento de cancelamento para este sinistro !", vbExclamation
       Screen.MousePointer = vbNormal
       If (flexSelecao.Rows - 1) = 1 Then
          flexSelecao.Rows = 1
          'SinistrosSelecionados.Remove 1
       Else
          flexSelecao.RemoveItem OIndice
          'SinistrosSelecionados.Remove OIndice
       End If
       Exit Sub
    End If

    Screen.MousePointer = 0

    With frmDetalhamento
         .Tipo_Evento = "1113"
         .Carrega_Dados_Anteriores = (flexSelecao.TextMatrix(flexSelecao.RowSel, 17) = "10117")
         .Indice = OIndice
         .Show vbModal
    End With
    
End Sub

Private Sub cmdRessalvaDoctosBasicos_Click()
    
    Dim i As Integer, OIndice As Integer
    
    If Selecionou_Registro = False Then
       mensagem_erro 6, "Nenhuma altera��o foi efetuada !"
       Exit Sub
    End If

    'Carrega os dados para a ressalva
    arrAviso = Obtem_Linha_Selecionada()

    For i = 1 To flexSelecao.Rows - 1
        If Val(flexSelecao.TextMatrix(i, GRID_SELECAO_ACAO_ID)) <> 0 Then
           
           If flexSelecao.TextMatrix(flexSelecao.RowSel, 17) = "10123" Then
              MsgBox "N�o � poss�vel efetuar uma recusa de documentos b�sicos para um pagamento.", vbOKOnly + vbCritical
              cmdRessalvaDoctosBasicos.Enabled = False
              Exit Sub
           End If
           
           'Verifica se o registro que ser� alterado est� sendo usuado por outro usu�rio.
           If flexSelecao.TextMatrix(i, GRID_SELECAO_SINISTRO_EM_USO) = "" Then
              OIndice = i
              Exit For
           Else
              MsgBox "Sinistro n�o foi alterado porque est� sendo utilizado por outro usu�rio!", vbOKOnly + vbExclamation, "Aten��o"
              flexSelecao.TextMatrix(i, GRID_SELECAO_ACAO_ID) = 0
              ' Nakata - Flow 198349 - 06/02/2007
              Exit Sub
           End If
        End If
    Next
    
    If Existe_Cancelamento(flexSelecao.TextMatrix(OIndice, GRID_SELECAO_SINISTRO_BB), flexSelecao.TextMatrix(OIndice, GRID_SELECAO_COD_REMESSA)) Then
       MsgBox "N�o foi poss�vel gerar uma recusa de documentos b�sicos pois existe um evento de cancelamento para este sinistro !", vbExclamation
       If (flexSelecao.Rows - 1) = 1 Then
          flexSelecao.Rows = 1
          'SinistrosSelecionados.Remove 1
       Else
          flexSelecao.RemoveItem OIndice
          'SinistrosSelecionados.Remove OIndice
       End If
       Exit Sub
    End If
    
    With frmDetalhamento
         .Tipo_Evento = "1112"
         .Sinistro_BB = flexSelecao.TextMatrix(i, GRID_SELECAO_SINISTRO_BB)
         .Indice = OIndice
         .Show vbModal
    End With
    
End Sub

Private Sub flexSelecao_DblClick()

    If flexSelecao.Rows > 1 And optVida.Value = True Then
        If MsgBox("Deseja visualizar a tela ""Outros Avisos"" ?", vbQuestion + vbDefaultButton2 + vbYesNo) = vbYes Then
            frmOutrosAvisos.Show 1
        End If
    End If

End Sub

Private Sub Form_Load()

    glAmbiente_id = 3
 cUserName = "MFRASCA"

    'Abre a conex�o com o SEGBR
    Call Conexao

    '* Abre conex�o com o Banco INTERFACE_DB para atualiza��o da remessa.
    Call Conexao_GTR

    'Habilitar linha depois de inclu�da a seguran�a
    Call Seguranca("SEGP0421", "Confirma��o de Recebimento de Documenta��o de Sinistro")
    frmConfDocumentos.Show

    Me.Caption = "SEGP0421 - Confirma��o de Recebimento de Documenta��o de Sinistro " & Ambiente

    Call CentraFrm(frmConfDocumentos)

    Call MontaFlexGrid
    Call PreencheComboCampo
    Call PreencheComboConteudo
    
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
        
    If Selecionou_Registro = True Then
           If MsgBox("Existem altera��es, deseja sair assim mesmo ?", vbYesNo + vbQuestion + vbDefaultButton2, "!!! Aten��o !!!") = vbNo Then Cancel = -1: Exit Sub
    End If
        
    Call TerminaSEGBR
        
End Sub

Private Sub flexSelecao_Click()

        Dim iTipo As String
        Dim SQL As String
        Dim flgSel              As Boolean
        Dim iSel                As Integer
        Dim contGrid            As Integer
        Dim Sinistro_BB         As String
        Dim evento_SEGBR_id     As String

        flgSel = False
        iSel = 1
        contGrid = 0

        If flexSelecao.Rows = 1 Then Exit Sub
        If flexSelecao.TextMatrix(flexSelecao.RowSel, GRID_SELECAO_SINISTRO_EM_USO) = "*" Then mensagem_erro 6, "Este Sinistro est� sendo atualizado por outro usu�rio!": flexSelecao.SetFocus: Exit Sub

        '-- Flow 5390168 - 16/09/2010 - MFrasca - Verifica��o comentada temporariamente at� o t�rmino das ------
        '-- implementa��es do processo de integra��o com Brasil Assist�ncia ------------------------------------
'        SQL = "EXEC [SISAB013\WEB].integracao_evento_db.dbo.SEGS8450_SPS @sinistro_id=" & flexSelecao.TextMatrix(flexSelecao.Row, GRID_SELECAO_SINISTRO_ID)
'
'        Set rs = rdocn.OpenResultset(SQL)
'
'        If Not rs.EOF Then
'            MsgBox "Opera��o n�o poder� ser realizada, pois o sinistro est� sendo regulado por uma empresa parceira, a qual � respons�vel pelos tramites de documenta��o.", vbExclamation
'            rs.Close
'            Exit Sub
'        End If
        '-------------------------------------------------------------------------------------------------------
        
        'Marca o item selecionado
        If flexSelecao.Col = 2 Then
            If flexSelecao.TextMatrix(flexSelecao.Row, GRID_SELECAO_LINHA_SEL) = "" Then
                flexSelecao.TextMatrix(flexSelecao.Row, GRID_SELECAO_LINHA_SEL) = "1"
                iTipo = 0
            Else
                flexSelecao.TextMatrix(flexSelecao.Row, GRID_SELECAO_LINHA_SEL) = ""
                iTipo = 1
            End If

            Call AlteraStatusFlexGrid(Val(iTipo) + 1, True)
        End If

        'Verifica se foi selecionado mais de um item
        For iSel = 1 To flexSelecao.Rows - 1
            If flexSelecao.TextMatrix(iSel, GRID_SELECAO_LINHA_SEL) <> "" Then
                contGrid = contGrid + 1
            End If
        Next

        'Se foi selecionado mais de uma linha
        'desmarca o ultimo marcado
        If contGrid > 1 Then
            MsgBox "Protocolar somente um registro por vez.", vbInformation, "Aviso..."
            flexSelecao.TextMatrix(flexSelecao.Row, GRID_SELECAO_LINHA_SEL) = ""
            Call AlteraStatusFlexGrid(2, False)
        End If

        'Se encontrar alguma linha
        'checkada habilita os botoes
        If contGrid = 0 Then
            cmdConfirmarRecebimento.Enabled = False
            cmdRessalvaDoctosBasicos.Enabled = False
            cmdRecusadoMotivosDiversos.Enabled = False
        Else
            'Verifica qual linha ficou checkada
            For iSel = 1 To flexSelecao.Rows - 1
                If flexSelecao.TextMatrix(iSel, GRID_SELECAO_LINHA_SEL) <> "" Then
                    Sinistro_BB = flexSelecao.TextMatrix(iSel, GRID_SELECAO_SINISTRO_BB)
                    evento_SEGBR_id = flexSelecao.TextMatrix(iSel, GRID_SELECAO_EVENTO_SEGBR_ID)
                End If
            Next
            'Call Habilita_Botoes(flexSelecao.TextMatrix(flexSelecao.RowSel, GRID_SELECAO_SINISTRO_BB), flexSelecao.TextMatrix(flexSelecao.RowSel, GRID_SELECAO_EVENTO_SEGBR_ID))
            Call Habilita_Botoes(Sinistro_BB, evento_SEGBR_id)
        End If

End Sub

Private Sub txtConteudo_KeyPress(KeyAscii As Integer)
        If KeyAscii = 13 Then
           btnPesquisar_Click
        End If
End Sub

Private Sub PreencheComboCampo()
        
        With cmbCampo
             .Clear
             .ListIndex = -1
             .AddItem "Todos os Encaminhados"
             .AddItem "Sinistro BB"
             .AddItem "N� do Sinistro"
        End With
        
End Sub

Private Sub PreencheComboConteudo()
        
        With cboConteudo
             .Clear
             .ListIndex = -1
             .AddItem "TODOS"
             
             .AddItem "AVISO HOMOLOGADO"
             .ItemData(1) = EVENTO_SEGBR_AVISO_HOMOLOGADO
             
             .AddItem "EXIG�NCIA PROVIDENCIADA - COM ANEXO"
             .ItemData(2) = EVENTO_SEGBR_EXIGENCIA_PROVIDENCIADA
             
             .AddItem "PAGAMENTO DE RECIBO"
             .ItemData(3) = EVENTO_SEGBR_PGTO_DE_RECIBO
        End With
        
End Sub

Private Function Obtem_Proposta_BB(opProposta_id As String) As String
        
        Dim SQL As String
        
        On Error GoTo TrataErro
        
        With rdocn_aux
             .Connect = rdocn.Connect
             .CursorDriver = rdUseNone
             .EstablishConnection rdDriverNoPrompt
        End With
        
        SQL = "SELECT proposta_BB FROM proposta_fechada_tb with(nolock) "
        SQL = SQL & " WHERE proposta_ID = " & opProposta_id
        SQL = SQL & " AND   proposta_BB IS NOT NULL"
        Set rsTB = rdocn_aux.OpenResultset(SQL)
        If rsTB.EOF Then
           rsTB.Close
           SQL = "SELECT proposta_BB FROM proposta_adesao_tb with(nolock) "
           SQL = SQL & " WHERE proposta_ID = " & opProposta_id
           SQL = SQL & " AND   proposta_BB IS NOT NULL"
           Set rsTB = rdocn_aux.OpenResultset(SQL)
           If rsTB.EOF Then
              Obtem_Proposta_BB = "0"
           Else
              Obtem_Proposta_BB = rsTB!Proposta_BB
           End If
           rsTB.Close
        Else
           Obtem_Proposta_BB = rsTB!Proposta_BB
           rsTB.Close
        End If
        rdocn_aux.Close
        
        Exit Function
        
TrataErro:
    mensagem_erro 6, "Conex�o com SEGBR indispon�vel."
    Call TerminaSEGBR
    
End Function

Private Sub MontaFlexGrid()
        
        With flexSelecao
             .Clear
             .Rows = 1
             .Cols = 18
             .RowHeightMin = 250
             .FixedCols = 1
             .FormatString = "   | Id A��o | A��o | N� do Sinistro | N� do Sinistro BB | Segurado ID | Nome do Segurado                                                                    | Proposta BB  | Proposta ID  | Ap�lice ID | Suc. Seg. ID | Seg. Cod. Susep | Ramo ID | Situa��o | Motivo Encerramento | Seq. Estimativa | Num. Recibo | Evento_SEGBR_id | Cod_Remessa | Lock_Aviso | Linha_Sel"
             .ColWidth(GRID_SELECAO_ACAO_ID) = 0
             .ColWidth(GRID_SELECAO_SEGURADO_ID) = 0
             .ColWidth(GRID_SELECAO_APOLICE_ID) = 0
             .ColWidth(GRID_SELECAO_SUCURSAL_SEGURADORA_ID) = 0
             .ColWidth(GRID_SELECAO_SEGURADORA_COD_SUSEP) = 0
             .ColWidth(GRID_SELECAO_RAMO_ID) = 0
             .ColWidth(GRID_SELECAO_SITUACAO) = 0
             .ColWidth(GRID_SELECAO_MOTIVO_ENCERRAMENTO) = 0
             .ColWidth(GRID_SELECAO_SEQ_ESTIMATIVA) = 0
             .ColWidth(GRID_SELECAO_EVENTO_SEGBR_ID) = 0
             .ColWidth(GRID_SELECAO_COD_REMESSA) = 0
             .ColWidth(GRID_SELECAO_LOCK_AVISO) = 0
             .ColWidth(GRID_SELECAO_LINHA_SEL) = 0
        End With
        
End Sub

Private Sub AlteraStatusFlexGrid(mTipoImagem As Byte, flgSel As Boolean)
        
        '--------------------------------------------------------------------'
        ' Altera a Imagem referente ao status da Remessa                     '
        '--------------------------------------------------------------------'
        ' Utiliza o mTipoImagem para Colocar a Figura Correspondente         '
        ' 0-Vazio; 1-Confirmar; 2-Cancelar(N�o � mais utilizado)             '
        '--------------------------------------------------------------------'
        
'        If flgSel = True Then
            If mTipoImagem > 1 Then
               mTipoImagem = 0
'               cmdConfirmarRecebimento.Enabled = False
'               cmdRessalvaDoctosBasicos.Enabled = False
'               cmdRecusadoMotivosDiversos.Enabled = False
            End If
'        Else
'            mTipoImagem = 0
'        End If
        
        With flexSelecao
             If .Row = 0 Then Exit Sub
             .TextMatrix(.RowSel, GRID_SELECAO_ACAO_ID) = mTipoImagem
             .Col = 2
             .CellPictureAlignment = 3
             .Row = .RowSel
             If mTipoImagem = 0 Then
                Set .CellPicture = Nothing
             Else
                Set .CellPicture = imglstAcao.ListImages(mTipoImagem).Picture
             End If
        End With
        
End Sub


Private Function Selecionou_Registro() As Boolean
        
        Dim iGrid               As Integer
        Dim intNumRecibo        As Integer

        iGrid = 1
        Selecionou_Registro = False
        
        If flexSelecao.Rows = 1 Then Exit Function
        
        For iGrid = 1 To flexSelecao.Rows - 1
            If Val(flexSelecao.TextMatrix(iGrid, GRID_SELECAO_ACAO_ID)) <> 0 Then
               Selecionou_Registro = True
                intNumRecibo = CInt(flexSelecao.TextMatrix(iGrid, GRID_SELECAO_NUM_RECIBO))
               Exit For
            End If
        Next
        
'        For iGrid = 1 To UBound(arrAviso)
'            If arrAviso(iGrid).Num_Recibo = intNumRecibo Then
'                '**************************
'                'Guarda a Linha Selecionada
'                posAviso = iGrid
'                '**************************
'                MsgBox "Numero do Recibo: " & arraviso.Num_Recibo
'            End If
'        Next

End Function


Private Function Registro_Lock() As Boolean
'Private Function Registro_Lock(OSinistro As cSinistro) As Boolean
        
        Registro_Lock = False
        
        If arrAviso.Sinistro_id = "" Then Exit Function
        
        vsql = "SELECT lock_aviso FROM sinistro_tb with(nolock) "
        vsql = vsql & " WHERE sinistro_id            = " & arrAviso.Sinistro_id
        'myoshimura
        '07/03/2004
        'tratamento sem ap�lice
        'vSQL = vSQL & " AND   apolice_id             = " & OSinistro.Apolice_id
        'vSQL = vSQL & " AND   sucursal_seguradora_id = " & OSinistro.Sucursal_id
        'vSQL = vSQL & " AND   seguradora_cod_susep   = " & OSinistro.Seguradora_id
        'vSQL = vSQL & " AND   ramo_id                = " & OSinistro.Ramo_id
        Set rsTB = rdocn.OpenResultset(vsql)
        If rsTB(0) = "s" Then
           Registro_Lock = True
        End If
        rsTB.Close
        
End Function

Private Function Monta_Insert_Temporaria(ByVal AQuery_WHERE As String, ByVal Sinistro_BB As String, ByVal Sinistro_id As String) As String

    AQuery_WHERE = AQuery_WHERE & ", 2014"

    vsql = ""
    vsql = vsql & "/* DELETA TABELA TEMPORARIA */ " & vbNewLine
    'vSql = vSql & "BEGIN" & vbNewLine
    vsql = vsql & "IF OBJECT_ID('tempdb..##documentos_recebidos_GTR_tb') IS NOT NULL " & vbNewLine
    vsql = vsql & "     DROP TABLE ##documentos_recebidos_GTR_tb " & vbNewLine & vbNewLine
    
    vsql = vsql & "IF OBJECT_ID('tempdb..##entrada_GTR_registro_tb') IS NOT NULL " & vbNewLine
    vsql = vsql & "     DROP TABLE ##entrada_GTR_registro_tb " & vbNewLine & vbNewLine
    
    vsql = vsql & "DECLARE @sinistro_bb AS NUMERIC(13)" & vbNewLine
    If Sinistro_BB <> "" Then
        vsql = vsql & "SELECT @sinistro_bb = " & Sinistro_BB & vbNewLine & vbNewLine
    ElseIf Sinistro_id <> "" Then
                vsql = vsql & "SELECT @sinistro_bb = sinistro_bb from seguros_db.dbo.sinistro_bb_tb with(nolock) where sinistro_id = " & Sinistro_id & " and dt_fim_vigencia is null " & vbNewLine & vbNewLine
    End If
    
    If glAmbiente_id = 6 Or glAmbiente_id = 7 Then
    
        vsql = vsql & "DECLARE @sqlQuery AS NVARCHAR(4000)" & vbNewLine
        vsql = vsql & "SET @sqlQuery = '" & vbNewLine
        vsql = vsql & "SELECT * into ##documentos_recebidos_GTR_tb FROM openquery(ABSS, ''select " & vbNewLine & _
                                                                                        "d.cod_remessa, d.evento_bb, d.sinistro_bb, d.dt_envio, " & vbNewLine & _
                                                                                        "ER.reg_cd_tip , ER.registro, ER.entrada_GTR_id " & vbNewLine & _
                                                                                        "from INTERFACE_DB.DBO.documentos_recebidos_GTR_tb d (nolock) " & vbNewLine & _
                                                                                        "INNER JOIN INTERFACE_DB.dbo.entrada_GTR_ATUAL_tb E WITH(NOLOCK) " & vbNewLine & _
                                                                                        "   on d.evento_bb = e.evento_bb " & vbNewLine & _
                                                                                        "   and d.sinistro_bb = e.sinistro_bb " & vbNewLine & _
                                                                                        "   AND convert(numeric, E.cod_remessa) = convert(numeric, d.cod_remessa) " & vbNewLine & _
                                                                                        "INNER JOIN INTERFACE_DB..entrada_GTR_registro_ATUAL_tb ER with(nolock) " & vbNewLine & _
                                                                                        "   ON ER.entrada_GTR_id = E.entrada_GTR_id " & vbNewLine & _
                                                                                        "   AND ER.reg_cd_tip in( ''''c'''',''''u'''',''''p'''',''''a'''') " & vbNewLine & _
                                                                                        "WHERE d.evento_BB IN (" & AQuery_WHERE & ") AND d.dt_envio IS NULL AND e.cod_remessa <> '''''''' [SINISTROBB]'')" & vbNewLine & vbNewLine
                                                                                                
        vsql = vsql & "SELECT * into ##entrada_GTR_registro_tb FROM openquery(ABSS, ''select " & vbNewLine & _
                                                                                    "   E.cod_remessa, e.evento_bb, e.sinistro_bb, " & vbNewLine & _
                                                                                    "   ER.reg_cd_tip , ER.registro, ER.entrada_GTR_id " & vbNewLine & _
                                                                                    "from INTERFACE_DB.DBO.documentos_recebidos_GTR_tb d (nolock) " & vbNewLine & _
                                                                                    "INNER JOIN INTERFACE_DB.dbo.entrada_GTR_tb E WITH(NOLOCK) " & vbNewLine & _
                                                                                    "   on d.evento_bb = e.evento_bb " & vbNewLine & _
                                                                                    "   and d.sinistro_bb = e.sinistro_bb " & vbNewLine & _
                                                                                    "INNER JOIN INTERFACE_DB..entrada_GTR_registro_tb ER with(nolock) " & vbNewLine & _
                                                                                    "   ON ER.entrada_GTR_id = E.entrada_GTR_id " & vbNewLine & _
                                                                                    "   AND ER.reg_cd_tip in( ''''c'''',''''u'''',''''p'''',''''a'''') " & vbNewLine & _
                                                                                    "WHERE d.evento_BB IN (" & AQuery_WHERE & ") AND d.dt_envio IS NULL AND e.cod_remessa <> '''''''' [SINISTROBB] '') " & vbNewLine & vbNewLine
                                                                                        
        vsql = vsql & "'" & vbNewLine
        vsql = vsql & "EXEC seguros_db.dbo.sp_executesql @sqlQuery " & vbNewLine & vbNewLine
    Else
    
        vsql = vsql & "select " & vbNewLine & _
                        "   d.cod_remessa, d.evento_bb, d.sinistro_bb, d.dt_envio, " & vbNewLine & _
                        "   ER.reg_cd_tip , ER.registro, ER.entrada_GTR_id " & vbNewLine & _
                        "into ##documentos_recebidos_GTR_tb " & vbNewLine & _
                        "from INTERFACE_DB.DBO.documentos_recebidos_GTR_tb d (nolock) " & vbNewLine & _
                        "INNER JOIN INTERFACE_DB.dbo.entrada_GTR_ATUAL_tb E WITH(NOLOCK) " & vbNewLine & _
                        "   on d.evento_bb = e.evento_bb " & vbNewLine & _
                        "   and d.sinistro_bb = e.sinistro_bb " & vbNewLine & _
                        "   AND convert(numeric, E.cod_remessa) = convert(numeric, d.cod_remessa) " & vbNewLine & _
                        "INNER JOIN INTERFACE_DB..entrada_GTR_registro_ATUAL_tb ER with(nolock) " & vbNewLine & _
                        "   ON ER.entrada_GTR_id = E.entrada_GTR_id " & vbNewLine & _
                        "   AND ER.reg_cd_tip in( 'c','u','p','a') " & vbNewLine & _
                        "WHERE d.evento_BB IN (" & AQuery_WHERE & ") AND d.dt_envio IS NULL AND e.cod_remessa <> '' [SINISTROBB] " & vbNewLine & vbNewLine
                        
        vsql = vsql & "select " & vbNewLine & _
                        "   E.cod_remessa, e.evento_bb, e.sinistro_bb, " & vbNewLine & _
                        "   ER.reg_cd_tip , ER.registro, ER.entrada_GTR_id " & vbNewLine & _
                        "into ##entrada_GTR_registro_tb " & vbNewLine & _
                        "from INTERFACE_DB.DBO.documentos_recebidos_GTR_tb d (nolock) " & vbNewLine & _
                        "INNER JOIN INTERFACE_DB.dbo.entrada_GTR_tb E WITH(NOLOCK) " & vbNewLine & _
                        "   on d.evento_bb = e.evento_bb " & vbNewLine & _
                        "   and d.sinistro_bb = e.sinistro_bb " & vbNewLine & _
                        "INNER JOIN INTERFACE_DB..entrada_GTR_registro_tb ER with(nolock) " & vbNewLine & _
                        "   ON ER.entrada_GTR_id = E.entrada_GTR_id " & vbNewLine & _
                        "   AND ER.reg_cd_tip in( 'c','u','p','a') " & vbNewLine & _
                        "WHERE d.evento_BB IN (" & AQuery_WHERE & ") AND d.dt_envio IS NULL AND e.cod_remessa <> '' [SINISTROBB] " & vbNewLine & vbNewLine
    
    End If
    
    'vSql = vSql & "END" & vbNewLine
    
    
    If Sinistro_BB <> "" Or Sinistro_id <> "" Then
        If glAmbiente_id = 6 Or glAmbiente_id = 7 Then
            vsql = Replace(vsql, "[SINISTROBB]", " AND d.sinistro_bb = ' + CONVERT(VARCHAR(13), @sinistro_bb) + ' ")
        Else
            vsql = Replace(vsql, "[SINISTROBB]", " AND d.sinistro_bb = @sinistro_bb")
        End If
    Else
        vsql = Replace(vsql, "[SINISTROBB]", "")
    End If
    
    
    
    Monta_Insert_Temporaria = vsql
    

End Function

Private Function Monta_Query_Documento(ByVal TP_RAMO As String, ByVal AQuery_WHERE As String) As String
        Dim vNmlinkedserver As String
        
        vsql = "SELECT DISTINCT sinistro_id = SN.sinistro_id, sinistro_BB = SN_BB.sinistro_bb, " & vbCrLf
        If TP_RAMO = "1" Then       'VIDA
           vsql = vsql & " segurado_id=convert(varchar, SN_VD.cpf), nome_segurado=SN_VD.nome," & vbCrLf
        ElseIf TP_RAMO = "2" Then   'RE
           vsql = vsql & " segurado_id=convert(varchar, CLI.cliente_id), nome_segurado=CLI.nome," & vbCrLf
        End If
        vsql = vsql & " SN.proposta_id, SN.apolice_id, SN.sucursal_seguradora_id, " & vbCrLf
        vsql = vsql & " SN.seguradora_cod_susep, SN.ramo_id, SN.situacao, SN.lock_aviso, " & vbCrLf
        
        'Anderson Fiuza FLOW:1391554 21/10/2009 Adicionado o campo reg_cd_tip = 'a'
        'O processo estava sempre pegando recibo =0
        
        'vSql = vSql & " num_recibo = case ER.reg_cd_tip when 'c' then convert(int, SUBSTRING(ER.registro, 26, 2)) WHEN   'A' THEN SUBSTRING(er.registro,44,2) " & vbCrLf
        vsql = vsql & " num_recibo = case d.reg_cd_tip when 'c' then convert(int, SUBSTRING(d.registro, 26, 2)) WHEN   'A' THEN SUBSTRING(d.registro,44,2) " & vbCrLf
        'vSql = vSql & " else convert(int, SUBSTRING(ER.registro, 53, 2)) end," & vbCrLf
        vsql = vsql & " else convert(int, SUBSTRING(d.registro, 53, 2)) end," & vbCrLf
        'vSQL = vSQL & " num_recibo = convert(int, SUBSTRING(ER.registro, 26, 2)), "
        vsql = vsql & " EV.evento_SEGBR_id, EV.Descricao, d.cod_remessa" & vbCrLf
        'ALTERACAO TABELA TEMPORARIA
        
        'vSql = vSql & " FROM INTERFACE_DB..documentos_recebidos_GTR_tb d  with(nolock) " & vbCrLf
        'vSql = vSql & "     INNER JOIN INTERFACE_DB..entrada_GTR_ATUAL_tb E  with(nolock) " & vbCrLf
        'vSql = vSql & "         ON (convert(numeric, E.cod_remessa) = convert(numeric, d.cod_remessa)" & vbCrLf
        '**********************************************************
        'linha abaixo inclu�da por Stefanini Consultoria, 01/09/2004, solicitado por M�rcio
        'para garantir que o inner join retornar� somente um registro da tabela entrada_gtr
        'vSql = vSql & "         AND d.evento_bb = e.evento_bb" & vbCrLf
        '**********************************************************
        'Inclu�do por Gustavo Machado em 02/04/2004
        'Para garantir que vai selecionar o sinistro_bb correto
        'Pois estavam sendo gerados 1110 com recibos incorretos
        'vSql = vSql & "         AND d.sinistro_bb = e.sinistro_bb)" & vbCrLf
        'vSql = vSql & "     INNER JOIN INTERFACE_DB..entrada_GTR_registro_ATUAL_tb ER  with(nolock) " & vbCrLf
        'vSql = vSql & "         ON (ER.entrada_GTR_id            = E.entrada_GTR_id" & vbCrLf
        'vSQL = vSQL & "         AND ER.reg_cd_tip            = 'c')"
        'Anderson Fiuza adicionado o parametro = 'a'
        'vSql = vSql & "         AND ER.reg_cd_tip            in( 'c','u','p','a'))" & vbCrLf
        
        vsql = vsql & "     FROM ##documentos_recebidos_GTR_tb d "
        'ALTERACAO TABELA TEMPORARIA
        vsql = vsql & "     INNER JOIN sinistro_bb_tb SN_BB     with(nolock) " & vbCrLf '(Index = PK_sinistro_bb)"
        vsql = vsql & "         ON (SN_BB.sinistro_BB               = d.sinistro_BB)" & vbCrLf
        'vSQL = vSQL & "         AND SN_BB.dt_fim_vigencia           IS NULL)"
        vsql = vsql & "     INNER JOIN sinistro_tb SN           with(nolock) " & vbCrLf '(Index = PK_sinistro)"
        vsql = vsql & "         ON (SN_BB.sinistro_id                = SN.sinistro_id)" & vbCrLf
        'vSQL = vSQL & "         AND SN_BB.apolice_id                 = SN.apolice_id"
        'vSQL = vSQL & "         AND SN_BB.seguradora_cod_susep       = SN.seguradora_cod_susep"
        'vSQL = vSQL & "         AND SN_BB.sucursal_seguradora_id     = SN.sucursal_seguradora_id"
        'vSQL = vSQL & "         AND SN_BB.ramo_id                    = SN.ramo_id)"
        vsql = vsql & "         AND SN.situacao <> 7"  'Demanda 3105507 - Diogo Ferreira 06/09/2010 - Segregacao AB/ABS
        vsql = vsql & "     LEFT JOIN proposta_tb PP           with(nolock) " & vbCrLf '(Index = PK_proposta)"
        vsql = vsql & "         ON (PP.proposta_id                   = SN.proposta_id)" & vbCrLf
        If TP_RAMO = "1" Then       'VIDA
           vsql = vsql & "      LEFT JOIN sinistro_vida_tb SN_VD   with(nolock) " & vbCrLf '(Index = PK_sinistro_vida)"
           vsql = vsql & "         ON (SN_BB.sinistro_id                = SN_VD.sinistro_id)" & vbCrLf
           'vSQL = vSQL & "         AND SN_BB.apolice_id                 = SN_VD.apolice_id"
           'vSQL = vSQL & "         AND SN_BB.seguradora_cod_susep       = SN_VD.seguradora_cod_susep"
           'vSQL = vSQL & "         AND SN_BB.sucursal_seguradora_id     = SN_VD.sucursal_seguradora_id"
           'vSQL = vSQL & "         AND SN_BB.ramo_id                    = SN_VD.ramo_id)"
        ElseIf TP_RAMO = "2" Then   'RE
           vsql = vsql & "     LEFT JOIN cliente_tb CLI       with(nolock) " & vbCrLf    '(Index = PK_cliente)"
           vsql = vsql & "         ON (CLI.cliente_id                   = SN.cliente_id)"
        End If
        vsql = vsql & "     INNER JOIN ramo_tb RAMO             with(nolock) " & vbCrLf   '(index = PK_ramo)"
        vsql = vsql & "         ON (SN.ramo_id                       = RAMO.ramo_id)" & vbCrLf
        vsql = vsql & "     INNER JOIN evento_SEGBR_tb EV       with(nolock) " & vbCrLf   '(index = PK_evento_SEGBR)"
        vsql = vsql & "         ON (d.evento_BB                      = EV.evento_BB_id)" & vbCrLf
        vsql = vsql & "     WHERE   d.evento_BB                      IN (" & AQuery_WHERE & ") " & vbCrLf
        
        'R�mulo Ribeiro
        'Data:09/06/2010
         vsql = vsql & "       AND  SN_BB.dt_fim_vigencia          IS NULL" & vbCrLf
        
        'myoshimura
        '20/02/2004
        'se tiver um evento de cancelamento do n�o protocolo tem que fazer o protocolo novamente
        'Alterado por Gustavo Machado em 31/03/2004
        vsql = vsql & "       AND  d.dt_envio                       IS NULL" & vbCrLf
'        vSQL = vSQL & "   AND  (d.dt_envio            IS NULL"
'        vSQL = vSQL & "   OR (select count(*) from evento_segbr_sinistro_tb where sinistro_bb = d.sinistro_bb and evento_bb_id in(1200,1201)) <> 0) "

        'Demanda: 214723 - Alexandre Queiroz (STF) 19/06/2007
        vsql = vsql & " AND (SELECT ISNULL(SITUACAO_SINISTRO,0) FROM EVENTO_SEGBR_SINISTRO_TB with(nolock) " & vbCrLf
        vsql = vsql & "      WHERE EVENTO_ID = (SELECT MAX(EVENTO_ID) FROM EVENTO_SEGBR_SINISTRO_TB with(nolock) " & vbCrLf
        vsql = vsql & "                         WHERE SINISTRO_BB = D.SINISTRO_BB )) <> 3 " & vbCrLf
        
        'Demanda: 211964 - Alexandre Queiroz (STF) 04/05/2007
        'n�o exibir no grid sinistros j� encerrados: (1140/1160)
        'vSql = vSql & " AND (SELECT COUNT(*) FROM EVENTO_SEGBR_SINISTRO_TB "
        'vSql = vSql & " Where Sinistro_BB = d.Sinistro_BB "
        'vSql = vSql & " AND EVENTO_BB_ID IN(1140,1160) )  = 0 "

        'MFrasca - 16794492 - Ajuste para contemplar produto 1218 - Microsseguro que � vida mas tem ramo RE
        'vSql = vSql & "       AND   RAMO.tp_ramo_id                  = " & vTpRamo
        If vTpRamo = 1 Then
            vsql = vsql & "       AND   ((RAMO.tp_ramo_id                  = " & vTpRamo
            vsql = vsql & "               and pp.produto_id not in(1226, 1227)) OR PP.produto_id = 1218 )" & vbCrLf
        Else
            vsql = vsql & "       AND   ((RAMO.tp_ramo_id                  = " & vTpRamo
            vsql = vsql & "              AND PP.produto_id <> 1218 )or pp.produto_id in(1226, 1227))" & vbCrLf
        End If
        'MFrasca - Fim
        
        'Demanda - 18335755 - Bloqueio de eventos de protocolo (Firstone)
        'Ajuste para que os sinistros dos produtos integrados e ativos que foram avisados a partir
        'da data de integra��o do produto, n�o sejam exibidos no programa.
        Select Case glAmbiente_id
               Case "2", "3"
                  vNmlinkedserver = "[SISAB013\WEB]."
               Case "6", "7"
                  vNmlinkedserver = "[SISAS013\WEB_ABS]."
               Case Else
                  vNmlinkedserver = ""
        End Select
        vsql = vsql & "       AND NOT EXISTS ( SELECT 1 " & vbCrLf
        vsql = vsql & "                          FROM " & vNmlinkedserver & "integracao_evento_db.dbo.produto_integracao_tb produto_integracao with(nolock)" & vbCrLf
        vsql = vsql & "                         WHERE pp.produto_id = produto_integracao.produto_id " & vbCrLf
        vsql = vsql & "                           AND   produto_integracao.ativo = 'S' " & vbCrLf
        vsql = vsql & "                           AND   (dt_implantacao IS NULL OR sn.dt_aviso_sinistro >= dt_implantacao ) " & vbCrLf
        vsql = vsql & "                           AND   parceiro_id = 1)"
                
        Monta_Query_Documento = vsql
        
End Function

Private Function Monta_Query_Recibo(ByVal TP_RAMO As String) As String
        Dim vNmlinkedserver As String
        
        
        vsql = "SELECT sinistro_id = SN.sinistro_id, sinistro_BB = SN_BB.sinistro_bb, " & vbCrLf
        If TP_RAMO = "1" Then       'VIDA
           vsql = vsql & " segurado_id=convert(varchar, SN_VD.cpf), nome_segurado=SN_VD.nome," & vbCrLf
        ElseIf TP_RAMO = "2" Then   'RE
           vsql = vsql & " segurado_id=convert(varchar, CLI.cliente_id), nome_segurado=CLI.nome," & vbCrLf
        End If
        vsql = vsql & " SN.proposta_id, SN.apolice_id, SN.sucursal_seguradora_id, " & vbCrLf
        vsql = vsql & " SN.seguradora_cod_susep, SN.ramo_id, SN.situacao, " & vbCrLf
        vsql = vsql & " SN.lock_aviso, num_recibo=PG.num_recibo, EV.evento_SEGBR_id, EV.Descricao, d.cod_remessa" & vbCrLf
        'ALTERACAO TABELA TEMPORARIA
        
        'vSql = vSql & " FROM    INTERFACE_DB..documentos_recebidos_GTR_tb d with(nolock) " & vbCrLf
        vsql = vsql & " FROM ##documentos_recebidos_GTR_tb /*INTERFACE_DB..documentos_recebidos_GTR_tb*/ d with(nolock) " & vbCrLf
        
        'ALTERACAO TABELA TEMPORARIA
        vsql = vsql & "     INNER JOIN sinistro_bb_tb SN_BB     with(nolock) " & vbCrLf  '(Index = PK_sinistro_bb)"
        vsql = vsql & "         ON (SN_BB.sinistro_BB               = d.sinistro_BB)" & vbCrLf
        '*******************************************************************************************
        ' ALTERA��O FEITA POR GUSTAVO MACHADO EM 12/05/2003 CONFORME SOLICITA��O DE JUNIOR RICARDO
        '*******************************************************************************************
        ' vSQL = vSQL & "         AND SN_BB.dt_fim_vigencia           IS NULL)"
        vsql = vsql & "     INNER JOIN sinistro_tb SN           with(nolock)" & vbCrLf    '(Index = PK_sinistro)"
        vsql = vsql & "         ON (SN_BB.sinistro_id                = SN.sinistro_id" & vbCrLf
        vsql = vsql & "         AND SN_BB.apolice_id                 = SN.apolice_id" & vbCrLf
        vsql = vsql & "         AND SN_BB.seguradora_cod_susep       = SN.seguradora_cod_susep" & vbCrLf
        vsql = vsql & "         AND SN_BB.sucursal_seguradora_id     = SN.sucursal_seguradora_id" & vbCrLf
        vsql = vsql & "         AND SN_BB.ramo_id                    = SN.ramo_id)" & vbCrLf
        vsql = vsql & "         AND SN.situacao <> 7"  'Demanda 3105507 - Diogo Ferreira 06/09/2010 - Segregacao AB/ABS
        vsql = vsql & "     INNER JOIN pgto_sinistro_tb PG           with(nolock)" & vbCrLf   '(Index = PK_pgto_sinistro)"
        vsql = vsql & "         ON (SN_BB.sinistro_id                = PG.sinistro_id" & vbCrLf
        vsql = vsql & "         AND SN_BB.apolice_id                 = PG.apolice_id" & vbCrLf
        vsql = vsql & "         AND SN_BB.seguradora_cod_susep       = PG.seguradora_cod_susep" & vbCrLf
        vsql = vsql & "         AND SN_BB.sucursal_seguradora_id     = PG.sucursal_seguradora_id" & vbCrLf
        vsql = vsql & "         AND SN_BB.ramo_id                    = PG.ramo_id" & vbCrLf
        vsql = vsql & "         AND PG.retorno_recibo_bb             = 'p'" & vbCrLf
        vsql = vsql & "         AND PG.item_val_estimativa           = 1" & vbCrLf
        vsql = vsql & "         AND PG.situacao_op                   = 'a'" & vbCrLf
        vsql = vsql & "         AND PG.acerto_id                     IS NOT NULL" & vbCrLf
        vsql = vsql & "         AND (PG.sol_estorno_SISBB             <> 's'" & vbCrLf
        vsql = vsql & "          OR PG.sol_estorno_SISBB             IS NULL))" & vbCrLf
        vsql = vsql & "     INNER JOIN proposta_tb PP           with(nolock) " & vbCrLf    '(Index = PK_proposta)"
        vsql = vsql & "         ON (PP.proposta_id                   = SN.proposta_id)" & vbCrLf
        If TP_RAMO = "1" Then       'VIDA
           vsql = vsql & "      INNER JOIN sinistro_vida_tb SN_VD   with(nolock) " & vbCrLf   '(Index = PK_sinistro_vida)"
           vsql = vsql & "         ON (SN_BB.sinistro_id                = SN_VD.sinistro_id" & vbCrLf
           vsql = vsql & "         AND SN_BB.apolice_id                 = SN_VD.apolice_id" & vbCrLf
           vsql = vsql & "         AND SN_BB.seguradora_cod_susep       = SN_VD.seguradora_cod_susep" & vbCrLf
           vsql = vsql & "         AND SN_BB.sucursal_seguradora_id     = SN_VD.sucursal_seguradora_id" & vbCrLf
           vsql = vsql & "         AND SN_BB.ramo_id                    = SN_VD.ramo_id)" & vbCrLf
        ElseIf TP_RAMO = "2" Then   'RE
           vsql = vsql & "     INNER JOIN cliente_tb CLI       with(nolock) " & vbCrLf    '(Index = PK_cliente)"
           vsql = vsql & "         ON (CLI.cliente_id                   = SN.cliente_id)" & vbCrLf
        End If
        vsql = vsql & "     INNER JOIN ramo_tb RAMO             with(nolock) " & vbCrLf   '(index = PK_ramo)"
        vsql = vsql & "         ON (SN.ramo_id                       = RAMO.ramo_id)" & vbCrLf
        vsql = vsql & "     INNER JOIN evento_SEGBR_tb EV       with(nolock) " & vbCrLf   '(index = PK_evento_SEGBR)"
        vsql = vsql & "         ON (d.evento_BB                      = EV.evento_BB_id)" & vbCrLf
        
        'ALTERACAO TABELA TEMPORARIA
        
        ''INC000005010688 - Cleber Sardo - Lentid�o na base ABS - 15/07/2016
        'If glAmbiente_id = 6 Or glAmbiente_id = 7 Then
        'vSql = vSql & "     INNER JOIN ABSS.INTERFACE_DB.dbo.entrada_GTR_tb e with(nolock)" & vbCrLf
        'Else
        'vSql = vSql & "     INNER JOIN INTERFACE_DB..entrada_GTR_tb e with(nolock)" & vbCrLf
        'End If
        'vSql = vSql & "         ON (e.sinistro_BB = d.sinistro_BB" & vbCrLf
        ''**********************************************************
        ''linha abaixo inclu�da por Stefanini Consultoria, 01/09/2004, solicitado por M�rcio
        ''para garantir que o inner join retornar� somente um registro da tabela entrada_gtr
        'vSql = vSql & "         AND e.evento_bb = d.evento_bb" & vbCrLf
        ''**********************************************************
        'vSql = vSql & "         AND convert(numeric, e.cod_remessa) = convert(numeric, d.cod_remessa))" & vbCrLf
        ''INC000005010688 - Cleber Sardo - Lentid�o na base ABS - 15/07/2016
        'If glAmbiente_id = 6 Or glAmbiente_id = 7 Then
        'vSql = vSql & "     INNER JOIN ABSS.INTERFACE_DB.dbo.entrada_GTR_registro_tb er  with(nolock)" & vbCrLf
        'Else
        'vSql = vSql & "     INNER JOIN INTERFACE_DB..entrada_GTR_registro_tb er  with(nolock)" & vbCrLf
        'End If
        'vSql = vSql & "         ON  (er.entrada_GTR_id                               = e.entrada_GTR_id" & vbCrLf
        
        vsql = vsql & "     INNER JOIN ##entrada_GTR_registro_tb er " & vbCrLf
        vsql = vsql & "         ON ((( er.reg_cd_tip                                   = 'c'" & vbCrLf
        
        'ALTERACAO TABELA TEMPORARIA
        'vSql = vSql & "         AND ((er.reg_cd_tip                                   = 'c'" & vbCrLf
        vsql = vsql & "         AND convert(tinyint, SUBSTRING(er.registro, 26, 2)) = PG.num_recibo)" & vbCrLf
        vsql = vsql & "         OR  (er.reg_cd_tip                                   = 'u'" & vbCrLf
        vsql = vsql & "         AND convert(tinyint, SUBSTRING(er.registro, 53, 2)) = PG.num_recibo)" & vbCrLf
         '*** Incluida para contemplar o registro A para o evento 2014 do ALS - inicio
         vsql = vsql & "        OR  (er.reg_cd_tip                                   = 'a'" & vbCrLf
        vsql = vsql & "         AND convert(tinyint, SUBSTRING(er.registro, 44, 2)) = PG.num_recibo)))" & vbCrLf
         '*** Incluida para contemplar o evento A para o evento 2014 do ALS - fim
         
        vsql = vsql & "     WHERE   d.evento_BB                      = 2014" & vbCrLf
        'Alterado por Gustavo Machado em 31/03/2004
        vsql = vsql & "       AND   d.dt_envio                       IS NULL" & vbCrLf
        
       'R�mulo
        'Data:10/06/2010
        vsql = vsql & "       AND   SN_BB.dt_fim_vigencia                       IS NULL" & vbCrLf
        
       'Demanda: 214723 - Alexandre Queiroz (STF) 19/06/2007
        vsql = vsql & " AND (SELECT ISNULL(SITUACAO_SINISTRO,0) FROM EVENTO_SEGBR_SINISTRO_TB with(nolock) " & vbCrLf
        vsql = vsql & "      WHERE EVENTO_ID = (SELECT MAX(EVENTO_ID) FROM EVENTO_SEGBR_SINISTRO_TB with(nolock) " & vbCrLf
        vsql = vsql & "                         WHERE SINISTRO_BB = D.SINISTRO_BB )) <> 3 " & vbCrLf
        
        'Demanda: 211964 - Alexandre Queiroz (STF) 04/05/2007
        'n�o exibir no grid sinistros j� encerrados: (1140/1160)
        'vSql = vSql & " AND (SELECT COUNT(*) FROM EVENTO_SEGBR_SINISTRO_TB "
        'vSql = vSql & " Where Sinistro_BB = d.Sinistro_BB "
        'vSql = vSql & " AND EVENTO_BB_ID IN(1140,1160) )  = 0 "
        
        
'        vSQL = vSQL & "       AND  (d.dt_envio            IS NULL"
'        vSQL = vSQL & "       OR (select count(*) from evento_segbr_sinistro_tb where sinistro_bb = d.sinistro_bb and evento_bb_id in(1200,1201)) <> 0) "
        
        'MFrasca - 16794492 - Ajuste para contemplar produto 1218 - Microsseguro que � vida mas tem ramo RE
        'vSql = vSql & "       AND   RAMO.tp_ramo_id                  = " & vTpRamo
        If vTpRamo = 1 Then
            vsql = vsql & "       AND   ((RAMO.tp_ramo_id                  = " & vTpRamo & vbCrLf
            vsql = vsql & "               and pp.produto_id not in(1226, 1227)) OR PP.produto_id = 1218 )" & vbCrLf
        Else
            vsql = vsql & "       AND   ((RAMO.tp_ramo_id                  = " & vTpRamo & vbCrLf
            vsql = vsql & "              AND PP.produto_id <> 1218 )or pp.produto_id in(1226, 1227))" & vbCrLf
        End If
        'MFrasca - Fim
        
        'Alterado em 29/12/2005 - Sylvestre Xavier - CONFITEC
        'vSql = vSql & "       AND   PG.val_resseguro_recuperado      IS NULL"
        vsql = vsql & "       AND   PG.num_parcela = 1" & vbCrLf
        
        Monta_Query_Recibo = vsql
        
End Function

Private Function Monta_Query_Documento_OLD(ByVal TP_RAMO As String, ByVal AQuery_WHERE As String) As String
        Dim vNmlinkedserver As String
        
        vsql = "SELECT DISTINCT sinistro_id = SN.sinistro_id, sinistro_BB = SN_BB.sinistro_bb, " & vbCrLf
        If TP_RAMO = "1" Then       'VIDA
           vsql = vsql & " segurado_id=convert(varchar, SN_VD.cpf), nome_segurado=SN_VD.nome," & vbCrLf
        ElseIf TP_RAMO = "2" Then   'RE
           vsql = vsql & " segurado_id=convert(varchar, CLI.cliente_id), nome_segurado=CLI.nome," & vbCrLf
        End If
        vsql = vsql & " SN.proposta_id, SN.apolice_id, SN.sucursal_seguradora_id, " & vbCrLf
        vsql = vsql & " SN.seguradora_cod_susep, SN.ramo_id, SN.situacao, SN.lock_aviso, " & vbCrLf
        
        'Anderson Fiuza FLOW:1391554 21/10/2009 Adicionado o campo reg_cd_tip = 'a'
        'O processo estava sempre pegando recibo =0
        
        vsql = vsql & " num_recibo = case ER.reg_cd_tip when 'c' then convert(int, SUBSTRING(ER.registro, 26, 2)) WHEN   'A' THEN SUBSTRING(er.registro,44,2) " & vbCrLf
        vsql = vsql & " else convert(int, SUBSTRING(ER.registro, 53, 2)) end," & vbCrLf
        'vSQL = vSQL & " num_recibo = convert(int, SUBSTRING(ER.registro, 26, 2)), "
        vsql = vsql & " EV.evento_SEGBR_id, EV.Descricao, d.cod_remessa" & vbCrLf
        vsql = vsql & " FROM INTERFACE_DB..documentos_recebidos_GTR_tb d  with(nolock) " & vbCrLf
        vsql = vsql & "     INNER JOIN INTERFACE_DB..entrada_GTR_ATUAL_tb E  with(nolock) " & vbCrLf
        vsql = vsql & "         ON (convert(numeric, E.cod_remessa) = convert(numeric, d.cod_remessa)" & vbCrLf
        '**********************************************************
        'linha abaixo inclu�da por Stefanini Consultoria, 01/09/2004, solicitado por M�rcio
        'para garantir que o inner join retornar� somente um registro da tabela entrada_gtr
        vsql = vsql & "         AND d.evento_bb = e.evento_bb" & vbCrLf
        '**********************************************************
        'Inclu�do por Gustavo Machado em 02/04/2004
        'Para garantir que vai selecionar o sinistro_bb correto
        'Pois estavam sendo gerados 1110 com recibos incorretos
        vsql = vsql & "         AND d.sinistro_bb = e.sinistro_bb)" & vbCrLf
        vsql = vsql & "     INNER JOIN INTERFACE_DB..entrada_GTR_registro_ATUAL_tb ER  with(nolock) " & vbCrLf
        vsql = vsql & "         ON (ER.entrada_GTR_id            = E.entrada_GTR_id" & vbCrLf
        'vSQL = vSQL & "         AND ER.reg_cd_tip            = 'c')"
        'Anderson Fiuza adicionado o parametro = 'a'
        vsql = vsql & "         AND ER.reg_cd_tip            in( 'c','u','p','a'))" & vbCrLf
        vsql = vsql & "     INNER JOIN sinistro_bb_tb SN_BB     with(nolock) " & vbCrLf '(Index = PK_sinistro_bb)"
        vsql = vsql & "         ON (SN_BB.sinistro_BB               = d.sinistro_BB)" & vbCrLf
        'vSQL = vSQL & "         AND SN_BB.dt_fim_vigencia           IS NULL)"
        vsql = vsql & "     INNER JOIN sinistro_tb SN           with(nolock) " & vbCrLf '(Index = PK_sinistro)"
        vsql = vsql & "         ON (SN_BB.sinistro_id                = SN.sinistro_id)" & vbCrLf
        'vSQL = vSQL & "         AND SN_BB.apolice_id                 = SN.apolice_id"
        'vSQL = vSQL & "         AND SN_BB.seguradora_cod_susep       = SN.seguradora_cod_susep"
        'vSQL = vSQL & "         AND SN_BB.sucursal_seguradora_id     = SN.sucursal_seguradora_id"
        'vSQL = vSQL & "         AND SN_BB.ramo_id                    = SN.ramo_id)"
        vsql = vsql & "         AND SN.situacao <> 7"  'Demanda 3105507 - Diogo Ferreira 06/09/2010 - Segregacao AB/ABS
        vsql = vsql & "     LEFT JOIN proposta_tb PP           with(nolock) " & vbCrLf '(Index = PK_proposta)"
        vsql = vsql & "         ON (PP.proposta_id                   = SN.proposta_id)" & vbCrLf
        If TP_RAMO = "1" Then       'VIDA
           vsql = vsql & "      LEFT JOIN sinistro_vida_tb SN_VD   with(nolock) " & vbCrLf '(Index = PK_sinistro_vida)"
           vsql = vsql & "         ON (SN_BB.sinistro_id                = SN_VD.sinistro_id)" & vbCrLf
           'vSQL = vSQL & "         AND SN_BB.apolice_id                 = SN_VD.apolice_id"
           'vSQL = vSQL & "         AND SN_BB.seguradora_cod_susep       = SN_VD.seguradora_cod_susep"
           'vSQL = vSQL & "         AND SN_BB.sucursal_seguradora_id     = SN_VD.sucursal_seguradora_id"
           'vSQL = vSQL & "         AND SN_BB.ramo_id                    = SN_VD.ramo_id)"
        ElseIf TP_RAMO = "2" Then   'RE
           vsql = vsql & "     LEFT JOIN cliente_tb CLI       with(nolock) " & vbCrLf    '(Index = PK_cliente)"
           vsql = vsql & "         ON (CLI.cliente_id                   = SN.cliente_id)"
        End If
        vsql = vsql & "     INNER JOIN ramo_tb RAMO             with(nolock) " & vbCrLf   '(index = PK_ramo)"
        vsql = vsql & "         ON (SN.ramo_id                       = RAMO.ramo_id)" & vbCrLf
        vsql = vsql & "     INNER JOIN evento_SEGBR_tb EV       with(nolock) " & vbCrLf   '(index = PK_evento_SEGBR)"
        vsql = vsql & "         ON (d.evento_BB                      = EV.evento_BB_id)" & vbCrLf
        vsql = vsql & "     WHERE   d.evento_BB                      IN (" & AQuery_WHERE & ") " & vbCrLf
        
        'R�mulo Ribeiro
        'Data:09/06/2010
         vsql = vsql & "       AND  SN_BB.dt_fim_vigencia          IS NULL" & vbCrLf
        
        'myoshimura
        '20/02/2004
        'se tiver um evento de cancelamento do n�o protocolo tem que fazer o protocolo novamente
        'Alterado por Gustavo Machado em 31/03/2004
        vsql = vsql & "       AND  d.dt_envio                       IS NULL" & vbCrLf
'        vSQL = vSQL & "   AND  (d.dt_envio            IS NULL"
'        vSQL = vSQL & "   OR (select count(*) from evento_segbr_sinistro_tb where sinistro_bb = d.sinistro_bb and evento_bb_id in(1200,1201)) <> 0) "

        'Demanda: 214723 - Alexandre Queiroz (STF) 19/06/2007
        vsql = vsql & " AND (SELECT ISNULL(SITUACAO_SINISTRO,0) FROM EVENTO_SEGBR_SINISTRO_TB with(nolock) " & vbCrLf
        vsql = vsql & "      WHERE EVENTO_ID = (SELECT MAX(EVENTO_ID) FROM EVENTO_SEGBR_SINISTRO_TB with(nolock) " & vbCrLf
        vsql = vsql & "                         WHERE SINISTRO_BB = D.SINISTRO_BB )) <> 3 " & vbCrLf
        
        'Demanda: 211964 - Alexandre Queiroz (STF) 04/05/2007
        'n�o exibir no grid sinistros j� encerrados: (1140/1160)
        'vSql = vSql & " AND (SELECT COUNT(*) FROM EVENTO_SEGBR_SINISTRO_TB "
        'vSql = vSql & " Where Sinistro_BB = d.Sinistro_BB "
        'vSql = vSql & " AND EVENTO_BB_ID IN(1140,1160) )  = 0 "

        'MFrasca - 16794492 - Ajuste para contemplar produto 1218 - Microsseguro que � vida mas tem ramo RE
        'vSql = vSql & "       AND   RAMO.tp_ramo_id                  = " & vTpRamo
        If vTpRamo = 1 Then
            vsql = vsql & "       AND   ((RAMO.tp_ramo_id                  = " & vTpRamo
            vsql = vsql & "               and pp.produto_id not in(1226, 1227)) OR PP.produto_id = 1218 )" & vbCrLf
        Else
            vsql = vsql & "       AND   ((RAMO.tp_ramo_id                  = " & vTpRamo
            vsql = vsql & "              AND PP.produto_id <> 1218 )or pp.produto_id in(1226, 1227))" & vbCrLf
        End If
        'MFrasca - Fim
        
        'Demanda - 18335755 - Bloqueio de eventos de protocolo (Firstone)
        'Ajuste para que os sinistros dos produtos integrados e ativos que foram avisados a partir
        'da data de integra��o do produto, n�o sejam exibidos no programa.
        Select Case glAmbiente_id
               Case "2", "3"
                  vNmlinkedserver = "[SISAB013\WEB]."
               Case "6", "7"
                  vNmlinkedserver = "[SISAS013\WEB_ABS]."
               Case Else
                  vNmlinkedserver = ""
        End Select
        vsql = vsql & "       AND NOT EXISTS ( SELECT 1 " & vbCrLf
        vsql = vsql & "                          FROM " & vNmlinkedserver & "integracao_evento_db.dbo.produto_integracao_tb produto_integracao with(nolock)" & vbCrLf
        vsql = vsql & "                         WHERE pp.produto_id = produto_integracao.produto_id " & vbCrLf
        vsql = vsql & "                           AND   produto_integracao.ativo = 'S' " & vbCrLf
        vsql = vsql & "                           AND   (dt_implantacao IS NULL OR sn.dt_aviso_sinistro >= dt_implantacao ) " & vbCrLf
        vsql = vsql & "                           AND   parceiro_id = 1)"
                
        Monta_Query_Documento_OLD = vsql
        
End Function

Private Function Monta_Query_Recibo_OLD(ByVal TP_RAMO As String) As String
        Dim vNmlinkedserver As String
        
        vsql = "SELECT DISTINCT sinistro_id = SN.sinistro_id, sinistro_BB = SN_BB.sinistro_bb, " & vbCrLf
        If TP_RAMO = "1" Then       'VIDA
           vsql = vsql & " segurado_id=convert(varchar, SN_VD.cpf), nome_segurado=SN_VD.nome," & vbCrLf
        ElseIf TP_RAMO = "2" Then   'RE
           vsql = vsql & " segurado_id=convert(varchar, CLI.cliente_id), nome_segurado=CLI.nome," & vbCrLf
        End If
        vsql = vsql & " SN.proposta_id, SN.apolice_id, SN.sucursal_seguradora_id, " & vbCrLf
        vsql = vsql & " SN.seguradora_cod_susep, SN.ramo_id, SN.situacao, " & vbCrLf
        vsql = vsql & " SN.lock_aviso, num_recibo=PG.num_recibo, EV.evento_SEGBR_id, EV.Descricao, d.cod_remessa" & vbCrLf
        vsql = vsql & " FROM    INTERFACE_DB..documentos_recebidos_GTR_tb d with(nolock) " & vbCrLf
        vsql = vsql & "     INNER JOIN sinistro_bb_tb SN_BB     with(nolock) "   '(Index = PK_sinistro_bb)"
        vsql = vsql & "         ON (SN_BB.sinistro_BB               = d.sinistro_BB)" & vbCrLf
        '*******************************************************************************************
        ' ALTERA��O FEITA POR GUSTAVO MACHADO EM 12/05/2003 CONFORME SOLICITA��O DE JUNIOR RICARDO
        '*******************************************************************************************
        ' vSQL = vSQL & "         AND SN_BB.dt_fim_vigencia           IS NULL)"
        vsql = vsql & "     INNER JOIN sinistro_tb SN           with(nolock)" & vbCrLf    '(Index = PK_sinistro)"
        vsql = vsql & "         ON (SN_BB.sinistro_id                = SN.sinistro_id" & vbCrLf
        vsql = vsql & "         AND SN_BB.apolice_id                 = SN.apolice_id" & vbCrLf
        vsql = vsql & "         AND SN_BB.seguradora_cod_susep       = SN.seguradora_cod_susep" & vbCrLf
        vsql = vsql & "         AND SN_BB.sucursal_seguradora_id     = SN.sucursal_seguradora_id" & vbCrLf
        vsql = vsql & "         AND SN_BB.ramo_id                    = SN.ramo_id)" & vbCrLf
        vsql = vsql & "         AND SN.situacao <> 7"  'Demanda 3105507 - Diogo Ferreira 06/09/2010 - Segregacao AB/ABS
        vsql = vsql & "     INNER JOIN pgto_sinistro_tb PG           with(nolock)" & vbCrLf   '(Index = PK_pgto_sinistro)"
        vsql = vsql & "         ON (SN_BB.sinistro_id                = PG.sinistro_id" & vbCrLf
        vsql = vsql & "         AND SN_BB.apolice_id                 = PG.apolice_id" & vbCrLf
        vsql = vsql & "         AND SN_BB.seguradora_cod_susep       = PG.seguradora_cod_susep" & vbCrLf
        vsql = vsql & "         AND SN_BB.sucursal_seguradora_id     = PG.sucursal_seguradora_id" & vbCrLf
        vsql = vsql & "         AND SN_BB.ramo_id                    = PG.ramo_id" & vbCrLf
        vsql = vsql & "         AND PG.retorno_recibo_bb             = 'p'" & vbCrLf
        vsql = vsql & "         AND PG.item_val_estimativa           = 1" & vbCrLf
        vsql = vsql & "         AND PG.situacao_op                   = 'a'" & vbCrLf
        vsql = vsql & "         AND PG.acerto_id                     IS NOT NULL" & vbCrLf
        vsql = vsql & "         AND (PG.sol_estorno_SISBB             <> 's'" & vbCrLf
        vsql = vsql & "          OR PG.sol_estorno_SISBB             IS NULL))" & vbCrLf
        vsql = vsql & "     INNER JOIN proposta_tb PP           with(nolock) " & vbCrLf    '(Index = PK_proposta)"
        vsql = vsql & "         ON (PP.proposta_id                   = SN.proposta_id)" & vbCrLf
        If TP_RAMO = "1" Then       'VIDA
           vsql = vsql & "      INNER JOIN sinistro_vida_tb SN_VD   with(nolock) " & vbCrLf   '(Index = PK_sinistro_vida)"
           vsql = vsql & "         ON (SN_BB.sinistro_id                = SN_VD.sinistro_id" & vbCrLf
           vsql = vsql & "         AND SN_BB.apolice_id                 = SN_VD.apolice_id" & vbCrLf
           vsql = vsql & "         AND SN_BB.seguradora_cod_susep       = SN_VD.seguradora_cod_susep" & vbCrLf
           vsql = vsql & "         AND SN_BB.sucursal_seguradora_id     = SN_VD.sucursal_seguradora_id" & vbCrLf
           vsql = vsql & "         AND SN_BB.ramo_id                    = SN_VD.ramo_id)" & vbCrLf
        ElseIf TP_RAMO = "2" Then   'RE
           vsql = vsql & "     INNER JOIN cliente_tb CLI       with(nolock) " & vbCrLf    '(Index = PK_cliente)"
           vsql = vsql & "         ON (CLI.cliente_id                   = SN.cliente_id)" & vbCrLf
        End If
        vsql = vsql & "     INNER JOIN ramo_tb RAMO             with(nolock) " & vbCrLf   '(index = PK_ramo)"
        vsql = vsql & "         ON (SN.ramo_id                       = RAMO.ramo_id)" & vbCrLf
        vsql = vsql & "     INNER JOIN evento_SEGBR_tb EV       with(nolock) " & vbCrLf   '(index = PK_evento_SEGBR)"
        vsql = vsql & "         ON (d.evento_BB                      = EV.evento_BB_id)" & vbCrLf
        'INC000005010688 - Cleber Sardo - Lentid�o na base ABS - 15/07/2016
        If glAmbiente_id = 6 Or glAmbiente_id = 7 Then
        vsql = vsql & "     INNER JOIN ABSS.INTERFACE_DB.dbo.entrada_GTR_tb e with(nolock) " & vbCrLf
        Else
         vsql = vsql & "     INNER JOIN INTERFACE_DB..entrada_GTR_tb e with(nolock) " & vbCrLf
        End If
        vsql = vsql & "         ON (e.sinistro_BB = d.sinistro_BB" & vbCrLf
        '**********************************************************
        'linha abaixo inclu�da por Stefanini Consultoria, 01/09/2004, solicitado por M�rcio
        'para garantir que o inner join retornar� somente um registro da tabela entrada_gtr
        vsql = vsql & "         AND d.evento_bb = e.evento_bb" & vbCrLf
        '**********************************************************
        vsql = vsql & "         AND convert(numeric, e.cod_remessa) = convert(numeric, d.cod_remessa))" & vbCrLf
        'INC000005010688 - Cleber Sardo - Lentid�o na base ABS - 15/07/2016
        If glAmbiente_id = 6 Or glAmbiente_id = 7 Then
        vsql = vsql & "     INNER JOIN ABSS.INTERFACE_DB.dbo.entrada_GTR_registro_tb er  with(nolock)" & vbCrLf
        Else
        vsql = vsql & "     INNER JOIN INTERFACE_DB..entrada_GTR_registro_tb er  with(nolock)" & vbCrLf
        End If
        vsql = vsql & "         ON (er.entrada_GTR_id                               = e.entrada_GTR_id" & vbCrLf
        vsql = vsql & "         AND er.reg_cd_tip                                   = 'c'" & vbCrLf
        vsql = vsql & "         AND convert(tinyint, SUBSTRING(er.registro, 26, 2)) = PG.num_recibo) " & vbCrLf
        '*** Incluida para contemplar o evento A para o evento 2014 do ALS
        vsql = vsql & "         OR (er.entrada_GTR_id                               = e.entrada_GTR_id)" & vbCrLf
        vsql = vsql & "         AND er.reg_cd_tip                                   = 'a'" & vbCrLf
        vsql = vsql & "         AND convert(tinyint, SUBSTRING(er.registro, 44, 2)) = PG.num_recibo))" & vbCrLf
        vsql = vsql & "     WHERE   d.evento_BB                      = 2014" & vbCrLf
        
        'R�mulo
        'Data:10/06/2010
        vsql = vsql & "       AND   SN_BB.dt_fim_vigencia                       IS NULL" & vbCrLf
        
        'myoshimura
        '20/02/2004
        'se tiver um evento de cancelamento do n�o protocolo tem que fazer o protocolo novamente
        'Alterado por Gustavo Machado em 31/03/2004
        vsql = vsql & "       AND   d.dt_envio                       IS NULL" & vbCrLf
'        vSQL = vSQL & "   AND  (d.dt_envio            IS NULL"
'        vSQL = vSQL & "   or (d.dt_envio is not null"
'        vSQL = vSQL & "   and (select count(*) from evento_segbr_sinistro_tb where sinistro_bb = d.sinistro_bb and evento_bb_id in(1200,1201)) <> 0 ))"
        
        'Demanda: 211964 - Alexandre Queiroz (STF) 04/05/2007
        'n�o exibir no grid sinistros j� encerrados: (1140/1160)
        vsql = vsql & " AND (SELECT COUNT(*) FROM EVENTO_SEGBR_SINISTRO_TB with(nolock) " & vbCrLf
        vsql = vsql & " Where Sinistro_BB = d.Sinistro_BB " & vbCrLf
        vsql = vsql & " AND EVENTO_BB_ID IN(1140,1160) )  = 0 " & vbCrLf
        
        vsql = vsql & "       AND   RAMO.tp_ramo_id                  = " & vTpRamo & vbCrLf
        
        'Alterado em 29/12/2005 - Sylvestre Xavier - CONFITEC
        'vSql = vSql & "       AND   PG.val_resseguro_recuperado      IS NULL"
        vsql = vsql & "       AND   PG.num_parcela = 1" & vbCrLf
        
        'Demanda - 18335755 - Bloqueio de eventos de protocolo (Firstone)
        'Ajuste para que os sinistros dos produtos integrados e ativos que foram avisados a partir
        'da data de integra��o do produto, n�o sejam exibidos no programa.
        Select Case glAmbiente_id
               Case "2", "3"
                  vNmlinkedserver = "[SISAB013\WEB]."
               Case "6", "7"
                  vNmlinkedserver = "[SISAS013\WEB_ABS]."
               Case Else
                  vNmlinkedserver = ""
        End Select
        vsql = vsql & "               AND NOT EXISTS (SELECT 1 " & vbCrLf
        vsql = vsql & "                   FROM " & vNmlinkedserver & "integracao_evento_db.dbo.produto_integracao_tb produto_integracao with(nolock)" & vbCrLf
        vsql = vsql & "                   WHERE pp.produto_id = produto_integracao.produto_id " & vbCrLf
        vsql = vsql & "                   AND   produto_integracao.ativo = 'S' " & vbCrLf
        vsql = vsql & "                   AND   (dt_implantacao IS NULL OR sn.dt_aviso_sinistro >= dt_implantacao ) " & vbCrLf
        vsql = vsql & "                   AND   parceiro_id = 1)"

        vsql = vsql & " UNION " & vbCrLf
        vsql = "SELECT sinistro_id = SN.sinistro_id, sinistro_BB = SN_BB.sinistro_bb, " & vbCrLf
        If TP_RAMO = "1" Then       'VIDA
           vsql = vsql & " segurado_id=convert(varchar, SN_VD.cpf), nome_segurado=SN_VD.nome," & vbCrLf
        ElseIf TP_RAMO = "2" Then   'RE
           vsql = vsql & " segurado_id=convert(varchar, CLI.cliente_id), nome_segurado=CLI.nome," & vbCrLf
        End If
        vsql = vsql & " SN.proposta_id, SN.apolice_id, SN.sucursal_seguradora_id, " & vbCrLf
        vsql = vsql & " SN.seguradora_cod_susep, SN.ramo_id, SN.situacao, " & vbCrLf
        vsql = vsql & " SN.lock_aviso, num_recibo=PG.num_recibo, EV.evento_SEGBR_id, EV.Descricao, d.cod_remessa" & vbCrLf
        vsql = vsql & " FROM    INTERFACE_DB..documentos_recebidos_GTR_tb d with(nolock) " & vbCrLf
        vsql = vsql & "     INNER JOIN sinistro_bb_tb SN_BB     with(nolock) " & vbCrLf  '(Index = PK_sinistro_bb)"
        vsql = vsql & "         ON (SN_BB.sinistro_BB               = d.sinistro_BB)" & vbCrLf
        '*******************************************************************************************
        ' ALTERA��O FEITA POR GUSTAVO MACHADO EM 12/05/2003 CONFORME SOLICITA��O DE JUNIOR RICARDO
        '*******************************************************************************************
        ' vSQL = vSQL & "         AND SN_BB.dt_fim_vigencia           IS NULL)"
        vsql = vsql & "     INNER JOIN sinistro_tb SN           with(nolock)" & vbCrLf    '(Index = PK_sinistro)"
        vsql = vsql & "         ON (SN_BB.sinistro_id                = SN.sinistro_id" & vbCrLf
        vsql = vsql & "         AND SN_BB.apolice_id                 = SN.apolice_id" & vbCrLf
        vsql = vsql & "         AND SN_BB.seguradora_cod_susep       = SN.seguradora_cod_susep" & vbCrLf
        vsql = vsql & "         AND SN_BB.sucursal_seguradora_id     = SN.sucursal_seguradora_id" & vbCrLf
        vsql = vsql & "         AND SN_BB.ramo_id                    = SN.ramo_id)" & vbCrLf
        vsql = vsql & "         AND SN.situacao <> 7"  'Demanda 3105507 - Diogo Ferreira 06/09/2010 - Segregacao AB/ABS
        vsql = vsql & "     INNER JOIN pgto_sinistro_tb PG           with(nolock)" & vbCrLf   '(Index = PK_pgto_sinistro)"
        vsql = vsql & "         ON (SN_BB.sinistro_id                = PG.sinistro_id" & vbCrLf
        vsql = vsql & "         AND SN_BB.apolice_id                 = PG.apolice_id" & vbCrLf
        vsql = vsql & "         AND SN_BB.seguradora_cod_susep       = PG.seguradora_cod_susep" & vbCrLf
        vsql = vsql & "         AND SN_BB.sucursal_seguradora_id     = PG.sucursal_seguradora_id" & vbCrLf
        vsql = vsql & "         AND SN_BB.ramo_id                    = PG.ramo_id" & vbCrLf
        vsql = vsql & "         AND PG.retorno_recibo_bb             = 'p'" & vbCrLf
        vsql = vsql & "         AND PG.item_val_estimativa           = 1" & vbCrLf
        vsql = vsql & "         AND PG.situacao_op                   = 'a'" & vbCrLf
        vsql = vsql & "         AND PG.acerto_id                     IS NOT NULL" & vbCrLf
        vsql = vsql & "         AND (PG.sol_estorno_SISBB             <> 's'" & vbCrLf
        vsql = vsql & "          OR PG.sol_estorno_SISBB             IS NULL))" & vbCrLf
        vsql = vsql & "     INNER JOIN proposta_tb PP           with(nolock) " & vbCrLf    '(Index = PK_proposta)"
        vsql = vsql & "         ON (PP.proposta_id                   = SN.proposta_id)" & vbCrLf
        If TP_RAMO = "1" Then       'VIDA
           vsql = vsql & "      INNER JOIN sinistro_vida_tb SN_VD   with(nolock) " & vbCrLf   '(Index = PK_sinistro_vida)"
           vsql = vsql & "         ON (SN_BB.sinistro_id                = SN_VD.sinistro_id" & vbCrLf
           vsql = vsql & "         AND SN_BB.apolice_id                 = SN_VD.apolice_id" & vbCrLf
           vsql = vsql & "         AND SN_BB.seguradora_cod_susep       = SN_VD.seguradora_cod_susep" & vbCrLf
           vsql = vsql & "         AND SN_BB.sucursal_seguradora_id     = SN_VD.sucursal_seguradora_id" & vbCrLf
           vsql = vsql & "         AND SN_BB.ramo_id                    = SN_VD.ramo_id)" & vbCrLf
        ElseIf TP_RAMO = "2" Then   'RE
           vsql = vsql & "     INNER JOIN cliente_tb CLI       with(nolock) " & vbCrLf    '(Index = PK_cliente)"
           vsql = vsql & "         ON (CLI.cliente_id                   = SN.cliente_id)" & vbCrLf
        End If
        vsql = vsql & "     INNER JOIN ramo_tb RAMO             with(nolock) " & vbCrLf   '(index = PK_ramo)"
        vsql = vsql & "         ON (SN.ramo_id                       = RAMO.ramo_id)" & vbCrLf
        vsql = vsql & "     INNER JOIN evento_SEGBR_tb EV       with(nolock) " & vbCrLf   '(index = PK_evento_SEGBR)"
        vsql = vsql & "         ON (d.evento_BB                      = EV.evento_BB_id)" & vbCrLf
        'INC000005010688 - Cleber Sardo - Lentid�o na base ABS - 15/07/2016
        If glAmbiente_id = 6 Or glAmbiente_id = 7 Then
        vsql = vsql & "     INNER JOIN ABSS.INTERFACE_DB.dbo.entrada_GTR_tb e with(nolock)" & vbCrLf
        Else
        vsql = vsql & "     INNER JOIN INTERFACE_DB..entrada_GTR_tb e with(nolock)" & vbCrLf
        End If
        vsql = vsql & "         ON (e.sinistro_BB = d.sinistro_BB" & vbCrLf
        '**********************************************************
        'linha abaixo inclu�da por Stefanini Consultoria, 01/09/2004, solicitado por M�rcio
        'para garantir que o inner join retornar� somente um registro da tabela entrada_gtr
        vsql = vsql & "         AND e.evento_bb = d.evento_bb" & vbCrLf
        '**********************************************************
        vsql = vsql & "         AND convert(numeric, e.cod_remessa) = convert(numeric, d.cod_remessa))" & vbCrLf
        'INC000005010688 - Cleber Sardo - Lentid�o na base ABS - 15/07/2016
        If glAmbiente_id = 6 Or glAmbiente_id = 7 Then
        vsql = vsql & "     INNER JOIN ABSS.INTERFACE_DB.dbo.entrada_GTR_registro_tb er  with(nolock)" & vbCrLf
        Else
        vsql = vsql & "     INNER JOIN INTERFACE_DB..entrada_GTR_registro_tb er  with(nolock)" & vbCrLf
        End If
        vsql = vsql & "         ON  (er.entrada_GTR_id                               = e.entrada_GTR_id" & vbCrLf
        vsql = vsql & "         AND ((er.reg_cd_tip                                   = 'c'" & vbCrLf
        vsql = vsql & "         AND convert(tinyint, SUBSTRING(er.registro, 26, 2)) = PG.num_recibo)" & vbCrLf
        vsql = vsql & "         OR  (er.reg_cd_tip                                   = 'u'" & vbCrLf
        vsql = vsql & "         AND convert(tinyint, SUBSTRING(er.registro, 53, 2)) = PG.num_recibo)" & vbCrLf
         '*** Incluida para contemplar o registro A para o evento 2014 do ALS - inicio
         vsql = vsql & "        OR  (er.reg_cd_tip                                   = 'a'" & vbCrLf
        vsql = vsql & "         AND convert(tinyint, SUBSTRING(er.registro, 44, 2)) = PG.num_recibo)))" & vbCrLf
         '*** Incluida para contemplar o evento A para o evento 2014 do ALS - fim
         
        vsql = vsql & "     WHERE   d.evento_BB                      = 2014" & vbCrLf
        'Alterado por Gustavo Machado em 31/03/2004
        vsql = vsql & "       AND   d.dt_envio                       IS NULL" & vbCrLf
        
       'R�mulo
        'Data:10/06/2010
        vsql = vsql & "       AND   SN_BB.dt_fim_vigencia                       IS NULL" & vbCrLf
        
       'Demanda: 214723 - Alexandre Queiroz (STF) 19/06/2007
        vsql = vsql & " AND (SELECT ISNULL(SITUACAO_SINISTRO,0) FROM EVENTO_SEGBR_SINISTRO_TB with(nolock) " & vbCrLf
        vsql = vsql & "      WHERE EVENTO_ID = (SELECT MAX(EVENTO_ID) FROM EVENTO_SEGBR_SINISTRO_TB with(nolock) " & vbCrLf
        vsql = vsql & "                         WHERE SINISTRO_BB = D.SINISTRO_BB )) <> 3 " & vbCrLf
        
        'Demanda: 211964 - Alexandre Queiroz (STF) 04/05/2007
        'n�o exibir no grid sinistros j� encerrados: (1140/1160)
        'vSql = vSql & " AND (SELECT COUNT(*) FROM EVENTO_SEGBR_SINISTRO_TB "
        'vSql = vSql & " Where Sinistro_BB = d.Sinistro_BB "
        'vSql = vSql & " AND EVENTO_BB_ID IN(1140,1160) )  = 0 "
        
        
'        vSQL = vSQL & "       AND  (d.dt_envio            IS NULL"
'        vSQL = vSQL & "       OR (select count(*) from evento_segbr_sinistro_tb where sinistro_bb = d.sinistro_bb and evento_bb_id in(1200,1201)) <> 0) "
        
        'MFrasca - 16794492 - Ajuste para contemplar produto 1218 - Microsseguro que � vida mas tem ramo RE
        'vSql = vSql & "       AND   RAMO.tp_ramo_id                  = " & vTpRamo
        If vTpRamo = 1 Then
            vsql = vsql & "       AND   ((RAMO.tp_ramo_id                  = " & vTpRamo & vbCrLf
            vsql = vsql & "               and pp.produto_id not in(1226, 1227)) OR PP.produto_id = 1218 )" & vbCrLf
        Else
            vsql = vsql & "       AND   ((RAMO.tp_ramo_id                  = " & vTpRamo & vbCrLf
            vsql = vsql & "              AND PP.produto_id <> 1218 )or pp.produto_id in(1226, 1227))" & vbCrLf
        End If
        'MFrasca - Fim
        
        'Alterado em 29/12/2005 - Sylvestre Xavier - CONFITEC
        'vSql = vSql & "       AND   PG.val_resseguro_recuperado      IS NULL"
        vsql = vsql & "       AND   PG.num_parcela = 1" & vbCrLf
        
        Monta_Query_Recibo_OLD = vsql
        
End Function

Private Function Monta_Query_Documento_LOG(ByVal AQuery_WHERE As String) As String
        
        vsql = "SELECT DISTINCT sinistro_id = 0, d.sinistro_bb, segurado_id = convert(varchar, ROBO.cpf_cgc), "
        vsql = vsql & " nome_segurado = ROBO.nome_segurado, proposta_id = 0, apolice_id = 0,"
        vsql = vsql & " sucursal_seguradora_id = 0, seguradora_cod_susep = 0, ramo_id = 0,"
        vsql = vsql & " situacao = '0', lock_aviso = '', num_recibo = 0, ev.evento_SEGBR_id,"
        vsql = vsql & " ev.Descricao, d.cod_remessa"
        vsql = vsql & " FROM INTERFACE_DB..documentos_recebidos_GTR_tb d with(nolock)"
        vsql = vsql & "     INNER JOIN WEB_SEGUROS_DB..ROBO_aviso_sinistro_LOG ROBO with(nolock)"
        vsql = vsql & "         ON (convert(numeric, ROBO.num_aviso) = d.sinistro_BB)"
        vsql = vsql & "     INNER JOIN evento_SEGBR_tb EV      with(nolock)" ' (index = PK_evento_SEGBR)"
        vsql = vsql & "         ON (d.evento_BB                      = ev.evento_BB_id)"
        vsql = vsql & " WHERE d.evento_BB           IN (" & AQuery_WHERE & ") "
               
       'R�mulo
        'Data:10/06/2010
        vsql = vsql & "       AND   SN_BB.dt_fim_vigencia                       IS NULL"
        
        'myoshimura
        '20/02/2004
        'se tiver um evento de cancelamento do n�o protocolo tem que fazer o protocolo novamente
        'Alterado por Gustavo Machado em 31/03/2004
        vsql = vsql & "   AND  d.dt_envio            IS NULL"

        'Demanda: 214723 - Alexandre Queiroz (STF) 19/06/2007
        vsql = vsql & " AND (SELECT ISNULL(SITUACAO_SINISTRO,0) FROM EVENTO_SEGBR_SINISTRO_TB with(nolock) "
        vsql = vsql & "      WHERE EVENTO_ID = (SELECT MAX(EVENTO_ID) FROM EVENTO_SEGBR_SINISTRO_TB with(nolock) "
        vsql = vsql & "                         WHERE SINISTRO_BB = D.SINISTRO_BB )) <> 3 "

        'Demanda: 211964 - Alexandre Queiroz (STF) 04/05/2007
        'n�o exibir no grid sinistros j� encerrados: (1140/1160)
        'vSql = vSql & " AND (SELECT COUNT(*) FROM EVENTO_SEGBR_SINISTRO_TB "
        'vSql = vSql & " Where Sinistro_BB = d.Sinistro_BB "
        'vSql = vSql & " AND EVENTO_BB_ID IN(1140,1160) )  = 0 "
   

'        vSQL = vSQL & "   AND  (d.dt_envio            IS NULL"
'        vSQL = vSQL & "   or (d.dt_envio is not null"
'        vSQL = vSQL & "   and (select count(*) from evento_segbr_sinistro_tb where sinistro_bb = d.sinistro_bb and evento_bb_id in(1200,1201)) <> 0 ))"

        vsql = vsql & "   AND dt_aviso              IS NOT NULL"
        vsql = vsql & "   AND NOT EXISTS            (SELECT * FROM sinistro_BB_tb sbb with(nolock)"
        vsql = vsql & "                              WHERE sbb.sinistro_BB = d.sinistro_BB)"
        
        Monta_Query_Documento_LOG = vsql
        
End Function

Public Sub Habilita_Botoes(ByVal OSinistro_BB As String, ByVal OEvento_SEGBR_ID As String)

        If (Selecionou_Registro = False) Or (OSinistro_BB = "" And OEvento_SEGBR_ID = "") Then
           cmdConfirmarRecebimento.Enabled = False
           cmdRessalvaDoctosBasicos.Enabled = False
           cmdRecusadoMotivosDiversos.Enabled = False
           Exit Sub
        End If
        
        cmdConfirmarRecebimento.Enabled = True
        If OEvento_SEGBR_ID = "10110" Then
           cmdRessalvaDoctosBasicos.Enabled = True
        ElseIf (OEvento_SEGBR_ID = "10117" And Existem_Documentos_na_Exigencia(OSinistro_BB)) Then
           cmdRessalvaDoctosBasicos.Enabled = True
        Else
           cmdRessalvaDoctosBasicos.Enabled = False
        End If
        cmdRecusadoMotivosDiversos.Enabled = IIf(OEvento_SEGBR_ID = "10110", False, True)
        
End Sub

Public Function Existem_Documentos_na_Exigencia(ByVal OSinistro_BB As String) As Boolean
       
       Dim SQL  As String
       Dim rs   As rdoResultset
       
       On Error GoTo TrataErro
       
       Existem_Documentos_na_Exigencia = False
       
       SQL = "SELECT total_docs = COUNT(ed.tp_documento_id)"
       SQL = SQL & " FROM evento_SEGBR_sinistro_tb e with(nolock) "
       SQL = SQL & "    INNER JOIN evento_SEGBR_sinistro_detalhe_tb ed with(nolock)"
       SQL = SQL & "        ON (ed.evento_id = e.evento_id)"
       SQL = SQL & " WHERE e.Sinistro_BB = " & OSinistro_BB
       SQL = SQL & "   AND e.evento_bb_id = 2011"
       SQL = SQL & "   AND e.evento_id = (SELECT MAX(e2.evento_id)"
       SQL = SQL & "                        FROM evento_SEGBR_sinistro_tb e2 with(nolock) "
       SQL = SQL & "                        WHERE e2.Sinistro_BB  = e.Sinistro_BB"
       SQL = SQL & "                          AND e2.evento_bb_id = 2011)"
       SQL = SQL & "   AND ed.tp_documento_id IS NOT NULL"
       Set rs = rdocn.OpenResultset(SQL)
           Existem_Documentos_na_Exigencia = rs("total_docs") > 0
       rs.Close
       
       Exit Function
       
TrataErro:
    '***********************************************************
    'Alterado por Cleber da Stefanini - data: 11/12/2004
    'Foi adicionado � mensagem de erro qual � o SQL que esta
    '   sendo executado nesta fun��o.
    'mensagem_erro 6, "Rotina: EXISTEM_DOCUMENTOS_NA_EXIGENCIA"
    mensagem_erro 6, "Rotina: EXISTEM_DOCUMENTOS_NA_EXIGENCIA" & vbCrLf & SQL
    '***********************************************************
    Call TerminaSEGBR
    
End Function
Private Function Existe_Cancelamento(ByVal Sinistro_BB As String, ByVal Cod_Remessa_GTR As String) As Boolean
        
        Dim SQL As String
        Dim rs As rdoResultset
        Dim OSaida_GTR_id   As String
        Dim ASituacao       As String
        
        On Error GoTo TrataErro
        
        Existe_Cancelamento = False
        
        'Alterado por Gustavo Machado em 10/11/2003
        SQL = " SELECT s.saida_gtr_id,s.situacao,* FROM evento_segbr_sinistro_tb e with(nolock) "
        SQL = SQL & " INNER JOIN interface_db..saida_gtr_tb s with(nolock) "
        SQL = SQL & " ON e.sinistro_bb = s.sinistro_bb"
        SQL = SQL & " Where e.Sinistro_BB = " & Sinistro_BB
        SQL = SQL & " AND e.evento_bb_id = 1120"
        SQL = SQL & " AND NOT EXISTS (SELECT sinistro_id"
        SQL = SQL & " FROM evento_segbr_sinistro_tb e1 with(nolock) "
        SQL = SQL & " Where e1.evento_bb_id = 2006"
        SQL = SQL & " AND e1.evento_id > e.evento_id )"

                
        Set rs = rdocn.OpenResultset(SQL)
        
        If Not rs.EOF Then
           
           OSaida_GTR_id = rs("saida_gtr_id")
           ASituacao = UCase(rs("Situacao"))
           rs.Close
           
           SQL = "EXEC INTERFACE_DB..documentos_recebidos_GTR_spu "
           SQL = SQL & "'" & Cod_Remessa_GTR & "'"
           SQL = SQL & ", '" & Format(Data_Sistema, "YYYYMMDD") & "'"
           SQL = SQL & ", '" & cUserName & "'"
           Set rs = rdocn.OpenResultset(SQL)
           
           
           If ASituacao = "E" Then
           
                rs.Close
                SQL = "EXEC INTERFACE_DB..altera_remessa_saida_gtr_spu "
                SQL = SQL & OSaida_GTR_id
                SQL = SQL & ", ''"
                SQL = SQL & ", 'N'"
                SQL = SQL & ", ''"
                SQL = SQL & ", '" & cUserName & "'"
                Set rs = rdocn.OpenResultset(SQL)
                 
           End If
           
           Existe_Cancelamento = True
        
           
        End If
        
        rs.Close
        
        Exit Function
                   
TrataErro:
    mensagem_erro 6, "Erro n�: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description
    Call TerminaSEGBR
        
End Function

Private Function ValidaRecebimentoDocumentoExigencia(ByVal sinistroID As String) As String
Dim strSql As String
Dim rsExig As rdoResultset

On Error GoTo Erro:

    strSql = ""
    strSql = strSql & " SET NOCOUNT ON EXEC seguros_db.dbo.SEGS13136_SPS " & sinistroID
    
    Set rsExig = rdocn.OpenResultset(strSql)
    
    If Not rsExig.EOF Then
        ValidaRecebimentoDocumentoExigencia = UCase(rsExig(0))
    Else
        ValidaRecebimentoDocumentoExigencia = "N"
    End If
    
    rsExig.Close
    Set rsExig = Nothing
    
    Exit Function

Erro:
    mensagem_erro 9, "ValidaRecebimentoDocumentoExigencia - Erro"
    Call TerminaSEGBR
End Function

