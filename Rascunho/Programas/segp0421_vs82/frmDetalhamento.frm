VERSION 5.00
Begin VB.Form frmDetalhamento 
   Caption         =   "SEG10421 - Detalhamento - "
   ClientHeight    =   8310
   ClientLeft      =   630
   ClientTop       =   1410
   ClientWidth     =   9225
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   8310
   ScaleWidth      =   9225
   Begin VB.Frame frameNovaDetalhe 
      Caption         =   " Detalhamento"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   7890
      Left            =   75
      TabIndex        =   0
      Top             =   135
      Width           =   9060
      Begin VB.TextBox txtObservacao3 
         Height          =   315
         Left            =   135
         MaxLength       =   70
         TabIndex        =   19
         Top             =   6630
         Width           =   8775
      End
      Begin VB.TextBox txtObservacao2 
         Height          =   315
         Left            =   135
         MaxLength       =   70
         TabIndex        =   18
         Top             =   6320
         Width           =   8775
      End
      Begin VB.TextBox txtObservacao1 
         Height          =   315
         Left            =   135
         MaxLength       =   70
         TabIndex        =   17
         Top             =   6000
         Width           =   8775
      End
      Begin VB.TextBox txtExigencia 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   9
         Left            =   135
         MaxLength       =   70
         TabIndex        =   11
         Top             =   3015
         Width           =   8775
      End
      Begin VB.TextBox txtExigencia 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   8
         Left            =   135
         MaxLength       =   70
         TabIndex        =   10
         Top             =   2745
         Width           =   8775
      End
      Begin VB.TextBox txtExigencia 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   7
         Left            =   135
         MaxLength       =   70
         TabIndex        =   9
         Top             =   2475
         Width           =   8775
      End
      Begin VB.TextBox txtExigencia 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   6
         Left            =   135
         MaxLength       =   70
         TabIndex        =   8
         Top             =   2205
         Width           =   8775
      End
      Begin VB.TextBox txtExigencia 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   5
         Left            =   135
         MaxLength       =   70
         TabIndex        =   7
         Top             =   1935
         Width           =   8775
      End
      Begin VB.TextBox txtExigencia 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   4
         Left            =   135
         MaxLength       =   70
         TabIndex        =   6
         Top             =   1665
         Width           =   8775
      End
      Begin VB.ListBox lstDocumentos 
         Height          =   1860
         Left            =   135
         Style           =   1  'Checkbox
         TabIndex        =   13
         Top             =   3690
         Width           =   8775
      End
      Begin VB.TextBox txtExigencia 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   3
         Left            =   135
         MaxLength       =   70
         TabIndex        =   5
         Top             =   1395
         Width           =   8775
      End
      Begin VB.TextBox txtExigencia 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   2
         Left            =   135
         MaxLength       =   70
         TabIndex        =   4
         Top             =   1110
         Width           =   8775
      End
      Begin VB.TextBox txtExigencia 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   1
         Left            =   135
         MaxLength       =   70
         TabIndex        =   3
         Top             =   825
         Width           =   8775
      End
      Begin VB.TextBox txtExigencia 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   0
         Left            =   135
         MaxLength       =   70
         TabIndex        =   2
         Top             =   540
         Width           =   8775
      End
      Begin VB.CommandButton btnDetalheOk 
         Caption         =   "Ok"
         Height          =   375
         Left            =   6345
         TabIndex        =   14
         Top             =   7350
         Width           =   1215
      End
      Begin VB.CommandButton btnDetalheCancelar 
         Caption         =   "Cancelar"
         Height          =   375
         Left            =   7710
         TabIndex        =   15
         Top             =   7350
         Width           =   1215
      End
      Begin VB.Label Label1 
         Caption         =   "Observa��o:"
         Height          =   255
         Left            =   135
         TabIndex        =   16
         Top             =   5760
         Width           =   1695
      End
      Begin VB.Label lbl 
         Caption         =   "Lista de Documentos para exig�ncia:"
         Height          =   255
         Index           =   0
         Left            =   135
         TabIndex        =   12
         Top             =   3465
         Width           =   2640
      End
      Begin VB.Label lbl 
         Caption         =   "Detalhamento:"
         Height          =   255
         Index           =   50
         Left            =   150
         TabIndex        =   1
         Top             =   315
         Width           =   1335
      End
   End
End
Attribute VB_Name = "frmDetalhamento"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public Tipo_Evento              As String 'Esta vari�vel ir� indicar qual evento dever� ser gerado
Public Sinistro_BB              As String
Public Indice                   As Integer
Public Carrega_Dados_Anteriores As Boolean
Dim Lista_De_Documentos_temp    As String

'incluido por andre luvison - 17/01/2005
Dim COD_TRANSACAO               As String
'----------------------------------------------------

Private Sub Form_Load()
    
    Dim Sin As cSinistro
    
    Call CentraFrm(Me)
    
    'If Tipo_Evento = "1112" Then
    If IsNumeric(Tipo_Evento) Then
        If CInt(Tipo_Evento) = 1112 Then
           Me.Caption = "SEGP10421 - Protocolo com Ressalva por falta de Documentos B�sicos"
           Call Lock_Campos(True)
        Else
           Me.Caption = "SEGP10421 - Protocolo por Motivos Diversos"
           Call Lock_Campos(False)
        End If
    Else
       Me.Caption = "SEGP10421 - Protocolo por Motivos Diversos"
       Call Lock_Campos(False)
    End If
    
    Call Monta_Lista_Documentos
    
    If Carrega_Dados_Anteriores Then
       'Set Sin = SinistrosSelecionados(Indice)
       'Call Carrega_Exigencia_Anterior(Sin)
       Call Carrega_Exigencia_Anterior
       Call Seleciona_Documentos
       'Set Sin = Nothing
    End If
    
    Screen.MousePointer = vbNormal
    
End Sub

Private Sub btnDetalheOk_Click()
    
    Dim Linha               As String
    Dim numlinhas           As Integer
    Dim i                   As Integer
    Dim Sin                 As cSinistro
    Dim Tipo_Alteracao_Aux  As String
    
    On Error GoTo TrataErro
    
    '***************************************************
    'Alterado por Cleber da Stefanini - data: 24/03/2005
    'Captura os codigos dos documentos e joga em uma string
    '   para depois fazer uma consist�ncia
    Captura_List_Checks
    '***************************************************
    
    'teste
    txtObservacao1.Text = Replace(txtObservacao1.Text, "'", "�")
    txtObservacao2.Text = Replace(txtObservacao2.Text, "'", "�")
    txtObservacao3.Text = Replace(txtObservacao3.Text, "'", "�")
    
    'Tipo_Alteracao_Aux = IIf(Tipo_Evento = "1112", "2", "3")
    If IsNumeric(Tipo_Evento) Then
        Tipo_Alteracao_Aux = IIf(CInt(Tipo_Evento) = 1112, "2", "3")
    Else
        Tipo_Alteracao_Aux = IIf(Tipo_Evento = "1112", "2", "3")
    End If

    If Tipo_Alteracao_Aux = "2" Then
       '*********************************************************************
        'Alterado por Cleber da Stefanini - data: 24/03/2005
        'Essa verifica��o estava sendo feito com uma string, s� que quando
        '   checkava e tirava o check a string continuava com valor, portanto
        '   ia inserir o evento de forma incorreta.
        
        'ANTES
        'If Lista_De_Documentos_temp = "" Then
        '   MsgBox "Nenhuma altera��o foi indicada!", vbOKOnly + vbExclamation
        '   lstDocumentos.SetFocus
        '   Exit Sub
        'End If
        
        'DEPOIS
        Dim Selecionado As Boolean
        
        Selecionado = False
        For i = 0 To lstDocumentos.ListCount - 1
            If lstDocumentos.Selected(i) Then
                Selecionado = True
                Exit For
            End If
        Next
        If Not Selecionado Then
            MsgBox "Nenhuma altera��o foi indicada!", vbOKOnly + vbExclamation
            lstDocumentos.SetFocus
            Exit Sub
        End If
        '*********************************************************************
      
    Else
        For i = 0 To 9
            txtExigencia(i).Text = MudaAspaSimples(txtExigencia(i).Text)
            If Trim(txtExigencia(i).Text) <> "" Then
                Linha = Linha & txtExigencia(i).Text & Chr(10)
            End If
        Next
        If Linha = "" Then
           MsgBox "Nenhuma altera��o foi indicada!", vbOKOnly + vbExclamation
           txtExigencia(0).SetFocus
           Exit Sub
        End If
    End If
    
    If MsgBox("Confirma a Recusa do Documento ?", vbYesNo + vbQuestion + vbDefaultButton2, "Recusar de Documenta��o") = vbNo Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    
    'Set Sin = SinistrosSelecionados(Indice)
    
    'Efetua a a��o informada
    'If Not AlterarConfirmacaoDocumentos(Tipo_Alteracao_Aux, Sin) Then
    If Not AlterarConfirmacaoDocumentos(Tipo_Alteracao_Aux) Then
       MsgBox "A Recusa n�o foi efetuada !!!", vbOKOnly + vbCritical
       Screen.MousePointer = vbNormal
       Exit Sub
    End If
        
    ProtocolarExigencia frmConfDocumentos.flexSelecao.TextMatrix(Indice, 3)
                  
    'Retira a linha do grid e a linha da cole��o
    With frmConfDocumentos
        If (.flexSelecao.Rows - 1) = 1 Then
           .flexSelecao.Rows = 1
           'SinistrosSelecionados.Remove 1
        Else
           .flexSelecao.RemoveItem Indice
           'SinistrosSelecionados.Remove Indice
        End If
    End With
    
    Call frmConfDocumentos.Habilita_Botoes("", "")
    frmConfDocumentos.lblSelecionados = Str(frmConfDocumentos.flexSelecao.Rows - 1) & " Iten(s) Encontrado(s)"

    MsgBox "Recusa efetuada com Sucesso !!!", vbOKOnly + vbInformation
    
    Screen.MousePointer = vbNormal
    Unload Me
    
    Exit Sub
    
TrataErro:
    TrataErroGeral "btnDetalheOK", Me.name
    Call TerminaSEGBR
    
End Sub

Private Sub btnDetalheCancelar_Click()
    
    Unload Me
    
End Sub

Private Sub lstDocumentos_Click()
    
'******************************************************************
'Comentado por Cleber da Stefanini - data: 26/11/2005 - Flow 111855
'Foi trocado a l�gica e foi colocado no evento ItemCheck deste List
'******************************************************************
'    Dim Copia(10)   As String
'    Dim Agrupou     As Boolean
'    Dim i           As Integer
'    Dim pos         As Integer
'    Dim strRetirar  As String
'    Dim pos_String  As Integer
'
'    If Carrega_Dados_Anteriores Then
'       Lista_De_Documentos_temp = Lista_De_Documentos_temp & ";" & Format(lstDocumentos.ItemData(lstDocumentos.ListIndex), "000000")
'       Exit Sub
'    End If
'
'    If lstDocumentos.Selected(lstDocumentos.ListIndex) Then
'        ' L�gica para mandar o m�ximo de documentos poss�veis.
'        Agrupou = Agrupa_Documentos
'
'        If Not Agrupou Then
'            For i = 0 To 9
'                txtExigencia(i).Text = Copia(i)
'            Next i
'            For i = 0 To 9
'                If Len(txtExigencia(i).Text & ";" & lstDocumentos.Text) <= 70 Then
'                    txtExigencia(i).Text = txtExigencia(i).Text & ";" & lstDocumentos.Text
'                    Agrupou = True
'                    Lista_De_Documentos_temp = Lista_De_Documentos_temp & ";" & _
'                                    Format(lstDocumentos.ItemData(lstDocumentos.ListIndex), "000000")
'                    Exit For
'                End If
'            Next i
'        Else
'            Lista_De_Documentos_temp = Lista_De_Documentos_temp & ";" & _
'                            Format(lstDocumentos.ItemData(lstDocumentos.ListIndex), "000000")
'        End If
'
'        If Not Agrupou Then
'            lstDocumentos.Selected(lstDocumentos.ListIndex) = False
'            MsgBox "Foi alcan�ado o limite de espa�o para documentos."
'        End If
'    Else
'        'alterado por andre luvison
'        'strRetirar = lstDocumentos.List(lstDocumentos.ListIndex)
'        strRetirar = Mid(lstDocumentos.List(lstDocumentos.ListIndex), 1, 70)
'        '------------------------------------------------------------------
'
'        For i = 0 To 9
'            pos_String = InStr(1, txtExigencia(i), strRetirar)
'
'            If pos_String > 0 Then
'               If pos_String = 1 Then
'                  txtExigencia(i) = Mid(txtExigencia(i), pos_String + Len(strRetirar))
'               Else
'                  txtExigencia(i) = Mid(txtExigencia(i), 1, pos_String - 2) & Mid(txtExigencia(i), pos_String + Len(strRetirar))
'               End If
'               Exit For
'            End If
'        Next
'
'        pos = InStr(Lista_De_Documentos_temp, Format(lstDocumentos.ItemData(lstDocumentos.ListIndex), "000000"))
'        If pos > 0 Then
'            Lista_De_Documentos_temp = Left(Lista_De_Documentos_temp, pos - 2) & Mid(Lista_De_Documentos_temp, pos + 6)
'        End If
'
'        Agrupou = Agrupa_Documentos
'    End If
'******************************************************************
End Sub

Private Sub lstDocumentos_ItemCheck(Item As Integer)
'*******************************************************************************
'Adicionado por Cleber da Stefanini - data: 26/11/2005 - Flow 111855
'Altera��o da l�gica de funcionamento e altera��o do evento Click para ItemCheck
'*******************************************************************************
Dim i       As Integer
Dim j       As Integer
Dim Count   As Integer

'-------------------------------------------------------------------------
'Data: 01/11/2006 - Alterado por Cleber - Flow 162281
'No clique da lista de documentos, foi colocado uma verifica��o para
'   validar se o documento que o usu�rio esta checando j� foi protocolado
'   antes ou n�o, caso j� tenha sido ser� exibido uma mensagem para ele
'   dizendo que o documento j� foi protocolado e n�o pode ser mais recusado.
'-------------------------------------------------------------------------
Dim RsValidacao As rdoResultset

If lstDocumentos.Selected(Item) And arrAviso.evento_SEGBR_id = "10117" Then

    vSql = " select  e2.tp_documento_id, e2.descricao "
    vSql = vSql & " from    evento_Segbr_sinistro_tb e1 with(nolock) "
    vSql = vSql & " inner join evento_segbr_sinistro_detalhe_tb e2 with(nolock) "
    vSql = vSql & " on    e2.evento_id = e1.evento_id "
    vSql = vSql & " and   e2.tp_documento_id is not null "
    vSql = vSql & " Where e1.Sinistro_BB = " & arrAviso.Sinistro_BB
    vSql = vSql & " and     e2.tp_documento_id = " & lstDocumentos.ItemData(Item)
    vSql = vSql & " and     e1.evento_id = (    select  max(e3.evento_id) "
    vSql = vSql & " from    evento_Segbr_sinistro_tb e3 with(nolock) "
    vSql = vSql & " where   e3.evento_bb_id in (2003, 2011) "
    vSql = vSql & " and     e3.sinistro_bb = e1.sinistro_bb) "

    Set RsValidacao = rdocn.OpenResultset(vSql)

    If RsValidacao.EOF Then
        MsgBox "O documento: " & lstDocumentos.List(Item) & ", j� consta como protocolado anteriormente. " & vbNewLine & "N�o pode ser recusado.", vbOKOnly + vbExclamation, "Recusar de Documenta��o"
        lstDocumentos.Selected(Item) = False
    End If
    RsValidacao.Close
End If
'-------------------------------------------------------------------------

'Limpa todos os Texts
For i = 0 To 9
    txtExigencia(i).Text = ""
Next

Count = 0
'Carrega as informa��es nos Texts
For i = 0 To lstDocumentos.ListCount - 1
    If lstDocumentos.Selected(i) Then
        If Count >= 10 Then
            lstDocumentos.Selected(lstDocumentos.ListIndex) = False
            MsgBox "Foi alcan�ado o limite de espa�o para documentos."
            Exit For
        End If
        
        txtExigencia(Count).Text = lstDocumentos.List(i)
        Count = Count + 1
        
    End If
Next
'*******************************************************************************
End Sub


Private Sub txtExigencia_KeyPress(Index As Integer, KeyAscii As Integer)
    
    If KeyAscii = 13 Then
        If Index < 9 Then
            txtExigencia(Index + 1).SetFocus
        End If
    Else
        If txtExigencia(Index).SelStart = 70 Then
            If Index < 9 Then
                txtExigencia(Index + 1).SetFocus
                txtExigencia(Index).SelStart = 1
            End If
        End If
    End If
    
End Sub

Private Sub Monta_Lista_Documentos()

    'ALTERADO POR ANDR� LUVISON - 17/11/2004
    Dim SQL As String, rs As rdoResultset
    Dim rsaux As rdoResultset
        
    On Error GoTo TrataErro
    
    lstDocumentos.Clear
    
    SQL = "SELECT COD_TRANSACAO FROM evento_SEGBR_sinistro_tb with(nolock) WHERE Sinistro_BB  = " & arrAviso.Sinistro_BB & " AND evento_bb_id = 1100 "
    Set rsaux = rdocn.OpenResultset(SQL)
    If Not rsaux.EOF Then
        If Not IsNull(rsaux!COD_TRANSACAO) Then
            COD_TRANSACAO = rsaux!COD_TRANSACAO
        End If
        rsaux.Close
    End If
       
       If COD_TRANSACAO <> "ST10" Then
            'If Tipo_Evento = "1112" Then
            If IsNumeric(Tipo_Evento) Then
                If CInt(Tipo_Evento) = 1112 Then
                   SQL = ""
                   SQL = "SELECT tp_documento_BB_id = ed.tp_documento_id, t.nome"
                   SQL = SQL & " FROM tp_documento_BB_tb t with(nolock) "
                   SQL = SQL & "    LEFT JOIN evento_SEGBR_sinistro_detalhe_tb ed with(nolock) "
                   SQL = SQL & "        ON ed.tp_documento_id = t.tp_documento_BB_id"
                   SQL = SQL & "    INNER JOIN evento_SEGBR_sinistro_tb e with(nolock) "
                   SQL = SQL & "        ON e.evento_id = ed.evento_id"
                   SQL = SQL & " WHERE e.Sinistro_BB  = " & arrAviso.Sinistro_BB
                   SQL = SQL & " AND   e.evento_bb_id = 2003"
                Else
                    SQL = "SELECT tp_documento_BB_id, nome FROM tp_documento_BB_tb with(nolock) "
                End If
            Else
                SQL = "SELECT tp_documento_BB_id, nome FROM tp_documento_BB_tb with(nolock) "
            End If
            
            Set rs = rdocn.OpenResultset(SQL)
            
            While Not rs.EOF
                lstDocumentos.AddItem rs!NOME
                lstDocumentos.ItemData(lstDocumentos.ListCount - 1) = rs!tp_documento_BB_id
                rs.MoveNext
            Wend
            rs.Close
            
       Else 'ALS - Cod_Transacao = 'ST10'
            If IsNumeric(Tipo_Evento) Then
                If CInt(Tipo_Evento) = 1112 Then
'''''                   SQL = ""
'''''                   SQL = "SELECT tp_documento_id = ed.tp_documento_id, t.nome"
'''''                   SQL = SQL & " FROM tp_documento_tb t"
'''''                   SQL = SQL & "    LEFT JOIN evento_SEGBR_sinistro_detalhe_tb ed"
'''''                   SQL = SQL & "        ON ed.tp_documento_id = t.tp_documento_id"
'''''                   SQL = SQL & "    INNER JOIN evento_SEGBR_sinistro_tb e"
'''''                   SQL = SQL & "        ON e.evento_id = ed.evento_id"
'''''                   SQL = SQL & " WHERE e.Sinistro_BB  = " & Sinistro_BB
'''''                   SQL = SQL & " AND   e.evento_bb_id = 2003 "
'''''                   SQL = SQL & " ORDER BY t.nome "
                   SQL = ""
                   SQL = "SELECT tp_documento_id = ed.tp_documento_id, t.Descricao From  documento_tb t with(nolock) LEFT JOIN "
                   SQL = SQL & "evento_SEGBR_sinistro_detalhe_tb ed with(nolock) ON ed.tp_documento_id = t.documento_id INNER JOIN "
                   SQL = SQL & "evento_SEGBR_sinistro_tb e with(nolock) ON e.evento_id = ed.evento_id "
                   SQL = SQL & " WHERE e.Sinistro_BB  = " & arrAviso.Sinistro_BB
                   SQL = SQL & " AND   e.evento_bb_id = 2003 "
                   SQL = SQL & " ORDER BY t.descricao "
                Else
                    SQL = "SELECT tp_documento_id = documento_id, descricao FROM documento_tb with(nolock) ORDER BY descricao "
                End If
            Else
                SQL = "SELECT tp_documento_id = documento_id, descricao FROM documento_tb with(nolock) ORDER BY descricao "
            End If
            
            Set rs = rdocn.OpenResultset(SQL)
            
            While Not rs.EOF
                lstDocumentos.AddItem rs!Descricao
                lstDocumentos.ItemData(lstDocumentos.ListCount - 1) = rs!tp_documento_id
                rs.MoveNext
            Wend
            rs.Close
            
       End If
            
    Exit Sub
    '--------------------------------------------------------------------------------------------------------
TrataErro:
    TrataErroGeral "Monta_Lista_Documentos", Me.name
    Call TerminaSEGBR
    
End Sub

Private Function Agrupa_Documentos() As Boolean
    
    Dim Maior As Integer, Menor As Integer
    Dim Menor_vetor As Integer, Maior_vetor As Integer
    Dim Terminou As Boolean, Vetor() As String
    Dim Vetores As Integer, i As Integer
    Dim Vetores_Validos As Integer
    Dim Linha_id As Integer
    
    Dim strRetirar  As String
    Dim pos_String  As Integer
    Dim j           As Integer
    
    Agrupa_Documentos = True
    
    Vetores = 0
    ReDim Vetor(lstDocumentos.SelCount) As String
    For i = 0 To lstDocumentos.ListCount - 1
        If lstDocumentos.Selected(i) Then
            Vetores = Vetores + 1
            Vetor(Vetores) = lstDocumentos.List(i)
            
            
            strRetirar = Vetor(Vetores)
            
            For j = 0 To 9
                pos_String = InStr(1, txtExigencia(j), strRetirar)
                
                If pos_String > 0 Then
                   If pos_String = 1 Then
                      txtExigencia(j) = Mid(txtExigencia(j), pos_String + Len(strRetirar))
                   Else
                      txtExigencia(j) = Mid(txtExigencia(j), 1, pos_String - 2) & Mid(txtExigencia(j), pos_String + Len(strRetirar))
                   End If
                   Exit For
                End If
            Next
            
        End If
    Next i
    
    'Obt�m os dados digitados pelo usu�rio que n�o se encontram na lista de documentos
    For i = 0 To 9
        If txtExigencia(i).Text <> "" Then
           Vetores = Vetores + 1
           ReDim Preserve Vetor(Vetores) As String
           Vetor(Vetores) = TiraCaracter(txtExigencia(i).Text, ";")
           'limpa o campo txt para nova montagem
           txtExigencia(i).Text = ""
        End If
    Next
    
    Linha_id = 0
    Terminou = False
    While Not Terminou
        Maior = 0
        Vetores_Validos = 0
        For i = 1 To Vetores
            If (Len(Vetor(i)) > Maior) Then
                Maior = Len(Vetor(i))
                Maior_vetor = i
            End If
            If Vetor(i) <> "" Then
                Vetores_Validos = Vetores_Validos + 1
            End If
        Next i
    
        If Maior = 0 Then
            Terminou = True
        Else
            If Vetores_Validos = 1 Then
                Linha_id = Linha_id + 1
                If Linha_id > 10 Then
                    Agrupa_Documentos = False
                    Terminou = True
                Else
                    txtExigencia(Linha_id - 1).Text = Vetor(Maior_vetor)
                    Terminou = True
                End If
            Else
                Menor = 70
                For i = 1 To Vetores
                    If (Len(Vetor(i)) < Menor) And (Vetor(i) <> "") And (Maior_vetor <> i) Then
                        Menor = Len(Vetor(i))
                        Menor_vetor = i
                    End If
                Next i
                
                If Len(Vetor(Menor_vetor) & ";" & Vetor(Maior_vetor)) > 70 Then
                    Linha_id = Linha_id + 1
                    If Linha_id > 10 Then
                        Agrupa_Documentos = False
                        Terminou = True
                    Else
                        txtExigencia(Linha_id - 1).Text = Vetor(Maior_vetor)
                        Vetor(Maior_vetor) = ""
                    End If
                Else
                    Vetor(Maior_vetor) = Vetor(Menor_vetor) & ";" & Vetor(Maior_vetor)
                    Vetor(Menor_vetor) = ""
                End If
            End If
        End If
    
    Wend

End Function

Private Function Retira_Enter_Linha(ByVal relLinha As String) As String
    
    ' Esta fun��o retira o Enter - CHR(13) e o espa�o - CHR(10)
    Dim i As Long, strLinha As String
    
    strLinha = relLinha
    i = Len(strLinha)
    If i > 0 Then
       Do While (i >= 1)
           If ((Mid(strLinha, i, 1) = Chr(13)) Or (Mid(strLinha, i, 1) = Chr(10)) Or (Mid(strLinha, i, 1) = " ")) Then
              strLinha = Mid(strLinha, 1, i - 1)
              i = i - 1
           Else
              Exit Do
           End If
       Loop
    End If
    
    Retira_Enter_Linha = strLinha
    
End Function

Private Sub Lock_Campos(ByVal OValor As Boolean)
    
    Dim i As Integer
    
    For i = 0 To 9
        txtExigencia(i).Locked = OValor
    Next
    
    'lstDocumentos.Enabled = OValor
    lstDocumentos.Enabled = True
    
End Sub

Private Sub txtExigencia_LostFocus(Index As Integer)
    txtExigencia(Index).Text = UCase(txtExigencia(Index).Text)
End Sub

Private Sub Carrega_Exigencia_Anterior()
'Private Sub Carrega_Exigencia_Anterior(ByVal OSinistro As cSinistro)
    
    Dim SQL As String
    Dim rs  As rdoResultset
    Dim i   As Byte
    
    On Error GoTo TrataErro
    
    '* Se n�o tiver sinistro id � porque este sinistro � do LOG
    'If OSinistro.Sinistro_id <> "" Then
    If arrAviso.Sinistro_id <> "" Then
        SQL = "SELECT ld.linha"
        SQL = SQL & " FROM sinistro_historico_tb sh with(nolock) "
        SQL = SQL & "     INNER JOIN sinistro_linha_detalhamento_tb ld with(nolock) "
        SQL = SQL & "         ON (ld.sinistro_id            = sh.sinistro_id"
        SQL = SQL & "         AND ld.apolice_id             = sh.apolice_id"
        SQL = SQL & "         AND ld.seguradora_cod_susep   = sh.seguradora_cod_susep"
        SQL = SQL & "         AND ld.sucursal_seguradora_id = sh.sucursal_seguradora_id"
        SQL = SQL & "         AND ld.ramo_id                = sh.ramo_id"
        SQL = SQL & "         AND ld.detalhamento_id        = sh.detalhamento_id)"
        SQL = SQL & " WHERE sh.evento_SEGBR_id        in (10032, 10127, 10125, 10126)"
        SQL = SQL & "   AND sh.sinistro_id            = " & arrAviso.Sinistro_id
        SQL = SQL & "   AND sh.apolice_id             = " & arrAviso.Apolice_id
        SQL = SQL & "   AND sh.seguradora_cod_susep   = " & arrAviso.Seguradora_id
        SQL = SQL & "   AND sh.sucursal_seguradora_id = " & arrAviso.Sucursal_id
        SQL = SQL & "   AND sh.ramo_id                = " & arrAviso.ramo_id
        SQL = SQL & "   AND sh.seq_evento             = (SELECT MAX(seq_evento)"
        SQL = SQL & "                                    FROM sinistro_historico_tb sh2 with(nolock) "
        SQL = SQL & "                                    WHERE sh2.Sinistro_id            = sh.Sinistro_id"
        SQL = SQL & "                                      AND sh2.apolice_id             = sh.apolice_id"
        SQL = SQL & "                                      AND sh2.seguradora_cod_susep   = sh.seguradora_cod_susep"
        SQL = SQL & "                                      AND sh2.sucursal_seguradora_id = sh.sucursal_seguradora_id"
        SQL = SQL & "                                      AND sh2.ramo_id                = sh.ramo_id"
        SQL = SQL & "                                      AND sh2.evento_SEGBR_id        IN (10032, 10127, 10125, 10126))"
        
    '    SQL = SQL & "   AND ld.detalhamento_id        = (SELECT MAX(d.detalhamento_id)"
    '    SQL = SQL & "                                    FROM sinistro_detalhamento_tb d"
    '    SQL = SQL & "                                    WHERE d.Sinistro_id            = ld.Sinistro_id"
    '    SQL = SQL & "                                      AND d.apolice_id             = ld.apolice_id"
    '    SQL = SQL & "                                      AND d.seguradora_cod_susep   = ld.seguradora_cod_susep"
    '    SQL = SQL & "                                      AND d.sucursal_seguradora_id = ld.sucursal_seguradora_id"
    '    SQL = SQL & "                                      AND d.ramo_id                = ld.ramo_id"
    '    SQL = SQL & "                                      AND d.tp_detalhamento        = 1)"
    Else
        SQL = "SELECT ld.linha"
        SQL = SQL & " FROM sinistro_historico_LOG_tb sh with(nolock) "
        SQL = SQL & "     INNER JOIN sinistro_linha_detalhamento_LOG_tb ld with(nolock) "
        SQL = SQL & "         ON (ld.sinistro_BB            = sh.sinistro_BB"
        SQL = SQL & "         AND ld.detalhamento_id        = sh.detalhamento_id)"
        SQL = SQL & " WHERE sh.evento_SEGBR_id        in (10032, 10127, 10125, 10126)"
        SQL = SQL & "   AND sh.sinistro_BB            = " & arrAviso.Sinistro_BB
        SQL = SQL & "   AND sh.seq_evento             = (SELECT MAX(seq_evento)"
        SQL = SQL & "                                    FROM sinistro_historico_LOG_tb sh2 with(nolock) "
        SQL = SQL & "                                    WHERE sh2.Sinistro_BB            = sh.Sinistro_BB"
        SQL = SQL & "                                      AND sh2.evento_SEGBR_id        IN (10032, 10127, 10125, 10126))"
    End If
    SQL = SQL & " ORDER BY ld.linha_id"
    Set rs = rdocn.OpenResultset(SQL)
    i = 0
    While (Not rs.EOF) And (i <= 9)
        txtExigencia(i).Text = rs("linha")
        i = i + 1
        rs.MoveNext
    Wend
    rs.Close
    
    Exit Sub
    
TrataErro:
    TrataErroGeral "Carrega_Exigencia_Anterior", Me.name
    Call TerminaSEGBR
    
End Sub

Private Sub Seleciona_Documentos()
    
    Dim pos     As Integer
    Dim pos_Ini As Integer
    Dim conta   As Byte
    Dim conta2  As Byte
    Dim strAux  As String
'''''    'Alterado por Andr� Luvison - 17/01/2005
'''''    Dim SQL As String
'''''    Dim rsaux As rdoResultset
'''''    Dim i As Integer
'''''    '----------------------------------------------------------------------------------------
    
    On Error GoTo TrataErro
    
'''''    'Alterado por Andr� Luvison - 17/01/2005
'''''    If COD_TRANSACAO = "ST10" Then
'''''        For i = 0 To 9
'''''            SQL = ""
'''''            SQL = "SELECT A.NOME FROM TP_DOCUMENTO_TB A INNER JOIN TP_DOCUMENTO_BB_TB B ON A.TP_DOCUMENTO_ID = B.TP_DOCUMENTO_BB_ID "
'''''            SQL = SQL & " WHERE  B.NOME = '" & UCase(txtExigencia(i).Text) & "'"
'''''            Set rsaux = rdocn.OpenResultset(SQL)
'''''            If Not rsaux.EOF Then
'''''                    For conta2 = 0 To lstDocumentos.ListCount - 1
'''''                        If rsaux(0) = UCase(lstDocumentos.List(conta2)) Then
'''''                           txtExigencia(i).Text = UCase(rsaux(0))
'''''                           lstDocumentos.Selected(conta2) = True
'''''                           Exit For
'''''                        End If
'''''                    Next
'''''            End If
'''''            rsaux.Close
'''''        Next
'''''    '----------------------------------------------------------------------------------------
'''''    Else
        pos_Ini = 1
        For conta = 0 To 9
            If txtExigencia(conta).Text <> "" Then
               While pos_Ini > 0
                    pos_Ini = IIf(Mid(txtExigencia(conta).Text, pos_Ini, 1) = ";", pos_Ini + 1, pos_Ini)
                    pos = InStr(pos_Ini, txtExigencia(conta).Text, ";")
                    If pos > 0 Then
                       strAux = UCase(Mid(txtExigencia(conta).Text, pos_Ini, pos - pos_Ini))
                    Else
                       strAux = UCase(Mid(txtExigencia(conta).Text, pos_Ini))
                    End If
                    pos_Ini = pos
                    
                    For conta2 = 0 To lstDocumentos.ListCount - 1
                        If strAux = UCase(lstDocumentos.List(conta2)) Then
                           lstDocumentos.Selected(conta2) = True
                           Exit For
                        End If
                    Next
               Wend
               pos_Ini = 1
            Else
               Exit For
            End If
        Next
'''''    End If
    
    Carrega_Dados_Anteriores = False
    
    Exit Sub
    
TrataErro:
    TrataErroGeral "Seleciona_Documentos", Me.name
    Call TerminaSEGBR
    
End Sub
