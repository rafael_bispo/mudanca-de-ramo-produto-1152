Attribute VB_Name = "Inicializa"
Private Type tpItem_estimativa
        ID      As String
        Nome    As String
End Type
Global Item_estimativa(8) As tpItem_estimativa

Global alterou As Boolean

Global Operacao As String 'Alteracao ou Inclusao
Global Linha As Integer 'Linha em que o usu�rio clicou em frmAlcada para fazer a altera��o


Sub Main()
    On Error GoTo TrataErro
    
    'MsgBox "Command: " & Command
        
    '----- Para testes - comentar as linhas abaixo
    If Not Trata_Parametros(Command) Then 'Or InStr(1, Command, " ") = 0 Then
        FinalizarAplicacao    'cristovao.rodrigues 06/06 descomentar
    End If
    
    'MsgBox "Ambiente recebido do par�metro Command: " & glAmbiente_id
    
    '----- Para testes - descomentar as linhas abaixo
    'glAmbiente_id = 3
    'cUserName = "NTendencia"
    'Ambiente = ConvAmbiente(glAmbiente_id)
    '********************************
    
    lConexaoLocal = AbrirConexao(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription)
        
    'MsgBox "Conectado!"

    frmAlcada.Show
        
    Exit Sub

TrataErro:
    Call TrataErroGeral("FormLoad", "frmEstimativa")
    Call FinalizarAplicacao
    
End Sub
Public Sub Carrega_cboItem_Estimativa(ByRef cboAux As ComboBox, Optional ByVal Todos As Boolean = False)
    Dim SQL As String
    Dim rs As ADODB.Recordset
    Dim cont As Integer
    
    cboAux.Clear
    
    If Todos Then
       cboAux.AddItem "TODOS"
       cboAux.ItemData(cboAux.NewIndex) = 0
    End If
    
    Call Carrega_Matriz_Item_Estimativa
    
    For cont = 1 To 8
        cboAux.AddItem Item_estimativa(cont).Nome
        cboAux.ItemData(cboAux.NewIndex) = Item_estimativa(cont).ID
    Next
    cboAux.ListIndex = 0

End Sub


Public Sub Carrega_Matriz_Item_Estimativa()
    Item_estimativa(1).ID = 1
    Item_estimativa(1).Nome = "Indeniza��o"
    Item_estimativa(2).ID = 2
    Item_estimativa(2).Nome = "Honor�rios"
    Item_estimativa(3).ID = 3
    Item_estimativa(3).Nome = "Despesas"
    Item_estimativa(4).ID = 4
    Item_estimativa(4).Nome = "Ressarcimento"
    Item_estimativa(5).ID = 5
    Item_estimativa(5).Nome = "Despesas com ressarcimento"
    Item_estimativa(6).ID = 6
    Item_estimativa(6).Nome = "Salvados"
    Item_estimativa(7).ID = 7
    Item_estimativa(7).Nome = "Respesas com salvados"
    Item_estimativa(8).ID = 8
    Item_estimativa(8).Nome = "Al�ada Liquida de Resseguro/Cosseguro"

End Sub
    

Public Sub Carrega_cboProduto(ByRef cboAux As ComboBox, Optional ByVal Todos As Boolean = False)
    Dim SQL As String
    Dim rs As ADODB.Recordset
    
    cboAux.Clear
    
    If Todos Then
       cboAux.AddItem "TODOS"
       cboAux.ItemData(cboAux.NewIndex) = 0
    End If
    
    SQL = "SELECT produto_id, nome"
    SQL = SQL & " FROM produto_tb"
    SQL = SQL & " WHERE ativo = 's'"
    SQL = SQL & " ORDER BY produto_id"
    
    Set rs = Conexao_ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, SQL, lConexaoLocal, True)
        
    While Not rs.EOF
        cboAux.AddItem Format(rs("produto_id"), "000") & " - " & rs("nome")
        cboAux.ItemData(cboAux.NewIndex) = rs("produto_id")
        rs.MoveNext
    Wend
    rs.Close
    
    cboAux.ListIndex = 0
    
End Sub

Public Sub Carrega_cboRamo(ByRef cboAux As ComboBox, ByRef cboAux_Produto As Integer, Optional ByVal Todos As Boolean = False)
    Dim SQL As String
    Dim rs As ADODB.Recordset
    
    cboAux.Clear
    
    If Todos Then
       cboAux.AddItem "TODOS"
       cboAux.ItemData(cboAux.NewIndex) = 0
    End If
    
    If cboAux_Produto <> 0 Then
       SQL = " Select i.ramo_id , r.nome from item_produto_tb i"
       SQL = SQL & " Join ramo_tb r on i.ramo_id = r.ramo_id "
       SQL = SQL & " Where i.produto_id = " & cboAux_Produto
       
       Set rs = Conexao_ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, SQL, lConexaoLocal, True)
       
       While Not rs.EOF
           cboAux.AddItem rs("nome")
           cboAux.ItemData(cboAux.NewIndex) = rs("ramo_id")
           rs.MoveNext
       Wend
       rs.Close
    End If
    
    cboAux.ListIndex = 0
    
End Sub


Public Sub Carrega_cboTecnico(ByRef cboAux As ComboBox, Optional ByVal Todos As Boolean = False)
    Dim SQL As String
    Dim rs As ADODB.Recordset
    
    cboAux.Clear
    
    If Todos Then
        cboAux.AddItem "TODOS"
        cboAux.ItemData(cboAux.NewIndex) = 0
    End If
    
    SQL = "SELECT tecnico_id, nome"
    SQL = SQL & " FROM tecnico_tb"
    SQL = SQL & " WHERE ativo = 's'"
    SQL = SQL & " ORDER BY nome"
    
    Set rs = Conexao_ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, SQL, lConexaoLocal, True)
    
    While Not rs.EOF
        cboAux.AddItem rs("nome")
        cboAux.ItemData(cboAux.NewIndex) = rs("tecnico_id")
        rs.MoveNext
    Wend
    rs.Close
    
    cboAux.ListIndex = 0
    
End Sub

