VERSION 5.00
Object = "{C932BA88-4374-101B-A56C-00AA003668DC}#1.1#0"; "MSMASK32.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.2#0"; "MSCOMCTL.OCX"
Begin VB.Form frmAlcadaCon 
   Caption         =   "SEGP1509 - Regra de al�ada para pagamentos de sinistro"
   ClientHeight    =   3255
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   12990
   LinkTopic       =   "Form1"
   ScaleHeight     =   3255
   ScaleWidth      =   12990
   StartUpPosition =   3  'Windows Default
   Begin VB.PictureBox tsFundo 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2430
      Left            =   60
      ScaleHeight     =   2370
      ScaleWidth      =   12675
      TabIndex        =   3
      Top             =   0
      Width           =   12735
      Begin VB.Frame fraCriterios 
         Height          =   2310
         Left            =   75
         TabIndex        =   4
         Top             =   -15
         Width           =   12525
         Begin VB.ComboBox cboProduto 
            Height          =   315
            Left            =   6885
            Style           =   2  'Dropdown List
            TabIndex        =   8
            Top             =   495
            Width           =   5550
         End
         Begin VB.ComboBox cboItem_EstimativaInc 
            Height          =   315
            Left            =   6855
            Style           =   1  'Simple Combo
            TabIndex        =   7
            Text            =   "cboItem_EstimativaInc"
            Top             =   1140
            Width           =   5565
         End
         Begin VB.ComboBox cboTecnico 
            Height          =   315
            Left            =   120
            Style           =   2  'Dropdown List
            TabIndex        =   6
            Top             =   495
            Width           =   6705
         End
         Begin VB.ComboBox cboRamo 
            Height          =   315
            Left            =   135
            TabIndex        =   5
            Text            =   "cboRamo"
            Top             =   1140
            Width           =   6660
         End
         Begin MSMask.MaskEdBox mskDtIniVig 
            Height          =   300
            Left            =   165
            TabIndex        =   9
            Top             =   1815
            Width           =   1230
            _ExtentX        =   2170
            _ExtentY        =   529
            _Version        =   393216
            MaxLength       =   10
            Mask            =   "##/##/####"
            PromptChar      =   " "
         End
         Begin MSMask.MaskEdBox mskDtFimVig 
            Height          =   300
            Left            =   1650
            TabIndex        =   10
            Top             =   1815
            Width           =   1230
            _ExtentX        =   2170
            _ExtentY        =   529
            _Version        =   393216
            MaxLength       =   10
            Mask            =   "##/##/####"
            PromptChar      =   " "
         End
         Begin VB.Label lblPesquisa 
            Caption         =   "Tecnico:"
            Height          =   255
            Index           =   4
            Left            =   120
            TabIndex        =   16
            Top             =   270
            Width           =   810
         End
         Begin VB.Label lblPesquisa 
            Caption         =   "Produto:"
            Height          =   255
            Index           =   0
            Left            =   6900
            TabIndex        =   15
            Top             =   225
            Width           =   660
         End
         Begin VB.Label lblPesquisa 
            Caption         =   "Item de estimativa:"
            Height          =   255
            Index           =   1
            Left            =   6870
            TabIndex        =   14
            Top             =   900
            Width           =   1440
         End
         Begin VB.Label lbTecti 
            AutoSize        =   -1  'True
            Caption         =   "Ramo:"
            Height          =   195
            Index           =   1
            Left            =   135
            TabIndex        =   13
            Top             =   900
            Width           =   465
         End
         Begin VB.Label Label3 
            Caption         =   "Inicio de vig�ncia:"
            Height          =   240
            Left            =   165
            TabIndex        =   12
            Top             =   1590
            Width           =   1365
         End
         Begin VB.Label Label2 
            Caption         =   "Fim de vig�ncia:"
            Height          =   240
            Left            =   1650
            TabIndex        =   11
            Top             =   1590
            Width           =   1365
         End
      End
   End
   Begin VB.CommandButton cmdSair 
      Caption         =   "&Sair"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      Left            =   11700
      TabIndex        =   2
      Top             =   2550
      Width           =   1125
   End
   Begin VB.CommandButton cmdAplicar 
      Caption         =   "Aplicar"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      Left            =   10470
      TabIndex        =   0
      Top             =   2550
      Width           =   1125
   End
   Begin MSComctlLib.StatusBar stbarPesquisa 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   1
      Top             =   2970
      Width           =   12990
      _ExtentX        =   22913
      _ExtentY        =   503
      Style           =   1
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmAlcadaCon"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'Constantes para o GRID de Pesquisa
Const GRID_PESQ_TECNICO = 0
Const GRID_PESQ_PRODUTO_NOME = 1
Const GRID_PESQ_RAMO_NOME = 2
Const GRID_PESQ_ITEM_VAL_ESTIMATIVA = 3
Const GRID_PESQ_VAL_PAGAMENTO = 4
Const GRID_PESQ_DT_INI_VIG = 5
Const GRID_PESQ_DT_FIM_VIG = 6
Const GRID_PESQ_PRODUTO_ID = 7
Const GRID_PESQ_TECNICO_ID = 8
Const GRID_PESQ_RAMO_ID = 9
Const GRID_PESQ_ITEM_ESTIMATIVA_ID = 10
Const GRID_PESQ_LIMITE_APROVACAO_ESTIMATIVA_ID = 11
Dim sugestao_dt_inicio_vigencia

Private Sub cboProduto_Click()
    If cboProduto.Text <> "" Then
        Call Carrega_cboRamoInc(cboRamo, cboProduto.ItemData(cboProduto.ListIndex), False)
    End If
End Sub


Private Sub cmdAplicar_Click()
    If Operacao = "Inclusao" Then
        If Verifica_Campos Then
            If Dados_Validos Then
                If Not Inclui_tecnico Then
                    MsgBox "Falha na inclus�o do t�cnico"
                Else
                    MsgBox "T�cnico inclu�do com sucesso!"
                    Unload Me
                End If
            End If
        Else
                MsgBox "Todos os campos s�o de preenchimento obrigat�rio com exce��o do Fim de Vig�ncia."
        End If
    ElseIf Operacao = "Alteracao" Then
            If Altera_regra Then
                MsgBox "Regra alterada com Sucesso.", vbOKOnly + vbInformation, "Alera��o de Estimativa"
                frmAlcada.Refresh
                Unload Me
            End If
    End If
End Sub

Private Function Altera_regra() As Boolean
    Dim SQL As String
    Dim grid_Val_Limite
    Dim grid_produto_id As Integer
    Dim grid_ramo_id As Integer
    Dim grid_tecnico_id As String
    Dim grid_Item_Estimativa As Integer
    Dim grid_dt_inicio_vigencia As String
    Dim grid_dt_fim_vigencia As String
    Dim LDT_FIM_VIG As Date
    Dim grid_Ilimitado As String
    Dim grid_Liberado_Aprov_Maior As String
    Dim grid_alcada_liquida As String
    Dim grid_limite_aprovacao_estimativa_id As Integer
    Dim strDataVigencia As String
    Dim strDataInicioVigencia As String
    Dim strDataFimVigencia As String
    
    On Error GoTo Erro
    
    Altera_regra = True

    Linha = frmAlcada.fgridConfiguracoes.RowSel
    With frmAlcada
        .fgridConfiguracoes.Col = GRID_PESQ_PRODUTO_ID
        grid_produto_id = .fgridConfiguracoes.Text
    
        '.fgridConfiguracoes.Col = GRID_PESQ_VAL_PAGAMENTO
        'grid_Val_Limite = TrocaVirgulaPorPonto(.fgridConfiguracoes.Text)
    
        .fgridConfiguracoes.Col = GRID_PESQ_RAMO_ID
        grid_ramo_id = .fgridConfiguracoes.Text
        
        .fgridConfiguracoes.Col = GRID_PESQ_TECNICO_ID
        grid_tecnico_id = .fgridConfiguracoes.Text
        
        .fgridConfiguracoes.Col = IIf(IsEmpty(GRID_PESQ_ITEM_ESTIMATIVA_ID), 0, GRID_PESQ_ITEM_ESTIMATIVA_ID)
        grid_Item_Estimativa = .fgridConfiguracoes.Text
        
        .fgridConfiguracoes.Col = GRID_PESQ_DT_INI_VIG
        grid_dt_inicio_vigencia = Format(.fgridConfiguracoes.Text, "dd/mm/yyyy")
        
        .fgridConfiguracoes.Col = GRID_PESQ_DT_FIM_VIG
        grid_dt_fim_vigencia = Format(.fgridConfiguracoes.Text, "dd/mm/yyyy")
        
        .fgridConfiguracoes.Col = GRID_PESQ_LIMITE_APROVACAO_ESTIMATIVA_ID
        grid_limite_aprovacao_estimativa_id = .fgridConfiguracoes.Text
        
    End With
    
    If IsDate(mskDtFimVig.Text) Then
        strDataVigencia = Format(mskDtFimVig.Text, "dd/mm/yyyy")
    Else
        strDataVigencia = ""
    End If
    
    'If TrocaVirgulaPorPonto(mskValLimite.Text) = frmAlcada.fgridConfiguracoes.TextMatrix(Linha, GRID_PESQ_VAL_PAGAMENTO) And Format(frmAlcada.fgridConfiguracoes.TextMatrix(Linha, GRID_PESQ_DT_INI_VIG), "dd/mm/yyyy") = Format(mskDtIniVig, "dd/mm/yyyy") And (Format(frmAlcada.fgridConfiguracoes.TextMatrix(Linha, GRID_PESQ_DT_FIM_VIG), "dd/mm/yyyy") = strDataVigencia) Then
    If Format(frmAlcada.fgridConfiguracoes.TextMatrix(Linha, GRID_PESQ_DT_INI_VIG), "dd/mm/yyyy") = Format(mskDtIniVig, "dd/mm/yyyy") And (Format(frmAlcada.fgridConfiguracoes.TextMatrix(Linha, GRID_PESQ_DT_FIM_VIG), "dd/mm/yyyy") = strDataVigencia) Then
        MsgBox "N�o houve altera��es na regra"
        Altera_regra = False
        Exit Function
    'ElseIf Format(frmAlcada.fgridConfiguracoes.TextMatrix(Linha, GRID_PESQ_DT_INI_VIG), "dd/mm/yyyy") >= Format(mskDtIniVig.Text, "dd/mm/yyyy") Then
    '    MsgBox "A data de inicio de vig�ncia informada deve ser superior a data de inicio de vig�ncia da regra atual."
    '    mskDtIniVig.SetFocus
    '    Altera_regra = False
    '    Exit Function
    'ElseIf CDate(Format(Date, "dd/mm/yyyy")) > CDate(Format(mskDtIniVig, "dd/mm/yyyy")) Then
    '    MsgBox "A data de inicio de vig�ncia informada deve ser superior � data atual."
    '    mskDtIniVig.SetFocus
    '    Altera_regra = False
    '    Exit Function
    ElseIf IsDate(mskDtFimVig.Text) Then
        If (CDate(Format(mskDtFimVig.Text, "dd/mm/yyyy")) < CDate(Format(mskDtIniVig.Text, "dd/mm/yyyy"))) And IsDate(Format(mskDtFimVig.Text, "dd/mm/yyyy")) Then
            MsgBox "Data de fim de vig�ncia deve ser maior que a data de in�cio de vig�ncia.", vbOKOnly + vbExclamation
            mskDtFimVig.SetFocus
            Altera_regra = False
            Exit Function
        End If
    End If
    
    '---Se nova data de in�cio ou nova data de fim est� no intervalo de vig�ncia da configura��o existente, n�o grava
    'If (CDate(Format(mskDtIniVig.Text, "dd/mm/yyyy")) >= CDate(Format(grid_dt_inicio_vigencia, "dd/mm/yyyy")) And CDate(Format(mskDtIniVig.Text, "dd/mm/yyyy")) <= CDate(Format(grid_dt_fim_vigencia, "dd/mm/yyyy"))) Or _
    '      (CDate(Format(mskDtFimVig.Text, "dd/mm/yyyy")) >= CDate(Format(grid_dt_inicio_vigencia, "dd/mm/yyyy")) And CDate(Format(mskDtFimVig.Text, "dd/mm/yyyy")) <= CDate(Format(grid_dt_fim_vigencia, "dd/mm/yyyy"))) Then
    '        MsgBox "Configura��es de regra de al�ada j� existente e com vig�ncia nesse per�odo. "
    '        Altera_regra = False
    '        Exit Function
    'End If
    '---
    'If Not Verifica_fim_de_vigencia_alteracao(cboProduto.ItemData(cboProduto.ListIndex), cboTecnico.ItemData(cboTecnico.ListIndex), cboItem_EstimativaInc.ItemData(cboItem_EstimativaInc.ListIndex), cboRamo.ItemData(cboRamo.ListIndex), grid_limite_aprovacao_estimativa_id) Then
    '    'J� tem um outra al�ada com essas configura��es
    '    If Not Verifica_fim_de_vigencia(cboProduto.ItemData(cboProduto.ListIndex), cboTecnico.ItemData(cboTecnico.ListIndex), cboItem_EstimativaInc.ItemData(cboItem_EstimativaInc.ListIndex), cboRamo.ItemData(cboRamo.ListIndex)) Then
    '        Exit Function
    '    End If
    'End If
    
    If Altera_regra Then
            'Exclui regra anterior
            SQL = "exec seguros_db.dbo.SEGS14490_SPU "
            SQL = SQL & grid_limite_aprovacao_estimativa_id
            SQL = SQL & ", '" & cUserName & "' "
            
            Conexao_ExecutarSQL gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, SQL, lConexaoLocal, False
            
            strDataInicioVigencia = Format(mskDtIniVig.Text & " " & Time, "yyyymmdd HH:nn:ss") & "." & Right(Format(Timer, "#0.00"), 2)
                        
            'Inclui regra nova
            SQL = "exec seguros_db.dbo.SEGS14491_SPI "
            SQL = SQL & " @TECNICO_ID                 = " & grid_tecnico_id & ",        " & vbNewLine
            SQL = SQL & " @PRODUTO_ID                 = " & grid_produto_id & ",        " & vbNewLine
            SQL = SQL & " @RAMO_ID                    = " & grid_ramo_id & ",           " & vbNewLine
            SQL = SQL & " @ITEM_ESTIMATIVA            = " & grid_Item_Estimativa & ",   " & vbNewLine
            SQL = SQL & " @VAL_LIMITE_APROVACAO       = " & "0" & ",        " & vbNewLine
            'SQL = SQL & " @DT_INICIO_VIGENCIA         = '" & Format(mskDtIniVig.Text, "yyyymmdd") & "', " & vbNewLine
            SQL = SQL & " @DT_INICIO_VIGENCIA         = '" & strDataInicioVigencia & "', " & vbNewLine
            If IsDate(mskDtFimVig.Text) Then
               'SQL = SQL & " @DT_FIM_VIGENCIA        = '" & Format(mskDtFimVig.Text, "yyyymmdd") & "',                      " & vbNewLine
               strDataFimVigencia = Format(mskDtFimVig.Text & " " & Time, "yyyymmdd HH:nn:ss") & "." & Right(Format(Timer, "#0.00"), 2)
               SQL = SQL & " @DT_FIM_VIGENCIA        = '" & strDataFimVigencia & "',                      " & vbNewLine
            End If
            SQL = SQL & "  @USUARIO        = '" & cUserName & "'"
            
            Conexao_ExecutarSQL gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, SQL, lConexaoLocal, False
        
            Altera_regra = True
    End If

    Exit Function

Erro:
   TrataErroGeral "Altera_regra"
   TerminaSEGBR

End Function

Public Function Inclui_tecnico() As Boolean
    Dim SQL As String
    Dim rs As Recordset
    Dim LinhasGrid As Integer
    Dim i As Integer
    Dim grid_Val_Limite
    Dim grid_produto_id As Integer
    Dim grid_ramo_id As Integer
    Dim grid_tecnico_id As String
    Dim grid_Item_Estimativa As Integer
    Dim grid_dt_inicio_vigencia As String
    Dim grid_dt_fim_vigencia As String
    Dim grid_Ilimitado As String
    Dim grid_Liberado_Aprov_Maior As String
    Dim grid_alcada_liquida As String
    
    On Error GoTo Erro
    Screen.MousePointer = vbHourglass
    Inclui_tecnico = False
    
        
    grid_tecnico_id = cboTecnico.ItemData(cboTecnico.ListIndex)

    grid_produto_id = cboProduto.ItemData(cboProduto.ListIndex)

    grid_ramo_id = cboRamo.ItemData(cboRamo.ListIndex)
    
    grid_Item_Estimativa = cboItem_EstimativaInc.ItemData(cboItem_EstimativaInc.ListIndex)
    
    grid_Val_Limite = 0 'TrocaVirgulaPorPonto(Format(mskValLimite.Text, "###############0.00"))
     
    'grid_dt_inicio_vigencia = Format(mskDtIniVig.Text, "yyyymmdd")
    grid_dt_inicio_vigencia = Format(mskDtIniVig.Text & " " & Time, "yyyymmdd HH:nn:ss") & "." & Right(Format(Timer, "#0.00"), 2)
    
    'grid_dt_fim_vigencia = Format(mskDtFimVig.Text, "yyyymmdd")
    grid_dt_fim_vigencia = Format(mskDtFimVig.Text & " " & Time, "yyyymmdd HH:nn:ss") & "." & Right(Format(Timer, "#0.00"), 2)
                                    
    SQL = "exec seguros_db.dbo.SEGS14491_SPI "
    SQL = SQL & " @TECNICO_ID                 = " & grid_tecnico_id & ",        " & vbNewLine
    SQL = SQL & " @PRODUTO_ID                 = " & grid_produto_id & ",        " & vbNewLine
    SQL = SQL & " @RAMO_ID                    = " & grid_ramo_id & ",           " & vbNewLine
    SQL = SQL & " @ITEM_ESTIMATIVA            = " & grid_Item_Estimativa & ",   " & vbNewLine
    SQL = SQL & " @VAL_LIMITE_APROVACAO       = " & grid_Val_Limite & ",        " & vbNewLine
    SQL = SQL & " @DT_INICIO_VIGENCIA         = '" & grid_dt_inicio_vigencia & "', " & vbNewLine

    If IsDate(mskDtFimVig.Text) Then
       SQL = SQL & " @DT_FIM_VIGENCIA        = '" & grid_dt_fim_vigencia & "', " & vbNewLine
    End If
    SQL = SQL & "  @USUARIO        = '" & cUserName & "'"
    
        
    AbrirTransacao 1
    Conexao_ExecutarSQL gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, SQL, lConexaoLocal, False
    ConfirmarTransacao 1
    
    Screen.MousePointer = vbDefault
    
    Inclui_tecnico = True
    
    Exit Function
    
Erro:
        RetornarTransacao 1
        Screen.MousePointer = vbDefault
        TrataErroGeral "inclui_regras"
        TerminaSEGBR

End Function


Public Function Dados_Validos() As Boolean
    Dim i, k, j As Integer
    Dados_Validos = True
    
        Screen.MousePointer = vbHourglass
        
        'Valida interface
'        If Not Verifica_Campos Then
'            Screen.MousePointer = vbDefault
'            Dados_Validos = False
'            Exit Function
'        End If
        
        If Trim(cboProduto.Text) <> "TODOS" And Trim(cboItem_EstimativaInc.Text) <> "TODOS" And cboRamo.Text <> "TODOS" Then
            
            'Verifica se tem no banco
            If Verifica_fim_de_vigencia(cboProduto.ItemData(cboProduto.ListIndex), cboTecnico.ItemData(cboTecnico.ListIndex), cboItem_EstimativaInc.ItemData(cboItem_EstimativaInc.ListIndex), cboRamo.ItemData(cboRamo.ListIndex)) Then
                If sugestao_dt_inicio_vigencia <> "" Then
                    mskDtIniVig.Text = Format(sugestao_dt_inicio_vigencia, "dd/mm/yyyy")
                End If
            Else
                Screen.MousePointer = vbDefault
                Dados_Validos = False
                Exit Function
            End If
            
        Else
            If Trim(cboProduto.Text) = "TODOS" And Trim(cboItem_EstimativaInc.Text) <> "TODOS" And cboRamo.Text = "TODOS" Then
                For i = 1 To cboProduto.ListCount - 1
                    'cboProduto.ListIndex = I
                    Call Carrega_cboRamoInc(cboRamo, cboProduto.ItemData(i), True)
                    For k = 1 To cboRamo.ListCount - 1
                        If Verifica_fim_de_vigencia(cboProduto.ItemData(i), cboTecnico.ItemData(cboTecnico.ListIndex), cboItem_EstimativaInc.ItemData(cboItem_EstimativaInc.ListIndex), cboRamo.ItemData(k)) Then
                            If sugestao_dt_inicio_vigencia <> "" Then
                                mskDtIniVig.Text = Format(sugestao_dt_inicio_vigencia, "dd/mm/yyyy")
                            End If
                        Else
                            Screen.MousePointer = vbDefault
                            Dados_Validos = False
                            Exit Function
                        End If
                        
                    Next
                Next
                cboProduto.ListIndex = 0
                cboRamo.ListIndex = 0
                Call Carrega_cboRamoInc(cboRamo, cboProduto.ItemData(0), True)
                
            ElseIf Trim(cboProduto.Text) = "TODOS" And Trim(cboItem_EstimativaInc.Text) <> "TODOS" And cboRamo.Text <> "TODOS" Then
                For i = 1 To cboProduto.ListCount - 1
                    If Verifica_fim_de_vigencia(cboProduto.ItemData(i), cboTecnico.ItemData(cboTecnico.ListIndex), cboItem_EstimativaInc.ItemData(cboItem_EstimativaInc.ListIndex), cboRamo.ItemData(cboRamo.ListIndex)) Then
                        If sugestao_dt_inicio_vigencia <> "" Then
                            mskDtIniVig.Text = Format(sugestao_dt_inicio_vigencia, "dd/mm/yyyy")
                        End If
                    Else
                        Screen.MousePointer = vbDefault
                        Dados_Validos = False
                        Exit Function
                    End If

                Next
                cboProduto.ListIndex = 0
                
            ElseIf Trim(cboProduto.Text) <> "TODOS" And Trim(cboItem_EstimativaInc.Text) <> "TODOS" And cboRamo.Text = "TODOS" Then
                For k = 1 To cboRamo.ListCount - 1
                    If Verifica_fim_de_vigencia(cboProduto.ItemData(cboProduto.ListIndex), cboTecnico.ItemData(cboTecnico.ListIndex), cboItem_EstimativaInc.ItemData(cboItem_EstimativaInc.ListIndex), cboRamo.ItemData(k)) Then
                        If sugestao_dt_inicio_vigencia <> "" Then
                            mskDtIniVig.Text = Format(sugestao_dt_inicio_vigencia, "dd/mm/yyyy")
                        End If
                    Else
                        Screen.MousePointer = vbDefault
                        Dados_Validos = False
                        Exit Function
                    End If

                Next
                cboRamo.ListIndex = 0
                
            ElseIf Trim(cboProduto.Text) <> "TODOS" And Trim(cboRamo.Text) <> "TODOS" And Trim(cboItem_EstimativaInc.Text) = "TODOS" Then
                'Um produto, um ramo e todas as estimativas
                'Limpa matriz
                For i = 1 To cboItem_EstimativaInc.ListCount - 1
                    'cboItem_EstimativaInc.ListIndex = I
                    'Verifica se tem no banco
                    If Verifica_fim_de_vigencia(cboProduto.ItemData(cboProduto.ListIndex), cboTecnico.ItemData(cboTecnico.ListIndex), cboItem_EstimativaInc.ItemData(i), cboRamo.ItemData(cboRamo.ListIndex)) Then
                        'N�o tem no banco
                        
                        'Inclui na matriz
                        Incluir_matriz cboTecnico.ItemData(cboTecnico.ListIndex), cboProduto.ItemData(cboProduto.ListIndex), cboRamo.ItemData(cboRamo.ListIndex), cboItem_EstimativaInc.ItemData(i), mskValLimite.Text, mskDtIniVig.Text, mskDtFimVig.Text
                        
                        'If sugestao_dt_inicio_vigencia <> "" Then
                        '    mskDtIniVig.Text = Format(sugestao_dt_inicio_vigencia, "dd/mm/yyyy")
                        'End If
                    Else
                        Screen.MousePointer = vbDefault
                        Dados_Validos = False
                        Exit Function
                    End If

                Next
                cboItem_EstimativaInc.ListIndex = 0
                        
            ElseIf Trim(cboProduto.Text) <> "TODOS" And Trim(cboItem_EstimativaInc.Text) = "TODOS" And Trim(cboRamo.Text) = "TODOS" Then
                'ok
                For i = 1 To cboItem_EstimativaInc.ListCount - 1
                    For k = 1 To cboRamo.ListCount - 1
                        If Verifica_fim_de_vigencia(cboProduto.ItemData(cboProduto.ListIndex), cboTecnico.ItemData(cboTecnico.ListIndex), cboItem_EstimativaInc.ItemData(i), cboRamo.ItemData(k)) Then
                            If sugestao_dt_inicio_vigencia <> "" Then
                                mskDtIniVig.Text = Format(sugestao_dt_inicio_vigencia, "dd/mm/yyyy")
                            End If
                        Else
                            Screen.MousePointer = vbDefault
                            Dados_Validos = False
                            Exit Function
                        End If
                        
                    Next
                Next
                cboItem_EstimativaInc.ListIndex = 0
                cboRamo.ListIndex = 0
                        
            ElseIf Trim(cboProduto.Text) = "TODOS" And Trim(cboItem_EstimativaInc.Text) = "TODOS" And Trim(cboRamo.Text) = "TODOS" Then
                For i = 1 To cboProduto.ListCount - 1
                    'cboProduto.ListIndex = i
                    For j = 1 To cboItem_EstimativaInc.ListCount - 1
                        'cboItem_EstimativaInc.ListIndex = j
                        Call Carrega_cboRamoInc(cboRamo, cboProduto.ItemData(i), True)
                        For k = 1 To cboRamo.ListCount - 1
                            If Verifica_fim_de_vigencia(cboProduto.ItemData(i), cboTecnico.ItemData(cboTecnico.ListIndex), cboItem_EstimativaInc.ItemData(j), cboRamo.ItemData(cboRamo.ListIndex)) Then
                                If sugestao_dt_inicio_vigencia <> "" Then
                                    mskDtIniVig.Text = Format(sugestao_dt_inicio_vigencia, "dd/mm/yyyy")
                                End If
                            Else
                               Screen.MousePointer = vbDefault
                               Dados_Validos = False
                               Exit Function
                            End If
 
                        Next
                    Next
                Next
                cboItem_EstimativaInc.ListIndex = 0
                cboProduto.ListIndex = 0
                cboRamo.ListIndex = 0
                Call Carrega_cboRamoInc(cboRamo, cboProduto.ItemData(0), True)
            End If
        End If
        Screen.MousePointer = vbDefault

End Function



Private Function Verifica_Campos() As Boolean
    
    Verifica_Campos = True
    
    If Trim(cboTecnico.Text) = "" Then
       'MsgBox "Selecione o Tecnico.", vbOKOnly + vbExclamation
       cboTecnico.SetFocus
       Verifica_Campos = False
       Exit Function
    End If
    
    If Trim(cboProduto.Text) = "" Or cboProduto.Text = "Todos" Then
       'MsgBox "Selecione o Produto.", vbOKOnly + vbExclamation
       cboProduto.SetFocus
       Verifica_Campos = False
       Exit Function
    End If
    
    If Trim(cboRamo.Text) = "" Or cboRamo.Text = "Todos" Then
       'MsgBox "Selecione o Ramo.", vbOKOnly + vbExclamation
       cboRamo.SetFocus
       Verifica_Campos = False
       Exit Function
    End If
    
    If Trim(cboItem_EstimativaInc.Text) = "Selecione" Then
       'MsgBox "Selecione o Item de Estimativa.", vbOKOnly + vbExclamation
       cboItem_EstimativaInc.SetFocus
       Verifica_Campos = False
       Exit Function
    End If
    
        
    If Not VerificaData2(mskDtIniVig.Text) Then
       'MsgBox "Data de in�cio da vig�ncia inv�lida.", vbOKOnly + vbExclamation
       mskDtIniVig.SetFocus
       Verifica_Campos = False
       Exit Function
    End If
    
    If Trim(mskDtIniVig.ClipText) = "" Then
       'MsgBox "Digite a data de in�cio da vig�ncia.", vbOKOnly + vbExclamation
       mskDtIniVig.SetFocus
       Verifica_Campos = False
       Exit Function
    End If
    
    If Trim(mskDtFimVig.ClipText) <> "" Then
         If Not VerificaData2(mskDtFimVig.Text) Then
            'MsgBox "Digite corretamente a data de fim de vig�ncia.", vbOKOnly + vbExclamation
            mskDtFimVig.SetFocus
            Verifica_Campos = False
            Exit Function
         End If
         If Format(mskDtFimVig.Text, "yyyymmdd") < Format(mskDtIniVig.Text, "yyyymmdd") Then
            MsgBox "Data de fim de vig�ncia deve ser maior que a data de in�cio de vig�ncia.", vbOKOnly + vbExclamation
            mskDtFimVig.SetFocus
            Verifica_Campos = False
            Exit Function
         End If
    End If
    
End Function

Public Sub Incluir_matriz(Tecnico, Produto, Ramo, Estimativa, ValAprovacao, IniVigencia, FimVigencia)
    
    ReDim arrTecnicos(1, 8)
    
    
    
    
End Sub

Private Function Verifica_fim_de_vigencia(ByVal pproduto As Integer, ByVal pTecnico As Integer, ByVal pItem_Estimativa As String, ByVal pramo As Integer) As Boolean
    Dim sSQL As String
    Dim rs As Recordset
    Dim lvalor_inicio
    Dim lvalor_fim
    Dim respmsg As Integer
    Dim ldt_fim_vigencia As Date
    
    'Verifica se j� existe a configura��o selecionada ainda em vig�ncia (com data de vig�ncia nula)
    'ou com uma data de fim de vig�ncia � frente da data de in�cio escolhida
    
    On Error GoTo Erro
    Verifica_fim_de_vigencia = True
    sugestao_dt_inicio_vigencia = ""
    
    
    sSQL = "EXEC seguros_db.dbo.SEGS14489_SPS                    " & vbNewLine
        sSQL = sSQL & " @tecnico_id           = " & pTecnico & ",      " & vbNewLine
        sSQL = sSQL & " @produto_id           = " & pproduto & ",      " & vbNewLine
        sSQL = sSQL & " @ramo_id              = " & pramo & ",         " & vbNewLine
        sSQL = sSQL & " @item_estimativa      = " & pItem_Estimativa & ""
        'Essa procedure acima s� verifica se a configura��o n�o tem data de fim de vig�ncia ou a data de fim de vig�ncia � maior que a data corrente
        'Mas pode haver o caso de uma configura��o com data de fim de vig�ncia nula ou a data de fim de vig�ncia maior que a data corrente e a data da in�cio MAIOR que a data corrente
        'Ou seja j� existe mas ainda n�o est� valendo. Com data de in�cio futura.
        'Nesse caso a procedure vai achar essa configura��o
        'Ou seja, se a procedure achar, � porque tem configura��o v�lida.
        'Se j� tem configura��o v�lida, tem que preencher a data final dessa nova configura��o, porque se deixar sem data, essa configura��o pode
        'se estender para al�m da data da outra configura��o e a� teremos duas configura��es v�lidas, simultaneamente.
    
    Set rs = Conexao_ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, sSQL, lConexaoLocal, True)
    
    If Not rs.EOF Then
        
        '-- Se a configura��o atual n�o tem data de fim de vigencia n�o grava
        If IsNull(rs!dt_fim_vigencia) Or Year(rs!dt_fim_vigencia) = "1900" Then
            MsgBox "Configura��e de regra de al�ada j� existente. " & vbCrLf & _
                    "Tecnico: " & rs!Tecnico & vbCrLf & _
                    "Produto: " & rs!Produto & vbCrLf & _
                    "Ramo: " & rs!Ramo & vbCrLf & _
                    "Item Estimativa: " & cboItem_EstimativaInc.List(pItem_Estimativa) & vbCrLf
            Verifica_fim_de_vigencia = False
            Exit Function
        End If
        
        '--- Exige uma data de fim da nova configura��o
        If Not IsDate(mskDtFimVig.Text) Then
            MsgBox "Informe uma data de fim de vig�ncia, pois j� existe uma regra de al�ada com essas configura��es em vig�ncia ou com vig�ncia futura."
            Verifica_fim_de_vigencia = False
            Exit Function
        End If
        '---
        
        '---Se nova data de in�cio ou nova data de fim est� no intervalo de vig�ncia da configura��o existente, n�o grava
        'If Operacao <> "Alteracao" Then
            If (CDate(Format(mskDtIniVig.Text, "dd/mm/yyyy")) > CDate(Format(rs!dt_inicio_vigencia, "dd/mm/yyyy")) And CDate(Format(mskDtIniVig.Text, "dd/mm/yyyy")) < CDate(Format(rs!dt_fim_vigencia, "dd/mm/yyyy"))) Or _
               (CDate(Format(mskDtFimVig.Text, "dd/mm/yyyy")) > CDate(Format(rs!dt_inicio_vigencia, "dd/mm/yyyy")) And CDate(Format(mskDtFimVig.Text, "dd/mm/yyyy")) < CDate(Format(rs!dt_fim_vigencia, "dd/mm/yyyy"))) Then
                MsgBox "Configura��e de regra de al�ada j� existente e com vig�ncia nesse per�odo. "
                Verifica_fim_de_vigencia = False
            End If
        'End If
        '---
        
        'If CDate(Format(rs!dt_fim_vigencia, "dd/mm/yyyy")) >= CDate(Format(Date, "dd/mm/yyyy")) Then
        '     MsgBox "Configura��e de regra de al�ada j� existente. " & vbCrLf & _
        '            "Tecnico: " & rs!Tecnico & vbCrLf & _
        '            "Produto: " & rs!produto_id & " - " & rs!Produto & vbCrLf & _
        '            "Ramo: " & rs!ramo_id & " - " & rs!Ramo & vbCrLf & _
        '            "Item Estimativa: " & cboItem_EstimativaInc.List(pItem_Estimativa) & vbCrLf & _
        '            "Fim de Vig�ncia: " & Format(rs!dt_fim_vigencia, "dd/mm/yyyy") & vbCrLf
        '     Verifica_fim_de_vigencia = False
        '     Exit Function
        'End If
    End If
    
    rs.Close
    Exit Function
        
Erro:
       TrataErroGeral "Verifica_fim_de_vigencia"
       TerminaSEGBR

End Function




Private Sub cmdSair_Click()
    alterou = True
    frmAlcada.Refresh
    Unload Me
End Sub

Public Sub Carrega_Configuracao( _
    ByVal produto_id As Integer, ByVal ramo_id As Integer, ByVal tecnico_id As Integer, ByVal Item_val_estimativa As String, _
    ByVal ValLimite As String, ByVal Dt_Ini_Vig As String, ByVal Dt_Fim_Vig As String)
            
    On Error GoTo MErr:
        
    Call Encontra_Produto(produto_id)
    Call Encontra_Tecnico(tecnico_id)
    Call Encontra_Item_Val_Estimativa(Item_val_estimativa)
    cboItem_EstimativaInc.ListIndex = 1
    Call Encontra_Ramo(ramo_id)
    
    mskDtIniVig.Text = Dt_Ini_Vig
    mskDtFimVig.Text = IIf(Dt_Fim_Vig = "", "  /  /    ", Dt_Fim_Vig)
        
    DoEvents
    Screen.MousePointer = vbDefault
    
Exit Sub
MErr:
    Screen.MousePointer = vbDefault
    MsgBox "Erro ao carregar as configura��es do t�cnico." & Chr(13) & Chr(13) & Err.Number & " - " & Err.Description, vbCritical + vbOKOnly, "Aviso"
End Sub



Private Sub Encontra_Item_Val_Estimativa(ByVal Item_val_estimativa As String)
        
        Dim i As Integer
        
        For i = 0 To cboItem_EstimativaInc.ListCount - 1
            If cboItem_EstimativaInc.ItemData(i) = Item_val_estimativa Then
               cboItem_EstimativaInc.ListIndex = i
               Exit For
            End If
        Next
End Sub


Private Sub Encontra_Ramo(ByVal ramo_id As Integer)
        
        Dim i As Integer
        
        For i = 0 To cboRamo.ListCount - 1
            If cboRamo.ItemData(i) = ramo_id Then
               cboRamo.ListIndex = i
               Exit For
            End If
        Next
        
End Sub



Private Sub Encontra_Produto(ByVal produto_id As Integer)
        
        Dim i As Integer
        
        For i = 0 To cboProduto.ListCount - 1
            If cboProduto.ItemData(i) = produto_id Then
               cboProduto.ListIndex = i
               Exit For
            End If
        Next
End Sub


Private Sub Encontra_Tecnico(ByVal tecnico_id As Integer)
        
        Dim i As Integer
        
        For i = 0 To cboTecnico.ListCount - 1
            If cboTecnico.ItemData(i) = tecnico_id Then
               cboTecnico.ListIndex = i
               Exit For
            End If
        Next
        
End Sub



Private Sub Command_Click()
    cboItem_EstimativaInc.ListIndex = 1
End Sub

Private Sub Form_Load()
    
    'Centraliza Formul�rio
    Call CentraFrm(Me)
    alterou = False
    Me.Caption = "SEGP1509 - Autoriza��o de Altera��o de Estimativa "
        
    'Carrega as combos do formul�rio
    Call Carrega_cboProdutoInc(cboProduto)
    Call Carrega_cboTecnico(cboTecnico, False)
    Call Carrega_cboItem_EstimativaInc(cboItem_EstimativaInc)
    cboItem_EstimativaInc.ListIndex = 1
    cboItem_EstimativaInc.Locked = True
    Call Carrega_cboRamoInc(cboRamo, cboProduto.ItemData(cboProduto.ListIndex), False)
    
    Call CentraFrm(Me)
    
    If Operacao = "Alteracao" Then
        Me.Caption = "SEGP1509 - Autoriza��o de Altera��o de Estimativa "
        cboProduto.Locked = True
        cboProduto.Enabled = False
        cboTecnico.Locked = True
        cboTecnico.Enabled = False
        cboItem_EstimativaInc.Locked = True
        cboItem_EstimativaInc.Enabled = False
        cboRamo.Locked = True
        cboRamo.Enabled = False
        cmdAplicar.Enabled = True
        
    Else
        alterou = False
        Me.Caption = "SEGP1509 - Autoriza��o de Altera��o de Estimativa "
                
        'Inicializa valores
        cboProduto.ListIndex = -1
        cboTecnico.ListIndex = -1
        cboRamo.ListIndex = -1
        mskDtIniVig.Text = Format(Date, "dd/mm/yyyy")
    End If

End Sub

Public Sub Carrega_cboRamoInc(ByRef cboAux As ComboBox, ByRef cboAux_Produto As Integer, Optional ByVal Todos As Boolean = False)
    Dim SQL As String
    Dim lConexaoLocal As Integer
    Dim rs As Recordset
    
    cboAux.Clear
    
    If Todos Then
       cboAux.AddItem "TODOS"
       cboAux.ItemData(cboAux.NewIndex) = 0
    End If
    If cboAux_Produto <> 0 Then
       SQL = " Select i.ramo_id , r.nome from item_produto_tb i"
       SQL = SQL & " Join ramo_tb r on i.ramo_id = r.ramo_id "
       SQL = SQL & " Where i.produto_id = " & cboAux_Produto
       
       Set rs = Conexao_ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, SQL, lConexaoLocal, True)
       
       While Not rs.EOF
           cboAux.AddItem rs("ramo_id") & " - " & rs("nome")
           cboAux.ItemData(cboAux.NewIndex) = rs("ramo_id")
           rs.MoveNext
           
       Wend
       rs.Close
    End If
    
    cboAux.ListIndex = 0
    
End Sub

Public Sub Carrega_cboItem_EstimativaInc(ByRef cboAux As ComboBox)
Dim cont As Integer
    
    cboAux.Clear
    
    Call Carrega_Matriz_Item_Estimativa
 
    cboAux.AddItem "Selecione"
    cboAux.ItemData(cboAux.NewIndex) = 1
    For cont = 1 To 8
        cboAux.AddItem Item_estimativa(cont).Nome
        cboAux.ItemData(cboAux.NewIndex) = Item_estimativa(cont).ID
    Next
    cboAux.ListIndex = 1
End Sub

Public Sub Carrega_cboProdutoInc(ByRef cboAux As ComboBox)
    Dim SQL As String
    Dim lConexaoLocal As Integer
    Dim rs As Recordset
    
    cboAux.Clear
    
    SQL = "SELECT produto_id, nome"
    SQL = SQL & " FROM produto_tb"
    SQL = SQL & " WHERE ativo = 's'"
    SQL = SQL & " ORDER BY produto_id"
    
    Set rs = Conexao_ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, SQL, lConexaoLocal, True)
    
    While Not rs.EOF
        cboAux.AddItem Format(rs("produto_id"), "000") & " - " & rs("nome")
        cboAux.ItemData(cboAux.NewIndex) = rs("produto_id")
        rs.MoveNext
    Wend
    rs.Close
    cboProduto.ListIndex = 0
    
End Sub





Public Function Verifica_fim_de_vigencia_alteracao(ByVal pproduto As Integer, ByVal pTecnico As Integer, ByVal pItem_Estimativa As String, ByVal pramo As Integer, limite_aprovacao_estimativa_id As Integer) As Boolean
    Dim sSQL As String
    Dim rs As Recordset
    Dim lvalor_inicio
    Dim lvalor_fim
    Dim respmsg As Integer
    Dim ldt_fim_vigencia As Date
    
    'Verifica se j� existe a configura��o selecionada ainda em vig�ncia (com data de vig�ncia nula)
    'ou com uma data de fim de vig�ncia � frente da data de in�cio escolhida
    
    On Error GoTo Erro
    Verifica_fim_de_vigencia_alteracao = False
    sugestao_dt_inicio_vigencia = ""
    
    
    sSQL = "SELECT tecnico_tb.nome as Tecnico , produto_tb.nome as Produto, ramo_tb.nome as Ramo, limite_aprovacao_estimativa_tb.item_estimativa, "
    sSQL = sSQL & " limite_aprovacao_estimativa_tb.val_limite_aprovacao , limite_aprovacao_estimativa_tb.dt_inicio_vigencia, "
    'sSQL = sSQL & " isnull(limite_aprovacao_estimativa_tb.dt_fim_vigencia, '''') as dt_fim_vigencia,
    sSQL = sSQL & " limite_aprovacao_estimativa_tb.produto_id , "
    sSQL = sSQL & " limite_aprovacao_estimativa_tb.tecnico_id, limite_aprovacao_estimativa_tb.ramo_id "
    sSQL = sSQL & " from seguros_db.dbo.limite_aprovacao_estimativa_tb limite_aprovacao_estimativa_tb "
    sSQL = sSQL & " INNER JOIN seguros_db.dbo.tecnico_tb tecnico_tb ON  limite_aprovacao_estimativa_tb.tecnico_id = tecnico_tb.tecnico_id "
    sSQL = sSQL & " INNER JOIN seguros_db.dbo.ramo_tb ramo_tb ON  limite_aprovacao_estimativa_tb.ramo_id = ramo_tb.ramo_id  "
    sSQL = sSQL & " INNER JOIN seguros_db.dbo.produto_tb produto_tb ON limite_aprovacao_estimativa_tb.produto_id = produto_tb.produto_id  "
    sSQL = sSQL & " WHERE (limite_aprovacao_estimativa_tb.dt_fim_vigencia > GetDate() or limite_aprovacao_estimativa_tb.dt_fim_vigencia is null)"
    
    sSQL = sSQL & " AND limite_aprovacao_estimativa_id <> " & limite_aprovacao_estimativa_id
    
    sSQL = sSQL & " AND tecnico_tb.tecnico_id = " & pTecnico
    sSQL = sSQL & " AND produto_tb.produto_id = " & pproduto
    sSQL = sSQL & " AND ramo_tb.ramo_id = " & pramo
    sSQL = sSQL & " AND limite_aprovacao_estimativa_tb.item_estimativa = " & pItem_Estimativa
    sSQL = sSQL & " ORDER BY tecnico_tb.Nome , produto_tb.Nome, limite_aprovacao_estimativa_tb.Item_estimativa, limite_aprovacao_estimativa_tb.dt_inicio_vigencia "
    
    
        'Essa procedure acima s� verifica se a configura��o n�o tem data de fim de vig�ncia ou a data de fim de vig�ncia � maior que a data corrente
        'Mas pode haver o caso de uma configura��o com data de fim de vig�ncia nula ou a data de fim de vig�ncia maior que a data corrente e a data da in�cio MAIOR que a data corrente
        'Ou seja j� existe mas ainda n�o est� valendo. Com data de in�cio futura.
        'Nesse caso a procedure vai achar essa configura��o
        'Ou seja, se a procedure achar, � porque tem configura��o v�lida.
        'Se j� tem configura��o v�lida, tem que preencher a data final dessa nova configura��o, porque se deixar sem data, essa configura��o pode
        'se estender para al�m da data da outra configura��o e a� teremos duas configura��es v�lidas, simultaneamente.
    
    Set rs = Conexao_ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, sSQL, lConexaoLocal, True)
    
    If Not rs.EOF Then
    
        Verifica_fim_de_vigencia_alteracao = False
    
    
        
'        '-- Se a configura��o atual n�o tem data de fim de vigencia n�o grava
'        If IsNull(rs!dt_fim_vigencia) Or Year(rs!dt_fim_vigencia) = "1900" Then
'            MsgBox "Configura��e de regra de al�ada j� existente. " & vbCrLf & _
'                    "Tecnico: " & rs!Tecnico & vbCrLf & _
'                    "Produto: " & rs!Produto & vbCrLf & _
'                    "Ramo: " & rs!Ramo & vbCrLf & _
'                    "Item Estimativa: " & cboItem_EstimativaInc.List(pItem_Estimativa) & vbCrLf
'            Verifica_fim_de_vigencia_alteracao = False
'            Exit Function
'        End If
        
'        '--- Exige uma data de fim da nova configura��o
'        If Not IsDate(mskDtFimVig.Text) Then
'            MsgBox "Informe uma data de fim de vig�ncia, pois j� existe uma regra de al�ada com essas configura��es em vig�ncia ou com vig�ncia futura."
'            Verifica_fim_de_vigencia_alteracao = False
'            Exit Function
'        End If
'        '---
        
'        '---Se nova data de in�cio ou nova data de fim est� no intervalo de vig�ncia da configura��o existente, n�o grava
'        'If Operacao <> "Alteracao" Then
'            If (CDate(Format(mskDtIniVig.Text, "dd/mm/yyyy")) > CDate(Format(rs!dt_inicio_vigencia, "dd/mm/yyyy")) And CDate(Format(mskDtIniVig.Text, "dd/mm/yyyy")) < CDate(Format(rs!dt_fim_vigencia, "dd/mm/yyyy"))) Or _
'               (CDate(Format(mskDtFimVig.Text, "dd/mm/yyyy")) > CDate(Format(rs!dt_inicio_vigencia, "dd/mm/yyyy")) And CDate(Format(mskDtFimVig.Text, "dd/mm/yyyy")) < CDate(Format(rs!dt_fim_vigencia, "dd/mm/yyyy"))) Then
'                MsgBox "Configura��e de regra de al�ada j� existente e com vig�ncia nesse per�odo. "
'                Verifica_fim_de_vigencia_alteracao = False
'            End If
'        'End If
'        '---
        
        'If CDate(Format(rs!dt_fim_vigencia, "dd/mm/yyyy")) >= CDate(Format(Date, "dd/mm/yyyy")) Then
        '     MsgBox "Configura��e de regra de al�ada j� existente. " & vbCrLf & _
        '            "Tecnico: " & rs!Tecnico & vbCrLf & _
        '            "Produto: " & rs!produto_id & " - " & rs!Produto & vbCrLf & _
        '            "Ramo: " & rs!ramo_id & " - " & rs!Ramo & vbCrLf & _
        '            "Item Estimativa: " & cboItem_EstimativaInc.List(pItem_Estimativa) & vbCrLf & _
        '            "Fim de Vig�ncia: " & Format(rs!dt_fim_vigencia, "dd/mm/yyyy") & vbCrLf
        '     Verifica_fim_de_vigencia = False
        '     Exit Function
        'End If
    End If
    
    rs.Close
    Exit Function
        
Erro:
       TrataErroGeral "Verifica_fim_de_vigencia"
       TerminaSEGBR


End Function
