VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.2#0"; "MSCOMCTL.OCX"
Begin VB.Form frmAlcada 
   Caption         =   "Inclus�o de pagamentos de honor�rios e despesas"
   ClientHeight    =   6765
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   12135
   LinkTopic       =   "Form1"
   ScaleHeight     =   6765
   ScaleWidth      =   12135
   StartUpPosition =   3  'Windows Default
   Begin VB.Frame fraConfiguracao 
      Height          =   5955
      Left            =   180
      TabIndex        =   1
      Top             =   0
      Width           =   11820
      Begin VB.ComboBox cboTecnico 
         Height          =   315
         Left            =   180
         Style           =   2  'Dropdown List
         TabIndex        =   9
         Top             =   540
         Width           =   4965
      End
      Begin VB.ComboBox cboItem_Estimativa 
         Height          =   315
         Left            =   5355
         Style           =   1  'Simple Combo
         TabIndex        =   8
         TabStop         =   0   'False
         Text            =   "cboItem_Estimativa"
         Top             =   1125
         Width           =   4995
      End
      Begin VB.CommandButton cmdAdicionar 
         Caption         =   "<< Adicionar"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   10530
         TabIndex        =   7
         Top             =   3105
         Width           =   1125
      End
      Begin VB.CommandButton cmdRemover 
         Caption         =   ">> Remover"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   10530
         TabIndex        =   6
         Top             =   3510
         Width           =   1125
      End
      Begin VB.ComboBox cboProduto 
         Height          =   315
         Left            =   5355
         Style           =   2  'Dropdown List
         TabIndex        =   5
         Top             =   540
         Width           =   5010
      End
      Begin VB.CommandButton cmdAlterar 
         Caption         =   "Alterar"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   10530
         TabIndex        =   4
         Top             =   3915
         Width           =   1140
      End
      Begin VB.ComboBox cboRamo 
         Height          =   315
         Left            =   180
         TabIndex        =   3
         Text            =   "cboRamo"
         Top             =   1125
         Width           =   4965
      End
      Begin VB.CommandButton cmdPesquisar 
         Caption         =   "Pesquisar"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   10530
         TabIndex        =   2
         Top             =   780
         Width           =   1125
      End
      Begin MSFlexGridLib.MSFlexGrid fgridConfiguracoes 
         Height          =   4305
         Left            =   135
         TabIndex        =   10
         Top             =   1515
         Width           =   10215
         _ExtentX        =   18018
         _ExtentY        =   7594
         _Version        =   393216
         Rows            =   1
         Cols            =   12
         FixedCols       =   0
         HighLight       =   2
         SelectionMode   =   1
         AllowUserResizing=   1
         FormatString    =   $"frmAlcada.frx":0000
      End
      Begin VB.Label lbTecti 
         Caption         =   "Tecnico:"
         Height          =   255
         Index           =   0
         Left            =   165
         TabIndex        =   14
         Top             =   315
         Width           =   660
      End
      Begin VB.Label lbCadastro 
         Caption         =   "Item de estimativa :"
         Height          =   255
         Index           =   4
         Left            =   5355
         TabIndex        =   13
         Top             =   900
         Width           =   2385
      End
      Begin VB.Label Label2 
         Caption         =   "Produto:"
         Height          =   285
         Left            =   5355
         TabIndex        =   12
         Top             =   315
         Width           =   2310
      End
      Begin VB.Label lbTecti 
         AutoSize        =   -1  'True
         Caption         =   "Ramo:"
         Height          =   195
         Index           =   1
         Left            =   180
         TabIndex        =   11
         Top             =   900
         Width           =   465
      End
   End
   Begin VB.CommandButton cmdSair 
      Caption         =   "Sair"
      Height          =   315
      Left            =   10470
      TabIndex        =   0
      Top             =   6090
      Width           =   1515
   End
   Begin MSComctlLib.StatusBar stbarPesquisa 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   15
      Top             =   6480
      Width           =   12135
      _ExtentX        =   21405
      _ExtentY        =   503
      Style           =   1
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "frmAlcada"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'Constantes para o GRID de Pesquisa
Const GRID_PESQ_TECNICO = 0
Const GRID_PESQ_PRODUTO = 1
Const GRID_PESQ_RAMO = 2
Const GRID_PESQ_ITEM_VAL_ESTIMATIVA = 3
Const GRID_PESQ_VAL_PAGAMENTO = 4
Const GRID_PESQ_DT_INI_VIG = 5
Const GRID_PESQ_DT_FIM_VIG = 6
Const GRID_PESQ_PRODUTO_ID = 7
Const GRID_PESQ_TECNICO_ID = 8
Const GRID_PESQ_RAMO_ID = 9
Const GRID_PESQ_ITEM_VAL_ESTIMATIVA_ID = 10
Const GRID_PESQ_LIMITE_APROVACAO_ESTIMATIVA_ID = 11

Private Sub cboProduto_Click()
    If cboProduto.Text <> "" Then
        Call Carrega_cboRamo(cboRamo, cboProduto.ItemData(cboProduto.ListIndex), True)
    End If
End Sub


Private Sub cmdAdicionar_Click()
    Operacao = "Inclusao"
    frmAlcadaCon.Show 1
    cmdPesquisar_Click
End Sub

Private Sub cmdAlterar_Click()
    Dim grid_Val_Limite
    Dim grid_produto_id As Integer
    Dim grid_ramo_id As Integer
    Dim grid_tecnico_id As String
    Dim grid_Item_Estimativa As Integer
    Dim grid_dt_inicio_vigencia As Date
    Dim grid_dt_final_vigencia As String
    Dim grid_limite_aprovacao_estimativa_id As Integer
    Dim fim_vigencia As String

    Operacao = "Alteracao"
    With fgridConfiguracoes
        If .Rows = 1 Then
            MsgBox "Nenhum registro existente no Grid.", vbOKOnly + vbExclamation, "Aten��o"
            Exit Sub
        End If
    End With
    
    'Screen.MousePointer = vbHourglass
    Linha = fgridConfiguracoes.RowSel
    With frmAlcadaCon
        fgridConfiguracoes.Col = GRID_PESQ_PRODUTO_ID
        grid_produto_id = fgridConfiguracoes.Text
    
        fgridConfiguracoes.Col = GRID_PESQ_VAL_PAGAMENTO
        grid_Val_Limite = TrocaVirgulaPorPonto(fgridConfiguracoes.Text)
    
        fgridConfiguracoes.Col = GRID_PESQ_RAMO_ID
        grid_ramo_id = fgridConfiguracoes.Text
        
        fgridConfiguracoes.Col = GRID_PESQ_TECNICO_ID
        grid_tecnico_id = fgridConfiguracoes.Text
        
        fgridConfiguracoes.Col = IIf(IsEmpty(GRID_PESQ_ITEM_VAL_ESTIMATIVA_ID), 0, GRID_PESQ_ITEM_VAL_ESTIMATIVA_ID)
        grid_Item_Estimativa = fgridConfiguracoes.Text
        
        fgridConfiguracoes.Col = GRID_PESQ_DT_INI_VIG
        grid_dt_inicio_vigencia = Format(fgridConfiguracoes.Text, "dd/mm/yyyy")
        
        fgridConfiguracoes.Col = GRID_PESQ_LIMITE_APROVACAO_ESTIMATIVA_ID
        grid_limite_aprovacao_estimativa_id = fgridConfiguracoes.Text
        
        If DateDiff("d", Date, grid_dt_inicio_vigencia) > 0 Then
            fim_vigencia = grid_dt_inicio_vigencia
        Else
            fim_vigencia = Date
        End If
        
        Call .Carrega_Configuracao(fgridConfiguracoes.TextMatrix(Linha, GRID_PESQ_PRODUTO_ID), fgridConfiguracoes.TextMatrix(Linha, GRID_PESQ_RAMO_ID), fgridConfiguracoes.TextMatrix(Linha, GRID_PESQ_TECNICO_ID), _
             fgridConfiguracoes.TextMatrix(Linha, GRID_PESQ_ITEM_VAL_ESTIMATIVA_ID), fgridConfiguracoes.TextMatrix(Linha, GRID_PESQ_VAL_PAGAMENTO), fgridConfiguracoes.TextMatrix(Linha, GRID_PESQ_DT_INI_VIG), fgridConfiguracoes.TextMatrix(Linha, GRID_PESQ_DT_FIM_VIG))
    
         .Show 1
         
         DoEvents
         cmdPesquisar_Click
         DoEvents
    
    End With
    'Screen.MousePointer = vbNormal

End Sub

Private Sub cmdPesquisar_Click()
    Dim SQL As String
    Dim SQL_aux As String
    Dim i As Integer
    Dim Item_val_estimativa As String
    Dim rs As ADODB.Recordset
    
    On Error GoTo Erro
    
    'Retornar todos os registros em vig�ncia
    
    'Verifica se algum crit�rio foi preenchido pra pesquisa
    If (cboProduto.ListIndex = -1) And (cboTecnico.ListIndex = -1) And (cboItem_Estimativa.ListIndex = -1) Then
       Call mensagem_erro(6, "Selecione um crit�rio antes de clicar em pesquisar.")
       cboProduto.SetFocus
       Exit Sub
    End If
    
    fgridConfiguracoes.Rows = 1
    stbarPesquisa.SimpleText = fgridConfiguracoes.Rows - 1 & " Registro(s) encontrado(s)"
                    
    'Monta a clausula que ser� utilizada para pesquisar os registros
    
    If (cboTecnico.ListIndex <> -1) And (cboTecnico.ListIndex <> 0) Then
       SQL_aux = SQL_aux & " AND lim.Tecnico_id = " & cboTecnico.ItemData(cboTecnico.ListIndex)
    End If
    
    If (cboProduto.ListIndex <> -1) And (cboProduto.ListIndex <> 0) Then
       SQL_aux = SQL_aux & " AND lim.produto_id = " & cboProduto.ItemData(cboProduto.ListIndex)
    End If
    
    If (cboRamo.ListIndex <> -1) And (cboRamo.ListIndex <> 0) Then
       SQL_aux = SQL_aux & " AND lim.ramo_id = " & cboRamo.ItemData(cboRamo.ListIndex)
    End If
    
    If (cboItem_Estimativa.ListIndex <> -1) And (cboItem_Estimativa.ListIndex <> 0) Then
       SQL_aux = SQL_aux & " AND lim.item_estimativa = " & cboItem_Estimativa.ItemData(cboItem_Estimativa.ListIndex)
    End If
    
    If Right(SQL_aux, 5) = " AND " Then
       SQL_aux = Mid(SQL_aux, 1, Len(SQL_aux) - 5)
    End If
     
    'Habilitar a data de in�cio e fim de vig�ncia
    'As configura��es que tem data de fim de vig�ncia no dia da consulta, ainda s�o v�lidos
    SQL = "SELECT "
    SQL = SQL & "tec.nome Tecnico , pro.nome Produto, r.nome Ramo, lim.item_estimativa, lim.val_limite_aprovacao , lim.dt_inicio_vigencia, "
    SQL = SQL & "isnull(lim.dt_fim_vigencia, '') dt_fim_vigencia, lim.produto_id, lim.tecnico_id, lim.ramo_id, lim.LIMITE_APROVACAO_ESTIMATIVA_ID "
    SQL = SQL & "From "
    SQL = SQL & "seguros_db.dbo.LIMITE_APROVACAO_ESTIMATIVA_TB lim "
    SQL = SQL & "INNER JOIN seguros_db.dbo.tecnico_tb tec ON  lim.tecnico_id = tec.tecnico_id "
    SQL = SQL & "INNER JOIN seguros_db.dbo.ramo_tb r ON  lim.ramo_id = r.ramo_id "
    SQL = SQL & "INNER JOIN seguros_db.dbo.produto_tb pro ON lim.produto_id = pro.produto_id "
    'SQL = SQL & " WHERE (dt_fim_vigencia is null or (CONVERT(DATETIME,CONVERT(VARCHAR, dt_fim_vigencia ,112)) >= CONVERT(DATETIME,CONVERT(VARCHAR, getdate() ,112)))) "
    SQL = SQL & " WHERE (dt_fim_vigencia is null or dt_fim_vigencia >= getdate()) "
    If SQL_aux <> "" Then
       SQL = SQL & SQL_aux
    End If
    SQL = SQL & " Order By "
    SQL = SQL & "tec.Nome , pro.Nome, lim.Item_estimativa, lim.dt_inicio_vigencia "
    
    Set rs = Conexao_ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, SQL, lConexaoLocal, True)
    
    Screen.MousePointer = vbHourglass
    While Not rs.EOF
    
        Item_val_estimativa = cboItem_Estimativa.List(rs("item_estimativa"))
        fgridConfiguracoes.AddItem rs("Tecnico") & vbTab & rs("produto_id") & " - " & rs("Produto") & vbTab & rs("Ramo") & vbTab & Item_val_estimativa & vbTab & rs("val_limite_aprovacao") & vbTab & _
            Format(rs("dt_inicio_vigencia"), "dd/mm/yyyy") & vbTab & IIf(Format(rs("dt_fim_vigencia"), "dd/mm/yyyy") = "01/01/1900", "", Format(rs("dt_fim_vigencia"), "dd/mm/yyyy")) & vbTab & rs("produto_id") & vbTab & rs("tecnico_id") & vbTab & _
            rs("RAMO_id") & vbTab & rs("item_estimativa") & vbTab & rs("LIMITE_APROVACAO_ESTIMATIVA_ID")
            '& vbTab & rs("ilimitado") & vbTab & rs("liberado_aprov_maior") & vbTab & rs("alcada_liquida")
        If Format(rs("dt_fim_vigencia"), "dd/mm/yyyy") <> "01/01/1900" Then
            'Coloca em vermelho as linhas que tem data de fim de vig�ncia preenchido. Ou seja, est� vigente, mas tem data de fim vig�ncia v�lida.
            '� diferente dos que j� est�o vencidos.
            For i = 0 To fgridConfiguracoes.Cols - 1
                fgridConfiguracoes.Row = fgridConfiguracoes.Rows - 1
                fgridConfiguracoes.Col = i
                fgridConfiguracoes.CellForeColor = vbRed
            Next
        End If
        rs.MoveNext
    Wend
    Screen.MousePointer = vbDefault
    rs.Close
    
    'fgridConfiguracoes.Col = 11
    fgridConfiguracoes.ColWidth(4) = 0
     
    stbarPesquisa.SimpleText = fgridConfiguracoes.Rows - 1 & " Registro(s) encontrado(s)"
Exit Sub
Erro:
    Screen.MousePointer = vbDefault
    TrataErroGeral "cmdPesquisa_Click"
    TerminaSEGBR
End Sub

Private Sub cmdRemover_Click()
    With fgridConfiguracoes
            If .Rows = 1 Then
                MsgBox "Nenhum registro existente no Grid.", vbOKOnly + vbExclamation, "Aten��o"
                Exit Sub
            End If
            
            If fgridConfiguracoes.RowSel Then
                If MsgBox("Confirma a exclus�o da configura��o selecionada ?", vbYesNo + vbQuestion, "Aten��o") = vbYes Then
                    If .Rows > 1 Then
                        Exclui_regra
                    End If
                End If
            End If
    End With
End Sub


Private Sub Exclui_regra()
    Dim SQL As String
    Dim grid_Val_Limite
    Dim grid_produto_id As Integer
    Dim grid_ramo_id As Integer
    Dim grid_tecnico_id As String
    Dim grid_Item_Estimativa As Integer
    Dim grid_dt_inicio_vigencia As Date
    Dim grid_dt_final_vigencia As String
    Dim grid_limite_aprovacao_estimativa_id As Integer
    Dim fim_vigencia As String
    Dim rs As ADODB.Recordset
    
        On Error GoTo Erro
        
        fgridConfiguracoes.Row = fgridConfiguracoes.RowSel
        
        fgridConfiguracoes.Col = GRID_PESQ_PRODUTO_ID
        grid_produto_id = fgridConfiguracoes.Text
        
        fgridConfiguracoes.Col = GRID_PESQ_VAL_PAGAMENTO
        grid_Val_Limite = TrocaVirgulaPorPonto(fgridConfiguracoes.Text)
    
        fgridConfiguracoes.Col = GRID_PESQ_RAMO_ID
        grid_ramo_id = fgridConfiguracoes.Text
        
        fgridConfiguracoes.Col = GRID_PESQ_TECNICO_ID
        grid_tecnico_id = fgridConfiguracoes.Text
        
        fgridConfiguracoes.Col = IIf(IsEmpty(GRID_PESQ_ITEM_VAL_ESTIMATIVA_ID), 0, GRID_PESQ_ITEM_VAL_ESTIMATIVA_ID)
        grid_Item_Estimativa = fgridConfiguracoes.Text
        
        fgridConfiguracoes.Col = GRID_PESQ_DT_INI_VIG
        grid_dt_inicio_vigencia = Format(fgridConfiguracoes.Text, "dd/mm/yyyy")
        
        fgridConfiguracoes.Col = GRID_PESQ_LIMITE_APROVACAO_ESTIMATIVA_ID
        grid_limite_aprovacao_estimativa_id = fgridConfiguracoes.Text
        
        If DateDiff("d", Date, grid_dt_inicio_vigencia) > 0 Then
            fim_vigencia = grid_dt_inicio_vigencia
        Else
            fim_vigencia = Date
        End If
        
        SQL = "exec seguros_db.dbo.SEGS14490_SPU "
        SQL = SQL & grid_limite_aprovacao_estimativa_id
        SQL = SQL & ", '" & cUserName & "'"
        
        AbrirTransacao 1
        Conexao_ExecutarSQL gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, SQL, lConexaoLocal, False
        ConfirmarTransacao 1
        
        If fgridConfiguracoes.Rows > 2 Then
            fgridConfiguracoes.RemoveItem fgridConfiguracoes.RowSel
        Else
            fgridConfiguracoes.Rows = 1
        End If
        stbarPesquisa.SimpleText = fgridConfiguracoes.Rows - 1 & " Registro(s) encontrado(s)"
                
        MsgBox "Regra excluida com sucesso.", vbOKOnly, "Aten��o"
    Exit Sub
    
Erro:
       RetornarTransacao 1
       TrataErroGeral "Exclui_regra"
       TerminaSEGBR
End Sub


Private Sub cmdSair_Click()
    Screen.MousePointer = vbArrowHourglass
    Call TerminaSEGBR
    Screen.MousePointer = vbDefault
End Sub

Private Sub Form_Load()
    Call CentraFrm(Me)
    
    Carrega_cboTecnico cboTecnico, True
    Carrega_cboProduto cboProduto, True
    
    Call Carrega_cboItem_Estimativa(cboItem_Estimativa, True)
    cboItem_Estimativa.ListIndex = 1
    cboItem_Estimativa.Locked = True
    
    Carrega_cboRamo cboRamo, cboProduto.ItemData(cboProduto.ListIndex), True
    
    cmdPesquisar_Click
        
    Me.Caption = "SEGP1509 - Autoriza��o de Altera��o de Estimativa"

End Sub

