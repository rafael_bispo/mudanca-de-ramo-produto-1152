VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.2#0"; "MSCOMCTL.OCX"
Begin VB.Form frmCalculo 
   ClientHeight    =   6360
   ClientLeft      =   60
   ClientTop       =   1560
   ClientWidth     =   6555
   Icon            =   "frmCalculo.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   6360
   ScaleWidth      =   6555
   Begin VB.OptionButton optReativacao_resseguro 
      Alignment       =   1  'Right Justify
      Caption         =   "Reativa��o de Resseguro"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   240
      TabIndex        =   24
      Tag             =   "8"
      Top             =   1800
      Width           =   2655
   End
   Begin VB.Frame Frame2 
      Caption         =   "Logs"
      Height          =   2475
      Left            =   60
      TabIndex        =   13
      Top             =   3000
      Width           =   6435
      Begin VB.Label lblFim 
         BorderStyle     =   1  'Fixed Single
         Height          =   345
         Left            =   2640
         TabIndex        =   23
         Top             =   690
         Width           =   2055
      End
      Begin VB.Label lblInicio 
         BorderStyle     =   1  'Fixed Single
         Height          =   345
         Left            =   2640
         TabIndex        =   22
         Top             =   300
         Width           =   2055
      End
      Begin VB.Label Label3 
         Alignment       =   1  'Right Justify
         Caption         =   "Fim:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   1920
         TabIndex        =   21
         Top             =   765
         Width           =   675
      End
      Begin VB.Label Label2 
         Alignment       =   1  'Right Justify
         Caption         =   "In�cio:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   1860
         TabIndex        =   20
         Top             =   375
         Width           =   735
      End
      Begin VB.Label lblResseguros 
         BorderStyle     =   1  'Fixed Single
         Height          =   345
         Left            =   2640
         TabIndex        =   19
         Top             =   1080
         Width           =   2055
      End
      Begin VB.Label lblLog1 
         Alignment       =   1  'Right Justify
         Caption         =   "Resseguros Calculados:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   270
         TabIndex        =   18
         Top             =   1140
         Width           =   2325
      End
      Begin VB.Label lblLog2 
         Alignment       =   1  'Right Justify
         Caption         =   "Participa��es em Faturas:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   195
         Left            =   270
         TabIndex        =   17
         Top             =   1560
         Width           =   2325
      End
      Begin VB.Label lblDetalhes 
         BorderStyle     =   1  'Fixed Single
         Height          =   345
         Left            =   2640
         TabIndex        =   16
         Top             =   1500
         Width           =   2055
      End
      Begin VB.Label lblCancelamento 
         BorderStyle     =   1  'Fixed Single
         Height          =   345
         Left            =   2640
         TabIndex        =   15
         Top             =   1920
         Width           =   2055
      End
      Begin VB.Label lblLog3 
         Alignment       =   1  'Right Justify
         Caption         =   "Resseguros Cancelados:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   195
         Left            =   270
         TabIndex        =   14
         Top             =   1980
         Width           =   2325
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Parametros"
      Height          =   2955
      Left            =   60
      TabIndex        =   3
      Top             =   60
      Width           =   6435
      Begin VB.TextBox TxtMes 
         Height          =   285
         Left            =   2670
         MaxLength       =   2
         TabIndex        =   10
         Tag             =   "5"
         Top             =   2235
         Width           =   435
      End
      Begin VB.TextBox TxtAno 
         Height          =   285
         Left            =   2670
         MaxLength       =   4
         TabIndex        =   9
         Tag             =   "6"
         Top             =   2565
         Width           =   765
      End
      Begin VB.OptionButton optRE 
         Alignment       =   1  'Right Justify
         Caption         =   "Ramos Elementares"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   780
         TabIndex        =   8
         Tag             =   "1"
         Top             =   260
         Width           =   2055
      End
      Begin VB.OptionButton optVida 
         Alignment       =   1  'Right Justify
         Caption         =   "Vida"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   1860
         TabIndex        =   7
         Tag             =   "2"
         Top             =   590
         Width           =   975
      End
      Begin VB.OptionButton optOuroVidaGarantia 
         Alignment       =   1  'Right Justify
         Caption         =   "Ouro Vida Garantia"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Left            =   840
         TabIndex        =   6
         Tag             =   "3"
         Top             =   950
         Width           =   1995
      End
      Begin VB.OptionButton optCancelamentoRE 
         Alignment       =   1  'Right Justify
         Caption         =   "Cancelamento RE"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   780
         TabIndex        =   5
         Tag             =   "4"
         Top             =   1195
         Width           =   2055
      End
      Begin VB.OptionButton optCancelamentoVida 
         Alignment       =   1  'Right Justify
         Caption         =   "Cancelamento Vida"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   780
         TabIndex        =   4
         Tag             =   "7"
         Top             =   1500
         Width           =   2055
      End
      Begin VB.Label lblMes 
         Caption         =   "M�s:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   2190
         TabIndex        =   12
         Top             =   2265
         Width           =   375
      End
      Begin VB.Label lblAno 
         Caption         =   "Ano:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   2190
         TabIndex        =   11
         Top             =   2625
         Width           =   375
      End
   End
   Begin VB.CommandButton cmdProcessar 
      Caption         =   "Processar"
      Height          =   375
      Left            =   3180
      TabIndex        =   0
      Top             =   5595
      Width           =   1635
   End
   Begin VB.CommandButton cmdSair 
      Caption         =   "Sair"
      Height          =   375
      Left            =   4860
      TabIndex        =   1
      Top             =   5595
      Width           =   1635
   End
   Begin MSComctlLib.StatusBar StbRodape 
      Align           =   2  'Align Bottom
      Height          =   315
      Left            =   0
      TabIndex        =   2
      Top             =   6045
      Width           =   6555
      _ExtentX        =   11562
      _ExtentY        =   556
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   3
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   8997
            MinWidth        =   8997
            Text            =   "Mensagem"
            TextSave        =   "Mensagem"
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            AutoSize        =   2
            Object.Width           =   2170
            MinWidth        =   88
            Text            =   "Data do sistema"
            TextSave        =   "Data do sistema"
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            AutoSize        =   2
            Object.Width           =   1111
            MinWidth        =   88
            Text            =   "Usu�rio"
            TextSave        =   "Usu�rio"
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "frmCalculo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Const VIDA = 0
Const RE = 1
Const OURO_VIDA_GARANTIA = 2
Const CANCELAMENTO_RE = 3
Const REATIVACAO_RESSEGURO = 4

Private Sub AtualizarInterface(iTipo As Integer, _
                               lTotalRessegurosVida As Long, _
                               lTotalRessegurosVidaDetalhados As Long, _
                               lTotalRegistros As Long)
 
Dim sMensagem As String
 
On Error GoTo Trata_Erro
    
    lblFim = Now
    
    If iTipo = 0 Then
    
      lblResseguros = lTotalRessegurosVida
      lblDetalhes = lTotalRessegurosVidaDetalhados
      
      If lTotalRessegurosVida > 0 Or _
         lTotalRessegurosVidaDetalhados > 0 Then
        sMensagem = "Processamento realizado com sucesso."
      Else
        sMensagem = "Nenhuma vida foi gerada."
      End If
    
    ElseIf iTipo = 3 Then
    
      lblCancelamento.Caption = lTotalRegistros
      
      If Val(lblCancelamento.Caption) > 0 Then
        sMensagem = "Processamento realizado com sucesso."
      Else
        sMensagem = "Nenhum documento foi gerado."
      End If
    
    Else
    
        lblResseguros.Caption = lTotalRegistros
          
        If Val(lblResseguros.Caption) > 0 Then
          sMensagem = "Processamento realizado com sucesso."
        Else
          sMensagem = "Nenhum documento foi gerado."
        End If
    End If
        
    MousePointer = vbNormal
    StbRodape.Panels(1).Text = sMensagem
    
Exit Sub
Trata_Erro:
    Call TratarErro("AtualizarInterface", Me.name)
    Call FinalizarAplicacao
End Sub

Private Function ObterUltimoResseguro() As Long

Dim SQL As String
Dim oRs As rdoResultset

On Error GoTo Trata_Erro
    
    If optOuroVidaGarantia.Value = True Then
        SQL = "SELECT COUNT(*) FROM resseguro_vida_tb "
        Set oRs = rdocn.OpenResultset(SQL)
        ObterUltimoResseguro = oRs(0)
    Else
        SQL = "SELECT MAX(resseguro_objeto_id) FROM resseguro_objeto_tb"
        Set oRs = rdocn.OpenResultset(SQL)
        ObterUltimoResseguro = oRs(0)
    End If
    
    Exit Function
    
Trata_Erro:
    Call TratarErro("ObterUltimoResseguro", Me.name)
    Call FinalizarAplicacao

End Function

Function ValidarParametros() As Boolean

On Error GoTo Trata_Erro

  ValidarParametros = False
  
  If optVida.Value = True Then
  
    If IsNumeric(TxtMes.Text) = False Then
       MensagemBatch ("Parametro inv�lido ou n�o informado")
       TxtMes.SetFocus
       Exit Function
    End If
    
    If IsNumeric(TxtAno.Text) = False Then
       MensagemBatch ("Parametro inv�lido ou n�o informado")
       TxtAno.SetFocus
       Exit Function
    End If
    
    If Val(TxtMes.Text) < 1 Or Val(TxtMes.Text) > 12 Then
       MensagemBatch ("Parametro inv�lido ou n�o informado")
       TxtMes.SetFocus
       Exit Function
    End If
  
  End If
  
  If optVida.Value = False And _
     optRE.Value = False And _
     optOuroVidaGarantia.Value = False And _
     optCancelamentoRE.Value = False And _
     optCancelamentoVida.Value = False And _
     optReativacao_resseguro.Value = False Then
     MensagemBatch ("Parametro inv�lido ou n�o informado")
     optRE.SetFocus
     Exit Function
  End If
  
  ValidarParametros = True
  
Exit Function

Trata_Erro:
    Call TratarErro("ValidarParametros", Me.name)
    Call FinalizarAplicacao

End Function


Private Sub cmdProcessar_Click()

    Call ProcessarCalculoResseguro

End Sub

Sub ConexaoResseg()

Dim SIS_server As String
Dim SIS_banco As String
Dim SIS_usuario As String
Dim SIS_senha As String
Dim oSABL0010 As Object

On Error GoTo Trata_Erro

    Set oSABL0010 = CreateObject("SABL0010.cls00009")
    
    SIS_server = oSABL0010.RetornaValorAmbiente("RESSEG", "RELATORIO", "Servidor", gsCHAVESISTEMA, glAmbiente_id)
    SIS_banco = oSABL0010.RetornaValorAmbiente("RESSEG", "RELATORIO", "Banco", gsCHAVESISTEMA, glAmbiente_id)
    SIS_usuario = oSABL0010.RetornaValorAmbiente("RESSEG", "RELATORIO", "Usuario", gsCHAVESISTEMA, glAmbiente_id)
    SIS_senha = oSABL0010.RetornaValorAmbiente("RESSEG", "RELATORIO", "Senha", gsCHAVESISTEMA, glAmbiente_id)
    
    With rdocn
       .Connect = "UID=" & SIS_usuario & ";PWD=" & UCase(SIS_senha) & ";server=" & SIS_server & ";driver={SQL Server};database=" & SIS_banco & ";"
       .QueryTimeout = 30000
       .CursorDriver = rdUseNone
       .EstablishConnection rdDriverNoPrompt
    End With
      
    Set oSABL0010 = Nothing

Exit Sub

Trata_Erro:
    Call TratarErro("ConexaoResseg", Me.name)
    Call FinalizarAplicacao

End Sub
Private Sub cmdSair_Click()

On Error GoTo Trata_Erro

    Call FinalizarAplicacao
    
Exit Sub

Trata_Erro:
    Call TratarErro("cmdSair_Click", Me.name)
    Call FinalizarAplicacao

End Sub

Private Sub Form_Load()
   
On Local Error GoTo Trata_Erro
    
'Pula se for executado pelo control-m
If Verifica_Origem_ControlM(Command) = False Then
    'Verificando permiss�o de acesso � aplica��o''''''''''''''''''''''''''''''''''''''''''
    If Not Trata_Parametros(Command) Then
'      FinalizarAplicacao
    End If
End If
    'C:\RSGP0006.30592.LOG CTM 3 SVCCONTROLM "N" "N" "N" "N" "0" "0" "N" "S"
    'Teste - Comentar antes da libera��o
    cUserName = "vanessa"
    glAmbiente_id = 3
    
    Call ConexaoResseg
    
    'Obtendo a data operacional ''''''''''''''''''''''''''''''''''''''''''''
    Call ObterDataSistema("SEGBR")
        
    ' Configurando interface'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    Me.Caption = App.Title & " - C�lculo de Resseguro" & ExibirAmbiente(glAmbiente_id)
    
    Call InicializarInterface
    
    ' Iniciando o processo automaticamente caso seja uma chamada do Scheduler '''''''''''''''
    If Obtem_agenda_diaria_id(Command) > 0 Or Verifica_Origem_ControlM(Command) = True Then
        Me.Show
        Call cmdProcessar_Click
        FinalizarAplicacao
    End If
       
Exit Sub
  
Trata_Erro:
    Call TratarErro("Form_Load", Me.name)
    Call FinalizarAplicacao

End Sub

Private Sub Form_Resize()

'On Error GoTo Trata_Erro
'
'    Call InicializarRodape(Me)
'
'    Exit Sub
'
'Trata_Erro:
'    Call TratarErro("Form_Resize", Me.name)
'    Call FinalizarAplicacao

End Sub

Private Sub Form_Unload(Cancel As Integer)

On Error GoTo Trata_Erro

    Call FinalizarAplicacao

    Exit Sub

Trata_Erro:
    Call TratarErro("Form_Unload", Me.name)
    Call FinalizarAplicacao

End Sub

Sub InicializarInterface()
    
On Error GoTo Trata_Erro

    optRE.Value = True
            
    lblDetalhes.Visible = False
    lblCancelamento.Visible = False
    lblResseguros.Visible = True
    lblLog1.Visible = True
    lblLog2.Visible = False
    lblLog3.Visible = False
    TxtAno.Visible = False
    TxtMes.Visible = False
    lblAno.Visible = False
    lblMes.Visible = False
    
    lblInicio.Caption = ""
    lblFim.Caption = ""
    lblResseguros.Caption = ""
    lblDetalhes.Caption = ""
    lblCancelamento.Caption = ""
    
    StbRodape.Panels(1).Text = "Selecione Processar..."
    
Exit Sub

Trata_Erro:
    Call TratarErro("InicializarInterface", Me.name)
    Call FinalizarAplicacao
        
End Sub

Sub ProcessarCalculoResseguro()

Dim rs As Object
Dim sMensagem As String

Dim sAnoMes As String
Dim oResseguro As Object

Dim SQL As String
Dim oRs As rdoResultset

Dim lUltimo_Resseguro_Antes As Long
Dim lUltimo_Resseguro_Apos  As Long

Dim lTotalRessegurosVida As Long
Dim lTotalRessegurosVidaDetalhados As Long

Dim lTotalCanceladoRE As Long

Dim iTipo As Integer

Dim oRessegPlano      As Object
Dim oRessegEndosso    As Object

On Error GoTo Trata_Erro
    
    ' Tratamento para o Scheduler ''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'    InicializaParametrosExecucaoBatch Me
    
    If ValidarParametros = False Then
       If Obtem_agenda_diaria_id(Command) > 0 Then
        '   FinalizarAplicacao
       Else
           Exit Sub
       End If
    End If
    
    ' Atualizando interface ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    MousePointer = vbHourglass
    cmdProcessar.Enabled = False
    lblInicio = Now
    sMensagem = "Processando c�lculo resseguro..."
    StbRodape.Panels(1).Text = sMensagem
    DoEvents
    
    ' Iniciando gera��o de fatura resumo '''''''''''''''''''''''''''''''''''''''''''''''''
    sAnoMes = TxtAno.Text & TxtMes.Text
    lUltimo_Resseguro_Antes = ObterUltimoResseguro
    
    If Not optRE.Value Then rdocn.BeginTrans
    
    If optRE.Value = True Then
    

        Set oRessegPlano = CreateObject("RSGL0002.cls00423")

        Screen.MousePointer = vbHourglass

        Call oRessegPlano.GerarCalculodeResseguro("RESSEG", _
                                                    App.Title, _
                                                    App.FileDescription, _
                                                    glAmbiente_id, _
                                                    0, _
                                                    0, _
                                                    cUserName, _
                                                    Data_Sistema)



        'Eduardo.Maior - Demanda 19566551 - Calcular Resseguro em Endosso de Ap�lice Aberta.
        'Calcular os Endossos.
        '
        Set oRessegEndosso = CreateObject("RSGL0002.cls00423")

        Screen.MousePointer = vbHourglass

        Call oRessegEndosso.GerarCalculoEndossodeResseguro("RESSEG", _
                                                           App.Title, _
                                                           App.FileDescription, _
                                                           glAmbiente_id, _
                                                           cUserName, _
                                                           Data_Sistema)


'      SQL = ""
'      SQL = "EXEC calculo_resseguro_re_contrato_spi '" & cUserName & "'" & vbNewLine
'      Call rdocn.Execute(SQL)

''' pfiche - 30/11/2006 - tratamento de erro ser� feito somente pelo 'On Error'
'''      If rdoErrors().Count > 0 Then
''' pfiche - 08/11/2006 - If comentado para tratar todos os erros ocorridos
'''        If rdoErrors(0).Number = 55555 Then
'''           GoTo Trata_erro
'''        End If
'''      End If

    ElseIf optVida.Value = True Then

      SQL = ""
      SQL = "EXEC resseg_db..calculo_resseguro_vida_contrato_spi '" & sAnoMes & "', '" & cUserName & "'"
      Set oRs = rdocn.OpenResultset(SQL)

''' pfiche - 30/11/2006 - tratamento de erro ser� feito somente pelo 'On Error'
'''      If rdoErrors().Count > 0 Then
'''        If rdoErrors(0).Number = 55555 Then
'''           GoTo Trata_erro
'''        End If
'''      End If

      lTotalRessegurosVida = oRs(0)
      lTotalRessegurosVidaDetalhados = oRs(1)
    
    ElseIf optOuroVidaGarantia.Value = True Then

      'LSA - 06/09/2006
      'FLOW 161116 - Desativar processo para acompanhamento de implanta��o do modelo de contrato
      'SQL = ""
      'SQL = "EXEC calculo_resseguro_ourovida_garantia_old_spi '" & cUserName & "','" & sAnoMes & "'"
      'Call rdocn.Execute(SQL)
      'If rdoErrors().Count > 0 Then
      '  If rdoErrors(0).Number = 55555 Then
      '     GoTo Trata_Erro
      '   End If
      'End If

      SQL = ""
      SQL = "EXEC calculo_resseguro_ourovida_garantia_contrato_spi '" & cUserName & "','" & sAnoMes & "'"
      Call rdocn.Execute(SQL)
      
''' pfiche - 30/11/2006 - tratamento de erro ser� feito somente pelo 'On Error'
'''      If rdoErrors().Count > 0 Then
'''        If rdoErrors(0).Number = 55555 Then
'''           GoTo Trata_erro
'''        End If
'''      End If
   
    ElseIf optCancelamentoRE.Value = True Then
      SQL = ""
      SQL = "EXEC resseg_db..RSGS0200_spu '" & cUserName & "'" ''''& IIf(sAnoMes <> "", ",'" & sAnoMes & "'", "")
      Call rdocn.Execute(SQL)

''' pfiche - 30/11/2006 - tratamento de erro ser� feito somente pelo 'On Error'
'''      If rdoErrors().Count > 0 Then
'''        If rdoErrors(0).Number = 55555 Then
'''           GoTo Trata_erro
'''        End If
'''      End If

    'JOAO.MACHADO
    '29/02/2012
    'INCLUS�O DO CANCELAMENTO VIDA
    ElseIf optCancelamentoVida.Value = True Then
        SQL = ""
        SQL = "EXEC resseg_db..RSGS00356_SPI '" & cUserName & "'"
        Set oRs = rdocn.OpenResultset(SQL)
        lblCancelamento.Caption = oRs(0)
    
    'Inicio-Autor: Rodrigo.Moura Data:24/11/2016
    ElseIf optReativacao_resseguro.Value = True Then
    
      SQL = ""
      SQL = "EXEC resseg_db.dbo.RSGS00366_SPI '" & cUserName & "'"
      Call rdocn.Execute(SQL)
    
    'Fim- Autor: Rodrigo.Moura Data:24/11/2016
    End If
    
    
    
    If Not optRE Then rdocn.CommitTrans
    lUltimo_Resseguro_Apos = ObterUltimoResseguro

    'Atualizando interface ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    'Passar os valores do rs para vari�veis antes de comitar e depois passar para a
    'funcao de interface
    'Verificando op��o de c�lculo escolhida '''''''''''''''''''''''''''''''''''''''''''''
    If optVida.Value = True Then
        iTipo = 0
    ElseIf optRE.Value = True Then
        iTipo = 1
    ElseIf optOuroVidaGarantia.Value = True Then
        iTipo = 2
    ElseIf optCancelamentoRE.Value = True Then
        iTipo = 3
    ElseIf optReativacao_resseguro.Value = True Then
        iTipo = 4
    End If

    Call AtualizarInterface(iTipo, lTotalRessegurosVida, lTotalRessegurosVidaDetalhados, lUltimo_Resseguro_Apos - lUltimo_Resseguro_Antes)
    Call GravarLog(iTipo)

Exit Sub

Trata_Erro:
    
    Call TrataErroGeral("ProcessarCalculoResseguro", Me.name)
    Call TerminaSEGBR

End Sub
Private Sub GravarLog(iTipo As Integer)

On Error GoTo Trata_Erro

  ' Logando o n�mero de registros processados - Scheduler '''''''''''''''''''''''''''
  If iTipo = VIDA Then
    Call goProducao.AdicionaLog(1, Val(lblResseguros.Caption))
    Call goProducao.AdicionaLog(2, Val(lblDetalhes.Caption))
    
    '###########################grava log arquivo CTM
    If CTM = True Then
      Call GravaLogCTM("OK - ", Val(lblResseguros.Caption), Val(lblDetalhes.Caption), "")
    End If
    '################################################
    
  ElseIf iTipo = RE Then
    Call goProducao.AdicionaLog(1, Val(lblResseguros.Caption))
    
    '###########################grava log arquivo CTM
    If CTM = True Then
      Call GravaLogCTM("OK - ", Val(lblResseguros.Caption), "", "")
    End If
    '################################################
    
  ElseIf iTipo = CANCELAMENTO_RE Then
    Call goProducao.AdicionaLog(1, Val(lblCancelamento.Caption))
    
    '###########################grava log arquivo CTM
    If CTM = True Then
      Call GravaLogCTM("OK - ", Val(lblCancelamento.Caption), "", "")
    End If
    '################################################
  ElseIf iTipo = REATIVACAO_RESSEGURO Then
  
     Call goProducao.AdicionaLog(1, Val(lblResseguros.Caption))
    
    '###########################grava log arquivo CTM
    If CTM = True Then
      Call GravaLogCTM("OK - ", Val(lblResseguros.Caption), "", "")
    End If
    '################################################
  End If
  
'pula caso executado pelo control-m
  If CTM = False Then
    Call goProducao.finaliza
  End If


Exit Sub

Trata_Erro:
    Call TratarErro("GravarLog", Me.name)
    Call FinalizarAplicacao
    
End Sub

Private Sub optCancelamentoRE_Click()
    lblDetalhes.Visible = False
    lblResseguros.Visible = False
    lblCancelamento.Visible = True
    lblLog1.Visible = False
    lblLog2.Visible = False
    lblLog3.Visible = True
    TxtAno.Visible = True
    TxtMes.Visible = True
    lblAno.Visible = True
    lblMes.Visible = True
End Sub

Private Sub optCancelamentoVida_Click()
    TxtAno.Visible = False
    TxtMes.Visible = False
    lblAno.Visible = False
    lblMes.Visible = False
    
    lblLog3.Visible = True
    lblCancelamento.Visible = True
End Sub

Private Sub optOuroVidaGarantia_Click()
    
    lblDetalhes.Visible = True
    lblResseguros.Visible = True
    lblCancelamento.Visible = False
    lblLog1.Visible = True
    lblLog2.Visible = True
    lblLog3.Visible = False
    TxtAno.Visible = True
    TxtMes.Visible = True
    lblAno.Visible = True
    lblMes.Visible = True
    
End Sub

Private Sub optRE_Click()
    
    lblDetalhes.Visible = False
    lblResseguros.Visible = True
    lblCancelamento.Visible = False
    lblLog1.Visible = True
    lblLog2.Visible = False
    lblLog3.Visible = False
    TxtAno.Visible = False
    TxtMes.Visible = False
    lblAno.Visible = False
    lblMes.Visible = False

End Sub


Private Sub optVida_Click()
    
    lblDetalhes.Visible = True
    lblResseguros.Visible = True
    lblCancelamento.Visible = False
    lblLog1.Visible = True
    lblLog2.Visible = True
    lblLog3.Visible = False
    TxtAno.Visible = True
    TxtMes.Visible = True
    lblAno.Visible = True
    lblMes.Visible = True
  
End Sub


