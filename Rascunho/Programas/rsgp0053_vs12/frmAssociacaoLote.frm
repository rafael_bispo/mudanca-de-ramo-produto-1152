VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFlxGrd.ocx"
Begin VB.Form frmAssociacaoLote 
   Caption         =   " "
   ClientHeight    =   5775
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   8895
   LinkTopic       =   "Form2"
   ScaleHeight     =   5775
   ScaleWidth      =   8895
   StartUpPosition =   1  'CenterOwner
   Begin VB.CommandButton cmdVoltar 
      Caption         =   "Voltar"
      Height          =   375
      Left            =   7080
      TabIndex        =   8
      Top             =   4800
      Width           =   1755
   End
   Begin VB.CommandButton cmdAssociarLote 
      Caption         =   "Associar"
      Height          =   375
      Left            =   5160
      TabIndex        =   7
      Top             =   4800
      Width           =   1755
   End
   Begin MSComctlLib.StatusBar StbRodape 
      Align           =   2  'Align Bottom
      Height          =   315
      Left            =   0
      TabIndex        =   0
      Top             =   5460
      Width           =   8895
      _ExtentX        =   15690
      _ExtentY        =   556
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   3
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   8997
            MinWidth        =   8997
            Text            =   "Mensagem"
            TextSave        =   "Mensagem"
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            AutoSize        =   2
            Object.Width           =   2170
            MinWidth        =   88
            Text            =   "Data do sistema"
            TextSave        =   "Data do sistema"
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            AutoSize        =   2
            Object.Width           =   1111
            MinWidth        =   88
            Text            =   "Usu�rio"
            TextSave        =   "Usu�rio"
         EndProperty
      EndProperty
   End
   Begin VB.Frame fraContrato 
      Caption         =   "Contratos"
      Height          =   4560
      Left            =   120
      TabIndex        =   1
      Top             =   120
      Width           =   8745
      Begin MSFlexGridLib.MSFlexGrid GrdContratoLote 
         Height          =   3555
         Left            =   225
         TabIndex        =   5
         Top             =   720
         Width           =   6615
         _ExtentX        =   11668
         _ExtentY        =   6271
         _Version        =   393216
         Cols            =   3
         FixedCols       =   0
         SelectionMode   =   1
         AllowUserResizing=   1
         Appearance      =   0
         FormatString    =   "Contrato                                             | Vers�o                                                              | ID"
      End
      Begin VB.CommandButton cmdAdicionarLote 
         Caption         =   "Adicionar"
         Height          =   375
         Left            =   6975
         TabIndex        =   4
         Top             =   1080
         Width           =   1635
      End
      Begin VB.CommandButton cmdRemoverLote 
         Caption         =   "Remover"
         Height          =   375
         Left            =   6975
         TabIndex        =   3
         Top             =   1620
         Width           =   1635
      End
      Begin VB.ComboBox cmbContratoLote 
         Height          =   315
         ItemData        =   "frmAssociacaoLote.frx":0000
         Left            =   1140
         List            =   "frmAssociacaoLote.frx":0002
         Style           =   2  'Dropdown List
         TabIndex        =   2
         Top             =   270
         Width           =   3210
      End
      Begin VB.Label lblContrato 
         AutoSize        =   -1  'True
         Caption         =   "Contrato:"
         Height          =   195
         Left            =   390
         TabIndex        =   6
         Top             =   360
         Width           =   645
      End
   End
End
Attribute VB_Name = "frmAssociacaoLote"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public Contratos As New Collection
Public DtInicio As String
Public DtFim As String
Public Apolices As New Collection
Private Const strChecked = "�"      'Na fonte Wingdings � um Checkbox Checado


Private Sub cmdAdicionarLote_Click()


If Not (VerificaContrato) Then

    If cmbContratoLote.ListIndex <> -1 Then
        
        GrdContratoLote.AddItem cmbContratoLote.ItemData(cmbContratoLote.ListIndex) & vbTab & _
                                Trim(Mid(cmbContratoLote.Text, InStr(cmbContratoLote.Text, ":") + 1, 3))

    Else
        
        MsgBox "Selecione um contrato!", vbOKOnly
    
    End If
    
    If GrdContratoLote.Rows > 1 Then
        cmdRemoverLote.Enabled = True
    End If

End If


End Sub

Private Sub cmdAssociarLote_Click()

  Dim oDocumento As Object
  Dim Apolice As Object
  Dim Contrato As Object
  
  On Error GoTo TrataErro
  
  Screen.MousePointer = vbHourglass
  
  Call CollectionContratos
  
  Set oDocumento = CreateObject("RSGL0002.cls00127")
    
  For Each Contrato In Contratos
    
    For Each Apolice In Apolices

        Call oDocumento.AssociarDocumentos("RESSEG", _
                                           App.Title, _
                                           App.FileDescription, _
                                           glAmbiente_id, _
                                           Apolice.apolice_id, _
                                           Apolice.ramo_id, _
                                           Apolice.seguradora_cod_susep, _
                                           Apolice.sucursal_seguradora_id, _
                                           Contratos, _
                                           Data_Sistema, _
                                           cUserName, _
                                           , _
                                           True)
                                     
    
    Next
  
  Next
  
  Screen.MousePointer = vbDefault
  
  MsgBox "Associa��es efetuadas com sucesso", , "Associa��o de Documentos"
  
  'SD02281614 - Cibele validar quando o usu�rio digita data de fim maior que data de inicio 20/04/2020
  
  Set oDocumento = Nothing
  Set Contratos = Nothing
  Set Apolices = Nothing
  frmPesquisa.cmdAssociarLote.Enabled = False
  
  Screen.MousePointer = vbDefault
  Unload Me
  frmPesquisa.Pesquisar
  
  Exit Sub
  
TrataErro:
    MsgBox "Erro ao associar documento: " & Err.Description
    Call TratarErro("cmdAssociarLote_Click", Me.name)
  
End Sub


Private Sub cmdVoltar_Click()

    Unload Me

End Sub

Private Sub Form_Load()

On Error GoTo Trata_Erro
  
  'Configurando interface''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  Screen.MousePointer = vbHourglass
  Call CentraFrm(Me)
  GrdContratoLote.ColWidth(2) = 0 'C�digo do Contrato
  GrdContratoLote.Rows = 1

  'Carregando contratos para associa��o '''''''''''''''''''''''''''''''''''''''''''''''''
  Call CarregarContratos
  
  'Carregando apolices da grid de pesquisa'''''''''''''''''''''''''''''''''''''''''''''''
  Call CarregaApolices
    
  'Atualizando interfaze ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  Me.Caption = App.Title & " - Associa��o Manual de Documentos" & ExibirAmbiente(glAmbiente_id)
  Screen.MousePointer = vbDefault
    
  Exit Sub
    
Trata_Erro:
    Call TratarErro("Form_Load", Me.name)

End Sub

Private Sub CarregarContratos()

Dim rsContrato As Object
Dim sSQL       As String
Dim oContrato  As Object


Dim iAux       As Integer
Dim ExisteContrato As Boolean

On Error GoTo Trata_Erro

 
   
'Selecionando contratos que podem ser associados a proposta '''''''''''''''''''''''''''''
Set oContrato = CreateObject("RSGL0002.cls00127")

Set rsContrato = oContrato.ObterContratoPossivelAssociacaoLote("RESSEG", _
                                                           App.Title, _
                                                           App.FileDescription, _
                                                           glAmbiente_id, _
                                                           frmPesquisa.GrdResult.TextMatrix(frmPesquisa.GrdResult.Row, 3), _
                                                           frmPesquisa.GrdResult.TextMatrix(frmPesquisa.GrdResult.Row, 4), _
                                                           Format(frmPesquisa.mskDtEmissaoInicio, "yyyymmdd"), _
                                                           Format(frmPesquisa.mskDtEmissaoFim, "yyyymmdd"))

If Not rsContrato.EOF Then
   
    'Montando combo de contrato '''''''''''''''''''''''''''''''''''''''''''''''''''''
    While Not rsContrato.EOF
      
      'Verificando se o contrato foi inserido no grid (previamente escolhido) '''''''
      ExisteContrato = False
      If GrdContratoLote.Rows > 1 Then
        For iAux = 1 To GrdContratoLote.Rows - 1
          If rsContrato("contrato_id") = GrdContratoLote.TextMatrix(iAux, 2) Then
              ExisteContrato = True
              Exit For
          End If
        Next
      End If
    
      'Adicionando apenas os contratos que n�o foram inseridos na combo '''''''''''''
      If ExisteContrato = False Then
        cmbContratoLote.AddItem rsContrato("contrato_id") & " - " & rsContrato("documento_referencia") & " - Vers�o: " & rsContrato("num_versao")
        cmbContratoLote.ItemData(cmbContratoLote.NewIndex) = rsContrato("contrato_id")
      End If
          
      rsContrato.MoveNext
      
    Wend
End If

rsContrato.Close

Set rsContrato = Nothing

    Exit Sub
    
Trata_Erro:
    Call TratarErro("CarregarContrato", Me.name)

End Sub

Private Sub CarregaApolices()

    Dim Apolice As clsApolice
    Dim iCont As Integer
    
    For iCont = 1 To frmPesquisa.GrdResult.Rows - 1
        
        If frmPesquisa.GrdResult.TextMatrix(iCont, 0) = strChecked Then
        
            Set Apolice = New clsApolice
            
            Apolice.apolice_id = frmPesquisa.GrdResult.TextMatrix(iCont, 2)
            Apolice.ramo_id = frmPesquisa.GrdResult.TextMatrix(iCont, 3)
            Apolice.seguradora_cod_susep = frmPesquisa.GrdResult.TextMatrix(frmPesquisa.GrdResult.Row, 10)
            Apolice.sucursal_seguradora_id = frmPesquisa.GrdResult.TextMatrix(frmPesquisa.GrdResult.Row, 11)
           
            Apolices.Add Apolice
        
        End If
    
    Next

End Sub

Private Sub CollectionContratos()

    Dim Contrato As clsContrato
    Dim iCont As Integer
    
    For iCont = 1 To Me.GrdContratoLote.Rows - 1
    
        Set Contrato = New clsContrato
        
        Contrato.contrato_id = GrdContratoLote.TextMatrix(iCont, 0)
        Contrato.num_versao = GrdContratoLote.TextMatrix(iCont, 1)
        
        Contratos.Add Contrato
            
    Next
    
End Sub
Function VerificaContrato() As Boolean

Dim iAux As Integer

On Error GoTo Trata_Erro

'True  - Existe o contrato cadastrado na grid
'False - N�o existe o contrato cadastrado na grid

VerificaContrato = False

If GrdContratoLote.Rows > 1 Then
    For iAux = 1 To GrdContratoLote.Rows - 1
        If Trim(Mid(cmbContratoLote.Text, 1, InStr(cmbContratoLote.Text, "-") - 1)) = GrdContratoLote.TextMatrix(iAux, 0) Then
            VerificaContrato = True
            MsgBox "Contrato j� selecionado", vbOKOnly
            Exit For
        End If
    Next
End If

Exit Function

Trata_Erro:
    Call TratarErro("VerificaContrato", Me.name)

End Function
Private Sub cmdRemoverLote_Click()

On Error GoTo Trata_Erro

  Call RemoverContrato

Exit Sub

Trata_Erro:
    Call TratarErro("cmdRemover_Click", Me.name)

End Sub

Sub RemoverContrato()

Dim iAux As Integer

On Error GoTo Trata_Erro

    'Removendo item da grid '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    
    If GrdContratoLote.Rows > 1 Then
        For iAux = 1 To GrdContratoLote.Rows - 1
            If iAux = GrdContratoLote.Row Then
                If GrdContratoLote.Rows > 2 Then
                    GrdContratoLote.RemoveItem (GrdContratoLote.Row)
                    Exit For
                Else
                    'Removendo �ltimo item
                    GrdContratoLote.Rows = 1
                End If
            End If
        Next
    End If
    
    'Atualizando Interface ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    cmbContratoLote.ListIndex = -1
       
    If GrdContratoLote.Rows = 1 Then
        cmdRemoverLote.Enabled = False
    End If

Exit Sub

Trata_Erro:
    Call TratarErro("RemoverContrato", Me.name)

End Sub
