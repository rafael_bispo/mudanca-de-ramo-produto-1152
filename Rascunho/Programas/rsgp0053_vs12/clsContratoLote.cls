VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsApolice"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Private mvarapolice_id As Long
Private mvarramo_id As Integer
Private mvarseguradoracodsusep As Integer
Private mvarsucursalseguradora_id As Integer


Public Property Let ramo_id(ByVal vData As Integer)
    
    mvarramo_id = vData

End Property

Public Property Get ramo_id() As Integer

    ramo_id = mvarramo_id
    
End Property

Public Property Let seguradora_cod_susep(ByVal vData As Integer)
    
    mvarseguradoracodsusep = vData

End Property

Public Property Get seguradora_cod_susep() As Integer

    seguradora_cod_susep = mvarseguradoracodsusep
    
End Property

Public Property Let sucursal_seguradora_id(ByVal vData As Integer)
    
    mvarsucursalseguradora_id = vData

End Property

Public Property Get sucursal_seguradora_id() As Integer

    sucursal_seguradora_id = mvarsucursalseguradora_id
    
End Property


Public Property Let apolice_id(ByVal vData As Long)
    
    mvarapolice_id = vData
    
End Property

Public Property Get apolice_id() As Long
    
    apolice_id = mvarapolice_id
    
End Property






