VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsContrato"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
'local variable(s) to hold property value(s)
Private mvarcontrato_id As Long 'local copy
Private mvarnum_versao As Integer 'local copy

Public Property Let num_versao(ByVal vData As Integer)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.num_versao = 5
    mvarnum_versao = vData
End Property


Public Property Get num_versao() As Integer
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.num_versao
    num_versao = mvarnum_versao
End Property



Public Property Let contrato_id(ByVal vData As Long)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.contrato_id = 5
    mvarcontrato_id = vData
End Property


Public Property Get contrato_id() As Long
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.contrato_id
    contrato_id = mvarcontrato_id
End Property





