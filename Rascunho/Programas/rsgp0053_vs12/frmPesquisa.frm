VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.2#0"; "MSCOMCTL.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Object = "{C932BA88-4374-101B-A56C-00AA003668DC}#1.1#0"; "msmask32.ocx"
Begin VB.Form frmPesquisa 
   Caption         =   "Form1"
   ClientHeight    =   7845
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   9090
   LinkTopic       =   "Form1"
   ScaleHeight     =   7845
   ScaleWidth      =   9090
   StartUpPosition =   3  'Windows Default
   Begin VB.Frame fraResultado 
      Caption         =   "Opera��es"
      Height          =   5085
      Left            =   120
      TabIndex        =   6
      Top             =   1680
      Width           =   8895
      Begin MSFlexGridLib.MSFlexGrid GrdResult 
         Height          =   4095
         Left            =   80
         TabIndex        =   15
         Top             =   240
         Width           =   6975
         _ExtentX        =   12303
         _ExtentY        =   7223
         _Version        =   393216
         Cols            =   13
         FixedCols       =   0
         SelectionMode   =   1
         AllowUserResizing=   1
         Appearance      =   0
         FormatString    =   $"frmPesquisa.frx":0000
      End
      Begin VB.CommandButton cmdAssociarLote 
         Caption         =   "Associar em Lote"
         Enabled         =   0   'False
         Height          =   375
         Left            =   7140
         TabIndex        =   19
         Top             =   1920
         Width           =   1635
      End
      Begin VB.CommandButton cmdAlterarContrato 
         Caption         =   "Alterar Contrato"
         Height          =   375
         Left            =   7140
         TabIndex        =   14
         Top             =   1320
         Width           =   1635
      End
      Begin VB.CommandButton cmdNaoAssociar 
         Caption         =   "N�o Associar"
         Height          =   375
         Left            =   7140
         TabIndex        =   13
         Top             =   780
         Width           =   1635
      End
      Begin VB.CommandButton cmdAssociar 
         Caption         =   "Associar"
         Height          =   375
         Left            =   7140
         TabIndex        =   2
         Top             =   300
         Width           =   1635
      End
      Begin MSFlexGridLib.MSFlexGrid GrdResumo 
         Height          =   585
         Left            =   120
         TabIndex        =   7
         TabStop         =   0   'False
         Top             =   4440
         Width           =   6825
         _ExtentX        =   12039
         _ExtentY        =   1032
         _Version        =   393216
         FixedCols       =   0
         BackColor       =   -2147483628
         ForeColor       =   -2147483635
         AllowBigSelection=   0   'False
         Appearance      =   0
         FormatString    =   "Informa��es de Resumo                                                                                  |Valor             "
      End
      Begin MSFlexGridLib.MSFlexGrid GrdProposta 
         Height          =   4095
         Left            =   90
         TabIndex        =   16
         Top             =   300
         Visible         =   0   'False
         Width           =   6885
         _ExtentX        =   12144
         _ExtentY        =   7223
         _Version        =   393216
         Cols            =   8
         FixedCols       =   0
         SelectionMode   =   1
         AllowUserResizing=   1
         Appearance      =   0
         FormatString    =   $"frmPesquisa.frx":013D
      End
   End
   Begin VB.Frame fraCriterios 
      Caption         =   "Crit�rios de Pesquisa"
      Height          =   1575
      Left            =   120
      TabIndex        =   4
      Top             =   0
      Width           =   8895
      Begin VB.ComboBox cmbSubramo 
         Height          =   315
         ItemData        =   "frmPesquisa.frx":01DF
         Left            =   3720
         List            =   "frmPesquisa.frx":01E6
         Style           =   2  'Dropdown List
         TabIndex        =   17
         Top             =   480
         Width           =   2985
      End
      Begin VB.TextBox txtApolice 
         Height          =   315
         Left            =   1920
         MaxLength       =   9
         TabIndex        =   10
         Top             =   1080
         Width           =   1605
      End
      Begin VB.ComboBox cmbRamo 
         Height          =   315
         ItemData        =   "frmPesquisa.frx":01F1
         Left            =   120
         List            =   "frmPesquisa.frx":01F8
         Style           =   2  'Dropdown List
         TabIndex        =   9
         Top             =   480
         Width           =   3465
      End
      Begin VB.TextBox txtProposta 
         Height          =   315
         Left            =   120
         MaxLength       =   9
         TabIndex        =   0
         Top             =   1080
         Width           =   1605
      End
      Begin VB.CommandButton cmdPesquisar 
         Caption         =   "Pesquisar"
         Default         =   -1  'True
         Height          =   375
         Left            =   7140
         TabIndex        =   1
         Top             =   360
         Width           =   1635
      End
      Begin MSMask.MaskEdBox mskDtEmissaoInicio 
         Height          =   315
         Left            =   3720
         TabIndex        =   21
         Top             =   1080
         Width           =   1395
         _ExtentX        =   2461
         _ExtentY        =   556
         _Version        =   393216
         HideSelection   =   0   'False
         MaxLength       =   11
         Mask            =   " ##/##/####"
         PromptChar      =   "_"
      End
      Begin MSMask.MaskEdBox mskDtEmissaoFim 
         Height          =   315
         Left            =   5280
         TabIndex        =   23
         Top             =   1080
         Width           =   1395
         _ExtentX        =   2461
         _ExtentY        =   556
         _Version        =   393216
         HideSelection   =   0   'False
         MaxLength       =   11
         Mask            =   " ##/##/####"
         PromptChar      =   "_"
      End
      Begin VB.Label lblDtEmissaoFim 
         AutoSize        =   -1  'True
         Caption         =   "Data Final:"
         Height          =   195
         Left            =   5280
         TabIndex        =   22
         Top             =   840
         Width           =   765
      End
      Begin VB.Label lblDtEmissaoInicio 
         AutoSize        =   -1  'True
         Caption         =   "Data Inicial:"
         Height          =   195
         Left            =   3720
         TabIndex        =   20
         Top             =   840
         Width           =   840
      End
      Begin VB.Label Label1 
         Caption         =   "Sub Ramo:"
         Height          =   225
         Left            =   3720
         TabIndex        =   18
         Top             =   240
         Width           =   975
      End
      Begin VB.Label lblRamo 
         Caption         =   "Ramo:"
         Height          =   225
         Left            =   120
         TabIndex        =   12
         Top             =   240
         Width           =   975
      End
      Begin VB.Label lblApolice 
         Caption         =   "Ap�lice:"
         Height          =   225
         Left            =   1920
         TabIndex        =   11
         Top             =   840
         Width           =   975
      End
      Begin VB.Label lblProposta 
         Caption         =   "Proposta:"
         Height          =   225
         Left            =   120
         TabIndex        =   5
         Top             =   840
         Width           =   975
      End
   End
   Begin VB.CommandButton cmdSair 
      Caption         =   "Sair"
      Height          =   375
      Left            =   7320
      TabIndex        =   3
      Top             =   6960
      Width           =   1635
   End
   Begin MSComctlLib.StatusBar StbRodape 
      Align           =   2  'Align Bottom
      Height          =   315
      Left            =   0
      TabIndex        =   8
      Top             =   7530
      Width           =   9090
      _ExtentX        =   16034
      _ExtentY        =   556
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   3
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   8997
            MinWidth        =   8997
            Text            =   "Mensagem"
            TextSave        =   "Mensagem"
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            AutoSize        =   2
            Object.Width           =   2170
            MinWidth        =   88
            Text            =   "Data do sistema"
            TextSave        =   "Data do sistema"
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            AutoSize        =   2
            Object.Width           =   1111
            MinWidth        =   88
            Text            =   "Usu�rio"
            TextSave        =   "Usu�rio"
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "frmPesquisa"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
 'Option Explicit
Public Propostas      As New Collection
Private Const strChecked = "�"      'Na fonte Wingdings � um Checkbox Checado
Private Const strUnChecked = " "    'Na fonte Wingdings � um Checkbox n�o Checado
Private bMarcaTodos As Boolean

Public Sub Pesquisar()

On Error GoTo Trata_Erro

Dim oAssociacao  As Object
Dim rsAssociacao As Recordset
Dim sLinha       As String
Dim lApolice     As Long
Dim iRamo        As Integer
Dim lSubRamoId   As Long
Dim lProposta    As Long
Dim lmskDtEmissaoInicio As String
Dim lmskDtEmissaoFim As String

'Demanda 18759130 - Melhorias no Resseguro do Habitacional
'Confitec Sistemas - Eduardo.Maior - 29/02/2016
Dim oConsultaProposta As Object
Dim rsConsultaProposta As Recordset
''''''''''''''''''''''''''''''''''''''''''''''''

  'Atualizando Interface ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''Cibele 05/05/2020
  GrdResult.Rows = 1
  GrdResult.TextMatrix(0, 0) = strChecked
    
  Set rsConsultaProposta = Nothing
  Set rsAssociacao = Nothing
  Set rsAssociacaoTotal = Nothing
      
  'Obtendo parametros de pesquisa '''''''''''''''''''''''''''''''''''''''''''''''''''''''
  If txtApolice.Text <> "" Then
    lApolice = txtApolice.Text
  Else
    lApolice = 0
  End If
      
  If cmbRamo.ListIndex <> -1 Then
    iRamo = cmbRamo.ItemData(cmbRamo.ListIndex)
  Else
    iRamo = 0
  End If
  
  If cmbSubramo.ListIndex <> -1 Then
      lSubRamoId = cmbSubramo.ItemData(cmbSubramo.ListIndex)
  Else
      lSubRamoId = 0
  End If
    
  If txtProposta.Text <> "" Then
    lProposta = txtProposta.Text
  Else
    lProposta = 0
  End If
  
  
   If Format(mskDtEmissaoInicio.Text, "yyyymmdd") <> " __/__/____" Then
    lmskDtEmissaoInicio = Format(mskDtEmissaoInicio.Text, "yyyymmdd")
   Else
    lmskDtEmissaoInicio = 0
   End If
  
   If Format(mskDtEmissaoFim.Text, "yyyymmdd") <> " __/__/____" Then
    lmskDtEmissaoFim = Format(mskDtEmissaoFim.Text, "yyyymmdd")
   Else
   lmskDtEmissaoFim = 0
   End If

  
  'Consultando documento com associa��o de contrato de resseguro pendente '''''''''''''''
  Set oAssociacao = CreateObject("RSGL0002.cls00127")
  
  Set rsAssociacao = oAssociacao.ConsultarAssociacaoDocumentoLote(gsSIGLASISTEMA, _
                                                              App.Title, _
                                                              App.FileDescription, _
                                                              glAmbiente_id, _
                                                              "P", _
                                                              lApolice, _
                                                              iRamo, _
                                                              lSubRamoId, _
                                                              lProposta, _
                                                              lmskDtEmissaoInicio, _
                                                              lmskDtEmissaoFim)
                                                                        
                                                              
  
  Set oAssociacao = Nothing
  
   'Consultando documento com associa��o de contrato de resseguro pendente Total'''''''''''''''Cibele 05/05/2020
    Set oAssociacaoTotal = CreateObject("RSGL0002.cls00127")
  
    Set rsAssociacaoTotal = oAssociacaoTotal.ConsultarAssociacaoDocumentoLoteTotal(gsSIGLASISTEMA, _
                                                              App.Title, _
                                                              App.FileDescription, _
                                                              glAmbiente_id, _
                                                              "P", _
                                                              lApolice, _
                                                              iRamo, _
                                                              lSubRamoId, _
                                                              lProposta, _
                                                              lmskDtEmissaoInicio, _
                                                              lmskDtEmissaoFim)
                                                                        
                                                              
  
    Set oAssociacaoTotal = Nothing
    
    sTotal = rsAssociacaoTotal("total") 'Cibele
    
    If sTotal > 1000 Then
        MsgBox ("Ser�o mostrados 1000 registros do total : " & sTotal)
    End If
    
    rsAssociacaoTotal.Close
    Set rsAssociacaoTotal = Nothing
      
     If rsAssociacao.EOF Then
        
  
    'Consultando documento com associa��o de contrato de resseguro para propostas b�sicas
    Set oConsultaProposta = CreateObject("RSGL0002.cls00127")
  
    Set rsConsultaProposta = oConsultaProposta.ConsultarPropostaBasica(gsSIGLASISTEMA, _
                                                                        App.Title, _
                                                                        App.FileDescription, _
                                                                        glAmbiente_id, _
                                                                        lApolice, _
                                                                        iRamo, _
                                                                        lProposta)
                                                                        
                                                                        'Rafael nacio
                                                                        'lmskDtEmissaoInicio, _
                                                                        'lmskDtEmissaoFim)
                                                                        
  
    Set oConsultaProposta = Nothing
    
    If Not rsConsultaProposta.EOF Then
        Call PreencherPropostaBasica(rsConsultaProposta)
        rsAssociacao.Close
        Set rsAssociacao = Nothing
        rsConsultaProposta.Close
        Set rsConsultaProposta = Nothing
        Exit Sub
    Else
        Call PreencherQuadroResumo
        Call MsgBox("Nenhum documento pendente de associa��o!")
        rsAssociacao.Close
        Set rsAssociacao = Nothing
        rsConsultaProposta.Close
        Set rsConsultaProposta = Nothing
        GrdResult.Visible = True
        GrdProposta.Visible = False
        cmdAssociar.Enabled = False
        cmdNaoAssociar.Enabled = False
        cmdAlterarContrato.Enabled = False
        Exit Sub
    End If
    
  End If
  
  'Preenchendo grid com documentos pendentes de associa��o ''''''''''''''''''''''''''''''
  While Not rsAssociacao.EOF
  
    sLinha = ""
    sLinha = sLinha & strUnChecked & vbTab
    sLinha = sLinha & rsAssociacao("proposta_id") & vbTab
    sLinha = sLinha & rsAssociacao("apolice_id") & vbTab
    sLinha = sLinha & rsAssociacao("ramo_id") & vbTab
    sLinha = sLinha & rsAssociacao("subramo_id") & vbTab
    sLinha = sLinha & rsAssociacao("produto_id") & vbTab
    sLinha = sLinha & rsAssociacao("nome_cliente") & vbTab
    sLinha = sLinha & rsAssociacao("nome_ramo") & vbTab              'oculta
    sLinha = sLinha & rsAssociacao("nome_subramo") & vbTab           'oculta
    sLinha = sLinha & rsAssociacao("nome_produto") & vbTab           'oculta
    sLinha = sLinha & rsAssociacao("seguradora_cod_susep") & vbTab   'oculta
    sLinha = sLinha & rsAssociacao("sucursal_seguradora_id") & vbTab 'oculta
    sLinha = sLinha & rsAssociacao("nome_pendencia")
    
    GrdResult.AddItem sLinha
    
    rsAssociacao.MoveNext
  Wend
    
  rsAssociacao.Close
  Set rsAssociacao = Nothing
  
 'Atualizando interface''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
 
 If cmbRamo.ListIndex <> -1 And cmbSubramo.ListIndex <> -1 And mskDtEmissaoInicio <> " __/__/____" And mskDtEmissaoFim <> " __/__/____" Then
  cmdAssociarLote.Enabled = True
 End If
 
  cmdAssociar.Enabled = True
  cmdNaoAssociar.Enabled = True
  
  GrdProposta.Visible = False
  cmdAlterarContrato.Enabled = False
    
  Call PreencherQuadroResumo
       
  StbRodape.Panels(1).Text = "Selecione um documento para associa��o"
  Exit Sub
  
Trata_Erro:
    Call TratarErro("Pesquisar", Me.name)
    Call FinalizarAplicacao
End Sub

Private Sub PreencherQuadroResumo()
Dim sLinha As String
  
  On Error GoTo Trata_Erro
  
  sLinha = "Registros Encontrados" & vbTab & GrdResult.Rows - 1
  GrdResumo.Rows = 1
  GrdResumo.AddItem sLinha
  
  Exit Sub
  
Trata_Erro:
    Call TratarErro("PreencherQuadroResumo", Me.name)
    Call FinalizarAplicacao
End Sub

Private Sub cmbRamo_Click()

     Call PreencherComboSubRamo(1)

End Sub




Private Sub cmdAlterarContrato_Click()

On Error GoTo Trata_Erro
  
  frmAlterarContrato.Show
  
  Exit Sub
  
Trata_Erro:
    Call TratarErro("cmdAlterarContrato_Click", Me.name)
    Call FinalizarAplicacao

End Sub

Private Sub cmdAssociar_Click()

  On Error GoTo Trata_Erro
  
  frmAssociacao.Show
  
  Exit Sub
  
Trata_Erro:
    Call TratarErro("cmdAssociar_Click", Me.name)
    Call FinalizarAplicacao

End Sub

Private Sub cmdAssociarLote_Click()
  On Error GoTo Trata_Erro
  
  Dim iCont As Integer
  Dim bTestaMarcado As Boolean
  
  'Rafael Inacio - Verifica se existe algum registro selecionado
  bTestaMarcado = False
   
  For iCont = 1 To GrdResult.Rows - 1
        
    If GrdResult.TextMatrix(iCont, 0) = strChecked Then
        
        bTestaMarcado = True
        Exit For
    
    End If
    
  Next
  
  If bTestaMarcado = False Then
    
    MsgBox ("Favor selecionar as ap�lices para associa��o")
    Exit Sub
    
  End If
  
  'Call CarregaApolices
  
  frmAssociacaoLote.Show
  
  Exit Sub
  
Trata_Erro:
    Call TratarErro("cmdAssociar_Click", Me.name)
    Call FinalizarAplicacao

End Sub

Private Sub cmdNaoAssociar_Click()
Dim oDocumento As Object
  
On Error GoTo Trata_Erro

  'Atualizando ap�lice pendente de associa��o ''''''''''''''''''''''
  Set oDocumento = CreateObject("RSGL0002.cls00127")
      
  If Not oDocumento.ValidarNaoAssociacaoDocumento("RESSEG", _
                                                  App.Title, _
                                                  App.FileDescription, _
                                                  glAmbiente_id, _
                                                  GrdResult.TextMatrix(frmPesquisa.GrdResult.Row, 2), _
                                                  GrdResult.TextMatrix(frmPesquisa.GrdResult.Row, 3)) Then
    
    If MsgBox("Aten��o: este documento obriga a vincula��o de contrato." & vbNewLine & "Voc� deseja confirmar a n�o associa��o ?", vbYesNo) = vbNo Then
      Exit Sub
    End If
    
  End If
  
  Call oDocumento.NaoAssociarDocumentos("RESSEG", _
                                        App.Title, _
                                        App.FileDescription, _
                                        glAmbiente_id, _
                                        GrdResult.TextMatrix(GrdResult.Row, 2), _
                                        GrdResult.TextMatrix(GrdResult.Row, 3), _
                                        Data_Sistema, _
                                        cUserName)
  
  'Destruindo objetos '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  Set oDocumento = Nothing
  
  
  Exit Sub
  
Trata_Erro:
    Call TratarErro("cmdNaoAssociar_Click", Me.name)
    Call FinalizarAplicacao

End Sub

Private Sub cmdPesquisar_Click()

On Error GoTo Trata_Erro

  'Atualizando interface ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  Screen.MousePointer = vbHourglass

  If ValidarPesquisa Then
    Call Pesquisar
  End If

  'Atualizando interface ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  'txtProposta.Text = ""
  'txtApolice.Text = ""
  'cmbRamo.ListIndex = 0
  Screen.MousePointer = vbDefault
  'mskDtEmissaoInicio.Text = " __/__/____"
  'mskDtEmissaoFim.Text = " __/__/____"

  Exit Sub
  
Trata_Erro:
    Call TratarErro("cmdPesquisar_Click", Me.name)
    Call FinalizarAplicacao
Resume
End Sub

Private Sub cmdSair_Click()

  On Error GoTo Trata_Erro

  Call FinalizarAplicacao
    
  Exit Sub

Trata_Erro:
    Call TratarErro("cmdSair_Click", Me.name)
End Sub

Private Sub Form_Load()

On Error GoTo Trata_Erro

  'Verificando permiss�o de acesso � aplica��o''''''''''''''''''''''''''''''''''''''''''Cibele
  If Not Trata_Parametros(Command) Then
'     Call FinalizarAplicacao
  End If
    
  'Retirar antes da libera��o cibele
  cUserName = "99988877714"
  glAmbiente_id = 3

  'Obtendo a data operacional ''''''''''''''''''''''''''''''''''''''''''''
  Call ObterDataSistema("SEGBR")

  'Configurando interface'''''''''''''''''''''''''''''''''''''''''''''''''
  Call CentraFrm(Me)
  
  bMarcaTodos = False
 
  'Atualizando interface''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  GrdResult.ColWidth(6) = 0  'Nome do Ramo
  GrdResult.ColWidth(7) = 0  'Nome do Subramo
  GrdResult.ColWidth(8) = 0  'Nome do Produto
  GrdResult.ColWidth(9) = 0  'C�digo da Seguradora
  GrdResult.ColWidth(10) = 0 'C�digo da Sucursal
  GrdResult.Col = 0: GrdResult.Row = 0: GrdResult.CellFontName = "Wingdings"
  GrdResult.TextMatrix(0, 0) = strChecked
  
  
  cmdAssociar.Enabled = False
  cmdNaoAssociar.Enabled = False
  Me.Caption = App.Title & " - Associa��o Manual de Documentos" & ExibirAmbiente(glAmbiente_id)
  
  Call PreencherComboRamo(1)
  GrdResult.Rows = 1
  
  'Demanda 18759130 - Melhorias no Resseguro do Habitacional ''''''''
  'Confitec Sistemas - Eduardo.Maior - 29/02/2016
  '
  GrdProposta.Rows = 1
  cmdAlterarContrato.Enabled = False
  '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

  StbRodape.Panels(1).Text = "Selecione a forma de pesquisa"
  
  Exit Sub

Trata_Erro:

    Call TratarErro("Form_Load", Me.name)
End Sub

Private Sub Form_Resize()
On Error GoTo Trata_Erro

  Call InicializarRodape

  Exit Sub

Trata_Erro:
  Call TratarErro("Form_Resize", Me.name)

End Sub

Function ValidarPesquisa() As Boolean

  On Error GoTo Trata_Erro

  ValidarPesquisa = True

  'Validando conteudo da proposta '''''''''''''''''''''''''''''''''''''''''''''''''''''''
  If Not IsNumeric(txtProposta.Text) And txtProposta.Text <> "" Then
    MsgBox "Conte�do da proposta inv�lido!", vbCritical
    ValidarPesquisa = False
    Exit Function
  End If

  'Validando conteudo da ap�lice ''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  If Not IsNumeric(txtApolice.Text) And txtApolice.Text <> "" Then
    MsgBox "Conte�do ap�lice inv�lido!", vbCritical
    ValidarPesquisa = False
    Exit Function
  End If
  
  If mskDtEmissaoInicio <> " __/__/____" And mskDtEmissaoFim = " __/__/____" Then
    MsgBox "A data final deve ser preenchida!", vbCritical
    ValidarPesquisa = False
    Exit Function
  End If
  
 'SD02281614 - Cibele validar quando o usu�rio digita data de fim maior que data de inicio 20/04/2020
 If Format(mskDtEmissaoInicio.Text, "yyyymmdd") > Format(mskDtEmissaoFim.Text, "yyyymmdd") Then
    MsgBox "A data final deve ser maior que a data inicial!", vbCritical
    ValidarPesquisa = False
    Exit Function
  End If
  
  Exit Function
  
Trata_Erro:
    Call TratarErro("ValidarPesquisa", Me.name)
    Call FinalizarAplicacao
    
End Function

Sub PreencherComboRamo(iFormulario As Integer)

'Combo do formul�rio:
   '1 - Pesquisa
   '2 - Subramo
   '3 - Cat�strofe
   '4 - ED
   '5 - Stop Loss

Dim oDadosBasicos As Object
Dim rsRamo As Recordset

On Error GoTo TratarErro:
       
    Set oDadosBasicos = CreateObject("SEGL0022.cls00117")
    
    Set rsRamo = oDadosBasicos.ConsultarRamo("SEGBR", _
                                            App.Title, _
                                            App.FileDescription, _
                                            glAmbiente_id)
                                               
    If iFormulario = 1 Then
      frmPesquisa.cmbRamo.Clear
      frmPesquisa.cmbRamo.AddItem "Todos"
      frmPesquisa.cmbRamo.ItemData(frmPesquisa.cmbRamo.NewIndex) = 0
    ElseIf iFormulario = 2 Then
      frmSubRamo.cmbRamo.Clear
    ElseIf iFormulario = 3 Then
      frmPlanoCatastrofe.cmbRamoContabilizacao.Clear
    ElseIf iFormulario = 4 Then
      frmPlanoED.cmbRamoContabilizacao.Clear
    Else
      frmPlanoStopLoss.cmbRamoContabilizacao.Clear
    End If
    
    While Not rsRamo.EOF
       
       If iFormulario = 1 Then
          If rsRamo("ramo_id") <> 0 Then
            frmPesquisa.cmbRamo.AddItem rsRamo("ramo_id") & " - " & rsRamo("nome")
            frmPesquisa.cmbRamo.ItemData(frmPesquisa.cmbRamo.NewIndex) = rsRamo("ramo_id")
          End If
       ElseIf iFormulario = 2 Then
            frmSubRamo.cmbRamo.AddItem rsRamo("ramo_id") & " - " & rsRamo("nome")
            frmSubRamo.cmbRamo.ItemData(frmSubRamo.cmbRamo.NewIndex) = rsRamo("ramo_id")
       ElseIf iFormulario = 3 Then
            frmPlanoCatastrofe.cmbRamoContabilizacao.AddItem rsRamo("ramo_id") & " - " & rsRamo("nome")
            frmPlanoCatastrofe.cmbRamoContabilizacao.ItemData(frmPlanoCatastrofe.cmbRamoContabilizacao.NewIndex) = rsRamo("ramo_id")
       ElseIf iFormulario = 4 Then
            frmPlanoED.cmbRamoContabilizacao.AddItem rsRamo("ramo_id") & " - " & rsRamo("nome")
            frmPlanoED.cmbRamoContabilizacao.ItemData(frmPlanoED.cmbRamoContabilizacao.NewIndex) = rsRamo("ramo_id")
       Else
            frmPlanoStopLoss.cmbRamoContabilizacao.AddItem rsRamo("ramo_id") & " - " & rsRamo("nome")
            frmPlanoStopLoss.cmbRamoContabilizacao.ItemData(frmPlanoStopLoss.cmbRamoContabilizacao.NewIndex) = rsRamo("ramo_id")
       End If
    
       rsRamo.MoveNext
            
    Wend
                                    
    rsRamo.Close
    
    Set rsRamo = Nothing
    Set oDadosBasicos = Nothing

Exit Sub

TratarErro:
  Call TratarErro("PreencherComboRamo", "M�dulo Local")
  Call FinalizarAplicacao

End Sub

Sub PreencherComboSubRamo(iFormulario As Integer)

'Combo do formul�rio:
   '1 - Pesquisa
   '2 - Subramo

Dim oDadosBasicos As Object
Dim oSubRamo As Object

On Error GoTo TratarErro:
    
    Set oDadosBasicos = CreateObject("SEGL0022.cls00117")
    
    If iFormulario = 1 Then
    
      Set oSubRamo = oDadosBasicos.ConsultarSubRamo("SEGBR", _
                                                     App.Title, _
                                                     App.FileDescription, _
                                                     glAmbiente_id, 0, _
                                                     frmPesquisa.cmbRamo.ItemData(frmPesquisa.cmbRamo.ListIndex))
      frmPesquisa.cmbSubramo.Clear
      
      If frmPesquisa.cmbRamo.ItemData(frmPesquisa.cmbRamo.ListIndex) = 0 Then
         Exit Sub
      End If
      
    Else
    
      Set oSubRamo = oDadosBasicos.ConsultarSubRamo("SEGBR", _
                                                     App.Title, _
                                                     App.FileDescription, _
                                                     glAmbiente_id, 0, _
                                                     frmSubRamo.cmbRamo.ItemData(frmSubRamo.cmbRamo.ListIndex))
      frmSubRamo.cmbSubramo.Clear
      
    End If
    
    While Not oSubRamo.EOF
    
       If iFormulario = 1 Then
         frmPesquisa.cmbSubramo.AddItem oSubRamo("subramo_id") & " - " & oSubRamo("nome")
         frmPesquisa.cmbSubramo.ItemData(frmPesquisa.cmbSubramo.NewIndex) = oSubRamo("subramo_id")
       Else
         frmSubRamo.cmbSubramo.AddItem oSubRamo("subramo_id") & " - " & oSubRamo("nome")
         frmSubRamo.cmbSubramo.ItemData(frmSubRamo.cmbSubramo.NewIndex) = oSubRamo("subramo_id")
       End If
       
       oSubRamo.MoveNext
         
    Wend
                                    
    oSubRamo.Close
    
    Set oSubRamo = Nothing
    Set oDadosBasicos = Nothing

Exit Sub

TratarErro:
  Call TratarErro("PreencherComboSubRamo", "M�dulo Local")
  Call FinalizarAplicacao

End Sub

Private Sub PreencherPropostaBasica(rs As Recordset)

Dim sLinha As String

On Error GoTo Trata_Erro

    'Atualizando Grid de Propostas
    GrdProposta.Rows = 1

    'Preenchendo grid com documento associado a proposta b�sica '''''''''''
    While Not rs.EOF
  
        sLinha = ""
        sLinha = sLinha & rs("proposta_id") & vbTab
        sLinha = sLinha & rs("produto_id") & vbTab
        sLinha = sLinha & rs("apolice_id") & vbTab
        sLinha = sLinha & rs("ramo_id") & vbTab
        sLinha = sLinha & rs("contrato_id") & vbTab
        sLinha = sLinha & rs("num_versao") & vbTab
        sLinha = sLinha & rs("seguradora_cod_susep") & vbTab
        sLinha = sLinha & rs("sucursal_seguradora_id") & vbTab
        
        GrdProposta.AddItem sLinha
    
        rs.MoveNext
    Wend
  
    'Atualizando interface
    GrdResult.Visible = False
    GrdProposta.Visible = True
    GrdProposta.ColWidth(6) = 0  'C�digo da Seguradora
    GrdProposta.ColWidth(7) = 0  'C�digo da Sucursal
    cmdAssociar.Enabled = False
    cmdNaoAssociar.Enabled = False
    cmdAlterarContrato.Enabled = True
    
    Call PreencherQuadroResumoProposta
       
    StbRodape.Panels(1).Text = "Selecione um documento para altera��o do contrato"
  
  Exit Sub
  
Trata_Erro:
    Call TratarErro("PreencherPropostaBasica", Me.name)
    Call FinalizarAplicacao

End Sub

Private Sub PreencherQuadroResumoProposta()

Dim sLinha As String
  
  On Error GoTo Trata_Erro
  
  sLinha = "Registros Encontrados" & vbTab & GrdProposta.Rows - 1
  GrdResumo.Rows = 1
  GrdResumo.AddItem sLinha
  
  Exit Sub
  
Trata_Erro:
    Call TratarErro("PreencherQuadroResumoProposta", Me.name)
    Call FinalizarAplicacao
    
End Sub

Public Sub ColocarCheck(ByRef grdGrid As Object _
                      , ByVal bytLinha As Integer _
                      , ByVal bytColuna As Integer _
                      , Optional strFixo As String)

    With GrdResult
        .Row = bytLinha
        .Col = bytColuna
        .CellFontName = "Wingdings"
        .CellFontSize = 10
        .CellAlignment = flexAlignCenterCenter
        .CellAlignment = vbCenter
        If LenB(Trim(strFixo)) <> 0 Then
            .Text = strFixo
        Else
            If .Text = strUnChecked Then
                .Text = strChecked
            Else
                .Text = strUnChecked
            End If
        End If
    End With

    Exit Sub

Trata_Erro:
    Exit Sub
End Sub

Private Sub GrdResult_Click()

    If GrdResult.MouseRow = 0 Then
           
        Call SelecionarTudo
    
    Else
    
        Call ColocarCheck(GrdResult, GrdResult.RowSel, 0)
        'GrdResult.TextMatrix(GrdResult.RowSel, 0) = "X"
        
    End If
    
End Sub

Public Sub SelecionarTudo()

    Dim iCont As Integer
    
    If bMarcaTodos = False Then
    
        For iCont = 1 To GrdResult.Rows - 1
             
            If GrdResult.TextMatrix(iCont, 0) <> strChecked Then
                               
                With GrdResult
                    .Row = iCont
                    .Col = 0
                    .CellFontName = "Wingdings"
                    .CellFontSize = 10
                    .CellAlignment = flexAlignCenterCenter
                    .CellAlignment = vbCenter
                    .TextMatrix(iCont, 0) = strChecked
                End With
                
                bMarcaTodos = True
                
            End If
            
            Next
        
    Else
        
         For iCont = 1 To GrdResult.Rows - 1
             
            If GrdResult.TextMatrix(iCont, 0) = strChecked Then
                               
                With GrdResult
                
                    .TextMatrix(iCont, 0) = strUnChecked
                    .TextMatrix(0, 0) = strChecked
                        
                End With
                
                bMarcaTodos = False
                
            End If
            
            Next

    End If
End Sub





