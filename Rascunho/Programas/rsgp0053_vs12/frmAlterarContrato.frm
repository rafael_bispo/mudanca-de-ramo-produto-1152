VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFlxGrd.ocx"
Begin VB.Form frmAlterarContrato 
   Caption         =   " "
   ClientHeight    =   7605
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   8895
   LinkTopic       =   "Form2"
   ScaleHeight     =   7605
   ScaleWidth      =   8895
   StartUpPosition =   3  'Windows Default
   Begin VB.Frame fraContrato 
      Caption         =   "Contratos"
      Height          =   4560
      Left            =   45
      TabIndex        =   8
      Top             =   2070
      Width           =   8745
      Begin VB.TextBox txtVersaoContrato 
         BackColor       =   &H80000004&
         Height          =   285
         Left            =   1140
         Locked          =   -1  'True
         TabIndex        =   15
         Top             =   675
         Width           =   1185
      End
      Begin VB.ComboBox cmbContrato 
         Height          =   315
         ItemData        =   "frmAlterarContrato.frx":0000
         Left            =   1140
         List            =   "frmAlterarContrato.frx":0002
         Style           =   2  'Dropdown List
         TabIndex        =   12
         Top             =   270
         Width           =   3210
      End
      Begin VB.CommandButton cmdRemover 
         Caption         =   "Remover"
         Height          =   375
         Left            =   6975
         TabIndex        =   11
         Top             =   1620
         Width           =   1635
      End
      Begin VB.CommandButton cmdAdicionar 
         Caption         =   "Adicionar"
         Height          =   375
         Left            =   6975
         TabIndex        =   10
         Top             =   1080
         Width           =   1635
      End
      Begin MSFlexGridLib.MSFlexGrid GrdContrato 
         Height          =   3195
         Left            =   225
         TabIndex        =   9
         Top             =   1080
         Width           =   6615
         _ExtentX        =   11668
         _ExtentY        =   5636
         _Version        =   393216
         Cols            =   3
         FixedCols       =   0
         SelectionMode   =   1
         AllowUserResizing=   1
         Appearance      =   0
         FormatString    =   "Contrato                                             | Vers�o                                                              | ID"
      End
      Begin VB.Label lblVersao 
         AutoSize        =   -1  'True
         Caption         =   "Vers�o:"
         Height          =   195
         Left            =   495
         TabIndex        =   14
         Top             =   720
         Width           =   540
      End
      Begin VB.Label lblContrato 
         AutoSize        =   -1  'True
         Caption         =   "Contrato:"
         Height          =   195
         Left            =   390
         TabIndex        =   13
         Top             =   360
         Width           =   645
      End
   End
   Begin VB.Frame fraDocumento 
      Caption         =   "Documento"
      Height          =   1710
      Left            =   45
      TabIndex        =   3
      Top             =   165
      Width           =   8745
      Begin VB.TextBox txtProposta 
         BackColor       =   &H80000004&
         Height          =   285
         Left            =   2885
         TabIndex        =   22
         Top             =   315
         Width           =   1280
      End
      Begin VB.TextBox txtCliente 
         BackColor       =   &H80000004&
         Height          =   285
         Left            =   5220
         TabIndex        =   20
         Top             =   315
         Width           =   3400
      End
      Begin VB.TextBox txtSubramo 
         BackColor       =   &H80000004&
         Height          =   285
         Left            =   5220
         TabIndex        =   17
         Top             =   720
         Width           =   3400
      End
      Begin VB.TextBox txtProduto 
         BackColor       =   &H80000004&
         Height          =   285
         Left            =   765
         TabIndex        =   16
         Top             =   1125
         Width           =   3400
      End
      Begin VB.TextBox txtRamo 
         BackColor       =   &H80000004&
         Height          =   285
         Left            =   765
         TabIndex        =   7
         Top             =   720
         Width           =   3400
      End
      Begin VB.TextBox txtApolice 
         BackColor       =   &H80000004&
         Height          =   285
         Left            =   765
         TabIndex        =   5
         Top             =   315
         Width           =   1280
      End
      Begin VB.Label lblProposta 
         AutoSize        =   -1  'True
         Caption         =   "Proposta:"
         Height          =   195
         Left            =   2160
         TabIndex        =   23
         Top             =   360
         Width           =   675
      End
      Begin VB.Label lblCliente 
         AutoSize        =   -1  'True
         Caption         =   "Cliente:"
         Height          =   195
         Left            =   4605
         TabIndex        =   21
         Top             =   360
         Width           =   525
      End
      Begin VB.Label lblSubramo 
         AutoSize        =   -1  'True
         Caption         =   "SubRamo:"
         Height          =   195
         Left            =   4380
         TabIndex        =   19
         Top             =   765
         Width           =   750
      End
      Begin VB.Label lblProduto 
         AutoSize        =   -1  'True
         Caption         =   "Produto:"
         Height          =   195
         Left            =   90
         TabIndex        =   18
         Top             =   1170
         Width           =   600
      End
      Begin VB.Label lblRamo 
         AutoSize        =   -1  'True
         Caption         =   "Ramo:"
         Height          =   195
         Left            =   225
         TabIndex        =   6
         Top             =   765
         Width           =   465
      End
      Begin VB.Label lblApolice 
         AutoSize        =   -1  'True
         Caption         =   "Ap�lice:"
         Height          =   195
         Left            =   120
         TabIndex        =   4
         Top             =   360
         Width           =   570
      End
   End
   Begin VB.CommandButton cmdAssociar 
      Caption         =   "Associar"
      Height          =   375
      Left            =   5145
      TabIndex        =   0
      Top             =   6750
      Width           =   1755
   End
   Begin VB.CommandButton cmdVoltar 
      Caption         =   "Voltar"
      Height          =   375
      Left            =   7035
      TabIndex        =   1
      Top             =   6750
      Width           =   1755
   End
   Begin MSComctlLib.StatusBar StbRodape 
      Align           =   2  'Align Bottom
      Height          =   315
      Left            =   0
      TabIndex        =   2
      Top             =   7290
      Width           =   8895
      _ExtentX        =   15690
      _ExtentY        =   556
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   3
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   8997
            MinWidth        =   8997
            Text            =   "Mensagem"
            TextSave        =   "Mensagem"
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            AutoSize        =   2
            Object.Width           =   2170
            MinWidth        =   88
            Text            =   "Data do sistema"
            TextSave        =   "Data do sistema"
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            AutoSize        =   2
            Object.Width           =   1111
            MinWidth        =   88
            Text            =   "Usu�rio"
            TextSave        =   "Usu�rio"
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "frmAlterarContrato"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public Contratos      As New Collection

Private Sub Retornar()

On Error GoTo Trata_Erro

    Unload Me
    frmPesquisa.Show
    
    Exit Sub

Trata_Erro:
  Call TratarErro("Retornar", Me.name)
  
End Sub

Private Sub cmbContrato_Click()

On Error GoTo Trata_Erro

  If cmbContrato.ListIndex <> -1 Then
    Call ObterVersaoContrato
  End If
  
  Exit Sub

Trata_Erro:
  Call TratarErro("cmbContrato_Click", Me.name)

End Sub

Private Sub cmdAssociar_Click()

On Error GoTo Trata_Erro

  'Atualizando interface ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  Screen.MousePointer = vbHourglass

  'Associando documentos ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  If GrdContrato.Rows > 1 Then
    Call AssociarDocumento
  Else
    MsgBox "N�o foram selecionados contratos para associa��o", vbOKOnly
  End If
  
  'Atualizando interface ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  Screen.MousePointer = vbDefault
  
  Exit Sub

Trata_Erro:
  Call TratarErro("cmdAssociar_Click", Me.name)

End Sub

Private Sub cmdVoltar_Click()

On Error GoTo Trata_Erro

    Call Retornar
    
    Exit Sub

Trata_Erro:
  Call TratarErro("cmdVoltar_Click", Me.name)

End Sub

Private Sub Form_Load()

On Error GoTo Trata_Erro
  
  'Configurando interface''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  Screen.MousePointer = vbHourglass
  Call CentraFrm(Me)
  GrdContrato.ColWidth(2) = 0 'C�digo do Contrato
  GrdContrato.Rows = 1

  'Definindo documento para associa��o manual '''''''''''''''''''''''''''''''''''''''''''
  Call DefinirDocumento
  
  'Carregando contratos para associa��o '''''''''''''''''''''''''''''''''''''''''''''''''
  Call CarregarContratoPropostaBasica
  
  'Atualizando interface ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  Me.Caption = App.Title & " - Altera��o de Contrato Associado" & ExibirAmbiente(glAmbiente_id)
  Screen.MousePointer = vbDefault
    
  Exit Sub
    
Trata_Erro:
    Call TratarErro("Form_Load", Me.name)

End Sub

Private Sub Form_Resize()

On Error GoTo Trata_Erro

    Call InicializarRodape
    
    Exit Sub
    
Trata_Erro:
    Call TratarErro("Form_Resize", Me.name)

End Sub

Sub DefinirDocumento()

Dim oDadosProposta As Object
Dim rsDados As Object

On Error GoTo Trata_Erro


    'Selecionando as informa��es da proposta b�sica ''''''''''''''''
    Set oDadosProposta = CreateObject("RSGL0002.cls00127")

    Set rsDados = oDadosProposta.ObterDadosPropostaBasica("RESSEG", _
                                                        App.Title, _
                                                        App.FileDescription, _
                                                        glAmbiente_id, _
                                                        frmPesquisa.GrdProposta.TextMatrix(frmPesquisa.GrdProposta.Row, 0), _
                                                        frmPesquisa.GrdProposta.TextMatrix(frmPesquisa.GrdProposta.Row, 1), _
                                                        frmPesquisa.GrdProposta.TextMatrix(frmPesquisa.GrdProposta.Row, 3))

    txtProposta.Text = frmPesquisa.GrdProposta.TextMatrix(frmPesquisa.GrdProposta.Row, 0)
    txtApolice.Text = frmPesquisa.GrdProposta.TextMatrix(frmPesquisa.GrdProposta.Row, 2)
    
    If Not rsDados.EOF Then
        txtRamo.Text = rsDados("ramo_id") & " - " & rsDados("nome_ramo")
        txtSubramo.Text = rsDados("subramo_id") & " - " & rsDados("nome_subramo")
        txtProduto.Text = rsDados("produto_id") & " - " & rsDados("nome_produto")
        txtCliente.Text = rsDados("nome_cliente")
    End If
    
    Set oDadosProposta = Nothing
    rsDados.Close
    Set rsDados = Nothing

    Exit Sub
    
Trata_Erro:
    Call TratarErro("DefinirDocumento", Me.name)

End Sub

Sub CarregarContratoPropostaBasica()

Dim rsContrato As Object
Dim sSQL       As String
Dim oContrato  As Object
Dim iAux       As Integer
Dim ExisteContrato As Boolean

On Error GoTo Trata_Erro
   
'Selecionando contratos que podem ser associados a proposta '''''''''''''''''''''''''''''
Set oContrato = CreateObject("RSGL0002.cls00127")
                                                                
Set rsContrato = oContrato.ObterContratoPropostaBasica("RESSEG", _
                                                        App.Title, _
                                                        App.FileDescription, _
                                                        glAmbiente_id, _
                                                        txtProposta.Text)

If Not rsContrato.EOF Then
   
    'Montando combo de contrato '''''''''''''''''''''''''''''''''''''''''''''''''''''
    While Not rsContrato.EOF
      
      'Verificando se o contrato foi inserido no grid (previamente escolhido) '''''''
      ExisteContrato = False
      If GrdContrato.Rows > 1 Then
        For iAux = 1 To GrdContrato.Rows - 1
          If rsContrato("contrato_id") = GrdContrato.TextMatrix(iAux, 2) Then
              ExisteContrato = True
              Exit For
          End If
        Next
      End If
    
      'Adicionando apenas os contratos que n�o foram inseridos na combo '''''''''''''
      If ExisteContrato = False Then
        cmbContrato.AddItem rsContrato("documento_referencia")
        cmbContrato.ItemData(cmbContrato.NewIndex) = rsContrato("contrato_id")
      End If
    
      rsContrato.MoveNext
      
    Wend
End If

    rsContrato.Close
    Set rsContrato = Nothing

    Exit Sub
    
Trata_Erro:
    Call TratarErro("CarregarContratoPropostaBasica", Me.name)

End Sub

Sub ObterVersaoContrato()

Dim oVersao As Object
Dim rsVersao As Recordset
Dim sSQL As String

On Error GoTo Trata_Erro:
  
'Atualizando interface ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Screen.MousePointer = vbHourglass
  
'Pesquisando vers�o do contrato '''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Set oVersao = CreateObject("RSGL0002.cls00127")

Set rsVersao = oVersao.ObterContratoVigenteApolice("RESSEG", _
                                                    App.Title, _
                                                    App.FileDescription, _
                                                    glAmbiente_id, _
                                                    cmbContrato.ItemData(cmbContrato.ListIndex), _
                                                    Val(txtApolice.Text), _
                                                    Val(txtRamo.Text))
    
If Not rsVersao.EOF Then
    txtVersaoContrato.Text = rsVersao("num_versao")
Else
    txtVersaoContrato.Text = ""
End If

rsVersao.Close
Set rsVersao = Nothing
Set oVersao = Nothing
    
'Atualizando interface ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Screen.MousePointer = vbDefault
    
Exit Sub
    
Trata_Erro:
    Call TratarErro("ObterVersaoContrato", Me.name)

End Sub

Private Sub cmdAdicionar_Click()
    
On Error GoTo Trata_Erro
    
  If Not (VerificaContrato) Then
    Call AdicionarContrato
  End If
    
Exit Sub
    
Trata_Erro:
    Call TratarErro("ObterVersaoContrato", Me.name)
    
End Sub

Function VerificaContrato() As Boolean

Dim iAux As Integer

On Error GoTo Trata_Erro

'True  - Existe o contrato cadastrado na grid
'False - N�o existe o contrato cadastrado na grid

VerificaContrato = False

If GrdContrato.Rows > 1 Then
    For iAux = 1 To GrdContrato.Rows - 1
        If cmbContrato.Text = GrdContrato.TextMatrix(iAux, 0) Then
            VerificaContrato = True
            MsgBox "Contrato j� selecionado", vbOKOnly
            Exit For
        End If
    Next
End If

Exit Function

Trata_Erro:
    Call TratarErro("VerificaContrato", Me.name)

End Function

Sub AdicionarContrato()

On Error GoTo Trata_Erro

'Adcionando contrato ''''''''''''''''''''''''''''

If cmbContrato.ListIndex <> -1 Then
    GrdContrato.AddItem cmbContrato.Text & vbTab & _
                        txtVersaoContrato.Text & vbTab & _
                        cmbContrato.ItemData(cmbContrato.ListIndex)
Else
    MsgBox "Selecione um contrato", vbOKOnly
End If

'Atualizando Interface ''''''''''''''''''''''''''
cmbContrato.ListIndex = -1
txtVersaoContrato.Text = ""

If GrdContrato.Rows > 1 Then
    cmdRemover.Enabled = True
End If

Exit Sub

Trata_Erro:
    Call TratarErro("AdicionarContrato", Me.name)

End Sub

Private Sub cmdRemover_Click()

On Error GoTo Trata_Erro

  Call RemoverContrato

Exit Sub

Trata_Erro:
    Call TratarErro("cmdRemover_Click", Me.name)

End Sub

Sub RemoverContrato()

Dim iAux As Integer

On Error GoTo Trata_Erro

    'Removendo item da grid '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    
    If GrdContrato.Rows > 1 Then
        For iAux = 1 To GrdContrato.Rows - 1
            If iAux = GrdContrato.Row Then
                If GrdContrato.Rows > 2 Then
                    GrdContrato.RemoveItem (GrdContrato.Row)
                    Exit For
                Else
                    'Removendo �ltimo item
                    GrdContrato.Rows = 1
                End If
            End If
        Next
    End If
    
    'Atualizando Interface ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    cmbContrato.ListIndex = -1
    txtVersaoContrato.Text = ""
    
    If GrdContrato.Rows = 1 Then
        cmdRemover.Enabled = False
    End If

Exit Sub

Trata_Erro:
    Call TratarErro("RemoverContrato", Me.name)

End Sub

Private Sub AssociarDocumento()

Dim iAux As Integer
Dim oDocumento As Object

On Error GoTo Trata_Erro

  'Atualizando interface ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  Screen.MousePointer = vbHourglass

  'Montando cole��o com os contratos a serem associados ao documento ''''''''''''''''''''
  Call MontarColecaoContrato
  
  'Associando documento aos contratos de resseguro ''''''''''''''''''''''''''''''''''''''
  Set oDocumento = CreateObject("RSGL0002.cls00127")

  Call oDocumento.AssociarDocumentos("RESSEG", _
                                     App.Title, _
                                     App.FileDescription, _
                                     glAmbiente_id, _
                                     frmPesquisa.GrdProposta.TextMatrix(frmPesquisa.GrdProposta.Row, 2), _
                                     frmPesquisa.GrdProposta.TextMatrix(frmPesquisa.GrdProposta.Row, 3), _
                                     frmPesquisa.GrdProposta.TextMatrix(frmPesquisa.GrdProposta.Row, 6), _
                                     frmPesquisa.GrdProposta.TextMatrix(frmPesquisa.GrdProposta.Row, 7), _
                                     Contratos, _
                                     Data_Sistema, _
                                     cUserName)
  
  'Destruindo objetos '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  Set oDocumento = Nothing
  Set Contratos = Nothing
  
  'Atualizando Interface ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  Screen.MousePointer = vbDefault
  Unload Me
  frmPesquisa.Pesquisar
    
Exit Sub
    
Trata_Erro:
    Call TratarErro("AssociarDocumento", Me.name)

End Sub

Sub MontarColecaoContrato()

Dim Contrato As clsContrato
Dim iAux As Integer

On Error GoTo Trata_Erro:
    
  'Populando a cole��o de contratos '''''''''''''''''''''''''''''''''''''''''''
  If GrdContrato.Rows > 1 Then
    For iAux = 1 To GrdContrato.Rows - 1
        Set Contrato = New clsContrato
        Contrato.contrato_id = GrdContrato.TextMatrix(iAux, 2)
        Contrato.num_versao = GrdContrato.TextMatrix(iAux, 1)
        Contratos.Add Contrato
    Next
  End If
    
Exit Sub

Trata_Erro:
    Call TratarErro("MontarColecaoContrato", Me.name)

End Sub
